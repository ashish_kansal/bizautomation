/******************************************************************
Project: Release 14.9 Date: 11.FEB.2021
Comments: STORE PROCEDURES
*******************************************************************/

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetKitInventory')
DROP FUNCTION fn_GetKitInventory
GO
CREATE FUNCTION [dbo].[fn_GetKitInventory]
(
	@numKitId NUMERIC
	,@numWarehouseID NUMERIC(18,0)
)          
RETURNS FLOAT          
AS          
BEGIN          
	DECLARE @OnHand AS FLOAT
	SET @OnHand = 0       
    
	;WITH CTE(numItemKitID,numItemCode,numQtyItemsReq,numCalculatedQty) AS
	(
		SELECT 
			CONVERT(NUMERIC(18,0),0),
			numItemCode,
			DTL.numQtyItemsReq,
			CAST(DTL.numQtyItemsReq AS NUMERIC(9,0)) AS numCalculatedQty                                
		FROM 
			Item
		INNER JOIN 
			ItemDetails Dtl 
		ON 
			numChildItemID=numItemCode
		WHERE 
			numItemKitID=@numKitId 
		UNION ALL
		SELECT 
			Dtl.numItemKitID,
			i.numItemCode,
			DTL.numQtyItemsReq,
			CAST((DTL.numQtyItemsReq * c.numCalculatedQty) AS NUMERIC(9,0)) AS numCalculatedQty
		FROM 
			Item i                               
		INNER JOIN 
			ItemDetails Dtl 
		ON 
			Dtl.numChildItemID=i.numItemCode
		INNER JOIN 
			CTE c 
		ON 
			Dtl.numItemKitID = c.numItemCode 
		WHERE 
			Dtl.numChildItemID != @numKitId
	)

	SELECT
		@OnHand = CAST(MIN(FLOOR(numOnHand)) AS FLOAT) 
	FROM
	(
		SELECT 
			(CASE WHEN ISNULL(SUM(numOnHand),0)=0 THEN 0 WHEN ISNULL(SUM(numOnHand),0)>=numQtyItemsReq AND numQtyItemsReq>0 THEN ISNULL(SUM(numOnHand),0)/numQtyItemsReq ELSE 0 end) AS numOnHand
		FROM 
			cte c  
		LEFT JOIN
		(
			SELECT 
				numItemID
				,numWareHouseID
				,ISNULL(numOnHand,0) numOnHand
			FROM
				WareHouseItems
		) WI
		ON
			WI.numItemID = c.numItemCode
			AND WI.numWareHouseID = @numWarehouseID 
		GROUP BY
			c.numItemCode
			,c.numQtyItemsReq
	) TEMP

	RETURN @OnHand          
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetOpportunitySourceValue]    Script Date: 07/26/2008 18:12:38 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetOpportunitySourceValue')
DROP FUNCTION fn_GetOpportunitySourceValue
GO
CREATE FUNCTION [dbo].[fn_GetOpportunitySourceValue](@tintSource AS NUMERIC(18),@tintSourceType TINYINT,@numDomainID NUMERIC(18))
Returns varchar(100) 
As
BEGIN

declare @listName as varchar(200)

IF @tintSourceType=1 --List Master
BEGIN
	IF @tintSource = 0
	BEGIN
		SELECT @listName = 'Internal Order'
	END
	ELSE
	BEGIN
		SELECT @listName=ISNULL(vcData,'') from ListDetails WHERE numListItemID=@tintSource
	END
END
ELSE IF @tintSourceType=2 --WebSite
BEGIN
	  SELECT @listName=ISNULL(vcSiteName,'')
  FROM   Sites WHERE  [numDomainID] = @numDomainID AND numSiteID=@tintSource
END
ELSE IF @tintSourceType=3 --WebAPI
BEGIN
	 SELECT @listName=ISNULL(vcProviderName,'') FROM dbo.WebAPI WHERE WebApiId=@tintSource
END
ELSE IF @tintSourceType=4 --Marketplaces (EchannelHub)
BEGIN
	 SELECT @listName=ISNULL(vcMarketplace,'') FROM dbo.eChannelHub WHERE ID=@tintSource
END
	
	RETURN @listName
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetKitChildItemsDisplay')
DROP FUNCTION GetKitChildItemsDisplay
GO
CREATE FUNCTION [dbo].[GetKitChildItemsDisplay]
(
	@numItemKitID NUMERIC(18,0)
	,@numChildKitItemID  NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
)    
RETURNS VARCHAR(MAX)
AS    
BEGIN   
	DECLARE @vcDisplayText VARCHAR(MAX)
	DECLARE @vcItemName VARCHAR(300)
	DECLARE @vcSKU VARCHAR(100)
	DECLARE @vcModelID VARCHAR(100)
	
	SELECT @numItemCode=numItemCode,@vcItemName=ISNULL(vcItemName,''),@vcSKU=ISNULL(vcSKU,''),@vcModelID=ISNULL(vcModelID,'') FROM Item WHERE numItemCode=@numItemCode

	DECLARE @tintView TINYINT
	DECLARE @vcFields VARCHAR(1000)
	SELECT @tintView=tintView,@vcFields=ISNULL(vcFields,'') FROM ItemDetails WHERE numItemKitID=@numItemKitID AND numChildItemID=@numChildKitItemID

	IF LEN(@vcFields) > 0
	BEGIN
		IF @tintView = 2
		BEGIN
			SET @vcDisplayText = '<ul class="list-unstyled ul-child-kit-item" style="margin-bottom:0px;padding:10px">'

			IF CHARINDEX(',0~1,',CONCAT(',',@vcFields,','),0) <> 0
			BEGIN
				SET @vcDisplayText = CONCAT(@vcDisplayText,'<li><img class="ul-child-kit-item-image" src="',(CASE 
																WHEN ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1),'') = ''
																THEN '../Images/DefaultProduct.png'
																ELSE CONCAT('##URL##/',(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1))
															END),'" alt="',@vcItemName,'"/></li><li>')
			END
		END
		ELSE
		BEGIN
			SET @vcDisplayText = '<ul class="list-unstyled ul-child-kit-item" style="margin-bottom:0px;padding:10px">'
		END

		IF CHARINDEX(',0~2,',CONCAT(',',@vcFields,','),0) <> 0 AND LEN(@vcItemName) > 0
		BEGIN
			SET @vcDisplayText = CONCAT(@vcDisplayText,'<li class=" ul-child-kit-item-name">',@vcItemName,'</li>')
		END

		IF CHARINDEX(',0~3,',CONCAT(',',@vcFields,','),0) <> 0 AND LEN(@vcSKU) > 0
		BEGIN
			SET @vcDisplayText = CONCAT(@vcDisplayText,'<li>',@vcSKU,'</li>')
		END

		IF CHARINDEX(',0~4',CONCAT(',',@vcFields,','),0) <> 0 AND LEN(@vcModelID) > 0
		BEGIN
			SET @vcDisplayText = CONCAT(@vcDisplayText,'<li>',@vcModelID,'</li>')
		END

		SELECT
			@vcDisplayText = CONCAT(@vcDisplayText,'<li>',dbo.GetCustFldValueItem(CAST(REPLACE(OutParam,'1~','') AS NUMERIC),@numItemCode),'</li>')
		FROM
			dbo.SplitString(@vcFields,',')
		WHERE
			CHARINDEX('1~',OutParam,0) <> 0

		IF CHARINDEX(',0~1,',CONCAT(',',@vcFields,','),0) <> 0 AND @tintView = 1
		BEGIN
			SET @vcDisplayText = CONCAT('<ul class="list-inline ul-child-kit-item"><li><img class="ul-child-kit-item-image" src="',(CASE 
																							WHEN ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1),'') = ''
																							THEN '../Images/DefaultProduct.png'
																							ELSE CONCAT('##URL##/',(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1))
																						END),'" alt="',@vcItemName,'"/></li><li>',@vcDisplayText,'</ul></li></ul>')
		END
		ELSE
		BEGIN
			SET @vcDisplayText = CONCAT(@vcDisplayText,'</ul>')
		END
	END
	ELSE
	BEGIN
		SET @vcDisplayText = CONCAT(@vcItemName,' (Item Code:',@numItemCode,(CASE WHEN LEN(@vcSKU) > 0 THEN CONCAT(', SKU:',@vcSKU) ELSE '' END),')')
	END

	RETURN @vcDisplayText
END
GO

--- Created By Anoop jayaraj     
--- Modified By Gangadhar 03/05/2008                                                        

--Select all records  with 
--Rule 1. ANY relationship with a Sales Order. 
--Rule 2. Relationships that are Customers, that have been entered as Accounts from the UI or Record Import, or ..... 
GO
SET ANSI_NULLS on
GO
SET QUOTED_IDENTIFIER on
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_accountlist1')
DROP PROCEDURE usp_accountlist1
GO
CREATE PROCEDURE [dbo].[USP_AccountList1]                                                              
		@CRMType numeric,                                                              
		@numUserCntID numeric,                                                              
		@tintUserRightType tinyint,                                                              
		@tintSortOrder numeric=4,                                                                                  
		@SortChar char(1)='0',  
		@numDomainID as numeric(9)=0,                                                       
		@CurrentPage int,                                                            
		@PageSize int,        
		 @TotRecs int output,                                                           
		@columnName as Varchar(MAX),                                                            
		@columnSortOrder as Varchar(10)    ,                    
		@numProfile as numeric   ,                  
		@bitPartner as BIT,       
		@ClientTimeZoneOffset as int,
		@bitActiveInActive as bit,
		@vcRegularSearchCriteria varchar(MAX)='',
		@vcCustomSearchCriteria varchar(MAX)=''   ,
		@SearchText VARCHAR(100) = ''
AS                                                            
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
           
	DECLARE @tintPerformanceFilter AS TINYINT

	IF PATINDEX('%cmp.vcPerformance=1%', @vcRegularSearchCriteria)>0 --Last 3 Months
	BEGIN
		SET @tintPerformanceFilter = 1
	END
	ELSE IF PATINDEX('%cmp.vcPerformance=2%', @vcRegularSearchCriteria)>0 --Last 6 Months
	BEGIN
		SET @tintPerformanceFilter = 2
	END
	ELSE IF PATINDEX('%cmp.vcPerformance=3%', @vcRegularSearchCriteria)>0 --Last 1 Year
	BEGIN
		SET @tintPerformanceFilter = 3
	END
	ELSE
	BEGIN
		SET @tintPerformanceFilter = 0
	END

	IF CHARINDEX('AD.numBillCountry',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.numBillCountry','AD1.numCountry')
	END
	ELSE IF CHARINDEX('AD.numShipCountry',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.numShipCountry','AD2.numCountry')
	END
	ELSE IF CHARINDEX('AD.vcBillCity',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.vcBillCity','AD5.vcCity')
	END
	ELSE IF CHARINDEX('AD.vcShipCity',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.vcShipCity','AD6.vcCity')
	END
	IF CHARINDEX('ADC.vcLastFollowup',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'ADC.vcLastFollowup', '(SELECT dtExecutionDate FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND ISNULL(ADC.numECampaignID,0) > 0 AND numConECampDTLID IN (SELECT MAX(numConECampDTLID) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND ISNULL(ADC.numECampaignID,0) > 0 AND ISNULL(bitSend,0)=1))')
	END
	IF CHARINDEX('ADC.vcNextFollowup',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'ADC.vcNextFollowup','(SELECT dtExecutionDate FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND ISNULL(ADC.numECampaignID,0) > 0 AND numConECampDTLID IN (SELECT MIN(numConECampDTLID) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND ISNULL(ADC.numECampaignID,0) > 0 AND ISNULL(bitSend,0)=0))')
	END     

	

	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(200),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),
		intColumnWidth INT,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)   

	DECLARE @Nocolumns AS TINYINT = 0                

	SELECT 
		@Nocolumns =ISNULL(SUM(TotalRow),0)  
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=36 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType=2 
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=36 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType=2
	) TotalRows

	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 36 AND numRelCntType=2 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END


	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			36,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,2,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=36 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=2 AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			36,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,2,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=36 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=2 AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=36 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=2 AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=36 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=2 AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		if @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				36,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,2,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=36 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder ASC   
		END
          
		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,
			bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
			bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID ,intColumnWidth,bitAllowFiltering,vcFieldDataType
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=36 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType=2
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=36 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType=2
			AND ISNULL(bitCustom,0)=1
		ORDER BY 
			tintOrder asc  
	END


	DECLARE  @strColumns AS VARCHAR(MAX) = ''
	set @strColumns=' isnull(ADC.numContactId,0)numContactId,isnull(DM.numDivisionID,0)numDivisionID,isnull(DM.numTerID,0)numTerID,isnull(ADC.numRecOwner,0)numRecOwner,isnull(DM.tintCRMType,0) as tintCRMType,ADC.vcGivenName,ADC.vcFirstName+'' ''+ADC.vcLastName AS vcContactName,ADC.numPhone AS numContactPhone,ADC.numPhoneExtension AS numContactPhoneExtension,ADC.vcEmail AS vcContactEmail '   

	--Custom field 
	DECLARE @tintOrder AS TINYINT = 0                                                 
	DECLARE @vcFieldName AS VARCHAR(50)                                                  
	DECLARE @vcListItemType AS VARCHAR(3)                                             
	DECLARE @vcAssociatedControlType VARCHAR(20)     
	declare @numListID AS numeric(9)
	DECLARE @vcDbColumnName VARCHAR(200)                      
	DECLARE @WhereCondition VARCHAR(MAX) = ''                   
	DECLARE @vcLookBackTableName VARCHAR(2000)                
	DECLARE @bitCustom AS BIT
	DECLARE @bitAllowEdit AS CHAR(1)                   
	Declare @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)    
	DECLARE @vcColumnName AS VARCHAR(500)             
	Declare @ListRelID as numeric(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = '' 
	   
    SELECT TOP 1 
		@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
		@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,
		@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
	FROM  
		#tempForm 
	ORDER BY 
		tintOrder ASC            
                             
	WHILE @tintOrder>0                                                  
	BEGIN
		IF @bitCustom = 0                
		BEGIN
			DECLARE @Prefix AS VARCHAR(5)
			
			IF @vcLookBackTableName = 'AdditionalContactsInformation' 
				SET @Prefix = 'ADC.'
			IF @vcLookBackTableName = 'DivisionMaster' 
				SET @Prefix = 'DM.'
	
			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

			
			IF @vcDbColumnName='bitEcommerceAccess'
			BEGIN
				SET @strColumns=@strColumns+ ' ,(CASE WHEN (SELECt COUNT(*) FROM ExtarnetAccounts WHERE numDivisionID=DM.numDivisionID) >0 THEN 1 ELSE 0 END) AS ['+ @vcColumnName+']' 
			END

			----- Added by Priya For Followups(14Jan2018)---- 
			if @vcDbColumnName = 'vcLastFollowup'
			BEGIN
				set @strColumns=@strColumns+',  ISNULL((CASE WHEN (ADC.numECampaignID > 0) THEN dbo.fn_FollowupDetailsInOrgList(ADC.numContactID,1,ADC.numDomainID) ELSE '''' END),'''') ['+ @vcColumnName+']' 
			END
			ELSE IF @vcDbColumnName = 'vcNextFollowup'
			BEGIN
				set @strColumns=@strColumns+',  ISNULL((CASE WHEN  (ADC.numECampaignID >0) THEN dbo.fn_FollowupDetailsInOrgList(ADC.numContactID,2,ADC.numDomainID) ELSE '''' END),'''') ['+ @vcColumnName+']' 
			END
			----- Added by Priya For Followups(14Jan2018)---- 


			ELSE IF @vcAssociatedControlType='SelectBox' or @vcAssociatedControlType='ListBox'                  
			BEGIN

				IF @vcDbColumnName = 'vcSignatureType'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(CASE WHEN vcSignatureType = 0 THEN ''Service Default''
																	WHEN vcSignatureType = 1 THEN ''Adult Signature Required''
																	WHEN vcSignatureType = 2 THEN ''Direct Signature''
																	WHEN vcSignatureType = 3 THEN ''InDirect Signature''
																	WHEN vcSignatureType = 4 THEN ''No Signature Required''
																ELSE '''' END) '  + ' [' + @vcColumnName + ']'
				END
		
				IF @vcDbColumnName='numDefaultShippingServiceID'
				BEGIN
					SET @strColumns=@strColumns+ ' ,ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=DM.numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = DM.numDefaultShippingServiceID),'''')' + ' [' + @vcColumnName + ']' 
				END
				Else IF @vcDbColumnName = 'numPartenerSource'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(DM.numPartenerSource) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=DM.numPartenerSource) AS vcPartenerSource' 
				END
				ELSE IF @vcListItemType='LI'                                                         
				BEGIN
					IF @numListID=40 
					BEGIN
						SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'  
						                                                 
						IF LEN(@SearchText) > 0
						BEGIN 
							SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'' '     
						END                              
						IF @vcDbColumnName='numShipCountry'
						BEGIN
							SET @WhereCondition = @WhereCondition
												+ ' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= DM.numDomainID '
												+ ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=AD2.numCountry '
						END
						ELSE
						BEGIN
							SET @WhereCondition = @WhereCondition
											+ ' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= DM.numDomainID '
											+ ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=AD1.numCountry '
						END
					 END
					ELSE IF @numListID=5
					BEGIN
						SET @strColumns=@strColumns+',(CASE WHEN CMP.numCompanyType=47 THEN CONCAT(L'+ convert(varchar(3),@tintOrder)+'.vcData,'' '',''<i class="fa fa-info-circle text-blue" aria-hidden="true" title="Organizations with a sales orders will be listed in the Account list, just as those with Purchase Orders will be listed in the Vendor list. This explains why you may see a Vendor in the Accounts list and an Account in the Vendor list" style="font-size: 20px;"></i>'') ELSE L'+ convert(varchar(3),@tintOrder)+'.vcData END)'+' ['+ @vcColumnName+']'   
						
						IF LEN(@SearchText) > 0
						BEGIN 
							SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'' '          
						END  
						                                                        
						SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName
					END
					ELSE
					BEGIN
						SET @strColumns=@strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'   
						
						IF LEN(@SearchText) > 0
						BEGIN 
							SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'' '          
						END  
						                                                        
						SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                        
					 END
				END                                                        
				ELSE IF @vcListItemType='S'                                                         
				BEGIN 
					
					                                                           
					IF LEN(@SearchText) > 0
					BEGIN 
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S' + CONVERT(VARCHAR(3), @tintOrder) + '.vcState LIKE ''%' + @SearchText + '%'' '     
					END
					      						
					IF @vcDbColumnName='numShipState'
					BEGIN
						SET @strColumns = @strColumns + ',ShipState.vcState' + ' [' + @vcColumnName + ']' 
						SET @WhereCondition = @WhereCondition
							+ ' left Join AddressDetails AD3 on AD3.numRecordId= DM.numDivisionID and AD3.tintAddressOf=2 and AD3.tintAddressType=2 AND AD3.bitIsPrimary=1 and AD3.numDomainID= DM.numDomainID '
							+ ' left Join State ShipState on ShipState.numStateID=AD3.numState '
					END
					ELSE IF @vcDbColumnName='numBillState'
					BEGIN
						SET @strColumns = @strColumns + ',BillState.vcState' + ' [' + @vcColumnName + ']' 
						SET @WhereCondition = @WhereCondition
							+ ' left Join AddressDetails AD4 on AD4.numRecordId= DM.numDivisionID and AD4.tintAddressOf=2 and AD4.tintAddressType=1 AND AD4.bitIsPrimary=1 and AD4.numDomainID= DM.numDomainID '
							+ ' left Join State BillState on BillState.numStateID=AD4.numState '
					END
					ELSE  
					BEGIN
					SET @strColumns = @strColumns + ',S' + CONVERT(VARCHAR(3), @tintOrder) + '.vcState' + ' [' + @vcColumnName + ']' 
						SET @WhereCondition = @WhereCondition + ' left join State S' + CONVERT(VARCHAR(3), @tintOrder) + ' on S' + CONVERT(VARCHAR(3), @tintOrder) + '.numStateID=' + @vcDbColumnName        
					END		                                           
				END                                                        
				ELSE IF @vcListItemType='T'                                                         
				BEGIN
					SET @strColumns=@strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'   

					IF LEN(@SearchText) > 0
					BEGIN 
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcState LIKE ''%' + @SearchText + '%'' '     
					END                                                           
				  
					SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                        
				END  
				ELSE IF @vcListItemType='C'
				BEGIN
					SET @strColumns=@strColumns+',C'+ convert(varchar(3),@tintOrder)+'.vcCampaignName'+' ['+ @vcColumnName+']' 
					 
					IF LEN(@SearchText) > 0
					BEGIN 
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'C' + CONVERT(VARCHAR(3),@tintOrder) + '.vcCampaignName LIKE ''%' + @SearchText + '%'' '     
					END  
					                                                  
					SET @WhereCondition= @WhereCondition +' left Join CampaignMaster C'+ convert(varchar(3),@tintOrder)+ ' on C'+convert(varchar(3),@tintOrder)+ '.numCampaignId=DM.'+@vcDbColumnName                                                    
				END                       
				ELSE IF @vcListItemType='U'                                                     
				BEGIN
					SET @strColumns=@strColumns+',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'

					IF LEN(@SearchText) > 0
					BEGIN 
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'' '    
					END                                                          
				END  
				ELSE IF @vcListItemType='SYS'                                                         
				BEGIN
					SET @strColumns=@strColumns+',case tintCRMType when 1 then ''Prospect'' when 2 then ''Account'' else ''Lead'' end as ['+ @vcColumnName+']'                                                        

					IF LEN(@SearchText) > 0
					BEGIN 
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'case tintCRMType when 1 then ''Prospect'' when 2 then ''Account'' else ''Lead'' end LIKE ''%' + @SearchText + '%'' '    
					END         
				END                     
			END                                                        
			ELSE IF @vcAssociatedControlType='DateField'                                                    
			BEGIN
				 SET @strColumns=@strColumns+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''        
				 SET @strColumns=@strColumns+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''        
				 SET @strColumns=@strColumns+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '        
				 SET @strColumns=@strColumns+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'                  
					
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'' '
				END
			END      
			ELSE IF @vcAssociatedControlType='TextBox' AND @vcDbColumnName='vcBillCity' 
			BEGIN
				SET @strColumns = @strColumns + ',AD5.vcCity' + ' [' + @vcColumnName + ']'   

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + ' AD5.vcCity LIKE ''%' + @SearchText + '%'' '
				END
				
				SET @WhereCondition = @WhereCondition
							+ ' left Join AddressDetails AD5 on AD5.numRecordId= DM.numDivisionID and AD5.tintAddressOf=2 and AD5.tintAddressType=1 AND AD5.bitIsPrimary=1 and AD5.numDomainID= DM.numDomainID '

			END
			ELSE IF @vcAssociatedControlType='TextBox' AND  @vcDbColumnName='vcShipCity'
			BEGIN
				SET @strColumns=@strColumns + ',AD6.vcCity' + ' [' + @vcColumnName + ']' 
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + ' AD6.vcCity LIKE ''%' + @SearchText + '%'' '
				END  
				
				SET @WhereCondition = @WhereCondition + ' left Join AddressDetails AD6 on AD6.numRecordId= DM.numDivisionID and AD6.tintAddressOf=2 and AD6.tintAddressType=2 AND AD6.bitIsPrimary=1 and AD6.numDomainID= DM.numDomainID '
			END
			ELSE IF @vcAssociatedControlType='Label' and @vcDbColumnName='vcLastSalesOrderDate'
			BEGIN
				SET @WhereCondition = @WhereCondition
												+ ' OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=DM.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder '

				set @strColumns=@strColumns+','+ CONCAT('ISNULL(dbo.FormatedDateFromDate(DateAdd(minute, ',-@ClientTimeZoneOffset,',TempLastOrder.bintCreatedDate),',@numDomainID,'),'''')') +' ['+ @vcColumnName+']'
			END	
			ELSE IF @vcDbColumnName = 'vcCompactContactDetails'
			BEGIN
				SET @strColumns=@strColumns+ ' ,'''' AS vcCompactContactDetails'   
			END  
			ELSE IF @vcAssociatedControlType='Label' and @vcDbColumnName='vcPerformance'
			BEGIN
				SET @WhereCondition = @WhereCondition
												+ CONCAT(' OUTER APPLY(SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = DM.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		AND 1 = (CASE ', @tintPerformanceFilter,' 
																				WHEN 1 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-3,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 2 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-6,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 3 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(YEAR,-1,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				ELSE 1
																				END)
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId ) AS TempPerformance ')

				set @strColumns=@strColumns +', (CASE WHEN ISNULL(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(FORMAT(TempPerformance.monDealAmount,''##,####.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) ['+ @vcColumnName+']'
			END	
			ELSE IF @vcAssociatedControlType='TextBox'  OR @vcAssociatedControlType='Label'                                                   
			BEGIN
				SET @strColumns=@strColumns+','
								+ case  
									when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' 
									when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' 
									else @vcDbColumnName 
								end +' ['+ @vcColumnName+']'                
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 
					case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when      
				  @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' else @vcDbColumnName end
					+' LIKE ''%' + @SearchText + '%'' '
				END  
			END   
		 	ELSE IF @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
			BEGIN
				set @strColumns=@strColumns+',(SELECT SUBSTRING(
				(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
				FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
										 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
					 WHERE SR.numDomainID=DM.numDomainID AND SR.numModuleID=1 AND SR.numRecordID=DM.numDivisionID
							AND UM.numDomainID=DM.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
			END  
			ELSE                                                 
			BEGIN
				 set @strColumns=@strColumns+','+ @vcDbColumnName +' ['+ @vcColumnName+']'
			END             
		END                
		ELSE IF @bitCustom = 1                
		BEGIN
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'  
                
			SELECT 
				@vcFieldName=FLd_label,
				@vcAssociatedControlType=fld_type,
				@vcDbColumnName='Cust'+Convert(varchar(10),Fld_Id)                               
			FROM 
				CFW_Fld_Master
			WHERE 
				CFW_Fld_Master.Fld_Id = @numFieldId
			
			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'            
			BEGIN
				SET @strColumns= @strColumns+',CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
				
				SET @WhereCondition= @WhereCondition 
									+' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)
									+ ' on CFW' + convert(varchar(3),@tintOrder) 
									+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
									+ 'and CFW' + convert(varchar(3),@tintOrder) 
									+ '.RecId=DM.numDivisionId   '                                                         
			END   
			ELSE IF @vcAssociatedControlType = 'CheckBox'       
			BEGIN
				set @strColumns= @strColumns+',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0)='''' then 0 else  isnull(CFW'
												+ CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0) end ['+ @vcColumnName + ']'
 
				set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId   '                                                     
		   END                
			ELSE IF @vcAssociatedControlType = 'DateField'            
			BEGIN
				set @strColumns= @strColumns+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
				set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '                 
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId   '                                                         
			END                
			ELSE IF @vcAssociatedControlType = 'SelectBox'             
			BEGIN
				set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
				set @strColumns=@strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
				set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '                 
					on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId    '                                                         
				set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
			END
			ELSE IF @vcAssociatedControlType = 'CheckBoxList' 
			BEGIN
				SET @strColumns= @strColumns+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

			SET @WhereCondition= @WhereCondition 
								+' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId=DM.numDivisionId '
			END              
		END          
                
		SELECT TOP 1 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,
			@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
		FROM
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 SET @tintOrder=0 
	END     

	  
	DECLARE @strShareRedordWith AS VARCHAR(300);
	SET @strShareRedordWith= CONCAT(' DM.numDivisionID IN (SELECT SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID=',@numDomainID,' AND SR.numModuleID=1 AND SR.numAssignedTo=',@numUserCntId,') ')

	DECLARE @strExternalUser AS VARCHAR(MAX) = ''
	SET @strExternalUser = CONCAT(' (NOT EXISTS (SELECT numUserDetailID FROM UserMaster WHERE numDomainID=',@numDomainID,' AND numUserDetailID=',@numUserCntId,') AND EXISTS (SELECT EAD.numExtranetDtlID FROM ExtranetAccountsDtl EAD INNER JOIN ExtarnetAccounts EA ON EAD.numExtranetID=EA.numExtranetID WHERE EAD.numDomainID=',@numDomainID,' AND EAD.numContactID=',@numUserCntID,' AND (EA.numDivisionID=DM.numDivisionID OR EA.numDivisionID=DM.numPartenerSource)))')
   
	SET @strColumns=@strColumns+' ,ISNULL(VIE.Total,0) as TotalEmail '
	SET @strColumns=@strColumns+' ,ISNULL(VOA.OpenActionItemCount,0) as TotalActionItem '

	SET @strColumns=@strColumns+' ,

	((CASE 
	WHEN ADC.numECampaignID > 0 
	THEN 
		 ISNULL( (CASE WHEN 
				ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											WHERE ConECampaign.numContactID = ADC.numContactID AND  ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =1 AND ISNULL(bitSend,0) = 1 AND ADC.bitPrimaryContact = 1),0) 
				= ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											 WHERE ConECampaign.numContactID = ADC.numContactID AND ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =1 AND ADC.bitPrimaryContact = 1 ),0)
		THEN
		  ''CheckeredFlag''
		  Else
		  ''GreenFlag''
		  End),0)



	ELSE '''' 
	END)) as FollowupFlag '

	SET @strColumns=@strColumns+' ,

	((CASE 
	WHEN ADC.numECampaignID > 0 
	THEN 
		''('' + CAST(ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											WHERE ConECampaign.numContactID = ADC.numContactID AND  ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =1 AND ISNULL(bitSend,0) = 1 AND ADC.bitPrimaryContact = 1),0) AS varchar)

			+ ''/'' + CAST(ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											 WHERE ConECampaign.numContactID = ADC.numContactID AND ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =1 AND ADC.bitPrimaryContact = 1 ),0)AS varchar) + '')''
			+ ''<a onclick=openDrip('' + (cast(ISNULL((SELECT MAX([numConEmailCampID]) FROM [ConECampaign] WHERE [numContactID] = ADC.numContactID AND [numECampaignID]= ADC.numECampaignID AND ISNULL(bitEngaged,0)=1 AND ADC.bitPrimaryContact = 1 ),0)AS varchar)) + '');>
		 <img alt=Follow-up Campaign history height=16px width=16px title=Follow-up campaign history src=../images/GLReport.png
		 ></a>'' 
	ELSE '''' 
	END)) as ReadUnreadCntNHstr '
   
	DECLARE @StrSql AS VARCHAR(MAX) = ''
	SET @StrSql=' FROM  CompanyInfo CMP                                                            
					join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID
					LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=DM.numDivisionID 
					left join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID
					left join VIEW_OPEN_ACTION_ITEMS VOA WITH (NOEXPAND) ON VOA.numDomainID = '+ CAST(@numDomainID as varchar )+' AND  VOA.numDivisionId = DM.numDivisionID 
					left join View_InboxEmail VIE  WITH (NOEXPAND) ON VIE.numDomainID = '+ CAST(@numDomainID as varchar )+' AND  VIE.numContactId = ADC.numContactId ' + ISNULL(@WhereCondition,'')

	IF @tintSortOrder= 9 set @strSql=@strSql+' join Favorites F on F.numContactid=DM.numDivisionID ' 
	
	 -------Change Row Color-------
	Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
	SET @vcCSOrigDbCOlumnName=''
	SET @vcCSLookBackTableName=''

	Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

	insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
	from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
	join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
	where DFCS.numDomainID=@numDomainID and DFFM.numFormID=36 AND DFCS.numFormID=36 and isnull(DFFM.bitAllowGridColor,0)=1

	IF(SELECT COUNT(*) FROM #tempColorScheme)>0
	BEGIN
	   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
	END   
	----------------------------                   
 
	 IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
	set @strColumns=@strColumns+',tCS.vcColorScheme'
	END

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
			set @Prefix = 'ADC.'                  
		 if @vcCSLookBackTableName = 'DivisionMaster'                  
			set @Prefix = 'DM.'
		 if @vcCSLookBackTableName = 'CompanyInfo'                  
			set @Prefix = 'CMP.'   
 
		IF @vcCSAssociatedControlType='DateField'
		BEGIN
				set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
				 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
		END
		ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
		BEGIN
			set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
		END
	END

	IF @columnName LIKE 'CFW.Cust%' 
	BEGIN                
		SET @strSql = @strSql + ' left Join CFW_FLD_Values CFW on CFW.RecId=DM.numDivisionID and CFW.fld_id= ' + REPLACE(@columnName, 'CFW.Cust', '') + ' '                                                         
		SET @columnName = 'CFW.Fld_Value'                
	END                                         
	ELSE IF @columnName LIKE 'DCust%' 
    BEGIN                
        SET @strSql = @strSql + ' left Join CFW_FLD_Values CFW on CFW.RecId=DM.numDivisionID  and CFW.fld_id= ' + REPLACE(@columnName, 'DCust', '')                                                                                                   
        SET @strSql = @strSql + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '                
        SET @columnName = 'LstCF.vcData'                  
    END
	ELSE IF @columnName = 'Opp.vcLastSalesOrderDate'   
	BEGIN
		SET @columnName = 'TempLastOrder.bintCreatedDate'
	END
	ELSE IF @columnName = 'DMSC.vcSignatureType'
	BEGIN
		SET @columnName = '(CASE WHEN vcSignatureType = 0 THEN ''Service Default''
																	WHEN vcSignatureType = 1 THEN ''Adult Signature Required''
																	WHEN vcSignatureType = 2 THEN ''Direct Signature''
																	WHEN vcSignatureType = 3 THEN ''InDirect Signature''
																	WHEN vcSignatureType = 4 THEN ''No Signature Required''
																ELSE '''' END)'
	END
	
	IF @columnName = 'CMP.vcPerformance'   
	BEGIN
		SET @columnName = 'TempPerformance.monDealAmount'
	END

	SET @strSql = @strSql + ' WHERE  (ISNULL(ADC.bitPrimaryContact,0)=1   OR ADC.numContactID IS NULL)
							AND CMP.numDomainID=DM.numDomainID     
							AND (CMP.numCompanyType=46 OR (CMP.numCompanyType=47 AND EXISTS (SELECT numOppID FROM [OpportunityMaster] OM WHERE OM.numDomainId=' + CONVERT(VARCHAR(15), @numDomainID) + ' AND OM.numDivisionID=DM.numDivisionID AND OM.tintOppType = 1 AND OM.tintOppStatus = 1)))                                    
							AND DM.tintCRMType= '+ convert(varchar(2),@CRMType)+'                                            
							AND DM.numDomainID= ' + convert(varchar(15),@numDomainID)

	SET @strSql=@strSql + ' AND DM.bitActiveInActive=' + CONVERT(VARCHAR(15), @bitActiveInActive)
	
	IF LEN(@SearchQuery) > 0
	BEGIN
		SET @StrSql = @StrSql + ' AND (' + @SearchQuery + ') '
	END	

	 IF @bitPartner = 1 
	BEGIN
        SET @strSql = @strSql + 'and (DM.numAssignedTo=' + CONVERT(VARCHAR(15), @numUserCntID) + ' or DM.numCreatedBy=' + CONVERT(VARCHAR(15), @numUserCntID) + ') '                                                                 
    END
	                                                         
    IF @SortChar <> '0' 
	BEGIN
        SET @strSql = @strSql + ' And CMP.vcCompanyName like ''' + @SortChar + '%'''
		                                                               
	END

    IF @tintUserRightType = 1 
	BEGIN
        SET @strSql = @strSql + CONCAT(' AND (DM.numRecOwner = ',@numUserCntID,' OR DM.numAssignedTo=',@numUserCntID,' OR ',@strShareRedordWith,' OR ',@strExternalUser,')')                                                                     
	END
    ELSE IF @tintUserRightType = 2 
	BEGIN
        SET @strSql = @strSql + CONCAT(' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ',@numUserCntID,' ) or DM.numAssignedTo=',@numUserCntID,' or ',@strShareRedordWith,')')
    END
	               
    IF @numProfile <> 0 
	BEGIN
		SET @strSQl = @strSql + ' and cmp.vcProfile = ' + CONVERT(VARCHAR(15), @numProfile)                                                    
    END
	                                                      
    IF @tintSortOrder = 1 
        SET @strSql = @strSql + ' AND DM.numStatusID=2 '                                                              
    ELSE IF @tintSortOrder = 2 
        SET @strSql = @strSql + ' AND DM.numStatusID=3 '                          
    ELSE IF @tintSortOrder = 3 
        SET @strSql = @strSql + ' AND (DM.numRecOwner = ' + CONVERT(VARCHAR(15), @numUserCntID) + ' or DM.numAssignedTo=' + CONVERT(VARCHAR(15), @numUserCntID) + ' or ' + @strShareRedordWith +' OR ' + @strExternalUser + ')'                                                                                  
    ELSE IF @tintSortOrder = 5 
        SET @strSql = @strSql + ' order by numCompanyRating desc '                                                                     
    ELSE IF @tintSortOrder = 6 
        SET @strSql = @strSql + ' AND DM.bintCreatedDate > ''' + CONVERT(VARCHAR(20), DATEADD(day, -7,GETUTCDATE())) + ''''                                                                    
    ELSE IF @tintSortOrder = 7 
        SET @strSql = @strSql + ' and DM.numCreatedby=' + CONVERT(VARCHAR(15), @numUserCntID)                     
    ELSE IF @tintSortOrder = 8 
        SET @strSql = @strSql + ' and DM.numModifiedby=' + CONVERT(VARCHAR(15), @numUserCntID)                  
                                                   
        
	IF @vcRegularSearchCriteria<>'' 
	BEGIN
		IF PATINDEX('%cmp.vcPerformance%', @vcRegularSearchCriteria)>0
		BEGIN
			-- WE ARE MANAGING CONDITION IN OUTER APPLY
			set @strSql=@strSql
		END
		ELSE
			set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
	END


	IF @vcCustomSearchCriteria <> '' 
	BEGIN
		SET @strSql=@strSql +' AND ' + @vcCustomSearchCriteria 
    END  

	DECLARE @firstRec AS INTEGER                                                            
	DECLARE @lastRec AS INTEGER                                                            
	SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                                            
	SET @lastRec = ( @CurrentPage * @PageSize + 1 )                  

	-- EXECUTE FINAL QUERY
	DECLARE @strFinal AS NVARCHAR(MAX)
	SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, ',@strColumns,' INTO #tempTable',@StrSql,'; SELECT @TotalRecords = COUNT(*) FROM #tempTable; SELECT * FROM #tempTable WHERE ID > ',@firstRec,' and ID <',@lastRec,'; DROP TABLE #tempTable;')
	
	PRINT CAST(@strFinal AS NTEXT)
	exec sp_executesql @strFinal, N'@TotalRecords INT OUT', @TotRecs OUT

	SELECT * FROM #tempForm
	DROP TABLE #tempForm
	DROP TABLE #tempColorScheme
END
/****** Object:  StoredProcedure [dbo].[USP_AdvancedSearch]    Script Date: 05/07/2009 22:04:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_advancedsearch')
DROP PROCEDURE usp_advancedsearch
GO
CREATE PROCEDURE [dbo].[USP_AdvancedSearch] 
@WhereCondition as varchar(4000)='',                            
@AreasofInt as varchar(100)='',                              
@tintUserRightType as tinyint,                              
@numDomainID as numeric(9)=0,                              
@numUserCntID as numeric(9)=0,                                                        
@CurrentPage int,                                                                    
@PageSize int,                                                                    
@TotRecs int output,                                                                                                             
@columnSortOrder as Varchar(10),                              
@ColumnSearch as varchar(100)='',                           
@ColumnName as varchar(20)='',                             
@GetAll as bit,                              
@SortCharacter as char(1),                        
@SortColumnName as varchar(100)='',        
@bitMassUpdate as bit,        
@LookTable as varchar(200)='' ,        
@strMassUpdate as varchar(MAX)='',
@vcRegularSearchCriteria varchar(MAX)='',
@vcCustomSearchCriteria varchar(MAX)='',
@ClientTimeZoneOffset as int,
@vcDisplayColumns VARCHAR(MAX)
as                              
BEGIN
	DECLARE @tintOrder  AS TINYINT
	DECLARE @vcFormFieldName  AS VARCHAR(50)
	DECLARE @vcListItemType  AS VARCHAR(3)
	DECLARE @vcListItemType1  AS VARCHAR(3)
	DECLARE @vcAssociatedControlType VARCHAR(20)
	DECLARE @numListID  AS NUMERIC(9)
	DECLARE @vcDbColumnName VARCHAR(50)
	DECLARE @vcLookBackTableName VARCHAR(50)
	DECLARE @WCondition VARCHAR(1000)
	DECLARE @bitContactAddressJoinAdded AS BIT;
	DECLARE @bitBillAddressJoinAdded AS BIT;
	DECLARE @bitShipAddressJoinAdded AS BIT;
	DECLARE @vcOrigDbColumnName as varchar(50)
	DECLARE @bitAllowSorting AS CHAR(1)
	DECLARE @bitAllowEdit AS CHAR(1)
	DECLARE @bitCustomField AS BIT;
	DECLARE @ListRelID AS NUMERIC(9)
	DECLARE @intColumnWidth INT
	DECLARE @bitAllowFiltering AS BIT;
	DECLARE @vcfieldatatype CHAR(1)
	DECLARE @zipCodeSearch AS VARCHAR(100) = ''


	IF CHARINDEX('$SZ$', @WhereCondition)>0
	BEGIN
		SELECT @zipCodeSearch=SUBSTRING(@WhereCondition, CHARINDEX('$SZ$', @WhereCondition)+4,CHARINDEX('$EZ$', @WhereCondition) - CHARINDEX('$SZ$', @WhereCondition)-4) 

		CREATE TABLE #tempAddress
		(
			[numAddressID] [numeric](18, 0) NOT NULL,
			[vcAddressName] [varchar](50) NULL,
			[vcStreet] [varchar](100) NULL,
			[vcCity] [varchar](50) NULL,
			[vcPostalCode] [varchar](15) NULL,
			[numState] [numeric](18, 0) NULL,
			[numCountry] [numeric](18, 0) NULL,
			[bitIsPrimary] [bit] NOT NULL CONSTRAINT [DF_AddressMaster_bitIsPrimary]  DEFAULT ((0)),
			[tintAddressOf] [tinyint] NOT NULL,
			[tintAddressType] [tinyint] NOT NULL CONSTRAINT [DF_AddressMaster_tintAddressType]  DEFAULT ((0)),
			[numRecordID] [numeric](18, 0) NOT NULL,
			[numDomainID] [numeric](18, 0) NOT NULL
		)

		IF Len(@zipCodeSearch)>0
		BEGIN
			Declare @Strtemp as nvarchar(500);
			SET @Strtemp='insert into #tempAddress select * from AddressDetails  where numDomainID=@numDomainID1  
							and vcPostalCode in (SELECT REPLICATE(''0'', 5 - LEN(ZipCode)) + ZipCode  FROM ' + @zipCodeSearch +')';
	
			EXEC sp_executesql @Strtemp,N'@numDomainID1 numeric(18)',@numDomainID1=@numDomainID

			set @WhereCondition=replace(@WhereCondition,'$SZ$' + @zipCodeSearch + '$EZ$',
			' (ADC.numContactID in (select  numRecordID from #tempAddress where tintAddressOf=1 AND tintAddressType=0)   
			or (DM.numDivisionID in (select  numRecordID from #tempAddress where tintAddressOf=2 AND tintAddressType in(1,2))))')
		END
	END

	SET @WCondition = ''
   
	IF (@SortCharacter <> '0' AND @SortCharacter <> '')
	  SET @ColumnSearch = @SortCharacter

	CREATE TABLE #temptable 
	(
		id INT IDENTITY PRIMARY KEY,
		numcontactid VARCHAR(15)
	)


---------------------Find Duplicate Code Start-----------------------------
	DECLARE  @Sql           VARCHAR(8000),
			 @numMaxFieldId NUMERIC(9),
			 @Where         NVARCHAR(1000),
			 @OrderBy       NVARCHAR(1000),
			 @GroupBy       NVARCHAR(1000),
			 @SelctFields   NVARCHAR(4000),
			 @InneJoinOn    NVARCHAR(1000)
	SET @InneJoinOn = ''
	SET @OrderBy = ''
	SET @Sql = ''
	SET @Where = ''
	SET @GroupBy = ''
	SET @SelctFields = ''
    
	IF (@WhereCondition = 'DUP' OR @WhereCondition = 'DUP-PRIMARY')
	BEGIN
		SELECT @numMaxFieldId= MAX([numFieldID]) FROM View_DynamicColumns
		WHERE [numFormID] =24 AND [numDomainId] = @numDomainID
		--PRINT @numMaxFieldId
		WHILE @numMaxFieldId > 0
		BEGIN
			SELECT @vcFormFieldName=[vcFieldName],@vcDbColumnName=[vcDbColumnName],@vcLookBackTableName=[vcLookBackTableName] 
			FROM View_DynamicColumns 
			WHERE [numFieldID] = @numMaxFieldId and [numFormID] =24 AND [numDomainId] = @numDomainID
			--	PRINT @vcDbColumnName
				SET @SelctFields = @SelctFields +  @vcDbColumnName + ','
				IF(@vcLookBackTableName ='AdditionalContactsInformation')
				BEGIN
					SET @InneJoinOn = @InneJoinOn + 'ADC.'+ @vcDbColumnName +' = a1.' + @vcDbColumnName + ' and '
					SET @GroupBy = @GroupBy + 'ADC.'+  @vcDbColumnName + ','
				END
				ELSE IF (@vcLookBackTableName ='DivisionMaster')
				BEGIN
					SET @InneJoinOn = @InneJoinOn + 'DM.'+ @vcDbColumnName +' = a1.' + @vcDbColumnName + ' and '
					SET @GroupBy = @GroupBy + 'DM.'+  @vcDbColumnName + ','
				END
				ELSE
				BEGIN
					SET @InneJoinOn = @InneJoinOn + 'C.' + @vcDbColumnName +' = a1.' + @vcDbColumnName + ' and '
					SET @GroupBy = @GroupBy + 'C.' + @vcDbColumnName + ','
				END
				SET @Where = @Where + 'and LEN(' + @vcDbColumnName + ')>0 '

	
			SELECT @numMaxFieldId = MAX([numFieldID]) FROM View_DynamicColumns 
			WHERE [numFormID] =24 AND [numDomainId] = @numDomainID AND [numFieldID] < @numMaxFieldId
		END
			
				IF(LEN(@Where)>2)
				BEGIN
					SET @Where =RIGHT(@Where,LEN(@Where)-3)
					SET @OrderBy =  LEFT(@GroupBy,(Len(@GroupBy)- 1))
					SET @GroupBy =  LEFT(@GroupBy,(Len(@GroupBy)- 1))
					SET @InneJoinOn = LEFT(@InneJoinOn,LEN(@InneJoinOn)-3)
					SET @SelctFields = LEFT(@SelctFields,(Len(@SelctFields)- 1))
					SET @Sql = @Sql + ' INSERT INTO [#tempTable] ([numContactID])SELECT  ADC.numContactId FROM additionalcontactsinformation ADC JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId  
						JOIN (SELECT   '+ @SelctFields + '
					   FROM  additionalcontactsinformation ADC JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId  
					   WHERE  ' + @Where + ' AND C.[numDomainID] = '+convert(varchar(15),@numDomainID)+ ' AND ADC.[numDomainID] = '+convert(varchar(15),@numDomainID)+ ' AND DM.[numDomainID] = '+convert(varchar(15),@numDomainID)+ 
					   ' GROUP BY ' + @GroupBy + '
					   HAVING   COUNT(* ) > 1) a1
						ON  '+ @InneJoinOn + ' where ADC.[numDomainID] = '+convert(varchar(15),@numDomainID) + ' and DM.[numDomainID] = '+convert(varchar(15),@numDomainID) + ' ORDER BY '+ @OrderBy
				END	
				ELSE
				BEGIN
					SET @Where = ' 1=0 '
					SET @Sql = @Sql + ' INSERT INTO [#tempTable] ([numContactID])SELECT  ADC.numContactId FROM additionalcontactsinformation ADC  '
								+ ' Where ' + @Where
				END 	
			   PRINT @Sql
			   EXEC( @Sql)
			   SET @Sql = ''
			   SET @GroupBy = ''
			   SET @SelctFields = ''
			   SET @Where = ''
	
		IF (@WhereCondition = 'DUP-PRIMARY')
		  BEGIN
			SET @WCondition = ' AND ADC.numContactId IN (SELECT DISTINCT numContactId FROM #tempTable) and ISNULL(ADC.bitPrimaryContact,0)=1'
		  END
		ELSE
		  BEGIN
			SET @WCondition = ' AND ADC.numContactId IN (SELECT DISTINCT numContactId FROM #tempTable)'
		  END
    
		SET @Sql = ''
		SET @WhereCondition =''
		SET @Where= 'DUP' 
	END
-----------------------------End Find Duplicate ------------------------

	declare @strSql as varchar(8000)                                                              
	 set  @strSql='Select ADC.numContactId                    
	  FROM AdditionalContactsInformation ADC                                                   
	  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                  
	  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId'   

	IF @SortColumnName='vcStreet' OR @SortColumnName='vcCity' OR @SortColumnName='vcPostalCode' OR @SortColumnName ='numCountry' OR @SortColumnName ='numState'
	   OR @ColumnName='vcStreet' OR @ColumnName='vcCity' OR @ColumnName='vcPostalCode' OR @ColumnName ='numCountry' OR @ColumnName ='numState'	
	BEGIN 
		set @strSql= @strSql +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
	END 
	ELSE IF @SortColumnName='vcBillStreet' OR @SortColumnName='vcBillCity' OR @SortColumnName='vcBillPostCode' OR @SortColumnName ='numBillCountry' OR @SortColumnName ='numBillState'
		 OR @ColumnName='vcBillStreet' OR @ColumnName='vcBillCity' OR @ColumnName='vcBillPostCode' OR @ColumnName ='numBillCountry' OR @ColumnName ='numBillState'
	BEGIN 	
		set @strSql= @strSql +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
	END	
	ELSE IF @SortColumnName='vcShipStreet' OR @SortColumnName='vcShipCity' OR @SortColumnName='vcShipPostCode' OR @SortColumnName ='numShipCountry' OR @SortColumnName ='numShipState'
		 OR @ColumnName='vcShipStreet' OR @ColumnName='vcShipCity' OR @ColumnName='vcShipPostCode' OR @ColumnName ='numShipCountry' OR @ColumnName ='numShipState'
	BEGIN
		set @strSql= @strSql +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
	END	
                         
	if (@ColumnName<>'' and  @ColumnSearch<>'')                        
	begin                          
		SELECT @vcListItemType=vcListItemType,@numListID=numListID from View_DynamicDefaultColumns where vcDbColumnName=@ColumnName and numFormID=1 and numDomainId=@numDomainId                         
    
		if @vcListItemType='LI'                               
		begin                      
			IF @numListID=40--Country
			BEGIN
				IF @ColumnName ='numCountry'
				BEGIN
						set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD.numCountry' 
				END
				ELSE IF @ColumnName ='numBillCountry'
				BEGIN
						set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD1.numCountry' 
				END
				ELSE IF @ColumnName ='numShipCountry'
				BEGIN
						set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD2.numCountry' 
				END
			END                        
			ELSE
			BEGIN
				set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID='+@ColumnName                              
			END   
		end                              
		else if @vcListItemType='S'                               
		begin           
		   IF @ColumnName ='numState'
			BEGIN
   				set @strSql= @strSql +' left join State S1 on S1.numStateID=AD.numState'                            
			END
			ELSE IF @ColumnName ='numBillState'
			BEGIN
   				set @strSql= @strSql +' left join State S1 on S1.numStateID=AD1.numState'                            
			END
			ELSE IF @ColumnName ='numShipState'
			BEGIN
   				set @strSql= @strSql +' left join State S1 on S1.numStateID=AD2.numState'                           
			END
		end                              
		else if @vcListItemType='T'                               
		begin                              
		  set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID='+@ColumnName                             
		end                           
	end       
                      
	if (@SortColumnName<>'')                        
	begin                          
		select @vcListItemType1=vcListItemType,@numListID=numListID from View_DynamicDefaultColumns where vcDbColumnName=@SortColumnName and numFormID=1 and numDomainId=@numDomainId                         
    
		if @vcListItemType1='LI'                   
		begin     
			IF @numListID=40--Country
			BEGIN
				IF @SortColumnName ='numCountry'
				BEGIN
						set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD.numCountry' 
				END
				ELSE IF @SortColumnName ='numBillCountry'
				BEGIN
						set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD1.numCountry' 
				END
				ELSE IF @SortColumnName ='numShipCountry'
				BEGIN
						set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD2.numCountry' 
				END
			  END                        
			ELSE
			BEGIN
				set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                              
			END
		END                              
		else if @vcListItemType1='S'           
		begin                
			IF @SortColumnName ='numState'
			BEGIN
   				set @strSql= @strSql +' left join State S2 on S2.numStateID=AD.numState'                            
			END
			ELSE IF @SortColumnName ='numBillState'
			BEGIN
   				set @strSql= @strSql +' left join State S2 on S2.numStateID=AD1.numState'                            
			END
			ELSE IF @SortColumnName ='numShipState'
			BEGIN
   				set @strSql= @strSql +' left join State S2 on S2.numStateID=AD2.numState'                           
			END
		end                              
		else if @vcListItemType1='T'                  
		begin                              
			set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                               
		end                           
	end                          
  
	set @strSql=@strSql+' where DM.numDomainID  = '+convert(varchar(15),@numDomainID)+   @WhereCondition                                  
                 
--if @tintUserRightType=1 set @strSql=@strSql + ' AND (ADC.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ')'                                       
--else if @tintUserRightType=2 set @strSql=@strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or DM.numTerID=0) '
                        
IF @vcRegularSearchCriteria<>'' set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
IF @vcCustomSearchCriteria<>'' set @strSql=@strSql+' and ' + @vcCustomSearchCriteria                          
                                            
if    @AreasofInt<>'' set  @strSql=@strSql+ ' and ADC.numContactID in (select numContactID from  AOIContactLink where numAOIID in('+ @AreasofInt+'))'
                             
if (@ColumnName<>'' and  @ColumnSearch<>'')                         
begin                          
  if @vcListItemType='LI'                               
    begin                                
      set @strSql= @strSql +' and L1.vcData like '''+@ColumnSearch +'%'''                             
    end                              
    else if @vcListItemType='S'                               
    begin                                  
      set @strSql= @strSql +' and S1.vcState like '''+@ColumnSearch+'%'''                          
    end                              
    else if @vcListItemType='T'                               
    begin                              
      set @strSql= @strSql +' and T1.vcTerName like '''+@ColumnSearch  +'%'''                            
    end   
    else  
       BEGIN     
		   IF @ColumnName='vcStreet' OR @ColumnName='vcBillStreet' OR @ColumnName='vcShipStreet'
				set @strSql= @strSql +' and vcStreet like '''+@ColumnSearch  +'%'''                            
		   ELSE IF @ColumnName='vcCity' OR @ColumnName='vcBillCity' OR @ColumnName='vcShipCity'
				set @strSql= @strSql +' and vcCity like '''+@ColumnSearch  +'%'''                            
		   ELSE IF @ColumnName='vcPostalCode' OR @ColumnName='vcBillPostCode' OR @ColumnName='vcShipPostCode'
				set @strSql= @strSql +' and vcPostalCode like '''+@ColumnSearch  +'%'''                            
		   else		                      
				set @strSql= @strSql +' and '+@ColumnName+' like '''+@ColumnSearch  +'%'''                            
        END   
                          
end                          
   
	if (@SortColumnName<>'')                        
	begin                           
		if @vcListItemType1='LI'                               
		begin                                
			set @strSql= @strSql +' order by L2.vcData '+@columnSortOrder                              
		end                              
		else if @vcListItemType1='S'                               
		begin                                  
			set @strSql= @strSql +' order by S2.vcState '+@columnSortOrder                              
		end                              
		else if @vcListItemType1='T'                               
		begin                              
			set @strSql= @strSql +' order by T2.vcTerName '+@columnSortOrder                              
		end  
		else  
       BEGIN     
		   IF @SortColumnName='vcStreet' OR @SortColumnName='vcBillStreet' OR @SortColumnName='vcShipStreet'
				set @strSql= @strSql +' order by vcStreet ' +@columnSortOrder                            
		   ELSE IF @SortColumnName='vcCity' OR @SortColumnName='vcBillCity' OR @SortColumnName='vcShipCity'
				set @strSql= @strSql +' order by vcCity ' +@columnSortOrder                            
		   ELSE IF @SortColumnName='vcPostalCode' OR @SortColumnName='vcBillPostCode' OR @SortColumnName='vcShipPostCode'
				set @strSql= @strSql +' order by vcPostalCode ' +@columnSortOrder
		   else if @SortColumnName='numRecOwner'
				set @strSql= @strSql + ' order by dbo.fn_GetContactName(ADC.numRecOwner) ' +@columnSortOrder
			ELSE IF @SortColumnName='Opp.vcLastSalesOrderDate'
				SET @strSql= @strSql + ' order by (' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=DM.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ') ' +@columnSortOrder	                            
		   else		                      
				set @strSql= @strSql +' order by '+ @SortColumnName +' ' +@columnSortOrder                            
        END                        
	END
	ELSE 
		set @strSql= @strSql +' order by  ADC.numContactID asc '
IF(@Where <>'DUP')
BEGIN
    PRINT @strSql

	INSERT INTO #temptable (numcontactid)
	EXEC( @strSql)
	
	SET @strSql = ''
END

SET @bitBillAddressJoinAdded=0;
SET @bitShipAddressJoinAdded=0;
SET @bitContactAddressJoinAdded=0;
set @tintOrder=0;
set @WhereCondition ='';
set @strSql='select ADC.numContactId,DM.numDivisionID,DM.numTerID,ADC.numRecOwner,DM.tintCRMType '                              
DECLARE @Prefix AS VARCHAR(5)
DECLARE @bitCustom BIT
DECLARE @numFieldGroupID AS INT
DECLARE @vcColumnName AS VARCHAR(200)
DECLARE @numFieldID AS NUMERIC(18,0)

	DECLARE @TEMPSelectedColumns TABLE
	(
		numFieldID NUMERIC(18,0)
		,bitCustomField BIT
		,tintOrder INT
		,intColumnWidth FLOAT
	)
	DECLARE @vcLocationID VARCHAR(100) = '0'
	SELECT @vcLocationID =isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=1    

	-- IF USER HAS SELECTED ITS OWN DISPLAY COLUMNS THEN USE IT
	IF LEN(ISNULL(@vcDisplayColumns,'')) > 0
	BEGIN
		DECLARE @TempIDs TABLE
		(
			ID INT IDENTITY(1,1),
			vcFieldID VARCHAR(300)
		)

		INSERT INTO @TempIDs
		(
			vcFieldID
		)
		SELECT 
			OutParam 
		FROM 
			SplitString(@vcDisplayColumns,',')

		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT
			numFormFieldID
			,bitCustomField
			,(SELECT (ID-1) FROM @TempIDs T3 WHERE T3.vcFieldID = TEMP.vcFieldID)
			,0
		FROM
		(
			SELECT  
				numFieldID as numFormFieldID,
				0 bitCustomField,
				CONCAT(numFieldID,'~0') vcFieldID
			FROM    
				View_DynamicDefaultColumns
			WHERE   
				numFormID = 1
				AND bitInResults = 1
				AND bitDeleted = 0 
				AND numDomainID = @numDomainID
			UNION 
			SELECT  
				c.Fld_id AS numFormFieldID
				,1 bitCustomField
				,CONCAT(Fld_id,'~1') vcFieldID
			FROM    
				CFW_Fld_Master C 
			LEFT JOIN 
				CFW_Validation V ON V.numFieldID = C.Fld_id
			JOIN 
				CFW_Loc_Master L on C.GRP_ID=L.Loc_Id
			WHERE   
				C.numDomainID = @numDomainID
				AND GRP_ID IN (SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
		) TEMP
		WHERE
			vcFieldID IN (SELECT vcFieldID FROM @TempIDs)
	END

	-- IF USER SELECTED DISPLAY COLUMNS IS NOT AVAILABLE THAN GET COLUMNS CONFIGURED FROM SEARCH RESULT GRID
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT
			numFormFieldID
			,bitCustomField
			,tintOrder
			,intColumnWidth
		FROM
		(
			SELECT  
				A.numFormFieldID,
				0 bitCustomField,
				A.tintOrder,
				A.intColumnWidth
			FROM    AdvSerViewConf A
					JOIN View_DynamicDefaultColumns D ON D.numFieldID = A.numFormFieldID
			WHERE   A.numFormID = 1
					AND D.bitInResults = 1
					AND D.bitDeleted = 0
					AND D.numDomainID = @numDomainID 
					AND D.numFormID = 1
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 0
			UNION
			SELECT  
				A.numFormFieldID,
				1 bitCustomField,
				A.tintOrder,
				A.intColumnWidth
			FROM    AdvSerViewConf A
					JOIN CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
			WHERE   A.numFormID = 1
					AND C.numDomainID = @numDomainID 
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 1
		) T1
		ORDER BY
			 tintOrder
	END

	-- IF USER HASN'T CONFIGURED COLUMNS FROM SEARCH RESULT GRID THEN RETURN DEFAULT COLUMNS
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT  
			numFieldID,
			0,
			1,
			0
		FROM    
			View_DynamicDefaultColumns
		WHERE   
			numFormID = 1
			AND numDomainID = @numDomainID
			AND numFieldID = 3
	END

	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@vcLookBackTableName=vcLookBackTableName,
		@bitCustom=bitCustom,
		@vcOrigDbColumnName=vcOrigDbColumnName,
		@bitAllowSorting=bitAllowSorting,
		@bitAllowEdit= bitAllowEdit,
		@ListRelID=ListRelID,
        @intColumnWidth=intColumnWidth,
		@bitAllowFiltering=bitAllowFiltering,
		@vcfieldatatype=vcFieldDataType,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			D.numFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			vcOrigDbColumnName,
			bitAllowSorting,
			bitAllowEdit,			
			ListRelID,			
			T1.intColumnWidth as intColumnWidth,
			bitAllowFiltering,
			vcFieldDataType,			
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			D.numFieldID=T1.numFieldID                                                     
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 1 
			AND ISNULL(T1.bitCustomField,0) = 0
			AND d.vcDbColumnName not in ('numBillShipCountry','numBillShipState')
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			Fld_id AS numFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',			
			1 AS bitCustom,
			'',
			null,
			null,
			null,
			null,
			null,
			'',
			Grp_id
		FROM    
			CFW_Fld_Master C 
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			C.Fld_id=T1.numFieldID 
		WHERE   
			C.numDomainID = @numDomainID 
			AND ISNULL(T1.bitCustomField,0) = 1
	) T1
	ORDER BY 
		tintOrder asc                              

while @tintOrder>0                              
begin                              
     
	
    IF @vcLookBackTableName = 'AdditionalContactsInformation' 
        SET @Prefix = 'ADC.'
    IF @vcLookBackTableName = 'DivisionMaster' 
        SET @Prefix = 'DM.'
     
	IF @bitCustom = 0 
	BEGIN                           
		SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

		if @vcAssociatedControlType='SelectBox'                              
		begin        
		
		print @vcDbColumnName
					IF @vcDbColumnName='numDefaultShippingServiceID'
				BEGIN
					SET @strSql=@strSql+ ' ,ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=DM.numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = DM.numDefaultShippingServiceID),'''')' + ' [' + @vcColumnName + ']' 
				END                      
                                              
			 else if @vcListItemType='LI'                               
			  begin    
				  IF @numListID=40--Country
				  BEGIN
	  				set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                              
					IF @vcDbColumnName ='numCountry'
					BEGIN
						IF @bitContactAddressJoinAdded=0 
					  BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
							SET @bitContactAddressJoinAdded=1;
					  END
					  set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD.numCountry'	
					END
					ELSE IF @vcDbColumnName ='numBillCountry'
					BEGIN
						IF @bitBillAddressJoinAdded=0 
						BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
					
							SET @bitBillAddressJoinAdded=1;
						END
						set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD1.numCountry'
					END
					ELSE IF @vcDbColumnName ='numShipCountry'
					BEGIN
						IF @bitShipAddressJoinAdded=0 
						BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
					
							SET @bitShipAddressJoinAdded=1;
						END
						set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD2.numCountry'
					END
				  END                        
				  ELSE
				  BEGIN
						set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                              
						set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName	
				  END
			  end                              
	                      
			  else if @vcListItemType='T'                               
			  begin                              
				  set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                              
			   set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                              
			  end  
			  else if   @vcListItemType='U'                                                     
				begin           
					set @strSql=@strSql+',dbo.fn_GetContactName('+ @Prefix + @vcDbColumnName+') ['+ @vcColumnName +']'
				end   
	
		end  
		ELSE IF @vcAssociatedControlType='ListBox'   
		BEGIN
			if @vcListItemType='S'                               
			  begin                         
				set @strSql=@strSql+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName +']'
				IF @vcDbColumnName ='numState'
				BEGIN
					IF @bitContactAddressJoinAdded=0 
					   BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
							SET @bitContactAddressJoinAdded=1;
					  END	
					  set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD.numState'
				END
				ELSE IF @vcDbColumnName ='numBillState'
				BEGIN
					IF @bitBillAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
						SET @bitBillAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD1.numState'
				END
				ELSE IF @vcDbColumnName ='numShipState'
				BEGIN
					IF @bitShipAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
						SET @bitShipAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD2.numState'
				END
			  end         
		END 
		ELSE IF @vcAssociatedControlType='TextBox'   
		BEGIN
			IF @vcDbColumnName='vcStreet' OR @vcDbColumnName='vcCity' OR @vcDbColumnName='vcPostalCode'
			begin                              
			   set @strSql=@strSql+',AD.'+ @vcDbColumnName+' ['+ @vcColumnName +']'                              
			   IF @bitContactAddressJoinAdded=0 
			   BEGIN
					set @WhereCondition= @WhereCondition +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
					SET @bitContactAddressJoinAdded=1;
			   END
			end 
			ELSE IF @vcDbColumnName='vcBillStreet' OR @vcDbColumnName='vcBillCity' OR @vcDbColumnName='vcBillPostCode'
			begin                              
			   set @strSql=@strSql+',AD1.'+ REPLACE(REPLACE(@vcDbColumnName,'Bill',''),'PostCode','PostalCode') +' ['+ @vcColumnName +']'                              
			   IF @bitBillAddressJoinAdded=0 
			   BEGIN 
	   				set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
	   				SET @bitBillAddressJoinAdded=1;
			   END
			end 
			ELSE IF @vcDbColumnName='vcShipStreet' OR @vcDbColumnName='vcShipCity' OR @vcDbColumnName='vcShipPostCode'
			begin                              
			   set @strSql=@strSql+',AD2.'+ REPLACE(REPLACE(@vcDbColumnName,'Ship',''),'PostCode','PostalCode') +' ['+ @vcColumnName +']'                              
			   IF @bitShipAddressJoinAdded=0 
			   BEGIN
	   				set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2  AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
	   				SET @bitShipAddressJoinAdded=1;
			   END
			end 
			ELSE
			BEGIN
				set @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' when @vcDbColumnName='bintCreatedDate' then 'dbo.[FormatedDateFromDate](DateAdd(minute, ' + CAST(-@ClientTimeZoneOffset AS VARCHAR) + ',DM.bintCreatedDate),DM.numDomainID)' else @vcDbColumnName end+' ['+ @vcColumnName +']'		
			END
		END
		ELSE IF @vcAssociatedControlType='DateField' and @vcDbColumnName='vcLastSalesOrderDate'
		BEGIN
			SET @WhereCondition = @WhereCondition
											+ ' OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=DM.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder '

			set @strSql=@strSql+','+ CONCAT('ISNULL(dbo.FormatedDateFromDate(DateAdd(minute, ',-@ClientTimeZoneOffset,',TempLastOrder.bintCreatedDate),',@numDomainID,'),'''')') +' ['+ @vcColumnName+']'
		END	                    
		ELSE 
			SET @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' when @vcDbColumnName='bintCreatedDate' then 'dbo.[FormatedDateFromDate](DM.bintCreatedDate,DM.numDomainID)' else @vcDbColumnName end+' ['+ @vcColumnName +']'
	END
	ELSE IF @bitCustom = 1                
	BEGIN
		--SET @vcColumnName= CONCAT(@vcFormFieldName,'~','Cust',@numFieldId)
		SET @vcColumnName= CONCAT('Cust',@numFieldId,'~',@numFieldId,'~',1)
			
		IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'            
		BEGIN
			SET @strSql= @strSql+',CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
				
			SET @WhereCondition= @WhereCondition 
								+' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '                                                         
		END   
		ELSE IF @vcAssociatedControlType = 'CheckBox'       
		BEGIN
			set @strSql= @strSql+',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0)='''' then 0 else  isnull(CFW'
											+ CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0) end ['+ @vcColumnName + ']'
 
			set @WhereCondition= @WhereCondition +' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)+ '             
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '                                                     
		END                
		ELSE IF @vcAssociatedControlType = 'DateField'            
		BEGIN
			set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
			set @WhereCondition= @WhereCondition +' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)+ '                 
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '                                                         
		END                
		ELSE IF @vcAssociatedControlType = 'SelectBox'             
		BEGIN
			set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
			set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
			set @WhereCondition= @WhereCondition +' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)+ '                 
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '                                                        
			set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
		END         
		ELSE IF @vcAssociatedControlType = 'CheckBoxList'
		BEGIN
			PRINT 'Custom1'

			SET @strSql= @strSql+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

			SET @WhereCondition= @WhereCondition 
								+' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '										
		
		END        
	END          
    
	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@vcLookBackTableName=vcLookBackTableName,
		@bitCustom=bitCustom,
		@vcOrigDbColumnName=vcOrigDbColumnName,
		@bitAllowSorting=bitAllowSorting,
		@bitAllowEdit= bitAllowEdit,
		@ListRelID=ListRelID,
        @intColumnWidth=intColumnWidth,
		@bitAllowFiltering=bitAllowFiltering,
		@vcfieldatatype=vcFieldDataType,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			D.numFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			vcOrigDbColumnName,
			bitAllowSorting,
			bitAllowEdit,			
			ListRelID,			
			T1.intColumnWidth as intColumnWidth,
			bitAllowFiltering,
			vcFieldDataType,			
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			D.numFieldID=T1.numFieldID                                                     
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 1 
			AND ISNULL(T1.bitCustomField,0) = 0
			AND d.vcDbColumnName not in ('numBillShipCountry','numBillShipState')
			AND T1.tintOrder > @tintOrder-1
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			Fld_id AS numFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',			
			1 AS bitCustom,
			'',
			null,
			null,
			null,
			null,
			null,
			'',
			Grp_id
		FROM    
			CFW_Fld_Master C 
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			C.Fld_id=T1.numFieldID 
		WHERE   
			C.numDomainID = @numDomainID 
			AND ISNULL(T1.bitCustomField,0) = 1
			AND T1.tintOrder > @tintOrder-1
	) T1
	ORDER BY 
		tintOrder asc                    
                                      
 if @@rowcount=0 set @tintOrder=0                              
end                              

                                                                  
  declare @firstRec as integer                                                                    
  declare @lastRec as integer                                                                    
 set @firstRec= (@CurrentPage-1) * @PageSize                      
 set @lastRec= (@CurrentPage*@PageSize+1)                                                                     
set @TotRecs=(select count(*) from #tempTable)                                                   
                                          
if @GetAll=0         
begin        
        
 if @bitMassUpdate=1            
 begin  

   Declare @strReplace as varchar(8000) = ''
    set @strReplace = case when @LookTable='DivisionMaster' then 'DM.numDivisionID' when @LookTable='AdditionalContactsInformation' then 'ADC.numContactId' when @LookTable='ContactAddress' then 'ADC.numContactId' when @LookTable='ShipAddress' then 'DM.numDivisionID' when @LookTable='BillAddress' then 'DM.numDivisionID' when @LookTable='CompanyInfo' then 'C.numCompanyId' end           
   +' FROM AdditionalContactsInformation ADC inner join DivisionMaster DM on ADC.numDivisionID = DM.numDivisionID                  
   JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                                
    '+@WhereCondition        
    set @strMassUpdate=replace(@strMassUpdate,'@$replace',@strReplace)      
    PRINT CONCAT('MassUPDATE:',@strMassUpdate)
  exec (@strMassUpdate)         
 end         
 
 
 IF (@Where = 'DUP')
 BEGIN
	set @strSql=@strSql+' FROM AdditionalContactsInformation ADC
	  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID
	  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId    
	  JOIN OpportunityMaster OM on OM.numDivisionID= DM.numDivisionID                  
	  inner join #tempTable T on T.numContactId=ADC.numContactId ' 
	  + @WhereCondition +'
	  WHERE ID > '+convert(varchar(10),@firstRec)+ ' and ID <'+ convert(varchar(10),@lastRec) +  @WCondition + ' order by T.ID, C.vcCompanyName' -- @OrderBy
	  PRINT @strSql
	  exec(@strSql)                                                                 
	 drop table #tempTable
	 RETURN	
 END
ELSE
BEGIN
	SET @strSql = @strSql + ' FROM AdditionalContactsInformation ADC
		JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID   
	  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                              
	   '+@WhereCondition+' inner join #tempTable T on T.numContactId=ADC.numContactId                                                               
	  WHERE ID > '+convert(varchar(10),@firstRec)+ ' and ID <'+ convert(varchar(10),@lastRec)
	  
	   
	SET @strSql=@strSql + ' order by T.ID, C.vcCompanyName ' 
END
             
end                           
 else        
 begin                      
     set @strSql=@strSql+' FROM AdditionalContactsInformation ADC                                                   
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                                   
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                              
  '+@WhereCondition+' join #tempTable T on T.numContactId=ADC.numContactId'        
  
	   SET @strSql=@strSql + ' order by T.ID, C.vcCompanyName'   
	end       
print @strSql                                                      
exec(@strSql) 
drop table #tempTable


	SELECT
		tintOrder,
		vcDbColumnName,
		vcFieldName,
		vcAssociatedControlType,
		vcListItemType,
		numListID,
		vcLookBackTableName,
		numFieldID,
		numFieldID AS numFormFieldID,
		intColumnWidth,
		bitCustom AS bitCustomField,
		vcOrigDbColumnName,
		bitAllowSorting,
		bitAllowEdit,
		ListRelID,
		bitAllowFiltering,
		vcFieldDataType
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			D.numFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			vcOrigDbColumnName,
			bitAllowSorting,
			bitAllowEdit,			
			ListRelID,			
			T1.intColumnWidth as intColumnWidth,
			bitAllowFiltering,
			vcFieldDataType,			
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			D.numFieldID=T1.numFieldID                                                     
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 1 
			AND ISNULL(T1.bitCustomField,0) = 0
			AND d.vcDbColumnName not in ('numBillShipCountry','numBillShipState')
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			Fld_id AS numFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',			
			1 AS bitCustom,
			'',
			null,
			null,
			null,
			null,
			null,
			'',
			Grp_id
		FROM    
			CFW_Fld_Master C 
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			C.Fld_id=T1.numFieldID 
		WHERE   
			C.numDomainID = @numDomainID 
			AND ISNULL(T1.bitCustomField,0) = 1
	) T1
	ORDER BY 
		tintOrder asc       

IF OBJECT_ID('tempdb..#tempAddress') IS NOT NULL
BEGIN
    DROP TABLE #tempAddress
END
END
--- Created By Anoop jayaraj       
--- Modified By Gangadhar 03/05/2008                                                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_companylist1')
DROP PROCEDURE usp_companylist1
GO
CREATE PROCEDURE [dbo].[USP_CompanyList1]                                                      
	@numRelationType as numeric(9),                                                   
	@numUserCntID numeric(9),                                                      
	@tintUserRightType tinyint,                                                          
	@SortChar char(1)='0',                                                     
	@FirstName varChar(100)= '',                                                      
	@LastName varChar(100)= '',                                                      
	@CustName varChar(100)= '',                                                    
	@CurrentPage int,                                                    
	@PageSize int,                                                    
	@columnName as Varchar(50),                                                    
	@columnSortOrder as Varchar(10),                                            
	@tintSortOrder numeric=0,                                             
	@numProfile as numeric(9)=0,                                    
	@numDomainID as numeric(9)=0,                            
	@tintCRMType as tinyint,                            
	@bitPartner as BIT,
	@numFormID AS NUMERIC(9),
	@vcRegularSearchCriteria varchar(MAX)='',
	@vcCustomSearchCriteria varchar(MAX)=''                                                     
as       
	IF CHARINDEX('AD.numBillCountry',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.numBillCountry','AD1.numCountry')
	END
	ELSE IF CHARINDEX('AD.numShipCountry',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.numShipCountry','AD2.numCountry')
	END
	ELSE IF CHARINDEX('AD.vcBillCity',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.vcBillCity','AD5.vcCity')
	END
	ELSE IF CHARINDEX('AD.vcShipCity',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.vcShipCity','AD6.vcCity')
	END
	IF CHARINDEX('ADC.vcLastFollowup',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'ADC.vcLastFollowup', '(SELECT dtExecutionDate FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND ISNULL(ADC.numECampaignID,0) > 0 AND numConECampDTLID IN (SELECT MAX(numConECampDTLID) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND ISNULL(ADC.numECampaignID,0) > 0 AND ISNULL(bitSend,0)=1))')
	END
	IF CHARINDEX('ADC.vcNextFollowup',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'ADC.vcNextFollowup','(SELECT dtExecutionDate FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND ISNULL(ADC.numECampaignID,0) > 0 AND numConECampDTLID IN (SELECT MIN(numConECampDTLID) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND ISNULL(ADC.numECampaignID,0) > 0 AND ISNULL(bitSend,0)=0))')
	END     

    declare @firstRec as integer                                                                
  declare @lastRec as integer                                                                
 set @firstRec= (@CurrentPage-1) * @PageSize                                                                
     set @lastRec= (@CurrentPage*@PageSize+1)                                                                          
  declare @column as varchar(50)                  
set @column = @columnName                  
declare @lookbckTable as varchar(50)                  
set @lookbckTable = ''                                                               
                                                    
  declare @strSql as varchar(MAX)                                                  
set @strSql='with tblCompany as (SELECT  '          
if @tintSortOrder=7 or @tintSortOrder=8  set @strSql=@strSql + ' '             
if @tintSortOrder=5  set @strSql=@strSql + 'ROW_NUMBER() OVER ( order by numCompanyRating desc) AS RowNumber,COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount, '           
else           
 set @strSql=@strSql +' ROW_NUMBER() OVER (ORDER BY '+@column+' '+ @columnSortOrder+') AS RowNumber,COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount, '                                                       
set @strSql=@strSql +'  DM.numDivisionID                                          
    FROM  CompanyInfo CMP                                 
    join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID 
	left join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID AND ISNULL(ADC.bitPrimaryContact,0)=1
	left join VIEW_OPEN_ACTION_ITEMS VOA WITH (NOEXPAND) ON VOA.numDomainID = DM.numDomainID AND  VOA.numDivisionId = DM.numDivisionID
	left join View_InboxEmail VIE  WITH (NOEXPAND) ON VIE.numDomainID = DM.numDomainID AND  VIE.numContactId = ADC.numContactId '                                            
                               
if @tintSortOrder= 9 set @strSql=@strSql+' join Favorites F on F.numContactid=DM.numDivisionID '                                            
if @bitPartner=1 set @strSql=@strSql + ' left join CompanyAssociations CA on CA.numAssociateFromDivisionID=DM.numDivisionID and bitShareportal=1 and bitDeleted=0                           
and CA.numDivisionID=(select numDivisionID from AdditionalContactsInformation where numContactID='+convert(varchar(15),@numUserCntID)+ ')'                          
set @strSql=@strSql + ' ##JOIN##                                                     
    left join ListDetails LD on LD.numListItemID=CMP.numCompanyRating                                                     
    left join ListDetails LD1 on LD1.numListItemID= DM.numFollowUpStatus 
left Join AddressDetails AD on AD.numRecordId= DM.numDivisionID and AD.tintAddressOf=2 and AD.tintAddressType=1 AND AD.bitIsPrimary=1 and AD.numDomainID= DM.numDomainID                                                   
  WHERE CMP.numDomainID=  DM.numDomainID                                                    
    AND DM.numDomainID = '+convert(varchar(15),@numDomainID)+ ''                                                    
   if @FirstName<>'' set    @strSql=@strSql+' and ADC.vcFirstName  like '''+@FirstName+'%'''                                                     
    if @LastName<>'' set    @strSql=@strSql+' and ADC.vcLastName like '''+@LastName+'%'''                                                    
    if @CustName<>'' set    @strSql=@strSql+' and CMP.vcCompanyName like '''+@CustName+'%'''                               
	IF @numRelationType <> '0' 
	BEGIN
		IF @numRelationType = 47
		BEGIN
			SET @strSql=@strSql + ' AND (CMP.numCompanyType = 47 OR (CMP.numCompanyType=46 AND EXISTS (SELECT numOppID FROM [OpportunityMaster] OM WHERE OM.numDomainId=' + CONVERT(VARCHAR(15), @numDomainID) + ' AND OM.numDivisionID=DM.numDivisionID AND OM.tintOppType = 2 AND OM.tintOppStatus = 1)))'   
		END
		ELSE
		BEGIN
			SET @strSql=@strSql + ' AND CMP.numCompanyType = '+convert(varchar(15),@numRelationType)+ ''   
		END
	END

                                                 
if @SortChar<>'0' set @strSql=@strSql + ' And CMP.vcCompanyName like '''+@SortChar+'%'''

	DECLARE @strExternalUser AS VARCHAR(MAX) = ''
	SET @strExternalUser = CONCAT(' (NOT EXISTS (SELECT numUserDetailID FROM UserMaster WHERE numDomainID=',@numDomainID,' AND numUserDetailID=',@numUserCntId,') AND EXISTS (SELECT EAD.numExtranetDtlID FROM ExtranetAccountsDtl EAD INNER JOIN ExtarnetAccounts EA ON EAD.numExtranetID=EA.numExtranetID WHERE EAD.numDomainID=',@numDomainID,' AND EAD.numContactID=',@numUserCntID,' AND (EA.numDivisionID=DM.numDivisionID OR EA.numDivisionID=DM.numPartenerSource)))')
                                           
	IF @tintUserRightType=1 
		SET @strSql = CONCAT(@strSql,' AND ((DM.numRecOwner = ',@numUserCntID,')',' OR ',@strExternalUser,(CASE WHEN @bitPartner=1 THEN ' OR ( CA.bitShareportal=1 and CA.bitDeleted=0))' ELSE ')' END))                                                 
else if @tintUserRightType=2 set @strSql=@strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ') '                     
if @tintSortOrder=1  set @strSql=@strSql + ' AND DM.numStatusID=2 '                                                                
else if @tintSortOrder=2  set @strSql=@strSql + ' AND DM.numStatusID=3 '                                                                          
else if @tintSortOrder=3  set @strSql=@strSql + ' AND (DM.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ' or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ')'                                                                       
--else if @tintSortOrder=5  set @strSql=@strSql + ' order by numCompanyRating desc '                                                                       
else if @tintSortOrder=6  set @strSql=@strSql + ' AND DM.bintCreatedDate > '''+convert(varchar(20),dateadd(day,-7,getutcdate()))+''''                                                                      
else if @tintSortOrder=7  set @strSql=@strSql + ' and DM.numCreatedby='+convert(varchar(15),@numUserCntID)                    
--+ ' ORDER BY DM.bintCreateddate desc '                                                              
else if @tintSortOrder=8  set @strSql=@strSql + ' and DM.numModifiedby='+convert(varchar(15),@numUserCntID)          
           
--if (@tintSortOrder=7 and @column!='DM.bintcreateddate')  set @strSql='select * from ('+@strSql+')X   '                                                     
--else if (@tintSortOrder=8 and @column!='DM.bintcreateddate')  set @strSql='select * from ('+@strSql+')X  '                                                               
--else 
if @tintSortOrder=9  set @strSql=@strSql + ' and F.numUserCntID='+convert(varchar(15),@numUserCntID)+ ' and cType=''O'''              
                                       
if @numProfile> 0 set @strSql=@strSql + ' and  vcProfile='+convert(varchar(15),@numProfile)                                            
if @tintCRMType>0 set @strSql=@strSql + ' and  DM.tintCRMType='+convert(varchar(1),@tintCRMType)                                              
                                                  
IF @vcRegularSearchCriteria<>'' set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
IF @vcCustomSearchCriteria<>'' set @strSql=@strSql+' AND ' + @vcCustomSearchCriteria
                                                  
                                                    
  set @strSql=@strSql + ')'                      
   
                                                                                 
declare @tintOrder as tinyint                                                        
declare @vcFieldName as varchar(50)                                                        
declare @vcListItemType as varchar(3)           
declare @numListID AS numeric(9)                                        
declare @vcAssociatedControlType varchar(20)                                                        
declare @vcDbColumnName varchar(200)                            
declare @WhereCondition varchar(MAX)                             
declare @vcLookBackTableName varchar(2000)                      
Declare @bitCustom as bit  
DECLARE @bitAllowEdit AS CHAR(1)             
Declare @numFieldId as numeric  
DECLARE @bitAllowSorting AS CHAR(1)            
declare @vcColumnName AS VARCHAR(500)                          
set @tintOrder=0                                                        
set @WhereCondition =''                       
                         
declare @Nocolumns as tinyint                 
declare @DefaultNocolumns as tinyint                      
set @Nocolumns=0  

select @Nocolumns=isnull(sum(TotalRow),0)  from(            
Select count(*) TotalRow from View_DynamicColumns where numFormId=@numFormID and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=@numRelationType 
Union 
Select count(*) TotalRow from View_DynamicCustomColumns where numFormId=@numFormID and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=@numRelationType) TotalRows

                    
  set @DefaultNocolumns=  @Nocolumns  
                       
while @DefaultNocolumns>0                      
begin                     
 set @DefaultNocolumns=@DefaultNocolumns-1                    
end              
   

   CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(200),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth INT,bitAllowFiltering BIT,vcFieldDataType CHAR(1))
    


set @strSql=@strSql+' select ADC.numContactId,DM.numDivisionID,DM.numTerID,ADC.numRecOwner,DM.tintCRMType,RowNumber,ADC.vcFirstName+'' ''+ADC.vcLastName AS vcContactName,ADC.numPhone AS numContactPhone,ADC.numPhoneExtension AS numContactPhoneExtension,ADC.vcEmail AS vcContactEmail'                       

--Check if Master Form Configuration is created by administrator if NoColumns=0
DECLARE @IsMasterConfAvailable BIT = 0
DECLARE @numUserGroup NUMERIC(18,0)

IF @Nocolumns=0
BEGIN
	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormID AND numRelCntType=@numRelationType AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END
END


--If MasterConfiguration is available then load it otherwise load default columns
IF @IsMasterConfAvailable = 1
BEGIN
	INSERT INTO DycFormConfigurationDetails 
	(
		numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
	)
	SELECT 
		@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numRelationType,1,0,intColumnWidth
	FROM 
		View_DynamicColumnsMasterConfig
	WHERE
		View_DynamicColumnsMasterConfig.numFormId=@numFormID AND 
		View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicColumnsMasterConfig.numRelCntType=@numRelationType AND 
		View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
		ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	UNION
    SELECT 
		@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numRelationType,1,1,intColumnWidth
	FROM 
		View_DynamicCustomColumnsMasterConfig
	WHERE 
		View_DynamicCustomColumnsMasterConfig.numFormId=@numFormID AND 
		View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicCustomColumnsMasterConfig.numRelCntType=@numRelationType AND 
		View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

	INSERT INTO #tempForm
	SELECT 
		(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
		vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
		bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
		ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
	FROM 
		View_DynamicColumnsMasterConfig
	WHERE
		View_DynamicColumnsMasterConfig.numFormId=@numFormID AND 
		View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicColumnsMasterConfig.numRelCntType=@numRelationType AND 
		View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
		ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	UNION
    SELECT 
		tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
		bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
	FROM 
		View_DynamicCustomColumnsMasterConfig
	WHERE 
		View_DynamicCustomColumnsMasterConfig.numFormId=@numFormID AND 
		View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicCustomColumnsMasterConfig.numRelCntType=@numRelationType AND 
		View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
	ORDER BY 
		tintOrder ASC  
END
ELSE                                        
BEGIN      

	if @Nocolumns=0
	BEGIN
	INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
	select @numFormID,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,@numRelationType,1,0,intColumnWidth
	 FROM View_DynamicDefaultColumns
	 where numFormId=@numFormID and bitDefault=1 AND ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID
	order by tintOrder asc   
	END          
    
    INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
 FROM View_DynamicColumns 
 where numFormId=@numFormID and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=@numRelationType
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
       
       UNION
    
   select tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
 from View_DynamicCustomColumns
 where numFormId=@numFormID and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=@numRelationType
 AND ISNULL(bitCustom,0)=1
 
  order by tintOrder asc  
   
 END                                                  
                        
--    set @DefaultNocolumns=  @Nocolumns  
Declare @ListRelID as numeric(9)     
      select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
@ListRelID=ListRelID
  from  #tempForm order by tintOrder asc            

                                                    
while @tintOrder>0                                                        
begin                                             
     print @vcFieldName                                               
    if @bitCustom = 0            
 begin                      
       
      
 declare @Prefix as varchar(5)                            
      if @vcLookBackTableName = 'AdditionalContactsInformation'                            
		set @Prefix = 'ADC.'                            
      else if @vcLookBackTableName = 'DivisionMaster'                            
		set @Prefix = 'DM.'   
		
			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

					
	if @vcDbColumnName = 'vcLastFollowup'
BEGIN
	set @strSql=@strSql+',  ISNULL((CASE WHEN ADC.numECampaignID > 0 THEN dbo.fn_FollowupDetailsInOrgList(ADC.numContactID,1,ADC.numDomainID) ELSE '''' END),'''') ['+ @vcColumnName+']' 
END
ELSE IF @vcDbColumnName = 'vcNextFollowup'
BEGIN
	set @strSql=@strSql+',  ISNULL((CASE WHEN ADC.numECampaignID > 0 THEN dbo.fn_FollowupDetailsInOrgList(ADC.numContactID,2,ADC.numDomainID) ELSE '''' END),'''') ['+ @vcColumnName+']' 
END
ELSE IF @vcAssociatedControlType='Label' and @vcDbColumnName='vcLastSalesOrderDate'
BEGIN
	SET @WhereCondition = @WhereCondition
									+ ' OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=DM.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder '

	set @strSql=@strSql+','+ CONCAT('ISNULL(dbo.FormatedDateFromDate(TempLastOrder.bintCreatedDate,',@numDomainID,'),'''')') +' ['+ @vcColumnName+']'
END

else if @vcAssociatedControlType='SelectBox'                                                              
        begin                                                              
    IF @vcDbColumnName='numDefaultShippingServiceID'
	BEGIN
		SET @strSql=@strSql+ ' ,ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=DM.numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = DM.numDefaultShippingServiceID),'''')' + ' [' + @vcColumnName + ']' 
	END                  
	Else IF @vcDbColumnName = 'numPartenerSource'
	BEGIN
		SET @strSql=@strSql+ ' ,(DM.numPartenerSource) '+' ['+ @vcColumnName+']'    
		SET @strSql=@strSql+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=DM.numPartenerSource) AS vcPartenerSource' 
	END                                        
    ELSE if @vcListItemType='LI'                                                               
     BEGIN
     	IF @numListID=40 
		 BEGIN
			set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'          
			
			IF @vcDbColumnName='numShipCountry'
			BEGIN
				SET @WhereCondition = @WhereCondition
									+ ' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= DM.numDomainID '
									+ ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=AD2.numCountry '
			END
			ELSE
			BEGIN
				SET @WhereCondition = @WhereCondition
								+ ' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= DM.numDomainID '
								+ ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=AD1.numCountry '
			END
		 END
		 ELSE IF @numListID=5
		BEGIN						
			set @strSql=@strSql+',(CASE WHEN CMP.numCompanyType=46 THEN CONCAT(L'+ convert(varchar(3),@tintOrder)+'.vcData,'' '',''<i class="fa fa-info-circle text-blue" aria-hidden="true" title="Organizations with a sales orders will be listed in the Account list, just as those with Purchase Orders will be listed in the Vendor list. This explains why you may see a Vendor in the Accounts list and an Account in the Vendor list" style="font-size: 20px;"></i>'') ELSE L'+ convert(varchar(3),@tintOrder)+'.vcData END)'+' ['+ @vcColumnName+']'                                                             
			set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName
		END
		 ELSE
		 BEGIN    
		  set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                             
		  set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                              
		 END
     end    
       else if @vcListItemType='C'                                                     
     begin                                                    
      set @strSql=@strSql+',C'+ convert(varchar(3),@tintOrder)+'.vcCampaignName'+' ['+ @vcColumnName+']'                                                    
      set @WhereCondition= @WhereCondition +' left Join CampaignMaster C'+ convert(varchar(3),@tintOrder)+ ' on C'+convert(varchar(3),@tintOrder)+ '.numCampaignId=DM.'+@vcDbColumnName                                                    
     end                                                            
     else if @vcListItemType='S'                                                               
     begin       
		IF @vcDbColumnName='numShipState'
		BEGIN
			SET @strSql = @strSql + ',ShipState.vcState' + ' [' + @vcColumnName + ']' 
			SET @WhereCondition = @WhereCondition
				+ ' left Join AddressDetails AD3 on AD3.numRecordId= DM.numDivisionID and AD3.tintAddressOf=2 and AD3.tintAddressType=2 AND AD3.bitIsPrimary=1 and AD3.numDomainID= DM.numDomainID '
				+ ' left Join State ShipState on ShipState.numStateID=AD3.numState '
		END
		ELSE IF @vcDbColumnName='numBillState'
		BEGIN
			SET @strSql = @strSql + ',BillState.vcState' + ' [' + @vcColumnName + ']' 
			SET @WhereCondition = @WhereCondition
				+ ' left Join AddressDetails AD4 on AD4.numRecordId= DM.numDivisionID and AD4.tintAddressOf=2 and AD4.tintAddressType=1 AND AD4.bitIsPrimary=1 and AD4.numDomainID= DM.numDomainID '
				+ ' left Join State BillState on BillState.numStateID=AD4.numState '
		END
                                                       
--      set @strSql=@strSql+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName+']'                                                              
--      set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID='+@vcDbColumnName                                                              
     end                                                              
     else if @vcListItemType='T'                                                               
     begin                                                              
      set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                              
      set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                              
     end                             
     else if   @vcListItemType='U'                                                           
    begin                             
                                
                                  
    set @strSql=@strSql+',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'                                                              
    end
	ELSE IF @vcListItemType='SYS'                                                         
	BEGIN
		SET @strSql=@strSql+',case tintCRMType when 1 then ''Prospect'' when 2 then ''Account'' else ''Lead'' end as ['+ @vcColumnName+']'                                                              
	END                                  
    end           
          
else if @vcAssociatedControlType='DateField'                                                        
 begin                  
             
     set @strSql=@strSql+',case when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''            
     set @strSql=@strSql+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''            
     set @strSql=@strSql+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '            
     set @strSql=@strSql+'else dbo.FormatedDateFromDate('+@Prefix+@vcDbColumnName+','+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'                      
   end   
        
ELSE IF @vcAssociatedControlType='TextBox' AND @vcDbColumnName='vcBillCity' 
BEGIN
	SET @strSql = @strSql + ',AD5.vcCity' + ' [' + @vcColumnName + ']'  
				
	SET @WhereCondition = @WhereCondition
				+ ' left Join AddressDetails AD5 on AD5.numRecordId= DM.numDivisionID and AD5.tintAddressOf=2 and AD5.tintAddressType=1 AND AD5.bitIsPrimary=1 and AD5.numDomainID= DM.numDomainID '

END
ELSE IF @vcAssociatedControlType='TextBox' AND   @vcDbColumnName = 'vcCompactContactDetails'
BEGIN
	SET @strSql=@strSql+ ' ,'''' AS vcCompactContactDetails'   
END  
ELSE IF @vcAssociatedControlType='TextBox' AND  @vcDbColumnName='vcShipCity'
BEGIN
	SET @strSql=@strSql + ',AD6.vcCity' + ' [' + @vcColumnName + ']' 
						
	SET @WhereCondition = @WhereCondition + ' left Join AddressDetails AD6 on AD6.numRecordId= DM.numDivisionID and AD6.tintAddressOf=2 and AD6.tintAddressType=2 AND AD6.bitIsPrimary=1 and AD6.numDomainID= DM.numDomainID '
END                
else if @vcAssociatedControlType='TextBox'  OR @vcAssociatedControlType='Label'                                                       
begin          
 set @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when          
  @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' else @vcDbColumnName end+' ['+ @vcColumnName+']'                    
             
 end          
else                                                  
begin      
 set @strSql=@strSql+','+ @vcDbColumnName +' ['+ @vcColumnName+']'                
         
 end    
            
 end            
else if @bitCustom = 1            
begin            
               
   select @vcFieldName=FLd_label,@vcAssociatedControlType=fld_type,@vcDbColumnName='Cust'+Convert(varchar(10),Fld_Id)                           
    from CFW_Fld_Master                                            
   where  CFW_Fld_Master.Fld_Id = @numFieldId             
            
   SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'    
          
   if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'             
   begin              
                 
    set @strSql= @strSql+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'               
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Dm.numDivisionId   '                                                     
   end            
    else if @vcAssociatedControlType = 'CheckBox'              
   begin              
                 
-- OLD    set @strSql= @strSql+',case when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=0 then ''No'' when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=1 then ''Yes'' end   ['+ @vcFieldName+'~'+ @vcDbColumnName+']'               
set @strSql= @strSql+',case when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=0 then 0 when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=1 then 1 end   ['+ @vcColumnName +']'               
  
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '               
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId   '                                                       
   end              
   else if @vcAssociatedControlType = 'DateField'          
   begin    
    set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'               
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Dm.numDivisionId   '                                                     
   end            
   else if @vcAssociatedControlType = 'SelectBox'                
   begin              
    set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)            
    set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                      
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
     on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Dm.numDivisionId   '                                                     
    set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'            
   end
   ELSE IF @vcAssociatedControlType = 'CheckBoxList' 
	BEGIN
		SET @strSql= @strSql+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

	SET @WhereCondition= @WhereCondition 
						+' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)
						+ ' on CFW' + convert(varchar(3),@tintOrder) 
						+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
						+ 'and CFW' + convert(varchar(3),@tintOrder) 
						+ '.RecId=DM.numDivisionId '
	END                    
end                
   
     select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc            
 
  if @@rowcount=0 set @tintOrder=0 
                  
                       
                                      
                                                       
end                             

---- Added by Priya 16 Jan 2018(Flag with Organisation Name)--------
SET @strSql=@strSql+' ,

	((CASE 
	WHEN ADC.numECampaignID > 0 
	THEN 
		 ISNULL( (CASE WHEN 
				ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											WHERE ConECampaign.numContactID = ADC.numContactID AND  ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =1 AND ISNULL(bitSend,0) = 1 AND ADC.bitPrimaryContact = 1),0) 
				= ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											 WHERE ConECampaign.numContactID = ADC.numContactID AND ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =1 AND ADC.bitPrimaryContact = 1 ),0)
		THEN
		  ''CheckeredFlag''
		  Else
		  ''GreenFlag''
		  End),0)



	ELSE '''' 
	END)) as FollowupFlag '

	SET @strSql=@strSql+' ,

	((CASE 
	WHEN ADC.numECampaignID > 0 
	THEN 
		''('' + CAST(ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											WHERE ConECampaign.numContactID = ADC.numContactID AND  ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =1 AND ISNULL(bitSend,0) = 1 AND ADC.bitPrimaryContact = 1),0) AS varchar)

			+ ''/'' + CAST(ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											 WHERE ConECampaign.numContactID = ADC.numContactID AND ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =1 AND ADC.bitPrimaryContact = 1 ),0)AS varchar) + '')''
			+ ''<a onclick=openDrip('' + (cast(ISNULL((SELECT MAX([numConEmailCampID]) FROM [ConECampaign] WHERE [numContactID] = ADC.numContactID AND [numECampaignID]= ADC.numECampaignID AND ISNULL(bitEngaged,0)=1 AND ADC.bitPrimaryContact = 1 ),0)AS varchar)) + '');>
		 <img alt=Follow-up Campaign history height=16px width=16px title=Follow-up campaign history src=../images/GLReport.png
		 ></a>'' 
	ELSE '''' 
	END)) as ReadUnreadCntNHstr '

	---- Added by Priya 16 Jan 2018(Flag with Organisation Name)--------
set @strSql=@strSql+' ,TotalRowCount,
	ISNULL(VIE.Total,0) as TotalEmail,
	ISNULL(VOA.OpenActionItemCount,0) as TotalActionItem
  From CompanyInfo CMP                                                                
    join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID                                                      
    left join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID  
    left join VIEW_OPEN_ACTION_ITEMS VOA WITH (NOEXPAND) ON VOA.numDomainID = DM.numDomainID AND  VOA.numDivisionId = DM.numDivisionID
	left join View_InboxEmail VIE  WITH (NOEXPAND) ON VIE.numDomainID = DM.numDomainID AND  VIE.numContactId = ADC.numContactId
   '+@WhereCondition+       
' join tblCompany T on T.numDivisionID=DM.numDivisionID                                                                                          
  WHERE (ISNULL(ADC.bitPrimaryContact,0)=1 OR ADC.numContactID IS NULL) and 
  CMP.numDomainID=  DM.numDomainID and DM.numDomainID = ' + convert(varchar(18),@numDomainID) + '
  and RowNumber > '+convert(varchar(10),@firstRec)+ ' and RowNumber <'+ convert(varchar(10),@lastRec)     
  
  

SET @strSql = REPLACE(@strSql,'##JOIN##',@WhereCondition)              
-- new code added by sojan 05 Mar 2010

--declare @vcTestSql varchar(8000);
--declare @FieldCount numeric(8);
--
--set @vcTestSql = substring(@strSql, charindex('select',@strSql),len(@strSql))
--select @FieldCount= [dbo].[uf_charCount] (@vcTestSql,',')
--
--set @vcTestSql=' union select count(*),' + replicate('Null,',@FieldCount-2)
--
--set @vcTestSql=substring(@vcTestSql,1,len(@vcTestSql)-1)
--set @vcTestSql = @vcTestSql +' from tblCompany order by RowNumber'   
--
--set @strSql=REPLACE(@strSql,'|',',') + @vcTestSql

-- old code
--' union select count(*),null,null,null,null,null '+@strColumns+' from tblCompany order by RowNumber'                                                 
      
SET @strSql =@strSql + ' ORDER BY '+@column+' '+ @columnSortOrder+' '                                
print @strSql                              
                      
exec (@strSql) 


SELECT * FROM #tempForm

DROP TABLE #tempForm
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAccountReceivableAging')
DROP PROCEDURE USP_GetAccountReceivableAging
GO
CREATE PROCEDURE [dbo].[USP_GetAccountReceivableAging]
(
	@numDomainId   AS NUMERIC(9)  = 0,
	@numDivisionId AS NUMERIC(9)  = NULL,
	@numUserCntID AS NUMERIC(9),
	@numAccountClass AS NUMERIC(9)=0,
	@dtFromDate AS DATETIME = NULL,
	@dtToDate AS DATETIME = NULL,
	@ClientTimeZoneOffset INT = 0,
	@bitCustomerStatement BIT = 0,
	@tintBasedOn TINYINT = 1
)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF @dtFromDate IS NULL
		SET @dtFromDate = '1990-01-01 00:00:00.000'
	IF @dtToDate IS NULL
		SET @dtToDate = GETUTCDATE()

	--SET @dtFromDate = DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), @dtFromDate);
	--SET @dtToDate =  DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), @dtToDate);

	PRINT @dtFromDate
	PRINT @dtToDate

	DECLARE  @AuthoritativeSalesBizDocId  AS INTEGER
    SELECT @AuthoritativeSalesBizDocId = isnull(numAuthoritativeSales,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId = @numDomainId;
  
INSERT  INTO [TempARRecord]
        (
          numUserCntID,
          [numOppId],
          [numDivisionId],
          [numOppBizDocsId],
          [DealAmount],
          [numBizDocId],
          [numCurrencyID],
          [dtDueDate],AmountPaid
        )
        SELECT  @numUserCntID,
                OB.numOppId,
                Opp.numDivisionId,
                OB.numOppBizDocsId,
                ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate, 1), 0) - ISNULL(ISNULL(TablePayments.monPaidAmount,0) * ISNULL(Opp.fltExchangeRate, 1),0) DealAmount,
                OB.numBizDocId,
                ISNULL(Opp.numCurrencyID, 0) numCurrencyID,
				DATEADD(DAY,CASE WHEN Opp.bitBillingTerms = 1 
								 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)), 0))
								 ELSE 0 
							END, dtFromDate) AS dtDueDate,
                ISNULL(ISNULL(TablePayments.monPaidAmount,0) * ISNULL(Opp.fltExchangeRate, 1),0) as AmountPaid
        FROM    OpportunityMaster Opp
                JOIN OpportunityBizDocs OB ON OB.numOppId = Opp.numOppID
                LEFT OUTER JOIN [Currency] C ON Opp.[numCurrencyID] = C.[numCurrencyID]
				JOIN [dbo].[DivisionMaster] AS DIV ON DIV.[numDivisionID] = Opp.[numDivisionId]
				OUTER APPLY
			   (
					SELECT 
						SUM(monAmountPaid) AS monPaidAmount
					FROM
						DepositeDetails DD
					INNER JOIN
						DepositMaster DM
					ON
						DD.numDepositID=DM.numDepositId
					WHERE 
						numOppID = Opp.numOppID
						AND numOppBizDocsID=OB.numOppBizDocsID
						AND 1 = (CASE WHEN ISNULL(DM.numReturnHeaderID,0) > 0 THEN (CASE WHEN CONVERT(DATE,DATEADD(MINUTE,-@ClientTimeZoneOffset,DD.dtCreatedDate)) <= CAST(@dtToDate AS DATE) THEN 1 ELSE 0 END) ELSE (CASE WHEN CONVERT(DATE,DM.dtDepositDate) <= CAST(@dtToDate AS DATE) THEN 1 ELSE 0 END) END)
				) TablePayments
        WHERE   tintOpptype = 1
                AND tintoppStatus = 1
                --AND dtShippedDate IS NULL --Commented by chintan reason: Account Transaction and AR values was not matching
                AND opp.numDomainID = @numDomainId
                AND numBizDocId = @AuthoritativeSalesBizDocId
                AND OB.bitAuthoritativeBizDocs=1
                AND ISNULL(OB.tintDeferred,0) <> 1
                AND (Opp.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
                            AND (Opp.numAccountClass=@numAccountClass OR @numAccountClass=0)
				AND (CASE 
						WHEN ISNULL(@bitCustomerStatement,0) = 1 
						THEN CONVERT(DATE,OB.[dtCreatedDate]) 
						ELSE CONVERT(DATE,DATEADD(DAY,CASE 
														WHEN Opp.bitBillingTerms = 1 AND @tintBasedOn=1
														THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)), 0))
														ELSE 0 
													END, dtFromDate)) 
						END) <= @dtToDate
        GROUP BY OB.numOppId,
                Opp.numDivisionId,
                OB.numOppBizDocsId,
                Opp.fltExchangeRate,
                OB.numBizDocId,
                OB.dtCreatedDate,
                Opp.bitBillingTerms,
                Opp.intBillingDays,
                OB.monDealAmount,
                Opp.numCurrencyID,
                OB.dtFromDate
				,TablePayments.monPaidAmount
        HAVING  ( ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate, 1), 0)
                  - ISNULL(ISNULL(TablePayments.monPaidAmount,0) * ISNULL(Opp.fltExchangeRate, 1),
                           0) ) != 0
                           
	--Show Impact of AR journal entries in report as well 
UNION 
 SELECT @numUserCntID,
        0,
        GJD.numCustomerId,
        0,
        CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END,
        0,
        GJD.[numCurrencyID],
        GJH.datEntry_Date ,0 AS AmountPaid
 FROM   dbo.General_Journal_Header GJH
        INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
                                                      AND GJH.numDomainId = GJD.numDomainId
        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
		JOIN [dbo].[DivisionMaster] AS DIV ON DIV.[numDivisionID] = [GJD].[numCustomerId]
 WHERE  GJH.numDomainId = @numDomainId
        AND COA.vcAccountCode LIKE '01010105%'
        AND ISNULL(numOppId,0)=0 AND ISNULL(numOppBizDocsId,0)=0
        AND (GJD.numCustomerId = @numDivisionId OR @numDivisionId IS NULL)
		AND ISNULL(GJH.numDepositID,0)=0 AND ISNULL(GJH.numReturnID,0)=0
		AND (GJH.numAccountClass=@numAccountClass OR @numAccountClass=0)                            
		AND CONVERT(DATE,GJH.[datEntry_Date]) <= @dtToDate
UNION ALL --Standalone Refund against Account Receivable
SELECT @numUserCntID,
        0,
        RH.numDivisionId,
        0,
        CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END,
		--(ISNULL(RH.[monBizDocAmount],0) - (CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END)),
        0,
        GJD.[numCurrencyID],
        GJH.datEntry_Date ,0 AS AmountPaid
 FROM   dbo.ReturnHeader RH JOIN dbo.General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
 JOIN [dbo].[DivisionMaster] AS DIV ON DIV.[numDivisionID] = RH.[numDivisionId]
JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId 
JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId AND COA.vcAccountCode LIKE '0101010501'
WHERE RH.tintReturnType=4 AND RH.numDomainId=@numDomainId  
AND ISNULL(RH.numParentID,0) = 0
AND ISNULL(RH.IsUnappliedPayment,0) = 0
		AND (RH.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
		AND (monBizDocAmount > 0) AND ISNULL(RH.numBillPaymentIDRef,0)=0 --AND ISNULL(RH.numDepositIDRef,0) > 0
		AND (RH.numAccountClass=@numAccountClass OR @numAccountClass=0)
		AND CONVERT(DATE,GJH.[datEntry_Date]) <= @dtToDate
		--AND RH.numDivisionId <> 235216
		--AND (ISNULL(RH.[monBizDocAmount],0) - (CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END)) != 0
		--GROUP BY DM.numDivisionId,DM.dtDepositDate

INSERT  INTO [TempARRecord]
        (
          numUserCntID,
          [numOppId],
          [numDivisionId],
          [numOppBizDocsId],
          [DealAmount],
          [numBizDocId],
          [numCurrencyID],
          [dtDueDate],AmountPaid,monUnAppliedAmount
        )
		SELECT
			@numUserCntID,0, numDivisionId,0,0,0,0,NULL,0,SUM(monDepositAmount)
		FROM
		(
			SELECT
				DM.numDivisionID
				,ISNULL(DM.monDepositAmount,0) -  ISNULL(DM.monAppliedAmount,0) monDepositAmount
			FROM
				DepositMaster DM 
			JOIN 
				[dbo].[DivisionMaster] AS DIV 
			ON 
				DIV.[numDivisionID] = DM.[numDivisionID]
			WHERE 
				DM.numDomainId=@numDomainId AND tintDepositePage IN(2,3)  
				AND (DM.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
				AND ((ISNULL(monDepositAmount,0) - ISNULL(DM.monAppliedAmount,0)) <> 0)
				AND (DM.numAccountClass=@numAccountClass OR @numAccountClass=0)
				AND CONVERT(DATE,DM.[dtDepositDate]) <= @dtToDate
				AND ISNULL(DM.numReturnHeaderID,0) = 0
			UNION ALL
			SELECT
				DM.numDivisionID
				,ISNULL(DM.monDepositAmount,0) - ISNULL((SELECT SUM(DD.monAmountPaid) FROM DepositeDetails DD WHERE DD.numDepositID=DM.numDepositId AND DATEADD(MINUTE,-@ClientTimeZoneOffset,DD.dtCreatedDate) <= @dtToDate),0) monDepositAmount
			FROM
				DepositMaster DM 
			JOIN 
				[dbo].[DivisionMaster] AS DIV 
			ON 
				DIV.[numDivisionID] = DM.[numDivisionID]
			WHERE 
				DM.numDomainId=@numDomainId AND tintDepositePage IN(2,3)  
				AND (DM.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
				AND ((ISNULL(monDepositAmount,0) - ISNULL((SELECT SUM(DD.monAmountPaid) FROM DepositeDetails DD WHERE DD.numDepositID=DM.numDepositId AND DATEADD(MINUTE,-@ClientTimeZoneOffset,DD.dtCreatedDate) <= @dtToDate),0)) <> 0)
				AND (DM.numAccountClass=@numAccountClass OR @numAccountClass=0)
				AND CONVERT(DATE,DM.[dtDepositDate]) <= @dtToDate
				AND ISNULL(DM.numReturnHeaderID,0) > 0
		) TEMP
		GROUP BY 
			numDivisionId

INSERT  INTO [TempARRecord1] (
	[numUserCntID],
	[numDivisionId],
	[tintCRMType],
	[vcCompanyName],
	[vcCustPhone],
	[numContactId],
	[vcEmail],
	[numPhone],
	[vcContactName],
	[numCurrentDays],
	[numThirtyDays],
	[numSixtyDays],
	[numNinetyDays],
	[numOverNinetyDays],
	[numThirtyDaysOverDue],
	[numSixtyDaysOverDue],
	[numNinetyDaysOverDue],
	[numOverNinetyDaysOverDue],
	numCompanyID,
	numDomainID,
	[intCurrentDaysCount],
	[intThirtyDaysCount],
	[intThirtyDaysOverDueCount],
	[intSixtyDaysCount],
	[intSixtyDaysOverDueCount],
	[intNinetyDaysCount],
	[intOverNinetyDaysCount],
	[intNinetyDaysOverDueCount],
	[intOverNinetyDaysOverDueCount],
	[numCurrentDaysPaid],
	[numThirtyDaysPaid],
	[numSixtyDaysPaid],
	[numNinetyDaysPaid],
	[numOverNinetyDaysPaid],
	[numThirtyDaysOverDuePaid],
	[numSixtyDaysOverDuePaid],
	[numNinetyDaysOverDuePaid],
	[numOverNinetyDaysOverDuePaid],monUnAppliedAmount
) 
        SELECT DISTINCT
                @numUserCntID,
                Div.numDivisionID,
                tintCRMType AS tintCRMType,
                vcCompanyName AS vcCompanyName,
                '',
                NULL,
                '',
                '',
                '',
				0 numCurrentDays,
                0 numThirtyDays,
                0 numSixtyDays,
                0 numNinetyDays,
                0 numOverNinetyDays,
                0 numThirtyDaysOverDue,
                0 numSixtyDaysOverDue,
                0 numNinetyDaysOverDue,
                0 numOverNinetyDaysOverDue,
                C.numCompanyID,
				Div.numDomainID,
				0 [intCurrentDaysCount],
                0 [intThirtyDaysCount],
				0 [intThirtyDaysOverDueCount],
				0 [intSixtyDaysCount],
				0 [intSixtyDaysOverDueCount],
				0 [intNinetyDaysCount],
				0 [intOverNinetyDaysCount],
				0 [intNinetyDaysOverDueCount],
				0 [intOverNinetyDaysOverDueCount],
				0 numCurrentDaysPaid,
				0 numThirtyDaysPaid,
                0 numSixtyDaysPaid,
                0 numNinetyDaysPaid,
                0 numOverNinetyDaysPaid,
                0 numThirtyDaysOverDuePaid,
                0 numSixtyDaysOverDuePaid,
                0 numNinetyDaysOverDuePaid,
                0 numOverNinetyDaysOverDuePaid,0 monUnAppliedAmount
        FROM    Divisionmaster Div
                INNER JOIN CompanyInfo C ON C.numCompanyId = Div.numCompanyID
                    --JOIN [TempARRecord] t ON Div.[numDivisionID] = t.[numDivisionId]
        WHERE   Div.[numDomainID] = @numDomainId
                AND Div.[numDivisionID] IN ( SELECT DISTINCT
                                                    [numDivisionID]
                                             FROM   [TempARRecord] )



--Update Custome Phone 
UPDATE  TempARRecord1
SET     [vcCustPhone] = X.[vcCustPhone]
FROM    ( SELECT 
                            CASE WHEN numPhone IS NULL THEN ''
                                 WHEN numPhone = '' THEN ''
                                 ELSE ', ' + numPhone
                            END AS vcCustPhone,
                            numDivisionId
                  FROM      AdditionalContactsInformation
                  WHERE     bitPrimaryContact = 1
                            AND numDivisionId  IN (SELECT DISTINCT numDivisionID FROM [TempARRecord1] WHERE numUserCntID =@numUserCntID)

        ) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID

--Update Customer info 
UPDATE  TempARRecord1
SET     [numContactId] = X.[numContactID],[vcEmail] = X.[vcEmail],[vcContactName] = X.[vcContactName],numPhone=X.numPhone
FROM    ( SELECT numContactId AS numContactID,
                     ISNULL(vcEmail,'') AS vcEmail,
                     ISNULL(vcFirstname,'') + ' ' + ISNULL(vcLastName,'') AS vcContactName,
                     CAST(ISNULL( CASE WHEN numPhone IS NULL THEN '' WHEN numPhone = '' THEN '' ELSE ', ' + numPhone END,'') AS VARCHAR) AS numPhone,
                     [numDivisionId]
                  FROM      AdditionalContactsInformation
                  WHERE     (vcPosition = 844 or isnull(bitPrimaryContact,0)=1)
                            AND numDivisionId  IN (SELECT DISTINCT numDivisionID FROM [TempARRecord1] WHERE numUserCntID =@numUserCntID)
    ) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID

                    
--SELECT  GETDATE()
------------------------------------------      
DECLARE @baseCurrency AS NUMERIC
SELECT @baseCurrency=numCurrencyID FROM dbo.Domain WHERE numDomainId=@numDomainId
/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */
------------------------------------------

UPDATE  TempARRecord1
SET     numCurrentDays = X.numCurrentDays
FROM    ( SELECT    SUM(DealAmount) AS numCurrentDays,
                    [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND
					DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) >= 0
					--AND 
					--dbo.GetUTCDateWithoutTime() <= [dtDueDate]
          GROUP BY  numDivisionId
        ) X WHERE   TempARRecord1.numDivisionID = X.numDivisionID

UPDATE  TempARRecord1
SET     numCurrentDaysPaid = X.numCurrentDaysPaid
FROM    ( SELECT    SUM(AmountPaid) AS numCurrentDaysPaid,
                    [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND
					DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate])>= 0 
					--AND 
					--dbo.GetUTCDateWithoutTime() <= [dtDueDate]
          GROUP BY  numDivisionId
        ) X WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intCurrentDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
    FROM      [TempARRecord]
    WHERE     numUserCntID =@numUserCntID AND 
			 DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) >= 0
			  -- AND 
			  --dbo.GetUTCDateWithoutTime() <= [dtDueDate] 
			  AND 
			  numCurrencyID <>@baseCurrency )

---------------------------------------------------------------------------------------------


-----------------------Below ThirtyDays to OverNinetyDays is not being Used Now----------------
UPDATE  TempARRecord1
SET     numThirtyDays = X.numThirtyDays
FROM    ( SELECT    SUM(DealAmount) AS numThirtyDays,
                    [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND
		DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 0 AND 30	
          GROUP BY  numDivisionId
        ) X WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numThirtyDaysPaid = X.numThirtyDaysPaid
FROM    ( SELECT    SUM(AmountPaid) AS numThirtyDaysPaid,
                    [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND
		DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 0 AND 30	
          GROUP BY  numDivisionId
        ) X WHERE   TempARRecord1.numDivisionID = X.numDivisionID

                
UPDATE  TempARRecord1 SET intThirtyDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 0 AND 30	
                                                AND numCurrencyID <>@baseCurrency )
------------------------------------------
UPDATE  TempARRecord1
SET     numSixtyDays = X.numSixtyDays
FROM    ( SELECT  SUM(DealAmount) AS numSixtyDays,
        numDivisionId
FROM    [TempARRecord]
WHERE   numUserCntID =@numUserCntID AND 
	DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 31 AND 60	
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numSixtyDaysPaid = X.numSixtyDaysPaid
FROM    ( SELECT  SUM(AmountPaid) AS numSixtyDaysPaid,
        numDivisionId
FROM    [TempARRecord]
WHERE   numUserCntID =@numUserCntID AND 
	DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 31 AND 60	
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intSixtyDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 31 AND 60
		AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                                                AND numCurrencyID <>@baseCurrency )
------------------------------------------
UPDATE  TempARRecord1
SET     numNinetyDays = X.numNinetyDays
FROM    (SELECT  SUM(DealAmount) AS numNinetyDays,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 61 AND 90
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numNinetyDaysPaid = X.numNinetyDaysPaid
FROM    (SELECT  SUM(AmountPaid) AS numNinetyDaysPaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 61 AND 90
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intNinetyDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 61 AND 90
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                                                AND numCurrencyID <>@baseCurrency )
------------------------------------------
UPDATE  TempARRecord1
SET     numOverNinetyDays = X.numOverNinetyDays
FROM    (SELECT  SUM(DealAmount) AS numOverNinetyDays,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) > 90
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numOverNinetyDaysPaid = X.numOverNinetyDaysPaid
FROM    (SELECT  SUM(AmountPaid) AS numOverNinetyDaysPaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) > 90
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intOverNinetyDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND [dtDueDate] >= DATEADD(DAY, 91,
                                                    dbo.GetUTCDateWithoutTime())
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                                                AND numCurrencyID <>@baseCurrency )

-----------------------Above ThirtyDays to OverNinetyDays is not being Used Now----------------

------------------------------------------
UPDATE  TempARRecord1
SET     numThirtyDaysOverDue = X.numThirtyDaysOverDue
FROM    (SELECT  SUM(DealAmount) AS numThirtyDaysOverDue,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 1 AND 30
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numThirtyDaysOverDuePaid = X.numThirtyDaysOverDuePaid
FROM    (SELECT  SUM(AmountPaid) AS numThirtyDaysOverDuePaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 1 AND 30
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intThirtyDaysOverDueCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 1 AND 30
                                                AND numCurrencyID <>@baseCurrency )

----------------------------------
UPDATE  TempARRecord1
SET     numSixtyDaysOverDue = X.numSixtyDaysOverDue
FROM    ( SELECT    SUM(DealAmount) AS numSixtyDaysOverDue,
                    numDivisionId
          FROM      TempARRecord
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                    AND DATEDIFF(DAY,
                                 [dtDueDate],
                                 dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                              AND     60
          GROUP BY  numDivisionId
        ) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID

UPDATE  TempARRecord1
SET     numSixtyDaysOverDuePaid = X.numSixtyDaysOverDuePaid
FROM    ( SELECT    SUM(AmountPaid) AS numSixtyDaysOverDuePaid,
                    numDivisionId
          FROM      TempARRecord
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                    AND DATEDIFF(DAY,
                                 [dtDueDate],
                                 dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                              AND     60
          GROUP BY  numDivisionId
        ) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intSixtyDaysOverDueCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                    AND DATEDIFF(DAY,
                                 [dtDueDate],
                                 dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                              AND     60
                                                AND numCurrencyID <>@baseCurrency )

------------------------------------------
UPDATE  TempARRecord1
SET     numNinetyDaysOverDue = X.numNinetyDaysOverDue
FROM    (SELECT  SUM(DealAmount) AS numNinetyDaysOverDue,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numNinetyDaysOverDuePaid = X.numNinetyDaysOverDuePaid
FROM    (SELECT  SUM(AmountPaid) AS numNinetyDaysOverDuePaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intNinetyDaysOverDueCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90
                                                AND numCurrencyID <>@baseCurrency )
------------------------------------------
UPDATE  TempARRecord1
SET     numOverNinetyDaysOverDue = X.numOverNinetyDaysOverDue
FROM    (SELECT  SUM(DealAmount) AS numOverNinetyDaysOverDue,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY, [dtDueDate], dbo.GetUTCDateWithoutTime()) > 90
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numOverNinetyDaysOverDuePaid = X.numOverNinetyDaysOverDuePaid
FROM    (SELECT  SUM(AmountPaid) AS numOverNinetyDaysOverDuePaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY, [dtDueDate], dbo.GetUTCDateWithoutTime()) > 90
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intOverNinetyDaysOverDueCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY, [dtDueDate], dbo.GetUTCDateWithoutTime()) > 90
                                                AND numCurrencyID <>@baseCurrency )


UPDATE  TempARRecord1
SET     monUnAppliedAmount = ISNULL(X.monUnAppliedAmount,0)
FROM    (SELECT  SUM(ISNULL(monUnAppliedAmount,0)) AS monUnAppliedAmount,numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID 
GROUP BY numDivisionId
) X WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET numTotal=  isnull(numCurrentDays,0) + isnull(numThirtyDaysOverDue,0)
      + isnull(numSixtyDaysOverDue,0)
      + isnull(numNinetyDaysOverDue,0)
      --+ isnull(numOverNinetyDays,0)
      + isnull(numOverNinetyDaysOverDue,0) - ISNULL(monUnAppliedAmount,0)
           
SELECT  
		[numDivisionId],
		[numContactId],
		[tintCRMType],
		[vcCompanyName],
		[vcCustPhone],
		[vcContactName],
		[vcEmail],
		[numPhone],
		[numCurrentDays],
		[numThirtyDays],
		[numSixtyDays],
		[numNinetyDays],
		[numOverNinetyDays],
		[numThirtyDaysOverDue],
		[numSixtyDaysOverDue],
		[numNinetyDaysOverDue],
		[numOverNinetyDaysOverDue],
--		CASE WHEN (ISNULL(numTotal,0) - ISNULL(monUnAppliedAmount,0))>=0
--		 THEN ISNULL(numTotal,0) - ISNULL(monUnAppliedAmount,0) ELSE ISNULL(numTotal,0) END AS numTotal ,
		ISNULL(numTotal,0) AS numTotal,
		numCompanyID,
		numDomainID,
		[intCurrentDaysCount],
		[intThirtyDaysCount],
		[intThirtyDaysOverDueCount],
		[intSixtyDaysCount],
		[intSixtyDaysOverDueCount],
		[intNinetyDaysCount],
		[intOverNinetyDaysCount],
		[intNinetyDaysOverDueCount],
		[intOverNinetyDaysOverDueCount],
		[numCurrentDaysPaid],
		[numThirtyDaysPaid],
		[numSixtyDaysPaid],
		[numNinetyDaysPaid],
		[numOverNinetyDaysPaid],
		[numThirtyDaysOverDuePaid],
		[numSixtyDaysOverDuePaid],
		[numNinetyDaysOverDuePaid],
		[numOverNinetyDaysOverDuePaid],ISNULL(monUnAppliedAmount,0) AS monUnAppliedAmount
FROM    TempARRecord1 WHERE numUserCntID =@numUserCntID AND ISNULL(numTotal,0)!=0
ORDER BY [vcCompanyName] 
DELETE  FROM TempARRecord1 WHERE   numUserCntID = @numUserCntID ;
DELETE  FROM TempARRecord WHERE   numUserCntID = @numUserCntID

END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAccountReceivableAging_Details]    Script Date: 05/24/2010 03:48:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAccountReceivableAging_Details')
DROP PROCEDURE USP_GetAccountReceivableAging_Details
GO
CREATE PROCEDURE [dbo].[USP_GetAccountReceivableAging_Details]
(
    @numDomainId AS NUMERIC(9)  = 0,
    @numDivisionID NUMERIC(9),
    @vcFlag VARCHAR(20),
	@numAccountClass AS NUMERIC(9)=0,
	@dtFromDate AS DATE = NULL,
	@dtToDate AS DATE = NULL,
	@ClientTimeZoneOffset INT = 0,
	@tintBasedOn TINYINT = 1
)
AS
  BEGIN
	DECLARE @strSql VARCHAR(8000);
	Declare @strSql1 varchar(8000);
	Declare @strCredit varchar(8000); SET @strCredit=''
	
    DECLARE  @AuthoritativeSalesBizDocId  AS INTEGER
    SELECT @AuthoritativeSalesBizDocId = isnull(numAuthoritativeSales,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId = @numDomainId;--287
    ------------------------------------------      
    DECLARE @baseCurrency AS NUMERIC
	SELECT @baseCurrency=numCurrencyID FROM dbo.Domain WHERE numDomainId=@numDomainId
	/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */
	------------------------------------------      
    IF @dtFromDate IS NULL
		SET @dtFromDate = '1990-01-01 00:00:00.000'
	IF @dtToDate IS NULL
		SET @dtToDate = GETUTCDATE()
    --Get Master data
	 
	 SET @strSql = '
     SELECT DISTINCT OM.[numOppId],
					 -1 AS numJournal_Id,
					 -1 as numDepositId, -1 as tintDepositePage,-1 as numReturnHeaderID,
                        OM.[vcPOppName],
                        OB.[numOppBizDocsId],
                        OB.[vcBizDocID],
                        isnull(OB.monDealAmount
                                 ,0) TotalAmount,--OM.[monPAmount]
                        (ISNULL(TablePayments.monPaidAmount,0)
                           ) AmountPaid,
                        ISNULL(OB.monDealAmount * ISNULL(OM.fltExchangeRate, 1), 0) - ISNULL(ISNULL(TablePayments.monPaidAmount,0) * ISNULL(OM.fltExchangeRate, 1),0) BalanceDue,
						[dbo].[FormatedDateFromDate](ob.[dtFromDate],'+ CONVERT(VARCHAR(15),@numDomainId) +') dtDate,
                        CASE ISNULL(bitBillingTerms,0) 
                          --WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0)),OB.dtFromDate),
                          WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate),
                                                                   '+ CONVERT(VARCHAR(15),@numDomainId) +')
                          WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],'+ CONVERT(VARCHAR(15),@numDomainId) +')
                        END AS DueDate,
                        bitBillingTerms,
                        intBillingDays,
                        OB.dtCreatedDate,
                        ISNULL(C.[varCurrSymbol],'''') [varCurrSymbol],
                        CASE WHEN '+ CONVERT(VARCHAR(15),@baseCurrency) +' <> OM.numCurrencyID THEN 2 ELSE 1 END AS CurrencyCount,
                        OB.vcRefOrderNo ,OM.[numDivisionId]
        FROM   [OpportunityMaster] OM INNER JOIN [DivisionMaster] DM ON OM.[numDivisionId] = DM.[numDivisionID]
               LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN [Currency] C ON OM.[numCurrencyID] = C.[numCurrencyID]
			   OUTER APPLY
			   (
					SELECT 
						SUM(monAmountPaid) AS monPaidAmount
					FROM
						DepositeDetails DD
					INNER JOIN
						DepositMaster DM
					ON
						DD.numDepositID=DM.numDepositId
					WHERE 
						numOppID = OM.numOppID
						AND numOppBizDocsID=OB.numOppBizDocsID
						AND 1 = (CASE WHEN ISNULL(DM.numReturnHeaderID,0) > 0 THEN (CASE WHEN CONVERT(DATE,DATEADD(MINUTE,-1 * ' + CAST(@ClientTimeZoneOffset AS VARCHAR) + ',DD.dtCreatedDate)) <= ''' + CAST(@dtToDate AS VARCHAr) + ''' THEN 1 ELSE 0 END) ELSE (CASE WHEN CONVERT(DATE,DM.dtDepositDate) <= ''' + CAST(@dtToDate AS VARCHAr) + ''' THEN 1 ELSE 0 END) END)
				) TablePayments
        WHERE  OM.[tintOppType] = 1
               AND OM.[tintOppStatus] = 1
               AND om.[numDomainId] = '+ CONVERT(VARCHAR(15),@numDomainId) +'
               AND OB.[numBizDocId] = '+ CONVERT(VARCHAR(15),ISNULL(@AuthoritativeSalesBizDocId,0)) +'
               AND OB.bitAuthoritativeBizDocs=1
               AND ISNULL(OB.tintDeferred,0) <> 1
               AND (DM.[numDivisionID] = '+ CONVERT(VARCHAR(15),@numDivisionID) + ' OR ' + CAST(@numDivisionID AS VARCHAR) + '=0)
			   AND (OM.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
			   AND CONVERT(DATE,DATEADD(DAY,CASE WHEN OM.bitBillingTerms = 1  AND ' + CAST(@tintBasedOn AS VARCHAR) + ' = 1
								 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(OM.intBillingDays,0)), 0))
								 ELSE 0 
							END, dtFromDate)) <= ''' + CAST(@dtToDate AS VARCHAr) + '''
               AND isnull(OB.monDealAmount
                            ,0) > 0  AND isnull(OB.monDealAmount * OM.fltExchangeRate
                        - ISNULL(TablePayments.monPaidAmount,0) * OM.fltExchangeRate,0) != 0'
    
    	--Show Impact of AR journal entries in report as well 
    	
SET @strSql1 = '    	
		UNION 
		 SELECT 
				-1 AS [numOppId],
				GJH.numJournal_Id AS numJournal_Id,
				-1 as numDepositId, -1 as tintDepositePage,-1 as numReturnHeaderID,
				''Journal'' [vcPOppName],
				0 [numOppBizDocsId],
				'''' [vcBizDocID],
				CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END TotalAmount,
				0 AmountPaid,
				0 BalanceDue,
				[dbo].[FormatedDateFromDate](GJH.datEntry_Date,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS dtDate,
				[dbo].[FormatedDateFromDate](GJH.datEntry_Date,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate,
				0 bitBillingTerms,
				0 intBillingDays,
				GJH.datCreatedDate,
				ISNULL(C.[varCurrSymbol], '''') [varCurrSymbol],
				1 AS CurrencyCount,'''' as vcRefOrderNo,0 as numDivisionId
		 FROM   dbo.General_Journal_Header GJH
				INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
															  AND GJH.numDomainId = GJD.numDomainId
				INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
				LEFT OUTER JOIN [Currency] C ON GJD.[numCurrencyID] = C.[numCurrencyID]
		 WHERE  GJH.numDomainId = '+ CONVERT(VARCHAR(15),@numDomainId) +'
				and GJD.numCustomerID =' + CONVERT(VARCHAR(15),@numDivisionID) +'
				AND COA.vcAccountCode LIKE ''01010105%'' 
				AND ISNULL(numOppId,0)=0 AND ISNULL(numOppBizDocsId,0)=0 
				AND ISNULL(GJH.numDepositID,0)=0 AND ISNULL(GJH.numReturnID,0)=0
				AND (GJH.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
				AND CONVERT(DATE,GJH.[datEntry_Date]) <= ''' + CAST(@dtToDate AS VARCHAr) + ''''				
    
   SET @strCredit = ' UNION SELECT -1 AS [numOppId],
								-1 AS numJournal_Id,
								numDepositId,tintDepositePage,isnull(numReturnHeaderID,0) as numReturnHeaderID,
                      			CASE WHEN tintDepositePage=2 THEN ''Unapplied Payment'' ELSE ''Credit Memo'' END AS [vcPOppName],
								0 [numOppBizDocsId],
								'''' [vcBizDocID],
								(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) * -1 AS TotalAmount,
								0 AmountPaid,
								(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) * -1 AS BalanceDue,
								[dbo].[FormatedDateFromDate](DM.dtDepositDate,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS dtDate,
								[dbo].[FormatedDateFromDate](DM.dtDepositDate,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate,
								0 bitBillingTerms,
								0 intBillingDays,
								DM.dtCreationDate,
								'''' [varCurrSymbol],
								1 AS CurrencyCount,'''' as vcRefOrderNo,0 as numDivisionId
		 FROM  DepositMaster DM 
		WHERE DM.numDomainId='+ CONVERT(VARCHAR(15),@numDomainId) +' AND tintDepositePage IN(2,3)  
		AND DM.numDivisionId = ' + CONVERT(VARCHAR(15),@numDivisionID) +'
		AND (DM.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
		AND CONVERT(DATE,DM.dtDepositDate)  <= ''' + CAST(@dtToDate AS VARCHAr) + '''
		AND ((ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) <> 0)
		AND ISNULL(DM.numReturnHeaderID,0) = 0 
		UNION 
			SELECT -1 AS [numOppId],
					-1 AS numJournal_Id,
					numDepositId,tintDepositePage,isnull(numReturnHeaderID,0) as numReturnHeaderID,
                    CASE WHEN tintDepositePage=2 THEN ''Unapplied Payment'' ELSE ''Credit Memo'' END AS [vcPOppName],
					0 [numOppBizDocsId],
					'''' [vcBizDocID],
					(ISNULL(monDepositAmount,0) - ISNULL((SELECT SUM(DD.monAmountPaid) FROM DepositeDetails DD WHERE DD.numDepositID=DM.numDepositId AND DATEADD(MINUTE,-1 * ' + CAST(@ClientTimeZoneOffset AS VARCHAR) + ',DD.dtCreatedDate) <= ''' + CAST(@dtToDate AS VARCHAr) + '''),0)) * -1 AS TotalAmount,
					0 AmountPaid,
					(ISNULL(monDepositAmount,0) - ISNULL((SELECT SUM(DD.monAmountPaid) FROM DepositeDetails DD WHERE DD.numDepositID=DM.numDepositId AND DATEADD(MINUTE,-1 * ' + CAST(@ClientTimeZoneOffset AS VARCHAR) + ',DD.dtCreatedDate) <= ''' + CAST(@dtToDate AS VARCHAr) + '''),0)) * -1 AS BalanceDue,
					[dbo].[FormatedDateFromDate](DM.dtDepositDate,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS dtDate,
					[dbo].[FormatedDateFromDate](DM.dtDepositDate,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate,
					0 bitBillingTerms,
					0 intBillingDays,
					DM.dtCreationDate,
					'''' [varCurrSymbol],
					1 AS CurrencyCount,'''' as vcRefOrderNo,0 as numDivisionId
		 FROM  DepositMaster DM 
		WHERE DM.numDomainId='+ CONVERT(VARCHAR(15),@numDomainId) +' AND tintDepositePage IN(2,3)  
		AND DM.numDivisionId = ' + CONVERT(VARCHAR(15),@numDivisionID) +'
		AND (DM.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
		AND CONVERT(DATE,DM.dtDepositDate)  <= ''' + CAST(@dtToDate AS VARCHAr) + '''
		AND ((ISNULL(monDepositAmount,0) - ISNULL((SELECT SUM(DD.monAmountPaid) FROM DepositeDetails DD WHERE DD.numDepositID=DM.numDepositId AND DATEADD(MINUTE,-1 * ' + CAST(@ClientTimeZoneOffset AS VARCHAR) + ',DD.dtCreatedDate) <= ''' + CAST(@dtToDate AS VARCHAr) + '''),0)) <> 0)
		AND ISNULL(DM.numReturnHeaderID,0) > 0'  
    
    IF (@vcFlag = '0+30')
      BEGIN
      
      SET @strSql =@strSql +
						'AND dateadd(DAY,CASE 
                                 --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                 WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                 ELSE 0
                               END,OB.dtFromDate) BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())
               AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                ELSE 0
                                                              END,OB.dtFromDate)'
                                                              
		SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())'
		
		SET @strCredit =@strCredit + 'AND DM.dtDepositDate BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())'
		
      END
    ELSE
      IF (@vcFlag = '30+60')
        BEGIN
         SET @strSql =@strSql +
                 'AND dateadd(DAY,CASE 
                                   --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                   WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                   ELSE 0
                                 END,OB.dtFromDate) BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())
                 AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                  --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                  WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                  ELSE 0
                                                                END,OB.dtFromDate)'
		SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())'

		SET @strCredit =@strCredit + 'AND DM.dtDepositDate BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())'
        END
      ELSE
        IF (@vcFlag = '60+90')
          BEGIN
          SET @strSql =@strSql +
                   'AND dateadd(DAY,CASE 
                                     --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                     WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                     ELSE 0
                                   END,OB.dtFromDate) BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                   AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                    --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                    WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                    ELSE 0
                                                                  END,OB.dtFromDate)'
			SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
			
			SET @strCredit =@strCredit + 'AND DM.dtDepositDate BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
			
          END
        ELSE
          IF (@vcFlag = '90+')
            BEGIN
             SET @strSql =@strSql +
                     'AND dateadd(DAY,CASE 
                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                       ELSE 0
                                     END,OB.dtFromDate) > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                     AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                      --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                      WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                      ELSE 0
                                                                    END,OB.dtFromDate)'
			SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date  > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
                
			SET @strCredit =@strCredit + 'AND DM.dtDepositDate  > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
                     
            END
          ELSE
            IF (@vcFlag = '0-30')
              BEGIN
               SET @strSql =@strSql +
                       'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                       ELSE 0
                                                                     END,OB.dtFromDate)
                       AND datediff(DAY,dateadd(DAY,CASE 
                                                      --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                      WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                      ELSE 0
                                                    END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
				SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
				
				SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
				
              END
            ELSE
              IF (@vcFlag = '30-60')
                BEGIN
                  SET @strSql =@strSql +
                         'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                         --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                         WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                         ELSE 0
                                                                       END,OB.dtFromDate)
                         AND datediff(DAY,dateadd(DAY,CASE 
                                                        --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                        WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                        ELSE 0
                                                      END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60'
                SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60 '
                
                SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60 '
                
                END
              ELSE
                IF (@vcFlag = '60-90')
                  BEGIN
					SET @strSql =@strSql +
                           'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                           --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                           WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                           ELSE 0
                                                                         END,OB.dtFromDate)
                           AND datediff(DAY,dateadd(DAY,CASE 
                                                          --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                          WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                          ELSE 0
                                                        END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90'
					SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 '
					
					SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 '
					
                           
                  END
                ELSE
                  IF (@vcFlag = '90-')
                    BEGIN
                      SET @strSql =@strSql +
                             'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                             --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                             WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                             ELSE 0
                                                                           END,OB.dtFromDate)
                             AND datediff(DAY,dateadd(DAY,CASE 
                                                            --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                            WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                            ELSE 0
                                                          END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) > 90'
					SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) > 90'
					
					SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) > 90'
                             
                    END
                  ELSE
                    BEGIN
                      SET @strSql =@strSql +''
                    END
                    



	SET @strSql =@strSql + @strSql1 + @strCredit
	PRINT @strSql
	EXEC(@strSql)
  END
/****** Object:  StoredProcedure [dbo].[USP_GetAccountReceivableAging_Invoice]    Script Date: 05/24/2010 03:48:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAccountReceivableAging_Invoice')
DROP PROCEDURE USP_GetAccountReceivableAging_Invoice
GO
CREATE PROCEDURE [dbo].[USP_GetAccountReceivableAging_Invoice]
(
    @numDomainId AS NUMERIC(9) = 0,
	@numDivisionId AS NUMERIC(9) = NULL,
	@vcFlag VARCHAR(20),
	@numAccountClass AS NUMERIC(9)=0,
	@dtFromDate AS DATE = NULL,
	@dtToDate AS DATE = NULL,
	@ClientTimeZoneOffset INT = 0,
	@vcRegularSearch VARCHAR(MAX) = '',
	@bitCustomerStatement BIT = 0,
	@tintBasedOn TINYINT = 1
)
AS
  BEGIN
	DECLARE @numDefaultARAccount NUMERIC(18,0)
	SET @numDefaultARAccount = ISNULL((SELECT numAccountID FROM AccountingCharges WHERE numDomainID=@numDomainId AND numChargeTypeId=4),0)

    DECLARE  @AuthoritativeSalesBizDocId  AS INTEGER
    SELECT @AuthoritativeSalesBizDocId = isnull(numAuthoritativeSales,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId = @numDomainId;--287
    ------------------------------------------      
    DECLARE @baseCurrency AS NUMERIC
	SELECT @baseCurrency=numCurrencyID FROM dbo.Domain WHERE numDomainId=@numDomainId
	/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */
	------------------------------------------      
	IF @dtFromDate IS NULL
		SET @dtFromDate = '1990-01-01 00:00:00.000'
	IF @dtToDate IS NULL
		SET @dtToDate = GETUTCDATE()

	SET @numDivisionId = ISNULL(@numDivisionId,0)

    DECLARE @strSql VARCHAR(MAX);
	Declare @strSql1 VARCHAR(MAX);
	Declare @strCredit varchar(MAX); SET @strCredit=''
    
    
    SET @strSql = ' SELECT DISTINCT OM.[numOppId],
                        OM.[vcPOppName],
                        OM.[tintOppType],
                        OB.[numOppBizDocsId],
                        OB.[vcBizDocID],
                        isnull(OB.monDealAmount * OM.fltExchangeRate,0) TotalAmount,
                        (ISNULL(TablePayments.monPaidAmount,0) * OM.fltExchangeRate) AmountPaid,
                        isnull(OB.monDealAmount
                                 * OM.fltExchangeRate
                                 - ISNULL(TablePayments.monPaidAmount,0)
                                     * OM.fltExchangeRate,0) BalanceDue,
						[dbo].[FormatedDateFromDate](ob.[dtFromDate],'+ CONVERT(VARCHAR(15),@numDomainId) +') dtDate,
                        CASE ISNULL(bitBillingTerms,0) 
                          --WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0)),OB.dtFromDate),
                          WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate),
                                                                   '+ CONVERT(VARCHAR(15),@numDomainId) +')
                          WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],'+ CONVERT(VARCHAR(15),@numDomainId) +')
                        END AS DueDate,
                        bitBillingTerms,
                        intBillingDays,
                        OB.dtCreatedDate,
                        ISNULL(C.[varCurrSymbol],'''') [varCurrSymbol],CO.vcCompanyName,ADC.vcFirstname + '' ''+ ADC.vcLastName AS vcContactName,
                           dbo.fn_GetContactName(OM.numRecOwner) AS RecordOwner,
						   dbo.fn_GetContactName(OM.numAssignedTo) AS AssignedTo
						   ,CASE WHEN ADC.numPhone IS NULL THEN '''' ELSE + ADC.numPhone + (CASE WHEN numPhoneExtension IS NULL THEN '''' WHEN numPhoneExtension='''' THEN '''' ELSE '','' + ADC.numPhoneExtension END) END AS vcCustPhone,
						   OM.numCurrencyID,OB.vcRefOrderNo,[dbo].[FormatedDateFromDate](ob.[dtFromDate],'+ CONVERT(VARCHAR(15),@numDomainId) +') AS dtFromDate,
						    -1 AS [numJournal_Id],-1 as numReturnHeaderID,
							CASE WHEN ISNULL(BT.[numDiscount],0) > 0 AND ISNULL(BT.[numInterestPercentage],0) > 0 
								 THEN '+ '''-''+' + 'CONVERT(VARCHAR(10),ISNULL(BT.[numDiscount],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numDiscountPaidInDays],0)) +' + ''' days early / ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentage],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentageIfPaidAfterDays],0)) +' + ''' days or later ''' +
								 ' WHEN ISNULL(BT.[numDiscount],0) > 0 THEN '+ '''-''+' + 'CONVERT(VARCHAR(10),ISNULL(BT.[numDiscount],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numDiscountPaidInDays],0)) +' + ''' days early ''' + 
								 ' WHEN ISNULL(BT.[numInterestPercentage],0) > 0  THEN +CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentage],0))+' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentageIfPaidAfterDays],0))+' + ''' days or later ''' +
								' END AS [vcTerms],
							ISNULL(BT.[numDiscount],0) AS [numDiscount],
							ISNULL(BT.[numInterestPercentage],0) AS [numInterest],
							0 AS tintLevel
							,ISNULL(OB.numARAccountID,'+ CAST(@numDefaultARAccount AS VARCHAR) +') AS numChartAcntId
        INTO #TempRecords                
        FROM   [OpportunityMaster] OM
               INNER JOIN [DivisionMaster] DM
                 ON OM.[numDivisionId] = DM.[numDivisionID]
                  INNER JOIN CompanyInfo CO ON CO.numCompanyId = DM.numCompanyID
                  LEFT JOIN AdditionalContactsInformation ADC ON ADC.numDivisionId=DM.numDivisionID AND ADC.numContactId=OM.numContactId
               LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN [Currency] C ON OM.[numCurrencyID] = C.[numCurrencyID]
			   LEFT JOIN [dbo].[BillingTerms] AS BT ON [OM].[intBillingDays] = [BT].[numTermsID] 
			   OUTER APPLY
			   (
					SELECT 
						SUM(monAmountPaid) AS monPaidAmount
					FROM
						DepositeDetails DD
					INNER JOIN
						DepositMaster DM
					ON
						DD.numDepositID=DM.numDepositId
					WHERE 
						numOppID = OM.numOppID
						AND numOppBizDocsID=OB.numOppBizDocsID
						AND 1 = (CASE WHEN ISNULL(DM.numReturnHeaderID,0) > 0 THEN (CASE WHEN CONVERT(DATE,DATEADD(MINUTE,-1 * ' + CAST(@ClientTimeZoneOffset AS VARCHAR) + ',DD.dtCreatedDate)) <= ''' + CAST(@dtToDate AS VARCHAr) + ''' THEN 1 ELSE 0 END) ELSE (CASE WHEN CONVERT(DATE,DM.dtDepositDate) <= ''' + CAST(@dtToDate AS VARCHAr) + ''' THEN 1 ELSE 0 END) END)
				) TablePayments
        WHERE  OM.[tintOppType] = 1
               AND OM.[tintOppStatus] = 1
               AND om.[numDomainId] = '+ CONVERT(VARCHAR(15),@numDomainId) +'
               AND OB.[numBizDocId] = '+ CONVERT(VARCHAR(15),ISNULL(@AuthoritativeSalesBizDocId,0)) +' 
			   AND (OM.numDivisionId = ' + CAST(@numDivisionId AS VARCHAR) + ' OR ' + CAST(@numDivisionId AS VARCHAR) + ' = 0)
				AND (OM.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
				AND '+ (CASE 
							WHEN ISNULL(@bitCustomerStatement,0) = 1 
							THEN 'CONVERT(DATE,OB.[dtCreatedDate])' 
							ELSE 'CONVERT(DATE,DATEADD(DAY,CASE WHEN OM.bitBillingTerms = 1 AND ' + CAST(@tintBasedOn AS VARCHAR) + '=1
								 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(OM.intBillingDays,0)), 0))
								 ELSE 0 
									END, dtFromDate))' 
						END) + ' <= ''' + CAST(@dtToDate AS VARCHAr) + '''
               AND OB.bitAuthoritativeBizDocs=1
               AND ISNULL(OB.tintDeferred,0) <> 1
			   AND isnull(OB.monDealAmount
                                 * OM.fltExchangeRate
                                 - ISNULL(TablePayments.monPaidAmount,0)
                                     * OM.fltExchangeRate,0) <> 0'     
       
       SET @strSql1 = '  UNION 
		 SELECT 
				-1 [numOppId],
				''Journal'' [vcPOppName],
				0 [tintOppType],
				0 [numOppBizDocsId],
				''Journal'' [vcBizDocID],
				CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END TotalAmount,
				0 AmountPaid,
				CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END BalanceDue,
				[dbo].[FormatedDateFromDate](GJH.datEntry_Date,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS dtDate,
				[dbo].[FormatedDateFromDate](GJH.datEntry_Date,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate,
				0 bitBillingTerms,
				0 intBillingDays,
				GJH.datCreatedDate,
                ISNULL(C.[varCurrSymbol], '''') [varCurrSymbol]
                ,CO.vcCompanyName,ADC.vcFirstname + '' ''+ ADC.vcLastName AS vcContactName,
                           '''' AS RecordOwner,
						   '''' AS AssignedTo
						   ,CASE WHEN ADC.numPhone IS NULL THEN '''' ELSE + ADC.numPhone + (CASE WHEN numPhoneExtension IS NULL THEN '''' WHEN numPhoneExtension='''' THEN '''' ELSE '','' + ADC.numPhoneExtension END) END AS vcCustPhone,
						   GJD.numCurrencyID,'''' as vcRefOrderNo,NULL, ISNULL(GJH.numJournal_Id,0) AS [numJournal_Id],-1 as numReturnHeaderID,
							CASE WHEN ISNULL(BT.[numDiscount],0) > 0 AND ISNULL(BT.[numInterestPercentage],0) > 0 
								 THEN '+ '''-''+' + 'CONVERT(VARCHAR(10),ISNULL(BT.[numDiscount],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numDiscountPaidInDays],0)) +' + ''' days early / ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentage],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentageIfPaidAfterDays],0)) +' + ''' days or later ''' +
								 ' WHEN ISNULL(BT.[numDiscount],0) > 0 THEN '+ '''-''+' + 'CONVERT(VARCHAR(10),ISNULL(BT.[numDiscount],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numDiscountPaidInDays],0)) +' + ''' days early ''' + 
								 ' WHEN ISNULL(BT.[numInterestPercentage],0) > 0  THEN +CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentage],0))+' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentageIfPaidAfterDays],0))+' + ''' days or later ''' +
								' END AS [vcTerms],
							ISNULL(BT.[numDiscount],0) AS [numDiscount],
							ISNULL(BT.[numInterestPercentage],0) AS [numInterest],
							0 AS tintLevel,
							GJD.numChartAcntId
		 FROM   dbo.General_Journal_Header GJH
				INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
															  AND GJH.numDomainId = GJD.numDomainId
				INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
				INNER JOIN [DivisionMaster] DM ON GJD.numCustomerID = DM.[numDivisionID]
				INNER JOIN CompanyInfo CO ON CO.numCompanyId = DM.numCompanyID
                Left JOIN AdditionalContactsInformation ADC ON ADC.numContactId=GJD.numContactId 
				LEFT OUTER JOIN [Currency] C ON GJD.[numCurrencyID] = C.[numCurrencyID]
				LEFT JOIN [dbo].[BillingTerms] AS BT ON [DM].[numBillingDays] = [BT].[numTermsID]
		 WHERE  GJH.numDomainId ='+ CONVERT(VARCHAR(15),@numDomainId) +'
				AND COA.vcAccountCode LIKE ''01010105%'' 
				AND ISNULL(numOppId,0)=0 
				AND ISNULL(numOppBizDocsId,0)=0 
				AND ISNULL(GJH.numDepositID,0)=0 
				AND ISNULL(GJH.numReturnID,0)=0
				AND (GJD.numCustomerId = ' + CAST(@numDivisionId AS VARCHAR) + ' OR ' + CAST(@numDivisionId AS VARCHAR) + '  = 0)
				AND (GJH.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
				AND CONVERT(DATE,GJH.[datEntry_Date]) <= ''' + CAST(@dtToDate AS VARCHAr) + ''''
   
	 SET @strCredit = ' UNION SELECT -1 [numOppId],
									CASE WHEN tintDepositePage=2 THEN ''Unapplied Payment'' ELSE ''Credit Memo'' END AS [vcPOppName],
									0 [tintOppType],
									0 [numOppBizDocsId],
									''''  [vcBizDocID],
									(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) * -1 AS TotalAmount,
									0 AmountPaid,
									(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) * -1 AS BalanceDue,
									[dbo].[FormatedDateFromDate](DM.dtDepositDate,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS dtDate,
									[dbo].[FormatedDateFromDate](DM.dtDepositDate,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate,
									0 bitBillingTerms,
									0 intBillingDays,
									DM.dtCreationDate AS [datCreatedDate],
									'''' [varCurrSymbol],
									dbo.fn_GetComapnyName(DM.numDivisionId) vcCompanyName,
									'''' vcContactName,
									'''' AS RecordOwner,
									'''' AS AssignedTo,
									'''' AS vcCustPhone,
									ISNULL(DM.numCurrencyID,0) AS [numCurrencyID],
									'''' as vcRefOrderNo,
									NULL, 
									-1 AS [numJournal_Id],isnull(numReturnHeaderID,0) as numReturnHeaderID,
									'''' AS [vcTerms],
									0 AS [numDiscount],
									0 AS [numInterest],
									0 AS tintLeve,
									0 AS numChartAcntId
					FROM  DepositMaster DM 
					WHERE DM.numDomainId='+ CONVERT(VARCHAR(15),@numDomainId) +' AND tintDepositePage IN(2,3)  
					AND ((ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) <> 0)
					AND (DM.numDivisionId = ' + CAST(@numDivisionId AS VARCHAR) + ' OR ' + CAST(@numDivisionId AS VARCHAR) + ' = 0)
					AND (DM.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
					AND CONVERT(DATE,DM.dtDepositDate) <= ''' + CAST(@dtToDate AS VARCHAr) + '''
					AND ISNULL(DM.numReturnHeaderID,0) = 0
					UNION SELECT -1 [numOppId],
									CASE WHEN tintDepositePage=2 THEN ''Unapplied Payment'' ELSE ''Credit Memo'' END AS [vcPOppName],
									0 [tintOppType],
									0 [numOppBizDocsId],
									''''  [vcBizDocID],
									(ISNULL(monDepositAmount,0) - ISNULL((SELECT SUM(DD.monAmountPaid) FROM DepositeDetails DD WHERE DD.numDepositID=DM.numDepositId AND DATEADD(MINUTE,-1 * ' + CAST(@ClientTimeZoneOffset AS VARCHAR) + ',DD.dtCreatedDate) <= ''' + CAST(@dtToDate AS VARCHAr) + '''),0)) * -1 AS TotalAmount,
									0 AmountPaid,
									(ISNULL(monDepositAmount,0) - ISNULL((SELECT SUM(DD.monAmountPaid) FROM DepositeDetails DD WHERE DD.numDepositID=DM.numDepositId AND DATEADD(MINUTE,-1 * ' + CAST(@ClientTimeZoneOffset AS VARCHAR) + ',DD.dtCreatedDate) <= ''' + CAST(@dtToDate AS VARCHAr) + '''),0)) * -1 AS BalanceDue,
									[dbo].[FormatedDateFromDate](DM.dtDepositDate,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS dtDate,
									[dbo].[FormatedDateFromDate](DM.dtDepositDate,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate,
									0 bitBillingTerms,
									0 intBillingDays,
									DM.dtCreationDate AS [datCreatedDate],
									'''' [varCurrSymbol],
									dbo.fn_GetComapnyName(DM.numDivisionId) vcCompanyName,
									'''' vcContactName,
									'''' AS RecordOwner,
									'''' AS AssignedTo,
									'''' AS vcCustPhone,
									ISNULL(DM.numCurrencyID,0) AS [numCurrencyID],
									'''' as vcRefOrderNo,
									NULL, 
									-1 AS [numJournal_Id],isnull(numReturnHeaderID,0) as numReturnHeaderID,
									'''' AS [vcTerms],
									0 AS [numDiscount],
									0 AS [numInterest],
									0 AS tintLeve,
									0 AS numChartAcntId
					FROM  DepositMaster DM 
					WHERE DM.numDomainId='+ CONVERT(VARCHAR(15),@numDomainId) +' AND tintDepositePage IN(2,3)  
					AND ((ISNULL(monDepositAmount,0) - ISNULL((SELECT SUM(DD.monAmountPaid) FROM DepositeDetails DD WHERE DD.numDepositID=DM.numDepositId AND DATEADD(MINUTE,-1 * ' + CAST(@ClientTimeZoneOffset AS VARCHAR) + ',DD.dtCreatedDate) <= ''' + CAST(@dtToDate AS VARCHAr) + '''),0)) <> 0)
					AND (DM.numDivisionId = ' + CAST(@numDivisionId AS VARCHAR) + ' OR ' + CAST(@numDivisionId AS VARCHAR) + ' = 0)
					AND (DM.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
					AND CONVERT(DATE,DM.dtDepositDate) <= ''' + CAST(@dtToDate AS VARCHAr) + '''
					AND ISNULL(DM.numReturnHeaderID,0) > 0
					UNION
					SELECT 
						-1 [numOppId]
						,''Refund'' AS [vcPOppName]
						,0 [tintOppType]
						,0 [numOppBizDocsId]
						,''''  [vcBizDocID]
						,(CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1 * GJD.numCreditAmt END) AS TotalAmount
						,0 AmountPaid
						,(CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1 * GJD.numCreditAmt END) AS BalanceDue
						,[dbo].[FormatedDateFromDate](GJH.datEntry_Date,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS dtDate
						,[dbo].[FormatedDateFromDate](GJH.datEntry_Date,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate
						,0 bitBillingTerms
						,0 intBillingDays
						,RH.dtCreatedDate AS [datCreatedDate]
						,'''' [varCurrSymbol]
						,dbo.fn_GetComapnyName(DIV.numDivisionId) vcCompanyName
						,'''' vcContactName
						,'''' AS RecordOwner
						,'''' AS AssignedTo
						,'''' AS vcCustPhone
						,ISNULL(DIV.numCurrencyID,0) AS [numCurrencyID]
						,'''' as vcRefOrderNo
						,NULL
						,GJH.numJournal_Id AS [numJournal_Id]
						,isnull(numReturnHeaderID,0) as numReturnHeaderID
						,'''' AS [vcTerms]
						,0 AS [numDiscount]
						,0 AS [numInterest]
						,0 AS tintLevel
						,GJD.numChartAcntId
					 FROM   dbo.ReturnHeader RH JOIN dbo.General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
					 JOIN [dbo].[DivisionMaster] AS DIV ON DIV.[numDivisionID] = RH.[numDivisionId]
					JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId 
					JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId AND COA.vcAccountCode LIKE ''0101010501''
					WHERE 
						RH.tintReturnType=4 
						AND RH.numDomainId=' + CAST(@numDomainId AS VARCHAR) + '  
						AND ISNULL(RH.numParentID,0) = 0
						AND ISNULL(RH.IsUnappliedPayment,0) = 0
						AND (monBizDocAmount > 0) AND ISNULL(RH.numBillPaymentIDRef,0)=0
						AND (RH.numDivisionId = ' + CAST(@numDivisionId AS VARCHAR) + ' OR ' + CAST(@numDivisionId AS VARCHAR) + ' = 0)
						AND (RH.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
						AND CONVERT(DATE,GJH.[datEntry_Date]) <= ''' + CAST(@dtToDate AS VARCHAr) + ''''
	            	
    IF (@vcFlag = '0+30')
      BEGIN
      
      SET @strSql =@strSql +
						'AND dateadd(DAY,CASE 
                                 --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                 WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                 ELSE 0
                               END,OB.dtFromDate) BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())
               AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                ELSE 0
                                                              END,OB.dtFromDate)'
                                                              
		SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())'
		
		SET @strCredit =@strCredit + 'AND DM.dtDepositDate BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())'
      END
    ELSE
      IF (@vcFlag = '30+60')
        BEGIN
         SET @strSql =@strSql +
                 'AND dateadd(DAY,CASE 
                                   --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                   WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                   ELSE 0
                                 END,OB.dtFromDate) BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())
                 AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                  --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                  WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                  ELSE 0
                                                                END,OB.dtFromDate)'
		SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())'
        
        SET @strCredit =@strCredit + 'AND DM.dtDepositDate BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())'
                 
        END
      ELSE
        IF (@vcFlag = '60+90')
          BEGIN
          SET @strSql =@strSql +
                   'AND dateadd(DAY,CASE 
                                     --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                     WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                     ELSE 0
                                   END,OB.dtFromDate) BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                   AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                    --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                    WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                    ELSE 0
                                                                  END,OB.dtFromDate)'
			SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
			
			SET @strCredit =@strCredit + 'AND DM.dtDepositDate BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
			
          END
        ELSE
          IF (@vcFlag = '90+')
            BEGIN
             SET @strSql =@strSql +
                     'AND dateadd(DAY,CASE 
                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                       ELSE 0
                                     END,OB.dtFromDate) > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                     AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                      --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                      WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                      ELSE 0
                                                                    END,OB.dtFromDate)'
			SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date  > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
            
            SET @strCredit =@strCredit + 'AND DM.dtDepositDate  > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
                     
            END
          ELSE
            IF (@vcFlag = '0-30')
              BEGIN
               SET @strSql =@strSql +
                       'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                       ELSE 0
                                                                     END,OB.dtFromDate)
                       AND datediff(DAY,dateadd(DAY,CASE 
                                                      --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                      WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                      ELSE 0
                                                    END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
				SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
				
				SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
				
              END
            ELSE
              IF (@vcFlag = '30-60')
                BEGIN
                  SET @strSql =@strSql +
                         'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                         --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                         WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                         ELSE 0
                                                                       END,OB.dtFromDate)
                         AND datediff(DAY,dateadd(DAY,CASE 
                                                        --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                        WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                        ELSE 0
                                                      END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60'
                SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60 '
                
                SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60 '
                
                END
              ELSE
                IF (@vcFlag = '60-90')
                  BEGIN
					SET @strSql =@strSql +
                           'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                           --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                           WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                           ELSE 0
                                                                         END,OB.dtFromDate)
                           AND datediff(DAY,dateadd(DAY,CASE 
                                                          --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                          WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                          ELSE 0
                                                        END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90'
					SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 '
                    
                    SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 '
                           
                  END
                ELSE
                  IF (@vcFlag = '90-')
                    BEGIN
                      SET @strSql =@strSql +
                             'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                             --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                             WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																															   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                             ELSE 0
                                                                           END,OB.dtFromDate)
                             AND datediff(DAY,dateadd(DAY,CASE 
                                                            --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                            WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                            ELSE 0
                                                          END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) > 90'
					SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) > 90'
                     
                    SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) > 90'   
                    END
     
	        
    SET @strSql =@strSql + @strSql1 +  @strCredit

	

    SET @strSql = CONCAT(@strSql,' SELECT *,CASE WHEN DATEDIFF (day ,[dbo].[FormatedDateFromDate](DueDate,',@numDomainID,') ,GETDATE()) > 0 
										  THEN DATEDIFF (day ,[dbo].[FormatedDateFromDate](DueDate,',@numDomainID,') ,GETDATE()) 
										  ELSE NULL 
									 END AS DaysLate,
									 CASE WHEN DATEDIFF (day ,[dbo].[FormatedDateFromDate](DueDate,',@numDomainID,') ,GETDATE()) > 0 
										  THEN + ''',' Past Due (', ''' + CAST(DATEDIFF (day ,[dbo].[FormatedDateFromDate](DueDate,',@numDomainID,') ,GETDATE()) AS VARCHAR(10))+''',')','''
										  ELSE NULL 
									 END AS [Status(DaysLate)],
									 CASE WHEN ',@baseCurrency,' <> numCurrencyID THEN 2 ELSE 1 END AS CurrencyCount,
									 dbo.fn_GetChart_Of_AccountsName(numChartAcntId) vcARAccount
							FROM #TempRecords WHERE (ISNULL(BalanceDue,0) <> 0 or [numOppId]=-1)')
	
	IF ISNULL(@vcRegularSearch,'') <> ''
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND ',@vcRegularSearch)
	END
 
   SET @strSql = CONCAT(@strSql,' DROP TABLE #TempRecords')
   
	PRINT CAST(@strSql AS NTEXt)
	EXEC(@strSql)
	        
     
  END
/****** Object:  StoredProcedure [dbo].[USP_GetChildItemsForKitsAss]    Script Date: 07/26/2008 16:16:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetChildItemsForKitsAss')
DROP PROCEDURE USP_GetChildItemsForKitsAss
GO
CREATE PROCEDURE [dbo].[USP_GetChildItemsForKitsAss]                             
	@numKitId AS NUMERIC(18,0)                            
	,@numWarehouseItemID NUMERIC(18,0)
	,@bitApplyFilter BIT=0
AS                            
BEGIN

	WITH CTE
	(
		ID
		,numItemDetailID
		,numParentID
		,numItemKitID
		,numItemCode
		,vcItemName
		,vcSKU
		,bitKitParent
		,monAverageCost
		,numAssetChartAcntId
		,txtItemDesc
		,numQtyItemsReq
		,numOppChildItemID
		,charItemType
		,ItemType
		,StageLevel
		,monListPriceC
		,numBaseUnit
		,numCalculatedQty
		,numIDUOMId,sintOrder
		,numRowNumber
		,RStageLevel
		,numUOMQuantity
		,tintView
		,vcFields
	)
	AS
	(
		SELECT 
			CAST(CONCAT('#0#',ISNULL(Dtl.numItemDetailID,'0'),'#') AS VARCHAR(1000))
			,Dtl.numItemDetailID
			,CAST('' AS VARCHAR(1000))
			,convert(NUMERIC(18,0),0)
			,numItemCode
			,CONCAT(vcItemName,(CASE WHEN LEN(ISNULL(vcSKU,'')) > 0 THEN CONCAT(' (',vcSKU,')') ELSE '' END)) vcItemName
			,vcSKU
			,ISNULL(Item.bitKitParent,0)
			,(CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE monAverageCost END)
			,numAssetChartAcntId
			,ISNULL(Dtl.vcItemDesc,txtItemDesc) AS txtItemDesc
			,DTL.numQtyItemsReq
			,0 as numOppChildItemID
			,case when charItemType='P' then CONCAT('Inventory',CASE WHEN ISNULL(item.bitKitParent,0)=1 THEN ' (Kit)' WHEN ISNULL(item.bitAssembly,0)=1 THEN ' (Assembly)' WHEN ISNULL(item.numItemGroup,0) > 0 AND (ISNULL(item.bitMatrix,0)=1 OR EXISTS (SELECT numItemGroupDTL FROM ItemGroupsDTL WHERE numItemGroupID=item.numItemGroup AND tintType=2)) THEN ' (Matrix)' ELSE '' END) when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory' end as charItemType
			,charItemType as ItemType                            
			,1
			,Item.monListPrice
			,ISNULL(numBaseUnit,0)
			,CAST(DTL.numQtyItemsReq AS FLOAT) AS numCalculatedQty
			,ISNULL(Dtl.numUOMId,ISNULL(numBaseUnit,0)) AS numIDUOMId
			,ISNULL(sintOrder,0) sintOrder
			,ROW_NUMBER() OVER (ORDER BY ISNULL(sintOrder,0)) AS numRowNumber
			,0
			,(DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(DTL.numUOMID,item.numItemCode,item.numDomainID,item.numBaseUnit),1))
			,ISNULL(Dtl.tintView,1) tintView
			,ISNULL(Dtl.vcFields,'') vcFields
		FROM 
			Item                                
		INNER JOIN 
			ItemDetails Dtl 
		ON 
			numChildItemID=numItemCode
		WHERE 
			numItemKitID=@numKitId 
		UNION ALL
		SELECT 
			CAST(CONCAT(c.ID,'-#',(c.StageLevel + 1),'#',ISNULL(Dtl.numItemDetailID,'0'),'#') AS VARCHAR(1000))
			,Dtl.numItemDetailID
			,C.ID
			,Dtl.numItemKitID
			,i.numItemCode
			,CONCAT(i.vcItemName,(CASE WHEN LEN(ISNULL(i.vcSKU,'')) > 0 THEN CONCAT(' (',i.vcSKU,')') ELSE '' END)) vcItemName
			,i.vcSKU
			,ISNULL(i.bitKitParent,0)
			,(CASE WHEN ISNULL(i.bitVirtualInventory,0) = 1 THEN 0 ELSE i.monAverageCost END)
			,i.numAssetChartAcntId
			,ISNULL(Dtl.vcItemDesc,i.txtItemDesc) AS txtItemDesc
			,DTL.numQtyItemsReq
			,0 as numOppChildItemID
			,CASE WHEN i.charItemType='P' THEN CONCAT('Inventory',CASE WHEN ISNULL(i.bitKitParent,0)=1 THEN ' (Kit)' WHEN ISNULL(i.bitAssembly,0)=1 THEN ' (Assembly)' WHEN ISNULL(i.numItemGroup,0) > 0 AND (ISNULL(i.bitMatrix,0)=1 OR EXISTS (SELECT numItemGroupDTL FROM ItemGroupsDTL WHERE numItemGroupID=i.numItemGroup AND tintType=2)) THEN ' (Matrix)' ELSE '' END) when i.charItemType='S' then 'Service' when i.charItemType='A' then 'Accessory' when i.charItemType='N' then 'Non-Inventory' end as charItemType
			,i.charItemType as ItemType                            
			,c.StageLevel + 1
			,i.monListPrice,ISNULL(i.numBaseUnit,0),CAST((DTL.numQtyItemsReq * c.numCalculatedQty) AS FLOAT) AS numCalculatedQty
			,ISNULL(Dtl.numUOMId,ISNULL(i.numBaseUnit,0)) AS numIDUOMId
			,ISNULL(Dtl.sintOrder,0) sintOrder
			,ROW_NUMBER() OVER (ORDER BY ISNULL(Dtl.sintOrder,0)) AS numRowNumber
			,0
			,(DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(DTL.numUOMID,i.numItemCode,i.numDomainID,i.numBaseUnit),1))
			,ISNULL(Dtl.tintView,1) tintView
			,ISNULL(Dtl.vcFields,'') vcFields
		FROM
			Item i                               
		INNER JOIN 
			ItemDetails Dtl
		ON 
			Dtl.numChildItemID=i.numItemCode
		INNER JOIN 
			CTE c 
		ON 
			Dtl.numItemKitID = c.numItemCode 
		WHERE 
			Dtl.numChildItemID!=@numKitId
	)

	SELECT * INTO #temp FROM CTE

	;WITH Final AS 
	(
		SELECT 
			*
			,1 AS RStageLevel1 
		FROM 
			#temp 
		WHERE 
			numitemcode NOT IN (SELECT numItemKitID FROM #temp)
		UNION ALL
		SELECT 
			t.*
			,c.RStageLevel1 + 1 AS RStageLevel1 
		FROM 
			#temp t 
		JOIN 
			Final c 
		ON 
			t.numitemcode=c.numItemKitID
	)


	UPDATE 
		t 
	SET 
		t.RStageLevel=f.RStageLevel 
	FROM 
		#temp t,(SELECT numitemcode,numItemKitID,MAX(RStageLevel1) AS RStageLevel FROM Final GROUP BY numitemcode,numItemKitID) f
	WHERE 
		t.numitemcode=f.numitemcode 
		AND t.numItemKitID=f.numItemKitID 


	IF ISNULL(@numWarehouseItemID,0) > 0
	BEGIN
		DECLARE @numWarehouseID NUMERIC(18,0)
		SELECT @numWarehouseID = numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID=@numWarehouseItemID
		IF(@bitApplyFilter=0)
		BEGIN
		IF EXISTS 
		(
			SELECT
				c.numItemCode
			FROM
				#temp c 
			WHERE
				c.ItemType='P'
				AND (SELECT COUNT(*) FROM WareHouseItems WHERE numItemID = c.numItemCode AND numWareHouseID=@numWarehouseID) = 0
		)
		BEGIN
			RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			RETURN
		END
		END

		SELECT DISTINCT 
			c.*
			,CASE WHEN isnull(WI.numWareHouseItemID,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE c.monListPriceC END monListPrice
			,CONVERT(VARCHAR(30),CASE WHEN isnull(WI.numWareHouseItemID,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE c.monListPriceC END)+'/'+isnull(UOM.vcUnitName,'Units') as UnitPrice
			,UOM.vcUnitName
			,ISNULL(UOM.numUOMId,0) numUOMId
			,WI.numItemID,vcWareHouse
			,IDUOM.vcUnitName AS vcIDUnitName
			,dbo.fn_UOMConversion(IDUOM.numUOMId,0,IDUOM.numDomainId,UOM.numUOMId) AS UOMConversionFactor
			,CAST(c.numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId) AS FLOAT) AS numConQty
			,(c.numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId)) * c.monAverageCost AS monAverageCostTotalForQtyRequired
			,ISNULL(WI.[numWareHouseItemID],0) AS numWarehouseItmsID
			,ISNULL(WI.[numWareHouseItemID],0) AS numWareHouseItemID
			,ISNULL(numOnHand,0) numOnHand
			,ISNULL(numOnHand,0) + isnull(numAllocation,0) AS numAvailable
			,isnull(numOnOrder,0) numOnOrder
			,isnull(numReorder,0) numReorder
			,isnull(numAllocation,0) numAllocation
			,isnull(numBackOrder,0) numBackOrder
		INTO
			#TEMPFinal
		FROM 
			#temp c  
		LEFT JOIN 
			UOM 
		ON 
			UOM.numUOMId=c.numBaseUnit
		LEFT JOIN 
			UOM IDUOM 
		ON 
			IDUOM.numUOMId=c.numIDUOMId
		OUTER APPLY
		(
			SELECT TOP 1
				numWareHouseItemID
				,numOnHand
				,numOnOrder
				,numReorder
				,numAllocation
				,numBackOrder
				,numItemID
				,vcWareHouse
				,monWListPrice
			FROM
				WareHouseItems
			LEFT JOIN
				Warehouses W 
			ON
				W.numWareHouseID=WareHouseItems.numWareHouseID
			WHERE 
				WareHouseItems.numItemID = c.numItemCode 
				AND WareHouseItems.numWareHouseID=@numWarehouseID
			ORDER BY
				WareHouseItems.numWareHouseID,numWareHouseItemID
		) WI
		ORDER BY 
			c.StageLevel

		UPDATE
			T
		SET
			T.numOnHand = ISNULL((SELECT MIN(FLOOR(TF.numOnHand/TF.numQtyItemsReq)) FROM #TEMPFinal TF WHERE TF.numParentID = T.ID),0)
			,T.numAvailable = ISNULL((SELECT MIN(FLOOR(TF.numAvailable/TF.numQtyItemsReq)) FROM #TEMPFinal TF WHERE TF.numParentID = T.ID),0)
			,T.numBackOrder = 0
			,T.numOnOrder=0
		FROM
			#TEMPFinal T
		WHERE
			ISNULL(T.bitKitParent,0) = 1

		SELECT * FROM #TEMPFinal

		DROP TABLE #TEMPFinal
	END
	ELSE IF @bitApplyFilter=1
	BEGIN
		SELECT DISTINCT 
			c.*
			,CASE WHEN isnull(WI.numWareHouseItemID,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE c.monListPriceC END monListPrice
			,CONVERT(VARCHAR(30),CASE WHEN isnull(WI.numWareHouseItemID,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE c.monListPriceC END)+'/'+isnull(UOM.vcUnitName,'Units') as UnitPrice
			,UOM.vcUnitName
			,ISNULL(UOM.numUOMId,0) numUOMId
			,WI.numItemID,vcWareHouse
			,IDUOM.vcUnitName AS vcIDUnitName
			,dbo.fn_UOMConversion(IDUOM.numUOMId,0,IDUOM.numDomainId,UOM.numUOMId) AS UOMConversionFactor
			,CAST(c.numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId) AS FLOAT) AS numConQty
			,(c.numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId)) * c.monAverageCost AS monAverageCostTotalForQtyRequired
			,ISNULL(WI.[numWareHouseItemID],0) AS numWarehouseItmsID
			,ISNULL(WI.[numWareHouseItemID],0) AS numWareHouseItemID
			,ISNULL(numOnHand,0) numOnHand
			,ISNULL(numOnHand,0) + isnull(numAllocation,0) AS numAvailable
			,isnull(numOnOrder,0) numOnOrder
			,isnull(numReorder,0) numReorder
			,isnull(numAllocation,0) numAllocation
			,isnull(numBackOrder,0) numBackOrder
		INTO
			#TEMPFinal1
		FROM 
			#temp c  
		LEFT JOIN 
			UOM 
		ON 
			UOM.numUOMId=c.numBaseUnit
		LEFT JOIN 
			UOM IDUOM 
		ON 
			IDUOM.numUOMId=c.numIDUOMId
		OUTER APPLY
		(
			SELECT TOP 1
				1 AS numWareHouseItemID
				,SUM(numOnHand) AS numOnHand
				,SUM(numOnOrder) AS numOnOrder
				,0 AS numReorder
				,0 AS numAllocation
				,0 AS numBackOrder
				,0 AS numItemID
				,'' AS vcWareHouse
				,0 AS monWListPrice
			FROM
				WareHouseItems
			LEFT JOIN
				Warehouses W 
			ON
				W.numWareHouseID=WareHouseItems.numWareHouseID
			WHERE 
				WareHouseItems.numItemID = c.numItemCode 
			ORDER BY
				numWareHouseItemID
			
		) WI
		ORDER BY 
			c.StageLevel

		UPDATE
			T
		SET
			T.numOnHand = ISNULL((SELECT MIN(FLOOR(TF.numOnHand/TF.numQtyItemsReq)) FROM #TEMPFinal1 TF WHERE TF.numParentID = T.ID),0)
			,T.numAvailable = ISNULL((SELECT MIN(FLOOR(TF.numAvailable/TF.numQtyItemsReq)) FROM #TEMPFinal1 TF WHERE TF.numParentID = T.ID),0)
			,T.numBackOrder = 0
			,T.numOnOrder=0
		FROM
			#TEMPFinal1 T
		WHERE
			ISNULL(T.bitKitParent,0) = 1

		SELECT * FROM #TEMPFinal1

		DROP TABLE #TEMPFinal1
	END
	ELSE
	BEGIN
		SELECT DISTINCT 
			c.*
			,UOM.vcUnitName
			,ISNULL(UOM.numUOMId,0) numUOMId
			,IDUOM.vcUnitName AS vcIDUnitName
			,dbo.fn_UOMConversion(IDUOM.numUOMId,0,IDUOM.numDomainId,UOM.numUOMId) AS UOMConversionFactor
			,c.numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId) AS numConQty
			,(c.numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId)) * c.monAverageCost AS monAverageCostTotalForQtyRequired
			,c.sintOrder
			,c.numUOMQuantity
		FROM 
			#temp c  
		LEFT JOIN 
			UOM 
		ON 
			UOM.numUOMId=c.numBaseUnit
		LEFT JOIN 
			UOM IDUOM 
		ON 
			IDUOM.numUOMId=c.numIDUOMId
		ORDER BY 
			c.StageLevel
	END
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p' AND NAME ='USP_GetDomainDetailsForPriceMargin')
DROP PROCEDURE USP_GetDomainDetailsForPriceMargin
GO
CREATE PROCEDURE [dbo].[USP_GetDomainDetailsForPriceMargin]                                          
	@numDomainID AS NUMERIC(9) = 0,
	@numListItemID AS NUMERIC(9) = 0                                           
AS  

DECLARE @listIds VARCHAR(MAX)
SELECT @listIds = COALESCE(@listIds+',' ,'') + CAST(numUserID AS VARCHAR(100)) FROM UnitPriceApprover WHERE numDomainID = @numDomainID

IF @numListItemID > 0
BEGIN
	SELECT     
		ISNULL(AP.numAbovePercent,0) AS numAbovePercent,
		ISNULL(AP.numBelowPercent,0) AS numBelowPercent,
		ISNULL(AP.numBelowPriceField,0) AS numBelowPriceField,
		ISNULL(AP.bitCostApproval,0) bitCostApproval,
		ISNULL(AP.bitListPriceApproval,0) bitListPriceApproval,
		ISNULL(AP.bitMarginPriceViolated,0) bitMarginPriceViolated,
		ISNULL(numListItemID,0) AS numListItemID,
		@listIds AS vcUnitPriceApprover
	FROM Domain D  
		LEFT JOIN ApprovalProcessItemsClassification AP ON AP.numDomainID = D.numDomainID
	WHERE (D.numDomainID=@numDomainID OR @numDomainID=-1)
		AND numListItemID = @numListItemID 
END
ELSE
BEGIN
	SELECT     
		ISNULL(AP.numAbovePercent,0) AS numAbovePercent,
		ISNULL(AP.numBelowPercent,0) AS numBelowPercent,
		ISNULL(AP.numBelowPriceField,0) AS numBelowPriceField,
		ISNULL(AP.bitCostApproval,0) bitCostApproval,
		ISNULL(AP.bitListPriceApproval,0) bitListPriceApproval,
		ISNULL(AP.bitMarginPriceViolated,0) bitMarginPriceViolated,
		ISNULL(numListItemID,0) AS numListItemID,
		@listIds AS vcUnitPriceApprover
	FROM Domain D  
		LEFT JOIN ApprovalProcessItemsClassification AP ON AP.numDomainID = D.numDomainID
	WHERE (D.numDomainID=@numDomainID OR @numDomainID=-1)
END

/****** Object:  StoredProcedure [dbo].[usp_getDynamicFormList]    Script Date: 07/26/2008 16:17:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Modified By: Anoop Jayaraj
   -- Created By: Debasish Nag              
-- Created On: 07/08/2005              
-- Purpose: To get a list of dynamic form names and their details              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdynamicformlist')
DROP PROCEDURE usp_getdynamicformlist
GO
CREATE PROCEDURE [dbo].[usp_getDynamicFormList]              
@numDomainID as numeric(9)=0,
@tintFlag AS TINYINT=0,
@bitWorkFlow as bit=0,
@bitAllowGridColor as bit=0,
@numBizFormModuleID as INT=0
as              
              
begin     
IF @bitAllowGridColor=1
BEGIn
	Select DM.numFormId, vcFormName, cCustomFieldsAssociated, cAOIAssociated
			from DynamicFormMaster DM where isnull(bitAllowGridColor,0)=@bitAllowGridColor order by DM.numFormId
END

ELSE IF @bitWorkFlow=1
BEGIn
	Select DM.numFormId, vcFormName, cCustomFieldsAssociated, cAOIAssociated
			from DynamicFormMaster DM where isnull(bitWorkFlow,0)=@bitWorkFlow order by DM.numFormId
END

Else IF  @tintFlag =1 OR @tintFlag=3
BEGIN
	Select DM.numFormId, vcFormName, cCustomFieldsAssociated, cAOIAssociated
from DynamicFormMaster DM 
where tintFlag=@tintFlag 
order by DM.numFormId
END 
Else IF  @tintFlag =6
BEGIN
	Select DM.numFormId, vcFormName, cCustomFieldsAssociated, cAOIAssociated
from DynamicFormMaster DM 
where tintFlag=1  AND DM.numFormId IN (34,35,12,36,10,13,40,41,38,39)
order by DM.numFormId
END
ELSE IF(@tintFlag=5)
BEGIN
	Select DM.numFormId, vcFormName, cCustomFieldsAssociated, cAOIAssociated, isNull(vcAdditionalParam,'') as vcAdditionalParam, numGrpId    
from DynamicFormMaster DM 
left join DynamicFormMasterParam  DMP 
on DMP.numFormID=DM.numFormID and DMP.numDomainID=@numDomainID            
where bitDeleted = 0 AND (numBizFormModuleID = @numBizFormModuleID OR ISNULL(@numBizFormModuleID,0) = 0) 
AND tintFlag=0 /*tintFlag=1 are internal forms need not to show in bizform wizard*/
AND DM.numFormId IN (34,35,12,36,10,13,40,41,38,39)
order by DM.numFormId
END
ELSE
BEGIN       
Select DM.numFormId, vcFormName, cCustomFieldsAssociated, cAOIAssociated, isNull(vcAdditionalParam,'') as vcAdditionalParam, numGrpId, DM.tintPageType    
from DynamicFormMaster DM 
left join DynamicFormMasterParam  DMP 
on DMP.numFormID=DM.numFormID and DMP.numDomainID=@numDomainID            
where bitDeleted = 0 AND (numBizFormModuleID = @numBizFormModuleID OR ISNULL(@numBizFormModuleID,0) = 0) 
AND tintFlag=0 /*tintFlag=1 are internal forms need not to show in bizform wizard*/
order by DM.numFormId
END              
end
GO


--Created by Anoop Jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetFieldRelationship')
DROP PROCEDURE USP_GetFieldRelationship
GO
CREATE PROCEDURE USP_GetFieldRelationship
@byteMode TINYINT,
@numFieldRelID NUMERIC(18,0),
@numDomainID NUMERIC(18,0),
@numModuleID NUMERIC(18,0),
@numPrimaryListID NUMERIC(18,0),
@numPrimaryListItemID NUMERIC(18,0),
@numSecondaryListID NUMERIC(18,0),
@bitTaskRelation BIT
as
BEGIN
	IF @byteMode=1
	BEGIN
		IF(@bitTaskRelation=1)
		BEGIN
			SELECT 
				FR.numFieldRelID,
				FR.numPrimaryListID,
				SD.vcStageName as RelationshipName
			FROM
				FieldRelationship AS FR
			LEFT JOIN
				StagePercentageDetails AS SD
			ON
				FR.numPrimaryListID=SD.numStageDetailsId
			WHERE
				FR.numDomainId=@numDomainID
				AND ISNULL(FR.bitTaskRelation,0)=1
				AND 1 = (CASE WHEN ISNULL(@numModuleID,0) = 0 THEN 1 ELSE (CASE WHEN ISNULL(numModuleID,0) = @numModuleID THEN 1 ELSE 0 END) END)
				AND 1 = (CASE WHEN ISNULL(@numPrimaryListID,0) = 0 THEN 1 ELSE (CASE WHEN ISNULL(FR.numPrimaryListID,0) = @numPrimaryListID THEN 1 ELSE 0 END) END)
				AND 1 = (CASE WHEN ISNULL(@numSecondaryListID,0) = 0 THEN 1 ELSE (CASE WHEN ISNULL(FR.numSecondaryListID,0) = @numSecondaryListID THEN 1 ELSE 0 END) END)
		END
		ELSE
		BEGIN
			SELECT 
				numFieldRelID,L1.vcListName +' - ' + L2.vcListName as RelationshipName 
			FROM 
				FieldRelationship 
			JOIN 
				ListMaster L1
			ON 
				L1.numListID=numPrimaryListID
			JOIN 
				ListMaster L2
			ON
				L2.numListID=numSecondaryListID
			WHERE 
				FieldRelationship.numDomainID=@numDomainID 
				AND ISNULL(FieldRelationship.numModuleID,0) <> 14
				AND ISNULL(bitTaskRelation,0)=0
				AND 1 = (CASE WHEN ISNULL(@numModuleID,0) = 0 THEN 1 ELSE (CASE WHEN ISNULL(FieldRelationship.numModuleID,0) = @numModuleID THEN 1 ELSE 0 END) END)
				AND 1 = (CASE WHEN ISNULL(@numPrimaryListID,0) = 0 THEN 1 ELSE (CASE WHEN ISNULL(FieldRelationship.numPrimaryListID,0) = @numPrimaryListID THEN 1 ELSE 0 END) END)
				AND 1 = (CASE WHEN ISNULL(@numSecondaryListID,0) = 0 THEN 1 ELSE (CASE WHEN ISNULL(FieldRelationship.numSecondaryListID,0) = @numSecondaryListID THEN 1 ELSE 0 END) END)
			UNION
			SELECT 
				numFieldRelID,L1.vcItemName +' - ' + L2.vcItemName as RelationshipName 
			FROM 
				FieldRelationship 
			JOIN 
				Item L1
			ON 
				L1.numItemCode=numPrimaryListID
			JOIN 
				Item L2
			ON 
				L2.numItemCode=numSecondaryListID
			WHERE 
				FieldRelationship.numDomainID=@numDomainID
				AND ISNULL(FieldRelationship.numModuleID,0) = 14
				AND ISNULL(bitTaskRelation,0) = 0
				AND 1 = (CASE WHEN ISNULL(@numModuleID,0) = 0 THEN 1 ELSE (CASE WHEN ISNULL(numModuleID,0) = @numModuleID THEN 1 ELSE 0 END) END)
				AND 1 = (CASE WHEN ISNULL(@numPrimaryListID,0) = 0 THEN 1 ELSE (CASE WHEN ISNULL(FieldRelationship.numPrimaryListID,0) = @numPrimaryListID THEN 1 ELSE 0 END) END)
				AND 1 = (CASE WHEN ISNULL(@numSecondaryListID,0) = 0 THEN 1 ELSE (CASE WHEN ISNULL(FieldRelationship.numSecondaryListID,0) = @numSecondaryListID THEN 1 ELSE 0 END) END)
		END
	END
	ELSE IF @byteMode=2
	BEGIN
		SELECT 
			numFieldRelID
			,ISNULL(FieldRelationship.numModuleID,0) numModuleID
			,numPrimaryListID
			,numSecondaryListID
			,L1.vcListName +' - ' + L2.vcListName AS RelationshipName
			,L1.vcListName AS Item1
			,L2.vcListName AS Item2 
		FROM 
			FieldRelationship 
		JOIN 
			ListMaster L1
		ON 
			L1.numListID=numPrimaryListID
		JOIN 
			ListMaster L2
		ON 
			L2.numListID=numSecondaryListID
		WHERE 
			numFieldRelID=@numFieldRelID 
			AND ISNULL(FieldRelationship.numModuleID,0) <> 14 
			AND ISNULL(bitTaskRelation,0)=0
		UNION
		SELECT 
			numFieldRelID
			,ISNULL(numModuleID,0) numModuleID
			,numPrimaryListID
			,numSecondaryListID
			,L1.vcItemName +' - ' + L2.vcItemName AS RelationshipName
			,L1.vcItemName as Item1
			,L2.vcItemName as Item2 
		FROM 
			FieldRelationship 
		JOIN 
			Item L1
		ON 
			L1.numItemCode=numPrimaryListID
		JOIN 
			Item L2
		ON 
			L2.numItemCode=numSecondaryListID
		WHERE 
			numFieldRelID=@numFieldRelID 
			AND ISNULL(numModuleID,0) = 14
			AND ISNULL(bitTaskRelation,0)=0

		IF @bitTaskRelation=1
		BEGIN
			SELECT 
				FieldRelationship.numFieldRelID
				,numFieldRelDTLID,SLI1.vcTaskName as PrimaryListItem
				,SLI2.vcTaskName as SecondaryListItem 
			FROM 
				FieldRelationship
			JOIN 
				FieldRelationshipDTL 
			ON 
				FieldRelationshipDTL.numFieldRelID=FieldRelationship.numFieldRelID
			JOIN 
				StagePercentageDetailsTask SLI1
			ON 
				SLI1.numTaskId=numPrimaryListItemID
			JOIN 
				StagePercentageDetailsTask SLI2
			ON 
				SLI2.numTaskId=numSecondaryListItemID
			WHERE 
				FieldRelationship.numFieldRelID=@numFieldRelID 
				AND numPrimaryListItemID=@numPrimaryListItemID 
				AND ISNULL(bitTaskRelation,0)=1
		END
		ELSE
		BEGIN
			SELECT 
				FieldRelationship.numFieldRelID
				,numFieldRelDTLID
				,L1.vcData AS PrimaryListItem
				,L2.vcData AS SecondaryListItem 
			FROM 
				FieldRelationship
			JOIN 
				FieldRelationshipDTL 
			ON 
				FieldRelationshipDTL.numFieldRelID=FieldRelationship.numFieldRelID
			JOIN 
				ListDetails L1
			ON 
				L1.numListItemID=numPrimaryListItemID
			JOIN 
				ListDetails L2
			ON 
				L2.numListItemID=numSecondaryListItemID
			WHERE 
				FieldRelationship.numFieldRelID=@numFieldRelID 
				AND (ISNULL(@numPrimaryListItemID,0) = 0 OR numPrimaryListItemID=@numPrimaryListItemID)
				AND ISNULL(numModuleID,0) <> 14
				AND ISNULL(bitTaskRelation,0)=0
			UNION
			SELECT 
				FieldRelationship.numFieldRelID
				,numFieldRelDTLID
				,L1.vcItemName AS PrimaryListItem
				,L2.vcItemName AS SecondaryListItem 
			FROM 
				FieldRelationship
			JOIN 
				FieldRelationshipDTL 
			ON 
				FieldRelationshipDTL.numFieldRelID=FieldRelationship.numFieldRelID
			JOIN 
				Item L1
			ON 
				L1.numItemCode=numPrimaryListItemID
			JOIN 
				Item L2
			ON 
				L2.numItemCode=numSecondaryListItemID
			WHERE 
				FieldRelationship.numFieldRelID=@numFieldRelID 
				AND (ISNULL(@numPrimaryListItemID,0) = 0 OR numPrimaryListItemID=@numPrimaryListItemID)
				AND ISNULL(numModuleID,0) = 14
				AND ISNULL(bitTaskRelation,0)=0
				AND ISNULL(numSecondaryListItemID,0) <> -1
			UNION
			SELECT 
				FieldRelationship.numFieldRelID
				,numFieldRelDTLID
				,L1.vcItemName AS PrimaryListItem
				,'Do not display' AS SecondaryListItem 
			FROM 
				FieldRelationship
			JOIN 
				FieldRelationshipDTL 
			ON 
				FieldRelationshipDTL.numFieldRelID=FieldRelationship.numFieldRelID
			JOIN 
				Item L1
			ON 
				L1.numItemCode=numPrimaryListItemID
			WHERE 
				FieldRelationship.numFieldRelID=@numFieldRelID 
				AND (ISNULL(@numPrimaryListItemID,0) = 0 OR numPrimaryListItemID=@numPrimaryListItemID)
				AND ISNULL(numModuleID,0) = 14
				AND ISNULL(bitTaskRelation,0)=0
				AND ISNULL(numSecondaryListItemID,0) = -1
		end

	end
	else if @byteMode=3
	begin


		Select L2.numListItemID,L2.vcData as SecondaryListItem   from FieldRelationship
		Join FieldRelationshipDTL 
		on FieldRelationshipDTL.numFieldRelID=FieldRelationship.numFieldRelID
		Join ListDetails L2
		on L2.numListItemID=numSecondaryListItemID
		where FieldRelationship.numDomainID=@numDomainID and numPrimaryListItemID=@numPrimaryListItemID
		and FieldRelationship.numSecondaryListID=@numSecondaryListID AND ISNULL(bitTaskRelation,0)=0
	end
END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetListMaster]    Script Date: 07/26/2008 16:17:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---created by anoop jayaraj  
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getlistmaster' ) 
    DROP PROCEDURE usp_getlistmaster
GO
CREATE PROCEDURE [dbo].[USP_GetListMaster]
    @numDomainID AS NUMERIC(9),
    @numModuleID AS NUMERIC(9) = 0,
    @bitCustom BIT = 0
AS 
BEGIN
	DECLARE @strSql VARCHAR(MAX)
    IF @bitCustom = 0 
    BEGIN
        SELECT  
			LM.numListID,
			(CASE WHEN (@numModuleID = 8 AND ISNULL(CLM.Loc_name,'') <> '') THEN CONCAT(LM.vcListName,' (',CLM.Loc_name,')') ELSE LM.vcListName END) AS vcListName 
		FROM    
			listmaster LM
		LEFT JOIN
			dbo.CFW_Fld_Master CFM
		ON
			CFM.numDomainID = LM.numDomainID 
			AND CFM.numlistid= LM.numListID 
		LEFT JOIN
			CFW_Loc_Master CLM    
		ON
			CFM.Grp_id = CLM.Loc_Id
        WHERE   
			(LM.numdomainID = @numDomainID OR LM.bitFlag = 1)
			AND ISNULL(LM.bitDeleted, 0) = 0 
			AND ( ISNULL(LM.numModuleID, 0) = @numModuleID OR @numModuleID = 0 )
        ORDER BY 
			vcListName  	
    END
    ELSE
	BEGIN
		SET @strSql = 'SELECT  L.numListID,L.vcListName FROM listmaster L'
		SET @strSql = @strSql +' WHERE (L.numdomainID = '+ CONVERT(VARCHAR,@numDomainID)+' OR L.bitFlag = 1) AND ISNULL(L.bitDeleted, 0) = 0 AND (ISNULL(L.numModuleID, 0) = '+ CONVERT(VARCHAR,@numModuleID)+''
		Set @strSql = @strSql +' OR  L.numModuleID = 0 )'
		SET @strSql = @strSql +   ' UNION '
		SET @strSql = @strSql +   '(SELECT 
										CFM.numListID,
										(CASE WHEN '+ CONVERT(VARCHAR,@numModuleID)+' = 8 AND ISNULL(CLM.Loc_name,'''') <> '''' THEN CONCAT(LM.vcListName,'' ('',CLM.Loc_name,'')'') ELSE LM.vcListName END) AS vcListName
									FROM 
										dbo.CFW_Fld_Master CFM 
									LEFT JOIN
										CFW_Loc_Master CLM    
									ON
										CFM.Grp_id = CLM.Loc_Id
									INNER JOIN 
										dbo.ListMaster LM 
									ON 
										CFM.numDomainID = LM.numDomainID 
										AND CFM.numlistid= LM.numListID 
										'
			IF @numModuleID = 1
			BEGIN
				SET @strSql = @strSql + 'AND Grp_id IN (1,12,13,14)'	
			END
			IF @numModuleID = 2
			BEGIN
				SET @strSql = @strSql + 'AND Grp_id IN (2,6)'	
			END
			IF @numModuleID = 3
			BEGIN
				SET @strSql = @strSql + 'AND Grp_id IN (5)'	
			END
			IF @numModuleID = 4
			BEGIN
				SET @strSql = @strSql + 'AND Grp_id IN (11)'	
			END
			IF @numModuleID = 5
			BEGIN
				SET @strSql = @strSql + 'AND Grp_id IN (3)'	
			END
			IF @numModuleID = 6
			BEGIN
				SET @strSql = @strSql + 'AND Grp_id IN (12,13,14)'	
			END
			IF @numModuleID = 7
			BEGIN
				SET @strSql = @strSql + 'AND Grp_id IN (0)'	
			END
			IF @numModuleID = 8
			BEGIN
				SET @strSql = @strSql + 'AND Grp_id IN (0)'	
			END
			IF @numModuleID = 9
			BEGIN
				SET @strSql = @strSql + 'AND Grp_id IN (4)'	
			END
			IF @numModuleID = 10
			BEGIN
				SET @strSql = @strSql + 'AND Grp_id IN (0)'	
			END
			IF @numModuleID = 11
			BEGIN
				SET @strSql = @strSql + 'AND Grp_id IN (15)'	
			END
			IF @numModuleID = 12
			BEGIN
				SET @strSql = @strSql + 'AND Grp_id IN (0)'	
			END
			SET @strSql = @strSql + ' WHERE CFM.numDomainID='+ CONVERT(VARCHAR,@numDomainID)+') '
				
			SET @strSql = @strSql + ' order by vcListName '
			PRINT @strSql
			EXEC (@strSql)
	END
END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetOrderItems' ) 
    DROP PROCEDURE USP_GetOrderItems
GO
CREATE PROCEDURE USP_GetOrderItems ( @tintMode TINYINT = 0,
                                     @numOppItemID NUMERIC(9) = 0,
                                     @numOppID NUMERIC(9) = 0,
                                     @numDomainId NUMERIC(9),
                                     @ClientTimeZoneOffset INT,
									 @vcOppItemIDs VARCHAR(2000)='' )
AS 
BEGIN
	
    IF @tintMode = 1 --Add Return grid data & For Recurring Get Original Units to split
        BEGIN
            SELECT  OI.numoppitemtCode,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcItemDesc,
					OI.vcNotes as txtNotes,ISNULL(OI.numCost,0) AS numCost,
                    OI.numUnitHour numOriginalUnitHour,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					OI.monPrice,
					ISNULL(OI.bitMappingRequired,0) bitMappingRequired
            FROM    dbo.OpportunityMaster OM
                    INNER JOIN dbo.OpportunityItems OI ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
            WHERE   OM.numDomainId = @numDomainId
                    AND OM.numOppId = @numOppID
                    
        END
 
    IF @tintMode = 2 -- Get single item based on PK, or all items
		-- Used by Sales/Purchase Template Load items in New Sales Order
        BEGIN 
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
                    OI.numItemCode,
					OI.bitItemPriceApprovalRequired,
                    OI.numUnitHour numOriginalUnitHour,
                    OI.monPrice,
                    OI.monTotAmount,
                    OI.numSourceID,
					ISNULL(OI.numCost,0) AS numCost,
                    OI.vcItemDesc,
					OI.vcNotes as txtNotes,
                    OI.numWarehouseItmsID,
                    OI.vcType,
                    OI.vcAttributes,
                    OI.bitDropShip,
                    ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,
                    OI.bitDiscountType,
                    OI.fltDiscount,
                    OI.monTotAmtBefDiscount,
                    ISNULL(OI.numQtyShipped,0) AS numQtyShipped,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcPathForTImage,
                    OI.vcManufacturer,
                    OI.monVendorCost,
                    ISNULL(OI.numUOMId, 0) AS numUOMId,
                    ISNULL(OI.numUOMId, 0) numUOM, -- used from add edit order
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL) AS UOMConversionFactor,
                    OI.bitWorkOrder,
                    isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
                    OI.numShipmentMethod,
                    OI.numSOVendorId,
                    OI.numProjectID,
                    OI.numClassID,
                    OI.numProjectStageID,
                    OI.numToWarehouseItemID,
                    WItems.numWarehouseID,
                    W.vcWareHouse AS Warehouse,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,
					isnull(OI.numProjectID,0) as numProjectID,
					ISNULL(dbo.fn_GetUOMName(I.numBaseUnit),'') AS vcBaseUOMName,
					OI.bitDropShip AS bitAllowDropShip,
					I.numVendorID,
					ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.txtItemDesc,'') txtItemDesc,
					ISNULL(I.vcModelID,'') vcModelID,
					ISNULL(I.vcManufacturer,'') vcManufacturer,
					ISNULL(I.numBarCodeId,'') numBarCodeId,
					ISNULL(I.bitSerialized,0) bitSerialized,
					ISNULL(I.bitKitParent,0) bitKitParent,
					ISNULL(I.bitAssembly,0) bitAssembly,
					ISNULL(I.IsArchieve,0) IsArchieve,
					ISNULL(I.bitLotNo,0) bitLotNo,
					ISNULL(WItems.monWListPrice,0) monWListPrice,
					ISNULL(I.bitTaxable, 0) bitTaxable,
					ISNULL(I.numItemClassification,0) AS numItemClassification,
                    I.charItemType,
                    I.bitKitParent,
					I.bitAssembly,
					I.bitSerialized,
					ISNULL(I.fltWeight,0) AS [fltWeight],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					CASE WHEN I.numItemGroup > 0 AND ISNULL(I.bitMatrix,0)=0 THEN ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) ELSE ISNULL(I.vcSKU,'') END AS [vcSKU],
					(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END)  AS monAverageCost,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder,
					ISNULL(OI.vcAttrValues,'') AS vcAttrValues,
					ISNULL(OI.numSortOrder,0) numSortOrder
					,(CASE WHEN OM.tintOppType=2 THEN ISNULL(vcNotes,'') ELSE '' END) AS vcVendorNotes
					,CPN.CustomerPartNo
					,ISNULL(OI.vcASIN,0) vcASIN
					,ISNULL(OI.vcSKU,0) vcSKUOpp
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
					INNER JOIN DivisionMaster DM ON OM.numDivisionId = DM.numDivisionID
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
                    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
					LEFT JOIN CustomerPartNumber CPN ON CPN.numItemCode = I.numItemCode 
						AND CPN.numCompanyId=DM.numCompanyID
            WHERE   OI.numOppId = @numOppID
                    AND ( OI.numoppitemtCode = @numOppItemID
                          OR @numOppItemID = 0
                        )
                    AND OM.numDomainId = @numDomainId
                    
        END
 
    IF @tintMode = 3 -- Get item for SO to PO and vice versa creation
        BEGIN
            DECLARE @tintOppType AS TINYINT
			SELECT @tintOppType=tintOppType FROM OpportunityMaster WHERE numOppId=@numOppID

			IF @tintOppType = 1
			BEGIN
				DECLARE @TempItems TABLE
				(
					ID INT IDENTITY(1,1),numoppitemtCode NUMERIC(18,0),numItemCode NUMERIC(18,0),vcItemName VARCHAR(300),txtNotes Varchar(MAX),charItemType char,vcModelID VARCHAR(100),vcItemDesc TEXT,vcType VARCHAR(20),vcPathForTImage VARCHAR(200),
					numWarehouseItmsID NUMERIC(18,0),vcAttributes VARCHAR(500),bitDropShip BIT,numOrigUnitHour FLOAT,numUnitHour FLOAT,numUOM NUMERIC(18,0),vcUOMName VARCHAR(100),UOMConversionFactor DECIMAL(18,5),
					monPrice DECIMAL(20,5),numVendorID NUMERIC(18,0),numCost decimal(30, 16),vcPartNo VARCHAR(300),monVendorCost DECIMAL(20,5),numSOVendorId NUMERIC(18,0),numClassID NUMERIC(18,0),numProjectID NUMERIC(18,0),tintLevel TINYINT, vcAttrValues VARCHAR(500),
					ItemRequiredDate DATETIME,numPOID NUMERIC(18,0), numPOItemID NUMERIC(18,0),vcPOName VARCHAR(300)
				)

				INSERT INTO 
					@TempItems
				SELECT
					OI.numoppitemtCode,OI.numItemCode,OI.vcItemName,OI.vcNotes as txtNotes,I.charItemType,OI.vcModelID,OI.vcItemDesc,OI.vcType,ISNULL(OI.vcPathForTImage, ''),ISNULL(OI.numWarehouseItmsID,0),ISNULL(OI.vcAttributes, ''),
					ISNULL(OI.bitDropShip, 0),ISNULL(OI.numUnitHour,0),OI.numUnitHour * dbo.fn_UOMConversion(I.numBaseUnit, OI.numItemCode,OM.numDomainId, OI.numUOMId),ISNULL(OI.numUOMId, 0),ISNULL(U.vcUnitName, ''),dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,OM.numDomainId, NULL) AS UOMConversionFactor,
					OI.monPrice,ISNULL(I.numVendorID, 0),ISNULL(OI.numCost,0),ISNULL(V.vcPartNo, ''),ISNULL(V.monCost, 0),ISNULL(OI.numSOVendorId, 0),0,0,0,ISNULL(vcAttrValues,'')
					,(CASE WHEN ISNULL(OI.ItemReleaseDate,'') = '' THEN
							 ISNULL(OM.dtReleaseDate,'') 
						ELSE OI.ItemReleaseDate 
						END ),ISNULL(SOLIPL.numPurchaseOrderID,0),ISNULL(SOLIPL.numPurchaseOrderItemID,0),ISNULL(OMPO.vcPOppName,'')
				FROM    
					dbo.OpportunityItems OI
					INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
					INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
					LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
					LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
					LEFT JOIN SalesOrderLineItemsPOLinking SOLIPL ON OI.numOppId=SOLIPL.numSalesOrderID AND OI.numoppitemtCode=SOLIPL.numSalesOrderItemID
					LEFT JOIN OpportunityMaster OMPO ON SOLIPL.numPurchaseOrderID = OMPO.numOppId
				WHERE   
					OI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
				ORDER BY
					OI.numSortOrder


				--INSERT KIT CHILD ITEMS WHICH ARE NOT KIT AND NOT YET ADDED
				INSERT INTO 
					@TempItems
				SELECT
					0,OKI.numChildItemID,'' as txtNotes,I.vcItemName,I.charItemType,I.vcModelID,I.txtItemDesc,(CASE WHEN I.charitemType = 'P' THEN 'Product' WHEN I.charitemType = 'S' THEN 'Service' END),
					ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault=1),''),ISNULL(OKI.numWareHouseItemId,0),
					ISNULL(dbo.USP_GetAttributes(OKI.numWarehouseItemID, bitSerialized),''),0,ISNULL(OKI.numQtyItemsReq,0),ISNULL(OKI.numQtyItemsReq,0)  * dbo.fn_UOMConversion(I.numBaseUnit, I.numItemCode,OM.numDomainId, OKI.numUOMId),ISNULL(OKI.numUOMId, 0),ISNULL(U.vcUnitName, ''),
					dbo.fn_UOMConversion(OKI.numUOMId, OKI.numChildItemID,OM.numDomainId, NULL),
					(CASE WHEN I.charitemType = 'P' THEN WI.monWListPrice ELSE I.monListPrice END),
					ISNULL(I.numVendorID, 0),ISNULL(t1.numCost,0),ISNULL(V.vcPartNo, ''),ISNULL(V.monCost, 0),0,0,0,1,'',
					t1.ItemRequiredDate,0,0,''
				FROM    
					dbo.OpportunityKitItems OKI
				LEFT JOIN @TempItems t1 ON OKI.numChildItemID = t1.numItemCode AND ISNULL(OKI.numWareHouseItemId,0) = ISNULL(t1.numWarehouseItmsID,0)
				LEFT JOIN WarehouseItems WI ON OKI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKI.numChildItemID
				LEFT JOIN UOM U ON U.numUOMId = OKI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OKI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t1.ID IS NULL


				-- FOR KIT CHILD ITEMS WHICH ARE ALREADY ADDED AT PARENT LEVEL - UPDATE QTY ONLY
				UPDATE 
					t2
				SET 
					t2.numOrigUnitHour = ISNULL(t2.numOrigUnitHour,0) + ISNULL(OKI.numQtyItemsReq,0),
					t2.numUnitHour = ISNULL(t2.numUnitHour,0) + (ISNULL(OKI.numQtyItemsReq,0) * dbo.fn_UOMConversion(I.numBaseUnit, I.numItemCode,OM.numDomainId, OKI.numUOMId)),
					t2.monPrice = (CASE 
									WHEN ISNULL(t2.monPrice,0) < (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)  
									THEN t2.monPrice
									ELSE (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)
									END)
				FROM
					@TempItems t2
				INNER JOIN
					OpportunityKitItems OKI 
				ON
					OKI.numChildItemID = t2.numItemCode AND ISNULL(OKI.numWareHouseItemId,0) = ISNULL(t2.numWarehouseItmsID,0)
				LEFT JOIN WarehouseItems WI ON OKI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKI.numChildItemID
				LEFT JOIN UOM U ON U.numUOMId = OKI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OKI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t2.tintLevel <> 1

				--INSERT SUB KIT CHILD ITEMS WHICH ARE NOT KIT AND NOT YET ADDED
				INSERT INTO 
					@TempItems
				SELECT
					0,OKCI.numItemID,'' as txtNotes,I.vcItemName,I.charItemType,I.vcModelID,I.txtItemDesc,(CASE WHEN I.charitemType = 'P' THEN 'Product' WHEN I.charitemType = 'S' THEN 'Service' END),
					ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault=1),''),ISNULL(OKCI.numWareHouseItemId,0),
					ISNULL(dbo.USP_GetAttributes(OKCI.numWarehouseItemID, bitSerialized),''),0,ISNULL(OKCI.numQtyItemsReq,0),ISNULL(OKCI.numQtyItemsReq,0) * dbo.fn_UOMConversion(I.numBaseUnit, I.numItemCode,OM.numDomainId, OKCI.numUOMId),ISNULL(OKCI.numUOMId, 0),ISNULL(U.vcUnitName, ''),
					dbo.fn_UOMConversion(OKCI.numUOMId, OKCI.numItemID,OM.numDomainId, NULL),
					(CASE WHEN I.charitemType = 'P' THEN WI.monWListPrice ELSE I.monListPrice END),
					ISNULL(I.numVendorID, 0),ISNULL(t1.numCost,0),ISNULL(V.vcPartNo, ''),ISNULL(V.monCost, 0),0,0,0,2,'',
					t1.ItemRequiredDate,0,0,''
				FROM    
					dbo.OpportunityKitChildItems OKCI
				LEFT JOIN @TempItems t1 ON OKCI.numItemID = t1.numItemCode AND ISNULL(OKCI.numWareHouseItemId,0) = ISNULL(t1.numWarehouseItmsID,0)
				LEFT JOIN WarehouseItems WI ON OKCI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKCI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKCI.numItemID
				LEFT JOIN UOM U ON U.numUOMId = OKCI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OKCI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t1.ID IS NULL

				-- FOR SUB KIT CHILD ITEMS WHICH ARE ALREADY ADDED AT PARENT LEVEL - UPDATE QTY ONLY
				UPDATE 
					t2
				SET 
					t2.numOrigUnitHour = ISNULL(t2.numOrigUnitHour,0) + ISNULL(OKCI.numQtyItemsReq,0),
					t2.numUnitHour = ISNULL(t2.numUnitHour,0) + (ISNULL(OKCI.numQtyItemsReq,0) * dbo.fn_UOMConversion(I.numBaseUnit, I.numItemCode,OM.numDomainId, OKCI.numUOMId)),
					t2.monPrice = (CASE 
									WHEN ISNULL(t2.monPrice,0) < (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)  
									THEN t2.monPrice
									ELSE (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)
									END)
				FROM
					@TempItems t2
				INNER JOIN
					OpportunityKitChildItems OKCI 
				ON
					OKCI.numItemID = t2.numItemCode AND ISNULL(OKCI.numWareHouseItemId,0) = ISNULL(t2.numWarehouseItmsID,0)
				LEFT JOIN WarehouseItems WI ON OKCI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKCI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKCI.numItemID
				LEFT JOIN UOM U ON U.numUOMId = OKCI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OKCI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t2.tintLevel <> 2

				SELECT * FROM @TempItems
			END
			ELSE
			BEGIN
				SELECT  OI.numoppitemtCode,
                    OI.numOppId,
					OI.vcNotes as txtNotes,
					OI.bitItemPriceApprovalRequired,
                    OI.vcItemDesc,
                    ISNULL(OI.vcPathForTImage, '') vcPathForTImage,
                    ISNULL(OI.numWarehouseItmsID, 0) numWarehouseItmsID,
                    OI.vcItemName,
                    OI.vcModelID,
					ISNULL(OI.numCost,0) numCost,
                    ISNULL(OI.vcAttributes, '') vcAttributes,
                    OI.vcType,
                    ISNULL(OI.bitDropShip, 0) bitDropShip,
                    OI.numItemCode,
					OI.numUnitHour numOrigUnitHour,
                    OI.numUnitHour numOriginalUnitHour,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL) AS UOMConversionFactor,
                    ISNULL(I.numVendorID, 0) numVendorID,
                    ISNULL(V.monCost, 0) monVendorCost,
                    ISNULL(V.vcPartNo, '') vcPartNo,
                    OI.monPrice,
                    ISNULL(OI.numSOVendorId, 0) numSOVendorId,
                    ISNULL(I.charItemType, 'N') charItemType,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL((SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numWarehouseItemID = OI.numWarehouseItmsID ),ISNULL(I.vcSKU,'')) AS [vcSKU],
					ISNULL(vcAttrValues,'') AS vcAttrValues,
					(CASE WHEN ISNULL(OI.ItemReleaseDate,'') = '' THEN
							 ISNULL(OM.dtReleaseDate,'') 
						ELSE  OI.ItemReleaseDate
						END ) AS ItemRequiredDate, 0 AS numPOID,0 AS numPOItemID,'' AS vcPOName
				FROM    dbo.OpportunityItems OI
						INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
						INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
						LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
						LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID
												AND I.numItemCode = V.numItemCode
				WHERE   OI.numOppId = @numOppID
						AND OM.numDomainId = @numDomainId
			END
        END
        IF @tintMode = 4 -- Add Edit Order items
        BEGIN 
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
					OI.vcNotes as txtNotes,
                    OI.numItemCode,
					OI.bitItemPriceApprovalRequired,
					OI.numUnitHour AS numOrigUnitHour,
                    OI.numUnitHour,
                    OI.monPrice,
                    OI.monTotAmount,
                    OI.numSourceID,
                    OI.vcItemDesc,
					OI.vcNotes,
					ISNULL(OI.numCost,0) numCost,
                    OI.numWarehouseItmsID,
                    OI.vcType ItemType,
                    OI.vcAttributes Attributes,
                    OI.bitDropShip DropShip,
                    OI.numUnitHourReceived,
                    OI.bitDiscountType,
                    OI.fltDiscount,
                    OI.monTotAmtBefDiscount,
					ISNULL(OI.monTotAmtBefDiscount,0) - ISNULL(OI.monTotAmount,0) AS TotalDiscountAmount,
                    OI.numQtyShipped,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcPathForTImage,
                    OI.vcManufacturer,
                    OI.monVendorCost,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    ISNULL(dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL),1) AS UOMConversionFactor,
                    OI.bitWorkOrder,
                    isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
                    OI.numShipmentMethod,
                    OI.numSOVendorId,
                    OI.numProjectStageID,
                    OI.numToWarehouseItemID,
                   UPPER(I.charItemType) charItemType,
                    WItems.numWarehouseID,
                    W.vcWareHouse AS Warehouse,
                    0 as Op_Flag,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,
					CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON ob.numOppBizDocsId=obi.numOppBizDocID
					AND (ob.bitAuthoritativeBizDocs=1 OR ob.numBizDocId=304) AND obi.numOppItemID=oi.numoppitemtCode AND ob.numOppId=OM.numOppId)>0 THEN 1 ELSE 0 END  AS bitIsAuthBizDoc,
					ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(numQtyShipped,0) AS numQtyShipped,ISNULL(dbo.fn_GetUOMName(I.numBaseUnit),'') AS vcBaseUOMName,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) AS [vcSKU],
					(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) AS monAverageCost,
					ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.txtItemDesc,'') txtItemDesc,
					ISNULL(I.numBarCodeId,'') numBarCodeId,
					ISNULL(I.bitSerialized,0) bitSerialized,
					ISNULL(I.bitKitParent,0) bitKitParent,
					ISNULL(I.bitAssembly,0) bitAssembly,
					ISNULL(I.IsArchieve,0) IsArchieve,
					ISNULL(I.bitLotNo,0) bitLotNo,
					ISNULL(WItems.monWListPrice,0) monWListPrice,
					ISNULL(I.bitTaxable, 0) bitTaxable,
					ISNULL(I.numItemClassification,0) AS numItemClassification,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder,
					case when ISNULL(I.bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode,WItems.numWareHouseID) else 0 end AS numMaxWorkOrderQty,
					ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) AS numSerialNoAssigned,
					(CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON OB.numOppBizDocsId=OBI.numOppBizDocID WHERE OB.numOppId = OI.numOppId AND OBI.numOppItemID=OI.numoppitemtCode AND OB.numBizDocId=296) > 0 THEN 1 ELSE 0 END) AS bitAddedFulFillmentBizDoc,
					ISNULL(OI.vcAttrValues,'') AS AttributeIDs,
					ISNULL(I.numContainer,0) AS numContainer
					,ISNULL(I.numNoItemIntoContainer,0) AS numContainerQty
					,ISNULL(numPromotionID,0) AS numPromotionID,
					ISNULL(bitPromotionTriggered,0) AS bitPromotionTriggered,
					ISNULL(vcPromotionDetail,'') AS vcPromotionDetail,
					CASE WHEN ISNULL(OI.numSortOrder,0)=0 THEN row_number() OVER (ORDER BY OI.numOppId) ELSE ISNULL(OI.numSortOrder,0) END AS numSortOrder
					,ISNULL(vcNotes,'') AS vcVendorNotes
					,ISNULL(CPN.CustomerPartNo,'') CustomerPartNo
					,OI.ItemRequiredDate AS ItemRequiredDate
					,ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) ItemReleaseDate
					,(CASE WHEN ISNULL(bitMarkupDiscount,0) = 0 THEN '0' ELSE '1' END) AS bitMarkupDiscount
					,ISNULL(vcChildKitSelectedItems,'') KitChildItems
					,(CASE 
						WHEN ISNULL(I.bitKitParent,0) = 1 
						THEN 
							(CASE 
								WHEN EXISTS (SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID=IInner.numItemCode WHERE ID.numItemKitID=I.numItemCode AND ISNULL(IInner.bitKitParent,0)=1)
								THEN 1
								ELSE 0
							END)
						ELSE 0 
					END) bitHasKitAsChild
					,ISNULL(I.bitFreeShipping,0) IsFreeShipping
					,(CASE 
						WHEN ISNULL(I.bitKitParent,0)=1
								AND (CASE 
										WHEN EXISTS (SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID=IInner.numItemCode WHERE ID.numItemKitID=I.numItemCode AND ISNULL(IInner.bitKitParent,0)=1)
										THEN 1
										ELSE 0
									END) = 0
						THEN 
							dbo.GetKitWeightBasedOnItemSelection(I.numItemCode,OI.vcChildKitSelectedItems)
						ELSE 
							ISNULL(I.fltWeight,0) 
					END) fltItemWeight
					,ISNULL(I.fltWidth,0) fltItemWidth
					,ISNULL(I.fltHeight,0) fltItemHeight
					,ISNULL(I.fltLength,0) fltItemLength
					,ISNULL(OI.numWOQty,0) numWOQty
					,ISNULL(I.bitSOWorkOrder,0) bitAlwaysCreateWO
					,ISNULL(OI.bitMappingRequired,0) bitMappingRequired
					,ISNULL(OI.vcASIN,0) vcASIN
					,ISNULL(OI.vcSKU,0) vcSKUOpp
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
					INNER JOIN DivisionMaster DM ON OM.numDivisionId=DM.numDivisionID
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
                    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
					LEFT JOIN CustomerPartNumber CPN ON CPN.numItemCode = I.numItemCode AND CPN.numCompanyId=DM.numCompanyID
            WHERE   OI.numOppId = @numOppID
                    AND OM.numDomainId = @numDomainId
			ORDER BY OI.numSortOrder
                    
			SELECT  
				vcSerialNo,
				vcComments AS Comments,
				numOppItemID AS numoppitemtCode,
				W.numWarehouseItmsDTLID,
				numWarehouseItmsID AS numWItmsID,
				dbo.Fn_getattributes(W.numWarehouseItmsDTLID, 1) Attributes
			FROM    
				WareHouseItmsDTL W
			JOIN 
				OppWarehouseSerializedItem O 
			ON 
				O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
			WHERE   
				numOppID = @numOppID         
        END
        
        
		IF @tintMode = 5 -- Get WebApi Order Items to enter WebApi Order line Item Details 
		BEGIN 
			SELECT OI.numOppItemtCode,OI.numOppId,OI.numItemCode,OI.vcNotes as txtNotes,ISNULL(OI.numCost,0) numCost,
			ISNULL((SELECT vcWHSKU FROM dbo.WareHouseItems WHERE WareHouseItems.numWarehouseItemID = OI.numWarehouseItmsID),ISNULL(IT.vcSKU,'')) AS [vcSKU],
			OI.vcItemDesc,OMAPI.vcAPIOppId
			FROM OpportunityItems OI 
			INNER JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId 
			INNER JOIN dbo.OpportunityMasterAPI OMAPI ON OM.numOppId = OMAPI.numOppId
			INNER JOIN dbo.Item IT ON OI.numItemCode = IT.numItemCode
			WHERE OI.numOppId = @numOppID and OM.numDomainID = @numDomainId 
        END

	IF @tintMode = 6 -- Add Edit Order items
        BEGIN 
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
					OI.vcNotes as txtNotes,
                    OI.numItemCode,
					OI.bitItemPriceApprovalRequired,
					OI.numUnitHour AS numOrigUnitHour,
                    (OI.numUnitHour-ISNULL((SELECT SUM(numUnitHour) FROM ReturnItems where 
					numReturnHeaderID IN (SELECT numReturnHeaderID FROM ReturnHeader 
					WHERE numOppId=@numOppID) AND numOppItemID=OI.numoppitemtCode),0))
					AS numUnitHour,
                    OI.monPrice,
                    OI.monTotAmount,
                    OI.numSourceID,
                    OI.vcItemDesc,
					OI.numCost,
                    OI.numWarehouseItmsID,
                    OI.vcType ItemType,
                    OI.vcAttributes Attributes,
                    OI.bitDropShip DropShip,
                    OI.numUnitHourReceived,
                    OI.bitDiscountType,
                    OI.fltDiscount,
                    OI.monTotAmtBefDiscount,
                    OI.numQtyShipped,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcPathForTImage,
                    OI.vcManufacturer,
                    OI.monVendorCost,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    ISNULL(dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL),1) AS UOMConversionFactor,
                    OI.bitWorkOrder,
                    isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
                    OI.numShipmentMethod,
                    OI.numSOVendorId,
                    OI.numProjectID,
                    OI.numClassID,
                    OI.numProjectStageID,
                    OI.numToWarehouseItemID,
                   UPPER(I.charItemType) charItemType,
                    WItems.numWarehouseID,
                    W.vcWareHouse AS Warehouse,
                    0 as Op_Flag,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,
					CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON ob.numOppBizDocsId=obi.numOppBizDocID
					AND (ob.bitAuthoritativeBizDocs=1 OR ob.numBizDocId=304) AND obi.numOppItemID=oi.numoppitemtCode AND ob.numOppId=OM.numOppId)>0 THEN 1 ELSE 0 END  AS bitIsAuthBizDoc,
					ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(numQtyShipped,0) AS numQtyShipped,ISNULL(dbo.fn_GetUOMName(I.numBaseUnit),'') AS vcBaseUOMName,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) AS [vcSKU],
					(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) AS monAverageCost,
					ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.txtItemDesc,'') txtItemDesc,
					ISNULL(I.vcModelID,'') vcModelID,
					ISNULL(I.vcManufacturer,'') vcManufacturer,
					ISNULL(I.numBarCodeId,'') numBarCodeId,
					ISNULL(I.bitSerialized,0) bitSerialized,
					ISNULL(I.bitKitParent,0) bitKitParent,
					ISNULL(I.bitAssembly,0) bitAssembly,
					ISNULL(I.IsArchieve,0) IsArchieve,
					ISNULL(I.bitLotNo,0) bitLotNo,
					ISNULL(WItems.monWListPrice,0) monWListPrice,
					ISNULL(I.bitTaxable, 0) bitTaxable,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder,
					case when ISNULL(I.bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode,WItems.numWareHouseID) else 0 end AS numMaxWorkOrderQty,
					ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) AS numSerialNoAssigned,
					(CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON OB.numOppBizDocsId=OBI.numOppBizDocID WHERE OB.numOppId = OI.numOppId AND OBI.numOppItemID=OI.numoppitemtCode AND OB.numBizDocId=296) > 0 THEN 1 ELSE 0 END) AS bitAddedFulFillmentBizDoc,
					ISNULL(OI.vcAttrValues,'') AS AttributeIDs
					,(CASE WHEN OM.tintOppType=2 THEN ISNULL(vcNotes,'') ELSE '' END) AS vcVendorNotes
					,(CASE WHEN ISNULL(bitMarkupDiscount,0) = 0 THEN '0' ELSE '1' END) AS bitMarkupDiscount 
--					0 AS [Tax0],0 AS bitTaxable0
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
                    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
            WHERE   OI.numOppId = @numOppID
                    AND OM.numDomainId = @numDomainId
					AND (OI.numUnitHour-ISNULL((SELECT SUM(numUnitHour) FROM ReturnItems where 
					numReturnHeaderID IN (SELECT numReturnHeaderID FROM ReturnHeader 
					WHERE numOppId=@numOppID) AND numOppItemID=OI.numoppitemtCode),0))>0
                    
		SELECT  vcSerialNo,
				vcComments AS Comments,
				numOppItemID AS numoppitemtCode,
				W.numWarehouseItmsDTLID,
				numWarehouseItmsID AS numWItmsID,
				dbo.Fn_getattributes(W.numWarehouseItmsDTLID, 1) Attributes
		FROM    WareHouseItmsDTL W
				JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
		WHERE   numOppID = @numOppID AND numOppItemID IN (SELECT OI.numoppitemtCode FROM OpportunityItems OI WHERE OI.numOppId = @numOppID AND (OI.numUnitHour-ISNULL((SELECT SUM(numUnitHour) FROM ReturnItems where 
					numReturnHeaderID IN (SELECT numReturnHeaderID FROM ReturnHeader 
					WHERE numOppId=@numOppID) AND numOppItemID=OI.numoppitemtCode),0))>0)
					              
                   
END
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSitesDetail')
DROP PROCEDURE USP_GetSitesDetail
GO
CREATE PROCEDURE [dbo].[USP_GetSitesDetail]
(
	@numSiteID NUMERIC(9)
)
AS 
BEGIN
    SELECT  
		S.[numDomainID],
        D.[numDivisionID],
        ISNULL(S.[bitIsActive], 1),
        ISNULL(E.[numDefaultWareHouseID],0) numDefaultWareHouseID,
        ISNULL(E.[numRelationshipId],0) numRelationshipId,
        ISNULL(E.[numProfileId],0) numProfileId,
        ISNULL(E.[bitHidePriceBeforeLogin],0) AS bitHidePriceBeforeLogin,
		dbo.fn_GetContactEmail(1,0,numAdminID) AS DomainAdminEmail,
		S.vcLiveURL,
		ISNULL(E.bitAuthOnlyCreditCard,0) bitAuthOnlyCreditCard,
		ISNULL(D.numDefCountry,0) numDefCountry,
		ISNULL(E.bitSendEMail,0) bitSendMail,
		ISNULL((SELECT numAuthoritativeSales FROM dbo.AuthoritativeBizDocs WHERE numDomainId=S.numDomainID),0) AuthSalesBizDoc,
		ISNULL((SELECT TOP 1 numBizDocId FROM dbo.eCommercePaymentConfig WHERE numSiteID=@numSiteID AND numPaymentMethodId = 27261 AND bitEnable =1 ),0) numCreditTermBizDocID,
		ISNULL((SELECT TOP 1 numBizDocId FROM dbo.eCommercePaymentConfig WHERE numSiteID=@numSiteID AND numPaymentMethodId =1 AND bitEnable =1 ),0) numCreditCardBizDocID,
		ISNULL((SELECT TOP 1 numBizDocId FROM dbo.eCommercePaymentConfig WHERE numSiteID=@numSiteID AND numPaymentMethodId = 31488 AND bitEnable =1 ),0) numGoogleCheckoutBizDocID,
		ISNULL((SELECT TOP 1 numBizDocId FROM dbo.eCommercePaymentConfig WHERE numSiteID=@numSiteID AND numPaymentMethodId = 35141 AND bitEnable =1 ),0) numPaypalCheckoutBizDocID,
		ISNULL((SELECT TOP 1 numBizDocId FROM dbo.eCommercePaymentConfig WHERE numSiteID=@numSiteID AND numPaymentMethodId = 84 AND bitEnable =1 ),0) numSalesInquiryBizDocID,
		ISNULL(D.bitSaveCreditCardInfo,0) bitSaveCreditCardInfo,
		ISNULL(bitOnePageCheckout,0) bitOnePageCheckout,
		ISNULL(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
		ISNULL(D.numShippingServiceItemID,0) numShippingServiceItemID,
		ISNULL(D.bitAutolinkUnappliedPayment,0) bitAutolinkUnappliedPayment,
		ISNULL(D.numDiscountServiceItemID,0) numDiscountServiceItemID,
		ISNULL(D.tintBaseTax,0) AS [tintBaseTax],
		ISNULL(E.bitSkipStep2,0) AS [bitSkipStep2],
		ISNULL(E.bitDisplayCategory,0) AS [bitDisplayCategory],
		ISNULL(E.bitShowPriceUsingPriceLevel,0) AS [bitShowPriceUsingPriceLevel],
		ISNULL(E.[bitPreSellUp],0) [bitPreSellUp],
		ISNULL(E.[bitPostSellUp],0) [bitPostSellUp],
		ISNULL(E.[dcPostSellDiscount],0) [dcPostSellDiscount],
		ISNULL(E.numDefaultClass,0) numDefaultClass,
		E.vcPreSellUp,E.vcPostSellUp,ISNULL(E.vcRedirectThankYouUrl,'') AS vcRedirectThankYouUrl,
		ISNULL(D.vcSalesOrderTabs,'') AS vcSalesOrderTabs,
		ISNULL(D.vcSalesQuotesTabs,'') AS vcSalesQuotesTabs,
		ISNULL(D.vcItemPurchaseHistoryTabs,'') AS vcItemPurchaseHistoryTabs,
		ISNULL(D.vcItemsFrequentlyPurchasedTabs,'') AS vcItemsFrequentlyPurchasedTabs,
		ISNULL(D.vcOpenCasesTabs,'') AS vcOpenCasesTabs,
		ISNULL(D.vcOpenRMATabs,'') AS vcOpenRMATabs,
		ISNULL(D.bitSalesOrderTabs,0) AS bitSalesOrderTabs,
		ISNULL(D.bitSalesQuotesTabs,0) AS bitSalesQuotesTabs,
		ISNULL(D.bitItemPurchaseHistoryTabs,0) AS bitItemPurchaseHistoryTabs,
		ISNULL(D.bitItemsFrequentlyPurchasedTabs,0) AS bitItemsFrequentlyPurchasedTabs,
		ISNULL(D.bitOpenCasesTabs,0) AS bitOpenCasesTabs,
		ISNULL(D.bitOpenRMATabs,0) AS bitOpenRMATabs,
		ISNULL(D.bitSupportTabs,0) AS bitSupportTabs,
		ISNULL(D.vcSupportTabs,'') AS vcSupportTabs,
		ISNULL(bitHideAddtoCart,0) AS bitHideAddtoCart,
		ISNULL(bitShowPromoDetailsLink,0) bitShowPromoDetailsLink,
		ISNULL(D.bitDefaultProfileURL,0) AS bitDefaultProfileURL
    FROM
		Sites S
    INNER JOIN 
		[Domain] D 
	ON 
		D.[numDomainId] = S.[numDomainID]
    LEFT OUTER JOIN 
		[eCommerceDTL] E 
	ON 
		D.[numDomainId] = E.[numDomainId] 
		AND S.numSiteID = E.numSiteId
    WHERE
		S.[numSiteID] = @numSiteID
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Import_SaveItemDetails')
DROP PROCEDURE USP_Import_SaveItemDetails
GO
CREATE PROCEDURE [dbo].[USP_Import_SaveItemDetails]  
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0) OUTPUT
	,@numShipClass NUMERIC(18,0)
	,@vcItemName VARCHAR(300)
	,@monListPrice DECIMAL(20,5)
	,@txtItemDesc VARCHAR(1000)
	,@vcModelID VARCHAR(200)
	,@vcManufacturer VARCHAR(250)
	,@numBarCodeId VARCHAR(50)
	,@fltWidth FLOAT
	,@fltHeight FLOAT
	,@fltWeight FLOAT
	,@fltLength FLOAT
	,@monAverageCost DECIMAL(20,5)
	,@bitSerialized BIT
	,@bitKitParent BIT
	,@bitAssembly BIT
	,@IsArchieve BIT
	,@bitLotNo BIT
	,@bitAllowBackOrder BIT
	,@numIncomeChartAcntId NUMERIC(18,0)
	,@numAssetChartAcntId NUMERIC(18,0)
	,@numCOGsChartAcntId NUMERIC(18,0)
	,@numItemClassification NUMERIC(18,0)
	,@numItemGroup NUMERIC(18,0)
	,@numPurchaseUnit NUMERIC(18,0)
	,@numSaleUnit NUMERIC(18,0)
	,@numItemClass NUMERIC(18,0)
	,@numBaseUnit NUMERIC(18,0)
	,@vcSKU VARCHAR(50)
	,@charItemType CHAR(1)
	,@bitTaxable BIT
	,@txtDesc NVARCHAR(MAX)
	,@vcExportToAPI VARCHAR(50)
	,@bitAllowDropShip BIT
	,@bitMatrix BIT
	,@vcItemAttributes VARCHAR(2000)
	,@vcCategories VARCHAR(1000)
	,@txtShortDesc NVARCHAR(MAX)
	,@numReOrder FLOAT
	,@bitAutomateReorderPoint BIT
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	-- IMPORTANT: PLEASE MAKE SURE NEW FIELD WHICH YOUR ARE ADDING IN STORE PROCEDURE FOR UPDATE ARE ADDED IN SELECT IN PROCEDURE USP_Import_GetItemDetails

	IF @numAssetChartAcntId = 0
	BEGIN
		SET @numAssetChartAcntId =NULL
	END

	DECLARE @bitAttributesChanged AS BIT  = 1
	DECLARE @bitOppOrderExists AS BIT = 0
	DECLARE @hDoc AS INT     
	                                                                        	
	DECLARE @TempTable TABLE 
	(
		ID INT IDENTITY(1,1),
		Fld_ID NUMERIC(18,0),
		Fld_Value NUMERIC(18,0)
	)   

	IF ISNULL(@bitMatrix,0) = 1 AND ISNULL(@numItemGroup,0) = 0
	BEGIN
		RAISERROR('ITEM_GROUP_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) = 0 AND ISNULL(@numItemCode,0) = 0
	BEGIN
		RAISERROR('ITEM_ATTRIBUTES_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) > 0
	BEGIN
		DECLARE @bitDuplicate AS BIT = 0
	
		IF DATALENGTH(ISNULL(@vcItemAttributes,''))>2
		BEGIN
			EXEC sp_xml_preparedocument @hDoc OUTPUT, @vcItemAttributes
	                                      
			INSERT INTO @TempTable 
			(
				Fld_ID,
				Fld_Value
			)    
			SELECT
				*          
			FROM 
				OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
			WITH 
				(Fld_ID NUMERIC(18,0),Fld_Value NUMERIC(18,0)) 
			ORDER BY 
				Fld_ID       

			DECLARE @strAttribute VARCHAR(500) = ''
			DECLARE @i AS INT = 1
			DECLARE @COUNT AS INT 
			DECLARE @numFldID AS NUMERIC(18,0)
			DECLARE @numFldValue AS NUMERIC(18,0)
			SELECT @COUNT = COUNT(*) FROM @TempTable

			WHILE @i <= @COUNT
			BEGIN
				SELECT @numFldID=Fld_ID,@numFldValue=Fld_Value FROM @TempTable WHERE ID=@i

				IF ISNUMERIC(@numFldValue)=1
				BEGIN
					SELECT @strAttribute=@strAttribute+Fld_label FROM CFW_Fld_Master WHERE Fld_id=@numFldID AND Grp_id = 9
                
					IF LEN(@strAttribute) > 0
					BEGIN
						SELECT @strAttribute=@strAttribute+':'+ vcData +',' FROM ListDetails WHERE numListItemID=@numFldValue
					END
				END

				SET @i = @i + 1
			END

			SET @strAttribute = SUBSTRING(@strAttribute,0,LEN(@strAttribute))

			IF
				(SELECT
					COUNT(*)
				FROM
					Item
				INNER JOIN
					ItemAttributes
				ON
					Item.numItemCode = ItemAttributes.numItemCode
					AND ItemAttributes.numDomainID = @numDomainID                     
				WHERE
					Item.numItemCode <> @numItemCode
					AND Item.vcItemName = @vcItemName
					AND Item.numItemGroup = @numItemGroup
					AND Item.numDomainID = @numDomainID
					AND dbo.fn_GetItemAttributes(@numDomainID,Item.numItemCode,0) = @strAttribute
				) > 0
			BEGIN
				RAISERROR('ITEM_WITH_ATTRIBUTES_ALREADY_EXISTS',16,1)
				RETURN
			END

			If ISNULL(@numItemCode,0) > 0 AND dbo.fn_GetItemAttributes(@numDomainID,@numItemCode,0) = @strAttribute
			BEGIN
				SET @bitAttributesChanged = 0
			END

			EXEC sp_xml_removedocument @hDoc 
		END	
	END
	ELSE IF ISNULL(@bitMatrix,0) = 0
	BEGIN
		-- REMOVE ATTRIBUTES CONFIGURATION OF ITEM AND WAREHOUSE
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode
	END 


	IF ISNULL(@numItemGroup,0) > 0 AND ISNULL(@bitMatrix,0) = 1
	BEGIN
		IF (SELECT COUNT(*) FROM ItemGroupsDTL WHERE numItemGroupID = @numItemGroup AND tintType=2) = 0
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
		ELSE IF EXISTS (SELECT 
							CFM.Fld_id
						FROM 
							CFW_Fld_Master CFM
						LEFT JOIN
							ListDetails LD
						ON
							CFM.numlistid = LD.numListID
						WHERE
							CFM.numDomainID = @numDomainID
							AND CFM.Fld_id IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID=@numItemGroup AND tintType = 2)
						GROUP BY
							CFM.Fld_id HAVING COUNT(LD.numListItemID) = 0)
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
	END


	IF ISNULL(@numItemCode,0) = 0
	BEGIN
		IF (ISNULL(@vcItemName,'') = '')
		BEGIN
			RAISERROR('ITEM_NAME_NOT_SELECTED',16,1)
			RETURN
		END
        ELSE IF (ISNULL(@charItemType,'') = '' OR ISNULL(@charItemType,'') = '0' )
		BEGIN
			RAISERROR('ITEM_TYPE_NOT_SELECTED',16,1)
			RETURN
		END
        ELSE
		BEGIN 
		    If (@charItemType = 'P')
			BEGIN
				IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numAssetChartAcntId) = 0)
				BEGIN
					RAISERROR('INVALID_ASSET_ACCOUNT',16,1)
					RETURN
				END
			END
			ELSE IF @numAssetChartAcntId = 0
			BEGIN
				SET @numAssetChartAcntId = NULL
			END

			-- This is common check for CharItemType P and Other
			IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numCOGSChartAcntId) = 0)
			BEGIN
				RAISERROR('INVALID_COGS_ACCOUNT',16,1)
				RETURN
			END

			IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numIncomeChartAcntId) = 0)
			BEGIN
				RAISERROR('INVALID_INCOME_ACCOUNT',16,1)
				RETURN
			END
		END


		INSERT INTO Item
		(
			numDomainID
			,numCreatedBy
			,bintCreatedDate
			,bintModifiedDate
			,numModifiedBy
			,numShipClass
			,vcItemName
			,monListPrice
			,txtItemDesc
			,vcModelID
			,vcManufacturer
			,numBarCodeId
			,fltWidth
			,fltHeight
			,fltWeight
			,fltLength
			,monAverageCost
			,bitSerialized
			,bitKitParent
			,bitAssembly
			,IsArchieve
			,bitLotNo
			,bitAllowBackOrder
			,numIncomeChartAcntId
			,numAssetChartAcntId
			,numCOGsChartAcntId
			,numItemClassification
			,numItemGroup
			,numPurchaseUnit
			,numSaleUnit
			,numItemClass
			,numBaseUnit
			,vcSKU
			,charItemType
			,bitTaxable
			,vcExportToAPI
			,bitAllowDropShip
			,bitMatrix
			,numManufacturer
			,bitAutomateReorderPoint
		)
		VALUES
		(
			@numDomainID
			,@numUserCntID
			,GETUTCDATE()
			,GETUTCDATE()
			,@numUserCntID
			,@numShipClass
			,@vcItemName
			,@monListPrice
			,@txtItemDesc
			,@vcModelID
			,@vcManufacturer
			,@numBarCodeId
			,@fltWidth
			,@fltHeight
			,@fltWeight
			,@fltLength
			,@monAverageCost
			,@bitSerialized
			,@bitKitParent
			,@bitAssembly
			,@IsArchieve
			,@bitLotNo
			,@bitAllowBackOrder
			,NULLIF(@numIncomeChartAcntId,0)
			,NULLIF(@numAssetChartAcntId,0)
			,NULLIF(@numCOGsChartAcntId,0)
			,@numItemClassification
			,@numItemGroup 
			,@numPurchaseUnit 
			,@numSaleUnit
			,@numItemClass
			,@numBaseUnit
			,@vcSKU
			,@charItemType
			,@bitTaxable
			,@vcExportToAPI
			,@bitAllowDropShip
			,@bitMatrix
			,(CASE WHEN LEN(ISNULL(@vcManufacturer,'')) > 0 THEN ISNULL((SELECT TOP 1 numDivisionID FROM CompanyInfo INNER JOIN DivisionMaster ON CompanyInfo.numCompanyId=DivisionMaster.numCompanyID WHERE CompanyInfo.numDOmainID=@numDomainID AND LOWER(vcCompanyName) = LOWER(@vcManufacturer)),0) ELSE 0 END)
			,@bitAutomateReorderPoint
		)

		SET @numItemCode = SCOPE_IDENTITY()

		INSERT INTO ItemExtendedDetails (numItemCode,txtDesc,txtShortDesc) VALUES (@numItemCode,@txtDesc,@txtShortDesc)

		IF @charItemType = 'P'
		BEGIN
			
			DECLARE @TEMPWarehouse TABLE
			(
				ID INT IDENTITY(1,1)
				,numWarehouseID NUMERIC(18,0)
			)
		
			INSERT INTO @TEMPWarehouse (numWarehouseID) SELECT numWareHouseID FROM Warehouses WHERE numDomainID=@numDomainID

			DECLARE @j AS INT = 1
			DECLARE @jCount AS INT
			DECLARE @numTempWarehouseID NUMERIC(18,0)
			SELECT @jCount = COUNT(*) FROM @TEMPWarehouse

			WHILE @j <= @jCount
			BEGIN
				SELECT @numTempWarehouseID=numWarehouseID FROM @TEMPWarehouse WHERE ID=@j

				EXEC USP_WarehouseItems_Save @numDomainID,
											@numUserCntID,
											0,
											@numTempWarehouseID,
											0,
											@numItemCode,
											'',
											@monListPrice,
											0,
											0,
											'',
											'',
											'',
											'',
											0
 
				SET @j = @j + 1
			END
		END 
	END
	ELSE IF ISNULL(@numItemCode,0) > 0 AND EXISTS (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode)
	BEGIN
		IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END

		DECLARE @OldGroupID NUMERIC 
		SELECT @OldGroupID = ISNULL(numItemGroup,0) FROM [Item] WHERE numitemcode = @numItemCode AND [numDomainId] = @numDomainID
	
		DECLARE @monOldAverageCost AS DECIMAL(18,4);SET @monOldAverageCost=0
		SELECT @monOldAverageCost=(CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE numItemCode =@numItemCode AND [numDomainId] = @numDomainID
	   
		UPDATE 
			Item 
		SET 
			vcItemName=@vcItemName,txtItemDesc=@txtItemDesc,charItemType=@charItemType,monListPrice= @monListPrice,numItemClassification=@numItemClassification,                                             
			bitTaxable=@bitTaxable,vcSKU = @vcSKU,bitKitParent=@bitKitParent,numDomainID=@numDomainID,bintModifiedDate=getutcdate(),numModifiedBy=@numUserCntID
			,bitSerialized=@bitSerialized,vcModelID=@vcModelID,numItemGroup=@numItemGroup,numCOGsChartAcntId=NULLIF(@numCOGsChartAcntId,0),numAssetChartAcntId=NULLIF(@numAssetChartAcntId,0),                                      
			numIncomeChartAcntId=NULLIF(@numIncomeChartAcntId,0),fltWeight=@fltWeight,fltHeight=@fltHeight,fltWidth=@fltWidth,     
			fltLength=@fltLength,bitAllowBackOrder=@bitAllowBackOrder,bitAssembly=@bitAssembly,numBarCodeId=@numBarCodeId,
			vcManufacturer=@vcManufacturer,numBaseUnit=@numBaseUnit,numPurchaseUnit=@numPurchaseUnit,numSaleUnit=@numSaleUnit,bitLotNo=@bitLotNo,
			IsArchieve = @IsArchieve,numItemClass=@numItemClass,vcExportToAPI=@vcExportToAPI,
			numShipClass=@numShipClass,bitAllowDropShip=@bitAllowDropShip, bitArchiveItem = @IsArchieve,bitMatrix=@bitMatrix,
			numManufacturer=(CASE WHEN LEN(ISNULL(@vcManufacturer,'')) > 0 THEN ISNULL((SELECT TOP 1 numDivisionID FROM CompanyInfo INNER JOIN DivisionMaster ON CompanyInfo.numCompanyId=DivisionMaster.numCompanyID WHERE CompanyInfo.numDOmainID=@numDomainID AND LOWER(vcCompanyName) = LOWER(@vcManufacturer)),0) ELSE 0 END)
			,bitAutomateReorderPoint=@bitAutomateReorderPoint
		WHERE 
			numItemCode=@numItemCode 
			AND numDomainID=@numDomainID

		IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
		BEGIN
			IF (((SELECT ISNULL(SUM(numOnHand),0) FROM WareHouseItems WHERE numItemID = @numItemCode AND numDomainId=@numDomainID) = 0) AND ((SELECT (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item  WHERE numItemCode = @numItemCode AND numDomainId=@numDomainID ) = 0))
			BEGIN
				UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
			END
		END
	
		--If Average Cost changed then add in Tracking
		IF ISNULL(@monAverageCost,0) != ISNULL(@monOldAverageCost,0)
		BEGIN
			DECLARE @numWareHouseItemID NUMERIC(9);SET @numWareHouseItemID=0
			DECLARE @numTotal int;SET @numTotal=0
			SET @i=0
			DECLARE @vcDescription AS VARCHAR(100);SET @vcDescription=''
		
			SELECT @numTotal=COUNT(*) FROM WareHouseItems where numItemID=@numItemCode AND numDomainId=@numDomainId
	     
			WHILE @i < @numTotal
			BEGIN
				SELECT @numWareHouseItemID = min(numWareHouseItemID) FROM WareHouseItems 
					WHERE numItemID=@numItemCode AND numDomainId=@numDomainId and numWareHouseItemID > @numWareHouseItemID 
				
				IF @numWareHouseItemID>0
				BEGIN
					SET @vcDescription='Average Cost Changed Manually OLD : ' + CAST(ISNULL(@monOldAverageCost,0) AS VARCHAR(20))
				
					DECLARE @numDomain AS NUMERIC(18,0)
					SET @numDomain = @numDomainID
					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)		
						@numReferenceID = @numItemCode, --  numeric(9, 0)
						@tintRefType = 1, --  tinyint
						@vcDescription = @vcDescription, --  varchar(100)
						@tintMode = 0, --  tinyint
						@numModifiedBy = @numUserCntID, --  numeric(9, 0)
						@numDomainID = @numDomain
				END
		    
				SET @i = @i + 1
			END
		END
 
		IF	@bitTaxable = 1
		BEGIN
			IF NOT EXISTS(SELECT * FROM dbo.ItemTax WHERE numItemCode = @numItemCode AND numTaxItemID = 0)
			BEGIN
			    INSERT INTO dbo.ItemTax 
				(
					numItemCode,
					numTaxItemID,
					bitApplicable
				)
				VALUES 
				( 
					@numItemCode,
					0,
					1 
				) 						
			END
		END	 
      
		IF @charItemType='S' or @charItemType='N'                                                                       
		BEGIN
			DELETE FROM WareHouseItmsDTL where numWareHouseItemID in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID)
			DELETE FROM WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID
		END

		IF NOT EXISTS (SELECT numItemCode FROM ItemExtendedDetails  where numItemCode=@numItemCode)
		BEGIN
			INSERT INTO ItemExtendedDetails (numItemCode,txtDesc,txtShortDesc) VALUES (@numItemCode,@txtDesc,@txtShortDesc)
		END
		ELSE
		BEGIN
			UPDATE ItemExtendedDetails SET txtDesc=@txtDesc,@txtShortDesc=txtShortDesc WHERE numItemCode=@numItemCode
		END
	END

	IF ISNULL(@numItemGroup,0) = 0 OR ISNULL(@bitMatrix,0) = 1
	BEGIN
		UPDATE WareHouseItems SET monWListPrice = @monListPrice, vcWHSKU=@vcSKU, vcBarCode=@numBarCodeId WHERE numDomainID = @numDomainID AND numItemID = @numItemCode
	END

	IF @vcCategories IS NOT NULL
	BEGIN
		IF @vcCategories <> ''	
		BEGIN
			INSERT INTO ItemCategory( numItemID,numCategoryID )  SELECT @numItemCode,ID from dbo.SplitIDs(@vcCategories,',')	
		END
		ELSE
		BEGIN
			DELETE FROM ItemCategory WHERE numItemID = @numItemCode
		END
	END

	-- SAVE MATRIX ITEM ATTRIBUTES
	IF @numItemCode > 0 AND ISNULL(@bitAttributesChanged,0)=1 AND ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) > 0 AND ISNULL(@bitOppOrderExists,0) <> 1 AND (SELECT COUNT(*) FROM @TempTable) > 0
	BEGIN
		-- DELETE PREVIOUS ITEM ATTRIBUTES
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

		-- INSERT NEW ITEM ATTRIBUTES
		INSERT INTO ItemAttributes
		(
			numDomainID
			,numItemCode
			,FLD_ID
			,FLD_Value
		)
		SELECT
			@numDomainID
			,@numItemCode
			,Fld_ID
			,Fld_Value
		FROM
			@TempTable

		IF (SELECT COUNT(*) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode) > 0
		BEGIN

			-- DELETE PREVIOUS WAREHOUSE ATTRIBUTES
			DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId IN (SELECT ISNULL(numWarehouseItemID,0) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode)

			-- INSERT NEW WAREHOUSE ATTRIBUTES
			INSERT INTO CFW_Fld_Values_Serialized_Items
			(
				Fld_ID,
				Fld_Value,
				RecId,
				bitSerialized
			)                                                  
			SELECT 
				Fld_ID,
				ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
				TEMPWarehouse.numWarehouseItemID,
				0 
			FROM 
				@TempTable  
			OUTER APPLY
			(
				SELECT
					numWarehouseItemID
				FROM 
					WarehouseItems 
				WHERE 
					numDomainID=@numDomainID 
					AND numItemID=@numItemCode
			) AS TEMPWarehouse
		END
	END	 

	UPDATE WareHouseItems SET numReorder=ISNULL(@numReOrder,0) WHERE numDomainID=@numDomain AND numItemID=@numItemCode
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH        
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_InLineEditDataSave')
DROP PROCEDURE USP_InLineEditDataSave
GO
CREATE PROCEDURE [dbo].[USP_InLineEditDataSave] 
( 
 @numDomainID NUMERIC(18,0),
 @numUserCntID NUMERIC(18,0),
 @numFormFieldId NUMERIC(18,0),
 @bitCustom AS BIT = 0,
 @InlineEditValue VARCHAR(8000),
 @numContactID NUMERIC(18,0),
 @numDivisionID NUMERIC(18,0),
 @numWOId NUMERIC(18,0),
 @numProId NUMERIC(18,0),
 @numOppId NUMERIC(18,0),
 @numOppItemId NUMERIC(18,0),
 @numRecId NUMERIC(18,0),
 @vcPageName NVARCHAR(100),
 @numCaseId NUMERIC(18,0),
 @numWODetailId NUMERIC(18,0),
 @numCommID NUMERIC(18,0)
 )
AS 

declare @strSql as nvarchar(4000)
DECLARE @vcDbColumnName AS VARCHAR(100),@vcOrigDbColumnName AS VARCHAR(100),@vcLookBackTableName AS varchar(100)
DECLARE @vcListItemType as VARCHAR(3),@numListID AS numeric(9),@vcAssociatedControlType varchar(20)
declare @tempAssignedTo as numeric(9);set @tempAssignedTo=null           
DECLARE @Value1 AS VARCHAR(18),@Value2 AS VARCHAR(18)

IF  @bitCustom =0
BEGIN
	SELECT @vcDbColumnName=vcDbColumnName,@vcLookBackTableName=(CASE WHEN vcLookBackTableName='T' THEN 'Tickler' ELSE vcLookBackTableName END),
		   @vcListItemType=vcListItemType,@numListID=numListID,@vcAssociatedControlType=vcAssociatedControlType,
		   @vcOrigDbColumnName=vcOrigDbColumnName
	 FROM DycFieldMaster WHERE numFieldId=@numFormFieldId
	 
	IF @vcAssociatedControlType = 'DateField' AND  @InlineEditValue=''
	BEGIN
		SET @InlineEditValue = null
	END

	DECLARE @intExecuteDiv INT=0
	if @vcLookBackTableName = 'DivisionMaster' 
	 BEGIN 
		
	   IF @vcDbColumnName='numFollowUpStatus' --Add to FollowUpHistory
	    Begin
			IF(@numCommID>0 AND @numDivisionID=0)
			BEGIN
				SET @numDivisionID=(SELECT TOP 1 numDivisionId FROM Communication WHERE numCommId=@numCommID)
			END
			declare @numFollow as varchar(10),@binAdded as varchar(20),@PreFollow as varchar(10),@RowCount as varchar(2)                            
			                      
			set @PreFollow=(select count(*)  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID)                          
			set @RowCount=@@rowcount  
			                        
			select   @numFollow=numFollowUpStatus, @binAdded=bintModifiedDate from divisionmaster where numDivisionID=@numDivisionID and numDomainId= @numDomainID                    
			
			if @numFollow <>'0' and @numFollow <> @InlineEditValue                          
			begin                          
				if @PreFollow<>0                          
					begin                          
			                   
						if @numFollow <> (select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID order by numFollowUpStatusID desc)                          
							begin                          
			                      	insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
									values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
							end                          
					end                          
				else                          
					begin                          
						insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
						 values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
					end                          
			   end 
			   
			
			UPDATE  DivisionMaster SET  dtLastFollowUp=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID
		END
		 IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from DivisionMaster where numDivisionID = @numDivisionID and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update DivisionMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID where numDivisionID = @numDivisionID  and numDomainId= @numDomainID  
						UPDATE CompanyInfo SET bintModifiedDate=GETUTCDATE() WHERE numCompanyId IN (SELECT numCompanyId 
																									FROM 
																										DivisionMaster
																									WHERE 
																										numDivisionID = @numDivisionID  and numDomainId= @numDomainID)         
					end					
				else if  (@InlineEditValue ='0')          
					begin          
						update DivisionMaster set numAssignedTo=0 ,numAssignedBy=0 where numDivisionID = @numDivisionID  and numDomainId= @numDomainID
						UPDATE CompanyInfo SET bintModifiedDate=GETUTCDATE() WHERE numCompanyId IN (SELECT numCompanyId 
																									FROM 
																										DivisionMaster
																									WHERE 
																										numDivisionID = @numDivisionID  and numDomainId= @numDomainID)          
					end    
			END
	    UPDATE CompanyInfo SET numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() WHERE numCompanyId=(SELECT TOP 1 numCompanyId FROM DivisionMaster where numDivisionID=@numDivisionID and numDomainID=@numDomainID) and numDomainID=@numDomainID
		
		IF @vcDbColumnName='vcPartnerCode'
		BEGIN
			IF(@InlineEditValue<>'')
			BEGIN
				IF EXISTS(SELECT vcPartnerCode FROM DivisionMaster where numDivisionID<>@numDivisionID and numDomainID=@numDomainID AND vcPartnerCode=@InlineEditValue)
				BEGIN
					SET @intExecuteDiv=1
					SET @InlineEditValue='-#12'
				END
			END
		END
		IF @intExecuteDiv=0
		BEGIN
			SET @strSql='update DivisionMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID'
			
		END
	 END 
	 ELSE if @vcLookBackTableName = 'AdditionalContactsInformation' 
	 BEGIN 
			IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='True'
			BEGIN
				DECLARE @bitPrimaryContactCheck AS BIT             
				DECLARE @numID AS NUMERIC(9) 
				
				SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
					FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID 
                   
					IF @@rowcount = 0 SET @numID = 0             
				
					WHILE @numID > 0            
					BEGIN            
						IF @bitPrimaryContactCheck = 1 
							UPDATE  AdditionalContactsInformation SET bitPrimaryContact = 0 WHERE numContactID = @numID  
                              
						SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
						FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactID > @numID    
                                
						IF @@rowcount = 0 SET @numID = 0             
					END 
				END
				ELSE IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='False'
				BEGIN
					IF (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactId!=@numContactId AND ISNULL(bitPrimaryContact,0)=1) = 0
						SET @InlineEditValue='True'
				END 
				-- Added by Priya(to check no Campaign got engaged wothout contacts email
				ELSE IF @vcDbColumnName = 'numECampaignID' 
				BEGIN
					
					IF EXISTS (select 1 from AdditionalContactsInformation WHERE numContactId = @numContactID AND (vcEmail IS NULL OR vcEmail = ''))
					BEGIN
						RAISERROR('EMAIL_ADDRESS_REQUIRED',16,1)
								RETURN
					END

				END
				
		SET @strSql='update AdditionalContactsInformation set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID and numContactId=@numContactId'
	 END 
	 ELSE if @vcLookBackTableName = 'CompanyInfo' 
	 BEGIN 
		SET @strSql='update CompanyInfo set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCompanyId=(select numCompanyId from divisionmaster where numDivisionID=@numDivisionID and numDomainID=@numDomainID) and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'WorkOrder' 
	 BEGIN 
		DECLARE @numItemCode AS NUMERIC(9)
	 
		IF @numWODetailId>0 AND @vcDbColumnName <> 'numQtyItemsReq'
		BEGIN
			SET @strSql='update WorkOrderDetails set ' + @vcDbColumnName + '=@InlineEditValue where numWOId=@numWOId and numWODetailId=@numWODetailId' 
		END 	
		ELSE
		BEGIN
			SET @strSql='update WorkOrder set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numWOId=@numWOId and numDomainID=@numDomainID'
			
			IF @vcDbColumnName='numQtyItemsReq'
			BEGIN 
				DECLARE @numPickedQty FLOAT = 0
				
				SET @numPickedQty = ISNULL((SELECT
												MAX(numPickedQty)
											FROM
											(
												SELECT 
													WorkOrderDetails.numWODetailId
													,CEILING(SUM(WorkOrderPickedItems.numPickedQty)/numQtyItemsReq_Orig) numPickedQty
												FROM
													WorkOrderDetails
												INNER JOIN
													WorkOrderPickedItems
												ON
													WorkOrderDetails.numWODetailId=WorkOrderPickedItems.numWODetailId
												WHERE
													WorkOrderDetails.numWOId=@numWOID
													AND ISNULL(WorkOrderDetails.numWareHouseItemId,0) > 0
												GROUP BY
													WorkOrderDetails.numWODetailId
													,WorkOrderDetails.numQtyItemsReq_Orig
											) TEMP),0)

				IF @InlineEditValue < ISNULL((@numPickedQty),0)
				BEGIN
					RAISERROR('WORKORDR_QUANITY_CAN_NOT_BE_LESS_THEN_PICKED_QTY',16,1)
					RETURN
				END

				IF EXISTS (SELECT 
								SPDTTL.ID 
							FROM 
								StagePercentageDetailsTask SPDT
							INNER JOIN
								StagePercentageDetailsTaskTimeLog SPDTTL
							ON
								SPDT.numTaskId = SPDTTL.numTaskID 
							WHERE 
								SPDT.numDomainID=@numDomainID 
								AND SPDT.numWorkOrderId = @numWOId)
				BEGIN
					RAISERROR('TASKS_ARE_ALREADY_STARTED',16,1)
					RETURN
				END

				IF EXISTS (SELECT numWOId FROM WorkOrder WHERE numDomainID=@numDomainID AND numWOId=@numWOId AND numWOStatus=23184)
				BEGIN
					RAISERROR('WORKORDR_COMPLETED',16,1)
					RETURN
				END

				SELECT @numItemCode=numItemCode FROM WorkOrder WHERE numWOId=@numWOId and numDomainID=@numDomainID
			
				EXEC USP_UpdateWorkOrderDetailQuantity @numDomainID,@numUserCntID,@numItemCode,@InlineEditValue,@numWOId
			END 
		END	
	 END
	 ELSE if @vcLookBackTableName = 'ProjectsMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if Project is assigned to someone 
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from ProjectsMaster where numProId=@numProId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update ProjectsMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numProId = @numProId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update ProjectsMaster set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numProId = @numProId  and numDomainId= @numDomainID        
					end    
			END
		else IF @vcDbColumnName='numProjectStatus'  ---Updating All Statge to 100% if Completed
			BEGIN
				IF @InlineEditValue='27492'
				BEGIN
					UPDATE PM SET dtCompletionDate=GETUTCDATE(),monTotalExpense=PIE.monExpense,
					monTotalIncome=PIE.monIncome,monTotalGrossProfit=PIE.monBalance,numProjectStatus=27492
					FROM ProjectsMaster PM,dbo.GetProjectIncomeExpense(@numDomainId,@numProId) PIE
					WHERE PM.numProId=@numProId
					
					EXEC USP_ManageProjectCommission @numDomainId,@numProId
						
					Update StagePercentageDetails set tinProgressPercentage=100,bitclose=1 where numProjectID=@numProId

					EXEC dbo.USP_GetProjectTotalProgress
							@numDomainID = @numDomainID, --  numeric(9, 0)
							@numProId = @numProId, --  numeric(9, 0)
							@tintMode = 1 --  tinyint
				END			
				ELSE
				BEGIN
					DELETE from BizDocComission WHERE numDomainID = @numDomainID AND numProId=@numProId AND numComissionID 
						NOT IN (SELECT BC.numComissionID FROM dbo.BizDocComission BC JOIN dbo.PayrollTracking PT 
							ON BC.numComissionID = PT.numComissionID AND BC.numDomainId = PT.numDomainId WHERE BC.numDomainID = @numDomainID AND ISNULL(BC.numProId,0) = @numProId)
				END
			END
			

		SET @strSql='update ProjectsMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numProId=@numProId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'OpportunityMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from OpportunityMaster where numOppId=@numOppId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update OpportunityMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update OpportunityMaster set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID        
					end    
			END
		ELSE IF @vcDbColumnName='numPartner' OR @vcDbColumnName='numPartenerSource'
		 BEGIN
			update OpportunityMaster set numPartner=@InlineEditValue ,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID           
		 END
		
		ELSE IF @vcDbColumnName='tintActive'
		 BEGIN
		 	SET @InlineEditValue= CASE @InlineEditValue WHEN 'True' THEN 1 ELSE 0 END 
		 END
		 ELSE IF @vcDbColumnName='tintSource'
		 BEGIN
			IF CHARINDEX('~', @InlineEditValue)>0
			BEGIN
				SET @Value1=SUBSTRING(@InlineEditValue, 1,CHARINDEX('~', @InlineEditValue)-1) 
				SET @Value2=SUBSTRING(@InlineEditValue, CHARINDEX('~', @InlineEditValue)+1,LEN(@InlineEditValue)) 
			END	
			ELSE
			BEGIN
				SET @Value1=@InlineEditValue
				SET @Value2=0
			END
			
		 	update OpportunityMaster set tintSourceType=@Value2 where numOppId = @numOppId  and numDomainId= @numDomainID        
		 	
		 	SET @InlineEditValue= @Value1
		 END
		ELSE IF @vcDbColumnName='tintOppStatus' --Update Inventory  0=Open,1=Won,2=Lost
		 BEGIN			
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT
			DECLARE @tintCommitAllocation1 TINYINT
			SELECT @tintCommitAllocation1=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainId

			IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainId=@numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1)
			BEGIN
				SET @bitViolatePrice = 1
			END
			SET @intPendingApprovePending=(SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppId AND ISNULL(bitItemPriceApprovalRequired,0)=1)

			IF(@intPendingApprovePending > 0 AND @bitViolatePrice=1 AND @InlineEditValue='1')
			BEGIN
				SET @intExecuteDiv=1
				SET @InlineEditValue='-#123'
			END

			IF ISNULL(@intExecuteDiv,0)=0
			BEGIN
				DECLARE @tintOppStatus AS TINYINT
				SELECT @tintOppStatus=tintOppStatus FROM OpportunityMaster WHERE numOppID=@numOppID              

				IF (@tintOppStatus=0 OR @tintOppStatus=2) AND @InlineEditValue='1' --Open to Won
				BEGIN
					SELECT @numDivisionID=numDivisionID FROM OpportunityMaster WHERE numOppID=@numOppID   
					
					DECLARE @tintCRMType AS TINYINT      
					SELECT @tintCRMType=tintCRMType FROM divisionmaster WHERE numDivisionID =@numDivisionID 

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN        
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=GETUTCDATE()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=GETUTCDATE()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN        
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=GETUTCDATE()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END    

					IF @tintCommitAllocation1=1
					BEGIN
		 				EXEC USP_UpdatingInventoryonCloseDeal @numOppID,0,@numUserCntID
					END

					UPDATE OpportunityMaster SET bintOppToOrder=GETUTCDATE() WHERE numOppId=@numOppId and numDomainID=@numDomainID
					UPDATE OpportunityItems SET bitRequiredWarehouseCorrection=NULL WHERE numOppId=@numOppId
					EXEC USP_OpportunityItems_CheckWarehouseMapping @numDomainID,@numOppId
				END
				ELSE IF (@InlineEditValue='2') -- Win to Lost
				BEGIN
					UPDATE OpportunityMaster SET dtDealLost=GETUTCDATE() WHERE numOppId=@numOppId and numDomainID=@numDomainID
				END
				
				IF ((@tintOppStatus=1 and @InlineEditValue='2') or (@tintOppStatus=1 and @InlineEditValue='0')) AND @tintCommitAllocation1=1 --Won to Lost or Won to Open
						EXEC USP_RevertDetailsOpp @numOppID,0,0,@numUserCntID 

				UPDATE OpportunityMaster SET tintOppStatus = @InlineEditValue WHERE numOppId=@numOppID
				
			END
		 END
		 ELSE IF @vcDbColumnName='vcOppRefOrderNo' --Update vcRefOrderNo OpportunityBizDocs
		 BEGIN
			 IF LEN(ISNULL(@InlineEditValue,''))>0
			 BEGIN
	   			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@InlineEditValue WHERE numOppId=@numOppID
			 END
		 END
		 ELSE IF @vcDbColumnName='numStatus' 
		 BEGIN
			DECLARE @tintOppType AS TINYINT
			select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType from OpportunityMaster where numOppID=@numOppID

			IF @tintOppType=1 AND @tintOppStatus=1
			BEGIN
				DECLARE @numOppStatus AS NUMERIC(18,0) = CAST(@InlineEditValue AS NUMERIC(18,0))
				EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numOppStatus
			END

		 	 IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @InlineEditValue)
			 BEGIN
	   				EXEC USP_ManageOpportunityAutomationQueue
							@numOppQueueID = 0, -- numeric(18, 0)
							@numDomainID = @numDomainID, -- numeric(18, 0)
							@numOppId = @numOppID, -- numeric(18, 0)
							@numOppBizDocsId = 0, -- numeric(18, 0)
							@numOrderStatus = @InlineEditValue, -- numeric(18, 0)
							@numUserCntID = @numUserCntID, -- numeric(18, 0)
							@tintProcessStatus = 1, -- tinyint
							@tintMode = 1 -- TINYINT
			END 
		 END	
		 ELSE IF @vcDbColumnName = 'dtReleaseDate'
		 BEGIN
			IF EXISTS (SELECT 1 FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND tintOppType=1 AND tintOppStatus=0)
			BEGIN
				IF (SELECT dbo.GetListIemName(ISNULL(numPercentageComplete,0)) FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId) = '100'
				BEGIN
					SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), @InlineEditValue),numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
				END
				ELSE
				BEGIN
					RAISERROR('PERCENT_COMPLETE_MUST_BE_100',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), @InlineEditValue),numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
			END
		END
		ELSE IF @vcDbColumnName = 'numPercentageComplete'
		BEGIN
			IF EXISTS (SELECT 1 FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND tintOppType=1 AND tintOppStatus=0 AND dbo.GetListIemName(ISNULL(numPercentageComplete,0))='100')
			BEGIN
				IF EXISTS(SELECT dtReleaseDate FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND dtReleaseDate IS NULL)
				BEGIN
					SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
				END
				ELSE
				BEGIN
					RAISERROR('REMOVE_RELEASE_DATE',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
			END
		END
		ELSE IF @vcDbColumnName = 'numProjectID'
		BEGIN
			UPDATE OpportunityItems SET numProjectID=@InlineEditValue WHERE numOppId=@numOppId
		END
		ELSE IF @vcDbColumnName = 'intUsedShippingCompany'
		BEGIN
			UPDATE OpportunityMaster SET numShippingService=0 WHERE numOppId=@numOppId
		END

		IF(@vcDbColumnName!='tintOppStatus')
		BEGIN
			SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
		END
	 END
	 ELSE if @vcLookBackTableName = 'Cases' 
	 BEGIN 
	 		IF @vcDbColumnName='numAssignedTo' ---Updating if Case is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from Cases where numCaseId=@numCaseId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update Cases set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numCaseId = @numCaseId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update Cases set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numCaseId = @numCaseId  and numDomainId= @numDomainID        
					end    
			END
		UPDATE Cases SET bintModifiedDate=GETUTCDATE() WHERE numCaseId = @numCaseId  and numDomainId= @numDomainID    
		SET @strSql='update Cases set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCaseId=@numCaseId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'Tickler' 
	 BEGIN 	
		IF @vcOrigDbColumnName = 'Description'
		BEGIN
			SET @vcOrigDbColumnName = 'textDetails'
		END
		ELSE IF @vcOrigDbColumnName = 'Activity'
		BEGIN
			SET @vcOrigDbColumnName = 'numActivity'
		END 
		ELSE IF @vcOrigDbColumnName = 'Priority'
		BEGIN
			SET @vcOrigDbColumnName = 'numStatus'
		END

		IF(@vcOrigDbColumnName = 'textDetails' AND EXISTS(SELECT * FROM Communication WHERE numCommId=@numCommID and numDomainID=@numDomainID))
		BEGIN
			SET @strSql='update Communication set ' + @vcOrigDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate() where numCommId=@numCommId and numDomainID=@numDomainID'
		END
		ELSE IF(@vcOrigDbColumnName = 'textDetails' AND EXISTS(SELECT * FROM Activity WHERE ActivityID=@numCommID))
		BEGIN
			SET @strSql='update Activity set Comments=@InlineEditValue where ActivityID=@numCommId'
		END
		ELSE
		BEGIN
			SET @strSql='update Communication set ' + @vcOrigDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate() where numCommId=@numCommId and numDomainID=@numDomainID'
		END
	 END
	 ELSE IF @vcLookBackTableName = 'ExtarnetAccounts'
	 BEGIN
		IF @vcDbColumnName='bitEcommerceAccess'
		BEGIN
			IF LOWER(@InlineEditValue) = 'yes' OR LOWER(@InlineEditValue) = 'true'
			BEGIN
				IF NOT EXISTS (SELECT * FROM ExtarnetAccounts where numDivisionID=@numDivisionID) 
				BEGIN
					DECLARE @numGroupID NUMERIC
					DECLARE @numCompanyID NUMERIC(18,0)
					SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionID

					SELECT TOP 1 @numGroupID=numGroupID FROM dbo.AuthenticationGroupMaster WHERE numDomainID=@numDomainID AND tintGroupType=2
				
					IF @numGroupID IS NULL SET @numGroupID = 0 
					INSERT INTO ExtarnetAccounts (numCompanyID,numDivisionID,numGroupID,numDomainID)                                                
					VALUES(@numCompanyID,@numDivisionID,@numGroupID,@numDomainID)
				END
			END
			ELSE
			BEGIN
				DELETE FROM ExtranetAccountsDtl WHERE numExtranetID IN (SELECT ISNULL(numExtranetID,0) FROM ExtarnetAccounts where numDivisionID=@numDivisionID)
				DELETE FROM ExtarnetAccounts where numDivisionID=@numDivisionID
			END

			SELECT (CASE WHEN LOWER(@InlineEditValue) = 'yes' OR LOWER(@InlineEditValue) = 'true' THEN 'Yes' ELSE 'No' END) AS vcData

			RETURN
		END
	 END
	ELSE IF @vcLookBackTableName = 'ExtranetAccountsDtl'
	BEGIN
		IF @vcDbColumnName='vcPassword'
		BEGIN
			IF LEN(@InlineEditValue) = 0
			BEGIN
				DELETE FROM ExtranetAccountsDtl WHERE numContactID=@numContactID
			END
			ELSE
			BEGIN
				IF EXISTS (SELECT numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID)
				BEGIN
					IF EXISTS (SELECT numExtranetDtlID FROM ExtranetAccountsDtl WHERE numContactID=@numContactID)
					BEGIN
						UPDATE
							ExtranetAccountsDtl
						SET 
							vcPassword=@InlineEditValue
							,tintAccessAllowed=1
						WHERE
							numContactID=@numContactID
					END
					ELSE
					BEGIN
						DECLARE @numExtranetID NUMERIC(18,0)
						SELECT @numExtranetID=numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID

						INSERT INTO ExtranetAccountsDtl (numExtranetID,numContactID,vcPassword,tintAccessAllowed,numDomainID)
						VALUES (@numExtranetID,@numContactID,@InlineEditValue,1,@numDomainID)
					END
				END
				ELSE
				BEGIN
					RAISERROR('Organization e-commerce access is not enabled',16,1)
				END
			END

			SELECT @InlineEditValue AS vcData

			RETURN
		END
	END
	ELSE IF @vcLookBackTableName = 'CustomerPartNumber'
	BEGIN
		IF @vcDbColumnName='CustomerPartNo'
		BEGIN
			SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionID
			
			IF EXISTS(SELECT CustomerPartNoID FROM CustomerPartNumber WHERE numItemCode = @numRecId AND numCompanyID = @numCompanyID)
			BEGIN
				UPDATE CustomerPartNumber SET CustomerPartNo = @InlineEditValue WHERE numItemCode = @numRecId AND numCompanyId = @numCompanyID AND numDomainID = @numDomainId 
			END
			ELSE
			BEGIN
				INSERT INTO CustomerPartNumber
				(numItemCode, numCompanyId, numDomainID, CustomerPartNo)
				VALUES
				(@numRecId, @numCompanyID, @numDomainID, @InlineEditValue)
			END

			SELECT @InlineEditValue AS vcData

			RETURN
		END
		
	END
	ELSE IF @vcLookBackTableName = 'CheckHeader'
	BEGIN
		IF EXISTS (SELECT 1 FROM CheckHeader WHERE numDomainID=@numDomainID AND numCheckNo=@InlineEditValue AND numCheckHeaderID <> @numRecId)
		BEGIN
			RAISERROR('DUPLICATE_CHECK_NUMBER',16,1)
			RETURN
		END

		SET @strSql='UPDATE CheckHeader SET ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate() WHERE numCheckHeaderID=@numRecId and numDomainID=@numDomainID'
	END
	ELSE IF @vcLookBackTableName = 'Item'
	BEGIN
		IF @vcDbColumnName = 'monListPrice'
		BEGIN
			SET @InlineEditValue = REPLACE(@InlineEditValue,',','')

			IF EXISTS (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numRecId AND 1 = (CASE WHEN ISNULL(numItemGroup,0) > 0 THEN (CASE WHEN ISNULL(bitMatrix,0)=1 THEN 1 ELSE 0 END) ELSE 1 END))
			BEGIN
				UPDATE WareHouseItems SET monWListPrice=@InlineEditValue WHERE numDomainID=@numDomainID AND numItemID=@numRecId
				SELECT @InlineEditValue AS vcData
			END
			ELSE
			BEGIN
				RAISERROR('You are not allowed to edit list price for warehouse level matrix item.',16,1)
			END

			RETURN
		END

		SET @strSql='UPDATE Item set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numItemCode=@numRecId AND numDomainID=@numDomainID'
	END
	ELSE IF @vcLookBackTableName = 'Warehouses'
	BEGIN
		IF EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppID=@numOppId AND ISNULL(tintOppStatus,0)=1 AND ISNULL(tintshipped,0)=0)
		BEGIN
			IF EXISTS (SELECT numWareHouseID FROM Warehouses WHERE numDomainID=@numDomainID AND numWareHouseID=@InlineEditValue)
			BEGIN
				DECLARE @numTempOppItemID NUMERIC(18,0)
				DECLARE @numTempItemCode NUMERIC(18,0)
				DECLARE @numOldWarehouseItemID NUMERIC(18,0)
				DECLARE @numNewWarehouseItemID NUMERIC(18,0)
				DECLARE @tintCommitAllocation TINYINT 

				SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID


				DECLARE @TEMPItems TABLE
				(
					ID INT IDENTITY(1,1)
					,numOppItemID NUMERIC(18,0)
					,numItemCode NUMERIC(18,0)
					,numWarehouseItemID NUMERIC(18,0)
				)
				INSERT INTO @TEMPItems
				(
					numOppItemID
					,numItemCode
					,numWarehouseItemID
				)
				SELECT
					numoppitemtCode
					,numItemCode
					,numWarehouseItmsID
				FROM 
					OpportunityItems
				WHERE
					numOppID=@numOppID
					AND (numOppItemtcode=@numOppItemId OR ISNULL(@numOppItemId,0) = 0)

				IF EXISTS (SELECT TI.numItemCode FROM @TEMPItems TI LEFT JOIN WareHouseItems WI ON TI.numItemCode=WI.numItemID AND WI.numWareHouseID=@InlineEditValue WHERE WI.numWareHouseItemID IS NULL)
				BEGIN
					RAISERROR('WAREHOUSE_DOES_NOT_EXISTS_IN_ITEM',16,1)
				END
				ELSE
				BEGIN
					DECLARE @j INT = 1
					DECLARE @jCount INT 

					SELECT @jCount=COUNT(*) FROM @TEMPItems

					WHILE @j <= @jCount
					BEGIN
						SELECT 
							@numTempOppItemID = numOppItemID
							,@numTempItemCode = numItemCode
							,@numOldWarehouseItemID = numWarehouseItemID
						FROM 
							@TEMPItems TI 
						WHERE 
							ID=@j

						SELECT TOP 1
							@numNewWarehouseItemID = numWareHouseItemID
						FROM
							WareHouseItems 
						WHERE 
							numDomainID=@numDomainID 
							AND numItemID=@numTempItemCode 
							AND numWareHouseID=@InlineEditValue
						ORDER BY
							ISNULL(numWLocationID,0) ASC

						IF @numOldWarehouseItemID <> @numNewWarehouseItemID
						BEGIN
							BEGIN TRY
							BEGIN TRANSACTION 
								--REVERTING BACK THE WAREHOUSE ITEMS                  
								IF @tintCommitAllocation=1
								BEGIN               
									EXEC USP_RevertDetailsOpp @numOppID,@numTempOppItemID,0,@numUserCntID                  
								END  

								UPDATE OpportunityItems SET numWarehouseItmsID=@numNewWarehouseItemID WHERE numoppitemtCode=@numTempOppItemID

								IF @tintCommitAllocation=1
								BEGIN               
									EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numTempOppItemID,@numUserCntID
								END  

								UPDATE OpportunityItems SET bitRequiredWarehouseCorrection=0 WHERE numoppitemtCode=@numTempOppItemID
							COMMIT
							END TRY
							BEGIN CATCH
								IF @@TRANCOUNT > 0
									ROLLBACK

								-- Raise an error with the details of the exception
								DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
								SELECT @ErrMsg = ERROR_MESSAGE(), @ErrSeverity = ERROR_SEVERITY()

								RAISERROR(@ErrMsg, @ErrSeverity, 1)
							END CATCH
						END

						SET @j = @j + 1
					END
				END
			END
			ELSE
			BEGIN
				RAISERROR('WAREHOUSE_DOES_NOT_EXISTS',16,1)
			END
		END
	END

	EXECUTE sp_executeSQL @strSql, N'@numDomainID NUMERIC(18,0),@numUserCntID NUMERIC(18,0),@numDivisionID NUMERIC(18,0),@numContactID NUMERIC(18,0),@InlineEditValue VARCHAR(8000),@numWOId NUMERIC(18,0),@numProId NUMERIC(18,0),@numOppId NUMERIC(18,0),@numOppItemId NUMERIC(18,0),@numCaseId NUMERIC(18,0),@numWODetailId NUMERIC(18,0),@numCommID Numeric(18,0),@numRecId NUMERIC(18,0)',
		     @numDomainID,@numUserCntID,@numDivisionID,@numContactID,@InlineEditValue,@numWOId,@numProId,@numOppId,@numOppItemId,@numCaseId,@numWODetailId,@numCommID,@numRecId;
		     
	IF @vcAssociatedControlType='SelectBox'                                                    
       BEGIN       
			IF @vcDbColumnName='tintSource' AND @vcLookBackTableName = 'OpportunityMaster'
			BEGIN
				SELECT dbo.fn_GetOpportunitySourceValue(@Value1,@Value2,@numDomainID) AS vcData                                             
			END
			ELSE IF @vcDbColumnName='numContactID'
			BEGIN
				SELECT dbo.fn_GetContactName(@InlineEditValue)
			END
			ELSE IF @vcDbColumnName='numPartner' OR @vcDbColumnName='numPartenerSource'
			BEGIN
				SET @intExecuteDiv=1
				SELECT D.vcPartnerCode+'-'+C.vcCompanyName  AS vcData FROM DivisionMaster AS D 
				LEFT JOIN CompanyInfo AS C
				ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=@InlineEditValue
			END
			ELSE IF @vcDbColumnName='tintOppStatus' AND @intExecuteDiv=1
			BEGIN
				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numReleaseStatus'
			BEGIN
				If @InlineEditValue = '1'
				BEGIN
					SELECT 'Open'
				END
				ELSE IF @InlineEditValue = '2'
				BEGIN
					SELECT 'Purchased'
				END
				ELSE
				BEGIN
					SELECT ''
				END
			END
			ELSE IF @vcDbColumnName='numPartenerContact'
			BEGIN
				SET @InlineEditValue=(SELECT TOP 1 A.vcGivenName FROM AdditionalContactsInformation AS A 
										LEFT JOIN DivisionMaster AS D 
										ON D.numDivisionID=A.numDivisionId
										WHERE D.numDomainID=@numDomainID AND A.numCOntactId=@InlineEditValue )
				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numPartenerSource'
			BEGIN
				
				SET @InlineEditValue=(SELECT D.vcPartnerCode+'-'+C.vcCompanyName  AS vcData FROM DivisionMaster AS D 
				LEFT JOIN CompanyInfo AS C
				ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=@InlineEditValue)

				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numShippingService'
			BEGIN
				SELECT ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = @InlineEditValue),'') AS vcData
			END
			ELSE IF @vcDbColumnName='intDropShip'
			BEGIN
				 SELECT CASE WHEN ISNULL(@InlineEditValue,0)=1 THEN 'Not Available'
						 WHEN ISNULL(@InlineEditValue,0)=2 THEN 'Blank Available'
						 WHEN ISNULL(@InlineEditValue,0)=3 THEN 'Vendor Label'
						 WHEN ISNULL(@InlineEditValue,0)=4 THEN 'Private Label'
						 ELSE 'Select One'
					END AS vcData
			END
			ELSE IF @vcDbColumnName = 'numProjectID'
			BEGIN
				SELECT ISNULL((SELECT vcProjectName FROM ProjectsMaster WHERE numDomainID=@numDomainID AND numProID=@InlineEditValue),'-') AS vcData
			END
			else
			BEGIN
				SELECT dbo.fn_getSelectBoxValue(@vcListItemType,@InlineEditValue,@vcDbColumnName) AS vcData                                             
			END
		end 
		ELSE
		BEGIN
			IF @vcDbColumnName='tintActive'
		  	BEGIN
				SET @InlineEditValue= CASE @InlineEditValue WHEN 1 THEN 'True' ELSE 'False' END 
			END
			ELSE IF @vcDbColumnName='vcWareHouse' AND ISNUMERIC(@InlineEditValue) = 1
			BEGIN
				SELECT @InlineEditValue=vcWareHouse FROM Warehouses WHERE numDomainID=@numDomainID AND numWareHouseID=@InlineEditValue
			END

			IF @vcAssociatedControlType = 'Check box' or @vcAssociatedControlType = 'Checkbox'
			BEGIN
	 			SET @InlineEditValue= Case @InlineEditValue WHEN 'True' THEN 'Yes' ELSE 'No' END
			END
			
		    SELECT @InlineEditValue AS vcData
		END  
		
	IF @vcLookBackTableName = 'AdditionalContactsInformation' AND @vcDbColumnName = 'numECampaignID'
	BEGIN
		--Manage ECampiagn When Campaign is changed from inline edit
		DECLARE @Date AS DATETIME 
		SELECT @Date = dbo.[GetUTCDateWithoutTime]()
		DECLARE @numECampaignID AS NUMERIC(18,0)
		SET @numECampaignID = @InlineEditValue

		IF @numECampaignID = -1 
		SET @numECampaignID=0
	 
 		EXEC [USP_ManageConEmailCampaign]
		@numConEmailCampID = 0, --  numeric(9, 0)
		@numContactID = @numContactID, --  numeric(9, 0)
		@numECampaignID = @numECampaignID, --  numeric(9, 0)
		@intStartDate = @Date, --  datetime
		@bitEngaged = 1, --  bit
		@numUserCntID = @numUserCntID --  numeric(9, 0)
	END  
END
ELSE
BEGIN	 

	SET @InlineEditValue=Cast(@InlineEditValue as varchar(1000))
	SELECT @vcAssociatedControlType=Fld_type
	 FROM CFW_Fld_Master WHERE Fld_Id=@numFormFieldId 

	IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	    SET @InlineEditValue=(case when @InlineEditValue='False' then 0 ELSE 1 END)
	END

	IF @vcPageName='organization'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
  
	  END
	ELSE IF @vcPageName='contact'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Cont SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Cont (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END
			
			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Cont_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='project'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Pro SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Pro (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Pro_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END
	ELSE IF @vcPageName='opp'
	BEGIN 
		DECLARE @dtDate AS DATETIME = GETUTCDATE()

		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE 
				CFW_Fld_Values_Opp 
			SET 
				Fld_Value=@InlineEditValue ,
				numModifiedBy = @numUserCntID,
				bintModifiedDate = @dtDate
			WHERE 
				FldDTLID= (SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_Fld_Values_Opp (Fld_ID,Fld_Value,RecId,numModifiedBy,bintModifiedDate) VALUES (@numFormFieldId,@InlineEditValue,@numRecId,@numUserCntID,@dtDate)         
		END

		UPDATE OpportunityMaster SET numModifiedBy=@numUserCntID,bintModifiedDate=@dtDate WHERE numOppID = @numRecId 

		--Added By :Sachin Sadhu ||Date:14thOct2014
		--Purpose  :To manage CustomFields in Queue
		EXEC USP_CFW_Fld_Values_Opp_CT
				@numDomainID =@numDomainID,
				@numUserCntID =@numUserCntID,
				@numRecordID =@numRecId ,
				@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='oppitems'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_OppItems CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE 
				CFW_Fld_Values_OppItems 
			SET 
				Fld_Value=@InlineEditValue
			WHERE 
				FldDTLID= (SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_OppItems CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_Fld_Values_OppItems(Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
		END
	END
	ELSE IF @vcPageName='case'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Case SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Case (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Case_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END
	ELSE IF @vcPageName='item'
	BEGIN
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Item CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE CFW_FLD_Values_Item SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Item CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_FLD_Values_Item (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
		END
	END


	IF @vcAssociatedControlType = 'SelectBox'            
	BEGIN            
		    
		 SELECT vcData FROM ListDetails WHERE numListItemID=@InlineEditValue            
	END
	ELSE IF @vcAssociatedControlType = 'CheckBoxList'
	BEGIN
		DECLARE @str AS VARCHAR(MAX)
		SET @str = 'SELECT STUFF((SELECT CONCAT('','', vcData) FROM ListDetails WHERE numlistitemid IN (' + (CASE WHEN LEN(@InlineEditValue) > 0 THEN @InlineEditValue ELSE '0' END) + ') FOR XML PATH('''')), 1, 1, '''')'
		EXEC(@str)
	END
	ELSE IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	 	select CASE @InlineEditValue WHEN 1 THEN 'Yes' ELSE 'No' END AS vcData
	END
	ELSE
	BEGIN
		SELECT @InlineEditValue AS vcData
	END 
END

 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetPriceLevelEcommHtml')
DROP PROCEDURE USP_Item_GetPriceLevelEcommHtml
GO
CREATE PROCEDURE [dbo].[USP_Item_GetPriceLevelEcommHtml]
	@numDomainID NUMERIC(18,0)
	,@numSiteID NUMERIC(18,0)
	,@numItemCode AS NUMERIC(18,0)
AS  
BEGIN  
	DECLARE @vcHtml VARCHAR(MAX) = ''

	IF (SELECT numDefaultSalesPricing FROM Domain WHERE numDomainId=@numDomainID) = 1
	BEGIN
		DECLARE @numCurrencyID NUMERIC(18,0)

		DECLARE @TEMPPriceLevel TABLE
		(
			ID INT IDENTITY(1,1)
			,numQtyFrom FLOAT
			,numQtyTo FLOAT
			,vcPrice VARCHAR(30)
			,bitLastRow BIT
		)

		-- FIRST CHECK SITE CURRENY
		SELECT @numCurrencyID=numCurrencyID FROM Sites WHERE numDomainID=@numDomainID AND numSiteID=@numSiteID

		IF NOT EXISTS (SELECT numPricingID FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0)=0 AND ISNULL(numCurrencyID,0)=ISNULL(@numCurrencyID,0))
		BEGIN
			SELECT @numCurrencyID=numCurrencyID FROM Domain WHERE numDomainId=@numDomainID
		END

		IF NOT EXISTS (SELECT numPricingID FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0)=0 AND ISNULL(numCurrencyID,0)=ISNULL(@numCurrencyID,0))
		BEGIN
			SET @numCurrencyID = 0
		END

		IF EXISTS (SELECT numPricingID FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0)=0 AND ISNULL(numCurrencyID,0)=ISNULL(@numCurrencyID,0))
		BEGIN
			DECLARE @numBaseUnit NUMERIC(18,0)
			DECLARE @numSaleUnit NUMERIC(18,0)
			DECLARE @monListPrice DECIMAL(20,5)
			DECLARE @monVendorCost DECIMAL(20,5)
			DECLARE @fltExchangeRate FLOAT 
			DECLARE @fltUOMConversionFactor FLOAT

			SET @fltExchangeRate = ISNULL((SELECT fltExchangeRate FROM Currency WHERE numDomainID=@numDomainID AND numCurrencyID=ISNULL(@numCurrencyID,0)),1)

			IF ISNULL(@fltExchangeRate,0) = 0
			BEGIN
				SET @fltExchangeRate = 1
			END

			SELECT
				@numBaseUnit = ISNULL(numBaseUnit,0),
				@numSaleUnit = ISNULL(numSaleUnit,0),
				@monListPrice = (CASE 
									WHEN EXISTS (SELECT ID FROM ItemCurrencyPrice WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode AND numCurrencyID=ISNULL(@numCurrencyID,0))  
									THEN ISNULL((SELECT monListPrice FROM ItemCurrencyPrice WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode AND numCurrencyID=ISNULL(@numCurrencyID,0)),0)
									ELSE (CASE WHEN @fltExchangeRate <> 1 THEN CAST((ISNULL(Item.monListPrice,0) / @fltExchangeRate) AS INT) ELSE ISNULL(Item.monListPrice,0) END)
								END),
				@monVendorCost = ISNULL((SELECT (CASE WHEN @fltExchangeRate <> 1 THEN CAST((ISNULL(monCost,0) / @fltExchangeRate) AS INT) ELSE ISNULL(monCost,0) END) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,Item.numDomainID,Item.numPurchaseUnit) FROM Vendor WHERE numVendorID=Item.numVendorID AND Vendor.numItemCode=Item.numItemCode),0)
			FROM
				Item
			WHERE
				numDomainID = @numDomainID
				AND numItemCode= @numItemCode

			SET @fltUOMConversionFactor = dbo.fn_UOMConversion(@numSaleUnit,@numItemCode,@numDomainId,@numBaseUnit)
			IF ISNULL(@fltUOMConversionFactor,0) = 0
			BEGIN
				SET @fltUOMConversionFactor = 1
			END

			SET @monListPrice = @monListPrice * @fltUOMConversionFactor
			SET @monVendorCost = @monVendorCost * @fltUOMConversionFactor


			DECLARE @varCurrSymbol VARCHAR(100)
			SET @varCurrSymbol = ISNULL((SELECT varCurrSymbol FROM Currency WHERE numDomainID=@numDomainID AND numCurrencyID=@numCurrencyID),'$')

			INSERT INTO @TEMPPriceLevel
			(
				numQtyFrom
				,numQtyTo
				,vcPrice
			)
			SELECT
				intFromQty
				,intToQty
				,(CASE tintRuleType
					WHEN 1 -- Deduct From List Price
					THEN
						(CASE tintDiscountType 
							WHEN 1 -- PERCENT
							THEN FORMAT(ISNULL((CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END),0),'#,##0.#####')
							WHEN 2 -- FLAT AMOUNT
							THEN CONCAT(FORMAT((CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END),'#,##0.#####'),' off on total amount')
							WHEN 3 -- NAMED PRICE
							THEN FORMAT((CASE WHEN @numCurrencyID <> numCurrencyID THEN (CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) * @fltUOMConversionFactor) ELSE (ISNULL(decDiscount,0) * @fltUOMConversionFactor) END),'#,##0.#####')
						END)
					WHEN 2 -- Add to primary vendor cost
					THEN
						(CASE tintDiscountType 
							WHEN 1  -- PERCENT
							THEN FORMAT(@monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100)),'#,##0.#####')
							WHEN 2 -- FLAT AMOUNT
							THEN CONCAT(FORMAT((CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END),'#,##0.#####'),' extra on total amount')
							WHEN 3 -- NAMED PRICE
							THEN FORMAT((CASE WHEN @numCurrencyID <> numCurrencyID THEN (CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) * @fltUOMConversionFactor) ELSE (ISNULL(decDiscount,0) * @fltUOMConversionFactor) END),'#,##0.#####')
						END)
					WHEN 3 -- Named price
					THEN
						FORMAT((CASE WHEN @numCurrencyID <> numCurrencyID THEN  (CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) * @fltUOMConversionFactor) ELSE (ISNULL(decDiscount,0) * @fltUOMConversionFactor) END),'#,##0.#####')
					END
				) 
			FROM
				PricingTable
			WHERE
				numItemCode=@numItemCode
				AND ISNULL(numPriceRuleID,0) = 0
				AND ISNULL(numCurrencyID,0)=ISNULL(@numCurrencyID,0)
			ORDER BY 
				numPricingID
		END

		IF EXISTS (SELECT * FROM @TEMPPriceLevel)
		BEGIN
			DECLARE @vcUnitName VARCHAR(100)
			SET @vcUnitName = ISNULL((SELECT vcUnitName FROM UOM WHERE numDomainId=@numDomainID AND numUOMId=@numSaleUnit),'each')

			SET @vcHtml = CONCAT('<table class="table table-bordered table-pricelevel"><tr><th>Qty (',@vcUnitName,')</th><th>Price per ',@vcUnitName,'</th></tr>')
			SET @vchtml = CONCAT(@vchtml,STUFF((SELECT
													CONCAT('<tr><td>',FORMAT(numQtyFrom,'#,##0.#####'),' - ',FORMAT(numQtyTo,'#,##0.#####'),'</td><td><span>',@varCurrSymbol,'</span>',vcPrice,'</td></tr>')
												FROM 
													@TEMPPriceLevel 
												ORDER BY
													ID
												FOR XML PATH(''), TYPE).value('.', 'varchar(max)'),1,0,''))

			SET @vcHtml = CONCAT(@vcHtml,'</table>')
		END
	END

	SELECT @vcHtml AS vcHtml
END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemDetails_GetChildKitsOfKit' ) 
    DROP PROCEDURE USP_ItemDetails_GetChildKitsOfKit
GO

CREATE PROCEDURE USP_ItemDetails_GetChildKitsOfKit  
(  
	@numDomainID NUMERIC(18,0),  
	@numItemCode NUMERIC(18,0)
)  
AS  
BEGIN  
	-- SET NOCOUNT ON added to prevent extra result sets from  
	-- interfering with SELECT statements.  
	SET NOCOUNT ON;
	SELECT
		Item.numItemCode
		,Item.vcItemName
		,ISNULL(Item.bitKitSingleSelect,0) bitKitSingleSelect
		,ISNULL(ItemDetails.tintView,1) tintView
		,ISNULL(STUFF((SELECT 
					CONCAT(',',numSecondaryListID)
				FROM 
					FieldRelationship FR
				WHERE 
					FR.numDomainID=@numDomainID
					AND FR.numModuleID = 14
					AND numPrimaryListID=Item.numItemCode
					AND numSecondaryListID IN (SELECT numChildItemID FROM ItemDetails WHERE numItemKitID=@numItemCode)
				GROUP BY
					numSecondaryListID
				FOR XML PATH('')),1,1,''),'') vcDependedKits
		,ISNULL(STUFF((SELECT 
					CONCAT(',',numPrimaryListID)
				FROM 
					FieldRelationship FR 
				WHERE 
					FR.numDomainID=@numDomainID
					AND FR.numModuleID = 14 
					AND numSecondaryListID=Item.numItemCode
					AND numPrimaryListID IN (SELECT numChildItemID FROM ItemDetails WHERE numItemKitID=@numItemCode)
				GROUP BY
					numPrimaryListID
				FOR XML PATH('')),1,1,''),'') vcParentKits
	FROM
		ItemDetails
	INNER JOIN
		Item
	ON
		ItemDetails.numChildItemID = Item.numItemCode
		AND ISNULL(Item.bitKitParent,0) = 1
	WHERE
		ItemDetails.numItemKitID = @numItemCode
	ORDER BY
		ISNULL(sintOrder,0)
END  
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ItemDetails_GetDependedChildKitItems')
DROP PROCEDURE USP_ItemDetails_GetDependedChildKitItems
GO
CREATE PROCEDURE [dbo].[USP_ItemDetails_GetDependedChildKitItems]  
	@numDomainID NUMERIC(18,0)
	,@numMainKitItemCode NUMERIC(18,0)
	,@numChildKitItemCode NUMERIC(18,0)
	,@numChildKitSelectedItem VARCHAR(MAX)
AS  
BEGIN  
	DECLARE @TEMP TABLE
	(
		numPrimaryListID NUMERIC(18,0)
		,numSelectedValue NUMERIC(18,0)
	)

	INSERT INTO @TEMP
	(
		numPrimaryListID
		,numSelectedValue
	)
	SELECT 
		CAST(SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)) AS NUMERIC)
		,CAST(SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1,LEN(OutParam)) AS NUMERIC)
	FROM 
		dbo.SplitString(@numChildKitSelectedItem,',')

	SELECT 
		numSecondaryListID AS numChildKitItemCode
		,ISNULL(I.bitKitSingleSelect,1) bitKitSingleSelect
		,ISNULL((SELECT tintView FROM ItemDetails ID WHERE ID.numItemKitID=@numMainKitItemCode AND ID.numChildItemID=numSecondaryListID),1) tintView
	FROM
		FieldRelationship FR
	INNER JOIN
		Item I
	ON
		I.numItemCode = FR.numSecondaryListID
	WHERE
		FR.numDomainID=@numDomainID
		AND FR.numModuleID = 14
		AND FR.numPrimaryListID=@numChildKitItemCode
		AND (SELECT COUNT(*) FROM FieldRelationshipDTL FRD WHERE FRD.numFieldRelID=FR.numFieldRelID) > 0
	ORDER BY
		FR.numSecondaryListID

	DECLARE @TEMPItems TABLE
	(
		numPrimaryListID NUMERIC(18,0)
		,numChildKitItemCode NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,vcItemName VARCHAR(300)
		,vcDisplayText VARCHAR(500)
		,UnitName VARCHAR(100)
		,Qty FLOAT
	)

	INSERT INTO @TEMPItems
	(
		numPrimaryListID
		,numChildKitItemCode
		,numItemCode
		,vcItemName
		,vcDisplayText
		,UnitName
		,Qty
	)
	SELECT 
		FR.numPrimaryListID,
		FR.numSecondaryListID AS numChildKitItemCode,
		Item.numItemCode,
		ISNULL(Item.vcItemName,'') AS vcItemName,
		dbo.GetKitChildItemsDisplay(@numMainKitItemCode,FR.numSecondaryListID,Item.numItemCode) AS vcDisplayText,
		ISNULL(UOM.vcUnitName,'') AS UnitName,
		ItemDetails.numQtyItemsReq as Qty
	FROM
		FieldRelationshipDTL FRD
	INNER JOIN
		FieldRelationship FR
	ON
		FRD.numFieldRelID = FR.numFieldRelID
	INNER JOIN
		@TEMP T
	ON
		T.numPrimaryListID = FR.numPrimaryListID
		AND FRD.numPrimaryListItemID = T.numSelectedValue
	INNER JOIN
		ItemDetails
	ON
		FR.numSecondaryListID = ItemDetails.numItemKitID
	INNER JOIN
		Item
	ON
		ItemDetails.numChildItemID = Item.numItemCode
		AND Item.numItemCode = FRD.numSecondaryListItemID
	LEFT JOIN 
		UOM
	ON 
		ItemDetails.numUOMId = UOM.numUOMId
	WHERE
		FR.numDomainID=@numDomainID
		AND FR.numModuleID = 14
		AND FRD.numSecondaryListItemID <> -1
		AND FR.numSecondaryListID IN (SELECT 
											FR.numSecondaryListID
										FROM
											FieldRelationship FR
										WHERE
											FR.numDomainID=@numDomainID
											AND FR.numModuleID = 14
											AND FR.numPrimaryListID=@numChildKitItemCode
											AND (SELECT COUNT(*) FROM FieldRelationshipDTL FRD WHERE FRD.numFieldRelID=FR.numFieldRelID) > 0)
	UNION
	SELECT 
		FR.numPrimaryListID,
		FR.numSecondaryListID AS numChildKitItemCode,
		-1,
		'' AS vcItemName,
		'' AS vcDisplayText,
		'' AS UnitName,
		0 as Qty
	FROM
		FieldRelationshipDTL FRD
	INNER JOIN
		FieldRelationship FR
	ON
		FRD.numFieldRelID = FR.numFieldRelID
	INNER JOIN
		@TEMP T
	ON
		T.numPrimaryListID = FR.numPrimaryListID
		AND FRD.numPrimaryListItemID = T.numSelectedValue
	INNER JOIN
		ItemDetails
	ON
		FR.numSecondaryListID = ItemDetails.numItemKitID
	WHERE
		FR.numDomainID=@numDomainID
		AND FR.numModuleID = 14
		AND FRD.numSecondaryListItemID = -1
		AND FR.numSecondaryListID IN (SELECT 
											FR.numSecondaryListID
										FROM
											FieldRelationship FR
										WHERE
											FR.numDomainID=@numDomainID
											AND FR.numModuleID = 14
											AND FR.numPrimaryListID=@numChildKitItemCode
											AND (SELECT COUNT(*) FROM FieldRelationshipDTL FRD WHERE FRD.numFieldRelID=FR.numFieldRelID) > 0)

	
	SELECT 
		* 
	FROM
	(
		SELECT 
			numChildKitItemCode
			,numItemCode
			,vcItemName
			,vcDisplayText
			,UnitName
			,Qty
		FROM 
			@TEMPItems TI
		GROUP BY
			numChildKitItemCode
			,numItemCode
			,vcItemName
			,vcDisplayText
			,UnitName
			,Qty
		HAVING 
			1 = (CASE 
					WHEN ISNULL((SELECT COUNT(DISTINCT TIInner.numPrimaryListID) FROM @TEMPItems TIInner WHERE TIInner.numChildKitItemCode = TI.numChildKitItemCode),0) > 1
					THEN (CASE WHEN COUNT(*) > 1 THEN 1 ELSE 0 END)
					ELSE 1 
				END)
	) T
	WHERE
		numItemCode <> -1
END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemDetails_GetKitChildItems' ) 
    DROP PROCEDURE USP_ItemDetails_GetKitChildItems
GO
CREATE PROCEDURE USP_ItemDetails_GetKitChildItems  
(  
	@numDomainID NUMERIC(18,0),  
	@numKitItemID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0)
)  
AS  
BEGIN  
	-- SET NOCOUNT ON added to prevent extra result sets from  
	-- interfering with SELECT statements.  
	SET NOCOUNT ON;  

	SELECT 
		@numItemCode AS numKitItemCode,
		Item.numItemCode,
		ISNULL(Item.vcItemName,'') AS vcItemName,
		dbo.GetKitChildItemsDisplay(@numKitItemID,@numItemCode,Item.numItemCode) AS vcDisplayText,
		ISNULL(UOM.vcUnitName,'') AS UnitName,
		ISNULL(ItemDetails.numQtyItemsReq,0) as Qty,
		ISNULL((SELECT tintView FROM ItemDetails ID WHERE ID.numItemKitID=@numKitItemID AND ID.numChildItemID=@numItemCode),1) tintView
	FROM 
		ItemDetails
	LEFT JOIN 
		UOM
	ON 
		ItemDetails.numUOMId = UOM.numUOMId
	INNER JOIN
		Item
	ON
		ItemDetails.numChildItemID = Item.numItemCode
	WHERE
		ItemDetails.numItemKitID = @numItemCode 
	ORDER BY 
		sintOrder
END  
GO

GO
/****** Object:  StoredProcedure [dbo].[USP_ItemList1]    Script Date: 05/07/2009 18:14:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop Jayaraj                                        
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_itemlist1' ) 
    DROP PROCEDURE usp_itemlist1
GO
CREATE PROCEDURE [dbo].[USP_ItemList1]
    @ItemClassification AS NUMERIC(9) = 0,
    @IsKit AS TINYINT,
    @SortChar CHAR(1) = '0',
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT, 
    @columnName AS VARCHAR(50),
    @columnSortOrder AS VARCHAR(10),
    @numDomainID AS NUMERIC(18,0) = 0,
    @ItemType AS CHAR(1),
    @bitSerialized AS BIT,
    @numItemGroup AS NUMERIC(9),
    @bitAssembly AS BIT,
    @numUserCntID AS NUMERIC(18,0),
    @Where	AS VARCHAR(MAX),
    @IsArchive AS BIT = 0,
    @byteMode AS TINYINT,
	@bitAsset as BIT=0,
	@bitRental as BIT=0,
	@bitContainer AS BIT=0,
	@SearchText VARCHAR(300),
	@vcRegularSearchCriteria varchar(MAX)='',
	@vcCustomSearchCriteria varchar(MAX)='',
	@tintDashboardReminderType TINYINT = 0,
	@bitMultiSelect AS TINYINT,
	@numFilteredWarehouseID AS NUMERIC(18,0)=0,
	@vcAsile AS VARCHAR(500)='',
	@vcRack AS VARCHAR(500)='',
	@vcShelf AS VARCHAR(500)='',
	@vcBin AS VARCHAR(500)='',
	@bitApplyFilter As BIT=0,
	@tintUserRightType TINYINT
AS 
BEGIN	
	DECLARE @numWarehouseID NUMERIC(18,0)
	SELECT TOP 1 @numWarehouseID = numWareHouseID FROM Warehouses WHERE numDomainID = @numDomainID

	SET @vcCustomSearchCriteria = REPLACE(@vcCustomSearchCriteria,'ctl00_ctl00_MainContent_GridPlaceHolder_gvSearch_ctl00_ctl02_ctl00_','')

	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1),Grp_Id TINYINT
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = 0
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = 0
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 21 AND numRelCntType=0 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			21,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=21 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			21,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=21 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType,0
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=21 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'',Grp_Id
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=21 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				21,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=21 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType,0
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = 0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'',Grp_Id
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = 0
		ORDER BY 
			tintOrder asc  
	END   

	IF @byteMode=0
	BEGIN
		DECLARE @strColumns AS VARCHAR(MAX)
		SET @strColumns = ' I.numItemCode,I.vcItemName,ISNULL(DivisionMaster.numDivisionID,0) numDivisionID,ISNULL(DivisionMaster.tintCRMType,0) tintCRMType, ' + CAST(ISNULL(@numWarehouseID,0) AS VARCHAR) + ' AS numWarehouseID, I.numDomainID,0 AS numRecOwner,0 AS numTerID, I.charItemType,I.bitSerialized,I.bitLotNo, I.numItemCode AS [numItemCode~211~0]'

		DECLARE @tintOrder AS TINYINT                                                  
		DECLARE @vcFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(3)                                             
		DECLARE @vcListItemType1 AS VARCHAR(1)                                                 
		DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
		DECLARE @numListID AS NUMERIC(9)                                                  
		DECLARE @vcDbColumnName VARCHAR(50)                      
		DECLARE @WhereCondition VARCHAR(MAX)                       
		DECLARE @vcLookBackTableName VARCHAR(2000)                
		DECLARE @bitCustom AS BIT                  
		DECLARE @numFieldId AS NUMERIC  
		DECLARE @bitAllowSorting AS CHAR(1)   
		DECLARE @bitAllowEdit AS CHAR(1)                   
        DECLARE @vcColumnName AS VARCHAR(200)
		DECLARE @Grp_ID AS TINYINT
		SET @tintOrder = 0                                                  
		SET @WhereCondition = ''                 
                   
		DECLARE @ListRelID AS NUMERIC(9) 
		DECLARE @SearchQuery AS VARCHAR(MAX) = ''

		IF LEN(@SearchText) > 0
		BEGIN
			SET @SearchQuery = ' I.numItemCode LIKE ''%' + @SearchText + '%'' OR I.vcItemName LIKE ''%' + @SearchText + '%'' OR I.vcModelID LIKE ''%' + @SearchText + '%'' OR I.vcSKU LIKE ''%' + @SearchText + '%'' OR cmp.vcCompanyName LIKE ''%' + @SearchText + '%'' OR I.vcManufacturer LIKE ''%' + @SearchText + '%'''
		END


		DECLARE @j INT = 0
  
		SELECT TOP 1
				@tintOrder = tintOrder + 1,
				@vcDbColumnName = vcDbColumnName,
				@vcFieldName = vcFieldName,
				@vcAssociatedControlType = vcAssociatedControlType,
				@vcListItemType = vcListItemType,
				@numListID = numListID,
				@vcLookBackTableName = vcLookBackTableName,
				@bitCustom = bitCustomField,
				@numFieldId = numFieldId,
				@bitAllowSorting = bitAllowSorting,
				@bitAllowEdit = bitAllowEdit,
				@ListRelID = ListRelID
		FROM    #tempForm --WHERE bitCustomField=1
		ORDER BY tintOrder ASC        
		IF((SELECT COUNt(*) FROM #tempForm WHERE vcDbColumnName='StockQtyCount'  OR  vcDbColumnName='StockQtyAdjust')  >0)
		BEGIN
		IF ISNULL(@numFilteredWarehouseID,0)=0
		BEGIN
			SET @strColumns = @strColumns+ ',0 as numIndividualOnHandQty,0 as numAvailabilityQty,0 AS monAverageCost,0 as numWareHouseItemId,0 AS numAssetChartAcntId'
		END   
		ELSE IF ISNULL(@numFilteredWarehouseID,0)>0
		BEGIN
			SET @strColumns = @strColumns+ ',WarehouseItems.numOnHand as numIndividualOnHandQty,(WarehouseItems.numOnHand + WarehouseItems.numAllocation) as numAvailabilityQty,I.monAverageCost,WarehouseItems.numWareHouseItemId as numWareHouseItemId,I.numAssetChartAcntId,
			((isnull(WarehouseItems.numOnHand,0) + isnull(WarehouseItems.numAllocation,0))*isnull(I.monAverageCost ,0)) as StockValue'
		END  
		END
		WHILE @tintOrder > 0                                                  
		BEGIN
			IF @bitCustom = 0
			BEGIN
				DECLARE  @Prefix  AS VARCHAR(5)

				IF @vcLookBackTableName = 'Item'
					SET @Prefix = 'I.'
				ELSE IF @vcLookBackTableName = 'CompanyInfo'
					SET @Prefix = 'cmp.'
				ELSE IF @vcLookBackTableName = 'Vendor'
					SET @Prefix = 'V.'

				SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

				IF @vcAssociatedControlType='SelectBox' 
				BEGIN
					IF @vcListItemType='LI' 
					BEGIN
						SET @strColumns = @strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'
						SET @WhereCondition= @WhereCondition +' LEFT JOIN ListDetails L'+ convert(varchar(3),@tintOrder)+ ' ON L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                      
					END
					ELSE IF @vcListItemType = 'UOM'
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = I.' + @vcDbColumnName + '),'''')' + ' ['+ @vcColumnName+']'
					END
					ELSE IF @vcListItemType = 'IG'
					BEGIN
						SET @strColumns = @strColumns+ ',ISNULL((SELECT vcItemGroup FROM ItemGroups WHERE numItemGroupID = I.numItemGroup),'''')' + ' ['+ @vcColumnName+']'  
					END
				END
				ELSE IF @vcDbColumnName = 'vcPathForTImage'
		   		BEGIN
					SET @strColumns = @strColumns + ',ISNULL(II.vcPathForTImage,'''') AS ' + ' ['+ @vcColumnName+']'                  
					SET @WhereCondition = @WhereCondition + ' LEFT JOIN ItemImages II ON II.numItemCode = I.numItemCode AND bitDefault = 1'
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'monListPrice'
				BEGIN
					SET @strColumns = @strColumns + ',CASE WHEN I.charItemType=''P'' THEN ISNULL((SELECT TOP 1 monWListPrice FROM WarehouseItems WHERE numDomainID=I.numDomainID AND numItemID=I.numItemCode AND monWListPrice > 0),0.00) ELSE ISNULL(I.monListPrice,0.00) END AS ' + ' ['+ @vcColumnName+']'
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'ItemType'
				BEGIN
					SET @strColumns = @strColumns + ',' + '(CASE 
																WHEN I.charItemType=''P'' 
																THEN 
																	CASE 
																		WHEN ISNULL(I.bitAssembly,0) = 1 THEN ''Assembly''
																		WHEN ISNULL(I.bitKitParent,0) = 1 THEN ''Kit''
																		WHEN ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=0 THEN ''Asset''
																		WHEN ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=1 THEN ''Rental Asset''
																		WHEN ISNULL(I.bitSerialized,0) = 1 AND ISNULL(I.bitAsset,0) = 0  THEN ''Serialized''
																		WHEN ISNULL(I.bitSerialized,0) = 1 AND ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=0 THEN ''Serialized Asset''
																		WHEN ISNULL(I.bitSerialized,0) = 1 AND ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=1 THEN ''Serialized Rental Asset''
																		WHEN ISNULL(I.bitLotNo,0)=1 THEN ''Lot #''
																		ELSE ''Inventory Item'' 
																	END
																WHEN I.charItemType=''N'' THEN ''Non Inventory Item'' 
																WHEN I.charItemType=''S'' THEN ''Service'' 
																WHEN I.charItemType=''A'' THEN ''Accessory'' 
															END)' + ' AS ' + ' [' + @vcColumnName + ']'  
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'monStockValue'
				BEGIN
					SET @strColumns = @strColumns+',WarehouseItems.monStockValue'+' ['+ @vcColumnName+']'                                                      
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'StockQtyCount'
				BEGIN
					SET @strColumns = @strColumns+',0'+' ['+ @vcColumnName+']'                                                      
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'StockQtyAdjust'
				BEGIN
					SET @strColumns = @strColumns+',0'+' ['+ @vcColumnName+']'     
					                                             
				END
				ELSE IF @vcLookBackTableName = 'WareHouseItems' AND @vcDbColumnName = 'vcLocation' AND ISNULL(@numFilteredWarehouseID,0)=0
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT STUFF((SELECT CONCAT('', '', WIL.vcLocation) FROM WareHouseItems WI  LEFT JOIN WarehouseLocation AS WIL ON WI.numWLocationId=WIL.numWLocationId WHERE WI.numItemID=I.numItemCode FOR XML PATH('''')), 1, 1, ''''))' + ' AS ' + ' [' + @vcColumnName + '] '
				END
				ELSE IF @vcLookBackTableName = 'WareHouseItems' AND @vcDbColumnName = 'vcLocation' AND ISNULL(@numFilteredWarehouseID,0)>0
				BEGIN
					SET @strColumns = @strColumns + ', WarehouseItems.vcLocation AS ' + ' [' + @vcColumnName + '] '
				END
				ELSE IF @vcLookBackTableName = 'Warehouses' AND @vcDbColumnName = 'vcWareHouse'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT STUFF((SELECT CONCAT('', '', vcWareHouse) FROM WareHouseItems WI JOIN Warehouses W ON WI.numWareHouseID=W.numWareHouseID WHERE WI.numItemID=I.numItemCode FOR XML PATH('''')), 1, 1, ''''))' + ' AS ' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcLookBackTableName = 'WorkOrder' AND @vcDbColumnName = 'WorkOrder'
				BEGIN
					SET @strColumns = @strColumns + ',(CASE WHEN (SELECT SUM(WO.numQtyItemsReq) from WorkOrder WO where WO.numItemCode=I.numItemCode and WO.numWOStatus=0) > 0 THEN 1 ELSE 0 END)' + ' AS ' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'vcPriceLevelDetail' 
				BEGIN
					SELECT @strColumns = @strColumns + CONCAT(', dbo.fn_GetItemPriceLevelDetail( ',@numDomainID,',I.numItemCode,',@numWarehouseID,')') +' ['+ @vcColumnName+']' 
				END
				ELSE
				BEGIN
					If @vcDbColumnName <> 'numItemCode' AND @vcDbColumnName <> 'vcPriceLevelDetail' --WE AE ALREADY ADDING COLUMN IN SELECT CLUASE DIRECTLY
					BEGIN
						SET @strColumns = @strColumns + ',' + (CASE @vcLookBackTableName WHEN 'Item' THEN 'I' WHEN 'CompanyInfo' THEN 'cmp' WHEN 'Vendor' THEN 'V' ELSE @vcLookBackTableName END) + '.' + @vcDbColumnName + ' AS ' + ' [' + @vcColumnName + ']' 
					END
				END
			END                              
			ELSE IF @bitCustom = 1 
			BEGIN
				
				SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

				SELECT 
					@vcFieldName = FLd_label,
					@vcAssociatedControlType = fld_type,
					@vcDbColumnName = 'Cust' + CONVERT(VARCHAR(10), Fld_Id),
					@Grp_ID = Grp_id
				FROM 
					CFW_Fld_Master
				WHERE 
					CFW_Fld_Master.Fld_Id = @numFieldId                 
        
				IF @Grp_ID = 9
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'     
             
					SET @WhereCondition = @WhereCondition + ' left Join ItemAttributes CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.numDOmainID=' + CAST(@numDomainID AS VARCHAR) + ' AND CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
									+ CONVERT(VARCHAR(10), @numFieldId)
									+ 'and CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ '.numItemCode=I.numItemCode   '  
					SET @WhereCondition = @WhereCondition
						+ ' left Join ListDetails L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.numListItemID=CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value'      
				END
				ELSE IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea' 
				BEGIN
					SET @strColumns = @strColumns + ',CFW'  + CONVERT(VARCHAR(3), @tintOrder)  + '.Fld_Value  [' + @vcColumnName + ']'     
             
					SET @WhereCondition = @WhereCondition + ' left Join CFW_FLD_Values_Item CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
									+ CONVERT(VARCHAR(10), @numFieldId)
									+ 'and CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ '.RecId=I.numItemCode   '                                                         
				END   
				ELSE IF @vcAssociatedControlType = 'CheckBox' 
				BEGIN
					SET @strColumns = @strColumns
						+ ',case when isnull(CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value,0)=0 then 0 when isnull(CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value,0)=1 then 1 end   ['
						+  @vcColumnName
						+ ']'               
 
					SET @WhereCondition = @WhereCondition
						+ ' left Join CFW_FLD_Values_Item CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
						+ CONVERT(VARCHAR(10), @numFieldId)
						+ ' and CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.RecId=I.numItemCode   '                                                     
				END                
				ELSE IF @vcAssociatedControlType = 'DateField' 
				BEGIN
					SET @strColumns = @strColumns
						+ ',dbo.FormatedDateFromDate(CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value,'
						+ CONVERT(VARCHAR(10), @numDomainId)
						+ ')  [' +@vcColumnName + ']'    
					                  
					SET @WhereCondition = @WhereCondition
						+ ' left Join CFW_FLD_Values_Item CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
						+ CONVERT(VARCHAR(10), @numFieldId)
						+ ' and CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.RecId=I.numItemCode   '                                                         
				END                
				ELSE IF @vcAssociatedControlType = 'SelectBox' 
				BEGIN
					SET @vcDbColumnName = 'Cust' + CONVERT(VARCHAR(10), @numFieldId)

					SET @strColumns = @strColumns + ',L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.vcData' + ' [' + @vcColumnName + ']'          
					                                                    
					SET @WhereCondition = @WhereCondition
						+ ' left Join CFW_FLD_Values_Item CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
						+ CONVERT(VARCHAR(10), @numFieldId)
						+ ' and CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.RecId=I.numItemCode    '     
					                                                    
					SET @WhereCondition = @WhereCondition
						+ ' left Join ListDetails L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.numListItemID=CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value'                
				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueItem(',@numFieldId,',I.numItemCode)') + ' [' + @vcColumnName + ']'
				END                
			END        
  
			

			SELECT TOP 1
					@tintOrder = tintOrder + 1,
					@vcDbColumnName = vcDbColumnName,
					@vcFieldName = vcFieldName,
					@vcAssociatedControlType = vcAssociatedControlType,
					@vcListItemType = vcListItemType,
					@numListID = numListID,
					@vcLookBackTableName = vcLookBackTableName,
					@bitCustom = bitCustomField,
					@numFieldId = numFieldId,
					@bitAllowSorting = bitAllowSorting,
					@bitAllowEdit = bitAllowEdit,
					@ListRelID = ListRelID
			FROM    #tempForm
			WHERE   tintOrder > @tintOrder - 1 --AND bitCustomField=1
			ORDER BY tintOrder ASC            
 
			IF @@rowcount = 0 
				SET @tintOrder = 0 
		END  
		
		--DECLARE @vcApplyRackFilter as VARCHAR(MAX)=''
		--DECLARE @vcApplyRackFilterSearch as VARCHAR(MAX)=''
		--IF(ISNULL(@numFilteredWarehouseID,0)>0)
		--BEGIN
		--	SET @vcApplyRackFilter=' LEFT JOIN WareHouseItems AS WRF ON WRF.numItemId=I.numItemCode LEFT JOIN WarehouseLocation AS WFL ON WRF.numWLocationId=WFL.numWLocationId AND WRF.numWareHouseId=WFL.numWareHouseId '
		--	SET @vcApplyRackFilterSearch=@vcApplyRackFilterSearch + ' AND WRF.numWareHouseId='''+CAST(@numFilteredWarehouseID AS VARCHAR(500))+''' '
		--	SET @vcApplyRackFilterSearch=@vcApplyRackFilterSearch + ' AND WFL.numWareHouseId='''+CAST(@numFilteredWarehouseID AS VARCHAR(500))+''' '
		--	IF(ISNULL(@vcAsile,'')<>'')
		--	BEGIN
		--		SET @vcApplyRackFilterSearch=@vcApplyRackFilterSearch + ' AND WFL.vcAisle='''+CAST(@vcAsile AS VARCHAR(500))+''' '
		--	END
		--	IF(ISNULL(@vcRack,'')<>'')
		--	BEGIN
		--		SET @vcApplyRackFilterSearch=@vcApplyRackFilterSearch + ' AND WFL.vcRack='''+CAST(@vcRack AS VARCHAR(500))+''' '
		--	END
		--	IF(ISNULL(@vcShelf,'')<>'')
		--	BEGIN
		--		SET @vcApplyRackFilterSearch=@vcApplyRackFilterSearch + ' AND WFL.vcShelf='''+CAST(@vcShelf AS VARCHAR(500))+''' '
		--	END
		--	IF(ISNULL(@vcBin,'')<>'')
		--	BEGIN
		--		SET @vcApplyRackFilterSearch=@vcApplyRackFilterSearch + ' AND WFL.vcBin='''+CAST(@vcBin AS VARCHAR(500))+''' '
		--	END
		--END
		
		DECLARE @strSQL AS VARCHAR(MAX)
		SET @strSQL = CONCAT(' FROM 
							Item I
						LEFT JOIN
							Vendor V
						ON
							I.numItemCode = V.numItemCode
							AND I.numVendorID = V.numVendorID
						LEFT JOIN
							DivisionMaster
						ON
							V.numVendorID = DivisionMaster.numDivisionID
						LEFT JOIN
							CompanyInfo cmp
						ON
							DivisionMaster.numCompanyId = cmp.numCompanyId
						LEFT JOIN
							CustomerPartNumber
						ON
							CustomerPartNumber.numItemCode = I.numItemCode AND CustomerPartNumber.numCompanyId = 0
						',(CASE 
								WHEN ISNULL(@numFilteredWarehouseID,0) > 0 
								THEN CONCAT(' CROSS APPLY (
											SELECT
												WareHouseItems.numWareHouseItemID,
												numOnHand AS numOnHand,
												ISNULL(numOnHand,0) + ISNULL(numAllocation,0) AS numAvailable,
												numBackOrder AS numBackOrder,
												numOnOrder AS numOnOrder,
												numAllocation AS numAllocation,
												numReorder AS numReorder,
												numOnHand * (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) AS monStockValue,
												WarehouseLocation.vcLocation
											FROM
												WareHouseItems
											LEFT JOIN 
												WarehouseLocation 
											ON 
												WareHouseItems.numWLocationId=WarehouseLocation.numWLocationId
											WHERE
												numItemID = I.numItemCode
												AND (WareHouseItems.numWareHouseID=',ISNULL(@numFilteredWarehouseID,0),' OR ',ISNULL(@numFilteredWarehouseID,0),'=0)'
												,CASE WHEN ISNULL(@vcAsile,'') <> '' THEN CONCAT(' AND WarehouseLocation.vcAisle=''',@vcAsile+'''') ELSE '' END  
												,CASE WHEN ISNULL(@vcRack,'') <> '' THEN CONCAT(' AND WarehouseLocation.vcRack=''',@vcRack +'''') ELSE '' END  
												,CASE WHEN ISNULL(@vcShelf,'') <> '' THEN CONCAT(' AND WarehouseLocation.vcShelf=''',@vcShelf+'''') ELSE '' END  
												,CASE WHEN ISNULL(@vcBin,'') <> '' THEN CONCAT(' AND WarehouseLocation.vcBin=''',@vcBin+'''') ELSE '' END  
										,') WarehouseItems ')
								ELSE ' OUTER APPLY (
							SELECT
								SUM(numOnHand) AS numOnHand,
								SUM(ISNULL(numOnHand,0) + ISNULL(numAllocation,0)) AS numAvailable,
								SUM(numBackOrder) AS numBackOrder,
								SUM(numOnOrder) AS numOnOrder,
								SUM(numAllocation) AS numAllocation,
								SUM(numReorder) AS numReorder,
								SUM(numOnHand) * (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) AS monStockValue
							FROM
								WareHouseItems
							WHERE
								numItemID = I.numItemCode) WarehouseItems ' 
							END),'
						  ', @WhereCondition)

		IF @columnName = 'Backorder' OR @columnName = 'WHI.numBackOrder'
			SET @columnName = 'numBackOrder'
		ELSE IF @columnName = 'OnOrder' OR @columnName = 'WHI.numOnOrder' 
			SET @columnName = 'numOnOrder'
		ELSE IF @columnName = 'OnAllocation' OR @columnName = 'WHI.numAllocation' 
			SET @columnName = 'numAllocation'
		ELSE IF @columnName = 'Reorder' OR @columnName = 'WHI.numReorder' 
			SET @columnName = 'numReorder'
		ELSE IF @columnName = 'WHI.numOnHand'
			SET @columnName = 'numOnHand'
		ELSE IF @columnName = 'WHI.numAvailable'
			SET @columnName = 'numAvailable'
		ELSE IF @columnName = 'monStockValue' 
			SET @columnName = 'sum(numOnHand) * (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END)'
		ELSE IF @columnName = 'ItemType' 
			SET @columnName = 'charItemType'
		ELSE IF @columnName = 'numItemCode' 
			SET @columnName = 'I.numItemCode'

		DECLARE @column AS VARCHAR(50)              
		SET @column = @columnName
	         
		IF @columnName LIKE '%Cust%' 
		BEGIN
			DECLARE @CustomFieldType AS VARCHAR(10)
			DECLARE @fldId AS VARCHAR(10)
			
			IF CHARINDEX('CFW.Cust',@columnName) > 0
			BEGIN
				SET @fldId = REPLACE(@columnName, 'CFW.Cust', '')
			END
			ELSE
			BEGIN
				SET @fldId = REPLACE(@columnName, 'Cust', '')
			END
			SELECT TOP 1 @CustomFieldType = ISNULL(vcAssociatedControlType,'') FROM View_DynamicCustomColumns WHERE numFieldID = @fldId
            
			IF ISNULL(@CustomFieldType,'') = 'SelectBox' OR ISNULL(@CustomFieldType,'') = 'ListBox'
			BEGIN
				SET @strSQL = @strSQL + ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=i.numItemCode and CFW.fld_id= ' + @fldId + ' ' 
				SET @strSQL = @strSQL + ' left Join ListDetails LstCF on LstCF.numListItemID = CFW.Fld_Value  '            
				SET @columnName = ' LstCF.vcData ' 	
			END
			ELSE
			BEGIN
				SET @strSQL =  @strSQL + ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=i.numItemCode and CFW.fld_id= ' + @fldId + ' '                                                         
				SET @columnName = 'CFW.Fld_Value'                	
			END
		END                                      
		DECLARE @strWhere AS VARCHAR(8000)
		SET @strWhere = CONCAT(' WHERE I.numDomainID = ',@numDomainID)
    
		IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND I.charItemType= ''P''' 
		END
		ELSE IF @ItemType = 'S'
		BEGIN
			SET @strWhere = @strWhere + ' AND I.charItemType= ''S'''
		END
		ELSE IF @ItemType = 'N'    
		BEGIN
			SET @strWhere = @strWhere + ' AND I.charItemType= ''N'''
		END
	    
		IF @bitContainer= '1'
		BEGIN
			SET @strWhere = @strWhere + ' AND I.bitContainer= ' + CONVERT(VARCHAR(2), @bitContainer) + ''  
		END

		IF @bitAssembly = '1' 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.bitAssembly	= ' + CONVERT(VARCHAR(2), @bitAssembly) + ''  
		END 
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAssembly,0)=0 '
		END

		IF @IsKit = '1' 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAssembly,0)=0 AND I.bitKitParent= ' + CONVERT(VARCHAR(2), @IsKit) + ''  
		END           
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitKitParent,0)=0 '
		END

		IF @bitSerialized = 1 
		BEGIN
			SET @strWhere = @strWhere + ' AND (I.bitSerialized=1 OR I.bitLotNo=1)'
		END
		--ELSE IF @ItemType = 'P'
		--BEGIN
		--	SET @strWhere = @strWhere + ' AND ISNULL(Item.bitSerialized,0)=0 AND ISNULL(Item.bitLotNo,0)=0 '
		--END

		IF @bitAsset = 1 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAsset,0) = 1'
		END
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAsset,0) = 0 '
		END

		IF @bitRental = 1
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitRental,0) = 1 AND ISNULL(I.bitAsset,0) = 1 '
		END
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitRental,0) = 0 '
		END

		IF @SortChar <> '0' 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.vcItemName like ''' + @SortChar + '%'''                                       
		END

		IF @ItemClassification <> '0' 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.numItemClassification=' + CONVERT(VARCHAR(15), @ItemClassification)    
		END

		IF @IsArchive = 0 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.IsArchieve,0) = 0'       
		END
        
		IF @numItemGroup > 0 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.numItemGroup = ' + CONVERT(VARCHAR(20), @numItemGroup)  
        END  
		
		IF @numItemGroup = -1 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.numItemGroup in (select numItemGroupID from ItemGroups where numDomainID=' + CONVERT(VARCHAR(20), @numDomainID) + ')'                                      
        END

		IF @bitMultiSelect = '1'
		BEGIN
			SET  @strWhere = @strWhere + 'AND ISNULL(I.bitKitParent,0)=0 '
		END

		IF @vcRegularSearchCriteria<>'' 
		BEGIN
			SET @strWhere = @strWhere +' AND ' + @vcRegularSearchCriteria 
		END
	
		IF @vcCustomSearchCriteria<>'' 
		BEGIN 
		--comment
			SET @strWhere = @strWhere + ' AND ' + @vcCustomSearchCriteria  		END
		
		IF LEN(@SearchQuery) > 0
		BEGIN
			SET @strWhere = @strWhere + ' AND (' + @SearchQuery + ') '
		END
		
		SET @strWhere = @strWhere + ISNULL(@Where,'')

		IF ISNULL(@tintDashboardReminderType,0) = 17
		BEGIN
			SET @strWhere = CONCAT(@strWhere,' AND I.numItemCode IN (','SELECT DISTINCT
																		numItemID
																	FROM
																		WareHouseItems WIInner
																	INNER JOIN
																		Item IInner
																	ON
																		WIInner.numItemID = IInner.numItemCode
																	WHERE
																		WIInner.numDomainID=',@numDomainID,'
																		AND IInner.numDomainID = ',@numDomainID,'
																		AND ISNULL(IInner.bitArchiveItem,0) = 0
																		AND ISNULL(IInner.IsArchieve,0) = 0
																		AND ISNULL(WIInner.numReorder,0) > 0 
																		AND ISNULL(WIInner.numOnHand,0) < ISNULL(WIInner.numReorder,0)',') ')

		END

		IF @tintUserRightType=1 AND NOT EXISTS (SELECT numUserDetailId FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailId=@numUserCntID)
		BEGIN
			SET @strWhere = CONCAT(@strWhere,' AND I.numItemCode IN (SELECT 
																		V.numItemCode
																	FROM 
																		ExtranetAccountsDtl EAD 
																	INNER JOIN 
																		ExtarnetAccounts EA
																	ON 
																		EAD.numExtranetID=EA.numExtranetID 
																	INNER JOIN 
																		Vendor V 
																	ON 
																		EA.numDivisionID=V.numVendorID 
																	WHERE 
																		EAD.numDomainID=',@numDomainID,' 
																		AND V.numDomainID=',@numDomainID,' 
																		AND EAD.numContactID=',@numUserCntID,')')
		END
  
		DECLARE @firstRec AS INTEGER                                        
		DECLARE @lastRec AS INTEGER                                        
		SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                        
		SET @lastRec = ( @CurrentPage * @PageSize + 1 )

		-- EXECUTE FINAL QUERY
		DECLARE @strFinal AS NVARCHAR(MAX)=''
		IF ISNULL(@numFilteredWarehouseID,0)>0
		BEGIN
			SET @strFinal = CONCAT(@strFinal,'SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,',WarehouseItems.vcLocation asc) RowID, ',@strColumns,' INTO #temp2',@strSql,@strWhere,' ;')
			SET @strFinal = @strFinal + ' SELECT @TotalRecords = COUNT(*) FROM #temp2; '
			SET @strFinal = @strFinal + ' SELECT * FROM #temp2 WHERE RowID > '+ CAST(@firstRec AS VARCHAR(100)) +' and RowID <'+CAST(@lastRec AS VARCHAR(100))+';DROP TABLE #temp2; '
		END
		ELSE IF @CurrentPage = -1 AND @PageSize = -1
		BEGIN
			SET @strFinal = CONCAT('SELECT I.numItemCode, I.vcItemName, I.vcModelID INTO #tempTable ',@strSql  , @strWhere ,'; SELECT * FROM #tempTable; SELECT @TotalRecords = COUNT(*) FROM #tempTable; DROP TABLE #tempTable;')
		END
		ELSE
		BEGIN
			SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') RowID,I.numItemCode INTO #temp2 ',@strSql  , @strWhere,'; SELECT RowID, ',@strColumns,' INTO #tempTable ',@strSql, ' JOIN #temp2 tblAllItems ON I.numItemCode = tblAllItems.numItemCode ',@strWhere,' AND tblAllItems.RowID > ',@firstRec,' and tblAllItems.RowID <',@lastRec,'; SELECT  * FROM  #tempTable ORDER BY RowID; DROP TABLE #tempTable; SELECT @TotalRecords = COUNT(*) FROM #temp2; DROP TABLE #temp2;')
		END

		
		PRINT @strFInal
		exec sp_executesql @strFinal, N'@TotalRecords INT OUT', @TotRecs OUT

	END

	UPDATE  
		#tempForm
    SET 
		intColumnWidth = (CASE WHEN ISNULL(intColumnWidth,0) <= 0 THEN 25 ELSE intColumnWidth END)
                                  
	SELECT * FROM #tempForm
    DROP TABLE #tempForm
END
GO
---Created By Anoop Jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageFieldRelationship')
DROP PROCEDURE USP_ManageFieldRelationship
GO
CREATE PROCEDURE USP_ManageFieldRelationship
@byteMode as tinyint,
@numFieldRelID as numeric(9),
@numModuleID NUMERIC(18,0),
@numPrimaryListID as numeric(9),
@numSecondaryListID as numeric(9),
@numDomainID as numeric(9),
@numFieldRelDTLID as numeric(9),
@numPrimaryListItemID as numeric(9),
@numSecondaryListItemID as numeric(9),
@bitTaskRelation AS BIT=0
as
BEGIN
if @byteMode =1
BEGIN
	IF(@bitTaskRelation=1)
	BEGIN
		IF NOT EXISTS(SELECT * FROM dbo.FieldRelationship WHERE numDomainID=@numDomainID AND numPrimaryListID=@numPrimaryListID AND bitTaskRelation=1)
		BEGIN
			insert into FieldRelationship (numPrimaryListID,numSecondaryListID,numDomainID,bitTaskRelation,numModuleID)
			values (@numPrimaryListID,@numSecondaryListID,@numDomainID,@bitTaskRelation,@numModuleID)
			set @numFieldRelID=@@identity 
		END
		ELSE 
			set @numFieldRelID=0
	END
	ELSE
	BEGIN
	IF NOT EXISTS(SELECT * FROM dbo.FieldRelationship WHERE numDomainID=@numDomainID AND numPrimaryListID=@numPrimaryListID AND numSecondaryListID=@numSecondaryListID AND (numModuleID IS NULL OR numModuleID=@numModuleID))
	BEGIN
		insert into FieldRelationship (numPrimaryListID,numSecondaryListID,numDomainID,numModuleID,bitTaskRelation)
		values (@numPrimaryListID,@numSecondaryListID,@numDomainID,@numModuleID,@bitTaskRelation)
		set @numFieldRelID=@@identity 
	END
	ELSE
	BEGIN
		SET @numFieldRelID=0
	END
END
END
else if @byteMode =2
begin

	 if not exists(select * from FieldRelationshipDTL where numFieldRelID=@numFieldRelID 
	and numPrimaryListItemID=@numPrimaryListItemID and numSecondaryListItemID=@numSecondaryListItemID)
		insert into FieldRelationshipDTL (numFieldRelID,numPrimaryListItemID,numSecondaryListItemID)
		values (@numFieldRelID,@numPrimaryListItemID,@numSecondaryListItemID)
	else
	set @numFieldRelID=0 -- to identify that there is already one

END
	else if @byteMode =3
	begin
		Delete from FieldRelationshipDTL where numFieldRelDTLID=@numFieldRelDTLID
	end
	else if @byteMode =4
	begin
		Delete from FieldRelationshipDTL where numFieldRelID=@numFieldRelID
		Delete from FieldRelationship where numFieldRelID=@numFieldRelID
	end

	select @numFieldRelID
END
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageItemsAndKits]    Script Date: 02/15/2010 21:44:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageitemsandkits')
DROP PROCEDURE usp_manageitemsandkits
GO
CREATE PROCEDURE [dbo].[USP_ManageItemsAndKits]                                                                
@numItemCode as numeric(9) output,                                                                
@vcItemName as varchar(300),                                                                
@txtItemDesc as varchar(2000),                                                                
@charItemType as char(1),                                                                
@monListPrice as DECIMAL(20,5),                                                                
@numItemClassification as numeric(9),                                                                
@bitTaxable as bit,                                                                
@vcSKU as varchar(50),                                                                                                                                      
@bitKitParent as bit,                                                                                   
--@dtDateEntered as datetime,                                                                
@numDomainID as numeric(9),                                                                
@numUserCntID as numeric(9),                                                                                                  
@numVendorID as numeric(9),                                                            
@bitSerialized as bit,                                               
@strFieldList as text,                                            
@vcModelID as varchar(200)='' ,                                            
@numItemGroup as numeric(9)=0,                                      
@numCOGSChartAcntId as numeric(9)=0,                                      
@numAssetChartAcntId as numeric(9)=0,                                      
@numIncomeChartAcntId as numeric(9)=0,                            
@monAverageCost as DECIMAL(20,5),                          
@monLabourCost as DECIMAL(20,5),                  
@fltWeight as float,                  
@fltHeight as float,                  
@fltLength as float,                  
@fltWidth as float,                  
@bitFreeshipping as bit,                
@bitAllowBackOrder as bit,              
@UnitofMeasure as varchar(30)='',              
@strChildItems as text,            
@bitShowDeptItem as bit,              
@bitShowDeptItemDesc as bit,        
@bitCalAmtBasedonDepItems as bit,    
@bitAssembly as BIT,
@intWebApiId INT=null,
@vcApiItemId AS VARCHAR(50)=null,
@numBarCodeId as VARCHAR(25)=null,
@vcManufacturer as varchar(250)=NULL,
@numBaseUnit AS NUMERIC(18)=0,
@numPurchaseUnit AS NUMERIC(18)=0,
@numSaleUnit AS NUMERIC(18)=0,
@bitLotNo as BIT,
@IsArchieve AS BIT,
@numItemClass as numeric(9)=0,
@tintStandardProductIDType AS TINYINT=0,
@vcExportToAPI VARCHAR(50),
@numShipClass NUMERIC,
@ProcedureCallFlag SMALLINT =0,
@vcCategories AS VARCHAR(2000) = '',
@bitAllowDropShip AS BIT=0,
@bitArchiveItem AS BIT = 0,
@bitAsset AS BIT = 0,
@bitRental AS BIT = 0,
@numCategoryProfileID AS NUMERIC(18,0) = 0,
@bitVirtualInventory BIT,
@bitContainer BIT,
@numContainer NUMERIC(18,0)=0,
@numNoItemIntoContainer NUMERIC(18,0),
@bitMatrix BIT = 0 ,
@vcItemAttributes VARCHAR(2000) = '',
@numManufacturer NUMERIC(18,0),
@vcASIN as varchar(50),
@tintKitAssemblyPriceBasedOn TINYINT = 1,
@fltReorderQty FLOAT = 0,
@bitSOWorkOrder BIT = 0,
@bitKitSingleSelect BIT = 0,
@bitExpenseItem BIT = 0,
@numExpenseChartAcntId NUMERIC(18,0) = 0,
@numBusinessProcessId NUMERIC(18,0)=0,
@bitTimeContractFromSalesOrder BIT =0 
AS
BEGIN     
	SET @vcManufacturer = CASE WHEN ISNULL(@numManufacturer,0) > 0 THEN ISNULL((SELECT vcCompanyName FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=@numManufacturer),'')  ELSE ISNULL(@vcManufacturer,'') END

	DECLARE @bitAttributesChanged AS BIT  = 1
	DECLARE @bitOppOrderExists AS BIT = 0

	IF ISNULL(@numItemCode,0) > 0
	BEGIN
		IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
	END
	ELSE 
	BEGIN
		IF (ISNULL(@vcItemName,'') = '')
		BEGIN
			RAISERROR('ITEM_NAME_NOT_SELECTED',16,1)
			RETURN
		END
        ELSE IF (ISNULL(@charItemType,'') = '0')
		BEGIN
			RAISERROR('ITEM_TYPE_NOT_SELECTED',16,1)
			RETURN
		END
	END

	If (@charItemType = 'P' AND ISNULL(@bitKitParent,0) = 0)
	BEGIN
		IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numAssetChartAcntId) = 0)
		BEGIN
			RAISERROR('INVALID_ASSET_ACCOUNT',16,1)
			RETURN
		END
	END

	If @charItemType = 'P' OR ISNULL((SELECT bitExpenseNonInventoryItem FROM Domain WHERE numDomainID=@numDomainID),0) = 1
	BEGIN
		-- This is common check for CharItemType P and Other
		IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numCOGSChartAcntId) = 0)
		BEGIN
			RAISERROR('INVALID_COGS_ACCOUNT',16,1)
			RETURN
		END
	END

	IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numIncomeChartAcntId) = 0)
	BEGIN
		RAISERROR('INVALID_INCOME_ACCOUNT',16,1)
		RETURN
	END

	IF ISNULL(@bitExpenseItem,0) = 1
	BEGIN
		IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numExpenseChartAcntId) = 0)
		BEGIN
			RAISERROR('INVALID_EXPENSE_ACCOUNT',16,1)
			RETURN
		END
	END

	--INSERT CUSTOM FIELDS BASE ON WAREHOUSEITEMS
	DECLARE @hDoc AS INT     
	                                                                        	
	DECLARE @TempTable TABLE 
	(
		ID INT IDENTITY(1,1),
		Fld_ID NUMERIC(18,0),
		Fld_Value NUMERIC(18,0)
	)   

	IF ISNULL(@bitMatrix,0) = 1 AND ISNULL(@numItemGroup,0) = 0
	BEGIN
		RAISERROR('ITEM_GROUP_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) = 0 
	BEGIN
		RAISERROR('ITEM_ATTRIBUTES_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 
	BEGIN
		DECLARE @bitDuplicate AS BIT = 0
	
		IF DATALENGTH(ISNULL(@vcItemAttributes,''))>2
		BEGIN
			SET @vcItemAttributes = CONCAT('<?xml version="1.0" encoding="iso-8859-1"?>',@vcItemAttributes)

			EXEC sp_xml_preparedocument @hDoc OUTPUT, @vcItemAttributes
	                                      
			INSERT INTO @TempTable 
			(
				Fld_ID,
				Fld_Value
			)    
			SELECT
				*          
			FROM 
				OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
			WITH 
				(Fld_ID NUMERIC(18,0),Fld_Value NUMERIC(18,0)) 
			ORDER BY 
				Fld_ID       

			DECLARE @strAttribute VARCHAR(500) = ''
			DECLARE @i AS INT = 1
			DECLARE @COUNT AS INT 
			DECLARE @numFldID AS NUMERIC(18,0)
			DECLARE @numFldValue AS NUMERIC(18,0)
			SELECT @COUNT = COUNT(*) FROM @TempTable

			WHILE @i <= @COUNT
			BEGIN
				SELECT @numFldID=Fld_ID,@numFldValue=Fld_Value FROM @TempTable WHERE ID=@i

				IF ISNUMERIC(@numFldValue)=1
				BEGIN
					SELECT @strAttribute=@strAttribute+Fld_label FROM CFW_Fld_Master WHERE Fld_id=@numFldID AND Grp_id = 9
                
					IF LEN(@strAttribute) > 0
					BEGIN
						SELECT @strAttribute=@strAttribute+':'+ vcData +',' FROM ListDetails WHERE numListItemID=@numFldValue
					END
				END

				SET @i = @i + 1
			END

			SET @strAttribute = SUBSTRING(@strAttribute,0,LEN(@strAttribute))

			IF
				(SELECT
					COUNT(*)
				FROM
					Item
				INNER JOIN
					ItemAttributes
				ON
					Item.numItemCode = ItemAttributes.numItemCode
					AND ItemAttributes.numDomainID = @numDomainID                     
				WHERE
					Item.numItemCode <> @numItemCode
					AND Item.vcItemName = @vcItemName
					AND Item.numItemGroup = @numItemGroup
					AND Item.numDomainID = @numDomainID
					AND dbo.fn_GetItemAttributes(@numDomainID,Item.numItemCode,0) = @strAttribute
				) > 0
			BEGIN
				RAISERROR('ITEM_WITH_ATTRIBUTES_ALREADY_EXISTS',16,1)
				RETURN
			END

			If ISNULL(@numItemCode,0) > 0 AND dbo.fn_GetItemAttributes(@numDomainID,@numItemCode,0) = @strAttribute
			BEGIN
				SET @bitAttributesChanged = 0
			END

			EXEC sp_xml_removedocument @hDoc 
		END	
	END
	ELSE IF ISNULL(@bitMatrix,0) = 0
	BEGIN
		-- REMOVE ATTRIBUTES CONFIGURATION OF ITEM AND WAREHOUSE
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode
	END

	IF ISNULL(@numItemGroup,0) > 0 AND ISNULL(@bitMatrix,0) = 1
	BEGIN
		IF (SELECT COUNT(*) FROM ItemGroupsDTL WHERE numItemGroupID = @numItemGroup AND tintType=2) = 0
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
		ELSE IF EXISTS (SELECT 
							CFM.Fld_id
						FROM 
							CFW_Fld_Master CFM
						LEFT JOIN
							ListDetails LD
						ON
							CFM.numlistid = LD.numListID
						WHERE
							CFM.numDomainID = @numDomainID
							AND CFM.Fld_id IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID=@numItemGroup AND tintType = 2)
						GROUP BY
							CFM.Fld_id HAVING COUNT(LD.numListItemID) = 0)
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
	END

BEGIN TRY
BEGIN TRANSACTION

	

	IF @numCOGSChartAcntId=0 SET @numCOGSChartAcntId=NULL
	IF @numAssetChartAcntId=0 SET @numAssetChartAcntId=NULL  
	IF @numIncomeChartAcntId=0 SET @numIncomeChartAcntId=NULL     
	IF @numExpenseChartAcntId=0 SET @numExpenseChartAcntId=NULL

	DECLARE @ParentSKU VARCHAR(50) = @vcSKU                        
	DECLARE @ItemID AS NUMERIC(9)
	DECLARE @cnt AS INT

	IF ISNULL(@charItemType,'') = '' OR ISNULL(@charItemType,'') = '0'  
		SET @charItemType = 'N'

	IF @intWebApiId = 2 AND LEN(ISNULL(@vcApiItemId, '')) > 0 AND ISNULL(@numItemGroup, 0) > 0 
    BEGIN 
        -- check wether this id already Mapped in ITemAPI 
        SELECT 
			@cnt = COUNT([numItemID])
        FROM 
			[ItemAPI]
        WHERE 
			[WebApiId] = @intWebApiId
            AND [numDomainId] = @numDomainID
            AND [vcAPIItemID] = @vcApiItemId

        IF @cnt > 0 
        BEGIN
            SELECT 
				@ItemID = [numItemID]
            FROM 
				[ItemAPI]
            WHERE 
				[WebApiId] = @intWebApiId
                AND [numDomainId] = @numDomainID
                AND [vcAPIItemID] = @vcApiItemId
        
			--Update Existing Product coming from API
            SET @numItemCode = @ItemID
        END
    END
	ELSE IF @intWebApiId > 1 AND LEN(ISNULL(@vcSKU, '')) > 0 
    BEGIN
        SET @ParentSKU = @vcSKU
		 
        SELECT 
			@cnt = COUNT([numItemCode])
        FROM 
			[Item]
        WHERE 
			[numDomainId] = @numDomainID
            AND (vcSKU = @vcSKU OR @vcSKU IN (SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numItemID = Item.[numItemCode] AND vcWHSKU = @vcSKU))
            
		IF @cnt > 0 
        BEGIN
            SELECT 
				@ItemID = [numItemCode],
                @ParentSKU = vcSKU
            FROM 
				[Item]
            WHERE 
				[numDomainId] = @numDomainID
                AND (vcSKU = @vcSKU OR @vcSKU IN (SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numItemID = Item.[numItemCode] AND vcWHSKU = @vcSKU))

            SET @numItemCode = @ItemID
        END
		ELSE 
		BEGIN
			-- check wether this id already Mapped in ITemAPI 
			SELECT 
				@cnt = COUNT([numItemID])
			FROM 
				[ItemAPI]
			WHERE 
				[WebApiId] = @intWebApiId
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId
                    
			IF @cnt > 0 
			BEGIN
				SELECT 
					@ItemID = [numItemID]
				FROM 
					[ItemAPI]
				WHERE 
					[WebApiId] = @intWebApiId
					AND [numDomainId] = @numDomainID
					AND [vcAPIItemID] = @vcApiItemId
        
				--Update Existing Product coming from API
				SET @numItemCode = @ItemID
			END
		END
    END
                                                         
	IF @numItemCode=0 OR @numItemCode IS NULL
	BEGIN
		INSERT INTO Item 
		(                                             
			vcItemName,txtItemDesc,charItemType,monListPrice,numItemClassification,bitTaxable,vcSKU,bitKitParent,
			numVendorID,numDomainID,numCreatedBy,bintCreatedDate,bintModifiedDate,numModifiedBy,bitSerialized,
			vcModelID,numItemGroup,numCOGsChartAcntId,numAssetChartAcntId,numIncomeChartAcntId,monAverageCost,                          
			monCampaignLabourCost,fltWeight,fltHeight,fltWidth,fltLength,bitFreeShipping,bitAllowBackOrder,vcUnitofMeasure,
			bitShowDeptItem,bitShowDeptItemDesc,bitCalAmtBasedonDepItems,bitAssembly,numBarCodeId,vcManufacturer,numBaseUnit,
			numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,tintStandardProductIDType,vcExportToAPI,numShipClass,
			bitAllowDropShip,bitArchiveItem,bitAsset,bitRental,bitVirtualInventory,bitContainer,numContainer,numNoItemIntoContainer
			,bitMatrix,numManufacturer,vcASIN,tintKitAssemblyPriceBasedOn,fltReorderQty,bitSOWorkOrder,bitKitSingleSelect,bitExpenseItem,numExpenseChartAcntId,numBusinessProcessId,bitTimeContractFromSalesOrder 
		)                                                              
		VALUES                                                                
		(                                                                
			@vcItemName,@txtItemDesc,@charItemType,@monListPrice,@numItemClassification,@bitTaxable,@ParentSKU,@bitKitParent,
			@numVendorID,@numDomainID,@numUserCntID,getutcdate(),getutcdate(),@numUserCntID,@bitSerialized,@vcModelID,@numItemGroup,                                      
			@numCOGsChartAcntId,@numAssetChartAcntId,@numIncomeChartAcntId,@monAverageCost,@monLabourCost,@fltWeight,@fltHeight,@fltWidth,                  
			@fltLength,@bitFreeshipping,@bitAllowBackOrder,@UnitofMeasure,@bitShowDeptItem,@bitShowDeptItemDesc,@bitCalAmtBasedonDepItems,    
			@bitAssembly,@numBarCodeId,@vcManufacturer,@numBaseUnit,@numPurchaseUnit,@numSaleUnit,@bitLotNo,@IsArchieve,@numItemClass,
			@tintStandardProductIDType,@vcExportToAPI,@numShipClass,@bitAllowDropShip,@bitArchiveItem ,@bitAsset,@bitRental,
			@bitVirtualInventory,@bitContainer,@numContainer,@numNoItemIntoContainer
			,@bitMatrix,@numManufacturer,@vcASIN,@tintKitAssemblyPriceBasedOn,@fltReorderQty,@bitSOWorkOrder,@bitKitSingleSelect,@bitExpenseItem,@numExpenseChartAcntId,@numBusinessProcessId,@bitTimeContractFromSalesOrder 
		)                                             
 
		SET @numItemCode = SCOPE_IDENTITY()
  
		IF  @intWebApiId > 1 
		BEGIN
			 -- insert new product
			 --insert this id into linking table
			 INSERT INTO [ItemAPI] (
	 			[WebApiId],
	 			[numDomainId],
	 			[numItemID],
	 			[vcAPIItemID],
	 			[numCreatedby],
	 			[dtCreated],
	 			[numModifiedby],
	 			[dtModified],vcSKU
			 ) VALUES     
			 (
				@intWebApiId,
				@numDomainID,
				@numItemCode,
				@vcApiItemId,
	 			@numUserCntID,
	 			GETUTCDATE(),
	 			@numUserCntID,
	 			GETUTCDATE(),@vcSKU
	 		) 
		END
 
		IF @charItemType = 'P'
		BEGIN
			
			DECLARE @TEMPWarehouse TABLE
			(
				ID INT IDENTITY(1,1)
				,numWarehouseID NUMERIC(18,0)
			)
		
			INSERT INTO @TEMPWarehouse (numWarehouseID) SELECT numWareHouseID FROM Warehouses WHERE numDomainID=@numDomainID

			DECLARE @j AS INT = 1
			DECLARE @jCount AS INT
			DECLARE @numTempWarehouseID NUMERIC(18,0)
			SELECT @jCount = COUNT(*) FROM @TEMPWarehouse

			WHILE @j <= @jCount
			BEGIN
				SELECT @numTempWarehouseID=numWarehouseID FROM @TEMPWarehouse WHERE ID=@j

				EXEC USP_WarehouseItems_Save @numDomainID,
											@numUserCntID,
											0,
											@numTempWarehouseID,
											0,
											@numItemCode,
											'',
											@monListPrice,
											0,
											0,
											'',
											'',
											'',
											'',
											0
 
				SET @j = @j + 1
			END
		END 
	END         
	ELSE IF @numItemCode>0 AND @intWebApiId > 0 
	BEGIN 
		IF @ProcedureCallFlag = 1 
		BEGIN
			DECLARE @ExportToAPIList AS VARCHAR(30)
			SELECT @ExportToAPIList = vcExportToAPI FROM dbo.Item WHERE numItemCode = @numItemCode

			IF @ExportToAPIList != ''
			BEGIN
				IF NOT EXISTS(select * from (SELECT * FROM dbo.Split(@ExportToAPIList ,','))as A where items= @vcExportToAPI ) 
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList + ',' + @vcExportToAPI
				End
				ELSE
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList 
				End
			END

			--update ExportToAPI String value
			UPDATE dbo.Item SET vcExportToAPI=@vcExportToAPI where numItemCode=@numItemCode  AND numDomainID=@numDomainID
					
			SELECT 
				@cnt = COUNT([numItemID]) 
			FROM 
				[ItemAPI] 
			WHERE  
				[WebApiId] = @intWebApiId 
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId
				AND numItemID = @numItemCode

			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE
			--Insert ItemAPI mapping
			BEGIN
				INSERT INTO [ItemAPI] 
				(
					[WebApiId],
					[numDomainId],
					[numItemID],
					[vcAPIItemID],
					[numCreatedby],
					[dtCreated],
					[numModifiedby],
					[dtModified],vcSKU
				) VALUES 
				(
					@intWebApiId,
					@numDomainID,
					@numItemCode,
					@vcApiItemId,
					@numUserCntID,
					GETUTCDATE(),
					@numUserCntID,
					GETUTCDATE(),@vcSKU
				)	
			END
		END   
	END                                            
	ELSE IF @numItemCode > 0 AND @intWebApiId <= 0                                                           
	BEGIN
		DECLARE @OldGroupID NUMERIC 
		SELECT @OldGroupID = ISNULL(numItemGroup,0) FROM [Item] WHERE numitemcode = @numItemCode AND [numDomainId] = @numDomainID
	
		DECLARE @monOldAverageCost AS DECIMAL(18,4);SET @monOldAverageCost=0
		SELECT @monOldAverageCost=(CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE numItemCode =@numItemCode AND [numDomainId] = @numDomainID
	   
		UPDATE 
			Item 
		SET 
			vcItemName=@vcItemName,txtItemDesc=@txtItemDesc,charItemType=@charItemType,monListPrice= @monListPrice,numItemClassification=@numItemClassification,                                             
			bitTaxable=@bitTaxable,vcSKU = (CASE WHEN ISNULL(@ParentSKU,'') = '' THEN @vcSKU ELSE @ParentSKU END),bitKitParent=@bitKitParent,                                             
			numVendorID=@numVendorID,numDomainID=@numDomainID,bintModifiedDate=getutcdate(),numModifiedBy=@numUserCntID,bitSerialized=@bitSerialized,                                                         
			vcModelID=@vcModelID,numItemGroup=@numItemGroup,numCOGsChartAcntId=@numCOGsChartAcntId,numAssetChartAcntId=@numAssetChartAcntId,                                      
			numIncomeChartAcntId=@numIncomeChartAcntId,monCampaignLabourCost=@monLabourCost,fltWeight=@fltWeight,fltHeight=@fltHeight,fltWidth=@fltWidth,                  
			fltLength=@fltLength,bitFreeShipping=@bitFreeshipping,bitAllowBackOrder=@bitAllowBackOrder,vcUnitofMeasure=@UnitofMeasure,bitShowDeptItem=@bitShowDeptItem,            
			bitShowDeptItemDesc=@bitShowDeptItemDesc,bitCalAmtBasedonDepItems=@bitCalAmtBasedonDepItems,bitAssembly=@bitAssembly,numBarCodeId=@numBarCodeId,
			vcManufacturer=@vcManufacturer,numBaseUnit=@numBaseUnit,numPurchaseUnit=@numPurchaseUnit,numSaleUnit=@numSaleUnit,bitLotNo=@bitLotNo,
			IsArchieve = @IsArchieve,numItemClass=@numItemClass,tintStandardProductIDType=@tintStandardProductIDType,vcExportToAPI=@vcExportToAPI,
			numShipClass=@numShipClass,bitAllowDropShip=@bitAllowDropShip, bitArchiveItem = @bitArchiveItem,bitAsset=@bitAsset,bitRental=@bitRental,
			bitVirtualInventory = @bitVirtualInventory,numContainer=@numContainer,numNoItemIntoContainer=@numNoItemIntoContainer,bitMatrix=@bitMatrix,numManufacturer=@numManufacturer,
			vcASIN = @vcASIN,tintKitAssemblyPriceBasedOn=@tintKitAssemblyPriceBasedOn,fltReorderQty=@fltReorderQty,bitSOWorkOrder=@bitSOWorkOrder,bitKitSingleSelect=@bitKitSingleSelect
			,bitExpenseItem=@bitExpenseItem,numExpenseChartAcntId=@numExpenseChartAcntId,numBusinessProcessId=@numBusinessProcessId,bitTimeContractFromSalesOrder =@bitTimeContractFromSalesOrder 
		WHERE 
			numItemCode=@numItemCode 
			AND numDomainID=@numDomainID

		IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
		BEGIN
			IF (((SELECT ISNULL(SUM(numOnHand),0) FROM WareHouseItems WHERE numItemID = @numItemCode AND numDomainId=@numDomainID) = 0) AND ((SELECT ISNULL(bitVirtualInventory,0) FROM Item  WHERE numItemCode = @numItemCode AND numDomainId=@numDomainID) = 0))
			BEGIN
				UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
			END
		END
	
		--If Average Cost changed then add in Tracking
		IF ISNULL(@monAverageCost,0) != ISNULL(@monOldAverageCost,0)
		BEGIN
			DECLARE @numWareHouseItemID NUMERIC(9);SET @numWareHouseItemID=0
			DECLARE @numTotal int;SET @numTotal=0
			SET @i=0
			DECLARE @vcDescription AS VARCHAR(100);SET @vcDescription=''
		
			SELECT @numTotal=COUNT(*) FROM WareHouseItems where numItemID=@numItemCode AND numDomainId=@numDomainId
	     
			WHILE @i < @numTotal
			BEGIN
				SELECT @numWareHouseItemID = min(numWareHouseItemID) FROM WareHouseItems 
					WHERE numItemID=@numItemCode AND numDomainId=@numDomainId and numWareHouseItemID > @numWareHouseItemID 
				
				IF @numWareHouseItemID>0
				BEGIN
					SET @vcDescription='Average Cost Changed Manually OLD : ' + CAST(ISNULL(@monOldAverageCost,0) AS VARCHAR(20))
				
					DECLARE @numDomain AS NUMERIC(18,0)
					SET @numDomain = @numDomainID
					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)		
						@numReferenceID = @numItemCode, --  numeric(9, 0)
						@tintRefType = 1, --  tinyint
						@vcDescription = @vcDescription, --  varchar(100)
						@tintMode = 0, --  tinyint
						@numModifiedBy = @numUserCntID, --  numeric(9, 0)
						@numDomainID = @numDomain
				END
		    
				SET @i = @i + 1
			END
		END
 
		DECLARE @bitCartFreeShipping as  bit 
		SELECT @bitCartFreeShipping = bitFreeShipping FROM dbo.CartItems where numItemCode = @numItemCode  AND numDomainID = @numDomainID 
 
		IF @bitCartFreeShipping <> @bitFreeshipping
		BEGIN
			UPDATE dbo.CartItems SET bitFreeShipping = @bitFreeshipping WHERE numItemCode = @numItemCode  AND numDomainID=@numDomainID
		END 
 
		IF @intWebApiId > 1 
		BEGIN
			SELECT 
				@cnt = COUNT([numItemID])
			FROM 
				[ItemAPI]
			WHERE 
				[WebApiId] = @intWebApiId
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId

			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE --Insert ItemAPI mapping
			BEGIN
				INSERT INTO [ItemAPI] 
				(
	 				[WebApiId],
	 				[numDomainId],
	 				[numItemID],
	 				[vcAPIItemID],
	 				[numCreatedby],
	 				[dtCreated],
	 				[numModifiedby],
	 				[dtModified],vcSKU
				 )
				 VALUES 
				 (
					@intWebApiId,
					@numDomainID,
					@numItemCode,
					@vcApiItemId,
	 				@numUserCntID,
	 				GETUTCDATE(),
	 				@numUserCntID,
	 				GETUTCDATE(),@vcSKU
	 			) 
			END
		END   
		                                                                            
		-- kishan It is necessary to add item into itemTax table . 
		IF	@bitTaxable = 1
		BEGIN
			IF NOT EXISTS(SELECT * FROM dbo.ItemTax WHERE numItemCode = @numItemCode AND numTaxItemID = 0)
			BEGIN
			    INSERT INTO dbo.ItemTax 
				(
					numItemCode,
					numTaxItemID,
					bitApplicable
				)
				VALUES 
				( 
					@numItemCode,
					0,
					1 
				) 						
			END
		END	 
      
		IF @charItemType='S' or @charItemType='N'                                                                       
		BEGIN
			DELETE FROM WareHouseItmsDTL where numWareHouseItemID in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID)
			DELETE FROM WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID AND numWareHouseItemID NOT IN (SELECT OI.numWarehouseItmsID FROM OpportunityItems OI WHERE OI.numItemCode=@numItemCode)
		END                                      
   
		IF @bitKitParent=1 OR @bitAssembly = 1              
		BEGIN              
			IF DATALENGTH(ISNULL(@strChildItems,'')) > 0
			BEGIN
				SET @strChildItems = CONCAT('<?xml version="1.0" encoding="iso-8859-1"?>',@strChildItems)
			END

			DECLARE @hDoc1 AS INT                                            
			EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strChildItems                                                                        
                                             
			UPDATE 
				ItemDetails 
			SET
				numQtyItemsReq=X.QtyItemsReq
				,numUOMId=X.numUOMId
				,vcItemDesc=X.vcItemDesc
				,sintOrder=X.sintOrder
				,tintView = X.tintView
				,vcFields=X.vcFields                                                                                     
			From 
			(
				SELECT 
					QtyItemsReq
					,ItemKitID
					,ChildItemID
					,numUOMId
					,vcItemDesc
					,sintOrder
					,numItemDetailID As ItemDetailID
					,tintView
					,vcFields                          
				FROM 
					OPENXML(@hDoc1,'/NewDataSet/Table1',2)                                                                         
				WITH
				(
					ItemKitID NUMERIC(9),                                                          
					ChildItemID NUMERIC(9),                                                                        
					QtyItemsReq DECIMAL(30, 16),
					numUOMId NUMERIC(9),
					vcItemDesc VARCHAR(1000),
					sintOrder int,
					numItemDetailID NUMERIC(18,0),
					tintView TINYINT,
					vcFields VARCHAR(500)
				)
			)X                                                                         
			WHERE 
				numItemKitID=X.ItemKitID 
				AND numChildItemID=ChildItemID 
				AND numItemDetailID = X.ItemDetailID

			EXEC sp_xml_removedocument @hDoc1
		 END   
		 ELSE
		 BEGIN
 			DELETE FROM ItemDetails WHERE numItemKitID=@numItemCode
		 END                                                
	END

	IF ISNULL(@numItemGroup,0) = 0 OR ISNULL(@bitMatrix,0) = 1
	BEGIN
		UPDATE WareHouseItems SET monWListPrice = @monListPrice, vcWHSKU=@vcSKU, vcBarCode=@numBarCodeId WHERE numDomainID = @numDomainID AND numItemID = @numItemCode
	END

	-- SAVE MATRIX ITEM ATTRIBUTES
	IF @numItemCode > 0 AND ISNULL(@bitAttributesChanged,0)=1 AND ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) > 0 AND ISNULL(@bitOppOrderExists,0) <> 1 AND (SELECT COUNT(*) FROM @TempTable) > 0
	BEGIN
		-- DELETE PREVIOUS ITEM ATTRIBUTES
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

		-- INSERT NEW ITEM ATTRIBUTES
		INSERT INTO ItemAttributes
		(
			numDomainID
			,numItemCode
			,FLD_ID
			,FLD_Value
		)
		SELECT
			@numDomainID
			,@numItemCode
			,Fld_ID
			,Fld_Value
		FROM
			@TempTable

		IF (SELECT COUNT(*) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode) > 0
		BEGIN

			-- DELETE PREVIOUS WAREHOUSE ATTRIBUTES
			DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId IN (SELECT ISNULL(numWarehouseItemID,0) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode)

			-- INSERT NEW WAREHOUSE ATTRIBUTES
			INSERT INTO CFW_Fld_Values_Serialized_Items
			(
				Fld_ID,
				Fld_Value,
				RecId,
				bitSerialized
			)                                                  
			SELECT 
				Fld_ID,
				ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
				TEMPWarehouse.numWarehouseItemID,
				0 
			FROM 
				@TempTable  
			OUTER APPLY
			(
				SELECT
					numWarehouseItemID
				FROM 
					WarehouseItems 
				WHERE 
					numDomainID=@numDomainID 
					AND numItemID=@numItemCode
			) AS TEMPWarehouse
		END
	END	  
COMMIT

	IF @numItemCode > 0
	BEGIN
		DELETE 
			IC
		FROM 
			dbo.ItemCategory IC
		INNER JOIN 
			Category 
		ON 
			IC.numCategoryID = Category.numCategoryID
		WHERE 
			numItemID = @numItemCode		
			AND numCategoryProfileID = @numCategoryProfileID

		IF @vcCategories <> ''	
		BEGIN
			INSERT INTO ItemCategory( numItemID , numCategoryID )  SELECT  @numItemCode AS numItemID,ID from dbo.SplitIDs(@vcCategories,',')	
		END
	END
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH        
END
/****** Object:  StoredProcedure [dbo].[USP_OPPGetINItemsForAuthorizativeAccounting]    Script Date: 07/26/2008 16:20:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by Siva                                                                                                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppgetinitemsforauthorizativeaccounting')
DROP PROCEDURE usp_oppgetinitemsforauthorizativeaccounting
GO
CREATE PROCEDURE  [dbo].[USP_OPPGetINItemsForAuthorizativeAccounting]                                                                                                                                                
(                                                                                                                                                
	@numOppId AS NUMERIC(18,0)=null,                                                                                                                                                
	@numOppBizDocsId AS NUMERIC(18,0)=null,                                                                
	@numDomainID AS NUMERIC(18,0)=0 ,                                                      
	@numUserCntID AS NUMERIC(18,0)=0                                                                                                                                               
)                                                                                                                                                                                                                                                                                         
AS 
BEGIN
	DECLARE @numCurrencyID AS NUMERIC(18,0)
	DECLARE @fltExchangeRate AS FLOAT

	DECLARE @fltCurrentExchangeRate AS FLOAT
	DECLARE @tintType AS TINYINT
	DECLARE @vcPOppName AS VARCHAR(100)
	DECLARE @bitPPVariance AS BIT
	DECLARE @numDivisionID AS NUMERIC(18,0)

	SELECT 
		@numCurrencyID=numCurrencyID
		,@fltExchangeRate=fltExchangeRate
		,@tintType=tintOppType
		,@vcPOppName=vcPOppName
		,@bitPPVariance=ISNULL(bitPPVariance,0)
		,@numDivisionID=numDivisionID 
	FROM 
		OpportunityMaster
	WHERE 
		numOppID=@numOppId 

	DECLARE @vcBaseCurrency AS VARCHAR(100)=''
	DECLARE @bitAutolinkUnappliedPayment AS BIT 

	SELECT 
		@vcBaseCurrency=ISNULL(C.varCurrSymbol,'')
		,@bitAutolinkUnappliedPayment=ISNULL(bitAutolinkUnappliedPayment,0) 
	FROM 
		dbo.Domain D 
	LEFT JOIN 
		Currency C 
	ON 
		D.numCurrencyID=C.numCurrencyID 
	WHERE 
		D.numDomainID=@numDomainID

	DECLARE @vcForeignCurrency AS VARCHAR(100) = ''
	SELECT 
		@vcForeignCurrency=ISNULL(C.varCurrSymbol,'') 
	FROM  
		Currency C 
	WHERE 
		numCurrencyID=@numCurrencyID

	DECLARE @fltExchangeRateBizDoc AS FLOAT 
	DECLARE @numBizDocId AS NUMERIC(18,0)                                                           
	DECLARE @vcBizDocID AS VARCHAR(100)

	SELECT 
		@numBizDocId=numBizDocId
		,@fltExchangeRateBizDoc=fltExchangeRateBizDoc
		,@vcBizDocID=vcBizDocID 
	FROM 
		OpportunityBizDocs 
	WHERE 
		numOppBizDocsId=@numOppBizDocsId                                                         

	IF @numCurrencyID > 0 
		SET @fltCurrentExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
	ELSE 
		SET @fltCurrentExchangeRate=@fltExchangeRate                                                                                                                                          

	IF @numBizDocId = 296 OR @numBizDocId = 287 --FULFILLMENT BIZDOC OR INVOICE
	BEGIN
		--STORE AVERAGE COST FOR SHIPPED ITEM SO THAT WE CAN CALCULATE INVENTORY IMAPCT IN ACCOUNTING AND ALSO USE IT WHEN IETM RETURN
		UPDATE
			OBDI
		SET
			OBDI.monAverageCost= (CASE 
										WHEN ISNULL(I.bitKitParent,0) = 1 
										THEN ISNULL((SELECT SUM(ISNULL(OKI.numQtyItemsReq_Orig,0) * ISNULL(IInner.monAverageCost,0)) FROM OpportunityKitItems OKI INNER JOIN Item IInner ON OKI.numChildItemID=IInner.numItemCode WHERE OKI.numOppId=OI.numOppId AND OKI.numOppItemID=OI.numoppitemtCode),0) + 
												ISNULL((SELECT SUM(ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(OKI.numQtyItemsReq_Orig,0) * ISNULL(IInner.monAverageCost,0)) FROM OpportunityKitChildItems OKCI INNER JOIN OpportunityKitItems OKI ON OKCI.numOppChildItemID=OKI.numOppChildItemID INNER JOIN Item IInner ON OKCI.numItemID=IInner.numItemCode WHERE OKI.numOppId=OI.numOppId AND OKI.numOppItemID=OI.numoppitemtCode),0)
										ELSE ISNULL(I.monAverageCost,0) 
									END)
		FROM
			OpportunityBizDocItems OBDI
		INNER JOIN
			OpportunityItems OI
		ON
			OBDI.numOppItemID=OI.numoppitemtCode
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE
			OBDI.numOppBizDocID=@numOppBizDocsId
			AND OBDI.monAverageCost IS NULL
	END
                                                                                                                     
	SELECT 
		I.[vcItemName] as Item
		,charitemType as type
		,OBI.vcitemdesc as [desc]
		,OBI.numUnitHour as Unit
		,ISNULL(OBI.monPrice,0) as Price
		,ISNULL(OBI.monTotAmount,0) as Amount
		,ISNULL((opp.monTotAmount/opp.numUnitHour)*OBI.numUnitHour,0) AS  ItemTotalAmount
		,isnull(monListPrice,0) as listPrice
		,convert(varchar,i.numItemCode) as ItemCode
		,numoppitemtCode
		,L.vcdata as vcItemClassification
		,case when bitTaxable=0 then 'No' else 'Yes' end as Taxable
		,isnull(i.numIncomeChartAcntId,0) as itemIncomeAccount
		,isnull(i.numAssetChartAcntId,0) as itemInventoryAsset,'NI' as ItemType        
		,isnull(i.numCOGsChartAcntId,0) as itemCoGs
		,isnull(i.bitExpenseItem,0) as bitExpenseItem
		,isnull(i.numExpenseChartAcntId,0) as itemExpenseAccount
		,(CASE WHEN ISNULL(i.bitVirtualInventory,0) = 1 THEN 0 ELSE isnull(Opp.monAvgCost,0) END) as AverageCost
		,isnull(OBI.bitDropShip,0) as bitDropShip
		,(OBI.monTotAmtBefDiscount-OBI.monTotAmount) as DiscAmt
		,NULLIF(Opp.numProjectID,0) numProjectID
		,NULLIF(Opp.numClassID,0) numClassID
		,ISNULL(i.bitKitParent,0) AS bitKitParent
		,ISNULL(i.bitAssembly,0) AS bitAssembly
		,(CASE WHEN ISNULL(i.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(OBI.monAverageCost,0) END) AS ShippedAverageCost
	FROM 
		OpportunityItems opp
	INNER JOIN 
		OpportunityBizDocItems OBI
	ON 
		OBI.numOppItemID=Opp.numoppitemtCode       
	LEFT JOIN 
		Item I 
	ON 
		opp.numItemCode=I.numItemCode        
	LEFT JOIN 
		ListDetails L 
	ON 
		I.numItemClassification=L.numListItemID        
	WHERE 
		Opp.numOppId=@numOppId 
		AND OBI.numOppBizDocID=@numOppBizDocsId
                                                               
	SELECT 
		ISNULL(@numCurrencyID,0) as numCurrencyID
		,ISNULL(@fltExchangeRate,1) as fltExchangeRate
		,ISNULL(@fltCurrentExchangeRate,1) as CurrfltExchangeRate
		,ISNULL(@fltExchangeRateBizDoc,1) AS fltExchangeRateBizDoc
		,@vcBaseCurrency AS vcBaseCurrency
		,@vcForeignCurrency AS vcForeignCurrency
		,ISNULL(@vcPOppName,'') AS vcPOppName
		,ISNULL(@vcBizDocID,'') AS vcBizDocID
		,ISNULL(@bitPPVariance,0) AS bitPPVariance
		,@numDivisionID AS numDivisionID
		,ISNULL(@bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment                                            
END
GO
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount DECIMAL(20,5) =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(18,0),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0,
  @numShipmentMethod NUMERIC(18,0)=0,
  @dtReleaseDate DATE = NULL,
  @numPartner NUMERIC(18,0)=0,
  @tintClickBtn INT=0,
  @numPartenerContactId NUMERIC(18,0)=0,
  @numAccountClass NUMERIC(18,0) = 0,
  @numWillCallWarehouseID NUMERIC(18,0) = 0,
  @numVendorAddressID NUMERIC(18,0) = 0,
  @numShipFromWarehouse NUMERIC(18,0) = 0,
  @numShippingService NUMERIC(18,0) = 0,
  @ClientTimeZoneOffset INT = 0,
  @vcCustomerPO VARCHAR(100)=NULL,
  @bitDropShipAddress BIT = 0,
  @PromCouponCode AS VARCHAR(100) = NULL,
  @numPromotionId AS NUMERIC(18,0) = 0,
  @dtExpectedDate AS DATETIME = null,
  @numProjectID NUMERIC(18,0) = 0
  -- USE PARAMETER WITH OPTIONAL VALUE BECAUSE PROCEDURE IS CALLED FROM OTHER PROCEDURE WHICH WILL NOT PASS NEW PARAMETER VALUE
)                                                                      
AS
SET NOCOUNT ON
SET XACT_ABORT ON

BEGIN TRY
	IF ISNULL(@numOppID,0) = 0 AND ISNULL(@numDomainID,0) = 209 AND ISNULL(@vcOppRefOrderNo,'') <> '' AND ISNULL(@tintSourceType,0) = 4
	BEGIN
		IF EXISTS (SELECT numOppID FROM OpportunityMaster WHERE numDomainID=209 AND tintOppType=1 AND vcOppRefOrderNo=@vcOppRefOrderNo)
		BEGIN
			RAISERROR('CUSTOMERPO_ALREADY_EXISTS',16,1)
		END
	END
	
	IF ISNULL(@numContactId,0) = 0
	BEGIN
		RAISERROR('CONTACT_REQUIRED',16,1)
	END  
	
	IF ISNULL(@tintOppType,0) = 0
	BEGIN
		RAISERROR('OPP_TYPE_CAN_NOT_BE_0',16,1)
	END             

	IF ISNULL(@numOppID,0) > 0 AND EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numDomainId=@numDomainId AND numOppId=@numOppID AND ISNULL(tintshipped,0) = 1) 
	BEGIN	 
		RAISERROR('OPP/ORDER_IS_CLOSED',16,1)
	END

BEGIN TRANSACTION
	DECLARE @fltExchangeRate float                                 
	DECLARE @hDocItem int                                                                                                                            
	DECLARE @TotalAmount as DECIMAL(20,5)                                                                          
	DECLARE @tintOppStatus as tinyint
	DECLARE @bitNewOrder AS BIT = (CASE WHEN ISNULL(@numOppID,0) = 0 THEN 1 ELSE 0 END)

	

	DECLARE @dtItemRelease AS DATE
	SET @dtItemRelease = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())

	DECLARE @numCompany AS NUMERIC(18)
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)
	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainId

	SELECT 
		@numCompany = D.numCompanyID
		,@bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0)
		,@numRelationship=numCompanyType
		,@numProfile=vcProfile
	FROM 
		DivisionMaster D
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID
	WHERE 
		D.numDivisionID = @numDivisionId

	IF @numOppID = 0 AND CONVERT(VARCHAR(10),@strItems) <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

		--VALIDATE PROMOTION OF COUPON CODE
		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1),
			numOppItemID NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numOppItemID,
			numPromotionID
		)
		SELECT
			numoppitemtCode,
			numPromotionID
		FROM
			OPENXML (@hDocItem,'/NewDataSet/Item',2)
		WITH
		(
			numoppitemtCode NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)
		WHERE
			numPromotionID > 0

		EXEC sp_xml_removedocument @hDocItem 

		IF (SELECT COUNT(*) FROM @TEMP) > 0
		BEGIN
			-- IF ITEM PROMOTION USED IN ORDER EXIPIRES THAN RAISE ERROR
			IF  (SELECT 
						COUNT(PO.numProId)
					FROM 
						PromotionOffer PO
					LEFT JOIN
						PromotionOffer POOrder
					ON
						PO.numOrderPromotionID = POOrder.numProId
					INNER JOIN
						@TEMP T1
					ON
						PO.numProId = T1.numPromotionID
					WHERE 
						PO.numDomainId=@numDomainID 
						AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
						AND ISNULL(PO.bitEnabled,0) = 1
						AND 1 = (CASE 
									WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
									THEN (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
									ELSE (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
								END)
						AND 1 = (CASE 
									WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
									ELSE
										(CASE PO.tintCustomersBasedOn 
											WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
											WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
											WHEN 3 THEN 1
											ELSE 0
										END)
								END)
				) <> (SELECT COUNT(*) FROM @TEMP)
			BEGIN
				RAISERROR('ITEM_PROMOTION_EXPIRED',16,1)
				RETURN
			END

			-- NOW IF COUPON BASED ITEM PROMOTION IS USED THEN VALIDATE COUPON AND ITS USAGE
			IF 
			(
				SELECT 
					COUNT(*)
				FROM
					PromotionOffer PO
				INNER JOIN
					@TEMP T1
				ON
					PO.numProId = T1.numPromotionID
				WHERE
					PO.numDomainId = @numDomainId
					AND ISNULL(IsOrderBasedPromotion,0) = 0
					AND ISNULL(numOrderPromotionID,0) > 0 
			) = 1
			BEGIN
				IF ISNULL(@PromCouponCode,0) <> ''
				BEGIN
					IF (SELECT 
							COUNT(*)
						FROM
							PromotionOffer PO
						INNER JOIN 
							PromotionOffer POOrder
						ON
							PO.numOrderPromotionID = POOrder.numProId
						INNER JOIN
							@TEMP T1
						ON
							PO.numProId = T1.numPromotionID
						INNER JOIN
							DiscountCodes DC
						ON
							POOrder.numProId = DC.numPromotionID
						WHERE
							PO.numDomainId = @numDomainId
							AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
							AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode) = 0
					BEGIN
						RAISERROR('INVALID_COUPON_CODE_ITEM_PROMOTION',16,1)
						RETURN
					END
					ELSE
					BEGIN
						-- IF THERE IS COUPON CODE USAGE LIMIT THAN VALIDATE USAGE LIMIT
						IF (SELECT 
								COUNT(*)
							FROM
								PromotionOffer PO
							INNER JOIN 
								PromotionOffer POOrder
							ON
								PO.numOrderPromotionID = POOrder.numProId
							INNER JOIN
								@TEMP T1
							ON
								PO.numProId = T1.numPromotionID
							INNER JOIN
								DiscountCodes DC
							ON
								POOrder.numProId = DC.numPromotionID
							WHERE
								PO.numDomainId = @numDomainId
								AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
								AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode
								AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN 1 ELSE 0 END)
								AND ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE DiscountCodeUsage.numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit) > 0
						BEGIN
							RAISERROR('ITEM_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED',16,1)
							RETURN
						END
					END
				END
				ELSE
				BEGIN
					RAISERROR('COUPON_CODE_REQUIRED_ITEM_PROMOTION',16,1)
					RETURN
				END
			END
		END
	END

	--IF NEW OPPERTUNITY
	IF @numOppID = 0                                                                          
	BEGIN
		DECLARE @bitIsInitalSalesOrder BIT
		IF(@DealStatus=1 AND @tintOppType=1)
		BEGIN
			SET @bitIsInitalSalesOrder=1
		END

		DECLARE @intOppTcode AS NUMERIC(9)  
  
		SELECT @intOppTcode = MAX(numOppId) FROM OpportunityMaster                

		SET @intOppTcode = ISNULL(@intOppTcode,0) + 1                                                
		
		SET @vcPOppName = @vcPOppName + '-'+ CONVERT(varchar(4),YEAR(GETUTCDATE()))  +'-A'+ CONVERT(varchar(10),@intOppTcode)                                  
  
		IF ISNULL(@numCurrencyID,0) = 0 
		BEGIN
			SET @fltExchangeRate=1
			SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
		END
		ELSE
		BEGIN 
			SET @fltExchangeRate = dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
        END  
		                                                        
		IF ISNULL(@numAccountClass,0) = 0
		BEGIN                                                  
		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivisionID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
		END

        IF(@DealStatus=1 AND @tintOppType=1 AND @tintClickBtn=1)
		BEGIN
			SET @DealStatus=0
		END         
		--PRINT ('Deal Staus ' +@DealStatus)  

		-------- VALIDATE ORDER BASED PROMOTION -------------

		DECLARE @numDiscountID NUMERIC(18,0) = NULL
		IF @numPromotionId > 0 
		BEGIN
			-- VALIDTE IF ITS ORDER BASED PROMOTION (BOTH ITEM AND ORDER BASED PROMOTION CAN HAVE COUPON CODE)
			IF EXISTS (SELECT numProId FROM PromotionOffer WHERE numDomainId=@numDomainId AND numProId=@numPromotionId AND ISNULL(IsOrderBasedPromotion,0) = 1)
			BEGIN
				-- CHECK IF ORDER PROMOTION IS STILL VALID
				IF NOT EXISTS (SELECT 
									PO.numProId
								FROM 
									PromotionOffer PO
								INNER JOIN 
									PromotionOfferOrganizations PORG
								ON 
									PO.numProId = PORG.numProId
								WHERE 
									numDomainId=@numDomainID 
									AND PO.numProId=@numPromotionId
									AND ISNULL(IsOrderBasedPromotion,0) = 1
									AND ISNULL(bitEnabled,0) = 1
									AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=dtValidFrom AND GETUTCDATE()<=dtValidTo THEN 1 ELSE 0 END) END)
									AND numRelationship=@numRelationship 
									AND numProfile=@numProfile
				)
				BEGIN
					RAISERROR('ORDER_PROMOTION_EXPIRED',16,1)
					RETURN
				END
				ELSE IF ISNULL((SELECT ISNULL(bitRequireCouponCode,0) FROM PromotionOffer WHERE numDomainId=@numDomainId AND numProId=@numPromotionId),0) = 1
				BEGIN
					IF ISNULL(@PromCouponCode,0) <> ''
					BEGIN
						IF (SELECT 
								COUNT(*)
							FROM
								PromotionOffer PO
							INNER JOIN
								DiscountCodes DC
							ON
								PO.numProId = DC.numPromotionID
							WHERE
								PO.numDomainId = @numDomainId
								AND PO.numProId = @numPromotionId
								AND ISNULL(IsOrderBasedPromotion,0) = 1
								AND ISNULL(bitRequireCouponCode,0) = 1
								AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode) = 0
						BEGIN
							RAISERROR('INVALID_COUPON_CODE_ORDER_PROMOTION',16,1)
							RETURN
						END
						ELSE
						BEGIN
							-- IF THERE IS COUPON CODE USAGE LIMIT THAN VALIDATE USAGE LIMIT
							IF (SELECT 
									COUNT(*)
								FROM
									PromotionOffer PO
								INNER JOIN
									DiscountCodes DC
								ON
									PO.numProId = DC.numPromotionID
								WHERE
									PO.numDomainId = @numDomainId
									AND PO.numProId = @numPromotionId
									AND ISNULL(IsOrderBasedPromotion,0) = 1
									AND ISNULL(bitRequireCouponCode,0) = 1
									AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode
									AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN 1 ELSE 0 END)
									AND ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE DiscountCodeUsage.numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit) > 0
							BEGIN
								RAISERROR('ORDER_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED',16,1)
								RETURN
							END
							ELSE
							BEGIN			
								SELECT
									@numDiscountID=numDiscountId
								FROM
									DiscountCodes DC
								WHERE
									DC.numPromotionID=@numPromotionId
									AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode
									
								IF EXISTS (SELECT DC.numDiscountID FROM DiscountCodes DC INNER JOIN DiscountCodeUsage DCU ON DC.numDiscountID=DCU.numDIscountID WHERE DC.numPromotionID=@numPromotionId AND DCU.numDivisionID=@numDivisionID AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode)
								BEGIN
									UPDATE
										DCU
									SET
										DCU.intCodeUsed = ISNULL(DCU.intCodeUsed,0) + 1
									FROM 
										DiscountCodes DC 
									INNER JOIN 
										DiscountCodeUsage DCU 
									ON 
										DC.numDiscountID=DCU.numDIscountID 
									WHERE 
										DC.numPromotionID=@numPromotionId 
										AND DCU.numDivisionID=@numDivisionID 
										AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode
								END
								ELSE
								BEGIN
									INSERT INTO DiscountCodeUsage
									(
										numDiscountId
										,numDivisionId
										,intCodeUsed
									)
									SELECT 
										DC.numDiscountId
										,@numDivisionId
										,1
									FROM
										DiscountCodes DC
									WHERE
										DC.numPromotionID=@numPromotionId
										AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode
								END
							END
						END
					END
					ELSE
					BEGIN
						RAISERROR('COUPON_CODE_REQUIRED_ORDER_PROMOTION',16,1)
						RETURN
					END
				END
			END
		END

		-------- ORDER BASED Promotion Logic-------------
		                                                    
		INSERT INTO OpportunityMaster
		(                                                                             
			numContactId,numDivisionId,txtComments,numCampainID,bitPublicFlag,tintSource,tintSourceType,vcPOppName,                                                                          
			intPEstimatedCloseDate,monPAmount,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,                                                                                                             
			numRecOwner,lngPConclAnalysis,tintOppType,numSalesOrPurType,numCurrencyID,fltExchangeRate,numAssignedTo,
			numAssignedBy,[tintOppStatus],numStatus,vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,
			bitInterestType,fltInterest,vcCouponCode,bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,
			vcMarketplaceOrderReportId,numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
			bitUseMarkupShippingRate,numMarkupShippingRate,intUsedShippingCompany,numShipmentMethod,numShippingService,numPartner,numPartenerContact,dtReleaseDate,bitIsInitalSalesOrder,numVendorAddressID,numShipFromWarehouse
			,vcCustomerPO#,bitDropShipAddress,numDiscountID,dtExpectedDate,numProjectID
		)                                                                          
		Values                                                                          
		(                                                                          
			@numContactId,@numDivisionId,ISNULL(@Comments,''),@CampaignID,@bitPublicFlag,@tintSource,@tintSourceType,@vcPOppName,                                                                          
			@dtEstimatedCloseDate,@monPAmount,@numUserCntID,(CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END),                                                                          
			@numUserCntID,GETUTCDATE(),@numDomainId,@numUserCntID,@lngPConclAnalysis,@tintOppType,@numSalesOrPurType,@numCurrencyID, 
			@fltExchangeRate,(CASE WHEN @numAssignedTo > 0 THEN @numAssignedTo ELSE NULL END),(CASE WHEN @numAssignedTo>0 THEN @numUserCntID ELSE NULL END),
			@DealStatus,@numStatus,@vcOppRefOrderNo,@bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,
			@PromCouponCode,@bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,@numPercentageComplete,
			@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,@bitUseMarkupShippingRate,@numMarkupShippingRate,@intUsedShippingCompany,@numShipmentMethod,@numShippingService,@numPartner,@numPartenerContactId,@dtReleaseDate,@bitIsInitalSalesOrder,@numVendorAddressID,@numShipFromWarehouse
			,@vcCustomerPO,@bitDropShipAddress,@numDiscountID,@dtExpectedDate,@numProjectID
		)                                                                                                                      
		
		SET @numOppID=(SELECT SCOPE_IDENTITY())                
                            
        EXEC dbo.usp_OppDefaultAssociateContacts @numOppId=@numOppID,@numDomainId=@numDomainId                    
		--UPDATE OPPNAME AS PER NAME TEMPLATE
		EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

		SELECT @vcPOppName=ISNULL(vcPOppName,'') FROM OpportunityMaster WHERE numOppId=@numOppID

		--MAP CUSTOM FIELD	
		DECLARE @tintPageID AS TINYINT;
		SET @tintPageID = CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
		EXEC dbo.USP_AddParentChildCustomFieldMap
				@numDomainID = @numDomainID, --  numeric(18, 0)
				@numRecordID = @numOppID, --  numeric(18, 0)
				@numParentRecId = @numDivisionId, --  numeric(18, 0)
				@tintPageID = @tintPageID --  tinyint
 	
		IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
		BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END
  
		-- INSERTING ITEMS                                                                                                                    
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,
				monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
				numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,vcAttrValues,monAvgCost,bitItemPriceApprovalRequired,
				numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,dtPlannedStart,numWOAssignedTo,numShipToAddressID,ItemReleaseDate
				,bitMarkupDiscount,vcChildKitSelectedItems,numWOQty,bitMappingRequired,vcSKU,vcASIN
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
				(CASE X.numWarehouseItmsID 
				WHEN -1 
				THEN 
					CASE 
						WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU= X.vcSKU) > 0)
							THEN
								(SELECT TOP 1 
									numWareHouseItemID 
								FROM 
									WareHouseItems 
								WHERE 
									[numItemID] = X.numItemCode 
									AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
						ELSE
							(SELECT 
								[numWareHouseItemID] 
							FROM 
								[WareHouseItems] 
							WHERE 
								[numItemID] = X.numItemCode 
								AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				WHEN 0 
				THEN 
					CASE 
					WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU=X.vcSKU) > 0)
						THEN
							(SELECT TOP 1 
								numWareHouseItemID 
							FROM 
								WareHouseItems 
							WHERE 
								[numItemID] = X.numItemCode 
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
					ELSE
						(SELECT 
							[numWareHouseItemID] 
						FROM 
							[WareHouseItems] 
						WHERE 
							[numItemID] = X.numItemCode 
							AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
							AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				ELSE  
					X.numWarehouseItmsID 
				END) AS numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
				(SELECT isnull(VN.monCost,0) FROM Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode AND VN.numVendorID=IT.numVendorID WHERE  VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,
				0,
				X.numToWarehouseItemID,X.Attributes,X.AttributeIDs,(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),
				X.bitItemPriceApprovalRequired,X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(X.vcVendorNotes,'')
				,X.vcInstruction,DATEADD(MINUTE,@ClientTimeZoneOffset,X.bintCompliationDate),DATEADD(MINUTE,@ClientTimeZoneOffset,X.dtPlannedStart),X.numWOAssignedTo,X.numShipToAddressID
				,ISNULL(ItemReleaseDate,@dtItemRelease),X.bitMarkupDiscount,ISNULL(X.KitChildItems,''),X.numWOQty,ISNULL(X.bitMappingRequired,0),X.vcSKU,X.vcASIN
			FROM
				(
					SELECT 
						*
					FROM 
						OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
					WITH                       
					(                                                                          
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9), numUnitHour FLOAT, monPrice DECIMAL(30,16), monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(2000),                                    
						numWarehouseItmsID NUMERIC(9), ItemType VARCHAR(30), DropShip BIT, bitDiscountType BIT,	fltDiscount DECIMAL(30,16), monTotAmtBefDiscount DECIMAL(20,5),
						vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),
						numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),AttributeIDs VARCHAR(500),vcSKU VARCHAR(100)
						,bitItemPriceApprovalRequired BIT,numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0)
						,numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,dtPlannedStart DATETIME,numWOAssignedTo NUMERIC(18,0)
						,numShipToAddressID  NUMERIC(18,0),ItemReleaseDate DATETIME,bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX),numWOQty FLOAT,bitMappingRequired BIT,vcASIN VARCHAR(100)
					)
				)X    
			ORDER BY 
				X.numoppitemtCode
    
			-- UPDATE EXISTING CUSTOMER PART NO
			UPDATE
				CPN
			SET
				CPN.CustomerPartNo = ISNULL(TEMPCPN.CustomerPartNo,'')
			FROM
				CustomerPartNumber CPN
			INNER JOIN
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) AS TEMPCPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.numDomainID=@numDomainID
				AND CPN.numCompanyId = @numCompany
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- INSERT NEW CUSTOMER PART NO
			INSERT INTO CustomerPartNumber
			(
				numCompanyId, numDomainID, CustomerPartNo, numItemCode 
			)
			SELECT 
				@numCompany, @numDomainID, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
			FROM
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) TEMPCPN 
			LEFT JOIN
				CustomerPartNumber CPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.CustomerPartNoID IS NULL
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'

			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,numWarehouseItmsID=X.numWarehouseItmsID,bitDropShip=X.DropShip,
				bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
				bitWorkOrder=X.bitWorkOrder,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,
				numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,vcAttributes=X.Attributes,
				numClassID=0,vcModelID=X.vcModelID,
				bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired,vcAttrValues=X.AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost
				,vcNotes=X.vcVendorNotes,vcInstruction=X.vcInstruction,bintCompliationDate=DATEADD(MINUTE,@ClientTimeZoneOffset,X.bintCompliationDate),dtPlannedStart=DATEADD(MINUTE,@ClientTimeZoneOffset,X.dtPlannedStart),numWOAssignedTo=X.numWOAssignedTo,ItemRequiredDate=X.ItemRequiredDate
				,bitMarkupDiscount=X.bitMarkupDiscount,vcChildKitSelectedItems=ISNULL(X.KitChildItems,'')
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,
					monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,
					Attributes,bitItemPriceApprovalRequired,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) numCost
					,ISNULL(vcVendorNotes,'') vcVendorNotes,vcInstruction,bintCompliationDate,dtPlannedStart,numWOAssignedTo,ItemRequiredDate,bitMarkupDiscount,KitChildItems
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
				WITH  
					(                      
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(2000), vcModelID VARCHAR(300),
						numWarehouseItmsID NUMERIC(9),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),
						numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),
						numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000)
						,vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,dtPlannedStart DATETIME,numWOAssignedTo NUMERIC(18,0),ItemRequiredDate DATETIME,bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX)
					)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppID=@numOppID
				
			--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
			DECLARE @TempKitConfiguration TABLE
			(
				numOppItemID NUMERIC(18,0),
				numItemCode NUMERIC(18,0),
				numKitItemID NUMERIC(18,0),
				numKitChildItemID NUMERIC(18,0),
				numQty FLOAT,
				numSequence INT
			)

			INSERT INTO @TempKitConfiguration
			(
				numOppItemID,
				numItemCode,
				numKitItemID,
				numKitChildItemID,
				numQty,
				numSequence
			)
			SELECT
				numoppitemtCode,
				numItemCode,
				numKitItemID,
				numChildItemID,
				numQty,
				numSequence
			FROM
				OpportunityItems
			CROSS APPLY
			(
				SELECT 
					Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 1)) As numKitItemID
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 2)) As numChildItemID
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 3)) As numQty
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 4)) As numSequence
				FROM  
				(
					SELECT 
						OutParam 
					FROM 
						SplitString(vcChildKitSelectedItems,',')
				) X
			) Y
			WHERE
				numOppId = @numOppID
				AND LEN(ISNULL(vcChildKitSelectedItems,'')) > 0
			ORDER BY
				ISNULL(Y.numSequence,0)

			--INSERT KIT ITEMS 
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				T1.numKitChildItemID,
				ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=T1.numKitChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),0),
				T1.numQty * OI.numUnitHour,
				T1.numQty,
				I.numBaseUnit,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			INNER JOIN
				@TempKitConfiguration T1
			ON
				T1.numOppItemID=OI.numoppitemtCode
				AND T1.numItemCode = OI.numItemCode
			JOIN
				Item I
			ON
				T1.numKitChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND (SELECT COUNT(*) FROM @TempKitConfiguration t2 WHERE t2.numOppItemID=OI.numoppitemtCode AND t2.numItemCode=OI.numItemCode AND ISNULL(t2.numKitItemID,0) = 0) > 0
			ORDER BY
				ISNULL(T1.numSequence,0)

			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),0),
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND (SELECT COUNT(*) FROM @TempKitConfiguration t2 WHERE t2.numOppItemID=OI.numoppitemtCode AND t2.numItemCode=OI.numItemCode AND ISNULL(t2.numKitItemID,0) = 0) = 0
			ORDER BY
				ISNULL(ID.sintOrder,0)

			IF (SELECT 
					COUNT(*) 
				FROM 
					OpportunityKitItems OI 
				INNER JOIN
					Item I
				ON
					OI.numChildItemID = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND ISNULL(charItemType,0) = 'P'
					AND numWareHouseItemId = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END

			IF (SELECT 
						COUNT(*) 
					FROM 
					(
						SELECT 
							t1.numOppItemID
							,t1.numItemCode
							,numKitItemID
							,numKitChildItemID
						FROM 
							@TempKitConfiguration t1
						WHERE
							ISNULL(t1.numKitItemID,0) > 0
					) TempChildItems
					INNER JOIN
						OpportunityKitItems OKI
					ON
						OKI.numOppId=@numOppId
						AND OKI.numOppItemID=TempChildItems.numOppItemID
						AND OKI.numChildItemID=TempChildItems.numKitItemID
					INNER JOIN
						OpportunityItems OI
					ON
						OI.numItemCode = TempChildItems.numItemCode
						AND OI.numoppitemtcode = OKI.numOppItemID
					LEFT JOIN
						WarehouseItems WI
					ON
						OI.numWarehouseItmsID = WI.numWarehouseItemID
					LEFT JOIN
						Warehouses W
					ON
						WI.numWarehouseID = W.numWarehouseID
					INNER JOIN
						ItemDetails
					ON
						TempChildItems.numKitItemID = ItemDetails.numItemKitID
						AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					INNER JOIN
						Item 
					ON
						ItemDetails.numChildItemID = Item.numItemCode
					INNER JOIN
						Item IMain
					ON
						OKI.numChildItemID = IMain.numItemCode
					WHERE
						Item.charItemType = 'P'
						AND ISNULL(IMain.bitKitParent,0) = 1
						AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
				BEGIN
					RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
				END

			INSERT INTO OpportunityKitChildItems
			(
				numOppID,
				numOppItemID,
				numOppChildItemID,
				numItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT 
				@numOppID,
				OKI.numOppItemID,
				OKI.numOppChildItemID,
				ItemDetails.numChildItemID,
				(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
				(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
				ItemDetails.numQtyItemsReq,
				ItemDetails.numUOMId,
				0,
				ISNULL(Item.monAverageCost,0)
			FROM
			(
				SELECT 
					t1.numOppItemID
					,t1.numItemCode
					,numKitItemID
					,numKitChildItemID
				FROM 
					@TempKitConfiguration t1
				WHERE
					ISNULL(t1.numKitItemID,0) > 0
			) TempChildItems
			INNER JOIN
				OpportunityKitItems OKI
			ON
				OKI.numOppId=@numOppId
				AND OKI.numOppItemID=TempChildItems.numOppItemID
				AND OKI.numChildItemID=TempChildItems.numKitItemID
			INNER JOIN
				OpportunityItems OI
			ON
				OI.numItemCode = TempChildItems.numItemCode
				AND OI.numoppitemtcode = OKI.numOppItemID
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			INNER JOIN
				ItemDetails
			ON
				TempChildItems.numKitItemID = ItemDetails.numItemKitID
				AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
			INNER JOIN
				Item 
			ON
				ItemDetails.numChildItemID = Item.numItemCode

			-- UPDATE UOM OF OPPORTUNITY ITEMS HAVE NOT UOM SET WHILE EXPORTED FROM MARKETPLACE (MEANS TINTSOURCETYPE = 3)
			IF ISNULL(@tintSourceType,0) = 3
			BEGIN
				UPDATE 
					OI 
				SET 
					numUOMID = ISNULL(I.numSaleUnit,0), 
					numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
				FROM 
					OpportunityItems OI
				JOIN 
					Item I 
				ON 
					OI.numItemCode = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND I.numDomainID = @numDomainId
			END	
                                     
			INSERT INTO OppWarehouseSerializedItem
			(
				numOppID,
				numWarehouseItmsDTLID,
				numOppItemID,
				numWarehouseItmsID
			)                      
			SELECT 
				@numOppID,
				X.numWarehouseItmsDTLID,
				X.numoppitemtCode,
				X.numWItmsID 
			FROM
			(                                                                          
				SELECT
					* 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
				WITH                        
				(                                               
				numWarehouseItmsDTLID numeric(9),                      
				numoppitemtCode numeric(9),                      
				numWItmsID numeric(9)                                                     
				)
			)X                                    
                                   
			UPDATE 
				OppWarehouseSerializedItem                       
			SET 
				numOppItemID=X.numoppitemtCode                                
			FROM                       
			(
				SELECT 
					numWarehouseItmsDTLID AS DTLID,
					O.numoppitemtCode 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
				WITH
				(
					numWarehouseItmsDTLID NUMERIC(9),numoppitemtCode NUMERIC(9),numWItmsID NUMERIC(9)
				)                                
				JOIN 
					OpportunityItems O 
				ON 
					O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID
			)X                 
			WHERE 
				numOppID=@numOppID 
				AND numWarehouseItmsDTLID=X.DTLID      
    
			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@numUserCntID
			END
                     
			EXEC sp_xml_removedocument @hDocItem                                     
		END          
	END
	ELSE                                                                                                                          
	BEGIN

		DECLARE @tempAssignedTo AS NUMERIC(9) = NULL                                                    
		SELECT 
			@tintOppStatus = tintOppStatus,
			@tempAssignedTo=ISNULL(numAssignedTo,0) 
		FROM   
			OpportunityMaster 
		WHERE  
			numOppID = @numOppID

		IF @tempAssignedTo <> @numAssignedTo
		BEGIN
			IF (SELECT COUNT(*) FROM BizDocComission WHERE ISNULL(numOppID,0) = @numOppID AND ISNULL(bitCommisionPaid,0) = 1) > 0
			BEGIN
				RAISERROR('COMMISSION_PAID_AGAINST_BIZDOC',16,1)
			END
		END
 
		--REVERTING BACK THE WAREHOUSE ITEMS                  
		IF @tintOppStatus = 1 AND @tintCommitAllocation=1
		BEGIN               
			EXEC USP_RevertDetailsOpp @numOppID,0,0,@numUserCntID                  
		END                  

		IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
		BEGIN
			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
		END
		
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
		BEGIN
			EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END 
						
		UPDATE   
			OpportunityMaster
		SET      
			vcPOppName = @vcPOppName,txtComments = ISNULL(@Comments,''),bitPublicFlag = @bitPublicFlag,numCampainID = @CampaignID,tintSource = @tintSource,tintSourceType=@tintSourceType,
			intPEstimatedCloseDate = @dtEstimatedCloseDate,numModifiedBy = @numUserCntID,bintModifiedDate = GETUTCDATE(),lngPConclAnalysis = @lngPConclAnalysis,monPAmount = @monPAmount,
			tintActive = @tintActive,numSalesOrPurType = @numSalesOrPurType,numStatus = @numStatus,vcOppRefOrderNo=@vcOppRefOrderNo,
			bintOppToOrder=(CASE WHEN @tintOppStatus=0 AND @DealStatus=1 THEN GETUTCDATE() ELSE bintOppToOrder END),bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,
			bitBillingTerms=@bitBillingTerms,intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,tintTaxOperator=@tintTaxOperator,
			numDiscountAcntType=@numDiscountAcntType,numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
			intUsedShippingCompany = @intUsedShippingCompany, numContactID=@numContactId,numShipmentMethod=@numShipmentMethod,numShippingService=@numShippingService
			,numPartner=@numPartner,numPartenerContact=@numPartenerContactId,numProjectID=@numProjectID
		WHERE
			numOppId = @numOppID   
	   
		-- UPDATING IF ORGANIZATION IS ASSIGNED TO SOMEONE                                                      
		IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN
			UPDATE
				OpportunityMaster 
			SET	
				numAssignedTo = @numAssignedTo, 
				numAssignedBy = @numUserCntID 
			WHERE 
				numOppId = @numOppID                                                    
		END                                                     
		ELSE IF(@numAssignedTo = 0) 
		BEGIN
			UPDATE 
				OpportunityMaster 
			SET 
				numAssignedTo = 0, 
				numAssignedBy = 0 
			WHERE
				numOppId = @numOppID
		END  
			                                                                    
		-- UPDATING OPP ITEMS
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0                                                                         
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   
			IF EXISTS 
			(
				SELECT 
					OI.numoppitemtCode
				FROM 
					OpportunityItems OI
				WHERE 
					numOppID=@numOppID 
					AND OI.numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))
					AND ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) > 0
			)
			BEGIN
				RAISERROR('SERIAL/LOT#_ASSIGNED_TO_ITEM_DELETED',16,1)
			END

			DELETE FROM OpportunityKitChildItems WHERE numOppID=@numOppID   
			DELETE FROM OpportunityKitItems WHERE numOppID=@numOppID                  
			DELETE FROM OpportunityItems WHERE numOppID=@numOppID AND numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9))) 
	          
			-- DELETE CORRESPONSING TIME AND EXPENSE ENTRIES OF DELETED ITEMS
			DELETE FROM
				TimeAndExpense
			WHERE
				numDomainID=@numDomainId
				AND numOppId=@numOppID
				AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9))) 

			--INSERT NEW ADDED ITEMS                        
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,
				[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numToWarehouseItemID,numShipmentMethod,
				numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired,vcAttrValues,numPromotionID,bitPromotionTriggered
				,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,dtPlannedStart,numWOAssignedTo,bitMarkupDiscount,ItemReleaseDate
				,vcChildKitSelectedItems,numWOQty,vcSKU,vcASIN
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
				(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),
				(select isnull(VN.monCost,0) from Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode and VN.numVendorID=IT.numVendorID where VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numToWarehouseItemID,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
				0,
				(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired, X.AttributeIDs,
				X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(vcVendorNotes,'')
				,X.vcInstruction,DATEADD(MINUTE,@ClientTimeZoneOffset,X.bintCompliationDate),DATEADD(MINUTE,@ClientTimeZoneOffset,X.dtPlannedStart),X.numWOAssignedTo,X.bitMarkupDiscount
				,@dtItemRelease,ISNULL(KitChildItems,''),X.numWOQty,X.vcSKU,X.vcASIN
			FROM
			(
				SELECT 
					*
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH  
				(                      
					numoppitemtCode numeric(9),numItemCode numeric(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc varchar(2000),                                    
					numWarehouseItmsID numeric(9),numToWarehouseItemID numeric(9),Op_Flag tinyint,ItemType varchar(30),DropShip bit,bitDiscountType bit,
					fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),
					numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(500), 
					bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000)
					,numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,dtPlannedStart DATETIME
					,numWOAssignedTo NUMERIC(18,0),bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX),numWOQty FLOAT,vcSKU VARCHAR(100),vcASIN VARCHAR(100)
				)
			)X 
			
			-- UPDATE EXISTING CUSTOMER PART NO
			UPDATE
				CPN
			SET
				CPN.CustomerPartNo = ISNULL(TEMPCPN.CustomerPartNo,'')
			FROM
				CustomerPartNumber CPN
			INNER JOIN
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) AS TEMPCPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.numDomainID=@numDomainID
				AND CPN.numCompanyId = @numCompany
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- INSERT NEW CUSTOMER PART NO
			INSERT INTO CustomerPartNumber
			(
				numCompanyId, numDomainID, CustomerPartNo, numItemCode 
			)
			SELECT 
				@numCompany, @numDomainID, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
			FROM
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) TEMPCPN 
			LEFT JOIN
				CustomerPartNumber CPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.CustomerPartNoID IS NULL
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- UPDATE ITEMS
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,vcModelID=X.vcModelID,numWarehouseItmsID=X.numWarehouseItmsID,numToWarehouseItemID = X.numToWarehouseItemID,             
				bitDropShip=X.DropShip,bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,
				numUOMId=numUOM,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,
				numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired,bitWorkOrder=X.bitWorkOrder,vcAttrValues=AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost,vcNotes=vcVendorNotes
				,bitMarkupDiscount=X.bitMarkupDiscount,ItemRequiredDate=X.ItemRequiredDate,vcChildKitSelectedItems=ISNULL(KitChildItems,'')
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,numToWarehouseItemID,DropShip,bitDiscountType,
					fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
					bitItemPriceApprovalRequired,bitWorkOrder,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) AS numCost,ISNULL(vcVendorNotes,'') vcVendorNotes,
					ItemRequiredDate,bitMarkupDiscount,KitChildItems
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
				WITH  
				(                      
					numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(2000),vcModelID VARCHAR(300), numWarehouseItmsID NUMERIC(9), 
					numToWarehouseItemID NUMERIC(18,0),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),
					numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),numProjectStageID NUMERIC(9),Attributes VARCHAR(500),
					bitItemPriceApprovalRequired BIT, bitWorkOrder BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),
					ItemRequiredDate DATETIME, bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX)
				)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppId=@numOppID

			--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
			DELETE FROM @TempKitConfiguration

			INSERT INTO @TempKitConfiguration
			(
				numOppItemID,
				numItemCode,
				numKitItemID,
				numKitChildItemID,
				numQty,
				numSequence
			)
			SELECT
				numoppitemtCode,
				numItemCode,
				numKitItemID,
				numChildItemID,
				numQty,
				numSequence
			FROM
				OpportunityItems
			CROSS APPLY
			(
				SELECT 
					Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 1)) As numKitItemID
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 2)) As numChildItemID
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 3)) As numQty
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 4)) As numSequence
				FROM  
				(
					SELECT 
						OutParam 
					FROM 
						SplitString(vcChildKitSelectedItems,',')
				) X
			) Y
			WHERE
				numOppId = @numOppID
				AND LEN(ISNULL(vcChildKitSelectedItems,'')) > 0
			ORDER BY
				ISNULL(Y.numSequence,0)

			--INSERT KIT ITEMS 
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				T1.numKitChildItemID,
				ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=T1.numKitChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),0),
				T1.numQty * OI.numUnitHour,
				T1.numQty,
				I.numBaseUnit,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			INNER JOIN
				@TempKitConfiguration T1
			ON
				T1.numOppItemID=OI.numoppitemtCode
				AND T1.numItemCode = OI.numItemCode
			JOIN
				Item I
			ON
				T1.numKitChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND (SELECT COUNT(*) FROM @TempKitConfiguration t2 WHERE t2.numOppItemID=OI.numoppitemtCode AND t2.numItemCode=OI.numItemCode AND ISNULL(t2.numKitItemID,0) = 0) > 0
			ORDER BY
				ISNULL(T1.numSequence,0)

			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),0),
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND (SELECT COUNT(*) FROM @TempKitConfiguration t2 WHERE t2.numOppItemID=OI.numoppitemtCode AND t2.numItemCode=OI.numItemCode AND ISNULL(t2.numKitItemID,0) = 0) = 0
			ORDER BY
				ISNULL(ID.sintOrder,0)

			IF (SELECT 
					COUNT(*) 
				FROM 
					OpportunityKitItems OI 
				INNER JOIN
					Item I
				ON
					OI.numChildItemID = I.numItemCode
				WHERE 
					numOppId=@numOppID
					AND ISNULL(charItemType,0) = 'P'
					AND numWareHouseItemId = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END

			IF (SELECT 
					COUNT(*) 
				FROM 
				(
					SELECT 
						t1.numOppItemID
						,t1.numItemCode
						,numKitItemID
						,numKitChildItemID
					FROM 
						@TempKitConfiguration t1
					WHERE
						ISNULL(t1.numKitItemID,0) > 0
				) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numOppItemID=TempChildItems.numOppItemID
					AND OKI.numChildItemID=TempChildItems.numKitItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numoppitemtcode = OKI.numOppItemID
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
				INNER JOIN
					Item IMain
				ON
					OKI.numChildItemID = IMain.numItemCode
				WHERE
					Item.charItemType = 'P'
					AND ISNULL(IMain.bitKitParent,0) = 1
					AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END

			INSERT INTO OpportunityKitChildItems
			(
				numOppID,
				numOppItemID,
				numOppChildItemID,
				numItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT 
				@numOppID,
				OKI.numOppItemID,
				OKI.numOppChildItemID,
				ItemDetails.numChildItemID,
				(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
				(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
				ItemDetails.numQtyItemsReq,
				ItemDetails.numUOMId,
				0,
				ISNULL(Item.monAverageCost,0)
			FROM
			(
				SELECT 
					t1.numOppItemID
					,t1.numItemCode
					,numKitItemID
					,numKitChildItemID
				FROM 
					@TempKitConfiguration t1
				WHERE
					ISNULL(t1.numKitItemID,0) > 0
			) TempChildItems
			INNER JOIN
				OpportunityKitItems OKI
			ON
				OKI.numOppId=@numOppId
				AND OKI.numOppItemID=TempChildItems.numOppItemID
				AND OKI.numChildItemID=TempChildItems.numKitItemID
			INNER JOIN
				OpportunityItems OI
			ON
				OI.numItemCode = TempChildItems.numItemCode
				AND OI.numoppitemtcode = OKI.numOppItemID
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			INNER JOIN
				ItemDetails
			ON
				TempChildItems.numKitItemID = ItemDetails.numItemKitID
				AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
			INNER JOIN
				Item 
			ON
				ItemDetails.numChildItemID = Item.numItemCode                                                  
			                                    
			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@numUserCntID
			END
			   		                                                                      
			EXEC sp_xml_removedocument @hDocItem                                               
		END     

		IF @tintOppType=1 AND @tintOppStatus=1 --Sales Order
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0) = 0 AND (ISNULL(numUnitHour,0) - ISNULL(numQtyShipped,0)) < 0)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_IS_LESS_THEN_SHIPPED_QTY',16,1)
				RETURN
			END

			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems INNER JOIN WorkOrder ON OpportunityItems.numOppID=WorkOrder.numOppID AND OpportunityItems.numoppitemtCode=WorkOrder.numOppItemID AND ISNULL(WorkOrder.numParentWOID,0)=0 AND WorkOrder.numWOStatus=23184 AND ISNULL(OpportunityItems.numUnitHour,0) > ISNULL(WorkOrder.numQtyItemsReq,0) WHERE OpportunityItems.numOppID=@numOppID)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_CAN_NOT_BE_GREATER_THEN_COMPLETED_WORK_ORDER',16,1)
				RETURN
			END
		END
		ELSE IF @tintOppType=2 AND @tintOppStatus=1 --Purchase Order
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0) = 0 AND (ISNULL(numUnitHour,0) - ISNULL(numUnitHourReceived,0)) < 0)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_IS_LESS_THEN_RECEIVED_QTY',16,1)
				RETURN
			END
		END

		--FOLLOWING DELETE STATEMENT IS COMMENTED BECUASE NOW WE ARE NOT ALLOWING USER TO EIDT SERIAL/LOT ITEM UNTILL USER REMOVED ASSIGNED SERIAL/LOT NUMBERS
	    --Delete previously added serial lot number added for opportunity
		--DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
	END

	IF @tintOppStatus = 1
	BEGIN
		IF (SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0)=0) <>
			(SELECT COUNT(*) FROM OpportunityItems INNER JOIN WareHouseItems ON OpportunityItems.numWarehouseItmsID=WareHouseItems.numWareHouseItemID AND OpportunityItems.numItemCode=WareHouseItems.numItemID WHERE numOppId=@numOppID AND OpportunityItems.numWarehouseItmsID > 0 AND ISNULL(bitDropShip,0)=0)
		BEGIN
			RAISERROR('INVALID_ITEM_WAREHOUSES',16,1)
			RETURN
		END
	END

	IF(SELECT 
			COUNT(*) 
		FROM 
			OpportunityBizDocItems OBDI 
		INNER JOIN 
			OpportunityBizDocs OBD 
		ON 
			OBDI.numOppBizDocID=OBD.numOppBizDocsID 
		WHERE 
			OBD.numOppId=@numOppID AND OBDI.numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)) > 0
	BEGIN
		RAISERROR('ITEM_USED_IN_BIZDOC',16,1)
		RETURN
	END

	IF(SELECT 
			COUNT(*)
		FROM
			OpportunityItems OI
		INNER JOIN
		(
			SELECT
				numOppItemID
				,SUM(numUnitHour) numInvoiceBillQty
			FROM 
				OpportunityBizDocItems OBDI 
			INNER JOIN 
				OpportunityBizDocs OBD 
			ON 
				OBDI.numOppBizDocID=OBD.numOppBizDocsID 
			WHERE 
				OBD.numOppId=@numOppID
				AND ISNULL(OBD.bitAuthoritativeBizDocs,0) = 1
			GROUP BY
				numOppItemID
		) AS TEMP
		ON
			TEMP.numOppItemID = OI.numoppitemtCode
		WHERE
			OI.numOppId = @numOppID
			AND OI.numUnitHour < Temp.numInvoiceBillQty
		) > 0
	BEGIN
		RAISERROR('ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_INVOICE/BILL_QTY',16,1)
		RETURN
	END

	IF @tintOppType=1 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
	BEGIN
		IF(SELECT 
				COUNT(*)
			FROM
			(
				SELECT
					numOppItemID
					,SUM(numUnitHour) PackingSlipUnits
				FROM 
					OpportunityBizDocItems OBDI 
				INNER JOIN 
					OpportunityBizDocs OBD 
				ON 
					OBDI.numOppBizDocID=OBD.numOppBizDocsID 
				WHERE 
					OBD.numOppId=@numOppID
					AND OBD.numBizDocId=29397 --Picking Slip
				GROUP BY
					numOppItemID
			) AS TEMP
			INNER JOIN
				OpportunityItems OI
			ON
				TEMP.numOppItemID = OI.numoppitemtCode
			WHERE
				OI.numUnitHour < PackingSlipUnits
			) > 0
		BEGIN
			RAISERROR('ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_PACKING_SLIP_QTY',16,1)
			RETURN
		END
	END

	SET @TotalAmount=0                                                           
	SELECT @TotalAmount = SUM(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID                                                                          
  
	UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numOppID

/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
            
			
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT

			SET @bitIsInitalSalesOrder=(SELECT bitIsInitalSalesOrder FROM OpportunityMaster WHERE numOppId=@numOppID AND numDomainId=@numDomainID)
			--SET @bitViolatePrice=(SELECT bitMarginPriceViolated FROM Domain WHERE numDomainId=@numDomainID)
			IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainId=@numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1)
			BEGIN
				SET @bitViolatePrice = 1
			END
			
			IF(@bitViolatePrice=1 AND @bitIsInitalSalesOrder=1 AND @tintClickBtn = 1)
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus=@DealStatus WHERE numOppId=@numOppID
			END
			ELSE
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus = @DealStatus WHERE numOppId=@numOppID
				DECLARE @tintShipped AS TINYINT               
				SELECT @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID               
				
				IF @tintOppStatus=1 AND @tintCommitAllocation=1             
				BEGIN         
					EXEC USP_UpdatingInventoryonCloseDeal @numOppID,0,@numUserCntID
				END  
				declare @tintCRMType as numeric(9)        
				declare @AccountClosingDate as datetime        

				select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from OpportunityMaster where numOppID=@numOppID         
				select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

				--When deal is won                                         
				IF @DealStatus = 1 
				BEGIN
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID              
					END

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=getutcdate()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
				END         
				--When Deal is Lost
				ELSE IF @DealStatus = 2
				BEGIN
					SELECT @AccountClosingDate=bintAccountClosingDate FROM OpportunityMaster WHERE numOppID=@numOppID         
					
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID
					END
				END 
			END

			/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
			-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
			if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
			begin        
				update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				where numDivisionID=@numDivisionID        
			end        

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50), @vcAltContact VARCHAR(200)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9), @numContact NUMERIC(18,0)
DECLARE @bitIsPrimary BIT, @bitAltContact BIT;
DECLARE @tintAddressOf TINYINT

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

IF EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numOppId=@numOppID AND ISNULL(tintBillToType,0) = 0 AND ISNULL(numBillToAddressID,0) = 0)
BEGIN
	SELECT 
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM 
		dbo.AddressDetails AD
	WHERE 
		AD.numDomainID=@numDomainID 
		AND AD.numRecordID=@numDivisionID 
		AND AD.tintAddressOf = 2 
		AND AD.tintAddressType = 1 
		AND AD.bitIsPrimary=1       

	UPDATE OpportunityMaster SET tintBillToType=2,numBillToAddressID=0 WHERE numOppId=@numOppID

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END

IF EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numOppId=@numOppID AND ISNULL(tintShipToType,0) = 0 AND ISNULL(numShipToAddressID,0) = 0)
BEGIN
	SELECT 
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM 
		dbo.AddressDetails AD
	WHERE 
		AD.numDomainID=@numDomainID 
		AND AD.numRecordID=@numDivisionID 
		AND AD.tintAddressOf = 2 
		AND AD.tintAddressType = 2 
		AND AD.bitIsPrimary=1       

	UPDATE OpportunityMaster SET tintShipToType=2,numShipToAddressID=0 WHERE numOppId=@numOppID

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END

--Bill Address
IF @numBillAddressId>0
BEGIN
	SELECT 
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM 
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numBillAddressId

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numBillAddressId
	END

	UPDATE OpportunityMaster SET tintBillToType=2,numBillToAddressID=0 WHERE numOppId=@numOppID

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END
  
--Ship Address
IF @intUsedShippingCompany = 92 -- WILL CALL (PICK UP FROM Warehouse)
BEGIN
	IF EXISTS (SELECT numWareHouseID FROM Warehouses WHERE Warehouses.numDomainID = @numDomainID AND numWareHouseID = @numWillCallWarehouseID AND ISNULL(numAddressID,0) > 0)
	BEGIN
		SELECT  
			@vcStreet=ISNULL(vcStreet,'')
			,@vcCity=ISNULL(vcCity,'')
			,@vcPostalCode=ISNULL(AddressDetails.vcPostalCode,'')
			,@numState=ISNULL(numState,0)
			,@numCountry=ISNULL(numCountry,0)
			,@vcAddressName=vcAddressName
		FROM 
			dbo.Warehouses 
		INNER JOIN
			AddressDetails
		ON
			Warehouses.numAddressID = AddressDetails.numAddressID
		WHERE 
			Warehouses.numDomainID = @numDomainID 
			AND numWareHouseID = @numWillCallWarehouseID
	END
	ELSE
	BEGIN
	SELECT  
		@vcStreet=ISNULL(vcWStreet,'')
		,@vcCity=ISNULL(vcWCity,'')
		,@vcPostalCode=ISNULL(vcWPinCode,'')
		,@numState=ISNULL(numWState,0)
		,@numCountry=ISNULL(numWCountry,0)
		,@vcAddressName=''
	FROM 
		dbo.Warehouses 
	WHERE 
		numDomainID = @numDomainID 
		AND numWareHouseID = @numWillCallWarehouseID
	END

	UPDATE OpportunityMaster SET tintShipToType=2 WHERE numOppId=@numOppID
 
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = '', --  varchar(100)
	@numCompanyId =0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = 0,
	@bitAltContact = 0,
	@vcAltContact = ''
END
ELSE IF @numShipAddressId>0
BEGIN
	SELECT
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM  
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numShipAddressId
 

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numShipAddressId
	END

	UPDATE OpportunityMaster SET tintShipToType=2,numShipToAddressID=0 WHERE numOppId=@numOppID

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END

-- IMPORTANT:KEEP THIS BELOW ADDRESS UPDATE LOGIC
-- GET TAX APPLICABLE TO DIVISION WHEN ORDER IS CREATED AFTER THAT NOT NEED TO CHANGE IT
IF @bitNewOrder = 1
BEGIN
	--INSERT TAX FOR DIVISION   
	IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
	BEGIN
		INSERT INTO dbo.OpportunityMasterTaxItems 
		(
			numOppId,
			numTaxItemID,
			fltPercentage,
			tintTaxType,
			numTaxID
		) 
		SELECT 
			@numOppID,
			TI.numTaxItemID,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			TaxItems TI 
		JOIN 
			DivisionTaxTypes DTT 
		ON 
			TI.numTaxItemID = DTT.numTaxItemID
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
		) AS TEMPTax
		WHERE 
			DTT.numDivisionID=@numDivisionId 
			AND DTT.bitApplicable=1
		UNION 
		SELECT 
			@numOppID,
			0,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			dbo.DivisionMaster 
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
		) AS TEMPTax
		WHERE 
			bitNoTax=0 
			AND numDivisionID=@numDivisionID	
		UNION 
		SELECT
			@numOppId
			,1
			,decTaxValue
			,tintTaxType
			,numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,@numOppId,1,NULL)		
	END	
END

--INSERT TAX FOR DIVISION   
IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN
--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID
) 
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID = IT.numTaxItemID 
WHERE 
	OI.numOppId=@numOppID 
	AND IT.bitApplicable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	0,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numOppID 
	AND I.bitTaxable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT
	@numOppId,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
FROM
	dbo.OpportunityItems OI 
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
INNER JOIN
	TaxDetails TD
ON
	TD.numTaxID = IT.numTaxID
	AND TD.numDomainId = @numDomainId
WHERE
	OI.numOppId = @numOppID
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

END

	UPDATE 
		OI
	SET
		OI.vcInclusionDetail = [dbo].[GetOrderAssemblyKitInclusionDetails](OI.numOppID,OI.numoppitemtCode,OI.numUnitHour,1,1)
	FROM 
		OpportunityItems OI
	INNER JOIN
		Item I
	ON
		OI.numItemCode = I.numItemCode
	WHERE
		numOppId=@numOppID
		AND ISNULL(I.bitKitParent,0) = 1
  
	UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID

	IF @tintOppType=1 AND @tintOppStatus=1 
	BEGIN
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numStatus
		EXEC USP_OpportunityMaster_CreateBackOrderPO @numDomainID,@numUserCntID,@numOppID
	END

	IF EXISTS (SELECT 
					numoppitemtCode 
				FROM 
					OpportunityItems 
				INNER JOIN 
					Item 
				ON 
					OpportunityItems.numItemCode=Item.numItemCode 
				WHERE 
					numOppId=@numOppID 
					AND charItemType='P' 
					AND ISNULL(numWarehouseItmsID,0) = 0 
					AND ISNULL(bitDropShip,0) = 0)
	BEGIN
		RAISERROR('WAREHOUSE_REQUIRED',16,1)
	END

	IF (SELECT COUNT(*) FROM WorkOrder WHERE numDomainID=@numDomainID AND numOppId=@numOppId AND ISNULL(numOppItemID,0) > 0 AND numOppItemID NOT IN (SELECT OIInner.numoppitemtCode FROM OpportunityItems OIInner WHERE OIInner.numOppId=@numOppID)) > 0
	BEGIN
		RAISERROR ( 'WORK_ORDER_EXISTS', 16, 1 ) ;
	END

	IF (SELECT 
			COUNT(*) 
		FROM 
			WorkOrder 
		INNER JOIN 
			OpportunityItems 
		ON 
			OpportunityItems.numOppId=@numOppID
			AND OpportunityItems.numoppitemtCode=WorkOrder.numOppItemID
		WHERE 
			WorkOrder.numDomainID=@numDomainID 
			AND WorkOrder.numOppId=@numOppID
			AND WorkOrder.numWareHouseItemId <> OpportunityItems.numWarehouseItmsID) > 0
	BEGIN
		RAISERROR ('CAN_NOT_CHANGE_ASSEMBLY_WAREHOUSE_WORK_ORDER_EXISTS', 16, 1 ) ;
	END

	IF @tintOppType = 1 AND @tintOppStatus=1
	BEGIN
		EXEC USP_OpportunityItems_CheckWarehouseMapping @numDomainID,@numOppID
	END
	ELSE IF @tintOppType = 2 AND @tintOppStatus = 1
	BEGIN
		EXEC USP_OpportunityMaster_AddVendor @numDomainID,@numOppID
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
/****** Object:  StoredProcedure [dbo].[USP_ProsContacts]    Script Date: 07/26/2008 16:20:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_proscontacts')
DROP PROCEDURE usp_proscontacts
GO
CREATE PROCEDURE [dbo].[USP_ProsContacts]            
@numDivisionID as numeric(9)=0,  
@numDomainID as numeric(9)=0,
@numUserCntID numeric(9)=0,
@ClientTimeZoneOffset as int,
@numUserGroupID numeric(9)=0                                                                                   
as            
            
-- select case when (isnull(vcFirstName,'')='' and isnull(vcLastName,'')='') then '-' else vcFirstName+' '+ vcLastName end   as [Name], dbo.GetListIemName(vcPosition)+','+ dbo.GetListIemName(numContacttype)  as Pos, vcEmail as  Email,             
-- case when numPhone<>'' then + numPhone +case when numPhoneExtension<>'' then ' - ' + numPhoneExtension else '' end  else '' end as [Phone],                                                                                                              
-- isnull(vcAsstFirstName,'')+' '+isnull(vcAsstLastName,'') as Asst,    
-- case when numAsstPhone<>'' then + numAsstPhone +case when numAsstExtn<>'' then ' - ' + numAsstExtn else '' end  else '' end as [AsstPhone],                                                                                                              
-- numContactID,ac.numCreatedBy,dM.numTerid,isnull(AC.bitPrimaryContact,0) as bitPrimaryContact
-- FROM AdditionalContactsInformation AC join               
-- divisionmaster DM on DM.numDivisionID=AC.numDivisionID                  
-- WHERE AC.numDivisionID = @numDivisionID and AC.numDomainID=  @numDomainID

declare @numRelCntType as numeric(9)

select @numRelCntType=isnull(CI.numCompanyType,0) from  DivisionMaster DM JOIN CompanyInfo CI ON DM.numCompanyID = CI.numCompanyId 
	where DM.numDomainID=@numDomainID and DM.numDivisionID=@numDivisionID

	IF EXISTS (SELECT * FROM View_DynamicColumns where numFormId=56 and numDomainID=@numDomainID AND numAuthGroupID=@numUserGroupID AND ISNULL(numRelCntType,0) = 0) OR EXISTS (SELECT * FROM View_DynamicCustomColumns where numFormId=56 and numDomainID=@numDomainID AND numAuthGroupID=@numUserGroupID AND ISNULL(numRelCntType,0)=0)
	BEGIN
		SET @numRelCntType = 0
	END


declare @strSql as varchar(8000) 

declare @tintOrder as tinyint                                                        
declare @vcFieldName as varchar(50)                                                        
declare @vcListItemType as varchar(3)                                                   
declare @vcListItemType1 as varchar(1)                                                       
declare @vcAssociatedControlType varchar(20)                                                        
declare @numListID AS numeric(9)                                                        
declare @vcDbColumnName varchar(20)                            
declare @WhereCondition varchar(2000)                             
declare @vcLookBackTableName varchar(2000)                      
Declare @bitCustom as bit  
DECLARE @bitAllowEdit AS CHAR(1)                 
Declare @numFieldId as numeric  
DECLARE @bitAllowSorting AS CHAR(1)                   
declare @vcColumnName AS VARCHAR(500)                            
set @tintOrder=0                                                        
set @WhereCondition =''                       
                         
declare @Nocolumns as tinyint                      
set @Nocolumns=0                      
  
select @Nocolumns=isnull(sum(TotalRow),0)  from(            
Select count(*) TotalRow from View_DynamicColumns where numFormId=56 and numDomainID=@numDomainID AND numAuthGroupID=@numUserGroupID AND numRelCntType=@numRelCntType 
Union 
Select count(*) TotalRow from View_DynamicCustomColumns where numFormId=56 and numDomainID=@numDomainID AND numAuthGroupID=@numUserGroupID AND numRelCntType=@numRelCntType) TotalRows

   CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int)
                 
     
if @Nocolumns=0
BEGIN
INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType,bitCustom,intColumnWidth)
select 56,numFieldId,1,Row_number() over(order by tintRow desc),@numDomainID,@numUserGroupID,0,0,intColumnWidth
 FROM View_DynamicDefaultColumns
 where numFormId=56 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID
order by tintOrder asc      

END
                 
set @strSql=' select convert(varchar(10),isnull(ADC.numContactId,0)) as numContactId            
                    
       ,isnull(DM.numDivisionID,0) numDivisionID, isnull(DM.numTerID,0)numTerID,isnull( ADC.numRecOwner,0) numRecOwner,isnull( DM.tintCRMType,0) tintCRMType,
	   isnull(ADC.bitPrimaryContact,0) as bitPrimaryContact '                      
                         
   INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID ,intColumnWidth
 FROM View_DynamicColumns 
 where numFormId=56 and numDomainID=@numDomainID AND numAuthGroupID=@numUserGroupID AND numRelCntType=@numRelCntType
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
       
       UNION
    
   select tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth
 from View_DynamicCustomColumns
 where numFormId=56 and numDomainID=@numDomainID AND numAuthGroupID=@numUserGroupID AND numRelCntType=@numRelCntType
 AND ISNULL(bitCustom,0)=1
 
  order by tintOrder asc  
   
                                                      
Declare @ListRelID as numeric(9) 
  
    select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from  #tempForm order by tintOrder asc            

                   
while @tintOrder>0                                                        
begin                                                        
                                            
   if @bitCustom = 0                
 begin           
        
    declare @Prefix as varchar(5)                      
      if @vcLookBackTableName = 'AdditionalContactsInformation'                      
    set @Prefix = 'ADC.'                      
      if @vcLookBackTableName = 'DivisionMaster'                      
    set @Prefix = 'DM.'         
        
                  SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'
                          
 if @vcAssociatedControlType='SelectBox'                                                        
        begin                                                        
                                                        
     if @vcListItemType='LI'                                                         
     begin                                                        
      set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                       
      set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                        
     end                                                        
     else if @vcListItemType='S'                                                         
     begin                                                        
      set @strSql=@strSql+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName+']'                                                        
      set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID='+@vcDbColumnName                                                        
     end                                                        
     else if @vcListItemType='T'                                                         
     begin                                                        
      set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                        
      set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                        
     end                       
     else if   @vcListItemType='U'                                                     
    begin                       
    set @strSql=@strSql+',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'                                                        
    end                      
    end         
 else if @vcAssociatedControlType='DateField'                                                      
 begin                
           
     set @strSql=@strSql+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''          
     set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''          
     set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '          
     set @strSql=@strSql+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'                    
   end        
              
else if @vcAssociatedControlType='TextBox' OR @vcAssociatedControlType='Label'                                                     
begin        
         
     set @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'         
 when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' else @vcDbColumnName end+' ['+ @vcColumnName+']'                   
 end      
else                                                
begin      
 set @strSql=@strSql+', '+ @vcDbColumnName +' ['+ @vcColumnName+']'                
         
 end                                               
            
              
                    
end                
else if @bitCustom = 1                
begin                
            
	SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'
       
   select @vcFieldName=FLd_label,@vcAssociatedControlType=fld_type,@vcDbColumnName='Cust'+Convert(varchar(10),Fld_Id)                               
    from CFW_Fld_Master                                                
   where    CFW_Fld_Master.Fld_Id = @numFieldId                 
                
                 
    if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'             
   begin                
                   
    set @strSql= @strSql+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=ADC.numContactId   '                                                         
   end      
    else if @vcAssociatedControlType = 'CheckBox'             
   begin              
                 
    set @strSql= @strSql+',case when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=0 then ''No'' when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=1 then ''Yes'' end   ['+ @vcColumnName +']'              
   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '               
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=ADC.numContactId   '                                                       
   end              
  else if @vcAssociatedControlType = 'DateField'            
   begin                
                   
    set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=ADC.numContactId   '                                                         
   end                
   else if @vcAssociatedControlType = 'SelectBox'            
   begin                
    set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
    set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
     on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=ADC.numContactId   '                                                         
    set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
   end                 
end                    
                    
   select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc            
 
  if @@rowcount=0 set @tintOrder=0 

                                  
end                             
                          
                          
                                  
set @strSql=@strSql+' FROM AdditionalContactsInformation ADC                                                                             
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                                            
  JOIN CompanyInfo CI ON DM.numCompanyID = CI.numCompanyId
  LEFT JOIN AddressDetails AD ON AD.numRecordID = ADC.numContactID AND AD.tintAddressOf=1 AND AD.tintAddressType=0 AND AD.bitIsPrimary=1
   '+@WhereCondition+ '                     
  WHERE CI.numDomainID=DM.numDomainID   
  and DM.numDomainID=ADC.numDomainID  and ADC.numDivisionID = '+convert(varchar(18),@numDivisionID)+ ' and ADC.numDomainID=  '+convert(varchar(18),@numDomainID)
--' union select convert(varchar(10),count(*)),null,null,null,null,null '+@strColumns+' from tblcontacts order by RowNumber'                                                 
                                                     
print @strSql                              
                      
exec (@strSql)


SELECT * FROM #tempForm

DROP TABLE #tempForm  

GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportQueryExecute')
DROP PROCEDURE USP_ReportQueryExecute
GO
CREATE PROCEDURE [dbo].[USP_ReportQueryExecute]     
    @numDomainID NUMERIC(18,0),
    @numUserCntID NUMERIC(18,0),
	@numReportID NUMERIC(18,0),
    @ClientTimeZoneOffset INT, 
    @textQuery NTEXT,
	@numCurrentPage NUMERIC(18,0)
AS                 
BEGIN
	DECLARE @tintReportType INT
	DECLARE @numReportModuleID INT
	SELECT @tintReportType=ISNULL(tintReportType,0),@numReportModuleID=ISNULL(numReportModuleID,0) FROM ReportListMaster WHERE numReportID=@numReportID

	IF CHARINDEX('WareHouseItems.numBackOrder',CAST(@textQuery AS VARCHAR(MAX))) > 0 AND @numReportModuleID <> 6
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'WareHouseItems.numBackOrder','(CASE WHEN ISNULL((SELECT SUM(WIInner.numOnHand) FROM WareHouseItems WIInner WHERE WIInner.numItemID=WareHouseItems.numItemID AND WIInner.numWareHouseItemID <> WareHouseItems.numWareHouseItemID AND WIInner.numWareHouseID = WareHouseItems.numWareHouseID),0) >= WareHouseItems.numBackOrder THEN 0 ELSE ISNULL(WareHouseItems.numBackOrder,0) END)')
	END
	
	IF CHARINDEX('OpportunityItems.vcOpenBalance',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityItems.vcOpenBalance','(CASE WHEN OpportunityMaster.tintOppType=2 THEN (ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numUnitHourReceived,0)) ELSE (ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0)) END)')	
	END	

	IF CHARINDEX('isnull(OpportunityItems.numRemainingQtyToPick,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityItems.numRemainingQtyToPick,0)','(isnull(OpportunityItems.numUnitHour,0) - isnull(OpportunityItems.numQtyPicked,0))')	
	END	

	IF CHARINDEX('OpportunityItems.numRemainingQtyToPick',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityItems.numRemainingQtyToPick','(isnull(OpportunityItems.numUnitHour,0) - isnull(OpportunityItems.numQtyPicked,0))')	
	END		

	IF CHARINDEX('isnull(OpportunityItems.numRemainingQtyToShip,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityItems.numRemainingQtyToShip,0)','(isnull(OpportunityItems.numUnitHour,0) - isnull(OpportunityItems.numQtyShipped,0))')	
	END	

	IF CHARINDEX('OpportunityItems.numRemainingQtyToShip',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityItems.numRemainingQtyToShip','(isnull(OpportunityItems.numUnitHour,0) - isnull(OpportunityItems.numQtyShipped,0))')	
	END	

	IF CHARINDEX('isnull(OpportunityItems.numRemainingQtyToInvoice,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityItems.numRemainingQtyToInvoice,0)','(isnull(OpportunityItems.numUnitHour,0) - isnull((SELECT SUM(numUnitHour) FROM OpportunityBizDocItems INNER JOIN OpportunityBizDocs ON OpportunityBizDocItems.numOppBizDocID = OpportunityBizDocs.numOppBizDocsId  WHERE OpportunityBizDocs.numOppId = OpportunityItems.numOppId AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode AND ISNULL(bitAuthoritativeBizDocs, 0) = 1 AND ISNULL(OpportunityBizDocs.numBizDocID,0) = 287),0))')	
	END	

	IF CHARINDEX('OpportunityItems.numRemainingQtyToInvoice',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityItems.numRemainingQtyToInvoice','(isnull(OpportunityItems.numUnitHour,0) - isnull((SELECT SUM(numUnitHour) FROM OpportunityBizDocItems INNER JOIN OpportunityBizDocs ON OpportunityBizDocItems.numOppBizDocID = OpportunityBizDocs.numOppBizDocsId  WHERE OpportunityBizDocs.numOppId = OpportunityItems.numOppId AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode AND ISNULL(bitAuthoritativeBizDocs, 0) = 1 AND ISNULL(OpportunityBizDocs.numBizDocID,0) = 287),0))')	
	END	

	IF CHARINDEX('AND OpportunityItems.monPendingSales',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'AND OpportunityItems.monPendingSales','AND (ISNULL((ISNULL(ISNULL(dbo.fn_UOMConversion(ISNULL(Item.numBaseUnit, 0), OpportunityItems.numItemCode, Item.numDomainId, ISNULL(OpportunityItems.numUOMId, 0)), 1) * OpportunityItems.numUnitHour, 0) - isnull(OpportunityItems.numQtyShipped, 0) ) * (isnull(OpportunityItems.monPrice, 0)), 0))')	
	END	
	
	IF CHARINDEX('AND OpportunityItems.numPendingToBill',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'AND OpportunityItems.numPendingToBill','AND ISNULL((isnull(OpportunityItems.numUnitHourReceived, 0)) - (ISNULL(( SELECT SUM(numUnitHour)  FROM	 OpportunityBizDocItems  INNER JOIN	OpportunityBizDocs ON OpportunityBizDocItems.numOppBizDocID = OpportunityBizDocs.numOppBizDocsId  WHERE OpportunityBizDocs.numOppId = OpportunityItems.numOppId AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode AND ISNULL(bitAuthoritativeBizDocs, 0) = 1), 0)), 0)')	
	END	
		
	IF CHARINDEX('AND OpportunityItems.numPendingtoReceive',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'AND OpportunityItems.numPendingtoReceive','AND isnull((isnull((SELECT SUM(numUnitHour) FROM OpportunityBizDocItems INNER JOIN OpportunityBizDocs ON OpportunityBizDocItems.numOppBizDocID = OpportunityBizDocs.numOppBizDocsId WHERE OpportunityBizDocs.numOppId = OpportunityItems.numOppId AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode AND ISNULL(bitAuthoritativeBizDocs, 0) = 1), 0)) - (isnull(OpportunityItems.numUnitHourReceived, 0)), 0)')	
	END

	IF CHARINDEX('AND WareHouseItems.vcLocation',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'AND WareHouseItems.vcLocation','AND WareHouseItems.numWLocationID')
	END

	IF CHARINDEX('ReturnItems.monPrice',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'ReturnItems.monPrice','CAST(ReturnItems.monPrice AS NUMERIC(18,4))')
	END

	IF CHARINDEX('OpportunityItems.monPrice',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityItems.monPrice','CAST(OpportunityItems.monPrice AS NUMERIC(18,4))')
	END

	IF CHARINDEX('isnull(category.numCategoryID,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(category.numCategoryID,0)' ,'(SELECT ISNULL(STUFF((SELECT '','' + CONVERT(VARCHAR(50) , Category.vcCategoryName) FROM ItemCategory LEFT JOIN Category ON ItemCategory.numCategoryID = Category.numCategoryID WHERE ItemCategory.numItemID = Item.numItemCode FOR XML PATH('''')), 1, 1, ''''),''''))')
	END

	IF CHARINDEX('dbo.FormatedDateFromDate(dateadd(minute,-@ClientTimeZoneOffset,OpportunityBizDocs.dtFullPaymentDate),@numDomainId)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'dbo.FormatedDateFromDate(dateadd(minute,-@ClientTimeZoneOffset,OpportunityBizDocs.dtFullPaymentDate),@numDomainId)' ,'(CASE 
																																															WHEN ISNULL(OpportunityMaster.tintOppType,0) = 2 AND ISNULL(OpportunityBizDocs.monDealAmount,0) > 0 AND ISNULL(OpportunityBizDocs.monAmountPaid,0) >= ISNULL(OpportunityBizDocs.monDealAmount,0)
																																															THEN dbo.FormatedDateFromDate((SELECT MAX(BPH.dtPaymentDate) FROM BillPaymentDetails BPD INNER JOIN BillPaymentHeader BPH ON BPD.numBillPaymentID=BPH.numBillPaymentID  WHERE BPD.numOppBizDocsID=OpportunityBizDocs.numOppBizDocsId),@numDomainID)
																																															WHEN ISNULL(OpportunityMaster.tintOppType,0) = 1 AND ISNULL(OpportunityBizDocs.monDealAmount,0) > 0 AND ISNULL(OpportunityBizDocs.monAmountPaid,0) >= ISNULL(OpportunityBizDocs.monDealAmount,0)
																																															THEN dbo.FormatedDateFromDate((SELECT MAX(dtCreatedDate) FROM DepositeDetails WHERE DepositeDetails.numOppBizDocsID=OpportunityBizDocs.numOppBizDocsId),@numDomainID)
																																															ELSE ''''
																																														END)')
	END

	--KEEP IT BELOW ABOVE REPLACEMENT
	IF CHARINDEX('OpportunityBizDocs.dtFullPaymentDate',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityBizDocs.dtFullPaymentDate' ,'CAST((CASE 
																												WHEN ISNULL(OpportunityMaster.tintOppType,0) = 2 AND ISNULL(OpportunityBizDocs.monDealAmount,0) > 0 AND ISNULL(OpportunityBizDocs.monAmountPaid,0) >= ISNULL(OpportunityBizDocs.monDealAmount,0)
																												THEN (SELECT MAX(BPH.dtPaymentDate) FROM BillPaymentDetails BPD INNER JOIN BillPaymentHeader BPH ON BPD.numBillPaymentID=BPH.numBillPaymentID  WHERE BPD.numOppBizDocsID=OpportunityBizDocs.numOppBizDocsId)
																												WHEN ISNULL(OpportunityMaster.tintOppType,0) = 1 AND ISNULL(OpportunityBizDocs.monDealAmount,0) > 0 AND ISNULL(OpportunityBizDocs.monAmountPaid,0) >= ISNULL(OpportunityBizDocs.monDealAmount,0)
																												THEN (SELECT MAX(dtCreatedDate) FROM DepositeDetails WHERE DepositeDetails.numOppBizDocsID=OpportunityBizDocs.numOppBizDocsId)
																												ELSE ''''
																											END) AS DATE)')
	END

	IF CHARINDEX('dbo.FormatedDateFromDate(dateadd(minute,-@ClientTimeZoneOffset,OpportunityMaster.vcLastSalesOrderDate),@numDomainId)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'dbo.FormatedDateFromDate(dateadd(minute,-@ClientTimeZoneOffset,OpportunityMaster.vcLastSalesOrderDate),@numDomainId)' ,'dbo.FormatedDateFromDate((SELECT MAX(dateadd(minute,-@ClientTimeZoneOffset,OMInner.bintCreatedDate)) FROM OpportunityMaster OMInner WHERE OMInner.numDomainID=@numDomainID AND OMInner.numDivisionID=DivisionMaster.numDivisionID AND OMInner.tintOppType = 1 AND OMInner.tintOppStatus = 1),@numDomainID)')
	END

	--KEEP IT BELOW ABOVE REPLACEMENT
	IF CHARINDEX('dateadd(minute,-@ClientTimeZoneOffset,OpportunityMaster.vcLastSalesOrderDate)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'dateadd(minute,-@ClientTimeZoneOffset,OpportunityMaster.vcLastSalesOrderDate)' ,'CAST((SELECT MAX(dateadd(minute,-@ClientTimeZoneOffset,OMInner.bintCreatedDate)) AS bintCreatedDate FROM OpportunityMaster OMInner WHERE OMInner.numDomainID=@numDomainID AND OMInner.numDivisionID=DivisionMaster.numDivisionID AND OMInner.tintOppType = 1 AND OMInner.tintOppStatus = 1) AS DATE)')
	END
	--KEEP IT BELOW ABOVE REPLACEMENT
	IF CHARINDEX('OpportunityMaster.vcLastSalesOrderDate',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityMaster.vcLastSalesOrderDate' ,'CAST((SELECT MAX(OMInner.bintCreatedDate) FROM OpportunityMaster OMInner WHERE OMInner.numDomainID=@numDomainID AND OMInner.numDivisionID=DivisionMaster.numDivisionID AND OMInner.tintOppType = 1 AND OMInner.tintOppStatus = 1) AS DATE)')
	END

	IF CHARINDEX('isnull(OpportunityMaster.monProfit,''0'')',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityMaster.monProfit,''0'')' ,'ISNULL(dbo.GetOrderItemProfitAmountOrMargin(OpportunityMaster.numDomainID,OpportunityItems.numOppId,OpportunityItems.numoppitemtCode,1),0)')
	END

	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityMaster.monProfit',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityMaster.monProfit' ,'ISNULL(dbo.GetOrderItemProfitAmountOrMargin(OpportunityMaster.numDomainID,OpportunityItems.numOppId,OpportunityItems.numoppitemtCode,1),0)')
	END

	IF CHARINDEX('isnull(OpportunityBizDocItems.monProfit,''0'')',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityBizDocItems.monProfit,''0'')','ISNULL(dbo.GetBizDocItemProfitAmountOrMargin(OpportunityMaster.numDomainID,OpportunityMaster.numOppId,OpportunityBizDocItems.numOppBizDocID,OpportunityBizDocItems.numOppBizDocItemID,1),0)')
	END

	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityBizDocItems.monProfit',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityBizDocItems.monProfit','ISNULL(dbo.GetBizDocItemProfitAmountOrMargin(OpportunityMaster.numDomainID,OpportunityMaster.numOppId,OpportunityBizDocItems.numOppBizDocID,OpportunityBizDocItems.numOppBizDocItemID,1),0)')
	END

	IF CHARINDEX('ISNULL(PT.numItemCode,0)=Item.numItemCode',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'ISNULL(PT.numItemCode,0)=Item.numItemCode' ,'Item.numItemCode=PT.numItemCode')
	END

	IF CHARINDEX('isnull(OpportunityMaster.numProfitPercent,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityMaster.numProfitPercent,0)' ,'ISNULL(dbo.GetOrderItemProfitAmountOrMargin(OpportunityMaster.numDomainID,OpportunityItems.numOppId,OpportunityItems.numoppitemtCode,2),0)')
	END
	
	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityMaster.numProfitPercent',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityMaster.numProfitPercent' ,'ISNULL(dbo.GetOrderItemProfitAmountOrMargin(OpportunityMaster.numDomainID,OpportunityItems.numOppId,OpportunityItems.numoppitemtCode,2),0)')
	END

	IF CHARINDEX('isnull(OpportunityBizDocItems.numProfitPercent,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityBizDocItems.numProfitPercent,0)','ISNULL(dbo.GetBizDocItemProfitAmountOrMargin(OpportunityMaster.numDomainID,OpportunityItems.numOppId,OpportunityItems.numoppitemtCode,2),0)')
	END
	
	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityBizDocItems.numProfitPercent',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityBizDocItems.numProfitPercent' ,'ISNULL(dbo.GetBizDocItemProfitAmountOrMargin(OpportunityMaster.numDomainID,OpportunityItems.numOppId,OpportunityItems.numoppitemtCode,2),0)')
	END

	IF ISNULL(@numCurrentPage,0) > 0 
		AND ISNULL(@tintReportType,0) <> 3 
		AND ISNULL(@tintReportType,0) <> 4 
		AND ISNULL(@tintReportType,0) <> 5 
		AND CHARINDEX('Select top',CAST(@textQuery AS VARCHAR(MAX))) <> 1
		AND (SELECT COUNT(*) FROM ReportSummaryGroupList WHERE numReportID=@numReportID) = 0
	BEGIN
		SET @textQuery = CONCAT(CAST(@textQuery AS VARCHAR(MAX)),' OFFSET ',(@numCurrentPage-1) * 100 ,' ROWS FETCH NEXT 100 ROWS ONLY OPTION (OPTIMIZE FOR (@numDomainID UNKNOWN));')
	END
	ELSE
	BEGIN
		SET @textQuery = CONCAT(CAST(@textQuery AS VARCHAR(MAX)),' OPTION (OPTIMIZE FOR (@numDomainID UNKNOWN));')
	END
	 
	 print CAST(@textQuery AS ntext)
	EXECUTE sp_executesql @textQuery,N'@numDomainID numeric(18, 0),@numUserCntID numeric(18,0),@ClientTimeZoneOffset int',@numDomainID=@numDomainID,@numUserCntID=@numUserCntID,@ClientTimeZoneOffset=@ClientTimeZoneOffset
END
/****** Object:  StoredProcedure [dbo].[USP_UpdatedomainDetails]    Script Date: 05/07/2009 21:56:59 ******/
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatedomaindetails')
DROP PROCEDURE usp_updatedomaindetails
GO
CREATE PROCEDURE [dbo].[USP_UpdatedomainDetails]                                      
@numDomainID as numeric(9)=0,                                      
@vcDomainName as varchar(50)='',                                      
@vcDomainDesc as varchar(100)='',                                      
@bitExchangeIntegration as bit,                                      
@bitAccessExchange as bit,                                      
@vcExchUserName as varchar(100)='',                                      
@vcExchPassword as varchar(100)='',                                      
@vcExchPath as varchar(200)='',                                      
@vcExchDomain as varchar(100)='',                                    
@tintCustomPagingRows as tinyint,                                    
@vcDateFormat as varchar(30)='',                                    
@numDefCountry as numeric(9),                                    
@tintComposeWindow as tinyint,                                    
@sintStartDate as smallint,                                    
@sintNoofDaysInterval as smallint,                                    
@tintAssignToCriteria as tinyint,                                    
@bitIntmedPage as bit,                                    
@tintFiscalStartMonth as tinyint,                                      
@tintPayPeriod as tinyint,                        
@numCurrencyID as numeric(9) ,                
@charUnitSystem as char(1)='',              
@vcPortalLogo as varchar(100)='',            
@tintChrForComSearch as tinyint  ,            
@intPaymentGateWay as int, -- DO NOT UPDATE VALUE FROM THIS PARAMETER
@tintChrForItemSearch as tinyint,
@ShipCompany as numeric(9),
@bitMultiCurrency as bit,
@bitCreateInvoice AS tinyint = null,
@numDefaultSalesBizDocID AS NUMERIC(9)=NULL,
@numDefaultPurchaseBizDocID AS NUMERIC(9)=NULL,
@bitSaveCreditCardInfo AS BIT = NULL,
@intLastViewedRecord AS INT=10,
@tintCommissionType AS TINYINT,
@tintBaseTax AS TINYINT,
@bitEmbeddedCost AS BIT,
@numDefaultSalesOppBizDocId NUMERIC=NULL,
@tintSessionTimeOut TINYINT=1,
@tintDecimalPoints TINYINT,
@bitCustomizePortal BIT=0,
@tintBillToForPO TINYINT,
@tintShipToForPO TINYINT,
@tintOrganizationSearchCriteria TINYINT,
@bitDocumentRepositary BIT=0,
@tintComAppliesTo TINYINT=0,
@bitGtoBContact BIT=0,
@bitBtoGContact BIT=0,
@bitGtoBCalendar BIT=0,
@bitBtoGCalendar BIT=0,
@bitExpenseNonInventoryItem BIT=0,
@bitInlineEdit BIT=0,
@bitRemoveVendorPOValidation BIT=0,
@tintBaseTaxOnArea tinyint=0,
@bitAmountPastDue BIT=0,
@monAmountPastDue DECIMAL(20,5),
@bitSearchOrderCustomerHistory BIT=0,
@tintCalendarTimeFormat as tinyint=NULL,
@tintDefaultClassType as tinyint=NULL,
@tintPriceBookDiscount as tinyint=0,
@bitAutoSerialNoAssign as bit=0,
@bitIsShowBalance AS BIT = 0,
@numIncomeAccID NUMERIC(18, 0) = 0,
@numCOGSAccID NUMERIC(18, 0) = 0,
@numAssetAccID NUMERIC(18, 0) = 0,
@IsEnableProjectTracking AS BIT = 0,
@IsEnableCampaignTracking AS BIT = 0,
@IsEnableResourceScheduling AS BIT = 0,
@ShippingServiceItem  AS NUMERIC(9)=0,
@numSOBizDocStatus AS NUMERIC(9)=0,
@bitAutolinkUnappliedPayment AS BIT=0,
@numDiscountServiceItemID AS NUMERIC(9)=0,
@vcShipToPhoneNumber as VARCHAR(50)='',
@vcGAUserEmailId as VARCHAR(50)='',
@vcGAUserPassword as VARCHAR(50)='',
@vcGAUserProfileId as VARCHAR(50)='',
@bitDiscountOnUnitPrice AS BIT=0,
@intShippingImageWidth INT = 0,
@intShippingImageHeight INT = 0,
@numTotalInsuredValue NUMERIC(10,2) = 0,
@numTotalCustomsValue NUMERIC(10,2) = 0,
@vcHideTabs VARCHAR(1000) = '',
@bitUseBizdocAmount BIT = 0,
@bitDefaultRateType BIT = 0,
@bitIncludeTaxAndShippingInCommission  BIT = 0,
@bitPurchaseTaxCredit BIT=0,
@bitLandedCost BIT=0,
@vcLanedCostDefault VARCHAR(10)='',
@bitMinUnitPriceRule BIT = 0,
@numAbovePercent AS NUMERIC(18,2),
@numBelowPercent AS NUMERIC(18,2),
@numBelowPriceField AS NUMERIC(18,0),
@vcUnitPriceApprover AS VARCHAR(100),
@numDefaultSalesPricing AS TINYINT,
@bitReOrderPoint AS BIT = 0,
@numReOrderPointOrderStatus AS NUMERIC(18,0) = 0,
@numPODropShipBizDoc AS NUMERIC(18,0) = 0,
@numPODropShipBizDocTemplate AS NUMERIC(18,0) = 0,
@IsEnableDeferredIncome AS BIT = 0,
@bitApprovalforTImeExpense AS BIT=0,
@numCost AS Numeric(9,0)=0,
@intTimeExpApprovalProcess int=0,
@bitApprovalforOpportunity AS BIT=0,
@intOpportunityApprovalProcess int=0,
@numDefaultSalesShippingDoc NUMERIC(18,0)=0,
@numDefaultPurchaseShippingDoc NUMERIC(18,0)=0,
@bitchkOverRideAssignto AS BIT=0,
@vcPrinterIPAddress VARCHAR(15) = '',
@vcPrinterPort VARCHAR(5) = '',
@numDefaultSiteID NUMERIC(18,0) = 0,
@bitRemoveGlobalLocation BIT = 0,
@numAuthorizePercentage NUMERIC(18,0)=0,
@bitEDI BIT = 0,
@bit3PL BIT = 0,
@bitAllowDuplicateLineItems BIT = 0,
@tintCommitAllocation TINYINT = 1,
@tintInvoicing TINYINT = 1,
@tintMarkupDiscountOption TINYINT = 1,
@tintMarkupDiscountValue TINYINT = 1,
@bitCommissionBasedOn BIT = 0,
@tintCommissionBasedOn TINYINT = 1,
@bitDoNotShowDropshipPOWindow BIT = 0,
@tintReceivePaymentTo TINYINT = 2,
@numReceivePaymentBankAccount NUMERIC(18,0) = 0,
@numOverheadServiceItemID NUMERIC(18,0) = 0,
@bitDisplayCustomField BIT = 0,
@bitFollowupAnytime BIT = 0,
@bitpartycalendarTitle BIT = 0,
@bitpartycalendarLocation BIT = 0,
@bitpartycalendarDescription BIT = 0,
@bitREQPOApproval BIT = 0,
@bitARInvoiceDue BIT = 0,
@bitAPBillsDue BIT = 0,
@bitItemsToPickPackShip BIT = 0,
@bitItemsToInvoice BIT = 0,
@bitSalesOrderToClose BIT = 0,
@bitItemsToPutAway BIT = 0,
@bitItemsToBill BIT = 0,
@bitPosToClose BIT = 0,
@bitPOToClose BIT = 0,
@bitBOMSToPick BIT = 0,
@vchREQPOApprovalEmp  VARCHAR(500)='',
@vchARInvoiceDue VARCHAR(500)='',
@vchAPBillsDue VARCHAR(500)='',
@vchItemsToPickPackShip VARCHAR(500)='',
@vchItemsToInvoice VARCHAR(500)='',
@vchPriceMarginApproval VARCHAR(500)='',
@vchSalesOrderToClose VARCHAR(500)='',
@vchItemsToPutAway VARCHAR(500)='',
@vchItemsToBill VARCHAR(500)='',
@vchPosToClose VARCHAR(500)='',
@vchPOToClose VARCHAR(500)='',
@vchBOMSToPick VARCHAR(500)='',
@decReqPOMinValue VARCHAR(500)='',
@bitUseOnlyActionItems BIT = 0,
@bitDisplayContractElement BIT,
@vcEmployeeForContractTimeElement VARCHAR(500)='',
@numARContactPosition NUMERIC(18,0) = 0,
@bitShowCardConnectLink BIT = 0,
@vcBluePayFormName VARCHAR(500)='',
@vcBluePaySuccessURL VARCHAR(500)='',
@vcBluePayDeclineURL VARCHAR(500)='',
@bitUseDeluxeCheckStock BIT = 1,
@bitEnableSmartyStreets BIT=0,
@vcSmartyStreetsAPIKeys VARCHAR(500)='',
@bitUsePreviousEmailBizDoc BIT = 0,
@bitInventoryInvoicing BIT=0
as                                      
BEGIN
BEGIN TRY
	IF ISNULL(@bitMinUnitPriceRule,0) = 1 AND ISNULL(@intOpportunityApprovalProcess,0) = 0
	BEGIN
		RAISERROR('INCOMPLETE_MIN_UNIT_PRICE_CONFIGURATION',16,1)
		RETURN
	END

	If ISNULL(@bitApprovalforTImeExpense,0) = 1 AND (SELECT
														COUNT(*) 
													FROM
														UserMaster
													LEFT JOIN
														ApprovalConfig
													ON
														ApprovalConfig.numUserId = UserMaster.numUserDetailId
													WHERE
														UserMaster.numDomainID=@numDomainID
														AND numModule=1
														AND ISNULL(numGroupID,0) > 0
														AND ISNULL(bitActivateFlag,0) = 1
														AND ISNULL(numLevel1Authority,0) = 0
														AND ISNULL(numLevel2Authority,0) = 0
														AND ISNULL(numLevel3Authority,0) = 0
														AND ISNULL(numLevel4Authority,0) = 0
														AND ISNULL(numLevel5Authority,0) = 0) > 0
	BEGIN
		RAISERROR('INCOMPLETE_TIMEEXPENSE_APPROVAL',16,1)
		RETURN
	END

	DECLARE @tintCommitAllocationOld AS TINYINT
	SELECT @tintCommitAllocationOld=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainID=@numDomainID

	IF @tintCommitAllocation <> @tintCommitAllocationOld AND (SELECT COUNT(*) FROM OpportunityMaster WHERE numDomainId=@numDomainID AND tintOppType=1 AND tintOppStatus=1 AND ISNULL(tintshipped,0)=0) > 0
	BEGIN
		RAISERROR('NOT_ALLOWED_CHANGE_COMMIT_ALLOCATION_OPEN_SALES_ORDER',16,1)
		RETURN
	END
                                  
	UPDATE 
		Domain                                       
	SET                                       
		vcDomainName=@vcDomainName,                                      
		vcDomainDesc=@vcDomainDesc,                                      
		bitExchangeIntegration=@bitExchangeIntegration,                                      
		bitAccessExchange=@bitAccessExchange,                                      
		vcExchUserName=@vcExchUserName,                                      
		vcExchPassword=@vcExchPassword,                                      
		vcExchPath=@vcExchPath,                                      
		vcExchDomain=@vcExchDomain,                                    
		tintCustomPagingRows =@tintCustomPagingRows,                                    
		vcDateFormat=@vcDateFormat,                                    
		numDefCountry =@numDefCountry,                                    
		tintComposeWindow=@tintComposeWindow,                                    
		sintStartDate =@sintStartDate,                                    
		sintNoofDaysInterval=@sintNoofDaysInterval,                                    
		tintAssignToCriteria=@tintAssignToCriteria,                                    
		bitIntmedPage=@bitIntmedPage,                                    
		tintFiscalStartMonth=@tintFiscalStartMonth,                                                      
		tintPayPeriod=@tintPayPeriod,
		vcCurrency=isnull((select varCurrSymbol from Currency Where numCurrencyID=@numCurrencyID  ),'')  ,                      
		numCurrencyID=@numCurrencyID,                
		charUnitSystem= @charUnitSystem,              
		vcPortalLogo=@vcPortalLogo ,            
		tintChrForComSearch=@tintChrForComSearch  ,      
		tintChrForItemSearch=@tintChrForItemSearch,
		numShipCompany= @ShipCompany,
		bitMultiCurrency=@bitMultiCurrency,
		bitCreateInvoice= @bitCreateInvoice,
		[numDefaultSalesBizDocId] =@numDefaultSalesBizDocId,
		bitSaveCreditCardInfo=@bitSaveCreditCardInfo,
		intLastViewedRecord=@intLastViewedRecord,
		[tintCommissionType]=@tintCommissionType,
		[tintBaseTax]=@tintBaseTax,
		[numDefaultPurchaseBizDocId]=@numDefaultPurchaseBizDocID,
		bitEmbeddedCost=@bitEmbeddedCost,
		numDefaultSalesOppBizDocId=@numDefaultSalesOppBizDocId,
		tintSessionTimeOut=@tintSessionTimeOut,
		tintDecimalPoints=@tintDecimalPoints,
		bitCustomizePortal=@bitCustomizePortal,
		tintBillToForPO = @tintBillToForPO,
		tintShipToForPO = @tintShipToForPO,
		tintOrganizationSearchCriteria=@tintOrganizationSearchCriteria,
		bitDocumentRepositary=@bitDocumentRepositary,
		bitGtoBContact=@bitGtoBContact,
		bitBtoGContact=@bitBtoGContact,
		bitGtoBCalendar=@bitGtoBCalendar,
		bitBtoGCalendar=@bitBtoGCalendar,
		bitExpenseNonInventoryItem=@bitExpenseNonInventoryItem,
		bitInlineEdit=@bitInlineEdit,
		bitRemoveVendorPOValidation=@bitRemoveVendorPOValidation,
		tintBaseTaxOnArea=@tintBaseTaxOnArea,
		bitAmountPastDue = @bitAmountPastDue,
		monAmountPastDue = @monAmountPastDue,
		bitSearchOrderCustomerHistory=@bitSearchOrderCustomerHistory,
		tintCalendarTimeFormat=@tintCalendarTimeFormat,
		tintDefaultClassType=@tintDefaultClassType,
		tintPriceBookDiscount=@tintPriceBookDiscount,
		bitAutoSerialNoAssign=@bitAutoSerialNoAssign,
		bitIsShowBalance = @bitIsShowBalance,
		numIncomeAccID = @numIncomeAccID,
		numCOGSAccID = @numCOGSAccID,
		numAssetAccID = @numAssetAccID,
		IsEnableProjectTracking = @IsEnableProjectTracking,
		IsEnableCampaignTracking = @IsEnableCampaignTracking,
		IsEnableResourceScheduling = @IsEnableResourceScheduling,
		numShippingServiceItemID = @ShippingServiceItem,
		numSOBizDocStatus=@numSOBizDocStatus,
		bitAutolinkUnappliedPayment=@bitAutolinkUnappliedPayment,
		numDiscountServiceItemID=@numDiscountServiceItemID,
		vcShipToPhoneNo = @vcShipToPhoneNumber,
		vcGAUserEMail = @vcGAUserEmailId,
		vcGAUserPassword = @vcGAUserPassword,
		vcGAUserProfileId = @vcGAUserProfileId,
		bitDiscountOnUnitPrice=@bitDiscountOnUnitPrice,
		intShippingImageWidth = @intShippingImageWidth ,
		intShippingImageHeight = @intShippingImageHeight,
		numTotalInsuredValue = @numTotalInsuredValue,
		numTotalCustomsValue = @numTotalCustomsValue,
		vcHideTabs = @vcHideTabs,
		bitUseBizdocAmount = @bitUseBizdocAmount,
		bitDefaultRateType = @bitDefaultRateType,
		bitIncludeTaxAndShippingInCommission = @bitIncludeTaxAndShippingInCommission,
		bitPurchaseTaxCredit=@bitPurchaseTaxCredit,
		bitLandedCost=@bitLandedCost,
		vcLanedCostDefault=@vcLanedCostDefault,
		bitMinUnitPriceRule = @bitMinUnitPriceRule,
		numDefaultSalesPricing = @numDefaultSalesPricing,
		numPODropShipBizDoc=@numPODropShipBizDoc,
		numPODropShipBizDocTemplate=@numPODropShipBizDocTemplate,
		IsEnableDeferredIncome=@IsEnableDeferredIncome,
		bitApprovalforTImeExpense=@bitApprovalforTImeExpense,
		numCost= @numCost,intTimeExpApprovalProcess=@intTimeExpApprovalProcess,
		bitApprovalforOpportunity=@bitApprovalforOpportunity,intOpportunityApprovalProcess=@intOpportunityApprovalProcess,
		numDefaultPurchaseShippingDoc=@numDefaultPurchaseShippingDoc,numDefaultSalesShippingDoc=@numDefaultSalesShippingDoc,
		bitchkOverRideAssignto=@bitchkOverRideAssignto,
		vcPrinterIPAddress=@vcPrinterIPAddress,
		vcPrinterPort=@vcPrinterPort,
		numDefaultSiteID=ISNULL(@numDefaultSiteID,0),
		bitRemoveGlobalLocation=@bitRemoveGlobalLocation,
		numAuthorizePercentage=@numAuthorizePercentage,
		bitEDI=ISNULL(@bitEDI,0),
		bit3PL=ISNULL(@bit3PL,0),
		bitAllowDuplicateLineItems=ISNULL(@bitAllowDuplicateLineItems,0),
		tintCommitAllocation=ISNULL(@tintCommitAllocation,1),
		tintInvoicing = ISNULL(@tintInvoicing,1),
		tintMarkupDiscountOption = ISNULL(@tintMarkupDiscountOption,1),
		tintMarkupDiscountValue = ISNULL(@tintMarkupDiscountValue,1)
		,bitCommissionBasedOn=ISNULL(@bitCommissionBasedOn,0)
		,tintCommissionBasedOn=ISNULL(@tintCommissionBasedOn,1)
		,bitDoNotShowDropshipPOWindow=ISNULL(@bitDoNotShowDropshipPOWindow,0)
		,tintReceivePaymentTo=ISNULL(@tintReceivePaymentTo,2)
		,numReceivePaymentBankAccount = ISNULL(@numReceivePaymentBankAccount,0)
		,numOverheadServiceItemID=@numOverheadServiceItemID,
		bitDisplayCustomField=@bitDisplayCustomField,
		bitFollowupAnytime=@bitFollowupAnytime,
		bitpartycalendarTitle=@bitpartycalendarTitle,
		bitpartycalendarLocation=@bitpartycalendarLocation,
		bitpartycalendarDescription=@bitpartycalendarDescription,
		bitREQPOApproval=@bitREQPOApproval,
		bitARInvoiceDue=@bitARInvoiceDue,
		bitAPBillsDue=@bitAPBillsDue,
		bitItemsToPickPackShip=@bitItemsToPickPackShip,
		bitItemsToInvoice=@bitItemsToInvoice,
		bitSalesOrderToClose=@bitSalesOrderToClose,
		bitItemsToPutAway=@bitItemsToPutAway,
		bitItemsToBill=@bitItemsToBill,
		bitPosToClose=@bitPosToClose,
		bitPOToClose=@bitPOToClose,
		bitBOMSToPick=@bitBOMSToPick,
		vchREQPOApprovalEmp=@vchREQPOApprovalEmp,
		vchARInvoiceDue=@vchARInvoiceDue,
		vchAPBillsDue=@vchAPBillsDue,
		vchItemsToPickPackShip=@vchItemsToPickPackShip,
		vchItemsToInvoice=@vchItemsToInvoice,
		vchSalesOrderToClose=@vchSalesOrderToClose,
		vchItemsToPutAway=@vchItemsToPutAway,
		vchItemsToBill=@vchItemsToBill,
		vchPosToClose=@vchPosToClose,
		vchPOToClose=@vchPOToClose,
		vchBOMSToPick=@vchBOMSToPick,
		decReqPOMinValue=@decReqPOMinValue,
		bitUseOnlyActionItems = @bitUseOnlyActionItems,
		bitDisplayContractElement = @bitDisplayContractElement,
		vcEmployeeForContractTimeElement=@vcEmployeeForContractTimeElement,
		numARContactPosition= @numARContactPosition,
		bitShowCardConnectLink = @bitShowCardConnectLink,
		vcBluePayFormName=@vcBluePayFormName,
		vcBluePaySuccessURL=@vcBluePaySuccessURL,
		vcBluePayDeclineURL=@vcBluePayDeclineURL,
		bitUseDeluxeCheckStock=@bitUseDeluxeCheckStock,
		bitEnableSmartyStreets = @bitEnableSmartyStreets,
		vcSmartyStreetsAPIKeys = @vcSmartyStreetsAPIKeys,
		bitUsePreviousEmailBizDoc=@bitUsePreviousEmailBizDoc,
		bitInventoryInvoicing=@bitInventoryInvoicing
	WHERE 
		numDomainId=@numDomainID

	DELETE FROM UnitPriceApprover WHERE numDomainID = @numDomainID

	IF(LEN(@vchPriceMarginApproval)>0)
	BEGIN
		INSERT INTO UnitPriceApprover
		(
			numDomainID,
			numUserID
		)
		SELECT @numDomainID,Items FROM Split(@vchPriceMarginApproval,',') WHERE Items<>''
	END
	IF ISNULL(@bitRemoveGlobalLocation,0) = 1
	BEGIN
		DECLARE @TEMPGlobalWarehouse TABLE
		(
			ID INT IDENTITY(1,1)
			,numItemCode NUMERIC(18,0)
			,numWarehouseID NUMERIC(18,0)
			,numWarehouseItemID NUMERIC(28,0)
		)

		INSERT INTO @TEMPGlobalWarehouse
		(
			numItemCode
			,numWarehouseID
			,numWarehouseItemID
		)
		SELECT
			numItemID
			,numWareHouseID
			,numWareHouseItemID
		FROM
			WareHouseItems
		WHERE
			numDomainID=@numDomainID
			AND numWLocationID = -1
			AND ISNULL(numOnHand,0) = 0
			AND ISNULL(numOnOrder,0)=0
			AND ISNULL(numAllocation,0)=0
			AND ISNULL(numBackOrder,0)=0


		DECLARE @i AS INT = 1
		DECLARE @iCount AS INT
		DECLARE @numTempItemCode AS NUMERIC(18,0)
		DECLARE @numTempWareHouseID AS NUMERIC(18,0)
		DECLARE @numTempWareHouseItemID AS NUMERIC(18,0)
		SELECT @iCount=COUNT(*) FROM @TEMPGlobalWarehouse

		WHILE @i <= @iCount
		BEGIN
			SELECT @numTempItemCode=numItemCode,@numTempWareHouseID=numWarehouseID,@numTempWareHouseItemID=numWarehouseItemID FROM @TEMPGlobalWarehouse WHERE ID=@i

			BEGIN TRY
				EXEC USP_AddUpdateWareHouseForItems @numTempItemCode,  
													@numTempWareHouseID,
													@numTempWareHouseItemID,
													'',
													0,
													0,
													0,
													'',
													'',
													@numDomainID,
													'',
													'',
													'',
													0,
													3,
													0,
													0,
													NULL,
													0
			END TRY
			BEGIN CATCH

			END CATCH

			SET @i = @i + 1
		END
	END
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH

END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemDetailsFor850')
DROP PROCEDURE dbo.USP_GetItemDetailsFor850
GO

CREATE PROCEDURE [dbo].[USP_GetItemDetailsFor850]
	@Inbound850PickItem INT
	,@vcItemIdentification VARCHAR(100)
	,@numDivisionId NUMERIC(18,0)
	,@numCompanyId NUMERIC(18,0)
	,@numDomainId NUMERIC(18,0)
	,@numOrderSource NUMERIC(18,0)
AS 
BEGIN

	If EXISTS (SELECT ID FROM ItemMarketplaceMapping WHERE numDomainID=@numDomainId AND numMarketplaceID=@numOrderSource AND ISNULL(vcMarketplaceUniqueID,'')=ISNULL(@vcItemIdentification,''))
	BEGIN
		SELECT
			Item.* 
		FROM 
			ItemMarketplaceMapping 
		INNER JOIN
			Item
		ON
			ItemMarketplaceMapping.numItemCode = Item.numItemCode
		WHERE 
			ItemMarketplaceMapping.numDomainID=@numDomainId 
			AND ItemMarketplaceMapping.numMarketplaceID=@numOrderSource 
			AND ISNULL(ItemMarketplaceMapping.vcMarketplaceUniqueID,'')=ISNULL(@vcItemIdentification,'')
	END
	ELSE
	BEGIN
		IF @Inbound850PickItem = 1 --SKU
		BEGIN
			IF @numDomainID = 209 AND EXISTS (SELECT numItemCode FROM Item WHERE numDomainID = @numDomainId AND vcItemName = @vcItemIdentification)
			BEGIN
				SELECT * FROM Item WHERE numDomainID = @numDomainId AND vcItemName = @vcItemIdentification
			END
			ELSE
			BEGIN
				SELECT * FROM Item WHERE numDomainID = @numDomainId AND vcSKU = @vcItemIdentification
			END
		END
		ELSE IF @Inbound850PickItem = 2  -- UPC
		BEGIN
			SELECT * FROM ITEM WHERE numDomainID = @numDomainId AND numBarCodeId = @vcItemIdentification
		END
		ELSE IF @Inbound850PickItem = 3  -- ItemName
		BEGIN
			SELECT * FROM ITEM WHERE numDomainID = @numDomainId AND vcItemName = @vcItemIdentification
		END
		ELSE IF @Inbound850PickItem = 4   -- BizItemID
		BEGIN
			SELECT * FROM ITEM WHERE numDomainID = @numDomainId AND numItemCode = @vcItemIdentification
		END
		ELSE IF @Inbound850PickItem = 5   -- ASIN
		BEGIN
			SELECT * FROM ITEM WHERE numDomainID = @numDomainId AND vcASIN = @vcItemIdentification
		END
		ELSE IF @Inbound850PickItem = 6  -- CustomerPart#
		BEGIN
			SELECT * FROM ITEM I
			INNER JOIN CustomerPartNumber CPN ON I.numItemCode = CPN.numItemCode
			WHERE CPN.numDomainId = @numDomainId AND CPN.numCompanyId = @numCompanyId AND CPN.CustomerPartNo = @vcItemIdentification
		END
		ELSE IF @Inbound850PickItem = 7   -- VendorPart#
		BEGIN
			SELECT * FROM ITEM I
			INNER JOIN Vendor V ON I.numItemCode = V.numItemCode
			WHERE V.numDomainID = @numDomainId AND V.numVendorID = @numDivisionId AND V.vcPartNo = @vcItemIdentification
		END
	END
END

GO


/****** Object:  StoredProcedure [dbo].[USP_GetInvoiceList]    Script Date: 07/26/2008 16:17:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by prasanta Pradhan                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOrderListInEcommerce')
DROP PROCEDURE USP_GetOrderListInEcommerce
GO
CREATE PROCEDURE [dbo].[USP_GetOrderListInEcommerce]                            
 @numDivisionID as numeric(9)=0 ,
 @tintOppType AS TINYINT=1,
 @numDomainID AS NUMERIC(9),
 @CurrentPage INT,
 @PageSize INT,
 @tintShipped INT=0,
 @tintOppStatus INT=0,
 @UserId NUMERIC(18,0)
as                        
 
 DECLARE @firstRec INT=0
 DECLARE @lastRec INT=0
DECLARE @ClientTimeZoneOffset Int
SET @ClientTimeZoneOffset=-330
IF (@tintOppType = 1 OR  @tintOppType =2 )
BEGIN
	SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
    SET @lastRec = ( @CurrentPage * @PageSize + 1 )
	SELECT  * FROM (
	SELECT ROW_NUMBER() OVER(ORDER BY numOppID) AS Rownumber,* FROM (
	SELECT distinct OM.numOppID,OM.vcPOppName,(SELECT SUBSTRING((SELECT '$^$' + CAST(numOppBizDocsId AS VARCHAR(18)) +'#^#'+ vcBizDocID
										FROM OpportunityBizDocs WHERE numOppId=OM.numOppID AND numBizDOcId=287 FOR XML PATH('')),4,200000)) AS vcBizDoc,

		(SELECT SUBSTRING((SELECT '$^$' + CAST(vcTrackingNo AS VARCHAR(18)) +'#^#'+ vcTrackingUrl
										FROM OpportunityBizDocs WHERE numOppId=OM.numOppID FOR XML PATH('')),4,200000)) AS vcShipping,
		(isnull(dbo.GetDealAmount(OM.numOppID,getdate(),0),0) * OM.fltExchangeRate) as monDealAmount,
		(isnull(dbo.GetPaidAmount(OM.numOppID),0) * OM.fltExchangeRate) AS monAmountPaid,
		(isnull(dbo.GetDealAmount(OM.numOppID,getdate(),0),0) * OM.fltExchangeRate)
		 - (isnull(dbo.GetPaidAmount(OM.numOppID),0) * OM.fltExchangeRate) AS BalanceDue,
		(select  TOP 1 vcData from ListDetails where numListItemID=(SELECT TOP 1 numShipVia FROM OpportunityBizDocs WHERE numOppId=OM.numOppID AND numShipVia>0)) AS vcShippingMethod,
		(select  TOP 1 vcData from ListDetails where numListItemID=(SELECT TOP 1 intUsedShippingCompany FROM OpportunityMaster where numOppId=OM.numOppID)) AS vcMasterShippingMethod,
		(SELECT TOP 1 intUsedShippingCompany FROM OpportunityMaster where numOppId=OM.numOppID) AS numOppMasterShipVia,
		(SELECT TOP 1 numShipVia FROM OpportunityBizDocs WHERE numOppId=OM.numOppID  AND numShipVia>0) AS numShipVia,
		(SELECT TOP 1 vcTrackingNo FROM OpportunityBizDocs WHERE numOppId=OM.numOppID  AND numShipVia>0) AS vcTrackingNo,
		(SELECT TOP 1 numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] = (SELECT TOP 1 numOppBizDocsId FROM OpportunityBizDocs WHERE numOppId=OM.numOppID  AND numShipVia>0)) AS numShippingReportId,
		(SELECT TOP 1 numOppBizDocsId FROM OpportunityBizDocs WHERE numOppId=OM.numOppID  AND numShipVia>0) AS numShippingBizDocId,
		[dbo].[FormatedDateFromDate]((SELECT TOP 1 dtDeliveryDate FROM OpportunityBizDocs WHERE numOppId=OM.numOppID  AND numShipVia>0),OM.numDomainId) AS DeliveryDate,
		[dbo].[FormatedDateFromDate]((SELECT TOP 1 dtFromDate FROM OpportunityBizDocs WHERE numOppId=OM.numOppID order by dtFromDate desc),OM.numDomainId) AS BillingDate,

		CASE WHEN (SELECT COUNT(numBizDocId) FROM OpportunityBizDocs WHERE numOppId=OM.numOppID)=1 THEN
		(CASE ISNULL(OM.bitBillingTerms,0)  
                 WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(OM.intBillingDays,0)), 0)),(SELECT TOP 1 dtFromDate FROM OpportunityBizDocs WHERE numOppId=OM.numOppID order by dtFromDate desc)),OM.numDomainId)
                 WHEN 0 THEN [dbo].[FormatedDateFromDate]((SELECT TOP 1 dtFromDate FROM OpportunityBizDocs WHERE numOppId=OM.numOppID order by dtFromDate desc),OM.numDomainId)
               END) 
		ELSE 'Multiple' END AS DueDate,
		[dbo].[FormatedDateFromDate](OM.dtReleaseDate,OM.numDomainId) AS ReleaseDate,
		[dbo].[FormatedDateFromDate](OM.bintCreatedDate,OM.numDomainId) AS CreatedDate,
		(select TOP 1 vcData from ListDetails WHERE numListItemID=OM.numStatus) AS vcOrderStatus,
        dbo.GetCreditTerms(OM.numOppId) as Credit,
		dbo.fn_GetContactName(OM.numCreatedBy)  as CreatedName,
		CONVERT(DATETIME, OM.bintCreatedDate) AS  dtCreatedDate,
		CONVERT(DATETIME, OM.bintCreatedDate) AS  numCreatedDate,
		CASE WHEN 
		(select TOP 1 ISNULL(numPaymentMethod,0) from General_Journal_Details WHERE numJournalId IN (select numJournal_Id from General_Journal_Header where numOppBizDocsId IN(select numBizDocId from OpportunityBizDocs where numOppId=OM.numOppID)))=0
		THEN
		(select TOP 1 vcData from ListDetails WHERE numListItemID IN (
		select ISNULL(numDefaultPaymentMethod,0) from DivisionMaster where numDivisionId=@numDivisionID)) 
		ELSE  (select TOP 1 vcData from ListDetails WHERE numListItemID IN (
			  select numPaymentMethod from General_Journal_Details WHERE numJournalId IN (select numJournal_Id from General_Journal_Header where numOppBizDocsId IN(select numBizDocId from OpportunityBizDocs where numOppId=OM.numOppID))))
		END
		AS PaymentMethod,
		isnull(dbo.GetDealAmount(OM.numOppID,getdate(),0),0) as TotalAmt,
		OM.numContactId,
		ISNULL(OM.tintOppStatus,0) As [tintOppStatus],
		CASE 
WHEN convert(varchar(11),OM.bintCreatedDate)=convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,getutcdate())) 
then CONCAT('<b><font color="#FF0000" style="font-size:14px">Today','</font></b>') 
WHEN convert(varchar(11),OM.bintCreatedDate)=convert(varchar(11),DateAdd(day,1,DateAdd(minute,-@ClientTimeZoneOffset,getutcdate()))) 
THEN CONCAT('<b><font color=#ED8F11 style="font-size:14px">Tommorow','</font></b>') 
WHEN OM.bintCreatedDate<DateAdd(day,7,DateAdd(minute, -@ClientTimeZoneOffset,getutcdate()))
AND OM.bintCreatedDate>DateAdd(day,1,DateAdd(minute, -@ClientTimeZoneOffset,getutcdate()))
then CONCAT('<b><font color=#8FAADC style="font-size:14px">',datename(dw,DateAdd(minute, -@ClientTimeZoneOffset,OM.bintCreatedDate)),'<span style="color:#000;font-weight:500"> - ',CAST(DAY(OM.bintCreatedDate) AS VARCHAR(10)),CASE
	WHEN DAY(OM.bintCreatedDate) % 100 IN (11, 12, 13) THEN 'th'
	WHEN DAY(OM.bintCreatedDate) % 10 = 1 THEN 'st'
	WHEN DAY(OM.bintCreatedDate) % 10 = 2 THEN 'nd'
	WHEN DAY(OM.bintCreatedDate) % 10 = 3 THEN 'rd'
ELSE 'th' END,'</span>','</font></b>','<b>','<br/>'
,'<Span style="font-size:14px;font-weight:normal;font-style: italic;">'
,'</span>') ELSE 

CONCAT('<b><font color=#8FAADC style="font-size:14px">',datename(dw,DateAdd(minute, -@ClientTimeZoneOffset,OM.bintCreatedDate)),'<span style="color:#000;font-weight:500"> - ',CAST(DAY(OM.bintCreatedDate) AS VARCHAR(10)),CASE
	WHEN DAY(OM.bintCreatedDate) % 100 IN (11, 12, 13) THEN 'th'
	WHEN DAY(OM.bintCreatedDate) % 10 = 1 THEN 'st'
	WHEN DAY(OM.bintCreatedDate) % 10 = 2 THEN 'nd'
	WHEN DAY(OM.bintCreatedDate) % 10 = 3 THEN 'rd'
ELSE 'th' END,'</span>','</font></b>','<b>','<br/>'
,'<Span style="font-size:14px;font-weight:normal;font-style: italic;">'
,'</span>')
END AS FormattedCreatedDate




		FROM dbo.OpportunityMaster OM 
		LEFT JOIN dbo.OpportunityBizDocs OBD ON OM.numoppID=OBD.numoppID
		LEFT JOIN dbo.DivisionMaster D ON D.numDivisionID = OM.numDivisionId
		LEFT JOIN dbo.CompanyInfo C ON C.numCompanyId = D.numCompanyID
		WHERE OM.numDomainId=@numDomainID AND OM.tintOppType= @tintOppType
		AND D.numDivisionID= @numDivisionID 
		--AND ISNULL(bitAuthoritativeBizDocs,0)=1
		AND OM.tintOppStatus=@tintOppStatus  AND OM.numContactId=@UserId
		AND 1=(CASE WHEN @tintOppStatus=1 AND OM.tintShipped=@tintShipped THEN 1 WHEN @tintOppStatus=0 THEN 1 ELSE 0 END)
		--AND ISNULL(OBD.tintDeferred,0) <> 1 AND  isnull(OBD.monDealAmount * OM.fltExchangeRate,0) > 0 
		--AND isnull(OBD.monDealAmount * OM.fltExchangeRate - OBD.[monAmountPaid] * OM.fltExchangeRate,0) > 0
		) AS K) AS T
		WHERE T.Rownumber>@firstRec AND T.Rownumber<@lastRec

		SELECT DISTINCT COUNT(OM.numOppId) FROM dbo.OpportunityMaster OM 
		LEFT JOIN dbo.DivisionMaster D ON D.numDivisionID = OM.numDivisionId
		LEFT JOIN dbo.CompanyInfo C ON C.numCompanyId = D.numCompanyID
		WHERE OM.numDomainId=@numDomainID AND OM.tintOppType= @tintOppType
		AND D.numDivisionID= @numDivisionID 
		--AND ISNULL(bitAuthoritativeBizDocs,0)=1
		AND OM.tintOppStatus=@tintOppStatus  AND OM.numContactId=@UserId
		AND 1=(CASE WHEN @tintOppStatus=1 AND OM.tintShipped=@tintShipped THEN 1 WHEN @tintOppStatus=0 THEN 1 ELSE 0 END)
		--AND ISNULL(OBD.tintDeferred,0) <> 1 AND  isnull(OBD.monDealAmount * OM.fltExchangeRate,0) > 0 
		--AND isnull(OBD.monDealAmount * OM.fltExchangeRate - OBD.[monAmountPaid] * OM.fltExchangeRate,0) > 0


		----Create a Temporary table to hold data                        
--Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                         
--      numOppID varchar(15),vcPOppName varchar(100),                  
--      vcBizDocID varchar(100),                
--   BizDocName varchar(100),              
--   CreatedName varchar(100),                       
--      Credit VARCHAR(100),                        
--      numCreatedDate datetime,                        
-- monAmountPaid DECIMAL(20,5),    
-- TotalAmt DECIMAL(20,5),
-- numContactId NUMERIC(9),
-- tintOppStatus TINYINT)                        
--                  
--                  
--declare @strSql as varchar(8000)                        
--set @strSql=
--'SELECT 
--O.numOppID,
--O.vcPOppName,               
--CASE WHEN O.tintOppType = 1 AND O.tintOppStatus = 0 THEN ''Sales Opportunity''
--	 WHEN O.tintOppType = 1 AND O.tintOppStatus = 1 THEN ''Sales Order''
--END as BizDocName,              
--dbo.fn_GetContactName(O.numCreatedBy)  as CreatedName,     
--dbo.GetCreditTerms(O.numOppId) as Credit,                  
--CONVERT(DATETIME, O.bintCreatedDate) AS  dtCreatedDate,
--isnull(O.numPClosingPercent,0)as monAmountPaid,
--isnull(dbo.GetDealAmount(O.numOppID,getdate(),0),0) as TotalAmt,
--O.numContactId,ISNULL(O.tintOppStatus,0) As [tintOppStatus]
--FROM dbo.OpportunityMaster O 
--     JOIN dbo.DivisionMaster D 
--        ON D.numDivisionID = O.numDivisionId
--     JOIN dbo.CompanyInfo C 
--        ON C.numCompanyId = D.numCompanyID
--WHERE O.numDomainId = D.numDomainID '
--      
--set @strSql=@strSql + ' and O.tintOppType= ' + convert(varchar(3),@tintOppType)
--
--if @numDivisionID<>0 set @strSql=@strSql + ' and D.numDivisionID= ' + convert(varchar(15),@numDivisionID)                        
--if @SortChar<>'0' set @strSql=@strSql + ' And vcBizDocID like '''+@SortChar+'%'''   
--IF @bitflag =0
--BEGIN
--	SET @strSql = @strSql + ' And O.tintShipped  =1'                                            
--END 
--ELSE IF @bitflag=1
--BEGIN
--SET @strSql = @strSql + ' And (O.tintOppStatus = 1 OR O.tintOppStatus = 0)'
--	
--END
--
--set @strSql=@strSql +' ORDER BY ' + @columnName +' '+ @columnSortOrder                     
--print @strSql                      
--insert into #tempTable(                        
--      numOppID,                  
-- vcBizDocID,                
-- BizDocName,              
--  CreatedName,                        
--      Credit,                        
--      numCreatedDate,                        
-- monAmountPaid,
-- TotalAmt,
-- numContactId,tintOppStatus)                        
--exec (@strSql)                         
--                        
--  declare @firstRec as integer                         
--  declare @lastRec as integer                        
-- set @firstRec= (@CurrentPage-1) * @PageSize                        
--     set @lastRec= (@CurrentPage*@PageSize+1)                        
--select *,(TotalAmt-monAmountPaid) AS BalanceDue,
--CASE WHEN @tintOppType = 1 AND tintOppStatus = 0 THEN 'Sales Opportunity' 
--	 WHEN @tintOppType = 1 AND tintOppStatus = 1 THEN 'Sales Order' 
--     WHEN @tintOppType = 2 THEN 'Purchase Order' END AS [Type]
-- from #tempTable where ID > @firstRec and ID < @lastRec                        
--set @TotRecs=(select count(*) from #tempTable)                        
--drop table #tempTable
END 
--For bills
IF @tintOppType =  3 
BEGIN
-- SELECT 'Bill' AS [Type],OBD.vcMemo,OBD.vcReference,OBD.monAmount,dbo.FormatedDateFromDate(PD.dtDueDate,OBD.numDomainId) DueDate
-- FROM   dbo.OpportunityBizDocsDetails OBD
--        INNER JOIN dbo.OpportunityBizDocsPaymentDetails PD ON PD.numBizDocsPaymentDetId = OBD.numBizDocsPaymentDetId
--  WHERE OBD.numDivisionID = @numDivisionID AND PD.bitIntegrated=0

SELECT 0 AS numOppId,'Bill' vcPOppName,0 AS numOppBizDocsId,'Bill' + CASE WHEN len(BH.vcReference)=0 THEN '' ELSE '-' + BH.vcReference END AS [vcBizDocID],
						   BH.monAmountDue as monDealAmount,
						   ISNULL(BH.monAmtPaid, 0) as monAmountPaid,
					       ISNULL(BH.monAmountDue, 0) - ISNULL(BH.monAmtPaid, 0) as BalanceDue,
					       [dbo].[FormatedDateFromDate](BH.dtBillDate,BH.numDomainID) AS BillingDate,
						   [dbo].[FormatedDateFromDate](BH.dtDueDate,BH.numDomainID) AS DueDate
						   FROM    
							BillHeader BH 
							WHERE BH.numDomainId=@numDomainID AND BH.numDivisionId = @numDivisionID
							AND ISNULL(BH.monAmountDue, 0) - ISNULL(BH.monAmtPaid, 0) > 0
							
END



GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TicklerActItemsV2')
DROP PROCEDURE USP_TicklerActItemsV2
GO
CREATE Proc [dbo].[USP_TicklerActItemsV2]       
@numUserCntID as numeric(9)=null,                                                                        
@numDomainID as numeric(9)=null,                                                                        
@startDate as datetime,                                                                        
@endDate as datetime,                                                                    
@ClientTimeZoneOffset Int,  --Added by Debasish to enable calculation of date according to client machine             
@bitFilterRecord bit=0,
@columnName varchar(50), 
@columnSortOrder varchar(50),
@vcBProcessValue varchar(500),
@RegularSearchCriteria VARCHAR(MAX)='',
@CustomSearchCriteria VARCHAR(MAX)='', 
@OppStatus int,
@CurrentPage int,                                                              
@PageSize int
AS
BEGIN
	SET @endDate = DATEADD(MILLISECOND,-2,DATEADD(DAY,1,@endDate))

--SET @PageSize=10
DECLARE @tintActionItemsViewRights TINYINT  = 3
DECLARE @bitREQPOApproval AS BIT
DECLARE @bitARInvoiceDue AS BIT
DECLARE @bitAPBillsDue AS BIT
DECLARE @vchARInvoiceDue AS VARCHAR(500)
DECLARE @vchAPBillsDue AS VARCHAR(500)
DECLARE @decReqPOMinValue AS DECIMAL(18,2)
DECLARE @vchREQPOApprovalEmp AS VARCHAR(500)=''
DECLARE @numDomainDivisionID NUMERIC(18,0)
DECLARE @dynamicPartQuery AS NVARCHAR(MAX)=''
DECLARE @ActivityRegularSearch AS VARCHAR(MAX)=''
DECLARE @OtherRegularSearch AS VARCHAR(MAX)=''
DECLARE @IndivRecordPaging AS NVARCHAR(MAX)=''
--SET @IndivRecordPaging=' '
DECLARE @TotalRecordCount INT =0
SET @IndivRecordPaging=  ' ORDER BY dtDueDate DESC OFFSET '+CAST(((@CurrentPage - 1 ) * @PageSize) AS VARCHAR) +' ROWS FETCH NEXT '+CAST(@PageSize AS VARCHAR)+' ROWS ONLY'

SELECT TOP 1 
	@bitREQPOApproval=bitREQPOApproval,
	@vchREQPOApprovalEmp=vchREQPOApprovalEmp,
	@decReqPOMinValue=ISNULL(decReqPOMinValue,0),
	@bitARInvoiceDue=bitARInvoiceDue,
	@bitAPBillsDue = bitAPBillsDue,
	@vchARInvoiceDue = vchARInvoiceDue,
	@vchAPBillsDue = vchAPBillsDue,
	@numDomainDivisionID=numDivisionID
FROM 
	Domain 
WHERE 
	numDomainId=@numDomainID

IF EXISTS (SELECT * FROM GroupAuthorization WHERE numDomainID=@numDomainID AND numModuleID=1 AND numPageID=1 AND numGroupID IN (SELECT ISNULL(numGroupID,0) FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailId=@numUserCntID))
BEGIN
	SELECT @tintActionItemsViewRights=ISNULL(intViewAllowed,0) FROM GroupAuthorization WHERE numDomainID=@numDomainID AND numModuleID=1 AND numPageID=1 AND numGroupID IN (SELECT ISNULL(numGroupID,0) FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailId=@numUserCntID)
END
--12 - Communication task
--1 - Case Task
--2 - Project Task
--3 - Opportunity Task
--4 - Sales Order Task
--5 - Work Order Management
--6 - Work Center Task
--7 - Requisition Approval
--8 - PO Approval
--9 - Price Margin Approval
--10 - A/R Invoice Due
--11 - A/P Bill Due

DECLARE @dynamicQuery AS NVARCHAR(MAX)=''
DECLARE @dynamicQueryCount AS NVARCHAR(MAX)=''

Create table #tempRecords (RecordCount NUMERIC(9),RecordId NUMERIC(9),numTaskId numeric(9),IsTaskClosed bit,TaskTypeName VARCHAR(MAX),TaskType NUMERIC(9),
OrigDescription NVARCHAR(MAX),Description NVARCHAR(MAX),TotalProgress INT,numAssignToId NUMERIC(9),
Priority NVARCHAR(MAX),PriorityId  NUMERIC(9),ActivityId NUMERIC(9),OrgName NVARCHAR(MAX),CompanyRating NVARCHAR(MAX),
numContactID numeric(9),numDivisionID numeric(9),tintCRMType tinyint,
Id numeric(9),bitTask varchar(100),Startdate datetime,EndTime datetime,
itemDesc text,[Name] varchar(200),vcFirstName varchar(100),vcLastName varchar(100),numPhone varchar(15),numPhoneExtension varchar(7),
[Phone] varchar(30),vcCompanyName varchar(100),vcProfile VARCHAR(100), vcEmail varchar(50),Task varchar(100),Activity varchar(MAX),
Status varchar(100),numCreatedBy numeric(9),numRecOwner VARCHAR(500), numModifiedBy VARCHAR(500), numOrgTerId NUMERIC(18,0), numTerId VARCHAR(200),numAssignedTo varchar(1000),numAssignedBy varchar(50),
 caseid numeric(9),vcCasenumber varchar(50),casetimeId numeric(9),caseExpId numeric(9),type int,itemid varchar(50),
changekey varchar(50),dtDueDate DATETIME, DueDate varchar(500),bitFollowUpAnyTime bit,numNoOfEmployeesId varchar(50),vcComPhone varchar(50),numFollowUpStatus varchar(200),dtLastFollowUp varchar(500),vcLastSalesOrderDate VARCHAR(100),monDealAmount DECIMAL(20,5), vcPerformance VARCHAR(100),vcColorScheme VARCHAR(50),
Slp_Name varchar(100),intTotalProgress INT,vcPoppName varchar(100), numOppId NUMERIC(18,0),numBusinessProcessID numeric(18,0),
numCompanyRating varchar(500),numStatusID varchar(500),numCampaignID varchar(500),[Action-Item Participants] NVARCHAR(MAX),HtmlLink varchar(500),
Location nvarchar(150),OrignalDescription text,numTaskEstimationInMinutes INT,numTimeSpentInMinutes INT,dtLastStartDate DATETIME,numRemainingQty FLOAT)                                                         
 

declare @strSql as varchar(MAX)                                                            
declare @strSql1 as varchar(MAX)                         
declare @strSql2 as varchar(MAX)                                                            
declare @vcCustomColumnName AS VARCHAR(8000)
declare @vcnullCustomColumnName AS VARCHAR(8000)
   -------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

declare @tintOrder as tinyint                                                
declare @vcFieldName as varchar(50)                                                
declare @vcListItemType as varchar(3)                                           
declare @vcAssociatedControlType varchar(20)                                                
declare @numListID AS numeric(9)                                                
declare @vcDbColumnName varchar(20)                    
declare @WhereCondition varchar(2000)                     
declare @vcLookBackTableName varchar(2000)              
Declare @bitCustomField as BIT
DECLARE @bitAllowEdit AS CHAR(1)                 
Declare @numFieldId as numeric  
DECLARE @bitAllowSorting AS CHAR(1)   
declare @vcColumnName AS VARCHAR(500)       
declare @vcCustomColumnNameOnly AS VARCHAR(500)       


IF(@RegularSearchCriteria='')
BEGIN
	SET @RegularSearchCriteria =@RegularSearchCriteria + ' 1=1'
END
if(@OppStatus=0)
BEGIN
SET @RegularSearchCriteria =@RegularSearchCriteria + ' AND IsTaskClosed = 0'
END
if(@OppStatus=1)
BEGIN
SET @RegularSearchCriteria = @RegularSearchCriteria + ' AND IsTaskClosed = 1'
END
 SET @RegularSearchCriteria  = REPLACE(@RegularSearchCriteria,'TaskTypeName','TaskType')

 SET @ActivityRegularSearch = ''
 SET @OtherRegularSearch = ''

 SET @RegularSearchCriteria = REPLACE(@RegularSearchCriteria,'DueDate','dtDueDate')

 SET @ActivityRegularSearch = ISNULL((SELECT STUFF( 
(
	SELECT  'AND ' + [Items] FROM 
	(
	  SELECT [Items] FROM SplitByString(@RegularSearchCriteria,'AND') WHERE Items<>'' AND 1=
	  (CASE WHEN Items LIKE '%IsTaskClosed%' THEN 0
	 WHEN Items LIKE '%TaskType%'  THEN 0
	 WHEN  Items LIKE '%numAssignToId%'  THEN 0
	 WHEN  Items LIKE '%PriorityId%'  THEN 0
	 WHEN  Items LIKE '%ActivityId%'  THEN 0
	 WHEN  Items LIKE '%Description%'   THEN 0
	 WHEN Items LIKE '%dtDueDate%' THEN 0
	 WHEN  Items LIKE '%OrgName%' THEN 0 ELSE 1 END)
	) AS T FOR XML PATH('')
)
,1,3,'') AS [Items]),'')
SET @ActivityRegularSearch = REPLACE(@ActivityRegularSearch,'&gt;','>')
SET @ActivityRegularSearch = REPLACE(@ActivityRegularSearch,'&lt;','<')
SET @ActivityRegularSearch = REPLACE(@ActivityRegularSearch,'dtDueDate','DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtStartTime)')
 SET @OtherRegularSearch = ISNULL((SELECT STUFF( 
(
	SELECT  'AND ' + [Items] FROM 
	(
	  SELECT [Items] FROM SplitByString(@RegularSearchCriteria,'AND') WHERE Items<>'' AND 1=
	  (CASE WHEN Items LIKE '%IsTaskClosed%' THEN 1
	 WHEN Items LIKE '%TaskType%'  THEN 1
	 WHEN  Items LIKE '%numAssignToId%'  THEN 1
	 WHEN  Items LIKE '%PriorityId%'  THEN 1
	 WHEN  Items LIKE '%ActivityId%'  THEN 1
	 WHEN  Items LIKE '%Description%'   THEN 1
	 WHEN  Items LIKE '%dtDueDate%'   THEN 1
	 WHEN  Items LIKE '%OrgName%'   THEN 1 ELSE 0 END)
	) AS T FOR XML PATH('')
)
,1,3,'') AS [Items]),'')
SET @OtherRegularSearch = ISNULL(REPLACE(@OtherRegularSearch,'T.',''),'')
SET @OtherRegularSearch = REPLACE(@OtherRegularSearch,'&gt;','>')
SET @OtherRegularSearch = REPLACE(@OtherRegularSearch,'&lt;','<')
IF(LEN(@OtherRegularSearch)=0)
BEGIN
	SET @OtherRegularSearch = ' 1=1 '
END
Declare @ListRelID as numeric(9)             
set @tintOrder=0   

CREATE TABLE #tempForm 
	(
		tintOrder INT,vcDbColumnName nvarchar(50),vcFieldDataType nvarchar(50),vcOrigDbColumnName nvarchar(50),bitAllowFiltering BIT,vcFieldName nvarchar(50)
		,vcAssociatedControlType nvarchar(50),vcListItemType char(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC
		,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int
		,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth NUMERIC(18,2)
	)
                
	--Custom Fields related to Organization
	INSERT INTO 
		#tempForm
	SELECT  
		tintRow + 1 AS tintOrder,vcDbColumnName,'',vcDbColumnName,1,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'' as vcLookBackTableName                                                 
		,bitCustom,numFieldId,bitAllowSorting,0,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength
		,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
	FROM 
		View_DynamicCustomColumns
	WHERE 
		numFormId=43 
		AND numUserCntID=@numUserCntID 
		AND numDomainID=@numDomainID 
		AND tintPageType=1
		AND ISNULL(bitCustom,0)=1
 
	SELECT TOP 1 
		@tintOrder = tintOrder + 1
		,@vcDbColumnName=vcDbColumnName
		,@vcFieldName=vcFieldName
		,@vcAssociatedControlType=vcAssociatedControlType
		,@vcListItemType=vcListItemType
		,@numListID=numListID
		,@vcLookBackTableName=vcLookBackTableName                                               
		,@bitCustomField=bitCustomField
		,@numFieldId=numFieldId
		,@bitAllowSorting=bitAllowSorting
		,@bitAllowEdit=0
		,@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder ASC     
  
	SET @vcCustomColumnName=''
	SET @vcnullCustomColumnName=''
	SET @vcCustomColumnNameOnly = ''
	WHILE @tintOrder>0      
	BEGIN  
		SET @vcColumnName= @vcDbColumnName
		SET @strSql = 'ALTER TABLE #tempRecords ADD [' + @vcColumnName + '] NVARCHAR(MAX)'
		SET @vcCustomColumnNameOnly = @vcCustomColumnNameOnly+','+@vcColumnName
		EXEC(@strSql)
		
		IF @bitCustomField = 1
		BEGIN 
			IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea'            
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+', ISNULL((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),'''') ['+ @vcColumnName +']'
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+','''' AS  ['+ @vcColumnName +']'
			END  
			ELSE IF @vcAssociatedControlType = 'CheckBox'         
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+',ISNULL((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),0)  ['+ @vcColumnName + ']'
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+',0 AS ['+ @vcColumnName + ']'
			END            
			ELSE IF @vcAssociatedControlType = 'DateField'        
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+',dbo.FormatedDateFromDate((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'              
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+','''' AS  ['+ @vcColumnName +']'
			END            
			ELSE IF @vcAssociatedControlType = 'SelectBox'           
			BEGIN            
				SET @vcCustomColumnName=@vcCustomColumnName+',(SELECT TOP 1 ISNULL(L.vcData,'''') FROM CFW_FLD_Values CFW left Join ListDetails L on L.numListItemID=CFW.Fld_Value WHERE  CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID)'+' ['+ @vcColumnName +']'                                                         
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']'                                                         
			END
			ELSE IF @vcAssociatedControlType = 'CheckBoxList'
			BEGIN
				SET @vcCustomColumnName=@vcCustomColumnName+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(ISNULL((SELECT CFWInner.Fld_Value FROM CFW_FLD_Values CFWInner WHERE CFWInner.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFWInner.RecId=Div.numDivisionID),''''),'','')) FOR XML PATH('''')), 1, 1, ''''))'+' ['+ @vcColumnName +']'
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']'
			END
			ELSE
			BEGIN
				SET @vcCustomColumnName= CONCAT(@vcCustomColumnName,',(SELECT dbo.fn_GetCustFldStringValue(',@numFieldId,',CFW.RecId,CFW.Fld_Value',') FROM CFW_FLD_Values CFW left Join ListDetails L on L.numListItemID=CFW.Fld_Value WHERE  CFW.Fld_ID=',@numFieldId,' AND CFW.RecId=Div.numDivisionID)',' [', @vcColumnName ,']')
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']' 
			END
		END
		  
		SELECT TOP 1 
			@tintOrder=tintOrder + 1
			,@vcDbColumnName=vcDbColumnName
			,@vcFieldName=vcFieldName
			,@vcAssociatedControlType=vcAssociatedControlType
			,@vcListItemType=vcListItemType
			,@numListID=numListID
			,@vcLookBackTableName=vcLookBackTableName                                               
			,@bitCustomField=bitCustomField
			,@numFieldId=numFieldId
			,@bitAllowSorting=bitAllowSorting
			,@bitAllowEdit=0
			,@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 set @tintOrder=0 
	END
SET @strSql=''

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=43 AND DFCS.numFormID=43 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
-------------------- 0 - TASK----------------------------

IF(LEN(@CustomSearchCriteria)>0)
BEGIN
SET @CustomSearchCriteria = ' AND '+@CustomSearchCriteria
END
IF(LEN(@ActivityRegularSearch)=0)
BEGIN
	SET @ActivityRegularSearch = '1=1'
END
DECLARE @StaticColumns AS NVARCHAR(MAX)=''

SET @StaticColumns = ' RecordCount,RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,
numContactID,numDivisionID,tintCRMType,Id,bitTask,Startdate,EndTime,itemDesc,
[Name],vcFirstName,vcLastName,numPhone,numPhoneExtension,[Phone],vcCompanyName,vcProfile,vcEmail,Task,Status,numRecOwner,numModifiedBy,numTerId,numAssignedBy,caseid,vcCasenumber,
casetimeId,caseExpId,type,itemid,bitFollowUpAnyTime,numNoOfEmployeesId,vcComPhone,numFollowUpStatus,dtLastFollowUp,vcLastSalesOrderDate,monDealAmount,vcPerformance'
SET @dynamicQuery = ' INSERT INTO #tempRecords(
 '+@StaticColumns +@vcCustomColumnNameOnly+'
) SELECT COUNT(*) OVER() RecordCount,* FROM (SELECT 
Comm.numCommId AS RecordId,
Comm.numCreatedBy AS numCreatedBy, 
ISNULL(Div.numTerId,0) AS numOrgTerId,
-2 AS numTaskId,
bitClosedFlag AS IsTaskClosed,
CONCAT(''<div><div class="pull-left"><a onclick="openCommTask('',Comm.numCommId,'',0)" href="javascript:void(0)">'',dbo.GetListIemName(Comm.bitTask),''</a><br/>'',ISNULL(Comp.vcCompanyName,''''),(CASE WHEN AddC.numContactID IS NOT NULL THEN CONCAT(''&nbsp;<a class="text-green" href="javascript:OpenContact('',AddC.numContactID,'',1)">('',ISNULL(AddC.vcFirstName,''-''),'' '',ISNULL(AddC.vcLastName,''-''),'')</a>'') ELSE '''' END),''<br/>'',(CASE WHEN ISNULL(AddC.numPhone,'''') <> '''' THEN AddC.numPhone ELSE '''' END),(CASE WHEN ISNULL(AddC.numPhoneExtension,'''') <> '''' THEN CONCAT(''&nbsp;('',AddC.numPhoneExtension,'')'') ELSE '''' END),(CASE WHEN ISNULL(AddC.vcEmail,'''') <> '''' THEN CONCAT(''&nbsp;<img src="../images/msg_unread_small.gif" style="cursor:pointer" onclick="return OpemEmail('''''',AddC.vcEmail,'''''','',AddC.numcontactID,'')" />'') ELSE '''' END),''</div><div class="pull-right"><ul class="list-inline"><li><button class="btn btn-flat btn-task-finish" onclick="return ActionItemFinished('',Comm.numCommId,'');"><img src="../images/comflag.png" />&nbsp;Finish</button></li><li><button class="btn btn-comm-followup btn-flat btn-warning" onclick="return ActionItemFollowup('',Comm.numCommId,'','',AddC.numcontactID,'');">Follow-up</button></li></ul></div></div>'') AS TaskTypeName,
Comm.bitTask AS TaskType,
'''' AS OrigDescription,
comm.textDetails AS Description,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtStartTime) AS dtDueDate,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtStartTime) as DueDate,
''-'' As TotalProgress,
dbo.fn_GetContactName(numAssign) As numAssignedTo,
numAssign As numAssignToId,
listdetailsStatus.VcData As Priority, -- Priority column   
 listdetailsActivity.VcData As Activity ,
listdetailsStatus.NumlistItemID As PriorityId, -- Priority column   
 listdetailsActivity.NumlistItemID As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Comp.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,
 Addc.numContactID,Addc.numDivisionID,Div.tintCRMType,Comm.numCommId AS Id,convert(varchar(15),bitTask) as bitTask,
  DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtStartTime) as Startdate,                        
 DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtEndTime) as EndTime ,                                                                        
 Comm.textDetails AS itemDesc,  
 AddC.vcFirstName + '' '' + AddC.vcLastName  as [Name], 
 AddC.vcFirstName,AddC.vcLastName,Addc.numPhone,AddC.numPhoneExtension,                               
 case when Addc.numPhone<>'''' then + AddC.numPhone +case when AddC.numPhoneExtension<>'''' then '' - '' + AddC.numPhoneExtension else '''' end  else '''' end as [Phone], 
  Comp.vcCompanyName As vcCompanyName , ISNULL((SELECT vcData FROM ListDetails WHERE numListID =21 AND numDomainID='+ CAST(@numDomainID AS VARCHAR) +' AND  numListItemID=Comp.vcProfile ) ,'''') vcProfile ,                                                        
 AddC.vcEmail As vcEmail ,    
 dbo.GetListIemName(Comm.bitTask)AS Task,   
 listdetailsStatus.VcData As Status,dbo.fn_GetContactName(Comm.numCreatedBy) AS numRecOwner,dbo.fn_GetContactName(Comm.numModifiedBy) AS numModifiedBy,  
  (select TOP 1 vcData from ListDetails where numListItemID=Div.numTerId) AS numTerId,dbo.fn_GetContactName(Comm.numAssignedBy) AS numAssignedBy,
  convert(varchar(50),comm.caseid) as caseid,(select top 1 vcCaseNumber from cases where cases.numcaseid= comm.caseid )as vcCasenumber,
  ISNULL(comm.casetimeId,0) casetimeId,ISNULL(comm.caseExpId,0) caseExpId,0 as type,'''' as itemid,ISNULL(bitFollowUpAnyTime,0) bitFollowUpAnyTime,  
  dbo.GetListIemName(Comp.numNoOfEmployeesId) AS numNoOfEmployeesId,Div.vcComPhone,
dbo.GetListIemName(Div.numFollowUpStatus) AS numFollowUpStatus,' + 
CONCAT('ISNULL(dbo.FormatedDateFromDate(Div.dtLastFollowUp,',@numDomainID,'),'''')') + ' AS dtLastFollowUp,
' + CONCAT('ISNULL(dbo.FormatedDateFromDate(DateAdd(minute, ',-@ClientTimeZoneOffset,',TempLastOrder.bintCreatedDate),',@numDomainID,'),'''')') + ' AS vcLastSalesOrderDate,ISNULL(TempPerformance.monDealAmount,0) monDealAmount,
(CASE WHEN ISNULL(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(FORMAT(TempPerformance.monDealAmount,''##,####.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) AS vcPerformance
  '+@vcCustomColumnName+'
 FROM  Communication Comm                              
 JOIN AdditionalContactsInformation AddC                                                  
 ON Comm.numContactId = AddC.numContactId                                  
 JOIN DivisionMaster Div                                                                         
 ON AddC.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                                                                         
 ON Div.numCompanyID = Comp.numCompanyId  
 OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=Div.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder
 OUTER APPLY(' + CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = Div.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') + ' AS TempPerformance
 LEFT JOIN OpportunityMaster Opp ON Opp.numDivisionId = Div.numDivisionID AND Opp.numContactId = AddC.numContactId  AND Opp.numOppID = Comm.numOppID 
 LEFT JOIN Sales_process_List_Master splm ON splm.Slp_Id = Opp.numBusinessProcessID 
 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId
  Left Join listdetails listdetailsStatus                                                 
 On Comm.numStatus = listdetailsStatus.NumlistItemID
  Left Join listdetails listdetailsActivity                                                  
 On Comm.numActivity = listdetailsActivity.NumlistItemID
 LEFT JOIN ListDetails AS listCompanyRating
 ON Comp.numCompanyRating=listCompanyRating.NumlistItemID
WHERE '+@ActivityRegularSearch+' AND
	Comm.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (Comm.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or Comm.numAssign= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END)  AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) <= '''+Cast(@endDate as varchar(30))+''')
'+@CustomSearchCriteria+'
 )   Q WHERE '+@OtherRegularSearch+'
  '+@IndivRecordPaging+'
'


EXEC (@dynamicQuery)

-------------------- 0 - Email Communication----------------------------
--''<a onclick="openCommTask(''+CAST(ac.activityid AS VARCHAR)+'',1)" href="javascript:void(0)">Calendar</a>'' As TaskTypeName,
IF(LEN(@ActivityRegularSearch)=0 OR RTRIM(LTRIM(@ActivityRegularSearch))='1=1')
BEGIN
DECLARE @FormattedItems As VARCHAR(MAX)
SET @FormattedItems ='
RecordCount,RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate,EndTime'
SET @dynamicQuery =  ' INSERT INTO #tempRecords(
'+@FormattedItems+'
) SELECT  COUNT(*) OVER() RecordCount,* FROM (  SELECT 
ac.activityid AS RecordId,
0 AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
-2 AS numTaskId,
case when alldayevent = 1 then ''1'' else ''0'' end AS IsTaskClosed,
CONCAT(''<div><div class="pull-left"><a href="javascript:void(0)">Calendar</a><br/>'',ISNULL(Comp.vcCompanyName,''''),(CASE WHEN ACI.numContactID IS NOT NULL THEN CONCAT(''&nbsp;<a class="text-green" href="javascript:OpenContact('',ACI.numContactID,'',1)">('',ISNULL(ACI.vcFirstName,''-''),'' '',ISNULL(ACI.vcLastName,''-''),'')</a>'') ELSE '''' END),''<br/>'',(CASE WHEN ISNULL(ACI.numPhone,'''') <> '''' THEN ACI.numPhone ELSE '''' END),(CASE WHEN ISNULL(ACI.numPhoneExtension,'''') <> '''' THEN CONCAT(''&nbsp;('',ACI.numPhoneExtension,'')'') ELSE '''' END),(CASE WHEN ISNULL(ACI.vcEmail,'''') <> '''' THEN CONCAT(''&nbsp;<img src="../images/msg_unread_small.gif" style="cursor:pointer" onclick="return OpemEmail('''''',ACI.vcEmail,'''''','',ACI.numcontactID,'')" />'') ELSE '''' END),''</div><div class="pull-right"><button class="btn btn-flat btn-task-finish" onclick="return CalendarFinished('',ac.activityid,'');"><img src="../images/comflag.png" />&nbsp;Finish</button></div></div>'') AS TaskTypeName,
12 AS TaskType,
CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END AS OrigDescription,
(SELECT DISTINCT  
replace(replace((SELECT 
CASE(ISNULL(bitpartycalendarTitle,1)) WHEN 1 THEN ''<b><font color=red>Title</font></b>'' + CASE(ISNULL(ac.Subject,'''')) WHEN '''' THEN '''' else ac.Subject END + ''<br>'' else ''''  END  
+ CASE(ISNULL(bitpartycalendarLocation,1)) WHEN 1 THEN ''<b><font color=green>Location</font></b>'' + CASE(ISNULL(ac.location,'''')) WHEN '''' THEN '''' else ac.location END + ''<br>'' else ''''  END  
+ CASE ISNULL(bitpartycalendarDescription,1) 
	WHEN 1 
	THEN CONCAT(''<b><font color=#3c8dbc>Description</font></b>'',
				(CASE 
					WHEN LEN(CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END) > 100 AND NOT ((CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END) like ''%<[A-Za-z0-9]%'' OR (CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END) like ''%</[A-Za-z0-9]%'')
					THEN CONCAT(CAST((CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END) AS VARCHAR(100)),CONCAT(''...'',''<a href="#" role="button" title="Description" data-toggle="popover" data-content="'',(CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END) ,''"> more</a>'')) 
					ELSE (CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END) 
				END)) 
	ELSE ''''  
	END  
FROM Domain where numDomainId='+ CAST(@numDomainID AS VARCHAR) +' 
FOR XML PATH('''')
),''&lt;'', ''<''), ''&gt;'', ''>''))   AS Description,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dateadd(second,duration,startdatetimeutc)) AS dtDueDate,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dateadd(second,duration,startdatetimeutc)) as DueDate,
''0'' As TotalProgress,
''-'' As numAssignedTo,
''0'' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Comp.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',startdatetimeutc) AS Startdate,DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dateadd(second,ISNULL(duration,0),startdatetimeutc)) AS EndTime
 From activity ac join             
activityresource ar on  ac.activityid=ar.activityid             
join resource res on ar.resourceid=res.resourceid                         
LEFT JOIN AdditionalContactsInformation ACI on ACI.numContactId =  (SELECT TOP 1 ContactId FROM ActivityAttendees where ActivityID=ac.activityid)           
 LEFT JOIN DivisionMaster Div                                                          
 ON ACI.numDivisionId = Div.numDivisionID                                   
 LEFT JOIN CompanyInfo  Comp                             
 ON Div.numCompanyID = Comp.numCompanyId
  LEFT JOIN ListDetails AS listCompanyRating
 ON Comp.numCompanyRating=listCompanyRating.NumlistItemID
 OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=Div.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder                 
 OUTER APPLY(' + CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = Div.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') + ' AS TempPerformance

 where  res.numUserCntId = ' + Cast(@numUserCntID as varchar(10)) +'             
and ac.OriginalStartDateTimeUtc IS NULL            
and res.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  and 
(DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) <= '''+Cast(@endDate as varchar(30))+''')
and [status] <> -1                                                     
and ac.activityid not in 
(select distinct(numActivityId) from communication Comm where Comm.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  
AND 1=(Case ' + Cast(@bitFilterRecord as varchar(10)) +' when 1 then Case when Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  then 1 else 0 end 
		else  Case when (Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  or Comm.numCreatedBy=' + Cast(@numUserCntID as varchar(10)) +' ) then 1 else 0 end end)   
		AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (Comm.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or Comm.numAssign= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END)                                                                   

)) Q WHERE '+@OtherRegularSearch+'  '+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)
-------------------- 1 - CASE----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate
) SELECT  COUNT(*) OVER() RecordCount,* FROM (  SELECT 
C.numCaseId AS RecordId,
C.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
CASE WHEN C.numStatus=1008 THEN 1 ELSE 0 END AS IsTaskClosed,
''<a onclick="openCaseDetails(''+CAST(C.numCaseId AS VARCHAR)+'')" href="javascript:void(0)">Case</a>'' As TaskTypeName,
1 AS TaskType,
'''' AS OrigDescription,
C.textSubject AS Description,
cast(C.intTargetResolveDate as datetime) AS dtDueDate,
cast(C.intTargetResolveDate as datetime) as DueDate,
''0'' As TotalProgress,
dbo.fn_GetContactName(C.numAssignedTo) As numAssignedTo,
C.numAssignedTo As numAssignToId,
listdetailsStatus.VcData As Priority, -- Priority column   
'''' As Activity ,
listdetailsStatus.NumlistItemID As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,cast(C.intTargetResolveDate as datetime) AS Startdate

 FROM 
	Cases  C                          
INNER JOIN  
	AdditionalContactsInformation  AddC                          
ON 
	C.numContactId = AddC.numContactId                           
INNER JOIN 
	DivisionMaster Div                           
ON 
	AddC.numDivisionId = Div.numDivisionID AND  
	AddC.numDivisionId = Div.numDivisionID                           
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
 LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
  Left Join listdetails listdetailsStatus                                                 
 On C.numPriority = listdetailsStatus.NumlistItemID
WHERE
	C.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND C.numStatus <> 136  AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (C.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or C.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END)  AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',intTargetResolveDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',intTargetResolveDate) <= '''+Cast(@endDate as varchar(30))+''') 
) Q WHERE '+@OtherRegularSearch+' '+@IndivRecordPaging+'
'

PRINT CAST(@dynamicQuery AS NTEXT)
EXEC (@dynamicQuery)

-------------------- 2 - Project Task ----------------------------

DECLARE @dynamicQuery1 VARCHAR(MAX) = ''

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,
numTaskEstimationInMinutes
,numTimeSpentInMinutes
,dtLastStartDate,Startdate
)  SELECT  COUNT(*) OVER() RecordCount,* FROM (SELECT
T.numProjectId AS RecordId,
T.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
T.bitTaskClosed AS IsTaskClosed,
CONCAT(''<div><div class="pull-left">'',''<a onclick="openProjectTask(''+CAST(T.numProjectId AS VARCHAR)+'')" href="javascript:void(0)">Project Task</a><br/><span>'',ISNULL(OP.vcProjectID,''''),''</span></div><div id="divTaskControlsProject" class="pull-right">'',(CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=T.numTaskId ORDER BY dtActionTime DESC,ID DESC),0) 
	WHEN 4 THEN (CASE WHEN EXISTS (SELECT C.numContractId FROM Contracts C WHERE C.numDivisonId=Div.numDivisionID) THEN CONCAT(''<ul class="list-inline">
	<li><button class="btn btn-xs btn-primary" onclick="return TaskClosed('',T.numTaskId,'');">Close</button></li>
    <li style="padding-left: 0px; padding-right: 0px">'',dbo.GetTimeSpendOnTaskByProject(T.numDomainID,T.numTaskId,1),''</li>
    <li style="padding-left: 0px; padding-right: 0px"><img src="../images/timeIconnnnn.png" runat="server" id="imgTimeIconn" style="height: 25px" /></li>
    <li style="padding-left: 0px; padding-right: 0px"><input type="checkbox" onclick="addRemoveTimeProject('',T.numDomainID,'','',Div.numDivisionID,'','',dbo.GetTimeSpendOnTaskByProject(T.numDomainID,T.numTaskId,2),'','',T.numTaskId,'','''''',DATEADD(MINUTE,'+Cast(-@ClientTimeZoneOffset as varchar(10)) +',(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId AND tintAction=1)),'''''','''''',DATEADD(MINUTE,'+Cast(-@ClientTimeZoneOffset as varchar(10)) +',(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId AND tintAction=4)),'''''','',T.numAssignTo,'','''''',T.vcTaskName,'''''','''''',(CASE WHEN dbo.GetTimeSpendOnTaskByProject(T.numDomainID,T.numTaskId,0) = ''Invalid time sequence'' THEN ''00:00'' ELSE dbo.GetTimeSpendOnTaskByProject(T.numDomainID,T.numTaskId,0) END),'''''','''''',ISNULL(ACustomer.vcEmail,''''),'''''','''''',dbo.fn_GetContactName(ACustomer.numContactId),'''''')"'',(CASE WHEN ISNULL(T.bitTimeAddedToContract,0)=1 THEN '' checked=''''checked'''''' ELSE '''' END),'' class="chk_'',T.numTaskId,''" /></li>
</ul>'') ELSE ''<img src="../images/comflag.png" />'' END)
	WHEN 3 THEN CONCAT(''<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowProject('',T.numTaskId,'');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-warning" onclick="return ShowTaskPausedWindowProject(this,'',T.numTaskId,'');">Pause</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-flat btn-task-finish" onclick="return TaskFinishedProject(this,'',T.numTaskId,'',4);"><img src="../images/comflag.png" />&nbsp;Finish</button></li><li style="vertical-align:middle;padding-right:0px;"><label class="taskTimerInitial"></label></li></ul>'')
	WHEN 2 THEN CONCAT(''<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowProject('',T.numTaskId,'');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat bg-purple" onclick="return TaskStartedProject(this,'',T.numTaskId,'',1);">Resume</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-flat btn-task-finish" onclick="return TaskFinishedProject(this,'',T.numTaskId,'',4);"><img src="../images/comflag.png" />&nbsp;Finish</button></li></ul>'')
	WHEN 1 THEN CONCAT(''<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowProject('',T.numTaskId,'');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-warning" onclick="return ShowTaskPausedWindowProject(this,'',T.numTaskId,'');">Pause</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-flat btn-task-finish" onclick="return TaskFinishedProject(this,'',T.numTaskId,'',4);"><img src="../images/comflag.png" />&nbsp;Finish</button></li><li style="vertical-align:middle;padding-right:0px;"><label class="taskTimerInitial"></label></li></ul>'')
	ELSE CONCAT(''<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowProject('',T.numTaskId,'');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-success btn-task-start" onclick="return TaskStartedProject(this,'',T.numTaskId,'',0);">Start</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-flat btn-task-finish" onclick="return TaskFinishedProject(this,'',T.numTaskId,'',4);"><img src="../images/comflag.png" />&nbsp;Finish</button></li></ul>'')
END),''</div></div>'') As TaskTypeName,
2 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',OP.dtmEndDate) AS dtDueDate,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',OP.dtmEndDate) as DueDate,
ISNULL((SELECT TOP 1 intTotalProgress FROM ProjectProgress WHERE numProId = T.numProjectId),0) As TotalProgress,
dbo.fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating
,((ISNULL(T.numHours,0) * 60) + ISNULL(T.numMinutes,0)) numTaskEstimationInMinutes
,(CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC),0) 
	WHEN 4 THEN 0
	WHEN 3 THEN dbo.GetTimeSpendOnTaskByProject(T.numDomainID,T.numTaskId,2)
	WHEN 2 THEN dbo.GetTimeSpendOnTaskByProject(T.numDomainID,T.numTaskId,2)
	WHEN 1 THEN 0
END) numTimeSpentInMinutes
,(CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=T.numTaskId ORDER BY dtActionTime DESC,ID DESC),0) 
	WHEN 4 THEN NULL
	WHEN 3 THEN DATEADD(MINUTE,'+Cast(-@ClientTimeZoneOffset as varchar(10)) +',(SELECT TOP 1 dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC))
	WHEN 2 THEN NULL
	WHEN 1 THEN DATEADD(MINUTE,'+Cast(-@ClientTimeZoneOffset as varchar(10)) +',(SELECT TOP 1 dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC))
END) dtLastStartDate,CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END AS Startdate'
SET @dynamicQuery1 = ' FROM 
	StagePercentageDetailsTask AS T
	LEFT JOIN StagePercentageDetails AS S
	ON S.numStageDetailsId=T.numStageDetailsId AND S.numProjectID=T.numProjectID
	INNER JOIN ProjectsMaster AS OP 
	ON T.numProjectID=OP.numProId
	LEFT JOIN StagePercentageMaster AS SM
	ON S.numStagePercentageId=SM.numStagePercentageId                        
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating ON Com.numCompanyRating=listCompanyRating.NumlistItemID
LEFT JOIN AdditionalContactsInformation ACustomer ON OP.numCustPrjMgr = ACustomer.numContactId
WHERE
	T.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' 
	AND (NOT EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID AND SPDTTL.tintAction=4) OR (ISNULL(T.bitTaskClosed,0) = 0 AND EXISTS (SELECT C.numContractId FROM Contracts C WHERE C.numDivisonId=Div.numDivisionID)))
	AND ((CAST(DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',OP.dtmStartDate) AS DATE) >= '''+Cast(CAST(@startDate AS DATE) as varchar(30))+''' and CAST(DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',OP.dtmStartDate) AS DATE) <= '''+Cast(CAST(@endDate AS DATE) as varchar(30))+''') OR (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',OP.dtmEndDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',OP.dtmEndDate) <= '''+Cast(@endDate as varchar(30))+''')) 
	AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (T.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or T.numAssignTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END) ) Q WHERE '+@OtherRegularSearch + @IndivRecordPaging

--PRINT CAST(@dynamicQuery AS NTEXT)

EXEC (@dynamicQuery + @dynamicQuery1)

------------------ 3 - Opportunity  Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate
) SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT 
T.numOppId AS RecordId,
T.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
T.bitTaskClosed AS IsTaskClosed,
CONCAT(''<div><div class="pull-left"><a onclick="openTask('',CAST(T.numOppId AS VARCHAR),'','',CAST(S.numStagePercentageId AS VARCHAR),'','',CAST(S.tinProgressPercentage AS VARCHAR),'','',CAST(S.tintConfiguration AS VARCHAR)+'')" href="javascript:void(0)">Opportunity Task</a><br/>'',ISNULL(Com.vcCompanyName,''''),(CASE WHEN ADC.numContactID IS NOT NULL THEN CONCAT(''&nbsp;<a class="text-green" href="javascript:OpenContact('',ADC.numContactID,'',1)">('',ISNULL(ADC.vcFirstName,''-''),'' '',ISNULL(ADC.vcLastName,''-''),'')</a>'') ELSE '''' END),''<br/>'',(CASE WHEN ISNULL(ADC.numPhone,'''') <> '''' THEN ADC.numPhone ELSE '''' END),(CASE WHEN ISNULL(ADC.numPhoneExtension,'''') <> '''' THEN CONCAT(''&nbsp;('',ADC.numPhoneExtension,'')'') ELSE '''' END),(CASE WHEN ISNULL(ADC.vcEmail,'''') <> '''' THEN CONCAT(''&nbsp;<img src="../images/msg_unread_small.gif" style="cursor:pointer" onclick="return OpemEmail('''''',ADC.vcEmail,'''''','',ADC.numcontactID,'')" />'') ELSE '''' END),''</div><div class="pull-right"><button class="btn btn-flat btn-task-finish" onclick="return TaskClosed('',T.numTaskId,'');"><img src="../images/comflag.png" />&nbsp;Finish</button></div></div>'') As TaskTypeName,
3 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END AS dtDueDate,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END as DueDate,
ISNULL((SELECT TOP 1 intTotalProgress FROM ProjectProgress WHERE numOppId = T.numOppId),0) As TotalProgress,
dbo.fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END  AS Startdate

 FROM 
	StagePercentageDetailsTask AS T
	LEFT JOIN StagePercentageDetails AS S
	ON S.numStageDetailsId=T.numStageDetailsId AND S.numOppId=T.numOppId
	LEFT JOIN OpportunityMaster AS OP 
	ON T.numOppId=OP.numOppId
	LEFT JOIN StagePercentageMaster AS SM
	ON S.numStagePercentageId=SM.numStagePercentageId                        
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN AdditionalContactsInformation ADC ON OP.numContactId = ADC.numContactId
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	T.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType=1 AND OP.tintOppStatus=0 AND T.numOppId>0 AND s.numOppId>0 AND T.bitSavedTask=1 AND T.bitTaskClosed=0 AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (T.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or T.numAssignTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END) ) Q WHERE '+@OtherRegularSearch+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''')
'+@IndivRecordPaging+' 
 '
 
 EXEC (@dynamicQuery)

------------------ 4 -  Sales Order Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate
)SELECT  COUNT(*) OVER() RecordCount,* FROM (  SELECT 
T.numOppId AS RecordId,
T.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
T.bitTaskClosed AS IsTaskClosed,
CONCAT(''<div><div class="pull-left"><a onclick="openTask('',CAST(T.numOppId AS VARCHAR),'','',CAST(S.numStagePercentageId AS VARCHAR),'','',CAST(S.tinProgressPercentage AS VARCHAR),'','',CAST(S.tintConfiguration AS VARCHAR)+'')" href="javascript:void(0)">Sales Order Task</a><br/>'',ISNULL(Com.vcCompanyName,''''),(CASE WHEN ADC.numContactID IS NOT NULL THEN CONCAT(''&nbsp;<a class="text-green" href="javascript:OpenContact('',ADC.numContactID,'',1)">('',ISNULL(ADC.vcFirstName,''-''),'' '',ISNULL(ADC.vcLastName,''-''),'')</a>'') ELSE '''' END),''<br/>'',(CASE WHEN ISNULL(ADC.numPhone,'''') <> '''' THEN ADC.numPhone ELSE '''' END),(CASE WHEN ISNULL(ADC.numPhoneExtension,'''') <> '''' THEN CONCAT(''&nbsp;('',ADC.numPhoneExtension,'')'') ELSE '''' END),(CASE WHEN ISNULL(ADC.vcEmail,'''') <> '''' THEN CONCAT(''&nbsp;<img src="../images/msg_unread_small.gif" style="cursor:pointer" onclick="return OpemEmail('''''',ADC.vcEmail,'''''','',ADC.numcontactID,'')" />'') ELSE '''' END),''</div><div class="pull-right"><button class="btn btn-flat btn-task-finish" onclick="return TaskClosed('',T.numTaskId,'');"><img src="../images/comflag.png" />&nbsp;Finish</button></div></div>'') As TaskTypeName,
4 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END AS dtDueDate,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END as DueDate,
ISNULL((SELECT TOP 1 intTotalProgress FROM ProjectProgress WHERE numOppId = T.numOppId),0) As TotalProgress,
dbo.fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END AS Startdate

 FROM 
	StagePercentageDetailsTask AS T
	LEFT JOIN StagePercentageDetails AS S
	ON S.numStageDetailsId=T.numStageDetailsId AND S.numOppId=T.numOppId
	LEFT JOIN OpportunityMaster AS OP 
	ON T.numOppId=OP.numOppId
	LEFT JOIN StagePercentageMaster AS SM
	ON S.numStagePercentageId=SM.numStagePercentageId                        
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN AdditionalContactsInformation ADC ON OP.numContactId = ADC.numContactId 
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	T.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType=1 AND OP.tintOppStatus=1 AND T.numOppId>0 AND s.numOppId>0 AND T.bitSavedTask=1 AND T.bitTaskClosed=0 AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (T.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or T.numAssignTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END) ) Q WHERE '+@OtherRegularSearch+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''')
'+@IndivRecordPaging+'
 '
 EXEC (@dynamicQuery)
  
------------------ 5 -  Work Order Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate
)SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT 
WorkOrder.numWOID AS RecordId,
WorkOrder.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
WorkOrder.numWOID AS numTaskId,
(CASE WHEN WorkOrder.numWOStatus = 23184 THEN 1 ELSE 0 END) AS IsTaskClosed,
CONCAT(''<a href="../items/frmWorkOrder.aspx?WOID='',WorkOrder.numWOID,''" target="_blank">Work Order Management</a>'') As TaskTypeName,
5 AS TaskType,
'''' AS OrigDescription,
CONCAT(''<b>'',Item.vcItemName,''</b>'','' ('',WorkOrder.numQtyItemsReq,'')'','' <b>Work Order Status :'', ''</b>'',''<label class="lblWorkOrderStatus" id="'',WorkOrder.numWOID,''"><i class="fa fa-refresh fa-spin"></i></lable>'') AS Description,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) AS dtDueDate,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) as DueDate,
dbo.GetTotalProgress(WorkOrder.numDomainID,WorkOrder.numWOID,1,1,'''',0) As TotalProgress,
dbo.fn_GetContactName(WorkOrder.numAssignedTo) As numAssignedTo,
WorkOrder.numAssignedTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) AS Startdate

 FROM 
	WorkOrder
INNER JOIN 
	Item
ON
	WorkOrder.numItemCode = Item.numItemCode
LEFT JOIN 
	OpportunityMaster
ON 
	WorkOrder.numOppId=OpportunityMaster.numOppId
INNER JOIN 
	DivisionMaster Div                           
ON 
	Div.numDivisionId = (CASE WHEN OpportunityMaster.numOppID IS NOT NULL THEN OpportunityMaster.numDivisionID ELSE ' + CAST(@numDomainDivisionID AS VARCHAR) + ' END)                        
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN 
	ListDetails AS listCompanyRating
ON 
	Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	WorkOrder.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' 
	AND WorkOrder.numWOStatus <> 23184
	AND WorkOrder.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + '
	AND ((DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmStartDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmStartDate) <= '''+Cast(@endDate as varchar(30))+''') OR (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) <= '''+Cast(@endDate as varchar(30))+'''))
) Q WHERE '+@OtherRegularSearch+' '+@IndivRecordPaging+'
 '
 EXEC (@dynamicQuery)
------------------ 6 -  Work Center Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,
numTaskEstimationInMinutes
,numTimeSpentInMinutes
,dtLastStartDate
,numRemainingQty
,Startdate
) SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT 
T.numTaskId AS RecordId,
T.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
(CASE WHEN EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID AND SPDTTL.tintAction=4) THEN 1 ELSE 0 END) AS IsTaskClosed,
CONCAT(''<div><div class="pull-left"><a href="../items/frmWorkOrder.aspx?WOID='',W.numWOID,''" target="_blank">Work Center Task</a><br/><span>'',CONCAT(I.vcItemName,'' ('',W.numQtyItemsReq,'')''),''</span></div><div class="pull-right" id="divTaskControlsWorkOrder">'',(CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=T.numTaskId ORDER BY dtActionTime DESC, ID DESC),0) 
	WHEN 4 THEN ''<img src="../images/comflag.png" />''
	WHEN 3 THEN CONCAT(''<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowWorkOrder('',T.numTaskId,'');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-success btn-task-start" onclick="return TaskStartedWorkOrder(this,'',T.numTaskId,'',0);">Start</button></li><li style="vertical-align:middle;padding-right:0px;"><label class="taskTimerInitial"></label></li></ul>'')
	WHEN 2 THEN CONCAT(''<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowWorkOrder('',T.numTaskId,'');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat bg-purple" onclick="return TaskStartedWorkOrder(this,'',T.numTaskId,'',1);">Resume</button></li></ul>'')
	WHEN 1 THEN CONCAT(''<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowWorkOrder('',T.numTaskId,'');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-warning" onclick="return ShowTaskPausedWindowWorkOrder(this,'',T.numTaskId,'');">Pause</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-flat btn-task-finish" onclick="return TaskFinishedWorkOrder(this,'',T.numTaskId,'',4);"><img src="../images/comflag.png" />&nbsp;Finish</button></li><li style="vertical-align:middle;padding-right:0px;"><input type="text" onkeydown="return ProcessedUnitsChangedWorkOrder(this,'',T.numTaskId,'',event);" class="form-control txtUnitsProcessed" style="width:50px;padding:1px 2px 1px 2px;height:23px;" /></li><li style="vertical-align:middle;padding-right:0px;"><label class="taskTimerInitial"></label></li></ul>'')
	ELSE CONCAT(''<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowWorkOrder('',T.numTaskId,'');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-success btn-task-start" onclick="return TaskStartedWorkOrder(this,'',T.numTaskId,'',0);">Start</button></li></ul>'')
END),''</div></div>'') As TaskTypeName,
6 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) AS dtDueDate,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) as DueDate,
(CASE 
	WHEN EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID AND SPDTTL.tintAction=4) THEN CAST(100 AS INT)
	ELSE CAST(((ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID),0) / W.numQtyItemsReq) * 100) AS INT)
END) As TotalProgress,
dbo.fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating
 ,(((ISNULL(T.numHours,0) * 60) + ISNULL(T.numMinutes,0)) * ISNULL(W.numQtyItemsReq,0)) numTaskEstimationInMinutes
,(CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC),0) 
	WHEN 4 THEN 0
	WHEN 3 THEN dbo.GetTimeSpendOnTask(T.numDomainID,T.numTaskId,2)
	WHEN 2 THEN dbo.GetTimeSpendOnTask(T.numDomainID,T.numTaskId,2)
	WHEN 1 THEN 0
END) numTimeSpentInMinutes
,(CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=T.numTaskId ORDER BY dtActionTime DESC,ID DESC),0) 
	WHEN 4 THEN NULL
	WHEN 3 THEN DATEADD(MINUTE,'+Cast(-@ClientTimeZoneOffset as varchar(10)) +',(SELECT TOP 1 dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC))
	WHEN 2 THEN NULL
	WHEN 1 THEN DATEADD(MINUTE,'+Cast(-@ClientTimeZoneOffset as varchar(10)) +',(SELECT TOP 1 dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC))
END) dtLastStartDate
,(CASE
	WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId AND tintAction=4) THEN 0
	ELSE ISNULL(W.numQtyItemsReq,0) - ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId),0)
END) AS numRemainingQty
,DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) AS Startdate
 FROM 
	StagePercentageDetailsTask AS T
	INNER JOIN WorkOrder AS W 
	ON T.numWorkOrderId=W.numWOId     
	INNER JOIN Item I ON W.numItemCode=I.numItemCode
LEFT JOIN OpportunityMaster AS OP 
	ON W.numOppId=OP.numOppId
INNER JOIN 
	DivisionMaster Div                           
ON 
	Div.numDivisionId = (CASE WHEN OP.numOppID IS NOT NULL THEN OP.numDivisionID ELSE ' + CAST(@numDomainDivisionID AS VARCHAR) + ' END)                         
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	W.numDomainID = ' + Cast(@numDomainID as varchar(10))  + '
	AND W.numWOStatus <> 23184 
	AND NOT EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID AND SPDTTL.tintAction=4) 
	AND T.numAssignTo = ' + CAST(@numUserCntID AS VARCHAR) + '
	AND ((DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmStartDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmStartDate) <= '''+Cast(@endDate as varchar(30))+''') OR (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) <= '''+Cast(@endDate as varchar(30))+''')) 
) Q WHERE '+@OtherRegularSearch+' '+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)

------------------ 7 -  Requisition Approval ----------------------------

IF(@bitREQPOApproval=1 AND @numUserCntID IN(SELECT * FROM Split(@vchREQPOApprovalEmp,',') WHERE Items<>''))
BEGIN
SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate
) SELECT  COUNT(*) OVER() RecordCount,* FROM( SELECT 
OP.numOppId AS RecordId,
OP.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
CASE WHEN ISNULL(OP.intReqPOApproved,0)=0 THEN 0 ELSE 1 END AS IsTaskClosed,
''<a  onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')" href="javascript:void(0)">Requisition Approval</a>'' As TaskTypeName,
7 AS TaskType,
'''' AS OrigDescription,
''<b>For :</b> <a class="underLineHyperLink" onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')" href="javascript:void(0)">''+OP.vcPOppName+''</a> &nbsp;<i onclick="ApproveRejectPORequistion(''+CAST(OP.numOppId AS VARCHAR)+'',1)"  class="fa fa-thumbs-up cursor"></i>  &nbsp;<i onclick="ApproveRejectPORequistion(''+CAST(OP.numOppId AS VARCHAR)+'',2)"  class="fa fa-thumbs-down cursor"></i>'' AS Description,
OP.intPEstimatedCloseDate AS dtDueDate,
OP.intPEstimatedCloseDate as DueDate,
0 As TotalProgress,
dbo.fn_GetContactName('+CAST(@numUserCntID AS VARCHAR(200))+') As numAssignedTo,
'+CAST(@numUserCntID AS VARCHAR(200))+' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,OP.intPEstimatedCloseDate  AS Startdate

 FROM  
 OpportunityMaster AS OP 
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	OP.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType = 2 AND OP.tintOppStatus=0 
	AND 1 = (CASE WHEN '+CAST(@decReqPOMinValue AS VARCHAR(100))+'>0 AND Op.monPAmount>'+CAST(@decReqPOMinValue AS VARCHAR(100))+' THEN 1  WHEN '+CAST(@decReqPOMinValue AS VARCHAR(100))+'=0 THEN 1  ELSE 0 END ) ) AS Q
WHERE '+@OtherRegularSearch+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)

END
------------------ 8 -  PO Approval ----------------------------
IF(@bitREQPOApproval=1 AND @numUserCntID IN(SELECT * FROM Split(@vchREQPOApprovalEmp,',') WHERE Items<>''))
BEGIN
SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate
) SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT
OP.numOppId AS RecordId,
OP.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
CASE WHEN ISNULL(OP.intReqPOApproved,0)=0 THEN 0 ELSE 1 END AS IsTaskClosed,
''<a onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')" href="javascript:void(0)">PO Approval</a>'' As TaskTypeName,
8 AS TaskType,
'''' AS OrigDescription,
''<b>For:</b> <a href="javascript:void(0)"  class="underLineHyperLink" onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')">''+OP.vcPOppName+''</a> &nbsp;<i onclick="ApproveRejectPORequistion(''+CAST(OP.numOppId AS VARCHAR)+'',1)"  class="fa fa-thumbs-up cursor"></i>  &nbsp;<i onclick="ApproveRejectPORequistion(''+CAST(OP.numOppId AS VARCHAR)+'',2)"  class="fa fa-thumbs-down cursor"></i> '' AS Description,
OP.intPEstimatedCloseDate AS dtDueDate,
OP.intPEstimatedCloseDate as DueDate,
0 As TotalProgress,
dbo.fn_GetContactName('+CAST(@numUserCntID AS VARCHAR(200))+') As numAssignedTo,
'+CAST(@numUserCntID AS VARCHAR(200))+' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,OP.intPEstimatedCloseDate AS Startdate

 FROM  
 OpportunityMaster AS OP 
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	OP.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType = 2 AND OP.tintOppStatus=1 
	AND 1 = (CASE WHEN '+CAST(@decReqPOMinValue AS VARCHAR(100))+'>0 AND Op.monPAmount>'+CAST(@decReqPOMinValue AS VARCHAR(100))+' THEN 1  WHEN '+CAST(@decReqPOMinValue AS VARCHAR(100))+'=0 THEN 1  ELSE 0 END )) AS Q
	WHERE  '+@OtherRegularSearch+' AND  (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)

END

------------------ 9 -  Price Margin Approval ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate
) SELECT COUNT(*) OVER() RecordCount,* FROM (SELECT
OP.numOppId AS RecordId,
OP.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
ISNULL(OP.bitReqPOApproved,0) AS IsTaskClosed,
''<a onclick="OpenMarginApproval(''+CAST(OP.numOppId AS VARCHAR)+'')" href="javascript:void(0)">Price Margin Approval</a>'' As TaskTypeName,
9 AS TaskType,
'''' AS OrigDescription,
''<b>For : </b> <a href="javascript:void(0)"  onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')">''+OP.vcPOppName+''</a> : '' +dbo.ItemUnitPriceApproval_CheckAndGetItemDetails(OP.numOppId,OP.numDomainId)+''  &nbsp;<i onclick="ApproveRejectPriceMarginApproval(''+CAST(OP.numOppId AS VARCHAR)+'',1)" class="fa fa-thumbs-up cursor"></i>  &nbsp;<i onclick="ApproveRejectPriceMarginApproval(''+CAST(OP.numOppId AS VARCHAR)+'',2)" class="fa fa-thumbs-down cursor"></i>''  AS Description,
OP.bintCreatedDate AS dtDueDate,
OP.bintCreatedDate as DueDate,
0 As TotalProgress,
dbo.fn_GetContactName('+CAST(@numUserCntID AS VARCHAR(200))+') As numAssignedTo,
'+CAST(@numUserCntID AS VARCHAR(200))+' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,OP.bintCreatedDate AS Startdate

 FROM  
 OpportunityMaster AS OP 
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	OP.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType = 1 AND (SELECT COUNT(OpportunityItems.numoppitemtCode) FROM OpportunityItems 
						WHERE OpportunityItems.bitItemPriceApprovalRequired=1 AND OpportunityItems.numOppId=OP.numOppId) >0
	AND '+CAST(@numUserCntID AS VARCHAR(100))+' IN (SELECT numUserID FROM UnitPriceApprover WHERE numDomainId= ' + Cast(@numDomainID as varchar(10))  + ' )) AS Q
	WHERE  '+@OtherRegularSearch+' AND  (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'

'
EXEC (@dynamicQuery)

------------------------10 - A/R Invoice Due------------------------------------
IF(@bitARInvoiceDue=1 AND @numUserCntID IN(SELECT * FROM Split(@vchARInvoiceDue,',') WHERE Items<>''))
BEGIN
 DECLARE  @AuthoritativeSalesBizDocId  AS INTEGER
    SELECT @AuthoritativeSalesBizDocId = isnull(numAuthoritativeSales,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId = @numDomainId;
SET @dynamicQuery=  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate
) SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT
OM.numOppId AS RecordId,
OM.numCreatedBy AS numCreatedBy,
ISNULL(DM.numTerId,0) AS numOrgTerId,
OB.numOppBizDocsId AS numTaskId,
0 AS IsTaskClosed,
''<a href="javascript:void(0)"  onclick="openBizDoc(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'')">A/R Invoice Due</a>'' As TaskTypeName,
10 AS TaskType,
'''' AS OrigDescription,
''<b>For : </b> <a href="javascript:void(0)" class="underLineHyperLink" onclick="openBizDoc(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'')">''+CAST(OB.[vcBizDocID] AS VARCHAR)+''</a> of <a href="javascript:void(0)" class="underLineHyperLink" onclick="openSalesOrder(''+CAST(OM.numOppId AS VARCHAR)+'')">''+CAST(OM.[vcPOppName] AS VARCHAR)+''</a> <b>Balance Due</b> <a href="javascript:void(0)" onclick="OpenAmtPaid(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'',''+CAST(OM.[numDivisionId] AS VARCHAR)+'')"   class="underLineHyperLink">'' + CAST(ISNULL(C.[varCurrSymbol],'''') AS VARCHAR) + '''' + CAST(CAST(isnull(OB.monDealAmount  - ISNULL(TablePayments.monPaidAmount,0),0) AS DECIMAL(18,2)) AS VARCHAR)+''</a>''  AS Description,
 CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate)
                          WHEN 0 THEN ob.[dtFromDate]
                        END AS dtDueDate,
 CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate),
                                                                   1)
                          WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],1)
                        END as DueDate,
0 As TotalProgress,
'''' As numAssignedTo,
0 As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(DM.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,
  (CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate)
                          WHEN 0 THEN ob.[dtFromDate]
                        END) AS StartDate

 FROM   [OpportunityMaster] OM INNER JOIN [DivisionMaster] DM ON OM.[numDivisionId] = DM.[numDivisionID]
				LEFT JOIN CompanyInfo  Com ON DM.numCompanyID = Com.numCompanyId    
				LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
               LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN [Currency] C ON OM.[numCurrencyID] = C.[numCurrencyID]
			   OUTER APPLY
			   (
					SELECT 
						SUM(monAmountPaid) AS monPaidAmount
					FROM
						DepositeDetails DD
					INNER JOIN
						DepositMaster DM
					ON
						DD.numDepositID=DM.numDepositId
					WHERE 
						numOppID = OM.numOppID
						AND numOppBizDocsID=OB.numOppBizDocsID
						AND CONVERT(DATE,DM.dtDepositDate) <= DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +','''+Cast(@endDate as varchar(100))+''')
				) TablePayments
        WHERE  OM.[tintOppType] = 1
               AND OM.[tintOppStatus] = 1
               AND om.[numDomainId] = ' + Cast(@numDomainId as varchar(10))  + '
               AND OB.[numBizDocId] = '+CAST(@AuthoritativeSalesBizDocId As VARCHAR)+'
               AND OB.bitAuthoritativeBizDocs=1
               AND ISNULL(OB.tintDeferred,0) <> 1
			   AND (OM.numAccountClass= 0 OR 0=0)
			   AND CONVERT(DATE,OB.[dtCreatedDate]) <= DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +','''+Cast(@endDate as varchar(100))+''')
               AND isnull(OB.monDealAmount
                            ,0) > 0  AND isnull(OB.monDealAmount * OM.fltExchangeRate
                        - ISNULL(TablePayments.monPaidAmount,0) * OM.fltExchangeRate,0) != 0
	) AS Q
	WHERE  '+@OtherRegularSearch+' AND  (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)
END
------------------------11 - A/P Bill Due------------------------------------
IF(@bitAPBillsDue=1 AND @numUserCntID IN(SELECT * FROM Split(@vchAPBillsDue,',') WHERE Items<>''))
BEGIN   
   DECLARE  @AuthoritativePurchaseBizDocId  AS INTEGER
    SELECT  @AuthoritativePurchaseBizDocId= isnull(numAuthoritativePurchase,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId =  @numDomainId
	

SET @dynamicQuery = '  INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate
) SELECT  COUNT(*) OVER() RecordCount,* FROM (SELECT
OM.numOppId AS RecordId,
OM.numCreatedBy AS numCreatedBy,
ISNULL(DM.numTerId,0) AS numOrgTerId,
OB.numOppBizDocsId AS numTaskId,
0 AS IsTaskClosed,
''<a href="javascript:void(0)" onclick="openPurchaseBill(''+CAST(DM.[numDivisionID] As VARCHAR)+'')">A/P Bill Due<a/>'' As TaskTypeName,
11 AS TaskType,
'''' AS OrigDescription,
''<b>For : </b> <a href="javascript:void(0)" class="underLineHyperLink" onclick="openBizDoc(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'')">''+CAST(OB.[vcBizDocID] AS VARCHAR)+''</a> of <a href="javascript:void(0)" class="underLineHyperLink" onclick="openSalesOrder(''+CAST(OM.numOppId AS VARCHAR)+'')">''+CAST(OM.[vcPOppName] AS VARCHAR)+''</a> <b>Balance Due : </b> <a href="javascript:void(0)" onclick="openBizDoc(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'')"   class="underLineHyperLink">'' + CAST(ISNULL(C.[varCurrSymbol],'''') AS VARCHAR) + '''' + CAST(CAST(isnull((OB.monDealAmount * OM.fltExchangeRate)  - (ISNULL(TablePayment.monPaidAmount,0)* OM.fltExchangeRate),0) AS DECIMAL(18,2))  AS VARCHAR)+''</a>''  AS Description,
 CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate)
                          WHEN 0 THEN ob.[dtFromDate]
                        END AS dtDueDate,
 CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate),
                                                                   1)
                          WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],1)
                        END as DueDate,
0 As TotalProgress,
'''' As numAssignedTo,
0 As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(DM.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,
  (CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate)
                          WHEN 0 THEN ob.[dtFromDate]
                        END) AS StartDate

   FROM   [OpportunityMaster] OM
               INNER JOIN [DivisionMaster] DM
                 ON OM.[numDivisionId] = DM.[numDivisionID]
				 LEFT JOIN CompanyInfo  Com ON DM.numCompanyID = Com.numCompanyId    
				LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
               LEFT OUTER JOIN OpportunityBizDocs OB
                 ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN [Currency] C
                 ON OM.[numCurrencyID] = C.[numCurrencyID]
				 OUTER APPLY
				(
					SELECT
						SUm(BPD.monAmount) monPaidAmount
					FROM	
						BillPaymentDetails BPD
					INNER JOIN
						BillPaymentHeader BPH
					ON
						BPD.numBillPaymentID=BPH.numBillPaymentID
					WHERE
						BPD.numOppBizDocsID = OB.numOppBizDocsId
						AND CAST(BPH.dtPaymentDate AS DATE) <= DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +','''+Cast(@endDate as varchar(100))+''')
				) TablePayment
        WHERE  OM.[tintOppType] = 2
               AND OM.[tintOppStatus] = 1
               AND om.[numDomainId] = 1
               AND OB.[numBizDocId] = '+CAST(@AuthoritativePurchaseBizDocId AS VARCHAR)+' 
               AND OB.bitAuthoritativeBizDocs=1
               AND ISNULL(OB.tintDeferred,0) <> 1
			   AND (OM.numAccountClass= 0 OR 0=0)
			   AND CONVERT(DATE,OB.[dtCreatedDate]) <= DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +','''+Cast(@endDate as varchar(100))+''')
               AND  isnull(OB.monDealAmount * OM.fltExchangeRate,0) > 0 
			   AND isnull(OB.monDealAmount * OM.fltExchangeRate
                        - ISNULL(TablePayment.monPaidAmount,0) * OM.fltExchangeRate,0) > 0
	) AS Q
	WHERE  '+@OtherRegularSearch+' AND  (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'

EXEC (@dynamicQuery)
END

END

DECLARE @strDueDateUpdate1 NVARCHAR(MAX) = ''
DECLARE @strDueDateUpdate2 NVARCHAR(MAX) = ''

SET @strDueDateUpdate1=' UPDATE #tempRecords SET DueDate = 
CASE 
WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getutcdate())) 
then CONCAT(''<b><font color="#FF0000" style="font-size:14px">Today''
	,CASE 
		WHEN ISNULL(bitFollowUpAnyTime,0)=1 
		THEN '' <i>Any time</i>'' 
		WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL 
		THEN  '' @ ''+LEFT(RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7))-1) 
		WHEN Startdate IS NOT NULL AND EndTime IS NULL THEN  '' @ ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7) 
		ELSE '''' 
	END
	,CASE 
		WHEN ISNULL(bitFollowUpAnyTime,0)=0 AND Startdate IS NOT NULL AND EndTime IS NOT NULL 
		THEN  ''<Span style="font-size:14px;font-weight:normal;font-style: italic;"><br/>to ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,EndTime,100),8)),7)+''</Span>'' 
		ELSE '''' 
	END ,''</font></b>'') 
WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(day,1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getutcdate()))) 
THEN CONCAT(''<b><font color=#ED8F11 style="font-size:14px">Tommorow''
	,CASE 
		WHEN ISNULL(bitFollowUpAnyTime,0)=1 
		THEN '' <i>Any time</i>''  
		WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL 
		THEN  '' @ ''+LEFT(RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7))-1) 
		WHEN Startdate IS NOT NULL AND EndTime IS NULL 
		THEN  '' @ ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7) 
		ELSE '''' 
	END
	,CASE 
		WHEN ISNULL(bitFollowUpAnyTime,0)=0 AND Startdate IS NOT NULL AND EndTime IS NOT NULL 
		THEN  ''<Span style="font-size:14px;font-weight:normal;font-style: italic;"><br/>to ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,EndTime,100),8)),7)+''</Span>'' 
		ELSE '''' 
	END ,''</font></b>'') 
WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(day,2,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getutcdate()))) then CONCAT(''<b><font color=#8FAADC style="font-size:14px">'',datename(dw,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',dtDueDate)),''<span style="color:#000;font-weight:500"> - '',CAST(DAY(Startdate) AS VARCHAR(10)),CASE
	WHEN DAY(Startdate) % 100 IN (11, 12, 13) THEN ''th''
	WHEN DAY(Startdate) % 10 = 1 THEN ''st''
	WHEN DAY(Startdate) % 10 = 2 THEN ''nd''
	WHEN DAY(Startdate) % 10 = 3 THEN ''rd''
ELSE ''th'' END,''</span>'',''</font></b>'',''<b>'',''<br/>''
,''<Span style="font-size:14px;font-weight:normal;font-style: italic;">''
,CASE 
	WHEN ISNULL(bitFollowUpAnyTime,0)=1 
	THEN '' <i>Any time</i>'' 
	WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL 
	THEN  LEFT(RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7))-1) 
	WHEN Startdate IS NOT NULL AND EndTime IS NULL 
	THEN  RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7) 
	ELSE '''' 
END,''</span>''
,CASE WHEN ISNULL(bitFollowUpAnyTime,0)=0 AND Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  ''<Span style="font-size:14px;font-weight:normal;font-style: italic;"> to ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,EndTime,100),8)),7)+''</Span>'' ELSE '''' END,''</b>'')
WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(day,3,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getutcdate()))) then CONCAT(''<b><font color=#CC99FF style="font-size:14px">'',datename(dw,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',dtDueDate)),''<span style="color:#000;font-weight:500"> - '',CAST(DAY(Startdate) AS VARCHAR(10)),CASE
	WHEN DAY(Startdate) % 100 IN (11, 12, 13) THEN ''th''
	WHEN DAY(Startdate) % 10 = 1 THEN ''st''
	WHEN DAY(Startdate) % 10 = 2 THEN ''nd''
	WHEN DAY(Startdate) % 10 = 3 THEN ''rd''
ELSE ''th'' END,''</span>'',''</font></b>'',''<b>'',''<br/>''
,''<Span style="font-size:14px;font-weight:normal;font-style: italic;">''
,CASE 
	WHEN ISNULL(bitFollowUpAnyTime,0)=1 
	THEN '' <i>Any time</i>'' 
	WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL 
	THEN  LEFT(RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7))-1) 
	WHEN Startdate IS NOT NULL AND EndTime IS NULL THEN  RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7) 
	ELSE '''' 
END,''</span>''
,CASE WHEN ISNULL(bitFollowUpAnyTime,0)=0 AND Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  ''<Span style="font-size:14px;font-weight:normal;font-style: italic;"> to ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,EndTime,100),8)),7)+''</Span>'' ELSE '''' END ,''</b>'')
WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(day,4,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getutcdate()))) then CONCAT(''<b><font color=#AED495 style="font-size:14px">'',datename(dw,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',dtDueDate)),''<span style="color:#000;font-weight:500"> - '',CAST(DAY(Startdate) AS VARCHAR(10)),CASE
	WHEN DAY(Startdate) % 100 IN (11, 12, 13) THEN ''th''
	WHEN DAY(Startdate) % 10 = 1 THEN ''st''
	WHEN DAY(Startdate) % 10 = 2 THEN ''nd''
	WHEN DAY(Startdate) % 10 = 3 THEN ''rd''
ELSE ''th'' END,''</span>'',''</font></b>'',''<b>'',''<br/>'''

SET @strDueDateUpdate2 = ',''<Span style="font-size:14px;font-weight:normal;font-style: italic;">''
,CASE 
	WHEN ISNULL(bitFollowUpAnyTime,0)=1 
	THEN '' <i>Any time</i>'' 
	WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL 
	THEN  LEFT(RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7))-1) 
	WHEN Startdate IS NOT NULL AND EndTime IS NULL 
	THEN  RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7) 
	ELSE '''' 
END,''</span>''
,CASE WHEN ISNULL(bitFollowUpAnyTime,0)=0 AND Startdate IS NOT NULL AND EndTime IS NOT NULL THEN ''<Span style="font-size:14px;font-weight:normal;font-style: italic;"> to ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,EndTime,100),8)),7)+''</Span>'' ELSE '''' END ,''</b>'')
WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(day,5,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getutcdate()))) then CONCAT(''<b><font color=#72DFDC style="font-size:14px">'',datename(dw,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',dtDueDate)),''<span style="color:#000;font-weight:500"> - '',CAST(DAY(Startdate) AS VARCHAR(10)),CASE
	WHEN DAY(Startdate) % 100 IN (11, 12, 13) THEN ''th''
	WHEN DAY(Startdate) % 10 = 1 THEN ''st''
	WHEN DAY(Startdate) % 10 = 2 THEN ''nd''
	WHEN DAY(Startdate) % 10 = 3 THEN ''rd''
ELSE ''th'' END,''</span>'',''</font></b>'',''<b>'',''<br/>''
,''<Span style="font-size:14px;font-weight:normal;font-style: italic;">''
,CASE 
	WHEN ISNULL(bitFollowUpAnyTime,0)=1 THEN '' <i>Any time</i>'' 
	WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL 
	THEN  LEFT(RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7))-1) 
	WHEN Startdate IS NOT NULL AND EndTime IS NULL 
	THEN  RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7) 
	ELSE '''' 
END,''</span>''
,CASE WHEN ISNULL(bitFollowUpAnyTime,0)=0 AND Startdate IS NOT NULL AND EndTime IS NOT NULL THEN ''<Span style="font-size:14px;font-weight:normal;font-style: italic;"> to ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,EndTime,100),8)),7)+''</Span>'' ELSE '''' END,''</b>'')
WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(day,6,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getutcdate()))) then CONCAT(''<b><font color=#FF9999 style="font-size:14px">'',datename(dw,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',dtDueDate)),''<span style="color:#000;font-weight:500"> - '',CAST(DAY(Startdate) AS VARCHAR(10)),CASE
	WHEN DAY(Startdate) % 100 IN (11, 12, 13) THEN ''th''
	WHEN DAY(Startdate) % 10 = 1 THEN ''st''
	WHEN DAY(Startdate) % 10 = 2 THEN ''nd''
	WHEN DAY(Startdate) % 10 = 3 THEN ''rd''
ELSE ''th'' END,''</span>'',''</font></b>'',''<b>'',''<br/>''
,''<Span style="font-size:14px;font-weight:normal;font-style: italic;">''
,CASE 
	WHEN ISNULL(bitFollowUpAnyTime,0)=1 THEN '' <i>Any time</i>'' 
	WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL 
	THEN  LEFT(RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7))-1) 
	WHEN Startdate IS NOT NULL AND EndTime IS NULL 
	THEN  RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7) 
	ELSE '''' 
END,''</span>''
,CASE WHEN ISNULL(bitFollowUpAnyTime,0)=0 AND Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  ''<Span style="font-size:14px;font-weight:normal;font-style: italic;"> to ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,EndTime,100),8)),7)+''</Span>'' ELSE '''' END ,''</b>'') 
ELSE CONCAT(''<span style="color:#909090;font-size:14px;font-weight:bold">'',(SELECT FORMAT(CAST(DueDate AS DATE), ''MMM-dd-yyyy''))
,CASE 
	WHEN ISNULL(bitFollowUpAnyTime,0)=1 
	THEN '' <i>Any time</i>''
	WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL 
	THEN  '' @ ''+LEFT(RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7))-1) 
	WHEN Startdate IS NOT NULL AND EndTime IS NULL THEN  '' @ ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7) 
	ELSE '''' 
END,CASE WHEN ISNULL(bitFollowUpAnyTime,0)=0 AND Startdate IS NOT NULL AND EndTime IS NOT NULL 
THEN  ''<Span style="font-size:14px;font-weight:normal;font-style: italic;"><br/>to ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,EndTime,100),8)),7)+''</Span>'' 
ELSE '''' 
END,''</span>'') END  '
 EXEC (@strDueDateUpdate1 + @strDueDateUpdate2)
 
 DECLARE @strSql3 VARCHAR(MAX)=''
 IF LEN(@columnName) > 0
BEGIN
	SET @strSql3= CONCAT(@strSql3,' order by ',CASE WHEN @columnName = 'Action-Item Participants' THEN CONCAT('[','Action-Item Participants',']') WHEN @columnName='DueDate' THEN 'dtDueDate' ELSE @columnName END,' ',@columnSortOrder)
END
ELSE
BEGIN
	SET @strSql3= CONCAT(@strSql3,' ORDER BY dtDueDate ASC  ')
END
SET @TotalRecordCount=(SELECT ISNULL(SUM(DISTINCT RecordCount),0) FROM #tempRecords)

SET @dynamicQuery = 'SELECT '+CAST(@TotalRecordCount AS varchar(400))+' AS TotalRecords,T.*
 FROM #tempRecords T WHERE 1=1 '+@strSql3
--SET @dynamicQuery = 'SELECT '+CAST(@TotalRecordCount AS varchar(400))+' AS TotalRecords,T.*
-- FROM #tempRecords T WHERE 1=1 '+@strSql3

 PRINT @dynamicQuery
EXEC(@dynamicQuery)
DECLARE @TempColumns TABLE
	(
        vcFieldName VARCHAR(200)
        ,vcDbColumnName VARCHAR(200)
        ,vcOrigDbColumnName VARCHAR(200)
        ,bitAllowSorting BIT
        ,numFieldId INT
        ,bitAllowEdit BIT
        ,bitCustomField BIT
        ,vcAssociatedControlType VARCHAR(50)
        ,vcListItemType  VARCHAR(200)
        ,numListID INT
        ,ListRelID INT
        ,vcLookBackTableName VARCHAR(200)
        ,bitAllowFiltering BIT
        ,vcFieldDataType  VARCHAR(20)
		,intColumnWidth INT
		,bitClosedColumn BIT
		,bitFieldMessage BIT DEFAULT 0
		,vcFieldMessage VARCHAR(500)
		,bitIsRequired BIT DEFAULT 0
		,bitIsNumeric BIT DEFAULT 0
		,bitIsAlphaNumeric BIT DEFAULT 0
		,bitIsEmail BIT DEFAULT 0
		,bitIsLengthValidation  BIT DEFAULT 0
		,intMaxLength INT DEFAULT 0
		,intMinLength INT DEFAULT 0
	)



	DECLARE @Nocolumns TINYINT
	SET @Nocolumns=0       
         
	SELECT 
		@Nocolumns=COUNT(*) 
	FROM 
		View_DynamicColumns 
	WHERE 
		numFormId=43 
		AND numUserCntID=@numUserCntID 
		AND numDomainID=@numDomainID 
		AND tintPageType=1 
  
  
if @Nocolumns > 0            
begin      
INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcFieldDataType,vcOrigDbColumnName,bitAllowFiltering,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
 FROM View_DynamicColumns 
 where numFormId=43 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
  order by tintOrder asc  
  
end            
else            
begin 

	--Check if Master Form Configuration is created by administrator if NoColumns=0
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 43 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END

	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			43,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,NULL,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=43 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0

		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,vcFieldDataType,vcOrigDbColumnName,bitAllowFiltering, ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType,numListID,vcLookBackTableName, bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=43 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	ORDER BY 
		tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		 INSERT INTO #tempForm
		select tintOrder,vcDbColumnName,vcFieldDataType
		,vcOrigDbColumnName,bitAllowFiltering,ISNULL(vcCultureFieldName,vcFieldName),                  
		 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
		,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
		 FROM View_DynamicDefaultColumns
		 where numFormId=43 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 AND numDomainID=@numDomainID
		order by tintOrder asc  
	END 
enD

INSERT INTO #tempForm
	SELECT 
		tintOrder,vcDbColumnName,vcFieldDataType
		,vcOrigDbColumnName,bitAllowFiltering,ISNULL(vcCultureFieldName,vcFieldName),                  
		 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
		,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
	FROM 
		View_DynamicDefaultColumns
	WHERE 
		numFormId=43 
		AND bitDefault=1 
		AND ISNULL(bitSettingField,0)=1 
		AND numDomainID=@numDomainID
		AND numFieldID NOT IN (SELECT numFieldId FROM #tempForm)
	ORDER BY 
		tintOrder asc  
			
UPDATE
		TC
	SET
		TC.intColumnWidth = (CASE WHEN TC.vcOrigDbColumnName = 'DueDate' AND ISNULL(DFCD.intColumnWidth,0) < 180 THEN 180 ELSE ISNULL(DFCD.intColumnWidth,0) END)
	FROM
		#tempForm TC
	INNER JOIN
		DycFormConfigurationDetails DFCD
	ON
		TC.numFieldId = DFCD.numFieldId
	WHERE
		DFCD.numDomainId = @numDomainID
		AND DFCD.numUserCntID = @numUserCntID
		AND DFCD.numFormId = 43
		AND DFCD.tintPageType = 1 


	SELECT * FROM #tempForm order by tintOrder asc  
END
GO
