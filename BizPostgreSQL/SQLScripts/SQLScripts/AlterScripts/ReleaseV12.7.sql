/******************************************************************
Project: Release 12.7 Date: 17.SEP.2019
Comments: ALTER SCRIPTS
*******************************************************************/


/******************************************** SANDEEP *********************************************/

DECLARE @numFieldID NUMERIC(18,0)
SET @numFieldID = (SELECT TOP 1 numFieldID FROM DycFieldMaster WHERE vcOrigDbCOlumnName='numRemainingQty' AND vcLookBackTableName='OpportunityItems')

DECLARE @numFormFieldID NUMERIC(18,0)
INSERT INTO DynamicFormFieldMaster
(
	numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,numListID,bitDeleted,vcFieldDataType,bitAllowEdit,bitDefault,bitInResults,bitSettingField,vcDbColumnName
)
VALUES
(
	7,'Remaining','R','Label',0,0,'N',0,0,1,1,''
)

SET @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,numFormFieldID
)
VALUES
(
	3,@numFieldID,7,0,0,'Remaining','Label',1,0,0,@numFormFieldID
)

--------------------------------

ALTER TABLE MassPurchaseFulfillmentConfiguration ADD bitShowOnlyFullyReceived BIT

-----------------------------------

SET IDENTITY_INSERT ReportFieldGroupMaster ON

INSERT INTO ReportFieldGroupMaster
(
	numReportFieldGroupID,vcFieldGroupName,bitActive
)
VALUES
(
	35,'Associated Contacts',1
)

SET IDENTITY_INSERT ReportFieldGroupMaster OFF

--------------------------------------------------

INSERT INTO ReportFieldGroupMappingMaster
(
	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
)
VALUES
(35,3,'(AC) Organization','TextBox','V',1,1,0,1)
,(35,51,'(AC) First Name','TextBox','V',1,1,0,1)
,(35,52,'(AC) Last Name','TextBox','V',1,1,0,1)
,(35,53,'(AC) Email','TextBox','V',1,1,0,1)
,(35,59,'(AC) Phone','TextBox','V',1,1,0,1)
,(35,60,'(AC) Ext','TextBox','V',1,1,0,1)
,(35,347,'(AC) Contact Role','SelectBox','N',1,1,0,1)

-----------------------------------

INSERT INTO ReportModuleGroupFieldMappingMaster
(
	numReportModuleGroupID,numReportFieldGroupID
)
VALUES
(8,35)
,(9,35)
,(10,35)
,(13,35)

---------------------------

UPDATE DycFieldMaster SET vcDbColumnName='numRole',vcOrigDbColumnName='numRole'  WHERE numFieldId=347


/******************************************** PRASANT *********************************************/

ALTER TABLE SalesOrderConfiguration ADD bitDisplayExpectedDate BIT