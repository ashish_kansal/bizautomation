/******************************************************************
Project: Release 12.9 Date: 09.NOV.2019
Comments: STORE PROCEDURES
*******************************************************************/

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ActivityAttendees')
DROP PROCEDURE USP_ActivityAttendees
GO

CREATE PROCEDURE [dbo].[USP_ActivityAttendees]
    @strRow AS TEXT = '' ,
	@numActivityID AS NUMERIC(9) = 0
AS 
BEGIN                   
BEGIN TRY 
	
	IF ISNULL(CAST(@strRow AS VARCHAR),'') <> '' AND CHARINDEX('<?xml',@strRow) = 0
	BEGIN
		SET @strRow = CONCAT('<?xml version="1.0" encoding="ISO-8859-1"?>',@strRow)
	END

      
    IF CONVERT(VARCHAR(100), @strRow) <> '' 
    BEGIN
        DECLARE @hDoc3 INT                                                                                                                                                                
        EXEC sp_xml_preparedocument @hDoc3 OUTPUT, @strRow                                                                                                             

		CREATE TABLE #temp(EmailId varchar(50),ResponseStatus varchar(20))


		INSERT INTO #temp(EmailId,ResponseStatus)
        SELECT 
			EmailId,
			ResponseStatus
		FROM 
			OPENXML (@hdoc3,'/ArrayOfAttendee/Attendee',2)
		WITH 
		(
			EmailId varchar(50)
			,ResponseStatus varchar(20)
		)

        EXEC sp_xml_removedocument @hDoc3 
                                    

/*-----------------------Validation of balancing entry*/
Delete From ActivityAttendees where ActivityID= @numActivityID
		IF EXISTS(SELECT * FROM #temp)
		BEGIN
			DECLARE @numDomainID NUMERIC(18,0)

			SELECT
				@numDomainID=res.numDomainId
			FROM 
				Activity ac 
			INNER JOIN             
				activityresource ar 
			ON 
				ac.activityid=ar.activityid             
			INNER JOIN 
				[resource] res 
			ON 
				ar.resourceid=res.resourceid
			WHERE
				ac.ActivityID=@numActivityID

		
			INSERT INTO ActivityAttendees
			(
				ActivityID,
				ResponseStatus,
				AttendeeEmail,
				AttendeeFirstName,
				AttendeeLastName,
				AttendeePhone,
				AttendeePhoneExtension,
				CompanyNameinBiz,
				DivisionID,
				tintCRMType,
				ContactID
			)
			SELECT 
				@numActivityID
				,T.ResponseStatus
				,T.EmailId
				,Ac.vcFirstName
				,Ac.vcLastName
				,Ac.numPhone
				,Ac.numPhoneExtension
				,CI.numCompanyId
				,AC.numDivisionId
				,Div.tintCRMType
				,AC.numContactId
			From 
				#temp T
			left Join 
				AdditionalContactsInformation AC 
			on 
				numContactId = (select MAX(numContactId) From AdditionalContactsInformation where vcEmail=T.EmailId AND numDomainID=@numDomainID)
			left join 
				DivisionMaster Div 
			on 
				AC.numDivisionId=Div.numDivisionId
			left join 
				CompanyInfo CI 
			on 
				Div.numCompanyID=CI.numCompanyID
		END
	
END
END TRY 
BEGIN CATCH		
   RollBack
END CATCH	

            
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ChartOfAccounts_SaveAccountTypeOrder')
DROP PROCEDURE USP_ChartOfAccounts_SaveAccountTypeOrder
GO
CREATE PROCEDURE [dbo].[USP_ChartOfAccounts_SaveAccountTypeOrder]    
(
	@numDomainID NUMERIC(18,0)
	,@vcAccountTypes VARCHAR(MAX)
)
AS
BEGIN
	DECLARE @hDocItem int

	IF LEN(ISNULL(@vcAccountTypes,'')) > 0
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcAccountTypes

		UPDATE
			ATD
		SET
			ATD.tintSortOrder = T1.tintSortOrder
		FROM
			AccountTypeDetail ATD
		INNER JOIN
		(
			SELECT
				numAccountTypeID,
				tintSortOrder
			FROM
				OPENXML (@hDocItem,'/NewDataSet/AccountType',2)
			WITH
			(
				numAccountTypeID NUMERIC(18,0),
				tintSortOrder INT
			)
		) AS T1
		ON 
			ATD.numAccountTypeID = T1.numAccountTypeID
		WHERE
			ATD.numDomainID = @numDomainID

		EXEC sp_xml_removedocument @hDocItem 
	END

	
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteDomain')
DROP PROCEDURE USP_DeleteDomain
GO
-- exec USP_DeleteDomain 82
-- exec USP_DeleteDomain 153
-- 72,97,103,104,107,110
CREATE PROCEDURE USP_DeleteDomain
@numDomainID NUMERIC(9)
AS 
BEGIN
IF @numDomainID=1
BEGIN
	raiserror('Verry funny.. can''t delete yourself',16,1);
	RETURN ;
END

/*Allow deletion only for suspended subscription*/
IF exists (SELECT * FROM [Subscribers] WHERE bitActive = 1 AND numTargetDomainID = @numDomainID)
BEGIN
	raiserror('Can not DELETE active subscription',16,1);
	RETURN ;
END

BEGIN TRY 
	BEGIN TRANSACTION

			--DELETE FROM [OpportunityAddress] WHERE [numDomainId]=@numDomainID;

			DELETE FROM [General_Journal_Details] WHERE numJournalId IN (SELECT numJournal_Id FROM dbo.General_Journal_Header WHERE numDomainId = @numDomainID)
			DELETE FROM [General_Journal_Header] WHERE [numDomainId]=@numDomainID;


			--Project Management
			---------------------------------------------------------
			DELETE FROM ProjectProgress WHERE numDomainId=@numDomainID
			 DELETE FROM [ProjectsOpportunities] WHERE [numDomainId]=@numDomainID
			 delete RECENTITEMS where numRecordID IN (SELECT [numProId] FROM [ProjectsMaster] WHERE numDomainID = @numDomainID) and chrRecordType='P'       
			 delete from ProjectsStageDetails where numProId IN (SELECT [numProId] FROM [ProjectsMaster] WHERE numDomainID = @numDomainID)        
			 delete from ProjectsSubStageDetails where numProId IN (SELECT [numProId] FROM [ProjectsMaster] WHERE numDomainID = @numDomainID)              
			 delete from TimeAndExpense where numProId IN (SELECT [numProId] FROM [ProjectsMaster] WHERE numDomainID = @numDomainID)        
			 delete from ProjectsDependency where numProId IN (SELECT [numProId] FROM [ProjectsMaster] WHERE numDomainID = @numDomainID)        
			 delete from ProjectsContacts where numProId IN (SELECT [numProId] FROM [ProjectsMaster] WHERE numDomainID = @numDomainID)         
			 delete from ProjectsMaster WHERE numDomainID = @numDomainID  


			--Case Management
			---------------------------------------------------------
			 DELETE FROM [CaseOpportunities] WHERE [numDomainId] =@numDomainID
			 delete recentItems where numRecordID IN ( SELECT numCaseID FROM Cases WHERE numDomainID=@numDomainID) and chrRecordType = 'S'
			 delete from TimeAndExpense where  numCaseId IN ( SELECT numCaseID FROM Cases WHERE numDomainID=@numDomainID) and numDomainID=   @numDomainID  
			 delete from CaseContacts where numCaseID IN ( SELECT numCaseID FROM Cases WHERE numDomainID=@numDomainID)      
			 delete from CaseSolutions where numCaseID IN ( SELECT numCaseID FROM Cases WHERE numDomainID=@numDomainID)      
			 DELETE from Cases where numDomainID=@numDomainID

			--Contract 
			---------------------------------------------------------

			DELETE FROM [ContractsContact] WHERE [numDomainId]=@numDomainID
			delete contractManagement where numDomainID=@numDomainID

			--Solution/KnowledgeBase
			---------------------------------------------------------
			delete from CaseSolutions where [numSolnID] IN ( SELECT numSolnID FROM [SolutionMaster] WHERE numDomainID=@numDomainID)      
			DELETE FROM [SolutionMaster] WHERE [numDomainID]=@numDomainID


			--Document Management
			---------------------------------------------------------
			DELETE FROM 	DocumentWorkflow	 WHERE [numContactID] IN (SELECT [numContactId] FROM [AdditionalContactsInformation] WHERE [numDomainID]=@numDomainID)
			DELETE FROM [GenericDocuments] WHERE [numDomainID]=@numDomainID
			delete from SpecificDocuments where [numDomainID]=@numDomainID

			--Outlook
			---------------------------------------------------------
			DELETE FROM 	ProfileEGroupDTL	 WHERE [numContactID] IN (SELECT [numContactId] FROM [AdditionalContactsInformation] WHERE [numDomainID]=@numDomainID)
			DELETE FROM 	ProfileEGroupDTL	 WHERE [numEmailGroupID] IN (SELECT [numEmailGroupID] FROM  ProfileEmailGroup	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	ProfileEmailGroup	 WHERE numDomainID = @numDomainID
			DELETE FROM [Correspondence] WHERE [numDomainID]=@numDomainID /***/
			DELETE FROM [Correspondence] WHERE [numEmailHistoryID] IN(select numEmailHSTRID from emailhistory where [numDomainID] =@numDomainID)
			DELETE FROM [EmailMaster] WHERE [numEmailId] IN (select [numEmailId] from EmailHStrToBCCAndCC where [numEmailHstrID] in(select numEmailHSTRID from emailhistory where [numDomainID] =@numDomainID) )
			DELETE FROM [EmailHstrAttchDtls] WHERE [numEmailHstrID] IN (select numEmailHSTRID from emailhistory where [numDomainID] =@numDomainID)
			delete EmailHStrToBCCAndCC where numEmailHSTRID in(select numEmailHSTRID from emailhistory where [numDomainID] =@numDomainID)
			delete [EmailHistory] where [numDomainID] =@numDomainID

			--Marketing/Campaign Mgmt
			---------------------------------------------------------
			DELETE FROM [CampaignDetails] WHERE [numCampaignId] IN (SELECT [numCampaignId] FROM [CampaignMaster] WHERE [numDomainID]=@numDomainID)
			delete from CampaignMaster where numDomainID= @numDomainID

			delete from  ConECampaignDTL where numECampDTLID in (select numECampDTLId from ECampaignDTLs where numECampID IN (SELECT [numECampaignID] FROM [ECampaign] WHERE [numDomainID]=@numDomainID) )
			delete from  ConECampaign where numECampaignID IN (SELECT [numECampaignID] FROM [ECampaign] WHERE [numDomainID]=@numDomainID)
			DELETE FROM [ECampaignAssignee] WHERE [numDomainID]=@numDomainID
			DELETE FROM [ECampaignDTLs] WHERE [numECampID] IN (SELECT [numECampaignID] FROM [ECampaign] WHERE [numDomainID]=@numDomainID)
			DELETE FROM [ECampaign] WHERE [numDomainID]=@numDomainID
			
			DELETE FROM 	BroadCastDtls	 WHERE [numBroadCastID] IN (SELECT [numBroadCastId] FROM [Broadcast] WHERE [numDomainID]=@numDomainID)
			DELETE FROM 	Broadcast	 WHERE numDomainID = @numDomainID

			--Survey
			---------------------------------------------------------
			 --First Delete All Survey Responses related to this Survey.      
			 DELETE FROM dbo.SurveyTemplate WHERE numDomainId=@numDomainID
			 
			 DELETE FROM SurveyResponse       
			  WHERE numRespondantID IN       
			   (SELECT numRespondantID       
				FROM SurveyRespondentsMaster       
				 WHERE numSurID IN (SELECT [numSurID] FROM [SurveyMaster] WHERE [numDomainID]=@numDomainID) )      
			    
			 --Delete all Survey Resposen for the Current Survey   
			 DELETE FROM  SurveyResponse  
			  WHERE numSurID IN (SELECT [numSurID] FROM [SurveyMaster] WHERE [numDomainID]=@numDomainID)
			  
			 --Then Delete All Survey Respondants related to this Survey.      
			 DELETE FROM  SurveyRespondentsChild  
			  WHERE numSurID IN (SELECT [numSurID] FROM [SurveyMaster] WHERE [numDomainID]=@numDomainID)
			    
			 DELETE FROM  SurveyRespondentsMaster  
			  WHERE numSurID IN (SELECT [numSurID] FROM [SurveyMaster] WHERE [numDomainID]=@numDomainID)    
			  
			 --Deletes the Rules for the Survey Answers    
			 DELETE FROM SurveyWorkflowRules    
			  WHERE numSurID IN (SELECT [numSurID] FROM [SurveyMaster] WHERE [numDomainID]=@numDomainID)
			    
			 --Then Delete All Survey Ans Master related to this Survey.      
			 DELETE FROM SurveyAnsMaster       
			  WHERE numSurID IN (SELECT [numSurID] FROM [SurveyMaster] WHERE [numDomainID]=@numDomainID)
			    
			 --Then Delete All Survey Question Master related to this Survey.      
			 DELETE FROM SurveyQuestionMaster       
			  WHERE numSurID IN (SELECT [numSurID] FROM [SurveyMaster] WHERE [numDomainID]=@numDomainID)
			    
			 --Then Delete The Survey from the Survey Master.      
			 DELETE FROM SurveyMaster       
			  WHERE  numDomainId = @numDomainID  


			--E-Commerce
			---------------------------------------------------------
			DELETE FROM dbo.ReviewDetail WHERE numReviewId IN (SELECT numReviewId FROM dbo.Review WHERE numDomainId = @numDomainID)
			DELETE FROM dbo.Review WHERE numDomainId = @numDomainID
			
			DELETE FROM Ratings WHERE numDomainId = @numDomainID
			
			DELETE FROM 	WebAPIDetail	 WHERE numDomainID = @numDomainID
			DELETE FROM 	WebService	 WHERE numDomainID = @numDomainID
			DELETE FROM 	SiteWiseCategories	 WHERE [numSiteID] IN (SELECT [numSiteID] FROM [Sites] WHERE [numDomainID]=@numDomainID)
			DELETE FROM 	MetaTags	 WHERE [tintMetatagFor]=1 AND [numReferenceID] IN (SELECT numitemcode FROM item WHERE [numDomainID]=@numDomainID)
			DELETE FROM SiteSubscriberDetails WHERE [numSiteID] IN (SELECT [numSiteID] FROM [Sites] WHERE [numDomainID]=@numDomainID)
			DELETE FROM [SiteBreadCrumb] WHERE [numDomainID] = @numDomainID
			DELETE FROM [StyleSheetDetails] WHERE [numCssID] IN (SELECT numCssID FROM   [StyleSheets] WHERE  [numDomainID] = @numDomainID)
			DELETE FROM [StyleSheets] WHERE [numDomainID] = @numDomainID
			DELETE FROM [SiteTemplates] WHERE numSiteID IN (SELECT numSiteID FROM Sites WHERE numDomainID = @numDomainID)
			DELETE FROM [PageElementDetail] WHERE   [numDomainID] = @numDomainID
			DELETE FROM [SiteMenu] WHERE [numDomainID] = @numDomainID
			DELETE FROM [SitePages] WHERE numSiteID IN (SELECT numSiteID FROM Sites WHERE numDomainID = @numDomainID)
			DELETE FROM [SiteCategories] WHERE [numSiteID] IN (SELECT [numSiteID] FROM [Sites] WHERE [numDomainID]=@numDomainID)

			delete from ShippingRules WHERE numDomainID = @numDomainID
			DELETE FROM Sites WHERE numDomainID = @numDomainID
			
			DELETE FROM dbo.PromotionOfferItems WHERE numProId IN (SELECT numProId FROM dbo.PromotionOffer WHERE numDomainId=@numDomainID)
			DELETE FROM dbo.PromotionOfferContacts WHERE numProId IN (SELECT numProId FROM dbo.PromotionOffer WHERE numDomainId=@numDomainID)
			DELETE FROM PromotionOffer WHERE numDomainId=@numDomainID


			--Module-Opportunity,Shipping 
			---------------------------------------------------------
			
			delete from CreditBalanceHistory WHERE numDomainId = @numDomainID
--			where numReturnID IN (SELECT numReturnID FROM dbo.[Returns] WHERE numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID))
--			AND numOppBizDocsId in (select numOppBizDocsId FROM OpportunityBizDocs where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID))
			DELETE FROM TransactionHistory WHERE numDomainID = @numDomainID
			
			DELETE FROM 	OrderAutoRuleDetails	 WHERE [numRuleID] IN (SELECT [numRuleID] FROM [OrderAutoRule] WHERE [numDomainID]=@numDomainID)
			DELETE FROM 	OrderAutoRule	 WHERE numDomainID = @numDomainID

			DELETE FROM 	InventroyReportDTL	 WHERE numDomainID = @numDomainID
			DELETE FROM 	BizDocStatusApprove	 WHERE numDomainID = @numDomainID
			DELETE FROM 	BizDocApprovalRuleEmployee	 WHERE [numBizDocAppId] IN (SELECT [numBizDocAppId] FROM [BizDocApprovalRule] WHERE [numDomainID]=@numDomainID)
			DELETE FROM 	BizDocApprovalRule	 WHERE numDomainID = @numDomainID
			DELETE FROM 	BizActionDetails	 WHERE [numBizActionId] IN (SELECT [numBizActionId] FROM  BizDocAction	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	BizDocAction	 WHERE numDomainID = @numDomainID
			DELETE FROM 	BizDocComission	 WHERE numDomainID = @numDomainID

			delete from Comments where numStageID in (select numStageDetailsId from StagePercentageDetails	 WHERE numDomainID = @numDomainID)
			delete from StageDependency where numDependantOnID in (select numStageDetailsId from StagePercentageDetails	 WHERE numDomainID = @numDomainID)

			  DELETE FROM 	StagePercentageDetails	 WHERE numDomainID = @numDomainID
			  DELETE FROM 	[SubStageDetails]	 WHERE [numSubStageHdrID] IN (SELECT [numSubStageHdrID] FROM [SubStageHDR]	 WHERE [numProcessId] IN (SELECT [Slp_Id] FROM  Sales_process_List_Master	 WHERE numDomainID = @numDomainID))
			  DELETE FROM 	[SubStageHDR]	 WHERE [numProcessId] IN (SELECT [Slp_Id] FROM  Sales_process_List_Master	 WHERE numDomainID = @numDomainID)


			
			DELETE FROM dbo.EmbeddedCostItems WHERE numDomainID=@numDomainID
			  DELETE FROM dbo.EmbeddedCostDefaults WHERE numDomainID=@numDomainID	
			  DELETE FROM EmbeddedCostConfig WHERE numDomainID=@numDomainID
				DELETE FROM EmbeddedCost WHERE numDomainID=@numDomainID
			  DELETE FROM  ShippingProviderForEComerce WHERE [numDomainID]=@numDomainID 
			  DELETE FROM [ShippingFieldValues] WHERE [numDomainID]=@numDomainID 
			 
			  DELETE FROM [ShippingReportItems] WHERE [numItemCode] IN (SELECT numItemCode FROM Item WHERE numDomainID =@numDomainID)/***/
			  DELETE FROM [ShippingReportItems] WHERE [numShippingReportId] IN (select numShippingReportId from [ShippingReport] WHERE numOppBizDocId in (select numOppBizDocId FROM OpportunityBizDocs where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)))

			  DELETE FROM [ShippingBox] WHERE [numShippingReportId] IN (SELECT numShippingReportId FROM [ShippingReport] WHERE [numDomainId] =@numDomainID)
			  DELETE FROM [ShippingBox] WHERE [numShippingReportId] IN (select numShippingReportId from [ShippingReport] WHERE numOppBizDocId in (select numOppBizDocId FROM OpportunityBizDocs where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)))
			
			  DELETE FROM [ShippingReport] WHERE [numDomainId] =@numDomainID	/***/
			  DELETE FROM [ShippingReport] WHERE numOppBizDocId in (select numOppBizDocId FROM OpportunityBizDocs where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID))

			  DELETE FROM SalesTemplateItems WHERE [numDomainID] =@numDomainID /***/
			  DELETE FROM [OpportunitySalesTemplate] WHERE [numDomainID]=@numDomainID
			  DELETE FROM [OpportunityLinking] WHERE [numParentOppBizDocID] IN (SELECT [numOppBizDocsId] FROM [OpportunityMaster] INNER JOIN [OpportunityBizDocs] ON [OpportunityMaster].[numOppId] = [OpportunityBizDocs].[numOppId] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM [OpportunityLinking] WHERE [numParentOppID] IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM [OpportunityLinking] WHERE [numChildOppID] IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM [OpportunityLinking] WHERE [numParentProjectID] IN (SELECT [numProId] FROM [ProjectsMaster] WHERE [numDomainId]=@numDomainID)
			  
			  DELETE FROM dbo.WorkOrderDetails WHERE numWOId IN (SELECT numWOId FROM dbo.WorkOrder WHERE numDomainID=@numDomainID)
			  DELETE FROM WorkOrder WHERE numDomainID=@numDomainID
			  
			  DELETE FROM RECENTITEMS where numRecordID in  (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID) and chrRecordType='O'
			      
			 
			  DELETE FROM 	PortalBizDocs	 WHERE numDomainID = @numDomainID
			  DELETE FROM 	PortalWorkflow	 WHERE numDomainID = @numDomainID
			  DELETE FROM 	BizDocAttachments	 WHERE numDomainID = @numDomainID
			  DELETE FROM 	WareHouseForSalesFulfillment	 WHERE numDomainID = @numDomainID
			  DELETE FROM 	OpportunityBizDocsPaymentDetails	 WHERE [numBizDocsPaymentDetId] IN (SELECT [numBizDocsPaymentDetId] FROM [OpportunityBizDocsDetails] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM 	OpportunityBizDocsDetails	 WHERE numDomainID = @numDomainID
			  DELETE FROM OppWarehouseSerializedItem where numOppID IN(SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM OpportunityItemLinking where numNewOppID IN(SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM OpportunityAddress  where numOppID IN(SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			                      
			  DELETE FROM OpportunityBizDocItems where numOppBizDocID in                        
			  (select numOppBizDocsId from OpportunityBizDocs where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID))

--			  DELETE FROM OpportunityBizDocTaxItems where numOppBizDocID in                        
--			  (select numOppBizDocsId from OpportunityBizDocs where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID))
--			
			  DELETE FROM OpportunityItemsTaxItems WHERE numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)                    
			  DELETE FROM OpportunityMasterTaxItems WHERE numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)                    

			  DELETE FROM OpportunityBizDocDtl where numBizDocID in                        
			  (select numOppBizDocsId from OpportunityBizDocs where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID))                        
			  
			  DELETE FROM 	OpportunityRecurring	 WHERE [numOppId] IN (SELECT numOppID FROM [OpportunityMaster] WHERE numDomainID = @numDomainID)
			  DELETE FROM 	RecurringTransactionReport	 WHERE [numRecTranSeedId] IN (SELECT numOppID FROM [OpportunityMaster] WHERE numDomainID = @numDomainID)
			  DELETE FROM 	RecurringTransactionReport	 WHERE [numRecTranOppID] IN (SELECT numOppID FROM [OpportunityMaster] WHERE numDomainID = @numDomainID)
			  --DELETE FROM OpportunityKitItems WHERE [numChildItem] IN (SELECT [numItemCode] FROM item WHERE [numDomainID]=@numDomainID)/***/
			  --DELETE FROM [OpportunityBizDocTaxItems] WHERE [numOppBizDocID] IN (SELECT [numOppBizDocsId] FROM [OpportunityBizDocs] WHERE [numOppId] IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID) ) /***/
			  
			  --'OpportunityOrderStatus','OpportunityMasterTaxItems','OpportunityItemsTaxItems'
			  DELETE FROM [OpportunityOrderStatus] WHERE numDomainID = @numDomainID
			  DELETE FROM [OpportunityMasterTaxItems] WHERE [numOppId] IN (SELECT numOppID FROM [OpportunityMaster] WHERE numDomainID = @numDomainID)
			  DELETE FROM [OpportunityItemsTaxItems]  WHERE [numOppItemID] IN (SELECT [numOppItemID] FROM [dbo].[OpportunityItems] WHERE [numOppId] IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID) )
			  
			  DELETE FROM OpportunityBizDocKitItems WHERE [numOppBizDocItemID] IN (SELECT [numOppBizDocItemID] FROM [OpportunityBizDocItems] WHERE [numItemCode] IN (SELECT numItemCode FROM item WHERE [numDomainID]=@numDomainID) ) /***/
			  DELETE FROM OpportunityBizDocs where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM OpportunityContact where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM OpportunityDependency where numOpportunityId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM 	TimeExpAddAmount	 WHERE [numUserCntID] IN (SELECT [numContactId] FROM [AdditionalContactsInformation] WHERE [numDomainID]=@numDomainID)
			  DELETE FROM TimeAndExpense where numDomainID= @numDomainID
			  --delete from CreditBalanceHistory where numDomainID= @numDomainID 
			   delete from OpportunityKitChildItems where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  delete from OpportunityKitItems where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)                        
 
			  DELETE FROM [Returns] WHERE [numOppId] IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM OpportunityItems where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM OpportunityItems where numItemCode IN (SELECT numItemCode FROM item WHERE [numDomainID]=@numDomainID)
			  
			  DELETE FROM OpportunityStageDetails WHERE numDomainId=@numDomainID
			  DELETE FROM OpportunityStageDetails where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM OpportunitySubStageDetails where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM [OpportunityRecurring] WHERE [numOppId] IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM [OpportunityMasterAPI] WHERE [numOppId] IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID;
			  DELETE FROM [OpportunityMaster] WHERE [numDivisionId] IN (SELECT numDivisionID FROM [DivisionMaster] WHERE [numDomainID]=@numDomainID)
			  DELETE FROM 	Sales_process_List_Master	 WHERE numDomainID = @numDomainID

			--Item Management,Pricebook,Category,Group,Assets
			---------------------------------------------------------
			DELETE FROM WebApiOppItemDetails WHERE numDomainId = @numDomainID
			
			DELETE FROM [PricingTable] WHERE [numPriceRuleID] IN (SELECT [numPricRuleID] FROM [PriceBookRules] WHERE [numDomainID]=@numDomainID)
			DELETE FROM [PriceBookRuleDTL] WHERE [numRuleID] IN (SELECT [numPricRuleID] FROM [PriceBookRules] WHERE [numDomainID]=@numDomainID)
			DELETE FROM [PriceBookRuleItems] WHERE [numRuleID] IN (SELECT [numPricRuleID] FROM [PriceBookRules] WHERE [numDomainID]=@numDomainID)
			DELETE FROM [PriceBookRules] WHERE [numDomainID]=@numDomainID         

			DELETE FROM [CompanyAssetSerial] WHERE [numAssetItemID] IN (SELECT [numAItemCode] FROM [CompanyAssets] WHERE [numDomainId]=@numDomainID)
			DELETE FROM [CompanyAssets] WHERE [numDomainId]=@numDomainID

			DELETE FROM [ItemCategory] WHERE [numItemID] IN (SELECT numItemCode FROM item WHERE [numDomainID]=@numDomainID)
			DELETE FROM [Category] WHERE [numDomainID] =@numDomainID

			DELETE FROM [ItemGroupsDTL] WHERE [numItemGroupID] IN (SELECT [numItemGroupID] FROM [ItemGroups] WHERE [numDomainID]=@numDomainID)
			DELETE FROM [ItemGroups] WHERE [numDomainID]=@numDomainID

			
			DELETE FROM [Vendor] WHERE [numDomainID] =@numDomainID
			DELETE FROM [Vendor] WHERE numItemCode IN (SELECT numItemCode FROM item WHERE [numDomainID]=@numDomainID)
			DELETE FROM [ItemDiscountProfile] WHERE [numItemID] IN (SELECT numItemCode FROM item WHERE [numDomainID]=@numDomainID)
			DELETE FROM [ItemExtendedDetails] WHERE [numItemCode] IN (SELECT numItemCode FROM item WHERE [numDomainID]=@numDomainID)/***/
			DELETE FROM [ItemDetails] WHERE [numItemKitID] IN (SELECT numItemCode FROM item WHERE [numDomainID]=@numDomainID)
			DELETE FROM [ItemDetails] WHERE [numChildItemID] IN (SELECT numItemCode FROM item WHERE [numDomainID]=@numDomainID) /***/
			DELETE FROM [ItemOppAccAttrDTL] WHERE [numOptAccAttrID] IN(SELECT [numItemAttrOptAccID] FROM  [ItemOptionAccAttr] WHERE [numDomainID]=@numDomainID)/***/
			DELETE FROM [ItemOptionAccAttr] WHERE [numDomainID]=@numDomainID
			DELETE FROM [ItemTax] WHERE [numItemCode] IN (SELECT numItemCode FROM item WHERE [numDomainID]=@numDomainID) /***/
			DELETE FROM [TaxItems] WHERE [numDomainID]=@numDomainID

			DELETE FROM WareHouseItems_Tracking WHERE numDomainID=@numDomainID

			DELETE FROM warehouseItmsDTL WHERE numWareHouseItemID IN (SELECT numWareHouseItemID FROM WareHouseItems WHERE [numWareHouseID] IN (SELECT [numWareHouseID] FROM [Warehouses] WHERE [numDomainID]=@numDomainID)) /***/
			DELETE FROM [WareHouseDetails] WHERE [numDomainId]=@numDomainID
			delete from WareHouseItems WHERE [numDomainID]=@numDomainID
			DELETE FROM [Warehouses] WHERE [numDomainID]=@numDomainID

			Delete from ItemImages where [numDomainID]=@numDomainID
			delete from item where [numDomainID] = @numDomainID
			DELETE FROM [ItemAPI] WHERE  [numDomainId] = @numDomainID
			DELETE FROM LuceneItemsIndex WHERE [numDomainId] = @numDomainID

			--Administration (custom fiels,master lists,alerts)
			---------------------------------------------------------
			DELETE FROM 	FieldRelationshipDTL	 WHERE [numFieldRelID] IN (SELECT [numFieldRelID] FROM [FieldRelationship] WHERE [numDomainID] = @numDomainID)
			DELETE FROM 	FieldRelationship	 WHERE numDomainID = @numDomainID
			DELETE FROM 	Currency	 WHERE numDomainID = CASE @numDomainID WHEN 0 THEN -1 ELSE @numDomainID END 
			delete from VendorShipmentMethod where numDomainID = @numDomainID
			DELETE FROM 	ListMaster	 WHERE numDomainID = @numDomainID
			DELETE FROM 	ListDetails	 WHERE numDomainID = @numDomainID
			DELETE FROM 	TaxDetails	 WHERE numDomainID = @numDomainID
			DELETE FROM 	ExceptionLog	 WHERE numDomainID = @numDomainID
			DELETE FROM 	PaymentGatewayDTLID	 WHERE numDomainID = @numDomainID
			--DELETE FROM 	DomainWiseSort	 WHERE numDomainID = @numDomainID
			DELETE FROM 	ShortCutBar	 WHERE numDomainID = @numDomainID
			DELETE FROM 	TabDefault	 WHERE numDomainID = @numDomainID
			DELETE FROM 	AuthoritativeBizDocs	 WHERE numDomainID = @numDomainID
			DELETE FROM 	TabMaster	 WHERE numDomainID = @numDomainID
			DELETE FROM 	ListOrder	 WHERE numDomainID = @numDomainID
			DELETE FROM 	InboxTree	 WHERE numDomainID = @numDomainID
			DELETE FROM 	ImapUserDetails	 WHERE numDomainID = @numDomainID
			DELETE FROM		NameTemplate WHERE numDomainID = CASE @numDomainID WHEN 0 THEN -1 ELSE @numDomainID END

			DELETE FROM 	ShortCutGrpConf	 WHERE numDomainID = @numDomainID			
			DELETE FROM 	ShortCutUsrCnf	 WHERE numDomainID = @numDomainID
			DELETE FROM 	DefaultTabs	 WHERE numDomainID = @numDomainID
			DELETE FROM 	GroupTabDetails	 WHERE [numGroupId] IN (SELECT [numGroupID] FROM AuthenticationGroupMaster WHERE [numDomainID]=@numDomainID )
			
		
			
			DELETE FROM 	RecImportConfg	 WHERE numDomainID = @numDomainID
			DELETE FROM 	PageLayoutDTL	 WHERE numDomainID = @numDomainID
			DELETE FROM 	RoutinLeadsValues	 WHERE [numRoutID] IN (SELECT [numRoutID] FROM RoutingLeads	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	RoutingLeadDetails	 WHERE [numRoutID] IN (SELECT [numRoutID] FROM RoutingLeads	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	RoutingLeads	 WHERE numDomainID = @numDomainID
			
			DELETE FROM 	SavedSearch WHERE numDomainID=@numDomainID
			DELETE FROM 	AdvSearchCriteria WHERE numDomainID =@numDomainID
			DELETE FROM 	AdvSerViewConf	 WHERE numDomainID = @numDomainID
			DELETE FROM 	UserRoles	 WHERE [numUserCntID] IN (SELECT [numContactId] FROM [AdditionalContactsInformation] WHERE [numDomainID]=@numDomainID)
			DELETE FROM 	UserTeams	 WHERE numDomainID = @numDomainID
			DELETE FROM 	UserTerritory	 WHERE [numDomainID]= @numDomainID
			DELETE FROM 	State	 WHERE numDomainID = @numDomainID
			--DELETE FROM 	HomePage	 WHERE numDomainID = @numDomainID
			DELETE FROM [Favorites] WHERE [numContactID] IN (SELECT [numContactId] FROM [AdditionalContactsInformation] WHERE [numDomainID]=@numDomainID)
			DELETE FROM Favorites	WHERE [numUserCntID] IN (SELECT [numContactId] FROM [AdditionalContactsInformation] WHERE [numDomainID]=@numDomainID)
			DELETE FROM HTMLFormURL WHERE numDomainId=@numDomainID
			
			DELETE FROM 	CFW_FLD_Values	 WHERE [Fld_ID] IN (SELECT [Fld_id] FROM CFW_Fld_Master	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	CFW_Fld_Values_Attributes	 WHERE [Fld_ID] IN (SELECT [Fld_id] FROM CFW_Fld_Master	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	CFW_FLD_Values_Case	 WHERE [Fld_ID] IN (SELECT [Fld_id] FROM CFW_Fld_Master	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	CFW_FLD_Values_Cont	 WHERE [Fld_ID] IN (SELECT [Fld_id] FROM CFW_Fld_Master	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	CFW_FLD_Values_Item	 WHERE [Fld_ID] IN (SELECT [Fld_id] FROM CFW_Fld_Master	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	CFW_Fld_Values_Opp	 WHERE [Fld_ID] IN (SELECT [Fld_id] FROM CFW_Fld_Master	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	CFW_Fld_Values_Opp_Items WHERE [Fld_ID] IN (SELECT [Fld_id] FROM CFW_Fld_Master	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	CFW_FLD_Values_Pro	 WHERE [Fld_ID] IN (SELECT [Fld_id] FROM CFW_Fld_Master	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	CFW_Fld_Values_Product	 WHERE [Fld_ID] IN (SELECT [Fld_id] FROM CFW_Fld_Master	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	CFW_Fld_Values_Serialized_Items	 WHERE [Fld_ID] IN (SELECT [Fld_id] FROM CFW_Fld_Master	 WHERE numDomainID = @numDomainID)
			--DELETE FROM 	InitialListColumnConf	 WHERE numDomainID = @numDomainID
			DELETE FROM 	RelationsDefaultFilter	 WHERE numDomainID = @numDomainID
			DELETE FROM 	DycFrmConfigBizDocsSumm	 WHERE numDomainID = @numDomainID
			DELETE FROM 	DynamicFormMasterParam	 WHERE numDomainID = @numDomainID
			
			DELETE FROM     DycField_Globalization WHERE numDomainID = @numDomainID
			DELETE FROM     DycField_Validation WHERE numDomainID = @numDomainID
			DELETE FROM 	DycFormConfigurationDetails	 WHERE numDomainID = @numDomainID
			DELETE FROM 	DycFormField_Mapping	 WHERE numDomainID = @numDomainID -- deletes custom field
			DELETE FROM		DycFieldMaster WHERE numDomainID = @numDomainID 

			DELETE FROM 	CFw_Grp_Master	 WHERE numDomainID = @numDomainID
			DELETE FROM 	CFW_Fld_Dtl	 WHERE [numFieldId] IN (SELECT [Fld_id] FROM CFW_Fld_Master	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	CFW_Fld_Master	 WHERE numDomainID = @numDomainID

			DELETE TreeNavigationAuthorization WHERE numDomainID = @numDomainID
			
			DELETE FROM 	GroupAuthorization	 WHERE [numGroupID] IN (SELECT [numGroupID] from AuthenticationGroupMaster	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	AuthenticationGroupMaster	 WHERE numDomainID = @numDomainID
			
			DELETE FROM 	AlertEmailDTL	 WHERE numDomainID = @numDomainID
			DELETE FROM 	AlertDomainDtl	 WHERE [numDomainId] = @numDomainID
			DELETE FROM 	AlertContactsAndCompany	 WHERE [numContactID] IN (SELECT numContactId FROM [AdditionalContactsInformation] WHERE [numDomainID]=@numDomainID)
			-- Wil need FK to DOmainID 
			--DELETE FROM 	AlertDTL	 WHERE numDomainID = @numDomainID 
			-- table need column domainID
			DELETE FROM 	BizDocAlerts	 WHERE numDomainID = @numDomainID
			DELETE FROM 	BizDocFilter	 WHERE numDomainID = @numDomainID

			DELETE FROM UOMConversion WHERE numDomainID=@numDomainID
			DELETE FROM UOM WHERE numDomainID = CASE @numDomainID WHEN 0 THEN -1 ELSE @numDomainID END
			
			/*
			SELECT * FROM [AlertHDR]
			SELECT * FROM AlertDTL
			SELECT * FROM [AlertEmailDTL]
			SELECT * FROM [AlertDomainDtl]
			SELECT * FROM [AlertContactsAndCompany]
			SELECT * FROM [AlertEmailMergeFlds]
			SELECT * FROM [BizDocAlerts] 
			*/
			-- Web Analytics,Tracking
			---------------------------------------------------------
			DELETE FROM 	TrackingCampaignData	 WHERE numDomainID = @numDomainID
			DELETE FROM 	TrackingVisitorsDTL	 WHERE [numTracVisitorsHDRID] IN (SELECT [numTrackingID] FROM [TrackingVisitorsHDR] WHERE [numDomainID]=@numDomainID)
			DELETE FROM 	TrackingVisitorsHDR	 WHERE numDomainID = @numDomainID
			

			
			--Action Item, Time & expense,TIckler
			---------------------------------------------------------
			DELETE FROM 	Recurrence	 WHERE  [RecurrenceID] IN (SELECT [RecurrenceID] FROM [Activity] WHERE [ActivityID] IN (SELECT [ActivityID] FROM ActivityResource WHERE [ResourceID] IN (SELECT ResourceID FROM Resource WHERE numDomainID = @numDomainID)) ) 
			--DELETE FROM 	tmpCalandarTbl	 WHERE numDomainID = @numDomainID
			DELETE FROM 	ActivityResource	 WHERE [ResourceID] IN (SELECT ResourceID FROM Resource WHERE numDomainID = @numDomainID)
			DELETE FROM 	ResourcePreference	 WHERE [ResourceID] IN (SELECT [ResourceID] FROM Resource WHERE numDomainID = @numDomainID)
			DELETE FROM 	Resource	 WHERE numDomainID = @numDomainID
			DELETE FROM 	Communication	 WHERE numDomainID = @numDomainID
			DELETE FROM 	Activity	 WHERE [ActivityID] IN (SELECT [ActivityID] FROM ActivityResource WHERE [ResourceID] IN (SELECT ResourceID FROM Resource WHERE numDomainID = @numDomainID))
		

			DELETE FROM 	TimeAndExpense	 WHERE numDomainID = @numDomainID
			DELETE FROM 	tblActionItemData	 WHERE numDomainID = @numDomainID
			DELETE FROM 	FollowUpHistory	 WHERE numDomainID = @numDomainID
			DELETE FROM 	Forecast	 WHERE numDomainID = @numDomainID


			--Reports
			---------------------------------------------------------
			DELETE FROM     DashboardAllowedReports WHERE [numGrpId] IN (SELECT [numGroupID] FROM AuthenticationGroupMaster WHERE [numDomainID]=@numDomainID )
			DELETE FROM 	MyReportList	 WHERE [numUserCntID] IN (SELECT [numContactId] FROM [AdditionalContactsInformation] WHERE [numDomainID]=@numDomainID)
			DELETE FROM 	ForReportsByTeam	 WHERE numDomainID = @numDomainID
			DELETE FROM 	ForReportsByTerritory	 WHERE numDomainID = @numDomainID
			DELETE FROM 	ForReportsByCampaign	 WHERE [numUserCntID] IN (SELECT [numContactId] FROM [AdditionalContactsInformation] WHERE [numDomainID]=@numDomainID)
			DELETE FROM 	ForReportsByAsItsType	 WHERE numDomainID = @numDomainID
			DELETE FROM 	CustRptSchContacts	 WHERE [numScheduleId] IN (SELECT numScheduleId FROM CustRptScheduler	 WHERE [numReportId] IN (SELECT [numCustomReportID] FROM [CustomReport] WHERE [numDomainID]=@numDomainID))
			DELETE FROM 	CustRptScheduler	 WHERE [numReportId] IN (SELECT [numCustomReportID] FROM [CustomReport] WHERE [numDomainID]=@numDomainID)
			DELETE FROM 	CustReportSummationlist	 WHERE [numCustomReportId] IN (SELECT [numCustomReportID] FROM [CustomReport] WHERE [numDomainID]=@numDomainID)
			DELETE FROM 	CustReportOrderlist	 WHERE [numCustomReportId] IN (SELECT [numCustomReportID] FROM [CustomReport] WHERE [numDomainID]=@numDomainID)
			DELETE FROM 	CustReportFilterlist	 WHERE [numCustomReportId] IN (SELECT [numCustomReportID] FROM [CustomReport] WHERE [numDomainID]=@numDomainID)
			DELETE FROM CustReportFields WHERE [numCustomReportId] IN (SELECT [numCustomReportID] FROM [CustomReport] WHERE [numDomainID]=@numDomainID)
			DELETE FROM [CustomReport] WHERE [numDomainID]=@numDomainID;
			--Accounting
			---------------------------------------------------------
			DELETE FROM dbo.ReturnItems WHERE [numReturnHeaderID] IN ( SELECT [numReturnHeaderID] FROM ReturnHeader WHERE numDomainID = @numDomainID)
			DELETE FROM ReturnPaymentHistory WHERE numReturnHeaderID IN ( SELECT [numReturnHeaderID] FROM ReturnHeader WHERE numDomainID = @numDomainID)
			DELETE FROM	dbo.ReturnHeader WHERE numDomainID = @numDomainID
			
			DELETE FROM BankReconcileMaster WHERE numDomainID = @numDomainID
			
			DELETE FROM dbo.BillDetails WHERE numBillID IN (SELECT numBillID FROM dbo.BillHeader WHERE numDomainID = @numDomainID)
			DELETE FROM dbo.BillHeader WHERE numDomainID = @numDomainID
			
			DELETE FROM dbo.BillPaymentDetails WHERE numBillID IN (SELECT numBillPaymentID FROM BillPaymentHeader WHERE numDomainID = @numDomainID)
			DELETE FROM BillPaymentHeader WHERE numDomainID = @numDomainID
			
			DELETE FROM OnlineBillPayeeDetails WHERE numBankDetailId IN (SELECT numBankDetailID FROM dbo.BankDetails WHERE numDomainID = @numDomainID)
			DELETE FROM BankTransactionMapping WHERE numTransactionID IN (SELECT numTransactionID FROM BankStatementTransactions WHERE numDomainID = @numDomainID)
			DELETE FROM BankStatementTransactions WHERE numDomainID = @numDomainID
			DELETE FROM BankStatementHeader WHERE numDomainID = numDomainID
			DELETE FROM dbo.BankDetails WHERE numDomainID = @numDomainID
								
			DELETE FROM 	CashCreditCardRecurringDetails	 WHERE  [numCashCreditCardId] IN ( SELECT [numCashCreditId] FROM CashCreditCardDetails	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	CashCreditCardDetails	 WHERE numDomainID = @numDomainID
			DELETE FROM 	ProcurementBudgetDetails	 WHERE [numProcurementId] IN (SELECT [numProcurementBudgetId] FROM ProcurementBudgetMaster	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	FixedAssetDetails	 WHERE numDomainID = @numDomainID
			
--			DELETE FROM 	ChecksRecurringDetails	 WHERE [numCheckId] IN (SELECT [numCheckId] FROM [CheckDetails] WHERE [numDomainId]=@numDomainID)
--			DELETE FROM 	CheckDetails	 WHERE numDomainID = @numDomainID
			DELETE FROM dbo.CheckDetails WHERE numCheckHeaderID IN (SELECT numCheckHeaderID FROM dbo.CheckHeader WHERE numDomainID = @numDomainID)
			--DELETE FROM dbo.CheckCompanyDetails	 WHERE numDomainID = @numDomainID
			DELETE FROM dbo.CheckHeader WHERE numDomainID = @numDomainID
						
			DELETE FROM 	MarketBudgetDetails	 WHERE [numMarketId] IN (SELECT [numMarketBudgetId] FROM  MarketBudgetMaster	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	OperationBudgetDetails	 WHERE numDomainID = @numDomainID
			DELETE FROM 	OperationBudgetDetails	 WHERE [numBudgetId] IN (SELECT [numBudgetId] FROM [OperationBudgetMaster] WHERE [numDomainId]=@numDomainID)
			
			DELETE FROM 	DepositeDetails	 WHERE numDepositID IN (SELECT numDepositID FROM dbo.DepositMaster WHERE numDomainID = @numDomainID)
			DELETE FROM 	dbo.DepositMaster WHERE numDomainID = @numDomainID
			
			DELETE FROM 	OperationBudgetMaster	 WHERE numDomainID = @numDomainID
			DELETE FROM 	MarketBudgetMaster	 WHERE numDomainID = @numDomainID
			DELETE FROM 	ProcurementBudgetMaster	 WHERE numDomainID = @numDomainID
			DELETE FROM 	AccountingCharges	 WHERE numDomainID = @numDomainID
			DELETE FROM 	ChartAccountOpening	 WHERE numDomainID = @numDomainID
			DELETE FROM 	RecurringTemplate	 WHERE numDomainID = @numDomainID
			DELETE FROM 	FinancialYear	 WHERE numDomainID = @numDomainID
			DELETE FROM 	AccountTypeDetail	 WHERE numDomainID = @numDomainID
			DELETE FROM 	COARelationships	 WHERE numDomainID = @numDomainID
			DELETE FROM 	COAShippingMapping	 WHERE numDomainID = @numDomainID
			DELETE FROM 	COACreditCardCharge	 WHERE numDomainID = @numDomainID
			DELETE FROM 	ContactTypeMapping	 WHERE numDomainID = @numDomainID
			DELETE FROM 	Chart_Of_Accounts	 WHERE numDomainID = @numDomainID

			--Commission
			---------------------------------------------------------
			DELETE FROM 	CommissionContacts	 WHERE numDomainID = @numDomainID
			DELETE FROM 	CommissionReports	 WHERE numDomainID = @numDomainID
			DELETE FROM 	CommissionRuleContacts WHERE numComRuleID IN (SELECT numComRuleID FROM CommissionRules WHERE numDomainID=@numDomainID)
			DELETE FROM 	CommissionRuleItems WHERE numComRuleID IN (SELECT numComRuleID FROM CommissionRules WHERE numDomainID=@numDomainID)
			DELETE FROM 	CommissionRuleDtl WHERE numComRuleID IN (SELECT numComRuleID FROM CommissionRules WHERE numDomainID=@numDomainID)
			DELETE FROM 	CommissionRules	 WHERE numDomainID = @numDomainID
			

			--Relationship
			---------------------------------------------------------
			 
			DELETE FROM 	DynamicFormAOIConf	 WHERE numDomainID = @numDomainID
			delete  RECENTITEMS  where numRecordID IN (SELECT [numDivisionID] FROM [DivisionMaster] WHERE [numDomainID]=@numDomainID) and chrRecordType='C'
			DELETE FROM CommunicationAttendees WHERE numCommId IN (SELECT numCommId FROM Communication WHERE [numDomainID]=@numDomainID)
			DELETE FROM Communication WHERE [numDomainID]=@numDomainID
			delete from ExtranetAccountsDtl WHERE [numDomainID] =@numDomainID
			delete from ExtranetAccountsDtl WHERE [numExtranetID] IN (SELECT [numExtranetID] FROM  ExtarnetAccounts where [numDomainID]=@numDomainID)
			delete from ExtarnetAccounts where [numDomainID]=@numDomainID
			DELETE FROM [ExtarnetAccounts] WHERE [numDivisionID] IN (SELECT [numDivisionID] FROM [DivisionMaster] WHERE [numDomainID]=@numDomainID)
			delete from CaseContacts WHERE numContactID in (select numContactID from AdditionalContactsInformation where numDivisionID IN (SELECT [numDivisionID] FROM [DivisionMaster] WHERE [numDomainID]=@numDomainID))                     
			delete from ProjectsContacts where numContactID in (select numContactID from AdditionalContactsInformation where numDivisionID  IN (SELECT [numDivisionID] FROM [DivisionMaster] WHERE [numDomainID]=@numDomainID))                   
			delete from OpportunityContact where numContactID in (select numContactID from AdditionalContactsInformation where numDivisionID  IN (SELECT [numDivisionID] FROM [DivisionMaster] WHERE [numDomainID]=@numDomainID))                     
			delete AOIContactLink where [numContactID] IN (SELECT numContactId FROM [AdditionalContactsInformation] WHERE [numDomainID]=@numDomainID)
			delete UserTeams WHERE [numDomainID]=@numDomainID
			delete UserTerritory where  [numDomainID]=@numDomainID
			DELETE FROM CustomerCreditCardInfo	 WHERE numContactID IN (SELECT numContactId FROM [AdditionalContactsInformation] WHERE [numDomainID]=@numDomainID)
			
			DELETE FROM AddressDetails	 WHERE numDomainID=@numDomainID 
			DELETE FROM AdditionalContactsInformation WHERE [numDomainID]=@numDomainID
			DELETE FROM [CompanyAssociations] WHERE [numDomainID]=@numDomainID/***/
			DELETE FROM [Vendor] WHERE [numDomainID] =@numDomainID
			DELETE FROM 	DivisionTaxTypes	 WHERE numDivisionID IN (SELECT numDivisionID FROM [DivisionMaster] WHERE [numDomainID] =@numDomainID)
			
			DELETE FROM [DivisionMaster] WHERE [numDomainID] =@numDomainID
			DELETE FROM [CompanyInfo] WHERE [numDomainID]=@numDomainID


			
	
			
			
			DELETE FROM UserAccessedDTL	 WHERE numDomainID = @numDomainID
			DELETE FROM [UserMaster] WHERE [numDomainID] =@numDomainID; 
			DELETE FROM [AccountTypeDetail] WHERE [numDomainID]=@numDomainID;
			DELETE FROM [ExportDataSettings] WHERE [numDomainID]=@numDomainID;
			DELETE FROM TaxCountryConfi WHERE [numDomainID]=@numDomainID
			Delete from AlertConfig where [numDomainID]=@numDomainID
			
			DELETE FROM [Domain] WHERE [numDomainId]=@numDomainID;
			DELETE FROM 	SubscriberHstr	 WHERE [numTargetDomainID] = @numDomainID
			DELETE FROM 	Subscribers	 WHERE [numTargetDomainID] = @numDomainID


			PRINT 'Domain Deleted successfully.'
/*---------------------------------------------------------------------------------*/

/* Not sure about following table 

DELETE FROM 	EformConfiguration	 WHERE numDomainID = @numDomainID
*/

/*---------------------------------------------------------------------------------*/
	ROLLBACK
	---COMMIT TRAN
END TRY 
BEGIN CATCH		
PRINT @@ERROR
PRINT ERROR_MESSAGE() 
    IF (@@TRANCOUNT > 0) BEGIN
        PRINT 'Unexpected error occurred!'
        ROLLBACK TRAN
        RETURN 1
    END
END CATCH	
END
/*
UPDATE [Subscribers] SET [bitActive]=0 WHERE [numTargetDomainID]=70
*/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_Execute')
DROP PROCEDURE dbo.USP_DemandForecast_Execute
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_Execute]
	@numDFID NUMERIC (18,0)
	,@numUserCntID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@CurrentPage INT
    ,@PageSize INT
	,@columnName VARCHAR(MAX)                                                          
	,@columnSortOrder VARCHAR(10)
	,@numDomainID NUMERIC(18,0)
	,@vcRegularSearchCriteria VARCHAR(MAX)=''
	,@bitShowAllItems BIT
	,@bitShowHistoricSales BIT
	,@numHistoricalAnalysisPattern BIT
	,@bitBasedOnLastYear BIT
	,@bitIncludeOpportunity BIT
	,@numOpportunityPercentComplete NUMERIC(18,0)
	,@tintDemandPlanBasedOn TINYINT
AS 
BEGIN
	DECLARE @dtLastExecution DATETIME

	IF (SELECT COUNT(*) FROM DemandForecast WHERE numDFID = @numDFID) > 0
	BEGIN
		DECLARE @tintUnitsRecommendationForAutoPOBackOrder TINYINT
		DECLARE @bitIncludeRequisitions BIT

		SELECT 
			@tintUnitsRecommendationForAutoPOBackOrder=ISNULL(tintUnitsRecommendationForAutoPOBackOrder,1)
			,@bitIncludeRequisitions=ISNULL(bitIncludeRequisitions,0)
		FROM 
			Domain 
		WHERE 
			numDomainID = @numDomainID

		DECLARE @bitWarehouseFilter BIT = 0
		DECLARE @bitItemClassificationFilter BIT = 0
		DECLARE @bitItemGroupFilter BIT = 0

		IF (SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = @numDFID) > 0
		BEGIN
			SET @bitWarehouseFilter = 1
		END

		IF (SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = @numDFID) > 0
		BEGIN
			SET @bitItemClassificationFilter = 1
		END

		IF (SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = @numDFID) > 0
		BEGIN
			SET @bitItemGroupFilter = 1
		END				

		DECLARE @TEMP TABLE
		(
			numItemCode NUMERIC(18,0)
			,numWarehouseItemID NUMERIC(18,0)
			,numUnitHour FLOAT
			,dtReleaseDate DATE
		)

		INSERT INTO @TEMP
		(
			numItemCode
			,numWarehouseItemID
			,numUnitHour
			,dtReleaseDate
		)
		SELECT
			Item.numItemCode
			,OpportunityItems.numWarehouseItmsID
			,OpportunityItems.numUnitHour - ISNULL(OpportunityItems.numQtyShipped,0)
			,ISNULL(OpportunityItems.ItemReleaseDate,OpportunityMaster.dtReleaseDate)
		FROM
			OpportunityMaster
		INNER JOIN
			OpportunityItems
		ON
			OpportunityMaster.numOppId = OpportunityItems.numOppId
		INNER JOIN
			Item
		ON
			OpportunityItems.numItemCode = Item.numItemCode
		INNER JOIN
			WareHouseItems
		ON
			OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
		WHERE
			OpportunityMaster.numDomainId=@numDomainID
			AND OpportunityMaster.tintOppType=1
			AND OpportunityMaster.tintOppStatus=1
			AND ISNULL(OpportunityMaster.tintshipped,0)=0
			AND (ISNULL(OpportunityItems.numUnitHour,0)  - ISNULL(OpportunityItems.numQtyShipped,0)) > 0
			AND ISNULL(OpportunityItems.bitDropShip,0) = 0
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(Item.bitAssembly,0) = 0
			AND ISNULL(Item.bitKitParent,0) = 0
			AND (OpportunityItems.ItemReleaseDate IS NOT NULL OR OpportunityMaster.dtReleaseDate IS NOT NULL)
			AND
			(
				@bitWarehouseFilter = 0 OR
				WareHouseItems.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
			)
			AND
			(
				@bitItemClassificationFilter = 0 OR
				Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
			)
			AND
			(
				@bitItemGroupFilter = 0 OR
				Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
			)

		INSERT INTO @TEMP
		(
			numItemCode
			,numWarehouseItemID
			,numUnitHour
			,dtReleaseDate
		)
		SELECT
			Item.numItemCode
			,OKI.numWareHouseItemId
			,OKI.numQtyItemsReq - ISNULL(OKI.numQtyShipped,0)
			,ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate)
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		INNER JOIN
			OpportunityKitItems OKI
		ON
			OI.numoppitemtCode=OKI.numOppItemID
		INNER JOIN
			Item
		ON
			OKI.numChildItemID = Item.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OKI.numWareHouseItemId = WI.numWareHouseItemID
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=1
			AND ISNULL(OM.tintshipped,0)=0
			AND ISNULL(OI.bitDropShip,0) = 0
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(Item.bitAssembly,0) = 0
			AND ISNULL(Item.bitKitParent,0) = 0
			AND (ISNULL(OKI.numQtyItemsReq,0) - ISNULL(OKI.numQtyShipped,0)) > 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
			AND
			(
				@bitWarehouseFilter = 0 OR
				WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
			)
			AND
			(
				@bitItemClassificationFilter = 0 OR
				Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
			)
			AND
			(
				@bitItemGroupFilter = 0 OR
				Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
			)

		INSERT INTO @TEMP
		(
			numItemCode
			,numWarehouseItemID
			,numUnitHour
			,dtReleaseDate
		)
		SELECT
			Item.numItemCode
			,OKCI.numWareHouseItemId
			,OKCI.numQtyItemsReq - ISNULL(OKCI.numQtyShipped,0)
			,ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate)
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		INNER JOIN
			OpportunityKitChildItems OKCI
		ON
			OI.numoppitemtCode = OKCI.numOppItemID
		INNER JOIN
			Item
		ON
			OKCI.numItemID = Item.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OKCI.numWareHouseItemId = WI.numWareHouseItemID
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=1
			AND ISNULL(OM.tintshipped,0)=0
			AND ISNULL(OI.bitDropShip,0) = 0
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(Item.bitAssembly,0) = 0
			AND ISNULL(Item.bitKitParent,0) = 0
			AND (ISNULL(OKCI.numQtyItemsReq,0) - ISNULL(OKCI.numQtyShipped,0)) > 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
			AND
			(
				@bitWarehouseFilter = 0 OR
				WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
			)
			AND
			(
				@bitItemClassificationFilter = 0 OR
				Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
			)
			AND
			(
				@bitItemGroupFilter = 0 OR
				Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
			)

		INSERT INTO @TEMP
		(
			numItemCode
			,numWarehouseItemID
			,numUnitHour
			,dtReleaseDate
		)
		SELECT
			WOD.numChildItemID
			,WOD.numWareHouseItemId
			,WOD.numQtyItemsReq
			,(CASE WHEN OM.numOppId IS NOT NULL THEN ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) ELSE WO.bintCompliationDate END)
		FROM
			WorkOrderDetails WOD
		INNER JOIN
			Item
		ON
			WOD.numChildItemID = Item.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			WOD.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			WorkOrder WO
		ON
			WOD.numWOId=WO.numWOId
		LEFT JOIN
			OpportunityItems OI
		ON
			WO.numOppItemID = OI.numoppitemtCode
		LEFT JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			WO.numDomainID=@numDomainID
			AND WO.numWOStatus <> 23184 -- NOT COMPLETED
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(Item.bitAssembly,0) = 0
			AND ISNULL(Item.bitKitParent,0) = 0
			AND ISNULL(WOD.numQtyItemsReq,0) > 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL OR WO.bintCompliationDate IS NOT NULL)
			AND 1 = (CASE WHEN OM.numOppId IS NOT NULL THEN (CASE WHEN OM.tintOppStatus=1 THEN 1 ELSE 0 END) ELSE 1 END)
			AND
			(
				@bitWarehouseFilter = 0 OR
				WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
			)
			AND
			(
				@bitItemClassificationFilter = 0 OR
				Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
			)
			AND
			(
				@bitItemGroupFilter = 0 OR
				Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
			)

		CREATE TABLE #TEMPItems
		(
			numItemCode NUMERIC(18,0)
			,numWarehouseItemID NUMERIC(18,0)
			,numUnitHour FLOAT
			,dtReleaseDate VARCHAR(MAX)
			,dtReleaseDateHidden VARCHAR(MAX)
		)

		INSERT INTO #TEMPItems
		(
			numItemCode
			,numWarehouseItemID
			,numUnitHour
			,dtReleaseDate
			,dtReleaseDateHidden
		)
		SELECT
			numItemCode
			,numWarehouseItemID
			,SUM(numUnitHour)
			,STUFF((SELECT 
						CONCAT(', <a href="#" onclick="return OpenDFReleaseDateRecords(',numItemCode,',',numWarehouseItemID,',''',CONVERT(VARCHAR(10), dtReleaseDate, 101),''');">',CONVERT(VARCHAR(10), dtReleaseDate, 101),'</a> (',SUM(numUnitHour),')')
					FROM 
						@TEMP
					WHERE 
						numItemCode=T1.numItemCode
						AND numWarehouseItemID=T1.numWarehouseItemID
					GROUP BY
						numItemCode
						,numWarehouseItemID
						,dtReleaseDate
					FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)')
				  ,1,2,'')
			,STUFF((SELECT 
						CONCAT(', ',CONVERT(VARCHAR(10), dtReleaseDate, 101),' (',SUM(numUnitHour),')')
					FROM 
						@TEMP
					WHERE 
						numItemCode=T1.numItemCode
						AND numWarehouseItemID=T1.numWarehouseItemID
					GROUP BY
						numItemCode
						,numWarehouseItemID
						,dtReleaseDate
					FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)')
				  ,1,2,'')
		FROM
			@TEMP T1
		GROUP BY
			numItemCode
			,numWarehouseItemID

		---------------------------- Dynamic Query -------------------------------
		
		CREATE TABLE #tempForm 
		(
			tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
			numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
			bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
			ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
		)

		-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
		DECLARE  @Nocolumns  AS TINYINT = 0

		SELECT
			@Nocolumns=ISNULL(SUM(TotalRow),0) 
		FROM
		(            
			SELECT 
				COUNT(*) TotalRow 
			FROM 
				View_DynamicColumns 
			WHERE 
				numFormId=139 
				AND numUserCntID=@numUserCntID 
				AND numDomainID=@numDomainID 
				AND tintPageType=1 
				AND numRelCntType = 0
			UNION 
			SELECT 
				COUNT(*) TotalRow 
			FROM 
				View_DynamicCustomColumns 
			WHERE 
				numFormId=139 
				AND numUserCntID=@numUserCntID 
				AND numDomainID=@numDomainID 
				AND tintPageType=1 
				AND numRelCntType = 0
		) TotalRows

		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				139,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=139 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=139 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = 0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=139 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = 0
		ORDER BY 
			tintOrder asc
					
		DECLARE  @strColumns AS VARCHAR(MAX) = ''
		SET @strColumns = CONCAT(' COUNT(*) OVER() TotalRowsCount, Item.numItemCode, Item.vcItemName, Item.vcSKU
		,ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = Item.numBaseUnit),''-'') AS vcUOM
		,Item.charItemType AS vcItemType
		,(SELECT TOP 1 numContactId FROM AdditionalContactsInformation WHERE numDivisionId = Item.numVendorID ORDER BY ISNULL(bitPrimaryContact,0) DESC) AS numContactID
		,ISNULL(Item.numPurchaseUnit,0) AS numUOM
		,dbo.fn_UOMConversion(ISNULL(numBaseUnit, 0), Item.numItemCode, Item.numDomainId, ISNULL(Item.numPurchaseUnit, 0)) fltUOMConversionfactor
		,dbo.fn_GetAttributes(WI.numWareHouseItemId,1) AS vcAttributes
		,WI.numWareHouseItemID
		,W.vcWarehouse
		,ISNULL(V.numVendorID,0) numVendorID
		,ISNULL(V.intMinQty,0) intMinQty
		,ISNULL(monCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,Item.numDomainID,Item.numPurchaseUnit) AS monVendorCost
		,T1.numUnitHour AS numQtyToPurchase, T1.dtReleaseDateHidden, T1.dtReleaseDate,(CASE
			WHEN ',@tintUnitsRecommendationForAutoPOBackOrder,'= 3
			THEN 
				(ISNULL(numBackOrder,0) - ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  
												THEN ISNULL((SELECT 
															SUM(numUnitHour) 
														FROM 
															OpportunityItems 
														INNER JOIN 
															OpportunityMaster 
														ON 
															OpportunityItems.numOppID=OpportunityMaster.numOppId 
														WHERE 
															OpportunityMaster.numDomainId=',@numDomainID,'
															AND OpportunityMaster.tintOppType=2 
															AND OpportunityMaster.tintOppStatus=0 
															AND OpportunityItems.numItemCode=Item.numItemCode 
															AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) 
												ELSE 0 
												END)) +
				(CASE WHEN ISNULL(Item.fltReorderQty,0) >= ISNULL(V.intMinQty,0) THEN ISNULL(Item.fltReorderQty,0) ELSE ISNULL(V.intMinQty,0) END) 
			WHEN ',@tintUnitsRecommendationForAutoPOBackOrder,'=2
			THEN 
				(CASE
					WHEN ISNULL(Item.fltReorderQty,0) >= ISNULL(V.intMinQty,0) AND ISNULL(Item.fltReorderQty,0) >= ISNULL(numBackOrder,0) THEN ISNULL(Item.fltReorderQty,0)
					WHEN ISNULL(V.intMinQty,0) >= ISNULL(Item.fltReorderQty,0) AND ISNULL(V.intMinQty,0) >= ISNULL(numBackOrder,0) THEN ISNULL(V.intMinQty,0)
					WHEN ISNULL(numBackOrder,0) >= ISNULL(Item.fltReorderQty,0) AND ISNULL(numBackOrder,0) >= ISNULL(V.intMinQty,0) THEN ISNULL(numBackOrder,0)
					ELSE ISNULL(Item.fltReorderQty,0)
				END) - (ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  THEN ISNULL((SELECT 
															SUM(numUnitHour) 
														FROM 
															OpportunityItems 
														INNER JOIN 
															OpportunityMaster 
														ON 
															OpportunityItems.numOppID=OpportunityMaster.numOppId 
														WHERE 
															OpportunityMaster.numDomainId=',@numDomainID,'
															AND OpportunityMaster.tintOppType=2 
															AND OpportunityMaster.tintOppStatus=0 
															AND OpportunityItems.numItemCode=Item.numItemCode 
															AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END))
			ELSE
				(CASE WHEN ISNULL(Item.fltReorderQty,0) > ISNULL(V.intMinQty,0) THEN ISNULL(Item.fltReorderQty,0) ELSE ISNULL(V.intMinQty,0) END) 
				+ ISNULL(numBackOrder,0) - (ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  THEN ISNULL((SELECT 
																														SUM(numUnitHour) 
																													FROM 
																														OpportunityItems 
																													INNER JOIN 
																														OpportunityMaster 
																													ON 
																														OpportunityItems.numOppID=OpportunityMaster.numOppId 
																													WHERE 
																														OpportunityMaster.numDomainId=',@numDomainID,'
																														AND OpportunityMaster.tintOppType=2 
																														AND OpportunityMaster.tintOppStatus=0 
																														AND OpportunityItems.numItemCode=Item.numItemCode 
																														AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END)) 
		END) AS numQtyBasedOnPurchasePlan ')

		--Custom field 
		DECLARE @tintOrder AS TINYINT = 0                                                 
		DECLARE @vcFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(3)                                             
		DECLARE @vcAssociatedControlType VARCHAR(20)     
		declare @numListID AS numeric(9)
		DECLARE @vcDbColumnName VARCHAR(50)                      
		DECLARE @WhereCondition VARCHAR(MAX) = ''                   
		DECLARE @vcLookBackTableName VARCHAR(2000)                
		DECLARE @bitCustom AS BIT
		DECLARE @bitAllowEdit AS CHAR(1)                   
		Declare @numFieldId as numeric  
		DECLARE @bitAllowSorting AS CHAR(1)    
		DECLARE @vcColumnName AS VARCHAR(500)             
		Declare @ListRelID as numeric(9) 
		DECLARE @SearchQuery AS VARCHAR(MAX) = '' 
	   
		SELECT TOP 1 
			@tintOrder=tintOrder+1
			,@vcDbColumnName=vcDbColumnName
			,@vcFieldName=vcFieldName
			,@vcAssociatedControlType=vcAssociatedControlType
			,@vcListItemType=vcListItemType
			,@numListID=numListID
			,@vcLookBackTableName=vcLookBackTableName
			,@bitCustom=bitCustomField
			,@numFieldId=numFieldId
			,@bitAllowSorting=bitAllowSorting
			,@bitAllowEdit=bitAllowEdit
			,@ListRelID=ListRelID
		FROM  
			#tempForm 
		ORDER BY 
			tintOrder ASC   

		WHILE @tintOrder>0                                                  
		BEGIN
	
			IF @bitCustom = 0  
			BEGIN
				DECLARE @Prefix AS VARCHAR(10)

				IF @vcLookBackTableName = 'AdditionalContactsInformation'
					SET @Prefix = 'ADC.'
				ELSE IF @vcLookBackTableName = 'DivisionMaster'
					SET @Prefix = 'Div.'
				ELSE IF @vcLookBackTableName = 'OpportunityMaster'
					SET @PreFix = 'Opp.'			
				ELSE IF @vcLookBackTableName = 'CompanyInfo'
					SET @PreFix = 'cmp.'
				ELSE IF @vcLookBackTableName = 'DemandForecastGrid'
					SET @PreFix = 'DF.'
				ELSE IF @vcLookBackTableName = 'Warehouses'
					SET @PreFix = 'W.'
				ELSE IF @vcLookBackTableName = 'WareHouseItems'
					SET @PreFix = 'WI.'
				ELSE 
					SET @PreFix = CONCAT(@vcLookBackTableName,'.')
			
				SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

				IF @vcAssociatedControlType = 'TextBox'
				BEGIN
					IF @vcDbColumnName = 'numQtyToPurchase'
					BEGIN
						SET @strColumns=@strColumns+',' + ' (CASE WHEN ISNULL(WI.numBackOrder,0) > ISNULL(WI.numOnOrder,0) THEN  CEILING(ISNULL(WI.numBackOrder,0) - ISNULL(WI.numOnOrder,0))  ELSE 0 END) '  + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'moncost'
					BEGIN
						SET @strColumns=@strColumns+',' + ' ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = Item.numBaseUnit),''-'') '  + ' [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns=@strColumns+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']' 
					END
				END
				ELSE IF  @vcAssociatedControlType='Label'                                              
				BEGIN
					IF @vcDbColumnName = 'vc7Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,7,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vc15Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,15,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vc30Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,30,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vc60Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,60,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vc90Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,90,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vc180Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,180,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
					END
					ELSE IF (@vcDbColumnName = 'vcBuyUOM')
					BEGIN
						SET @strColumns=@strColumns+',' + ' ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = ISNULL(Item.numPurchaseUnit,0)),'''')' + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'numOnOrderReq'
					BEGIN
						SET @strColumns=@strColumns+',' + ' (ISNULL(WI.numOnOrder,0) + ISNULL(WI.numReorder,0)) '  + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'numAvailable'
					BEGIN
						SET @strColumns=@strColumns+',' + 'CONCAT(ISNULL(WI.numOnHand,0) + ISNULL(WI.numAllocation,0),'' ('',ISNULL(WI.numOnHand,0),'')'')'  + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'numTotalOnHand'
					BEGIN
						SET @strColumns=@strColumns+',' + 'CONCAT(ISNULL((SELECT SUM(WIInner.numOnHand) + SUM(WIInner.numAllocation) FROM WarehouseItems WIInner WHERE WIInner.numItemID=Item.numItemCode),0),'' ('',ISNULL((SELECT SUM(WIInner.numOnHand) FROM WarehouseItems WIInner WHERE WIInner.numItemID=Item.numItemCode),0),'')'')'  + ' [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns=@strColumns + ',' + @Prefix + @vcDbColumnName  +' ['+ @vcColumnName+']' 
					END   
				END
				ELSE IF  @vcAssociatedControlType='SelectBox' or @vcAssociatedControlType='ListBox'  
				BEGIN
					IF @vcDbColumnName = 'numVendorID'
					BEGIN
						SET @strColumns=@strColumns+',' + ' V.numVendorID '  + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'ShipmentMethod'
					BEGIN
						SET @strColumns=@strColumns+',' + ' '''' '  + ' [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns=@strColumns + ',' + @Prefix + @vcDbColumnName  +' ['+ @vcColumnName+']' 
					END
				END
				ELSE IF @vcAssociatedControlType='DateField'   
				BEGIN
					IF @vcDbColumnName = 'dtExpectedDelivery'
					BEGIN
						SET @strColumns=@strColumns+',' + ' GETDATE() '  + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'ItemRequiredDate'
					BEGIN
						SET @strColumns=@strColumns+',T1.dtReleaseDate [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns=@strColumns + ',' + @Prefix + @vcDbColumnName  +' ['+ @vcColumnName+']' 
					END					
				END
			END

			SELECT TOP 1 
				@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
				@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,
				@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
			FROM
				#tempForm 
			WHERE 
				tintOrder > @tintOrder-1 
			ORDER BY 
				tintOrder asc            
 
			IF @@rowcount=0 SET @tintOrder=0 

		 END
	  
		DECLARE @StrSql AS VARCHAR(MAX) = ''
		SET @StrSql = CONCAT('SELECT ',@strColumns,' 
							FROM
								#TEMPItems T1
							INNER JOIN
								Item
							ON
								T1.numItemCode = Item.numItemCode
							INNER JOIN
								WarehouseItems WI
							ON
								Item.numItemCode=WI.numItemID
								AND T1.numWarehouseItemID=WI.numWarehouseItemID
							INNER JOIN
								Warehouses W
							ON
								WI.numWarehouseID = W.numWareHouseID
							LEFT JOIN
								Vendor V
							ON
								Item.numVendorID = V.numVendorID
								AND Item.numItemCode = V.numItemCode
							OUTER APPLY
							(
								SELECT TOP 1
									numListValue AS numLeadDays
								FROM
									VendorShipmentMethod VSM
								WHERE
									VSM.numVendorID = V.numVendorID
								ORDER BY
									ISNULL(bitPrimary,0) DESC, bitPreferredMethod DESC, numListItemID ASC
							) TempLeadDays
							WHERE
								Item.numDomainID=',@numDomainID,'
								AND 1 = (CASE 
											WHEN ',@bitShowAllItems,' = 1 
											THEN 1 
											ELSE 
												(CASE 
													WHEN ', ISNULL(@tintDemandPlanBasedOn,1),' = 2
													THEN (CASE 
															WHEN (CASE
																	WHEN ',@tintUnitsRecommendationForAutoPOBackOrder,'= 3
																	THEN 
																		(ISNULL(numBackOrder,0) - ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  
																										THEN ISNULL((SELECT 
																													SUM(numUnitHour) 
																												FROM 
																													OpportunityItems 
																												INNER JOIN 
																													OpportunityMaster 
																												ON 
																													OpportunityItems.numOppID=OpportunityMaster.numOppId 
																												WHERE 
																													OpportunityMaster.numDomainId=',@numDomainID,'
																													AND OpportunityMaster.tintOppType=2 
																													AND OpportunityMaster.tintOppStatus=0 
																													AND OpportunityItems.numItemCode=Item.numItemCode 
																													AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) 
																										ELSE 0 
																										END)) +
																		(CASE WHEN ISNULL(Item.fltReorderQty,0) >= ISNULL(V.intMinQty,0) THEN ISNULL(Item.fltReorderQty,0) ELSE ISNULL(V.intMinQty,0) END) 
																	WHEN ',@tintUnitsRecommendationForAutoPOBackOrder,'=2
																	THEN 
																		(CASE
																			WHEN ISNULL(Item.fltReorderQty,0) >= ISNULL(V.intMinQty,0) AND ISNULL(Item.fltReorderQty,0) >= ISNULL(numBackOrder,0) THEN ISNULL(Item.fltReorderQty,0)
																			WHEN ISNULL(V.intMinQty,0) >= ISNULL(Item.fltReorderQty,0) AND ISNULL(V.intMinQty,0) >= ISNULL(numBackOrder,0) THEN ISNULL(V.intMinQty,0)
																			WHEN ISNULL(numBackOrder,0) >= ISNULL(Item.fltReorderQty,0) AND ISNULL(numBackOrder,0) >= ISNULL(V.intMinQty,0) THEN ISNULL(numBackOrder,0)
																			ELSE ISNULL(Item.fltReorderQty,0)
																		END) - (ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  THEN ISNULL((SELECT 
																													SUM(numUnitHour) 
																												FROM 
																													OpportunityItems 
																												INNER JOIN 
																													OpportunityMaster 
																												ON 
																													OpportunityItems.numOppID=OpportunityMaster.numOppId 
																												WHERE 
																													OpportunityMaster.numDomainId=',@numDomainID,'
																													AND OpportunityMaster.tintOppType=2 
																													AND OpportunityMaster.tintOppStatus=0 
																													AND OpportunityItems.numItemCode=Item.numItemCode 
																													AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END))
																	ELSE
																		(CASE WHEN ISNULL(Item.fltReorderQty,0) > ISNULL(V.intMinQty,0) THEN ISNULL(Item.fltReorderQty,0) ELSE ISNULL(V.intMinQty,0) END) 
																		+ ISNULL(numBackOrder,0) - (ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  THEN ISNULL((SELECT 
																																												SUM(numUnitHour) 
																																											FROM 
																																												OpportunityItems 
																																											INNER JOIN 
																																												OpportunityMaster 
																																											ON 
																																												OpportunityItems.numOppID=OpportunityMaster.numOppId 
																																											WHERE 
																																												OpportunityMaster.numDomainId=',@numDomainID,'
																																												AND OpportunityMaster.tintOppType=2 
																																												AND OpportunityMaster.tintOppStatus=0 
																																												AND OpportunityItems.numItemCode=Item.numItemCode 
																																												AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END)) 
																END) > 0
															THEN 1
															ELSE 0
														END)
													ELSE
														(CASE 
															WHEN 
																T1.numUnitHour > 0 
															THEN 1 
															ELSE 0 
														END)
												END)
										END)
							')

		IF CHARINDEX('I.',@vcRegularSearchCriteria) > 0
		BEGIN			   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'I.','Item.')
		END
		IF CHARINDEX('WH.',@vcRegularSearchCriteria) > 0
		BEGIN			   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WH.','W.')
		END
	
		IF @vcRegularSearchCriteria<>'' 
		BEGIN
			SET @strSql=@strSql +' AND ' + @vcRegularSearchCriteria 
		END
	  
		DECLARE  @firstRec  AS INTEGER
		DECLARE  @lastRec  AS INTEGER
  
		SET @firstRec = (@CurrentPage - 1) * @PageSize
		SET @lastRec = (@CurrentPage * @PageSize + 1)

		-- EXECUTE FINAL QUERY
		DECLARE @strFinal AS NVARCHAR(MAX)
		SET @strFinal = CONCAT(@StrSql,' ORDER BY ',CASE WHEN CHARINDEX(@columnName,@strColumns) > 0 THEN @columnName ELSE 'Item.numItemCode' END,' OFFSET ',(@CurrentPage-1) * @PageSize,' ROWS FETCH NEXT ',@PageSize,' ROWS ONLY')

		PRINT CAST(@strFinal AS NTEXT)
		EXEC sp_executesql @strFinal

		SELECT * FROM #tempForm

		DROP TABLE #tempForm

		DROP TABLE #TEMPItems


		UPDATE DemandForecast SET dtExecutionDate = dateadd(MINUTE,-@ClientTimeZoneOffset,GETDATE()) WHERE numDFID = @numDFID

		SELECT CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,@dtLastExecution)) AS dtLastExecution
	END
END
/****** Object:  StoredProcedure [dbo].[USP_EcommerceSettings]    Script Date: 03/25/2009 16:46:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ecommercesettings')
DROP PROCEDURE usp_ecommercesettings
GO
CREATE PROCEDURE [dbo].[USP_EcommerceSettings]              
@numDomainID as numeric(9),              
@vcPaymentGateWay as int ,                
@vcPGWUserId as varchar(100)='' ,                
@vcPGWPassword as varchar(100),              
@bitShowInStock as bit,              
@bitShowQOnHand as bit,
@bitCheckCreditStatus as BIT,
@numDefaultWareHouseID as numeric(9),
@numRelationshipId as numeric(9),
@numProfileId as numeric(9),
@bitHidePriceBeforeLogin	BIT,
@bitAuthOnlyCreditCard BIT=0,
@bitSendMail BIT = 1,
@vcGoogleMerchantID VARCHAR(1000)  = '',
@vcGoogleMerchantKey VARCHAR(1000) = '',
@IsSandbox BIT ,
@numAuthrizedOrderStatus NUMERIC,
@numSiteID numeric(18, 0),
@vcPaypalUserName VARCHAR(50)  = '',
@vcPaypalPassword VARCHAR(50)  = '',
@vcPaypalSignature VARCHAR(500)  = '',
@IsPaypalSandbox BIT,
@bitSkipStep2 BIT = 0,
@bitDisplayCategory BIT = 0,
@bitShowPriceUsingPriceLevel BIT = 0,
@bitEnableSecSorting BIT = 0,
@bitSortPriceMode   BIT=0,
@numPageSize    INT=15,
@numPageVariant  INT=6,
@bitAutoSelectWarehouse BIT =0,
@bitPreSellUp BIT = 0, 
@bitPostSellUp BIT = 0, 
@numDefaultClass NUMERIC(18,0) = 0,
@vcPreSellUp VARCHAR(200)=NULL,
@vcPostSellUp VARCHAR(200)=NULL,
@vcRedirectThankYouUrl VARCHAR(300)=NULL,
@tintPreLoginProceLevel TINYINT = 0,
@bitHideAddtoCart BIT=1,
@bitShowPromoDetailsLink BIT = 1,
@tintWarehouseAvailability TINYINT = 1,
@bitDisplayQtyAvailable BIT = 0
as              
BEGIN	        
	IF NOT EXISTS(select 'col1' from eCommerceDTL where numDomainID=@numDomainID AND [numSiteId] = @numSiteID)
	BEGIN
		INSERT INTO eCommerceDTL
        (
			numDomainId,
            vcPaymentGateWay,
            vcPGWManagerUId,
            vcPGWManagerPassword,
            bitShowInStock,
            bitShowQOnHand,
            numDefaultWareHouseID,
            bitCheckCreditStatus,
            [numRelationshipId],
            [numProfileId],
            [bitHidePriceBeforeLogin],
            bitAuthOnlyCreditCard,
            bitSendEmail,
            vcGoogleMerchantID,
            vcGoogleMerchantKey,
            IsSandbox ,
            numAuthrizedOrderStatus,
			numSiteId,
			vcPaypalUserName,
			vcPaypalPassword,
			vcPaypalSignature,
			IsPaypalSandbox,
			bitSkipStep2,
			bitDisplayCategory,
			bitShowPriceUsingPriceLevel,
			bitEnableSecSorting,
			bitSortPriceMode,
			numPageSize,
			numPageVariant,
			bitAutoSelectWarehouse,
			[bitPreSellUp],
			[bitPostSellUp],
			numDefaultClass,
			vcPreSellUp,
			vcPostSellUp,
			vcRedirectThankYouUrl,
			tintPreLoginProceLevel,
			bitHideAddtoCart,
			bitShowPromoDetailsLink,
			tintWarehouseAvailability,
			bitDisplayQtyAvailable
		)
		VALUES
		(
			@numDomainID,
            @vcPaymentGateWay,
            @vcPGWUserId,
            @vcPGWPassword,
            @bitShowInStock,
            @bitShowQOnHand,
            @numDefaultWareHouseID,
            @bitCheckCreditStatus,
            @numRelationshipId,
            @numProfileId,
            @bitHidePriceBeforeLogin,
            @bitAuthOnlyCreditCard,
            @bitSendMail,
            @vcGoogleMerchantID,
            @vcGoogleMerchantKey ,
            @IsSandbox,
            @numAuthrizedOrderStatus,
			@numSiteID,
			@vcPaypalUserName,
			@vcPaypalPassword,
			@vcPaypalSignature,
			@IsPaypalSandbox,
			@bitSkipStep2,
			@bitDisplayCategory,
			@bitShowPriceUsingPriceLevel,
			@bitEnableSecSorting,
			@bitSortPriceMode,
			@numPageSize,
			@numPageVariant,
			@bitAutoSelectWarehouse,
			@bitPreSellUp,
			@bitPostSellUp,
			@numDefaultClass,
			@vcPreSellUp,
			@vcPostSellUp,
			@vcRedirectThankYouUrl,
			@tintPreLoginProceLevel,
			@bitHideAddtoCart,
			@bitShowPromoDetailsLink,
			@tintWarehouseAvailability,
			@bitDisplayQtyAvailable
        )

		IF ISNULL((SELECT numDefaultSiteID FROM DOmain WHERE numDomainId=@numDomainID ),0) = 0
		BEGIN
			UPDATE Domain SET numDefaultSiteID=SCOPE_IDENTITY() WHERE numDomainId=@numDomainID
		END
	END
	ELSE
	BEGIN
		UPDATE 
			eCommerceDTL                                   
		SET              
			vcPaymentGateWay=@vcPaymentGateWay,                
			vcPGWManagerUId=@vcPGWUserId ,                
			vcPGWManagerPassword= @vcPGWPassword,              
			bitShowInStock=@bitShowInStock ,               
			bitShowQOnHand=@bitShowQOnHand,        
			numDefaultWareHouseID =@numDefaultWareHouseID,
			bitCheckCreditStatus=@bitCheckCreditStatus ,
			numRelationshipId = @numRelationshipId,
			numProfileId = @numProfileId,
			[bitHidePriceBeforeLogin]=@bitHidePriceBeforeLogin,
			bitAuthOnlyCreditCard=@bitAuthOnlyCreditCard,
			bitSendEmail = @bitSendMail,
			vcGoogleMerchantID = @vcGoogleMerchantID ,
			vcGoogleMerchantKey =  @vcGoogleMerchantKey ,
			IsSandbox = @IsSandbox,
			numAuthrizedOrderStatus = @numAuthrizedOrderStatus,
			numSiteId = @numSiteID,
			vcPaypalUserName = @vcPaypalUserName ,
			vcPaypalPassword = @vcPaypalPassword,
			vcPaypalSignature = @vcPaypalSignature,
			IsPaypalSandbox = @IsPaypalSandbox,
			bitSkipStep2 = @bitSkipStep2,
			bitDisplayCategory = @bitDisplayCategory,
			bitShowPriceUsingPriceLevel = @bitShowPriceUsingPriceLevel,
			bitEnableSecSorting = @bitEnableSecSorting,
			bitSortPriceMode=@bitSortPriceMode,
			numPageSize=@numPageSize,
			numPageVariant=@numPageVariant,
			bitAutoSelectWarehouse=@bitAutoSelectWarehouse,
			[bitPreSellUp] = @bitPreSellUp,
			[bitPostSellUp] = @bitPostSellUp,
			numDefaultClass=@numDefaultClass,
			vcPreSellUp=@vcPreSellUp,
			vcPostSellUp=@vcPostSellUp,
			vcRedirectThankYouUrl=@vcRedirectThankYouUrl,
			tintPreLoginProceLevel=@tintPreLoginProceLevel,
			bitHideAddtoCart=@bitHideAddtoCart,
			bitShowPromoDetailsLink=@bitShowPromoDetailsLink,
			tintWarehouseAvailability=@tintWarehouseAvailability,
			bitDisplayQtyAvailable=@bitDisplayQtyAvailable
		WHERE 
			numDomainId=@numDomainID 
			AND [numSiteId] = @numSiteID
	END
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects SYSOBJ WHERE SYSOBJ.xtype='p'AND SYSOBJ.NAME ='USP_GET_DROPDOWN_DATA_FOR_IMPORT')
DROP PROCEDURE USP_GET_DROPDOWN_DATA_FOR_IMPORT
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- USP_GET_DROPDOWN_DATA_FOR_IMPORT 75,1
CREATE PROCEDURE USP_GET_DROPDOWN_DATA_FOR_IMPORT
(
	@intImportFileID    BIGINT,
	@numDomainID		BIGINT
)
AS 
BEGIN
	
	DECLARE @numFormID AS NUMERIC
	
	SELECT @numFormID = numMasterID  FROM dbo.Import_File_Master  WHERE intImportFileID=@intImportFileID
	
	
	SELECT ROW_NUMBER() OVER (ORDER BY intImportFileID) AS [ID],* 
			INTO #tempDropDownData FROM(
					SELECT  PARENT.intImportFileID,
							vcImportFileName,
							PARENT.numDomainID,
							DFFM.numFieldId [numFormFieldId],
							DFFM.vcAssociatedControlType,
							DYNAMIC_FIELD.vcDbColumnName,
							DFFM.vcFieldName [vcFormFieldName],
							FIELD_MAP.intMapColumnNo,
							DYNAMIC_FIELD.numListID,
							0 AS [bitCustomField]					
					FROM Import_File_Master PARENT
					INNER JOIN Import_File_Field_Mapping FIELD_MAP ON PARENT.intImportFileID = FIELD_MAP.intImportFileID
					--INNER JOIN dbo.DynamicFormFieldMaster DYNAMIC_FIELD ON FIELD_MAP.numFormFieldID = DYNAMIC_FIELD.numFormFieldId
					INNER JOIN dbo.DycFormField_Mapping DFFM ON FIELD_MAP.numFormFieldID = DFFM.numFieldId
					INNER JOIN dbo.DycFieldMaster DYNAMIC_FIELD ON DYNAMIC_FIELD.numFieldId = DFFM.numFieldId
					WHERE DFFM.vcAssociatedControlType ='SelectBox'
					  AND PARENT.intImportFileID = @intImportFileID
					  AND PARENT.numDomainID = @numDomainID   
					  AND DFFM.numFormID = @numFormID
					
					UNION ALL
					
						  SELECT    FIELD_MAP.intImportFileID AS [intImportFileID],
									cfm.Fld_label  AS [vcImportFileName],
									numDomainID AS [numDomainID],
									CFm.Fld_id AS [numFormFieldId],
									CASE WHEN CFM.Fld_type = 'Drop Down List Box'
										 THEN 'SelectBox'
										 ELSE CFM.Fld_type 
									END	 AS [vcAssociatedControlType],
									cfm.Fld_label AS [vcDbColumnName],
									cfm.Fld_label AS [vcFormFieldName],
									FIELD_MAP.intMapColumnNo  AS [intMapColumnNo],
									CFM.numlistid AS [numListID],
									1 AS [bitCustomField]
						  FROM      dbo.CFW_Fld_Master CFM
						  INNER JOIN Import_File_Field_Mapping FIELD_MAP ON CFM.Fld_id = FIELD_MAP.numFormFieldID		
						  WHERE FIELD_MAP.intImportFileID = @intImportFileID     
						    AND numDomainID = @numDomainID) TABLE1 WHERE  vcAssociatedControlType = 'SelectBox' 
                    
	SELECT * FROM #tempDropDownData   
	
	DECLARE @intRow AS INT
	SELECT @intRow = COUNT(*) FROM #tempDropDownData
	PRINT @intRow
	
	DECLARE @intCnt AS INT
	DECLARE @numListIDValue AS BIGINT
	DECLARE @numFormFieldIDValue AS BIGINT
	DECLARE @bitCustomField AS BIT
		
	DECLARE @strSQL AS NVARCHAR(MAX)
	DECLARE @vcTableNames AS VARCHAR(MAX) 
	DECLARE @strDBColumnName AS NVARCHAR(1000)
	
	SET @intCnt = 0
	SET @strSQL = '' 
	SET @vcTableNames = ''

	CREATE TABLE #tempGroups
	(
		[Key] NUMERIC(9),
		[Value] VARCHAR(100)
	) 	
	INSERT INTO #tempGroups([Key],[Value]) 
	SELECT 0 AS [Key],'Groups' AS [Value]
	UNION ALL
	SELECT 1 AS [Key],'Prospects' AS [Value]
	UNION ALL
	SELECT 1 AS [Key],'Prospect' AS [Value]
	UNION ALL
	SELECT 1 AS [Key],'Pros' AS [Value]
	UNION ALL
	SELECT 2 AS [Key],'Accounts' AS [Value]
	UNION ALL
	SELECT 2 AS [Key],'Account' AS [Value]
	UNION ALL
	SELECT 2 AS [Key],'Acc' AS [Value]

	--SELECT * FROM #tempGroups
	
	WHILE(@intCnt < @intRow   )
		BEGIN
			SET @intCnt = @intCnt + 1
				
			SELECT  
					@numListIDValue = numListID,
					@strDBColumnName = vcDbColumnName, 
					@numFormFieldIDValue = numFormFieldID,
					@bitCustomField = bitCustomField 	 
			FROM #tempDropDownData 
			WHERE ID = @intCnt
			  AND intImportFileID = @intImportFileID
			
			PRINT 'BIT'
			PRINT @bitCustomField
			PRINT @strDBColumnName
			SELECT @strSQL = CASE  
								WHEN @strDBColumnName = 'numItemGroup'
								THEN @strSQL + 'SELECT ITEM_GROUP.numItemGroupID AS [Key],ITEM_GROUP.vcItemGroup AS [Value] FROM dbo.ItemGroups ITEM_GROUP WHERE ITEM_GROUP.numDomainID = ' + CONVERT(VARCHAR,@numDomainID)
								
								WHEN @strDBColumnName = 'tintCRMType'
								THEN @strSQL + 'SELECT DISTINCT [Key],[Value] FROM #tempGroups '
								
								WHEN @strDBColumnName = 'vcLocation'
								THEN @strSQL + 'SELECT numWLocationID AS [Key],vcLocation AS [Value] FROM dbo.WarehouseLocation WHERE numDomainID = ' + CONVERT(VARCHAR,@numDomainID)
								
								WHEN @strDBColumnName = 'vcExportToAPI'
								THEN @strSQL + 'SELECT DISTINCT WebApiId AS [Key],vcProviderName AS [Value] FROM dbo.WebAPI '
								
								WHEN @strDBColumnName = 'numCategoryID'
								THEN @strSQL + 'SELECT numCategoryID AS [Key], vcCategoryName AS [Value] FROM dbo.Category WHERE numDomainID =' + CONVERT(VARCHAR,@numDomainID)
								
								WHEN @strDBColumnName = 'numGrpID'
								THEN @strSQL + 'SELECT GRP.numGrpID AS [Key],GRP.vcGrpName AS [Value] FROM dbo.Groups GRP ' 
											    			    
								WHEN @strDBColumnName = 'numWareHouseID' --OR @strDBColumnName = 'numWareHouseItemID'															  
								THEN @strSQL + 'SELECT WAREHOUSE.numWareHouseID AS [Key],WAREHOUSE.vcWareHouse AS [Value] FROM dbo.Warehouses WAREHOUSE WHERE WAREHOUSE.numDomainID = '+ CONVERT(VARCHAR,@numDomainID) 
								
								WHEN @strDBColumnName = 'numWareHouseItemID'
								THEN @strSQL + 'SELECT WI.numWareHouseItemID AS [Key],W.vcWareHouse AS [Value] FROM dbo.WareHouseItems WI INNER JOIN dbo.Warehouses W ON WI.numWareHouseID = W.numWareHouseID WHERE W.numDomainID = '+ CONVERT(VARCHAR,@numDomainID)
												
								WHEN @strDBColumnName = 'numChildItemID' OR @strDBColumnName = 'numItemKitID'
								THEN @strSQL + ' SELECT ITEM.numItemCode AS [Key],ITEM.vcItemName AS [Value] FROM dbo.Item ITEM WHERE ITEM.numDomainID = '+ CONVERT(VARCHAR,@numDomainID)
								
								WHEN (@strDBColumnName = 'numUOMId' OR @strDBColumnName = 'numPurchaseUnit' OR @strDBColumnName = 'numSaleUnit' OR @strDBColumnName = 'numBaseUnit') 
								THEN @strSQL + '  SELECT numUOMId AS [Key], vcUnitName AS [Value] FROM dbo.UOM
												  JOIN Domain d ON dbo.UOM.numDomainID = d.numDomainID 
												  WHERE dbo.UOM.numDomainId = ' + CONVERT(VARCHAR(10),@numDomainID) +' 
												  AND bitEnabled=1 
												  AND dbo.UOM.tintUnitType=(CASE WHEN ISNULL(d.charUnitSystem,''E'')=''E'' THEN 1 WHEN d.charUnitSystem=''M'' THEN 2 END) '	
								
								WHEN (@strDBColumnName = 'numIncomeChartAcntId' OR @strDBColumnName = 'numAssetChartAcntId' OR @strDBColumnName = 'numCOGsChartAcntId')
								THEN @strSQL + '  SELECT numAccountId AS [Key], LTRIM(RTRIM(REPLACE(vcAccountName,ISNULL(vcNumber,''''),''''))) AS [Value] FROM dbo.Chart_Of_Accounts WHERE numDomainID= ' + CONVERT(VARCHAR(10),@numDomainID)

								WHEN @strDBColumnName = 'numCampaignID'
								THEN @strSQL + '  SELECT numCampaignID AS [Key], vcCampaignName AS [Value] FROM  CampaignMaster WHERE CampaignMaster.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID)
								
								WHEN @strDBColumnName = 'numVendorID'
								THEN @strSQL + '  SELECT DM.numDivisionID AS [Key], CI.vcCompanyName AS [Value] FROM dbo.CompanyInfo CI INNER JOIN dbo.DivisionMaster DM ON DM.numCompanyID = CI.numCompanyId 
												  WHERE DM.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID)
								
								WHEN @strDBColumnName = 'numAssignedTo' OR @strDBColumnName = 'numRecOwner' 
								THEN @strSQL + '  SELECT A.numContactID AS [Key], A.vcFirstName+'' ''+A.vcLastName AS [Value] from UserMaster UM join AdditionalContactsInformation A  on UM.numUserDetailId=A.numContactID  where UM.numDomainID = A.numDomainID AND UM.numDomainID = ' + CONVERT(VARCHAR,@numDomainID)	
								
------								 SELECT A.numContactID,A.vcFirstName+' '+A.vcLastName as vcUserName from UserMaster UM         
------								 join AdditionalContactsInformation A        
------								 on UM.numUserDetailId=A.numContactID          
------								 where UM.numDomainID=@numDomainID and UM.numDomainID=A.numDomainID   
							
								WHEN @strDBColumnName = 'numTermsID'
								THEN @strSQL + 'SELECT DISTINCT numTermsID AS [Key],vcTerms AS [Value] FROM BillingTerms 
												WHERE numDomainID = ' + CONVERT(VARCHAR,@numDomainID) 
												
								WHEN @strDBColumnName = 'numShipState' OR @strDBColumnName = 'numBillState' OR @strDBColumnName = 'numState' OR @strDBColumnName = 'State'
								THEN @strSQL + '  SELECT numStateID AS [Key], vcState AS [Value], (CASE WHEN LEN(ISNULL(vcAbbreviations,'''')) > 0 THEN vcAbbreviations ELSE ''NOTAVAILABLE'' END) vcAbbreviations FROM State where numDomainID=' + CONVERT(VARCHAR,@numDomainID)
								
								WHEN @strDBColumnName = 'charItemType'
								THEN @strSQL + ' SELECT ''A'' AS [Key], ''asset'' AS [Value]
											     UNION 				
											     SELECT ''P'' AS [Key], ''inventory item'' AS [Value]
											     UNION 				
											     SELECT ''S'' AS [Key], ''service'' AS [Value]
											     UNION 				
											     SELECT ''N'' AS [Key], ''non-inventory item'' AS [Value]'	
								
								WHEN @strDBColumnName = 'tintRuleType'
								THEN @strSQL + ' SELECT 1 AS [Key], ''Deduct from List price'' AS [Value]
											     UNION 				
											     SELECT 2 AS [Key], ''Add to Primary Vendor Cost'' AS [Value]
											     UNION 				
											     SELECT 3 AS [Key], ''Named Price'' AS [Value]'	
											     
								WHEN @strDBColumnName = 'tintDiscountType'
								THEN @strSQL + ' SELECT 1 AS [Key], ''Percentage'' AS [Value]
											     UNION 				
											     SELECT 2 AS [Key], ''Flat Amount'' AS [Value]
												 UNION 				
											     SELECT 3 AS [Key], ''Named Price'' AS [Value]'
												 											     
								WHEN @strDBColumnName = 'numShippingService'
								THEN @strSQL + 'SELECT 
													ShippingService.numShippingServiceID AS [Key]
													,ShippingService.vcShipmentService AS [Value]
													,ISNULL(ShippingServiceAbbreviations.vcAbbreviation,'''') vcAbbreviations
												FROM 
													ShippingService 
												LEFT JOIN
													ShippingServiceAbbreviations
												ON
													ShippingServiceAbbreviations.numShippingServiceID = ShippingService.numShippingServiceID
												WHERE 
													ShippingService.numDomainID=' + CONVERT(VARCHAR,@numDomainID) + ' OR ISNULL(ShippingService.numDomainID,0) = 0'					     																		    
								WHEN @strDBColumnName = 'numChartAcntId'
								THEN @strSQL + 'SELECT 
													numAccountId AS [Key]
													,vcAccountName AS [Value] 
												FROM 
													Chart_Of_Accounts 
												WHERE 
													numDomainId=' + CONVERT(VARCHAR,@numDomainID)
								ELSE 
									 @strSQL + ' SELECT LIST.numListItemID AS [Key],vcData AS [Value] FROM ListDetails LIST LEFT JOIN listorder LIST_ORDER ON LIST.numListItemID = LIST_ORDER.numListItemID AND LIST_ORDER.numDomainId = ' + CONVERT(VARCHAR,@numDomainID) + ' WHERE ( LIST.numDomainID = ' + CONVERT(VARCHAR,@numDomainID) + ' OR LIST.constFlag = 1) AND LIST.numListID = ' + CONVERT(VARCHAR,@numListIDValue) + ' AND  (LIST.numDomainID = ' + CONVERT(VARCHAR,@numDomainID) + ' OR constFlag = 1 )  ORDER BY intSortOrder '
						     END						
						     
			SET @vcTableNames = @vcTableNames + CONVERT(VARCHAR,@numFormFieldIDValue) + '_' + @strDBColumnName + ',' 
		END
	
	PRINT @vcTableNames
	PRINT @strSQL	
	
	SELECT @vcTableNames AS TableNames
	EXEC SP_EXECUTESQL @strSQL		
	
	DROP TABLE #tempDropDownData
	DROP TABLE #tempGroups
END
--Created by chintan
--EXEC [dbo].USP_GetAccountTypes 72,0,0
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAccountTypes')
DROP PROCEDURE USP_GetAccountTypes
GO
CREATE PROCEDURE [dbo].[USP_GetAccountTypes]
               @numDomainId      AS NUMERIC(9)  = 0,
               @numAccountTypeID AS NUMERIC(9)  = 0,
               @tintMode         TINYINT  = 0
AS
  BEGIN
    IF @tintMode = 1 -- select all account type with len(accountcode) = 4
      BEGIN
        SELECT [numAccountTypeID],
               [vcAccountCode],
               [vcAccountType]
                 + '-'
                 + [vcAccountCode] AS [vcAccountType],
                 [vcAccountType] AS [vcAccountType1],
               [numParentID],
               [numDomainID],
               [vcAccountCode] + '~' + CAST ([numAccountTypeID] AS VARCHAR(20)) AS CandidateKey
        FROM   [AccountTypeDetail]
        WHERE  [numDomainID] = @numDomainId
               AND LEN([vcAccountCode]) = 4
      END
    ELSE IF @tintMode = 2
		BEGIN
		DECLARE @vcParentAccountCode VARCHAR(50)
			SELECT @vcParentAccountCode=vcAccountCode FROM [AccountTypeDetail] WHERE numAccountTypeID=@numAccountTypeID
			SELECT [numAccountTypeID],
               [vcAccountCode],
               [vcAccountType]
                 + '-'
                 + [vcAccountCode] AS [vcAccountType],
                  [vcAccountType] AS [vcAccountType1],
               [numParentID],
               [numDomainID]
        FROM   [AccountTypeDetail]
        WHERE  [numDomainID] = @numDomainId
               AND LEN([vcAccountCode]) > 4
               AND [vcAccountCode] LIKE  @vcParentAccountCode +'%' 
--               AND [numAccountTypeID]  NOT IN ( SELECT [numParentID]
--        FROM   [AccountTypeDetail]
--        WHERE  [numDomainID] = @numDomainId
--               AND LEN([vcAccountCode]) > 4
--               AND [vcAccountCode] LIKE  @vcParentAccountCode +'%' )
		END
    ELSE IF @tintMode = 3 --Select all account type which has atleast one account 
	  SELECT    [numAccountTypeID],
				[vcAccountCode],
				[vcAccountType] /*+ '-' + [vcAccountCode]*/ AS [vcAccountType],
				[numParentID],
				[numDomainID]
	  FROM      [AccountTypeDetail]
	  WHERE     [numDomainID] = @numDomainId
				AND [numAccountTypeID] IN (
				SELECT  ISNULL([numParntAcntTypeId], 0)
				FROM    [Chart_Of_Accounts]
				WHERE   [numDomainId] = @numDomainId )
    ELSE 
      BEGIN
        IF @numAccountTypeID = 0
          BEGIN
            SELECT [numAccountTypeID],
                   [vcAccountCode],
                   [vcAccountType]
                     + '-'
                     + [vcAccountCode] AS [vcAccountType],
                   [vcAccountType] AS [vcAccountType1],
                   [numParentID],
                   [numDomainID]
            FROM   [AccountTypeDetail]
            WHERE  [numDomainID] = @numDomainId
                   AND [numParentID] IS NULL

            SELECT [numAccountTypeID],
                   [vcAccountCode],
                   [vcAccountType]
                     + '-'
                     + [vcAccountCode] AS [vcAccountType],
                     [vcAccountType] AS [vcAccountType1],
                   [numParentID],
                   [numDomainID]
            FROM   [AccountTypeDetail]
            WHERE  [numDomainID] = @numDomainId
                   AND [numParentID] IS NOT NULL
                   ORDER BY (CASE vcAccountType WHEN 'Assets' THEN 1 WHEN 'Liabilities' THEN 2 WHEN 'Equity' THEN 3 WHEN 'Income' THEN 4 WHEN 'Cost Of Goods' THEN 5 WHEN 'Expense' THEN 6 ELSE 7 END),numParentID,ISNULL(tintSortOrder,0),vcAccountCode
                   
          END
        ELSE
          IF @numAccountTypeID > 0
            BEGIN
              SELECT [numAccountTypeID],
                     [vcAccountCode],
                     [vcAccountType],
                     [numParentID],
                     [numDomainID],
					 [bitActive]
              FROM   [AccountTypeDetail]
              WHERE  [numDomainID] = @numDomainId
                     AND [numAccountTypeID] = @numAccountTypeID
            END
      END
  END


GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetAddressDetails' ) 
    DROP PROCEDURE USP_GetAddressDetails
GO
CREATE PROCEDURE USP_GetAddressDetails
    @tintMode TINYINT,
    @numDomainID NUMERIC,
    @numRecordID NUMERIC,
    @numAddressID NUMERIC,
    @tintAddressType TINYINT,
    @tintAddressOf TINYINT
AS 
BEGIN

    IF @tintMode = 1 
        BEGIN
            SELECT  numAddressID,
                    vcAddressName,
                    vcStreet,
                    vcCity,
                    vcPostalCode,
                    numState,
                    numCountry,
                    bitIsPrimary,
					bitResidential,
                    tintAddressOf,
                    tintAddressType,
                    '<pre>' + isnull(vcStreet,'') + '</pre> ' + isnull(vcCity,'') + ', '
                            + isnull(dbo.fn_GetState(numState),'') + ' '
                            + isnull(vcPostalCode,'') + ' <br>'
                            + isnull(dbo.fn_GetListItemName(numCountry),'') vcFullAddress
				 ,ISNULL((SELECT TOP 1 numWarehouseID FROM Warehouses WHERE numDomainID=@numDomainID AND numAddressID=AddressDetails.numAddressID),0) AS numWarehouseID
				 ,ISNULL(numContact,0) numContact
				 ,ISNULL(bitAltContact,0) bitAltContact
				 ,ISNULL(vcAltContact,'') vcAltContact
            FROM    dbo.AddressDetails
            WHERE   numDomainID = @numDomainID
                    AND numAddressID = @numAddressID
            
        END
    IF @tintMode = 2
        BEGIN
            SELECT  numAddressID,
                    vcAddressName,
					isnull(vcPostalCode,'') vcPostalCode,
					'<pre>' + isnull(vcStreet,'') + '</pre> ' + isnull(vcCity,'') + ', '
                            + isnull(dbo.fn_GetState(numState),'') + ' '
                            + isnull(vcPostalCode,'') + ' <br>'
                            + isnull(dbo.fn_GetListItemName(numCountry),'') vcFullAddress
					,ISNULL(numContact,0) numContact
					,ISNULL(bitAltContact,0) bitAltContact
					,ISNULL(vcAltContact,'') vcAltContact
					,(CASE WHEN @tintAddressOf=2 THEN ISNULL((SELECT TOP 1 numContactID FROM AdditionalCOntactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AddressDetails.numRecordID),0) ELSE 0 END) numPrimaryContact
					
					,isnull(vcStreet + ', ','')  + isnull(vcCity + ', ','') 
                            + isnull(dbo.fn_GetState(numState) + ', ','') 
                            + isnull(vcPostalCode + ', ','') 
                            + isnull(dbo.fn_GetListItemName(numCountry),'') FullAddress
		
            FROM    dbo.AddressDetails
            WHERE   numDomainID = @numDomainID
                    AND numRecordID = @numRecordID	
                    AND tintAddressOf = @tintAddressOf
                    AND tintAddressType = @tintAddressType
            ORDER BY bitIsPrimary desc
        END
    IF @tintMode = 3
        BEGIN
            SELECT  numAddressID,
                    vcAddressName,
                    vcStreet,
                    vcCity,
                    vcPostalCode,
                    numState,
                    numCountry,
                    bitIsPrimary,
					bitResidential,
                    tintAddressOf,
                    tintAddressType,
                    '<pre>' + isnull(vcStreet,'') + '</pre> ' + isnull(vcCity,'') + ', '
                            + isnull(dbo.fn_GetState(numState),'') + ' '
                            + isnull(vcPostalCode,'') + ' <br>'
                            + isnull(dbo.fn_GetListItemName(numCountry),'') vcFullAddress
            FROM    dbo.AddressDetails
            WHERE   numDomainID = @numDomainID
                    AND numRecordID = @numRecordID	
                    AND tintAddressOf = @tintAddressOf
                    AND tintAddressType = @tintAddressType
                    AND bitIsPrimary = 1
            ORDER BY bitIsPrimary desc
        END

		IF @tintMode = 4
        BEGIN
            SELECT  numAddressID,
                    vcAddressName,
					isnull(vcPostalCode,'') vcPostalCode
					,ISNULL(numContact,0) numContact
					,ISNULL(bitAltContact,0) bitAltContact
					,ISNULL(vcAltContact,'') vcAltContact
					,isnull(vcStreet + ', ','') + isnull(vcCity + ', ','')
                            + isnull(dbo.fn_GetState(numState) + ', ','')
                            + isnull(vcPostalCode + ', ','') 
                            + isnull(dbo.fn_GetListItemName(numCountry),'') FullAddress
					, ISNULL((SELECT TOP 1 vcStateCode FROM ShippingStateMaster where vcStateName= isnull(dbo.fn_GetState(numState),'')),'') AS StateAbbr
					, ISNULL((SELECT TOP 1 vcCountryCode from ShippingCountryMaster where vcCountryName= isnull(dbo.fn_GetListItemName(numCountry),'')),'') AS CountryAbbr
		
            FROM    dbo.AddressDetails
            WHERE   numDomainID = @numDomainID
                    AND numRecordID = @numRecordID	
                    --AND tintAddressOf = @tintAddressOf
                    --AND tintAddressType = @tintAddressType
					AND numAddressID = @numAddressID
            ORDER BY bitIsPrimary desc
        END
    
        

            
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCashFlowReport')
DROP PROCEDURE USP_GetCashFlowReport
GO
CREATE PROCEDURE USP_GetCashFlowReport
(
	@numDomainId as int,
	@dtFromDate AS DATETIME,
	@dtToDate as DATETIME,
	@ClientTimeZoneOffset INT,
	@numAccountClass AS NUMERIC(9)=0,                                           
	@DateFilter AS VARCHAR(20),
	@ReportColumn AS VARCHAR(20)
)
AS
BEGIN
	CREATE TABLE #View_Journal
	(
		numAccountId NUMERIC(18,0),
		datEntry_Date SMALLDATETIME,
		vcAccountCode VARCHAR(300),
		Debit DECIMAL(20,5),
		Credit DECIMAL(20,5)
	)

	INSERT INTO 
		#View_Journal
	SELECT 
		numAccountId
		,datEntry_Date
		,vcAccountCode
		,Debit
		,Credit 
	FROM 
		VIEW_JOURNAL 
	WHERE 
		numDomainId = @numDomainID 
		AND (numAccountClass=@numAccountClass OR @numAccountClass=0)

	DECLARE @PLCHARTID AS NUMERIC(18,0)
	SELECT TOP 1 @PLCHARTID=numAccountId FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND bitProfitLoss=1

	DECLARE @FinancialYearStartDate AS DATETIME
	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainId)

	DECLARE @TEMPQuarter AS TABLE
	(
		ID INT,
		StartDate DATETIME,
		EndDate DATETIME
	)

	INSERT INTO @TEMPQuarter VALUES (1,@FinancialYearStartDate,DATEADD(ms,-3,DATEADD(QUARTER,1,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (2,DATEADD(QUARTER,1,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,2,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (3,DATEADD(QUARTER,2,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,3,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (4,DATEADD(QUARTER,3,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,4,@FinancialYearStartDate)))

	IF @DateFilter = 'CurYear'
	BEGIN
		SELECT @dtFromDate = @FinancialYearStartDate,
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'PreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,DATEADD(YEAR,-1,@FinancialYearStartDate)));
	END
	ELSE IF @DateFilter = 'CurPreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'CuQur'
	BEGIN
		SELECT @dtFromDate = StartDate, @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'CurPreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'PreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=DATEADD(QUARTER,-1,EndDate) FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'LastMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)));  
	END
	ELSE IF @DateFilter = 'ThisMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0)));  
	END
	ELSE IF @DateFilter = 'CurPreMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0))); 
	END

	DECLARE @TEMP TABLE
	(
		ParentId VARCHAR(300)
		,vcCompundParentKey VARCHAR(300)
		,numAccountTypeID NUMERIC(18,0)
		,numAccountID NUMERIC(18,0)
		,vcAccountType VARCHAR(300)
		,vcAccountCode VARCHAR(300)
		,[LEVEL] INT
		,Struc VARCHAR(300)
		,bitTotal BIT
		,[Type] INT
	)

	INSERT INTO @TEMP
	(
		ParentId,vcCompundParentKey,numAccountTypeID,vcAccountType,[LEVEL],numAccountID,vcAccountCode,Struc,bitTotal,[Type]
	)
	VALUES
	(
		'','-1',-1,'Operating Activities',0,NULL,NULL,'-1',0,2
	),
	(
		'-1','-2',-2,'Net Income',1,NULL,NULL,'-1#2',0,1
	),
	(
		'-1','-3',-3,'Adjustments to reconcile Net Income to Net Cash provided by operations:',1,NULL,NULL,'-1#3',0,2
	),
	(
		'-1','-3',-3,'Total Adjustments to reconcile Net Income to Net Cash provided by operations:',1,NULL,NULL,'-1#3#Total',1,2
	),
	(
		'','-1',-1,'Net cash provided by operating activities',0,NULL,NULL,'-1#Total',1,2
	),
	(
		'','-4',-4,'Investing Activities',0,NULL,NULL,'-4',0,2
	),
	(
		'','-4',-4,'Net cash provided by investing activities',0,NULL,NULL,'-4#Total',1,2
	),
	(
		'','-5',-5,'Financing Activities',0,NULL,NULL,'-5',0,2
	),
	(
		'','-5',-5,'Net cash provided by financing activities',0,NULL,NULL,'-5#Total',1,2
	),
	(
		'','-6',-6,'Net cash increase for period',0,NULL,NULL,'-6',1,2
	),
	(
		'','-7',-7,'Net cash at Beginning of Period',0,NULL,NULL,'-7',0,2
	),
	(
		'','-8',-8,'Net cash at End of Period',0,NULL,NULL,'-8',1,2
	)

	-- Operating Activities
	;WITH DirectReport (ParentId,vcCompundParentKey,numAccountTypeID,vcAccountType,[LEVEL],numAccountID,vcAccountCode,Struc,bitTotal,[Type])
	AS
	(
		SELECT
			CAST('-3' AS VARCHAR)
			,CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR)
			,NULL
			,vcAccountName
			,2
			,numAccountID
			,vcAccountCode
			,CAST(CONCAT('-1#3#',numAccountID) AS VARCHAR(300))
			,0
			,1
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitIsSubAccount,0) = 0
			AND (vcAccountCode LIKE '01010105%' OR vcAccountCode LIKE '01010104%' OR vcAccountCode LIKE '010105%' OR vcAccountCode LIKE '01020102%' OR vcAccountCode LIKE '01020101%')
		UNION ALL
		SELECT 
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numParentAccId) AS VARCHAR) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
			NULL,
			vcAccountName,
			D.[LEVEL] + 1,
			[COA].[numAccountId],
			COA.[vcAccountCode],
			CAST(d.Struc + '#' + CAST(COA.[numAccountId] AS VARCHAR) AS VARCHAR(300)) AS Struc
			,0
			,1
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport] D 
		ON 
			D.numAccountId = COA.numParentAccId
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitIsSubAccount,0) = 1
			AND (COA.vcAccountCode LIKE '01010105%' OR COA.vcAccountCode LIKE '01010104%' OR COA.vcAccountCode LIKE '010105%' OR COA.vcAccountCode LIKE '01020102%' OR COA.vcAccountCode LIKE '01020101%')
	)

	INSERT INTO @TEMP
	(
		ParentId,vcCompundParentKey,numAccountTypeID,vcAccountType,[LEVEL],numAccountID,vcAccountCode,Struc,bitTotal,[Type]
	)
	SELECT 
		ParentId,vcCompundParentKey,numAccountTypeID,vcAccountType,[LEVEL],numAccountID,vcAccountCode,Struc,bitTotal,[Type]
	FROM
		DirectReport

	-- Investing Activities
	;WITH DirectReport (ParentId,vcCompundParentKey,numAccountTypeID,vcAccountType,[LEVEL],numAccountID,vcAccountCode,Struc,bitTotal,[Type])
	AS
	(
		SELECT
			CAST('-4' AS VARCHAR)
			,CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR)
			,NULL
			,vcAccountName
			,1
			,numAccountID
			,vcAccountCode
			,CAST(CONCAT('-4#',numAccountID) AS VARCHAR(300))
			,0
			,1
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitIsSubAccount,0) = 0
			AND vcAccountCode LIKE '010102%'
		UNION ALL
		SELECT 
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numParentAccId) AS VARCHAR) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
			NULL,
			[vcAccountName],
			LEVEL + 1,
			[COA].[numAccountId], 
			COA.[vcAccountCode],			
			CAST(d.Struc + '#' + CAST(COA.[numAccountId] AS VARCHAR) AS VARCHAR(300)) AS Struc
			,0
			,1
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport] D 
		ON 
			D.numAccountId = COA.numParentAccId
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitIsSubAccount,0) = 1
			AND COA.vcAccountCode LIKE '010102%'
	)

	INSERT INTO @TEMP
	(
		ParentId,vcCompundParentKey,numAccountTypeID,vcAccountType,[LEVEL],numAccountID,vcAccountCode,Struc,bitTotal,[Type]
	)
	SELECT 
		ParentId,vcCompundParentKey,numAccountTypeID,vcAccountType,[LEVEL],numAccountID,vcAccountCode,Struc,bitTotal,[Type]
	FROM
		DirectReport

	-- Financing Activities
	;WITH DirectReport (ParentId,vcCompundParentKey,numAccountTypeID,vcAccountType,[LEVEL],numAccountID,vcAccountCode,Struc,bitTotal,[Type])
	AS
	(
		SELECT
			CAST('-5'  AS VARCHAR)
			,CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR)
			,NULL
			,vcAccountName
			,1
			,numAccountID
			,vcAccountCode
			,CAST(CONCAT('-5#',numAccountID) AS VARCHAR(300))
			,0
			,1
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitIsSubAccount,0) = 0
			AND vcAccountCode LIKE '010202%'
		UNION ALL
		SELECT 
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numParentAccId) AS VARCHAR) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
			NULL,
			[vcAccountName],
			LEVEL + 1,
			[COA].[numAccountId],
			COA.[vcAccountCode],
			CAST(d.Struc + '#' + CAST(COA.[numAccountId] AS VARCHAR) AS VARCHAR(300)) AS Struc
			,0
			,1
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport] D 
		ON 
			D.numAccountId = COA.numParentAccId
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitIsSubAccount,0) = 1
			AND COA.vcAccountCode LIKE '010202%'
	)

	INSERT INTO @TEMP
	(
		ParentId,vcCompundParentKey,numAccountTypeID,vcAccountType,[LEVEL],numAccountID,vcAccountCode,Struc,bitTotal,[Type]
	)
	SELECT 
		ParentId,vcCompundParentKey,numAccountTypeID,vcAccountType,[LEVEL],numAccountID,vcAccountCode,Struc,bitTotal,[Type]
	FROM
		DirectReport

	DECLARE @columns VARCHAR(8000);--SET @columns = '';
	DECLARE @SUMColumns VARCHAR(8000);--SET @SUMColumns = '';
	DECLARE @PivotColumns VARCHAR(8000);--SET @PivotColumns = '';
	DECLARE @Select VARCHAR(8000) = 'SELECT ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType, LEVEL, vcAccountCode, numAccountId,Struc'
	DECLARE @Where VARCHAR(8000)
	DECLARE @Update6 VARCHAR(MAX) = ''
	DECLARE @Update8 VARCHAR(MAX) = ''
	DECLARE @sql VARCHAR(MAX) = ''

	IF @ReportColumn = 'Year'
	BEGIN
		SELECT 
			*
		INTO 
			#tempDirectReportYear
		FROM 
			@TEMP

		CREATE TABLE #tempYearMonth
		(
			intYear INT,
			intMonth INT,
			MonthYear VARCHAR(50),
			StartDate DATETIME,
			EndDate DATETIME,
			NetIncomeAmount DECIMAL(20,5),
			CashAtBegining DECIMAL(20,5)
		)

		;WITH CTE AS 
		(
			SELECT
				YEAR(@dtFromDate) AS 'yr',
				MONTH(@dtFromDate) AS 'mm',
				FORMAT(@dtFromDate,'MMM') AS 'mon',
				(FORMAT(@dtFromDate,'MMM') + '_' + CAST(YEAR(@dtFromDate) AS VARCHAR)) AS MonthYear,
				CAST(DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1) AS DATETIME) dtStartDate,
				DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1)) AS DATETIME)) dtEndDate,
				@dtFromDate 'new_date'
			UNION ALL
			SELECT
				YEAR(DATEADD(d,1,new_date)) AS 'yr',
				MONTH(DATEADD(d,1,new_date)) AS 'mm',
				FORMAT(DATEADD(d,1,new_date),'MMM') AS 'mon',
				(FORMAT(DATEADD(d,1,new_date),'MMM') + '_' + CAST(YEAR(DATEADD(d,1,new_date)) AS VARCHAR)) AS MonthYear,
				CAST(DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1) AS DATETIME) dtStartDate,
				DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1)) AS DATETIME)) dtEndDate,
				DATEADD(d,1,new_date) 'new_date'
			FROM CTE
			WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		INSERT INTO 
			#tempYearMonth
		SELECT 
			yr,mm,MonthYear,dtStartDate,dtEndDate,0,0
		FROM 
			CTE
		GROUP BY 
			yr, mm, MonthYear, dtStartDate, dtEndDate
		ORDER BY 
			yr, mm
		OPTION (MAXRECURSION 5000)

		INSERT INTO #tempYearMonth VALUES (5000,5000,'Total',@dtFromDate,@dtToDate,0,0)

		UPDATE 
			#tempYearMonth
		SET
			EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)

		UPDATE 
			#tempYearMonth
		SET
			NetIncomeAmount = ISNULL((SELECT 
											ISNULL(SUM(VJ.Credit),0) - ISNULL(SUM(VJ.Debit),0) 
										FROM 
											#View_Journal VJ
										WHERE 
											((VJ.vcAccountCode  like '0103%' OR VJ.vcAccountCode LIKE '0104%' OR VJ.vcAccountCode LIKE '0106%') OR VJ.numaccountid = @PLCHARTID) 
											AND VJ.datentry_date BETWEEN StartDate AND EndDate),0) + ISNULL((SELECT 
																												ISNULL(SUM(VJ.Credit),0) - ISNULL(SUM(VJ.Debit),0) 
																											FROM 
																												#View_Journal VJ
																											WHERE 
																												((VJ.vcAccountCode  like '0103%' OR VJ.vcAccountCode LIKE '0104%' OR VJ.vcAccountCode LIKE '0106%') OR VJ.numaccountid = @PLCHARTID) 
																												AND VJ.datentry_date <  StartDate),0)
			,CashAtBegining = ISNULL((SELECT 
											ISNULL(SUM(credit),0) - ISNULL(SUM(debit),0)
										FROM 
											#View_Journal vj 
										WHERE 
											vcAccountCode LIKE '01010101%' 
											AND datentry_date < StartDate),0)


		SELECT @columns = STUFF((SELECT ', ISNULL(' + MonthYear  + ',0) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 
		SELECT @sumColumns = STUFF((SELECT ', SUM(ISNULL(V.[' + REPLACE(MonthYear,'_',' ') + ']' + ',0)) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 
		SET @Update6 = CONCAT('UPDATE #tempFinalDataYear1 SET ',STUFF((SELECT ', [' + REPLACE(MonthYear,'_',' ') + '] = ISNULL((SELECT SUM(TInner.[' + REPLACE(MonthYear,'_',' ') + ']) FROM #tempFinalDataYear1 TInner WHERE TInner.numAccountTypeID IN (-1,-4,-5) AND bitTotal=1),0)' FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''),' WHERE numAccountTypeID=-6')
		SET @Update8 = CONCAT('UPDATE #tempFinalDataYear1 SET ',STUFF((SELECT ', [' + REPLACE(MonthYear,'_',' ') + '] = ISNULL((SELECT SUM(TInner.[' + REPLACE(MonthYear,'_',' ') + ']) FROM #tempFinalDataYear1 TInner WHERE TInner.numAccountTypeID IN (-6,-7)),0)' FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''),' WHERE numAccountTypeID=-8')

		SELECT @PivotColumns = STUFF((SELECT ', ' + MonthYear FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 


		SET @sql = 'SELECT
						ParentId,
						vcCompundParentKey,
						numAccountId,
						numAccountTypeID,
						vcAccountType,
						LEVEL,
						vcAccountCode,
						Struc,
						[Type],
						bitTotal,' + @columns + '
					INTO
						#tempFinalDataYear
					FROM
					(
						SELECT 
							T.ParentId,
							T.vcCompundParentKey,
							T.numAccountId,
							T.numAccountTypeID,
							T.vcAccountType,
							T.LEVEL,
							T.vcAccountCode,
							T.Struc,
							(CASE WHEN ISNULL(T.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
							T.bitTotal,
							(CASE 
								WHEN numAccountTypeID = -2
								THEN Period.NetIncomeAmount
								WHEN numAccountTypeID = -7
								THEN Period.CashAtBegining
								ELSE
									ISNULL(TCurrent.monAmount,0) - ISNULL(TOpening.monAmount,0)
							END) AS Amount,
							Period.MonthYear
						FROM 
							#tempDirectReportYear T
						OUTER APPLY
						(
							SELECT
								MonthYear,
								StartDate,
								EndDate,
								NetIncomeAmount,
								CashAtBegining
							FROM
								#tempYearMonth
						) AS Period
						OUTER APPLY
						(
							SELECT
								ISNULL(SUM(VF.Credit),0) - ISNULL(SUM(VF.debit),0) monAmount
							FROM
								#View_Journal VF 
							WHERE  
								VF.numAccountId = T.numAccountId
								AND VF.datEntry_Date < Period.StartDate
						) TOpening
						OUTER APPLY
						(
							SELECT
								ISNULL(SUM(VT.Credit),0) - ISNULL(SUM(VT.debit),0) monAmount
							FROM
								#View_Journal VT 
							WHERE  
								VT.numAccountId = T.numAccountId
								AND VT.datEntry_Date <= Period.EndDate
						) TCurrent
						
					) AS SourceTable
					pivot
					(
						SUM(AMOUNT)
						FOR [MonthYear] IN ( ' + @pivotcolumns + ' )
					) AS P; '

		SET @sql = CONCAT(@sql,'SELECT 
									COA.ParentId
									,COA.vcCompundParentKey
									,COA.numAccountTypeID
									,COA.vcAccountType
									,COA.[LEVEL]
									,COA.vcAccountCode
									,COA.numAccountId
									,(''#'' + COA.Struc + ''#'') AS Struc
									,COA.[Type]
									,COA.bitTotal
									,',@SUMColumns,' INTO #tempFinalDataYear1
								FROM 
									#tempDirectReportYear COA 
								LEFT JOIN 
									#tempFinalDataYear V
								ON  
									V.Struc like REPLACE(COA.Struc,''#Total'','''') + ''%''
								GROUP BY 
									COA.ParentId
									,COA.vcCompundParentKey
									,COA.numAccountTypeID
									,COA.vcAccountType
									,COA.LEVEL
									,COA.vcAccountCode
									,COA.numAccountId
									,COA.Struc
									,COA.[Type]
									,COA.bitTotal
								ORDER BY
									COA.Struc;',@Update6,';',@Update8,'; SELECT * FROM #tempFinalDataYear1; DROP TABLE #tempFinalDataYear; DROP TABLE #tempFinalDataYear1;')

		PRINT CAST(@sql AS NTEXT)

		EXECUTE (@sql)

		DROP TABLE #tempYearMonth
		DROP TABLE #tempDirectReportYear
		
	END
	ELSE IF @ReportColumn = 'Quarter'
	BEGIN
		SELECT 
			*
		INTO 
			#tempDirectReportQuarter
		FROM 
			@TEMP

		CREATE TABLE #TempYearMonthQuarter
		(
			intYear INT,
			intQuarter INT,
			MonthYear VARCHAR(50),
			StartDate DATETIME,
			EndDate DATETIME,
			NetIncomeAmount DECIMAL(20,5),
			CashAtBegining DECIMAL(20,5)
		)

		;WITH CTE AS 
		(
			SELECT
				dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS 'yr',
				dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS 'qq',
				('Q' + cast(dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS VARCHAR)) AS MonthYear,
				CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
				WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
				WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
				WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
				WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
				END AS dtStartDate,
				CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
				WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				END AS dtEndDate,
				@dtFromDate 'new_date'
			UNION ALL
			SELECT
				dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS 'yr',
				dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS 'qq',
				('Q' + cast(dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR)) AS MonthYear,
				CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
				WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
				WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
				WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
				WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
				END AS dtStartDate,
				CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
				WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				END AS dtEndDate,
				DATEADD(d,1,new_date) 'new_date'
			FROM CTE
			WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		INSERT INTO 
			#TempYearMonthQuarter 		
		SELECT 
			yr,qq,MonthYear,dtStartDate,dtEndDate,0,0
		FROM 
			CTE
		GROUP BY 
			yr, qq, MonthYear, dtStartDate, dtEndDate
		ORDER BY 
			yr, qq
		OPTION
			(MAXRECURSION 5000)

		INSERT INTO #TempYearMonthQuarter VALUES (5000,5000,'Total',@dtFromDate,@dtToDate,0,0)

		UPDATE 
			#TempYearMonthQuarter
		SET
			EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)

		UPDATE 
			#TempYearMonthQuarter
		SET
			NetIncomeAmount = ISNULL((SELECT 
											ISNULL(SUM(VJ.Credit),0) - ISNULL(SUM(VJ.Debit),0) 
										FROM 
											#View_Journal VJ
										WHERE 
											((VJ.vcAccountCode  like '0103%' OR VJ.vcAccountCode LIKE '0104%' OR VJ.vcAccountCode LIKE '0106%') OR VJ.numaccountid = @PLCHARTID) 
											AND VJ.datentry_date BETWEEN StartDate AND EndDate),0) + ISNULL((SELECT 
																												ISNULL(SUM(VJ.Credit),0) - ISNULL(SUM(VJ.Debit),0) 
																											FROM 
																												#View_Journal VJ
																											WHERE 
																												((VJ.vcAccountCode  like '0103%' OR VJ.vcAccountCode LIKE '0104%' OR VJ.vcAccountCode LIKE '0106%') OR VJ.numaccountid = @PLCHARTID) 
																												AND VJ.datentry_date <  StartDate),0)
			,CashAtBegining = ISNULL((SELECT 
											ISNULL(SUM(credit),0) - ISNULL(SUM(debit),0)
										FROM 
											#View_Journal vj 
										WHERE 
											vcAccountCode LIKE '01010101%' 
											AND datentry_date < StartDate),0)

		SELECT @columns = STUFF((SELECT ', ISNULL(' + MonthYear  + ',0) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''); 
		SELECT @sumColumns = STUFF((SELECT ', SUM(ISNULL(V.[' + REPLACE(MonthYear,'_',' ') + ']' + ',0)) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''); 
		SET @Update6 = CONCAT('UPDATE #tempFinalDataQuarter1 SET ',STUFF((SELECT ', [' + REPLACE(MonthYear,'_',' ') + '] = ISNULL((SELECT SUM(TInner.[' + REPLACE(MonthYear,'_',' ') + ']) FROM #tempFinalDataQuarter1 TInner WHERE TInner.numAccountTypeID IN (-1,-4,-5) AND bitTotal=1),0)' FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''),' WHERE numAccountTypeID=-6')
		SET @Update8 = CONCAT('UPDATE #tempFinalDataQuarter1 SET ',STUFF((SELECT ', [' + REPLACE(MonthYear,'_',' ') + '] = ISNULL((SELECT SUM(TInner.[' + REPLACE(MonthYear,'_',' ') + ']) FROM #tempFinalDataQuarter1 TInner WHERE TInner.numAccountTypeID IN (-6,-7)),0)' FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''),' WHERE numAccountTypeID=-8')

		SELECT @PivotColumns = STUFF((SELECT ', ' + MonthYear FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''); 


		SET @sql = 'SELECT
						ParentId,
						vcCompundParentKey,
						numAccountId,
						numAccountTypeID,
						vcAccountType,
						LEVEL,
						vcAccountCode,
						Struc,
						[Type],
						bitTotal,' + @columns + '
					INTO
						#tempFinalDataQuarter
					FROM
					(
						SELECT 
							T.ParentId,
							T.vcCompundParentKey,
							T.numAccountId,
							T.numAccountTypeID,
							T.vcAccountType,
							T.LEVEL,
							T.vcAccountCode,
							T.Struc,
							(CASE WHEN ISNULL(T.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
							T.bitTotal,
							(CASE 
								WHEN numAccountTypeID = -2
								THEN Period.NetIncomeAmount
								WHEN numAccountTypeID = -7
								THEN Period.CashAtBegining
								ELSE
									ISNULL(TCurrent.monAmount,0) - ISNULL(TOpening.monAmount,0)
							END) AS Amount,
							Period.MonthYear
						FROM 
							#tempDirectReportQuarter T
						OUTER APPLY
						(
							SELECT
								MonthYear,
								StartDate,
								EndDate,
								NetIncomeAmount,
								CashAtBegining
							FROM
								#TempYearMonthQuarter
						) AS Period
						OUTER APPLY
						(
							SELECT
								ISNULL(SUM(VF.Credit),0) - ISNULL(SUM(VF.debit),0) monAmount
							FROM
								#View_Journal VF 
							WHERE  
								VF.numAccountId = T.numAccountId
								AND VF.datEntry_Date < Period.StartDate
						) TOpening
						OUTER APPLY
						(
							SELECT
								ISNULL(SUM(VT.Credit),0) - ISNULL(SUM(VT.debit),0) monAmount
							FROM
								#View_Journal VT 
							WHERE  
								VT.numAccountId = T.numAccountId
								AND VT.datEntry_Date <= Period.EndDate
						) TCurrent
						
					) AS SourceTable
					pivot
					(
						SUM(AMOUNT)
						FOR [MonthYear] IN ( ' + @pivotcolumns + ' )
					) AS P; '

		SET @sql = CONCAT(@sql,'SELECT 
									COA.ParentId
									,COA.vcCompundParentKey
									,COA.numAccountTypeID
									,COA.vcAccountType
									,COA.[LEVEL]
									,COA.vcAccountCode
									,COA.numAccountId
									,(''#'' + COA.Struc + ''#'') AS Struc
									,COA.[Type]
									,COA.bitTotal
									,',@SUMColumns,' INTO #tempFinalDataQuarter1
								FROM 
									#tempDirectReportQuarter COA 
								LEFT JOIN 
									#tempFinalDataQuarter V
								ON  
									V.Struc like REPLACE(COA.Struc,''#Total'','''') + ''%''
								GROUP BY 
									COA.ParentId
									,COA.vcCompundParentKey
									,COA.numAccountTypeID
									,COA.vcAccountType
									,COA.LEVEL
									,COA.vcAccountCode
									,COA.numAccountId
									,COA.Struc
									,COA.[Type]
									,COA.bitTotal
								ORDER BY
									COA.Struc;',@Update6,';',@Update8,'; SELECT * FROM #tempFinalDataQuarter1; DROP TABLE #tempFinalDataQuarter; DROP TABLE #tempFinalDataQuarter1;')

		PRINT CAST(@sql AS NTEXT)

		EXECUTE (@sql)

		DROP TABLE #TempYearMonthQuarter
		DROP TABLE #tempDirectReportQuarter
	END
	ELSE 
	BEGIN
		SELECT 
			COA.ParentId
			,COA.vcCompundParentKey
			,COA.numAccountTypeID
			,COA.vcAccountType
			,COA.[LEVEL]
			,COA.vcAccountCode
			,COA.numAccountId
			,COA.Struc
			,COA.[Type]
			,COA.bitTotal
			,0 Total
		INTO 
			#tempDirectReportMain 
		FROM 
			 @TEMP COA

		UPDATE
			T
		SET
			T.Total = ISNULL(TCurrent.monAmount,0) - ISNULL(TOpening.monAmount,0)
		FROM
			#tempDirectReportMain  T
		OUTER APPLY
		(
			SELECT
				ISNULL(SUM(VF.Credit),0) - ISNULL(SUM(VF.debit),0) monAmount
			FROM
				#View_Journal VF 
			WHERE  
				VF.numAccountId = T.numAccountId
				AND VF.datEntry_Date < @dtFromDate
		) TOpening
		OUTER APPLY
		(
			SELECT
				ISNULL(SUM(VT.Credit),0) - ISNULL(SUM(VT.debit),0) monAmount
			FROM
				#View_Journal VT 
			WHERE  
				VT.numAccountId = T.numAccountId
				AND VT.datEntry_Date <= @dtToDate
		) TCurrent
		WHERE 
			T.[numAccountId] IS NOT NULL

		UPDATE
			#tempDirectReportMain
		SET
			Total = ISNULL((SELECT 
								ISNULL(SUM(VJ.Credit),0) - ISNULL(SUM(VJ.Debit),0) 
							FROM 
								#View_Journal VJ
							WHERE 
								((VJ.vcAccountCode  like '0103%' OR VJ.vcAccountCode LIKE '0104%' OR VJ.vcAccountCode LIKE '0106%') OR VJ.numaccountid = @PLCHARTID) 
								AND VJ.datentry_date BETWEEN @dtFromDate AND @dtToDate),0) + ISNULL((SELECT 
																									ISNULL(SUM(VJ.Credit),0) - ISNULL(SUM(VJ.Debit),0) 
																								FROM 
																									#View_Journal VJ
																								WHERE 
																									((VJ.vcAccountCode  like '0103%' OR VJ.vcAccountCode LIKE '0104%' OR VJ.vcAccountCode LIKE '0106%') OR VJ.numaccountid = @PLCHARTID) 
																									AND VJ.datentry_date <  @dtFromDate),0)
		WHERE
			numAccountTypeID=-2

		SELECT 
			ParentId
			,vcCompundParentKey
			,numAccountTypeID
			,vcAccountType
			,[LEVEL]
			,vcAccountCode
			,numAccountId
			,('#' + Struc + '#') AS Struc
			,[Type]
			,bitTotal
			,Total
		INTO 
			#tempFinalDataMain
		FROM
		(
			SELECT 
				COA.ParentId
				,COA.vcCompundParentKey
				,COA.numAccountTypeID
				,COA.vcAccountType
				,COA.[LEVEL]
				,COA.vcAccountCode
				,COA.numAccountId
				,COA.Struc
				,COA.[Type]
				,COA.bitTotal
				,SUM(ISNULL(V.Total,0)) AS Total
			FROM 
				@TEMP COA 
			LEFT JOIN 
				#tempDirectReportMain V
			ON  
				V.Struc like REPLACE(COA.Struc,'#Total','') + '%'
			GROUP BY 
				COA.ParentId
				,COA.vcCompundParentKey
				,COA.numAccountTypeID
				,COA.vcAccountType
				,COA.LEVEL
				,COA.vcAccountCode
				,COA.numAccountId
				,COA.Struc
				,COA.[Type]
				,COA.bitTotal			
		) AS t
		ORDER BY 
			Struc

		UPDATE 
			#tempFinalDataMain
		SET
			Total = ISNULL((SELECT SUM(Total) FROM #tempFinalDataMain TInner WHERE TInner.numAccountTypeID IN (-1,-4,-5) AND bitTotal=1),0)
		WHERE
			numAccountTypeID=-6

		UPDATE 
			#tempFinalDataMain
		SET
			Total = ISNULL((SELECT 
								ISNULL(SUM(credit),0) - ISNULL(SUM(debit),0)
							FROM 
								#View_Journal vj 
							WHERE 
								vcAccountCode LIKE '01010101%' 
								AND datentry_date < @dtFromDate),0)
		WHERE
			numAccountTypeID=-7

		UPDATE 
			#tempFinalDataMain
		SET
			Total =  ISNULL((SELECT SUM(Total) FROM #tempFinalDataMain TInner WHERE TInner.numAccountTypeID IN (-6,-7)),0)
		WHERE
			numAccountTypeID=-8


		SELECT * FROM #tempFinalDataMain

		DROP TABLE #tempDirectReportMain
		DROP TABLE #tempFinalDataMain
	END

	DROP TABLE #View_Journal
	
END
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

-- Created by Siva    

-- [USP_GetChartAcntDetailsForBalanceSheet] 72,'2008-09-28 17:09:54.263','2009-09-28 17:09:54.263'
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getchartacntdetailsforbalancesheet')
DROP PROCEDURE usp_getchartacntdetailsforbalancesheet
GO
CREATE PROCEDURE [dbo].[USP_GetChartAcntDetailsForBalanceSheet]                                 
@numDomainId as numeric(9),                                                  
@dtFromDate as datetime,                                                
@dtToDate as DATETIME,
@ClientTimeZoneOffset INT, --Added by Chintan to enable calculation of date according to client machine                                                   
@numFRID AS NUMERIC=0,
@numAccountClass AS NUMERIC(9)=0

--@tintByteMode as tinyint                                                            
As                                                                
Begin                                                                
DECLARE @CURRENTPL DECIMAL(20,5) ;
DECLARE @PLCHARTID NUMERIC(8);
DECLARE @PLOPENING DECIMAL(20,5);
--DECLARE @numFinYear INT;
--DECLARE @dtFinYearFrom datetime;
DECLARE @TotalIncome DECIMAL(20,5);
DECLARE @TotalExpense DECIMAL(20,5);
DECLARE @TotalCOGS DECIMAL(20,5);


select * into #VIEW_JOURNALBS from VIEW_JOURNALBS where numdomainid=@numDomainId AND (numAccountClass=@numAccountClass OR @numAccountClass=0) ; 
SELECT * INTO #view_journal FROM view_journal WHERE numDomainID=@numDomainId AND (numAccountClass=@numAccountClass OR @numAccountClass=0) ;

--Select Financial Year and From Date
--SELECT @numFinYear=numFinYearId,@dtFinYearFrom=dtPeriodFrom FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND  
--dtPeriodTo >=@dtFromDate AND numDomainId=@numDomainId

--SELECT @dtFromDate=MIN(datEntry_Date) FROM dbo.VIEW_JOURNAL WHERE numDomainID=@numDomainId
--Reflact timezone difference in From and To date
--SET @dtFromDate = DateAdd(minute, @ClientTimeZoneOffset, @dtFromDate);

  /*Comment by chintan
        Reason: Standard US was facing balance not matching for undeposited func(check video for more info)
        We do not need timezone settings since all journal entry date does not store time. we only store date. 
        */
SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate);
PRINT '@dtToDate :' 
PRINT @dtToDate
--PRINT @numFinYear
--PRINT @dtFinYearFrom
PRINT @dtFromDate
--RETURN
--select * from VIEW_JOURNALPL where numDomainid=72

--------------------------------------------------------
-- GETTING P&L VALUE
SET @CURRENTPL =0;	
SET @PLOPENING=0;

--SELECT @CURRENTPL = /*ISNULL(SUM(Opening),0)+*/
--+
--ISNULL(sum(Credit),0)- ISNULL(sum(Debit),0) FROM
--VIEW_DAILY_INCOME_EXPENSE P WHERE 
--numDomainID=@numDomainId AND datEntry_Date between @dtFromDate and @dtToDate ;


SELECT @TotalIncome = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID AND vcAccountCode  LIKE '0103%' AND datEntry_Date between @dtFromDate and @dtToDate ;
SELECT @TotalExpense = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID AND vcAccountCode  LIKE '0104%' AND datEntry_Date between @dtFromDate and @dtToDate ;
SELECT @TotalCOGS =  ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID AND vcAccountCode  LIKE '0106%' AND datEntry_Date between @dtFromDate and @dtToDate ;
PRINT 'TotalIncome=									' + CAST(@TotalIncome AS VARCHAR(20))
PRINT 'TotalExpense=									'+ CAST(@TotalExpense AS VARCHAR(20))
PRINT 'TotalCOGS=										'+ CAST(@TotalCOGS AS VARCHAR(20))
SET @CURRENTPL = (@TotalIncome + @TotalExpense + @TotalCOGS)
PRINT 'Current Profit/Loss = (Income - expense - cogs)= ' + CAST( @CURRENTPL AS VARCHAR(20))



PRINT @CURRENTPL


SELECT @PLCHARTID = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId=@numDomainID AND bitProfitLoss=1
PRINT 'PL Account ID=' +CAST( @PLCHARTID AS VARCHAR(20))
SELECT @PLOPENING = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID 
/* AND numAccountId=@PLCHARTID*/
AND ( vcAccountCode  LIKE '0103%' or  vcAccountCode  LIKE '0104%' or  vcAccountCode  LIKE '0106%')
AND datEntry_Date <=  DATEADD(Minute,-1,@dtFromDate);

SELECT @PLOPENING = ISNULL(@PLOPENING,0) + ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID
AND numAccountId=@PLCHARTID /*AND datEntry_Date <=  DATEADD(Minute,-1,@dtFromDate);*/


PRINT 'PL Account Opening=' +CAST( @PLOPENING AS VARCHAR(20))
SET @CURRENTPL = @PLOPENING + @CURRENTPL
PRINT 'PL = ' + CAST( @CURRENTPL AS VARCHAR(20))






CREATE TABLE #PLSummary (numAccountId numeric(9),vcAccountName varchar(250),
numParntAcntTypeID numeric(9),vcAccountDescription varchar(250),
vcAccountCode varchar(50) COLLATE Database_Default,Opening DECIMAL(20,5),Debit DECIMAL(20,5),Credit DECIMAL(20,5),bitIsSubAccount Bit);

INSERT INTO  #PLSummary
SELECT COA.numAccountId,vcAccountName,numParntAcntTypeID,vcAccountDescription,vcAccountCode,


/*isnull((SELECT SUM(isnull(monOpening,0)) from CHARTACCOUNTOPENING CAO WHERE
numFinYearId=@numFinYear and numDomainID=@numDomainId 
AND CAO.numAccountId IN (SELECT numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountCode like COA.vcAccountCode + '%' )  ),0) 

+*/

/*ISNULL((SELECT sum(ISNULL(Debit,0)-ISNULL(Credit,0)) FROM #VIEW_JOURNALBS VJ
WHERE VJ.numDomainId=@numDomainId AND
	VJ.COAvcAccountCode like COA.vcAccountCode + '%' /*VJ.numAccountId=COA.numAccountId*/ AND
	datEntry_Date <= DATEADD(Minute,-1,@dtFromDate)),0)*/ 0 AS OPENING,

ISNULL((SELECT sum(ISNULL(Debit,0)) FROM #VIEW_JOURNALBS VJ
WHERE VJ.numDomainId=@numDomainId AND
	VJ.COAvcAccountCode like COA.vcAccountCode + '%' /*VJ.numAccountId=COA.numAccountId*/ AND
	datEntry_Date <= @dtToDate ),0) as DEBIT,

ISNULL((SELECT sum(ISNULL(Credit,0)) FROM #VIEW_JOURNALBS VJ
WHERE VJ.numDomainId=@numDomainId AND
	VJ.COAvcAccountCode like COA.vcAccountCode + '%' /*VJ.numAccountId=COA.numAccountId*/ AND
	datEntry_Date <= @dtToDate ),0) as CREDIT
,ISNULL(COA.bitIsSubAccount,0)
FROM Chart_of_Accounts COA
WHERE COA.numDomainId=@numDomainId  AND COA.bitActive = 1  AND
		ISNULL(bitProfitLoss,0)=0 AND
      (COA.vcAccountCode LIKE '0101%' OR
       COA.vcAccountCode LIKE '0102%' OR
       COA.vcAccountCode LIKE '0105%') 

UNION


/*(* -1) added by chintan to fix bug 2148 #3  */
SELECT @PLCHARTID,vcAccountName,numParntAcntTypeID,vcAccountDescription,
vcAccountCode,
@CURRENTPL,0,0,ISNULL(COA.bitIsSubAccount,0)
 FROM Chart_of_Accounts COA WHERE numDomainID=@numDomainId AND COA.bitActive = 1 and
bitProfitLoss=1 AND COA.numAccountId=@PLCHARTID;


/*Added by chintan bugid 962 #3, For presentation do follwoing Show Credit amount in Posite and Debit Amount in Negative (only for accounts under quaity except PL account)*/
UPDATE #PLSummary SET 
Opening =Opening * (-1),--CASE WHEN Opening <0 THEN Opening * (-1) ELSE Opening END ,
Debit = Debit * (-1),--CASE WHEN Debit >0 THEN Debit * (-1) ELSE Debit END,
Credit = Credit * (-1)--CASE WHEN Credit <0 THEN Credit * (-1) ELSE Credit END 
WHERE vcAccountCode LIKE '0105%' AND numAccountId <> @PLCHARTID;



CREATE TABLE #PLOutPut (numAccountId numeric(9),vcAccountName varchar(250),
numParntAcntTypeID numeric(9),vcAccountDescription varchar(250),
vcAccountCode varchar(50) COLLATE Database_Default,Opening DECIMAL(20,5),Debit DECIMAL(20,5),Credit DECIMAL(20,5));
--insert account types and balances
INSERT INTO #PLOutPut
SELECT ATD.numAccountTypeID,ATD.vcAccountType,ATD.numParentID, '',ATD.vcAccountCode,
 ISNULL(SUM(ISNULL(Opening,0)),0) as Opening,
ISNUlL(Sum(ISNULL(Debit,0)),0) as Debit,ISNULL(Sum(ISNULL(Credit,0)),0) as Credit
FROM 
 AccountTypeDetail ATD RIGHT OUTER JOIN 
#PLSummary PL ON
PL.vcAccountCode LIKE ATD.vcAccountCode + '%'
AND ATD.numDomainId=@numDomainId AND
(ATD.vcAccountCode LIKE '0101%' OR
       ATD.vcAccountCode LIKE '0102%' OR
       ATD.vcAccountCode LIKE '0105%')
WHERE PL.bitIsSubAccount=0
GROUP BY 
ATD.numAccountTypeID,ATD.vcAccountCode,ATD.vcAccountType,ATD.numParentID;

ALTER TABLE #PLSummary
DROP COLUMN bitIsSubAccount

CREATE TABLE #PLShow (numAccountId numeric(9),vcAccountName varchar(250),
numParntAcntTypeID numeric(9),vcAccountDescription varchar(250),
vcAccountCode varchar(50),Opening DECIMAL(20,5),Debit DECIMAL(20,5),Credit DECIMAL(20,5),
Balance DECIMAL(20,5),AccountCode1  varchar(100),vcAccountName1 varchar(250),[Type] INT);





INSERT INTO #PLShow(#PLShow.numAccountId,
					#PLShow.vcAccountName,
					#PLShow.numParntAcntTypeID,
					#PLShow.vcAccountDescription,
					#PLShow.vcAccountCode,
					#PLShow.Opening,
					#PLShow.Debit,
					#PLShow.Credit,
					#PLShow.Balance,
					#PLShow.AccountCode1,
					#PLShow.vcAccountName1,
					#PLShow.[Type])	

 
SELECT numAccountId,
	   vcAccountName,
	   numParntAcntTypeID,
	   vcAccountDescription,
	   vcAccountCode,
	   Opening,
	   Debit,
	   Credit,(Opening * case substring(P.[vcAccountCode],1,4) when '0102' then (-1) else 1 end)
+(Debit * case substring(P.[vcAccountCode],1,4) when '0102' then (-1) else 1 end )
-(Credit * case substring(P.[vcAccountCode],1,4) when '0102' then (-1) else 1 end) as Balance,
CASE 
 WHEN LEN(P.[vcAccountCode])>4 THEN REPLICATE('&nbsp;', LEN(P.[vcAccountCode])-4) + P.[vcAccountCode] 
 ELSE  P.[vcAccountCode]
 END AS AccountCode1,
 CASE 
 WHEN LEN(P.[vcAccountCode])>4 THEN REPLICATE('&nbsp;', LEN(P.[vcAccountCode])-4) + P.[vcAccountName]
 ELSE P.[vcAccountName]
 END AS vcAccountName1, 1 AS Type
 FROM #PLSummary P

UNION

SELECT numAccountId,
	   vcAccountName,
	   numParntAcntTypeID,
	   vcAccountDescription,
	   vcAccountCode,
	   Opening,
	   Debit,
	   Credit,(Opening  * case substring(O.[vcAccountCode],1,4) when '0102' then (-1) else 1 end)+
(Debit * case substring(O.[vcAccountCode],1,4) when '0102' then (-1) else 1 end)-
(Credit * case substring(O.[vcAccountCode],1,4) when '0102' then (-1) else 1 end) as Balance,

CASE 
 WHEN LEN(O.[vcAccountCode])>4 THEN REPLICATE('&nbsp;', LEN(O.[vcAccountCode])-4) +O.[vcAccountCode] 
 ELSE  O.[vcAccountCode]
 END AS AccountCode1,
 CASE 
 WHEN LEN(O.[vcAccountCode])>4 THEN REPLICATE('&nbsp;', LEN(O.[vcAccountCode])-4) + O.[vcAccountName]
 ELSE O.[vcAccountName]
 END AS vcAccountName1, 2 as Type
 FROM #PLOutPut O;

UPDATE #PLShow SET [TYPE]=3 WHERE numAccountId=@PLCHARTID;




select  A.numAccountId ,
        A.vcAccountName ,
        A.numParntAcntTypeID ,
        A.vcAccountDescription ,
        A.vcAccountCode ,
        A.Opening ,
        A.Debit ,
        A.Credit ,
        A.Balance ,
        A.AccountCode1 ,
        CASE WHEN LEN(A.[vcAccountCode]) > 4
                     THEN REPLICATE('&nbsp;',LEN(A.[vcAccountCode]) - 4 )+ A.[vcAccountName]
                     ELSE A.[vcAccountName]
                END AS vcAccountName1, A.vcAccountCode ,
        A.Type	from #PLShow A ORDER BY A.vcAccountCode;

DROP TABLE #PLOutPut;
DROP TABLE #PLSummary;
DROP TABLE #PLShow;
DROP TABLE #VIEW_JOURNALBS;
 End

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetChartAcntDetailsForBalanceSheet_New')
DROP PROCEDURE USP_GetChartAcntDetailsForBalanceSheet_New
GO
CREATE PROCEDURE [dbo].[USP_GetChartAcntDetailsForBalanceSheet_New]                                 
@numDomainId AS NUMERIC(9),                                                  
@dtFromDate AS DATETIME,                                                
@dtToDate AS DATETIME,
@ClientTimeZoneOffset INT, 
@numAccountClass AS NUMERIC(9)=0,
@DateFilter AS VARCHAR(20),
@ReportColumn AS VARCHAR(20),
@bitAddTotalRow BIT                                                        
AS                                                                
BEGIN            
	SET @dtFromDate = DATEADD (MS , 000 , @dtFromDate)
	SET @dtToDate = DATEADD (MS , 997 , @dtToDate)
                                                    
	DECLARE @PLAccountStruc VARCHAR(500)
	DECLARE @FinancialYearStartDate AS DATETIME
	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainId)

	DECLARE @TEMPQuarter AS TABLE
	(
		ID INT,
		StartDate DATETIME,
		EndDate DATETIME
	)

	INSERT INTO @TEMPQuarter VALUES (1,@FinancialYearStartDate,DATEADD(ms,-3,DATEADD(QUARTER,1,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (2,DATEADD(QUARTER,1,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,2,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (3,DATEADD(QUARTER,2,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,3,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (4,DATEADD(QUARTER,3,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,4,@FinancialYearStartDate)))

	IF @DateFilter = 'CurYear'
	BEGIN
		SELECT @dtFromDate = @FinancialYearStartDate,
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'PreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,DATEADD(YEAR,-1,@FinancialYearStartDate)));
	END
	ELSE IF @DateFilter = 'CurPreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'CuQur'
	BEGIN
		SELECT @dtFromDate = StartDate, @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'CurPreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'PreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=DATEADD(QUARTER,-1,EndDate) FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'LastMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)));  
	END
	ELSE IF @DateFilter = 'ThisMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0)));  
	END
	ELSE IF @DateFilter = 'CurPreMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0))); 
	END

	CREATE TABLE #VIEW_JOURNALBS
	(
		numDomainID NUMERIC(18,0),
		numAccountID NUMERIC(18,0),
		numAccountClass NUMERIC(18,0),
		COAvcAccountCode VARCHAR(50),
		datEntry_Date DATETIME,
		Debit DECIMAL(20,5),
		Credit DECIMAL(20,5)
	)

	INSERT INTO #VIEW_JOURNALBS SELECT numDomainID,numAccountID,numAccountClass,COAvcAccountCode,datEntry_Date,Debit,Credit FROM VIEW_JOURNALBS_MASTER WHERE numDomainID =@numDomainId AND (numAccountClass=@numAccountClass OR @numAccountClass=0) ORDER BY numAccountId; 

	DECLARE @view_journal TABLE
	(
		numAccountId NUMERIC(18,0),
		vcAccountCode VARCHAR(50),
		datEntry_Date SMALLDATETIME,
		Debit DECIMAL(20,5),
		Credit DECIMAL(20,5)
	)

	INSERT INTO @view_journal SELECT numAccountId,vcAccountCode,datEntry_Date,Debit,Credit FROM view_journal WHERE numDomainId = @numDomainID AND (numAccountClass=@numAccountClass OR @numAccountClass=0);
	
	DECLARE @PLCHARTID NUMERIC(8)
	SELECT @PLCHARTID = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId=@numDomainID AND bitProfitLoss=1

	DECLARE @PLOPENING AS DECIMAL(20,5)
	SELECT @PLOPENING = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE numAccountId=@PLCHARTID

	;WITH DirectReport (ParentId, vcCompundParentKey, numAccountTypeID, numAccountID, vcAccountType,vcAccountCode, LEVEL,Struc,bitTotal)
	AS
	(
		SELECT 
			CAST('' AS VARCHAR) AS ParentId, 
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
			[ATD].[numAccountTypeID],
			0,			
			CAST([ATD].[vcAccountType] AS VARCHAR(300)),
			[ATD].[vcAccountCode],
			1 AS LEVEL, 
			CAST(CONCAT(FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),[ATD].[vcAccountCode]) AS VARCHAR(300))  AS Struc,
			0
		FROM 
			[dbo].[AccountTypeDetail] AS ATD
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND [ATD].[vcAccountCode] IN ('0101','0102','0105')
			AND ATD.bitActive = 1
		UNION ALL
		SELECT 
			CAST('' AS VARCHAR) AS ParentId, 
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
			[ATD].[numAccountTypeID],
			0,
			CAST(CONCAT('Total ',[ATD].[vcAccountType]) AS VARCHAR(300)),
			[ATD].[vcAccountCode],
			1 AS LEVEL, 
			CAST(CONCAT(FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),[ATD].[vcAccountCode],'#Total') AS VARCHAR(300))  AS Struc,
			1
		FROM 
			[dbo].[AccountTypeDetail] AS ATD
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND [ATD].[vcAccountCode] IN ('0101','0102','0105')
			AND ATD.bitActive = 1
			AND ISNULL(@bitAddTotalRow,0) = 1
		UNION ALL
		SELECT 
			D.vcCompundParentKey AS ParentId,
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
			[ATD].[numAccountTypeID],
			0,
			CAST([ATD].[vcAccountType] AS VARCHAR(300)),
			[ATD].[vcAccountCode],
			LEVEL + 1, 
			CAST(d.Struc + '#' + CONCAT(FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),[ATD].[vcAccountCode]) AS VARCHAR(300))  AS Struc,
			0
		FROM 
			[dbo].[AccountTypeDetail] AS ATD 
		JOIN 
			[DirectReport] D 
		ON 
			D.[numAccountTypeID] = [ATD].[numParentID]
			AND D.bitTotal = 0
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND ATD.bitActive = 1
		UNION ALL
		SELECT 
			D.vcCompundParentKey AS ParentId,
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
			[ATD].[numAccountTypeID],
			0,
			CAST(CONCAT('Total ',[ATD].[vcAccountType]) AS VARCHAR(300)),
			[ATD].[vcAccountCode],
			LEVEL + 1, 
			CAST(CONCAT(d.Struc,'#',FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),[ATD].[vcAccountCode],'#Total') AS VARCHAR(300))  AS Struc,
			1
		FROM 
			[dbo].[AccountTypeDetail] AS ATD 
		JOIN 
			[DirectReport] D 
		ON 
			D.[numAccountTypeID] = [ATD].[numParentID]
			AND D.bitTotal = 0
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND ATD.bitActive = 1
			AND ISNULL(@bitAddTotalRow,0) = 1
	),
	DirectReport1 (ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,numAccountId,Struc, bitProfitLoss, bitTotal)
	AS
	(
		SELECT 
			ParentId, 
			vcCompundParentKey,
			numAccountTypeID, 
			vcAccountType,
			vcAccountCode, 
			LEVEL,
			CAST(numAccountID AS NUMERIC(18)),
			Struc,
			CAST(0 AS BIT),
			bitTotal
		FROM 
			DirectReport 
		UNION ALL
		SELECT 
			CAST(CONCAT(COA.numParntAcntTypeId,'#',0) AS VARCHAR) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
			CAST(NULL AS NUMERIC(18)), 
			CAST([vcAccountName] AS VARCHAR(300)),
			COA.[vcAccountCode],
			LEVEL + 1,
			[COA].[numAccountId], 
			CAST(d.Struc + '#' + CAST(COA.[numAccountId] AS VARCHAR) AS VARCHAR(300)) AS Struc,
			COA.bitProfitLoss,
			0
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport1] D 
		ON 
			D.[numAccountTypeID] = COA.[numParntAcntTypeId]
			AND D.bitTotal = 0
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitActive,0) = 1
			AND ISNULL(COA.bitIsSubAccount,0) = 0
		UNION ALL
		SELECT 
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numParentAccId) AS VARCHAR) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
			CAST(NULL AS NUMERIC(18)),
			CAST([vcAccountName] AS VARCHAR(300)),
			COA.[vcAccountCode],
			LEVEL + 1,
			[COA].[numAccountId], 
			CAST(d.Struc + '#' + CAST(COA.[numAccountId] AS VARCHAR) AS VARCHAR(300)) AS Struc,
			COA.bitProfitLoss
			,0
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport1] D 
		ON 
			D.numAccountId = COA.numParentAccId
			AND D.bitTotal = 0
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitActive,0) = 1
			AND ISNULL(COA.bitIsSubAccount,0) = 1
	)

  
	SELECT 
		ParentId,
		vcCompundParentKey,
		numAccountTypeID, 
		vcAccountType, 
		LEVEL, 
		vcAccountCode,
		numAccountId,
		Struc,
		bitProfitLoss,
		bitTotal
	INTO 
		#tempDirectReport
	FROM 
		DirectReport1

	SELECT 
		COA.ParentId, 
		COA.vcCompundParentKey,
		COA.numAccountTypeID, 
		COA.vcAccountType, 
		COA.LEVEL, 
		COA.vcAccountCode, 
		COA.numAccountId, 
		COA.Struc,
		COA.bitTotal,
		(CASE 
			WHEN COA.vcAccountCode LIKE '0105%' AND COA.numAccountId <> @PLCHARTID THEN (ISNULL(Debit,0) * (-1) - ISNULL(Credit,0) * (-1)) 
			WHEN COA.vcAccountCode LIKE '0102%' THEN (ISNULL(Debit,0) * (-1) - ISNULL(Credit,0) * (-1)) 
			ELSE ISNULL(Debit,0) - ISNULL(Credit,0) 
		END) AS Amount,
		V.datEntry_Date
	INTO 
		#tempViewData
	FROM 
		#tempDirectReport COA 
	LEFT JOIN 
		#VIEW_JOURNALBS V 
	ON  
		COA.numAccountId = V.numAccountID 
		AND datEntry_Date <= @dtToDate
	WHERE 
		COA.[numAccountId] IS NOT NULL
	
	DECLARE @columns VARCHAR(8000);--SET @columns = '';
	DECLARE @SUMColumns VARCHAR(8000);--SET @SUMColumns = '';
	DECLARE @PivotColumns VARCHAR(8000);--SET @PivotColumns = '';
	DECLARE @Select VARCHAR(8000)
	DECLARE @Where VARCHAR(8000)
	SET @Select = 'SELECT ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType, LEVEL, vcAccountCode, numAccountId,Struc'


	DECLARE @sql VARCHAR(MAX) = ''

	IF @ReportColumn = 'Year'
	BEGIN
		CREATE TABLE #tempYearMonth
		(
			intYear INT,
			intMonth INT,
			MonthYear VARCHAR(50),
			StartDate DATETIME,
			EndDate DATETIME,
			ProfitLossAmount DECIMAL(20,5)
		)

		;WITH CTE AS 
		(
			SELECT
				YEAR(@dtFromDate) AS 'yr',
				MONTH(@dtFromDate) AS 'mm',
				FORMAT(@dtFromDate,'MMM') AS 'mon',
				(FORMAT(@dtFromDate,'MMM') + '_' + CAST(YEAR(@dtFromDate) AS VARCHAR)) AS MonthYear,
				CAST(DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1) AS DATETIME) dtStartDate,
				DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1)) AS DATETIME)) dtEndDate,
				@dtFromDate 'new_date'
			UNION ALL
			SELECT
				YEAR(DATEADD(d,1,new_date)) AS 'yr',
				MONTH(DATEADD(d,1,new_date)) AS 'mm',
				FORMAT(DATEADD(d,1,new_date),'MMM') AS 'mon',
				(FORMAT(DATEADD(d,1,new_date),'MMM') + '_' + CAST(YEAR(DATEADD(d,1,new_date)) AS VARCHAR)) AS MonthYear,
				CAST(DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1) AS DATETIME) dtStartDate,
				DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1)) AS DATETIME)) dtEndDate,
				DATEADD(d,1,new_date) 'new_date'
			FROM CTE
			WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		INSERT INTO 
			#tempYearMonth
		SELECT 
			yr,mm,MonthYear,dtStartDate,dtEndDate,0
		FROM 
			CTE
		GROUP BY 
			yr, mm, MonthYear, dtStartDate, dtEndDate
		ORDER BY 
			yr, mm
		OPTION (MAXRECURSION 5000)

		INSERT INTO #tempYearMonth VALUES (5000,5000,'Total',@dtFromDate,@dtToDate,0)

		UPDATE 
			#tempYearMonth
		SET
			EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)

		UPDATE 
			#tempYearMonth
		SET
			ProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode LIKE '0106%') AND datEntry_Date <=  DATEADD(Minute,-1,StartDate))
			+ @PLOPENING

		SELECT @columns = STUFF((SELECT ', ISNULL(' + MonthYear  + ',0) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 
		SELECT @PivotColumns = STUFF((SELECT ', ' + MonthYear FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 

		SELECT 
			COA.ParentId, 
			COA.vcCompundParentKey,
			COA.numAccountTypeID, 
			COA.vcAccountType, 
			COA.LEVEL, 
			COA.vcAccountCode, 
			COA.numAccountId, 
			COA.Struc,
			COA.bitTotal,
			Period.MonthYear,
			Period.ProfitLossAmount,
			(CASE 
				WHEN bitProfitLoss = 1 
				THEN Period.ProfitLossAmount
				WHEN COA.vcAccountCode LIKE '0105%' AND COA.numAccountId <> @PLCHARTID THEN (ISNULL(Debit,0) * (-1) - ISNULL(Credit,0) * (-1)) 
				WHEN COA.vcAccountCode LIKE '0102%' THEN (ISNULL(Debit,0) * (-1) - ISNULL(Credit,0) * (-1)) 
				ELSE
					ISNULL(Debit,0) - ISNULL(Credit,0)
			END) AS Amount
		INTO 
			#tempViewDataYear
		FROM 
			#tempDirectReport COA 
		OUTER APPLY
		(
			SELECT
				MonthYear,
				StartDate,
				EndDate,
				ProfitLossAmount
			FROM
				#tempYearMonth
		) AS Period
		OUTER APPLY
		(
			SELECT 
				SUM(Debit) AS Debit,
				SUM(Credit) AS Credit
			FROM
				#VIEW_JOURNALBS V 
			WHERE  
				V.numAccountID = COA.numAccountID 
				AND datEntry_Date <= Period.EndDate
		) AS t1

		SET @sql = 'SELECT
						ParentId,
						vcCompundParentKey,
						numAccountId,
						numAccountTypeID,
						vcAccountType,
						LEVEL,
						vcAccountCode,
						Struc,
						[Type],
						bitTotal,' + @columns + '
					FROM
					(
						SELECT 
							COA.ParentId, 
							COA.vcCompundParentKey,
							COA.numAccountId,
							COA.numAccountTypeID, 
							COA.vcAccountType, 
							COA.LEVEL, 
							COA.vcAccountCode, 
							COA.bitTotal,
							(''#'' + COA.Struc + ''#'') AS Struc,
							(CASE WHEN ISNULL(COA.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
							ISNULL(SUM(Amount),0)  AS Amount,
							V.MonthYear
						FROM 
							#tempDirectReport COA 
						LEFT OUTER JOIN 
							#tempViewDataYear V 
						ON  
							V.Struc like REPLACE(COA.Struc,''#Total'','''') + ''%'' 
						GROUP BY 
							COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitTotal,V.MonthYear,V.ProfitLossAmount
					) AS SourceTable
					pivot
					(
						SUM(AMOUNT)
						FOR [MonthYear] IN ( ' + @pivotcolumns + ' )
					) AS P ORDER BY Struc, [Type] desc'


		EXECUTE (@sql)

		DROP TABLE #tempYearMonth
		DROP TABLE #tempViewDataYear
	END
	Else IF @ReportColumn = 'Quarter'
	BEGIN
		CREATE TABLE #TempYearMonthQuarter
		(
			intYear INT,
			intQuarter INT,
			MonthYear VARCHAR(50),
			StartDate DATETIME,
			EndDate DATETIME,
			ProfitLossAmount DECIMAL(20,5)
		)

		;WITH CTE AS 
		(
			SELECT
				dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS 'yr',
				dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS 'qq',
				('Q' + cast(dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS VARCHAR)) AS MonthYear,
				CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
				WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
				WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
				WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
				WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
				END AS dtStartDate,
				CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
				WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				END AS dtEndDate,
				@dtFromDate 'new_date'
			UNION ALL
			SELECT
				dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS 'yr',
				dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS 'qq',
				('Q' + cast(dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR)) AS MonthYear,
				CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
				WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
				WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
				WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
				WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
				END AS dtStartDate,
				CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
				WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				END AS dtEndDate,
				DATEADD(d,1,new_date) 'new_date'
			FROM CTE
			WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		INSERT INTO 
			#TempYearMonthQuarter 		
		SELECT 
			yr,qq,MonthYear,dtStartDate,dtEndDate,0
		FROM 
			CTE
		GROUP BY 
			yr, qq, MonthYear, dtStartDate, dtEndDate
		ORDER BY 
			yr, qq
		OPTION
			(MAXRECURSION 5000)

		INSERT INTO #TempYearMonthQuarter VALUES (5000,5000,'Total',@dtFromDate,@dtToDate,0)

		UPDATE 
			#TempYearMonthQuarter
		SET
			EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)

		UPDATE 
			#TempYearMonthQuarter
		SET
			ProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode LIKE '0106%') AND datEntry_Date <=  DATEADD(Minute,-1,StartDate))
			+ @PLOPENING

		SELECT @columns = STUFF((SELECT ', ISNULL(' + MonthYear  + ',0) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''); 
		SELECT @PivotColumns = STUFF((SELECT ', ' + MonthYear FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''); 

		SELECT 
			COA.ParentId, 
			COA.vcCompundParentKey,
			COA.numAccountTypeID, 
			COA.vcAccountType, 
			COA.LEVEL, 
			COA.vcAccountCode, 
			COA.numAccountId, 
			COA.Struc,
			COA.bitTotal,
			Period.MonthYear,
			Period.ProfitLossAmount,
			(CASE 
				WHEN bitProfitLoss = 1 
				THEN Period.ProfitLossAmount
				WHEN COA.vcAccountCode LIKE '0105%' AND COA.numAccountId <> @PLCHARTID THEN (ISNULL(Debit,0) * (-1) - ISNULL(Credit,0) * (-1)) 
				WHEN COA.vcAccountCode LIKE '0102%' THEN (ISNULL(Debit,0) * (-1) - ISNULL(Credit,0) * (-1)) 
				ELSE
					ISNULL(Debit,0) - ISNULL(Credit,0)
			END) AS Amount
		INTO 
			#tempViewDataQuarter
		FROM 
			#tempDirectReport COA 
		OUTER APPLY
		(
			SELECT
				MonthYear,
				StartDate,
				EndDate,
				ProfitLossAmount
			FROM
				#TempYearMonthQuarter
		) AS Period
		OUTER APPLY
		(
			SELECT 
				SUM(Debit) AS Debit,
				SUM(Credit) AS Credit
			FROM
				#VIEW_JOURNALBS V 
			WHERE  
				V.numAccountID = COA.numAccountID 
				AND datEntry_Date <= Period.EndDate
		) AS t1

		SET @sql = 'SELECT
						ParentId,
						vcCompundParentKey,
						numAccountId,
						numAccountTypeID,
						vcAccountType,
						LEVEL,
						vcAccountCode,
						Struc,
						[Type],
						bitTotal,' + @columns + '
					FROM
					(
						SELECT 
							COA.ParentId, 
							COA.vcCompundParentKey,
							COA.numAccountId,
							COA.numAccountTypeID, 
							COA.vcAccountType, 
							COA.LEVEL, 
							COA.vcAccountCode, 
							COA.bitTotal,
							(''#'' + COA.Struc + ''#'') AS Struc,
							(CASE WHEN ISNULL(COA.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
							ISNULL(SUM(Amount),0)  AS Amount,
							V.MonthYear
						FROM 
							#tempDirectReport COA 
						LEFT OUTER JOIN 
							#tempViewDataQuarter V 
						ON  
							V.Struc like REPLACE(COA.Struc,''#Total'','''') + ''%'' 
						GROUP BY 
							COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitTotal,V.MonthYear,V.ProfitLossAmount
					) AS SourceTable
					pivot
					(
						SUM(AMOUNT)
						FOR [MonthYear] IN ( ' + @pivotcolumns + ' )
					) AS P ORDER BY Struc, [Type] desc'

		PRINT @sql
		EXECUTE (@sql)

		DROP TABLE #TempYearMonthQuarter
		DROP TABLE #tempViewDataQuarter
	END
	ELSE
	BEGIN
		DECLARE @ProifitLossAmount AS DECIMAL(20,5) = 0

		SET @ProifitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode  LIKE '0103%' AND datEntry_Date between @dtFromDate and @dtToDate)
								+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode  LIKE '0104%' AND datEntry_Date between @dtFromDate and @dtToDate)
								+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode  LIKE '0106%' AND datEntry_Date between @dtFromDate and @dtToDate)
								+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode LIKE '0106%') AND datEntry_Date <=  DATEADD(MILLISECOND,-3,@dtFromDate))
								+ @PLOPENING		

		SELECT TOP 1 @PLAccountStruc= '#' + Struc + '#' FROM #tempDirectReport WHERe bitProfitLoss = 1

		SELECT 
			COA.ParentId,
			COA.vcCompundParentKey,
			COA.numAccountId,
			COA.numAccountTypeID,
			COA.vcAccountType,
			COA.LEVEL,
			COA.vcAccountCode,
			('#' + COA.Struc + '#') AS Struc,
			(CASE WHEN ISNULL(COA.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
			COA.bitTotal,
			(Case 
					When COA.bitProfitLoss = 1
					THEN @ProifitLossAmount
					ELSE ((CASE WHEN CHARINDEX(('#' + COA.Struc + '#'),@PLAccountStruc) > 0 THEN @ProifitLossAmount ELSE 0 END) + SUM(ISNULL(Amount,0)))
			END) AS Total
		FROM 
			#tempDirectReport COA 
		LEFT OUTER JOIN 
			#tempViewData V 
		ON  
			V.Struc like REPLACE(COA.Struc,'#Total','') + '%'
		GROUP BY 
			COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitTotal,COA.bitProfitLoss
		ORDER BY 
			Struc, [Type] desc
	END

	DROP TABLE #tempViewData 
	DROP TABLE #tempDirectReport
	DROP TABLE #VIEW_JOURNALBS
END

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetChartAcntDetailsForProfitLoss_New')
DROP PROCEDURE USP_GetChartAcntDetailsForProfitLoss_New
GO
CREATE PROCEDURE [dbo].[USP_GetChartAcntDetailsForProfitLoss_New]
@numDomainId as numeric(9),                                          
@dtFromDate as datetime,                                        
@dtToDate as DATETIME,
@ClientTimeZoneOffset INT, --Added by Chintan to enable calculation of date according to client machine                                                   
@numAccountClass AS NUMERIC(9)=0,
@DateFilter AS VARCHAR(20),
@ReportColumn AS VARCHAR(20),
@numDivisionID NUMERIC(18,0),
@bitAddTotalRow BIT
AS                                                        
BEGIN 
	DECLARE @FinancialYearStartDate AS DATETIME
	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainId)

	DECLARE @TEMPQuarter AS TABLE
	(
		ID INT,
		StartDate DATETIME,
		EndDate DATETIME
	)

	INSERT INTO @TEMPQuarter VALUES (1,@FinancialYearStartDate,DATEADD(ms,-3,DATEADD(QUARTER,1,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (2,DATEADD(QUARTER,1,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,2,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (3,DATEADD(QUARTER,2,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,3,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (4,DATEADD(QUARTER,3,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,4,@FinancialYearStartDate)))

	DECLARE @PLCHARTID NUMERIC(18,0)

	SELECT 
		@PLCHARTID=COA.numAccountId 
	FROM 
		Chart_of_Accounts COA 
	WHERE 
		numDomainID=@numDomainId 
		AND bitProfitLoss=1;

	CREATE TABLE #View_Journal
	(
		numAccountId NUMERIC(18,0),
		vcAccountCode VARCHAR(50),
		COAvcAccountCode VARCHAR(100),
		datEntry_Date SMALLDATETIME,
		Debit DECIMAL(20,5),
		Credit DECIMAL(20,5)
	)

	INSERT INTO 
		#View_Journal 
	SELECT 
		numAccountId
		,AccountTypeCode
		,AccountCode
		,datEntry_Date
		,Debit
		,Credit 
	FROM 
		view_journal_master 
	WHERE 
		numDomainId = @numDomainID 
		AND (numClassIDDetail=@numAccountClass OR @numAccountClass=0)
		AND (numDivisionId=@numDivisionID OR @numDivisionID=0)

	IF @DateFilter = 'CurYear'
	BEGIN
		SELECT @dtFromDate = @FinancialYearStartDate,
		   @dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'PreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
		   @dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,DATEADD(YEAR,-1,@FinancialYearStartDate)));
	END
	ELSE IF @DateFilter = 'CurPreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
		   @dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'CuQur'
	BEGIN
		SELECT @dtFromDate = StartDate, @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'CurPreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'PreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=DATEADD(QUARTER,-1,EndDate) FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'LastMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)));  
	END
	ELSE IF @DateFilter = 'ThisMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0)));  
	END
	ELSE IF @DateFilter = 'CurPreMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0))); 
	END

	;WITH DirectReport (ParentId, vcCompundParentKey, numAccountTypeID, numAccountID, vcAccountType,vcAccountCode, LEVEL,Struc, bitTotal, bitIsSubAccount)
	AS
	(
		SELECT 
			CAST('' AS VARCHAR) AS ParentId,
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR), 
			[ATD].[numAccountTypeID], 
			0,
			CAST([ATD].[vcAccountType] AS VARCHAR(300)),
			[ATD].[vcAccountCode],
			1 AS LEVEL, 
			CAST(CONCAT(FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),[ATD].[vcAccountCode]) AS VARCHAR(300))  AS Struc,
			0,
			0
		FROM 
			[dbo].[AccountTypeDetail] AS ATD
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND [ATD].[vcAccountCode] IN ('0103','0104','0106')
			AND ATD.bitActive = 1
		UNION ALL
		SELECT 
			CAST('' AS VARCHAR) AS ParentId,
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR), 
			[ATD].[numAccountTypeID], 
			0,
			CAST(CONCAT('Total ',[ATD].[vcAccountType]) AS VARCHAR(300)),
			[ATD].[vcAccountCode],
			1 AS LEVEL, 
			CAST(CONCAT(FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),[ATD].[vcAccountCode],'#Total') AS VARCHAR(300))  AS Struc,
			1,
			0
		FROM 
			[dbo].[AccountTypeDetail] AS ATD
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND [ATD].[vcAccountCode] IN ('0103','0104','0106')
			AND ATD.bitActive = 1
			AND ISNULL(@bitAddTotalRow,0) = 1
		UNION ALL
		SELECT 
			D.vcCompundParentKey AS ParentId, 
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
			[ATD].[numAccountTypeID],
			0,
			CAST([ATD].[vcAccountType] AS VARCHAR(300)),
			[ATD].[vcAccountCode],
			LEVEL + 1, 
			CAST(d.Struc + '#' + CONCAT(FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),[ATD].[vcAccountCode]) AS VARCHAR(300))  AS Struc,
			0,
			0
		FROM 
			[dbo].[AccountTypeDetail] AS ATD 
		JOIN 
			[DirectReport] D 
		ON 
			D.[numAccountTypeID] = [ATD].[numParentID]
			AND D.bitTotal = 0
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND ATD.bitActive = 1
		UNION ALL
		SELECT 
			D.vcCompundParentKey AS ParentId, 
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
			[ATD].[numAccountTypeID],
			0,
			CAST(CONCAT('Total ',[ATD].[vcAccountType]) AS VARCHAR(300)),
			[ATD].[vcAccountCode],
			LEVEL + 1, 
			CAST(CONCAT(d.Struc,'#',FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),[ATD].[vcAccountCode],'#Total') AS VARCHAR(300))  AS Struc,
			1,
			0
		FROM 
			[dbo].[AccountTypeDetail] AS ATD 
		JOIN 
			[DirectReport] D 
		ON 
			D.[numAccountTypeID] = [ATD].[numParentID]
			AND D.bitTotal = 0
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND ATD.bitActive = 1
			AND ISNULL(@bitAddTotalRow,0) = 1
	),
	DirectReport1 (ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,numAccountId,Struc, bitTotal,bitIsSubAccount)
	AS
	(
		SELECT 
			ParentId,
			vcCompundParentKey,
			numAccountTypeID, 
			vcAccountType,
			vcAccountCode, 
			LEVEL,
			CAST(numAccountID AS NUMERIC(18)),
			Struc,
			bitTotal,
			CAST(0 AS BIT)
		FROM 
			DirectReport 
		UNION ALL
		SELECT 
			CAST(CONCAT(COA.numParntAcntTypeId,'#',0) AS VARCHAR) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
			CAST(NULL AS NUMERIC(18)),
			CAST([vcAccountName] AS VARCHAR(300)),
			COA.[vcAccountCode],
			LEVEL + 1,
			[COA].[numAccountId], 
			CAST(CONCAT(d.Struc ,'#',[COA].[numAccountId]) AS VARCHAR(300)) AS Struc,
			0,
			ISNULL(COA.bitIsSubAccount,0)
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport1] D 
		ON 
			D.[numAccountTypeID] = COA.[numParntAcntTypeId]
			AND D.bitTotal = 0
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitIsSubAccount,0) = 0
		UNION ALL
		SELECT 
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numParentAccId) AS VARCHAR) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
			CAST(NULL AS NUMERIC(18)),
			CAST([vcAccountName] AS VARCHAR(300)),
			COA.[vcAccountCode],
			LEVEL + 1,
			[COA].[numAccountId], 
			CAST(d.Struc + '#' + CAST(COA.[numAccountId] AS VARCHAR) AS VARCHAR(300)) AS Struc
			,0
			,ISNULL(COA.bitIsSubAccount,0)
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport1] D 
		ON 
			D.numAccountId = COA.numParentAccId
			AND D.bitTotal = 0
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitIsSubAccount,0) = 1
	)

  
	SELECT 
		ParentId, 
		vcCompundParentKey,
		numAccountTypeID, 
		vcAccountType, 
		LEVEL, 
		vcAccountCode,
		numAccountId,
		Struc,
		bitTotal,
		bitIsSubAccount
	INTO 
		#tempDirectReport
	FROM 
		DirectReport1 

	INSERT INTO #tempDirectReport
	SELECT '','-1',-1,'Ordinary Income/Expense',0,NULL,NULL,'-1',0,0
	UNION ALL
	SELECT '','-2',-2,'Other Income and Expenses',0,NULL,NULL,'-2',0,0

	IF ISNULL(@bitAddTotalRow,0) = 1
	BEGIN
		INSERT INTO #tempDirectReport
		SELECT '','-1',-1,'Total Ordinary Income/Expense',0,NULL,NULL,'-1#Total',1,0
		UNION ALL
		SELECT '','-2',-2,'Total Other Income and Expenses',0,NULL,NULL,'-2#Total',1,0
	END	

	UPDATE 
		#tempDirectReport 
	SET 
		Struc='-1#' + Struc 
	WHERE 
		[vcAccountCode] NOT LIKE '010302%' 
		AND [vcAccountCode] NOT LIKE '010402%' 

	UPDATE 
		#tempDirectReport 
	SET 
		Struc='-2#' + Struc
		,[LEVEL] = [LEVEL] - 1
	WHERE 
		[vcAccountCode] LIKE '010302%' 
		OR [vcAccountCode] LIKE '010402%'

	UPDATE 
		#tempDirectReport 
	SET 
		[ParentId]=-1
	WHERE 
		[vcAccountCode] NOT IN ('010302','010402')
		AND [LEVEL] = 1

	UPDATE 
		#tempDirectReport 
	SET 
		[ParentId]=-2 
	WHERE 
		[vcAccountCode] IN ('010302','010402')

	SELECT 
		COA.ParentId, 
		COA.numAccountTypeID, 
		COA.vcAccountType, 
		COA.LEVEL, 
		COA.vcAccountCode, 
		COA.numAccountId, 
		COA.Struc,
		CASE 
			WHEN COA.vcAccountCode LIKE '010301%' OR COA.vcAccountCode LIKE '010302%' 
			THEN (ISNULL(Credit,0) - ISNULL(Debit,0))
			ELSE (ISNULL(Debit,0) - ISNULL(Credit,0)) 
		END AS Amount,
		V.datEntry_Date,
		COA.bitIsSubAccount
	INTO 
		#tempViewData
	FROM 
		#tempDirectReport COA 
	JOIN 
		#View_Journal V 
	ON  
		V.numAccountId = COA.numAccountId
		AND datEntry_Date BETWEEN  @dtFromDate AND @dtToDate 
	WHERE 
		COA.[numAccountId] IS NOT NULL

	DECLARE @columns VARCHAR(8000);--SET @columns = '';
	DECLARE @SUMColumns VARCHAR(8000);--SET @SUMColumns = '';
	DECLARE @PivotColumns VARCHAR(8000);--SET @PivotColumns = '';
	DECLARE @ProfitLossCurrentColumns VARCHAR(8000) = '';
	DECLARE @ProfitLossSumCurrentColumns VARCHAR(8000) = ',0';
	DECLARE @ProfitLossOpeningColumns VARCHAR(8000) = '';
	DECLARE @ProfitLossSumOpeningColumns VARCHAR(8000) = '';

	DECLARE @Select VARCHAR(8000)
	DECLARE @Where VARCHAR(8000)
	SET @Select = 'SELECT ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType, LEVEL, vcAccountCode, numAccountId,(''#'' + Struc + ''#'') AS Struc'

	IF @ReportColumn = 'Year' OR @ReportColumn = 'Quarter'
	BEGIN
	; WITH CTE AS (
		SELECT
			(CASE WHEN @ReportColumn = 'Quarter' THEN dbo.GetFiscalyear(@dtFromDate,@numDomainId) ELSE YEAR(@dtFromDate) END) AS 'yr',
			MONTH(@dtFromDate) AS 'mm',
			FORMAT(@dtFromDate,'MMM') AS 'mon',
			DATEPART(d,@dtFromDate) AS 'dd',
			dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS 'qq',
			@dtFromDate 'new_date'
		UNION ALL
		SELECT
			(CASE WHEN @ReportColumn = 'Quarter' THEN dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) ELSE YEAR(DATEADD(d,1,new_date)) END) AS 'yr',
			MONTH(DATEADD(d,1,new_date)) AS 'mm',
			FORMAT(DATEADD(d,1,new_date),'MMM') AS 'mon',
			DATEPART(d,@dtFromDate) AS 'dd',
			dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS 'qq',
			DATEADD(d,1,new_date) 'new_date'
		FROM CTE
		WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		SELECT yr AS 'Year1',qq AS 'Quarter1', mm as 'Month1', mon AS 'MonthName1', count(dd) AS 'Days1' into #tempYearMonth
		FROM CTE
		GROUP BY mon, yr, mm, qq
		ORDER BY yr, mm, qq
		OPTION (MAXRECURSION 5000)

		IF @ReportColumn = 'Year'
		BEGIN
			-- Profit / (Loss) Current dynamic columns
			-- We are just inserting rows calculation is made in code
			SELECT
				@ProfitLossCurrentColumns = COALESCE(@ProfitLossCurrentColumns + ',0 AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				',0 AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
			FROM (SELECT DISTINCT Year1,MonthName1,Month1  FROM #tempYearMonth)T
			ORDER BY Year1,Month1

			-- Profit / (Loss) Opening dynamic columns
			SELECT
				@ProfitLossOpeningColumns = COALESCE(@ProfitLossOpeningColumns + ',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj where (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),DATEADD(DAY, 0, DATEADD(MONTH, Month1 - 1, DATEADD(YEAR, Year1-1900, 0))),23) + '''),0) AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj WHERE (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),DATEADD(DAY, 0, DATEADD(MONTH, Month1 - 1, DATEADD(YEAR, Year1-1900, 0))),23) + '''),0) AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
			FROM (SELECT DISTINCT Year1,MonthName1,Month1  FROM #tempYearMonth)T
			ORDER BY Year1,Month1

			SELECT @ProfitLossSumOpeningColumns = ',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj WHERE (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),@dtFromDate,23) + '''),0) AS [Total]'
			
			-- Chart of accounts dynamic columns
			SELECT
				@columns = COALESCE(@columns + ',ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				',ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']'),
				@SUMColumns = COALESCE(@SUMColumns + '+ ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)',
				',ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)'),
				@PivotColumns = COALESCE(@PivotColumns + ',[' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				'[' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
			FROM #tempYearMonth ORDER BY Year1,MONTH1

			SET @SUMColumns = @SUMColumns + ' AS Total, (CASE WHEN ISNULL(numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type], bitTotal,bitIsSubAccount'
			SET @Where = '	FROM (Select COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitTotal,COA.bitIsSubAccount,
					SUM(Case 
						When (COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2)
						THEN (Case WHEN V.vcAccountCode LIKE ''0104%'' OR V.vcAccountCode LIKE ''0106%'' THEN ISNULL(Amount,0) * -1
						ELSE ISNULL(Amount,0) END)
 					ELSE ISNULL(Amount,0) END) AS Amount,
					LEFT(DATENAME(mm,datEntry_Date),3) + '' '' + CAST(YEAR(datEntry_Date) AS VARCHAR(4)) AS MonthYear 
					FROM #tempDirectReport COA LEFT OUTER JOIN #tempViewData V ON  
					V.Struc like REPLACE(COA.Struc,''#Total'','''') + ''%'' 
					GROUP BY YEAR(datEntry_Date),MONTH(datEntry_Date),DATENAME(mm,datEntry_Date),COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitTotal,COA.bitIsSubAccount) AS t
					PIVOT
					(
					  SUM(Amount)
					  FOR [MonthYear] IN( ' + @PivotColumns +  ')
					) AS p 
					UNION SELECT '''', ''-3'', -3, ''Profit / (Loss) Current'', 0, NULL, NULL,''-3''' + @ProfitLossCurrentColumns + @ProfitLossSumCurrentColumns + ',2,1,0
					UNION SELECT '''', ''-4'', -4, ''Profit / (Loss) Opening'', 0, NULL, NULL,''-4''' + @ProfitLossOpeningColumns + @ProfitLossSumOpeningColumns + ',2,1,0 ORDER BY Struc'

		END
		Else IF @ReportColumn = 'Quarter'
		BEGIN

			-- Profit / (Loss) Current dynamic columns
			-- We are just inserting rows calculation is made in code
			SELECT
				@ProfitLossCurrentColumns = COALESCE(@ProfitLossCurrentColumns + ',0 AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				',0 AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
			FROM (SELECT DISTINCT Year1,Quarter1  FROM #tempYearMonth)T
			ORDER BY Year1,Quarter1

			-- Profit / (Loss) Opening dynamic columns
			SELECT
				@ProfitLossOpeningColumns = COALESCE(@ProfitLossOpeningColumns + ',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj where (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),DATEADD(QUARTER,Quarter1 - 1,dbo.GetFiscalStartDate(Year1,@numDomainID)),23) + '''),0) AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj where (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),DATEADD(QUARTER,Quarter1 - 1,dbo.GetFiscalStartDate(Year1,@numDomainID)),23) + '''),0) AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
			FROM (SELECT DISTINCT Year1,Quarter1  FROM #tempYearMonth)T
			ORDER BY Year1,Quarter1

			SELECT @ProfitLossSumOpeningColumns = ',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj where (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),@dtFromDate,23) + '''),0) AS [Total]'

			
			-- Chart of accounts dynamic columns
			SELECT
				@columns = COALESCE(@columns + ',ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				',ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']'),
				@SUMColumns = COALESCE(@SUMColumns + '+ ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)',
				',ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)'),
				@PivotColumns = COALESCE(@PivotColumns + ',[Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
				'[Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
			FROM (SELECT DISTINCT Year1,Quarter1  FROM #tempYearMonth)T
			ORDER BY Year1,Quarter1

			SET @SUMColumns = @SUMColumns + ' AS Total, (CASE WHEN ISNULL(numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type], bitTotal,bitIsSubAccount'
			SET @Where = '	FROM (Select COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitTotal,COA.bitIsSubAccount,
					SUM(Case 
							When(COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2) 
							THEN (Case WHEN V.vcAccountCode LIKE ''0104%'' OR V.vcAccountCode LIKE ''0106%'' THEN ISNULL(Amount,0) * -1
							ELSE ISNULL(Amount,0) END)
						ELSE ISNULL(Amount,0) END) AS Amount,
					''Q'' + cast(dbo.GetFiscalQuarter(datEntry_Date,' + CAST(@numDomainID AS VARCHAR(20)) + ') as varchar) + '' '' + CAST(dbo.GetFiscalyear(datEntry_Date,' + CAST(@numDomainID AS VARCHAR(20)) + ') AS VARCHAR(4)) AS MonthYear 
					FROM #tempDirectReport COA LEFT OUTER JOIN #tempViewData V ON  
					V.Struc like REPLACE(COA.Struc,''#Total'','''') + ''%'' 
					GROUP BY dbo.GetFiscalyear(datEntry_Date,' + CAST(@numDomainID AS VARCHAR(20)) + '),dbo.GetFiscalQuarter(datEntry_Date,' + CAST(@numDomainID AS VARCHAR(20)) + '),COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitTotal,COA.bitIsSubAccount) AS t
					PIVOT
					(
					  SUM(Amount)
					  FOR [MonthYear] IN( ' + @PivotColumns +  ')
					) AS p 
					UNION SELECT '''', ''-3'', -3, ''Profit / (Loss) Current'', 0, NULL, NULL,''-3''' + @ProfitLossCurrentColumns + @ProfitLossSumCurrentColumns + ',2,1,0
					UNION SELECT '''', ''-4'', -4, ''Profit / (Loss) Opening'', 0, NULL, NULL,''-4''' + @ProfitLossOpeningColumns + @ProfitLossSumOpeningColumns + ',2,1,0 ORDER BY Struc'

			
		END

		DROP TABLE #tempYearMonth
	END
	ELSE
	BEGIN		
		SELECT @ProfitLossSumOpeningColumns = ',ISNULL((select isnull(sum(credit),0)-isnull(sum(debit),0) from #View_Journal vj where (( vcaccountcode  like ''0103%'' or  vcaccountcode  like ''0104%'' or  vcaccountcode  like ''0106%'') OR numaccountid = ' + CAST(@PLCHARTID AS VARCHAR) + ') and datentry_date <  ''' + CONVERT(VARCHAR(50),@dtFromDate,23) + '''),0) AS [Total]'

		SET @columns = ',ISNULL(ISNULL(Amount,0),0) as Total, (CASE WHEN ISNULL(numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type], bitTotal,bitIsSubAccount';
		SET @SUMColumns = '';
		SET @PivotColumns = '';
		SET @Where = ' FROM (Select COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitTotal,COA.bitIsSubAccount,
				SUM(Case 
						When (COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2)
						THEN (Case WHEN V.vcAccountCode LIKE ''0104%'' OR V.vcAccountCode LIKE ''0106%'' THEN ISNULL(Amount,0) * -1
						ELSE ISNULL(Amount,0) END)
					ELSE ISNULL(Amount,0) END) AS Amount
				FROM #tempDirectReport COA LEFT OUTER JOIN #tempViewData V ON  
				V.Struc like REPLACE(COA.Struc,''#Total'','''') + ''%''
				GROUP BY COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitTotal,COA.bitIsSubAccount) AS t
				UNION SELECT '''', ''-3'', -3, ''Profit / (Loss) Current'', 0, NULL, NULL,''-3'',0,2,1,0
				UNION SELECT '''', ''-4'', -4, ''Profit / (Loss) Opening'', 0, NULL, NULL,''-4''' + @ProfitLossSumOpeningColumns + ',2,1,0 ORDER BY Struc'
	END

	PRINT @Select
	PRINT @columns
	PRINT @SUMColumns
	PRINT @Where


	EXECUTE (@Select + @columns + @SUMColumns  + ' ' + @Where)

	DROP TABLE #View_Journal
	DROP TABLE #tempViewData 
	DROP TABLE #tempDirectReport
END
/****** Object:  StoredProcedure [dbo].[usp_GetDuplicateCompany]    Script Date: 07/26/2008 16:17:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdueamount')
DROP PROCEDURE usp_getdueamount
GO
CREATE PROCEDURE [dbo].[USP_GetDueAmount]                      
@numDivisionID numeric(9)=0                      
as                      
declare @Amt as DECIMAL(20,5);set @Amt=0                        
declare @Paid as DECIMAL(20,5);set @Paid=0      
                 
declare @AmountDueSO as DECIMAL(20,5);set @AmountDueSO=0                    
declare @AmountPastDueSO as DECIMAL(20,5);set @AmountPastDueSO=0 

declare @AmountDuePO as DECIMAL(20,5);set @AmountDuePO=0                    
declare @AmountPastDuePO as DECIMAL(20,5);set @AmountPastDuePO=0 
                   
declare @CompanyCredit as DECIMAL(20,5);set @CompanyCredit=0   
          
declare @PCreditMemo as DECIMAL(20,5);set @PCreditMemo=0            
declare @SCreditMemo as DECIMAL(20,5);set @SCreditMemo=0            

DECLARE @UTCtime AS datetime	       
SET	@UTCtime = dbo.[GetUTCDateWithoutTime]()

--Sales Order Total Amount Due                
select @Amt=isnull(sum(OppBD.[monDealAmount]),0) from OpportunityMaster  Opp
join  OpportunityBizDocs OppBD
on    OppBD.numOppID=Opp.numOppID               
where bitAuthoritativeBizDocs=1 and numDivisionID=@numDivisionID and tintOppStatus=1   and tintOpptype=1              
               
select @Paid=isnull(sum(monAmountPaid),0) from OpportunityMaster  Opp
join  OpportunityBizDocs OppBD
on    OppBD.numOppID=Opp.numOppID               
where bitAuthoritativeBizDocs=1 and numDivisionID=@numDivisionID and tintOppStatus=1   and tintOpptype=1               
                
set @AmountDueSO=@Amt-@Paid                
         
--Sales Order Total Amount Past Due                
select @Amt=isnull(sum(OppBD.[monDealAmount]),0) from OpportunityMaster   Opp
join  OpportunityBizDocs OppBD
on    OppBD.numOppID=Opp.numOppID               
where  bitAuthoritativeBizDocs=1 and numDivisionID=@numDivisionID and tintOppStatus=1   and tintOpptype=1              
AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
					 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)),0))  
					 ELSE 0 
				END, OppBD.[dtFromDate]) < @UTCtime
--AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
--					 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0))  
--					 ELSE 0 
--				END, OppBD.[dtFromDate]) < @UTCtime				

select @Paid=isnull(SUM(OppBD.monAmountPaid),0) from OpportunityMaster  Opp
join  OpportunityBizDocs OppBD on    OppBD.numOppID=Opp.numOppID               
where  OppBD.bitAuthoritativeBizDocs=1 and Opp.numDivisionID=@numDivisionID and tintOppStatus=1  and tintOpptype=1               
AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
					 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)),0)) 
					 ELSE 0 
				END, OppBD.[dtFromDate]) <@UTCtime
--AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
--					 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0)) 
--					 ELSE 0 
--				END, OppBD.[dtFromDate]) <@UTCtime				

set @AmountPastDueSO=@Amt-@Paid                    
                    
   
--Purchase Order Total Amount Due                
select @Amt=isnull(sum(OppBD.[monDealAmount]),0) from OpportunityMaster  Opp
join  OpportunityBizDocs OppBD
on    OppBD.numOppID=Opp.numOppID               
where bitAuthoritativeBizDocs=1 and numDivisionID=@numDivisionID and tintOppStatus=1   and tintOpptype=2              
               
select @Paid=isnull(sum(monAmountPaid),0) from OpportunityMaster  Opp
join  OpportunityBizDocs OppBD
on    OppBD.numOppID=Opp.numOppID               
where bitAuthoritativeBizDocs=1 and numDivisionID=@numDivisionID and tintOppStatus=1   and tintOpptype=2              
                
set @AmountDuePO=@Amt-@Paid                
         
--Purchase Order Total Amount Past Due                
select @Amt=isnull(sum(OppBD.[monDealAmount]),0) from OpportunityMaster   Opp
join  OpportunityBizDocs OppBD
on    OppBD.numOppID=Opp.numOppID               
where  bitAuthoritativeBizDocs=1 and numDivisionID=@numDivisionID and tintOppStatus=1   and tintOpptype=2              
AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
					 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)),0))  
					 ELSE 0 
				END, OppBD.[dtFromDate]) <@UTCtime
--AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
--					 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0))  
--					 ELSE 0 
--				END, OppBD.[dtFromDate]) <@UTCtime
				

select @Paid=isnull(SUM(OppBD.monAmountPaid),0) from OpportunityMaster  Opp
join  OpportunityBizDocs OppBD on    OppBD.numOppID=Opp.numOppID               
where  OppBD.bitAuthoritativeBizDocs=1 and Opp.numDivisionID=@numDivisionID and tintOppStatus=1  and tintOpptype=2               
AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
					 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)),0)) 
					 ELSE 0 
				END, OppBD.[dtFromDate]) <@UTCtime
--AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
--					 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0)) 
--					 ELSE 0 
--				END, OppBD.[dtFromDate]) <@UTCtime

set @AmountPastDuePO=@Amt-@Paid  


--Company Cresit Limit                    
select @CompanyCredit=convert(DECIMAL(20,5),case when isnumeric(dbo.fn_GetListItemName(numCompanyCredit))=1 then REPLACE(dbo.fn_GetListItemName(numCompanyCredit),',','') else 0 end)
	 from companyinfo C                    
join divisionmaster d                    
on d.numCompanyID=C.numCompanyID                    
where d.numdivisionid=@numDivisionID                    
                    
 
--Purchase Credit Memo
select @PCreditMemo=ISNULL(BPH.monPaymentAmount,0) - ISNULL (BPH.monAppliedAmount,0)
	 from BillPaymentHeader BPH          
where BPH.numdivisionid=@numDivisionID AND ISNULL(numReturnHeaderID,0)>0                  

--Sales Credit Memo
select @SCreditMemo=ISNULL(DM.monDepositAmount,0) - ISNULL (DM.monAppliedAmount,0)
	 from dbo.DepositMaster DM          
where DM.numdivisionid=@numDivisionID AND tintDepositePage=3 AND ISNULL(numReturnHeaderID,0)>0  

DECLARE @vcShippersAccountNo AS VARCHAR(100)
DECLARE @numDefaultShippingServiceID AS NUMERIC(18,0)
DECLARE @intShippingCompany AS NUMERIC(18,0)
DECLARE @numPartnerSource NUMERIC(18,0)
DECLARE @bitAutoCheckCustomerPart BIT
SELECT @vcShippersAccountNo = ISNULL(vcShippersAccountNo,''), @numDefaultShippingServiceID=ISNULL(numDefaultShippingServiceID,0), @intShippingCompany=ISNULL(intShippingCompany,0), 
@numPartnerSource=ISNULL(numPartenerSource,0),@bitAutoCheckCustomerPart=ISNULL(bitAutoCheckCustomerPart,0) FROM dbo.DivisionMaster WHERE numDivisionID = @numDivisionID

select isnull(@AmountDueSO,0) as AmountDueSO,isnull(@AmountPastDueSO,0) as AmountPastDueSO, 
isnull(@AmountDuePO,0) as AmountDuePO,isnull(@AmountPastDuePO,0) as AmountPastDuePO,                      
ISNULL(@CompanyCredit,0) - (isnull(@AmountDueSO,0) + isnull(@AmountDuePO,0)) as RemainingCredit,
isnull(@PCreditMemo,0) as PCreditMemo,
isnull(@SCreditMemo,0) as SCreditMemo,
ISNULL(@CompanyCredit,0) AS CreditLimit,
@vcShippersAccountNo AS vcShippersAccountNo,
(CASE WHEN (SELECT COUNT(*) FROM DivisionMasterShippingAccount WHERE numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END) AS bitUseShippersAccountNo,
@numDefaultShippingServiceID AS numDefaultShippingServiceID,
@intShippingCompany AS intShippingCompany,
@numPartnerSource AS numPartnerSource,
@bitAutoCheckCustomerPart AS bitAutoCheckCustomerPart

GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetECommerceDetails')
DROP PROCEDURE USP_GetECommerceDetails
GO
CREATE PROCEDURE [dbo].[USP_GetECommerceDetails]      
@numDomainID as numeric(9)=0,
@numSiteID AS INTEGER = 0  
as
BEGIN
		IF ISNULL(@numSiteID,0) = 0
	BEGIN
		SELECT @numSiteID=numDefaultSiteID FROM Domain WHERE numDomainId=@numDomainID
	END                  
select                               
isnull(vcPaymentGateWay,'0') as vcPaymentGateWay,                
isnull(vcPGWManagerUId,'')as vcPGWManagerUId ,                
isnull(vcPGWManagerPassword,'') as vcPGWManagerPassword,              
isnull(bitShowInStock,0) as bitShowInStock,              
isnull(bitShowQOnHand,0) as bitShowQOnHand, 
isnull([numDefaultWareHouseID],0) numDefaultWareHouseID,
isnull(bitCheckCreditStatus,0) as bitCheckCreditStatus,
isnull([numRelationshipId],0) numRelationshipId,
isnull([numProfileId],0) numProfileId,
ISNULL([bitHidePriceBeforeLogin],0) AS bitHidePriceBeforeLogin,
ISNULL([numBizDocForCreditCard],0) AS numBizDocForCreditCard,
ISNULL([numBizDocForCreditTerm],0) AS numBizDocForCreditTerm,
ISNULL(bitAuthOnlyCreditCard,0) AS bitAuthOnlyCreditCard,
ISNULL(numAuthrizedOrderStatus,0) AS numAuthrizedOrderStatus,
ISNULL(bitSendEmail,1) AS bitSendEmail,
ISNULL(vcGoogleMerchantID , '') AS vcGoogleMerchantID,
ISNULL(vcGoogleMerchantKey,'') AS vcGoogleMerchantKey,
ISNULL(IsSandbox,0) AS IsSandbox,
ISNULL(numSiteID,0) AS numSiteID,
ISNULL(vcPaypalUserName,'') AS vcPaypalUserName,
ISNULL(vcPaypalPassword,'') AS vcPaypalPassword,
ISNULL(vcPaypalSignature,'') AS vcPaypalSignature,
ISNULL(IsPaypalSandbox,0) AS IsPaypalSandbox,
ISNULL(bitSkipStep2,0) AS [bitSkipStep2],
ISNULL(bitDisplayCategory,0) AS [bitDisplayCategory],
ISNULL(bitShowPriceUsingPriceLevel,0) AS [bitShowPriceUsingPriceLevel],
ISNULL(bitEnableSecSorting,0) AS [bitEnableSecSorting],
ISNULL(bitSortPriceMode,0) AS [bitSortPriceMode],
ISNULL(numPageSize,0) AS [numPageSize],
ISNULL(numPageVariant,0) AS [numPageVariant],
ISNULL(bitAutoSelectWarehouse,0) AS bitAutoSelectWarehouse,
ISNULL([bitPreSellUp],0) AS [bitPreSellUp],
ISNULL([bitPostSellUp],0) AS [bitPostSellUp],
ISNULL([dcPostSellDiscount],0) AS [dcPostSellDiscount],
ISNULL(numDefaultClass,0) numDefaultClass,
vcPreSellUp
,vcPostSellUp
,ISNULL(vcRedirectThankYouUrl,'') AS vcRedirectThankYouUrl
,ISNULL(tintPreLoginProceLevel,0) tintPreLoginProceLevel
,ISNULL(bitHideAddtoCart,0) bitHideAddtoCart
,ISNULL(bitShowPromoDetailsLink,0) bitShowPromoDetailsLink
,ISNULL(tintWarehouseAvailability,1) tintWarehouseAvailability
,ISNULL(bitDisplayQtyAvailable,0) bitDisplayQtyAvailable
FROM eCommerceDTL 
WHERE numDomainID=@numDomainID
AND ([numSiteId] = @numSiteID OR ISNULL(@numSiteID,0) = 0)
END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetIncomeExpenseStatementNew_Kamal' ) 
    DROP PROCEDURE USP_GetIncomeExpenseStatementNew_Kamal
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE USP_GetIncomeExpenseStatementNew_Kamal
@numDomainId AS NUMERIC(9),                                          
@dtFromDate AS DATETIME = NULL,                                        
@dtToDate AS DATETIME = NULL,
@ClientTimeZoneOffset INT, --Added by Chintan to enable calculation of date according to client machine   
@numAccountClass AS NUMERIC(9)=0,
@DateFilter AS VARCHAR(20),
@ReportColumn AS VARCHAR(20),
@numDivisionID NUMERIC(18,0),
@bitAddTotalRow BIT
AS                                                          
BEGIN 

	DECLARE @FinancialYearStartDate AS DATETIME
	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainId)

	DECLARE @TEMPQuarter AS TABLE
	(
		ID INT,
		StartDate DATETIME,
		EndDate DATETIME
	)

	INSERT INTO @TEMPQuarter VALUES (1,@FinancialYearStartDate,DATEADD(ms,-3,DATEADD(QUARTER,1,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (2,DATEADD(QUARTER,1,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,2,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (3,DATEADD(QUARTER,2,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,3,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (4,DATEADD(QUARTER,3,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,4,@FinancialYearStartDate)))

	IF @DateFilter = 'CurYear'
	BEGIN
		SELECT @dtFromDate = @FinancialYearStartDate,
		   @dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'PreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
		   @dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,DATEADD(YEAR,-1,@FinancialYearStartDate)));
	END
	ELSE IF @DateFilter = 'CurPreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
		   @dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'CuQur'
	BEGIN
		SELECT @dtFromDate = StartDate, @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'CurPreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'PreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=DATEADD(QUARTER,-1,EndDate) FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'LastMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)));  
	END
	ELSE IF @DateFilter = 'ThisMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0)));  
	END
	ELSE IF @DateFilter = 'CurPreMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0))); 
	END
 

;WITH DirectReport (ParentId, vcCompundParentKey, numAccountTypeID, numAccountID, vcAccountType,vcAccountCode, LEVEL,Struc,bitTotal,bitIsSubAccount)
AS
(
  -- anchor
	SELECT 
		CAST('-1' AS VARCHAR) AS ParentId,
		CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
		[ATD].[numAccountTypeID], 
		0,
		CAST([ATD].[vcAccountType] AS VARCHAR(300)),
		[ATD].[vcAccountCode],
		1 AS LEVEL, 
		CAST(CONCAT(FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),(CASE vcAccountCode WHEN '0103' THEN '1' WHEN '0106' THEN '2' WHEN '0104' THEN '3' END),[ATD].[vcAccountCode]) AS VARCHAR(300))  AS Struc
		,0
		,0
	FROM 
		[dbo].[AccountTypeDetail] AS ATD
	WHERE 
		[ATD].[numDomainID]=@numDomainId 
		AND [ATD].[vcAccountCode] IN('0103','0104','0106')
		AND ATD.bitActive = 1
	UNION ALL
		SELECT 
			CAST('' AS VARCHAR) AS ParentId,
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR), 
			[ATD].[numAccountTypeID], 
			0,
			CAST(CONCAT('Total ',[ATD].[vcAccountType]) AS VARCHAR(300)),
			[ATD].[vcAccountCode],
			1 AS LEVEL, 
			CAST(CONCAT(FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),CONCAT((CASE vcAccountCode WHEN '0103' THEN '1' WHEN '0106' THEN '2' WHEN '0104' THEN '3' END),[ATD].[vcAccountCode]),'#Total') AS VARCHAR(300))  AS Struc,
			1,
			0
		FROM 
			[dbo].[AccountTypeDetail] AS ATD
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND [ATD].[vcAccountCode] IN ('0103','0104','0106')
			AND ATD.bitActive = 1
			AND ISNULL(@bitAddTotalRow,0) = 1
	UNION ALL
	-- recursive
	SELECT 
		D.vcCompundParentKey AS ParentId, 
		CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
		[ATD].[numAccountTypeID], 
		0,
		CAST([ATD].[vcAccountType] AS VARCHAR(300)),
		[ATD].[vcAccountCode],
		LEVEL + 1, 
		CAST(d.Struc + '#' + CONCAT(FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),[ATD].[vcAccountCode]) AS varchar(300))  AS Struc,
		0,
		0
	FROM 
		[dbo].[AccountTypeDetail] AS ATD 
	JOIN 
		[DirectReport] D 
	ON 
		D.[numAccountTypeID] = [ATD].[numParentID]
		AND D.bitTotal = 0
	WHERE 
		[ATD].[numDomainID]=@numDomainId 
		AND ATD.bitActive = 1
	UNION ALL
	SELECT 
		D.vcCompundParentKey AS ParentId, 
		CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
		[ATD].[numAccountTypeID], 
		0,
		CAST(CONCAT('Total ',[ATD].[vcAccountType]) AS VARCHAR(300)),
		[ATD].[vcAccountCode],
		LEVEL + 1, 
		CAST(CONCAT(d.Struc,'#',FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),[ATD].[vcAccountCode],'#Total') AS VARCHAR(300))  AS Struc,
		1,
		0
	FROM 
		[dbo].[AccountTypeDetail] AS ATD 
	JOIN 
		[DirectReport] D 
	ON 
		D.[numAccountTypeID] = [ATD].[numParentID]
		AND D.bitTotal = 0
	WHERE 
		[ATD].[numDomainID]=@numDomainId 
		AND ATD.bitActive = 1
		AND ISNULL(@bitAddTotalRow,0) = 1
),
DirectReport1 (ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,numAccountId,Struc,bitTotal,bitIsSubAccount)
AS
(
	-- anchor
	SELECT 
		ParentId, 
		vcCompundParentKey,
		numAccountTypeID, 
		vcAccountType,
		vcAccountCode, 
		LEVEL,
		CAST(numAccountID AS NUMERIC(18)),
		Struc,
		bitTotal,
		CAST(0 AS BIT)
	FROM 
		DirectReport 
	UNION ALL
	-- recursive
	SELECT 
		CAST(CONCAT(COA.numParntAcntTypeId,'#',0) AS VARCHAR) AS ParentId,
		CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
		CAST(NULL AS NUMERIC(18)), 
		CAST(COA.[vcAccountName] AS VARCHAR(300)),
		COA.[vcAccountCode],
		LEVEL + 1,
		[COA].[numAccountId], 
		CAST(d.Struc + '#' + CAST([COA].[numAccountId] AS VARCHAR) AS VARCHAR(300))  AS Struc,
		0,
		ISNULL(COA.bitIsSubAccount,0)
	FROM 
		[dbo].[Chart_of_Accounts] AS COA 
	JOIN 
		[DirectReport1] D 
	ON 
		D.[numAccountTypeID] = COA.[numParntAcntTypeId]
		AND D.bitTotal = 0
	WHERE 
		[COA].[numDomainID]=@numDomainId 
		AND COA.bitActive = 1
		AND ISNULL(COA.bitIsSubAccount,0) = 0
	UNION ALL
	SELECT 
		CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numParentAccId) AS VARCHAR) AS ParentId,
		CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
		CAST(NULL AS NUMERIC(18)),
		CAST(COA.[vcAccountName] AS VARCHAR(300)),
		COA.[vcAccountCode],
		LEVEL + 1,
		[COA].[numAccountId], 
		CAST(d.Struc + '#' + CAST(COA.[numAccountId] AS VARCHAR) AS VARCHAR(300)) AS Struc
		,0
		,ISNULL(COA.bitIsSubAccount,0)
	FROM 
		[dbo].[Chart_of_Accounts] AS COA 
	JOIN 
		[DirectReport1] D 
	ON 
		D.numAccountId = COA.numParentAccId
		AND D.bitTotal = 0
	WHERE 
		[COA].[numDomainID]=@numDomainId 
		AND ISNULL(COA.bitActive,0) = 1
		AND ISNULL(COA.bitIsSubAccount,0) = 1
)

  
SELECT ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType, LEVEL, vcAccountCode,numAccountId,Struc, bitTotal, bitIsSubAccount INTO #tempDirectReport FROM DirectReport1 

INSERT INTO #tempDirectReport
SELECT '','-1',-1,'Operating Income/Expense',0,NULL,NULL,'-1',0,0
UNION ALL
SELECT '','-2',-2,'Other Income/Expense',0,NULL,NULL,'-2',0,0

IF ISNULL(@bitAddTotalRow,0) = 1
BEGIN
	INSERT INTO #tempDirectReport
	SELECT '','-1',-1,'Net Operating Income',0,NULL,NULL,'-1#Total',1,0
	UNION ALL
	SELECT '','-2',-2,'Net Other Income',0,NULL,NULL,'-2#Total',1,0
END	

DECLARE @cogsStruc VARCHAR(300)

SELECT @cogsStruc=Struc FROM #tempDirectReport WHERE vcAccountCode = '0106'

INSERT INTO #tempDirectReport SELECT '-1','-4',-4,'Gross Profit',0,NULL,NULL,CONCAT('-1#',@cogsStruc,'#grossprofit'),1,0

UPDATE #tempDirectReport SET Struc='-1#' + Struc WHERE [vcAccountCode] NOT LIKE '010302%' AND 
[vcAccountCode] NOT LIKE '010402%' 

UPDATE #tempDirectReport SET Struc='-2#' + Struc,[LEVEL]=[Level]-1 WHERE [vcAccountCode] LIKE '010302%' OR 
[vcAccountCode] LIKE '010402%'

UPDATE #tempDirectReport SET [ParentId]=-2 WHERE [vcAccountCode] IN('010302','010402')


SELECT 
	COA.ParentId
	,COA.vcCompundParentKey
	,COA.numAccountTypeID
	,COA.vcAccountType
	,COA.LEVEL
	,COA.vcAccountCode
	,COA.numAccountId
	,COA.Struc
	,(CASE 
		WHEN COA.vcAccountCode LIKE '010301%' OR COA.vcAccountCode LIKE '010302%' 
		THEN (ISNULL(Credit,0) - ISNULL(Debit,0))
		ELSE (ISNULL(Debit,0) - ISNULL(Credit,0)) 
	END) AS Amount
	,V.datEntry_Date
	,COA.bitTotal
	,COA.bitIsSubAccount
INTO 
	#tempViewData
FROM 
	#tempDirectReport COA 
JOIN 
	View_Journal_Master V 
ON  
	V.numAccountId = COA.numAccountId
	AND V.numDomainID=@numDomainId 
	AND datEntry_Date BETWEEN  @dtFromDate AND @dtToDate
	AND (V.numClassIDDetail=@numAccountClass OR @numAccountClass=0)
	AND (V.numDivisionId=@numDivisionID OR @numDivisionID=0)
WHERE 
	COA.[numAccountId] IS NOT NULL

DECLARE @columns VARCHAR(8000);--SET @columns = '';
DECLARE @SUMColumns VARCHAR(8000);--SET @SUMColumns = '';
DECLARE @PivotColumns VARCHAR(8000);--SET @PivotColumns = '';
DECLARE @Select VARCHAR(8000)
DECLARE @Where VARCHAR(8000)
SET @Select = 'SELECT ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType, LEVEL, vcAccountCode, numAccountId,(''#'' + Struc + ''#'') AS Struc'

IF @ReportColumn = 'Year' OR @ReportColumn = 'Quarter'
BEGIN
; WITH CTE AS (
	SELECT
		(CASE WHEN @ReportColumn = 'Quarter' THEN dbo.GetFiscalyear(@dtFromDate,@numDomainId) ELSE YEAR(@dtFromDate) END) AS 'yr',
		MONTH(@dtFromDate) AS 'mm',
		DATENAME(mm, @dtFromDate) AS 'mon',
		DATEPART(d,@dtFromDate) AS 'dd',
		dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS 'qq',
		@dtFromDate 'new_date'
	UNION ALL
	SELECT
		(CASE WHEN @ReportColumn = 'Quarter' THEN dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) ELSE YEAR(DATEADD(d,1,new_date)) END) AS 'yr',
		MONTH(DATEADD(d,1,new_date)) AS 'mm',
		DATENAME(mm, DATEADD(d,1,new_date)) AS 'mon',
		DATEPART(d,@dtFromDate) AS 'dd',
		dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS 'qq',
		DATEADD(d,1,new_date) 'new_date'
	FROM CTE
	WHERE DATEADD(d,1,new_date) < @dtToDate
	)
	
SELECT yr AS 'Year1',qq AS 'Quarter1', mm as 'Month1', mon AS 'MonthName1', count(dd) AS 'Days1' into #tempYearMonth
FROM CTE
GROUP BY mon, yr, mm, qq
ORDER BY yr, mm, qq
OPTION (MAXRECURSION 5000)

	IF @ReportColumn = 'Year'
		BEGIN
		SELECT
			@columns = COALESCE(@columns + ',ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
			',ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']'),
			@SUMColumns = COALESCE(@SUMColumns + '+ ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)',
			',ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)'),
			@PivotColumns = COALESCE(@PivotColumns + ',[' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
			'[' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
		FROM #tempYearMonth ORDER BY Year1,MONTH1

		SET @SUMColumns = @SUMColumns + ' AS Total, (CASE WHEN ISNULL(numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type], bitTotal, bitIsSubAccount'
		SET @Where = '	FROM (Select COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitTotal,COA.bitIsSubAccount,
				SUM(Case 
					When (COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2)
					THEN (Case WHEN V.vcAccountCode LIKE ''0104%'' OR V.vcAccountCode LIKE ''0106%'' THEN Amount * -1
					ELSE Amount END)
 				ELSE Amount END) AS Amount,
				DATENAME(mm,datEntry_Date) + '' '' + CAST(YEAR(datEntry_Date) AS VARCHAR(4)) AS MonthYear 
				FROM #tempDirectReport COA LEFT OUTER JOIN #tempViewData V ON  
				V.Struc like REPLACE(COA.Struc,''#Total'','''') + ''%'' 
				GROUP BY YEAR(datEntry_Date),MONTH(datEntry_Date),DATENAME(mm,datEntry_Date),COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitTotal,COA.bitIsSubAccount) AS t
				PIVOT
				(
				  SUM(Amount)
				  FOR [MonthYear] IN( ' + @PivotColumns +  ')
				) AS p ORDER BY Struc'

	END
	Else IF @ReportColumn = 'Quarter'
		BEGIN
		SELECT
		    @columns = COALESCE(@columns + ',ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
			',ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']'),
			@SUMColumns = COALESCE(@SUMColumns + '+ ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)',
			',ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)'),
			@PivotColumns = COALESCE(@PivotColumns + ',[Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
			'[Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
		FROM (SELECT DISTINCT Year1,Quarter1  FROM #tempYearMonth)T
		ORDER BY Year1,Quarter1

		SET @SUMColumns = @SUMColumns + ' AS Total, (CASE WHEN ISNULL(numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type], bitTotal,bitIsSubAccount'
		SET @Where = '	FROM (Select COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitTotal,COA.bitIsSubAccount,
				SUM(Case 
						When(COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2) 
						THEN (Case WHEN V.vcAccountCode LIKE ''0104%'' OR V.vcAccountCode LIKE ''0106%'' THEN Amount * -1
						ELSE Amount END)
					ELSE Amount END) AS Amount,
				''Q'' + cast(dbo.GetFiscalQuarter(datEntry_Date,' + CAST(@numDomainID AS VARCHAR(20)) + ') as varchar) + '' '' + CAST(dbo.GetFiscalyear(datEntry_Date,' + CAST(@numDomainID AS VARCHAR(20)) + ') AS VARCHAR(4)) AS MonthYear 
				FROM #tempDirectReport COA LEFT OUTER JOIN #tempViewData V ON  
				V.Struc like REPLACE(COA.Struc,''#Total'','''') + ''%'' 
				GROUP BY dbo.GetFiscalyear(datEntry_Date,' + CAST(@numDomainID AS VARCHAR(20)) + '),dbo.GetFiscalQuarter(datEntry_Date,' + CAST(@numDomainID AS VARCHAR(20)) + '),COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitTotal,COA.bitIsSubAccount) AS t
				PIVOT
				(
				  SUM(Amount)
				  FOR [MonthYear] IN( ' + @PivotColumns +  ')
				) AS p ORDER BY Struc'
	END

	DROP TABLE #tempYearMonth
END
ELSE
BEGIN
	SET @columns = ',ISNULL(Amount,0) as Total, (CASE WHEN ISNULL(numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type], bitTotal,bitIsSubAccount';
	SET @SUMColumns = '';
	SET @PivotColumns = '';
	SET @Where = ' FROM (Select COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitTotal,COA.bitIsSubAccount,
				SUM(Case 
						When (COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2)
						THEN (Case WHEN V.vcAccountCode LIKE ''0104%'' OR V.vcAccountCode LIKE ''0106%'' THEN Amount * -1
						ELSE Amount END)
					ELSE Amount END) AS Amount
				FROM #tempDirectReport COA LEFT OUTER JOIN #tempViewData V ON  
				V.Struc like REPLACE(COA.Struc,''#Total'','''') + ''%''  
				GROUP BY COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitTotal,COA.bitIsSubAccount) AS t
				ORDER BY Struc'
END

PRINT @Select
PRINT @columns
PRINT @SUMColumns
PRINT @Where


EXECUTE (@Select + @columns + @SUMColumns  + ' ' + @Where)

DROP TABLE #tempViewData 
DROP TABLE #tempDirectReport

END
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTrialBalance')
DROP PROCEDURE USP_GetTrialBalance
GO
CREATE PROCEDURE [dbo].[USP_GetTrialBalance]                                 
@numDomainId AS NUMERIC(9),                                                  
@dtFromDate AS DATETIME,                                                
@dtToDate AS DATETIME,
@ClientTimeZoneOffset INT, 
@numAccountClass AS NUMERIC(9)=0,
@DateFilter AS VARCHAR(20),
@ReportColumn AS VARCHAR(20),
@bitAddTotalRow BIT                                                        
AS                                                                
BEGIN        
	DECLARE @PLCHARTID NUMERIC(18,0) = 0

	SELECT @PLCHARTID=COA.numAccountId FROM Chart_of_Accounts COA WHERE numDomainID=@numDomainId AND bitProfitLoss=1

	DECLARE @FinancialYearStartDate AS DATETIME
	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainId)

	DECLARE @TEMPQuarter AS TABLE
	(
		ID INT,
		StartDate DATETIME,
		EndDate DATETIME
	)

	INSERT INTO @TEMPQuarter VALUES (1,@FinancialYearStartDate,DATEADD(ms,-3,DATEADD(QUARTER,1,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (2,DATEADD(QUARTER,1,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,2,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (3,DATEADD(QUARTER,2,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,3,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (4,DATEADD(QUARTER,3,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,4,@FinancialYearStartDate)))

	IF @DateFilter = 'CurYear'
	BEGIN
		SELECT @dtFromDate = @FinancialYearStartDate,
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'PreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,DATEADD(YEAR,-1,@FinancialYearStartDate)));
	END
	ELSE IF @DateFilter = 'CurPreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'CuQur'
	BEGIN
		SELECT @dtFromDate = StartDate, @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'CurPreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'PreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=DATEADD(QUARTER,-1,EndDate) FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'LastMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)));  
	END
	ELSE IF @DateFilter = 'ThisMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0)));  
	END
	ELSE IF @DateFilter = 'CurPreMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0))); 
	END

	CREATE TABLE #view_journal
	(
		numAccountId NUMERIC(18,0),
		vcAccountCode VARCHAR(50),
		COAvcAccountCode VARCHAR(100),
		datEntry_Date SMALLDATETIME,
		Debit DECIMAL(20,5),
		Credit DECIMAL(20,5)
	)

	INSERT INTO #view_journal SELECT numAccountId,vcAccountCode,COAvcAccountCode,datEntry_Date,Debit,Credit FROM view_journal WHERE numDomainId = @numDomainID AND (numAccountClass=@numAccountClass OR @numAccountClass=0);

	DECLARE @dtFinYearFromJournal DATETIME ;
	DECLARE @dtFinYearFrom DATETIME ;

	SELECT @dtFinYearFromJournal = MIN(datEntry_Date) FROM #view_journal
	SET @dtFinYearFrom = (SELECT dtPeriodFrom FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND dtPeriodTo >= @dtFromDate AND numDomainId = @numDomainId)

	

	;WITH DirectReport (ParentId, vcCompundParentKey, numAccountTypeID, numAccountID, vcAccountType,vcAccountCode, LEVEL,Struc,bitTotal)
	AS
	(
		SELECT 
			CAST('' AS VARCHAR) AS ParentId, 
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
			[ATD].[numAccountTypeID], 
			0,
			CAST([ATD].[vcAccountType] AS VARCHAR(300)),
			[ATD].[vcAccountCode],
			0 AS LEVEL, 
			CAST(CONCAT(FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),[ATD].[vcAccountCode]) AS VARCHAR(300))  AS Struc,
			0
		FROM 
			[dbo].[AccountTypeDetail] AS ATD
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND vcAccountCode IN ('0101','0102','0103','0104','0105','0106')
			AND ATD.bitActive = 1
		UNION ALL
		SELECT 
			CAST('' AS VARCHAR) AS ParentId, 
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
			[ATD].[numAccountTypeID], 
			0,
			CAST(CONCAT('Total ',[ATD].[vcAccountType]) AS VARCHAR(300)),
			[ATD].[vcAccountCode],
			0 AS LEVEL, 
			CAST(CONCAT(FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),[ATD].[vcAccountCode],'#Total') AS VARCHAR(300))  AS Struc,
			1
		FROM 
			[dbo].[AccountTypeDetail] AS ATD
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND vcAccountCode IN ('0101','0102','0103','0104','0105','0106')
			AND ATD.bitActive = 1
			AND ISNULL(@bitAddTotalRow,0) = 1
		UNION ALL
		SELECT 
			D.vcCompundParentKey AS ParentId,
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
			[ATD].[numAccountTypeID], 
			0,
			CAST([ATD].[vcAccountType] AS VARCHAR(300)),
			[ATD].[vcAccountCode],
			LEVEL + 1, 
			CAST(d.Struc + '#' + CONCAT(FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),[ATD].[vcAccountCode]) AS VARCHAR(300))  AS Struc,
			0
		FROM 
			[dbo].[AccountTypeDetail] AS ATD 
		JOIN 
			[DirectReport] D 
		ON 
			D.[numAccountTypeID] = [ATD].[numParentID]
			AND D.bitTotal = 0
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND ATD.bitActive = 1
		UNION ALL
		SELECT 
			D.vcCompundParentKey AS ParentId,
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
			[ATD].[numAccountTypeID], 
			0,
			CAST(CONCAT('Total ',[ATD].[vcAccountType]) AS VARCHAR(300)),
			[ATD].[vcAccountCode],
			LEVEL + 1, 
			CAST(CONCAT(d.Struc,'#',FORMAT(ISNULL(ATD.tintSortOrder,0),'000'),[ATD].[vcAccountCode],'#Total') AS VARCHAR(300))  AS Struc,
			1
		FROM 
			[dbo].[AccountTypeDetail] AS ATD 
		JOIN 
			[DirectReport] D 
		ON 
			D.[numAccountTypeID] = [ATD].[numParentID]
			AND D.bitTotal = 0
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND ATD.bitActive = 1
			AND ISNULL(@bitAddTotalRow,0) = 1
	),
	DirectReport1 (ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType, vcAccountCode, LEVEL,numAccountId,Struc, bitProfitLoss, bitTotal)
	AS
	(
		SELECT 
			ParentId, 
			vcCompundParentKey,
			numAccountTypeID,
			vcAccountType,
			vcAccountCode, 
			LEVEL,
			CAST(numAccountID AS NUMERIC(18)),
			Struc,
			CAST(0 AS BIT),
			bitTotal
		FROM 
			DirectReport 
		UNION ALL
		SELECT 
			CAST(CONCAT(COA.numParntAcntTypeId,'#',0) AS VARCHAR) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
			CAST(NULL AS NUMERIC(18)),
			CAST([vcAccountName] AS VARCHAR(300)),
			COA.[vcAccountCode],
			LEVEL + 1,
			[COA].[numAccountId], 
			CAST(d.Struc + '#' + CAST(COA.[numAccountId] AS VARCHAR) AS VARCHAR(300)) Struc,
			COA.bitProfitLoss,
			0
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport1] D 
		ON 
			D.[numAccountTypeID] = COA.[numParntAcntTypeId]
			AND D.bitTotal = 0
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitActive,0) = 1
			AND ISNULL(COA.bitIsSubAccount,0) = 0
		UNION ALL
		SELECT 
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numParentAccId) AS VARCHAR) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
			CAST(NULL AS NUMERIC(18)),
			CAST([vcAccountName] AS VARCHAR(300)),
			COA.[vcAccountCode],
			LEVEL + 1,
			[COA].[numAccountId], 
			CAST(d.Struc + '#' + CAST(COA.[numAccountId] AS VARCHAR) AS VARCHAR(300)) AS Struc,
			COA.bitProfitLoss,
			0
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport1] D 
		ON 
			D.numAccountId = COA.numParentAccId
			AND D.bitTotal = 0
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitActive,0) = 1
			AND ISNULL(COA.bitIsSubAccount,0) = 1
	)
  
	SELECT 
		ParentId, 
		vcCompundParentKey,
		numAccountTypeID, 
		vcAccountType, 
		LEVEL, 
		vcAccountCode,
		numAccountId,
		Struc,
		bitProfitLoss,
		bitTotal
	INTO 
		#tempDirectReport
	FROM 
		DirectReport1

	DECLARE @columns VARCHAR(8000) = ''
	DECLARE @PivotColumns VARCHAR(8000) = ''
	DECLARE @sql VARCHAR(MAX) = ''

	IF @ReportColumn = 'Year'
	BEGIN
		CREATE TABLE #TempYearMonth
		(
			intYear INT,
			intMonth INT,
			MonthYear VARCHAR(50),
			StartDate DATETIME,
			EndDate DATETIME,
			ProfitLossAmount DECIMAL(20,5)
		)
		
		;WITH CTE AS 
		(
			SELECT
				YEAR(@dtFromDate) AS 'yr',
				MONTH(@dtFromDate) AS 'mm',
				DATENAME(mm, @dtFromDate) AS 'mon',
				(FORMAT(@dtFromDate,'MMM') + '_' + CAST(YEAR(@dtFromDate) AS VARCHAR)) AS MonthYear,
				CAST(DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1) AS DATETIME) dtStartDate,
				DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1)) AS DATETIME)) dtEndDate,
				@dtFromDate 'new_date'
			UNION ALL
			SELECT
				YEAR(DATEADD(d,1,new_date)) AS 'yr',
				MONTH(DATEADD(d,1,new_date)) AS 'mm',
				DATENAME(mm, DATEADD(d,1,new_date)) AS 'mon',
				(FORMAT(DATEADD(d,1,new_date),'MMM') + '_' + CAST(YEAR(DATEADD(d,1,new_date)) AS VARCHAR)) AS MonthYear,
				CAST(DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1) AS DATETIME) dtStartDate,
				DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1)) AS DATETIME)) dtEndDate,
				DATEADD(d,1,new_date) 'new_date'
			FROM CTE
			WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		INSERT INTO 
			#tempYearMonth
		SELECT 
			yr,mm,MonthYear,dtStartDate,dtEndDate,0
		FROM 
			CTE
		GROUP BY 
			yr, mm, MonthYear, dtStartDate, dtEndDate
		ORDER BY 
			yr, mm
		OPTION (MAXRECURSION 5000)

		INSERT INTO #tempYearMonth VALUES (5000,5000,'Total',@dtFromDate,@dtToDate,0)

		UPDATE 
			#tempYearMonth
		SET
			EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)

		UPDATE 
			#tempYearMonth
		SET
			ProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode  LIKE '0106%') AND datEntry_Date <=  DATEADD(MS,-3,StartDate))
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND datEntry_Date <=  DATEADD(MS,-3,StartDate))

		SELECT @columns = STUFF((SELECT ', ISNULL(' + MonthYear  + ',0) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 
		SELECT @PivotColumns = STUFF((SELECT ', ' + MonthYear FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 

		SELECT 
			COA.ParentId, 
			COA.vcCompundParentKey,
			COA.numAccountTypeID, 
			COA.vcAccountType, 
			COA.LEVEL, 
			COA.vcAccountCode, 
			COA.numAccountId, 
			COA.Struc,
			COA.bitTotal,
			Period.MonthYear,
			Period.ProfitLossAmount,
			(CASE 
				WHEN bitProfitLoss = 1 
				THEN -Period.ProfitLossAmount
				ELSE
					(CASE 
						WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN 0 
						ELSE ISNULL(t2.OPENING,0) 
					END) + (ISNULL(Debit,0) - ISNULL(Credit,0)) 
			END) AS Amount
		INTO 
			#tempViewDataYear
		FROM 
			#tempDirectReport COA 
		OUTER APPLY
		(
			SELECT
				MonthYear,
				StartDate,
				EndDate,
				ProfitLossAmount
			FROM
				#tempYearMonth
		) AS Period
		OUTER APPLY
		(
			SELECT 
				SUM(Debit) AS Debit,
				SUM(Credit) AS Credit
			FROM
				#view_journal V 
			WHERE  
				V.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN Period.StartDate AND Period.EndDate
		) AS t1
		OUTER APPLY 
		(
			SELECT   
				SUM(Debit - Credit) AS OPENING
			FROM     
				#view_journal VJ
			WHERE    
				VJ.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN (CASE WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN @dtFinYearFrom ELSE @dtFinYearFromJournal END )
                                                              AND  DATEADD(MS,-3,Period.StartDate)
		)  AS t2

		SET @sql = 'SELECT
						ParentId,
						vcCompundParentKey,
						numAccountId,
						numAccountTypeID,
						vcAccountType,
						LEVEL,
						vcAccountCode,
						Struc,
						[Type],
						bitTotal,' + @columns + '
					FROM
					(
						SELECT 
							COA.ParentId, 
							COA.vcCompundParentKey,
							COA.numAccountId,
							COA.numAccountTypeID, 
							COA.vcAccountType, 
							COA.LEVEL, 
							COA.vcAccountCode, 
							COA.bitTotal,
							(''#'' + COA.Struc + ''#'') AS Struc,
							(CASE WHEN ISNULL(COA.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
							ISNULL(SUM(Amount),0) AS Amount,
							V.MonthYear
						FROM 
							#tempDirectReport COA 
						LEFT OUTER JOIN 
							#tempViewDataYear V 
						ON  
							V.Struc like REPLACE(COA.Struc,''#Total'','''') + ''%'' 
						GROUP BY 
							COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitProfitLoss,V.MonthYear,V.ProfitLossAmount,COA.bitTotal
						UNION
						SELECT 
							'''', 
							''-1'',
							NULL,
							NULL,
							''Total'',
							-1, 
							NULL, 
							1,
							''-1'',
							2,
							ISNULL((SELECT 
										ISNULL(SUM(Debit) - SUM(Credit),0)
									FROM 
										VIEW_Journal_Master
									WHERE 
										numDomainID = ' + CAST(@numDomainId AS VARCHAR) + '
										AND (AccountCode LIKE ''0101%'' 
										OR AccountCode LIKE ''0102%''
										OR AccountCode LIKE ''0103%''
										OR AccountCode LIKE ''0104%''
										OR AccountCode LIKE ''0105%''
										OR AccountCode LIKE ''0106%'')
										AND (numClassIDDetail=' + CAST(ISNULL(@numAccountClass,0) AS VARCHAR) + ' OR ' + CAST(ISNULL(@numAccountClass,0) AS VARCHAR) +  ' = 0)
										AND datEntry_Date BETWEEN #TempYearMonth.StartDate AND #TempYearMonth.EndDate),0),
							MonthYear
						FROM
							#TempYearMonth
					) AS SourceTable
					pivot
					(
						SUM(AMOUNT)
						FOR [MonthYear] IN ( ' + @pivotcolumns + ' )
					) AS P 
					ORDER BY Struc, [Type] desc'

		PRINT @sql
		EXECUTE (@sql)

		DROP TABLE #TempYearMonth
		DROP TABLE #tempViewDataYear
	END
	ELSE IF @ReportColumn = 'Quarter'
	BEGIN
		CREATE TABLE #TempYearMonthQuarter
		(
			intYear INT,
			intQuarter INT,
			MonthYear VARCHAR(50),
			StartDate DATETIME,
			EndDate DATETIME,
			ProfitLossAmount DECIMAL(20,5)
		)

		;WITH CTE AS 
		(
			SELECT
				dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS 'yr',
				dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS 'qq',
				('Q' + cast(dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS VARCHAR)) AS MonthYear,
				CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
				WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
				WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
				WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
				WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
				END AS dtStartDate,
				CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
				WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				END AS dtEndDate,
				@dtFromDate 'new_date'
			UNION ALL
			SELECT
				dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS 'yr',
				dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS 'qq',
				('Q' + cast(dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR)) AS MonthYear,
				CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
				WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
				WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
				WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
				WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
				END AS dtStartDate,
				CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
				WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				END AS dtEndDate,
				DATEADD(d,1,new_date) 'new_date'
			FROM CTE
			WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		INSERT INTO 
			#TempYearMonthQuarter 		
		SELECT 
			yr,qq,MonthYear,dtStartDate,dtEndDate,0
		FROM 
			CTE
		GROUP BY 
			yr, qq, MonthYear, dtStartDate, dtEndDate
		ORDER BY 
			yr, qq
		OPTION
			(MAXRECURSION 5000)

		INSERT INTO #TempYearMonthQuarter VALUES (5000,5000,'Total',@dtFromDate,@dtToDate,0)

		UPDATE 
			#TempYearMonthQuarter
		SET
			EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)

		UPDATE 
			#TempYearMonthQuarter
		SET
			ProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode  LIKE '0106%') AND datEntry_Date <=  DATEADD(MS,-3,StartDate))
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND datEntry_Date <=  DATEADD(MS,-3,StartDate))

		SELECT @columns = STUFF((SELECT ', ISNULL(' + MonthYear  + ',0) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''); 
		SELECT @PivotColumns = STUFF((SELECT ', ' + MonthYear FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''); 

		SELECT 
			COA.ParentId, 
			COA.vcCompundParentKey,
			COA.numAccountTypeID, 
			COA.vcAccountType, 
			COA.LEVEL, 
			COA.vcAccountCode, 
			COA.numAccountId, 
			COA.Struc,
			COA.bitTotal,
			Period.MonthYear,
			Period.ProfitLossAmount,
			(CASE 
				WHEN bitProfitLoss = 1 
				THEN -Period.ProfitLossAmount
				ELSE
					(CASE 
						WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN 0 
						ELSE ISNULL(t2.OPENING,0) 
					END) + (ISNULL(Debit,0) - ISNULL(Credit,0))
			END) AS Amount
		INTO 
			#tempViewDataQuarter
		FROM 
			#tempDirectReport COA 
		OUTER APPLY
		(
			SELECT
				MonthYear,
				StartDate,
				EndDate,
				ProfitLossAmount
			FROM
				#TempYearMonthQuarter
		) AS Period
		OUTER APPLY
		(
			SELECT 
				SUM(Debit) AS Debit,
				SUM(Credit) AS Credit
			FROM
				#view_journal V 
			WHERE  
				V.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN Period.StartDate AND Period.EndDate
		) AS t1
		OUTER APPLY 
		(
			SELECT   
				SUM(Debit - Credit) AS OPENING
			FROM     
				#view_journal VJ
			WHERE    
				VJ.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN (CASE WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN @dtFinYearFrom ELSE @dtFinYearFromJournal END )
                                                              AND  DATEADD(MS,-3,Period.StartDate)
		)  AS t2
		
		SET @sql = 'SELECT
						ParentId,
						vcCompundParentKey,
						numAccountId,
						numAccountTypeID,
						vcAccountType,
						LEVEL,
						vcAccountCode,
						Struc,
						[Type],
						bitTotal,' + @columns + '
					FROM
					(
						SELECT 
							COA.ParentId, 
							COA.vcCompundParentKey,
							COA.numAccountId,
							COA.numAccountTypeID, 
							COA.vcAccountType, 
							COA.LEVEL, 
							COA.vcAccountCode, 
							COA.bitTotal,
							(''#'' + COA.Struc + ''#'') AS Struc,
							(CASE WHEN ISNULL(COA.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
							ISNULL(SUM(Amount),0)  AS Amount,
							V.MonthYear
						FROM 
							#tempDirectReport COA 
						LEFT OUTER JOIN 
							#tempViewDataQuarter V 
						ON  
							V.Struc like REPLACE(COA.Struc,''#Total'','''') + ''%'' 
						GROUP BY 
							COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitProfitLoss,V.MonthYear,V.ProfitLossAmount,COA.bitTotal
						UNION
						SELECT 
							'''',
							''-1'', 
							NULL,
							NULL,
							''Total'',
							-1, 
							NULL,
							1,
							''-1'',
							2,
							ISNULL((SELECT 
										ISNULL(SUM(Debit) - SUM(Credit),0)
									FROM 
										VIEW_Journal_Master
									WHERE 
										numDomainID = ' + CAST(@numDomainId AS VARCHAR) + '
										AND (AccountCode LIKE ''0101%'' 
										OR AccountCode LIKE ''0102%''
										OR AccountCode LIKE ''0103%''
										OR AccountCode LIKE ''0104%''
										OR AccountCode LIKE ''0105%''
										OR AccountCode LIKE ''0106%'')
										AND (numClassIDDetail=' + CAST(ISNULL(@numAccountClass,0) AS VARCHAR) + ' OR ' + CAST(ISNULL(@numAccountClass,0) AS VARCHAR) +  ' = 0)
										AND datEntry_Date BETWEEN #TempYearMonthQuarter.StartDate AND #TempYearMonthQuarter.EndDate),0),
							MonthYear
						FROM
							#TempYearMonthQuarter
					) AS SourceTable
					pivot
					(
						SUM(AMOUNT)
						FOR [MonthYear] IN ( ' + @pivotcolumns + ' )
					) AS P 
					ORDER BY Struc, [Type] desc'

		PRINT @sql
		EXECUTE (@sql)

		DROP TABLE #TempYearMonthQuarter
		DROP TABLE #tempViewDataQuarter
	END
	ELSE
	BEGIN
		DECLARE @monProfitLossAmount AS DECIMAL(20,5) = 0
		-- GETTING P&L VALUE
		SELECT @monProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND vcAccountCode LIKE '0103%' AND datEntry_Date between @dtFromDate and @dtToDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode  LIKE '0106%') AND datEntry_Date <=  DATEADD(MS,-3,@dtFromDate))
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND datEntry_Date <=  DATEADD(MS,-3,@dtFromDate))

		SELECT 
			COA.ParentId, 
			COA.numAccountTypeID, 
			COA.vcAccountType, 
			COA.LEVEL, 
			COA.vcAccountCode, 
			COA.numAccountId, 
			COA.Struc,
			COA.bitTotal,
			(CASE 
				WHEN bitProfitLoss = 1 
				THEN -@monProfitLossAmount
				ELSE
					(CASE 
						WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN 0 
						ELSE 
							ISNULL(t2.OPENING,0) 
					END) + (ISNULL(Debit,0) - ISNULL(Credit,0))
			END) AS Amount
		INTO 
			#tempViewData
		FROM 
			#tempDirectReport COA 
		OUTER APPLY
		(
			SELECT 
				SUM(Debit) AS Debit,
				SUM(Credit) AS Credit
			FROM
				#view_journal V 
			WHERE  
				V.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate
		) AS t1
		OUTER APPLY 
		(
			SELECT   
				SUM(Debit - Credit) AS OPENING
			FROM     
				#view_journal VJ
			WHERE    
				VJ.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN (CASE WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN @dtFinYearFrom ELSE @dtFinYearFromJournal END )
                                                              AND  DATEADD(MS,-3,@dtFromDate)
		)  AS t2

		SELECT 
			COA.ParentId, 
			COA.vcCompundParentKey,
			COA.numAccountId,
			COA.numAccountTypeID, 
			COA.vcAccountType, 
			COA.LEVEL, 
			COA.vcAccountCode, 
			COA.bitTotal,
			('#' + COA.Struc + '#') AS Struc,
			(CASE WHEN ISNULL(COA.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
			ISNULL(SUM(Amount),0) AS Total
		FROM 
			#tempDirectReport COA 
		LEFT OUTER JOIN 
			#tempViewData V 
		ON  
			V.Struc like REPLACE(COA.Struc,'#Total','') + '%' 
		GROUP BY 
			COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitProfitLoss,COA.bitTotal
		UNION
		SELECT 
			'', 
			'-1',
			NULL,
			-1, 
			'Total', 
			0, 
			NULL, 
			1,
			'-1' AS Struc,
			2,
			ISNULL((SELECT 
						SUM(Debit) - SUM(Credit)
					FROM 
						VIEW_Journal_Master
					WHERE 
						numDomainID = @numDomainId
						AND (AccountCode LIKE '0101%' 
						OR AccountCode LIKE '0102%'
						OR AccountCode LIKE '0103%'
						OR AccountCode LIKE '0104%'
						OR AccountCode LIKE '0105%'
						OR AccountCode LIKE '0106%')
						AND (numClassIDDetail=ISNULL(@numAccountClass,0) OR ISNULL(@numAccountClass,0) = 0)
						AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate),0) AS Total
		ORDER BY
			Struc, [Type] desc

		DROP TABLE #tempViewData 
	END

	DROP TABLE #view_journal
	DROP TABLE #tempDirectReport
END

/****** Object:  StoredProcedure [dbo].[USP_InsertDepositHeaderDet]    Script Date: 07/26/2008 16:19:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertdepositheaderdet')
DROP PROCEDURE usp_insertdepositheaderdet
GO
CREATE PROCEDURE [dbo].[USP_InsertDepositHeaderDet]                              
(@numDepositId AS NUMERIC(9)=0,        
@numChartAcntId AS NUMERIC(9)=0,                             
@datEntry_Date AS DATETIME,                                          
@numAmount AS DECIMAL(20,5),        
@numRecurringId AS NUMERIC(9)=0,                               
@numDomainId AS NUMERIC(9)=0,              
@numUserCntID AS NUMERIC(9)=0,
@strItems TEXT=''
,@numPaymentMethod NUMERIC
,@vcReference VARCHAR(500)
,@vcMemo VARCHAR(1000)
,@tintDepositeToType TINYINT=1 --1 = Direct Deposite to Bank Account , 2= Deposite to Default Undeposited Funds Account
,@numDivisionID NUMERIC
,@tintMode TINYINT =0 --0 = when called from receive payment page, 1 = when called from MakeDeposit Page
,@tintDepositePage TINYINT --1:Make Deposite 2:Receive Payment 3:Credit Memo(Sales Return)
,@numTransHistoryID NUMERIC(9)=0,
@numReturnHeaderID NUMERIC(9)=0,
@numCurrencyID numeric(9) =0,
@fltExchangeRate FLOAT = 1,
@numAccountClass NUMERIC(18,0)=0
)            
AS                                           
BEGIN 

IF ISNULL(@numCurrencyID,0) = 0 
BEGIN
 SET @fltExchangeRate=1
 SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
END
 

--Validation of closed financial year
EXEC dbo.USP_ValidateFinancialYearClosingDate @numDomainID,@datEntry_Date

DECLARE  @hDocItem INT
 
 IF @tintMode != 2
 BEGIN
	 IF @numRecurringId=0 SET @numRecurringId=NULL 
	 IF @numDivisionID=0 SET @numDivisionID=NULL
	 IF @numReturnHeaderID=0 SET @numReturnHeaderID=NULL
	 
	 IF @numDepositId=0                                  
	  BEGIN                              
		  INSERT INTO DepositMaster(numChartAcntId,dtDepositDate,monDepositAmount,numRecurringId,numDomainId,numCreatedBy,dtCreationDate,numPaymentMethod,vcReference,vcMemo,tintDepositeToType,numDivisionID,tintDepositePage,numTransHistoryID,numReturnHeaderID,numCurrencyID,fltExchangeRate,numAccountClass) VALUES            
	   (@numChartAcntId,@datEntry_Date,@numAmount,@numRecurringId,@numDomainId,@numUserCntID,GETUTCDATE(),@numPaymentMethod,@vcReference,@vcMemo,@tintDepositeToType,@numDivisionID,@tintDepositePage,@numTransHistoryID,@numReturnHeaderID,@numCurrencyID,ISNULL(@fltExchangeRate,1),@numAccountClass)                                        
	                         
	   SET @numDepositId = SCOPE_IDENTITY()                                        
	  END                            
	                        
	 IF @numDepositId<>0                        
	  BEGIN                        
		UPDATE DepositMaster SET numChartAcntId=@numChartAcntId,dtDepositDate=@datEntry_Date,monDepositAmount=@numAmount,numRecurringId=@numRecurringId,
	   numModifiedBy=@numUserCntID,dtModifiedDate=GETUTCDATE(),numPaymentMethod =@numPaymentMethod,vcReference=@vcReference,vcMemo=@vcMemo,tintDepositeToType =@tintDepositeToType,numDivisionID=@numDivisionID,tintDepositePage=@tintDepositePage,numTransHistoryID=@numTransHistoryID ,numCurrencyID=@numCurrencyID,fltExchangeRate=@fltExchangeRate
	   WHERE numDepositId=@numDepositId AND numDomainId=@numDomainId
	  END            
 END 
  
  IF @tintMode = 1
  BEGIN
  
 
      IF CONVERT(VARCHAR(10),@strItems) <> ''
        BEGIN
          EXEC sp_xml_preparedocument
            @hDocItem OUTPUT ,
            @strItems
            
				-- When user unchecks deposit entry of undeposited fund, reset original entry
				UPDATE dbo.DepositMaster 
				SET bitDepositedToAcnt = 0 WHERE numDepositId IN (  
				
				 SELECT numChildDepositID FROM dbo.DepositeDetails WHERE numDepositID = @numDepositId AND numChildDepositID>0 
				 AND numDepositeDetailID NOT IN (SELECT X.numDepositeDetailID 
												FROM OPENXML(@hDocItem,'/NewDataSet/Item [numDepositeDetailID>0]',2)                                                                                                              
												WITH(numDepositeDetailID NUMERIC(9))X)
				
				)
			
			
              DELETE FROM dbo.DepositeDetails WHERE numDepositID = @numDepositId
              AND numDepositeDetailID NOT IN (SELECT X.numDepositeDetailID 
				 FROM OPENXML(@hDocItem,'/NewDataSet/Item [numDepositeDetailID>0]',2)                                                                                                              
				WITH(numDepositeDetailID NUMERIC(9))X)
			
			  INSERT INTO dbo.DepositeDetails
					  ( 
						numDepositID ,
						numOppBizDocsID ,
						numOppID ,
						monAmountPaid,
						numChildDepositID,
						numPaymentMethod,
						vcMemo,
						vcReference,
						numAccountID,
						numClassID,
						numProjectID,
						numReceivedFrom,dtCreatedDate,dtModifiedDate
					  )
			  SELECT 
					 @numDepositId,
					 NULL ,
					 NULL ,
					 X.monAmountPaid,
					 X.numChildDepositID,
					 X.numPaymentMethod,
						X.vcMemo,
						X.vcReference,
						X.numAccountID,
						X.numClassID,
						X.numProjectID,
						X.numReceivedFrom,GETUTCDATE(),GETUTCDATE()
			  FROM   (SELECT *
					  FROM   OPENXML (@hDocItem, '/NewDataSet/Item [numDepositeDetailID=0]', 2)
								WITH (
								numDepositeDetailID NUMERIC,
								numChildDepositID NUMERIC(9),
								monAmountPaid DECIMAL(20,5),
								numPaymentMethod NUMERIC(18, 0),
								vcMemo VARCHAR(1000),
								vcReference VARCHAR(500),
								numClassID	NUMERIC(18, 0),	
								numProjectID	NUMERIC(18, 0),
								numReceivedFrom	NUMERIC(18, 0),
								numAccountID NUMERIC(18, 0)
										)) X 
								
				UPDATE dbo.DepositeDetails
				SET
						[monAmountPaid] = X.monAmountPaid,
						[numChildDepositID] = X.numChildDepositID,
						[numPaymentMethod] = X.numPaymentMethod,
						[vcMemo] = X.vcMemo,
						[vcReference] = X.vcReference,
						[numClassID] = X.numClassID,
						[numProjectID] = X.numProjectID,
						[numReceivedFrom] = X.numReceivedFrom,
						[numAccountID] = X.numAccountID,
						dtModifiedDate=GETUTCDATE()
				FROM (SELECT *
									  FROM   OPENXML (@hDocItem, '/NewDataSet/Item [numDepositeDetailID>0]', 2)
												WITH (
												numDepositeDetailID NUMERIC,
												numChildDepositID NUMERIC(9),
												monAmountPaid DECIMAL(20,5),
												numPaymentMethod NUMERIC(18, 0),
												vcMemo VARCHAR(1000),
												vcReference VARCHAR(500),
												numClassID	NUMERIC(18, 0),	
												numProjectID	NUMERIC(18, 0),
												numReceivedFrom	NUMERIC(18, 0),
												numAccountID NUMERIC(18, 0)
														)) X 
				WHERE X.numDepositeDetailID =DepositeDetails.numDepositeDetailID AND dbo.DepositeDetails.numDepositID = @numDepositId




				UPDATE dbo.DepositMaster SET bitDepositedToAcnt=1 WHERE numDomainId=@numDomainID AND  numDepositId IN (
									   SELECT numChildDepositID
									  FROM   OPENXML (@hDocItem, '/NewDataSet/Item', 2)
												WITH (numChildDepositID NUMERIC(9)) WHERE numChildDepositID > 0
				)
				
								
  								
          EXEC sp_xml_removedocument
            @hDocItem
        END
  
  
  END 
  
  
  
  IF @tintMode = 0 OR @tintMode = 2
  BEGIN
      IF CONVERT(VARCHAR(10),@strItems) <> ''
        BEGIN
				  EXEC sp_xml_preparedocument
					@hDocItem OUTPUT ,
					@strItems
						
					  SELECT 
					  numDepositeDetailID,numOppBizDocsID ,numOppID ,monAmountPaid INTO #temp
					  FROM   (SELECT *
							  FROM   OPENXML (@hDocItem, '/NewDataSet/Item', 2)
										WITH (numDepositeDetailID NUMERIC(9),numOppBizDocsID NUMERIC(9),numOppID NUMERIC(9),monAmountPaid DECIMAL(20,5))) X
				 	
					
					IF @tintMode = 0
					BEGIN
						UPDATE OBD
						SET monAmountPaid = ISNULL(OBD.monAmountPaid,0) - DD.monAmountPaid,OBD.numModifiedBy=@numUserCntID
						FROM OpportunityBizDocs OBD JOIN DepositeDetails DD ON OBD.numOppBizDocsId=DD.numOppBizDocsID
						WHERE DD.numDepositID = @numDepositId AND DD.numOppBizDocsID>0 
							 AND DD.numDepositeDetailID NOT IN (SELECT numDepositeDetailID 
														FROM #temp WHERE numDepositeDetailID>0)
														
													
						  DELETE FROM dbo.DepositeDetails WHERE numDepositID = @numDepositId
						  AND numDepositeDetailID NOT IN (SELECT X.numDepositeDetailID 
									FROM OPENXML(@hDocItem,'/NewDataSet/Item [numDepositeDetailID>0]',2)                                                                                                              
									WITH(numDepositeDetailID NUMERIC(9))X)
--							 AND numDepositeDetailID NOT IN (SELECT numDepositeDetailID 
--														FROM #temp WHERE numDepositeDetailID>0)
					 END
		              
		              
					  UPDATE  DD SET [monAmountPaid] = DD.monAmountPaid +  X.monAmountPaid 
								FROM DepositeDetails DD JOIN #temp X ON DD.numOppBizDocsID=X.numOppBizDocsID AND 
								DD.numOppID = X.numOppID
								WHERE X.numDepositeDetailID = 0 AND DD.numDepositID = @numDepositId
										
										
					  INSERT INTO dbo.DepositeDetails
							  ( numDepositID ,numOppBizDocsID ,numOppID ,monAmountPaid,dtCreatedDate,dtModifiedDate )
					  SELECT @numDepositId, numOppBizDocsID , numOppID , monAmountPaid,GETUTCDATE(),GETUTCDATE()
					  FROM #temp WHERE numDepositeDetailID=0 AND numOppBizDocsID NOT IN (SELECT numOppBizDocsID FROM DepositeDetails WHERE numDepositID = @numDepositId)
										
						
						IF @tintMode = 0
						BEGIN
								UPDATE  dbo.DepositeDetails
								SET     [monAmountPaid] = X.monAmountPaid ,
										numOppBizDocsID = X.numOppBizDocsID ,
										numOppID = X.numOppID,
										dtModifiedDate=GETUTCDATE()
								FROM    #temp X
								WHERE   X.numDepositeDetailID = DepositeDetails.numDepositeDetailID
										AND dbo.DepositeDetails.numDepositID = @numDepositId
						END


					UPDATE dbo.OpportunityBizDocs 
					SET dbo.OpportunityBizDocs.monAmountPaid = (SELECT ISNULL(SUM(ISNULL(monAmountPaid,0)),0) FROM DepositeDetails WHERE numOppBizDocsID  = OpportunityBizDocs.numOppBizDocsId AND numOppID  = OpportunityBizDocs.numOppID),numModifiedBy=@numUserCntID
					WHERE  OpportunityBizDocs.numOppBizDocsId IN (SELECT numOppBizDocsId FROM DepositeDetails WHERE numDepositId=@numDepositId)

					--Add to OpportunityAutomationQueue if full Amount Paid	
					INSERT INTO [dbo].[OpportunityAutomationQueue] ([numDomainID], [numOppId], [numOppBizDocsId], [numBizDocStatus], [numCreatedBy], [dtCreatedDate], [tintProcessStatus],numRuleID)
						SELECT OM.numDomainID, OM.numOppId, OBD.numOppBizDocsId, 0, @numUserCntID, GETUTCDATE(), 1,7	
						    FROM OpportunityBizDocs OBD JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
								WHERE OM.numDomainId=@numDomainId AND OBD.numOppBizDocsId IN (SELECT numOppBizDocsId FROM DepositeDetails WHERE numDepositId=@numDepositId)
								  AND ISNULL(OBD.monDealAmount,0)=ISNULL(OBD.monAmountPaid,0) AND ISNULL(OBD.monAmountPaid,0)>0

					--Add to OpportunityAutomationQueue if Balance due
					INSERT INTO [dbo].[OpportunityAutomationQueue] ([numDomainID], [numOppId], [numOppBizDocsId], [numBizDocStatus], [numCreatedBy], [dtCreatedDate], [tintProcessStatus],numRuleID)
						SELECT OM.numDomainID, OM.numOppId, OBD.numOppBizDocsId, 0, @numUserCntID, GETUTCDATE(), 1,14	
						    FROM OpportunityBizDocs OBD JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
								WHERE OM.numDomainId=@numDomainId AND OBD.numOppBizDocsId IN (SELECT numOppBizDocsId FROM DepositeDetails WHERE numDepositId=@numDepositId)
								  AND ISNULL(OBD.monDealAmount,0)>ISNULL(OBD.monAmountPaid,0) AND ISNULL(OBD.monAmountPaid,0)>0

														
  					UPDATE DepositMaster SET monAppliedAmount=(SELECT ISNULL(SUM(ISNULL(monAmountPaid,0)),0) FROM DepositeDetails WHERE numDepositId=@numDepositId)
  							WHERE numDepositId=@numDepositId
		  				
		  								
  					DROP TABLE #temp
		  								
				  EXEC sp_xml_removedocument
					@hDocItem
        END
        
        ELSE
        BEGIN
			--IF @tintMode = 0
			--BEGIN
				UPDATE OBD
				SET monAmountPaid = ISNULL(OBD.monAmountPaid,0) - DD.monAmountPaid,OBD.numModifiedBy=@numUserCntID
				FROM OpportunityBizDocs OBD JOIN DepositeDetails DD ON OBD.numOppBizDocsId=DD.numOppBizDocsID
				WHERE DD.numDepositID = @numDepositId 
											
				DELETE FROM dbo.DepositeDetails WHERE numDepositID = @numDepositId
				  
				UPDATE DepositMaster SET monAppliedAmount=(SELECT ISNULL(SUM(ISNULL(monAmountPaid,0)),0) FROM DepositeDetails WHERE numDepositId=@numDepositId)
  					WHERE numDepositId=@numDepositId
  		  
			--END
        END 
    END
  
  SELECT @numDepositId   
  
  BEGIN TRY
	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppBizDocsId NUMERIC(18,0)
	)

	INSERT INTO @TEMP
	(
		numOppBizDocsId
	)
	SELECT 
		numOppBizDocsId 
	FROM 
		DepositeDetails 
	WHERE 
		numDepositId=@numDepositId
		AND ISNULL(numOppBizDocsId,0) > 0

	DECLARE @i INT = 1
	DECLARE @iCount INT 
	DECLARE @numOppBizDocsId NUMERIC(18,0)

	SELECT @iCount = COUNT(*) FROM @TEMP

	WHILE @i <= @iCount
	BEGIN
		SELECT @numOppBizDocsId=numOppBizDocsId FROM @TEMP WHERE ID=@i
		EXEC USP_OpportunityBizDocs_CT @numDomainID,@numUserCntID,@numOppBizDocsId
		SET @i = @i + 1
	END

	
  END TRY
  BEGIN CATCH
	-- DO NOT RAISE ERROR
  END CATCH      
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetEcommerceWarehouseAvailability')
DROP PROCEDURE USP_Item_GetEcommerceWarehouseAvailability
GO
CREATE PROCEDURE [dbo].[USP_Item_GetEcommerceWarehouseAvailability]    
(
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@tintWarehouseAvailability TINYINT
	,@bitDisplayQtyAvailable BIT
)
AS
BEGIN
	SELECT STUFF((SELECT
					CONCAT('  ',vcWareHouse,(CASE WHEN @bitDisplayQtyAvailable=1 THEN CONCAT('(',numOnHand,')') ELSE '' END))
				FROM
				(
					SELECT 
						Warehouses.numWareHouseID
						,Warehouses.vcWareHouse
						,SUM(numOnHand) numOnHand
					FROM
						WarehouseItems
					INNER JOIN
						Warehouses
					ON
						WarehouseItems.numWareHouseID = Warehouses.numWareHouseID
					WHERE
						WarehouseItems.numDomainID=@numDomainID
						AND WarehouseItems.numItemID=@numItemCode
					GROUP BY
						Warehouses.numWareHouseID
						,Warehouses.vcWareHouse
				) TEMP
				WHERE
					1 = (CASE WHEN @tintWarehouseAvailability=2 THEN (CASE WHEN Temp.numOnHand > 0 THEN 1 ELSE 0 END) ELSE 1 END)
				ORDER BY
					vcWareHouse
				FOR XML PATH('')),1,1,'')
END
--SELECT * FROM [AccountTypeDetail] Where len(vcaccountcode)>4
--DELETE FROM [AccountTypeDetail] WHERE [numAccountTypeID]>2232
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageAccountType')
DROP PROCEDURE USP_ManageAccountType
GO
CREATE PROCEDURE USP_ManageAccountType
(@numAccountTypeID NUMERIC(9)=0 OUTPUT,
@vcAccountCode varchar(50),
@vcAccountType varchar(100),
@numParentID NUMERIC(9)=null,
@numDomainID NUMERIC(9),
@tintMode TINYINT,
@bitActive BIT = 1)
AS 
BEGIN
DECLARE @newAccountCode VARCHAR(50)
IF @tintMode = 1
BEGIN

	SELECT @newAccountCode=dbo.GetAccountTypeCode(@numDomainID,@numParentID,0);
	INSERT INTO AccountTypeDetail (
		vcAccountCode,
		vcAccountType,
		numParentID,
		numDomainID,
		dtCreateDate,
		dtModifiedDate,
		bitSystem,
		bitActive
	) VALUES ( 
		@newAccountCode,
		@vcAccountType,
		@numParentID,
		@numDomainID,
		GETUTCDATE(),
		GETUTCDATE(),
		0,
		ISNULL(@bitActive,1))
	SET @numAccountTypeID = SCOPE_IDENTITY()
END

IF @tintMode = 2
BEGIN
	IF	(SELECT  LEN(vcAccountCode) FROM [AccountTypeDetail] WHERE numAccountTypeID=@numAccountTypeID AND numDomainID=@numDomainID) > 4
	BEGIN
--		SELECT @newAccountCode= dbo.GetAccountTypeCode(@numDomainID,@numParentID);
--		UPDATE [AccountTypeDetail]
--		SET vcAccountCode=@newAccountCode,
--			vcAccountType=@vcAccountType,
--			numParentID=@numParentID
--		WHERE numAccountTypeID=@numAccountTypeID AND numDomainID=@numDomainID
--		-- Update Child if any 
		
		
--Commented by chintan, reason:any account faling under given account type is using prefix of account type code,when changing account type code,then child account codes become invalid.
--		EXEC USP_UpdateAccountTypeCode @numDomainID,@numAccountTypeID,@numParentID
		
		UPDATE [AccountTypeDetail] SET [vcAccountType]  = @vcAccountType,bitActive=ISNULL(@bitActive,1) WHERE [numAccountTypeID]=@numAccountTypeID AND [numDomainID]=@numDomainID
--		UPDATE [AccountTypeDetail]
--		SET vcAccountCode=dbo.GetAccountTypeCode(numDomainID,@numAccountTypeID),
--			numParentID=@numAccountTypeID
--		WHERE numParentID=@numAccountTypeID AND numDomainID=@numDomainID
--		
	END
		
END

IF @tintMode = 3
BEGIN
		IF EXISTS(SELECT * FROM dbo.AccountTypeDetail WHERE bitSystem=1 AND numDomainID=@numDomainID AND numAccountTypeID=@numAccountTypeID)
		BEGIN
			RAISERROR('SYSTEM', 16, 1);
			RETURN
		END
		
		IF EXISTS(SELECT * FROM dbo.AccountTypeDetail WHERE (vcAccountCode LIKE '01020102%' OR vcAccountCode LIKE '01010105') AND numDomainID=@numDomainID AND numAccountTypeID=@numAccountTypeID)
		BEGIN
			RAISERROR('AR/AP', 16, 1);
			RETURN
		END
		IF EXISTS(SELECT * FROM dbo.Chart_Of_Accounts WHERE numParntAcntTypeId=@numAccountTypeID)
		BEGIN
			RAISERROR('CHILD_ACCOUNT', 16, 1);
			RETURN
		END
		IF (SELECT COUNT(*) FROM [AccountTypeDetail] WHERE numParentID=@numAccountTypeID) >0
		BEGIN
			RAISERROR('CHILD', 16, 1);
			RETURN
		END
		ELSE
		BEGIN
		--only allow deltion of where account code lenth is > 4
			DELETE FROM [AccountTypeDetail] WHERE numDomainID=@numDomainID AND  numAccountTypeID=@numAccountTypeID AND LEN(vcAccountCode) > 4	
		END
		

END


	
	
	
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageBankReconcileMaster')
DROP PROCEDURE USP_ManageBankReconcileMaster
GO
CREATE PROCEDURE USP_ManageBankReconcileMaster
	@tintMode AS TINYINT, 
    @numDomainID numeric(18, 0),
    @numReconcileID numeric(18, 0),
    @numCreatedBy numeric(18, 0),
    @dtStatementDate datetime,
    @monBeginBalance DECIMAL(20,5),
    @monEndBalance DECIMAL(20,5),
    @numChartAcntId numeric(18, 0),
    @monServiceChargeAmount DECIMAL(20,5),
    @numServiceChargeChartAcntId numeric(18, 0),
    @dtServiceChargeDate datetime,
    @monInterestEarnedAmount DECIMAL(20,5),
    @numInterestEarnedChartAcntId numeric(18, 0),
    @dtInterestEarnedDate DATETIME,
    @bitReconcileComplete BIT=0,
	@vcFileName VARCHAR(300)='',
	@vcFileData VARCHAR(MAX) = ''
AS

--Validation of closed financial year
EXEC dbo.USP_ValidateFinancialYearClosingDate @numDomainID,@dtStatementDate
	
	IF @tintMode=1 --SELECT
	BEGIN
		DECLARE @numCreditAmt AS DECIMAL(20,5),@numDebitAmt AS DECIMAL(20,5) 
		
		SELECT 
			@numCreditAmt=ISNULL(SUM(numCreditAmt),0)
			,@numDebitAmt=ISNULL(SUM(numDebitAmt),0) 
		FROM 
			General_Journal_Details 
		WHERE 
			numDomainID=@numDomainID 
			AND [numReconcileID] = @numReconcileID 
			AND numChartAcntId=(SELECT numChartAcntId FROM [BankReconcileMaster] WHERE  numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID)
			AND numJournalId NOT IN (SELECT numJournal_Id FROM General_Journal_Header WHERE numDomainId=@numDomainID AND numReconcileID=@numReconcileID AND ISNULL(bitReconcileInterest,0) = 0)
		OPTION (OPTIMIZE FOR (@numDomainID UNKNOWN))
	
		SELECT 
			[numReconcileID]
			,[numDomainID]
			,[numCreatedBy]
			,[dtCreatedDate]
			,[dtStatementDate]
			,[monBeginBalance]
			,[monEndBalance]
			,[numChartAcntId]
			,[monServiceChargeAmount]
			,[numServiceChargeChartAcntId]
			,[dtServiceChargeDate]
			,[monInterestEarnedAmount]
			,[numInterestEarnedChartAcntId]
			,[dtInterestEarnedDate]
			,[bitReconcileComplete]
			,[dtReconcileDate]
			,@numCreditAmt AS Payment
			,@numDebitAmt AS Deposit
			,dbo.fn_GetContactName(ISNULL(numCreatedBy,0)) AS vcReconciledby
			,dbo.fn_GetChart_Of_AccountsName(ISNULL(numChartAcntId,0)) AS vcAccountName,ISNULL(vcFileName,'') AS vcFileName
		FROM 
			[BankReconcileMaster] 
		WHERE 
			numDomainID=@numDomainID 
			AND [numReconcileID] = @numReconcileID
	END
	ELSE IF @tintMode=2 --Insert/Update
	BEGIN
		IF @numReconcileID>0 --Update
			BEGIN
				UPDATE [BankReconcileMaster]
					SET    [dtStatementDate] = @dtStatementDate, [monBeginBalance]=@monBeginBalance, [monEndBalance]=@monEndBalance, [numChartAcntId] = @numChartAcntId, [monServiceChargeAmount] = @monServiceChargeAmount, [numServiceChargeChartAcntId] = @numServiceChargeChartAcntId,dtServiceChargeDate=@dtServiceChargeDate, [monInterestEarnedAmount] = @monInterestEarnedAmount, [numInterestEarnedChartAcntId] = @numInterestEarnedChartAcntId,dtInterestEarnedDate=@dtInterestEarnedDate
					WHERE  [numDomainID] = @numDomainID AND [numReconcileID] = @numReconcileID
			END
		ELSE --Insert
			BEGIN
				INSERT INTO [BankReconcileMaster] ([numDomainID], [numCreatedBy], [dtCreatedDate], [dtStatementDate], [monBeginBalance], [monEndBalance], [numChartAcntId], [monServiceChargeAmount], [numServiceChargeChartAcntId],dtServiceChargeDate,[monInterestEarnedAmount], [numInterestEarnedChartAcntId],dtInterestEarnedDate,vcFileName)
				SELECT @numDomainID, @numCreatedBy, GETUTCDATE(), @dtStatementDate, @monBeginBalance, @monEndBalance, @numChartAcntId, @monServiceChargeAmount, @numServiceChargeChartAcntId,@dtServiceChargeDate,@monInterestEarnedAmount, @numInterestEarnedChartAcntId,@dtInterestEarnedDate,@vcFileName
			
				SET @numReconcileID=SCOPE_IDENTITY()


				IF LEN(@vcFileName) > 0
				BEGIN
					DECLARE @hDocItem int
					EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcFileData

					INSERT INTO BankReconcileFileData
					(
						[numDOmainID],
						[numReconcileID],
						[dtEntryDate],
						[vcReference],
						[fltAmount],
						[vcPayee],
						[vcDescription]
					)
					SELECT 
						@numDomainID,
						@numReconcileID,
						dtEntryDate,
						vcReference,
						fltAmount,
						vcPayee,
						vcDescription
					FROM 
						OPENXML (@hDocItem,'/BankStatement/Trasactions',2)                                                                          
					WITH                       
					(                                                                          
						dtEntryDate Date, vcReference VARCHAR(500), fltAmount FLOAT, vcPayee VARCHAR(1000),vcDescription VARCHAR(1000)
					)
				END
			END	
			
			SELECT [numReconcileID], [numDomainID], [numCreatedBy], [dtCreatedDate], [dtStatementDate], [monBeginBalance], [monEndBalance], [numChartAcntId], [monServiceChargeAmount], [numServiceChargeChartAcntId], [monInterestEarnedAmount], [numInterestEarnedChartAcntId] 
			FROM   [BankReconcileMaster] WHERE  numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID
	   END
	 ELSE IF @tintMode=3 --Delete
	 BEGIN
	    --Delete Service Charge,Interest Earned and Adjustment entry
	    DELETE FROM General_Journal_Details WHERE numJournalId IN (SELECT numJournal_Id FROM General_Journal_Header WHERE numDomainID=@numDomainID AND ISNULL(numReconcileID,0) = @numReconcileID)
	    DELETE FROM General_Journal_Header WHERE numDomainID=@numDomainID AND ISNULL(numReconcileID,0) = @numReconcileID
	 
	    --Set Journal_Details entry to bitReconcile=0 & bitCleared=0
        UPDATE dbo.General_Journal_Details SET bitReconcile=0,bitCleared=0,numReconcileID=NULL WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID
        
		DELETE FROM [BankReconcileFileData] WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID

	 	DELETE FROM [BankReconcileMaster] WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID
	 END
	 ELSE IF @tintMode=4 -- Select based on numChartAcntId and bitReconcileComplete
	 BEGIN
		SELECT *,(SELECT ISNULL(SUM(numCreditAmt),0) FROM General_Journal_Details WHERE numDomainID=@numDomainID AND [numReconcileID] = BRM.numReconcileID /*AND ISNULL(bitReconcile,0)=1*/ AND numChartAcntId=BRM.numChartAcntId) AS monPayment
		,(SELECT ISNULL(SUM(numDebitAmt),0) FROM General_Journal_Details WHERE numDomainID=@numDomainID AND [numReconcileID] = BRM.numReconcileID /*AND ISNULL(bitReconcile,0)=1*/ AND numChartAcntId=BRM.numChartAcntId) AS monDeposit
	      FROM BankReconcileMaster BRM WHERE  numDomainID=@numDomainID AND numChartAcntId=@numChartAcntId 
			AND ISNULL(bitReconcileComplete,0)=@bitReconcileComplete ORDER BY numReconcileID DESC 
	 END
	 ELSE IF @tintMode=5 -- Complete Bank Reconcile
	 BEGIN
	    UPDATE General_Journal_Details SET bitReconcile=1,bitCleared=0 WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID

		UPDATE BankReconcileFileData SET bitReconcile=1,bitCleared=0 WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID AND ISNULL(bitCleared,0)=1
	 
		UPDATE BankReconcileMaster SET bitReconcileComplete=1,dtReconcileDate=GETUTCDATE(),numCreatedBy=@numCreatedBy 
			WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID
	 END
	ELSE IF @tintMode=6 -- Get Last added entry for numChartAcntId
	BEGIN
	SELECT TOP 1 *
	    FROM BankReconcileMaster BRM WHERE  numDomainID=@numDomainID AND numChartAcntId=@numChartAcntId 
		AND ISNULL(bitReconcileComplete,0)=1 ORDER BY numReconcileID DESC 
	END
	ELSE IF @tintMode = 7 -- Select based on numChartAcntId
	BEGIN
		SELECT *,(SELECT ISNULL(SUM(numCreditAmt),0) FROM General_Journal_Details WHERE numDomainID=@numDomainID AND [numReconcileID] = BRM.numReconcileID /*AND ISNULL(bitReconcile,0)=1*/ AND numChartAcntId=BRM.numChartAcntId) AS monPayment
		,(SELECT ISNULL(SUM(numDebitAmt),0) FROM General_Journal_Details WHERE numDomainID=@numDomainID AND [numReconcileID] = BRM.numReconcileID /*AND ISNULL(bitReconcile,0)=1*/ AND numChartAcntId=BRM.numChartAcntId) AS monDeposit
	      FROM BankReconcileMaster BRM WHERE  numDomainID=@numDomainID AND numChartAcntId=@numChartAcntId 
			ORDER BY numReconcileID DESC 
	END
    
        
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageBillPayment' ) 
    DROP PROCEDURE USP_ManageBillPayment
GO

CREATE PROCEDURE [dbo].[USP_ManageBillPayment]
    @numBillPaymentID NUMERIC(18, 0) =0 output,
    @dtPaymentDate DATETIME ,
    @numAccountID NUMERIC(18, 0) ,
    @numPaymentMethod NUMERIC(18, 0) ,
    @numUserCntID NUMERIC(18, 0) ,
    @numDomainID NUMERIC(18, 0) ,
    @strItems TEXT,
    @tintMode TINYINT=0,
    @monPaymentAmount DECIMAL(20,5)=0,
    @numDivisionID NUMERIC(18,0),
    @numReturnHeaderID NUMERIC(18,0),
    @numCurrencyID NUMERIC(18,0)=0,
    @fltExchangeRate FLOAT=1,
    @numAccountClass NUMERIC(18,0)=0
AS 
BEGIN
SET NOCOUNT ON
    DECLARE @numBillPaymentID_X NUMERIC(18,0)
    DECLARE @dtPaymentDate_X DATETIME
    DECLARE @numAccountID_X NUMERIC(18,0)
    DECLARE @numPaymentMethod_X NUMERIC(18,0)
    DECLARE @numUserCntID_X NUMERIC(18,0)
    DECLARE @numDomainID_X NUMERIC(18,0)
    DECLARE @strItems_X VARCHAR(MAX)
    DECLARE @tintMode_X TINYINT=0
    DECLARE @monPaymentAmount_X DECIMAL(20,5)
    DECLARE @numDivisionID_X NUMERIC(18,0)
    DECLARE @numReturnHeaderID_X NUMERIC(18,0)
    DECLARE @numCurrencyID_X NUMERIC(18,0)
    DECLARE @fltExchangeRate_X FLOAT
    DECLARE @numAccountClass_X NUMERIC(18,0)

    SET @numBillPaymentID_X = @numBillPaymentID
	SET @dtPaymentDate_X = @dtPaymentDate
	SET @numAccountID_X = @numAccountID
	SET @numPaymentMethod_X = @numPaymentMethod
	SET @numUserCntID_X = @numUserCntID
	SET @numDomainID_X = @numDomainID
	SET @strItems_X = @strItems
	SET @tintMode_X = @tintMode
	SET @monPaymentAmount_X = @monPaymentAmount
	SET @numDivisionID_X = @numDivisionID
	SET @numReturnHeaderID_X = @numReturnHeaderID
	SET @numCurrencyID_X = @numCurrencyID
	SET @fltExchangeRate_X = @fltExchangeRate
	SET @numAccountClass_X = @numAccountClass
    
BEGIN TRY 
        BEGIN TRAN  
        
			IF ISNULL(@numCurrencyID_X,0) = 0 
			BEGIN
			 SET @fltExchangeRate_X=1
			 SELECT @numCurrencyID_X= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainID_X	
			END
 
        
			IF @tintMode_X=1
			BEGIN
				--Validation of closed financial year
				EXEC dbo.USP_ValidateFinancialYearClosingDate @numDomainID_X,@dtPaymentDate_X
				
				
				IF EXISTS ( SELECT  [numBillPaymentID] FROM    [dbo].[BillPaymentHeader] WHERE   [numBillPaymentID] = @numBillPaymentID_X ) 
					BEGIN
						UPDATE  [dbo].[BillPaymentHeader] 
						SET     [dtPaymentDate] = @dtPaymentDate_X ,[numAccountID] = @numAccountID_X ,
								[numPaymentMethod] = @numPaymentMethod_X ,[numModifiedDate] = GETUTCDATE() ,
								[numModifiedBy] = @numUserCntID_X,monPaymentAmount=@monPaymentAmount_X,numCurrencyID=@numCurrencyID_X,fltExchangeRate=@fltExchangeRate_X
						WHERE   [numBillPaymentID] = @numBillPaymentID_X AND numDomainID =@numDomainID_X
					END
				ELSE 
					BEGIN
						INSERT  INTO [dbo].[BillPaymentHeader]
								( [dtPaymentDate] ,[numAccountID] ,[numPaymentMethod] ,[dtCreateDate] ,[numCreatedBy] ,[numModifiedDate] ,
								  [numModifiedBy],numDomainID,monPaymentAmount,numDivisionID,numReturnHeaderID,numCurrencyID,fltExchangeRate,numAccountClass
								)
						VALUES  ( 
								  @dtPaymentDate_X ,@numAccountID_X ,@numPaymentMethod_X ,GETUTCDATE() ,@numUserCntID_X ,GETUTCDATE() ,
								  @numUserCntID_X,@numDomainID_X,@monPaymentAmount_X,@numDivisionID_X,@numReturnHeaderID_X,@numCurrencyID_X,@fltExchangeRate_X,@numAccountClass_X
								)
						SET @numBillPaymentID = SCOPE_IDENTITY()
						SET @numBillPaymentID_X = @numBillPaymentID
					END
				END	
				
				
				DECLARE @hDocItem INT
				IF CONVERT(VARCHAR(10), @strItems_X) <> '' 
				BEGIN
					EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems_X
--						INSERT  INTO [dbo].[BillPaymentDetails]
--								( [numBillPaymentID] ,[tintBillType] ,[numOppBizDocsID] ,[numBillID] ,[monAmount])
					SELECT  X.tintBillType ,ISNULL(X.numOppBizDocsID,0) AS  numOppBizDocsID,ISNULL(X.numBillID,0) AS numBillID ,X.monAmount INTO #temp
								FROM    ( SELECT    * FROM      OPENXML (@hDocItem, '/NewDataSet/Item', 2)
										WITH ( tintBillType TINYINT,numOppBizDocsID NUMERIC(18, 0),numBillID NUMERIC(18, 0),monAmount DECIMAL(20,5))
								) X
					
					 EXEC sp_xml_removedocument @hDocItem
					 
					IF @tintMode_X = 1
					BEGIN			
						--Revert back all amount for BillHeader
						 UPDATE BH SET monAmtPaid = monAmtPaid - BPD.monAmount,
						 bitIsPaid = CASE WHEN monAmountDue -( monAmtPaid - BPD.monAmount) = 0 THEN 1 ELSE 0 END 
						 FROM BillHeader BH JOIN BillPaymentDetails BPD ON BPD.numBillID > 0 AND BH.numBillID = BPD.numBillID
						 WHERE BPD.numBillPaymentID = @numBillPaymentID_X 
						 
						 --Revert back all amount for OpportunityBizDocs
						UPDATE OBD SET monAmountPaid = monAmountPaid - BPD.monAmount
						FROM OpportunityBizDocs OBD JOIN BillPaymentDetails BPD ON BPD.numOppBizDocsID > 0 AND OBD.numOppBizDocsId = BPD.numOppBizDocsID
						WHERE BPD.numBillPaymentID = @numBillPaymentID_X
						
						--Delete all BillPaymentDetails entries 				 
						DELETE FROM dbo.BillPaymentDetails WHERE numBillPaymentID = @numBillPaymentID_X 
					END		
						
				UPDATE BPD SET monAmount=BPD.monAmount + X.monAmount,dtAppliedDate=GETUTCDATE()
						FROM dbo.BillPaymentDetails BPD JOIN  #temp X ON BPD.numBillID=X.numBillID 
						WHERE BPD.numBillPaymentID=@numBillPaymentID_X AND X.tintBillType=2
				
				UPDATE BPD SET monAmount=BPD.monAmount + X.monAmount,dtAppliedDate=GETUTCDATE()
						FROM dbo.BillPaymentDetails BPD JOIN  #temp X ON BPD.numOppBizDocsID=X.numOppBizDocsID 
						WHERE BPD.numBillPaymentID=@numBillPaymentID_X AND X.tintBillType=1	
				
				INSERT  INTO [dbo].[BillPaymentDetails]
					( [numBillPaymentID] ,[tintBillType] ,[numOppBizDocsID] ,[numBillID] ,[monAmount],dtAppliedDate)
				 SELECT @numBillPaymentID_X,X.tintBillType ,X.numOppBizDocsID ,X.numBillID ,X.monAmount,GETUTCDATE() 
						FROM #temp X WHERE X.tintBillType=1 AND X.numOppBizDocsID NOT IN (SELECT numOppBizDocsID FROM BillPaymentDetails WHERE numBillPaymentID=@numBillPaymentID_X)
				UNION
				SELECT @numBillPaymentID_X,X.tintBillType ,X.numOppBizDocsID ,X.numBillID ,X.monAmount,GETUTCDATE() 
						FROM #temp X WHERE X.tintBillType=2 AND X.numBillID NOT IN (SELECT numBillID FROM BillPaymentDetails WHERE numBillPaymentID=@numBillPaymentID_X)
						

			   UPDATE dbo.BillHeader SET monAmtPaid = monAmtPaid + X.monAmount,
					bitIsPaid = CASE WHEN monAmountDue -( monAmtPaid + X.monAmount) = 0 THEN 1 ELSE 0 END 
					FROM  #temp X 
					WHERE X.numBillID>0 AND dbo.BillHeader.numBillID = X.numBillID  
			
									 
				UPDATE dbo.OpportunityBizDocs SET monAmountPaid = monAmountPaid + monAmount
						FROM #temp X  
						WHERE X.numOppBizDocsID>0 AND dbo.OpportunityBizDocs.numOppBizDocsId = X.numOppBizDocsID
				
				--Add to OpportunityAutomationQueue if full Amount Paid	
					INSERT INTO [dbo].[OpportunityAutomationQueue] ([numDomainID], [numOppId], [numOppBizDocsId], [numBizDocStatus], [numCreatedBy], [dtCreatedDate], [tintProcessStatus],numRuleID)
						SELECT OM.numDomainID, OM.numOppId, OBD.numOppBizDocsId, 0, @numUserCntID_X, GETUTCDATE(), 1,15	
						    FROM OpportunityBizDocs OBD JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
								WHERE OM.numDomainId=@numDomainID_X AND OBD.numOppBizDocsId IN (SELECT numOppBizDocsID FROM BillPaymentDetails WHERE numBillPaymentID=@numBillPaymentID_X AND ISNULL(numOppBizDocsID,0)>0)
								  AND ISNULL(OBD.monDealAmount,0)=ISNULL(OBD.monAmountPaid,0) AND ISNULL(OBD.monAmountPaid,0)>0

				--Add to OpportunityAutomationQueue if Balance due
					INSERT INTO [dbo].[OpportunityAutomationQueue] ([numDomainID], [numOppId], [numOppBizDocsId], [numBizDocStatus], [numCreatedBy], [dtCreatedDate], [tintProcessStatus],numRuleID)
						SELECT OM.numDomainID, OM.numOppId, OBD.numOppBizDocsId, 0, @numUserCntID_X, GETUTCDATE(), 1,16	
						    FROM OpportunityBizDocs OBD JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
								WHERE OM.numDomainId=@numDomainID_X AND OBD.numOppBizDocsId IN (SELECT numOppBizDocsID FROM BillPaymentDetails WHERE numBillPaymentID=@numBillPaymentID_X AND ISNULL(numOppBizDocsID,0)>0)
								  AND ISNULL(OBD.monDealAmount,0)>ISNULL(OBD.monAmountPaid,0) AND ISNULL(OBD.monAmountPaid,0)>0

				DROP TABLE #temp
				
				UPDATE BillPaymentHeader SET monAppliedAmount=(SELECT ISNULL(SUM(ISNULL(monAmount,0)),0) FROM BillPaymentDetails WHERE numBillPaymentID=@numBillPaymentID_X)
  							WHERE numBillPaymentID=@numBillPaymentID_X 
			END
			ELSE IF @tintMode_X=2
			BEGIN
				--Revert back all amount for BillHeader
				 UPDATE BH SET monAmtPaid = monAmtPaid - BPD.monAmount,
				 bitIsPaid = CASE WHEN monAmountDue -( monAmtPaid - BPD.monAmount) = 0 THEN 1 ELSE 0 END 
				 FROM BillHeader BH JOIN BillPaymentDetails BPD ON BPD.numBillID > 0 AND BH.numBillID = BPD.numBillID
				 WHERE BPD.numBillPaymentID = @numBillPaymentID_X 
						 
				--Revert back all amount for OpportunityBizDocs
				UPDATE OBD SET monAmountPaid = monAmountPaid - BPD.monAmount
				FROM OpportunityBizDocs OBD JOIN BillPaymentDetails BPD ON BPD.numOppBizDocsID > 0 AND OBD.numOppBizDocsId = BPD.numOppBizDocsID
				WHERE BPD.numBillPaymentID = @numBillPaymentID_X
						
				--Delete all BillPaymentDetails entries 				 
				DELETE FROM dbo.BillPaymentDetails WHERE numBillPaymentID = @numBillPaymentID_X 
				
				UPDATE BillPaymentHeader SET monAppliedAmount=(SELECT ISNULL(SUM(ISNULL(monAmount,0)),0) FROM BillPaymentDetails WHERE numBillPaymentID=@numBillPaymentID_X)
  							WHERE numBillPaymentID=@numBillPaymentID_X 
			END
        COMMIT TRAN 
    END TRY 
    BEGIN CATCH		
        DECLARE @strMsg VARCHAR(200)
        SET @strMsg = ERROR_MESSAGE()
        IF ( @@TRANCOUNT > 0 ) 
            BEGIN
                RAISERROR ( @strMsg, 16, 1 ) ;
                ROLLBACK TRAN
                RETURN 1
            END
    END CATCH	

	BEGIN TRY
	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppBizDocsId NUMERIC(18,0)
	)

	INSERT INTO @TEMP
	(
		numOppBizDocsId
	)
	SELECT 
		numOppBizDocsId 
	FROM 
		BillPaymentDetails 
	WHERE 
		numBillPaymentID=@numBillPaymentID_X
		AND ISNULL(numOppBizDocsId,0) > 0

	DECLARE @i INT = 1
	DECLARE @iCount INT 
	DECLARE @numOppBizDocsId NUMERIC(18,0)

	SELECT @iCount = COUNT(*) FROM @TEMP

	WHILE @i <= @iCount
	BEGIN
		SELECT @numOppBizDocsId=numOppBizDocsId FROM @TEMP WHERE ID=@i
		EXEC USP_OpportunityBizDocs_CT @numDomainID,@numUserCntID,@numOppBizDocsId
		SET @i = @i + 1
	END

	
  END TRY
  BEGIN CATCH
	-- DO NOT RAISE ERROR
  END CATCH   
END
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

--created by anoop jayaraj                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managetrackingvisitors')
DROP PROCEDURE usp_managetrackingvisitors
GO
CREATE PROCEDURE [dbo].[USP_ManageTrackingVisitors]                      
@vcUserHostAddress as varchar(100)='',
@vcUserDomain as varchar(100)='',
@vcUserAgent as varchar(1000)='',
@vcBrowser as varchar(50)='',
@vcCrawler as varchar(50)='',
@vcURL as varchar(500)='',
@vcReferrer as varchar(500)='',
@numVisits as numeric(9)=0,
@vcOrginalURL as varchar(500)='',                      
@vcOrginalRef as varchar(500)='',                      
@numVistedPages as numeric(9)=0,                      
@vcTotalTime as varchar(100)='',                      
@strURL as text='',                               
@vcCountry as varchar(50),                
@IsVisited as bit,
@numDivisionID as numeric(9)  ,    
@numDomainId as numeric (9) =0,
@vcCampaign AS VARCHAR(500)
AS  
BEGIN
	SET @vcOrginalURL = replace(replace(replace(replace(isnull(@vcOrginalURL,'-'),'%3A',':'),'%3F','?'),'%3D','='),'%26','&')                      
	SET @vcOrginalRef = replace(replace(replace(replace(isnull(@vcOrginalRef,'-'),'%3A',':'),'%3F','?'),'%3D','='),'%26','&')  
	SET @vcURL = replace(replace(replace(replace(isnull(@vcURL,'-'),'%3A',':'),'%3F','?'),'%3D','='),'%26','&')                      
	SET @vcReferrer = replace(replace(replace(replace(isnull(@vcReferrer,'-'),'%3A',':'),'%3F','?'),'%3D','='),'%26','&')
           
	DECLARE @numIdentity AS NUMERIC           
	DECLARE @vcTime AS VARCHAR(15)           

	/*Check for online campaign existance*/   
	DECLARE @numCampaignID NUMERIC(9)
	IF @numDivisionID > 0
	BEGIN
		SELECT TOP 1 
			@numCampaignID = [numCampaignId] 
		FROM 
			[CampaignMaster] 
		WHERE 
			 vcCampaignCode =@vcCampaign
			AND [numDomainID]=@numDomainId 
			AND [bitIsOnline] = 1
	END          
          
          


	SELECT 
		@IsVisited = COUNT(*) 
	FROM 
		TrackingVisitorsHDR
	WHERE 
		vcUserHostAddress=@vcUserHostAddress 
		AND dtCreated BETWEEN CONVERT(VARCHAR(20),GETUTCDATE(),101) AND CONVERT(VARCHAR(20),DATEADD(DAY,1,GETUTCDATE()),101) 
		AND numDomainID= @numDomainID


		IF @numDivisionID > 0
		BEGIN
			/*Check if previous visit had already created Division- if yes then update old record else create new record for TrackingVisitorsHDR */          
			IF (SELECT 
					COUNT(*) 
				FROM 
					TrackingVisitorsHDR
				WHERE 
					numDomainID= @numDomainID
					AND ISNULL([numDivisionID],0) = @numDivisionID) > 0
			BEGIN
				SET @IsVisited = 1
			END
		END
       
       
PRINT   @IsVisited
if @IsVisited=0          
begin
		declare @startPos as integer          
		declare @EndPos as integer          
		declare @vcSearchTerm as varchar(100)          
		          
		          
		          
		 SELECT @startPos=CHARINDEX( 'Referrer', @vcURL)          
		 if @startPos>0          
		 begin          
		  set @vcReferrer=substring(@vcURL,@startPos+9,500)          
				end          
		           
		 SELECT @startPos=CHARINDEX( '&q=', @vcReferrer)          
		 if @startPos>0          
		 begin          
		  set @vcSearchTerm=substring(@vcReferrer,@startPos+1,150)          
		            
		  SELECT @EndPos=CHARINDEX( '&', @vcSearchTerm)          
		  if @EndPos= 0 set @EndPos=100          
		  set @vcSearchTerm=substring(@vcSearchTerm,3,@EndPos-3)          
		           
		 end          
		 SELECT @startPos=CHARINDEX( '&p=', @vcReferrer)          
		 if @startPos>0          
		 begin          
		  set @vcSearchTerm=substring(@vcReferrer,@startPos+1,150)          
		            
		            
		  SELECT @EndPos=CHARINDEX( '&', @vcSearchTerm)          
		  if @EndPos= 0 set @EndPos=100          
		  set @vcSearchTerm=substring(@vcSearchTerm,3,@EndPos-3)          
		           
		 end           
		 SELECT @startPos=CHARINDEX( '?q=', @vcReferrer)          
		 if @startPos>0          
		 begin          
		  set @vcSearchTerm=substring(@vcReferrer,@startPos+1,150)          
		            
		  SELECT @EndPos=CHARINDEX( '&', @vcSearchTerm)          
		  if @EndPos= 0 set @EndPos=100          
		  set @vcSearchTerm=substring(@vcSearchTerm,3,@EndPos-3)          
		           
		 end          
		 SELECT @startPos=CHARINDEX( '?p=', @vcReferrer)          
		 if @startPos>0          
		 begin          
		  set @vcSearchTerm=substring(@vcReferrer,@startPos+1,150)          
		            
		            
		  SELECT @EndPos=CHARINDEX( '&', @vcSearchTerm)          
		  if @EndPos= 0 set @EndPos=100          
		  set @vcSearchTerm=substring(@vcSearchTerm,3,@EndPos-3)          
		           
		 end          
		          
		           
		 set @vcSearchTerm=replace(@vcSearchTerm,'+',' ')       
		      
		    
		if (@vcUserDomain like '%inktomisearch.com' or @vcUserDomain like '%ask.com'  or @vcUserDomain like  '%yahoo.net' or @vcUserDomain like  '%chello.nl' or @vcUserDomain like  '%linksmanager.com' or @vcUserDomain like  '%googlebot.com') set @vcCrawler='True'
  
   
   


         
          
          
          
			INSERT  INTO TrackingVisitorsHDR
					(
					  vcUserHostAddress,
					  vcUserDomain,
					  vcUserAgent,
					  vcBrowser,
					  vcCrawler,
					  vcURL,
					  vcReferrer,
					  vcSearchTerm,
					  numVisits,
					  vcOrginalURL,
					  vcOrginalRef,
					  numVistedPages,
					  vcTotalTime,
					  dtCreated,
					  vcCountry,
					  numDivisionID,
					  numDomainId,
					  [numCampaignID],
					  [vcDomainName]
					)
			VALUES  (
					  @vcUserHostAddress,
					  @vcUserDomain,
					  @vcUserAgent,
					  @vcBrowser,
					  @vcCrawler,
					  @vcURL,
					  @vcReferrer,
					  @vcSearchTerm,
					  @numVisits,
					  @vcOrginalURL,
					  @vcOrginalRef,
					  @numVistedPages,
					  @vcTotalTime,
					  GETUTCDATE(),
					  @vcCountry,
					  @numDivisionID,
					  @numDomainId,
					  @numCampaignID,
					  dbo.GetDomainNameFromURL(@vcOrginalURL)
					)           
			          
			select @numIdentity=@@identity   

	

                    
 end          
else          
begin          
          
	select top 1 @numIdentity=numTrackingID,@vcTime=vcTotalTime from TrackingVisitorsHDR where vcUserHostAddress=@vcUserHostAddress order by numTrackingID desc          
	declare @sec1 as integer          
	declare @min1 as integer           
	declare @hour1 as integer          
	declare @sec2 as integer          
	declare @min2 as integer          
	declare @hour2 as integer          
	set @sec1=substring(@vcTime,7,2)          
	set @min1=substring(@vcTime,4,2)          
	set @hour1=left(@vcTime,2)          
	set @sec2=substring(@vcTotalTime,7,2)          
	set @min2=substring(@vcTotalTime,4,2)          
	set @hour2=left(@vcTotalTime,2)          
	--print @sec1          
	--print @min1          
	--print @hour1          
	--print @sec2          
	--print @min2          
	--print @hour2          
	          
	set @sec1=@sec1+@sec2          
	 if @sec1>=60           
	 begin          
	  set @sec1=@sec1-60          
	  set @min1=@min1+1          
	 end          
	set @min1=@min1+@min2          
	 if @min1>=60           
	 begin          
	  set @min1=@min1-60          
	  set @hour1=@hour1+1           
	 end          
	set @hour1=@hour1+@hour2          
	--print @sec1          
	--print @min1          
	--print @hour1      
	if len(@hour1)=1 set @vcTotalTime='0'+convert(varchar,@hour1)+':'      
	else set @vcTotalTime=convert(varchar,@hour1)+':'      
	if len(@min1)=1  set @vcTotalTime=@vcTotalTime+'0'+convert(varchar,@min1)+':'      
	else set @vcTotalTime=@vcTotalTime+convert(varchar,@min1)+':'      
	      
	         
	if len(@sec1)=1 set @vcTotalTime=@vcTotalTime+'0'+convert(varchar,@sec1)      
	else set @vcTotalTime=@vcTotalTime+convert(varchar,@sec1)      
	         
	      
--set @vcTotalTime=convert(varchar,@hour1)+':'+convert(varchar,@min1)+':'+convert(varchar,@sec1)          
--print @vcTotalTime          
end          
          
                           
 DECLARE @hDoc int                            
 EXEC sp_xml_preparedocument @hDoc OUTPUT, @strURL                            
                            
 insert into TrackingVisitorsDTL                            
  (numTracVisitorsHDRID,vcPageName,vcElapsedTime,numPageID,dtTime)                            
                             
 select @numIdentity,X.URL,X.ElapsedTime,(select numPageID from PageMasterWebAnlys where X.URL like '%'+ vcPageURL+'%' ),X.[Time] from(                            
 SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table',2)                            
 WITH  (                            
  URL varchar(500),                            
  ElapsedTime varchar(100),          
  [Time] datetime                             
  ))X                   
  EXEC sp_xml_removedocument @hDoc                   
                          
update TrackingVisitorsHDR set vcTotalTime=@vcTotalTime,  numVistedPages=(select count(*) from TrackingVisitorsDTL where numTracVisitorsHDRID=@numIdentity)  where  numTrackingID= @numIdentity                      
        
if @numDivisionID>0         
begin        
  update TrackingVisitorsHDR set numDivisionID=@numDivisionID  where  numTrackingID= @numIdentity     
  
	DECLARE @vcTempOrigReferer VARCHAR(MAX)
	SELECT @vcTempOrigReferer=(CASE WHEN ISNULL(vcOrginalRef,'-') = '-' THEN ISNULL(vcOrginalURL,'-') ELSE vcOrginalRef END) FROM TrackingVisitorsHDR WHERE numTrackingID= @numIdentity

	DECLARE @numTempDomainID NUMERIC(18,0)
	SELECT @numTempDomainID=numDomainID FROM DivisionMaster WHERE numDivisionID=@numDivisionID

	IF EXISTS (SELECT CompanyInfo.numCompanyID FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=@numDivisionID AND ISNULL(vcHow,0) = 0)
	BEGIN
		IF EXISTS (SELECT numTrackingID FROM TrackingVisitorsHDR WHERE numDomainID=@numTempDomainID AND ISNULL(numListItemID,0) > 0 AND CHARINDEX(vcOrginalRef,@vcTempOrigReferer) > 0)
		BEGIN
			DECLARE @vcHow NUMERIC(18,0)
	
			SELECT TOP 1
				@vcHow=numListItemID 
			FROM 
				TrackingVisitorsHDR 
			WHERE 
				numDomainID=@numTempDomainID 
				AND ISNULL(numListItemID,0) > 0 
				AND @vcTempOrigReferer LIKE CONCAT(vcOrginalRef,'%')
			ORDER BY
				LEN(ISNULL(vcOrginalRef,'')) DESC

			UPDATE
				C
			SET
				vcHow=@vcHow
			FROM
				DivisionMaster D
			INNER JOIN 
				CompanyInfo C
			ON  
				D.numCompanyID=C.numCompanyId 
			WHERE 
				numDivisionID=@numDivisionID
		END
	END
END


/*In Order to increase performace for campaign report we need to insert filtered data into seperate table-chintan*/
IF @numCampaignID >0 AND @numIdentity>0 AND @numDivisionID>0
	BEGIN
	-- making TrackingCampaignData table obsolete .. by directly updating data on division
	UPDATE dbo.DivisionMaster SET numCampaignID = @numCampaignID WHERE numDivisionID = @numDivisionID

	END
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetRecords')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetRecords
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetRecords]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@numViewID NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@bitGroupByOrder BIT
	,@bitRemoveBO BIT
	,@numShippingZone NUMERIC(18,0)
	,@bitIncludeSearch BIT
	,@vcOrderStauts VARCHAR(MAX)
	,@tintFilterBy TINYINT
	,@vcFilterValue VARCHAR(MAX)
	,@vcCustomSearchValue VARCHAR(MAX)
	,@numBatchID NUMERIC(18,0)
	,@tintPackingViewMode TINYINT
	,@tintPrintBizDocViewMode TINYINT
	,@tintPendingCloseFilter TINYINT
	,@numPageIndex INT
	,@vcSortColumn VARCHAR(100) OUTPUT
	,@vcSortOrder VARCHAR(4) OUTPUT
	,@numTotalRecords NUMERIC(18,0) OUTPUT
)
AS 
BEGIN
	IF @numViewID = 4 OR @numViewID = 5 OR @numViewID = 6
	BEGIN
		SET @bitGroupByOrder = 1
	END

	SET @vcCustomSearchValue = REPLACE(@vcCustomSearchValue,'OpportunityMaster.dtItemReceivedDate','(SELECT TOP 1 OMInner.dtItemReceivedDate FROM OpportunityMaster OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numOppID=OIInner.numOppID WHERE OMInner.tintOppType=2 AND OMInner.tintOppStatus=1 AND OIInner.numItemCode=OpportunityItems.numItemCode ORDER BY OMInner.dtItemReceivedDate DESC)')

	DECLARE @TempFieldsLeft TABLE
	(
		numPKID INT IDENTITY(1,1)
		,ID VARCHAR(50)
		,numFieldID NUMERIC(18,0)
		,vcFieldName VARCHAR(100)
		,vcOrigDbColumnName VARCHAR(100)
		,vcListItemType VARCHAR(10)
		,numListID NUMERIC(18,0)
		,bitAllowSorting BIT
		,bitAllowFiltering BIT
		,vcAssociatedControlType VARCHAR(20)
		,vcLookBackTableName VARCHAR(100)
		,bitCustomField BIT
		,intRowNum INT
		,intColumnWidth FLOAT
		,bitIsRequired BIT
		,bitIsEmail BIT
		,bitIsAlphaNumeric BIT
		,bitIsNumeric BIT
		,bitIsLengthValidation BIT
		,intMaxLength INT
		,intMinLength INT
		,bitFieldMessage BIT
		,vcFieldMessage VARCHAR(MAX)
		,Grp_id INT
	)

	CREATE TABLE #TEMPResult
	(
		numOppID NUMERIC(18,0)
		,numDivisionID NUMERIC(18,0)
		,numTerID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numOppBizDocID NUMERIC(18,0)
		,bintCreatedDate VARCHAR(50)
		,numItemCode NUMERIC(18,0)
		,vcCompanyName VARCHAR(300)
		,vcPoppName VARCHAR(300)
		,numAssignedTo VARCHAR(100)
		,numRecOwner VARCHAR(100)
		,vcBizDocID VARCHAR(300)
		,numStatus VARCHAR(300)
		,txtComments VARCHAR(MAX)
		,numBarCodeId VARCHAR(50)
		,vcLocation VARCHAR(100)
		,vcItemName VARCHAR(300)
		,vcItemDesc VARCHAR(2000)
		,numQtyShipped FLOAT
		,numUnitHour FLOAT
		,monAmountPaid DECIMAL(20,5)
		,monAmountToPay DECIMAL(20,5)
		,numItemClassification VARCHAR(100)
		,vcSKU VARCHAR(50)
		,charItemType VARCHAR(50)
		,vcAttributes VARCHAR(300)
		,vcNotes VARCHAR(MAX)
		,vcItemReleaseDate DATE
		,dtItemReceivedDate VARCHAR(50)
		,intUsedShippingCompany VARCHAR(300)
		,intShippingCompany VARCHAR(300)
		,numPreferredShipVia NUMERIC(18,0)
		,numPreferredShipService NUMERIC(18,0)
		,vcInvoiced FLOAT
		,vcInclusionDetails VARCHAR(MAX)
		,bitPaidInFull BIT
		,numRemainingQty FLOAT
		,numAge VARCHAR(50)
		,dtAnticipatedDelivery  DATE
		,vcShipStatus VARCHAR(MAX)
		,dtExpectedDate VARCHAR(20)
		,dtExpectedDateOrig DATE
		,numQtyPicked FLOAT
		,numQtyPacked FLOAT
		,vcPaymentStatus VARCHAR(MAX)
		,numShipRate VARCHAR(40)
	)

	IF @bitGroupByOrder = 1
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName','OpportunityBizDocs.monAmountPaid','OpportunityBizDocs.vcBizDocID','OpportunityMaster.bintCreatedDate','OpportunityMaster.bitPaidInFull','OpportunityMaster.numAge','OpportunityMaster.numStatus','OpportunityMaster.vcPoppName','OpportunityMaster.dtItemReceivedDate','OpportunityMaster.numAssignedTo','OpportunityMaster.numRecOwner')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'DESC'
		END
	END
	ELSE 
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName'
								,'Item.charItemType'
								,'Item.numBarCodeId'
								,'Item.numItemClassification'
								,'Item.vcSKU'
								,'OpportunityBizDocs.monAmountPaid'
								,'OpportunityMaster.dtExpectedDate'
								,'OpportunityItems.numQtyPacked'
								,'OpportunityItems.numQtyPicked'
								,'OpportunityItems.numQtyShipped'
								,'OpportunityItems.numRemainingQty'
								,'OpportunityItems.numUnitHour'
								,'OpportunityItems.vcInvoiced'
								,'OpportunityItems.vcItemName'
								,'OpportunityItems.vcItemReleaseDate'
								,'OpportunityMaster.bintCreatedDate'
								,'OpportunityMaster.bitPaidInFull'
								,'OpportunityMaster.numAge'
								,'OpportunityMaster.numStatus'
								,'OpportunityMaster.vcPoppName'
								,'WareHouseItems.vcLocation'
								,'OpportunityMaster.dtItemReceivedDate'
								,'OpportunityBizDocs.vcBizDocID')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'DESC'
		END
	END

	DECLARE @tintPackingMode TINYINT
	DECLARE @tintInvoicingType TINYINT
	DECLARE @tintCommitAllocation TINYINT
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @numDefaultSalesShippingDoc NUMERIC(18,0)
	SELECT 
		@tintCommitAllocation=ISNULL(tintCommitAllocation,1) 
		,@numShippingServiceItemID=ISNULL(numShippingServiceItemID,0)
		,@numDefaultSalesShippingDoc=ISNULL(numDefaultSalesShippingDoc,0)
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID 

	IF @numViewID = 2 AND @tintPackingViewMode=3 AND @numDefaultSalesShippingDoc = 0
	BEGIN
		RAISERROR('DEFAULT_SHIPPING_BIZDOC_NOT_CONFIGURED',16,1)
		RETURN
	END

	SELECT
		@tintPackingMode=tintPackingMode
		,@tintInvoicingType=tintInvoicingType
	FROM
		MassSalesFulfillmentConfiguration
	WHERE
		numDomainID = @numDomainID 
		AND numUserCntID = @numUserCntID

	INSERT INTO @TempFieldsLeft
	(
		ID
		,numFieldID
		,vcFieldName
		,vcOrigDbColumnName
		,vcListItemType
		,numListID
		,bitAllowSorting
		,bitAllowFiltering
		,vcAssociatedControlType
		,vcLookBackTableName
		,bitCustomField
		,intRowNum
		,intColumnWidth
		,bitIsRequired
		,bitIsEmail
		,bitIsAlphaNumeric
		,bitIsNumeric
		,bitIsLengthValidation
		,intMaxLength
		,intMinLength
		,bitFieldMessage
		,vcFieldMessage
		,Grp_id
	)
	SELECT
		CONCAT(141,'~',DFM.numFieldID,'~',0)
		,DFM.numFieldID
		,(CASE WHEN @numViewID=6 AND DFM.numFieldID=248 THEN 'BizDoc' ELSE DFFM.vcFieldName END)
		,DFM.vcOrigDbColumnName
		,ISNULL(DFM.vcListItemType,'')
		,ISNULL(DFM.numListID,0)
		,DFFM.bitAllowSorting
		,DFFM.bitAllowFiltering
		,DFM.vcAssociatedControlType
		,DFM.vcLookBackTableName
		,0
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
		,0
		,0
		,0
		,0
		,0
		,0
		,0
		,0
		,''
		,0
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		DycFormField_Mapping DFFM
	ON
		DFCD.numFormId = DFFM.numFormID
		AND DFCD.numFieldId = DFFM.numFieldID
	INNER JOIN
		DycFieldMaster DFM
	ON
		DFFM.numFieldID = DFM.numFieldId
	WHERE 
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=141 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFFM.bitDeleted,0) = 0
		AND ISNULL(DFFM.bitSettingField,0) = 1
		AND ISNULL(DFCD.bitCustom,0) = 0
	UNION
	SELECT
		CONCAT(141,'~',CFM.Fld_id,'~',0)
		,CFM.Fld_id
		,CFM.Fld_label
		,CONCAT('CUST',CFM.Fld_id)
		,''
		,ISNULL(CFM.numListID,0)
		,0
		,0
		,CFM.Fld_type
		,''
		,1
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
		,ISNULL(bitIsRequired,0)
		,ISNULL(bitIsEmail,0)
		,ISNULL(bitIsAlphaNumeric,0)
		,ISNULL(bitIsNumeric,0)
		,ISNULL(bitIsLengthValidation,0)
		,ISNULL(intMaxLength,0)
		,ISNULL(intMinLength,0)
		,ISNULL(bitFieldMessage,0)
		,ISNULL(vcFieldMessage,'')
		,CFM.Grp_id
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		CFW_Fld_Master CFM
	ON
		DFCD.numFieldId = CFM.Fld_id
	LEFT JOIN
		CFW_Validation CV
	ON
		CFM.Fld_id = CV.numFieldID
	WHERE
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=141 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFCD.bitCustom,0) = 1

	IF (SELECT COUNT(*) FROM @TempFieldsLeft) = 0
	BEGIN
		INSERT INTO @TempFieldsLeft
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,vcListItemType
			,numListID
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,vcLookBackTableName
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(141,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,ISNULL(DFM.vcListItemType,'')
			,ISNULL(DFM.numListID,0)
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,DFM.vcLookBackTableName
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=141 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND ISNULL(DFFM.bitDefault,0) = 1
	END

	DECLARE @j INT = 0
	DECLARE @jCount INT
	DECLARE @numFieldId NUMERIC(18,0)
	DECLARE @vcFieldName AS VARCHAR(50)                                                                                                
	DECLARE @vcAssociatedControlType VARCHAR(10)  
	DECLARE @vcDbColumnName VARCHAR(100)
	DECLARE @Grp_id INT                                          
	--DECLARE @numListID AS NUMERIC(9)       
	DECLARE @bitCustomField BIT       
	SET @jCount = (SELECT COUNT(*) FROM @TempFieldsLeft)
	DECLARE @vcCustomFields VARCHAR(MAX) = ''
	DECLARE @vcCustomWhere VARCHAR(MAX) = ''

	WHILE @j <= @jCount
	BEGIN
		SELECT
			@numFieldId=numFieldID,
			@vcFieldName = vcFieldName,
			@vcAssociatedControlType = vcAssociatedControlType,
			@vcDbColumnName = CONCAT('Cust',numFieldID),
			@bitCustomField=ISNULL(bitCustomField,0),
			@Grp_id = ISNULL(Grp_id,0)
		FROM 
			@TempFieldsLeft
		WHERE 
			numPKID = @j     
		
		
			
		IF @bitCustomField = 1
		BEGIN         
			IF @Grp_id = 5
			BEGIN
				SET @vcCustomFields = CONCAT(@vcCustomFields,CONCAT(',dbo.GetCustFldValueOppItems(',@numFieldId,',T1.numOppItemID,T1.numItemCode)'),' [',@vcDbColumnName,']')
			END
			ELSE
			BEGIN
				IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea' 
				BEGIN
					SET @vcCustomFields = CONCAT(@vcCustomFields,',CFW',@numFieldId,'.Fld_Value  [',@vcDbColumnName,']')
             
					SET @vcCustomWhere = CONCAT(@vcCustomWhere,' left Join CFW_FLD_Values_Opp CFW',@numFieldId
									,' ON CFW' , @numFieldId , '.Fld_Id='
									,@numFieldId
									, ' and CFW' , @numFieldId
									, '.RecId=T1.numOppID')                                                         
				END   
				ELSE IF @vcAssociatedControlType = 'CheckBox' 
				BEGIN
					SET @vcCustomFields =CONCAT( @vcCustomFields
						, ',case when isnull(CFW'
						, @numFieldId
						, '.Fld_Value,0)=0 then 0 when isnull(CFW'
						, @numFieldId
						, '.Fld_Value,0)=1 then 1 end   ['
						,  @vcDbColumnName
						, ']')               
 
					SET @vcCustomWhere = CONCAT(@vcCustomWhere
						, ' left Join CFW_FLD_Values_Opp CFW'
						, @numFieldId
						, ' ON CFW',@numFieldId,'.Fld_Id='
						, @numFieldId
						, ' and CFW'
						, @numFieldId
						, '.RecId=T1.numOppID   ')                                                    
				END                
				ELSE IF @vcAssociatedControlType = 'DateField' 
				BEGIN
					SET @vcCustomFields = CONCAT(@vcCustomFields
						, ',dbo.FormatedDateFromDate(CFW'
						, @numFieldId
						, '.Fld_Value,'
						, @numDomainId
						, ')  [',@vcDbColumnName ,']' )  
					                  
					SET @vcCustomWhere = CONCAT(@vcCustomWhere
						, ' left Join CFW_FLD_Values_Opp CFW'
						, @numFieldId
						, ' on CFW', @numFieldId, '.Fld_Id='
						, @numFieldId
						, ' and CFW'
						, @numFieldId
						, '.RecId=T1.numOppID   ' )                                                        
				END                
				ELSE IF @vcAssociatedControlType = 'SelectBox' 
				BEGIN
					SET @vcCustomFields = CONCAT(@vcCustomFields,',L',@numFieldId,'.vcData',' [',@vcDbColumnName,']')
					                                                    
					SET @vcCustomWhere = CONCAT(@vcCustomWhere
						, ' left Join CFW_FLD_Values_Opp CFW'
						, @numFieldId
						, ' on CFW',@numFieldId ,'.Fld_Id='
						, @numFieldId
						, ' and CFW'
						, @numFieldId
						, '.RecId=T1.numOppID    ')     
					                                                    
					SET @vcCustomWhere = CONCAT(@vcCustomWhere
						, ' left Join ListDetails L'
						, @numFieldId
						, ' on L'
						, @numFieldId
						, '.numListItemID=CFW'
						, @numFieldId
						, '.Fld_Value' )              
				END
				ELSE
				BEGIN
					SET @vcCustomFields = CONCAT(@vcCustomFields,CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',T1.numOppID)'),' [',@vcDbColumnName,']')
				END 
			END
			
		END

		SET @j = @j + 1
	END

	CREATE TABLE #TEMPMSRecords
	(
		numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numOppBizDocID NUMERIC(18,0)
	)

	DECLARE @vcSQL NVARCHAR(MAX)
	DECLARE @vcSQLFinal NVARCHAR(MAX)
	
	SET @vcSQL = CONCAT('INSERT INTO #TEMPMSRecords
						(
							numOppID
							,numOppItemID
							,numOppBizDocID
						)
						SELECT
							OpportunityMaster.numOppId
							,',(CASE WHEN @bitGroupByOrder = 1 THEN '0' ELSE 'OpportunityItems.numoppitemtCode' END),'
							,',(CASE 
									WHEN @numViewID = 4 OR @numViewID = 6
									THEN 'OpportunityBizDocs.numOppBizDocsId'  
									ELSE '0' 
								END),'
						FROM
							OpportunityMaster
						INNER JOIN
							DivisionMaster
						ON
							OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
						INNER JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						INNER JOIN 
							OpportunityItems
						ON
							OpportunityMaster.numOppId = OpportunityItems.numOppId
						INNER JOIN
							Item
						ON
							OpportunityItems.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
						',
						(CASE 
							WHEN ISNULL(@numBatchID,0) > 0 
							THEN 
								' INNER JOIN 
										MassSalesFulfillmentBatchOrders
								ON
									OpportunityMaster.numOppId = MassSalesFulfillmentBatchOrders.numOppID
									AND (OpportunityItems.numOppItemtCode = MassSalesFulfillmentBatchOrders.numOppItemID OR ISNULL(MassSalesFulfillmentBatchOrders.numOppItemID,0) = 0)'
							ELSE 
								'' 
						END)	
						,
						(CASE 
							WHEN @numViewID = 4 
							THEN
								'INNER JOIN 
									OpportunityBizDocs
								ON
									OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
									AND OpportunityBizDocs.numBizDocID=287'
							WHEN @numViewID = 6
							THEN
								CONCAT('INNER JOIN 
											OpportunityBizDocs
										ON
											OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
											AND OpportunityBizDocs.numBizDocID=',(CASE @tintPrintBizDocViewMode WHEN 2 THEN 29397 WHEN 3 THEN 296 WHEN 4 THEN 287 ELSE 55206 END))
							ELSE ''

						END)
						,'
						WHERE
							OpportunityMaster.numDomainId=@numDomainID
							AND OpportunityMaster.tintOppType = 1
							AND OpportunityMaster.tintOppStatus = 1
							AND ISNULL(OpportunityMaster.tintshipped,0) = 0
							AND 1 = (CASE 
										WHEN @numViewID IN (1,2) AND ISNULL(@bitRemoveBO,0) = 1
										THEN (CASE 
												WHEN (Item.charItemType = ''p'' AND ISNULL(OpportunityItems.bitDropShip, 0) = 0 AND ISNULL(Item.bitAsset,0)=0)
												THEN 
													(CASE WHEN dbo.CheckOrderItemInventoryStatus(OpportunityItems.numItemCode,(CASE 
				WHEN @numViewID = 1
				THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 2
				THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) END)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 3
				THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL((SELECT 
																																											SUM(OpportunityBizDocItems.numUnitHour)
																																										FROM
																																											OpportunityBizDocs
																																										INNER JOIN
																																											OpportunityBizDocItems
																																										ON
																																											OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																										WHERE
																																											OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																											AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
					 																																					AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
				
				ELSE OpportunityItems.numUnitHour
			END),OpportunityItems.numoppitemtCode,OpportunityItems.numWarehouseItmsID,(CASE WHEN ISNULL(Item.bitKitParent,0) = 1 THEN 1 ELSE 0 END)) = 1 THEN 0 ELSE 1 END)
												ELSE 1
											END) 
										ELSE 1
									END)		
							AND 1 = (CASE WHEN LEN(ISNULL(@vcOrderStauts,'''')) > 0 THEN (CASE WHEN OpportunityMaster.numStatus ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) ELSE 1 END)
							AND 1 = (CASE 
										WHEN LEN(ISNULL(@vcFilterValue,'''')) > 0
										THEN
											CASE 
												WHEN @tintFilterBy=1 --Item Classification
												THEN (CASE WHEN Item.numItemClassification ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END)
												ELSE 1
											END
										ELSE 1
									END)
							AND 1 = (CASE
										WHEN @numViewID = 1 THEN (CASE WHEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) > 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 THEN (CASE 
																		WHEN @tintPackingViewMode = 1 
																		THEN (CASE 
																				WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=ISNULL(@numShippingServiceItemID,0)),0) = 0 
																				THEN 1 
																				ELSE 0 
																			END)
																		WHEN @tintPackingViewMode = 2
																		THEN (CASE 
																				WHEN ',(CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN 'ISNULL(OpportunityItems.numQtyPicked,0)' ELSE 'ISNULL(OpportunityItems.numUnitHour,0)' END),' - ISNULL(OpportunityItems.numQtyShipped,0) > 0
																				THEN 1 
																				ELSE 0 
																			END)
																		WHEN @tintPackingViewMode = 3
																		THEN (CASE 
																				WHEN ',(CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN 'ISNULL(OpportunityItems.numQtyPicked,0)' ELSE 'ISNULL(OpportunityItems.numUnitHour,0)' END),' - ISNULL((SELECT 
																																																													SUM(OpportunityBizDocItems.numUnitHour)
																																																												FROM
																																																													OpportunityBizDocs
																																																												INNER JOIN
																																																													OpportunityBizDocItems
																																																												ON
																																																													OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																																												WHERE
																																																													OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																																													AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
																																																													AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
																				THEN 1 
																				ELSE 0 
																			END)','
																		ELSE 0
																	END)
										WHEN @numViewID = 3
										THEN (CASE 
												WHEN (CASE 
														WHEN ISNULL(@tintInvoicingType,0)=1 
														THEN ISNULL(OpportunityItems.numQtyShipped,0) 
														ELSE ISNULL(OpportunityItems.numUnitHour,0) 
													END) - ISNULL((SELECT 
																		SUM(OpportunityBizDocItems.numUnitHour)
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																		AND OpportunityBizDocs.numBizDocId=287
																		AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0 
												THEN 1
												ELSE 0 
											END)
										WHEN @numViewID = 4 THEN 1
										WHEN @numViewID = 5 THEN 1
										WHEN @numViewID = 6 THEN 1
									END)
							AND 1 = (CASE
										WHEN @numViewID = 1 THEN (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 AND @tintPackingViewMode <> 2 THEN (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 AND @tintPackingViewMode = 2 THEN 1
										WHEN @numViewID = 3 THEN 1
										WHEN @numViewID = 4 THEN 1
										WHEN @numViewID = 5 THEN 1
										WHEN @numViewID = 6 THEN 1
										ELSE 0
									END)',
							(CASE 
								WHEN @numViewID = 5 AND ISNULL(@tintPendingCloseFilter,0) = 2
								THEN ' AND (SELECT 
												COUNT(*) 
											FROM 
											(
												SELECT
													OI.numoppitemtCode,
													ISNULL(OI.numUnitHour,0) AS OrderedQty,
													ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
												FROM
													OpportunityItems OI
												INNER JOIN
													Item I
												ON
													OI.numItemCode = I.numItemCode
												OUTER APPLY
												(
													SELECT
														SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
													FROM
														OpportunityBizDocs
													INNER JOIN
														OpportunityBizDocItems 
													ON
														OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
													WHERE
														OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
														AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
														AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
												) AS TempFulFilled
												WHERE
													OI.numOppID = OpportunityMaster.numOppID
													AND ISNULL(OI.numUnitHour,0) > 0
													AND ISNULL(OI.bitDropShip,0) = 0
											) X
											WHERE
												X.OrderedQty <> X.FulFilledQty) = 0
										AND (SELECT 
												COUNT(*) 
											FROM 
											(
												SELECT
													OI.numoppitemtCode,
													ISNULL(OI.numUnitHour,0) AS OrderedQty,
													ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
												FROM
													OpportunityItems OI
												INNER JOIN
													Item I
												ON
													OI.numItemCode = I.numItemCode
												OUTER APPLY
												(
													SELECT
														SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
													FROM
														OpportunityBizDocs
													INNER JOIN
														OpportunityBizDocItems 
													ON
														OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
													WHERE
														OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
														AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
														AND ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
												) AS TempInvoice
												WHERE
													OI.numOppID = OpportunityMaster.numOppID
													AND ISNULL(OI.numUnitHour,0) > 0
											) X
											WHERE
												X.OrderedQty > X.InvoicedQty) = 0'
								ELSE '' 
							END)
							,(CASE WHEN @numViewID = 4 THEN 'AND 1 = (CASE WHEN ISNULL(OpportunityBizDocs.monDealAmount,0) - ISNULL(OpportunityBizDocs.monAmountPaid,0) > 0 THEN 1 ELSE 0 END)' ELSE '' END)
							,' AND 1 = (CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 3 THEN (CASE WHEN ISNULL(Item.bitContainer,0)=1 THEN 0 ELSE 1 END) ELSE 1 END)
								AND 1 = (CASE WHEN ISNULL(@numWarehouseID,0) > 0 AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID THEN 1 ELSE 0 END) ELSE 1 END)'
								,(CASE WHEN ISNULL(@numShippingZone,0) > 0 THEN ' AND 1 = (CASE 
																				WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),0) > 0 
																				THEN
																					CASE 
																						WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=@numDomainId AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)) AND numShippingZone=@numShippingZone) > 0 
																						THEN 1
																						ELSE 0
																					END
																				ELSE 0 
																			END)' ELSE '' END),
								@vcCustomSearchValue,(CASE WHEN @bitGroupByOrder = 1 THEN CONCAT('GROUP BY OpportunityMaster.numOppId',(CASE WHEN @numViewID = 4 THEN ',OpportunityBizDocs.numOppBizDocsId,OpportunityBizDocs.monAmountPaid' WHEN @numViewID = 6 THEN ',OpportunityBizDocs.numOppBizDocsId'  ELSE '' END)) ELSE '' END));

	EXEC sp_executesql @vcSQL, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @tintPackingMode TINYINT, @bitRemoveBO BIT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @tintPackingMode, @bitRemoveBO;

	If @numViewID = 2 AND @tintPackingViewMode = 3
	BEGIN
		SET @vcSQL = CONCAT('INSERT INTO #TEMPMSRecords
						(
							numOppID
							,numOppItemID
							,numOppBizDocID
						)
						SELECT
							OpportunityMaster.numOppId
							,',(CASE WHEN @bitGroupByOrder = 1 THEN '0' ELSE 'OpportunityItems.numoppitemtCode' END),'
							,OpportunityBizDocs.numOppBizDocsId
						FROM
							OpportunityMaster
						INNER JOIN
							DivisionMaster
						ON
							OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
						INNER JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						INNER JOIN 
							OpportunityBizDocs
						ON
							OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
							AND OpportunityBizDocs.numBizDocID=@numDefaultSalesShippingDoc
						INNER JOIN 
							OpportunityItems
						ON
							OpportunityMaster.numOppId = OpportunityItems.numOppId
						INNER JOIN
							Item
						ON
							OpportunityItems.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
						
						',
						(CASE 
							WHEN ISNULL(@numBatchID,0) > 0 
							THEN 
								' INNER JOIN 
										MassSalesFulfillmentBatchOrders
								ON
									OpportunityMaster.numOppId = MassSalesFulfillmentBatchOrders.numOppID
									AND (OpportunityItems.numOppItemtCode = MassSalesFulfillmentBatchOrders.numOppItemID OR ISNULL(MassSalesFulfillmentBatchOrders.numOppItemID,0) = 0)'
							ELSE 
								'' 
						END)	
						,'
						WHERE
							OpportunityMaster.numDomainId=@numDomainID
							AND OpportunityMaster.tintOppType = 1
							AND OpportunityMaster.tintOppStatus = 1
							AND ISNULL(OpportunityMaster.tintshipped,0) = 0		
							AND 1 = (CASE WHEN LEN(ISNULL(@vcOrderStauts,'''')) > 0 THEN (CASE WHEN OpportunityMaster.numStatus ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) ELSE 1 END)
							AND 1 = (CASE 
										WHEN LEN(ISNULL(@vcFilterValue,'''')) > 0
										THEN
											CASE 
												WHEN @tintFilterBy=1 --Item Classification
												THEN (CASE WHEN Item.numItemClassification ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END)
												ELSE 1
											END
										ELSE 1
									END)
							AND 1 = (CASE 
										WHEN ISNULL((SELECT COUNT(*) FROM ShippingBox WHERE ISNULL(vcShippingLabelImage,'''') <> '''' AND ISNULL(vcTrackingNumber,'''') <> '''' AND numShippingReportId IN (SELECT numShippingReportId FROM ShippingReport WHERE ShippingReport.numOppID=OpportunityMaster.numOppId AND ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId)),0) = 0 
										THEN 1 
										ELSE 0 
									END)
										
							AND 1 = (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)',
							' AND 1 = (CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 3 THEN (CASE WHEN ISNULL(Item.bitContainer,0)=1 THEN 0 ELSE 1 END) ELSE 1 END)
								AND 1 = (CASE WHEN ISNULL(@numWarehouseID,0) > 0 AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID THEN 1 ELSE 0 END) ELSE 1 END)'
								,(CASE WHEN ISNULL(@numShippingZone,0) > 0 THEN ' AND 1 = (CASE 
																				WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),0) > 0 
																				THEN
																					CASE 
																						WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=@numDomainId AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)) AND numShippingZone=@numShippingZone) > 0 
																						THEN 1
																						ELSE 0
																					END
																				ELSE 0 
																			END)' ELSE '' END),
								@vcCustomSearchValue,(CASE WHEN @bitGroupByOrder = 1 THEN ' GROUP BY OpportunityMaster.numOppId,OpportunityBizDocs.numOppBizDocsId,OpportunityBizDocs.monAmountPaid' ELSE '' END));

		EXEC sp_executesql @vcSQL, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @tintPackingMode TINYINT, @bitRemoveBO BIT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @tintPackingMode, @bitRemoveBO;
	END

	SET @numTotalRecords = (SELECT COUNT(*) FROM #TEMPMSRecords)

	IF @bitGroupByOrder = 1
	BEGIN
		
		SET @vcSQLFinal = CONCAT('

		INSERT INTO #TEMPResult
		(
			numOppID
			,numDivisionID
			,numTerID
			,numOppItemID
			,numOppBizDocID
			,bintCreatedDate
			,vcCompanyName
			,vcPoppName
			,numAssignedTo
			,numRecOwner
			,vcBizDocID
			,numStatus
			,txtComments
			,monAmountPaid
			,monAmountToPay
			,intUsedShippingCompany
			,intShippingCompany
			,numPreferredShipVia
			,numPreferredShipService
			,bitPaidInFull
			,numAge
			,dtAnticipatedDelivery
			,vcShipStatus
			,dtExpectedDate
			,dtExpectedDateOrig
			,vcPaymentStatus
			,numShipRate
		)
		SELECT
			OpportunityMaster.numOppId
			,OpportunityMaster.numDivisionID
			,ISNULL(DivisionMaster.numTerID,0) numTerID
			,0
			,TEMPOrder.numOppBizDocID
			,(CASE WHEN @numViewID = 6 THEN dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityBizDocs.dtCreatedDate),@numDomainID) ELSE dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate),@numDomainID) END) 
			,ISNULL(CompanyInfo.vcCompanyName,'''')
			,ISNULL(OpportunityMaster.vcPoppName,'''')
			,dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)
			,dbo.fn_GetContactName(OpportunityMaster.numRecOwner)
			,(CASE WHEN @numViewID = 4 OR @numViewID = 6 THEN ISNULL(OpportunityBizDocs.vcBizDocID,''-'') ELSE '''' END)
			,dbo.GetListIemName(OpportunityMaster.numStatus)
			,ISNULL(OpportunityMaster.txtComments,'''')
			,ISNULL(TempPaid.monAmountPaid,0)
			,ISNULL(OpportunityBizDocs.monDealAmount,0) - ISNULL(OpportunityBizDocs.monAmountPaid,0)
			,CONCAT(ISNULL(LDOpp.vcData,''-''),'', '',ISNULL(SSOpp.vcShipmentService,''-'')) 
			,CONCAT(ISNULL(LDDiv.vcData,''-''),'', '',ISNULL(SSDiv.vcShipmentService,''-''))
			,ISNULL(DivisionMaster.intShippingCompany,0)
			,ISNULL(DivisionMaster.numDefaultShippingServiceID,0)
			,(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)
			,CONCAT(''<span style="color:'',(CASE
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 4  
						THEN ''#00ff00''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 4 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 8)
						THEN ''#00b050''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 8 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 12)
						THEN ''#3399ff''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 12 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 24)
						THEN ''#ff9900''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) BETWEEN 1 AND 2  
						THEN ''#9b3596''
						ELSE ''#ff0000''
					END),''">'',CONCAT(DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24,''d '',DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24,''h''),''</span>'')
			,'''' dtAnticipatedDelivery
			,'''' vcShipStatus
			,dbo.FormatedDateFromDate(OpportunityMaster.dtExpectedDate,@numDomainID)
			,OpportunityMaster.dtExpectedDate
			,'''' vcPaymentStatus
			,ISNULL(CAST((SELECT SUM(monTotAmount) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=@numShippingServiceItemID) AS VARCHAR),'''')
		FROM
			#TEMPMSRecords TEMPOrder
		INNER JOIN
			OpportunityMaster
		ON
			TEMPOrder.numOppID = OpportunityMaster.numOppId
		INNER JOIN
			DivisionMaster
		ON
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		LEFT JOIN
			OpportunityBizDocs
		ON
			OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
			AND OpportunityBizDocs.numOppBizDocsId = ISNULL(TEMPOrder.numOppBizDocID,0)
		LEFT JOIN
			ListDetails  LDOpp
		ON
			LDOpp.numListID = 82
			AND (LDOpp.numDomainID = @numDomainID OR LDOpp.constFlag=1)
			AND ISNULL(OpportunityMaster.intUsedShippingCompany,0) = LDOpp.numListItemID
		LEFT JOIN
			ShippingService AS SSOpp
		ON
			(SSOpp.numDomainID=@numDomainID OR ISNULL(SSOpp.numDomainID,0)=0) 
			AND OpportunityMaster.numShippingService = SSOpp.numShippingServiceID
		LEFT JOIN
			ListDetails  LDDiv
		ON
			LDDiv.numListID = 82
			AND (LDDiv.numDomainID = @numDomainID OR LDDiv.constFlag=1)
			AND ISNULL(DivisionMaster.intShippingCompany,0) = LDDiv.numListItemID
		LEFT JOIN
			ShippingService AS SSDiv
		ON
			(SSDiv.numDomainID=@numDomainID OR ISNULL(SSDiv.numDomainID,0)=0) 
			AND ISNULL(DivisionMaster.numDefaultShippingServiceID,0) = SSDiv.numShippingServiceID
		OUTER APPLY
		(
			SELECT 
				SUM(OpportunityBizDocs.monAmountPaid) monAmountPaid
			FROM 
				OpportunityBizDocs 
			WHERE 
				OpportunityBizDocs.numOppId=OpportunityMaster.numOppId 
				AND OpportunityBizDocs.numBizDocId=287
		) TempPaid
		ORDER BY ',
		(CASE @vcSortColumn
			WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName '
			WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(OpportunityMaster.numStatus)'
			WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName'
			WHEN 'OpportunityMaster.numAssignedTo' THEN 'dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)'
			WHEN 'OpportunityMaster.numRecOwner' THEN 'dbo.fn_GetContactName(OpportunityMaster.numRecOwner)'
			WHEN 'OpportunityMaster.numAge' THEN 'OpportunityMaster.bintCreatedDate'
			WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TempPaid.monAmountPaid'
			WHEN 'OpportunityBizDocs.vcBizDocID' THEN 'OpportunityBizDocs.vcBizDocID'
			WHEN 'OpportunityMaster.bitPaidInFull' THEN '(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)'
			ELSE (CASE WHEN @numViewID = 6 THEN 'OpportunityBizDocs.dtCreatedDate' ELSE 'OpportunityMaster.bintCreatedDate' END)
		END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'DESC' END),' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY')

		--PRINT CAST(@vcSQLFinal AS NTEXT)

		EXEC sp_executesql @vcSQLFinal, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @ClientTimeZoneOffset INT, @tintPackingMode TINYINT, @numPageIndex INT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @ClientTimeZoneOffset, @tintPackingMode, @numPageIndex;
	END
	ELSE
	BEGIN
		SET @vcSQLFinal = CONCAT('

		INSERT INTO #TEMPResult
		(
			numOppID
			,numDivisionID
			,numTerID
			,numOppItemID
			,numOppBizDocID
			,bintCreatedDate
			,numItemCode
			,vcCompanyName
			,vcPoppName
			,numAssignedTo
			,numRecOwner
			,vcBizDocID
			,numStatus
			,txtComments
			,numBarCodeId
			,vcLocation
			,vcItemName
			,vcItemDesc
			,numQtyShipped
			,numUnitHour
			,monAmountPaid
			,numItemClassification
			,vcSKU
			,charItemType
			,vcAttributes
			,vcNotes
			,vcItemReleaseDate
			,dtItemReceivedDate
			,intUsedShippingCompany
			,intShippingCompany
			,numPreferredShipVia
			,numPreferredShipService
			,vcInvoiced
			,vcInclusionDetails
			,bitPaidInFull
			,numAge
			,dtAnticipatedDelivery
			,vcShipStatus
			,numRemainingQty
			,dtExpectedDate
			,dtExpectedDateOrig
			,numQtyPicked
			,numQtyPacked
			,vcPaymentStatus
			,numShipRate
		)
		SELECT
			OpportunityMaster.numOppId
			,OpportunityMaster.numDivisionID
			,ISNULL(DivisionMaster.numTerID,0) numTerID
			,OpportunityItems.numoppitemtCode
			,TEMPOrder.numOppBizDocID
			,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate),@numDomainID)
			,Item.numItemCode
			,ISNULL(CompanyInfo.vcCompanyName,'''')
			,ISNULL(OpportunityMaster.vcPoppName,'''')
			,dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)
			,dbo.fn_GetContactName(OpportunityMaster.numRecOwner)
			,ISNULL(OpportunityBizDocs.vcBizDocID,'''')
			,dbo.GetListIemName(OpportunityMaster.numStatus)
			,ISNULL(OpportunityMaster.txtComments,'''')
			,ISNULL(Item.numBarCodeId,'''')
			,ISNULL(WarehouseLocation.vcLocation,'''')
			,ISNULL(Item.vcItemName,'''')
			,ISNULL(OpportunityItems.vcItemDesc,'''')
			,ISNULL(OpportunityItems.numQtyShipped,0)
			,ISNULL(OpportunityItems.numUnitHour,0)
			,ISNULL(TempPaid.monAmountPaid,0)
			,dbo.GetListIemName(Item.numItemClassification)
			,ISNULL(Item.vcSKU,'''')
			,(CASE 
				WHEN Item.charItemType=''P''  THEN ''Inventory Item''
				WHEN Item.charItemType=''N'' THEN ''Non Inventory Item'' 
				WHEN Item.charItemType=''S'' THEN ''Service'' 
				WHEN Item.charItemType=''A'' THEN ''Accessory'' 
			END)
			,ISNULL(OpportunityItems.vcAttributes,'''')
			,ISNULL(OpportunityItems.vcNotes,'''')
			,dbo.FormatedDateFromDate(OpportunityItems.ItemReleaseDate,@numDomainID)
			,dbo.FormatedDateFromDate((SELECT TOP 1 OMInner.dtItemReceivedDate FROM OpportunityMaster OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numOppID=OIInner.numOppID WHERE OMInner.tintOppType=2 AND OMInner.tintOppStatus=1 AND OIInner.numItemCode=OpportunityItems.numItemCode ORDER BY OMInner.dtItemReceivedDate DESC),@numDomainID)
			,CONCAT(ISNULL(LDOpp.vcData,''-''),'', '',ISNULL(SSOpp.vcShipmentService,''-'')) 
			,CONCAT(ISNULL(LDDiv.vcData,''-''),'', '',ISNULL(SSDiv.vcShipmentService,''-''))
			,ISNULL(DivisionMaster.intShippingCompany,0)
			,ISNULL(DivisionMaster.numDefaultShippingServiceID,0)
			,ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)
			,ISNULL(OpportunityItems.vcInclusionDetail,'''')
			,(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)
			,CONCAT(''<span style="color:'',(CASE
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 4  
						THEN ''#00ff00''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 4 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 8)
						THEN ''#00b050''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 8 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 12)
						THEN ''#3399ff''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 12 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 24)
						THEN ''#ff9900''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) BETWEEN 1 AND 2  
						THEN ''#9b3596''
						ELSE ''#ff0000''
					END),''">'',CONCAT(DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24,''d '',DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24,''h''),''</span>'')
			,'''' dtAnticipatedDelivery
			,'''' vcShipStatus
			,(CASE 
				WHEN @numViewID = 1
				THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 2
				THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) END)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 3 AND ISNULL(TEMPOrder.numOppBizDocID,0) = 0
				THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL((SELECT 
																																											SUM(OpportunityBizDocItems.numUnitHour)
																																										FROM
																																											OpportunityBizDocs
																																										INNER JOIN
																																											OpportunityBizDocItems
																																										ON
																																											OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																										WHERE
																																											OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																											AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
					 																																					AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 3 AND ISNULL(OpportunityBizDocs.numOppBizDocsId,0) > 0
				THEN ISNULL(OpportunityBizDocItems.numUnitHour,0)
				WHEN @numViewID = 3
				THEN (CASE 
						WHEN ISNULL(@tintInvoicingType,0)=1 
						THEN ISNULL(OpportunityItems.numQtyShipped,0) 
						ELSE ISNULL(OpportunityItems.numUnitHour,0) 
					END) - ISNULL((SELECT 
										SUM(OpportunityBizDocItems.numUnitHour)
									FROM
										OpportunityBizDocs
									INNER JOIN
										OpportunityBizDocItems
									ON
										OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
									WHERE
										OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
										AND OpportunityBizDocs.numBizDocId=287
										AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
				ELSE 0
			END) numRemainingQty
			,dbo.FormatedDateFromDate(OpportunityMaster.dtExpectedDate,@numDomainID)
			,OpportunityMaster.dtExpectedDate
			,ISNULL(OpportunityItems.numQtyPicked,0)
			,ISNULL(TempInvoiced.numInvoicedQty,0)
			,'''' vcPaymentStatus
			,ISNULL(CAST((SELECT SUM(monTotAmount) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=@numShippingServiceItemID) AS VARCHAR),'''')
		FROM
			#TEMPMSRecords TEMPOrder
		INNER JOIN
			OpportunityMaster
		ON
			TEMPOrder.numOppID = OpportunityMaster.numOppId
		INNER JOIN
			DivisionMaster
		ON
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		INNER JOIN 
			OpportunityItems
		ON
			OpportunityMaster.numOppId = OpportunityItems.numOppId
			AND TEMPOrder.numOppItemID = OpportunityItems.numoppitemtCode
		LEFT JOIN
			OpportunityBizDocs 
		ON
			TEMPOrder.numOppId = OpportunityBizDocs.numOppID
			AND TEMPOrder.numOppBizDocID = OpportunityBizDocs.numOppBizDocsId
		LEFT JOIN
			OpportunityBizDocItems
		ON
			OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocId
			AND OpportunityItems.numoppitemtCode = OpportunityBizDocItems.numOppItemID
		INNER JOIN
			Item
		ON
			OpportunityItems.numItemCode = Item.numItemCode
		LEFT JOIN
			ListDetails  LDOpp
		ON
			LDOpp.numListID = 82
			AND (LDOpp.numDomainID = @numDomainID OR LDOpp.constFlag=1)
			AND ISNULL(OpportunityMaster.intUsedShippingCompany,0) = LDOpp.numListItemID
		LEFT JOIN
			ShippingService AS SSOpp
		ON
			(SSOpp.numDomainID=@numDomainID OR ISNULL(SSOpp.numDomainID,0)=0) 
			AND OpportunityMaster.numShippingService = SSOpp.numShippingServiceID
		LEFT JOIN
			ListDetails  LDDiv
		ON
			LDDiv.numListID = 82
			AND (LDDiv.numDomainID = @numDomainID OR LDDiv.constFlag=1)
			AND ISNULL(DivisionMaster.intShippingCompany,0) = LDDiv.numListItemID
		LEFT JOIN
			ShippingService AS SSDiv
		ON
			(SSDiv.numDomainID=@numDomainID OR ISNULL(SSDiv.numDomainID,0)=0) 
			AND ISNULL(DivisionMaster.numDefaultShippingServiceID,0) = SSDiv.numShippingServiceID
		LEFT JOIN
			WareHouseItems
		ON
			OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
		LEFT JOIN
			WarehouseLocation
		ON
			WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
		OUTER APPLY
		(
			SELECT 
				SUM(OpportunityBizDocItems.numUnitHour) numInvoicedQty
			FROM 
				OpportunityBizDocs
			INNER JOIN
				OpportunityBizDocItems
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE 
				OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
				AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
				AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode
		) TempInvoiced
		OUTER APPLY
		(
			SELECT 
				SUM(OpportunityBizDocs.monAmountPaid) monAmountPaid
			FROM 
				OpportunityBizDocs 
			WHERE 
				OpportunityBizDocs.numOppId=OpportunityMaster.numOppId 
				AND OpportunityBizDocs.numBizDocId=287
		) TempPaid
		WHERE
			1 = (CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 3 THEN (CASE WHEN ISNULL(Item.bitContainer,0)=1 THEN 0 ELSE 1 END) ELSE 1 END)
			AND 1 = (CASE WHEN ISNULL(TEMPOrder.numOppBizDocID,0) > 0 THEN (CASE WHEN OpportunityBizDocItems.numOppItemID > 0 THEN 1 ELSE 0 END) ELSE 1 END)',
			(CASE WHEN @numViewID=2 AND @tintPackingViewMode=3 THEN ' AND 1 = (CASE WHEN ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 AND ISNULL(OpportunityItems.bitDropShip,0) = 0 THEN 1 ELSE 0 END)' ELSE '' END),'
		ORDER BY ',
		(CASE @vcSortColumn
			WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName' 
			WHEN 'Item.charItemType' THEN '(CASE 
												WHEN Item.charItemType=''P''  THEN ''Inventory Item''
												WHEN Item.charItemType=''N'' THEN ''Non Inventory Item'' 
												WHEN Item.charItemType=''S'' THEN ''Service''
												WHEN Item.charItemType=''A'' THEN ''Accessory''
											END)'
			WHEN 'Item.numBarCodeId' THEN 'Item.numBarCodeId'
			WHEN 'Item.numItemClassification' THEN 'dbo.GetListIemName(Item.numItemClassification)'
			WHEN 'Item.vcSKU' THEN 'Item.vcSKU'
			WHEN 'OpportunityItems.vcItemName' THEN 'Item.vcItemName'
			WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(OpportunityMaster.numStatus)'
			WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName'
			WHEN 'OpportunityMaster.numAssignedTo' THEN 'dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)'
			WHEN 'OpportunityMaster.numRecOwner' THEN 'dbo.fn_GetContactName(OpportunityMaster.numRecOwner)'
			WHEN 'WareHouseItems.vcLocation' THEN 'WarehouseLocation.vcLocation'
			WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TempPaid.monAmountPaid'
			WHEN 'OpportunityBizDocs.vcBizDocID' THEN 'OpportunityBizDocs.vcBizDocID'
			WHEN 'OpportunityItems.numQtyPacked' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)'
			WHEN 'OpportunityItems.numQtyPicked' THEN 'OpportunityItems.numQtyPicked'
			WHEN 'OpportunityItems.numQtyShipped' THEN 'OpportunityItems.numQtyShipped'
			WHEN 'OpportunityItems.numRemainingQty' THEN '(CASE 
																WHEN @numViewID = 1
																THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
																WHEN @numViewID = 2
																THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)
																ELSE 0
															END)'
			WHEN 'OpportunityItems.numUnitHour' THEN 'OpportunityItems.numUnitHour'
			WHEN 'OpportunityItems.vcInvoiced' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)'
			WHEN 'OpportunityMaster.dtExpectedDate' THEN 'OpportunityMaster.dtExpectedDate'
			WHEN 'OpportunityItems.vcItemReleaseDate' THEN 'OpportunityItems.ItemReleaseDate'
			WHEN 'OpportunityMaster.bintCreatedDate' THEN 'OpportunityMaster.bintCreatedDate'
			WHEN 'OpportunityMaster.numAge' THEN 'OpportunityMaster.bintCreatedDate'
			WHEN 'OpportunityMaster.bitPaidInFull' THEN '(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)'
			WHEN 'OpportunityMaster.dtItemReceivedDate' THEN '(SELECT TOP 1 OMInner.dtItemReceivedDate FROM OpportunityMaster OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numOppID=OIInner.numOppID WHERE OMInner.tintOppType=2 AND OMInner.tintOppStatus=1 AND OIInner.numItemCode=OpportunityItems.numItemCode ORDER BY OMInner.dtItemReceivedDate)'
			ELSE 'OpportunityMaster.bintCreatedDate'
		END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'ASC' END),' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY')

		--PRINT CAST(@vcSQLFinal AS NTEXT)

		EXEC sp_executesql @vcSQLFinal, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @ClientTimeZoneOffset INT, @tintPackingMode TINYINT, @numPageIndex INT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @ClientTimeZoneOffset, @tintPackingMode, @numPageIndex;
	END

	SET @vcSQLFinal = CONCAT('SELECT T1.*',@vcCustomFields,' FROM #TEMPResult T1',@vcCustomWhere)

	EXEC sp_executesql @vcSQLFinal

	SELECT * FROM @TempFieldsLeft ORDER BY intRowNum

	IF OBJECT_ID('tempdb..#TEMPMSRecords') IS NOT NULL DROP TABLE #TEMPMSRecords
	IF OBJECT_ID('tempdb..#TEMPResult') IS NOT NULL DROP TABLE #TEMPResult
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetShippingBizDocForPacking')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetShippingBizDocForPacking
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetShippingBizDocForPacking]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@numOppBizDocID NUMERIC(18,0)
	,@numOppItemID NUMERIC(18,0)
)
AS
BEGIN
	IF EXISTS (SELECT numShippingReportId FROM ShippingReport WHERE numOppID=@numOppID AND numOppBizDocId=@numOppBizDocID)
	BEGIN
		DECLARE @numShippingReportID NUMERIC(18,0)
		DECLARE @vcPOppName VARCHAR(300)

		SELECT TOP 1  
			@numShippingReportID = numShippingReportId
			,@vcPOppName=OpportunityMaster.vcPOppName
		FROM 
			ShippingReport 
		INNER JOIN
			OpportunityMaster
		ON
			ShippingReport.numOppID = OpportunityMaster.numOppId
		WHERE 
			ShippingReport.numOppID=@numOppID 
			AND ShippingReport.numOppBizDocId=@numOppBizDocID


		SELECT 
			@numShippingReportID AS numShippingReportID
			,@vcPOppName vcPOppName
			,numBoxID
			,numShippingReportId
			,vcBoxName
			,fltTotalWeight
			,fltHeight
			,fltWidth
			,fltLength 
		FROM 
			ShippingBox 
		WHERE 
			numShippingReportId = @numShippingReportID


		SELECT 
			ShippingReportItems.numBoxID
			,ShippingReportItems.numOppBizDocItemID
			,Item.numItemCode
			,Item.vcItemName
			,ShippingReportItems.intBoxQty 
		FROM 
			ShippingReportItems 
		INNER JOIN 
			Item 
		ON 
			ShippingReportItems.numItemCode=Item.numItemCode 
		WHERE 
			numShippingReportId = @numShippingReportID
	END
	ELSE
	BEGIN
		SELECT 
			OM.numOppID
			,ISNULL(OM.intUsedShippingCompany,ISNULL(DM.intShippingCompany,0)) numShipVia
			,ISNULL(OM.numShippingService,ISNULL(DM.numDefaultShippingServiceID,0)) numShipService
			,OBD.numOppBizDocsId
			,OBDI.numOppBizDocItemID
			,OM.vcPOppName
			,I.numContainer
			,ISNULL(IContainer.vcItemName,'') vcContainer
			,(CASE WHEN ISNULL(I.numContainer,0) > 0 THEN I.numNoItemIntoContainer ELSE OI.numUnitHour END) AS numNoItemIntoContainer
			,ISNULL(IContainer.fltWeight,0) fltContainerWeight
			,ISNULL(IContainer.fltHeight,0) fltContainerHeight
			,ISNULL(IContainer.fltWidth,0) fltContainerWidth
			,ISNULL(IContainer.fltLength,0) fltContainerLength
			,OI.numoppitemtCode
			,I.numItemCode
			,I.vcItemName
			,ISNULL(I.fltWeight,0) fltItemWeight
			,ISNULL(I.fltHeight,0) fltItemHeight
			,ISNULL(I.fltWidth,0) fltItemWidth
			,ISNULL(I.fltLength,0) fltItemLength
			,ISNULL(OBDI.numUnitHour,0) numTotalQty
		FROM 
			OpportunityBizDocs OBD
		INNER JOIN
			OpportunityBizDocItems OBDI
		ON
			OBD.numOppBizDocsId = OBDI.numOppBizDocID
		INNER JOIN
			OpportunityMaster OM
		ON
			OBD.numOppId = OM.numOppId
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
			AND OBDI.numOppItemID = OI.numoppitemtCode
		INNER JOIN 
			Item AS I
		ON 
			OI.numItemCode = I.numItemCode
		LEFT JOIN
			Item AS IContainer
		ON
			I.numContainer = IContainer.numItemCode
		WHERE
			OM.numOppId=@numOppID
			AND OBD.numOppBizDocsId = @numOppBizDocID
			AND ISNULL(I.bitContainer,0) = 0
			AND (OI.numoppitemtCode=@numOppItemID OR ISNULL(@numOppItemID,0) = 0)
	END	
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_UpdateItemsPickedQty')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_UpdateItemsPickedQty
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_UpdateItemsPickedQty]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@vcSelectedRecords VARCHAR(MAX)
)
AS
BEGIN
	DECLARE @numStatus NUMERIC(18,0) = 0
	IF EXISTS (SELECT ID FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID)
	BEGIN
		SELECT @numStatus = ISNULL(numOrderStatusPicked,0) FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID
	END

	DECLARE @hDocItem INT

	DECLARE @Temp TABLE
	(
		ID INT IDENTITY(1,1)
		,numPickedQTY FLOAT
		,vcOppItems VARCHAR(MAX)
	)

	EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcSelectedRecords

	INSERT INTO @Temp
	(
		numPickedQty,
		vcOppItems
	)
	SELECT
		numPickedQty,
		vcOppItems
	FROM
		OPENXML (@hDocItem,'/NewDataSet/Table1',2)
	WITH
	(
		numPickedQty FLOAT,
		vcOppItems VARCHAR(MAX)
	)

	EXEC sp_xml_removedocument @hDocItem 

	DECLARE @TEMPItems TABLE
	(
		ID INT
		,numOppItemID NUMERIC(18,0)
		,numQtyLeftToPick FLOAT
	)


	DECLARE @i INT = 1
	DECLARE @iCount INT
	DECLARE @j INT = 1
	DECLARE @jCount INT
	DECLARE @numPickedQty FLOAT
	DECLARE @numOppItemID NUMERIC(18,0)
	DECLARE @numQtyLeftToPick FLOAT

	DECLARE @vcOppItems VARCHAR(MAX)
	SET @iCount = (SELECT COUNT(*) FROM @TEMP)

	BEGIN TRY
	BEGIN TRANSACTION

		WHILE @i <= @iCount
		BEGIN
			SELECT
				@numPickedQty=ISNULL(numPickedQty,0)
				,@vcOppItems=ISNULL(vcOppItems,'')
			FROM 
				@TEMP 
			WHERE 
				ID=@i

		
			DELETE FROM @TEMPItems

			INSERT INTO @TEMPItems
			(
				ID
				,numOppItemID
				,numQtyLeftToPick
			)
			SELECT 
				ROW_NUMBER() OVER(ORDER BY numOppItemID ASC)
				,TEMP.numOppItemID
				,ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
			FROM
			(
				SELECT
					CAST(SUBSTRING(OutParam,0,CHARINDEX('(',OutParam)) AS NUMERIC) numOppItemID
				FROM
					dbo.SplitString(@vcOppItems,',')
			) TEMP
			INNER JOIN
				OpportunityItems
			ON
				Temp.numOppItemID = OpportunityItems.numoppitemtCode


			SET @j = 1
			SET @jCount = (SELECT COUNT(*) FROM @TEMPItems)

			WHILE @j <= @jCount
			BEGIN
				SELECT 
					@numOppItemID=numOppItemID
					,@numQtyLeftToPick=numQtyLeftToPick
				FROM
					@TEMPItems
				WHERE
					ID=@j

				UPDATE
					OpportunityItems
				SET 
					numQtyPicked = ISNULL(numQtyPicked,0) + (CASE WHEN @numPickedQty > @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numPickedQty END)
				WHERE 
					numoppitemtCode=@numOppItemID

				SET @numPickedQty = @numPickedQty - (CASE WHEN @numPickedQty > @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numPickedQty END)


				IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numoppitemtCode=@numOppItemID AND ISNULL(numQtyPicked,0) > ISNULL(numUnitHour,0))
				BEGIN
					RAISERROR('PICKED_QTY_IS_GREATER_THEN_REMAINING_QTY',16,1)
				END

				IF @numPickedQty <= 0
				BEGIN
					BREAK
				END

				SET @j = @j + 1
			END

			IF @numPickedQty > 0
			BEGIN
				RAISERROR('PICKED_QTY_IS_GREATER_THEN_REMAINING_QTY',16,1)
			END

			SET @i = @i + 1
		END

		IF ISNULL(@numStatus,0) > 0
		BEGIN
			UPDATE
				OpportunityMaster
			SET
				numStatus=@numStatus
			WHERE
				numDomainId=@numDomainID
				AND numOppId IN (SELECT DISTINCT OI.numOppId FROM @TEMPItems T1 INNER JOIN OpportunityItems OI ON T1.numOppItemID = OI.numoppitemtCode)
				AND NOT EXISTS (SELECT
									numOppItemtcode
								FROM
									OpportunityItems
								WHERE
									numOppId = OpportunityMaster.numOppId
									AND ISNULL(numWarehouseItmsID,0) > 0
									AND ISNULL(bitDropShip,0) = 0
									AND ISNULL(numUnitHour,0) <> ISNULL(numQtyPicked,0))
		END
	COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorNumber INT
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT
		DECLARE @ErrorLine INT
		DECLARE @ErrorProcedure NVARCHAR(200)

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorNumber = ERROR_NUMBER(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@ErrorLine = ERROR_LINE(),
			@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

		RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount DECIMAL(20,5) =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(18,0),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0,
  @numShipmentMethod NUMERIC(18,0)=0,
  @dtReleaseDate DATE = NULL,
  @numPartner NUMERIC(18,0)=0,
  @tintClickBtn INT=0,
  @numPartenerContactId NUMERIC(18,0)=0,
  @numAccountClass NUMERIC(18,0) = 0,
  @numWillCallWarehouseID NUMERIC(18,0) = 0,
  @numVendorAddressID NUMERIC(18,0) = 0,
  @numShipFromWarehouse NUMERIC(18,0) = 0,
  @numShippingService NUMERIC(18,0) = 0,
  @ClientTimeZoneOffset INT = 0,
  @vcCustomerPO VARCHAR(100)=NULL,
  @bitDropShipAddress BIT = 0,
  @PromCouponCode AS VARCHAR(100) = NULL,
  @numPromotionId AS NUMERIC(18,0) = 0,
  @dtExpectedDate AS DATETIME = null
  -- USE PARAMETER WITH OPTIONAL VALUE BECAUSE PROCEDURE IS CALLED FROM OTHER PROCEDURE WHICH WILL NOT PASS NEW PARAMETER VALUE
)                                                                          
                                                                          
as  
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @fltExchangeRate float                                 
	DECLARE @hDocItem int                                                                                                                            
	DECLARE @TotalAmount as DECIMAL(20,5)                                                                          
	DECLARE @tintOppStatus as tinyint
	DECLARE @bitNewOrder AS BIT = (CASE WHEN ISNULL(@numOppID,0) = 0 THEN 1 ELSE 0 END)
	
	IF ISNULL(@numContactId,0) = 0
	BEGIN
		RAISERROR('CONCAT_REQUIRED',16,1)
		RETURN
	END  
	
	IF ISNULL(@tintOppType,0) = 0
	BEGIN
		RAISERROR('OPP_TYPE_CAN_NOT_BE_0',16,1)
		RETURN
	END             

	IF ISNULL(@numOppID,0) > 0 AND EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numDomainId=@numDomainId AND numOppId=@numOppID AND ISNULL(tintshipped,0) = 1) 
	BEGIN	 
		RAISERROR('OPP/ORDER_IS_CLOSED',16,1)
		RETURN
	END

	DECLARE @dtItemRelease AS DATE
	SET @dtItemRelease = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())

	DECLARE @numCompany AS NUMERIC(18)
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)
	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainId

	SELECT 
		@numCompany = D.numCompanyID
		,@bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0)
		,@numRelationship=numCompanyType
		,@numProfile=vcProfile
	FROM 
		DivisionMaster D
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID
	WHERE 
		D.numDivisionID = @numDivisionId

	IF @numOppID = 0 AND CONVERT(VARCHAR(10),@strItems) <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

		--VALIDATE PROMOTION OF COUPON CODE
		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1),
			numOppItemID NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numOppItemID,
			numPromotionID
		)
		SELECT
			numoppitemtCode,
			numPromotionID
		FROM
			OPENXML (@hDocItem,'/NewDataSet/Item',2)
		WITH
		(
			numoppitemtCode NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)
		WHERE
			numPromotionID > 0

		EXEC sp_xml_removedocument @hDocItem 

		IF (SELECT COUNT(*) FROM @TEMP) > 0
		BEGIN
			-- IF ITEM PROMOTION USED IN ORDER EXIPIRES THAN RAISE ERROR
			IF  (SELECT 
						COUNT(PO.numProId)
					FROM 
						PromotionOffer PO
					LEFT JOIN
						PromotionOffer POOrder
					ON
						PO.numOrderPromotionID = POOrder.numProId
					INNER JOIN
						@TEMP T1
					ON
						PO.numProId = T1.numPromotionID
					WHERE 
						PO.numDomainId=@numDomainID 
						AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
						AND ISNULL(PO.bitEnabled,0) = 1
						AND 1 = (CASE 
									WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
									THEN (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
									ELSE (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
								END)
						AND 1 = (CASE 
									WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
									ELSE
										(CASE PO.tintCustomersBasedOn 
											WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
											WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
											WHEN 3 THEN 1
											ELSE 0
										END)
								END)
				) <> (SELECT COUNT(*) FROM @TEMP)
			BEGIN
				RAISERROR('ITEM_PROMOTION_EXPIRED',16,1)
				RETURN
			END

			-- NOW IF COUPON BASED ITEM PROMOTION IS USED THEN VALIDATE COUPON AND ITS USAGE
			IF 
			(
				SELECT 
					COUNT(*)
				FROM
					PromotionOffer PO
				INNER JOIN
					@TEMP T1
				ON
					PO.numProId = T1.numPromotionID
				WHERE
					PO.numDomainId = @numDomainId
					AND ISNULL(IsOrderBasedPromotion,0) = 0
					AND ISNULL(numOrderPromotionID,0) > 0 
			) = 1
			BEGIN
				IF ISNULL(@PromCouponCode,0) <> ''
				BEGIN
					IF (SELECT 
							COUNT(*)
						FROM
							PromotionOffer PO
						INNER JOIN 
							PromotionOffer POOrder
						ON
							PO.numOrderPromotionID = POOrder.numProId
						INNER JOIN
							@TEMP T1
						ON
							PO.numProId = T1.numPromotionID
						INNER JOIN
							DiscountCodes DC
						ON
							POOrder.numProId = DC.numPromotionID
						WHERE
							PO.numDomainId = @numDomainId
							AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
							AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode) = 0
					BEGIN
						RAISERROR('INVALID_COUPON_CODE_ITEM_PROMOTION',16,1)
						RETURN
					END
					ELSE
					BEGIN
						-- IF THERE IS COUPON CODE USAGE LIMIT THAN VALIDATE USAGE LIMIT
						IF (SELECT 
								COUNT(*)
							FROM
								PromotionOffer PO
							INNER JOIN 
								PromotionOffer POOrder
							ON
								PO.numOrderPromotionID = POOrder.numProId
							INNER JOIN
								@TEMP T1
							ON
								PO.numProId = T1.numPromotionID
							INNER JOIN
								DiscountCodes DC
							ON
								POOrder.numProId = DC.numPromotionID
							WHERE
								PO.numDomainId = @numDomainId
								AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
								AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode
								AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN 1 ELSE 0 END)
								AND ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE DiscountCodeUsage.numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit) > 0
						BEGIN
							RAISERROR('ITEM_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED',16,1)
							RETURN
						END
					END
				END
				ELSE
				BEGIN
					RAISERROR('COUPON_CODE_REQUIRED_ITEM_PROMOTION',16,1)
					RETURN
				END
			END
		END
	END

	--IF NEW OPPERTUNITY
	IF @numOppID = 0                                                                          
	BEGIN
		DECLARE @bitIsInitalSalesOrder BIT
		IF(@DealStatus=1 AND @tintOppType=1)
		BEGIN
			SET @bitIsInitalSalesOrder=1
		END

		DECLARE @intOppTcode AS NUMERIC(9)  
  
		SELECT @intOppTcode = MAX(numOppId) FROM OpportunityMaster                

		SET @intOppTcode = ISNULL(@intOppTcode,0) + 1                                                
		
		SET @vcPOppName = @vcPOppName + '-'+ CONVERT(varchar(4),YEAR(GETUTCDATE()))  +'-A'+ CONVERT(varchar(10),@intOppTcode)                                  
  
		IF ISNULL(@numCurrencyID,0) = 0 
		BEGIN
			SET @fltExchangeRate=1
			SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
		END
		ELSE
		BEGIN 
			SET @fltExchangeRate = dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
        END  
		                                                        
		IF ISNULL(@numAccountClass,0) = 0
		BEGIN                                                  
		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivisionID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
		END

        IF(@DealStatus=1 AND @tintOppType=1 AND @tintClickBtn=1)
		BEGIN
			SET @DealStatus=0
		END         
		--PRINT ('Deal Staus ' +@DealStatus)  

		-------- VALIDATE ORDER BASED PROMOTION -------------

		DECLARE @numDiscountID NUMERIC(18,0) = NULL
		IF @numPromotionId > 0 
		BEGIN
			-- VALIDTE IF ITS ORDER BASED PROMOTION (BOTH ITEM AND ORDER BASED PROMOTION CAN HAVE COUPON CODE)
			IF EXISTS (SELECT numProId FROM PromotionOffer WHERE numDomainId=@numDomainId AND numProId=@numPromotionId AND ISNULL(IsOrderBasedPromotion,0) = 1)
			BEGIN
				-- CHECK IF ORDER PROMOTION IS STILL VALID
				IF NOT EXISTS (SELECT 
									PO.numProId
								FROM 
									PromotionOffer PO
								INNER JOIN 
									PromotionOfferOrganizations PORG
								ON 
									PO.numProId = PORG.numProId
								WHERE 
									numDomainId=@numDomainID 
									AND PO.numProId=@numPromotionId
									AND ISNULL(IsOrderBasedPromotion,0) = 1
									AND ISNULL(bitEnabled,0) = 1
									AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=dtValidFrom AND GETUTCDATE()<=dtValidTo THEN 1 ELSE 0 END) END)
									AND numRelationship=@numRelationship 
									AND numProfile=@numProfile
				)
				BEGIN
					RAISERROR('ORDER_PROMOTION_EXPIRED',16,1)
					RETURN
				END
				ELSE IF ISNULL((SELECT ISNULL(bitRequireCouponCode,0) FROM PromotionOffer WHERE numDomainId=@numDomainId AND numProId=@numPromotionId),0) = 1
				BEGIN
					IF ISNULL(@PromCouponCode,0) <> ''
					BEGIN
						IF (SELECT 
								COUNT(*)
							FROM
								PromotionOffer PO
							INNER JOIN
								DiscountCodes DC
							ON
								PO.numProId = DC.numPromotionID
							WHERE
								PO.numDomainId = @numDomainId
								AND PO.numProId = @numPromotionId
								AND ISNULL(IsOrderBasedPromotion,0) = 1
								AND ISNULL(bitRequireCouponCode,0) = 1
								AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode) = 0
						BEGIN
							RAISERROR('INVALID_COUPON_CODE_ORDER_PROMOTION',16,1)
							RETURN
						END
						ELSE
						BEGIN
							-- IF THERE IS COUPON CODE USAGE LIMIT THAN VALIDATE USAGE LIMIT
							IF (SELECT 
									COUNT(*)
								FROM
									PromotionOffer PO
								INNER JOIN
									DiscountCodes DC
								ON
									PO.numProId = DC.numPromotionID
								WHERE
									PO.numDomainId = @numDomainId
									AND PO.numProId = @numPromotionId
									AND ISNULL(IsOrderBasedPromotion,0) = 1
									AND ISNULL(bitRequireCouponCode,0) = 1
									AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode
									AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN 1 ELSE 0 END)
									AND ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE DiscountCodeUsage.numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit) > 0
							BEGIN
								RAISERROR('ORDER_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED',16,1)
								RETURN
							END
							ELSE
							BEGIN			
								SELECT
									@numDiscountID=numDiscountId
								FROM
									DiscountCodes DC
								WHERE
									DC.numPromotionID=@numPromotionId
									AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode
									
								IF EXISTS (SELECT DC.numDiscountID FROM DiscountCodes DC INNER JOIN DiscountCodeUsage DCU ON DC.numDiscountID=DCU.numDIscountID WHERE DC.numPromotionID=@numPromotionId AND DCU.numDivisionID=@numDivisionID AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode)
								BEGIN
									UPDATE
										DCU
									SET
										DCU.intCodeUsed = ISNULL(DCU.intCodeUsed,0) + 1
									FROM 
										DiscountCodes DC 
									INNER JOIN 
										DiscountCodeUsage DCU 
									ON 
										DC.numDiscountID=DCU.numDIscountID 
									WHERE 
										DC.numPromotionID=@numPromotionId 
										AND DCU.numDivisionID=@numDivisionID 
										AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode
								END
								ELSE
								BEGIN
									INSERT INTO DiscountCodeUsage
									(
										numDiscountId
										,numDivisionId
										,intCodeUsed
									)
									SELECT 
										DC.numDiscountId
										,@numDivisionId
										,1
									FROM
										DiscountCodes DC
									WHERE
										DC.numPromotionID=@numPromotionId
										AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode
								END
							END
						END
					END
					ELSE
					BEGIN
						RAISERROR('COUPON_CODE_REQUIRED_ORDER_PROMOTION',16,1)
						RETURN
					END
				END
			END
		END

		-------- ORDER BASED Promotion Logic-------------
		                                                    
		INSERT INTO OpportunityMaster
		(                                                                             
			numContactId,numDivisionId,txtComments,numCampainID,bitPublicFlag,tintSource,tintSourceType,vcPOppName,                                                                          
			intPEstimatedCloseDate,monPAmount,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,                                                                                                             
			numRecOwner,lngPConclAnalysis,tintOppType,numSalesOrPurType,numCurrencyID,fltExchangeRate,numAssignedTo,
			numAssignedBy,[tintOppStatus],numStatus,vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,
			bitInterestType,fltInterest,vcCouponCode,bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,
			vcMarketplaceOrderReportId,numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
			bitUseMarkupShippingRate,numMarkupShippingRate,intUsedShippingCompany,numShipmentMethod,numShippingService,numPartner,numPartenerContact,dtReleaseDate,bitIsInitalSalesOrder,numVendorAddressID,numShipFromWarehouse
			,vcCustomerPO#,bitDropShipAddress,numDiscountID,dtExpectedDate
		)                                                                          
		Values                                                                          
		(                                                                          
			@numContactId,@numDivisionId,ISNULL(@Comments,''),@CampaignID,@bitPublicFlag,@tintSource,@tintSourceType,@vcPOppName,                                                                          
			@dtEstimatedCloseDate,@monPAmount,@numUserCntID,(CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END),                                                                          
			@numUserCntID,GETUTCDATE(),@numDomainId,@numUserCntID,@lngPConclAnalysis,@tintOppType,@numSalesOrPurType,@numCurrencyID, 
			@fltExchangeRate,(CASE WHEN @numAssignedTo > 0 THEN @numAssignedTo ELSE NULL END),(CASE WHEN @numAssignedTo>0 THEN @numUserCntID ELSE NULL END),
			@DealStatus,@numStatus,@vcOppRefOrderNo,@bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,
			@PromCouponCode,@bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,@numPercentageComplete,
			@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,@bitUseMarkupShippingRate,@numMarkupShippingRate,@intUsedShippingCompany,@numShipmentMethod,@numShippingService,@numPartner,@numPartenerContactId,@dtReleaseDate,@bitIsInitalSalesOrder,@numVendorAddressID,@numShipFromWarehouse
			,@vcCustomerPO,@bitDropShipAddress,@numDiscountID,@dtExpectedDate
		)                                                                                                                      
		
		SET @numOppID=(SELECT SCOPE_IDENTITY())                
                            
		--UPDATE OPPNAME AS PER NAME TEMPLATE
		EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

		SELECT @vcPOppName=ISNULL(vcPOppName,'') FROM OpportunityMaster WHERE numOppId=@numOppID

		--MAP CUSTOM FIELD	
		DECLARE @tintPageID AS TINYINT;
		SET @tintPageID = CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
		EXEC dbo.USP_AddParentChildCustomFieldMap
				@numDomainID = @numDomainID, --  numeric(18, 0)
				@numRecordID = @numOppID, --  numeric(18, 0)
				@numParentRecId = @numDivisionId, --  numeric(18, 0)
				@tintPageID = @tintPageID --  tinyint
 	
		IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
		BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END
  
		-- INSERTING ITEMS                                                                                                                    
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,
				monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
				numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,vcAttrValues,monAvgCost,bitItemPriceApprovalRequired,
				numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,numWOAssignedTo,numShipToAddressID,ItemReleaseDate
				,bitMarkupDiscount,vcChildKitSelectedItems
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
				(CASE X.numWarehouseItmsID 
				WHEN -1 
				THEN 
					CASE 
						WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU= X.vcSKU) > 0)
							THEN
								(SELECT TOP 1 
									numWareHouseItemID 
								FROM 
									WareHouseItems 
								WHERE 
									[numItemID] = X.numItemCode 
									AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
						ELSE
							(SELECT 
								[numWareHouseItemID] 
							FROM 
								[WareHouseItems] 
							WHERE 
								[numItemID] = X.numItemCode 
								AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				WHEN 0 
				THEN 
					CASE 
					WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU=X.vcSKU) > 0)
						THEN
							(SELECT TOP 1 
								numWareHouseItemID 
							FROM 
								WareHouseItems 
							WHERE 
								[numItemID] = X.numItemCode 
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
					ELSE
						(SELECT 
							[numWareHouseItemID] 
						FROM 
							[WareHouseItems] 
						WHERE 
							[numItemID] = X.numItemCode 
							AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
							AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				ELSE  
					X.numWarehouseItmsID 
				END) AS numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
				(SELECT isnull(VN.monCost,0) FROM Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode AND VN.numVendorID=IT.numVendorID WHERE  VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,
				0,
				X.numToWarehouseItemID,X.Attributes,X.AttributeIDs,(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),
				X.bitItemPriceApprovalRequired,X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(X.vcVendorNotes,'')
				,X.vcInstruction,X.bintCompliationDate,X.numWOAssignedTo,X.numShipToAddressID,ISNULL(ItemReleaseDate,@dtItemRelease),X.bitMarkupDiscount,ISNULL(X.KitChildItems,'')
			FROM
				(
					SELECT 
						*
					FROM 
						OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
					WITH                       
					(                                                                          
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9), numUnitHour FLOAT, monPrice DECIMAL(30,16), monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(2000),                                    
						numWarehouseItmsID NUMERIC(9), ItemType VARCHAR(30), DropShip BIT, bitDiscountType BIT,	fltDiscount DECIMAL(30,16), monTotAmtBefDiscount DECIMAL(20,5),
						vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),
						numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),AttributeIDs VARCHAR(500),vcSKU VARCHAR(100)
						,bitItemPriceApprovalRequired BIT,numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0)
						,numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(18,0)
						,numShipToAddressID  NUMERIC(18,0),ItemReleaseDate DATETIME,bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX)
					)
				)X    
			ORDER BY 
				X.numoppitemtCode
    
			-- UPDATE EXISTING CUSTOMER PART NO
			UPDATE
				CPN
			SET
				CPN.CustomerPartNo = ISNULL(TEMPCPN.CustomerPartNo,'')
			FROM
				CustomerPartNumber CPN
			INNER JOIN
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) AS TEMPCPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.numDomainID=@numDomainID
				AND CPN.numCompanyId = @numCompany
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- INSERT NEW CUSTOMER PART NO
			INSERT INTO CustomerPartNumber
			(
				numCompanyId, numDomainID, CustomerPartNo, numItemCode 
			)
			SELECT 
				@numCompany, @numDomainID, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
			FROM
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) TEMPCPN 
			LEFT JOIN
				CustomerPartNumber CPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.CustomerPartNoID IS NULL
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'

			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,numWarehouseItmsID=X.numWarehouseItmsID,bitDropShip=X.DropShip,
				bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
				bitWorkOrder=X.bitWorkOrder,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,
				numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,vcAttributes=X.Attributes,
				numClassID=0,vcModelID=X.vcModelID,
				bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired,vcAttrValues=X.AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost
				,vcNotes=X.vcVendorNotes,vcInstruction=X.vcInstruction,bintCompliationDate=X.bintCompliationDate,numWOAssignedTo=X.numWOAssignedTo,ItemRequiredDate=X.ItemRequiredDate
				,bitMarkupDiscount=X.bitMarkupDiscount,vcChildKitSelectedItems=ISNULL(X.KitChildItems,'')
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,
					monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,
					Attributes,bitItemPriceApprovalRequired,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) numCost
					,ISNULL(vcVendorNotes,'') vcVendorNotes,vcInstruction,bintCompliationDate,numWOAssignedTo,ItemRequiredDate,bitMarkupDiscount,KitChildItems
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
				WITH  
					(                      
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(2000), vcModelID VARCHAR(300),
						numWarehouseItmsID NUMERIC(9),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),
						numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),
						numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000)
						,vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(18,0),ItemRequiredDate DATETIME,bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX)
					)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppID=@numOppID
				
			--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
			DECLARE @TempKitConfiguration TABLE
			(
				numOppItemID NUMERIC(18,0),
				numItemCode NUMERIC(18,0),
				numKitItemID NUMERIC(18,0),
				numKitChildItemID NUMERIC(18,0),
				numQty FLOAT,
				numSequence INT
			)

			INSERT INTO @TempKitConfiguration
			(
				numOppItemID,
				numItemCode,
				numKitItemID,
				numKitChildItemID,
				numQty,
				numSequence
			)
			SELECT
				numoppitemtCode,
				numItemCode,
				numKitItemID,
				numChildItemID,
				numQty,
				numSequence
			FROM
				OpportunityItems
			CROSS APPLY
			(
				SELECT 
					Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 1)) As numKitItemID
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 2)) As numChildItemID
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 3)) As numQty
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 4)) As numSequence
				FROM  
				(
					SELECT 
						OutParam 
					FROM 
						SplitString(vcChildKitSelectedItems,',')
				) X
			) Y
			WHERE
				numOppId = @numOppID
				AND LEN(ISNULL(vcChildKitSelectedItems,'')) > 0
			ORDER BY
				ISNULL(Y.numSequence,0)

			--INSERT KIT ITEMS 
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				T1.numKitChildItemID,
				ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=T1.numKitChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),0),
				T1.numQty * OI.numUnitHour,
				T1.numQty,
				I.numBaseUnit,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			INNER JOIN
				@TempKitConfiguration T1
			ON
				T1.numOppItemID=OI.numoppitemtCode
				AND T1.numItemCode = OI.numItemCode
			JOIN
				Item I
			ON
				T1.numKitChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND (SELECT COUNT(*) FROM @TempKitConfiguration t2 WHERE t2.numOppItemID=OI.numoppitemtCode AND t2.numItemCode=OI.numItemCode AND ISNULL(t2.numKitItemID,0) = 0) > 0
			ORDER BY
				ISNULL(T1.numSequence,0)

			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),0),
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND (SELECT COUNT(*) FROM @TempKitConfiguration t2 WHERE t2.numOppItemID=OI.numoppitemtCode AND t2.numItemCode=OI.numItemCode AND ISNULL(t2.numKitItemID,0) = 0) = 0
			ORDER BY
				ISNULL(ID.sintOrder,0)

			IF (SELECT 
					COUNT(*) 
				FROM 
					OpportunityKitItems OI 
				INNER JOIN
					Item I
				ON
					OI.numChildItemID = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND ISNULL(charItemType,0) = 'P'
					AND numWareHouseItemId = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END

			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				IF (SELECT 
						COUNT(*) 
					FROM 
					(
						SELECT 
							t1.numOppItemID
							,t1.numItemCode
							,numKitItemID
							,numKitChildItemID
						FROM 
							@TempKitConfiguration t1
						WHERE
							ISNULL(t1.numKitItemID,0) > 0
					) TempChildItems
					INNER JOIN
						OpportunityKitItems OKI
					ON
						OKI.numOppId=@numOppId
						AND OKI.numOppItemID=TempChildItems.numOppItemID
						AND OKI.numChildItemID=TempChildItems.numKitItemID
					INNER JOIN
						OpportunityItems OI
					ON
						OI.numItemCode = TempChildItems.numItemCode
						AND OI.numoppitemtcode = OKI.numOppItemID
					LEFT JOIN
						WarehouseItems WI
					ON
						OI.numWarehouseItmsID = WI.numWarehouseItemID
					LEFT JOIN
						Warehouses W
					ON
						WI.numWarehouseID = W.numWarehouseID
					INNER JOIN
						ItemDetails
					ON
						TempChildItems.numKitItemID = ItemDetails.numItemKitID
						AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					INNER JOIN
						Item 
					ON
						ItemDetails.numChildItemID = Item.numItemCode
					INNER JOIN
						Item IMain
					ON
						OKI.numChildItemID = IMain.numItemCode
					WHERE
						Item.charItemType = 'P'
						AND ISNULL(IMain.bitKitParent,0) = 1
						AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
				BEGIN
					RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
				END

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped,
					monAvgCost
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
					(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0,
					ISNULL(Item.monAverageCost,0)
				FROM
				(
					SELECT 
						t1.numOppItemID
						,t1.numItemCode
						,numKitItemID
						,numKitChildItemID
					FROM 
						@TempKitConfiguration t1
					WHERE
						ISNULL(t1.numKitItemID,0) > 0
				) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numOppItemID=TempChildItems.numOppItemID
					AND OKI.numChildItemID=TempChildItems.numKitItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numoppitemtcode = OKI.numOppItemID
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END

			-- UPDATE UOM OF OPPORTUNITY ITEMS HAVE NOT UOM SET WHILE EXPORTED FROM MARKETPLACE (MEANS TINTSOURCETYPE = 3)
			IF ISNULL(@tintSourceType,0) = 3
			BEGIN
				UPDATE 
					OI 
				SET 
					numUOMID = ISNULL(I.numSaleUnit,0), 
					numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
				FROM 
					OpportunityItems OI
				JOIN 
					Item I 
				ON 
					OI.numItemCode = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND I.numDomainID = @numDomainId
			END	
                                     
			INSERT INTO OppWarehouseSerializedItem
			(
				numOppID,
				numWarehouseItmsDTLID,
				numOppItemID,
				numWarehouseItmsID
			)                      
			SELECT 
				@numOppID,
				X.numWarehouseItmsDTLID,
				X.numoppitemtCode,
				X.numWItmsID 
			FROM
			(                                                                          
				SELECT
					* 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
				WITH                        
				(                                               
				numWarehouseItmsDTLID numeric(9),                      
				numoppitemtCode numeric(9),                      
				numWItmsID numeric(9)                                                     
				)
			)X                                    
                                   
			UPDATE 
				OppWarehouseSerializedItem                       
			SET 
				numOppItemID=X.numoppitemtCode                                
			FROM                       
			(
				SELECT 
					numWarehouseItmsDTLID AS DTLID,
					O.numoppitemtCode 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
				WITH
				(
					numWarehouseItmsDTLID NUMERIC(9),numoppitemtCode NUMERIC(9),numWItmsID NUMERIC(9)
				)                                
				JOIN 
					OpportunityItems O 
				ON 
					O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID
			)X                 
			WHERE 
				numOppID=@numOppID 
				AND numWarehouseItmsDTLID=X.DTLID      
    
			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			END
                     
			EXEC sp_xml_removedocument @hDocItem                                     
		END          
	END
	ELSE                                                                                                                          
	BEGIN

		DECLARE @tempAssignedTo AS NUMERIC(9) = NULL                                                    
		SELECT 
			@tintOppStatus = tintOppStatus,
			@tempAssignedTo=ISNULL(numAssignedTo,0) 
		FROM   
			OpportunityMaster 
		WHERE  
			numOppID = @numOppID

		IF @tempAssignedTo <> @numAssignedTo
		BEGIN
			IF (SELECT COUNT(*) FROM BizDocComission WHERE ISNULL(numOppID,0) = @numOppID AND ISNULL(bitCommisionPaid,0) = 1) > 0
			BEGIN
				RAISERROR('COMMISSION_PAID_AGAINST_BIZDOC',16,1)
			END
		END
 
		--REVERTING BACK THE WAREHOUSE ITEMS                  
		IF @tintOppStatus = 1 AND @tintCommitAllocation=1
		BEGIN               
			EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID                  
		END                  

		IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
		BEGIN
			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
		END
		
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
		BEGIN
			EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END 
						
		UPDATE   
			OpportunityMaster
		SET      
			vcPOppName = @vcPOppName,txtComments = ISNULL(@Comments,''),bitPublicFlag = @bitPublicFlag,numCampainID = @CampaignID,tintSource = @tintSource,tintSourceType=@tintSourceType,
			intPEstimatedCloseDate = @dtEstimatedCloseDate,numModifiedBy = @numUserCntID,bintModifiedDate = GETUTCDATE(),lngPConclAnalysis = @lngPConclAnalysis,monPAmount = @monPAmount,
			tintActive = @tintActive,numSalesOrPurType = @numSalesOrPurType,numStatus = @numStatus,vcOppRefOrderNo=@vcOppRefOrderNo,
			bintOppToOrder=(CASE WHEN @tintOppStatus=0 AND @DealStatus=1 THEN GETUTCDATE() ELSE bintOppToOrder END),bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,
			bitBillingTerms=@bitBillingTerms,intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,tintTaxOperator=@tintTaxOperator,
			numDiscountAcntType=@numDiscountAcntType,numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
			intUsedShippingCompany = @intUsedShippingCompany, numContactID=@numContactId,numShipmentMethod=@numShipmentMethod,numShippingService=@numShippingService
			,numPartner=@numPartner,numPartenerContact=@numPartenerContactId
		WHERE
			numOppId = @numOppID   
	   
		-- UPDATING IF ORGANIZATION IS ASSIGNED TO SOMEONE                                                      
		IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN
			UPDATE
				OpportunityMaster 
			SET	
				numAssignedTo = @numAssignedTo, 
				numAssignedBy = @numUserCntID 
			WHERE 
				numOppId = @numOppID                                                    
		END                                                     
		ELSE IF(@numAssignedTo = 0) 
		BEGIN
			UPDATE 
				OpportunityMaster 
			SET 
				numAssignedTo = 0, 
				numAssignedBy = 0 
			WHERE
				numOppId = @numOppID
		END  
			                                                                    
		-- UPDATING OPP ITEMS
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0                                                                         
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   
			IF EXISTS 
			(
				SELECT 
					OI.numoppitemtCode
				FROM 
					OpportunityItems OI
				WHERE 
					numOppID=@numOppID 
					AND OI.numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))
					AND ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) > 0
			)
			BEGIN
				RAISERROR('SERIAL/LOT#_ASSIGNED_TO_ITEM_DELETED',16,1)
			END

			DELETE FROM OpportunityKitChildItems WHERE numOppID=@numOppID   
			DELETE FROM OpportunityKitItems WHERE numOppID=@numOppID                  
			DELETE FROM OpportunityItems WHERE numOppID=@numOppID AND numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9))) 
	          
			-- DELETE CORRESPONSING TIME AND EXPENSE ENTRIES OF DELETED ITEMS
			DELETE FROM
				TimeAndExpense
			WHERE
				numDomainID=@numDomainId
				AND numOppId=@numOppID
				AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9))) 

			--INSERT NEW ADDED ITEMS                        
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,
				[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numToWarehouseItemID,numShipmentMethod,
				numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired,vcAttrValues,numPromotionID,bitPromotionTriggered
				,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,numWOAssignedTo,bitMarkupDiscount,ItemReleaseDate,vcChildKitSelectedItems
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
				(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),
				(select isnull(VN.monCost,0) from Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode and VN.numVendorID=IT.numVendorID where VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numToWarehouseItemID,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
				0,
				(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired, X.AttributeIDs,
				X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(vcVendorNotes,'')
				,X.vcInstruction,X.bintCompliationDate,X.numWOAssignedTo,X.bitMarkupDiscount,@dtItemRelease,ISNULL(KitChildItems,'')
			FROM
			(
				SELECT 
					*
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH  
				(                      
					numoppitemtCode numeric(9),numItemCode numeric(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc varchar(2000),                                    
					numWarehouseItmsID numeric(9),numToWarehouseItemID numeric(9),Op_Flag tinyint,ItemType varchar(30),DropShip bit,bitDiscountType bit,
					fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),
					numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(500), 
					bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000)
					,numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(18,0)
					,bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX)
				)
			)X 
			
			-- UPDATE EXISTING CUSTOMER PART NO
			UPDATE
				CPN
			SET
				CPN.CustomerPartNo = ISNULL(TEMPCPN.CustomerPartNo,'')
			FROM
				CustomerPartNumber CPN
			INNER JOIN
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) AS TEMPCPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.numDomainID=@numDomainID
				AND CPN.numCompanyId = @numCompany
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- INSERT NEW CUSTOMER PART NO
			INSERT INTO CustomerPartNumber
			(
				numCompanyId, numDomainID, CustomerPartNo, numItemCode 
			)
			SELECT 
				@numCompany, @numDomainID, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
			FROM
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) TEMPCPN 
			LEFT JOIN
				CustomerPartNumber CPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.CustomerPartNoID IS NULL
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- UPDATE ITEMS
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,vcModelID=X.vcModelID,numWarehouseItmsID=X.numWarehouseItmsID,numToWarehouseItemID = X.numToWarehouseItemID,             
				bitDropShip=X.DropShip,bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,
				numUOMId=numUOM,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,
				numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired,bitWorkOrder=X.bitWorkOrder,vcAttrValues=AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost,vcNotes=vcVendorNotes
				,bitMarkupDiscount=X.bitMarkupDiscount,ItemRequiredDate=X.ItemRequiredDate,vcChildKitSelectedItems=ISNULL(KitChildItems,'')
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,numToWarehouseItemID,DropShip,bitDiscountType,
					fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
					bitItemPriceApprovalRequired,bitWorkOrder,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) AS numCost,ISNULL(vcVendorNotes,'') vcVendorNotes,
					ItemRequiredDate,bitMarkupDiscount,KitChildItems
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
				WITH  
				(                      
					numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(2000),vcModelID VARCHAR(300), numWarehouseItmsID NUMERIC(9), 
					numToWarehouseItemID NUMERIC(18,0),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),
					numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),numProjectStageID NUMERIC(9),Attributes VARCHAR(500),
					bitItemPriceApprovalRequired BIT, bitWorkOrder BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),
					ItemRequiredDate DATETIME, bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX)
				)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppId=@numOppID

			--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
			DELETE FROM @TempKitConfiguration

			INSERT INTO @TempKitConfiguration
			(
				numOppItemID,
				numItemCode,
				numKitItemID,
				numKitChildItemID,
				numQty,
				numSequence
			)
			SELECT
				numoppitemtCode,
				numItemCode,
				numKitItemID,
				numChildItemID,
				numQty,
				numSequence
			FROM
				OpportunityItems
			CROSS APPLY
			(
				SELECT 
					Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 1)) As numKitItemID
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 2)) As numChildItemID
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 3)) As numQty
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 4)) As numSequence
				FROM  
				(
					SELECT 
						OutParam 
					FROM 
						SplitString(vcChildKitSelectedItems,',')
				) X
			) Y
			WHERE
				numOppId = @numOppID
				AND LEN(ISNULL(vcChildKitSelectedItems,'')) > 0
			ORDER BY
				ISNULL(Y.numSequence,0)

			--INSERT KIT ITEMS 
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				T1.numKitChildItemID,
				ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=T1.numKitChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),0),
				T1.numQty * OI.numUnitHour,
				T1.numQty,
				I.numBaseUnit,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			INNER JOIN
				@TempKitConfiguration T1
			ON
				T1.numOppItemID=OI.numoppitemtCode
				AND T1.numItemCode = OI.numItemCode
			JOIN
				Item I
			ON
				T1.numKitChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND (SELECT COUNT(*) FROM @TempKitConfiguration t2 WHERE t2.numOppItemID=OI.numoppitemtCode AND t2.numItemCode=OI.numItemCode AND ISNULL(t2.numKitItemID,0) = 0) > 0
			ORDER BY
				ISNULL(T1.numSequence,0)

			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),0),
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND (SELECT COUNT(*) FROM @TempKitConfiguration t2 WHERE t2.numOppItemID=OI.numoppitemtCode AND t2.numItemCode=OI.numItemCode AND ISNULL(t2.numKitItemID,0) = 0) = 0
			ORDER BY
				ISNULL(ID.sintOrder,0)

			IF (SELECT 
					COUNT(*) 
				FROM 
					OpportunityKitItems OI 
				INNER JOIN
					Item I
				ON
					OI.numChildItemID = I.numItemCode
				WHERE 
					numOppId=@numOppID
					AND ISNULL(charItemType,0) = 'P'
					AND numWareHouseItemId = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END

			IF (SELECT 
					COUNT(*) 
				FROM 
				(
					SELECT 
						t1.numOppItemID
						,t1.numItemCode
						,numKitItemID
						,numKitChildItemID
					FROM 
						@TempKitConfiguration t1
					WHERE
						ISNULL(t1.numKitItemID,0) > 0
				) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numOppItemID=TempChildItems.numOppItemID
					AND OKI.numChildItemID=TempChildItems.numKitItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numoppitemtcode = OKI.numOppItemID
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
				INNER JOIN
					Item IMain
				ON
					OKI.numChildItemID = IMain.numItemCode
				WHERE
					Item.charItemType = 'P'
					AND ISNULL(IMain.bitKitParent,0) = 1
					AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END

			INSERT INTO OpportunityKitChildItems
			(
				numOppID,
				numOppItemID,
				numOppChildItemID,
				numItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT 
				@numOppID,
				OKI.numOppItemID,
				OKI.numOppChildItemID,
				ItemDetails.numChildItemID,
				(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
				(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
				ItemDetails.numQtyItemsReq,
				ItemDetails.numUOMId,
				0,
				ISNULL(Item.monAverageCost,0)
			FROM
			(
				SELECT 
					t1.numOppItemID
					,t1.numItemCode
					,numKitItemID
					,numKitChildItemID
				FROM 
					@TempKitConfiguration t1
				WHERE
					ISNULL(t1.numKitItemID,0) > 0
			) TempChildItems
			INNER JOIN
				OpportunityKitItems OKI
			ON
				OKI.numOppId=@numOppId
				AND OKI.numOppItemID=TempChildItems.numOppItemID
				AND OKI.numChildItemID=TempChildItems.numKitItemID
			INNER JOIN
				OpportunityItems OI
			ON
				OI.numItemCode = TempChildItems.numItemCode
				AND OI.numoppitemtcode = OKI.numOppItemID
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			INNER JOIN
				ItemDetails
			ON
				TempChildItems.numKitItemID = ItemDetails.numItemKitID
				AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
			INNER JOIN
				Item 
			ON
				ItemDetails.numChildItemID = Item.numItemCode                                                  
			                                    
			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				IF @tintCommitAllocation = 2
				BEGIN
					DELETE FROM WorkOrderDetails WHERE numWOId IN (SELECT numWOId FROM WorkOrder WHERE numOppId=@numOppID AND numWOStatus <> 23184)
					DELETE FROM WorkOrder WHERE numOppId=@numOppID AND numWOStatus <> 23184
				END

				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			END
			   		                                                                      
			EXEC sp_xml_removedocument @hDocItem                                               
		END     

		IF @tintOppType=1 AND @tintOppStatus=1 --Sales Order
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0) = 0 AND (ISNULL(numUnitHour,0) - ISNULL(numQtyShipped,0)) < 0)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_IS_LESS_THEN_SHIPPED_QTY',16,1)
				RETURN
			END

			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems INNER JOIN WorkOrder ON OpportunityItems.numOppID=WorkOrder.numOppID AND OpportunityItems.numoppitemtCode=WorkOrder.numOppItemID AND ISNULL(WorkOrder.numParentWOID,0)=0 AND WorkOrder.numWOStatus=23184 AND ISNULL(OpportunityItems.numUnitHour,0) > ISNULL(WorkOrder.numQtyItemsReq,0) WHERE OpportunityItems.numOppID=@numOppID)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_CAN_NOT_BE_GREATER_THEN_COMPLETED_WORK_ORDER',16,1)
				RETURN
			END
		END
		ELSE IF @tintOppType=2 AND @tintOppStatus=1 --Purchase Order
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0) = 0 AND (ISNULL(numUnitHour,0) - ISNULL(numUnitHourReceived,0)) < 0)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_IS_LESS_THEN_RECEIVED_QTY',16,1)
				RETURN
			END
		END

		--FOLLOWING DELETE STATEMENT IS COMMENTED BECUASE NOW WE ARE NOT ALLOWING USER TO EIDT SERIAL/LOT ITEM UNTILL USER REMOVED ASSIGNED SERIAL/LOT NUMBERS
	    --Delete previously added serial lot number added for opportunity
		--DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
	END

	IF @tintOppStatus = 1
	BEGIN
		IF (SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0)=0) <>
			(SELECT COUNT(*) FROM OpportunityItems INNER JOIN WareHouseItems ON OpportunityItems.numWarehouseItmsID=WareHouseItems.numWareHouseItemID AND OpportunityItems.numItemCode=WareHouseItems.numItemID WHERE numOppId=@numOppID AND OpportunityItems.numWarehouseItmsID > 0 AND ISNULL(bitDropShip,0)=0)
		BEGIN
			RAISERROR('INVALID_ITEM_WAREHOUSES',16,1)
			RETURN
		END
	END

	IF(SELECT 
			COUNT(*) 
		FROM 
			OpportunityBizDocItems OBDI 
		INNER JOIN 
			OpportunityBizDocs OBD 
		ON 
			OBDI.numOppBizDocID=OBD.numOppBizDocsID 
		WHERE 
			OBD.numOppId=@numOppID AND OBDI.numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)) > 0
	BEGIN
		RAISERROR('ITEM_USED_IN_BIZDOC',16,1)
		RETURN
	END

	IF(SELECT 
			COUNT(*)
		FROM
			OpportunityItems OI
		INNER JOIN
		(
			SELECT
				numOppItemID
				,SUM(numUnitHour) numInvoiceBillQty
			FROM 
				OpportunityBizDocItems OBDI 
			INNER JOIN 
				OpportunityBizDocs OBD 
			ON 
				OBDI.numOppBizDocID=OBD.numOppBizDocsID 
			WHERE 
				OBD.numOppId=@numOppID
				AND ISNULL(OBD.bitAuthoritativeBizDocs,0) = 1
			GROUP BY
				numOppItemID
		) AS TEMP
		ON
			TEMP.numOppItemID = OI.numoppitemtCode
		WHERE
			OI.numOppId = @numOppID
			AND OI.numUnitHour < Temp.numInvoiceBillQty
		) > 0
	BEGIN
		RAISERROR('ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_INVOICE/BILL_QTY',16,1)
		RETURN
	END

	IF @tintOppType=1 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
	BEGIN
		IF(SELECT 
				COUNT(*)
			FROM
			(
				SELECT
					numOppItemID
					,SUM(numUnitHour) PackingSlipUnits
				FROM 
					OpportunityBizDocItems OBDI 
				INNER JOIN 
					OpportunityBizDocs OBD 
				ON 
					OBDI.numOppBizDocID=OBD.numOppBizDocsID 
				WHERE 
					OBD.numOppId=@numOppID
					AND OBD.numBizDocId=29397 --Picking Slip
				GROUP BY
					numOppItemID
			) AS TEMP
			INNER JOIN
				OpportunityItems OI
			ON
				TEMP.numOppItemID = OI.numoppitemtCode
			WHERE
				OI.numUnitHour < PackingSlipUnits
			) > 0
		BEGIN
			RAISERROR('ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_PACKING_SLIP_QTY',16,1)
			RETURN
		END
	END

	SET @TotalAmount=0                                                           
	SELECT @TotalAmount = SUM(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID                                                                          
  
	UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numOppID

/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
            
			
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT

			SET @bitIsInitalSalesOrder=(SELECT bitIsInitalSalesOrder FROM OpportunityMaster WHERE numOppId=@numOppID AND numDomainId=@numDomainID)
			--SET @bitViolatePrice=(SELECT bitMarginPriceViolated FROM Domain WHERE numDomainId=@numDomainID)
			IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainId=@numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1)
			BEGIN
				SET @bitViolatePrice = 1
			END
			
			IF(@bitViolatePrice=1 AND @bitIsInitalSalesOrder=1 AND @tintClickBtn = 1)
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus=@DealStatus WHERE numOppId=@numOppID
			END
			ELSE
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus = @DealStatus WHERE numOppId=@numOppID
				DECLARE @tintShipped AS TINYINT               
				SELECT @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID               
				
				IF @tintOppStatus=1 AND @tintCommitAllocation=1             
				BEGIN         
					EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
				END  
				declare @tintCRMType as numeric(9)        
				declare @AccountClosingDate as datetime        

				select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from OpportunityMaster where numOppID=@numOppID         
				select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

				--When deal is won                                         
				IF @DealStatus = 1 
				BEGIN
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID              
					END

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=getutcdate()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
				END         
				--When Deal is Lost
				ELSE IF @DealStatus = 2
				BEGIN
					SELECT @AccountClosingDate=bintAccountClosingDate FROM OpportunityMaster WHERE numOppID=@numOppID         
					
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID
					END
				END 
			END

			/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
			-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
			if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
			begin        
				update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				where numDivisionID=@numDivisionID        
			end        

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50), @vcAltContact VARCHAR(200)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9), @numContact NUMERIC(18,0)
DECLARE @bitIsPrimary BIT, @bitAltContact BIT;
DECLARE @tintAddressOf TINYINT

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

--Bill Address
IF @numBillAddressId>0
BEGIN
	SELECT 
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM 
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numBillAddressId

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numBillAddressId
	END

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END
  
  
--Ship Address
IF @intUsedShippingCompany = 92 -- WILL CALL (PICK UP FROM Warehouse)
BEGIN
	IF EXISTS (SELECT numWareHouseID FROM Warehouses WHERE Warehouses.numDomainID = @numDomainID AND numWareHouseID = @numWillCallWarehouseID AND ISNULL(numAddressID,0) > 0)
	BEGIN
		SELECT  
			@vcStreet=ISNULL(vcStreet,'')
			,@vcCity=ISNULL(vcCity,'')
			,@vcPostalCode=ISNULL(AddressDetails.vcPostalCode,'')
			,@numState=ISNULL(numState,0)
			,@numCountry=ISNULL(numCountry,0)
			,@vcAddressName=vcAddressName
		FROM 
			dbo.Warehouses 
		INNER JOIN
			AddressDetails
		ON
			Warehouses.numAddressID = AddressDetails.numAddressID
		WHERE 
			Warehouses.numDomainID = @numDomainID 
			AND numWareHouseID = @numWillCallWarehouseID
	END
	ELSE
	BEGIN
	SELECT  
		@vcStreet=ISNULL(vcWStreet,'')
		,@vcCity=ISNULL(vcWCity,'')
		,@vcPostalCode=ISNULL(vcWPinCode,'')
		,@numState=ISNULL(numWState,0)
		,@numCountry=ISNULL(numWCountry,0)
		,@vcAddressName=''
	FROM 
		dbo.Warehouses 
	WHERE 
		numDomainID = @numDomainID 
		AND numWareHouseID = @numWillCallWarehouseID
	END

	UPDATE OpportunityMaster SET tintShipToType=2 WHERE numOppId=@numOppID
 
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = '', --  varchar(100)
	@numCompanyId =0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = 0,
	@bitAltContact = 0,
	@vcAltContact = ''
END
ELSE IF @numShipAddressId>0
BEGIN
	SELECT
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM  
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numShipAddressId
 

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numShipAddressId
	END


  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END

-- IMPORTANT:KEEP THIS BELOW ADDRESS UPDATE LOGIC
-- GET TAX APPLICABLE TO DIVISION WHEN ORDER IS CREATED AFTER THAT NOT NEED TO CHANGE IT
IF @bitNewOrder = 1
BEGIN
	--INSERT TAX FOR DIVISION   
	IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
	BEGIN
		INSERT INTO dbo.OpportunityMasterTaxItems 
		(
			numOppId,
			numTaxItemID,
			fltPercentage,
			tintTaxType,
			numTaxID
		) 
		SELECT 
			@numOppID,
			TI.numTaxItemID,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			TaxItems TI 
		JOIN 
			DivisionTaxTypes DTT 
		ON 
			TI.numTaxItemID = DTT.numTaxItemID
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
		) AS TEMPTax
		WHERE 
			DTT.numDivisionID=@numDivisionId 
			AND DTT.bitApplicable=1
		UNION 
		SELECT 
			@numOppID,
			0,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			dbo.DivisionMaster 
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
		) AS TEMPTax
		WHERE 
			bitNoTax=0 
			AND numDivisionID=@numDivisionID	
		UNION 
		SELECT
			@numOppId
			,1
			,decTaxValue
			,tintTaxType
			,numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,@numOppId,1,NULL)		
	END	
END

--INSERT TAX FOR DIVISION   
IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN
--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID
) 
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID = IT.numTaxItemID 
WHERE 
	OI.numOppId=@numOppID 
	AND IT.bitApplicable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	0,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numOppID 
	AND I.bitTaxable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT
	@numOppId,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
FROM
	dbo.OpportunityItems OI 
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
INNER JOIN
	TaxDetails TD
ON
	TD.numTaxID = IT.numTaxID
	AND TD.numDomainId = @numDomainId
WHERE
	OI.numOppId = @numOppID
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

END

	UPDATE 
		OI
	SET
		OI.vcInclusionDetail = [dbo].[GetOrderAssemblyKitInclusionDetails](OI.numOppID,OI.numoppitemtCode,OI.numUnitHour,1,1)
	FROM 
		OpportunityItems OI
	INNER JOIN
		Item I
	ON
		OI.numItemCode = I.numItemCode
	WHERE
		numOppId=@numOppID
		AND ISNULL(I.bitKitParent,0) = 1
  
	UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID

	IF @tintOppType=1 AND @tintOppStatus=1 
	BEGIN
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numStatus
		EXEC USP_OpportunityMaster_CreateBackOrderPO @numDomainID,@numUserCntID,@numOppID
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportQueryExecute')
DROP PROCEDURE USP_ReportQueryExecute
GO
CREATE PROCEDURE [dbo].[USP_ReportQueryExecute]     
    @numDomainID NUMERIC(18,0),
    @numUserCntID NUMERIC(18,0),
	@numReportID NUMERIC(18,0),
    @ClientTimeZoneOffset INT, 
    @textQuery NTEXT,
	@numCurrentPage NUMERIC(18,0)
AS                 
BEGIN
	DECLARE @tintReportType INT
	SELECT @tintReportType=ISNULL(tintReportType,0) FROM ReportListMaster WHERE numReportID=@numReportID

	DECLARE @avgCost int
	SET @avgCost =(select TOP 1 numCost from Domain where numDOmainId=@numDomainID)

	IF CHARINDEX('AND WareHouseItems.vcLocation',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'AND WareHouseItems.vcLocation','AND WareHouseItems.numWLocationID')
	END

	IF CHARINDEX('ReturnItems.monPrice',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'ReturnItems.monPrice','CAST(ReturnItems.monPrice AS NUMERIC(18,4))')
	END

	IF CHARINDEX('OpportunityItems.monPrice',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityItems.monPrice','CAST(OpportunityItems.monPrice AS NUMERIC(18,4))')
	END

	IF CHARINDEX('isnull(category.numCategoryID,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(category.numCategoryID,0)' ,'(SELECT ISNULL(STUFF((SELECT '','' + CONVERT(VARCHAR(50) , Category.vcCategoryName) FROM ItemCategory LEFT JOIN Category ON ItemCategory.numCategoryID = Category.numCategoryID WHERE ItemCategory.numItemID = Item.numItemCode FOR XML PATH('''')), 1, 1, ''''),''''))')
	END

	IF CHARINDEX('dbo.FormatedDateFromDate(dateadd(minute,-@ClientTimeZoneOffset,OpportunityBizDocs.dtFullPaymentDate),@numDomainId)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'dbo.FormatedDateFromDate(dateadd(minute,-@ClientTimeZoneOffset,OpportunityBizDocs.dtFullPaymentDate),@numDomainId)' ,'(CASE 
																																															WHEN ISNULL(OpportunityMaster.tintOppType,0) = 2 AND ISNULL(OpportunityBizDocs.monDealAmount,0) > 0 AND ISNULL(OpportunityBizDocs.monAmountPaid,0) >= ISNULL(OpportunityBizDocs.monDealAmount,0)
																																															THEN dbo.FormatedDateFromDate((SELECT MAX(BPH.dtPaymentDate) FROM BillPaymentDetails BPD INNER JOIN BillPaymentHeader BPH ON BPD.numBillPaymentID=BPH.numBillPaymentID  WHERE BPD.numOppBizDocsID=OpportunityBizDocs.numOppBizDocsId),@numDomainID)
																																															WHEN ISNULL(OpportunityMaster.tintOppType,0) = 1 AND ISNULL(OpportunityBizDocs.monDealAmount,0) > 0 AND ISNULL(OpportunityBizDocs.monAmountPaid,0) >= ISNULL(OpportunityBizDocs.monDealAmount,0)
																																															THEN dbo.FormatedDateFromDate((SELECT MAX(dtCreatedDate) FROM DepositeDetails WHERE DepositeDetails.numOppBizDocsID=OpportunityBizDocs.numOppBizDocsId),@numDomainID)
																																															ELSE ''''
																																														END)')
	END

	--KEEP IT BELOW ABOVE REPLACEMENT
	IF CHARINDEX('OpportunityBizDocs.dtFullPaymentDate',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityBizDocs.dtFullPaymentDate' ,'(CASE 
																												WHEN ISNULL(OpportunityMaster.tintOppType,0) = 2 AND ISNULL(OpportunityBizDocs.monDealAmount,0) > 0 AND ISNULL(OpportunityBizDocs.monAmountPaid,0) >= ISNULL(OpportunityBizDocs.monDealAmount,0)
																												THEN dbo.FormatedDateFromDate((SELECT MAX(BPH.dtPaymentDate) FROM BillPaymentDetails BPD INNER JOIN BillPaymentHeader BPH ON BPD.numBillPaymentID=BPH.numBillPaymentID  WHERE BPD.numOppBizDocsID=OpportunityBizDocs.numOppBizDocsId),@numDomainID)
																												WHEN ISNULL(OpportunityMaster.tintOppType,0) = 1 AND ISNULL(OpportunityBizDocs.monDealAmount,0) > 0 AND ISNULL(OpportunityBizDocs.monAmountPaid,0) >= ISNULL(OpportunityBizDocs.monDealAmount,0)
																												THEN dbo.FormatedDateFromDate((SELECT MAX(dtCreatedDate) FROM DepositeDetails WHERE DepositeDetails.numOppBizDocsID=OpportunityBizDocs.numOppBizDocsId),@numDomainID)
																												ELSE ''''
																											END)')
	END

	IF CHARINDEX('isnull(OpportunityMaster.monProfit,''0'')',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityMaster.monProfit,''0'')' ,CONCAT('CAST(ISNULL(OpportunityItems.monTotAmount,0) - (ISNULL(OpportunityItems.numUnitHour,0) * (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityItems.monAvgCost,0) END)) AS DECIMAL(20,5))'))
	END

	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityMaster.monProfit',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityMaster.monProfit' ,CONCAT('CAST(ISNULL(OpportunityItems.monTotAmount,0) - (ISNULL(OpportunityItems.numUnitHour,0) * (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityItems.monAvgCost,0) END)) AS DECIMAL(20,5))'))
	END

	IF CHARINDEX('isnull(OpportunityBizDocItems.monProfit,''0'')',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityBizDocItems.monProfit,''0'')' ,CONCAT('CAST(ISNULL(OpportunityBizDocItems.monTotAmount,0) - (ISNULL(OpportunityBizDocItems.numUnitHour,0) * (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityBizDocItems.monAverageCost,ISNULL(OpportunityItems.monAvgCost,0)) END)) AS DECIMAL(20,5))'))
	END

	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityBizDocItems.monProfit',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityBizDocItems.monProfit' ,CONCAT('CAST(ISNULL(OpportunityBizDocItems.monTotAmount,0) - (ISNULL(OpportunityBizDocItems.numUnitHour,0) * (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityBizDocItems.monAverageCost,ISNULL(OpportunityItems.monAvgCost,0)) END)) AS DECIMAL(20,5))'))
	END

	IF CHARINDEX('ISNULL(PT.numItemCode,0)=Item.numItemCode',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'ISNULL(PT.numItemCode,0)=Item.numItemCode' ,'Item.numItemCode=PT.numItemCode')
	END

	IF CHARINDEX('isnull(OpportunityMaster.numProfitPercent,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityMaster.numProfitPercent,0)' ,CONCAT('CAST((((ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1)) - (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityItems.monAvgCost,0) END)) / (CASE WHEN (ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1))=0 then 1 ELSE (ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1)) end) * 100) AS NUMERIC(18,2))'))
	END
	
	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityMaster.numProfitPercent',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityMaster.numProfitPercent' ,CONCAT('CAST((((ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1)) - (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityItems.monAvgCost,0) END)) / (CASE WHEN (ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1))=0 then 1 ELSE (ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1)) end) * 100) AS NUMERIC(18,2))'))
	END

	IF CHARINDEX('isnull(OpportunityBizDocItems.numProfitPercent,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityBizDocItems.numProfitPercent,0)' ,CONCAT('CAST((((ISNULL(OpportunityBizDocItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityBizDocItems.numUnitHour,0),1)) - (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityBizDocItems.monAverageCost,ISNULL(OpportunityItems.monAvgCost,0)) END)) / (CASE WHEN (ISNULL(OpportunityBizDocItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityBizDocItems.numUnitHour,0),1))=0 then 1 ELSE (ISNULL(OpportunityBizDocItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityBizDocItems.numUnitHour,0),1)) end) * 100) AS NUMERIC(18,2))'))
	END
	
	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityBizDocItems.numProfitPercent',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityBizDocItems.numProfitPercent' ,CONCAT('CAST((((ISNULL(OpportunityBizDocItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityBizDocItems.numUnitHour,0),1)) - (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityBizDocItems.monAverageCost,ISNULL(OpportunityItems.monAvgCost,0)) END)) / (CASE WHEN (ISNULL(OpportunityBizDocItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityBizDocItems.numUnitHour,0),1))=0 then 1 ELSE (ISNULL(OpportunityBizDocItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityBizDocItems.numUnitHour,0),1)) end) * 100) AS NUMERIC(18,2))'))
	END

	IF ISNULL(@numCurrentPage,0) > 0 
		AND ISNULL(@tintReportType,0) <> 3 
		AND ISNULL(@tintReportType,0) <> 4 
		AND ISNULL(@tintReportType,0) <> 5 
		AND CHARINDEX('Select top',CAST(@textQuery AS VARCHAR(MAX))) <> 1
		AND (SELECT COUNT(*) FROM ReportSummaryGroupList WHERE numReportID=@numReportID) = 0
	BEGIN
		SET @textQuery = CONCAT(CAST(@textQuery AS VARCHAR(MAX)),' OFFSET ',(@numCurrentPage-1) * 100 ,' ROWS FETCH NEXT 100 ROWS ONLY OPTION (OPTIMIZE FOR (@numDomainID UNKNOWN));')
	END
	ELSE
	BEGIN
		SET @textQuery = CONCAT(CAST(@textQuery AS VARCHAR(MAX)),' OPTION (OPTIMIZE FOR (@numDomainID UNKNOWN));')
	END
	 
	 print CAST(@textQuery AS ntext)
	EXECUTE sp_executesql @textQuery,N'@numDomainID numeric(18, 0),@numUserCntID numeric(18,0),@ClientTimeZoneOffset int',@numDomainID=@numDomainID,@numUserCntID=@numUserCntID,@ClientTimeZoneOffset=@ClientTimeZoneOffset
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Sites_GetSettingFieldValue')
DROP PROCEDURE USP_Sites_GetSettingFieldValue
GO
CREATE PROCEDURE [dbo].[USP_Sites_GetSettingFieldValue]    
(
	@numDomainID NUMERIC(18,0)
	,@numSiteID NUMERIC(18,0)
	,@vcFieldName VARCHAR(300)
)
AS
BEGIN
	IF @vcFieldName = 'tintWarehouseAvailability'
	BEGIN
		SELECT ISNULL(tintWarehouseAvailability,1)  FROM eCommerceDTL WHERE numDomainId=@numDomainID AND numSiteId = @numSiteID
	END
	ELSE IF @vcFieldName='bitDisplayQtyAvailable'
	BEGIN
		SELECT ISNULL(bitDisplayQtyAvailable,0)  FROM eCommerceDTL WHERE numDomainId=@numDomainID AND numSiteId = @numSiteID
	END
	ELSE
	BEGIN
		RAISERROR('FIELD_NOT_FOUND',16,1)
	END
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TaxReports')
DROP PROCEDURE USP_TaxReports
GO
CREATE PROCEDURE [dbo].[USP_TaxReports]     
    @numDomainID numeric(18, 0),
    @dtFromDate DATETIME,
    @dtToDate DATETIME,
	@ClientTimeZoneOffset INT=0
AS                 
BEGIN                
	SELECT 
		T.numOppBizDocsId,
		T.numReturnHeaderID,
		T.numOppId,
		vcBizDocID,
		TI.vcTaxName,
		T.fltPercentage,
		T.tintTaxType,
		SUM(Case When T.tintOppType=1 then TaxAmount else 0 end) SalesTax,
		SUM(Case When T.tintOppType=2 then TaxAmount else 0 end) PurchaseTax,
		T.INVCreatedDate,
		T.Organization,
		T.vcShipState,
		ISNULL((SELECT SUM(monTotAmtBefDiscount) FROM OpportunityBizDocItems WHERE numOppBizDocID=T.numOppBizDocsId),0) AS SubTotal,
		T.GrandTotal
	FROM 
	(
		SELECT 
			OBD.numOppBizDocsId,
			0 AS numReturnHeaderID1,
		    OBD.vcBizDocID,
			OMTI.*,
			OM.tintOppType,
			ISNULL(dbo.fn_CalOppItemTotalTaxAmt(OM.numDomainId,OMTI.numTaxItemID,OM.numOppId,OBD.numOppBizDocsId),0) AS TaxAmount,
			[dbo].[FormatedDateFromDate](OBD.dtCreatedDate,CONVERT(VARCHAR(10), @numDomainId)) AS INVCreatedDate,
			CI.vcCompanyName AS Organization,
			ISNULL(dbo.fn_getOPPState(OM.numOppId,@numDomainId,2),'') AS vcShipState,			
			OBD.monDealAmount AS GrandTotal
			
		FROM OpportunityMaster OM 
		JOIN OpportunityBizDocs OBD 
		ON OM.numOppId=OBD.numOppId
		JOIN OpportunityMasterTaxItems OMTI 
		ON OMTI.numOppId=OBD.numOppId
		LEFT JOIN DivisionMaster DM
		ON OM.numDivisionId = DM.numDivisionID
		LEFT JOIN CompanyInfo CI
		ON DM.numCompanyID = CI.numCompanyId
		
		WHERE 
			OM.numDomainID=@numDomainID 
			AND OBD.bitAuthoritativeBizDocs=1 
			AND OMTI.fltPercentage>0
			AND OBD.dtFromDate BETWEEN  @dtFromDate AND @dtToDate
		UNION
		SELECT
			0 AS numOppBizDocsId,
			RH.numReturnHeaderID,
		    RH.vcRMA,
			OMTI.*,
			OM.tintOppType,
			RH.monTotalTax * -1 AS TaxAmount,
			[dbo].[FormatedDateFromDate](DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,RH.dtCreatedDate),@numDomainID) AS INVCreatedDate,
			CI.vcCompanyName AS Organization,
			ISNULL(dbo.fn_getOPPState(OM.numOppId,212,2),'') AS vcShipState,			
			RH.monBizDocAmount AS GrandTotal
		FROM
			ReturnHeader RH
		INNER JOIN OpportunityMaster OM ON RH.numOppId=OM.numOppId
		JOIN OpportunityMasterTaxItems OMTI 
		ON OMTI.numOppId=RH.numOppId
		LEFT JOIN DivisionMaster DM
		ON RH.numDivisionId = DM.numDivisionID
		LEFT JOIN CompanyInfo CI
		ON DM.numCompanyID = CI.numCompanyId
		WHERE
			RH.numDomainId=@numDomainID
			AND DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,RH.dtCreatedDate) BETWEEN  @dtFromDate AND @dtToDate
			AND RH.numReturnStatus=303
	) T 
	JOIN 
	(
		SELECT 
			numTaxItemId,
			vcTaxName 
		FROM 
			TaxItems 
		WHERE 
			numDomainID=@numDomainID 
		UNION 
		SELECT 
			0,
			'Sales Tax'
		UNION 
		SELECT 
			1,
			'CRV'
	) TI 
	ON T.numTaxItemId=TI.numTaxItemID 
	WHERE 
		TaxAmount > 0
	GROUP BY 
		T.vcBizDocID,
		TI.vcTaxName,
		T.fltPercentage,
		T.tintTaxType,
		T.INVCreatedDate,
		T.Organization,
		T.vcShipState,
		T.GrandTotal,
		T.numOppBizDocsId,
		T.numReturnHeaderID,
		T.numOppId
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TicklerActItems')
DROP PROCEDURE USP_TicklerActItems
GO
CREATE Proc [dbo].[USP_TicklerActItems]                                                                       
@numUserCntID as numeric(9)=null,                                                                          
@numDomainID as numeric(9)=null,                                                                        
@startDate as datetime,                                                                        
@endDate as datetime,                                                                    
@ClientTimeZoneOffset Int,                                                                 
@bitFilterRecord bit=0,
@columnName varchar(50), 
@columnSortOrder varchar(50),
@vcBProcessValue varchar(500),
@RegularSearchCriteria VARCHAR(MAX)='',
@CustomSearchCriteria VARCHAR(MAX)='', 
@OppStatus int,
@CurrentPage int,                                                              
@PageSize int                                        
As     

SET @RegularSearchCriteria=REPLACE(@RegularSearchCriteria, '.numFollowUpStatus', 'Div.numFollowUpStatus'); 

	                                                                    
  
DECLARE @tintPerformanceFilter AS TINYINT
DECLARE @tintActionItemsViewRights TINYINT  = 3

IF EXISTS (SELECT * FROM GroupAuthorization WHERE numDomainID=@numDomainID AND numModuleID=1 AND numPageID=2 AND numGroupID IN (SELECT ISNULL(numGroupID,0) FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailId=@numUserCntID))
BEGIN
	SELECT @tintActionItemsViewRights=ISNULL(intViewAllowed,0) FROM GroupAuthorization WHERE numDomainID=@numDomainID AND numModuleID=1 AND numPageID=2 AND numGroupID IN (SELECT ISNULL(numGroupID,0) FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailId=@numUserCntID)
END

IF PATINDEX('%cmp.vcPerformance=1%', @RegularSearchCriteria)>0 --Last 3 Months
BEGIN
	SET @tintPerformanceFilter = 1
END
ELSE IF PATINDEX('%cmp.vcPerformance=2%', @RegularSearchCriteria)>0 --Last 6 Months
BEGIN
	SET @tintPerformanceFilter = 2
END
ELSE IF PATINDEX('%cmp.vcPerformance=3%', @RegularSearchCriteria)>0 --Last 1 Year
BEGIN
	SET @tintPerformanceFilter = 3
END
ELSE
BEGIN
	SET @tintPerformanceFilter = 0
END

IF @columnName = 'vcPerformance'   
BEGIN
	SET @columnName = 'monDealAmount'
END

DECLARE @vcSelectedEmployess VARCHAR(100)
DECLARE @numCriteria INT
DECLARE @vcActionTypes VARCHAR(100)

SELECT 
	@vcSelectedEmployess = vcSelectedEmployee,
	@numCriteria = numCriteria,
	@vcActionTypes = vcActionTypes
FROM 
	TicklerListFilterConfiguration
WHERE 
	numDomainID = @numDomainID AND
	numUserCntID = @numUserCntID


Create table #tempRecords (numContactID numeric(9),numDivisionID numeric(9),tintCRMType tinyint,
Id numeric(9),bitTask varchar(100),Startdate datetime,EndTime datetime,
itemDesc text,[Name] varchar(200),vcFirstName varchar(100),vcLastName varchar(100),numPhone varchar(15),numPhoneExtension varchar(7),
[Phone] varchar(30),vcCompanyName varchar(100),vcProfile VARCHAR(100), vcEmail varchar(50),Task varchar(100),Activity varchar(MAX),
Status varchar(100),numCreatedBy numeric(9),numRecOwner VARCHAR(500), numModifiedBy VARCHAR(500), numOrgTerId NUMERIC(18,0), numTerId VARCHAR(200),numAssignedTo varchar(1000),numAssignedBy varchar(50),
 caseid numeric(9),vcCasenumber varchar(50),casetimeId numeric(9),caseExpId numeric(9),type int,itemid varchar(50),
changekey varchar(50),DueDate varchar(50),bitFollowUpAnyTime bit,numNoOfEmployeesId varchar(50),vcComPhone varchar(50),numFollowUpStatus varchar(200),dtLastFollowUp varchar(500),vcLastSalesOrderDate VARCHAR(100),monDealAmount DECIMAL(20,5), vcPerformance VARCHAR(100),vcColorScheme VARCHAR(50),
Slp_Name varchar(100),intTotalProgress INT,vcPoppName varchar(100), numOppId NUMERIC(18,0),numBusinessProcessID numeric(18,0),numCompanyRating varchar(500),numStatusID varchar(500),numCampaignID varchar(500),[Action-Item Participants] NVARCHAR(MAX),HtmlLink varchar(500),Location nvarchar(150),OrignalDescription text)                                                         
 
 
declare @strSql as varchar(MAX)                                                            
declare @strSql1 as varchar(MAX)                         
declare @strSql2 as varchar(MAX)                                                            
declare @vcCustomColumnName AS VARCHAR(8000)
declare @vcnullCustomColumnName AS VARCHAR(8000)
   -------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

declare @tintOrder as tinyint                                                
declare @vcFieldName as varchar(50)                                                
declare @vcListItemType as varchar(3)                                           
declare @vcAssociatedControlType varchar(20)                                                
declare @numListID AS numeric(9)                                                
declare @vcDbColumnName varchar(20)                    
declare @WhereCondition varchar(2000)                     
declare @vcLookBackTableName varchar(2000)              
Declare @bitCustomField as BIT
DECLARE @bitAllowEdit AS CHAR(1)                 
Declare @numFieldId as numeric  
DECLARE @bitAllowSorting AS CHAR(1)   
declare @vcColumnName AS VARCHAR(500)       
       
Declare @ListRelID as numeric(9)             
set @tintOrder=0               


	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcFieldDataType nvarchar(50),vcOrigDbColumnName nvarchar(50),bitAllowFiltering BIT,vcFieldName nvarchar(50)
		,vcAssociatedControlType nvarchar(50),vcListItemType char(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC
		,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int
		,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth NUMERIC(18,2)
	)
                
	--Custom Fields related to Organization
	INSERT INTO 
		#tempForm
	SELECT  
		tintRow + 1 AS tintOrder,vcDbColumnName,'',vcDbColumnName,1,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'' as vcLookBackTableName                                                 
		,bitCustom,numFieldId,bitAllowSorting,0,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength
		,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
	FROM 
		View_DynamicCustomColumns
	WHERE 
		numFormId=43 
		AND numUserCntID=@numUserCntID 
		AND numDomainID=@numDomainID 
		AND tintPageType=1
		AND ISNULL(bitCustom,0)=1
 
	SELECT TOP 1 
		@tintOrder = tintOrder + 1
		,@vcDbColumnName=vcDbColumnName
		,@vcFieldName=vcFieldName
		,@vcAssociatedControlType=vcAssociatedControlType
		,@vcListItemType=vcListItemType
		,@numListID=numListID
		,@vcLookBackTableName=vcLookBackTableName                                               
		,@bitCustomField=bitCustomField
		,@numFieldId=numFieldId
		,@bitAllowSorting=bitAllowSorting
		,@bitAllowEdit=0
		,@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder ASC     
  
	SET @vcCustomColumnName=''
	SET @vcnullCustomColumnName=''

	WHILE @tintOrder>0      
	BEGIN  
		SET @vcColumnName= @vcDbColumnName
		SET @strSql = 'ALTER TABLE #tempRecords ADD [' + @vcColumnName + '] VARCHAR(800)'
		EXEC(@strSql)
		
		IF @bitCustomField = 1
		BEGIN 
			IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea'            
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+', ISNULL((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),'''') ['+ @vcColumnName +']'
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+','''' AS  ['+ @vcColumnName +']'
			END  
			ELSE IF @vcAssociatedControlType = 'CheckBox'         
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+',ISNULL((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),0)  ['+ @vcColumnName + ']'
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+',0 AS ['+ @vcColumnName + ']'
			END            
			ELSE IF @vcAssociatedControlType = 'DateField'        
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+',dbo.FormatedDateFromDate((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'              
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+','''' AS  ['+ @vcColumnName +']'
			END            
			ELSE IF @vcAssociatedControlType = 'SelectBox'           
			BEGIN            
				SET @vcCustomColumnName=@vcCustomColumnName+',(SELECT TOP 1 ISNULL(L.vcData,'''') FROM CFW_FLD_Values CFW left Join ListDetails L on L.numListItemID=CFW.Fld_Value WHERE  CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID)'+' ['+ @vcColumnName +']'                                                         
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']'                                                         
			END
			ELSE
			BEGIN
				SET @vcCustomColumnName= CONCAT(@vcCustomColumnName,',(SELECT dbo.fn_GetCustFldStringValue(',@numFieldId,',CFW.RecId,CFW.Fld_Value',') FROM CFW_FLD_Values CFW left Join ListDetails L on L.numListItemID=CFW.Fld_Value WHERE  CFW.Fld_ID=',@numFieldId,' AND CFW.RecId=Div.numDivisionID)',' [', @vcColumnName ,']')
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']' 
			END
		END
		  
		SELECT TOP 1 
			@tintOrder=tintOrder + 1
			,@vcDbColumnName=vcDbColumnName
			,@vcFieldName=vcFieldName
			,@vcAssociatedControlType=vcAssociatedControlType
			,@vcListItemType=vcListItemType
			,@numListID=numListID
			,@vcLookBackTableName=vcLookBackTableName                                               
			,@bitCustomField=bitCustomField
			,@numFieldId=numFieldId
			,@bitAllowSorting=bitAllowSorting
			,@bitAllowEdit=0
			,@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 set @tintOrder=0 
	END

 SET @strSql=''
 PRINT @vcCustomColumnName
--select top 1 @vcCSOrigDbCOlumnName=DFM.vcOrigDbCOlumnName,@vcCSLookBackTableName=DFM.vcLookBackTableName from DycFieldColorScheme DFCS join DycFieldMaster DFM on 
--DFCS.numModuleID=DFM.numModuleID and DFCS.numFieldID=DFM.numFieldID
--where DFCS.numDomainID=@numDomainID and DFM.numModuleID=1

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=43 AND DFCS.numFormID=43 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
----------------------------                   


SET @strSql = @strSql+'insert into #tempRecords
Select * from (                                                    
 SELECT Addc.numContactID,Addc.numDivisionID,Div.tintCRMType,Comm.numCommId AS Id,convert(varchar(15),bitTask) as bitTask,                                               
 DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtStartTime) as Startdate,                        
 DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtEndTime) as EndTime ,                                                                        
 Comm.textDetails AS itemDesc,                                                                                                               
 AddC.vcFirstName + '' '' + AddC.vcLastName  as [Name], 
 AddC.vcFirstName,AddC.vcLastName,Addc.numPhone,AddC.numPhoneExtension,                               
 case when Addc.numPhone<>'''' then + AddC.numPhone +case when AddC.numPhoneExtension<>'''' then '' - '' + AddC.numPhoneExtension else '''' end  else '''' end as [Phone],                                                                       
 Comp.vcCompanyName As vcCompanyName , ISNULL((SELECT vcData FROM ListDetails WHERE numListID =21 AND numDomainID='+ CAST(@numDomainID AS VARCHAR) +' AND  numListItemID=Comp.vcProfile ) ,'''') vcProfile ,                                                        
 AddC.vcEmail As vcEmail ,         
dbo.GetListIemName(Comm.bitTask)AS Task,                
 listdetailsActivity.VcData As Activity ,                                                    
 listdetailsStatus.VcData As Status   ,  -- Priority column                                                                                             
 Comm.numCreatedBy,   
 dbo.fn_GetContactName(Comm.numCreatedBy) AS numRecOwner,
 dbo.fn_GetContactName(Comm.numModifiedBy) AS numModifiedBy,                                                                                                          
 ISNULL(Div.numTerId,0) AS numOrgTerId,
 (select TOP 1 vcData from ListDetails where numListItemID=Div.numTerId) AS numTerId,                                           
Substring( 
dbo.fn_GetContactName(numAssign) + 
ISNULL((SELECT SUBSTRING((SELECT '','' + dbo.fn_GetContactName(numContactId) FROM CommunicationAttendees WHERE numCommId=Comm.numCommId FOR XML PATH('''')),1,200000)),''''),0,50)
As numAssignedTo , 
dbo.fn_GetContactName(Comm.numAssignedBy) AS numAssignedBy,        
convert(varchar(50),comm.caseid) as caseid,                          
(select top 1 vcCaseNumber from cases where cases.numcaseid= comm.caseid )as vcCasenumber,                          
ISNULL(comm.casetimeId,0) casetimeId,                          
ISNULL(comm.caseExpId,0) caseExpId,                        
0 as type   ,                    
'''' as itemid ,                    
'''' as changekey ,cast(Null as datetime) as DueDate ,ISNULL(bitFollowUpAnyTime,0) bitFollowUpAnyTime,
dbo.GetListIemName(Comp.numNoOfEmployeesId) AS numNoOfEmployeesId,Div.vcComPhone,
dbo.GetListIemName(Div.numFollowUpStatus) AS numFollowUpStatus,' + 
CONCAT('ISNULL(dbo.FormatedDateFromDate(Div.dtLastFollowUp,',@numDomainID,'),'''')') + ' AS dtLastFollowUp,' 
+ CONCAT('ISNULL(dbo.FormatedDateFromDate(TempLastOrder.bintCreatedDate,',@numDomainID,'),'''')') + ' AS vcLastSalesOrderDate,ISNULL(TempPerformance.monDealAmount,0) monDealAmount,(CASE WHEN ISNULL(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(FORMAT(TempPerformance.monDealAmount,''##,####.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) AS vcPerformance '

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	set @strSql=@strSql+',tCS.vcColorScheme'
ELSE
	set @strSql=@strSql+','''' as vcColorScheme'

SET @strSql =@strSql + ' ,ISNULL(Slp_Name,'''') AS Slp_Name,ISNULL(intTotalProgress,0) AS [intTotalProgress],ISNULL(vcPoppName,'''') AS [vcPoppName], ISNULL(Opp.numOppId,0) AS [numOppId],ISNULL(Opp.numBusinessProcessID,0) AS numBusinessProcessID'
SET @strSql =@strSql + ' ,(select TOP 1 vcData from ListDetails where numListItemID=Comp.numCompanyRating) AS numCompanyRating ,(select TOP 1 vcData from ListDetails where numListItemID=Div.numStatusID) AS numStatusID,(SELECT TOP 1 vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID) AS numCampaignID,(SELECT DISTINCT  
replace(replace(STUFF((SELECT ''^<br>'' 
+ CASE(ISNULL(Comp.vcCompanyName,'''')) WHEN '''' THEN '''' else ''<a href=javascript:OpenCompany('' +  cast(isnull(Addc.numDivisionID,0) as varchar(20)) + '','' +  cast(isnull(Div.tintCRMType,0) as varchar(20)) + '',1) style=''''COLOR:black;TEXT-DECORATION:underline;''''>'' + ltrim(RTRIM(vcCompanyName)) +  ''</a> ''  END
+ CASE(isnull(AddC.vcFirstName,'''')) when '''' THEN ''''	else ''<a href=javascript:OpenContact('' +  cast(isnull(AddC.numContactId,0) as varchar(20)) + '',1) style=''''COLOR:#dd8047;''''>'' + ltrim(RTRIM(AddC.vcFirstName)) + ''</a> ''  END
+ CASE(isnull(addc.vcLastName,'''')) when '''' THEN '''' else ''<a href=javascript:OpenContact('' +  cast(isnull(AddC.numContactId,0) as varchar(20)) + '',1) style=''''COLOR:#dd8047;''''>'' + ltrim(RTRIM(AddC.vcLastName)) + ''</a> ''  END 
+ CASE(isnull(AddC.numPhone,'''')) WHEN '''' then '''' ELSE AddC.numPhone + '' '' END
+ CASE(isnull(AddC.numPhoneExtension,'''')) when '''' then '''' else '' ('' + AddC.numPhoneExtension + '') '' END 
+ CASE(isnull(addc.vcEmail,'''')) when '''' THEN '''' ELSE ''<a href=''''#'''' onclick=javascript:OpemEmail(''''../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail='' + ltrim(RTRIM(AddC.vcEmail)) + '''''') style=''''COLOR:#3c8dbc;''''>'' + ltrim(RTRIM(AddC.vcEmail)) + ''</a> ''  END 
FOR XML PATH('''')
), 1, 1, ''''),''&lt;'', ''<''), ''&gt;'', ''>'')) AS [Action-Item Participants],'''' AS HtmlLink, '''' AS Location,'''' As OrignalDescription'
SET @strSql =@strSql+ @vcCustomColumnName
SET @strSql =@strSql + ' FROM  Communication Comm                              
 JOIN AdditionalContactsInformation AddC                                                  
 ON Comm.numContactId = AddC.numContactId                                  
 JOIN DivisionMaster Div                                                                         
 ON AddC.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                                                                         
 ON Div.numCompanyID = Comp.numCompanyId     
 OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=Div.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder
 OUTER APPLY(' + CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = Div.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		AND 1 = (CASE ',@tintPerformanceFilter,' 
																				WHEN 1 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-3,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 2 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-6,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 3 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(YEAR,-1,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				ELSE 1
																				END)
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') + ' AS TempPerformance
 Left Join listdetails listdetailsActivity                                                  
 On Comm.numActivity = listdetailsActivity.NumlistItemID                                               
 Left Join listdetails listdetailsStatus                                                 
 On Comm.numStatus = listdetailsStatus.NumlistItemID
 LEFT JOIN OpportunityMaster Opp ON Opp.numDivisionId = Div.numDivisionID AND Opp.numContactId = AddC.numContactId  AND Opp.numOppID = Comm.numOppID 
 LEFT JOIN Sales_process_List_Master splm ON splm.Slp_Id = Opp.numBusinessProcessID 
 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId '
 

DECLARE @Prefix AS VARCHAR(5)
 
 IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
	 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
		set @Prefix = 'AddC.'                  
  else if @vcCSLookBackTableName = 'DivisionMaster'                  
		set @Prefix = 'Div.'
	else if @vcCSLookBackTableName = 'CompanyInfo'                  
		set @Prefix = 'Comp.'  
	ELSE IF @vcCSLookBackTableName='Tickler'	
		set @Prefix = 'Comm.'  
--	else if @vcCSLookBackTableName = 'ProjectProgress'                  
--		set @Prefix = 'pp.'  	
--	else if @vcCSLookBackTableName = 'OpportunityMaster'                  
--		set @Prefix = 'om.'  
--	else if @vcCSLookBackTableName = 'Sales_process_List_Master'                  
--		set @Prefix = 'splm.'  		
	else
		set @Prefix = ''   
	
	
	IF @vcCSAssociatedControlType='DateField'
	BEGIN
	IF @vcCSOrigDbCOlumnName='FormattedDate'
			SET @vcCSOrigDbCOlumnName='dtStartTime'
	
	
			set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',' + @Prefix + @vcCSOrigDbCOlumnName + '),111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
			 and CONVERT(VARCHAR(10),DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',' + @Prefix + @vcCSOrigDbCOlumnName + '),111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
	END
	ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'  OR @vcCSAssociatedControlType='TextBox'
	BEGIN
		--PRINT 1
		set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
	END
END

set @strSql=@strSql+'  WHERE Comm.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (Comm.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or Comm.numAssign= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END) '
 
 IF LEN(ISNULL(@vcBProcessValue,''))>0
BEGIN
	SET @strSql = @strSql + ' and ISNULL(Opp.numBusinessProcessID,0) in (SELECT Items FROM dbo.Split(''' +@vcBProcessValue + ''','',''))' 			
END

IF LEN(@vcActionTypes) > 0 
BEGIN
	SET @strSql = @strSql + ' AND Comm.bitTask IN (' + @vcActionTypes + ')'  
END
  
IF @numCriteria  = 1 --Action Items	Assigned
BEGIN
	set @strSql=@strSql+' AND 1=(Case when Comm.numAssign IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +') then 1 else 0 end)'
END
ELSE IF @numCriteria = 2 --Action Items Owned
BEGIN
	set @strSql=@strSql+' AND 1=(Case when Comm.numCreatedBy IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +' ) then 1 else 0 end)'
END
ELSE --Action Items Assigned Or Owned
BEGIN
	set @strSql=@strSql+' AND 1=(Case when (Comm.numAssign IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')  or Comm.numCreatedBy IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')) then 1 else 0 end)'
END

        
  
IF(LEN(@RegularSearchCriteria)>0)
BEGIN
IF PATINDEX('%cmp.vcPerformance%', @RegularSearchCriteria)>0
BEGIN
	-- WE ARE MANAGING CONDITION IN OUTER APPLY
	set @strSql=@strSql
END
ELSE
BEGIN
	SET @strSql=@strSql+' AND '+@RegularSearchCriteria
END
END

IF LEN(@CustomSearchCriteria) > 0
BEGIN
	SET @strSql=@strSql +' AND '+ @CustomSearchCriteria
END

set @strSql=@strSql+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) <= '''+Cast(@endDate as varchar(30))+''')'                                             

IF @OppStatus  = 0 --OPEN
BEGIN
	set @strSql=@strSql+'  AND Comm.bitclosedflag=0 AND Comm.bitTask <> 973  ) As X '
END
ELSE IF @OppStatus = 1 --CLOSED
BEGIN
	set @strSql=@strSql+' AND Comm.bitclosedflag=1 AND Comm.bitTask <> 973  ) As X '
END
ELSE 
BEGIN
	set @strSql=@strSql+' AND Comm.bitclosedflag in (0,1) AND Comm.bitTask <> 973  ) As X '
END
                                
              

IF LEN(ISNULL(@vcBProcessValue,''))=0 AND LEN(@RegularSearchCriteria) = 0 AND LEN(@CustomSearchCriteria) = 0
BEGIN
	If LEN(ISNULL(@vcActionTypes,'')) = 0 OR EXISTS (SELECT OutParam FROM dbo.SplitString(@vcActionTypes,',') WHERE OutParam='982')
	BEGIN
		SET @strSql1 =  ' insert into #tempRecords 
select                          
 ACI.numContactID,ACI.numDivisionID,Div.tintCRMType,ac.activityid as id ,case when alldayevent = 1 then ''5'' else ''0'' end as bitTask,                        
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) startdate,                        
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dateadd(second,duration,startdatetimeutc)) as endtime,                        
(SELECT DISTINCT  
replace(replace((SELECT 
CASE(ISNULL(bitTitle,1)) WHEN 1 THEN ''<b><font color=red>Title</font></b><br>'' + CASE(ISNULL(ac.Subject,'''')) WHEN '''' THEN '''' else ac.Subject END + ''<br>'' else ''''  END  
+ CASE(ISNULL(bitLocation,1)) WHEN 1 THEN ''<b><font color=green>Location</font></b><br>'' + CASE(ISNULL(ac.location,'''')) WHEN '''' THEN '''' else ac.location END + ''<br>'' else ''''  END  
+ CASE(isnull(bitDescription,1)) when 1 THEN ''<b><font color=#3c8dbc>Description</font></b><br>$'' else ''''  END  
FROM ActivityDisplayConfiguration where numDomainId='+ CAST(@numDomainID AS VARCHAR) +' and numUserCntId='+ CAST(@numUserCntID AS VARCHAR) +'
FOR XML PATH('''')
),''&lt;'', ''<''), ''&gt;'', ''>'')) AS itemDesc,                        
ACI.vcFirstName + '' '' + ACI.vcLastName name, 
 ACI.vcFirstName,ACI.vcLastName,ACI.numPhone,ACI.numPhoneExtension,                   
 case when ACI.numPhone<>'''' then + ACI.numPhone +case when ACI.numPhoneExtension<>'''' then '' - '' + ACI.numPhoneExtension else '''' end  else '''' end as [Phone],                        
 Substring ( Comp.vcCompanyName , 0 , 100 ) + ''..'' As vcCompanyName , ISNULL((SELECT vcData FROM ListDetails WHERE numListID =21 AND numDomainID='+ CAST(@numDomainID AS VARCHAR) +' AND  numListItemID=Comp.vcProfile ) ,'''') vcProfile ,                      
vcEmail,                        
''Calendar'' as task,   
[subject] as Activity,                   
case when importance =0 then ''Low'' when importance =1 then ''Normal'' when importance =2 then ''High'' end as status,                        
0 as numcreatedby,
'''' as numRecOwner, 
'''' AS numModifiedBy,                  
0 as numorgterid,       
'''' as numterid,                 
''-/-'' as numAssignedTo, 
''-'' AS numAssignedBy,        
''0'' as caseid,                        
null as vccasenumber,                        
0 as casetimeid,                        
0 as caseexpid,                        
1 as type ,                    
isnull(itemid,'''') as itemid  ,                    
changekey  ,cast(Null as datetime) as DueDate,cast(0 as bit) as bitFollowUpAnyTime,
dbo.GetListIemName(Comp.numNoOfEmployeesId) AS numNoOfEmployeesId,Div.vcComPhone,
dbo.GetListIemName(Div.numFollowUpStatus) AS numFollowUpStatus,' + 
CONCAT('ISNULL(dbo.FormatedDateFromDate(Div.dtLastFollowUp,',@numDomainID,'),'''')') + ' AS dtLastFollowUp,' 
+ CONCAT('ISNULL(dbo.FormatedDateFromDate(TempLastOrder.bintCreatedDate,',@numDomainID,'),'''')') + ' AS vcLastSalesOrderDate,ISNULL(TempPerformance.monDealAmount,0) monDealAmount, (CASE WHEN ISNULL(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(FORMAT(TempPerformance.monDealAmount,''##,####.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) vcPerformance,'''' as vcColorScheme,'''',0,'''',0 AS [numOppId],0 AS numBusinessProcessID '
SET @strSql1 =@strSql1 + ' ,(select TOP 1 vcData from ListDetails where numListItemID=Comp.numCompanyRating) AS numCompanyRating ,(select TOP 1 vcData from ListDetails where numListItemID=Comp.numCompanyRating) AS numCompanyRating,''-'' AS numCampaignID '+@vcnullCustomColumnName 



SET @strSql1=@strSql1 + ' ,(SELECT DISTINCT  
replace(replace(STUFF((SELECT ''^<br>'' 
+ CASE(ISNULL(CI.vcCompanyName,'''')) WHEN '''' THEN ''''		else ''<a href=javascript:OpenCompany('' +  cast(isnull(ATM.DivisionID,0) as varchar(20)) + '','' +  cast(isnull(ATM.tintCRMType,0) as varchar(20)) + '',1) style=''''COLOR:black;TEXT-DECORATION:underline;''''>'' + ltrim(RTRIM(CI.vcCompanyName)) + ''</a> ''  END  
+ CASE(isnull(ATM.AttendeeFirstName,'''')) when '''' THEN ''''	else ''<a href=javascript:OpenContact('' +  cast(isnull(ATM.ContactID,0) as varchar(20)) + '',1) style=''''COLOR:#dd8047;''''>'' + ltrim(RTRIM(ATM.AttendeeFirstName)) + ''</a> ''  END
+ CASE(isnull(ATM.AttendeeLastName,'''')) when '''' THEN '''' else ''<a href=javascript:OpenContact('' +  cast(isnull(ATM.ContactID,0) as varchar(20)) + '',1) style=''''COLOR:#dd8047;''''>'' + ltrim(RTRIM(ATM.AttendeeLastName)) + ''</a> ''  END 
+ CASE(isnull(ATM.AttendeePhone,'''')) WHEN '''' then '''' ELSE ATM.AttendeePhone + '' '' END
+ CASE(isnull(ATM.AttendeePhoneExtension,'''')) when '''' then '''' else '' ('' + ATM.AttendeePhoneExtension + '') '' END 
+ CASE(isnull(ATM.AttendeeEmail,''''))    when '''' THEN '''' ELSE ''<a href=''''#'''' onclick=javascript:OpemEmail(''''../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail='' + ltrim(RTRIM(ATM.AttendeeEmail)) + '''''') style=''''COLOR:#3c8dbc;''''>'' + ltrim(RTRIM(ATM.AttendeeEmail)) + ''</a> ''  END 
+ CASE(isnull(ATM.ResponseStatus,'''')) WHEN '''' THEN '''' WHEN  ''needsAction''  then '''' ELSE ''<img src=''''../images/'' + ATM.ResponseStatus + ''.gif'''' align=''''BASELINE'''' style=''''float:none;vertical-align:middle;width: 15px;''''/>'' END
FROM ActivityAttendees ATM
Left Join CompanyInfo CI on ATM.CompanyNameinBiz=CI.numCompanyId
WHERE ATM.ActivityID = ac.ActivityID and ATM.AttendeeEmail is not null
FOR XML PATH('''')
), 1, 1, ''''),''&lt;'', ''<''), ''&gt;'', ''>'')) AS [Action-Item Participants],ac.HtmlLink AS HtmlLink,ac.Location As Location,ac.ActivityDescription As OrignalDescription'



SET @strSql1 = @strSql1+' from activity ac join             
activityresource ar on  ac.activityid=ar.activityid             
join resource res on ar.resourceid=res.resourceid                         
--join usermaster UM on um.numUserId = res.numUserCntId                
join AdditionalContactsInformation ACI on ACI.numContactId = res.numUserCntId                
 JOIN DivisionMaster Div                                                          
 ON ACI.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                             
 ON Div.numCompanyID = Comp.numCompanyId
 OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=Div.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder                 
 OUTER APPLY(' + CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = Div.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		AND 1 = (CASE ',@tintPerformanceFilter,' 
																				WHEN 1 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-3,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 2 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-6,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 3 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(YEAR,-1,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				ELSE 1
																				END)
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') + ' AS TempPerformance
 where  res.numUserCntId = ' + Cast(@numUserCntID as varchar(10)) +'             
and ac.OriginalStartDateTimeUtc IS NULL            
and res.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  and 
(DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) <= '''+Cast(@endDate as varchar(30))+''')
--(startdatetimeutc between dateadd(month,-1,getutcdate()) and dateadd(month,1,getutcdate())) 
and [status] <> -1                                                     
and ac.activityid not in 
(select distinct(numActivityId) from communication Comm where Comm.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  
AND 1=(Case ' + Cast(@bitFilterRecord as varchar(10)) +' when 1 then Case when Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  then 1 else 0 end 
		else  Case when (Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  or Comm.numCreatedBy=' + Cast(@numUserCntID as varchar(10)) +' ) then 1 else 0 end end)   
		AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (Comm.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or Comm.numAssign= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END)                                                                   
-- AND (dtStartTime >= '''+Cast(@startDate as varchar(30))+''' and dtStartTime <= '''+Cast(@endDate as varchar(30))+''') 
) 
 Order by endtime'
 
	END

-- btDocType =2 for Document approval request, =1 for bizdoc
SET @strSql2 = ' insert into #tempRecords
select 
B.numContactID,B.numDivisionID,E.tintCRMType,CASE BA.btDocType WHEN 2 THEN BA.numOppBizDocsId WHEN 3 THEN BA.numOppBizDocsId ELSE A.numBizActionID END as ID,
convert(varchar(15),A.bitTask) as bitTask,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) as Startdate,                        
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) as EndTime ,  
'''' as itemDesc,
vcFirstName + '' '' + vcLastName  as [Name],
B.vcFirstName , B.vcLastName ,B.numPhone,B.numPhoneExtension,  
case when B.numPhone<>'''' then + B.numPhone +case when B.numPhoneExtension<>'''' then '' - '' + B.numPhoneExtension else '''' end  else '''' end as [Phone],
vcCompanyName, ISNULL((SELECT vcData FROM ListDetails WHERE numListID =21 AND numDomainID='+ CAST(@numDomainID AS VARCHAR) +' AND  numListItemID=D.vcProfile ) ,'''') vcProfile ,
vcEmail,
CASE BA.btDocType WHEN 2  THEN  ''Document Approval Request'' ELSE ''BizDoc Approval Request'' END as Task,
dbo.GetListIemName(bitTask) as Activity,
'''' as Status,
A.numCreatedBy,
dbo.fn_GetContactName(A.numCreatedBy) as numRecOwner,
dbo.fn_GetContactName(A.numModifiedBy) as numModifiedBy,
 ISNULL(E.numTerId,0) AS numOrgTerId, 
 (select TOP 1 vcData from ListDetails where numListItemID=E.numTerId) AS numTerId, 
case When Len( dbo.fn_GetContactName(numAssign) ) > 12                                           
  Then Substring( dbo.fn_GetContactName(numAssign) , 0  ,  12 ) + ''..''                                           
  Else dbo.fn_GetContactName(numAssign)                                           
 end                                          
 numAssignedTo ,  
 ''-'' AS numAssignedBy,        
''0'' as caseid,
''0'' as vcCasenumber,
0 as CaseTimeId,
0 as CaseExpId,
CASE BA.btDocType WHEN 2 THEN 2 ELSE 3 END as [type], 
'''' AS itemid  ,      
 '''' AS changekey ,   
dtCreatedDate as DueDate  ,cast(0 as bit) as bitFollowUpAnyTime,
dbo.GetListIemName(D.numNoOfEmployeesId) AS numNoOfEmployeesId,E.vcComPhone,
dbo.GetListIemName(E.numFollowUpStatus) AS numFollowUpStatus,' + 
CONCAT('ISNULL(dbo.FormatedDateFromDate(E.dtLastFollowUp,',@numDomainID,'),'''')') + ' AS dtLastFollowUp,' + 
CONCAT('ISNULL(dbo.FormatedDateFromDate(TempLastOrder.bintCreatedDate,',@numDomainID,'),'''')') + ' AS vcLastSalesOrderDate,ISNULL(TempPerformance.monDealAmount,0) monDealAmount, (CASE WHEN ISNULL(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(FORMAT(TempPerformance.monDealAmount,''##,####.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) AS vcPerformance,'''' as vcColorScheme,'''',0,'''', 0 AS [numOppId],0 AS numBusinessProcessID '
SET @strSql2 =@strSql2 + ' ,(select TOP 1 vcData from ListDetails where numListItemID=D.numCompanyRating) AS numCompanyRating ,(select TOP 1 vcData from ListDetails where numListItemID=D.numCompanyRating) AS numCompanyRating,''-'' AS numCampaignID '+@vcnullCustomColumnName 
SET @strSql2 =@strSql2 +',(SELECT DISTINCT  
replace(replace(STUFF((SELECT ''^<br>'' 
+ CASE(ISNULL(vcCompanyName,'''')) WHEN '''' THEN '''' else ''<a href=javascript:OpenCompany('' +  cast(isnull(B.numDivisionID,0) as varchar(20)) + '','' +  cast(isnull(E.tintCRMType,0) as varchar(20)) + '',1) style=''''COLOR:black;TEXT-DECORATION:underline;''''>'' + ltrim(RTRIM(vcCompanyName)) +  ''</a> ''  END
+ CASE(isnull(B.vcFirstName,'''')) when '''' THEN ''''	else ''<a href=javascript:OpenContact('' +  cast(isnull(B.numContactId,0) as varchar(20)) + '',1) style=''''COLOR:#dd8047;''''>'' + ltrim(RTRIM(B.vcFirstName)) + ''</a> ''  END
+ CASE(isnull(B.vcLastName,'''')) when '''' THEN '''' else ''<a href=javascript:OpenContact('' +  cast(isnull(B.numContactId,0) as varchar(20)) + '',1) style=''''COLOR:#dd8047;''''>'' + ltrim(RTRIM(B.vcLastName)) + ''</a> ''  END 
+ CASE(isnull(B.numPhone,'''')) WHEN '''' then '''' ELSE B.numPhone + '' '' END
+ CASE(isnull(B.numPhoneExtension,'''')) when '''' then '''' else '' ('' + B.numPhoneExtension + '') '' END 
+ CASE(isnull(B.vcEmail,'''')) when '''' THEN '''' ELSE ''<a href=''''#'''' onclick=javascript:OpemEmail(''''../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail='' + ltrim(RTRIM(B.vcEmail)) + '''''') style=''''COLOR:#3c8dbc;''''>'' + ltrim(RTRIM(B.vcEmail)) + ''</a> ''  END 
FOR XML PATH('''')
), 1, 1, ''''),''&lt;'', ''<''), ''&gt;'', ''>'')) AS [Action-Item Participants],'''' AS HtmlLink,'''' As Location,'''' As OrignalDescription
FROM 
	BizDocAction A 
LEFT JOIN 
	dbo.BizActionDetails BA 
ON 
	BA.numBizActionId = A.numBizActionId, 
AdditionalContactsInformation B, 
CompanyInfo D,
DivisionMaster E
OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=E.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder
OUTER APPLY(' + CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = E.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		AND 1 = (CASE ',@tintPerformanceFilter,' 
																				WHEN 1 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-3,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 2 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-6,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 3 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(YEAR,-1,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				ELSE 1
																				END)
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') + ' AS TempPerformance
WHERE A.numContactId=b.numContactId and a.numDomainID=b.numDomainID  and 
D.numCompanyId=E.numCompanyId and 
D.numDomainID=a.numDomainID and 
E.numDivisionId=B.numDivisionId and 
a.numDomainID= ' + Cast(@numDomainID as varchar(10)) +' and a.numContactId = ' + Cast(@numUserCntID as varchar(10)) +'  and  
A.numStatus=0 
and (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) <= '''+Cast(@endDate as varchar(30))+''')'

END


declare @Nocolumns as tinyint         
set @Nocolumns=0       
         
Select @Nocolumns=isnull(count(*),0) from View_DynamicColumns where numFormId=43 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  
  
if @Nocolumns > 0            
begin      
INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcFieldDataType,vcOrigDbColumnName,bitAllowFiltering,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
 FROM View_DynamicColumns 
 where numFormId=43 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
  order by tintOrder asc  
  
end            
else            
begin 

	--Check if Master Form Configuration is created by administrator if NoColumns=0
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 43 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END

	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			43,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,NULL,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=43 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0

		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,vcFieldDataType,vcOrigDbColumnName,bitAllowFiltering, ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType,numListID,vcLookBackTableName, bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=43 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	ORDER BY 
		tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		 INSERT INTO #tempForm
		select tintOrder,vcDbColumnName,vcFieldDataType
		,vcOrigDbColumnName,bitAllowFiltering,ISNULL(vcCultureFieldName,vcFieldName),                  
		 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
		,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
		 FROM View_DynamicDefaultColumns
		 where numFormId=43 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 AND numDomainID=@numDomainID
		order by tintOrder asc  
	END 
end                                            

DECLARE @SQLtemp varchar(max)
SET @SQLtemp= 'update #tempRecords
SET [Action-Item Participants] = SUBSTRING([Action-Item Participants], 5, LEN([Action-Item Participants]))'

exec (@strSql + @strSql1 + @strSql2 + @SQLtemp )

DECLARE @strSql3 AS NVARCHAR(MAX)



SET @strSql3=   'SELECT COUNT(*) OVER() TotalRecords, * FROM #tempRecords '


IF CHARINDEX(@columnName,@strSql) > 0 OR CHARINDEX(@columnName,@strSql1) > 0 OR CHARINDEX(@columnName,@strSql2) > 0
BEGIN
	SET @strSql3= CONCAT(@strSql3,' order by ',CASE WHEN @columnName = 'Action-Item Participants' THEN CONCAT('[','Action-Item Participants',']') ELSE @columnName END,' ',@columnSortOrder,' OFFSET ',( @CurrentPage - 1 ) * @PageSize,' ROWS FETCH NEXT ',@PageSize,' ROWS ONLY')
END
ELSE
BEGIN
	SET @strSql3= CONCAT(@strSql3,' order by EndTime Asc OFFSET ',( @CurrentPage - 1 ) * @PageSize,' ROWS FETCH NEXT ',@PageSize,' ROWS ONLY')
END

     	exec sp_executesql @strSql3
	---- EXECUTE FINAL QUERY
	--DECLARE @strFinal AS VARCHAR(MAX)
	--SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, ',@strColumns,' INTO #tempTable',@strSql,'; SELECT * FROM #tempTable WHERE ID > ',@firstRec,' and ID <',@lastRec,' ORDER BY ID; DROP TABLE #tempTable;')


	--PRINT @strFinal

	

drop table #tempRecords

SELECT * FROM #tempForm order by tintOrder

DROP TABLE #tempForm
