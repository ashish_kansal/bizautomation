/******************************************************************
Project: Release 3.8 Date: 03.NOV.2014
Comments: STORED PROCEDURES
*******************************************************************/


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AccountsReports')
DROP PROCEDURE USP_AccountsReports
GO
CREATE PROCEDURE [dbo].[USP_AccountsReports]     
    @numDomainID numeric(18, 0),
    @dtFromDate DATETIME,
    @dtToDate DATETIME,
    @tintReportType TINYINT,
    @numAccountClass AS NUMERIC(9)=0
as                 

IF @tintReportType=1 --Report Name: Check Register
BEGIN
	SELECT CH.numCheckNo,CH.dtCheckDate,CH.monAmount,CH.numDivisionID,C.vcCompanyName,COA.vcAccountName
	FROM dbo.CheckHeader CH 
	JOIN dbo.DivisionMaster D ON CH.numDivisionID=D.numDivisionID
	JOIN dbo.CompanyInfo C ON D.numCompanyID = C.numCompanyId
	JOIN dbo.Chart_Of_Accounts COA ON CH.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE CH.numDomainID=@numDomainID --AND ISNULL(bitIsPrint,0)=0 
	AND CH.dtCheckDate BETWEEN  @dtFromDate AND @dtToDate
	AND (CH.numAccountClass=@numAccountClass OR @numAccountClass=0)
	order by dtCheckDate,numCheckNo
END

ELSE IF @tintReportType=2 --Report Name: Sales Journal Detail (By GL Account)
BEGIN
	SELECT vcAccountName,SUM(ISNULL(numCreditAmt,0)) AS numCreditAmt,SUM(ISNULL(numDebitAmt,0)) AS numDebitAmt 
	FROM (
	SELECT GJD.numChartAcntId,COA.vcAccountName,ISNULL(GJD.numCreditAmt,0) AS numCreditAmt,ISNULL(GJD.numDebitAmt,0) AS numDebitAmt 
	FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
	JOIN dbo.OpportunityMaster OM ON OM.numOppId=GJH.numOppId AND OM.tintOppType=1 AND OM.numDomainId=@numDomainID
	JOIN dbo.OpportunityBizDocs OBD ON OM.numOppId=OBD.numOppId AND OBD.numOppBizDocsId=GJH.numOppBizDocsId 
	AND OBD.bitAuthoritativeBizDocs=1
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE GJH.numDomainId=@numDomainID
	AND GJH.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate
	AND (OM.numAccountClass=@numAccountClass OR @numAccountClass=0)
	
	UNION ALL 
	
	SELECT GJD.numChartAcntId,COA.vcAccountName,ISNULL(GJD.numCreditAmt,0) AS numCreditAmt,ISNULL(GJD.numDebitAmt,0) AS numDebitAmt 
	FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
	JOIN dbo.ReturnHeader RH ON RH.numDomainID=@numDomainID AND RH.numReturnHeaderID=GJH.numReturnID
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE GJH.numDomainId=@numDomainID AND RH.tintReceiveType=2 AND RH.tintReturnType IN(1,3) AND  RH.numDomainId=@numDomainID
	AND GJH.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate
	AND (RH.numAccountClass=@numAccountClass OR @numAccountClass=0)) AS T
	GROUP BY numChartAcntId,vcAccountName ORDER BY vcAccountName
END

ELSE IF @tintReportType=3 --Report Name: Purchase Journal
BEGIN
SELECT vcAccountName,SUM(ISNULL(numCreditAmt,0)) AS numCreditAmt,SUM(ISNULL(numDebitAmt,0)) AS numDebitAmt
FROM (
	SELECT GJD.numChartAcntId,COA.vcAccountName,ISNULL(GJD.numCreditAmt,0) AS numCreditAmt,ISNULL(GJD.numDebitAmt,0) AS numDebitAmt 
	FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
	JOIN dbo.OpportunityMaster OM ON OM.numOppId=GJH.numOppId AND OM.tintOppType=2 AND OM.numDomainId=@numDomainID
	JOIN dbo.OpportunityBizDocs OBD ON OM.numOppId=OBD.numOppId AND OBD.numOppBizDocsId=GJH.numOppBizDocsId AND OBD.bitAuthoritativeBizDocs=1
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE GJH.numDomainId=@numDomainID 
	AND GJH.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate
	AND (OM.numAccountClass=@numAccountClass OR @numAccountClass=0)
	
	UNION ALL
	
	SELECT GJD.numChartAcntId,COA.vcAccountName,ISNULL(GJD.numCreditAmt,0) AS numCreditAmt,ISNULL(GJD.numDebitAmt,0) AS numDebitAmt 
	FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
	JOIN dbo.BillHeader BH ON BH.numBillID=GJH.numBillID AND BH.numDomainId=@numDomainID
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE GJH.numDomainId=@numDomainID 
	AND GJH.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate
	AND (BH.numAccountClass=@numAccountClass OR @numAccountClass=0)) AS T
	GROUP BY numChartAcntId,vcAccountName ORDER BY vcAccountName
END

ELSE IF @tintReportType=4 --Report Name: Invoice Register
BEGIN

	SELECT OM.vcPOppName,OBD.vcBizDocID,OBD.dtFromDate AS FromDate,C.vcCompanyName,OM.monDealAMount,OBD.monDealAMount AS monBizDocAmount,OBD.monAmountPaid
	FROM OpportunityMaster OM
	JOIN dbo.OpportunityBizDocs OBD ON OM.numOppId=OBD.numOppId AND OBD.bitAuthoritativeBizDocs=1 
	JOIN dbo.DivisionMaster D ON OM.numDivisionID=D.numDivisionID
	JOIN dbo.CompanyInfo C ON D.numCompanyID = C.numCompanyId
	WHERE OM.tintOppType=1 AND OM.numDomainId=@numDomainID
	AND OBD.dtFromDate BETWEEN  @dtFromDate AND @dtToDate
	AND (OM.numAccountClass=@numAccountClass OR @numAccountClass=0)

	UNION ALL

	SELECT RH.vcRMA,RH.vcBizDocName,RH.dtCreatedDate AS FromDate,C.vcCompanyName,DM.monDepositAmount * -1 AS monDealAMount,0 AS monBizDocAmount,0 AS monAmountPaid 
	FROM dbo.ReturnHeader RH
	JOIN dbo.DepositMaster DM ON RH.numReturnHeaderID=DM.numReturnHeaderID AND DM.numDomainID=@numDomainID
	JOIN dbo.DivisionMaster D ON RH.numDivisionID=D.numDivisionID
	JOIN dbo.CompanyInfo C ON D.numCompanyID = C.numCompanyId
	WHERE RH.tintReceiveType=2 AND  RH.numDomainId=@numDomainID
	AND RH.dtCreatedDate BETWEEN  @dtFromDate AND @dtToDate
	AND (RH.numAccountClass=@numAccountClass OR @numAccountClass=0)
	ORDER BY FromDate

END

ELSE IF @tintReportType=5 --Report Name: Receipt Journal (Summary format)
BEGIN
	
	SELECT COA.vcAccountName,SUM(ISNULL(GJD.numCreditAmt,0)) AS numCreditAmt,SUM(ISNULL(GJD.numDebitAmt,0)) AS numDebitAmt 
	FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE ISNULL(GJH.numDepositId,0)>0 AND GJH.numDomainId=@numDomainID
	AND GJH.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate
	AND (GJH.numAccountClass=@numAccountClass OR @numAccountClass=0)
	GROUP BY GJD.numChartAcntId,COA.vcAccountName 
	ORDER BY COA.vcAccountName

END

ELSE IF @tintReportType=6 --Report Name: Total Receipts (Detailed)
BEGIN

	SELECT DM.dtDepositDate AS datEntry_Date,COA.vcAccountName,GJD.varDescription AS Description,numDebitAmt,numCreditAmt,
	DM.numDepositId AS numReference,GJD.tintReferenceType
	FROM dbo.DepositMaster DM JOIN dbo.General_Journal_Header GJH ON DM.numDepositId=GJH.numDepositId
	JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId AND GJD.tintReferenceType=6
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE DM.numDomainID=@numDomainID /*AND DM.tintDepositeToType=2 AND DM.tintDepositePage=2*/
	AND DM.dtDepositDate BETWEEN  @dtFromDate AND @dtToDate
	AND (DM.numAccountClass=@numAccountClass OR @numAccountClass=0)
	
	UNION ALL
	SELECT DM.dtDepositDate AS datEntry_Date,COA.vcAccountName,C.vcCompanyName AS Description,numDebitAmt,numCreditAmt,
	DM.numDepositId AS numReference,GJD.tintReferenceType
	FROM dbo.DepositMaster DM JOIN dbo.General_Journal_Header GJH ON DM.numDepositId=GJH.numDepositId
	JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId AND GJD.tintReferenceType=7
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	LEFT JOIN dbo.DivisionMaster D ON GJD.numCustomerId=D.numDivisionID
	LEFT JOIN dbo.CompanyInfo C ON D.numCompanyID = C.numCompanyId
	WHERE DM.numDomainID=@numDomainID /*AND DM.tintDepositeToType=2 AND DM.tintDepositePage=2*/
	AND DM.dtDepositDate BETWEEN  @dtFromDate AND @dtToDate
	AND (DM.numAccountClass=@numAccountClass OR @numAccountClass=0)
	
	UNION ALL
	SELECT DM.dtDepositDate AS datEntry_Date,NULL,OM.vcPOppName + ' : ' + OBD.vcbizdocid + ' (' + CAST(DD.monAmountPaid AS VARCHAR(20)) +')',NULL,NULL,
	DM.numDepositId AS numReference,8 AS tintReferenceType
	FROM dbo.DepositMaster DM JOIN dbo.DepositeDetails DD ON DM.numDepositId = DD.numDepositID 
	JOIN dbo.OpportunityBizDocs OBD ON DD.numOppBizDocsID = OBD.numOppBizDocsId
	JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId AND DD.numOppID=OM.numOppId
	WHERE DM.numDomainID=@numDomainID /*AND DM.tintDepositeToType=2 AND DM.tintDepositePage=2*/
	AND DM.dtDepositDate BETWEEN  @dtFromDate AND @dtToDate
	AND (DM.numAccountClass=@numAccountClass OR @numAccountClass=0)
	ORDER BY datEntry_Date,numReference,tintReferenceType	
	
END

ELSE IF @tintReportType=7 --Report Name: Total Disbursement Journal (Summary)
BEGIN

	SELECT COA.vcAccountName,SUM(ISNULL(GJD.numCreditAmt,0)) AS numCreditAmt,SUM(ISNULL(GJD.numDebitAmt,0)) AS numDebitAmt 
	FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE (ISNULL(GJH.numBillPaymentID,0)>0 OR ISNULL(GJH.numCheckHeaderID,0)>0) AND GJH.numDomainId=@numDomainID 
	AND GJH.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate
	AND (GJH.numAccountClass=@numAccountClass OR @numAccountClass=0)
	GROUP BY GJD.numChartAcntId,COA.vcAccountName 
	ORDER BY COA.vcAccountName
	
END

ELSE IF @tintReportType=8 --Report Name: Cash disbursement Journal (Detailed)
BEGIN

	SELECT ISNULL(GJH.numCheckHeaderID,0) AS numCheckHeaderID,ISNULL(GJH.numBillPaymentID,0) AS numBillPaymentID,
	GJH.datEntry_Date,COA.vcAccountName,numDebitAmt,numCreditAmt,GJD.tintReferenceType INTO #tempGJ 
	FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE GJH.numDomainID=@numDomainID AND GJH.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate
	AND (ISNULL(GJH.numCheckHeaderID,0)>0 OR ISNULL(GJH.numBillPaymentID,0)>0)
	AND (GJH.numAccountClass=@numAccountClass OR @numAccountClass=0)
	
	SELECT GJH.datEntry_Date,CH.numCheckNo,GJH.vcAccountName,'' AS Description,numDebitAmt,numCreditAmt,CH.numCheckHeaderID AS numReference,GJH.tintReferenceType
	FROM dbo.CheckHeader CH JOIN #tempGJ GJH ON CH.numCheckHeaderID=GJH.numCheckHeaderID AND GJH.tintReferenceType=1
	WHERE CH.numDomainID=@numDomainID AND CH.tintReferenceType=1
	AND (CH.numAccountClass=@numAccountClass OR @numAccountClass=0)
		
	UNION ALL
	SELECT GJH.datEntry_Date AS datEntry_Date,NULL,GJH.vcAccountName,C.vcCompanyName AS Description,numDebitAmt,numCreditAmt,CH.numCheckHeaderID AS numReference,GJH.tintReferenceType
	FROM dbo.CheckHeader CH JOIN #tempGJ GJH ON CH.numCheckHeaderID=GJH.numCheckHeaderID AND GJH.tintReferenceType=2
	JOIN dbo.DivisionMaster D ON CH.numDivisionID=D.numDivisionID
	JOIN dbo.CompanyInfo C ON D.numCompanyID = C.numCompanyId
	WHERE CH.numDomainID=@numDomainID AND CH.tintReferenceType=1
	AND (CH.numAccountClass=@numAccountClass OR @numAccountClass=0)
	
	UNION ALL
	SELECT BPH.dtPaymentDate AS datEntry_Date,CH.numCheckNo,GJH.vcAccountName,'' AS Description,numDebitAmt,numCreditAmt,BPH.numBillPaymentID,GJH.tintReferenceType
	FROM dbo.BillPaymentHeader BPH JOIN #tempGJ GJH ON BPH.numBillPaymentID=GJH.numBillPaymentID AND GJH.tintReferenceType=8
	LEFT JOIN dbo.CheckHeader CH ON CH.numReferenceID=BPH.numBillPaymentID AND CH.tintReferenceType=8
	WHERE BPH.numDomainID=@numDomainID 
	AND BPH.dtPaymentDate BETWEEN  @dtFromDate AND @dtToDate
	AND (BPH.numAccountClass=@numAccountClass OR @numAccountClass=0)
	
	UNION ALL
	SELECT BPH.dtPaymentDate AS datEntry_Date,NULL AS numCheckNo,GJH.vcAccountName,C.vcCompanyName AS Description,numDebitAmt,numCreditAmt,BPH.numBillPaymentID,GJH.tintReferenceType
	FROM dbo.BillPaymentHeader BPH JOIN #tempGJ GJH ON BPH.numBillPaymentID=GJH.numBillPaymentID AND GJH.tintReferenceType=9
	JOIN dbo.DivisionMaster D ON BPH.numDivisionID=D.numDivisionID
	JOIN dbo.CompanyInfo C ON D.numCompanyID = C.numCompanyId
	WHERE BPH.numDomainID=@numDomainID
	AND BPH.dtPaymentDate BETWEEN  @dtFromDate AND @dtToDate
	AND (BPH.numAccountClass=@numAccountClass OR @numAccountClass=0)
	
	UNION ALL
	SELECT BPH.dtPaymentDate AS datEntry_Date,NULL,NULL,OM.vcPOppName + ' : ' + OBD.vcbizdocid + ' (' + CAST(BPD.monAmount AS VARCHAR(20)) +')'
	,NULL,NULL,BPH.numBillPaymentID,10
	FROM dbo.BillPaymentHeader BPH JOIN dbo.BillPaymentDetails BPD ON BPH.numBillPaymentID=BPD.numBillPaymentID
	JOIN dbo.OpportunityBizDocs OBD ON BPD.numOppBizDocsID = OBD.numOppBizDocsId
	JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
	WHERE BPH.numDomainID=@numDomainID 
	AND BPH.dtPaymentDate BETWEEN  @dtFromDate AND @dtToDate
	AND (BPH.numAccountClass=@numAccountClass OR @numAccountClass=0)
	
	UNION ALL
	SELECT BPH.dtPaymentDate AS datEntry_Date,NULL,NULL,COA.vcAccountName + '(' + CAST(GJD.numDebitAmt AS VARCHAR(20)) + ' : Bill-' + CONVERT(VARCHAR(10),BH.numBillID)+')'
	,NULL,NULL,BPH.numBillPaymentID,10
	FROM dbo.BillPaymentHeader BPH JOIN dbo.BillPaymentDetails BPD ON BPH.numBillPaymentID=BPD.numBillPaymentID
	JOIN dbo.BillHeader BH ON BH.numBillID = BPD.numBillID
	JOIN dbo.General_Journal_Header GJH ON BH.numBillID=GJH.numBillID JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE BPH.numDomainID=@numDomainID AND GJD.numDebitAmt>0
	AND BPH.dtPaymentDate BETWEEN  @dtFromDate AND @dtToDate
	AND (BH.numAccountClass=@numAccountClass OR @numAccountClass=0)
	
	ORDER BY datEntry_Date,numReference,tintReferenceType
	
	DROP TABLE #tempGJ
END

ELSE IF @tintReportType=9 --Report Name: Credit Card Register
BEGIN
    SELECT  BPH.[dtPaymentDate] ,
            BPH.[monPaymentAmount] ,
			[BPH].[monAppliedAmount],
            BPH.numDivisionID ,
            C.vcCompanyName ,
            COA.vcAccountName
    FROM    dbo.[BillPaymentHeader] AS BPH
            --JOIN [dbo].[BillPaymentDetails] AS BPD ON BPH.[numBillPaymentID] = BPD.[numBillPaymentID]
            JOIN dbo.DivisionMaster D ON BPH.numDivisionID = D.numDivisionID
            JOIN dbo.CompanyInfo C ON D.numCompanyID = C.numCompanyId
            JOIN dbo.Chart_Of_Accounts COA ON BPH.[numAccountID] = COA.numAccountId
                                              AND COA.numDomainId = @numDomainID
    WHERE   BPH.numDomainID = @numDomainID
            AND BPH.[dtPaymentDate] BETWEEN @dtFromDate
                                    AND     @dtToDate
            AND ( BPH.numAccountClass = @numAccountClass
                  OR @numAccountClass = 0
                )
			AND ISNULL([BPH].[numPaymentMethod],0) = 1
    ORDER BY [BPH].[dtPaymentDate]

END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_DeleteSalesReturnDetails' ) 
    DROP PROCEDURE USP_DeleteSalesReturnDetails
GO

CREATE PROCEDURE [dbo].[USP_DeleteSalesReturnDetails]
    (
      @numReturnHeaderID NUMERIC(9),
      @numReturnStatus	NUMERIC(18,0),
      @numDomainId NUMERIC(9),
      @numUserCntID NUMERIC(9)
    )
AS 
    BEGIN
    DECLARE @tintReturnType TINYINT,@tintReceiveType TINYINT
    
    SELECT @tintReturnType=tintReturnType,@tintReceiveType=tintReceiveType FROM ReturnHeader WHERE numReturnHeaderID = @numReturnHeaderID    
   
     IF (@tintReturnType=1 AND @tintReceiveType=2) OR @tintReturnType=3
     BEGIN
		IF EXISTS(SELECT numDepositID FROM DepositMaster WHERE tintDepositePage=3 AND numReturnHeaderID=@numReturnHeaderID AND ISNULL(monAppliedAmount,0)>0)
		BEGIN
			RAISERROR ( 'CreditMemo_PAID', 16, 1 ) ;
			RETURN ;
		END	
     END
     
     IF (@tintReturnType=2 AND @tintReceiveType=2) OR @tintReturnType=3
     BEGIN
		IF EXISTS(SELECT numReturnHeaderID FROM BillPaymentHeader WHERE numReturnHeaderID=@numReturnHeaderID AND ISNULL(monAppliedAmount,0)>0)
		BEGIN
			RAISERROR ( 'CreditMemo_PAID', 16, 1 ) ;
			RETURN ;
		END	
     END
     
     IF (@tintReturnType=1 or @tintReturnType=2) 
     BEGIN
		IF EXISTS(SELECT numReturnHeaderID FROM OppWarehouseSerializedItem WHERE ISNULL(numOppID,0)>0 AND ISNULL(numOppBizDocsId,0)>0 
			AND numWareHouseItmsDTLID IN (SELECT numWareHouseItmsDTLID FROM OppWarehouseSerializedItem 
			WHERE numReturnHeaderID=@numReturnHeaderID))
		BEGIN
			RAISERROR ( 'Serial_LotNo_Used', 16, 1 ) ;
			RETURN ;
		END	
		ELSE
		BEGIN
			  UPDATE WareHouseItmsDTL SET tintStatus=(CASE WHEN @tintReturnType=1 THEN 3 ELSE tintStatus END)
				WHERE numWareHouseItmsDTLID IN (SELECT numWareHouseItmsDTLID FROM OppWarehouseSerializedItem WHERE numReturnHeaderID=@numReturnHeaderID)
		END
     END
	
	 IF @tintReturnType=1 OR @tintReturnType=2 
        BEGIN
			EXEC usp_ManageRMAInventory @numReturnHeaderID,@numDomainId,@numUserCntID,2
        END
         	              
        UPDATE  dbo.ReturnHeader
        SET     numReturnStatus = @numReturnStatus,
                numBizdocTempID = CASE WHEN @tintReturnType=1 OR @tintReturnType=2 THEN NULL ELSE numBizdocTempID END,
                vcBizDocName =CASE WHEN @tintReturnType=1 OR @tintReturnType=2 THEN '' ELSE vcBizDocName END ,
                IsCreateRefundReceipt = 0,
                tintReceiveType=0,monBizDocAmount=0, numItemCode = 0
        WHERE   numReturnHeaderID = @numReturnHeaderID
         AND	numDomainId = @numDomainId
    END
    
     IF (@tintReturnType=1 AND @tintReceiveType=2) OR @tintReturnType=3
     BEGIN
		DELETE FROM DepositMaster WHERE tintDepositePage=3 AND numReturnHeaderID=@numReturnHeaderID
	 END	
     
     IF (@tintReturnType=2 AND @tintReceiveType=2) OR @tintReturnType=3
     BEGIN
		DELETE FROM BillPaymentHeader WHERE numReturnHeaderID=@numReturnHeaderID
     END
    
	PRINT @tintReceiveType 
	PRINT @tintReturnType

	 IF @tintReceiveType = 1 AND @tintReturnType = 4
	 BEGIN
	
		DECLARE @numDepositIDRef AS NUMERIC(18,0)
		SELECT @numDepositIDRef = ISNULL(numDepositIDRef,0) FROM [dbo].[ReturnHeader] AS RH WHERE [RH].[numReturnHeaderID] = @numReturnHeaderID
		PRINT @numDepositIDRef

		DECLARE @monAppliedAmount AS MONEY
		SELECT @monAppliedAmount = ISNULL([RH].[monAmount],0)
		FROM [dbo].[ReturnHeader] AS RH 
		WHERE [RH].[numReturnHeaderID] = @numReturnHeaderID 
		AND [RH].[numDomainId] = @numDomainId

		PRINT 1
		IF @numDepositIDRef > 0			
			BEGIN
			PRINT 2
				UPDATE [dbo].[DepositMaster] SET [monAppliedAmount] = ISNULL([monAppliedAmount],0) - @monAppliedAmount
												 ,[monRefundAmount] = [monDepositAmount] - ISNULL((SELECT SUM([ReturnHeader].[monAmount]) FROM [dbo].[ReturnHeader] WHERE [ReturnHeader].[numDepositIDRef] = @numDepositIDRef),0)
												 ,[numReturnHeaderID] = NULL
				WHERE [numDepositId] = @numDepositIDRef AND [numDomainId] = @numDomainId
				--DELETE FROM [dbo].[DepositeDetails] WHERE [DepositeDetails].[numDepositID] = @numDepositIDRef
				--DELETE FROM [dbo].[DepositMaster] WHERE [numDepositId] = @numDepositIDRef AND [numDomainId] = @numDomainId
			END
		PRINT 3		

	--	DECLARE @numBillPaymentIDRef AS NUMERIC(18,0)
	--	SELECT @numBillPaymentIDRef = ISNULL(numBillPaymentIDRef,0) FROM [dbo].[ReturnHeader] AS RH WHERE [RH].[numReturnHeaderID] = @numReturnHeaderID
	--	PRINT @numBillPaymentIDRef

	--	IF @numBillPaymentIDRef > 0			
	--		BEGIN
	--			--UPDATE [dbo].[BillPaymentHeader] SET [monAppliedAmount] = ISNULL([monAppliedAmount],0) - @monAppliedAmount, 
	--			--									 [monRefundAmount] = ISNULL([monAppliedAmount],0) - ISNULL([monRefundAmount],0) 
	--			--WHERE [BillPaymentHeader].[numBillPaymentID] = @numBillPaymentIDRef AND [numDomainId] = @numDomainId
	--			DELETE FROM [dbo].[BillPaymentDetails] WHERE [BillPaymentDetails].[numBillPaymentID] = @numBillPaymentIDRef
	--			DELETE FROM [dbo].[BillPaymentHeader] WHERE [BillPaymentHeader].[numBillPaymentID] = @numBillPaymentIDRef AND [numDomainId] = @numDomainId
	--		END

	--	DELETE FROM [dbo].[CheckHeader] WHERE [CheckHeader].[numReferenceID] = @numReturnHeaderID
	END
	
	--DELETE FROM [dbo].[ReturnHeader] WHERE [ReturnHeader].[numReturnHeaderID] = @numReturnHeaderID
	DELETE FROM [dbo].[CheckHeader] WHERE [CheckHeader].[numReferenceID] = @numReturnHeaderID
    DELETE FROM dbo.General_Journal_Details WHERE numJournalId IN (SELECT numJournal_Id FROM General_Journal_Header WHERE numReturnID=@numReturnHeaderID AND [General_Journal_Header].[numDomainId] = @numDomainId)
	AND [General_Journal_Details].[numDomainId] = @numDomainId
    DELETE FROM dbo.General_Journal_Header WHERE numReturnID=@numReturnHeaderID AND [numDomainId] = @numDomainId

GO
/****** Object:  StoredProcedure [dbo].[USP_EcommerceSettings]    Script Date: 03/25/2009 16:46:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ecommercesettings')
DROP PROCEDURE usp_ecommercesettings
GO
CREATE PROCEDURE [dbo].[USP_EcommerceSettings]              
@numDomainID as numeric(9),              
@vcPaymentGateWay as int ,                
@vcPGWUserId as varchar(100)='' ,                
@vcPGWPassword as varchar(100),              
@bitShowInStock as bit,              
@bitShowQOnHand as bit,        
--@tintColumns as tinyint,      
--@numCategory as numeric(9),
--@bitHideNewUsers as bit,
@bitCheckCreditStatus as BIT,
@numDefaultWareHouseID as numeric(9),
@numDefaultIncomeChartAcntId as numeric(9),
@numDefaultAssetChartAcntId as numeric(9),
@numDefaultCOGSChartAcntId as numeric(9),
@numRelationshipId as numeric(9),
@numProfileId as numeric(9),
@bitEnableDefaultAccounts BIT,
--@bitEnableCreditCart	BIT,
@bitHidePriceBeforeLogin	BIT,
@bitHidePriceAfterLogin	BIT,
@numRelationshipIdHidePrice	numeric(18, 0),
@numProfileIDHidePrice	numeric(18, 0),
--@numBizDocForCreditCard numeric(18, 0),
--@numBizDocForCreditTerm numeric(18, 0),
@bitAuthOnlyCreditCard BIT=0,
@bitSendMail BIT = 1,
@vcGoogleMerchantID VARCHAR(1000)  = '',
@vcGoogleMerchantKey VARCHAR(1000) = '',
@IsSandbox BIT ,
@numAuthrizedOrderStatus NUMERIC,
@numSiteID numeric(18, 0),
@vcPaypalUserName VARCHAR(50)  = '',
@vcPaypalPassword VARCHAR(50)  = '',
@vcPaypalSignature VARCHAR(500)  = '',
@IsPaypalSandbox BIT,
@bitSkipStep2 BIT = 0,
@bitDisplayCategory BIT = 0,
@bitShowPriceUsingPriceLevel BIT = 0,
@bitEnableSecSorting BIT = 0,
@bitSortPriceMode   BIT=0,
@numPageSize    INT=15,
@numPageVariant  INT=6,
@bitAutoSelectWarehouse BIT =0 
as              
            
            
if not exists(select 'col1' from eCommerceDTL where numDomainID=@numDomainID AND [numSiteId] = @numSiteID)            
INSERT INTO eCommerceDTL
           (numDomainId,
            vcPaymentGateWay,
            vcPGWManagerUId,
            vcPGWManagerPassword,
            bitShowInStock,
            bitShowQOnHand,
            --tintItemColumns,
           -- numDefaultCategory,
            numDefaultWareHouseID,
            numDefaultIncomeChartAcntId,
            numDefaultAssetChartAcntId,
            numDefaultCOGSChartAcntId,
            --bitHideNewUsers,
            bitCheckCreditStatus,
            [numRelationshipId],
            [numProfileId],
            [bitEnableDefaultAccounts],
 --           [bitEnableCreditCart],
            [bitHidePriceBeforeLogin],
            [bitHidePriceAfterLogin],
            [numRelationshipIdHidePrice],
            [numProfileIDHidePrice],
--            numBizDocForCreditCard,
--            numBizDocForCreditTerm,
            bitAuthOnlyCreditCard,
--            numAuthrizedOrderStatus,
            bitSendEmail,
            vcGoogleMerchantID,
            vcGoogleMerchantKey,
            IsSandbox ,
            numAuthrizedOrderStatus,
			numSiteId,
			vcPaypalUserName,
			vcPaypalPassword,
			vcPaypalSignature,
			IsPaypalSandbox,
			bitSkipStep2,
			bitDisplayCategory,
			bitShowPriceUsingPriceLevel,
			bitEnableSecSorting,
			bitSortPriceMode,
			numPageSize,
			numPageVariant,
			bitAutoSelectWarehouse
			)

VALUES     (@numDomainID,
            @vcPaymentGateWay,
            @vcPGWUserId,
            @vcPGWPassword,
            @bitShowInStock,
            @bitShowQOnHand,
           -- @tintColumns,
           -- @numCategory,
            @numDefaultWareHouseID,
            @numDefaultIncomeChartAcntId,
            @numDefaultAssetChartAcntId,
            @numDefaultCOGSChartAcntId,
           -- @bitHideNewUsers,
            @bitCheckCreditStatus,
            @numRelationshipId,
            @numProfileId,
            @bitEnableDefaultAccounts,
 --           @bitEnableCreditCart,
            @bitHidePriceBeforeLogin,
            @bitHidePriceAfterLogin,
            @numRelationshipIdHidePrice,
            @numProfileIDHidePrice,
--            @numBizDocForCreditCard,
--            @numBizDocForCreditTerm,
            @bitAuthOnlyCreditCard,
--            @numAuthrizedOrderStatus,
            @bitSendMail,
            @vcGoogleMerchantID,
            @vcGoogleMerchantKey ,
            @IsSandbox,
            @numAuthrizedOrderStatus,
			@numSiteID,
			@vcPaypalUserName,
			@vcPaypalPassword,
			@vcPaypalSignature,
			@IsPaypalSandbox,
			@bitSkipStep2,
			@bitDisplayCategory,
			@bitShowPriceUsingPriceLevel,
			@bitEnableSecSorting,
			@bitSortPriceMode,
			@numPageSize,
			@numPageVariant,
			@bitAutoSelectWarehouse
            )
 else            
 update eCommerceDTL                                   
   set              
 vcPaymentGateWay=@vcPaymentGateWay,                
 vcPGWManagerUId=@vcPGWUserId ,                
 vcPGWManagerPassword= @vcPGWPassword,              
 bitShowInStock=@bitShowInStock ,               
 bitShowQOnHand=@bitShowQOnHand,        
 --tintItemColumns= @tintColumns,      
 --numDefaultCategory=@numCategory,
 numDefaultWareHouseID =@numDefaultWareHouseID,
 numDefaultIncomeChartAcntId =@numDefaultIncomeChartAcntId ,
 numDefaultAssetChartAcntId =@numDefaultAssetChartAcntId ,
 numDefaultCOGSChartAcntId =@numDefaultCOGSChartAcntId ,
 --bitHideNewUsers =@bitHideNewUsers,
 bitCheckCreditStatus=@bitCheckCreditStatus ,
 numRelationshipId = @numRelationshipId,
 numProfileId = @numProfileId,
 bitEnableDefaultAccounts = @bitEnableDefaultAccounts,
 --[bitEnableCreditCart]=@bitEnableCreditCart,
 [bitHidePriceBeforeLogin]=@bitHidePriceBeforeLogin,
 [bitHidePriceAfterLogin]=@bitHidePriceAfterLogin,
 [numRelationshipIdHidePrice]=@numRelationshipIdHidePrice,
 [numProfileIDHidePrice]=@numProfileIDHidePrice,
-- numBizDocForCreditCard=@numBizDocForCreditCard,
-- numBizDocForCreditTerm=@numBizDocForCreditTerm,
 bitAuthOnlyCreditCard=@bitAuthOnlyCreditCard,
-- numAuthrizedOrderStatus=@numAuthrizedOrderStatus,
 bitSendEmail = @bitSendMail,
 vcGoogleMerchantID = @vcGoogleMerchantID ,
 vcGoogleMerchantKey =  @vcGoogleMerchantKey ,
 IsSandbox = @IsSandbox,
 numAuthrizedOrderStatus = @numAuthrizedOrderStatus,
	numSiteId = @numSiteID,
vcPaypalUserName = @vcPaypalUserName ,
vcPaypalPassword = @vcPaypalPassword,
vcPaypalSignature = @vcPaypalSignature,
IsPaypalSandbox = @IsPaypalSandbox,
bitSkipStep2 = @bitSkipStep2,
bitDisplayCategory = @bitDisplayCategory,
bitShowPriceUsingPriceLevel = @bitShowPriceUsingPriceLevel,
bitEnableSecSorting = @bitEnableSecSorting,
bitSortPriceMode=@bitSortPriceMode,
numPageSize=@numPageSize,
numPageVariant=@numPageVariant,
bitAutoSelectWarehouse=@bitAutoSelectWarehouse
 where numDomainId=@numDomainID AND [numSiteId] = @numSiteID

GO
/****** Object:  StoredProcedure [dbo].[USP_GetAccountReceivableAging]    Script Date: 09/01/2009 00:40:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SELECT * FROM domain
--Created By Siva
-- exec [dbo].[USP_GetAccountReceivableAging] 72,null,1
/*Note By-Chintan 
Some places we have used 
DATEADD(DAY, 0, DATEDIFF(DAY,0,GETUTCDATE()))
insted of GETUTCDATE()
Reson is the difference in result repectively
"2009-03-05 00:00:00.000"	"2009-03-05 09:38:02.560"
There WE are comparing UTCDate without Time.
Created function for same thing dbo.GetUTCDateWithoutTime()
*/
-- EXEC usp_getaccountreceivableaging 169,0,1,0,'2001-01-01 00:00:00.000', '2014-12-31 23:59:59:999'
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[USP_GetAccountReceivableAging]
(
               @numDomainId   AS NUMERIC(9)  = 0,
               @numDivisionId AS NUMERIC(9)  = NULL,
               @numUserCntID AS NUMERIC(9),
               @numAccountClass AS NUMERIC(9)=0,
			   @dtFromDate AS DATETIME = NULL,
			   @dtToDate AS DATETIME = NULL
               )
AS
  BEGIN
	SET NOCOUNT ON

	IF @dtFromDate IS NULL
		SET @dtFromDate = '1990-01-01 00:00:00.000'
	IF @dtToDate IS NULL
		SET @dtToDate = GETUTCDATE()

  DECLARE  @AuthoritativeSalesBizDocId  AS INTEGER
    SELECT @AuthoritativeSalesBizDocId = isnull(numAuthoritativeSales,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId = @numDomainId;
  
--SELECT  GETDATE()
INSERT  INTO [TempARRecord]
        (
          numUserCntID,
          [numOppId],
          [numDivisionId],
          [numOppBizDocsId],
          [DealAmount],
          [numBizDocId],
          [numCurrencyID],
          [dtDueDate],AmountPaid
        )
        SELECT  @numUserCntID,
                OB.numOppId,
                Opp.numDivisionId,
                OB.numOppBizDocsId,
                ISNULL(OB.monDealAmount --dbo.GetDealAmount(OB.numOppID,getutcdate(),OB.numOppBizDocsId)
                       * ISNULL(Opp.fltExchangeRate, 1), 0)
                - ISNULL(OB.monAmountPaid * ISNULL(Opp.fltExchangeRate, 1),
                         0) DealAmount,
                OB.numBizDocId,
                ISNULL(Opp.numCurrencyID, 0) numCurrencyID,
--                DATEADD(DAY,CASE WHEN Opp.bitBillingTerms = 1 
--								 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0)) 
--								 ELSE 0 
--							END, dtFromDate) AS dtDueDate,
				DATEADD(DAY,CASE WHEN Opp.bitBillingTerms = 1 
								 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)), 0))
								 ELSE 0 
							END, dtFromDate) AS dtDueDate,
                ISNULL(OB.monAmountPaid * ISNULL(Opp.fltExchangeRate, 1),0) as AmountPaid
        FROM    OpportunityMaster Opp
                JOIN OpportunityBizDocs OB ON OB.numOppId = Opp.numOppID
                LEFT OUTER JOIN [Currency] C ON Opp.[numCurrencyID] = C.[numCurrencyID]
				JOIN [dbo].[DivisionMaster] AS DIV ON DIV.[numDivisionID] = Opp.[numDivisionId]
        WHERE   tintOpptype = 1
                AND tintoppStatus = 1
                --AND dtShippedDate IS NULL --Commented by chintan reason: Account Transaction and AR values was not matching
                AND opp.numDomainID = @numDomainId
                AND numBizDocId = @AuthoritativeSalesBizDocId
                AND OB.bitAuthoritativeBizDocs=1
                AND ISNULL(OB.tintDeferred,0) <> 1
                AND (Opp.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
                            AND (Opp.numAccountClass=@numAccountClass OR @numAccountClass=0)
				AND OB.[dtCreatedDate] BETWEEN @dtFromDate AND @dtToDate
        GROUP BY OB.numOppId,
                Opp.numDivisionId,
                OB.numOppBizDocsId,
                Opp.fltExchangeRate,
                OB.numBizDocId,
                OB.dtCreatedDate,
                Opp.bitBillingTerms,
                Opp.intBillingDays,
                OB.monDealAmount,
                Opp.numCurrencyID,
                OB.dtFromDate,OB.monAmountPaid
        HAVING  ( ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate, 1), 0)
                  - ISNULL(OB.monAmountPaid * ISNULL(Opp.fltExchangeRate, 1),
                           0) ) != 0
                           
	--Show Impact of AR journal entries in report as well 
UNION 
 SELECT @numUserCntID,
        0,
        GJD.numCustomerId,
        0,
        CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END,
        0,
        GJD.[numCurrencyID],
        GJH.datEntry_Date ,0 AS AmountPaid
 FROM   dbo.General_Journal_Header GJH
        INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
                                                      AND GJH.numDomainId = GJD.numDomainId
        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
		JOIN [dbo].[DivisionMaster] AS DIV ON DIV.[numDivisionID] = [GJD].[numCustomerId]
 WHERE  GJH.numDomainId = @numDomainId
        AND COA.vcAccountCode LIKE '0101010501'
        AND ISNULL(numOppId,0)=0 AND ISNULL(numOppBizDocsId,0)=0
        AND (GJD.numCustomerId = @numDivisionId OR @numDivisionId IS NULL)
		AND ISNULL(GJH.numDepositID,0)=0 AND ISNULL(GJH.numReturnID,0)=0
		AND (GJH.numAccountClass=@numAccountClass OR @numAccountClass=0)                            
		AND GJH.[datEntry_Date] BETWEEN @dtFromDate AND @dtToDate
UNION ALL
 SELECT @numUserCntID,
        0,
        DM.numDivisionId,
        0,
        (ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0) /*- ISNULL(monRefundAmount,0)*/) * -1,
        0,
        DM.numCurrencyID,
        DM.dtDepositDate ,0 AS AmountPaid
 FROM   DepositMaster DM 
 JOIN [dbo].[DivisionMaster] AS DIV ON [DM].[numDivisionID] = DIV.[numDivisionID]
 WHERE DM.numDomainId=@numDomainId AND tintDepositePage IN(2,3)  
		AND (DM.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
		AND ((ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0) /*- ISNULL(monRefundAmount,0)*/) > 0)
		AND (DM.numAccountClass=@numAccountClass OR @numAccountClass=0)
		--GROUP BY DM.numDivisionId,DM.dtDepositDate
		AND DM.[dtDepositDate] BETWEEN @dtFromDate AND @dtToDate
UNION ALL --Standalone Refund against Account Receivable
SELECT @numUserCntID,
        0,
        RH.numDivisionId,
        0,
        CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END,
		--(ISNULL(RH.[monBizDocAmount],0) - (CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END)),
        0,
        GJD.[numCurrencyID],
        GJH.datEntry_Date ,0 AS AmountPaid
 FROM   dbo.ReturnHeader RH JOIN dbo.General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
 JOIN [dbo].[DivisionMaster] AS DIV ON DIV.[numDivisionID] = RH.[numDivisionId]
JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId 
JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId AND COA.vcAccountCode LIKE '0101010501'
WHERE RH.tintReturnType=4 AND RH.numDomainId=@numDomainId  
AND ISNULL(RH.numParentID,0) = 0
AND ISNULL(RH.IsUnappliedPayment,0) = 0
		AND (RH.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
		AND (monBizDocAmount > 0) AND ISNULL(RH.numBillPaymentIDRef,0)=0 --AND ISNULL(RH.numDepositIDRef,0) > 0
		AND (RH.numAccountClass=@numAccountClass OR @numAccountClass=0)
		AND GJH.[datEntry_Date]  BETWEEN @dtFromDate AND @dtToDate
		--AND RH.numDivisionId <> 235216
		--AND (ISNULL(RH.[monBizDocAmount],0) - (CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END)) != 0
		--GROUP BY DM.numDivisionId,DM.dtDepositDate

	
INSERT  INTO [TempARRecord]
        (
          numUserCntID,
          [numOppId],
          [numDivisionId],
          [numOppBizDocsId],
          [DealAmount],
          [numBizDocId],
          [numCurrencyID],
          [dtDueDate],AmountPaid,monUnAppliedAmount
        )
SELECT @numUserCntID,0, DM.numDivisionId,0,0,0,0,NULL,0,sum(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0))       
 FROM DepositMaster DM 
 JOIN [dbo].[DivisionMaster] AS DIV ON DIV.[numDivisionID] = DM.[numDivisionID]
 		WHERE DM.numDomainId=@numDomainId AND tintDepositePage IN(2,3)  
		AND (DM.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
		AND ((ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) > 0)
		AND (DM.numAccountClass=@numAccountClass OR @numAccountClass=0)
		AND DM.[dtDepositDate] BETWEEN @dtFromDate AND @dtToDate
		GROUP BY DM.numDivisionId


--SELECT  * FROM    [TempARRecord]


--SELECT  GETDATE()
INSERT  INTO [TempARRecord1] (
	[numUserCntID],
	[numDivisionId],
	[tintCRMType],
	[vcCompanyName],
	[vcCustPhone],
	[numContactId],
	[vcEmail],
	[numPhone],
	[vcContactName],
	[numThirtyDays],
	[numSixtyDays],
	[numNinetyDays],
	[numOverNinetyDays],
	[numThirtyDaysOverDue],
	[numSixtyDaysOverDue],
	[numNinetyDaysOverDue],
	[numOverNinetyDaysOverDue],
	numCompanyID,
	numDomainID,
	[intThirtyDaysCount],
	[intThirtyDaysOverDueCount],
	[intSixtyDaysCount],
	[intSixtyDaysOverDueCount],
	[intNinetyDaysCount],
	[intOverNinetyDaysCount],
	[intNinetyDaysOverDueCount],
	[intOverNinetyDaysOverDueCount],
	[numThirtyDaysPaid],
	[numSixtyDaysPaid],
	[numNinetyDaysPaid],
	[numOverNinetyDaysPaid],
	[numThirtyDaysOverDuePaid],
	[numSixtyDaysOverDuePaid],
	[numNinetyDaysOverDuePaid],
	[numOverNinetyDaysOverDuePaid],monUnAppliedAmount
) 
        SELECT DISTINCT
                @numUserCntID,
                Div.numDivisionID,
                tintCRMType AS tintCRMType,
                vcCompanyName AS vcCompanyName,
                '',
                NULL,
                '',
                '',
                '',
                0 numThirtyDays,
                0 numSixtyDays,
                0 numNinetyDays,
                0 numOverNinetyDays,
                0 numThirtyDaysOverDue,
                0 numSixtyDaysOverDue,
                0 numNinetyDaysOverDue,
                0 numOverNinetyDaysOverDue,
                C.numCompanyID,
				Div.numDomainID,
                0 [intThirtyDaysCount],
				0 [intThirtyDaysOverDueCount],
				0 [intSixtyDaysCount],
				0 [intSixtyDaysOverDueCount],
				0 [intNinetyDaysCount],
				0 [intOverNinetyDaysCount],
				0 [intNinetyDaysOverDueCount],
				0 [intOverNinetyDaysOverDueCount],
				0 numThirtyDaysPaid,
                0 numSixtyDaysPaid,
                0 numNinetyDaysPaid,
                0 numOverNinetyDaysPaid,
                0 numThirtyDaysOverDuePaid,
                0 numSixtyDaysOverDuePaid,
                0 numNinetyDaysOverDuePaid,
                0 numOverNinetyDaysOverDuePaid,0 monUnAppliedAmount
        FROM    Divisionmaster Div
                INNER JOIN CompanyInfo C ON C.numCompanyId = Div.numCompanyID
                    --JOIN [TempARRecord] t ON Div.[numDivisionID] = t.[numDivisionId]
        WHERE   Div.[numDomainID] = @numDomainId
                AND Div.[numDivisionID] IN ( SELECT DISTINCT
                                                    [numDivisionID]
                                             FROM   [TempARRecord] )



--Update Custome Phone 
UPDATE  TempARRecord1
SET     [vcCustPhone] = X.[vcCustPhone]
FROM    ( SELECT 
                            CASE WHEN numPhone IS NULL THEN ''
                                 WHEN numPhone = '' THEN ''
                                 ELSE ', ' + numPhone
                            END AS vcCustPhone,
                            numDivisionId
                  FROM      AdditionalContactsInformation
                  WHERE     bitPrimaryContact = 1
                            AND numDivisionId  IN (SELECT DISTINCT numDivisionID FROM [TempARRecord1] WHERE numUserCntID =@numUserCntID)

        ) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID

--Update Customer info 
UPDATE  TempARRecord1
SET     [numContactId] = X.[numContactID],[vcEmail] = X.[vcEmail],[vcContactName] = X.[vcContactName],numPhone=X.numPhone
FROM    ( SELECT numContactId AS numContactID,
                     ISNULL(vcEmail,'') AS vcEmail,
                     ISNULL(vcFirstname,'') + ' ' + ISNULL(vcLastName,'') AS vcContactName,
                     CAST(ISNULL( CASE WHEN numPhone IS NULL THEN '' WHEN numPhone = '' THEN '' ELSE ', ' + numPhone END,'') AS VARCHAR) AS numPhone,
                     [numDivisionId]
                  FROM      AdditionalContactsInformation
                  WHERE     (vcPosition = 844 or isnull(bitPrimaryContact,0)=1)
                            AND numDivisionId  IN (SELECT DISTINCT numDivisionID FROM [TempARRecord1] WHERE numUserCntID =@numUserCntID)
    ) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID

                    
--SELECT  GETDATE()
------------------------------------------      
DECLARE @baseCurrency AS NUMERIC
SELECT @baseCurrency=numCurrencyID FROM dbo.Domain WHERE numDomainId=@numDomainId
/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */
------------------------------------------
UPDATE  TempARRecord1
SET     numThirtyDays = X.numThirtyDays
FROM    ( SELECT    SUM(DealAmount) AS numThirtyDays,
                    [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND
		DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 0 AND 30	
          GROUP BY  numDivisionId
        ) X WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numThirtyDaysPaid = X.numThirtyDaysPaid
FROM    ( SELECT    SUM(AmountPaid) AS numThirtyDaysPaid,
                    [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND
		DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 0 AND 30	
          GROUP BY  numDivisionId
        ) X WHERE   TempARRecord1.numDivisionID = X.numDivisionID

                
UPDATE  TempARRecord1 SET intThirtyDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 0 AND 30	
                                                AND numCurrencyID <>@baseCurrency )
------------------------------------------
UPDATE  TempARRecord1
SET     numSixtyDays = X.numSixtyDays
FROM    ( SELECT  SUM(DealAmount) AS numSixtyDays,
        numDivisionId
FROM    [TempARRecord]
WHERE   numUserCntID =@numUserCntID AND 
	DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 31 AND 60	
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numSixtyDaysPaid = X.numSixtyDaysPaid
FROM    ( SELECT  SUM(AmountPaid) AS numSixtyDaysPaid,
        numDivisionId
FROM    [TempARRecord]
WHERE   numUserCntID =@numUserCntID AND 
	DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 31 AND 60	
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intSixtyDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 31 AND 60
		AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                                                AND numCurrencyID <>@baseCurrency )
------------------------------------------
UPDATE  TempARRecord1
SET     numNinetyDays = X.numNinetyDays
FROM    (SELECT  SUM(DealAmount) AS numNinetyDays,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 61 AND 90
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numNinetyDaysPaid = X.numNinetyDaysPaid
FROM    (SELECT  SUM(AmountPaid) AS numNinetyDaysPaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 61 AND 90
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intNinetyDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 61 AND 90
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                                                AND numCurrencyID <>@baseCurrency )
------------------------------------------
UPDATE  TempARRecord1
SET     numOverNinetyDays = X.numOverNinetyDays
FROM    (SELECT  SUM(DealAmount) AS numOverNinetyDays,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND [dtDueDate] >= DATEADD(DAY, 91,
                                                    dbo.GetUTCDateWithoutTime())
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numOverNinetyDaysPaid = X.numOverNinetyDaysPaid
FROM    (SELECT  SUM(AmountPaid) AS numOverNinetyDaysPaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND [dtDueDate] >= DATEADD(DAY, 91,
                                                    dbo.GetUTCDateWithoutTime())
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intOverNinetyDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND [dtDueDate] >= DATEADD(DAY, 91,
                                                    dbo.GetUTCDateWithoutTime())
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                                                AND numCurrencyID <>@baseCurrency )

------------------------------------------
UPDATE  TempARRecord1
SET     numThirtyDaysOverDue = X.numThirtyDaysOverDue
FROM    (SELECT  SUM(DealAmount) AS numThirtyDaysOverDue,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numThirtyDaysOverDuePaid = X.numThirtyDaysOverDuePaid
FROM    (SELECT  SUM(AmountPaid) AS numThirtyDaysOverDuePaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intThirtyDaysOverDueCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30
                                                AND numCurrencyID <>@baseCurrency )

----------------------------------
UPDATE  TempARRecord1
SET     numSixtyDaysOverDue = X.numSixtyDaysOverDue
FROM    ( SELECT    SUM(DealAmount) AS numSixtyDaysOverDue,
                    numDivisionId
          FROM      TempARRecord
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                    AND DATEDIFF(DAY,
                                 [dtDueDate],
                                 dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                              AND     60
          GROUP BY  numDivisionId
        ) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID

UPDATE  TempARRecord1
SET     numSixtyDaysOverDuePaid = X.numSixtyDaysOverDuePaid
FROM    ( SELECT    SUM(AmountPaid) AS numSixtyDaysOverDuePaid,
                    numDivisionId
          FROM      TempARRecord
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                    AND DATEDIFF(DAY,
                                 [dtDueDate],
                                 dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                              AND     60
          GROUP BY  numDivisionId
        ) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intSixtyDaysOverDueCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                    AND DATEDIFF(DAY,
                                 [dtDueDate],
                                 dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                              AND     60
                                                AND numCurrencyID <>@baseCurrency )

------------------------------------------
UPDATE  TempARRecord1
SET     numNinetyDaysOverDue = X.numNinetyDaysOverDue
FROM    (SELECT  SUM(DealAmount) AS numNinetyDaysOverDue,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numNinetyDaysOverDuePaid = X.numNinetyDaysOverDuePaid
FROM    (SELECT  SUM(AmountPaid) AS numNinetyDaysOverDuePaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intNinetyDaysOverDueCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90
                                                AND numCurrencyID <>@baseCurrency )
------------------------------------------
UPDATE  TempARRecord1
SET     numOverNinetyDaysOverDue = X.numOverNinetyDaysOverDue
FROM    (SELECT  SUM(DealAmount) AS numOverNinetyDaysOverDue,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY, [dtDueDate], dbo.GetUTCDateWithoutTime()) > 90
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numOverNinetyDaysOverDuePaid = X.numOverNinetyDaysOverDuePaid
FROM    (SELECT  SUM(AmountPaid) AS numOverNinetyDaysOverDuePaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY, [dtDueDate], dbo.GetUTCDateWithoutTime()) > 90
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intOverNinetyDaysOverDueCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY, [dtDueDate], dbo.GetUTCDateWithoutTime()) > 90
                                                AND numCurrencyID <>@baseCurrency )


UPDATE  TempARRecord1
SET     monUnAppliedAmount = ISNULL(X.monUnAppliedAmount,0)
FROM    (SELECT  SUM(ISNULL(monUnAppliedAmount,0)) AS monUnAppliedAmount,numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID 
GROUP BY numDivisionId
) X WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET numTotal=isnull(numThirtyDays,0)
      + isnull(numThirtyDaysOverDue,0)
      + isnull(numSixtyDays,0)
      + isnull(numSixtyDaysOverDue,0)
      + isnull(numNinetyDays,0)
      + isnull(numNinetyDaysOverDue,0)
      + isnull(numOverNinetyDays,0)
      + isnull(numOverNinetyDaysOverDue,0) /*- ISNULL(monUnAppliedAmount,0) */
           
SELECT  
		[numDivisionId],
		[numContactId],
		[tintCRMType],
		[vcCompanyName],
		[vcCustPhone],
		[vcContactName],
		[vcEmail],
		[numPhone],
		[numThirtyDays],
		[numSixtyDays],
		[numNinetyDays],
		[numOverNinetyDays],
		[numThirtyDaysOverDue],
		[numSixtyDaysOverDue],
		[numNinetyDaysOverDue],
		[numOverNinetyDaysOverDue],
--		CASE WHEN (ISNULL(numTotal,0) - ISNULL(monUnAppliedAmount,0))>=0
--		 THEN ISNULL(numTotal,0) - ISNULL(monUnAppliedAmount,0) ELSE ISNULL(numTotal,0) END AS numTotal ,
		ISNULL(numTotal,0) AS numTotal,
		numCompanyID,
		numDomainID,
		[intThirtyDaysCount],
		[intThirtyDaysOverDueCount],
		[intSixtyDaysCount],
		[intSixtyDaysOverDueCount],
		[intNinetyDaysCount],
		[intOverNinetyDaysCount],
		[intNinetyDaysOverDueCount],
		[intOverNinetyDaysOverDueCount],
		[numThirtyDaysPaid],
		[numSixtyDaysPaid],
		[numNinetyDaysPaid],
		[numOverNinetyDaysPaid],
		[numThirtyDaysOverDuePaid],
		[numSixtyDaysOverDuePaid],
		[numNinetyDaysOverDuePaid],
		[numOverNinetyDaysOverDuePaid],ISNULL(monUnAppliedAmount,0) AS monUnAppliedAmount
FROM    TempARRecord1 WHERE numUserCntID =@numUserCntID AND ISNULL(numTotal,0)!=0
ORDER BY [vcCompanyName] 
DELETE  FROM TempARRecord1 WHERE   numUserCntID = @numUserCntID ;
DELETE  FROM TempARRecord WHERE   numUserCntID = @numUserCntID ;

SET NOCOUNT OFF   
    
  END
  



/****** Object:  StoredProcedure [dbo].[usp_GetAllSelectedSystemModulesPagesAndAccesses]    Script Date: 07/26/2008 16:16:13 ******/
SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetECommerceDetails')
DROP PROCEDURE USP_GetECommerceDetails
GO
CREATE PROCEDURE [dbo].[USP_GetECommerceDetails]      
@numDomainID as numeric(9)=0,
@numSiteID AS INTEGER = 0  
as                              
select                               
isnull(vcPaymentGateWay,'0') as vcPaymentGateWay,                
isnull(vcPGWManagerUId,'')as vcPGWManagerUId ,                
isnull(vcPGWManagerPassword,'') as vcPGWManagerPassword,              
isnull(bitShowInStock,0) as bitShowInStock,              
isnull(bitShowQOnHand,0) as bitShowQOnHand,        
--isnull(tintItemColumns,0) as tintItemColumns,
--isnull(numDefaultCategory,0) as numDefaultCategory,  
isnull([numDefaultWareHouseID],0) numDefaultWareHouseID,
isnull([numDefaultIncomeChartAcntId],0) numDefaultIncomeChartAcntId,
isnull([numDefaultAssetChartAcntId],0) numDefaultAssetChartAcntId,
isnull([numDefaultCOGSChartAcntId],0) numDefaultCOGSChartAcntId,
--isnull(bitHideNewUsers,0) as bitHideNewUsers,
isnull(bitCheckCreditStatus,0) as bitCheckCreditStatus,
isnull([numRelationshipId],0) numRelationshipId,
isnull([numProfileId],0) numProfileId,
isnull(bitEnableDefaultAccounts,0) as bitEnableDefaultAccounts,
--ISNULL([bitEnableCreditCart],0) AS bitEnableCreditCart,
ISNULL([bitHidePriceBeforeLogin],0) AS bitHidePriceBeforeLogin,
ISNULL([bitHidePriceAfterLogin],0) AS bitHidePriceAfterLogin,
ISNULL([numRelationshipIdHidePrice],0) AS numRelationshipIdHidePrice,
ISNULL([numProfileIDHidePrice],0) AS numProfileIDHidePrice,
ISNULL([numBizDocForCreditCard],0) AS numBizDocForCreditCard,
ISNULL([numBizDocForCreditTerm],0) AS numBizDocForCreditTerm,
ISNULL(bitAuthOnlyCreditCard,0) AS bitAuthOnlyCreditCard,
ISNULL(numAuthrizedOrderStatus,0) AS numAuthrizedOrderStatus,
ISNULL(bitSendEmail,1) AS bitSendEmail,
ISNULL(vcGoogleMerchantID , '') AS vcGoogleMerchantID,
ISNULL(vcGoogleMerchantKey,'') AS vcGoogleMerchantKey,
ISNULL(IsSandbox,0) AS IsSandbox,
ISNULL(numSiteID,0) AS numSiteID,
ISNULL(vcPaypalUserName,'') AS vcPaypalUserName,
ISNULL(vcPaypalPassword,'') AS vcPaypalPassword,
ISNULL(vcPaypalSignature,'') AS vcPaypalSignature,
ISNULL(IsPaypalSandbox,0) AS IsPaypalSandbox,
ISNULL(bitSkipStep2,0) AS [bitSkipStep2],
ISNULL(bitDisplayCategory,0) AS [bitDisplayCategory],
ISNULL(bitShowPriceUsingPriceLevel,0) AS [bitShowPriceUsingPriceLevel],
ISNULL(bitEnableSecSorting,0) AS [bitEnableSecSorting],
ISNULL(bitSortPriceMode,0) AS [bitSortPriceMode],
ISNULL(numPageSize,0) AS [numPageSize],
ISNULL(numPageVariant,0) AS [numPageVariant],
ISNULL(bitAutoSelectWarehouse,0) AS bitAutoSelectWarehouse
FROM eCommerceDTL 
WHERE numDomainID=@numDomainID
AND ([numSiteId] = @numSiteID OR @numSiteID = 0)
-- CREATED BY MANISH ANJARA : 1st OCT, 2014
/*
EXEC USP_GetJournalEntries_Virtual @numDomainId = 169, @vcAccountId = '6013',
    @dtFromDate = '2014-01-01 00:00:00', @dtToDate = '2014-01-01 23:59:59',
    @vcTranType = '', @varDescription = '', @CompanyName = '',
    @vcBizPayment = '', @vcCheqNo = '', @vcBizDocID = '', @vcTranRef = '',
    @vcTranDesc = '', @numDivisionID = 0, @ClientTimeZoneOffset = -330,
    @charReconFilter = 'A', @tintMode = 1, @numItemID = 0, @CurrentPage = 1,
    @PageSize = 100

*/

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetJournalEntries_Virtual' )
    DROP PROCEDURE USP_GetJournalEntries_Virtual
GO
CREATE PROCEDURE [dbo].[USP_GetJournalEntries_Virtual]
(
      @numDomainID INT,
      @vcAccountId VARCHAR(4000),
      @dtFromDate DATETIME,
      @dtToDate DATETIME,
      @vcTranType VARCHAR(50) = '',
      @varDescription VARCHAR(50) = '',
      @CompanyName VARCHAR(50) = '',
      @vcBizPayment VARCHAR(50) = '',
      @vcCheqNo VARCHAR(50) = '',
      @vcBizDocID VARCHAR(50) = '',
      @vcTranRef VARCHAR(50) = '',
      @vcTranDesc VARCHAR(50) = '',
	  @numDivisionID NUMERIC(9)=0,
      @ClientTimeZoneOffset INT,  --Added by Chintan to enable calculation of date according to client machine
      @charReconFilter CHAR(1)='',
	  @tintMode AS TINYINT=0,
      @numItemID AS NUMERIC(9)=0,
      @CurrentPage INT=0,
	  @PageSize INT=0
)
WITH Recompile 
AS 
    BEGIN

	DECLARE @vcAccountIDs AS VARCHAR(MAX)
	SELECT @vcAccountIDs = COALESCE(@vcAccountIDs + ',', '') + CAST([COA].[numAccountId] AS VARCHAR(20))
	FROM [dbo].[Chart_Of_Accounts] AS COA 
	WHERE [COA].[numDomainId] = @numDomainID
	PRINT @vcAccountIDs
	SET @vcAccountId= @vcAccountIDs

DECLARE @dtFinFromDate DATETIME;
SET @dtFinFromDate = ( SELECT   dtPeriodFrom
                       FROM     FINANCIALYEAR
                       WHERE    dtPeriodFrom <= @dtFromDate
                                AND dtPeriodTo >= @dtFromDate
                                AND numDomainId = @numDomainId
                     );

PRINT @dtFromDate
PRINT @dtToDate

DECLARE @dtFinYearFromJournal DATETIME ;
DECLARE @dtFinYearFrom DATETIME ;
SELECT @dtFinYearFromJournal = MIN(datEntry_Date) FROM view_journal WHERE numDomainID=@numDomainId 
SET @dtFinYearFrom = ( SELECT   dtPeriodFrom
                        FROM     FINANCIALYEAR
                        WHERE    dtPeriodFrom <= @dtFromDate
                                AND dtPeriodTo >= @dtFromDate
                                AND numDomainId = @numDomainId
                        ) ;

SELECT * INTO #view_journal FROM view_journal WHERE numDomainID=@numDomainId 

DECLARE @numMaxFinYearID NUMERIC
DECLARE @Month INT
DECLARE @year  INT
SELECT @numMaxFinYearID = MAX(numFinYearId) FROM dbo.FinancialYear WHERE numDomainId=@numDomainId
SELECT @Month = DATEPART(MONTH,dtPeriodFrom),@year =DATEPART(year,dtPeriodFrom) FROM dbo.FinancialYear WHERE numDomainId=@numDomainId AND numFinYearId=@numMaxFinYearID
PRINT 'MONTH:' + CAST(@Month AS VARCHAR(100))
PRINT 'YEAR:' + CAST(@year AS VARCHAR(100))
		 
DECLARE @first_id NUMERIC, @startRow int
DECLARE @CurrRecord as INT

if @tintMode = 0
BEGIN
SELECT  COA1.numAccountId INTO #Temp1 /*,COA1.vcAccountCode ,COA2.vcAccountCode*/
		FROM    dbo.Chart_Of_Accounts COA1
		WHERE   COA1.numAccountId IN(SELECT  cast(ID AS NUMERIC(9)) FROM dbo.SplitIDs(@vcAccountId, ','))
				AND COA1.numDomainId = @numDomainID
	SELECT @vcAccountId = ISNULL( STUFF((SELECT ',' + CAST(numAccountID AS VARCHAR(10)) FROM #Temp1 FOR XML PATH('')),1, 1, '') , '') 
    
    	--SELECT  @numDomainID AS numDomainID,* FROM dbo.fn_GetOpeningBalance(@vcAccountID,@numDomainID,@dtFromDate,@ClientTimeZoneOffset)
		SELECT  @numDomainID AS numDomainID, numAccountId,vcAccountName, numParntAcntTypeID,	vcAccountDescription,	vcAccountCode,	dtAsOnDate,	
		CASE WHEN ([Table1].[vcAccountCode] LIKE '0103%' OR [Table1].[vcAccountCode] LIKE '0104%' OR [Table1].[vcAccountCode] LIKE '0106%') AND (@year + @Month) = (DATEPART(YEAR,@dtFromDate) + DATEPART(MONTH,@dtFromDate)) THEN 0 
			 WHEN (@year + @Month) > (DATEPART(YEAR,@dtFromDate) + DATEPART(MONTH,@dtFromDate)) THEN isnull(t1.[OPENING],0)
			 WHEN (@year + @Month) < (DATEPART(YEAR,@dtFromDate) + DATEPART(MONTH,@dtFromDate)) THEN isnull(t2.[OPENING],0)
			 ELSE isnull(mnOpeningBalance,0)
		END  [mnOpeningBalance],
							(SELECT ISNULL(SUM(GJD.numDebitAmt),0) - ISNULL(SUM(GJD.numCreditAmt),0)
							FROM
							General_Journal_Details GJD 
							JOIN Domain D ON D.[numDomainId] = GJD.numDomainID AND D.[numDomainId] = @numDomainID
							INNER JOIN General_Journal_Header GJH ON GJD.numJournalId = GJH.numJournal_Id AND 
							GJD.numDomainId = @numDomainID AND 
							GJD.numChartAcntId = [Table1].numAccountId AND 
							GJH.numDomainID = D.[numDomainId] AND 
							GJH.datEntry_Date <= @dtFinFromDate) 
FROM dbo.fn_GetOpeningBalance(@vcAccountID,@numDomainID,@dtFromDate,@ClientTimeZoneOffset) [Table1]
OUTER APPLY (SELECT   SUM(Debit - Credit) AS OPENING
            FROM     #view_journal VJ
            WHERE    VJ.numDomainId = @numDomainId
                    AND VJ.numAccountID = [Table1].numAccountID /*AND VJ.COAvcAccountCode like COA.vcAccountCode + '%'*/ /*VJ.numAccountId = COA.numAccountId*/
                    AND datEntry_Date BETWEEN (CASE WHEN [Table1].[vcAccountCode] LIKE '0103%' OR [Table1].[vcAccountCode] LIKE '0104%' OR [Table1].[vcAccountCode] LIKE '0106%' 
													THEN @dtFinYearFrom 
													ELSE @dtFinYearFromJournal 
											   END ) AND  DATEADD(Minute,-1,@dtFromDate)) AS t1
OUTER APPLY (SELECT   SUM(Debit - Credit) AS OPENING
            FROM     #view_journal VJ
            WHERE    VJ.numDomainId = @numDomainId
                    AND VJ.numAccountID = [Table1].numAccountID /*AND VJ.COAvcAccountCode like COA.vcAccountCode + '%'*/ /*VJ.numAccountId = COA.numAccountId*/
                    AND datEntry_Date BETWEEN (CASE WHEN [Table1].[vcAccountCode] LIKE '0103%' OR [Table1].[vcAccountCode] LIKE '0104%' OR [Table1].[vcAccountCode] LIKE '0106%' 
													THEN @dtFinYearFrom 
													ELSE @dtFinYearFromJournal 
											   END ) AND  DATEADD(Minute,1,@dtFromDate)) AS t2

		--SET @dtFromDate = DateAdd(minute, @ClientTimeZoneOffset, @dtFromDate);
		--SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate);
SELECT * FROM 
(
SELECT TOP 1
dbo.General_Journal_Details.numDomainId,
 [TABLE1].numAccountId , 
dbo.General_Journal_Header.numJournal_Id,
dbo.[FormatedDateFromDate](dbo.General_Journal_Header.datEntry_Date,dbo.General_Journal_Details.numDomainID) Date,
dbo.General_Journal_Header.varDescription, 
dbo.CompanyInfo.vcCompanyName AS CompanyName,
dbo.General_Journal_Details.numTransactionId,

ISNULL(ISNULL(ISNULL(dbo.VIEW_BIZPAYMENT.Narration,dbo.General_Journal_Header.varDescription),'') + ' ' + 
(CASE WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN ' Chek No: ' + isnull( CAST(CH.numCheckNo AS VARCHAR(50)),'No-Cheq') 
ELSE  dbo.VIEW_JOURNALCHEQ.Narration 
END),'') as Narration,

ISNULL(dbo.General_Journal_Details.vcReference,'') AS TranRef, 
ISNULL(dbo.General_Journal_Details.varDescription,'') AS TranDesc, 
CONVERT(VARCHAR(20),ISNULL([GJD].numDebitAmt,0)) numDebitAmt,
CONVERT(VARCHAR(20),ISNULL(General_Journal_Details.numDebitAmt,0)) [numDebitAmt_General_Journal_Details],
CONVERT(VARCHAR(20),ISNULL(General_Journal_Details.numCreditAmt,0)) numCreditAmt,
CONVERT(VARCHAR(20),ISNULL([GJD].numCreditAmt,0)) [numCreditAmt_GJD],
CASE WHEN ISNULL(General_Journal_Details.[numDebitAmt],0) = 0 THEN [TABLE1].vcAccountName ELSE '' END [vcAccountName],
CASE WHEN ISNULL([GJD].[numCreditAmt],0) = 0 THEN [TABLE2].[vcAccountName] ELSE '' END AS [vcSplitAccountName],
'' as balance,
CASE 
WHEN isnull(dbo.General_Journal_Header.numCheckHeaderID, 0) <> 0 THEN + 'Checks' 
WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 1 THEN 'Deposit' 
WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 2 THEN 'Receved Pmt' 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
dbo.OpportunityMaster.tintOppType = 1 THEN 'BizDocs Invoice' 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
dbo.OpportunityMaster.tintOppType = 2 THEN 'BizDocs Purchase' 
WHEN isnull(dbo.General_Journal_Header.numCategoryHDRID, 0) <> 0 THEN 'Time And Expenses' 
WHEN ISNULL(General_Journal_Header.numBillId,0) <>0 AND isnull(BH.bitLandedCost,0)=1 then 'Landed Cost' 
WHEN isnull(dbo.General_Journal_Header.numBillID, 0) <> 0 THEN 'Bill'
WHEN isnull(dbo.General_Journal_Header.numBillPaymentID, 0) <> 0 THEN 'Pay Bill'
WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN 
																CASE 
																	WHEN RH.tintReturnType=1 AND RH.tintReceiveType=1 THEN 'Sales Return Refund'
																	WHEN RH.tintReturnType=1 AND RH.tintReceiveType=2 THEN 'Sales Return Credit Memo'
																	WHEN RH.tintReturnType=2 AND RH.tintReceiveType=2 THEN 'Purchase Return Credit Memo'
																	WHEN RH.tintReturnType=3 AND RH.tintReceiveType=2 THEN 'Credit Memo'
																	WHEN RH.tintReturnType=4 AND RH.tintReceiveType=1 THEN 'Refund Against Credit Memo'
																END 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) = 0 THEN 'PO Fulfillment'
WHEN dbo.General_Journal_Header.numJournal_Id <> 0 THEN 'Journal' 
END AS TransactionType,

ISNULL(dbo.General_Journal_Details.bitCleared,0) AS bitCleared,
ISNULL(dbo.General_Journal_Details.bitReconcile,0) AS bitReconcile,
ISNULL(dbo.General_Journal_Details.numItemID,0) AS numItemID,
Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else 0 end AS numLandedCostOppId,
--STUFF((SELECT ', ' + CAST([COA].[vcAccountName] AS VARCHAR(4000))
--			FROM [dbo].[General_Journal_Details] AS GJD JOIN [dbo].[Chart_Of_Accounts] AS COA ON GJD.[numChartAcntId] = COA.[numAccountId]
--			WHERE [GJD].[numJournalId] = dbo.General_Journal_Header.numJournal_Id 
--			AND [COA].[numDomainId] = General_Journal_Header.[numDomainId]
--			AND [COA].[numAccountId] <> dbo.Chart_Of_Accounts.numAccountId FOR XML PATH('')) ,1,2, '') AS [vcSplitAccountName]
[dbo].[General_Journal_Details].numEntryDateSortOrder1
FROM dbo.General_Journal_Header 
INNER JOIN dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId 
INNER JOIN dbo.General_Journal_Details GJD ON dbo.General_Journal_Header.numJournal_Id = GJD.numJournalId 
INNER JOIN dbo.Chart_Of_Accounts ON dbo.General_Journal_Details.numChartAcntId = dbo.Chart_Of_Accounts.numAccountId AND dbo.General_Journal_Details.[numChartAcntId] <> GJD.[numChartAcntId]
OUTER APPLY (SELECT vcAccountName,[COA].[numAccountId] FROM [dbo].[Chart_Of_Accounts] AS COA 
			WHERE dbo.Chart_Of_Accounts.numAccountId = COA.[numAccountId]
			AND [COA].[numDomainId] = General_Journal_Header.[numDomainId]
			AND dbo.General_Journal_Details.[numChartAcntId] = [COA].[numAccountId]) TABLE1
OUTER APPLY (SELECT vcAccountName,[COA1].[numAccountId] FROM [dbo].[Chart_Of_Accounts] AS COA1 
			WHERE dbo.Chart_Of_Accounts.numAccountId <> COA1.[numAccountId]
			AND [COA1].[numDomainId] = General_Journal_Header.[numDomainId]
			AND [GJD].[numChartAcntId] = [COA1].[numAccountId]) TABLE2
LEFT OUTER JOIN dbo.TimeAndExpense ON dbo.General_Journal_Header.numCategoryHDRID = dbo.TimeAndExpense.numCategoryHDRID 
LEFT OUTER JOIN dbo.DivisionMaster ON dbo.General_Journal_Details.numCustomerId = dbo.DivisionMaster.numDivisionID and 
dbo.General_Journal_Details.numDomainID = dbo.DivisionMaster.numDomainID
LEFT OUTER JOIN dbo.CompanyInfo ON dbo.DivisionMaster.numCompanyID = dbo.CompanyInfo.numCompanyId 
LEFT OUTER JOIN dbo.OpportunityMaster ON dbo.General_Journal_Header.numOppId = dbo.OpportunityMaster.numOppId LEFT OUTER JOIN
dbo.OpportunityBizDocs ON dbo.General_Journal_Header.numOppBizDocsId = dbo.OpportunityBizDocs.numOppBizDocsId LEFT OUTER JOIN
dbo.VIEW_JOURNALCHEQ ON (dbo.General_Journal_Header.numCheckHeaderID = dbo.VIEW_JOURNALCHEQ.numCheckHeaderID 
or (dbo.General_Journal_Header.numBillPaymentID=dbo.VIEW_JOURNALCHEQ.numReferenceID and dbo.VIEW_JOURNALCHEQ.tintReferenceType=8))LEFT OUTER JOIN
dbo.VIEW_BIZPAYMENT ON dbo.General_Journal_Header.numBizDocsPaymentDetId = dbo.VIEW_BIZPAYMENT.numBizDocsPaymentDetId LEFT OUTER JOIN
dbo.DepositMaster DM ON DM.numDepositId = dbo.General_Journal_Header.numDepositId  LEFT OUTER JOIN
dbo.ReturnHeader RH ON RH.numReturnHeaderID = dbo.General_Journal_Header.numReturnID LEFT OUTER JOIN
dbo.CheckHeader CH ON CH.numReferenceID=RH.numReturnHeaderID AND CH.tintReferenceType=10
LEFT OUTER JOIN BillHeader BH  ON ISNULL(General_Journal_Header.numBillId,0) = BH.numBillId
WHERE   dbo.General_Journal_Header.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate -- Removed Tmezone bug id 1029 
AND dbo.General_Journal_Header.numDomainId = @numDomainID 
 AND (dbo.DivisionMaster.numDivisionID = @numDivisionID OR @numDivisionID = 0)
AND ([dbo].[General_Journal_Details].bitCleared = CASE @charReconFilter WHEN 'C' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A')
AND ([dbo].[General_Journal_Details].bitReconcile = CASE @charReconFilter WHEN 'R' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A') 
AND ([TABLE1].numAccountId IN (SELECT numAccountId FROM #Temp1))
--AND numJournal_Id = 181544
--ORDER BY dbo.General_Journal_Header.numEntryDateSortOrder asc
--ORDER BY dbo.Chart_Of_Accounts.numAccountId ASC,  dbo.General_Journal_Header.datEntry_Date ASC, dbo.General_Journal_Details.numTransactionId asc 
) TABLE1 WHERE ISNULL(TransactionType,'') = 'Journal'
ORDER BY TABLE1.numEntryDateSortOrder1 ASC
END
ELSE IF @tintMode = 1
BEGIN

--SELECT @vcAccountID

SELECT  @numDomainID AS numDomainID, numAccountId,vcAccountName, numParntAcntTypeID,	vcAccountDescription,	vcAccountCode,	dtAsOnDate,	
		CASE WHEN ([Table1].[vcAccountCode] LIKE '0103%' OR [Table1].[vcAccountCode] LIKE '0104%' OR [Table1].[vcAccountCode] LIKE '0106%') AND (@year + @Month) = (DATEPART(YEAR,@dtFromDate) + DATEPART(MONTH,@dtFromDate)) THEN 0 
			 WHEN (@year + @Month) > (DATEPART(YEAR,@dtFromDate) + DATEPART(MONTH,@dtFromDate)) THEN isnull(t1.[OPENING],0)
			 WHEN (@year + @Month) < (DATEPART(YEAR,@dtFromDate) + DATEPART(MONTH,@dtFromDate)) THEN isnull(t2.[OPENING],0)
			 ELSE isnull(mnOpeningBalance,0)
		END  [mnOpeningBalance],
							(SELECT ISNULL(SUM(GJD.numDebitAmt),0) - ISNULL(SUM(GJD.numCreditAmt),0)
							FROM
							General_Journal_Details GJD 
							JOIN Domain D ON D.[numDomainId] = GJD.numDomainID AND D.[numDomainId] = @numDomainID
							INNER JOIN General_Journal_Header GJH ON GJD.numJournalId = GJH.numJournal_Id AND 
							GJD.numDomainId = @numDomainID AND 
							GJD.numChartAcntId = [Table1].numAccountId AND 
							GJH.numDomainID = D.[numDomainId] AND 
							GJH.datEntry_Date <= @dtFinFromDate) 
FROM dbo.fn_GetOpeningBalance(@vcAccountID,@numDomainID,@dtFromDate,@ClientTimeZoneOffset) [Table1]
OUTER APPLY (SELECT   SUM(Debit - Credit) AS OPENING
            FROM     #view_journal VJ
            WHERE    VJ.numDomainId = @numDomainId
                    AND VJ.numAccountID = [Table1].numAccountID /*AND VJ.COAvcAccountCode like COA.vcAccountCode + '%'*/ /*VJ.numAccountId = COA.numAccountId*/
                    AND datEntry_Date BETWEEN (CASE WHEN [Table1].[vcAccountCode] LIKE '0103%' OR [Table1].[vcAccountCode] LIKE '0104%' OR [Table1].[vcAccountCode] LIKE '0106%' 
													THEN @dtFinYearFrom 
													ELSE @dtFinYearFromJournal 
											   END ) AND  DATEADD(Minute,-1,@dtFromDate)) AS t1
OUTER APPLY (SELECT   SUM(Debit - Credit) AS OPENING
            FROM     #view_journal VJ
            WHERE    VJ.numDomainId = @numDomainId
                    AND VJ.numAccountID = [Table1].numAccountID /*AND VJ.COAvcAccountCode like COA.vcAccountCode + '%'*/ /*VJ.numAccountId = COA.numAccountId*/
                    AND datEntry_Date BETWEEN (CASE WHEN [Table1].[vcAccountCode] LIKE '0103%' OR [Table1].[vcAccountCode] LIKE '0104%' OR [Table1].[vcAccountCode] LIKE '0106%' 
													THEN @dtFinYearFrom 
													ELSE @dtFinYearFromJournal 
											   END ) AND  DATEADD(Minute,1,@dtFromDate)) AS t2

--SET @dtFromDate = DateAdd(minute, @ClientTimeZoneOffset, @dtFromDate);
--SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate);

SET @CurrRecord = ((@CurrentPage - 1) * @PageSize) + 1
PRINT @CurrRecord
SET ROWCOUNT @CurrRecord

SELECT @first_id = dbo.General_Journal_Details.numEntryDateSortOrder1
FROM dbo.General_Journal_Header 
INNER JOIN dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId   
INNER JOIN dbo.Chart_Of_Accounts ON dbo.General_Journal_Details.numChartAcntId = dbo.Chart_Of_Accounts.numAccountId 
LEFT OUTER JOIN dbo.TimeAndExpense ON dbo.General_Journal_Header.numCategoryHDRID = dbo.TimeAndExpense.numCategoryHDRID 
WHERE dbo.General_Journal_Header.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate 
AND dbo.General_Journal_Header.numDomainId = @numDomainID 
AND dbo.General_Journal_Details.numDomainId = @numDomainID 
AND dbo.Chart_Of_Accounts.numDomainId = @numDomainID 
AND (bitCleared = CASE @charReconFilter WHEN 'C' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A')
AND (bitReconcile = CASE @charReconFilter WHEN 'R' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A') 
--AND (dbo.Chart_Of_Accounts.numAccountId IN (SELECT [SS].[OutParam] FROM [dbo].[SplitString](@vcAccountId,',') AS SS))
AND (General_Journal_Details.numItemID = @numItemID OR @numItemID = 0)
AND dbo.General_Journal_Details.numEntryDateSortOrder1 > 0
ORDER BY dbo.General_Journal_Details.numEntryDateSortOrder1 asc

PRINT @first_id
PRINT ROWCOUNT_BIG()
SET ROWCOUNT @PageSize

SELECT * FROM 
(
SELECT dbo.General_Journal_Details.numDomainId, [TABLE1].numAccountId , dbo.General_Journal_Header.numJournal_Id,
dbo.[FormatedDateFromDate](dbo.General_Journal_Header.datEntry_Date,dbo.General_Journal_Details.numDomainID) Date,
dbo.General_Journal_Header.varDescription, 
dbo.CompanyInfo.vcCompanyName AS CompanyName,
dbo.General_Journal_Details.numTransactionId,
ISNULL(ISNULL(ISNULL(dbo.VIEW_BIZPAYMENT.Narration,dbo.General_Journal_Header.varDescription),'') + ' ' + 
(CASE WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN ' Chek No: ' + isnull( CAST(CH.numCheckNo AS VARCHAR(50)),'No-Cheq') 
ELSE  dbo.VIEW_JOURNALCHEQ.Narration 
END),'') as Narration,
ISNULL(dbo.General_Journal_Details.vcReference,'') AS TranRef, 
ISNULL(dbo.General_Journal_Details.varDescription,'') AS TranDesc, 
CONVERT(VARCHAR(20),ISNULL([GJD].numDebitAmt,0)) numDebitAmt,
CONVERT(VARCHAR(20),ISNULL(General_Journal_Details.numDebitAmt,0)) [numDebitAmt_General_Journal_Details],
CONVERT(VARCHAR(20),ISNULL(General_Journal_Details.numCreditAmt,0)) numCreditAmt,
CONVERT(VARCHAR(20),ISNULL([GJD].numCreditAmt,0)) [numCreditAmt_GJD],
CASE WHEN ISNULL(General_Journal_Details.[numDebitAmt],0) = 0 THEN [TABLE1].vcAccountName ELSE '' END [vcAccountName],
CASE WHEN ISNULL([GJD].[numCreditAmt],0) = 0 THEN [TABLE2].[vcAccountName] ELSE '' END AS [vcSplitAccountName],
'' as balance,
CASE 
WHEN isnull(dbo.General_Journal_Header.numCheckHeaderID, 0) <> 0 THEN + 'Checks' 
WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 1 THEN 'Deposit' 
WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 2 THEN 'Receved Pmt' 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
dbo.OpportunityMaster.tintOppType = 1 THEN 'BizDocs Invoice' 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
dbo.OpportunityMaster.tintOppType = 2 THEN 'BizDocs Purchase' 
WHEN isnull(dbo.General_Journal_Header.numCategoryHDRID, 0) <> 0 THEN 'Time And Expenses' 
WHEN ISNULL(General_Journal_Header.numBillId,0) <>0 AND isnull(BH.bitLandedCost,0)=1 then 'Landed Cost' 
WHEN isnull(dbo.General_Journal_Header.numBillID, 0) <> 0 THEN 'Bill'
WHEN isnull(dbo.General_Journal_Header.numBillPaymentID, 0) <> 0 THEN 'Pay Bill'
WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN 
																CASE 
																	WHEN RH.tintReturnType=1 AND RH.tintReceiveType=1 THEN 'Sales Return Refund'
																	WHEN RH.tintReturnType=1 AND RH.tintReceiveType=2 THEN 'Sales Return Credit Memo'
																	WHEN RH.tintReturnType=2 AND RH.tintReceiveType=2 THEN 'Purchase Return Credit Memo'
																	WHEN RH.tintReturnType=3 AND RH.tintReceiveType=2 THEN 'Credit Memo'
																	WHEN RH.tintReturnType=4 AND RH.tintReceiveType=1 THEN 'Refund Against Credit Memo'
																END 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) = 0 THEN 'PO Fulfillment'
WHEN dbo.General_Journal_Header.numJournal_Id <> 0 THEN 'Journal' 
END AS TransactionType,
ISNULL(dbo.General_Journal_Details.bitCleared,0) AS bitCleared,
ISNULL(dbo.General_Journal_Details.bitReconcile,0) AS bitReconcile,
ISNULL(dbo.General_Journal_Details.numItemID,0) AS numItemID,
dbo.General_Journal_Details.numEntryDateSortOrder1 AS numEntryDateSortOrder,
Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else 0 end AS numLandedCostOppId,
--STUFF((SELECT ', ' + CAST([COA].[vcAccountName] AS VARCHAR(4000))
--			FROM [dbo].[General_Journal_Details] AS GJD JOIN [dbo].[Chart_Of_Accounts] AS COA ON GJD.[numChartAcntId] = COA.[numAccountId]
--			WHERE [GJD].[numJournalId] = dbo.General_Journal_Header.numJournal_Id 
--			AND [COA].[numDomainId] = General_Journal_Header.[numDomainId]
--			AND [COA].[numAccountId] <> dbo.Chart_Of_Accounts.numAccountId FOR XML PATH('')) ,1,2, '') AS [vcSplitAccountName]
[dbo].[General_Journal_Details].numEntryDateSortOrder1
FROM dbo.General_Journal_Header 
LEFT JOIN dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId 
LEFT JOIN dbo.General_Journal_Details GJD ON dbo.General_Journal_Header.numJournal_Id = GJD.numJournalId 
INNER JOIN dbo.Chart_Of_Accounts ON dbo.General_Journal_Details.numChartAcntId = dbo.Chart_Of_Accounts.numAccountId AND dbo.General_Journal_Details.[numChartAcntId] <> GJD.[numChartAcntId]
OUTER APPLY (SELECT vcAccountName,[COA].[numAccountId] FROM [dbo].[Chart_Of_Accounts] AS COA 
			WHERE dbo.Chart_Of_Accounts.numAccountId = COA.[numAccountId]
			AND [COA].[numDomainId] = General_Journal_Header.[numDomainId]
			AND dbo.General_Journal_Details.[numChartAcntId] = [COA].[numAccountId]) TABLE1
OUTER APPLY (SELECT vcAccountName,[COA1].[numAccountId] FROM [dbo].[Chart_Of_Accounts] AS COA1 
			WHERE dbo.Chart_Of_Accounts.numAccountId <> COA1.[numAccountId]
			AND [COA1].[numDomainId] = General_Journal_Header.[numDomainId]
			AND [GJD].[numChartAcntId] = [COA1].[numAccountId]) TABLE2 
LEFT OUTER JOIN dbo.TimeAndExpense ON dbo.General_Journal_Header.numCategoryHDRID = dbo.TimeAndExpense.numCategoryHDRID 
LEFT OUTER JOIN dbo.DivisionMaster ON dbo.General_Journal_Details.numCustomerId = dbo.DivisionMaster.numDivisionID and 
dbo.General_Journal_Details.numDomainID = dbo.DivisionMaster.numDomainID
LEFT OUTER JOIN dbo.CompanyInfo ON dbo.DivisionMaster.numCompanyID = dbo.CompanyInfo.numCompanyId 
LEFT OUTER JOIN dbo.OpportunityMaster ON dbo.General_Journal_Header.numOppId = dbo.OpportunityMaster.numOppId LEFT OUTER JOIN
dbo.OpportunityBizDocs ON dbo.General_Journal_Header.numOppBizDocsId = dbo.OpportunityBizDocs.numOppBizDocsId LEFT OUTER JOIN
dbo.VIEW_JOURNALCHEQ ON (dbo.General_Journal_Header.numCheckHeaderID = dbo.VIEW_JOURNALCHEQ.numCheckHeaderID 
or (dbo.General_Journal_Header.numBillPaymentID=dbo.VIEW_JOURNALCHEQ.numReferenceID and dbo.VIEW_JOURNALCHEQ.tintReferenceType=8))LEFT OUTER JOIN
dbo.VIEW_BIZPAYMENT ON dbo.General_Journal_Header.numBizDocsPaymentDetId = dbo.VIEW_BIZPAYMENT.numBizDocsPaymentDetId LEFT OUTER JOIN
dbo.DepositMaster DM ON DM.numDepositId = dbo.General_Journal_Header.numDepositId  LEFT OUTER JOIN
dbo.ReturnHeader RH ON RH.numReturnHeaderID = dbo.General_Journal_Header.numReturnID LEFT OUTER JOIN
dbo.CheckHeader CH ON CH.numReferenceID=RH.numReturnHeaderID AND CH.tintReferenceType=10
LEFT OUTER JOIN BillHeader BH  ON ISNULL(General_Journal_Header.numBillId,0) = BH.numBillId
WHERE   dbo.General_Journal_Header.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate -- Removed Tmezone bug id 1029 
AND dbo.General_Journal_Header.numDomainId = @numDomainID 
AND (dbo.DivisionMaster.numDivisionID = @numDivisionID OR @numDivisionID = 0)
AND ([dbo].[General_Journal_Details].bitCleared = CASE @charReconFilter WHEN 'C' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A')
AND ([dbo].[General_Journal_Details].bitReconcile = CASE @charReconFilter WHEN 'R' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A') 
--AND (dbo.Chart_Of_Accounts.numAccountId =@vcAccountId)
--AND (dbo.Chart_Of_Accounts. IN ( SELECT numAccountId FROM #Temp) OR  @tintMode=1)
--AND (dbo.Chart_Of_Accounts.numAccountId IN (SELECT [SS].[OutParam] FROM [dbo].[SplitString](@vcAccountId,',') AS SS))
AND (General_Journal_Details.numItemID = @numItemID OR @numItemID = 0)
--AND numJournal_Id = 181544
   AND dbo.General_Journal_Details.numEntryDateSortOrder1  >= @first_id              
--AND dbo.General_Journal_Details.numTransactionId >= @first_id
--ORDER BY dbo.Chart_Of_Accounts.numAccountId ASC,   dbo.General_Journal_Details.numTransactionId asc 
) TABLE1 WHERE ISNULL(TransactionType,'') = 'Journal'
ORDER BY TABLE1.numEntryDateSortOrder1 ASC

SET ROWCOUNT 0

END
	
IF object_id('TEMPDB.DBO.#Temp1') IS NOT NULL drop table #Temp1	
END


--    (
--      @numDomainID INT ,
--      @dtFromDate DATETIME ,
--      @dtToDate DATETIME ,
--      @ClientTimeZoneOffset INT ,  --To enable calculation of date according to client machine
--      @charReconFilter CHAR(1) = '' ,
--      @CurrentPage INT = 0 ,
--      @PageSize INT = 0
--    )
--    WITH RECOMPILE -- To avoid parameter sniffing
--AS
--    BEGIN
		
--		DECLARE @vcAccountIDs AS VARCHAR(MAX)
--		SELECT @vcAccountIDs = COALESCE(@vcAccountIDs + ',', '') + CAST([COA].[numAccountId] AS VARCHAR(20))
--		FROM [dbo].[Chart_Of_Accounts] AS COA 
--		WHERE [COA].[numDomainId] = @numDomainID
--		PRINT @vcAccountIDs

--        DECLARE @dtFinFromDate DATETIME;
--        SET @dtFinFromDate = ( SELECT   dtPeriodFrom
--                               FROM     FINANCIALYEAR
--                               WHERE    dtPeriodFrom <= @dtFromDate
--                                        AND dtPeriodTo >= @dtFromDate
--                                        AND numDomainId = @numDomainId
--                             );

--        PRINT @dtFromDate
--        PRINT @dtToDate
	 
--        DECLARE @first_id NUMERIC ,
--            @startRow INT
--        DECLARE @CurrRecord AS INT


--        BEGIN
--			--SELECT  @numDomainID AS numDomainID, numAccountId,vcAccountName, numParntAcntTypeID,	vcAccountDescription,	vcAccountCode,	dtAsOnDate,	
--			--mnOpeningBalance  [mnOpeningBalance],
--			--							(SELECT ISNULL(SUM(GJD.numDebitAmt),0) - ISNULL(SUM(GJD.numCreditAmt),0)
--			--							FROM
--			--							General_Journal_Details GJD 
--			--							JOIN Domain D ON D.[numDomainId] = GJD.numDomainID AND D.[numDomainId] = @numDomainID
--			--							INNER JOIN General_Journal_Header GJH ON GJD.numJournalId = GJH.numJournal_Id AND 
--			--							GJD.numDomainId = @numDomainID AND 
--			--							GJD.numChartAcntId = [Table1].numAccountId AND 
--			--							GJH.numDomainID = D.[numDomainId] AND 
--			--							GJH.datEntry_Date <= @dtFinFromDate) 
--			--FROM dbo.fn_GetOpeningBalance(@vcAccountID,@numDomainID,@dtFromDate,@ClientTimeZoneOffset) [Table1]

--            SET @CurrRecord = ( ( @CurrentPage - 1 ) * @PageSize ) + 1
--            PRINT @CurrRecord
--            SET ROWCOUNT @CurrRecord

--            SELECT  @first_id = dbo.General_Journal_Details.numEntryDateSortOrder1
--            FROM    dbo.General_Journal_Header
--                    INNER JOIN dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId
--                    INNER JOIN dbo.Chart_Of_Accounts ON dbo.General_Journal_Details.numChartAcntId = dbo.Chart_Of_Accounts.numAccountId
--                    LEFT OUTER JOIN dbo.TimeAndExpense ON dbo.General_Journal_Header.numCategoryHDRID = dbo.TimeAndExpense.numCategoryHDRID -- LEFT OUTER JOIN
--            WHERE   dbo.General_Journal_Header.datEntry_Date BETWEEN @dtFromDate
--                                                             AND
--                                                              @dtToDate
--                    AND dbo.General_Journal_Header.numDomainId = @numDomainID
--                    AND dbo.General_Journal_Details.numDomainId = @numDomainID
--                    AND dbo.Chart_Of_Accounts.numDomainId = @numDomainID
--                    AND ( bitCleared = CASE @charReconFilter
--                                         WHEN 'C' THEN 1
--                                         ELSE 0
--                                       END
--                          OR RTRIM(@charReconFilter) = 'A'
--                        )
--                    AND ( bitReconcile = CASE @charReconFilter
--                                           WHEN 'R' THEN 1
--                                           ELSE 0
--                                         END
--                          OR RTRIM(@charReconFilter) = 'A'
--                        )
--                    AND ( dbo.Chart_Of_Accounts.numAccountId = [dbo].[General_Journal_Details].[numChartAcntId] )
--                    --AND dbo.General_Journal_Details.numEntryDateSortOrder1 > 0
--            ORDER BY dbo.General_Journal_Details.numEntryDateSortOrder1 ASC

--            PRINT @first_id
--            PRINT ROWCOUNT_BIG()
--            SET ROWCOUNT @PageSize

--            SELECT  dbo.General_Journal_Details.numDomainId ,
--                    dbo.Chart_Of_Accounts.numAccountId ,
--                    dbo.General_Journal_Header.numJournal_Id ,
--                    dbo.[FormatedDateFromDate](dbo.General_Journal_Header.datEntry_Date,
--                                               dbo.General_Journal_Details.numDomainID) Date ,
--                    dbo.General_Journal_Header.varDescription ,
--                    dbo.CompanyInfo.vcCompanyName AS CompanyName ,
--                    dbo.General_Journal_Details.numTransactionId ,
--                    ISNULL(ISNULL(ISNULL(dbo.VIEW_BIZPAYMENT.Narration,
--                                         dbo.General_Journal_Header.varDescription),
--                                  '') + ' '
--                           + ( CASE WHEN ISNULL(dbo.General_Journal_Header.numReturnID,
--                                                0) <> 0
--                                    THEN ' Chek No: '
--                                         + ISNULL(CAST(CH.numCheckNo AS VARCHAR(50)),
--                                                  'No-Cheq')
--                                    ELSE dbo.VIEW_JOURNALCHEQ.Narration
--                               END ), '') AS Narration ,
--                    ISNULL(dbo.General_Journal_Details.vcReference, '') AS TranRef ,
--                    ISNULL(dbo.General_Journal_Details.varDescription, '') AS TranDesc ,
--                    CONVERT(VARCHAR(20), ISNULL(General_Journal_Details.numDebitAmt,
--                                                0)) numDebitAmt ,
--                    CONVERT(VARCHAR(20), ISNULL(General_Journal_Details.numCreditAmt,
--                                                0)) numCreditAmt ,
--                    dbo.Chart_Of_Accounts.vcAccountName ,
--                    '' AS balance ,
--                    CASE WHEN ISNULL(dbo.General_Journal_Header.numCheckHeaderID,
--                                     0) <> 0 THEN +'Checks'
--                         WHEN ISNULL(dbo.General_Journal_Header.numDepositId,
--                                     0) <> 0
--                              AND DM.tintDepositePage = 1 THEN 'Deposit'
--                         WHEN ISNULL(dbo.General_Journal_Header.numDepositId,
--                                     0) <> 0
--                              AND DM.tintDepositePage = 2 THEN 'Receved Pmt'
--                         WHEN ISNULL(dbo.General_Journal_Header.numOppId, 0) <> 0
--                              AND ISNULL(dbo.General_Journal_Header.numOppBizDocsId,
--                                         0) <> 0
--                              AND ISNULL(dbo.General_Journal_Header.numBizDocsPaymentDetId,
--                                         0) = 0
--                              AND dbo.OpportunityMaster.tintOppType = 1
--                         THEN 'BizDocs Invoice'
--                         WHEN ISNULL(dbo.General_Journal_Header.numOppId, 0) <> 0
--                              AND ISNULL(dbo.General_Journal_Header.numOppBizDocsId,
--                                         0) <> 0
--                              AND ISNULL(dbo.General_Journal_Header.numBizDocsPaymentDetId,
--                                         0) = 0
--                              AND dbo.OpportunityMaster.tintOppType = 2
--                         THEN 'BizDocs Purchase'
--                         WHEN ISNULL(dbo.General_Journal_Header.numCategoryHDRID,
--                                     0) <> 0 THEN 'Time And Expenses'
--                         WHEN ISNULL(General_Journal_Header.numBillId, 0) <> 0
--                              AND ISNULL(BH.bitLandedCost, 0) = 1
--                         THEN 'Landed Cost'
--                         WHEN ISNULL(dbo.General_Journal_Header.numBillID, 0) <> 0
--                         THEN 'Bill'
--                         WHEN ISNULL(dbo.General_Journal_Header.numBillPaymentID,
--                                     0) <> 0 THEN 'Pay Bill'
--                         WHEN ISNULL(dbo.General_Journal_Header.numReturnID, 0) <> 0
--                         THEN CASE WHEN RH.tintReturnType = 1
--                                        AND RH.tintReceiveType = 1
--                                   THEN 'Sales Return Refund'
--                                   WHEN RH.tintReturnType = 1
--                                        AND RH.tintReceiveType = 2
--                                   THEN 'Sales Return Credit Memo'
--                                   WHEN RH.tintReturnType = 2
--                                        AND RH.tintReceiveType = 2
--                                   THEN 'Purchase Return Credit Memo'
--                                   WHEN RH.tintReturnType = 3
--                                        AND RH.tintReceiveType = 2
--                                   THEN 'Credit Memo'
--                                   WHEN RH.tintReturnType = 4
--                                        AND RH.tintReceiveType = 1
--                                   THEN 'Refund Against Credit Memo'
--                              END
--                         WHEN ISNULL(dbo.General_Journal_Header.numOppId, 0) <> 0
--                              AND ISNULL(dbo.General_Journal_Header.numOppBizDocsId,
--                                         0) = 0 THEN 'PO Fulfillment'
--                         WHEN dbo.General_Journal_Header.numJournal_Id <> 0
--                         THEN 'Journal'
--                    END AS TransactionType ,
--                    ISNULL(dbo.General_Journal_Details.bitCleared, 0) AS bitCleared ,
--                    ISNULL(dbo.General_Journal_Details.bitReconcile, 0) AS bitReconcile ,
--                    ISNULL(dbo.General_Journal_Details.numItemID, 0) AS numItemID ,
--                    dbo.General_Journal_Details.numEntryDateSortOrder1 AS numEntryDateSortOrder ,
--					(SELECT mnOpeningBalance FROM dbo.fn_GetOpeningBalance(dbo.Chart_Of_Accounts.numAccountId,@numDomainID,@dtFromDate,@ClientTimeZoneOffset) AS TABLE1
--					WHERE TABLE1.[numAccountId] = dbo.Chart_Of_Accounts.numAccountId) [mnOpeningBalance],
--                    CASE WHEN ISNULL(BH.bitLandedCost, 0) = 1
--                         THEN ISNULL(BH.numOppId, 0)
--                         ELSE 0
--                    END AS numLandedCostOppId
--            FROM    dbo.General_Journal_Header
--                    INNER JOIN dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId
--                    INNER JOIN dbo.Chart_Of_Accounts ON dbo.General_Journal_Details.numChartAcntId = dbo.Chart_Of_Accounts.numAccountId
--                    LEFT OUTER JOIN dbo.TimeAndExpense ON dbo.General_Journal_Header.numCategoryHDRID = dbo.TimeAndExpense.numCategoryHDRID
--                    LEFT OUTER JOIN dbo.DivisionMaster ON dbo.General_Journal_Details.numCustomerId = dbo.DivisionMaster.numDivisionID
--                                                          AND dbo.General_Journal_Details.numDomainID = dbo.DivisionMaster.numDomainID
--                    LEFT OUTER JOIN dbo.CompanyInfo ON dbo.DivisionMaster.numCompanyID = dbo.CompanyInfo.numCompanyId
--                    LEFT OUTER JOIN dbo.OpportunityMaster ON dbo.General_Journal_Header.numOppId = dbo.OpportunityMaster.numOppId
--                    LEFT OUTER JOIN dbo.OpportunityBizDocs ON dbo.General_Journal_Header.numOppBizDocsId = dbo.OpportunityBizDocs.numOppBizDocsId
--                    LEFT OUTER JOIN dbo.VIEW_JOURNALCHEQ ON ( dbo.General_Journal_Header.numCheckHeaderID = dbo.VIEW_JOURNALCHEQ.numCheckHeaderID
--                                                              OR ( dbo.General_Journal_Header.numBillPaymentID = dbo.VIEW_JOURNALCHEQ.numReferenceID
--                                                              AND dbo.VIEW_JOURNALCHEQ.tintReferenceType = 8
--                                                              )
--                                                            )
--                    LEFT OUTER JOIN dbo.VIEW_BIZPAYMENT ON dbo.General_Journal_Header.numBizDocsPaymentDetId = dbo.VIEW_BIZPAYMENT.numBizDocsPaymentDetId
--                    LEFT OUTER JOIN dbo.DepositMaster DM ON DM.numDepositId = dbo.General_Journal_Header.numDepositId
--                    LEFT OUTER JOIN dbo.ReturnHeader RH ON RH.numReturnHeaderID = dbo.General_Journal_Header.numReturnID
--                    LEFT OUTER JOIN dbo.CheckHeader CH ON CH.numReferenceID = RH.numReturnHeaderID
--                                                          AND CH.tintReferenceType = 10
--                    LEFT OUTER JOIN BillHeader BH ON ISNULL(General_Journal_Header.numBillId,
--                                                            0) = BH.numBillId
--            WHERE   dbo.General_Journal_Header.datEntry_Date BETWEEN @dtFromDate
--                                                             AND
--                                                              @dtToDate
--                    AND dbo.General_Journal_Header.numDomainId = @numDomainID
--                    AND ( bitCleared = CASE @charReconFilter
--                                         WHEN 'C' THEN 1
--                                         ELSE 0
--                                       END
--                          OR RTRIM(@charReconFilter) = 'A'
--                        )
--                    AND ( bitReconcile = CASE @charReconFilter
--                                           WHEN 'R' THEN 1
--                                           ELSE 0
--                                         END
--                          OR RTRIM(@charReconFilter) = 'A'
--                        )
--                    AND ( dbo.Chart_Of_Accounts.numAccountId = [dbo].[General_Journal_Details].[numChartAcntId] )
--                    --AND dbo.General_Journal_Details.numEntryDateSortOrder1 >= @first_id
--            ORDER BY dbo.General_Journal_Details.numEntryDateSortOrder1 ASC

--            SET ROWCOUNT 0

    
--            IF OBJECT_ID('TEMPDB.DBO.#Temp1') IS NOT NULL
--                DROP TABLE #Temp1	
--        END
--    END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
-- USP_GetOpportunitySource 1
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOpportunitySource')
DROP PROCEDURE USP_GetOpportunitySource 
GO
CREATE PROCEDURE [dbo].[USP_GetOpportunitySource]
          @numDomainID NUMERIC(9)
AS
CREATE TABLE #temp (numItemID/*numListItemID*/ VARCHAR(50),vcItemName VARCHAR(100), constFlag BIT, bitDelete BIT,intSortOrder INT, numListItemID NUMERIC(9),vcData VARCHAR(100), vcListType VARCHAR(1000), vcOrderType VARCHAR(50))

INSERT INTO #temp
  SELECT '0~1','Internal Order', 1 AS  constFlag, 1 AS  bitDelete, 0 intSortOrder, -1 AS numListItemID,'Internal Order','Internal Order',''
  
  UNION
  
  SELECT CAST([numSiteID] AS VARCHAR(18)) +'~2',[vcSiteName], 1 AS  constFlag, 1 AS  bitDelete, 0 intSortOrder, CAST([numSiteID] AS VARCHAR(18)) AS numListItemID,[vcSiteName],[Sites].[vcSiteName],''
  FROM   Sites WHERE  [numDomainID] = @numDomainID
		
  UNION
  
  SELECT DISTINCT CAST(WebApiId  AS VARCHAR(18)) +'~3',vcProviderName,1 constFlag, 1 bitDelete, 0 intSortOrder, CAST(WebApiId  AS VARCHAR(18)) AS numListItemID,vcProviderName,[WebAPI].[vcProviderName],''
  FROM dbo.WebAPI   

  UNION
  
  SELECT CAST(Ld.numListItemID  AS VARCHAR(18)) +'~1', isnull(vcRenamedListName,vcData) as vcData, Ld.constFlag,bitDelete,isnull(intSortOrder,0) intSortOrder,CAST(Ld.numListItemID  AS VARCHAR(18)),isnull(vcRenamedListName,vcData) as vcData,
		(SELECT vcData FROM ListDetails WHERE numListId=9 and numListItemId = ld.numListType AND [ListDetails].[numDomainID] = @numDomainID),(CASE when ld.numListType=1 then 'Sales' when ld.numListType=2 then 'Purchase' else 'All' END) 
		FROM listdetails Ld  left join listorder LO 
		on Ld.numListItemID= LO.numListItemID and Lo.numDomainId = @numDomainID 
		WHERE Ld.numListID=9 and (constFlag=1 or Ld.numDomainID=@numDomainID )  
		 
SELECT * FROM #temp	ORDER BY bitDelete DESC
DROP TABLE #temp		 	

GO
--SELECT * FROM Category
--- Alterd By Chintan Prajapati
-- EXEC dbo.USP_ItemsForECommerce 9,60,1,1,15,0,''
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemsForECommerce' ) 
    DROP PROCEDURE USP_ItemsForECommerce
GO
CREATE PROCEDURE [dbo].[USP_ItemsForECommerce]
    @numSiteID AS NUMERIC(9) = 0,
    @numCategoryID AS NUMERIC(18) = 0,
    @numWareHouseID AS NUMERIC(9) = 0,
    @SortBy NVARCHAR(MAX) = 0,
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT = 0 OUTPUT,
    @SearchText AS NVARCHAR(MAX) = '',--Comma seperated item ids
    @FilterQuery AS NVARCHAR(MAX) = '',----Comma seperated Attributes Ids
    @FilterRegularCondition AS NVARCHAR(MAX),
    --@FilterCustomCondition AS NVARCHAR(MAX),
    @FilterCustomFields AS NVARCHAR(MAX),
    @FilterCustomWhere AS NVARCHAR(MAX)
   
AS 
    BEGIN
		
		DECLARE @numDomainID AS NUMERIC(9)
		SELECT  @numDomainID = numDomainID
        FROM    [Sites]
        WHERE   numSiteID = @numSiteID
        
        DECLARE @tintDisplayCategory AS TINYINT
		SELECT @tintDisplayCategory = ISNULL(bitDisplayCategory,0) FROM dbo.eCommerceDTL WHERE numSiteID = @numSiteId
		PRINT @tintDisplayCategory 
		
		CREATE TABLE #tmpItemCat(numCategoryID NUMERIC(18,0))
		CREATE TABLE #tmpItemCode(numItemCode NUMERIC(18,0))
		IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
		BEGIN
--			DECLARE @strCustomSql AS NVARCHAR(MAX)
--			SET @strCustomSql = 'INSERT INTO #tmpItemCode(numItemCode)
--								 SELECT DISTINCT t2.RecId FROM CFW_Fld_Master t1 
--								 JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID 
--								 WHERE t1.Grp_id = 5 
--								 AND t1.numDomainID = (SELECT numDomainID FROM dbo.Sites WHERE numSiteID = ' + CONVERT(VARCHAR(10),@numSiteID) + ') 
--								 AND t1.fld_type <> ''Link'' AND ' + @FilterCustomCondition
			
			DECLARE @FilterCustomFieldIds AS NVARCHAR(MAX)
			SET @FilterCustomFieldIds = Replace(@FilterCustomFields,'[','')
			SET @FilterCustomFieldIds = Replace(@FilterCustomFieldIds,']','')
			
			DECLARE @strCustomSql AS NVARCHAR(MAX)
			SET @strCustomSql = 'INSERT INTO #tmpItemCode(numItemCode)
								 SELECT DISTINCT T.RecId FROM 
															(
																SELECT * FROM 
																	(
																		SELECT  t2.RecId, t1.fld_id, t2.Fld_Value FROM CFW_Fld_Master t1 
																		JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID 
																		WHERE t1.Grp_id = 5 
																		AND t1.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
																		AND t1.fld_type <> ''Link'' 
																		AND t1.fld_id IN (' + @FilterCustomFieldIds + ')
																	) p  PIVOT (MAX([Fld_Value]) FOR fld_id IN (' + @FilterCustomFields + ')) AS pvt 
															) T  WHERE 1=1 AND ' + @FilterCustomWhere
								 
			PRINT @strCustomSql
			--EXEC SP_EXECUTESQL @strCustomSql								 					 
		END
		
        DECLARE @strSQL NVARCHAR(MAX)
        DECLARE @firstRec AS INTEGER
        DECLARE @lastRec AS INTEGER
        DECLARE @Where NVARCHAR(MAX)
        DECLARE @SortString NVARCHAR(MAX)
        DECLARE @searchPositionColumn NVARCHAR(MAX)
        DECLARE @row AS INTEGER
        DECLARE @data AS VARCHAR(100)
        DECLARE @dynamicFilterQuery NVARCHAR(MAX)
        DECLARE @checkFldType VARCHAR(100)
        DECLARE @count NUMERIC(9,0)
        DECLARE @whereNumItemCode NVARCHAR(MAX)
        DECLARE @filterByNumItemCode VARCHAR(100)
        
		SET @SortString = ''
        set @searchPositionColumn = ''
        SET @strSQL = ''
		SET @whereNumItemCode = ''
		SET @dynamicFilterQuery = ''
		 
        SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
        SET @lastRec = ( @CurrentPage * @PageSize + 1 )
                
        DECLARE @bitAutoSelectWarehouse AS BIT
		SELECT @bitAutoSelectWarehouse = ISNULL([ECD].[bitAutoSelectWarehouse],0) FROM [dbo].[eCommerceDTL] AS ECD WHERE [ECD].[numSiteId] = @numSiteID
		AND [ECD].[numDomainId] = @numDomainID

		DECLARE @vcWarehouseIDs AS VARCHAR(1000)
        IF (@numWareHouseID = 0 OR @bitAutoSelectWarehouse = 1)
            BEGIN				
				IF @bitAutoSelectWarehouse = 1
					BEGIN
						SELECT @vcWarehouseIDs = ISNULL( STUFF((SELECT DISTINCT ',' + CAST(numWarehouseID AS VARCHAR(10)) FROM [dbo].[Warehouses] WHERE [Warehouses].[numDomainID] = @numDomainID FOR XML PATH('')),1, 1, '') , '') 
					END
				
				ELSE
					BEGIN
						SELECT  @numWareHouseID = ISNULL(numDefaultWareHouseID,0)
						FROM    [eCommerceDTL]
						WHERE   [numDomainID] = @numDomainID	

						SET @vcWarehouseIDs = @numWareHouseID
					END
            END
		ELSE
		BEGIN
			SET @vcWarehouseIDs = @numWareHouseID
		END

		PRINT @vcWarehouseIDs

		--PRIAMRY SORTING USING CART DROPDOWN
		DECLARE @Join AS NVARCHAR(MAX)
		DECLARE @SortFields AS NVARCHAR(MAX)
		DECLARE @vcSort AS VARCHAR(10)

		SET @Join = ''
		SET @SortFields = ''
		SET @vcSort = ''

		IF @SortBy = ''
			BEGIN
				SET @SortString = ' vcItemName Asc '
			END
		ELSE IF CHARINDEX('~0',@SortBy) > 0 OR CHARINDEX('~1',@SortBy) > 0
			BEGIN
				DECLARE @fldID AS NUMERIC(18,0)
				DECLARE @RowNumber AS VARCHAR(MAX)
				
				SELECT * INTO #fldValues FROM dbo.SplitIDsWithPosition(@SortBy,'~')
				SELECT @RowNumber = RowNumber, @fldID = id FROM #fldValues WHERE RowNumber = 1
				SELECT @vcSort = CASE WHEN Id = 0 THEN 'ASC' ELSE 'DESC' END FROM #fldValues WHERE RowNumber = 2
				
				SET @Join = @Join + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber + ' ON I.numItemCode = CFVI' + @RowNumber + '.RecID AND CFVI' + @RowNumber + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID)
				---SET @SortString = 'CFVI' + @RowNumber + '.fld_Value' 
				SET @SortString = '[fld_Value' + @RowNumber + '] ' --+ @vcSort
				SET @SortFields = ', CFVI' + @RowNumber + '.fld_Value [fld_Value' + @RowNumber + ']'
			END
		ELSE
			BEGIN
				--SET @SortString = @SortBy	
				
				IF CHARINDEX('monListPrice',@SortBy) > 0
				BEGIN
					DECLARE @bitSortPriceMode AS BIT
					SELECT @bitSortPriceMode = ISNULL(bitSortPriceMode,0) FROM dbo.eCommerceDTL WHERE numSiteId = @numSiteID
					PRINT @bitSortPriceMode
					IF @bitSortPriceMode = 1
					BEGIN
						SET @SortString = ' '
					END
					ELSE
					BEGIN
						SET @SortString = @SortBy	
					END
				END
				ELSE
				BEGIN
					SET @SortString = @SortBy	
				END
				
			END	

		--SECONDARY SORTING USING DEFAULT SETTINGS FROM ECOMMERCE
		SELECT ROW_NUMBER() OVER (ORDER BY Custom,vcFieldName) AS [RowID],* INTO #tempSort FROM (
		SELECT  CONVERT(VARCHAR(9),numFieldID)+'~0' AS numFieldID,
				ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
				0 AS Custom,
				vcDbColumnName 
		FROM View_DynamicColumns
		WHERE numFormID = 84 
		AND ISNULL(bitSettingField,0) = 1 
		AND ISNULL(bitDeleted,0)=0 
		AND ISNULL(bitCustom,0) = 0
		AND numDomainID = @numDomainID
					       
		UNION     

		SELECT  CONVERT(VARCHAR(9),numFieldID)+'~1' AS numFieldID ,
				vcFieldName ,
				1 AS Custom,
				'' AS vcDbColumnName
		FROM View_DynamicCustomColumns          
		WHERE numFormID = 84
		AND numDomainID = @numDomainID
		AND grp_id = 5 
		AND vcAssociatedControlType = 'TextBox' 
		AND ISNULL(bitCustom,0) = 1 
		AND tintPageType=1  
		) TABLE1 ORDER BY Custom,vcFieldName
		
		--SELECT * FROM #tempSort
		DECLARE @DefaultSort AS NVARCHAR(MAX)
		DECLARE @DefaultSortFields AS NVARCHAR(MAX)
		DECLARE @DefaultSortJoin AS NVARCHAR(MAX)
		DECLARE @intCnt AS INT
		DECLARE @intTotalCnt AS INT	
		DECLARE @strColumnName AS VARCHAR(100)
		DECLARE @bitCustom AS BIT
		DECLARE @FieldID AS VARCHAR(100)				

		SET @DefaultSort = ''
		SET @DefaultSortFields = ''
		SET @DefaultSortJoin = ''		
		SET @intCnt = 0
		SET @intTotalCnt = ( SELECT COUNT(*) FROM #tempSort )
		SET @strColumnName = ''
		
		IF (SELECT ISNULL(bitEnableSecSorting,0) FROM EcommerceDtl WHERE numSiteID = @numSiteID ) = 1
		BEGIN
			CREATE TABLE #fldDefaultValues(RowNumber INT,Id VARCHAR(1000))

			WHILE(@intCnt < @intTotalCnt)
			BEGIN
				SET @intCnt = @intCnt + 1
				SELECT @FieldID = numFieldID, @strColumnName = vcDbColumnName, @bitCustom = Custom FROM #tempSort WHERE RowID = @intCnt
				
				--PRINT @FieldID
				--PRINT @strColumnName
				--PRINT @bitCustom

				IF @bitCustom = 0
					BEGIN
						SET @DefaultSort =  @strColumnName + ',' + @DefaultSort 
					END

				ELSE
					BEGIN
						DECLARE @fldID1 AS NUMERIC(18,0)
						DECLARE @RowNumber1 AS VARCHAR(MAX)
						DECLARE @vcSort1 AS VARCHAR(10)
						DECLARE @vcSortBy AS VARCHAR(1000)
						SET @vcSortBy = CONVERT(VARCHAR(100),@FieldID) + '~' + '0'
						--PRINT @vcSortBy

						INSERT INTO #fldDefaultValues(RowNumber,Id) SELECT RowNumber, id FROM dbo.SplitIDsWithPosition(@vcSortBy,'~')
						SELECT @RowNumber1 = RowNumber + 100 + @intCnt, @fldID1 = id FROM #fldDefaultValues WHERE RowNumber = 1
						SELECT @vcSort1 = 'ASC' 
						
						SET @DefaultSortJoin = @DefaultSortJoin + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber1 + ' ON I.numItemCode = CFVI' + @RowNumber1 + '.RecID AND CFVI' + @RowNumber1 + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID1)
						SET @DefaultSort = '[fld_Value' + @RowNumber1 + '] ' + ',' + @DefaultSort 
						SET @DefaultSortFields = @DefaultSortFields + ', CFVI' + @RowNumber1 + '.fld_Value [fld_Value' + @RowNumber1 + ']'
					END

			END
			IF LEN(@DefaultSort) > 0
			BEGIN
				SET  @DefaultSort = LEFT(@DefaultSort,LEN(@DefaultSort) - 1)
				SET @SortString = @SortString + ',' 
			END
			--PRINT @SortString
			--PRINT @DefaultSort
			--PRINT @DefaultSortFields
		END
--        IF @SortBy = 1 
--            SET @SortString = ' vcItemName Asc '
--        ELSE IF @SortBy = 2 
--            SET @SortString = ' vcItemName Desc '
--        ELSE IF @SortBy = 3 
--            SET @SortString = ' monListPrice Asc '
--        ELSE IF @SortBy = 4 
--            SET @SortString = ' monListPrice Desc '
--        ELSE IF @SortBy = 5 
--            SET @SortString = ' bintCreatedDate Desc '
--        ELSE IF @SortBy = 6 
--            SET @SortString = ' bintCreatedDate ASC '	
--        ELSE IF @SortBy = 7 --sort by relevance, invokes only when search
--            BEGIN
--                 IF LEN(@SearchText) > 0 
--		            BEGIN
--						SET @SortString = ' SearchOrder ASC ';
--					    set @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 						
--					END
--			END
--        ELSE 
--            SET @SortString = ' vcItemName Asc '
		
                                		
        IF LEN(ISNULL(@SearchText,'')) > 0
			BEGIN
				--PRINT 1
				IF CHARINDEX('SearchOrder',@SortString) > 0
				BEGIN
					SET @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 
				END

				SET @Where = ' INNER JOIN dbo.SplitIDsWithPosition('''+ @SearchText +''','','') AS SearchItems 
                        ON SearchItems.Id = I.numItemCode  and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID) + ' WHERE ISNULL(IsArchieve,0) = 0'
				END
		ELSE
			BEGIN
				--PRINT 2
				SET @Where = ' WHERE 1=1 '
							+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
							+ ' AND ISNULL(IsArchieve,0) = 0'
							
				IF @tintDisplayCategory = 1
				BEGIN
					
					;WITH CTE AS(
					SELECT C.numCategoryID,C.tintLevel,C.numDepCategory,CASE WHEN tintLevel=1 THEN 1 ELSE 0 END AS bitChild 
						FROM Category C WHERE (numCategoryID = @numCategoryID OR ISNULL(@numCategoryID,0) = 0) AND numDomainID = @numDomainID
					UNION ALL
					SELECT  C.numCategoryID,C.tintLevel,C.numDepCategory,CTE.bitChild FROM Category C JOIN CTE ON 
					C.numDepCategory=CTE.numCategoryID AND CTE.bitChild=1 AND numDomainID = @numDomainID)
					
					INSERT INTO #tmpItemCat(numCategoryID)												 
					SELECT DISTINCT C.numCategoryID FROM CTE C JOIN dbo.ItemCategory IC ON C.numCategoryID = IC.numCategoryID 
														WHERE numItemID IN ( 
																						
													 						SELECT numItemID FROM WareHouseItems WI
																			INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																			GROUP BY numItemID 
																			HAVING SUM(WI.numOnHand) > 0 
																			UNION ALL
													 						SELECT numItemID FROM WareHouseItems WI
																			INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																			GROUP BY numItemID 
																			HAVING SUM(WI.numAllocation) > 0
																		 )		
					SELECT numItemID INTO #tmpOnHandItems FROM 
					(													 					
						SELECT numItemID FROM WareHouseItems WI 
						INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
						GROUP BY numItemID 
						HAVING SUM(WI.numOnHand) > 0
						UNION ALL
						SELECT numItemID FROM WareHouseItems WI 
						INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
						GROUP BY numItemID 
						HAVING SUM(WI.numAllocation) > 0				
					) TABLE1
				END
			END
		
			--SELECT * FROM  #tmpOnHandItems
			--SELECT * FROM  #tmpItemCat
			
----		IF (SELECT COUNT(*) FROM #tmpItemCat) = 0 AND ISNULL(@numCategoryID,0) = 0
----		BEGIN
----			
----			INSERT INTO #tmpItemCat(numCategoryID)
----			SELECT DISTINCT C.numCategoryID FROM Category C
----			JOIN dbo.ItemCategory IC ON C.numCategoryID = IC.numCategoryID 
----							WHERE C.numDepCategory = (SELECT TOP 1 numCategoryID FROM dbo.Category 
----													  WHERE numDomainID = @numDomainID
----													  AND tintLevel = 1)
----							AND tintLevel <> 1
----							AND numDomainID = @numDomainID
----							AND numItemID IN (
----											 	SELECT numItemID FROM WareHouseItems WI
----												INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID
----												GROUP BY numItemID 
----												HAVING SUM(WI.numOnHand) > 0
----											 )
----			--SELECT * FROM #tmpItemCat								 
----		END
----		ELSE
----		BEGIN
----			INSERT INTO #tmpItemCat(numCategoryID)VALUES(@numCategoryID)
----		END

		       SET @strSQL = @strSQL + ' WITH Items AS 
	                                     ( 
	                                     SELECT   
                                               I.numItemCode,
                                               ROW_NUMBER() OVER ( Partition by I.numItemCode Order by I.numItemCode) RowID,
                                               vcItemName,
                                               I.bintCreatedDate,
                                               vcManufacturer,
                                               C.vcCategoryName,
                                               ISNULL(txtItemDesc,'''') txtItemDesc,
                                               ISNULL(CASE  WHEN I.[charItemType] = ''P''
															THEN CASE WHEN I.bitSerialized = 1
																	  THEN ( UOM * monListPrice )
																	  ELSE ( UOM * ISNULL(W1.[monWListPrice], 0) )
																 END
															ELSE ( UOM * monListPrice )
													  END, 0) monListPrice,
                                               vcSKU,
                                               fltHeight,
                                               fltWidth,
                                               fltLength,
                                               IM.vcPathForImage,
                                               IM.vcPathForTImage,
                                               vcModelID,
                                               fltWeight,
                                               bitFreeShipping,
                                               UOM AS UOMConversionFactor,
												( CASE WHEN charItemType <> ''S'' AND charItemType <> ''N''
													   THEN ( CASE WHEN bitShowInStock = 1
																   THEN ( CASE WHEN ( ISNULL(W.numOnHand,0)<=0) THEN ''<font color=red>Out Of Stock</font>''
																			   WHEN bitAllowBackOrder = 1 THEN ''In Stock''
																			   ELSE ''In Stock'' 
																		  END )
																   ELSE ''''
															  END )
													   ELSE ''''
												  END ) AS InStock, C.vcDescription as CategoryDesc, ISNULL(W1.numWareHouseItemID,0) [numWareHouseItemID] ' 
								               +	@searchPositionColumn + @SortFields + @DefaultSortFields + '
								
                                         FROM      
                                                      Item AS I
                                         INNER JOIN   ItemCategory IC   ON   I.numItemCode = IC.numItemID
                                         LEFT JOIN    ItemImages IM     ON   I.numItemCode = IM.numItemCode AND IM.bitdefault=1 AND IM.bitIsImage = 1
                                         INNER JOIN   Category C        ON   IC.numCategoryID = C.numCategoryID
                                         INNER JOIN   SiteCategories SC ON   IC.numCategoryID = SC.numCategoryID
                                         INNER JOIN   eCommerceDTL E    ON   E.numDomainID = I.numDomainID ' + @Join + ' ' + @DefaultSortJoin + ' 
                                         CROSS APPLY (SELECT SUM(numOnHand) numOnHand FROM WareHouseItems W 
													  WHERE  W.numItemID = I.numItemCode
                                                      AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
                                                      AND W.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + ') AS W  
										 CROSS APPLY (SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems W 
													  WHERE  W.numItemID = I.numItemCode 
													  AND W.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
													  AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))) AS W1
										 CROSS APPLY (SELECT SUM(numAllocation) numAllocation FROM WareHouseItems W2 
													  WHERE  W2.numItemID = I.numItemCode
                                                      AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
                                                      AND W2.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + ') AS W2  			  
										 CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numSaleUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOM) AS UOM '

			IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
			BEGIN
				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCode TP ON TP.numItemCode = I.numItemCode ' 
			END
			
			IF (SELECT COUNT(*) FROM #tmpItemCat) > 0
			BEGIN
				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCat TC ON TC.numCategoryID = IC.numCategoryID '
				SET @strSQL = @strSQL + ' LEFT JOIN #tmpOnHandItems TOIC ON TOIC.numItemID = I.numItemCode '
			END
			ELSE IF @numCategoryID>0
			BEGIN
				SET @Where = REPLACE(@Where,'WHERE',' WHERE IC.numCategoryID = ' + CONVERT(VARCHAR(10),@numCategoryID) + ' AND ')
			END 
			
			IF @tintDisplayCategory = 1
			BEGIN
				SET @Where = @Where + ' AND (ISNULL(W.numOnHand, 0) > 0 OR ISNULL(W2.numAllocation, 0) > 0) '
			END
			
			--PRINT @Where
			SET @strSQL = @strSQL + @Where
			
			IF LEN(@FilterRegularCondition) > 0
			BEGIN
				SET @strSQL = @strSQL + @FilterRegularCondition
			END                                         
			
---PRINT @SortString + ',' + @DefaultSort + ' ' + @vcSort 
	
            IF LEN(@whereNumItemCode) > 0
                                        BEGIN
								              SET @strSQL = @strSQL + @filterByNumItemCode	
								    	END
            SET @strSQL = @strSQL +     '   )
                                        , ItemSorted AS (SELECT  ROW_NUMBER() OVER ( ORDER BY  ' + @SortString + @DefaultSort + ' ' + @vcSort + ' ) AS Rownumber,* FROM  Items WHERE RowID = 1)'
            
            DECLARE @fldList AS NVARCHAR(MAX)
			SELECT @fldList=COALESCE(@fldList + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_C') FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=5
            
            SET @strSQL = @strSQL +' SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,InStock,CategoryDesc,numWareHouseItemID
                                     INTO #tempItem FROM ItemSorted ' 
--                                        + 'WHERE Rownumber > ' + CONVERT(VARCHAR(10), @firstRec) + '
--                                        AND   Rownumber < ' + CONVERT(VARCHAR(10), @lastRec) + '
--                                        order by Rownumber;' 
            
            SET @strSQL = @strSQL + ' SELECT * INTO #tmpPagedItems FROM #tempItem WHERE Rownumber > ' + CONVERT(VARCHAR(10), @firstRec) + '
                                      AND   Rownumber < ' + CONVERT(VARCHAR(10), @lastRec) + '
                                      order by Rownumber '
                                                                  
            IF LEN(ISNULL(@fldList,''))>0
            BEGIN
				PRINT 'flds1'
				SET @strSQL = @strSQL +'SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,InStock,CategoryDesc,numWareHouseItemID,' + @fldList + '
                                        FROM #tmpPagedItems I left join (
			SELECT  t2.RecId, t1.Fld_label + ''_C'' as Fld_label, 
				CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
								 WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
								 WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
								 ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
			JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 5 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
			AND t2.RecId IN (SELECT numItemCode FROM #tmpPagedItems )
			) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList + ')) AS pvt on I.numItemCode=pvt.RecId order by Rownumber;' 	
			END 
			ELSE
			BEGIN
				PRINT 'flds2'
				SET @strSQL = @strSQL +' SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,InStock,CategoryDesc,numWareHouseItemID
                                         FROM  #tmpPagedItems order by Rownumber;'
			END               
                                        
            
--            PRINT(@strSQL)        
--            EXEC ( @strSQL) ; 
			                           
            SET @strSQL = @strSQL + ' SELECT @TotRecs = COUNT(*) FROM #tempItem '                            
--             SET @strSQL = 'SELECT @TotRecs = COUNT(*) 
--                           FROM Item AS I
--                           INNER JOIN   ItemCategory IC ON I.numItemCode = IC.numItemID
--                           INNER JOIN   Category C ON IC.numCategoryID = C.numCategoryID
--                           INNER JOIN   SiteCategories SC ON IC.numCategoryID = SC.numCategoryID ' --+ @Where
--             
--			 IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
--			 BEGIN
--				 SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCode TP ON TP.numItemCode = I.numItemCode ' 
--			 END
--			
--			 IF (SELECT COUNT(*) FROM #tmpItemCat) > 0
--			 BEGIN
--				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCat TC ON TC.numCategoryID = IC.numCategoryID '
--				SET @strSQL = @strSQL + ' INNER JOIN #tmpOnHandItems TOIC ON TOIC.numItemID = I.numItemCode '
--			 END
--			 ELSE IF @numCategoryID>0
--			 BEGIN
--				SET @Where = REPLACE(@Where,'WHERE',' WHERE IC.numCategoryID = ' + CONVERT(VARCHAR(10),@numCategoryID) + ' AND ')
--			 END 
--			
--			 SET @strSQL = @strSQL + @Where  
--			 
--			 IF LEN(@FilterRegularCondition) > 0
--			 BEGIN
--				SET @strSQL = @strSQL + @FilterRegularCondition
--			 END 
			
			 PRINT  @strSQL     
             EXEC sp_executeSQL @strSQL, N'@TotRecs INT OUTPUT', @TotRecs OUTPUT
             
             
        DECLARE @tintOrder AS TINYINT                                                  
		DECLARE @vcFormFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(1)                                             
		DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
		DECLARE @numListID AS NUMERIC(9)                                                  
		DECLARE @WhereCondition VARCHAR(2000)                       
		DECLARE @numFormFieldId AS NUMERIC  
		DECLARE @vcFieldType CHAR(1)
		                  
		SET @tintOrder=0                                                  
		SET @WhereCondition =''                 
                   
              
		CREATE TABLE #tempAvailableFields(numFormFieldId  NUMERIC(9),vcFormFieldName NVARCHAR(50),
		vcFieldType CHAR(1),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),
		vcListItemType CHAR(1),intRowNum INT,vcItemValue VARCHAR(3000))
		   
		INSERT INTO #tempAvailableFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
				,numListID,vcListItemType,intRowNum)                         
		SELECT  Fld_id,REPLACE(Fld_label,' ','') AS vcFormFieldName,
			   CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType,
				fld_type as vcAssociatedControlType,
				ISNULL(C.numListID, 0) numListID,
				CASE WHEN C.numListID > 0 THEN 'L'
					 ELSE ''
				END vcListItemType,ROW_NUMBER() OVER (ORDER BY Fld_id ASC) AS intRowNum
		FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
		WHERE   C.numDomainID = @numDomainId
				AND GRP_ID IN (5)

		  
		SELECT * FROM #tempAvailableFields
		
		/*
		DROP TABLE #tempAvailableFields
		DROP TABLE #tmpItemCode
		DROP TABLE #tmpItemCat
		*/	
		
		IF OBJECT_ID('tempdb..#tempAvailableFields') IS NOT NULL
		DROP TABLE #tempAvailableFields
		
		IF OBJECT_ID('tempdb..#tmpItemCode') IS NOT NULL
		DROP TABLE #tmpItemCode
		
		IF OBJECT_ID('tempdb..#tmpItemCat') IS NOT NULL
		DROP TABLE #tmpItemCat
		
		IF OBJECT_ID('tempdb..#fldValues') IS NOT NULL
		DROP TABLE #fldValues
		
		IF OBJECT_ID('tempdb..#tempSort') IS NOT NULL
		DROP TABLE #tempSort
		
		IF OBJECT_ID('tempdb..#fldDefaultValues') IS NOT NULL
		DROP TABLE #fldDefaultValues
		
		IF OBJECT_ID('tempdb..#tmpOnHandItems') IS NOT NULL
		DROP TABLE #tmpOnHandItems

		IF OBJECT_ID('tempdb..#tmpPagedItems') IS NOT NULL
		DROP TABLE #tmpPagedItems
    END


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageCheckHeader')
DROP PROCEDURE USP_ManageCheckHeader
GO
CREATE PROCEDURE USP_ManageCheckHeader
	@tintMode AS TINYINT, 
    @numDomainID numeric(18, 0),
    @numCheckHeaderID NUMERIC(18,0),
    @numChartAcntId numeric(18, 0),
    @numDivisionID numeric(18, 0),
    @monAmount money,
    @numCheckNo numeric(18, 0),
    @tintReferenceType tinyint,
    @numReferenceID numeric(18, 0),
    @dtCheckDate datetime,
    @bitIsPrint bit,
    @vcMemo varchar(1000),
    @numUserCntID numeric(18, 0)
AS
	
	--Validation of closed financial year
	EXEC dbo.USP_ValidateFinancialYearClosingDate @numDomainID,@dtCheckDate
	
	IF @tintMode=1 --SELECT Particulat Check Header
	BEGIN
			SELECT CH.numCheckHeaderID, CH.numChartAcntId, CH.numDomainID, CH.numDivisionID, CH.monAmount, CH.numCheckNo, CH.tintReferenceType, CH.numReferenceID, 
			CH.dtCheckDate, CH.bitIsPrint, CH.vcMemo, CH.numCreatedBy, CH.dtCreatedDate, CH.numModifiedBy,
			 CH.dtModifiedDate,ISNULL(GJD.numTransactionId,0) AS numTransactionId
	FROM   dbo.CheckHeader CH LEFT OUTER JOIN General_Journal_Details GJD ON GJD.tintReferenceType=1 AND GJD.numReferenceID=CH.numCheckHeaderID 
	WHERE  numCheckHeaderID = @numCheckHeaderID AND CH.numDomainID = @numDomainID	

	END
	ELSE IF @tintMode=2 --Insert/Update Check Header
	BEGIN
		IF @numCheckHeaderID>0 --Update
			BEGIN
			UPDATE dbo.CheckHeader
				SET    numChartAcntId = @numChartAcntId, numDivisionID = @numDivisionID, monAmount = @monAmount, numCheckNo = @numCheckNo, tintReferenceType = @tintReferenceType, numReferenceID = @numReferenceID, dtCheckDate = @dtCheckDate, bitIsPrint = @bitIsPrint, vcMemo = @vcMemo, numModifiedBy = @numUserCntID, dtModifiedDate = GETUTCDATE()
				WHERE  numCheckHeaderID = @numCheckHeaderID AND numDomainID = @numDomainID

			END
		ELSE --Insert
			BEGIN
					  --Set Default Class If enable User Level Class Accountng 
					  DECLARE @numAccountClass AS NUMERIC(18);SET @numAccountClass=0
					  
					  IF @tintReferenceType=1 --Only for Direct Check
					  BEGIN
						  SELECT @numAccountClass=ISNULL(numDefaultClass,0) FROM dbo.UserMaster UM JOIN dbo.Domain D ON UM.numDomainID=D.numDomainId
						  WHERE D.numDomainId=@numDomainId AND UM.numUserDetailId=@numUserCntID AND ISNULL(D.IsEnableUserLevelClassTracking,0)=1
					  END
					  
					INSERT INTO dbo.CheckHeader (numChartAcntId, numDomainID, numDivisionID, monAmount, numCheckNo, tintReferenceType, numReferenceID, dtCheckDate, bitIsPrint, vcMemo, numCreatedBy, dtCreatedDate, numAccountClass)
					SELECT @numChartAcntId, @numDomainID, @numDivisionID, @monAmount, @numCheckNo, @tintReferenceType, @numReferenceID, @dtCheckDate, @bitIsPrint, @vcMemo, @numUserCntID, GETUTCDATE(), @numAccountClass

					SET @numCheckHeaderID=SCOPE_IDENTITY()
			END	
			
				SELECT numCheckHeaderID, numChartAcntId, numDomainID, numDivisionID, monAmount, numCheckNo, tintReferenceType, numReferenceID, dtCheckDate, bitIsPrint, vcMemo, numCreatedBy, dtCreatedDate, numModifiedBy, dtModifiedDate
				FROM   dbo.CheckHeader WHERE  numCheckHeaderID = @numCheckHeaderID AND numDomainID = @numDomainID	
	   END
	ELSE IF @tintMode=3 --Print Check List
	BEGIN
			SELECT CH.numCheckHeaderID, CH.numChartAcntId, CH.numDomainID, CH.numDivisionID, CH.monAmount, CH.numCheckNo, CH.tintReferenceType, CH.numReferenceID, 
			CH.dtCheckDate, CH.bitIsPrint, CH.vcMemo, CH.numCreatedBy, CH.dtCreatedDate, CH.numModifiedBy,
			 CH.dtModifiedDate, 
			 CASE CH.tintReferenceType WHEN 11 THEN dbo.fn_GetUserName(ISNULL(PD.numUserCntID,0))
				 ELSE dbo.fn_GetComapnyName(ISNULL(CH.numDivisionID,0)) END AS vcCompanyName,
			 CASE CH.tintReferenceType WHEN 1 THEN 'Checks' 
				  WHEN 8 THEN 'Bill Payment'
				  WHEN 10 THEN 'RMA'
				  WHEN 11 THEN 'Payroll' END AS vcReferenceType,
				  dbo.fn_GetContactName(CH.numCreatedBy) AS EmployerName,
				  CASE CH.tintReferenceType WHEN 1 THEN ''
					WHEN 8 THEN (SELECT ISNULL((SELECT SUBSTRING((SELECT ',' + CASE WHEN ISNULL(BPD.numBillID,0)>0 THEN 'Bill-' + Cast(BPD.numBillID as varchar(18))  ELSE Cast(OBD.vcBizDocID as varchar(18)) END FROM
	 BillPaymentHeader BPH JOIN BillPaymentDetails BPD ON BPH.numBillPaymentID=BPD.numBillPaymentID 
	 LEFT JOIN OpportunityBizDocs OBD ON OBD.numOppBizDocsId=BPD.numOppBizDocsID WHERE BPH.numDomainID=CH.numDomainID AND BPH.numBillPaymentID= CH.numReferenceID
	 FOR XML PATH('')),2,200000)),'')) END  AS vcBizDoc,
	 CASE CH.tintReferenceType WHEN 1 THEN ''
					WHEN 8 THEN (SELECT ISNULL((SELECT SUBSTRING((SELECT ',' + CASE WHEN BPD.numOppBizDocsID>0 THEN Cast(ISNULL(OBD.vcRefOrderNo,'') as varchar(18)) WHEN BPD.numBillID>0 THEN Cast(ISNULL(BH.vcReference,'') as varchar(500)) END  FROM
	 BillPaymentHeader BPH JOIN BillPaymentDetails BPD ON BPH.numBillPaymentID=BPD.numBillPaymentID 
	 LEFT JOIN OpportunityBizDocs OBD ON OBD.numOppBizDocsId=BPD.numOppBizDocsID
	 LEFT JOIN BillHeader BH ON BPD.numBillID=BH.numBillID WHERE BPH.numDomainID=CH.numDomainID AND BPH.numBillPaymentID= CH.numReferenceID
	 FOR XML PATH('')),2,200000)),'')) ELSE '' END  AS vcRefOrderNo
	FROM   dbo.CheckHeader CH 
	LEFT JOIN PayrollDetail PD ON CH.numReferenceID=PD.numPayrollDetailID AND CH.tintReferenceType=11
	WHERE CH.numDomainID = @numDomainID AND (numCheckHeaderID = @numCheckHeaderID OR @numCheckHeaderID=0) 
		AND (CH.tintReferenceType = @tintReferenceType OR @tintReferenceType=0)
		AND (CH.numReferenceID = @numReferenceID OR @numReferenceID=0)	 
		AND CH.numChartAcntId = @numChartAcntId AND ISNULL(bitIsPrint,0)=0	 
	END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageReturnBizDocs' ) 
    DROP PROCEDURE USP_ManageReturnBizDocs
GO

CREATE PROCEDURE [dbo].[USP_ManageReturnBizDocs]
    (
      @numReturnHeaderID NUMERIC(9) = 0,
      @tintReceiveType TINYINT,
      @numReturnStatus NUMERIC(9),
      @numDomainId NUMERIC(9),
      @numUserCntID NUMERIC(9),
      @numAccountID NUMERIC(9),
      @vcCheckNumber VARCHAR(50),
      @IsCreateRefundReceipt BIT,
      @numItemCode	NUMERIC(18,0)
    )
AS 
    BEGIN
    DECLARE @tintType TINYINT,@tintReturnType TINYINT
        
    SELECT @tintReturnType=tintReturnType FROM ReturnHeader WHERE numReturnHeaderID = @numReturnHeaderID    
    
	PRINT @tintReturnType
	PRINT @tintReceiveType

    IF @tintReturnType=1 OR @tintReturnType=2 
        BEGIN
        BEGIN TRY 
           EXEC usp_ManageRMAInventory @numReturnHeaderID,@numDomainId,@numUserCntID,1 
           
           UPDATE WareHouseItmsDTL SET tintStatus=(CASE WHEN @tintReturnType=1 THEN 0 ELSE tintStatus END)
				WHERE numWareHouseItmsDTLID IN (SELECT numWareHouseItmsDTLID FROM OppWarehouseSerializedItem WHERE numReturnHeaderID=@numReturnHeaderID)
        END TRY 
		BEGIN CATCH		
			DECLARE @strMsg VARCHAR(200)
			SET @strMsg = ERROR_MESSAGE()
			RAISERROR ( @strMsg, 16, 1 ) ;
            RETURN 
	    END CATCH	
    END   
    
		SET @tintType=CASE When @tintReturnType=1 AND @tintReceiveType=1 THEN 5 
				       When @tintReturnType=1 AND @tintReceiveType=2 THEN 3 
				       When @tintReturnType=2 AND @tintReceiveType=2 THEN 4
				       ELSE 0 END 
		
		PRINT @tintType

		IF 	@tintType>0
		BEGIN
			EXEC dbo.USP_UpdateBizDocNameTemplate @tintType, @numDomainID,@numReturnHeaderID
        END    
    	
        DECLARE @numBizdocTempID AS NUMERIC(18, 0);SET @numBizdocTempID=0
        
        IF @tintReturnType=1 OR @tintReturnType=2 
        BEGIN
        IF @tintReceiveType = 2 AND @tintReturnType=1
             BEGIN
               SELECT TOP 1 @numBizdocTempID = numBizDocTempID FROM    BizDocTemplate
                 WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
                         AND numDomainID = @numDomainID AND numBizDocID IN (SELECT TOP 1 numListItemID
                         FROM    dbo.ListDetails WHERE   vcData = 'Sales Credit Memo' AND constFlag = 1 )
             END
		ELSE IF @tintReturnType=1 OR @tintReceiveType=1
           BEGIN 
              SELECT TOP 1 @numBizdocTempID = numBizdocTempID FROM BizDocTemplate
                 WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1 AND numDomainID = @numDomainID
						AND numBizDocID = ( SELECT TOP 1 numListItemID 
                        FROM    dbo.ListDetails WHERE   vcData = 'Refund Receipt' AND constFlag = 1)
            END
       ELSE IF @tintReceiveType = 2 AND @tintReturnType=2
             BEGIN
                 SELECT TOP 1 @numBizdocTempID = numBizDocTempID FROM    BizDocTemplate
                  WHERE   numOppType = 2 AND bitDefault = 1 AND bitEnabled = 1
                          AND numDomainID = @numDomainID AND numBizDocID IN (SELECT TOP 1 numListItemID
                          FROM    dbo.ListDetails WHERE   vcData = 'Purchase Credit Memo' AND constFlag = 1 )
             END
             
		END
		
		DECLARE @monAmount AS money,@monTotalTax  AS money,@monTotalDiscount AS  money  
		
		SET @tintType=CASE When @tintReturnType=1 AND @tintReceiveType=1 THEN 9 
				       When @tintReturnType=1 AND @tintReceiveType=2 THEN 7 
				       When @tintReturnType=1 AND @tintReceiveType=3 THEN 9 
				       When @tintReturnType=2 AND @tintReceiveType=2 THEN 8
				       When @tintReturnType=3 THEN 10
				       When @tintReturnType=4 THEN 9 END 
				       
		SELECT @monAmount=monAmount,@monTotalTax=monTotalTax,@monTotalDiscount=monTotalDiscount FROM dbo.GetReturnDealAmount(@numReturnHeaderID,@tintType)
		
        UPDATE  dbo.ReturnHeader
        SET     tintReceiveType = @tintReceiveType,
                numReturnStatus = @numReturnStatus,
                numModifiedBy = @numUserCntID,
                dtModifiedDate = GETUTCDATE(),
                numAccountID = @numAccountID,
                vcCheckNumber = @vcCheckNumber,
                IsCreateRefundReceipt = @IsCreateRefundReceipt,
                numBizdocTempID = CASE When @numBizdocTempID>0 THEN @numBizdocTempID ELSE numBizdocTempID END,
                monBizDocAmount= @monAmount + @monTotalTax - @monTotalDiscount,
                numItemCode = @numItemCode
        WHERE   numReturnHeaderID = @numReturnHeaderID
        
		DECLARE @numDepositIDRef AS NUMERIC(18,0)
		SELECT @numDepositIDRef = ISNULL(numDepositIDRef,0) FROM [dbo].[ReturnHeader] AS RH WHERE [RH].[numReturnHeaderID] = @numReturnHeaderID
		PRINT @numDepositIDRef

		IF @numDepositIDRef > 0
			BEGIN
				IF NOT EXISTS(SELECT * FROM [dbo].[DepositeDetails] AS DD WHERE [DD].[numDepositID] = @numDepositIDRef) AND @tintReceiveType = 1 AND @tintReturnType = 4
					BEGIN
						--SELECT * FROM [dbo].[DepositMaster] AS DM WHERE [DM].[numDepositId] = @numDepositIDRef
						--DELETE FROM [dbo].[DepositMaster] WHERE [numDepositId] = @numDepositIDRef
						--UPDATE [dbo].[DepositMaster] SET [monAppliedAmount] = ISNULL((SELECT monBizDocAmount FROM [dbo].[ReturnHeader] AS RH WHERE [RH].[numReturnHeaderID] = @numReturnHeaderID),0) 
						--								 ,[monRefundAmount] = [monDepositAmount] - @monAmount
						--WHERE [numDepositId] = @numDepositIDRef

						UPDATE dbo.DepositMaster SET [monAppliedAmount] = ISNULL((SELECT SUM([ReturnHeader].[monAmount]) FROM [dbo].[ReturnHeader] 
																				WHERE [ReturnHeader].[numDepositIDRef] = @numDepositIDRef),0)
													 ,[numReturnHeaderID] = @numReturnHeaderID	
													 ,[monRefundAmount] = [monDepositAmount] - ISNULL((SELECT SUM([ReturnHeader].[monAmount]) FROM [dbo].[ReturnHeader] WHERE [ReturnHeader].[numDepositIDRef] = @numDepositIDRef),0) 
						WHERE numDepositId=@numDepositIDRef

					END
				ELSE
					BEGIN
						UPDATE dbo.DepositMaster SET [monAppliedAmount] = @monAmount + ISNULL((SELECT SUM([DD].[monAmountPaid]) FROM [dbo].[DepositeDetails] AS DD WHERE [DD].[numDepositID] = @numDepositIDRef),0)
													 ,[monRefundAmount] = [monDepositAmount] - ISNULL((SELECT SUM([DD].[monAmountPaid]) FROM [dbo].[DepositeDetails] AS DD WHERE [DD].[numDepositID] = @numDepositIDRef),0)
						WHERE numDepositId=@numDepositIDRef						
					END
			END
		
		--DECLARE @numBillPaymentIDRef AS NUMERIC(18,0)
		--SELECT @numBillPaymentIDRef = ISNULL(numBillPaymentIDRef,0) FROM [dbo].[ReturnHeader] AS RH WHERE [RH].[numReturnHeaderID] = @numReturnHeaderID
		--PRINT @numBillPaymentIDRef

		--IF @numBillPaymentIDRef > 0
		--	BEGIN
		--		IF @tintReceiveType = 1 AND @tintReturnType = 4
		--			BEGIN
		--				UPDATE [dbo].[BillPaymentHeader] SET [monAppliedAmount] = ISNULL((SELECT RH.[monBizDocAmount] FROM [dbo].[ReturnHeader] AS RH WHERE [RH].[numReturnHeaderID] = @numReturnHeaderID),0), 
		--													 [monRefundAmount] = ISNULL((SELECT [RH].[monAmount] FROM [dbo].[ReturnHeader] AS RH WHERE [RH].[numReturnHeaderID] = @numReturnHeaderID),0)
		--				WHERE [numBillPaymentID] = @numBillPaymentIDRef
		--			END
		--	END
    END

GO
/****** Object:  StoredProcedure [USP_ManageReturnHeaderItems]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageReturnHeaderItems' ) 
    DROP PROCEDURE USP_ManageReturnHeaderItems
GO
CREATE PROCEDURE [USP_ManageReturnHeaderItems]
    (
      @numReturnHeaderID NUMERIC(18, 0) OUTPUT,
      @vcRMA VARCHAR(100),
      @vcBizDocName VARCHAR(100),
      @numDomainId NUMERIC(18, 0),
      @numUserCntID AS NUMERIC(9) = 0,
      @numDivisionId NUMERIC(18, 0),
      @numContactId NUMERIC(18, 0),
      @numOppId NUMERIC(18, 0),
      @tintReturnType TINYINT,
      @numReturnReason NUMERIC(18, 0),
      @numReturnStatus NUMERIC(18, 0),
      @monAmount MONEY,
      @monTotalTax MONEY,
      @monTotalDiscount MONEY,
      @tintReceiveType TINYINT,
      @vcComments TEXT,
      @strItems TEXT = '',
      @tintMode TINYINT = 0,
      @numBillAddressId NUMERIC(18, 0),
      @numShipAddressId NUMERIC(18, 0),
      @numDepositIDRef NUMERIC(18,0),
      @numBillPaymentIDRef NUMERIC(18,0),
	  @numParentID NUMERIC(18,0) = 0
    )
AS 
    BEGIN 
        DECLARE @hDocItem INT                                                                                                                                                                
 
        IF @numReturnHeaderID = 0 
            BEGIN             
				--Set Default Class If enable User Level Class Accountng 
				  DECLARE @numAccountClass AS NUMERIC(18);SET @numAccountClass=0
				  SELECT @numAccountClass=ISNULL(numDefaultClass,0) FROM dbo.UserMaster UM JOIN dbo.Domain D ON UM.numDomainID=D.numDomainId
				  WHERE D.numDomainId=@numDomainId AND UM.numUserDetailId=@numUserCntID AND ISNULL(D.IsEnableUserLevelClassTracking,0)=1
                   
                       INSERT  INTO [ReturnHeader]([vcRMA],[vcBizDocName],[numDomainId],[numDivisionId],[numContactId],
                                  [numOppId],[tintReturnType],[numReturnReason],[numReturnStatus],[monAmount],
                                  [monTotalTax],[monTotalDiscount],[tintReceiveType],[vcComments],
                                  [numCreatedBy],[dtCreatedDate],monBizDocAmount,monBizDocUsedAmount,numDepositIDRef,numBillPaymentIDRef,numAccountClass,numParentID,IsUnappliedPayment)
                                SELECT  @vcRMA,@vcBizDocName,@numDomainId,@numDivisionId,@numContactId,@numOppId,
                                        @tintReturnType,@numReturnReason,@numReturnStatus,@monAmount,
                                        @monTotalTax,@monTotalDiscount,@tintReceiveType,@vcComments,@numUserCntID,GETUTCDATE(),0,0,@numDepositIDRef,@numBillPaymentIDRef,@numAccountClass,
										@numParentID,
										(CASE WHEN (SELECT [DM].[tintDepositePage] FROM [dbo].[DepositMaster] AS DM 
													WHERE DM.[numDepositId] = @numDepositIDRef AND [DM].[numDomainId] = @numDomainId) = 2 THEN 1
											  ELSE 0
										END)
                    
                        SET @numReturnHeaderID = SCOPE_IDENTITY()                                        
                        SELECT  @numReturnHeaderID

                        --Update DepositMaster if Refund UnApplied Payment
                        IF @numDepositIDRef>0 AND @tintReturnType=4
                        BEGIN
							UPDATE dbo.DepositMaster SET monRefundAmount=@monAmount WHERE numDepositId=@numDepositIDRef
						END
						
						--Update BillPaymentHeader if Refund UnApplied Payment
						IF @numBillPaymentIDRef>0 AND @tintReturnType=4
						BEGIN
							UPDATE dbo.BillPaymentHeader SET monRefundAmount=@monAmount WHERE numBillPaymentID=@numBillPaymentIDRef
						END
						                        
      --                  --Update DepositMaster if Refund UnApplied Payment
      --                  IF @numDepositIDRef>0 AND @tintReturnType=4
      --                  BEGIN
						--	IF EXISTS(SELECT numDepositIDRef FROM [dbo].[ReturnHeader] AS RH WHERE [RH].numDepositIDRef=@numDepositIDRef)
						--	BEGIN
						--		UPDATE dbo.DepositMaster SET monRefundAmount = [monDepositAmount] - @monAmount + (ISNULL((SELECT SUM([ReturnHeader].[monAmount]) FROM [dbo].[ReturnHeader] WHERE [ReturnHeader].[numDepositIDRef] = @numDepositIDRef),0))
						--		--,[numReturnHeaderID] = @numReturnHeaderID 
						--		WHERE numDepositId=@numDepositIDRef
						--	END
						--	ELSE
						--	BEGIN
						--		UPDATE dbo.DepositMaster SET monRefundAmount = @monAmount
						--		--,[numReturnHeaderID] = @numReturnHeaderID 
						--		WHERE numDepositId=@numDepositIDRef
						--	END
						--END
						
						----Update BillPaymentHeader if Refund UnApplied Payment
						--IF @numBillPaymentIDRef>0 AND @tintReturnType=4
						--BEGIN
						--	UPDATE dbo.BillPaymentHeader SET monRefundAmount=@monAmount, [numReturnHeaderID] = @numReturnHeaderID WHERE numBillPaymentID=@numBillPaymentIDRef
						--END
						
                        DECLARE @tintType TINYINT 
                        SET @tintType=CASE When @tintReturnType=1 OR @tintReturnType=2 THEN 7 
												WHEN @tintReturnType=3 THEN 6
												WHEN @tintReturnType=4 THEN 5 END
												
                        EXEC dbo.USP_UpdateBizDocNameTemplate
								@tintType =  @tintType, --  tinyint
								@numDomainID = @numDomainID, --  numeric(18, 0)
								@RecordID = @numReturnHeaderID --  numeric(18, 0)

                        DECLARE @numRMATempID AS NUMERIC(18, 0)
                        DECLARE @numBizdocTempID AS NUMERIC(18, 0)
                        
                        --                            IF @tintReceiveType = 1 
--                            BEGIN 
--                                SELECT TOP 1 @numBizdocTempID = numBizdocTempID FROM BizDocTemplate
--                                WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1 AND numDomainID = @numDomainID
--                                        AND numBizDocID = ( SELECT TOP 1 numListItemID 
--                                        FROM    dbo.ListDetails WHERE   vcData = 'Refund Receipt' AND constFlag = 1)
--                            END
--							ELSE IF @tintReceiveType = 2 AND @tintReturnType=1
--                            BEGIN
--                                SELECT TOP 1 @numBizdocTempID = numBizDocTempID FROM    BizDocTemplate
--                                WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
--                                        AND numDomainID = @numDomainID AND numBizDocID IN (SELECT TOP 1 numListItemID
--                                        FROM    dbo.ListDetails WHERE   vcData = 'Sales Credit Memo' AND constFlag = 1 )
--                            END
--                            ELSE IF @tintReceiveType = 2 AND @tintReturnType=2
--                            BEGIN
--                                SELECT TOP 1 @numBizdocTempID = numBizDocTempID FROM    BizDocTemplate
--                                WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
--                                        AND numDomainID = @numDomainID AND numBizDocID IN (SELECT TOP 1 numListItemID
--                                        FROM    dbo.ListDetails WHERE   vcData = 'Purchase Credit Memo' AND constFlag = 1 )
--                            END

                        IF @tintReturnType=1 
                        BEGIN
							SELECT TOP 1 @numRMATempID = numBizDocTempID FROM BizDocTemplate
							WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
                                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
									    FROM   dbo.ListDetails WHERE  vcData = 'RMA' AND constFlag = 1 )							
						END
						ELSE IF @tintReturnType=2
                        BEGIN
							SELECT TOP 1 @numRMATempID = numBizDocTempID FROM BizDocTemplate
							WHERE   numOppType = 2 AND bitDefault = 1 AND bitEnabled = 1
                                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
									    FROM   dbo.ListDetails WHERE  vcData = 'RMA' AND constFlag = 1 )	
						END			    
                        ELSE IF @tintReturnType=3
                        BEGIN
							SELECT TOP 1 @numRMATempID = numBizDocTempID FROM    BizDocTemplate
							WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
                                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
                                FROM   dbo.ListDetails WHERE  vcData = 'Credit Memo' AND constFlag = 1 )							
						END
						ELSE IF @tintReturnType=4
                        BEGIN
							SELECT TOP 1 @numRMATempID = numBizDocTempID FROM    BizDocTemplate
							WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
                                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
                                FROM   dbo.ListDetails WHERE  vcData = 'Refund Receipt' AND constFlag = 1 )							
                                
                            SET @numBizdocTempID=@numRMATempID    
						END
                                               
                        UPDATE  dbo.ReturnHeader
                        SET     numRMATempID = ISNULL(@numRMATempID,0),
                                numBizdocTempID = ISNULL(@numBizdocTempID,0)
                        WHERE   numReturnHeaderID = @numReturnHeaderID
			                
			                --Add/Update Address Details
        SET @numOppID = NULLIF(@numOppID, 0)
		            
        DECLARE @vcStreet VARCHAR(100),
            @vcCity VARCHAR(50),
            @vcPostalCode VARCHAR(15),
            @vcCompanyName VARCHAR(100),
            @vcAddressName VARCHAR(50)      
        DECLARE @numState NUMERIC(9),
            @numCountry NUMERIC(9),
            @numCompanyId NUMERIC(9) 
        DECLARE @bitIsPrimary BIT ;

        SELECT  @vcCompanyName = vcCompanyName,
                @numCompanyId = div.numCompanyID
        FROM    CompanyInfo Com
                JOIN divisionMaster Div ON div.numCompanyID = com.numCompanyID
        WHERE   div.numdivisionID = @numDivisionId

--Bill Address
        IF @numBillAddressId > 0 
            BEGIN
                SELECT  @vcStreet = ISNULL(vcStreet, ''),
                        @vcCity = ISNULL(vcCity, ''),
                        @vcPostalCode = ISNULL(vcPostalCode, ''),
                        @numState = ISNULL(numState, 0),
                        @numCountry = ISNULL(numCountry, 0),
                        @bitIsPrimary = bitIsPrimary,
                        @vcAddressName = vcAddressName
                FROM    dbo.AddressDetails
                WHERE   numDomainID = @numDomainID
                        AND numAddressID = @numBillAddressId
                EXEC dbo.USP_UpdateOppAddress @numOppID = @numOppID, --  numeric(9, 0)
                    @byteMode = 0, --  tinyint
                    @vcStreet = @vcStreet, --  varchar(100)
                    @vcCity = @vcCity, --  varchar(50)
                    @vcPostalCode = @vcPostalCode, --  varchar(15)
                    @numState = @numState, --  numeric(9, 0)
                    @numCountry = @numCountry, --  numeric(9, 0)
                    @vcCompanyName = @vcCompanyName, --  varchar(100)
                    @numCompanyId = 0, --  numeric(9, 0)
                    @vcAddressName = @vcAddressName,
                    @numReturnHeaderID = @numReturnHeaderID
            END
    
  --Ship Address
        IF @numShipAddressId > 0 
            BEGIN
                SELECT  @vcStreet = ISNULL(vcStreet, ''),
                        @vcCity = ISNULL(vcCity, ''),
                        @vcPostalCode = ISNULL(vcPostalCode, ''),
                        @numState = ISNULL(numState, 0),
                        @numCountry = ISNULL(numCountry, 0),
                        @bitIsPrimary = bitIsPrimary,
                        @vcAddressName = vcAddressName
                FROM    dbo.AddressDetails
                WHERE   numDomainID = @numDomainID
                        AND numAddressID = @numShipAddressId
 
                EXEC dbo.USP_UpdateOppAddress @numOppID = @numOppID, --  numeric(9, 0)
                    @byteMode = 1, --  tinyint
                    @vcStreet = @vcStreet, --  varchar(100)
                    @vcCity = @vcCity, --  varchar(50)
                    @vcPostalCode = @vcPostalCode, --  varchar(15)
                    @numState = @numState, --  numeric(9, 0)
                    @numCountry = @numCountry, --  numeric(9, 0)
                    @vcCompanyName = @vcCompanyName, --  varchar(100)
                    @numCompanyId = @numCompanyId, --  numeric(9, 0)
                    @vcAddressName = @vcAddressName,
                    @numReturnHeaderID = @numReturnHeaderID
            END
            
							IF @tintReturnType=1
							BEGIN    
								IF @numOppId>0
								BEGIN
									INSERT dbo.OpportunityMasterTaxItems (
											numReturnHeaderID,
											numTaxItemID,
											fltPercentage
										) SELECT @numReturnHeaderID,numTaxItemID,fltPercentage FROM OpportunityMasterTaxItems WHERE numOppID=@numOppID
								END
								ELSE
								BEGIN 
															--Insert Tax for Division                       
										INSERT dbo.OpportunityMasterTaxItems (
											numReturnHeaderID,
											numTaxItemID,
											fltPercentage
										) SELECT @numReturnHeaderID,TI.numTaxItemID,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,NULL,0,@numReturnHeaderID) FROM TaxItems TI JOIN DivisionTaxTypes DTT ON TI.numTaxItemID = DTT.numTaxItemID
										 WHERE DTT.numDivisionID=@numDivisionId AND DTT.bitApplicable=1
										   union 
										  select @numReturnHeaderID,0,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,NULL,1,@numReturnHeaderID) 
										  FROM dbo.DivisionMaster WHERE bitNoTax=0 AND numDivisionID=@numDivisionID
								END			  
							END
          
          IF CONVERT(VARCHAR(10), @strItems) <> '' 
            BEGIN
                PRINT 1
                EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                                                                                             
                SELECT  *
                INTO    #temp
                FROM    OPENXML (@hDocItem, '/NewDataSet/Item',2) WITH ( numReturnItemID NUMERIC, numItemCode NUMERIC, numUnitHour NUMERIC, numUnitHourReceived NUMERIC, monPrice MONEY, monTotAmount MONEY, vcItemDesc VARCHAR(1000), numWareHouseItemID NUMERIC, vcModelID VARCHAR(200), vcManufacturer VARCHAR(250), numUOMId NUMERIC,numOppItemID NUMERIC )

                EXEC sp_xml_removedocument @hDocItem 
                
                        INSERT  INTO [ReturnItems]
                                (
                                  [numReturnHeaderID],
                                  [numItemCode],
                                  [numUnitHour],
                                  [numUnitHourReceived],
                                  [monPrice],
                                  [monTotAmount],
                                  [vcItemDesc],
                                  [numWareHouseItemID],
                                  [vcModelID],
                                  [vcManufacturer],
                                  [numUOMId],numOppItemID
                                )
                                SELECT  @numReturnHeaderID,
                                        numItemCode,
                                        numUnitHour,
                                        0,
                                        monPrice,
                                        monTotAmount,
                                        vcItemDesc,
                                        NULLIF(numWareHouseItemID, 0) numWareHouseItemID,
                                        vcModelID,
                                        vcManufacturer,
                                        numUOMId,numOppItemID
                                FROM    #temp
                                WHERE   numReturnItemID = 0
					
                     
                DROP TABLE #temp
   
						IF @tintReturnType=1
						BEGIN               
							IF @numOppId>0
								BEGIN
									INSERT INTO dbo.OpportunityItemsTaxItems (
										numReturnHeaderID,numReturnItemID,numTaxItemID
									)  SELECT @numReturnHeaderID,OI.numReturnItemID,IT.numTaxItemID 
									FROM dbo.ReturnItems OI JOIN dbo.OpportunityItemsTaxItems IT ON OI.numOppItemID=IT.numOppItemID 
									WHERE OI.numReturnHeaderID=@numReturnHeaderID AND IT.numOppId=@numOppId
								END
								ELSE
								BEGIN
										--Delete Tax for ReturnItems if item deleted 
						--DELETE FROM OpportunityItemsTaxItems WHERE numReturnHeaderID=@numReturnHeaderID AND numReturnItemID NOT IN (SELECT numReturnItemID FROM ReturnItems WHERE numReturnHeaderID=@numReturnHeaderID)

						--Insert Tax for ReturnItems
						INSERT INTO dbo.OpportunityItemsTaxItems (
							numReturnHeaderID,
							numReturnItemID,
							numTaxItemID
						) SELECT @numReturnHeaderID,OI.numReturnItemID,TI.numTaxItemID FROM dbo.ReturnItems OI JOIN dbo.ItemTax IT ON OI.numItemCode=IT.numItemCode JOIN
						TaxItems TI ON TI.numTaxItemID=IT.numTaxItemID WHERE OI.numReturnHeaderID=@numReturnHeaderID AND IT.bitApplicable=1  
						--AND OI.numReturnItemID NOT IN (SELECT numReturnItemID FROM OpportunityItemsTaxItems WHERE numReturnHeaderID=@numReturnHeaderID)
						UNION
						  select @numReturnHeaderID,OI.numReturnItemID,0 FROM dbo.ReturnItems OI JOIN dbo.Item I ON OI.numItemCode=I.numItemCode
						   WHERE OI.numReturnHeaderID=@numReturnHeaderID  AND I.bitTaxable=1
						--AND OI.numReturnItemID NOT IN (SELECT numReturnItemID FROM OpportunityItemsTaxItems WHERE numReturnHeaderID=@numReturnHeaderID)
							END
						END
            END 
            
            -- Update leads,prospects to Accounts
		EXEC dbo.USP_UpdateCRMTypeToAccounts @numDivisionID ,@numDomainID ,@numUserCntID
                          
            END    
        ELSE IF @numReturnHeaderID <> 0 
                BEGIN    
                   IF ISNULL(@tintMode, 0) = 0
                   BEGIN
				   			UPDATE  [ReturnHeader]
							SET     [vcRMA] = @vcRMA,
									[numReturnReason] = @numReturnReason,
									[numReturnStatus] = @numReturnStatus,
									[monAmount] = ISNULL(monAmount,0) + ISNULL(monTotalDiscount,0) - ISNULL(@monTotalDiscount,0),
									[monTotalDiscount] = @monTotalDiscount,
									[tintReceiveType] = @tintReceiveType,
									[vcComments] = @vcComments,
									[numModifiedBy] = @numUserCntID,
									[dtModifiedDate] = GETUTCDATE()
							WHERE   [numDomainId] = @numDomainId
									AND [numReturnHeaderID] = @numReturnHeaderID
				   END                 
                   ELSE IF ISNULL(@tintMode, 0) = 1 
                   BEGIN
                   	UPDATE  ReturnHeader
								SET     numReturnStatus = @numReturnStatus
								WHERE   numReturnHeaderID = @numReturnHeaderID
							
				   	IF CONVERT(VARCHAR(10), @strItems) <> '' 
					BEGIN
						EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                                                                                             
						SELECT  *
						INTO    #temp1
						FROM    OPENXML (@hDocItem, '/NewDataSet/Item',2) WITH ( numReturnItemID NUMERIC, numUnitHourReceived NUMERIC, numWareHouseItemID NUMERIC)
						EXEC sp_xml_removedocument @hDocItem 

								
								UPDATE  [ReturnItems]
								SET     [numUnitHourReceived] = X.numUnitHourReceived,
										[numWareHouseItemID] =CASE WHEN X.numWareHouseItemID=0 THEN NULL ELSE X.numWareHouseItemID END 
								FROM    #temp1 AS X
								WHERE   X.numReturnItemID = ReturnItems.numReturnItemID
										AND ReturnItems.numReturnHeaderID = @numReturnHeaderID
						DROP TABLE #temp1
				END 
			END
       END            
    END
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageWareHouseItems_Tracking]    Script Date: 07/26/2008 16:14:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageWareHouseItems_Tracking' ) 
    DROP PROCEDURE USP_ManageWareHouseItems_Tracking
GO
CREATE PROCEDURE [dbo].[USP_ManageWareHouseItems_Tracking]
    @numWareHouseItemID AS NUMERIC(9) = 0,
    @numReferenceID AS NUMERIC(9)=0,
    @tintRefType AS TINYINT=0,
    @vcDescription AS VARCHAR(100)='',
    @tintMode AS TINYINT=0,
    @CurrentPage INT=0,
    @PageSize INT=0,
    @TotRecs INT=0  OUTPUT,
    @numModifiedBy NUMERIC(9)=0,
    @ClientTimeZoneOffset INT=0,
    @dtRecordDate AS DATETIME=NULL,
	@numDomainID AS NUMERIC(18,0) = 0
AS 

  DECLARE  @firstRec  AS INTEGER
  DECLARE  @lastRec  AS INTEGER


IF @tintMode=1 --Select based on WareHouseItemID
BEGIN

SELECT X.*,Row_number() over(order by dtCreatedDate1 DESC,numWareHouseItemTrackingID DESC) AS RowNumber INTO #tempTable FROM (SELECT        WHIT.numWareHouseItemID,
              WHIT.numOnHand,
              WHIT.numOnOrder,
              WHIT.numReorder,
              WHIT.numAllocation,
              WHIT.numBackOrder,
              WHIT.numDomainID,
              WHIT.vcDescription,
              WHIT.tintRefType,
              WHIT.numReferenceID,
              '' as vcPOppName,
              0 as tintOppType ,
              dbo.FormatedDateTimeFromDate(DateAdd(minute,-@ClientTimeZoneOffset, WHIT.dtCreatedDate),WHIT.numDomainID) dtCreatedDate
			  ,dtCreatedDate AS dtCreatedDate1,WHIT.numModifiedBy,ISNULL(WHIT.monAverageCost,0) AS monAverageCost,ISNULL(WHIT.numTotalOnHand,0) AS numTotalOnHand,dbo.FormatedDateFromDate(WHIT.dtRecordDate,WHIT.numDomainID) AS dtRecordDate,WHIT.numWareHouseItemTrackingID
              FROM WareHouseItems_Tracking WHIT
              join Item I  on I.numItemCode = WHIT.numReferenceID
              WHERE numWareHouseItemID=@numWareHouseItemID  
			  AND [WHIT].[numDomainID] = @numDomainID
			  AND WHIT.tintRefType=1
UNION  
SELECT    WHIT.numWareHouseItemID,
              WHIT.numOnHand,
              WHIT.numOnOrder,
              WHIT.numReorder,
              WHIT.numAllocation,
              WHIT.numBackOrder,
              WHIT.numDomainID,
              WHIT.vcDescription,
              WHIT.tintRefType,
              ISNULL(WHIT.numReferenceID,0) AS numReferenceID,
              ISNULL(ISNULL(OM.vcPOppName,''),'Order Deleted - ' + CONVERT(VARCHAR(10),ISNULL(WHIT.numReferenceID,0))) as vcPOppName,
              ISNULL(OM.tintOppType,0) as tintOppType,
              dbo.FormatedDateTimeFromDate(DateAdd(minute,-@ClientTimeZoneOffset, WHIT.dtCreatedDate),WHIT.numDomainID) dtCreatedDate
			  ,dtCreatedDate AS dtCreatedDate1,WHIT.numModifiedBy,ISNULL(WHIT.monAverageCost,0) AS monAverageCost,ISNULL(WHIT.numTotalOnHand,0) AS numTotalOnHand,dbo.FormatedDateFromDate(WHIT.dtRecordDate,WHIT.numDomainID) AS dtRecordDate,WHIT.numWareHouseItemTrackingID
              FROM WareHouseItems_Tracking WHIT
              LEFT join OpportunityMaster OM  on WHIT.numReferenceID = OM.numOppId
    WHERE numWareHouseItemID=@numWareHouseItemID AND WHIT.tintRefType IN (3,4)
	AND [WHIT].[numDomainID] = @numDomainID
    UNION
	SELECT    WHIT.numWareHouseItemID,
              WHIT.numOnHand,
              WHIT.numOnOrder,
              WHIT.numReorder,
              WHIT.numAllocation,
              WHIT.numBackOrder, 
              WHIT.numDomainID,
              WHIT.vcDescription,
              WHIT.tintRefType,
              WHIT.numReferenceID,
              '' as vcPOppName,
              0 as tintOppType,
              dbo.FormatedDateTimeFromDate(DateAdd(minute,-@ClientTimeZoneOffset, WHIT.dtCreatedDate),WHIT.numDomainID) dtCreatedDate
			  ,dtCreatedDate AS dtCreatedDate1,WHIT.numModifiedBy,ISNULL(WHIT.monAverageCost,0) AS monAverageCost,ISNULL(WHIT.numTotalOnHand,0) AS numTotalOnHand,dbo.FormatedDateFromDate(WHIT.dtRecordDate,WHIT.numDomainID) AS dtRecordDate,WHIT.numWareHouseItemTrackingID
              FROM WareHouseItems_Tracking WHIT
              JOIN WorkOrder WO  on WHIT.numReferenceID = WO.numWOId
    WHERE  WHIT.numWareHouseItemID=@numWareHouseItemID AND  WHIT.tintRefType=2
	AND [WHIT].[numDomainID] = @numDomainID
    UNION
    SELECT    WHIT.numWareHouseItemID,
              WHIT.numOnHand,
              WHIT.numOnOrder,
              WHIT.numReorder,
              WHIT.numAllocation,
              WHIT.numBackOrder,
              WHIT.numDomainID,
              WHIT.vcDescription,
              WHIT.tintRefType,
              ISNULL(WHIT.numReferenceID,0) AS numReferenceID,
              ISNULL(ISNULL(RH.vcBizDocName,RH.vcRMA),'RMA Deleted - ' + CONVERT(VARCHAR(10),ISNULL(WHIT.numReferenceID,0))) as vcPOppName,
              5 as tintOppType,
              dbo.FormatedDateTimeFromDate(DateAdd(minute,-@ClientTimeZoneOffset, WHIT.dtCreatedDate),WHIT.numDomainID) dtCreatedDate
			  ,WHIT.dtCreatedDate AS dtCreatedDate1,WHIT.numModifiedBy,ISNULL(WHIT.monAverageCost,0) AS monAverageCost,ISNULL(WHIT.numTotalOnHand,0) AS numTotalOnHand,dbo.FormatedDateFromDate(WHIT.dtRecordDate,WHIT.numDomainID) AS dtRecordDate,WHIT.numWareHouseItemTrackingID
              FROM WareHouseItems_Tracking WHIT
              LEFT join ReturnHeader RH  on WHIT.numReferenceID = RH.numReturnHeaderID
    WHERE numWareHouseItemID=@numWareHouseItemID AND WHIT.tintRefType=5
	AND [WHIT].[numDomainID] = @numDomainID) X

  SET @firstRec = (@CurrentPage
                     - 1)
                    * @PageSize
  SET @lastRec = (@CurrentPage
                    * @PageSize
                    + 1)
  SET @TotRecs = (SELECT COUNT(* )
                  FROM   #tempTable)
                  
  SELECT *,dbo.fn_GetContactName(ISNULL(numModifiedBy,0)) AS vcUserName FROM #tempTable WHERE RowNumber > @firstRec and RowNumber < @lastRec order by RowNumber
  DROP TABLE #tempTable
END
ELSE IF @tintMode=2 --Only Biz Calculated Avg Cost
BEGIN

SELECT X.*,Row_number() over(order by dtCreatedDate1 DESC,numWareHouseItemTrackingID DESC) AS RowNumber INTO #tempTable1 FROM (
SELECT    WHIT.numWareHouseItemID,
              WHIT.numOnHand,
              WHIT.numOnOrder,
              WHIT.numReorder,
              WHIT.numAllocation,
              WHIT.numBackOrder,
              WHIT.numDomainID,
              WHIT.vcDescription,
              WHIT.tintRefType,
              ISNULL(WHIT.numReferenceID,0) AS numReferenceID,
              ISNULL(ISNULL(OM.vcPOppName,''),'Order Deleted - ' + CONVERT(VARCHAR(10),ISNULL(WHIT.numReferenceID,0))) as vcPOppName,
              ISNULL(OM.tintOppType,0) as tintOppType,
              dbo.FormatedDateTimeFromDate(DateAdd(minute,-@ClientTimeZoneOffset, WHIT.dtCreatedDate),WHIT.numDomainID) dtCreatedDate
			  ,dtCreatedDate AS dtCreatedDate1,WHIT.numModifiedBy,ISNULL(WHIT.monAverageCost,0) AS monAverageCost,ISNULL(WHIT.numTotalOnHand,0) AS numTotalOnHand,dbo.FormatedDateFromDate(WHIT.dtRecordDate,WHIT.numDomainID) AS dtRecordDate,WHIT.numWareHouseItemTrackingID
              FROM WareHouseItems_Tracking WHIT
              LEFT join OpportunityMaster OM  on WHIT.numReferenceID = OM.numOppId
    WHERE numWareHouseItemID=@numWareHouseItemID AND WHIT.tintRefType =6
	AND [WHIT].[numDomainID] = @numDomainID
    ) X

  SET @firstRec = (@CurrentPage
                     - 1)
                    * @PageSize
  SET @lastRec = (@CurrentPage
                    * @PageSize
                    + 1)
  SET @TotRecs = (SELECT COUNT(* )
                  FROM   #tempTable1)
                  
  SELECT *,dbo.fn_GetContactName(ISNULL(numModifiedBy,0)) AS vcUserName FROM #tempTable1 WHERE RowNumber > @firstRec and RowNumber < @lastRec order by RowNumber
  
  DROP TABLE #tempTable1
END
ELSE IF @tintMode=0 --Insert new record
BEGIN

    INSERT  INTO WareHouseItems_Tracking
            (
              numWareHouseItemID,
              numOnHand,
              numOnOrder,
              numReorder,
              numAllocation,
              numBackOrder,
              numDomainID,
              vcDescription,
              tintRefType,
              numReferenceID,
              dtCreatedDate,numModifiedBy,monAverageCost,numTotalOnHand,dtRecordDate
            )
            SELECT  WHI.numWareHouseItemID,
                    ISNULL(WHI.numOnHand,0),
                    ISNULL(WHI.numOnOrder,0),
                    ISNULL(WHI.numReorder,0),
                    ISNULL(WHI.numAllocation,0),
                    ISNULL(WHI.numBackOrder,0),
                    WHI.numDomainID,
                    @vcDescription,
                    @tintRefType,
                    @numReferenceID,
                    GETUTCDATE(),@numModifiedBy,ISNULL(I.monAverageCost,0),
                    (SELECT SUM(ISNULL(numOnHand, 0)) FROM dbo.WareHouseItems WHERE numItemID=I.numItemCode),@dtRecordDate
            FROM    Item I JOIN WareHouseItems WHI ON I.numItemCode=WHI.numItemID
			WHERE WHI.numWareHouseItemID = @numWareHouseItemID
			AND [WHI].[numDomainID] = @numDomainID
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateNameTemplateValue')
DROP PROCEDURE USP_UpdateNameTemplateValue
GO
CREATE PROCEDURE USP_UpdateNameTemplateValue
@tintType TINYINT,
@numDomainID NUMERIC,
@RecordID numeric
AS 
BEGIN
	DECLARE @Tempate VARCHAR(200)
	DECLARE @OrgName AS VARCHAR(100)
	
	DECLARE @numSequenceId NUMERIC(9),@numMinLength NUMERIC(9)
	SET @numSequenceId=0

	  IF NOT EXISTS ( SELECT  * FROM dbo.NameTemplate WHERE   numDomainID = @numDomainID AND tintModuleID=1) 
                BEGIN
                    INSERT  INTO dbo.NameTemplate (numDomainID,vcModuleName,vcNameTemplate,
                                                    tintModuleID,numSequenceId,numMinLength,numRecordID)
                            SELECT  @numDomainID,vcModuleName,vcNameTemplate,
                                    tintModuleID,numSequenceId,numMinLength,numRecordID
                            FROM    dbo.NameTemplate
                            WHERE   numDomainID = 0	AND tintModuleID=1
                END
		
	--Get Custom Template If enabled
	SELECT @Tempate = ISNULL(vcNameTemplate,''),@numSequenceId=ISNULL(numSequenceId,1),@numMinLength=ISNULL(numMinLength,4) FROM dbo.NameTemplate WHERE numDomainID=@numDomainID 
	AND tintModuleID=1 AND numRecordID=@tintType
	
	--Replace Other Values
	SET @Tempate = REPLACE(@Tempate,'$YEAR$',DATENAME(year ,GETUTCDATE()) )
	SET @Tempate = REPLACE(@Tempate,'$MONTH$',DATENAME(month ,GETUTCDATE()) )
	SET @Tempate = REPLACE(@Tempate,'$DAY$',DATENAME(day ,GETUTCDATE()) )
	SET @Tempate = REPLACE(@Tempate,'$QUARTER$',DATENAME(quarter ,GETUTCDATE()) )
		
--		SELECT DATENAME(quarter,GETUTCDATE())
	
		--Replace Organization Name
		IF CHARINDEX(lower(@Tempate),'$organizationname$')>=0
		BEGIN
			SELECT @OrgName= ISNULL(C.vcCompanyName,'') FROM dbo.OpportunityMaster OM INNER JOIN dbo.DivisionMaster DM ON OM.numDivisionId=DM.numDivisionID
			INNER JOIN dbo.CompanyInfo C ON C.numCompanyId = DM.numCompanyID
			WHERE numOppId=@RecordID;
			
			SET @Tempate = REPLACE(@Tempate,'$OrganizationName$',@OrgName)
		END
	
		SET @Tempate = REPLACE(@Tempate,'$OrderID$',@RecordID )
		
	    SET @Tempate = @Tempate + ISNULL(REPLICATE('0', @numMinLength - LEN(@numSequenceId)),'') + CAST(@numSequenceId AS VARCHAR(18))
	
		UPDATE dbo.OpportunityMaster SET vcPOppName = ISNULL(@Tempate,ISNULL(vcPOppName,'')) WHERE numOppId=@RecordID		
		
		UPDATE NameTemplate SET numSequenceId=@numSequenceId + 1  WHERE numDomainID=@numDomainID 
			AND tintModuleID=1 AND numRecordID=@tintType
END
/*
EXEC USP_UpdateNameTemplateValue
	@tintType = 1, -- TINYINT
	@numDomainID = 1, -- NUMERIC
	@RecordID = 12494 -- numeric
SELECT * FROM dbo.OpportunityMaster WHERE numDomainId=1	AND numOppId=12494

EXEC USP_UpdateNameTemplateValue
	@tintType = 3, -- TINYINT
	@numDomainID = 1, -- NUMERIC
	@RecordID = 12138 -- numeric
SELECT * FROM dbo.OpportunityBizDocs WHERE numOppId=12494
*/
GO 

-- CREATED BY MANISH ANJARA : 28th OCT, 2014
/*
EXEC USP_GetGenralJournalDetailsReport @numDomainId = 169, 
    @dtFromDate = '2014-01-01 00:00:00', @dtToDate = '2014-01-01 23:59:59',
    @CurrentPage = 1,@PageSize = 100

*/

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetGenralJournalDetailsReport' )
    DROP PROCEDURE USP_GetGenralJournalDetailsReport
GO
CREATE PROCEDURE [dbo].[USP_GetGenralJournalDetailsReport]
    (
      @numDomainID NUMERIC(18, 0) ,
      @dtFromDate DATETIME ,
      @dtToDate DATETIME ,
      @CurrentPage INT = 0 ,
      @PageSize INT = 0 ,
      @tintMode AS TINYINT = 0
    )
AS
    BEGIN
	
        --SELECT TOP 10
        --        *
        --FROM    [dbo].[General_Journal_Details] AS GJD
        --ORDER BY [GJD].[numJournalId] DESC

        DECLARE @first_id NUMERIC
        DECLARE @startRow INT

        DECLARE @CurrRecord AS INT
        IF @tintMode = 0
            BEGIN

                SELECT TOP 1
                        [GJH].[numJournal_Id] ,
                        [GJD].[numTransactionId] ,
                        dbo.[FormatedDateFromDate]([GJH].[datEntry_Date],
                                                   [GJD].numDomainID) [Date] ,
                        COA.[numAccountId] ,
                        ISNULL([COA].vcAccountName, '') AS [vcAccountName] ,
                        ISNULL([GJD].[numDebitAmt], 0) [numDebitAmt] ,
                        ISNULL([GJD].[numCreditAmt], 0) [numCreditAmt]
                FROM    [dbo].[General_Journal_Header] AS GJH
                        JOIN [dbo].[General_Journal_Details] AS GJD ON [GJD].[numJournalId] = [GJH].[numJournal_Id]
                                                              AND [GJD].[numDomainId] = [GJH].[numDomainId]
                        JOIN [dbo].[Chart_Of_Accounts] AS COA ON [GJD].[numChartAcntId] = [COA].[numAccountId]
                                                              AND [GJH].[numDomainId] = [COA].[numDomainId]
                WHERE   [GJH].[numDomainId] = @numDomainID
                        AND [GJH].[datEntry_Date] BETWEEN @dtFromDate
                                                  AND     @dtToDate
                --AND [GJH].[numJournal_Id] = 181544
                ORDER BY [GJD].numEntryDateSortOrder1 ASC
            END
        ELSE
            IF @tintMode = 1
                BEGIN
                    SET @CurrRecord = ( ( @CurrentPage - 1 ) * @PageSize ) + 1
                    PRINT @CurrRecord
                    SET ROWCOUNT @CurrRecord

                    SELECT  @first_id = [GJD].numEntryDateSortOrder1
                    FROM    [dbo].[General_Journal_Header] AS GJH
                            JOIN [dbo].[General_Journal_Details] AS GJD ON [GJD].[numJournalId] = [GJH].[numJournal_Id]
                                                              AND [GJD].[numDomainId] = [GJH].[numDomainId]
                            JOIN [dbo].[Chart_Of_Accounts] AS COA ON [GJD].[numChartAcntId] = [COA].[numAccountId]
                                                              AND [GJH].[numDomainId] = [COA].[numDomainId]
                    WHERE   [GJH].[numDomainId] = @numDomainID
                            AND [GJH].[datEntry_Date] BETWEEN @dtFromDate
                                                      AND     @dtToDate
                    ORDER BY [GJD].numEntryDateSortOrder1 ASC

                    PRINT @first_id
                    PRINT ROWCOUNT_BIG()
                    SET ROWCOUNT @PageSize

                    SELECT  [GJH].[numJournal_Id] ,
                            [GJD].[numTransactionId] ,
                            dbo.[FormatedDateFromDate]([GJH].[datEntry_Date],
                                                       [GJD].numDomainID) [Date] ,
                            COA.[numAccountId] ,
                            ISNULL([COA].vcAccountName, '') AS [vcAccountName] ,
                            ISNULL([GJD].[numDebitAmt], 0) [numDebitAmt] ,
                            ISNULL([GJD].[numCreditAmt], 0) [numCreditAmt]
                    FROM    [dbo].[General_Journal_Header] AS GJH
                            JOIN [dbo].[General_Journal_Details] AS GJD ON [GJD].[numJournalId] = [GJH].[numJournal_Id]
                                                              AND [GJD].[numDomainId] = [GJH].[numDomainId]
                            JOIN [dbo].[Chart_Of_Accounts] AS COA ON [GJD].[numChartAcntId] = [COA].[numAccountId]
                                                              AND [GJH].[numDomainId] = [COA].[numDomainId]
                    WHERE   [GJH].[numDomainId] = @numDomainID
                            AND [GJH].[datEntry_Date] BETWEEN @dtFromDate
                                                      AND     @dtToDate
            --AND [GJH].[numJournal_Id] = 181544
                            AND [GJD].numEntryDateSortOrder1 >= @first_id
                    ORDER BY [GJD].numEntryDateSortOrder1 ASC
                
                    SET ROWCOUNT 0

                END
            ELSE
                IF @tintMode = 3
                    BEGIN
                        SELECT  dbo.[FormatedDateFromDate]([GJH].[datEntry_Date],
                                                           [GJD].numDomainID) [Date] ,
                                COA.[numAccountId] AS [AccountID],
                                CASE WHEN ISNULL([GJD].[numDebitAmt], 0) = 0 THEN ISNULL([COA].vcAccountName, '') ELSE '' END AS [Reference] ,
								CASE WHEN ISNULL([GJD].[numCreditAmt], 0) = 0 THEN ISNULL([COA].vcAccountName, '')  ELSE '' END AS [TransDesc] ,
                                ISNULL([GJD].[numDebitAmt], 0) [Debit] ,
                                ISNULL([GJD].[numCreditAmt], 0) [Credit]
                        FROM    [dbo].[General_Journal_Header] AS GJH
                                JOIN [dbo].[General_Journal_Details] AS GJD ON [GJD].[numJournalId] = [GJH].[numJournal_Id]
                                                              AND [GJD].[numDomainId] = [GJH].[numDomainId]
                                JOIN [dbo].[Chart_Of_Accounts] AS COA ON [GJD].[numChartAcntId] = [COA].[numAccountId]
                                                              AND [GJH].[numDomainId] = [COA].[numDomainId]
                        WHERE   [GJH].[numDomainId] = @numDomainID
                                AND [GJH].[datEntry_Date] BETWEEN @dtFromDate
                                                          AND @dtToDate
                        ORDER BY [GJD].numEntryDateSortOrder1 ASC
                    END
    END

GO