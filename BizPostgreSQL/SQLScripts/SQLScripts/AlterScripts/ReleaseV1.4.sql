/******************************************************************
Project: Release 1.4 Date: 16.03.2013
Comments: 
*******************************************************************/
/*KV matz trial balance issue*/
SELECT * FROM dbo.General_Journal_Details WHERE numChartAcntId IS NULL AND numDomainId=153
update General_Journal_Details set numChartAcntID = 4529 where numTransactionID in (392200,392153,392152,392131)

/*For GIS already updated on production */
UPDATE dbo.OpportunityMaster SET tintSourceType = 2 WHERE tintSource=21 AND tintSourceType<>2 AND numDomainId=135


/*Upadate connection string in UI portal and BizCart*/
"Pooling=true; Max pool size=200; Min pool size=0;"

DECLARE @v sql_variant 
SET @v = N'0 = Lead ,1 = Prospect , 2 = Account'
EXECUTE sp_updateextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'DivisionMaster', N'COLUMN', N'tintCRMType'


SELECT * FROM dbo.ListMaster WHERE vcListName LIKE '%billing term%'

UPDATE dbo.ListMaster SET bitDeleted = 1 WHERE numListID=296

/******************************************************************
Project: BACRMUI   Date: 05.Mar.2013
Comments: Add column in ShippingBox
*******************************************************************/
BEGIN TRANSACTION

ALTER TABLE dbo.ShippingBox ADD numShipCompany INT

---ALTER TABLE OpportunityMaster ADD monTermDiscount NUMERIC(10,0)
ROLLBACK


/******************************************************************
Project: BACRMUI   Date: 02.Mar.2013
Comments: Add column in Billing Terms/OpportunityMaster
*******************************************************************/
BEGIN TRANSACTION

ALTER TABLE dbo.BillingTerms ADD bitActive BIT
UPDATE BillingTerms SET bitActive = 1

---ALTER TABLE OpportunityMaster ADD monTermDiscount NUMERIC(10,0)
ROLLBACK


/******************************************************************
Project: BACRMUI   Date: 20.Feb.2013
Comments: To Migrate Billing Terms with domain
*******************************************************************/
BEGIN TRANSACTION

DELETE FROM BillingTerms
SET IDENTITY_INSERT dbo.BillingTerms ON
INSERT INTO dbo.BillingTerms 
(numTermsID,vcTerms,tintApplyTo,numNetDueInDays,numDiscount,numDiscountPaidInDays,numListItemID,numDomainID)
SELECT numListItemID,'Net ' + vcData,1,vcData,0,0,numListItemID,ListDetails.numDomainID FROM dbo.ListDetails 
--CROSS JOIN dbo.Domain
WHERE numListID = (SELECT numListID FROM dbo.ListMaster WHERE vcListName = 'Billing term net days')
--AND ListDetails.numDomainID =1
ORDER BY ListDetails.numDomainID
SELECT * FROM BillingTerms

SET IDENTITY_INSERT dbo.BillingTerms OFF	
ROLLBACK


/******************************************************************
Project: BACRMUI   Date: 20.Feb.2013
Comments: Financial Term Changes
*******************************************************************/
BEGIN TRANSACTION

GO
/****** Object:  Table [dbo].[BillingTerms]    Script Date: 02/20/2013 15:50:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BillingTerms](
	[numTermsID] [bigint] IDENTITY(1,1) NOT NULL,
	[vcTerms] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[tintApplyTo] [tinyint] NOT NULL,
	[numNetDueInDays] [int] NOT NULL,
	[numDiscount] [numeric](10, 0) NULL,
	[numDiscountPaidInDays] [int] NULL,
	[numListItemID] [numeric](18, 0) NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_BillingTerms] PRIMARY KEY CLUSTERED 
(
	[numTermsID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 - Sales Order, 2 - Purchase Order' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'BillingTerms', @level2type=N'COLUMN', @level2name=N'tintApplyTo'

ROLLBACK
-----------------------------------------------------kishan
BEGIN TRANSACTION
DELETE FROM dbo.PageMaster WHERE numModuleID = 13 AND numPageID = 52
GO
ROLLBACK TRANSACTION
 
BEGIN TRANSACTION
DELETE FROM dbo.GroupAuthorization  WHERE numModuleID = 13 AND numPageID = 52
GO
ROLLBACK TRANSACTION
/******************************************************************
Project:    Date: 08-03-2013
Note : 
*******************************************************************/

--13-3-2013 
BEGIN TRANSACTION

INSERT INTO dbo.ErrorMaster (
	vcErrorCode,
	vcErrorDesc,
	tintApplication
) VALUES ( 
	/* vcErrorCode - varchar(6) */ 'ERR072',
	/* vcErrorDesc - nvarchar(max) */ N'Service item for Coupen is not selected,Please contact your merchant.',
	/* tintApplication - tinyint */ 3 )  

GO
ROLLBACK TRANSACTION


BEGIN TRANSACTION

ALTER TABLE dbo.ItemImages 
ADD bitIsImage BIT NULL DEFAULT 0

GO
ROLLBACK TRANSACTION

/******************************************************************
Project:    Date: 22-2-2013
Note : 
*******************************************************************/

--07-03-2013 add page for Authentication frmSiteCategory
BEGIN TRANSACTION
DECLARE @PageID AS NUMERIC(9,0)
SELECT @PageID = (MAX(numPageID)+1) FROM dbo.PageMaster WHERE  numModuleID = 13

INSERT INTO dbo.PageMaster (
	numPageID,
	numModuleID,
	vcFileName,
	vcPageDesc,
	bitIsViewApplicable,
	bitIsAddApplicable,
	bitIsUpdateApplicable,
	bitIsDeleteApplicable,
	bitIsExportApplicable
) VALUES ( 
	/* numPageID - numeric(18, 0) */ @PageID,
	/* numModuleID - numeric(18, 0) */ 13,
	/* vcFileName - varchar(50) */ 'frmSiteCategory.aspx',
	/* vcPageDesc - varchar(100) */ 'Site Category ',
	/* bitIsViewApplicable - int */ 1,
	/* bitIsAddApplicable - int */ 1,
	/* bitIsUpdateApplicable - int */ 0,
	/* bitIsDeleteApplicable - int */ 0,
	/* bitIsExportApplicable - int */ 0
	 ) 
GO
ROLLBACK TRANSACTION


--BEGIN TRANSACTION
--DECLARE @PageID AS NUMERIC(9,0)
--SELECT @PageID = (MAX(numPageID)+1) FROM dbo.PageMaster WHERE  numModuleID = 13
--
--INSERT INTO dbo.PageMaster (
--	numPageID,
--	numModuleID,
--	vcFileName,
--	vcPageDesc,
--	bitIsViewApplicable,
--	bitIsAddApplicable,
--	bitIsUpdateApplicable,
--	bitIsDeleteApplicable,
--	bitIsExportApplicable
--) VALUES ( 
--	/* numPageID - numeric(18, 0) */ @PageID,
--	/* numModuleID - numeric(18, 0) */ 13,
--	/* vcFileName - varchar(50) */ 'frmCategoryAdd.aspx',
--	/* vcPageDesc - varchar(100) */ 'Category Add',
--	/* bitIsViewApplicable - int */ 1,
--	/* bitIsAddApplicable - int */ 1,
--	/* bitIsUpdateApplicable - int */ 1,
--	/* bitIsDeleteApplicable - int */ 1,
--	/* bitIsExportApplicable - int */ 0
--	 ) 
--GO
--ROLLBACK TRANSACTION

--07-03-2013
BEGIN TRANSACTION

UPDATE dbo.PageNavigationDTL SET bitVisible = 0 WHERE numPageNavID = 165

GO
ROLLBACK TRANSACTION

--5-3-2013
--Note there should be a script for moving data of vcMetaTags to vcKeyword and vcDescription
BEGIN TRANSACTION

ALTER TABLE MetaTags 
ADD vcMetaKeywords VARCHAR(4000) NULL

ALTER TABLE MetaTags 
ADD vcMetaDescription VARCHAR(4000) NULL

GO
ROLLBACK TRANSACTION

--4-3-2013 
--note check for max of numPageNavID in local it is 168 and parentID is 157 
BEGIN TRANSACTION

DECLARE @numPageNavID NUMERIC(9,0)
SET @numPageNavID = 0
SELECT @numPageNavID = (MAX(numPageNavID)+1) FROM dbo.PageNavigationDTL WHERE numParentID = 157 
--PRINT(@numPageNavID)
INSERT INTO dbo.PageNavigationDTL (
	numPageNavID,
	numModuleID,
	numParentID,
	vcPageNavName,
	vcNavURL,
	vcImageURL,
	bitVisible,
	numTabID
) VALUES ( 
	/* numPageNavID - numeric(18, 0) */ @numPageNavID,
	/* numModuleID - numeric(18, 0) */ 13,
	/* numParentID - numeric(18, 0) */ 157,
	/* vcPageNavName - varchar(300) */ 'Site Category',
	/* vcNavURL - varchar(1000) */ '../ECommerce/frmSiteCategory.aspx',
	/* vcImageURL - varchar(100) */ '',
	/* bitVisible - bit */ 1,
	/* numTabID - numeric(18, 0) */ -1 ) 
		
GO
ROLLBACK TRANSACTION
------------------------------------------------------------------kamal
/************************21_Feb_2013*******************************************************************/
/************************************************************************************************/


DROP TABLE OpportunityFulfillmentBizDocs
DROP TABLE OpportunityFulfillmentRules


CREATE TABLE [dbo].[OpportunityAutomationRules](
	[numRuleID] [numeric](18, 0) NOT NULL,
	[numBizDocStatus1] [numeric](18, 0) NULL,
	[numBizDocStatus2] [numeric](18, 0) NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[tintOppType] [tinyint] NOT NULL,
	[numOrderStatus] [numeric](18, 0) NULL
) ON [PRIMARY]



CREATE TABLE [dbo].[OpportunityAutomationQueue](
	[numOppQueueID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[numOppId] [numeric](18, 0) NULL,
	[numOppBizDocsId] [numeric](18, 0) NULL,
	[numBizDocStatus] [numeric](18, 0) NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[dtCreatedDate] [datetime] NULL,
	[tintProcessStatus] [tinyint] NULL,
	[numRuleID] [numeric](18, 0) NULL,
	[numOrderStatus] [numeric](18, 0) NULL,
	[numShippingReportID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_OpportunityAutomationQueue] PRIMARY KEY CLUSTERED 
(
	[numOppQueueID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1:Pending Execution 2:In Progress 3:Success 4:Failed 5:Change Status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OpportunityAutomationQueue', @level2type=N'COLUMN',@level2name=N'tintProcessStatus'



CREATE TABLE [dbo].[OpportunityAutomationQueueExecution](
	[numOppQueueExeID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numOppQueueID] [numeric](18, 0) NULL,
	[bitSuccess] [bit] NULL,
	[vcDescription] [varchar](1000) NULL,
	[dtExecutionDate] [datetime] NULL,
	[numRuleID] [numeric](18, 0) NULL,
	[numOppId] [numeric](18, 0) NULL,
	[numOppBizDocsId] [numeric](18, 0) NULL,
	[numOrderStatus] [numeric](18, 0) NULL,
	[numBizDocStatus] [numeric](18, 0) NULL,
 CONSTRAINT [PK_OpportunityAutomationQueueExecution] PRIMARY KEY CLUSTERED 
(
	[numOppQueueExeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

-----Add to app.congif of Service------------------------

    <add key="OpportunityAutomation" value="1"/>
    
---------------------------------------------------------
    
    ALTER TABLE dbo.Domain ADD
	numDiscountServiceItemID NUMERIC(18,0) NULL	

    
--------Return hide discount column-------------------------------------------------
   
--SELECT * FROM dbo.DynamicFormMaster WHERE numFormId IN (60,61,62,63)
--SELECT * FROM dbo.DycFormField_Mapping WHERE numFormId IN (60,61,62,63) AND vcPropertyName='TotalDiscount'

UPDATE DycFormField_Mapping SET bitSettingField=0,bitAddField=0,bitDetailField=0 WHERE numFormId IN (60,61,62,63) AND vcPropertyName='TotalDiscount'

--------Item Price Level-------------------------------------------------

--ALTER TABLE dbo.Domain ADD
--	bitItemPriceLevel bit NULL

ALTER TABLE dbo.PricingTable ADD
	numItemCode numeric(18, 0) NULL
	
	
ALTER TABLE dbo.PricingTable
	DROP CONSTRAINT FK_PricingTable_PriceBookRules	
	
	
--ALTER TABLE [dbo].[PricingTable]  WITH CHECK ADD  CONSTRAINT [FK_PricingTable_PriceBookRules] FOREIGN KEY([numPriceRuleID])
--REFERENCES [dbo].[PriceBookRules] ([numPricRuleID])
--GO
--ALTER TABLE [dbo].[PricingTable] CHECK CONSTRAINT [FK_PricingTable_PriceBookRules]	


---------Project Commission------------------------------------------------

--SELECT tintComAppliesTo,* FROM domain WHERE ISNULL(tintComAppliesTo,0)=0
UPDATE domain SET tintComAppliesTo=3 WHERE ISNULL(tintComAppliesTo,0)=0

--SELECT tintCommissionType,* FROM domain WHERE ISNULL(tintCommissionType,0)=0
UPDATE domain SET tintCommissionType=1 WHERE ISNULL(tintCommissionType,0)=0


ALTER TABLE dbo.BizDocComission ADD
	numProId numeric(18, 0) NULL
	

ALTER TABLE dbo.ProjectsMaster ADD
	dtCompletionDate datetime NULL,
	monTotalExpense money NULL,
	monTotalIncome money NULL,
	monTotalGrossProfit money NULL	
	

ALTER TABLE dbo.BizDocComission ADD
	dtProCompletionDate datetime NULL,
	monProTotalExpense money NULL,
	monProTotalIncome money NULL,
	monProTotalGrossProfit money NULL	
-----------------------------------------------------Joseph


/******************************************************************
Project: Ship To Phone Number Mapping			  Date: 12/March/2013
Comments: Ship To Phone Number Mapping if Missing in Api Orders
*******************************************************************/

BEGIN TRANSACTION
GO
ALTER TABLE ImportApiOrder ADD
numOppId NUMERIC NULL
GO
ROLLBACK TRANSACTION



BEGIN TRANSACTION
GO
ALTER TABLE Domain ADD
vcShipToPhoneNo VARCHAR(50) NULL
GO
ROLLBACK TRANSACTION



/******************************************************************
Project: Update vcMarketplaceOrderId with vcOppRefOrderNo Date: 08/03/2013
Comments:  
*******************************************************************/
BEGIN TRANSACTION
GO

ALTER TABLE WebApiOrderDetails DROP CONSTRAINT IX_WebApiOrderDetails
GO
ROLLBACK TRANSACTION


BEGIN TRANSACTION
GO

ALTER TABLE OpportunityMaster
ALTER COLUMN vcMarketplaceOrderId VARCHAR(100)

update OpportunityMaster set vcMarketplaceOrderId = vcOppRefOrderNo
where vcOppRefOrderNo IS NOT NULL

ALTER TABLE WebApiOrderReports DROP CONSTRAINT IX_WebApiOrderReports

GO
ROLLBACK TRANSACTION

/******************************************************************
Project: Adding Marketplace Order Report Id field: 07/03/2013
Comments:  
*******************************************************************/
BEGIN TRANSACTION
GO

ALTER TABLE dbo.OpportunityMaster ADD
	vcMarketplaceOrderReportId VARCHAR(100) NULL
GO
ROLLBACK TRANSACTION

/******************************************************************
Project: ReImport API Orders : 06/03/2013
Comments:  
*******************************************************************/

/****** Object:  Table [dbo].[ImportApiOrder]    Script Date: 06/03/2013 10:09:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ImportApiOrder](
	[numImportApiOrderReqId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainId] [numeric](18, 0) NOT NULL,
	[numWebApiId] [numeric](18, 0) NOT NULL,
	[vcApiOrderId] [varchar](50) NOT NULL,
	[dtCreatedDate] [datetime] NULL CONSTRAINT [DF_ImportApiOrder_dtCreatedDate]  DEFAULT (getdate()),
	[dtModifiedDate] [datetime] NULL,
	[bitIsActive] [bit] NOT NULL CONSTRAINT [DF_ImportApiOrder_bitIsActive]  DEFAULT ((1)),
	[tintRequestType] [tinyint] NULL,
	[dtFromDate] [datetime] NULL,
	[dtToDate] [datetime] NULL,
 CONSTRAINT [PK_ImportApiOrder] PRIMARY KEY CLUSTERED 
(
	[numImportApiOrderReqId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON

/******************************************************************
Project: Adding FBA specific BizDoc and BizDoc Status : 06/03/2013
Comments:  
*******************************************************************/

ALTER TABLE dbo.WebApiDetail ADD
numFBABizDocId NUMERIC(18,0) NULL


ALTER TABLE dbo.WebApiDetail ADD
numFBABizDocStatusId NUMERIC(18,0) NULL

/******************************************************************
Project: Adding Marketplace Order Id field: 26/02/2013
Comments:  
*******************************************************************/
BEGIN TRANSACTION
GO

ALTER TABLE dbo.OpportunityMaster ADD
	vcMarketplaceOrderID VARCHAR(50) NULL
GO
ROLLBACK TRANSACTION

BEGIN TRANSACTION
GO

INSERT INTO [dbo].[DycFieldMaster]([numModuleID], [numDomainID], [vcFieldName], [vcDbColumnName], [vcOrigDbColumnName], [vcPropertyName], [vcLookBackTableName], [vcFieldDataType], [vcFieldType], [vcAssociatedControlType], 
[vcToolTip], [vcListItemType], [numListID], [PopupFunctionName], [order], [tintRow], [tintColumn], 
[bitInResults], [bitDeleted], [bitAllowEdit], [bitDefault], [bitSettingField], [bitAddField], 
[bitDetailField], [bitAllowSorting],bitWorkFlowField,bitAllowFiltering )
SELECT 3, NULL, N'Marketplace Order ID', N'vcMarketplaceOrderID', N'vcMarketplaceOrderID', 'MarketplaceOrderID', N'OpportunityMaster', N'V', N'R',
 N'Label', NULL, N'', 0, N'', 1, 1, 2, 1, 0, 1, 0,1, 0, 1, 1,1,0
		
insert into DycFormField_Mapping (numModuleID,numFieldID,numFormID,vcFieldName,vcAssociatedControlType,PopupFunctionName,bitSettingField,bitAddField,bitDetailField,bitAllowEdit,bitInlineEdit,vcPropertyName,[order],tintRow,tintColumn)
select numModuleID,numFieldID,39,'Marketplace Order ID',vcAssociatedControlType,PopupFunctionName,bitSettingField,0,bitDetailField,0,0,vcPropertyName,[order],tintRow,tintColumn
from DycFieldMaster where [numModuleID]=3 and  [vcDbColumnName]='vcMarketplaceOrderID' AND vcLookBackTableName='OpportunityMaster'
	
GO
ROLLBACK TRANSACTION
