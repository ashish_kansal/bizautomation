/******************************************************************
Project: Release 2.0 Date: 24.09.2013
Comments: 
*******************************************************************/

/*******************Kamal Script******************/
--/************************************************************************************************/
--/************************23_Aug_2013*******************************************************************/
--/************************************************************************************************/

ALTER TABLE dbo.DivisionMaster ADD
	numDefaultPaymentMethod numeric(18, 0) NULL,
	numDefaultCreditCard numeric(18, 0) NULL,
	bitOnCreditHold bit NULL
	
	
--SELECT * FROM dbo.ReturnHeader WHERE numDomainId=172 AND tintReturnType=1 and numBizDocTempID=978
--UPDATE ReturnHeader SET numBizDocTempID=974 WHERE numDomainId=172 AND tintReturnType=1 and numBizDocTempID=978
	
	

--SELECT * FROM ReportFieldGroupMappingMaster where vcFieldName like '%Date%' AND numReportFieldGroupID=5

UPDATE ReportFieldGroupMappingMaster SET vcFieldName='Order Created Date' WHERE vcFieldName like '%Created Date%' AND numReportFieldGroupID=3
UPDATE ReportFieldGroupMappingMaster SET vcFieldName='Order Due Date' WHERE vcFieldName like '%Due Date%' AND numReportFieldGroupID=3
UPDATE ReportFieldGroupMappingMaster SET vcFieldName='Order Estimated Created Date' WHERE vcFieldName like '%Closing Date%' AND numReportFieldGroupID=3
	
UPDATE ReportFieldGroupMappingMaster SET vcFieldName='BizDoc Billing Date' WHERE vcFieldName like '%Billing Date%' AND numReportFieldGroupID=5
UPDATE ReportFieldGroupMappingMaster SET vcFieldName='BizDoc Created Date' WHERE vcFieldName like '%Created Date%' AND numReportFieldGroupID=5
UPDATE ReportFieldGroupMappingMaster SET vcFieldName='BizDoc Due Date' WHERE vcFieldName like '%Due Date%' AND numReportFieldGroupID=5
UPDATE ReportFieldGroupMappingMaster SET vcFieldName='BizDoc Modified Date' WHERE vcFieldName like '%Modified Date%' AND numReportFieldGroupID=5
	
	
	
--SELECT * FROM ReportFieldGroupMaster
INSERT INTO [dbo].[ReportFieldGroupMaster]([vcFieldGroupName], [bitActive], [bitCustomFieldGroup], [numGroupID], [vcCustomTableName])
SELECT N'Leads Custom Field', 1, 1, 14, N'CFW_FLD_Values' UNION ALL
SELECT N'Prospect Custom Field', 1, 1, 12, N'CFW_FLD_Values' UNION ALL
SELECT N'Account Custom Field', 1, 1, 13, N'CFW_FLD_Values'	


--SELECT * FROM ReportModuleGroupFieldMappingMaster WHERE numReportFieldGroupID=15
INSERT INTO [dbo].[ReportModuleGroupFieldMappingMaster]([numReportModuleGroupID], [numReportFieldGroupID])
SELECT 1, 27 UNION ALL
SELECT 5, 27 UNION ALL
SELECT 14, 27 UNION ALL
SELECT 16, 27 UNION ALL
SELECT 1, 28 UNION ALL
SELECT 5, 28 UNION ALL
SELECT 14, 28 UNION ALL
SELECT 16, 28 UNION ALL
SELECT 1, 29 UNION ALL
SELECT 5, 29 UNION ALL
SELECT 14, 29 UNION ALL
SELECT 16, 29 

---Add Custom Fields to Other Module(Orders)
INSERT INTO [dbo].[ReportModuleGroupFieldMappingMaster]([numReportModuleGroupID], [numReportFieldGroupID])
SELECT 8, 15 UNION ALL
SELECT 8, 27 UNION ALL
SELECT 8, 28 UNION ALL
SELECT 8, 29 UNION ALL
SELECT 9, 15 UNION ALL
SELECT 9, 27 UNION ALL
SELECT 9, 28 UNION ALL
SELECT 9, 29 UNION ALL
SELECT 10, 15 UNION ALL
SELECT 10, 27 UNION ALL
SELECT 10, 28 UNION ALL
SELECT 10, 29 UNION ALL
SELECT 13, 15 UNION ALL
SELECT 13, 27 UNION ALL
SELECT 13, 28 UNION ALL
SELECT 13, 29
--=========================================================================================	
DECLARE @v sql_variant 
SET @v = N'0: BizForm Wizards
1: Regular Form (Grid Settings)
2: Other Form
3: WorkFlow Form'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'DynamicFormMaster', N'COLUMN', N'tintFlag'


INSERT INTO dbo.DynamicFormMaster (numFormId,vcFormName,cCustomFieldsAssociated,
	cAOIAssociated,bitDeleted,tintFlag,bitWorkFlow,vcLocationID,bitAllowGridColor)
SELECT 68,'Organization','Y','N',0,3,0,'1,12,13,14',0 UNION
SELECT 69,'Contacts','Y','N',0,3,0,'4',0 UNION
SELECT 70,'Opportunities & Orders','Y','N',0,3,0,'2,6',0 UNION
SELECT 71,'BizDocs','Y','N',0,3,0,'',0 UNION
SELECT 72,'Cases','Y','N',0,3,0,'3',0 UNION
SELECT 73,'Projects','Y','N',0,3,0,'11',0 UNION
SELECT 74,'Items','Y','N',0,3,0,'5',0


--SELECT * FROM dbo.DynamicFormMaster WHERE tintFlag=3

--SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID=36

--Organization
INSERT INTO dbo.DycFormField_Mapping (
	numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,
	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,
	bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
SELECT numModuleID,numFieldID,numDomainID,68,0,0,vcFieldName,vcAssociatedControlType,vcPropertyName,
	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,0,0,
	0,0,1,0,0,0,bitRequired,numFormFieldID,intSectionID,0
	 FROM dbo.DycFormField_Mapping WHERE numFormID=36


--Contacts
INSERT INTO dbo.DycFormField_Mapping (
	numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,
	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,
	bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
SELECT numModuleID,numFieldID,numDomainID,69,0,0,vcFieldName,vcAssociatedControlType,vcPropertyName,
	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,0,0,
	0,0,1,0,0,0,bitRequired,numFormFieldID,intSectionID,0
	 FROM dbo.DycFormField_Mapping WHERE numFormID=10


--Opportunities & Orders
INSERT INTO dbo.DycFormField_Mapping (
	numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,
	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,
	bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
SELECT numModuleID,numFieldID,numDomainID,70,0,0,vcFieldName,vcAssociatedControlType,vcPropertyName,
	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,0,0,
	0,0,1,0,0,0,bitRequired,numFormFieldID,intSectionID,0
	 FROM dbo.DycFormField_Mapping WHERE numFormID=39

--BizDocs
INSERT INTO dbo.DycFormField_Mapping (
	numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,
	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,
	bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
SELECT numModuleID,numFieldID,numDomainID,71,0,0,vcFieldName,vcAssociatedControlType,vcPropertyName,
	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,0,0,
	0,0,1,0,0,0,bitRequired,numFormFieldID,intSectionID,0
	 FROM dbo.DycFormField_Mapping WHERE numFormID=7
	 

--Cases
INSERT INTO dbo.DycFormField_Mapping (
	numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,
	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,
	bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
SELECT numModuleID,numFieldID,numDomainID,72,0,0,vcFieldName,vcAssociatedControlType,vcPropertyName,
	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,0,0,
	0,0,1,0,0,0,bitRequired,numFormFieldID,intSectionID,0
	 FROM dbo.DycFormField_Mapping WHERE numFormID=12
	 

--Projects
INSERT INTO dbo.DycFormField_Mapping (
	numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,
	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,
	bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
SELECT numModuleID,numFieldID,numDomainID,73,0,0,vcFieldName,vcAssociatedControlType,vcPropertyName,
	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,0,0,
	0,0,1,0,0,0,bitRequired,numFormFieldID,intSectionID,0
	 FROM dbo.DycFormField_Mapping WHERE numFormID=13
	 

--Items
INSERT INTO dbo.DycFormField_Mapping (
	numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,
	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,
	bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
SELECT numModuleID,numFieldID,numDomainID,74,0,0,vcFieldName,vcAssociatedControlType,vcPropertyName,
	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,0,0,
	0,0,1,0,0,0,bitRequired,numFormFieldID,intSectionID,0
	 FROM dbo.DycFormField_Mapping WHERE numFormID=21	 	 	 
	 
	 
--=========================================================================================

ALTER TABLE dbo.OpportunityBizDocs ADD
	bitAutoCreated bit NULL	 
	
UPDATE OpportunityBizDocs SET bitAutoCreated=0

UPDATE dbo.OpportunityBizDocs SET bitAutoCreated=1 WHERE numOppBizDocsId IN (
SELECT numOppBizDocsId FROM dbo.OpportunityAutomationQueueExecution WHERE numRuleID IN (1,2))


--=========================================================================================

--SELECT * FROM dbo.ReportFieldGroupMaster
INSERT INTO [dbo].[ReportFieldGroupMaster]([vcFieldGroupName], [bitActive], [bitCustomFieldGroup], [numGroupID], [vcCustomTableName])
SELECT N'Item Price Level', 1, NULL, NULL, NULL

--SELECT *FROM dbo.ReportModuleGroupMaster
INSERT INTO [dbo].[ReportModuleGroupFieldMappingMaster]([numReportModuleGroupID], [numReportFieldGroupID])
SELECT 19, 30 

INSERT INTO [dbo].[ReportModuleGroupFieldMappingMaster]([numReportModuleGroupID], [numReportFieldGroupID])
SELECT 23, 30 





SELECT * FROM dbo.DycFieldMaster WHERE numModuleID=4 AND vcLookBackTableName='PricingRuleTable'

INSERT INTO [dbo].[DycFieldMaster]([numModuleID], [numDomainID], [vcFieldName], [vcDbColumnName], [vcOrigDbColumnName], [vcPropertyName], [vcLookBackTableName], [vcFieldDataType], [vcFieldType], [vcAssociatedControlType], [vcToolTip], [vcListItemType], [numListID], [PopupFunctionName], [order], [tintRow], [tintColumn], [bitInResults], [bitDeleted], [bitAllowEdit], [bitDefault], [bitSettingField], [bitAddField], [bitDetailField], [bitAllowSorting], [bitWorkFlowField], [bitImport], [bitExport], [bitAllowFiltering], [bitInlineEdit], [bitRequired], [intColumnWidth], [intFieldMaxLength])
SELECT 4, NULL, N'Qty From', N'intFromQty', N'intFromQty', NULL, N'PricingRuleTable', N'N', N'R', N'TextBox', NULL, NULL, 0, NULL, 18, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL
UNION
SELECT 4, NULL, N'Qty To', N'intToQty', N'intToQty', NULL, N'PricingRuleTable', N'N', N'R', N'TextBox', NULL, NULL, 0, NULL, 18, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL
UNION
SELECT 4, NULL, N'Rule Type', N'vcRuleType', N'vcRuleType', NULL, N'PricingRuleTable', N'V', N'R', N'TextBox', NULL, NULL, 0, NULL, 18, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL
UNION
SELECT 4, NULL, N'Discount Type', N'vcDiscountType', N'vcDiscountType', NULL, N'PricingRuleTable', N'V', N'R', N'TextBox', NULL, NULL, 0, NULL, 18, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL
UNION
SELECT 4, NULL, N'Discount', N'decDiscount', N'decDiscount', NULL, N'PricingRuleTable', N'M', N'R', N'TextBox', NULL, NULL, 0, NULL, 18, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL
UNION
SELECT 4, NULL, N'Price Level Price', N'monPriceLevelPrice', N'monPriceLevelPrice', NULL, N'PricingRuleTable', N'M', N'R', N'TextBox', NULL, NULL, 0, NULL, 18, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL
UNION
SELECT 4, NULL, N'Price Level Type', N'vcPriceLevelType', N'vcPriceLevelType', NULL, N'PricingRuleTable', N'V', N'R', N'TextBox', NULL, NULL, 0, NULL, 18, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL


INSERT INTO [dbo].[ReportFieldGroupMappingMaster]([numReportFieldGroupID], [numFieldID], [vcFieldName], 
[vcAssociatedControlType], [vcFieldDataType], [bitAllowSorting], [bitAllowGrouping], [bitAllowAggregate], [bitAllowFiltering])
SELECT 30, numFieldId, vcFieldName, vcDbColumnName, vcFieldDataType, 0, 0, 0, 0 FROM DycFieldMaster 
WHERE numModuleID=4 AND vcLookBackTableName='PricingRuleTable'





/*******************Manish Script******************/
/******************************************************************/


/******************************************************************
Project: BACRMUI   Date: 20.Sep.2013
Comments: Please go to Manage Authorization, and pull up the Opportunity/Orders module. In that list, 
		  you will see "Sales Opportunity Details", which needs to be relabeld to "Sales Opportunity/Order Details" 
		  there is also one called "Purchase Opportunity Details" and that one must be relabeled "Purchase Opportunity/Order Details" 
*******************************************************************/
BEGIN TRANSACTION
--SELECT * FROM PageMaster WHERE numModuleID = 10 AND vcPageDesc = 'Sales Opportunity Details'
--SELECT * FROM PageMaster WHERE numModuleID = 10 AND vcPageDesc = 'Purchase Opportunity Details'
UPDATE PageMaster SET vcPageDesc = 'Sales Opportunity/Order Details' WHERE numModuleID = 10 AND vcPageDesc = 'Sales Opportunity Details'
UPDATE PageMaster SET vcPageDesc = 'Purchase Opportunity/Order Details' WHERE numModuleID = 10 AND vcPageDesc = 'Purchase Opportunity Details'

ROLLBACK

/******************************************************************
Project: BACRMUI   Date: 16.Sep.2013
Comments: Add New Page Element
*******************************************************************/

BEGIN TRANSACTION

SELECT * FROM dbo.PageElementMaster 
INSERT INTO dbo.PageElementMaster 
(numElementID,vcElementName,vcUserControlPath,vcTagName,bitCustomization,bitAdd,bitDelete)
SELECT 43,'CartItemFilters','~/UserControls/CartItemFilter.ascx','{#CartItemFilter#}',1,NULL,NULL
--------------------------------
INSERT INTO dbo.EmailMergeModule (numModuleID,vcModuleName,tintModuleType) 
SELECT 43,'CartItemFilters',1
--------------------------------
SELECT * FROM dbo.EmailMergeFields
INSERT INTO dbo.EmailMergeFields (vcMergeField,vcMergeFieldValue,numModuleID,tintModuleType)
SELECT 'FilterName','##FilterName##',43,1
UNION 
SELECT 'FilterControl','##FilterControl##',43,1
--------------------------------
INSERT INTO dbo.PageElementDetail (numElementID,numAttributeID,vcAttributeValue,numSiteID,numDomainID,vcHtml)
SELECT 43,96,'',91,172,''
--------------------------------
INSERT INTO dbo.PageElementAttributes (numElementID,vcAttributeName,vcControlType,vcControlValues,bitEditor) 
SELECT 43,'Html Customize','HtmlEditor','',1
--------------------------------

ROLLBACK


/******************************************************************
Project: BACRMUI   Date: 10.Sep.2013
Comments: Add New Payment Method
*******************************************************************/
BEGIN TRANSACTION

GO
/****** Object:  Table [dbo].[DycCartFilters]    Script Date: 09/12/2013 18:56:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DycCartFilters](
	[numFieldID] [numeric](18, 0) NOT NULL,
	[numFormID] [numeric](18, 0) NOT NULL,
	[numSiteID] [numeric](18, 0) NOT NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[numFilterType] [int] NOT NULL,
	[bitCustomField] [bit] NOT NULL,
	[tintOrder] [tinyint] NULL,
 CONSTRAINT [PK_DycCartFilters] PRIMARY KEY CLUSTERED 
(
	[numFieldID] ASC,
	[numFormID] ASC,
	[numSiteID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 = Checkbox' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'DycCartFilters', @level2type=N'COLUMN', @level2name=N'numFilterType'

--------------------------
--SELECT * FROM DynamicFormMaster
INSERT dbo.DynamicFormMaster 
(numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,tintFlag,bitWorkFlow,vcLocationID,bitAllowGridColor )
SELECT 77 ,'Cart Item Filter Fields', 'Y', 'N',0,0, NULL,'5', NULL 
--------------------------

INSERT INTO dbo.DycFormField_Mapping 
	(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
	 bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
SELECT numModuleID,numFieldID,numDomainID,77,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,1,
	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,0,bitExport,
	 1,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor 
	 FROM dbo.DycFormField_Mapping WHERE numFormID = 20
	 AND vcFieldName IN ('Item Classification','Item Group','Item Class','Item Category')

ROLLBACK

/******************************************************************
Project: BACRMUI   Date: 10.Sep.2013
Comments: Add New Payment Method
*******************************************************************/
BEGIN TRANSACTION

SET IDENTITY_INSERT dbo.ListDetails ON

INSERT INTO dbo.ListDetails 
(numListItemID,numListID,vcData,numCreatedBY,bintCreatedDate,numModifiedBy,bintModifiedDate,bitDelete,numDomainID,constFlag,sintOrder)
SELECT 84,31,	'Sales Inquiry',	1,	NULL,	NULL,	NULL,	0,	1,	1,	0

SET IDENTITY_INSERT dbo.ListDetails OFF
ROLLBACK


/******************************************************************
Project: BACRMUI   Date: 29.Aug.2013
Comments: Add Ecommerce settings
*******************************************************************/
BEGIN TRANSACTION
ALTER TABLE eCommerceDTL ADD bitSkipStep2 BIT,bitDisplayCategory BIT
ROLLBACK
