/******************************************************************
Project: Release 14.4 Date: 24.OCT.2020
Comments: ALTER SCRIPTS
*******************************************************************/

/******************************************** PRASHANT *********************************************/

ALTER TABLE Domain ADD bitEnableSmartyStreets BIT DEFAULT 0
ALTER TABLE Domain ADD vcSmartyStreetsAPIKeys VARCHAR(500) NULL

/******************************************** SANDEEP *********************************************/

INSERT INTO dbo.EmailMergeFields
(
	vcMergeField,vcMergeFieldValue,numModuleID,tintModuleType
)
VALUES
(
	'Price Level Table','##PriceLevelTable##',3,1
)

-------------------------------------------------

ALTER TABLE Domain ADD bitReceiveOrderWithNonMappedItem BIT DEFAULT 0
ALTER TABLE Domain ADD numItemToUseForNonMappedItem NUMERIC(18,0) DEFAULT 0
ALTER TABLE OpportunityItems ADD bitMappingRequired BIT DEFAULT 0
-----------------------------------------------

UPDATE DycFormField_Mapping SET bitDefault=0 WHERE numFormID=43 AND bitDefault=1

-----------------------------------------------

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	8,'Type','TaskTypeName','TaskTypeName','TaskTypeName','T','V','R','SelectBox','',0,1,0,0,1,1,0,0,0,1
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	8,@numFieldID,43,0,0,'Type','SelectBox',1,0,1,1,0,0,0,1
)

UPDATE DycFormConfigurationDetails SET numFieldId=@numFieldID WHERE numFormId=43 AND numFieldId=1

----------------------------------------------

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	8,'Description','Description','Description','Description','T','V','R','TextArea','',0,1,0,1,0,1,0,0,0,1
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	8,@numFieldID,43,0,0,'Description','TextArea',1,0,0,1,0,0,0,1
)

UPDATE DycFormConfigurationDetails SET numFieldId=@numFieldID WHERE numFormId=43 AND numFieldId=184

-----------------------------------------------

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	8,'Due Date','DueDate','DueDate','DueDate','T','V','R','DateField','',0,1,0,0,1,1,0,0,1,1
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	8,@numFieldID,43,0,0,'Due Date','DateField',1,0,1,1,0,0,1,1
)

UPDATE DycFormConfigurationDetails SET numFieldId=@numFieldID WHERE numFormId=43 AND numFieldId=3

---------------------------------------------------------

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	8,'Progress','TotalProgress','TotalProgress','TotalProgress','T','V','R','TextBox','',0,1,0,0,0,1,0,0,1,0
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	8,@numFieldID,43,0,0,'Progress','TextBox',1,0,0,1,0,0,1,0
)

UPDATE DycFormConfigurationDetails SET numFieldId=@numFieldID WHERE numFormId=43 AND numFieldId=4

-----------------------------------------------

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	8,'Assigned to','numAssignedTo','numAssignedTo','numAssignedTo','T','V','R','SelectBox','',0,1,0,0,0,1,0,0,0,1
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	8,@numFieldID,43,0,0,'Assigned to','SelectBox',1,0,0,1,0,0,0,1
)

UPDATE DycFormConfigurationDetails SET numFieldId=@numFieldID WHERE numFormId=43 AND numFieldId=5

------------------------------------------------------
DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	8,'Priority','Priority','Priority','Priority','T','V','R','SelectBox','',0,1,0,0,0,1,0,0,0,1
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	8,@numFieldID,43,0,0,'Priority','SelectBox',1,0,0,1,0,0,0,1
)

UPDATE DycFormConfigurationDetails SET numFieldId=@numFieldID WHERE numFormId=43 AND numFieldId=183

---------------------------------------------------------

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	8,'Activity','Activity','Activity','Activity','T','V','R','SelectBox','',0,1,0,0,0,1,0,0,0,1
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	8,@numFieldID,43,0,0,'Activity','SelectBox',1,0,0,1,0,0,0,1
)

UPDATE DycFormConfigurationDetails SET numFieldId=@numFieldID WHERE numFormId=43 AND numFieldId=182

------------------------------------------------

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	8,'Organization (Rating)','OrgName','OrgName','OrgName','T','V','R','TextBox','',0,1,0,0,0,1,0,0,1,1
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	8,@numFieldID,43,0,0,'Organization (Rating)','TextBox',1,0,0,1,0,0,1,1
)

UPDATE DycFormConfigurationDetails SET numFieldId=@numFieldID WHERE numFormId=43 AND numFieldId=8




