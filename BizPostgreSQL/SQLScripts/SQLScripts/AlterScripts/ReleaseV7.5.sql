/******************************************************************
Project: Release 7.5 Date: 17.MAY.2017
Comments: ALTER SCRIPTS
*******************************************************************/

/************************** SANDEEP ************************/

INSERT INTO DynamicFormMaster
(
	numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,tintFlag
)
VALUES
(
	133,'Import/Update Organization','Y','N',0,2
),
(
	134,'Import/Update Contact','Y','N',0,2
)

------------------------------------------------------------------------

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitImport,bitAllowFiltering,numFormFieldID,intSectionID,bitAllowGridColor) 
SELECT 
	numModuleID,numFieldID,133,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitImport,bitAllowFiltering,numFormFieldID,intSectionID,bitAllowGridColor
FROM 
	DycFormField_Mapping 
WHERE 
	numFormID=36 
	AND bitImport=1 
	AND numFieldID NOT IN (51,52,53,56,59,60,61,62,63,64,65,66,67,68,82,83,84,85,86,94,386,387,89,347)

----------------------------------------------------------------------

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitImport,bitAllowFiltering,numFormFieldID,intSectionID,bitAllowGridColor) 
SELECT 
	2,numFieldID,134,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitImport,bitAllowFiltering,numFormFieldID,intSectionID,bitAllowGridColor
FROM 
	DycFormField_Mapping 
WHERE 
	numFormID=36 
	AND bitImport=1 
	AND numFieldID IN (51,52,53,56,59,60,61,62,63,64,65,66,67,68,82,83,84,85,86,94,386,387,380)


--------------------------------------------------------------------------------------

BEGIN TRY
BEGIN TRANSACTION
	UPDATE DycFieldMaster SET vcDbColumnName='numDivisionID',vcOrigDbColumnName='numDivisionID',vcPropertyName='numDivisionID',vcLookBackTableName='DivisionMaster' WHERE numFieldId=539

	INSERT INTO DycFormField_Mapping
	(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitImport,bitAllowFiltering,intSectionID,bitAllowGridColor) 
	VALUES
	(1,539,133,0,0,'Organization ID','Label','DivisionID',1,0,1,1,0,1,1,1,1,1,0)

	INSERT INTO DycFormField_Mapping
	(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitImport,bitAllowFiltering,intSectionID,bitAllowGridColor) 
	VALUES
	(1,539,134,0,0,'Organization ID','Label','DivisionID',1,0,1,1,0,1,1,1,1,1,0)

	INSERT INTO DycFormField_Mapping
	(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitImport,bitAllowFiltering,intSectionID,bitAllowGridColor) 
	VALUES
	(1,860,134,0,0,'Contact ID','Label','ContactID',1,0,1,1,0,1,1,1,1,1,0)
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH

----------------------------------------------------------------------
ALTER TABLE DycFieldMaster ALTER COLUMN vcFieldName VARCHAR(100)
ALTER TABLE DynamicFormField_Validation ALTER COLUMN vcNewFormFieldName VARCHAR(100)
ALTER TABLE DycFormField_Mapping ALTER COLUMN vcFieldName VARCHAR(100)
ALTER TABLE Import_File_Master ADD tintImportType TINYINT

UPDATE DycFieldMaster SET vcPropertyName = 'DivisionID' WHERE numFieldId=539
UPDATE DycFormField_Mapping SET vcPropertyName = 'DivisionID' WHERE numFieldId=539
UPDATE DycFieldMaster SET vcPropertyName = 'ContactID' WHERE numFieldId=860
UPDATE DycFormField_Mapping SET vcPropertyName = 'ContactID' WHERE numFieldId=860
UPDATE DycFormField_Mapping SET vcPropertyName = 'ContactPhoneExt' WHERE numFormID=134 AND numFieldID=60
UPDATE DycFormField_Mapping SET vcPropertyName = 'CellPhone' WHERE numFormID=134 AND numFieldID=61
UPDATE DycFormField_Mapping SET vcPropertyName = 'Team' WHERE numFormID=134 AND numFieldID=65
UPDATE DycFormField_Mapping SET vcPropertyName = 'Department' WHERE numFormID=134 AND numFieldID=68

UPDATE DycFormField_Mapping SET vcFieldName = 'Relationship (e.g. Customer, Vendor, etc�)' WHERE numFormID=133 AND numFieldID=6
UPDATE DycFormField_Mapping SET vcFieldName = 'Relationship Type (e.g. Leads, Prospect or Accounts)' WHERE numFormID=133 AND numFieldID=451
UPDATE DycFormField_Mapping SET vcFieldName = 'Group (e.g. My Lead, Web Lead  or Public Lead)' WHERE numFormID=133 AND numFieldID=19
UPDATE DycFormField_Mapping SET vcFieldName = 'Item Type (e.g. Inventory Item, Non-Inventory Item or Service)' WHERE numFormID=20 AND numFieldID=294

DELETE FROM DycFormField_Mapping WHERE numFormID=20 AND numFieldID IN (195,197,200,235,293,233,469,470,471,320,390,391,236,237)

INSERT INTO DycFormSectionDetail (numFormID,intSectionID,vcSectionName) VALUES (20,5,'Item Price Level Details')

UPDATE DycFormField_Mapping SET numFormID=20,intSectionID=5 WHERE numFormID=132 AND numFieldID NOT IN (193,203,211,236,237,281)


UPDATE DynamicFormMaster SET vcFormName='Items' WHERE numFormId=20
UPDATE DynamicFormMaster SET vcFormName='Organizations' WHERE numFormId=133
UPDATE DynamicFormMaster SET vcFormName='Contacts' WHERE numFormId=134
UPDATE DynamicFormMaster SET vcFormName='Item Warehouses' WHERE numFormId=48
UPDATE DynamicFormMaster SET vcFormName='Assembly/Kit Child Items' WHERE numFormId=31
UPDATE DynamicFormMaster SET vcFormName='Item Images' WHERE numFormId=54
UPDATE DynamicFormMaster SET vcFormName='Item Vendors' WHERE numFormId=55
UPDATE DynamicFormMaster SET vcFormName='Organization Correspondence' WHERE numFormId=43
UPDATE DynamicFormMaster SET vcFormName='Address Details' WHERE numFormId=98



INSERT INTO DycFormSectionDetail (numFormID,intSectionID,vcSectionName,Loc_id) VALUES (133,1,'Organization Detail',0),(133,2,'Organization Custom Fields',1)
INSERT INTO DycFormSectionDetail (numFormID,intSectionID,vcSectionName,Loc_id) VALUES (134,1,'Contact Detail',0),(134,2,'Contact Custom Fields',4)
 
----------------------------------------------

DELETE FROM ImportOrganizationContactReference WHERE numDivisionID NOT IN (SELECT numDivisionID FROM DivisionMaster)

USE [Production.2014]
GO

ALTER TABLE [dbo].[ImportOrganizationContactReference]  WITH CHECK ADD  CONSTRAINT [FK_ImportOrganizationContactReference_DivisionMaster] FOREIGN KEY([numDivisionID])
REFERENCES [dbo].[DivisionMaster] ([numDivisionID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[ImportOrganizationContactReference] CHECK CONSTRAINT [FK_ImportOrganizationContactReference_DivisionMaster]
GO


-----------------------------------------------------
DELETE FROM ImportActionItemReference WHERE numDivisionID NOT IN (SELECT numDivisionID FROM DivisionMaster)
DELETE FROM ImportActionItemReference WHERE numContactID NOT IN (SELECT numContactID FROM AdditionalContactsInformation)

USE [Production.2014]
GO

ALTER TABLE [dbo].[ImportActionItemReference]  WITH CHECK ADD  CONSTRAINT [FK_ImportActionItemReference_AdditionalContactsInformation] FOREIGN KEY([numContactID])
REFERENCES [dbo].[AdditionalContactsInformation] ([numContactId])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[ImportActionItemReference] CHECK CONSTRAINT [FK_ImportActionItemReference_AdditionalContactsInformation]
GO


USE [Production.2014]
GO

ALTER TABLE [dbo].[ImportActionItemReference]  WITH CHECK ADD  CONSTRAINT [FK_ImportActionItemReference_DivisionMaster] FOREIGN KEY([numDivisionID])
REFERENCES [dbo].[DivisionMaster] ([numDivisionID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[ImportActionItemReference] CHECK CONSTRAINT [FK_ImportActionItemReference_DivisionMaster]
GO


UPDATE DycFormField_Mapping SET numFieldID=211 WHERE numFieldID=382 AND numFormID=54
DELETE FROM DycFormField_Mapping WHERE numFormID=55 AND vcPropertyName='LeadTimeDays'


ALTER TABLE Domain ADD bitElasticSearch BIT DEFAULT 1

UPDATE Domain SET bitElasticSearch=1 WHERE numDomainId IN (1,72,153,170,172,204,206)


/************************** ANUSHA ************************/


/****** Object:  Table [dbo].[PricingTable]    Script Date: 08-05-2017 00:11:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PricingNamesTable](
	[pricingNamesID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numCreatedBy] [numeric](18, 0) NOT NULL,
	[dtCreatedDate] [datetime] NOT NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[dtModifiedDate] [datetime] NULL,
	[tintPriceLevel] [tinyint] NOT NULL,
	[vcPriceLevelName] [varchar](300) NULL
 CONSTRAINT [PK_PricingNamesTable] PRIMARY KEY CLUSTERED 
(
	[pricingNamesID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO
