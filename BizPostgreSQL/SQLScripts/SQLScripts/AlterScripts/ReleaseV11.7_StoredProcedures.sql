/******************************************************************
Project: Release 11.7 Date: 10.APR.2019
Comments: STORE PROCEDURES
*******************************************************************/


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DashboardTemplate_GetAllByDomain')
DROP PROCEDURE USP_DashboardTemplate_GetAllByDomain
GO
CREATE PROCEDURE [dbo].[USP_DashboardTemplate_GetAllByDomain]      
@numDomainID AS NUMERIC(18,0)
,@numUserCntID AS NUMERIC(18,0) = 0
AS
BEGIN 

	IF(@numUserCntID=0)
		BEGIN
			SELECT * FROM DashboardTemplate WHERE numDomainID=@numDomainID
		END
	ELSE
		BEGIN
			DECLARE @vcDashboardTemplateIDs AS VARCHAR(250)
			
			SELECT @vcDashboardTemplateIDs = vcDashboardTemplateIDs from UserMaster where numDomainID=@numDomainID	AND numUserDetailId	=@numUserCntID

			IF LEN(ISNULL(@vcDashboardTemplateIDs,'')) = 0
				BEGIN
					SELECT  @vcDashboardTemplateIDs = numDashboardTemplateID FROM UserMaster WHERE numDomainID=@numDomainID
				END
			
			SELECT * FROM DashboardTemplate WHERE numDomainID=@numDomainID AND 
			1= (CASE WHEN LEN(ISNULL(@vcDashboardTemplateIDs,'')) > 0 THEN (CASE WHEN numTemplateID IN (SELECT Id FROM dbo.SplitIDs((@vcDashboardTemplateIDs),',')) THEN 1 ELSE 0 END) ELSE 1 END)
			
		END
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCompanyInfoDtlPl]    Script Date: 07/26/2008 16:16:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--usp_GetCompanyInfoDtlPl 91099,72,-333
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcompanyinfodtlpl')
DROP PROCEDURE usp_getcompanyinfodtlpl
GO
CREATE PROCEDURE [dbo].[usp_GetCompanyInfoDtlPl]                                                                                                                   
@numDivisionID numeric(9) ,                      
@numDomainID numeric(9) ,        
@ClientTimeZoneOffset as int                              
    --                                                                           
AS                                                                            
BEGIN                                                                            
 SELECT CMP.numCompanyID,               
 CMP.vcCompanyName,                
 (select vcdata from listdetails where numListItemID = DM.numStatusID) as numCompanyStatusName,
 DM.numStatusID as numCompanyStatus,                                                                           
 (select vcdata from listdetails where numListItemID = CMP.numCompanyRating) as numCompanyRatingName, 
 CMP.numCompanyRating,             
 (select vcdata from listdetails where numListItemID = CMP.numCompanyType) as numCompanyTypeName,
 CMP.numCompanyType,              
 CMP.numCompanyType as numCmptype,               
 DM.bitPublicFlag,                 
 DM.numDivisionID,                                   
 DM. vcDivisionName,        
dbo.getCompanyAddress(dm.numdivisionId,1,dm.numDomainID) as Address,
dbo.getCompanyAddress(dm.numdivisionId,2,dm.numDomainID) as ShippingAddress,          
  isnull(AD2.vcStreet,'') as vcShipStreet,
  isnull(AD2.vcCity,'') as vcShipCity,
  isnull(dbo.fn_GetState(AD2.numState),'')  as vcShipState,
  ISNULL((SELECT numShippingZone FROM [State] WHERE (numDomainID=@numDomainID OR ConstFlag=1) AND numStateID=AD2.numState),0) AS numShippingZone,
 (select vcdata from listdetails where numListItemID = DM.numFollowUpStatus) as numFollowUpStatusName,
 DM.numFollowUpStatus,                
 CMP.vcWebLabel1,               
 CMP.vcWebLink1,               
 CMP.vcWebLabel2,               
 CMP.vcWebLink2,                                                                             
 CMP.vcWebLabel3,               
 CMP.vcWebLink3,               
 CMP.vcWeblabel4,               
 CMP.vcWebLink4,                                                                       
 DM.tintBillingTerms,              
 DM.numBillingDays,              
 DM.tintInterestType,              
 DM.fltInterest,                                                  
 isnull(AD2.vcPostalCode,'') as vcShipPostCode, 
 AD2.numCountry vcShipCountry,                          
 DM.bitPublicFlag,              
 (select vcdata from listdetails where numListItemID = DM.numTerID)  as numTerIDName,
 DM.numTerID,                                                                         
 (select vcdata from listdetails where numListItemID = CMP.numCompanyIndustry) as numCompanyIndustryName, 
  CMP.numCompanyIndustry,              
 (select vcdata from listdetails where numListItemID = CMP.numCompanyCredit) as  numCompanyCreditName,
 CMP.numCompanyCredit,                                                                             
 isnull(CMP.txtComments,'') as txtComments,               
 isnull(CMP.vcWebSite,'') vcWebSite,              
 (select vcdata from listdetails where numListItemID = CMP.numAnnualRevID) as  numAnnualRevIDName,
 CMP.numAnnualRevID,                
 (select vcdata from listdetails where numListItemID = CMP.numNoOfEmployeesID) as numNoOfEmployeesIDName,               
CMP.numNoOfEmployeesID,                                                                           
    (select vcdata from listdetails where numListItemID = CMP.vcProfile) as vcProfileName, 
 CMP.vcProfile,              
 DM.numCreatedBy,               
 dbo.fn_GetContactName(DM.numRecOwner) as RecOwner,               
 dbo.fn_GetContactName(DM.numCreatedBy)+' ' + convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintCreatedDate)) as vcCreatedBy,               
 DM.numRecOwner,                
 dbo.fn_GetContactName(DM.numModifiedBy)+' ' +convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintModifiedDate)) as vcModifiedBy,                    
 isnull(DM.vcComPhone,'') as vcComPhone, 
 DM.vcComFax,                   
 DM.numGrpID,  
 (select vcGrpName from Groups where numGrpId= DM.numGrpID) as numGrpIDName,              
 (select vcdata from listdetails where numListItemID = CMP.vcHow) as vcInfoSourceName,
 CMP.vcHow as vcInfoSource,                                                                            
 DM.tintCRMType,                                                                         
 (SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= DM.numCampaignID) as  numCampaignIDName,
 DM.numCampaignID,              
 DM.numAssignedBy, isnull(DM.bitNoTax,0) bitNoTax,                     
 (select A.vcFirstName+' '+A.vcLastName                       
 from AdditionalContactsInformation A                  
 join DivisionMaster D                        
 on D.numDivisionID=A.numDivisionID
 where A.numContactID=DM.numAssignedTo) as numAssignedToName,DM.numAssignedTo,
 (select  count(*) from dbo.GenericDocuments where numRecID= @numDivisionID and vcDocumentSection='A' AND tintDocumentType=2) as DocumentCount ,
 (SELECT count(*)from CompanyAssociations where numDivisionID=@numDivisionID and bitDeleted=0 ) as AssociateCountFrom,                            
 (SELECT count(*)from CompanyAssociations where numAssociateFromDivisionID=@numDivisionID and bitDeleted=0 ) as AssociateCountTo,
 (select vcdata from listdetails where numListItemID = DM.numCompanyDiff) as numCompanyDiffName,               
 DM.numCompanyDiff,DM.vcCompanyDiff,isnull(DM.bitActiveInActive,1) as bitActiveInActive,
(SELECT SUBSTRING(
	(SELECT ','+  A.vcFirstName+' ' +A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN '(' + dbo.fn_GetListItemName(SR.numContactType) + ')' ELSE '' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=DM.numDomainID AND SR.numModuleID=1 AND SR.numRecordID=DM.numDivisionID
				AND UM.numDomainID=DM.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('')),2,200000)) AS numShareWith,
 DM.bitOnCreditHold,
 DM.numDefaultPaymentMethod,
 DM.numAccountClassID,
 DM.tintPriceLevel,DM.vcPartnerCode as vcPartnerCode  ,
  ISNULL(DM.numPartenerSource,0) AS numPartenerSourceId,
 D3.vcPartnerCode+'-'+C3.vcCompanyName as numPartenerSource,
 ISNULL(DM.numPartenerContact,0) AS numPartenerContactId,
A1.vcFirstName+' '+A1.vcLastName AS numPartenerContact,
ISNULL(DM.bitEmailToCase,0) bitEmailToCase,
'' AS vcCreditCards,
----Fetch Last Followup(Template) and EmailSentDate(if any) Value
		  
	(CASE WHEN A2.numECampaignID > 0 THEN dbo.fn_FollowupDetailsInOrgList(A2.numContactID,1,@numDomainID) ELSE '' END) vcLastFollowup,

		----Fetch Last Followup(Template) Value

		----Fetch Next Followup(Template) and ExecutionDate Value
	(CASE WHEN A2.numECampaignID > 0 THEN dbo.fn_FollowupDetailsInOrgList(A2.numContactID,2,@numDomainID) ELSE '' END) vcNextFollowup,										 

		----Fetch Next Followup(Template) Value
(CASE WHEN (SELECt COUNT(*) FROM ExtarnetAccounts WHERE numDivisionID=DM.numDivisionID) >0 THEN 1 ELSE 0 END) AS bitEcommerceAccess,

ISNULL(DM.numDefaultShippingServiceID,0) numShippingService,
ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = DM.numDefaultShippingServiceID),'') AS vcShippingService,
		CASE 
			WHEN vcSignatureType = 0 THEN 'Service Default'
			WHEN vcSignatureType = 1 THEN 'Adult Signature Required'
			WHEN vcSignatureType = 2 THEN 'Direct Signature'
			WHEN vcSignatureType = 3 THEN 'InDirect Signature'
			WHEN vcSignatureType = 4 THEN 'No Signature Required'
			ELSE ''
		END AS vcSignatureType,
		ISNULL(DM.intShippingCompany,0) intShippingCompany,
		CASE WHEN ISNULL(DM.intShippingCompany,0) = 0 THEN '' ELSE ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 82 AND numListItemID=ISNULL(DM.intShippingCompany,0)),'') END AS vcShipVia,
        A2.vcFirstName AS vcFirstName,
		A2.vcLastName  AS vcLastName,
		A2.vcFirstname + ' ' + A2.vcLastName AS vcGivenName,
		A2.numPhone AS numPhone,
		A2.numPhoneExtension As numPhoneExtension,
		A2.vcEmail As vcEmail		     
		                       
 FROM  CompanyInfo CMP    
  join DivisionMaster DM    
  on DM.numCompanyID=CMP.numCompanyID                                                                                                                                        
   LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
   AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
   LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
   AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
    LEFT JOIN divisionMaster D3 ON DM.numPartenerSource = D3.numDivisionID
  LEFT JOIN CompanyInfo C3 ON C3.numCompanyID = D3.numCompanyID    
  LEFT JOIN AdditionalContactsInformation A1 ON DM.numPartenerContact = A1.numContactId 
  LEft join   AdditionalContactsInformation A2 on   DM.numDivisionID = A2.numDivisionId and A2.bitPrimaryContact = 1
  LEFT JOIN DivisionMasterShippingConfiguration ON DM.numDivisionID=DivisionMasterShippingConfiguration.numDivisionID
 where DM.numDivisionID=@numDivisionID   and DM.numDomainID=@numDomainID                                                                                          
END

GO




SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetReportDashboardDTL')
DROP PROCEDURE USP_GetReportDashboardDTL
GO
CREATE PROCEDURE [dbo].[USP_GetReportDashboardDTL]   
@numDomainID AS NUMERIC(9),               
@numDashBoardID AS NUMERIC(9)                 
as  

 SELECT ReportDashboard.*,
		ISNULL(ReportListMaster.bitDefault,0) bitDefault,
		ISNULL(ReportListMaster.intDefaultReportID,0) intDefaultReportID
 FROM ReportDashboard 
		LEFT JOIN ReportListMaster 
 ON ReportDashboard.numReportID=ReportListMaster.numReportID WHERE ReportDashboard.numDomainID=@numDomainID AND numDashBoardID=@numDashBoardID
     
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getuserswithdomains')
DROP PROCEDURE usp_getuserswithdomains
GO
CREATE PROCEDURE [dbo].[usp_GetUsersWithDomains]                            
 @numUserID NUMERIC(18,0)=0,
 @numDomainID NUMERIC(18,0)                       
AS                            
BEGIN                            
	IF @numUserID = 0 AND @numDomainID > 0 --Check if request is made while session is on
	BEGIN                            
		SELECT 
			numUserID
			,vcUserName
			,UserMaster.numGroupID
			,ISNULL(vcGroupName,'-') AS vcGroupName
			,vcUserDesc
			,ISNULL(ADC.vcfirstname,'') +' '+ ISNULL(ADC.vclastname,'') as Name
			,UserMaster.numDomainID
			,vcDomainName
			,UserMaster.numUserDetailId
			,ISNULL(UserMaster.SubscriptionId,'') AS SubscriptionId
			,ISNULL(UserMaster.tintDefaultRemStatus,0) tintDefaultRemStatus
			,ISNULL(UserMaster.numDefaultTaskType,0) numDefaultTaskType
			,ISNULL(UserMaster.bitOutlook,0) bitOutlook
			,UserMaster.vcLinkedinId
			,ISNULL(UserMaster.intAssociate,0) AS intAssociate
			,UserMaster.ProfilePic
			,UserMaster.numDashboardTemplateID
			,UserMaster.vcDashboardTemplateIDs
		FROM 
			UserMaster                      
		JOIN 
			Domain                        
		ON 
			UserMaster.numDomainID =  Domain.numDomainID                       
		LEFT JOIN 
			AdditionalContactsInformation ADC                      
		ON 
			ADC.numContactid=UserMaster.numUserDetailId                     
		LEFT JOIN 
			AuthenticationGroupMaster GM                     
		ON 
			Gm.numGroupID= UserMaster.numGroupID                    
		WHERE 
			tintGroupType=1 -- Application Users                    
		ORDER BY 
			Domain.numDomainID, vcUserDesc                            
	END   
	ELSE IF @numUserID = -1 AND @numDomainID > 0 --Support Email
	BEGIN                            
		SELECT 
			'' AS vcEmailId
			,vcImapUserName
			,ISNULL(bitUseUserName,0) bitUseUserName
			,ISNULL(ImapUserDTL.bitImap,0) bitImap
			,ImapUserDTL.vcImapServerUrl
			,ISNULL(ImapUserDTL.vcImapPassword,'') vcImapPassword
			,ImapUserDTL.bitSSl
			,ImapUserDTL.numPort 
		FROM 
			[ImapUserDetails] ImapUserDTL   
		WHERE 
			ImapUserDTL.numUserCntId = @numUserID 
			AND ImapUserDTL.numDomainID = @numDomainID
	END                         
	ELSE                            
	BEGIN   
		DECLARE @vcSMTPServer VARCHAR(100)
			,@numSMTPPort NUMERIC(18,0)
			,@bitSMTPSSL BIT
			,@bitSMTPAuth BIT
			,@vcIMAPServer  VARCHAR(100)
			,@numIMAPPort NUMERIC(18,0)
			,@bitIMAPSSL BIT
			,@bitIMAPAuth BIT
	    
		SELECT
			@vcSMTPServer=vcSMTPServer
			,@numSMTPPort=numSMTPPort
			,@bitSMTPSSL=bitSMTPSSL
			,@bitSMTPAuth=bitSMTPAuth
			,@vcIMAPServer=vcIMAPServer
			,@numIMAPPort=numIMAPPort
			,@bitIMAPSSL=bitIMAPSSL
			,@bitIMAPAuth=bitIMAPAuth
		FROM
			UniversalSMTPIMAP
	    WHERE
			numDomainID=@numDomainID
		             
		SELECT       
			U.numUserID
			,vcUserName
			,numGroupID
			,vcUserDesc
			,U.numDomainID
			,vcDomainName
			,U.numUserDetailId
			,bitHourlyRate
			,bitActivateFlag
			,monHourlyRate
			,bitSalary
			,numDailyHours
			,bitOverTime
			,numLimDailHrs
			,monOverTimeRate
			,bitMainComm
			,fltMainCommPer
			,bitRoleComm
			,(SELECT COUNT(*) FROM UserRoles WHERE UserRoles.numUserCntID=U.numUserDetailId) AS Roles
			,vcEmailid
			,ISNULL(vcEmailAlias,'') vcEmailAlias
			,vcPassword
			,ISNULL(ExcUserDTL.bitExchangeIntegration,0) bitExchangeIntegration
			,ISNULL(ExcUserDTL.bitAccessExchange,0) bitAccessExchange
			,ISNULL(ExcUserDTL.vcExchPassword,'') as vcExchPassword
			,ISNULL(ExcUserDTL.vcExchPath,'') as vcExchPath
			,ISNULL(ExcUserDTL.vcExchDomain,'') as vcExchDomain
			,ISNULL(U.SubscriptionId,'') as SubscriptionId
			,ISNULL(ImapUserDTL.bitImap,0) bitImap
			,ISNULL(ImapUserDTL.vcImapServerUrl,ISNULL(@vcIMAPServer,'')) vcImapServerUrl
			,ISNULL(ImapUserDTL.vcImapPassword,'') vcImapPassword
			,ISNULL(ImapUserDTL.bitSSl,ISNULL(@bitIMAPSSL,0)) bitSSl
			,ISNULL(ImapUserDTL.numPort,ISNULL(@numIMAPPort,0)) numPort
			,ISNULL(ImapUserDTL.bitUseUserName,ISNULL(@bitIMAPAuth,0)) bitUseUserName
			,(CASE WHEN ISNULL([vcSMTPServer],'') <> '' AND ISNULL(numSMTPPort,0) <> 0 THEN ISNULL(bitSMTPAuth,0) ELSE ISNULL(@bitSMTPAuth,0) END) bitSMTPAuth
			,ISNULL([vcSmtpPassword],'')vcSmtpPassword
			,(CASE WHEN ISNULL([vcSMTPServer],'') = '' THEN ISNULL(@vcSMTPServer,'') ELSE [vcSMTPServer] END) vcSMTPServer
			,(CASE WHEN ISNULL(numSMTPPort,0) = 0 THEN ISNULL(@numSMTPPort,0) ELSE numSMTPPort END) numSMTPPort
			,(CASE WHEN ISNULL([vcSMTPServer],'') <> '' AND ISNULL(numSMTPPort,0) <> 0 THEN ISNULL(bitSMTPSSL,0) ELSE ISNULL(@bitSMTPSSL,0) END) bitSMTPSSL
			,ISNULL(bitSMTPServer,0) bitSMTPServer
			,ISNULL(U.tintDefaultRemStatus,0) tintDefaultRemStatus
			,ISNULL(U.numDefaultTaskType,0) numDefaultTaskType
			,ISNULL(U.bitOutlook,0) bitOutlook
			,ISNULL(numDefaultClass,0) numDefaultClass
			,ISNULL(numDefaultWarehouse,0) numDefaultWarehouse
			,U.vcLinkedinId
			,ISNULL(U.intAssociate,0) as intAssociate
			,U.ProfilePic
			,U.numDashboardTemplateID
			,U.vcDashboardTemplateIDs
		FROM 
			UserMaster U                        
		JOIN 
			Domain D                       
		ON 
			U.numDomainID =  D.numDomainID       
		LEFT JOIN 
			ExchangeUserDetails ExcUserDTL      
		ON 
			ExcUserDTL.numUserID=U.numUserID                      
		LEFT JOIN 
			[ImapUserDetails] ImapUserDTL   
		ON 
			ImapUserDTL.numUserCntId = U.numUserDetailId   
			AND ImapUserDTL.numDomainID = D.numDomainID
		WHERE 
			U.numUserID=@numUserID                            
	 END                            
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManagegeReportDashboard')
DROP PROCEDURE USP_ManagegeReportDashboard
GO
CREATE PROCEDURE [dbo].[USP_ManagegeReportDashboard]
@numDomainID AS NUMERIC(18,0),
@numDashBoardID AS NUMERIC(18,0),
@numReportID AS NUMERIC(18,0),
@numUserCntID AS NUMERIC(9)=0,
@tintReportType AS TINYINT,
@tintChartType AS TINYINT,
@vcHeaderText AS VARCHAR(100)='',
@vcFooterText AS VARCHAR(100)='',
@vcXAxis AS VARCHAR(50),
@vcYAxis AS VARCHAR(50),
@vcAggType AS VARCHAR(50),
@tintReportCategory AS TINYINT,
@numDashboardTemplateID NUMERIC(18,0),
@vcTimeLine VARCHAR(50)
,@vcGroupBy VARCHAR(50)
,@vcTeritorry VARCHAR(MAX)
,@vcFilterBy TINYINT
,@vcFilterValue VARCHAR(MAX)
,@dtFromDate DATE
,@dtToDate DATE
,@numRecordCount INT
,@tintControlField INT
,@vcDealAmount VARCHAR(MAX)
,@tintOppType INT
,@lngPConclAnalysis VARCHAR(MAX)
,@bitTask VARCHAR(MAX)
,@tintTotalProgress INT
,@tintMinNumber INT
,@tintMaxNumber INT
,@tintQtyToDisplay INT
,@vcDueDate VARCHAR(MAX)
AS
BEGIN
	IF @numDashBoardID=0 
	BEGIN
		DECLARE @tintRow AS TINYINT;SET @tintRow=0
		SELECT @tintRow = ISNULL(MAX(tintRow),0) + 1 FROM ReportDashboard WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID AND ISNULL(numDashboardTemplateID,0)=ISNULL(@numDashboardTemplateID,0)
		
		INSERT INTO ReportDashboard 
		(
			numDomainID
			,numReportID
			,numUserCntID
			,tintReportType
			,tintChartType
			,vcHeaderText
			,vcFooterText
			,tintRow
			,tintColumn
			,vcXAxis
			,vcYAxis
			,vcAggType
			,tintReportCategory
			,intHeight
			,intWidth
			,numDashboardTemplateID
			,bitNewAdded
			,vcTimeLine
			,vcGroupBy
			,vcTeritorry
			,vcFilterBy
			,vcFilterValue
			,dtFromDate
			,dtToDate
			,numRecordCount
			,tintControlField
			,vcDealAmount
			,tintOppType
			,lngPConclAnalysis
			,bitTask
			,tintTotalProgress
			,tintMinNumber
			,tintMaxNumber
			,tintQtyToDisplay
			,vcDueDate
		)
		VALUES
		(
			@numDomainID
			,@numReportID
			,@numUserCntID
			,@tintReportType
			,@tintChartType
			,@vcHeaderText
			,@vcFooterText
			,@tintRow
			,1
			,@vcXAxis
			,@vcYAxis
			,@vcAggType
			,@tintReportCategory
			,7
			,8
			,@numDashboardTemplateID
			,1
			,@vcTimeLine
			,@vcGroupBy
			,@vcTeritorry
			,@vcFilterBy
			,@vcFilterValue
			,@dtFromDate
			,@dtToDate
			,@numRecordCount
			,@tintControlField
			,@vcDealAmount
			,@tintOppType
			,@lngPConclAnalysis
			,@bitTask
			,@tintTotalProgress
			,@tintMinNumber
			,@tintMaxNumber
			,@tintQtyToDisplay
			,@vcDueDate
		)
	END
	ELSE IF @numDashBoardID>0
	BEGIN
		UPDATE 
			ReportDashboard 
		SET
			numReportID=@numReportID, 
			tintReportType=@tintReportType, 
			tintChartType=@tintChartType, 
			vcHeaderText=@vcHeaderText, 
			vcFooterText=@vcFooterText,
			vcXAxis=@vcXAxis,
			vcYAxis=@vcYAxis,
			vcAggType=@vcAggType,
			tintReportCategory=@tintReportCategory
			,vcTimeLine=@vcTimeLine
			,vcGroupBy=@vcGroupBy
			,vcTeritorry=@vcTeritorry
			,vcFilterBy=@vcFilterBy
			,vcFilterValue=@vcFilterValue	
			,dtFromDate=@dtFromDate
			,dtToDate=@dtToDate
			,numRecordCount=@numRecordCount
			,tintControlField=@tintControlField
			,vcDealAmount=@vcDealAmount
			,tintOppType = @tintOppType
			,lngPConclAnalysis= @lngPConclAnalysis
			,bitTask=@bitTask
			,tintTotalProgress=@tintTotalProgress
			,tintMinNumber=@tintMinNumber
			,tintMaxNumber=@tintMaxNumber
			,tintQtyToDisplay=@tintQtyToDisplay
			,vcDueDate=@vcDueDate
		WHERE
			numDashBoardID=@numDashBoardID 
			AND numDomainID=@numDomainID
	END
END	
GO
/****** Object:  StoredProcedure [dbo].[USP_OPPDetails]    Script Date: 03/25/2009 15:10:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By Anoop Jayaraj
-- [dbo].[USP_OPPDetails] 1362,1,-333
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppdetails')
DROP PROCEDURE usp_oppdetails
GO
CREATE PROCEDURE [dbo].[USP_OPPDetails]
(
               @numOppID             AS NUMERIC(9)  = NULL,
               @numDomainID          AS NUMERIC(9)  = 0,
               @ClientTimeZoneOffset AS INT)
AS
BEGIN	
  SELECT Opp.numoppid,
         numCampainID,
         ADC.vcFirstname + ' ' + ADC.vcLastName AS numContactId,
         isnull(ADC.vcEmail,'') AS vcEmail,
         isnull(ADC.numPhone,'') AS Phone,
         isnull(ADC.numPhoneExtension,'') AS PhoneExtension,
         Opp.txtComments,
         Opp.numDivisionID,
         tintOppType,
         ISNULL(dateadd(MINUTE,-@ClientTimeZoneOffset,bintAccountClosingDate),'') AS bintAccountClosingDate,
         Opp.numContactID AS ContactID,
         tintSource,
         C2.vcCompanyName + Case when isnull(D2.numCompanyDiff,0)>0 then  '  ' + dbo.fn_getlistitemname(D2.numCompanyDiff) + ':' + isnull(D2.vcCompanyDiff,'') else '' end as vcCompanyname,
         D2.tintCRMType,
         Opp.vcPoppName,
         intpEstimatedCloseDate,
         [dbo].[GetDealAmount](@numOppID,getutcdate(),0) AS monPAmount,
         lngPConclAnalysis,
         monPAmount AS OppAmount,
         numSalesOrPurType,
         Opp.tintActive,
         dbo.fn_GetContactName(Opp.numCreatedby)
           + ' '
           + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintCreatedDate)) AS CreatedBy,
         dbo.fn_GetContactName(Opp.numModifiedBy)
           + ' '
           + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintModifiedDate)) AS ModifiedBy,
         dbo.fn_GetContactName(Opp.numRecOwner) AS RecordOwner,
         Opp.numRecOwner,
         isnull(tintshipped,0) AS tintshipped,
         isnull(tintOppStatus,0) AS tintOppStatus,
         dbo.OpportunityLinkedItems(@numOppID) AS NoOfProjects,
         Opp.numAssignedTo,
         Opp.numAssignedBy,
         (SELECT COUNT(* )
          FROM   dbo.GenericDocuments
          WHERE  numRecID = @numOppID
                 AND vcDocumentSection = 'O') AS DocumentCount,
         isnull(OPR.numRecurringId,0) AS numRecurringId,
		 OPR.dtRecurringDate AS dtLastRecurringDate,
         D2.numTerID,
         Link.numOppID AS numParentOppID,
         Link.vcPoppName AS ParentOppName,
         Link.vcSource AS Source,
         Link.numParentProjectID,
         Link.vcProjectName,
         ISNULL(C.varCurrSymbol,'') varCurrSymbol,
         ISNULL(Opp.fltExchangeRate,0) AS [fltExchangeRate],
         ISNULL(C.chrCurrency,'') AS [chrCurrency],
         ISNULL(C.numCurrencyID,0) AS [numCurrencyID],
		 (Select Count(*) from OpportunityBizDocs 
		 where numOppId=@numOppID and bitAuthoritativeBizDocs=1) As AuthBizDocCount,
		 (SELECT COUNT(distinct numChildOppID) FROM [OpportunityLinking] OL WHERE OL.[numParentOppID] = Opp.numOppId) AS ChildCount,
		 (SELECT COUNT(*) FROM [RecurringTransactionReport] RTR WHERE RTR.[numRecTranSeedId] = Opp.numOppId)  AS RecurringChildCount
		 ,Opp.bintCreatedDate,
		 ISNULL(Opp.bitStockTransfer ,0) bitStockTransfer,	ISNULL(Opp.tintTaxOperator,0) AS tintTaxOperator, 
         isnull(Opp.intBillingDays,0) AS numBillingDays,isnull(Opp.bitInterestType,0) AS bitInterestType,
         isnull(opp.fltInterest,0) AS fltInterest,  isnull(Opp.fltDiscount,0) AS fltDiscount,
         isnull(Opp.bitDiscountType,0) AS bitDiscountType,ISNULL(Opp.bitBillingTerms,0) AS bitBillingTerms,ISNULL(OPP.numDiscountAcntType,0) AS numDiscountAcntType,
         ISNULL((SELECT TOP 1 numCountry FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)),0) AS [numShipCountry],
		 ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)),0) AS [numShipState],
		 ISNULL((SELECT TOP 1 vcState FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)),'') AS [vcShipState],
		 ISNULL((SELECT TOP 1 vcCountry FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)),'') AS [vcShipCountry],
		 ISNULL((SELECT TOP 1 vcPostalCode FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)),'') AS [vcPostalCode],
		 ISNULL(vcCouponCode,'') AS [vcCouponCode],ISNULL(Opp.bitPPVariance,0) AS bitPPVariance,
		 Opp.dtItemReceivedDate,ISNULL(bitUseShippersAccountNo,0) [bitUseShippersAccountNo], ISNULL(D2.vcShippersAccountNo,'') [vcShippersAccountNo],
		 ISNULL(bitUseMarkupShippingRate,0) [bitUseMarkupShippingRate],
		 ISNULL(numMarkupShippingRate,0) [numMarkupShippingRate],
		 ISNULL(Opp.intUsedShippingCompany,0) AS intUsedShippingCompany,
		 ISNULL(SR.[vcValue2],ISNULL(numShippingService,
		 (SELECT TOP 1 ISNULL([SST].[intNsoftEnum],0) FROM [dbo].[ShippingServiceTypes] AS SST 
		 WHERE [SST].[numDomainID] = @numDomainID 
		 AND [SST].[numRuleID] = 0
		 AND [SST].[vcServiceName] IN (SELECT ISNULL([OI].[vcItemDesc],'') FROM [dbo].[OpportunityItems] AS OI 
									 WHERE [OI].[numOppId] = [Opp].[numOppId] 
									 AND [OI].[vcType] = 'Service Item')))) AS numShippingService,
		 ISNULL(Opp.[monLandedCostTotal],0) AS monLandedCostTotal,opp.[vcLanedCost],
		 ISNULL(Opp.vcRecurrenceType,'') AS vcRecurrenceType,
		 ISNULL(RecurrenceConfiguration.numRecConfigID,0) AS numRecConfigID,
		 (CASE WHEN ISNULL(Opp.vcRecurrenceType,'') = '' THEN 0 ELSE 1 END) AS bitRecur,
		 (CASE WHEN RecurrenceTransaction.numRecTranID IS NULL THEN 0 ELSE 1 END) AS bitRecurred,
		 dbo.FormatedDateFromDate(RecurrenceConfiguration.dtStartDate,@numDomainID) AS dtStartDate,
		 dbo.FormatedDateFromDate(RecurrenceConfiguration.dtEndDate,@numDomainID) AS dtEndDate,
		 RecurrenceConfiguration.vcFrequency AS vcFrequency,
		 ISNULL(RecurrenceConfiguration.bitDisabled,0) AS bitDisabled,
		 ISNULL(Opp.dtReleaseDate,'') AS dtReleaseDate,
		 ISNULL(Opp.numAccountClass,0) AS numAccountClass,D3.vcPartnerCode+'-'+C3.vcCompanyName as numPartenerSource,
		ISNULL(Opp.numPartner,0) AS numPartenerSourceId,
		ISNULL(Opp.numPartenerContact,0) AS numPartenerContactId,
		A.vcFirstName+' '+A.vcLastName AS numPartenerContact,
		numShipFromWarehouse, CPN.CustomerPartNo,
		Opp.numBusinessProcessID,
		SLP.Slp_Name AS vcProcessName
  FROM   OpportunityMaster Opp 
         JOIN divisionMaster D2
           ON Opp.numDivisionID = D2.numDivisionID
		   LEFT JOIN divisionMaster D3 ON Opp.numPartner = D3.numDivisionID
		LEFT JOIN CompanyInfo C3 ON C3.numCompanyID = D3.numCompanyID
		LEFT JOIN AdditionalContactsInformation A ON Opp.numPartenerContact = A.numContactId
		LEFT JOIN Sales_process_List_Master AS SLP ON Opp.numBusinessProcessID=SLP.Slp_Id
         LEFT JOIN CompanyInfo C2
           ON C2.numCompanyID = D2.numCompanyID
         LEFT JOIN (SELECT TOP 1 OM.vcPoppName,
                                 OM.numOppID,
                                 numChildOppID,
                                 vcSource,
                                 numParentProjectID,
                                 vcProjectName
                    FROM   OpportunityLinking
                          LEFT JOIN OpportunityMaster OM
                             ON numOppID = numParentOppID
                          LEFT JOIN [ProjectsMaster]
							 ON numParentProjectID = numProId   
                    WHERE  numChildOppID = @numOppID) Link
           ON Link.numChildOppID = Opp.numOppID
         LEFT OUTER JOIN [Currency] C
					ON C.numCurrencyID = Opp.numCurrencyID
		 LEFT OUTER JOIN [OpportunityRecurring] OPR
					ON OPR.numOppId = Opp.numOppId
		 LEFT OUTER JOIN additionalContactsinformation ADC ON
		 ADC.numdivisionId = D2.numdivisionId AND  ADC.numContactId = Opp.numContactId	
		 LEFT JOIN RecurrenceConfiguration ON Opp.numOppId = RecurrenceConfiguration.numOppID AND RecurrenceConfiguration.numType = 1
		 LEFT JOIN RecurrenceTransaction ON Opp.numOppId = RecurrenceTransaction.numRecurrOppID
		 LEFT JOIN [dbo].[ShippingReport] AS SR ON SR.[numOppID] = [Opp].[numOppId] AND SR.[numDomainID] = [Opp].[numDomainId]
		LEFT JOIN [dbo].CustomerPartNumber CPN ON C3.numCompanyID = CPN.numCompanyID
		WHERE  Opp.numOppId = @numOppID
         AND Opp.numDomainID = @numDomainID
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppdetailsdtlpl')
DROP PROCEDURE usp_oppdetailsdtlpl
GO
CREATE PROCEDURE [dbo].[USP_OPPDetailsDTLPL]
(
               @OpportunityId        AS NUMERIC(9)  = NULL,
               @numDomainID          AS NUMERIC(9),
               @ClientTimeZoneOffset AS INT)
AS
  SELECT Opp.numoppid,numCampainID,
		 CAMP.vcCampaignName AS [numCampainIDName],
         dbo.Fn_getcontactname(Opp.numContactId) numContactIdName,
         isnull(ADC.vcEmail,'') AS vcEmail,
         isnull(ADC.numPhone,'') AS Phone,
         isnull(ADC.numPhoneExtension,'') AS PhoneExtension,
		 Opp.numContactId,
         Opp.txtComments,
         Opp.numDivisionID,
         tintOppType,
         Dateadd(MINUTE,-@ClientTimeZoneOffset,bintAccountClosingDate) AS bintAccountClosingDate,
         dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID) AS tintSourceName,
          tintSource,
         Opp.VcPoppName,
         intpEstimatedCloseDate,
         [dbo].[getdealamount](@OpportunityId,Getutcdate(),0) AS CalAmount,
         --monPAmount,
         CONVERT(DECIMAL(10,2),Isnull(monPAmount,0)) AS monPAmount,
		lngPConclAnalysis,
         (SELECT vcdata
          FROM   listdetails
          WHERE  numlistitemid = lngPConclAnalysis) AS lngPConclAnalysisName,
         (SELECT vcdata
          FROM   listdetails
          WHERE  numlistitemid = numSalesOrPurType) AS numSalesOrPurTypeName,
		  numSalesOrPurType,
		 Opp.vcOppRefOrderNo,
		 Opp.vcMarketplaceOrderID,
         C2.vcCompanyName,
         D2.tintCRMType,
         Opp.tintActive,
         dbo.Fn_getcontactname(Opp.numCreatedby)
           + ' '
           + CONVERT(VARCHAR(20),Dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintCreatedDate)) AS CreatedBy,
         dbo.Fn_getcontactname(Opp.numModifiedBy)
           + ' '
           + CONVERT(VARCHAR(20),Dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintModifiedDate)) AS ModifiedBy,
         dbo.Fn_getcontactname(Opp.numRecOwner) AS RecordOwner,
         Opp.numRecOwner,
         Isnull(tintshipped,0) AS tintshipped,
         Isnull(tintOppStatus,0) AS tintOppStatus,
         [dbo].[GetOppStatus](Opp.numOppId) AS vcDealStatus,
         dbo.Opportunitylinkeditems(@OpportunityId) +
         (SELECT COUNT(*) FROM  dbo.Communication Comm JOIN Correspondence Corr ON Comm.numCommId=Corr.numCommID
			WHERE Comm.numDomainId=Opp.numDomainId AND Corr.numDomainID=Comm.numDomainID AND Corr.tintCorrType IN (1,2,3,4) AND Corr.numOpenRecordID=Opp.numOppId) AS NoOfProjects,
         (SELECT A.vcFirstName
                   + ' '
                   + A.vcLastName
          FROM   AdditionalContactsInformation A
          WHERE  A.numcontactid = opp.numAssignedTo) AS numAssignedToName,
		  opp.numAssignedTo,
         Opp.numAssignedBy,
         (SELECT COUNT(* )
          FROM   GenericDocuments
          WHERE  numRecID = @OpportunityId
                 AND vcDocumentSection = 'O') AS DocumentCount,
         CASE 
           WHEN tintOppStatus = 1 THEN 100
           ELSE isnull((SELECT SUM(tintPercentage)
                 FROM   OpportunityStageDetails
                 WHERE  numOppId = @OpportunityId
                        AND bitStageCompleted = 1),0)
         END AS TProgress,
         (SELECT TOP 1 Isnull(varRecurringTemplateName,'')
          FROM   RecurringTemplate
          WHERE  numRecurringId = OPR.numRecurringId) AS numRecurringIdName,
		 OPR.numRecurringId,
         Link.numOppID AS numParentOppID,
         Link.vcPoppName AS ParentOppName,
         Link.vcSource AS Source,
         ISNULL(C.varCurrSymbol,'') varCurrSymbol,
          ISNULL(Opp.fltExchangeRate,0) AS [fltExchangeRate],
           ISNULL(C.chrCurrency,'') AS [chrCurrency],
         ISNULL(C.numCurrencyID,0) AS [numCurrencyID],
         (SELECT COUNT(*) FROM [OpportunityLinking] OL WHERE OL.[numParentOppID] = Opp.numOppId) AS ChildCount,
         (SELECT COUNT(*) FROM [RecurringTransactionReport] RTR WHERE RTR.[numRecTranSeedId] = Opp.numOppId)  AS RecurringChildCount,
         (SELECT COUNT(*) FROM [ShippingReport] WHERE numOppBizDocId IN (SELECT numOppBizDocsId FROM   OpportunityBizDocs BD WHERE  BD.numOppId = @OpportunityId))AS ShippingReportCount,
         ISNULL(Opp.numStatus ,0) numStatus,
         (SELECT vcdata
          FROM   listdetails
          WHERE  numlistitemid = Opp.numStatus) AS numStatusName,
          (SELECT SUBSTRING(
	(SELECT ','+  A.vcFirstName+' ' +A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN '(' + dbo.fn_GetListItemName(SR.numContactType) + ')' ELSE '' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
				AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('')),2,200000)) AS numShareWith,
		ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = @OpportunityId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) AS monDealAmount,
		ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = @OpportunityId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) AS monAmountPaid,
		ISNULL(Opp.tintSourceType,0) AS tintSourceType,
		ISNULL(Opp.tintTaxOperator,0) AS tintTaxOperator,  
             isnull(Opp.intBillingDays,0) AS numBillingDays,isnull(Opp.bitInterestType,0) AS bitInterestType,
             isnull(opp.fltInterest,0) AS fltInterest,  isnull(Opp.fltDiscount,0) AS fltDiscount,
             isnull(Opp.bitDiscountType,0) AS bitDiscountType,ISNULL(Opp.bitBillingTerms,0) AS bitBillingTerms,
             ISNULL(OPP.numDiscountAcntType,0) AS numDiscountAcntType,
              dbo.fn_getOPPAddress(Opp.numOppId,@numDomainID,1) AS BillingAddress,
            dbo.fn_getOPPAddress(Opp.numOppId,@numDomainID,2) AS ShippingAddress,

            -- ISNULL((SELECT SUBSTRING((SELECT '$^$' + ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=Opp.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OBD.numShipVia  AND vcFieldName='Tracking URL')),'') +'#^#'+ RTRIM(LTRIM(vcTrackingNo) )  FROM dbo.OpportunityBizDocs OBD WHERE OBD.numOppId=Opp.numOppID FOR XML PATH('')),4,200000)),'') AS  vcTrackingNo,
			 (SELECT  
				SUBSTRING(
							(SELECT '$^$' + 
							(SELECT vcShipFieldValue FROM dbo.ShippingFieldValues 
								WHERE numDomainID=Opp.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OBD.numShipVia  AND vcFieldName='Tracking URL')
							) +'#^#'+ RTRIM(LTRIM(vcTrackingNo))						
							FROM dbo.OpportunityBizDocs OBD WHERE OBD.numOppId= Opp.numOppID and ISNULL(OBD.numShipVia,0) <> 0
				FOR XML PATH('')),4,200000)
						
			 ) AS  vcTrackingNo,

            ISNULL(numPercentageComplete,0) numPercentageComplete,
            dbo.GetListIemName(ISNULL(numPercentageComplete,0)) AS PercentageCompleteName,
            ISNULL(numBusinessProcessID,0) AS [numBusinessProcessID],
            CASE WHEN opp.tintOppType=1 THEN dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID) ELSE '' END AS vcInventoryStatus
            ,ISNULL(bitUseShippersAccountNo,0) [bitUseShippersAccountNo], ISNULL(D2.vcShippersAccountNo,'') [vcShippersAccountNo] ,
			(SELECT SUM((ISNULL(monTotAmtBefDiscount, ISNULL(monTotAmount,0))- ISNULL(monTotAmount,0))) FROM OpportunityBizDocItems WHERE numOppBizDocID IN (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppId = @OpportunityId AND bitAuthoritativeBizDocs = 1)) AS numTotalDiscount,
			ISNULL(Opp.vcRecurrenceType,'') AS vcRecurrenceType,
			ISNULL(Opp.bitRecurred,0) AS bitRecurred,
			(
				CASE 
				WHEN opp.tintOppType=1 AND opp.tintOppStatus = 1 
				THEN  
					dbo.GetOrderShipReceiveStatus(opp.numOppId,1,opp.tintShipped)
				ELSE 
						'' 
				END
			) AS vcOrderedShipped,
			(
				CASE 
				WHEN opp.tintOppType = 2 AND opp.tintOppStatus = 1 
				THEN  
					dbo.GetOrderShipReceiveStatus(opp.numOppId,2,opp.tintShipped)
				ELSE 
						'' 
				END
			) AS vcOrderedReceived,
			ISNULL(C2.numCompanyType,0) AS numCompanyType,
		ISNULL(C2.vcProfile,0) AS vcProfile,
		ISNULL(Opp.numAccountClass,0) AS numAccountClass,D3.vcPartnerCode+'-'+C3.vcCompanyName as numPartner,
		ISNULL(Opp.numPartner,0) AS numPartnerId,
		ISNULL(Opp.bitIsInitalSalesOrder,0) AS bitIsInitalSalesOrder,
		ISNULL(Opp.dtReleaseDate,'') AS dtReleaseDate,
		ISNULL(Opp.numPartenerContact,0) AS numPartenerContactId,
		A.vcGivenName AS numPartenerContact,
		Opp.numReleaseStatus,
		(CASE Opp.numReleaseStatus WHEN 1 THEN 'Open' WHEN 2 THEN 'Purchased' ELSE '' END) numReleaseStatusName,
		ISNULL(Opp.intUsedShippingCompany,0) intUsedShippingCompany,
		CASE WHEN ISNULL(Opp.intUsedShippingCompany,0) = 0 THEN '' ELSE ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 82 AND numListItemID=ISNULL(Opp.intUsedShippingCompany,0)),'') END AS vcShippingCompany,
		ISNULL(Opp.numShipmentMethod,0) numShipmentMethod
		,CASE WHEN ISNULL(Opp.numShipmentMethod,0) = 0 THEN '' ELSE ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 338 AND numListItemID=ISNULL(Opp.numShipmentMethod,0)),'') END AS vcShipmentMethod
		,ISNULL(Opp.vcCustomerPO#,'') vcCustomerPO#,
		CONCAT(CASE tintEDIStatus 
			--WHEN 1 THEN '850 Received'
			WHEN 2 THEN '850 Acknowledged'
			WHEN 3 THEN '940 Sent'
			WHEN 4 THEN '940 Acknowledged'
			WHEN 5 THEN '856 Received'
			WHEN 6 THEN '856 Acknowledged'
			WHEN 7 THEN '856 Sent'
			WHEN 8 THEN 'Send 940 Failed'
			WHEN 9 THEN 'Send 856 & 810 Failed'
			WHEN 11 THEN '850 Partially Created'
			WHEN 12 THEN '850 SO Created'
			ELSE '' 
		END,'  ','<a onclick="return Open3PLEDIHistory(',Opp.numOppID,')"><img src="../images/GLReport.png" border="0"></a>') tintEDIStatusName,
		ISNULL(Opp.numShippingService,0) numShippingService,
		ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = Opp.numShippingService),'') AS vcShippingService,
		CASE 
			WHEN vcSignatureType = 0 THEN 'Service Default'
			WHEN vcSignatureType = 1 THEN 'Adult Signature Required'
			WHEN vcSignatureType = 2 THEN 'Direct Signature'
			WHEN vcSignatureType = 3 THEN 'InDirect Signature'
			WHEN vcSignatureType = 4 THEN 'No Signature Required'
			ELSE ''
		END AS vcSignatureType
		,dbo.fn_getOPPState(Opp.numOppId,Opp.numDomainID,2) vcShipState
  FROM   OpportunityMaster Opp
         JOIN divisionMaster D2 ON Opp.numDivisionID = D2.numDivisionID
         JOIN CompanyInfo C2 ON C2.numCompanyID = D2.numCompanyID
		 LEFT JOIN DivisionMasterShippingConfiguration ON D2.numDivisionID=DivisionMasterShippingConfiguration.numDivisionID
		 LEFT JOIN divisionMaster D3 ON Opp.numPartner = D3.numDivisionID
		 LEFT JOIN AdditionalContactsInformation A ON Opp.numPartenerContact = A.numContactId
		 LEFT JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId
         LEFT JOIN CompanyInfo C3 ON C3.numCompanyID = D3.numCompanyID
         LEFT JOIN (SELECT TOP 1 vcPoppName,
                                 numOppID,
                                 numChildOppID,
                                 vcSource,numParentOppID,OpportunityMaster.numDivisionID AS numParentDivisionID 
                    FROM   OpportunityLinking
                          left JOIN OpportunityMaster
                             ON numOppID = numParentOppID
                    WHERE  numChildOppID = @OpportunityId) Link
           ON Link.numChildOppID = Opp.numOppID
           LEFT OUTER JOIN [Currency] C
					ON C.numCurrencyID = Opp.numCurrencyID
			LEFT OUTER JOIN [OpportunityRecurring] OPR
					ON OPR.numOppId = Opp.numOppId
					 LEFT JOIN AddressDetails AD --Billing add
				ON AD.numDomainID=D2.numDomainID AND AD.numRecordID=D2.numDivisionID AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1
			 LEFT JOIN AddressDetails AD2--Shipping address
				ON AD2.numDomainID=D2.numDomainID AND AD2.numRecordID=D2.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary=1	
		LEFT JOIN dbo.CampaignMaster CAMP ON CAMP.numCampaignID = Opp.numCampainID 
  WHERE  Opp.numOppId = @OpportunityId
         AND Opp.numDomainID = @numDomainID
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_ActionItemPreBuildReport')
DROP PROCEDURE USP_ReportListMaster_ActionItemPreBuildReport
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_ActionItemPreBuildReport]
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@vcTimeLine VARCHAR(50)
	,@vcTeritorry VARCHAR(MAX)
	,@vcFilterBy TINYINT  -- 1:Assign TO, 2: Record Owner, 3: Teams (Based on Assigned To) 
	,@vcFilterValue VARCHAR(MAX)
	,@bitTask VARCHAR(MAX)
AS
BEGIN 
	DECLARE @TEMP TABLE
	(
		ID NUMERIC(18,0)
		,vcOrganizationName VARCHAR(300)
		,vcComments NVARCHAR(MAX)
		,vcType VARCHAR(50)
		,dtDate DATETIME
		,tintType TINYINT
	)

	INSERT INTO @TEMP
	(
		ID
		,vcOrganizationName
		,vcComments
		,vcType
		,dtDate
		,tintType
	)
	SELECT
		Communication.numCommId
		,CompanyInfo.vcCompanyName
		,ISNULL(Communication.textDetails,'')
		,ISNULL(LD.vcData,'')
		,DATEADD(MINUTE,-@ClientTimeZoneOffset,dtStartTime)
		,1
	FROM
		Communication                               
	INNER JOIN 
		AdditionalContactsInformation
	ON 
		Communication.numContactId = AdditionalContactsInformation.numContactId                                  
	INNER JOIN 
		DivisionMaster                                                                         
	ON 
		AdditionalContactsInformation.numDivisionId = DivisionMaster.numDivisionID                                   
	INNER JOIN 
		CompanyInfo                                                                         
	ON 
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	LEFT JOIN
		ListDetails LD
	ON
		LD.numListID = 73
		AND (LD.numDomainID=@numDomainID OR LD.constFlag = 1)
		AND LD.numListItemID = Communication.bitTask
	WHERE
		Communication.numDomainID = @numDomainID
		AND AdditionalContactsInformation.numDomainID=@numDomainID
		AND DivisionMaster.numDomainID=@numDomainID
		AND CompanyInfo.numDomainID=@numDomainID
		AND Communication.bitclosedflag=0                                                
		AND Communication.bitTask <> 973
		AND CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtEndTime) AS DATE) = CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()) AS DATE)
		AND 1= (CASE WHEN ISNULL(@numUserCntID,0) > 0 THEN (CASE WHEN (Communication.numAssign  = @numUserCntID  OR Communication.numCreatedBy = @numUserCntID) THEN 1 ELSE 0 END) ELSE 1 END)


	INSERT INTO @TEMP
	(
		ID
		,vcOrganizationName
		,vcComments
		,vcType
		,dtDate
		,tintType
	)
	SELECT
		Activity.Activityid
		,Comp.vcCompanyName
		,ISNULL(Activity.ActivityDescription,'')
		,'Calendar'
		,DATEADD(MINUTE,-@ClientTimeZoneOffset,startdatetimeutc)
		,2
	FROM 
		Activity 
	INNER JOIN             
		ActivityResource 
	ON 
		Activity.ActivityID=ActivityResource.ActivityID
	INNER JOIN 
		[Resource] Res 
	ON 
		ActivityResource.ResourceID=Res.ResourceID                                   
	INNER JOIN 
		AdditionalContactsInformation ACI 
	ON 
		ACI.numContactId = Res.numUserCntId                
	INNER JOIN 
		DivisionMaster Div                                                          
	ON 
		ACI.numDivisionId = Div.numDivisionID   
	INNER JOIN
		OpportunityMaster
	ON
		OpportunityMaster.numDivisionId = div.numDivisionID 
	INNER JOIN
		Communication
	ON
		Communication.numDivisionId = div.numDivisionID                                
	INNER JOIN 
		CompanyInfo Comp                             
	ON 
		Div.numCompanyID = Comp.numCompanyId
	WHERE
		OpportunityMaster.numDomainId=@numDomainID
		AND Res.numDomainId =@numDomainID
		AND 1 = (CASE WHEN ISNULL(@numUserCntID,0) > 0 THEN (CASE WHEN Res.numUserCntId = @numUserCntID THEN 1 ELSE 0 END) ELSE 1 END)
		AND ACI.numDomainID=@numDomainID
		AND Div.numDomainID=@numDomainID
		AND Comp.numDomainID=@numDomainID
		AND CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,startdatetimeutc) AS DATE) = CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()) AS DATE)
		--AND DATEADD(MINUTE,-@ClientTimeZoneOffset,startdatetimeutc) >= DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()) AND DATEADD(MINUTE,-@ClientTimeZoneOffset,startdatetimeutc) < CAST(CONVERT(CHAR(8), GETUTCDATE(), 112) + ' 00:00:00.00' AS DATETIME)
		AND [status] <> -1                                                     
		AND Activity.activityid NOT IN (SELECT DISTINCT(numActivityId) FROM Communication WHERE numDomainID=@numDomainID AND 1 = (CASE WHEN ISNULL(@numUserCntID,0) > 0 THEN (CASE WHEN (numAssign =@numUserCntID OR numCreatedBy=@numUserCntID) THEN 1 ELSE 0 END) ELSE 1 END))
		AND 1 = (CASE WHEN LEN(ISNULL(@vcTeritorry,'')) > 0 THEN (CASE WHEN Div.numTerID IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END) ELSE 1 END)
		AND 1 = (CASE 
					WHEN LEN(ISNULL(@vcFilterValue,0)) > 0 AND @vcFilterBy IN (1,2,3)
					THEN 
						(CASE @vcFilterBy
							WHEN 1  -- Assign To
							THEN (CASE WHEN OpportunityMaster.numAssignedTo IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)
							WHEN 2  -- Record Owner
							THEN (CASE WHEN OpportunityMaster.numRecOwner IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)
							WHEN 3  -- Teams (Based on Assigned To)
							THEN (CASE WHEN OpportunityMaster.numAssignedTo IN (SELECT numUserCntID FROM UserTeams WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END)
						END)
					ELSE 1 
				END)
		AND 1 = (CASE WHEN LEN(ISNULL(@bitTask,'')) > 0 THEN (CASE WHEN Communication.bitTask IN (SELECT Id FROM dbo.SplitIDs(@bitTask,',')) THEN 1 ELSE 0 END) ELSE 1 END)
		

	--INSERT INTO @TEMP
	--(
	--	ID
	--	,vcOrganizationName
	--	,vcComments
	--	,vcType
	--	,dtDate
	--	,tintType
	--)
	--SELECT
	--	(CASE BAD.btDocType WHEN 2 THEN BAD.numOppBizDocsId WHEN 3 THEN BAD.numOppBizDocsId ELSE BDA.numBizActionID END)
	--	,CI.vcCompanyName
	--	,ISNULL(BAD.vcComment,'')
	--	,(CASE BAD.btDocType WHEN 2  THEN  'Document Approval Request' ELSE 'BizDoc Approval Request' END)
	--	,DATEADD(MINUTE,-@ClientTimeZoneOffset,dtCreatedDate)
	--	,2
	--FROM
	--	BizDocAction BDA 
	--INNER JOIN
	--	AdditionalContactsInformation ACI
	--ON
	--	BDA.numContactId = ACI.numContactId
	--INNER JOIN
	--	DivisionMaster DM
	--ON
	--	ACI.numDivisionId = DM.numDivisionID
	--INNER JOIN
	--	CompanyInfo CI
	--ON
	--	DM.numCompanyID = CI.numCompanyId
	--LEFT JOIN 
	--	dbo.BizActionDetails BAD
	--ON 
	--	BDA.numBizActionId = BAD.numBizActionId
	--WHERE
	--	BDA.numDomainId =@numDomainID
	--	AND 1 = (CASE WHEN ISNULL(@numUserCntID,0) > 0 THEN (CASE WHEN BDA.numContactId = @numUserCntID THEN 1 ELSE 0 END) ELSE 1 END)
	--	AND ACI.numDomainID=@numDomainID
	--	AND DM.numDomainID=@numDomainID
	--	AND CI.numDomainID=@numDomainID
	--	AND BDA.numStatus=0 
	--	AND CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtCreatedDate) AS DATE) = CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()) AS DATE)
	
	SELECT *,dbo.FormatedDateTimeFromDate(dtDate,@numDOmainID) AS dtFormatted FROM @TEMP ORDER BY dtDate ASC
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_EmployeeBenefitToCompany')
DROP PROCEDURE USP_ReportListMaster_EmployeeBenefitToCompany
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_EmployeeBenefitToCompany]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@vcTimeLine VARCHAR(50)
	,@vcFilterBy TINYINT  -- 1:Assign TO, 2: Record Owner, 3: Teams (Based on Assigned To), 4: Employees
	,@vcFilterValue VARCHAR(MAX)
AS
BEGIN 
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @ProfitCost INT
	SELECT @numShippingItemID = numShippingServiceItemID,@numDiscountItemID=numDiscountServiceItemID,@ProfitCost=numCost FROM Domain WHERE numDomainId=@numDomainID

	DECLARE @dtStartDate DATETIME --= DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(MONTH,-12,GETUTCDATE()))
	DECLARE @dtEndDate AS DATETIME = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())

	IF @vcTimeLine = 'Last12Months'
	BEGIN
		SET @dtStartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, @dtEndDate), 0)
	END
	IF @vcTimeLine = 'Last6Months'
	BEGIN
		SET @dtStartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, @dtEndDate), 0)
	END
	IF @vcTimeLine = 'Last3Months'
	BEGIN
		SET @dtStartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, @dtEndDate), 0)
	END
	IF @vcTimeLine = 'Last30Days'
	BEGIN
		SET @dtStartDate = DATEADD(DAY, DATEDIFF(DAY, -30, @dtEndDate), 0)
	END
	IF @vcTimeLine = 'Last7Days'
	BEGIN
		SET @dtStartDate = DATEADD(DAY, DATEDIFF(DAY, -7, @dtEndDate), 0)
	END
	
	-- FETCH ALLREADY PAID COMMISSION DATA AND STORE IN TEMPORARY TABLE
	SELECT * INTO #tempPayrollTracking FROM PayrollTracking WHERE numDomainId=@numDomainId

	DECLARE @TempCommissionPaidCreditMemoOrRefund TABLE
	(
		numUserCntID NUMERIC(18,0),
		numReturnHeaderID NUMERIC(18,0),
		tintReturnType TINYINT,
		numReturnItemID NUMERIC(18,0),
		monCommission DECIMAL(20,5),
		numComRuleID NUMERIC(18,0),
		tintComType TINYINT,
		tintComBasedOn TINYINT,
		decCommission FLOAT,
		tintAssignTo TINYINT
	)    

	INSERT INTO 
		@TempCommissionPaidCreditMemoOrRefund 
	SELECT 
		numUserCntID,
		numReturnHeaderID,
		tintReturnType,
		numReturnItemID,
		monCommission,
		numComRuleID,
		tintComType,
		tintComBasedOn,
		decCommission,
		tintAssignTo
	FROM 
		dbo.GetCommissionPaidCreditMemoOrRefund(@numDomainId,@ClientTimeZoneOffset,@dtStartDate,@dtEndDate)

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numUserCntID NUMERIC(18,0)
		,vcUserName VARCHAR(300)
		,monHourlyRate DECIMAL(20,5)
		,TotalHrsWorked FLOAT
		,TotalPayroll DECIMAL(20,5)
		,ProfitMinusPayroll DECIMAL(20,5)
	)

	INSERT INTO @TEMP
	(
		numUserCntID
		,vcUserName
		,monHourlyRate
	)
	SELECT 
		UM.numUserDetailId
		,CONCAT(ADC.vcfirstname,' ',adc.vclastname) vcUserName
		,ISNULL(UM.monHourlyRate,0)
	FROM  
		dbo.UserMaster UM 
	JOIN 
		dbo.AdditionalContactsInformation adc 
	ON 
		adc.numDomainID=@numDomainId 
		AND adc.numcontactid = um.numuserdetailid
	JOIN
		userteams UT
	ON 
		UT.numDomainID=@numDomainId 
		AND UM.numUserDetailId=UT.numUserCntID
	WHERE 
		UM.numDomainId=@numDomainId
		AND UM.bitActivateFlag = 1
		AND 1 = (CASE WHEN @vcFilterBy = 4  THEN (CASE WHEN UT.numUserCntID IN (SELECT Id FROM dbo.SplitIDs((@vcFilterValue),',')) THEN 1 ELSE 0 END) 
					ELSE 
				(CASE WHEN LEN(ISNULL(@vcFilterValue,'')) > 0 THEN (CASE WHEN UT.numTeam IN (SELECT Id FROM dbo.SplitIDs((@vcFilterValue),',')) THEN 1 ELSE 0 END) ELSE 1 END)
					END)
		


	DECLARE @i INT = 1
	DECLARE @COUNT INT = 0
	DECLARE @numUserCntID NUMERIC(18,0)
	DECLARE @decTotalHrsWorked as decimal(10,2)
	DECLARE @decTotalPaidLeaveHrs AS DECIMAL(10,2)
	DECLARE @monExpense DECIMAL(20,5)
	DECLARE @monReimburse DECIMAL(20,5)
	DECLARE @monCommPaidInvoice DECIMAL(20,5)
	DECLARE @monCommPaidInvoiceDepositedToBank DECIMAL(20,5)
	DECLARE @monCommUNPaidInvoice DECIMAL(20,5)
	DECLARE @monCommPaidCreditMemoOrRefund DECIMAL(20,5)
	DECLARE @monCommUnPaidCreditMemoOrRefund DECIMAL(20,5)
	DECLARE @GrossProfit DECIMAL(20,5)
    
	SELECT @COUNT=COUNT(*) FROM @TEMP

	-- LOOP OVER EACH EMPLOYEE
	WHILE @i <= @COUNT
	BEGIN
		SELECT @decTotalHrsWorked=0,@decTotalPaidLeaveHrs=0,@monExpense=0,@monReimburse=0,@monCommPaidInvoice=0,@monCommPaidInvoiceDepositedToBank=0,@monCommUNPaidInvoice=0, @numUserCntID=0, @GrossProfit=0

		SELECT @numUserCntID = numUserCntID FROM @TEMP WHERE ID=@i

		SELECT 
			@decTotalHrsWorked=sum(x.Hrs) 
		FROM 
		(
			SELECT  
				ISNULL(SUM(CAST(DATEDIFF(MINUTE,dtfromdate,dttodate) AS FLOAT)/CAST(60 AS FLOAT)),0) AS Hrs            
			FROM 
				TimeAndExpense 
			WHERE 
				(numType=1 OR numType=2) 
				AND numCategory=1                 
				AND (
						(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtFromDate) BETWEEN @dtStartDate AND @dtEndDate) 
						OR 
						(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtToDate) BETWEEN @dtStartDate AND @dtEndDate)
					) 
				AND numUserCntID=@numUserCntID  
				AND numDomainID=@numDomainId
				AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE ISNULL(numCategoryHDRID,0)>0)
		) x

		-- CALCULATE PAID LEAVE HRS
		SELECT 
			@decTotalPaidLeaveHrs=ISNULL(
											SUM( 
													CASE 
													WHEN dtfromdate = dttodate 
													THEN 
														24 - (CASE WHEN bitfromfullday = 0 THEN 12 ELSE 0 END)
													ELSE 
														(Datediff(DAY,dtfromdate,dttodate) + 1) * 24 - (CASE WHEN bitfromfullday = 0 THEN 12 ELSE 0 END) - (CASE WHEN bittofullday = 0 THEN 12 ELSE 0 END)
														END
												)
											,0)
		FROM   
			TimeAndExpense
		WHERE  
			numtype = 3 
			AND numcategory = 3 
			AND numusercntid = @numUserCntID 
			AND numdomainid = @numDomainId 
			AND (
					(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtFromDate) BETWEEN @dtStartDate AND @dtEndDate)
					Or 
					(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtToDate) BETWEEN @dtStartDate AND @dtEndDate)
				)
			AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE ISNULL(numCategoryHDRID,0)>0)		

		-- CALCULATE EXPENSES
		SELECT 
			@monExpense=ISNULL((SUM(CAST(monAmount AS FLOAT))),0)    
		FROM 
			TimeAndExpense 
		WHERE 
			numCategory=2 
			AND numType in (1,2) 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainId 
			AND ((dtFromDate BETWEEN @dtStartDate And @dtEndDate) Or (dtToDate BETWEEN @dtStartDate And @dtEndDate)) 
			AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE ISNULL(numCategoryHDRID,0)>0)		

		-- CALCULATE REIMBURSABLE EXPENSES
		SELECT 
			@monReimburse=Isnull((SUM(CAST(monamount AS FLOAT))),0)
		FROM   
			TimeAndExpense 
		WHERE  
			bitreimburse = 1 
			AND numcategory = 2 
			AND numusercntid = @numUserCntID 
			AND numdomainid = @numDomainId
			AND dtfromdate BETWEEN @dtStartDate AND @dtEndDate
			AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE ISNULL(numCategoryHDRID,0)>0)        

		-- CALCULATE COMMISSION PAID INVOICE
		SELECT 
			@monCommPaidInvoice=ISNULL(SUM(Amount),0) 
		FROM 
			(
				SELECT 
					oppBiz.numOppID,
					oppBiz.numOppBizDocsID,
					ISNULL(SUM(BC.numComissionAmount - ISNULL(PT.monCommissionAmt,0)),0) AS Amount
				FROM 
					OpportunityMaster Opp 
				INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
				INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId 
				LEFT JOIN #tempPayrollTracking PT ON PT.numComissionID=BC.numComissionID
				CROSS APPLY
				(
					SELECT 
						MAX(DM.dtDepositDate) dtDepositDate
					FROM 
						DepositMaster DM 
					JOIN dbo.DepositeDetails DD	ON DM.numDepositId=DD.numDepositID 
					WHERE 
						DM.tintDepositePage = 2
						AND DM.numDomainId=@numDomainId
						AND DD.numOppID=oppBiz.numOppID 
						AND DD.numOppBizDocsID =oppBiz.numOppBizDocsID
				) TEMPDeposit
				WHERE 
					Opp.numDomainId=@numDomainId 
					AND (BC.numUserCntId=@numUserCntID AND ISNULL(BC.tintAssignTo,0) <> 3)
					AND BC.bitCommisionPaid=0 
					AND oppBiz.bitAuthoritativeBizDocs = 1 
					AND isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount 
					AND CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,TEMPDeposit.dtDepositDate) AS DATE) BETWEEN CAST(@dtStartDate AS DATE) AND CAST(@dtEndDate AS DATE)
				GROUP BY 
					oppBiz.numOppID,
					oppBiz.numOppBizDocsID
			) A

		-- CALCULATE COMMISSION UNPAID INVOICE 
		SELECT 
			@monCommUNPaidInvoice=isnull(sum(BC.numComissionAmount - ISNULL(PT.monCommissionAmt,0)),0) 
		FROM 
			OpportunityMaster Opp 
		INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
		INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId
		LEFT JOIN #tempPayrollTracking PT ON PT.numComissionID=BC.numComissionID 
		WHERE 
			Opp.numDomainId=@numDomainId 
			AND (BC.numUserCntId=@numUserCntID AND ISNULL(BC.tintAssignTo,0) <> 3)
			AND BC.bitCommisionPaid=0 
			AND oppBiz.bitAuthoritativeBizDocs = 1 
			AND ISNULL(oppBiz.monAmountPaid,0) <oppBiz.monDealAmount
			AND ((DATEADD(MINUTE,-@ClientTimeZoneOffset,oppBiz.dtFromDate) BETWEEN @dtStartDate AND @dtEndDate))	

		SET @monCommPaidCreditMemoOrRefund = ISNULL((SELECT SUM(monCommission) FROM @TempCommissionPaidCreditMemoOrRefund WHERE (numUserCntId=@numUserCntID AND ISNULL(tintAssignTo,0) <> 3)),0)


		SET @GrossProfit = ISNULL((SELECT
										SUM(ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * (CASE @ProfitCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numPurchaseUnit) WHEN 2 THEN numCost * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numUOMId) ELSE monAverageCost END))) Profit
									FROM
										OpportunityItems OI
									INNER JOIN
										OpportunityMaster OM
									ON
										OI.numOppId = OM.numOppID
									INNER JOIN
										Item I
									ON
										OI.numItemCode = I.numItemCode
									Left JOIN 
										Vendor V 
									ON 
										V.numVendorID=I.numVendorID 
										AND V.numItemCode=I.numItemCode
									WHERE
										OM.numDomainId=@numDomainID
										AND OM.numAssignedTo = @numUserCntID
										AND ISNULL(OI.monTotAmount,0) <> 0
										AND ISNULL(OI.numUnitHour,0) <> 0
										AND ISNULL(OM.tintOppType,0)=1
										AND ISNULL(OM.tintOppStatus,0)=1
										AND DATEADD(MINUTE,-@ClientTimeZoneOffset,OM.bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate
										AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)),0)

		UPDATE 
			@TEMP 
		SET    
			TotalHrsWorked=@decTotalHrsWorked,
			TotalPayroll= @decTotalHrsWorked * monHourlyRate + @monReimburse + @monCommPaidInvoice + @monCommUNPaidInvoice - ISNULL(@monCommPaidCreditMemoOrRefund,0),
			ProfitMinusPayroll = @GrossProfit - (@decTotalHrsWorked * monHourlyRate + @monReimburse + @monCommPaidInvoice + @monCommUNPaidInvoice - ISNULL(@monCommPaidCreditMemoOrRefund,0))
		WHERE 
			numUserCntID=@numUserCntID 

		SET @i = @i + 1
	END

	SELECT vcUserName,TotalHrsWorked FROM @TEMP ORDER BY TotalHrsWorked DESC
	SELECT vcUserName,TotalPayroll FROM @TEMP ORDER BY TotalPayroll DESC
	SELECT vcUserName,ProfitMinusPayroll FROM @TEMP ORDER BY ProfitMinusPayroll DESC
	DROP TABLE #tempPayrollTracking
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_EmployeeSalesPerformance1')
DROP PROCEDURE USP_ReportListMaster_EmployeeSalesPerformance1
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_EmployeeSalesPerformance1]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@vcTimeLine VARCHAR(50)
	,@vcFilterBy TINYINT  -- 1:Assign TO, 2: Record Owner, 3: Teams (Based on Assigned To), 4: Employees
	,@vcFilterValue VARCHAR(MAX)
AS
BEGIN 

	DECLARE @dtCreatedDate DATETIME
	DECLARE @dtEndDate AS DATETIME = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())

	IF @vcTimeLine = 'Last12Months'
	BEGIN
		SET @dtCreatedDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, @dtEndDate), 0)
	END
	IF @vcTimeLine = 'Last6Months'
	BEGIN
		SET @dtCreatedDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, @dtEndDate), 0)
	END
	IF @vcTimeLine = 'Last3Months'
	BEGIN
		SET @dtCreatedDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, @dtEndDate), 0)
	END
	IF @vcTimeLine = 'Last30Days'
	BEGIN
		SET @dtCreatedDate = DATEADD(DAY, DATEDIFF(DAY, -30, @dtEndDate), 0)
	END
	IF @vcTimeLine = 'Last7Days'
	BEGIN
		SET @dtCreatedDate = DATEADD(DAY, DATEDIFF(DAY, -7, @dtEndDate), 0)
	END

	DECLARE @TempFinal TABLE
	(
		vcUsername VARCHAR(300)
		,LeadsToAccount FLOAT
		,LeadsToProspect FLOAT
		,ProspectToAccount FLOAT
	)

	DECLARE @TEMPEmployees TABLE
	(
		ID INT IDENTITY(1,1)
		,numAssignedTo NUMERIC(18,0)
	)

	INSERT INTO @TEMPEmployees
	(
		numAssignedTo
	)
	SELECT DISTINCT
		numAssignedTo
	FROM
		DivisionMaster
	WHERE
		numDomainID=@numDomainID
		AND bintCreatedDate >= @dtCreatedDate


	DECLARE @TEMP TABLE
	(
		numDivisionID NUMERIC(18,0)
		,numAssignedTo NUMERIC(18,0)
		,FirstRecordID NUMERIC(18,0)
		,LastRecordID NUMERIC(18,0)
		,tintFirstCRMType TINYINT
		,tintLastCRMType TINYINT
	)

	INSERT INTO @TEMP
	(
		numDivisionID
		,numAssignedTo
		,FirstRecordID
		,LastRecordID
	)
	SELECT 
		DivisionMasterPromotionHistory.numDivisionID
		,DM.numAssignedTo
		,MIN(ID)
		,MAX(ID)
	FROM 
		DivisionMasterPromotionHistory 
	INNER JOIN
		DivisionMaster DM
	ON
		DivisionMasterPromotionHistory.numDivisionID = DM.numDivisionID
	WHERE 
		DivisionMasterPromotionHistory.numDomainID=@numDomainID 
		AND DM.numDomainID = @numDomainID
		AND DM.bintCreatedDate >= @dtCreatedDate
	GROUP BY
		DivisionMasterPromotionHistory.numDivisionID
		,DM.numAssignedTo

	UPDATE
		T1
	SET
		tintFirstCRMType = DMPH1.tintPreviousCRMType
		,tintLastCRMType = DMPH2.tintNewCRMType
	FROM
		@TEMP T1
	INNER JOIN
		DivisionMasterPromotionHistory DMPH1
	ON
		T1.FirstRecordID = DMPH1.ID
	INNER JOIN
		DivisionMasterPromotionHistory DMPH2
	ON
		T1.LastRecordID = DMPH2.ID


	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT 
	DECLARE @TotalLeads FLOAT
	DECLARE @LeadsToProspect FLOAT
	DECLARE @LeadsToAccount FLOAT
	DECLARE @TotalProspects FLOAT
	DECLARE @ProspectToAccount FLOAT

	DECLARE @TempAssignedTo NUMERIC(18,0)
	SELECT @iCount=COUNT(*) FROM @TEMPEmployees

	WHILE @i <= @iCount
	BEGIN
		SELECT @TempAssignedTo=numAssignedTo FROM @TEMPEmployees WHERE ID=@i

		SET @TotalLeads = ISNULL((SELECT 
								COUNT(*) 
							FROM 
								DivisionMaster 
							WHERE 
								numDomainID=@numDomainID 
								AND numAssignedTo=@TempAssignedTo
								AND bintCreatedDate >= @dtCreatedDate
								AND tintCRMType=0 
								AND numDivisionID NOT IN (SELECT numDivisionID FROM @TEMP)),0) + ISNULL((SELECT COUNT(*) FROM @TEMP WHERE tintFirstCRMType=0 AND numAssignedTo=@TempAssignedTo),0)


		SET @LeadsToProspect = ISNULL((SELECT COUNT(*) FROM @TEMP WHERE tintFirstCRMType=0 ANd tintLastCRMType=1 AND numAssignedTo=@TempAssignedTo),0)
		SET @LeadsToAccount = ISNULL((SELECT COUNT(*) FROM @TEMP WHERE tintFirstCRMType=0 ANd tintLastCRMType=2 AND numAssignedTo=@TempAssignedTo),0)


		SET @TotalProspects = ISNULL((SELECT 
										COUNT(*) 
									FROM 
										DivisionMaster 
									WHERE 
										numDomainID=@numDomainID 
										AND numAssignedTo=@TempAssignedTo
										AND bintCreatedDate >= @dtCreatedDate 
										AND tintCRMType=1
										AND numDivisionID NOT IN (SELECT numDivisionID FROM @TEMP)),0) + ISNULL((SELECT COUNT(*) FROM @TEMP WHERE tintFirstCRMType=1 AND numAssignedTo=@TempAssignedTo),0)

		SET @ProspectToAccount = ISNULL((SELECT COUNT(*) FROM @TEMP WHERE tintFirstCRMType=1 ANd tintLastCRMType=2 AND numAssignedTo=@TempAssignedTo),0)
	

		INSERT INTO @TempFinal
		(
			vcUsername
			,LeadsToAccount
			,LeadsToProspect
			,ProspectToAccount
		)
		SELECT
			vcUserName
			,(@LeadsToAccount * 100) / (CASE WHEN ISNULL(@TotalLeads,0) = 0 THEN 1 ELSE @TotalLeads END)
			,(@LeadsToProspect * 100) / (CASE WHEN ISNULL(@TotalLeads,0) = 0 THEN 1 ELSE @TotalLeads END)
			,(@ProspectToAccount * 100) / (CASE WHEN ISNULL(@TotalProspects,0) = 0 THEN 1 ELSE @TotalProspects END)
		FROM
			UserMaster

		JOIN
			userteams UT
		ON 
			UT.numDomainID=@numDomainId 
			AND UserMaster.numUserDetailId=UT.numUserCntID

		WHERE 
			UserMaster.numDomainID=@numDomainID
			AND UserMaster.numUserDetailId=@TempAssignedTo
			AND 1 = (CASE WHEN @vcFilterBy = 4  THEN (CASE WHEN UT.numUserCntID IN (SELECT Id FROM dbo.SplitIDs((@vcFilterValue),',')) THEN 1 ELSE 0 END) 
					ELSE 
				(CASE WHEN LEN(ISNULL(@vcFilterValue,'')) > 0 THEN (CASE WHEN UT.numTeam IN (SELECT Id FROM dbo.SplitIDs((@vcFilterValue),',')) THEN 1 ELSE 0 END) ELSE 1 END)
					END)

		SET @i = @i + 1
	END

	SELECT vcUsername,LeadsToProspect FROM @TempFinal WHERE ISNULL(LeadsToProspect,0.0) > 0
	SELECT vcUsername,LeadsToAccount FROM @TempFinal WHERE ISNULL(LeadsToAccount,0.0) > 0
	SELECT vcUsername,ProspectToAccount FROM @TempFinal WHERE ISNULL(ProspectToAccount,0.0) > 0
END
GO




SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_EmployeeSalesPerformancePanel1')
DROP PROCEDURE USP_ReportListMaster_EmployeeSalesPerformancePanel1
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_EmployeeSalesPerformancePanel1]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@vcTimeLine VARCHAR(50)
	,@vcFilterBy TINYINT  -- 1:Assign TO, 2: Record Owner, 3: Teams (Based on Assigned To), 4: Employees
	,@vcFilterValue VARCHAR(MAX)
AS
BEGIN 
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @ProfitCost INT
	SELECT @numShippingItemID=numShippingServiceItemID,@numDiscountItemID=numDiscountServiceItemID,@ProfitCost=numCost FROM Domain WHERE numDomainId=@numDomainID

	DECLARE @TotalSalesReturn FLOAT = 0.0

	SELECT 
		UserMaster.vcUserName
		,(SELECT COUNT(*) FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numAssignedTo=TEMP1.numAssignedTo AND tintOppType=1 AND tintOppStatus=1 AND bintOppToOrder IS NOT NULL) * 100.0 / (SELECT COUNT(*) FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numAssignedTo=TEMP1.numAssignedTo AND tintOppType=1 AND ((tintOppStatus=1 AND bintOppToOrder IS NOT NULL) OR tintOppStatus=2)) * 1.0 WonPercent
		--,(SELECT COUNT(*) FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numAssignedTo=TEMP1.numAssignedTo AND tintOppType=1 AND tintOppStatus=2 ) * 100.0 / (SELECT COUNT(*) FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numAssignedTo=TEMP1.numAssignedTo AND tintOppType=1 AND ((tintOppStatus=1 AND bintOppToOrder IS NOT NULL) OR tintOppStatus=2)) * 1.0
	FROM
	(
		SELECT DISTINCT
			numAssignedTo
		FROM
			OpportunityMaster
		WHERE
			numDomainId=@numDomainID
			AND tintOppType=1
			AND ((tintOppStatus=1 AND bintOppToOrder IS NOT NULL) OR tintOppStatus=2)
	) TEMP1
	INNER JOIN
		UserMaster
	ON
		TEMP1.numAssignedTo = UserMaster.numUserDetailId
	WHERE
		UserMaster.numDomainID = @numDomainID


	SELECT
		UserMaster.vcUserName
		,Temp2.Profit
	FROM
	(
		SELECT DISTINCT
			numAssignedTo
		FROM
			OpportunityMaster
		WHERE
			numDomainId=@numDomainID
			AND tintOppType=1
			AND tintOppStatus = 1
	) TEMP1
	INNER JOIN
		UserMaster
	ON
		TEMP1.numAssignedTo = UserMaster.numUserDetailId
		AND UserMaster.numDomainID = @numDomainID
	CROSS APPLY
	(
		SELECT
			SUM(ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * (CASE @ProfitCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numPurchaseUnit) WHEN 2 THEN numCost * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numUOMId) ELSE monAverageCost END))) Profit
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.numAssignedTo = TEMP1.numAssignedTo
			AND ISNULL(OI.monTotAmount,0) <> 0
			AND ISNULL(OI.numUnitHour,0) <> 0
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP2
	ORDER BY
		 TEMP2.Profit DESC

	SELECT
		UserMaster.vcUserName
		,AVG(ProfitPercentByOrder) AS AvgGrossProfitMargin
	FROM
	(
		SELECT DISTINCT
			numAssignedTo
		FROM
			OpportunityMaster
		WHERE
			numDomainId=@numDomainID
			AND tintOppType=1
			AND tintOppStatus = 1
	) TEMP1
	INNER JOIN
		UserMaster
	ON
		TEMP1.numAssignedTo = UserMaster.numUserDetailId
		AND UserMaster.numDomainID = @numDomainID
	CROSS APPLY
	(
		SELECT
			OM.numOppID
			,(SUM(ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * (CASE @ProfitCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numPurchaseUnit) WHEN 2 THEN numCost * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numUOMId) ELSE monAverageCost END)))/SUM(ISNULL(monTotAmount,0))) * 100.0 ProfitPercentByOrder
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.numAssignedTo = TEMP1.numAssignedTo
			AND ISNULL(monTotAmount,0) <> 0
			AND ISNULL(numUnitHour,0) <> 0
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
		GROUP BY
			OM.numOppId
	) TEMP2
	GROUP BY
		UserMaster.vcUserName
	HAVING
		AVG(ProfitPercentByOrder) > 0
	ORDER BY
		AVG(ProfitPercentByOrder) DESC
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_RPELastMonthAndSamePeriodLastYear')
DROP PROCEDURE USP_ReportListMaster_RPELastMonthAndSamePeriodLastYear
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_RPELastMonthAndSamePeriodLastYear]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@vcFilterValue VARCHAR(MAX)
AS
BEGIN 
	DECLARE @ExpenseLastMonth DECIMAL(20,5)
	DECLARE @ExpenseSamePeriodLastYear DECIMAL(20,5)
	DECLARE @ProfitLastMonth DECIMAL(20,5)
	DECLARE @ProfitSamePeriodLastYear DECIMAL(20,5)
	DECLARE @RevenueLastMonth DECIMAL(20,5)
	DECLARE @RevenueSamePeriodLastYear DECIMAL(20,5)
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @ProfitCost INT
	DECLARE @dtLastMonthFromDate AS DATETIME                                       
	DECLARE @dtLastMonthToDate AS DATETIME
	DECLARE @dtSameMonthLYFromDate AS DATETIME                                       
	DECLARE @dtSameMonthLYToDate AS DATETIME

	DECLARE @FinancialYearStartDate AS DATETIME
	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainID)

	--SELECT @dtLastMonthFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)), @dtLastMonthToDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)));
	--SELECT @dtSameMonthLYFromDate = DATEADD(YEAR,-1,@dtLastMonthFromDate),@dtSameMonthLYToDate = DATEADD(YEAR,-1,@dtLastMonthToDate);

	SELECT @dtSameMonthLYFromDate = DATEADD(YEAR,-1,@dtLastMonthFromDate),@dtSameMonthLYToDate = DATEADD(YEAR,-1,@dtLastMonthToDate);
	DECLARE @StartDate AS DATETIME 
	SET @StartDate= DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)

	IF @vcFilterValue = 'ThisWeekVSLastWeek'
	BEGIN
		SET @dtLastMonthFromDate = DATEADD(WEEK,0,@StartDate)
		SET @dtLastMonthToDate= DATEADD(WEEK,-1,DATEADD(mm,0,@StartDate))
	END
	IF @vcFilterValue = 'ThisMonthVSLastMonth'
	BEGIN
		SET @dtLastMonthFromDate = DATEADD(MM,0,@StartDate)
		SET @dtLastMonthToDate= DATEADD(MM,-1,DATEADD(mm,0,@StartDate))
	END
	IF @vcFilterValue = 'ThisQTDVSLastQTD'
	BEGIN
		SET @dtLastMonthFromDate = DATEADD(QQ,0,@StartDate)
		SET @dtLastMonthToDate= DATEADD(QQ,-1,DATEADD(mm,0,@StartDate))
	END
	IF @vcFilterValue = 'ThisYTDVSLastYTD'
	BEGIN
		SET @dtLastMonthFromDate = DATEADD(YY,0,@StartDate)
		SET @dtLastMonthToDate= DATEADD(YY,-1,DATEADD(mm,0,@StartDate))
	END
	


	SELECT 
		@numShippingItemID=numShippingServiceItemID
		,@numDiscountItemID=numDiscountServiceItemID
		,@ProfitCost=numCost 
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID


	-- GET EXPENSE YTD VS SAME PERIOD LAST YEAR
	SELECT @ExpenseLastMonth = SUM(numDebitAmt) - SUM(numCreditAmt) FROM General_Journal_Details INNER JOIN General_Journal_Header ON General_Journal_Details.numJournalId=General_Journal_Header.numJournal_Id WHERE General_Journal_Details.numDomainId=@numDomainID AND datEntry_Date BETWEEN  @dtLastMonthFromDate AND @dtLastMonthToDate AND General_Journal_Details.numChartAcntId IN (SELECT numAccountId FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND bitActive = 1 AND numAcntTypeId IN (SELECT numAccountTypeID FROM AccountTypeDetail WHERE numDomainID=@numDomainID AND vcAccountCode LIKE '0104%'))
	SELECT @ExpenseSamePeriodLastYear = SUM(numDebitAmt) - SUM(numCreditAmt) FROM General_Journal_Details INNER JOIN General_Journal_Header ON General_Journal_Details.numJournalId=General_Journal_Header.numJournal_Id WHERE General_Journal_Details.numDomainId=@numDomainID AND datEntry_Date BETWEEN  @dtSameMonthLYFromDate AND @dtSameMonthLYToDate AND General_Journal_Details.numChartAcntId IN (SELECT numAccountId FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND bitActive = 1 AND numAcntTypeId IN (SELECT numAccountTypeID FROM AccountTypeDetail WHERE numDomainID=@numDomainID AND vcAccountCode LIKE '0104%'))

	-- GET PROFIT YTD VS SAME PERIOD LAST YEAR
	SELECT 
		@ProfitLastMonth = SUM(Profit)
	FROM
	(
		SELECT
			ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * (CASE @ProfitCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numPurchaseUnit) WHEN 2 THEN numCost * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numUOMId) ELSE monAverageCost END)) Profit
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.bintCreatedDate BETWEEN  @dtLastMonthFromDate AND @dtLastMonthToDate
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP

	SELECT 
		@ProfitSamePeriodLastYear = SUM(Profit)
	FROM
	(
		SELECT
			ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * (CASE @ProfitCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numPurchaseUnit) WHEN 2 THEN numCost * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numUOMId) ELSE monAverageCost END)) Profit
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.bintCreatedDate BETWEEN  @dtSameMonthLYFromDate AND @dtSameMonthLYToDate
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP

	-- GET REVENUE YTD VS SAME PERIOD LAST YEAR
	SELECT 
		@RevenueLastMonth = SUM(monTotAmount)
	FROM
	(
		SELECT
			ISNULL(monTotAmount,0) monTotAmount
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.bintCreatedDate BETWEEN  @dtLastMonthFromDate AND @dtLastMonthToDate
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
	) TEMP

	SELECT 
		@RevenueSamePeriodLastYear = SUM(monTotAmount)
	FROM
	(
		SELECT
			ISNULL(monTotAmount,0) monTotAmount
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.bintCreatedDate BETWEEN  @dtSameMonthLYFromDate AND @dtSameMonthLYToDate
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
	) TEMP

	SELECT
		100.0 * (ISNULL(@RevenueLastMonth,0) - ISNULL(@RevenueSamePeriodLastYear,0)) / CASE WHEN ISNULL(@RevenueSamePeriodLastYear,0) = 0 THEN 1 ELSE @RevenueSamePeriodLastYear END As RevenueDifference
		,100.0 * (ISNULL(@ProfitLastMonth,0) - ISNULL(@ProfitSamePeriodLastYear,0)) / CASE WHEN ISNULL(@ProfitSamePeriodLastYear,0) = 0 THEN 1 ELSE @ProfitSamePeriodLastYear END As ProfitDifference
		,100.0 * (ISNULL(@ExpenseLastMonth,0) - ISNULL(@ExpenseSamePeriodLastYear,0)) / CASE WHEN ISNULL(@ExpenseSamePeriodLastYear,0) = 0 THEN 1 ELSE @ExpenseSamePeriodLastYear END As ExpenseDifference
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_RunPrebuildReport')
DROP PROCEDURE USP_ReportListMaster_RunPrebuildReport
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_RunPrebuildReport]   
@numDomainID AS NUMERIC(18,0),               
@numUserCntID AS NUMERIC(18,0),
@ClientTimeZoneOffset AS INT,
@intDefaultReportID INT,
@numDashBoardID NUMERIC(18,0)
AS  
BEGIN
	DECLARE @vcTimeLine VARCHAR(50)
		,@vcGroupBy VARCHAR(50)
		,@vcTeritorry VARCHAR(MAX)
		,@vcFilterBy TINYINT
		,@vcFilterValue VARCHAR(MAX)
		,@dtFromDate DATE
		,@dtToDate DATE
		,@numRecordCount INT
		,@tintControlField INT
		,@vcDealAmount VARCHAR(MAX)
		,@tintOppType INT
		,@lngPConclAnalysis VARCHAR(MAX)
		,@vcTeams VARCHAR(MAX)
		,@vcEmploees VARCHAR(MAX)
		,@bitTask VARCHAR(MAX)
		,@tintTotalProgress INT
		,@tintMinNumber INT
		,@tintMaxNumber INT
		,@tintQtyToDisplay INT
		,@vcDueDate VARCHAR(MAX)

	IF @intDefaultReportID = 1 --1 - A/R
	BEGIN
		EXEC USP_ReportListMaster_ARPreBuildReport @numDomainID
	END
	ELSE IF @intDefaultReportID = 2 -- A/P
	BEGIN
		EXEC USP_ReportListMaster_APPreBuildReport @numDomainID
	END
	ELSE IF @intDefaultReportID = 3 -- MONEY IN THE BANK
	BEGIN
		EXEC USP_ReportListMaster_BankPreBuildReport @numDomainID
	END
	ELSE IF @intDefaultReportID = 5 -- TOP 10 ITEMS BY PROFIT AMOUNT (LAST 12 MONTHS)
	BEGIN

		SELECT
			@vcTimeLine=vcTimeLine
			,@vcGroupBy=vcGroupBy
			,@vcTeritorry=vcTeritorry
			,@vcFilterBy=vcFilterBy
			,@vcFilterValue=vcFilterValue
			,@dtFromDate=dtFromDate
			,@dtToDate=dtToDate
			,@numRecordCount = numRecordCount
			,@tintControlField = tintControlField
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID

		EXEC USP_ReportListMaster_Top10ItemByProfitPreBuildReport @numDomainID,@vcTimeLine,@vcGroupBy,@vcTeritorry,@vcFilterBy,@vcFilterValue,@dtFromDate,@dtToDate,@numRecordCount,@tintControlField
	
		--EXEC USP_ReportListMaster_Top10ItemByProfitPreBuildReport @numDomainID
	END
	ELSE IF @intDefaultReportID = 6 -- TOP 10 ITEMS BY REVENUE SOLD (LAST 12 MONTHS)
	BEGIN
		EXEC USP_ReportListMaster_Top10ItemByRevenuePreBuildReport @numDomainID
	END
	ELSE IF @intDefaultReportID = 7  -- PROFIT MARGIN BY ITEM CLASSIFICATION (LAST 12 MONTHS)
	BEGIN
		EXEC USP_ReportListMaster_ItemClassificationProfitPreBuildReport @numDomainID
	END
	ELSE IF @intDefaultReportID = 8  -- TOP 10 CUSTOMERS BY PROFIT MARGIN
	BEGIN

		SELECT
			@vcTimeLine=vcTimeLine
			,@tintControlField=tintControlField
			,@numRecordCount=numRecordCount
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID

		EXEC USP_ReportListMaster_Top10CustomerByProfitPercentPreBuildReport @numDomainID,@ClientTimeZoneOffset,@vcTimeLine,@tintControlField,@numRecordCount
	
		--EXEC USP_ReportListMaster_Top10CustomerByProfitPercentPreBuildReport @numDomainID
	END
	ELSE IF @intDefaultReportID = 9  -- TOP 10 CUSTOMERS BY PROFIT AMOUNT
	BEGIN

	SELECT
			@vcTimeLine=vcTimeLine
			,@tintControlField=tintControlField
			,@numRecordCount=numRecordCount
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID

		EXEC USP_ReportListMaster_Top10CustomerByProfitAmountPreBuildReport @numDomainID,@ClientTimeZoneOffset,@vcTimeLine,@tintControlField,@numRecordCount

		--EXEC USP_ReportListMaster_Top10CustomerByProfitAmountPreBuildReport @numDomainID
	END
	ELSE IF @intDefaultReportID = 10  -- ACTION ITEMS & MEETINGS DUE TODAY
	BEGIN
		SELECT
			@vcTimeLine=vcTimeLine
			,@vcTeritorry=vcTeritorry
			,@vcFilterBy=vcFilterBy
			,@vcFilterValue=vcFilterValue
			,@bitTask=bitTask
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID

		EXEC USP_ReportListMaster_ActionItemPreBuildReport @numDomainID,0,@ClientTimeZoneOffset,@vcTimeLine,@vcTeritorry,@vcFilterBy,@vcFilterValue,@bitTask
		
		--EXEC USP_ReportListMaster_ActionItemPreBuildReport @numDomainID,0,@ClientTimeZoneOffset
	END
	ELSE IF @intDefaultReportID = 11  -- YTD VS SAME PERIOD LAST YEAR
	BEGIN
		EXEC USP_ReportListMaster_RPEYTDAndSamePeriodLastYear @numDomainID
	END
	ELSE IF @intDefaultReportID = 12  -- ACTION ITEMS & MEETINGS DUE TODAY
	BEGIN

		SELECT
			@vcFilterValue=vcFilterValue
			
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID
		
		EXEC USP_ReportListMaster_RPELastMonthAndSamePeriodLastYear @numDomainID,@ClientTimeZoneOffset,@vcFilterValue
		--EXEC USP_ReportListMaster_RPELastMonthAndSamePeriodLastYear @numDomainID
	END
	ELSE IF @intDefaultReportID = 13  --  SALES VS EXPENSES (LAST 12 MONTHS)
	BEGIN
		SELECT
			@vcTimeLine=vcTimeLine
			,@vcGroupBy=vcGroupBy
			,@vcTeritorry=vcTeritorry
			,@vcFilterBy=vcFilterBy
			,@vcFilterValue=vcFilterValue
			,@dtFromDate=dtFromDate
			,@dtToDate=dtToDate
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID

		EXEC USP_ReportListMaster_SalesVsExpenseLast12Months @numDomainID,@ClientTimeZoneOffset,@vcTimeLine,@vcGroupBy,@vcTeritorry,@vcFilterBy,@vcFilterValue,@dtFromDate,@dtToDate
	END
	ELSE IF @intDefaultReportID = 14  --  TOP SOURCES OF SALES ORDERS (LAST 12 MONTHS)
	BEGIN

		SELECT
			@vcDealAmount=vcDealAmount
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID

		EXEC USP_ReportListMaster_TopSourceOfSalesOrder @numDomainID,@ClientTimeZoneOffset,@vcDealAmount
	
		--EXEC USP_ReportListMaster_TopSourceOfSalesOrder @numDomainID
	END
	ELSE IF @intDefaultReportID = 15  --  TOP 10 ITEMS BY PROFIT MARGIN
	BEGIN
		EXEC USP_ReportListMaster_Top10ItemByProfitMargin @numDomainID
	END
	ELSE IF @intDefaultReportID = 16  --  TOP 10 SALES OPPORTUNITIES BY REVENUE (LAST 12 MONTHS)
	BEGIN

		SELECT
			@numRecordCount=numRecordCount
			,@vcTimeLine=vcTimeLine
			,@vcDealAmount=vcDealAmount
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID

		EXEC USP_ReportListMaster_Top10SalesOpportunityByRevenue @numDomainID,@ClientTimeZoneOffset, @numRecordCount, @vcTimeLine, @vcDealAmount
		--EXEC USP_ReportListMaster_Top10SalesOpportunityByRevenue @numDomainID
	END
	ELSE IF @intDefaultReportID = 17  --  TOP 10 SALES OPPORTUNITIES BY TOTAL PROGRESS
	BEGIN
		EXEC USP_ReportListMaster_Top10SalesOpportunityByTotalProgress @numDomainID
	END
	ELSE IF @intDefaultReportID = 18  --  TOP 10 ITEMS RETURNED VS QTY SOLD
	BEGIN

		SELECT
			 @numRecordCount=numRecordCount
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID

		EXEC USP_ReportListMaster_Top10ItemsByReturnedVsSold @numDomainID, @numRecordCount
	END
	ELSE IF @intDefaultReportID = 19  --  LARGEST 10 SALES OPPORTUNITIES PAST THEIR DUE DATE
	BEGIN

		SELECT
			@vcTimeLine = vcTimeLine,
			@vcDealAmount = vcDealAmount,
			@vcTeritorry = vcTeritorry,
			@tintOppType=tintOppType,
			@vcFilterBy = vcFilterBy,
			@vcFilterValue = vcFilterValue,
			@tintTotalProgress = tintTotalProgress,
			@tintMinNumber = tintMinNumber,
			@tintMaxNumber = tintMaxNumber,
			@tintQtyToDisplay = tintQtyToDisplay,
			@vcDueDate = vcDueDate
			,@dtFromDate = dtFromDate
			,@dtToDate = dtToDate
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID

		EXEC USP_ReportListMaster_Top10SalesOpportunityByPastDue @numDomainID,@ClientTimeZoneOffset,@vcTimeLine,@vcTeritorry,@tintOppType,@vcDealAmount,@vcFilterBy,@vcFilterValue,@tintTotalProgress,@tintMinNumber,@tintMaxNumber,@tintQtyToDisplay,@vcDueDate,@dtFromDate,@dtToDate
	
		--EXEC USP_ReportListMaster_Top10SalesOpportunityByPastDue @numDomainID,@ClientTimeZoneOffset
	END
	ELSE IF @intDefaultReportID = 20  --  Top 10 Campaigns by ROI
	BEGIN
		EXEC USP_ReportListMaster_Top10CampaignsByROI @numDomainID
	END
	ELSE IF @intDefaultReportID = 21  --  MARKET FRAGMENTATION � TOP 10 CUSTOMERS, AS A PORTION TOTAL SALES
	BEGIN
		EXEC USP_ReportListMaster_TopCustomersByPortionOfTotalSales @numDomainID
	END
	ELSE IF @intDefaultReportID = 22  --  SCORE CARD
	BEGIN
		EXEC USP_ReportListMaster_PrebuildScoreCard @numDomainID
	END
	ELSE IF @intDefaultReportID = 23  --  TOP 10 LEAD SOURCES (NEW LEADS)
	BEGIN

		SELECT
			@vcTimeLine=vcTimeLine
			,@vcGroupBy=vcGroupBy
			,@vcTeritorry=vcTeritorry
			,@vcFilterBy=vcFilterBy
			,@vcFilterValue=vcFilterValue
			,@dtFromDate=dtFromDate
			,@dtToDate=dtToDate
			,@numRecordCount=numRecordCount
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID

		EXEC USP_ReportListMaster_Top10LeadSource @numDomainID,@ClientTimeZoneOffset,@vcTimeLine,@vcGroupBy,@vcTeritorry,@vcFilterBy,@vcFilterValue,@dtFromDate,@dtToDate,@numRecordCount
	
		--EXEC USP_ReportListMaster_Top10LeadSource @numDomainID
	END
	ELSE IF @intDefaultReportID = 24  --  LAST 10 EMAIL MESSAGES FROM PEOPLE I DO BUSINESS WITH
	BEGIN
		EXEC USP_ReportListMaster_Top10Email @numDomainID,@numUserCntID
	END
	ELSE IF @intDefaultReportID = 25  --  Reminders
	BEGIN
		EXEC USP_ReportListMaster_RemindersPreBuildReport @numDomainID,@ClientTimeZoneOffset
	END
	ELSE IF @intDefaultReportID = 26  --  TOP REASONS WHY WE�RE WINING DEALS
	BEGIN
		
		SELECT
			@tintOppType=tintOppType
			,@vcDealAmount=vcDealAmount
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID

		EXEC USP_ReportListMaster_Top10ReasonsDealWins @numDomainID,@ClientTimeZoneOffset, @tintOppType, @vcDealAmount
		--EXEC USP_ReportListMaster_Top10ReasonsDealWins @numDomainID
	END
	ELSE IF @intDefaultReportID = 27  --  TOP REASONS WHY WE�RE LOOSING DEALS
	BEGIN

		SELECT
			@tintOppType=tintOppType
			,@vcDealAmount=vcDealAmount
			,@lngPConclAnalysis= lngPConclAnalysis
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID

		EXEC USP_ReportListMaster_Top10ReasonsDealLost @numDomainID,@ClientTimeZoneOffset, @tintOppType, @vcDealAmount, @lngPConclAnalysis
		--EXEC USP_ReportListMaster_Top10ReasonsDealLost @numDomainID
	END
	ELSE IF @intDefaultReportID = 28  --  TOP 10 REASONS FOR SALES RETURNS
	BEGIN

		SELECT
			 @numRecordCount=numRecordCount
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID
		EXEC USP_ReportListMaster_Top10ReasonsForSalesReturns @numDomainID, @numDashBoardID
	END
	ELSE IF @intDefaultReportID = 29  --  EMPLOYEE SALES PERFORMANCE PANEL 1
	BEGIN

		SELECT
			@vcTimeLine=vcTimeLine
			,@vcFilterBy=vcFilterBy
			,@vcFilterValue=vcFilterValue
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID

		EXEC USP_ReportListMaster_EmployeeSalesPerformancePanel1 @numDomainID,@ClientTimeZoneOffset, @vcTimeLine, @vcFilterBy,@vcFilterValue
		--EXEC USP_ReportListMaster_EmployeeSalesPerformancePanel1 @numDomainID
	END
	ELSE IF @intDefaultReportID = 30  --  PARTNER REVENUES, MARGINS & PROFITS (LAST 12 MONTHS)
	BEGIN
		EXEC USP_ReportListMaster_PartnerRMPLast12Months @numDomainID
	END
	ELSE IF @intDefaultReportID = 31  --  EMPLOYEE SALES PERFORMANCE PT 1 (LAST 12 MONTHS)
	BEGIN
		
		SELECT
			@vcTimeLine=vcTimeLine
			,@vcFilterBy=vcFilterBy
			,@vcFilterValue=vcFilterValue
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID

		EXEC USP_ReportListMaster_EmployeeSalesPerformance1 @numDomainID,@ClientTimeZoneOffset, @vcTimeLine, @vcFilterBy,@vcFilterValue
		--EXEC USP_ReportListMaster_EmployeeSalesPerformance1 @numDomainID
	END
	ELSE IF @intDefaultReportID = 32  --  EMPLOYEE BENEFIT TO COMPANY (LAST 12 MONTHS)
	BEGIN

		SELECT
			@vcTimeLine=vcTimeLine
			,@vcFilterBy=vcFilterBy
			,@vcFilterValue=vcFilterValue
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID

		EXEC USP_ReportListMaster_EmployeeBenefitToCompany @numDomainID,@ClientTimeZoneOffset, @vcTimeLine, @vcFilterBy,@vcFilterValue
	END
	ELSE IF @intDefaultReportID = 33  --  FIRST 10 SAVED SEARCHES
	BEGIN
		EXEC USP_ReportListMaster_First10SavedSearch @numDomainID
	END
	ELSE IF @intDefaultReportID = 34  --  SALES OPPORTUNITY WON/LOST REPORT
	BEGIN
		SELECT
			@vcTimeLine=vcTimeLine
			,@vcGroupBy=vcGroupBy
			,@vcTeritorry=vcTeritorry
			,@vcFilterBy=vcFilterBy
			,@vcFilterValue=vcFilterValue
			,@dtFromDate=dtFromDate
			,@dtToDate=dtToDate
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID

		EXEC USP_ReportListMaster_SalesOppWonLostPreBuildReport @numDomainID,@ClientTimeZoneOffset,@vcTimeLine,@vcGroupBy,@vcTeritorry,@vcFilterBy,@vcFilterValue,@dtFromDate,@dtToDate
	END
	ELSE IF @intDefaultReportID = 35  --  SALES OPPORTUNITY PIPELINE
	BEGIN
		SELECT
			@vcTimeLine=vcTimeLine
			,@vcGroupBy=vcGroupBy
			,@vcTeritorry=vcTeritorry
			,@vcFilterBy=vcFilterBy
			,@vcFilterValue=vcFilterValue
			,@dtFromDate=dtFromDate
			,@dtToDate=dtToDate
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID

		EXEC USP_ReportListMaster_SalesOppPipeLine @numDomainID,@ClientTimeZoneOffset,@vcTimeLine,@vcGroupBy,@vcTeritorry,@vcFilterBy,@vcFilterValue,@dtFromDate,@dtToDate
	END
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_SalesVsExpenseLast12Months')
DROP PROCEDURE USP_ReportListMaster_SalesVsExpenseLast12Months
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_SalesVsExpenseLast12Months]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@vcTimeLine VARCHAR(50)
	,@vcGroupBy VARCHAR(50)
	,@vcTeritorry VARCHAR(MAX)
	,@vcFilterBy TINYINT  -- 1:Assign TO, 2: Record Owner, 3: Teams (Based on Assigned To) 
	,@vcFilterValue VARCHAR(MAX)
	,@dtFromDate DATE
	,@dtToDate DATE
AS
BEGIN 
	DECLARE @StartDate DATE
	DECLARE @EndDate DATE
	DECLARE @TableInsertCount AS INT = 0, @j AS INT =0
	SET @EndDate = EOMONTH(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))

	DECLARE @TABLE TABLE
	(
		ID INT IDENTITY(1,1)
		,MonthStartDate DATE
		,MonthEndDate DATE
		,MonthLabel VARCHAR(20)
		,MonthSales DECIMAL(20,5)
		,MonthExpense DECIMAL(20,5)
	)

	IF @vcTimeLine = 'Last12Months'
	BEGIN
		SET @StartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, @EndDate), 0)
		SET @TableInsertCount=12
	END
	IF @vcTimeLine = 'Last6Months'
	BEGIN
		SET @StartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, @EndDate), 0)
		SET @TableInsertCount=6
	END
	IF @vcTimeLine = 'Last3Months'
	BEGIN
		SET @StartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, @EndDate), 0)
		SET @TableInsertCount=3
	END
	IF @vcTimeLine = 'Last30Days'
	BEGIN
		SET @EndDate = (GETDATE()) 
		SET @StartDate = DATEADD(DAY, DATEDIFF(DAY, -30, @EndDate), 0)
		SET @TableInsertCount=30
	END
	IF @vcTimeLine = 'Last7Days'
	BEGIN
		SET @EndDate = (DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())) 
		SET @StartDate = DATEADD(DAY, DATEDIFF(DAY, -7, @EndDate), 0)
		SET @TableInsertCount=7
	END
	

	WHILE @j < @TableInsertCount
	BEGIN

		IF(@vcTimeLine = 'Last30Days' OR @vcTimeLine = 'Last7Days')
		BEGIN 
			INSERT INTO @TABLE
			(
				MonthStartDate,
				MonthEndDate,
				MonthLabel
			)
			VALUES
			(
				DATEADD(DAY,-(@j+1),@EndDate),
				DATEADD(DAY,-@j,@EndDate),
				CONCAT(CONVERT(VARCHAR(6),DATEADD(DAY,-@j,@EndDate), 100),' ',YEAR(DATEADD(DAY,-@j,@EndDate)))
			);
		END
		ELSE
		BEGIN
			INSERT INTO @TABLE
			(
				MonthStartDate,
				MonthEndDate,
				MonthLabel
			)
			VALUES
			(
				DATEADD(MONTH,-@j,@StartDate),
				DATEADD(MONTH,-@j,@EndDate),
				CONCAT(CONVERT(VARCHAR(3),DATEADD(MONTH,-@j,@EndDate), 100),' ',YEAR(DATEADD(MONTH,-@j,@EndDate)))
			);
		END

		SET @j= @j + 1;
	END

	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT
	DECLARE @TempSales DECIMAL(20,5) = 0
	DECLARE @TempExpense DECIMAL(20,5) = 0


	SELECT @iCount=COUNT(*) FROM @TABLE

	WHILE @i <= @iCount
	BEGIN
		SET @TempSales = 0
		SET @TempExpense = 0

		SELECT @StartDate=MonthStartDate,@EndDate=MonthEndDate FROM @TABLE WHERE ID=@i

		SELECT 
			@TempSales = SUM(monTotAmount)
		FROM
		(
			SELECT
				ISNULL(monTotAmount,0) monTotAmount
			FROM
				OpportunityItems OI
			INNER JOIN
				OpportunityMaster OM
			ON
				OI.numOppId = OM.numOppID
			INNER JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			WHERE
				OM.numDomainId=@numDomainID
				AND CAST(OM.bintCreatedDate AS DATE) BETWEEN  @StartDate AND @EndDate
				AND ISNULL(OM.tintOppType,0)=1
				AND ISNULL(OM.tintOppStatus,0)=1
		) TEMP
	
		SELECT @TempExpense = SUM(numDebitAmt) - SUM(numCreditAmt) FROM General_Journal_Details INNER JOIN General_Journal_Header ON General_Journal_Details.numJournalId=General_Journal_Header.numJournal_Id WHERE General_Journal_Details.numDomainId=@numDomainID AND CAST(datEntry_Date AS DATE) BETWEEN  @StartDate AND @EndDate AND General_Journal_Details.numChartAcntId IN (SELECT numAccountId FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND bitActive = 1 AND numAcntTypeId IN (SELECT numAccountTypeID FROM AccountTypeDetail WHERE numDomainID=@numDomainID AND vcAccountCode LIKE '0104%'))

		UPDATE @TABLE SET MonthSales=ISNULL(@TempSales,0), MonthExpense=ISNULL(@TempExpense,0) WHERE ID=@i

		SET @i = @i + 1
	END

	SELECT * FROM @TABLE
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10CustomerByProfitAmountPreBuildReport')
DROP PROCEDURE USP_ReportListMaster_Top10CustomerByProfitAmountPreBuildReport
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10CustomerByProfitAmountPreBuildReport]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@vcTimeLine VARCHAR(50)
	,@tintControlField INT
	,@numRecordCount INT
AS
BEGIN 
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @ProfitCost INT
	SELECT @numShippingItemID=numShippingServiceItemID,@numDiscountItemID=numDiscountServiceItemID,@ProfitCost=numCost FROM Domain WHERE numDomainId=@numDomainID


	SELECT TOP (@numRecordCount)
		numDivisionID
		,(CASE tintCRMType WHEN 1 THEN CONCAT('~/prospects/frmProspects.aspx?DivID=',numDivisionID) WHEN 2 THEN CONCAT('~/Account/frmAccounts.aspx?DivID=',numDivisionID) ELSE CONCAT('~/Leads/frmLeads.aspx?DivID=',numDivisionID) END) AS URL
		,vcCompanyName
		,SUM(Profit) Profit
	FROM
	(
		SELECT
			DM.numDivisionID
			,tintCRMType
			,CI.vcCompanyName
			,ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * (CASE @ProfitCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numPurchaseUnit) WHEN 2 THEN numCost * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numUOMId) ELSE monAverageCost END)) Profit
			,ISNULL(monTotAmount,0) monTotAmount
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND ISNULL(OI.monTotAmount,0) <> 0
			AND ISNULL(OI.numUnitHour,0) <> 0
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP
	GROUP BY
		numDivisionID
		,tintCRMType
		,vcCompanyName

	HAVING
		(CASE
		   WHEN @tintControlField=2 THEN (SUM(Profit)/SUM(monTotAmount)) * 100
		   WHEN @tintControlField=1 THEN SUM(Profit) 
		END ) > 0
		--(SUM(Profit)/SUM(monTotAmount)) * 100 > 0
	ORDER BY
		CASE
		   WHEN @tintControlField=2 THEN (SUM(Profit)/ SUM(monTotAmount)) * 100 
		   WHEN @tintControlField=1 THEN SUM(Profit) 
		END DESC
		--(SUM(Profit)/SUM(monTotAmount)) * 100 DESC
	--HAVING
	--	SUM(Profit) > 0
	--ORDER BY
	--	SUM(Profit) DESC
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10CustomerByProfitPercentPreBuildReport')
DROP PROCEDURE USP_ReportListMaster_Top10CustomerByProfitPercentPreBuildReport
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10CustomerByProfitPercentPreBuildReport]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@vcTimeLine VARCHAR(50)
	,@tintControlField INT
	,@numRecordCount INT
AS
BEGIN 
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @ProfitCost INT
	SELECT @numShippingItemID=numShippingServiceItemID,@numDiscountItemID=numDiscountServiceItemID,@ProfitCost=numCost FROM Domain WHERE numDomainId=@numDomainID


	SELECT TOP (@numRecordCount)
		numDivisionID
		,vcCompanyName
		,(CASE tintCRMType WHEN 1 THEN CONCAT('~/prospects/frmProspects.aspx?DivID=',numDivisionID) WHEN 2 THEN CONCAT('~/Account/frmAccounts.aspx?DivID=',numDivisionID) ELSE CONCAT('~/Leads/frmLeads.aspx?DivID=',numDivisionID) END) AS URL
		,(SUM(Profit)/SUM(monTotAmount)) * 100 BlendedProfit
	FROM
	(
		SELECT
			DM.numDivisionID
			,tintCRMType
			,CI.vcCompanyName
			,ISNULL(monTotAmount,0) monTotAmount
			,ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * (CASE @ProfitCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numPurchaseUnit) WHEN 2 THEN numCost * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numUOMId) ELSE monAverageCost END)) Profit
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND ISNULL(monTotAmount,0) <> 0
			AND ISNULL(numUnitHour,0) <> 0
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP
	GROUP BY
		numDivisionID
		,tintCRMType
		,vcCompanyName
	HAVING
		(CASE
		   WHEN @tintControlField=2 THEN (SUM(Profit)/SUM(monTotAmount)) * 100 
		   WHEN @tintControlField=1 THEN SUM(Profit) 
		END) > 0 
		--(SUM(Profit)/SUM(monTotAmount)) * 100 > 0
	ORDER BY
		CASE
		   WHEN @tintControlField=2 THEN (SUM(Profit)/SUM(monTotAmount)) * 100 
		   WHEN @tintControlField=1 THEN SUM(Profit) 
		END DESC
		--(SUM(Profit)/SUM(monTotAmount)) * 100 DESC
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10ItemByProfitPreBuildReport')
DROP PROCEDURE USP_ReportListMaster_Top10ItemByProfitPreBuildReport
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10ItemByProfitPreBuildReport]
	@numDomainID NUMERIC(18,0)
	--,@ClientTimeZoneOffset INT
	,@vcTimeLine VARCHAR(50)
	,@vcGroupBy VARCHAR(50)
	,@vcTeritorry VARCHAR(MAX)
	,@vcFilterBy TINYINT  -- 1:Assign TO, 2: Record Owner, 3: Teams (Based on Assigned To) 
	,@vcFilterValue VARCHAR(MAX)
	,@dtFromDate DATE
	,@dtToDate DATE
	,@numRecordCount INT
	,@tintControlField INT
AS
BEGIN 
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @ProfitCost INT
	SELECT @numShippingItemID=numShippingServiceItemID,@numDiscountItemID=numDiscountServiceItemID,@ProfitCost=numCost FROM Domain WHERE numDomainId=@numDomainID


	SELECT TOP 10
		numItemCode
		,vcItemName,
		(CASE WHEN @tintControlField = 1 THEN SUM(Profit) 
		   WHEN @tintControlField = 2 THEN (SUM(Profit)/SUM(monTotAmount)) * 100 
		   WHEN @tintControlField = 3 THEN SUM(monTotAmount)  END) as Profit 	 		
		--,SUM(Profit) Profit
	FROM
	(
		SELECT
			I.numItemCode
			,I.vcItemName
			,ISNULL(monTotAmount,0) monTotAmount
			,ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * (CASE @ProfitCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numPurchaseUnit) WHEN 2 THEN numCost * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numUOMId) ELSE monAverageCost END)) Profit
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND OM.bintCreatedDate >= DATEADD(MONTH, -12, GETUTCDATE())
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP	
	GROUP BY
		numItemCode
		,vcItemName
	HAVING
		   (CASE 
		   WHEN @tintControlField = 1 THEN SUM(Profit) 
		   WHEN @tintControlField = 2 THEN (SUM(Profit)/SUM(monTotAmount)) * 100 
		   WHEN @tintControlField = 3 THEN SUM(monTotAmount) 
		   END) > 0
		
	ORDER BY
		   (CASE 
		   WHEN @tintControlField = 1 THEN SUM(Profit) 
		   WHEN @tintControlField = 2 THEN (SUM(Profit)/SUM(monTotAmount)) * 100 
		   WHEN @tintControlField = 3 THEN SUM(monTotAmount) 
		   END) DESC
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10ItemsByReturnedVsSold')
DROP PROCEDURE USP_ReportListMaster_Top10ItemsByReturnedVsSold
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10ItemsByReturnedVsSold]
	@numDomainID NUMERIC(18,0),
	@numRecordCount INT
AS
BEGIN 
	SELECT TOP (@numRecordCount)
		I.vcItemName
		,CONCAT('~/Items/frmKitDetails.aspx?ItemCode=',I.numItemCode,'&frm=All Items') AS URL
		,SUM(RI.numUnitHour)
		,SUM(OI.numUnitHour)
		,(SUM(RI.numUnitHour) * 100)/ (CASE WHEN SUM(OI.numUnitHour) = 0 THEN 1.0 ELSE SUM(OI.numUnitHour) END) ReturnPercent
	FROM 
		OpportunityItems OI
	INNER JOIN
		OpportunityMaster OM
	ON
		OI.numOppId = OM.numOppId
	INNER JOIN
		Item I
	ON
		OI.numItemCode = I.numItemCode
	LEFT JOIN
		ReturnItems RI
	ON
		(RI.numOppItemID = OI.numoppitemtCode
		OR RI.numReturnItemID = I.numItemCode)
	LEFT JOIN
		ReturnHeader RH
	ON
		RI.numReturnHeaderID = RH.numReturnHeaderID
	WHERE
		OM.numDomainId=@numDomainID
		AND RH.numDomainId=@numDomainID
		AND RH.tintReturnType IN (1,3)
		AND ISNULL(tintOppType,0) = 1
		AND ISNULL(tintOppStatus,0) = 1
	GROUP BY
		I.numItemCode
		,I.vcItemName
	ORDER BY
		(SUM(RI.numUnitHour) * 100)/ (CASE WHEN SUM(OI.numUnitHour) = 0 THEN 1.0 ELSE SUM(OI.numUnitHour) END) DESC,I.numItemCode ASC
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10LeadSource')
DROP PROCEDURE USP_ReportListMaster_Top10LeadSource
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10LeadSource]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@vcTimeLine VARCHAR(50)
	,@vcGroupBy VARCHAR(50)
	,@vcTeritorry VARCHAR(MAX)
	,@vcFilterBy TINYINT  -- 1:Assign TO, 2: Record Owner, 3: Teams (Based on Assigned To) 
	,@vcFilterValue VARCHAR(MAX)
	,@dtFromDate DATE
	,@dtToDate DATE
	,@numRecordCount INT
AS
BEGIN 
	DECLARE @TEMP TABLE
	(
		numDivisionID NUMERIC(18,0)
		,RowID NUMERIC(18,0)
	)

	INSERT INTO @TEMP
	(
		numDivisionID
		,RowID
	)
	SELECT 
		numDivisionID
		,MIN(ID) 
	FROM 
		DivisionMasterPromotionHistory
	WHERE
		numDomainID = @numDomainID
	GROUP BY
		numDivisionID


	DECLARE @TotalLeads NUMERIC(18,2) 

	SELECT
		@TotalLeads = COUNT(DM.numDivisionID)
	FROM
		DivisionMaster DM
	INNER JOIN	
		CompanyInfo CI
	ON
		DM.numCompanyID = CI.numCompanyId
	LEFT JOIN
		ListDetails LD
	ON
		LD.numListID=18
		AND CI.vcHow = LD.numListItemID
		AND (LD.constFlag=1 OR LD.numDomainID=@numDomainID)
	LEFT JOIN
		@TEMP T1
	ON
		DM.numDivisionID = T1.numDivisionID
	LEFT JOIN
		DivisionMasterPromotionHistory DMPH
	ON
		T1.RowID = DMPH.ID
	WHERE
		DM.numDomainID = @numDomainID
		AND ((ISNULL(DM.tintCRMType,0) = 0 AND DMPH.ID IS NULL) OR ISNULL(DMPH.tintPreviousCRMType,0) = 0)

	SELECT TOP (@numRecordCount)
		LD.vcData
		,CAST((COUNT(*) * 100) / @TotalLeads AS NUMERIC(18,2)) LeadSourcePercent
	FROM
		DivisionMaster DM
	INNER JOIN	
		CompanyInfo CI
	ON
		DM.numCompanyID = CI.numCompanyId
	LEFT JOIN
		ListDetails LD
	ON
		LD.numListID=18
		AND CI.vcHow = LD.numListItemID
		AND (LD.constFlag=1 OR LD.numDomainID=@numDomainID)
	LEFT JOIN
		@TEMP T1
	ON
		DM.numDivisionID = T1.numDivisionID
	LEFT JOIN
		DivisionMasterPromotionHistory DMPH
	ON
		T1.RowID = DMPH.ID
	WHERE
		DM.numDomainID = @numDomainID
		AND ((ISNULL(DM.tintCRMType,0) = 0 AND DMPH.ID IS NULL) OR ISNULL(DMPH.tintPreviousCRMType,0) = 0)
		AND LD.numListItemID IS NOT NULL
	GROUP BY
		LD.numListItemID
		,LD.vcData
	ORDER BY
		(COUNT(*) * 100) / @TotalLeads DESC
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10ReasonsDealLost')
DROP PROCEDURE USP_ReportListMaster_Top10ReasonsDealLost
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10ReasonsDealLost]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@tintOppType NUMERIC(18,0)
	,@vcDealAmount VARCHAR(MAX)
	,@lngPConclAnalysis VARCHAR(MAX)
AS
BEGIN 
	DECLARE @TotalDealWon FLOAT = 0.0

	SELECT 
		@TotalDealWon = COUNT(numOppId)
	FROM 
		OpportunityMaster
	WHERE 
		numDomainId=@numDomainID 
		AND tintOppType=(@tintOppType) 
		AND tintOppStatus=2
	
	SELECT TOP 10
		Reason
		,(NoOfOppToOrder * 100)/ (CASE WHEN ISNULL(@TotalDealWon,0) = 0 THEN 1 ELSE @TotalDealWon END) AS TotalDealLostPercent
	FROM
	(
		SELECT 
			ISNULL(ListDetails.vcData,'-') AS Reason
			,COUNT(numOppID) AS NoOfOppToOrder 
		FROM 
			OpportunityMaster 
		LEFT JOIN
			ListDetails
		ON
			OpportunityMaster.lngPConclAnalysis = ListDetails.numListItemID
			AND ListDetails.numListID = 12
			AND (ListDetails.numDomainID =@numDomainID OR ISNULL(ListDetails.constFlag,0) = 1)
		WHERE 
			OpportunityMaster.numDomainId=@numDomainID 
			AND tintOppType=(@tintOppType)
			AND tintOppStatus=2

			AND ( 1 = (CASE WHEN CHARINDEX('1-5K',@vcDealAmount) > 0 THEN (CASE WHEN OpportunityMaster.monDealAmount BETWEEN 1000 AND 5000 THEN 1 ELSE 0 END) ELSE 0 END)
				OR 1 = (CASE WHEN CHARINDEX('5-10K',@vcDealAmount) > 0 THEN (CASE WHEN OpportunityMaster.monDealAmount BETWEEN 5000 AND 10000 THEN 1 ELSE 0 END) ELSE 0 END)
				OR 1 = (CASE WHEN CHARINDEX('10-20K',@vcDealAmount) > 0 THEN (CASE WHEN OpportunityMaster.monDealAmount BETWEEN 10000 AND 20000 THEN 1 ELSE 0 END) ELSE 0 END)
				OR 1 = (CASE WHEN CHARINDEX('20-50K',@vcDealAmount) > 0 THEN (CASE WHEN OpportunityMaster.monDealAmount BETWEEN 20000 AND 50000 THEN 1 ELSE 0 END) ELSE 0 END)
				OR 1 = (CASE WHEN CHARINDEX('>50K',@vcDealAmount) > 0 THEN (CASE WHEN OpportunityMaster.monDealAmount > 50000 THEN 1 ELSE 0 END) ELSE 0 END))

		AND 1 = (CASE WHEN LEN(ISNULL(@lngPConclAnalysis,'')) > 0 THEN (CASE WHEN OpportunityMaster.lngPConclAnalysis IN (SELECT Id FROM dbo.SplitIDs((@lngPConclAnalysis),',')) THEN 1 ELSE 0 END) ELSE 1 END)
		
		GROUP BY
			ListDetails.vcData
	) TEMP
	ORDER BY
		NoOfOppToOrder DESC
		
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10ReasonsDealWins')
DROP PROCEDURE USP_ReportListMaster_Top10ReasonsDealWins
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10ReasonsDealWins]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@tintOppType NUMERIC(18,0)
	,@vcDealAmount VARCHAR(MAX)
AS
BEGIN 
	DECLARE @TotalDealWon FLOAT = 0.0

	SELECT 
		@TotalDealWon = COUNT(numOppId)
	FROM 
		OpportunityMaster
	WHERE 
		numDomainId=@numDomainID 
		AND tintOppType=(@tintOppType)
		AND tintOppStatus=1 
		AND bintOppToOrder IS NOT NULL
	
	SELECT TOP 10
		Reason
		,(NoOfOppToOrder * 100)/ (CASE WHEN ISNULL(@TotalDealWon,0) = 0 THEN 1 ELSE @TotalDealWon END) AS TotalDealWonPercent
	FROM
	(
		SELECT 
			ISNULL(ListDetails.vcData,'-') AS Reason
			,COUNT(numOppID) AS NoOfOppToOrder 
		FROM 
			OpportunityMaster 
		LEFT JOIN
			ListDetails
		ON
			OpportunityMaster.lngPConclAnalysis = ListDetails.numListItemID
			AND ListDetails.numListID = 12
			AND (ListDetails.numDomainID =@numDomainID OR ISNULL(ListDetails.constFlag,0) = 1)
		WHERE 
			OpportunityMaster.numDomainId=@numDomainID 
			AND tintOppType=(@tintOppType)
			AND tintOppStatus=1 
			AND bintOppToOrder IS NOT NULL
			
			AND ( 1 = (CASE WHEN CHARINDEX('1-5K',@vcDealAmount) > 0 THEN (CASE WHEN OpportunityMaster.monDealAmount BETWEEN 1000 AND 5000 THEN 1 ELSE 0 END) ELSE 0 END)
				OR 1 = (CASE WHEN CHARINDEX('5-10K',@vcDealAmount) > 0 THEN (CASE WHEN OpportunityMaster.monDealAmount BETWEEN 5000 AND 10000 THEN 1 ELSE 0 END) ELSE 0 END)
				OR 1 = (CASE WHEN CHARINDEX('10-20K',@vcDealAmount) > 0 THEN (CASE WHEN OpportunityMaster.monDealAmount BETWEEN 10000 AND 20000 THEN 1 ELSE 0 END) ELSE 0 END)
				OR 1 = (CASE WHEN CHARINDEX('20-50K',@vcDealAmount) > 0 THEN (CASE WHEN OpportunityMaster.monDealAmount BETWEEN 20000 AND 50000 THEN 1 ELSE 0 END) ELSE 0 END)
				OR 1 = (CASE WHEN CHARINDEX('>50K',@vcDealAmount) > 0 THEN (CASE WHEN OpportunityMaster.monDealAmount > 50000 THEN 1 ELSE 0 END) ELSE 0 END))
		GROUP BY
			ListDetails.vcData
	) TEMP
	ORDER BY
		NoOfOppToOrder DESC
		
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10ReasonsForSalesReturns')
DROP PROCEDURE USP_ReportListMaster_Top10ReasonsForSalesReturns
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10ReasonsForSalesReturns]
	@numDomainID NUMERIC(18,0),
	@numRecordCount INT
AS
BEGIN 
	DECLARE @TotalSalesReturn FLOAT = 0.0

	SELECT 
		@TotalSalesReturn = COUNT(numReturnHeaderID)
	FROM 
		ReturnHeader
	WHERE 
		numDomainId=@numDomainID 
		AND tintReturnType IN (1,3)
	
	SELECT TOP (@numRecordCount)
		Reason
		,(NoOfSalesReturn * 100)/ (CASE WHEN ISNULL(@TotalSalesReturn,0) = 0 THEN 1 ELSE @TotalSalesReturn END) AS TotalReturnPercent
	FROM
	(
		SELECT 
			ISNULL(ListDetails.vcData,'-') AS Reason
			,COUNT(numReturnHeaderID) AS NoOfSalesReturn
		FROM 
			ReturnHeader 
		LEFT JOIN
			ListDetails
		ON
			ReturnHeader.numReturnReason = ListDetails.numListItemID
			AND ListDetails.numListID = 48
			AND (ListDetails.numDomainID =@numDomainID OR ISNULL(ListDetails.constFlag,0) = 1)
		WHERE 
			ReturnHeader.numDomainId=@numDomainID 
			AND tintReturnType IN (1,3)
		GROUP BY
			ListDetails.vcData
	) TEMP
	ORDER BY
		NoOfSalesReturn DESC
		
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10SalesOpportunityByPastDue')
DROP PROCEDURE USP_ReportListMaster_Top10SalesOpportunityByPastDue
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10SalesOpportunityByPastDue]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@vcTimeLine VARCHAR(50)
	,@vcTeritorry VARCHAR(MAX)
	,@tintOppType INT
	,@vcDealAmount VARCHAR(250)
	,@vcFilterBy TINYINT  -- 1:Assign TO, 2: Record Owner, 3: Teams (Based on Assigned To) 
	,@vcFilterValue VARCHAR(MAX)
	,@tintTotalProgress INT
	,@tintMinNumber INT
	,@tintMaxNumber INT
	,@tintQtyToDisplay INT
	,@vcDueDate VARCHAR(250)
	,@dtFromDate DATE
	,@dtToDate DATE
AS
BEGIN 

	DECLARE @tintFiscalStartMonth TINYINT

	SELECT @tintFiscalStartMonth=ISNULL(tintFiscalStartMonth,1) FROM Domain WHERE numDomainId=@numDomainID
	DECLARE @CurrentYearStartDate DATE 
	DECLARE @CurrentYearEndDate DATE

	SET @CurrentYearStartDate = DATEFROMPARTS(YEAR(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())),@tintFiscalStartMonth,1)
	IF @CurrentYearStartDate > DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
	BEGIN
		SET @CurrentYearStartDate = DATEADD(YEAR,-1,@CurrentYearStartDate)
	END	
	SET @CurrentYearEndDate = DATEADD(DAY,-1, DATEADD(YEAR,1,@CurrentYearStartDate))

	DECLARE @TABLEQuater TABLE
	(
		ID INT
		,QuaterStartDate DATE
		,QuaterEndDate DATE
	)

	INSERT INTO @TABLEQuater
	(
		ID
		,QuaterStartDate
		,QuaterEndDate
	)
	VALUES
	(
		1,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1),DATEADD(DAY,-1,DATEADD(MONTH,3,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1)))
	),
	(
		2,DATEADD(MONTH,3,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1)),DATEADD(DAY,-1,DATEADD(MONTH,6,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1)))
	),
	(
		3,DATEADD(MONTH,6,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1)),DATEADD(DAY,-1,DATEADD(MONTH,9,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1)))
	),
	(
		4,DATEADD(MONTH,9,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1)),DATEADD(DAY,-1,DATEADD(MONTH,12,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1)))
	)

	IF @vcDueDate = 'CurYear'
	BEGIN
		SET @dtFromDate = @CurrentYearStartDate
		SET @dtToDate = @CurrentYearEndDate
	END
	ELSE IF @vcDueDate = 'PreYear'
	BEGIN
		SET @dtFromDate =  DATEADD(YEAR,-1,@CurrentYearStartDate)
		SET @dtToDate = DATEADD(DAY,-1, DATEADD(YEAR,1,@dtFromDate))
	END
	ELSE IF @vcDueDate = 'Pre2Year'
	BEGIN
		SET @dtFromDate = DATEADD(YEAR,-2,@CurrentYearStartDate)
		SET @dtToDate = DATEADD(DAY,-1,DATEADD(YEAR,2,@dtFromDate))
	END
	ELSE IF @vcDueDate = 'Ago2Year'
	BEGIN
		SET @dtFromDate =  NULL
		SET @dtToDate = DATEADD(DAY,-1,DATEADD(YEAR,-2,@CurrentYearStartDate))
	END
	ELSE IF @vcDueDate = 'CurPreYear'
	BEGIN
		SET @dtFromDate = DATEADD(YEAR,-1,@CurrentYearStartDate)
		SET @dtToDate = DATEADD(DAY,-1, DATEADD(YEAR,1,@dtFromDate))
	END
	ELSE IF @vcDueDate = 'CurPre2Year'
	BEGIN
		SET @dtFromDate = DATEADD(YEAR,-2,@CurrentYearStartDate)
		SET @dtToDate = DATEADD(DAY,-1,DATEADD(YEAR,1, DATEFROMPARTS(YEAR(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())),@tintFiscalStartMonth,1)))
	END
	ELSE IF @vcDueDate = 'CuQur'
	BEGIN
		SELECT
			@dtFromDate = QuaterStartDate
			,@dtToDate = QuaterEndDate
		FROM
			@TABLEQuater
		WHERE
			DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()) BETWEEN QuaterStartDate AND QuaterEndDate
	END
	ELSE IF @vcDueDate = 'PreQur'
	BEGIN
		SELECT
			@dtFromDate = DATEADD(MONTH,-3,QuaterStartDate)
			,@dtToDate = DATEADD(MONTH,-3,QuaterEndDate)
		FROM
			@TABLEQuater
		WHERE
			DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()) BETWEEN QuaterStartDate AND QuaterEndDate
	END
	ELSE IF @vcDueDate = 'CurPreQur'
	BEGIN
		SELECT
			@dtFromDate = DATEADD(MONTH,-3,QuaterStartDate)
			,@dtToDate = QuaterEndDate
		FROM
			@TABLEQuater
		WHERE
			DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()) BETWEEN QuaterStartDate AND QuaterEndDate
	END
	ELSE IF @vcDueDate = 'ThisMonth'
	BEGIN
		SET @dtFromDate = dbo.get_month_start(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = dbo.get_month_end(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
	END
	ELSE IF @vcDueDate = 'LastMonth'
	BEGIN
		SET @dtFromDate = DATEADD(MONTH,-1,dbo.get_month_start(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())))
		SET @dtToDate = DATEADD(MONTH,-1,dbo.get_month_end(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())))
	END
	ELSE IF @vcDueDate = 'CurPreMonth'
	BEGIN
		SET @dtFromDate = DATEADD(MONTH,-1,dbo.get_month_start(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())))
		SET @dtToDate = dbo.get_month_end(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
	END
	ELSE IF @vcDueDate = 'LastWeek'
	BEGIN
		SET @dtFromDate = DATEADD(DAY,-7,dbo.get_week_start(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())))
		SET @dtToDate = DATEADD(DAY,-7,dbo.get_week_end(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())))
	END
	ELSE IF @vcDueDate = 'ThisWeek'
	BEGIN
		SET @dtFromDate = dbo.get_week_start(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = dbo.get_week_end(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
	END
	ELSE IF @vcDueDate = 'Yesterday'
	BEGIN
		SET @dtFromDate = DATEADD(DAY,-1,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = DATEADD(DAY,-1,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
	END
	ELSE IF @vcDueDate = 'Today'
	BEGIN
		SET @dtFromDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
		SET @dtToDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
	END
	ELSE IF @vcDueDate = 'Last7Day'
	BEGIN
		SET @dtFromDate = DATEADD(DAY,-7,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
	END
	ELSE IF @vcDueDate = 'Last30Day'
	BEGIN
		SET @dtFromDate = DATEADD(DAY,-30,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
	END
	ELSE IF @vcDueDate = 'Last60Day'
	BEGIN
		SET @dtFromDate = DATEADD(DAY,-60,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
	END
	ELSE IF @vcDueDate = 'Last90Day'
	BEGIN
		SET @dtFromDate = DATEADD(DAY,-90,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
	END
	ELSE IF @vcDueDate = 'Last120Day'
	BEGIN
		SET @dtFromDate = DATEADD(DAY,-120,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
	END

	DECLARE @TodayDate DATE
	DECLARE @CreatedDate DATE
	SET @TodayDate = DATEADD(MINUTE,@ClientTimeZoneOffset,GETUTCDATE())

	IF @vcTimeLine = 'Last12Months'
	BEGIN
		SET @CreatedDate = DATEADD(MONTH, -12, @TodayDate)
	END
	IF @vcTimeLine = 'Last6Months'
	BEGIN
		SET @CreatedDate = DATEADD(MONTH, -6, @TodayDate)
	END
	IF @vcTimeLine = 'Last3Months'
	BEGIN
		SET @CreatedDate = DATEADD(MONTH, -3, @TodayDate)
	END
	IF @vcTimeLine = 'Last30Days'
	BEGIN
		SET @CreatedDate = DATEADD(DAY, -30, @TodayDate)
	END
	IF @vcTimeLine = 'Last7Days'
	BEGIN
		SET @CreatedDate = DATEADD(DAY, -7, @TodayDate)
	END
	

	SELECT DISTINCT TOP (@tintQtyToDisplay)
		OM.vcPOppName
		,CONCAT('~/opportunity/frmOpportunities.aspx?opId=',OM.numOppId) AS URL
		,ISNULL(monTotAmount,0) monTotAmount
		,ISNULL(OM.numPercentageComplete,0) numPercentageComplete
	FROM
		OpportunityItems OI
	INNER JOIN
		OpportunityMaster OM
	ON
		OI.numOppId = OM.numOppID
	INNER JOIN
		DivisionMaster DM
	ON
		OM.numDivisionId = DM.numDivisionID
	LEFT JOIN 
		ProjectProgress 
	ON 
		OM.numOppId=ProjectProgress.numOppId
	LEFT JOIN
		ListDetails LD
	ON
		LD.numListID=50 
		AND (ISNULL(LD.constFlag,0)=1 OR LD.numDomainID=@numDomainID)
		AND LD.numListItemID = OM.numPercentageComplete
	WHERE
		OM.numDomainId=@numDomainID
		AND ISNULL(OM.tintOppType,0)=@tintOppType
		AND ISNULL(OM.tintOppStatus,0)=0
		AND (1 = (CASE WHEN (@tintTotalProgress) > 0 THEN (CASE WHEN ISNULL(ProjectProgress.intTotalProgress,0) >= @tintTotalProgress  THEN 1 ELSE 0 END) ELSE 1 END))
		AND	(1= (CASE WHEN OM.monDealAmount BETWEEN @tintMinNumber AND @tintMaxNumber THEN 1 ELSE 0 END))
		AND OM.intPEstimatedCloseDate IS NOT NULL
		--AND OM.intPEstimatedCloseDate < DateAdd(minute, -@ClientTimeZoneOffset,GETUTCDATE())
		AND (CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,OM.intPEstimatedCloseDate) AS DATE) BETWEEN @dtFromDate AND @dtToDate )
		AND OM.bintCreatedDate >= @CreatedDate
		AND 1 = (CASE WHEN LEN(ISNULL(@vcTeritorry,'')) > 0 THEN (CASE WHEN DM.numTerID IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END) ELSE 1 END)
		AND 1 = (CASE 
					WHEN LEN(ISNULL(@vcFilterValue,0)) > 0 AND @vcFilterBy IN (1,2,3)
					THEN 
						(CASE @vcFilterBy
							WHEN 1  -- Assign To
							THEN (CASE WHEN OM.numAssignedTo IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)
							WHEN 2  -- Record Owner
							THEN (CASE WHEN OM.numRecOwner IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)
							WHEN 3  -- Teams (Based on Assigned To)
							THEN (CASE WHEN OM.numAssignedTo IN (SELECT numUserCntID FROM UserTeams WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END)
						END)
					ELSE 1 
				END)
		AND ( 1 = (CASE WHEN CHARINDEX('1-5K',@vcDealAmount) > 0 THEN (CASE WHEN OM.monDealAmount BETWEEN 1000 AND 5000 THEN 1 ELSE 0 END) ELSE 0 END)
				OR 1 = (CASE WHEN CHARINDEX('5-10K',@vcDealAmount) > 0 THEN (CASE WHEN OM.monDealAmount BETWEEN 5000 AND 10000 THEN 1 ELSE 0 END) ELSE 0 END)
				OR 1 = (CASE WHEN CHARINDEX('10-20K',@vcDealAmount) > 0 THEN (CASE WHEN OM.monDealAmount BETWEEN 10000 AND 20000 THEN 1 ELSE 0 END) ELSE 0 END)
				OR 1 = (CASE WHEN CHARINDEX('20-50K',@vcDealAmount) > 0 THEN (CASE WHEN OM.monDealAmount BETWEEN 20000 AND 50000 THEN 1 ELSE 0 END) ELSE 0 END)
				OR 1 = (CASE WHEN CHARINDEX('>50K',@vcDealAmount) > 0 THEN (CASE WHEN OM.monDealAmount > 50000 THEN 1 ELSE 0 END) ELSE 0 END))
			
	ORDER BY
		ISNULL(monTotAmount,0) DESC
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10SalesOpportunityByRevenue')
DROP PROCEDURE USP_ReportListMaster_Top10SalesOpportunityByRevenue
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10SalesOpportunityByRevenue]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@numRecordCount NUMERIC(18,0)
	,@vcTimeLine VARCHAR(50)
	,@vcDealAmount VARCHAR(MAX)
AS
BEGIN 

	DECLARE @TodayDate DATE
	DECLARE @CreatedDate DATE
	SET @TodayDate = DATEADD(MINUTE,@ClientTimeZoneOffset,GETUTCDATE())

	IF @vcTimeLine = 'Last12Months'
	BEGIN
		SET @CreatedDate = DATEADD(MONTH, -12, @TodayDate)
	END
	IF @vcTimeLine = 'Last6Months'
	BEGIN
		SET @CreatedDate = DATEADD(MONTH, -6, @TodayDate)
	END
	IF @vcTimeLine = 'Last3Months'
	BEGIN
		SET @CreatedDate = DATEADD(MONTH, -3, @TodayDate)
	END
	IF @vcTimeLine = 'Last30Days'
	BEGIN
		SET @CreatedDate = DATEADD(DAY, -30, @TodayDate)
	END
	IF @vcTimeLine = 'Last7Days'
	BEGIN
		SET @CreatedDate = DATEADD(DAY, -7, @TodayDate)
	END
	

	SELECT TOP 10
		OM.vcPOppName
		,CONCAT('~/opportunity/frmOpportunities.aspx?opId=',OM.numOppId) AS URL
		,ISNULL(monTotAmount,0) monTotAmount
		,ISNULL(OM.numPercentageComplete,0) numPercentageComplete
	FROM
		OpportunityItems OI
	INNER JOIN
		OpportunityMaster OM
	ON
		OI.numOppId = OM.numOppID
	LEFT JOIN
		ListDetails LD
	ON
		--LD.numListID=50 
		LD.numListID=(@numRecordCount) 
		AND (ISNULL(LD.constFlag,0)=1 OR LD.numDomainID=@numDomainID)
		AND LD.numListItemID = OM.numPercentageComplete
	WHERE
		OM.numDomainId=@numDomainID
		AND ISNULL(OM.tintOppType,0)=1
		AND ISNULL(OM.tintOppStatus,0)=0
		--AND OM.bintCreatedDate >= DATEADD(MONTH, -12, GETUTCDATE())
		AND OM.bintCreatedDate >= @CreatedDate
		AND ( 1 = (CASE WHEN CHARINDEX('1-5K',@vcDealAmount) > 0 THEN (CASE WHEN monDealAmount BETWEEN 1000 AND 5000 THEN 1 ELSE 0 END) ELSE 0 END)
		OR 1 = (CASE WHEN CHARINDEX('5-10K',@vcDealAmount) > 0 THEN (CASE WHEN monDealAmount BETWEEN 5000 AND 10000 THEN 1 ELSE 0 END) ELSE 0 END)
		OR 1 = (CASE WHEN CHARINDEX('10-20K',@vcDealAmount) > 0 THEN (CASE WHEN monDealAmount BETWEEN 10000 AND 20000 THEN 1 ELSE 0 END) ELSE 0 END)
		OR 1 = (CASE WHEN CHARINDEX('20-50K',@vcDealAmount) > 0 THEN (CASE WHEN monDealAmount BETWEEN 20000 AND 50000 THEN 1 ELSE 0 END) ELSE 0 END)
		OR 1 = (CASE WHEN CHARINDEX('>50K',@vcDealAmount) > 0 THEN (CASE WHEN monDealAmount > 50000 THEN 1 ELSE 0 END) ELSE 0 END))
	ORDER BY
		ISNULL(monTotAmount,0) DESC
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_TopSourceOfSalesOrder')
DROP PROCEDURE USP_ReportListMaster_TopSourceOfSalesOrder
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_TopSourceOfSalesOrder]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@vcDealAmount VARCHAR(MAX)
AS
BEGIN 
	DECLARE @TotalSalesOrderCount AS NUMERIC(18,4)

	SELECT DISTINCT
		@TotalSalesOrderCount = COUNT(*)
	FROM 
		OpportunityMaster
	WHERE 
		numDomainId=@numDomainID 
		AND tintOppType=1 
		AND tintOppStatus=1 
		AND bintCreatedDate >= DATEADD(MONTH, -12, GETUTCDATE())

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,tintSource VARCHAR(100)
		,bitPartner BIT
		,numPartner NUMERIC(18,0)
		,vcSource VARCHAR(300)
		,TotalOrdersCount INT
		,TotalOrdersPercent NUMERIC(18,2)
		,TotalOrdersAmount DECIMAL(20,5)
	)

	INSERT INTO @TEMP
	(
		tintSource
		,vcSource
		,bitPartner
		,numPartner
	)
	SELECT
		'0~0'
		,'-'
		,0
		,0
	UNION
	SELECT 
		'0~1'
		,'Internal Order'
		,0
		,0
	UNION
	SELECT 
		CONCAT(numSiteID,'~2')
		,vcSiteName
		,0
		,0
	FROM 
		Sites 
	WHERE 
		numDomainID = @numDomainID
	UNION
	SELECT DISTINCT 
		CONCAT(WebApiId,'~3')
		,vcProviderName
		,0
		,0
	FROM 
		dbo.WebAPI
	UNION
	SELECT 
		CONCAT(Ld.numListItemID,'~1')
		,ISNULL(vcRenamedListName,vcData)
		,0
		,0 
	FROM 
		ListDetails Ld  
	LEFT JOIN 
		listorder LO 
	ON 
		Ld.numListItemID= LO.numListItemID 
		AND Lo.numDomainId = @numDomainID 
	WHERE 
		Ld.numListID=9 
		AND (constFlag=1 OR Ld.numDomainID=@numDomainID)  
	UNION
	SELECT DISTINCT
		''
		,''
		,1
		,numPartner
	FROM 
		OpportunityMaster
	WHERE 
		numDomainId=@numDomainID 
		AND tintOppType=1 
		AND tintOppStatus=1
		AND ISNULL(numPartner,0) > 0
		AND bintCreatedDate >= DATEADD(MONTH, -12, GETUTCDATE())

	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT
	DECLARE @tintSource AS VARCHAR(100)
	DECLARE @bitPartner BIT
	DECLARE @numPartner NUMERIC(18,0)
	DECLARE @TotalSalesOrderBySource AS INT
	DECLARE @TotalSalesOrderAmountBySource AS DECIMAL(20,5)


	SELECT @iCount = COUNT(*) FROM @TEMP

	WHILE @i <= @iCount
	BEGIN
		SELECT @tintSource=ISNULL(tintSource,''),@bitPartner=bitPartner,@numPartner=numPartner FROM @TEMP WHERE ID=@i

		SELECT 
			@TotalSalesOrderBySource = COUNT(numOppId)
		FROM 
			OpportunityMaster
		WHERE 
			numDomainId=@numDomainID 
			AND tintOppType=1
			AND tintOppStatus=1
			AND 1 = (CASE 
						WHEN @bitPartner=1 
						THEN CASE WHEN numPartner=@numPartner THEN 1 ELSE 0 END
						ELSE CASE WHEN CONCAT(ISNULL(tintSource,0),'~',ISNULL(tintSourceType,0))=@tintSource AND ISNULL(numPartner,0)=0 THEN 1 ELSE 0 END
					END)
			AND bintCreatedDate >= DATEADD(MONTH, -12, GETUTCDATE())

		SELECT 
			@TotalSalesOrderAmountBySource = SUM(monTotAmount)
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE 
			OM.numDomainId=@numDomainID 
			AND I.numDomainID = @numDomainID
			AND tintOppType=1 
			AND tintOppStatus=1
			AND 1 = (CASE 
						WHEN @bitPartner=1 
						THEN CASE WHEN numPartner=@numPartner THEN 1 ELSE 0 END
						ELSE CASE WHEN CONCAT(ISNULL(tintSource,0),'~',ISNULL(tintSourceType,0))=@tintSource AND ISNULL(numPartner,0)=0 THEN 1 ELSE 0 END
					END)
			AND OM.bintCreatedDate >= DATEADD(MONTH, -12, GETUTCDATE())
			--
			AND ( 1 = (CASE WHEN CHARINDEX('1-5K',@vcDealAmount) > 0 THEN (CASE WHEN monDealAmount BETWEEN 1000 AND 5000 THEN 1 ELSE 0 END) ELSE 0 END)
					OR 1 = (CASE WHEN CHARINDEX('5-10K',@vcDealAmount) > 0 THEN (CASE WHEN monDealAmount BETWEEN 5000 AND 10000 THEN 1 ELSE 0 END) ELSE 0 END)
					OR 1 = (CASE WHEN CHARINDEX('10-20K',@vcDealAmount) > 0 THEN (CASE WHEN monDealAmount BETWEEN 10000 AND 20000 THEN 1 ELSE 0 END) ELSE 0 END)
					OR 1 = (CASE WHEN CHARINDEX('20-50K',@vcDealAmount) > 0 THEN (CASE WHEN monDealAmount BETWEEN 20000 AND 50000 THEN 1 ELSE 0 END) ELSE 0 END)
					OR 1 = (CASE WHEN CHARINDEX('>50K',@vcDealAmount) > 0 THEN (CASE WHEN monDealAmount > 50000 THEN 1 ELSE 0 END) ELSE 0 END))

		UPDATE
			@TEMP
		SET
			vcSource = (CASE 
						WHEN @bitPartner=1 
						THEN ISNULL((SELECT CI.vcCompanyName FROM DivisionMaster DM INNER JOIN CompanyInfo CI ON DM.numCompanyID=CI.numCompanyId WHERE DM.numDomainID=@numDomainID AND numDivisionID=@numPartner),'-')
						ELSE vcSource
					END)
			,TotalOrdersPercent = (@TotalSalesOrderBySource * 100)/ISNULL(@TotalSalesOrderCount,1)
			,TotalOrdersAmount = ISNULL(@TotalSalesOrderAmountBySource,0)
			,TotalOrdersCount = @TotalSalesOrderBySource
		WHERE
			ID=@i

		SET @i = @i + 1
	END

	SELECT vcSource,TotalOrdersPercent,TotalOrdersAmount FROM @TEMP WHERE ISNULL(TotalOrdersPercent,0) > 0 ORDER BY TotalOrdersPercent DESC
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UserMaster_ChangeDashboardTemplate')
DROP PROCEDURE USP_UserMaster_ChangeDashboardTemplate
GO
CREATE PROCEDURE [dbo].[USP_UserMaster_ChangeDashboardTemplate]
	@numDomainID NUMERIC(18,0)
	,@numUserID AS NUMERIC(18,0)
	,@numTemplateID AS NUMERIC(18,0)
	,@vcDashboardTemplateIDs AS VARCHAR(250) = ''
AS
BEGIN 

IF @vcDashboardTemplateIDs IS NULL OR @vcDashboardTemplateIDs = ''
	BEGIN
		UPDATE 
			UserMaster
		SET
			numDashboardTemplateID=@numTemplateID
		WHERE
			numUserId=@numUserID
	END
ELSE
	BEGIN
		UPDATE 
			UserMaster
		SET
			numDashboardTemplateID=@numTemplateID,
			vcDashboardTemplateIDs=@vcDashboardTemplateIDs
		WHERE
			numUserId=@numUserID
	END
	
	IF ISNULL(@numTemplateID,0) > 0 and (@vcDashboardTemplateIDs IS NOT NULL OR @vcDashboardTemplateIDs <> '')
	BEGIN
		-- DELETE OLD DASHBOARD CONFIGURATION FOR USER BECAUSE IT MAY BE CHANGED FROM LAST ASSIGNMENT 
		DELETE FROM ReportDashboard WHERE numDomainID=@numDomainID AND numDashboardTemplateID=@numTemplateID AND numUserCntID=ISNULL((SELECT numUserDetailId FROM UserMaster WHERE numUserId=@numUserID),0)

		-- INSERT LATEST CONFIGURATION OF DASHBOARD TEMPLATE
		INSERT INTO [dbo].[ReportDashboard]
		(
			numDomainID
			,numReportID
			,numUserCntID
			,tintReportType
			,tintChartType
			,vcHeaderText
			,vcFooterText
			,tintRow
			,tintColumn
			,vcXAxis
			,vcYAxis
			,vcAggType
			,tintReportCategory
			,intHeight
			,intWidth
			,numDashboardTemplateID
		)
		SELECT
			DashboardTemplateReports.numDomainID
			,DashboardTemplateReports.numReportID
			,ISNULL((SELECT numUserDetailId FROM UserMaster WHERE numUserId=@numUserID),0)
			,DashboardTemplateReports.tintReportType
			,tintChartType
			,vcHeaderText
			,vcFooterText
			,tintRow
			,tintColumn
			,vcXAxis
			,vcYAxis
			,vcAggType
			,tintReportCategory
			,intHeight
			,intWidth
			,numDashboardTemplateID
		FROM
			DashboardTemplateReports
		INNER JOIN
			ReportListMaster
		ON
			DashboardTemplateReports.numReportID=ReportListMaster.numReportID
			AND (ReportListMaster.numDomainID=@numDomainID OR bitDefault=1)
		WHERE
			DashboardTemplateReports.numDomainID=@numDOmainID
			AND numDashboardTemplateID=@numTemplateID
	END
END
GO
