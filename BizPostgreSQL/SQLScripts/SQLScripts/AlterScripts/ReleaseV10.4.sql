/******************************************************************
Project: Release 10.4 Date: 29.OCT.2018
Comments: ALTER SCRIPTS
*******************************************************************/

/******************************************** SANDEEP *********************************************/

ALTER TABLE WorkOrder ADD numOppChildItemID NUMERIC(18,0) 
ALTER TABLE WorkOrder ADD numOppKitChildItemID NUMERIC(18,0) 

ALTER TABLE Item ADD bitSOWorkOrder BIT DEFAULT 0
ALTER TABLE Item ADD bitKitSingleSelect BIT DEFAULT 0

UPDATE Item SET bitKitSingleSelect=0 WHERE bitKitParent=1