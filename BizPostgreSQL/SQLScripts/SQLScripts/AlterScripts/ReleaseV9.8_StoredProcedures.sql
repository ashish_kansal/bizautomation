/******************************************************************
Project: Release 9.8 Date: 06.JUN.2018
Comments: STORE PROCEDURES
*******************************************************************/

/****** Object:  UserDefinedFunction [dbo].[fn_GetKitInventoryByWarehouse]    Script Date: 05/30/2014  ******/
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'fn'
                    AND NAME = 'fn_GetKitAvailabilityByWarehouse' ) 
    DROP FUNCTION fn_GetKitAvailabilityByWarehouse
GO
CREATE FUNCTION [dbo].[fn_GetKitAvailabilityByWarehouse]
(
    @numKitId NUMERIC(18,0),
    @numWarehouseID NUMERIC(18,0)
)
RETURNS FLOAT
AS BEGIN          
    DECLARE @fltAvailable AS FLOAT = 0;     
    
	WITH CTE(numItemKitID, numItemCode, numWarehouseItmsID, numQtyItemsReq, numCalculatedQty) AS 
	( 
		SELECT   
			CONVERT(NUMERIC(18,0),0),
			numItemCode,
            ISNULL(Dtl.numWareHouseItemId, 0) AS numWarehouseItmsID,
            DTL.numQtyItemsReq,
            CAST(DTL.numQtyItemsReq AS NUMERIC(9, 0)) AS numCalculatedQty
        FROM
			Item
        INNER JOIN 
			ItemDetails Dtl 
		ON 
			numChildItemID = numItemCode
        WHERE 
			numItemKitID = @numKitId
        UNION ALL
        SELECT 
			Dtl.numItemKitID,
            i.numItemCode,
            ISNULL(Dtl.numWareHouseItemId, 0) AS numWarehouseItmsID,
            DTL.numQtyItemsReq,
            CAST(( DTL.numQtyItemsReq * c.numCalculatedQty ) AS NUMERIC(9, 0)) AS numCalculatedQty
        FROM 
			Item i
        INNER JOIN 
			ItemDetails Dtl 
		ON 
			Dtl.numChildItemID = i.numItemCode
        INNER JOIN 
			CTE c 
		ON 
			Dtl.numItemKitID = c.numItemCode
        WHERE    
			Dtl.numChildItemID != @numKitId
    )
    
	SELECT  
		@fltAvailable = CAST(FLOOR(MIN(CASE 
									WHEN ISNULL(numOnHand,0) + ISNULL(numAllocation,0) = 0 THEN 0
                                    WHEN ISNULL(numOnHand,0) + ISNULL(numAllocation,0) >= numQtyItemsReq AND numQtyItemsReq > 0 THEN (ISNULL(numOnHand,0) + ISNULL(numAllocation,0)) / numQtyItemsReq
                                    ELSE 0
                                    END)
						) AS FLOAT)
    FROM    
		cte c
    LEFT OUTER JOIN 
		[WareHouseItems] WI 
	ON 
		WI.[numItemID] = c.numItemCode
		AND WI.[numWareHouseItemID] = c.numWarehouseItmsID
    LEFT OUTER JOIN 
		Warehouses W 
	ON 
		W.numWareHouseID = WI.numWareHouseID
    WHERE 
		WI.numWareHouseID = @numWarehouseID
        
    RETURN @fltAvailable         
          
   END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='tf'AND NAME ='fn_ItemPriceLevelPrice')
DROP FUNCTION fn_ItemPriceLevelPrice
GO
CREATE FUNCTION [dbo].[fn_ItemPriceLevelPrice] 
(
	@numItemCode NUMERIC(18,0)
	,@monListPrice DECIMAL(20,5)
	,@monVendorCost DECIMAL(20,5)
)
RETURNS @Result TABLE     
(    
	vcPriceLevelType VARCHAR(300),
	vcDiscountType VARCHAR(300),
	monPriceLevel1 VARCHAR(300),
	monPriceLevel2 VARCHAR(300),
	monPriceLevel3 VARCHAR(300),
	monPriceLevel4 VARCHAR(300),
	monPriceLevel5 VARCHAR(300),
	monPriceLevel6 VARCHAR(300),
	monPriceLevel7 VARCHAR(300),
	monPriceLevel8 VARCHAR(300),
	monPriceLevel9 VARCHAR(300),
	monPriceLevel10 VARCHAR(300),
	monPriceLevel11 VARCHAR(300),
	monPriceLevel12 VARCHAR(300),
	monPriceLevel13 VARCHAR(300),
	monPriceLevel14 VARCHAR(300),
	monPriceLevel15 VARCHAR(300),
	monPriceLevel16 VARCHAR(300),
	monPriceLevel17 VARCHAR(300),
	monPriceLevel18 VARCHAR(300),
	monPriceLevel19 VARCHAR(300),
	monPriceLevel20 VARCHAR(300)
)    
AS
BEGIN
	
	INSERT INTO @Result
	(
		monPriceLevel1,
		monPriceLevel2,
		monPriceLevel3,
		monPriceLevel4,
		monPriceLevel5,
		monPriceLevel6,
		monPriceLevel7,
		monPriceLevel8,
		monPriceLevel9,
		monPriceLevel10,
		monPriceLevel11,
		monPriceLevel12,
		monPriceLevel13,
		monPriceLevel14,
		monPriceLevel15,
		monPriceLevel16,
		monPriceLevel17,
		monPriceLevel18,
		monPriceLevel19,
		monPriceLevel20
	)
	SELECT 
		ISNULL(DestinationTable.monPriceLevel1,''),
		ISNULL(DestinationTable.monPriceLevel2,''),
		ISNULL(DestinationTable.monPriceLevel3,''),
		ISNULL(DestinationTable.monPriceLevel4,''),
		ISNULL(DestinationTable.monPriceLevel5,''),
		ISNULL(DestinationTable.monPriceLevel6,''),
		ISNULL(DestinationTable.monPriceLevel7,''),
		ISNULL(DestinationTable.monPriceLevel8,''),
		ISNULL(DestinationTable.monPriceLevel9,''),
		ISNULL(DestinationTable.monPriceLevel10,''),
		ISNULL(DestinationTable.monPriceLevel11,''),
		ISNULL(DestinationTable.monPriceLevel12,''),
		ISNULL(DestinationTable.monPriceLevel13,''),
		ISNULL(DestinationTable.monPriceLevel14,''),
		ISNULL(DestinationTable.monPriceLevel15,''),
		ISNULL(DestinationTable.monPriceLevel16,''),
		ISNULL(DestinationTable.monPriceLevel17,''),
		ISNULL(DestinationTable.monPriceLevel18,''),
		ISNULL(DestinationTable.monPriceLevel19,''),
		ISNULL(DestinationTable.monPriceLevel20,'')
	FROM
	(
		SELECT
			CONCAT('monPriceLevel',Id) Id
			,CASE 
				WHEN tintRuleType=1 AND tintDiscountType=1 --Deduct from List price & Percentage
				THEN CONCAT(CAST(ISNULL(@monListPrice,0) - (ISNULL(@monListPrice,0) * (decDiscount /100)) AS DECIMAL(20,5)),' (',intFromQty,'-',intToQty,')')
				WHEN tintRuleType=1 AND tintDiscountType=2 --Deduct from List price & Flat discount
				THEN CONCAT(CAST(ISNULL(@monListPrice,0) - decDiscount AS DECIMAL(20,5)),' (',intFromQty,'-',intToQty,')')
				WHEN tintRuleType=1 AND tintDiscountType=3 --Deduct from List price & Named Price
				THEN CAST(decDiscount AS VARCHAR)
				WHEN tintRuleType=2 AND tintDiscountType=1 --Add to Primary Vendor Cost & Percentage
				THEN CONCAT(CAST(ISNULL(@monVendorCost,0) + (ISNULL(@monVendorCost,0) * (decDiscount /100)) AS DECIMAL(20,5)),' (',intFromQty,'-',intToQty,')')
				WHEN tintRuleType=2 AND tintDiscountType=2 --Add to Primary Vendor Cost & Flat discount
				THEN CONCAT(CAST(ISNULL(@monVendorCost,0) + decDiscount AS DECIMAL(20,5)),' (',intFromQty,'-',intToQty,')')
				WHEN tintRuleType=2 AND tintDiscountType=3 -- Add to Primary Vendor Cost & Named Price
				THEN CAST(decDiscount AS VARCHAR)
				WHEN tintRuleType=3 --Named Price
				THEN CAST(decDiscount AS VARCHAR)
			END AS monPrice
		FROM
		(
			SELECT 
				ROW_NUMBER() OVER(ORDER BY numPricingID) Id
				,numItemCode
				,[numPriceRuleID]
				,[intFromQty]
				,[intToQty]
				,[tintRuleType]
				,[tintDiscountType]
				,decDiscount
		
			FROM 
				PricingTable
			WHERE 
				PricingTable.numItemCode=@numItemCode
				AND ISNULL(numPriceRuleID,0) = 0
		) TEMP
	) SourceTable
	PIVOT 
	(
	 MIN(monPrice)
	 FOR Id
	 IN ([monPriceLevel1],[monPriceLevel2],[monPriceLevel3],[monPriceLevel4],[monPriceLevel5],[monPriceLevel6],[monPriceLevel7],[monPriceLevel8],[monPriceLevel9],[monPriceLevel10],[monPriceLevel11],[monPriceLevel12],[monPriceLevel13],[monPriceLevel14],[monPriceLevel15],[monPriceLevel16],[monPriceLevel17],[monPriceLevel18],[monPriceLevel19],[monPriceLevel20])
	) AS DestinationTable

	UPDATE 
		@Result
	SET
		vcPriceLevelType=ISNULL((SELECT TOP 1 
									(CASE tintRuleType 
										WHEN 1
										THEN 'Deduct From List Price'
										WHEN 2 
										THEN 'Add to Primary Vendor Cost'
										WHEN 3 
										THEN 'Named Price'
										ELSE ''
									END) 
								FROM 
									PricingTable 
								WHERE 
									numItemCode=@numItemCode
									AND ISNULL(numPriceRuleID,0) = 0),'')
		,vcDiscountType=ISNULL((SELECT TOP 1 
									(CASE 
										WHEN tintDiscountType=1 AND (tintRuleType=1 OR tintRuleType=2)
										THEN 'Percentage'
										WHEN tintDiscountType=2 AND (tintRuleType=1 OR tintRuleType=2)
										THEN 'Flat Amount'
										WHEN tintDiscountType=3 AND (tintRuleType=1 OR tintRuleType=2)
										THEN 'Named Price' 
										ELSE ''
									END)
								FROM 
									PricingTable 
								WHERE 
									numItemCode=@numItemCode
									AND ISNULL(numPriceRuleID,0) = 0),'')
	RETURN
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_createbizdocs')
DROP PROCEDURE usp_createbizdocs
GO
CREATE PROCEDURE [dbo].[USP_CreateBizDocs]                        
(                                              
 @numOppId as numeric(9)=null,                        
 @numBizDocId as numeric(9)=null,                        
 @numUserCntID as numeric(9)=null,                        
 @numOppBizDocsId as numeric(9)=NULL OUTPUT,
 @vcComments as varchar(1000),
 @bitPartialFulfillment as bit,
 @strBizDocItems as text='' ,
 @numShipVia as numeric(9),
 @vcTrackingURL as varchar(1000),
 @dtFromDate AS DATETIME,
 @numBizDocStatus as numeric(9) = 0,
 @bitRecurringBizDoc AS BIT = NULL,
 @numSequenceId AS VARCHAR(50) = '',
 @tintDeferred as tinyint=0,
 @monCreditAmount as DECIMAL(20,5)= 0 OUTPUT,
 @ClientTimeZoneOffset as int=0,
 @monDealAmount as DECIMAL(20,5)= 0 OUTPUT,
 @bitRentalBizDoc as bit=0,
 @numBizDocTempID as numeric(9)=0,
 @vcTrackingNo AS VARCHAR(500),
 @vcRefOrderNo AS VARCHAR(100),
 @OMP_SalesTaxPercent AS FLOAT = 0, --(joseph) Added to Get Online Marketplace Sales Tax Percentage
 @numFromOppBizDocsId AS NUMERIC(9)=0,
 @bitTakeSequenceId AS BIT=0,
 @bitAllItems AS BIT=0,
 @bitNotValidateBizDocFulfillment AS BIT=0,
 @fltExchangeRateBizDoc AS FLOAT=0,
 @bitRecur BIT = 0,
 @dtStartDate DATE = NULL,
 @dtEndDate DATE = NULL,
 @numFrequency SMALLINT = 0,
 @vcFrequency VARCHAR(20) = '',
 @numRecConfigID NUMERIC(18,0) = 0,
 @bitDisable bit = 0,
 @bitInvoiceForDeferred BIT = 0,
 @numSourceBizDocId Numeric(18,0)=0,
 @bitShippingBizDoc BIT=0,
 @vcVendorInvoiceName  VARCHAR(100) = NULL
)                        
AS 
BEGIN TRY
	DECLARE @hDocItem as INTEGER
	DECLARE @numDivisionID AS NUMERIC(9)
	DECLARE @numDomainID NUMERIC(9)
	DECLARE @tintOppType AS TINYINT	
	DECLARE @numFulfillmentOrderBizDocId NUMERIC(9);
	DECLARE @bitShippingGenerated BIT=0;

	IF ISNULL(@numFromOppBizDocsId,0) > 0
	BEGIN
		IF (SELECT 
				COUNT(*) 
			FROM 
			(
				SELECT
					OBDI.numOppItemID
					,OI.numUnitHour AS OrderedQty
					,OBDI.numUnitHour AS FromBizDocQty
					,OtherSameBizDocType.BizDocQty
				FROM
					OpportunityBizDocs OBD
				INNER JOIN
					OpportunityBizDocItems OBDI
				ON
					OBD.numOppBizDocsId = OBDI.numOppBizDocID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numoppitemtCode = OBDI.numOppItemID
				OUTER APPLY
				(
					SELECT
						SUM(OpportunityBizDocItems.numUnitHour) BizDocQty
					FROM
						OpportunityBizDocs
					INNER JOIN
						OpportunityBizDocItems
					ON
						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
					WHERE
						OpportunityBizDocs.numOppId = OBD.numOppId
						AND OpportunityBizDocItems.numOppItemID=OBDI.numOppItemID
						AND OpportunityBizDocs.numBizDocId = @numBizDocId
				) AS OtherSameBizDocType
				WHERE
					OBD.numOppBizDocsId = @numFromOppBizDocsId
			) X
			WHERE
				ISNULL(X.FromBizDocQty,0) > (ISNULL(X.OrderedQty,0) - ISNULL(X.BizDocQty,0))) > 0
		BEGIN
			-- IF SOMEONE HAS ALREADY CREATED OTHE SAME BIZDOC TYPE AND ENOUGHT QTY IS NOT LEFT FOR ALL ITEMS OF BIZDOC THAN BIZDOC CAN NOT BE CREATED AGAINST VENDOR INVOICE
			SELECT 0
		END
	END

	SET @numFulfillmentOrderBizDocId=296 
	    
	IF ISNULL(@fltExchangeRateBizDoc,0)=0
		SELECT @fltExchangeRateBizDoc=fltExchangeRate FROM OpportunityMaster where numOppID=@numOppId
	
	SELECT 
		@numDomainID=numDomainID,
		@numDivisionID=numDivisionID,
		@tintOppType=tintOppType 
	FROM 
		OpportunityMaster 
	WHERE 
		numOppID=@numOppId
	
	IF(@bitShippingBizDoc=1)
	BEGIN
		SET @bitShippingGenerated=1
	END
	ELSE
	BEGIN
	IF((SELECT COUNT(numBizDocId) FROM OpportunityBizDocs WHERE bitShippingGenerated=1 AND numOppId=@numOppId)=0)
	BEGIN
		IF((SELECT TOP 1 CAST(numDefaultSalesShippingDoc AS int) FROM Domain WHERE numDomainId=@numDomainID)=@numBizDocId)
		BEGIN
			SET @bitShippingGenerated=1
		END
	END
	END

	IF @numBizDocTempID=0
	BEGIN
		SELECT 
			@numBizDocTempID=numBizDocTempID 
		FROM 
			BizDocTemplate 
		WHERE 
			numBizDocId=@numBizDocId 
			AND numDomainID=@numDomainID and numOpptype=@tintOppType 
			AND tintTemplateType=0 
			AND isnull(bitDefault,0)=1 
			AND isnull(bitEnabled,0)=1
	END

	IF @numOppBizDocsId=0
	BEGIN
		IF (
			(@bitPartialFulfillment = 1 AND NOT EXISTS (SELECT * FROM OpportunityBizDocs WHERE numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 0))
			 OR (NOT EXISTS (SELECT * FROM OpportunityBizDocs WHERE numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 0)
				 AND NOT EXISTS (SELECT * FROM OpportunityBizDocs WHERE numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 1))
			 OR (@bitRecurringBizDoc = 1) -- Added new flag to add unit qty in case of recurring bizdoc
			 OR (@numFromOppBizDocsId > 0)
			)
		BEGIN                      
			DECLARE @tintShipped AS TINYINT	
			DECLARE @dtShipped AS DATETIME
			DECLARE @bitAuthBizdoc AS BIT
        
			SELECT 
				@tintShipped=ISNULL(tintShipped,0),
				@numDomainID=numDomainID 
			FROM 
				OpportunityMaster 
			WHERE numOppId=@numOppId                        
		
			IF @tintOppType = 1 AND @bitNotValidateBizDocFulfillment=0
				EXEC USP_ValidateBizDocFulfillment @numDomainID,@numOppId,@numFromOppBizDocsId,@numBizDocId
		

			IF @tintShipped =1 
				SET @dtShipped = GETUTCDATE();
			ELSE
				SET @dtShipped = null;
			IF @tintOppType = 1 AND (SELECT numAuthoritativeSales FROM [AuthoritativeBizDocs] WHERE numDomainID = @numDomainID) = @numBizDocId
				SET @bitAuthBizdoc = 1
			ELSE if @tintOppType = 2 AND (SELECT numAuthoritativePurchase FROM [AuthoritativeBizDocs] WHERE numDomainID = @numDomainID) = @numBizDocId
				SET @bitAuthBizdoc = 1
			ELSE 
				SET @bitAuthBizdoc = 0
		
			Declare @dtCreatedDate as datetime;SET @dtCreatedDate=Getutcdate()
			IF @tintDeferred=1
			BEGIn
				SET @dtCreatedDate=DateAdd(minute, @ClientTimeZoneOffset,@dtFromDate)
			END
	
			IF @bitTakeSequenceId=1
			BEGIN
				--Sequence #
				CREATE TABLE #tempSequence (numSequenceId bigint )
				INSERT INTO #tempSequence EXEC [dbo].[USP_GetScalerValue] @numDomainID ,33,@numBizDocId,0
				SELECT @numSequenceId =ISNULL(numSequenceId,0) FROM #tempSequence
				DROP TABLE #tempSequence
			
				--BizDoc Template ID
				CREATE TABLE #tempBizDocTempID (numBizDocTempID NUMERIC(9) )
				INSERT INTO #tempBizDocTempID EXEC [dbo].[USP_GetBizDocTemplateList] @numDomainID,@numBizDocId,1,1
				SELECT @numBizDocTempID =ISNULL(numBizDocTempID,0) FROM #tempBizDocTempID
				DROP TABLE #tempBizDocTempID
			END	
			
			--To insert Tracking Id of previous record into the new record
			IF @vcTrackingNo IS NULL OR @vcTrackingNo = ''
			BEGIN
				SELECT TOP 1 @vcTrackingNo = vcTrackingNo FROM OpportunityBizDocs WHERE numOppId = @numOppId ORDER BY numOppBizDocsId DESC 
			END
			INSERT INTO OpportunityBizDocs
			(
				numOppId
				,numBizDocId
				,numCreatedBy
				,dtCreatedDate
				,numModifiedBy
				,dtModifiedDate
				,vcComments
				,bitPartialFulfilment
				,numShipVia
				,vcTrackingURL
				,dtFromDate
				,numBizDocStatus
				,[dtShippedDate]
				,[bitAuthoritativeBizDocs]
				,tintDeferred
				,bitRentalBizDoc
				,numBizDocTempID
				,vcTrackingNo
				,vcRefOrderNo
				,numSequenceId
				,numBizDocStatusOLD
				,fltExchangeRateBizDoc
				,bitAutoCreated
				,[numMasterBizdocSequenceID]
				,bitShippingGenerated
				,numSourceBizDocId
				,vcVendorInvoice
			)
			VALUES     
			(
				@numOppId
				,@numBizDocId
				,@numUserCntID
				,@dtCreatedDate
				,@numUserCntID
				,GETUTCDATE()
				,@vcComments
				,@bitPartialFulfillment
				,@numShipVia
				,@vcTrackingURL
				,@dtFromDate
				,@numBizDocStatus
				,@dtShipped
				,ISNULL(@bitAuthBizdoc,0)
				,@tintDeferred
				,@bitRentalBizDoc
				,@numBizDocTempID
				,@vcTrackingNo
				,@vcRefOrderNo
				,@numSequenceId
				,0
				,@fltExchangeRateBizDoc
				,0
				,@numSequenceId
				,@bitShippingGenerated
				,@numSourceBizDocId
				,@vcVendorInvoiceName
			)
			
        
			SET @numOppBizDocsId = SCOPE_IDENTITY()
		
			--Added By:Sachin Sadhu||Date:27thAug2014
			--Purpose :MAke entry in WFA queue 
			IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 )
			BEGIN
				EXEC 
				USP_ManageOpportunityAutomationQueue			
				@numOppQueueID = 0, -- numeric(18, 0)							
				@numDomainID = @numDomainID, -- numeric(18, 0)							
				@numOppId = @numOppID, -- numeric(18, 0)							
				@numOppBizDocsId = @numOppBizDocsId, -- numeric(18, 0)							
				@numOrderStatus = 0, -- numeric(18, 0)					
				@numUserCntID = @numUserCntID, -- numeric(18, 0)					
				@tintProcessStatus = 1, -- tinyint						
				@tintMode = 1 -- TINYINT
			END 
		   --end of script

			--Deferred BizDocs : Create Recurring entry only for create Account Journal
			if @tintDeferred=1
			BEGIN
				exec USP_RecurringAddOpportunity @numOppId,@numOppBizDocsId,0,4,@dtCreatedDate,0,@numDomainID,0,0,100,''
			END

			-- Update name template set for bizdoc
			--DECLARE @tintType TINYINT
			--SELECT @tintType = CASE @tintOppType WHEN 1 THEN 3 WHEN 2 THEN 4 END;
			EXEC dbo.USP_UpdateBizDocNameTemplate @tintOppType,@numDomainID,@numOppBizDocsId

			IF @numFromOppBizDocsId>0
			BEGIN
				INSERT INTO OpportunityBizDocItems
  				(
					numOppBizDocID
					,numOppItemID
					,numItemCode
					,numUnitHour
					,monPrice
					,monTotAmount
					,vcItemDesc
					,numWarehouseItmsID
					,vcType
					,vcAttributes
					,bitDropShip
					,bitDiscountType
					,fltDiscount
					,monTotAmtBefDiscount
					,vcNotes
					,bitEmbeddedCost
					,monEmbeddedCost
					,dtRentalStartDate
					,dtRentalReturnDate
					,tintTrackingStatus
				)
				SELECT 
					@numOppBizDocsId
					,OBDI.numOppItemID
					,OBDI.numItemCode
					,OBDI.numUnitHour
					,OBDI.monPrice
					,OBDI.monTotAmount
					,OBDI.vcItemDesc
					,OBDI.numWarehouseItmsID
					,OBDI.vcType
					,OBDI.vcAttributes
					,OBDI.bitDropShip
					,OBDI.bitDiscountType
					,OBDI.fltDiscount
					,OBDI.monTotAmtBefDiscount
					,OBDI.vcNotes
					,OBDI.bitEmbeddedCost
					,OBDI.monEmbeddedCost
					,OBDI.dtRentalStartDate
					,OBDI.dtRentalReturnDate
					,OBDI.tintTrackingStatus
				FROM 
					OpportunityBizDocs OBD 
				JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID 
				JOIN OpportunityItems OI ON OBDI.numOppItemID=OI.numoppitemtCode
				JOIN Item I ON OBDI.numItemCode=I.numItemCode
				WHERE 
					OBD.numOppId=@numOppId 
					AND OBD.numOppBizDocsId=@numFromOppBizDocsId
			END
			ELSE IF (@bitPartialFulfillment=1 and DATALENGTH(@strBizDocItems)>2)
			BEGIN
				IF DATALENGTH(@strBizDocItems)>2
				BEGIN
					EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strBizDocItems 

						IF @bitRentalBizDoc=1
						BEGIN
							update OI set OI.monPrice=OBZ.monPrice,OI.monTotAmount=OBZ.monTotAmount,OI.monTotAmtBefDiscount=OBZ.monTotAmount
							from OpportunityItems OI
							Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
								WITH ( OppItemID numeric(9),Quantity FLOAT,monPrice DECIMAL(20,5),monTotAmount DECIMAL(20,5))) OBZ
							on OBZ.OppItemID=OI.numoppitemtCode
							where OBZ.OppItemID=OI.numoppitemtCode
						END
	
						insert into                       
					   OpportunityBizDocItems                                                                          
					   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes,/*vcTrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate)
				   
					   select @numOppBizDocsId,numoppitemtCode,numItemCode,Quantity,CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmount/numUnitHour)*Quantity END,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,
					   case when bitDiscountType=0 then fltDiscount when bitDiscountType=1 then (fltDiscount/numUnitHour)*Quantity else fltDiscount end ,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmtBefDiscount/numUnitHour)*Quantity END,Notes,/*TrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate  from OpportunityItems OI
					   Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
					   WITH                       
					   (OppItemID numeric(9),                               
						Quantity FLOAT,
						Notes varchar(500),
						monPrice DECIMAL(20,5),
						dtRentalStartDate datetime,dtRentalReturnDate datetime
						)) OBZ
					   on OBZ.OppItemID=OI.numoppitemtCode
					   where  numOppId=@numOppId AND ISNULL(OI.numUnitHour,0)>0 AND ISNULL(OI.numUnitHour,0)>0
				   
					EXEC sp_xml_removedocument @hDocItem 
				END
			END
			ELSE
			BEGIN
				IF @numBizDocId <> 304 AND @bitAuthBizdoc = 1 AND @bitInvoiceForDeferred = 1
				BEGIN
					DECLARE @numTop1DeferredBizDocID AS NUMERIC(18,0)

					SELECT TOP 1
						@numTop1DeferredBizDocID = 	OpportunityBizDocs.numOppBizDocsId
					FROM 
						OpportunityBizDocs 
					WHERE 
						numOppId=@numOppId 
						AND numOppBizDocsId NOT IN (SELECT ISNULL(numDeferredBizDocID,0) FROM OpportunityBizDocs WHERE numOppId=@numOppId)
						AND OpportunityBizDocs.numBizDocId = 304 -- 304 IS DEFERRED INVOICE BIZDOC
					ORDER BY
						numOppBizDocsId

					INSERT INTO OpportunityBizDocItems
  					(
						numOppBizDocID,
						numOppItemID,
						numItemCode,
						numUnitHour,
						monPrice,
						monTotAmount,
						vcItemDesc,
						numWarehouseItmsID,
						vcType,
						vcAttributes,
						bitDropShip,
						bitDiscountType,
						fltDiscount,
						monTotAmtBefDiscount,
						vcNotes
					)
					SELECT 
						@numOppBizDocsId,
						numoppitemtCode,
						OI.numItemCode,
						(CASE @bitRecurringBizDoc WHEN 1 THEN 1 ELSE ISNULL(TempBizDoc.numUnitHour,0) END) AS numUnitHour,
						OI.monPrice,
						(CASE @bitRecurringBizDoc 
							WHEN 1 THEN monPrice * 1 
							ELSE (OI.monTotAmount/OI.numUnitHour) * ISNULL(TempBizDoc.numUnitHour,0)
						END) AS monTotAmount,
						vcItemDesc,
						numWarehouseItmsID,
						vcType,vcAttributes,
						bitDropShip,
						bitDiscountType,
						(CASE 
							WHEN bitDiscountType=0 THEN fltDiscount 
							WHEN bitDiscountType=1 THEN ((fltDiscount/OI.numUnitHour) * ISNULL(TempBizDoc.numUnitHour,0))
							ELSE fltDiscount 
						END),
						(monTotAmtBefDiscount/OI.numUnitHour) * ISNULL(TempBizDoc.numUnitHour,0),
						OI.vcNotes
					FROM 
						OpportunityItems OI
					JOIN 
						[dbo].[Item] AS I 
					ON 
						I.[numItemCode] = OI.[numItemCode]
					INNER JOIN
						(
							SELECT 
								OpportunityBizDocItems.numOppItemID,
								OpportunityBizDocItems.numUnitHour
							FROM 
								OpportunityBizDocs 
							JOIN 
								OpportunityBizDocItems 
							ON 
								OpportunityBizDocs.numOppBizDocsId  = OpportunityBizDocItems.numOppBizDocId
							WHERE 
								numOppId=@numOppId 
								AND OpportunityBizDocs.numOppBizDocsId = @numTop1DeferredBizDocID
						) TempBizDoc
					ON
						TempBizDoc.numOppItemID = OI.numoppitemtCode
					WHERE  
						numOppId=@numOppId 
						AND ISNULL(OI.numUnitHour,0) > 0
						AND (@bitRecurringBizDoc = 1 OR ISNULL(TempBizDoc.numUnitHour,0) > 0)


					UPDATE OpportunityBizDocs SET numDeferredBizDocID = @numTop1DeferredBizDocID WHERE numOppBizDocsId = @numOppBizDocsId
				END
				ELSE
				BEGIN
		   			INSERT INTO OpportunityBizDocItems                                                                          
  					(
						numOppBizDocID,
						numOppItemID,
						numItemCode,
						numUnitHour,
						monPrice,
						monTotAmount,
						vcItemDesc,
						numWarehouseItmsID,
						vcType,
						vcAttributes,
						bitDropShip,
						bitDiscountType,
						fltDiscount,
						monTotAmtBefDiscount,
						vcNotes
					)
					SELECT 
						@numOppBizDocsId,
						numoppitemtCode,
						OI.numItemCode,
						(CASE @bitRecurringBizDoc 
							WHEN 1 
							THEN 1 
							ELSE (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0)) 
						END) AS numUnitHour,
						OI.monPrice,
						(CASE @bitRecurringBizDoc 
							WHEN 1 THEN monPrice * 1 
							ELSE (OI.monTotAmount/OI.numUnitHour) * (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0))
						END) AS monTotAmount,
						vcItemDesc,
						numWarehouseItmsID,
						vcType,vcAttributes,
						bitDropShip,
						bitDiscountType,
						(CASE 
							WHEN bitDiscountType=0 THEN fltDiscount 
							WHEN bitDiscountType=1 THEN ((fltDiscount/OI.numUnitHour) * (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0)))
							ELSE fltDiscount 
						END),
						(monTotAmtBefDiscount/OI.numUnitHour) * (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0)),
						OI.vcNotes
					FROM 
						OpportunityItems OI
					JOIN 
						[dbo].[Item] AS I 
					ON 
						I.[numItemCode] = OI.[numItemCode]
					OUTER APPLY
						(
							SELECT 
								SUM(OBDI.numUnitHour) AS numUnitHour
							FROM 
								OpportunityBizDocs OBD 
							JOIN 
								dbo.OpportunityBizDocItems OBDI 
							ON 
								OBDI.numOppBizDocId  = OBD.numOppBizDocsId
								AND OBDI.numOppItemID = OI.numoppitemtCode
							WHERE 
								numOppId=@numOppId 
								AND 1 = (CASE 
											WHEN @bitAuthBizdoc = 1 
											THEN (CASE WHEN (numBizDocId = 304 OR (numBizDocId = @numBizDocId AND ISNULL(numDeferredBizDocID,0) = 0)) THEN 1 ELSE 0 END)
											WHEN @numBizDocId = 304 --Deferred Income
											THEN (CASE WHEN (numBizDocId = @numBizDocId OR ISNULL(OBD.bitAuthoritativeBizDocs,0) = 1) THEN 1 ELSE 0 END)
											ELSE (CASE WHEN numBizDocId = @numBizDocId THEN 1 ELSE 0 END) 
										END)
						) TempBizDoc
					WHERE  
						numOppId=@numOppId 
						AND ISNULL(OI.numUnitHour,0) > 0
						AND (@bitRecurringBizDoc = 1 OR (ISNULL(OI.numUnitHour,0) - ISNULL(TempBizDoc.numUnitHour,0)) > 0)
				END
			END

			SET @numOppBizDocsID = @numOppBizDocsId
			--select @numOppBizDocsId
		END
		ELSE
		BEGIN
  			SET @numOppBizDocsId=0
			SET @monDealAmount = 0
			SET @monCreditAmount = 0
			RAISERROR ('NOT_ALLOWED',16,1);
		END
	END
	ELSE
	BEGIN
		UPDATE 
			OpportunityBizDocs
		SET    
			numModifiedBy = @numUserCntID,
			dtModifiedDate = GETUTCDATE(),
			vcComments = @vcComments,
			numShipVia = @numShipVia,
			vcTrackingURL = @vcTrackingURL,
			dtFromDate = @dtFromDate,
			numBizDocStatus = @numBizDocStatus,
			numSequenceId=@numSequenceId,
			numBizDocTempID=@numBizDocTempID,
			vcTrackingNo=@vcTrackingNo,
			vcRefOrderNo=@vcRefOrderNo,
			fltExchangeRateBizDoc=@fltExchangeRateBizDoc,
			bitShippingGenerated=@bitShippingGenerated,
			vcVendorInvoice=@vcVendorInvoiceName
		WHERE  
			numOppBizDocsId = @numOppBizDocsId

		--Added By:Sachin Sadhu||Date:27thAug2014
		--Purpose :MAke entry in WFA queue 
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 )
		BEGIN
			EXEC 
			USP_ManageOpportunityAutomationQueue			
			@numOppQueueID = 0, -- numeric(18, 0)							
			@numDomainID = @numDomainID, -- numeric(18, 0)							
			@numOppId = @numOppID, -- numeric(18, 0)							
			@numOppBizDocsId = @numOppBizDocsId, -- numeric(18, 0)							
			@numOrderStatus = 0, -- numeric(18, 0)					
			@numUserCntID = @numUserCntID, -- numeric(18, 0)					
			@tintProcessStatus = 1, -- tinyint						
			@tintMode = 1 -- TINYINT
		END 
		--end of code

   		IF DATALENGTH(@strBizDocItems)>2
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strBizDocItems 

            DELETE FROM 
				OpportunityBizDocItems  
			WHERE 
				numOppItemID NOT IN 
				(
					SELECT 
						OppItemID 
					FROM OPENXML 
						(@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
					WITH                       
					   (OppItemID numeric(9))
				)
				AND numOppBizDocID=@numOppBizDocsId  
                  
                     
			IF @bitRentalBizDoc=1
			BEGIN
				update OI set OI.monPrice=OBZ.monPrice,OI.monTotAmount=OBZ.monTotAmount,OI.monTotAmtBefDiscount=OBZ.monTotAmount
				from OpportunityItems OI
				Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
					WITH ( OppItemID numeric(9),Quantity FLOAT,monPrice DECIMAL(20,5),monTotAmount DECIMAL(20,5))) OBZ
				on OBZ.OppItemID=OI.numoppitemtCode
				where OBZ.OppItemID=OI.numoppitemtCode
			END

			update OBI set OBI.numUnitHour= Quantity,OBI.monPrice=CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,OBI.monTotAmount=CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (OI.monTotAmount/OI.numUnitHour)*Quantity END,OBI.vcNotes= Notes,
			OBI.fltDiscount=case when OI.bitDiscountType=0 then OI.fltDiscount when OI.bitDiscountType=1 then (OI.fltDiscount/OI.numUnitHour)*Quantity else OI.fltDiscount end ,
			OBI.monTotAmtBefDiscount=(OI.monTotAmtBefDiscount/OI.numUnitHour)*Quantity ,                                                                    
			/*OBI.vcTrackingNo=TrackingNo,OBI.vcShippingMethod=OBZ.vcShippingMethod,OBI.monShipCost=OBZ.monShipCost,OBI.dtDeliveryDate=OBZ.dtDeliveryDate*/
			OBI.dtRentalStartDate=OBZ.dtRentalStartDate,OBI.dtRentalReturnDate=OBZ.dtRentalReturnDate
			from OpportunityBizDocItems OBI
			join OpportunityItems OI
			on OBI.numOppItemID=OI.numoppitemtCode
			Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
			WITH                       
			( OppItemID numeric(9),                                     
			Quantity FLOAT,
			Notes varchar(500),
			monPrice DECIMAL(20,5),
			dtRentalStartDate datetime,dtRentalReturnDate datetime
			)) OBZ
			on OBZ.OppItemID=OI.numoppitemtCode
			where  OI.numOppId=@numOppId and OBI.numOppBizDocID=@numOppBizDocsId AND ISNULL(OI.numUnitHour,0)>0


            insert into                       
			OpportunityBizDocItems                                                                          
			(numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes/*,vcTrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate*/)                      
			select @numOppBizDocsId,numoppitemtCode,numItemCode,Quantity,CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmount/numUnitHour)*Quantity END,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,
			case when bitDiscountType=0 then fltDiscount when bitDiscountType=1 then (fltDiscount/numUnitHour)*Quantity else fltDiscount end ,(monTotAmtBefDiscount/numUnitHour)*Quantity,Notes/*,TrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate*/ from OpportunityItems OI
			Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
			WITH                       
			( OppItemID numeric(9),                                     
			Quantity FLOAT,
			Notes varchar(500),
			monPrice DECIMAL(20,5)
			--TrackingNo varchar(500),
			--vcShippingMethod varchar(100),
			--monShipCost DECIMAL(20,5),
			--dtDeliveryDate DATETIME
			)) OBZ
			on OBZ.OppItemID=OI.numoppitemtCode and OI.numoppitemtCode not in (select numOppItemID from OpportunityBizDocItems where numOppBizDocID =@numOppBizDocsId)
			where  numOppId=@numOppId AND ISNULL(OI.numUnitHour,0)>0
                 
			EXEC sp_xml_removedocument @hDocItem 
		END
	END

	IF @numOppBizDocsId>0
	BEGIN
		SELECT @monDealAmount=isnull(dbo.[GetDealAmount](@numOppId,GETUTCDATE(),@numOppBizDocsId),0)

		IF @numBizDocId = 297 OR @numBizDocId = 299
		BEGIN
			IF NOT EXISTS(SELECT * FROM dbo.NameTemplate WHERE numDomainID=@numDomainID AND numRecordID=@numBizDocId AND tintModuleID=2)
			BEGIN
				INSERT INTO NameTemplate
				(numDomainId,tintModuleID,numRecordID,vcNameTemplate,numSequenceId,numMinLength)
				SELECT 
					@numDomainId,2,@numBizDocId,UPPER(SUBSTRING(dbo.GetListIemName(@numBizDocId),0,4)) + '-',1,4

				SET @numSequenceId = 1
			END
			ELSE
			BEGIN
				SELECT @numSequenceId=numSequenceId FROM NameTemplate WHERE numDomainID=@numDomainID AND numRecordID=@numBizDocId AND tintModuleID=2
				UPDATE NameTemplate SET numSequenceId=ISNULL(numSequenceId,0) + 1 WHERE numDomainID=@numDomainID AND numRecordID=@numBizDocId AND tintModuleID=2
			END
		END

		UPDATE 
			OpportunityBizDocs 
		SET 
			vcBizDocID=vcBizDocName + ISNULL(@numSequenceId,''),
			numSequenceId = CASE WHEN (numBizDocId = 297 OR numBizDocId = 299) THEN @numSequenceId ELSE numSequenceId END,
			monDealAmount=@monDealAmount
		WHERE  
			numOppBizDocsId = @numOppBizDocsId
		
	END

	-- 1 if recurrence is enabled for authorative bizdoc
	IF ISNULL(@bitRecur,0) = 1
	BEGIN
		-- First Check if receurrece is already created for sales order because user should not 
		-- be able to create recurrence on both order and invoice
		IF (SELECT COUNT(*) FROM RecurrenceConfiguration WHERE numOppID = @numOppId AND numType=1) > 0 
		BEGIN
			RAISERROR ('RECURRENCE_ALREADY_CREATED_FOR_ORDER',16,1);
		END
		--User can not create recurrence on bizdoc where all items are added to bizdoc
		ELSE IF (SELECT dbo.CheckAllItemQuantityAddedToBizDoc(@numOppId,@numOppBizDocsID)) = 1
		BEGIN
			RAISERROR ('RECURRENCE_NOT_ALLOWED_ALL_ITEM_QUANTITY_ADDED_TO_INVOICE',16,1);
		END
		ELSE
		BEGIN

			EXEC USP_RecurrenceConfiguration_Insert
				@numDomainID = @numDomainId,
				@numUserCntID = @numUserCntId,
				@numOppID = @numOppID,
				@numOppBizDocID = @numOppBizDocsId,
				@dtStartDate = @dtStartDate,
				@dtEndDate =NULL,
				@numType = 2,
				@vcType = 'Invoice',
				@vcFrequency = @vcFrequency,
				@numFrequency = @numFrequency

			UPDATE OpportunityMaster SET vcRecurrenceType = 'Invoice' WHERE numOppId = @numOppID
		END
	END
	ELSE IF ISNULL(@numRecConfigID,0) > 0 AND ISNULL(@bitDisable,0)= 1
	BEGIN
		UPDATE
			RecurrenceConfiguration
		SET
			bitDisabled = 1,
			numDisabledBy = @numUserCntId,
			dtDisabledDate = GETDATE()
		WHERE	
			numRecConfigID = @numRecConfigID
	END 
 
END TRY
BEGIN CATCH
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return error
    -- information about the original error that caused
    -- execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
               );
END CATCH;
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON


GO

IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAttributesForSelectedItemGroup')
DROP PROCEDURE USP_GetAttributesForSelectedItemGroup
GO

CREATE PROCEDURE [dbo].[USP_GetAttributesForSelectedItemGroup]
	@numDomainID AS NUMERIC(18,0),
	@numListID AS NUMERIC(9)=0,  
	@numItemGroupID AS NUMERIC(9),     
	@numWareHouseID AS NUMERIC(9)=0    
AS
BEGIN
	SELECT numListItemID,vcData 
				FROM 
					ListDetails 
				WHERE 
					numlistitemid IN (
										SELECT 
											DISTINCT(fld_value) 
										FROM 
											Item I
										JOIN
											WareHouseItems W      
										ON
											I.numItemCode=W.numItemID
										JOIN 
											ItemAttributes
										ON 
											I.numItemCode=ItemAttributes.numItemCode      
										JOIN 
											CFW_Fld_Master M 
										ON 
											ItemAttributes.Fld_ID=M.Fld_ID                             
										WHERE 
											I.numDomainID = @numDomainID
											AND W.numDomainID = @numDomainID
											AND M.numListID=@numListID 
											--AND bitMatrix = 0
											AND numItemGroup = @numItemGroupID
											AND (W.numWareHouseID=@numWareHouseID OR ISNULL(@numWareHouseID,0)=0)
									)      
END
/****** Object:  StoredProcedure [dbo].[USP_GetColumnConfiguration1]    Script Date: 07/26/2008 16:16:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getcolumnconfiguration1' )
    DROP PROCEDURE usp_getcolumnconfiguration1
GO
CREATE PROCEDURE [dbo].[USP_GetColumnConfiguration1]
    @numDomainID NUMERIC(9) ,
    @numUserCntId NUMERIC(9) ,
    @FormId TINYINT ,
    @numtype NUMERIC(9) ,
    @numViewID NUMERIC(9) = 0
AS
    DECLARE @PageId AS TINYINT         
    IF @FormId = 10 OR @FormId = 44
        SET @PageId = 4          
    ELSE IF @FormId = 11 OR @FormId = 34 OR @FormId = 35 OR @FormId = 36 OR @FormId = 96 OR @FormId = 97
            SET @PageId = 1          
    ELSE IF @FormId = 12
                SET @PageId = 3          
    ELSE IF @FormId = 13
                    SET @PageId = 11       
    ELSE IF (@FormId = 14 AND (@numtype = 1 OR @numtype = 3)) OR @FormId = 33 OR @FormId = 38 OR @FormId = 39 OR @FormId = 23
                        SET @PageId = 2          
    ELSE IF @FormId = 14 AND (@numtype = 2 OR @numtype = 4) OR @FormId = 40 OR @FormId = 41
                            SET @PageId = 6  
    ELSE IF @FormId = 21 OR @FormId = 26 OR @FormId = 32 OR @FormId = 126 OR @FormId = 129
                                SET @PageId = 5   
    ELSE IF @FormId = 22 OR @FormId = 30
                                    SET @PageId = 5    
    ELSE IF @FormId = 76
                                        SET @PageId = 10 
    ELSE IF @FormId = 84 OR @FormId = 85
                                            SET @PageId = 5

              
    IF @PageId = 1 OR @PageId = 4
        BEGIN          
            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                    0 AS Custom
            FROM    View_DynamicDefaultColumns
            WHERE   numFormID = @FormId
                    AND ISNULL(bitSettingField, 0) = 1
                    AND ISNULL(bitDeleted, 0) = 0
                    AND numDomainID = @numDomainID
            UNION
            SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                    fld_label AS vcFieldName ,
                    1 AS Custom
            FROM    CFW_Fld_Master
                    LEFT JOIN CFW_Fld_Dtl ON Fld_id = numFieldId
                                             AND numRelation = CASE
                                                              WHEN @numtype IN (
                                                              1, 2, 3 )
                                                              THEN numRelation
                                                              ELSE @numtype
                                                              END
                    LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
            WHERE   CFW_Fld_Master.grp_id = @PageId
                    AND CFW_Fld_Master.numDomainID = @numDomainID
                    AND Fld_type <> 'Link'
            ORDER BY Custom ,
                    vcFieldName                                       
        END  
    ELSE IF @PageId = 10
    BEGIN 
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        0 AS Custom
                FROM    View_DynamicDefaultColumns
                WHERE   numFormID = @FormId
                        AND ISNULL(bitSettingField, 0) = 1
                        AND ISNULL(bitDeleted, 0) = 0
                        AND numDomainID = @numDomainID
                UNION
                SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                        fld_label AS vcFieldName ,
                        1 AS Custom
                FROM    CFW_Fld_Master
                        LEFT JOIN CFW_Fld_Dtl ON Fld_id = numFieldId
                                                 AND numRelation = CASE
                                                              WHEN @numtype IN (
                                                              1, 2, 3 )
                                                              THEN numRelation
                                                              ELSE @numtype
                                                              END
                        LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
                WHERE   CFW_Fld_Master.grp_id = 5
                        AND CFW_Fld_Master.numDomainID = @numDomainID
                        AND Fld_Type = 'SelectBox'
                ORDER BY Custom ,
                        vcFieldName       
            END 
	ELSE IF @FormId=43
			BEGIN
				PRINT 'ABC'
				SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                            ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                            0 AS Custom ,
                            vcDbColumnName
                    FROM    View_DynamicDefaultColumns
                    WHERE   numFormID = @FormId
                            AND ISNULL(bitSettingField, 0) = 1
                            AND ISNULL(bitDeleted, 0) = 0
                            AND numDomainID = @numDomainID
                    UNION
                    SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                            fld_label AS vcFieldName ,
                            1 AS Custom ,
                            CONVERT(VARCHAR(10), fld_id) AS vcDbColumnName
                    FROM    CFW_Fld_Master
                            LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
                    WHERE   CFW_Fld_Master.grp_id = 1
                            AND CFW_Fld_Master.numDomainID = @numDomainID
                            AND Fld_type <> 'Link'
                    ORDER BY Custom ,
                            vcFieldName          
			END
	ELSE IF @FormId = 125 AND @PageId=0
	BEGIN
			SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                    0 AS Custom
            FROM    View_DynamicDefaultColumns
            WHERE   numFormID = @FormId
                    AND ISNULL(bitSettingField, 0) = 1
                    AND ISNULL(bitDeleted, 0) = 0
                    AND numDomainID = @numDomainID
	END
    ELSE IF @PageId = 5 AND (@FormId = 84 OR @FormId = 85)
                BEGIN          
                    SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                            ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                            0 AS Custom ,
                            vcDbColumnName
                    FROM    View_DynamicDefaultColumns
                    WHERE   numFormID = @FormId
                            AND ISNULL(bitSettingField, 0) = 1
                            AND ISNULL(bitDeleted, 0) = 0
                            AND numDomainID = @numDomainID
                    UNION
                    SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                            fld_label AS vcFieldName ,
                            1 AS Custom ,
                            CONVERT(VARCHAR(10), fld_id) AS vcDbColumnName
                    FROM    CFW_Fld_Master
                            LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
                    WHERE   CFW_Fld_Master.grp_id = @PageId
                            AND CFW_Fld_Master.numDomainID = @numDomainID
                            AND Fld_type = 'TextBox'
                    ORDER BY Custom ,
                            vcFieldName             
                END  
            ELSE
                BEGIN          
                    SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                            ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                            0 AS Custom ,
                            vcDbColumnName
                    FROM    View_DynamicDefaultColumns
                    WHERE   numFormID = @FormId
                            AND ISNULL(bitSettingField, 0) = 1
                            AND ISNULL(bitDeleted, 0) = 0
                            AND numDomainID = @numDomainID
                    UNION
                    SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                            fld_label AS vcFieldName ,
                            1 AS Custom ,
                            CONVERT(VARCHAR(10), fld_id) AS vcDbColumnName
                    FROM    CFW_Fld_Master
                            LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
                    WHERE   1 = (CASE WHEN @FormId=21 THEN (CASE WHEN  CFW_Fld_Master.grp_id IN (5,9) THEN 1 ELSE 0 END) ELSE (CASE WHEN  CFW_Fld_Master.grp_id = @PageId THEN 1 ELSE 0 END) END)
                            AND CFW_Fld_Master.numDomainID = @numDomainID
                            AND Fld_type <> 'Link'
                    ORDER BY Custom ,
                            vcFieldName             
                END                      
                      
                   
    IF @PageId = 1 OR @PageId = 4
        BEGIN
		IF (@FormId = 96 OR @FormId = 97)
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        vcDbColumnName ,
                        0 AS Custom ,
                        tintRow AS tintOrder
                FROM    View_DynamicColumns
                WHERE   numFormId = @FormId
                        AND numDomainID = @numDomainID
                        AND ISNULL(bitCustom, 0) = 0
                        AND tintPageType = 1
                        AND ISNULL(bitSettingField, 0) = 1
                        AND numRelCntType = @numtype
                        AND ISNULL(bitDeleted, 0) = 0
                UNION
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                        vcFieldName ,
                        vcFieldName AS vcDbColumnName ,
                        1 AS Custom ,
                        tintRow AS tintOrder
                FROM    View_DynamicCustomColumns
                WHERE   numFormID = @FormId
                        AND numDomainID = @numDomainID
                        AND grp_id = @PageId
                        AND numDomainID = @numDomainID
                        AND vcAssociatedControlType <> 'Link'
                        AND ISNULL(bitCustom, 0) = 1
                        AND tintPageType = 1
                        AND numRelCntType = @numtype
                ORDER BY tintOrder                
            ELSE
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        vcDbColumnName ,
                        0 AS Custom ,
                        tintRow AS tintOrder
                FROM    View_DynamicColumns
                WHERE   numFormId = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND ISNULL(bitCustom, 0) = 0
                        AND tintPageType = 1
                        AND ISNULL(bitSettingField, 0) = 1
                        AND numRelCntType = @numtype
                        AND ISNULL(bitDeleted, 0) = 0
                UNION
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                        vcFieldName ,
                        '' AS vcDbColumnName ,
                        1 AS Custom ,
                        tintRow AS tintOrder
                FROM    View_DynamicCustomColumns
                WHERE   numFormID = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND grp_id = @PageId
                        AND numDomainID = @numDomainID
                        AND vcAssociatedControlType <> 'Link'
                        AND ISNULL(bitCustom, 0) = 1
                        AND tintPageType = 1
                        AND numRelCntType = @numtype
                ORDER BY tintOrder          
        END     
    ELSE IF @PageId = 10
            BEGIN
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        vcDbColumnName ,
                        0 AS Custom ,
                        tintRow AS tintOrder ,
                        numListID ,
                        numFieldID AS [numFieldID1]
                FROM    View_DynamicColumns
                WHERE   numFormId = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND ISNULL(bitCustom, 0) = 0
                        AND tintPageType = 1
                        AND ISNULL(bitSettingField, 0) = 1
                        AND numRelCntType = @numtype
                        AND ISNULL(bitDeleted, 0) = 0
                UNION
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                        vcFieldName ,
                        '' AS vcDbColumnName ,
                        1 AS Custom ,
                        tintRow AS tintOrder ,
                        numListID ,
                        numFieldID AS [numFieldID1]
                FROM    View_DynamicCustomColumns
                WHERE   numFormID = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID   
                        AND numDomainID = @numDomainID
                        AND vcAssociatedControlType = 'SelectBox'
                        AND ISNULL(bitCustom, 0) = 1
                        AND tintPageType = 1
                        AND numRelCntType = @numtype
                ORDER BY tintOrder    
            END	   
    ELSE IF @PageId = 5 AND (@FormId = 84 OR @FormId = 85)
                BEGIN
                    IF EXISTS ( SELECT  'col1'
                                FROM    DycFormConfigurationDetails
                                WHERE   numDomainID = @numDomainID
                                        AND numFormId = @FormID
                                        AND numFieldID IN ( 507, 508, 509, 510,
                                                            511, 512 ) )
                        BEGIN
                            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                                    vcDbColumnName ,
                                    0 AS Custom ,
                                    tintRow AS tintOrder ,
                                    vcAssociatedControlType ,
                                    numlistid ,
                                    vcLookBackTableName ,
                                    numFieldID AS FieldId ,
                                    bitAllowEdit ,
                                    0 AS AZordering
                            FROM    View_DynamicColumns
                            WHERE   numFormId = @FormID
                                    AND numDomainID = @numDomainID
                                    AND ISNULL(bitCustom, 0) = 0   
                                    AND ISNULL(bitSettingField, 0) = 1
                                    AND ISNULL(bitDeleted, 0) = 0
                            UNION
                            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                                    vcFieldName ,
                                    '' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    tintRow AS tintOrder ,
                                    vcAssociatedControlType ,
                                    numlistid ,
                                    '' AS vcLookBackTableName ,
                                    numFieldID AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    0 AS AZordering
                            FROM    View_DynamicCustomColumns
                            WHERE   numFormID = @FormID
                                    AND numDomainID = @numDomainID
                                    AND grp_id = @PageID
                                    AND vcAssociatedControlType = 'TextBox'
                                    AND ISNULL(bitCustom, 0) = 1
                            ORDER BY tintOrder 
                        END
                    ELSE
                        BEGIN
                            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                                    vcDbColumnName ,
                                    0 AS Custom ,
                                    tintRow AS tintOrder ,
                                    vcAssociatedControlType ,
                                    numlistid ,
                                    vcLookBackTableName ,
                                    numFieldID AS FieldId ,
                                    bitAllowEdit ,
                                    0 AS AZordering
                            FROM    View_DynamicColumns
                            WHERE   numFormId = @FormID
                                    AND numDomainID = @numDomainID
                                    AND ISNULL(bitCustom, 0) = 0   
                                    AND ISNULL(bitSettingField, 0) = 1
                                    AND ISNULL(bitDeleted, 0) = 0
                            UNION
                            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                                    vcFieldName ,
                                    '' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    tintRow AS tintOrder ,
                                    vcAssociatedControlType ,
                                    numlistid ,
                                    '' AS vcLookBackTableName ,
                                    numFieldID AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    0 AS AZordering
                            FROM    View_DynamicCustomColumns
                            WHERE   numFormID = @FormID
                                    AND numDomainID = @numDomainID
                                    AND grp_id = @PageID
                                    AND vcAssociatedControlType = 'TextBox'
                                    AND ISNULL(bitCustom, 0) = 1
                            UNION
                            SELECT  '507~0' AS numFieldID ,
                                    'Title:A-Z' AS vcFieldName ,
                                    'Title:A-Z' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    0 AS tintOrder ,
                                    ' ' AS vcAssociatedControlType ,
                                    1 AS numlistid ,
                                    '' AS vcLookBackTableName ,
                                    507 AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    1 AS IsFixed
                            UNION
                            SELECT  '508~0' AS numFieldID ,
                                    'Title:Z-A' AS vcFieldName ,
                                    'Title:Z-A' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    0 AS tintOrder ,
                                    ' ' AS vcAssociatedControlType ,
                                    1 AS numlistid ,
                                    '' AS vcLookBackTableName ,
                                    508 AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    1 AS IsFixed
                            UNION
                            SELECT  '509~0' AS numFieldID ,
                                    'Price:Low to High' AS vcFieldName ,
                                    'Price:Low to High' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    0 AS tintOrder ,
                                    ' ' AS vcAssociatedControlType ,
                                    1 AS numlistid ,
                                    '' AS vcLookBackTableName ,
                                    509 AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    1 AS IsFixed
                            UNION
                            SELECT  '510~0' AS numFieldID ,
                                    'Price:High to Low' AS vcFieldName ,
                                    'Price:High to Low' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    0 AS tintOrder ,
                                    ' ' AS vcAssociatedControlType ,
                                    1 AS numlistid ,
                                    '' AS vcLookBackTableName ,
                                    510 AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    1 AS IsFixed
                            UNION
                            SELECT  '511~0' AS numFieldID ,
                                    'New Arrival' AS vcFieldName ,
                                    'New Arrival' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    0 AS tintOrder ,
                                    ' ' AS vcAssociatedControlType ,
                                    1 AS numlistid ,
                                    '' AS vcLookBackTableName ,
                                    511 AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    1 AS IsFixed
                            UNION
                            SELECT  '512~0' AS numFieldID ,
                                    'Oldest' AS vcFieldName ,
                                    'Oldest' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    0 AS tintOrder ,
                                    ' ' AS vcAssociatedControlType ,
                                    1 AS numlistid ,
                                    '' AS vcLookBackTableName ,
                                    512 AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    1 AS IsFixed
                            ORDER BY tintOrder 
        END
                        END

			IF @FormId=43
			BEGIN
                    SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                            ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                            vcDbColumnName ,
                            0 AS Custom ,
                            tintRow AS tintOrder ,
                            vcAssociatedControlType ,
                            numlistid ,
                            vcLookBackTableName ,
                            numFieldID AS FieldId ,
                            bitAllowEdit
                    FROM    View_DynamicColumns
                    WHERE   numFormId = @FormId
                            AND numUserCntID = @numUserCntID
                            AND numDomainID = @numDomainID
                            AND ISNULL(bitCustom, 0) = 0
                            AND tintPageType = 1
                            AND ISNULL(bitSettingField, 0) = 1
                            AND ISNULL(numRelCntType, 0) = @numtype
                            AND ISNULL(bitDeleted, 0) = 0
                            AND ISNULL(numViewID, 0) = @numViewID
                    UNION
                    SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                            vcFieldName ,
                            '' AS vcDbColumnName ,
                            1 AS Custom ,
                            tintRow AS tintOrder ,
                            vcAssociatedControlType ,
                            numlistid ,
                            '' AS vcLookBackTableName ,
                            numFieldID AS FieldId ,
                            0 AS bitAllowEdit
                    FROM    View_DynamicCustomColumns
                    WHERE   numFormID = @FormId
                            AND numUserCntID = @numUserCntID
                            AND numDomainID = @numDomainID
                            AND grp_id = 1
                            AND numDomainID = @numDomainID
                            AND vcAssociatedControlType <> 'Link'
                            AND ISNULL(bitCustom, 0) = 1
                            AND tintPageType = 1
                            AND ISNULL(numRelCntType, 0) = @numtype
                            AND ISNULL(numViewID, 0) = @numViewID
                    ORDER BY tintOrder       
			END
            ELSE
                BEGIN  
                    SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                            ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                            vcDbColumnName ,
                            0 AS Custom ,
                            tintRow AS tintOrder ,
                            vcAssociatedControlType ,
                            numlistid ,
                            vcLookBackTableName ,
                            numFieldID AS FieldId ,
                            bitAllowEdit
                    FROM    View_DynamicColumns
                    WHERE   numFormId = @FormId
                            AND numUserCntID = @numUserCntID
                            AND numDomainID = @numDomainID
                            AND ISNULL(bitCustom, 0) = 0
                            AND tintPageType = 1
                            AND ISNULL(bitSettingField, 0) = 1
                            AND ISNULL(numRelCntType, 0) = @numtype
                            AND ISNULL(bitDeleted, 0) = 0
                            AND ISNULL(numViewID, 0) = @numViewID
                    UNION
                    SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                            vcFieldName ,
                            '' AS vcDbColumnName ,
                            1 AS Custom ,
                            tintRow AS tintOrder ,
                            vcAssociatedControlType ,
                            numlistid ,
                            '' AS vcLookBackTableName ,
                            numFieldID AS FieldId ,
                            0 AS bitAllowEdit
                    FROM    View_DynamicCustomColumns
                    WHERE   numFormID = @FormId
                            AND numUserCntID = @numUserCntID
                            AND numDomainID = @numDomainID
                            AND 1 = (CASE WHEN @FormId=21 THEN (CASE WHEN  grp_id IN (5,9) THEN 1 ELSE 0 END) ELSE (CASE WHEN  grp_id = @PageId THEN 1 ELSE 0 END) END)
                            AND numDomainID = @numDomainID
                            AND vcAssociatedControlType <> 'Link'
                            AND ISNULL(bitCustom, 0) = 1
                            AND tintPageType = 1
                            AND ISNULL(numRelCntType, 0) = @numtype
                            AND ISNULL(numViewID, 0) = @numViewID
                    ORDER BY tintOrder          
                END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCompanyInfoDtlPl]    Script Date: 07/26/2008 16:16:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--usp_GetCompanyInfoDtlPl 91099,72,-333
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcompanyinfodtlpl')
DROP PROCEDURE usp_getcompanyinfodtlpl
GO
CREATE PROCEDURE [dbo].[usp_GetCompanyInfoDtlPl]                                                                                                                   
@numDivisionID numeric(9) ,                      
@numDomainID numeric(9) ,        
@ClientTimeZoneOffset as int                              
    --                                                                           
AS                                                                            
BEGIN                                                                            
 SELECT CMP.numCompanyID,               
 CMP.vcCompanyName,                
 (select vcdata from listdetails where numListItemID = DM.numStatusID) as numCompanyStatusName,
 DM.numStatusID as numCompanyStatus,                                                                           
 (select vcdata from listdetails where numListItemID = CMP.numCompanyRating) as numCompanyRatingName, 
 CMP.numCompanyRating,             
 (select vcdata from listdetails where numListItemID = CMP.numCompanyType) as numCompanyTypeName,
 CMP.numCompanyType,              
 CMP.numCompanyType as numCmptype,               
 DM.bitPublicFlag,                 
 DM.numDivisionID,                                   
 DM. vcDivisionName,        
dbo.getCompanyAddress(dm.numdivisionId,1,dm.numDomainID) as Address,
dbo.getCompanyAddress(dm.numdivisionId,2,dm.numDomainID) as ShippingAddress,          
  isnull(AD2.vcStreet,'') as vcShipStreet,
  isnull(AD2.vcCity,'') as vcShipCity,
  isnull(dbo.fn_GetState(AD2.numState),'')  as vcShipState,
  ISNULL((SELECT numShippingZone FROM [State] WHERE (numDomainID=@numDomainID OR ConstFlag=1) AND numStateID=AD2.numState),0) AS numShippingZone,
 (select vcdata from listdetails where numListItemID = DM.numFollowUpStatus) as numFollowUpStatusName,
 DM.numFollowUpStatus,                
 CMP.vcWebLabel1,               
 CMP.vcWebLink1,               
 CMP.vcWebLabel2,               
 CMP.vcWebLink2,                                                                             
 CMP.vcWebLabel3,               
 CMP.vcWebLink3,               
 CMP.vcWeblabel4,               
 CMP.vcWebLink4,                                                                       
 DM.tintBillingTerms,              
 DM.numBillingDays,              
 DM.tintInterestType,              
 DM.fltInterest,                                                  
 isnull(AD2.vcPostalCode,'') as vcShipPostCode, 
 AD2.numCountry vcShipCountry,                          
 DM.bitPublicFlag,              
 (select vcdata from listdetails where numListItemID = DM.numTerID)  as numTerIDName,
 DM.numTerID,                                                                         
 (select vcdata from listdetails where numListItemID = CMP.numCompanyIndustry) as numCompanyIndustryName, 
  CMP.numCompanyIndustry,              
 (select vcdata from listdetails where numListItemID = CMP.numCompanyCredit) as  numCompanyCreditName,
 CMP.numCompanyCredit,                                                                             
 isnull(CMP.txtComments,'') as txtComments,               
 isnull(CMP.vcWebSite,'') vcWebSite,              
 (select vcdata from listdetails where numListItemID = CMP.numAnnualRevID) as  numAnnualRevIDName,
 CMP.numAnnualRevID,                
 (select vcdata from listdetails where numListItemID = CMP.numNoOfEmployeesID) as numNoOfEmployeesIDName,               
CMP.numNoOfEmployeesID,                                                                           
    (select vcdata from listdetails where numListItemID = CMP.vcProfile) as vcProfileName, 
 CMP.vcProfile,              
 DM.numCreatedBy,               
 dbo.fn_GetContactName(DM.numRecOwner) as RecOwner,               
 dbo.fn_GetContactName(DM.numCreatedBy)+' ' + convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintCreatedDate)) as vcCreatedBy,               
 DM.numRecOwner,                
 dbo.fn_GetContactName(DM.numModifiedBy)+' ' +convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintModifiedDate)) as vcModifiedBy,                    
 isnull(DM.vcComPhone,'') as vcComPhone, 
 DM.vcComFax,                   
 DM.numGrpID,  
 (select vcGrpName from Groups where numGrpId= DM.numGrpID) as numGrpIDName,              
 (select vcdata from listdetails where numListItemID = CMP.vcHow) as vcInfoSourceName,
 CMP.vcHow as vcInfoSource,                                                                            
 DM.tintCRMType,                                                                         
 (SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= DM.numCampaignID) as  numCampaignIDName,
 DM.numCampaignID,              
 DM.numAssignedBy, isnull(DM.bitNoTax,0) bitNoTax,                     
 (select A.vcFirstName+' '+A.vcLastName                       
 from AdditionalContactsInformation A                  
 join DivisionMaster D                        
 on D.numDivisionID=A.numDivisionID
 where A.numContactID=DM.numAssignedTo) as numAssignedToName,DM.numAssignedTo,
 (select  count(*) from dbo.GenericDocuments where numRecID= @numDivisionID and vcDocumentSection='A' AND tintDocumentType=2) as DocumentCount ,
 (SELECT count(*)from CompanyAssociations where numDivisionID=@numDivisionID and bitDeleted=0 ) as AssociateCountFrom,                            
 (SELECT count(*)from CompanyAssociations where numAssociateFromDivisionID=@numDivisionID and bitDeleted=0 ) as AssociateCountTo,
 (select vcdata from listdetails where numListItemID = DM.numCompanyDiff) as numCompanyDiffName,               
 DM.numCompanyDiff,DM.vcCompanyDiff,isnull(DM.bitActiveInActive,1) as bitActiveInActive,
(SELECT SUBSTRING(
	(SELECT ','+  A.vcFirstName+' ' +A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN '(' + dbo.fn_GetListItemName(SR.numContactType) + ')' ELSE '' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=DM.numDomainID AND SR.numModuleID=1 AND SR.numRecordID=DM.numDivisionID
				AND UM.numDomainID=DM.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('')),2,200000)) AS numShareWith,
 DM.bitOnCreditHold,
 DM.numDefaultPaymentMethod,
 DM.numAccountClassID,
 DM.tintPriceLevel,DM.vcPartnerCode as vcPartnerCode  ,
  ISNULL(DM.numPartenerSource,0) AS numPartenerSourceId,
 D3.vcPartnerCode+'-'+C3.vcCompanyName as numPartenerSource,
 ISNULL(DM.numPartenerContact,0) AS numPartenerContactId,
A1.vcFirstName+' '+A1.vcLastName AS numPartenerContact,
ISNULL(DM.bitEmailToCase,0) bitEmailToCase,
'' AS vcCreditCards,
----Fetch Last Followup(Template) and EmailSentDate(if any) Value
		  
	(CASE WHEN A2.numECampaignID > 0 THEN dbo.fn_FollowupDetailsInOrgList(A2.numContactID,1,@numDomainID) ELSE '' END) vcLastFollowup,

		----Fetch Last Followup(Template) Value

		----Fetch Next Followup(Template) and ExecutionDate Value
	(CASE WHEN A2.numECampaignID > 0 THEN dbo.fn_FollowupDetailsInOrgList(A2.numContactID,2,@numDomainID) ELSE '' END) vcNextFollowup,										 

		----Fetch Next Followup(Template) Value
(CASE WHEN (SELECt COUNT(*) FROM ExtarnetAccounts WHERE numDivisionID=DM.numDivisionID) >0 THEN 1 ELSE 0 END) AS bitEcommerceAccess,

ISNULL(DM.numDefaultShippingServiceID,0) numShippingService,
		CASE 
			WHEN DM.numDefaultShippingServiceID = 10 THEN 'Priority Overnight'
			WHEN DM.numDefaultShippingServiceID = 11 THEN 'Standard Overnight'
			WHEN DM.numDefaultShippingServiceID = 12 THEN 'First Overnight'
			WHEN DM.numDefaultShippingServiceID = 13 THEN 'FedEx 2nd Day'
			WHEN DM.numDefaultShippingServiceID = 14 THEN 'FedEx Express Saver'
			WHEN DM.numDefaultShippingServiceID = 15 THEN 'FedEx Ground'
			WHEN DM.numDefaultShippingServiceID = 16 THEN 'Ground Home Delivery'
			WHEN DM.numDefaultShippingServiceID = 17 THEN 'FedEx 1 Day Freight'
			WHEN DM.numDefaultShippingServiceID = 18 THEN 'FedEx 2 Day Freight'
			WHEN DM.numDefaultShippingServiceID = 19 THEN 'FedEx 3 Day Freight'
			WHEN DM.numDefaultShippingServiceID = 20 THEN 'International Priority'
			WHEN DM.numDefaultShippingServiceID = 21 THEN 'International Priority Distribution'
			WHEN DM.numDefaultShippingServiceID = 22 THEN 'International Economy'
			WHEN DM.numDefaultShippingServiceID = 23 THEN 'International Economy Distribution'
			WHEN DM.numDefaultShippingServiceID = 24 THEN 'International First'
			WHEN DM.numDefaultShippingServiceID = 25 THEN 'International Priority Freight'
			WHEN DM.numDefaultShippingServiceID = 26 THEN 'International Economy Freight'
			WHEN DM.numDefaultShippingServiceID = 27 THEN 'International Distribution Freight'
			WHEN DM.numDefaultShippingServiceID = 28 THEN 'Europe International Priority'
			WHEN DM.numDefaultShippingServiceID = 40 THEN 'UPS Next Day Air'
			WHEN DM.numDefaultShippingServiceID = 42 THEN 'UPS 2nd Day Air'
			WHEN DM.numDefaultShippingServiceID = 43 THEN 'UPS Ground'
			WHEN DM.numDefaultShippingServiceID = 48 THEN 'UPS 3Day Select'
			WHEN DM.numDefaultShippingServiceID = 49 THEN 'UPS Next Day Air Saver'
			WHEN DM.numDefaultShippingServiceID = 50 THEN 'UPS Saver'
			WHEN DM.numDefaultShippingServiceID = 51 THEN 'UPS Next Day Air Early A.M.'
			WHEN DM.numDefaultShippingServiceID = 55 THEN 'UPS 2nd Day Air AM'
			WHEN DM.numDefaultShippingServiceID = 70 THEN 'USPS Express'
			WHEN DM.numDefaultShippingServiceID = 71 THEN 'USPS First Class'
			WHEN DM.numDefaultShippingServiceID = 72 THEN 'USPS Priority'
			WHEN DM.numDefaultShippingServiceID = 73 THEN 'USPS Parcel Post'
			WHEN DM.numDefaultShippingServiceID = 74 THEN 'USPS Bound Printed Matter'
			WHEN DM.numDefaultShippingServiceID = 75 THEN 'USPS Media'
			WHEN DM.numDefaultShippingServiceID = 76 THEN 'USPS Library'
			ELSE '' 
		END AS vcShippingService,
		CASE 
			WHEN vcSignatureType = 0 THEN 'Service Default'
			WHEN vcSignatureType = 1 THEN 'Adult Signature Required'
			WHEN vcSignatureType = 2 THEN 'Direct Signature'
			WHEN vcSignatureType = 3 THEN 'InDirect Signature'
			WHEN vcSignatureType = 4 THEN 'No Signature Required'
			ELSE ''
		END AS vcSignatureType,
		ISNULL(DM.intShippingCompany,0) intShippingCompany,
		CASE WHEN ISNULL(DM.intShippingCompany,0) = 0 THEN '' ELSE ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 82 AND numListItemID=ISNULL(DM.intShippingCompany,0)),'') END AS vcShipVia
                                        
 FROM  CompanyInfo CMP    
  join DivisionMaster DM    
  on DM.numCompanyID=CMP.numCompanyID                                                                                                                                        
   LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
   AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
   LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
   AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
    LEFT JOIN divisionMaster D3 ON DM.numPartenerSource = D3.numDivisionID
  LEFT JOIN CompanyInfo C3 ON C3.numCompanyID = D3.numCompanyID    
  LEFT JOIN AdditionalContactsInformation A1 ON DM.numPartenerContact = A1.numContactId 
  LEft join   AdditionalContactsInformation A2 on   DM.numDivisionID = A2.numDivisionId and A2.bitPrimaryContact = 1
  LEFT JOIN DivisionMasterShippingConfiguration ON DM.numDivisionID=DivisionMasterShippingConfiguration.numDivisionID
 where DM.numDivisionID=@numDivisionID   and DM.numDomainID=@numDomainID                                                                                          
END

GO




/****** Object:  StoredProcedure [dbo].[USP_GetItemGroups]    Script Date: 07/26/2008 16:17:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getitemgroups')
DROP PROCEDURE usp_getitemgroups
GO
CREATE PROCEDURE [dbo].[USP_GetItemGroups]    
@numDomainID as numeric(9)=0,    
@numItemGroupID as numeric(9)=0,
@numItemCode as numeric(9)=0    
 
as    
    
if @numItemGroupID=0    
begin    
	IF @numItemCode>0
		BEGIN
			Select @numItemGroupID=numItemGroup FROM Item WHERE numItemCode=@numItemCode
			
			  select numItemCode,vcItemName,isnull(Dtl.numWareHouseItemId,0) as numWarehouseItmsID,txtItemDesc,DTL.numQtyItemsReq,0 as numOppChildItemID,
CASE WHEN isnull(DTL.numWareHouseItemId,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE Item.monListPrice END monListPrice, 
convert(varchar(30),CASE WHEN isnull(DTL.numWareHouseItemId,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE Item.monListPrice END)+'/'+isnull(UOM.vcUnitName,'Units') as UnitPrice,            
case when charItemType='P' then 'Inventory Item' when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory Item' end as charItemType,charItemType as ItemType                            
,WI.numItemID,numDefaultSelect,(SELECT vcPathForImage FROM ItemImages WHERE numItemCode = @numItemCode AND numDomainId = @numDomainId AND bitDefault = 1 AND bitIsImage = 1) AS  vcPathForImage,vcWareHouse,ISNULL(Dtl.numListId,0) AS numListId
from item                                
join ItemGroupsDTL Dtl 
LEFT OUTER JOIN [WareHouseItems] WI ON WI.[numItemID] = DTL.[numOppAccAttrID] AND WI.[numWareHouseItemID] = Dtl.[numWareHouseItemId]                  
LEFT OUTER JOIN Warehouses W ON W.numWareHouseID=WI.numWareHouseID
on numOppAccAttrID=numItemCode  
LEFT JOIN UOM ON UOM.numUOMId=item.numBaseUnit
                  
where  Dtl.numItemGroupID=@numItemGroupID and tintType=1 
			
		END
	ELSE
	BEGIN
		select numItemGroupID, vcItemGroup, numDomainID,bitCombineAssemblies,
		ISNULL(CAST((SELECT TOP 1 Fld_id FROM CFW_Fld_Master WHERE numlistid=numMapToDropdownFld) AS VARCHAR),0)+'-'+CAST(numMapToDropdownFld AS VARCHAR) AS numMapToDropdownFld
		,numProfileItem from ItemGroups     
		where numDomainID=@numDomainID    		
	END
end    
else if @numItemGroupID>0     
begin    
 select numItemGroupID, vcItemGroup, numDomainID,bitCombineAssemblies,
 ISNULL(CAST((SELECT TOP 1 Fld_id FROM CFW_Fld_Master WHERE numlistid=numMapToDropdownFld) AS VARCHAR),0)+'-'+CAST(numMapToDropdownFld AS VARCHAR) AS numMapToDropdownFld
 ,numProfileItem from ItemGroups     
 where numDomainID=@numDomainID and numItemGroupID=@numItemGroupID    
  
--select numItemCode,vcItemName,txtItemDesc,monListPrice from Item     
--  join ItemGroupsDTL    
--  on numOppAccAttrID=numItemCode    
--  where numItemGroupID=@numItemGroupID and tintType=1    
  
  select numItemCode,vcItemName,isnull(Dtl.numWareHouseItemId,0) as numWarehouseItmsID,txtItemDesc,DTL.numQtyItemsReq,0 as numOppChildItemID,
CASE WHEN isnull(DTL.numWareHouseItemId,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE Item.monListPrice END monListPrice, 
convert(varchar(30),CASE WHEN isnull(DTL.numWareHouseItemId,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE Item.monListPrice END)+'/'+isnull(UOM.vcUnitName,'Units') as UnitPrice,            
case when charItemType='P' then 'Inventory Item' when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory Item' end as charItemType,charItemType as ItemType                            
,WI.numItemID,numDefaultSelect,(SELECT vcPathForImage FROM ItemImages WHERE numItemCode = @numItemCode AND numDomainId = @numDomainId AND bitDefault = 1 AND bitIsImage = 1) AS  vcPathForImage,vcWareHouse,ISNULL(Dtl.numListId,0) AS numListId
from item                                
join ItemGroupsDTL Dtl 
LEFT OUTER JOIN [WareHouseItems] WI ON WI.[numItemID] = DTL.[numOppAccAttrID] AND WI.[numWareHouseItemID] = Dtl.[numWareHouseItemId]                  
on numOppAccAttrID=numItemCode     
LEFT OUTER JOIN Warehouses W ON W.numWareHouseID=WI.numWareHouseID
LEFT JOIN UOM ON UOM.numUOMId=item.numBaseUnit

where  numItemGroupID=@numItemGroupID and tintType=1 


SELECT CFW.Fld_id,Fld_label, CFW.numlistid
		
FROM CFW_Fld_Master CFW  
JOIN ItemGroupsDTL    
	ON numOppAccAttrID=Fld_id    
WHERE numItemGroupID=@numItemGroupID and tintType=2 

--SELECT CFW.Fld_id,Fld_label,
--		CFW.numlistid,IA.FLD_Value,
--		CASE WHEN CFW.fld_type='SelectBox' THEN dbo.GetListIemName(Fld_Value)
--			ELSE CAST(Fld_Value AS VARCHAR)
--		END AS FLD_ValueName
--FROM CFW_Fld_Master CFW  
--JOIN ItemGroupsDTL    
--	ON numOppAccAttrID=Fld_id    
--LEFT JOIN ItemAttributes IA
--	ON CFW.Fld_id = IA.FLD_ID

--WHERE numItemGroupID=@numItemGroupID and tintType=2     
--GROUP BY CFW.Fld_id,Fld_label,CFW.numlistid,IA.FLD_Value,CFW.fld_type
    
end
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetMultiSelectItems')
DROP PROCEDURE USP_GetMultiSelectItems
GO
CREATE PROCEDURE [dbo].[USP_GetMultiSelectItems]    
@numDomainID NUMERIC(18,0),
	@numItemCodes VARCHAR(8000) = '',
	@tintOppType TINYINT
AS    
BEGIN
 
	SELECT 
		I.numItemCode
		,I.vcItemName AS ItemName
		,I.charItemType
		,dbo.fn_GetItemAttributes(@numDomainID,I.numItemCode) AS Attr
		,(SELECT SUM(numOnHand)  FROM WareHouseItems WHERE numItemID = I.numItemCode)AS numOnHand
		,(SELECT SUM(numAllocation) FROM WareHouseItems WHERE numItemID = I.numItemCode)AS numAllocation
		,ISNULL(I.numSaleUnit,0) AS numSaleUnit
		,dbo.fn_UOMConversion((CASE WHEN  @tintOppType = 1 THEN ISNULL(I.numSaleUnit,0) ELSE ISNULL(I.numPurchaseUnit,0) END),I.numItemCode,I.numDomainID,ISNULL(I.numBaseUnit,0)) fltUOMConversionFactor
FROM 
	Item I
	WHERE 
		I.numItemCode IN (SELECT  Id FROM dbo.SplitIDs(@numItemCodes, ','))
END
GO
GO
/****** Object:  StoredProcedure [dbo].[USP_GetOpportuntityCommission]    Script Date: 02/28/2009 13:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva
-- [dbo].[USP_GetOpportuntityCommission] 1,1,1,-333,'1/1/2009','1/15/2009'
-- exec USP_GetOpportuntityCommission @numUserId=372,@numUserCntID=82979,@numDomainId=1,@ClientTimeZoneOffset=-330,@dtStartDate='2010-03-01 00:00:00:000',@dtEndDate='2010-04-01 00:00:00:000'
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getopportuntitycommission')
DROP PROCEDURE usp_getopportuntitycommission
GO
CREATE PROCEDURE [dbo].[USP_GetOpportuntityCommission]
(
               @numUserId            AS NUMERIC(9)  = 0,
               @numUserCntID         AS NUMERIC(9)  = 0,
               @numDomainId          AS NUMERIC(9)  = 0,
               @ClientTimeZoneOffset AS INT,
               @dtStartDate          AS DATETIME,
               @dtEndDate            AS DATETIME,
               @numOppBizDocsId AS NUMERIC(9) =0,
               @tintMode AS tinyint,
			   @numComRuleID AS NUMERIC(18,0) = NULL,
			   @numDivisionID NUMERIC(18,0) = NULL
)
AS
  BEGIN
  
  DECLARE @bitIncludeShippingItem AS BIT
  DECLARE @numShippingItemID AS NUMERIC(18,0)
  SELECT @bitIncludeShippingItem=bitIncludeTaxAndShippingInCommission,@numShippingItemID=numShippingServiceItemID FROM Domain WHERE numDomainID = @numDomainId


  IF @tintMode=0  -- Paid Invoice
  BEGIN
		SELECT 
			LD.vcData as BizDoc,
			OBD.numBizDocID,
			OM.numOppId,
			OBD.numOppBizDocsId,
			isnull(OBD.monAmountPaid,0) monAmountPaid,
			TEMPDeposit.dtDepositDate AS bintCreatedDate,
			OM.vcPOppName AS Name,
			Case when OM.tintOppStatus=0 then 'Open' Else 'Close' End as OppStatus, 
			isnull(OBD.monDealAmount,0) as DealAmount,
			Case When (OM.numRecOwner=@numUserCntID and OM.numAssignedTo=@numUserCntID) then  'Deal Owner/Assigned'
				 When OM.numRecOwner=@numUserCntID then 'Deal Owner' 
				 When OM.numAssignedTo=@numUserCntID then 'Deal Assigned' 
			end as EmpRole,
			ISNULL(TempBizDocComission.numComissionAmount,0) as decTotalCommission,
			OBD.vcBizDocID,
			(SELECT SUM(ISNULL(monTotAmount,0)) FROM dbo.OpportunityBizDocItems WHERE numOppBizDocID=OBD.numOppBizDocsId AND 1 = (CASE @bitIncludeShippingItem WHEN 1 THEN 1 ELSE CASE WHEN OpportunityBizDocItems.numItemCode = @numShippingItemID THEN 0 ELSE 1 END END)) AS monSubTotAMount
		FROM
		OpportunityBizDocs OBD 
		INNER JOIN OpportunityMaster OM ON OBD.numOppId =OM.numOppId
		LEFT JOIN ListDetails LD on LD.numListItemID = OBD.numBizDocId 
		CROSS APPLY
		(
			SELECT 
				MAX(DM.dtDepositDate) dtDepositDate
			FROM 
				DepositMaster DM 
			JOIN dbo.DepositeDetails DD	ON DM.numDepositId=DD.numDepositID 
			WHERE 
				DM.tintDepositePage=2 
				AND DM.numDomainId=@numDomainId
				AND DD.numOppID=OBD.numOppID 
				AND DD.numOppBizDocsID =OBD.numOppBizDocsID
		) TEMPDEPOSIT
		CROSS APPLY
		(
			SELECT
				SUM(ISNULL(numComissionAmount,0)) numComissionAmount,
				COUNT(*) AS intCount
			FROM
				BizDocComission BDC
			WHERE
				BDC.numDomainId = @numDomainID
				AND ((BDC.numUserCntID = @numUserCntID AND ISNULL(BDC.tintAssignTo,0) <> 3) OR (BDC.numUserCntID = @numDivisionID AND BDC.tintAssignTo = 3))
				AND BDC.numOppBizDocId = OBD.numOppBizDocsId
				AND bitCommisionPaid=0
				AND (ISNULL(@numComRuleID,0)=0 OR BDC.numComRuleID = @numComRuleID)
		) TempBizDocComission
		WHERE 
			OM.numDomainId=@numDomainId 
			AND OM.tintOppStatus=1 
			AND OM.tintOppType=1 
			AND (OM.numRecOwner=@numUserCntID OR OM.numAssignedTo=@numUserCntID OR OM.numPartenerContact=@numUserCntID OR OM.numPartner=@numDivisionID)
			AND OBD.bitAuthoritativeBizDocs = 1 
			AND isnull(OBD.monAmountPaid,0) >= OBD.monDealAmount 
			AND OBD.numOppBizDocsId IN (SELECT numOppBizDocId FROM BizDocComission WHERE ((numUserCntID = @numUserCntID AND ISNULL(tintAssignTo,0) <> 3) OR (numUserCntID = @numDivisionID AND tintAssignTo = 3)) AND numDomainId=@numDomainId AND bitCommisionPaid=0)
			AND CAST(TEMPDeposit.dtDepositDate AS DATE) BETWEEN CAST(@dtStartDate AS DATE) AND CAST(@dtEndDate AS DATE)
			AND TempBizDocComission.intCount > 0
		ORDER BY
			TEMPDeposit.dtDepositDate
	END
  ELSE IF @tintMode=1 -- Non Paid Invoice
  BEGIN
		Select 
			vcData as BizDoc,
			oppBiz.numBizDocID,
			Opp.numOppId,
			oppBiz.numOppBizDocsId,
			isnull(oppBiz.monAmountPaid,0) monAmountPaid,
			dbo.FormatedDateFromDate(DATEADD(MINUTE,-@ClientTimeZoneOffset,Opp.bintCreatedDate),@numDomainID) bintCreatedDate,
			Opp.vcPOppName AS Name,
			Case when Opp.tintOppStatus=0 then 'Open' Else 'Close' End as OppStatus, 
			isnull(oppBiz.monDealAmount,0) as DealAmount,
            Case 
				When (Opp.numRecOwner=@numUserCntID and Opp.numAssignedTo=@numUserCntID) then  'Deal Owner/Assigned'
				When Opp.numRecOwner=@numUserCntID then 'Deal Owner' 
				When Opp.numAssignedTo=@numUserCntID then 'Deal Assigned' 
			end as EmpRole,
			ISNULL(TempBizDocComission.numComissionAmount,0) as decTotalCommission,oppBiz.vcBizDocID,
			(SELECT SUM(ISNULL(monTotAmount,0)) FROM dbo.OpportunityBizDocItems WHERE numOppBizDocID=oppBiz.numOppBizDocsId AND 1 = (CASE @bitIncludeShippingItem WHEN 1 THEN 1 ELSE CASE WHEN OpportunityBizDocItems.numItemCode = @numShippingItemID THEN 0 ELSE 1 END END)) AS monSubTotAMount
        From OpportunityMaster Opp 
		join OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
        left join ListDetails LD on LD.numListItemID=OPPBIZ.numBizDocId 
		CROSS APPLY
		(
			select 
				SUM(ISNULL(numComissionAmount,0)) numComissionAmount,
				COUNT(*) AS intCount
			from 
				BizDocComission 
			WHERE 
				((numUserCntID = @numUserCntID AND ISNULL(tintAssignTo,0) <> 3) OR (numUserCntID = @numDivisionID AND tintAssignTo = 3))
				AND numDomainId=@numDomainId 
				AND numOppBizDocId=oppBiz.numOppBizDocsId
				AND bitCommisionPaid=0 
				AND (ISNULL(@numComRuleID,0)=0 OR numComRuleID = @numComRuleID)
		) TempBizDocComission
        Where 
			oppBiz.bitAuthoritativeBizDocs = 1 AND Opp.tintOppStatus=1 And Opp.tintOppType=1 
			And (Opp.numRecOwner=@numUserCntID or Opp.numAssignedTo=@numUserCntID OR Opp.numPartner=@numDivisionID)
            And Opp.numDomainId=@numDomainId AND oppBiz.bitAuthoritativeBizDocs = 1 AND isnull(oppBiz.monAmountPaid,0) <oppBiz.monDealAmount
			AND ((dateadd(minute,-@ClientTimeZoneOffset,oppBiz.dtFromDate) between @dtStartDate And @dtEndDate)) 
			AND oppBiz.numOppBizDocsId IN (SELECT DISTINCT numOppBizDocId FROM BizDocComission WHERE ((numUserCntID = @numUserCntID AND ISNULL(tintAssignTo,0) <> 3) OR (numUserCntID = @numDivisionID AND tintAssignTo = 3)) AND numDomainId=@numDomainId AND bitCommisionPaid=0) --added by chintan BugID 982
			AND TempBizDocComission.intCount > 0
  END
   ELSE IF @tintMode=2  -- Paid Invoice Detail
   BEGIN
        SELECT * FROM (Select BC.numComissionID,oppBiz.numOppBizDocsId,BC.decCommission,Case when BC.tintComBasedOn = 1 then 'Amount' when BC.tintComBasedOn = 2 then 'Units' end AS BasedOn,
			Case when BC.tintComType = 1 then 'Percentage' when BC.tintComType = 2 then 'Flat' end as CommissionType, 
                        numComissionAmount  as CommissionAmt,I.vcItemName,BDI.numUnitHour,BDI.monPrice,BDI.monTotAmount,ISNULL(OT.monVendorCost, 0)* ISNULL(BDI.[numUnitHour],0) AS VendorCost,
						ISNULL(OT.monAvgCost, 0)* ISNULL(BDI.[numUnitHour],0) AS monAvgCost,
                        (SELECT MAX(DM.dtDepositDate) FROM DepositMaster DM JOIN dbo.DepositeDetails DD
									ON DM.numDepositId=DD.numDepositID WHERE DM.tintDepositePage=2 AND DM.numDomainId=@numDomainId
									AND DD.numOppID=oppBiz.numOppID AND DD.numOppBizDocsID =oppBiz.numOppBizDocsID) AS dtDepositDate          
                              From OpportunityMaster Opp 
							  INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
                              INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId and ((BC.numUserCntID = @numUserCntID AND ISNULL(BC.tintAssignTo,0) <> 3) OR (BC.numUserCntID = @numDivisionID AND BC.tintAssignTo = 3))
                              INNER JOIN OpportunityBizDocItems BDI ON oppBiz.numOppBizDocsId=BDI.numOppBizDocID AND BC.numOppBizDocItemID=BDI.numOppBizDocItemID
							  INNER JOIN OpportunityItems OT ON OT.numOppItemtCode=BDI.numOppItemID
							  INNER JOIN Item I ON BDI.numItemCode=I.numItemCode
                              Where oppBiz.bitAuthoritativeBizDocs = 1 and Opp.tintOppStatus=1 And Opp.tintOppType=1 And (Opp.numRecOwner=@numUserCntID OR Opp.numAssignedTo=@numUserCntID OR Opp.numPartner=@numDivisionID)
                              And Opp.numDomainId=@numDomainId 
							  AND oppBiz.numOppBizDocsId=@numOppBizDocsId AND BC.bitCommisionPaid=0
							  AND (ISNULL(@numComRuleID,0)=0 OR BC.numComRuleID=@numComRuleID)
							  AND oppBiz.bitAuthoritativeBizDocs = 1 AND 
							  1=(CASE WHEN @tintMode=2 THEN CASE WHEN isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount THEN 1 ELSE 0 END
							       WHEN @tintMode=3 THEN CASE WHEN isnull(oppBiz.monAmountPaid,0) <oppBiz.monDealAmount THEN 1 ELSE 0 END END)) A
						WHERE (dtDepositDate between @dtStartDate And @dtEndDate)  
							     
    END
    ELSE IF @tintMode=3 -- Non Paid Invoice Detail
   BEGIN
        Select BC.numComissionID,oppBiz.numOppBizDocsId,BC.decCommission,Case when BC.tintComBasedOn = 1 then 'Amount' when BC.tintComBasedOn = 2 then 'Units' end AS BasedOn,
			Case when BC.tintComType = 1 then 'Percentage' when BC.tintComType = 2 then 'Flat' end as CommissionType, 
                        numComissionAmount  as CommissionAmt,I.vcItemName,BDI.numUnitHour,BDI.monPrice,BDI.monTotAmount,ISNULL(OT.monVendorCost, 0)* ISNULL(BDI.[numUnitHour],0) AS VendorCost,
						ISNULL(OT.monAvgCost, 0)* ISNULL(BDI.[numUnitHour],0) AS monAvgCost       
                              From OpportunityMaster Opp 
							  INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
                              INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId and ((BC.numUserCntID = @numUserCntID AND ISNULL(BC.tintAssignTo,0) <> 3) OR (BC.numUserCntID = @numDivisionID AND BC.tintAssignTo = 3))
                              INNER JOIN OpportunityBizDocItems BDI ON oppBiz.numOppBizDocsId=BDI.numOppBizDocID AND BC.numOppBizDocItemID=BDI.numOppBizDocItemID
							  INNER JOIN OpportunityItems OT ON OT.numOppItemtCode=BDI.numOppItemID
							  INNER JOIN Item I ON BDI.numItemCode=I.numItemCode
                              Where oppBiz.bitAuthoritativeBizDocs = 1 and Opp.tintOppStatus=1 And Opp.tintOppType=1 And (Opp.numRecOwner=@numUserCntID OR Opp.numAssignedTo=@numUserCntID OR Opp.numPartner=@numDivisionID)
                              And Opp.numDomainId=@numDomainId 
							  AND ((dateadd(minute,-@ClientTimeZoneOffset,oppBiz.dtFromDate) between @dtStartDate And @dtEndDate)) 
							  AND oppBiz.numOppBizDocsId=@numOppBizDocsId AND BC.bitCommisionPaid=0
							  AND (ISNULL(@numComRuleID,0)=0 OR BC.numComRuleID=@numComRuleID)
							  AND oppBiz.bitAuthoritativeBizDocs = 1 AND 
							  1=(CASE WHEN @tintMode=2 THEN CASE WHEN isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount THEN 1 ELSE 0 END
							       WHEN @tintMode=3 THEN CASE WHEN isnull(oppBiz.monAmountPaid,0) <oppBiz.monDealAmount THEN 1 ELSE 0 END END)
    END
  END
--Created by anoop jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPOItemsForFulfillment')
DROP PROCEDURE USP_GetPOItemsForFulfillment
GO
CREATE PROCEDURE [dbo].[USP_GetPOItemsForFulfillment]
	@numDomainID as numeric(9),
	@numUserCntID numeric(9)=0,                                                                                                               
	@SortChar char(1)='0',                                                            
	@CurrentPage int,                                                              
	@PageSize int,                                                              
	@TotRecs int output,                                                              
	@columnName as Varchar(50),                                                              
	@columnSortOrder as Varchar(10),
	@numRecordID NUMERIC(18,0),
	@FilterBy TINYINT,
	@Filter VARCHAR(300),
	@ClientTimeZoneOffset INT,
	@numVendorInvoiceBizDocID NUMERIC(18,0)
AS
BEGIN
	DECLARE @firstRec AS INTEGER                                                              
	DECLARE @lastRec AS INTEGER                                                     
	SET @firstRec = (@CurrentPage - 1) * @PageSize                             
	SET @lastRec = (@CurrentPage * @PageSize + 1 ) 

	IF LEN(ISNULL(@columnName,''))=0
		SET @columnName = 'Opp.numOppID'
		
	IF LEN(ISNULL(@columnSortOrder,''))=0	
		SET @columnSortOrder = 'DESC'

	CREATE TABLE #tempForm 
	(
		ID INT IDENTITY(1,1), tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0
	DECLARE @numFormId INT = 135

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormId AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1


		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				@numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=@numFormId 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1
		ORDER BY 
			tintOrder asc  
	END

	DECLARE @strColumns VARCHAR(MAX) = ''
    DECLARE @FROM NVARCHAR(MAX) = ''
    DECLARE @WHERE NVARCHAR(MAX) = ''

	SET @FROM =' FROM 
					OpportunityMaster Opp
				INNER JOIN DivisionMaster DM ON Opp.numDivisionId = DM.numDivisionID
				INNER JOIN CompanyInfo cmp ON cmp.numCompanyID=DM.numCompanyID
				INNER JOIN OpportunityItems OI On OI.numOppId=Opp.numOppId
				INNER JOIN Item I on I.numItemCode=OI.numItemCode
				LEFT JOIN WareHouseItems WI on OI.numWarehouseItmsID=WI.numWareHouseItemID
				LEFT JOIN Warehouses W on W.numWarehouseID=WI.numWarehouseID
				LEFT JOIN WarehouseLocation WL on WL.numWLocationID =WI.numWLocationID'

	SET @WHERE = CONCAT(' WHERE 
							tintOppType=2 
							AND tintOppstatus=1
							AND tintShipped=0 
							AND ',(CASE WHEN ISNULL(@numVendorInvoiceBizDocID,0) > 0 THEN 'OBDI.numUnitHour - ISNULL(OBDI.numVendorInvoiceUnitReceived,0) > 0' ELSE 'OI.numUnitHour - ISNULL(OI.numUnitHourReceived,0) > 0' END),'
							AND I.[charItemType] IN (''P'',''N'',''S'') 
							AND (OI.bitDropShip=0 or OI.bitDropShip IS NULL) 
							AND Opp.numDomainID=',@numDomainID)
	
	IF ISNULL(@numRecordID,0) > 0
	BEGIN
		IF @FilterBy=1 --Item
			set	@WHERE=@WHERE + CONCAT(' AND I.numItemCode = ',@numRecordID)
		ELSE IF @FilterBy=2 --Warehouse
			set	@WHERE=@WHERE + CONCAT(' AND WI.numWarehouseID = ',@numRecordID)
		ELSE IF @FilterBy=3 --Purchase Order
			set	@WHERE=@WHERE + CONCAT(' AND Opp.numOppID = ',@numRecordID)
		ELSE IF @FilterBy=4 --Vendor
			set	@WHERE=@WHERE + CONCAT(' AND DM.numDivisionID = ',@numRecordID)	
		ELSE IF @FilterBy=5 --BizDoc
			set	@WHERE=@WHERE + CONCAT(' AND Opp.numOppID in (SELECT numOppId FROM OpportunityBizDocs WHERE numOppBizDocsId = ',@numRecordID,')')
		ELSE IF @FilterBy=6 --SKU
			set	@WHERE=@WHERE + CONCAT(' AND I.numItemCode = ',@numRecordID)
	END
	ELSE IF LEN(ISNULL(@Filter,'')) > 0
	BEGIN
		IF @FilterBy=1 --Item
			set	@WHERE=@WHERE + ' and OI.vcItemName like ''%' + @Filter + '%'''
		ELSE IF @FilterBy=2 --Warehouse
			set	@WHERE=@WHERE + ' and W.vcWareHouse like ''%' + @Filter + '%'''
		ELSE IF @FilterBy=3 --Purchase Order
			set	@WHERE=@WHERE + ' and Opp.vcPOppName like ''%' + @Filter + '%'''
		ELSE IF @FilterBy=4 --Vendor
			set	@WHERE=@WHERE + ' and cmp.vcCompanyName LIKE ''%' + @Filter + '%'''		
		ELSE IF @FilterBy=5 --BizDoc
			set	@WHERE=@WHERE + ' and Opp.numOppID in (SELECT numOppId FROM OpportunityBizDocs WHERE vcBizDocID like ''%' + @Filter + '%'')'
		ELSE IF @FilterBy=6 --SKU
			set	@WHERE=@WHERE + ' and  I.vcSKU LIKE ''%' + @Filter + '%'' OR WI.vcWHSKU LIKE ''%' + @Filter + '%'')'
	END

	IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
	BEGIN
		SET @FROM = @FROM + ' INNER JOIN OpportunityBizDocs OBD ON OBD.numOppID=Opp.numOppID INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId = OBDI.numOppBizDocID AND OBDI.numOppItemID=OI.numoppitemtCode'
		SET	@WHERE=@WHERE + CONCAT(' AND OBD.numOppBizDocsId = ',@numVendorInvoiceBizDocID)
	END


	DECLARE @strSql NVARCHAR(MAX)
    DECLARE @SELECT NVARCHAR(MAX) = CONCAT('WITH POItems AS 
									(SELECT
										Row_number() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') AS row 
										,Opp.numDomainID
										,Opp.numOppID
										,ISNULL(Opp.bitStockTransfer,0) bitStockTransfer
										,OI.numoppitemtCode
										,OI.numWarehouseItmsID
										,WI.numWLocationID
										,Opp.numRecOwner
										,DM.tintCRMType
										,ISNULL(DM.numTerID,0) numTerID
										,DM.numDivisionID
										,',(CASE WHEN ISNULL(@numVendorInvoiceBizDocID,0) > 0 THEN 'ISNULL(OBDI.numUnitHour,0)' ELSE 'ISNULL(OI.numUnitHour,0)' END), ' numUnitHour
										,',(CASE WHEN ISNULL(@numVendorInvoiceBizDocID,0) > 0 THEN 'ISNULL(OBDI.numVendorInvoiceUnitReceived,0)' ELSE 'ISNULL(OI.numUnitHourReceived,0)' END), ' numUnitHourReceived
										,ISNULL(Opp.fltExchangeRate,0) AS fltExchangeRate
										,ISNULL(Opp.numCurrencyID,0) AS numCurrencyID
										,ISNULL(I.numIncomeChartAcntId,0) AS itemIncomeAccount
										,ISNULL(I.numAssetChartAcntId,0) AS itemInventoryAsset
										,ISNULL(I.numCOGsChartAcntId,0) AS itemCoGs
										,ISNULL(OI.bitDropShip,0) AS DropShip
										,I.charItemType
										,OI.vcType ItemType
										,ISNULL(OI.numProjectID, 0) numProjectID
										,ISNULL(OI.numClassID, 0) numClassID
										,OI.numItemCode
										,OI.monPrice
										,ISNULL(OI.vcItemName,I.vcItemName) vcItemName
										,vcPOppName
										,ISNULL(Opp.bitPPVariance,0) AS bitPPVariance
										,ISNULL(I.bitLotNo,0) as bitLotNo
										,ISNULL(I.bitSerialized,0) AS bitSerialized
										,cmp.vcCompanyName')

	DECLARE @tintOrder  AS TINYINT;SET @tintOrder = 0
	DECLARE @vcFieldName  AS VARCHAR(50)
	DECLARE @vcListItemType  AS VARCHAR(3)
	DECLARE @vcListItemType1  AS VARCHAR(1)
	DECLARE @vcAssociatedControlType VARCHAR(30)
	DECLARE @numListID  AS NUMERIC(9)
	DECLARE @vcDbColumnName VARCHAR(40)
	DECLARE @vcLookBackTableName VARCHAR(2000)
	DECLARE @bitCustom  AS BIT
	Declare @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)   
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @vcColumnName AS VARCHAR(500)
	DECLARE @ListRelID AS NUMERIC(9) 

	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT 
	SELECT @iCount = COUNT(*) FROM #tempForm

	WHILE @i <= @iCount
	BEGIN
		SELECT 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName ,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
			@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE
			ID=@i

		IF @bitCustom = 0
		BEGIN
			DECLARE  @Prefix  AS VARCHAR(5)

			IF @vcLookBackTableName = 'AdditionalContactsInformation'
				SET @Prefix = 'ADC.'
			IF @vcLookBackTableName = 'DivisionMaster'
				SET @Prefix = 'DM.'
			IF @vcLookBackTableName = 'OpportunityMaster'
				SET @PreFix = 'Opp.'
			IF @vcLookBackTableName = 'OpportunityItems'
				SET @PreFix = 'OI.'
			IF @vcLookBackTableName = 'Item'
				SET @PreFix = 'I.'
			IF @vcLookBackTableName = 'WareHouseItems'
				SET @PreFix = 'WI.'
			IF @vcLookBackTableName = 'Warehouses'
				SET @PreFix = 'W.'
			IF @vcLookBackTableName = 'WarehouseLocation'
				SET @PreFix = 'WL.'
			IF @vcLookBackTableName = 'CompanyInfo'
				SET @PreFix = 'cmp.'

			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

			IF @vcAssociatedControlType = 'SelectBox'
			BEGIN
				IF @vcDbColumnName = 'numPartner'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartner' 
				END
				ELSE IF @vcDbColumnName = 'tintSource'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'')' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numContactID'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'vcLocation'
				BEGIN
					SET @strColumns = @strColumns + ',CONCAT(W.vcWarehouse,CASE WHEN LEN(ISNULL(WL.vcLocation,'''')) > 0 THEN CONCAT('' ('',WL.vcLocation,'')'') ELSE '''' END) [' + @vcColumnName + ']'
				END
				ELSE IF @vcListItemType = 'LI'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					SET @FROM = @FROM + ' LEFT JOIN ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'U'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
				END
				ELSE IF @vcListItemType = 'SGT'
				BEGIN
					SET @strColumns = @strColumns + ',(CASE ' + @Prefix + @vcDbColumnName + ' 
														WHEN 0 THEN ''Service Default''
														WHEN 1 THEN ''Adult Signature Required''
														WHEN 2 THEN ''Direct Signature''
														WHEN 3 THEN ''InDirect Signature''
														WHEN 4 THEN ''No Signature Required''
														ELSE ''''
														END) [' + @vcColumnName + ']'
				END
				ELSE IF @vcListItemType = 'PSS'
				BEGIN
					SET @strColumns = @strColumns + ',(CASE ' + @Prefix + @vcDbColumnName + ' 
														WHEN 10 THEN ''FedEx Priority Overnight''
														WHEN 11 THEN ''FedEx Standard Overnight''
														WHEN 12 THEN ''FedEx Overnight''
														WHEN 13 THEN ''FedEx 2nd Day''
														WHEN 14 THEN ''FedEx Express Saver''
														WHEN 15 THEN ''FedEx Ground''
														WHEN 16 THEN ''FedEx Ground Home Delivery''
														WHEN 17 THEN ''FedEx 1 Day Freight''
														WHEN 18 THEN ''FedEx 2 Day Freight''
														WHEN 19 THEN ''FedEx 3 Day Freight''
														WHEN 20 THEN ''FedEx International Priority''
														WHEN 21 THEN ''FedEx International Priority Distribution''
														WHEN 22 THEN ''FedEx International Economy''
														WHEN 23 THEN ''FedEx International Economy Distribution''
														WHEN 24 THEN ''FedEx International First''
														WHEN 25 THEN ''FedEx International Priority Freight''
														WHEN 26 THEN ''FedEx International Economy Freight''
														WHEN 27 THEN ''FedEx International Distribution Freight''
														WHEN 28 THEN ''FedEx Europe International Priority''
														WHEN 40 THEN ''UPS Next Day Air''
														WHEN 42 THEN ''UPS 2nd Day Air''
														WHEN 43 THEN ''UPS Ground''
														WHEN 48 THEN ''UPS 3Day Select''
														WHEN 49 THEN ''UPS Next Day Air Saver''
														WHEN 50 THEN ''UPS Saver''
														WHEN 51 THEN ''UPS Next Day Air Early A.M.''
														WHEN 55 THEN ''UPS 2nd Day Air AM''
														WHEN 70 THEN ''USPS Express''
														WHEN 71 THEN ''USPS First Class''
														WHEN 72 THEN ''USPS Priority''
														WHEN 73 THEN ''USPS Parcel Post''
														WHEN 74 THEN ''USPS Bound Printed Matter''
														WHEN 75 THEN ''USPS Media''
														WHEN 76 THEN ''USPS Library''
														ELSE ''''
														END) [' + @vcColumnName + ']'
						
				END
            END
			ELSE IF @vcAssociatedControlType = 'DateField'
			BEGIN
				IF @vcDbColumnName = 'dtReleaseDate'
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),'
                                + @Prefix
                                + @vcDbColumnName
                                + ')= convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate())) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,-1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']'
				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
                                + @Prefix
                                + @vcDbColumnName
                                + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']'
				 END	
            END
            ELSE IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea'
            BEGIN
				IF @vcDbColumnName = 'numShipState'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_getOPPAddressState(Opp.numOppId,Opp.numDomainID,2)' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numQtyToShipReceive'
				BEGIN
					IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(OBDI.numUnitHour,0) - ISNULL(OBDI.numVendorInvoiceUnitReceived,0)' + ' [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(OI.numUnitHour,0) - ISNULL(OI.numUnitHourReceived,0)' + ' [' + @vcColumnName + ']'
					END
				END
				ELSE IF @vcDbColumnName = 'vcPathForTImage'
		   		BEGIN
					SET @strColumns = @strColumns + ',ISNULL(II.vcPathForTImage,'''') AS ' + ' ['+ @vcColumnName+']'                  
					SET @FROM = @FROM + ' LEFT JOIN ItemImages II ON II.numItemCode = I.numItemCode AND II.bitDefault = 1'
				END
				ELSE IF @vcDbColumnName = 'SerialLotNo'
				BEGIN
					SET @strColumns = @strColumns + ',SUBSTRING((SELECT '','' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN '' ('' + CONVERT(VARCHAR(15),whi.numQty) + '')'' ELSE '''' end 
							FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=OI.numOppId and oppI.numOppItemID=OI.numoppitemtCode 
							ORDER BY vcSerialNo FOR XML PATH('''')),2,200000)  AS ' + ' ['+ @vcColumnName+']' 
				END
				ELSE IF @vcDbColumnName = 'numUnitHour'
				BEGIN
					IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(OBDI.numUnitHour,0) [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(' + @Prefix + @vcDbColumnName + ',0) [' + @vcColumnName + ']'
					END
				END
				ELSE IF @vcDbColumnName = 'numUnitHourReceived'
				BEGIN
					IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(OBDI.numVendorInvoiceUnitReceived,0) [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(' + @Prefix + @vcDbColumnName + ',0) [' + @vcColumnName + ']'
					END
				END
				ELSE IF @vcDbColumnName = 'vcSKU'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(OI.vcSKU,ISNULL(I.vcSKU,'''')) [' + @vcColumnName + ']'
				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns + ',' + @Prefix + @vcDbColumnName + ' [' + @vcColumnName + ']'
				END
            END
            ELSE IF  @vcAssociatedControlType='Label'                                              
			BEGIN
				IF @vcDbColumnName = 'numRemainingQty'
				BEGIN
					IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(OBDI.numUnitHour,0) - ISNULL(OBDI.numVendorInvoiceUnitReceived,0)' + ' [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(OI.numUnitHour,0) - ISNULL(OI.numUnitHourReceived,0)' + ' [' + @vcColumnName + ']'
					END
				END
				ELSE
				BEGIN
					SET @strColumns=@strColumns + ',' + @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']'        
				END
			END
        END
		ELSE
        BEGIN
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
            BEGIN
              SET @strColumns = @strColumns
                              + ',CFW'
                              + CONVERT(VARCHAR(3),@tintOrder)
                              + '.Fld_Value  ['
                              + @vcColumnName + ']'
              SET @FROM = @FROM
                                      + ' left Join CFW_FLD_Values_Opp CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + ' on CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.Fld_Id='
                                      + CONVERT(VARCHAR(10),@numFieldId)
                                      + 'and CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.RecId=Opp.numOppid   '
			END
			ELSE
            IF @vcAssociatedControlType = 'CheckBox' 
            BEGIN
                SET @strColumns = @strColumns
                                + ',case when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=0 then 0 when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=1 then 1 end   ['
                                + @vcColumnName + ']'
                SET @FROM = @FROM
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid   '
            END
            ELSE
            IF @vcAssociatedControlType = 'DateField'
            BEGIN
                  SET @strColumns = @strColumns
                                  + ',dbo.FormatedDateFromDate(CFW'
                                  + CONVERT(VARCHAR(3),@tintOrder)
                                  + '.Fld_Value,'
                                  + CONVERT(VARCHAR(10),@numDomainId)
                                  + ')  ['
                                  + @vcColumnName + ']'
                  SET @FROM = @FROM
                                          + ' left Join CFW_FLD_Values_Opp CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + ' on CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.Fld_Id='
                                          + CONVERT(VARCHAR(10),@numFieldId)
                                          + 'and CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.RecId=Opp.numOppid    '
            END
            ELSE
            IF @vcAssociatedControlType = 'SelectBox'
            BEGIN
                SET @vcDbColumnName = 'DCust'
                                            + CONVERT(VARCHAR(10),@numFieldId)
                SET @strColumns = @strColumns
                                + ',L'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.vcData'
                                + ' ['
                                + @vcColumnName + ']'

                SET @FROM = @FROM
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid '

                SET @FROM = @FROM
                                        + ' left Join ListDetails L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.numListItemID=CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Value'
            END
			ELSE
			BEGIN
				SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID)') + ' [' + @vcColumnName + ']'
			END
		END

		SET @i = @i + 1
	END

	SET @strSql = @SELECT + @strColumns + @FROM + @WHERE + ') SELECT * INTO #tempTable FROM [POItems] WHERE row > ' + CONVERT(VARCHAR(10), @firstRec) + ' and row <' + CONVERT(VARCHAR(10), @lastRec)

	PRINT CAST(@strSql AS NTEXT)
	SET @strSql = @strSql + '; (SELECT @TotRecs =COUNT(*) ' + @FROM + @WHERE + ')' + '; SELECT * FROM #tempTable; DROP TABLE #tempTable;'

	EXEC sp_executesql 
        @query = @strSql, 
        @params = N'@TotRecs INT OUTPUT', 
        @TotRecs = @TotRecs OUTPUT 

	SELECT * FROM #tempForm
	DROP TABLE #tempForm
END
GO



/****** Object:  StoredProcedure [dbo].[USP_GetPriceOfItem]    Script Date: 07/26/2008 16:18:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_GetPriceOfItem @numItemID=173028,@intQuantity=1,@numDomainID=72,@numDivisionID=7056

--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getpriceofitem')
DROP PROCEDURE usp_getpriceofitem
GO
CREATE PROCEDURE [dbo].[USP_GetPriceOfItem]
@numItemID AS NUMERIC(18,0)=0,
@intQuantity AS INTEGER=0,
@numDomainID AS NUMERIC(18,0)=0,
@numDivisionID as numeric(18,0)=0,
@numWarehouseID AS NUMERIC(18,0)=0,
@numWarehouseItemID AS NUMERIC(18,0) = 0
AS
BEGIN
	IF ISNULL(@numWarehouseItemID,0) = 0
	BEGIN
		SELECT TOP 1 @numWareHouseItemID=[numWareHouseItemID] FROM [WareHouseItems] WHERE [numItemID]=@numItemID AND [numWareHouseID]=@numWarehouseID
	END

	EXEC USP_ItemPricingRecomm @numItemID,@intQuantity,0,@numDivisionID,@numDomainID,1,0.0,@numWareHouseItemID,2
END
GO
--EXEC USP_GetSalesTemplateItems 6,72
--SELECT * FROM SalesTemplateItems
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSalesTemplateItems')
DROP PROCEDURE USP_GetSalesTemplateItems
GO
CREATE PROCEDURE [dbo].[USP_GetSalesTemplateItems]
    @numSalesTemplateID NUMERIC(18,0),
    @numDomainID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0)
AS
	DECLARE @tintPriceLevel TINYINT
	DECLARE @tintDefaultSalesPricing AS TINYINT

	SELECT @tintDefaultSalesPricing=numDefaultSalesPricing FROM Domain WHERE numDomainId=@numDomainID

	SELECT 
		@tintPriceLevel = ISNULL(D.tintPriceLevel,0)
	FROM 
		DivisionMaster D                
	WHERE 
		numDivisionID =@numDivisionID       

    SELECT  
		X.*
		,(SELECT TOP 1 vcCategoryName FROM [Category] WHERE numCategoryID IN (SELECT numCategoryID FROM [ItemCategory] IC WHERE X.numItemCode = IC.[numItemID])) AS vcCategoryName
    FROM    
	( 
		SELECT DISTINCT
            STI.[numSalesTemplateItemID],
            STI.[numSalesTemplateID],
            STI.[numSalesTemplateItemID] AS numoppitemtCode,
            STI.[numItemCode],
            STI.[numUnitHour],
            CASE WHEN ISNULL(@numDivisionID,0) > 0 AND ISNULL(@tintPriceLevel,0) > 0 AND @tintDefaultSalesPricing = 1 AND (SELECT COUNT(*) FROM PricingTable WHERE numItemCode=STI.[numItemCode] AND ISNULL(numPriceRuleID,0) = 0) > 0 THEN 
			ISNULL((SELECT 
						decDiscount 
					FROM
					(
						SELECT 
							ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
							*
						FROM
							PricingTable
						WHERE 
							PricingTable.numItemCode = STI.[numItemCode]
					
					) TEMP
					WHERE
						numItemCode=STI.[numItemCode] 
						AND (tintRuleType = 3 AND Id=@tintPriceLevel)),STI.[monPrice]) ELSE STI.[monPrice] END monPrice,
            STI.[monTotAmount],
            STI.[numSourceID],
            STI.[numWarehouseID],
            STI.[Warehouse],
            STI.[numWarehouseItmsID],
            STI.[ItemType],
            STI.[ItemType] as vcType,
            STI.[Attributes],
			STI.[Attributes] as vcAttributes,
            STI.[AttrValues],
            I.[bitFreeShipping] FreeShipping, --Use from Item table
            STI.[Weight],
            STI.[Op_Flag],
            STI.[bitDiscountType],
            STI.[fltDiscount],
            STI.[monTotAmtBefDiscount],
            STI.[ItemURL],
            STI.[numDomainID],
            '' vcItemDesc,
            (SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1) vcPathForTImage,
            I.vcItemName,
            'False' AS DropShip,
			0 as bitDropShip,
            I.[bitTaxable],
            ISNULL(STI.numUOM,0) numUOM,
			ISNULL(STI.numUOM,0) as numUOMId,
            ISNULL(STI.vcUOMName,'') vcUOMName,
            ISNULL(STI.UOMConversionFactor,1) UOMConversionFactor,
			ISNULL(STI.numVendorWareHouse,0) as numVendorWareHouse
			,ISNULL(STI.numShipmentMethod,0) as numShipmentMethod
			,ISNULL(STI.numSOVendorId,0) as numSOVendorId
			,0 as bitWorkOrder
			,I.charItemType
			,dbo.fn_GetUOMName(I.numBaseUnit) AS vcBaseUOMName
			,0 AS numSortOrder
        FROM 
			SalesTemplateItems STI
        INNER JOIN item I ON I.numItemCode = STI.numItemCode
        LEFT JOIN dbo.UOM U ON U.numUOMID = I.numSaleUnit
        WHERE 
			STI.[numSalesTemplateID] = @numSalesTemplateID
            AND STI.[numDomainID] = @numDomainID
) X

GO
/****** Object:  StoredProcedure [dbo].[USP_GetUSAEpayDtls]    Script Date: 05/07/2009 21:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getusaepaydtls')
DROP PROCEDURE usp_getusaepaydtls
GO
CREATE PROCEDURE [dbo].[USP_GetUSAEpayDtls]
@numDomainID as numeric(9),
@intPaymentGateway AS INT=0,
@numSiteId as int
as  
BEGIN 
	IF @intPaymentGateway =0 
	BEGIN
		IF ISNULL(@numSiteId,0) > 0 AND (SELECT ISNULL(intPaymentGateWay,0) FROM Sites WHERE numDomainId=@numDomainID AND numSiteID=@numSiteID) > 0
		BEGIN
			SELECT 
				S.intPaymentGateWay,
				ISNULL(vcFirstFldValue,'') AS vcFirstFldValue,
				ISNULL(vcSecndFldValue,'') AS vcSecndFldValue,
				ISNULL(vcThirdFldValue,'') AS vcThirdFldValue,
				ISNULL(bitTest,0) AS bitTest,
				ISNULL(D.bitSaveCreditCardInfo,0) AS bitSaveCreditCardInfo
			FROM 
				Sites S
			INNER JOIN
				Domain D
			ON
				S.numDomainID = D.numDomainId
			LEFT JOIN 
				PaymentGatewayDTLID PD
			ON 
				PD.intPaymentGateWay = S.intPaymentGateWay
			WHERE 
				S.numDomainID = @numDomainID
				AND PD.numDomainID = @numDomainID
  				AND ISNULL(PD.numSiteId,0)=@numSiteId
		END
		ELSE
		BEGIN
			SELECT 
				D.intPaymentGateWay,
				ISNULL(vcFirstFldValue,'') AS vcFirstFldValue,
				ISNULL(vcSecndFldValue,'') AS vcSecndFldValue,
				ISNULL(vcThirdFldValue,'') AS vcThirdFldValue,
				ISNULL(bitTest,0) AS bitTest,
				ISNULL(D.bitSaveCreditCardInfo,0) AS bitSaveCreditCardInfo
			FROM 
				Domain D
			LEFT JOIN 
				PaymentGatewayDTLID PD
			ON 
				PD.intPaymentGateWay = D.intPaymentGateWay
			WHERE 
				D.numDomainID = @numDomainID
				AND PD.numDomainID = @numDomainID
  				AND ISNULL(PD.numSiteId,0)=0
		END
	
	END
	ELSE IF @intPaymentGateway >0 
	BEGIN
	SELECT intPaymentGateWay,
		isnull(vcFirstFldValue,'') AS vcFirstFldValue,
		isnull(vcSecndFldValue,'') AS vcSecndFldValue,
		isnull(vcThirdFldValue,'') AS vcThirdFldValue,
		isnull(bitTest,0) AS bitTest
	FROM   PaymentGatewayDTLID PD
	WHERE  
		PD.numDomainID = @numDomainID
		AND PD.intPaymentGateWay = @intPaymentGateway
		AND PD.numSiteId=@numSiteId
	END
END
GO	   

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON


GO

IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWareHouseItems')
DROP PROCEDURE USP_GetWareHouseItems
GO



CREATE PROCEDURE [dbo].[USP_GetWareHouseItems]                              
@numItemCode as numeric(9)=0,
@byteMode as tinyint=0,
@numWarehouseItemID AS NUMERIC(18,0) = NULL,
@numWarehouseID numeric(18,0)=0,
@numWLocationID AS NUMERIC(18,0)=0 --05052018 Change: Warehouse Location added

                         
as                              
                              
                          
                              
declare @bitSerialize as bit                      
declare @str as nvarchar(max)                 
declare @str1 as nvarchar(max)               
declare @ColName as varchar(500)                      
set @str=''                       

DECLARE @numDomainID AS INT 
DECLARE @numBaseUnit AS NUMERIC(18,0)
DECLARE @vcUnitName as VARCHAR(100) 
DECLARE @numSaleUnit AS NUMERIC(18,0)
DECLARE @vcSaleUnitName as VARCHAR(100) 
DECLARE @numPurchaseUnit AS NUMERIC(18,0)
DECLARE @vcPurchaseUnitName as VARCHAR(100)
DECLARE @bitLot AS BIT                     
DECLARE @bitKitParent BIT
DECLARE @bitMatrix BIT
declare @numItemGroupID as numeric(9)                        
                        
select 
	@numDomainID=numDomainID,
	@numBaseUnit = ISNULL(numBaseUnit,0),
	@numSaleUnit = ISNULL(numSaleUnit,0),
	@numPurchaseUnit = ISNULL(numPurchaseUnit,0),
	@numItemGroupID=numItemGroup,
	@bitLot = bitLotNo,
	@bitSerialize=CASE WHEN bitSerialized=0 THEN bitLotNo ELSE bitSerialized END,
	@bitKitParent = ( CASE 
						WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0
                        WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                        ELSE 0
                     END )
	,@bitMatrix=bitMatrix
FROM 
	Item 
WHERE 
	numItemCode=@numItemCode     
	
SELECT @vcUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numBaseUnit
SELECT @vcSaleUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numSaleUnit
SELECT @vcPurchaseUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numPurchaseUnit
	
DECLARE @numSaleUOMFactor AS DECIMAL(28,14)
DECLARE @numPurchaseUOMFactor AS DECIMAL(28,14)

SELECT @numSaleUOMFactor = dbo.fn_UOMConversion(@numBaseUnit,@numItemCode,@numDOmainID,@numSaleUnit)
SELECT @numPurchaseUOMFactor = dbo.fn_UOMConversion(@numBaseUnit,@numItemCode,@numDOmainID,@numPurchaseUnit)


DECLARE @ID AS NUMERIC(18,0)  = 0                       
DECLARE @numCusFlDItemID AS VARCHAR(20)                        
DECLARE @fld_label AS VARCHAR(100),@fld_type AS VARCHAR(100)                        
DECLARE @Fld_ValueMatrix NUMERIC(18,0)
DECLARE @Fld_ValueNameMatrix VARCHAR(300)

IF @bitMatrix = 1
BEGIN
	SET @ColName='I.numItemCode,1' 

	SELECT 
		ROW_NUMBER() OVER(ORDER BY CFW_Fld_Master.fld_id) ID
		,CFW_Fld_Master.Fld_label
		,CFW_Fld_Master.fld_id
		,CFW_Fld_Master.fld_type
		,CFW_Fld_Master.numlistid
		,CFW_Fld_Master.vcURL
		,ItemAttributes.FLD_Value
		,CASE 
			WHEN CFW_Fld_Master.fld_type='SelectBox' THEN dbo.GetListIemName(Fld_Value)
			WHEN CFW_Fld_Master.fld_type='CheckBox' THEN CASE WHEN isnull(Fld_Value,0)=1 THEN 'Yes' ELSE 'No' END
			ELSE CAST(Fld_Value AS VARCHAR)
		END AS FLD_ValueName
	INTO 
		#tempTableMatrix
	FROM
		CFW_Fld_Master 
	INNER JOIN
		ItemGroupsDTL 
	ON 
		CFW_Fld_Master.Fld_id = ItemGroupsDTL.numOppAccAttrID
		AND ItemGroupsDTL.tintType = 2
	LEFT JOIN
		ItemAttributes
	ON
		CFW_Fld_Master.Fld_id = ItemAttributes.FLD_ID
		AND ItemAttributes.numItemCode = @numItemCode
	LEFT JOIN
		ListDetails LD
	ON
		CFW_Fld_Master.numlistid = LD.numListID
	WHERE
		CFW_Fld_Master.numDomainID = @numDomainId
		AND ItemGroupsDTL.numItemGroupID = @numItemGroupID
	GROUP BY
		CFW_Fld_Master.Fld_label
		,CFW_Fld_Master.fld_id
		,CFW_Fld_Master.fld_type
		,CFW_Fld_Master.bitAutocomplete
		,CFW_Fld_Master.numlistid
		,CFW_Fld_Master.vcURL
		,ItemAttributes.FLD_Value 

	
	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT
	SELECT @iCount = COUNT(*) FROM #tempTableMatrix
                         
	WHILE @i <= @iCount
	 BEGIN                        
		SELECT @fld_label=Fld_label,@Fld_ValueMatrix=FLD_Value,@Fld_ValueNameMatrix=FLD_ValueName FROM #tempTableMatrix WHERE ID=@i
                          
		SET @str = @str+ CONCAT (',',@Fld_ValueMatrix,' as [',@fld_label,']')
	
		IF @byteMode=1                                        
			SET @str = @str+ CONCAT (',''',@Fld_ValueNameMatrix,''' as [',@fld_label,'Value]')                                   
                          
		SET @i = @i + 1
	 END
END
ELSE
BEGIN
	SET @ColName='WareHouseItems.numWareHouseItemID,0' 

	--Create a Temporary table to hold data                                                            
	create table #tempTable 
	( 
		ID INT IDENTITY PRIMARY KEY,                                                                      
		numCusFlDItemID NUMERIC(9),
		Fld_Value VARCHAR(20)                                                         
	)

	INSERT INTO #tempTable                         
	(
		numCusFlDItemID
		,Fld_Value
	)                                                            
	SELECT DISTINCT
		numOppAccAttrID
		,''
	FROM 
		ItemGroupsDTL 
	WHERE 
		numItemGroupID=@numItemGroupID 
		AND tintType=2    

	SELECT TOP 1 
		@ID=ID
		,@numCusFlDItemID=numCusFlDItemID
		,@fld_label=fld_label
		,@fld_type=fld_type 
	FROM 
		#tempTable                         
	JOIN 
		CFW_Fld_Master 
	ON 
		numCusFlDItemID=Fld_ID                        
                         
	 WHILE @ID>0                        
	 BEGIN                        
                          
		SET @str = @str+',  dbo.GetCustFldItems('+@numCusFlDItemID+',9,'+@ColName+') as ['+ @fld_label+']'
	
		IF @byteMode=1                                        
			SET @str = @str+',  dbo.GetCustFldItemsValue('+@numCusFlDItemID+',9,'+@ColName+','''+@fld_type+''') as ['+ @fld_label+'Value]'                                        
                          
	   select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempTable                         
	   join CFW_Fld_Master on numCusFlDItemID=Fld_ID and ID >@ID                        
	   if @@rowcount=0 set @ID=0                        
                          
	 END
END          
                   
                          
                 
                      
                         
                       
--      

DECLARE @KitOnHand AS NUMERIC(9);SET @KitOnHand=0

--IF @bitKitParent=1
	--SELECT @KitOnHand=ISNULL(dbo.fn_GetKitInventory(@numItemCode),0)          
  
set @str1='select '

IF @byteMode=1                                        
		set @str1 =@str1 +'I.numItemCode,'
	
set @str1 =@str1 + 'numWareHouseItemID,WareHouseItems.numWareHouseID,
ISNULL(vcWarehouse,'''') + (CASE WHEN ISNULL(WL.vcLocation,'''') = '''' THEN '''' ELSE '', '' + isnull(WL.vcLocation,'''') END) vcWarehouse,
ISNULL(vcWarehouse,'''') AS vcExternalLocation,
(CASE WHEN WareHouseItems.numWLocationID = -1 THEN ''Global'' ELSE ISNULL(WL.vcLocation,'''') END) AS vcInternalLocation,
W.numWareHouseID,
ISNULL(WareHouseItems.numOnHand,0) + ISNULL(WareHouseItems.numAllocation,0) AS TotalOnHand,
(ISNULL(WareHouseItems.numOnHand,0) * (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END)) AS monCurrentValue,
Case when @bitKitParent=1 then CAST(ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) AS DECIMAL(18,2)) ELSE CAST(ISNULL(numOnHand,0) AS FLOAT) END AS [OnHand],
CAST((@numPurchaseUOMFactor * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as FLOAT) as PurchaseOnHand,
CAST((@numSaleUOMFactor * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as FLOAT) as SalesOnHand,
Case when @bitKitParent=1 THEN 0 ELSE CAST(ISNULL(numReorder,0) AS FLOAT) END as [Reorder] 
,Case when @bitKitParent=1 THEN 0 ELSE CAST(ISNULL(numOnOrder,0) AS FLOAT) END as [OnOrder]
,Case when @bitKitParent=1 THEN 0 ELSE CAST(ISNULL(numAllocation,0) AS FLOAT) END as [Allocation] 
,Case when @bitKitParent=1 THEN 0 ELSE CAST(ISNULL(numBackOrder,0) AS FLOAT) END as [BackOrder],
CAST((@numSaleUOMFactor * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as numeric(18,2)) as OnHandUOM,
CAST((@numPurchaseUOMFactor * Case when @bitKitParent=1 THEN 0 ELSE isnull(numOnOrder,0) END) AS DECIMAL(18,2)) as OnOrderUOM,
CAST((@numPurchaseUOMFactor * Case when @bitKitParent=1 THEN 0 ELSE isnull(numReorder,0) END) AS DECIMAL(18,2)) as ReorderUOM,
CAST((@numSaleUOMFactor * Case when @bitKitParent=1 THEN 0 ELSE isnull(numAllocation,0) END) AS DECIMAL(18,2)) as AllocationUOM,
CAST((@numSaleUOMFactor * Case when @bitKitParent=1 THEN 0 ELSE isnull(numBackOrder,0) END) AS DECIMAL(18,2)) as BackOrderUOM,
@vcUnitName As vcBaseUnit,
@vcSaleUnitName As vcSaleUnit,
@vcPurchaseUnitName As vcPurchaseUnit
,0 as Op_Flag , isnull(WL.vcLocation,'''') Location,isnull(WL.numWLocationID,0) as numWLocationID,
round(isnull(monWListPrice,0),2) Price,isnull(vcWHSKU,'''') as SKU,isnull(vcBarCode,'''') as BarCode '+ @str +'                   
,(SELECT CASE WHEN ISNULL(subI.bitKitParent,0) = 1 AND ISNULL((SELECT COUNT(*) FROM OpportunityItems 
															   WHERE OpportunityItems.numItemCode = WareHouseItems.numItemID
															   AND WareHouseItems.numWareHouseItemID = OpportunityItems.numWarehouseItmsID),0) = 0 THEN 1
			ELSE 0 
	    END 
FROM Item subI WHERE subI.numItemCode = WareHouseItems.numItemID) [IsDeleteKitWarehouse],@bitKitParent [bitKitParent],
(CASE WHEN (SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID=WareHouseItems.numItemID AND numWareHouseItemId = WareHouseItems.numWareHouseItemID) > 0 THEN 1 ELSE 0 END) AS bitChildItemWarehouse
,' + (CASE WHEN @bitSerialize=1 THEN CONCAT('dbo.GetWarehouseSerialLot(',@numDomainID,',WareHouseItems.numWareHouseItemID,',ISNULL(@bitLot,0),')') ELSE '''''' END) + ' AS vcSerialLot,
CASE 
	WHEN ISNULL(I.numItemGroup,0) > 0 
	THEN dbo.fn_GetAttributes(WareHouseItems.numWareHouseItemID,0)
	ELSE ''''
END AS vcAttribute,
I.numItemcode,
ISNULL(I.bitKitParent,0) AS bitKitParent,
ISNULL(I.bitSerialized,0) AS bitSerialized,
ISNULL(I.bitLotNo,0) as bitLotNo,
ISNULL(I.bitAssembly,0) as bitAssembly,
ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 461 AND numListItemID = I.numItemClass) ,'''') AS numShipClass,
		(
			CASE 
				WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE OpportunityItems.numItemCode = WareHouseItems.numItemID AND WareHouseItems.numWareHouseItemID = OpportunityItems.numWarehouseItmsID),0) = 0 
				THEN 1
			ELSE 
					0 
			END 
		) [IsDeleteKitWarehouse],
CASE WHEN (SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID=WareHouseItems.numItemID AND numWareHouseItemId = WareHouseItems.numWareHouseItemID) > 0 THEN 1 ELSE 0 END AS bitChildItemWarehouse,
I.numAssetChartAcntId,
(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) monAverageCost 
from WareHouseItems                           
join Warehouses W                             
on W.numWareHouseID=WareHouseItems.numWareHouseID 
left join WarehouseLocation WL on WL.numWLocationID =WareHouseItems.numWLocationID
join Item I on I.numItemCode=WareHouseItems.numItemID and WareHouseItems.numDomainId=I.numDomainId
where numItemID='+convert(varchar(15),@numItemCode) 
+ ' AND (WareHouseItems.numWarehouseItemID = ' + CAST(@numWarehouseItemID AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@numWarehouseItemID,0) AS VARCHAR(20)) + ' = 0 )'    
+ ' AND (WareHouseItems.numWareHouseID='+ CONVERT(VARCHAR(20),@numWarehouseID) + ' OR ' + CONVERT(VARCHAR(20),@numWarehouseID) + ' = 0)' 
+ ' AND (WareHouseItems.numWLocationID='+ CONVERT(VARCHAR(20),@numWLocationID) + ' OR ' + CONVERT(VARCHAR(20),@numWLocationID) + ' = 0)'        
   
print CAST(@str1 AS NTEXT)           

EXECUTE sp_executeSQL @str1, N'@bitKitParent bit,@numSaleUOMFactor DECIMAL(28,14), @numPurchaseUOMFactor DECIMAL(28,14), @vcUnitName VARCHAR(100), @vcSaleUnitName VARCHAR(100), @vcPurchaseUnitName VARCHAR(100)', @bitKitParent, @numSaleUOMFactor, @numPurchaseUOMFactor, @vcUnitName, @vcSaleUnitName, @vcPurchaseUnitName
                       
                        
set @str1='select numWareHouseItmsDTLID,WDTL.numWareHouseItemID, vcSerialNo,WDTL.vcComments as Comments,WDTL.numQty, WDTL.numQty as OldQty,ISNULL(W.vcWarehouse,'''') + (CASE WHEN ISNULL(WL.vcLocation,'''') = '''' THEN '''' ELSE '', '' + isnull(WL.vcLocation,'''') END) vcWarehouse '+ case when @bitSerialize=1 then @str else '' end +'
,WDTL.dExpirationDate, WDTL.bitAddedFromPO
from WareHouseItmsDTL WDTL                             
join WareHouseItems                             
on WDTL.numWareHouseItemID=WareHouseItems.numWareHouseItemID                              
join Warehouses W                             
on W.numWareHouseID=WareHouseItems.numWareHouseID  
left join WarehouseLocation WL on WL.numWLocationID =WareHouseItems.numWLocationID
where ISNULL(numQty,0) > 0 and numItemID='+ convert(varchar(15),@numItemCode) 
+ ' AND (WareHouseItems.numWarehouseItemID = ' + CAST(@numWarehouseItemID AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@numWarehouseItemID,0) AS VARCHAR(20)) + ' = 0 )' 
+ ' AND (WareHouseItems.numWareHouseID='+ CONVERT(VARCHAR(20),@numWarehouseID) + ' OR ' + CONVERT(VARCHAR(20),@numWarehouseID) + ' = 0)' 
+ ' AND (WareHouseItems.numWLocationID='+ CONVERT(VARCHAR(20),@numWLocationID) + ' OR ' + CONVERT(VARCHAR(20),@numWLocationID) + ' = 0)'   
                         
print CAST(@str1 AS NTEXT)                      
exec (@str1)                       
                      
IF @bitMatrix = 1
BEGIN
	SELECT
		Fld_label
		,fld_id
		,fld_type
		,numlistid
		,vcURL
		,Fld_Value 
	FROM 
		#tempTableMatrix

	DROP TABLE #tempTableMatrix
END
ELSE
BEGIN                      
	SELECT 
		Fld_label
		,fld_id
		,fld_type
		,numlistid
		,vcURL
		,Fld_Value 
	FROM 
		#tempTable                         
	JOIN 
		CFW_Fld_Master
	ON 
		numCusFlDItemID=Fld_ID                      

	DROP TABLE #tempTable
END

                      


-----------------For Kendo UI testting purpose
--create table #tempColumnConfig ( field varchar(100),                                                                      
-- title varchar(100) , format varchar(100)                                                      
-- )  
--
--insert into #tempColumnConfig
--	select 'vcWarehouse','WareHouse','' union all
--	select 'OnHand','On Hand','' union all
--	select 'Reorder','Re Order','' union all
--	select 'Allocation','Allocation','' union all
--	select 'BackOrder','Back Order','' union all
--	select 'Price','Price','{0:c}'
--
--select * from #tempColumnConfig
--
--drop table #tempColumnConfig

-----------------

--set @str1='select case when COUNT(*)=1 then MIN(numWareHouseItmsDTLID) else 0 end as numWareHouseItmsDTLID,MIN(WDTL.numWareHouseItemID) as numWareHouseItemID, vcSerialNo,0 as Op_Flag,WDTL.vcComments as Comments'+ case when @bitSerialize=1 then @str else'' end +',W.vcWarehouse,COUNT(*) Qty
--from WareHouseItmsDTL WDTL                             
--join WareHouseItems WI                             
--on WDTL.numWareHouseItemID=WI.numWareHouseItemID                              
--join Warehouses W                             
--on W.numWareHouseID=WI.numWareHouseID  
--where (tintStatus is null or tintStatus=0)  and  numItemID='+ convert(varchar(15),@numItemCode) +'
--GROUP BY vcSerialNo,WDTL.vcComments,W.vcWarehouse'
--                          
--print @str1                       
--exec (@str1)                       
              
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_Item_GetAll_BIZAPI' ) 
    DROP PROCEDURE USP_Item_GetAll_BIZAPI
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 9 April 2014
-- Description:	Gets list of items domain wise
-- =============================================
CREATE PROCEDURE USP_Item_GetAll_BIZAPI
	@numDomainID NUMERIC(18,0) = NULL,
	@numPageIndex AS INT = 0,
    @numPageSize AS INT = 0,
    @TotalRecords INT OUTPUT,
	@dtCreatedAfter DateTime = NULL,
	@dtModifiedAfter DateTime = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT 
		@TotalRecords = COUNT(*) 
	FROM 
		Item 
	WHERE 
		Item.numDomainID = @numDomainID
		AND (@dtCreatedAfter IS NULL OR Item.bintCreatedDate > @dtCreatedAfter)
		AND (@dtModifiedAfter IS NULL OR Item.bintModifiedDate > @dtModifiedAfter OR (SELECT COUNT(*) FROM WareHouseItems WHERE numItemID=Item.numItemCode AND DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), dtModified) > @dtModifiedAfter) > 0)

	SELECT
		ROW_NUMBER() OVER (ORDER BY Item.numItemCode asc) AS RowNumber,
		Item.numItemCode,
		Item.vcItemName,
		(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = Item.numItemCode AND ISNULL(bitDefault,0) = 1) AS vcPathForImage,
		(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = Item.numItemCode AND ISNULL(bitDefault,0) = 1) AS vcPathForTImage,
		Item.txtItemDesc,
		Item.charItemType,
		CASE 
			WHEN Item.charItemType='P' THEN 'Inventory Item' 
			WHEN Item.charItemType='N' THEN 'Non Inventory Item' 
			WHEN Item.charItemType='S' THEN 'Service' 
			WHEN Item.charItemType='A' THEN 'Accessory' 
		END AS ItemType,
		Item.vcSKU,
		Item.vcModelID,
		Item.numBarCodeId,
		Item.vcManufacturer,
		Item.vcUnitofMeasure,
		Item.monListPrice,
		(CASE WHEN ISNULL(Item.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(Item.monAverageCost,0.00) END) AS monAverageCost,
		Item.monCampaignLabourCost,
		Item.numItemGroup,
		Item.numItemClass,
		Item.numShipClass,
		Item.numItemClassification,
		Item.fltWidth,
		Item.fltWeight,
		Item.fltHeight,
		Item.fltLength,
		Item.numBaseUnit,
		Item.numSaleUnit,
		Item.numPurchaseUnit,
		Item.bitKitParent,
		Item.bitLotNo,
		Item.bitAssembly,
		Item.bitSerialized,
		Item.bitTaxable,
		Item.bitAllowBackOrder,
		Item.bitFreeShipping,
		Item.numCOGsChartAcntId,
		Item.numAssetChartAcntId,
		Item.numIncomeChartAcntId,
		Item.numVendorID,
		WI.numOnHand,                      
		WI.numOnOrder,                      
		WI.numReorder,                      
		WI.numAllocation,                      
		WI.numBackOrder,
		bintCreatedDate,
		bintModifiedDate,
		STUFF((SELECT CONCAT(',',numCategoryID) FROM ItemCategory WHERE numItemID=Item.numItemCode GROUP BY numCategoryID FOR XML PATH('')),1,1,'') AS vcCategories
	INTO
		#Temp
	FROM
		Item
	OUTER APPLY
	(
		SELECT
			SUM(numOnHand) as numOnHand,                      
			SUM(numOnOrder) as numOnOrder,                      
			SUM(numReorder)  as numReorder,                      
			SUM(numAllocation)  as numAllocation,                      
			SUM(numBackOrder)  as numBackOrder
		FROM 
			WarehouseItems
		WHERE
			numItemID=Item.numItemCode
	) WI
	WHERE 
		Item.numDomainID = @numDomainID
		AND (@dtCreatedAfter IS NULL OR Item.bintCreatedDate > @dtCreatedAfter)
		AND (@dtModifiedAfter IS NULL OR Item.bintModifiedDate > @dtModifiedAfter OR (SELECT COUNT(*) FROM WareHouseItems WHERE numItemID=Item.numItemCode AND DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), dtModified) > @dtModifiedAfter) > 0)
	ORDER BY
		Item.numItemCode
	OFFSET (CASE WHEN ISNULL(@numPageIndex,0) > 0 THEN (@numPageIndex - 1) ELSE 0 END) * @numPageSize ROWS FETCH NEXT (CASE WHEN ISNULL(@numPageSize,0) > 0 THEN @numPageSize ELSE 999999999 END) ROWS ONLY

	SELECT * FROM #Temp

	--GET CHILD ITEMS
	SELECT
		ItemDetails.numItemKitID,
		ItemDetails.numChildItemID,
		ItemDetails.numQtyItemsReq,
		WareHouseItems.numWareHouseItemID,
		Warehouses.numWareHouseID,
		(SELECT vcUnitName FROM UOM WHERE numUOMId = ItemDetails.numUOMId) AS vcUnitName
	FROM
		ItemDetails
	LEFT JOIN  
		WareHouseItems 
	ON
		ItemDetails.numWareHouseItemId = WareHouseItems.numWareHouseItemID
	LEFT JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	WHERE
		ItemDetails.numItemKitID IN (SELECT numItemCode FROM #Temp)

	--GET CUSTOM FIELDS
	SELECT 
		I.numItemCode,
		CFW_Fld_Master.Fld_id AS Id,
		CFW_Fld_Master.Fld_label AS Name,
		CFW_Fld_Master.Fld_type AS [Type],
		CFW_FLD_Values_Item.Fld_Value AS ValueId,
		CASE 
			WHEN CFW_Fld_Master.Fld_type='TextBox' or CFW_Fld_Master.Fld_type='TextArea' THEN
				(CASE WHEN CFW_FLD_Values_Item.Fld_Value='0' OR CFW_FLD_Values_Item.Fld_Value='' THEN '-' ELSE CFW_FLD_Values_Item.Fld_Value END)
			WHEN CFW_Fld_Master.Fld_type='SelectBox' THEN
				(CASE WHEN CFW_FLD_Values_Item.Fld_Value='' THEN '-' ELSE dbo.GetListIemName(CFW_FLD_Values_Item.Fld_Value) END)
			WHEN CFW_Fld_Master.Fld_type='CheckBox' THEN
				(CASE WHEN CFW_FLD_Values_Item.Fld_Value=0 THEN 'No' ELSE 'Yes' END)
			ELSE
				''
		END AS Value
	FROM 
		Item I
	INNER JOIN
		CFW_FLD_Values_Item 
	ON
		I.numItemCode = CFW_FLD_Values_Item.RecId
	INNER JOIN
		CFW_Fld_Master
	ON
		CFW_FLD_Values_Item.Fld_ID = CFW_Fld_Master.Fld_id
	WHERE 
		I.numDomainID = @numDomainID
		AND I.numItemCode IN (SELECT numItemCode FROM #Temp)


	DROp TABLE #Temp

END
GO


GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_Item_GetDetailForStockTransfer' ) 
    DROP PROCEDURE USP_Item_GetDetailForStockTransfer
GO
CREATE PROCEDURE USP_Item_GetDetailForStockTransfer
    @numDomainId NUMERIC(18,0),
    @numItemCode NUMERIC(18,0),
	@numFromWarehouseItemID NUMERIC(18,0),
	@numToWarehouseItemID NUMERIC(18,0)
AS 
BEGIN
	IF ISNULL(@numFromWarehouseItemID,0) > 0 AND ISNULL(@numToWarehouseItemID,0) > 0
	BEGIN
		SELECT
			Item.numItemCode AS numFromItemCode
			,ISNULL(Item.bitSerialized,0) bitSerialized
			,ISNULL(Item.bitLotNo,0) bitLotNo
			,CONCAT(vcItemName,', ',Warehouses.vcWareHouse,(CASE WHEN LEN(ISNULL(WarehouseLocation.vcLocation,'')) > 0 THEN CONCAT(' (',WarehouseLocation.vcLocation,')') ELSE '' END)) AS vcFrom
			,WareHouseItems.numWareHouseItemID AS numFromWarehouseItemID
			,ISNULL(WareHouseItems.numOnHand,0) numFromOnHand
			,ISNULL(WareHouseItems.numOnHand,0) + ISNULL(WareHouseItems.numAllocation,0) numFromAvailable
			,ISNULL(WareHouseItems.numBackOrder,0) numFromBackOrder
			,WarehouseTo.numItemCode AS numToItemCode
			,WarehouseTo.vcTo
			,WarehouseTo.numWareHouseItemID AS numToWarehouseItemID
			,WarehouseTo.numToAvailable
			,WarehouseTo.numToBackOrder
		FROM
			Item 
		INNER JOIN
			WareHouseItems
		ON
			WareHouseItems.numItemID = Item.numItemCode
			AND WareHouseItems.numWareHouseItemID = @numFromWarehouseItemID
		INNER JOIN
			Warehouses
		ON
			WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
		LEFT JOIN
			WarehouseLocation
		ON
			WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
		CROSS APPLY
		(
			SELECT 
				numItemCode
				,CONCAT(vcItemName,', ',WTo.vcWareHouse,(CASE WHEN LEN(ISNULL(WLTo.vcLocation,'')) > 0 THEN CONCAT(' (',WLTo.vcLocation,')') ELSE '' END)) AS vcTo
				,WHITo.numWareHouseItemID
				,ISNULL(WHITo.numOnHand,0) + ISNULL(WHITo.numAllocation,0) numToAvailable
				,ISNULL(WHITo.numBackOrder,0) numToBackOrder
			FROM
				WareHouseItems WHITo
			INNER JOIN
				Item ITo
			ON	
				WHITo.numItemID = ITo.numItemCode
			INNER JOIN
				Warehouses WTo
			ON
				WHITo.numWareHouseID = WTo.numWareHouseID
			LEFT JOIN
				WarehouseLocation WLTo
			ON
				WHITo.numWLocationID = WLTo.numWLocationID
			WHERE
				WHITo.numWareHouseItemID=@numToWarehouseItemID
		) WarehouseTo
		WHERE
			Item.numDomainID=@numDomainId
			AND Item.numItemCode=@numItemCode
	END
	ELSE
	BEGIN
		SELECT
			ISNULL(vcSKU,'') vcSKU
			,dbo.fn_GetItemAttributes(@numDomainId,@numItemCode) vcAttributes
			,ISNULL(Item.bitSerialized,0) bitSerialized
			,ISNULL(Item.bitLotNo,0) bitLotNo
		FROM
			Item
		WHERE
			numDomainID=@numDomainId
			AND numItemCode=@numItemCode
	END

	
END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_Item_MassStockTransfer' ) 
    DROP PROCEDURE USP_Item_MassStockTransfer
GO
CREATE PROCEDURE [dbo].[USP_Item_MassStockTransfer]
@numDomainID NUMERIC(18,0),
@numUserCntID NUMERIC(18,0),
@strItems as NVARCHAR(MAX)=''
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	IF LEN(ISNULL(@strItems,'')) > 0
	BEGIN
		DECLARE @hDocItem INT
		DECLARE @description VARCHAR(MAX)

		DECLARE @TEMP AS TABLE
		(
			ID INT IDENTITY(1,1)
			,numFromItemCode NUMERIC(18,0)
			,numQty NUMERIC(18,0)
			,numFromWarehouseItemID NUMERIC(18,0)
			,numToItemCode NUMERIC(18,0)
			,numToWarehouseItemID NUMERIC(18,0)
			,bitSerial BIT
			,bitLot BIT
			,vcSelectedSerialLotNumbers VARCHAR(MAX)
		)

		DECLARE @j INT 
		DECLARE @jCount INT
		DECLARE @numTempWareHouseItmsDTLID NUMERIC(18,0)
		DECLARE @numTempLotQty NUMERIC(18,0)
		DECLARE @vcTemp VARCHAR(MAX)

		DECLARE @TEMPSetialLot AS TABLE
		(
			ID INT
			,vcSerialLot VARCHAR(100)
		)

		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

		INSERT INTO @TEMP
		(
			numFromItemCode
			,numQty
			,numFromWarehouseItemID
			,numToItemCode
			,numToWarehouseItemID
			,bitSerial
			,bitLot
			,vcSelectedSerialLotNumbers
		)
		SELECT
			ISNULL(numFromItemCode,0),
			ISNULL(numUnitHour,0),
			ISNULL(numFromWarehouseItemID,0),
			ISNULL(numToItemCode,0),
			ISNULL(numToWarehouseItemID,0),
			ISNULL(IsSerial,0),
			ISNULL(IsLot,0),
			ISNULL(vcSelectedSerialLotNumbers,'')
		FROM
			OPENXML (@hDocItem,'/NewDataSet/Item',2)
		WITH
		(
			numFromItemCode NUMERIC(18,0),
			numUnitHour NUMERIC(18,0),
			numFromWarehouseItemID NUMERIC(18,0),
			numToItemCode NUMERIC(18,0),
			numToWarehouseItemID NUMERIC(18,0),
			IsSerial BIT,
			IsLot BIT,
			vcSelectedSerialLotNumbers VARCHAR(MAX)
		)

		DECLARE @i AS INT = 1
		DECLARE @numTempItemCode NUMERIC(18,0)
		DECLARE @numTempToItemCode NUMERIC(18,0)
		DECLARE @numTempQty NUMERIC(18,0)
		DECLARE @numTempFromWarehouseItemID NUMERIC(18,0)
		DECLARE @numTempToWarehouseItemID NUMERIC(18,0)
		DECLARE @bitTempSerial BIT
		DECLARE @bitTempLot BIT
		DECLARE @vcTempSelectedSerialLotNumbers VARCHAR(MAX)
		DECLARE @numFromOnHandBefore FLOAT
		DECLARE @numFromOnAllocationBefore FLOAT
		DECLARE @numFromOnHandAfter FLOAT
		DECLARE @numToOnHandBefore FLOAT
		DECLARE @numToOnHandAfter FLOAT

		DECLARE @iCount AS INT

		SELECT @iCount = COUNT(ID) FROM @TEMP


		IF @iCount = 0
		BEGIN
			RAISERROR('NO_ITEMS_ADDED_FOR_STOCK_TRANSFER',16,1)
		END
		ELSE 
		BEGIN
			WHILE @i <= @iCount
			BEGIN
				SELECT
					@numTempItemCode=numFromItemCode
					,@numTempToItemCode=numToItemCode
					,@numTempQty=numQty
					,@numTempFromWarehouseItemID=numFromWarehouseItemID
					,@numTempToWarehouseItemID=numToWarehouseItemID
					,@bitTempSerial=bitSerial
					,@bitTempLot=bitLot
					,@vcTempSelectedSerialLotNumbers=vcSelectedSerialLotNumbers
				FROM
					@TEMP
				WHERE
					ID=@i
				
				IF @numTempQty > 0
				BEGIN
					IF NOT EXISTS (SELECT numItemCode FROM Item WHERE numDomainID = @numDomainID AND numItemCode=@numTempItemCode)
					BEGIN
						RAISERROR('INVLID_ITEM_ID',16,1)
					END

					IF NOT EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WHERE numDomainID = @numDomainID AND numWareHouseItemID=@numTempFromWarehouseItemID)
					BEGIN
						RAISERROR('INVLID_FROM_LOCATION',16,1)
					END

					IF NOT EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WHERE numDomainID = @numDomainID AND numWareHouseItemID=@numTempToWarehouseItemID)
					BEGIN
						RAISERROR('INVLID_TO_LOCATION',16,1)
					END

					IF (@bitTempSerial = 1 OR @bitTempLot=1) AND LEN(@vcTempSelectedSerialLotNumbers) = 0
					BEGIN
						RAISERROR('SERIAL/LOT#_ARE_NOT_SELECTED',16,1)
					END


					SELECT @numFromOnHandBefore=ISNULL(numOnHand,0),@numFromOnAllocationBefore=ISNULL(numAllocation,0) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numWareHouseItemID=@numTempFromWarehouseItemID 

					IF (@numFromOnHandBefore + @numFromOnAllocationBefore) < @numTempQty
					BEGIN
						RAISERROR('INSUFFICIENT_ONHAND_QUANTITY',16,1)
					END
					ELSE
					BEGIN
						IF @bitTempSerial = 1 OR @bitTempLot=1
						BEGIN
							SET @vcTemp = ''
							DELETE FROM @TEMPSetialLot
							INSERT INTO @TEMPSetialLot
							(
								ID,
								vcSerialLot
							)
							SELECT 
								ROW_NUMBER() OVER(ORDER BY OutParam)
								,OutParam
							FROM 
								dbo.SplitString(@vcTempSelectedSerialLotNumbers,',')

							
							SET @j = 1
							SELECT @jCount=COUNT(*) FROM @TEMPSetialLot

							IF @jCount > 0
							BEGIN
								WHILE @j <= @jCount
								BEGIN
									SELECT
										@numTempWareHouseItmsDTLID=SUBSTRING(vcSerialLot,0,CHARINDEX('-',vcSerialLot))
										,@numTempLotQty=SUBSTRING(vcSerialLot,CHARINDEX('-',vcSerialLot)+1,LEN(vcSerialLot) + 1)
									FROM 
										@TEMPSetialLot
									WHERE
										ID=@j
									
									IF ISNULL(@numTempLotQty,0) > 0
									BEGIN
										IF @bitTempLot=1 
										BEGIN
											IF EXISTS (SELECT * FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numTempFromWarehouseItemID AND numWareHouseItmsDTLID=@numTempWareHouseItmsDTLID AND numQty >= @numTempLotQty)
											BEGIN
												UPDATE 
													WareHouseItmsDTL 
												SET 
													numQty = numQty - @numTempLotQty
													,@vcTemp = CONCAT(@vcTemp,(CASE WHEN LEN(@vcTemp) = 0 THEN '' ELSE ',' END),ISNULL(vcSerialNo,'') ,'(', @numTempLotQty ,')')
												WHERE 
													numWareHouseItemID=@numTempFromWarehouseItemID 
													AND numWareHouseItmsDTLID=@numTempWareHouseItmsDTLID

												INSERT INTO WareHouseItmsDTL
												(
													numWareHouseItemID,
													vcSerialNo,
													numQty,
													dExpirationDate,
													bitAddedFromPO
												)
												SELECT
													@numTempToWarehouseItemID,
													vcSerialNo,
													@numTempLotQty,
													dExpirationDate,
													bitAddedFromPO
												FROM 
													WareHouseItmsDTL
												WHERE
													numWareHouseItemID=@numTempFromWarehouseItemID AND numWareHouseItmsDTLID=@numTempWareHouseItmsDTLID
											END
											ELSE
											BEGIN
												RAISERROR('INVALID SERIAL/LOT OR INSUFFICIENT SERIAL/LOT QUANTITY',16,1)
											END
										END
										ELSE
										BEGIN
											IF EXISTS (SELECT * FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numTempFromWarehouseItemID AND numWareHouseItmsDTLID=@numTempWareHouseItmsDTLID AND numQty = 1)
											BEGIN
												UPDATE 
													WareHouseItmsDTL 
												SET 
													numWareHouseItemID=@numTempToWarehouseItemID
													,@vcTemp = @vcTemp + (CASE WHEN LEN(@vcTemp) = 0 THEN '' ELSE ',' END) + ISNULL(vcSerialNo,'') 
												WHERE 
													numWareHouseItemID=@numTempFromWarehouseItemID 
													AND numWareHouseItmsDTLID=@numTempWareHouseItmsDTLID
											END
											ELSE
											BEGIN
												RAISERROR('INVALID SERIAL/LOT OR INSUFFICIENT SERIAL/LOT QUANTITY',16,1)
											END
										END  
									END
									ELSE
									BEGIN
										RAISERROR('INVALID_SERIAL/LOT#_QTY',16,1)
									END

									SET @j = @j + 1
								END
							END
							ELSE 
							BEGIN
								RAISERROR('SERIAL/LOT#_ARE_NOT_SELECTED',16,1)
							END
						END

						SET @description = ''

						IF @bitTempSerial = 1 OR @bitTempLot = 1
						BEGIN
							SET @description = 'Mass Stock Transfer - Transferred (Qty:' + CAST(@numTempQty AS VARCHAR(10)) + ', Serial/Lot#:' + @vcTemp + ')'
						END
						ELSE
						BEGIN
							SET @description = 'Mass Stock Transfer - Transferred (Qty:' + CAST(@numTempQty AS VARCHAR(10)) + ')'
						END

						UPDATE
							WarehouseItems
						SET
							numOnHand = (CASE WHEN ISNULL(numOnHand,0) - ISNULL(@numTempQty,0) >= 0 THEN ISNULL(numOnHand,0) - ISNULL(@numTempQty,0) ELSE 0 END)
							,numAllocation = (CASE WHEN ISNULL(numOnHand,0) - ISNULL(@numTempQty,0) < 0 THEN (CASE WHEN ISNULL(numAllocation,0) - (ISNULL(@numTempQty,0) - ISNULL(numOnHand,0)) >= 0 THEN ISNULL(numAllocation,0) - (ISNULL(@numTempQty,0) - ISNULL(numOnHand,0)) ELSE 0 END) ELSE ISNULL(numAllocation,0) END)
							,numBackOrder = (CASE WHEN ISNULL(numOnHand,0) - ISNULL(@numTempQty,0) < 0 THEN ISNULL(numBackOrder,0) + (CASE WHEN ISNULL(numAllocation,0) - (ISNULL(@numTempQty,0) - ISNULL(numOnHand,0)) >= 0 THEN ISNULL(@numTempQty,0) - ISNULL(numOnHand,0) ELSE ISNULL(numAllocation,0) END) ELSE ISNULL(numBackOrder,0) END)
							,@numFromOnHandAfter = (CASE WHEN ISNULL(numOnHand,0) - ISNULL(@numTempQty,0) >= 0 THEN ISNULL(numOnHand,0) - ISNULL(@numTempQty,0) ELSE 0 END)
							,dtModified = GETDATE() 
						WHERE
							numWareHouseItemID=@numTempFromWarehouseItemID
						
						EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numTempFromWarehouseItemID,
							@numReferenceID = @numTempItemCode,
							@tintRefType = 1,
							@vcDescription = @description,
							@numModifiedBy = @numUserCntID,
							@numDomainID = @numDomainID

						IF @bitTempSerial = 1 OR @bitTempLot = 1
						BEGIN
							SET @description = 'Mass Stock Transfer - Receieved (Qty:' + CAST(@numTempQty AS VARCHAR(10)) + ', Serial/Lot#:' + @vcTemp + ')'
						END
						ELSE
						BEGIN
							SET @description = 'Mass Stock Transfer - Receieved (Qty:' + CAST(@numTempQty AS VARCHAR(10)) + ')'
						END


						SELECT @numToOnHandBefore = numOnHand FROM WareHouseItems WHERE numWareHouseItemID=@numTempToWarehouseItemID
						UPDATE
							WarehouseItems
						SET
							numOnHand = (CASE WHEN numBackOrder >= @numTempQty THEN numOnHand ELSE ISNULL(numOnHand,0) + (@numTempQty - ISNULL(numBackOrder,0)) END) 
							,numAllocation = (CASE WHEN numBackOrder >= @numTempQty THEN ISNULL(numAllocation,0) + @numTempQty ELSE ISNULL(numAllocation,0) + ISNULL(numBackOrder,0) END) 
							,numBackOrder = (CASE WHEN numBackOrder >= @numTempQty THEN ISNULL(numBackOrder,0) - @numTempQty ELSE 0 END) 
							,dtModified = GETDATE() 
						WHERE
							numWareHouseItemID=@numTempToWarehouseItemID
						SELECT @numToOnHandAfter = numOnHand FROM WareHouseItems WHERE numWareHouseItemID=@numTempToWarehouseItemID

						EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numTempToWarehouseItemID,
							@numReferenceID = @numTempToItemCode,
							@tintRefType = 1,
							@vcDescription = @description,
							@numModifiedBy = @numUserCntID,
							@numDomainID = @numDomainID


						INSERT INTO MassStockTransfer
						(
							numDomainID
							,numItemCode
							,numFromWarehouseItemID
							,numToItemCode
							,numToWarehouseItemID
							,numQty
							,numTransferredBy
							,dtTransferredDate
							,bitMassTransfer
							,vcSerialLot
							,numFromQtyBefore
							,numToQtyBefore
							,numFromQtyAfter
							,numToQtyAfter
						)
						VALUES
						(
							@numDomainID
							,@numTempItemCode
							,@numTempFromWarehouseItemID
							,@numTempToItemCode
							,@numTempToWarehouseItemID
							,@numTempQty
							,@numUserCntID
							,GETUTCDATE()
							,1
							,@vcTemp
							,@numFromOnHandBefore
							,@numToOnHandBefore
							,@numFromOnHandAfter
							,@numToOnHandAfter
						)
					END
				END
				ELSE 
				BEGIN
					RAISERROR('QTY_TO_TRANSFER_CAN_NOT_BE_0',16,1)
				END

				SET @i = @i + 1
			END
		END
	END
	ELSE
	BEGIN
		RAISERROR('NO_ITEMS_ADDED_FOR_STOCK_TRANSFER',16,1)
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_Item_SearchForSalesOrder' )
    DROP PROCEDURE USP_Item_SearchForSalesOrder
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sandeep Patel
-- Create date: 3 March 2014
-- Description:	Search items for sales order
-- =============================================
CREATE PROCEDURE USP_Item_SearchForSalesOrder
    @tintOppType AS TINYINT ,
    @numDomainID AS NUMERIC(9) ,
    @numDivisionID AS NUMERIC(9) = 0 ,
    @str AS VARCHAR(1000) ,
    @numUserCntID AS NUMERIC(9) ,
    @tintSearchOrderCustomerHistory AS TINYINT = 0 ,
    @numPageIndex AS INT ,
    @numPageSize AS INT ,
    @WarehouseID AS NUMERIC(18, 0) = NULL ,
    @TotalCount AS INT OUTPUT,
	@bitTransferTo AS BIT
AS
    BEGIN
  SET NOCOUNT ON;

DECLARE @strNewQuery NVARCHAR(MAX)
DECLARE @strSQL AS NVARCHAR(MAX)
DECLARE @strSQLCount NVARCHAR(MAX)
SELECT  @str = REPLACE(@str, '''', '''''')

DECLARE @TableRowCount TABLE ( Value INT );
	
SELECT  *
INTO    #Temp1
FROM    ( SELECT    numFieldId ,
                    vcDbColumnName ,
                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                    tintRow AS tintOrder ,
                    0 AS Custom
          FROM      View_DynamicColumns
          WHERE     numFormId = 22
                    AND numDomainID = @numDomainID
                    AND tintPageType = 1
                    AND bitCustom = 0
                    AND ISNULL(bitSettingField, 0) = 1
                    AND numRelCntType = 0
          UNION
          SELECT    numFieldId ,
                    vcFieldName ,
                    vcFieldName ,
                    tintRow AS tintOrder ,
                    1 AS Custom
          FROM      View_DynamicCustomColumns
          WHERE     Grp_id = 5
                    AND numFormId = 22
                    AND numDomainID = @numDomainID
                    AND tintPageType = 1
                    AND bitCustom = 1
                    AND numRelCntType = 0
        ) X 
  
IF NOT EXISTS ( SELECT  *
                FROM    #Temp1 )
    BEGIN
        INSERT  INTO #Temp1
                SELECT  numFieldId ,
                        vcDbColumnName ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        tintorder ,
                        0
                FROM    View_DynamicDefaultColumns
                WHERE   numFormId = 22
                        AND bitDefault = 1
                        AND ISNULL(bitSettingField, 0) = 1
                        AND numDomainID = @numDomainID 
    END

CREATE TABLE #tempItemCode ( numItemCode NUMERIC(9) )
DECLARE @bitRemoveVendorPOValidation AS BIT
DECLARE @tintDecimalPoints AS TINYINT
SELECT  
	@bitRemoveVendorPOValidation = ISNULL(bitRemoveVendorPOValidation,0)
	,@tintDecimalPoints=ISNULL(tintDecimalPoints,4)
FROM    
	domain
WHERE  
	numDomainID = @numDomainID

IF @tintOppType > 0
    BEGIN 
        IF CHARINDEX(',', @str) > 0
        BEGIN
			DECLARE @itemSearchText VARCHAR(100)
            DECLARE @vcAttribureSearch VARCHAR(MAX)
			DECLARE @vcAttribureSearchCondition VARCHAR(MAX)

			SET @itemSearchText = LTRIM(RTRIM(SUBSTRING(@str,0,CHARINDEX(',',@str))))

			--GENERATES ATTRIBUTE SEARCH CONDITION
			SET @vcAttribureSearch = SUBSTRING(@str,CHARINDEX(',',@str) + 1,LEN(@str))
	
			DECLARE @attributeSearchText VARCHAR(MAX)
			DECLARE @TempTable TABLE (vcValue VARCHAR(MAX))

			WHILE LEN(@vcAttribureSearch) > 0
			BEGIN
				SET @attributeSearchText = LEFT(@vcAttribureSearch, 
										ISNULL(NULLIF(CHARINDEX(',', @vcAttribureSearch) - 1, -1),
										LEN(@vcAttribureSearch)))
				SET @vcAttribureSearch = SUBSTRING(@vcAttribureSearch,
												ISNULL(NULLIF(CHARINDEX(',', @vcAttribureSearch), 0),
												LEN(@vcAttribureSearch)) + 1, LEN(@vcAttribureSearch))

				INSERT INTO @TempTable (vcValue) VALUES ( @attributeSearchText )
			END
		
			--REMOVES WHITE SPACES
			UPDATE
				@TempTable
			SET
				vcValue = LTRIM(RTRIM(vcValue))
	
			--CONVERTING TABLE TO COMMA SEPERATED STRING AFTER REMOVEING WHITE SPACES
			SELECT @vcAttribureSearch = COALESCE(@vcAttribureSearch,'') +  CONVERT(VARCHAR(MAX),vcValue) + ',' FROM @TempTable
	
			--REMOVE LAST COMMA FROM FINAL SEARCH STRING
			SET @vcAttribureSearch = LTRIM(RTRIM(@vcAttribureSearch))
			IF DATALENGTH(@vcAttribureSearch) > 0
				SET @vcAttribureSearch = LEFT(@vcAttribureSearch, LEN(@vcAttribureSearch) - 1)
	
			--START: GENERATES ATTRIBUTE SEARCH CONDITION
			SELECT 	@vcAttribureSearchCondition = '(TEMPMATRIX.Attributes LIKE ''%' + REPLACE(@vcAttribureSearch, ',', '%'' AND TEMPMATRIX.Attributes LIKE ''%') + '%'')' 

			--LOGIC FOR GENERATING FINAL SQL STRING
			IF @tintOppType = 1 OR ( @bitRemoveVendorPOValidation = 1 AND @tintOppType = 2)
                BEGIN      

				SET @strSQL = 'SELECT
									I.numItemCode,
									I.numVendorID,
									MIN(WHT.numWareHouseItemID) as numWareHouseItemID,
									dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [Attributes]
								FROM
									dbo.Item I WITH (NOLOCK)
								LEFT JOIN
									dbo.WareHouseItems WHT
								ON
									I.numItemCode = WHT.numItemID
								WHERE 
									((ISNULL(I.numItemGroup,0) > 0 AND I.vcItemName LIKE ''%' + @itemSearchText + '%'') OR (ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%''))
									AND ISNULL(I.Isarchieve, 0) <> 1
									AND I.charItemType NOT IN ( ''A'' )
									AND I.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + '
									AND (WHT.numWareHouseID = ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'

				--- added Asset validation  by sojan
                IF @bitRemoveVendorPOValidation = 1
                    AND @tintOppType = 2
                    SET @strSQL = @strSQL
                        + ' and ISNULL(I.bitKitParent,0) = 0 AND ISNULL(I.bitAssembly,0) = 0 '

                IF @tintSearchOrderCustomerHistory = 1 AND @numDivisionID > 0
                BEGIN
                    SET @strSQL = @strSQL
                        + ' and I.numItemCode in (select distinct oppI.numItemCode from 
						OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                        + CONVERT(VARCHAR(20), @numDomainID)
                        + ' and oppM.numDivisionId='
                        + CONVERT(VARCHAR(20), @numDivisionId)
                        + ')'
                END

				SET @strSQL = @strSQL + ' GROUP BY
										I.numItemCode,
										I.numVendorID,
										dbo.fn_GetAttributes(numWareHouseItemId,I.bitSerialized)'

				SET @strSQL = 
				'
				SELECT    
					SRNO=ROW_NUMBER() OVER( ORDER BY  I.vcItemName),
					I.numItemCode,
					(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForImage ,
					(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForTImage ,
					ISNULL(vcItemName, '''') vcItemName ,
					CAST(CAST(CASE WHEN I.[charItemType] = ''P''
					THEN ISNULL((SELECT TOP 1 monWListPrice FROM [WareHouseItems] WHERE [numItemID] = I.numItemCode), 0)
					ELSE monListPrice
					END AS DECIMAL(20,'+ CAST(@tintDecimalPoints AS VARCHAR) + ')) AS VARCHAR) AS monListPrice,
					ISNULL(vcSKU, '''') vcSKU ,
					ISNULL(numBarCodeId, 0) numBarCodeId ,
					ISNULL(vcModelID, '''') vcModelID ,
					ISNULL(txtItemDesc, '''') AS txtItemDesc ,
					ISNULL(C.vcCompanyName, '''') AS vcCompanyName,
					TEMPMATRIX.numWareHouseItemID,
					TEMPMATRIX.Attributes AS [vcAttributes],
					(CASE 
						WHEN ISNULL(I.bitKitParent,0) = 1 
						THEN 
							(
								CASE
									WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND ISNULL(Item.bitKitParent,0) = 1) > 0
									THEN 1
									ELSE 0
								END
							)
						ELSE 0 
					END) bitHasKitAsChild
				FROM  
					(' + @strSQL +') AS TEMPMATRIX
				INNER JOIN    
					Item I
				ON
					TEMPMATRIX.numItemCode = I.numItemCode
				LEFT JOIN 
					DivisionMaster D 
				ON 
					I.numVendorID = D.numDivisionID
				LEFT JOIN 
					CompanyInfo C 
				ON 
					C.numCompanyID = D.numCompanyID
				WHERE  
				  ((ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%'') OR ' + @vcAttribureSearchCondition + ')'

				

                INSERT  INTO @TableRowCount
                        EXEC
                            ( 'SELECT COUNT(*) FROM ( ' + @strSQL
                                + ') as t2'
                                    );

                SELECT  @TotalCount = Value
                FROM    @TableRowCount;

                SET @strNewQuery = 'select * from (' + @strSQL
                    + ') as t where SRNO> '
                    + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                    + ' and SRNO < '
                    + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                    + ' order by  vcItemName'

				--PRINT @strNewQuery
                EXEC(@strNewQuery)
				END
			ELSE
				BEGIN

				SET @strSQL = 'SELECT
									I.numItemCode,
									I.numVendorID,
									MIN(WHT.numWareHouseItemID) as numWareHouseItemID,
									dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [Attributes]
								FROM
									dbo.Item I WITH (NOLOCK)
								INNER JOIN
									dbo.Vendor V
								ON
									V.numItemCode = I.numItemCode
								LEFT JOIN
									dbo.WareHouseItems WHT
								ON
									I.numItemCode = WHT.numItemID
								WHERE 
									((ISNULL(I.numItemGroup,0) > 0 AND I.vcItemName LIKE ''%' + @itemSearchText + '%'') OR (ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%''))
									AND ISNULL(I.Isarchieve, 0) <> 1
									AND I.charItemType NOT IN ( ''A'' )
									AND I.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + '
									AND (WHT.numWareHouseID = ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'

				--- added Asset validation  by sojan
                IF @tintSearchOrderCustomerHistory = 1 AND @numDivisionID > 0
                BEGIN
                    SET @strSQL = @strSQL
                        + ' and I.numItemCode in (select distinct oppI.numItemCode from 
						OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                        + CONVERT(VARCHAR(20), @numDomainID)
                        + ' and oppM.numDivisionId='
                        + CONVERT(VARCHAR(20), @numDivisionId)
                        + ')'
                END

				SET @strSQL = @strSQL + ' GROUP BY
										I.numItemCode,
										I.numVendorID,
										dbo.fn_GetAttributes(numWareHouseItemId,I.bitSerialized)'

				SET @strSQL = 
				'
				SELECT    
					SRNO=ROW_NUMBER() OVER( ORDER BY  I.vcItemName),
					I.numItemCode,
					(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForImage ,
					(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForTImage ,
					ISNULL(vcItemName, '''') vcItemName ,
					CAST(CAST(CASE WHEN I.[charItemType] = ''P''
					THEN ISNULL((SELECT TOP 1 ROUND(monWListPrice,2) FROM [WareHouseItems] WHERE [numItemID] = I.numItemCode), 0)
					ELSE monListPrice
					END AS DEIMCAL(20,' + CAST(@tintDecimalPoints AS VARCHAR) + ')) AS VARCHAR) AS monListPrice,
					ISNULL(vcSKU, '''') vcSKU ,
					ISNULL(numBarCodeId, 0) numBarCodeId ,
					ISNULL(vcModelID, '''') vcModelID ,
					ISNULL(txtItemDesc, '''') AS txtItemDesc ,
					ISNULL(C.vcCompanyName, '''') AS vcCompanyName,
					TEMPMATRIX.numWareHouseItemID,
					TEMPMATRIX.Attributes AS [vcAttributes],
					(CASE 
						WHEN ISNULL(I.bitKitParent,0) = 1 
						THEN 
							(
								CASE
									WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND ISNULL(Item.bitKitParent,0) = 1) > 0
									THEN 1
									ELSE 0
								END
							)
						ELSE 0 
					END) bitHasKitAsChild
				FROM  
					(' + @strSQL +') AS TEMPMATRIX
				INNER JOIN    
					Item I
				ON
					TEMPMATRIX.numItemCode = I.numItemCode
				LEFT JOIN 
					DivisionMaster D 
				ON 
					I.numVendorID = D.numDivisionID
				LEFT JOIN 
					CompanyInfo C 
				ON 
					C.numCompanyID = D.numCompanyID
				WHERE  
				 ((ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%'') OR ' + @vcAttribureSearchCondition + ')'

                INSERT  INTO @TableRowCount
                        EXEC
                            ( 'SELECT COUNT(*) FROM ( ' + @strSQL
                                + ') as t2'
                                    );

                SELECT  @TotalCount = Value
                FROM    @TableRowCount;

                SET @strNewQuery = 'select * from (' + @strSQL
                    + ') as t where SRNO> '
                    + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                    + ' and SRNO < '
                    + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                    + ' order by  vcItemName'

				--PRINT @strNewQuery
                EXEC(@strNewQuery)
			END	
        END
        ELSE
        BEGIN
				DECLARE @vcWareHouseSearch VARCHAR(1000)
				DECLARE @vcVendorSearch VARCHAR(1000)
               

				SET @vcWareHouseSearch = ''
				SET @vcVendorSearch = ''

                DECLARE @tintOrder AS INT
                DECLARE @Fld_id AS NVARCHAR(20)
                DECLARE @Fld_Name AS VARCHAR(20)
                SET @strSQL = ''
                SELECT TOP 1
                        @tintOrder = tintOrder + 1 ,
                        @Fld_id = numFieldId ,
                        @Fld_Name = vcFieldName
                FROM    #Temp1
                WHERE   Custom = 1
                ORDER BY tintOrder
                WHILE @tintOrder > 0
                    BEGIN
                        SET @strSQL = @strSQL + ', dbo.GetCustFldValueItem('
                            + @Fld_id + ', I.numItemCode) as [' + @Fld_Name
                            + ']'

                        SELECT TOP 1
                                @tintOrder = tintOrder + 1 ,
                                @Fld_id = numFieldId ,
                                @Fld_Name = vcFieldName
                        FROM    #Temp1
                        WHERE   Custom = 1
                                AND tintOrder >= @tintOrder
                        ORDER BY tintOrder
                        IF @@rowcount = 0
                            SET @tintOrder = 0
                    END


				--Temp table for Item Search Configuration

                SELECT  *
                INTO    #tempSearch
                FROM    ( SELECT    numFieldId ,
                                    vcDbColumnName ,
                                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                                    tintRow AS tintOrder ,
                                    0 AS Custom
                          FROM      View_DynamicColumns
                          WHERE     numFormId = 22
                                    AND numDomainID = @numDomainID
                                    AND tintPageType = 1
                                    AND bitCustom = 0
                                    AND ISNULL(bitSettingField, 0) = 1
                                    AND numRelCntType = 1
                          UNION
                          SELECT    numFieldId ,
                                    vcFieldName ,
                                    vcFieldName ,
                                    tintRow AS tintOrder ,
                                    1 AS Custom
                          FROM      View_DynamicCustomColumns
                          WHERE     Grp_id = 5
                                    AND numFormId = 22
                                    AND numDomainID = @numDomainID
                                    AND tintPageType = 1
                                    AND bitCustom = 1
                                    AND numRelCntType = 1
                        ) X 
  
                IF NOT EXISTS ( SELECT  *
                                FROM    #tempSearch )
                    BEGIN
                        INSERT  INTO #tempSearch
                                SELECT  numFieldId ,
                                        vcDbColumnName ,
                                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                                        tintorder ,
                                        0
                                FROM    View_DynamicDefaultColumns
                                WHERE   numFormId = 22
                                        AND bitDefault = 1
                                        AND ISNULL(bitSettingField, 0) = 1
                                        AND numDomainID = @numDomainID 
                    END

				--Regular Search
                DECLARE @strSearch AS VARCHAR(8000) ,
                    @CustomSearch AS VARCHAR(4000) ,
                    @numFieldId AS NUMERIC(9)
                SET @strSearch = ''
                SELECT TOP 1
                        @tintOrder = tintOrder + 1 ,
                        @Fld_Name = vcDbColumnName ,
                        @numFieldId = numFieldId
                FROM    #tempSearch
                WHERE   Custom = 0
                ORDER BY tintOrder
                WHILE @tintOrder > -1
                    BEGIN
                        IF @Fld_Name = 'vcPartNo'
						BEGIN
                            INSERT  INTO #tempItemCode
                                    SELECT DISTINCT
                                            Item.numItemCode
                                    FROM    Vendor
                                            JOIN Item ON Item.numItemCode = Vendor.numItemCode
                                    WHERE   Item.numDomainID = @numDomainID
                                            AND Vendor.numDomainID = @numDomainID
                                            AND Vendor.vcPartNo IS NOT NULL
                                            AND LEN(Vendor.vcPartNo) > 0
                                            AND vcPartNo LIKE '%' + @str + '%'
							
							SET @vcVendorSearch = 'dbo.Vendor.vcPartNo LIKE ''%' + @str + '%'''
							
						END
                        ELSE IF @Fld_Name = 'vcBarCode'
						BEGIN
                            INSERT  INTO #tempItemCode
                                    SELECT DISTINCT
                                            Item.numItemCode
                                    FROM    Item
                                            JOIN WareHouseItems WI ON WI.numDomainID = @numDomainID
                                                            AND Item.numItemCode = WI.numItemID
                                            JOIN Warehouses W ON W.numDomainID = @numDomainID
                                                            AND W.numWareHouseID = WI.numWareHouseID
                                    WHERE   Item.numDomainID = @numDomainID
                                            AND ISNULL(Item.numItemGroup,
                                                        0) > 0
                                            AND WI.vcBarCode IS NOT NULL
                                            AND LEN(WI.vcBarCode) > 0
                                            AND WI.vcBarCode LIKE '%' + @str + '%'

							SET @vcWareHouseSearch = 'dbo.WareHouseItems.vcBarCode LIKE ''%' + @str + '%'''
						END
                        ELSE IF @Fld_Name = 'vcWHSKU'
						BEGIN
                            INSERT  INTO #tempItemCode
                                    SELECT DISTINCT
                                            Item.numItemCode
                                    FROM    Item
                                            JOIN WareHouseItems WI ON WI.numDomainID = @numDomainID
                                                        AND Item.numItemCode = WI.numItemID
                                            JOIN Warehouses W ON W.numDomainID = @numDomainID
                                                        AND W.numWareHouseID = WI.numWareHouseID
                                    WHERE   Item.numDomainID = @numDomainID
                                            AND ISNULL(Item.numItemGroup,
                                                        0) > 0
                                            AND WI.vcWHSKU IS NOT NULL
                                            AND LEN(WI.vcWHSKU) > 0
                                            AND WI.vcWHSKU LIKE '%'
                                            + @str + '%'

							IF LEN(@vcWareHouseSearch) > 0
								SET @vcWareHouseSearch = @vcWareHouseSearch + ' OR dbo.WareHouseItems.vcWHSKU LIKE ''%' + @str + '%'''
							ELSE
								SET @vcWareHouseSearch = 'dbo.WareHouseItems.vcWHSKU LIKE ''%' + @str + '%'''
						END
                        ELSE
                            SET @strSearch = @strSearch
                                + (CASE @Fld_Name WHEN 'numItemCode' THEN ' I.[' + @Fld_Name ELSE ' [' + @Fld_Name END)  + '] LIKE ''%' + @str
                                + '%'''

                        SELECT TOP 1
                                @tintOrder = tintOrder + 1 ,
                                @Fld_Name = vcDbColumnName ,
                                @numFieldId = numFieldId
                        FROM    #tempSearch
                        WHERE   tintOrder >= @tintOrder
                                AND Custom = 0
                        ORDER BY tintOrder
                        IF @@rowcount = 0
                            BEGIN
                                SET @tintOrder = -1
                            END
                        ELSE
                            IF @Fld_Name != 'vcPartNo'
                                AND @Fld_Name != 'vcBarCode'
                                AND @Fld_Name != 'vcWHSKU'
                                BEGIN
                                    SET @strSearch = @strSearch + ' or '
                                END
                    END

                IF ( SELECT COUNT(*)
                     FROM   #tempItemCode
                   ) > 0
                    SET @strSearch = @strSearch
                        + ' or  I.numItemCode in (select numItemCode from #tempItemCode)' 

				--Custom Search
                SET @CustomSearch = ''
                SELECT TOP 1
                        @tintOrder = tintOrder + 1 ,
                        @Fld_Name = vcDbColumnName ,
                        @numFieldId = numFieldId
                FROM    #tempSearch
                WHERE   Custom = 1
                ORDER BY tintOrder
                WHILE @tintOrder > -1
                    BEGIN
    
                        SET @CustomSearch = @CustomSearch
                            + CAST(@numFieldId AS VARCHAR(10)) 

                        SELECT TOP 1
                                @tintOrder = tintOrder + 1 ,
                                @Fld_Name = vcDbColumnName ,
                                @numFieldId = numFieldId
                        FROM    #tempSearch
                        WHERE   tintOrder >= @tintOrder
                                AND Custom = 1
                        ORDER BY tintOrder
                        IF @@rowcount = 0
                            BEGIN
                                SET @tintOrder = -1
                            END
                        ELSE
                            BEGIN
                                SET @CustomSearch = @CustomSearch + ' , '
                            END
                    END
	
                IF LEN(@CustomSearch) > 0
                    BEGIN
                        SET @CustomSearch = ' I.numItemCode in (SELECT RecId FROM dbo.CFW_FLD_Values_Item WHERE Fld_ID IN ('
                            + @CustomSearch + ') and dbo.GetCustFldValueItem(Fld_ID,RecId) like ''%'
                            + @str + '%'')'

                        IF LEN(@strSearch) > 0
                            SET @strSearch = @strSearch + ' OR '
                                + @CustomSearch
                        ELSE
                            SET @strSearch = @CustomSearch  
                    END

				DECLARE @numCompany AS NUMERIC(18)
				SELECT @numCompany = numCompanyID FROM DivisionMaster WHERE numDivisionID = @numDivisionId
						 
                IF @tintOppType = 1 OR (@bitRemoveVendorPOValidation = 1 AND @tintOppType = 2)
                BEGIN    
					SET @strSQLCount = 'SET @TotalCount = ISNULL((SELECT 
											COUNT(I.numItemCode) 
										FROM 
											Item  I WITH (NOLOCK)
										LEFT JOIN 
											DivisionMaster D              
										ON 
											I.numVendorID=D.numDivisionID  
										LEFT JOIN 
											CompanyInfo C  
										ON 
											C.numCompanyID=D.numCompanyID 
										LEFT JOIN 
											CustomerPartNumber  CPN  
										ON 
											I.numItemCode=CPN.numItemCode AND CPN.numCompanyID=' + CAST(ISNULL(@numCompany,0) AS VARCHAR) +  '
										LEFT JOIN
											dbo.WareHouseItems WHT
										ON
											WHT.numItemID = I.numItemCode AND
											WHT.numWareHouseItemID = (' +
																			CASE LEN(@vcWareHouseSearch)
																			WHEN 0 
																			THEN 
																				'SELECT 
																					TOP 1 numWareHouseItemID 
																				FROM 
																					dbo.WareHouseItems 
																				WHERE 
																					dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																					AND dbo.WareHouseItems.numItemID = I.numItemCode 
																					AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'
																			ELSE
																				'CASE 
																					WHEN (SELECT 
																								COUNT(*) 
																							FROM 
																								dbo.WareHouseItems 
																							WHERE 
																								dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																								AND dbo.WareHouseItems.numItemID = I.numItemCode 
																								AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)' +
																								' AND (' + @vcWareHouseSearch + ')) > 0 
																					THEN
																						(SELECT 
																							TOP 1 numWareHouseItemID 
																						FROM 
																							dbo.WareHouseItems 
																						WHERE 
																							dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																							AND dbo.WareHouseItems.numItemID = I.numItemCode 
																							AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) AND (' + @vcWareHouseSearch + '))
																					ELSE
																						(SELECT 
																							TOP 1 numWareHouseItemID 
																						FROM 
																							dbo.WareHouseItems 
																						WHERE 
																							dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																							AND dbo.WareHouseItems.numItemID = I.numItemCode 
																							AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0))
																					END' 
																			END
																		+
																	') 
										where ((I.charItemType <> ''P'') OR ((SELECT COUNT(*) FROM dbo.WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
									  + CONVERT(VARCHAR(20), @numDomainID)
									  + ' AND numWareHouseID =  ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) > 0)) AND ISNULL(I.Isarchieve,0) <> 1 And I.charItemType NOT IN(''A'') AND  I.numDomainID='
									  + CONVERT(VARCHAR(20), @numDomainID) + ' and ('
									  + @strSearch + ') '   
						     
						--- added Asset validation  by sojan
					IF @bitRemoveVendorPOValidation = 1 AND @tintOppType = 2
                            SET @strSQLCount = @strSQLCount + ' AND ISNULL(I.bitKitParent,0) = 0 AND ISNULL(I.bitAssembly,0) = 0 '

                    IF @tintSearchOrderCustomerHistory = 1 AND @numDivisionID > 0
                    BEGIN
                        SET @strSQLCount = @strSQLCount
                            + ' AND I.numItemCode in (select distinct oppI.numItemCode from 
							OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                            + CONVERT(VARCHAR(20), @numDomainID)
                            + ' and oppM.numDivisionId='
                            + CONVERT(VARCHAR(20), @numDivisionId)
                            + ')'
                    END

					SET @strSQLCount = @strSQLCount + '),0);'

                        SET @strSQL = 'select I.numItemCode, ISNULL(CPN.CustomerPartNo,'''') AS CustomerPartNo,
									  (SELECT TOP 1 vcPathForImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForImage,
									  (SELECT TOP 1 vcPathForTImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForTImage,
									  isnull(vcItemName,'''') vcItemName,
									  CAST(CAST(CASE 
									  WHEN I.[charItemType]=''P''
									  THEN ISNULL((SELECT TOP 1 monWListPrice FROM [WareHouseItems] WHERE [numItemID]=I.numItemCode),0) 
									  ELSE 
										monListPrice 
									  END AS DECIMAL(20,' + CAST(@tintDecimalPoints AS VARCHAR) +')) AS VARCHAR) AS monListPrice,
									  isnull(vcSKU,'''') vcSKU,
									  isnull(numBarCodeId,0) numBarCodeId,
									  isnull(vcModelID,'''') vcModelID,
									  isnull(txtItemDesc,'''') as txtItemDesc,
									  isnull(C.vcCompanyName,'''') as vcCompanyName,
									  WHT.numWareHouseItemID,
									  dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [vcAttributes],
									  (CASE 
											WHEN ISNULL(I.bitKitParent,0) = 1 
											THEN 
												(
													CASE
														WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND ISNULL(Item.bitKitParent,0) = 1) > 0
														THEN 1
														ELSE 0
													END
												)
											ELSE 0 
										END) bitHasKitAsChild,ISNULL(I.bitMatrix,0) AS bitMatrix,ISNULL(I.numItemGroup,0) AS numItemGroup '
									  + @strSQL
									  + ' INTO #TEMPItems FROM Item  I WITH (NOLOCK)
									  Left join DivisionMaster D              
									  on I.numVendorID=D.numDivisionID  
									  left join CompanyInfo C  
									  on C.numCompanyID=D.numCompanyID 
									 LEFT JOIN CustomerPartNumber  CPN  
									 ON I.numItemCode=CPN.numItemCode AND CPN.numCompanyID=' + CAST(ISNULL(@numCompany,0) AS VARCHAR) +  '
									  LEFT JOIN
											dbo.WareHouseItems WHT
									  ON
											WHT.numItemID = I.numItemCode AND
											WHT.numWareHouseItemID = (' +
																			CASE LEN(@vcWareHouseSearch)
																			WHEN 0 
																			THEN 
																				'SELECT 
																					TOP 1 numWareHouseItemID 
																				FROM 
																					dbo.WareHouseItems 
																				WHERE 
																					dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																					AND dbo.WareHouseItems.numItemID = I.numItemCode 
																					AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'
																			ELSE
																				'CASE 
																					WHEN (SELECT 
																								COUNT(*) 
																							FROM 
																								dbo.WareHouseItems 
																							WHERE 
																								dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																								AND dbo.WareHouseItems.numItemID = I.numItemCode 
																								AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)' +
																								' AND (' + @vcWareHouseSearch + ')) > 0 
																					THEN
																						(SELECT 
																							TOP 1 numWareHouseItemID 
																						FROM 
																							dbo.WareHouseItems 
																						WHERE 
																							dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																							AND dbo.WareHouseItems.numItemID = I.numItemCode 
																							AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) AND (' + @vcWareHouseSearch + '))
																					ELSE
																						(SELECT 
																							TOP 1 numWareHouseItemID 
																						FROM 
																							dbo.WareHouseItems 
																						WHERE 
																							dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																							AND dbo.WareHouseItems.numItemID = I.numItemCode 
																							AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0))
																					END' 
																			END
																		+
																	') 
										where ((I.charItemType <> ''P'') OR ((SELECT COUNT(*) FROM dbo.WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
									  + CONVERT(VARCHAR(20), @numDomainID)
									  + ' AND numWareHouseID =  ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) > 0)) AND ISNULL(I.Isarchieve,0) <> 1 And I.charItemType NOT IN(''A'') AND  I.numDomainID='
									  + CONVERT(VARCHAR(20), @numDomainID) + ' and ('
									  + @strSearch + ') '   
						     
						--- added Asset validation  by sojan
                        IF @bitRemoveVendorPOValidation = 1 AND @tintOppType = 2
						BEGIN
                            SET @strSQL = @strSQL + ' and ISNULL(I.bitKitParent,0) = 0 AND ISNULL(I.bitAssembly,0) = 0 '

							IF ISNULL(@bitTransferTo,0) = 1
							BEGIN
								SET @strSQL = @strSQL + ' and ISNULL(I.bitLotNo,0) = 0 AND ISNULL(I.bitSerialized,0) = 0 AND I.charItemType = ''P'' '
							END
						END

                        IF @tintSearchOrderCustomerHistory = 1
                            AND @numDivisionID > 0
                            BEGIN
                                SET @strSQL = @strSQL
                                    + ' and I.numItemCode in (select distinct oppI.numItemCode from 
									OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                                    + CONVERT(VARCHAR(20), @numDomainID)
                                    + ' and oppM.numDivisionId='
                                    + CONVERT(VARCHAR(20), @numDivisionId)
                                    + ')'
                            END

						IF @numDomainID <> 204
						BEGIN
							SET @strSQL = @strSQL + CONCAT(' ORDER BY vcItemName OFFSET ', (@numPageIndex - 1) * @numPageSize,' ROWS FETCH NEXT ',@numPageSize,' ROWS ONLY ')
						END
						ELSE
						BEGIN
							SET @strSQL = @strSQL + 'DELETE FROM 
														#TEMPItems
													WHERE 
														numItemCode IN (
																			SELECT 
																				F.numItemCode
																			FROM 
																				#TEMPItems AS F
																			WHERE 
																				ISNULL(F.bitMatrix,0) = 1 AND
																				EXISTS (
																							SELECT 
																								t1.vcItemName, t1.bitMatrix, t1.numItemGroup, Count(t1.numItemCode)
																							FROM 
																								#TEMPItems t1
																							WHERE 
																								t1.vcItemName = F.vcItemName
																								AND t1.bitMatrix = 1
																								AND t1.numItemGroup = F.numItemGroup
																							GROUP BY 
																								t1.vcItemName, t1.bitMatrix, t1.numItemGroup
																							HAVING 
																								Count(t1.numItemCode) > 1
																						)
																		)
														AND numItemCode NOT IN (
																								SELECT 
																									Min(numItemCode)
																								FROM 
																									#TEMPItems AS F
																								WHERE
																									ISNULL(F.bitMatrix,0) = 1 AND
																									Exists (
																												SELECT 
																													t2.vcItemName, t2.bitMatrix, t2.numItemGroup, Count(t2.numItemCode)
																												FROM 
																													#TEMPItems t2
																												WHERE 
																													t2.vcItemName = F.vcItemName
																												   AND t2.bitMatrix = 1
																												   AND t2.numItemGroup = F.numItemGroup
																												GROUP BY 
																													t2.vcItemName, t2.bitMatrix, t2.numItemGroup
																												HAVING 
																													Count(t2.numItemCode) > 1
																											)
																								GROUP BY 
																									vcItemName, bitMatrix, numItemGroup
																							);'

						END

						SET @strSQL = @strSQL + ' SELECT SRNO=ROW_NUMBER() OVER( ORDER BY vcItemName),(CASE WHEN (vcSKU=''' + @str + ''' OR ' + CAST(ISNULL(@numDomainID,0) AS VARCHAR) + '=209) THEN 1 ELSE 0 END) bitSKUMatch,* INTO #TEMPItemsFinal FROM #TEMPItems;'

						IF @numDomainID <> 204
						BEGIN
							SET @strSQL = @strSQL + @strSQLCount

							 SET @strSQL = @strSQL + ' SELECT * FROM #TEMPItemsFinal ORDER BY SRNO; DROP TABLE #TEMPItemsFinal; DROP TABLE #TEMPItems;'
						END
						ELSE
						BEGIn
							SET @strSQL = @strSQL + 'SELECT @TotalCount = COUNT(*) FROM #TEMPItemsFinal;'

							 SET @strSQL = @strSQL + ' SELECT * FROM #TEMPItemsFinal where SRNO> '
                            + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                            + ' and SRNO < '
                            + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                            + ' order by  SRNO; DROP TABLE #TEMPItemsFinal; DROP TABLE #TEMPItems;'
						END
						
						--PRINT @strSQL
						EXEC sp_executeSQL @strSQL, N'@TotalCount INT OUTPUT', @TotalCount OUTPUT
                END      
                ELSE IF @tintOppType = 2
                BEGIN     
					SET @strSQLCount = 'SET @TotalCount = ISNULL((SELECT 
																	COUNT(I.numItemCode) 
																FROM 
																	Item I  WITH (NOLOCK)
																INNER JOIN
																	dbo.Vendor V
																ON
																	V.numItemCode = I.numItemCode
																LEFT JOIN 
																	DivisionMaster D              
																ON 
																	V.numVendorID=D.numDivisionID  
																LEFT join 
																	CompanyInfo C  
																ON 
																	C.numCompanyID=D.numCompanyID
																LEFT JOIN 
																	CustomerPartNumber  CPN  
																ON 
																	I.numItemCode=CPN.numItemCode 
																	AND CPN.numCompanyID=' + CAST(ISNULL(@numCompany,0) AS VARCHAR) +  ' 
																LEFT JOIN
																	dbo.WareHouseItems WHT
																ON
																	WHT.numItemID = I.numItemCode AND
																	WHT.numWareHouseItemID = (' +
																								CASE LEN(@vcWareHouseSearch)
																								WHEN 0 
																								THEN 
																									'SELECT 
																										TOP 1 numWareHouseItemID 
																									FROM 
																										dbo.WareHouseItems 
																									WHERE 
																										dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																										AND dbo.WareHouseItems.numItemID = I.numItemCode 
																										AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'
																								ELSE
																									'CASE 
																										WHEN (SELECT 
																													COUNT(*) 
																												FROM 
																													dbo.WareHouseItems 
																												WHERE 
																													dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																													AND dbo.WareHouseItems.numItemID = I.numItemCode 
																													AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)' +
																													' AND (' + @vcWareHouseSearch + ')) > 0 
																										THEN
																											(SELECT 
																												TOP 1 numWareHouseItemID 
																											FROM 
																												dbo.WareHouseItems 
																											WHERE 
																												dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																												AND dbo.WareHouseItems.numItemID = I.numItemCode 
																												AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) AND (' + @vcWareHouseSearch + '))
																										ELSE
																											(SELECT 
																												TOP 1 numWareHouseItemID 
																											FROM 
																												dbo.WareHouseItems 
																											WHERE 
																												dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																												AND dbo.WareHouseItems.numItemID = I.numItemCode 
																												AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0))
																										END' 
																								END
																							+
																						')
										       
																		WHERE (
																				(I.charItemType <> ''P'') OR 
																				((SELECT COUNT(*) FROM dbo.WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
																				+ CONVERT(VARCHAR(20), @numDomainID)
																				+ ' AND numWareHouseID =  ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) > 0)) AND ISNULL(I.Isarchieve,0) <> 1 and ISNULL(I.bitKitParent,0) = 0  AND ISNULL(I.bitAssembly,0) = 0 And I.charItemType NOT IN(''A'') AND V.numVendorID='
																				+ CONVERT(VARCHAR(20), @numDivisionID)
																				+ ' and ('+ CASE LEN(@vcVendorSearch)
																								WHEN 0 THEN ''
																								ELSE REPLACE(@vcVendorSearch,'dbo.Vendor','V') + ' OR '
																								END + @strSearch + ') ' 

					--- added Asset validation  by sojan
                    IF @tintSearchOrderCustomerHistory = 1 AND @numDivisionID > 0
                    BEGIN
                            SET @strSQLCount = @strSQLCount
                                + ' and I.numItemCode in (select distinct oppI.numItemCode from 
							OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                                + CONVERT(VARCHAR(20), @numDomainID)
                                + ' and oppM.numDivisionId='
                                + CONVERT(VARCHAR(20), @numDivisionId)
                                + ')'
                    END
				 
                    SET @strSQL = 'SELECT I.numItemCode,
									ISNULL(CPN.CustomerPartNo,'''') AS CustomerPartNo,
									(SELECT TOP 1 vcPathForImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForImage,
									(SELECT TOP 1 vcPathForTImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForTImage,
									ISNULL(vcItemName,'''') vcItemName,
									CAST(CAST(monListPrice AS DECIMAL(20,' + CAST(@tintDecimalPoints AS VARCHAR) + ')) AS VARCHAR) as monListPrice,
									isnull(vcSKU,'''') vcSKU,
									isnull(numBarCodeId,0) numBarCodeId,
									isnull(vcModelID,'''') vcModelID,
									isnull(txtItemDesc,'''') as txtItemDesc,
									isnull(C.vcCompanyName,'''') as vcCompanyName,
									WHT.numWareHouseItemID,
									dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [vcAttributes],
									(CASE 
										WHEN ISNULL(I.bitKitParent,0) = 1 
										THEN 
											(
												CASE
													WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND ISNULL(Item.bitKitParent,0) = 1) > 0
													THEN 1
													ELSE 0
												END
											)
										ELSE 0 
									END) bitHasKitAsChild,ISNULL(I.bitMatrix,0) AS bitMatrix,ISNULL(I.numItemGroup,0) AS numItemGroup'
									+ @strSQL
									+ ' INTO #TEMPItems from item I  WITH (NOLOCK)
									INNER JOIN
									dbo.Vendor V
									ON
									V.numItemCode = I.numItemCode
									Left join 
									DivisionMaster D              
									ON 
									V.numVendorID=D.numDivisionID  
									LEFT join 
									CompanyInfo C  
									ON 
									C.numCompanyID=D.numCompanyID
									LEFT JOIN 
									CustomerPartNumber  CPN  
									ON 
									I.numItemCode=CPN.numItemCode 
									AND CPN.numCompanyID=' + CAST(ISNULL(@numCompany,0) AS VARCHAR) +  ' 
									LEFT JOIN
									dbo.WareHouseItems WHT
									ON
									WHT.numItemID = I.numItemCode AND
									WHT.numWareHouseItemID = (' +
																	CASE LEN(@vcWareHouseSearch)
																	WHEN 0 
																	THEN 
																		'SELECT 
																			TOP 1 numWareHouseItemID 
																		FROM 
																			dbo.WareHouseItems 
																		WHERE 
																			dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																			AND dbo.WareHouseItems.numItemID = I.numItemCode 
																			AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'
																	ELSE
																		'CASE 
																			WHEN (SELECT 
																						COUNT(*) 
																					FROM 
																						dbo.WareHouseItems 
																					WHERE 
																						dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																						AND dbo.WareHouseItems.numItemID = I.numItemCode 
																						AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)' +
																						' AND (' + @vcWareHouseSearch + ')) > 0 
																			THEN
																				(SELECT 
																					TOP 1 numWareHouseItemID 
																				FROM 
																					dbo.WareHouseItems 
																				WHERE 
																					dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																					AND dbo.WareHouseItems.numItemID = I.numItemCode 
																					AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) AND (' + @vcWareHouseSearch + '))
																			ELSE
																				(SELECT 
																					TOP 1 numWareHouseItemID 
																				FROM 
																					dbo.WareHouseItems 
																				WHERE 
																					dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																					AND dbo.WareHouseItems.numItemID = I.numItemCode 
																					AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0))
																			END' 
																	END
																+
															')
										       
									WHERE (
											(I.charItemType <> ''P'') OR 
											((SELECT COUNT(*) FROM dbo.WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
									+ CONVERT(VARCHAR(20), @numDomainID)
									+ ' AND numWareHouseID =  ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) > 0)) AND ISNULL(I.Isarchieve,0) <> 1 and ISNULL(I.bitKitParent,0) = 0  AND ISNULL(I.bitAssembly,0) = 0 And I.charItemType NOT IN(''A'') AND V.numVendorID='
									+ CONVERT(VARCHAR(20), @numDivisionID)
									+ ' and ('+ CASE LEN(@vcVendorSearch)
																	WHEN 0 THEN ''
																	ELSE REPLACE(@vcVendorSearch,'dbo.Vendor','V') + ' OR '
																	END + @strSearch + ') ' 

							--- added Asset validation  by sojan
                            IF @tintSearchOrderCustomerHistory = 1
                                AND @numDivisionID > 0
                                BEGIN
                                    SET @strSQL = @strSQL
                                        + ' and I.numItemCode in (select distinct oppI.numItemCode from 
									OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                                        + CONVERT(VARCHAR(20), @numDomainID)
                                        + ' and oppM.numDivisionId='
                                        + CONVERT(VARCHAR(20), @numDivisionId)
                                        + ')'
                                END

							
							IF ISNULL(@bitTransferTo,0) = 1
							BEGIN
								SET @strSQL = @strSQL + ' and ISNULL(I.bitLotNo,0) = 0 AND ISNULL(I.bitSerialized,0) = 0 AND I.charItemType = ''P'' '
							END

							IF @numDomainID <> 204
							BEGIN
								SET @strSQL = @strSQL + CONCAT(' ORDER BY vcItemName OFFSET ', (@numPageIndex - 1) * @numPageSize,' ROWS FETCH NEXT ',@numPageSize,' ROWS ONLY ')
							END
							ELSE
							BEGIN
								SET @strSQL = @strSQL + 'DELETE FROM 
															#TEMPItems
														WHERE 
															numItemCode IN (
																				SELECT 
																					F.numItemCode
																				FROM 
																					#TEMPItems AS F
																				WHERE
																					ISNULL(F.bitMatrix,0) = 1 AND
																					EXISTS (
																								SELECT 
																									t1.vcItemName, t1.bitMatrix, t1.numItemGroup, Count(t1.numItemCode)
																								FROM 
																									#TEMPItems t1
																								WHERE 
																									t1.vcItemName = F.vcItemName
																									AND t1.bitMatrix = 1
																									AND t1.numItemGroup = F.numItemGroup
																								GROUP BY 
																									t1.vcItemName, t1.bitMatrix, t1.numItemGroup
																								HAVING 
																									Count(t1.numItemCode) > 1
																							)
																			)
															AND numItemCode NOT IN (
																									SELECT 
																										Min(numItemCode)
																									FROM 
																										#TEMPItems AS F
																									WHERE
																										ISNULL(F.bitMatrix,0) = 1 AND
																										Exists (
																													SELECT 
																														t2.vcItemName, t2.bitMatrix, t2.numItemGroup, Count(t2.numItemCode)
																													FROM 
																														#TEMPItems t2
																													WHERE 
																														t2.vcItemName = F.vcItemName
																													   AND t2.bitMatrix = 1
																													   AND t2.numItemGroup = F.numItemGroup
																													GROUP BY 
																														t2.vcItemName, t2.bitMatrix, t2.numItemGroup
																													HAVING 
																														Count(t2.numItemCode) > 1
																												)
																									GROUP BY 
																										vcItemName, bitMatrix, numItemGroup
																								);'
							END

							SET @strSQL = @strSQL + ' SELECT SRNO=ROW_NUMBER() OVER( ORDER BY vcItemName),(CASE WHEN (vcSKU=''' + @str + ''' OR ' + CAST(ISNULL(@numDomainID,0) AS VARCHAR) + '=209) THEN 1 ELSE 0 END) bitSKUMatch,* INTO #TEMPItemsFinal FROM #TEMPItems;'


							IF @numDomainID <> 204
							BEGIN
								SET @strSQL = @strSQL + @strSQLCount

								SET @strSQL = @strSQL + ' SELECT * FROM #TEMPItemsFinal ORDER BY SRNO; DROP TABLE #TEMPItemsFinal; DROP TABLE #TEMPItems;'
							END
							ELSE
							BEGIn
								SET @strSQL = @strSQL + 'SELECT @TotalCount = COUNT(*) FROM #TEMPItemsFinal;'

								SET @strSQL = @strSQL + ' SELECT * FROM #TEMPItemsFinal where SRNO> '
								+ CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
								+ ' and SRNO < '
								+ CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
								+ ' order by  vcItemName; DROP TABLE #TEMPItemsFinal; DROP TABLE #TEMPItems;'
							END

							
                        
							EXEC sp_executeSQL @strSQL, N'@TotalCount INT OUTPUT', @TotalCount OUTPUT
                        END
                    ELSE
                        SELECT  0  
            END
    END
ELSE
    SELECT  0  

SELECT  *
FROM    #Temp1
WHERE   vcDbColumnName NOT IN ( 'vcPartNo', 'vcBarCode', 'vcWHSKU' )ORDER BY tintOrder 
   
   PRINT(CAST (@strSQL AS NTEXT))

IF OBJECT_ID('dbo.Scores', 'U') IS NOT NULL
  DROP TABLE dbo.Scores

IF OBJECT_ID('tempdb..#Temp1') IS NOT NULL
BEGIN
    DROP TABLE #Temp1
END

IF OBJECT_ID('tempdb..#tempSearch') IS NOT NULL
BEGIN
    DROP TABLE #tempSearch
END

IF OBJECT_ID('tempdb..#tempItemCode') IS NOT NULL
BEGIN
    DROP TABLE #tempItemCode
END


    END
--Created By Anoop Jayaraj          
--exec USP_ItemDetailsForEcomm @numItemCode=859064,@numWareHouseID=1074,@numDomainID=172,@numSiteId=90
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itemdetailsforecomm')
DROP PROCEDURE USP_ItemDetailsForEcomm
GO
CREATE PROCEDURE [dbo].[USP_ItemDetailsForEcomm]
@numItemCode as numeric(9),          
@numWareHouseID as numeric(9),    
@numDomainID as numeric(9),
@numSiteId as numeric(9),
@vcCookieId as VARCHAR(MAX)=null,
@numUserCntID AS NUMERIC(9)=0,
@numDivisionID AS NUMERIC(18,0) = 0                                        
AS     
BEGIN
/*Removed warehouse parameter, and taking Default warehouse by default from DB*/
    
	DECLARE @bitShowInStock AS BIT        
	DECLARE @bitShowQuantity AS BIT
	DECLARE @bitAutoSelectWarehouse AS BIT              
	DECLARE @numDefaultWareHouseID AS NUMERIC(9)
	DECLARE @numDefaultRelationship AS NUMERIC(18,0)
	DECLARE @numDefaultProfile AS NUMERIC(18,0)
	DECLARE @tintPreLoginPriceLevel AS TINYINT
	DECLARE @tintPriceLevel AS TINYINT
	DECLARE @bitMatrix BIT
	DECLARE @numItemGroup NUMERIC(18,0)
	SET @bitShowInStock=0        
	SET @bitShowQuantity=0        
       
        
	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT 
			@numDefaultRelationship=ISNULL(tintCRMType,0)
			,@numDefaultProfile=ISNULL(vcProfile,0)
			,@tintPriceLevel=ISNULL(tintPriceLevel,0) 
		FROM 
			DivisionMaster 
		INNER JOIN 
			CompanyInfo 
		ON 
			DivisionMaster.numCompanyID=CompanyInfo.numCompanyId 
		WHERE 
			numDivisionID=@numDivisionID 
			AND DivisionMaster.numDomainID=@numDomainID
	END     
        
	SELECT 
		@bitShowInStock=ISNULL(bitShowInStock,0)
		,@bitShowQuantity=ISNULL(bitShowQOnHand,0)
		,@bitAutoSelectWarehouse=ISNULL(bitAutoSelectWarehouse,0)
		,@numDefaultRelationship=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultRelationship ELSE numRelationshipId END)
		,@numDefaultProfile=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultProfile ELSE numProfileId END)
		,@tintPreLoginPriceLevel=(CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN (CASE WHEN @numDivisionID > 0 THEN (CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN ISNULL(@tintPriceLevel,0) ELSE 0 END) ELSE ISNULL(tintPreLoginProceLevel,0) END) ELSE 0 END)
	FROM 
		eCommerceDTL 
	WHERE 
		numDomainID=@numDomainID

	IF @numWareHouseID=0
	BEGIN
		SELECT @numDefaultWareHouseID=ISNULL(numDefaultWareHouseID,0) FROM [eCommerceDTL] WHERE [numDomainID]=@numDomainID	
		SET @numWareHouseID =@numDefaultWareHouseID;
	END

	DECLARE @vcWarehouseIDs AS VARCHAR(1000)
	IF @bitAutoSelectWarehouse = 1
	BEGIN		
		SELECT @vcWarehouseIDs = ISNULL( STUFF((SELECT DISTINCT ',' + CAST(numWarehouseID AS VARCHAR(10)) FROM [dbo].[Warehouses] WHERE [Warehouses].[numDomainID] = @numDomainID FOR XML PATH('')),1, 1, '') , '')
	END
	ELSE
	BEGIN
		SET @vcWarehouseIDs = @numWareHouseID
	END

	/*If qty is not found in default warehouse then choose next warehouse with sufficient qty */
	IF @bitAutoSelectWarehouse = 1 AND ( SELECT COUNT(*) FROM dbo.WareHouseItems WHERE  numItemID = @numItemCode AND numWarehouseID= @numWareHouseID ) = 0 
	BEGIN
		SELECT TOP 1 
			@numDefaultWareHouseID = numWareHouseID 
		FROM 
			dbo.WareHouseItems 
		WHERE 
			numItemID = @numItemCode 
			AND numOnHand > 0 
		ORDER BY 
			numOnHand DESC 
		
		IF (ISNULL(@numDefaultWareHouseID,0)>0)
			SET @numWareHouseID =@numDefaultWareHouseID;
	END

    DECLARE @UOMConversionFactor AS DECIMAL(18,5)
      
    SELECT 
		@UOMConversionFactor= ISNULL(dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)),0)
		,@bitMatrix=ISNULL(bitMatrix,0)
		,@numItemGroup=ISNULL(numItemGroup,0)
    FROM 
		Item I 
	WHERE 
		numItemCode=@numItemCode   
	   
	DECLARE @PromotionOffers AS DECIMAL(18,2)=0     
	
	IF(@vcCookieId!=null OR @vcCookieId!='')   
	BEGIN                    
		SET @PromotionOffers=(SELECT TOP 1 
								CASE 
									WHEN P.tintDiscountType=1  
									THEN (I.monListPrice*fltDiscountValue)/100
									WHEN P.tintDiscountType=2
									THEN fltDiscountValue
									WHEN P.tintDiscountType=3
									THEN fltDiscountValue*I.monListPrice
									ELSE 0 
								END
							FROM CartItems AS C
							LEFT JOIN PromotionOffer AS P ON P.numProId=C.PromotionID
							LEFT JOIN Item I ON I.numItemCode=C.numItemCode
							WHERE 
								I.numItemCode=@numItemCode 
								AND I.numItemCode=@numDomainID 
								AND C.vcCookieId=@vcCookieId 
								AND C.numUserCntId=@numUserCntID 
								AND C.fltDiscount=0 
								AND (I.numItemCode IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=1 AND PIS.tintRecordType=6) 
									OR I.numItemClassification IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=2 AND PIS.tintRecordType=6))
								AND P.numDomainId=@numDomainID 
								AND ISNULL(bitEnabled,0)=1 
								AND ISNULL(bitAppliesToSite,0)=1 
								AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
								AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END))
	END
	
 
	DECLARE @strSql AS VARCHAR(MAX)
	
	SET @strSql=' With tblItem AS (SELECT 
										I.numItemCode
										,vcItemName
										,txtItemDesc
										,charItemType
										,ISNULL(I.bitMatrix,0) bitMatrix,' + 
										(CASE WHEN @tintPreLoginPriceLevel > 0 THEN 'ISNULL(PricingOption.monPrice,ISNULL(CASE WHEN I.[charItemType]=''P'' 
											THEN '+ convert(varchar(15),@UOMConversionFactor) + ' * W.[monWListPrice]  
											ELSE '+ convert(varchar(15),@UOMConversionFactor) + ' * monListPrice 
									   END,0))' ELSE 'ISNULL(CASE WHEN I.[charItemType]=''P'' 
											THEN '+ convert(varchar(15),@UOMConversionFactor) + ' * W.[monWListPrice]  
											ELSE '+ convert(varchar(15),@UOMConversionFactor) + ' * monListPrice 
									   END,0)' END)
										+' AS monListPrice
										,ISNULL(CASE WHEN I.[charItemType]=''P'' 
											THEN '+ convert(varchar(15),@UOMConversionFactor) + ' * W.[monWListPrice]  
											ELSE '+ convert(varchar(15),@UOMConversionFactor) + ' * monListPrice 
										END,0) AS monMSRP
										,numItemClassification
										,bitTaxable
										,vcSKU
										,bitKitParent
										,I.numModifiedBy
										,(SELECT numItemImageId,vcPathForImage,vcPathForTImage,bitDefault,case when bitdefault=1  then -1 else isnull(intDisplayOrder,0) end as intDisplayOrder FROM ItemImages  WHERE numItemCode=I.numItemCode and bitIsImage = 1 order by case when bitdefault=1 then -1 else isnull(intDisplayOrder,0) end  asc FOR XML AUTO,ROOT(''Images''))  vcImages
										,(SELECT vcPathForImage FROM ItemImages  WHERE numItemCode=I.numItemCode and bitIsImage = 0 order by intDisplayOrder  asc FOR XML AUTO,ROOT(''Documents''))  vcDocuments
										,ISNULL(bitSerialized,0) as bitSerialized
										,vcModelID
										,numItemGroup
										,(ISNULL(sum(numOnHand),0) / '+ convert(varchar(15),@UOMConversionFactor) + ') as numOnHand
										,sum(numOnOrder) as numOnOrder,                    
										sum(numReorder)  as numReorder,                    
										sum(numAllocation)  as numAllocation,                    
										sum(numBackOrder)  as numBackOrder,              
										isnull(fltWeight,0) as fltWeight,              
										isnull(fltHeight,0) as fltHeight,              
										isnull(fltWidth,0) as fltWidth,              
										isnull(fltLength,0) as fltLength,              
										case when charItemType=''P''then isnull(bitFreeShipping,0) else 1 end as bitFreeShipping,
										Case When (case when charItemType=''P''then isnull(bitFreeShipping,0) else 1 end )=1 then ''Free shipping'' else ''Calculated at checkout'' end as Shipping,              
										isnull(bitAllowBackOrder,0) as bitAllowBackOrder,bitShowInStock,bitShowQOnHand,isnull(numWareHouseItemID,0)  as numWareHouseItemID,
										(case when charItemType<>''S'' and charItemType<>''N'' then         
										 (CASE 
											WHEN I.bitAllowBackOrder = 1 THEN 1
											WHEN (ISNULL(WAll.numTotalOnHand,0)<=0) THEN 0
											ELSE 1
										END) ELSE 1 END ) as bitInStock,
										(case when charItemType<>''S'' and charItemType<>''N'' then         
										 (Case when '+ convert(varchar(15),@bitShowInStock) + '=1  then 
										 (CASE 
											WHEN I.bitAllowBackOrder = 1 AND I.numDomainID <> 172 AND ISNULL(WAll.numTotalOnHand,0)<=0 AND ISNULL(I.numVendorID,0) > 0 AND dbo.GetVendorPreferredMethodLeadTime(I.numVendorID) > 0 THEN CONCAT(''<font class="vendorLeadTime" style="color:green">Usually ships in '',dbo.GetVendorPreferredMethodLeadTime(I.numVendorID),'' days</font>'')
											WHEN I.bitAllowBackOrder = 1 AND ISNULL(WAll.numTotalOnHand,0)<=0 THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)
											WHEN (ISNULL(WAll.numTotalOnHand,0)<=0) THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:red">ON HOLD</font>'' ELSE ''<font style="color:red">Out Of Stock</font>'' END)
											ELSE (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)  
										END) else '''' end) ELSE '''' END ) as InStock,
										ISNULL(numSaleUnit,0) AS numUOM,
										ISNULL(vcUnitName,'''') AS vcUOMName,
										 '+ convert(varchar(15),@UOMConversionFactor) + ' AS UOMConversionFactor,
										I.numCreatedBy,
										I.numBarCodeId,
										I.[vcManufacturer],
										(SELECT  COUNT(*) FROM  Review where vcReferenceId = CONVERT(VARCHAR(50) ,I.numItemCode) and numDomainId = I.numDomainId and numSiteId = '+ CONVERT(VARCHAR(20) , @numSiteId)  +' AND bitHide = 0 ) as ReviewCount ,
										(SELECT  COUNT(*) FROM  Ratings where vcReferenceId = CONVERT(VARCHAR(50) ,I.numItemCode) and numDomainId = I.numDomainId and numSiteId = '+ CONVERT(VARCHAR(20) , @numSiteId)  +'  ) as RatingCount ,
										(select top 1 vcMetaKeywords  from MetaTags where numReferenceID = I.numItemCode and tintMetaTagFor =2) as vcMetaKeywords,
										(select top 1 vcMetaDescription  from MetaTags where numReferenceID = I.numItemCode and tintMetaTagFor =2) as vcMetaDescription ,
										(SELECT vcCategoryName  FROM  dbo.Category WHERE numCategoryID =(SELECT TOP 1 numCategoryID FROM dbo.ItemCategory WHERE numItemID = I.numItemCode)) as vcCategoryName,('+(SELECT CAST(ISNULL(@PromotionOffers,0) AS varchar(20)))+') as PromotionOffers

										from Item I ' + 
										(CASE WHEN @tintPreLoginPriceLevel > 0 THEN ' OUTER APPLY(SELECT decDiscount AS monPrice FROM PricingTable WHERE tintRuleType=3 AND numItemCode=I.numItemCode ORDER BY numPricingID OFFSET ' + CAST(@tintPreLoginPriceLevel-1 AS VARCHAR) + ' ROWS FETCH NEXT 1 ROWS ONLY) As PricingOption ' ELSE '' END)
										+ '                 
										left join  WareHouseItems W                
										on W.numItemID=I.numItemCode and numWareHouseID= '+ convert(varchar(15),@numWareHouseID) + '
										 OUTER APPLY (SELECT SUM(numOnHand) numTotalOnHand FROM WareHouseItems W 
																							  WHERE  W.numItemID = I.numItemCode
																							  AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
																							  AND W.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + ') AS WAll
										left join  eCommerceDTL E          
										on E.numDomainID=I.numDomainID     
										LEFT JOIN UOM u ON u.numUOMId=I.numSaleUnit    
										where ISNULL(I.IsArchieve,0)=0 AND I.numDomainID='+ convert(varchar(15),@numDomainID) + ' AND I.numItemCode='+ convert(varchar(15),@numItemCode) +
										CASE WHEN (SELECT COUNT(*) FROM [dbo].[EcommerceRelationshipProfile] WHERE numSiteID=@numSiteID AND numRelationship = @numDefaultRelationship AND numProfile = @numDefaultProfile) > 0
										THEN
											CONCAT(' AND I.numItemClassification NOT IN (SELECT numItemClassification FROM EcommerceRelationshipProfile ERP INNER JOIN ECommerceItemClassification EIC ON EIC.numECommRelatiionshipProfileID=ERP.ID WHERE ERP.numSiteID=',@numSiteID,' AND ERP.numRelationship=',@numDefaultRelationship,' AND ERP.numProfile=', @numDefaultProfile,')')
										ELSE
											''
										END
										+ '           
										group by I.numItemCode, vcItemName, txtItemDesc, charItemType, monListPrice,numItemClassification, bitTaxable, vcSKU, bitKitParent, WAll.numTotalOnHand,         
										I.numDomainID,I.numModifiedBy,  bitSerialized, vcModelID,numItemGroup, fltWeight,fltHeight,fltWidth,          
										fltLength,bitFreeShipping,bitAllowBackOrder,bitShowInStock,bitShowQOnHand ,numWareHouseItemID,vcUnitName,I.numCreatedBy,I.numBarCodeId,W.[monWListPrice],I.[vcManufacturer],
										numSaleUnit, I.numVendorID, I.bitMatrix ' + (CASE WHEN @tintPreLoginPriceLevel > 0 THEN ',PricingOption.monPrice' ELSE '' END) + ')'

										set @strSql=@strSql+' select numItemCode,vcItemName,txtItemDesc,charItemType, bitMatrix, monListPrice,monMSRP,numItemClassification, bitTaxable, vcSKU, bitKitParent,                  
										tblItem.numModifiedBy, vcImages,vcDocuments, bitSerialized, vcModelID, numItemGroup,numOnHand,numOnOrder,numReorder,numAllocation, 
										numBackOrder, fltWeight, fltHeight, fltWidth, fltLength, bitFreeShipping,bitAllowBackOrder,bitShowInStock,
										bitShowQOnHand,numWareHouseItemID,bitInStock,InStock,numUOM,vcUOMName,UOMConversionFactor,tblItem.numCreatedBy,numBarCodeId,vcManufacturer,ReviewCount,RatingCount,Shipping,isNull(vcMetaKeywords,'''') as vcMetaKeywords ,isNull(vcMetaDescription,'''') as vcMetaDescription,isNull(vcCategoryName,'''') as vcCategoryName,PromotionOffers'     

	set @strSql=@strSql+ ' from tblItem ORDER BY numOnHand DESC'
	PRINT @strSql
	exec (@strSql)


	declare @tintOrder as tinyint                                                  
	declare @vcFormFieldName as varchar(50)                                                  
	declare @vcListItemType as varchar(1)                                             
	declare @vcAssociatedControlType varchar(10)                                                  
	declare @numListID AS numeric(9)                                                  
	declare @WhereCondition varchar(2000)                       
	Declare @numFormFieldId as numeric  
	DECLARE @vcFieldType CHAR(1)
	DECLARE @GRP_ID INT
                  
	set @tintOrder=0                                                  
	set @WhereCondition =''                 
                   
              
	CREATE TABLE #tempAvailableFields
	(
		numFormFieldId NUMERIC(9)
		,vcFormFieldName NVARCHAR(50)
		,vcFieldType CHAR(1)
		,vcAssociatedControlType NVARCHAR(50)
		,numListID NUMERIC(18)
		,vcListItemType CHAR(1)
		,intRowNum INT
		,vcItemValue VARCHAR(3000)
		,GRP_ID INT
	)
   
	INSERT INTO #tempAvailableFields 
	(
		numFormFieldId
		,vcFormFieldName
		,vcFieldType
		,vcAssociatedControlType
        ,numListID
		,vcListItemType
		,intRowNum
		,GRP_ID
	)                         
    SELECT 
		Fld_id
		,REPLACE(Fld_label,' ','') AS vcFormFieldName
		,CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType
		,fld_type as vcAssociatedControlType
		,ISNULL(C.numListID, 0) numListID
		,CASE WHEN C.numListID > 0 THEN 'L' ELSE '' END vcListItemType
		,ROW_NUMBER() OVER (ORDER BY Fld_id asc) AS intRowNum
		,GRP_ID
    FROM 
		CFW_Fld_Master C 
	LEFT JOIN 
		CFW_Validation V 
	ON 
		V.numFieldID = C.Fld_id
    WHERE 
		C.numDomainID = @numDomainId
        AND GRP_ID  = 5

    SELECT TOP 1 
		@tintOrder=intRowNum
		,@numFormFieldId=numFormFieldId
		,@vcFormFieldName=vcFormFieldName
		,@vcFieldType=vcFieldType
		,@vcAssociatedControlType=vcAssociatedControlType
		,@numListID=numListID
		,@vcListItemType=vcListItemType
		,@GRP_ID=GRP_ID
	FROM 
		#tempAvailableFields 
	ORDER BY 
		intRowNum ASC
   
	while @tintOrder>0                                                  
	begin                   
		IF @GRP_ID=5
		BEGIN
			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
			BEGIN  
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT Fld_Value FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END   
			ELSE IF @vcAssociatedControlType = 'CheckBox'
			BEGIN      
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT case when isnull(Fld_Value,0)=0 then 'No' when isnull(Fld_Value,0)=1 then 'Yes' END FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END                
			ELSE IF @vcAssociatedControlType = 'DateField'           
			BEGIN   
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT dbo.FormatedDateFromDate(Fld_Value,@numDomainId) FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END                
			ELSE IF @vcAssociatedControlType = 'SelectBox'           
			BEGIN 
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT L.vcData FROM CFW_FLD_Values_Item CFW left Join ListDetails L
					on L.numListItemID=CFW.Fld_Value                
					WHERE CFW.Fld_Id=@numFormFieldId AND CFW.RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END   
		END    
 
		SELECT TOP 1 
			@tintOrder=intRowNum
			,@numFormFieldId=numFormFieldId
			,@vcFormFieldName=vcFormFieldName
			,@vcFieldType=vcFieldType
			,@vcAssociatedControlType=vcAssociatedControlType
			,@numListID=numListID
			,@vcListItemType=vcListItemType
			,@GRP_ID=GRP_ID
		FROM 
			#tempAvailableFields 
		WHERE 
			intRowNum > @tintOrder 
		ORDER BY 
			intRowNum ASC
 
	   IF @@rowcount=0 set @tintOrder=0                                                  
	END   


	INSERT INTO #tempAvailableFields 
	(
		numFormFieldId
		,vcFormFieldName
		,vcFieldType
		,vcAssociatedControlType
		,numListID
		,vcListItemType
		,intRowNum
		,vcItemValue
		,GRP_ID
	)                         
	SELECT 
		Fld_id
		,REPLACE(Fld_label,' ','') AS vcFormFieldName
		,CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType
		,fld_type as vcAssociatedControlType
		,ISNULL(C.numListID, 0) numListID
		,CASE WHEN C.numListID > 0 THEN 'L' ELSE '' END vcListItemType
		,ROW_NUMBER() OVER (ORDER BY Fld_id asc) AS intRowNum
		,''
		,GRP_ID
	FROM 
		CFW_Fld_Master C 
	LEFT JOIN 
		CFW_Validation V 
	ON 
		V.numFieldID = C.Fld_id
	WHERE 
		C.numDomainID = @numDomainId
		AND GRP_ID = 9

	IF ISNULL(@bitMatrix,0) = 1 AND ISNULL(@numItemGroup,0) > 0
	BEGIN
		UPDATE
			TEMP
		SET
			TEMP.vcItemValue = FLD_ValueName
		FROM
			#tempAvailableFields TEMP
		INNER JOIN
		(
			SELECT 
				CFW_Fld_Master.fld_id
				,CASE 
					WHEN CFW_Fld_Master.fld_type='SelectBox' THEN dbo.GetListIemName(Fld_Value)
					WHEN CFW_Fld_Master.fld_type='CheckBox' THEN CASE WHEN isnull(Fld_Value,0)=1 THEN 'Yes' ELSE 'No' END
					ELSE CAST(Fld_Value AS VARCHAR)
				END AS FLD_ValueName
			FROM 
				CFW_Fld_Master 
			INNER JOIN
				ItemGroupsDTL 
			ON 
				CFW_Fld_Master.Fld_id = ItemGroupsDTL.numOppAccAttrID
				AND ItemGroupsDTL.tintType = 2
			LEFT JOIN
				ItemAttributes
			ON
				CFW_Fld_Master.Fld_id = ItemAttributes.FLD_ID
				AND ItemAttributes.numItemCode = @numItemCode
			LEFT JOIN
				ListDetails LD
			ON
				CFW_Fld_Master.numlistid = LD.numListID
			WHERE
				CFW_Fld_Master.numDomainID = @numDomainId
				AND ItemGroupsDTL.numItemGroupID = @numItemGroup
			GROUP BY
				CFW_Fld_Master.Fld_label
				,CFW_Fld_Master.fld_id
				,CFW_Fld_Master.fld_type
				,CFW_Fld_Master.bitAutocomplete
				,CFW_Fld_Master.numlistid
				,CFW_Fld_Master.vcURL
				,ItemAttributes.FLD_Value 
		) T1
		ON
			TEMP.numFormFieldId = T1.fld_id
		WHERE
			TEMP.GRP_ID=9
	END
  
	SELECT * FROM #tempAvailableFields

	DROP TABLE #tempAvailableFields
END
GO
GO
/****** Object:  StoredProcedure [dbo].[USP_ItemList1]    Script Date: 05/07/2009 18:14:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop Jayaraj                                        
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_itemlist1' ) 
    DROP PROCEDURE usp_itemlist1
GO
CREATE PROCEDURE [dbo].[USP_ItemList1]
    @ItemClassification AS NUMERIC(9) = 0,
    @IsKit AS TINYINT,
    @SortChar CHAR(1) = '0',
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT, 
    @columnName AS VARCHAR(50),
    @columnSortOrder AS VARCHAR(10),
    @numDomainID AS NUMERIC(9) = 0,
    @ItemType AS CHAR(1),
    @bitSerialized AS BIT,
    @numItemGroup AS NUMERIC(9),
    @bitAssembly AS BIT,
    @numUserCntID AS NUMERIC(9),
    @Where	AS VARCHAR(MAX),
    @IsArchive AS BIT = 0,
    @byteMode AS TINYINT,
	@bitAsset as BIT=0,
	@bitRental as BIT=0,
	@bitContainer AS BIT=0,
	@SearchText VARCHAR(300),
	@vcRegularSearchCriteria varchar(MAX)='',
	@vcCustomSearchCriteria varchar(MAX)='',
	@tintDashboardReminderType TINYINT = 0,
	@bitMultiSelect AS TINYINT
AS 
BEGIN	
	DECLARE @numWarehouseID NUMERIC(18,0)
	SELECT TOP 1 @numWarehouseID = numWareHouseID FROM Warehouses WHERE numDomainID = @numDomainID


	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1),Grp_Id TINYINT
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = 0
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = 0
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 21 AND numRelCntType=0 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			21,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=21 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			21,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=21 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType,0
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=21 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'',Grp_Id
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=21 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				21,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=21 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType,0
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = 0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'',Grp_Id
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = 0
		ORDER BY 
			tintOrder asc  
	END   

	IF @byteMode=0
	BEGIN
		DECLARE @strColumns AS VARCHAR(MAX)
		SET @strColumns = ' I.numItemCode,ISNULL(DivisionMaster.numDivisionID,0) numDivisionID,ISNULL(DivisionMaster.tintCRMType,0) tintCRMType, ' + CAST(ISNULL(@numWarehouseID,0) AS VARCHAR) + ' AS numWarehouseID, I.numDomainID,0 AS numRecOwner,0 AS numTerID, I.charItemType, I.numItemCode AS [numItemCode~211~0]'

		DECLARE @tintOrder AS TINYINT                                                  
		DECLARE @vcFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(3)                                             
		DECLARE @vcListItemType1 AS VARCHAR(1)                                                 
		DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
		DECLARE @numListID AS NUMERIC(9)                                                  
		DECLARE @vcDbColumnName VARCHAR(50)                      
		DECLARE @WhereCondition VARCHAR(2000)                       
		DECLARE @vcLookBackTableName VARCHAR(2000)                
		DECLARE @bitCustom AS BIT                  
		DECLARE @numFieldId AS NUMERIC  
		DECLARE @bitAllowSorting AS CHAR(1)   
		DECLARE @bitAllowEdit AS CHAR(1)                   
        DECLARE @vcColumnName AS VARCHAR(200)
		DECLARE @Grp_ID AS TINYINT
		SET @tintOrder = 0                                                  
		SET @WhereCondition = ''                 
                   
		DECLARE @ListRelID AS NUMERIC(9) 
		DECLARE @SearchQuery AS VARCHAR(MAX) = ''

		IF LEN(@SearchText) > 0
		BEGIN
			SET @SearchQuery = ' I.numItemCode LIKE ''%' + @SearchText + '%'' OR I.vcItemName LIKE ''%' + @SearchText + '%'' OR I.vcModelID LIKE ''%' + @SearchText + '%'' OR I.vcSKU LIKE ''%' + @SearchText + '%'' OR cmp.vcCompanyName LIKE ''%' + @SearchText + '%'' OR I.vcManufacturer LIKE ''%' + @SearchText + '%'''
		END


		DECLARE @j INT = 0
  
		SELECT TOP 1
				@tintOrder = tintOrder + 1,
				@vcDbColumnName = vcDbColumnName,
				@vcFieldName = vcFieldName,
				@vcAssociatedControlType = vcAssociatedControlType,
				@vcListItemType = vcListItemType,
				@numListID = numListID,
				@vcLookBackTableName = vcLookBackTableName,
				@bitCustom = bitCustomField,
				@numFieldId = numFieldId,
				@bitAllowSorting = bitAllowSorting,
				@bitAllowEdit = bitAllowEdit,
				@ListRelID = ListRelID
		FROM    #tempForm --WHERE bitCustomField=1
		ORDER BY tintOrder ASC            

		WHILE @tintOrder > 0                                                  
		BEGIN
			IF @bitCustom = 0
			BEGIN
				DECLARE  @Prefix  AS VARCHAR(5)

				IF @vcLookBackTableName = 'Item'
					SET @Prefix = 'I.'
				ELSE IF @vcLookBackTableName = 'CompanyInfo'
					SET @Prefix = 'cmp.'
				ELSE IF @vcLookBackTableName = 'Vendor'
					SET @Prefix = 'V.'

				SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

				IF @vcAssociatedControlType='SelectBox' 
				BEGIN
					IF @vcListItemType='LI' 
					BEGIN
						SET @strColumns = @strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'
						SET @WhereCondition= @WhereCondition +' LEFT JOIN ListDetails L'+ convert(varchar(3),@tintOrder)+ ' ON L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                      
					END
					ELSE IF @vcListItemType = 'UOM'
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = I.' + @vcDbColumnName + '),'''')' + ' ['+ @vcColumnName+']'
					END
					ELSE IF @vcListItemType = 'IG'
					BEGIN
						SET @strColumns = @strColumns+ ',ISNULL((SELECT vcItemGroup FROM ItemGroups WHERE numItemGroupID = I.numItemGroup),'''')' + ' ['+ @vcColumnName+']'  
					END
				END
				ELSE IF @vcDbColumnName = 'vcPathForTImage'
		   		BEGIN
					SET @strColumns = @strColumns + ',ISNULL(II.vcPathForTImage,'''') AS ' + ' ['+ @vcColumnName+']'                  
					SET @WhereCondition = @WhereCondition + ' LEFT JOIN ItemImages II ON II.numItemCode = I.numItemCode AND bitDefault = 1'
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'monListPrice'
				BEGIN
					SET @strColumns = @strColumns + ',CASE WHEN I.charItemType=''P'' THEN ISNULL((SELECT TOP 1 monWListPrice FROM WarehouseItems WHERE numDomainID=I.numDomainID AND numItemID=I.numItemCode AND monWListPrice > 0),0.00) ELSE ISNULL(I.monListPrice,0.00) END AS ' + ' ['+ @vcColumnName+']'
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'ItemType'
				BEGIN
					SET @strColumns = @strColumns + ',' + '(CASE 
																WHEN I.charItemType=''P'' 
																THEN 
																	CASE 
																		WHEN ISNULL(I.bitAssembly,0) = 1 THEN ''Assembly''
																		WHEN ISNULL(I.bitKitParent,0) = 1 THEN ''Kit''
																		WHEN ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=0 THEN ''Asset''
																		WHEN ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=1 THEN ''Rental Asset''
																		WHEN ISNULL(I.bitSerialized,0) = 1 AND ISNULL(I.bitAsset,0) = 0  THEN ''Serialized''
																		WHEN ISNULL(I.bitSerialized,0) = 1 AND ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=0 THEN ''Serialized Asset''
																		WHEN ISNULL(I.bitSerialized,0) = 1 AND ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=1 THEN ''Serialized Rental Asset''
																		WHEN ISNULL(I.bitLotNo,0)=1 THEN ''Lot #''
																		ELSE ''Inventory Item'' 
																	END
																WHEN I.charItemType=''N'' THEN ''Non Inventory Item'' 
																WHEN I.charItemType=''S'' THEN ''Service'' 
																WHEN I.charItemType=''A'' THEN ''Accessory'' 
															END)' + ' AS ' + ' [' + @vcColumnName + ']'  
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'monStockValue'
				BEGIN
					SET @strColumns = @strColumns+',WarehouseItems.monStockValue'+' ['+ @vcColumnName+']'                                                      
				END
				ELSE IF @vcLookBackTableName = 'Warehouses' AND @vcDbColumnName = 'vcWareHouse'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT STUFF((SELECT CONCAT('', '', vcWareHouse) FROM WareHouseItems WI JOIN Warehouses W ON WI.numWareHouseID=W.numWareHouseID WHERE WI.numItemID=I.numItemCode FOR XML PATH('''')), 1, 1, ''''))' + ' AS ' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcLookBackTableName = 'WorkOrder' AND @vcDbColumnName = 'WorkOrder'
				BEGIN
					SET @strColumns = @strColumns + ',(CASE WHEN (SELECT SUM(WO.numQtyItemsReq) from WorkOrder WO where WO.numItemCode=I.numItemCode and WO.numWOStatus=0) > 0 THEN 1 ELSE 0 END)' + ' AS ' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'vcPriceLevelDetail' 
				BEGIN
					SELECT @strColumns = @strColumns + CONCAT(', dbo.fn_GetItemPriceLevelDetail( ',@numDomainID,',I.numItemCode,',@numWarehouseID,')') +' ['+ @vcColumnName+']' 
				END
				ELSE
				BEGIN
					If @vcDbColumnName <> 'numItemCode' AND @vcDbColumnName <> 'vcPriceLevelDetail' --WE AE ALREADY ADDING COLUMN IN SELECT CLUASE DIRECTLY
					BEGIN
						SET @strColumns = @strColumns + ',' + (CASE @vcLookBackTableName WHEN 'Item' THEN 'I' WHEN 'CompanyInfo' THEN 'cmp' WHEN 'Vendor' THEN 'V' ELSE @vcLookBackTableName END) + '.' + @vcDbColumnName + ' AS ' + ' [' + @vcColumnName + ']' 
					END
				END
			END                              
			ELSE IF @bitCustom = 1 
			BEGIN
				
				SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

				SELECT 
					@vcFieldName = FLd_label,
					@vcAssociatedControlType = fld_type,
					@vcDbColumnName = 'Cust' + CONVERT(VARCHAR(10), Fld_Id),
					@Grp_ID = Grp_id
				FROM 
					CFW_Fld_Master
				WHERE 
					CFW_Fld_Master.Fld_Id = @numFieldId                 
        
				IF @Grp_ID = 9
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'     
             
					SET @WhereCondition = @WhereCondition + ' left Join ItemAttributes CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.numDOmainID=' + CAST(@numDomainID AS VARCHAR) + ' AND CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
									+ CONVERT(VARCHAR(10), @numFieldId)
									+ 'and CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ '.numItemCode=I.numItemCode   '  
					SET @WhereCondition = @WhereCondition
						+ ' left Join ListDetails L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.numListItemID=CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value'      
				END
				ELSE IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea' 
				BEGIN
					SET @strColumns = @strColumns + ',CFW'  + CONVERT(VARCHAR(3), @tintOrder)  + '.Fld_Value  [' + @vcColumnName + ']'     
             
					SET @WhereCondition = @WhereCondition + ' left Join CFW_FLD_Values_Item CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
									+ CONVERT(VARCHAR(10), @numFieldId)
									+ 'and CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ '.RecId=I.numItemCode   '                                                         
				END   
				ELSE IF @vcAssociatedControlType = 'CheckBox' 
				BEGIN
					SET @strColumns = @strColumns
						+ ',case when isnull(CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value,0)=0 then 0 when isnull(CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value,0)=1 then 1 end   ['
						+  @vcColumnName
						+ ']'               
 
					SET @WhereCondition = @WhereCondition
						+ ' left Join CFW_FLD_Values_Item CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
						+ CONVERT(VARCHAR(10), @numFieldId)
						+ ' and CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.RecId=I.numItemCode   '                                                     
				END                
				ELSE IF @vcAssociatedControlType = 'DateField' 
				BEGIN
					SET @strColumns = @strColumns
						+ ',dbo.FormatedDateFromDate(CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value,'
						+ CONVERT(VARCHAR(10), @numDomainId)
						+ ')  [' +@vcColumnName + ']'    
					                  
					SET @WhereCondition = @WhereCondition
						+ ' left Join CFW_FLD_Values_Item CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
						+ CONVERT(VARCHAR(10), @numFieldId)
						+ ' and CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.RecId=I.numItemCode   '                                                         
				END                
				ELSE IF @vcAssociatedControlType = 'SelectBox' 
				BEGIN
					SET @vcDbColumnName = 'Cust' + CONVERT(VARCHAR(10), @numFieldId)

					SET @strColumns = @strColumns + ',L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.vcData' + ' [' + @vcColumnName + ']'          
					                                                    
					SET @WhereCondition = @WhereCondition
						+ ' left Join CFW_FLD_Values_Item CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
						+ CONVERT(VARCHAR(10), @numFieldId)
						+ ' and CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.RecId=I.numItemCode    '     
					                                                    
					SET @WhereCondition = @WhereCondition
						+ ' left Join ListDetails L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.numListItemID=CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value'                
				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueItem(',@numFieldId,',I.numItemCode)') + ' [' + @vcColumnName + ']'
				END                
			END        
  
			

			SELECT TOP 1
					@tintOrder = tintOrder + 1,
					@vcDbColumnName = vcDbColumnName,
					@vcFieldName = vcFieldName,
					@vcAssociatedControlType = vcAssociatedControlType,
					@vcListItemType = vcListItemType,
					@numListID = numListID,
					@vcLookBackTableName = vcLookBackTableName,
					@bitCustom = bitCustomField,
					@numFieldId = numFieldId,
					@bitAllowSorting = bitAllowSorting,
					@bitAllowEdit = bitAllowEdit,
					@ListRelID = ListRelID
			FROM    #tempForm
			WHERE   tintOrder > @tintOrder - 1 --AND bitCustomField=1
			ORDER BY tintOrder ASC            
 
			IF @@rowcount = 0 
				SET @tintOrder = 0 
		END  

		DECLARE @strSQL AS VARCHAR(MAX)
		SET @strSQL = ' FROM 
							Item I
						LEFT JOIN
							Vendor V
						ON
							I.numItemCode = V.numItemCode
							AND I.numVendorID = V.numVendorID
						LEFT JOIN
							DivisionMaster
						ON
							V.numVendorID = DivisionMaster.numDivisionID
						LEFT JOIN
							CompanyInfo cmp
						ON
							DivisionMaster.numCompanyId = cmp.numCompanyId
						LEFT JOIN
							CustomerPartNumber
						ON
							CustomerPartNumber.numItemCode = I.numItemCode AND CustomerPartNumber.numCompanyId = 0
						OUTER APPLY
						(
							SELECT
								SUM(numOnHand) AS numOnHand,
								SUM(numBackOrder) AS numBackOrder,
								SUM(numOnOrder) AS numOnOrder,
								SUM(numAllocation) AS numAllocation,
								SUM(numReorder) AS numReorder,
								SUM(numOnHand) * (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) AS monStockValue
							FROM
								WareHouseItems
							WHERE
								numItemID = I.numItemCode
						) WarehouseItems ' + @WhereCondition

		IF @columnName = 'OnHand' OR @columnName = 'WHI.numOnHand'
			SET @columnName = 'numOnHand'
		ELSE IF @columnName = 'Backorder' OR @columnName = 'WHI.numBackOrder'
			SET @columnName = 'numBackOrder'
		ELSE IF @columnName = 'OnOrder' OR @columnName = 'WHI.numOnOrder' 
			SET @columnName = 'numOnOrder'
		ELSE IF @columnName = 'OnAllocation' OR @columnName = 'WHI.numAllocation' 
			SET @columnName = 'numAllocation'
		ELSE IF @columnName = 'Reorder' OR @columnName = 'WHI.numReorder' 
			SET @columnName = 'numReorder'
		ELSE IF @columnName = 'monStockValue' 
			SET @columnName = 'sum(numOnHand) * (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END)'
		ELSE IF @columnName = 'ItemType' 
			SET @columnName = 'charItemType'
		ELSE IF @columnName = 'numItemCode' 
			SET @columnName = 'I.numItemCode'

		DECLARE @column AS VARCHAR(50)              
		SET @column = @columnName
	         
		IF @columnName LIKE '%Cust%' 
		BEGIN
			DECLARE @CustomFieldType AS VARCHAR(10)
			DECLARE @fldId AS VARCHAR(10)
			
			IF CHARINDEX('CFW.Cust',@columnName) > 0
			BEGIN
				SET @fldId = REPLACE(@columnName, 'CFW.Cust', '')
			END
			ELSE
			BEGIN
				SET @fldId = REPLACE(@columnName, 'Cust', '')
			END
			SELECT TOP 1 @CustomFieldType = ISNULL(vcAssociatedControlType,'') FROM View_DynamicCustomColumns WHERE numFieldID = @fldId
            
			IF ISNULL(@CustomFieldType,'') = 'SelectBox' OR ISNULL(@CustomFieldType,'') = 'ListBox'
			BEGIN
				SET @strSQL = @strSQL + ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=i.numItemCode and CFW.fld_id= ' + @fldId + ' ' 
				SET @strSQL = @strSQL + ' left Join ListDetails LstCF on LstCF.numListItemID = CFW.Fld_Value  '            
				SET @columnName = ' LstCF.vcData ' 	
			END
			ELSE
			BEGIN
				SET @strSQL =  @strSQL + ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=i.numItemCode and CFW.fld_id= ' + @fldId + ' '                                                         
				SET @columnName = 'CFW.Fld_Value'                	
			END
		END                                              
                  
		DECLARE @strWhere AS VARCHAR(8000)
		SET @strWhere = CONCAT(' WHERE I.numDomainID = ',@numDomainID)
    
		IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND I.charItemType= ''P''' 
		END
		ELSE IF @ItemType = 'S'
		BEGIN
			SET @strWhere = @strWhere + ' AND I.charItemType= ''S'''
		END
		ELSE IF @ItemType = 'N'    
		BEGIN
			SET @strWhere = @strWhere + ' AND I.charItemType= ''N'''
		END
	    
		IF @bitContainer= '1'
		BEGIN
			SET @strWhere = @strWhere + ' AND I.bitContainer= ' + CONVERT(VARCHAR(2), @bitContainer) + ''  
		END

		IF @bitAssembly = '1' 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.bitAssembly= ' + CONVERT(VARCHAR(2), @bitAssembly) + ''  
		END 
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAssembly,0)=0 '
		END

		IF @IsKit = '1' 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAssembly,0)=0 AND I.bitKitParent= ' + CONVERT(VARCHAR(2), @IsKit) + ''  
		END           
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitKitParent,0)=0 '
		END

		IF @bitSerialized = 1 
		BEGIN
			SET @strWhere = @strWhere + ' AND (I.bitSerialized=1 OR I.bitLotNo=1)'
		END
		--ELSE IF @ItemType = 'P'
		--BEGIN
		--	SET @strWhere = @strWhere + ' AND ISNULL(Item.bitSerialized,0)=0 AND ISNULL(Item.bitLotNo,0)=0 '
		--END

		IF @bitAsset = 1 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAsset,0) = 1'
		END
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAsset,0) = 0 '
		END

		IF @bitRental = 1
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitRental,0) = 1 AND ISNULL(I.bitAsset,0) = 1 '
		END
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitRental,0) = 0 '
		END

		IF @SortChar <> '0' 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.vcItemName like ''' + @SortChar + '%'''                                       
		END

		IF @ItemClassification <> '0' 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.numItemClassification=' + CONVERT(VARCHAR(15), @ItemClassification)    
		END

		IF @IsArchive = 0 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.IsArchieve,0) = 0'       
		END
        
		IF @numItemGroup > 0 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.numItemGroup = ' + CONVERT(VARCHAR(20), @numItemGroup)  
        END  
		
		IF @numItemGroup = -1 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.numItemGroup in (select numItemGroupID from ItemGroups where numDomainID=' + CONVERT(VARCHAR(20), @numDomainID) + ')'                                      
        END

		IF @bitMultiSelect = '1'
		BEGIN
			SET  @strWhere = @strWhere + 'AND ISNULL(I.bitKitParent,0)=0 '
		END

		IF @vcRegularSearchCriteria<>'' 
		BEGIN
			SET @strWhere = @strWhere +' AND ' + @vcRegularSearchCriteria 
		END
	
		IF @vcCustomSearchCriteria<>'' 
		BEGIN
			SET @strWhere = @strWhere + ' AND ' + @vcCustomSearchCriteria
		END
		
		IF LEN(@SearchQuery) > 0
		BEGIN
			SET @strWhere = @strWhere + ' AND (' + @SearchQuery + ') '
		END

		SET @strWhere = @strWhere + ISNULL(@Where,'')

		IF ISNULL(@tintDashboardReminderType,0) = 17
		BEGIN
			SET @strWhere = CONCAT(@strWhere,' AND I.numItemCode IN (','SELECT DISTINCT
																		numItemID
																	FROM
																		WareHouseItems WIInner
																	INNER JOIN
																		Item IInner
																	ON
																		WIInner.numItemID = IInner.numItemCode
																	WHERE
																		WIInner.numDomainID=',@numDomainID,'
																		AND IInner.numDomainID = ',@numDomainID,'
																		AND ISNULL(IInner.bitArchiveItem,0) = 0
																		AND ISNULL(IInner.IsArchieve,0) = 0
																		AND ISNULL(WIInner.numReorder,0) > 0 
																		AND ISNULL(WIInner.numOnHand,0) < ISNULL(WIInner.numReorder,0)',') ')

		END
  
		DECLARE @firstRec AS INTEGER                                        
		DECLARE @lastRec AS INTEGER                                        
		SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                        
		SET @lastRec = ( @CurrentPage * @PageSize + 1 )

		-- EXECUTE FINAL QUERY
		DECLARE @strFinal AS NVARCHAR(MAX)
		
		IF @CurrentPage = -1 AND @PageSize = -1
		BEGIN
			SET @strFinal = CONCAT('SELECT I.numItemCode, I.vcItemName, I.vcModelID INTO #tempTable ',@strSql + @strWhere,'; SELECT * FROM #tempTable; SELECT @TotalRecords = COUNT(*) FROM #tempTable; DROP TABLE #tempTable;')
		END
		ELSE
		BEGIN
			SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') RowID,I.numItemCode INTO #temp2 ',@strSql, @strWhere,'; SELECT RowID, ',@strColumns,' INTO #tempTable ',@strSql, ' JOIN #temp2 tblAllItems ON I.numItemCode = tblAllItems.numItemCode ',@strWhere,' AND tblAllItems.RowID > ',@firstRec,' and tblAllItems.RowID <',@lastRec,'; SELECT * FROM  #tempTable ORDER BY RowID; DROP TABLE #tempTable; SELECT @TotalRecords = COUNT(*) FROM #temp2; DROP TABLE #temp2;')
		END

		
		PRINT @strFinal
		exec sp_executesql @strFinal, N'@TotalRecords INT OUT', @TotRecs OUT

	END

	--UPDATE  
	--	#tempForm
 --   SET 
	--	vcDbColumnName = (CASE WHEN bitCustomField = 1 THEN vcFieldName + '~' + vcDbColumnName ELSE vcDbColumnName END)
                                  
	SELECT * FROM #tempForm
    DROP TABLE #tempForm
END
--SELECT * FROM Category
--- Alterd By Chintan Prajapati
-- EXEC dbo.USP_ItemsForECommerce 9,60,1,1,15,0,''
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemsForECommerce' ) 
    DROP PROCEDURE USP_ItemsForECommerce
GO
CREATE PROCEDURE [dbo].[USP_ItemsForECommerce]
    @numSiteID AS NUMERIC(9) = 0,
    @numCategoryID AS NUMERIC(18) = 0,
    @numWareHouseID AS NUMERIC(9) = 0,
    @SortBy NVARCHAR(MAX) = 0,
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT = 0 OUTPUT,
    @SearchText AS NVARCHAR(MAX) = '',--Comma seperated item ids
    @FilterQuery AS NVARCHAR(MAX) = '',----Comma seperated Attributes Ids
    @FilterRegularCondition AS NVARCHAR(MAX),
    @FilterCustomFields AS NVARCHAR(MAX),
    @FilterCustomWhere AS NVARCHAR(MAX),
	@numDivisionID AS NUMERIC(18,0) = 0,
	@numManufacturerID AS NUMERIC(18,0)=0,
	@FilterItemAttributes AS NVARCHAR(MAX)
   
AS 
    BEGIN

		
		
		DECLARE @numDomainID AS NUMERIC(9)
		DECLARE @numDefaultRelationship AS NUMERIC(18,0)
		DECLARE @numDefaultProfile AS NUMERIC(18,0)
		DECLARE @tintPreLoginPriceLevel AS TINYINT
		DECLARE @tintPriceLevel AS TINYINT

		SELECT  @numDomainID = numDomainID
        FROM    [Sites]
        WHERE   numSiteID = @numSiteID
        
        DECLARE @tintDisplayCategory AS TINYINT
		
		IF ISNULL(@numDivisionID,0) > 0
		BEGIN
			SELECT @numDefaultRelationship=ISNULL(tintCRMType,0),@numDefaultProfile=ISNULL(vcProfile,0),@tintPriceLevel=ISNULL(tintPriceLevel,0) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=@numDivisionID AND DivisionMaster.numDomainID=@numDomainID
		END

		SELECT 
			@tintDisplayCategory = ISNULL(bitDisplayCategory,0)
			,@numDefaultRelationship=numRelationshipId
			,@numDefaultProfile=numProfileId
			,@tintPreLoginPriceLevel=(CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN (CASE WHEN @numDivisionID > 0 THEN (CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN ISNULL(@tintPriceLevel,0) ELSE 0 END) ELSE ISNULL(tintPreLoginProceLevel,0) END) ELSE 0 END)
		FROM 
			dbo.eCommerceDTL 
		WHERE 
			numSiteID = @numSiteId
		
		CREATE TABLE #tmpItemCat(numCategoryID NUMERIC(18,0))
		CREATE TABLE #tmpItemCode(numItemCode NUMERIC(18,0))
		IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
		BEGIN			
			DECLARE @FilterCustomFieldIds AS NVARCHAR(MAX)
			SET @FilterCustomFieldIds = Replace(@FilterCustomFields,'[','')
			SET @FilterCustomFieldIds = Replace(@FilterCustomFieldIds,']','')
			
			DECLARE @strCustomSql AS NVARCHAR(MAX)
			SET @strCustomSql = 'INSERT INTO #tmpItemCode(numItemCode)
								 SELECT DISTINCT T.RecId FROM 
															(
																SELECT * FROM 
																	(
																		SELECT  t2.RecId, t1.fld_id, t2.Fld_Value FROM CFW_Fld_Master t1 
																		JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID 
																		WHERE t1.Grp_id  In (5,9)
																		AND t1.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
																		AND t1.fld_type <> ''Link'' 
																		AND t1.fld_id IN (' + @FilterCustomFieldIds + ')
																	) p  PIVOT (MAX([Fld_Value]) FOR fld_id IN (' + @FilterCustomFields + ')) AS pvt 
															) T  WHERE 1=1 AND ' + @FilterCustomWhere
								 
			PRINT @strCustomSql
			EXEC SP_EXECUTESQL @strCustomSql								 					 
		END
		
        DECLARE @strSQL NVARCHAR(MAX)
        DECLARE @firstRec AS INTEGER
        DECLARE @lastRec AS INTEGER
        DECLARE @Where NVARCHAR(MAX)
        DECLARE @SortString NVARCHAR(MAX)
        DECLARE @searchPositionColumn NVARCHAR(MAX)
		DECLARE @searchPositionColumnGroupBy NVARCHAR(MAX)
        DECLARE @row AS INTEGER
        DECLARE @data AS VARCHAR(100)
        DECLARE @dynamicFilterQuery NVARCHAR(MAX)
        DECLARE @checkFldType VARCHAR(100)
        DECLARE @count NUMERIC(9,0)
        DECLARE @whereNumItemCode NVARCHAR(MAX)
        DECLARE @filterByNumItemCode VARCHAR(100)
        
		SET @SortString = ''
        set @searchPositionColumn = ''
		SET @searchPositionColumnGroupBy = ''
        SET @strSQL = ''
		SET @whereNumItemCode = ''
		SET @dynamicFilterQuery = ''
		 
        SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
        SET @lastRec = ( @CurrentPage * @PageSize + 1 )
                
        DECLARE @bitAutoSelectWarehouse AS BIT
		SELECT @bitAutoSelectWarehouse = ISNULL([ECD].[bitAutoSelectWarehouse],0) FROM [dbo].[eCommerceDTL] AS ECD WHERE [ECD].[numSiteId] = @numSiteID
		AND [ECD].[numDomainId] = @numDomainID

		DECLARE @vcWarehouseIDs AS VARCHAR(1000)
        IF (@numWareHouseID = 0 OR @bitAutoSelectWarehouse = 1)
            BEGIN				
				IF @bitAutoSelectWarehouse = 1
					BEGIN
						SELECT @vcWarehouseIDs = ISNULL( STUFF((SELECT DISTINCT ',' + CAST(numWarehouseID AS VARCHAR(10)) FROM [dbo].[Warehouses] WHERE [Warehouses].[numDomainID] = @numDomainID FOR XML PATH('')),1, 1, '') , '') 
					END
				
				ELSE
					BEGIN
						SELECT  @numWareHouseID = ISNULL(numDefaultWareHouseID,0)
						FROM    [eCommerceDTL]
						WHERE   [numDomainID] = @numDomainID	

						SET @vcWarehouseIDs = @numWareHouseID
					END
            END
		ELSE
		BEGIN
			SET @vcWarehouseIDs = @numWareHouseID
		END

		PRINT @vcWarehouseIDs

		--PRIAMRY SORTING USING CART DROPDOWN
		DECLARE @Join AS NVARCHAR(MAX)
		DECLARE @SortFields AS NVARCHAR(MAX)
		DECLARE @vcSort AS VARCHAR(MAX)

		SET @Join = ''
		SET @SortFields = ''
		SET @vcSort = ''

		IF @SortBy = ''
			BEGIN
				SET @SortString = ' vcItemName Asc '
			END
		ELSE IF CHARINDEX('~0',@SortBy) > 0 OR CHARINDEX('~1',@SortBy) > 0
			BEGIN
				DECLARE @fldID AS NUMERIC(18,0)
				DECLARE @RowNumber AS VARCHAR(MAX)
				
				SELECT * INTO #fldValues FROM dbo.SplitIDsWithPosition(@SortBy,'~')
				SELECT @RowNumber = RowNumber, @fldID = id FROM #fldValues WHERE RowNumber = 1
				SELECT @vcSort = CASE WHEN Id = 0 THEN 'ASC' ELSE 'DESC' END FROM #fldValues WHERE RowNumber = 2
				
				SET @Join = @Join + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber + ' ON I.numItemCode = CFVI' + @RowNumber + '.RecID AND CFVI' + @RowNumber + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID)
				---SET @SortString = 'CFVI' + @RowNumber + '.fld_Value' 
				SET @SortString = '[fld_Value' + @RowNumber + '] ' --+ @vcSort
				SET @SortFields = ', CFVI' + @RowNumber + '.fld_Value [fld_Value' + @RowNumber + ']'
			END
		ELSE
			BEGIN
				--SET @SortString = @SortBy	
				
				IF CHARINDEX('monListPrice',@SortBy) > 0
				BEGIN
					DECLARE @bitSortPriceMode AS BIT
					SELECT @bitSortPriceMode = ISNULL(bitSortPriceMode,0) FROM dbo.eCommerceDTL WHERE numSiteId = @numSiteID
					PRINT @bitSortPriceMode
					IF @bitSortPriceMode = 1
					BEGIN
						SET @SortString = ' '
					END
					ELSE
					BEGIN
						SET @SortString = @SortBy	
					END
				END
				ELSE
				BEGIN
					SET @SortString = @SortBy	
				END
				
			END	

		--SECONDARY SORTING USING DEFAULT SETTINGS FROM ECOMMERCE
		SELECT ROW_NUMBER() OVER (ORDER BY Custom,vcFieldName) AS [RowID],* INTO #tempSort FROM (
		SELECT  CONVERT(VARCHAR(9),numFieldID)+'~0' AS numFieldID,
				ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
				0 AS Custom,
				vcDbColumnName 
		FROM View_DynamicColumns
		WHERE numFormID = 84 
		AND ISNULL(bitSettingField,0) = 1 
		AND ISNULL(bitDeleted,0)=0 
		AND ISNULL(bitCustom,0) = 0
		AND numDomainID = @numDomainID
					       
		UNION     

		SELECT  CONVERT(VARCHAR(9),numFieldID)+'~1' AS numFieldID ,
				vcFieldName ,
				1 AS Custom,
				'' AS vcDbColumnName
		FROM View_DynamicCustomColumns          
		WHERE numFormID = 84
		AND numDomainID = @numDomainID
		AND grp_id IN (5,9)
		AND vcAssociatedControlType = 'TextBox' 
		AND ISNULL(bitCustom,0) = 1 
		AND tintPageType=1  
		) TABLE1 ORDER BY Custom,vcFieldName
		
		--SELECT * FROM #tempSort
		DECLARE @DefaultSort AS NVARCHAR(MAX)
		DECLARE @DefaultSortFields AS NVARCHAR(MAX)
		DECLARE @DefaultSortJoin AS NVARCHAR(MAX)
		DECLARE @intCnt AS INT
		DECLARE @intTotalCnt AS INT	
		DECLARE @strColumnName AS VARCHAR(100)
		DECLARE @bitCustom AS BIT
		DECLARE @FieldID AS VARCHAR(100)				

		SET @DefaultSort = ''
		SET @DefaultSortFields = ''
		SET @DefaultSortJoin = ''		
		SET @intCnt = 0
		SET @intTotalCnt = ( SELECT COUNT(*) FROM #tempSort )
		SET @strColumnName = ''
		
		IF (SELECT ISNULL(bitEnableSecSorting,0) FROM EcommerceDtl WHERE numSiteID = @numSiteID ) = 1
		BEGIN
			CREATE TABLE #fldDefaultValues(RowNumber INT,Id VARCHAR(1000))

			WHILE(@intCnt < @intTotalCnt)
			BEGIN
				SET @intCnt = @intCnt + 1
				SELECT @FieldID = numFieldID, @strColumnName = vcDbColumnName, @bitCustom = Custom FROM #tempSort WHERE RowID = @intCnt
				
				--PRINT @FieldID
				--PRINT @strColumnName
				--PRINT @bitCustom

				IF @bitCustom = 0
					BEGIN
						SET @DefaultSort =  @strColumnName + ',' + @DefaultSort 
					END

				ELSE
					BEGIN
						DECLARE @fldID1 AS NUMERIC(18,0)
						DECLARE @RowNumber1 AS VARCHAR(MAX)
						DECLARE @vcSort1 AS VARCHAR(10)
						DECLARE @vcSortBy AS VARCHAR(1000)
						SET @vcSortBy = CONVERT(VARCHAR(100),@FieldID) + '~' + '0'
						--PRINT @vcSortBy

						INSERT INTO #fldDefaultValues(RowNumber,Id) SELECT RowNumber, id FROM dbo.SplitIDsWithPosition(@vcSortBy,'~')
						SELECT @RowNumber1 = RowNumber + 100 + @intCnt, @fldID1 = id FROM #fldDefaultValues WHERE RowNumber = 1
						SELECT @vcSort1 = 'ASC' 
						
						SET @DefaultSortJoin = @DefaultSortJoin + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber1 + ' ON I.numItemCode = CFVI' + @RowNumber1 + '.RecID AND CFVI' + @RowNumber1 + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID1)
						SET @DefaultSort = '[fld_Value' + @RowNumber1 + '] ' + ',' + @DefaultSort 
						SET @DefaultSortFields = @DefaultSortFields + ', CFVI' + @RowNumber1 + '.fld_Value [fld_Value' + @RowNumber1 + ']'
					END

			END
			IF LEN(@DefaultSort) > 0
			BEGIN
				SET  @DefaultSort = LEFT(@DefaultSort,LEN(@DefaultSort) - 1)
				SET @SortString = @SortString + ',' 
			END
			--PRINT @SortString
			--PRINT @DefaultSort
			--PRINT @DefaultSortFields
		END
--        IF @SortBy = 1 
--            SET @SortString = ' vcItemName Asc '
--        ELSE IF @SortBy = 2 
--            SET @SortString = ' vcItemName Desc '
--        ELSE IF @SortBy = 3 
--            SET @SortString = ' monListPrice Asc '
--        ELSE IF @SortBy = 4 
--            SET @SortString = ' monListPrice Desc '
--        ELSE IF @SortBy = 5 
--            SET @SortString = ' bintCreatedDate Desc '
--        ELSE IF @SortBy = 6 
--            SET @SortString = ' bintCreatedDate ASC '	
--        ELSE IF @SortBy = 7 --sort by relevance, invokes only when search
--            BEGIN
--                 IF LEN(@SearchText) > 0 
--		            BEGIN
--						SET @SortString = ' SearchOrder ASC ';
--					    set @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 						
--					END
--			END
--        ELSE 
--            SET @SortString = ' vcItemName Asc '
		
                                		
        IF LEN(@SearchText) > 0
			BEGIN
				PRINT 1
				IF CHARINDEX('SearchOrder',@SortString) > 0
				BEGIN
					SET @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 
					SET @searchPositionColumnGroupBy = ' ,SearchItems.RowNumber ' 
				END

				SET @Where = ' INNER JOIN dbo.SplitIDsWithPosition('''+ @SearchText +''','','') AS SearchItems 
                        ON SearchItems.Id = I.numItemCode  and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID) + ' WHERE ISNULL(IsArchieve,0) = 0'
				END

		ELSE IF @SearchText = ''

			SET @Where = ' WHERE 1=2 '
			+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
			+ ' AND ISNULL(IsArchieve,0) = 0'
		ELSE
			BEGIN
				PRINT 2
				SET @Where = ' WHERE 1=1 '
							+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
							+ ' AND ISNULL(IsArchieve,0) = 0'


				PRINT @tintDisplayCategory							
				PRINT @Where

				IF @tintDisplayCategory = 1
				BEGIN
					
					;WITH CTE AS(
					SELECT C.numCategoryID,C.tintLevel,C.numDepCategory,CASE WHEN tintLevel=1 THEN 1 ELSE 0 END AS bitChild 
						FROM SiteCategories SC INNER JOIN Category C ON SC.numCategoryID=C.numCategoryID WHERE SC.numSiteID=@numSiteID AND (C.numCategoryID = @numCategoryID OR ISNULL(@numCategoryID,0) = 0) AND numDomainID = @numDomainID
					UNION ALL
					SELECT  C.numCategoryID,C.tintLevel,C.numDepCategory,CTE.bitChild FROM Category C JOIN CTE ON 
					C.numDepCategory=CTE.numCategoryID AND CTE.bitChild=1 AND numDomainID = @numDomainID)
					
					INSERT INTO #tmpItemCat(numCategoryID)												 
					SELECT DISTINCT C.numCategoryID FROM CTE C JOIN dbo.ItemCategory IC ON C.numCategoryID = IC.numCategoryID 
														WHERE numItemID IN ( 
																						
													 						SELECT numItemID FROM WareHouseItems WI
																			INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																			GROUP BY numItemID 
																			HAVING SUM(WI.numOnHand) > 0 
																			UNION ALL
													 						SELECT numItemID FROM WareHouseItems WI
																			INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																			GROUP BY numItemID 
																			HAVING SUM(WI.numAllocation) > 0
																		 )
				END

			END

		       SET @strSQL = @strSQL + CONCAT(' WITH Items AS 
												( 
													 SELECT
														I.numDomainID
														,I.numItemCode
														,ROW_NUMBER() OVER ( Partition by I.numItemCode Order by I.numItemCode) RowID
														,I.bitMatrix
														,I.bitKitParent
														,I.numItemGroup
														,vcItemName
														,I.bintCreatedDate
														,vcManufacturer
														,ISNULL(txtItemDesc,'''') txtItemDesc
														,vcSKU
														,fltHeight
														,fltWidth
														,fltLength
														,vcModelID
														,fltWeight
														,numBaseUnit
														,numSaleUnit
														,numPurchaseUnit
														,bitFreeShipping
														,C.vcCategoryName
														,C.vcDescription as CategoryDesc
														,charItemType
														,monListPrice
														,bitAllowBackOrder
														,bitShowInStock
														,numVendorID
														,numItemClassification',@searchPositionColumn,@SortFields,@DefaultSortFields,'
														,SUM(numOnHand) numOnHand
														,SUM(numAllocation) numAllocation
													 FROM      
														Item AS I
													 INNER JOIN eCommerceDTL E ON E.numDomainID = I.numDomainID AND E.numSiteId=',@numSiteID,'
													 LEFT JOIN WareHouseItems WI ON WI.numDomainID=' + CONVERT(VARCHAR(10),@numDomainID) + ' AND WI.numItemID=I.numItemCode AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
													 INNER JOIN ItemCategory IC ON I.numItemCode = IC.numItemID
													 INNER JOIN Category C ON IC.numCategoryID = C.numCategoryID
													 INNER JOIN SiteCategories SC ON IC.numCategoryID = SC.numCategoryID 
													  ',@Join,' ',@DefaultSortJoin)

			IF LEN(ISNULL(@FilterItemAttributes,'')) > 0
			BEGIN
				SET @strSQL = @strSQL + @FilterItemAttributes
			END

			IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
			BEGIN
				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCode TP ON TP.numItemCode = I.numItemCode ' 
			END
			
			IF (SELECT COUNT(*) FROM #tmpItemCat) > 0
			BEGIN
				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCat TC ON TC.numCategoryID = IC.numCategoryID '
			END
			ELSE IF @numManufacturerID>0
			BEGIN
				SET @Where = REPLACE(@Where,'WHERE',' WHERE I.numManufacturer = ' + CONVERT(VARCHAR(10),@numManufacturerID) + ' AND ')
			END 
			ELSE IF @numCategoryID>0
			BEGIN
				SET @Where = REPLACE(@Where,'WHERE',' WHERE IC.numCategoryID = ' + CONVERT(VARCHAR(10),@numCategoryID) + ' AND ')
			END 
			
			IF (SELECT COUNT(*) FROM [dbo].[EcommerceRelationshipProfile] WHERE numSiteID=@numSiteID AND numRelationship = @numDefaultRelationship AND numProfile = @numDefaultProfile) > 0
			BEGIN
				SET @Where = @Where + CONCAT(' AND I.numItemClassification NOT IN (SELECT numItemClassification FROM EcommerceRelationshipProfile ERP INNER JOIN ECommerceItemClassification EIC ON EIC.numECommRelatiionshipProfileID=ERP.ID WHERE ERP.numSiteID=',@numSiteID,' AND ERP.numRelationship=',@numDefaultRelationship,' AND ERP.numProfile=', @numDefaultProfile,')')
			END

			--PRINT @Where
			SET @strSQL = @strSQL + @Where
			PRINT @strSQL
			IF LEN(@FilterRegularCondition) > 0
			BEGIN
				SET @strSQL = @strSQL + @FilterRegularCondition
			END                                         

            IF LEN(@whereNumItemCode) > 0
                                        BEGIN
								              SET @strSQL = @strSQL + @filterByNumItemCode	
								    	END
            SET @strSQL = CONCAT(@strSQL,' GROUP BY
												I.numDomainID
												,I.numItemCode
												,I.bitMatrix
												,I.bitKitParent
												,I.numItemGroup
												,vcItemName
												,I.bintCreatedDate
												,vcManufacturer
												,txtItemDesc
												,vcSKU
												,fltHeight
												,fltWidth
												,fltLength
												,vcModelID
												,fltWeight
												,numBaseUnit
												,numSaleUnit
												,numPurchaseUnit
												,bitFreeShipping
												,C.vcCategoryName
												,C.vcDescription
												,charItemType
												,monListPrice
												,bitAllowBackOrder
												,bitShowInStock
												,numVendorID
												,numItemClassification',@searchPositionColumnGroupBy,@SortFields,@DefaultSortFields,(CASE WHEN ISNULL(@tintDisplayCategory,0) = 1 THEN ' HAVING 1 = (CASE WHEN (I.charItemType=''P'' AND ISNULL(I.bitKitParent,0) = 0) THEN CASE WHEN (ISNULL(SUM(numOnHand), 0) > 0 OR ISNULL(SUM(numAllocation), 0) > 0) THEN 1 ELSE 0 END ELSE 1 END) ' ELSE '' END), ') SELECT * INTO #TEMPItems FROM Items;')
                                        
			IF @numDomainID = 204
			BEGIN
				SET @strSQL = @strSQL + 'DELETE FROM 
											#TEMPItems
										WHERE 
											numItemCode IN (
																SELECT 
																	F.numItemCode
																FROM 
																	#TEMPItems AS F
																WHERE 
																	ISNULL(F.bitMatrix,0) = 1 
																	AND EXISTS (
																				SELECT 
																					t1.vcItemName, t1.bitMatrix, t1.numItemGroup, Count(t1.numItemCode)
																				FROM 
																					#TEMPItems t1
																				WHERE 
																					t1.vcItemName = F.vcItemName
																					AND t1.bitMatrix = 1
																					AND t1.numItemGroup = F.numItemGroup
																				GROUP BY 
																					t1.vcItemName, t1.bitMatrix, t1.numItemGroup
																				HAVING 
																					Count(t1.numItemCode) > 1
																			)
															)
											AND numItemCode NOT IN (
																					SELECT 
																						Min(numItemCode)
																					FROM 
																						#TEMPItems AS F
																					WHERE
																						ISNULL(F.bitMatrix,0) = 1 
																						AND Exists (
																									SELECT 
																										t2.vcItemName, t2.bitMatrix, t2.numItemGroup, Count(t2.numItemCode)
																									FROM 
																										#TEMPItems t2
																									WHERE 
																										t2.vcItemName = F.vcItemName
																									   AND t2.bitMatrix = 1
																									   AND t2.numItemGroup = F.numItemGroup
																									GROUP BY 
																										t2.vcItemName, t2.bitMatrix, t2.numItemGroup
																									HAVING 
																										Count(t2.numItemCode) > 1
																								)
																					GROUP BY 
																						vcItemName, bitMatrix, numItemGroup
																				);'

			END
									  
									  
			SET @strSQL = @strSQL + ' WITH ItemSorted AS (SELECT  ROW_NUMBER() OVER ( ORDER BY  ' + @SortString + @DefaultSort + ' ' + @vcSort + ' ) AS Rownumber,* FROM  #TEMPItems WHERE RowID = 1)'



            DECLARE @fldList AS NVARCHAR(MAX)
			DECLARE @fldList1 AS NVARCHAR(MAX)
			SELECT @fldList=COALESCE(@fldList + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_C') FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=5
			SELECT @fldList1=COALESCE(@fldList1 + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_' + CAST(Fld_Id AS VARCHAR)) FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=9
            
            SET @strSQL = @strSQL + 'SELECT * INTO #tmpPagedItems FROM ItemSorted WHERE Rownumber > ' + CONVERT(VARCHAR(10), @firstRec) + '
                                      AND   Rownumber < ' + CONVERT(VARCHAR(10), @lastRec) + '
                                      order by Rownumber '
                                                                  
            IF LEN(ISNULL(@fldList,'')) > 0 OR LEN(ISNULL(@fldList1,'')) > 0
            BEGIN
				
				SET @strSQL = @strSQL +'SELECT  
											Rownumber
											,I.numItemCode
											,vcCategoryName
											,CategoryDesc
											,vcItemName
											,txtItemDesc
											,vcManufacturer
											,vcSKU
											,fltHeight
											,fltWidth
											,fltLength
											,vcModelID
											,fltWeight
											,bitFreeShipping
											,' + 
											   (CASE 
												WHEN @tintPreLoginPriceLevel > 0 
												THEN 'ISNULL(PricingOption.monPrice,ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0))' 
												ELSE 'ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0)' 
												END)
											   +' AS monListPrice,
											   ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0) monMSRP
											   ' + (CASE WHEN @numDomainID = 172 THEN ',ISNULL(CASE 
														WHEN tintRuleType = 1 AND tintDiscountType = 1
														THEN ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - (ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) * ( decDiscount / 100 ) )
														WHEN tintRuleType = 1 AND tintDiscountType = 2
														THEN ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - decDiscount
														WHEN tintRuleType = 2 AND tintDiscountType = 1
														THEN ISNULL(Vendor.monCost,0) + (ISNULL(Vendor.monCost,0) * ( decDiscount / 100 ) )
														WHEN tintRuleType = 2 AND tintDiscountType = 2
														THEN ISNULL(Vendor.monCost,0) + decDiscount
														WHEN tintRuleType = 3
														THEN decDiscount
													END, 0) AS monFirstPriceLevelPrice,ISNULL(CASE 
													WHEN tintRuleType = 1 AND tintDiscountType = 1 
													THEN decDiscount 
													WHEN tintRuleType = 1 AND tintDiscountType = 2
													THEN (decDiscount * 100) / (ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - decDiscount)
													WHEN tintRuleType = 2 AND tintDiscountType = 1
													THEN decDiscount
													WHEN tintRuleType = 2 AND tintDiscountType = 2
													THEN (decDiscount * 100) / (ISNULL(Vendor.monCost,0) + decDiscount)
													WHEN tintRuleType = 3
													THEN 0
												END,'''') AS fltFirstPriceLevelDiscount' ELSE ',0 AS monFirstPriceLevelPrice, 0 AS fltFirstPriceLevelDiscount' END) + '  
											,vcPathForImage
											,vcpathForTImage
											,UOM AS UOMConversionFactor
											,UOMPurchase AS UOMPurchaseConversionFactor
											,(CASE WHEN charItemType <> ''S'' AND charItemType <> ''N''
													   THEN (CASE 
																WHEN bitAllowBackOrder = 1 THEN 1
																WHEN (ISNULL(I.numOnHand,0)<=0) THEN 0
																ELSE 1
															END)
													   ELSE 1
											END) AS bitInStock
											,(CASE WHEN charItemType <> ''S'' AND charItemType <> ''N''
													   THEN ( CASE WHEN bitShowInStock = 1
																   THEN (CASE 
																			WHEN bitAllowBackOrder = 1 AND I.numDomainID <> 172 AND ISNULL(I.numOnHand,0)<=0 AND ISNULL(I.numVendorID,0) > 0 AND dbo.GetVendorPreferredMethodLeadTime(I.numVendorID) > 0 THEN CONCAT(''<font class="vendorLeadTime" style="color:green">Usually ships in '',dbo.GetVendorPreferredMethodLeadTime(I.numVendorID),'' days</font>'')
																			WHEN bitAllowBackOrder = 1 AND ISNULL(I.numOnHand,0)<=0 THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)
																			WHEN (ISNULL(I.numOnHand,0)<=0) THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:red">ON HOLD</font>'' ELSE ''<font style="color:red">Out Of Stock</font>'' END)
																			ELSE (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)  
																		END)
																   ELSE ''''
															  END )
													   ELSE ''''
												  END ) AS InStock
											, ISNULL(W1.numWareHouseItemID,0) [numWareHouseItemID]
											,(SELECT 
													COUNT(numProId) 
												FROM 
													PromotionOffer PO
												WHERE 
													numDomainId='+CAST(@numDomainID AS VARCHAR(20))+' 
													AND ISNULL(bitEnabled,0)=1
													AND ISNULL(bitAppliesToSite,0)=1 
													AND ISNULL(bitRequireCouponCode,0)=0
													AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
													AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
													AND (1 = (CASE 
																WHEN tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID IN (SELECT numItemGroupID FROM ItemGroups WHERE numProfileItem=I.numItemCode AND numDomainID='+CAST(@numDomainID AS VARCHAR(20))+' ) UNION SELECT I.numItemCode)) > 0 THEN 1 ELSE 0 END)
																WHEN tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=I.numItemClassification) > 0 THEN 1 ELSE 0 END)
																ELSE 0
															END)
														OR
														1 = (CASE 
																WHEN tintDiscoutBaseOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 AND numValue IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID IN (SELECT numItemGroupID FROM ItemGroups WHERE numProfileItem=I.numItemCode AND numDomainID='+CAST(@numDomainID AS VARCHAR(20))+' ) UNION SELECT I.numItemCode)) > 0 THEN 1 ELSE 0 END)
																WHEN tintDiscoutBaseOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=2 AND numValue=I.numItemClassification) > 0 THEN 1 ELSE 0 END)
																WHEN tintDiscoutBaseOn = 3 AND tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(SimilarItems.numItemCode) FROM SimilarItems WHERE numParentItemCode IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND SimilarItems.numItemCode=I.numItemCode))>0 THEN 1 ELSE 0 END)
																WHEN tintDiscoutBaseOn = 3 AND tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(SimilarItems.numItemCode) FROM SimilarItems WHERE SimilarItems.numItemCode IN(I.numItemCode) AND numParentItemCode IN (SELECT numItemCode FROM Item WHERE numItemClassification IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2)))>0 THEN 1 ELSE 0 END)
																ELSE 0
															END)
														)
												)  IsOnSale,' + @fldList + (CASE WHEN LEN(ISNULL(@fldList1,'')) > 0 THEN CONCAT(',',@fldList1) ELSE '' END) +'
                                        FROM 
											#tmpPagedItems I
										LEFT JOIN ItemImages IM ON I.numItemCode = IM.numItemCode AND IM.bitdefault=1 AND IM.bitIsImage = 1' +
										(CASE 
											WHEN @tintPreLoginPriceLevel > 0 
											THEN ' OUTER APPLY(SELECT decDiscount AS monPrice FROM PricingTable WHERE tintRuleType=3 AND numItemCode=I.numItemCode ORDER BY numPricingID OFFSET ' + CAST(@tintPreLoginPriceLevel-1 AS VARCHAR) + ' ROWS FETCH NEXT 1 ROWS ONLY) As PricingOption ' 
											ELSE '' 
										END) + (CASE 
											WHEN @numDomainID = 172
											THEN ' OUTER APPLY (SELECT TOP 1 * FROM PricingTable WHERE numItemCode=I.numItemCode) AS PT LEFT JOIN Vendor ON Vendor.numVendorID =I.numVendorID AND Vendor.numItemCode=I.numItemCode'
											ELSE '' 
										END) + ' 
										OUTER APPLY (SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems W 
													  WHERE  W.numItemID = I.numItemCode 
													  AND W.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
													  AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))) AS W1 			  
										 CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numSaleUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOM) AS UOM
										 CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numPurchaseUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOMPurchase) AS UOMPurchase'

				IF LEN(ISNULL(@fldList,'')) > 0
				BEGIN
					SET @strSQL = @strSQL +' left join (
														SELECT  t2.RecId, REPLACE(t1.Fld_label,'' '','''') + ''_C'' as Fld_label, 
															CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
																				WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
																				WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
																				ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
														JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 5 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
														AND t2.RecId IN (SELECT numItemCode FROM #tmpPagedItems )
														) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList + ')) AS pvt on I.numItemCode=pvt.RecId' 
				END

				IF LEN(ISNULL(@fldList1,'')) > 0
				BEGIN
					
					SET @strSQL = @strSQL +' left join (
														SELECT  t2.numItemCode AS RecId, REPLACE(t1.Fld_label,'' '','''') + ''_'' + CAST(t1.Fld_Id AS VARCHAR) as Fld_label, 
															CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
																				WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
																				WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
																				ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
														JOIN ItemAttributes AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 9 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
														AND t2.numItemCode IN (SELECT numItemCode FROM #tmpPagedItems )
														) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList1 + ')) AS pvt1 on I.numItemCode=pvt1.RecId'
				END
				ELSE
				BEGIN
					SET @strSQL = REPLACE(@strSQL, ',##FLDLIST1##', '');
				END

				SET  @strSQL = @strSQL +' order by Rownumber';
			END 
			ELSE
			BEGIN
				PRINT 'flds2'
				SET @strSQL = @strSQL +' SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,monMSRP,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,UOMPurchaseConversionFactor,InStock,CategoryDesc,numWareHouseItemID,IsOnSale
                                         FROM  #tmpPagedItems order by Rownumber;'
			END               
                                        
            --print('@strSQL=')
            --PRINT(@strSQL)        
--            EXEC ( @strSQL) ; 
			                           
            SET @strSQL = @strSQL + ' SELECT @TotRecs = COUNT(*) FROM #TEMPItems WHERE RowID=1 '                            

			 PRINT  LEN(@strSQL)
			 DECLARE @tmpSQL AS NVARCHAR(MAX)
			 SET @tmpSQL = @strSQL
			 --PRINT  @tmpSQL
			 PRINT CAST(@tmpSQL AS NTEXT)
             EXEC sp_executeSQL @strSQL, N'@TotRecs INT OUTPUT', @TotRecs OUTPUT
             
             
        DECLARE @tintOrder AS TINYINT                                                  
		DECLARE @vcFormFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(1)                                             
		DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
		DECLARE @numListID AS NUMERIC(9)                                                  
		DECLARE @WhereCondition VARCHAR(MAX)                       
		DECLARE @numFormFieldId AS NUMERIC  
		--DECLARE @vcFieldType CHAR(1)
		DECLARE @vcFieldType VARCHAR(15)
		                  
		SET @tintOrder=0                                                  
		SET @WhereCondition =''                 
                   
              
		CREATE TABLE #tempAvailableFields(numFormFieldId  NUMERIC(9),vcFormFieldName NVARCHAR(50),
		--vcFieldType CHAR(1),
		vcFieldType VARCHAR(15),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),
		vcListItemType CHAR(1),intRowNum INT,vcItemValue VARCHAR(3000))
		   
		INSERT INTO #tempAvailableFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
				,numListID,vcListItemType,intRowNum)                         
		SELECT  Fld_id,REPLACE(Fld_label,' ','') AS vcFormFieldName,
			   CASE GRP_ID WHEN 4 then 'C' 
					WHEN 1 THEN 'D'
					--WHEN 9 THEN 'A' 
					WHEN 9 THEN CONVERT(VARCHAR(15), Fld_id)
					ELSE 'C' END AS vcFieldType,
				fld_type as vcAssociatedControlType,
				ISNULL(C.numListID, 0) numListID,
				CASE WHEN C.numListID > 0 THEN 'L'
					 ELSE ''
				END vcListItemType,ROW_NUMBER() OVER (ORDER BY Fld_id ASC) AS intRowNum
		FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
		WHERE   C.numDomainID = @numDomainId
				AND GRP_ID IN (5,9)

		  
		SELECT * FROM #tempAvailableFields
		
		/*
		DROP TABLE #tempAvailableFields
		DROP TABLE #tmpItemCode
		DROP TABLE #tmpItemCat
		*/	

		IF OBJECT_ID('tempdb..#tempAvailableFields') IS NOT NULL
		DROP TABLE #tempAvailableFields
		
		IF OBJECT_ID('tempdb..#tmpItemCode') IS NOT NULL
		DROP TABLE #tmpItemCode
		
		IF OBJECT_ID('tempdb..#tmpItemCat') IS NOT NULL
		DROP TABLE #tmpItemCat
		
		IF OBJECT_ID('tempdb..#fldValues') IS NOT NULL
		DROP TABLE #fldValues
		
		IF OBJECT_ID('tempdb..#tempSort') IS NOT NULL
		DROP TABLE #tempSort
		
		IF OBJECT_ID('tempdb..#fldDefaultValues') IS NOT NULL
		DROP TABLE #fldDefaultValues

		IF OBJECT_ID('tempdb..#tmpPagedItems') IS NOT NULL
		DROP TABLE #tmpPagedItems


		IF OBJECT_ID('tempdb..#TEMPItems') IS NOT NULL
		DROP TABLE #TEMPItems
    END


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageAddressDetails')
DROP PROCEDURE USP_ManageAddressDetails
GO
CREATE PROCEDURE USP_ManageAddressDetails
@numAddressID NUMERIC(18,0)=0 output,
@vcAddressName  varchar(50) ,
@vcStreet  varchar(100) ,
@vcCity  varchar(50) ,
@vcPostalCode  varchar(15) ,
@numState  numeric(18, 0) ,
@numCountry  numeric(18, 0) ,
@bitIsPrimary  bit ,
@tintAddressOf  tinyint ,
@tintAddressType  tinyint ,
@numRecordID  numeric(18, 0),
@numDomainID  numeric(18, 0),
@bitResidential BIT,
@vcPhone VARCHAR(20) = '',
@bitFromEcommerce BIT = 0,
@numContact NUMERIC(18,0) = 0,
@bitAltContact BIT = 0,
@vcAltContact VARCHAR(200) = '',
@numUserCntID NUMERIC(18,0) = 0
AS 
BEGIN

	IF @tintAddressOf = 2 AND ISNULL(@numContact,0) = 0 AND ISNULL(@bitAltContact,0) = 0
	BEGIN
		SELECT TOP 1
			@numContact = numContactId
		FROM
			AdditionalContactsInformation
		WHERE
			numDomainID=@numDomainID
			AND numDivisionId = @numRecordID
			AND ISNULL(bitPrimaryContact,0) = 1
	END

IF ISNULL(@bitFromEcommerce,0) = 1
BEGIN
	UPDATE 
		DivisionMaster 
	SET 
		vcComPhone=CASE WHEN ISNULL(@vcPhone,'') <> '' THEN ISNULL(@vcPhone,'')  ELSE  vcComPhone END
	WHERE 
		numDomainID=@numDomainID AND numDivisionID=@numRecordID
END

IF @numAddressID = 0
BEGIN
	IF @bitIsPrimary=1
	BEGIN
		UPDATE dbo.AddressDetails SET bitIsPrimary=0 WHERE numDomainID=@numDomainID AND numRecordID=@numRecordID AND tintAddressOf=@tintAddressOf 
		AND tintAddressType = @tintAddressType
	END
	
	IF NOT EXISTS (SELECT 
						numAddressID
					FROM 
						AddressDetails 
					WHERE 
						numRecordID=@numRecordID 
						AND numDomainID=@numDomainID 
						AND tintAddressOf=@tintAddressOf 
						AND tintAddressType=@tintAddressType
						AND vcStreet=@vcStreet
						AND vcCity=@vcCity
						AND vcPostalCode=@vcPostalCode
						AND numState=@numState
						AND numCountry=@numCountry)
	BEGIN
			INSERT INTO dbo.AddressDetails 
			(
				vcAddressName,
				vcStreet,
				vcCity,
				vcPostalCode,
				numState,
				numCountry,
				bitIsPrimary,
				tintAddressOf,
				tintAddressType,
				numRecordID,
				numDomainID,
				bitResidential,
				numContact,
				bitAltContact,
				vcAltContact
			) 
			VALUES 
			( 
				@vcAddressName,
				@vcStreet,
				@vcCity,
				@vcPostalCode,
				@numState,
				@numCountry,
				@bitIsPrimary,
				@tintAddressOf,
				@tintAddressType,
				@numRecordID,
				@numDomainID,
				@bitResidential,
				@numContact,
				@bitAltContact,
				@vcAltContact
			) 

			SET @numAddressID = SCOPE_IDENTITY()	
	END
	ELSE
	BEGIN
		SELECT TOP 1
			@numAddressID = numAddressID
		FROM 
			AddressDetails 
		WHERE 
			numRecordID=@numRecordID 
			AND numDomainID=@numDomainID 
			AND tintAddressOf=@tintAddressOf 
			AND tintAddressType=@tintAddressType
			AND vcStreet=@vcStreet
			AND vcCity=@vcCity
			AND vcPostalCode=@vcPostalCode
			AND numState=@numState
			AND numCountry=@numCountry

		UPDATE
			AddressDetails
		SET
			bitIsPrimary=@bitIsPrimary
			,bitResidential=@bitResidential
			,vcAddressName=@vcAddressName
			,numContact=@numContact
			,bitAltContact=@bitAltContact
			,vcAltContact=@vcAltContact
		WHERE 
			numRecordID=@numRecordID 
			AND numDomainID=@numDomainID 
			AND tintAddressOf=@tintAddressOf 
			AND tintAddressType=@tintAddressType
			AND vcStreet=@vcStreet
			AND vcCity=@vcCity
			AND vcPostalCode=@vcPostalCode
			AND numState=@numState
			AND numCountry=@numCountry
	END



	
	
END
ELSE IF @numAddressID>0
BEGIN
	
	
	SELECT  @tintAddressOf=tintAddressOf,@tintAddressType=tintAddressType,@numRecordID=numRecordID FROM dbo.AddressDetails WHERE numAddressID=@numAddressID

	IF @bitIsPrimary=1
	BEGIN
		UPDATE dbo.AddressDetails SET bitIsPrimary=0 WHERE numDomainID=@numDomainID 
		AND numRecordID=@numRecordID AND tintAddressOf=@tintAddressOf AND tintAddressType=@tintAddressType
	END


	UPDATE dbo.AddressDetails
	SET 
		vcAddressName=@vcAddressName,
		vcStreet =@vcStreet,
		vcCity =@vcCity,
		vcPostalCode =@vcPostalCode,
		numState =@numState,
		numCountry =@numCountry,
		bitIsPrimary =@bitIsPrimary,
		bitResidential=@bitResidential
		,numContact=@numContact
		,bitAltContact=@bitAltContact
		,vcAltContact=@vcAltContact
	WHERE 
	numDomainID =@numDomainID
	AND numAddressID=@numAddressID

	If @tintAddressOf = 2 --Organization Address
	BEGIN
		IF ISNULL(@numUserCntID,0) > 0
		BEGIN
			IF NOT EXISTS (SELECT ID FROM OrgOppAddressModificationHistory WHERE numRecordID=@numRecordID AND tintAddressOf=1 AND tintAddressType=@tintAddressType AND numAddressID=@numAddressID)
			BEGIN
				INSERT INTO OrgOppAddressModificationHistory
				(
					tintAddressOf
					,tintAddressType
					,numRecordID
					,numAddressID
					,numModifiedBy
					,dtModifiedDate
				)
				VALUES
				(
					1
					,@tintAddressType
					,@numRecordID
					,@numAddressID
					,@numUserCntID
					,GETUTCDATE()
				)
			END
			ELSE
			BEGIN
				UPDATE
					OrgOppAddressModificationHistory
				SET
					numModifiedBy=@numUserCntID
					,dtModifiedDate=GETUTCDATE()
				WHERE
					numRecordID=@numRecordID
					AND numAddressID=@numAddressID
					AND tintAddressOf=1
					AND tintAddressType=@tintAddressType
			END
		END
	END

END


	
END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_ManageInventory' ) 
    DROP PROCEDURE usp_ManageInventory
GO
CREATE PROCEDURE [dbo].[usp_ManageInventory]
    @itemcode AS NUMERIC(9) = 0,
    @numWareHouseItemID AS NUMERIC(9) = 0,
    @monAmount AS DECIMAL(20,5),
    @tintOpptype AS TINYINT,
    @numUnits AS FLOAT,
    @QtyShipped AS FLOAT,
    @QtyReceived AS FLOAT,
    @monPrice AS DECIMAL(20,5),
    @tintFlag AS TINYINT,  --1: SO/PO insert/edit 2:SO/PO Close 3:SO/PO Re-Open 4:Sales Fulfillment Release 5:Sales Fulfillment Revert
    @numOppID as numeric(9)=0,
    @numoppitemtCode AS NUMERIC(9)=0,
    @numUserCntID AS NUMERIC(9)=0,
	@bitWorkOrder AS BIT = 0   
AS 
	IF ISNULL(@numUserCntID,0) = 0
	BEGIN
		RAISERROR('INVALID_USERID',16,1)
		RETURN
	END

    DECLARE @onHand AS FLOAT                                    
    DECLARE @onOrder AS FLOAT 
    DECLARE @onBackOrder AS FLOAT
    DECLARE @onAllocation AS FLOAT
	DECLARE @onReOrder AS FLOAT
    DECLARE @monAvgCost AS DECIMAL(20,5) 
    DECLARE @bitKitParent BIT
	DECLARE @numOrigUnits AS NUMERIC		
	DECLARE @description AS VARCHAR(100)
	DECLARE @bitAsset as BIT
	DECLARE @numDomainID AS NUMERIC 
      
	SELECT @bitAsset=ISNULL(bitAsset,0),@numDomainID=numDomainID from Item where numItemCode=@itemcode--Added By Sachin
    
	SELECT  @monAvgCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost, 0) END) ,
            @bitKitParent = ( CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END )
    FROM    Item
    WHERE   numitemcode = @itemcode                                   
    
    IF @numWareHouseItemID>0
    BEGIN                     
		SELECT  @onHand = ISNULL(numOnHand, 0),
				@onAllocation = ISNULL(numAllocation, 0),
				@onOrder = ISNULL(numOnOrder, 0),
				@onBackOrder = ISNULL(numBackOrder, 0),
				@onReOrder = ISNULL(numReorder,0)
		FROM    WareHouseItems
		WHERE   numWareHouseItemID = @numWareHouseItemID 
    END
   
   DECLARE @dtItemReceivedDate AS DATETIME;SET @dtItemReceivedDate=NULL
   
   DECLARE @TotalOnHand AS NUMERIC;SET @TotalOnHand=0  
   SELECT @TotalOnHand=(SUM(ISNULL(numOnHand,0)) + SUM(ISNULL(numAllocation,0))) FROM dbo.WareHouseItems WHERE numItemID=@itemcode
					
   SET @numOrigUnits=@numUnits
            
    IF @tintFlag = 1  --1: SO/PO insert/edit 
    BEGIN
        IF @tintOpptype = 1 
        BEGIN  
		--When change below code also change to USP_ManageWorkOrderStatus  

			/* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
			SET @description='SO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
				
			IF @bitKitParent = 1 
            BEGIN
                EXEC USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,@numOrigUnits, 0,@numOppID,0,@numUserCntID
            END 
			ELSE IF @bitWorkOrder = 1
			BEGIN
				IF EXISTS (SELECT numWOID FROM WorkOrder WHERE numOppID=@numOppID AND numOppItemID=@numoppitemtCode AND numItemCode=@itemcode AND numWareHouseItemId=@numWareHouseItemID AND numWOStatus <> 23184)
				BEGIN
					SET @description='SO-WO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
					-- MANAGE INVENTOY OF ASSEMBLY ITEMS
					EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@numoppitemtCode,@itemcode,@numWareHouseItemID,@QtyShipped,1
				END
				ELSE
				BEGIN
					SET @description='SO-WO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'

					SET @numUnits = @numUnits - @QtyShipped
                                                        
					IF @onHand >= @numUnits 
					BEGIN                                    
						SET @onHand = @onHand - @numUnits                            
						SET @onAllocation = @onAllocation + @numUnits                                    
					END                                    
					ELSE IF @onHand < @numUnits 
					BEGIN                                    
						SET @onAllocation = @onAllocation + @onHand                                    
						SET @onBackOrder = @onBackOrder + @numUnits
							- @onHand                                    
						SET @onHand = 0                                    
					END    
			                                 
					IF @bitAsset=0--Not Asset
					BEGIN 
						UPDATE  WareHouseItems
						SET     numOnHand = @onHand,
								numAllocation = @onAllocation,
								numBackOrder = @onBackOrder,dtModified = GETDATE() 
						WHERE   numWareHouseItemID = @numWareHouseItemID          
					END
				END
				
			END
            ELSE
			BEGIN       
				SET @numUnits = @numUnits - @QtyShipped
                                                        
                IF @onHand >= @numUnits 
                BEGIN                                    
                    SET @onHand = @onHand - @numUnits                            
                    SET @onAllocation = @onAllocation + @numUnits                                    
                END                                    
                ELSE IF @onHand < @numUnits 
                BEGIN                                    
                    SET @onAllocation = @onAllocation + @onHand                                    
                    SET @onBackOrder = @onBackOrder + @numUnits
                        - @onHand                                    
                    SET @onHand = 0                                    
                END    
			                                 

				IF @bitAsset=0--Not Asset
				BEGIN 
					UPDATE  WareHouseItems
					SET     numOnHand = @onHand,
							numAllocation = @onAllocation,
							numBackOrder = @onBackOrder,dtModified = GETDATE() 
					WHERE   numWareHouseItemID = @numWareHouseItemID          
				END
			END  
        END                       
        ELSE IF @tintOpptype = 2 
        BEGIN  
			SET @description='PO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@QtyReceived AS VARCHAR(10)) + ')'
		            
			/* When QtyReceived has been updated form purchase fulfillment then it need to be deducted */
            SET @numUnits = @numUnits - @QtyReceived   
                                             
            SET @onOrder = @onOrder + @numUnits 
		    
            UPDATE  
				WareHouseItems
            SET     
				numOnHand = @onHand,
                numAllocation = @onAllocation,
                numBackOrder = @onBackOrder,
                numOnOrder = @onOrder,dtModified = GETDATE() 
            WHERE   
				numWareHouseItemID = @numWareHouseItemID   
        END                                                                                                                                         
    END
    ELSE IF @tintFlag = 2 --2:SO/PO Close
    BEGIN
        PRINT '@OppType=' + CONVERT(VARCHAR(5), @tintOpptype)
		    
        IF @tintOpptype = 1 
        BEGIN
			SET @description='SO Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
                    
            IF @bitKitParent = 1 
            BEGIN
                EXEC USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,
                    @numOrigUnits, 2,@numOppID,0,@numUserCntID
            END  
			ELSE IF @bitWorkOrder = 1
			BEGIN
				SET @description='SO-WO Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
				EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@numoppitemtCode,@itemcode,@numWareHouseItemID,@QtyShipped,2
			END
            ELSE
            BEGIN       
				/* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
				SET @numUnits = @numUnits - @QtyShipped
				IF @bitAsset=0--Not Asset
				BEGIN
					SET @onAllocation = @onAllocation - @numUnits 
				END
				ELSE--Asset
				BEGIN
					SET @onAllocation = @onAllocation  
				END
											           
				UPDATE 
					WareHouseItems
				SET 
					numOnHand = @onHand,
					numAllocation = @onAllocation,
					numBackOrder = @onBackOrder,
					dtModified = GETDATE() 
				WHERE 
					numWareHouseItemID = @numWareHouseItemID   
			END
        END                
        ELSE IF @tintOpptype = 2 
        BEGIN
			SET @description='PO Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@QtyReceived AS VARCHAR(10)) + ')'
                        
			/* When QtyReceived has been updated form purchase fulfillment then it need to be deducted */
            SET @numUnits = @numUnits - @QtyReceived
            
			If @numUnits > 0
			BEGIN
				IF EXISTS(SELECT 1 FROM dbo.OpportunityMaster WHERE numOppID=@numOppID AND ISNULL(bitStockTransfer,0)=0)
				BEGIN
					--Updating the Average Cost
					IF @TotalOnHand + @numUnits <= 0
					BEGIN
						SET @monAvgCost = @monAvgCost
					END
					ELSE
					BEGIN
						SET @monAvgCost = (( @TotalOnHand * @monAvgCost) + (@numUnits * @monPrice)) / ( @TotalOnHand + @numUnits )
					END    
		                            
					UPDATE 
						Item
					SET 
						monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monAvgCost END)
					WHERE 
						numItemCode = @itemcode
				END
		    
				IF @onOrder >= @numUnits 
				BEGIN
					SET @onOrder = @onOrder - @numUnits            
                
					IF @onBackOrder >= @numUnits 
					BEGIN            
						SET @onBackOrder = @onBackOrder - @numUnits            
						SET @onAllocation = @onAllocation + @numUnits            
					END            
					ELSE 
					BEGIN            
						SET @onAllocation = @onAllocation + @onBackOrder            
						SET @numUnits = @numUnits - @onBackOrder            
						SET @onBackOrder = 0            
						SET @onHand = @onHand + @numUnits            
					END         
				END            
				ELSE IF @onOrder < @numUnits 
				BEGIN
					PRINT 'in @onOrder < @numUnits '
					SET @onHand = @onHand + @onOrder
					SET @onOrder = @numUnits - @onOrder
				END         

				UPDATE
					WareHouseItems
				SET 
					numOnHand = @onHand,
					numAllocation = @onAllocation,
					numBackOrder = @onBackOrder,
					numOnOrder = @onOrder,dtModified = GETDATE() 
				WHERE 
					numWareHouseItemID = @numWareHouseItemID  
			                
				SELECT @dtItemReceivedDate=dtItemReceivedDate FROM OpportunityMaster WHERE numOppId=@numOppId
			END
		END
    END
	ELSE IF @tintFlag = 3 --3:SO/PO Re-Open
    BEGIN
		PRINT '@OppType=' + CONVERT(VARCHAR(5), @tintOpptype)           
                
		IF @tintOpptype = 1 
        BEGIN
			SET @description='SO Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
                    
            IF @bitKitParent = 1 
            BEGIN
				EXEC USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,@numOrigUnits, 3,@numOppID,0,@numUserCntID
            END 
			ELSE IF @bitWorkOrder = 1
			BEGIN
				SET @description='SO-WO Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
				EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@numoppitemtCode,@itemcode,@numWareHouseItemID,@QtyShipped,3
			END
            ELSE
            BEGIN
				/* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
                SET @numUnits = @numUnits - @QtyShipped
  
				IF @bitAsset=0--Not Asset
				BEGIN
					SET @onAllocation = @onAllocation + @numUnits
				END
				ELSE--Asset
				BEGIN
					SET @onAllocation = @onAllocation 
				END
                                    
                UPDATE  
					WareHouseItems
                SET 
					numOnHand = @onHand,
                    numAllocation = @onAllocation,
                    numBackOrder = @onBackOrder,dtModified = GETDATE() 
                WHERE 
					numWareHouseItemID = @numWareHouseItemID
			END	               
        END                
        ELSE IF @tintOpptype = 2 
        BEGIN
			DECLARE @numDeletedReceievedQty AS NUMERIC(18,0)
			SELECT @numDeletedReceievedQty=ISNULL(numDeletedReceievedQty,0) FROM OpportunityItems WHERE numoppitemtcode=@numoppitemtCode
			UPDATE OpportunityItems SET numDeletedReceievedQty = 0 WHERE numoppitemtcode=@numoppitemtCode

			/* When QtyReceived has been updated form purchase fulfillment then it need to be deducted */
            SET @numUnits = @numUnits - @QtyReceived
            
			--Updating the Average Cost
			IF EXISTS(SELECT 1 FROM dbo.OpportunityMaster WHERE numOppID=@numOppID AND ISNULL(bitStockTransfer,0)=0)
			BEGIN
				IF @TotalOnHand - @numUnits <= 0
				BEGIN
					SET @monAvgCost = 0
				END
				ELSE
				BEGIN
					SET @monAvgCost = ((@TotalOnHand * @monAvgCost) - (@numUnits * @monPrice)) / ( @TotalOnHand - @numUnits )
				END        
				
				UPDATE  
					item
				SET 
					monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monAvgCost END)
				WHERE 
					numItemCode = @itemcode
			END


			DECLARE @TEMPReceievedItems TABLE
			(
				ID INT,
				numOIRLID NUMERIC(18,0),
				numWarehouseItemID NUMERIC(18,0),
				numUnitReceieved FLOAT,
				numDeletedReceievedQty FLOAT
			)

			INSERT INTO
				@TEMPReceievedItems
			SELECT
				ROW_NUMBER() OVER(ORDER BY ID),
				ID,
				numWarehouseItemID,
				numUnitReceieved,
				ISNULL(numDeletedReceievedQty,0)
			FROM
				OpportunityItemsReceievedLocation 
			WHERE
				numDomainID=@numDomainID
				AND numOppID=@numOppID
				AND numOppItemID=@numoppitemtCode

			DECLARE @i AS INT = 1
			DECLARE @COUNT AS INT
			DECLARE @numOIRLID NUMERIC(18,0)
			DECLARE @numTempWarehouseItemID NUMERIC(18,0)
			DECLARE @numTempUnitReceieved FLOAT
			DECLARE @numTempDeletedReceievedQty FLOAT

			SELECT @COUNT=COUNT(*) FROM @TEMPReceievedItems

			WHILE @i <= @COUNT
			BEGIN
				SELECT 
					@numOIRLID = numOIRLID,
					@numTempWarehouseItemID=ISNULL(numWarehouseItemID,0),
					@numTempUnitReceieved=ISNULL(numUnitReceieved,0),
					@numTempDeletedReceievedQty = numDeletedReceievedQty
				FROM 
					@TEMPReceievedItems 
				WHERE 
					ID=@i

				SET @description='PO Re-Open (Qty:' + CAST(@numTempUnitReceieved AS VARCHAR(10)) + ' Received:' +  CAST(@QtyReceived AS VARCHAR(10)) + ')'

				UPDATE
					WareHouseItems
				SET
					numOnHand = ISNULL(numOnHand,0) - (ISNULL(@numTempUnitReceieved,0) - @numTempDeletedReceievedQty),
					dtModified = GETDATE()
				WHERE
					numWareHouseItemID=@numTempWarehouseItemID

				UPDATE OpportunityItemsReceievedLocation SET numDeletedReceievedQty = 0 WHERE ID=@numOIRLID

				EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numTempWarehouseItemID, --  numeric(9, 0)
					@numReferenceID = @numOppID, --  numeric(9, 0)
					@tintRefType = 4, --  tinyint
					@vcDescription = @description, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@dtRecordDate =  @dtItemReceivedDate,
					@numDomainID = @numDomainID

				SET @i = @i + 1
			END

			DELETE FROM OpportunityItemsReceievedLocation WHERE numOppID=@numOppID AND numOppItemID=@numoppitemtCode

			SET @description='PO Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@QtyReceived AS VARCHAR(10)) + ' Deleted:' + CAST(@numDeletedReceievedQty AS VARCHAR) + ')'
             
			UPDATE  
				WareHouseItems
            SET 
				numOnHand = @onHand - (@numUnits-@numDeletedReceievedQty)
                ,numOnOrder = @onOrder + @numUnits
				,dtModified = GETDATE() 
            WHERE 
				numWareHouseItemID = @numWareHouseItemID
				AND numWLocationID <> -1                   

			UPDATE  
				WareHouseItems
            SET 
                numOnOrder = @onOrder + @numUnits
				,dtModified = GETDATE() 
            WHERE 
				numWareHouseItemID = @numWareHouseItemID
				AND numWLocationID = -1
        END
    END
            
            
            
    IF @numWareHouseItemID>0 AND @description <> 'DO NOT ADD WAREHOUSE TRACKING'
	BEGIN
		DECLARE @tintRefType AS TINYINT
					
		IF @tintOpptype = 1 --SO
			SET @tintRefType=3
		ELSE IF @tintOpptype = 2 --PO
			SET @tintRefType=4
					  
		DECLARE @numDomain AS NUMERIC(18,0)
		SET @numDomain = (SELECT TOP 1 [numDomainID] FROM WareHouseItems WHERE [WareHouseItems].[numWareHouseItemID] = @numWareHouseItemID)

		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppID, --  numeric(9, 0)
			@tintRefType = @tintRefType, --  tinyint
			@vcDescription = @description, --  varchar(100)
			@numModifiedBy = @numUserCntID,
			@dtRecordDate =  @dtItemReceivedDate,
			@numDomainID = @numDomain
    END
GO
/****** Object:  StoredProcedure [dbo].[USP_OPPBizDocItems]    Script Date: 07/26/2008 16:20:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                                                                                                                                        
-- exec USP_OPPBizDocItems @numOppId=12388,@numOppBizDocsId=11988,@numDomainID=110
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_oppbizdocitems' ) 
    DROP PROCEDURE usp_oppbizdocitems
GO
CREATE PROCEDURE [dbo].[USP_OPPBizDocItems]
    (
      @numOppId AS NUMERIC(9) = NULL,
      @numOppBizDocsId AS NUMERIC(9) = NULL,
      @numDomainID AS NUMERIC(9) = 0      
    )
AS 
    DECLARE @DivisionID AS NUMERIC(9)      
    DECLARE @tintTaxOperator AS TINYINT   
                                                                                                                                                                                                                                                                                                                                          
    SELECT  @DivisionID = numDivisionID, @tintTaxOperator = [tintTaxOperator]           
    FROM    OpportunityMaster
    WHERE   numOppId = @numOppId                                                                                                                                                                          
--Decimal points to show in bizdoc 
    DECLARE @DecimalPoint AS TINYINT
    SELECT  @DecimalPoint = tintDecimalPoints
    FROM    domain
    WHERE   numDomainID = @numDomainID
                                                                                          
    DECLARE @strSQL AS VARCHAR(MAX)                                                                                          
    DECLARE @strSQL1 AS VARCHAR(8000)                                       
    DECLARE @strSQLCusFields VARCHAR(1000)                      
    DECLARE @strSQLEmpFlds VARCHAR(500)                                                                         
    SET @strSQLCusFields = ''                                                                                          
    SET @strSQLEmpFlds = ''                                                                                          
    DECLARE @tintType AS TINYINT   
    DECLARE @tintOppType AS TINYINT                                                      
    DECLARE @intRowNum AS INT                                                  
    DECLARE @numFldID AS VARCHAR(15)                                          
    DECLARE @vcFldname AS VARCHAR(50)                                            
    DECLARE @numBizDocId AS NUMERIC(9)
    DECLARE @numBizDocTempID AS NUMERIC(9) ;
        SET @numBizDocTempID = 0
                                           
    SELECT  @numBizDocTempID = ISNULL(numBizDocTempID, 0),
            @numBizDocId = numBizDocId
    FROM    OpportunityBizDocs
    WHERE   numOppBizDocsId = @numOppBizDocsId
    SELECT  @tintType = tintOppType,
            @tintOppType = tintOppType
    FROM    OpportunityMaster
    WHERE   numOppID = @numOppId                                                                                          
                                                                                   
    IF @tintType = 1 
        SET @tintType = 7
    ELSE 
        SET @tintType = 8                                                                                      
                                                                                
                                                                                        
                                                                                          
 

    SELECT  *
    INTO    #Temp1
    FROM    ( SELECT   Opp.vcitemname AS vcItemName,Opp.numCost,Opp.vcNotes as txtNotes,
                        ISNULL(OBD.numOppBizDocItemID, 0) OppBizDocItemID,
                        CASE WHEN charitemType = 'P' THEN 'Product'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS charitemType,
                        CASE WHEN charitemType = 'P' THEN 'Inventory'
	                         WHEN charitemType = 'N' THEN 'Non-Inventory'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS vcItemType,
                        CASE WHEN bitShowDeptItemDesc = 0
                             THEN CONVERT(VARCHAR(2500), OBD.vcitemDesc)
                             WHEN bitShowDeptItemDesc = 1
                                  AND bitKitParent = 1
                                  AND bitAssembly = 0
                             THEN dbo.fn_PopulateKitDesc(opp.numOppId,
                                                         opp.numoppitemtCode)
                             WHEN bitShowDeptItemDesc = 1
                                  AND bitKitParent = 1
                                  AND bitAssembly = 1
                             THEN dbo.fn_PopulateAssemblyDesc(opp.numoppitemtCode, OBD.numUnitHour)
                             ELSE CONVERT(VARCHAR(2500), OBD.vcitemDesc)
                        END AS txtItemDesc, 
                        dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),
                                             i.numItemCode, @numDomainID,
                                             ISNULL(opp.numUOMId, 0))
                        * OBD.numUnitHour AS numUnitHour,
                        CONVERT(DECIMAL(18, 4), (dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * OBD.monPrice)) monPrice,
                        CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4), OBD.monTotAmount)) Amount,
                        OBD.monTotAmount/*Fo calculating sum*/,
                        ISNULL(CONVERT(VARCHAR, i.monListPrice), 0) AS monListPrice,
                        i.numItemCode AS ItemCode,
                        opp.numoppitemtCode,
                        L.vcdata AS numItemClassification,
                        CASE WHEN bitTaxable = 0 THEN 'No'
                             ELSE 'Yes'
                        END AS Taxable,
                        numIncomeChartAcntId AS itemIncomeAccount,
                        'NI' AS ItemType,
                        ISNULL(GJH.numJournal_Id, 0) AS numJournalId,
                        ( SELECT TOP 1
                                    ISNULL(GJD.numTransactionId, 0)
                          FROM      General_Journal_Details GJD
                          WHERE     GJH.numJournal_Id = GJD.numJournalId
                                    AND GJD.chBizDocItems = 'NI'
                                    AND GJD.numoppitemtCode = opp.numoppitemtCode
                        ) AS numTransactionId,
                        ISNULL(Opp.vcModelID, '') vcModelID,
                        (CASE WHEN charitemType = 'P' THEN dbo.USP_GetAttributes(OBD.numWarehouseItmsID,bitSerialized) ELSE ISNULL(OBD.vcAttributes,0) END) AS vcAttributes,
                        ISNULL(vcPartNo, '') AS vcPartNo,
                        ( ISNULL(OBD.monTotAmtBefDiscount, OBD.monTotAmount)
                          - OBD.monTotAmount ) AS DiscAmt,
						(CASE WHEN OBD.numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(18, 4),ISNULL(OBD.monTotAmount,0) / OBD.numUnitHour) END) As monUnitSalePrice,
                        OBD.vcNotes,
                        OBD.vcTrackingNo,
                        OBD.vcShippingMethod,
                        OBD.monShipCost,
                        [dbo].[FormatedDateFromDate](OBD.dtDeliveryDate,
                                                     @numDomainID) dtDeliveryDate,
                        Opp.numWarehouseItmsID,
                        ISNULL(u.vcUnitName, '') numUOMId,
                        ISNULL(Opp.vcManufacturer, '') AS vcManufacturer,
                        ISNULL(V.monCost, 0) AS VendorCost,
                        Opp.vcPathForTImage,
                        i.[fltLength],
                        i.[fltWidth],
                        i.[fltHeight],
                        i.[fltWeight],
                        ISNULL(I.numVendorID, 0) numVendorID,
                        Opp.bitDropShip AS DropShip,
                        SUBSTRING(( SELECT  ' ,' + vcSerialNo
                                            + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                                   THEN ' ('
                                                        + CONVERT(VARCHAR(15), oppI.numQty)
                                                        + ')'
                                                   ELSE ''
                                              END
                                    FROM    OppWarehouseSerializedItem oppI
                                            JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                    WHERE   oppI.numOppID = Opp.numOppId
                                            AND oppI.numOppItemID = Opp.numoppitemtCode
                                    ORDER BY vcSerialNo
                                  FOR
                                    XML PATH('')
                                  ), 3, 200000) AS SerialLotNo,
                        ISNULL(opp.numUOMId, 0) AS numUOM,
                        ISNULL(u.vcUnitName, '') vcUOMName,
                        dbo.fn_UOMConversion(opp.numUOMId, Opp.numItemCode,
                                             @numDomainID, NULL) AS UOMConversionFactor,
                        ISNULL(Opp.numSOVendorId, 0) AS numSOVendorId,
                        [dbo].FormatedDateTimeFromDate(OBD.dtRentalStartDate,
                                                       @numDomainID) dtRentalStartDate,
                        [dbo].FormatedDateTimeFromDate(OBD.dtRentalReturnDate,
                                                       @numDomainID) dtRentalReturnDate,
                        ISNULL(W.vcWareHouse, '') AS Warehouse,
						(CASE WHEN ISNULL(WI.numWareHouseItemID,0) > 0 THEN CONCAT(W.vcWareHouse,CASE WHEN WL.numWLocationID > 0 THEN CONCAT('(',WL.vcLocation ,')') ELSE '' END) ELSE '' END) vcWarehouse,
					--	ISNULL(W.vcWareHouse, '') AS vcWareHouse,
                        CASE WHEN charitemType = 'P' THEN WI.vcWHSKU
                             ELSE i.vcSKU
                        END vcSKU,
                        CASE WHEN charitemType = 'P' THEN WI.vcBarCode
                             ELSE i.numBarCodeId
                        END vcBarcode,
                        ISNULL(opp.numClassID, 0) AS numClassID,
                        ISNULL(opp.numProjectID, 0) AS numProjectID,
                        ISNULL(I.[numShipClass],0) AS numShipClass,OBD.numOppBizDocID,WL.vcLocation,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
                        opp.numOppId,OBD.numUnitHour AS numUnitHourOrig,ISNULL(I.numItemCode,0) AS [numItemCode], 
						ISNULL(opp.vcPromotionDetail, '') AS vcPromotionDetail
              FROM      OpportunityItems opp
                        JOIN OpportunityBizDocItems OBD ON OBD.numOppItemID = opp.numoppitemtCode
                        LEFT JOIN item i ON opp.numItemCode = i.numItemCode
                        LEFT JOIN ListDetails L ON i.numItemClassification = L.numListItemID
                        LEFT JOIN Vendor V ON V.numVendorID = @DivisionID
                                              AND V.numItemCode = opp.numItemCode
                        LEFT JOIN General_Journal_Header GJH ON opp.numoppid = GJH.numOppId
                                                                AND GJH.numBizDocsPaymentDetId IS NULL
                                                                AND GJH.numOppBizDocsId = @numOppBizDocsId
                        LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId                                                                       
                        LEFT JOIN dbo.WareHouseItems WI ON opp.numWarehouseItmsID = WI.numWareHouseItemID
                                                           AND WI.numDomainID = @numDomainID
                        LEFT JOIN dbo.Warehouses W ON WI.numDomainID = W.numDomainID
                                                      AND WI.numWareHouseID = W.numWareHouseID
                        LEFT JOIN dbo.WarehouseLocation WL ON WL.numWLocationID = WI.numWLocationID
              WHERE     opp.numOppId = @numOppId
                        AND ( bitShowDeptItem IS NULL
                              OR bitShowDeptItem = 0
                            )
                        AND OBD.numOppBizDocID = @numOppBizDocsId
            ) X


    --IF @DecimalPoint = 1 
    --    BEGIN
    --        UPDATE  #Temp1
    --        SET     monPrice = CONVERT(DECIMAL(18, 1), monPrice),
    --                Amount = CONVERT(DECIMAL(18, 1), Amount)
    --    END
    --IF @DecimalPoint = 2 
    --    BEGIN
    --        UPDATE  #Temp1
    --        SET     monPrice = CONVERT(DECIMAL(18, 2), monPrice),
    --                Amount = CONVERT(DECIMAL(18, 2), Amount)
    --    END
    --IF @DecimalPoint = 3 
    --    BEGIN
    --        UPDATE  #Temp1
    --        SET     monPrice = CONVERT(DECIMAL(18, 3), monPrice),
    --                Amount = CONVERT(DECIMAL(18, 3), Amount)
    --    END
    --IF @DecimalPoint = 4 
    --    BEGIN
    --        UPDATE  #Temp1
    --        SET     monPrice = CONVERT(DECIMAL(18, 4), monPrice),
    --                Amount = CONVERT(DECIMAL(18, 4), Amount)
    --    END

    DECLARE @strSQLUpdate AS VARCHAR(2000);SET @strSQLUpdate=''
   
    EXEC ('ALTER TABLE #Temp1 ADD [TotalTax] DECIMAL(20,5)')
	EXEC ('ALTER TABLE #Temp1 ADD [CRV] DECIMAL(20,5)')

    IF @tintTaxOperator = 1 --add Sales tax
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'

		SET @strSQLUpdate = @strSQLUpdate
            + ',[CRV]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',1,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'
	END
    ELSE IF @tintTaxOperator = 2 -- remove sales tax
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
		SET @strSQLUpdate = @strSQLUpdate + ',[CRV]= 0'
	END
    ELSE 
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'

		SET @strSQLUpdate = @strSQLUpdate
            + ',[CRV]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',1,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'
	END


	DECLARE @vcTaxName AS VARCHAR(100)
    DECLARE @numTaxItemID AS NUMERIC(9)
    SET @numTaxItemID = 0
    SELECT TOP 1
            @vcTaxName = vcTaxName,
            @numTaxItemID = numTaxItemID
    FROM    TaxItems
    WHERE   numDomainID = @numDomainID


    WHILE @numTaxItemID > 0
    BEGIN
        EXEC ('ALTER TABLE #Temp1 ADD [' + @vcTaxName + '] DECIMAL(20,5)')

        SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName
            + ']= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ','
            + CONVERT(VARCHAR(20), @numTaxItemID) + ','
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'
       

        SELECT TOP 1
                @vcTaxName = vcTaxName,
                @numTaxItemID = numTaxItemID
        FROM    TaxItems
        WHERE   numDomainID = @numDomainID
                AND numTaxItemID > @numTaxItemID

        IF @@rowcount = 0 
            SET @numTaxItemID = 0
    END

    IF @strSQLUpdate <> '' 
    BEGIN
        SET @strSQLUpdate = 'UPDATE #Temp1 SET ItemCode=ItemCode' + @strSQLUpdate + ' WHERE ItemCode > 0'
        EXEC (@strSQLUpdate)
    END

    DECLARE @vcDbColumnName NVARCHAR(50)
    SELECT TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow                                                                                          
    WHILE @intRowNum > 0                                                                                  
        BEGIN    
            EXEC
                ( 'alter table #Temp1 add [' + @vcDbColumnName
                  + '] varchar(100)'
                )
	
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode,['
                + @vcDbColumnName + ']=dbo.GetCustFldValueItem(' + @numFldID
                + ',ItemCode) where ItemCode>0'
            EXEC ( @strSQLUpdate
                )
		                   
            SELECT TOP 1
                    @numFldID = numfieldID,
                    @vcFldname = vcFieldName,
                    @vcDbColumnName = vcDbColumnName,
                    @intRowNum = ( tintRow + 1 )
            FROM    View_DynamicCustomColumns
            WHERE   numFormID = @tintType
                    AND Grp_id = 5
                    AND numDomainID = @numDomainID
                    AND numAuthGroupID = @numBizDocId
                    AND bitCustom = 1
                    AND tintRow >= @intRowNum
                    AND ISNULL(numRelCntType, 0) = @numBizDocTempID
            ORDER BY tintRow                                                                  
		           
            IF @@rowcount = 0 
                SET @intRowNum = 0                                                                                          
        END


    SELECT  Row_NUmber() OVER ( ORDER BY numoppitemtCode ) RowNumber,
            *
    FROM    #Temp1
    ORDER BY numoppitemtCode ASC 

    DROP TABLE #Temp1

    SELECT  ISNULL(tintOppStatus, 0)
    FROM    OpportunityMaster
    WHERE   numOppID = @numOppId                                                                                                 


IF (SELECT COUNT(*) FROM View_DynamicColumns WHERE   numFormID = @tintType AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId AND vcFieldType = 'R'AND ISNULL(numRelCntType, 0) = @numBizDocTempID)=0
BEGIN            
	INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType)
	select numFormId,numFieldId,1,tintRow,@numDomainID,@numBizDocId,@numBizDocTempID
	 FROM View_DynamicDefaultColumns
	 where numFormId=@tintType and bitDefault=1 and numDomainID=@numDomainID order by tintRow ASC	
	 
	 
	 IF(SELECT COUNT(*) FROM DycFrmConfigBizDocsSumm Ds where numBizDocID=@numBizDocId and Ds.numFormID=@tintType 
	and isnull(Ds.numBizDocTempID,0)=@numBizDocTempID and Ds.numDomainID=@numDomainID)=0
	BEGIN
		INSERT INTO DycFrmConfigBizDocsSumm (numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID)
		 SELECT @tintType,@numBizDocId,DFM.numFieldID,@numDomainID,@numBizDocTempID FROM
		 DycFormField_Mapping DFFM 
			JOIN DycFieldMaster DFM on DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
			where numFormID=16 and DFFM.numDomainID is null
	END

END      


    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicColumns
    WHERE   numFormID = @tintType
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND vcFieldType = 'R'
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    UNION
    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            'C' vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_OPPBizDocItems_GetWithKitChilds' ) 
    DROP PROCEDURE USP_OPPBizDocItems_GetWithKitChilds
GO
CREATE PROCEDURE [dbo].[USP_OPPBizDocItems_GetWithKitChilds]
(
    @numOppId AS NUMERIC(9) = NULL,
    @numOppBizDocsId AS NUMERIC(9) = NULL,
    @numDomainID AS NUMERIC(9) = 0      
)
AS 
BEGIN
    DECLARE @DivisionID AS NUMERIC(9)      
    DECLARE @tintTaxOperator AS TINYINT   
                                                                                                                                                                                                                                                                                                                                          
    SELECT  
		@DivisionID = numDivisionID, 
		@tintTaxOperator = [tintTaxOperator]           
    FROM    
		OpportunityMaster
    WHERE   
		numOppId = @numOppId                                                                                                                                                                          

    
                                                                                          
    DECLARE @strSQL AS VARCHAR(MAX)                                                                                          
    DECLARE @strSQL1 AS VARCHAR(8000)                                       
    DECLARE @strSQLCusFields VARCHAR(1000) = ''
    DECLARE @strSQLEmpFlds VARCHAR(500) = ''                                                                         
                                                                                              
    DECLARE @tintType AS TINYINT   
    DECLARE @tintOppType AS TINYINT                                                      
    DECLARE @intRowNum AS INT                                                  
    DECLARE @numFldID AS VARCHAR(15)                                          
    DECLARE @vcFldname AS VARCHAR(50)                                            
    DECLARE @numBizDocId AS NUMERIC(9)
    DECLARE @numBizDocTempID AS NUMERIC(9) = 0
	DECLARE @bitDisplayKitChild AS BIT = 0
        
                                           
    SELECT  
		@numBizDocTempID = ISNULL(numBizDocTempID, 0),
        @numBizDocId = numBizDocId
    FROM    
		OpportunityBizDocs
    WHERE   
		numOppBizDocsId = @numOppBizDocsId

	
	SELECT
		@bitDisplayKitChild = ISNULL(bitDisplayKitChild,0)
	FROM	
		BizDocTemplate
	WHERE
		numDomainID = @numDomainID
		AND numBizDocTempID = @numBizDocTempID


    SELECT  
		@tintType = tintOppType,
        @tintOppType = tintOppType
    FROM    
		OpportunityMaster
    WHERE   
		numOppID = @numOppId                                                                                          
                                                                                   
    IF @tintType = 1 
        SET @tintType = 7
    ELSE 
        SET @tintType = 8     
		
		
	IF (SELECT 
			COUNT(*) 
		FROM 
			View_DynamicColumns 
		WHERE   
			numFormID = @tintType 
			AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId 
			AND vcFieldType = 'R'
			AND ISNULL(numRelCntType, 0) = @numBizDocTempID
		)=0
	BEGIN            
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType
		)
		SELECT 
			numFormId,numFieldId,1,tintRow,@numDomainID,@numBizDocId,@numBizDocTempID
		FROM 
			View_DynamicDefaultColumns
		WHERE 
			numFormId=@tintType and bitDefault=1 and numDomainID=@numDomainID order by tintRow ASC	
	 
	 
		IF (SELECT 
				COUNT(*) 
			FROM 
				DycFrmConfigBizDocsSumm Ds 
			WHERE 
				numBizDocID=@numBizDocId 
				AND Ds.numFormID=@tintType 
				AND ISNULL(Ds.numBizDocTempID,0)=@numBizDocTempID 
				AND Ds.numDomainID=@numDomainID
			)=0
		BEGIN
			INSERT INTO DycFrmConfigBizDocsSumm 
			(
				numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID
			)
			SELECT 
				@tintType,@numBizDocId,DFM.numFieldID,@numDomainID,@numBizDocTempID 
			FROM
				DycFormField_Mapping DFFM 
			JOIN 
				DycFieldMaster DFM 
			ON 
				DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
			WHERE 
				numFormID=16 
				AND DFFM.numDomainID IS NULL
		END
	END      

	IF OBJECT_ID('tempdb..#TempBizDocProductGridColumns') IS NOT NULL DROP TABLE #TempBizDocProductGridColumns
	SELECT * INTO #TempBizDocProductGridColumns FROM (
    SELECT  
		vcFieldName AS vcFormFieldName,
        vcDbColumnName,
        vcFieldDataType,
        tintRow AS intRowNum
    FROM    
		View_DynamicColumns
    WHERE   
		numFormID = @tintType
        AND numDomainID = @numDomainID
        AND numAuthGroupID = @numBizDocId
        AND vcFieldType = 'R'
        AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    UNION
    SELECT  
		vcFieldName AS vcFormFieldName,
        vcDbColumnName,
        'C' vcFieldDataType,
        tintRow AS intRowNum
    FROM    
		View_DynamicCustomColumns
    WHERE   
		numFormID = @tintType
        AND Grp_id = 5
        AND numDomainID = @numDomainID
        AND numAuthGroupID = @numBizDocId
        AND bitCustom = 1
        AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ) t1   
	ORDER BY 
		intRowNum                                                                              
                                                                                
    SELECT  
		*
    INTO    
		#Temp1
    FROM    
	( 
		SELECT   
			Opp.vcitemname AS vcItemName,
            ISNULL(OBD.numOppBizDocItemID, 0) OppBizDocItemID,
			(CASE 
				WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 
				THEN 
					(CASE 
						WHEN (I.charItemType = 'p' AND ISNULL(Opp.bitDropShip, 0) = 0 AND ISNULL(I.bitAsset,0)=0)
						THEN 
							CAST(dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,Opp.numWarehouseItmsID,(CASE WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0  WHEN ISNULL(bitKitParent,0) = 1 THEN 1 ELSE 0 END)) AS VARCHAR)
						ELSE ''
					END) 
				ELSE ''
			END) vcBackordered,
			ISNULL(WI.numBackOrder,0) AS numBackOrder,
            CASE WHEN charitemType = 'P' THEN 'Product'
                    WHEN charitemType = 'S' THEN 'Service'
            END AS charitemType,
            CASE WHEN charitemType = 'P' THEN 'Inventory'
	                WHEN charitemType = 'N' THEN 'Non-Inventory'
                    WHEN charitemType = 'S' THEN 'Service'
            END AS vcItemType,
			CONVERT(VARCHAR(2500), OBD.vcitemDesc) AS txtItemDesc, 
			opp.numUnitHour AS numOrgTotalUnitHour,
			dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(opp.numUOMId, 0)) * opp.numUnitHour AS numTotalUnitHour,                                   
            dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(opp.numUOMId, 0)) * OBD.numUnitHour AS numUnitHour,
            CONVERT(DECIMAL(20,5), (dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * OBD.monPrice)) monPrice,
            CONVERT(VARCHAR, CONVERT(DECIMAL(20,5), OBD.monTotAmount)) Amount,
            OBD.monTotAmount/*Fo calculating sum*/,
            ISNULL(CONVERT(VARCHAR, i.monListPrice), 0) AS monListPrice,
            i.numItemCode AS ItemCode,
            opp.numoppitemtCode,
            L.vcdata AS numItemClassification,
            CASE WHEN bitTaxable = 0 THEN 'No'
                    ELSE 'Yes'
            END AS Taxable,
            numIncomeChartAcntId AS itemIncomeAccount,
            'NI' AS ItemType,
            ISNULL(GJH.numJournal_Id, 0) AS numJournalId,
            ( SELECT TOP 1
                        ISNULL(GJD.numTransactionId, 0)
                FROM      General_Journal_Details GJD
                WHERE     GJH.numJournal_Id = GJD.numJournalId
                        AND GJD.chBizDocItems = 'NI'
                        AND GJD.numoppitemtCode = opp.numoppitemtCode
            ) AS numTransactionId,
            ISNULL(Opp.vcModelID, '') vcModelID,
            (CASE WHEN charitemType = 'P' THEN dbo.USP_GetAttributes(OBD.numWarehouseItmsID, bitSerialized) ELSE ISNULL(OBD.vcAttributes,0) END) AS vcAttributes,
            ISNULL(vcPartNo, '') AS vcPartNo,
            (ISNULL(OBD.monTotAmtBefDiscount, OBD.monTotAmount) - OBD.monTotAmount ) AS DiscAmt,
			(CASE WHEN OBD.numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(20,5),(dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * (ISNULL(OBD.monTotAmount,0) / OBD.numUnitHour))) END) As monUnitSalePrice,
            ISNULL(OBD.vcNotes,ISNULL(opp.vcNotes,'')) vcNotes,
            OBD.vcTrackingNo,
            OBD.vcShippingMethod,
            OBD.monShipCost,
            [dbo].[FormatedDateFromDate](OBD.dtDeliveryDate,@numDomainID) dtDeliveryDate,
            Opp.numWarehouseItmsID,
            ISNULL(u.vcUnitName, '') numUOMId,
            ISNULL(Opp.vcManufacturer, '') AS vcManufacturer,
            ISNULL(V.monCost, 0) AS VendorCost,
            Opp.vcPathForTImage,
            i.[fltLength],
            i.[fltWidth],
            i.[fltHeight],
            i.[fltWeight],
            ISNULL(I.numVendorID, 0) numVendorID,
            Opp.bitDropShip AS DropShip,
            SUBSTRING(( SELECT  ' ,' + vcSerialNo
                                + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                        THEN ' ('
                                            + CONVERT(VARCHAR(15), oppI.numQty)
                                            + ')'
                                        ELSE ''
                                    END
                        FROM    OppWarehouseSerializedItem oppI
                                JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                        WHERE   oppI.numOppID = Opp.numOppId
                                AND oppI.numOppItemID = Opp.numoppitemtCode
                        ORDER BY vcSerialNo
                        FOR
                        XML PATH('')
                        ), 3, 200000) AS SerialLotNo,
            ISNULL(opp.numUOMId, 0) AS numUOM,
            ISNULL(u.vcUnitName, '') vcUOMName,
            dbo.fn_UOMConversion(opp.numUOMId, Opp.numItemCode,
                                    @numDomainID, NULL) AS UOMConversionFactor,
            ISNULL(Opp.numSOVendorId, 0) AS numSOVendorId,
            [dbo].FormatedDateTimeFromDate(OBD.dtRentalStartDate,
                                            @numDomainID) dtRentalStartDate,
            [dbo].FormatedDateTimeFromDate(OBD.dtRentalReturnDate,
                                            @numDomainID) dtRentalReturnDate,
            ISNULL(W.vcWareHouse, '') AS Warehouse,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcWHSKU,I.vcSKU) ELSE I.vcSKU END vcSKU,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END vcBarcode,
			CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END numBarCodeId,
            ISNULL(opp.numClassID, 0) AS numClassID,
            ISNULL(opp.numProjectID, 0) AS numProjectID,
            ISNULL(I.[numShipClass],0) AS numShipClass,OBD.numOppBizDocID,WL.vcLocation,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
            opp.numOppId,OBD.numUnitHour AS numUnitHourOrig,ISNULL(I.numItemCode,0) AS [numItemCode],
			ISNULL(I.bitKitParent,0) AS bitKitParent,
			0 AS numOppChildItemID, 
			0 AS numOppKitChildItemID,
			0 AS bitChildItem,
			0 AS tintLevel,
			ISNULL(opp.vcPromotionDetail,'') AS vcPromotionDetail,
			(CASE WHEN ISNULL(WI.numWareHouseItemID,0) > 0 THEN W.vcWareHouse ELSE '' END) vcWarehouse,
			(CASE WHEN (SELECT COUNT(*) FROM #TempBizDocProductGridColumns WHERE vcDbColumnName='vcInclusionDetails') > 0 THEN dbo.GetOrderAssemblyKitInclusionDetails(opp.numOppID,Opp.numoppitemtCode,OBD.numUnitHour) ELSE '' END) AS vcInclusionDetails,
			Opp.numSortOrder
			,CAST(ISNULL(Opp.numCost,0) AS DECIMAL(20,5)) AS numCost, numOnHand
			,ISNULL((SELECT TOP 1 dbo.FormatedDateFromDate(dtReleaseDate,@numDomainID)
					FROM
						OpportunityItemsReleaseDates
					WHERE
						numOppID=opp.numOppId
						AND numOppItemID=opp.numoppitemtCode),'') dtReleaseDate
			,(CASE WHEN I.bitKitParent = 1 THEN ISNULL(dbo.fn_GetKitAvailabilityByWarehouse(I.numItemCode,W.numWareHouseID),0) ELSE (ISNULL(numOnHand,0) + ISNULL(numAllocation,0)) END) AS numAvailable
			,CPN.CustomerPartNo 
        FROM      
			OpportunityItems opp
        JOIN 
			OpportunityBizDocItems OBD 
		ON 
			OBD.numOppItemID = opp.numoppitemtCode
		JOIN
			OpportunityBizDocs OB
		ON
			OBD.numOppBizDocID=OB.numOppBizDocsID
        LEFT JOIN 
			item i 
		ON 
			opp.numItemCode = i.numItemCode
        LEFT JOIN 
			ListDetails L 
		ON 
			i.numItemClassification = L.numListItemID
        LEFT JOIN 
			Vendor V 
		ON 
			V.numVendorID = @DivisionID
            AND V.numItemCode = opp.numItemCode
        LEFT JOIN 
			General_Journal_Header GJH 
		ON 
			opp.numoppid = GJH.numOppId
            AND GJH.numBizDocsPaymentDetId IS NULL
            AND GJH.numOppBizDocsId = @numOppBizDocsId
        LEFT JOIN 
			UOM u 
		ON 
			u.numUOMId = opp.numUOMId                                      
        LEFT JOIN 
			dbo.WareHouseItems WI 
		ON 
			opp.numWarehouseItmsID = WI.numWareHouseItemID
			AND WI.numDomainID = @numDomainID
        LEFT JOIN 
			dbo.Warehouses W 
		ON 
			WI.numDomainID = W.numDomainID
            AND WI.numWareHouseID = W.numWareHouseID
        LEFT JOIN 
			dbo.WarehouseLocation WL 
		ON 
			WL.numWLocationID = WI.numWLocationID
		LEFT JOIN
			dbo.OpportunityMaster OM
		ON 
			OM.numOppId = opp.numOppId
		LEFT JOIN	
			DivisionMaster DM
		ON
			OM.numDivisionId= DM.numDivisionID
		LEFT JOIN 
			CustomerPartNumber CPN
		ON 
			opp.numItemCode = CPN.numItemCode AND CPN.numDomainId = @numDomainID AND CPN.numCompanyId = DM.numCompanyID
        WHERE     
			opp.numOppId = @numOppId
            AND (bitShowDeptItem IS NULL OR bitShowDeptItem = 0)
            AND OBD.numOppBizDocID = @numOppBizDocsId
    ) X
	ORDER BY
		numSortOrder

	--GET KIT CHILD ITEMS (NOW WE ARE SUPPORTING KIT WITHIN KIT BUT ONLY 1 LEVEL MEANS CHILD KIT INSIDE PARENT KIT CAN NOT HAVE ANOTHER KIT AS CHILD)
	IF (@bitDisplayKitChild=1 AND (SELECT COUNT(*) FROM OpportunityKitItems WHERE numOppId = @numOppId ) > 0)
	BEGIN
		DECLARE @TEMPTABLE TABLE(
			numOppItemCode NUMERIC(18,0),
			numOppChildItemID NUMERIC(18,0), 
			numOppKitChildItemID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numWarehouseItemID NUMERIC(18,0),
			numTotalQty FLOAT,
			numQty FLOAT,
			numUOMID NUMERIC(18,0),
			numQtyShipped FLOAT,
			tintLevel TINYINT,
			numSortOrder INT
		);

		--FIRST LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,numOppChildItemID,numOppKitChildItemID,numItemCode,numWarehouseItemID,numTotalQty,numQty,numUOMID,numQtyShipped,tintLevel,numSortOrder
		)
		SELECT 
			OKI.numOppItemID,
			OKI.numOppChildItemID,
			CAST(0 AS NUMERIC(18,0)),
			OKI.numChildItemID,
			ISNULL(OKI.numWareHouseItemId,0),
			CAST((t1.numOrgTotalUnitHour * OKI.numQtyItemsReq_Orig) AS FLOAT),
			CAST((t1.numUnitHour * OKI.numQtyItemsReq_Orig) AS FLOAT),
			ISNULL(OKI.numUOMID,0),
			ISNULL(numQtyShipped,0)
			,1
			,t1.numSortOrder
		FROM 
			#Temp1 t1
		INNER JOIN
			OpportunityKitItems OKI
		ON
			t1.numoppitemtCode = OKI.numOppItemID
		WHERE
			t1.bitKitParent = 1

		--SECOND LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,numOppChildItemID,numOppKitChildItemID,numItemCode,numWarehouseItemID,numTotalQty,numQty,numUOMID,numQtyShipped,tintLevel,numSortOrder
		)
		SELECT 
			OKCI.numOppItemID,
			OKCI.numOppChildItemID,
			OKCI.numOppKitChildItemID,
			OKCI.numItemID,
			ISNULL(OKCI.numWareHouseItemId,0),
			CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			CAST((c.numQty * OKCI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			ISNULL(OKCI.numUOMID,0),
			ISNULL(OKCI.numQtyShipped,0),
			2
			,c.numSortOrder
		FROM 
			OpportunityKitChildItems OKCI
		INNER JOIN
			@TEMPTABLE c
		ON
			OKCI.numOppID = @numOppId
			AND OKCI.numOppItemID = c.numOppItemCode
			AND OKCI.numOppChildItemID = c.numOppChildItemID

		INSERT INTO 
			#Temp1
		SELECT   
			(CASE WHEN t2.tintLevel = 1 THEN CONCAT('&nbsp;&nbsp;',I.vcItemName) WHEN t2.tintLevel = 2 THEN CONCAT('&nbsp;&nbsp;&nbsp;&nbsp;',I.vcItemName) ELSE I.vcItemName END) AS vcItemName,
            0 OppBizDocItemID
			,(CASE WHEN (t2.numTotalQty-ISNULL(t2.numQtyShipped,0)) > ISNULL(WI.numAllocation,0) THEN ((t2.numTotalQty-ISNULL(t2.numQtyShipped,0)) - ISNULL(WI.numAllocation,0)) * dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(t2.numUOMID, 0))  ELSE 0 END) AS vcBackordered
			,ISNULL(WI.numBackOrder,0) AS numBackOrder
            ,(CASE WHEN I.charitemType = 'P' THEN 'Product' WHEN charitemType = 'S' THEN 'Service' END) AS charitemType,
            (CASE WHEN charitemType = 'P' THEN 'Inventory'
	                WHEN charitemType = 'N' THEN 'Non-Inventory'
                    WHEN charitemType = 'S' THEN 'Service'
            END) AS vcItemType,
			CONVERT(VARCHAR(2500), I.txtItemDesc) AS txtItemDesc,   
			t2.numTotalQty AS numOrigTotalUnitHour,
			t2.numTotalQty AS numTotalUnitHour,
            t2.numQty AS numUnitHour,
            (ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) monPrice,
            CONVERT(VARCHAR, CONVERT(DECIMAL(20,5),((t2.numQty * ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1)) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)))) Amount,
            ((t2.numQty * ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1)) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) AS monTotAmount/*Fo calculating sum*/,
            ISNULL(CONVERT(VARCHAR, (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)), 0) AS monListPrice,
            I.numItemCode AS ItemCode,
            t2.numOppItemCode,
            L.vcdata AS numItemClassification,
            CASE WHEN bitTaxable = 0 THEN 'No' ELSE 'Yes' END AS Taxable,
            numIncomeChartAcntId AS itemIncomeAccount,
            'NI' AS ItemType,
            0 AS numJournalId,
            0 AS numTransactionId,
            ISNULL(I.vcModelID, '') vcModelID,
            dbo.USP_GetAttributes(t2.numWarehouseItemID, bitSerialized) AS vcAttributes,
            ISNULL(vcPartNo, '') AS vcPartNo,
            0 AS DiscAmt,
			(ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) As monUnitSalePrice,
            '' AS vcNotes,'' AS vcTrackingNo,'' AS vcShippingMethod,0 AS monShipCost,NULL AS dtDeliveryDate,t2.numWarehouseItemID,
            ISNULL(u.vcUnitName, '') numUOMId,ISNULL(I.vcManufacturer, '') AS vcManufacturer,ISNULL(V.monCost, 0) AS VendorCost,
            ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault=1),'') AS vcPathForTImage,
            I.[fltLength],I.[fltWidth],I.[fltHeight],I.[fltWeight],ISNULL(I.numVendorID, 0) numVendorID,0 AS DropShip,
            '' AS SerialLotNo,
            ISNULL(t2.numUOMId, 0) AS numUOM,
            ISNULL(u.vcUnitName, '') vcUOMName,
            dbo.fn_UOMConversion(t2.numUOMId, t2.numItemCode,@numDomainID, NULL) AS UOMConversionFactor,
            0 AS numSOVendorId,
            NULL AS dtRentalStartDate,
            NULL dtRentalReturnDate,
            ISNULL(W.vcWareHouse, '') AS Warehouse,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcWHSKU,I.vcSKU) ELSE I.vcSKU END vcSKU,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END vcBarcode,
			CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END numBarCodeId,
            ISNULL(I.numItemClass, 0) AS numClassID,
            0 AS numProjectID,
            ISNULL(I.[numShipClass],0) AS numShipClass,
			@numOppBizDocsId AS numOppBizDocID,WL.vcLocation,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
            @numOppId AS numOppId,t2.numQty AS numUnitHourOrig,
			ISNULL(I.numItemCode,0) AS [numItemCode],
			ISNULL(I.bitKitParent,0) AS bitKitParent,
			ISNULL(t2.numOppChildItemID,0) AS numOppChildItemID, 
			ISNULL(t2.numOppKitChildItemID,0) AS numOppKitChildItemID,
			1 AS bitChildItem,
			t2.tintLevel AS tintLevel,
			'' AS vcPromotionDetail,
			(CASE WHEN ISNULL(WI.numWareHouseItemID,0) > 0 THEN W.vcWareHouse ELSE '' END) vcWarehouse,
			'' vcInclusionDetails
			,0
			,0
			,numOnHand
			, ''
			,(CASE WHEN I.bitKitParent = 1 THEN ISNULL(dbo.fn_GetKitAvailabilityByWarehouse(I.numItemCode,W.numWareHouseID),0) ELSE (ISNULL(numOnHand,0) + ISNULL(numAllocation,0)) END) AS numAvailable
			,''
		FROM
			@TEMPTABLE t2
		INNER JOIN
			Item I
		ON
			t2.numItemCode = I.numItemCode
		LEFT JOIN
			WareHouseItems WI
		ON
			WI.numWareHouseItemID = t2.numWarehouseItemID
		LEFT JOIN 
			dbo.Warehouses W 
		ON 
			WI.numDomainID = W.numDomainID
            AND WI.numWareHouseID = W.numWareHouseID
        LEFT JOIN 
			dbo.WarehouseLocation WL 
		ON 
			WL.numWLocationID = WI.numWLocationID
		LEFT JOIN 
			ListDetails L 
		ON 
			I.numItemClassification = L.numListItemID
		LEFT JOIN 
			Vendor V 
		ON 
			V.numVendorID = @DivisionID
            AND V.numItemCode = t2.numItemCode
		LEFT JOIN 
			UOM u 
		ON 
			u.numUOMId = t2.numUOMId 
	END


    DECLARE @strSQLUpdate AS VARCHAR(2000);SET @strSQLUpdate=''
   
    EXEC ('ALTER TABLE #Temp1 ADD [TotalTax] DECIMAL(20,5)')

    IF @tintTaxOperator = 1 --add Sales tax
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
	END        
    ELSE IF @tintTaxOperator = 2 -- remove sales tax
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
	END
    ELSE 
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
    END

	-- CRV TAX TYPE
	EXEC ('ALTER TABLE #Temp1 ADD [CRV] DECIMAL(20,5)')

	IF @tintTaxOperator = 2
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate
				+ ',[CRV]= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ',1,'
				+ CONVERT(VARCHAR(20), @numOppId) +','+
				+ 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
	END
	ELSE
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate + ',[CRV]= 0'
	END

	DECLARE @vcTaxName AS VARCHAR(100)
    DECLARE @numTaxItemID AS NUMERIC(9)
    SET @numTaxItemID = 0
    
	SELECT TOP 1
        @vcTaxName = vcTaxName,
        @numTaxItemID = numTaxItemID
    FROM    
		TaxItems
    WHERE   
		numDomainID = @numDomainID

    WHILE @numTaxItemID > 0
    BEGIN
        EXEC ('ALTER TABLE #Temp1 ADD [' + @vcTaxName + '] DECIMAL(20,5)')

		IF @tintTaxOperator = 2 -- remove sales tax
		BEGIN
			SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName + ']=0' 
		END
		ELSE
		BEGIN
			SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName
				+ ']= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ','
				+ CONVERT(VARCHAR(20), @numTaxItemID) + ','
				+ CONVERT(VARCHAR(20), @numOppId) +','+
				+ 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
		END

        SELECT TOP 1
			@vcTaxName = vcTaxName,
            @numTaxItemID = numTaxItemID
        FROM    
			TaxItems
        WHERE   
			numDomainID = @numDomainID
            AND numTaxItemID > @numTaxItemID

        IF @@rowcount = 0 
            SET @numTaxItemID = 0
    END

    IF @strSQLUpdate <> '' 
    BEGIN
        SET @strSQLUpdate = 'UPDATE #Temp1 SET ItemCode=ItemCode' + @strSQLUpdate + ' WHERE ItemCode > 0 AND bitChildItem=0'
        EXEC (@strSQLUpdate)
    END

    DECLARE @vcDbColumnName NVARCHAR(50)

    SELECT TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow     
	                                                                                     
    WHILE @intRowNum > 0                                                                                  
    BEGIN    
        EXEC('ALTER TABLE #Temp1 ADD [' + @vcDbColumnName + '] varchar(100)')
	
        SET @strSQLUpdate = 'UPDATE #Temp1 set ItemCode=ItemCode,[' + @vcDbColumnName + ']= dbo.GetCustFldValueOppItems(' + @numFldID  + ',numoppitemtCode,ItemCode) WHERE ItemCode > 0 AND bitChildItem=0'
            
		EXEC (@strSQLUpdate)
		                   
        SELECT 
			TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
        FROM    
			View_DynamicCustomColumns
        WHERE   
			numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND tintRow >= @intRowNum
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
        ORDER BY 
			tintRow                                                                  
		           
        IF @@rowcount = 0 
            SET @intRowNum = 0                                                                                          
    END


    SELECT  
		ROW_NUMBER() OVER ( ORDER BY numSortOrder) RowNumber,
        *
    FROM    
		#Temp1
    ORDER BY 
		numSortOrder ASC, tintLevel ASC, numoppitemtCode ASC, numOppChildItemID ASC, numOppKitChildItemID ASC

    DROP TABLE #Temp1

    SELECT  
		ISNULL(tintOppStatus, 0)
    FROM    
		OpportunityMaster
    WHERE   
		numOppID = @numOppId                                                                                                 


	SELECT * FROM #TempBizDocProductGridColumns

	IF OBJECT_ID('tempdb..#TempBizDocProductGridColumns') IS NOT NULL DROP TABLE #TempBizDocProductGridColumns
END
GO
/****** Object:  StoredProcedure [dbo].[USP_OppBizDocs]    Script Date: 03/25/2009 15:23:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppbizdocs')
DROP PROCEDURE usp_oppbizdocs
GO
CREATE PROCEDURE [dbo].[USP_OppBizDocs]
(                        
 @byteMode as tinyint=null,                        
 @numOppId as numeric(9)=null,                        
 @numOppBizDocsId as numeric(9)=null,
 @ClientTimeZoneOffset AS INT=0,
 @numDomainID AS NUMERIC(18,0	) = NULL,
 @numUserCntID AS NUMERIC(18,0) = NULL
)                        
as      

Declare @ParentBizDoc AS NUMERIC(9)=0
DECLARE @lngJournalId AS NUMERIC(18,0)

SET @ParentBizDoc=(SELECT TOP 1 numOppBizDocsId FROM OpportunityBizDocs WHERE numSourceBizDocId= @numOppBizDocsId)     
IF @ParentBizDoc>0
BEGIN	
	IF @byteMode = 1
	BEGIN
		SET @lngJournalId=(Select TOP 1 GJH.numJournal_Id as numJournalId from General_Journal_Header GJH Where GJH.numOppId=@numOppId And GJH.numOppBizDocsId=@ParentBizDoc)
		IF(@lngJournalId>0)
		BEGIN
			exec USP_DeleteJournalDetails @numDomainID=@numDomainID,@numJournalID=@lngJournalId,@numBillPaymentID=0,@numDepositID=0,@numCheckHeaderID=0,@numBillID=0,@numCategoryHDRID=0,@numReturnID=0,@numUserCntID=0
		END
		exec DeleteComissionDetails @numDomainId=@numDomainID,@numOppBizDocID=@ParentBizDoc       
	END
	exec USP_OppBizDocs @byteMode=@byteMode,@numOppId=@numOppId,@numOppBizDocsId=@ParentBizDoc,@ClientTimeZoneOffset=@ClientTimeZoneOffset,@numDomainID=@numDomainID,@numUserCntID=@numUserCntID
END      
SELECT 
	ISNULL(CompanyInfo.numCompanyType,0) AS numCompanyType,
	ISNULL(CompanyInfo.vcProfile,0) AS vcProfile,
	ISNULL(OpportunityMaster.numAccountClass,0) AS numAccountClass
FROM 
	OpportunityMaster 
JOIN 
	DivisionMaster 
ON 
	DivisionMaster.numDivisionId = OpportunityMaster.numDivisionId
JOIN 
	CompanyInfo
ON 
	CompanyInfo.numCompanyId = DivisionMaster.numCompanyID
WHERE 
	numOppId = @numOppId
		                                  
if @byteMode= 1                        
begin 
BEGIN TRY
BEGIN TRANSACTION
DECLARE @numBizDocID AS INT
DECLARE @bitFulfilled AS BIT
Declare @tintOppType as tinyint
DECLARE @tintShipped AS BIT
DECLARE @bitAuthoritativeBizDoc AS BIT

SELECT @tintOppType=tintOppType,@tintShipped=ISNULL(tintshipped,0) FROM OpportunityMaster WHERE  numOppId=@numOppId  and numDomainID= @numDomainID 
SELECT @numBizDocID = numBizDocId, @bitFulfilled=bitFulfilled, @bitAuthoritativeBizDoc=ISNULL(bitAuthoritativeBizDocs,0) FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocsId

IF ISNULL(@tintShipped,0) = 1 AND (@numBizDocID=296 OR ISNULL(@bitAuthoritativeBizDoc,0)=1)
BEGIN
	RAISERROR('NOT_ALLOWED_TO_DELETE_AUTHORITATIVE_BIZDOC_AFTER_ORDER_IS_CLOSED',16,1)
	RETURN
END

--IF it's as sales order and BizDoc is of type of fulfillment and already fulfilled revert items to allocations
IF @tintOppType = 1 AND @numBizDocID = 296 AND @bitFulfilled = 1
BEGIN
	EXEC USP_OpportunityBizDocs_RevertFulFillment @numDomainID,@numUserCntID,@numOppId,@numOppBizDocsId
END


DELETE FROM [ProjectsOpportunities] WHERE numOppBizDocID=@numOppBizDocsId

DELETE FROM [ShippingReportItems] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId)
DELETE FROM [ShippingBox] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId)
DELETE FROM [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId

 --Delete All entries for current bizdoc
EXEC [USP_DeleteEmbeddedCost] @numOppBizDocsId, 0, @numDomainID 
--DELETE FROM [OpportunityBizDocsPaymentDetails] WHERE [numBizDocsPaymentDetId] IN (SELECT  [numBizDocsPaymentDetId] FROM [OpportunityBizDocsDetails] WHERE [numBizDocsPaymentDetId] IN (SELECT [numBizDocsPaymentDetId] FROM [EmbeddedCost] WHERE [numOppBizDocID]=@numOppBizDocsId))
--DELETE FROM [EmbeddedCostItems] WHERE [numEmbeddedCostID] IN (SELECT [numEmbeddedCostID] FROM [EmbeddedCost] WHERE [numOppBizDocID] = @numOppBizDocsId)
--DELETE FROM [EmbeddedCost] WHERE [numOppBizDocID] = @numOppBizDocsId
--DELETE FROM [OpportunityBizDocsDetails] WHERE [numBizDocsPaymentDetId] IN (SELECT [numBizDocsPaymentDetId] FROM [EmbeddedCost] WHERE [numOppBizDocID]=@numOppBizDocsId)


delete  from OpportunityBizDocItems where   numOppBizDocID=  @numOppBizDocsId              

--Delete Deferred BizDoc Recurring Entries
delete from OpportunityRecurring where numOppBizDocID=@numOppBizDocsId and tintRecurringType=4

UPDATE dbo.OpportunityRecurring SET numOppBizDocID=NULL WHERE numOppBizDocID=@numOppBizDocsId

DELETE FROM [RecurringTransactionReport] WHERE [numRecTranBizDocID] = @numOppBizDocsId

--Credit Balance
Declare @monCreditAmount as DECIMAL(20,5);Set @monCreditAmount=0
Declare @numDivisionID as numeric(9);Set @numDivisionID=0

Select @monCreditAmount=isnull(oppBD.monCreditAmount,0),@numDivisionID=Om.numDivisionID,
@numOppId=OM.numOppId 
from OpportunityBizDocs oppBD join OpportunityMaster Om on OM.numOppId=oppBD.numOppId WHERE  numOppBizDocsId = @numOppBizDocsId

delete from [CreditBalanceHistory] where numOppBizDocsId=@numOppBizDocsId

 
IF @tintOppType=1 --Sales
	update DivisionMaster set monSCreditBalance=isnull(monSCreditBalance,0) + isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
else IF @tintOppType=2 --Purchase
	update DivisionMaster set monPCreditBalance=isnull(monPCreditBalance,0) + isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID


delete from DocumentWorkflow where numDocID=@numOppBizDocsId and cDocType='B'      

delete from BizDocAction where numBizActionId in(select numBizActionId from BizActionDetails where numOppBizDocsId =@numOppBizDocsId and btDocType=1) 
delete from BizActionDetails where numOppBizDocsId =@numOppBizDocsId and btDocType=1


  DELETE FROM dbo.General_Journal_Details WHERE numJournalId IN (SELECT numJournal_Id FROM dbo.General_Journal_Header WHERE numOppId=@numOppId AND numOppBizDocsId=@numOppBizDocsId AND numDomainId=@numDomainID)
  DELETE FROM dbo.General_Journal_Header WHERE numOppId=@numOppId AND numOppBizDocsId=@numOppBizDocsId AND numDomainId= @numDomainID


delete from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsId                        

COMMIT TRANSACTION
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH             
end                        
                        
if @byteMode= 2                        
begin  

DECLARE @BaseCurrencySymbol nVARCHAR(3);SET @BaseCurrencySymbol='$'
SELECT @BaseCurrencySymbol = ISNULL(varCurrSymbol,'$') FROM dbo.Currency 
														   WHERE numCurrencyID IN ( SELECT numCurrencyID FROM dbo.Domain 
																					WHERE numDomainId=@numDomainId)
--PRINT @BaseCurrencySymbol

																					                      
select *, isnull(X.monPAmount,0)-X.monAmountPaid as BalDue from (select  opp.numOppBizDocsId,                        
 opp.numOppId,                        
 opp.numBizDocId,                        
 ISNULL((SELECT TOP 1 numOrientation FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS numOrientation, 
 ISNULL((SELECT TOP 1 vcTemplateName FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS vcTemplateName, 
 ISNULL((SELECT TOP 1 bitKeepFooterBottom FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS bitKeepFooterBottom,                  
 oppmst.vcPOppName,                        
 ISNULL(ListDetailsName.vcName,mst.vcData) as BizDoc,                        
 opp.numCreatedBy,                        
DATEADD(minute,-@ClientTimeZoneOffset,opp.dtCreatedDate) CreatedDate,
 opp.vcRefOrderNo,                       
 vcBizDocID,
 CASE WHEN LEN(ISNULL(vcBizDocName,''))=0 THEN vcBizDocID ELSE vcBizDocName END vcBizDocName,      
  [dbo].[GetDealAmount](@numOppId ,getutcdate(),opp.numOppBizDocsId ) as monPAmount,
  CASE 
	WHEN tintOppType=1  
	THEN (CASE 
			WHEN (ISNULL(numAuthoritativeSales,0)=numBizDocId OR numBizDocId=296) 
			THEN 'Authoritative' + CASE WHEN ISNULL(Opp.tintDeferred,0)=1 THEN ' (Deferred)' ELSE '' END 
			WHEN opp.numBizDocId =  304 THEN 'Deferred-Authoritative'
			ELSE 'Non-Authoritative' END ) 
  when tintOppType=2  then (case when isnull(numAuthoritativePurchase,0)=numBizDocId then 'Authoritative' + Case when isnull(Opp.tintDeferred,0)=1 then ' (Deferred)' else '' end else 'Non-Authoritative' end )  end as BizDocType,
  isnull(monAmountPaid,0) monAmountPaid, 
  ISNULL(C.varCurrSymbol,'') varCurrSymbol ,
  ISNULL(Opp.[bitAuthoritativeBizDocs],0) bitAuthoritativeBizDocs,
  ISNULL(oppmst.[tintshipped] ,0) tintshipped,isnull(opp.numShipVia,0) as numShipVia,oppmst.numDivisionId,isnull(Opp.tintDeferred,0) as tintDeferred,
  ISNULL(Opp.numBizDocTempID,0) AS numBizDocTempID,con.numContactid,isnull(con.vcEmail,'') AS vcEmail,ISNULL(Opp.numBizDocStatus,0) AS numBizDocStatus,
--  case when isnumeric(ISNULL(dbo.fn_GetListItemName(isnull(oppmst.intBillingDays,0)),0))=1 
--	   then ISNULL(dbo.fn_GetListItemName(isnull(oppmst.intBillingDays,0)),0) 
--	   else 0 
--  end AS numBillingDaysName,
  CASE WHEN ISNUMERIC((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(oppmst.intBillingDays,0))) = 1
 	   THEN (SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(oppmst.intBillingDays,0))
	   ELSE 0
  END AS numBillingDaysName,
  Opp.dtFromDate,
  opp.dtFromDate AS BillingDate,
  ISNULL(C.fltExchangeRate,1) fltExchangeRateCurrent,
  ISNULL(oppmst.fltExchangeRate,1) fltExchangeRateOfOrder,
  @BaseCurrencySymbol BaseCurrencySymbol,0 AS numReturnHeaderID,0 AS tintReturnType,ISNULL(Opp.fltExchangeRateBizDoc,ISNULL(oppmst.fltExchangeRate,1)) AS fltExchangeRateBizDoc,
  CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingReport WHERE numOppBizDocId = opp.numOppBizDocsId AND numDomainID = @numDomainID) > 0 THEN 1 ELSE 0 END AS [IsShippingReportGenerated],
  --CASE WHEN (SELECT ISNULL(vcTrackingNo,'') FROM dbo.OpportunityBizDocs WHERE numOppBizDocsId = opp.numOppBizDocsId) <> '' THEN 1 ELSE 0 END AS [ISTrackingNumGenerated]
  CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingBox 
			 WHERE numShippingReportId IN (SELECT numShippingReportId FROM dbo.ShippingReport WHERE numOppBizDocId = opp.numOppBizDocsId AND numDomainID = @numDomainID)
			 AND LEN(ISNULL(vcTrackingNumber,'')) > 0) > 0 THEN 1 ELSE 0 END AS [ISTrackingNumGenerated],
  ISNULL((SELECT MAX(numShippingReportId) from ShippingReport SR 
		  WHERE SR.numOppBizDocId = opp.numoppbizdocsid 
		  AND SR.numDomainID = oppmst.numDomainID),0) AS numShippingReportId,
  ISNULL((SELECT MAX(SB.numShippingReportId) FROM [ShippingBox] SB 
		  JOIN dbo.ShippingReport SR ON SB.numShippingReportId = SR.numShippingReportId 
		  where SR.numOppBizDocId = opp.numoppbizdocsid AND SR.numDomainID = oppmst.numDomainID 
		  AND LEN(ISNULL(SB.vcTrackingNumber,''))>0),0) AS numShippingLabelReportId			 
 ,ISNULL(STUFF(( SELECT  ',' +  CASE WHEN LEN(ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'')) > 0 THEN ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'') ELSE '' END
                FROM    [dbo].[WarehouseLocation] AS WL
				JOIN [dbo].[Warehouses] W ON [WL].[numWarehouseID] = [W].[numWareHouseID]
                WHERE  [WL].[numWLocationID] IN (
                        SELECT  WHI.[numWLocationID]
                        FROM    [dbo].[WareHouseItems] AS WHI
                                JOIN [dbo].[OpportunityItems] AS OI ON [WHI].[numWareHouseItemID] = [OI].[numWarehouseItmsID]
                        WHERE   [OI].[numOppId] = oppmst.[numOppId])
              FOR
                XML PATH('')
              ), 1, 1, ''),'') AS [vcLocation],
		(CASE WHEN ISNULL(RecurrenceConfiguration.numRecConfigID,0) = 0 THEN 0 ELSE 1 END) AS bitRecur,
		ISNULL(Opp.bitRecurred,0) AS bitRecurred,
		ISNULL((SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppID=@numOppID AND ISNULL(numDeferredBizDocID,0) = ISNULL(opp.numOppBizDocsId,0)),0) AS intInvoiceForDiferredBizDoc,
		ISNULL(Opp.bitShippingGenerated,0) AS bitShippingGenerated,
		ISNULL(oppmst.intUsedShippingCompany,0) as intUsedShippingCompany,
		numSourceBizDocId,
		ISNULL((SELECT TOP 1 vcBizDocID FROM OpportunityBizDocs WHERE numOppBizDOcsId=opp.numSourceBizDocId),'NA') AS vcSourceBizDocID,
		ISNULL(oppmst.numShipmentMethod,0) numShipmentMethod,
		ISNULL(oppmst.numShippingService,0) numShippingService
 FROM                         
 OpportunityBizDocs  opp                        
 left join ListDetails mst                        
 on mst.numListItemID=opp.numBizDocId  
 LEFT JOIN ListDetailsName
 ON ListDetailsName.numListItemID=opp.numBizDocId 
 AND ListDetailsName.numDomainID=@numDomainID
 join OpportunityMaster oppmst                        
 on oppmst.numOppId= opp.numOppId                                
 left join  AuthoritativeBizDocs  AB
 on AB.numDomainId=  oppmst.numDomainID             
 LEFT OUTER JOIN [Currency] C
  ON C.numCurrencyID = oppmst.numCurrencyID
  LEFT JOIN AdditionalContactsInformation con ON con.numContactid = oppmst.numContactId
  LEFT JOIN
	RecurrenceConfiguration
  ON
	RecurrenceConfiguration.numOppBizDocID = opp.numOppBizDocsId
where opp.numOppId=@numOppId

UNION ALL

select  0 AS numOppBizDocsId,                        
 RH.numOppId,                        
 0 AS numBizDocId,                        
 1 AS numOrientation,  
 '' AS vcTemplateName,
 0 AS bitKeepFooterBottom,                   
 '' AS vcPOppName,                        
 'RMA' as BizDoc,                        
 RH.numCreatedBy,                        
DATEADD(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) CreatedDate,
 '' vcRefOrderNo,                       
 RH.vcRMA AS vcBizDocID,
 RH.vcRMA AS vcBizDocName,      
  ISNULL(monAmount,0) as monPAmount,
  'RMA' as BizDocType,
  0 AS monAmountPaid, 
  '' varCurrSymbol ,
  0 bitAuthoritativeBizDocs,
  0 tintshipped,0 as numShipVia,RH.numDivisionId,0 as tintDeferred,
  0 AS numBizDocTempID,con.numContactid,isnull(con.vcEmail,'') AS vcEmail,0 AS numBizDocStatus,
  0 AS numBillingDaysName,RH.dtCreatedDate AS dtFromDate,
  RH.dtCreatedDate AS BillingDate,
  1 fltExchangeRateCurrent,
  1 fltExchangeRateOfOrder,
  @BaseCurrencySymbol BaseCurrencySymbol,RH.numReturnHeaderID,RH.tintReturnType,1 AS fltExchangeRateBizDoc
  ,0 [IsShippingReportGenerated],0 [ISTrackingNumGenerated],
  0 AS numShippingReportId,
  0 AS numShippingLabelReportId			 
  ,ISNULL(STUFF(( SELECT  ',' +  CASE WHEN LEN(ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'')) > 0 THEN ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'') ELSE '' END
                FROM    [dbo].[WarehouseLocation] AS WL
				JOIN [dbo].[Warehouses] W ON [WL].[numWarehouseID] = [W].[numWareHouseID]
                WHERE  [WL].[numWLocationID] IN (
                        SELECT  WHI.[numWLocationID]
                        FROM    [dbo].[WareHouseItems] AS WHI
                                JOIN [dbo].[OpportunityItems] AS OI ON [WHI].[numWareHouseItemID] = [OI].[numWarehouseItmsID]
                        WHERE   [OI].[numOppId] = RH.[numOppId])
              FOR
                XML PATH('')
              ), 1, 1, ''),'') AS [vcLocation],
			  0 AS bitRecur,
			  0 AS bitRecurred,
		0 AS intInvoiceForDiferredBizDoc ,
		0 AS bitShippingGenerated,
		0 as intUsedShippingCompany,
		0 AS numSourceBizDocId,
		'NA' AS vcSourceBizDocID,
		0 AS numShipmentMethod,
		0 AS numShippingService
from                         
 ReturnHeader RH                        
 LEFT JOIN AdditionalContactsInformation con ON con.numContactid = RH.numContactId
  
where RH.numOppId=@numOppId
)X ORDER BY CreatedDate ASC       
                        
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OPPGetOppAddressDetails')
DROP PROCEDURE USP_OPPGetOppAddressDetails
GO
CREATE PROCEDURE [dbo].[USP_OPPGetOppAddressDetails]
(
    @numOppId AS NUMERIC(18,0) = NULL,
    @numDomainID AS NUMERIC(18,0)  = 0,
	@numOppBizDocID AS NUMERIC(18,0) = 0
)
AS

  DECLARE  @tintOppType  AS TINYINT, @tintBillType  AS TINYINT, @tintShipType  AS TINYINT
 
  DECLARE @numParentOppID AS NUMERIC,@numDivisionID AS NUMERIC,@numContactID AS NUMERIC, @numBillToAddressID NUMERIC(18,0), @numShipToAddressID NUMERIC(18,0),@numVendorAddressID NUMERIC(18,0)
      
  SELECT  @tintOppType = tintOppType,
		  @tintBillType = tintBillToType,
		  @tintShipType = tintShipToType,
          @numDivisionID = numDivisionID,
          @numContactID = numContactID,
		  @numBillToAddressID = ISNULL(numBillToAddressID,0),
		  @numShipToAddressID = ISNULL(numShipToAddressID,0),
		  @numVendorAddressID= ISNULL(numVendorAddressID,0)
      FROM   OpportunityMaster WHERE  numOppId = @numOppId


	 IF @tintOppType=2
		BEGIN
			--************Vendor Billing Address************
			IF @numBillToAddressID > 0
			BEGIN
				SELECT  
					'<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
            		(SELECT CMP.vcCompanyName FROM DivisionMaster DM JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID WHERE DM.numDivisionID = AD.numRecordID) AS vcCompanyName
					,(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  END) AS vcContact
				 FROM 
					AddressDetails AD 
				 WHERE	
					AD.numDomainID=@numDomainID 
					AND AD.numAddressID = @numBillToAddressID
			END
			ELSE IF EXISTS (SELECT * FROM OpportunityAddress WHERE numOppID=@numOppId)
				SELECT TOP 1
					'<pre>' + isnull(AD.vcBillStreet,'') + '</pre>' + isnull(AD.vcBillCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numBillState),'') + ' ' + isnull(AD.vcBillPostCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numBillCountry),'') AS vcFullAddress,
					isnull(AD.vcBillStreet,'') AS vcStreet,
					isnull(AD.vcBillCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numBillState),'') AS vcState,
					isnull(AD.vcBillPostCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numBillCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
            		CASE WHEN LEN(AD.vcBillCompanyName) > 0 THEN AD.vcBillCompanyName ELSE ISNULL((SELECT CMP.vcCompanyName FROM CompanyInfo CMP WHERE CMP.numCompanyId = AD.numBillCompanyId and CMP.numDomainID = @numDomainID),'') END AS vcCompanyName
					,(CASE 
						WHEN ISNULL(bitAltBillingContact,0) = 1 
						THEN ISNULL(vcAltBillingContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numBillingContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numBillingContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation INNER JOIN DivisionMaster ON AdditionalContactsInformation.numDivisionId=DivisionMaster.numDivisionID WHERE AdditionalContactsInformation.numDomainID=@numDomainID AND DivisionMaster.numCompanyID=AD.numBillCompanyId AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  END) AS vcContact
				 FROM 
					OpportunityAddress AD 
				 WHERE	
					AD.numOppID = @numOppId
			ELSE
			BEGIN
				SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
						isnull(AD.vcStreet,'') AS vcStreet,
						isnull(AD.vcCity,'') AS vcCity,
						isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
						isnull(AD.vcPostalCode,'') AS vcPostalCode,
						isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
						ISNULL(AD.vcAddressName,'') AS vcAddressName,
            			CMP.vcCompanyName
						,(CASE 
							WHEN ISNULL(bitAltContact,0) = 1 
							THEN ISNULL(vcAltContact,'') 
							ELSE 
								(CASE 
									WHEN ISNULL(numContact,0) > 0 
									THEN 
										ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
									ELSE 
										ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
									END
								)  END) AS vcContact
				 FROM AddressDetails AD 
				 JOIN [DivisionMaster] DM ON DM.numDivisionID = AD.numRecordID   
				 JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID  
				 WHERE AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
				 AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1
			END

			--************Vendor Shipping Address************
			IF @numVendorAddressID > 0
			BEGIN
				SELECT  
					'<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
            		(SELECT CMP.vcCompanyName FROM DivisionMaster DM JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID WHERE DM.numDivisionID = AD.numRecordID) AS vcCompanyName,
					(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  END) AS vcContact
				 FROM 
					AddressDetails AD 
				 WHERE	
					AD.numDomainID=@numDomainID 
					AND AD.numAddressID = @numVendorAddressID
			END
			ELSE IF @numShipToAddressID > 0
			BEGIN
				SELECT  
					'<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
            		(SELECT CMP.vcCompanyName FROM DivisionMaster DM JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID WHERE DM.numDivisionID = AD.numRecordID) AS vcCompanyName,
					(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  END) AS vcContact
				 FROM 
					AddressDetails AD 
				 WHERE	
					AD.numDomainID=@numDomainID 
					AND AD.numAddressID = @numShipToAddressID
			END
			ELSE IF EXISTS (SELECT * FROM OpportunityAddress WHERE numOppID=@numOppId)
				SELECT TOP 1
					'<pre>' + isnull(AD.vcShipStreet,'') + '</pre>' + isnull(AD.vcShipCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numShipState),'') + ' ' + isnull(AD.vcShipPostCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numShipCountry),'') AS vcFullAddress,
					isnull(AD.vcShipStreet,'') AS vcStreet,
					isnull(AD.vcShipCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numShipState),'') AS vcState,
					isnull(AD.vcShipPostCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numShipCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
            		CASE WHEN LEN(AD.vcShipCompanyName) > 0 THEN AD.vcShipCompanyName ELSE ISNULL((SELECT CMP.vcCompanyName FROM CompanyInfo CMP WHERE CMP.numCompanyId = AD.numShipCompanyId and CMP.numDomainID = @numDomainID),'') END AS vcCompanyName
					,(CASE 
						WHEN ISNULL(bitAltShippingContact,0) = 1 
						THEN ISNULL(vcAltShippingContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numShippingContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numShippingContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numShipCompanyID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  END) AS vcContact
				 FROM 
					OpportunityAddress AD 
				 WHERE	
					AD.numOppID = @numOppId
			ELSE
			BEGIN
				SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
					CMP.vcCompanyName
					,(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  END) AS vcContact
				FROM AddressDetails AD 
				JOIN [DivisionMaster] DM ON DM.numDivisionID = AD.numRecordID   
				JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID  
				WHERE AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
				AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1
			END
			
			 --************Employer Billing Address************
			 SELECT '<pre>' + isnull(vcStreet,'') + '</pre>' + isnull(vcCity,'') + ', ' +isnull(vcState,'') + ' ' + isnull(vcPostalCode,'') + ' <br>' + isnull(vcCountry,'') AS vcFullAddress,
					isnull(vcStreet,'') AS vcStreet,
					isnull(vcCity,'') AS vcCity,
					isnull(vcState,'') AS vcState,
					isnull(vcPostalCode,'') AS vcPostalCode,
					isnull(vcCountry,'') AS vcCountry,
					ISNULL(vcAddressName,'') AS vcAddressName,isnull(vcCompanyName,'') AS vcCompanyName
					,vcContact
					FROM fn_getOPPAddressDetails(@numOppId,@numDomainID,1) 
			 
			 
			 --************Employer Shipping Address************
			 SELECT '<pre>' + isnull(vcStreet,'') + '</pre>' + isnull(vcCity,'') + ', ' + isnull(vcState,'') + ' ' + isnull(vcPostalCode,'') + ' <br>' + isnull(vcCountry,'') AS vcFullAddress,
					isnull(vcStreet,'') AS vcStreet,
					isnull(vcCity,'') AS vcCity,
					isnull(vcState,'') AS vcState,
					isnull(vcPostalCode,'') AS vcPostalCode,
					isnull(vcCountry,'') AS vcCountry,
					ISNULL(vcAddressName,'') AS vcAddressName,isnull(vcCompanyName,'') AS vcCompanyName
					,vcContact
					FROM fn_getOPPAddressDetails(@numOppId,@numDomainID,2) 
	  END

	IF @tintOppType=1 
		BEGIN
			--************Customer Billing Address************
			IF @numBillToAddressID > 0
			BEGIN
				SELECT  
					'<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
            		(SELECT CMP.vcCompanyName FROM DivisionMaster DM JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID WHERE DM.numDivisionID = AD.numRecordID) AS vcCompanyName
					,(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  
					END) AS vcContact
				 FROM 
					AddressDetails AD 
				 WHERE	
					AD.numDomainID=@numDomainID 
					AND AD.numAddressID = @numBillToAddressID
			END
			ELSE
			BEGIN
				SELECT 
					'<pre>' + isnull(vcStreet,'') + '</pre>' + isnull(vcCity,'') + ', ' + isnull(vcState,'') + ' ' + isnull(vcPostalCode,'') + ' <br>' + isnull(vcCountry,'') AS vcFullAddress,
					isnull(vcStreet,'') AS vcStreet,
					isnull(vcCity,'') AS vcCity,
					isnull(vcState,'') AS vcState,
					isnull(vcPostalCode,'') AS vcPostalCode,
					isnull(vcCountry,'') AS vcCountry,
					ISNULL(vcAddressName,'') AS vcAddressName,isnull(vcCompanyName,'') AS vcCompanyName
					,vcContact
				FROM 
					fn_getOPPAddressDetails(@numOppId,@numDomainID,1) 
			END
		
			--************Customer Shipping Address************
			IF ISNULL(@numOppBizDocID,0) > 0 AND (SELECT COUNT(*) FROM (SELECT DISTINCT numShipToAddressID FROM OpportunityItems OI WHERE numOppId=@numOppId AND ISNULL(OI.numShipToAddressID,0) > 0) TEMP) > 1
			BEGIN
				SET @numShipToAddressID = (SELECT TOP 1 numShipToAddressID FROM OpportunityBizDocItems OBDI INNER JOIN OpportunityItems OI ON OBDI.numOppItemID=OI.numoppitemtCode WHERE OBDI.numOppBizDocID=@numOppBizDocID AND ISNULL(OI.numShipToAddressID,0) > 0)

				SELECT  
					'<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
            		(SELECT CMP.vcCompanyName FROM DivisionMaster DM JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID WHERE DM.numDivisionID = AD.numRecordID) AS vcCompanyName
					,(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  
					END) AS vcContact
				 FROM 
					AddressDetails AD 
				 WHERE	
					AD.numDomainID=@numDomainID 
					AND AD.numAddressID = @numShipToAddressID
			END
			ELSE IF @numShipToAddressID > 0
			BEGIN
				SELECT  
					'<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
            		(SELECT CMP.vcCompanyName FROM DivisionMaster DM JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID WHERE DM.numDivisionID = AD.numRecordID) AS vcCompanyName
					,(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  
					END) AS vcContact
				 FROM 
					AddressDetails AD 
				 WHERE	
					AD.numDomainID=@numDomainID 
					AND AD.numAddressID = @numShipToAddressID
			END
			ELSE
			BEGIN
				SELECT 
					'<pre>' + isnull(vcStreet,'') + '</pre>' + ISNULL(vcCity,'') + ', ' + isnull(vcState,'') + ' ' + isnull(vcPostalCode,'') + ' <br>' + isnull(vcCountry,'') AS vcFullAddress,
					isnull(vcStreet,'') AS vcStreet,
					isnull(vcCity,'') AS vcCity,
					isnull(vcState,'') AS vcState,
					isnull(vcPostalCode,'') AS vcPostalCode,
					isnull(vcCountry,'') AS vcCountry,
					ISNULL(vcAddressName,'') AS vcAddressName,isnull(vcCompanyName,'') AS vcCompanyName
					,vcContact
				FROM 
					fn_getOPPAddressDetails(@numOppId,@numDomainID,2) 
			END
	
			--************Employer Billing Address************ --(For SO take Domain Address)
			SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,vcCompanyName
					,(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  
					END) AS vcContact
             FROM Domain D1 JOIN divisionmaster div1 ON D1.numDivisionID = div1.numDivisionID
				  JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
                  JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID 
              WHERE  D1.numDomainID = @numDomainID
				AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1


			--************Employer Shipping Address************ --(For SO take Domain Address)
			SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,vcCompanyName
					,(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  
					END) AS vcContact
             FROM Domain D1 JOIN divisionmaster div1 ON D1.numDivisionID = div1.numDivisionID
				  JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
                  JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID 
			 WHERE AD.numDomainID=@numDomainID 
			 AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1
	  END
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount DECIMAL(20,5) =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(18,0),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0,
  @numShipmentMethod NUMERIC(18,0)=0,
  @dtReleaseDate DATE = NULL,
  @numPartner NUMERIC(18,0)=0,
  @tintClickBtn INT=0,
  @numPartenerContactId NUMERIC(18,0)=0,
  @numAccountClass NUMERIC(18,0) = 0,
  @numWillCallWarehouseID NUMERIC(18,0) = 0,
  @numVendorAddressID NUMERIC(18,0) = 0,
  @numShipFromWarehouse NUMERIC(18,0) = 0,
  @numShippingService NUMERIC(18,0) = 0,
  @ClientTimeZoneOffset INT = 0
  -- USE PARAMETER WITH OPTIONAL VALUE BECAUSE PROCEDURE IS CALLED FROM OTHER PROCEDURE WHICH WILL NOT PASS NEW PARAMETER VALUE
)                                                                          
                                                                          
as  
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @fltExchangeRate float                                 
	DECLARE @hDocItem int                                                                                                                            
	DECLARE @TotalAmount as DECIMAL(20,5)                                                                          
	DECLARE @tintOppStatus as tinyint
	DECLARE @bitNewOrder AS BIT = (CASE WHEN ISNULL(@numOppID,0) = 0 THEN 1 ELSE 0 END)
	
	IF ISNULL(@numContactId,0) = 0
	BEGIN
		RAISERROR('CONCAT_REQUIRED',16,1)
		RETURN
	END  
	
	IF ISNULL(@tintOppType,0) = 0
	BEGIN
		RAISERROR('OPP_TYPE_CAN_NOT_BE_0',16,1)
		RETURN
	END             

	IF ISNULL(@numOppID,0) > 0 AND EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numDomainId=@numDomainId AND numOppId=@numOppID AND ISNULL(tintshipped,0) = 1) 
	BEGIN	 
		RAISERROR('OPP/ORDER_IS_CLOSED',16,1)
		RETURN
	END

	DECLARE @dtItemRelease AS DATE
	SET @dtItemRelease = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())


	IF CONVERT(VARCHAR(10),@strItems) <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

		--VALIDATE PROMOTION OF COUPON CODE
		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1),
			numOppItemID NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numOppItemID,
			numPromotionID
		)
		SELECT
			numoppitemtCode,
			numPromotionID
		FROM
			OPENXML (@hDocItem,'/NewDataSet/Item',2)
		WITH
		(
			numoppitemtCode NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		IF (SELECT
				COUNT(*)
			FROM
				PromotionOffer PO
			WHERE
				numDomainId=@numDomainId
				AND bitRequireCouponCode = 1
				AND ISNULL(bitEnabled,0) = 1
				AND ISNULL(tintUsageLimit,0) > 0
				AND (intCouponCodeUsed 
					- (CASE WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE numOppID=ISNULL(@numOppID,0) AND numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END) 
					+ (CASE WHEN ISNULL((SELECT COUNT(*) FROM @TEMP WHERE numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END)) > tintUsageLimit) > 0
		BEGIN
			RAISERROR('USAGE_OF_COUPON_EXCEED',16,1)
			RETURN
		END
		ELSE
		BEGIN
			UPDATE
				PO
			SET 
				intCouponCodeUsed = ISNULL(intCouponCodeUsed,0) 
									- (CASE WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE numOppID=ISNULL(@numOppID,0) AND numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END) 
									+ (CASE WHEN ISNULL(T1.intUsed,0) > 0 THEN 1 ELSE 0 END) 
			FROM
				PromotionOffer PO
			INNER JOIN
				(
					SELECT
						numPromotionID,
						COUNT(*) AS intUsed
					FROM
						@TEMP
					GROUP BY
						numPromotionID
				) AS T1
			ON
				PO.numProId = T1.numPromotionID
			WHERE
				numDomainId=@numDomainId
				AND bitRequireCouponCode = 1
				AND ISNULL(bitEnabled,0) = 1
		END
	END
	DECLARE @numCompany AS NUMERIC(18)
	SELECT @numCompany = numCompanyID FROM DivisionMaster WHERE numDivisionID = @numDivisionId

	--IF NEW OPPERTUNITY
	IF @numOppID = 0                                                                          
	BEGIN
		DECLARE @bitIsInitalSalesOrder BIT
		IF(@DealStatus=1 AND @tintOppType=1)
		BEGIN
			SET @bitIsInitalSalesOrder=1
		END

		DECLARE @intOppTcode AS NUMERIC(9)  
  
		SELECT @intOppTcode = MAX(numOppId) FROM OpportunityMaster                

		SET @intOppTcode = ISNULL(@intOppTcode,0) + 1                                                
		
		SET @vcPOppName = @vcPOppName + '-'+ CONVERT(varchar(4),YEAR(GETUTCDATE()))  +'-A'+ CONVERT(varchar(10),@intOppTcode)                                  
  
		IF ISNULL(@numCurrencyID,0) = 0 
		BEGIN
			SET @fltExchangeRate=1
			SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
		END
		ELSE
		BEGIN 
			SET @fltExchangeRate = dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
        END  
		                                                        
		IF ISNULL(@numAccountClass,0) = 0
		BEGIN                                                  
		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivisionID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
		END

        IF(@DealStatus=1 AND @tintOppType=1 AND @tintClickBtn=1)
		BEGIN
			SET @DealStatus=0
		END         
		--PRINT ('Deal Staus ' +@DealStatus)                                                      
		INSERT INTO OpportunityMaster
		(                                                                             
			numContactId,numDivisionId,txtComments,numCampainID,bitPublicFlag,tintSource,tintSourceType,vcPOppName,                                                                          
			intPEstimatedCloseDate,monPAmount,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,                                                                                                             
			numRecOwner,lngPConclAnalysis,tintOppType,numSalesOrPurType,numCurrencyID,fltExchangeRate,numAssignedTo,
			numAssignedBy,[tintOppStatus],numStatus,vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,
			bitInterestType,fltInterest,vcCouponCode,bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,
			vcMarketplaceOrderReportId,numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
			bitUseMarkupShippingRate,numMarkupShippingRate,intUsedShippingCompany,numShipmentMethod,numShippingService,numPartner,numPartenerContact,dtReleaseDate,bitIsInitalSalesOrder,numVendorAddressID,numShipFromWarehouse
		)                                                                          
		Values                                                                          
		(                                                                          
			@numContactId,@numDivisionId,ISNULL(@Comments,''),@CampaignID,@bitPublicFlag,@tintSource,@tintSourceType,@vcPOppName,                                                                          
			@dtEstimatedCloseDate,@monPAmount,@numUserCntID,(CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END),                                                                          
			@numUserCntID,GETUTCDATE(),@numDomainId,@numUserCntID,@lngPConclAnalysis,@tintOppType,@numSalesOrPurType,@numCurrencyID, 
			@fltExchangeRate,(CASE WHEN @numAssignedTo > 0 THEN @numAssignedTo ELSE NULL END),(CASE WHEN @numAssignedTo>0 THEN @numUserCntID ELSE NULL END),
			@DealStatus,@numStatus,@vcOppRefOrderNo,@bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,
			@vcCouponCode,@bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,@numPercentageComplete,
			@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,@bitUseMarkupShippingRate,@numMarkupShippingRate,@intUsedShippingCompany,@numShipmentMethod,@numShippingService,@numPartner,@numPartenerContactId,@dtReleaseDate,@bitIsInitalSalesOrder,@numVendorAddressID,@numShipFromWarehouse
		)                                                                                                                      
		
		SET @numOppID=(SELECT SCOPE_IDENTITY())                
                            
		--UPDATE OPPNAME AS PER NAME TEMPLATE
		EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

		--MAP CUSTOM FIELD	
		DECLARE @tintPageID AS TINYINT;
		SET @tintPageID = CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
		EXEC dbo.USP_AddParentChildCustomFieldMap
				@numDomainID = @numDomainID, --  numeric(18, 0)
				@numRecordID = @numOppID, --  numeric(18, 0)
				@numParentRecId = @numDivisionId, --  numeric(18, 0)
				@tintPageID = @tintPageID --  tinyint
 	
		IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
		BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END
  
		-- INSERTING ITEMS                                                                                                                    
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,
				monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
				numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,vcAttrValues,monAvgCost,bitItemPriceApprovalRequired,
				numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,numWOAssignedTo,numShipToAddressID,ItemReleaseDate
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
				(CASE X.numWarehouseItmsID 
				WHEN -1 
				THEN 
					CASE 
						WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU= X.vcSKU) > 0)
							THEN
								(SELECT TOP 1 
									numWareHouseItemID 
								FROM 
									WareHouseItems 
								WHERE 
									[numItemID] = X.numItemCode 
									AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
						ELSE
							(SELECT 
								[numWareHouseItemID] 
							FROM 
								[WareHouseItems] 
							WHERE 
								[numItemID] = X.numItemCode 
								AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				WHEN 0 
				THEN 
					CASE 
					WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU=X.vcSKU) > 0)
						THEN
							(SELECT TOP 1 
								numWareHouseItemID 
							FROM 
								WareHouseItems 
							WHERE 
								[numItemID] = X.numItemCode 
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
					ELSE
						(SELECT 
							[numWareHouseItemID] 
						FROM 
							[WareHouseItems] 
						WHERE 
							[numItemID] = X.numItemCode 
							AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
							AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				ELSE  
					X.numWarehouseItmsID 
				END) AS numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
				(SELECT isnull(VN.monCost,0) FROM Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode AND VN.numVendorID=IT.numVendorID WHERE  VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,
				0,
				X.numToWarehouseItemID,X.Attributes,X.AttributeIDs,(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),
				X.bitItemPriceApprovalRequired,X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(X.vcVendorNotes,'')
				,X.vcInstruction,X.bintCompliationDate,X.numWOAssignedTo,X.numShipToAddressID,ISNULL(ItemReleaseDate,@dtItemRelease)
			FROM
				(
					SELECT 
						*
					FROM 
						OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
					WITH                       
					(                                                                          
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9), numUnitHour FLOAT, monPrice DECIMAL(30,16), monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000),                                    
						numWarehouseItmsID NUMERIC(9), ItemType VARCHAR(30), DropShip BIT, bitDiscountType BIT,	fltDiscount DECIMAL(30,16), monTotAmtBefDiscount DECIMAL(20,5),
						vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),
						numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),AttributeIDs VARCHAR(500),vcSKU VARCHAR(100),bitItemPriceApprovalRequired BIT,
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),
						vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(18,0),numShipToAddressID  NUMERIC(18,0),ItemReleaseDate DATETIME
					)
				)X    
			ORDER BY 
				X.numoppitemtCode
    
			-- UPDATE EXISTING CUSTOMER PART NO
			UPDATE
				CPN
			SET
				CPN.CustomerPartNo = ISNULL(TEMPCPN.CustomerPartNo,'')
			FROM
				CustomerPartNumber CPN
			INNER JOIN
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) AS TEMPCPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.numDomainID=@numDomainID
				AND CPN.numCompanyId = @numCompany
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- INSERT NEW CUSTOMER PART NO
			INSERT INTO CustomerPartNumber
			(
				numCompanyId, numDomainID, CustomerPartNo, numItemCode 
			)
			SELECT 
				@numCompany, @numDomainID, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
			FROM
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) TEMPCPN 
			LEFT JOIN
				CustomerPartNumber CPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.CustomerPartNoID IS NULL
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'

			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,numWarehouseItmsID=X.numWarehouseItmsID,bitDropShip=X.DropShip,
				bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
				bitWorkOrder=X.bitWorkOrder,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,
				numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,vcAttributes=X.Attributes,
				numClassID=0,vcModelID=X.vcModelID,
				bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired,vcAttrValues=X.AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost
				,vcNotes=X.vcVendorNotes,vcInstruction=X.vcInstruction,bintCompliationDate=X.bintCompliationDate,numWOAssignedTo=X.numWOAssignedTo
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,
					monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,
					Attributes,bitItemPriceApprovalRequired,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) numCost
					,ISNULL(vcVendorNotes,'') vcVendorNotes,vcInstruction,bintCompliationDate,numWOAssignedTo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
				WITH  
					(                      
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000), vcModelID VARCHAR(300),
						numWarehouseItmsID NUMERIC(9),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),
						numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),
						numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000)
						,vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(18,0)
					)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppID=@numOppID
				
			--INSERT KIT ITEMS                 
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ID.numWareHouseItemId,
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId 

			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
				DECLARE @TempKitConfiguration TABLE
				(
					numItemCode NUMERIC(18,0),
					numWarehouseItemID NUMERIC(18,0),
					KitChildItems VARCHAR(MAX)
				)

				INSERT INTO 
					@TempKitConfiguration
				SELECT
					numItemCode,
					numWarehouseItmsID,
					KitChildItems
				FROM
					OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)
				WITH                       
				(
					numItemCode NUMERIC(9),numWarehouseItmsID NUMERIC(9), KitChildItems VARCHAR(MAX)
				) 

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped,
					monAvgCost
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					ItemDetails.numWareHouseItemId,
					(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0,
					ISNULL(Item.monAverageCost,0)
				FROM
					(
						SELECT 
							t1.numItemCode,
							t1.numWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										0,
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,0,CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										0, 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemWarehouseItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration WHERE numItemCode=t1.numItemCode AND numWarehouseItemID=t1.numWarehouseItemID),',') 
						) as t2
					) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numChildItemID=TempChildItems.numKitItemID
					AND OKI.numWarehouseItemId=TempChildItems.numKitWarehouseItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numWarehouseItmsID = TempChildItems.numWarehouseItemID
					AND OI.numoppitemtcode = OKI.numOppItemID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					AND (TempChildItems.numKitChildItemWarehouseItemID = ItemDetails.numWareHouseItemId  OR ISNULL(TempChildItems.numKitChildItemWarehouseItemID,0) = 0)
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END

			-- UPDATE UOM OF OPPORTUNITY ITEMS HAVE NOT UOM SET WHILE EXPORTED FROM MARKETPLACE (MEANS TINTSOURCETYPE = 3)
			IF ISNULL(@tintSourceType,0) = 3
			BEGIN
				UPDATE 
					OI 
				SET 
					numUOMID = ISNULL(I.numSaleUnit,0), 
					numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
				FROM 
					OpportunityItems OI
				JOIN 
					Item I 
				ON 
					OI.numItemCode = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND I.numDomainID = @numDomainId
			END	
                                     
			INSERT INTO OppWarehouseSerializedItem
			(
				numOppID,
				numWarehouseItmsDTLID,
				numOppItemID,
				numWarehouseItmsID
			)                      
			SELECT 
				@numOppID,
				X.numWarehouseItmsDTLID,
				X.numoppitemtCode,
				X.numWItmsID 
			FROM
			(                                                                          
				SELECT
					* 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
				WITH                        
				(                                               
				numWarehouseItmsDTLID numeric(9),                      
				numoppitemtCode numeric(9),                      
				numWItmsID numeric(9)                                                     
				)
			)X                                    
                                   
			UPDATE 
				OppWarehouseSerializedItem                       
			SET 
				numOppItemID=X.numoppitemtCode                                
			FROM                       
			(
				SELECT 
					numWarehouseItmsDTLID AS DTLID,
					O.numoppitemtCode 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
				WITH
				(
					numWarehouseItmsDTLID NUMERIC(9),numoppitemtCode NUMERIC(9),numWItmsID NUMERIC(9)
				)                                
				JOIN 
					OpportunityItems O 
				ON 
					O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID
			)X                 
			WHERE 
				numOppID=@numOppID 
				AND numWarehouseItmsDTLID=X.DTLID      
    
			EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
                     
			EXEC sp_xml_removedocument @hDocItem                                     
		END          
	END
	ELSE                                                                                                                          
	BEGIN

		DECLARE @tempAssignedTo AS NUMERIC(9) = NULL                                                    
		SELECT 
			@tintOppStatus = tintOppStatus,
			@tempAssignedTo=ISNULL(numAssignedTo,0) 
		FROM   
			OpportunityMaster 
		WHERE  
			numOppID = @numOppID

		IF @tempAssignedTo <> @numAssignedTo
		BEGIN
			IF (SELECT COUNT(*) FROM BizDocComission WHERE ISNULL(numOppID,0) = @numOppID AND ISNULL(bitCommisionPaid,0) = 1) > 0
			BEGIN
				RAISERROR('COMMISSION_PAID_AGAINST_BIZDOC',16,1)
			END
		END
 
		--REVERTING BACK THE WAREHOUSE ITEMS                  
		IF @tintOppStatus = 1 
		BEGIN               
			EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID                  
		END                  

		IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
		BEGIN
			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
		END
		
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
		BEGIN
			EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END 
						
		UPDATE   
			OpportunityMaster
		SET      
			vcPOppName = @vcPOppName,txtComments = ISNULL(@Comments,''),bitPublicFlag = @bitPublicFlag,numCampainID = @CampaignID,tintSource = @tintSource,tintSourceType=@tintSourceType,
			intPEstimatedCloseDate = @dtEstimatedCloseDate,numModifiedBy = @numUserCntID,bintModifiedDate = GETUTCDATE(),lngPConclAnalysis = @lngPConclAnalysis,monPAmount = @monPAmount,
			tintActive = @tintActive,numSalesOrPurType = @numSalesOrPurType,numStatus = @numStatus,vcOppRefOrderNo=@vcOppRefOrderNo,
			bintOppToOrder=(CASE WHEN @tintOppStatus=0 AND @DealStatus=1 THEN GETUTCDATE() ELSE bintOppToOrder END),bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,
			bitBillingTerms=@bitBillingTerms,intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,tintTaxOperator=@tintTaxOperator,
			numDiscountAcntType=@numDiscountAcntType,vcCouponCode = @vcCouponCode,numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
			intUsedShippingCompany = @intUsedShippingCompany, numContactID=@numContactId,numShipmentMethod=@numShipmentMethod,numShippingService=@numShippingService,numPartner=@numPartner,numPartenerContact=@numPartenerContactId
		WHERE
			numOppId = @numOppID   
	   
		-- UPDATING IF ORGANIZATION IS ASSIGNED TO SOMEONE                                                      
		IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN
			UPDATE
				OpportunityMaster 
			SET	
				numAssignedTo = @numAssignedTo, 
				numAssignedBy = @numUserCntID 
			WHERE 
				numOppId = @numOppID                                                    
		END                                                     
		ELSE IF(@numAssignedTo = 0) 
		BEGIN
			UPDATE 
				OpportunityMaster 
			SET 
				numAssignedTo = 0, 
				numAssignedBy = 0 
			WHERE
				numOppId = @numOppID
		END  
			                                                                    
		-- UPDATING OPP ITEMS
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0                                                                         
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   
			IF EXISTS 
			(
				SELECT 
					OI.numoppitemtCode
				FROM 
					OpportunityItems OI
				WHERE 
					numOppID=@numOppID 
					AND OI.numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))
					AND ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) > 0
			)
			BEGIN
				RAISERROR('SERIAL/LOT#_ASSIGNED_TO_ITEM_DELETED',16,1)
			END

			--DELETE CHILD ITEMS OF KIT WITHIN KIT
			DELETE FROM 
				OpportunityKitChildItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   

			--DELETE ITEMS
			DELETE FROM 
				OpportunityKitItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))                      
			                            
			DELETE FROM 
				OpportunityItems 
			WHERE 
				numOppID=@numOppID 
				AND numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   
	          
			-- DELETE CORRESPONSING TIME AND EXPENSE ENTRIES OF DELETED ITEMS
			DELETE FROM
				TimeAndExpense
			WHERE
				numDomainID=@numDomainId
				AND numOppId=@numOppID
				AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9))) 

			--INSERT NEW ADDED ITEMS                        
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,
				[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numToWarehouseItemID,numShipmentMethod,
				numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired,vcAttrValues,numPromotionID,bitPromotionTriggered
				,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,numWOAssignedTo,ItemReleaseDate
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
				(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),
				(select isnull(VN.monCost,0) from Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode and VN.numVendorID=IT.numVendorID where VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numToWarehouseItemID,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
				0,
				(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired, X.AttributeIDs,
				X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(vcVendorNotes,'')
				,X.vcInstruction,X.bintCompliationDate,X.numWOAssignedTo,@dtItemRelease
			FROM
			(
				SELECT 
					*
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH  
				(                      
					numoppitemtCode numeric(9),numItemCode numeric(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc varchar(1000),                                    
					numWarehouseItmsID numeric(9),numToWarehouseItemID numeric(9),Op_Flag tinyint,ItemType varchar(30),DropShip bit,bitDiscountType bit,
					fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),
					numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(500), 
					bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000)
					,numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(18,0)
				)
			)X 
			
			-- UPDATE EXISTING CUSTOMER PART NO
			UPDATE
				CPN
			SET
				CPN.CustomerPartNo = ISNULL(TEMPCPN.CustomerPartNo,'')
			FROM
				CustomerPartNumber CPN
			INNER JOIN
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) AS TEMPCPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.numDomainID=@numDomainID
				AND CPN.numCompanyId = @numCompany
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- INSERT NEW CUSTOMER PART NO
			INSERT INTO CustomerPartNumber
			(
				numCompanyId, numDomainID, CustomerPartNo, numItemCode 
			)
			SELECT 
				@numCompany, @numDomainID, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
			FROM
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) TEMPCPN 
			LEFT JOIN
				CustomerPartNumber CPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.CustomerPartNoID IS NULL
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- UPDATE ITEMS
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,vcModelID=X.vcModelID,numWarehouseItmsID=X.numWarehouseItmsID,numToWarehouseItemID = X.numToWarehouseItemID,             
				bitDropShip=X.DropShip,bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,
				numUOMId=numUOM,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,
				numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired,bitWorkOrder=X.bitWorkOrder,vcAttrValues=AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost,vcNotes=vcVendorNotes
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,numToWarehouseItemID,DropShip,bitDiscountType,
					fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
					bitItemPriceApprovalRequired,bitWorkOrder,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) AS numCost,ISNULL(vcVendorNotes,'') vcVendorNotes
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
				WITH  
				(                      
					numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000),vcModelID VARCHAR(300), numWarehouseItmsID NUMERIC(9), 
					numToWarehouseItemID NUMERIC(18,0),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),
					numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),numProjectStageID NUMERIC(9),Attributes VARCHAR(500),
					bitItemPriceApprovalRequired BIT, bitWorkOrder BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000)                           
				)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppId=@numOppID

			--INSERT NEW ADDED KIT ITEMS                 
			INSERT INTO OpportunityKitItems                                                                          
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ID.numWareHouseItemId,
				ID.numQtyItemsReq * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID
			INNER JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId 
				AND OI.numoppitemtCode NOT IN (SELECT ISNULL(numOppItemID,0) FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID)  
				
			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
				DELETE FROM @TempKitConfiguration

				INSERT INTO 
					@TempKitConfiguration
				SELECT
					numItemCode,
					numWarehouseItmsID,
					KitChildItems
				FROM
					OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)
				WITH                       
				(
					numItemCode NUMERIC(9),numWarehouseItmsID NUMERIC(9), KitChildItems VARCHAR(MAX)
				) 

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped,
					monAvgCost
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					ItemDetails.numWareHouseItemId,
					ItemDetails.numQtyItemsReq * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0,
					ISNULL(Item.monAverageCost,0)
				FROM
					(
						SELECT 
							t1.numItemCode,
							t1.numWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										0,
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,0,CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										0, 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemWarehouseItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration WHERE numItemCode=t1.numItemCode AND numWarehouseItemID=t1.numWarehouseItemID),',') 
						) as t2
					) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numChildItemID=TempChildItems.numKitItemID
					AND OKI.numWarehouseItemId=TempChildItems.numKitWarehouseItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numWarehouseItmsID = TempChildItems.numWarehouseItemID
					AND OI.numoppitemtcode = OKI.numOppItemID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					AND (TempChildItems.numKitChildItemWarehouseItemID = ItemDetails.numWareHouseItemId  OR ISNULL(TempChildItems.numKitChildItemWarehouseItemID,0) = 0)
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END                                  
			
			--UPDATE KIT ITEMS                 
			UPDATE 
				OKI 
			SET 
				numQtyItemsReq = numQtyItemsReq_Orig * OI.numUnitHour
			FROM 
				OpportunityKitItems OKI 
			JOIN 
				OpportunityItems OI 
			ON 
				OKI.numOppItemID=OI.numoppitemtCode 
				AND OKI.numOppId=OI.numOppId  
			WHERE 
				OI.numOppId=@numOppId 

			-- UPDATE CHILD ITEMS OF KIT WITHIN KIT
			UPDATE
				OKCI
			SET
				OKCI.numQtyItemsReq = (OKI.numQtyItemsReq * OKCI.numQtyItemsReq_Orig)
			FROM
				OpportunityKitChildItems OKCI
			INNER JOIN
				OpportunityKitItems OKI
			ON
				OKCI.numOppID = @numOppId
				AND OKCI.numOppItemID = OKI.numOppItemID
				AND OKCI.numOppChildItemID = OKI.numOppChildItemID                     
			                                    
			EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			   		                                                                      
			EXEC sp_xml_removedocument @hDocItem                                               
		END     

		IF @tintOppType=1 AND @tintOppStatus=1 --Sales Order
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0) = 0 AND (ISNULL(numUnitHour,0) - ISNULL(numQtyShipped,0)) < 0)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_IS_LESS_THEN_SHIPPED_QTY',16,1)
				RETURN
			END

			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems INNER JOIN WorkOrder ON OpportunityItems.numOppID=WorkOrder.numOppID AND OpportunityItems.numoppitemtCode=WorkOrder.numOppItemID AND ISNULL(WorkOrder.numParentWOID,0)=0 AND WorkOrder.numWOStatus=23184 AND ISNULL(OpportunityItems.numUnitHour,0) > ISNULL(WorkOrder.numQtyItemsReq,0) WHERE OpportunityItems.numOppID=@numOppID)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_CAN_NOT_BE_GREATER_THEN_COMPLETED_WORK_ORDER',16,1)
				RETURN
			END
		END
		ELSE IF @tintOppType=2 AND @tintOppStatus=1 --Purchase Order
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0) = 0 AND (ISNULL(numUnitHour,0) - ISNULL(numUnitHourReceived,0)) < 0)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_IS_LESS_THEN_RECEIVED_QTY',16,1)
				RETURN
			END
		END

		--FOLLOWING DELETE STATEMENT IS COMMENTED BECUASE NOW WE ARE NOT ALLOWING USER TO EIDT SERIAL/LOT ITEM UNTILL USER REMOVED ASSIGNED SERIAL/LOT NUMBERS
	    --Delete previously added serial lot number added for opportunity
		--DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
	END

	
	-- Set item release date to today
	If @tintOppType = 1
	BEGIN
		INSERT INTO OpportunityItemsReleaseDates
		(
			numDomainID
			,numOppID
			,numOppItemID
			,dtReleaseDate
		)
		SELECT
			@numDomainId
			,OI.numOppId
			,OI.numoppitemtCode
			,ISNULL(OI.ItemReleaseDate,@dtItemRelease)
		FROM
			OpportunityItems OI
		LEFT JOIN
			OpportunityItemsReleaseDates OIRD
		ON
			OI.numOppId=OIRD.numOppID
			AND OI.numoppitemtCode = OIRD.numOppItemID
		WHERE
			OI.numOppId=@numOppID
			AND OIRD.numID IS NULL
	END



	SET @TotalAmount=0                                                           
	SELECT @TotalAmount = SUM(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID                                                                          
  
	UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numOppID

/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
            
			
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT

			SET @bitIsInitalSalesOrder=(SELECT bitIsInitalSalesOrder FROM OpportunityMaster WHERE numOppId=@numOppID AND numDomainId=@numDomainID)
			--SET @bitViolatePrice=(SELECT bitMarginPriceViolated FROM Domain WHERE numDomainId=@numDomainID)
			IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainId=@numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1)
			BEGIN
				SET @bitViolatePrice = 1
			END
			
			IF(@bitViolatePrice=1 AND @bitIsInitalSalesOrder=1 AND @tintClickBtn = 1)
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus=@DealStatus WHERE numOppId=@numOppID
			END
			ELSE
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus = @DealStatus WHERE numOppId=@numOppID
				declare @tintShipped as tinyint               
				select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID              
				if @tintOppStatus=1               
					begin         
					PRINT 'inside USP_UpdatingInventoryonCloseDeal'
						exec USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
					end  
				declare @tintCRMType as numeric(9)        
				declare @AccountClosingDate as datetime        

				select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
				select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

				--When deal is won                                         
				IF @DealStatus = 1 
				BEGIN
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID              
					END

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=getutcdate()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
				END         
				--When Deal is Lost
				ELSE IF @DealStatus = 2
				BEGIN
					SELECT @AccountClosingDate=bintAccountClosingDate FROM OpportunityMaster WHERE numOppID=@numOppID         
					
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID
					END
				END 
			END

			/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
			-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
			if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
			begin        
				update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				where numDivisionID=@numDivisionID        
			end        

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50), @vcAltContact VARCHAR(200)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9), @numContact NUMERIC(18,0)
DECLARE @bitIsPrimary BIT, @bitAltContact BIT;
DECLARE @tintAddressOf TINYINT

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

--Bill Address
IF @numBillAddressId>0
BEGIN
	SELECT 
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM 
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numBillAddressId

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numBillAddressId
	END

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END
  
  
--Ship Address
IF @intUsedShippingCompany = 92 -- WILL CALL (PICK UP FROM Warehouse)
BEGIN
	IF EXISTS (SELECT numWareHouseID FROM Warehouses WHERE Warehouses.numDomainID = @numDomainID AND numWareHouseID = @numWillCallWarehouseID AND ISNULL(numAddressID,0) > 0)
	BEGIN
		SELECT  
			@vcStreet=ISNULL(vcStreet,'')
			,@vcCity=ISNULL(vcCity,'')
			,@vcPostalCode=ISNULL(AddressDetails.vcPostalCode,'')
			,@numState=ISNULL(numState,0)
			,@numCountry=ISNULL(numCountry,0)
			,@vcAddressName=vcAddressName
		FROM 
			dbo.Warehouses 
		INNER JOIN
			AddressDetails
		ON
			Warehouses.numAddressID = AddressDetails.numAddressID
		WHERE 
			Warehouses.numDomainID = @numDomainID 
			AND numWareHouseID = @numWillCallWarehouseID
	END
	ELSE
	BEGIN
	SELECT  
		@vcStreet=ISNULL(vcWStreet,'')
		,@vcCity=ISNULL(vcWCity,'')
		,@vcPostalCode=ISNULL(vcWPinCode,'')
		,@numState=ISNULL(numWState,0)
		,@numCountry=ISNULL(numWCountry,0)
		,@vcAddressName=''
	FROM 
		dbo.Warehouses 
	WHERE 
		numDomainID = @numDomainID 
		AND numWareHouseID = @numWillCallWarehouseID
	END

	UPDATE OpportunityMaster SET tintShipToType=2 WHERE numOppId=@numOppID
 
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = '', --  varchar(100)
	@numCompanyId =0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = 0,
	@bitAltContact = 0,
	@vcAltContact = ''
END
ELSE IF @numShipAddressId>0
BEGIN
	SELECT
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM  
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numShipAddressId
 

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numShipAddressId
	END


  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END

-- IMPORTANT:KEEP THIS BELOW ADDRESS UPDATE LOGIC
-- GET TAX APPLICABLE TO DIVISION WHEN ORDER IS CREATED AFTER THAT NOT NEED TO CHANGE IT
IF @bitNewOrder = 1
BEGIN
	--INSERT TAX FOR DIVISION   
	IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
	BEGIN
		INSERT INTO dbo.OpportunityMasterTaxItems 
		(
			numOppId,
			numTaxItemID,
			fltPercentage,
			tintTaxType,
			numTaxID
		) 
		SELECT 
			@numOppID,
			TI.numTaxItemID,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			TaxItems TI 
		JOIN 
			DivisionTaxTypes DTT 
		ON 
			TI.numTaxItemID = DTT.numTaxItemID
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
		) AS TEMPTax
		WHERE 
			DTT.numDivisionID=@numDivisionId 
			AND DTT.bitApplicable=1
		UNION 
		SELECT 
			@numOppID,
			0,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			dbo.DivisionMaster 
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
		) AS TEMPTax
		WHERE 
			bitNoTax=0 
			AND numDivisionID=@numDivisionID	
		UNION 
		SELECT
			@numOppId
			,1
			,decTaxValue
			,tintTaxType
			,numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,@numOppId,1,NULL)		
	END	
END

--INSERT TAX FOR DIVISION   
IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN
--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID
) 
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID = IT.numTaxItemID 
WHERE 
	OI.numOppId=@numOppID 
	AND IT.bitApplicable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	0,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numOppID 
	AND I.bitTaxable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT
	@numOppId,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
FROM
	dbo.OpportunityItems OI 
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
INNER JOIN
	TaxDetails TD
ON
	TD.numTaxID = IT.numTaxID
	AND TD.numDomainId = @numDomainId
WHERE
	OI.numOppId = @numOppID
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

END
  
 UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID

	IF @tintOppType=1 AND @tintOppStatus=1 
	BEGIN
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numStatus
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocItems_ChangePackingSlipQty')
DROP PROCEDURE USP_OpportunityBizDocItems_ChangePackingSlipQty
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocItems_ChangePackingSlipQty]                        
(              
	@numDomainID NUMERIC(18,0)                                
	,@numUserCntID NUMERIC(18,0)
	,@numOppId NUMERIC(18,0)
	,@numOppBizDocsId NUMERIC(18,0)
)
AS 
BEGIN
	-- CODE LEVEL TRANSACTION
	IF EXISTS (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppId=@numOppId AND numBizDocId=296 AND numSourceBizDocId=@numOppBizDocsId)
	BEGIN
		IF EXISTS (SELECT 
					OBDI.numOppItemID
				FROM 
					OpportunityBizDocs OBD
				INNER JOIN
					OpportunityBizDocItems OBDI
				ON
					OBDI.numOppBizDocID=OBD.numOppBizDocsId
				INNER JOIN 
					OpportunityBizDocs OBDFulfillment 
				ON  
					OBDFulfillment.numSourceBizDocId = OBD.numOppBizDocsId
				INNER JOIN 
					OpportunityBizDocItems OBDIFulfillment 
				ON  
					OBDIFulfillment.numOppBizDocID = OBDFulfillment.numOppBizDocsId
					AND OBDIFulfillment.numOppItemID = OBDI.numOppItemID
				WHERE 
					OBD.numOppId=@numOppId
					AND OBD.numOppBizDocsId=@numOppBizDocsId
				GROUP BY
					OBDI.numOppItemID
					,OBDI.numUnitHour
				HAVING
					OBDI.numUnitHour > ISNULL(SUM(OBDIFulfillment.numUnitHour),0))
		BEGIN
			UPDATE 
				OpportunityBizDocs
			SET    
				numModifiedBy = @numUserCntID,
				dtModifiedDate = GETUTCDATE()
			WHERE  
				numOppBizDocsId = @numOppBizDocsId

			IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 )
			BEGIN
				EXEC 
				USP_ManageOpportunityAutomationQueue			
				@numOppQueueID = 0, -- numeric(18, 0)							
				@numDomainID = @numDomainID, -- numeric(18, 0)							
				@numOppId = @numOppID, -- numeric(18, 0)							
				@numOppBizDocsId = @numOppBizDocsId, -- numeric(18, 0)							
				@numOrderStatus = 0, -- numeric(18, 0)					
				@numUserCntID = @numUserCntID, -- numeric(18, 0)					
				@tintProcessStatus = 1, -- tinyint						
				@tintMode = 1 -- TINYINT
			END 

			DECLARE @TEMP TABLE
			(
				numOppItemID NUMERIC(18,0)
				,numNewUnitHour FLOAT
			)

			INSERT INTO @TEMP
			(
				numOppItemID
				,numNewUnitHour
			)
			SELECT
				OBDI.numOppItemID
				,SUM(OBDIFulfillment.numUnitHour)
			FROM 
				OpportunityBizDocs OBD
			INNER JOIN
				OpportunityBizDocItems OBDI
			ON
				OBDI.numOppBizDocID=OBD.numOppBizDocsId
			INNER JOIN 
				OpportunityBizDocs OBDFulfillment 
			ON  
				OBDFulfillment.numSourceBizDocId = OBD.numOppBizDocsId
			INNER JOIN 
				OpportunityBizDocItems OBDIFulfillment 
			ON  
				OBDIFulfillment.numOppBizDocID = OBDFulfillment.numOppBizDocsId
				AND OBDIFulfillment.numOppItemID = OBDI.numOppItemID
			WHERE 
				OBD.numOppId=@numOppId
				AND OBD.numOppBizDocsId=@numOppBizDocsId
			GROUP BY
				OBDI.numOppItemID
				,OBDI.numUnitHour
			HAVING
				OBDI.numUnitHour > ISNULL(SUM(OBDIFulfillment.numUnitHour),0)


			UPDATE 
				OBI
			SET 
				OBI.numUnitHour = T1.numNewUnitHour
				,OBI.monPrice = OI.monPrice
				,OBI.monTotAmount = (OI.monTotAmount/OI.numUnitHour) * T1.numNewUnitHour
				,OBI.fltDiscount = (CASE WHEN OI.bitDiscountType=0 THEN OI.fltDiscount WHEN OI.bitDiscountType=1 THEN (OI.fltDiscount/OI.numUnitHour) * T1.numNewUnitHour ELSE OI.fltDiscount END)
				,OBI.monTotAmtBefDiscount = (OI.monTotAmtBefDiscount/OI.numUnitHour) * T1.numNewUnitHour 
			FROM 
				OpportunityBizDocItems OBI
			JOIN 
				OpportunityItems OI
			ON 
				OBI.numOppItemID=OI.numoppitemtCode
			INNER JOIN
				@TEMP T1
			ON
				OBI.numOppItemID = T1.numOppItemID
			WHERE 
				OI.numOppId=@numOppId 
				AND OBI.numOppBizDocID=@numOppBizDocsId

			UPDATE 
				OBI
			SET 
				OBI.numUnitHour = 0
				,OBI.monPrice = OI.monPrice
				,OBI.monTotAmount = 0
				,OBI.fltDiscount = 0
				,OBI.monTotAmtBefDiscount = 0 
			FROM 
				OpportunityBizDocItems OBI
			JOIN 
				OpportunityItems OI
			ON 
				OBI.numOppItemID=OI.numoppitemtCode
			WHERE 
				OI.numOppId=@numOppId 
				AND OBI.numOppBizDocID=@numOppBizDocsId
				AND OBI.numOppItemID NOT IN (SELECT numOppItemID FROM @TEMP)


			DECLARE @monDealAmount as DECIMAL(20,5)
			SELECT @monDealAmount=isnull(dbo.[GetDealAmount](@numOppId,GETUTCDATE(),@numOppBizDocsId),0)

			UPDATE 
				OpportunityBizDocs 
			SET 
				monDealAmount=@monDealAmount
			WHERE  
				numOppBizDocsId = @numOppBizDocsId
		END
	END
END
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OrgOppAddressModificationHistory_Get')
DROP PROCEDURE USP_OrgOppAddressModificationHistory_Get
GO
CREATE PROCEDURE [dbo].[USP_OrgOppAddressModificationHistory_Get] 
(
	@numDomainID NUMERIC(18,0)
	,@numRecordID NUMERIC(18,0)
	,@numAddressID NUMERIC(18,0)
	,@tintAddressOf TINYINT
	,@tintAddressType TINYINT
	,@ClientTimeZoneOffset INT
)
AS
BEGIN
	SELECT
		dbo.fn_GetContactName(numModifiedBy) AS vcModifiedBy
		,dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtModifiedDate),@numDomainID) dtModifiedDate
	FROM
		OrgOppAddressModificationHistory
	WHERE
		numRecordID=@numRecordID 
		AND tintAddressOf=@tintAddressOf 
		AND tintAddressType=@tintAddressType
		AND ISNULL(@numAddressID,0) = 0
END

GO
/****** Object:  StoredProcedure [dbo].[USP_PayrollLiabilityList]    Script Date: 02/28/2009 13:55:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_payrollliabilitylist')
DROP PROCEDURE usp_payrollliabilitylist
GO
CREATE PROCEDURE [dbo].[USP_PayrollLiabilityList]
(
    @numDomainId          AS NUMERIC(9)  = 0,
    @numDepartmentId      AS NUMERIC(9)  = 0,
    @dtStartDate          AS DATETIME,
    @dtEndDate            AS DATETIME,
    @ClientTimeZoneOffset INT,
	@numOrgUserCntID AS NUMERIC(18,0),
	@tintUserRightType INT = 3,
	@tintUserRightForUpdate INT = 3
)
AS
BEGIN
	SET @dtStartDate = DATEADD (MS , 000 , @dtStartDate)
	SET @dtEndDate = DATEADD (MS , 997 , @dtEndDate)  

	DECLARE @TempCommissionPaidCreditMemoOrRefund TABLE
	(
		numUserCntID NUMERIC(18,0),
		numReturnHeaderID NUMERIC(18,0),
		tintReturnType TINYINT,
		numReturnItemID NUMERIC(18,0),
		monCommission DECIMAL(20,5),
		numComRuleID NUMERIC(18,0),
		tintComType TINYINT,
		tintComBasedOn TINYINT,
		decCommission FLOAT,
		tintAssignTo TINYINT
	)    

	INSERT INTO 
		@TempCommissionPaidCreditMemoOrRefund 
	SELECT 
		numUserCntID,
		numReturnHeaderID,
		tintReturnType,
		numReturnItemID,
		monCommission,
		numComRuleID,
		tintComType,
		tintComBasedOn,
		decCommission,
		tintAssignTo
	FROM 
		dbo.GetCommissionPaidCreditMemoOrRefund(@numDomainId,@ClientTimeZoneOffset,@dtStartDate,@dtEndDate)

	DECLARE @TempCommissionUnPaidCreditMemoOrRefund TABLE
	(
		numUserCntID NUMERIC(18,0),
		numReturnHeaderID NUMERIC(18,0),
		tintReturnType TINYINT,
		numReturnItemID NUMERIC(18,0),
		monCommission DECIMAL(20,5),
		numComRuleID NUMERIC(18,0),
		tintComType TINYINT,
		tintComBasedOn TINYINT,
		decCommission FLOAT,
		tintAssignTo TINYINT
	)    

	INSERT INTO 
		@TempCommissionUnPaidCreditMemoOrRefund 
	SELECT 
		numUserCntID,
		numReturnHeaderID,
		tintReturnType,
		numReturnItemID,
		monCommission,
		numComRuleID,
		tintComType,
		tintComBasedOn,
		decCommission,
		tintAssignTo
	FROM 
		dbo.GetCommissionUnPaidCreditMemoOrRefund(@numDomainId,@ClientTimeZoneOffset,@dtStartDate,@dtEndDate)

	
	-- TEMPORARY TABLE TO HOLD DATA
	CREATE TABLE #tempHrs 
	(
		ID INT IDENTITY(1,1),
		numUserId NUMERIC,
		vcteritorries VARCHAR(1000),
		vcTaxID VARCHAR(100),
		vcUserName VARCHAR(100),
		vcdepartment VARCHAR(100),
		bitLinkVisible BIT,
		bitpayroll BIT,
		numUserCntID NUMERIC,
		vcEmployeeId VARCHAR(50),
		monHourlyRate DECIMAL(20,5),
		decRegularHrs decimal(10,2),
		decPaidLeaveHrs decimal(10,2),
		monExpense DECIMAL(20,5),
		monReimburse DECIMAL(20,5),
		monCommPaidInvoice DECIMAL(20,5),
		monCommPaidInvoiceDeposited DECIMAL(20,5),
		monCommUNPaidInvoice DECIMAL(20,5),
		monCommPaidCreditMemoOrRefund DECIMAL(20,5),
		monCommUnPaidCreditMemoOrRefund DECIMAL(20,5),
		monTotalAmountDue DECIMAL(20,5),
		monAmountPaid DECIMAL(20,5), 
		bitCommissionContact BIT,
		numDivisionID NUMERIC(18,0),
		bitPartner BIT,
		tintCRMType TINYINT
	)

	-- GET EMPLOYEE OF DOMAIN
	INSERT INTO #tempHrs
	(
		numUserId,
		vcteritorries,
		vcTaxID,
		vcUserName,
		vcdepartment,
		bitLinkVisible,
		bitpayroll,
		numUserCntID,
		vcEmployeeId,
		monHourlyRate,
		bitCommissionContact
	)
	SELECT 
		UM.numUserId,
		CONCAT(',',STUFF((SELECT CONCAT(',',numTerritoryID) FROM UserTerritory WHERE numUserCntID=UM.numUserDetailId AND numDomainID=@numDomainId FOR XML PATH('')),1,1,''),','),
		ISNULL(adc.vcTaxID,''),
		Isnull(ADC.vcfirstname + ' ' + adc.vclastname,'-') vcUserName,
		dbo.fn_GetListItemName(adc.vcdepartment),
		(CASE @tintUserRightForUpdate
			WHEN 0 THEN 0
			WHEN 1 THEN CASE WHEN UM.numUserDetailId = @numOrgUserCntID THEN 1 ELSE 0 END
			WHEN 2 THEN CASE WHEN UM.numUserDetailId IN (SELECT numUserCntID FROM UserTerritory WHERE numUserCntID <> @numOrgUserCntID AND numTerritoryID IN (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID = @numOrgUserCntID)) THEN 1 ELSE 0 END
			ELSE 1
		END),
		Isnull(um.bitpayroll,0) AS bitpayroll,
		adc.numcontactid AS numUserCntID,
		Isnull(um.vcEmployeeId,'') AS vcEmployeeId,
		ISNULL(UM.monHourlyRate,0),
		0
	FROM  
		dbo.UserMaster UM 
	JOIN 
		dbo.AdditionalContactsInformation adc 
	ON 
		adc.numDomainID=@numDomainId 
		AND adc.numcontactid = um.numuserdetailid
	WHERE 
		UM.numDomainId=@numDomainId AND (adc.vcdepartment=@numDepartmentId OR @numDepartmentId=0)
		AND 1 = (CASE @tintUserRightType
					WHEN 1 THEN CASE WHEN UM.numUserDetailId = @numOrgUserCntID THEN 1 ELSE 0 END
					WHEN 2 THEN CASE WHEN UM.numUserDetailId IN (SELECT numUserCntID FROM UserTerritory WHERE numUserCntID <> @numOrgUserCntID AND numTerritoryID IN (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID = @numOrgUserCntID)) THEN 1 ELSE 0 END
					ELSE 1
				END)

	-- GET COMMISSION CONTACTS OF DOMAIN
	INSERT INTO #tempHrs 
	(
		numUserId,
		vcTaxID,
		vcUserName,
		numUserCntID,
		vcEmployeeId,
		vcdepartment,
		bitLinkVisible,
		bitpayroll,
		monHourlyRate,
		bitCommissionContact
	)
	SELECT  
		0,
		ISNULL(A.vcTaxID,''),
		ISNULL(A.vcFirstName +' '+ A.vcLastName + '(' + C.vcCompanyName +')','-'),
		A.numContactId,
		'',
		'-',
		(CASE WHEN @tintUserRightForUpdate = 0 OR @tintUserRightForUpdate = 1 OR @tintUserRightForUpdate = 2 THEN 0 ELSE 1 END),
		0,
		0,
		1
	FROM 
		CommissionContacts CC 
	JOIN DivisionMaster D ON CC.numDivisionID=D.numDivisionID
	JOIN AdditionalContactsInformation A ON D.numDivisionID=A.numDivisionID
	JOIN CompanyInfo C ON D.numCompanyID = C.numCompanyId
	WHERE 
		CC.numDomainID=@numDomainID
		AND 1 = (CASE WHEN @tintUserRightType = 1 OR @tintUserRightType = 2 THEN 0 ELSE 1 END)


	-- GET PARTNERS OF DOMAIN
	INSERT INTO #tempHrs 
	(
		numUserId,
		vcTaxID,
		vcUserName,
		numUserCntID,
		vcEmployeeId,
		vcdepartment,
		bitLinkVisible,
		bitpayroll,
		monHourlyRate,
		numDivisionID,
		bitPartner,
		bitCommissionContact,
		tintCRMType
	)
	SELECT  
		0,
		ISNULL(A.vcTaxID,''),
		ISNULL(C.vcCompanyName,'-'),
		0,
		'',
		'-',
		(CASE WHEN @tintUserRightForUpdate = 0 OR @tintUserRightForUpdate = 1 OR @tintUserRightForUpdate = 2 THEN 0 ELSE 1 END),
		0,
		0,
		D.numDivisionID,
		1,
		1,
		tintCRMType
	FROM  
		DivisionMaster AS D 
	LEFT JOIN 
		CompanyInfo AS C
	ON 
		D.numCompanyID=C.numCompanyID
	LEFT JOIN
		AdditionalContactsInformation A
	ON
		A.numDivisionID = D.numDivisionID
		AND ISNULL(A.bitPrimaryContact,1) = 1
	WHERE 
		D.numDomainID=@numDomainID 
		AND D.vcPartnerCode <> ''
		AND 1 = (CASE WHEN @tintUserRightType = 1 OR @tintUserRightType = 2 THEN 0 ELSE 1 END)
	ORDER BY
		vcCompanyName

	-- FETCH ALLREADY PAID COMMISSION DATA AND STORE IN TEMPORARY TABLE
	SELECT * INTO #tempPayrollTracking FROM PayrollTracking WHERE numDomainId=@numDomainId

	Declare @decTotalHrsWorked as decimal(10,2)
	DECLARE @decTotalPaidLeaveHrs AS DECIMAL(10,2)
	DECLARE @monExpense DECIMAL(20,5),@monReimburse DECIMAL(20,5),@monCommPaidInvoice DECIMAL(20,5),@monCommPaidInvoiceDepositedToBank DECIMAL(20,5),@monCommUNPaidInvoice DECIMAL(20,5),@monCommPaidCreditMemoOrRefund DECIMAL(20,5),@monCommUnPaidCreditMemoOrRefund DECIMAL(20,5)

	DECLARE @i INT = 1
	DECLARE @COUNT INT = 0
	DECLARE @numUserCntID NUMERIC(18,0)
	DECLARE @numDivisionID NUMERIC(18,0)
	DECLARE @bitCommissionContact BIT
    
	SELECT @COUNT=COUNT(*) FROM #tempHrs

	-- LOOP OVER EACH EMPLOYEE
	WHILE @i <= @COUNT
	BEGIN
		SELECT @decTotalHrsWorked=0,@decTotalPaidLeaveHrs=0,@monExpense=0,@monReimburse=0,@monCommPaidInvoice=0,@monCommPaidInvoiceDepositedToBank=0,@monCommUNPaidInvoice=0, @numUserCntID=0,@bitCommissionContact=0,@monCommPaidCreditMemoOrRefund=0,@monCommUnPaidCreditMemoOrRefund=0
		
		SELECT
			@numUserCntID = ISNULL(numUserCntID,0),
			@bitCommissionContact = ISNULL(bitCommissionContact,0),
			@numDivisionID=ISNULL(numDivisionID,0)
		FROM
			#tempHrs
		WHERE
			ID = @i
		
		-- EMPLOYEE (ONLY COMMISSION AMOUNT IS NEEDED FOR COMMISSION CONTACTS)
		IF @bitCommissionContact = 0
		BEGIN
			-- CALCULATE REGULAR HRS
			SELECT 
				@decTotalHrsWorked=sum(x.Hrs) 
			FROM 
				(
					SELECT  
						ISNULL(SUM(CAST(DATEDIFF(MINUTE,dtfromdate,dttodate) AS FLOAT)/CAST(60 AS FLOAT)),0) AS Hrs            
					FROM 
						TimeAndExpense 
					WHERE 
						(numType=1 OR numType=2) 
						AND numCategory=1                 
						AND (
								(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtFromDate) BETWEEN @dtStartDate AND @dtEndDate) 
								OR 
								(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtToDate) BETWEEN @dtStartDate AND @dtEndDate)
							) 
						AND numUserCntID=@numUserCntID  
						AND numDomainID=@numDomainId 
						AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE ISNULL(numCategoryHDRID,0)>0)
				) x

			-- CALCULATE PAID LEAVE HRS
			SELECT 
				@decTotalPaidLeaveHrs=ISNULL(
												SUM( 
														CASE 
														WHEN dtfromdate = dttodate 
														THEN 
															24 - (CASE WHEN bitfromfullday = 0 THEN 12 ELSE 0 END)
														ELSE 
															(Datediff(DAY,dtfromdate,dttodate) + 1) * 24 - (CASE WHEN bitfromfullday = 0 THEN 12 ELSE 0 END) - (CASE WHEN bittofullday = 0 THEN 12 ELSE 0 END)
														 END
													)
											 ,0)
            FROM   
				TimeAndExpense
            WHERE  
				numtype = 3 
				AND numcategory = 3 
				AND numusercntid = @numUserCntID 
				AND numdomainid = @numDomainId 
                AND (
						(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtFromDate) BETWEEN @dtStartDate AND @dtEndDate)
						Or 
						(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtToDate) BETWEEN @dtStartDate AND @dtEndDate)
					)
				AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE ISNULL(numCategoryHDRID,0)>0)

			-- CALCULATE EXPENSES
			SELECT 
				@monExpense=ISNULL((SUM(CAST(monAmount AS FLOAT))),0)    
			FROM 
				TimeAndExpense 
			WHERE 
				numCategory=2 
				AND numType in (1,2) 
				AND numUserCntID=@numUserCntID 
				AND numDomainID=@numDomainId 
				AND ((dtFromDate BETWEEN @dtStartDate And @dtEndDate) Or (dtToDate BETWEEN @dtStartDate And @dtEndDate)) 
				AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE ISNULL(numCategoryHDRID,0)>0)

			-- CALCULATE REIMBURSABLE EXPENSES
			SELECT 
				@monReimburse=Isnull((SUM(CAST(monamount AS FLOAT))),0)
            FROM   
				TimeAndExpense 
            WHERE  
				bitreimburse = 1 
				AND numcategory = 2 
                AND numusercntid = @numUserCntID 
				AND numdomainid = @numDomainId
                AND dtfromdate BETWEEN @dtStartDate AND @dtEndDate
                AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE ISNULL(numCategoryHDRID,0)>0)

		END

		-- CALCULATE COMMISSION PAID INVOICE
		SELECT 
			@monCommPaidInvoice=ISNULL(SUM(Amount),0) 
		FROM 
			(
				SELECT 
					oppBiz.numOppID,
					oppBiz.numOppBizDocsID,
					ISNULL(SUM(BC.numComissionAmount - ISNULL(PT.monCommissionAmt,0)),0) AS Amount
				FROM 
					OpportunityMaster Opp 
				INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
				INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId 
				LEFT JOIN #tempPayrollTracking PT ON PT.numComissionID=BC.numComissionID
				CROSS APPLY
				(
					SELECT 
						MAX(DM.dtDepositDate) dtDepositDate
					FROM 
						DepositMaster DM 
					JOIN dbo.DepositeDetails DD	ON DM.numDepositId=DD.numDepositID 
					WHERE 
						DM.tintDepositePage = 2
						AND DM.numDomainId=@numDomainId
						AND DD.numOppID=oppBiz.numOppID 
						AND DD.numOppBizDocsID =oppBiz.numOppBizDocsID
				) TEMPDeposit
				WHERE 
					Opp.numDomainId=@numDomainId 
					AND ((BC.numUserCntId=@numUserCntID AND ISNULL(BC.tintAssignTo,0) <> 3) OR (BC.numUserCntID=@numDivisionID AND BC.tintAssignTo=3))
					AND BC.bitCommisionPaid=0
					AND oppBiz.bitAuthoritativeBizDocs = 1 
					AND isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount 
					AND CAST(TEMPDeposit.dtDepositDate AS DATE) BETWEEN CAST(@dtStartDate AS DATE) AND CAST(@dtEndDate AS DATE)
				GROUP BY 
					oppBiz.numOppID,
					oppBiz.numOppBizDocsID
			) A

		-- CALCULATE COMMISSION PAID INVOICE (DEPOSITED TO BANK)
		SELECT 
			@monCommPaidInvoiceDepositedToBank=ISNULL(SUM(Amount),0) 
		FROM 
			(
				SELECT 
					oppBiz.numOppID,
					oppBiz.numOppBizDocsID,
					ISNULL(SUM(BC.numComissionAmount - ISNULL(PT.monCommissionAmt,0)),0) AS Amount
				FROM 
					OpportunityMaster Opp 
				INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
				INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId 
				LEFT JOIN #tempPayrollTracking PT ON PT.numComissionID=BC.numComissionID
				CROSS APPLY
				(
					SELECT 
						MAX(DM.dtDepositDate) dtDepositDate
					FROM 
						DepositMaster DM 
					JOIN dbo.DepositeDetails DD	ON DM.numDepositId=DD.numDepositID 
					WHERE 
						DM.tintDepositePage = 2
						AND DM.numDomainId=@numDomainId
						AND DD.numOppID=oppBiz.numOppID 
						AND DD.numOppBizDocsID =oppBiz.numOppBizDocsID
						AND DM.tintDepositePage=2 
						AND 1 = (CASE WHEN DM.tintDepositeToType=2 THEN (CASE WHEN ISNULL(DM.bitDepositedToAcnt,0)=1 THEN 1 ELSE 0 END) ELSE 1 END)
				) TEMPDeposit
				WHERE 
					Opp.numDomainId=@numDomainId 
					AND ((BC.numUserCntId=@numUserCntID AND ISNULL(BC.tintAssignTo,0) <> 3) OR (BC.numUserCntID=@numDivisionID AND BC.tintAssignTo=3))
					AND BC.bitCommisionPaid=0
					AND oppBiz.bitAuthoritativeBizDocs = 1 
					AND isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount 
					AND CAST(TEMPDeposit.dtDepositDate AS DATE) BETWEEN CAST(@dtStartDate AS DATE) AND CAST(@dtEndDate AS DATE)
				GROUP BY 
					oppBiz.numOppID,
					oppBiz.numOppBizDocsID
			) A
		

		-- CALCULATE COMMISSION UNPAID INVOICE 
		SELECT 
			@monCommUNPaidInvoice=isnull(sum(BC.numComissionAmount - ISNULL(PT.monCommissionAmt,0)),0) 
		FROM 
			OpportunityMaster Opp 
		INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
        INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId
        LEFT JOIN #tempPayrollTracking PT ON PT.numComissionID=BC.numComissionID 
		WHERE 
			Opp.numDomainId=@numDomainId 
			AND ((BC.numUserCntId=@numUserCntID AND ISNULL(BC.tintAssignTo,0) <> 3) OR (BC.numUserCntID=@numDivisionID AND BC.tintAssignTo=3))
			AND BC.bitCommisionPaid=0
			AND oppBiz.bitAuthoritativeBizDocs = 1 
			AND ISNULL(oppBiz.monAmountPaid,0) <oppBiz.monDealAmount
			AND ((DATEADD(MINUTE,-@ClientTimeZoneOffset,oppBiz.dtFromDate) BETWEEN @dtStartDate AND @dtEndDate))	

		-- CALCULATE COMMISSION FOR PAID CREDIT MEMO/REFUND
		SET @monCommPaidCreditMemoOrRefund = ISNULL((SELECT SUM(monCommission) FROM @TempCommissionPaidCreditMemoOrRefund WHERE ((numUserCntId=@numUserCntID AND ISNULL(tintAssignTo,0) <> 3) OR (numUserCntID=@numDivisionID AND tintAssignTo=3))),0)		
		-- CALCULATE COMMISSION FOR UN-PAID CREDIT MEMO/REFUND
		SET @monCommUnPaidCreditMemoOrRefund = ISNULL((SELECT SUM(monCommission) FROM @TempCommissionUnPaidCreditMemoOrRefund WHERE ((numUserCntId=@numUserCntID AND ISNULL(tintAssignTo,0) <> 3) OR (numUserCntID=@numDivisionID AND tintAssignTo=3))),0)
	
		UPDATE 
			#tempHrs 
		SET    
			decRegularHrs=@decTotalHrsWorked,
			decPaidLeaveHrs=@decTotalPaidLeaveHrs,
			monExpense=@monExpense,
			monReimburse=@monReimburse,
			monCommPaidInvoice=@monCommPaidInvoice,
			monCommPaidInvoiceDeposited=@monCommPaidInvoiceDepositedToBank,
			monCommUNPaidInvoice=@monCommUNPaidInvoice,
			monCommPaidCreditMemoOrRefund=@monCommPaidCreditMemoOrRefund,
			monCommUnPaidCreditMemoOrRefund=@monCommUnPaidCreditMemoOrRefund,
			monTotalAmountDue= @decTotalHrsWorked * monHourlyRate + @monReimburse + @monCommPaidInvoice + @monCommUNPaidInvoice - ISNULL(@monCommPaidCreditMemoOrRefund,0)
        WHERE 
			(numUserCntID=@numUserCntID AND ISNULL(bitPartner,0) = 0) 
			OR (numDivisionID=@numDivisionID AND ISNULL(bitPartner,0) = 1)

		SET @i = @i + 1
	END

	-- CALCULATE PAID AMOUNT
	UPDATE 
		temp 
	SET 
		monAmountPaid=ISNULL(PD.monTotalAmt,0) 
	FROM 
		#tempHrs temp  
	JOIN dbo.PayrollDetail PD ON PD.numUserCntID=temp.numUserCntID
	JOIN dbo.PayrollHeader PH ON PH.numPayrollHeaderID=PD.numPayrollHeaderID
	WHERE 
		ISNULL(PD.numCheckStatus,0)=2  
		AND ((dtFromDate BETWEEN @dtStartDate AND @dtEndDate) OR (dtToDate BETWEEN @dtStartDate And @dtEndDate))
 
	SELECT * FROM #tempHrs
	DROP TABLE #tempPayrollTracking
	DROP TABLE #tempHrs
END
/****** Object:  StoredProcedure [dbo].[USP_RevertDetailsOpp]    Script Date: 07/26/2008 16:20:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
/* deducts Qty from allocation and Adds back to onhand */
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_RevertDetailsOpp')
DROP PROCEDURE USP_RevertDetailsOpp
GO
CREATE PROCEDURE [dbo].[USP_RevertDetailsOpp] 
@numOppId NUMERIC,
@tintMode AS TINYINT=0, -- 0:Add/Edit, 1:Delete, 2: Demoted to opportunity
@numUserCntID AS NUMERIC(9)
AS ---reverting back to previous state if deal is being edited 
	DECLARE @numDomain AS NUMERIC(18,0)
	SELECT @numDomain = numDomainID FROM [dbo].[OpportunityMaster] AS OM WHERE [OM].[numOppId] = @numOppID
    
	DECLARE @OppType AS VARCHAR(2)              
    DECLARE @itemcode AS NUMERIC       
    DECLARE @numWareHouseItemID AS NUMERIC
	DECLARE @numWLocationID NUMERIC(18,0)                     
    DECLARE @numToWarehouseItemID AS NUMERIC 
    DECLARE @numUnits AS FLOAT                                              
    DECLARE @onHand AS FLOAT                                            
    DECLARE @onOrder AS FLOAT                                            
    DECLARE @onBackOrder AS FLOAT                                              
    DECLARE @onAllocation AS FLOAT
    DECLARE @numQtyShipped AS FLOAT
    DECLARE @numUnitHourReceived AS FLOAT
	DECLARE @numDeletedReceievedQty AS FLOAT
    DECLARE @numoppitemtCode AS NUMERIC(9) 
    DECLARE @monAmount AS DECIMAL(20,5) 
    DECLARE @monAvgCost AS DECIMAL(20,5)   
    DECLARE @Kit AS BIT                                        
    DECLARE @bitKitParent BIT
    DECLARE @bitStockTransfer BIT 
    DECLARE @numOrigUnits AS FLOAT			
    DECLARE @description AS VARCHAR(100)
	DECLARE @bitWorkOrder AS BIT
	--Added by :Sachin Sadhu||Date:18thSept2014
	--For Rental/Asset Project
	Declare @numRentalIN as FLOAT
	Declare @numRentalOut as FLOAT
	Declare @numRentalLost as FLOAT
	DECLARE @bitAsset as BIT
	--end sachin

    						
    SELECT TOP 1
            @numoppitemtCode = numoppitemtCode,
            @itemcode = OI.numItemCode,
            @numUnits = ISNULL(numUnitHour,0),
            @numWareHouseItemID = ISNULL(numWarehouseItmsID,0),
			@numWLocationID = ISNULL(numWLocationID,0),
            @Kit = ( CASE WHEN bitKitParent = 1
                               AND bitAssembly = 1 THEN 0
                          WHEN bitKitParent = 1 THEN 1
                          ELSE 0
                     END ),
            @monAmount = ISNULL(monTotAmount,0) * (CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END),
            @monAvgCost = ISNULL(monAverageCost,0),
            @numQtyShipped = ISNULL(numQtyShipped,0),
            @numUnitHourReceived = ISNULL(numUnitHourReceived,0),
			@numDeletedReceievedQty = ISNULL(numDeletedReceievedQty,0),
            @bitKitParent=ISNULL(bitKitParent,0),
            @numToWarehouseItemID =OI.numToWarehouseItemID,
            @bitStockTransfer = ISNULL(OM.bitStockTransfer,0),
            @OppType = tintOppType,
		    @numRentalIN=ISNULL(oi.numRentalIN,0),
			@numRentalOut=Isnull(oi.numRentalOut,0),
			@numRentalLost=Isnull(oi.numRentalLost,0),
			@bitAsset =ISNULL(I.bitAsset,0),
			@bitWorkOrder = ISNULL(OI.bitWorkOrder,0)
    FROM    OpportunityItems OI
			LEFT JOIN WareHouseItems WI ON OI.numWarehouseItmsID=WI.numWareHouseItemID
			JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId
            JOIN Item I ON OI.numItemCode = I.numItemCode
    WHERE   (charitemtype='P' OR 1=(CASE WHEN tintOppType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END)) AND OI.numOppId = @numOppId
                           AND ( bitDropShip = 0
                                 OR bitDropShip IS NULL
                               ) 
    ORDER BY OI.numoppitemtCode

	
    WHILE @numoppitemtCode > 0                                  
    BEGIN    
        SET @numOrigUnits=@numUnits
            
        IF @bitStockTransfer=1
        BEGIN
			SET @OppType = 1
		END
                 
        IF @numWareHouseItemID>0
        BEGIN                                
			SELECT  @onHand = ISNULL(numOnHand, 0),
					@onAllocation = ISNULL(numAllocation, 0),
					@onOrder = ISNULL(numOnOrder, 0),
					@onBackOrder = ISNULL(numBackOrder, 0)
			FROM    WareHouseItems
			WHERE   numWareHouseItemID = @numWareHouseItemID                                               
        END
            
        IF @OppType = 1 
        BEGIN
			IF @tintMode = 2
			BEGIN
				SET @description='SO DEMOTED TO OPPORTUNITY (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'
			END
			ELSE
			BEGIN
				SET @description='SO Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'
			END
                
            IF @Kit = 1
			BEGIN
				exec USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,@numOrigUnits,1,@numOppID,@tintMode,@numUserCntID
			END
			ELSE IF @bitWorkOrder = 1
			BEGIN
				DECLARE @numWOID AS NUMERIC(18,0)
				DECLARE @numWOStatus AS NUMERIC(18,0)
				SELECT @numWOID=numWOId,@numWOStatus=numWOStatus FROM WorkOrder WHERE numOppId=@numOppId AND numOppItemID=@numoppitemtCode AND numItemCode=@itemcode AND numWareHouseItemId=@numWareHouseItemID

				IF  @tintMode=2
				BEGIN
					SET @description='SO-WO Deal Lost (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'
				END
				ELSE
				BEGIN
					SET @description='SO-WO Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'
				END


				IF @numQtyShipped>0
				BEGIN
					IF @tintMode=0
						SET @numUnits = @numUnits - @numQtyShipped
					ELSE IF @tintmode=1 OR @tintMode=2
						SET @onAllocation = @onAllocation + @numQtyShipped 
				END 


				--IF WORK ORDER IS NOT COMPLETED THAN REMOVE ON ORDER QUANTITY
				IF @numWOStatus <> 23184
				BEGIN
					IF @onOrder >= @numUnits
						SET @onOrder = @onOrder - @numUnits
					ELSE
						SET @onOrder = 0
				END

				-- IF BACKORDER QTY IS GREATER THEN QTY TO REVERT THEN
				-- DECREASE BACKORDER QTY BY QTY TO REVERT
				IF @numUnits < @onBackOrder 
				BEGIN                  
					SET @onBackOrder = @onBackOrder - @numUnits
				END 
				-- IF BACKORDER QTY IS LESS THEN OR EQUAL TO QTY TO REVERT THEN
				-- DECREASE QTY TO REVERT BY QTY OF BACK ORDER AND
				-- SET BACKORDER QTY TO 0
				ELSE IF @numUnits >= @onBackOrder 
				BEGIN
					SET @numUnits = @numUnits - @onBackOrder
					SET @onBackOrder = 0
                        
					--REMOVE ITEM FROM ALLOCATION 
					IF (@onAllocation - @numUnits) >= 0
						SET @onAllocation = @onAllocation - @numUnits
						
					--ADD QTY TO ONHAND
					SET @onHand = @onHand + @numUnits
				END

				UPDATE  
					WareHouseItems
				SET 
					numOnHand = @onHand ,
					numAllocation = @onAllocation,
					numBackOrder = @onBackOrder,
					numOnOrder = @onOrder,
					dtModified = GETDATE()
				WHERE 
					numWareHouseItemID = @numWareHouseItemID   

				--IF WORK ORDER IS NOT COMPLETED THAN DELETE WORK ORDER AND ITS CHILDS WORK ORDER
				IF @numWOStatus <> 23184
				BEGIN
					EXEC USP_DeleteAssemblyWOAndInventory  @numDomain,@numUserCntID,@numWOID,@numOppID
				END
			END	
			ELSE
			BEGIN	
				IF @numQtyShipped>0
				BEGIN
					IF @tintMode=0
						SET @numUnits = @numUnits - @numQtyShipped
					ELSE IF @tintmode=1 OR @tintMode=2
						SET @onAllocation = @onAllocation + @numQtyShipped 
				END 
								                    
                IF @numUnits >= @onBackOrder 
                BEGIN
                    SET @numUnits = @numUnits - @onBackOrder
                    SET @onBackOrder = 0
                            
                    IF (@onAllocation - @numUnits >= 0)
						SET @onAllocation = @onAllocation - @numUnits

					IF @bitAsset=1--Not Asset
					BEGIN
						SET @onHand = @onHand +@numRentalIN+@numRentalLost+@numRentalOut     
					END
					ELSE
					BEGIN
						SET @onHand = @onHand + @numUnits     
					END                                         
                END                                            
                ELSE IF @numUnits < @onBackOrder 
                BEGIN                  
					IF (@onBackOrder - @numUnits >0)
						SET @onBackOrder = @onBackOrder - @numUnits
                END 
                 	
				UPDATE  
					WareHouseItems
				SET 
					numOnHand = @onHand ,
					numAllocation = @onAllocation,
					numBackOrder = @onBackOrder,
					dtModified = GETDATE()
				WHERE 
					numWareHouseItemID = @numWareHouseItemID              
			END
				

			IF @numWareHouseItemID>0 AND @description <> 'DO NOT ADD WAREHOUSE TRACKING'
			BEGIN 
					EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numOppId, --  numeric(9, 0)
					@tintRefType = 3, --  tinyint
					@vcDescription = @description, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@numDomainID = @numDomain 
			END		
		END      
          
        IF @bitStockTransfer=1
        BEGIN
			SET @numWareHouseItemID = @numToWarehouseItemID;
			SET @OppType = 2
			SET @numUnits = @numOrigUnits

			SELECT
				@onHand = ISNULL(numOnHand, 0),
                @onAllocation = ISNULL(numAllocation, 0),
                @onOrder = ISNULL(numOnOrder, 0),
                @onBackOrder = ISNULL(numBackOrder, 0)
			FROM 
				WareHouseItems
			WHERE
				numWareHouseItemID = @numWareHouseItemID   
		END
          
        IF @OppType = 2 
        BEGIN 
			IF @tintMode = 2
			BEGIN
				SET @description='PO DEMOTED TO OPPORTUNITY (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@numUnitHourReceived AS VARCHAR(10)) + ' Deleted:' + CAST(@numDeletedReceievedQty AS VARCHAR(10)) + ')'
			END
			ELSE
			BEGIN
				SET @description='PO Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@numUnitHourReceived AS VARCHAR(10)) + ' Deleted:' + CAST(@numDeletedReceievedQty AS VARCHAR(10)) + ')'
			END

			IF @numWLocationID = -1 AND ISNULL(@bitStockTransfer,0)=0
			BEGIN
				IF @tintmode = 1 OR @tintMode=2
				BEGIN
					DECLARE @TEMPReceievedItems TABLE
					(
						ID INT,
						numOIRLID NUMERIC(18,0),
						numWarehouseItemID NUMERIC(18,0),
						numUnitReceieved FLOAT,
						numDeletedReceievedQty FLOAT
					)

					DELETE FROM @TEMPReceievedItems

					INSERT INTO
						@TEMPReceievedItems
					SELECT
						ROW_NUMBER() OVER(ORDER BY ID),
						ID,
						numWarehouseItemID,
						numUnitReceieved,
						ISNULL(numDeletedReceievedQty,0)
					FROM
						OpportunityItemsReceievedLocation 
					WHERE
						numDomainID=@numDomain
						AND numOppID=@numOppId
						AND numOppItemID=@numoppitemtCode

					DECLARE @i AS INT = 1
					DECLARE @COUNT AS INT
					DECLARE @numTempOIRLID NUMERIC(18,0)
					DECLARE @numTempOnHand FLOAT
					DECLARE @numTempWarehouseItemID NUMERIC(18,0)
					DECLARE @numTempUnitReceieved FLOAT
					DECLARE @numTempDeletedReceievedQty FLOAT

					SELECT @COUNT=COUNT(*) FROM @TEMPReceievedItems

					WHILE @i <= @COUNT
					BEGIN
						SELECT 
							@numTempOIRLID=TRI.numOIRLID,
							@numTempOnHand= ISNULL(numOnHand,0),
							@numTempWarehouseItemID=ISNULL(TRI.numWarehouseItemID,0),
							@numTempUnitReceieved=ISNULL(numUnitReceieved,0),
							@numTempDeletedReceievedQty=ISNULL(numDeletedReceievedQty,0)
						FROM 
							@TEMPReceievedItems TRI
						INNER JOIN
							WareHouseItems WI
						ON
							TRI.numWarehouseItemID=WI.numWareHouseItemID
						WHERE 
							ID=@i

						IF @numTempOnHand >= (@numTempUnitReceieved - @numTempDeletedReceievedQty)
						BEGIN
							UPDATE
								WareHouseItems
							SET
								numOnHand = ISNULL(numOnHand,0) - (@numTempUnitReceieved - @numTempDeletedReceievedQty),
								dtModified = GETDATE()
							WHERE
								numWareHouseItemID=@numTempWarehouseItemID
						END
						ELSE
						BEGIN
							RAISERROR('INSUFFICIENT_ONHAND_QTY',16,1)
							RETURN
						END

						IF @tintMode = 2
						BEGIN
							SET @description='PO DEMOTED TO OPPORTUNITY (Total Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Deleted Qty: ' + CAST((@numTempUnitReceieved - @numTempDeletedReceievedQty) AS VARCHAR(10)) + ' Received:' +  CAST(@numUnitHourReceived AS VARCHAR(10)) + ')'
						END
						ELSE
						BEGIN
							SET @description='PO Deleted (Total Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Deleted Qty: ' + CAST((@numTempUnitReceieved - @numTempDeletedReceievedQty) AS VARCHAR(10)) + ' Received:' +  CAST(@numUnitHourReceived AS VARCHAR(10)) + ')'
						END

						EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numTempWarehouseItemID,
							@numReferenceID = @numOppId,
							@tintRefType = 4,
							@vcDescription = @description,
							@numModifiedBy = @numUserCntID,
							@numDomainID = @numDomain

						DELETE FROM OpportunityItemsReceievedLocation WHERE ID=@numTempOIRLID

						SET @i = @i + 1
					END
				END

				SET @numUnits = @numUnits - @numUnitHourReceived

				IF (@onOrder - @numUnits)>=0
				BEGIN
					UPDATE
						WareHouseItems
					SET 
						numOnOrder = ISNULL(numOnOrder,0) - @numUnits,
						dtModified = GETDATE()
					WHERE 
						numWareHouseItemID = @numWareHouseItemID

					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID,
						@numReferenceID = @numOppId,
						@tintRefType = 4,
						@vcDescription = @description,
						@numModifiedBy = @numUserCntID,
						@numDomainID = @numDomain
				END
				ELSE
				BEGIN
					RAISERROR('INSUFFICIENT_ONORDER_QTY',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				--Partial Fulfillment
				IF (@tintmode=1 OR @tintMode=2) and  @onHand >= (@numUnitHourReceived - @numDeletedReceievedQty)
				BEGIN
					SET @onHand= @onHand - (@numUnitHourReceived - @numDeletedReceievedQty)
				END
						
				SET @numUnits = @numUnits - @numUnitHourReceived

				IF (@onOrder - @numUnits)>=0
				BEGIN
					--Causing Negative Inventory Bug ID:494
					SET @onOrder = @onOrder - @numUnits	
				END
				ELSE IF (@onHand + @onOrder) - @numUnits >= 0
				BEGIN						
					SET @onHand = @onHand - (@numUnits-@onOrder)
					SET @onOrder = 0
				END
				ELSE IF  (@onHand + @onOrder + @onAllocation) - @numUnits >= 0
				BEGIN
					Declare @numDiff numeric
	
					SET @numDiff = @numUnits - @onOrder
					SET @onOrder = 0

					SET @numDiff = @numDiff - @onHand
					SET @onHand = 0

					SET @onAllocation = @onAllocation - @numDiff
					SET @onBackOrder = @onBackOrder + @numDiff
				END
					    
				UPDATE
					WareHouseItems
				SET 
					numOnHand = @onHand,
					numAllocation = @onAllocation,
					numBackOrder = @onBackOrder,
					numOnOrder = @onOrder,
					dtModified = GETDATE()
				WHERE 
					numWareHouseItemID = @numWareHouseItemID   
                        
						

				EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numOppId, --  numeric(9, 0)
				@tintRefType = 4, --  tinyint
				@vcDescription = @description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomain
			END                                        
        END   
                                                                
        SELECT TOP 1
                @numoppitemtCode = numoppitemtCode,
                @itemcode = OI.numItemCode,
                @numUnits = numUnitHour,
                @numWareHouseItemID = ISNULL(numWarehouseItmsID,0),
				@numWLocationID = ISNULL(numWLocationID,0),
                @Kit = ( CASE WHEN bitKitParent = 1
                                    AND bitAssembly = 1 THEN 0
                                WHEN bitKitParent = 1 THEN 1
                                ELSE 0
                            END ),
                @monAmount = ISNULL(monTotAmount,0)  * (CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END),
                @monAvgCost = monAverageCost,
                @numQtyShipped = ISNULL(numQtyShipped,0),
				@numUnitHourReceived = ISNULL(numUnitHourReceived,0),
				@numDeletedReceievedQty = ISNULL(numDeletedReceievedQty,0),
				@bitKitParent=ISNULL(bitKitParent,0),
				@numToWarehouseItemID =OI.numToWarehouseItemID,
				@bitStockTransfer = ISNULL(OM.bitStockTransfer,0),
				@OppType = tintOppType,
				@numRentalIN=ISNULL(oi.numRentalIN,0),
				@numRentalOut=Isnull(oi.numRentalOut,0),
				@numRentalLost=Isnull(oi.numRentalLost,0),
				@bitAsset =ISNULL(I.bitAsset,0),
				@bitWorkOrder = ISNULL(OI.bitWorkOrder,0)
        FROM    OpportunityItems OI
				LEFT JOIN WareHouseItems WI ON OI.numWarehouseItmsID=WI.numWareHouseItemID
				JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId
                JOIN Item I ON OI.numItemCode = I.numItemCode
                                   
        WHERE   (charitemtype='P' OR 1=(CASE WHEN tintOppType=1 THEN 
						CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								ELSE 0 END 
						ELSE 0 END))  
						AND OI.numOppId = @numOppId 
                AND OI.numoppitemtCode > @numoppitemtCode
                AND ( bitDropShip = 0
                        OR bitDropShip IS NULL
                    )
        ORDER BY OI.numoppitemtCode                                              
        IF @@rowcount = 0 
            SET @numoppitemtCode = 0      
    END


GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_UpdateInventoryAdjustments' ) 
    DROP PROCEDURE USP_UpdateInventoryAdjustments
GO
CREATE PROCEDURE USP_UpdateInventoryAdjustments
    @numDomainId NUMERIC,
    @numUserCntID NUMERIC,
    @strItems VARCHAR(8000),
    @dtAdjustmentDate AS DATETIME
AS 
BEGIN
	-- TRANSACTION IS USED IN CODE
	DECLARE @hDoc INT    
	EXEC sp_xml_preparedocument @hDoc OUTPUT, @strItems    
  
	SELECT 
		* 
	INTO 
		#temp 
	FROM 
		OPENXML (@hDoc,'/NewDataSet/Item',2)    
	WITH 
	( 
		numWareHouseItemID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,intAdjust FLOAT
	) 
		
	SELECT 
		*
		,ROW_NUMBER() OVER(ORDER BY numItemCode) AS RowNo 
	INTO 
		#tempSerialLotNO 
	FROM 
		OPENXML (@hDoc,'/NewDataSet/SerialLotNo',2)    
	WITH 
		(
			numItemCode NUMERIC(18,0)
			,numWareHouseID NUMERIC(18,0)
			,numWareHouseItemID NUMERIC(18,0)
			,vcSerialNo VARCHAR(3000)
			,byteMode TINYINT
		) 
		
	EXEC sp_xml_removedocument @hDoc  

    DECLARE @numOnHand FLOAT
	DECLARE @numAllocation FLOAT
	DECLARE @numBackOrder FLOAT
	DECLARE @numWareHouseItemID AS NUMERIC(18,0)
	DECLARE @numItemCode NUMERIC(18,0)
			
	SELECT 
		@numWareHouseItemID = MIN(numWareHouseItemID) 
	FROM 
		#temp
			
	DECLARE @bitLotNo AS BIT = 0  
	DECLARE @bitSerialized AS BIT = 0  

	WHILE @numWareHouseItemID>0    
	BEGIN		  
		SELECT 
			@bitLotNo=ISNULL(Item.bitLotNo,0)
			,@bitSerialized=ISNULL(Item.bitSerialized,0)
		FROM 
			Item 
		INNER JOIN 
			#temp 
		ON 
			item.numItemCode=#temp.numItemCode 
		WHERE 
			#temp.numWareHouseItemID=@numWareHouseItemID 
			AND numDomainID=@numDomainID

		IF @bitLotNo=0 AND @bitSerialized=0
		BEGIN		
			


			IF (SELECT 
					ISNULL(numOnHand,0) + ISNULL(numAllocation,0) + #temp.intAdjust 
				FROM 
					WareHouseItems WI 
				INNER JOIN 
					#temp 
				ON 
					WI.numWareHouseItemID = #temp.numWareHouseItemID 
				WHERE 
					WI.numWareHouseItemID = @numWareHouseItemID AND WI.numDomainID=@numDomainId) >= 0
			BEGIN
				IF (SELECT intAdjust FROM #temp WHERE numWareHouseItemID = @numWareHouseItemID) >= 0
				BEGIN
					UPDATE 
						WI 
					SET 
						numOnHand = ISNULL(numOnHand,0) + (ISNULL(numAllocation,0) + (CASE WHEN ISNULL(numBackOrder,0) >= #temp.intAdjust THEN  0 ELSE (#temp.intAdjust - ISNULL(numBackOrder,0)) END))
						,numAllocation = ISNULL(numAllocation,0) + (CASE WHEN ISNULL(numBackOrder,0) >= #temp.intAdjust THEN  #temp.intAdjust ELSE ISNULL(numBackOrder,0) END) 
						,numBackOrder = (CASE WHEN ISNULL(numBackOrder,0) >= #temp.intAdjust THEN (ISNULL(numBackOrder,0) - #temp.intAdjust) ELSE 0 END)
						,dtModified=GETDATE() 
					FROM 
						dbo.WareHouseItems WI 
					INNER JOIN 
						#temp 
					ON 
						WI.numWareHouseItemID = #temp.numWareHouseItemID
					WHERE 
						WI.numWareHouseItemID = @numWareHouseItemID 
						AND WI.numDomainID=@numDomainId
				END
				ELSE
				BEGIN
					UPDATE 
						WI 
					SET 
						numOnHand = (CASE WHEN ISNULL(numOnHand,0) + #temp.intAdjust >= 0 THEN ISNULL(numOnHand,0) + #temp.intAdjust ELSE 0 END)
						,numAllocation = (CASE WHEN ISNULL(numOnHand,0) + #temp.intAdjust < 0 THEN (CASE WHEN ISNULL(numAllocation,0) + (ISNULL(numOnHand,0) + #temp.intAdjust) >= 0 THEN ISNULL(numAllocation,0) + (ISNULL(numOnHand,0) + #temp.intAdjust) ELSE 0 END) ELSE ISNULL(numAllocation,0) END)
						,numBackOrder = (CASE WHEN ISNULL(numOnHand,0) + #temp.intAdjust < 0 THEN ISNULL(numBackOrder,0) - (CASE WHEN ISNULL(numAllocation,0) + (ISNULL(numOnHand,0) + #temp.intAdjust) >= 0 THEN ISNULL(numOnHand,0) + #temp.intAdjust ELSE ISNULL(numAllocation,0) + (ISNULL(numOnHand,0) + #temp.intAdjust) END) ELSE ISNULL(numBackOrder,0) END)
						,dtModified=GETDATE() 
					FROM 
						dbo.WareHouseItems WI 
					INNER JOIN 
						#temp 
					ON 
						WI.numWareHouseItemID = #temp.numWareHouseItemID
					WHERE 
						WI.numWareHouseItemID = @numWareHouseItemID 
						AND WI.numDomainID=@numDomainId
				END	  
				SELECT @numItemCode=numItemID FROM dbo.WareHouseItems WHERE numWareHouseItemID = @numWareHouseItemID AND numDomainID=@numDomainId
								
				EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numItemCode, --  numeric(9, 0)
				@tintRefType = 1, --  tinyint
				@vcDescription = 'Inventory Adjustment', --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@dtRecordDate = @dtAdjustmentDate,
				@numDomainId = @numDomainID
			END
			ELSE
			BEGIN
				RAISERROR('INVALID_INVENTORY_ADJUSTMENT_NUMBER',16,1)
			END
		END	  
				    
		SELECT TOP 1 
			@numWareHouseItemID = numWareHouseItemID 
		FROM 
			#temp 
		WHERE 
			numWareHouseItemID >@numWareHouseItemID 
		ORDER BY 
			numWareHouseItemID  
				    
		IF @@rowcount=0 
			SET @numWareHouseItemID =0    
	END    
    

    DROP TABLE #temp
    
    
    -------------Serial/Lot #s----------------
	DECLARE @minRowNo NUMERIC(18),@maxRowNo NUMERIC(18)
	SELECT @minRowNo = MIN(RowNo),@maxRowNo = Max(RowNo) FROM #tempSerialLotNO
		    
	DECLARE @numWareHouseID NUMERIC(9),@vcSerialNo VARCHAR(3000),@numWareHouseItmsDTLID AS NUMERIC(9),
			@vcComments VARCHAR(1000),@OldQty FLOAT,@numQty FLOAT,@byteMode TINYINT 		    
	DECLARE @posComma int, @strKeyVal varchar(20)
		    
	WHILE @minRowNo <= @maxRowNo
	BEGIN
		SELECT @numItemCode=numItemCode,@numWareHouseID=numWareHouseID,@numWareHouseItemID=numWareHouseItemID,
			@vcSerialNo=vcSerialNo,@byteMode=byteMode FROM #tempSerialLotNO WHERE RowNo=@minRowNo
		
		SET @posComma=0
		
		SET @vcSerialNo=RTRIM(@vcSerialNo)
		IF RIGHT(@vcSerialNo, 1)!=',' SET @vcSerialNo=@vcSerialNo+','

		SET @posComma=PatIndex('%,%', @vcSerialNo)
		WHILE @posComma>1
			BEGIN
				SET @strKeyVal=ltrim(rtrim(substring(@vcSerialNo, 1, @posComma-1)))
	
				DECLARE @posBStart INT,@posBEnd int, @strQty varchar(20),@strName varchar(20)

				SET @posBStart=PatIndex('%(%', @strKeyVal)
				SET @posBEnd=PatIndex('%)%', @strKeyVal)
				IF( @posBStart>1 AND @posBEnd>1)
				BEGIN
					SET @strName=ltrim(rtrim(substring(@strKeyVal, 1, @posBStart-1)))
					SET	@strQty=ltrim(rtrim(substring(@strKeyVal, @posBStart+1,len(@strKeyVal)-@posBStart-1)))
				END		
				ELSE
				BEGIN
					SET @strName=@strKeyVal
					SET	@strQty=1
				END
	  
			SET @numWareHouseItmsDTLID=0
			SET @OldQty=0
			SET @vcComments=''
			
			select top 1 @numWareHouseItmsDTLID=ISNULL(numWareHouseItmsDTLID,0),@OldQty=ISNULL(numQty,0),@vcComments=ISNULL(vcComments,'') 
					from WareHouseItmsDTL where numWareHouseItemID=@numWareHouseItemID and
					vcSerialNo=@strName AND ISNULL(numQty,0) > 0
			
			IF @byteMode=0 --Add
				SET @numQty=@OldQty + @strQty
			ELSE --Deduct
				SET @numQty=@OldQty - @strQty
			
			EXEC dbo.USP_AddUpdateWareHouseForItems
					@numItemCode = @numItemCode, --  numeric(9, 0)
					@numWareHouseID = @numWareHouseID, --  numeric(9, 0)
					@numWareHouseItemID =@numWareHouseItemID,
					@numDomainID = @numDomainId, --  numeric(9, 0)
					@vcSerialNo = @strName, --  varchar(100)
					@vcComments = @vcComments, -- varchar(1000)
					@numQty = @numQty, --  numeric(18, 0)
					@byteMode = 5, --  tinyint
					@numWareHouseItmsDTLID = @numWareHouseItmsDTLID, --  numeric(18, 0)
					@numUserCntID = @numUserCntID, --  numeric(9, 0)
					@dtAdjustmentDate = @dtAdjustmentDate
	  
			SET @vcSerialNo=substring(@vcSerialNo, @posComma +1, len(@vcSerialNo)-@posComma)
			SET @posComma=PatIndex('%,%',@vcSerialNo)
		END

		SET @minRowNo=@minRowNo+1
	END
	
	DROP TABLE #tempSerialLotNO	   
END



GO
/****** Object:  StoredProcedure [dbo].[USP_UpdateOppAddress]    Script Date: 05/31/2009 15:44:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updateoppaddress')
DROP PROCEDURE usp_updateoppaddress
GO
CREATE PROCEDURE [dbo].[USP_UpdateOppAddress]      
@numOppID as numeric(9)=0,      
@byteMode as tinyint,      
@vcStreet as varchar(100),      
@vcCity as varchar(50),      
@vcPostalCode as varchar(15),      
@numState as numeric(9),      
@numCountry as numeric(9),  
@vcCompanyName as varchar(100), 
@numCompanyId as numeric(9), -- This in Parameter is added by Ajit Singh on 01/10/2008,
@vcAddressName as varchar(50)='',
@numReturnHeaderID as numeric(9)=0,
@bitCalledFromProcedure AS BIT = 0, -- Avoid Tax calculation when this procedure is called from another procedure because all calling procedure have tax calulation uptill now
@numContact NUMERIC(18,0) = 0,
@bitAltContact BIT = 0,
@vcAltContact VARCHAR(200) = '',
@numUserCntID NUMERIC(18,0) = 0
as      
	declare @numDivisionID as numeric(9)   -- This local variable is added by Ajit Singh on 01/10/2008
	
	SET @numReturnHeaderID = NULLIF(@numReturnHeaderID,0)
	SET @numOppID = NULLIF(@numOppID,0)
	
	DECLARE @numDomainID AS NUMERIC(9)
	SET @numDomainID = 0
	
	IF @byteMode=0      
	BEGIN
		IF ISNULL(@numOppID,0) >0
		BEGIN
			 update OpportunityMaster set tintBillToType=2 where numOppID=@numOppID    
	 
			IF NOT EXISTS(SELECT * FROM OpportunityAddress WHERE numOppID=@numOppID)    
			BEGIN
				INSERT INTO OpportunityAddress
				(
					numOppID
					,vcBillCompanyName
					,vcBillStreet
					,vcBillCity
					,numBillState
					,vcBillPostCode
					,numBillCountry
					,vcAddressName
					,numReturnHeaderID
					,numBillCompanyID
					,numBillingContact
					,bitAltBillingContact
					,vcAltBillingContact
				)    
				VALUES
				(
					@numOppID
					,@vcCompanyName
					,@vcStreet
					,@vcCity
					,@numState
					,@vcPostalCode
					,@numCountry
					,@vcAddressName
					,@numReturnHeaderID
					,@numCompanyID
					,@numContact
					,@bitAltContact
					,@vcAltContact
				)    
			END
			ELSE
			BEGIN
				 UPDATE 
					OpportunityAddress 
				SET  
					vcBillCompanyName=@vcCompanyName,       
					vcBillStreet=@vcStreet,      
					vcBillCity=@vcCity,      
					numBillState=@numState,      
					vcBillPostCode=@vcPostalCode,      
					numBillCountry=@numCountry,vcAddressName=@vcAddressName,
					numBillCompanyID=@numCompanyId      
					,numBillingContact=@numContact
					,bitAltBillingContact=@bitAltContact
					,vcAltBillingContact=@vcAltContact
				 WHERE 
					numOppID=@numOppID 
			END

			IF ISNULL(@numUserCntID,0) > 0
			BEGIN
				IF NOT EXISTS (SELECT ID FROM OrgOppAddressModificationHistory WHERE numRecordID=@numOppID AND tintAddressOf=2 AND tintAddressType=1)
				BEGIN
					INSERT INTO OrgOppAddressModificationHistory
					(
						tintAddressOf
						,tintAddressType
						,numRecordID
						,numModifiedBy
						,dtModifiedDate
					)
					VALUES
					(
						2
						,1
						,@numOppID
						,@numUserCntID
						,GETUTCDATE()
					)
				END
				ELSE
				BEGIN
					UPDATE
						OrgOppAddressModificationHistory
					SET
						numModifiedBy=@numUserCntID
						,dtModifiedDate=GETUTCDATE()
					WHERE
						numRecordID=@numOppID 
						AND tintAddressOf=2 
						AND tintAddressType=1
				END
			END
		END	 
		ELSE IF ISNULL(@numReturnHeaderID,0) >0
		BEGIN
			IF NOT EXISTS(SELECT * FROM OpportunityAddress WHERE numReturnHeaderID=@numReturnHeaderID)    
			BEGIN
				INSERT INTO OpportunityAddress
				(
					numOppID
					,vcBillCompanyName
					,vcBillStreet
					,vcBillCity
					,numBillState
					,vcBillPostCode
					,numBillCountry
					,vcAddressName
					,numReturnHeaderID
					,numBillCompanyID
					,numBillingContact
					,bitAltBillingContact
					,vcAltBillingContact
				)    
				VALUES
				(
					@numOppID
					,@vcCompanyName
					,@vcStreet
					,@vcCity
					,@numState
					,@vcPostalCode
					,@numCountry
					,@vcAddressName
					,@numReturnHeaderID
					,@numCompanyID
					,@numContact
					,@bitAltContact
					,@vcAltContact
				)    
			END
			ELSE
			BEGIN
 				UPDATE 
					OpportunityAddress 
				SET  
					vcBillCompanyName=@vcCompanyName     
					,vcBillStreet=@vcStreet  
					,vcBillCity=@vcCity
					,numBillState=@numState    
					,vcBillPostCode=@vcPostalCode
					,numBillCountry=@numCountry
					,vcAddressName=@vcAddressName
					,numBillCompanyID=@numCompanyId
					,numBillingContact=@numContact
					,bitAltBillingContact=@bitAltContact
					,vcAltBillingContact=@vcAltContact
				WHERE 
					numReturnHeaderID = @numReturnHeaderID 
			END
		END
	END      
	ELSE IF @byteMode=1
	BEGIN
		IF ISNULL(@numOppID,0) >0
		BEGIN
			IF @numCompanyId >0 
			BEGIN
				UPDATE OpportunityMaster set tintShipToType=2 where numOppID=@numOppID     
			END 
		
			IF NOT EXISTS(SELECT * FROM OpportunityAddress where numOppID=@numOppID)
			BEGIN
				INSERT INTO OpportunityAddress
				(
					numOppID
					,vcShipCompanyName
					,vcShipStreet
					,vcShipCity
					,numShipState
					,vcShipPostCode
					,numShipCountry
					,vcAddressName
					,numShipCompanyID
					,numShippingContact
					,bitAltShippingContact
					,vcAltShippingContact
				)    
				VALUES
				(
					@numOppID
					,@vcCompanyName
					,@vcStreet
					,@vcCity
					,@numState
					,@vcPostalCode
					,@numCountry
					,@vcAddressName
					,@numCompanyID
					,@numContact
					,@bitAltContact
					,@vcAltContact
				)    
			END
			ELSE 
			BEGIN
				UPDATE 
					OpportunityAddress 
				SET  
					vcShipCompanyName= @vcCompanyName
					,vcShipStreet=@vcStreet
					,vcShipCity=@vcCity
					,numShipState=@numState
					,vcShipPostCode=@vcPostalCode
					,numShipCountry=@numCountry
					,vcAddressName=@vcAddressName
					,numShipCompanyID=@numCompanyId
					,numShippingContact=@numContact
					,bitAltShippingContact=@bitAltContact
					,vcAltShippingContact=@vcAltContact     
				WHERE 
					numOppID=@numOppID   
			END

			IF ISNULL(@numUserCntID,0) > 0
			BEGIN
				IF NOT EXISTS (SELECT ID FROM OrgOppAddressModificationHistory WHERE numRecordID=@numOppID AND tintAddressOf=2 AND tintAddressType=2)
				BEGIN
					INSERT INTO OrgOppAddressModificationHistory
					(
						tintAddressOf
						,tintAddressType
						,numRecordID
						,numModifiedBy
						,dtModifiedDate
					)
					VALUES
					(
						2
						,2
						,@numOppID
						,@numUserCntID
						,GETUTCDATE()
					)
				END
				ELSE
				BEGIN
					UPDATE
						OrgOppAddressModificationHistory
					SET
						numModifiedBy=@numUserCntID
						,dtModifiedDate=GETUTCDATE()
					WHERE
						numRecordID=@numOppID 
						AND tintAddressOf=2 
						AND tintAddressType=2
				END
			END
		END
		ELSE IF ISNULL(@numReturnHeaderID,0) >0
		BEGIN
			IF NOT EXISTS(SELECT * FROM OpportunityAddress where numReturnHeaderID=@numReturnHeaderID)    
			BEGIN
				INSERT INTO OpportunityAddress
				(
					numOppID
					,vcShipCompanyName
					,vcShipStreet
					,vcShipCity
					,numShipState
					,vcShipPostCode
					,numShipCountry
					,vcAddressName
					,numShipCompanyID
					,numShippingContact
					,bitAltShippingContact
					,vcAltShippingContact
				)    
				VALUES
				(
					@numOppID
					,@vcCompanyName
					,@vcStreet
					,@vcCity
					,@numState
					,@vcPostalCode
					,@numCountry
					,@vcAddressName
					,@numCompanyID
					,@numContact
					,@bitAltContact
					,@vcAltContact
				)    
			END
			ELSE
			BEGIN
				UPDATE 
					OpportunityAddress 
				SET  
					vcShipCompanyName= @vcCompanyName
					,vcShipStreet=@vcStreet
					,vcShipCity=@vcCity
					,numShipState=@numState  
					,vcShipPostCode=@vcPostalCode  
					,numShipCountry=@numCountry
					,vcAddressName=@vcAddressName
					,numShipCompanyID=@numCompanyId
					,numShippingContact=@numContact
					,bitAltShippingContact=@bitAltContact
					,vcAltShippingContact=@vcAltContact     
				WHERE 
					numReturnHeaderID = @numReturnHeaderID 
			END
		END
	END
	ELSE IF @byteMode=2
	BEGIN
		update OpportunityMaster set tintShipToType=3 where numOppID=@numOppID   

		IF ISNULL(@numUserCntID,0) > 0
		BEGIN
			IF NOT EXISTS (SELECT ID FROM OrgOppAddressModificationHistory WHERE numRecordID=@numOppID AND tintAddressOf=2 AND tintAddressType=2)
			BEGIN
				INSERT INTO OrgOppAddressModificationHistory
				(
					tintAddressOf
					,tintAddressType
					,numRecordID
					,numModifiedBy
					,dtModifiedDate
				)
				VALUES
				(
					2
					,2
					,@numOppID
					,@numUserCntID
					,GETUTCDATE()
				)
			END
			ELSE
			BEGIN
				UPDATE
					OrgOppAddressModificationHistory
				SET
					numModifiedBy=@numUserCntID
					,dtModifiedDate=GETUTCDATE()
				WHERE
					numRecordID=@numOppID 
					AND tintAddressOf=2 
					AND tintAddressType=2
			END
		END
	END
	ELSE IF @byteMode= 3     
	BEGIN
		--SELECT * FROM dbo.AddressDetails WHERE numRecordID = @numOppID 
		IF NOT EXISTS(SELECT * FROM dbo.AddressDetails WHERE numRecordID = @numOppID AND tintAddressOf = 4)    
			BEGIN
				PRINT 'ADD Mode'
				INSERT INTO dbo.AddressDetails 
				(vcAddressName,vcStreet,vcCity,vcPostalCode,numState,numCountry,bitIsPrimary,tintAddressOf,tintAddressType,numRecordID,numDomainID )
				SELECT @vcAddressName,@vcStreet,@vcCity,@vcPostalCode,@numState,@numCountry,1,4,2,@numOppID,numDomainID	
				FROM dbo.ProjectsMaster WHERE numProId = @numOppID
				
				UPDATE dbo.ProjectsMaster SET numAddressID = SCOPE_IDENTITY() WHERE numProId = @numOppID
			END
			
		ELSE
			BEGIN
				PRINT 'Update Mode'	 
				UPDATE dbo.AddressDetails SET 
							vcStreet = @vcStreet,
							vcCity = @vcCity,
							numState = @numState,
							vcPostalCode = @vcPostalCode,
							numCountry = @numCountry,
							vcAddressName = @vcAddressName  
				WHERE   numRecordID = @numOppID   	
			END
			
        --SELECT * FROM dbo.AddressDetails WHERE numRecordID = @numOppID 			
	END

IF @numOppID>0 AND ISNULL(@bitCalledFromProcedure,0)=0
BEGIN
	IF (SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppId=@numOppID AND bitAuthoritativeBizDocs=1) = 0
	BEGIN
		DECLARE @tintOppType TINYINT
		SELECT 
			@numDomainID=numDomainID
			,@numDivisionID=numDivisionID
			,@tintOppType=tintOppType
		FROM 
			OpportunityMaster 
		WHERE 
			numOppID=@numOppId

		DELETE FROM OpportunityMasterTaxItems WHERE numOppId=@numOppID

		--INSERT TAX FOR DIVISION   
		IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
		BEGIN
			INSERT INTO dbo.OpportunityMasterTaxItems 
			(
				numOppId,
				numTaxItemID,
				fltPercentage,
				tintTaxType,
				numTaxID
			) 
			SELECT 
				@numOppID,
				TI.numTaxItemID,
				TEMPTax.decTaxValue,
				TEMPTax.tintTaxType,
				0
			FROM 
				TaxItems TI 
			JOIN 
				DivisionTaxTypes DTT 
			ON 
				TI.numTaxItemID = DTT.numTaxItemID
			CROSS APPLY
			(
				SELECT
					decTaxValue,
					tintTaxType
				FROM
					dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
			) AS TEMPTax
			WHERE 
				DTT.numDivisionID=@numDivisionId 
				AND DTT.bitApplicable=1
			UNION 
			SELECT 
				@numOppID,
				0,
				TEMPTax.decTaxValue,
				TEMPTax.tintTaxType,
				0
			FROM 
				dbo.DivisionMaster 
			CROSS APPLY
			(
				SELECT
					decTaxValue,
					tintTaxType
				FROM
					dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
			) AS TEMPTax
			WHERE 
				bitNoTax=0 
				AND numDivisionID=@numDivisionID	
			UNION 
			SELECT
				@numOppId
				,1
				,decTaxValue
				,tintTaxType
				,numTaxID
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,@numOppId,1,NULL)		
		END	

		UPDATE OBD SET monDealAmount= [dbo].[GetDealAmount](@numOppId ,getutcdate(),OBD.numOppBizDocsId ) 
		FROM dbo.OpportunityBizDocs OBD WHERE numOppId=@numOppID
	END                
END 


--Created by Anoop Jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdatePOItemsForFulfillment')
DROP PROCEDURE USP_UpdatePOItemsForFulfillment
GO
CREATE PROCEDURE  USP_UpdatePOItemsForFulfillment
    @numQtyReceived FLOAT,
    @numOppItemID NUMERIC(9),
    @vcError VARCHAR(200) = '' OUTPUT,
    @numUserCntID AS NUMERIC(9),
    @dtItemReceivedDate AS DATETIME,
	@numSelectedWarehouseItemID AS NUMERIC(18,0) = 0,
	@numVendorInvoiceBizDocID NUMERIC(18,0) = 0
AS 
BEGIN
--Transaction is invoked from ADO.net (Purchase Fulfillment) Do not add transaction code here.
-- BEGIN TRY
-- BEGIN TRANSACTION
	IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
	BEGIN
		-- IF RECEIVED AGAINST VENDOR INVOICE MAKE SURE ENOUGH QTY IS PENDING TO RECIEVED AGAINST VEDOR INVOICE BECAUSE USER CAN RECEIVE QTY WITHOUT SELECTING VENDOR INVOCIE ALSO
		IF (SELECT
				COUNT(*)
			FROM
				OpportunityBizDocItems OBDI
			INNER JOIN
				OpportunityItems OI
			ON
				OBDI.numOppItemID=OI.numoppitemtCode
			WHERE
				numOppBizDocID=@numVendorInvoiceBizDocID
				AND OBDI.numOppItemID = @numOppItemID
				AND @numQtyReceived > (ISNULL(OI.numUnitHour,0) - ISNULL(OI.numUnitHourReceived,0))
			) > 0
		BEGIN
			RAISERROR('INVALID_VENDOR_INVOICE_RECEIVE_QTY',16,1)
			RETURN
		END
	END


	DECLARE @numDomain AS NUMERIC(18,0)
	          
	DECLARE @numWarehouseItemID AS NUMERIC
	DECLARE @numWLocationID AS NUMERIC(18,0)     
	DECLARE @numOldQtyReceived AS FLOAT       
	DECLARE @numNewQtyReceived AS FLOAT
	              
	DECLARE @bitStockTransfer BIT
	DECLARE @numToWarehouseItemID NUMERIC
	DECLARE @numOppId NUMERIC
	DECLARE @numUnits FLOAT
	DECLARE @monPrice AS DECIMAL(20,5) 
	DECLARE @numItemCode NUMERIC 
		
	SELECT  
		@numWarehouseItemID = ISNULL([numWarehouseItmsID], 0),
		@numWLocationID = ISNULL(WI.numWLocationID,0),
		@numOldQtyReceived = ISNULL([numUnitHourReceived], 0),
		@bitStockTransfer = ISNULL(OM.bitStockTransfer,0),
		@numToWarehouseItemID= ISNULL(numToWarehouseItemID,0),
		@numOppId=OM.numOppId,
		@numUnits=OI.numUnitHour,
		@monPrice=isnull(monPrice,0) * (CASE WHEN ISNULL(OM.fltExchangeRate,0)=0 THEN 1 ELSE OM.fltExchangeRate END),
		@numItemCode=OI.numItemCode,
		@numDomain = OM.[numDomainId]
	FROM 
		[OpportunityItems] OI 
	INNER JOIN 
		dbo.OpportunityMaster OM
	ON 
		OI.numOppId = OM.numOppId
	LEFT JOIN
		WareHouseItems WI
	ON
		OI.numWarehouseItmsID = WI.numWareHouseItemID
	WHERE 
		[numoppitemtCode] = @numOppItemID


	IF @bitStockTransfer = 1 --added by chintan
	BEGIN
	--ship item from FROM warehouse
		DECLARE @p3 VARCHAR(MAX) = ''
		
		EXEC USP_UpdateQtyShipped @numQtyReceived,@numOppItemID,@numUserCntID,@vcError=@p3 OUTPUT

		IF LEN(@p3)>0 
		BEGIN
			RAISERROR ( @p3,16, 1 )
			RETURN ;
		END
		
		-- Receive item from To Warehouse
		SET @numWarehouseItemID=@numToWarehouseItemID
    
	END  
    

	DECLARE @numTotalQuantityReceived FLOAT
	SET @numTotalQuantityReceived = @numQtyReceived + @numOldQtyReceived ;
	SET @numNewQtyReceived = @numQtyReceived;
	DECLARE @description AS VARCHAR(100)

	SET @description='PO Qty Received (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@numTotalQuantityReceived AS VARCHAR(10)) + ')'
	
	IF @numTotalQuantityReceived > @numUnits
	BEGIN
		RAISERROR('RECEIVED_QTY_MUST_BE_LESS_THEN_OR_EQUAL_TO_ORDERED_QTY',16,1)
	END

	IF @numNewQtyReceived <= 0 
		RETURN 
  
	
	IF ISNULL(@bitStockTransfer,0) = 0
	BEGIN
		DECLARE @TotalOnHand AS FLOAT;SET @TotalOnHand=0  
		SELECT @TotalOnHand=(SUM(ISNULL(numOnHand,0)) + SUM(ISNULL(numAllocation,0))) FROM dbo.WareHouseItems WHERE numItemID=@numItemCode 
						
		--Updating the Average Cost
		DECLARE @monAvgCost AS DECIMAL(20,5) 
		SELECT  @monAvgCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost, 0) END) FROM Item WHERE   numitemcode = @numItemCode  
    
  		SET @monAvgCost = ((@TotalOnHand * @monAvgCost) + (@numNewQtyReceived * @monPrice)) / ( @TotalOnHand + @numNewQtyReceived )
                            
		UPDATE  
			item
		SET 
			monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monAvgCost END)
		WHERE 
			numItemCode = @numItemCode
    END

	

	IF @numWLocationID = -1 AND ISNULL(@numSelectedWarehouseItemID,0) > 0		
	BEGIN
		-- FIRST DECRESE OnOrder OF GLOBAL LOCATION
		UPDATE
			WareHouseItems
		SET 
			numOnOrder = ISNULL(numOnOrder,0) - @numNewQtyReceived,
			dtModified = GETDATE()
		WHERE
			numWareHouseItemID=@numWareHouseItemID
		
		SET @description = CONCAT('PO Qty Received To Internal Location From Global Location (Qty:',@numUnits,' Received:',@numNewQtyReceived,' Total Received:',@numTotalQuantityReceived,')')

		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppId, --  numeric(9, 0)
			@tintRefType = 4, --  tinyint
			@vcDescription = @description,
			@numModifiedBy = @numUserCntID,
			@dtRecordDate =  @dtItemReceivedDate,
			@numDomainID = @numDomain

		-- INCREASE THE OnHand Of Destination Location
		UPDATE
			WareHouseItems
		SET
			numBackOrder = (CASE WHEN numBackOrder >= @numNewQtyReceived THEN ISNULL(numBackOrder,0) - @numNewQtyReceived ELSE 0 END),         
			numAllocation = (CASE WHEN numBackOrder >= @numNewQtyReceived THEN ISNULL(numAllocation,0) + @numNewQtyReceived ELSE ISNULL(numAllocation,0) + ISNULL(numBackOrder,0) END),
			numOnHand = (CASE WHEN numBackOrder >= @numNewQtyReceived THEN numOnHand ELSE ISNULL(numOnHand,0) + @numNewQtyReceived - ISNULL(numBackOrder,0) END),
			dtModified = GETDATE()
		WHERE
			numWareHouseItemID=@numSelectedWarehouseItemID

		SET @description = CONCAT('PO Qty Received From Global Location (Qty:',@numUnits,' Received:',@numNewQtyReceived,' Total Received:',@numTotalQuantityReceived,')')

		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numSelectedWarehouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppId, --  numeric(9, 0)
			@tintRefType = 4, --  tinyint
			@vcDescription = @description,
			@numModifiedBy = @numUserCntID,
			@dtRecordDate =  @dtItemReceivedDate,
			@numDomainID = @numDomain

		INSERT INTO OpportunityItemsReceievedLocation
		(
			numDomainID,
			numOppID,
			numOppItemID,
			numWarehouseItemID,
			numUnitReceieved
		)
		VALUES
		(
			@numDomain,
			@numOppId,
			@numOppItemID,
			@numSelectedWarehouseItemID,
			@numNewQtyReceived
		)
	END
	ELSE 
	BEGIN
		DECLARE @onHand AS FLOAT
		DECLARE @onAllocation AS FLOAT    
		DECLARE @onOrder AS FLOAT            
		DECLARE @onBackOrder AS FLOAT

		SELECT  
			@onHand = ISNULL(numOnHand, 0),
			@onAllocation = ISNULL(numAllocation, 0),
			@onOrder = ISNULL(numOnOrder, 0),
			@onBackOrder = ISNULL(numBackOrder, 0)
		FROM 
			WareHouseItems
		WHERE 
			numWareHouseItemID = @numWareHouseItemID

		IF @onOrder >= @numNewQtyReceived 
		BEGIN
			PRINT '1 case'

			SET @onOrder = @onOrder - @numNewQtyReceived             
			
			IF @onBackOrder >= @numNewQtyReceived 
			BEGIN            
				SET @onBackOrder = @onBackOrder - @numNewQtyReceived             
				SET @onAllocation = @onAllocation + @numNewQtyReceived             
			END            
			ELSE 
			BEGIN            
				SET @onAllocation = @onAllocation + @onBackOrder            
				SET @numNewQtyReceived = @numNewQtyReceived - @onBackOrder            
				SET @onBackOrder = 0            
				SET @onHand = @onHand + @numNewQtyReceived             
			END         
		END            
		ELSE IF @onOrder < @numNewQtyReceived 
		BEGIN
			PRINT '2 case'        
			SET @onHand = @onHand + @onOrder
			SET @onOrder = @numNewQtyReceived - @onOrder
		END   

		SELECT 
			@onHand onHand,
			@onAllocation onAllocation,
			@onBackOrder onBackOrder,
			@onOrder onOrder
                
		UPDATE 
			WareHouseItems
		SET     
			numOnHand = @onHand,
			numAllocation = @onAllocation,
			numBackOrder = @onBackOrder,
			numOnOrder = @onOrder,
			dtModified = GETDATE() 
		WHERE 
			numWareHouseItemID = @numWareHouseItemID
    
		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppId, --  numeric(9, 0)
			@tintRefType = 4, --  tinyint
			@vcDescription = @description, --  varchar(100)
			@numModifiedBy = @numUserCntID,
			@dtRecordDate =  @dtItemReceivedDate,
			@numDomainID = @numDomain
	END
 
	UPDATE  
		[OpportunityItems]
	SET 
		numUnitHourReceived = @numTotalQuantityReceived
	WHERE
		[numoppitemtCode] = @numOppItemID


	IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
	BEGIN
		UPDATE  
			OpportunityBizDocItems
		SET 
			numVendorInvoiceUnitReceived = ISNULL(numVendorInvoiceUnitReceived,0) +  ISNULL(@numQtyReceived,0)
		WHERE
			numOppBizDocID=@numVendorInvoiceBizDocID
			AND numOppItemID = @numOppItemID
	END

	UPDATE dbo.OpportunityMaster SET dtItemReceivedDate=@dtItemReceivedDate WHERE numOppId=@numOppId
END	
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateQtyShipped')
DROP PROCEDURE USP_UpdateQtyShipped
GO
CREATE PROCEDURE  USP_UpdateQtyShipped
    @numQtyShipped FLOAT,
    @numOppItemID NUMERIC(9),
    @numUserCntID NUMERIC(9),
    @vcError VARCHAR(500) ='' OUTPUT
AS 
BEGIN
-- TRANSACTION FROM CODE IS USED FOR PURCHASE FULFILLMENT
BEGIN TRY       
	DECLARE @numDomain AS NUMERIC(18,0)          
    DECLARE @onAllocation AS FLOAT          
    DECLARE @numWarehouseItemID AS NUMERIC       
    DECLARE @numOldQtyShipped AS FLOAT       
    DECLARE @numNewQtyShipped AS FLOAT       
    DECLARE @numOppId NUMERIC
	DECLARE @numUnits FLOAT
    DECLARE @Kit AS BIT                   
	DECLARE @vcOppName VARCHAR(200)

         
    SELECT 
		@numWarehouseItemID = ISNULL([numWarehouseItmsID], 0)
		,@numOldQtyShipped=ISNULL(numQtyShipped,0)
		,@numOppId=numOppId
		,@numUnits=numUnitHour
		,@Kit= (CASE WHEN bitKitParent=1 AND bitAssembly=1 THEN 0 WHEN bitKitParent=1 THEN 1 ELSE 0 END)
		,@numDomain = I.[numDomainID]
    FROM  
		OpportunityItems OI 
	INNER JOIN 
		Item I 
	ON 
		OI.numItemCode=I.numItemCode
    WHERE  
		[numoppitemtCode] = @numOppItemID
        
    SET @numNewQtyShipped = @numQtyShipped-@numOldQtyShipped;
        
    DECLARE @description AS VARCHAR(100)
	SET @description='SO Qty Shipped (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numNewQtyShipped AS VARCHAR(10)) + ')'

	IF @Kit=1
	BEGIN
		SELECT 
			OKI.numOppChildItemID
			,OKI.numWareHouseItemId
			,numQtyItemsReq
			,numQtyItemsReq_Orig
			,numQtyShipped
			,ISNULL(numAllocation, 0) AS numAllocation
			,ROW_NUMBER() OVER(ORDER BY numOppChildItemID) AS RowNumber 
		INTO 
			#tempKits
		FROM 
			OpportunityKitItems OKI 
		INNER JOIN 
			Item I 
		ON 
			OKI.numChildItemID=I.numItemCode  
		INNER JOIN 
			WareHouseItems WI 
		ON 
			WI.numWareHouseItemID=OKI.numWareHouseItemID  
		WHERE 
			charitemtype='P' 
			AND OKI.numWareHouseItemId > 0 
			AND OKI.numWareHouseItemId is not null 
			AND numOppItemID=@numOppItemID 
				
		IF(SELECT COUNT(*) FROM #tempKits WHERE numAllocation - ((@numQtyShipped * numQtyItemsReq_Orig) - numQtyShipped) < 0)>0
		BEGIN
			SELECT TOP 1 
				@vcOppName=ISNULL([vcPOppName],'') 
			FROM 
				[OpportunityMaster] 
			WHERE 
				[numOppId] IN (SELECT [numOppId] FROM [OpportunityItems] WHERE [numoppitemtCode]=@numOppItemID)
			
			SET @vcError ='You do not have enough KIT inventory to support this shipment('+ @vcOppName +'). Modify your Qty-Shipped to an amount equal to or less than the Qty-On-Allocation and try again.' 
		END
		ELSE
		BEGIN
			DECLARE @minRowNumber NUMERIC(9),@maxRowNumber NUMERIC(9),@KitWareHouseItemID NUMERIC(9)
			DECLARE @KitonAllocation as FLOAT,@KitQtyShipped as FLOAT,@KiyQtyItemsReq_Orig as FLOAT,@KiyQtyItemsReq as FLOAT  
			DECLARE @KitNewQtyShipped AS FLOAT,@KitOppChildItemID AS NUMERIC(9)
					
			SELECT  @minRowNumber = MIN(RowNumber),@maxRowNumber = MAX(RowNumber) FROM #tempKits

			WHILE  @minRowNumber <= @maxRowNumber
			BEGIN
				SELECT @KitOppChildItemID=numOppChildItemID,@KitWareHouseItemID=numWareHouseItemID,@KiyQtyItemsReq=numQtyItemsReq,
						@KitQtyShipped=numQtyShipped,@KiyQtyItemsReq_Orig=numQtyItemsReq_Orig,
						@KitonAllocation=numAllocation FROM #tempKits WHERE RowNumber=@minRowNumber
			
				SET @KitNewQtyShipped = (@numNewQtyShipped * @KiyQtyItemsReq_Orig) - @KitQtyShipped;
			
				DECLARE @Kitdescription AS VARCHAR(100)
				SET @Kitdescription='Transfer Order KIT Qty Shipped (Qty:' + CAST(@KiyQtyItemsReq AS VARCHAR(10)) + ' Shipped:' +  CAST(@KitNewQtyShipped AS VARCHAR(10)) + ')'

				SET @KitonAllocation = @KitonAllocation - @KitNewQtyShipped
        
				IF (@KitonAllocation >= 0 )
				BEGIN
					UPDATE  WareHouseItems
					SET     numAllocation = @KitonAllocation,dtModified = GETDATE() 
					WHERE   numWareHouseItemID = @KitWareHouseItemID      	
						
					UPDATE  [OpportunityKitItems]
					SET     [numQtyShipped] = @numQtyShipped * @KiyQtyItemsReq_Orig 
					WHERE   [numOppChildItemID] = @KitOppChildItemID
			                
					EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @KitWareHouseItemID, --  numeric(9, 0)
							@numReferenceID = @numOppId, --  numeric(9, 0)
							@tintRefType = 3, --  tinyint
							@vcDescription = @Kitdescription, --  varchar(100)
							@numModifiedBy = @numUserCntID,
							@numDomainID = @numDomain 
				END
            
				SET @minRowNumber=@minRowNumber + 1			 		
			END				
			   
			UPDATE
				[OpportunityItems]
			SET 
				[numQtyShipped] = @numQtyShipped
			WHERE
				[numoppitemtCode] = @numOppItemID
	                
			EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numOppId, --  numeric(9, 0)
					@tintRefType = 3, --  tinyint
					@vcDescription = @description, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@numDomainID = @numDomain 
		END			
	END
	ELSE
	BEGIN
        SELECT  
            @onAllocation = ISNULL(numAllocation, 0)
        FROM 
			WareHouseItems
        WHERE 
			numWareHouseItemID = @numWareHouseItemID
        
        
		SET @onAllocation = @onAllocation - @numNewQtyShipped
        
        IF (@onAllocation >= 0 )
        BEGIN
            UPDATE 
				WareHouseItems
            SET 
				numAllocation = @onAllocation
				,dtModified = GETDATE() 
            WHERE
				numWareHouseItemID = @numWareHouseItemID      	
			
			
            UPDATE 
				[OpportunityItems]
            SET 
				[numQtyShipped] = @numQtyShipped
            WHERE 
				[numoppitemtCode] = @numOppItemID
                
			EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numOppId, --  numeric(9, 0)
				@tintRefType = 3, --  tinyint
				@vcDescription = @description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomain 
        END
        ELSE
        BEGIN
			SELECT TOP 1 @vcOppName=ISNULL([vcPOppName],'') FROM [OpportunityMaster] WHERE [numOppId] IN (SELECT [numOppId] FROM [OpportunityItems] WHERE [numoppitemtCode]=@numOppItemID)
			SET @vcError ='You do not have enough inventory to support this shipment('+ @vcOppName +'). Modify your Qty-Shipped to an amount equal to or less than the Qty-On-Allocation and try again.' 
		END
    END
END TRY
BEGIN CATCH
  -- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH
END
GO
    
/****** Object:  StoredProcedure [dbo].[USP_DeleteOpportunitySalesTemplate]    Script Date: 09/01/2009 01:16:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageSalesSpecificGenericTemplateItems')
DROP PROCEDURE USP_ManageSalesSpecificGenericTemplateItems
GO
CREATE PROCEDURE [dbo].[USP_ManageSalesSpecificGenericTemplateItems]
	@numSalesTemplateItemID NUMERIC(18),
	@numSalesTemplateID NUMERIC(9),
	@numDomainID NUMERIC(9),
	@numItemCode NUMERIC(18),
	@numWarehouseItmsID NUMERIC(18) NULL,
	@numUOM NUMERIC(18)  NULL,
	@vcUOMName VARCHAR (50) NULL,
	@Attributes VARCHAR (1000) NULL,
	@AttrValues VARCHAR (1000) NULL
AS

		DECLARE @monListPrice DECIMAL(20,5), @numWareHouseID NUMERIC(18), @ItemType VARCHAR(100), @numVendorId NUMERIC(18) 
		--, @numUOM NUMERIC(18), @vcUOMName VARCHAR(100),@numWarehouseItmsID NUMERIC(18)
			
		--SELECT @monListPrice = monListPrice FROM Item WHERE numitemcode = @numitemcode

		/*SELECT @monListPrice = (CASE WHEN charItemType='P' THEN ISNULL(W.monWListPrice,0) ELSE ISNULL(monListPrice,0) END),
			@numWareHouseID = ISNULL(W.numWareHouseID,0),
			@numWarehouseItmsID = ISNULL(W.numWareHouseItemID,0),
			@ItemType = (CASE WHEN charItemType='P' THEN 'Inventory Item' 
			WHEN charItemType='N' THEN 'Non-Inventory Item' 
			WHEN charItemType='S' THEN 'Service Item' 
			ELSE '' END),
			@numUOM = numSaleUnit,
			@vcUOMName = dbo.fn_GetUOMName(ISNULL(I.numBaseUnit,0)), 
			@numVendorId = I.numVendorID
		FROM Item I       
			LEFT JOIN WareHouseItems W  ON  W.numItemID = I.numItemCode AND W.numWareHouseItemID = @numWarehouseItmsID           
			LEFT JOIN ItemExtendedDetails IED ON I.numItemCode = IED.numItemCode
			LEFT JOIN Vendor ON I.numVendorID = Vendor.numVendorID AND Vendor.numItemCode = I.numItemCode            
		WHERE I.numItemCode=@numItemCode 
		
		IF @monListPrice = 0
			SELECT @monListPrice = monListPrice FROM Item WHERE numitemcode = @numitemcode
		*/

		IF EXISTS(SELECT * FROM PricingTable WHERE numitemcode = @numitemcode)
		BEGIN
			SELECT TOP 1 @monListPrice = decDiscount FROM PricingTable WHERE numitemcode = @numitemcode
		END
		ELSE
		BEGIN
			SELECT @monListPrice = monListPrice FROM Item WHERE numitemcode = @numitemcode
		END
	IF @numSalesTemplateItemID > 0
	BEGIN
		UPDATE SalesTemplateItems
		SET numSalesTemplateID = @numSalesTemplateID,
			numDomainID = @numDomainID,
			numItemCode =@numItemCode,
			numWarehouseItmsID = @numWarehouseItmsID,
			numUOM = @numUOM,
			vcUOMName = @vcUOMName,
			Attributes = @Attributes,
			AttrValues = @AttrValues
		WHERE numSalesTemplateItemID = @numSalesTemplateItemID
	END
	ELSE
	BEGIN		

		INSERT INTO SalesTemplateItems
		(
			numSalesTemplateID,
			numDomainID,
			numItemCode,
			numWarehouseItmsID,
			numUOM,
			vcUOMName,
			Attributes,
			AttrValues,
			numUnitHour,
			monPrice,
			numWarehouseID,
			ItemType,
			monTotAmount,
			monTotAmtBefDiscount,
			numSOVendorId   
		)
		VALUES
		(
			@numSalesTemplateID,
			@numDomainID,
			@numItemCode,
			@numWarehouseItmsID,
			@numUOM,
			@vcUOMName,
			@Attributes,
			@AttrValues,
			1,
			@monListPrice,
			@numWareHouseID,
			@ItemType,
			@monListPrice,
			@monListPrice,
			@numVendorId
		)
	END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_OpportunityItems_OrderwiseItems' ) 
    DROP PROCEDURE USP_OpportunityItems_OrderwiseItems
GO
-- =============================================        
-- Author:  <Author,,Sachin Sadhu>        
-- Create date: <Create Date,,30Dec2013>        
-- Description: <Description,,To get Items Customer wise n order wise>        
-- Module:Accounts/Items tab        
-- ============================================= 
CREATE PROCEDURE [dbo].[USP_OpportunityItems_OrderwiseItems]         
 -- Add the parameters for the stored procedure here        
    @numDomainId NUMERIC(9) ,    
    @numDivisionId NUMERIC(9) ,    
    @keyword VARCHAR(50) ,    
    @startDate AS DATETIME =NULL,    
    @endDate AS DATETIME =NULL,    
    @imode AS INT ,    
    @CurrentPage INT = 0 ,    
    @PageSize INT = 0 ,    
    @TotRecs INT = 0 OUTPUT    
AS     
    BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
        SET NOCOUNT ON;        
                
	DECLARE @numCompanyID NUMERIC(18,0)
	SELECT @numCompanyID = numCompanyID FROM DivisionMaster where numDivisionID=@numDivisionId and numDomainID = @numDomainId 

    -- Insert statements for procedure here        
        IF ( @imode = 1 )     
            BEGIN        
                   
                CREATE TABLE #tempItems    
                    (    
                      Units FLOAT,    
                      BasePrice DECIMAL(18,2) ,    
                      ItemName VARCHAR(300) ,
					  vcSKU VARCHAR(300),
					  vcUPC VARCHAR(300),    
                      base DECIMAL(18,2) ,    
                      ChargedAmount DECIMAL(18,2) ,    
                      ORDERID NUMERIC(9) ,    
                      ModelId VARCHAR(200) ,    
                      OrderDate VARCHAR(30) ,    
                      tintOppType TINYINT ,    
                      tintOppStatus TINYINT ,    
                      numContactId NUMERIC ,    
                      numDivisionId NUMERIC ,    
                      OppId NUMERIC ,    
                      OrderNo VARCHAR(300)   ,
					  CustomerPartNo  VARCHAR(100),
					  numItemCode NUMERIc(18,0)
                    )        
                                
                INSERT  INTO #tempItems    
                        SELECT  OI.numUnitHour AS Units ,    
                                OI.monPrice AS BasePrice ,    
                                OI.vcItemName AS ItemName ,
								CASE WHEN ISNULL(I.numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcWHSKU,ISNULL(I.vcSKU,''))ELSE I.vcSKU END,
								CASE WHEN ISNULL(I.numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,ISNULL(I.numBarCodeId,''))ELSE I.numBarCodeId END,
                                OI.monAvgCost AS base ,    
                                OI.monTotAmount AS ChargedAmount ,    
                                Oi.numItemCode AS ORDERID ,    
                                ISNULL(oi.vcModelID, 'NA') AS ModelId ,    
                                dbo.FormatedDateFromDate(OM.bintCreatedDate,OM.numDomainId)   AS OrderDate,     
                                om.tintOppType ,    
                                om.tintOppStatus ,    
                                Om.numContactId ,    
                                ACI.numDivisionId ,    
                                Om.numOppId AS OppId ,    
								 Om.vcPOppName AS OrderNo ,
								 ISNULL(CPN.CustomerPartNo,'') AS  CustomerPartNo,
								 OI.numItemCode
                        FROM    dbo.OpportunityItems AS OI    
								INNER JOIN Item I ON OI.numItemCode = I.numItemCode
                                INNER JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId    
                                INNER JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactId = om.numContactId   
								LEFT JOIN WareHouseItems WI ON OI.numWarehouseItmsID = WI.numWareHouseItemID
								LEFT JOIN  CustomerPartNumber CPN  ON OI.numItemCode = CPN.numItemCode AND numCompanyId = @numCompanyID and CPN.numDomainId = @numDomainId 
                        WHERE   Om.numDomainId = @numDomainId    
                                AND ACI.numDivisionId = @numDivisionId    
                                AND ( OI.vcItemName LIKE @keyword + '%'    
                                      OR OI.vcModelID LIKE @keyword + '%'    
      )    
                                AND ( OM.bintCreatedDate >= ISNULL(@startDate,OM.bintCreatedDate)    
                                      AND Om.bintCreatedDate <=ISNULL(@endDate,OM.bintCreatedDate)     
                                    )         
                                            
                DECLARE @firstRec AS INTEGER          
                DECLARE @lastRec AS INTEGER          
          
                SET @firstRec = ( @CurrentPage - 1 ) * @PageSize          
                SET @lastRec = ( @CurrentPage * @PageSize + 1 )          
                SET @TotRecs = ( SELECT COUNT(*)    
                                 FROM   #tempItems    
                               )          
                   
                SELECT  *    
                FROM    ( SELECT    * ,    
                                    ROW_NUMBER() OVER ( ORDER BY ItemName ) AS RowNumber    
                          FROM      #tempItems    
                        ) a    
                WHERE   RowNumber > @firstRec    
                        AND RowNumber < @lastRec    
                ORDER BY ItemName         
                            
                     
                DROP TABLE #tempItems          
            END        
                   
        ELSE     
            BEGIN       
                   
                CREATE TABLE #tempVItems    
                    (    
					  numDivisionID NUMERIC(18,0),
                      ItemName VARCHAR(300) ,   
					  vcSKU VARCHAR(300),
					  vcUPC VARCHAR(300), 
                      MinOrderQty INT ,    
                      MinCost DECIMAL(18,2) ,    
                      ModelId VARCHAR(200) ,    
                      PartNo VARCHAR(100) ,    
                      numItemCode NUMERIC    ,
					  CustomerPartNo  VARCHAR(100)  
                    )        
                INSERT  INTO #tempVItems    
                        SELECT  @numDivisionId,
								i.vcItemName AS ItemName,
								ISNULL(I.vcSKU,''),
								ISNULL(I.numBarCodeId,''),
                                v.intMinQty AS MinOrderQty,
                                v.monCost AS MinCost,
                                I.vcModelID AS ModelId,
                                v.vcPartNo AS PartNo,
                                v.numItemCode AS numItemCode,
								ISNULL(CPN.CustomerPartNo,'') AS  CustomerPartNo     
                        FROM    dbo.Item AS I    
                                INNER JOIN dbo.Vendor AS V ON I.numVendorID = V.numVendorID    
								LEFT JOIN  CustomerPartNumber CPN  ON I.numItemCode = CPN.numItemCode AND numCompanyId = @numCompanyID and CPN.numDomainId = @numDomainId 
                        WHERE   i.numVendorID = @numDivisionId    
                                AND v.numItemCode = i.numItemCode    
                                AND v.numDomainID = @numDomainId        
                              AND ( I.vcItemName LIKE @keyword + '%'    
                                      OR I.vcModelID LIKE @keyword + '%'    
                                    )    
                 
                 
                DECLARE @VfirstRec AS INTEGER          
                DECLARE @VlastRec AS INTEGER          
          
                SET @VfirstRec = ( @CurrentPage - 1 ) * @PageSize          
                SET @VlastRec = ( @CurrentPage * @PageSize + 1 )          
                SET @TotRecs = ( SELECT COUNT(*)    
                                 FROM   #tempVItems    
                               )          
                     
                            
                SELECT  *    
                FROM    ( SELECT    * ,    
                                    ROW_NUMBER() OVER ( ORDER BY ItemName ) AS RowNumber    
                          FROM      #tempVItems    
                        ) a    
                WHERE   RowNumber > @VfirstRec    
                        AND RowNumber < @VlastRec    
                ORDER BY ItemName               
                            
                    
                DROP TABLE #tempVItems          
                 
                 
            END           
                    
                    
                      
    END 


