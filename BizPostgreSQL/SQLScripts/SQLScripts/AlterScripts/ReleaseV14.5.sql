/******************************************************************
Project: Release 14.5 Date: 09.NOV.2020
Comments: ALTER SCRIPTS
*******************************************************************/

/******************************************** SANDEEP *********************************************/

UPDATE echannelhub SET vcMarketplace=CONCAT(vcMarketplace,' ','SellerCentral') WHERE ID=1
UPDATE echannelhub SET vcMarketplace=CONCAT(vcMarketplace,' ','SellerCentral') WHERE ID=2
UPDATE echannelhub SET vcMarketplace=CONCAT(vcMarketplace,' ','SellerCentral') WHERE ID=3
UPDATE echannelhub SET vcMarketplace=CONCAT(vcMarketplace,' ','SellerCentral') WHERE ID=4

INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (59,'Amazon AU VendorCentral','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (60,'Amazon CAN VendorCentral','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (61,'Amazon UK VendorCentral','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (62,'Amazon US VendorCentral','Marketplaces')

-----------------------------------------

ALTER TABLE StagePercentageDetailsTaskNotes ADD bitDone BIT DEFAULT 0

-----------------------------------------

UPDATE PaymentGateway SET vcGateWayName='CardConnect Account #1',vcThirdFldName='Display Name' WHERE intPaymentGateWay=9

-----------------------------------------

SET IDENTITY_INSERT PaymentGateway ON

INSERT INTO PaymentGateway
(
	intPaymentGateWay,vcGateWayName,vcFirstFldName,vcSecndFldName,vcThirdFldName
)
VALUES
(
	10,'CardConnect Account #2','Login','Password','Display Name'
)

SET IDENTITY_INSERT PaymentGateway OFF

-----------------------------------------

ALTER TABLE TransactionHistory ADD intPaymentGateWay INT DEFAULT 0

-----------------------------------------

ALTER TABLE OpportunityItems ADD vcASIN VARCHAR(100)

-----------------------------------------

ALTER TABLE Domain ADD bitUsePredefinedCustomer BIT DEFAULT 0

-----------------------------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[DomainMarketplace]    Script Date: 05-Nov-20 2:26:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DomainMarketplace](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numMarketplaceID] [numeric](18, 0) NOT NULL,
	[numDivisionID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_DomainMarketplace] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

--------------------------------------------------------------
-- Manually transfer vcMarketplaces values to new table

--SELECT
--	numDomainID
--FROM
--	Domain
--WHERE
--	ISNULL(vcMarketplaces,'') <> ''

INSERT INTO DomainMarketplace
(
	numDomainID
	,numMarketplaceID
)
VALUES
(187,1),(187,11),(187,34),(187,35)