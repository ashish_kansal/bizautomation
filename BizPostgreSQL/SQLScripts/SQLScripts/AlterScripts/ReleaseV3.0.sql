/******************************************************************
Project: Release 3.0 Date: 31.05.2014
Comments: 
*******************************************************************/


-------------------------- MANISH SCRIPTS ----------------------------------

----/******************************************************************
----Project: BACRMUI   Date: 22.05.2014
----Comments: Add new order shipping details to OpportunityMaster
----*******************************************************************/

BEGIN TRANSACTION

ALTER TABLE dbo.OpportunityMaster ADD bitUseMarkupShippingRate BIT NULL,
									  numMarkupShippingRate NUMERIC(19,2) NULL,
									  intUsedShippingCompany INT NULL
									  	
ROLLBACK

----/******************************************************************
----Project: BACRMUI   Date: 20.05.2014
----Comments: Contact Phone extension field should be 7 characters (currently it's 3 or 4).
----*******************************************************************/
BEGIN TRANSACTION
ALTER TABLE AdditionalContactsInformation ALTER COLUMN numPhoneExtension VARCHAR(7)
ROLLBACK

----/******************************************************************
----Project: BACRMUI   Date: 20.05.2014
----Comments: Add "Default Expense" account to the Accounting section of the Organization. When selected, it would pre-select the expense account on new Bills.
----*******************************************************************/
BEGIN TRANSACTION
ALTER TABLE dbo.DivisionMaster ADD numDefaultExpenseAccountID NUMERIC(18,0) NULL

UPDATE dbo.DivisionMaster SET numDefaultExpenseAccountID = 0 

ROLLBACK

/*
----/******************************************************************
----Project: Marketplace Service   Date: 23.04.2014
----Comments: Changes those needs to apply on Production Server for Install/Uninstall service
----*******************************************************************/



Open Command Prompt and type commands as below :

cd\
cd\Windows\System32
sc delete OnlineMarketplaceIntegratorService
installutil /i "FilePath"

*/

----/******************************************************************
----Project: BACRMUI   Date: 07.04.2014
----Comments: Add Field into Domain for Include/Exclude Tax & Shipping from Calculation of Sales Commission
----*******************************************************************/

BEGIN TRANSACTION

ALTER TABLE dbo.Domain ADD bitIncludeTaxAndShippingInCommission BIT

----------
UPDATE dbo.Domain SET bitIncludeTaxAndShippingInCommission = 1 

-------------
ALTER TABLE dbo.DivisionMaster ADD intShippingCompany INT

ROLLBACK

----/******************************************************************
----Project: BACRMUI   Date: 14.Mar.2014
----Comments: Add new tab for Shipping/Packaging in Order/Opportunities
----*******************************************************************/

BEGIN TRANSACTION

ALTER TABLE dbo.DivisionMaster ADD vcShippersAccountNo VARCHAR(100)
ALTER TABLE dbo.OpportunityMaster ADD bitUseShippersAccountNo BIT

ROLLBACK

----/******************************************************************
----Project: BACRMUI   Date: 14.Mar.2014
----Comments: Add new tab for Shipping/Packaging in Order/Opportunities
----*******************************************************************/

BEGIN TRANSACTION

INSERT INTO CFW_Loc_Master
                      (Loc_name, vcFieldType, vcCustomLookBackTableName)
VALUES     ('Shipping & Packaging','O','CFW_Fld_Values_Opp')

DECLARE @numDomainID AS NUMERIC(18)
SELECT ROW_NUMBER() OVER (ORDER BY numDomainID)AS [RowID], numDomainID  INTO #tempDomains FROM dbo.Domain WHERE numDomainId > 0
DECLARE @intCnt AS INT
DECLARE @intCntDomain AS INT
SET @intCnt = 0
SET @intCntDomain = (SELECT COUNT(*) FROM #tempDomains)

IF @intCntDomain > 0
BEGIN
WHILE(@intCnt < @intCntDomain)
BEGIN

SET @intCnt = @intCnt + 1
SELECT @numDomainID = numDomainID FROM #tempDomains WHERE RowID = @intCnt
PRINT @numDomainID
--SELECT * FROM #tempDomains
 
exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=2,@TabName='Shipping & Packaging',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=6,@TabName='Shipping & Packaging',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
--DELETE FROM CFw_Grp_Master WHERE Grp_Name = 'Shipping & Packaging' AND numDomainID = @numDomainID
END 

END
DROP TABLE #tempDomains


-- Add new column into domain for Default Rate Type setting
ALTER TABLE Domain ADD bitDefaultRateType BIT NULL
 
ROLLBACK

----/******************************************************************
----Project: Amazon Service   Date: 16.Feb.2014
----Comments: Add SKU for Matrix Item Support on Marketplace
----*******************************************************************/
BEGIN TRANSACTION

ALTER TABLE dbo.ItemAPI ADD vcSKU VARCHAR(100)

ROLLBACK

--------------------------------- KAMAL SIR -------------------------------------


ALTER TABLE dbo.ImapUserDetails ADD
	vcImapUserName varchar(50) NULL
	
ALTER TABLE dbo.DivisionMaster ADD
	bitEmailToCase bit NULL	


INSERT INTO GenericDocuments 
SELECT '#SYS#EMAIL_ALERT:EMAIL_TO_CASE' ,'Email Alert: Email to Case',369,'','',0,
'HI ##CaseAssigneeName##,<br />  <br />  Case No:##CaseNo##<br />  <br />  Subject: ##CaseSubject##<br />  <br />  Resove Date: ##CaseResoveDate##<br />  <br />  regards,<br />  ##LoggedInUser##',0,0,GETDATE(),0,GETDATE(),
'Case No: ##CaseNo##',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,5


INSERT INTO GenericDocuments 
SELECT '#SYS#EMAIL_ALERT:EMAIL_TO_CASE' ,'Email Alert: Email to Case',369,'','',0,
'HI ##CaseAssigneeName##,<br />  <br />  Case No:##CaseNo##<br />  <br />  Subject: ##CaseSubject##<br />  <br />  Resove Date: ##CaseResoveDate##<br />  <br />  regards,<br />  ##LoggedInUser##',numDomainId,0,GETDATE(),0,GETDATE(),
'Case No: ##CaseNo##',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,5
FROM DOmain WHERE numDomainId>0


CREATE TABLE [dbo].[SMTPUserDetails](
	[numDomainId] [numeric](18, 0) NOT NULL,
	[numUserId] [numeric](18, 0) NOT NULL,
	[vcSMTPUserName] [varchar](50) NULL,
	[vcSMTPPassword] [varchar](100) NULL,
	[vcSMTPServer] [varchar](100) NULL,
	[numSMTPPort] [numeric](18, 0) NULL,
	[bitSMTPSSL] [bit] NULL,
	[bitSMTPServer] [bit] NULL,
	[bitSMTPAuth] [bit] NULL,
 CONSTRAINT [PK_SMTPUserDetails] PRIMARY KEY CLUSTERED 
(
	[numDomainId] ASC,
	[numUserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

-------------------------------------------------------------------------
------------------------  Sachin ----------------------------------------



-------------------------------------------------------------------------
------------------------  Sandeep ----------------------------------------

BEGIN TRANSACTION

----------------------------------------------------------------
---------------------  Organization Look Ahed ------------------
----------------------------------------------------------------

INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag]) VALUES (96,'Customer/Organization Display Fields','Y','N',0,0)
INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag]) VALUES (97,'Customer/Organization Search Fields','Y','N',0,0)

--- Customer/Organization Available Display Fields

IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 30 AND numFormID = 96)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,30,96,'Organization Status',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 29 AND numFormID = 96)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,29,96,'Organization Rating',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 31 AND numFormID = 96)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,31,96,'Organization Credit Limit',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 5 AND numFormID = 96)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,5,96,'Organization Profile',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 28 AND numFormID = 96)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,28,96,'Industry',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 6 AND numFormID = 96)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,6,96,'Relationship',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 16 AND numFormID = 96)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,16,96,'Annual Revenue',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 15 AND numFormID = 96)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,15,96,'Employees',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 10 AND numFormID = 96)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,10,96,'Organization Phone',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 32 AND numFormID = 96)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,32,96,'Organization Fax',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 22 AND numFormID = 96)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,22,96,'Lead Source',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 1 AND numFormID = 96)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,1,96,'Company Differentiation',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 2 AND numFormID = 96)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,2,96,'Company Differentiation Value',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 38 AND numFormID = 96)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,38,96,'Organization Comments',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 17 AND numFormID = 96)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,17,96,'Organization Website',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 222 AND numFormID = 96)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,222,96,'Bill-to Street',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 223 AND numFormID = 96)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,223,96,'Bill-to City',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 224 AND numFormID = 96)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,224,96,'Bill-to State',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 225 AND numFormID = 96)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,225,96,'Bill-to Postal Code',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 226 AND numFormID = 96)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,226,96,'Bill-to Country',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 217 AND numFormID = 96)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,217,96,'Ship-to Street',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 218 AND numFormID = 96)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,218,96,'Ship-to City',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 219 AND numFormID = 96)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,219,96,'Ship-to State',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 220 AND numFormID = 96)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,220,96,'Ship-to Postal Code',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 221 AND numFormID = 96)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,221,96,'Ship-to Country',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 21 AND numFormID = 96)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,21,96,'Territory',0,0,0,0,1)
END

--- Customer/Organization Available Search Fields

IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID =3  AND numFormID = 97)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,3,97,'Organization Name',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 30 AND numFormID = 97)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,30,97,'Organization Status',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 29 AND numFormID = 97)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,29,97,'Organization Rating',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 31 AND numFormID = 97)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,31,97,'Organization Credit Limit',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 5 AND numFormID = 97)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,5,97,'Organization Profile',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 28 AND numFormID = 97)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,28,97,'Industry',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 6 AND numFormID = 97)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,6,97,'Relationship',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 16 AND numFormID = 97)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,16,97,'Annual Revenue',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 15 AND numFormID = 97)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,15,97,'Employees',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 10 AND numFormID = 97)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,10,97,'Organization Phone',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 32 AND numFormID = 97)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,32,97,'Organization Fax',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 22 AND numFormID = 97)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,22,97,'Lead Source',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 1 AND numFormID = 97)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,1,97,'Company Differentiation',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 2 AND numFormID = 97)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,2,97,'Company Differentiation Value',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 38 AND numFormID = 97)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,38,97,'Organization Comments',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 17 AND numFormID = 97)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,17,97,'Organization Website',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 222 AND numFormID = 97)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,222,97,'Bill-to Street',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 223 AND numFormID = 97)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,223,97,'Bill-to City',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 224 AND numFormID = 97)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,224,97,'Bill-to State',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 225 AND numFormID = 97)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,225,97,'Bill-to Postal Code',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 226 AND numFormID = 97)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,226,97,'Bill-to Country',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 217 AND numFormID = 97)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,217,97,'Ship-to Street',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 218 AND numFormID = 97)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,218,97,'Ship-to City',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 219 AND numFormID = 97)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,219,97,'Ship-to State',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 220 AND numFormID = 97)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,220,97,'Ship-to Postal Code',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 221 AND numFormID = 97)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,221,97,'Ship-to Country',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 21 AND numFormID = 97)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,21,97,'Territory',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 2 AND numFieldID = 51 AND numFormID = 97)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (2,51,97,'Contact First Name',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 2 AND numFieldID = 52 AND numFormID = 97)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (2,52,97,'Contact Last Name',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 5 AND numFieldID = 148 AND numFormID = 97)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (5,148,97,'Project Name',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 7 AND numFieldID = 127 AND numFormID = 97)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (7,127,97,'Case Name',0,0,0,0,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 3 AND numFieldID = 96 AND numFormID = 97)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (3,96,97,'Order Name',0,0,0,0,1)
END

--------------------------------------------------------------------------------------
------ UPTO This update on Demo, after this update whole on Production -------------
--------------------------------------------------------------------------------------





----------------------------------------------------------------
---------  New Sales Order Form --------
----------------------------------------------------------------

DECLARE @NewFieldID INTEGER
--- Inserts Dynamic Forms for creating new inventory, non - invemtory and serialized items

	INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag]) VALUES (89,'New Order','Y','N',0,0)
	INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag]) VALUES (90,'New Order - Order Details Settings','Y','N',0,1)
	INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag]) VALUES (91,'New Order - Item Details Settings','Y','N',0,1)
	INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag]) VALUES (92,'New Order - Item Column Settings','Y','N',0,1)
	INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag]) VALUES (93,'New Order - Customer Detail Settings','Y','N',0,1)

-- Add On Credit Hold Field in DycFieldMaster

INSERT INTO dbo.DycFieldMaster 
(		
	numModuleID ,
	numDomainID ,
	vcFieldName ,
	vcDbColumnName ,
	vcOrigDbColumnName ,
	vcPropertyName ,
	vcLookBackTableName ,
	vcFieldDataType ,
	vcFieldType ,
	vcAssociatedControlType ,
	vcToolTip ,
	vcListItemType ,
	numListID ,
	PopupFunctionName ,
	[order] ,
	tintRow ,
	tintColumn ,
	bitInResults ,
	bitDeleted ,
	bitAllowEdit ,
	bitDefault ,
	bitSettingField ,
	bitAddField ,
	bitDetailField ,
	bitAllowSorting ,
	bitWorkFlowField ,
	bitImport ,
	bitExport ,
	bitAllowFiltering ,
	bitInlineEdit ,
	bitRequired ,
	intColumnWidth ,
	intFieldMaxLength
)
VALUES  
( 
	1 , 
	NULL,
	N'On Credit Hold' ,
	N'bitOnCreditHold' , 
	N'bitOnCreditHold' , 
	'OnCreditHold' ,
	N'DivisionMaster' ,
	'Y' , 
	'R' , 
	N'CheckBox' ,
	NULL ,
	'' , 
	0 ,
	NULL ,
	0 ,
	0 , 
	0 , 
	1 , 
	0 ,
	1 , 
	0 ,
	0 , 
	1 , 
	1 ,
	0 , 
	1 , 
	NULL , 
	NULL ,
	NULL ,
	NULL , 
	NULL , 
	NULL , 
	NULL  
)

SELECT @NewFieldID = SCOPE_IDENTITY() 

-- Following script creates tree menu New Item in Field Management Section

	DECLARE @numPageNavID numeric 
	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
	INSERT INTO dbo.PageNavigationDTL
			( numPageNavID ,
			  numModuleID ,
			  numParentID ,
			  vcPageNavName ,
			  vcNavURL ,
			  vcImageURL ,
			  bitVisible ,
			  numTabID
			)
	SELECT @numPageNavID ,13,109,'New Order','../opportunity/frmConfNewOrder.aspx',NULL,1,-1
	
-------------------------------------------------------------------------------
----------- Start - Give permission for each domain to tree node --------------
-------------------------------------------------------------------------------
	
	SELECT * INTO #temp FROM
    (
		SELECT 
			ROW_NUMBER() OVER(ORDER BY numDomainId DESC) AS Row, 
			numDomainId 
		FROM 
			Domain 
		WHERE 
			numDomainId <> -255
	) TABLE2
	
	DECLARE @RowCount INT
	SET @RowCount = (SELECT COUNT(Row) FROM #temp) 
	
	DECLARE @I INT
	DECLARE @numDomainId NUMERIC(18,0);
	
	SET @I = 1

	WHILE (@I <= @RowCount)
	BEGIN
			
		SELECT @numDomainId = numDomainID FROM #temp WHERE Row = @I
		
		DELETE FROM TreeNavigationAuthorization WHERE numPageNavID=@numPageNavID AND numDomainID = @numDomainId

		SELECT * INTO #tempData1 FROM
		(
		 SELECT    T.numTabID,
						CASE WHEN bitFixed = 1
							 THEN CASE WHEN EXISTS ( SELECT numTabName
													 FROM   TabDefault
													 WHERE  numTabId = T.numTabId
															AND tintTabType = T.tintTabType AND numDomainID =D.numDomainID)
									   THEN ( SELECT TOP 1
														numTabName
											  FROM      TabDefault
											  WHERE     numTabId = T.numTabId
														AND tintTabType = T.tintTabType AND numDomainID = D.numDomainID
											)
									   ELSE T.numTabName
								  END
							 ELSE T.numTabName
						END numTabname,
						vcURL,
						bitFixed,
						1 AS tintType,
						T.vcImage,
						D.numDomainID,
						A.numGroupID
			  FROM      TabMaster T
						CROSS JOIN Domain D --ON D.numDomainID = T.numDomainID
						LEFT JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
			  WHERE     bitFixed = 1
						AND D.numDomainId <> -255
						AND t.tintTabType = 1
	          
			  UNION ALL
			  SELECT    -1 AS [numTabID],
						'Administration' numTabname,
						'' vcURL,
						1 bitFixed,
						1 AS tintType,
						'' AS vcImage,
						D.numDomainID,
						A.numGroupID
			  FROM      Domain D
						JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
						WHERE D.numDomainID = @numDomainID
			  
			  UNION ALL
			  SELECT    -3 AS [numTabID],
						'Advanced Search' numTabname,
						'' vcURL,
						1 bitFixed,
						1 AS tintType,
						'' AS vcImage,
						D.numDomainID,
						A.numGroupID
			  FROM      Domain D
						JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId          
			) TABLE2       		
	                    
		INSERT  INTO dbo.TreeNavigationAuthorization
			(
			  numGroupID,
			  numTabID,
			  numPageNavID,
			  bitVisible,
			  numDomainID,
			  tintType
			)
			SELECT  numGroupID,
					PND.numTabID,
					numPageNavID,
					bitVisible,
					numDomainID,
					tintType
			FROM    dbo.PageNavigationDTL PND
					JOIN #tempData1 TD ON TD.numTabID = PND.numTabID 
					WHERE PND.numPageNavID= @numPageNavID
			
			DROP TABLE #tempData1
			
			SET @I = @I  + 1
	END
	
	DROP TABLE #temp


------ Maps fields for New Order - Order Details Settings

	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 3 AND numFieldID = 116 AND numFormID = 90)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (3,116,90,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 3 AND numFieldID = 97 AND numFormID = 90)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (3,97,90,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 3 AND numFieldID = 122 AND numFormID = 90)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (3,122,90,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 3 AND numFieldID = 101 AND numFormID = 90)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (3,101,90,1,1,1)
	END


------ Maps fields for New Order - Item Details Settings

	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 189 AND numFormID = 91)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,189,91,1,0,0)
	END
	
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 193 AND numFormID = 91)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,193,91,1,0,0)
	END
	
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 202 AND numFormID = 91)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,202,91,1,0,0)
	END
	
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 273 AND numFormID = 91)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,273,91,1,0,0)
	END
	
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 274 AND numFormID = 91)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,274,91,1,0,0)
	END
	
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 277 AND numFormID = 91)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,277,91,1,0,0)
	END
	
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 399 AND numFormID = 91)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,399,91,1,0,0)
	END
	
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 215 AND numFormID = 91)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,215,91,1,0,0)
	END
	
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 191 AND numFormID = 91)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,191,91,1,0,0)
	END
	
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 206 AND numFormID = 91)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,206,91,1,0,0)
	END
	
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 204 AND numFormID = 91)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,204,91,1,0,0)
	END
	
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 207 AND numFormID = 91)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,207,91,1,0,0)
	END
	
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 205 AND numFormID = 91)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,205,91,1,0,0)
	END
	
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 276 AND numFormID = 91)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,276,91,1,0,0)
	END
	
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 275 AND numFormID = 91)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,275,91,1,0,0)
	END
	
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 280 AND numFormID = 91)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,280,91,1,0,0)
	END
	
---- Maps fields for New Order - Item Column Settings

	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 203 AND numFormID = 92)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,203,92,1,0,0)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 193 AND numFormID = 92)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,193,92,1,0,0)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 281 AND numFormID = 92)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,281,92,1,0,0)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 202 AND numFormID = 92)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,202,92,1,0,0)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 215 AND numFormID = 92)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,215,92,1,0,0)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 191 AND numFormID = 92)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,191,92,1,0,0)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 206 AND numFormID = 92)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,206,92,1,0,0)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 207 AND numFormID = 92)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,207,92,1,0,0)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 205 AND numFormID = 92)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,205,92,1,0,0)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 204 AND numFormID = 92)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,204,92,1,0,0)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 209 AND numFormID = 92)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,209,92,1,0,0)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 313 AND numFormID = 92)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,313,92,1,0,0)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 234 AND numFormID = 92)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,234,92,1,0,0)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 212 AND numFormID = 92)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,212,92,1,0,0)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 213 AND numFormID = 92)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,213,92,1,0,0)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 214 AND numFormID = 92)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,214,92,1,0,0)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 216 AND numFormID = 92)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,216,92,1,0,0)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 195 AND numFormID = 92)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,195,92,1,0,0)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 196 AND numFormID = 92)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,196,92,1,0,0)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 197 AND numFormID = 92)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,197,92,1,0,0)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 198 AND numFormID = 92)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,198,92,1,0,0)
	END
	
------ Maps fields for New Order - Customer Details Settings
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 1 AND numFormID = 93)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,1,93,1,0,0)
	END
	
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 5 AND numFormID = 93)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,5,93,1,0,0)
	END
	
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 6 AND numFormID = 93)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,6,93,1,0,0)
	END
	
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 7 AND numFormID = 93)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,7,93,1,0,0)
	END
	
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 10 AND numFormID = 93)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,10,93,1,0,0)
	END
	
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 14 AND numFormID = 93)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,14,93,1,0,0)
	END
	
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 15 AND numFormID = 93)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,15,93,1,0,0)
	END
	
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 16 AND numFormID = 93)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,16,93,1,0,0)
	END
	
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 18 AND numFormID = 93)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,18,93,1,0,0)
	END
	
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 21 AND numFormID = 93)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,21,93,1,0,0)
	END
	
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 22 AND numFormID = 93)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,22,93,1,0,0)
	END
	
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 28 AND numFormID = 93)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,28,93,1,0,0)
	END
	
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 29 AND numFormID = 93)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,29,93,1,0,0)
	END
	
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 30 AND numFormID = 93)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,30,93,1,0,0)
	END
	
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 32 AND numFormID = 93)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,32,93,1,0,0)
	END
	
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 38 AND numFormID = 93)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,38,93,1,0,0)
	END
	
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 40 AND numFormID = 93)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,40,93,1,0,0)
	END
	
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 41 AND numFormID = 93)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,41,93,1,0,0)
	END
	
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = @NewFieldID AND numFormID = 93)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,@NewFieldID,93,1,0,0)
	END
	
--- Change Sales Order Form Shortcut
	
	UPDATE
		ShortCutBar
	SET	
		NewLink = '../opportunity/frmNewOrder.aspx'
	WHERE
		vcLinkName = 'Sales Orders' AND id = 115
		
-- Update dycfield vc property		
		UPDATE 
			dycFieldMaster
		SET
			vcPropertyName = 'OnHand'
		WHERE 
			numFieldId = 195
			
		UPDATE 
			dycFieldMaster
		SET
			vcPropertyName = 'OnOrder'
		WHERE 
			numFieldId = 196
			
		UPDATE 
			dycFieldMaster
		SET
			vcPropertyName = 'OnAllocation'
		WHERE 
			numFieldId = 197
			
		UPDATE 
			dycFieldMaster
		SET
			vcPropertyName = 'BackOrder'
		WHERE 
			numFieldId = 198

--TABLE

GO
/****** Object:  Table [dbo].[SalesOrderConfiguration]    Script Date: 05/30/2014 13:53:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SalesOrderConfiguration](
	[numSOCID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[bitAutoFocusCustomer] [bit] NULL,
	[bitAutoFocusItem] [bit] NULL,
	[bitDisplayRootLocation] [bit] NULL,
	[bitAutoAssignOrder] [bit] NULL,
	[bitDisplayLocation] [bit] NULL,
	[bitDisplayFinancialStamp] [bit] NULL,
	[bitDisplayItemDetails] [bit] NULL,
	[bitDisplayUnitCost] [bit] NULL,
	[bitDisplayProfitTotal] [bit] NULL,
	[bitDisplayShippingRates] [bit] NULL,
	[bitDisplayPaymentMethods] [bit] NULL,
	[bitCreateOpenBizDoc] [bit] NULL,
	[numListItemID] [numeric](18, 0) NULL,
	[vcPaymentMethodIDs] [varchar](100) NULL,
 CONSTRAINT [PK_SalesOrderConfiguration] PRIMARY KEY CLUSTERED 
(
	[numSOCID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON



ROLLBACK

-------------------------------------------------------------------------

