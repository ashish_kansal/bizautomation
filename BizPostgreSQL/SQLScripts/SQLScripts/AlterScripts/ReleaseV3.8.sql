/******************************************************************
Project: Release 3.8 Date: 03.NOV.2014
Comments: 
*******************************************************************/

------------------- MANISH ------------------

------/******************************************************************
------Project: BACRMUI   Date: 1.Nov.2014
------Comments: Add new column to ReturnHeader
------*******************************************************************/

ALTER TABLE [dbo].[ReturnHeader] ADD IsUnappliedPayment BIT DEFAULT 0

UPDATE [dbo].[ReturnHeader] SET IsUnappliedPayment = 0
