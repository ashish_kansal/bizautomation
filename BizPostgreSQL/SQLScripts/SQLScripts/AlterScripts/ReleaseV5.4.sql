/******************************************************************
Project: Release 5.4 Date: 04.Feb.2016
Comments: ALTER SCRIPTS
*******************************************************************/

/******************  SANDEEP  ************************/

ALTER TABLE OpportunityItems ADD
[vcAttrValues] VARCHAR(500)

=============================================================================

ALTER TABLE eCommerceDTL ADD
numDefaultClass NUMERIC(18,0)

===============================================================================

USE [Production.2014]
GO

/****** Object:  Table [dbo].[CategoryProfile]    Script Date: 22-Jan-16 12:12:30 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CategoryProfile](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[vcName] [varchar](200) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[dtCreated] [datetime] NOT NULL,
	[numCreatedBy] [numeric](18, 0) NOT NULL,
	[dtModified] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
 CONSTRAINT [PK_CategoryProfile] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

======================================================================================================


USE [Production.2014]
GO

/****** Object:  Table [dbo].[CategoryProfileSites]    Script Date: 22-Jan-16 12:12:40 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CategoryProfileSites](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numCategoryProfileID] [numeric](18, 0) NOT NULL,
	[numSiteID] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_CategoryProfileSites] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[CategoryProfileSites]  WITH CHECK ADD  CONSTRAINT [FK_CategoryProfileSites_Sites] FOREIGN KEY([numSiteID])
REFERENCES [dbo].[Sites] ([numSiteID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[CategoryProfileSites] CHECK CONSTRAINT [FK_CategoryProfileSites_Sites]
GO

===============================================================================================================

BEGIN TRY
BEGIN TRANSACTION

DECLARE @TEMP TABLE
(
	ID NUMERIC(18,0) IDENTITY(1,1),
	numDomainID NUMERIC(18,0)
)

INSERT INTO @TEMP (numDomainID) SELECT numDomainID FROM Domain

DECLARE @i AS INT = 1
DECLARE @COUNT AS INT
DECLARE @numDomainID AS NUMERIC(18,0)

SELECT @COUNT=COUNT(*) FROM @TEMP


WHILE @i <= @COUNT
BEGIN
	SELECT @numDomainID=numDomainID FROM @TEMP WHERE ID=@i


	INSERT INTO CategoryProfile
	(
		vcName,
		numDomainID,
		dtCreated,
		numCreatedBy
	)
	VALUES
	(
		'Default Category',
		@numDomainID,
		GETUTCDATE(),
		ISNULL((SELECT TOP 1 numUserDetailID FROM UserMaster WHERE numDomainID=@numDomainID),0)
	)


	SET @i = @i + 1
END

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

================================================================================

BEGIN TRY
BEGIN TRANSACTION

INSERT INTO CategoryProfileSites
(
	numCategoryProfileID,
	numSiteID
)
SELECT 
	ISNULL((SELECT TOP 1 CP.ID FROM CategoryProfile CP WHERE CP.numDomainID=S.numDomainID),0),
	numSiteID
FROM 
	Sites S

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

========================================================================================

ALTER TABLE Category ADD
numCategoryProfileID NUMERIC(18,0)

========================================================================================

UPDATE
	C
SET
	C.numCategoryProfileID=CP.ID
FROM 
	Category C
JOIN
	CategoryProfile CP
ON
	C.numDomainID = CP.numDomainID

=========================================================================================

--ADD FOLLOWING LINE IN BIZSERVICE CONFIG
 <add key="RecordHistoryRate" value="5"/>



/*************************  PRASANT  ************************/

ALTER TABLE AdditionalContactsInformation ADD 
vcLinkedinId VARCHAR(30) NULL,
vcLinkedinUrl VARCHAR(300) NULL

===============================================================================

BEGIN TRANSACTION

DECLARE @numFieldID AS NUMERIC(18,0)
DECLARE @numFormFieldID AS NUMERIC(18,0)

INSERT INTO DycFieldMaster
(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
VALUES
(2,'Linkedin ID','vcLinkedinId','vcLinkedinId','LinkedinId','AdditionalContactsInformation','V','R','Label',46,1,1,1,0,0,0,1,0,1,1,1,0)

SELECT @numFieldID = SCOPE_IDENTITY()

INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(10,'Linkedin ID','R','Label','vcLinkedinId',0,0,'V','vcLinkedinId',0,'Contacts',0,7,1,'LinkedinId',1,7,1,0,1,0)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(2,@numFieldID,10,1,1,'Linkedin ID','Label','LinkedinId',7,1,7,1,0,0,1,0,1,0,1,@numFormFieldID)

ROLLBACK

================================================================================
BEGIN TRANSACTION

DECLARE @numFieldID AS NUMERIC(18,0)
DECLARE @numFormFieldID AS NUMERIC(18,0)

INSERT INTO DycFieldMaster
(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
VALUES
(2,'Linkedin URL','vcLinkedinUrl','vcLinkedinUrl','LinkedinUrl','AdditionalContactsInformation','V','R','Website',46,1,1,1,0,0,0,1,0,1,1,1,0)

SELECT @numFieldID = SCOPE_IDENTITY()

INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(10,'Linkedin URL','R','Label','vcLinkedinUrl',0,0,'V','vcLinkedinUrl',0,'Contacts',0,7,1,'LinkedinId',1,7,1,0,1,0)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(2,@numFieldID,10,1,1,'Linkedin URL','Website','LinkedinUrl',7,1,7,1,0,0,1,0,1,0,1,@numFormFieldID)

ROLLBACK

============================================================

ALTER TABLE UserMaster ADD vcLinkedinId VARCHAR(300) NULL

UPDATE DycFormField_Mapping SET vcAssociatedControlType='TextArea' WHERE numFormID=26 and numModuleID=3 and numFieldID=253
UPDATE DynamicFormFieldMaster SET vcAssociatedControlType='TextArea' where numFormFieldId=1381
UPDATE DycFieldMaster SET vcAssociatedControlType='TextArea'  where numFieldId=253


ALTER TABLE OpportunityItems ADD vcNotes varchar(1000) NULL
==========================

BEGIN TRANSACTION

DECLARE @numFieldID AS NUMERIC(18,0)
DECLARE @numFormFieldID AS NUMERIC(18,0)

INSERT INTO DycFieldMaster
(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
VALUES
(3,'Notes','vcNotes','vcNotes','Notes','OpportunityItems','V','R','TextArea',46,1,1,1,0,0,0,1,0,1,1,1,0)

SELECT @numFieldID = SCOPE_IDENTITY()

INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(26,'Notes','R','TextArea','vcNotes',0,0,'V','vcNotes',0,'OpportunityItems',0,7,1,'Notes',1,7,1,0,1,0)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(3,@numFieldID,26,1,1,'Notes','TextArea','Notes',7,1,7,1,0,0,1,0,1,0,1,@numFormFieldID)

ROLLBACK

ALTER TABLE UserMaster ADD intAssociate int NULL

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(1,539,97,0,0,'Organization ID','','',7,1,7,1,0,0,1,0,1,0,1,null)


ALTER TABLE VendorShipmentMethod ADD bitPrimary bit
ALTER TABLE OpportunityMaster ADD numShipmentMethod numeric(18, 0)