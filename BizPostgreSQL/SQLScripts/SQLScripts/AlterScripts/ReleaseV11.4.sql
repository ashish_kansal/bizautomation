/******************************************************************
Project: Release 11.4 Date: 21.MAR.2019
Comments: ALTER SCRIPTS
*******************************************************************/

/******************************************** PRASANT *********************************************/

BEGIN TRY
BEGIN TRANSACTION

DECLARE @numFieldID AS NUMERIC(18,0)
DECLARE @numFormFieldID AS NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting
)
VALUES
(
	1,'Last Modified','bintModifiedDate','bintModifiedDate','CompanyInfo','V','R','DateField','',0,1,0,1,0,1,0,0,1
)
SELECT @numFieldID = SCOPE_IDENTITY()
--Lead Form
INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(34,'Last Modified','R','DateField','bintModifiedDate',0,0,'V','bintModifiedDate',0,'CompanyInfo',0,7,1,'bintModifiedDate',1,7,1,0,0,1)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(2,@numFieldID,34,1,1,'Last Modified','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)

--Prospect Form
INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(35,'Last Modified','R','DateField','bintModifiedDate',0,0,'V','bintModifiedDate',0,'CompanyInfo',0,7,1,'bintModifiedDate',1,7,1,0,0,1)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(3,@numFieldID,35,1,1,'Last Modified','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)

--Account Form
INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(36,'Last Modified','R','DateField','bintModifiedDate',0,0,'V','bintModifiedDate',0,'CompanyInfo',0,7,1,'bintModifiedDate',1,7,1,0,0,1)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(4,@numFieldID,36,1,1,'Last Modified','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)


--Tickler Form
INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(43,'Last Modified','R','DateField','bintModifiedDate',0,0,'V','bintModifiedDate',0,'CompanyInfo',0,7,1,'bintModifiedDate',1,7,1,0,0,1)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(1,@numFieldID,43,1,1,'Last Modified','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

------------------------------------------------------------------
BEGIN TRY
BEGIN TRANSACTION

DECLARE @numFieldID AS NUMERIC(18,0)
DECLARE @numFormFieldID AS NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting
)
VALUES
(
	1,'Last Follow-up','dtLastFollowUp','dtLastFollowUp','DivisionMaster','V','R','DateField','',0,1,0,1,0,1,0,0,1
)
SELECT @numFieldID = SCOPE_IDENTITY()
--Lead Form
INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(34,'Last Follow-up','R','DateField','dtLastFollowUp',0,0,'V','dtLastFollowUp',0,'DivisionMaster',0,7,1,'dtLastFollowUp',1,7,1,0,0,1)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(2,@numFieldID,34,1,1,'Last Follow-up','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)

--Prospect Form
INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(35,'Last Follow-up','R','DateField','dtLastFollowUp',0,0,'V','dtLastFollowUp',0,'DivisionMaster',0,7,1,'dtLastFollowUp',1,7,1,0,0,1)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(3,@numFieldID,35,1,1,'Last Follow-up','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)

--Account Form
INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(36,'Last Follow-up','R','DateField','dtLastFollowUp',0,0,'V','dtLastFollowUp',0,'DivisionMaster',0,7,1,'dtLastFollowUp',1,7,1,0,0,1)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(4,@numFieldID,36,1,1,'Last Follow-up','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)


--Tickler Form
INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(43,'Last Follow-up','R','DateField','dtLastFollowUp',0,0,'V','dtLastFollowUp',0,'DivisionMaster',0,7,1,'dtLastFollowUp',1,7,1,0,0,1)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(1,@numFieldID,43,1,1,'Last Follow-up','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

---------------------------------------------------------
ALTER  TABLE DivisionMaster ADD dtLastFollowUp DATETIME DEFAULT NULL

SET IDENTITY_INSERT modulemaster ON;
INSERT INTO modulemaster(numModuleId,vcModuleName,tintGroupType)VALUES(43,'Send Announcement',1)
SET IDENTITY_INSERT modulemaster OFF;

INSERT INTO PageMaster(
numPageID,numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted
)VALUES(
137,43,'','Accounts',1,0,0,0,0,'',0
)

INSERT INTO PageMaster(
numPageID,numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted
)VALUES(
138,43,'','Contacts',1,0,0,0,0,'',0
)

INSERT INTO PageMaster(
numPageID,numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted
)VALUES(
139,43,'','Leads',1,0,0,0,0,'',0
)

INSERT INTO PageMaster(
numPageID,numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted
)VALUES(
140,43,'','Opportunities/Orders',1,0,0,0,0,'',0
)

INSERT INTO PageMaster(
numPageID,numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted
)VALUES(
141,43,'','Prospects',1,0,0,0,0,'',0
)

INSERT INTO PageMaster(
numPageID,numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted
)VALUES(
142,43,'','Relationships',1,0,0,0,0,'',0
)



ALTER TABLE GenericDocuments ADD vcGroupsPermission VARCHAR(500)
ALTER TABLE GenericDocuments ADD numCategoryId NUMERIC DEFAULT 0
ALTER TABLE GenericDocuments ADD bitLastFollowupUpdate BIT DEFAULT 0
UPDATE ListMaster SET vcListName='Category' WHERE numListID=29


/******************************************** RICHA *********************************************/

GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ActivityAttendees](
	[AttendeeID] [int] IDENTITY(1,1) NOT NULL,
	[ActivityID] [int] NOT NULL,
	[ResponseStatus] [varchar](20) NULL,
	[AttendeeEmail] [varchar](50) NULL,
	[AttendeeFirstName] [varchar](50) NULL,
	[AttendeeLastName] [varchar](50) NULL,
	[AttendeePhone] [varchar](15) NULL,
	[AttendeePhoneExtension] [varchar](7) NULL,
	[CompanyNameinBiz] [varchar](100) NULL,
	[DivisionID] [numeric](18, 0) NULL,
	[tintCRMType] [tinyint] NULL,
	[ContactID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_ActivityAttendees] PRIMARY KEY CLUSTERED 
(
	[AttendeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

ALTER TABLE [dbo].[ActivityAttendees]  WITH CHECK ADD  CONSTRAINT [FK_ActivityID] FOREIGN KEY([ActivityID])
REFERENCES [dbo].[Activity] ([ActivityID])
GO

ALTER TABLE [dbo].[ActivityAttendees] CHECK CONSTRAINT [FK_ActivityID]
GO

--------------------------------------------------------

GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ActivityDisplayConfiguration](
	[numDisplayId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainId] [numeric](18, 0) NULL,
	[numUserCntId] [numeric](18, 0) NULL,
	[bitTitle] [bit] NULL,
	[bitLocation] [bit] NULL,
	[bitDescription] [bit] NULL,
 CONSTRAINT [PK_ActivityDisplayConfiguration] PRIMARY KEY CLUSTERED 
(
	[numDisplayId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

--------------------------------------------------------------


alter table Activity
ADD HtmlLink varchar(max)

Go

alter table Activity
ADD Priority numeric(18,0)

Go

alter table Activity
ADD [Activity] numeric(18,0)

Go

alter table Activity
ADD FollowUpStatus numeric(18,0)

Go

alter table Activity
ADD Comments varchar(max)

Go

--------------------------------------

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,
	vcFieldName,
	vcDbColumnName,
	vcOrigDbColumnName,
	vcLookBackTableName,
	vcFieldDataType,
	vcFieldType,
	vcAssociatedControlType,
	vcListItemType,
	numListID,
	bitInResults,
	bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	1,
	'Action-Item Participants',
	'Action-Item Participants',
	'Action-Item Participants',
	'',
	'V',
	'R',
	'TextArea',
	'',
	0,
	0,
	0,0,0,1,0,0,1,1
)


SET @numFieldID = SCOPE_IDENTITY()


IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 43 AND numFieldId = 189 AND numDomainId = 72)
BEGIN
	INSERT INTO DycFormConfigurationDetails 
	([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom])
	VALUES 
	(43,@numFieldID,1,2,72,17,0,1,0)
END

	

INSERT INTO DycFormField_Mapping 
([numModuleID],[numFieldID],[numFormID],vcFieldName,vcAssociatedControlType,[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit]) 
VALUES (1,@numFieldID,43,'Action-Item Participants','TextArea',1,1,1,1)