/******************************************************************
Project: Release 14.4 Date: 24.OCT.2020
Comments: STORE PROCEDURES
*******************************************************************/


--- Created By Anoop jayaraj     
--- Modified By Gangadhar 03/05/2008                                                        

--Select all records  with 
--Rule 1. ANY relationship with a Sales Order. 
--Rule 2. Relationships that are Customers, that have been entered as Accounts from the UI or Record Import, or ..... 
GO
SET ANSI_NULLS on
GO
SET QUOTED_IDENTIFIER on
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_accountlist1')
DROP PROCEDURE usp_accountlist1
GO
CREATE PROCEDURE [dbo].[USP_AccountList1]                                                              
		@CRMType numeric,                                                              
		@numUserCntID numeric,                                                              
		@tintUserRightType tinyint,                                                              
		@tintSortOrder numeric=4,                                                                                  
		@SortChar char(1)='0',  
		@numDomainID as numeric(9)=0,                                                       
		@CurrentPage int,                                                            
		@PageSize int,        
		 @TotRecs int output,                                                           
		@columnName as Varchar(MAX),                                                            
		@columnSortOrder as Varchar(10)    ,                    
		@numProfile as numeric   ,                  
		@bitPartner as BIT,       
		@ClientTimeZoneOffset as int,
		@bitActiveInActive as bit,
		@vcRegularSearchCriteria varchar(MAX)='',
		@vcCustomSearchCriteria varchar(MAX)=''   ,
		@SearchText VARCHAR(100) = ''
AS                                                            
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
           
	DECLARE @tintPerformanceFilter AS TINYINT

	IF PATINDEX('%cmp.vcPerformance=1%', @vcRegularSearchCriteria)>0 --Last 3 Months
	BEGIN
		SET @tintPerformanceFilter = 1
	END
	ELSE IF PATINDEX('%cmp.vcPerformance=2%', @vcRegularSearchCriteria)>0 --Last 6 Months
	BEGIN
		SET @tintPerformanceFilter = 2
	END
	ELSE IF PATINDEX('%cmp.vcPerformance=3%', @vcRegularSearchCriteria)>0 --Last 1 Year
	BEGIN
		SET @tintPerformanceFilter = 3
	END
	ELSE
	BEGIN
		SET @tintPerformanceFilter = 0
	END

	IF CHARINDEX('AD.numBillCountry',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.numBillCountry','AD1.numCountry')
	END
	ELSE IF CHARINDEX('AD.numShipCountry',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.numShipCountry','AD2.numCountry')
	END
	ELSE IF CHARINDEX('AD.vcBillCity',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.vcBillCity','AD5.vcCity')
	END
	ELSE IF CHARINDEX('AD.vcShipCity',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.vcShipCity','AD6.vcCity')
	END
	IF CHARINDEX('ADC.vcLastFollowup',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'ADC.vcLastFollowup', '(SELECT dtExecutionDate FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND ISNULL(ADC.numECampaignID,0) > 0 AND numConECampDTLID IN (SELECT MAX(numConECampDTLID) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND ISNULL(ADC.numECampaignID,0) > 0 AND ISNULL(bitSend,0)=1))')
	END
	IF CHARINDEX('ADC.vcNextFollowup',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'ADC.vcNextFollowup','(SELECT dtExecutionDate FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND ISNULL(ADC.numECampaignID,0) > 0 AND numConECampDTLID IN (SELECT MIN(numConECampDTLID) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND ISNULL(ADC.numECampaignID,0) > 0 AND ISNULL(bitSend,0)=0))')
	END     

	

	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(200),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),
		intColumnWidth INT,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)   

	DECLARE @Nocolumns AS TINYINT = 0                

	SELECT 
		@Nocolumns =ISNULL(SUM(TotalRow),0)  
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=36 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType=2 
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=36 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType=2
	) TotalRows

	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 36 AND numRelCntType=2 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END


	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			36,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,2,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=36 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=2 AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			36,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,2,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=36 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=2 AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=36 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=2 AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=36 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=2 AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		if @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				36,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,2,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=36 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder ASC   
		END
          
		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,
			bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
			bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID ,intColumnWidth,bitAllowFiltering,vcFieldDataType
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=36 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType=2
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=36 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType=2
			AND ISNULL(bitCustom,0)=1
		ORDER BY 
			tintOrder asc  
	END


	DECLARE  @strColumns AS VARCHAR(MAX) = ''
	set @strColumns=' isnull(ADC.numContactId,0)numContactId,isnull(DM.numDivisionID,0)numDivisionID,isnull(DM.numTerID,0)numTerID,isnull(ADC.numRecOwner,0)numRecOwner,isnull(DM.tintCRMType,0) as tintCRMType,ADC.vcGivenName,ADC.vcFirstName+'' ''+ADC.vcLastName AS vcContactName,ADC.numPhone AS numContactPhone,ADC.numPhoneExtension AS numContactPhoneExtension,ADC.vcEmail AS vcContactEmail '   

	--Custom field 
	DECLARE @tintOrder AS TINYINT = 0                                                 
	DECLARE @vcFieldName AS VARCHAR(50)                                                  
	DECLARE @vcListItemType AS VARCHAR(3)                                             
	DECLARE @vcAssociatedControlType VARCHAR(20)     
	declare @numListID AS numeric(9)
	DECLARE @vcDbColumnName VARCHAR(200)                      
	DECLARE @WhereCondition VARCHAR(MAX) = ''                   
	DECLARE @vcLookBackTableName VARCHAR(2000)                
	DECLARE @bitCustom AS BIT
	DECLARE @bitAllowEdit AS CHAR(1)                   
	Declare @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)    
	DECLARE @vcColumnName AS VARCHAR(500)             
	Declare @ListRelID as numeric(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = '' 
	   
    SELECT TOP 1 
		@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
		@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,
		@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
	FROM  
		#tempForm 
	ORDER BY 
		tintOrder ASC            
                             
	WHILE @tintOrder>0                                                  
	BEGIN
		IF @bitCustom = 0                
		BEGIN
			DECLARE @Prefix AS VARCHAR(5)
			
			IF @vcLookBackTableName = 'AdditionalContactsInformation' 
				SET @Prefix = 'ADC.'
			IF @vcLookBackTableName = 'DivisionMaster' 
				SET @Prefix = 'DM.'
	
			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

			
			IF @vcDbColumnName='bitEcommerceAccess'
			BEGIN
				SET @strColumns=@strColumns+ ' ,(CASE WHEN (SELECt COUNT(*) FROM ExtarnetAccounts WHERE numDivisionID=DM.numDivisionID) >0 THEN 1 ELSE 0 END) AS ['+ @vcColumnName+']' 
			END

			----- Added by Priya For Followups(14Jan2018)---- 
			if @vcDbColumnName = 'vcLastFollowup'
			BEGIN
				set @strColumns=@strColumns+',  ISNULL((CASE WHEN (ADC.numECampaignID > 0) THEN dbo.fn_FollowupDetailsInOrgList(ADC.numContactID,1,ADC.numDomainID) ELSE '''' END),'''') ['+ @vcColumnName+']' 
			END
			ELSE IF @vcDbColumnName = 'vcNextFollowup'
			BEGIN
				set @strColumns=@strColumns+',  ISNULL((CASE WHEN  (ADC.numECampaignID >0) THEN dbo.fn_FollowupDetailsInOrgList(ADC.numContactID,2,ADC.numDomainID) ELSE '''' END),'''') ['+ @vcColumnName+']' 
			END
			----- Added by Priya For Followups(14Jan2018)---- 


			ELSE IF @vcAssociatedControlType='SelectBox' or @vcAssociatedControlType='ListBox'                  
			BEGIN

				IF @vcDbColumnName = 'vcSignatureType'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(CASE WHEN vcSignatureType = 0 THEN ''Service Default''
																	WHEN vcSignatureType = 1 THEN ''Adult Signature Required''
																	WHEN vcSignatureType = 2 THEN ''Direct Signature''
																	WHEN vcSignatureType = 3 THEN ''InDirect Signature''
																	WHEN vcSignatureType = 4 THEN ''No Signature Required''
																ELSE '''' END) '  + ' [' + @vcColumnName + ']'
				END
		
				IF @vcDbColumnName='numDefaultShippingServiceID'
				BEGIN
					SET @strColumns=@strColumns+ ' ,ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=DM.numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = DM.numDefaultShippingServiceID),'''')' + ' [' + @vcColumnName + ']' 
				END
				Else IF @vcDbColumnName = 'numPartenerSource'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(DM.numPartenerSource) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=DM.numPartenerSource) AS vcPartenerSource' 
				END
				ELSE IF @vcListItemType='LI'                                                         
				BEGIN
					IF @numListID=40 
					BEGIN
						SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'  
						                                                 
						IF LEN(@SearchText) > 0
						BEGIN 
							SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'' '     
						END                              
						IF @vcDbColumnName='numShipCountry'
						BEGIN
							SET @WhereCondition = @WhereCondition
												+ ' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= DM.numDomainID '
												+ ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=AD2.numCountry '
						END
						ELSE
						BEGIN
							SET @WhereCondition = @WhereCondition
											+ ' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= DM.numDomainID '
											+ ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=AD1.numCountry '
						END
					 END
					ELSE IF @numListID=5
					BEGIN
						SET @strColumns=@strColumns+',(CASE WHEN CMP.numCompanyType=47 THEN CONCAT(L'+ convert(varchar(3),@tintOrder)+'.vcData,'' '',''<i class="fa fa-info-circle text-blue" aria-hidden="true" title="Organizations with a sales orders will be listed in the Account list, just as those with Purchase Orders will be listed in the Vendor list. This explains why you may see a Vendor in the Accounts list and an Account in the Vendor list" style="font-size: 20px;"></i>'') ELSE L'+ convert(varchar(3),@tintOrder)+'.vcData END)'+' ['+ @vcColumnName+']'   
						
						IF LEN(@SearchText) > 0
						BEGIN 
							SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'' '          
						END  
						                                                        
						SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName
					END
					ELSE
					BEGIN
						SET @strColumns=@strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'   
						
						IF LEN(@SearchText) > 0
						BEGIN 
							SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'' '          
						END  
						                                                        
						SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                        
					 END
				END                                                        
				ELSE IF @vcListItemType='S'                                                         
				BEGIN 
					
					                                                           
					IF LEN(@SearchText) > 0
					BEGIN 
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S' + CONVERT(VARCHAR(3), @tintOrder) + '.vcState LIKE ''%' + @SearchText + '%'' '     
					END
					      						
					IF @vcDbColumnName='numShipState'
					BEGIN
						SET @strColumns = @strColumns + ',ShipState.vcState' + ' [' + @vcColumnName + ']' 
						SET @WhereCondition = @WhereCondition
							+ ' left Join AddressDetails AD3 on AD3.numRecordId= DM.numDivisionID and AD3.tintAddressOf=2 and AD3.tintAddressType=2 AND AD3.bitIsPrimary=1 and AD3.numDomainID= DM.numDomainID '
							+ ' left Join State ShipState on ShipState.numStateID=AD3.numState '
					END
					ELSE IF @vcDbColumnName='numBillState'
					BEGIN
						SET @strColumns = @strColumns + ',BillState.vcState' + ' [' + @vcColumnName + ']' 
						SET @WhereCondition = @WhereCondition
							+ ' left Join AddressDetails AD4 on AD4.numRecordId= DM.numDivisionID and AD4.tintAddressOf=2 and AD4.tintAddressType=1 AND AD4.bitIsPrimary=1 and AD4.numDomainID= DM.numDomainID '
							+ ' left Join State BillState on BillState.numStateID=AD4.numState '
					END
					ELSE  
					BEGIN
					SET @strColumns = @strColumns + ',S' + CONVERT(VARCHAR(3), @tintOrder) + '.vcState' + ' [' + @vcColumnName + ']' 
						SET @WhereCondition = @WhereCondition + ' left join State S' + CONVERT(VARCHAR(3), @tintOrder) + ' on S' + CONVERT(VARCHAR(3), @tintOrder) + '.numStateID=' + @vcDbColumnName        
					END		                                           
				END                                                        
				ELSE IF @vcListItemType='T'                                                         
				BEGIN
					SET @strColumns=@strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'   

					IF LEN(@SearchText) > 0
					BEGIN 
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcState LIKE ''%' + @SearchText + '%'' '     
					END                                                           
				  
					SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                        
				END  
				ELSE IF @vcListItemType='C'
				BEGIN
					SET @strColumns=@strColumns+',C'+ convert(varchar(3),@tintOrder)+'.vcCampaignName'+' ['+ @vcColumnName+']' 
					 
					IF LEN(@SearchText) > 0
					BEGIN 
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'C' + CONVERT(VARCHAR(3),@tintOrder) + '.vcCampaignName LIKE ''%' + @SearchText + '%'' '     
					END  
					                                                  
					SET @WhereCondition= @WhereCondition +' left Join CampaignMaster C'+ convert(varchar(3),@tintOrder)+ ' on C'+convert(varchar(3),@tintOrder)+ '.numCampaignId=DM.'+@vcDbColumnName                                                    
				END                       
				ELSE IF @vcListItemType='U'                                                     
				BEGIN
					SET @strColumns=@strColumns+',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'

					IF LEN(@SearchText) > 0
					BEGIN 
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'' '    
					END                                                          
				END  
				ELSE IF @vcListItemType='SYS'                                                         
				BEGIN
					SET @strColumns=@strColumns+',case tintCRMType when 1 then ''Prospect'' when 2 then ''Account'' else ''Lead'' end as ['+ @vcColumnName+']'                                                        

					IF LEN(@SearchText) > 0
					BEGIN 
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'case tintCRMType when 1 then ''Prospect'' when 2 then ''Account'' else ''Lead'' end LIKE ''%' + @SearchText + '%'' '    
					END         
				END                     
			END                                                        
			ELSE IF @vcAssociatedControlType='DateField'                                                    
			BEGIN
				 SET @strColumns=@strColumns+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''        
				 SET @strColumns=@strColumns+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''        
				 SET @strColumns=@strColumns+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '        
				 SET @strColumns=@strColumns+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'                  
					
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'' '
				END
			END      
			ELSE IF @vcAssociatedControlType='TextBox' AND @vcDbColumnName='vcBillCity' 
			BEGIN
				SET @strColumns = @strColumns + ',AD5.vcCity' + ' [' + @vcColumnName + ']'   

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + ' AD5.vcCity LIKE ''%' + @SearchText + '%'' '
				END
				
				SET @WhereCondition = @WhereCondition
							+ ' left Join AddressDetails AD5 on AD5.numRecordId= DM.numDivisionID and AD5.tintAddressOf=2 and AD5.tintAddressType=1 AND AD5.bitIsPrimary=1 and AD5.numDomainID= DM.numDomainID '

			END
			ELSE IF @vcAssociatedControlType='TextBox' AND  @vcDbColumnName='vcShipCity'
			BEGIN
				SET @strColumns=@strColumns + ',AD6.vcCity' + ' [' + @vcColumnName + ']' 
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + ' AD6.vcCity LIKE ''%' + @SearchText + '%'' '
				END  
				
				SET @WhereCondition = @WhereCondition + ' left Join AddressDetails AD6 on AD6.numRecordId= DM.numDivisionID and AD6.tintAddressOf=2 and AD6.tintAddressType=2 AND AD6.bitIsPrimary=1 and AD6.numDomainID= DM.numDomainID '
			END
			ELSE IF @vcAssociatedControlType='Label' and @vcDbColumnName='vcLastSalesOrderDate'
			BEGIN
				SET @WhereCondition = @WhereCondition
												+ ' OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=DM.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder '

				set @strColumns=@strColumns+','+ CONCAT('ISNULL(dbo.FormatedDateFromDate(TempLastOrder.bintCreatedDate,',@numDomainID,'),'''')') +' ['+ @vcColumnName+']'
			END	
			ELSE IF @vcDbColumnName = 'vcCompactContactDetails'
			BEGIN
				SET @strColumns=@strColumns+ ' ,'''' AS vcCompactContactDetails'   
			END  
			ELSE IF @vcAssociatedControlType='Label' and @vcDbColumnName='vcPerformance'
			BEGIN
				SET @WhereCondition = @WhereCondition
												+ CONCAT(' OUTER APPLY(SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = DM.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		AND 1 = (CASE ', @tintPerformanceFilter,' 
																				WHEN 1 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-3,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 2 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-6,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 3 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(YEAR,-1,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				ELSE 1
																				END)
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId ) AS TempPerformance ')

				set @strColumns=@strColumns +', (CASE WHEN ISNULL(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(FORMAT(TempPerformance.monDealAmount,''##,####.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) ['+ @vcColumnName+']'
			END	
			ELSE IF @vcAssociatedControlType='TextBox'  OR @vcAssociatedControlType='Label'                                                   
			BEGIN
				SET @strColumns=@strColumns+','
								+ case  
									when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' 
									when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' 
									else @vcDbColumnName 
								end +' ['+ @vcColumnName+']'                
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 
					case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when      
				  @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' else @vcDbColumnName end
					+' LIKE ''%' + @SearchText + '%'' '
				END  
			END   
		 	ELSE IF @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
			BEGIN
				set @strColumns=@strColumns+',(SELECT SUBSTRING(
				(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
				FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
										 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
					 WHERE SR.numDomainID=DM.numDomainID AND SR.numModuleID=1 AND SR.numRecordID=DM.numDivisionID
							AND UM.numDomainID=DM.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
			END  
			ELSE                                                 
			BEGIN
				 set @strColumns=@strColumns+','+ @vcDbColumnName +' ['+ @vcColumnName+']'
			END             
		END                
		ELSE IF @bitCustom = 1                
		BEGIN
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'  
                
			SELECT 
				@vcFieldName=FLd_label,
				@vcAssociatedControlType=fld_type,
				@vcDbColumnName='Cust'+Convert(varchar(10),Fld_Id)                               
			FROM 
				CFW_Fld_Master
			WHERE 
				CFW_Fld_Master.Fld_Id = @numFieldId
			
			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'            
			BEGIN
				SET @strColumns= @strColumns+',CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
				
				SET @WhereCondition= @WhereCondition 
									+' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)
									+ ' on CFW' + convert(varchar(3),@tintOrder) 
									+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
									+ 'and CFW' + convert(varchar(3),@tintOrder) 
									+ '.RecId=DM.numDivisionId   '                                                         
			END   
			ELSE IF @vcAssociatedControlType = 'CheckBox'       
			BEGIN
				set @strColumns= @strColumns+',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0)='''' then 0 else  isnull(CFW'
												+ CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0) end ['+ @vcColumnName + ']'
 
				set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId   '                                                     
		   END                
			ELSE IF @vcAssociatedControlType = 'DateField'            
			BEGIN
				set @strColumns= @strColumns+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
				set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '                 
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId   '                                                         
			END                
			ELSE IF @vcAssociatedControlType = 'SelectBox'             
			BEGIN
				set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
				set @strColumns=@strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
				set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '                 
					on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId    '                                                         
				set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
			END
			ELSE IF @vcAssociatedControlType = 'CheckBoxList' 
			BEGIN
				SET @strColumns= @strColumns+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

			SET @WhereCondition= @WhereCondition 
								+' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId=DM.numDivisionId '
			END              
		END          
                
		SELECT TOP 1 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,
			@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
		FROM
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 SET @tintOrder=0 
	END     

	  
	DECLARE @strShareRedordWith AS VARCHAR(300);
	SET @strShareRedordWith= CONCAT(' DM.numDivisionID IN (SELECT SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID=',@numDomainID,' AND SR.numModuleID=1 AND SR.numAssignedTo=',@numUserCntId,') ')

	DECLARE @strExternalUser AS VARCHAR(MAX) = ''
	SET @strExternalUser = CONCAT(' (NOT EXISTS (SELECT numUserDetailID FROM UserMaster WHERE numDomainID=',@numDomainID,' AND numUserDetailID=',@numUserCntId,') AND EXISTS (SELECT EAD.numExtranetDtlID FROM ExtranetAccountsDtl EAD INNER JOIN ExtarnetAccounts EA ON EAD.numExtranetID=EA.numExtranetID WHERE EAD.numDomainID=',@numDomainID,' AND EAD.numContactID=',@numUserCntID,' AND (EA.numDivisionID=DM.numDivisionID OR EA.numDivisionID=DM.numPartenerSource)))')
   
	SET @strColumns=@strColumns+' ,ISNULL(VIE.Total,0) as TotalEmail '
	SET @strColumns=@strColumns+' ,ISNULL(VOA.OpenActionItemCount,0) as TotalActionItem '

	SET @strColumns=@strColumns+' ,

	((CASE 
	WHEN ADC.numECampaignID > 0 
	THEN 
		 ISNULL( (CASE WHEN 
				ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											WHERE ConECampaign.numContactID = ADC.numContactID AND  ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =1 AND ISNULL(bitSend,0) = 1 AND ADC.bitPrimaryContact = 1),0) 
				= ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											 WHERE ConECampaign.numContactID = ADC.numContactID AND ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =1 AND ADC.bitPrimaryContact = 1 ),0)
		THEN
		  ''CheckeredFlag''
		  Else
		  ''GreenFlag''
		  End),0)



	ELSE '''' 
	END)) as FollowupFlag '

	SET @strColumns=@strColumns+' ,

	((CASE 
	WHEN ADC.numECampaignID > 0 
	THEN 
		''('' + CAST(ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											WHERE ConECampaign.numContactID = ADC.numContactID AND  ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =1 AND ISNULL(bitSend,0) = 1 AND ADC.bitPrimaryContact = 1),0) AS varchar)

			+ ''/'' + CAST(ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											 WHERE ConECampaign.numContactID = ADC.numContactID AND ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =1 AND ADC.bitPrimaryContact = 1 ),0)AS varchar) + '')''
			+ ''<a onclick=openDrip('' + (cast(ISNULL((SELECT MAX([numConEmailCampID]) FROM [ConECampaign] WHERE [numContactID] = ADC.numContactID AND [numECampaignID]= ADC.numECampaignID AND ISNULL(bitEngaged,0)=1 AND ADC.bitPrimaryContact = 1 ),0)AS varchar)) + '');>
		 <img alt=Follow-up Campaign history height=16px width=16px title=Follow-up campaign history src=../images/GLReport.png
		 ></a>'' 
	ELSE '''' 
	END)) as ReadUnreadCntNHstr '
   
	DECLARE @StrSql AS VARCHAR(MAX) = ''
	SET @StrSql=' FROM  CompanyInfo CMP                                                            
					join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID
					LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=DM.numDivisionID 
					left join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID
					left join VIEW_OPEN_ACTION_ITEMS VOA WITH (NOEXPAND) ON VOA.numDomainID = '+ CAST(@numDomainID as varchar )+' AND  VOA.numDivisionId = DM.numDivisionID 
					left join View_InboxEmail VIE  WITH (NOEXPAND) ON VIE.numDomainID = '+ CAST(@numDomainID as varchar )+' AND  VIE.numContactId = ADC.numContactId ' + ISNULL(@WhereCondition,'')

	IF @tintSortOrder= 9 set @strSql=@strSql+' join Favorites F on F.numContactid=DM.numDivisionID ' 
	
	 -------Change Row Color-------
	Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
	SET @vcCSOrigDbCOlumnName=''
	SET @vcCSLookBackTableName=''

	Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

	insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
	from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
	join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
	where DFCS.numDomainID=@numDomainID and DFFM.numFormID=36 AND DFCS.numFormID=36 and isnull(DFFM.bitAllowGridColor,0)=1

	IF(SELECT COUNT(*) FROM #tempColorScheme)>0
	BEGIN
	   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
	END   
	----------------------------                   
 
	 IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
	set @strColumns=@strColumns+',tCS.vcColorScheme'
	END

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
			set @Prefix = 'ADC.'                  
		 if @vcCSLookBackTableName = 'DivisionMaster'                  
			set @Prefix = 'DM.'
		 if @vcCSLookBackTableName = 'CompanyInfo'                  
			set @Prefix = 'CMP.'   
 
		IF @vcCSAssociatedControlType='DateField'
		BEGIN
				set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
				 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
		END
		ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
		BEGIN
			set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
		END
	END

	IF @columnName LIKE 'CFW.Cust%' 
	BEGIN                
		SET @strSql = @strSql + ' left Join CFW_FLD_Values CFW on CFW.RecId=DM.numDivisionID and CFW.fld_id= ' + REPLACE(@columnName, 'CFW.Cust', '') + ' '                                                         
		SET @columnName = 'CFW.Fld_Value'                
	END                                         
	ELSE IF @columnName LIKE 'DCust%' 
    BEGIN                
        SET @strSql = @strSql + ' left Join CFW_FLD_Values CFW on CFW.RecId=DM.numDivisionID  and CFW.fld_id= ' + REPLACE(@columnName, 'DCust', '')                                                                                                   
        SET @strSql = @strSql + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '                
        SET @columnName = 'LstCF.vcData'                  
    END
	ELSE IF @columnName = 'Opp.vcLastSalesOrderDate'   
	BEGIN
		SET @columnName = 'TempLastOrder.bintCreatedDate'
	END
	ELSE IF @columnName = 'DMSC.vcSignatureType'
	BEGIN
		SET @columnName = '(CASE WHEN vcSignatureType = 0 THEN ''Service Default''
																	WHEN vcSignatureType = 1 THEN ''Adult Signature Required''
																	WHEN vcSignatureType = 2 THEN ''Direct Signature''
																	WHEN vcSignatureType = 3 THEN ''InDirect Signature''
																	WHEN vcSignatureType = 4 THEN ''No Signature Required''
																ELSE '''' END)'
	END
	
	IF @columnName = 'CMP.vcPerformance'   
	BEGIN
		SET @columnName = 'TempPerformance.monDealAmount'
	END

	SET @strSql = @strSql + ' WHERE  (ISNULL(ADC.bitPrimaryContact,0)=1   OR ADC.numContactID IS NULL)
							AND CMP.numDomainID=DM.numDomainID     
							AND (CMP.numCompanyType=46 OR (CMP.numCompanyType=47 AND EXISTS (SELECT numOppID FROM [OpportunityMaster] OM WHERE OM.numDomainId=' + CONVERT(VARCHAR(15), @numDomainID) + ' AND OM.numDivisionID=DM.numDivisionID AND OM.tintOppType = 1 AND OM.tintOppStatus = 1)))                                    
							AND DM.tintCRMType= '+ convert(varchar(2),@CRMType)+'                                            
							AND DM.numDomainID= ' + convert(varchar(15),@numDomainID)

	SET @strSql=@strSql + ' AND DM.bitActiveInActive=' + CONVERT(VARCHAR(15), @bitActiveInActive)
	
	IF LEN(@SearchQuery) > 0
	BEGIN
		SET @StrSql = @StrSql + ' AND (' + @SearchQuery + ') '
	END	

	 IF @bitPartner = 1 
	BEGIN
        SET @strSql = @strSql + 'and (DM.numAssignedTo=' + CONVERT(VARCHAR(15), @numUserCntID) + ' or DM.numCreatedBy=' + CONVERT(VARCHAR(15), @numUserCntID) + ') '                                                                 
    END
	                                                         
    IF @SortChar <> '0' 
	BEGIN
        SET @strSql = @strSql + ' And CMP.vcCompanyName like ''' + @SortChar + '%'''
		                                                               
	END

    IF @tintUserRightType = 1 
	BEGIN
        SET @strSql = @strSql + CONCAT(' AND (DM.numRecOwner = ',@numUserCntID,' OR DM.numAssignedTo=',@numUserCntID,' OR ',@strShareRedordWith,' OR ',@strExternalUser,')')                                                                     
	END
    ELSE IF @tintUserRightType = 2 
	BEGIN
        SET @strSql = @strSql + CONCAT(' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ',@numUserCntID,' ) or DM.numAssignedTo=',@numUserCntID,' or ',@strShareRedordWith,')')
    END
	               
    IF @numProfile <> 0 
	BEGIN
		SET @strSQl = @strSql + ' and cmp.vcProfile = ' + CONVERT(VARCHAR(15), @numProfile)                                                    
    END
	                                                      
    IF @tintSortOrder = 1 
        SET @strSql = @strSql + ' AND DM.numStatusID=2 '                                                              
    ELSE IF @tintSortOrder = 2 
        SET @strSql = @strSql + ' AND DM.numStatusID=3 '                          
    ELSE IF @tintSortOrder = 3 
        SET @strSql = @strSql + ' AND (DM.numRecOwner = ' + CONVERT(VARCHAR(15), @numUserCntID) + ' or DM.numAssignedTo=' + CONVERT(VARCHAR(15), @numUserCntID) + ' or ' + @strShareRedordWith +' OR ' + @strExternalUser + ')'                                                                                  
    ELSE IF @tintSortOrder = 5 
        SET @strSql = @strSql + ' order by numCompanyRating desc '                                                                     
    ELSE IF @tintSortOrder = 6 
        SET @strSql = @strSql + ' AND DM.bintCreatedDate > ''' + CONVERT(VARCHAR(20), DATEADD(day, -7,GETUTCDATE())) + ''''                                                                    
    ELSE IF @tintSortOrder = 7 
        SET @strSql = @strSql + ' and DM.numCreatedby=' + CONVERT(VARCHAR(15), @numUserCntID)                     
    ELSE IF @tintSortOrder = 8 
        SET @strSql = @strSql + ' and DM.numModifiedby=' + CONVERT(VARCHAR(15), @numUserCntID)                  
                                                   
        
	IF @vcRegularSearchCriteria<>'' 
	BEGIN
		IF PATINDEX('%cmp.vcPerformance%', @vcRegularSearchCriteria)>0
		BEGIN
			-- WE ARE MANAGING CONDITION IN OUTER APPLY
			set @strSql=@strSql
		END
		ELSE
			set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
	END


	IF @vcCustomSearchCriteria <> '' 
	BEGIN
		SET @strSql=@strSql +' AND ' + @vcCustomSearchCriteria 
    END  

	DECLARE @firstRec AS INTEGER                                                            
	DECLARE @lastRec AS INTEGER                                                            
	SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                                            
	SET @lastRec = ( @CurrentPage * @PageSize + 1 )                  

	-- EXECUTE FINAL QUERY
	DECLARE @strFinal AS NVARCHAR(MAX)
	SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, ',@strColumns,' INTO #tempTable',@StrSql,'; SELECT @TotalRecords = COUNT(*) FROM #tempTable; SELECT * FROM #tempTable WHERE ID > ',@firstRec,' and ID <',@lastRec,'; DROP TABLE #tempTable;')
	
	PRINT CAST(@strFinal AS NTEXT)
	exec sp_executesql @strFinal, N'@TotalRecords INT OUT', @TotRecs OUT

	SELECT * FROM #tempForm
	DROP TABLE #tempForm
	DROP TABLE #tempColorScheme
END
/****** Object:  StoredProcedure [dbo].[USP_AdvancedSearchOpp]    Script Date: 07/26/2008 16:14:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_advancedsearchopp')
DROP PROCEDURE usp_advancedsearchopp
GO
Create PROCEDURE [dbo].[USP_AdvancedSearchOpp]
@WhereCondition as varchar(4000)='',
@numDomainID as numeric(9)=0,
@numUserCntID as numeric(9)=0,
@CurrentPage int,
@PageSize int,                                                                  
@TotRecs int output,                                                                                                           
@columnSortOrder as Varchar(10),    
@ColumnName as varchar(50)='',    
@SortCharacter as char(1),                
@SortColumnName as varchar(50)='',    
@LookTable as varchar(10)='',
@strMassUpdate as varchar(2000)='',
@vcRegularSearchCriteria varchar(MAX)='',
@vcCustomSearchCriteria varchar(MAX)='',
@vcDisplayColumns VARCHAR(MAX) = '',
@GetAll BIT = 0
as   
  
	  
declare @tintOrder as tinyint                                  
declare @vcFormFieldName as varchar(50)                                  
declare @vcListItemType as varchar(3)                             
declare @vcListItemType1 as varchar(3)                                 
declare @vcAssociatedControlType varchar(20)                                  
declare @numListID AS numeric(9)                                  
declare @vcDbColumnName varchar(30)                                   
declare @ColumnSearch as varchar(10)
DECLARE @vcLookBackTableName VARCHAR(50)
DECLARE @bitIsSearchBizDoc BIT




IF CHARINDEX('OpportunityBizDocs', @WhereCondition) > 0
begin

	SET @bitIsSearchBizDoc = 1
	end
ELSE 
 BEGIN
	SET @bitIsSearchBizDoc = 0
	
 END
 --Added By Sachin Sadhu||Date:12thJune2014
  --Purpose:To add Functionaly :Search with Notes Field-OpportunityBizDocItems
  --Set Manually To fullfill "All" Selection in Search
 IF CHARINDEX('vcNotes', @WhereCondition) > 0
	BEGIN
			SET 	@bitIsSearchBizDoc = 1
	END
--End of script

if (@SortCharacter<>'0' and @SortCharacter<>'') set @ColumnSearch=@SortCharacter   
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                         
      numOppID varchar(15),
	  tintCRMType TINYINT,
	  numContactID NUMERIC(18,0),
	  numDivisionID NUMERIC(18,0),
      numOppBizDocID varchar(15)
 )  
  
declare @strSql as varchar(8000)
 set  @strSql='select OppMas.numOppId, DM.tintCRMType, OppMas.numDivisionID, ADC.numContactId '
 
IF @bitIsSearchBizDoc = 1 --Added to eliminate duplicate records 
	SET @strSql = @strSql + ' ,OpportunityBizDocs.numOppBizDocsId '
ELSE 
	SET @strSql = @strSql + ' ,0 numOppBizDocsId '

IF @SortColumnName ='CalAmount'
BEGIN
	ALTER TABLE #tempTable ADD CalAmount DECIMAL(20,5) ;
	SET @strSql = @strSql + ' ,[dbo].[getdealamount](OppMas.numOppId,Getutcdate(),0) as CalAmount'
END	

IF @SortColumnName ='monDealAmount'
BEGIN
	ALTER TABLE #tempTable ADD monDealAmount DECIMAL(20,5) ;

	SET @strSql = @strSql + ' ,ISNULL((SELECT SUM(ISNULL(BD.monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OppMas.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) as monDealAmount'
END	

IF @SortColumnName ='monAmountPaid'
BEGIN
	ALTER TABLE #tempTable ADD monAmountPaid DECIMAL(20,5) ;

	SET @strSql = @strSql + ' ,ISNULL((SELECT SUM(ISNULL(BD.monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OppMas.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) AS monAmountPaid'
END	

SET @strSql = @strSql + ' from   
OpportunityMaster OppMas   
Join AdditionalContactsinformation ADC on OppMas.numContactId = ADC.numContactId  
join DivisionMaster DM on Dm.numDivisionId = OppMas.numDivisionId  
JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId   
'  
IF @bitIsSearchBizDoc = 1 --Added to eliminate duplicate records 
BEGIN	
	IF CHARINDEX('vcNotes', @WhereCondition) > 0
		BEGIN
	  		SET @strSql = @strSql + ' left join OpportunityBizDocs on OppMas.numOppId = OpportunityBizDocs.numOppId left join OpportunityBizDocItems on OpportunityBizDocItems.numOppBizDocID=OpportunityBizDocs.numOppBizDocsId '
		END
	ELSE
		BEGIN
			SET @strSql = @strSql + ' left join OpportunityBizDocs on OppMas.numOppId = OpportunityBizDocs.numOppId '
		END


END
	
IF @SortColumnName ='numBillCountry' OR @SortColumnName ='numBillState'
     OR @ColumnName ='numBillCountry' OR @ColumnName ='numBillState'
BEGIN 	
	set @strSql= @strSql +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
END	
ELSE IF @SortColumnName ='numShipCountry' OR @SortColumnName ='numShipState'
     OR @ColumnName ='numShipCountry' OR @ColumnName ='numShipState'
BEGIN
	set @strSql= @strSql +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
END

   if (@SortColumnName<>'')                        
  begin                              
 select top 1 @vcListItemType1=vcListItemType from View_DynamicDefaultColumns where vcDbColumnName=@SortColumnName and numFormID=15 and numDomainId=@numDomainId
  if @vcListItemType1='LI'                       
  begin
			IF @SortColumnName ='numBillCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD1.numCountry' 
			END
			ELSE IF @SortColumnName ='numShipCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD2.numCountry' 
			END
		  ELSE
		  BEGIN                                    
				set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                                  
		  END
  end  
 else if @vcListItemType1='S'                               
    begin           
		IF @SortColumnName ='numBillState'
		BEGIN
   			set @strSql= @strSql +' left join State S2 on S2.numStateID=AD1.numState'                            
		END
	    ELSE IF @SortColumnName ='numShipState'
	    BEGIN
   			set @strSql= @strSql +' left join State S2 on S2.numStateID=AD2.numState'                           
		END
    end                          
  end                              
  set @strSql=@strSql+' where OppMas.numDomainID  = '+convert(varchar(15),@numDomainID)+   @WhereCondition                                      
     
  
if (@ColumnName<>'' and  @ColumnSearch<>'')                             
begin                              
  if @vcListItemType='LI'                                   
    begin    
			IF @ColumnName ='numBillCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD1.numCountry' 
			END
			ELSE IF @ColumnName ='numShipCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD2.numCountry' 
			END
		  ELSE
		  BEGIN                                
				set @strSql= @strSql +' and L1.vcData like '''+@ColumnSearch +'%'''                                 
			END
		end                                  
 else if @vcListItemType='S'                               
    begin           
		IF @ColumnName ='numBillState'
		BEGIN
   			set @strSql= @strSql +' left join State S1 on S1.numStateID=AD1.numState'                            
		END
	    ELSE IF @ColumnName ='numShipState'
	    BEGIN
   			set @strSql= @strSql +' left join State S1 on S1.numStateID=AD2.numState'                           
		END
    end
    else set @strSql= @strSql +' and '+ 
				case when @ColumnName = 'numAssignedTo' then 'OppMas.'+@ColumnName 
				else @ColumnName end 
				+' like '''+@ColumnSearch  +'%'''                                
                              
end                              
   if (@SortColumnName<>'')                            
  begin     
                          
    if @vcListItemType1='LI'                                   
    begin        
      set @strSql= @strSql +' order by L2.vcData '+@columnSortOrder                                  
    end   
	else if @vcListItemType1='S'                               
		begin                                  
		  set @strSql= @strSql +' order by S2.vcState '+@columnSortOrder                              
		end                                
    ELSE
	BEGIN
		--SET @strSql  = REPLACE(@strSql,'distinct','')
		set @strSql= @strSql +' order by '+ @SortColumnName +' ' +@columnSortOrder
	END  
  end   
  
  
insert into #tempTable exec(@strSql)    
 print @strSql 
 print '===================================================='  
         
         --SELECT * FROM #tempTable               
set @strSql=''                                  
                                  
DECLARE @bitBillAddressJoinAdded AS BIT;
DECLARE @bitShipAddressJoinAdded AS BIT;
SET @bitBillAddressJoinAdded=0;
SET @bitShipAddressJoinAdded=0;
                      
set @tintOrder=0                                  
set @WhereCondition =''                               
set @strSql='select OppMas.numOppId, DM.tintCRMType, OppMas.numDivisionID, ADC.numContactId, OppMas.numRecOwner  '
IF @bitIsSearchBizDoc = 1 --Added to eliminate duplicate records 
BEGIN
	SET @strSql = @strSql + ' ,OpportunityBizDocs.numOppBizDocsId '
END

	DECLARE @TEMPSelectedColumns TABLE
	(
		numFieldID NUMERIC(18,0)
		,bitCustomField BIT
		,tintOrder INT
		,intColumnWidth FLOAT
	)
	DECLARE @vcLocationID VARCHAR(100) = '2,6'

	-- IF USER HAS SELECTED ITS OWN DISPLAY COLUMNS THEN USE IT
	IF LEN(ISNULL(@vcDisplayColumns,'')) > 0
	BEGIN
		DECLARE @TempIDs TABLE
		(
			ID INT IDENTITY(1,1),
			vcFieldID VARCHAR(300)
		)

		INSERT INTO @TempIDs
		(
			vcFieldID
		)
		SELECT 
			OutParam 
		FROM 
			SplitString(@vcDisplayColumns,',')

		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT
			numFormFieldID
			,bitCustomField
			,(SELECT (ID-1) FROM @TempIDs T3 WHERE T3.vcFieldID = TEMP.vcFieldID)
			,0
		FROM
		(
			SELECT  
				numFieldID as numFormFieldID,
				0 bitCustomField,
				CONCAT(numFieldID,'~0') vcFieldID
			FROM    
				View_DynamicDefaultColumns
			WHERE   
				numFormID = 15
				AND bitInResults = 1
				AND bitDeleted = 0 
				AND numDomainID = @numDomainID
			UNION 
			SELECT  
				c.Fld_id AS numFormFieldID
				,1 bitCustomField
				,CONCAT(Fld_id,'~1') vcFieldID
			FROM    
				CFW_Fld_Master C 
			LEFT JOIN 
				CFW_Validation V ON V.numFieldID = C.Fld_id
			JOIN 
				CFW_Loc_Master L on C.GRP_ID=L.Loc_Id
			WHERE   
				C.numDomainID = @numDomainID
				AND GRP_ID IN (SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
		) TEMP
		WHERE
			vcFieldID IN (SELECT vcFieldID FROM @TempIDs)
	END

	-- IF USER SELECTED DISPLAY COLUMNS IS NOT AVAILABLE THAN GET COLUMNS CONFIGURED FROM SEARCH RESULT GRID
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT
			numFormFieldID
			,bitCustomField
			,tintOrder
			,intColumnWidth
		FROM
		(
			SELECT  
				A.numFormFieldID,
				0 bitCustomField,
				A.tintOrder,
				A.intColumnWidth
			FROM    AdvSerViewConf A
					JOIN View_DynamicDefaultColumns D ON D.numFieldID = A.numFormFieldID
			WHERE   A.numFormID = 15
					AND D.bitInResults = 1
					AND D.bitDeleted = 0
					AND D.numDomainID = @numDomainID 
					AND D.numFormID = 15
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 0
			UNION
			SELECT  
				A.numFormFieldID,
				1 bitCustomField,
				A.tintOrder,
				A.intColumnWidth
			FROM    AdvSerViewConf A
					JOIN CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
			WHERE   A.numFormID = 15
					AND C.numDomainID = @numDomainID 
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 1
		) T1
		ORDER BY
			 tintOrder
	END

	-- IF USER HASN'T CONFIGURED COLUMNS FROM SEARCH RESULT GRID THEN RETURN DEFAULT COLUMNS
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT  
			numFieldID,
			0,
			1,
			0
		FROM    
			View_DynamicDefaultColumns
		WHERE   
			numFormID = 15
			AND numDomainID = @numDomainID
			AND numFieldID = 96
	END

DECLARE @bitCustom BIT
DECLARE @numFieldGroupID AS INT
DECLARE @vcColumnName AS VARCHAR(200)
DECLARE @numFieldID AS NUMERIC(18,0)

	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFormFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@vcLookBackTableName=vcLookBackTableName,
		@bitCustom=bitCustom,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			D.numFieldID AS numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D                               
		INNER JOIN
			@TEMPSelectedColumns T1
		ON
			D.numFieldID = T1.numFieldID                       
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 15 
			AND ISNULL(T1.bitCustomField,0) = 0
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			Fld_id AS numFormFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',
			1 AS bitCustom,
			Grp_id
		FROM    
			CFW_Fld_Master C
		INNER JOIN
			@TEMPSelectedColumns T1
		ON
			C.Fld_id = T1.numFieldID
		WHERE   
			C.numDomainID = @numDomainID
			AND ISNULL(T1.bitCustomField,0) = 1
	) T1
	ORDER BY 
		tintOrder asc                                                             

while @tintOrder>0                                  
begin                                  
	IF @bitCustom = 0
	BEGIN
		DECLARE  @Prefix  AS VARCHAR(10)

		IF @vcLookBackTableName = 'OpportunityMaster'
			SET @PreFix = 'OppMas.'

	 SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'
		if @vcAssociatedControlType='SelectBox'                                  
        begin                                  
                                                                              
			IF @vcDbColumnName = 'tintSource'
            BEGIN
              SET @strSql = @strSql
                                  + ',dbo.fn_GetOpportunitySourceValue(ISNULL(OppMas.tintSource,0),ISNULL(OppMas.tintSourceType,0),OppMas.numDomainID) '
                                  +' ['+ @vcColumnName+']'
				
            END
			ELSE IF @vcListItemType = 'U'
			BEGIN
				SET @strSql = @strSql + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
			END                                    
			ELSE if @vcListItemType='LI'                                   
			begin   
				IF @numListID=40--Country
				BEGIN
	  				set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'++' ['+ @vcColumnName+']'                              
				IF @vcDbColumnName ='numBillCountry'
				BEGIN
					IF @bitBillAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
					
						SET @bitBillAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD1.numCountry'
				END
				ELSE IF @vcDbColumnName ='numShipCountry'
				BEGIN
					IF @bitShipAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
					
						SET @bitShipAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD2.numCountry'
				END
			  END                        
			  ELSE
			  BEGIN                               
				set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'
				
				if @vcDbColumnName = 'numSalesOrPurType' AND @numListID=46
					set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=OppMas.numSalesOrPurType and OppMas.tintOppType = 2 '                                 		
				else if @vcDbColumnName = 'numSalesOrPurType' AND @numListID=45
					set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=OppMas.numSalesOrPurType and OppMas.tintOppType = 1 '                                 
				else
					set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                  
			  END                                  
        END                      
		END 
		ELSE IF @vcAssociatedControlType='ListBox'   
		BEGIN
			if @vcListItemType='S'                               
			  begin                         
				set @strSql=@strSql+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName+']'
				IF @vcDbColumnName ='numBillState'
				BEGIN
					IF @bitBillAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
						SET @bitBillAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD1.numState'
				END
				ELSE IF @vcDbColumnName ='numShipState'
				BEGIN
					IF @bitShipAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
						SET @bitShipAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD2.numState'
				END
			  end  
		END  
		else if @vcAssociatedControlType = 'CheckBox'           
		   begin            
               
			set @strSql= @strSql+',case when isnull('+ @vcDbColumnName +',0)=0 then ''No'' when isnull('+ @vcDbColumnName +',0)=1 then ''Yes'' end  ['+ @vcColumnName+']'              
 
		   end                                  
		 else 
				BEGIN
					IF @bitIsSearchBizDoc = 0
					BEGIN
						IF @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName != 'monDealAmount' AND @vcDbColumnName != 'monAmountPaid'
						  BEGIN
	  							SET @vcDbColumnName = '''''' --include OpportunityBizDocs tables column as blank string
						  END	
					END
	  
					set @strSql=@strSql+','+ 
						case  
							when @vcLookBackTableName = 'OpportunityMaster' AND @vcDbColumnName='monDealAmount' then 'OppMas.monDealAmount' 
							when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'   
							when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' 
							when @vcDbColumnName='vcProgress' then  ' dbo.fn_OppTotalProgress(OppMas.numOppId)'
							when @vcDbColumnName='intPEstimatedCloseDate' or @vcDbColumnName='dtDateEntered' or @vcDbColumnName='bintShippedDate'  then  
									'dbo.FormatedDateFromDate('+@vcDbColumnName+','+convert(varchar(10),@numDomainId)+')'  
							WHEN @vcDbColumnName = 'CalAmount' THEN '[dbo].[getdealamount](OppMas.numOppId,Getutcdate(),0)'
							WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(BD.monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OppMas.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
							WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(BD.monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OppMas.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
							else @vcDbColumnName end+ ' ['+ @vcColumnName+']'
				END
    END
	ELSE IF @bitCustom = 1
	BEGIN
		--SET @vcColumnName= CONCAT(@vcFormFieldName,'~','Cust',@numFieldId)
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'  
		IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'            
		BEGIN
			SET @strSql= @strSql+',CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
				
			SET @WhereCondition= @WhereCondition 
								+' left Join CFW_Fld_Values_Opp CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId=OppMas.numOppId '                                                         
		END   
		ELSE IF @vcAssociatedControlType = 'CheckBox'       
		BEGIN
			set @strSql= @strSql+',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0)='''' then 0 else  isnull(CFW'
											+ CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0) end ['+ @vcColumnName + ']'
 
			set @WhereCondition= @WhereCondition +' left Join CFW_Fld_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '             
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=OppMas.numOppId '                                                     
		END                
		ELSE IF @vcAssociatedControlType = 'DateField'            
		BEGIN
			set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
			set @WhereCondition= @WhereCondition +' left Join CFW_Fld_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '                 
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=OppMas.numOppId '
		END                
		ELSE IF @vcAssociatedControlType = 'SelectBox'             
		BEGIN
			set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
			set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
			set @WhereCondition= @WhereCondition +' left Join CFW_Fld_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '                 
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=OppMas.numOppId '
			set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
		END         
		ELSE IF @vcAssociatedControlType = 'CheckBoxList'
		BEGIN
			SET @strSql= @strSql+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

			SET @WhereCondition= @WhereCondition 
								+' left Join CFW_Fld_Values_Opp CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId=OppMas.numOppId '
	END
	END            
                                          
	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFormFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@vcLookBackTableName=vcLookBackTableName,
		@bitCustom=bitCustom,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			D.numFieldID AS numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D                               
		INNER JOIN
			@TEMPSelectedColumns T1
		ON
			D.numFieldID = T1.numFieldID                       
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 15 
			AND ISNULL(T1.bitCustomField,0) = 0
			AND T1.tintOrder > @tintOrder-1
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			Fld_id AS numFormFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',
			1 AS bitCustom,
			Grp_id
		FROM    
			CFW_Fld_Master C
		INNER JOIN
			@TEMPSelectedColumns T1
		ON
			C.Fld_id = T1.numFieldID
		WHERE   
			C.numDomainID = @numDomainID
			AND ISNULL(T1.bitCustomField,0) = 1
			AND T1.tintOrder > @tintOrder-1
	) T1
	ORDER BY 
		tintOrder asc                                   

 if @@rowcount=0 set @tintOrder=0                                  
end   
	declare @firstRec as integer                                                                        
	declare @lastRec as integer                                                                        
	set @firstRec= (@CurrentPage-1) * @PageSize                          
	set @lastRec= (@CurrentPage*@PageSize+1)                                                                         
	set @TotRecs=(select count(*) from #tempTable)   


DECLARE @from VARCHAR(8000) 
set @from = ' from OpportunityMaster OppMas   
Join AdditionalContactsinformation ADC on OppMas.numContactId = ADC.numContactId  
join DivisionMaster DM on Dm.numDivisionId = OppMas.numDivisionId  
JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId '

IF @bitIsSearchBizDoc = 1 --Added to eliminate duplicate records 
 --Added By Sachin Sadhu||Date:12thJune2014
  --Purpose:To add Functionaly :Search with Notes Field-OpportunityBizDocItems

	IF CHARINDEX('vcNotes', @WhereCondition) > 0
		BEGIN
	  		SET @from = @from + ' left join OpportunityBizDocs on OppMas.numOppId = OpportunityBizDocs.numOppId left join OpportunityBizDocItems on OpportunityBizDocItems.numOppBizDocID=OpportunityBizDocs.numOppBizDocsId '
		END
	ELSE
		BEGIN
			SET @from = @from + ' left join OpportunityBizDocs on OppMas.numOppId = OpportunityBizDocs.numOppId '
		END
	--End of Script

SET @strSql=@strSql+@from;

 
 
 if LEN(@strMassUpdate)>1
 begin     
	Declare @strReplace as varchar(2000)
    set @strReplace = case when @LookTable='OppMas' OR @LookTable='Opportunit' then 'OppMas.numOppID' ELSE '' end  + @from
   
    set @strMassUpdate=replace(@strMassUpdate,'@$replace',@strReplace)
    PRINT @strMassUpdate
   exec (@strMassUpdate)         
 end         




SET @strSql=@strSql+ @WhereCondition


	SET @strSql = @strSql +' join #tempTable T on T.numOppID=OppMas.numOppID WHERE 1=1 ' 
	
	IF ISNULL(@GetAll,0)=0
	BEGIN
		SET @strSql = @strSql +' AND ID > '+convert(varchar(10),@firstRec)+ ' and ID <'+ convert(varchar(10),@lastRec)
	END

	
 IF @bitIsSearchBizDoc = 1 
 BEGIN
 	SET @strSql = @strSql + '  and T.numOppBizDocID = OpportunityBizDocs.numOppBizDocsID '
 END
 
 SET @strSql = @strSql + ' order by ID'
print @strSql
exec(@strSql)                                                                     
drop table #tempTable

 

	SELECT 
 		tintOrder,
		numFormFieldID,
		vcDbColumnName,
		vcFieldName,
		vcAssociatedControlType,
		vcListItemType,
		numListID,
		vcLookBackTableName,
		numFieldID,
		intColumnWidth,
		bitCustom as bitCustomField,
		numGroupID,
		vcOrigDbColumnName,
		bitAllowSorting,
		bitAllowEdit,
		ListRelID,
		bitAllowFiltering,
		vcFieldDataType
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder, 
			D.numFieldID numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			D.numFieldID,
			ISNULL(T1.intColumnWidth,0) AS intColumnWidth,
			ISNULL(D.bitCustom,0) AS bitCustom,
			0 AS numGroupID
			,vcOrigDbColumnName
			,bitAllowSorting,
			bitAllowEdit,
			ListRelID,
			bitAllowFiltering,
			vcFieldDataType
		FROM 
			View_DynamicDefaultColumns D                               
		INNER JOIN
			@TEMPSelectedColumns T1
		ON
			D.numFieldID = T1.numFieldID
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 15 
			AND ISNULL(T1.bitCustomField,0) = 0
			AND T1.tintOrder > @tintOrder-1
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			Fld_id numFormFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',
			Fld_id,
			ISNULL(T1.intColumnWidth,0) AS intColumnWidth,
			1 AS bitCustom,
			Grp_id
			,'',
			null,
			null,
			null,
			null,
			''
		FROM    
			CFW_Fld_Master C 
		INNER JOIN
			@TEMPSelectedColumns T1
		ON
			C.Fld_id = T1.numFieldID
		WHERE   
			C.numDomainID = @numDomainID
			AND ISNULL(T1.bitCustomField,0) = 1
			AND T1.tintOrder > @tintOrder-1
	) T1
	ORDER BY 
		tintOrder asc  

--Left Join OpportunityItems OppItems on OppItems.numOppId = OppMas.numOppId   
--left join Item on Item.numItemCode = OppItems.numItemCode  
--left join WareHouseItems on Item.numItemCode = WareHouseItems.numItemId  
--left Join WareHouses WareHouse on WareHouse.numWareHouseId =WareHouseItems.numWareHouseId  
--left Join WareHouseItmsDTL WareHouseItemDTL on WareHouseItemDTL.numWareHouseItemID = WareHouse.numWareHouseId
GO

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ChartofChildAccounts' ) 
    DROP PROCEDURE USP_ChartofChildAccounts
GO
/****** Object:  StoredProcedure [dbo].[USP_ChartofChildAccounts]    Script Date: 09/25/2009 16:21:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Ajit Kumar Singh>  
-- Create date: <Friday, July 25, 2008>  
-- Description: <This procedure is use for fetching the Child Account records against Parent Account Id>  
-- =============================================  
-- exec USP_ChartofChildAccounts @numDomainId=169,@numUserCntId=1,@strSortOn='DESCRIPTION',@strSortDirection='ASCENDING'
CREATE PROCEDURE [dbo].[USP_ChartofChildAccounts]
    @numDomainId AS NUMERIC(9) = 0,
    @numUserCntId AS NUMERIC(9) = 0,
    @strSortOn AS VARCHAR(11) = NULL,
    @strSortDirection AS VARCHAR(10) = NULL
AS 
    BEGIN    
		
		-- GETTING P&L VALUE
		SELECT * INTO #view_journal FROM view_journal WHERE numDomainID=@numDomainId ;

		DECLARE @dtFinYearFrom DATETIME ;
		DECLARE @dtFinYearTo DATETIME ;
		SET @dtFinYearFrom = ( SELECT   dtPeriodFrom
                        FROM     FINANCIALYEAR
                        WHERE    dtPeriodFrom <= GETDATE()
                                AND dtPeriodTo >= GETDATE()
                                AND numDomainId = @numDomainId
                        ) ;
		PRINT @dtFinYearFrom
		SELECT @dtFinYearTo = MAX(datEntry_Date) FROM #view_journal WHERE numDomainID=@numDomainId
		PRINT @dtFinYearTo

		CREATE TABLE #PLSummary (numAccountId numeric(9),vcAccountName varchar(250),
		numParntAcntTypeID numeric(9),vcAccountDescription varchar(250),
		vcAccountCode varchar(50) COLLATE Database_Default,Opening DECIMAL(20,5),Debit DECIMAL(20,5),Credit DECIMAL(20,5),bitIsSubAccount Bit);

		INSERT INTO  #PLSummary
		SELECT COA.numAccountId,COA.vcAccountName,COA.numParntAcntTypeID,COA.vcAccountDescription,COA.vcAccountCode,
		0 AS OPENING,
		ISNULL(SUM(Debit),0) as DEBIT,
		ISNULL(SUM(Credit),0) as CREDIT,
		ISNULL(COA.bitIsSubAccount,0)
		FROM 
			Chart_of_Accounts COA
		LEFT JOIN 
			#view_journal VJ 
		ON 
			VJ.numDomainId=@numDomainId 
			AND VJ.COAvcAccountCode like COA.vcAccountCode + '%'
			AND datEntry_Date BETWEEN  @dtFinYearFrom AND @dtFinYearTo
		WHERE 
			COA.numDomainId=@numDomainId 
			AND COA.bitActive = 1 
			AND (COA.vcAccountCode LIKE '0103%' OR COA.vcAccountCode LIKE '0104%' OR COA.vcAccountCode LIKE '0106%')
		GROUP BY
			COA.numAccountId,COA.vcAccountName,COA.numParntAcntTypeID,COA.vcAccountDescription,COA.vcAccountCode,COA.bitIsSubAccount


		CREATE TABLE #PLOutPut (numAccountId numeric(9),vcAccountName varchar(250),
		numParntAcntTypeID numeric(9),vcAccountDescription varchar(250),
		vcAccountCode varchar(50) COLLATE Database_Default,Opening DECIMAL(20,5),Debit DECIMAL(20,5),Credit DECIMAL(20,5));

		INSERT INTO #PLOutPut
		SELECT ATD.numAccountTypeID,ATD.vcAccountType,ATD.numParentID, '',ATD.vcAccountCode,
		 ISNULL(SUM(Opening),0) as Opening,
		ISNUlL(Sum(Debit),0) as Debit,ISNULL(Sum(Credit),0) as Credit
		FROM 
		 AccountTypeDetail ATD RIGHT OUTER JOIN 
		#PLSummary PL ON
		PL.vcAccountCode LIKE ATD.vcAccountCode + '%'
		AND ATD.numDomainId=@numDomainId AND
		(ATD.vcAccountCode LIKE '0103%' OR
			   ATD.vcAccountCode LIKE '0104%' OR
			   ATD.vcAccountCode LIKE '0106%')
		WHERE 
		PL.bitIsSubAccount=0
		GROUP BY 
		ATD.numAccountTypeID,ATD.vcAccountCode,ATD.vcAccountType,ATD.numParentID;


		DECLARE @CURRENTPL DECIMAL(20,5) ;
		DECLARE @PLOPENING DECIMAL(20,5);
		DECLARE @PLCHARTID NUMERIC(8)
		DECLARE @TotalIncome DECIMAL(20,5);
		DECLARE @TotalExpense DECIMAL(20,5);
		DECLARE @TotalCOGS DECIMAL(20,5);
		DECLARE  @CurrentPL_COA DECIMAL(20,5)
		SET @CURRENTPL =0;	
		SET @PLOPENING=0;

		SELECT @PLCHARTID=COA.numAccountId FROM Chart_of_Accounts COA WHERE numDomainID=@numDomainId and
		bitProfitLoss=1;

		SELECT @CurrentPL_COA =  ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID
		AND numAccountId=@PLCHARTID AND datEntry_Date between  @dtFinYearFrom AND @dtFinYearTo;

		SELECT  @TotalIncome= ISNULL(sum(Opening),0) + ISNULL(sum(Credit),0) -ISNULL(sum(Debit),0) FROM
		#PLOutPut P WHERE 
		vcAccountCode IN ('0103')

		SELECT  @TotalExpense=ISNULL(sum(Opening),0)+ ISNULL(sum(Credit),0) -ISNULL(sum(Debit),0) FROM
		#PLOutPut P WHERE 
		vcAccountCode IN ('0104')

		SELECT  @TotalCOGS= ISNULL(sum(Opening),0) + ISNULL(sum(Credit),0) -ISNULL(sum(Debit),0) FROM
		#PLOutPut P WHERE 
		vcAccountCode IN ('0106')

		PRINT 'CurrentPL_COA = ' + CAST(@CurrentPL_COA AS VARCHAR(20))
		PRINT 'TotalIncome = ' + CAST(@TotalIncome AS VARCHAR(20))
		PRINT 'TotalExpense = ' + CAST(@TotalExpense AS VARCHAR(20))
		PRINT 'TotalCOGS = ' + CAST(@TotalCOGS AS VARCHAR(20))

		SELECT @CURRENTPL = @CurrentPL_COA + @TotalIncome + @TotalExpense + @TotalCOGS 
		PRINT 'Current Profit/Loss = (Income - expense - cogs)= ' + CAST( @CURRENTPL AS VARCHAR(20))

		SET @CURRENTPL=@CURRENTPL * (-1)

		SELECT @PLOPENING = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID 
		AND ( vcAccountCode  LIKE '0103%' or  vcAccountCode  LIKE '0104%' or  vcAccountCode  LIKE '0106%')
		AND datEntry_Date <=  DATEADD(MINUTE,0,@dtFinYearFrom);

		SELECT @PLOPENING = ISNULL(@PLOPENING,0) + ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID
		AND numAccountId=@PLCHARTID AND datEntry_Date <=  DATEADD(MINUTE,0,@dtFinYearFrom);

		SET @CURRENTPL=@CURRENTPL * (-1)

		PRINT '@PLOPENING = ' + CAST(@PLOPENING AS VARCHAR(20))
		PRINT '@CURRENTPL = ' + CAST(@CURRENTPL AS VARCHAR(20))
		PRINT '-(@PLOPENING + @CURRENTPL) = ' + CAST((@PLOPENING + @CURRENTPL) AS VARCHAR(20))

        IF ( @strSortOn = 'ID'
             AND @strSortDirection = 'ASCENDING'
           ) 
            BEGIN  

                SELECT  c.numAccountId,
                        [numAccountTypeID],
                        c.[numParntAcntTypeId],
                        c.[vcAccountCode],
                        ISNULL(c.[vcAccountCode], '') + ' ~ '
                        + c.vcAccountName AS vcAccountName,
                        c.vcAccountName AS vcAccountName1,
                        AT.[vcAccountType],
                        CASE WHEN (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%') THEN ISNULL(t1.[OPENING],0)
							 WHEN c.[vcAccountCode] = '0105010101' THEN -(@PLOPENING + @CURRENTPL)
							 ELSE (SELECT  SUM(COA.numOpeningBal) FROM dbo.Chart_Of_Accounts COA 
								   WHERE COA.numDomainId=@numDomainId 
								   AND (COA.vcAccountCode LIKE c.vcAccountCode OR (COA.vcAccountCode LIKE c.vcAccountCode + '%' AND COA.numParentAccID>0)))
						END AS [numOpeningBal],	 
                        ISNULL(c.numParentAccId,0) AS [numParentAccId],
                        COA.vcAccountName AS [vcParentAccountName],
                        (SELECT MAX(intLevel) FROM dbo.Chart_Of_Accounts) AS [intMaxLevel],
                        ISNULL(c.intLevel,1) AS [intLevel]
						,ISNULL(c.bitActive,0) bitActive
                FROM    Chart_Of_Accounts c
				LEFT OUTER JOIN [AccountTypeDetail] AT ON AT.[numAccountTypeID] = c.[numParntAcntTypeId]
				LEFT OUTER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = c.numParentAccId
				OUTER APPLY (SELECT   SUM(Debit - Credit) AS OPENING
                                   FROM     #view_journal VJ
                                   WHERE    VJ.numDomainId = @numDomainId
                                            AND VJ.numAccountID = c.numAccountID 
											AND (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%')
                                            AND datEntry_Date BETWEEN @dtFinYearFrom AND  @dtFinYearTo) AS t1
                WHERE   c.numDomainId = @numDomainId
                --AND c.bitActive = 0
                ORDER BY numAccountID ASC  
  
                SELECT  c.numAccountId,
						[numAccountTypeID],
						c.[numParntAcntTypeId],
						c.[vcAccountCode],
						ISNULL(c.[vcAccountCode], '') + ' ~ ' + c.vcAccountName AS vcAccountName,
						c.vcAccountName AS vcAccountName1,
						AT.[vcAccountType],
						CASE WHEN c.[numParntAcntTypeId] IS NULL THEN NULL
							 ELSE CASE WHEN (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%') THEN ISNULL(t1.[OPENING],0)
									   WHEN c.[vcAccountCode] = '0105010101' THEN -(@PLOPENING + @CURRENTPL)	
									   ELSE (SELECT  SUM(COA.numOpeningBal) FROM dbo.Chart_Of_Accounts COA 
											 WHERE COA.numDomainId=@numDomainId 
											 AND (COA.vcAccountCode LIKE c.vcAccountCode OR (COA.vcAccountCode LIKE c.vcAccountCode + '%' AND COA.numParentAccID>0))) 
								  END
						END AS numOpeningBal,
						ISNULL(c.numParentAccId,0) AS [numParentAccId],
						COA.vcAccountName AS [vcParentAccountName],
						(SELECT MAX(intLevel) FROM dbo.Chart_Of_Accounts) AS [intMaxLevel],
						ISNULL(c.intLevel,1) AS [intLevel],
						ISNULL(c.IsConnected,0) AS [IsConnected]                        
						,ISNULL(c.numBankDetailID,0) numBankDetailID
						,ISNULL(c.bitActive,0) bitActive
				FROM    Chart_Of_Accounts c
						LEFT OUTER JOIN [AccountTypeDetail] AT ON AT.[numAccountTypeID] = c.[numParntAcntTypeId]
						LEFT OUTER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = c.numParentAccId
						OUTER APPLY (SELECT   SUM(Debit - Credit) AS OPENING
                                   FROM     #view_journal VJ
                                   WHERE    VJ.numDomainId = @numDomainId
                                            AND VJ.numAccountID = c.numAccountID 
											AND (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%')
                                            AND datEntry_Date BETWEEN @dtFinYearFrom AND  @dtFinYearTo) AS t1
                WHERE   c.numDomainId = @numDomainID
                --AND c.bitActive = 0
                ORDER BY c.numAccountID ASC
            END  
  
        IF ( @strSortOn = 'DESCRIPTION'
             AND @strSortDirection = 'ASCENDING'
           ) 
            BEGIN  
                
                SELECT  c.numAccountId,
						[numAccountTypeID],
						c.[numParntAcntTypeId],
						c.[vcAccountCode],
						ISNULL(c.[vcAccountCode], '') + ' ~ ' + c.vcAccountName AS vcAccountName,
						c.vcAccountName AS vcAccountName1,
						AT.[vcAccountType],
						CASE WHEN c.[numParntAcntTypeId] IS NULL THEN NULL
							 ELSE CASE WHEN (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%') THEN ISNULL(t1.[OPENING],0)
									   WHEN c.[vcAccountCode] = '0105010101' THEN -(@PLOPENING + @CURRENTPL)
								       ELSE (SELECT SUM(COA.numOpeningBal) FROM dbo.Chart_Of_Accounts COA 
											 WHERE COA.numDomainId=@numDomainId 
											 AND (COA.vcAccountCode LIKE c.vcAccountCode OR (COA.vcAccountCode LIKE c.vcAccountCode + '%' AND COA.numParentAccID>0))) 
								  END
						END AS numOpeningBal,
						ISNULL(c.numParentAccId,0) AS [numParentAccId],
						COA.vcAccountName AS [vcParentAccountName],
						(SELECT MAX(intLevel) FROM dbo.Chart_Of_Accounts) AS [intMaxLevel],
						ISNULL(c.intLevel,1) AS [intLevel],
						ISNULL(c.IsConnected,0) AS [IsConnected]                      
						,ISNULL(c.numBankDetailID,0) numBankDetailID
						,ISNULL(c.bitActive,0) bitActive
				FROM    Chart_Of_Accounts c
						LEFT OUTER JOIN [AccountTypeDetail] AT ON AT.[numAccountTypeID] = c.[numParntAcntTypeId]
						LEFT OUTER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = c.numParentAccId
						OUTER APPLY (SELECT   SUM(Debit - Credit) AS OPENING
                                   FROM     #view_journal VJ
                                   WHERE    VJ.numDomainId = @numDomainId
                                            AND VJ.numAccountID = c.numAccountID 
											AND (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%')
                                            AND datEntry_Date BETWEEN @dtFinYearFrom AND  @dtFinYearTo) AS t1

                WHERE   c.numDomainId = @numDomainId
                    AND c.[numParntAcntTypeId] IS NULL
                    --AND c.bitActive = 0
                ORDER BY AT.vcAccountCode, case IsNumeric(c.vcNumber) when 1 then Replicate(0, 50 - Len(c.vcNumber)) + c.vcNumber else Replicate(9,50) end  ASC
  
                SELECT  c.numAccountId,
						[numAccountTypeID],
						c.[numParntAcntTypeId],
						c.[vcAccountCode],
						ISNULL(c.[vcAccountCode], '') + ' ~ ' + c.vcAccountName AS vcAccountName,
						c.vcAccountName AS vcAccountName1,
						AT.[vcAccountType],
						CASE WHEN c.[numParntAcntTypeId] IS NULL THEN NULL
							 ELSE  CASE WHEN (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%') THEN ISNULL(t1.[OPENING],0)
									    WHEN c.[vcAccountCode] = '0105010101' THEN -(@PLOPENING + @CURRENTPL)
										ELSE (SELECT  SUM(COA.numOpeningBal) FROM dbo.Chart_Of_Accounts COA WHERE COA.numDomainId=@numDomainId AND (COA.vcAccountCode LIKE c.vcAccountCode OR (COA.vcAccountCode LIKE c.vcAccountCode + '%' AND COA.numParentAccID>0))) END
						END AS numOpeningBal,
						ISNULL(c.numParentAccId,0) AS [numParentAccId],
						COA.vcAccountName AS [vcParentAccountName],
						(SELECT MAX(intLevel) FROM dbo.Chart_Of_Accounts) AS [intMaxLevel],
						ISNULL(c.intLevel,1) AS [intLevel],
						ISNULL(c.IsConnected,0) AS [IsConnected]
						,ISNULL(c.numBankDetailID,0) numBankDetailID
						,ISNULL(c.bitActive,0) bitActive
				FROM    Chart_Of_Accounts c
						LEFT OUTER JOIN [AccountTypeDetail] AT ON AT.[numAccountTypeID] = c.[numParntAcntTypeId]
						LEFT OUTER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = c.numParentAccId
						OUTER APPLY (SELECT   SUM(Debit - Credit) AS OPENING
                                   FROM     #view_journal VJ
                                   WHERE    VJ.numDomainId = @numDomainId
                                            AND VJ.numAccountID = c.numAccountID 
											AND (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%')
                                            AND datEntry_Date BETWEEN @dtFinYearFrom AND  @dtFinYearTo) AS t1
                WHERE   c.numDomainId = @numDomainID
                        AND c.[numParntAcntTypeId] IS NOT NULL
                        --AND c.bitActive = 0
                ORDER BY AT.vcAccountCode, case IsNumeric(c.vcNumber) when 1 then Replicate(0, 50 - Len(c.vcNumber)) + c.vcNumber else Replicate(9,50) end  ASC
                
            END  
    END
	
--Created by anoop jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_checkuseratlogin')
DROP PROCEDURE usp_checkuseratlogin
GO
CREATE PROCEDURE [dbo].[USP_CheckUserAtLogin]                                                              
(                                                                                    
	@UserName as varchar(100)='',                                                                        
	@vcPassword as varchar(100)='',
	@vcLinkedinId as varchar(300)=''                                                                           
)                                                              
AS             
BEGIN
	DECLARE @listIds VARCHAR(MAX)

	SELECT 
		@listIds = COALESCE(@listIds+',' ,'') + CAST(UDTL.numUserId AS VARCHAR(100)) 
	FROM 
		UnitPriceApprover U
	Join 
		Domain D                              
	on 
		D.numDomainID=U.numDomainID
	Join  
		Subscribers S                            
	on 
		S.numTargetDomainID=D.numDomainID
	LEFT JOIN 
		UserMaster As UDTL
	ON
		U.numUserID=UDTL.numUserDetailId
	WHERE 
		1 = (CASE 
			WHEN @vcLinkedinId='N' THEN (CASE WHEN UDTL.vcEmailID=@UserName and UDTL.vcPassword=@vcPassword THEN 1 ELSE 0 END)
			ELSE (CASE WHEN UDTL.vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
			END)
       

	DECLARE @bitIsBusinessPortalUser AS BIT = 0

	DECLARE @numUserCount INT = ISNULL((SELECT 
			COUNT(*)
		FROM 
			UserMaster U                              
		JOIN 
			Domain D                              
		ON 
			D.numDomainID=U.numDomainID
		JOIN 
			Subscribers S                            
		ON 
			S.numTargetDomainID=D.numDomainID
		WHERE 
			1 = (CASE 
													WHEN @vcLinkedinId='N' 
													THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
					ELSE (CASE WHEN vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
				END) 
											AND bitActive=1),0)

	IF @numUserCount > 1
	BEGIN
		RAISERROR('MULTIPLE_RECORDS_WITH_SAME_EMAIL',16,1)
		RETURN;
	END   
	ELSE IF @numUserCount=1 AND EXISTS (SELECT 
											A.numContactId 
										FROM 
											AdditionalContactsInformation A                                  
										INNER JOIN 
											ExtranetAccountsDtl E                                  
										ON 
											A.numContactID=E.numContactID
										INNER JOIN
											ExtarnetAccounts EA
										ON
											E.numExtranetID = EA.numExtranetID
										WHERE 
											(E.vcUserName=@UserName OR vcemail=@UserName) 
											AND (vcPassword=@vcPassword) 
											AND bitPartnerAccess=1)
	BEGIN
		RAISERROR('MULTIPLE_RECORDS_WITH_SAME_EMAIL',16,1)
		RETURN;
	END
	ELSE IF @numUserCount = 1
	BEGIN
		SET @bitIsBusinessPortalUser = 0
	END
	ELSE IF @numUserCount = 0 AND (SELECT 
										COUNT(A.numContactId)
									FROM 
										AdditionalContactsInformation A                                  
									INNER JOIN 
										ExtranetAccountsDtl E                                  
									ON 
										A.numContactID=E.numContactID
									INNER JOIN
										ExtarnetAccounts EA
									ON
										E.numExtranetID = EA.numExtranetID                              
									WHERE 
										(E.vcUserName=@UserName OR vcemail=@UserName) 
										AND (vcPassword=@vcPassword) 
										AND bitPartnerAccess=1) > 1
	BEGIN
		RAISERROR('MULTIPLE_RECORDS_WITH_SAME_EMAIL',16,1)
		RETURN;
	END   
	ELSE IF EXISTS (SELECT 
						A.numContactId 
					FROM 
						AdditionalContactsInformation A                                  
					INNER JOIN 
						ExtranetAccountsDtl E                                  
					ON 
						A.numContactID=E.numContactID
					INNER JOIN
						ExtarnetAccounts EA
					ON
						E.numExtranetID = EA.numExtranetID                             
					WHERE 
						(E.vcUserName=@UserName OR vcemail=@UserName) 
						AND (vcPassword=@vcPassword) 
						AND bitPartnerAccess=1)
	BEGIN
		SET @bitIsBusinessPortalUser = 1
	END
	                                          
	/*check subscription validity */
	IF EXISTS(SELECT 
					*
			FROM 
				UserMaster U                              
			JOIN 
				Domain D                              
			ON 
				D.numDomainID=U.numDomainID
			JOIN 
				Subscribers S                            
			ON 
				S.numTargetDomainID=D.numDomainID
			WHERE 
				1 = (CASE 
						WHEN @vcLinkedinId='N' THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
						ELSE (CASE WHEN vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
					END) 
				AND bitActive=1 
				AND getutcdate() > dtSubEndDate)
	BEGIN
		RAISERROR('SUB_RENEW',16,1)
		RETURN;
	END

	IF EXISTS(SELECT 
					*
			FROM 
				AdditionalContactsInformation A                                  
			INNER JOIN 
				ExtranetAccountsDtl E                                  
			ON 
				A.numContactID=E.numContactID
			INNER JOIN
				ExtarnetAccounts EA
			ON
				E.numExtranetID = EA.numExtranetID                          
			JOIN 
				Domain D                              
			ON 
				D.numDomainID=E.numDomainID
			JOIN 
				Subscribers S                            
			ON 
				S.numTargetDomainID=D.numDomainID
			WHERE 
				(E.vcUserName=@UserName OR A.vcemail=@UserName) 
				AND (vcPassword=@vcPassword) 
				AND bitPartnerAccess=1
				AND getutcdate() > dtSubEndDate)
	BEGIN
		RAISERROR('SUB_RENEW',16,1)
		RETURN;
	END

	IF @bitIsBusinessPortalUser = 1
	BEGIN
		SELECT TOP 1 
			0 numUserID
			,EAD.numExtranetDtlID
			,A.numContactId AS numUserDetailId
			,D.numCost
			,isnull(A.vcEmail,'') vcEmailID
			,'' vcEmailAlias
			,'' vcEmailAliasPassword
			,isnull(numGroupID,0) numGroupID
			,1 AS bitActivateFlag
			,'' vcMailNickName
			,'' txtSignature
			,isnull(EAD.numDomainID,0) numDomainID
			,isnull(EA.numDivisionID,0) numDivisionID
			,isnull(vcCompanyName,'') vcCompanyName
			,0 bitExchangeIntegration
			,0 bitAccessExchange
			,'' vcExchPath
			,'' vcExchDomain
			,'' vcExchUserName
			,isnull(vcFirstname,'') + ' ' + isnull(vcLastName,'') as ContactName
			,'' as vcExchPassword
			,tintCustomPagingRows
			,vcDateFormat
			,numDefCountry
			,tintComposeWindow
			,dateadd(day,-sintStartDate,getutcdate()) as StartDate
			,dateadd(day,sintNoofDaysInterval,dateadd(day,-sintStartDate,getutcdate())) as EndDate
			,tintAssignToCriteria
			,bitIntmedPage
			,tintFiscalStartMonth
			,numAdminID
			,isnull(A.numTeam,0) numTeam
			,isnull(D.vcCurrency,'') as vcCurrency
			,isnull(D.numCurrencyID,0) as numCurrencyID, 
			isnull(D.bitMultiCurrency,0) as bitMultiCurrency,              
			bitTrial,isnull(tintChrForComSearch,0) as tintChrForComSearch,  
			isnull(tintChrForItemSearch,0) as tintChrForItemSearch,              
			'' as vcSMTPServer  ,          
			0 as numSMTPPort      
			,0 bitSMTPAuth    
			,'' vcSmtpPassword    
			,0 bitSMTPSSL
			,0 bitSMTPServer,       
			0 bitImapIntegration, isnull(charUnitSystem,'E')   UnitSystem, datediff(day,getutcdate(),dtSubEndDate) as NoOfDaysLeft,
			ISNULL(bitCreateInvoice,0) bitCreateInvoice,
			ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
			ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
			vcDomainName,
			S.numSubscriberID,
			D.[tintBaseTax],
			'' ProfilePic,
			ISNULL(D.[numShipCompany],0) numShipCompany,
			ISNULL(D.[bitEmbeddedCost],0) bitEmbeddedCost,
			(Select isnull(numAuthoritativeSales,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativeSales,
			(Select isnull(numAuthoritativePurchase,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativePurchase,
			ISNULL(D.[numDefaultSalesOppBizDocId],0) numDefaultSalesOppBizDocId,
			ISNULL(tintDecimalPoints,2) tintDecimalPoints,
			ISNULL(D.tintBillToForPO,0) tintBillToForPO, 
			ISNULL(D.tintShipToForPO,0) tintShipToForPO,
			ISNULL(D.tintOrganizationSearchCriteria,0) tintOrganizationSearchCriteria,
			ISNULL(D.tintLogin,0) tintLogin,
			ISNULL(S.intFullUserConcurrency,0) intFullUserConcurrency,
			ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
			ISNULL(D.vcPortalName,'') vcPortalName,
			(SELECT COUNT(*) FROM dbo.Subscribers WHERE getutcdate() between dtSubStartDate and dtSubEndDate and bitActive=1),
			ISNULL(D.bitGtoBContact,0) bitGtoBContact,
			ISNULL(D.bitBtoGContact,0) bitBtoGContact,
			ISNULL(D.bitGtoBCalendar,0) bitGtoBCalendar,
			ISNULL(D.bitBtoGCalendar,0) bitBtoGCalendar,
			ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
			ISNULL(D.bitInlineEdit,0) bitInlineEdit,
			0 numDefaultClass,
			ISNULL(D.bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
			ISNULL(D.bitTrakDirtyForm,1) bitTrakDirtyForm,
			isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
			ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
			CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
			ISNULL(D.bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
			0 as tintTabEvent,
			isnull(D.bitRentalItem,0) as bitRentalItem,
			isnull(D.numRentalItemClass,0) as numRentalItemClass,
			isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
			isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
			isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
			isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
			isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
			0 numDefaultWarehouse,
			ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
			ISNULL(D.bitAutoSerialNoAssign,0) bitAutoSerialNoAssign,
			ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
			ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
			ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
			ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
			ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
			ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
			ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
			ISNULL(D.tintCommissionType,1) AS tintCommissionType,
			ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
			ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
			ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
			ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
			ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
			ISNULL(D.bitUseBizdocAmount,1) AS [bitUseBizdocAmount],
			ISNULL(D.bitDefaultRateType,1) AS [bitDefaultRateType],
			ISNULL(D.bitIncludeTaxAndShippingInCommission,1) AS [bitIncludeTaxAndShippingInCommission],
			ISNULL(D.[bitLandedCost],0) AS bitLandedCost,
			ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
			ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
			ISNULL(@listIds,'') AS vcUnitPriceApprover,
			ISNULL(D.numPODropShipBizDoc,0) AS numPODropShipBizDoc,
			ISNULL(D.numPODropShipBizDocTemplate,0) AS numPODropShipBizDocTemplate,
			ISNULL(D.IsEnableDeferredIncome,0) AS IsEnableDeferredIncome,
			ISNULL(D.bitEnableItemLevelUOM,0) AS bitEnableItemLevelUOM,
			ISNULL(D.bitPurchaseTaxCredit,0) AS bitPurchaseTaxCredit,
			NULL AS bintCreatedDate,
			(CASE WHEN LEN(ISNULL(TempDefaultPage.vcDefaultNavURL,'')) = 0 THEN ISNULL(TempDefaultTab.vcDefaultNavURL,'Items/frmItemList.aspx?Page=All Items&ItemGroup=0') ELSE ISNULL(TempDefaultPage.vcDefaultNavURL,'') END) vcDefaultNavURL,
			ISNULL(D.bitApprovalforTImeExpense,0) AS bitApprovalforTImeExpense,
			ISNULL(D.intTimeExpApprovalProcess,0) AS intTimeExpApprovalProcess,ISNULL(D.bitApprovalforOpportunity,0) AS bitApprovalforOpportunity,
			ISNULL(D.intOpportunityApprovalProcess,0) AS intOpportunityApprovalProcess,
			ISNULL(D.numDefaultSalesShippingDoc,0) AS numDefaultSalesShippingDoc,
			ISNULL(D.numDefaultPurchaseShippingDoc,0) AS numDefaultPurchaseShippingDoc,
			ISNULL((SELECT TOP 1 bitMarginPriceViolated FROM ApprovalProcessItemsClassification  WHERE numDomainID=D.numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1),0) AS bitMarginPriceViolated,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=2 AND numPageID=2),0) AS tintLeadRights,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=3 AND numPageID=2),0) AS tintProspectRights,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=4 AND numPageID=2),0) AS tintAccountRights,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=10 AND numPageID=10),0) AS tintSalesOrderListRights,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=10 AND numPageID=11),0) AS tintPurchaseOrderListRights,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=10 AND numPageID=2),0) AS tintSalesOpportunityListRights,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=10 AND numPageID=8),0) AS tintPurchaseOpportunityListRights,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=11 AND numPageID=2),0) AS tintContactListRights,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=7 AND numPageID=2),0) AS tintCaseListRights,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=12 AND numPageID=2),0) AS tintProjectListRights,
			ISNULL((SELECT TOP 1 numCriteria FROM TicklerListFilterConfiguration WHERE CHARINDEX(CONCAT(',',A.numContactId,','),CONCAT(',',vcSelectedEmployee,',')) > 0),0) AS tintActionItemRights,
			ISNULL(D.numAuthorizePercentage,0) AS numAuthorizePercentage,
			ISNULL(D.bitAllowDuplicateLineItems,0) bitAllowDuplicateLineItems,
			ISNULL(D.bitLogoAppliedToBizTheme,0) bitLogoAppliedToBizTheme,
			ISNULL(D.vcLogoForBizTheme,'') vcLogoForBizTheme,
			ISNULL(D.bitLogoAppliedToLoginBizTheme,0) bitLogoAppliedToLoginBizTheme,
			ISNULL(D.vcLogoForLoginBizTheme,'') vcLogoForLoginBizTheme,
			ISNULL(D.vcThemeClass,'1473b4') vcThemeClass,
			ISNULL(D.vcLoginURL,'') AS vcLoginURL,
			ISNULL(D.bitDisplayCustomField,0) AS bitDisplayCustomField,
			ISNULL(D.bitFollowupAnytime,0) AS bitFollowupAnytime,
			ISNULL(D.bitpartycalendarTitle,0) AS bitpartycalendarTitle,
			ISNULL(D.bitpartycalendarLocation,0) AS bitpartycalendarLocation,
			ISNULL(D.bitpartycalendarDescription,0) AS bitpartycalendarDescription,
			ISNULL(D.bitREQPOApproval,0) AS bitREQPOApproval,
			ISNULL(D.bitARInvoiceDue,0) AS bitARInvoiceDue,
			ISNULL(D.bitAPBillsDue,0) AS bitAPBillsDue,
			ISNULL(D.bitItemsToPickPackShip,0) AS bitItemsToPickPackShip,
			ISNULL(D.bitItemsToInvoice,0) AS bitItemsToInvoice,
			ISNULL(D.bitSalesOrderToClose,0) AS bitSalesOrderToClose,
			ISNULL(D.bitItemsToPutAway,0) AS bitItemsToPutAway,
			ISNULL(D.bitItemsToBill,0) AS bitItemsToBill,
			ISNULL(D.bitPosToClose,0) AS bitPosToClose,
			ISNULL(D.bitPOToClose,0) AS bitPOToClose,
			ISNULL(D.bitBOMSToPick,0) AS bitBOMSToPick,
			ISNULL(D.vchREQPOApprovalEmp,'') AS vchREQPOApprovalEmp,
			ISNULL(D.vchARInvoiceDue,'') AS vchARInvoiceDue,
			ISNULL(D.vchAPBillsDue,'') AS vchAPBillsDue,
			ISNULL(D.vchItemsToPickPackShip,'') AS vchItemsToPickPackShip,
			ISNULL(D.vchItemsToInvoice,'') AS vchItemsToInvoice,
			ISNULL(stuff((
			select ',' + CAST(UPC.numUserID AS VARCHAR(10))
			from UnitPriceApprover UPC
			where UPC.numDomainID = D.numDomainId
			order by UPC.numUserID
			for xml path('')),1,1,''),'') AS vchPriceMarginApproval,
			ISNULL(D.vchSalesOrderToClose,'') AS vchSalesOrderToClose,
			ISNULL(D.vchItemsToPutAway,'') AS vchItemsToPutAway,
			ISNULL(D.vchItemsToBill,'') AS vchItemsToBill,
			ISNULL(D.vchPosToClose,'') AS vchPosToClose,
			ISNULL(D.vchPOToClose,'') AS vchPOToClose,
			ISNULL(D.vchBOMSToPick,'') AS vchBOMSToPick,
			ISNULL(D.decReqPOMinValue,0) AS decReqPOMinValue,
			0 tintPayrollType,
			(CASE WHEN EXISTS (SELECT numCategoryHDRID FROM TimeAndExpense WHERE numUserCntID=A.numContactId AND numCategory=1 AND numType=7 AND dtToDate IS NULL) THEN 1 ELSE 0 END) bitUserClockedIn,
			ISNULL(D.bitUseOnlyActionItems,0) AS bitUseOnlyActionItems,
			ISNULL(D.vcElectiveItemFields,'') AS vcElectiveItemFields,
			ISNULL(D.bitDisplayContractElement,0) AS bitDisplayContractElement,
			ISNULL(D.vcEmployeeForContractTimeElement,'') AS vcEmployeeForContractTimeElement,
			ISNULL(D.bitEnableSmartyStreets,0) bitEnableSmartyStreets,
			ISNULL(D.vcSmartyStreetsAPIKeys,'') vcSmartyStreetsAPIKeys
		FROM 
			ExtranetAccountsDtl EAD
		INNER JOIN
			ExtarnetAccounts EA
		ON
			EAD.numExtranetID = EA.numExtranetID                                                
		INNER JOIN 
			Domain D                              
		ON 
			D.numDomainID=EAD.numDomainID
		INNER JOIN 
			AdditionalContactsInformation A                              
		ON 
			A.numContactID=EAD.numContactID
		INNER JOIN 
			Subscribers S            
		ON 
			S.numTargetDomainID=D.numDomainID 
		LEFT JOIN 
			DivisionMaster Div                                            
		ON 
			EA.numDivisionID=Div.numDivisionID                               
		LEFT JOIN 
			CompanyInfo C                              
		ON 
			C.numCompanyID=Div.numCompanyID    
		OUTER APPLY
		(
			SELECT  
				TOP 1 REPLACE(SCB.Link,'../','') vcDefaultNavURL
			FROM    
				TabMaster T
			JOIN 
				GroupTabDetails G ON G.numTabId = T.numTabId
			LEFT JOIN
				ShortCutGrpConf SCUC ON G.numTabId=SCUC.numTabId AND SCUC.numDomainId=EAD.numDomainID AND SCUC.numGroupId=EA.numGroupID AND bitDefaultTab=1
			LEFT JOIN
				ShortCutBar SCB
			ON
				SCB.Id=SCUC.numLinkId
			WHERE   
				(T.numDomainID = EAD.numDomainID OR bitFixed = 1)
				AND G.numGroupID = EA.numGroupID
				AND ISNULL(G.[tintType], 0) <> 1
				AND tintTabType =1
				AND T.numTabID NOT IN (2,68)
			ORDER BY 
				SCUC.bitInitialPage DESC
		)  TempDefaultPage 
		 OUTER APPLY
		 (
			SELECT  
				TOP 1 REPLACE(T.vcURL,'../','') vcDefaultNavURL
			FROM    
				TabMaster T
			JOIN 
				GroupTabDetails G 
			ON 
				G.numTabId = T.numTabId
			WHERE   
				(T.numDomainID = EAD.numDomainID OR bitFixed = 1)
				AND G.numGroupID = EA.numGroupID
				AND ISNULL(G.[tintType], 0) <> 1
				AND tintTabType =1
				AND T.numTabID NOT IN (2,68)
				AND ISNULL(G.bitInitialTab,0) = 1 
				AND ISNULL(bitallowed,0)=1
			ORDER BY 
				G.bitInitialTab DESC
		 ) TEMPDefaultTab                   
		WHERE 
			(EAD.vcUserName=@UserName OR A.vcemail=@UserName) 
			AND (vcPassword=@vcPassword) 
			AND bitPartnerAccess=1 
			AND GETUTCDATE() BETWEEN dtSubStartDate and dtSubEndDate
	END
	ELSE
	BEGIN
	SELECT TOP 1 
		U.numUserID
		,numUserDetailId
		,D.numCost
		,isnull(vcEmailID,'') vcEmailID
		,ISNULL(vcEmailAlias,'') vcEmailAlias
		,ISNULL(vcEmailAliasPassword,'') vcEmailAliasPassword
		,isnull(numGroupID,0) numGroupID
		,bitActivateFlag
		,isnull(vcMailNickName,'') vcMailNickName
		,isnull(txtSignature,'') txtSignature
		,isnull(U.numDomainID,0) numDomainID
		,isnull(Div.numDivisionID,0) numDivisionID
		,isnull(vcCompanyName,'') vcCompanyName
		,isnull(E.bitExchangeIntegration,0) bitExchangeIntegration
		,isnull(E.bitAccessExchange,0) bitAccessExchange
		,isnull(E.vcExchPath,'') vcExchPath
		,isnull(E.vcExchDomain,'') vcExchDomain
		,isnull(D.vcExchUserName,'') vcExchUserName
		,isnull(vcFirstname,'') + ' ' + isnull(vcLastName,'') as ContactName
		,Case when E.bitAccessExchange=0 then  E.vcExchPassword when E.bitAccessExchange=1 then D.vcExchPassword end as vcExchPassword
		,tintCustomPagingRows
		,vcDateFormat
		,numDefCountry
		,tintComposeWindow
		,dateadd(day,-sintStartDate,getutcdate()) as StartDate
		,dateadd(day,sintNoofDaysInterval,dateadd(day,-sintStartDate,getutcdate())) as EndDate
		,tintAssignToCriteria
		,bitIntmedPage
		,tintFiscalStartMonth
		,numAdminID
		,isnull(A.numTeam,0) numTeam
		,isnull(D.vcCurrency,'') as vcCurrency
		,isnull(D.numCurrencyID,0) as numCurrencyID, 
		isnull(D.bitMultiCurrency,0) as bitMultiCurrency,              
		bitTrial,isnull(tintChrForComSearch,0) as tintChrForComSearch,  
		isnull(tintChrForItemSearch,0) as tintChrForItemSearch,              
		case when bitSMTPServer= 1 then vcSMTPServer else '' end as vcSMTPServer  ,          
		case when bitSMTPServer= 1 then  isnull(numSMTPPort,0)  else 0 end as numSMTPPort      
		,isnull(bitSMTPAuth,0) bitSMTPAuth    
		,isnull([vcSmtpPassword],'') vcSmtpPassword    
		,isnull(bitSMTPSSL,0) bitSMTPSSL   ,isnull(bitSMTPServer,0) bitSMTPServer,       
		isnull(IM.bitImap,0) bitImapIntegration, isnull(charUnitSystem,'E')   UnitSystem, datediff(day,getutcdate(),dtSubEndDate) as NoOfDaysLeft,
		ISNULL(bitCreateInvoice,0) bitCreateInvoice,
		ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
		ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
		vcDomainName,
		S.numSubscriberID,
		D.[tintBaseTax],
		u.ProfilePic,
		ISNULL(D.[numShipCompany],0) numShipCompany,
		ISNULL(D.[bitEmbeddedCost],0) bitEmbeddedCost,
		(Select isnull(numAuthoritativeSales,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativeSales,
		(Select isnull(numAuthoritativePurchase,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativePurchase,
		ISNULL(D.[numDefaultSalesOppBizDocId],0) numDefaultSalesOppBizDocId,
		ISNULL(tintDecimalPoints,2) tintDecimalPoints,
		ISNULL(D.tintBillToForPO,0) tintBillToForPO, 
		ISNULL(D.tintShipToForPO,0) tintShipToForPO,
		ISNULL(D.tintOrganizationSearchCriteria,0) tintOrganizationSearchCriteria,
		ISNULL(D.tintLogin,0) tintLogin,
			ISNULL(S.intFullUserConcurrency,0) intFullUserConcurrency,
		ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
		ISNULL(D.vcPortalName,'') vcPortalName,
		(SELECT COUNT(*) FROM dbo.Subscribers WHERE getutcdate() between dtSubStartDate and dtSubEndDate and bitActive=1),
		ISNULL(D.bitGtoBContact,0) bitGtoBContact,
		ISNULL(D.bitBtoGContact,0) bitBtoGContact,
		ISNULL(D.bitGtoBCalendar,0) bitGtoBCalendar,
		ISNULL(D.bitBtoGCalendar,0) bitBtoGCalendar,
		ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
		ISNULL(D.bitInlineEdit,0) bitInlineEdit,
		ISNULL(U.numDefaultClass,0) numDefaultClass,
		ISNULL(D.bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
		ISNULL(D.bitTrakDirtyForm,1) bitTrakDirtyForm,
		isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
		ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
		CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
		ISNULL(D.bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
		isnull(U.tintTabEvent,0) as tintTabEvent,
		isnull(D.bitRentalItem,0) as bitRentalItem,
		isnull(D.numRentalItemClass,0) as numRentalItemClass,
		isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
		isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
		isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
		isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
		isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
		ISNULL(U.numDefaultWarehouse,0) numDefaultWarehouse,
		ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
		ISNULL(D.bitAutoSerialNoAssign,0) bitAutoSerialNoAssign,
		ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
		ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
		ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
		ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
		ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
		ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
		ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
		ISNULL(D.tintCommissionType,1) AS tintCommissionType,
		ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
		ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
		ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
		ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
		ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
		ISNULL(D.bitUseBizdocAmount,1) AS [bitUseBizdocAmount],
		ISNULL(D.bitDefaultRateType,1) AS [bitDefaultRateType],
		ISNULL(D.bitIncludeTaxAndShippingInCommission,1) AS [bitIncludeTaxAndShippingInCommission],
		ISNULL(D.[bitLandedCost],0) AS bitLandedCost,
		ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
		ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
		ISNULL(@listIds,'') AS vcUnitPriceApprover,
		ISNULL(D.numPODropShipBizDoc,0) AS numPODropShipBizDoc,
		ISNULL(D.numPODropShipBizDocTemplate,0) AS numPODropShipBizDocTemplate,
		ISNULL(D.IsEnableDeferredIncome,0) AS IsEnableDeferredIncome,
		ISNULL(D.bitEnableItemLevelUOM,0) AS bitEnableItemLevelUOM,
		ISNULL(D.bitPurchaseTaxCredit,0) AS bitPurchaseTaxCredit,
		ISNULL(U.bintCreatedDate,0) AS bintCreatedDate,
		(CASE WHEN LEN(ISNULL(TempDefaultPage.vcDefaultNavURL,'')) = 0 THEN ISNULL(TempDefaultTab.vcDefaultNavURL,'Items/frmItemList.aspx?Page=All Items&ItemGroup=0') ELSE ISNULL(TempDefaultPage.vcDefaultNavURL,'') END) vcDefaultNavURL,
		ISNULL(D.bitApprovalforTImeExpense,0) AS bitApprovalforTImeExpense,
		ISNULL(D.intTimeExpApprovalProcess,0) AS intTimeExpApprovalProcess,ISNULL(D.bitApprovalforOpportunity,0) AS bitApprovalforOpportunity,
		ISNULL(D.intOpportunityApprovalProcess,0) AS intOpportunityApprovalProcess,
		ISNULL(D.numDefaultSalesShippingDoc,0) AS numDefaultSalesShippingDoc,
		ISNULL(D.numDefaultPurchaseShippingDoc,0) AS numDefaultPurchaseShippingDoc,
		ISNULL((SELECT TOP 1 bitMarginPriceViolated FROM ApprovalProcessItemsClassification  WHERE numDomainID=D.numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1),0) AS bitMarginPriceViolated,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=2 AND numPageID=2),0) AS tintLeadRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=3 AND numPageID=2),0) AS tintProspectRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=4 AND numPageID=2),0) AS tintAccountRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=10 AND numPageID=10),0) AS tintSalesOrderListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=10 AND numPageID=11),0) AS tintPurchaseOrderListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=10 AND numPageID=2),0) AS tintSalesOpportunityListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=10 AND numPageID=8),0) AS tintPurchaseOpportunityListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=11 AND numPageID=2),0) AS tintContactListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=7 AND numPageID=2),0) AS tintCaseListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=12 AND numPageID=2),0) AS tintProjectListRights,
		ISNULL((SELECT TOP 1 numCriteria FROM TicklerListFilterConfiguration WHERE CHARINDEX(CONCAT(',',U.numUserDetailID,','),CONCAT(',',vcSelectedEmployee,',')) > 0),0) AS tintActionItemRights,
		ISNULL(D.numAuthorizePercentage,0) AS numAuthorizePercentage,
		ISNULL(D.bitAllowDuplicateLineItems,0) bitAllowDuplicateLineItems,
		ISNULL(D.bitLogoAppliedToBizTheme,0) bitLogoAppliedToBizTheme,
		ISNULL(D.vcLogoForBizTheme,'') vcLogoForBizTheme,
		ISNULL(D.bitLogoAppliedToLoginBizTheme,0) bitLogoAppliedToLoginBizTheme,
		ISNULL(D.vcLogoForLoginBizTheme,'') vcLogoForLoginBizTheme,
		ISNULL(D.vcThemeClass,'1473b4') vcThemeClass,
		ISNULL(D.vcLoginURL,'') AS vcLoginURL,
		ISNULL(D.bitDisplayCustomField,0) AS bitDisplayCustomField,
		ISNULL(D.bitFollowupAnytime,0) AS bitFollowupAnytime,
		ISNULL(D.bitpartycalendarTitle,0) AS bitpartycalendarTitle,
		ISNULL(D.bitpartycalendarLocation,0) AS bitpartycalendarLocation,
		ISNULL(D.bitpartycalendarDescription,0) AS bitpartycalendarDescription,
		ISNULL(D.bitREQPOApproval,0) AS bitREQPOApproval,
		ISNULL(D.bitARInvoiceDue,0) AS bitARInvoiceDue,
		ISNULL(D.bitAPBillsDue,0) AS bitAPBillsDue,
		ISNULL(D.bitItemsToPickPackShip,0) AS bitItemsToPickPackShip,
		ISNULL(D.bitItemsToInvoice,0) AS bitItemsToInvoice,
		ISNULL(D.bitSalesOrderToClose,0) AS bitSalesOrderToClose,
		ISNULL(D.bitItemsToPutAway,0) AS bitItemsToPutAway,
		ISNULL(D.bitItemsToBill,0) AS bitItemsToBill,
		ISNULL(D.bitPosToClose,0) AS bitPosToClose,
		ISNULL(D.bitPOToClose,0) AS bitPOToClose,
		ISNULL(D.bitBOMSToPick,0) AS bitBOMSToPick,
		ISNULL(D.vchREQPOApprovalEmp,'') AS vchREQPOApprovalEmp,
		ISNULL(D.vchARInvoiceDue,'') AS vchARInvoiceDue,
		ISNULL(D.vchAPBillsDue,'') AS vchAPBillsDue,
		ISNULL(D.vchItemsToPickPackShip,'') AS vchItemsToPickPackShip,
		ISNULL(D.vchItemsToInvoice,'') AS vchItemsToInvoice,
		ISNULL(stuff((
        select ',' + CAST(UPC.numUserID AS VARCHAR(10))
        from UnitPriceApprover UPC
        where UPC.numDomainID = D.numDomainId
        order by UPC.numUserID
			for xml path('')),1,1,''),'') AS vchPriceMarginApproval,
		ISNULL(D.vchSalesOrderToClose,'') AS vchSalesOrderToClose,
		ISNULL(D.vchItemsToPutAway,'') AS vchItemsToPutAway,
		ISNULL(D.vchItemsToBill,'') AS vchItemsToBill,
		ISNULL(D.vchPosToClose,'') AS vchPosToClose,
		ISNULL(D.vchPOToClose,'') AS vchPOToClose,
		ISNULL(D.vchBOMSToPick,'') AS vchBOMSToPick,
		ISNULL(D.decReqPOMinValue,0) AS decReqPOMinValue,
		ISNULL(U.tintPayrollType,1) tintPayrollType,
		(CASE WHEN EXISTS (SELECT numCategoryHDRID FROM TimeAndExpense WHERE numUserCntID=U.numUserDetailId AND numCategory=1 AND numType=7 AND dtToDate IS NULL) THEN 1 ELSE 0 END) bitUserClockedIn,
		ISNULL(D.bitUseOnlyActionItems,0) AS bitUseOnlyActionItems,
ISNULL(D.vcElectiveItemFields,'') AS vcElectiveItemFields,
ISNULL(D.bitDisplayContractElement,0) AS bitDisplayContractElement,
ISNULL(D.vcEmployeeForContractTimeElement,'') AS vcEmployeeForContractTimeElement,
ISNULL(D.bitEnableSmartyStreets,0) bitEnableSmartyStreets,
ISNULL(D.vcSmartyStreetsAPIKeys,'') vcSmartyStreetsAPIKeys
	FROM 
		UserMaster U                              
	left join ExchangeUserDetails E                              
	on E.numUserID=U.numUserID                              
	left join ImapUserDetails IM                              
	on IM.numUserCntID=U.numUserDetailID          
	Join Domain D                              
	on D.numDomainID=U.numDomainID                                           
	left join DivisionMaster Div                                            
	on D.numDivisionID=Div.numDivisionID                               
	left join CompanyInfo C                              
	on C.numCompanyID=Div.numCompanyID                               
	left join AdditionalContactsInformation A                              
	on  A.numContactID=U.numUserDetailId                            
	Join  Subscribers S                            
	on S.numTargetDomainID=D.numDomainID    
	 OUTER APPLY
	 (
		SELECT  
			TOP 1 REPLACE(SCB.Link,'../','') vcDefaultNavURL
		FROM    
			TabMaster T
		JOIN 
			GroupTabDetails G ON G.numTabId = T.numTabId
		LEFT JOIN
			ShortCutGrpConf SCUC ON G.numTabId=SCUC.numTabId AND SCUC.numDomainId=U.numDomainID AND SCUC.numGroupId=U.numGroupID AND bitDefaultTab=1
		LEFT JOIN
			ShortCutBar SCB
		ON
			SCB.Id=SCUC.numLinkId
		WHERE   
			(T.numDomainID = U.numDomainID OR bitFixed = 1)
			AND G.numGroupID = U.numGroupID
			AND ISNULL(G.[tintType], 0) <> 1
			AND tintTabType =1
			AND T.numTabID NOT IN (2,68)
		ORDER BY 
			SCUC.bitInitialPage DESC
	 )  TempDefaultPage 
	 OUTER APPLY
	 (
		SELECT  
			TOP 1 REPLACE(T.vcURL,'../','') vcDefaultNavURL
		FROM    
			TabMaster T
		JOIN 
			GroupTabDetails G 
		ON 
			G.numTabId = T.numTabId
		WHERE   
			(T.numDomainID = U.numDomainID OR bitFixed = 1)
			AND G.numGroupID = U.numGroupID
			AND ISNULL(G.[tintType], 0) <> 1
			AND tintTabType =1
			AND T.numTabID NOT IN (2,68)
			AND ISNULL(G.bitInitialTab,0) = 1 
			AND ISNULL(bitallowed,0)=1
		ORDER BY 
			G.bitInitialTab DESC
	 ) TEMPDefaultTab                   
	WHERE 1 = (CASE 
		WHEN @vcLinkedinId='N' THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
		ELSE (CASE WHEN U.vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
		END) and bitActive IN (1,2) AND getutcdate() between dtSubStartDate and dtSubEndDate
	END
             
	SELECT 
		numTerritoryId 
	FROM
		UserTerritory UT                              
	JOIN 
		UserMaster U                              
	ON 
		numUserDetailId =UT.numUserCntID                                         
	WHERE 
		1 = (CASE 
			WHEN @vcLinkedinId='N' THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
			ELSE (CASE WHEN vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
			END)

END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Communication_Finish')
DROP PROCEDURE USP_Communication_Finish
GO
CREATE PROCEDURE [dbo].[USP_Communication_Finish]
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numCommID NUMERIC(18,0)
AS  
BEGIN  
	UPDATE 
		Communication 
	SET                                              
		nummodifiedby=@numUserCntID,                                                
		dtModifiedDate=GETUTCDATE(),                                                
		bitClosedFlag=1,   
		dtEventClosedDate=GETUTCDATE()
	WHERE 
		numDomainID = @numDomainID
		AND numCommId = @numCommID		 
END
GO
--- Created By Anoop jayaraj       
--- Modified By Gangadhar 03/05/2008                                                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_companylist1')
DROP PROCEDURE usp_companylist1
GO
CREATE PROCEDURE [dbo].[USP_CompanyList1]                                                      
	@numRelationType as numeric(9),                                                   
	@numUserCntID numeric(9),                                                      
	@tintUserRightType tinyint,                                                          
	@SortChar char(1)='0',                                                     
	@FirstName varChar(100)= '',                                                      
	@LastName varChar(100)= '',                                                      
	@CustName varChar(100)= '',                                                    
	@CurrentPage int,                                                    
	@PageSize int,                                                    
	@columnName as Varchar(50),                                                    
	@columnSortOrder as Varchar(10),                                            
	@tintSortOrder numeric=0,                                             
	@numProfile as numeric(9)=0,                                    
	@numDomainID as numeric(9)=0,                            
	@tintCRMType as tinyint,                            
	@bitPartner as BIT,
	@numFormID AS NUMERIC(9),
	@vcRegularSearchCriteria varchar(MAX)='',
	@vcCustomSearchCriteria varchar(MAX)=''                                                     
as       
	IF CHARINDEX('AD.numBillCountry',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.numBillCountry','AD1.numCountry')
	END
	ELSE IF CHARINDEX('AD.numShipCountry',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.numShipCountry','AD2.numCountry')
	END
	ELSE IF CHARINDEX('AD.vcBillCity',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.vcBillCity','AD5.vcCity')
	END
	ELSE IF CHARINDEX('AD.vcShipCity',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.vcShipCity','AD6.vcCity')
	END
	IF CHARINDEX('ADC.vcLastFollowup',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'ADC.vcLastFollowup', '(SELECT dtExecutionDate FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND ISNULL(ADC.numECampaignID,0) > 0 AND numConECampDTLID IN (SELECT MAX(numConECampDTLID) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND ISNULL(ADC.numECampaignID,0) > 0 AND ISNULL(bitSend,0)=1))')
	END
	IF CHARINDEX('ADC.vcNextFollowup',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'ADC.vcNextFollowup','(SELECT dtExecutionDate FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND ISNULL(ADC.numECampaignID,0) > 0 AND numConECampDTLID IN (SELECT MIN(numConECampDTLID) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID=ADC.numContactId AND ISNULL(ADC.numECampaignID,0) > 0 AND ISNULL(bitSend,0)=0))')
	END     

    declare @firstRec as integer                                                                
  declare @lastRec as integer                                                                
 set @firstRec= (@CurrentPage-1) * @PageSize                                                                
     set @lastRec= (@CurrentPage*@PageSize+1)                                                                          
  declare @column as varchar(50)                  
set @column = @columnName                  
declare @lookbckTable as varchar(50)                  
set @lookbckTable = ''                                                               
                                                    
  declare @strSql as varchar(MAX)                                                  
set @strSql='with tblCompany as (SELECT  '          
if @tintSortOrder=7 or @tintSortOrder=8  set @strSql=@strSql + ' '             
if @tintSortOrder=5  set @strSql=@strSql + 'ROW_NUMBER() OVER ( order by numCompanyRating desc) AS RowNumber,COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount, '           
else           
 set @strSql=@strSql +' ROW_NUMBER() OVER (ORDER BY '+@column+' '+ @columnSortOrder+') AS RowNumber,COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount, '                                                       
set @strSql=@strSql +'  DM.numDivisionID                                          
    FROM  CompanyInfo CMP                                 
    join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID 
	left join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID AND ISNULL(ADC.bitPrimaryContact,0)=1
	left join VIEW_OPEN_ACTION_ITEMS VOA WITH (NOEXPAND) ON VOA.numDomainID = DM.numDomainID AND  VOA.numDivisionId = DM.numDivisionID
	left join View_InboxEmail VIE  WITH (NOEXPAND) ON VIE.numDomainID = DM.numDomainID AND  VIE.numContactId = ADC.numContactId '                                            
                               
if @tintSortOrder= 9 set @strSql=@strSql+' join Favorites F on F.numContactid=DM.numDivisionID '                                            
if @bitPartner=1 set @strSql=@strSql + ' left join CompanyAssociations CA on CA.numAssociateFromDivisionID=DM.numDivisionID and bitShareportal=1 and bitDeleted=0                           
and CA.numDivisionID=(select numDivisionID from AdditionalContactsInformation where numContactID='+convert(varchar(15),@numUserCntID)+ ')'                          
set @strSql=@strSql + ' ##JOIN##                                                     
    left join ListDetails LD on LD.numListItemID=CMP.numCompanyRating                                                     
    left join ListDetails LD1 on LD1.numListItemID= DM.numFollowUpStatus 
left Join AddressDetails AD on AD.numRecordId= DM.numDivisionID and AD.tintAddressOf=2 and AD.tintAddressType=1 AND AD.bitIsPrimary=1 and AD.numDomainID= DM.numDomainID                                                   
  WHERE CMP.numDomainID=  DM.numDomainID                                                    
    AND DM.numDomainID = '+convert(varchar(15),@numDomainID)+ ''                                                    
   if @FirstName<>'' set    @strSql=@strSql+' and ADC.vcFirstName  like '''+@FirstName+'%'''                                                     
    if @LastName<>'' set    @strSql=@strSql+' and ADC.vcLastName like '''+@LastName+'%'''                                                    
    if @CustName<>'' set    @strSql=@strSql+' and CMP.vcCompanyName like '''+@CustName+'%'''                               
	IF @numRelationType <> '0' 
	BEGIN
		IF @numRelationType = 47
		BEGIN
			SET @strSql=@strSql + ' AND (CMP.numCompanyType = 47 OR (CMP.numCompanyType=46 AND EXISTS (SELECT numOppID FROM [OpportunityMaster] OM WHERE OM.numDomainId=' + CONVERT(VARCHAR(15), @numDomainID) + ' AND OM.numDivisionID=DM.numDivisionID AND OM.tintOppType = 2 AND OM.tintOppStatus = 1)))'   
		END
		ELSE
		BEGIN
			SET @strSql=@strSql + ' AND CMP.numCompanyType = '+convert(varchar(15),@numRelationType)+ ''   
		END
	END

                                                 
if @SortChar<>'0' set @strSql=@strSql + ' And CMP.vcCompanyName like '''+@SortChar+'%'''

	DECLARE @strExternalUser AS VARCHAR(MAX) = ''
	SET @strExternalUser = CONCAT(' (NOT EXISTS (SELECT numUserDetailID FROM UserMaster WHERE numDomainID=',@numDomainID,' AND numUserDetailID=',@numUserCntId,') AND EXISTS (SELECT EAD.numExtranetDtlID FROM ExtranetAccountsDtl EAD INNER JOIN ExtarnetAccounts EA ON EAD.numExtranetID=EA.numExtranetID WHERE EAD.numDomainID=',@numDomainID,' AND EAD.numContactID=',@numUserCntID,' AND (EA.numDivisionID=DM.numDivisionID OR EA.numDivisionID=DM.numPartenerSource)))')
                                           
	IF @tintUserRightType=1 
		SET @strSql = CONCAT(@strSql,' AND ((DM.numRecOwner = ',@numUserCntID,')',' OR ',@strExternalUser,(CASE WHEN @bitPartner=1 THEN ' OR ( CA.bitShareportal=1 and CA.bitDeleted=0))' ELSE ')' END))                                                 
else if @tintUserRightType=2 set @strSql=@strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ') '                     
if @tintSortOrder=1  set @strSql=@strSql + ' AND DM.numStatusID=2 '                                                                
else if @tintSortOrder=2  set @strSql=@strSql + ' AND DM.numStatusID=3 '                                                                          
else if @tintSortOrder=3  set @strSql=@strSql + ' AND (DM.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ' or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ')'                                                                       
--else if @tintSortOrder=5  set @strSql=@strSql + ' order by numCompanyRating desc '                                                                       
else if @tintSortOrder=6  set @strSql=@strSql + ' AND DM.bintCreatedDate > '''+convert(varchar(20),dateadd(day,-7,getutcdate()))+''''                                                                      
else if @tintSortOrder=7  set @strSql=@strSql + ' and DM.numCreatedby='+convert(varchar(15),@numUserCntID)                    
--+ ' ORDER BY DM.bintCreateddate desc '                                                              
else if @tintSortOrder=8  set @strSql=@strSql + ' and DM.numModifiedby='+convert(varchar(15),@numUserCntID)          
           
--if (@tintSortOrder=7 and @column!='DM.bintcreateddate')  set @strSql='select * from ('+@strSql+')X   '                                                     
--else if (@tintSortOrder=8 and @column!='DM.bintcreateddate')  set @strSql='select * from ('+@strSql+')X  '                                                               
--else 
if @tintSortOrder=9  set @strSql=@strSql + ' and F.numUserCntID='+convert(varchar(15),@numUserCntID)+ ' and cType=''O'''              
                                       
if @numProfile> 0 set @strSql=@strSql + ' and  vcProfile='+convert(varchar(15),@numProfile)                                            
if @tintCRMType>0 set @strSql=@strSql + ' and  DM.tintCRMType='+convert(varchar(1),@tintCRMType)                                              
                                                  
IF @vcRegularSearchCriteria<>'' set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
IF @vcCustomSearchCriteria<>'' set @strSql=@strSql+' AND ' + @vcCustomSearchCriteria
                                                  
                                                    
  set @strSql=@strSql + ')'                      
   
                                                                                 
declare @tintOrder as tinyint                                                        
declare @vcFieldName as varchar(50)                                                        
declare @vcListItemType as varchar(3)           
declare @numListID AS numeric(9)                                        
declare @vcAssociatedControlType varchar(20)                                                        
declare @vcDbColumnName varchar(200)                            
declare @WhereCondition varchar(MAX)                             
declare @vcLookBackTableName varchar(2000)                      
Declare @bitCustom as bit  
DECLARE @bitAllowEdit AS CHAR(1)             
Declare @numFieldId as numeric  
DECLARE @bitAllowSorting AS CHAR(1)            
declare @vcColumnName AS VARCHAR(500)                          
set @tintOrder=0                                                        
set @WhereCondition =''                       
                         
declare @Nocolumns as tinyint                 
declare @DefaultNocolumns as tinyint                      
set @Nocolumns=0  

select @Nocolumns=isnull(sum(TotalRow),0)  from(            
Select count(*) TotalRow from View_DynamicColumns where numFormId=@numFormID and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=@numRelationType 
Union 
Select count(*) TotalRow from View_DynamicCustomColumns where numFormId=@numFormID and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=@numRelationType) TotalRows

                    
  set @DefaultNocolumns=  @Nocolumns  
                       
while @DefaultNocolumns>0                      
begin                     
 set @DefaultNocolumns=@DefaultNocolumns-1                    
end              
   

   CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(200),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth INT,bitAllowFiltering BIT,vcFieldDataType CHAR(1))
    


set @strSql=@strSql+' select ADC.numContactId,DM.numDivisionID,DM.numTerID,ADC.numRecOwner,DM.tintCRMType,RowNumber,ADC.vcFirstName+'' ''+ADC.vcLastName AS vcContactName,ADC.numPhone AS numContactPhone,ADC.numPhoneExtension AS numContactPhoneExtension,ADC.vcEmail AS vcContactEmail'                       

--Check if Master Form Configuration is created by administrator if NoColumns=0
DECLARE @IsMasterConfAvailable BIT = 0
DECLARE @numUserGroup NUMERIC(18,0)

IF @Nocolumns=0
BEGIN
	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormID AND numRelCntType=@numRelationType AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END
END


--If MasterConfiguration is available then load it otherwise load default columns
IF @IsMasterConfAvailable = 1
BEGIN
	INSERT INTO DycFormConfigurationDetails 
	(
		numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
	)
	SELECT 
		@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numRelationType,1,0,intColumnWidth
	FROM 
		View_DynamicColumnsMasterConfig
	WHERE
		View_DynamicColumnsMasterConfig.numFormId=@numFormID AND 
		View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicColumnsMasterConfig.numRelCntType=@numRelationType AND 
		View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
		ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	UNION
    SELECT 
		@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numRelationType,1,1,intColumnWidth
	FROM 
		View_DynamicCustomColumnsMasterConfig
	WHERE 
		View_DynamicCustomColumnsMasterConfig.numFormId=@numFormID AND 
		View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicCustomColumnsMasterConfig.numRelCntType=@numRelationType AND 
		View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

	INSERT INTO #tempForm
	SELECT 
		(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
		vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
		bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
		ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
	FROM 
		View_DynamicColumnsMasterConfig
	WHERE
		View_DynamicColumnsMasterConfig.numFormId=@numFormID AND 
		View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicColumnsMasterConfig.numRelCntType=@numRelationType AND 
		View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
		ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	UNION
    SELECT 
		tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
		bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
	FROM 
		View_DynamicCustomColumnsMasterConfig
	WHERE 
		View_DynamicCustomColumnsMasterConfig.numFormId=@numFormID AND 
		View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicCustomColumnsMasterConfig.numRelCntType=@numRelationType AND 
		View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
	ORDER BY 
		tintOrder ASC  
END
ELSE                                        
BEGIN      

	if @Nocolumns=0
	BEGIN
	INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
	select @numFormID,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,@numRelationType,1,0,intColumnWidth
	 FROM View_DynamicDefaultColumns
	 where numFormId=@numFormID and bitDefault=1 AND ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID
	order by tintOrder asc   
	END          
    
    INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
 FROM View_DynamicColumns 
 where numFormId=@numFormID and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=@numRelationType
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
       
       UNION
    
   select tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
 from View_DynamicCustomColumns
 where numFormId=@numFormID and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=@numRelationType
 AND ISNULL(bitCustom,0)=1
 
  order by tintOrder asc  
   
 END                                                  
                        
--    set @DefaultNocolumns=  @Nocolumns  
Declare @ListRelID as numeric(9)     
      select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
@ListRelID=ListRelID
  from  #tempForm order by tintOrder asc            

                                                    
while @tintOrder>0                                                        
begin                                             
     print @vcFieldName                                               
    if @bitCustom = 0            
 begin                      
       
      
 declare @Prefix as varchar(5)                            
      if @vcLookBackTableName = 'AdditionalContactsInformation'                            
		set @Prefix = 'ADC.'                            
      else if @vcLookBackTableName = 'DivisionMaster'                            
		set @Prefix = 'DM.'   
		
			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

					
	if @vcDbColumnName = 'vcLastFollowup'
BEGIN
	set @strSql=@strSql+',  ISNULL((CASE WHEN ADC.numECampaignID > 0 THEN dbo.fn_FollowupDetailsInOrgList(ADC.numContactID,1,ADC.numDomainID) ELSE '''' END),'''') ['+ @vcColumnName+']' 
END
ELSE IF @vcDbColumnName = 'vcNextFollowup'
BEGIN
	set @strSql=@strSql+',  ISNULL((CASE WHEN ADC.numECampaignID > 0 THEN dbo.fn_FollowupDetailsInOrgList(ADC.numContactID,2,ADC.numDomainID) ELSE '''' END),'''') ['+ @vcColumnName+']' 
END

else if @vcAssociatedControlType='SelectBox'                                                              
        begin                                                              
    IF @vcDbColumnName='numDefaultShippingServiceID'
	BEGIN
		SET @strSql=@strSql+ ' ,ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=DM.numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = DM.numDefaultShippingServiceID),'''')' + ' [' + @vcColumnName + ']' 
	END                  
	Else IF @vcDbColumnName = 'numPartenerSource'
	BEGIN
		SET @strSql=@strSql+ ' ,(DM.numPartenerSource) '+' ['+ @vcColumnName+']'    
		SET @strSql=@strSql+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=DM.numPartenerSource) AS vcPartenerSource' 
	END                                        
    ELSE if @vcListItemType='LI'                                                               
     BEGIN
     	IF @numListID=40 
		 BEGIN
			set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'          
			
			IF @vcDbColumnName='numShipCountry'
			BEGIN
				SET @WhereCondition = @WhereCondition
									+ ' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= DM.numDomainID '
									+ ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=AD2.numCountry '
			END
			ELSE
			BEGIN
				SET @WhereCondition = @WhereCondition
								+ ' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= DM.numDomainID '
								+ ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=AD1.numCountry '
			END
		 END
		 ELSE IF @numListID=5
		BEGIN						
			set @strSql=@strSql+',(CASE WHEN CMP.numCompanyType=46 THEN CONCAT(L'+ convert(varchar(3),@tintOrder)+'.vcData,'' '',''<i class="fa fa-info-circle text-blue" aria-hidden="true" title="Organizations with a sales orders will be listed in the Account list, just as those with Purchase Orders will be listed in the Vendor list. This explains why you may see a Vendor in the Accounts list and an Account in the Vendor list" style="font-size: 20px;"></i>'') ELSE L'+ convert(varchar(3),@tintOrder)+'.vcData END)'+' ['+ @vcColumnName+']'                                                             
			set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName
		END
		 ELSE
		 BEGIN    
		  set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                             
		  set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                              
		 END
     end    
       else if @vcListItemType='C'                                                     
     begin                                                    
      set @strSql=@strSql+',C'+ convert(varchar(3),@tintOrder)+'.vcCampaignName'+' ['+ @vcColumnName+']'                                                    
      set @WhereCondition= @WhereCondition +' left Join CampaignMaster C'+ convert(varchar(3),@tintOrder)+ ' on C'+convert(varchar(3),@tintOrder)+ '.numCampaignId=DM.'+@vcDbColumnName                                                    
     end                                                            
     else if @vcListItemType='S'                                                               
     begin       
		IF @vcDbColumnName='numShipState'
		BEGIN
			SET @strSql = @strSql + ',ShipState.vcState' + ' [' + @vcColumnName + ']' 
			SET @WhereCondition = @WhereCondition
				+ ' left Join AddressDetails AD3 on AD3.numRecordId= DM.numDivisionID and AD3.tintAddressOf=2 and AD3.tintAddressType=2 AND AD3.bitIsPrimary=1 and AD3.numDomainID= DM.numDomainID '
				+ ' left Join State ShipState on ShipState.numStateID=AD3.numState '
		END
		ELSE IF @vcDbColumnName='numBillState'
		BEGIN
			SET @strSql = @strSql + ',BillState.vcState' + ' [' + @vcColumnName + ']' 
			SET @WhereCondition = @WhereCondition
				+ ' left Join AddressDetails AD4 on AD4.numRecordId= DM.numDivisionID and AD4.tintAddressOf=2 and AD4.tintAddressType=1 AND AD4.bitIsPrimary=1 and AD4.numDomainID= DM.numDomainID '
				+ ' left Join State BillState on BillState.numStateID=AD4.numState '
		END
                                                       
--      set @strSql=@strSql+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName+']'                                                              
--      set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID='+@vcDbColumnName                                                              
     end                                                              
     else if @vcListItemType='T'                                                               
     begin                                                              
      set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                              
      set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                              
     end                             
     else if   @vcListItemType='U'                                                           
    begin                             
                                
                                  
    set @strSql=@strSql+',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'                                                              
    end
	ELSE IF @vcListItemType='SYS'                                                         
	BEGIN
		SET @strSql=@strSql+',case tintCRMType when 1 then ''Prospect'' when 2 then ''Account'' else ''Lead'' end as ['+ @vcColumnName+']'                                                              
	END                                  
    end           
          
else if @vcAssociatedControlType='DateField'                                                        
 begin                  
             
     set @strSql=@strSql+',case when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''            
     set @strSql=@strSql+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''            
     set @strSql=@strSql+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '            
     set @strSql=@strSql+'else dbo.FormatedDateFromDate('+@Prefix+@vcDbColumnName+','+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'                      
   end   
        
ELSE IF @vcAssociatedControlType='TextBox' AND @vcDbColumnName='vcBillCity' 
BEGIN
	SET @strSql = @strSql + ',AD5.vcCity' + ' [' + @vcColumnName + ']'  
				
	SET @WhereCondition = @WhereCondition
				+ ' left Join AddressDetails AD5 on AD5.numRecordId= DM.numDivisionID and AD5.tintAddressOf=2 and AD5.tintAddressType=1 AND AD5.bitIsPrimary=1 and AD5.numDomainID= DM.numDomainID '

END
ELSE IF @vcAssociatedControlType='TextBox' AND   @vcDbColumnName = 'vcCompactContactDetails'
BEGIN
	SET @strSql=@strSql+ ' ,'''' AS vcCompactContactDetails'   
END  
ELSE IF @vcAssociatedControlType='TextBox' AND  @vcDbColumnName='vcShipCity'
BEGIN
	SET @strSql=@strSql + ',AD6.vcCity' + ' [' + @vcColumnName + ']' 
						
	SET @WhereCondition = @WhereCondition + ' left Join AddressDetails AD6 on AD6.numRecordId= DM.numDivisionID and AD6.tintAddressOf=2 and AD6.tintAddressType=2 AND AD6.bitIsPrimary=1 and AD6.numDomainID= DM.numDomainID '
END                
else if @vcAssociatedControlType='TextBox'  OR @vcAssociatedControlType='Label'                                                       
begin          
 set @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when          
  @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' else @vcDbColumnName end+' ['+ @vcColumnName+']'                    
             
 end          
else                                                  
begin      
 set @strSql=@strSql+','+ @vcDbColumnName +' ['+ @vcColumnName+']'                
         
 end    
            
 end            
else if @bitCustom = 1            
begin            
               
   select @vcFieldName=FLd_label,@vcAssociatedControlType=fld_type,@vcDbColumnName='Cust'+Convert(varchar(10),Fld_Id)                           
    from CFW_Fld_Master                                            
   where  CFW_Fld_Master.Fld_Id = @numFieldId             
            
   SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'    
          
   if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'             
   begin              
                 
    set @strSql= @strSql+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'               
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Dm.numDivisionId   '                                                     
   end            
    else if @vcAssociatedControlType = 'CheckBox'              
   begin              
                 
-- OLD    set @strSql= @strSql+',case when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=0 then ''No'' when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=1 then ''Yes'' end   ['+ @vcFieldName+'~'+ @vcDbColumnName+']'               
set @strSql= @strSql+',case when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=0 then 0 when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=1 then 1 end   ['+ @vcColumnName +']'               
  
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '               
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId   '                                                       
   end              
   else if @vcAssociatedControlType = 'DateField'          
   begin    
    set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'               
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Dm.numDivisionId   '                                                     
   end            
   else if @vcAssociatedControlType = 'SelectBox'                
   begin              
    set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)            
    set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                      
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
     on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Dm.numDivisionId   '                                                     
    set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'            
   end
   ELSE IF @vcAssociatedControlType = 'CheckBoxList' 
	BEGIN
		SET @strSql= @strSql+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

	SET @WhereCondition= @WhereCondition 
						+' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)
						+ ' on CFW' + convert(varchar(3),@tintOrder) 
						+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
						+ 'and CFW' + convert(varchar(3),@tintOrder) 
						+ '.RecId=DM.numDivisionId '
	END                    
end                
   
     select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc            
 
  if @@rowcount=0 set @tintOrder=0 
                  
                       
                                      
                                                       
end                             

---- Added by Priya 16 Jan 2018(Flag with Organisation Name)--------
SET @strSql=@strSql+' ,

	((CASE 
	WHEN ADC.numECampaignID > 0 
	THEN 
		 ISNULL( (CASE WHEN 
				ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											WHERE ConECampaign.numContactID = ADC.numContactID AND  ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =1 AND ISNULL(bitSend,0) = 1 AND ADC.bitPrimaryContact = 1),0) 
				= ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											 WHERE ConECampaign.numContactID = ADC.numContactID AND ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =1 AND ADC.bitPrimaryContact = 1 ),0)
		THEN
		  ''CheckeredFlag''
		  Else
		  ''GreenFlag''
		  End),0)



	ELSE '''' 
	END)) as FollowupFlag '

	SET @strSql=@strSql+' ,

	((CASE 
	WHEN ADC.numECampaignID > 0 
	THEN 
		''('' + CAST(ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											WHERE ConECampaign.numContactID = ADC.numContactID AND  ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =1 AND ISNULL(bitSend,0) = 1 AND ADC.bitPrimaryContact = 1),0) AS varchar)

			+ ''/'' + CAST(ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
											 WHERE ConECampaign.numContactID = ADC.numContactID AND ConECampaign.numECampaignID = ADC.numECampaignID AND bitEngaged =1 AND ADC.bitPrimaryContact = 1 ),0)AS varchar) + '')''
			+ ''<a onclick=openDrip('' + (cast(ISNULL((SELECT MAX([numConEmailCampID]) FROM [ConECampaign] WHERE [numContactID] = ADC.numContactID AND [numECampaignID]= ADC.numECampaignID AND ISNULL(bitEngaged,0)=1 AND ADC.bitPrimaryContact = 1 ),0)AS varchar)) + '');>
		 <img alt=Follow-up Campaign history height=16px width=16px title=Follow-up campaign history src=../images/GLReport.png
		 ></a>'' 
	ELSE '''' 
	END)) as ReadUnreadCntNHstr '

	---- Added by Priya 16 Jan 2018(Flag with Organisation Name)--------
set @strSql=@strSql+' ,TotalRowCount,
	ISNULL(VIE.Total,0) as TotalEmail,
	ISNULL(VOA.OpenActionItemCount,0) as TotalActionItem
  From CompanyInfo CMP                                                                
    join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID                                                      
    left join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID  
    left join VIEW_OPEN_ACTION_ITEMS VOA WITH (NOEXPAND) ON VOA.numDomainID = DM.numDomainID AND  VOA.numDivisionId = DM.numDivisionID
	left join View_InboxEmail VIE  WITH (NOEXPAND) ON VIE.numDomainID = DM.numDomainID AND  VIE.numContactId = ADC.numContactId
   '+@WhereCondition+       
' join tblCompany T on T.numDivisionID=DM.numDivisionID                                                                                          
  WHERE (ISNULL(ADC.bitPrimaryContact,0)=1 OR ADC.numContactID IS NULL) and 
  CMP.numDomainID=  DM.numDomainID and DM.numDomainID = ' + convert(varchar(18),@numDomainID) + '
  and RowNumber > '+convert(varchar(10),@firstRec)+ ' and RowNumber <'+ convert(varchar(10),@lastRec)     
  
  

SET @strSql = REPLACE(@strSql,'##JOIN##',@WhereCondition)              
-- new code added by sojan 05 Mar 2010

--declare @vcTestSql varchar(8000);
--declare @FieldCount numeric(8);
--
--set @vcTestSql = substring(@strSql, charindex('select',@strSql),len(@strSql))
--select @FieldCount= [dbo].[uf_charCount] (@vcTestSql,',')
--
--set @vcTestSql=' union select count(*),' + replicate('Null,',@FieldCount-2)
--
--set @vcTestSql=substring(@vcTestSql,1,len(@vcTestSql)-1)
--set @vcTestSql = @vcTestSql +' from tblCompany order by RowNumber'   
--
--set @strSql=REPLACE(@strSql,'|',',') + @vcTestSql

-- old code
--' union select count(*),null,null,null,null,null '+@strColumns+' from tblCompany order by RowNumber'                                                 
      
SET @strSql =@strSql + ' ORDER BY '+@column+' '+ @columnSortOrder+' '                                
print @strSql                              
                      
exec (@strSql) 


SELECT * FROM #tempForm

DROP TABLE #tempForm
/****** Object:  StoredProcedure [dbo].[USP_DeleteItemList]    Script Date: 07/26/2008 16:15:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteitemlist')
DROP PROCEDURE usp_deleteitemlist
GO
CREATE PROCEDURE [dbo].[USP_DeleteItemList]          
@numListItemID as numeric(9)=0,            
@numListId as numeric(9)=0  
as              
Begin    

	IF ISNULL((SELECT constFlag FROM ListDetails where numListItemID=@numListItemID),0) = 0
	BEGIN
		DELETE FROM ListDetails WHERE numListItemID=@numListItemID   
   
		/* delete dependant dropdown relations if anyone in use */
		DELETE FROM [FieldRelationshipDTL] WHERE [numPrimaryListItemID] = @numListItemID
		DELETE FROM [FieldRelationshipDTL] WHERE [numSecondaryListItemID] = @numListItemID
	END

End
GO
/****** Object:  StoredProcedure [dbo].[Usp_getCorrespondance]    Script Date: 07/26/2008 16:17:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcorrespondance')
DROP PROCEDURE usp_getcorrespondance
GO
CREATE PROCEDURE [dbo].[usp_getcorrespondance]                          
@FromDate as datetime,                          
@toDate as datetime,                          
@numContactId as numeric,                          
@vcMessageFrom as varchar(200)='' ,                        
@tintSortOrder as numeric,                        
@SeachKeyword as varchar(100),                        
@CurrentPage as INT,                        
@PageSize as INT,                        
@TotRecs as numeric output,                        
@columnName as varchar(100),                        
@columnSortOrder as varchar(50),                        
@filter as numeric,                        
@numdivisionId as numeric =0,              
@numDomainID as numeric(9)=0,              
@numCaseID as numeric(9)=0,              
@ClientTimeZoneOffset INT,
@numOpenRecordID NUMERIC=0,
@tintMode TINYINT=0,
@bitOpenCommu BIT=0,
@numUserCntID NUMERIC=0,
@vcTypes VARCHAR(200)
as 
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
DECLARE @vcUserLoginEmail AS VARCHAR(MAX)=''
SET @vcUserLoginEmail = (SELECT 
distinct  
    stuff((
        select ',' +  replace(vcEmailID,'''','''''') FROM UserMaster AS U where numDomainID=@numDomainID 
		order by U.vcEmailID
        for xml path('')
    ),1,1,'') as userlist
from UserMaster where numDomainID=@numDomainID 
group by vcEmailID)
set @vcMessageFrom=replace(@vcMessageFrom,'''','|')
                         
if @filter>0 set @tintSortOrder=  @filter
              
if ((@filter <=2 and @tintSortOrder<=2) or (@tintSortOrder>2 and @filter>2))                 
begin  
                                                                  
 declare @strSql as nvarchar(MAX)  =''     
                                  
  declare @firstRec as integer                                                        
  declare @lastRec as integer                                                        
  set @firstRec= (@CurrentPage-1) * @PageSize                                                        
  set @lastRec= (@CurrentPage*@PageSize+1)                 
  set @strSql=''                

 if (@filter <=2 and @tintSortOrder<=2 and @bitOpenCommu=0)                 
 begin  
 
 declare @strCondition as varchar(MAX);
 declare @vcContactEmail as varchar(MAX)='';
              
  if @numdivisionId =0 and  @vcMessageFrom <> '' 
 BEGIN 
	If @filter = 1 --ReceivedMessages
		set  @strCondition=' and (vcFrom like ''%'+ REPLACE(@vcMessageFrom,'''','''''')+'%'')  ' 
	ELSE IF @filter=2 --SentMessages
		set  @strCondition=' and (vcTo like ''%'+ REPLACE(@vcMessageFrom,'''','''''')+'%'')  ' 
	ELSE
		set  @strCondition=' and (vcFrom like ''%'+ REPLACE(@vcMessageFrom,'''','''''')+'%'' or vcTo like ''%'+ REPLACE(@vcMessageFrom,'''','''''')+'%'')  ' 
 END
  else if @numdivisionId <>0 
 BEGIN

 /* This approach does not work as numEmailId stores who sent email, id of sender
  set @strCondition=' and HDR.numEmailID in (
  SELECT numEmailID FROM dbo.EmailMaster WHERE numDomainID= ' + CONVERT(VARCHAR(15),@numDomainID) +' AND numContactID 
  IN (SELECT numcontactid FROM dbo.AdditionalContactsInformation WHERE numDivisionID = ' + convert(varchar(15),@numDivisionID) + ' AND numDomainID = ' + CONVERT(VARCHAR(15),@numDomainID) +' ))';
*/
-- bug fix id 1591,1628 
  DECLARE @TnumContactID NUMERIC(9);set @strCondition=''

  select TOP 1 @TnumContactID=numContactID,@vcContactEmail=replace(vcEmail,'''','''''') from AdditionalContactsInformation where numdivisionid=@numdivisionId and len(vcEmail)>2
  ORDER BY numContactID ASC
  SET @vcContactEmail = ISNULL(@vcContactEmail,'')
  WHILE @TnumContactID>0
  BEGIN
   if len(@strCondition)>0
    SET @strCondition=@strCondition + ' or '

	If @filter = 1 --ReceivedMessages
		set  @strCondition = @strCondition + ' (vcFrom like ''%'+ REPLACE(@vcContactEmail,'''','''''') +'%'')  ' 
	ELSE IF @filter=2 --SentMessages
		set  @strCondition= @strCondition + ' (vcTo like ''%'+ REPLACE(@vcContactEmail,'''','''''') +'%'')  ' 
	ELSE
		set  @strCondition= @strCondition + ' (vcFrom like ''%'+ REPLACE(@vcContactEmail,'''','''''')+'%'' or vcTo like ''%'+ REPLACE(@vcContactEmail,'''','''''')+'%'')  '

   select TOP 1 @TnumContactID=numContactID,@vcContactEmail=vcEmail from AdditionalContactsInformation where numdivisionid=@numdivisionId and len(vcEmail)>2 and numContactID>@TnumContactID
   ORDER BY numContactID ASC

   IF @@rowcount = 0 SET @TnumContactID = 0 
  END
 
  if len(@strCondition)>0
   SET @strCondition= 'and (' + @strCondition + ')'
  else if @tintMode <> 6 AND @tintMode <> 5
   SET @strCondition=' and (1=0)' 
  --set @strCondition=' and DTL.numEmailId  in (select numEmailId from EmailMaster where vcEmailID in (select vcEmail  from AdditionalContactsInformation where numdivisionid='''+ convert(varchar(15),@numdivisionId)+''' ))    '          
  END
  else if @numContactId>0 
 BEGIN
  select @vcContactEmail=isnull(replace(vcEmail,'''',''''''),'') from AdditionalContactsInformation where numContactID=@numContactId and len(vcEmail)>2
  SET @vcContactEmail = ISNULL(@vcContactEmail,'')
  if len(@vcContactEmail)>0
	If @filter = 1 --ReceivedMessages
		set  @strCondition=' and (vcFrom like ''%'+ REPLACE(@vcContactEmail,'''','''''') +'%'')  ' 
	ELSE IF @filter=2 --SentMessages
		set  @strCondition=' and (vcTo like ''%'+ REPLACE(@vcContactEmail,'''','''''')+'%'')  ' 
	ELSE
		set  @strCondition=' and (vcFrom like ''%'+ REPLACE(@vcContactEmail,'''','''''')+'%'' or vcTo like ''%'+ REPLACE(@vcContactEmail,'''','''''')+'%'')' 
  else if @tintMode <> 6 AND @tintMode <> 5
   SET @strCondition=' and (1=0)'
 END
 else 
  SET @strCondition=''

  CREATE TABLE #TEMPData
  (
		numTotalRecords INT
		,tintCountType TINYINT
		,numEmailHstrID NUMERIC(18,0)
		,DelData VARCHAR(200)
		,bintCreatedOn DATETIME
		,[date] VARCHAR(100)
		,[Subject] VARCHAR(MAX)
		,[type] VARCHAR(100)
		,phone VARCHAR(100)
		,assignedto VARCHAR(100)
		,caseid NUMERIC(18,0)
		,vcCasenumber VARCHAR(200)
		,caseTimeid NUMERIC(18,0)
		,caseExpid NUMERIC(18,0)
		,tinttype NUMERIC(18,0)
		,dtCreatedDate DATETIME
		,[From] VARCHAR(MAX)
		,bitClosedflag BIT
		,bitHasAttachments BIT
		,vcBody VARCHAR(MAX)
		,InlineEdit VARCHAR(1000)
		,numCreatedBy NUMERIC(18,0)
		,numOrgTerId NUMERIC(18,0)
		,TypeClass VARCHAR(100)
  )


  set @strSql= 'INSERT INTO 
					#TEMPData 
				SELECT 
					COUNT(*) OVER() AS numTotalRecords
					,1 AS tintCountType
					,HDR.numEmailHstrID
					,convert(varchar(15),HDR.numEmailHstrID)+''~1~0''+convert(varchar(15),HDR.numUserCntId) as DelData
					,HDR.dtReceivedOn AS bintCreatedOn
					,dbo.FormatedDateTimeFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtReceivedOn),'+convert(varchar(15),@numDomainID)+') as [date]
					,vcSubject as [Subject]
					,CASE WHEN (SELECT COUNT(*) FROM dbo.SplitByString(vcFrom,''$^$'') WHERE Items 
					IN( select  replace(vcEmailID,'''''''','''''''') FROM UserMaster AS U where numDomainID='+convert(varchar(15),@numDomainID)+'))  > 0  THEN ''Email Out'' ELSE ''Email In'' END as [type]
					,'''' as phone
					,'''' as assignedto
					,0 as caseid
					,null as vcCasenumber
					,0 as caseTimeid
					,0 as caseExpid
					,hdr.tinttype
					,HDR.dtReceivedOn as dtCreatedDate
					,vcFrom + '','' + vcTo as [From]
					,0 as bitClosedflag
					,ISNULL(HDR.bitHasAttachments,0) bitHasAttachments
					,CASE WHEN LEN(Cast(vcBodyText AS VARCHAR(1000)))>150 THEN SUBSTRING(vcBodyText,0,150) + ''...'' ELSE Cast(vcBodyText AS VARCHAR(1000)) END AS vcBody
					,''''  AS InlineEdit
					,HDR.numUserCntId AS numCreatedBy
					,0 AS numOrgTerId
					,CASE WHEN (SELECT COUNT(*) FROM dbo.SplitByString(vcFrom,''$^$'') WHERE Items 
					IN( select  replace(vcEmailID,'''''''','''''''') FROM UserMaster AS U where numDomainID='+convert(varchar(15),@numDomainID)+'))  > 0 THEN ''redColorLabel'' ELSE ''greenColorLabel'' END as TypeClass
			  FROM EmailHistory HDR                                 
			  LEFT JOIN [Correspondence] CR ON CR.numEmailHistoryID = HDR.numEmailHstrID ' 
     

   set  @strSql=@strSql +' where ISNULL(HDR.bitInvisible,0) = 0 AND  (HDR.numNodeId IN (SELECT numNodeID FROM InboxTreeSort WHERE vcNodeName NOT IN (''All Mail'',''Important'',''Drafts'') AND numDomainID='+convert(varchar(15),@numDomainID)+')  OR HDR.numNodeId=0) AND HDR.numDomainID='+convert(varchar(15),@numDomainID)
   + ' AND (CR.numOpenRecordID = '+ convert(varchar(15),@numOpenRecordID) +' OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
   + (CASE WHEN ISNULL(@vcTypes,'') <> '' THEN ' AND 1 = 0' ELSE ' AND 1 = 1' END)
   
   IF(@FromDate IS NOT NULL AND @FromDate<>'')
   BEGIN
  SET @strSql = @strSql +' and  (dtReceivedOn between '''+ convert(varchar(30),@FromDate)+''' and '''+ convert(varchar(30),@ToDate)+''') '
  END
 set  @strSql=@strSql+ @strCondition


  IF @tintMode=1 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(1,3) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=2 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(2,4) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=5 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(5) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=6 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(6) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  
  if @filter =0 set  @strSql=@strSql+ ' and (hdr.tinttype=1 or hdr.tinttype=2 or  hdr.tinttype=5)'                  
  else if @filter =1 OR @filter =2 set  @strSql=@strSql+ ' and (hdr.tinttype=1 OR hdr.tinttype=2)'                                             
  if @filter > 2 set  @strSql=@strSql+ ' and hdr.tinttype=0'                 
                        
  if @tintSortOrder =0 and @SeachKeyword <> ''                 
  begin                
   set  @strSql=@strSql+ ' and (vcFrom like ''%'+ @SeachKeyword+'%'' or vcTo like ''%'+ @SeachKeyword+'%'' or vcsubject like ''%'+@SeachKeyword+'%'' or vcBody like ''%'+@SeachKeyword+'%'')'               
  end                
  if @tintSortOrder =1 and @SeachKeyword <> ''                 
  begin                
     set  @strSql=@strSql+ ' and (vcFrom like ''%'+ @SeachKeyword+'%'' or vcsubject like ''%'+@SeachKeyword+'%'' or  vcBody like ''%'+@SeachKeyword+'%'')'                 
  end                
  if @tintSortOrder =2 and @SeachKeyword <> ''                 
  begin                
     set  @strSql=@strSql+ ' and (vcTo like ''%'+ @SeachKeyword+'%'' or vcsubject like ''%'+@SeachKeyword+'%'' or  vcBody like ''%'+@SeachKeyword+'%'')'                 
  end                            
 end                

 

 if ((@filter=0 or @filter>2) and (@tintSortOrder=0 or @tintSortOrder>2))                
 begin                
  set  @strSql=@strSql+ '                     
  INSERT INTO #TEMPData SELECT COUNT(*) OVER() AS numTotalRecords, 2 AS tintCountType, c.numCommId as numEmailHstrID,convert(varchar(15),C.numCommId)+''~2~''+convert(varchar(15),C.numCreatedBy) as DelData,CASE WHEN c.bitTask = 973 THEN dtCreatedDate WHEN c.bitTask = 974 THEN dtCreatedDate WHEN c.bitTask = 971 THEN dtStartTime ELSE dtEndTime END AS bintCreatedOn,
  case when c.bitTask=973 then dbo.FormatedDateTimeFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtStartTime),'+convert(varchar(15),@numDomainID)+') 
  when c.bitTask=974 then dbo.FormatedDateTimeFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtStartTime),'+convert(varchar(15),@numDomainID)+') 
  when c.bitTask=971 THEN dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtEndTime),'+convert(varchar(15),@numDomainID)+') + '' '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtStartTime]),100),7)  +'' - '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtEndTime]),100),7)
  else  dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtEndTime),'+convert(varchar(15),@numDomainID)+') + '' '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtStartTime]),100),7)  +'' - '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtEndTime]),100),7)  END AS [date],
  convert(varchar(8000),textdetails) as [Subject],                                                                         
 dbo.fn_GetListItemName(c.bitTask)  AS [Type],                                                                 
  A1.vcFirstName +'' ''+ A1.vcLastName +'' ''+                                      
  case when A1.numPhone<>'''' then + A1.numPhone +case when A1.numPhoneExtension<>'''' then '' - '' + A1.numPhoneExtension else '''' end  else '''' end as [Phone],                        
  A2.vcFirstName+ '' ''+A2.vcLastName  as assignedto,                         
  c.caseid,                           
  (select top 1 vcCaseNumber from cases where cases.numcaseid= c.caseid )as vcCasenumber,                               
  isnull(caseTimeid,0)as caseTimeid,                              
  isnull(caseExpid,0)as caseExpid  ,                        
  1 as tinttype ,dtCreatedDate,'''' as [FROM]     ,isnull(C.bitClosedflag,0) as bitClosedflag,0 as bitHasAttachments,
'''' as vcBody,
''id="Tickler~184~False~''+CAST(c.bitTask AS VARCHAR)+''~''+CAST(c.numCommId AS VARCHAR)+''" class="editable_textarea" onmousemove="bgColor=''''lightgoldenRodYellow''''"  title="Double click to edit..." bgcolor="lightgoldenRodYellow" '' AS InlineEdit,
C.numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
''blueColorLabel'' as TypeClass
  from  Communication C                  
  JOIN AdditionalContactsInformation  A1                                           
  ON c.numContactId = A1.numContactId
  LEFT JOIN CommunicationLinkedOrganization clo ON C.numCommID=clo.numCommID                                                                             
  left join AdditionalContactsInformation A2 on A2.numContactID=c.numAssign
  left join UserMaster UM on A2.numContactid=UM.numUserDetailId and isnull(UM.bitActivateFlag,0)=1                                                                           
  left join listdetails on numActivity=numListID                           
  LEFT JOIN [Correspondence] CR ON CR.numCommID = C.numCommID
  JOIN DivisionMaster Div                                                                         
 ON A1.numDivisionId = Div.numDivisionID    
  where  C.numDomainID=' +convert(varchar(15),@numDomainID)
  + (CASE WHEN ISNULL(@vcTypes,'') <> '' THEN ' AND C.bitTask IN (SELECT Id FROM dbo.SplitIDs(''' + ISNULL(@vcTypes,'') + ''','',''))' ELSE '' END) + 
  + ' AND (numOpenRecordID = '+ convert(varchar(15),@numOpenRecordID) +' OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  if (@numdivisionId =0 and @numContactId<>0) set  @strSql=@strSql+' and C.numContactId='+ convert(varchar(15),@numContactId)                        
  if @numdivisionId <> 0 set  @strSql=@strSql+' and  (C.numDivisionId='+ convert(varchar(15),@numdivisionId) + ' OR clo.numDivisionID='+ convert(varchar(15),@numdivisionId) + ') '             
  if @numCaseID>0  set  @strSql=@strSql+' and  C.CaseId='+ convert(varchar(15),@numCaseID)                       
  if @filter =0  
  BEGIN
	IF @tintMode <> 9
	BEGIN
		
		IF(@FromDate IS NOT NULL AND @FromDate<>'')
		BEGIN
			SET @strSql=@strSql+ ' and dtStartTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtStartTime  <= '''+ convert(varchar(30),@ToDate)  +''''
		END
	END
  END
  else
    SET @strSql=@strSql+ ' and c.bitTask=' + CONVERT(varchar(12),@filter)  
		IF(@FromDate IS NOT NULL AND @FromDate<>'')
		BEGIN
			SET @strSql=@strSql+ ' and ((dtStartTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtStartTime  <= '''+ convert(varchar(30),@ToDate)  +''') OR (dtEndTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtEndTime  <= '''+ convert(varchar(30),@ToDate)  +'''))'
		END
  IF @tintMode=1 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(1,3) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=2 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(2,4) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=5 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(5) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=6 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(6) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
                          
  IF @tintSortOrder =0 and @SeachKeyword <>'' 
	SET @strSql=@strSql+ ' and textdetails like ''%'+@SeachKeyword+'%'''
  ELSE
	IF ISNULL(@filter,0) = 0 
	BEGIN
		IF @SeachKeyword <>''
		BEGIN
			SET @strSql=@strSql+ ' and textdetails like ''%'+@SeachKeyword+'%'''
		END
	END
	ELSE
	BEGIN
		SET @strSql=@strSql+ ' and c.bitTask=' + CONVERT(varchar(12),@filter)
		
		IF @SeachKeyword <>''
		BEGIN
			SET @strSql= @strSql + ' and textdetails like ''%'+@SeachKeyword+'%'''
		END                     
	END
  IF @bitOpenCommu=1  SET @strSql=@strSql+ ' and isnull(C.bitClosedflag,0)=0'
 end                




 --set @strSql='With tblCorr as (SELECT ROW_NUMBER() OVER (ORDER BY '+@columnName+' '+ @columnSortOrder+') AS RowNumber,X.* from('+@strSql+')X)'                

 set @strSql= CONCAT(@strSql,'; SELECT ROW_NUMBER() OVER(ORDER BY bintCreatedOn DESC) RowNumber, * FROM #TEMPData ORDER BY bintCreatedOn DESC OFFSET ',((@CurrentPage-1) * @PageSize),' ROWS FETCH NEXT ',@PageSize,' ROWS ONLY;')
                    

print CAST(@strSql AS NTEXT)          

set @strSql = CONCAT(@strSql,'; SET @TotRecs = ISNULL((SELECT SUM(numTotalRecords) FROM (SELECT tintCountType,numTotalRecords FROM #TEMPData GROUP BY tintCountType,numTotalRecords) X),0)')
EXEC sp_executesql @strSql,N'@TotRecs INT OUTPUT',@TotRecs OUTPUT;
                   
IF OBJECT_ID(N'tempdb..#TEMPData') IS NOT NULL
BEGIN
	DROP TABLE #TEMPData
END
                
              
end              
END
GO

--created by anoop jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdomaindetails')
DROP PROCEDURE usp_getdomaindetails
GO
CREATE PROCEDURE [dbo].[USP_GetDomainDetails]                                          
@numDomainID as numeric(9)=0                                          
as           

DECLARE @listIds VARCHAR(MAX)
SELECT @listIds = COALESCE(@listIds+',' ,'') + CAST(numUserID AS VARCHAR(100)) FROM UnitPriceApprover WHERE numDomainID = @numDomainID
   
DECLARE @canChangeDiferredIncomeSelection AS BIT = 1

IF EXISTS(SELECT 
			OB.numOppBizDocsId 
		FROM 
			OpportunityBizDocs OB 
		INNER JOIN 
			OpportunityMaster OM 
		ON 
			OB.numOppId=OM.numOppId 
		WHERE 
			numBizDocId=304 
			AND OM.numDomainId=@numDomainID)
BEGIN
	SET @canChangeDiferredIncomeSelection = 0
END   
   
                               
select                                           
vcDomainName,                                           
isnull(vcDomainDesc,'') vcDomainDesc,                                           
isnull(bitExchangeIntegration,0) bitExchangeIntegration,                                           
isnull(bitAccessExchange,0) bitAccessExchange,                                           
isnull(vcExchUserName,'') vcExchUserName,                                           
isnull(vcExchPassword,'') vcExchPassword,                                           
isnull(vcExchPath,'') vcExchPath,                                           
isnull(vcExchDomain,'') vcExchDomain,                                         
tintCustomPagingRows,                                         
vcDateFormat,                                         
numDefCountry,                                         
tintComposeWindow,                                         
sintStartDate,                                         
sintNoofDaysInterval,                                         
tintAssignToCriteria,                                         
bitIntmedPage,
ISNULL(D.numCost,0) as numCost,                                         
tintFiscalStartMonth,                                    
--isnull(bitDeferredIncome,0) as bitDeferredIncome,                                  
isnull(tintPayPeriod,0) as tintPayPeriod,                                
isnull(numCurrencyID,0) as numCurrencyID,                            
isnull(vcCurrency,'') as vcCurrency,                      
isnull(charUnitSystem,'') as charUnitSystem ,                    
isnull(vcPortalLogo,'') as vcPortalLogo,                  
isnull(tintChrForComSearch,2) as tintChrForComSearch,                
isnull(tintChrForItemSearch,'') as tintChrForItemSearch,            
isnull(intPaymentGateWay,0) as intPaymentGateWay  ,        
 case when bitPSMTPServer= 1 then vcPSMTPServer else '' end as vcPSMTPServer  ,              
 case when bitPSMTPServer= 1 then  isnull(numPSMTPPort,0)  else 0 end as numPSMTPPort          
,isnull(bitPSMTPAuth,0) bitPSMTPAuth        
,isnull([vcPSmtpPassword],'')vcPSmtpPassword        
,isnull(bitPSMTPSSL,0) bitPSMTPSSL     
,isnull(bitPSMTPServer,0) bitPSMTPServer  ,isnull(vcPSMTPUserName,'') vcPSMTPUserName,      
--isnull(numDefaultCategory,0) numDefaultCategory,
isnull(numShipCompany,0)   numShipCompany,
isnull(bitAutoPopulateAddress,0) bitAutoPopulateAddress,
isnull(tintPoulateAddressTo,0) tintPoulateAddressTo,
isnull(bitMultiCompany,0) bitMultiCompany,
isnull(bitMultiCurrency,0) bitMultiCurrency,
ISNULL(bitCreateInvoice,0) bitCreateInvoice,
ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
ISNULL([bitSaveCreditCardInfo],0) bitSaveCreditCardInfo,
ISNULL([intLastViewedRecord],10) intLastViewedRecord,
ISNULL([tintCommissionType],1) tintCommissionType,
ISNULL([tintBaseTax],2) tintBaseTax,
ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
ISNULL(bitEmbeddedCost,0) bitEmbeddedCost,
ISNULL(numDefaultSalesOppBizDocId,0) numDefaultSalesOppBizDocId,
ISNULL(tintSessionTimeOut,1) tintSessionTimeOut,
ISNULL(tintDecimalPoints,2) tintDecimalPoints,
ISNULL(bitCustomizePortal,0) bitCustomizePortal,
ISNULL(tintBillToForPO,0) tintBillToForPO,
ISNULL(tintShipToForPO,0) tintShipToForPO,
ISNULL(tintOrganizationSearchCriteria,1) tintOrganizationSearchCriteria,
ISNULL(tintLogin,0) tintLogin,
lower(ISNULL(vcPortalName,'')) vcPortalName,
ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
D.numDomainID,
ISNULL(vcPSMTPDisplayName,'') vcPSMTPDisplayName,
ISNULL(bitGtoBContact,0) bitGtoBContact,
ISNULL(bitBtoGContact,0) bitBtoGContact,
ISNULL(bitGtoBCalendar,0) bitGtoBCalendar,
ISNULL(bitBtoGCalendar,0) bitBtoGCalendar,
ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
ISNULL(bitInlineEdit,0) bitInlineEdit,
ISNULL(bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
ISNULL(bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
isnull(D.bitRentalItem,0) as bitRentalItem,
isnull(D.numRentalItemClass,0) as numRentalItemClass,
isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
ISNULL(D.bitAutoSerialNoAssign,0) as bitAutoSerialNoAssign,
ISNULL(D.bitIsShowBalance,0) AS [bitIsShowBalance],
ISNULL(numIncomeAccID,0) AS [numIncomeAccID],
ISNULL(numCOGSAccID,0) AS [numCOGSAccID],
ISNULL(numAssetAccID,0) AS [numAssetAccID],
ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
ISNULL(D.vcShipToPhoneNo,0) AS vcShipToPhoneNo,
ISNULL(D.vcGAUserEMail,'') AS vcGAUserEMail,
ISNULL(D.vcGAUserPassword,'') AS vcGAUserPassword,
ISNULL(D.vcGAUserProfileId,0) AS vcGAUserProfileId,
ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
ISNULL(D.vcHideTabs,'') AS vcHideTabs,
ISNULL(D.bitUseBizdocAmount,0) AS bitUseBizdocAmount,
ISNULL(D.bitDefaultRateType,0) AS bitDefaultRateType,
ISNULL(D.bitIncludeTaxAndShippingInCommission,0) AS bitIncludeTaxAndShippingInCommission,
/*,ISNULL(D.bitAllowPPVariance,0) AS bitAllowPPVariance*/
ISNULL(D.bitPurchaseTaxCredit,0) AS bitPurchaseTaxCredit,
ISNULL(D.bitLandedCost,0) AS bitLandedCost,
ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
ISNULL(AP.numAbovePercent,0) AS numAbovePercent,
ISNULL(AP.numBelowPercent,0) AS numBelowPercent,
ISNULL(AP.numBelowPriceField,0) AS numBelowPriceField,
@listIds AS vcUnitPriceApprover,
ISNULL(D.numDefaultSalesPricing,2) AS numDefaultSalesPricing,
ISNULL(D.bitReOrderPoint,0) AS bitReOrderPoint,
ISNULL(D.numReOrderPointOrderStatus,0) AS numReOrderPointOrderStatus,
ISNULL(D.numPODropShipBizDoc,0) AS numPODropShipBizDoc,
ISNULL(D.numPODropShipBizDocTemplate,0) AS numPODropShipBizDocTemplate,
ISNULL(D.IsEnableDeferredIncome,0) AS IsEnableDeferredIncome,
ISNULL(D.bitApprovalforTImeExpense,0) AS bitApprovalforTImeExpense,
ISNULL(D.intTimeExpApprovalProcess,0) AS intTimeExpApprovalProcess,
ISNULL(D.bitApprovalforOpportunity,0) AS bitApprovalforOpportunity,
ISNULL(D.intOpportunityApprovalProcess,0) AS intOpportunityApprovalProcess,
@canChangeDiferredIncomeSelection AS canChangeDiferredIncomeSelection,
ISNULL(CAST(D.numDefaultSalesShippingDoc AS int),0) AS numDefaultSalesShippingDoc,
ISNULL(CAST(D.numDefaultPurchaseShippingDoc AS int),0) AS numDefaultPurchaseShippingDoc,
ISNULL(D.bitchkOverRideAssignto,0) AS bitchkOverRideAssignto,
ISNULL(D.vcPrinterIPAddress,'') vcPrinterIPAddress,
ISNULL(D.vcPrinterPort,'') vcPrinterPort,
ISNULL(AP.bitCostApproval,0) bitCostApproval
,ISNULL(AP.bitListPriceApproval,0) bitListPriceApproval
,ISNULL(AP.bitMarginPriceViolated,0) bitMarginPriceViolated
,ISNULL(vcSalesOrderTabs,'Sales Orders') AS vcSalesOrderTabs
,ISNULL(vcSalesQuotesTabs,'Sales Quotes') AS vcSalesQuotesTabs
,ISNULL(vcItemPurchaseHistoryTabs,'Item Purchase History') AS vcItemPurchaseHistoryTabs
,ISNULL(vcItemsFrequentlyPurchasedTabs,'Items Frequently Purchased') AS vcItemsFrequentlyPurchasedTabs
,ISNULL(vcOpenCasesTabs,'Open Cases') AS vcOpenCasesTabs
,ISNULL(vcOpenRMATabs,'Open RMAs') AS vcOpenRMATabs
,ISNULL(bitSalesOrderTabs,0) AS bitSalesOrderTabs
,ISNULL(bitSalesQuotesTabs,0) AS bitSalesQuotesTabs
,ISNULL(bitItemPurchaseHistoryTabs,0) AS bitItemPurchaseHistoryTabs
,ISNULL(bitItemsFrequentlyPurchasedTabs,0) AS bitItemsFrequentlyPurchasedTabs
,ISNULL(bitOpenCasesTabs,0) AS bitOpenCasesTabs
,ISNULL(bitOpenRMATabs,0) AS bitOpenRMATabs
,ISNULL(bitSupportTabs,0) AS bitSupportTabs
,ISNULL(vcSupportTabs,'Support') AS vcSupportTabs
,ISNULL(D.numDefaultSiteID,0) AS numDefaultSiteID
,ISNULL(tintOppStautsForAutoPOBackOrder,0) AS tintOppStautsForAutoPOBackOrder
,ISNULL(tintUnitsRecommendationForAutoPOBackOrder,1) AS tintUnitsRecommendationForAutoPOBackOrder
,ISNULL(bitRemoveGlobalLocation,0) AS bitRemoveGlobalLocation
,ISNULL(numAuthorizePercentage,0) AS numAuthorizePercentage
,ISNULL(bitEDI,0) AS bitEDI
,ISNULL(bit3PL,0) AS bit3PL
,ISNULL(numListItemID,0) AS numListItemID
,ISNULL(bitAllowDuplicateLineItems,0) bitAllowDuplicateLineItems
,ISNULL(tintCommitAllocation,1) tintCommitAllocation
,ISNULL(tintInvoicing,1) tintInvoicing
,ISNULL(tintMarkupDiscountOption,1) tintMarkupDiscountOption
,ISNULL(tintMarkupDiscountValue,1) tintMarkupDiscountValue
,ISNULL(bitIncludeRequisitions,0) bitIncludeRequisitions
,ISNULL(bitCommissionBasedOn,0) bitCommissionBasedOn
,ISNULL(tintCommissionBasedOn,0) tintCommissionBasedOn
,ISNULL(bitDoNotShowDropshipPOWindow,0) bitDoNotShowDropshipPOWindow
,ISNULL(tintReceivePaymentTo,2) tintReceivePaymentTo
,ISNULL(numReceivePaymentBankAccount,0) numReceivePaymentBankAccount
,ISNULL(numOverheadServiceItemID,0) numOverheadServiceItemID,
ISNULL(D.bitDisplayCustomField,0) AS bitDisplayCustomField,
ISNULL(D.bitFollowupAnytime,0) AS bitFollowupAnytime,
ISNULL(D.bitpartycalendarTitle,0) AS bitpartycalendarTitle,
ISNULL(D.bitpartycalendarLocation,0) AS bitpartycalendarLocation,
ISNULL(D.bitpartycalendarDescription,0) AS bitpartycalendarDescription,
ISNULL(D.bitREQPOApproval,0) AS bitREQPOApproval,
ISNULL(D.bitARInvoiceDue,0) AS bitARInvoiceDue,
ISNULL(D.bitAPBillsDue,0) AS bitAPBillsDue,
ISNULL(D.bitItemsToPickPackShip,0) AS bitItemsToPickPackShip,
ISNULL(D.bitItemsToInvoice,0) AS bitItemsToInvoice,
ISNULL(D.bitSalesOrderToClose,0) AS bitSalesOrderToClose,
ISNULL(D.bitItemsToPutAway,0) AS bitItemsToPutAway,
ISNULL(D.bitItemsToBill,0) AS bitItemsToBill,
ISNULL(D.bitPosToClose,0) AS bitPosToClose,
ISNULL(D.bitPOToClose,0) AS bitPOToClose,
ISNULL(D.bitBOMSToPick,0) AS bitBOMSToPick,

ISNULL(D.vchREQPOApprovalEmp,'') AS vchREQPOApprovalEmp,
ISNULL(D.vchARInvoiceDue,'') AS vchARInvoiceDue,
ISNULL(D.vchAPBillsDue,'') AS vchAPBillsDue,
ISNULL(D.vchItemsToPickPackShip,'') AS vchItemsToPickPackShip,
ISNULL(D.vchItemsToInvoice,'') AS vchItemsToInvoice,
ISNULL(D.vchPriceMarginApproval,'') AS vchPriceMarginApproval,
ISNULL(D.vchSalesOrderToClose,'') AS vchSalesOrderToClose,
ISNULL(D.vchItemsToPutAway,'') AS vchItemsToPutAway,
ISNULL(D.vchItemsToBill,'') AS vchItemsToBill,
ISNULL(D.vchPosToClose,'') AS vchPosToClose,
ISNULL(D.vchPOToClose,'') AS vchPOToClose,
ISNULL(D.vchBOMSToPick,'') AS vchBOMSToPick,
ISNULL(D.decReqPOMinValue,0) AS decReqPOMinValue,
ISNULL(D.bitUseOnlyActionItems,0) AS bitUseOnlyActionItems,
ISNULL(D.vcElectiveItemFields,'') AS vcElectiveItemFields,
ISNULL(tintMailProvider,3) tintMailProvider,
ISNULL(D.bitDisplayContractElement,0) AS bitDisplayContractElement,
ISNULL(D.vcEmployeeForContractTimeElement,'') AS vcEmployeeForContractTimeElement,
ISNULL(numARContactPosition,0) numARContactPosition,
ISNULL(bitShowCardConnectLink,0) bitShowCardConnectLink,
ISNULL(vcBluePayFormName,'') vcBluePayFormName,
ISNULL(vcBluePaySuccessURL,'') vcBluePaySuccessURL,
ISNULL(vcBluePayDeclineURL,'') vcBluePayDeclineURL,
ISNULL(bitUseDeluxeCheckStock,0) bitUseDeluxeCheckStock,
ISNULL(D.bitEnableSmartyStreets,0) bitEnableSmartyStreets,
ISNULL(D.vcSmartyStreetsAPIKeys,'') vcSmartyStreetsAPIKeys,
ISNULL(bitReceiveOrderWithNonMappedItem,0) bitReceiveOrderWithNonMappedItem,
ISNULL(numItemToUseForNonMappedItem,0) numItemToUseForNonMappedItem
from Domain D  
LEFT JOIN eCommerceDTL eComm  on eComm.numDomainID=D.numDomainID  
LEFT JOIN ApprovalProcessItemsClassification AP ON AP.numDomainID = D.numDomainID
where (D.numDomainID=@numDomainID OR @numDomainID=-1)


GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_gettableinfodefault' ) 
    DROP PROCEDURE usp_gettableinfodefault
GO
CREATE PROCEDURE [dbo].[usp_GetTableInfoDefault]                                                                        
@numUserCntID numeric=0,                        
@numRecordID numeric=0,                                    
@numDomainId numeric=0,                                    
@charCoType char(1) = 'a',                        
@pageId as numeric,                        
@numRelCntType as NUMERIC,                        
@numFormID as NUMERIC,                        
@tintPageType TINYINT                                               
AS                                      
BEGIN
	-- VENDORS ARE PROMATED TO ACCOUNT BUT IT SHOULD STILL CONTINUTE TO SHOW VENDOR DETAILS FIELDS
	IF @numRelCntType = 47 AND @numFormID = 36
	BEGIN
		SET @numFormID = 35
	END
                                   
IF (
	SELECT 
		ISNULL(sum(TotalRow),0)  
	FROM
		(            
			Select count(*) TotalRow FROM View_DynamicColumns WHERE numUserCntId=@numuserCntid AND numDomainId = @numDomainId AND numFormID=@numFormID AND numRelCntType=@numRelCntType AND tintPageType=@tintPageType
			Union 
			Select count(*) TotalRow from View_DynamicCustomColumns WHERE numUserCntId=@numuserCntid AND numDomainId = @numDomainId AND numFormID=@numFormID AND numRelCntType=@numRelCntType AND tintPageType=@tintPageType
		) TotalRows
	) <> 0              
BEGIN  
	IF @numFormID=88 AND @pageId=5 AND @numRelCntType=0 AND @tintPageType=2
	BEGIN
		IF NOT EXISTS (SELECT numFieldID FROM View_DynamicColumns WHERE numDomainId = @numDomainId AND numUserCntId=@numuserCntid AND numFormID=@numFormID AND numRelCntType=@numRelCntType AND tintPageType=@tintPageType AND numFieldID=212)
		BEGIN
			INSERT INTO DycFormConfigurationDetails
			(
				numFormId,numFieldId,numViewId,intColumnNum,intRowNum,numDomainId,numUserCntID,numRelCntType,tintPageType,bitCustom
			)
			VALUES
			(
				@numFormID
				,212
				,0
				,1
				,ISNULL((SELECT MAX(tintRow) + 1 FROM View_DynamicColumns WHERE numDomainId = @numDomainId AND numUserCntId=@numuserCntid AND numFormID=@numFormID AND numRelCntType=@numRelCntType AND tintPageType=@tintPageType),0)
				,@numDomainId
				,@numUserCntID
				,@numRelCntType
				,@tintPageType
				,0
			)
		END

		IF NOT EXISTS (SELECT numFieldID FROM View_DynamicColumns WHERE numDomainId = @numDomainId AND numUserCntId=@numuserCntid AND numFormID=@numFormID AND numRelCntType=@numRelCntType AND tintPageType=@tintPageType AND numFieldID=216)
		BEGIN
			INSERT INTO DycFormConfigurationDetails
			(
				numFormId,numFieldId,numViewId,intColumnNum,intRowNum,numDomainId,numUserCntID,numRelCntType,tintPageType,bitCustom
			)
			VALUES
			(
				@numFormID
				,216
				,0
				,1
				,ISNULL((SELECT MAX(tintRow) + 1 FROM View_DynamicColumns WHERE numDomainId = @numDomainId AND numUserCntId=@numuserCntid AND numFormID=@numFormID AND numRelCntType=@numRelCntType AND tintPageType=@tintPageType),0)
				,@numDomainId
				,@numUserCntID
				,@numRelCntType
				,@tintPageType
				,0
			)
		END
	END
	                         
IF @pageid=1 or @pageid=4 or @pageid= 12 or  @pageid= 13 or  @pageid= 14
BEGIN
    SELECT 
		numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,'' as vcURL,vcAssociatedControlType as fld_type
		,tintRow,tintcolumn as intcoulmn,'' as vcValue,bitCustom AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated
		,numListID,0 numListItemID,PopupFunctionName
		,CASE 
			WHEN vcAssociatedControlType='SelectBox' 
			THEN ISNULL((SELECT numPrimaryListID FROM FieldRelationship WHERE numSecondaryListID=numListID AND numDomainID=@numDomainId),0) 
			ELSE 0 
		END AS ListRelID
		,CASE 
			WHEN vcAssociatedControlType='SelectBox' 
			THEN (SELECT COUNT(*) FROM FieldRelationship WHERE numPrimaryListID=numListID AND numDomainID=@numDomainId) 
			ELSE 0 
		END AS DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
		,(SELECT TOP 1 FldDTLID from CFW_FLD_Values where  RecId=@numRecordID and Fld_Id=numFieldId) as FldDTLID
		,'' as Value,vcDbColumnName,vcToolTip,intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
	FROM 
		View_DynamicColumns
	WHERE 
		numFormId=@numFormID 
		AND numUserCntID=@numUserCntID 
		AND numDomainID=@numDomainID  
		AND ISNULL(bitCustom,0)=0 
		AND numRelCntType=@numRelCntType 
		AND tintPageType=@tintPageType 
		AND 1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
				WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
				ELSE 0 END) 
	UNION 
	SELECT 
		numFieldId,vcfieldName ,vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,
		CASE 
			WHEN vcAssociatedControlType = 'SelectBox' 
			THEN (SELECT vcData FROM listdetails WHERE numlistitemid = dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID))
			WHEN vcAssociatedControlType = 'CheckBoxList' 
			THEN STUFF((SELECT CONCAT(',', vcData) FROM ListDetails WHERE numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID),',')) FOR XML PATH('')), 1, 1, '')
			ELSE
				dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID) 
		END AS vcValue,                    
		CONVERT(BIT,1) bitCustomField,'' vcPropertyName, CONVERT(BIT,1) AS bitCanBeUpdated,numListID,
		CASE 
			WHEN vcAssociatedControlType = 'SelectBox' 
			THEN dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID) 
			ELSE 0 
		END numListItemID,'' PopupFunctionName,
		CASE 
			WHEN vcAssociatedControlType='SelectBox' 
			THEN ISNULL((SELECT numPrimaryListID FROM FieldRelationship WHERE numSecondaryListID=numListID AND numDomainID=@numDomainId),0) 
			ELSE 0 
		END AS ListRelID ,
		CASE 
			WHEN vcAssociatedControlType='SelectBox' 
			THEN (SELECT COUNT(*) FROM FieldRelationship where numPrimaryListID=numListID AND numDomainID=@numDomainId) 
			ELSE 0 
		END AS DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage
		,isnull((SELECT TOP 1 FldDTLID from CFW_FLD_Values where  RecId=@numRecordID and Fld_Id=numFieldID),0) as FldDTLID
		,dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID) as Value,'' AS vcDbColumnName,vcToolTip,0 intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
	FROM 
		View_DynamicCustomColumns_RelationShip
	WHERE 
		grp_id=@PageId 
		AND numDomainID=@numDomainID 
		AND subgrp=0
		AND numUserCntID=@numUserCntID 
		AND numFormId=@numFormID 
		AND numRelCntType=@numRelCntType 
		AND tintPageType=@tintPageType
	ORDER BY 
		tintRow,intcoulmn   
END                
                
if @PageId= 2 or  @PageId= 3 or @PageId= 5 or @PageId= 6 or @PageId= 7 or @PageId= 8   or @PageId= 11                          
 begin            
		IF @numFormID = 123 -- Add/Edit Order - Item Grid Column Settings
		BEGIN
			SELECT numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
					'' as vcValue,ISNULL(bitCustom,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
					numListID,0 numListItemID,PopupFunctionName,
		Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
		Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,vcDbColumnName,vcToolTip,intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
		FROM View_DynamicColumns
			where numFormId=@numFormID and numUserCntID=@numUserCntID and  numDomainID=@numDomainID  
			and ISNULL(bitCustom,0)=0 and numRelCntType=@numRelCntType AND tintPageType=@tintPageType 

			union          
           
			select numFieldId,vcfieldName ,vcURL,vcAssociatedControlType as fld_type,               
			tintRow,tintcolumn as intcoulmn,
			case when vcAssociatedControlType = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID))
			when vcAssociatedControlType = 'CheckBoxList' then STUFF((SELECT CONCAT(',', vcData) FROM ListDetails WHERE numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID),',')) FOR XML PATH('')), 1, 1, '')
			else dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) end as vcValue,                    
			convert(bit,1) bitCustomField,'' vcPropertyName,  
			convert(bit,1) as bitCanBeUpdated,numListID,case when vcAssociatedControlType = 'SelectBox' THEN dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
		Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
		Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,'' AS vcDbColumnName
		,vcToolTip,0 intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
		from View_DynamicCustomColumns
			where grp_id=@PageId and numDomainID=@numDomainID and subgrp=0
			and numUserCntID=@numUserCntID  
			AND numFormId=@numFormID and ISNULL(bitCustom,0)=1 and numRelCntType=@numRelCntType AND tintPageType=@tintPageType
    
			order by tintrow,intcoulmn      
		END
		ELSE
		BEGIN    
				SELECT numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
					'' as vcValue,ISNULL(bitCustom,0) AS bitCustomField,vcPropertyName,
					--DO NOT ALLOW CHANGE OF ASSIGNED TO FIELD AFTER COMMISSION IS PAID
					CAST((CASE 
					WHEN @charCoType='O' AND numFieldID=100
					THEN 
						CASE 
						WHEN (SELECT COUNT(*) FROM BizDocComission WHERE numOppID=@numRecordID AND ISNULL(bitCommisionPaid,0)=1) > 0 
						THEN 
							0 
						ELSE 
							ISNULL(bitAllowEdit,0) 
						END 
					ELSE 
						ISNULL(bitAllowEdit,0) 
					END) AS BIT) AS bitCanBeUpdated,
					numListID,0 numListItemID,PopupFunctionName,
		Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
		Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,vcDbColumnName,vcToolTip,intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
		FROM View_DynamicColumns
			where numFormId=@numFormID and numUserCntID=@numUserCntID and  numDomainID=@numDomainID  
			and ISNULL(bitCustom,0)=0 and numRelCntType=@numRelCntType AND tintPageType=@tintPageType 
			AND  1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
					WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
					ELSE 0 END) 

			union          
           
			select numFieldId,vcfieldName ,vcURL,vcAssociatedControlType as fld_type,               
			tintRow,tintcolumn as intcoulmn,
			case WHEN vcAssociatedControlType = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID))
			WHEN vcAssociatedControlType = 'CheckBoxList' then STUFF((SELECT CONCAT(',', vcData) FROM ListDetails WHERE numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID),',')) FOR XML PATH('')), 1, 1, '')
			ELSE dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) end as vcValue,                    
			convert(bit,1) bitCustomField,'' vcPropertyName,  
			convert(bit,1) as bitCanBeUpdated,numListID,case when vcAssociatedControlType = 'SelectBox' THEN dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
		Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
		Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,'' AS vcDbColumnName
		,vcToolTip,0 intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
		from View_DynamicCustomColumns
			where grp_id=@PageId and numDomainID=@numDomainID and subgrp=0
			and numUserCntID=@numUserCntID  
			AND numFormId=@numFormID and ISNULL(bitCustom,0)=1 and numRelCntType=@numRelCntType AND tintPageType=@tintPageType
    
			order by tintrow,intcoulmn      
	END                          
 end  

	IF @PageId = 153
	BEGIN
		SELECT 
			numFieldId
			,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
			,'' as vcURL
			,vcAssociatedControlType as fld_type
			,tintRow
			,tintcolumn as intcoulmn
			,'' as vcValue
			,ISNULL(bitCustom,0) AS bitCustomField
			,vcPropertyName
			,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated
			,numListID
			,0 numListItemID
			,PopupFunctionName
			,Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID
			,Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,vcDbColumnName,vcToolTip,intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
		FROM 
			View_DynamicColumns
		WHERE 
			numFormId=@numFormID 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID  
			AND ISNULL(bitCustom,0)=0 
			AND numRelCntType=@numRelCntType 
			AND tintPageType=@tintPageType 
		ORDER BY 
			tintrow,intcoulmn   
	END 
               
if @PageId= 0            
  begin         
  SELECT HDR.numFieldId,HDR.vcFieldName,'' as vcURL,'' as fld_type,DTL.tintRow,DTL.intcoulmn,          
  HDR.vcDBColumnName,convert(char(1),DTL.bitCustomField)as bitCustomField,
  0 as bitIsRequired,0 bitIsEmail,0 bitIsAlphaNumeric,0 bitIsNumeric, 0 bitIsLengthValidation,0 bitFieldMessage,0 intMaxLength,0 intMinLength, 0 bitFieldMessage,'' vcFieldMessage
   from PageLayoutdtl DTL                                    
  join pagelayout HDR on DTl.numFieldId= HDR.numFieldId                                    
   where HDR.Ctype = @charCoType and numUserCntId=@numUserCntId  and bitCustomField=0 and            
  numDomainId = @numDomainId and DTL.numRelCntType=@numRelCntType   order by DTL.tintrow,intcoulmn          
  end                
end                 



ELSE IF @tintPageType=2 and @numRelCntType>3 and 
((select isnull(sum(TotalRow),0) from (Select count(*) TotalRow from View_DynamicColumns where numFormId=@numFormID and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=@tintPageType AND numFormID IN(36) AND numRelCntType=2 
Union 
Select count(*) TotalRow from View_DynamicCustomColumns where numFormId=@numFormID and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=@tintPageType AND numFormID IN(36) AND numRelCntType=2) TotalRows
) <> 0 )
BEGIN

	exec usp_GetTableInfoDefault @numUserCntID=@numUserCntID,@numRecordID=@numRecordID,@numDomainId=@numDomainId,@charCoType=@charCoType,@pageId=@pageId,
			@numRelCntType=2,@numFormID=@numFormID,@tintPageType=@tintPageType
END
                           
ELSE IF @charCoType<>'b'/*added by kamal to prevent showing of all  fields on checkout by default*/                  
BEGIN                 
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)                                    

	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID   
       
	IF @pageid=1 or @pageid=4 or @pageid= 12 or  @pageid= 13 or  @pageid= 14                
	BEGIN   
		--Check if Master Form Configuration is created by administrator if NoColumns=0

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormID AND numRelCntType=@numRelCntType AND bitGridConfiguration = 0 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END

		--If MasterConfiguration is available then load it otherwise load default columns
		IF @IsMasterConfAvailable = 1
		BEGIN
			SELECT 
				numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
				 '' as vcValue,convert(bit,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
				 0 Tabletype,numListID,0 numListItemID,PopupFunctionName,
				Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
				Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
				bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
				bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
				,vcDbColumnName,vcToolTip,intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
			FROM 
				View_DynamicColumnsMasterConfig
			WHERE
				View_DynamicColumnsMasterConfig.numFormId=@numFormID AND 
				View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
				View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
				View_DynamicColumnsMasterConfig.numRelCntType=@numRelCntType AND 
				View_DynamicColumnsMasterConfig.bitGridConfiguration = 0 AND
				ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
			UNION
			SELECT 
				numFieldId as numFieldId,vcFieldName as vcfieldName ,vcURL,vcAssociatedControlType,               
				tintrow,tintColumn,  
				case when vcAssociatedControlType = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID))
				WHEN vcAssociatedControlType = 'CheckBoxList' then STUFF((SELECT CONCAT(',', vcData) FROM ListDetails WHERE numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID),',')) FOR XML PATH('')), 1, 1, '')
				 else dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) end as vcValue,                    
				 convert(bit,1) bitCustomField,'' vcPropertyName,  
				 convert(bit,1) as bitCanBeUpdated,1 Tabletype,numListID,case when vcAssociatedControlType = 'SelectBox' THEN dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
				Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
				Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
				ISNULL(bitIsRequired,0) bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
				,'' AS vcDbColumnName,vcToolTip,0 intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
			FROM 
				View_DynamicCustomColumnsMasterConfig
			WHERE 
				View_DynamicCustomColumnsMasterConfig.numFormId=@numFormID AND 
				View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
				View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
				View_DynamicCustomColumnsMasterConfig.numRelCntType=@numRelCntType AND 
				View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 0 AND
				ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
			ORDER BY 
				tintrow,intcoulmn  
		END
		ELSE                                        
		BEGIN
			SELECT numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
        ,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
         '' as vcValue,convert(bit,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
         0 Tabletype,numListID,0 numListItemID,PopupFunctionName,
Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
,vcDbColumnName,vcToolTip,intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName,0 AS numOrder
FROM View_DynamicDefaultColumns 
  where numFormId=@numFormID AND numDomainID=@numDomainID AND
   1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
		  WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
		  ELSE 0 END) 
                   
   union     
  
     select fld_id as numFieldId,fld_label as vcfieldName ,vcURL,fld_type,               
   convert(tinyint,0) as tintrow,convert(int,0) as intcoulmn,  
 case when fld_type = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(fld_id,@PageId,@numRecordID))                
 WHEN fld_type = 'CheckBoxList' then STUFF((SELECT CONCAT(',', vcData) FROM ListDetails WHERE numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(dbo.GetCustFldValue(fld_id,@PageId,@numRecordID),',')) FOR XML PATH('')), 1, 1, '')
 else dbo.GetCustFldValue(fld_id,@PageId,@numRecordID) end as vcValue,                    
 convert(bit,1) bitCustomField,'' vcPropertyName,  
 convert(bit,1) as bitCanBeUpdated,1 Tabletype,CFM.numListID,case when fld_type = 'SelectBox' THEN dbo.GetCustFldValue(fld_id,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
Case when fld_type='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=CFM.numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
Case when fld_type='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=CFM.numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
ISNULL(V.bitIsRequired,0) bitIsRequired,V.bitIsEmail,V.bitIsAlphaNumeric,V.bitIsNumeric,V.bitIsLengthValidation,V.bitFieldMessage,V.intMaxLength,V.intMinLength,V.bitFieldMessage,ISNULL(V.vcFieldMessage,'') vcFieldMessage
,'' AS vcDbColumnName,CFM.vcToolTip,0 intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName,0 AS numOrder
from CFW_Fld_Master CFM join CFW_Fld_Dtl CFD on Fld_id=numFieldId                                          
 left join CFw_Grp_Master CFG on subgrp=CFG.Grp_id                                          
 LEFT JOIN CFW_Validation V ON V.numFieldID = CFM.Fld_id
 where CFM.grp_id=@PageId and CFD.numRelation=@numRelCntType and CFM.numDomainID=@numDomainID and subgrp=0

 order by tintrow,intcoulmn                        
		END     
	END      
             
	IF @PageId= 2 or  @PageId= 3 or @PageId= 5 or @PageId= 6 or @PageId= 7 or @PageId= 8  or @PageId= 11                          
	BEGIN  
		--Check if Master Form Configuration is created by administrator if NoColumns=0

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormID AND numRelCntType=@numRelCntType AND bitGridConfiguration = 0 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END

		--If MasterConfiguration is available then load it otherwise load default columns
		IF @IsMasterConfAvailable = 1
		BEGIN
			SELECT 
				numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
				 '' as vcValue,convert(bit,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
				 0 Tabletype,numListID,0 numListItemID,PopupFunctionName,
				Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
				Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
				bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
				bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
				,vcDbColumnName,vcToolTip,intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
			FROM 
				View_DynamicColumnsMasterConfig
			WHERE
				View_DynamicColumnsMasterConfig.numFormId=@numFormID AND 
				View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
				View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
				View_DynamicColumnsMasterConfig.numRelCntType=@numRelCntType AND 
				View_DynamicColumnsMasterConfig.bitGridConfiguration = 0 AND
				ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
			UNION
			SELECT 
				numFieldId as numFieldId,vcFieldName as vcfieldName ,vcURL,vcAssociatedControlType,               
				tintrow,tintColumn,  
				case when vcAssociatedControlType = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID))
				WHEN vcAssociatedControlType = 'CheckBoxList' then STUFF((SELECT CONCAT(',', vcData) FROM ListDetails WHERE numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID),',')) FOR XML PATH('')), 1, 1, '')
				 else dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) end as vcValue,                    
				 convert(bit,1) bitCustomField,'' vcPropertyName,  
				 convert(bit,1) as bitCanBeUpdated,1 Tabletype,numListID,case when vcAssociatedControlType = 'SelectBox' THEN dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
				Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
				Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
				ISNULL(bitIsRequired,0) bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
				,'' AS vcDbColumnName,vcToolTip,0 intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
			FROM 
				View_DynamicCustomColumnsMasterConfig
			WHERE 
				View_DynamicCustomColumnsMasterConfig.numFormId=@numFormID AND 
				View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
				View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
				View_DynamicCustomColumnsMasterConfig.numRelCntType=@numRelCntType AND 
				View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 0 AND
				ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
			ORDER BY 
				tintrow,intcoulmn  
		END
		ELSE                                        
		BEGIN
			SELECT numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
			,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintColumn as intcoulmn,          
			 '' as vcValue,convert(bit,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
			 0 Tabletype,numListID,0 numListItemID,PopupFunctionName,
			Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
			Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage
			,vcDbColumnName,vcToolTip,intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName,0 AS numOrder
			FROM View_DynamicDefaultColumns DFV
			where numFormId=@numFormID AND numDomainID=@numDomainID AND
			1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
			  WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
			  ELSE 0 END)   
			UNION 
			select fld_id as numFieldId,fld_label as vcfieldName ,vcURL,fld_type,               
			convert(tinyint,0) as tintRow,convert(int,0) as intcoulmn,
			case when fld_type = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(fld_id,@PageId,@numRecordID))
			WHEN fld_type = 'CheckBoxList' then STUFF((SELECT CONCAT(',', vcData) FROM ListDetails WHERE numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(dbo.GetCustFldValue(fld_id,@PageId,@numRecordID),',')) FOR XML PATH('')), 1, 1, '')
			else dbo.GetCustFldValue(fld_id,@PageId,@numRecordID) end as vcValue,                    
			convert(bit,1) bitCustomField,'' vcPropertyName,  
			convert(bit,1) as bitCanBeUpdated,1 Tabletype,CFM.numListID,case when fld_type = 'SelectBox' THEN dbo.GetCustFldValue(fld_id,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
			Case when fld_type='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=CFM.numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
			Case when fld_type='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=CFM.numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
			ISNULL(V.bitIsRequired,0) bitIsRequired,V.bitIsEmail,V.bitIsAlphaNumeric,V.bitIsNumeric,V.bitIsLengthValidation,V.bitFieldMessage,V.intMaxLength,V.intMinLength,V.bitFieldMessage,ISNULL(V.vcFieldMessage,'') vcFieldMessage,'' AS vcDbColumnName
			,CFM.vcToolTip,0 intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName,0 AS numOrder
			FROM CFW_Fld_Master CFM left join CFW_Fld_Dtl CFD on Fld_id=numFieldId  
			left join CFw_Grp_Master CFG on subgrp=CFG.Grp_id                                    
			LEFT JOIN CFW_Validation V ON V.numFieldID = CFM.Fld_id
			WHERE CFM.grp_id=@PageId and CFM.numDomainID=@numDomainID   and subgrp=0           
			ORDER BY tintrow,intcoulmn                             
		END
	END   
	      
	IF @PageId= 153                      
	BEGIN  
		--Check if Master Form Configuration is created by administrator if NoColumns=0
		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormID AND numRelCntType=@numRelCntType AND bitGridConfiguration = 0 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END

		--If MasterConfiguration is available then load it otherwise load default columns
		IF @IsMasterConfAvailable = 1
		BEGIN
			SELECT 
				numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
				 '' as vcValue,convert(bit,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
				 0 Tabletype,numListID,0 numListItemID,PopupFunctionName,
				Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
				Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
				bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
				bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
				,vcDbColumnName,vcToolTip,intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName
			FROM 
				View_DynamicColumnsMasterConfig
			WHERE
				View_DynamicColumnsMasterConfig.numFormId=@numFormID AND 
				View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
				View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
				View_DynamicColumnsMasterConfig.numRelCntType=@numRelCntType AND 
				View_DynamicColumnsMasterConfig.bitGridConfiguration = 0 AND
				ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0 
		END
		ELSE                                        
		BEGIN
			SELECT numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
			,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintColumn as intcoulmn,          
			 '' as vcValue,convert(bit,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
			 0 Tabletype,numListID,0 numListItemID,PopupFunctionName,
			Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
			Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage
			,vcDbColumnName,vcToolTip,intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName
			FROM View_DynamicDefaultColumns DFV
			where numFormId=@numFormID AND numDomainID=@numDomainID AND bitDefault=1 AND
			1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
			  WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
			  ELSE 0 END)             
			ORDER BY tintrow,intcoulmn                             
		END
	END  

	IF @PageId= 0            
	BEGIN         
	   SELECT numFieldID,vcFieldName,'' as vcURL,'' as fld_type,vcDBColumnName,tintRow,intcolumn as intcoulmn,          
		'0' as bitCustomField,Ctype,'0' as tabletype,
		0 as bitIsRequired,0 bitIsEmail,0 bitIsAlphaNumeric,0 bitIsNumeric, 0 bitIsLengthValidation,0 bitFieldMessage,0 intMaxLength,0 intMinLength, 0 bitFieldMessage,'' vcFieldMessage
		from PageLayout where Ctype = @charCoType    order by tintrow,intcoulmn           
	END                       
END 
END
GO 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetPriceLevelEcommHtml')
DROP PROCEDURE USP_Item_GetPriceLevelEcommHtml
GO
CREATE PROCEDURE [dbo].[USP_Item_GetPriceLevelEcommHtml]
	@numDomainID NUMERIC(18,0)
	,@numSiteID NUMERIC(18,0)
	,@numItemCode AS NUMERIC(18,0)
AS  
BEGIN  
	DECLARE @vcHtml VARCHAR(MAX) = ''

	IF (SELECT numDefaultSalesPricing FROM Domain WHERE numDomainId=@numDomainID) = 1
	BEGIN
		DECLARE @numCurrencyID NUMERIC(18,0)

		DECLARE @TEMPPriceLevel TABLE
		(
			ID INT IDENTITY(1,1)
			,numQtyFrom FLOAT
			,numQtyTo FLOAT
			,vcPrice VARCHAR(30)
			,bitLastRow BIT
		)

		-- FIRST CHECK SITE CURRENY
		SELECT @numCurrencyID=numCurrencyID FROM Sites WHERE numDomainID=@numDomainID AND numSiteID=@numSiteID

		IF NOT EXISTS (SELECT numPricingID FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0)=0 AND ISNULL(numCurrencyID,0)=ISNULL(@numCurrencyID,0))
		BEGIN
			SELECT @numCurrencyID=numCurrencyID FROM Domain WHERE numDomainId=@numDomainID
		END

		IF NOT EXISTS (SELECT numPricingID FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0)=0 AND ISNULL(numCurrencyID,0)=ISNULL(@numCurrencyID,0))
		BEGIN
			SET @numCurrencyID = 0
		END

		IF EXISTS (SELECT numPricingID FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0)=0 AND ISNULL(numCurrencyID,0)=ISNULL(@numCurrencyID,0))
		BEGIN
			DECLARE @numBaseUnit NUMERIC(18,0)
			DECLARE @numSaleUnit NUMERIC(18,0)
			DECLARE @monListPrice DECIMAL(20,5)
			DECLARE @monVendorCost DECIMAL(20,5)
			DECLARE @fltExchangeRate FLOAT 
			DECLARE @fltUOMConversionFactor FLOAT

			SET @fltExchangeRate = ISNULL((SELECT fltExchangeRate FROM Currency WHERE numDomainID=@numDomainID AND numCurrencyID=ISNULL(@numCurrencyID,0)),1)

			IF ISNULL(@fltExchangeRate,0) = 0
			BEGIN
				SET @fltExchangeRate = 1
			END

			SELECT
				@numBaseUnit = ISNULL(numBaseUnit,0),
				@numSaleUnit = ISNULL(numSaleUnit,0),
				@monListPrice = (CASE 
									WHEN EXISTS (SELECT ID FROM ItemCurrencyPrice WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode AND numCurrencyID=ISNULL(@numCurrencyID,0))  
									THEN ISNULL((SELECT monListPrice FROM ItemCurrencyPrice WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode AND numCurrencyID=ISNULL(@numCurrencyID,0)),0)
									ELSE (CASE WHEN @fltExchangeRate <> 1 THEN CAST((ISNULL(Item.monListPrice,0) / @fltExchangeRate) AS INT) ELSE ISNULL(Item.monListPrice,0) END)
								END),
				@monVendorCost = ISNULL((SELECT (CASE WHEN @fltExchangeRate <> 1 THEN CAST((ISNULL(monCost,0) / @fltExchangeRate) AS INT) ELSE ISNULL(monCost,0) END) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,Item.numDomainID,Item.numPurchaseUnit) FROM Vendor WHERE numVendorID=Item.numVendorID AND Vendor.numItemCode=Item.numItemCode),0)
			FROM
				Item
			WHERE
				numDomainID = @numDomainID
				AND numItemCode= @numItemCode

			SET @fltUOMConversionFactor = dbo.fn_UOMConversion(@numSaleUnit,@numItemCode,@numDomainId,@numBaseUnit)
			IF ISNULL(@fltUOMConversionFactor,0) = 0
			BEGIN
				SET @fltUOMConversionFactor = 1
			END

			SET @monListPrice = @monListPrice * @fltUOMConversionFactor
			SET @monVendorCost = @monVendorCost * @fltUOMConversionFactor


			DECLARE @varCurrSymbol VARCHAR(100)
			SET @varCurrSymbol = ISNULL((SELECT varCurrSymbol FROM Currency WHERE numDomainID=@numDomainID AND numCurrencyID=@numCurrencyID),'$')

			INSERT INTO @TEMPPriceLevel
			(
				numQtyFrom
				,numQtyTo
				,vcPrice
			)
			SELECT
				intFromQty
				,intToQty
				,(CASE tintRuleType
					WHEN 1 -- Deduct From List Price
					THEN
						(CASE tintDiscountType 
							WHEN 1 -- PERCENT
							THEN FORMAT(ISNULL((CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END),0),'#,##0.#####')
							WHEN 2 -- FLAT AMOUNT
							THEN CONCAT(FORMAT((CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END),'#,##0.#####'),' off on total amount')
							WHEN 3 -- NAMED PRICE
							THEN FORMAT((CASE WHEN @numCurrencyID <> numCurrencyID THEN (CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) * @fltUOMConversionFactor) ELSE (ISNULL(decDiscount,0) * @fltUOMConversionFactor) END),'#,##0.#####')
						END)
					WHEN 2 -- Add to primary vendor cost
					THEN
						(CASE tintDiscountType 
							WHEN 1  -- PERCENT
							THEN FORMAT(@monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100)),'#,##0.#####')
							WHEN 2 -- FLAT AMOUNT
							THEN CONCAT(FORMAT((CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END),'#,##0.#####'),' extra on total amount')
							WHEN 3 -- NAMED PRICE
							THEN FORMAT((CASE WHEN @numCurrencyID <> numCurrencyID THEN (CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) * @fltUOMConversionFactor) ELSE (ISNULL(decDiscount,0) * @fltUOMConversionFactor) END),'#,##0.#####')
						END)
					WHEN 3 -- Named price
					THEN
						FORMAT((CASE WHEN @numCurrencyID <> numCurrencyID THEN  (CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) * @fltUOMConversionFactor) ELSE (ISNULL(decDiscount,0) * @fltUOMConversionFactor) END),'#,##0.#####')
					END
				) 
			FROM
				PricingTable
			WHERE
				numItemCode=@numItemCode
				AND ISNULL(numPriceRuleID,0) = 0
				AND ISNULL(numCurrencyID,0)=ISNULL(@numCurrencyID,0)
			ORDER BY 
				numPricingID
		END

		IF EXISTS (SELECT * FROM @TEMPPriceLevel)
		BEGIN
			DECLARE @vcUnitName VARCHAR(100)
			SET @vcUnitName = ISNULL((SELECT vcUnitName FROM UOM WHERE numDomainId=@numDomainID AND numUOMId=@numSaleUnit),'each')

			SET @vcHtml = CONCAT('<table class="table table-bordered table-pricelevel"><tr><th>Qty (',@vcUnitName,')</th><th>Price per ',@vcUnitName,'</th></tr>')
			SET @vchtml = CONCAT(@vchtml,STUFF((SELECT
													CONCAT('<tr><td>',FORMAT(numQtyFrom,'#,##0.#####'),' - ',FORMAT(numQtyTo,'#,##0.#####'),'</td><td>',@varCurrSymbol,vcPrice,'</td></tr>')
												FROM 
													@TEMPPriceLevel 
												ORDER BY
													ID
												FOR XML PATH(''), TYPE).value('.', 'varchar(max)'),1,0,''))

			SET @vcHtml = CONCAT(@vcHtml,'</table>')
		END
	END

	SELECT @vcHtml AS vcHtml
END
GO
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount DECIMAL(20,5) =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(18,0),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0,
  @numShipmentMethod NUMERIC(18,0)=0,
  @dtReleaseDate DATE = NULL,
  @numPartner NUMERIC(18,0)=0,
  @tintClickBtn INT=0,
  @numPartenerContactId NUMERIC(18,0)=0,
  @numAccountClass NUMERIC(18,0) = 0,
  @numWillCallWarehouseID NUMERIC(18,0) = 0,
  @numVendorAddressID NUMERIC(18,0) = 0,
  @numShipFromWarehouse NUMERIC(18,0) = 0,
  @numShippingService NUMERIC(18,0) = 0,
  @ClientTimeZoneOffset INT = 0,
  @vcCustomerPO VARCHAR(100)=NULL,
  @bitDropShipAddress BIT = 0,
  @PromCouponCode AS VARCHAR(100) = NULL,
  @numPromotionId AS NUMERIC(18,0) = 0,
  @dtExpectedDate AS DATETIME = null,
  @numProjectID NUMERIC(18,0) = 0
  -- USE PARAMETER WITH OPTIONAL VALUE BECAUSE PROCEDURE IS CALLED FROM OTHER PROCEDURE WHICH WILL NOT PASS NEW PARAMETER VALUE
)                                                                          
                                                                          
as  
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @fltExchangeRate float                                 
	DECLARE @hDocItem int                                                                                                                            
	DECLARE @TotalAmount as DECIMAL(20,5)                                                                          
	DECLARE @tintOppStatus as tinyint
	DECLARE @bitNewOrder AS BIT = (CASE WHEN ISNULL(@numOppID,0) = 0 THEN 1 ELSE 0 END)
	
	IF ISNULL(@numContactId,0) = 0
	BEGIN
		RAISERROR('CONTACT_REQUIRED',16,1)
		RETURN
	END  
	
	IF ISNULL(@tintOppType,0) = 0
	BEGIN
		RAISERROR('OPP_TYPE_CAN_NOT_BE_0',16,1)
		RETURN
	END             

	IF ISNULL(@numOppID,0) > 0 AND EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numDomainId=@numDomainId AND numOppId=@numOppID AND ISNULL(tintshipped,0) = 1) 
	BEGIN	 
		RAISERROR('OPP/ORDER_IS_CLOSED',16,1)
		RETURN
	END

	DECLARE @dtItemRelease AS DATE
	SET @dtItemRelease = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())

	DECLARE @numCompany AS NUMERIC(18)
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)
	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainId

	SELECT 
		@numCompany = D.numCompanyID
		,@bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0)
		,@numRelationship=numCompanyType
		,@numProfile=vcProfile
	FROM 
		DivisionMaster D
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID
	WHERE 
		D.numDivisionID = @numDivisionId

	IF @numOppID = 0 AND CONVERT(VARCHAR(10),@strItems) <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

		--VALIDATE PROMOTION OF COUPON CODE
		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1),
			numOppItemID NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numOppItemID,
			numPromotionID
		)
		SELECT
			numoppitemtCode,
			numPromotionID
		FROM
			OPENXML (@hDocItem,'/NewDataSet/Item',2)
		WITH
		(
			numoppitemtCode NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)
		WHERE
			numPromotionID > 0

		EXEC sp_xml_removedocument @hDocItem 

		IF (SELECT COUNT(*) FROM @TEMP) > 0
		BEGIN
			-- IF ITEM PROMOTION USED IN ORDER EXIPIRES THAN RAISE ERROR
			IF  (SELECT 
						COUNT(PO.numProId)
					FROM 
						PromotionOffer PO
					LEFT JOIN
						PromotionOffer POOrder
					ON
						PO.numOrderPromotionID = POOrder.numProId
					INNER JOIN
						@TEMP T1
					ON
						PO.numProId = T1.numPromotionID
					WHERE 
						PO.numDomainId=@numDomainID 
						AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
						AND ISNULL(PO.bitEnabled,0) = 1
						AND 1 = (CASE 
									WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
									THEN (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
									ELSE (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
								END)
						AND 1 = (CASE 
									WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
									ELSE
										(CASE PO.tintCustomersBasedOn 
											WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
											WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
											WHEN 3 THEN 1
											ELSE 0
										END)
								END)
				) <> (SELECT COUNT(*) FROM @TEMP)
			BEGIN
				RAISERROR('ITEM_PROMOTION_EXPIRED',16,1)
				RETURN
			END

			-- NOW IF COUPON BASED ITEM PROMOTION IS USED THEN VALIDATE COUPON AND ITS USAGE
			IF 
			(
				SELECT 
					COUNT(*)
				FROM
					PromotionOffer PO
				INNER JOIN
					@TEMP T1
				ON
					PO.numProId = T1.numPromotionID
				WHERE
					PO.numDomainId = @numDomainId
					AND ISNULL(IsOrderBasedPromotion,0) = 0
					AND ISNULL(numOrderPromotionID,0) > 0 
			) = 1
			BEGIN
				IF ISNULL(@PromCouponCode,0) <> ''
				BEGIN
					IF (SELECT 
							COUNT(*)
						FROM
							PromotionOffer PO
						INNER JOIN 
							PromotionOffer POOrder
						ON
							PO.numOrderPromotionID = POOrder.numProId
						INNER JOIN
							@TEMP T1
						ON
							PO.numProId = T1.numPromotionID
						INNER JOIN
							DiscountCodes DC
						ON
							POOrder.numProId = DC.numPromotionID
						WHERE
							PO.numDomainId = @numDomainId
							AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
							AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode) = 0
					BEGIN
						RAISERROR('INVALID_COUPON_CODE_ITEM_PROMOTION',16,1)
						RETURN
					END
					ELSE
					BEGIN
						-- IF THERE IS COUPON CODE USAGE LIMIT THAN VALIDATE USAGE LIMIT
						IF (SELECT 
								COUNT(*)
							FROM
								PromotionOffer PO
							INNER JOIN 
								PromotionOffer POOrder
							ON
								PO.numOrderPromotionID = POOrder.numProId
							INNER JOIN
								@TEMP T1
							ON
								PO.numProId = T1.numPromotionID
							INNER JOIN
								DiscountCodes DC
							ON
								POOrder.numProId = DC.numPromotionID
							WHERE
								PO.numDomainId = @numDomainId
								AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
								AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode
								AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN 1 ELSE 0 END)
								AND ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE DiscountCodeUsage.numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit) > 0
						BEGIN
							RAISERROR('ITEM_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED',16,1)
							RETURN
						END
					END
				END
				ELSE
				BEGIN
					RAISERROR('COUPON_CODE_REQUIRED_ITEM_PROMOTION',16,1)
					RETURN
				END
			END
		END
	END

	--IF NEW OPPERTUNITY
	IF @numOppID = 0                                                                          
	BEGIN
		DECLARE @bitIsInitalSalesOrder BIT
		IF(@DealStatus=1 AND @tintOppType=1)
		BEGIN
			SET @bitIsInitalSalesOrder=1
		END

		DECLARE @intOppTcode AS NUMERIC(9)  
  
		SELECT @intOppTcode = MAX(numOppId) FROM OpportunityMaster                

		SET @intOppTcode = ISNULL(@intOppTcode,0) + 1                                                
		
		SET @vcPOppName = @vcPOppName + '-'+ CONVERT(varchar(4),YEAR(GETUTCDATE()))  +'-A'+ CONVERT(varchar(10),@intOppTcode)                                  
  
		IF ISNULL(@numCurrencyID,0) = 0 
		BEGIN
			SET @fltExchangeRate=1
			SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
		END
		ELSE
		BEGIN 
			SET @fltExchangeRate = dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
        END  
		                                                        
		IF ISNULL(@numAccountClass,0) = 0
		BEGIN                                                  
		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivisionID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
		END

        IF(@DealStatus=1 AND @tintOppType=1 AND @tintClickBtn=1)
		BEGIN
			SET @DealStatus=0
		END         
		--PRINT ('Deal Staus ' +@DealStatus)  

		-------- VALIDATE ORDER BASED PROMOTION -------------

		DECLARE @numDiscountID NUMERIC(18,0) = NULL
		IF @numPromotionId > 0 
		BEGIN
			-- VALIDTE IF ITS ORDER BASED PROMOTION (BOTH ITEM AND ORDER BASED PROMOTION CAN HAVE COUPON CODE)
			IF EXISTS (SELECT numProId FROM PromotionOffer WHERE numDomainId=@numDomainId AND numProId=@numPromotionId AND ISNULL(IsOrderBasedPromotion,0) = 1)
			BEGIN
				-- CHECK IF ORDER PROMOTION IS STILL VALID
				IF NOT EXISTS (SELECT 
									PO.numProId
								FROM 
									PromotionOffer PO
								INNER JOIN 
									PromotionOfferOrganizations PORG
								ON 
									PO.numProId = PORG.numProId
								WHERE 
									numDomainId=@numDomainID 
									AND PO.numProId=@numPromotionId
									AND ISNULL(IsOrderBasedPromotion,0) = 1
									AND ISNULL(bitEnabled,0) = 1
									AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=dtValidFrom AND GETUTCDATE()<=dtValidTo THEN 1 ELSE 0 END) END)
									AND numRelationship=@numRelationship 
									AND numProfile=@numProfile
				)
				BEGIN
					RAISERROR('ORDER_PROMOTION_EXPIRED',16,1)
					RETURN
				END
				ELSE IF ISNULL((SELECT ISNULL(bitRequireCouponCode,0) FROM PromotionOffer WHERE numDomainId=@numDomainId AND numProId=@numPromotionId),0) = 1
				BEGIN
					IF ISNULL(@PromCouponCode,0) <> ''
					BEGIN
						IF (SELECT 
								COUNT(*)
							FROM
								PromotionOffer PO
							INNER JOIN
								DiscountCodes DC
							ON
								PO.numProId = DC.numPromotionID
							WHERE
								PO.numDomainId = @numDomainId
								AND PO.numProId = @numPromotionId
								AND ISNULL(IsOrderBasedPromotion,0) = 1
								AND ISNULL(bitRequireCouponCode,0) = 1
								AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode) = 0
						BEGIN
							RAISERROR('INVALID_COUPON_CODE_ORDER_PROMOTION',16,1)
							RETURN
						END
						ELSE
						BEGIN
							-- IF THERE IS COUPON CODE USAGE LIMIT THAN VALIDATE USAGE LIMIT
							IF (SELECT 
									COUNT(*)
								FROM
									PromotionOffer PO
								INNER JOIN
									DiscountCodes DC
								ON
									PO.numProId = DC.numPromotionID
								WHERE
									PO.numDomainId = @numDomainId
									AND PO.numProId = @numPromotionId
									AND ISNULL(IsOrderBasedPromotion,0) = 1
									AND ISNULL(bitRequireCouponCode,0) = 1
									AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode
									AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN 1 ELSE 0 END)
									AND ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE DiscountCodeUsage.numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit) > 0
							BEGIN
								RAISERROR('ORDER_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED',16,1)
								RETURN
							END
							ELSE
							BEGIN			
								SELECT
									@numDiscountID=numDiscountId
								FROM
									DiscountCodes DC
								WHERE
									DC.numPromotionID=@numPromotionId
									AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode
									
								IF EXISTS (SELECT DC.numDiscountID FROM DiscountCodes DC INNER JOIN DiscountCodeUsage DCU ON DC.numDiscountID=DCU.numDIscountID WHERE DC.numPromotionID=@numPromotionId AND DCU.numDivisionID=@numDivisionID AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode)
								BEGIN
									UPDATE
										DCU
									SET
										DCU.intCodeUsed = ISNULL(DCU.intCodeUsed,0) + 1
									FROM 
										DiscountCodes DC 
									INNER JOIN 
										DiscountCodeUsage DCU 
									ON 
										DC.numDiscountID=DCU.numDIscountID 
									WHERE 
										DC.numPromotionID=@numPromotionId 
										AND DCU.numDivisionID=@numDivisionID 
										AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode
								END
								ELSE
								BEGIN
									INSERT INTO DiscountCodeUsage
									(
										numDiscountId
										,numDivisionId
										,intCodeUsed
									)
									SELECT 
										DC.numDiscountId
										,@numDivisionId
										,1
									FROM
										DiscountCodes DC
									WHERE
										DC.numPromotionID=@numPromotionId
										AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode
								END
							END
						END
					END
					ELSE
					BEGIN
						RAISERROR('COUPON_CODE_REQUIRED_ORDER_PROMOTION',16,1)
						RETURN
					END
				END
			END
		END

		-------- ORDER BASED Promotion Logic-------------
		                                                    
		INSERT INTO OpportunityMaster
		(                                                                             
			numContactId,numDivisionId,txtComments,numCampainID,bitPublicFlag,tintSource,tintSourceType,vcPOppName,                                                                          
			intPEstimatedCloseDate,monPAmount,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,                                                                                                             
			numRecOwner,lngPConclAnalysis,tintOppType,numSalesOrPurType,numCurrencyID,fltExchangeRate,numAssignedTo,
			numAssignedBy,[tintOppStatus],numStatus,vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,
			bitInterestType,fltInterest,vcCouponCode,bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,
			vcMarketplaceOrderReportId,numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
			bitUseMarkupShippingRate,numMarkupShippingRate,intUsedShippingCompany,numShipmentMethod,numShippingService,numPartner,numPartenerContact,dtReleaseDate,bitIsInitalSalesOrder,numVendorAddressID,numShipFromWarehouse
			,vcCustomerPO#,bitDropShipAddress,numDiscountID,dtExpectedDate,numProjectID
		)                                                                          
		Values                                                                          
		(                                                                          
			@numContactId,@numDivisionId,ISNULL(@Comments,''),@CampaignID,@bitPublicFlag,@tintSource,@tintSourceType,@vcPOppName,                                                                          
			@dtEstimatedCloseDate,@monPAmount,@numUserCntID,(CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END),                                                                          
			@numUserCntID,GETUTCDATE(),@numDomainId,@numUserCntID,@lngPConclAnalysis,@tintOppType,@numSalesOrPurType,@numCurrencyID, 
			@fltExchangeRate,(CASE WHEN @numAssignedTo > 0 THEN @numAssignedTo ELSE NULL END),(CASE WHEN @numAssignedTo>0 THEN @numUserCntID ELSE NULL END),
			@DealStatus,@numStatus,@vcOppRefOrderNo,@bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,
			@PromCouponCode,@bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,@numPercentageComplete,
			@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,@bitUseMarkupShippingRate,@numMarkupShippingRate,@intUsedShippingCompany,@numShipmentMethod,@numShippingService,@numPartner,@numPartenerContactId,@dtReleaseDate,@bitIsInitalSalesOrder,@numVendorAddressID,@numShipFromWarehouse
			,@vcCustomerPO,@bitDropShipAddress,@numDiscountID,@dtExpectedDate,@numProjectID
		)                                                                                                                      
		
		SET @numOppID=(SELECT SCOPE_IDENTITY())                
                            
        EXEC dbo.usp_OppDefaultAssociateContacts @numOppId=@numOppID,@numDomainId=@numDomainId                    
		--UPDATE OPPNAME AS PER NAME TEMPLATE
		EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

		SELECT @vcPOppName=ISNULL(vcPOppName,'') FROM OpportunityMaster WHERE numOppId=@numOppID

		--MAP CUSTOM FIELD	
		DECLARE @tintPageID AS TINYINT;
		SET @tintPageID = CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
		EXEC dbo.USP_AddParentChildCustomFieldMap
				@numDomainID = @numDomainID, --  numeric(18, 0)
				@numRecordID = @numOppID, --  numeric(18, 0)
				@numParentRecId = @numDivisionId, --  numeric(18, 0)
				@tintPageID = @tintPageID --  tinyint
 	
		IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
		BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END
  
		-- INSERTING ITEMS                                                                                                                    
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,
				monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
				numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,vcAttrValues,monAvgCost,bitItemPriceApprovalRequired,
				numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,dtPlannedStart,numWOAssignedTo,numShipToAddressID,ItemReleaseDate
				,bitMarkupDiscount,vcChildKitSelectedItems,numWOQty,bitMappingRequired
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
				(CASE X.numWarehouseItmsID 
				WHEN -1 
				THEN 
					CASE 
						WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU= X.vcSKU) > 0)
							THEN
								(SELECT TOP 1 
									numWareHouseItemID 
								FROM 
									WareHouseItems 
								WHERE 
									[numItemID] = X.numItemCode 
									AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
						ELSE
							(SELECT 
								[numWareHouseItemID] 
							FROM 
								[WareHouseItems] 
							WHERE 
								[numItemID] = X.numItemCode 
								AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				WHEN 0 
				THEN 
					CASE 
					WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU=X.vcSKU) > 0)
						THEN
							(SELECT TOP 1 
								numWareHouseItemID 
							FROM 
								WareHouseItems 
							WHERE 
								[numItemID] = X.numItemCode 
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
					ELSE
						(SELECT 
							[numWareHouseItemID] 
						FROM 
							[WareHouseItems] 
						WHERE 
							[numItemID] = X.numItemCode 
							AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
							AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				ELSE  
					X.numWarehouseItmsID 
				END) AS numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
				(SELECT isnull(VN.monCost,0) FROM Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode AND VN.numVendorID=IT.numVendorID WHERE  VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,
				0,
				X.numToWarehouseItemID,X.Attributes,X.AttributeIDs,(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),
				X.bitItemPriceApprovalRequired,X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(X.vcVendorNotes,'')
				,X.vcInstruction,DATEADD(MINUTE,@ClientTimeZoneOffset,X.bintCompliationDate),DATEADD(MINUTE,@ClientTimeZoneOffset,X.dtPlannedStart),X.numWOAssignedTo,X.numShipToAddressID
				,ISNULL(ItemReleaseDate,@dtItemRelease),X.bitMarkupDiscount,ISNULL(X.KitChildItems,''),X.numWOQty,ISNULL(X.bitMappingRequired,0)
			FROM
				(
					SELECT 
						*
					FROM 
						OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
					WITH                       
					(                                                                          
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9), numUnitHour FLOAT, monPrice DECIMAL(30,16), monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(2000),                                    
						numWarehouseItmsID NUMERIC(9), ItemType VARCHAR(30), DropShip BIT, bitDiscountType BIT,	fltDiscount DECIMAL(30,16), monTotAmtBefDiscount DECIMAL(20,5),
						vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),
						numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),AttributeIDs VARCHAR(500),vcSKU VARCHAR(100)
						,bitItemPriceApprovalRequired BIT,numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0)
						,numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,dtPlannedStart DATETIME,numWOAssignedTo NUMERIC(18,0)
						,numShipToAddressID  NUMERIC(18,0),ItemReleaseDate DATETIME,bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX),numWOQty FLOAT,bitMappingRequired BIT
					)
				)X    
			ORDER BY 
				X.numoppitemtCode
    
			-- UPDATE EXISTING CUSTOMER PART NO
			UPDATE
				CPN
			SET
				CPN.CustomerPartNo = ISNULL(TEMPCPN.CustomerPartNo,'')
			FROM
				CustomerPartNumber CPN
			INNER JOIN
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) AS TEMPCPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.numDomainID=@numDomainID
				AND CPN.numCompanyId = @numCompany
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- INSERT NEW CUSTOMER PART NO
			INSERT INTO CustomerPartNumber
			(
				numCompanyId, numDomainID, CustomerPartNo, numItemCode 
			)
			SELECT 
				@numCompany, @numDomainID, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
			FROM
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) TEMPCPN 
			LEFT JOIN
				CustomerPartNumber CPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.CustomerPartNoID IS NULL
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'

			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,numWarehouseItmsID=X.numWarehouseItmsID,bitDropShip=X.DropShip,
				bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
				bitWorkOrder=X.bitWorkOrder,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,
				numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,vcAttributes=X.Attributes,
				numClassID=0,vcModelID=X.vcModelID,
				bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired,vcAttrValues=X.AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost
				,vcNotes=X.vcVendorNotes,vcInstruction=X.vcInstruction,bintCompliationDate=DATEADD(MINUTE,@ClientTimeZoneOffset,X.bintCompliationDate),dtPlannedStart=DATEADD(MINUTE,@ClientTimeZoneOffset,X.dtPlannedStart),numWOAssignedTo=X.numWOAssignedTo,ItemRequiredDate=X.ItemRequiredDate
				,bitMarkupDiscount=X.bitMarkupDiscount,vcChildKitSelectedItems=ISNULL(X.KitChildItems,'')
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,
					monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,
					Attributes,bitItemPriceApprovalRequired,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) numCost
					,ISNULL(vcVendorNotes,'') vcVendorNotes,vcInstruction,bintCompliationDate,dtPlannedStart,numWOAssignedTo,ItemRequiredDate,bitMarkupDiscount,KitChildItems
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
				WITH  
					(                      
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(2000), vcModelID VARCHAR(300),
						numWarehouseItmsID NUMERIC(9),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),
						numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),
						numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000)
						,vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,dtPlannedStart DATETIME,numWOAssignedTo NUMERIC(18,0),ItemRequiredDate DATETIME,bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX)
					)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppID=@numOppID
				
			--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
			DECLARE @TempKitConfiguration TABLE
			(
				numOppItemID NUMERIC(18,0),
				numItemCode NUMERIC(18,0),
				numKitItemID NUMERIC(18,0),
				numKitChildItemID NUMERIC(18,0),
				numQty FLOAT,
				numSequence INT
			)

			INSERT INTO @TempKitConfiguration
			(
				numOppItemID,
				numItemCode,
				numKitItemID,
				numKitChildItemID,
				numQty,
				numSequence
			)
			SELECT
				numoppitemtCode,
				numItemCode,
				numKitItemID,
				numChildItemID,
				numQty,
				numSequence
			FROM
				OpportunityItems
			CROSS APPLY
			(
				SELECT 
					Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 1)) As numKitItemID
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 2)) As numChildItemID
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 3)) As numQty
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 4)) As numSequence
				FROM  
				(
					SELECT 
						OutParam 
					FROM 
						SplitString(vcChildKitSelectedItems,',')
				) X
			) Y
			WHERE
				numOppId = @numOppID
				AND LEN(ISNULL(vcChildKitSelectedItems,'')) > 0
			ORDER BY
				ISNULL(Y.numSequence,0)

			--INSERT KIT ITEMS 
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				T1.numKitChildItemID,
				ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=T1.numKitChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),0),
				T1.numQty * OI.numUnitHour,
				T1.numQty,
				I.numBaseUnit,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			INNER JOIN
				@TempKitConfiguration T1
			ON
				T1.numOppItemID=OI.numoppitemtCode
				AND T1.numItemCode = OI.numItemCode
			JOIN
				Item I
			ON
				T1.numKitChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND (SELECT COUNT(*) FROM @TempKitConfiguration t2 WHERE t2.numOppItemID=OI.numoppitemtCode AND t2.numItemCode=OI.numItemCode AND ISNULL(t2.numKitItemID,0) = 0) > 0
			ORDER BY
				ISNULL(T1.numSequence,0)

			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),0),
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND (SELECT COUNT(*) FROM @TempKitConfiguration t2 WHERE t2.numOppItemID=OI.numoppitemtCode AND t2.numItemCode=OI.numItemCode AND ISNULL(t2.numKitItemID,0) = 0) = 0
			ORDER BY
				ISNULL(ID.sintOrder,0)

			IF (SELECT 
					COUNT(*) 
				FROM 
					OpportunityKitItems OI 
				INNER JOIN
					Item I
				ON
					OI.numChildItemID = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND ISNULL(charItemType,0) = 'P'
					AND numWareHouseItemId = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END

			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				IF (SELECT 
						COUNT(*) 
					FROM 
					(
						SELECT 
							t1.numOppItemID
							,t1.numItemCode
							,numKitItemID
							,numKitChildItemID
						FROM 
							@TempKitConfiguration t1
						WHERE
							ISNULL(t1.numKitItemID,0) > 0
					) TempChildItems
					INNER JOIN
						OpportunityKitItems OKI
					ON
						OKI.numOppId=@numOppId
						AND OKI.numOppItemID=TempChildItems.numOppItemID
						AND OKI.numChildItemID=TempChildItems.numKitItemID
					INNER JOIN
						OpportunityItems OI
					ON
						OI.numItemCode = TempChildItems.numItemCode
						AND OI.numoppitemtcode = OKI.numOppItemID
					LEFT JOIN
						WarehouseItems WI
					ON
						OI.numWarehouseItmsID = WI.numWarehouseItemID
					LEFT JOIN
						Warehouses W
					ON
						WI.numWarehouseID = W.numWarehouseID
					INNER JOIN
						ItemDetails
					ON
						TempChildItems.numKitItemID = ItemDetails.numItemKitID
						AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					INNER JOIN
						Item 
					ON
						ItemDetails.numChildItemID = Item.numItemCode
					INNER JOIN
						Item IMain
					ON
						OKI.numChildItemID = IMain.numItemCode
					WHERE
						Item.charItemType = 'P'
						AND ISNULL(IMain.bitKitParent,0) = 1
						AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
				BEGIN
					RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
				END

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped,
					monAvgCost
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
					(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0,
					ISNULL(Item.monAverageCost,0)
				FROM
				(
					SELECT 
						t1.numOppItemID
						,t1.numItemCode
						,numKitItemID
						,numKitChildItemID
					FROM 
						@TempKitConfiguration t1
					WHERE
						ISNULL(t1.numKitItemID,0) > 0
				) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numOppItemID=TempChildItems.numOppItemID
					AND OKI.numChildItemID=TempChildItems.numKitItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numoppitemtcode = OKI.numOppItemID
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END

			-- UPDATE UOM OF OPPORTUNITY ITEMS HAVE NOT UOM SET WHILE EXPORTED FROM MARKETPLACE (MEANS TINTSOURCETYPE = 3)
			IF ISNULL(@tintSourceType,0) = 3
			BEGIN
				UPDATE 
					OI 
				SET 
					numUOMID = ISNULL(I.numSaleUnit,0), 
					numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
				FROM 
					OpportunityItems OI
				JOIN 
					Item I 
				ON 
					OI.numItemCode = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND I.numDomainID = @numDomainId
			END	
                                     
			INSERT INTO OppWarehouseSerializedItem
			(
				numOppID,
				numWarehouseItmsDTLID,
				numOppItemID,
				numWarehouseItmsID
			)                      
			SELECT 
				@numOppID,
				X.numWarehouseItmsDTLID,
				X.numoppitemtCode,
				X.numWItmsID 
			FROM
			(                                                                          
				SELECT
					* 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
				WITH                        
				(                                               
				numWarehouseItmsDTLID numeric(9),                      
				numoppitemtCode numeric(9),                      
				numWItmsID numeric(9)                                                     
				)
			)X                                    
                                   
			UPDATE 
				OppWarehouseSerializedItem                       
			SET 
				numOppItemID=X.numoppitemtCode                                
			FROM                       
			(
				SELECT 
					numWarehouseItmsDTLID AS DTLID,
					O.numoppitemtCode 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
				WITH
				(
					numWarehouseItmsDTLID NUMERIC(9),numoppitemtCode NUMERIC(9),numWItmsID NUMERIC(9)
				)                                
				JOIN 
					OpportunityItems O 
				ON 
					O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID
			)X                 
			WHERE 
				numOppID=@numOppID 
				AND numWarehouseItmsDTLID=X.DTLID      
    
			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@numUserCntID
			END
                     
			EXEC sp_xml_removedocument @hDocItem                                     
		END          
	END
	ELSE                                                                                                                          
	BEGIN

		DECLARE @tempAssignedTo AS NUMERIC(9) = NULL                                                    
		SELECT 
			@tintOppStatus = tintOppStatus,
			@tempAssignedTo=ISNULL(numAssignedTo,0) 
		FROM   
			OpportunityMaster 
		WHERE  
			numOppID = @numOppID

		IF @tempAssignedTo <> @numAssignedTo
		BEGIN
			IF (SELECT COUNT(*) FROM BizDocComission WHERE ISNULL(numOppID,0) = @numOppID AND ISNULL(bitCommisionPaid,0) = 1) > 0
			BEGIN
				RAISERROR('COMMISSION_PAID_AGAINST_BIZDOC',16,1)
			END
		END
 
		--REVERTING BACK THE WAREHOUSE ITEMS                  
		IF @tintOppStatus = 1 AND @tintCommitAllocation=1
		BEGIN               
			EXEC USP_RevertDetailsOpp @numOppID,0,0,@numUserCntID                  
		END                  

		IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
		BEGIN
			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
		END
		
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
		BEGIN
			EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END 
						
		UPDATE   
			OpportunityMaster
		SET      
			vcPOppName = @vcPOppName,txtComments = ISNULL(@Comments,''),bitPublicFlag = @bitPublicFlag,numCampainID = @CampaignID,tintSource = @tintSource,tintSourceType=@tintSourceType,
			intPEstimatedCloseDate = @dtEstimatedCloseDate,numModifiedBy = @numUserCntID,bintModifiedDate = GETUTCDATE(),lngPConclAnalysis = @lngPConclAnalysis,monPAmount = @monPAmount,
			tintActive = @tintActive,numSalesOrPurType = @numSalesOrPurType,numStatus = @numStatus,vcOppRefOrderNo=@vcOppRefOrderNo,
			bintOppToOrder=(CASE WHEN @tintOppStatus=0 AND @DealStatus=1 THEN GETUTCDATE() ELSE bintOppToOrder END),bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,
			bitBillingTerms=@bitBillingTerms,intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,tintTaxOperator=@tintTaxOperator,
			numDiscountAcntType=@numDiscountAcntType,numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
			intUsedShippingCompany = @intUsedShippingCompany, numContactID=@numContactId,numShipmentMethod=@numShipmentMethod,numShippingService=@numShippingService
			,numPartner=@numPartner,numPartenerContact=@numPartenerContactId,numProjectID=@numProjectID
		WHERE
			numOppId = @numOppID   
	   
		-- UPDATING IF ORGANIZATION IS ASSIGNED TO SOMEONE                                                      
		IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN
			UPDATE
				OpportunityMaster 
			SET	
				numAssignedTo = @numAssignedTo, 
				numAssignedBy = @numUserCntID 
			WHERE 
				numOppId = @numOppID                                                    
		END                                                     
		ELSE IF(@numAssignedTo = 0) 
		BEGIN
			UPDATE 
				OpportunityMaster 
			SET 
				numAssignedTo = 0, 
				numAssignedBy = 0 
			WHERE
				numOppId = @numOppID
		END  
			                                                                    
		-- UPDATING OPP ITEMS
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0                                                                         
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   
			IF EXISTS 
			(
				SELECT 
					OI.numoppitemtCode
				FROM 
					OpportunityItems OI
				WHERE 
					numOppID=@numOppID 
					AND OI.numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))
					AND ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) > 0
			)
			BEGIN
				RAISERROR('SERIAL/LOT#_ASSIGNED_TO_ITEM_DELETED',16,1)
			END

			DELETE FROM OpportunityKitChildItems WHERE numOppID=@numOppID   
			DELETE FROM OpportunityKitItems WHERE numOppID=@numOppID                  
			DELETE FROM OpportunityItems WHERE numOppID=@numOppID AND numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9))) 
	          
			-- DELETE CORRESPONSING TIME AND EXPENSE ENTRIES OF DELETED ITEMS
			DELETE FROM
				TimeAndExpense
			WHERE
				numDomainID=@numDomainId
				AND numOppId=@numOppID
				AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9))) 

			--INSERT NEW ADDED ITEMS                        
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,
				[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numToWarehouseItemID,numShipmentMethod,
				numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired,vcAttrValues,numPromotionID,bitPromotionTriggered
				,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,dtPlannedStart,numWOAssignedTo,bitMarkupDiscount,ItemReleaseDate,vcChildKitSelectedItems,numWOQty
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
				(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),
				(select isnull(VN.monCost,0) from Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode and VN.numVendorID=IT.numVendorID where VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numToWarehouseItemID,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
				0,
				(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired, X.AttributeIDs,
				X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(vcVendorNotes,'')
				,X.vcInstruction,DATEADD(MINUTE,@ClientTimeZoneOffset,X.bintCompliationDate),DATEADD(MINUTE,@ClientTimeZoneOffset,X.dtPlannedStart),X.numWOAssignedTo,X.bitMarkupDiscount,@dtItemRelease,ISNULL(KitChildItems,''),X.numWOQty
			FROM
			(
				SELECT 
					*
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH  
				(                      
					numoppitemtCode numeric(9),numItemCode numeric(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc varchar(2000),                                    
					numWarehouseItmsID numeric(9),numToWarehouseItemID numeric(9),Op_Flag tinyint,ItemType varchar(30),DropShip bit,bitDiscountType bit,
					fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),
					numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(500), 
					bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000)
					,numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,dtPlannedStart DATETIME
					,numWOAssignedTo NUMERIC(18,0),bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX),numWOQty FLOAT
				)
			)X 
			
			-- UPDATE EXISTING CUSTOMER PART NO
			UPDATE
				CPN
			SET
				CPN.CustomerPartNo = ISNULL(TEMPCPN.CustomerPartNo,'')
			FROM
				CustomerPartNumber CPN
			INNER JOIN
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) AS TEMPCPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.numDomainID=@numDomainID
				AND CPN.numCompanyId = @numCompany
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- INSERT NEW CUSTOMER PART NO
			INSERT INTO CustomerPartNumber
			(
				numCompanyId, numDomainID, CustomerPartNo, numItemCode 
			)
			SELECT 
				@numCompany, @numDomainID, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
			FROM
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) TEMPCPN 
			LEFT JOIN
				CustomerPartNumber CPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.CustomerPartNoID IS NULL
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- UPDATE ITEMS
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,vcModelID=X.vcModelID,numWarehouseItmsID=X.numWarehouseItmsID,numToWarehouseItemID = X.numToWarehouseItemID,             
				bitDropShip=X.DropShip,bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,
				numUOMId=numUOM,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,
				numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired,bitWorkOrder=X.bitWorkOrder,vcAttrValues=AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost,vcNotes=vcVendorNotes
				,bitMarkupDiscount=X.bitMarkupDiscount,ItemRequiredDate=X.ItemRequiredDate,vcChildKitSelectedItems=ISNULL(KitChildItems,'')
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,numToWarehouseItemID,DropShip,bitDiscountType,
					fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
					bitItemPriceApprovalRequired,bitWorkOrder,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) AS numCost,ISNULL(vcVendorNotes,'') vcVendorNotes,
					ItemRequiredDate,bitMarkupDiscount,KitChildItems
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
				WITH  
				(                      
					numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(2000),vcModelID VARCHAR(300), numWarehouseItmsID NUMERIC(9), 
					numToWarehouseItemID NUMERIC(18,0),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),
					numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),numProjectStageID NUMERIC(9),Attributes VARCHAR(500),
					bitItemPriceApprovalRequired BIT, bitWorkOrder BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),
					ItemRequiredDate DATETIME, bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX)
				)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppId=@numOppID

			--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
			DELETE FROM @TempKitConfiguration

			INSERT INTO @TempKitConfiguration
			(
				numOppItemID,
				numItemCode,
				numKitItemID,
				numKitChildItemID,
				numQty,
				numSequence
			)
			SELECT
				numoppitemtCode,
				numItemCode,
				numKitItemID,
				numChildItemID,
				numQty,
				numSequence
			FROM
				OpportunityItems
			CROSS APPLY
			(
				SELECT 
					Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 1)) As numKitItemID
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 2)) As numChildItemID
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 3)) As numQty
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 4)) As numSequence
				FROM  
				(
					SELECT 
						OutParam 
					FROM 
						SplitString(vcChildKitSelectedItems,',')
				) X
			) Y
			WHERE
				numOppId = @numOppID
				AND LEN(ISNULL(vcChildKitSelectedItems,'')) > 0
			ORDER BY
				ISNULL(Y.numSequence,0)

			--INSERT KIT ITEMS 
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				T1.numKitChildItemID,
				ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=T1.numKitChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),0),
				T1.numQty * OI.numUnitHour,
				T1.numQty,
				I.numBaseUnit,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			INNER JOIN
				@TempKitConfiguration T1
			ON
				T1.numOppItemID=OI.numoppitemtCode
				AND T1.numItemCode = OI.numItemCode
			JOIN
				Item I
			ON
				T1.numKitChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND (SELECT COUNT(*) FROM @TempKitConfiguration t2 WHERE t2.numOppItemID=OI.numoppitemtCode AND t2.numItemCode=OI.numItemCode AND ISNULL(t2.numKitItemID,0) = 0) > 0
			ORDER BY
				ISNULL(T1.numSequence,0)

			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),0),
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND (SELECT COUNT(*) FROM @TempKitConfiguration t2 WHERE t2.numOppItemID=OI.numoppitemtCode AND t2.numItemCode=OI.numItemCode AND ISNULL(t2.numKitItemID,0) = 0) = 0
			ORDER BY
				ISNULL(ID.sintOrder,0)

			IF (SELECT 
					COUNT(*) 
				FROM 
					OpportunityKitItems OI 
				INNER JOIN
					Item I
				ON
					OI.numChildItemID = I.numItemCode
				WHERE 
					numOppId=@numOppID
					AND ISNULL(charItemType,0) = 'P'
					AND numWareHouseItemId = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END

			IF (SELECT 
					COUNT(*) 
				FROM 
				(
					SELECT 
						t1.numOppItemID
						,t1.numItemCode
						,numKitItemID
						,numKitChildItemID
					FROM 
						@TempKitConfiguration t1
					WHERE
						ISNULL(t1.numKitItemID,0) > 0
				) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numOppItemID=TempChildItems.numOppItemID
					AND OKI.numChildItemID=TempChildItems.numKitItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numoppitemtcode = OKI.numOppItemID
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
				INNER JOIN
					Item IMain
				ON
					OKI.numChildItemID = IMain.numItemCode
				WHERE
					Item.charItemType = 'P'
					AND ISNULL(IMain.bitKitParent,0) = 1
					AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END

			INSERT INTO OpportunityKitChildItems
			(
				numOppID,
				numOppItemID,
				numOppChildItemID,
				numItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT 
				@numOppID,
				OKI.numOppItemID,
				OKI.numOppChildItemID,
				ItemDetails.numChildItemID,
				(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
				(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
				ItemDetails.numQtyItemsReq,
				ItemDetails.numUOMId,
				0,
				ISNULL(Item.monAverageCost,0)
			FROM
			(
				SELECT 
					t1.numOppItemID
					,t1.numItemCode
					,numKitItemID
					,numKitChildItemID
				FROM 
					@TempKitConfiguration t1
				WHERE
					ISNULL(t1.numKitItemID,0) > 0
			) TempChildItems
			INNER JOIN
				OpportunityKitItems OKI
			ON
				OKI.numOppId=@numOppId
				AND OKI.numOppItemID=TempChildItems.numOppItemID
				AND OKI.numChildItemID=TempChildItems.numKitItemID
			INNER JOIN
				OpportunityItems OI
			ON
				OI.numItemCode = TempChildItems.numItemCode
				AND OI.numoppitemtcode = OKI.numOppItemID
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			INNER JOIN
				ItemDetails
			ON
				TempChildItems.numKitItemID = ItemDetails.numItemKitID
				AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
			INNER JOIN
				Item 
			ON
				ItemDetails.numChildItemID = Item.numItemCode                                                  
			                                    
			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@numUserCntID
			END
			   		                                                                      
			EXEC sp_xml_removedocument @hDocItem                                               
		END     

		IF @tintOppType=1 AND @tintOppStatus=1 --Sales Order
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0) = 0 AND (ISNULL(numUnitHour,0) - ISNULL(numQtyShipped,0)) < 0)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_IS_LESS_THEN_SHIPPED_QTY',16,1)
				RETURN
			END

			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems INNER JOIN WorkOrder ON OpportunityItems.numOppID=WorkOrder.numOppID AND OpportunityItems.numoppitemtCode=WorkOrder.numOppItemID AND ISNULL(WorkOrder.numParentWOID,0)=0 AND WorkOrder.numWOStatus=23184 AND ISNULL(OpportunityItems.numUnitHour,0) > ISNULL(WorkOrder.numQtyItemsReq,0) WHERE OpportunityItems.numOppID=@numOppID)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_CAN_NOT_BE_GREATER_THEN_COMPLETED_WORK_ORDER',16,1)
				RETURN
			END
		END
		ELSE IF @tintOppType=2 AND @tintOppStatus=1 --Purchase Order
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0) = 0 AND (ISNULL(numUnitHour,0) - ISNULL(numUnitHourReceived,0)) < 0)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_IS_LESS_THEN_RECEIVED_QTY',16,1)
				RETURN
			END
		END

		--FOLLOWING DELETE STATEMENT IS COMMENTED BECUASE NOW WE ARE NOT ALLOWING USER TO EIDT SERIAL/LOT ITEM UNTILL USER REMOVED ASSIGNED SERIAL/LOT NUMBERS
	    --Delete previously added serial lot number added for opportunity
		--DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
	END

	IF @tintOppStatus = 1
	BEGIN
		IF (SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0)=0) <>
			(SELECT COUNT(*) FROM OpportunityItems INNER JOIN WareHouseItems ON OpportunityItems.numWarehouseItmsID=WareHouseItems.numWareHouseItemID AND OpportunityItems.numItemCode=WareHouseItems.numItemID WHERE numOppId=@numOppID AND OpportunityItems.numWarehouseItmsID > 0 AND ISNULL(bitDropShip,0)=0)
		BEGIN
			RAISERROR('INVALID_ITEM_WAREHOUSES',16,1)
			RETURN
		END
	END

	IF(SELECT 
			COUNT(*) 
		FROM 
			OpportunityBizDocItems OBDI 
		INNER JOIN 
			OpportunityBizDocs OBD 
		ON 
			OBDI.numOppBizDocID=OBD.numOppBizDocsID 
		WHERE 
			OBD.numOppId=@numOppID AND OBDI.numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)) > 0
	BEGIN
		RAISERROR('ITEM_USED_IN_BIZDOC',16,1)
		RETURN
	END

	IF(SELECT 
			COUNT(*)
		FROM
			OpportunityItems OI
		INNER JOIN
		(
			SELECT
				numOppItemID
				,SUM(numUnitHour) numInvoiceBillQty
			FROM 
				OpportunityBizDocItems OBDI 
			INNER JOIN 
				OpportunityBizDocs OBD 
			ON 
				OBDI.numOppBizDocID=OBD.numOppBizDocsID 
			WHERE 
				OBD.numOppId=@numOppID
				AND ISNULL(OBD.bitAuthoritativeBizDocs,0) = 1
			GROUP BY
				numOppItemID
		) AS TEMP
		ON
			TEMP.numOppItemID = OI.numoppitemtCode
		WHERE
			OI.numOppId = @numOppID
			AND OI.numUnitHour < Temp.numInvoiceBillQty
		) > 0
	BEGIN
		RAISERROR('ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_INVOICE/BILL_QTY',16,1)
		RETURN
	END

	IF @tintOppType=1 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
	BEGIN
		IF(SELECT 
				COUNT(*)
			FROM
			(
				SELECT
					numOppItemID
					,SUM(numUnitHour) PackingSlipUnits
				FROM 
					OpportunityBizDocItems OBDI 
				INNER JOIN 
					OpportunityBizDocs OBD 
				ON 
					OBDI.numOppBizDocID=OBD.numOppBizDocsID 
				WHERE 
					OBD.numOppId=@numOppID
					AND OBD.numBizDocId=29397 --Picking Slip
				GROUP BY
					numOppItemID
			) AS TEMP
			INNER JOIN
				OpportunityItems OI
			ON
				TEMP.numOppItemID = OI.numoppitemtCode
			WHERE
				OI.numUnitHour < PackingSlipUnits
			) > 0
		BEGIN
			RAISERROR('ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_PACKING_SLIP_QTY',16,1)
			RETURN
		END
	END

	SET @TotalAmount=0                                                           
	SELECT @TotalAmount = SUM(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID                                                                          
  
	UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numOppID

/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
            
			
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT

			SET @bitIsInitalSalesOrder=(SELECT bitIsInitalSalesOrder FROM OpportunityMaster WHERE numOppId=@numOppID AND numDomainId=@numDomainID)
			--SET @bitViolatePrice=(SELECT bitMarginPriceViolated FROM Domain WHERE numDomainId=@numDomainID)
			IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainId=@numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1)
			BEGIN
				SET @bitViolatePrice = 1
			END
			
			IF(@bitViolatePrice=1 AND @bitIsInitalSalesOrder=1 AND @tintClickBtn = 1)
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus=@DealStatus WHERE numOppId=@numOppID
			END
			ELSE
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus = @DealStatus WHERE numOppId=@numOppID
				DECLARE @tintShipped AS TINYINT               
				SELECT @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID               
				
				IF @tintOppStatus=1 AND @tintCommitAllocation=1             
				BEGIN         
					EXEC USP_UpdatingInventoryonCloseDeal @numOppID,0,@numUserCntID
				END  
				declare @tintCRMType as numeric(9)        
				declare @AccountClosingDate as datetime        

				select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from OpportunityMaster where numOppID=@numOppID         
				select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

				--When deal is won                                         
				IF @DealStatus = 1 
				BEGIN
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID              
					END

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=getutcdate()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
				END         
				--When Deal is Lost
				ELSE IF @DealStatus = 2
				BEGIN
					SELECT @AccountClosingDate=bintAccountClosingDate FROM OpportunityMaster WHERE numOppID=@numOppID         
					
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID
					END
				END 
			END

			/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
			-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
			if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
			begin        
				update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				where numDivisionID=@numDivisionID        
			end        

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50), @vcAltContact VARCHAR(200)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9), @numContact NUMERIC(18,0)
DECLARE @bitIsPrimary BIT, @bitAltContact BIT;
DECLARE @tintAddressOf TINYINT

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

IF EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numOppId=@numOppID AND ISNULL(tintBillToType,0) = 0 AND ISNULL(numBillToAddressID,0) = 0)
BEGIN
	SELECT 
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM 
		dbo.AddressDetails AD
	WHERE 
		AD.numDomainID=@numDomainID 
		AND AD.numRecordID=@numDivisionID 
		AND AD.tintAddressOf = 2 
		AND AD.tintAddressType = 1 
		AND AD.bitIsPrimary=1       

	UPDATE OpportunityMaster SET tintBillToType=2,numBillToAddressID=0 WHERE numOppId=@numOppID

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END

IF EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numOppId=@numOppID AND ISNULL(tintShipToType,0) = 0 AND ISNULL(numShipToAddressID,0) = 0)
BEGIN
	SELECT 
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM 
		dbo.AddressDetails AD
	WHERE 
		AD.numDomainID=@numDomainID 
		AND AD.numRecordID=@numDivisionID 
		AND AD.tintAddressOf = 2 
		AND AD.tintAddressType = 2 
		AND AD.bitIsPrimary=1       

	UPDATE OpportunityMaster SET tintShipToType=2,numShipToAddressID=0 WHERE numOppId=@numOppID

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END

--Bill Address
IF @numBillAddressId>0
BEGIN
	SELECT 
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM 
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numBillAddressId

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numBillAddressId
	END

	UPDATE OpportunityMaster SET tintBillToType=2,numBillToAddressID=0 WHERE numOppId=@numOppID

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END
  
--Ship Address
IF @intUsedShippingCompany = 92 -- WILL CALL (PICK UP FROM Warehouse)
BEGIN
	IF EXISTS (SELECT numWareHouseID FROM Warehouses WHERE Warehouses.numDomainID = @numDomainID AND numWareHouseID = @numWillCallWarehouseID AND ISNULL(numAddressID,0) > 0)
	BEGIN
		SELECT  
			@vcStreet=ISNULL(vcStreet,'')
			,@vcCity=ISNULL(vcCity,'')
			,@vcPostalCode=ISNULL(AddressDetails.vcPostalCode,'')
			,@numState=ISNULL(numState,0)
			,@numCountry=ISNULL(numCountry,0)
			,@vcAddressName=vcAddressName
		FROM 
			dbo.Warehouses 
		INNER JOIN
			AddressDetails
		ON
			Warehouses.numAddressID = AddressDetails.numAddressID
		WHERE 
			Warehouses.numDomainID = @numDomainID 
			AND numWareHouseID = @numWillCallWarehouseID
	END
	ELSE
	BEGIN
	SELECT  
		@vcStreet=ISNULL(vcWStreet,'')
		,@vcCity=ISNULL(vcWCity,'')
		,@vcPostalCode=ISNULL(vcWPinCode,'')
		,@numState=ISNULL(numWState,0)
		,@numCountry=ISNULL(numWCountry,0)
		,@vcAddressName=''
	FROM 
		dbo.Warehouses 
	WHERE 
		numDomainID = @numDomainID 
		AND numWareHouseID = @numWillCallWarehouseID
	END

	UPDATE OpportunityMaster SET tintShipToType=2 WHERE numOppId=@numOppID
 
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = '', --  varchar(100)
	@numCompanyId =0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = 0,
	@bitAltContact = 0,
	@vcAltContact = ''
END
ELSE IF @numShipAddressId>0
BEGIN
	SELECT
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM  
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numShipAddressId
 

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numShipAddressId
	END

	UPDATE OpportunityMaster SET tintShipToType=2,numShipToAddressID=0 WHERE numOppId=@numOppID

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END

-- IMPORTANT:KEEP THIS BELOW ADDRESS UPDATE LOGIC
-- GET TAX APPLICABLE TO DIVISION WHEN ORDER IS CREATED AFTER THAT NOT NEED TO CHANGE IT
IF @bitNewOrder = 1
BEGIN
	--INSERT TAX FOR DIVISION   
	IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
	BEGIN
		INSERT INTO dbo.OpportunityMasterTaxItems 
		(
			numOppId,
			numTaxItemID,
			fltPercentage,
			tintTaxType,
			numTaxID
		) 
		SELECT 
			@numOppID,
			TI.numTaxItemID,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			TaxItems TI 
		JOIN 
			DivisionTaxTypes DTT 
		ON 
			TI.numTaxItemID = DTT.numTaxItemID
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
		) AS TEMPTax
		WHERE 
			DTT.numDivisionID=@numDivisionId 
			AND DTT.bitApplicable=1
		UNION 
		SELECT 
			@numOppID,
			0,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			dbo.DivisionMaster 
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
		) AS TEMPTax
		WHERE 
			bitNoTax=0 
			AND numDivisionID=@numDivisionID	
		UNION 
		SELECT
			@numOppId
			,1
			,decTaxValue
			,tintTaxType
			,numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,@numOppId,1,NULL)		
	END	
END

--INSERT TAX FOR DIVISION   
IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN
--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID
) 
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID = IT.numTaxItemID 
WHERE 
	OI.numOppId=@numOppID 
	AND IT.bitApplicable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	0,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numOppID 
	AND I.bitTaxable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT
	@numOppId,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
FROM
	dbo.OpportunityItems OI 
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
INNER JOIN
	TaxDetails TD
ON
	TD.numTaxID = IT.numTaxID
	AND TD.numDomainId = @numDomainId
WHERE
	OI.numOppId = @numOppID
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

END

	UPDATE 
		OI
	SET
		OI.vcInclusionDetail = [dbo].[GetOrderAssemblyKitInclusionDetails](OI.numOppID,OI.numoppitemtCode,OI.numUnitHour,1,1)
	FROM 
		OpportunityItems OI
	INNER JOIN
		Item I
	ON
		OI.numItemCode = I.numItemCode
	WHERE
		numOppId=@numOppID
		AND ISNULL(I.bitKitParent,0) = 1
  
	UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID

	IF @tintOppType=1 AND @tintOppStatus=1 
	BEGIN
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numStatus
		EXEC USP_OpportunityMaster_CreateBackOrderPO @numDomainID,@numUserCntID,@numOppID
	END

	IF EXISTS (SELECT 
					numoppitemtCode 
				FROM 
					OpportunityItems 
				INNER JOIN 
					Item 
				ON 
					OpportunityItems.numItemCode=Item.numItemCode 
				WHERE 
					numOppId=@numOppID 
					AND charItemType='P' 
					AND ISNULL(numWarehouseItmsID,0) = 0 
					AND ISNULL(bitDropShip,0) = 0)
	BEGIN
		RAISERROR('WAREHOUSE_REQUIRED',16,1)
	END

	IF (SELECT COUNT(*) FROM WorkOrder WHERE numDomainID=@numDomainID AND numOppId=@numOppId AND ISNULL(numOppItemID,0) > 0 AND numOppItemID NOT IN (SELECT OIInner.numoppitemtCode FROM OpportunityItems OIInner WHERE OIInner.numOppId=@numOppID)) > 0
	BEGIN
		RAISERROR ( 'WORK_ORDER_EXISTS', 16, 1 ) ;
	END

	IF (SELECT 
			COUNT(*) 
		FROM 
			WorkOrder 
		INNER JOIN 
			OpportunityItems 
		ON 
			OpportunityItems.numOppId=@numOppID
			AND OpportunityItems.numoppitemtCode=WorkOrder.numOppItemID
		WHERE 
			WorkOrder.numDomainID=@numDomainID 
			AND WorkOrder.numOppId=@numOppID
			AND WorkOrder.numWareHouseItemId <> OpportunityItems.numWarehouseItmsID) > 0
	BEGIN
		RAISERROR ('CAN_NOT_CHANGE_ASSEMBLY_WAREHOUSE_WORK_ORDER_EXISTS', 16, 1 ) ;
	END

	IF @tintOppType = 1 AND @tintOppStatus=1
	BEGIN
		EXEC USP_OpportunityItems_CheckWarehouseMapping @numDomainID,@numOppID
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
/****** Object:  StoredProcedure [dbo].[USP_ProjectManage]    Script Date: 07/26/2008 16:20:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_projectmanage')
DROP PROCEDURE usp_projectmanage
GO
CREATE PROCEDURE [dbo].[USP_ProjectManage]                        
(                        
	@numProId as numeric OUTPUT,
	@numDivisionId numeric(9)=null,
	@numIntPrjMgr numeric(9)=null,
	@numOppId numeric(9)=null,
	@numCustPrjMgr numeric(9)=null,
	@vcPProName Varchar(100)='' OUTPUT,
	@Comments varchar(1000)='',
	@numUserCntID numeric(9)=0,
	@numAssignedTo numeric(9)=0,
	@numDomainId numeric(9)=0,
	@dtDueDate as datetime,
	@numContractId as numeric(9) =0,
	@vcProjectName VARCHAR(100),
	@numProjectType NUMERIC(9),
	@numProjectStatus NUMERIC(9),
	@dtmStartDate DATETIME,
	@dtmEndDate DATETIME
)                        
as                        
           
IF DATEPART(YEAR,@dtDueDate) = 1753 set  @dtDueDate=null
                     
                        
declare @TotalAmount as integer                        
if @numProId = 0                        
begin                        
declare @intProTcode as numeric(9)                        
Select @intProTcode=max(numProId)from ProjectsMaster                        
set @intProTcode =isnull(@intProTcode,0) + 1                        
set @vcPProName=@vcPProName+ '-'+convert(varchar(4),year(getutcdate()))  +'-A'+ convert(varchar(10),@intProTcode)                        
                        
insert into ProjectsMaster                        
  (                        
  vcProjectID,
  numIntPrjMgr,                        
  numOppId,                        
  numCustPrjMgr,                                 
  numDivisionId,                        
  txtComments,                        
  numCreatedBy,                        
  bintCreatedDate,                        
  numModifiedBy,                        
  bintModifiedDate,                        
  numDomainId,                        
  numRecOwner,                      
  intDueDate,              
  numAssignedby,              
  numAssignedTo ,        
  numContractId,
  vcProjectName,
  numProjectType,numProjectStatus,dtmStartDate,dtmEndDate                  
  )                        
 Values                        
  (                        
  @vcPProName,                        
  @numIntPrjMgr,                        
  @numOppId,                        
  @numCustPrjMgr,                                  
  @numDivisionId,                        
  @Comments,                        
  @numUserCntID,                        
  getutcdate(),                        
  @numUserCntID,                        
  getutcdate(),                        
  @numDomainId,                        
  @numUserCntID,                      
  @dtDueDate,              
  0,              
  0 ,        
 @numContractId,
 @vcProjectName,
 @numProjectType,@numProjectStatus,@dtmStartDate,@dtmEndDate          
  )                        
                        
 set @numProId=SCOPE_IDENTITY()                        
 
 --Map Custom Field	
  DECLARE @tintPageID AS TINYINT;SET @tintPageID=11
  	
  EXEC dbo.USP_AddParentChildCustomFieldMap
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@numRecordID = @numProId, --  numeric(18, 0)
	@numParentRecId = @numDivisionId, --  numeric(18, 0)
	@tintPageID = @tintPageID --  tinyint

EXEC dbo.usp_ProjectDefaultAssociateContacts @numProId=@numProId,@numDomainId=@numDomainId
  insert into [ProjectsStageDetails] 
  (                                                                    
  [numProId],                                                                    
  numstagepercentage,                                                                    
  vcStageDetail,                                                                    
  vcComments,                            
  numDomainId,                                                                    
  numCreatedBy,                                                                    
  bintCreatedDate,                                                                    
  numModifiedBy,                                                                    
  bintModifiedDate,                                                                    
  numAssignTo,                                                                    
  bintStageComDate,                                                                    
  bintDueDate,                                                                    
  bitAlert,                                                                    
  bitStageCompleted                                                                    
  )                                                                          
  values                      
  (                      
  @numProId,                                                                    
  100,                      
  'Project Completed',                      
  '',                                                                    
  @numDomainId,                                                                    
  @numUserCntID,                                                                    
  getutcdate(),                                   
  @numUserCntID,                                                                    
  getutcdate(),                                                                    
  0,                                                                    
  0,                                                                    
  0,                                                                    
  0,                                                                    
  0--@DealCompleted
  )                                                                     
  insert into ProjectsStageDetails                                  
  (                                                             
  numProId,                                                                    
  numstagepercentage,                                                                    
  vcStageDetail,                                                                    
  vcComments,                                                                
  numDomainId,                                                                    
  numCreatedBy,                                                                    
  bintCreatedDate,                                                                    
  numModifiedBy,                                                            
  bintModifiedDate,                                                                    
  numAssignTo,                                                                  
  bintStageComDate,                                                                    
  bintDueDate,                                                                    
  bitAlert,                                                                    
  bitStageCompleted                                                                    
  )                                 
  values                      
  (                  
  @numProId,                                                                    
  0,                                                                    
  'Project Lost',                                 
  '',                                                                    
 @numDomainId,                                                                    
  @numUserCntID,                                                                    
  getutcdate(),                                                                    
  @numUserCntID,                                                                    
  getutcdate(),                        
   0,                                                                    
   0,                                                                
  0,                                                                    
  0,                                                                    
  0                                                                    
  )        
                                           
end                        
else                                          
begin                        
		-- Checking For Incidents
		declare @update as bit 
		set @update = 0
		if @numContractId <> 0
		begin
			IF (SELECT bitIncidents FROM [ContractManagement] WHERE numdomainId = @numDomainID  and numcontractId = @numContractId) = 1
			BEGIN
				declare @Usedincidents as numeric(9)
			set @Usedincidents = 0
			declare @Availableincidents as numeric(9)
			

			select @Usedincidents=@Usedincidents+count(*) from cases 
				where numcontractId = @numContractId and numdomainId = @numDomainID 

			select @Usedincidents=@Usedincidents+count(*) from projectsmaster 
				where numcontractId = @numContractId and numdomainId = @numDomainID and numProId <> @numProId


			select @Availableincidents=isnull(numincidents,0) from contractManagement 
			where numdomainId = @numDomainID  and numcontractId = @numContractId
				print '@Usedincidents-----'
				print @Usedincidents
				print @Availableincidents
			if @Usedincidents >=@Availableincidents
				set @update = 1	
			end
		end

if @update = 0
	begin 
		  DECLARE @numOldProjectStatus AS NUMERIC	
		  SELECT @numOldProjectStatus=ISNULL(numProjectStatus,0) FROM ProjectsMaster where numProId=@numProId
		  	
		  update ProjectsMaster                        
		  set  
		   vcProjectID=@vcPProName,  numIntPrjMgr=@numIntPrjMgr,                        
		   numOppId=@numOppId,                        
		   numCustPrjMgr=@numCustPrjMgr,                        
		   txtComments=@Comments,                        
		   numModifiedBy=@numUserCntID,                        
		   bintModifiedDate=getutcdate(),                      
		   intDueDate =@dtDueDate ,         
		   numContractId=@numContractId,
		   vcProjectName=@vcProjectName,
		   numProjectType=@numProjectType,numProjectStatus=@numProjectStatus,dtmStartDate=@dtmStartDate,dtmEndDate=@dtmEndDate
		  where numProId=@numProId 

		IF @numOldProjectStatus != @numProjectStatus
		BEGIN
			IF @numProjectStatus=27492  ---Updating All Statge to 100% if Completed
			BEGIN
					UPDATE PM SET dtCompletionDate=GETUTCDATE(),monTotalExpense=PIE.monExpense,
					monTotalIncome=PIE.monIncome,monTotalGrossProfit=PIE.monBalance
					FROM ProjectsMaster PM,dbo.GetProjectIncomeExpense(@numDomainId,@numProId) PIE
					WHERE PM.numProId=@numProId
					
					EXEC USP_ManageProjectCommission @numDomainId,@numProId
					
					Update StagePercentageDetails set tinProgressPercentage=100,bitclose=1 where numProjectID=@numProId

					EXEC dbo.USP_GetProjectTotalProgress
							@numDomainID = @numDomainID, --  numeric(9, 0)
							@numProId = @numProId, --  numeric(9, 0)
							@tintMode = 1 --  tinyint
			END
			ELSE
			BEGIN
				DELETE from BizDocComission WHERE numDomainID = @numDomainID AND numProId=@numProId AND numComissionID 
					NOT IN (SELECT BC.numComissionID FROM dbo.BizDocComission BC JOIN dbo.PayrollTracking PT 
						ON BC.numComissionID = PT.numComissionID AND BC.numDomainId = PT.numDomainId WHERE BC.numDomainID = @numDomainID AND ISNULL(BC.numProId,0) = @numProId)
			END
		END
	end       
	else
			set @numProId = 0	                        
                       
   PRINT '@numProId : ' + CONVERT(VARCHAR(10),@numProId)            
   IF ISNULL(@numProId,0) > 0
	BEGIN
		DECLARE @numPrimaryAddressID AS NUMERIC(18,0)
		DECLARE @numAddressID AS NUMERIC(18,0)
		
		SELECT @numPrimaryAddressID = ISNULL(numAddressID,0) FROM dbo.AddressDetails WHERE numRecordID = @numDivisionId  AND ISNULL(bitIsPrimary,0) = 1
		PRINT '@numPrimaryAddressID : ' + CONVERT(VARCHAR(10),@numPrimaryAddressID)
		
		INSERT INTO dbo.AddressDetails 
		(vcAddressName,vcStreet,vcCity,vcPostalCode,numState,numCountry,bitIsPrimary,tintAddressOf,tintAddressType,numRecordID,numDomainID )
		SELECT vcAddressName,vcStreet,vcCity,vcPostalCode,numState,numCountry,bitIsPrimary,4,tintAddressType,@numProId ,numDomainID
		FROM dbo.AddressDetails
		WHERE numAddressID = @numPrimaryAddressID
		
		--SELECT @numAddressID = numAddressID FROM dbo.AddressDetails WHERE numRecordID = @numProId AND ISNULL(bitIsPrimary,0) = 1  AND ISNULL(tintAddressOf,0) = 4
		SET @numAddressID = SCOPE_IDENTITY()  
		PRINT '@numAddressID : ' + CONVERT(VARCHAR(10),@numAddressID)
		UPDATE ProjectsMaster SET numAddressID = @numAddressID 	WHERE numProId=@numProId         	
	END 
	                     
    ---Updating if Project is assigned to someone                      
  declare @tempAssignedTo as numeric(9)                    
  set @tempAssignedTo=null                     
  select @tempAssignedTo=isnull(numAssignedTo,0) from ProjectsMaster where numProId=@numProId                     
print @tempAssignedTo                    
  if (@tempAssignedTo<>@numAssignedTo and  @numAssignedTo<>'0')                    
  begin                      
    update ProjectsMaster set numAssignedTo=@numAssignedTo ,numAssignedBy=@numUserCntID where numProId=@numProId                      
  end                     
  else if  (@numAssignedTo =0)                    
  begin                    
   update ProjectsMaster set numAssignedTo=0 ,numAssignedBy=0 where numProId=@numProId                   
  end                                  
                        
set @vcPProName='erer'                        
                
end
GO
/****** Object:  StoredProcedure [dbo].[USP_Projects]    Script Date: 07/26/2008 16:20:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_projects')
DROP PROCEDURE usp_projects
GO
CREATE PROCEDURE [dbo].[USP_Projects]                          
(                          
@numProId numeric(9)=null ,  
@numDomainID as numeric(9),  
@ClientTimeZoneOffset as int         
                         
)                          
as                          
                          
begin                          
                                          
 select  pro.numProId,                          
  pro.vcProjectName,                         
  pro.numintPrjMgr,                          
  pro.numOppId,                    
  pro.intDueDate,                          
  pro.numCustPrjMgr,                          
  pro.numDivisionId,                          
  pro.txtComments,      
  Div.tintCRMType,                          
  div.vcDivisionName,                          
  com.vcCompanyName,
  A.vcFirstname + ' ' + A.vcLastName AS vcContactName,
  isnull(A.vcEmail,'') AS vcEmail,
         isnull(A.numPhone,'') AS Phone,
         isnull(A.numPhoneExtension,'') AS PhoneExtension,
		 		 (SELECT TOP 1 ISNULL(vcData,'') FROM ListDetails WHERE numListID=5 AND numListItemID=com.numCompanyType AND numDomainId=@numDomainID) AS vcCompanyType,
pro.numcontractId,                       
  dbo.fn_GetContactName(pro.numCreatedBy)+' '+convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,pro.bintCreatedDate)) as vcCreatedBy ,                          
  dbo.fn_GetContactName(pro.numModifiedBy)+' '+convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,pro.bintModifiedDate)) as vcModifiedby,                        
  dbo.fn_GetContactName(pro.numCompletedBy)+' '+convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,pro.dtCompletionDate)) as vcFinishedby,                        
  pro.numDomainId,                          
  dbo.fn_GetContactName(pro.numRecOwner) as vcRecOwner,  pro.numRecOwner,   div.numTerID,       
  pro.numAssignedby,pro.numAssignedTo,          
(select  count(*) from dbo.GenericDocuments where numRecID=@numProId and  vcDocumentSection='P') as DocumentCount,
	
 numAccountID,
 '' TimeAndMaterial,
isnull((select sum(isnull(monTimeBudget,0)) from StagePercentageDetails where numProjectID=@numProId),0) TimeBudget,
isnull((select sum(isnull(monExpenseBudget,0)) from StagePercentageDetails where numProjectID=@numProId),0) ExpenseBudget,
isnull((select min(isnull(dtStartDate,getdate()))-1 from StagePercentageDetails where numProjectID=@numProId),getdate()) dtStartDate,
isnull((select max(isnull(dtEndDate,getdate()))+1 from StagePercentageDetails where numProjectID=@numProId),getdate()) dtEndDate,
ISNULL(numProjectType,0) AS numProjectType,
isnull(dbo.fn_GetExpenseDtlsbyProStgID(pro.numProId,0,0),0) as UsedExpense,                        
isnull(dbo.fn_GeTimeDtlsbyProStgID_BT_NBT(pro.numProId,0,1),'Billable Time (0)  Non Billable Time (0)') as UsedTime,
isnull(Pro.numProjectStatus,0) as numProjectStatus,
(SELECT ISNULL(vcStreet,'') + ',<pre>' + ISNULL(vcCity,'') + ',<pre>' +  dbo.[fn_GetState]([numState]) + ',<pre>' + dbo.fn_GetListItemName(numCountry) + ',<pre>' + vcPostalCode 
 FROM dbo.AddressDetails WHERE numDomainID = pro.numDomainID AND numRecordID = pro.numAddressID AND tintAddressOf = 4 AND tintAddressType = 2 AND bitIsPrimary = 1 )as ShippingAddress,
Pro.numBusinessProcessID,
SLP.Slp_Name AS vcProcessName,
pro.dtCompletionDate AS vcProjectedFinish,
NULL AS dtActualStartDate,
DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,pro.dtmEndDate) dtmEndDate,
DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,pro.dtmStartDate) dtmStartDate,
ISNULL(pro.numClientBudget,0) AS numClientBudget,
0 AS numClientBudgetBalance,
(CASE 
	WHEN EXISTS (SELECT CInner.numContractId FROM Contracts CInner WHERE CInner.numDomainId=@numDomainID AND CInner.numDivisonId=pro.numDivisionID AND intType=1) 
	THEN 1
	ELSE 0
END) AS bitContractExists,
0 AS numClientBudgetBalance,
(CASE 
	WHEN EXISTS (SELECT CInner.numContractId FROM Contracts CInner WHERE CInner.numDomainId=@numDomainID AND CInner.numDivisonId=pro.numDivisionID AND intType=1) 
	THEN dbo.fn_SecondsConversion(ISNULL((SELECT TOP 1 ((ISNULL(numHours,0) * 60 * 60) + (ISNULL(numMinutes,0) * 60)) - ISNULL((SELECT SUM(CLInner.numUsedTime) FROM ContractsLog CLInner WHERE CLInner.numContractId=C.numContractId),0) FROM Contracts C WHERE C.numDomainId=@numDomainID AND C.numDivisonId=pro.numDivisionID AND intType=1 ORDER BY C.numContractID DESC),0))
	ELSE '' 
END) AS timeLeft,
(CASE 
	WHEN EXISTS (SELECT CInner.numContractId FROM Contracts CInner WHERE CInner.numDomainId=@numDomainID AND CInner.numDivisonId=pro.numDivisionID AND intType=1) 
	THEN ISNULL((SELECT TOP 1 ((ISNULL(numHours,0) * 60 * 60) + (ISNULL(numMinutes,0) * 60)) - ISNULL((SELECT SUM(CLInner.numUsedTime) FROM ContractsLog CLInner WHERE CLInner.numContractId=C.numContractId),0) FROM Contracts C WHERE C.numDomainId=@numDomainID AND C.numDivisonId=pro.numDivisionID AND intType=1 ORDER BY C.numContractID DESC),0)
	ELSE 0
END) AS timeLeftInMinutes
from ProjectsMaster pro                          
  left join DivisionMaster div                          
  on pro.numDivisionID=div.numDivisionID    
  LEFT JOIN AdditionalContactsInformation A ON pro.numCustPrjMgr = A.numContactId
  left join CompanyInfo com                          
  on div.numCompanyID=com.numCompanyId         
  LEFT JOIN dbo.AddressDetails AD ON AD.numAddressID = pro.numAddressID       
		LEFT JOIN Sales_process_List_Master AS SLP ON pro.numBusinessProcessID=SLP.Slp_Id          
  where numProId=@numProId    and pro.numDomainID=@numDomainID                      
                          
end
GO
/****** Object:  StoredProcedure [dbo].[USP_ProjectsDTLPL]    Script Date: 07/26/2008 16:20:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_projectsdtlpl')
DROP PROCEDURE usp_projectsdtlpl
GO
CREATE PROCEDURE [dbo].[USP_ProjectsDTLPL]                                   
(                                    
@numProId numeric(9)=null  ,                  
@numDomainID numeric(9),    
@ClientTimeZoneOffset as int                                     
)                                    
as                                    
                                    
begin                                    
                                                    
                                    
 select  pro.vcProjectName,
		 pro.numintPrjMgr,
		 dbo.fn_GetContactName(pro.numintPrjMgr) as numintPrjMgrName,                              
		 pro.intDueDate,                                    
		 dbo.fn_GetContactName(pro.numCustPrjMgr) as numCustPrjMgrName,
		 pro.numCustPrjMgr,                                    
         pro.txtComments,
		 [dbo].[fn_GetContactName](pro.numAssignedTo ) as numAssignedToName, 
		 pro.numAssignedTo,                  
        (select  count(*) from GenericDocuments   where numRecID=@numProId and  vcDocumentSection='P') as DocumentCount,
		CASE 
           WHEN tintProStatus = 1 THEN 100
           ELSE isnull((SELECT SUM(tintPercentage)
                 FROM   ProjectsStageDetails
                 WHERE  numProId = @numProId
                        AND bitStageCompleted = 1),0)
         END AS TProgress,
         (select  count(*) from [ProjectsOpportunities] PO Left JOIN [OpportunityMaster] OM ON OM.[numOppId] = PO.[numOppId] WHERE [numProId]=@numProId) as LinkedOrderCount,
         vcProjectID,
         '' TimeAndMaterial,
         numContractId,
         ISNULL((SELECT vcContractName FROM dbo.ContractManagement WHERE numContractId=pro.numContractId),'') numContractIdName,
         isnull(numProjectType,0) numProjectType,
         dbo.fn_GetListItemName(numProjectType) vcProjectTypeName,isnull(Pro.numProjectStatus,0) as numProjectStatus,
dbo.fn_GetListItemName(numProjectStatus) vcProjectStatusName,
(SELECT SUBSTRING(
	(SELECT ','+  A.vcFirstName+' ' +A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN '(' + dbo.fn_GetListItemName(SR.numContactType) + ')' ELSE '' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=pro.numDomainID AND SR.numModuleID=5 AND SR.numRecordID=pro.numProId
				AND UM.numDomainID=pro.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('')),2,200000)) AS numShareWith ,
	isnull(AD.vcStreet,'')+ 
	' <br/>'+ isnull(AD.vcCity,'')+ ' ,'
	         + isnull(dbo.fn_GetState(AD.numState),'')+ ' '
	         + isnull(AD.vcPostalCode,'')+ ' <br>' 
	         + isnull(dbo.fn_GetListItemName(AD.numCountry),'') as ShippingAddress,
pro.dtCompletionDate AS vcProjectedFinish,
NULL AS dtActualStartDate,
DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,pro.dtmEndDate) dtmEndDate,
DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,pro.dtmStartDate) dtmStartDate,
ISNULL(pro.numClientBudget,0) AS numClientBudget,
0 AS numClientBudgetBalance
from ProjectsMaster pro    
LEFT JOIN dbo.AddressDetails AD ON AD.numAddressID = pro.numAddressID                                 
where numProId=@numProId     and pro.numdomainID=  @numDomainID                             
                                    
end
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ProjectsMaster_GetAssociatedContacts')
DROP PROCEDURE USP_ProjectsMaster_GetAssociatedContacts
GO
CREATE PROCEDURE [dbo].[USP_ProjectsMaster_GetAssociatedContacts]
(
	@numDomainID NUMERIC(18,0)
	,@numProId NUMERIC(18,0)
)
AS
BEGIN
	DECLARE @TEMPContacts TABLE
	(
		numContactID NUMERIC(18,0)
		,vcContact VARCHAR(200)
		,vcCompanyname VARCHAR(300)
		,tinUserType TINYINT
		,ActivityID NUMERIC(18,0)
		,Email VARCHAR(150)
		,vcStatus VARCHAR(200)
		,vcPosition VARCHAR(200)
		,vcEmail VARCHAR(200)
		,numPhone VARCHAR(200)
		,numPhoneExtension VARCHAR(200)
		,bitDefault BIT
		,vcUserName VARCHAR(200)
		,numRights TINYINT
		,numProjectRole NUMERIC(18,0)
	)

	INSERT INTO 
		@TEMPContacts
	SELECT 
		AC.numContactId
		,CONCAT(ISNULL(vcFirstName,''),' ',ISNULL(vcLastName,'')) AS vcContact
		,CI.vcCompanyName AS vcCompanyname
		,CASE WHEN UM.numUserDetailId IS NULL THEN 2 ELSE 1 END AS tinUserType
		,0 AS ActivityID
		,AC.vcEmail as Email
		,'' vcStatus
		,dbo.GetListIemName(AC.vcPosition) vcPosition
		,AC.vcEmail
		,AC.numPhone
		,AC.numPhoneExtension
		,1
		,CONCAT(ISNULL(vcFirstName,''),' ',ISNULL(vcLastName,''))
		,1
		,0
	FROM
		ProjectsMaster PM
	INNER JOIN 
		AdditionalContactsInformation AC 
	ON 
		PM.numIntPrjMgr=AC.numContactID  
	LEFT JOIN
		DivisionMaster DM 
	ON 
		DM.numDivisionID=AC.numDivisionID 
	LEFT JOIN 
		CompanyInfo CI 
	ON 
		DM.numCompanyId=CI.numCompanyId 
	LEFT JOIN 
		UserMaster UM  
	ON 
		UM.numUserDetailID=AC.numContactID
	WHERE 
		PM.numDomainID=@numDomainId
		AND PM.numProId=@numProId

	INSERT INTO 
		@TEMPContacts
	SELECT 
		AC.numContactId
		,CONCAT(ISNULL(vcFirstName,''),' ',ISNULL(vcLastName,'')) AS vcContact
		,CI.vcCompanyName AS vcCompanyname
		,CASE WHEN UM.numUserDetailId IS NULL THEN 2 ELSE 1 END AS tinUserType
		,0 AS ActivityID
		,AC.vcEmail as Email
		,'' vcStatus
		,dbo.GetListIemName(AC.vcPosition) vcPosition
		,AC.vcEmail
		,AC.numPhone
		,AC.numPhoneExtension
		,1
		,CONCAT(ISNULL(vcFirstName,''),' ',ISNULL(vcLastName,''))
		,1
		,0
	FROM
		ProjectsMaster PM
	INNER JOIN 
		AdditionalContactsInformation AC 
	ON 
		PM.numCustPrjMgr=AC.numContactID  
	LEFT JOIN
		DivisionMaster DM 
	ON 
		DM.numDivisionID=AC.numDivisionID 
	LEFT JOIN 
		CompanyInfo CI 
	ON 
		DM.numCompanyId=CI.numCompanyId 
	LEFT JOIN 
		UserMaster UM  
	ON 
		UM.numUserDetailID=AC.numContactID
	WHERE 
		PM.numDomainID=@numDomainId
		AND PM.numProId=@numProId
		AND AC.numContactId NOT IN (SELECT TC.numContactID FROM @TEMPContacts TC)

	INSERT INTO 
		@TEMPContacts
	SELECT DISTINCT
		AC.numContactId
		,CONCAT(ISNULL(vcFirstName,''),' ',ISNULL(vcLastName,'')) AS vcContact
		,CI.vcCompanyName AS vcCompanyname
		,CASE WHEN UM.numUserDetailId IS NULL THEN 2 ELSE 1 END AS tinUserType
		,0 AS ActivityID
		,AC.vcEmail as Email
		,'' vcStatus
		,dbo.GetListIemName(AC.vcPosition) vcPosition
		,AC.vcEmail
		,AC.numPhone
		,AC.numPhoneExtension
		,1
		,CONCAT(ISNULL(vcFirstName,''),' ',ISNULL(vcLastName,''))
		,1
		,0
	FROM
		ProjectsMaster PM
	INNER JOIN
		StagePercentageDetailsTask SPDT
	ON
		PM.numProId = SPDT.numProjectId
	INNER JOIN 
		AdditionalContactsInformation AC 
	ON 
		SPDT.numAssignTo=AC.numContactID  
	LEFT JOIN
		DivisionMaster DM 
	ON 
		DM.numDivisionID=AC.numDivisionID 
	LEFT JOIN 
		CompanyInfo CI 
	ON 
		DM.numCompanyId=CI.numCompanyId 
	LEFT JOIN 
		UserMaster UM  
	ON 
		UM.numUserDetailID=AC.numContactID
	WHERE 
		PM.numDomainID=@numDomainId
		AND PM.numProId=@numProId
		AND AC.numContactId NOT IN (SELECT TC.numContactID FROM @TEMPContacts TC)

	INSERT INTO 
		@TEMPContacts
	SELECT DISTINCT
		PC.numContactID
		,CONCAT(ISNULL(vcFirstName,''),' ',ISNULL(vcLastName,'')) AS vcContact
		,CI.vcCompanyName AS vcCompanyname
		,CASE WHEN UM.numUserDetailId IS NULL THEN 2 ELSE 1 END AS tinUserType
		,0 AS ActivityID
		,AC.vcEmail as Email
		,'' vcStatus
		,dbo.GetListIemName(AC.vcPosition) vcPosition
		,AC.vcEmail
		,AC.numPhone
		,AC.numPhoneExtension
		,0
		,CONCAT(ISNULL(vcFirstName,''),' ',ISNULL(vcLastName,''))
		,1
		,0
	FROM 
		ProjectsMaster PM
	INNER JOIN
		ProjectsContacts PC 
	ON
		PM.numProId = PC.numProId
	INNER JOIN 
		AdditionalContactsInformation AC 
	ON 
		PC.numContactID=AC.numContactID  
	LEFT JOIN
		DivisionMaster DM 
	ON 
		DM.numDivisionID=AC.numDivisionID 
	LEFT JOIN 
		CompanyInfo CI 
	ON 
		DM.numCompanyId=CI.numCompanyId 
	LEFT JOIN 
		UserMaster UM  
	ON 
		UM.numUserDetailID=AC.numContactID
	WHERE 
		PM.numDomainID=@numDomainId
		AND PM.numProId=@numProId
		AND AC.numContactId NOT IN (SELECT TC.numContactID FROM @TEMPContacts TC)

	SELECT * FROM @TEMPContacts ORDER BY vcContact
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportDashboard_GetForDataCache')
DROP PROCEDURE dbo.USP_ReportDashboard_GetForDataCache
GO
CREATE PROCEDURE [dbo].[USP_ReportDashboard_GetForDataCache]

AS 
BEGIN
	SELECT 
		ReportDashboard.numDomainID
		,ReportDashboard.numUserCntID
		,ReportDashboard.numDashBoardID
		,ReportDashboard.numReportID
		,ISNULL(ReportListMaster.bitDefault,0) bitDefault
		,ISNULL(ReportListMaster.intDefaultReportID,0) intDefaultReportID
		,ISNULL(ReportListMaster.tintReportType,0) tintReportType
	FROM
		ReportDashboard
	INNER JOIN
		Domain
	ON
		ReportDashboard.numDomainID = Domain.numDomainId
	INNER JOIN
		Subscribers
	ON
		Domain.numDomainId = Subscribers.numTargetDomainID
	INNER JOIN
		ReportListMaster
	ON
		ReportDashboard.numReportID = ReportListMaster.numReportID
	WHERE
		CAST(Subscribers.dtSubEndDate AS DATE) >= CAST(GETUTCDATE() AS dATE)
		AND (DATEPART(HOUR,GETDATE()) >= 0 AND DATEPART(HOUR,GETDATE()) <= 7) -- BETWEEN 0 AM TO 7 AM
		AND (ReportDashboard.dtLastCacheDate IS NULL OR ReportDashboard.dtLastCacheDate <> CAST(GETUTCDATE() AS DATE))
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ScheduledReportsGroup_GetForDataCache')
DROP PROCEDURE dbo.USP_ScheduledReportsGroup_GetForDataCache
GO
CREATE PROCEDURE [dbo].[USP_ScheduledReportsGroup_GetForDataCache]
(
	@numDomainID NUMERIC(18,0)
	,@numSRGID NUMERIC(18,0)
	,@tintMode TINYINT
)
AS 
BEGIN
	IF @tintMode = 1 AND EXISTS (SELECT ID FROM ScheduledReportsGroup WHERE numDomainID=@numDomainID AND ID=@numSRGID)
	BEGIN
		SELECT * FROM ScheduledReportsGroup WHERE numDomainID=@numDomainID AND ID=@numSRGID
	END
	ELSE IF @tintMode = 2
	BEGIN
		SELECT 
			*
		FROM 
			ScheduledReportsGroup 
		WHERE 
			ISNULL(tintDataCacheStatus,0) <> 1 --0: NOT STARTED, 1: STARTED, 2: FINISHED
			AND (DATEPART(HOUR,GETDATE()) >= 0 AND DATEPART(HOUR,GETDATE()) <= 7) -- BETWEEN 0 AM TO 7 AM
			AND 1 = (CASE 
						WHEN dtNextDate IS NULL AND CAST(dtStartDate AS DATE) <= CAST(DATEADD(DAY,1,GETUTCDATE()) AS DATE) THEN 1
						WHEN dtNextDate IS NOT NULL AND CAST(dtNextDate AS DATE) <= CAST(DATEADD(DAY,1,GETUTCDATE()) AS DATE) THEN 1
						ELSE 0
					END)
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_StagePercentageDetailsTask_Close')
DROP PROCEDURE dbo.USP_StagePercentageDetailsTask_Close
GO
CREATE PROCEDURE [dbo].[USP_StagePercentageDetailsTask_Close]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numTaskID NUMERIC(18,0)
)
AS 
BEGIN
	IF EXISTS (SELECT numTaskId FROM StagePercentageDetailsTask WHERE numDomainID=@numDomainID AND numTaskId = @numTaskID AND ISNULL(bitTaskClosed,0) = 0)
	BEGIN
		UPDATE
			StagePercentageDetailsTask
		SET
			bitTaskClosed=1
		WHERE
			numDomainID=@numDomainID 
			AND numTaskId = @numTaskID

		DECLARE @numOppId INT,@numProjectId INT,@numWorkOrderId INT

		SELECT 
			@numOppId=ISNULL(numOppId,0)
			,@numProjectId=ISNULL(numProjectId,0)
			,@numWorkOrderId=ISNULL(numWorkOrderId,0)
		FROM 
			StagePercentageDetailsTask
		WHERE 
			numDomainID=@numDomainID 
			AND numTaskId = @numTaskID

		IF(@numOppId>0 OR @numProjectId>0 OR @numWorkOrderID > 0)
		BEGIN
			DECLARE @intTotalProgress AS INT =0
			DECLARE @intTotalTaskCount AS INT=0
			DECLARE @intTotalTaskClosed AS INT =0
			
			SET @intTotalTaskCount=(SELECT COUNT(*) FROM StagePercentageDetailsTask WHERE (numOppId=@numOppId AND numProjectId=@numProjectId AND ISNULL(numWorkOrderId,0)=@numWorkOrderID) AND bitSavedTask=1)
			SET @intTotalTaskClosed=(SELECT COUNT(*) FROM StagePercentageDetailsTask WHERE (numOppId=@numOppId AND numProjectId=@numProjectId AND ISNULL(numWorkOrderId,0)=@numWorkOrderID) AND bitSavedTask=1 AND bitTaskClosed=1)
			
			IF(@intTotalTaskCount>0 AND @intTotalTaskClosed>0)
			BEGIN
				SET @intTotalProgress=ROUND(((@intTotalTaskClosed*100)/@intTotalTaskCount),0)
			END
			IF((SELECT COUNT(*) FROM ProjectProgress WHERE (ISNULL(numOppId,0)=@numOppId AND ISNULL(numProId,0)=@numProjectId AND ISNULL(numWorkOrderId,0)=@numWorkOrderID)AND numDomainId=@numDomainID)>0)
			BEGIN
				UPDATE ProjectProgress SET intTotalProgress=@intTotalProgress WHERE (ISNULL(numOppId,0)=@numOppId AND ISNULL(numProId,0)=@numProjectId AND ISNULL(numWorkOrderId,0)=@numWorkOrderID) AND numDomainId=@numDomainID
			END
			ELSE
			BEGIN
				IF(@numOppId=0)
				BEGIN
					SET @numOppId = NULL
				END
				IF(@numProjectId=0)
				BEGIN
					SET @numProjectId = NULL
				END
				IF (@numWorkOrderID = 0)
				BEGIN
					SET @numWorkOrderID = NULL
				END

				INSERT INTO ProjectProgress
				(
					numOppId,
					numProId,
					numWorkOrderID,
					numDomainId,
					intTotalProgress
				)VALUES(
					@numOppId,
					@numProjectId,
					@numWorkOrderID,
					@numDomainID,
					@intTotalProgress
				)
			END
		END
	END
END
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_StagePercentageDetailsTaskTimeLog_Delete')
DROP PROCEDURE dbo.USP_StagePercentageDetailsTaskTimeLog_Delete
GO
CREATE PROCEDURE [dbo].[USP_StagePercentageDetailsTaskTimeLog_Delete]
(
	@numDomainID NUMERIC(18,0)
	,@ID NUMERIC(18,0)
	,@numTaskID NUMERIC(18,0)
)
AS 
BEGIN
	IF @ID = -1
	BEGIN
		DELETE FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID=@numTaskID
		UPDATE StagePercentageDetailsTask SET bitTimeAddedToContract=0 WHERE numDomainID=@numDomainID AND numTaskID=@numTaskID
	END
	ELSE
	BEGIN
		DELETE FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID=@numTaskID AND ID=@ID
	END
END
GO
/****** Object:  StoredProcedure [dbo].[USP_UpdatedomainDetails]    Script Date: 05/07/2009 21:56:59 ******/
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatedomaindetails')
DROP PROCEDURE usp_updatedomaindetails
GO
CREATE PROCEDURE [dbo].[USP_UpdatedomainDetails]                                      
@numDomainID as numeric(9)=0,                                      
@vcDomainName as varchar(50)='',                                      
@vcDomainDesc as varchar(100)='',                                      
@bitExchangeIntegration as bit,                                      
@bitAccessExchange as bit,                                      
@vcExchUserName as varchar(100)='',                                      
@vcExchPassword as varchar(100)='',                                      
@vcExchPath as varchar(200)='',                                      
@vcExchDomain as varchar(100)='',                                    
@tintCustomPagingRows as tinyint,                                    
@vcDateFormat as varchar(30)='',                                    
@numDefCountry as numeric(9),                                    
@tintComposeWindow as tinyint,                                    
@sintStartDate as smallint,                                    
@sintNoofDaysInterval as smallint,                                    
@tintAssignToCriteria as tinyint,                                    
@bitIntmedPage as bit,                                    
@tintFiscalStartMonth as tinyint,                                      
@tintPayPeriod as tinyint,                        
@numCurrencyID as numeric(9) ,                
@charUnitSystem as char(1)='',              
@vcPortalLogo as varchar(100)='',            
@tintChrForComSearch as tinyint  ,            
@intPaymentGateWay as int, -- DO NOT UPDATE VALUE FROM THIS PARAMETER
@tintChrForItemSearch as tinyint,
@ShipCompany as numeric(9),
@bitMultiCurrency as bit,
@bitCreateInvoice AS tinyint = null,
@numDefaultSalesBizDocID AS NUMERIC(9)=NULL,
@numDefaultPurchaseBizDocID AS NUMERIC(9)=NULL,
@bitSaveCreditCardInfo AS BIT = NULL,
@intLastViewedRecord AS INT=10,
@tintCommissionType AS TINYINT,
@tintBaseTax AS TINYINT,
@bitEmbeddedCost AS BIT,
@numDefaultSalesOppBizDocId NUMERIC=NULL,
@tintSessionTimeOut TINYINT=1,
@tintDecimalPoints TINYINT,
@bitCustomizePortal BIT=0,
@tintBillToForPO TINYINT,
@tintShipToForPO TINYINT,
@tintOrganizationSearchCriteria TINYINT,
@bitDocumentRepositary BIT=0,
@tintComAppliesTo TINYINT=0,
@bitGtoBContact BIT=0,
@bitBtoGContact BIT=0,
@bitGtoBCalendar BIT=0,
@bitBtoGCalendar BIT=0,
@bitExpenseNonInventoryItem BIT=0,
@bitInlineEdit BIT=0,
@bitRemoveVendorPOValidation BIT=0,
@tintBaseTaxOnArea tinyint=0,
@bitAmountPastDue BIT=0,
@monAmountPastDue DECIMAL(20,5),
@bitSearchOrderCustomerHistory BIT=0,
@tintCalendarTimeFormat as tinyint=NULL,
@tintDefaultClassType as tinyint=NULL,
@tintPriceBookDiscount as tinyint=0,
@bitAutoSerialNoAssign as bit=0,
@bitIsShowBalance AS BIT = 0,
@numIncomeAccID NUMERIC(18, 0) = 0,
@numCOGSAccID NUMERIC(18, 0) = 0,
@numAssetAccID NUMERIC(18, 0) = 0,
@IsEnableProjectTracking AS BIT = 0,
@IsEnableCampaignTracking AS BIT = 0,
@IsEnableResourceScheduling AS BIT = 0,
@ShippingServiceItem  AS NUMERIC(9)=0,
@numSOBizDocStatus AS NUMERIC(9)=0,
@bitAutolinkUnappliedPayment AS BIT=0,
@numDiscountServiceItemID AS NUMERIC(9)=0,
@vcShipToPhoneNumber as VARCHAR(50)='',
@vcGAUserEmailId as VARCHAR(50)='',
@vcGAUserPassword as VARCHAR(50)='',
@vcGAUserProfileId as VARCHAR(50)='',
@bitDiscountOnUnitPrice AS BIT=0,
@intShippingImageWidth INT = 0,
@intShippingImageHeight INT = 0,
@numTotalInsuredValue NUMERIC(10,2) = 0,
@numTotalCustomsValue NUMERIC(10,2) = 0,
@vcHideTabs VARCHAR(1000) = '',
@bitUseBizdocAmount BIT = 0,
@bitDefaultRateType BIT = 0,
@bitIncludeTaxAndShippingInCommission  BIT = 0,
@bitPurchaseTaxCredit BIT=0,
@bitLandedCost BIT=0,
@vcLanedCostDefault VARCHAR(10)='',
@bitMinUnitPriceRule BIT = 0,
@numAbovePercent AS NUMERIC(18,2),
@numBelowPercent AS NUMERIC(18,2),
@numBelowPriceField AS NUMERIC(18,0),
@vcUnitPriceApprover AS VARCHAR(100),
@numDefaultSalesPricing AS TINYINT,
@bitReOrderPoint AS BIT = 0,
@numReOrderPointOrderStatus AS NUMERIC(18,0) = 0,
@numPODropShipBizDoc AS NUMERIC(18,0) = 0,
@numPODropShipBizDocTemplate AS NUMERIC(18,0) = 0,
@IsEnableDeferredIncome AS BIT = 0,
@bitApprovalforTImeExpense AS BIT=0,
@numCost AS Numeric(9,0)=0,
@intTimeExpApprovalProcess int=0,
@bitApprovalforOpportunity AS BIT=0,
@intOpportunityApprovalProcess int=0,
@numDefaultSalesShippingDoc NUMERIC(18,0)=0,
@numDefaultPurchaseShippingDoc NUMERIC(18,0)=0,
@bitchkOverRideAssignto AS BIT=0,
@vcPrinterIPAddress VARCHAR(15) = '',
@vcPrinterPort VARCHAR(5) = '',
@numDefaultSiteID NUMERIC(18,0) = 0,
@bitRemoveGlobalLocation BIT = 0,
@numAuthorizePercentage NUMERIC(18,0)=0,
@bitEDI BIT = 0,
@bit3PL BIT = 0,
@bitAllowDuplicateLineItems BIT = 0,
@tintCommitAllocation TINYINT = 1,
@tintInvoicing TINYINT = 1,
@tintMarkupDiscountOption TINYINT = 1,
@tintMarkupDiscountValue TINYINT = 1,
@bitCommissionBasedOn BIT = 0,
@tintCommissionBasedOn TINYINT = 1,
@bitDoNotShowDropshipPOWindow BIT = 0,
@tintReceivePaymentTo TINYINT = 2,
@numReceivePaymentBankAccount NUMERIC(18,0) = 0,
@numOverheadServiceItemID NUMERIC(18,0) = 0,
@bitDisplayCustomField BIT = 0,
@bitFollowupAnytime BIT = 0,
@bitpartycalendarTitle BIT = 0,
@bitpartycalendarLocation BIT = 0,
@bitpartycalendarDescription BIT = 0,
@bitREQPOApproval BIT = 0,
@bitARInvoiceDue BIT = 0,
@bitAPBillsDue BIT = 0,
@bitItemsToPickPackShip BIT = 0,
@bitItemsToInvoice BIT = 0,
@bitSalesOrderToClose BIT = 0,
@bitItemsToPutAway BIT = 0,
@bitItemsToBill BIT = 0,
@bitPosToClose BIT = 0,
@bitPOToClose BIT = 0,
@bitBOMSToPick BIT = 0,
@vchREQPOApprovalEmp  VARCHAR(500)='',
@vchARInvoiceDue VARCHAR(500)='',
@vchAPBillsDue VARCHAR(500)='',
@vchItemsToPickPackShip VARCHAR(500)='',
@vchItemsToInvoice VARCHAR(500)='',
@vchPriceMarginApproval VARCHAR(500)='',
@vchSalesOrderToClose VARCHAR(500)='',
@vchItemsToPutAway VARCHAR(500)='',
@vchItemsToBill VARCHAR(500)='',
@vchPosToClose VARCHAR(500)='',
@vchPOToClose VARCHAR(500)='',
@vchBOMSToPick VARCHAR(500)='',
@decReqPOMinValue VARCHAR(500)='',
@bitUseOnlyActionItems BIT = 0,
@bitDisplayContractElement BIT,
@vcEmployeeForContractTimeElement VARCHAR(500)='',
@numARContactPosition NUMERIC(18,0) = 0,
@bitShowCardConnectLink BIT = 0,
@vcBluePayFormName VARCHAR(500)='',
@vcBluePaySuccessURL VARCHAR(500)='',
@vcBluePayDeclineURL VARCHAR(500)='',
@bitUseDeluxeCheckStock BIT = 1,
@bitEnableSmartyStreets BIT=0,
@vcSmartyStreetsAPIKeys VARCHAR(500)=''
as                                      
BEGIN
BEGIN TRY
	IF ISNULL(@bitMinUnitPriceRule,0) = 1 AND ((SELECT COUNT(*) FROM UnitPriceApprover WHERE numDomainID=@numDomainID) = 0 OR ISNULL(@intOpportunityApprovalProcess,0) = 0)
	BEGIN
		RAISERROR('INCOMPLETE_MIN_UNIT_PRICE_CONFIGURATION',16,1)
		RETURN
	END

	If ISNULL(@bitApprovalforTImeExpense,0) = 1 AND (SELECT
														COUNT(*) 
													FROM
														UserMaster
													LEFT JOIN
														ApprovalConfig
													ON
														ApprovalConfig.numUserId = UserMaster.numUserDetailId
													WHERE
														UserMaster.numDomainID=@numDomainID
														AND numModule=1
														AND ISNULL(numGroupID,0) > 0
														AND ISNULL(bitActivateFlag,0) = 1
														AND ISNULL(numLevel1Authority,0) = 0
														AND ISNULL(numLevel2Authority,0) = 0
														AND ISNULL(numLevel3Authority,0) = 0
														AND ISNULL(numLevel4Authority,0) = 0
														AND ISNULL(numLevel5Authority,0) = 0) > 0
	BEGIN
		RAISERROR('INCOMPLETE_TIMEEXPENSE_APPROVAL',16,1)
		RETURN
	END

	DECLARE @tintCommitAllocationOld AS TINYINT
	SELECT @tintCommitAllocationOld=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainID=@numDomainID

	IF @tintCommitAllocation <> @tintCommitAllocationOld AND (SELECT COUNT(*) FROM OpportunityMaster WHERE numDomainId=@numDomainID AND tintOppType=1 AND tintOppStatus=1 AND ISNULL(tintshipped,0)=0) > 0
	BEGIN
		RAISERROR('NOT_ALLOWED_CHANGE_COMMIT_ALLOCATION_OPEN_SALES_ORDER',16,1)
		RETURN
	END
                                  
	UPDATE 
		Domain                                       
	SET                                       
		vcDomainName=@vcDomainName,                                      
		vcDomainDesc=@vcDomainDesc,                                      
		bitExchangeIntegration=@bitExchangeIntegration,                                      
		bitAccessExchange=@bitAccessExchange,                                      
		vcExchUserName=@vcExchUserName,                                      
		vcExchPassword=@vcExchPassword,                                      
		vcExchPath=@vcExchPath,                                      
		vcExchDomain=@vcExchDomain,                                    
		tintCustomPagingRows =@tintCustomPagingRows,                                    
		vcDateFormat=@vcDateFormat,                                    
		numDefCountry =@numDefCountry,                                    
		tintComposeWindow=@tintComposeWindow,                                    
		sintStartDate =@sintStartDate,                                    
		sintNoofDaysInterval=@sintNoofDaysInterval,                                    
		tintAssignToCriteria=@tintAssignToCriteria,                                    
		bitIntmedPage=@bitIntmedPage,                                    
		tintFiscalStartMonth=@tintFiscalStartMonth,                                                      
		tintPayPeriod=@tintPayPeriod,
		vcCurrency=isnull((select varCurrSymbol from Currency Where numCurrencyID=@numCurrencyID  ),'')  ,                      
		numCurrencyID=@numCurrencyID,                
		charUnitSystem= @charUnitSystem,              
		vcPortalLogo=@vcPortalLogo ,            
		tintChrForComSearch=@tintChrForComSearch  ,      
		tintChrForItemSearch=@tintChrForItemSearch,
		numShipCompany= @ShipCompany,
		bitMultiCurrency=@bitMultiCurrency,
		bitCreateInvoice= @bitCreateInvoice,
		[numDefaultSalesBizDocId] =@numDefaultSalesBizDocId,
		bitSaveCreditCardInfo=@bitSaveCreditCardInfo,
		intLastViewedRecord=@intLastViewedRecord,
		[tintCommissionType]=@tintCommissionType,
		[tintBaseTax]=@tintBaseTax,
		[numDefaultPurchaseBizDocId]=@numDefaultPurchaseBizDocID,
		bitEmbeddedCost=@bitEmbeddedCost,
		numDefaultSalesOppBizDocId=@numDefaultSalesOppBizDocId,
		tintSessionTimeOut=@tintSessionTimeOut,
		tintDecimalPoints=@tintDecimalPoints,
		bitCustomizePortal=@bitCustomizePortal,
		tintBillToForPO = @tintBillToForPO,
		tintShipToForPO = @tintShipToForPO,
		tintOrganizationSearchCriteria=@tintOrganizationSearchCriteria,
		bitDocumentRepositary=@bitDocumentRepositary,
		bitGtoBContact=@bitGtoBContact,
		bitBtoGContact=@bitBtoGContact,
		bitGtoBCalendar=@bitGtoBCalendar,
		bitBtoGCalendar=@bitBtoGCalendar,
		bitExpenseNonInventoryItem=@bitExpenseNonInventoryItem,
		bitInlineEdit=@bitInlineEdit,
		bitRemoveVendorPOValidation=@bitRemoveVendorPOValidation,
		tintBaseTaxOnArea=@tintBaseTaxOnArea,
		bitAmountPastDue = @bitAmountPastDue,
		monAmountPastDue = @monAmountPastDue,
		bitSearchOrderCustomerHistory=@bitSearchOrderCustomerHistory,
		tintCalendarTimeFormat=@tintCalendarTimeFormat,
		tintDefaultClassType=@tintDefaultClassType,
		tintPriceBookDiscount=@tintPriceBookDiscount,
		bitAutoSerialNoAssign=@bitAutoSerialNoAssign,
		bitIsShowBalance = @bitIsShowBalance,
		numIncomeAccID = @numIncomeAccID,
		numCOGSAccID = @numCOGSAccID,
		numAssetAccID = @numAssetAccID,
		IsEnableProjectTracking = @IsEnableProjectTracking,
		IsEnableCampaignTracking = @IsEnableCampaignTracking,
		IsEnableResourceScheduling = @IsEnableResourceScheduling,
		numShippingServiceItemID = @ShippingServiceItem,
		numSOBizDocStatus=@numSOBizDocStatus,
		bitAutolinkUnappliedPayment=@bitAutolinkUnappliedPayment,
		numDiscountServiceItemID=@numDiscountServiceItemID,
		vcShipToPhoneNo = @vcShipToPhoneNumber,
		vcGAUserEMail = @vcGAUserEmailId,
		vcGAUserPassword = @vcGAUserPassword,
		vcGAUserProfileId = @vcGAUserProfileId,
		bitDiscountOnUnitPrice=@bitDiscountOnUnitPrice,
		intShippingImageWidth = @intShippingImageWidth ,
		intShippingImageHeight = @intShippingImageHeight,
		numTotalInsuredValue = @numTotalInsuredValue,
		numTotalCustomsValue = @numTotalCustomsValue,
		vcHideTabs = @vcHideTabs,
		bitUseBizdocAmount = @bitUseBizdocAmount,
		bitDefaultRateType = @bitDefaultRateType,
		bitIncludeTaxAndShippingInCommission = @bitIncludeTaxAndShippingInCommission,
		bitPurchaseTaxCredit=@bitPurchaseTaxCredit,
		bitLandedCost=@bitLandedCost,
		vcLanedCostDefault=@vcLanedCostDefault,
		bitMinUnitPriceRule = @bitMinUnitPriceRule,
		numDefaultSalesPricing = @numDefaultSalesPricing,
		numPODropShipBizDoc=@numPODropShipBizDoc,
		numPODropShipBizDocTemplate=@numPODropShipBizDocTemplate,
		IsEnableDeferredIncome=@IsEnableDeferredIncome,
		bitApprovalforTImeExpense=@bitApprovalforTImeExpense,
		numCost= @numCost,intTimeExpApprovalProcess=@intTimeExpApprovalProcess,
		bitApprovalforOpportunity=@bitApprovalforOpportunity,intOpportunityApprovalProcess=@intOpportunityApprovalProcess,
		numDefaultPurchaseShippingDoc=@numDefaultPurchaseShippingDoc,numDefaultSalesShippingDoc=@numDefaultSalesShippingDoc,
		bitchkOverRideAssignto=@bitchkOverRideAssignto,
		vcPrinterIPAddress=@vcPrinterIPAddress,
		vcPrinterPort=@vcPrinterPort,
		numDefaultSiteID=ISNULL(@numDefaultSiteID,0),
		bitRemoveGlobalLocation=@bitRemoveGlobalLocation,
		numAuthorizePercentage=@numAuthorizePercentage,
		bitEDI=ISNULL(@bitEDI,0),
		bit3PL=ISNULL(@bit3PL,0),
		bitAllowDuplicateLineItems=ISNULL(@bitAllowDuplicateLineItems,0),
		tintCommitAllocation=ISNULL(@tintCommitAllocation,1),
		tintInvoicing = ISNULL(@tintInvoicing,1),
		tintMarkupDiscountOption = ISNULL(@tintMarkupDiscountOption,1),
		tintMarkupDiscountValue = ISNULL(@tintMarkupDiscountValue,1)
		,bitCommissionBasedOn=ISNULL(@bitCommissionBasedOn,0)
		,tintCommissionBasedOn=ISNULL(@tintCommissionBasedOn,1)
		,bitDoNotShowDropshipPOWindow=ISNULL(@bitDoNotShowDropshipPOWindow,0)
		,tintReceivePaymentTo=ISNULL(@tintReceivePaymentTo,2)
		,numReceivePaymentBankAccount = ISNULL(@numReceivePaymentBankAccount,0)
		,numOverheadServiceItemID=@numOverheadServiceItemID,
		bitDisplayCustomField=@bitDisplayCustomField,
		bitFollowupAnytime=@bitFollowupAnytime,
		bitpartycalendarTitle=@bitpartycalendarTitle,
		bitpartycalendarLocation=@bitpartycalendarLocation,
		bitpartycalendarDescription=@bitpartycalendarDescription,
		bitREQPOApproval=@bitREQPOApproval,
		bitARInvoiceDue=@bitARInvoiceDue,
		bitAPBillsDue=@bitAPBillsDue,
		bitItemsToPickPackShip=@bitItemsToPickPackShip,
		bitItemsToInvoice=@bitItemsToInvoice,
		bitSalesOrderToClose=@bitSalesOrderToClose,
		bitItemsToPutAway=@bitItemsToPutAway,
		bitItemsToBill=@bitItemsToBill,
		bitPosToClose=@bitPosToClose,
		bitPOToClose=@bitPOToClose,
		bitBOMSToPick=@bitBOMSToPick,
		vchREQPOApprovalEmp=@vchREQPOApprovalEmp,
		vchARInvoiceDue=@vchARInvoiceDue,
		vchAPBillsDue=@vchAPBillsDue,
		vchItemsToPickPackShip=@vchItemsToPickPackShip,
		vchItemsToInvoice=@vchItemsToInvoice,
		vchSalesOrderToClose=@vchSalesOrderToClose,
		vchItemsToPutAway=@vchItemsToPutAway,
		vchItemsToBill=@vchItemsToBill,
		vchPosToClose=@vchPosToClose,
		vchPOToClose=@vchPOToClose,
		vchBOMSToPick=@vchBOMSToPick,
		decReqPOMinValue=@decReqPOMinValue,
		bitUseOnlyActionItems = @bitUseOnlyActionItems,
		bitDisplayContractElement = @bitDisplayContractElement,
		vcEmployeeForContractTimeElement=@vcEmployeeForContractTimeElement,
		numARContactPosition= @numARContactPosition,
		bitShowCardConnectLink = @bitShowCardConnectLink,
		vcBluePayFormName=@vcBluePayFormName,
		vcBluePaySuccessURL=@vcBluePaySuccessURL,
		vcBluePayDeclineURL=@vcBluePayDeclineURL,
		bitUseDeluxeCheckStock=@bitUseDeluxeCheckStock,
		bitEnableSmartyStreets = @bitEnableSmartyStreets,
		vcSmartyStreetsAPIKeys = @vcSmartyStreetsAPIKeys
	WHERE 
		numDomainId=@numDomainID

	IF(LEN(@vchPriceMarginApproval)>0)
	BEGIN
		DELETE FROM UnitPriceApprover WHERE numDomainID = @numDomainID

		INSERT INTO UnitPriceApprover
			(
				numDomainID,
				numUserID
			)SELECT @numDomainID,Items FROM Split(@vchPriceMarginApproval,',') WHERE Items<>''
	END
	IF ISNULL(@bitRemoveGlobalLocation,0) = 1
	BEGIN
		DECLARE @TEMPGlobalWarehouse TABLE
		(
			ID INT IDENTITY(1,1)
			,numItemCode NUMERIC(18,0)
			,numWarehouseID NUMERIC(18,0)
			,numWarehouseItemID NUMERIC(28,0)
		)

		INSERT INTO @TEMPGlobalWarehouse
		(
			numItemCode
			,numWarehouseID
			,numWarehouseItemID
		)
		SELECT
			numItemID
			,numWareHouseID
			,numWareHouseItemID
		FROM
			WareHouseItems
		WHERE
			numDomainID=@numDomainID
			AND numWLocationID = -1
			AND ISNULL(numOnHand,0) = 0
			AND ISNULL(numOnOrder,0)=0
			AND ISNULL(numAllocation,0)=0
			AND ISNULL(numBackOrder,0)=0


		DECLARE @i AS INT = 1
		DECLARE @iCount AS INT
		DECLARE @numTempItemCode AS NUMERIC(18,0)
		DECLARE @numTempWareHouseID AS NUMERIC(18,0)
		DECLARE @numTempWareHouseItemID AS NUMERIC(18,0)
		SELECT @iCount=COUNT(*) FROM @TEMPGlobalWarehouse

		WHILE @i <= @iCount
		BEGIN
			SELECT @numTempItemCode=numItemCode,@numTempWareHouseID=numWarehouseID,@numTempWareHouseItemID=numWarehouseItemID FROM @TEMPGlobalWarehouse WHERE ID=@i

			BEGIN TRY
				EXEC USP_AddUpdateWareHouseForItems @numTempItemCode,  
													@numTempWareHouseID,
													@numTempWareHouseItemID,
													'',
													0,
													0,
													0,
													'',
													'',
													@numDomainID,
													'',
													'',
													'',
													0,
													3,
													0,
													0,
													NULL,
													0
			END TRY
			BEGIN CATCH

			END CATCH

			SET @i = @i + 1
		END
	END
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH

END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_UpdateSingleFieldValue' ) 
    DROP PROCEDURE USP_UpdateSingleFieldValue
GO

CREATE PROCEDURE USP_UpdateSingleFieldValue
	@tintMode AS TINYINT,
	@numUpdateRecordID NUMERIC,
	@numUpdateValueID NUMERIC=0,
	@vcText AS TEXT,
	@numDomainID AS NUMERIC= 0 
AS 
BEGIN
	 -- For 26 and 37
     DECLARE @Status AS NUMERIC(9,0)
     DECLARE @SiteID       AS NUMERIC(9,0)
     DECLARE @OrderStatus  AS NUMERIC(9,0)
	 DECLARE @BizdocStatus AS NUMERIC(9,0)  
	 DECLARE @numDomain AS NUMERIC(18,0)
	 SET @numDomain = @numDomainID 

	IF @tintMode=1
	BEGIN
		UPDATE  dbo.OpportunityBizDocsDetails
		SET     bitAmountCaptured = @numUpdateValueID
		WHERE   numBizDocsPaymentDetId = @numUpdateRecordID
	END
--	IF @tintMode = 2 
--		BEGIN
--			UPDATE  dbo.OpportunityMaster
--			SET     numStatus = @numUpdateValueID
--			WHERE   numOppId = @numUpdateRecordID
--		END
	IF @tintMode = 3
		BEGIN
			 UPDATE AdditionalContactsInformation SET txtNotes=ISNULL(CAST(txtNotes AS VARCHAR(8000)),'') + ' ' +  ISNULL(CAST(@vcText AS VARCHAR(8000)),'') WHERE numContactId=@numUpdateRecordID
		END
	IF @tintMode = 4
		BEGIN
			 UPDATE dbo.OpportunityBizDocsDetails SET numBillPaymentJournalID=@numUpdateValueID WHERE numBizDocsPaymentDetId=@numUpdateRecordID
		END
		
	IF @tintMode = 5
		BEGIN
			 UPDATE dbo.BizDocComission SET bitCommisionPaid=@numUpdateValueID WHERE numOppBizDocID=@numUpdateRecordID
		END
		
	IF @tintMode = 6
		BEGIN
			 UPDATE dbo.OpportunityRecurring SET numOppBizDocID=@numUpdateValueID WHERE numOppRecID=@numUpdateRecordID
		END
		
	IF @tintMode = 7
		BEGIN
			 UPDATE dbo.OpportunityRecurring SET dtRecurringDate=CAST( CAST( @vcText AS VARCHAR) AS DATETIME) WHERE numOppRecID=@numUpdateRecordID
		END
		
	IF @tintMode = 8
		BEGIN
			 UPDATE dbo.OpportunityRecurring SET fltBreakupPercentage=CAST( CAST( @vcText AS VARCHAR) AS float) WHERE numOppRecID=@numUpdateRecordID
		END
		/*Commented by chintan.. this is moved to USP_ShoppingCart SP*/
--	IF @tintMode = 9
--		BEGIN
--			UPDATE  dbo.OpportunityMaster
--			SET     txtComments = CAST(@vcText AS VARCHAR(1000))
--			WHERE   numOppId = @numUpdateRecordID
--		END    
--	IF @tintMode = 10
--		BEGIN
--			UPDATE  dbo.OpportunityMaster
--			SET     vcRefOrderNo = CAST(@vcText AS VARCHAR(100))
--			WHERE   numOppId = @numUpdateRecordID
--		END    
	IF @tintMode = 11
		BEGIN
			DELETE FROM dbo.LuceneItemsIndex WHERE numItemCode=@numUpdateRecordID
		END    
	IF @tintMode = 12
		BEGIN
			UPDATE dbo.Domain SET vcPortalName = CAST(@vcText AS VARCHAR(50))
			WHERE numDomainId=@numUpdateRecordID
		END 
	IF @tintMode = 13
		BEGIN
			UPDATE dbo.WareHouseItems SET numOnHand =numOnHand + CAST(CAST(@vcText AS VARCHAR(50)) AS numeric(18,0)),dtModified = GETDATE() 
			WHERE numWareHouseItemID = @numUpdateRecordID
			
			DECLARE @numItemCode NUMERIC(18)

			SELECT @numItemCode=numItemID from WareHouseItems where numWareHouseItemID = @numUpdateRecordID

			EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numUpdateRecordID, --  numeric(9, 0)
				@numReferenceID = @numItemCode, --  numeric(9, 0)
				@tintRefType = 1, --  tinyint
				@vcDescription = 'Inventory Adjustment', --  varchar(100)
				@numModifiedBy = @numUpdateValueID,
				@numDomainID = @numDomain
			 
		END
	IF @tintMode = 14
		BEGIN
			UPDATE dbo.Item SET monAverageCost=CAST(CAST(@vcText AS VARCHAR(50)) AS numeric(20,5))
			WHERE numItemCode = @numUpdateRecordID
		END
		IF @tintMode = 15
		BEGIN
			UPDATE dbo.EmailHistory  SET numListItemId = @numUpdateValueID  WHERE numEmailHstrID=@numUpdateRecordID
		END
		IF @tintMode = 16
		BEGIN
			UPDATE dbo.EmailHistory SET bitIsRead=@numUpdateValueID WHERE numEmailHstrID IN ( SELECT Id FROM dbo.SplitIDs(CAST(@vcText AS VARCHAR(200)) ,','))
		END
		IF @tintMode = 17
		BEGIN
			UPDATE CartItems SET numUserCntId = @numUpdateValueID WHERE vcCookieId =CAST(@vcText AS VARCHAR(MAX)) AND numUserCntId = 0
		END
		IF @tintMode = 18
		BEGIN
			UPDATE OpportunityBizDocs SET tintDeferred = 0 WHERE numOppBizDocsId=@numUpdateRecordID
		END
		IF @tintMode = 19
		BEGIN
			UPDATE dbo.OpportunityMaster SET vcPOppName=CAST(@vcText AS VARCHAR(100)) WHERE numOppId=@numUpdateRecordID
		END
	    IF @tintMode = 20
		BEGIN
			UPDATE UserMaster SET tintTabEvent=@numUpdateValueID WHERE numUserId=@numUpdateRecordID
		END
		IF @tintMode = 21
		BEGIN
			UPDATE OpportunityBizDocs SET numBizDocTempID = @numUpdateValueID WHERE numOppBizDocsId=@numUpdateRecordID
		END
		IF @tintMode = 22
		BEGIN
			UPDATE OpportunityBizDocItems SET tintTrackingStatus = 1 WHERE numOppBizDocItemID =@numUpdateRecordID
		END
		IF	 @tintMode = 23
		BEGIN
			UPDATE item SET bintModifiedDate = GETUTCDATE() WHERE numItemCode=@numUpdateRecordID			
		END
		IF @tintMode = 24
		BEGIN
			UPDATE dbo.OpportunityMaster SET numStatus=@numUpdateValueID WHERE numOppId in (select Items from dbo.split(@vcText,','))
		END
		
		IF @tintMode = 25
		BEGIN
			UPDATE dbo.WebAPIDetail SET vcEighthFldValue = @vcText WHERE WebApiId = @numUpdateRecordID AND numDomainId = @numDomainID
		END
		IF @tintMode = 26
		BEGIN
			
		     SELECT  @Status = ISNULL(numStatus ,0)  FROM dbo.OpportunityMaster WHERE numOppId = @numUpdateRecordID
		     
		     IF @Status = 0
		      BEGIN
				SELECT @SiteID  = CONVERT(NUMERIC(9,0),tintSource)   FROM	dbo.OpportunityMaster WHERE numOppId = @numUpdateRecordID
			    SELECT @OrderStatus = ISNULL(numOrderStatus,0) ,@BizdocStatus = ISNULL(numBizDocStatus,0)  FROM dbo.eCommercePaymentConfig WHERE numSiteId = @SiteID AND numPaymentMethodId = 31488
		       
			    UPDATE dbo.OpportunityMaster SET numStatus = ISNULL(@OrderStatus,0) WHERE numOppId = @numUpdateRecordID AND numDomainId = @numDomainID 
			    Update OpportunityBizDocs set monAmountPaid = ISNULL(monDealAmount,0),numBizDocStatus = @BizdocStatus  WHERE  numOppId = @numUpdateRecordID and bitAuthoritativeBizDocs =1
			  END   
		END
		IF @tintMode = 27
		BEGIN
			UPDATE OpportunityBizDocs SET numBizDocStatusOLD=numBizDocStatus,numBizDocStatus = @numUpdateValueID WHERE numOppBizDocsId=@numUpdateRecordID 
		END
	    IF @tintMode = 28
		BEGIN
			UPDATE General_Journal_Details SET bitReconcile=(CASE WHEN CAST(@vcText AS VARCHAR(5))='R' THEN 1 ELSE 0 END),
			bitCleared=(CASE WHEN CAST(@vcText AS VARCHAR(5))='C' THEN 1 ELSE 0 END),numReconcileID=0
			WHERE numDomainId=@numDomainID AND numTransactionId=@numUpdateRecordID 
		END
		
		IF @tintMode = 29
		BEGIN
			UPDATE dbo.ReturnHeader SET numRMATempID = @numUpdateValueID WHERE numReturnHeaderID = @numUpdateRecordID 
		END
		
		IF @tintMode = 30
		BEGIN
			UPDATE dbo.ReturnHeader SET numBizdocTempID = @numUpdateValueID WHERE numReturnHeaderID = @numUpdateRecordID 
		END
		
		IF @tintMode = 31
		BEGIN
			UPDATE dbo.OpportunityMaster SET numOppBizDocTempID = @numUpdateValueID WHERE numOppId = @numUpdateRecordID 
		END
		/*Commented by chintan.. this is moved to USP_ShoppingCart SP*/
--		IF @tintMode = 32
--		BEGIN
--			DECLARE @numBizDocId AS NUMERIC(9,0)
--			SELECT @numBizDocId = numBizDocId FROM eCommercePaymentConfig WHERE  numPaymentMethodId = @numUpdateValueId AND numDomainID = @numDomainID   
--			UPDATE dbo.OpportunityMaster SET numOppBizDocTempID = @numBizDocId WHERE numOppId = @numUpdateRecordID 
--		END
		
		IF @tintMode = 33
		BEGIN
			UPDATE dbo.ReturnHeader SET numBizdocTempID = @numUpdateValueID,numRMATempID = @numUpdateValueID WHERE numReturnHeaderID = @numUpdateRecordID 
		END
		
			
		IF @tintMode = 34
		BEGIN
			UPDATE dbo.WebAPIDetail SET vcFourteenthFldValue = @numUpdateValueID WHERE WebApiId = @numUpdateRecordID AND numDomainId = @numDomainID
		END
		
		IF @tintMode = 35
		BEGIN
			UPDATE OpportunityMasterTaxItems SET fltPercentage = CAST(CAST(@vcText AS VARCHAR(10)) AS FLOAT)
			WHERE numOppId = @numUpdateRecordID AND numTaxItemID = 0
		END
		
		IF @tintMode = 36
		BEGIN
			UPDATE dbo.Import_File_Master SET numProcessedCSVRowNumber = @numUpdateValueID
			WHERE intImportFileID = @numUpdateRecordID AND numDomainID = @numDomainID
		END
		IF @tintMode = 37
		BEGIN
		     SELECT  @Status = ISNULL(numStatus ,0)  FROM dbo.OpportunityMaster WHERE numOppId = @numUpdateRecordID
		     
		     IF @Status = 0
		      BEGIN
			    SELECT @SiteID  = CONVERT(NUMERIC(9,0),tintSource)   FROM	dbo.OpportunityMaster WHERE numOppId = @numUpdateRecordID
			    SELECT @OrderStatus = ISNULL(numFailedOrderStatus,0)    FROM dbo.eCommercePaymentConfig WHERE numSiteId = @SiteID AND numPaymentMethodId = 31488
		        
			    UPDATE dbo.OpportunityMaster SET numStatus = ISNULL(@OrderStatus,0) WHERE numOppId = @numUpdateRecordID AND numDomainId = @numDomainID 
			    
			  END   
		END
		IF @tintMode = 38
		BEGIN
			UPDATE [WebAPIDetail] SET    vcNinthFldValue = CAST(CAST(@vcText AS VARCHAR(30)) AS DATETIME)
			WHERE  [WebApiId] =  @numUpdateRecordID AND [numDomainId] = @numDomainId
		END
		
		IF @tintMode = 39
		BEGIN
			UPDATE [WebAPIDetail] SET    vcSixteenthFldValue = CAST(CAST(@vcText AS VARCHAR(30)) AS DATETIME)
			WHERE  [WebApiId] =  @numUpdateRecordID AND [numDomainId] = @numDomainId
		END

		IF @tintMode = 40
		BEGIN
			UPDATE dbo.WebAPIDetail SET bitEnableAPI = @numUpdateValueID WHERE WebApiId = @numUpdateRecordID AND numDomainId = @numDomainID
		END

		IF @tintMode = 41
		BEGIN
			UPDATE dbo.domain SET numShippingServiceItemID = @numUpdateValueID WHERE numDomainId = @numDomainID
		END
		
		IF @tintMode = 42
		BEGIN
			UPDATE dbo.domain SET numDiscountServiceItemID = @numUpdateValueID WHERE numDomainId = @numDomainID
		END
		
		IF @tintMode = 43
		BEGIN
			UPDATE dbo.Cases SET numStatus = @numUpdateValueID WHERE numCaseId = @numUpdateRecordID AND numDomainId = @numDomainID
		END
		
		IF @tintMode = 44
		BEGIN
			UPDATE dbo.UserMaster SET numDefaultClass = @numUpdateValueID WHERE numUserId = @numUpdateRecordID AND numDomainId = @numDomainID
		END

		IF @tintMode = 46
		BEGIN
			UPDATE dbo.Communication SET bitClosedFlag = @numUpdateValueID WHERE numCommId = @numUpdateRecordID AND numDomainId = @numDomainID
		END

		IF @tintMode = 47
		BEGIN
			UPDATE dbo.Domain SET bitCloneLocationWithItem = @numUpdateValueID WHERE numDomainId = @numDomainID
		END

		IF @tintMode = 48
		BEGIN
			UPDATE dbo.Domain SET vcMarketplaces = @vcText WHERE numDomainId = @numDomainID
		END

		IF @tintMode = 49
		BEGIN
			UPDATE dbo.OpportunityMaster SET intUsedShippingCompany=@numUpdateValueID,numShippingService=-1 WHERE numDomainId = @numDomainID AND numOppId=@numUpdateRecordID
		END

		IF @tintMode = 50
		BEGIN
			UPDATE dbo.StagePercentageDetailsTask SET vcTaskName=@vcText WHERE numDomainId = @numDomainID AND numTaskId=@numUpdateRecordID
		END

		IF @tintMode = 51
		BEGIN
			UPDATE dbo.Domain SET bitReceiveOrderWithNonMappedItem=CAST(@numUpdateValueID AS BIT) WHERE numDomainId = @numDomainID
		END

		IF @tintMode = 52
		BEGIN
			UPDATE dbo.Domain SET numItemToUseForNonMappedItem=@numUpdateValueID WHERE numDomainId = @numDomainID
		END
		
    SELECT @@ROWCOUNT
END
--Exec  USP_UpdateSingleFieldValue 26,52205,0,'',156
--UPDATE dbo.OpportunityMaster SET numStatus = 0 where numOppId = 52185
--SELECT numStatus FROM dbo.OpportunityMaster where numOppId = 52203
--SELECT * FROM dbo.OpportunityMaster 
--SELECT * FROM dbo.OpportunityBizDocs where numOppId = 52203
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_GetTasks')
DROP PROCEDURE dbo.USP_WorkOrder_GetTasks
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_GetTasks]
(
	@numDomainID NUMERIC(18,0)
	,@numWOID NUMERIC(18,0)
	,@numStageDetailsId NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@tintMode TINYINT
)
AS 
BEGIN
	IF @tintMode = 1
	BEGIN
		DECLARE @numQtyToBuild FLOAT
		DECLARE @dtPlannedStartDate DATETIME
		SELECT 
			@numQtyToBuild=numQtyItemsReq
			,@dtPlannedStartDate=ISNULL(WorkOrder.dtmStartDate,WorkOrder.bintCreatedDate) 
		FROM 
			WorkOrder 
		WHERE
			WorkOrder.numDomainID=@numDomainID 
			AND WorkOrder.numWOId=@numWOID

		DECLARE @TempTaskAssignee TABLE
		(
			numAssignedTo NUMERIC(18,0)
			,dtLastTaskCompletionTime DATETIME
		)

		DECLARE @TempTasks TABLE
		(
			ID INT IDENTITY(1,1)
			,numTaskID NUMERIC(18,0)
			,numTaskTimeInMinutes NUMERIC(18,0)
			,numTaskAssignee NUMERIC(18,0)
			,intTaskType INT --1:Parallel, 2:Sequential
			,dtPlannedStartDate DATETIME
		)

		INSERT INTO @TempTasks
		(
			numTaskID
			,numTaskTimeInMinutes
			,intTaskType
			,numTaskAssignee
		)
		SELECT 
			SPDT.numTaskID
			,((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * @numQtyToBuild
			,ISNULL(SPDT.intTaskType,1)
			,SPDT.numAssignTo
		FROM 
			StagePercentageDetailsTask SPDT
		INNER JOIN
			WorkOrder WO
		ON
			SPDT.numWorkOrderId = WO.numWOId
		WHERE 
			SPDT.numDomainId=@numDomainID
			AND SPDT.numWorkOrderId = @numWOID

		UPDATE @TempTasks SET dtPlannedStartDate=@dtPlannedStartDate

		INSERT INTO @TempTaskAssignee
		(
			numAssignedTo
		)
		SELECT DISTINCT
			numTaskAssignee
		FROM
			@TempTasks

		DECLARE @i INT = 1
		DECLARE @iCount INT 	
		SELECT @iCount = COUNT(*) FROM @TempTasks

		DECLARE @numTaskID NUMERIC(18,0)
		DECLARE @numTaskAssignee NUMERIC(18,0)
		DECLARE @intTaskType INT
		DECLARE @numWorkScheduleID NUMERIC(18,0)
		DECLARE @numTempUserCntID NUMERIC(18,0)
		DECLARE @dtStartDate DATETIME
		DECLARE @numTotalTaskInMinutes NUMERIC(18,0)
		DECLARE @numProductiveTimeInMinutes NUMERIC(18,0)
		DECLARE @tmStartOfDay TIME(7)
		DECLARE @numTimeLeftForDay NUMERIC(18,0)
		DECLARE @vcWorkDays VARCHAR(20)
		DECLARE @bitParallelStartSet BIT = 0

		WHILE @i <= @iCount
		BEGIN
			SELECT
				@numTaskID=numTaskID
				,@numTaskAssignee=numTaskAssignee
				,@numTotalTaskInMinutes=numTaskTimeInMinutes
				,@intTaskType = intTaskType
			FROM
				@TempTasks 
			WHERE
				ID=@i

			IF NOT EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=@numTaskID AND tintAction=4)
			BEGIN
				-- GET TASK COMPLETION TIME WHICH WILL BE START DATETIME FOR NEXT TASK
				SELECT
					@numWorkScheduleID = WS.ID
					,@numProductiveTimeInMinutes=(ISNULL(WS.numProductiveHours,0) * 60) + ISNULL(WS.numProductiveMinutes,0)
					,@tmStartOfDay = tmStartOfDay
					,@vcWorkDays=CONCAT(',',vcWorkDays,',')
					,@dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtPlannedStartDate),112)) + tmStartOfDay)
				FROM
					WorkSchedule WS
				INNER JOIN
					UserMaster
				ON
					WS.numUserCntID = UserMaster.numUserDetailId
				WHERE 
					WS.numUserCntID = @numTaskAssignee

				IF @intTaskType=1 AND EXISTS (SELECT numAssignedTo FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
				BEGIN
					SELECT @dtStartDate = dtLastTaskCompletionTime FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee
				END
				ELSE IF EXISTS (SELECT numAssignedTo FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
				BEGIN
					SELECT @dtStartDate = dtLastTaskCompletionTime FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee
				END

				UPDATE @TempTasks SET dtPlannedStartDate=@dtStartDate WHERE ID=@i 

				IF @numProductiveTimeInMinutes > 0 AND @numTotalTaskInMinutes > 0
				BEGIN
					WHILE @numTotalTaskInMinutes > 0
					BEGIN
						-- IF ITS WORKING DAY AND ALSO NOT HOLIDAY THEN PROCEED OR INCRESE DATE TO NEXT DAY
						IF CHARINDEX(CONCAT(',',DATEPART(WEEKDAY,@dtStartDate),','),@vcWorkDays) > 0 AND NOT EXISTS (SELECT ID FROM WorkScheduleDaysOff WHERE numWorkScheduleID=@numWorkScheduleID AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,@dtStartDate) AS DATE) BETWEEN dtDayOffFrom AND dtDayOffTo)
						BEGIN
							IF CAST(@dtStartDate AS DATE) = CAST(GETUTCDATE() AS DATE)
							BEGIN
								-- CHECK TIME LEFT FOR DAY BASED
								SET @numTimeLeftForDay = DATEDIFF(MINUTE,@dtStartDate,DATEADD(MINUTE,@numProductiveTimeInMinutes,DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtStartDate),112)) + @tmStartOfDay)))
							END
							ELSE
							BEGIN
								SET @numTimeLeftForDay = @numProductiveTimeInMinutes
							END

							IF @numTimeLeftForDay > 0
							BEGIN
								IF @numTimeLeftForDay > @numTotalTaskInMinutes
								BEGIN
									SET @dtStartDate = DATEADD(MINUTE,@numTotalTaskInMinutes,@dtStartDate)
								END
								ELSE
								BEGIN
									SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)
								END

								SET @numTotalTaskInMinutes = @numTotalTaskInMinutes - @numTimeLeftForDay
							END
							ELSE
							BEGIN
								SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)
							END
						END
						ELSE
						BEGIN
							SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)					
						END				
					END
				END	

				UPDATE @TempTaskAssignee SET dtLastTaskCompletionTime=@dtStartDate WHERE numAssignedTo=@numTaskAssignee
			END

			SET @i = @i + 1
		END

		SELECT	
			SPDT.numTaskId
			,SPDT.numAssignTo
			,SPDT.numReferenceTaskId
			,SPDT.vcTaskName
			,dbo.fn_GetContactName(SPDT.numAssignTo) vcAssignedTo
			,'Work Station 1' AS vcWorkStation
			,CASE
				WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4) THEN ISNULL(WO.numQtyItemsReq,0)
				ELSE ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId),0)
			END AS numProcessedQty
			,(CASE
				WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4) THEN 0
				ELSE ISNULL(WO.numQtyItemsReq,0) - ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId),0)
			END) AS numRemainingQty
			,CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId ORDER BY dtActionTime DESC, ID DESC),0) 
				WHEN 4 THEN '<img src="../images/comflag.png" />'
				WHEN 3 THEN CONCAT('<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowWorkOrder(',SPDT.numTaskId,');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-success btn-task-start" onclick="return TaskStartedWorkOrder(this,',SPDT.numTaskId,',0);">Start</button></li><li style="vertical-align:middle;padding-right:0px;"><label class="taskTimerInitial"></label></li></ul>')
				WHEN 2 THEN CONCAT('<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowWorkOrder(',SPDT.numTaskId,');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat bg-purple" onclick="return TaskStartedWorkOrder(this,',SPDT.numTaskId,',1);">Resume</button></li></ul>')
				WHEN 1 THEN CONCAT('<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowWorkOrder(',SPDT.numTaskId,');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-warning" onclick="return ShowTaskPausedWindowWorkOrder(this,',SPDT.numTaskId,');">Pause</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-flat btn-task-finish" onclick="return TaskFinishedWorkOrder(this,',SPDT.numTaskId,',4);"><img src="../images/comflag.png" />&nbsp;Finish</button></li><li style="vertical-align:middle;padding-right:0px;"><input type="text" onkeydown="return ProcessedUnitsChangedWorkOrder(this,',SPDT.numTaskId,',event);" class="form-control txtUnitsProcessed" style="width:50px;padding:1px 2px 1px 2px;height:23px;" /></li><li style="vertical-align:middle;padding-right:0px;"><label class="taskTimerInitial"></label></li></ul>')
				ELSE CONCAT('<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowWorkOrder(',SPDT.numTaskId,');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-success btn-task-start" onclick="return TaskStartedWorkOrder(this,',SPDT.numTaskId,',0);">Start</button></li></ul>')
			END  AS vcTaskControls
			,FORMAT(FLOOR((((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * @numQtyToBuild) / 60),'00') + ':' + FORMAT((((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * CAST(@numQtyToBuild AS DECIMAL)) % 60,'00')  vcEstimatedTaskTime
			,(((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * @numQtyToBuild) numTaskEstimationInMinutes
			,(CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=SPDT.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC),0) 
				WHEN 4 THEN 0
				WHEN 3 THEN dbo.GetTimeSpendOnTask(SPDT.numDomainID,SPDT.numTaskId,2)
				WHEN 2 THEN dbo.GetTimeSpendOnTask(SPDT.numDomainID,SPDT.numTaskId,2)
				WHEN 1 THEN 0
			END) numTimeSpentInMinutes
			,(CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId ORDER BY dtActionTime DESC,ID DESC),0) 
				WHEN 4 THEN NULL
				WHEN 3 THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT TOP 1 dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=SPDT.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC))
				WHEN 2 THEN NULL
				WHEN 1 THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT TOP 1 dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=SPDT.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC))
			END) dtLastStartDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN dbo.GetTimeSpendOnTaskByProject(SPDT.numDomainID,SPDT.numTaskId,1)
				ELSE ''
			END vcActualTaskTimeHtml
			,(CASE WHEN dbo.GetTimeSpendOnTask(SPDT.numDomainID,SPDT.numTaskId,0) = 'Invalid time sequence' THEN '00:00' ELSE dbo.GetTimeSpendOnTask(SPDT.numDomainID,SPDT.numTaskId,0) END) vcActualTaskTime
			,CASE 
				WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1)
				THEN CONCAT('<span>',dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1)),@numDomainID),'</span>')
				ELSE CONCAT('<i style="color:#a6a6a6">',dbo.FormatedDateTimeFromDate((CASE
										WHEN TT.dtPlannedStartDate IS NULL
										THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,ISNULL(WO.dtmStartDate,WO.bintCreatedDate))
										ELSE DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,TT.dtPlannedStartDate)
									END),@numDomainID),' (planned)</i>')
			END dtPlannedStartDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1))
				ELSE ''
			END AS dtStartDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4))
				ELSE ''
			END AS dtFinishDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)),@numDomainID)
				ELSE ''
			END AS vcFinishDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN 1
				ELSE 0
			END AS bitTaskCompleted
		FROM 
			StagePercentageDetailsTask SPDT
		INNER JOIN
			WorkOrder WO
		ON
			SPDT.numWorkOrderId = WO.numWOId
		LEFT JOIN
			@TempTasks TT
		ON
			SPDT.numTaskId = TT.numTaskID
		WHERE
			SPDT.numStageDetailsId = @numStageDetailsId
			AND SPDT.numWorkOrderId = @numWOID
	END
	ELSE IF @tintMode = 2
	BEGIN
		SELECT	
			SPDT.numTaskId
			,SPDT.numReferenceTaskId
			,SPDT.numStageDetailsId
			,SPDT.vcTaskName
			,ISNULL(SPDT.numHours,0) numHours
			,ISNULL(SPDT.numMinutes,0) numMinutes
			,SPDT.numAssignTo
			,ADC.numTeam
			,CONCAT(FORMAT(ISNULL(SPDT.numHours,0),'00'),':',FORMAT(ISNULL(SPDT.numMinutes,0),'00')) vcEstimatedTaskTime
			,(CASE WHEN dbo.GetTimeSpendOnTask(SPDT.numDomainID,SPDT.numTaskId,0) = 'Invalid time sequence' THEN '00:00' ELSE dbo.GetTimeSpendOnTask(SPDT.numDomainID,SPDT.numTaskId,0) END) vcActualTaskTime
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1))
				ELSE ''
			END AS dtStartDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4))
				ELSE ''
			END AS dtFinishDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId)
				THEN 1
				ELSE 0
			END bitTaskStarted
		FROM 
			StagePercentageDetailsTask SPDT
		INNER JOIN
			WorkOrder WO
		ON
			SPDT.numWorkOrderId = WO.numWOId
		INNER JOIN
			AdditionalContactsInformation ADC
		ON
			SPDT.numAssignTo = ADC.numContactId
		LEFT JOIN
			@TempTasks TT
		ON
			SPDT.numTaskId = TT.numTaskID
		WHERE
			SPDT.numStageDetailsId = @numStageDetailsId
			AND SPDT.numWorkOrderId = @numWOID
	END
END
GO
GO
SET ANSI_NULLS OFF
GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_SecondsConversion')
DROP FUNCTION fn_SecondsConversion
GO
CREATE FUNCTION [dbo].[fn_SecondsConversion]  
(  
	@TimeinSec INT 
)  
RETURNS VARCHAR(MAX)  
BEGIN  
DECLARE @op VARCHAR(MAX)  
SET @op = ''  
SELECT @op=RIGHT('0' + CAST(@TimeinSec / 3600 AS VARCHAR),2) + ':' +  
RIGHT('0' + CAST((@TimeinSec / 60) % 60 AS VARCHAR),2)
--+ ':' +  
--RIGHT('0' + CAST(@TimeinSec % 60 AS VARCHAR),2) 
RETURN @op  
End 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCompanyDetailsFrom850')
DROP PROCEDURE dbo.USP_GetCompanyDetailsFrom850
GO

CREATE PROCEDURE [dbo].[USP_GetCompanyDetailsFrom850]
	@numDivisionId NUMERIC(18,0)
	,@vcMarketplace VARCHAR(300)
AS 
BEGIN
	--1: SKU
	--2: UPC
	--3: ItemName
	--4: BizItemID
	--5: ASIN
	--6: CustomerPart
	--7: VendorPart

	DECLARE @numMarketplaceID NUMERIC(18,0) = 0

	IF EXISTS (SELECT ID FROM eChannelHub WHERE vcMarketplace=ISNULL(@vcMarketplace,''))
	BEGIN
		SELECT 
			@numMarketplaceID=ID
		FROM 
			eChannelHub 
		WHERE 
			vcMarketplace=ISNULL(@vcMarketplace,'')
	END

	SELECT 
		ISNULL(DM.numAssignedTo,ACI.numContactId) AS numContactID
		,DM.numDivisionID
		,(CASE WHEN DM.numDomainID=209 THEN 1 ELSE ISNULL(DMEmployer.tintInbound850PickItem,0) END) AS tintInbound850PickItem
		,CI.numCompanyId
		,CI.vcCompanyName
		,@numMarketplaceID numMarketplaceID
		,ISNULL(bitReceiveOrderWithNonMappedItem,0) bitReceiveOrderWithNonMappedItem
		,ISNULL(numItemToUseForNonMappedItem,0) numItemToUseForNonMappedItem
	FROM
		DivisionMaster DM
	INNER JOIN 
		CompanyInfo CI
	ON 
		DM.numCompanyID = CI.numCompanyId
	INNER JOIN
		AdditionalContactsInformation ACI 
	ON
		DM.numDivisionID = ACI.numDivisionId
	INNER JOIN 
		Domain D
	ON
		DM.numDomainID = D.numDomainID
	INNER JOIN
		DivisionMaster DMEmployer
	ON
		D.numDivisionID = DMEmployer.numDivisionID
	WHERE
		DM.numDivisionID = @numDivisionId
		AND ISNULL(ACI.bitPrimaryContact,0)=1
END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemDetailsFor850')
DROP PROCEDURE dbo.USP_GetItemDetailsFor850
GO

CREATE PROCEDURE [dbo].[USP_GetItemDetailsFor850]
	@Inbound850PickItem INT
	,@vcItemIdentification VARCHAR(100)
	,@numDivisionId NUMERIC(18,0)
	,@numCompanyId NUMERIC(18,0)
	,@numDomainId NUMERIC(18,0)
	,@numOrderSource NUMERIC(18,0)
AS 
BEGIN

	If EXISTS (SELECT ID FROM ItemMarketplaceMapping WHERE numDomainID=@numDomainId AND numMarketplaceID=@numOrderSource AND ISNULL(vcMarketplaceUniqueID,'')=ISNULL(@vcItemIdentification,''))
	BEGIN
		SELECT
			Item.* 
		FROM 
			ItemMarketplaceMapping 
		INNER JOIN
			Item
		ON
			ItemMarketplaceMapping.numItemCode = Item.numItemCode
		WHERE 
			ItemMarketplaceMapping.numDomainID=@numDomainId 
			AND ItemMarketplaceMapping.numMarketplaceID=@numOrderSource 
			AND ISNULL(ItemMarketplaceMapping.vcMarketplaceUniqueID,'')=ISNULL(@vcItemIdentification,'')
	END
	ELSE
	BEGIN
		IF @Inbound850PickItem = 1 --SKU
		BEGIN
			SELECT * FROM ITEM WHERE numDomainID = @numDomainId AND vcSKU = @vcItemIdentification
		END
		ELSE IF @Inbound850PickItem = 2  -- UPC
		BEGIN
			SELECT * FROM ITEM WHERE numDomainID = @numDomainId AND numBarCodeId = @vcItemIdentification
		END
		ELSE IF @Inbound850PickItem = 3  -- ItemName
		BEGIN
			SELECT * FROM ITEM WHERE numDomainID = @numDomainId AND vcItemName = @vcItemIdentification
		END
		ELSE IF @Inbound850PickItem = 4   -- BizItemID
		BEGIN
			SELECT * FROM ITEM WHERE numDomainID = @numDomainId AND numItemCode = @vcItemIdentification
		END
		ELSE IF @Inbound850PickItem = 5   -- ASIN
		BEGIN
			SELECT * FROM ITEM WHERE numDomainID = @numDomainId AND vcASIN = @vcItemIdentification
		END
		ELSE IF @Inbound850PickItem = 6  -- CustomerPart#
		BEGIN
			SELECT * FROM ITEM I
			INNER JOIN CustomerPartNumber CPN ON I.numItemCode = CPN.numItemCode
			WHERE CPN.numDomainId = @numDomainId AND CPN.numCompanyId = @numCompanyId AND CPN.CustomerPartNo = @vcItemIdentification
		END
		ELSE IF @Inbound850PickItem = 7   -- VendorPart#
		BEGIN
			SELECT * FROM ITEM I
			INNER JOIN Vendor V ON I.numItemCode = V.numItemCode
			WHERE V.numDomainID = @numDomainId AND V.numVendorID = @numDivisionId AND V.vcPartNo = @vcItemIdentification
		END
	END
END

GO


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Project_GetTasks')
DROP PROCEDURE USP_Project_GetTasks
GO
CREATE PROCEDURE [dbo].[USP_Project_GetTasks]
(
	@numDomainID NUMERIC(18,0)
	,@numProId NUMERIC(18,0)
	,@numStageDetailsId NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@tintMode TINYINT
)
AS 
BEGIN
	IF @tintMode = 1
	BEGIN
		DECLARE @numQtyToBuild FLOAT
		DECLARE @dtPlannedStartDate DATETIME
		SELECT 
			@numQtyToBuild=1
			,@dtPlannedStartDate=ISNULL(ProjectsMaster.dtmStartDate,ProjectsMaster.bintCreatedDate) 
		FROM 
			ProjectsMaster 
		WHERE
			ProjectsMaster.numDomainID=@numDomainID 
			AND ProjectsMaster.numProId=@numProId

		DECLARE @TempTaskAssignee TABLE
		(
			numAssignedTo NUMERIC(18,0)
			,dtLastTaskCompletionTime DATETIME
		)

		DECLARE @TempTasks TABLE
		(
			ID INT IDENTITY(1,1)
			,numTaskID NUMERIC(18,0)
			,numTaskTimeInMinutes NUMERIC(18,0)
			,numTaskAssignee NUMERIC(18,0)
			,intTaskType INT --1:Parallel, 2:Sequential
			,dtPlannedStartDate DATETIME
		)

		INSERT INTO @TempTasks
		(
			numTaskID
			,numTaskTimeInMinutes
			,intTaskType
			,numTaskAssignee
		)
		SELECT 
			SPDT.numTaskID
			,((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * @numQtyToBuild
			,ISNULL(SPDT.intTaskType,1)
			,SPDT.numAssignTo
		FROM 
			StagePercentageDetailsTask SPDT
		INNER JOIN
			ProjectsMaster PO
		ON
			SPDT.numProjectId = PO.numProId
		WHERE 
			SPDT.numDomainId=@numDomainID
			AND SPDT.numProjectId = @numProId

		UPDATE @TempTasks SET dtPlannedStartDate=@dtPlannedStartDate

		INSERT INTO @TempTaskAssignee
		(
			numAssignedTo
		)
		SELECT DISTINCT
			numTaskAssignee
		FROM
			@TempTasks

		DECLARE @i INT = 1
		DECLARE @iCount INT 	
		SELECT @iCount = COUNT(*) FROM @TempTasks

		DECLARE @numTaskID NUMERIC(18,0)
		DECLARE @numTaskAssignee NUMERIC(18,0)
		DECLARE @intTaskType INT
		DECLARE @numWorkScheduleID NUMERIC(18,0)
		DECLARE @numTempUserCntID NUMERIC(18,0)
		DECLARE @dtStartDate DATETIME
		DECLARE @numTotalTaskInMinutes NUMERIC(18,0)
		DECLARE @numProductiveTimeInMinutes NUMERIC(18,0)
		DECLARE @tmStartOfDay TIME(7)
		DECLARE @numTimeLeftForDay NUMERIC(18,0)
		DECLARE @vcWorkDays VARCHAR(20)
		DECLARE @bitParallelStartSet BIT = 0

		WHILE @i <= @iCount
		BEGIN
			SELECT
				@numTaskID=numTaskID
				,@numTaskAssignee=numTaskAssignee
				,@numTotalTaskInMinutes=numTaskTimeInMinutes
				,@intTaskType = intTaskType
			FROM
				@TempTasks 
			WHERE
				ID=@i

			IF NOT EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=@numTaskID AND tintAction=4)
			BEGIN
				-- GET TASK COMPLETION TIME WHICH WILL BE START DATETIME FOR NEXT TASK
				SELECT
					@numWorkScheduleID = WS.ID
					,@numProductiveTimeInMinutes=(ISNULL(WS.numProductiveHours,0) * 60) + ISNULL(WS.numProductiveMinutes,0)
					,@tmStartOfDay = tmStartOfDay
					,@vcWorkDays=vcWorkDays
					,@dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtPlannedStartDate),112)) + tmStartOfDay)
				FROM
					WorkSchedule WS
				INNER JOIN
					UserMaster
				ON
					WS.numUserCntID = UserMaster.numUserDetailId
				WHERE 
					WS.numUserCntID = @numTaskAssignee

				IF @intTaskType=1 AND EXISTS (SELECT numAssignedTo FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
				BEGIN
					SELECT @dtStartDate = dtLastTaskCompletionTime FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee
				END
				ELSE IF EXISTS (SELECT numAssignedTo FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
				BEGIN
					SELECT @dtStartDate = dtLastTaskCompletionTime FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee
				END

				UPDATE @TempTasks SET dtPlannedStartDate=@dtStartDate WHERE ID=@i 

				IF @numProductiveTimeInMinutes > 0 AND @numTotalTaskInMinutes > 0
				BEGIN
					WHILE @numTotalTaskInMinutes > 0
					BEGIN
						-- IF ITS WORKING DAY AND ALSO NOT HOLIDAY THEN PROCEED OR INCRESE DATE TO NEXT DAY
						IF DATEPART(WEEKDAY,@dtStartDate) IN (SELECT Id FROM dbo.SplitIDs(@vcWorkDays,',')) AND NOT EXISTS (SELECT ID FROM WorkScheduleDaysOff WHERE numWorkScheduleID=@numWorkScheduleID AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,@dtStartDate) AS DATE) BETWEEN dtDayOffFrom AND dtDayOffTo)
						BEGIN
							IF CAST(@dtStartDate AS DATE) = CAST(GETUTCDATE() AS DATE)
							BEGIN
								-- CHECK TIME LEFT FOR DAY BASED
								SET @numTimeLeftForDay = DATEDIFF(MINUTE,@dtStartDate,DATEADD(MINUTE,@numProductiveTimeInMinutes,DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtStartDate),112)) + @tmStartOfDay)))
							END
							ELSE
							BEGIN
								SET @numTimeLeftForDay = @numProductiveTimeInMinutes
							END

							IF @numTimeLeftForDay > 0
							BEGIN
								IF @numTimeLeftForDay > @numTotalTaskInMinutes
								BEGIN
									SET @dtStartDate = DATEADD(MINUTE,@numTotalTaskInMinutes,@dtStartDate)
								END
								ELSE
								BEGIN
									SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)
								END

								SET @numTotalTaskInMinutes = @numTotalTaskInMinutes - @numTimeLeftForDay
							END
							ELSE
							BEGIN
								SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)
							END
						END
						ELSE
						BEGIN
							SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)					
						END				
					END
				END	

				UPDATE @TempTaskAssignee SET dtLastTaskCompletionTime=@dtStartDate WHERE numAssignedTo=@numTaskAssignee
			END

			SET @i = @i + 1
		END

		SELECT	
			SPDT.numTaskId
			,SPDT.numReferenceTaskId
			,dbo.fn_GetContactName(ACustomer.numContactId) vcCustomerName
			,SPDT.vcTaskName
			,ISNULL(ACustomer.vcEmail,'') AS vcEmail
			,SPDT.numAssignTo AS numAssignedTo 
			,ISNULL(SPDT.bitTimeAddedToContract,0) AS bitTimeAddedToContract
			,dbo.fn_GetContactName(SPDT.numAssignTo) vcAssignedTo
			,ISNULL(L.vcData,'-') AS vcWorkStation
			,ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId),0) AS numProcessedQty
			,1 - ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId),0) AS numRemainingQty
			,CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId ORDER BY dtActionTime DESC,ID DESC),0) 
				WHEN 4 THEN '<img src="../images/comflag.png" />'
				WHEN 3 THEN CONCAT('<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowProject(',SPDT.numTaskId,');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-warning" onclick="return ShowTaskPausedWindowProject(this,',SPDT.numTaskId,');">Pause</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-flat btn-task-finish" onclick="return TaskFinishedProject(this,',SPDT.numTaskId,',4);"><img src="../images/comflag.png" />&nbsp;Finish</button></li><li style="vertical-align:middle;padding-right:0px;"><label class="taskTimerInitial"></label></li></ul>')
				WHEN 2 THEN CONCAT('<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowProject(',SPDT.numTaskId,');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat bg-purple" onclick="return TaskStartedProject(this,',SPDT.numTaskId,',1);">Resume</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-flat btn-task-finish" onclick="return TaskFinishedProject(this,',SPDT.numTaskId,',4);"><img src="../images/comflag.png" />&nbsp;Finish</button></li></ul>')
				WHEN 1 THEN CONCAT('<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowProject(',SPDT.numTaskId,');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-warning" onclick="return ShowTaskPausedWindowProject(this,',SPDT.numTaskId,');">Pause</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-flat btn-task-finish" onclick="return TaskFinishedProject(this,',SPDT.numTaskId,',4);"><img src="../images/comflag.png" />&nbsp;Finish</button></li><li style="vertical-align:middle;padding-right:0px;"><label class="taskTimerInitial"></label></li></ul>')
				ELSE CONCAT('<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowProject(',SPDT.numTaskId,');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-success btn-task-start" onclick="return TaskStartedProject(this,',SPDT.numTaskId,',0);">Start</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-flat btn-task-finish" onclick="return TaskFinishedProject(this,',SPDT.numTaskId,',4);"><img src="../images/comflag.png" />&nbsp;Finish</button></li></ul>')
			END  AS vcTaskControls
			,FORMAT((((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * @numQtyToBuild) / 60,'00') + ':' + FORMAT((((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * CAST(@numQtyToBuild AS DECIMAL)) % 60.0,'00')  vcEstimatedTaskTime
			,(((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * @numQtyToBuild) numTaskEstimationInMinutes
			,(CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=SPDT.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC),0) 
				WHEN 4 THEN 0
				WHEN 3 THEN dbo.GetTimeSpendOnTaskByProject(SPDT.numDomainID,SPDT.numTaskId,2)
				WHEN 2 THEN dbo.GetTimeSpendOnTaskByProject(SPDT.numDomainID,SPDT.numTaskId,2)
				WHEN 1 THEN 0
			END) numTimeSpentInMinutes
			,(CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId ORDER BY dtActionTime DESC,ID DESC),0) 
				WHEN 4 THEN NULL
				WHEN 3 THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT TOP 1 dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=SPDT.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC))
				WHEN 2 THEN NULL
				WHEN 1 THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT TOP 1 dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=SPDT.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC))
			END) dtLastStartDate
			,(CASE WHEN dbo.GetTimeSpendOnTaskByProject(SPDT.numDomainID,SPDT.numTaskId,0) = 'Invalid time sequence' THEN '00:00' ELSE dbo.GetTimeSpendOnTaskByProject(SPDT.numDomainID,SPDT.numTaskId,0) END) vcActualTaskTime
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN dbo.GetTimeSpendOnTaskByProject(SPDT.numDomainID,SPDT.numTaskId,1)
				ELSE ''
			END vcActualTaskTimeHtml
			,CASE 
				WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1)
				THEN CONCAT('<span>',dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1)),@numDomainID),'</span>')
				ELSE CONCAT('<i style="color:#a6a6a6">',dbo.FormatedDateTimeFromDate((CASE
										WHEN TT.dtPlannedStartDate IS NULL
										THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,ISNULL(PO.dtmStartDate,PO.bintCreatedDate))
										ELSE DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,TT.dtPlannedStartDate)
									END),@numDomainID),' (planned)</i>')
			END dtPlannedStartDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1))
				ELSE ''
			END AS dtStartDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4))
				ELSE ''
			END AS dtFinishDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)),@numDomainID)
				ELSE ''
			END AS vcFinishDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN 1
				ELSE 0
			END AS bitTaskCompleted
			,(SELECT COUNT(*) FROM TopicMaster WHERE intRecordType=4 AND numRecordId=SPDT.numTaskId) AS TopicCount
			,(CASE 
				WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskNotes SPDTN WHERE SPDTN.numTaskID=SPDT.numTaskId) 
				THEN CONCAT('<i class="fa fa-file-text-o" style="font-size: 23px;" onclick="return OpenTaskNotes(',SPDT.numTaskId,')"></i>') 
				ELSE CONCAT('<i class="fa fa-file-o" style="font-size: 23px;" onclick="return OpenTaskNotes(',SPDT.numTaskId,')"></i>') 
			END) vcNotesLink
		FROM 
			StagePercentageDetailsTask SPDT
		INNER JOIN
			ProjectsMaster PO
		ON
			SPDT.numProjectId = PO.numProId
		LEFT JOIN
			@TempTasks TT
		ON
			SPDT.numTaskId = TT.numTaskID
		LEFT JOIN
			AdditionalContactsInformation AS A
		ON
			SPDT.numAssignTo=A.numContactId
		LEFT JOIN 
			AdditionalContactsInformation ACustomer
		ON 
			PO.numCustPrjMgr = ACustomer.numContactId
		LEFT JOIN
			ListDetails AS L
		ON
			A.numTeam=L.numListItemID
		WHERE
			SPDT.numStageDetailsId = @numStageDetailsId
			AND SPDT.numProjectId = @numProId
	END
	ELSE IF @tintMode = 2
	BEGIN
		SELECT	
			SPDT.numTaskId
			,SPDT.numReferenceTaskId
			,SPDT.numStageDetailsId
			,SPDT.vcTaskName
			,ISNULL(SPDT.numHours,0) numHours
			,ISNULL(SPDT.numMinutes,0) numMinutes
			,SPDT.numAssignTo
			,SPDT.numAssignTo AS numAssignedTo 
			,ISNULL(SPDT.bitTimeAddedToContract,0) AS bitTimeAddedToContract
			,ADC.numTeam
			,CONCAT(FORMAT(ISNULL(SPDT.numHours,0),'00'),':',FORMAT(ISNULL(SPDT.numMinutes,0),'00')) vcEstimatedTaskTime
			,(CASE WHEN dbo.GetTimeSpendOnTaskByProject(SPDT.numDomainID,SPDT.numTaskId,0) = 'Invalid time sequence' THEN '00:00' ELSE dbo.GetTimeSpendOnTaskByProject(SPDT.numDomainID,SPDT.numTaskId,0) END) vcActualTaskTime
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1))
				ELSE ''
			END AS dtStartDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4))
				ELSE ''
			END AS dtFinishDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId)
				THEN 1
				ELSE 0
			END bitTaskStarted,
			(SELECT COUNT(*) FROM TopicMaster WHERE intRecordType=4 AND numRecordId=SPDT.numTaskId) AS TopicCount
		FROM 
			StagePercentageDetailsTask SPDT
		INNER JOIN
			ProjectsMaster PO
		ON
			SPDT.numProjectId = PO.numProId
		LEFT JOIN
			AdditionalContactsInformation ADC
		ON
			SPDT.numAssignTo = ADC.numContactId
		LEFT JOIN
			@TempTasks TT
		ON
			SPDT.numTaskId = TT.numTaskID
		WHERE
			SPDT.numStageDetailsId = @numStageDetailsId
			AND SPDT.numProjectId = @numProId
	END
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TicklerActItemsV2')
DROP PROCEDURE USP_TicklerActItemsV2
GO
CREATE Proc [dbo].[USP_TicklerActItemsV2]       
@numUserCntID as numeric(9)=null,                                                                        
@numDomainID as numeric(9)=null,                                                                        
@startDate as datetime,                                                                        
@endDate as datetime,                                                                    
@ClientTimeZoneOffset Int,  --Added by Debasish to enable calculation of date according to client machine             
@bitFilterRecord bit=0,
@columnName varchar(50), 
@columnSortOrder varchar(50),
@vcBProcessValue varchar(500),
@RegularSearchCriteria VARCHAR(MAX)='',
@CustomSearchCriteria VARCHAR(MAX)='', 
@OppStatus int,
@CurrentPage int,                                                              
@PageSize int
AS
BEGIN
	SET @endDate = DATEADD(MILLISECOND,-2,DATEADD(DAY,1,@endDate))

--SET @PageSize=10
DECLARE @tintActionItemsViewRights TINYINT  = 3
DECLARE @bitREQPOApproval AS BIT
DECLARE @bitARInvoiceDue AS BIT
DECLARE @bitAPBillsDue AS BIT
DECLARE @vchARInvoiceDue AS VARCHAR(500)
DECLARE @vchAPBillsDue AS VARCHAR(500)
DECLARE @decReqPOMinValue AS DECIMAL(18,2)
DECLARE @vchREQPOApprovalEmp AS VARCHAR(500)=''
DECLARE @numDomainDivisionID NUMERIC(18,0)
DECLARE @dynamicPartQuery AS NVARCHAR(MAX)=''
DECLARE @ActivityRegularSearch AS VARCHAR(MAX)=''
DECLARE @OtherRegularSearch AS VARCHAR(MAX)=''
DECLARE @IndivRecordPaging AS NVARCHAR(MAX)=''
--SET @IndivRecordPaging=' '
DECLARE @TotalRecordCount INT =0
SET @IndivRecordPaging=  ' ORDER BY dtDueDate DESC OFFSET '+CAST(((@CurrentPage - 1 ) * @PageSize) AS VARCHAR) +' ROWS FETCH NEXT '+CAST(@PageSize AS VARCHAR)+' ROWS ONLY'

SELECT TOP 1 
	@bitREQPOApproval=bitREQPOApproval,
	@vchREQPOApprovalEmp=vchREQPOApprovalEmp,
	@decReqPOMinValue=ISNULL(decReqPOMinValue,0),
	@bitARInvoiceDue=bitARInvoiceDue,
	@bitAPBillsDue = bitAPBillsDue,
	@vchARInvoiceDue = vchARInvoiceDue,
	@vchAPBillsDue = vchAPBillsDue,
	@numDomainDivisionID=numDivisionID
FROM 
	Domain 
WHERE 
	numDomainId=@numDomainID

IF EXISTS (SELECT * FROM GroupAuthorization WHERE numDomainID=@numDomainID AND numModuleID=1 AND numPageID=2 AND numGroupID IN (SELECT ISNULL(numGroupID,0) FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailId=@numUserCntID))
BEGIN
	SELECT @tintActionItemsViewRights=ISNULL(intViewAllowed,0) FROM GroupAuthorization WHERE numDomainID=@numDomainID AND numModuleID=1 AND numPageID=2 AND numGroupID IN (SELECT ISNULL(numGroupID,0) FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailId=@numUserCntID)
END
--12 - Communication task
--1 - Case Task
--2 - Project Task
--3 - Opportunity Task
--4 - Sales Order Task
--5 - Work Order Management
--6 - Work Center Task
--7 - Requisition Approval
--8 - PO Approval
--9 - Price Margin Approval
--10 - A/R Invoice Due
--11 - A/P Bill Due

DECLARE @dynamicQuery AS NVARCHAR(MAX)=''
DECLARE @dynamicQueryCount AS NVARCHAR(MAX)=''

Create table #tempRecords (RecordCount NUMERIC(9),RecordId NUMERIC(9),numTaskId numeric(9),IsTaskClosed bit,TaskTypeName VARCHAR(MAX),TaskType NUMERIC(9),
OrigDescription NVARCHAR(MAX),Description NVARCHAR(MAX),TotalProgress INT,numAssignToId NUMERIC(9),
Priority NVARCHAR(MAX),PriorityId  NUMERIC(9),ActivityId NUMERIC(9),OrgName NVARCHAR(MAX),CompanyRating NVARCHAR(MAX),
numContactID numeric(9),numDivisionID numeric(9),tintCRMType tinyint,
Id numeric(9),bitTask varchar(100),Startdate datetime,EndTime datetime,
itemDesc text,[Name] varchar(200),vcFirstName varchar(100),vcLastName varchar(100),numPhone varchar(15),numPhoneExtension varchar(7),
[Phone] varchar(30),vcCompanyName varchar(100),vcProfile VARCHAR(100), vcEmail varchar(50),Task varchar(100),Activity varchar(MAX),
Status varchar(100),numCreatedBy numeric(9),numRecOwner VARCHAR(500), numModifiedBy VARCHAR(500), numOrgTerId NUMERIC(18,0), numTerId VARCHAR(200),numAssignedTo varchar(1000),numAssignedBy varchar(50),
 caseid numeric(9),vcCasenumber varchar(50),casetimeId numeric(9),caseExpId numeric(9),type int,itemid varchar(50),
changekey varchar(50),dtDueDate DATETIME, DueDate varchar(500),bitFollowUpAnyTime bit,numNoOfEmployeesId varchar(50),vcComPhone varchar(50),numFollowUpStatus varchar(200),dtLastFollowUp varchar(500),vcLastSalesOrderDate VARCHAR(100),monDealAmount DECIMAL(20,5), vcPerformance VARCHAR(100),vcColorScheme VARCHAR(50),
Slp_Name varchar(100),intTotalProgress INT,vcPoppName varchar(100), numOppId NUMERIC(18,0),numBusinessProcessID numeric(18,0),
numCompanyRating varchar(500),numStatusID varchar(500),numCampaignID varchar(500),[Action-Item Participants] NVARCHAR(MAX),HtmlLink varchar(500),
Location nvarchar(150),OrignalDescription text,numTaskEstimationInMinutes INT,numTimeSpentInMinutes INT,dtLastStartDate DATETIME,numRemainingQty FLOAT)                                                         
 

declare @strSql as varchar(MAX)                                                            
declare @strSql1 as varchar(MAX)                         
declare @strSql2 as varchar(MAX)                                                            
declare @vcCustomColumnName AS VARCHAR(8000)
declare @vcnullCustomColumnName AS VARCHAR(8000)
   -------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

declare @tintOrder as tinyint                                                
declare @vcFieldName as varchar(50)                                                
declare @vcListItemType as varchar(3)                                           
declare @vcAssociatedControlType varchar(20)                                                
declare @numListID AS numeric(9)                                                
declare @vcDbColumnName varchar(20)                    
declare @WhereCondition varchar(2000)                     
declare @vcLookBackTableName varchar(2000)              
Declare @bitCustomField as BIT
DECLARE @bitAllowEdit AS CHAR(1)                 
Declare @numFieldId as numeric  
DECLARE @bitAllowSorting AS CHAR(1)   
declare @vcColumnName AS VARCHAR(500)       
declare @vcCustomColumnNameOnly AS VARCHAR(500)       


IF(@RegularSearchCriteria='')
BEGIN
	SET @RegularSearchCriteria =@RegularSearchCriteria + ' 1=1'
END
if(@OppStatus=0)
BEGIN
SET @RegularSearchCriteria =@RegularSearchCriteria + ' AND IsTaskClosed = 0'
END
if(@OppStatus=1)
BEGIN
SET @RegularSearchCriteria = @RegularSearchCriteria + ' AND IsTaskClosed = 1'
END
 SET @RegularSearchCriteria  = REPLACE(@RegularSearchCriteria,'TaskTypeName','TaskType')

 SET @ActivityRegularSearch = ''
 SET @OtherRegularSearch = ''

 SET @ActivityRegularSearch = ISNULL((SELECT STUFF( 
(
	SELECT  'AND ' + [Items] FROM 
	(
	  SELECT [Items] FROM SplitByString(@RegularSearchCriteria,'AND') WHERE Items<>'' AND 1=
	  (CASE WHEN Items LIKE '%IsTaskClosed%' THEN 0
	 WHEN Items LIKE '%TaskType%'  THEN 0
	 WHEN  Items LIKE '%numAssignToId%'  THEN 0
	 WHEN  Items LIKE '%PriorityId%'  THEN 0
	 WHEN  Items LIKE '%ActivityId%'  THEN 0
	 WHEN  Items LIKE '%Description%'   THEN 0
	 WHEN  Items LIKE '%OrgName%'   THEN 0 ELSE 1 END)
	) AS T FOR XML PATH('')
)
,1,3,'') AS [Items]),'')
SET @ActivityRegularSearch = REPLACE(@ActivityRegularSearch,'&gt;','>')
SET @ActivityRegularSearch = REPLACE(@ActivityRegularSearch,'&lt;','<')
 SET @OtherRegularSearch = ISNULL((SELECT STUFF( 
(
	SELECT  'AND ' + [Items] FROM 
	(
	  SELECT [Items] FROM SplitByString(@RegularSearchCriteria,'AND') WHERE Items<>'' AND 1=
	  (CASE WHEN Items LIKE '%IsTaskClosed%' THEN 1
	 WHEN Items LIKE '%TaskType%'  THEN 1
	 WHEN  Items LIKE '%numAssignToId%'  THEN 1
	 WHEN  Items LIKE '%PriorityId%'  THEN 1
	 WHEN  Items LIKE '%ActivityId%'  THEN 1
	 WHEN  Items LIKE '%Description%'   THEN 1
	 WHEN  Items LIKE '%OrgName%'   THEN 1 ELSE 0 END)
	) AS T FOR XML PATH('')
)
,1,3,'') AS [Items]),'')
SET @OtherRegularSearch = ISNULL(REPLACE(@OtherRegularSearch,'T.',''),'')
SET @OtherRegularSearch = REPLACE(@OtherRegularSearch,'&gt;','>')
SET @OtherRegularSearch = REPLACE(@OtherRegularSearch,'&lt;','<')
IF(LEN(@OtherRegularSearch)=0)
BEGIN
	SET @OtherRegularSearch = ' 1=1 '
END
Declare @ListRelID as numeric(9)             
set @tintOrder=0   

CREATE TABLE #tempForm 
	(
		tintOrder INT,vcDbColumnName nvarchar(50),vcFieldDataType nvarchar(50),vcOrigDbColumnName nvarchar(50),bitAllowFiltering BIT,vcFieldName nvarchar(50)
		,vcAssociatedControlType nvarchar(50),vcListItemType char(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC
		,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int
		,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth NUMERIC(18,2)
	)
                
	--Custom Fields related to Organization
	INSERT INTO 
		#tempForm
	SELECT  
		tintRow + 1 AS tintOrder,vcDbColumnName,'',vcDbColumnName,1,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'' as vcLookBackTableName                                                 
		,bitCustom,numFieldId,bitAllowSorting,0,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength
		,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
	FROM 
		View_DynamicCustomColumns
	WHERE 
		numFormId=43 
		AND numUserCntID=@numUserCntID 
		AND numDomainID=@numDomainID 
		AND tintPageType=1
		AND ISNULL(bitCustom,0)=1
 
	SELECT TOP 1 
		@tintOrder = tintOrder + 1
		,@vcDbColumnName=vcDbColumnName
		,@vcFieldName=vcFieldName
		,@vcAssociatedControlType=vcAssociatedControlType
		,@vcListItemType=vcListItemType
		,@numListID=numListID
		,@vcLookBackTableName=vcLookBackTableName                                               
		,@bitCustomField=bitCustomField
		,@numFieldId=numFieldId
		,@bitAllowSorting=bitAllowSorting
		,@bitAllowEdit=0
		,@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder ASC     
  
	SET @vcCustomColumnName=''
	SET @vcnullCustomColumnName=''
	SET @vcCustomColumnNameOnly = ''
	WHILE @tintOrder>0      
	BEGIN  
		SET @vcColumnName= @vcDbColumnName
		SET @strSql = 'ALTER TABLE #tempRecords ADD [' + @vcColumnName + '] NVARCHAR(MAX)'
		SET @vcCustomColumnNameOnly = @vcCustomColumnNameOnly+','+@vcColumnName
		EXEC(@strSql)
		
		IF @bitCustomField = 1
		BEGIN 
			IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea'            
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+', ISNULL((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),'''') ['+ @vcColumnName +']'
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+','''' AS  ['+ @vcColumnName +']'
			END  
			ELSE IF @vcAssociatedControlType = 'CheckBox'         
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+',ISNULL((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),0)  ['+ @vcColumnName + ']'
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+',0 AS ['+ @vcColumnName + ']'
			END            
			ELSE IF @vcAssociatedControlType = 'DateField'        
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+',dbo.FormatedDateFromDate((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'              
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+','''' AS  ['+ @vcColumnName +']'
			END            
			ELSE IF @vcAssociatedControlType = 'SelectBox'           
			BEGIN            
				SET @vcCustomColumnName=@vcCustomColumnName+',(SELECT TOP 1 ISNULL(L.vcData,'''') FROM CFW_FLD_Values CFW left Join ListDetails L on L.numListItemID=CFW.Fld_Value WHERE  CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID)'+' ['+ @vcColumnName +']'                                                         
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']'                                                         
			END
			ELSE IF @vcAssociatedControlType = 'CheckBoxList'
			BEGIN
				SET @vcCustomColumnName=@vcCustomColumnName+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(ISNULL((SELECT CFWInner.Fld_Value FROM CFW_FLD_Values CFWInner WHERE CFWInner.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFWInner.RecId=Div.numDivisionID),''''),'','')) FOR XML PATH('''')), 1, 1, ''''))'+' ['+ @vcColumnName +']'
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']'
			END
			ELSE
			BEGIN
				SET @vcCustomColumnName= CONCAT(@vcCustomColumnName,',(SELECT dbo.fn_GetCustFldStringValue(',@numFieldId,',CFW.RecId,CFW.Fld_Value',') FROM CFW_FLD_Values CFW left Join ListDetails L on L.numListItemID=CFW.Fld_Value WHERE  CFW.Fld_ID=',@numFieldId,' AND CFW.RecId=Div.numDivisionID)',' [', @vcColumnName ,']')
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']' 
			END
		END
		  
		SELECT TOP 1 
			@tintOrder=tintOrder + 1
			,@vcDbColumnName=vcDbColumnName
			,@vcFieldName=vcFieldName
			,@vcAssociatedControlType=vcAssociatedControlType
			,@vcListItemType=vcListItemType
			,@numListID=numListID
			,@vcLookBackTableName=vcLookBackTableName                                               
			,@bitCustomField=bitCustomField
			,@numFieldId=numFieldId
			,@bitAllowSorting=bitAllowSorting
			,@bitAllowEdit=0
			,@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 set @tintOrder=0 
	END
SET @strSql=''

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=43 AND DFCS.numFormID=43 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
-------------------- 0 - TASK----------------------------

IF(LEN(@CustomSearchCriteria)>0)
BEGIN
SET @CustomSearchCriteria = ' AND '+@CustomSearchCriteria
END
IF(LEN(@ActivityRegularSearch)=0)
BEGIN
	SET @ActivityRegularSearch = '1=1'
END
DECLARE @StaticColumns AS NVARCHAR(MAX)=''

SET @StaticColumns = ' RecordCount,RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,
numContactID,numDivisionID,tintCRMType,Id,bitTask,Startdate,EndTime,itemDesc,
[Name],vcFirstName,vcLastName,numPhone,numPhoneExtension,[Phone],vcCompanyName,vcProfile,vcEmail,Task,Status,numRecOwner,numModifiedBy,numTerId,numAssignedBy,caseid,vcCasenumber,
casetimeId,caseExpId,type,itemid,bitFollowUpAnyTime,numNoOfEmployeesId,vcComPhone,numFollowUpStatus,dtLastFollowUp,vcLastSalesOrderDate,monDealAmount,vcPerformance'
SET @dynamicQuery = ' INSERT INTO #tempRecords(
 '+@StaticColumns +@vcCustomColumnNameOnly+'
) SELECT COUNT(*) OVER() RecordCount,* FROM (SELECT 
Comm.numCommId AS RecordId,
Comm.numCreatedBy AS numCreatedBy, 
ISNULL(Div.numTerId,0) AS numOrgTerId,
-2 AS numTaskId,
bitClosedFlag AS IsTaskClosed,
CONCAT(''<div><div class="pull-left"><a onclick="openCommTask('',Comm.numCommId,'',0)" href="javascript:void(0)">'',dbo.GetListIemName(Comm.bitTask),''</a><br/>'',ISNULL(Comp.vcCompanyName,''''),(CASE WHEN AddC.numContactID IS NOT NULL THEN CONCAT(''&nbsp;<a class="text-green" href="javascript:OpenContact('',AddC.numContactID,'',1)">('',ISNULL(AddC.vcFirstName,''-''),'' '',ISNULL(AddC.vcLastName,''-''),'')</a>'') ELSE '''' END),''<br/>'',(CASE WHEN ISNULL(AddC.numPhone,'''') <> '''' THEN AddC.numPhone ELSE '''' END),(CASE WHEN ISNULL(AddC.numPhoneExtension,'''') <> '''' THEN CONCAT(''&nbsp;('',AddC.numPhoneExtension,'')'') ELSE '''' END),(CASE WHEN ISNULL(AddC.vcEmail,'''') <> '''' THEN CONCAT(''&nbsp;<img src="../images/msg_unread_small.gif" style="cursor:pointer" onclick="return OpemEmail('''''',AddC.vcEmail,'''''','',AddC.numcontactID,'')" />'') ELSE '''' END),''</div><div class="pull-right"><ul class="list-inline"><li><button class="btn btn-flat btn-task-finish" onclick="return ActionItemFinished('',Comm.numCommId,'');"><img src="../images/comflag.png" />&nbsp;Finish</button></li><li><button class="btn btn-comm-followup btn-flat btn-warning" onclick="return ActionItemFollowup('',Comm.numCommId,'','',AddC.numcontactID,'');">Follow-up</button></li></ul></div></div>'') AS TaskTypeName,
Comm.bitTask AS TaskType,
'''' AS OrigDescription,
comm.textDetails AS Description,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtStartTime) AS dtDueDate,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtStartTime) as DueDate,
''-'' As TotalProgress,
dbo.fn_GetContactName(numAssign) As numAssignedTo,
numAssign As numAssignToId,
listdetailsStatus.VcData As Priority, -- Priority column   
 listdetailsActivity.VcData As Activity ,
listdetailsStatus.NumlistItemID As PriorityId, -- Priority column   
 listdetailsActivity.NumlistItemID As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Comp.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,
 Addc.numContactID,Addc.numDivisionID,Div.tintCRMType,Comm.numCommId AS Id,convert(varchar(15),bitTask) as bitTask,
  DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtStartTime) as Startdate,                        
 DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtEndTime) as EndTime ,                                                                        
 Comm.textDetails AS itemDesc,  
 AddC.vcFirstName + '' '' + AddC.vcLastName  as [Name], 
 AddC.vcFirstName,AddC.vcLastName,Addc.numPhone,AddC.numPhoneExtension,                               
 case when Addc.numPhone<>'''' then + AddC.numPhone +case when AddC.numPhoneExtension<>'''' then '' - '' + AddC.numPhoneExtension else '''' end  else '''' end as [Phone], 
  Comp.vcCompanyName As vcCompanyName , ISNULL((SELECT vcData FROM ListDetails WHERE numListID =21 AND numDomainID='+ CAST(@numDomainID AS VARCHAR) +' AND  numListItemID=Comp.vcProfile ) ,'''') vcProfile ,                                                        
 AddC.vcEmail As vcEmail ,    
 dbo.GetListIemName(Comm.bitTask)AS Task,   
 listdetailsStatus.VcData As Status,dbo.fn_GetContactName(Comm.numCreatedBy) AS numRecOwner,dbo.fn_GetContactName(Comm.numModifiedBy) AS numModifiedBy,  
  (select TOP 1 vcData from ListDetails where numListItemID=Div.numTerId) AS numTerId,dbo.fn_GetContactName(Comm.numAssignedBy) AS numAssignedBy,
  convert(varchar(50),comm.caseid) as caseid,(select top 1 vcCaseNumber from cases where cases.numcaseid= comm.caseid )as vcCasenumber,
  ISNULL(comm.casetimeId,0) casetimeId,ISNULL(comm.caseExpId,0) caseExpId,0 as type,'''' as itemid,ISNULL(bitFollowUpAnyTime,0) bitFollowUpAnyTime,  
  dbo.GetListIemName(Comp.numNoOfEmployeesId) AS numNoOfEmployeesId,Div.vcComPhone,
dbo.GetListIemName(Div.numFollowUpStatus) AS numFollowUpStatus,' + 
CONCAT('ISNULL(dbo.FormatedDateFromDate(Div.dtLastFollowUp,',@numDomainID,'),'''')') + ' AS dtLastFollowUp,
' + CONCAT('ISNULL(dbo.FormatedDateFromDate(TempLastOrder.bintCreatedDate,',@numDomainID,'),'''')') + ' AS vcLastSalesOrderDate,ISNULL(TempPerformance.monDealAmount,0) monDealAmount,
(CASE WHEN ISNULL(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(FORMAT(TempPerformance.monDealAmount,''##,####.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) AS vcPerformance
  '+@vcCustomColumnName+'
 FROM  Communication Comm                              
 JOIN AdditionalContactsInformation AddC                                                  
 ON Comm.numContactId = AddC.numContactId                                  
 JOIN DivisionMaster Div                                                                         
 ON AddC.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                                                                         
 ON Div.numCompanyID = Comp.numCompanyId  
 OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=Div.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder
 OUTER APPLY(' + CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = Div.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') + ' AS TempPerformance
 LEFT JOIN OpportunityMaster Opp ON Opp.numDivisionId = Div.numDivisionID AND Opp.numContactId = AddC.numContactId  AND Opp.numOppID = Comm.numOppID 
 LEFT JOIN Sales_process_List_Master splm ON splm.Slp_Id = Opp.numBusinessProcessID 
 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId
  Left Join listdetails listdetailsStatus                                                 
 On Comm.numStatus = listdetailsStatus.NumlistItemID
  Left Join listdetails listdetailsActivity                                                  
 On Comm.numActivity = listdetailsActivity.NumlistItemID
 LEFT JOIN ListDetails AS listCompanyRating
 ON Comp.numCompanyRating=listCompanyRating.NumlistItemID
WHERE '+@ActivityRegularSearch+' AND
	Comm.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (Comm.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or Comm.numAssign= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END)  AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) <= '''+Cast(@endDate as varchar(30))+''')
'+@CustomSearchCriteria+'
 )   Q WHERE '+@OtherRegularSearch+'
  '+@IndivRecordPaging+'
'


EXEC (@dynamicQuery)

-------------------- 0 - Email Communication----------------------------
--''<a onclick="openCommTask(''+CAST(ac.activityid AS VARCHAR)+'',1)" href="javascript:void(0)">Calendar</a>'' As TaskTypeName,
IF(LEN(@ActivityRegularSearch)=0 OR RTRIM(LTRIM(@ActivityRegularSearch))='1=1')
BEGIN
DECLARE @FormattedItems As VARCHAR(MAX)
SET @FormattedItems ='
RecordCount,RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate'
SET @dynamicQuery =  ' INSERT INTO #tempRecords(
'+@FormattedItems+'
) SELECT  COUNT(*) OVER() RecordCount,* FROM (  SELECT 
ac.activityid AS RecordId,
0 AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
-2 AS numTaskId,
case when alldayevent = 1 then ''1'' else ''0'' end AS IsTaskClosed,
CONCAT(''<div><div class="pull-left"><a href="javascript:void(0)">Calendar</a><br/>'',ISNULL(Comp.vcCompanyName,''''),(CASE WHEN ACI.numContactID IS NOT NULL THEN CONCAT(''&nbsp;<a class="text-green" href="javascript:OpenContact('',ACI.numContactID,'',1)">('',ISNULL(ACI.vcFirstName,''-''),'' '',ISNULL(ACI.vcLastName,''-''),'')</a>'') ELSE '''' END),''<br/>'',(CASE WHEN ISNULL(ACI.numPhone,'''') <> '''' THEN ACI.numPhone ELSE '''' END),(CASE WHEN ISNULL(ACI.numPhoneExtension,'''') <> '''' THEN CONCAT(''&nbsp;('',ACI.numPhoneExtension,'')'') ELSE '''' END),(CASE WHEN ISNULL(ACI.vcEmail,'''') <> '''' THEN CONCAT(''&nbsp;<img src="../images/msg_unread_small.gif" style="cursor:pointer" onclick="return OpemEmail('''''',ACI.vcEmail,'''''','',ACI.numcontactID,'')" />'') ELSE '''' END),''</div><div class="pull-right"><button class="btn btn-flat btn-task-finish" onclick="return CalendarFinished('',ac.activityid,'');"><img src="../images/comflag.png" />&nbsp;Finish</button></div></div>'') AS TaskTypeName,
12 AS TaskType,
CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END AS OrigDescription,
(SELECT DISTINCT  
replace(replace((SELECT 
CASE(ISNULL(bitpartycalendarTitle,1)) WHEN 1 THEN ''<b><font color=red>Title</font></b>'' + CASE(ISNULL(ac.Subject,'''')) WHEN '''' THEN '''' else ac.Subject END + ''<br>'' else ''''  END  
+ CASE(ISNULL(bitpartycalendarLocation,1)) WHEN 1 THEN ''<b><font color=green>Location</font></b>'' + CASE(ISNULL(ac.location,'''')) WHEN '''' THEN '''' else ac.location END + ''<br>'' else ''''  END  
+ CASE ISNULL(bitpartycalendarDescription,1) 
	WHEN 1 
	THEN CONCAT(''<b><font color=#3c8dbc>Description</font></b>'',
				(CASE 
					WHEN LEN(CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END) > 100 
					THEN CONCAT(CAST((CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END) AS VARCHAR(100)),CONCAT(''...'',''<a href="#" role="button" title="Description" data-toggle="popover" data-content="'',(CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END) ,''"> more</a>'')) 
					ELSE (CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END) 
				END)) 
	ELSE ''''  
	END  
FROM Domain where numDomainId='+ CAST(@numDomainID AS VARCHAR) +' 
FOR XML PATH('''')
),''&lt;'', ''<''), ''&gt;'', ''>''))   AS Description,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dateadd(second,duration,startdatetimeutc)) AS dtDueDate,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dateadd(second,duration,startdatetimeutc)) as DueDate,
''0'' As TotalProgress,
''-'' As numAssignedTo,
''0'' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Comp.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,startdatetimeutc AS Startdate
 From activity ac join             
activityresource ar on  ac.activityid=ar.activityid             
join resource res on ar.resourceid=res.resourceid                         
LEFT JOIN AdditionalContactsInformation ACI on ACI.numContactId =  (SELECT TOP 1 ContactId FROM ActivityAttendees where ActivityID=ac.activityid)           
 LEFT JOIN DivisionMaster Div                                                          
 ON ACI.numDivisionId = Div.numDivisionID                                   
 LEFT JOIN CompanyInfo  Comp                             
 ON Div.numCompanyID = Comp.numCompanyId
  LEFT JOIN ListDetails AS listCompanyRating
 ON Comp.numCompanyRating=listCompanyRating.NumlistItemID
 OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=Div.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder                 
 OUTER APPLY(' + CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = Div.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') + ' AS TempPerformance

 where  res.numUserCntId = ' + Cast(@numUserCntID as varchar(10)) +'             
and ac.OriginalStartDateTimeUtc IS NULL            
and res.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  and 
(DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) <= '''+Cast(@endDate as varchar(30))+''')
--(startdatetimeutc between dateadd(month,-1,getutcdate()) and dateadd(month,1,getutcdate())) 
and [status] <> -1                                                     
and ac.activityid not in 
(select distinct(numActivityId) from communication Comm where Comm.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  
AND 1=(Case ' + Cast(@bitFilterRecord as varchar(10)) +' when 1 then Case when Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  then 1 else 0 end 
		else  Case when (Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  or Comm.numCreatedBy=' + Cast(@numUserCntID as varchar(10)) +' ) then 1 else 0 end end)   
		AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (Comm.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or Comm.numAssign= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END)                                                                   

)) Q WHERE '+@OtherRegularSearch+'  '+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)
-------------------- 1 - CASE----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate
) SELECT  COUNT(*) OVER() RecordCount,* FROM (  SELECT 
C.numCaseId AS RecordId,
C.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
CASE WHEN C.numStatus=1008 THEN 1 ELSE 0 END AS IsTaskClosed,
''<a onclick="openCaseDetails(''+CAST(C.numCaseId AS VARCHAR)+'')" href="javascript:void(0)">Case</a>'' As TaskTypeName,
1 AS TaskType,
'''' AS OrigDescription,
C.textSubject AS Description,
cast(C.intTargetResolveDate as datetime) AS dtDueDate,
cast(C.intTargetResolveDate as datetime) as DueDate,
''0'' As TotalProgress,
dbo.fn_GetContactName(C.numAssignedTo) As numAssignedTo,
C.numAssignedTo As numAssignToId,
listdetailsStatus.VcData As Priority, -- Priority column   
'''' As Activity ,
listdetailsStatus.NumlistItemID As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,cast(C.intTargetResolveDate as datetime) AS Startdate

 FROM 
	Cases  C                          
INNER JOIN  
	AdditionalContactsInformation  AddC                          
ON 
	C.numContactId = AddC.numContactId                           
INNER JOIN 
	DivisionMaster Div                           
ON 
	AddC.numDivisionId = Div.numDivisionID AND  
	AddC.numDivisionId = Div.numDivisionID                           
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
 LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
  Left Join listdetails listdetailsStatus                                                 
 On C.numPriority = listdetailsStatus.NumlistItemID
WHERE
	C.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND C.numStatus <> 136  AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (C.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or C.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END)  AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',intTargetResolveDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',intTargetResolveDate) <= '''+Cast(@endDate as varchar(30))+''') 
) Q WHERE '+@OtherRegularSearch+' '+@IndivRecordPaging+'
'

PRINT CAST(@dynamicQuery AS NTEXT)
EXEC (@dynamicQuery)

-------------------- 2 - Project Task ----------------------------

DECLARE @dynamicQuery1 VARCHAR(MAX) = ''

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,
numTaskEstimationInMinutes
,numTimeSpentInMinutes
,dtLastStartDate,Startdate
)  SELECT  COUNT(*) OVER() RecordCount,* FROM (SELECT
T.numProjectId AS RecordId,
T.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
T.bitTaskClosed AS IsTaskClosed,
CONCAT(''<div><div class="pull-left">'',''<a onclick="openProjectTask(''+CAST(T.numProjectId AS VARCHAR)+'')" href="javascript:void(0)">Project Task</a><br/><span>'',ISNULL(OP.vcProjectID,''''),''</span></div><div id="divTaskControlsProject" class="pull-right">'',(CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=T.numTaskId ORDER BY dtActionTime DESC,ID DESC),0) 
	WHEN 4 THEN (CASE WHEN EXISTS (SELECT C.numContractId FROM Contracts C WHERE C.numDivisonId=Div.numDivisionID) THEN CONCAT(''<ul class="list-inline">
	<li><button class="btn btn-xs btn-primary" onclick="return TaskClosed('',T.numTaskId,'');">Close</button></li>
    <li style="padding-left: 0px; padding-right: 0px">'',dbo.GetTimeSpendOnTaskByProject(T.numDomainID,T.numTaskId,1),''</li>
    <li style="padding-left: 0px; padding-right: 0px"><img src="../images/timeIconnnnn.png" runat="server" id="imgTimeIconn" style="height: 25px" /></li>
    <li style="padding-left: 0px; padding-right: 0px"><input type="checkbox" onclick="addRemoveTimeProject('',T.numDomainID,'','',Div.numDivisionID,'','',dbo.GetTimeSpendOnTaskByProject(T.numDomainID,T.numTaskId,2),'','',T.numTaskId,'','''''',DATEADD(MINUTE,'+Cast(-@ClientTimeZoneOffset as varchar(10)) +',(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId AND tintAction=1)),'''''','''''',DATEADD(MINUTE,'+Cast(-@ClientTimeZoneOffset as varchar(10)) +',(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId AND tintAction=4)),'''''','',T.numAssignTo,'','''''',T.vcTaskName,'''''','''''',(CASE WHEN dbo.GetTimeSpendOnTaskByProject(T.numDomainID,T.numTaskId,0) = ''Invalid time sequence'' THEN ''00:00'' ELSE dbo.GetTimeSpendOnTaskByProject(T.numDomainID,T.numTaskId,0) END),'''''','''''',ISNULL(ACustomer.vcEmail,''''),'''''','''''',dbo.fn_GetContactName(ACustomer.numContactId),'''''')"'',(CASE WHEN ISNULL(T.bitTimeAddedToContract,0)=1 THEN '' checked=''''checked'''''' ELSE '''' END),'' class="chk_'',T.numTaskId,''" /></li>
</ul>'') ELSE ''<img src="../images/comflag.png" />'' END)
	WHEN 3 THEN CONCAT(''<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowProject('',T.numTaskId,'');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-warning" onclick="return ShowTaskPausedWindowProject(this,'',T.numTaskId,'');">Pause</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-flat btn-task-finish" onclick="return TaskFinishedProject(this,'',T.numTaskId,'',4);"><img src="../images/comflag.png" />&nbsp;Finish</button></li><li style="vertical-align:middle;padding-right:0px;"><label class="taskTimerInitial"></label></li></ul>'')
	WHEN 2 THEN CONCAT(''<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowProject('',T.numTaskId,'');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat bg-purple" onclick="return TaskStartedProject(this,'',T.numTaskId,'',1);">Resume</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-flat btn-task-finish" onclick="return TaskFinishedProject(this,'',T.numTaskId,'',4);"><img src="../images/comflag.png" />&nbsp;Finish</button></li></ul>'')
	WHEN 1 THEN CONCAT(''<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowProject('',T.numTaskId,'');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-warning" onclick="return ShowTaskPausedWindowProject(this,'',T.numTaskId,'');">Pause</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-flat btn-task-finish" onclick="return TaskFinishedProject(this,'',T.numTaskId,'',4);"><img src="../images/comflag.png" />&nbsp;Finish</button></li><li style="vertical-align:middle;padding-right:0px;"><label class="taskTimerInitial"></label></li></ul>'')
	ELSE CONCAT(''<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowProject('',T.numTaskId,'');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-success btn-task-start" onclick="return TaskStartedProject(this,'',T.numTaskId,'',0);">Start</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-flat btn-task-finish" onclick="return TaskFinishedProject(this,'',T.numTaskId,'',4);"><img src="../images/comflag.png" />&nbsp;Finish</button></li></ul>'')
END),''</div></div>'') As TaskTypeName,
2 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',OP.dtmEndDate) AS dtDueDate,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',OP.dtmEndDate) as DueDate,
ISNULL((SELECT TOP 1 intTotalProgress FROM ProjectProgress WHERE numProId = T.numProjectId),0) As TotalProgress,
dbo.fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating
,((ISNULL(T.numHours,0) * 60) + ISNULL(T.numMinutes,0)) numTaskEstimationInMinutes
,(CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC),0) 
	WHEN 4 THEN 0
	WHEN 3 THEN dbo.GetTimeSpendOnTaskByProject(T.numDomainID,T.numTaskId,2)
	WHEN 2 THEN dbo.GetTimeSpendOnTaskByProject(T.numDomainID,T.numTaskId,2)
	WHEN 1 THEN 0
END) numTimeSpentInMinutes
,(CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=T.numTaskId ORDER BY dtActionTime DESC,ID DESC),0) 
	WHEN 4 THEN NULL
	WHEN 3 THEN DATEADD(MINUTE,'+Cast(-@ClientTimeZoneOffset as varchar(10)) +',(SELECT TOP 1 dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC))
	WHEN 2 THEN NULL
	WHEN 1 THEN DATEADD(MINUTE,'+Cast(-@ClientTimeZoneOffset as varchar(10)) +',(SELECT TOP 1 dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC))
END) dtLastStartDate,CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END AS Startdate'
SET @dynamicQuery1 = ' FROM 
	StagePercentageDetailsTask AS T
	LEFT JOIN StagePercentageDetails AS S
	ON S.numStageDetailsId=T.numStageDetailsId AND S.numProjectID=T.numProjectID
	INNER JOIN ProjectsMaster AS OP 
	ON T.numProjectID=OP.numProId
	LEFT JOIN StagePercentageMaster AS SM
	ON S.numStagePercentageId=SM.numStagePercentageId                        
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating ON Com.numCompanyRating=listCompanyRating.NumlistItemID
LEFT JOIN AdditionalContactsInformation ACustomer ON OP.numCustPrjMgr = ACustomer.numContactId
WHERE
	T.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' 
	AND (NOT EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID AND SPDTTL.tintAction=4) OR (ISNULL(T.bitTaskClosed,0) = 0 AND EXISTS (SELECT C.numContractId FROM Contracts C WHERE C.numDivisonId=Div.numDivisionID)))
	AND ((CAST(DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',OP.dtmStartDate) AS DATE) >= '''+Cast(CAST(@startDate AS DATE) as varchar(30))+''' and CAST(DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',OP.dtmStartDate) AS DATE) <= '''+Cast(CAST(@endDate AS DATE) as varchar(30))+''') OR (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',OP.dtmEndDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',OP.dtmEndDate) <= '''+Cast(@endDate as varchar(30))+''')) 
	AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (T.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or T.numAssignTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END) ) Q WHERE '+@OtherRegularSearch + @IndivRecordPaging

--PRINT CAST(@dynamicQuery AS NTEXT)

EXEC (@dynamicQuery + @dynamicQuery1)

------------------ 3 - Opportunity  Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate
) SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT 
T.numOppId AS RecordId,
T.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
T.bitTaskClosed AS IsTaskClosed,
CONCAT(''<div><div class="pull-left"><a onclick="openTask('',CAST(T.numOppId AS VARCHAR),'','',CAST(S.numStagePercentageId AS VARCHAR),'','',CAST(S.tinProgressPercentage AS VARCHAR),'','',CAST(S.tintConfiguration AS VARCHAR)+'')" href="javascript:void(0)">Opportunity Task</a><br/>'',ISNULL(Com.vcCompanyName,''''),(CASE WHEN ADC.numContactID IS NOT NULL THEN CONCAT(''&nbsp;<a class="text-green" href="javascript:OpenContact('',ADC.numContactID,'',1)">('',ISNULL(ADC.vcFirstName,''-''),'' '',ISNULL(ADC.vcLastName,''-''),'')</a>'') ELSE '''' END),''<br/>'',(CASE WHEN ISNULL(ADC.numPhone,'''') <> '''' THEN ADC.numPhone ELSE '''' END),(CASE WHEN ISNULL(ADC.numPhoneExtension,'''') <> '''' THEN CONCAT(''&nbsp;('',ADC.numPhoneExtension,'')'') ELSE '''' END),(CASE WHEN ISNULL(ADC.vcEmail,'''') <> '''' THEN CONCAT(''&nbsp;<img src="../images/msg_unread_small.gif" style="cursor:pointer" onclick="return OpemEmail('''''',ADC.vcEmail,'''''','',ADC.numcontactID,'')" />'') ELSE '''' END),''</div><div class="pull-right"><button class="btn btn-flat btn-task-finish" onclick="return TaskClosed('',T.numTaskId,'');"><img src="../images/comflag.png" />&nbsp;Finish</button></div></div>'') As TaskTypeName,
3 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END AS dtDueDate,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END as DueDate,
ISNULL((SELECT TOP 1 intTotalProgress FROM ProjectProgress WHERE numOppId = T.numOppId),0) As TotalProgress,
dbo.fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END  AS Startdate

 FROM 
	StagePercentageDetailsTask AS T
	LEFT JOIN StagePercentageDetails AS S
	ON S.numStageDetailsId=T.numStageDetailsId AND S.numOppId=T.numOppId
	LEFT JOIN OpportunityMaster AS OP 
	ON T.numOppId=OP.numOppId
	LEFT JOIN StagePercentageMaster AS SM
	ON S.numStagePercentageId=SM.numStagePercentageId                        
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN AdditionalContactsInformation ADC ON OP.numContactId = ADC.numContactId
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	T.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType=1 AND OP.tintOppStatus=0 AND T.numOppId>0 AND s.numOppId>0 AND T.bitSavedTask=1 AND T.bitTaskClosed=0 AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (T.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or T.numAssignTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END) ) Q WHERE '+@OtherRegularSearch+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''')
'+@IndivRecordPaging+' 
 '
 
 EXEC (@dynamicQuery)

------------------ 4 -  Sales Order Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate
)SELECT  COUNT(*) OVER() RecordCount,* FROM (  SELECT 
T.numOppId AS RecordId,
T.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
T.bitTaskClosed AS IsTaskClosed,
CONCAT(''<div><div class="pull-left"><a onclick="openTask('',CAST(T.numOppId AS VARCHAR),'','',CAST(S.numStagePercentageId AS VARCHAR),'','',CAST(S.tinProgressPercentage AS VARCHAR),'','',CAST(S.tintConfiguration AS VARCHAR)+'')" href="javascript:void(0)">Sales Order Task</a><br/>'',ISNULL(Com.vcCompanyName,''''),(CASE WHEN ADC.numContactID IS NOT NULL THEN CONCAT(''&nbsp;<a class="text-green" href="javascript:OpenContact('',ADC.numContactID,'',1)">('',ISNULL(ADC.vcFirstName,''-''),'' '',ISNULL(ADC.vcLastName,''-''),'')</a>'') ELSE '''' END),''<br/>'',(CASE WHEN ISNULL(ADC.numPhone,'''') <> '''' THEN ADC.numPhone ELSE '''' END),(CASE WHEN ISNULL(ADC.numPhoneExtension,'''') <> '''' THEN CONCAT(''&nbsp;('',ADC.numPhoneExtension,'')'') ELSE '''' END),(CASE WHEN ISNULL(ADC.vcEmail,'''') <> '''' THEN CONCAT(''&nbsp;<img src="../images/msg_unread_small.gif" style="cursor:pointer" onclick="return OpemEmail('''''',ADC.vcEmail,'''''','',ADC.numcontactID,'')" />'') ELSE '''' END),''</div><div class="pull-right"><button class="btn btn-flat btn-task-finish" onclick="return TaskClosed('',T.numTaskId,'');"><img src="../images/comflag.png" />&nbsp;Finish</button></div></div>'') As TaskTypeName,
4 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END AS dtDueDate,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END as DueDate,
ISNULL((SELECT TOP 1 intTotalProgress FROM ProjectProgress WHERE numOppId = T.numOppId),0) As TotalProgress,
dbo.fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END AS Startdate

 FROM 
	StagePercentageDetailsTask AS T
	LEFT JOIN StagePercentageDetails AS S
	ON S.numStageDetailsId=T.numStageDetailsId AND S.numOppId=T.numOppId
	LEFT JOIN OpportunityMaster AS OP 
	ON T.numOppId=OP.numOppId
	LEFT JOIN StagePercentageMaster AS SM
	ON S.numStagePercentageId=SM.numStagePercentageId                        
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN AdditionalContactsInformation ADC ON OP.numContactId = ADC.numContactId 
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	T.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType=1 AND OP.tintOppStatus=1 AND T.numOppId>0 AND s.numOppId>0 AND T.bitSavedTask=1 AND T.bitTaskClosed=0 AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (T.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or T.numAssignTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END) ) Q WHERE '+@OtherRegularSearch+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''')
'+@IndivRecordPaging+'
 '
 EXEC (@dynamicQuery)
  
------------------ 5 -  Work Order Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate
)SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT 
WorkOrder.numWOID AS RecordId,
WorkOrder.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
WorkOrder.numWOID AS numTaskId,
(CASE WHEN WorkOrder.numWOStatus = 23184 THEN 1 ELSE 0 END) AS IsTaskClosed,
CONCAT(''<a href="../items/frmWorkOrder.aspx?WOID='',WorkOrder.numWOID,''" target="_blank">Work Order Management</a>'') As TaskTypeName,
5 AS TaskType,
'''' AS OrigDescription,
CONCAT(''<b>'',Item.vcItemName,''</b>'','' ('',WorkOrder.numQtyItemsReq,'')'','' <b>Work Order Status :'', ''</b>'',''<label class="lblWorkOrderStatus" id="'',WorkOrder.numWOID,''"><i class="fa fa-refresh fa-spin"></i></lable>'') AS Description,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) AS dtDueDate,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) as DueDate,
dbo.GetTotalProgress(WorkOrder.numDomainID,WorkOrder.numWOID,1,1,'''',0) As TotalProgress,
dbo.fn_GetContactName(WorkOrder.numAssignedTo) As numAssignedTo,
WorkOrder.numAssignedTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) AS Startdate

 FROM 
	WorkOrder
INNER JOIN 
	Item
ON
	WorkOrder.numItemCode = Item.numItemCode
LEFT JOIN 
	OpportunityMaster
ON 
	WorkOrder.numOppId=OpportunityMaster.numOppId
INNER JOIN 
	DivisionMaster Div                           
ON 
	Div.numDivisionId = (CASE WHEN OpportunityMaster.numOppID IS NOT NULL THEN OpportunityMaster.numDivisionID ELSE ' + CAST(@numDomainDivisionID AS VARCHAR) + ' END)                        
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN 
	ListDetails AS listCompanyRating
ON 
	Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	WorkOrder.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' 
	AND WorkOrder.numWOStatus <> 23184
	AND WorkOrder.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + '
	AND ((DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmStartDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmStartDate) <= '''+Cast(@endDate as varchar(30))+''') OR (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) <= '''+Cast(@endDate as varchar(30))+'''))
) Q WHERE '+@OtherRegularSearch+' '+@IndivRecordPaging+'
 '
 EXEC (@dynamicQuery)
------------------ 6 -  Work Center Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,
numTaskEstimationInMinutes
,numTimeSpentInMinutes
,dtLastStartDate
,numRemainingQty
,Startdate
) SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT 
T.numTaskId AS RecordId,
T.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
(CASE WHEN EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID AND SPDTTL.tintAction=4) THEN 1 ELSE 0 END) AS IsTaskClosed,
CONCAT(''<div><div class="pull-left"><a href="../items/frmWorkOrder.aspx?WOID='',W.numWOID,''" target="_blank">Work Center Task</a><br/><span>'',CONCAT(I.vcItemName,'' ('',W.numQtyItemsReq,'')''),''</span></div><div class="pull-right" id="divTaskControlsWorkOrder">'',(CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=T.numTaskId ORDER BY dtActionTime DESC, ID DESC),0) 
	WHEN 4 THEN ''<img src="../images/comflag.png" />''
	WHEN 3 THEN CONCAT(''<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowWorkOrder('',T.numTaskId,'');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-success btn-task-start" onclick="return TaskStartedWorkOrder(this,'',T.numTaskId,'',0);">Start</button></li><li style="vertical-align:middle;padding-right:0px;"><label class="taskTimerInitial"></label></li></ul>'')
	WHEN 2 THEN CONCAT(''<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowWorkOrder('',T.numTaskId,'');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat bg-purple" onclick="return TaskStartedWorkOrder(this,'',T.numTaskId,'',1);">Resume</button></li></ul>'')
	WHEN 1 THEN CONCAT(''<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowWorkOrder('',T.numTaskId,'');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-warning" onclick="return ShowTaskPausedWindowWorkOrder(this,'',T.numTaskId,'');">Pause</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-flat btn-task-finish" onclick="return TaskFinishedWorkOrder(this,'',T.numTaskId,'',4);"><img src="../images/comflag.png" />&nbsp;Finish</button></li><li style="vertical-align:middle;padding-right:0px;"><input type="text" onkeydown="return ProcessedUnitsChangedWorkOrder(this,'',T.numTaskId,'',event);" class="form-control txtUnitsProcessed" style="width:50px;padding:1px 2px 1px 2px;height:23px;" /></li><li style="vertical-align:middle;padding-right:0px;"><label class="taskTimerInitial"></label></li></ul>'')
	ELSE CONCAT(''<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowWorkOrder('',T.numTaskId,'');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-success btn-task-start" onclick="return TaskStartedWorkOrder(this,'',T.numTaskId,'',0);">Start</button></li></ul>'')
END),''</div></div>'') As TaskTypeName,
6 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) AS dtDueDate,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) as DueDate,
(CASE 
	WHEN EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID AND SPDTTL.tintAction=4) THEN CAST(100 AS INT)
	ELSE CAST(((ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID),0) / W.numQtyItemsReq) * 100) AS INT)
END) As TotalProgress,
dbo.fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating
 ,(((ISNULL(T.numHours,0) * 60) + ISNULL(T.numMinutes,0)) * ISNULL(W.numQtyItemsReq,0)) numTaskEstimationInMinutes
,(CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC),0) 
	WHEN 4 THEN 0
	WHEN 3 THEN dbo.GetTimeSpendOnTask(T.numDomainID,T.numTaskId,2)
	WHEN 2 THEN dbo.GetTimeSpendOnTask(T.numDomainID,T.numTaskId,2)
	WHEN 1 THEN 0
END) numTimeSpentInMinutes
,(CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=T.numTaskId ORDER BY dtActionTime DESC,ID DESC),0) 
	WHEN 4 THEN NULL
	WHEN 3 THEN DATEADD(MINUTE,'+Cast(-@ClientTimeZoneOffset as varchar(10)) +',(SELECT TOP 1 dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC))
	WHEN 2 THEN NULL
	WHEN 1 THEN DATEADD(MINUTE,'+Cast(-@ClientTimeZoneOffset as varchar(10)) +',(SELECT TOP 1 dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC))
END) dtLastStartDate
,(CASE
	WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId AND tintAction=4) THEN 0
	ELSE ISNULL(W.numQtyItemsReq,0) - ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId),0)
END) AS numRemainingQty
,DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) AS Startdate
 FROM 
	StagePercentageDetailsTask AS T
	INNER JOIN WorkOrder AS W 
	ON T.numWorkOrderId=W.numWOId     
	INNER JOIN Item I ON W.numItemCode=I.numItemCode
LEFT JOIN OpportunityMaster AS OP 
	ON W.numOppId=OP.numOppId
INNER JOIN 
	DivisionMaster Div                           
ON 
	Div.numDivisionId = (CASE WHEN OP.numOppID IS NOT NULL THEN OP.numDivisionID ELSE ' + CAST(@numDomainDivisionID AS VARCHAR) + ' END)                         
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	W.numDomainID = ' + Cast(@numDomainID as varchar(10))  + '
	AND W.numWOStatus <> 23184 
	AND NOT EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID AND SPDTTL.tintAction=4) 
	AND T.numAssignTo = ' + CAST(@numUserCntID AS VARCHAR) + '
	AND ((DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmStartDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmStartDate) <= '''+Cast(@endDate as varchar(30))+''') OR (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) <= '''+Cast(@endDate as varchar(30))+''')) 
) Q WHERE '+@OtherRegularSearch+' '+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)

------------------ 7 -  Requisition Approval ----------------------------

IF(@bitREQPOApproval=1 AND @numUserCntID IN(SELECT * FROM Split(@vchREQPOApprovalEmp,',') WHERE Items<>''))
BEGIN
SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate
) SELECT  COUNT(*) OVER() RecordCount,* FROM( SELECT 
OP.numOppId AS RecordId,
OP.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
CASE WHEN ISNULL(OP.intReqPOApproved,0)=0 THEN 0 ELSE 1 END AS IsTaskClosed,
''<a  onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')" href="javascript:void(0)">Requisition Approval</a>'' As TaskTypeName,
7 AS TaskType,
'''' AS OrigDescription,
''<b>For :</b> <a class="underLineHyperLink" onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')" href="javascript:void(0)">''+OP.vcPOppName+''</a> &nbsp;<i onclick="ApproveRejectPORequistion(''+CAST(OP.numOppId AS VARCHAR)+'',1)"  class="fa fa-thumbs-up cursor"></i>  &nbsp;<i onclick="ApproveRejectPORequistion(''+CAST(OP.numOppId AS VARCHAR)+'',2)"  class="fa fa-thumbs-down cursor"></i>'' AS Description,
OP.intPEstimatedCloseDate AS dtDueDate,
OP.intPEstimatedCloseDate as DueDate,
0 As TotalProgress,
dbo.fn_GetContactName('+CAST(@numUserCntID AS VARCHAR(200))+') As numAssignedTo,
'+CAST(@numUserCntID AS VARCHAR(200))+' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,OP.intPEstimatedCloseDate  AS Startdate

 FROM  
 OpportunityMaster AS OP 
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	OP.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType = 2 AND OP.tintOppStatus=0 
	AND 1 = (CASE WHEN '+CAST(@decReqPOMinValue AS VARCHAR(100))+'>0 AND Op.monPAmount>'+CAST(@decReqPOMinValue AS VARCHAR(100))+' THEN 1  WHEN '+CAST(@decReqPOMinValue AS VARCHAR(100))+'=0 THEN 1  ELSE 0 END ) ) AS Q
WHERE '+@OtherRegularSearch+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)

END
------------------ 8 -  PO Approval ----------------------------
IF(@bitREQPOApproval=1 AND @numUserCntID IN(SELECT * FROM Split(@vchREQPOApprovalEmp,',') WHERE Items<>''))
BEGIN
SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate
) SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT
OP.numOppId AS RecordId,
OP.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
CASE WHEN ISNULL(OP.intReqPOApproved,0)=0 THEN 0 ELSE 1 END AS IsTaskClosed,
''<a onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')" href="javascript:void(0)">PO Approval</a>'' As TaskTypeName,
8 AS TaskType,
'''' AS OrigDescription,
''<b>For:</b> <a href="javascript:void(0)"  class="underLineHyperLink" onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')">''+OP.vcPOppName+''</a> &nbsp;<i onclick="ApproveRejectPORequistion(''+CAST(OP.numOppId AS VARCHAR)+'',1)"  class="fa fa-thumbs-up cursor"></i>  &nbsp;<i onclick="ApproveRejectPORequistion(''+CAST(OP.numOppId AS VARCHAR)+'',2)"  class="fa fa-thumbs-down cursor"></i> '' AS Description,
OP.intPEstimatedCloseDate AS dtDueDate,
OP.intPEstimatedCloseDate as DueDate,
0 As TotalProgress,
dbo.fn_GetContactName('+CAST(@numUserCntID AS VARCHAR(200))+') As numAssignedTo,
'+CAST(@numUserCntID AS VARCHAR(200))+' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,OP.intPEstimatedCloseDate AS Startdate

 FROM  
 OpportunityMaster AS OP 
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	OP.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType = 2 AND OP.tintOppStatus=1 
	AND 1 = (CASE WHEN '+CAST(@decReqPOMinValue AS VARCHAR(100))+'>0 AND Op.monPAmount>'+CAST(@decReqPOMinValue AS VARCHAR(100))+' THEN 1  WHEN '+CAST(@decReqPOMinValue AS VARCHAR(100))+'=0 THEN 1  ELSE 0 END )) AS Q
	WHERE  '+@OtherRegularSearch+' AND  (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)

END

------------------ 9 -  Price Margin Approval ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate
) SELECT COUNT(*) OVER() RecordCount,* FROM (SELECT
OP.numOppId AS RecordId,
OP.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
ISNULL(OP.bitReqPOApproved,0) AS IsTaskClosed,
''<a onclick="OpenMarginApproval(''+CAST(OP.numOppId AS VARCHAR)+'')" href="javascript:void(0)">Price Margin Approval</a>'' As TaskTypeName,
9 AS TaskType,
'''' AS OrigDescription,
''<b>For : </b> <a href="javascript:void(0)"  onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')">''+OP.vcPOppName+''</a> : '' +dbo.ItemUnitPriceApproval_CheckAndGetItemDetails(OP.numOppId,OP.numDomainId)+''  &nbsp;<i onclick="ApproveRejectPriceMarginApproval(''+CAST(OP.numOppId AS VARCHAR)+'',1)" class="fa fa-thumbs-up cursor"></i>  &nbsp;<i onclick="ApproveRejectPriceMarginApproval(''+CAST(OP.numOppId AS VARCHAR)+'',2)" class="fa fa-thumbs-down cursor"></i>''  AS Description,
OP.bintCreatedDate AS dtDueDate,
OP.bintCreatedDate as DueDate,
0 As TotalProgress,
dbo.fn_GetContactName('+CAST(@numUserCntID AS VARCHAR(200))+') As numAssignedTo,
'+CAST(@numUserCntID AS VARCHAR(200))+' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,OP.bintCreatedDate AS Startdate

 FROM  
 OpportunityMaster AS OP 
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	OP.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType = 1 AND (SELECT COUNT(OpportunityItems.numoppitemtCode) FROM OpportunityItems 
						WHERE OpportunityItems.bitItemPriceApprovalRequired=1 AND OpportunityItems.numOppId=OP.numOppId) >0
	AND '+CAST(@numUserCntID AS VARCHAR(100))+' IN (SELECT numUserID FROM UnitPriceApprover WHERE numDomainId= ' + Cast(@numDomainID as varchar(10))  + ' )) AS Q
	WHERE  '+@OtherRegularSearch+' AND  (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'

'
EXEC (@dynamicQuery)

------------------------10 - A/R Invoice Due------------------------------------
IF(@bitARInvoiceDue=1 AND @numUserCntID IN(SELECT * FROM Split(@vchARInvoiceDue,',') WHERE Items<>''))
BEGIN
 DECLARE  @AuthoritativeSalesBizDocId  AS INTEGER
    SELECT @AuthoritativeSalesBizDocId = isnull(numAuthoritativeSales,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId = @numDomainId;
SET @dynamicQuery=  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate
) SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT
OM.numOppId AS RecordId,
OM.numCreatedBy AS numCreatedBy,
ISNULL(DM.numTerId,0) AS numOrgTerId,
OB.numOppBizDocsId AS numTaskId,
0 AS IsTaskClosed,
''<a href="javascript:void(0)"  onclick="openBizDoc(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'')">A/R Invoice Due</a>'' As TaskTypeName,
10 AS TaskType,
'''' AS OrigDescription,
''<b>For : </b> <a href="javascript:void(0)" class="underLineHyperLink" onclick="openBizDoc(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'')">''+CAST(OB.[vcBizDocID] AS VARCHAR)+''</a> of <a href="javascript:void(0)" class="underLineHyperLink" onclick="openSalesOrder(''+CAST(OM.numOppId AS VARCHAR)+'')">''+CAST(OM.[vcPOppName] AS VARCHAR)+''</a> <b>Balance Due</b> <a href="javascript:void(0)" onclick="OpenAmtPaid(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'',''+CAST(OM.[numDivisionId] AS VARCHAR)+'')"   class="underLineHyperLink">'' + CAST(ISNULL(C.[varCurrSymbol],'''') AS VARCHAR) + '''' + CAST(CAST(isnull(OB.monDealAmount  - ISNULL(TablePayments.monPaidAmount,0),0) AS DECIMAL(18,2)) AS VARCHAR)+''</a>''  AS Description,
 CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate)
                          WHEN 0 THEN ob.[dtFromDate]
                        END AS dtDueDate,
 CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate),
                                                                   1)
                          WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],1)
                        END as DueDate,
0 As TotalProgress,
'''' As numAssignedTo,
0 As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(DM.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,
  (CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate)
                          WHEN 0 THEN ob.[dtFromDate]
                        END) AS StartDate

 FROM   [OpportunityMaster] OM INNER JOIN [DivisionMaster] DM ON OM.[numDivisionId] = DM.[numDivisionID]
				LEFT JOIN CompanyInfo  Com ON DM.numCompanyID = Com.numCompanyId    
				LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
               LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN [Currency] C ON OM.[numCurrencyID] = C.[numCurrencyID]
			   OUTER APPLY
			   (
					SELECT 
						SUM(monAmountPaid) AS monPaidAmount
					FROM
						DepositeDetails DD
					INNER JOIN
						DepositMaster DM
					ON
						DD.numDepositID=DM.numDepositId
					WHERE 
						numOppID = OM.numOppID
						AND numOppBizDocsID=OB.numOppBizDocsID
						AND CONVERT(DATE,DM.dtDepositDate) <= DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +','''+Cast(@endDate as varchar(100))+''')
				) TablePayments
        WHERE  OM.[tintOppType] = 1
               AND OM.[tintOppStatus] = 1
               AND om.[numDomainId] = ' + Cast(@numDomainId as varchar(10))  + '
               AND OB.[numBizDocId] = '+CAST(@AuthoritativeSalesBizDocId As VARCHAR)+'
               AND OB.bitAuthoritativeBizDocs=1
               AND ISNULL(OB.tintDeferred,0) <> 1
			   AND (OM.numAccountClass= 0 OR 0=0)
			   AND CONVERT(DATE,OB.[dtCreatedDate]) <= DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +','''+Cast(@endDate as varchar(100))+''')
               AND isnull(OB.monDealAmount
                            ,0) > 0  AND isnull(OB.monDealAmount * OM.fltExchangeRate
                        - ISNULL(TablePayments.monPaidAmount,0) * OM.fltExchangeRate,0) != 0
	) AS Q
	WHERE  '+@OtherRegularSearch+' AND  (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)
END
------------------------11 - A/P Bill Due------------------------------------
IF(@bitAPBillsDue=1 AND @numUserCntID IN(SELECT * FROM Split(@vchAPBillsDue,',') WHERE Items<>''))
BEGIN   
   DECLARE  @AuthoritativePurchaseBizDocId  AS INTEGER
    SELECT  @AuthoritativePurchaseBizDocId= isnull(numAuthoritativePurchase,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId =  @numDomainId
	

SET @dynamicQuery = '  INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate
) SELECT  COUNT(*) OVER() RecordCount,* FROM (SELECT
OM.numOppId AS RecordId,
OM.numCreatedBy AS numCreatedBy,
ISNULL(DM.numTerId,0) AS numOrgTerId,
OB.numOppBizDocsId AS numTaskId,
0 AS IsTaskClosed,
''<a href="javascript:void(0)" onclick="openPurchaseBill(''+CAST(DM.[numDivisionID] As VARCHAR)+'')">A/P Bill Due<a/>'' As TaskTypeName,
11 AS TaskType,
'''' AS OrigDescription,
''<b>For : </b> <a href="javascript:void(0)" class="underLineHyperLink" onclick="openBizDoc(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'')">''+CAST(OB.[vcBizDocID] AS VARCHAR)+''</a> of <a href="javascript:void(0)" class="underLineHyperLink" onclick="openSalesOrder(''+CAST(OM.numOppId AS VARCHAR)+'')">''+CAST(OM.[vcPOppName] AS VARCHAR)+''</a> <b>Balance Due : </b> <a href="javascript:void(0)" onclick="openBizDoc(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'')"   class="underLineHyperLink">'' + CAST(ISNULL(C.[varCurrSymbol],'''') AS VARCHAR) + '''' + CAST(CAST(isnull((OB.monDealAmount * OM.fltExchangeRate)  - (ISNULL(TablePayment.monPaidAmount,0)* OM.fltExchangeRate),0) AS DECIMAL(18,2))  AS VARCHAR)+''</a>''  AS Description,
 CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate)
                          WHEN 0 THEN ob.[dtFromDate]
                        END AS dtDueDate,
 CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate),
                                                                   1)
                          WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],1)
                        END as DueDate,
0 As TotalProgress,
'''' As numAssignedTo,
0 As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(DM.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,
  (CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate)
                          WHEN 0 THEN ob.[dtFromDate]
                        END) AS StartDate

   FROM   [OpportunityMaster] OM
               INNER JOIN [DivisionMaster] DM
                 ON OM.[numDivisionId] = DM.[numDivisionID]
				 LEFT JOIN CompanyInfo  Com ON DM.numCompanyID = Com.numCompanyId    
				LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
               LEFT OUTER JOIN OpportunityBizDocs OB
                 ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN [Currency] C
                 ON OM.[numCurrencyID] = C.[numCurrencyID]
				 OUTER APPLY
				(
					SELECT
						SUm(BPD.monAmount) monPaidAmount
					FROM	
						BillPaymentDetails BPD
					INNER JOIN
						BillPaymentHeader BPH
					ON
						BPD.numBillPaymentID=BPH.numBillPaymentID
					WHERE
						BPD.numOppBizDocsID = OB.numOppBizDocsId
						AND CAST(BPH.dtPaymentDate AS DATE) <= DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +','''+Cast(@endDate as varchar(100))+''')
				) TablePayment
        WHERE  OM.[tintOppType] = 2
               AND OM.[tintOppStatus] = 1
               AND om.[numDomainId] = 1
               AND OB.[numBizDocId] = '+CAST(@AuthoritativePurchaseBizDocId AS VARCHAR)+' 
               AND OB.bitAuthoritativeBizDocs=1
               AND ISNULL(OB.tintDeferred,0) <> 1
			   AND (OM.numAccountClass= 0 OR 0=0)
			   AND CONVERT(DATE,OB.[dtCreatedDate]) <= DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +','''+Cast(@endDate as varchar(100))+''')
               AND  isnull(OB.monDealAmount * OM.fltExchangeRate,0) > 0 
			   AND isnull(OB.monDealAmount * OM.fltExchangeRate
                        - ISNULL(TablePayment.monPaidAmount,0) * OM.fltExchangeRate,0) > 0
	) AS Q
	WHERE  '+@OtherRegularSearch+' AND  (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'

EXEC (@dynamicQuery)
END

END
--SELECT DueDate FROM #tempRecords
SET @FormattedItems =''
DECLARE @startEndDate AS VARCHAR(MAX)='  CONCAT(''<span style="color:#909090;font-size:14px;font-weight:bold">'',(SELECT FORMAT(CAST(DueDate AS DATE), ''MMM-dd-yyyy'')),CASE WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  '' @ ''+LEFT(RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7))-1) WHEN Startdate IS NOT NULL AND EndTime IS NULL THEN  '' @ ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7) ELSE '''' END,CASE WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  ''<Span style="font-size:14px;font-weight:normal;font-style: italic;"><br/>to ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,EndTime,100),8)),7)+''</Span>'' ELSE '''' END,''</span>'') END '
SET @FormattedItems=' UPDATE #tempRecords SET DueDate = 
CASE 
WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getutcdate())) then CONCAT(''<b><font color="#FF0000" style="font-size:14px">Today'',CASE WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  '' @ ''+LEFT(RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7))-1) WHEN Startdate IS NOT NULL AND EndTime IS NULL THEN  '' @ ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7) ELSE '''' END,CASE WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  ''<Span style="font-size:14px;font-weight:normal;font-style: italic;"><br/>to ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,EndTime,100),8)),7)+''</Span>'' ELSE '''' END ,''</font></b>'') 
WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(day,1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getutcdate()))) then CONCAT(''<b><font color=#ED8F11 style="font-size:14px">Tommorow'',CASE WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  '' @ ''+LEFT(RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7))-1) WHEN Startdate IS NOT NULL AND EndTime IS NULL THEN  '' @ ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7) ELSE '''' END,CASE WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  ''<Span style="font-size:14px;font-weight:normal;font-style: italic;"><br/>to ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,EndTime,100),8)),7)+''</Span>'' ELSE '''' END ,''</font></b>'') 
WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(day,2,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getutcdate()))) then CONCAT(''<b><font color=#8FAADC style="font-size:14px">'',datename(dw,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',dtDueDate)),''<span style="color:#000;font-weight:500"> - '',CAST(DAY(Startdate) AS VARCHAR(10)),CASE
	WHEN DAY(Startdate) % 100 IN (11, 12, 13) THEN ''th''
	WHEN DAY(Startdate) % 10 = 1 THEN ''st''
	WHEN DAY(Startdate) % 10 = 2 THEN ''nd''
	WHEN DAY(Startdate) % 10 = 3 THEN ''rd''
ELSE ''th'' END,''</span>'',''</font></b>'',''<b>'',''<br/>''
,''<Span style="font-size:14px;font-weight:normal;font-style: italic;">'',CASE WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  LEFT(RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7))-1) WHEN Startdate IS NOT NULL AND EndTime IS NULL THEN  RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7) ELSE '''' END,''</span>''
,CASE WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  ''<Span style="font-size:14px;font-weight:normal;font-style: italic;"> to ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,EndTime,100),8)),7)+''</Span>'' ELSE '''' END ,''</b>'')
WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(day,3,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getutcdate()))) then CONCAT(''<b><font color=#CC99FF style="font-size:14px">'',datename(dw,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',dtDueDate)),''<span style="color:#000;font-weight:500"> - '',CAST(DAY(Startdate) AS VARCHAR(10)),CASE
	WHEN DAY(Startdate) % 100 IN (11, 12, 13) THEN ''th''
	WHEN DAY(Startdate) % 10 = 1 THEN ''st''
	WHEN DAY(Startdate) % 10 = 2 THEN ''nd''
	WHEN DAY(Startdate) % 10 = 3 THEN ''rd''
ELSE ''th'' END,''</span>'',''</font></b>'',''<b>'',''<br/>''
,''<Span style="font-size:14px;font-weight:normal;font-style: italic;">'',CASE WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  LEFT(RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7))-1) WHEN Startdate IS NOT NULL AND EndTime IS NULL THEN  RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7) ELSE '''' END,''</span>''
,CASE WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  ''<Span style="font-size:14px;font-weight:normal;font-style: italic;"> to ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,EndTime,100),8)),7)+''</Span>'' ELSE '''' END ,''</b>'')
WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(day,4,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getutcdate()))) then CONCAT(''<b><font color=#AED495 style="font-size:14px">'',datename(dw,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',dtDueDate)),''<span style="color:#000;font-weight:500"> - '',CAST(DAY(Startdate) AS VARCHAR(10)),CASE
	WHEN DAY(Startdate) % 100 IN (11, 12, 13) THEN ''th''
	WHEN DAY(Startdate) % 10 = 1 THEN ''st''
	WHEN DAY(Startdate) % 10 = 2 THEN ''nd''
	WHEN DAY(Startdate) % 10 = 3 THEN ''rd''
ELSE ''th'' END,''</span>'',''</font></b>'',''<b>'',''<br/>''
,''<Span style="font-size:14px;font-weight:normal;font-style: italic;">'',CASE WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  LEFT(RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7))-1) WHEN Startdate IS NOT NULL AND EndTime IS NULL THEN  RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7) ELSE '''' END,''</span>''
,CASE WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  ''<Span style="font-size:14px;font-weight:normal;font-style: italic;"> to ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,EndTime,100),8)),7)+''</Span>'' ELSE '''' END ,''</b>'')
WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(day,5,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getutcdate()))) then CONCAT(''<b><font color=#72DFDC style="font-size:14px">'',datename(dw,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',dtDueDate)),''<span style="color:#000;font-weight:500"> - '',CAST(DAY(Startdate) AS VARCHAR(10)),CASE
	WHEN DAY(Startdate) % 100 IN (11, 12, 13) THEN ''th''
	WHEN DAY(Startdate) % 10 = 1 THEN ''st''
	WHEN DAY(Startdate) % 10 = 2 THEN ''nd''
	WHEN DAY(Startdate) % 10 = 3 THEN ''rd''
ELSE ''th'' END,''</span>'',''</font></b>'',''<b>'',''<br/>''
,''<Span style="font-size:14px;font-weight:normal;font-style: italic;">'',CASE WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  LEFT(RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7))-1) WHEN Startdate IS NOT NULL AND EndTime IS NULL THEN  RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7) ELSE '''' END,''</span>''
,CASE WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  ''<Span style="font-size:14px;font-weight:normal;font-style: italic;"> to ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,EndTime,100),8)),7)+''</Span>'' ELSE '''' END ,''</b>'')
WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(day,6,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getutcdate()))) then CONCAT(''<b><font color=#FF9999 style="font-size:14px">'',datename(dw,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',dtDueDate)),''<span style="color:#000;font-weight:500"> - '',CAST(DAY(Startdate) AS VARCHAR(10)),CASE
	WHEN DAY(Startdate) % 100 IN (11, 12, 13) THEN ''th''
	WHEN DAY(Startdate) % 10 = 1 THEN ''st''
	WHEN DAY(Startdate) % 10 = 2 THEN ''nd''
	WHEN DAY(Startdate) % 10 = 3 THEN ''rd''
ELSE ''th'' END,''</span>'',''</font></b>'',''<b>'',''<br/>''
,''<Span style="font-size:14px;font-weight:normal;font-style: italic;">'',CASE WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  LEFT(RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7))-1) WHEN Startdate IS NOT NULL AND EndTime IS NULL THEN  RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7) ELSE '''' END,''</span>''
,CASE WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  ''<Span style="font-size:14px;font-weight:normal;font-style: italic;"> to ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,EndTime,100),8)),7)+''</Span>'' ELSE '''' END ,''</b>'')
ELSE   '
 EXEC (@FormattedItems+@startEndDate)
 
 DECLARE @strSql3 VARCHAR(MAX)=''
 IF LEN(@columnName) > 0
BEGIN
	SET @strSql3= CONCAT(@strSql3,' order by ',CASE WHEN @columnName = 'Action-Item Participants' THEN CONCAT('[','Action-Item Participants',']') WHEN @columnName='DueDate' THEN 'dtDueDate' ELSE @columnName END,' ',@columnSortOrder)
END
ELSE
BEGIN
	SET @strSql3= CONCAT(@strSql3,' ORDER BY dtDueDate ASC  ')
END
SET @TotalRecordCount=(SELECT ISNULL(SUM(DISTINCT RecordCount),0) FROM #tempRecords)

SET @dynamicQuery = 'SELECT '+CAST(@TotalRecordCount AS varchar(400))+' AS TotalRecords,T.*
 FROM #tempRecords T WHERE 1=1 '+@strSql3
--SET @dynamicQuery = 'SELECT '+CAST(@TotalRecordCount AS varchar(400))+' AS TotalRecords,T.*
-- FROM #tempRecords T WHERE 1=1 '+@strSql3

 PRINT @dynamicQuery
EXEC(@dynamicQuery)
DECLARE @TempColumns TABLE
	(
        vcFieldName VARCHAR(200)
        ,vcDbColumnName VARCHAR(200)
        ,vcOrigDbColumnName VARCHAR(200)
        ,bitAllowSorting BIT
        ,numFieldId INT
        ,bitAllowEdit BIT
        ,bitCustomField BIT
        ,vcAssociatedControlType VARCHAR(50)
        ,vcListItemType  VARCHAR(200)
        ,numListID INT
        ,ListRelID INT
        ,vcLookBackTableName VARCHAR(200)
        ,bitAllowFiltering BIT
        ,vcFieldDataType  VARCHAR(20)
		,intColumnWidth INT
		,bitClosedColumn BIT
		,bitFieldMessage BIT DEFAULT 0
		,vcFieldMessage VARCHAR(500)
		,bitIsRequired BIT DEFAULT 0
		,bitIsNumeric BIT DEFAULT 0
		,bitIsAlphaNumeric BIT DEFAULT 0
		,bitIsEmail BIT DEFAULT 0
		,bitIsLengthValidation  BIT DEFAULT 0
		,intMaxLength INT DEFAULT 0
		,intMinLength INT DEFAULT 0
	)



	DECLARE @Nocolumns TINYINT
	SET @Nocolumns=0       
         
	SELECT 
		@Nocolumns=COUNT(*) 
	FROM 
		View_DynamicColumns 
	WHERE 
		numFormId=43 
		AND numUserCntID=@numUserCntID 
		AND numDomainID=@numDomainID 
		AND tintPageType=1 
  
  
if @Nocolumns > 0            
begin      
INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcFieldDataType,vcOrigDbColumnName,bitAllowFiltering,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
 FROM View_DynamicColumns 
 where numFormId=43 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
  order by tintOrder asc  
  
end            
else            
begin 

	--Check if Master Form Configuration is created by administrator if NoColumns=0
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 43 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END

	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			43,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,NULL,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=43 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0

		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,vcFieldDataType,vcOrigDbColumnName,bitAllowFiltering, ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType,numListID,vcLookBackTableName, bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=43 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	ORDER BY 
		tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		 INSERT INTO #tempForm
		select tintOrder,vcDbColumnName,vcFieldDataType
		,vcOrigDbColumnName,bitAllowFiltering,ISNULL(vcCultureFieldName,vcFieldName),                  
		 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
		,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
		 FROM View_DynamicDefaultColumns
		 where numFormId=43 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 AND numDomainID=@numDomainID
		order by tintOrder asc  
	END 
enD

INSERT INTO #tempForm
	SELECT 
		tintOrder,vcDbColumnName,vcFieldDataType
		,vcOrigDbColumnName,bitAllowFiltering,ISNULL(vcCultureFieldName,vcFieldName),                  
		 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
		,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
	FROM 
		View_DynamicDefaultColumns
	WHERE 
		numFormId=43 
		AND bitDefault=1 
		AND ISNULL(bitSettingField,0)=1 
		AND numDomainID=@numDomainID
		AND numFieldID NOT IN (SELECT numFieldId FROM #tempForm)
	ORDER BY 
		tintOrder asc  
			
UPDATE
		TC
	SET
		TC.intColumnWidth = (CASE WHEN TC.vcOrigDbColumnName = 'DueDate' AND ISNULL(DFCD.intColumnWidth,0) < 180 THEN 180 ELSE ISNULL(DFCD.intColumnWidth,0) END)
	FROM
		#tempForm TC
	INNER JOIN
		DycFormConfigurationDetails DFCD
	ON
		TC.numFieldId = DFCD.numFieldId
	WHERE
		DFCD.numDomainId = @numDomainID
		AND DFCD.numUserCntID = @numUserCntID
		AND DFCD.numFormId = 43
		AND DFCD.tintPageType = 1 


	SELECT * FROM #tempForm order by tintOrder asc  
END
GO
/****** Object:  StoredProcedure [dbo].[USP_CaseDetails]    Script Date: 07/26/2008 16:15:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON                     
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_UpdateTimeContract')
DROP PROCEDURE usp_UpdateTimeContract
GO
CREATE PROCEDURE [dbo].[usp_UpdateTimeContract]  
@numDomainID as numeric(9),
@numDivisonId as numeric(9) ,    
@numSecounds as numeric(9) ,  
@intType AS INT =0 ,
@numTaskID AS numeric(9),
@outPut AS INT  = 0 OUTPUT,
@balanceContractTime AS BIGINT=0  OUTPUT,
@isProjectActivityEmail AS INT
AS
BEGIN
	IF EXISTS(SELECT * FROM Contracts WHERE numDivisonId=@numDivisonId AND numDomainId=@numDomainID AND intType=1)
	BEGIN
		
		--@intType = 1
		IF(@intType=1)
		BEGIN
			SET @outPut=3
				IF(@isProjectActivityEmail=1)
				BEGIN
					UPDATE StagePercentageDetailsTask SET bitTimeAddedToContract=0 WHERE numTaskId=@numTaskID
				END
				IF(@isProjectActivityEmail=2)
				BEGIN
					UPDATE Communication SET bitTimeAddedToContract=0 WHERE numCommId=@numTaskID
				END
				
				IF(@isProjectActivityEmail=3)
				BEGIN
					UPDATE Activity SET bitTimeAddedToContract=0 WHERE ActivityID=@numTaskID
				END

				IF @isProjectActivityEmail <> 1 --PROJECTS TAsKS TIME IS MANAGED THROUGH TRIGGER ON StagePercentageDetailsTaskTimeLog TABLE
				BEGIN
					UPDATE Contracts SET timeUsed=timeUsed-@numSecounds,dtmModifiedOn=GETUTCDATE() WHERE numDivisonId=@numDivisonId AND numDomainId=@numDomainID
					DELETE FROM ContractsLog WHERE numDivisionID=@numDivisonId  AND numReferenceId=@numTaskID
					AND numContractId=(SELECT TOP 1 numContractId FRoM Contracts WHERE numDivisonId=@numDivisonId AND numDomainId=@numDomainID)
				END
		END
		--@intType = 2
		IF(@intType=2)
		BEGIN			
			IF @isProjectActivityEmail <> 1 AND ((SELECT ISNULL(timeLeft,0) FROM Contracts WHERE numDivisonId=@numDivisonId AND numDomainId=@numDomainID AND intType=1)<@numSecounds)
			BEGIN
				SET @outPut=1 --Time is not available
			END
			ELSE IF @isProjectActivityEmail = 1 AND ((SELECT ISNULL(timeLeft,0) FROM Contracts WHERE numDivisonId=@numDivisonId AND numDomainId=@numDomainID AND intType=1) <= 0)
			BEGIN
				SET @outPut=1 --Time is not available
			END
			ELSE
			BEGIN
				SET @outPut=2 --Time is available
				IF(@isProjectActivityEmail=1)
				BEGIN
					UPDATE StagePercentageDetailsTask SET bitTimeAddedToContract=1 WHERE numTaskId=@numTaskID
				END
				IF(@isProjectActivityEmail=2)
				BEGIN
					UPDATE Communication SET bitTimeAddedToContract=1 WHERE numCommId=@numTaskID
				END
				IF(@isProjectActivityEmail=3)
				BEGIN
					UPDATE Activity SET bitTimeAddedToContract=1 WHERE ActivityID=@numTaskID
				END

				IF @isProjectActivityEmail <> 1 --PROJECTS TAsKS TIME IS MANAGED THROUGH TRIGGER ON StagePercentageDetailsTaskTimeLog TABLE
				BEGIN
					UPDATE Contracts SET timeUsed=timeUsed+@numSecounds,dtmModifiedOn=GETUTCDATE() WHERE numDivisonId=@numDivisonId AND numDomainId=@numDomainID
					SET @balanceContractTime = (SELECT ISNULL((ISNULL(timeLeft,0)-ISNULL(timeUsed,0)),0) FROM Contracts WHERE numDivisonId=@numDivisonId AND numDomainId=@numDomainID AND intType=1)
					INSERT INTO ContractsLog (
						intType, numDivisionID, numReferenceId, dtmCreatedOn, numCreatedBy,vcDetails,numUsedTime,numBalance,numContractId,tintRecordType
					)
					SELECT TOP 1
						1,@numDivisonId,@numTaskID,GETUTCDATE(),0,@isProjectActivityEmail,@numSecounds,@balanceContractTime,numContractId,@isProjectActivityEmail
					FROM Contracts WHERE numDivisonId=@numDivisonId AND numDomainId=@numDomainID AND intType=1 ORDER BY numContractId DESC
				END
			END
		END
	END
	ELSE
	BEGIN
		SET @outPut=0
	END

	SET @balanceContractTime = (SELECT TOP 1 ((ISNULL(C.numHours,0) * 60 * 60) + (ISNULL(C.numMinutes,0) * 60)) - ISNULL((SELECT SUM(CLInner.numUsedTime) FROM ContractsLog CLInner WHERE CLInner.numContractId=C.numContractId),0) FROM Contracts C WHERE numDivisonId=@numDivisonId AND numDomainId=@numDomainID AND intType=1 ORDER BY numContractId DESC)
END
