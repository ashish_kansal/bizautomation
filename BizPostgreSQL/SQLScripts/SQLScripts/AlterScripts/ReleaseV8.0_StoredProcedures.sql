/******************************************************************
Project: Release 8.0 Date: 28.AUGUST.2017
Comments: STORE PROCEDURES
*******************************************************************/



GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetOrderAssemblyKitInclusionDetails')
DROP FUNCTION GetOrderAssemblyKitInclusionDetails
GO
CREATE FUNCTION [dbo].[GetOrderAssemblyKitInclusionDetails] 
(
      @numOppID AS NUMERIC(18,0),
	  @numOppItemID AS NUMERIC(18,0),
	  @numQty AS NUMERIC(18,0)
)
RETURNS VARCHAR(MAX)
AS 
BEGIN	
	DECLARE @vcInclusionDetails AS VARCHAR(MAX) = ''

	DECLARE @TEMPTABLE TABLE
	(
		ID INT IDENTITY(1,1),
		numOppItemCode NUMERIC(18,0),
		numOppChildItemID NUMERIC(18,0), 
		numOppKitChildItemID NUMERIC(18,0),
		numTotalQty NUMERIC(18,2),
		numUOMID NUMERIC(18,0),
		tintLevel TINYINT,
		vcInclusionDetail VARCHAR(1000)
	);

	--FIRST LEVEL CHILD
	INSERT INTO @TEMPTABLE
	(
		numOppItemCode,numOppChildItemID,numOppKitChildItemID,numTotalQty,numUOMID,tintLevel,vcInclusionDetail
	)
	SELECT 
		OKI.numOppItemID,
		OKI.numOppChildItemID,
		CAST(0 AS NUMERIC(18,0)),
		CAST((@numQty * OKI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
		ISNULL(OKI.numUOMID,0),
		1,
		CONCAT(I.vcItemName, ' (', CAST((@numQty * OKI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')')
	FROM 
		OpportunityMaster OM
	INNER JOIN
		OpportunityItems OI
	ON
		OM.numOppId=OI.numOppId
	INNER JOIN
		OpportunityKitItems OKI
	ON
		OI.numoppitemtCode=OKI.numOppItemID
	INNER JOIN
		Item I
	ON
		OKI.numChildItemID = I.numItemCode
	LEFT JOIN
		UOM
	ON
		UOM.numUOMId = (CASE WHEN ISNULL(OKI.numUOMID,0) > 0 THEN OKI.numUOMID ELSE I.numBaseUnit END)
	WHERE
		OM.numOppId = @numOppID
		AND OI.numoppitemtCode=@numOppItemID

	--SECOND LEVEL CHILD
	INSERT INTO @TEMPTABLE
	(
		numOppItemCode,numOppChildItemID,numOppKitChildItemID,numTotalQty,numUOMID,tintLevel,vcInclusionDetail
	)
	SELECT 
		OKCI.numOppItemID,
		OKCI.numOppChildItemID,
		OKCI.numOppKitChildItemID,
		CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
		ISNULL(OKCI.numUOMID,0),
		2,
		CONCAT(I.vcItemName, ' (', CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')')
	FROM 
		OpportunityKitChildItems OKCI
	INNER JOIN
		Item I
	ON
		OKCI.numItemID = I.numItemCode
	LEFT JOIN
		UOM
	ON
		UOM.numUOMId = (CASE WHEN ISNULL(OKCI.numUOMID,0) > 0 THEN OKCI.numUOMID ELSE I.numBaseUnit END)
	INNER JOIN
		@TEMPTABLE c
	ON
		OKCI.numOppID = @numOppID
		AND OKCI.numOppItemID = c.numOppItemCode
		AND OKCI.numOppChildItemID = c.numOppChildItemID

	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT

	SELECT @iCount = COUNT(*) FROM @TEMPTABLE WHERE tintLevel=1

	WHILE @i <= @iCount
	BEGIN
		SET @vcInclusionDetails = CONCAT(@vcInclusionDetails,(CASE WHEN LEN(@vcInclusionDetails) > 0 THEN ', ' ELSE '' END),(SELECT vcInclusionDetail FROM @TEMPTABLE WHERE ID=@i))

		IF (SELECT COUNT(*) FROM @TEMPTABLE WHERE tintLevel=2 AND numOppChildItemID=(SELECT numOppChildItemID FROM @TEMPTABLE WHERE ID=@i)) > 0
		BEGIN
			SET @vcInclusionDetails = CONCAT(@vcInclusionDetails,' (')

			SELECT @vcInclusionDetails = COALESCE(@vcInclusionDetails + ', ','') + vcInclusionDetail FROM @TEMPTABLE WHERE tintLevel=2 AND numOppChildItemID=(SELECT numOppChildItemID FROM @TEMPTABLE WHERE ID=@i)

			SET @vcInclusionDetails = CONCAT(@vcInclusionDetails,' )')
		END

		SET @i = @i + 1
	END

	RETURN REPLACE(@vcInclusionDetails,'(, ','(')
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AdditionalContactsInformation_RollbackImport')
DROP PROCEDURE USP_AdditionalContactsInformation_RollbackImport
GO
CREATE PROCEDURE [dbo].[USP_AdditionalContactsInformation_RollbackImport]
 @numContactID numeric(9)=0
AS      
BEGIN
	DECLARE @Email AS VARCHAR(1000) 
	DECLARE @AddEmail AS VARCHAR(1000)

	SELECT @Email =vcEmail FROM dbo.AdditionalContactsInformation WHERE numContactId=@numContactID
	SELECT @AddEmail =vcAsstEmail FROM dbo.AdditionalContactsInformation WHERE numContactId=@numContactID

	DECLARE @numDomainID AS NUMERIC(18,0) 
	DECLARE @numDivisionID NUMERIC(18,0)
	DECLARE @bitPrimaryContact BIT
	SELECT @numDomainID=[numDomainID],@numDivisionID=numDivisionID,@bitPrimaryContact=ISNULL(bitPrimaryContact,0) FROM AdditionalContactsInformation WHERE numContactID=@numContactID    


	IF EXISTS(SELECT * FROM UserMaster where numuserdetailId=@numContactID and numDomainID=@numDomainID and bitActivateFlag=1)
	BEGIN
		RAISERROR('USER', 16, 1);
	END

	IF EXISTS ( select * from [OpportunityMaster] WHERE [numContactId] =@numContactID)
	BEGIN
  		RAISERROR('CHILD_OPP', 16, 1);
	END

	IF EXISTS ( SELECT * FROM [Cases] WHERE [numContactId] = @numContactID)
	BEGIN
  		RAISERROR('CHILD_CASE', 16, 1 ) ;
		RETURN 1
	END

	BEGIN TRY
	BEGIN TRAN
		DELETE FROM dbo.CompanyAssociations WHERE numDomainID=@numDomainID AND numContactID=@numContactID
		DELETE FROM dbo.ImportActionItemReference WHERE numContactID=@numContactID
		delete ConECampaignDTL where numConECampID in (select numConEmailCampID from ConECampaign where numContactID=@numContactID)  
		delete ConECampaign where numContactID=@numContactID  
		delete AOIContactLink where numContactID=@numContactID    
		delete CaseContacts where numContactID=@numContactID   
		delete ProjectsContacts where numContactID=@numContactID  
		delete OpportunityContact where numContactID=@numContactID  
		delete UserTeams where numUserCntID=@numContactID 
		delete UserTerritory where numUserCntID=@numContactID 
		DELETE FROM dbo.UserMaster WHERE numUserDetailId = @numContactID AND bitActivateFlag=0
		DELETE AdditionalContactsInformation WHERE numContactID=@numContactID
		
		UPDATE EmailMaster SET numContactID=NULL WHERE vcEmailId = @Email AND numContactID =@numContactID
		UPDATE EmailMaster SET numContactID=NULL WHERE vcEmailID = @AddEmail AND numContactID =@numContactID
  
		IF @bitPrimaryContact = 1 AND (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionId=@numDivisionID) > 0
		BEGIN
			UPDATE AdditionalContactsInformation SET bitPrimaryContact=1 WHERE numContactId=ISNULL((SELECT TOP 1 numContactId FROM AdditionalContactsInformation WHERE numDivisionId=@numDivisionID),0)
		END

		COMMIT TRAN
    END TRY 
    BEGIN CATCH		
        IF (@@TRANCOUNT > 0) 
        BEGIN
            ROLLBACK TRAN
            DECLARE @error VARCHAR(1000)
			SET @error = ERROR_MESSAGE();
            RAISERROR(@error, 16, 1 ) ;
        END
    END CATCH	
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ApplyPromotionOffer')
DROP PROCEDURE USP_ApplyPromotionOffer
GO
CREATE PROCEDURE [dbo].[USP_ApplyPromotionOffer]    
 @numDomainID AS NUMERIC(9) = 0,
 @numSiteID AS NUMERIC(9) = 0,
 @vcCouponCode AS VARCHAR(20),
 @numDivisionId AS NUMERIC(9)=0
as    

DECLARE @numProId AS NUMERIC(9);SET @numProId=0
DECLARE @tintAppliesTo AS TINYINT;SET @tintAppliesTo=0
DECLARE @tintContactsType AS TINYINT;SET @tintContactsType=0
DECLARE @tintLimitationBasedOn AS TINYINT;SET @tintLimitationBasedOn=0
DECLARE @UsedCouponCount AS INT;SET @UsedCouponCount=0


SELECT TOP 1 @numProId=numProId,@tintAppliesTo=tintAppliesTo,@tintContactsType=tintContactsType,@tintLimitationBasedOn=tintLimitationBasedOn  
FROM PromotionOffer 
WHERE numDomainID=@numDomainID 
AND (numSiteID = @numSiteID OR ISNULL(@numSiteID,0) = -1) -- Changed By Manish Anjara On : 1st Feb,2013
AND vcCouponCode=@vcCouponCode

SELECT * FROM PromotionOffer WHERE numProId=@numProId

--Promotions Offer Contacts
IF @tintContactsType=1 OR @tintContactsType=2 --Organization,Profile-Relationship
  EXEC dbo.USP_GetPromotionOfferDtl @numProId,@numDomainID,2,@tintContactsType,@numSiteID,1
ELSE IF @tintContactsType=3 --All Customers
 SELECT''

--Promotion Offer Items
IF @tintAppliesTo=1 --Items
 EXEC dbo.USP_GetPromotionOfferDtl @numProId,@numDomainID,1,@tintAppliesTo,@numSiteID,1
 
ELSE IF @tintAppliesTo=2 --Items Category
  SELECT DISTINCT numItemID AS numValue FROM SiteCategories SC JOIN ItemCategory IC 
  ON SC.numCategoryID=IC.numCategoryID WHERE SC.numSiteId=@numSiteID AND SC.numCategoryID IN 
(SELECT numvalue FROM PromotionOfferItems WHERE numProId=@numProId AND tintType=2 AND tintRecordType=1 )

ELSE IF @tintAppliesTo=3 --All Items
 SELECT ''


--Usage Limitation tintLimitationBasedOn==> 1:Customer 2:Site
SELECT @UsedCouponCount=COUNT(OL.numPromotionId) FROM OpportunityLinking OL JOIN OpportunityMaster OM ON OM.numOppID = OL.numChildOppID 
WHERE ISNULL(OL.numPromotionId,0)=@numProId AND 1=(CASE WHEN @tintLimitationBasedOn=1 THEN CASE WHEN @numDivisionId=0 THEN 1 --Without Login
																						  WHEN OM.numDivisionId=@numDivisionId THEN 1 ELSE 0 END --Customer
												  WHEN @tintLimitationBasedOn=2 THEN CASE WHEN ISNULL(OL.numSiteID,0)=@numSiteID THEN 1 ELSE 0 END --Site
												  ELSE 0 END)
												  
SELECT @UsedCouponCount	AS UsedCouponCount									  

GO
/****** Object:  StoredProcedure [dbo].[Usp_CreateCloneItem]    Script Date: 18/06/2013 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Manish Anjara
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Usp_CreateCloneItem')
DROP PROCEDURE Usp_CreateCloneItem
GO
CREATE PROCEDURE [dbo].[Usp_CreateCloneItem]
(
	@numItemCode		BIGINT OUTPUT,
	@numDomainID		NUMERIC(18,0)
)
AS 

BEGIN
--Item 	
--ItemAPI-
--ItemCategory
--ItemDetails
--ItemImages
--ItemTax
--Vendor,
--WareHouseItems
--CFW_FLD_Values_Item
BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numItemGroup AS NUMERIC(18,0)
	DECLARE @bitMatrix AS NUMERIC(18,0)
	DECLARE @vcItemName AS VARCHAR(300)

	SELECT @numItemGroup=numItemGroup,@bitMatrix=bitMatrix,@vcItemName=vcItemName FROM Item WHERE numItemCode = @numItemCode AND numDomainID = @numDomainID

	DECLARE @numNewItemCode AS BIGINT
	
	INSERT INTO dbo.Item 
	(vcItemName,txtItemDesc,charItemType,monListPrice,numItemClassification,bitTaxable,vcSKU,bitKitParent,numVendorID,numDomainID,numCreatedBy,bintCreatedDate,
	bintModifiedDate,numModifiedBy,bitAllowBackOrder,bitSerialized,vcModelID,numItemGroup,numCOGsChartAcntId,numAssetChartAcntId,numIncomeChartAcntId,
	monAverageCost,monCampaignLabourCost,bitFreeShipping,fltLength,fltWidth,fltHeight,fltWeight,vcUnitofMeasure,bitCalAmtBasedonDepItems,bitShowDeptItemDesc,
	bitShowDeptItem,bitAssembly,numBarCodeId,vcManufacturer,numBaseUnit,numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,vcPathForImage,
	vcPathForTImage,tintStandardProductIDType,numShipClass,vcExportToAPI,bitAllowDropShip,bitVirtualInventory,bitContainer,bitMatrix)
	SELECT vcItemName,txtItemDesc,charItemType,monListPrice,numItemClassification,bitTaxable,vcSKU,bitKitParent,numVendorID,numDomainID,numCreatedBy,GETDATE(),
	GETDATE(),numModifiedBy,bitAllowBackOrder,bitSerialized,vcModelID,numItemGroup,numCOGsChartAcntId,numAssetChartAcntId,numIncomeChartAcntId,
	0,monCampaignLabourCost,bitFreeShipping,fltLength,fltWidth,fltHeight,fltWeight,vcUnitofMeasure,bitCalAmtBasedonDepItems,bitShowDeptItemDesc,
	bitShowDeptItem,bitAssembly,numBarCodeId,vcManufacturer,numBaseUnit,numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,vcPathForImage,
	vcPathForTImage,tintStandardProductIDType,numShipClass,vcExportToAPI,bitAllowDropShip,bitVirtualInventory,bitContainer,bitMatrix
	FROM Item WHERE numItemCode = @numItemCode AND numDomainID = @numDomainID
	 
	SELECT @numNewItemCode  = SCOPE_IDENTITY()


	IF ISNULL(@bitMatrix,0) = 1
	BEGIN
		DECLARE @vcProductVariation VARCHAR(300)

		WITH data (ProductId, ProductOptionGroupId, ProductOptionId) AS 
		(
			SELECT
				@numItemCode
				,CFW_Fld_Master.Fld_id
				,ListDetails.numListItemID
			FROM
				ItemGroupsDTL
			INNER JOIN
				CFW_Fld_Master
			ON
				ItemGroupsDTL.numOppAccAttrID = CFW_Fld_Master.Fld_id
			INNER JOIN
				ListDetails
			ON
				CFW_Fld_Master.numlistid = ListDetails.numListID
			WHERE
				numItemGroupID = @numItemGroup
		),
		ranked AS (
		/* ranking the group IDs */
			SELECT
				ProductId,
				ProductOptionGroupId,
				ProductOptionId,
				GroupRank = DENSE_RANK() OVER (PARTITION BY ProductId ORDER BY ProductOptionGroupId)
			FROM 
				data
		),
		crossjoined AS (
			/* obtaining all possible combinations */
			SELECT
				ProductId,
				ProductOptionGroupId,
				GroupRank,
				ProductVariant = CAST(CONCAT(ProductOptionGroupId,':',ProductOptionId) AS varchar(250))
			FROM 
				ranked
			WHERE 
				GroupRank = 1
			UNION ALL
			SELECT
				r.ProductId,
				r.ProductOptionGroupId,
				r.GroupRank,
				ProductVariant = CAST(CONCAT(c.ProductVariant,',',r.ProductOptionGroupId,':',r.ProductOptionId) AS VARCHAR(250))
			FROM 
				ranked r
			INNER JOIN 
				crossjoined c 
			ON 
				r.ProductId = c.ProductId
			AND 
				r.GroupRank = c.GroupRank + 1
		  ),
		  maxranks AS (
			/* getting the maximum group rank value for every product */
			SELECT
				ProductId,
				MaxRank = MAX(GroupRank)
			FROM 
				ranked
			GROUP BY 
				ProductId
		  )
		
		SELECT TOP 1  
			@vcProductVariation = c.ProductVariant 
		FROM 
			crossjoined c 
		INNER JOIN 
			maxranks m 
		ON 
			c.ProductId = m.ProductId
			AND c.GroupRank = m.MaxRank
			AND c.ProductVariant NOT IN (SELECT 
											STUFF((SELECT ',' + CONCAT(FLD_ID,':',FLD_Value) FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=IR.numItemCode FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)'),1,1,'') AS NameValues
										FROM 
											Item
										INNER JOIN
											ItemAttributes IR
										ON
											IR.numDomainID=@numDomainID
											AND Item.numItemCode =IR.numItemCode
										WHERE 
											Item.numDomainID=@numDomainID
											AND bitMatrix = 1
											AND numItemGroup = @numItemGroup
											AND vcItemName = @vcItemName
										GROUP BY
											IR.numItemCode)

		IF LEN(ISNULL(@vcProductVariation,'')) = 0
		BEGIN
			RAISERROR('ATTRIBUTES_ARE_NOT_AVAILABLE_OR_NO_COMBINATION_LEFT_TO_ADD',16,1)
		END
		ELSE
		BEGIN
			INSERT INTO ItemAttributes
			(
				numDomainID
				,numItemCode
				,FLD_ID
				,FLD_Value
			)
			SELECT
				@numDomainID
				,@numNewItemCode
				,SUBSTRING(OutParam,0,CHARINDEX(':',OutParam,0))
				,SUBSTRING(OutParam,CHARINDEX(':',OutParam,0) + 1,LEN(OutParam))
			FROM
				dbo.SplitString(@vcProductVariation,',')
		END
	END
	 
	INSERT INTO dbo.ItemAPI 
	(WebApiId,numDomainId,numItemID,vcAPIItemID,numCreatedby,dtCreated,numModifiedby,dtModified)
	SELECT WebApiId,numDomainId,@numNewItemCode,vcAPIItemID,numCreatedby,GETDATE(),numModifiedby,GETDATE()
	FROM dbo.ItemAPI WHERE numItemID = @numItemCode AND numDomainId = @numDomainId
	
	INSERT INTO dbo.ItemCategory ( numItemID,numCategoryID )
	SELECT @numNewItemCode, numCategoryID FROM ItemCategory WHERE numItemID = @numItemCode
	
	INSERT INTO dbo.ItemDetails (numItemKitID,numChildItemID,numQtyItemsReq,numWareHouseItemId,numUOMId,vcItemDesc,sintOrder)
	SELECT @numNewItemCode,numChildItemID,numQtyItemsReq,numWareHouseItemId,numUOMId,vcItemDesc,sintOrder FROM dbo.ItemDetails WHERE numItemKitID = @numItemCode
	
	INSERT INTO dbo.ItemImages (numItemCode,vcPathForImage,vcPathForTImage,bitDefault,intDisplayOrder,numDomainId,bitIsImage)
	SELECT 
		@numNewItemCode
		,CONCAT(SUBSTRING(vcPathForImage, 0, CHARINDEX('.',vcPathForImage,0)),'_',@numNewItemCode,SUBSTRING(vcPathForImage, charindex('.',vcPathForImage,0), LEN(vcPathForImage)))
		,CONCAT(SUBSTRING(vcPathForTImage, 0, CHARINDEX('.',vcPathForTImage,0)),'_',@numNewItemCode,SUBSTRING(vcPathForTImage, charindex('.',vcPathForTImage,0), LEN(vcPathForTImage)))
		,bitDefault
		,intDisplayOrder
		,numDomainId
		,bitIsImage 
	FROM dbo.ItemImages WHERE numItemCode = @numItemCode  AND numDomainId = @numDomainID
	
	INSERT INTO dbo.ItemTax (numItemCode,numTaxItemID,bitApplicable)
	SELECT @numNewItemCode,numTaxItemID,bitApplicable FROM dbo.ItemTax WHERE numItemCode = @numItemCode
	
	INSERT INTO dbo.Vendor (numVendorID,vcPartNo,numDomainID,monCost,numItemCode,intMinQty)
	SELECT numVendorID,vcPartNo,numDomainID,monCost,@numNewItemCode,intMinQty 
	FROM dbo.Vendor WHERE numItemCode = @numItemCode AND numDomainID = @numDomainID
	
	INSERT INTO dbo.WareHouseItems (numItemID,numWareHouseID,numOnHand,numOnOrder,numReorder,numAllocation,numBackOrder,monWListPrice,vcLocation,vcWHSKU,
	vcBarCode,numDomainID,dtModified,numWLocationID) 
	SELECT @numNewItemCode,numWareHouseID,0,0,0,0,0,monWListPrice,vcLocation,vcWHSKU,vcBarCode,numDomainID,GETDATE(),numWLocationID 
	FROM dbo.WareHouseItems  WHERE numItemID = @numItemCode AND numDomainID = @numDomainID

	IF ISNULL(@bitMatrix,0) = 1 AND (SELECT COUNT(*) FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numNewItemCode) > 0
	BEGIN
		-- INSERT NEW WAREHOUSE ATTRIBUTES
		INSERT INTO CFW_Fld_Values_Serialized_Items
		(
			Fld_ID,
			Fld_Value,
			RecId,
			bitSerialized
		)                                                  
		SELECT 
			Fld_ID,
			ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
			TEMPWarehouse.numWarehouseItemID,
			0 
		FROM 
			ItemAttributes  
		OUTER APPLY
		(
			SELECT
				numWarehouseItemID
			FROM 
				WarehouseItems 
			WHERE 
				numDomainID=@numDomainID 
				AND numItemID=@numNewItemCode
		) AS TEMPWarehouse
		WHERE
			numDomainID=@numDomainID 
			AND numItemCode=@numNewItemCode
	END
	
	INSERT INTO CFW_FLD_Values_Item (Fld_ID,Fld_Value,RecId)  
	SELECT Fld_ID,Fld_Value,@numNewItemCode FROM CFW_FLD_Values_Item  WHERE RecId = @numItemCode 


	SET @numItemCode = @numNewItemCode
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH	
END
/****** Object:  StoredProcedure [dbo].[USP_GetCategory]    Script Date: 08/08/2009 16:37:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                        
-- exec [dbo].[USP_GetCategory] 0,0,'',0,1,3
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcategory')
DROP PROCEDURE usp_getcategory
GO
CREATE PROCEDURE [dbo].[USP_GetCategory]                        
@numCatergoryId as numeric(9),                        
@byteMode as tinyint ,                  
@str as varchar(1000),                  
@numDomainID as numeric(9)=0,
@numSiteID AS NUMERIC(9)=0 ,
@numCurrentPage as numeric(9)=0,
@PageSize AS INT=20,
@numTotalPage as numeric(9) OUT,    
@numItemCode AS NUMERIC(9,0),
@SortChar CHAR(1) = '0',
@numCategoryProfileID NUMERIC(18,0)
               
as   

set nocount on                     
--selecting all categories                     
declare @strsql as varchar(8000)
DECLARE @strRowCount VARCHAR(8000)  


create table #TempCategory
(PKId numeric(9) identity,
numItemCode numeric(9),
vcItemName varchar(250));

                  
if @byteMode=0                        
BEGIN
	 ;WITH CategoryList
As 
( SELECT C1.numCategoryID,C1.vcCategoryName,C1.vcCategoryNameURL,C1.numDepCategory,C1.tintLevel,case when C1.bitLink=0 then 'No' when C1.bitLink=1 then 'Yes' end as Link,C1.vcLink as vcLink,CAST('' AS VARCHAR(1000)) as DepCategory,
	C1.intDisplayOrder,C1.vcDescription,
	CASE WHEN LEN(ISNULL(C1.vcDescription ,'')) > 0 THEN  
	CASE WHEN LEN(C1.vcDescription) > 40 THEN SUBSTRING(C1.vcDescription,0,40) + ' [...]' 
         ELSE C1.vcDescription + ' [...]'	END 
    ELSE
		 ISNULL(C1.vcDescription ,'')   
    END     
         AS vcShortDescription,1 AS [Level],
	CAST (REPLICATE('.',1) + C1.vcCategoryName as varchar(1000) )  as vcNewCategoryName,
	CAST(C1.numCategoryID AS VARCHAR(MAX)) AS [Order]
 FROM dbo.Category C1 WHERE C1.numDepCategory=0 AND  C1.numDomainID=@numDomainID AND C1.numCategoryProfileID = @numCategoryProfileID
 UNION ALL
 SELECT  C1.numCategoryID,C1.vcCategoryName,C1.vcCategoryNameURL,C1.numDepCategory,C1.tintLevel,case when C1.bitLink=0 then 'No' when C1.bitLink=1 then 'Yes' end as Link,C1.vcLink as vcLink,CL.vcCategoryName as DepCategory ,
	C1.intDisplayOrder,C1.vcDescription,
	CASE WHEN LEN(ISNULL(C1.vcDescription ,'')) > 0 THEN  
	CASE WHEN LEN(C1.vcDescription) > 40 THEN SUBSTRING(C1.vcDescription,0,40) + ' [...]' 
         ELSE C1.vcDescription + ' [...]'	END 
    ELSE
		 ISNULL(C1.vcDescription ,'')   
    END     
         AS vcShortDescription,CL.[Level] + 1 AS [Level],
	CAST (REPLICATE('.',[Level] * 3) + C1.vcCategoryName as varchar(1000) )  as vcNewCategoryName,
	[Order] + '.' + CAST(C1.numCategoryID AS VARCHAR(MAX)) AS [Order]
 FROM dbo.Category C1 
 INNER JOIN CategoryList CL
 ON CL.numCategoryID = C1.numDepCategory  WHERE  C1.numDomainID=@numDomainID  AND C1.numCategoryProfileID = @numCategoryProfileID
)   

SELECT *,(SELECT COUNT(*) FROM [SiteCategories] SC WHERE SC.[numSiteID] =@numSiteID AND SC.[numCategoryID]= C1.[numCategoryID]) AS ShowInECommerce,
ISNULL((SELECT COUNT(*) FROM [ItemCategory] WHERE [numCategoryID] = C1.[numCategoryID]),0) AS ItemCount FROM CategoryList C1 ORDER BY [Order]
end                                                
--deleting a category                      
else if @byteMode=1                        
begin     
	delete from SiteCategories where numCategoryID=@numCatergoryId                     
	delete from ItemCategory where numCategoryID=@numCatergoryId
	delete from Category where numCategoryID=@numCatergoryId       
	select 1                        
end                        
--selecting all catories where level is 1                       
else if @byteMode=2                      
begin                        
	select numCategoryID,vcCategoryName,vcCategoryNameURL,intDisplayOrder,vcDescription from Category                      
	where ((tintlevel=1 )  or (tintlevel<>1 and numDepCategory=@numCatergoryId))  and numCategoryID!=@numCatergoryId                      
	and numDomainID=@numDomainID                    
	ORDER BY [vcCategoryName]
end                      
--selecting subCategories                      
else if @byteMode=3                      
begin                        
	select numCategoryID,vcCategoryName,vcCategoryNameURL,intDisplayOrder,vcDescription  from Category                      
	where numDepCategory=@numCatergoryId                      
	ORDER BY [vcCategoryName]
end                      
                      
--selecting Items                      
if @byteMode=4                      
begin                        
	select numItemCode,vcItemName  from Item           
	where numDomainID=@numDomainID                       
end                      
                      
--selecting Item belongs to a category                      
else if @byteMode=5                      
begin    


	insert into #TempCategory
	                       
	select numItemCode,CONCAT(numItemCode,' - ',vcItemName) AS vcItemName  from Item                      
	join ItemCategory                       
	on numItemCode=numItemID                      
	where numCategoryID=@numCatergoryId           
	and numDomainID=@numDomainID                       
	ORDER BY [vcItemName]


	select @numTotalPage =COUNT(*) from #TempCategory

	if @numTotalPage<@PageSize 
		begin
			set @numTotalPage=1
		end
	else	
		begin
			set @numTotalPage=@numTotalPage/@PageSize
		end

	select * from #TempCategory  where PKId between (@numCurrentPage*@PageSize)- (@PageSize-1) and  @numCurrentPage*@PageSize

	select @numTotalPage

end                      
                    
else if @byteMode=6                    
begin                        

	select C1.numCategoryID,c1.vcCategoryName,((select Count(*) from Category C2 where C2.numDepCategory=C1.numCategoryID)+(select count(*) from ItemCategory C3 where C3.numCategoryID=C1.numCategoryID  ) ) as [Count],convert(integer,bitLink) as Type,vcLink,C1.intDisplayOrder,
	C1.vcDescription   
	from Category C1                     
	where tintlevel=1                     
	and numDomainID=@numDomainID   
	AND numCategoryProfileID=@numCategoryProfileID                   
                    
end                     
                    
else if @byteMode=7                    
begin                        

	select C1.numCategoryID,c1.vcCategoryName,((select Count(*) from Category C2 where C2.numDepCategory=C1.numCategoryID)+(select count(*) from ItemCategory C3 where C3.numCategoryID=C1.numCategoryID ) ) as [Count],convert(integer,bitLink) as Type,vcLink,C1.intDisplayOrder,
	C1.vcDescription,numDepCategory as Category
	from Category C1                     
	where C1.numDepCategory=@numCatergoryId and numDomainID=@numDomainID                      
	union                     
	select numItemCode,vcItemName,0 ,2,'',0,'',0  from Item                      
	join ItemCategory                       
	on numItemCode=numItemID                      
	where numCategoryID=@numCatergoryId  and numDomainID=@numDomainID                    
end                     
                
else if @byteMode=8                     
begin                    
     --kishan               
	set @strsql='select numItemCode,vcItemName,txtItemDesc,0,
	(
	SELECT TOP 1 II.vcPathForTImage 
    FROM dbo.ItemImages II 
        WHERE II.numItemCode = Item.numItemCode 
    AND II.bitDefault = 1 , AND II.bitIsImage = 1 
    AND II.numDomainID ='+ cast(@numDomainId as varchar(50))+') 
	vcPathForTImage ,
	1 as Type,monListPrice as price,0 as numQtyOnHand  from Item                    
	join ItemCategory                       
	on numItemCode=numItemID                      
	where numCategoryID in('+@str+')'                  
	exec (@strsql)                   
end                    
                  
else if @byteMode=9                  
begin                    
                    
	select distinct(numItemCode),vcItemName,txtItemDesc,0 , (SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numDomainId = @numDomainId AND bitDefault = 1 AND numItemCode = Item.numItemCode) AS vcPathForImage,1 as Type,monListPrice as price,0 as numQtyOnHand from Item                      
	join ItemCategory                       
	on numItemCode=numItemID                      
	where ( vcItemName like '%'+@str+'%' or txtItemDesc like '%'+@str+'%') and numDomainID= @numDomainID                                    
end        
      
else if @byteMode=10      
begin       
	select ROW_NUMBER() OVER (ORDER BY Category Asc) AS RowNumber,* from (select   C1.numCategoryID,c1.vcCategoryName,convert(integer,bitLink) as Type,vcLink ,numDepCategory as Category,C1.intDisplayOrder,
	C1.vcDescription         
	from Category C1                     
	where  numDomainID=@numDomainID)X       
      
end 


--selecting Items not present in Category                   
else if @byteMode=11                    
begin     

	insert into #TempCategory
	         
	SELECT I.numItemCode, CONCAT(I.numItemCode,' - ',I.vcItemName) AS vcItemName FROM item  I LEFT OUTER JOIN [ItemCategory] IC ON IC.numItemID = I.numItemCode
	WHERE [numDomainID] =@numDomainID AND ISNULL(IC.numCategoryID,0) <> @numCatergoryId
	ORDER BY [vcItemName]
          
--	select numItemCode,vcItemName  from Item LEFT OUTER JOIN [ItemCategory] ON Item.[numItemCode] = [ItemCategory].[numCategoryID]
--	where [ItemCategory].[numCategoryID] <> @numCatergoryId
----numItemCode not in (select numItemID from ItemCategory where numCategoryID=@numCatergoryId)
--	and numDomainID=@numDomainID                      
--	ORDER BY [vcItemName]



	select @numTotalPage =COUNT(*) from #TempCategory

	set @numTotalPage=@numTotalPage/@PageSize

	select * from #TempCategory  where PKId between (@numCurrentPage*@PageSize)-(@PageSize-1) and  @numCurrentPage*@PageSize

	select @numTotalPage


end   


--selecting Items From Item Groups                  
else if @byteMode=12                
begin   

	insert into #TempCategory
	                     
	select numItemCode,vcItemName  from Item 
	where numItemGroup= @numCatergoryId  
	ORDER BY [vcItemName] 



	select @numTotalPage =COUNT(*) from #TempCategory

	set @numTotalPage=@numTotalPage/@PageSize

	select * from #TempCategory  where PKId between (@numCurrentPage*@PageSize)-(@PageSize-1) and  @numCurrentPage*@PageSize

	select @numTotalPage

              
end

-- Selecting Categories Site Wise and Sorted into Sort Order 
else if @byteMode=13
BEGIN
	SELECT  
		@numCategoryProfileID=CPS.numCategoryProfileID
	FROM 
		Sites S
	JOIN
		CategoryProfileSites CPS
	ON
		S.numSiteID=CPS.numSiteID
	JOIN
		CategoryProfile CP
	ON
		CPS.numCategoryProfileID = CP.ID
	WHERE 
		S.numSiteID=@numSiteID


	DECLARE @bParentCatrgory BIT;
	SELECT @bParentCatrgory=vcAttributeValue FROM PageElementDetail WHERE [numSiteID] = @numSiteID AND numAttributeID=39
	SET @bParentCatrgory=ISNULL(@bParentCatrgory,0)

	DECLARE @tintDisplayCategory AS TINYINT
	SELECT @tintDisplayCategory = ISNULL(bitDisplayCategory,0) FROM dbo.eCommerceDTL WHERE numSiteID = @numSiteId

	
	IF ISNULL(@tintDisplayCategory,0) = 0 
	BEGIN
		SELECT C.numCategoryID,
			   C.vcCategoryName,
			   C.tintLevel,
			   convert(integer,C.bitLink) as TYPE,
			   C.vcLink AS vcLink,
			   numDepCategory as Category,C.intDisplayOrder,C.vcDescription 
		FROM   [SiteCategories] SC
			   INNER JOIN [Category] C ON SC.[numCategoryID] = C.[numCategoryID]
		WHERE  SC.[numSiteID] = @numSiteID
			   AND SC.[numCategoryID] = C.[numCategoryID] 
			   AND 1 = ( CASE WHEN @bParentCatrgory = 1 THEN tintLevel ELSE 1 END )
			   ORDER BY tintLevel , 
						ISNULL(C.intDisplayOrder,0),
						Category	
	END	
	ELSE
	BEGIN
		SELECT C.numCategoryID,
			   C.vcCategoryName,
			   C.tintLevel,
			   convert(integer,C.bitLink) as TYPE,
			   C.vcLink AS vcLink,
			   numDepCategory as Category,C.intDisplayOrder,C.vcDescription 
		FROM   [SiteCategories] SC
			   INNER JOIN [Category] C ON SC.[numCategoryID] = C.[numCategoryID]
		WHERE  SC.[numSiteID] = @numSiteID
			   AND SC.[numCategoryID] = C.[numCategoryID] 
			   AND 1 = ( CASE WHEN @bParentCatrgory = 1 THEN tintLevel ELSE 1 END )
			   AND (
					SC.numCategoryID IN (
										SELECT numCategoryID FROM dbo.ItemCategory 
										WHERE numItemID IN (
															SELECT numItemID FROM WareHouseItems WI
															INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID
															GROUP BY numItemID 
															HAVING SUM(WI.numOnHand) > 0
															UNION ALL
															SELECT numItemID FROM WareHouseItems WI
															INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID
															GROUP BY numItemID 
															HAVING SUM(WI.numAllocation) > 0
														   )
									   )
					OR tintLevel = 1
					)				   
			   ORDER BY tintLevel , 
						ISNULL(C.intDisplayOrder,0),
						Category
	END			

END

else if @byteMode=14
BEGIN

	SELECT C.numCategoryID,
		   C.vcCategoryName,
		   C.tintLevel,
		   convert(integer,C.bitLink) as TYPE,
		   C.vcLink AS vcLink,
		   numDepCategory as Category,C.intDisplayOrder,C.vcPathForCategoryImage,C.vcDescription 
	FROM   [SiteCategories] SC
		   INNER JOIN [Category] C
			 ON SC.[numCategoryID] = C.[numCategoryID]
	WHERE  SC.[numSiteID] = @numSiteID
		   AND SC.[numCategoryID] = C.[numCategoryID]
		   AND ISNULL(numDepCategory,0) = 0
		 ORDER BY ISNULL(C.intDisplayOrder,0),Category
END

else if @byteMode=15
BEGIN

DECLARE @bSubCatrgory BIT;

SELECT @bSubCatrgory=vcAttributeValue FROM PageElementDetail WHERE [numSiteID] = @numSiteID AND numAttributeID=40
SET @bSubCatrgory=ISNULL(@bSubCatrgory,0)

	SELECT C.numCategoryID,
		   C.vcCategoryName,
		   C.tintLevel,
		   convert(integer,C.bitLink) as TYPE,
		   C.vcLink AS vcLink,
		   numDepCategory as Category,C.intDisplayOrder,C.vcPathForCategoryImage,C.vcDescription 
		   
		   
	FROM   [SiteCategories] SC
		   INNER JOIN [Category] C
			 ON SC.[numCategoryID] = C.[numCategoryID]
	WHERE  SC.[numSiteID] = @numSiteID
		   AND SC.[numCategoryID] = C.[numCategoryID] AND numDepCategory=@numCatergoryId
		   AND 1=(CASE WHEN @bSubCatrgory=1 THEN 1 ELSE 0 END)
		    ORDER BY ISNULL(C.intDisplayOrder,0),Category
END

ELSE if @byteMode=16
BEGIN
 SELECT vcPathForCategoryImage FROM Category WHERE numCategoryID=@numCatergoryId AND numDomainID=@numDomainID
 
END
ELSE if @byteMode=17
BEGIN
 SELECT numCategoryID ,vcCategoryName, vcCategoryNameURL ,vcDescription,intDisplayOrder,vcPathForCategoryImage,numDepCategory,ISNULL(vcMetaTitle,'') vcMetaTitle,ISNULL(vcMetaKeywords,'') vcMetaKeywords,ISNULL(vcMetaDescription,'') vcMetaDescription FROM Category  WHERE numCategoryID = @numCatergoryId
END
ELSE IF @byteMode = 18
BEGIN
	SELECT
		*
	FROM
	(
		-- USED FOR HIERARCHICAL CATEGORY LIST
		SELECT 
			ROW_NUMBER() OVER ( ORDER BY vcCategoryName) AS RowNum,
			C1.numCategoryID,
			C1.vcCategoryName,
			C1.vcCategoryNameURL,
			NULLIF(numDepCategory,0) numDepCategory,
			ISNULL(numDepCategory,0) numDepCategory1,
			0 AS tintLevel,
			ISNULL((SELECT COUNT(*) FROM [ItemCategory] WHERE [numCategoryID] = C1.[numCategoryID]),0) AS ItemCount,
			ISNULL((SELECT COUNT(*) FROM [ItemCategory] WHERE [numCategoryID] IN (SELECT numCategoryID FROM category WHERE numDepCategory = C1.[numCategoryID])),0) AS ItemSubcategoryCount
		FROM 
			Category C1 
		WHERE 
			numDomainID =@numDomainID AND numCategoryProfileID = @numCategoryProfileID
	) TEMp
	ORDER BY
		vcCategoryName
END

ELSE IF @byteMode=19
BEGIN
	SELECT  
		@numCategoryProfileID=CPS.numCategoryProfileID
	FROM 
		Sites S
	JOIN
		CategoryProfileSites CPS
	ON
		S.numSiteID=CPS.numSiteID
	JOIN
		CategoryProfile CP
	ON
		CPS.numCategoryProfileID = CP.ID
	WHERE 
		S.numSiteID=@numSiteID

	BEGIN
	 ;WITH CategoryList
As 
( SELECT C1.numCategoryID,C1.vcCategoryName,C1.numDepCategory,C1.tintLevel,case when C1.bitLink=0 then 'No' when C1.bitLink=1 then 'Yes' end as Link,C1.vcLink as vcLink,CAST('' AS VARCHAR(1000)) as DepCategory,
	C1.intDisplayOrder,C1.vcDescription,
	CASE WHEN LEN(ISNULL(C1.vcDescription ,'')) > 0 THEN  
	CASE WHEN LEN(C1.vcDescription) > 40 THEN SUBSTRING(C1.vcDescription,0,40) + ' [...]' 
         ELSE C1.vcDescription + ' [...]'	END 
    ELSE
		 ISNULL(C1.vcDescription ,'')   
    END     
         AS vcShortDescription,1 AS [Level],
	CAST (REPLICATE('.',1) + C1.vcCategoryName as varchar(1000) )  as vcNewCategoryName,
	CAST(C1.numCategoryID AS VARCHAR(MAX)) AS [Order]
 FROM dbo.Category C1 WHERE C1.numDepCategory=0 AND  C1.numDomainID=@numDomainID AND C1.numCategoryProfileID=@numCategoryProfileID
 UNION ALL
 SELECT  C1.numCategoryID,C1.vcCategoryName,C1.numDepCategory,C1.tintLevel,case when C1.bitLink=0 then 'No' when C1.bitLink=1 then 'Yes' end as Link,C1.vcLink as vcLink,CL.vcCategoryName as DepCategory ,
	C1.intDisplayOrder,C1.vcDescription,
	CASE WHEN LEN(ISNULL(C1.vcDescription ,'')) > 0 THEN  
	CASE WHEN LEN(C1.vcDescription) > 40 THEN SUBSTRING(C1.vcDescription,0,40) + ' [...]' 
         ELSE C1.vcDescription + ' [...]'	END 
    ELSE
		 ISNULL(C1.vcDescription ,'')   
    END     
         AS vcShortDescription,CL.[Level] + 1 AS [Level],
	CAST (REPLICATE('.',[Level] * 3) + C1.vcCategoryName as varchar(1000) )  as vcNewCategoryName,
	[Order] + '.' + CAST(C1.numCategoryID AS VARCHAR(MAX)) AS [Order]
 FROM dbo.Category C1 
 INNER JOIN CategoryList CL
 ON CL.numCategoryID = C1.numDepCategory  WHERE  C1.numDomainID=@numDomainID AND C1.numCategoryProfileID=@numCategoryProfileID
)   

SELECT numCategoryID,vcCategoryName,(CASE WHEN  numDepCategory=0 THEN NULL ELSE numDepCategory END) AS numDepCategory ,tintLevel,Link,vcLink,DepCategory,(SELECT COUNT(*) FROM [SiteCategories] SC WHERE SC.[numSiteID] =@numSiteID AND SC.[numCategoryID]= C1.[numCategoryID]) AS ShowInECommerce,
ISNULL((SELECT COUNT(*) FROM [ItemCategory] WHERE [numCategoryID] = C1.[numCategoryID]),0) AS ItemCount FROM CategoryList C1 ORDER BY [Order]
END


DROP TABLE #TempCategory
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetGrossProfitEstimate')
DROP PROCEDURE USP_GetGrossProfitEstimate
GO
CREATE PROCEDURE [dbo].[USP_GetGrossProfitEstimate] 
( 
@numDomainID as numeric(9)=0,    
@numOppID AS NUMERIC(9)=0
)
AS
BEGIN
	DECLARE @avgCost int
	SET @avgCost =(select TOP 1 numCost from Domain where numDOmainId=@numDomainID)

	SELECT 
		numOppId
		,vcPOppName
		,vcItemName
		,monAverageCost
		,vcVendor
		,ISNULL(monTotAmount,0)/ISNULL(NULLIF(numUnitHour,0),1) AS monPrice
		,VendorCost
		,numUnitHour,
		ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * (CASE @avgCost WHEN 3 THEN VendorCost * dbo.fn_UOMConversion(numBaseUnit,numItemCode,@numDomainID,numPurchaseUnit) WHEN 2 THEN numCost * dbo.fn_UOMConversion(numBaseUnit,numItemCode,@numDomainID,numUOMId) ELSE monAverageCost END)) AS Profit,
		(((ISNULL(monTotAmount,0)/ISNULL(NULLIF(numUnitHour,0),1)) - (CASE @avgCost WHEN 3 THEN VendorCost * dbo.fn_UOMConversion(numBaseUnit,numItemCode,@numDomainID,numPurchaseUnit) WHEN 2 THEN numCost * dbo.fn_UOMConversion(numBaseUnit,numItemCode,@numDomainID,numUOMId) ELSE monAverageCost END)) / (CASE WHEN (ISNULL(monTotAmount,0)/ISNULL(NULLIF(numUnitHour,0),1))=0 then 1 ELSE (ISNULL(monTotAmount,0)/ISNULL(NULLIF(numUnitHour,0),1)) end)) * 100 ProfitPer
		,bitItemPriceApprovalRequired
	FROM
	(
		SELECT 
			opp.numOppId
			,Opp.vcPOppName
			,OppI.vcItemName
			,(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) monAverageCost,
			V.numVendorID AS numVendorID
			,dbo.fn_getcomapnyname(V.numVendorID) as vcVendor
			,oppI.numUnitHour
			,ISNULL(OPPI.monPrice,0) AS monPrice
			,ISNULL(OPPI.monTotAmount,0) AS monTotAmount
			,ISNULL(V.monCost,0) VendorCost
			,ISNULL(OppI.bitItemPriceApprovalRequired,0) AS bitItemPriceApprovalRequired
			,ISNULL(OppI.numCost,0) numCost
			,I.numItemCode
			,numBaseUnit
			,numPurchaseUnit
			,ISNULL(OppI.numUOMId,numBaseUnit) AS numUOMId
		FROM 
			OpportunityMaster Opp 
		INNER JOIN 
			OpportunityItems OppI 
		ON 
			opp.numOppId=OppI.numOppId 
		INNER JOIN 
			Item I 
		ON 
			OppI.numItemCode=i.numItemcode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE 
			Opp.numDomainId=@numDomainID 
			AND opp.numOppId=@numOppID
			AND ISNULL(I.bitContainer,0) = 0
	) temp
END
/****** Object:  StoredProcedure [dbo].[usp_GetMasterListDetailsDynamicForm]    Script Date: 07/26/2008 16:17:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Tapan Nag                                                    
--Purpose: Returns the available form fields from the database                                                        
--Created Date: 07/09/2005                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getmasterlistdetailsdynamicform')
DROP PROCEDURE usp_getmasterlistdetailsdynamicform
GO
CREATE PROCEDURE [dbo].[usp_GetMasterListDetailsDynamicForm]                      
 @numListID NUMERIC(9),                        
 @vcItemType CHAR(3),                      
 @numDomainID NUMERIC(9)                      
AS     
 
 IF @vcItemType = 'SYS'
	BEGIN
		SELECT 0 AS numItemID,'Lead' AS vcItemName
		UNION ALL
		SELECT 1 AS numItemID,'Prospect' AS vcItemName
		UNION ALL
		SELECT 2 AS numItemID,'Account' AS vcItemName
	END
	
 IF @vcItemType='LI' AND @numListID=9 --Opportunity Source
	 BEGIN
		EXEC USP_GetOpportunitySource @numDomainID
	 END                 
 ELSE IF @vcItemType = 'LI'    --Master List                    
	BEGIN                        
		SELECT LD.vcData AS vcItemName, LD.numListItemID AS numItemID, 'L' As vcItemType, LD.constFlag As flagConst 
		FROM ListDetails LD left join listorder LO on Ld.numListItemID= LO.numListItemID and Lo.numDomainId = @numDomainID  
		WHERE LD.numListID = @numListID AND (LD.numDomainID = @numDomainID OR LD.constFlag = 1)
		order by ISNULL(intSortOrder,LD.sintOrder)                        
	END      
	ELSE IF @vcItemType = 'L' AND ISNULL(@numListID,0) > 0    --Master List                    
	BEGIN                        
		SELECT LD.vcData AS vcItemName, LD.numListItemID AS numItemID, 'L' As vcItemType, LD.constFlag As flagConst 
		FROM ListDetails LD left join listorder LO on Ld.numListItemID= LO.numListItemID and Lo.numDomainId = @numDomainID  
		WHERE LD.numListID = @numListID AND (LD.numDomainID = @numDomainID OR LD.constFlag = 1)
		order by ISNULL(intSortOrder,LD.sintOrder)                        
	END                                          
 ELSE IF @vcItemType = 'T'    --Territories                    
	BEGIN    
		SELECT vcData AS vcItemName, numListItemID AS numItemID, 'T' As vcItemType, 0 As flagConst FROM ListDetails WHERE numListID = 78 AND numDomainID = @numDomainID                                        
	END                        
 ELSE IF @vcItemType = 'AG'   --Lead Groups                     
	BEGIN                        
		SELECT vcGrpName AS vcItemName, numGrpId AS numItemID, 'G' As vcItemType, 0 As flagConst FROM Groups                     
	END                         
 ELSE IF @vcItemType = 'S'    --States                    
	BEGIN                        
		SELECT vcState AS vcItemName, numStateID AS numItemID, 'S' As vcItemType, numCountryID As flagConst FROM State WHERE (numDomainID = @numDomainID OR constFlag = 1) AND 1 = 2                    
	END         
 ELSE IF @vcItemType = 'U'    --Users                    
	BEGIN                        
		SELECT A.numContactID AS numItemID,A.vcFirstName+' '+A.vcLastName AS vcItemName              
		from UserMaster UM             
		join AdditionalContactsInformation A            
		on UM.numUserDetailId=A.numContactID              
		where UM.numDomainID=@numDomainID        
		union        
		select  A.numContactID,vcCompanyName+' - '+A.vcFirstName+' '+A.vcLastName        
		from AdditionalContactsInformation A         
		join DivisionMaster D        
		on D.numDivisionID=A.numDivisionID        
		join ExtarnetAccounts E         
		on E.numDivisionID=D.numDivisionID        
		join ExtranetAccountsDtl DTL        
		on DTL.numExtranetID=E.numExtranetID        
		join CompanyInfo C        
		on C.numCompanyID=D.numCompanyID        
		where A.numDomainID=@numDomainID and bitPartnerAccess=1        
		and D.numDivisionID <>(select numDivisionID from domain where numDomainID=@numDomainID)                      
	END
  ELSE IF @vcItemType = 'C'    --Organization Campaign                    
	BEGIN
	   SELECT  numCampaignID AS numItemID,
                vcCampaignName + CASE bitIsOnline WHEN 1 THEN ' (Online)' ELSE ' (Offline)' END AS vcItemName
       FROM    CampaignMaster
       WHERE   numDomainID = @numDomainID
	END               
  ELSE IF @vcItemType = 'IG' 
	BEGIN
		SELECT vcItemGroup AS vcItemName, numItemGroupID AS numItemID, 'I' As vcItemType, 0 As flagConst FROM dbo.ItemGroups WHERE numDomainID=@numDomainID   	
	END
 ELSE IF @vcItemType = 'PP' 
   BEGIN
		SELECT 'Inventory Item' AS vcItemName, 'P' AS numItemID, 'P' As vcItemType, 0 As flagConst 
		UNION 
		SELECT 'Non-Inventory Item' AS vcItemName, 'N' AS numItemID, 'P' As vcItemType, 0 As flagConst 
		UNION 
		SELECT 'Service' AS vcItemName, 'S' AS numItemID, 'P' As vcItemType, 0 As flagConst 
   END
 ELSE IF @vcItemType = 'V' 
   BEGIN
		SELECT DISTINCT ISNULL(C.vcCompanyName,'') AS vcItemName,  numVendorID AS numItemID ,'V' As vcItemType, 0 As flagConst   FROM dbo.Vendor V INNER JOIN dbo.DivisionMaster DM ON DM.numDivisionID= V.numVendorID 
		INNER JOIN dbo.CompanyInfo C ON C.numCompanyId = DM.numCompanyID WHERE V.numDomainID =@numDomainID
   END
 ELSE IF @vcItemType = 'OC' 
   BEGIN
		SELECT DISTINCT C.vcCurrencyDesc AS vcItemName,C.numCurrencyID AS numItemID FROM dbo.OpportunityMaster OM INNER JOIN dbo.Currency C ON C.numCurrencyID = OM.numCurrencyID
		WHERE OM.numDomainId=@numDomainID
   END
 ELSE  IF @vcItemType = 'UOM'    --Organization Campaign                    
	BEGIN
	   
	   
	   --SELECT  numUOMId AS numItemID,
    --            vcUnitName AS vcItemName
    --   FROM    UOM
    --   WHERE   numDomainID = @numDomainID

	--Modified by Sachin Sadhu ||Date:7thAug2014
	--Purpose :Boneta facing problem in updating units 
	   
			SELECT u.numUOMId as numItemID , u.vcUnitName as vcItemName FROM 
			UOM u INNER JOIN Domain d ON u.numDomainID=d.numDomainID
			WHERE u.numDomainID=@numDomainID AND d.numDomainID=@numDomainID AND 
			u.tintUnitType=(CASE WHEN ISNULL(d.charUnitSystem,'E')='E' THEN 1 WHEN d.charUnitSystem='M' THEN 2 END)


    END   
 ELSE IF @vcItemType = 'O'    --Opp Type                   
	 BEGIN
		SELECT  1 AS numItemID,'Sales' AS vcItemName
		UNion ALL
		SELECT  2 AS numItemID,'Purchase' AS vcItemName
	 END  
 ELSE  IF @vcItemType = 'CHK'    --Opp Type                   
	BEGIN
		SELECT  1 AS numItemID,'Yes' AS vcItemName
		UNion ALL
		SELECT  0 AS numItemID,'No' AS vcItemName
	END  
 ELSE IF @vcItemType='COA'
	BEGIN
		SELECT  C.[numAccountId] AS numItemID,ISNULL(C.[vcAccountName],'') AS vcItemName
		FROM    [Chart_Of_Accounts] C
		INNER JOIN [AccountTypeDetail] ATD 
		ON C.[numParntAcntTypeId] = ATD.[numAccountTypeID]
		WHERE   C.[numDomainId] = @numDomainId
		ORDER BY C.[vcAccountCode]
	END
       
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Import_SaveContactDetails')
DROP PROCEDURE USP_Import_SaveContactDetails
GO
CREATE PROCEDURE [dbo].[USP_Import_SaveContactDetails]  
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@numContactID NUMERIC(18,0)
	,@vcFirstName VARCHAR(50)
	,@vcLastName VARCHAR(50)
	,@vcEmail VARCHAR(80)
	,@numContactType NUMERIC(18,0)
	,@numPhone VARCHAR(15)
	,@numPhoneExtension VARCHAR(7)
	,@numCell VARCHAR(15)
	,@numHomePhone VARCHAR(15)
	,@vcFax VARCHAR(15)
	,@vcCategory NUMERIC(18,0)
	,@numTeam NUMERIC(18,0)
	,@vcPosition NUMERIC(18,0)
	,@vcTitle VARCHAR(100)
	,@vcDepartment NUMERIC(18,0)
	,@txtNotes NVARCHAR(MAX)
	,@bitIsPrimaryContact BIT
	,@vcStreet VARCHAR(100)
	,@vcCity VARCHAR(50)
	,@numState NUMERIC(18,0)
	,@numCountry NUMERIC(18,0)
	,@vcPostalCode VARCHAR(15)
AS
BEGIN
	-- IMPORTANT: PLEASE MAKE SURE NEW FIELD WHICH YOUR ARE ADDING IN STORE PROCEDURE FOR UPDATE ARE ADDED IN SELECT IN PROCEDURE USP_Import_GetContactDetails

	IF LEN(ISNULL(@vcFirstName,'')) = 0
	BEGIN
		SET @vcFirstName = '-'
	END

	IF LEN(ISNULL(@vcLastName,'')) = 0
	BEGIN
		SET @vcLastName = '-'
	END

	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		IF ISNULL(@numContactID,0) = 0
		BEGIN
			IF(SELECT COUNT(numContactId) FROM AdditionalContactsInformation WHERE numDivisionId=@numDivisionID)=0
			BEGIN
				SET @bitIsPrimaryContact = 1
			END

			IF ISNULL(@numContactType,0) = 0
			BEGIN
				SET @numContactType = 70
			END

			INSERT into AdditionalContactsInformation                                    
			(                  
				numContactType,                  
				vcDepartment,                  
				vcCategory,                  
				vcGivenName,                  
				vcFirstName,                  
				vcLastName,                   
				numDivisionId,                  
				numPhone,                  
				numPhoneExtension,                  
				numCell,                  
				numHomePhone,                          
				vcFax,                  
				vcEmail,                
				vcPosition,                  
				txtNotes,                  
				numCreatedBy,                                    
				bintCreatedDate,             
				numModifiedBy,            
				bintModifiedDate,                   
				numDomainID,               
				numRecOwner,                  
				numTeam,                  
				numEmpStatus,
				vcTitle
				,bitPrimaryContact     
			)                                    
			VALUES                                    
			(                  
				@numContactType,                  
				@vcDepartment,                  
				@vcCategory,                  
				CONCAT(@vcLastName,', ',@vcFirstName,'.eml'),                  
				@vcFirstName ,                  
				@vcLastName,                   
				@numDivisionId ,                  
				@numPhone ,                                    
				@numPhoneExtension,                  
				@numCell ,                  
				@NumHomePhone ,                  
				@vcFax ,                  
				@vcEmail ,                  
				@vcPosition,                                   
				@txtNotes,                  
				@numUserCntID,                  
				GETUTCDATE(),             
				@numUserCntID,                  
				GETUTCDATE(),                   
				@numDomainID,                  
				@numUserCntID,                   
				@numTeam,                  
				658,
				@vcTitle
				,@bitIsPrimaryContact                
			)                                    
                     
			SET @numContactID= SCOPE_IDENTITY()


			DECLARE @bitAutoPopulateAddress BIT 
			DECLARE @tintPoulateAddressTo TINYINT 

			SELECT 
				@bitAutoPopulateAddress = ISNULL(bitAutoPopulateAddress,'0')
				,@tintPoulateAddressTo = ISNULL(tintPoulateAddressTo,'0') 
			FROM 
				Domain 
			WHERE 
				numDomainId = @numDomainId
			
			IF (@bitAutoPopulateAddress = 1)
			BEGIN
				IF(@tintPoulateAddressTo = 1) --if primary address is not specified then getting billing address    
				BEGIN
					SELECT                   
						@vcStreet = AD1.vcStreet,
						@vcCity= AD1.vcCity,
						@numState= AD1.numState,
						@vcPostalCode= AD1.vcPostalCode,
						@numCountry= AD1.numCountry              
					FROM 
						DivisionMaster  DM              
					LEFT JOIN 
						dbo.AddressDetails AD1 
					ON 
						AD1.numDomainID=DM.numDomainID 
						AND AD1.numRecordID=DM.numDivisionID 
						AND AD1.tintAddressOf=2 
						AND AD1.tintAddressType=1 
						AND AD1.bitIsPrimary=1
					WHERE 
						numDivisionID=@numDivisionId    
				END  
				ELSE IF (@tintPoulateAddressTo = 2)-- Primary Address is Shipping Address
				BEGIN
					SELECT                   
						@vcStreet= AD2.vcStreet,
						@vcCity=AD2.vcCity,
						@numState=AD2.numState,
						@vcPostalCode=AD2.vcPostalCode,
						@numCountry=AD2.numCountry                   
					FROM 
						DivisionMaster DM
					LEFT JOIN 
						dbo.AddressDetails AD2 
					ON 
						AD2.numDomainID=DM.numDomainID 
						AND AD2.numRecordID= DM.numDivisionID 
						AND AD2.tintAddressOf=2 
						AND AD2.tintAddressType=2 
						AND AD2.bitIsPrimary=1
					WHERE 
						numDivisionID=@numDivisionId
				END  
			END

			INSERT INTO dbo.AddressDetails 
			(
	 			vcAddressName,
	 			vcStreet,
	 			vcCity,
	 			vcPostalCode,
	 			numState,
	 			numCountry,
	 			bitIsPrimary,
	 			tintAddressOf,
	 			tintAddressType,
	 			numRecordID,
	 			numDomainID
			) 
			VALUES
			(
				'Primary'
				,@vcStreet
				,@vcCity
				,@vcPostalCode
				,@numState
				,@numCountry
				,1
				,1
				,0
				,@numContactID
				,@numDomainID
			)                  
		END
		ELSE
		BEGIN
			UPDATE 
				AdditionalContactsInformation 
			SET                                    
				numContactType=@numContactType,                                    
				vcGivenName=CONCAT(@vcLastName,', ',@vcFirstName,'.eml'),
				vcFirstName=@vcFirstName,
				vcLastName =@vcLastName,
				numDivisionId =@numDivisionId,
				numPhone=@numPhone,
				numPhoneExtension=@numPhoneExtension,
				numCell =@numCell,
				NumHomePhone =@NumHomePhone,
				vcFax=@vcFax,
				vcEmail=@vcEmail,
				vcPosition=@vcPosition,
				txtNotes=@txtNotes,
				numModifiedBy=@numUserCntID,
				bintModifiedDate=GETUTCDATE(),
				bitPrimaryContact=@bitIsPrimaryContact
			WHERE 
				numContactId=@numContactId
		END

		DECLARE @numCount AS NUMERIC(18,0) = 0

		IF ISNULL(@vcEmail,'') <> ''
		BEGIN 
			SELECT @numCount = COUNT(*) FROM dbo.EmailMaster WHERE vcEmailID =@vcEmail AND numDomainId=@numDomainID

			IF @numCount = 0 
			BEGIN
				INSERT INTO EmailMaster(vcEmailId,vcName,numDomainId,numContactId)
				VALUES(@vcEmail,@vcFirstName,@numDomainID,@numcontactId)
			END
		END 
		ELSE
		BEGIN
			UPDATE EmailMaster set numContactId=@numcontactId WHERE vcEmailID=@vcEmail and numDomainId=@numDomainID
		END
	END

	SELECT ISNULL(@numContactID,0) AS numContactID             
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Import_SaveItemDetails')
DROP PROCEDURE USP_Import_SaveItemDetails
GO
CREATE PROCEDURE [dbo].[USP_Import_SaveItemDetails]  
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0) OUTPUT
	,@numShipClass NUMERIC(18,0)
	,@vcItemName VARCHAR(300)
	,@monListPrice MONEY
	,@txtItemDesc VARCHAR(1000)
	,@vcModelID VARCHAR(200)
	,@vcManufacturer VARCHAR(250)
	,@numBarCodeId VARCHAR(50)
	,@fltWidth FLOAT
	,@fltHeight FLOAT
	,@fltWeight FLOAT
	,@fltLength FLOAT
	,@monAverageCost MONEY
	,@bitSerialized BIT
	,@bitKitParent BIT
	,@bitAssembly BIT
	,@IsArchieve BIT
	,@bitLotNo BIT
	,@bitAllowBackOrder BIT
	,@numIncomeChartAcntId NUMERIC(18,0)
	,@numAssetChartAcntId NUMERIC(18,0)
	,@numCOGsChartAcntId NUMERIC(18,0)
	,@numItemClassification NUMERIC(18,0)
	,@numItemGroup NUMERIC(18,0)
	,@numPurchaseUnit NUMERIC(18,0)
	,@numSaleUnit NUMERIC(18,0)
	,@numItemClass NUMERIC(18,0)
	,@numBaseUnit NUMERIC(18,0)
	,@vcSKU VARCHAR(50)
	,@charItemType CHAR(1)
	,@bitTaxable BIT
	,@txtDesc NVARCHAR(MAX)
	,@vcExportToAPI VARCHAR(50)
	,@bitAllowDropShip BIT
	,@bitMatrix BIT
	,@vcItemAttributes VARCHAR(2000)
	,@vcCategories VARCHAR(1000)
	,@txtShortDesc NVARCHAR(MAX)
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	-- IMPORTANT: PLEASE MAKE SURE NEW FIELD WHICH YOUR ARE ADDING IN STORE PROCEDURE FOR UPDATE ARE ADDED IN SELECT IN PROCEDURE USP_Import_GetItemDetails

	IF @numAssetChartAcntId = 0
	BEGIN
		SET @numAssetChartAcntId =NULL
	END

	DECLARE @bitAttributesChanged AS BIT  = 1
	DECLARE @bitOppOrderExists AS BIT = 0
	DECLARE @hDoc AS INT     
	                                                                        	
	DECLARE @TempTable TABLE 
	(
		ID INT IDENTITY(1,1),
		Fld_ID NUMERIC(18,0),
		Fld_Value NUMERIC(18,0)
	)   

	IF ISNULL(@bitMatrix,0) = 1 AND ISNULL(@numItemGroup,0) = 0
	BEGIN
		RAISERROR('ITEM_GROUP_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) = 0 AND ISNULL(@numItemCode,0) = 0
	BEGIN
		RAISERROR('ITEM_ATTRIBUTES_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) > 0
	BEGIN
		DECLARE @bitDuplicate AS BIT = 0
	
		IF DATALENGTH(ISNULL(@vcItemAttributes,''))>2
		BEGIN
			EXEC sp_xml_preparedocument @hDoc OUTPUT, @vcItemAttributes
	                                      
			INSERT INTO @TempTable 
			(
				Fld_ID,
				Fld_Value
			)    
			SELECT
				*          
			FROM 
				OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
			WITH 
				(Fld_ID NUMERIC(18,0),Fld_Value NUMERIC(18,0)) 
			ORDER BY 
				Fld_ID       

			DECLARE @strAttribute VARCHAR(500) = ''
			DECLARE @i AS INT = 1
			DECLARE @COUNT AS INT 
			DECLARE @numFldID AS NUMERIC(18,0)
			DECLARE @numFldValue AS NUMERIC(18,0)
			SELECT @COUNT = COUNT(*) FROM @TempTable

			WHILE @i <= @COUNT
			BEGIN
				SELECT @numFldID=Fld_ID,@numFldValue=Fld_Value FROM @TempTable WHERE ID=@i

				IF ISNUMERIC(@numFldValue)=1
				BEGIN
					SELECT @strAttribute=@strAttribute+Fld_label FROM CFW_Fld_Master WHERE Fld_id=@numFldID AND Grp_id = 9
                
					IF LEN(@strAttribute) > 0
					BEGIN
						SELECT @strAttribute=@strAttribute+':'+ vcData +',' FROM ListDetails WHERE numListItemID=@numFldValue
					END
				END

				SET @i = @i + 1
			END

			SET @strAttribute = SUBSTRING(@strAttribute,0,LEN(@strAttribute))

			IF
				(SELECT
					COUNT(*)
				FROM
					Item
				INNER JOIN
					ItemAttributes
				ON
					Item.numItemCode = ItemAttributes.numItemCode
					AND ItemAttributes.numDomainID = @numDomainID                     
				WHERE
					Item.numItemCode <> @numItemCode
					AND Item.vcItemName = @vcItemName
					AND Item.numItemGroup = @numItemGroup
					AND Item.numDomainID = @numDomainID
					AND dbo.fn_GetItemAttributes(@numDomainID,Item.numItemCode) = @strAttribute
				) > 0
			BEGIN
				RAISERROR('ITEM_WITH_ATTRIBUTES_ALREADY_EXISTS',16,1)
				RETURN
			END

			If ISNULL(@numItemCode,0) > 0 AND dbo.fn_GetItemAttributes(@numDomainID,@numItemCode) = @strAttribute
			BEGIN
				SET @bitAttributesChanged = 0
			END

			EXEC sp_xml_removedocument @hDoc 
		END	
	END
	ELSE IF ISNULL(@bitMatrix,0) = 0
	BEGIN
		-- REMOVE ATTRIBUTES CONFIGURATION OF ITEM AND WAREHOUSE
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode
	END 


	IF ISNULL(@numItemGroup,0) > 0
	BEGIN
		IF (SELECT COUNT(*) FROM ItemGroupsDTL WHERE numItemGroupID = @numItemGroup AND tintType=2) = 0
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
		ELSE IF EXISTS (SELECT 
							CFM.Fld_id
						FROM 
							CFW_Fld_Master CFM
						LEFT JOIN
							ListDetails LD
						ON
							CFM.numlistid = LD.numListID
						WHERE
							CFM.numDomainID = @numDomainID
							AND CFM.Fld_id IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID=@numItemGroup AND tintType = 2)
						GROUP BY
							CFM.Fld_id HAVING COUNT(LD.numListItemID) = 0)
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
	END


	IF ISNULL(@numItemCode,0) = 0
	BEGIN
		IF (ISNULL(@vcItemName,'') = '')
		BEGIN
			RAISERROR('ITEM_NAME_NOT_SELECTED',16,1)
			RETURN
		END
        ELSE IF (ISNULL(@charItemType,'') = '' OR ISNULL(@charItemType,'') = '0' )
		BEGIN
			RAISERROR('ITEM_TYPE_NOT_SELECTED',16,1)
			RETURN
		END
        ELSE
		BEGIN 
		    If (@charItemType = 'P')
			BEGIN
				IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numAssetChartAcntId) = 0)
				BEGIN
					RAISERROR('INVALID_ASSET_ACCOUNT',16,1)
					RETURN
				END
			END
			ELSE IF @numAssetChartAcntId = 0
			BEGIN
				SET @numAssetChartAcntId = NULL
			END

			-- This is common check for CharItemType P and Other
			IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numCOGSChartAcntId) = 0)
			BEGIN
				RAISERROR('INVALID_COGS_ACCOUNT',16,1)
				RETURN
			END

			IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numIncomeChartAcntId) = 0)
			BEGIN
				RAISERROR('INVALID_INCOME_ACCOUNT',16,1)
				RETURN
			END
		END


		INSERT INTO Item
		(
			numDomainID
			,numCreatedBy
			,bintCreatedDate
			,bintModifiedDate
			,numModifiedBy
			,numShipClass
			,vcItemName
			,monListPrice
			,txtItemDesc
			,vcModelID
			,vcManufacturer
			,numBarCodeId
			,fltWidth
			,fltHeight
			,fltWeight
			,fltLength
			,monAverageCost
			,bitSerialized
			,bitKitParent
			,bitAssembly
			,IsArchieve
			,bitLotNo
			,bitAllowBackOrder
			,numIncomeChartAcntId
			,numAssetChartAcntId
			,numCOGsChartAcntId
			,numItemClassification
			,numItemGroup
			,numPurchaseUnit
			,numSaleUnit
			,numItemClass
			,numBaseUnit
			,vcSKU
			,charItemType
			,bitTaxable
			,vcExportToAPI
			,bitAllowDropShip
			,bitMatrix
			,numManufacturer
		)
		VALUES
		(
			@numDomainID
			,@numUserCntID
			,GETUTCDATE()
			,GETUTCDATE()
			,@numUserCntID
			,@numShipClass
			,@vcItemName
			,@monListPrice
			,@txtItemDesc
			,@vcModelID
			,@vcManufacturer
			,@numBarCodeId
			,@fltWidth
			,@fltHeight
			,@fltWeight
			,@fltLength
			,@monAverageCost
			,@bitSerialized
			,@bitKitParent
			,@bitAssembly
			,@IsArchieve
			,@bitLotNo
			,@bitAllowBackOrder
			,@numIncomeChartAcntId
			,@numAssetChartAcntId
			,@numCOGsChartAcntId
			,@numItemClassification
			,@numItemGroup 
			,@numPurchaseUnit 
			,@numSaleUnit
			,@numItemClass
			,@numBaseUnit
			,@vcSKU
			,@charItemType
			,@bitTaxable
			,@vcExportToAPI
			,@bitAllowDropShip
			,@bitMatrix
			,(CASE WHEN LEN(ISNULL(@vcManufacturer,'')) > 0 THEN ISNULL((SELECT TOP 1 numDivisionID FROM CompanyInfo INNER JOIN DivisionMaster ON CompanyInfo.numCompanyId=DivisionMaster.numCompanyID WHERE CompanyInfo.numDOmainID=@numDomainID AND LOWER(vcCompanyName) = LOWER(@vcManufacturer)),0) ELSE 0 END)
		)

		SET @numItemCode = SCOPE_IDENTITY()

		INSERT INTO ItemExtendedDetails (numItemCode,txtDesc,txtShortDesc) VALUES (@numItemCode,@txtDesc,@txtShortDesc)

		IF @charItemType = 'P' AND 1 = (CASE WHEN ISNULL(@numItemGroup,0) > 0 THEN (CASE WHEN ISNULL(@bitMatrix,0)=1 THEN 1 ELSE 0 END) ELSE 1 END)
		BEGIN
			
			DECLARE @TEMPWarehouse TABLE
			(
				ID INT IDENTITY(1,1)
				,numWarehouseID NUMERIC(18,0)
			)
		
			INSERT INTO @TEMPWarehouse (numWarehouseID) SELECT numWareHouseID FROM Warehouses WHERE numDomainID=@numDomainID

			DECLARE @j AS INT = 1
			DECLARE @jCount AS INT
			DECLARE @numTempWarehouseID NUMERIC(18,0)
			SELECT @jCount = COUNT(*) FROM @TEMPWarehouse

			WHILE @j <= @jCount
			BEGIN
				SELECT @numTempWarehouseID=numWarehouseID FROM @TEMPWarehouse WHERE ID=@j

				EXEC USP_WarehouseItems_Save @numDomainID,
											@numUserCntID,
											0,
											@numTempWarehouseID,
											0,
											@numItemCode,
											'',
											@monListPrice,
											0,
											0,
											'',
											'',
											'',
											'',
											0
 
				SET @j = @j + 1
			END
		END 
	END
	ELSE IF ISNULL(@numItemCode,0) > 0 AND EXISTS (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode)
	BEGIN
		IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END

		DECLARE @OldGroupID NUMERIC 
		SELECT @OldGroupID = ISNULL(numItemGroup,0) FROM [Item] WHERE numitemcode = @numItemCode AND [numDomainId] = @numDomainID
	
		DECLARE @monOldAverageCost AS DECIMAL(18,4);SET @monOldAverageCost=0
		SELECT @monOldAverageCost=(CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE numItemCode =@numItemCode AND [numDomainId] = @numDomainID
	   
		UPDATE 
			Item 
		SET 
			vcItemName=@vcItemName,txtItemDesc=@txtItemDesc,charItemType=@charItemType,monListPrice= @monListPrice,numItemClassification=@numItemClassification,                                             
			bitTaxable=@bitTaxable,vcSKU = @vcSKU,bitKitParent=@bitKitParent,numDomainID=@numDomainID,bintModifiedDate=getutcdate(),numModifiedBy=@numUserCntID
			,bitSerialized=@bitSerialized,vcModelID=@vcModelID,numItemGroup=@numItemGroup,numCOGsChartAcntId=@numCOGsChartAcntId,numAssetChartAcntId=@numAssetChartAcntId,                                      
			numIncomeChartAcntId=@numIncomeChartAcntId,fltWeight=@fltWeight,fltHeight=@fltHeight,fltWidth=@fltWidth,     
			fltLength=@fltLength,bitAllowBackOrder=@bitAllowBackOrder,bitAssembly=@bitAssembly,numBarCodeId=@numBarCodeId,
			vcManufacturer=@vcManufacturer,numBaseUnit=@numBaseUnit,numPurchaseUnit=@numPurchaseUnit,numSaleUnit=@numSaleUnit,bitLotNo=@bitLotNo,
			IsArchieve = @IsArchieve,numItemClass=@numItemClass,vcExportToAPI=@vcExportToAPI,
			numShipClass=@numShipClass,bitAllowDropShip=@bitAllowDropShip, bitArchiveItem = @IsArchieve,bitMatrix=@bitMatrix,
			numManufacturer=(CASE WHEN LEN(ISNULL(@vcManufacturer,'')) > 0 THEN ISNULL((SELECT TOP 1 numDivisionID FROM CompanyInfo INNER JOIN DivisionMaster ON CompanyInfo.numCompanyId=DivisionMaster.numCompanyID WHERE CompanyInfo.numDOmainID=@numDomainID AND LOWER(vcCompanyName) = LOWER(@vcManufacturer)),0) ELSE 0 END)
		WHERE 
			numItemCode=@numItemCode 
			AND numDomainID=@numDomainID

		IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
		BEGIN
			IF (((SELECT ISNULL(SUM(numOnHand),0) FROM WareHouseItems WHERE numItemID = @numItemCode AND numDomainId=@numDomainID) = 0) AND ((SELECT (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item  WHERE numItemCode = @numItemCode AND numDomainId=@numDomainID ) = 0))
			BEGIN
				UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
			END
		END
	
		--If Average Cost changed then add in Tracking
		IF ISNULL(@monAverageCost,0) != ISNULL(@monOldAverageCost,0)
		BEGIN
			DECLARE @numWareHouseItemID NUMERIC(9);SET @numWareHouseItemID=0
			DECLARE @numTotal int;SET @numTotal=0
			SET @i=0
			DECLARE @vcDescription AS VARCHAR(100);SET @vcDescription=''
		
			SELECT @numTotal=COUNT(*) FROM WareHouseItems where numItemID=@numItemCode AND numDomainId=@numDomainId
	     
			WHILE @i < @numTotal
			BEGIN
				SELECT @numWareHouseItemID = min(numWareHouseItemID) FROM WareHouseItems 
					WHERE numItemID=@numItemCode AND numDomainId=@numDomainId and numWareHouseItemID > @numWareHouseItemID 
				
				IF @numWareHouseItemID>0
				BEGIN
					SET @vcDescription='Average Cost Changed Manually OLD : ' + CAST(ISNULL(@monOldAverageCost,0) AS VARCHAR(20))
				
					DECLARE @numDomain AS NUMERIC(18,0)
					SET @numDomain = @numDomainID
					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)		
						@numReferenceID = @numItemCode, --  numeric(9, 0)
						@tintRefType = 1, --  tinyint
						@vcDescription = @vcDescription, --  varchar(100)
						@tintMode = 0, --  tinyint
						@numModifiedBy = @numUserCntID, --  numeric(9, 0)
						@numDomainID = @numDomain
				END
		    
				SET @i = @i + 1
			END
		END
 
		IF	@bitTaxable = 1
		BEGIN
			IF NOT EXISTS(SELECT * FROM dbo.ItemTax WHERE numItemCode = @numItemCode AND numTaxItemID = 0)
			BEGIN
			    INSERT INTO dbo.ItemTax 
				(
					numItemCode,
					numTaxItemID,
					bitApplicable
				)
				VALUES 
				( 
					@numItemCode,
					0,
					1 
				) 						
			END
		END	 
      
		IF @charItemType='S' or @charItemType='N'                                                                       
		BEGIN
			DELETE FROM WareHouseItmsDTL where numWareHouseItemID in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID)
			DELETE FROM WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID
		END

		IF NOT EXISTS (SELECT numItemCode FROM ItemExtendedDetails  where numItemCode=@numItemCode)
		BEGIN
			INSERT INTO ItemExtendedDetails (numItemCode,txtDesc,txtShortDesc) VALUES (@numItemCode,@txtDesc,@txtShortDesc)
		END
		ELSE
		BEGIN
			UPDATE ItemExtendedDetails SET txtDesc=@txtDesc,@txtShortDesc=txtShortDesc WHERE numItemCode=@numItemCode
		END
	END

	IF ISNULL(@numItemGroup,0) = 0 OR ISNULL(@bitMatrix,0) = 1
	BEGIN
		UPDATE WareHouseItems SET monWListPrice = @monListPrice, vcWHSKU=@vcSKU, vcBarCode=@numBarCodeId WHERE numDomainID = @numDomainID AND numItemID = @numItemCode
	END

	IF @vcCategories <> ''	
	BEGIN
		INSERT INTO ItemCategory( numItemID,numCategoryID )  SELECT @numItemCode,ID from dbo.SplitIDs(@vcCategories,',')	
	END

	-- SAVE MATRIX ITEM ATTRIBUTES
	IF @numItemCode > 0 AND ISNULL(@bitAttributesChanged,0)=1 AND ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) > 0 AND ISNULL(@bitOppOrderExists,0) <> 1 AND (SELECT COUNT(*) FROM @TempTable) > 0
	BEGIN
		-- DELETE PREVIOUS ITEM ATTRIBUTES
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

		-- INSERT NEW ITEM ATTRIBUTES
		INSERT INTO ItemAttributes
		(
			numDomainID
			,numItemCode
			,FLD_ID
			,FLD_Value
		)
		SELECT
			@numDomainID
			,@numItemCode
			,Fld_ID
			,Fld_Value
		FROM
			@TempTable

		IF (SELECT COUNT(*) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode) > 0
		BEGIN

			-- DELETE PREVIOUS WAREHOUSE ATTRIBUTES
			DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId IN (SELECT ISNULL(numWarehouseItemID,0) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode)

			-- INSERT NEW WAREHOUSE ATTRIBUTES
			INSERT INTO CFW_Fld_Values_Serialized_Items
			(
				Fld_ID,
				Fld_Value,
				RecId,
				bitSerialized
			)                                                  
			SELECT 
				Fld_ID,
				ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
				TEMPWarehouse.numWarehouseItemID,
				0 
			FROM 
				@TempTable  
			OUTER APPLY
			(
				SELECT
					numWarehouseItemID
				FROM 
					WarehouseItems 
				WHERE 
					numDomainID=@numDomainID 
					AND numItemID=@numItemCode
			) AS TEMPWarehouse
		END
	END	 
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH        
END

/****** Object:  StoredProcedure [dbo].[USP_InsertCartItem]    Script Date: 11/08/2011 17:53:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_InsertCartItem' ) 
                    DROP PROCEDURE USP_InsertCartItem
                    Go
CREATE  PROCEDURE [dbo].[USP_InsertCartItem]
(
    @numCartId AS NUMERIC OUTPUT,
    @numUserCntId NUMERIC(18, 0),
    @numDomainId NUMERIC(18, 0),
    @vcCookieId VARCHAR(100),
    @numOppItemCode NUMERIC(18, 0),
    @numItemCode NUMERIC(18, 0),
    @numUnitHour NUMERIC(18, 0),
    @monPrice MONEY,
    @numSourceId NUMERIC(18, 0),
    @vcItemDesc VARCHAR(2000),
    @numWarehouseId NUMERIC(18, 0),
    @vcItemName VARCHAR(200),
    @vcWarehouse VARCHAR(200),
    @numWarehouseItmsID NUMERIC(18, 0),
    @vcItemType VARCHAR(200),
    @vcAttributes VARCHAR(100),
    @vcAttrValues VARCHAR(100),
    @bitFreeShipping BIT,
    @numWeight NUMERIC(18, 2),
    @tintOpFlag TINYINT,
    @bitDiscountType BIT,
    @fltDiscount DECIMAL(18,2),
    @monTotAmtBefDiscount MONEY,
    @ItemURL VARCHAR(200),
    @numUOM NUMERIC(18, 0),
    @vcUOMName VARCHAR(200),
    @decUOMConversionFactor DECIMAL(18, 5),
    @numHeight NUMERIC(18, 0),
    @numLength NUMERIC(18, 0),
    @numWidth NUMERIC(18, 0),
    @vcShippingMethod VARCHAR(200),
    @numServiceTypeId NUMERIC(18, 0),
    @decShippingCharge DECIMAL(18, 2),
    @numShippingCompany NUMERIC(18, 0),
    @tintServicetype TINYINT,
    @dtDeliveryDate DATETIME,
    @monTotAmount money,
	@postselldiscount INT=0,
	@numSiteID NUMERIC(18,0) = 0,
	@numPromotionID NUMERIC(18,0) = 0,
	@vcPromotionDescription VARCHAR(MAX) = ''
)
AS 
BEGIN TRY
BEGIN TRANSACTION;
	
	INSERT INTO [dbo].[CartItems]
    (
        [numUserCntId],
        [numDomainId],
        [vcCookieId],
        [numOppItemCode],
        [numItemCode],
        [numUnitHour],
        [monPrice],
        [numSourceId],
        [vcItemDesc],
        [numWarehouseId],
        [vcItemName],
        [vcWarehouse],
        [numWarehouseItmsID],
        [vcItemType],
        [vcAttributes],
        [vcAttrValues],
        [bitFreeShipping],
        [numWeight],
        [tintOpFlag],
        [bitDiscountType],
        [fltDiscount],
        [monTotAmtBefDiscount],
        [ItemURL],
        [numUOM],
        [vcUOMName],
        [decUOMConversionFactor],
        [numHeight],
        [numLength],
        [numWidth],
        [vcShippingMethod],
        [numServiceTypeId],
        [decShippingCharge],
        [numShippingCompany],
        [tintServicetype],
        [monTotAmount],
		[PromotionID],
		[PromotionDesc]
    )
	VALUES  
	(
        @numUserCntId,
        @numDomainId,
        @vcCookieId,
        @numOppItemCode,
        @numItemCode,
        @numUnitHour,
        @monPrice,
        @numSourceId,
        @vcItemDesc,
        @numWarehouseId,
        @vcItemName,
        @vcWarehouse,
        @numWarehouseItmsID,
        @vcItemType,
        @vcAttributes,
        @vcAttrValues,
        @bitFreeShipping,
        @numWeight,
        @tintOpFlag,
        @bitDiscountType,
        @fltDiscount,
        @monTotAmtBefDiscount,
        @ItemURL,
        @numUOM,
        @vcUOMName,
        @decUOMConversionFactor,
        @numHeight,
        @numLength,
        @numWidth,
        @vcShippingMethod,
        @numServiceTypeId,
        @decShippingCharge,
        @numShippingCompany,
        @tintServicetype,
        @monTotAmount,
		@numPromotionID,
		@vcPromotionDescription
    )

	SET @numCartId = SCOPE_IDENTITY()

	DECLARE @numItemClassification As NUMERIC(18,0)

	SELECT 
		@numItemClassification=numItemClassification
	FROM
		Item
	WHERE
		numItemCode=@numItemCode


	IF(@postselldiscount=0)
	BEGIN

		/************************** First check if any promotion can be applied *********************************/
		DECLARE @TEMPPromotion TABLE
		(
			ID INT IDENTITY(1,1),
			numPromotionID NUMERIC(18,0),
			vcPromotionDescription VARCHAR(MAX)
		)

		INSERT INTO 
			@TEMPPromotion
		SELECT
			CI.PromotionID,
			CI.PromotionDesc
		FROM 
			CartItems CI
		INNER JOIN
			PromotionOffer PO
		ON
			CI.PromotionID = PO.numProId
		WHERE
			CI.numDomainId=@numDomainId
			AND CI.numUserCntId=@numUserCntId
			AND CI.vcCookieId=@vcCookieId
			AND ISNULL(CI.bitParentPromotion,0)=1
			AND ISNULL(CI.PromotionID,0) > 0
			AND ISNULL(bitEnabled,0)=1
			AND 1=(CASE PO.tintDiscoutBaseOn
					 WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId=PO.numProId AND POI.numValue=@numItemCode AND tintRecordType=6 AND tintType=1) > 0 THEN 1 ELSE 0 END)
					 WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId=PO.numProId AND POI.numValue=@numItemClassification AND tintRecordType=6 AND tintType=2) > 0 THEN 1 ELSE 0 END)
					 WHEN 3 THEN (CASE PO.tintOfferBasedOn 
									WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=@numItemCode AND numParentItemCode IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=PO.numProId AND tintRecordType=5 AND tintType=1)) > 0 THEN 1 ELSE 0 END)
									WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=@numItemCode AND numParentItemCode IN (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainId AND numItemClassification IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=PO.numProId AND tintRecordType=5 AND tintType=2))) > 0 THEN 1 ELSE 0 END)
									ELSE 0
								END) 
					ELSE 0
				END)
		ORDER BY
			numCartId ASC

		DECLARE @fltDiscountValue FLOAT
		DECLARE @tintDiscountType INT
		DECLARE @tintDiscoutBaseOn INT
		DECLARE @IsPromotionApplied BIT = 0

		DECLARE @Count AS INT
		SELECT @Count=COUNT(*) FROM @TEMPPromotion

		IF @Count > 0
		BEGIN
			DECLARE @i AS INT = 1
		
			WHILE @i <= @Count
			BEGIN
				SELECT @numPromotionID=numPromotionID,@vcPromotionDescription=vcPromotionDescription FROM @TEMPPromotion WHERE ID=@i

				SELECT
					@tintDiscountType=tintDiscountType,
					@fltDiscountValue=fltDiscountValue
				FROM
					PromotionOffer 
				WHERE
					numProId=@numPromotionID
			
				IF @tintDiscountType=1 --Percentage
				BEGIN
					UPDATE 
						CartItems
					SET
						PromotionID=@numPromotionID,
						PromotionDesc=@vcPromotionDescription,
						bitParentPromotion=0,
						fltDiscount=@fltDiscountValue,
						bitDiscountType=0,
						monTotAmount=(monTotAmount-((monTotAmount*@fltDiscountValue)/100))
					WHERE
						numCartId=@numCartId

					SET @IsPromotionApplied = 1
					BREAK
				END
				ELSE IF @tintDiscountType=2 --Flat Amount
				BEGIN
					-- FIRST CHECK WHETHER DISCOUNT AMOUNT IS NOT ALREADY USED BY OTHER ITEMS
					DECLARE @usedDiscountAmount AS FLOAT
				
					SELECT
						@usedDiscountAmount = SUM(fltDiscount)
					FROM
						CartItems
					WHERE
						PromotionID=@numPromotionID

					IF @usedDiscountAmount < @fltDiscountValue
					BEGIN
						UPDATE 
							CartItems 
						SET 
							monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount)  >= monTotAmount THEN 0 ELSE (monTotAmount-(@fltDiscountValue - @usedDiscountAmount)) END),
							PromotionID=@numPromotionID ,
							PromotionDesc=@vcPromotionDescription,
							bitDiscountType=1,
							fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount) >= monTotAmount THEN monTotAmount ELSE (@fltDiscountValue - @usedDiscountAmount) END)
						WHERE 
							numCartId=@numCartId

						SET @IsPromotionApplied = 1
						BREAK
					END
				END
				ELSE IF @tintDiscountType=3 --Quantity
				BEGIN
					-- FIRST CHECK WHETHER DISCOUNT QTY IS NOT ALREADY USED BY OTHER ITEMS
					DECLARE @usedDiscountQty AS FLOAT

					SELECT
						@usedDiscountQty = SUM(fltDiscount/monPrice)
					FROM
						CartItems
					WHERE
						PromotionID=@numPromotionID

					IF @usedDiscountQty < @fltDiscountValue
					BEGIN
						UPDATE 
							CartItems 
						SET 
							monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty)  >= numUnitHour THEN 0 ELSE (monTotAmount-((@fltDiscountValue - @usedDiscountQty)*monPrice)) END),
							PromotionID=@numPromotionID ,
							PromotionDesc=@vcPromotionDescription,
							bitDiscountType=1,
							fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty) >= numUnitHour THEN (numUnitHour*monPrice) ELSE ((@fltDiscountValue - @usedDiscountQty) *monPrice) END) 
						WHERE 
							numCartId=@numCartId

						SET @IsPromotionApplied = 1
						BREAK
					END

				END

				SET @i = @i + 1
			END
		END


		IF @IsPromotionApplied = 0
		BEGIN
			/************************** Check if any promotion can be trigerred *********************************/
			EXEC USP_ECommerceTiggerPromotion @numDomainID,@numUserCntId,@vcCookieId,@numCartID,@numItemCode,@numItemClassification,@numUnitHour,@monPrice,@numSiteID
		
		END

	END


	SELECT @numCartId

 COMMIT TRANSACTION;
  END TRY
  BEGIN CATCH
    IF @@TRANCOUNT > 0
    ROLLBACK TRANSACTION;

    DECLARE @ErrorNumber INT = ERROR_NUMBER();
    DECLARE @ErrorLine INT = ERROR_LINE();
    DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
    DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
    DECLARE @ErrorState INT = ERROR_STATE();

    PRINT 'Actual error number: ' + CAST(@ErrorNumber AS VARCHAR(10));
    PRINT 'Actual line number: ' + CAST(@ErrorLine AS VARCHAR(10));

    RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
  END CATCH
            
/****** Object:  StoredProcedure [dbo].[USP_ManageCategory]    Script Date: 07/26/2008 16:19:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managecategory')
DROP PROCEDURE usp_managecategory
GO
CREATE PROCEDURE [dbo].[USP_ManageCategory]        
@numCatergoryId as numeric(9),        
@vcCatgoryName as varchar(1000),       
@numDomainID as numeric(9)=0,
@vcDescription as VARCHAR(MAX),
@intDisplayOrder AS INT ,
@vcPathForCategoryImage AS VARCHAR(100),
@numDepCategory AS NUMERIC(9,0),
@numCategoryProfileID AS NUMERIC(18,0),
@vcCatgoryNameURL VARCHAR(1000),
@vcMetaTitle as varchar(1000),
@vcMetaKeywords as varchar(1000),
@vcMetaDescription as varchar(1000)
as 

	DECLARE @tintLevel AS TINYINT
	SET @tintLevel = 1

	IF	@numDepCategory <> 0
	BEGIN
		SELECT @tintLevel = (tintLevel + 1) FROM dbo.Category WHERE numCategoryID = @numDepCategory
	END       
       
	IF @numCatergoryId=0        
	BEGIN        
		INSERT INTO Category 
		(
			vcCategoryName,
			tintLevel,
			numDomainID,
			vcDescription,
			intDisplayOrder,
			vcPathForCategoryImage,
			numDepCategory,
			numCategoryProfileID,
			vcCategoryNameURL,
			vcMetaTitle,
			vcMetaKeywords,
			vcMetaDescription
		)        
		VALUES
		(
			@vcCatgoryName,
			@tintLevel,
			@numDomainID,
			@vcDescription,
			@intDisplayOrder,
			@vcPathForCategoryImage,
			@numDepCategory,
			@numCategoryProfileID,
			@vcCatgoryNameURL,
			@vcMetaTitle,
			@vcMetaKeywords,
			@vcMetaDescription
		) 
		set @numCatergoryId = SCOPE_IDENTITY();   
		insert into SiteCategories 
	(
		numSiteID,
		numCategoryID
	)
	values
	(
		(select Top 1 numSiteID from CategoryProfileSites where numCategoryProfileID=@numCategoryProfileID),
		@numCatergoryId
	)    
	END        
	ELSE IF @numCatergoryId>0        
	BEGIN        
		UPDATE 
			Category 
		SET 
			vcCategoryName=@vcCatgoryName,
			vcDescription=@vcDescription,
			intDisplayOrder = @intDisplayOrder,
			tintLevel = @tintLevel,
			vcPathForCategoryImage = @vcPathForCategoryImage,
			numDepCategory = @numDepCategory,
			vcCategoryNameURL=@vcCatgoryNameURL,
			vcMetaTitle=@vcMetaTitle,
			vcMetaKeywords=@vcMetaKeywords,
			vcMetaDescription=@vcMetaDescription
		WHERE 
			numCategoryID=@numCatergoryId        
	END
	
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageTransactionHistory')
	DROP PROCEDURE USP_ManageTransactionHistory
GO

CREATE PROCEDURE [dbo].[USP_ManageTransactionHistory]
	@numTransHistoryID numeric(18, 0) OUTPUT,
	@numDomainID numeric(18, 0),
	@numDivisionID numeric(18, 0),
	@numContactID numeric(18, 0),
	@numOppID numeric(18, 0),
	@numOppBizDocsID numeric(18, 0),
	@vcTransactionID varchar(100),
	@tintTransactionStatus tinyint,
	@vcPGResponse varchar(200),
	@tintType tinyint,
	@monAuthorizedAmt money,
	@monCapturedAmt money,
	@monRefundAmt money,
	@vcCardHolder varchar(500),
	@vcCreditCardNo varchar(500),
	@vcCVV2 varchar(200),
	@tintValidMonth tinyint,
	@intValidYear int,
	@vcSignatureFile VARCHAR(100),
	@numUserCntID numeric(18, 0),
	@numCardType NUMERIC(9,0),
	@vcResponseCode VARCHAR(500) = ''
AS

 SET NOCOUNT ON
 SET TRANSACTION ISOLATION LEVEL READ COMMITTED


IF EXISTS(SELECT [numTransHistoryID] FROM [dbo].[TransactionHistory] WHERE [numTransHistoryID] = @numTransHistoryID)
BEGIN
	DECLARE @OldCapturedAmt AS MONEY
	DECLARE @OldRefundAmt AS MONEY
	
	SELECT 
		@OldCapturedAmt = ISNULL(monCapturedAmt,0)
		,@OldRefundAmt = ISNULL(monRefundAmt,0) 
	FROM 
		[dbo].[TransactionHistory] 
	WHERE 
		[numTransHistoryID] = @numTransHistoryID
	
	IF @tintTransactionStatus =2 -- captured
	BEGIN
		SET @OldCapturedAmt = @OldCapturedAmt + ISNULL(@monCapturedAmt,0)
		SET @OldRefundAmt = ISNULL(@monRefundAmt,0) + ISNULL(@OldRefundAmt,0)
	END
	ELSE IF @tintTransactionStatus =5 -- Credited
	BEGIN
		SET @OldRefundAmt = ISNULL(@monRefundAmt,0) + ISNULL(@OldRefundAmt,0)
	END
	
	UPDATE [dbo].[TransactionHistory] SET
		[tintTransactionStatus] = @tintTransactionStatus,
		[monCapturedAmt] = @OldCapturedAmt,
		[monRefundAmt] = @OldRefundAmt,
		[numModifiedBy] = @numUserCntID,
		[vcPGResponse] = (CASE WHEN @tintTransactionStatus = 2 OR @tintTransactionStatus = 4 THEN @vcPGResponse ELSE [vcPGResponse] END),
		[dtModifiedDate] = GETUTCDATE()
	WHERE
		[numTransHistoryID] = @numTransHistoryID AND numDomainID=@numDomainID
END
ELSE
BEGIN
	INSERT INTO [dbo].[TransactionHistory] (
		[numDomainID],
		[numDivisionID],
		[numContactID],
		[numOppID],
		[numOppBizDocsID],
		[vcTransactionID],
		[tintTransactionStatus],
		[vcPGResponse],
		[tintType],
		[monAuthorizedAmt],
		[monCapturedAmt],
		[monRefundAmt],
		[vcCardHolder],
		[vcCreditCardNo],
		[vcCVV2],
		[tintValidMonth],
		[intValidYear],
		vcSignatureFile,
		[numCreatedBy],
		[dtCreatedDate],
		[numModifiedBy],
		[dtModifiedDate],
		[numCardType],
		[vcResponseCode]
	) VALUES (
		@numDomainID,
		@numDivisionID,
		@numContactID,
		@numOppID,
		@numOppBizDocsID,
		@vcTransactionID,
		@tintTransactionStatus,
		@vcPGResponse,
		@tintType,
		@monAuthorizedAmt,
		@monCapturedAmt,
		@monRefundAmt,
		@vcCardHolder,
		@vcCreditCardNo,
		@vcCVV2,
		@tintValidMonth,
		@intValidYear,
		@vcSignatureFile,
		@numUserCntID,
		GETUTCDATE(),
		@numUserCntID,
		GETUTCDATE(),
		@numCardType,
		@vcResponseCode
	)
	
	SET @numTransHistoryID = SCOPE_IDENTITY()
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_OPPBizDocItems_GetWithKitChilds' ) 
    DROP PROCEDURE USP_OPPBizDocItems_GetWithKitChilds
GO
CREATE PROCEDURE [dbo].[USP_OPPBizDocItems_GetWithKitChilds]
(
    @numOppId AS NUMERIC(9) = NULL,
    @numOppBizDocsId AS NUMERIC(9) = NULL,
    @numDomainID AS NUMERIC(9) = 0      
)
AS 
BEGIN
    DECLARE @DivisionID AS NUMERIC(9)      
    DECLARE @tintTaxOperator AS TINYINT   
                                                                                                                                                                                                                                                                                                                                          
    SELECT  
		@DivisionID = numDivisionID, 
		@tintTaxOperator = [tintTaxOperator]           
    FROM    
		OpportunityMaster
    WHERE   
		numOppId = @numOppId                                                                                                                                                                          

    
                                                                                          
    DECLARE @strSQL AS VARCHAR(MAX)                                                                                          
    DECLARE @strSQL1 AS VARCHAR(8000)                                       
    DECLARE @strSQLCusFields VARCHAR(1000) = ''
    DECLARE @strSQLEmpFlds VARCHAR(500) = ''                                                                         
                                                                                              
    DECLARE @tintType AS TINYINT   
    DECLARE @tintOppType AS TINYINT                                                      
    DECLARE @intRowNum AS INT                                                  
    DECLARE @numFldID AS VARCHAR(15)                                          
    DECLARE @vcFldname AS VARCHAR(50)                                            
    DECLARE @numBizDocId AS NUMERIC(9)
    DECLARE @numBizDocTempID AS NUMERIC(9) = 0
	DECLARE @bitDisplayKitChild AS BIT = 0
        
                                           
    SELECT  
		@numBizDocTempID = ISNULL(numBizDocTempID, 0),
        @numBizDocId = numBizDocId
    FROM    
		OpportunityBizDocs
    WHERE   
		numOppBizDocsId = @numOppBizDocsId

	
	SELECT
		@bitDisplayKitChild = ISNULL(bitDisplayKitChild,0)
	FROM	
		BizDocTemplate
	WHERE
		numDomainID = @numDomainID
		AND numBizDocTempID = @numBizDocTempID


    SELECT  
		@tintType = tintOppType,
        @tintOppType = tintOppType
    FROM    
		OpportunityMaster
    WHERE   
		numOppID = @numOppId                                                                                          
                                                                                   
    IF @tintType = 1 
        SET @tintType = 7
    ELSE 
        SET @tintType = 8     
		
		
	IF (SELECT 
			COUNT(*) 
		FROM 
			View_DynamicColumns 
		WHERE   
			numFormID = @tintType 
			AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId 
			AND vcFieldType = 'R'
			AND ISNULL(numRelCntType, 0) = @numBizDocTempID
		)=0
	BEGIN            
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType
		)
		SELECT 
			numFormId,numFieldId,1,tintRow,@numDomainID,@numBizDocId,@numBizDocTempID
		FROM 
			View_DynamicDefaultColumns
		WHERE 
			numFormId=@tintType and bitDefault=1 and numDomainID=@numDomainID order by tintRow ASC	
	 
	 
		IF (SELECT 
				COUNT(*) 
			FROM 
				DycFrmConfigBizDocsSumm Ds 
			WHERE 
				numBizDocID=@numBizDocId 
				AND Ds.numFormID=@tintType 
				AND ISNULL(Ds.numBizDocTempID,0)=@numBizDocTempID 
				AND Ds.numDomainID=@numDomainID
			)=0
		BEGIN
			INSERT INTO DycFrmConfigBizDocsSumm 
			(
				numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID
			)
			SELECT 
				@tintType,@numBizDocId,DFM.numFieldID,@numDomainID,@numBizDocTempID 
			FROM
				DycFormField_Mapping DFFM 
			JOIN 
				DycFieldMaster DFM 
			ON 
				DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
			WHERE 
				numFormID=16 
				AND DFFM.numDomainID IS NULL
		END
	END      

	IF OBJECT_ID('tempdb..#TempBizDocProductGridColumns') IS NOT NULL DROP TABLE #TempBizDocProductGridColumns
	SELECT * INTO #TempBizDocProductGridColumns FROM (
    SELECT  
		vcFieldName AS vcFormFieldName,
        vcDbColumnName,
        vcFieldDataType,
        tintRow AS intRowNum
    FROM    
		View_DynamicColumns
    WHERE   
		numFormID = @tintType
        AND numDomainID = @numDomainID
        AND numAuthGroupID = @numBizDocId
        AND vcFieldType = 'R'
        AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    UNION
    SELECT  
		vcFieldName AS vcFormFieldName,
        vcDbColumnName,
        'C' vcFieldDataType,
        tintRow AS intRowNum
    FROM    
		View_DynamicCustomColumns
    WHERE   
		numFormID = @tintType
        AND Grp_id = 5
        AND numDomainID = @numDomainID
        AND numAuthGroupID = @numBizDocId
        AND bitCustom = 1
        AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ) t1   
	ORDER BY 
		intRowNum                                                                              
                                                                                
    SELECT  
		*
    INTO    
		#Temp1
    FROM    
	( 
		SELECT   
			Opp.vcitemname AS vcItemName,
            ISNULL(OBD.numOppBizDocItemID, 0) OppBizDocItemID,
			(CASE WHEN OBD.numUnitHour > ISNULL(WI.numAllocation,0) THEN (OBD.numUnitHour - ISNULL(WI.numAllocation,0)) * dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(opp.numUOMId, 0))  ELSE 0 END) AS numBackOrder,
            CASE WHEN charitemType = 'P' THEN 'Product'
                    WHEN charitemType = 'S' THEN 'Service'
            END AS charitemType,
            CASE WHEN charitemType = 'P' THEN 'Inventory'
	                WHEN charitemType = 'N' THEN 'Non-Inventory'
                    WHEN charitemType = 'S' THEN 'Service'
            END AS vcItemType,
			CONVERT(VARCHAR(2500), OBD.vcitemDesc) AS txtItemDesc, 
			opp.numUnitHour AS numOrgTotalUnitHour,
			dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(opp.numUOMId, 0)) * opp.numUnitHour AS numTotalUnitHour,                                   
            dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(opp.numUOMId, 0)) * OBD.numUnitHour AS numUnitHour,
            CONVERT(DECIMAL(18, 4), (dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * OBD.monPrice)) monPrice,
            CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4), OBD.monTotAmount)) Amount,
            OBD.monTotAmount/*Fo calculating sum*/,
            ISNULL(CONVERT(VARCHAR, i.monListPrice), 0) AS monListPrice,
            i.numItemCode AS ItemCode,
            opp.numoppitemtCode,
            L.vcdata AS numItemClassification,
            CASE WHEN bitTaxable = 0 THEN 'No'
                    ELSE 'Yes'
            END AS Taxable,
            numIncomeChartAcntId AS itemIncomeAccount,
            'NI' AS ItemType,
            ISNULL(GJH.numJournal_Id, 0) AS numJournalId,
            ( SELECT TOP 1
                        ISNULL(GJD.numTransactionId, 0)
                FROM      General_Journal_Details GJD
                WHERE     GJH.numJournal_Id = GJD.numJournalId
                        AND GJD.chBizDocItems = 'NI'
                        AND GJD.numoppitemtCode = opp.numoppitemtCode
            ) AS numTransactionId,
            ISNULL(Opp.vcModelID, '') vcModelID,
            (CASE WHEN charitemType = 'P' THEN dbo.USP_GetAttributes(OBD.numWarehouseItmsID, bitSerialized) ELSE ISNULL(OBD.vcAttributes,0) END) AS vcAttributes,
            ISNULL(vcPartNo, '') AS vcPartNo,
            (ISNULL(OBD.monTotAmtBefDiscount, OBD.monTotAmount) - OBD.monTotAmount ) AS DiscAmt,
			(CASE WHEN OBD.numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(18, 4),(dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * (ISNULL(OBD.monTotAmount,0) / OBD.numUnitHour))) END) As monUnitSalePrice,
            ISNULL(OBD.vcNotes,ISNULL(opp.vcNotes,'')) vcNotes,
            OBD.vcTrackingNo,
            OBD.vcShippingMethod,
            OBD.monShipCost,
            [dbo].[FormatedDateFromDate](OBD.dtDeliveryDate,@numDomainID) dtDeliveryDate,
            Opp.numWarehouseItmsID,
            ISNULL(u.vcUnitName, '') numUOMId,
            ISNULL(Opp.vcManufacturer, '') AS vcManufacturer,
            ISNULL(V.monCost, 0) AS VendorCost,
            Opp.vcPathForTImage,
            i.[fltLength],
            i.[fltWidth],
            i.[fltHeight],
            i.[fltWeight],
            ISNULL(I.numVendorID, 0) numVendorID,
            Opp.bitDropShip AS DropShip,
            SUBSTRING(( SELECT  ' ,' + vcSerialNo
                                + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                        THEN ' ('
                                            + CONVERT(VARCHAR(15), oppI.numQty)
                                            + ')'
                                        ELSE ''
                                    END
                        FROM    OppWarehouseSerializedItem oppI
                                JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                        WHERE   oppI.numOppID = Opp.numOppId
                                AND oppI.numOppItemID = Opp.numoppitemtCode
                        ORDER BY vcSerialNo
                        FOR
                        XML PATH('')
                        ), 3, 200000) AS SerialLotNo,
            ISNULL(opp.numUOMId, 0) AS numUOM,
            ISNULL(u.vcUnitName, '') vcUOMName,
            dbo.fn_UOMConversion(opp.numUOMId, Opp.numItemCode,
                                    @numDomainID, NULL) AS UOMConversionFactor,
            ISNULL(Opp.numSOVendorId, 0) AS numSOVendorId,
            [dbo].FormatedDateTimeFromDate(OBD.dtRentalStartDate,
                                            @numDomainID) dtRentalStartDate,
            [dbo].FormatedDateTimeFromDate(OBD.dtRentalReturnDate,
                                            @numDomainID) dtRentalReturnDate,
            ISNULL(W.vcWareHouse, '') AS Warehouse,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcWHSKU,I.vcSKU) ELSE I.vcSKU END vcSKU,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END vcBarcode,
			CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END numBarCodeId,
            ISNULL(opp.numClassID, 0) AS numClassID,
            ISNULL(opp.numProjectID, 0) AS numProjectID,
            ISNULL(I.[numShipClass],0) AS numShipClass,OBD.numOppBizDocID,WL.vcLocation,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
            opp.numOppId,OBD.numUnitHour AS numUnitHourOrig,ISNULL(I.numItemCode,0) AS [numItemCode],
			ISNULL(I.bitKitParent,0) AS bitKitParent,
			0 AS numOppChildItemID, 
			0 AS numOppKitChildItemID,
			0 AS bitChildItem,
			0 AS tintLevel,
			ISNULL(opp.vcPromotionDetail,'') AS vcPromotionDetail,
			(CASE WHEN ISNULL(WI.numWareHouseItemID,0) > 0 THEN CONCAT(W.vcWareHouse,CASE WHEN WL.numWLocationID > 0 THEN CONCAT('(',WL.vcLocation ,')') ELSE '' END) ELSE '' END) vcWarehouse,
			(CASE WHEN (SELECT COUNT(*) FROM #TempBizDocProductGridColumns WHERE vcDbColumnName='vcInclusionDetails') > 0 THEN dbo.GetOrderAssemblyKitInclusionDetails(opp.numOppID,Opp.numoppitemtCode,OBD.numUnitHour) ELSE '' END) AS vcInclusionDetails
        FROM      
			OpportunityItems opp
        JOIN 
			OpportunityBizDocItems OBD 
		ON 
			OBD.numOppItemID = opp.numoppitemtCode
        LEFT JOIN 
			item i 
		ON 
			opp.numItemCode = i.numItemCode
        LEFT JOIN 
			ListDetails L 
		ON 
			i.numItemClassification = L.numListItemID
        LEFT JOIN 
			Vendor V 
		ON 
			V.numVendorID = @DivisionID
            AND V.numItemCode = opp.numItemCode
        LEFT JOIN 
			General_Journal_Header GJH 
		ON 
			opp.numoppid = GJH.numOppId
            AND GJH.numBizDocsPaymentDetId IS NULL
            AND GJH.numOppBizDocsId = @numOppBizDocsId
        LEFT JOIN 
			UOM u 
		ON 
			u.numUOMId = opp.numUOMId                                      
        LEFT JOIN 
			dbo.WareHouseItems WI 
		ON 
			opp.numWarehouseItmsID = WI.numWareHouseItemID
			AND WI.numDomainID = @numDomainID
        LEFT JOIN 
			dbo.Warehouses W 
		ON 
			WI.numDomainID = W.numDomainID
            AND WI.numWareHouseID = W.numWareHouseID
        LEFT JOIN 
			dbo.WarehouseLocation WL 
		ON 
			WL.numWLocationID = WI.numWLocationID
        WHERE     
			opp.numOppId = @numOppId
            AND (bitShowDeptItem IS NULL OR bitShowDeptItem = 0)
            AND OBD.numOppBizDocID = @numOppBizDocsId
    ) X

	--GET KIT CHILD ITEMS (NOW WE ARE SUPPORTING KIT WITHIN KIT BUT ONLY 1 LEVEL MEANS CHILD KIT INSIDE PARENT KIT CAN NOT HAVE ANOTHER KIT AS CHILD)
	IF (@bitDisplayKitChild=1 AND (SELECT COUNT(*) FROM OpportunityKitItems WHERE numOppId = @numOppId ) > 0)
	BEGIN
		DECLARE @TEMPTABLE TABLE(
			numOppItemCode NUMERIC(18,0),
			numOppChildItemID NUMERIC(18,0), 
			numOppKitChildItemID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numWarehouseItemID NUMERIC(18,0),
			numTotalQty NUMERIC(18,2),
			numQty NUMERIC(18,2),
			numUOMID NUMERIC(18,0),
			tintLevel TINYINT
		);

		--FIRST LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,numOppChildItemID,numOppKitChildItemID,numItemCode,numWarehouseItemID,numTotalQty,numQty,numUOMID,tintLevel
		)
		SELECT 
			OKI.numOppItemID,
			OKI.numOppChildItemID,
			CAST(0 AS NUMERIC(18,0)),
			OKI.numChildItemID,
			ISNULL(OKI.numWareHouseItemId,0),
			CAST((t1.numOrgTotalUnitHour * OKI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			CAST((t1.numUnitHour * OKI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			ISNULL(OKI.numUOMID,0),
			1
		FROM 
			#Temp1 t1
		INNER JOIN
			OpportunityKitItems OKI
		ON
			t1.numoppitemtCode = OKI.numOppItemID
		WHERE
			t1.bitKitParent = 1

		--SECOND LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,numOppChildItemID,numOppKitChildItemID,numItemCode,numWarehouseItemID,numTotalQty,numQty,numUOMID,tintLevel
		)
		SELECT 
			OKCI.numOppItemID,
			OKCI.numOppChildItemID,
			OKCI.numOppKitChildItemID,
			OKCI.numItemID,
			ISNULL(OKCI.numWareHouseItemId,0),
			CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			CAST((c.numQty * OKCI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			ISNULL(OKCI.numUOMID,0),
			2
		FROM 
			OpportunityKitChildItems OKCI
		INNER JOIN
			@TEMPTABLE c
		ON
			OKCI.numOppID = @numOppId
			AND OKCI.numOppItemID = c.numOppItemCode
			AND OKCI.numOppChildItemID = c.numOppChildItemID

		INSERT INTO 
			#Temp1
		SELECT   
			(CASE WHEN t2.tintLevel = 1 THEN CONCAT('&nbsp;&nbsp;',I.vcItemName) WHEN t2.tintLevel = 2 THEN CONCAT('&nbsp;&nbsp;&nbsp;&nbsp;',I.vcItemName) ELSE I.vcItemName END) AS vcItemName,
            0 OppBizDocItemID,ISNULL(WI.numBackOrder,0) AS numBackOrder,
            (CASE WHEN I.charitemType = 'P' THEN 'Product' WHEN charitemType = 'S' THEN 'Service' END) AS charitemType,
            (CASE WHEN charitemType = 'P' THEN 'Inventory'
	                WHEN charitemType = 'N' THEN 'Non-Inventory'
                    WHEN charitemType = 'S' THEN 'Service'
            END) AS vcItemType,
			CONVERT(VARCHAR(2500), I.txtItemDesc) AS txtItemDesc,   
			t2.numTotalQty AS numOrigTotalUnitHour,
			t2.numTotalQty AS numTotalUnitHour,
            t2.numQty AS numUnitHour,
            (ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) monPrice,
            CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4),((t2.numQty * ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1)) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)))) Amount,
            ((t2.numQty * ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1)) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) AS monTotAmount/*Fo calculating sum*/,
            ISNULL(CONVERT(VARCHAR, (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)), 0) AS monListPrice,
            I.numItemCode AS ItemCode,
            t2.numOppItemCode,
            L.vcdata AS numItemClassification,
            CASE WHEN bitTaxable = 0 THEN 'No' ELSE 'Yes' END AS Taxable,
            numIncomeChartAcntId AS itemIncomeAccount,
            'NI' AS ItemType,
            0 AS numJournalId,
            0 AS numTransactionId,
            ISNULL(I.vcModelID, '') vcModelID,
            dbo.USP_GetAttributes(t2.numWarehouseItemID, bitSerialized) AS vcAttributes,
            ISNULL(vcPartNo, '') AS vcPartNo,
            0 AS DiscAmt,
			(ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) As monUnitSalePrice,
            '' AS vcNotes,'' AS vcTrackingNo,'' AS vcShippingMethod,0 AS monShipCost,NULL AS dtDeliveryDate,t2.numWarehouseItemID,
            ISNULL(u.vcUnitName, '') numUOMId,ISNULL(I.vcManufacturer, '') AS vcManufacturer,ISNULL(V.monCost, 0) AS VendorCost,
            ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault=1),'') AS vcPathForTImage,
            I.[fltLength],I.[fltWidth],I.[fltHeight],I.[fltWeight],ISNULL(I.numVendorID, 0) numVendorID,0 AS DropShip,
            '' AS SerialLotNo,
            ISNULL(t2.numUOMId, 0) AS numUOM,
            ISNULL(u.vcUnitName, '') vcUOMName,
            dbo.fn_UOMConversion(t2.numUOMId, t2.numItemCode,@numDomainID, NULL) AS UOMConversionFactor,
            0 AS numSOVendorId,
            NULL AS dtRentalStartDate,
            NULL dtRentalReturnDate,
            ISNULL(W.vcWareHouse, '') AS Warehouse,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcWHSKU,I.vcSKU) ELSE I.vcSKU END vcSKU,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END vcBarcode,
			CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END numBarCodeId,
            ISNULL(I.numItemClass, 0) AS numClassID,
            0 AS numProjectID,
            ISNULL(I.[numShipClass],0) AS numShipClass,
			@numOppBizDocsId AS numOppBizDocID,WL.vcLocation,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
            @numOppId AS numOppId,t2.numQty AS numUnitHourOrig,
			ISNULL(I.numItemCode,0) AS [numItemCode],
			ISNULL(I.bitKitParent,0) AS bitKitParent,
			ISNULL(t2.numOppChildItemID,0) AS numOppChildItemID, 
			ISNULL(t2.numOppKitChildItemID,0) AS numOppKitChildItemID,
			1 AS bitChildItem,
			t2.tintLevel AS tintLevel,
			'' AS vcPromotionDetail,
			(CASE WHEN ISNULL(WI.numWareHouseItemID,0) > 0 THEN CONCAT(W.vcWareHouse,CASE WHEN ISNULL(WL.numWLocationID,0) > 0 THEN CONCAT('(',WL.vcLocation ,')') ELSE '' END) ELSE '' END) vcWarehouse,
			'' vcInclusionDetails
		FROM
			@TEMPTABLE t2
		INNER JOIN
			Item I
		ON
			t2.numItemCode = I.numItemCode
		LEFT JOIN
			WareHouseItems WI
		ON
			WI.numWareHouseItemID = t2.numWarehouseItemID
		LEFT JOIN 
			dbo.Warehouses W 
		ON 
			WI.numDomainID = W.numDomainID
            AND WI.numWareHouseID = W.numWareHouseID
        LEFT JOIN 
			dbo.WarehouseLocation WL 
		ON 
			WL.numWLocationID = WI.numWLocationID
		LEFT JOIN 
			ListDetails L 
		ON 
			I.numItemClassification = L.numListItemID
		LEFT JOIN 
			Vendor V 
		ON 
			V.numVendorID = @DivisionID
            AND V.numItemCode = t2.numItemCode
		LEFT JOIN 
			UOM u 
		ON 
			u.numUOMId = t2.numUOMId 
	END


    DECLARE @strSQLUpdate AS VARCHAR(2000);SET @strSQLUpdate=''
   
    EXEC ('ALTER TABLE #Temp1 ADD [TotalTax] MONEY')

    IF @tintTaxOperator = 1 --add Sales tax
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
	END        
    ELSE IF @tintTaxOperator = 2 -- remove sales tax
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
	END
    ELSE 
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
    END

	-- CRV TAX TYPE
	EXEC ('ALTER TABLE #Temp1 ADD [CRV] MONEY')

	IF @tintTaxOperator = 2
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate
				+ ',[CRV]= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ',1,'
				+ CONVERT(VARCHAR(20), @numOppId) +','+
				+ 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
	END
	ELSE
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate + ',[CRV]= 0'
	END

	DECLARE @vcTaxName AS VARCHAR(100)
    DECLARE @numTaxItemID AS NUMERIC(9)
    SET @numTaxItemID = 0
    
	SELECT TOP 1
        @vcTaxName = vcTaxName,
        @numTaxItemID = numTaxItemID
    FROM    
		TaxItems
    WHERE   
		numDomainID = @numDomainID

    WHILE @numTaxItemID > 0
    BEGIN
        EXEC ('ALTER TABLE #Temp1 ADD [' + @vcTaxName + '] MONEY')

		IF @tintTaxOperator = 2 -- remove sales tax
		BEGIN
			SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName + ']=0' 
		END
		ELSE
		BEGIN
			SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName
				+ ']= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ','
				+ CONVERT(VARCHAR(20), @numTaxItemID) + ','
				+ CONVERT(VARCHAR(20), @numOppId) +','+
				+ 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
		END

        SELECT TOP 1
			@vcTaxName = vcTaxName,
            @numTaxItemID = numTaxItemID
        FROM    
			TaxItems
        WHERE   
			numDomainID = @numDomainID
            AND numTaxItemID > @numTaxItemID

        IF @@rowcount = 0 
            SET @numTaxItemID = 0
    END

    IF @strSQLUpdate <> '' 
    BEGIN
        SET @strSQLUpdate = 'UPDATE #Temp1 SET ItemCode=ItemCode' + @strSQLUpdate + ' WHERE ItemCode > 0 AND bitChildItem=0'
        EXEC (@strSQLUpdate)
    END

    DECLARE @vcDbColumnName NVARCHAR(50)

    SELECT TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow     
	                                                                                     
    WHILE @intRowNum > 0                                                                                  
    BEGIN    
        EXEC('ALTER TABLE #Temp1 ADD [' + @vcDbColumnName + '] varchar(100)')
	
        SET @strSQLUpdate = 'UPDATE #Temp1 set ItemCode=ItemCode,[' + @vcDbColumnName + ']= dbo.GetCustFldValueItem(' + @numFldID  + ',ItemCode) WHERE ItemCode > 0 AND bitChildItem=0'
            
		EXEC (@strSQLUpdate)
		                   
        SELECT 
			TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
        FROM    
			View_DynamicCustomColumns
        WHERE   
			numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND tintRow >= @intRowNum
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
        ORDER BY 
			tintRow                                                                  
		           
        IF @@rowcount = 0 
            SET @intRowNum = 0                                                                                          
    END


    SELECT  
		ROW_NUMBER() OVER ( ORDER BY numoppitemtCode ) RowNumber,
        *
    FROM    
		#Temp1
    ORDER BY 
		numoppitemtCode ASC, numOppChildItemID ASC, numOppKitChildItemID ASC

    DROP TABLE #Temp1

    SELECT  
		ISNULL(tintOppStatus, 0)
    FROM    
		OpportunityMaster
    WHERE   
		numOppID = @numOppId                                                                                                 


	SELECT * FROM #TempBizDocProductGridColumns

	IF OBJECT_ID('tempdb..#TempBizDocProductGridColumns') IS NOT NULL DROP TABLE #TempBizDocProductGridColumns
END
GO
/****** Object:  StoredProcedure [dbo].[USP_OPPItemDetails]    Script Date: 09/01/2009 18:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_OPPItemDetails @numItemCode=31455,@OpportunityId=12387,@numDomainId=110
--created by anooop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppitemdetails')
DROP PROCEDURE usp_oppitemdetails
GO
CREATE PROCEDURE [dbo].[USP_OPPItemDetails]
    (
      @numUserCntID NUMERIC(9) = 0,
      @OpportunityId NUMERIC(9) = 0,
      @numDomainId NUMERIC(9),
      @ClientTimeZoneOffset  INT
    )
AS 
BEGIN
    IF @OpportunityId >0
    BEGIN
        DECLARE @DivisionID AS NUMERIC(9)
        DECLARE @TaxPercentage AS FLOAT
        DECLARE @tintOppType AS TINYINT
        DECLARE @fld_Name AS VARCHAR(500)
        DECLARE @strSQL AS VARCHAR(MAX)
        DECLARE @fld_ID AS NUMERIC(9)
        DECLARE @numBillCountry AS NUMERIC(9)
        DECLARE @numBillState AS NUMERIC(9)

                
        SELECT  @DivisionID = numDivisionID,
                @tintOppType = tintOpptype
        FROM    OpportunityMaster
        WHERE   numOppId = @OpportunityId
               

		CREATE TABLE #tempForm 
		(
			tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
			vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
			numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
			bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,intVisible int
			,vcFieldDataType CHAR(1)
		)

		IF
			(
				SELECT 
					COUNT(*)
				FROM 
					View_DynamicColumns DC 
				WHERE 
					DC.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) and DC.numDomainID=@numDomainID AND numUserCntID = @numUserCntID AND
					ISNULL(DC.bitSettingField,0)=1 AND ISNULL(DC.bitDeleted,0)=0 AND ISNULL(DC.bitCustom,0)=0) > 0
			OR
			(
				SELECT 
					COUNT(*)
				FROM 
					View_DynamicCustomColumns
				WHERE 
					numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0 AND ISNULL(bitCustom,0)=1
			) > 0
		BEGIN	

		INSERT INTO #tempForm
		select isnull(DC.tintRow,0)+1 as tintOrder,DDF.vcDbColumnName,DDF.vcOrigDbColumnName,ISNULL(DDF.vcCultureFieldName,DDF.vcFieldName),                  
		 DDF.vcAssociatedControlType,DDF.vcListItemType,DDF.numListID,DDF.vcLookBackTableName                                                 
		,DDF.bitCustom,DDF.numFieldId,DDF.bitAllowSorting,DDF.bitInlineEdit,
		DDF.bitIsRequired,DDF.bitIsEmail,DDF.bitIsAlphaNumeric,DDF.bitIsNumeric,DDF.bitIsLengthValidation,
		DDF.intMaxLength,DDF.intMinLength,DDF.bitFieldMessage,DDF.vcFieldMessage vcFieldMessage,DDF.ListRelID,DDF.intColumnWidth,Case when DC.numFieldID is null then 0 else 1 end,DDF.vcFieldDataType
		 FROM View_DynamicDefaultColumns DDF left join View_DynamicColumns DC on DC.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) and DDF.numFieldId=DC.numFieldId and DC.numUserCntID=@numUserCntID and DC.numDomainID=@numDomainID and numRelCntType=0 AND tintPageType=1 
		 where DDF.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) and DDF.numDomainID=@numDomainID AND ISNULL(DDF.bitSettingField,0)=1 AND ISNULL(DDF.bitDeleted,0)=0 AND ISNULL(DDF.bitCustom,0)=0
       
		 UNION
    
			select tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
		 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
		,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		 from View_DynamicCustomColumns
		 where numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0
		 AND ISNULL(bitCustom,0)=1

		END
		ELSE
		BEGIN
			--Check if Master Form Configuration is created by administrator if NoColumns=0
			DECLARE @IsMasterConfAvailable BIT = 0
			DECLARE @numUserGroup NUMERIC(18,0)

			SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

			IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = (CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
			BEGIN
				SET @IsMasterConfAvailable = 1
			END

			--If MasterConfiguration is available then load it otherwise load default columns
			IF @IsMasterConfAvailable = 1
			BEGIN
				INSERT INTO #tempForm
				SELECT 
					(intRowNum + 1) as tintOrder, vcDbColumnName, vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
					vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
					bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
					bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,1,vcFieldDataType
				FROM 
					View_DynamicColumnsMasterConfig
				WHERE
					View_DynamicColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
					View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
					View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
					View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
					ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
					ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
				UNION
				SELECT 
					tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
					 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
					,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
					bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
					intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
				FROM 
					View_DynamicCustomColumnsMasterConfig
				WHERE 
					View_DynamicCustomColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
					View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
					View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
					View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
					ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
				ORDER BY 
					tintOrder ASC  
			END
		END


SET @strSQL = ''

DECLARE @avgCost int
SET @avgCost = ISNULL((select TOP 1 numCost from Domain where numDOmainId=@numDomainID),0)



--------------Make Dynamic Query--------------
SET @strSQL = @strSQL + '
Select 
Opp.numoppitemtCode,Opp.vcNotes,WItems.numBackOrder,CAST(ISNULL(Opp.numCost,0) AS MONEY) AS numCost,
ISNULL(Opp.bitItemPriceApprovalRequired,0) AS bitItemPriceApprovalRequired,
ISNULL(Opp.numSOVendorID,0) AS numVendorID,'+
CONCAT('ISNULL(Opp.monTotAmount,0) - (ISNULL(Opp.numUnitHour,0) * (CASE ',@avgCost,' WHEN 3 THEN ISNULL(V.monCost,0) / dbo.fn_UOMConversion(numPurchaseUnit,I.numItemCode,',@numDomainID,',numBaseUnit) WHEN 2 THEN (CASE WHEN ISNULL(Opp.numSOVendorID,0) > 0 THEN Opp.numCost ELSE ISNULL(V.monCost,0) / dbo.fn_UOMConversion(numPurchaseUnit,I.numItemCode,',@numDomainID,',numBaseUnit) END) ELSE monAverageCost END)) AS vcMargin,')
+'
OM.tintOppType,
Opp.numItemCode,
I.charItemType,
ISNULL(I.bitAsset,0) as bitAsset,
ISNULL(I.bitRental,0) as bitRental,
ISNULL(I.bitSerialized,0) bitSerialized,
ISNULL(I.bitLotNo,0) bitLotNo,
Opp.numOppId,
Opp.bitWorkOrder,
Opp.fltDiscount,
ISNULL(Opp.bitDiscountType,0) bitDiscountType,
dbo.fn_UOMConversion(Opp.numUOMId, Opp.numItemCode,' + CAST(@numDomainId AS VARCHAR) +', NULL) AS UOMConversionFactor,
ISNULL(Opp.bitWorkOrder,0) bitWorkOrder,
ISNULL(Opp.numUnitHourReceived,0) numUnitHourReceived,
ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=Opp.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS numSerialNoAssigned,
CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON ob.numOppBizDocsId=obi.numOppBizDocID
					AND (ob.bitAuthoritativeBizDocs=1 OR ob.numBizDocId=304) AND obi.numOppItemID=Opp.numoppitemtCode AND ob.numOppId=Opp.numOppId)>0 THEN 1 ELSE 0 END  AS bitIsAuthBizDoc,
(CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON OB.numOppBizDocsId=OBI.numOppBizDocID WHERE OB.numOppId = Opp.numOppId AND OBI.numOppItemID=Opp.numoppitemtCode AND OB.numBizDocId=296) > 0 THEN 1 ELSE 0 END) AS bitAddedFulFillmentBizDoc,
ISNULL(numPromotionID,0) AS numPromotionID,
ISNULL(( SELECT numReturnID FROM   Returns WHERE  numOppItemCode = Opp.numoppitemtCode AND numOppId = Opp.numOppID), 0) AS numReturnID,
dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) AS ReturrnedQty,
[dbo].fn_GetReturnStatus(Opp.numoppitemtCode) AS ReturnStatus,
ISNULL(opp.numUOMId, 0) AS numUOM,
(SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
Opp.numUnitHour AS numOriginalUnitHour, 
isnull(M.vcPOppName,''Internal'') as Source,
numSourceID,
  CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END as bitKitParent,
(CASE WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 THEN 
(CASE WHEN (I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)THEN dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,Opp.numWarehouseItmsID,(CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END)) ELSE 0 END) ELSE 0 END) AS bitBackOrder,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(Opp.numQtyShipped,0) numQtyShipped,ISNULL(Opp.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(OM.fltExchangeRate,0) AS fltExchangeRate,ISNULL(OM.numCurrencyID,0) AS numCurrencyID,ISNULL(OM.numDivisionID,0) AS numDivisionID,
(CASE 
	WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 AND UPPER(I.charItemType) = ''P'' AND bitDropShip = 0
	THEN (CASE WHEN OM.tintShipped = 1 THEN CAST(ISNULL(Opp.numUnitHour,0) AS VARCHAR) ELSE CAST(ISNULL(Opp.numQtyShipped,0) AS VARCHAR) END)
	WHEN OM.tintOppType = 2 AND OM.tintOppStatus = 1 AND UPPER(I.charItemType) = ''P'' AND bitDropShip = 0
	THEN (CASE WHEN OM.tintShipped = 1 THEN CAST(ISNULL(Opp.numUnitHour,0) AS VARCHAR) ELSE CAST(ISNULL(Opp.numUnitHourReceived,0) AS VARCHAR) END)
	ELSE '''' END) AS vcShippedReceived,
isnull(i.numIncomeChartAcntId,0) as itemIncomeAccount,isnull(i.numAssetChartAcntId,0) as itemInventoryAsset,isnull(i.numCOGsChartAcntId,0) as itemCoGs,
ISNULL(Opp.numRentalIN,0) as numRentalIN,
ISNULL(Opp.numRentalLost,0) as numRentalLost,
ISNULL(Opp.numRentalOut,0) as numRentalOut,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(bitFreeShipping,0) AS [IsFreeShipping],Opp.bitDiscountType,isnull(Opp.monLandedCost,0) as monLandedCost, ISNULL(Opp.vcPromotionDetail,'''') AS vcPromotionDetail,
(CASE WHEN I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 THEN dbo.GetOppItemReleaseDates(om.numDomainId,Opp.numOppId,Opp.numoppitemtCode,1) ELSE '''' END) AS vcItemReleaseDate,'''' AS numPurchasedQty, '

--	IF @fld_Name = 'vcPathForTImage' 
		SET @strSQL = @strSQL + 'Opp.vcPathForTImage,'
--	ELSE IF @fld_Name = 'vcItemName' 
		SET @strSQL = @strSQL + 'Opp.vcItemName,'
--	ELSE IF @fld_Name = 'vcModelID,' 
		SET @strSQL = @strSQL + 'Opp.vcModelID,'
--	ELSE IF @fld_Name = 'vcWareHouse' 
		SET @strSQL = @strSQL + 'vcWarehouse,WItems.numWarehouseID,'
		SET @strSQL = @strSQL + 'WItems.numOnHand,isnull(WItems.numAllocation,0) as numAllocation,'
		SET @strSQL = @strSQL + 'WL.vcLocation,'
--	ELSE IF @fld_Name = 'ItemType' 
		SET @strSQL = @strSQL + 'Opp.vcType ItemType,Opp.vcType,'
--	ELSE IF @fld_Name = 'vcItemDesc' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.vcItemDesc, '''') vcItemDesc,'
--	ELSE IF @fld_Name = 'Attributes' 
		SET @strSQL = @strSQL + 'Opp.vcAttributes,' --dbo.fn_GetAttributes(Opp.numWarehouseItmsID, I.bitSerialized) AS Attributes,
--	ELSE IF @fld_Name = 'bitDropShip' 
		SET @strSQL = @strSQL + 'ISNULL(bitDropShip, 0) as DropShip,CASE ISNULL(bitDropShip, 0) WHEN 1 THEN ''Yes'' ELSE '''' END AS bitDropShip,'
--	ELSE IF @fld_Name = 'numUnitHour' 
		SET @strSQL = @strSQL + 'CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,om.numDomainId, ISNULL(opp.numUOMId, 0))* Opp.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,'
--	ELSE IF @fld_Name = 'monPrice' 
		SET @strSQL = @strSQL + 'Opp.monPrice,'
--	ELSE IF @fld_Name = 'monPriceUOM' 
		SET @strSQL = @strSQL + 'CAST(( dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), Opp.numItemCode,om.numDomainId, ISNULL(I.numBaseUnit, 0)) * Opp.monPrice) AS NUMERIC(18, 4)) AS monPriceUOM,'
--	ELSE IF @fld_Name = 'monTotAmount' 
		SET @strSQL = @strSQL + 'Opp.monTotAmount,Opp.monTotAmtBefDiscount,'
--	ELSE IF @fld_Name = 'vcSKU' 
		SET @strSQL = @strSQL + 'ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'''')) [vcSKU],'
--	ELSE IF @fld_Name = 'vcManufacturer' 
		SET @strSQL = @strSQL + 'Opp.vcManufacturer,'
--	ELSE IF @fld_Name = 'vcItemClassification' 
		SET @strSQL = @strSQL + ' dbo.[fn_GetListItemName](I.[numItemClassification]) numItemClassification,'
--	ELSE IF @fld_Name = 'numItemGroup' 
		SET @strSQL = @strSQL + '( SELECT    [vcItemGroup] FROM      [ItemGroups] WHERE     [numItemGroupID] = I.numItemGroup ) numItemGroup,'
--	ELSE IF @fld_Name = 'vcUOMName' 
		SET @strSQL = @strSQL + 'ISNULL(u.vcUnitName, '''') numUOMId,'
--	ELSE IF @fld_Name = 'fltWeight' 
		SET @strSQL = @strSQL + 'I.fltWeight,'
--	ELSE IF @fld_Name = 'fltHeight' 
		SET @strSQL = @strSQL + 'I.fltHeight,'
--	ELSE IF @fld_Name = 'fltWidth' 
		SET @strSQL = @strSQL + 'I.fltWidth,'
--	ELSE IF @fld_Name = 'fltLength' 
		SET @strSQL = @strSQL + 'I.fltLength,'
--	ELSE IF @fld_Name = 'numBarCodeId' 
		SET @strSQL = @strSQL + 'I.numBarCodeId,'
		SET @strSQL = @strSQL + 'I.IsArchieve,ISNULL(I.numContainer,0) AS numContainer,ISNULL(I.numNoItemIntoContainer,0) AS numContainerQty,'
--	ELSE IF @fld_Name = 'vcWorkOrderStatus' 
		SET @strSQL = @strSQL + 'CASE WHEN ISNULL(Opp.bitWorkOrder, 0) = 1
				 THEN ( SELECT  dbo.GetListIemName(numWOStatus)
								+ CASE WHEN ISNULL(numWOStatus, 0) = 23184
									   THEN '' - ''
											+ CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo),
														  '''') + '',''
											+ CONVERT(VARCHAR(20), DATEADD(minute, '+ CONVERT(VARCHAR(15),-@ClientTimeZoneOffset)  +', bintCompletedDate)) AS VARCHAR(100))
									   ELSE ''''
								  END
						FROM    WorkOrder
						WHERE   ISNULL(numOppId, 0) = Opp.numOppId
								AND numItemCode = Opp.numItemCode AND ISNULL(numParentWOID,0)=0
					  )
				 ELSE ''''
			END AS vcWorkOrderStatus,'
--	ELSE IF @fld_Name = 'DiscAmt' 
		SET @strSQL = @strSQL + 'CASE WHEN Opp.bitDiscountType = 0
				 THEN REPLACE(STR(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount,20,4), '' '', '''') + '' (''
					  + CAST(FORMAT(Opp.fltDiscount,''0.00##'') AS VARCHAR(50)) + ''%)''
				 ELSE REPLACE(STR(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount,20,4), '' '', '''')
			END AS DiscAmt,'	
--	ELSE IF @fld_Name = 'numProjectID' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectID, 0) numProjectID,
		 CASE WHEN ( SELECT TOP 1
								ISNULL(OBI.numOppBizDocItemID, 0)
						FROM    dbo.OpportunityBizDocs OBD
								INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID
						WHERE   OBI.numOppItemID = Opp.numoppitemtCode
								AND ISNULL(OBD.bitAuthoritativeBizDocs, 0) = 1
								and isnull(OBD.numOppID,0)=OM.numOppID
					  ) > 0 THEN 1
				 ELSE 0
			END AS bitItemAddedToAuthBizDoc,'	
--	ELSE IF @fld_Name = 'numClassID' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.numClassID, 0) numClassID,'	
--	ELSE IF @fld_Name = 'vcProjectStageName' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectStageID, 0) numProjectStageID,
				CASE WHEN OPP.numProjectStageID > 0
				 THEN dbo.fn_GetProjectStageName(OPP.numProjectId,
												 OPP.numProjectStageID)
				 WHEN OPP.numProjectStageID = -1 THEN ''T&E''
				 WHEN OPP.numProjectId > 0
				 THEN CASE WHEN OM.tintOppType = 1 THEN ''S.O.''
						   ELSE ''P.O.''
					  END
				 ELSE ''-''
			END AS vcProjectStageName,'	
	   
	   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='vcInclusionDetails')
	   BEGIN
			SET @strSQL = @strSQL + 'dbo.GetOrderAssemblyKitInclusionDetails(OM.numOppID,Opp.numoppitemtCode,Opp.numUnitHour) AS vcInclusionDetails,'
	   END     
	
	-- (CASE WHEN @tintOppType=1 THEN 26 ELSE 129 END) --

--------------Add custom fields to query----------------
SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM View_DynamicCustomColumns
WHERE 
grp_id = 5 AND numDomainID=@numDomainId
AND numFormId=26 AND bitCustom=1 AND numUserCntID = @numUserCntID ORDER BY numFieldId
WHILE @fld_ID > 0
BEGIN ------------------

	 PRINT @fld_Name
	
	       SET @strSQL = @strSQL + 'dbo.GetCustFldValueItem('
                                    + CONVERT(VARCHAR(15), @fld_ID)
                                    + ',Opp.numItemCode) as ''' + @fld_Name + ''','
       

SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM View_DynamicCustomColumns
WHERE 
grp_id = 5 AND numDomainID=@numDomainId
AND numFormId=26 AND bitCustom=1  AND numUserCntID = @numUserCntID AND numFieldId > @fld_ID ORDER BY numFieldId
 IF @@ROWCOUNT=0
 SET @fld_ID = 0
END ------------------


                                
--Purchase Opportunity ?
IF @tintOppType = 2 
    BEGIN
        SET @strSQL =  @strSQL + 'ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo,
		  SUBSTRING(( SELECT  '', '' + vcSerialNo
                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                THEN '' (''
                                    + CONVERT(VARCHAR(15), oppI.numQty)
                                    + '')''
                                ELSE ''''
                            END
                FROM    OppWarehouseSerializedItem oppI
                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                WHERE   oppI.numOppID = Opp.numOppId
                        AND oppI.numOppItemID = Opp.numoppitemtCode
                ORDER BY vcSerialNo
                FOR
                XML PATH('''')
                ), 3, 200000) AS vcSerialLotNo,
		  CAST(Opp.numUnitHour AS FLOAT) numQty,
		  (SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  0 AS bitBarcodePrint,
		  Opp.numWarehouseItmsID as numWarehouseItmsID,'
	END
ELSE
	BEGIN	
        SET @strSQL =  @strSQL + ' ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo,
		   SUBSTRING(( SELECT  '', '' + vcSerialNo
                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                THEN '' (''
                                    + CONVERT(VARCHAR(15), oppI.numQty)
                                    + '')''
                                ELSE ''''
                            END
                FROM    OppWarehouseSerializedItem oppI
                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                WHERE   oppI.numOppID = Opp.numOppId
                        AND oppI.numOppItemID = Opp.numoppitemtCode
                ORDER BY vcSerialNo
                FOR
                XML PATH('''')
                ), 3, 200000) AS vcSerialLotNo,
		   CAST(Opp.numUnitHour AS FLOAT) numQty,
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  Opp.numWarehouseItmsID as numWarehouseItmsID,'
	END


        
 SET @strSQL = left(@strSQL,len(@strSQL)-1) + '   
        FROM    OpportunityItems Opp
        LEFT JOIN OpportunityMaster OM ON Opp.numOppID = oM.numOppID
        JOIN item I ON Opp.numItemCode = i.numItemcode
        LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = Opp.numWarehouseItmsID
        LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
        LEFT JOIN WarehouseLocation WL ON WL.numWLocationID = WItems.numWLocationID
        LEFT JOIN OpportunityMaster M ON Opp.numSourceID = M.numOppID
        LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId
		LEFT JOIN Vendor V ON I.numVendorID = V.numVendorID  AND V.numItemCode = I.numItemCode
WHERE   om.numDomainId = '+ CONVERT(VARCHAR(10),@numDomainId) + '
        AND Opp.numOppId = '+ CONVERT(VARCHAR(15),@OpportunityId) +'
ORDER BY opp.numSortOrder,numoppitemtCode ASC '
  
PRINT @strSQL;
EXEC (@strSQL);
SELECT  vcSerialNo,
       vcComments AS Comments,
        numOppItemID AS numoppitemtCode,
        W.numWarehouseItmsDTLID,
        numWarehouseItmsID AS numWItmsID,
        dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                             1) Attributes
FROM    WareHouseItmsDTL W
        JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
WHERE   numOppID = @OpportunityId


select * from #tempForm order by tintOrder

drop table #tempForm

            END
            
    END
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount money =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(9),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0,
  @numShipmentMethod NUMERIC(18,0)=0,
  @dtReleaseDate DATE = NULL,
  @numPartner NUMERIC(18,0)=0,
  @tintClickBtn INT=0,
  @numPartenerContactId NUMERIC(18,0)=0,
  @numAccountClass NUMERIC(18,0) = 0,
  @numWillCallWarehouseID NUMERIC(18,0) = 0,
  @numVendorAddressID NUMERIC(18,0) = 0
)                                                                          
                                                                          
as  
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @fltExchangeRate float                                 
	DECLARE @hDocItem int                                                                                                                            
	DECLARE @TotalAmount as money                                                                          
	DECLARE @tintOppStatus as tinyint
	DECLARE @bitNewOrder AS BIT = (CASE WHEN ISNULL(@numOppID,0) = 0 THEN 1 ELSE 0 END)
	
	  
	
	IF ISNULL(@tintOppType,0) = 0
	BEGIN
		RAISERROR('OPP_TYPE_CAN_NOT_BE_0',16,1)
		RETURN
	END             

	IF CONVERT(VARCHAR(10),@strItems) <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

		--VALIDATE PROMOTION OF COUPON CODE
		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1),
			numOppItemID NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numOppItemID,
			numPromotionID
		)
		SELECT
			numoppitemtCode,
			numPromotionID
		FROM
			OPENXML (@hDocItem,'/NewDataSet/Item',2)
		WITH
		(
			numoppitemtCode NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		IF (SELECT
				COUNT(*)
			FROM
				PromotionOffer PO
			WHERE
				numDomainId=@numDomainId
				AND bitRequireCouponCode = 1
				AND ISNULL(bitEnabled,0) = 1
				AND ISNULL(tintUsageLimit,0) > 0
				AND (intCouponCodeUsed 
					- (CASE WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE numOppID=ISNULL(@numOppID,0) AND numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END) 
					+ (CASE WHEN ISNULL((SELECT COUNT(*) FROM @TEMP WHERE numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END)) > tintUsageLimit) > 0
		BEGIN
			RAISERROR('USAGE_OF_COUPON_EXCEED',16,1)
			RETURN
		END
		ELSE
		BEGIN
			UPDATE
				PO
			SET 
				intCouponCodeUsed = ISNULL(intCouponCodeUsed,0) 
									- (CASE WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE numOppID=ISNULL(@numOppID,0) AND numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END) 
									+ (CASE WHEN ISNULL(T1.intUsed,0) > 0 THEN 1 ELSE 0 END) 
			FROM
				PromotionOffer PO
			INNER JOIN
				(
					SELECT
						numPromotionID,
						COUNT(*) AS intUsed
					FROM
						@TEMP
					GROUP BY
						numPromotionID
				) AS T1
			ON
				PO.numProId = T1.numPromotionID
			WHERE
				numDomainId=@numDomainId
				AND bitRequireCouponCode = 1
				AND ISNULL(bitEnabled,0) = 1
		END
	END

	--IF NEW OPPERTUNITY
	IF @numOppID = 0                                                                          
	BEGIN
		DECLARE @bitIsInitalSalesOrder BIT
		IF(@DealStatus=1 AND @tintOppType=1)
		BEGIN
			SET @bitIsInitalSalesOrder=1
		END

		DECLARE @intOppTcode AS NUMERIC(9)  
  
		SELECT @intOppTcode = MAX(numOppId) FROM OpportunityMaster                

		SET @intOppTcode = ISNULL(@intOppTcode,0) + 1                                                
		
		SET @vcPOppName = @vcPOppName + '-'+ CONVERT(varchar(4),YEAR(GETUTCDATE()))  +'-A'+ CONVERT(varchar(10),@intOppTcode)                                  
  
		IF ISNULL(@numCurrencyID,0) = 0 
		BEGIN
			SET @fltExchangeRate=1
			SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
		END
		ELSE
		BEGIN 
			SET @fltExchangeRate = dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
        END  
		                                                        
		IF ISNULL(@numAccountClass,0) = 0
		BEGIN                                                  
		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivisionID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
		END

        IF(@DealStatus=1 AND @tintOppType=1 AND @tintClickBtn=1)
		BEGIN
			SET @DealStatus=0
		END         
		--PRINT ('Deal Staus ' +@DealStatus)                                                      
		INSERT INTO OpportunityMaster
		(                                                                             
			numContactId,numDivisionId,txtComments,numCampainID,bitPublicFlag,tintSource,tintSourceType,vcPOppName,                                                                          
			intPEstimatedCloseDate,monPAmount,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,                                                                                                             
			numRecOwner,lngPConclAnalysis,tintOppType,numSalesOrPurType,numCurrencyID,fltExchangeRate,numAssignedTo,
			numAssignedBy,[tintOppStatus],numStatus,vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,
			bitInterestType,fltInterest,vcCouponCode,bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,
			vcMarketplaceOrderReportId,numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
			bitUseMarkupShippingRate,numMarkupShippingRate,intUsedShippingCompany,numShipmentMethod,numPartner,numPartenerContact,dtReleaseDate,bitIsInitalSalesOrder,numVendorAddressID
		)                                                                          
		Values                                                                          
		(                                                                          
			@numContactId,@numDivisionId,ISNULL(@Comments,''),@CampaignID,@bitPublicFlag,@tintSource,@tintSourceType,@vcPOppName,                                                                          
			@dtEstimatedCloseDate,@monPAmount,@numUserCntID,(CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END),                                                                          
			@numUserCntID,GETUTCDATE(),@numDomainId,@numUserCntID,@lngPConclAnalysis,@tintOppType,@numSalesOrPurType,@numCurrencyID, 
			@fltExchangeRate,(CASE WHEN @numAssignedTo > 0 THEN @numAssignedTo ELSE NULL END),(CASE WHEN @numAssignedTo>0 THEN @numUserCntID ELSE NULL END),
			@DealStatus,@numStatus,@vcOppRefOrderNo,@bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,
			@vcCouponCode,@bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,@numPercentageComplete,
			@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,@bitUseMarkupShippingRate,@numMarkupShippingRate,@intUsedShippingCompany,@numShipmentMethod,@numPartner,@numPartenerContactId,@dtReleaseDate,@bitIsInitalSalesOrder,@numVendorAddressID
		)                                                                                                                      
		
		SET @numOppID=(SELECT SCOPE_IDENTITY())                
                            
		
		--UPDATE OPPNAME AS PER NAME TEMPLATE
		EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

		--MAP CUSTOM FIELD	
		DECLARE @tintPageID AS TINYINT;
		SET @tintPageID = CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
		EXEC dbo.USP_AddParentChildCustomFieldMap
				@numDomainID = @numDomainID, --  numeric(18, 0)
				@numRecordID = @numOppID, --  numeric(18, 0)
				@numParentRecId = @numDivisionId, --  numeric(18, 0)
				@tintPageID = @tintPageID --  tinyint
 	
		IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
		BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END
  
		-- INSERTING ITEMS                                                                                                                    
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,
				monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
				numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,vcAttrValues,monAvgCost,bitItemPriceApprovalRequired,
				numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,numCost,vcNotes
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
				(CASE X.numWarehouseItmsID 
				WHEN -1 
				THEN 
					CASE 
						WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU= X.vcSKU) > 0)
							THEN
								(SELECT TOP 1 
									numWareHouseItemID 
								FROM 
									WareHouseItems 
								WHERE 
									[numItemID] = X.numItemCode 
									AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
						ELSE
							(SELECT 
								[numWareHouseItemID] 
							FROM 
								[WareHouseItems] 
							WHERE 
								[numItemID] = X.numItemCode 
								AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				WHEN 0 
				THEN 
					CASE 
					WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU=X.vcSKU) > 0)
						THEN
							(SELECT TOP 1 
								numWareHouseItemID 
							FROM 
								WareHouseItems 
							WHERE 
								[numItemID] = X.numItemCode 
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
					ELSE
						(SELECT 
							[numWareHouseItemID] 
						FROM 
							[WareHouseItems] 
						WHERE 
							[numItemID] = X.numItemCode 
							AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
							AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				ELSE  
					X.numWarehouseItmsID 
				END) AS numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
				(SELECT isnull(VN.monCost,0) FROM Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode AND VN.numVendorID=IT.numVendorID WHERE  VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,
				0,
				X.numToWarehouseItemID,X.Attributes,X.AttributeIDs,(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),
				X.bitItemPriceApprovalRequired,X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(X.vcVendorNotes,'')
			FROM
				(
					SELECT 
						*
					FROM 
						OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
					WITH                       
					(                                                                          
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9), numUnitHour FLOAT, monPrice DECIMAL(30,16), monTotAmount MONEY,vcItemDesc VARCHAR(1000),                                    
						numWarehouseItmsID NUMERIC(9), ItemType VARCHAR(30), DropShip BIT, bitDiscountType BIT,	fltDiscount DECIMAL(30,16), monTotAmtBefDiscount MONEY,
						vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),
						numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),AttributeIDs VARCHAR(500),vcSKU VARCHAR(100),bitItemPriceApprovalRequired BIT,
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost MONEY,vcVendorNotes VARCHAR(300)
					)
				)X    
			ORDER BY 
				X.numoppitemtCode
    
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,numWarehouseItmsID=X.numWarehouseItmsID,bitDropShip=X.DropShip,
				bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
				bitWorkOrder=X.bitWorkOrder,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,
				numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,vcAttributes=X.Attributes,
				numClassID=0,vcModelID=X.vcModelID,
				bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired,vcAttrValues=X.AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost,vcNotes=X.vcVendorNotes
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,
					monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,
					Attributes,bitItemPriceApprovalRequired,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) numCost,ISNULL(vcVendorNotes,'') vcVendorNotes
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
				WITH  
					(                      
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount MONEY,vcItemDesc VARCHAR(1000), vcModelID VARCHAR(300),
						numWarehouseItmsID NUMERIC(9),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount MONEY,vcItemName VARCHAR(300),
						numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),
						numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost MONEY,vcVendorNotes VARCHAR(300)
					)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppID=@numOppID
				
			--INSERT KIT ITEMS                 
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ID.numWareHouseItemId,
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0
			FROM 
				OpportunityItems OI 
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId 

			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
				DECLARE @TempKitConfiguration TABLE
				(
					numItemCode NUMERIC(18,0),
					numWarehouseItemID NUMERIC(18,0),
					KitChildItems VARCHAR(MAX)
				)

				INSERT INTO 
					@TempKitConfiguration
				SELECT
					numItemCode,
					numWarehouseItmsID,
					KitChildItems
				FROM
					OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)
				WITH                       
				(
					numItemCode NUMERIC(9),numWarehouseItmsID NUMERIC(9), KitChildItems VARCHAR(MAX)
				) 

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					ItemDetails.numWareHouseItemId,
					(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0
				FROM
					(
						SELECT 
							t1.numItemCode,
							t1.numWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										0,
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,0,CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										0, 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemWarehouseItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration WHERE numItemCode=t1.numItemCode AND numWarehouseItemID=t1.numWarehouseItemID),',') 
						) as t2
					) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numChildItemID=TempChildItems.numKitItemID
					AND OKI.numWarehouseItemId=TempChildItems.numKitWarehouseItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numWarehouseItmsID = TempChildItems.numWarehouseItemID
					AND OI.numoppitemtcode = OKI.numOppItemID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					AND (TempChildItems.numKitChildItemWarehouseItemID = ItemDetails.numWareHouseItemId  OR ISNULL(TempChildItems.numKitChildItemWarehouseItemID,0) = 0)
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END

			-- UPDATE UOM OF OPPORTUNITY ITEMS HAVE NOT UOM SET WHILE EXPORTED FROM MARKETPLACE (MEANS TINTSOURCETYPE = 3)
			IF ISNULL(@tintSourceType,0) = 3
			BEGIN
				UPDATE 
					OI 
				SET 
					numUOMID = ISNULL(I.numSaleUnit,0), 
					numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
				FROM 
					OpportunityItems OI
				JOIN 
					Item I 
				ON 
					OI.numItemCode = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND I.numDomainID = @numDomainId
			END	
                                     
			INSERT INTO OppWarehouseSerializedItem
			(
				numOppID,
				numWarehouseItmsDTLID,
				numOppItemID,
				numWarehouseItmsID
			)                      
			SELECT 
				@numOppID,
				X.numWarehouseItmsDTLID,
				X.numoppitemtCode,
				X.numWItmsID 
			FROM
			(                                                                          
				SELECT
					* 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
				WITH                        
				(                                               
				numWarehouseItmsDTLID numeric(9),                      
				numoppitemtCode numeric(9),                      
				numWItmsID numeric(9)                                                     
				)
			)X                                    
                                   
			UPDATE 
				OppWarehouseSerializedItem                       
			SET 
				numOppItemID=X.numoppitemtCode                                
			FROM                       
			(
				SELECT 
					numWarehouseItmsDTLID AS DTLID,
					O.numoppitemtCode 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
				WITH
				(
					numWarehouseItmsDTLID NUMERIC(9),numoppitemtCode NUMERIC(9),numWItmsID NUMERIC(9)
				)                                
				JOIN 
					OpportunityItems O 
				ON 
					O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID
			)X                 
			WHERE 
				numOppID=@numOppID 
				AND numWarehouseItmsDTLID=X.DTLID      
    
			EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
                     
			EXEC sp_xml_removedocument @hDocItem                                     
		END          
	END
	ELSE                                                                                                                          
	BEGIN
		DECLARE @tempAssignedTo AS NUMERIC(9) = NULL                                                    
		SELECT 
			@tintOppStatus = tintOppStatus,
			@tempAssignedTo=ISNULL(numAssignedTo,0) 
		FROM   
			OpportunityMaster 
		WHERE  
			numOppID = @numOppID

		IF @tempAssignedTo <> @numAssignedTo
		BEGIN
			IF (SELECT COUNT(*) FROM BizDocComission WHERE ISNULL(numOppID,0) = @numOppID AND ISNULL(bitCommisionPaid,0) = 1) > 0
			BEGIN
				RAISERROR('COMMISSION_PAID_AGAINST_BIZDOC',16,1)
			END
		END
 
		--REVERTING BACK THE WAREHOUSE ITEMS                  
		IF @tintOppStatus = 1 
		BEGIN               
			EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID                  
		END                  

		IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
		BEGIN
			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
		END
		
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
		BEGIN
			EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END 
						
		UPDATE   
			OpportunityMaster
		SET      
			vcPOppName = @vcPOppName,txtComments = ISNULL(@Comments,''),bitPublicFlag = @bitPublicFlag,numCampainID = @CampaignID,tintSource = @tintSource,tintSourceType=@tintSourceType,
			intPEstimatedCloseDate = @dtEstimatedCloseDate,numModifiedBy = @numUserCntID,bintModifiedDate = GETUTCDATE(),lngPConclAnalysis = @lngPConclAnalysis,monPAmount = @monPAmount,
			tintActive = @tintActive,numSalesOrPurType = @numSalesOrPurType,numStatus = @numStatus,vcOppRefOrderNo=@vcOppRefOrderNo,
			bintOppToOrder=(CASE WHEN @tintOppStatus=0 AND @DealStatus=1 THEN GETUTCDATE() ELSE bintOppToOrder END),bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,
			bitBillingTerms=@bitBillingTerms,intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,tintTaxOperator=@tintTaxOperator,
			numDiscountAcntType=@numDiscountAcntType,vcCouponCode = @vcCouponCode,numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
			bitUseMarkupShippingRate = @bitUseMarkupShippingRate,numMarkupShippingRate = @numMarkupShippingRate,intUsedShippingCompany = @intUsedShippingCompany, numContactID=@numContactId,numShipmentMethod=@numShipmentMethod,numPartner=@numPartner,numPartenerContact=@numPartenerContactId
		WHERE
			numOppId = @numOppID   
	   
		-- UPDATING IF ORGANIZATION IS ASSIGNED TO SOMEONE                                                      
		IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN
			UPDATE
				OpportunityMaster 
			SET	
				numAssignedTo = @numAssignedTo, 
				numAssignedBy = @numUserCntID 
			WHERE 
				numOppId = @numOppID                                                    
		END                                                     
		ELSE IF(@numAssignedTo = 0) 
		BEGIN
			UPDATE 
				OpportunityMaster 
			SET 
				numAssignedTo = 0, 
				numAssignedBy = 0 
			WHERE
				numOppId = @numOppID
		END  
			                                                                    
		-- UPDATING OPP ITEMS
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0                                                                         
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   
			--DELETE CHILD ITEMS OF KIT WITHIN KIT
			DELETE FROM 
				OpportunityKitChildItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   

			--DELETE ITEMS
			DELETE FROM 
				OpportunityKitItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))                      
			                            
			DELETE FROM 
				OpportunityItems 
			WHERE 
				numOppID=@numOppID 
				AND numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   
	          
			--INSERT NEW ADDED ITEMS                        
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,
				[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numToWarehouseItemID,numShipmentMethod,
				numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired,vcAttrValues,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,numCost,vcNotes
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
				(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),
				(select isnull(VN.monCost,0) from Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode and VN.numVendorID=IT.numVendorID where VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numToWarehouseItemID,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
				0,
				(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired, X.AttributeIDs,
				X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(vcVendorNotes,'')
			FROM
			(
				SELECT 
					*
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH  
				(                      
					numoppitemtCode numeric(9),numItemCode numeric(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount money,vcItemDesc varchar(1000),                                    
					numWarehouseItmsID numeric(9),numToWarehouseItemID numeric(9),Op_Flag tinyint,ItemType varchar(30),DropShip bit,bitDiscountType bit,
					fltDiscount DECIMAL(30,16),monTotAmtBefDiscount MONEY,vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),
					numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(500), 
					bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000)
					,numSortOrder NUMERIC(18,0),numCost MONEY,vcVendorNotes VARCHAR(300)
				)
			)X 
			
			-- UPDATE ITEMS
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,vcModelID=X.vcModelID,numWarehouseItmsID=X.numWarehouseItmsID,numToWarehouseItemID = X.numToWarehouseItemID,             
				bitDropShip=X.DropShip,bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,
				numUOMId=numUOM,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,
				numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired,bitWorkOrder=X.bitWorkOrder,vcAttrValues=AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost,vcNotes=vcVendorNotes
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,numToWarehouseItemID,DropShip,bitDiscountType,
					fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
					bitItemPriceApprovalRequired,bitWorkOrder,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) AS numCost,ISNULL(vcVendorNotes,'') vcVendorNotes
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
				WITH  
				(                      
					numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount MONEY,vcItemDesc VARCHAR(1000),vcModelID VARCHAR(300), numWarehouseItmsID NUMERIC(9), 
					numToWarehouseItemID NUMERIC(18,0),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount MONEY,vcItemName VARCHAR(300),numUOM NUMERIC(9),
					numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),numProjectStageID NUMERIC(9),Attributes VARCHAR(500),
					bitItemPriceApprovalRequired BIT, bitWorkOrder BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost MONEY,vcVendorNotes VARCHAR(300)                           
				)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppId=@numOppID

			--INSERT NEW ADDED KIT ITEMS                 
			INSERT INTO OpportunityKitItems                                                                          
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ID.numWareHouseItemId,
				ID.numQtyItemsReq * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0
			FROM 
				OpportunityItems OI 
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			WHERE 
				OI.numOppId=@numOppId 
				AND OI.numoppitemtCode NOT IN (SELECT ISNULL(numOppItemID,0) FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID)  
				
			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
				DELETE FROM @TempKitConfiguration

				INSERT INTO 
					@TempKitConfiguration
				SELECT
					numItemCode,
					numWarehouseItmsID,
					KitChildItems
				FROM
					OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)
				WITH                       
				(
					numItemCode NUMERIC(9),numWarehouseItmsID NUMERIC(9), KitChildItems VARCHAR(MAX)
				) 

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					ItemDetails.numWareHouseItemId,
					ItemDetails.numQtyItemsReq * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0
				FROM
					(
						SELECT 
							t1.numItemCode,
							t1.numWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										0,
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,0,CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										0, 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemWarehouseItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration WHERE numItemCode=t1.numItemCode AND numWarehouseItemID=t1.numWarehouseItemID),',') 
						) as t2
					) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numChildItemID=TempChildItems.numKitItemID
					AND OKI.numWarehouseItemId=TempChildItems.numKitWarehouseItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numWarehouseItmsID = TempChildItems.numWarehouseItemID
					AND OI.numoppitemtcode = OKI.numOppItemID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					AND (TempChildItems.numKitChildItemWarehouseItemID = ItemDetails.numWareHouseItemId  OR ISNULL(TempChildItems.numKitChildItemWarehouseItemID,0) = 0)
			END                                  
			
			--UPDATE KIT ITEMS                 
			UPDATE 
				OKI 
			SET 
				numQtyItemsReq = numQtyItemsReq_Orig * OI.numUnitHour
			FROM 
				OpportunityKitItems OKI 
			JOIN 
				OpportunityItems OI 
			ON 
				OKI.numOppItemID=OI.numoppitemtCode 
				AND OKI.numOppId=OI.numOppId  
			WHERE 
				OI.numOppId=@numOppId 

			-- UPDATE CHILD ITEMS OF KIT WITHIN KIT
			UPDATE
				OKCI
			SET
				OKCI.numQtyItemsReq = (OKI.numQtyItemsReq * OKCI.numQtyItemsReq_Orig)
			FROM
				OpportunityKitChildItems OKCI
			INNER JOIN
				OpportunityKitItems OKI
			ON
				OKCI.numOppID = @numOppId
				AND OKCI.numOppItemID = OKI.numOppItemID
				AND OKCI.numOppChildItemID = OKI.numOppChildItemID                     
			                                    
			EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			   		                                                                      
			EXEC sp_xml_removedocument @hDocItem                                               
		END     

		--FOLLOWING DELETE STATEMENT IS COMMENTED BECUASE NOW WE ARE NOT ALLOWING USER TO EIDT SERIAL/LOT ITEM UNTILL USER REMOVED ASSIGNED SERIAL/LOT NUMBERS
	    --Delete previously added serial lot number added for opportunity
		--DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
	END


	SET @TotalAmount=0                                                           
	SELECT @TotalAmount = SUM(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID                                                                          
  
	UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numOppID

/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
            
			declare @bitApprovalforOpportunity bit
			declare @intOpportunityApprovalProcess int
			SELECT @bitApprovalforOpportunity=bitApprovalforOpportunity,@intOpportunityApprovalProcess=intOpportunityApprovalProcess 
					FROM Domain WHERE numDomainId=@numDomainID
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT

			SET @bitIsInitalSalesOrder=(SELECT bitIsInitalSalesOrder FROM OpportunityMaster WHERE numOppId=@numOppID AND numDomainId=@numDomainID)
			SET @bitViolatePrice=(SELECT bitMarginPriceViolated FROM Domain WHERE numDomainId=@numDomainID)
			
			IF(@bitViolatePrice=1 AND @bitIsInitalSalesOrder=1 AND @tintClickBtn = 1)
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus=@DealStatus WHERE numOppId=@numOppID
			END
			ELSE
			BEGIN
				IF(@bitApprovalforOpportunity=1)
				BEGIN
					IF(@DealStatus='1' OR @DealStatus='2')
					BEGIN
						IF(@intOpportunityApprovalProcess=1)
						BEGIN
							UPDATE OpportunityMaster SET intPromotionApprovalStatus=6,intChangePromotionStatus=@DealStatus
							WHERE numOppId=@numOppId
						END
						IF(@intOpportunityApprovalProcess=2)
						BEGIN
							UPDATE OpportunityMaster SET intPromotionApprovalStatus=1,intChangePromotionStatus=@DealStatus
							WHERE numOppId=@numOppId
						END
					END
				END

				UPDATE OpportunityMaster SET tintOppStatus = @DealStatus WHERE numOppId=@numOppID
				declare @tintShipped as tinyint               
				select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID              
				if @tintOppStatus=1               
					begin         
					PRINT 'inside USP_UpdatingInventoryonCloseDeal'
						exec USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
					end  
				declare @tintCRMType as numeric(9)        
				declare @AccountClosingDate as datetime        

				select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
				select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

				--When deal is won                                         
				IF @DealStatus = 1 
				BEGIN
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID              
					END

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=getutcdate()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
				END         
				--When Deal is Lost
				ELSE IF @DealStatus = 2
				BEGIN
					SELECT @AccountClosingDate=bintAccountClosingDate FROM OpportunityMaster WHERE numOppID=@numOppID         
					
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID
					END
				END 
			END

			/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
			-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
			if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
			begin        
				update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				where numDivisionID=@numDivisionID        
			end        

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9) 
DECLARE @bitIsPrimary BIT;
DECLARE @tintAddressOf TINYINT

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

--Bill Address
IF @numBillAddressId>0
BEGIN
 SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName,@tintAddressOf=ISNULL(tintAddressOf,0)
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numBillAddressId

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numBillAddressId
	END

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1
END
  
  
--Ship Address
IF @numShipmentMethod = 92 -- WILL CALL (PICK UP FROM Warehouse)
BEGIN
	SELECT  
		@vcStreet=ISNULL(vcWStreet,'')
		,@vcCity=ISNULL(vcWCity,'')
		,@vcPostalCode=ISNULL(vcWPinCode,'')
		,@numState=ISNULL(numWState,0)
		,@numCountry=ISNULL(numWCountry,0)
		,@vcAddressName=''
	FROM 
		dbo.Warehouses 
	WHERE 
		numDomainID = @numDomainID 
		AND numWareHouseID = @numWillCallWarehouseID

	UPDATE OpportunityMaster SET tintShipToType=2 WHERE numOppId=@numOppID
 
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = '', --  varchar(100)
	@numCompanyId =0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1
END
ELSE IF @numShipAddressId>0
BEGIN
	SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName,@tintAddressOf=ISNULL(tintAddressOf,0)
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numShipAddressId
 

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numShipAddressId
	END


  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1
END

-- IMPORTANT:KEEP THIS BELOW ADDRESS UPDATE LOGIC
-- GET TAX APPLICABLE TO DIVISION WHEN ORDER IS CREATED AFTER THAT NOT NEED TO CHANGE IT
IF @bitNewOrder = 1
BEGIN
	--INSERT TAX FOR DIVISION   
	IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
	BEGIN
		INSERT INTO dbo.OpportunityMasterTaxItems 
		(
			numOppId,
			numTaxItemID,
			fltPercentage,
			tintTaxType,
			numTaxID
		) 
		SELECT 
			@numOppID,
			TI.numTaxItemID,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			TaxItems TI 
		JOIN 
			DivisionTaxTypes DTT 
		ON 
			TI.numTaxItemID = DTT.numTaxItemID
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
		) AS TEMPTax
		WHERE 
			DTT.numDivisionID=@numDivisionId 
			AND DTT.bitApplicable=1
		UNION 
		SELECT 
			@numOppID,
			0,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			dbo.DivisionMaster 
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
		) AS TEMPTax
		WHERE 
			bitNoTax=0 
			AND numDivisionID=@numDivisionID	
		UNION 
		SELECT
			@numOppId
			,1
			,decTaxValue
			,tintTaxType
			,numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,@numOppId,1,NULL)		
	END	
END

--INSERT TAX FOR DIVISION   
IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN
--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID
) 
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID = IT.numTaxItemID 
WHERE 
	OI.numOppId=@numOppID 
	AND IT.bitApplicable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	0,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numOppID 
	AND I.bitTaxable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT
	@numOppId,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
FROM
	dbo.OpportunityItems OI 
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
INNER JOIN
	TaxDetails TD
ON
	TD.numTaxID = IT.numTaxID
	AND TD.numDomainId = @numDomainId
WHERE
	OI.numOppId = @numOppID
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

END
  
 UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID

	IF @tintOppType=1 AND @tintOppStatus=1 
	BEGIN
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numStatus
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityItems_GetMaxID')
DROP PROCEDURE USP_OpportunityItems_GetMaxID
GO
CREATE PROCEDURE [dbo].[USP_OpportunityItems_GetMaxID] 

AS
BEGIN
	SELECT MAX(numOppitemtcode) + 1 AS numOppItemID FROM OpportunityItems
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_OpportunityMaster_GetAddress' ) 
    DROP PROCEDURE USP_OpportunityMaster_GetAddress
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetAddress]
	@numDomainID NUMERIC(18,0)
    ,@numOppID AS NUMERIC(18,0)
	,@tintAddressType TINYINT
AS 
BEGIN
	IF @tintAddressType = 1 --Billing Address
	BEGIN
		EXEC usp_getoppaddress @numOppID,0,0
		SELECT dbo.fn_getOPPAddress(@numOppID,@numDomainID,1) AS BillingAdderss
	END
	ELSE IF @tintAddressType = 2 --Shipping Address
	BEGIN
		EXEC usp_getoppaddress @numOppID,1,0
		SELECT dbo.fn_getOPPAddress(@numOppID,@numDomainID,2) AS ShippingAdderss
	END

END
GO
/****** Object:  StoredProcedure [dbo].[USP_ShoppingCart]    Script Date: 05/07/2009 22:09:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_shoppingcart')
DROP PROCEDURE usp_shoppingcart
GO
CREATE PROCEDURE [dbo].[USP_ShoppingCart]                            
@numDivID as numeric(18),                            
@numOppID as numeric(18)=null output ,                                              
@vcPOppName as varchar(200)='' output,  
@numContactId as numeric(18),                            
@numDomainId as numeric(18),
@vcSource VARCHAR(50)=NULL,
@numCampainID NUMERIC(9)=0,
@numCurrencyID as numeric(9),
@numBillAddressId numeric(9),
@numShipAddressId numeric(9),
@bitDiscountType BIT,
@fltDiscount FLOAT,
@numProId NUMERIC(9),
@numSiteID NUMERIC(9),
@tintOppStatus INT,
@monShipCost MONEY,
@tintSource AS BIGINT ,
@tintSourceType AS TINYINT ,                          
@numPaymentMethodId NUMERIC(9),
@txtComments varchar(1000),
@numPartner NUMERIC(18,0),
@txtFuturePassword VARCHAR(50) = NULL,
@intUsedShippingCompany NUMERIC(18,0)=0,
@vcPartenerContact VARCHAR(200)='',
@numShipmentMethod NUMERIC(18,0)=0,
@vcOppRefOrderNo VARCHAR(200) = '',
@bitBillingTerms as bit,
@intBillingDays as integer,
@bitInterestType as bit,
@fltInterest as float
as                            
BEGIN TRY
BEGIN TRANSACTION          
declare @CRMType as integer               
declare @numRecOwner as integer       
DECLARE @numPartenerContact NUMERIC(18,0)=0
	
SET @numPartenerContact=(SELECT Top 1 numContactId FROM AdditionalContactsInformation WHERE numDomainId=@numDomainID AND numDivisionId=@numPartner AND vcEmail=@vcPartenerContact)
	
if @numOppID=  0                             
begin                            
select @CRMType=tintCRMType,@numRecOwner=numRecOwner from DivisionMaster where numDivisionID=@numDivID                            
if @CRMType= 0                             
begin                            
UPDATE DivisionMaster                               
   SET tintCRMType=1                              
   WHERE numDivisionID=@numDivID                              
		
end                            
declare @TotalAmount as FLOAT
--declare @intOppTcode as numeric(9) 
--declare @numCurrencyID as numeric(9)
--declare @fltExchangeRate as float                          
--Select @intOppTcode=max(numOppId)from OpportunityMaster                              
--set @intOppTcode =isnull(@intOppTcode,0) + 1        

		IF LEN(ISNULL(@txtFuturePassword,'')) > 0
		BEGIN
			DECLARE @numExtranetID NUMERIC(18,0)

			SELECT @numExtranetID=numExtranetID FROM ExtarnetAccounts WHERE numDivisionID=@numDivID

			UPDATE ExtranetAccountsDtl SET vcPassword=@txtFuturePassword WHERE numExtranetID=@numExtranetID
		END                      

		DECLARE @numTemplateID NUMERIC
		SELECT  @numTemplateID= numMirrorBizDocTemplateId FROM eCommercePaymentConfig WHERE  numSiteId=@numSiteID AND numPaymentMethodId = @numPaymentMethodId AND numDomainID = @numDomainID   

		Declare @fltExchangeRate float                                 
    
   IF @numCurrencyID=0 select @numCurrencyID=isnull(numCurrencyID,0) from Domain where numDomainID=@numDomainId                            
   if @numCurrencyID=0 set   @fltExchangeRate=1
  else set @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)                          
			                       
                            
		-- GET ACCOUNT CLASS IF ENABLED
		DECLARE @numAccountClass AS NUMERIC(18) = 0
	-- GET ACCOUNT CLASS IF ENABLED
		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

	IF @tintDefaultClassType <> 0
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numContactId
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
		SET @numAccountClass=(SELECT 
								  TOP 1 
								  numDefaultClass from dbo.eCommerceDTL 
								  where 
								  numSiteId=@numSiteID 
								  and numDomainId=@numDomainId)                  
--set @vcPOppName=@vcPOppName+ '-'+convert(varchar(4),year(getutcdate()))  +'-A'+ convert(varchar(10),@intOppTcode)                              
  --SELECT tintSource , tintSourceType FROM dbo.OpportunityMaster
		insert into OpportunityMaster                              
		  (                              
		  numContactId,                              
		  numDivisionId,                              
		  txtComments,                              
		  numCampainID,                              
		  bitPublicFlag,                              
		  tintSource, 
		  tintSourceType ,                               
		  vcPOppName,                              
		  monPAmount,                              
		  numCreatedBy,                              
		  bintCreatedDate,                               
		  numModifiedBy,                              
		  bintModifiedDate,                              
		  numDomainId,                               
		  tintOppType, 
		  tintOppStatus,                   
		  intPEstimatedCloseDate,              
		  numRecOwner,
		  bitOrder,
		  numCurrencyID,
		  fltExchangeRate,fltDiscountTotal,bitDiscountTypeTotal,
		  monShipCost,
  numOppBizDocTempID,numAccountClass,numPartner,intUsedShippingCompany,numPartenerContact,numShipmentMethod,dtReleaseDate,vcOppRefOrderNo,
  bitBillingTerms,intBillingDays,bitInterestType,fltInterest
		  )                              
		 Values                              
		  (                              
		  @numContactId,                              
		  @numDivID,                              
		  @txtComments,                              
		  @numCampainID,--  0,
		  0,                              
		  @tintSource,   
		  @tintSourceType,                           
		  ISNULL(@vcPOppName,'SO'),                             
		  0,                                
		  @numContactId,                              
		  getutcdate(),                              
		  @numContactId,                              
		  getutcdate(),        
		  @numDomainId,                              
		  1,             
		  @tintOppStatus,       
		  getutcdate(),                                 
		  @numRecOwner ,
		  1,
		  @numCurrencyID,
		  @fltExchangeRate,@fltDiscount,@bitDiscountType
		  ,@monShipCost,
  @numTemplateID,@numAccountClass,@numPartner,@intUsedShippingCompany,@numPartenerContact,@numShipmentMethod,GETDATE(),@vcOppRefOrderNo
  ,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest
		  )        
set @numOppID=scope_identity()                             
--Update OppName as per Name Template
		EXEC dbo.USP_UpdateNameTemplateValue 1,@numDomainId,@numOppID
		SELECT @vcPOppName = vcPOppName FROM OpportunityMaster WHERE numOppID= @numOppID
end
delete  from    OpportunityItems where   numOppId= @numOppID                               

--if convert(varchar(10),@strItems) <>''                
--begin               
-- DECLARE @hDocItem int                
-- EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                

	UPDATE 
		P
	SET
		intCouponCodeUsed=ISNULL(intCouponCodeUsed,0)+1
	FROM 
		PromotionOffer AS P
	LEFT JOIN
		CartItems AS C
	ON
		P.numProId=C.PromotionID
	WHERE
	C.vcCoupon=P.txtCouponCode 
	AND (C.fltDiscount>0 OR C.numCartId IN (SELECT numCartId FROM CartItems AS CG WHERE CG.PromotionID=P.numProId AND CG.fltDiscount>0))
	AND numUserCntId =@numContactId
	AND ISNULL(bitEnabled,0)=1


insert into OpportunityItems                                                        
  (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,vcAttrValues,[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage, monVendorCost,numUOMId,bitDiscountType,fltDiscount,monTotAmtBefDiscount,numPromotionID,bitPromotionTriggered,bitDropShip)                                                        
  --kishan                                                       
 select @numOppID,X.numItemCode,x.numUnitHour * x.decUOMConversionFactor,x.monPrice/x.decUOMConversionFactor,x.monTotAmount,X.vcItemDesc, NULLIF(X.numWarehouseItmsID,0),X.vcItemType,X.vcAttributes,X.vcAttrValues,(SELECT vcItemName FROM item WHERE numItemCode = X.numItemCode),(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
		(SELECT TOP 1  vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND bitDefault = 1 AND numDomainId = @numDomainId),   (select isnull(VN.monCost,0) from Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode and VN.numVendorID=IT.numVendorID where VN.numItemCode=X.numItemCode), 
		numUOM,bitDiscountType,fltDiscount,monTotAmtBefDiscount,X.PromotionID,X.bitParentPromotion,ISNULL((SELECT bitAllowDropShip FROM Item WHERE numItemCode=X.numItemCode),0)
FROM dbo.CartItems X WHERE numUserCntId =@numContactId

	declare @tintShipped as tinyint      
	DECLARE @tintOppType AS TINYINT
	
select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID              
	if @tintOppStatus=1               
	begin         
	PRINT 'inside USP_UpdatingInventoryonCloseDeal'
		exec USP_UpdatingInventoryonCloseDeal @numOppID,@numContactId
	end              

/*Commented by Chintan..reason:tobe executed only when order is confirmed*/
--exec USP_UpdatingInventoryonCloseDeal @numOppID,@numContactId

-- SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/Table',2)                                                        
-- WITH  (                                                        
--  numoppitemtCode numeric(9),                   
--  numItemCode numeric(9),                                                        
--  numUnitHour numeric(9),                                                        
--  monPrice money,                                                     
--  monTotAmount money,                                                        
--  vcItemDesc varchar(1000),                  
--  numWarehouseItmsID numeric(9),        
--  vcItemType varchar(30),    
--  vcAttrValues varchar(50) ,
--  numUOM NUMERIC(18,0),
--  decUOMConversionFactor DECIMAL(18,5),bitDiscountType BIT,fltDiscount FLOAT,monTotAmtBefDiscount MONEY,
--  vcShippingMethod VARCHAR(100),decShippingCharge MONEY,dtDeliveryDate datetime
--  ))X                
--                
                
-- EXEC sp_xml_removedocument @hDocItem                
                
--end
                             
                  
	select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@numOppID                            
 update OpportunityMaster set  monPamount=@TotalAmount - @fltDiscount                           
 where numOppId=@numOppID                           
                          
--set @vcPOppName=(select  ISNULL(vcPOppName,numOppId) from OpportunityMaster where numOppId=@numOppID )
                          
	IF(@vcSource IS NOT NULL)
	BEGIN
		insert into OpportunityLinking ([numParentOppID],[numChildOppID],[vcSource],[numParentOppBizDocID],numPromotionId,numSiteID)  values(null,@numOppID,@vcSource,NULL,@numProId,@numSiteID);
	END

	--Add/Update Address Details
	DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50)      
	DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9) 
	DECLARE @bitIsPrimary BIT;

	--Bill Address
	IF @numBillAddressId>0
	BEGIN
	 SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
			@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
			@vcAddressName=vcAddressName
				FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numBillAddressId 
            
  
  		EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numOppID, --  numeric(9, 0)
		@byteMode = 0, --  tinyint
		@vcStreet = @vcStreet, --  varchar(100)
		@vcCity = @vcCity, --  varchar(50)
		@vcPostalCode = @vcPostalCode, --  varchar(15)
		@numState = @numState, --  numeric(9, 0)
		@numCountry = @numCountry, --  numeric(9, 0)
		@vcCompanyName = '', --  varchar(100)
		@numCompanyId = 0, --  numeric(9, 0)
		@vcAddressName =@vcAddressName,
		@bitCalledFromProcedure=1
        
	END
  
  
	--Ship Address
	IF @numShipAddressId>0
	BEGIN
   		SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
			@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
			@vcAddressName=vcAddressName
				FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numShipAddressId 
  
  
		select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
		join divisionMaster Div                            
		on div.numCompanyID=com.numCompanyID                            
		where div.numdivisionID=@numDivID

  		EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numOppID, --  numeric(9, 0)
		@byteMode = 1, --  tinyint
		@vcStreet = @vcStreet, --  varchar(100)
		@vcCity = @vcCity, --  varchar(50)
		@vcPostalCode = @vcPostalCode, --  varchar(15)
		@numState = @numState, --  numeric(9, 0)
		@numCountry = @numCountry, --  numeric(9, 0)
		@vcCompanyName = @vcCompanyName, --  varchar(100)
		@numCompanyId =@numCompanyId, --  numeric(9, 0)
		@vcAddressName =@vcAddressName,
		@bitCalledFromProcedure=1
   	
	END

	-- IMPORTANT: KEEP THIS BELOW ADDRESS UPDATE LOGIC
	--Insert Tax for Division                       
	INSERT dbo.OpportunityMasterTaxItems 
	(
		numOppId,
		numTaxItemID,
		fltPercentage,
		tintTaxType,
		numTaxID
	) 
	SELECT 
		@numOppID,
		TI.numTaxItemID,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		0
	FROM 
		TaxItems TI 
	JOIN 
		DivisionTaxTypes DTT 
	ON 
		TI.numTaxItemID = DTT.numTaxItemID
	CROSS APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType,
			numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,TI.numTaxItemID,@numOppId,0,NULL)
	) AS TEMPTax
	WHERE 
		DTT.numDivisionID=@numDivID AND DTT.bitApplicable=1
	UNION 
	SELECT 
		@numOppID,
		0,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		0
	FROM 
		dbo.DivisionMaster 
	CROSS APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType,
			numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,0,@numOppId,1,NULL)
	) AS TEMPTax
	WHERE 
		bitNoTax=0 
		AND numDivisionID=@numDivID
	UNION 
	SELECT
		@numOppId
		,1
		,decTaxValue
		,tintTaxType
		,numTaxID
	FROM
		dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,1,@numOppId,1,NULL)
  
	--Delete Tax for Opportunity Items if item deleted 
	DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

	--Insert Tax for Opportunity Items
	INSERT INTO dbo.OpportunityItemsTaxItems 
	(
		numOppId,
		numOppItemID,
		numTaxItemID,
		numTaxID
	) 
	SELECT 
		@numOppId,
		OI.numoppitemtCode,
		TI.numTaxItemID,
		0 
	FROM 
		dbo.OpportunityItems OI 
	JOIN 
		dbo.ItemTax IT 
	ON 
		OI.numItemCode=IT.numItemCode 
	JOIN
		TaxItems TI 
	ON 
		TI.numTaxItemID=IT.numTaxItemID 
	WHERE 
		OI.numOppId=@numOppID 
		AND IT.bitApplicable=1 
		AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
	UNION
	SELECT 
		@numOppId,
		OI.numoppitemtCode,
		0,
		0 
	FROM 
		dbo.OpportunityItems OI 
	JOIN 
		dbo.Item I 
	ON 
		OI.numItemCode=I.numItemCode
	WHERE 
		OI.numOppId=@numOppID 
		AND I.bitTaxable=1 
		AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
	UNION
	SELECT
		@numOppId,
		OI.numoppitemtCode,
		1,
		TD.numTaxID
	FROM
		dbo.OpportunityItems OI 
	INNER JOIN
		ItemTax IT
	ON
		IT.numItemCode = OI.numItemCode
	INNER JOIN
		TaxDetails TD
	ON
		TD.numTaxID = IT.numTaxID
		AND TD.numDomainId = @numDomainId
	WHERE
		OI.numOppId = @numOppID
		AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TransactionHistory_GetAuthrizedTrasaction')
DROP PROCEDURE USP_TransactionHistory_GetAuthrizedTrasaction
GO
CREATE PROCEDURE [dbo].[USP_TransactionHistory_GetAuthrizedTrasaction] 
( 
	@numDomainID AS NUMERIC(18,0),    
	@numDivisionID AS NUMERIC(18,0),
	@monAmount AS MONEY,
	@tintMode AS TINYINT
)
AS
BEGIN
	SELECT TOP 1 
		*
	FROM
		TransactionHistory
	WHERE
		numDomainID=@numDomainID
		AND numDivisionID=@numDivisionID
		AND 1 = (CASE WHEN @tintMode = 1 THEN (CASE WHEN  ISNULL(monAuthorizedAmt,0) - ISNULL(monCapturedAmt,0) = @monAmount THEN 1 ELSE 0 END) ELSE 1 END)
		AND ISNULL(tintTransactionStatus,0) = 1
		AND 1 = (CASE WHEN @tintMode = 1 THEN (CASE WHEN  DATEDIFF(DAY, dtCreatedDate, GETUTCDATE()) BETWEEN 0 AND 7 THEN 1 ELSE 0 END) ELSE 1 END) 
	ORDER BY
		numTransHistoryID DESC
END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_TransactionHistory_GetByOrder' ) 
    DROP PROCEDURE USP_TransactionHistory_GetByOrder
GO

CREATE PROCEDURE [dbo].[USP_TransactionHistory_GetByOrder]
    @numDomainID NUMERIC(18, 0),
    @numOppID NUMERIC(18, 0)
AS 
    SET NOCOUNT ON
    SET TRANSACTION ISOLATION LEVEL READ COMMITTED

	SELECT
		TH.[numTransHistoryID],
		TH.[numDivisionID],
		TH.[numContactID],
		TH.[numOppID],
		TH.[numOppBizDocsID],
		[vcTransactionID],
		[tintTransactionStatus],
		(CASE WHEN tintTransactionStatus = 1 THEN 'Authorized/Pending Capture' 
			 WHEN tintTransactionStatus = 2 THEN 'Captured'
			 WHEN tintTransactionStatus = 3 THEN 'Void'
			 WHEN tintTransactionStatus = 4 THEN 'Failed'
			 WHEN tintTransactionStatus = 5 THEN 'Credited'
			 ELSE 'Not Authorized'
		END) AS vcTransactionStatus,	 
		[vcPGResponse],
		[tintType],
		[monAuthorizedAmt],
		[monCapturedAmt],
		[monRefundAmt],
		[vcCardHolder],
		[vcCreditCardNo],
		isnull((SELECT vcData FROM dbo.ListDetails WHERE numListItemID = isnull([numCardType],0)),'-') as vcCardType,
		[vcCVV2],
		[tintValidMonth],
		[intValidYear],
		TH.dtCreatedDate,
		ISNULL(OBD.vcBizDocID, ISNULL(OM.vcPOppName,'')) AS [vcBizOrderID],
		vcSignatureFile,
		ISNULL(tintOppType,0) AS [tintOppType],
		ISNULL(vcResponseCode,'') vcResponseCode
		,ISNULL(DD.monAmountPaid,0) AS monAmountPaid
 	FROM
		DepositeDetails DD
	INNER JOIN DepositMaster DM ON DD.numDepositID=DM.numDepositId
	INNER JOIN dbo.OpportunityMaster OM ON OM.numoppID = DD.numOppID
	INNER JOIN dbo.OpportunityBizDocs OBD ON OBD.numOppID=OM.numOppID AND OBD.numOppBizDocsId = DD.numOppBizDocsID 
	INNER JOIN dbo.TransactionHistory TH ON TH.numTransHistoryID = DM.numTransHistoryID
	WHERE
		DM.numDomainID=@numDomainID
		AND TH.numDomainID=@numDomainID
		AND OM.numDomainId=@numDomainID
		AND DD.numOppID=@numOppID
	
GO

/****** Object:  StoredProcedure [dbo].[USP_UpdatedomainDetails]    Script Date: 05/07/2009 21:56:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatedomaindetails')
DROP PROCEDURE usp_updatedomaindetails
GO
CREATE PROCEDURE [dbo].[USP_UpdatedomainDetails]                                      
@numDomainID as numeric(9)=0,                                      
@vcDomainName as varchar(50)='',                                      
@vcDomainDesc as varchar(100)='',                                      
@bitExchangeIntegration as bit,                                      
@bitAccessExchange as bit,                                      
@vcExchUserName as varchar(100)='',                                      
@vcExchPassword as varchar(100)='',                                      
@vcExchPath as varchar(200)='',                                      
@vcExchDomain as varchar(100)='',                                    
@tintCustomPagingRows as tinyint,                                    
@vcDateFormat as varchar(30)='',                                    
@numDefCountry as numeric(9),                                    
@tintComposeWindow as tinyint,                                    
@sintStartDate as smallint,                                    
@sintNoofDaysInterval as smallint,                                    
@tintAssignToCriteria as tinyint,                                    
@bitIntmedPage as bit,                                    
@tintFiscalStartMonth as tinyint,                                      
@tintPayPeriod as tinyint,                        
@numCurrencyID as numeric(9) ,                
@charUnitSystem as char(1)='',              
@vcPortalLogo as varchar(100)='',            
@tintChrForComSearch as tinyint  ,            
@intPaymentGateWay as int, -- DO NOT UPDATE VALUE FROM THIS PARAMETER
@tintChrForItemSearch as tinyint,
@ShipCompany as numeric(9),
@bitMultiCurrency as bit,
@bitCreateInvoice AS tinyint = null,
@numDefaultSalesBizDocID AS NUMERIC(9)=NULL,
@numDefaultPurchaseBizDocID AS NUMERIC(9)=NULL,
@bitSaveCreditCardInfo AS BIT = NULL,
@intLastViewedRecord AS INT=10,
@tintCommissionType AS TINYINT,
@tintBaseTax AS TINYINT,
@bitEmbeddedCost AS BIT,
@numDefaultSalesOppBizDocId NUMERIC=NULL,
@tintSessionTimeOut TINYINT=1,
@tintDecimalPoints TINYINT,
@bitCustomizePortal BIT=0,
@tintBillToForPO TINYINT,
@tintShipToForPO TINYINT,
@tintOrganizationSearchCriteria TINYINT,
@bitDocumentRepositary BIT=0,
@tintComAppliesTo TINYINT=0,
@bitGtoBContact BIT=0,
@bitBtoGContact BIT=0,
@bitGtoBCalendar BIT=0,
@bitBtoGCalendar BIT=0,
@bitExpenseNonInventoryItem BIT=0,
@bitInlineEdit BIT=0,
@bitRemoveVendorPOValidation BIT=0,
@tintBaseTaxOnArea tinyint=0,
@bitAmountPastDue BIT=0,
@monAmountPastDue money,
@bitSearchOrderCustomerHistory BIT=0,
@bitRentalItem as bit=0,
@numRentalItemClass as NUMERIC(9)=NULL,
@numRentalHourlyUOM as NUMERIC(9)=NULL,
@numRentalDailyUOM as NUMERIC(9)=NULL,
@tintRentalPriceBasedOn as tinyint=NULL,
@tintCalendarTimeFormat as tinyint=NULL,
@tintDefaultClassType as tinyint=NULL,
@tintPriceBookDiscount as tinyint=0,
@bitAutoSerialNoAssign as bit=0,
@bitIsShowBalance AS BIT = 0,
@numIncomeAccID NUMERIC(18, 0) = 0,
@numCOGSAccID NUMERIC(18, 0) = 0,
@numAssetAccID NUMERIC(18, 0) = 0,
@IsEnableProjectTracking AS BIT = 0,
@IsEnableCampaignTracking AS BIT = 0,
@IsEnableResourceScheduling AS BIT = 0,
@ShippingServiceItem  AS NUMERIC(9)=0,
@numSOBizDocStatus AS NUMERIC(9)=0,
@bitAutolinkUnappliedPayment AS BIT=0,
@numDiscountServiceItemID AS NUMERIC(9)=0,
@vcShipToPhoneNumber as VARCHAR(50)='',
@vcGAUserEmailId as VARCHAR(50)='',
@vcGAUserPassword as VARCHAR(50)='',
@vcGAUserProfileId as VARCHAR(50)='',
@bitDiscountOnUnitPrice AS BIT=0,
@intShippingImageWidth INT = 0,
@intShippingImageHeight INT = 0,
@numTotalInsuredValue NUMERIC(10,2) = 0,
@numTotalCustomsValue NUMERIC(10,2) = 0,
@vcHideTabs VARCHAR(1000) = '',
@bitUseBizdocAmount BIT = 0,
@bitDefaultRateType BIT = 0,
@bitIncludeTaxAndShippingInCommission  BIT = 0,
@bitPurchaseTaxCredit BIT=0,
@bitLandedCost BIT=0,
@vcLanedCostDefault VARCHAR(10)='',
@bitMinUnitPriceRule BIT = 0,
@numAbovePercent AS NUMERIC(18,2),
@numBelowPercent AS NUMERIC(18,2),
@numBelowPriceField AS NUMERIC(18,0),
@vcUnitPriceApprover AS VARCHAR(100),
@numDefaultSalesPricing AS TINYINT,
@bitReOrderPoint AS BIT = 0,
@numReOrderPointOrderStatus AS NUMERIC(18,0) = 0,
@numPODropShipBizDoc AS NUMERIC(18,0) = 0,
@numPODropShipBizDocTemplate AS NUMERIC(18,0) = 0,
@IsEnableDeferredIncome AS BIT = 0,
@bitApprovalforTImeExpense AS BIT=0,
@numCost AS Numeric(9,0)=0,
@intTimeExpApprovalProcess int=0,
@bitApprovalforOpportunity AS BIT=0,
@intOpportunityApprovalProcess int=0,
@numDefaultSalesShippingDoc NUMERIC(18,0)=0,
@numDefaultPurchaseShippingDoc NUMERIC(18,0)=0,
@bitchkOverRideAssignto AS BIT=0,
@vcPrinterIPAddress VARCHAR(15) = '',
@vcPrinterPort VARCHAR(5) = '',
@numDefaultSiteID NUMERIC(18,0) = 0,
@vcSalesOrderTabs VARCHAR(200)='',
@vcSalesQuotesTabs VARCHAR(200)='',
@vcItemPurchaseHistoryTabs VARCHAR(200)='',
@vcItemsFrequentlyPurchasedTabs VARCHAR(200)='',
@vcOpenCasesTabs VARCHAR(200)='',
@vcOpenRMATabs VARCHAR(200)='',
@bitSalesOrderTabs BIT=0,
@bitSalesQuotesTabs BIT=0,
@bitItemPurchaseHistoryTabs BIT=0,
@bitItemsFrequentlyPurchasedTabs BIT=0,
@bitOpenCasesTabs BIT=0,
@bitOpenRMATabs BIT=0,
@bitRemoveGlobalLocation BIT = 0,
@bitSupportTabs BIT=0,
@vcSupportTabs VARCHAR(200)='',
@numAuthorizePercentage NUMERIC(18,0)=0
as                                      
BEGIN
BEGIN TRY
BEGIN TRANSACTION

	IF ISNULL(@bitMinUnitPriceRule,0) = 1 AND ((SELECT COUNT(*) FROM UnitPriceApprover WHERE numDomainID=@numDomainID) = 0 OR ISNULL(@bitApprovalforOpportunity,0)=0 OR ISNULL(@intOpportunityApprovalProcess,0) = 0)
	BEGIN
		RAISERROR('INCOMPLETE_MIN_UNIT_PRICE_CONFIGURATION',16,1)
		RETURN
	END


	If ISNULL(@bitApprovalforTImeExpense,0) = 1 AND (SELECT
														COUNT(*) 
													FROM
														UserMaster
													LEFT JOIN
														ApprovalConfig
													ON
														ApprovalConfig.numUserId = UserMaster.numUserDetailId
													WHERE
														UserMaster.numDomainID=@numDomainID
														AND numModule=1
														AND ISNULL(numGroupID,0) > 0
														AND ISNULL(bitActivateFlag,0) = 1
														AND ISNULL(numLevel1Authority,0) = 0
														AND ISNULL(numLevel2Authority,0) = 0
														AND ISNULL(numLevel3Authority,0) = 0
														AND ISNULL(numLevel4Authority,0) = 0
														AND ISNULL(numLevel5Authority,0) = 0) > 0
	BEGIN
		RAISERROR('INCOMPLETE_TIMEEXPENSE_APPROVAL',16,1)
		RETURN
	END

                                  
 update Domain                                       
   set                                       
   vcDomainName=@vcDomainName,                                      
   vcDomainDesc=@vcDomainDesc,                                      
   bitExchangeIntegration=@bitExchangeIntegration,                                      
   bitAccessExchange=@bitAccessExchange,                                      
   vcExchUserName=@vcExchUserName,                                      
   vcExchPassword=@vcExchPassword,                                      
   vcExchPath=@vcExchPath,                                      
   vcExchDomain=@vcExchDomain,                                    
   tintCustomPagingRows =@tintCustomPagingRows,                                    
   vcDateFormat=@vcDateFormat,                                    
   numDefCountry =@numDefCountry,                                    
   tintComposeWindow=@tintComposeWindow,                                    
   sintStartDate =@sintStartDate,                                    
   sintNoofDaysInterval=@sintNoofDaysInterval,                                    
   tintAssignToCriteria=@tintAssignToCriteria,                                    
   bitIntmedPage=@bitIntmedPage,                                    
   tintFiscalStartMonth=@tintFiscalStartMonth,                            
--   bitDeferredIncome=@bitDeferredIncome,                          
   tintPayPeriod=@tintPayPeriod,
   vcCurrency=isnull((select varCurrSymbol from Currency Where numCurrencyID=@numCurrencyID  ),'')  ,                      
  numCurrencyID=@numCurrencyID,                
  charUnitSystem= @charUnitSystem,              
  vcPortalLogo=@vcPortalLogo ,            
  tintChrForComSearch=@tintChrForComSearch  ,      
 tintChrForItemSearch=@tintChrForItemSearch,
 numShipCompany= @ShipCompany,
 bitMultiCurrency=@bitMultiCurrency,
 bitCreateInvoice= @bitCreateInvoice,
 [numDefaultSalesBizDocId] =@numDefaultSalesBizDocId,
 bitSaveCreditCardInfo=@bitSaveCreditCardInfo,
 intLastViewedRecord=@intLastViewedRecord,
 [tintCommissionType]=@tintCommissionType,
 [tintBaseTax]=@tintBaseTax,
 [numDefaultPurchaseBizDocId]=@numDefaultPurchaseBizDocID,
 bitEmbeddedCost=@bitEmbeddedCost,
 numDefaultSalesOppBizDocId=@numDefaultSalesOppBizDocId,
 tintSessionTimeOut=@tintSessionTimeOut,
 tintDecimalPoints=@tintDecimalPoints,
 bitCustomizePortal=@bitCustomizePortal,
 tintBillToForPO = @tintBillToForPO,
 tintShipToForPO = @tintShipToForPO,
 tintOrganizationSearchCriteria=@tintOrganizationSearchCriteria,
-- vcPortalName=@vcPortalName,
 bitDocumentRepositary=@bitDocumentRepositary,
 --tintComAppliesTo=@tintComAppliesTo,THIS FIELD IS REMOVED FROM GLOBAL SETTINGS
 bitGtoBContact=@bitGtoBContact,
 bitBtoGContact=@bitBtoGContact,
 bitGtoBCalendar=@bitGtoBCalendar,
 bitBtoGCalendar=@bitBtoGCalendar,
 bitExpenseNonInventoryItem=@bitExpenseNonInventoryItem,
 bitInlineEdit=@bitInlineEdit,
 bitRemoveVendorPOValidation=@bitRemoveVendorPOValidation,
tintBaseTaxOnArea=@tintBaseTaxOnArea,
bitAmountPastDue = @bitAmountPastDue,
monAmountPastDue = @monAmountPastDue,
bitSearchOrderCustomerHistory=@bitSearchOrderCustomerHistory,
bitRentalItem=@bitRentalItem,
numRentalItemClass=@numRentalItemClass,
numRentalHourlyUOM=@numRentalHourlyUOM,
numRentalDailyUOM=@numRentalDailyUOM,
tintRentalPriceBasedOn=@tintRentalPriceBasedOn,
tintCalendarTimeFormat=@tintCalendarTimeFormat,
tintDefaultClassType=@tintDefaultClassType,
tintPriceBookDiscount=@tintPriceBookDiscount,
bitAutoSerialNoAssign=@bitAutoSerialNoAssign,
bitIsShowBalance = @bitIsShowBalance,
numIncomeAccID = @numIncomeAccID,
numCOGSAccID = @numCOGSAccID,
numAssetAccID = @numAssetAccID,
IsEnableProjectTracking = @IsEnableProjectTracking,
IsEnableCampaignTracking = @IsEnableCampaignTracking,
IsEnableResourceScheduling = @IsEnableResourceScheduling,
numShippingServiceItemID = @ShippingServiceItem,
numSOBizDocStatus=@numSOBizDocStatus,
bitAutolinkUnappliedPayment=@bitAutolinkUnappliedPayment,
numDiscountServiceItemID=@numDiscountServiceItemID,
vcShipToPhoneNo = @vcShipToPhoneNumber,
vcGAUserEMail = @vcGAUserEmailId,
vcGAUserPassword = @vcGAUserPassword,
vcGAUserProfileId = @vcGAUserProfileId,
bitDiscountOnUnitPrice=@bitDiscountOnUnitPrice,
intShippingImageWidth = @intShippingImageWidth ,
intShippingImageHeight = @intShippingImageHeight,
numTotalInsuredValue = @numTotalInsuredValue,
numTotalCustomsValue = @numTotalCustomsValue,
vcHideTabs = @vcHideTabs,
bitUseBizdocAmount = @bitUseBizdocAmount,
bitDefaultRateType = @bitDefaultRateType,
bitIncludeTaxAndShippingInCommission = @bitIncludeTaxAndShippingInCommission,
/*,bitAllowPPVariance=@bitAllowPPVariance*/
bitPurchaseTaxCredit=@bitPurchaseTaxCredit,
bitLandedCost=@bitLandedCost,
vcLanedCostDefault=@vcLanedCostDefault,
bitMinUnitPriceRule = @bitMinUnitPriceRule,
numDefaultSalesPricing = @numDefaultSalesPricing,
numPODropShipBizDoc=@numPODropShipBizDoc,
numPODropShipBizDocTemplate=@numPODropShipBizDocTemplate,
IsEnableDeferredIncome=@IsEnableDeferredIncome,
bitApprovalforTImeExpense=@bitApprovalforTImeExpense,
numCost= @numCost,intTimeExpApprovalProcess=@intTimeExpApprovalProcess,
bitApprovalforOpportunity=@bitApprovalforOpportunity,intOpportunityApprovalProcess=@intOpportunityApprovalProcess,
 numDefaultPurchaseShippingDoc=@numDefaultPurchaseShippingDoc,numDefaultSalesShippingDoc=@numDefaultSalesShippingDoc,
 bitchkOverRideAssignto=@bitchkOverRideAssignto,
 vcPrinterIPAddress=@vcPrinterIPAddress,
 vcPrinterPort=@vcPrinterPort,
 numDefaultSiteID=ISNULL(@numDefaultSiteID,0),
  vcSalesOrderTabs=@vcSalesOrderTabs,
 vcSalesQuotesTabs=@vcSalesQuotesTabs,
 vcItemPurchaseHistoryTabs=@vcItemPurchaseHistoryTabs,
 vcItemsFrequentlyPurchasedTabs=@vcItemsFrequentlyPurchasedTabs,
 vcOpenCasesTabs=@vcOpenCasesTabs,
 vcOpenRMATabs=@vcOpenRMATabs,
 bitSalesOrderTabs=@bitSalesOrderTabs,
 bitSalesQuotesTabs=@bitSalesQuotesTabs,
 bitItemPurchaseHistoryTabs=@bitItemPurchaseHistoryTabs,
 bitItemsFrequentlyPurchasedTabs=@bitItemsFrequentlyPurchasedTabs,
 bitOpenCasesTabs=@bitOpenCasesTabs,
 bitOpenRMATabs=@bitOpenRMATabs,
 bitRemoveGlobalLocation=@bitRemoveGlobalLocation,
  bitSupportTabs=@bitSupportTabs,
 vcSupportTabs=@vcSupportTabs,
 numAuthorizePercentage=@numAuthorizePercentage
 where numDomainId=@numDomainID
COMMIT

 IF ISNULL(@bitRemoveGlobalLocation,0) = 1
 BEGIN
	DECLARE @TEMPGlobalWarehouse TABLE
	(
		ID INT IDENTITY(1,1)
		,numItemCode NUMERIC(18,0)
		,numWarehouseID NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(28,0)
	)

	INSERT INTO @TEMPGlobalWarehouse
	(
		numItemCode
		,numWarehouseID
		,numWarehouseItemID
	)
	SELECT
		numItemID
		,numWareHouseID
		,numWareHouseItemID
	FROM
		WareHouseItems
	WHERE
		numDomainID=@numDomainID
		AND numWLocationID = -1
		AND ISNULL(numOnHand,0) = 0
		AND ISNULL(numOnOrder,0)=0
		AND ISNULL(numAllocation,0)=0
		AND ISNULL(numBackOrder,0)=0


	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT
	DECLARE @numTempItemCode AS NUMERIC(18,0)
	DECLARE @numTempWareHouseID AS NUMERIC(18,0)
	DECLARE @numTempWareHouseItemID AS NUMERIC(18,0)
	SELECT @iCount=COUNT(*) FROM @TEMPGlobalWarehouse

	WHILE @i <= @iCount
	BEGIN
		SELECT @numTempItemCode=numItemCode,@numTempWareHouseID=numWarehouseID,@numTempWareHouseItemID=numWarehouseItemID FROM @TEMPGlobalWarehouse WHERE ID=@i

		BEGIN TRY
			EXEC USP_AddUpdateWareHouseForItems @numTempItemCode,  
												@numTempWareHouseID,
												@numTempWareHouseItemID,
												'',
												0,
												0,
												0,
												'',
												'',
												@numDomainID,
												'',
												'',
												'',
												0,
												3,
												0,
												0,
												NULL,
												0
		END TRY
		BEGIN CATCH

		END CATCH

		SET @i = @i + 1
	END
 END

END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH

END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ApplyCouponCodetoCart')
DROP PROCEDURE USP_ApplyCouponCodetoCart
GO
CREATE PROCEDURE [dbo].[USP_ApplyCouponCodetoCart]
	@numDomainID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0) OUTPUT,
	@cookieId VARCHAR(MAX)=NULL,
	@numUserCntId numeric=0,
	@vcSendCoupon VARCHAR(200)=0,
	@numSiteID NUMERIC(18,0)=0
AS
BEGIN
	



	IF (SELECT COUNT(*) FROM PromotionOffer PO INNER JOIN PromotionOfferSites POS ON PO.numProId=POS.numPromotionID WHERE numDomainId=@numDomainID AND numSiteID=@numSiteID AND txtCouponCode = @vcSendCoupon AND ISNULL(bitEnabled,0)=1)=0
	BEGIN
		RAISERROR('INVALID_COUPON_CODE',16,1)
		RETURN
	END 
	ELSE IF (SELECT 
			COUNT(*) 
		FROM 
			PromotionOffer PO 
		INNER JOIN 
			PromotionOfferSites POS 
		ON 
			PO.numProId=POS.numPromotionID
		WHERE 
			numDomainId=@numDomainID 
			AND numSiteID=@numSiteID
			AND txtCouponCode = @vcSendCoupon 
			AND bitEnabled = 1 
			AND 1= (CASE WHEN ISNULL(tintUsageLimit,0) > 0 THEN (CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) THEN 1 ELSE 0 END) ELSE 1 END)
			AND 1 = (CASE WHEN ISNULL(bitNeverExpires,0) = 1 THEN 1 ELSE (CASE WHEN GETDATE() BETWEEN dtValidFrom AND dtValidTo THEN 1 ELSE 0 END) END)) = 0
	BEGIN
		RAISERROR('COUPON_EXPIRED',16,1)
		RETURN
	END

	IF((SELECT COUNT(numCartId) FROM CartItems WHERE numDomainId=@numDomainID AND vcCookieId=@cookieId AND numUserCntId=@numUserCntId AND vcCoupon=@vcSendCoupon)=0)
	BEGIN
		SET @numItemCode=1
		
		DECLARE @fltOfferTriggerValue FLOAT
		DECLARE @tintOfferTriggerValueType TINYINT
		DECLARE @tintOfferBasedOn TINYINT
		DECLARE @fltDiscountValue FLOAT
		DECLARE @tintDiscountType TINYINT
		DECLARE @tintDiscoutBaseOn TINYINT
		DECLARE @PromotionID NUMERIC(18,0)
		DECLARE @PromotionDesc VARCHAR(500)
		


		 SElECT TOP 1 
			@PromotionID=PO.numProId,
			@fltOfferTriggerValue=PO.fltOfferTriggerValue,
			@tintOfferTriggerValueType=PO.tintOfferTriggerValueType,
			@tintOfferBasedOn=PO.tintOfferBasedOn,
			@fltDiscountValue=fltDiscountValue,
			@tintDiscountType=tintDiscountType,
			@tintDiscoutBaseOn=tintDiscoutBaseOn,
			@PromotionDesc=(CONCAT('Buy '
					,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
					,CASE tintOfferBasedOn
							WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
							WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
						END
					,' & get '
					, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
					, CASE 
						WHEN tintDiscoutBaseOn = 1 THEN 
							CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 2 THEN 
							CONCAT
							(
								CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(',',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
						ELSE '' 
					END
				))
		FROM
			PromotionOffer  AS PO
		WHERE
			PO.numDomainId=@numDomainID AND
			PO.txtCouponCode=@vcSendCoupon
		

		DECLARE @TEMPItems TABLE
		(
			ID INT,
			numCartID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numItemClassification NUMERIC(18,0),
			numUnitHour NUMERIC(18,0),
			monPrice NUMERIC(18,0),
			monTotalAmount NUMERIC(18,0)
		)

		IF @tintOfferBasedOn = 1 --individual items
		BEGIN
			INSERT INTO 
				@TEMPItems
			SELECT
				ROW_NUMBER() OVER(ORDER BY numCartID ASC),
				numCartId,
				CI.numItemCode,
				I.numItemClassification,
				CI.numUnitHour,
				CI.monPrice,
				(CI.numUnitHour * CI.monPrice)
			FROM
				CartItems CI
			INNER JOIN
				Item I
			ON
				CI.numItemCode = I.numItemCode
			WHERE
				CI.numDomainId=@numDomainId
				AND CI.numUserCntId=@numUserCntId
				AND CI.vcCookieId=@cookieId
				AND ISNULL(CI.PromotionID,0) = 0
				AND CI.numItemCode IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=@PromotionID AND tintRecordType=5 AND tintType=1)
				AND 1 = (CASE 
						WHEN @tintOfferTriggerValueType=1 -- Quantity
						THEN (CASE WHEN numUnitHour >= @fltOfferTriggerValue THEN 1 ELSE 0 END)
						WHEN @tintOfferTriggerValueType=2 -- Amount
						THEN (CASE WHEN (CI.numUnitHour * CI.monPrice) >= @fltOfferTriggerValue THEN 1 ELSE 0 END)
						ELSE 0
						END)
		END
		ELSE IF @tintOfferBasedOn = 2 --item classifications
		BEGIN
			;WITH CTE (numCartId,numItemCode,numItemClassification,numUnitHour,monPrice,monTotalAmount) AS
			(
				SELECT
					numCartId,
					CI.numItemCode,
					I.numItemClassification,
					CI.numUnitHour,
					CI.monPrice,
					(CI.numUnitHour * CI.monPrice)
				FROM
					CartItems CI
				INNER JOIN
					Item I
				ON
					CI.numItemCode = I.numItemCode
				WHERE
					CI.numDomainId=@numDomainId
					AND CI.numUserCntId=@numUserCntId
					AND CI.vcCookieId=@cookieId
					AND ISNULL(CI.PromotionID,0) = 0
					AND I.numItemClassification IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=@PromotionID AND tintRecordType=5 AND tintType=2)
			)

			INSERT INTO 
				@TEMPItems
			SELECT
				ROW_NUMBER() OVER(ORDER BY numCartID ASC),
				numCartId,
				numItemCode,
				numItemClassification,
				numUnitHour,
				monPrice,
				monTotalAmount
			FROM
				CTE
			WHERE
				1 = (CASE 
					WHEN @tintOfferTriggerValueType=1 -- Quantity
					THEN (CASE WHEN (SELECT SUM(numUnitHour) FROM CTE) >= @fltOfferTriggerValue THEN 1 ELSE 0 END)
					WHEN @tintOfferTriggerValueType=2 -- Amount
					THEN (CASE WHEN (SELECT SUM(monTotalAmount) FROM CTE) >= @fltOfferTriggerValue THEN 1 ELSE 0 END)
					ELSE 0
					END)
		END

		DECLARE @isPromotionTrigerred AS BIT = 0
		DECLARE @i AS INT = 1
		DECLARE @COUNT AS INT
		DECLARE @numTempCartID NUMERIC(18,0)
		DECLARE @numTempItemCode NUMERIC(18,0)
		DECLARE @numTempItemClassification NUMERIC(18,0)
		DECLARE @numTempUnitHour NUMERIC(18,0)
		DECLARE @monTempPrice NUMERIC(18,0)
		DECLARE @monTempTotalAmount NUMERIC(18,0)
		DECLARE @remainingOfferValue FLOAT = @fltOfferTriggerValue
		DECLARE @remainingDiscountValue FLOAT = @fltDiscountValue

		SELECT @COUNT=COUNT(*) FROM @TEMPItems

		IF @COUNT > 0
		BEGIN
			SET @isPromotionTrigerred = 1

			WHILE @i <= @COUNT
			BEGIN
				SELECT 
					@numTempCartID=numCartID,
					@numTempItemCode=numItemCode,
					@numTempItemClassification=ISNULL(numItemClassification,0),
					@numTempUnitHour=numUnitHour,
					@monTempPrice=monPrice,
					@monTempTotalAmount = (numUnitHour * monPrice)
				FROM 
					@TEMPItems 
				WHERE 
					ID=@i

				IF @tintOfferBasedOn = 1 -- Individual Items
					AND 1 = (CASE 
							WHEN @tintOfferTriggerValueType=1 -- Quantity
							THEN CASE WHEN @numTempUnitHour >= @fltOfferTriggerValue THEN 1 ELSE 0 END
							WHEN @tintOfferTriggerValueType=2 -- Amount
							THEN CASE WHEN @monTempTotalAmount >= @fltOfferTriggerValue THEN 1 ELSE 0 END
							ELSE 0
						END) 
				BEGIN
					UPDATE 
						CartItems 
					SET 
						PromotionID=@PromotionID ,
						PromotionDesc=@PromotionDesc,
						bitParentPromotion=1,
						vcCoupon=@vcSendCoupon
					WHERE 
						numCartId=@numTempCartID

					IF 1 =(CASE @tintDiscoutBaseOn
						WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId=@PromotionID AND POI.numValue=@numTempItemCode AND tintRecordType=6 AND tintType=1) > 0 THEN 1 ELSE 0 END)
						WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId=@PromotionID AND POI.numValue=@numTempItemClassification AND tintRecordType=6 AND tintType=2) > 0 THEN 1 ELSE 0 END)
						WHEN 3 THEN (CASE @tintOfferBasedOn
										WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=@numTempItemCode AND numParentItemCode IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=@PromotionID AND tintRecordType=5 AND tintType=1)) > 0 THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=@numTempItemCode AND numParentItemCode IN (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainId AND numItemClassification IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=@PromotionID AND tintRecordType=5 AND tintType=2))) > 0 THEN 1 ELSE 0 END)
										ELSE 0
									END) 
						ELSE 0
					END)
					BEGIN
						IF @tintDiscountType=1  AND @fltDiscountValue > 0 --Percentage
						BEGIN
							UPDATE 
								CartItems
							SET
								fltDiscount=@fltDiscountValue,
								bitDiscountType=0,
								monTotAmount=(@monTempTotalAmount-((@monTempTotalAmount*@fltDiscountValue)/100))
							WHERE
								numCartId=@numTempCartID
						END
						ELSE IF @tintDiscountType=2 AND @fltDiscountValue > 0 --Flat Amount
						BEGIN
							-- FIRST CHECK WHETHER DISCOUNT AMOUNT IS NOT ALREADY USED BY OTHER ITEMS
							DECLARE @usedDiscountAmount AS FLOAT
				
							SELECT
								@usedDiscountAmount = SUM(ISNULL(fltDiscount,0))
							FROM
								CartItems
							WHERE
								PromotionID=@PromotionID

							IF @usedDiscountAmount < @fltDiscountValue
							BEGIN
								UPDATE 
									CartItems 
								SET 
									monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount) >= @monTempTotalAmount THEN 0 ELSE (@monTempTotalAmount-(@fltDiscountValue - @usedDiscountAmount)) END),
									bitDiscountType=1,
									fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount) >= @monTempTotalAmount THEN @monTempTotalAmount ELSE (@fltDiscountValue - @usedDiscountAmount) END)
								WHERE 
									numCartId=@numTempCartID
							END
						END
						ELSE IF @tintDiscountType=3  AND @fltDiscountValue > 0 --Quantity
						BEGIN
							-- FIRST CHECK WHETHER DISCOUNT QTY IS NOT ALREADY USED BY OTHER ITEMS
							DECLARE @usedDiscountQty AS FLOAT

							SELECT
								@usedDiscountQty = SUM(ISNULL(fltDiscount,0)/(CASE WHEN ISNULL(monPrice,1) = 0 THEN 1 ELSE ISNULL(monPrice,1) END))
							FROM
								CartItems
							WHERE
								PromotionID=@PromotionID

							IF @usedDiscountQty < @fltDiscountValue
							BEGIN
								UPDATE 
									CartItems 
								SET 
									monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty) >= @numTempUnitHour THEN 0 ELSE (@monTempTotalAmount-((@fltDiscountValue - @usedDiscountQty) * @monTempPrice)) END),
									bitDiscountType=1,
									fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty) >= @numTempUnitHour THEN (@numTempUnitHour*@monTempPrice) ELSE ((@fltDiscountValue - @usedDiscountQty) * @monTempPrice) END) 
								WHERE 
									numCartId=@numTempCartID
							END
						END
					END

					
					BREAK
				END
				ELSE IF @tintOfferBasedOn = 2 -- Item Classification
				BEGIN
					UPDATE 
						CartItems 
					SET 
						PromotionID=@PromotionID ,
						PromotionDesc=@PromotionDesc,
						bitParentPromotion=1,
						vcCoupon=@vcSendCoupon
					WHERE 
						numCartId=@numTempCartID

					-- CHECK IF DISCOUNT FROM PROMOTION CAN ALSO BE APPLIED TO ITEM
					IF 1=(CASE @tintDiscoutBaseOn
							WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId=@PromotionID AND POI.numValue=@numTempItemCode AND tintRecordType=6 AND tintType=1) > 0 THEN 1 ELSE 0 END)
							WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId=@PromotionID AND POI.numValue=@numTempItemClassification AND tintRecordType=6 AND tintType=2) > 0 THEN 1 ELSE 0 END)
							WHEN 3 THEN (CASE @tintOfferBasedOn
											WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=@numTempItemCode AND numParentItemCode IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=@PromotionID AND tintRecordType=5 AND tintType=1)) > 0 THEN 1 ELSE 0 END)
											WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=@numTempItemCode AND numParentItemCode IN (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainId AND numItemClassification IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=@PromotionID AND tintRecordType=5 AND tintType=2))) > 0 THEN 1 ELSE 0 END)
											ELSE 0
										END) 
							ELSE 0
						END)
					BEGIN
						IF @tintDiscountType=1 --Percentage
						BEGIN
							UPDATE 
								CartItems
							SET
								fltDiscount=@fltDiscountValue,
								bitDiscountType=0,
								monTotAmount=(@monTempTotalAmount-((@monTempTotalAmount*@fltDiscountValue)/100))
							WHERE
								numCartId=@numTempCartID
						END
						ELSE IF @tintDiscountType=2 --Flat Amount
						BEGIN
							-- FIRST CHECK WHETHER DISCOUNT AMOUNT IS NOT ALREADY USED BY OTHER ITEMS
							SELECT
								@usedDiscountAmount = SUM(ISNULL(fltDiscount,0))
							FROM
								CartItems
							WHERE
								PromotionID=@PromotionID

							IF @usedDiscountAmount < @fltDiscountValue
							BEGIN
								UPDATE 
									CartItems 
								SET 
									monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount) >= @monTempTotalAmount THEN 0 ELSE (@monTempTotalAmount-(@fltDiscountValue - @usedDiscountAmount)) END),
									bitDiscountType=1,
									fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount) >= @monTempTotalAmount THEN @monTempTotalAmount ELSE (@fltDiscountValue - @usedDiscountAmount) END)
								WHERE 
									numCartId=@numTempCartID
							END
						END
						ELSE IF @tintDiscountType=3 --Quantity
						BEGIN
							-- FIRST CHECK WHETHER DISCOUNT QTY IS NOT ALREADY USED BY OTHER ITEMS
							SELECT
								@usedDiscountQty = SUM(ISNULL(fltDiscount,0)/(CASE WHEN ISNULL(monPrice,1) = 0 THEN 1 ELSE ISNULL(monPrice,1) END))
							FROM
								CartItems
							WHERE
								PromotionID=@PromotionID

							IF @usedDiscountQty < @fltDiscountValue
							BEGIN
								UPDATE 
									CartItems 
								SET 
									monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty) >= @numTempUnitHour THEN 0 ELSE (@monTempTotalAmount-((@fltDiscountValue - @usedDiscountQty) * @monTempPrice)) END),
									bitDiscountType=1,
									fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty) >= @numTempUnitHour THEN (@numTempUnitHour*@monTempPrice) ELSE ((@fltDiscountValue - @usedDiscountQty) * @monTempPrice) END) 
								WHERE 
									numCartId=@numTempCartID
							END
						END
					END

					SET @remainingOfferValue = @remainingOfferValue - (CASE WHEN @tintOfferTriggerValueType=1 THEN @numTempUnitHour ELSE @monTempTotalAmount END)

					IF @remainingOfferValue <= 0
					BEGIN
						BREAK
					END
				END

				SET @i = @i + 1
			END
		END

		

		IF ISNULL(@isPromotionTrigerred,0) = 1
		BEGIN
			PRINT CONCAT(@isPromotionTrigerred,@tintDiscoutBaseOn)
			DELETE FROM @TEMPItems
				
			INSERT INTO 
				@TEMPItems
			SELECT
				ROW_NUMBER() OVER(ORDER BY numCartID ASC),
				numCartId,
				CI.numItemCode,
				I.numItemClassification,
				CI.numUnitHour,
				CI.monPrice,
				(CI.numUnitHour * CI.monPrice)
			FROM
				CartItems CI
			INNER JOIN
				Item I
			ON
				CI.numItemCode = I.numItemCode
			WHERE
				CI.numDomainId=@numDomainId
				AND CI.numUserCntId=@numUserCntId
				AND CI.vcCookieId=@CookieId
				AND ISNULL(CI.PromotionID,0) = 0
				AND 1 = (CASE @tintDiscoutBaseOn
							WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numValue=I.numItemCode AND numProId=@PromotionID AND tintRecordType=6 AND tintType=1) > 0 THEN 1 ELSE 0 END)
							WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numValue=I.numItemClassification AND numProId=@PromotionID AND tintRecordType=6 AND tintType=2) > 0 THEN 1 ELSE 0 END)
							WHEN 3 THEN (CASE @tintOfferBasedOn 
									WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=I.numItemCode AND numParentItemCode IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=@PromotionID AND tintRecordType=5 AND tintType=1)) > 0 THEN 1 ELSE 0 END)
									WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=I.numItemCode AND numParentItemCode IN (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainId AND numItemClassification IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=@PromotionID AND tintRecordType=5 AND tintType=2))) > 0 THEN 1 ELSE 0 END)
									ELSE 0
								END) 
							ELSE 0
						END)

			DECLARE @k AS INT = 1
			DECLARE @kCount AS INT 

			SELECT @kCount = COUNT(*) FROM @TEMPItems
			SELECT * FROM @TEMPItems
			IF @kCount > 0
			BEGIN
				WHILE @k <= @kCount
				BEGIN
					SELECT 
						@numTempCartID=numCartID,
						@numTempItemCode=numItemCode,
						@numTempItemClassification=ISNULL(numItemClassification,0),
						@numTempUnitHour=numUnitHour,
						@monTempPrice=monPrice,
						@monTempTotalAmount = (numUnitHour * monPrice)
					FROM 
						@TEMPItems 
					WHERE 
						ID=@k

					IF @tintDiscountType=1 --Percentage
					BEGIN
						UPDATE 
							CartItems
						SET
							PromotionID=@PromotionID,
							PromotionDesc=@PromotionDesc,
							bitParentPromotion=0,
							fltDiscount=@fltDiscountValue,
							bitDiscountType=0,
							monTotAmount=(@monTempTotalAmount-((@monTempTotalAmount*@fltDiscountValue)/100))
						WHERE
							numCartId=@numTempCartID
					END
					ELSE IF @tintDiscountType=2 --Flat Amount
					BEGIN
						-- FIRST CHECK WHETHER DISCOUNT AMOUNT IS NOT ALREADY USED BY OTHER ITEMS
						SELECT
							@usedDiscountAmount = SUM(fltDiscount)
						FROM
							CartItems
						WHERE
							PromotionID=@PromotionID

						IF @usedDiscountAmount < @fltDiscountValue
						BEGIN
							UPDATE 
								CartItems 
							SET 
								monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount)  >= @monTempTotalAmount THEN 0 ELSE (@monTempTotalAmount-(@fltDiscountValue - @usedDiscountAmount)) END),
								PromotionID=@PromotionID ,
								PromotionDesc=@PromotionDesc,
								bitDiscountType=1,
								fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount) >= @monTempTotalAmount THEN @monTempTotalAmount ELSE (@fltDiscountValue - @usedDiscountAmount) END)
							WHERE 
								numCartId=@numTempCartID
						END
						ELSE
						BEGIN
							BREAK
						END
					END
					ELSE IF @tintDiscountType=3 --Quantity
					BEGIN
						-- FIRST CHECK WHETHER DISCOUNT QTY IS NOT ALREADY USED BY OTHER ITEMS
						SELECT
							@usedDiscountQty = SUM(fltDiscount/monPrice)
						FROM
							CartItems
						WHERE
							PromotionID=@PromotionID

						IF @usedDiscountQty < @fltDiscountValue
						BEGIN
							UPDATE 
								CartItems 
							SET 
								monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty)  >= numUnitHour THEN 0 ELSE (@monTempTotalAmount-((@fltDiscountValue - @usedDiscountQty) * @monTempPrice)) END),
								PromotionID=@PromotionID ,
								PromotionDesc=@PromotionDesc,
								bitDiscountType=1,
								fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty) >= numUnitHour THEN @monTempTotalAmount ELSE ((@fltDiscountValue - @usedDiscountQty) * @monTempPrice) END) 
							WHERE 
								numCartId=@numTempCartID
						END
						ELSE
						BEGIN
							BREAK
						END
					END

					SET @k = @k + 1
				END
			END
		END
	END
	ELSE
	BEGIN
		SET @numItemCode=2
	END

	SELECT @numItemCode AS ResOutput
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOrderSelectedItemDetail')
DROP PROCEDURE USP_GetOrderSelectedItemDetail
GO
CREATE PROCEDURE [dbo].[USP_GetOrderSelectedItemDetail]
	@numDomainID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0),
	@tintOppType AS TINYINT,
	@numItemCode NUMERIC(18,0),
	@numUOMID NUMERIC(18,0),
	@numWarehouseItemID AS NUMERIC(18,0),
	@numQty INT,
	@monPrice FLOAT,
	@numOppID NUMERIC(18,0),
	@numOppItemID NUMERIC(18,0),
	@vcSelectedKitChildItems VARCHAR(MAX),
	@numShippingCountry AS NUMERIC(18,0),
	@numWarehouseID AS NUMERIC(18,0),
	@vcCoupon AS VARCHAR(200)
AS
BEGIN
	DECLARE @vcItemName AS VARCHAR(500)
	DECLARE @bitDropship AS BIT
	DECLARE @bitMatrix AS BIT
	DECLARE @chrItemType AS CHAR(1)
	DECLARE @numItemGroup AS NUMERIC(18,0)
	DECLARE @numBaseUnit AS NUMERIC(18,0)
	DECLARE @numSaleUnit AS NUMERIC(18,0)
	DECLARE @fltUOMConversionFactor AS FLOAT
	DECLARE @numPurchaseUnit AS NUMERIC(18,0)
	DECLARE @bitAssemblyOrKit BIT
	DECLARE @bitAssembly AS BIT
	DECLARE @bitCalAmtBasedonDepItems AS BIT
	DECLARE @ItemDesc AS VARCHAR(1000)
	DECLARE @numItemClassification AS NUMERIC(18,0)

	DECLARE @monListPrice AS MONEY = 0.0000
	DECLARE @monVendorCost AS MONEY = 0.0000
	DECLARE @tintPricingBasedOn AS TINYINT = 0.0000

	DECLARE @tintRuleType AS TINYINT
	DECLARE @monPriceLevelPrice AS MONEY = 0.0000
	DECLARE @monPriceFinalPrice AS MONEY = 0.0000
	DECLARE @monPriceRulePrice AS MONEY = 0.0000
	DECLARE @monPriceRuleFinalPrice AS MONEY = 0.0000
	DECLARE @monLastPrice AS MONEY = 0.0000
	DECLARE @dtLastOrderDate VARCHAR(200) = ''

	DECLARE @tintDisountType TINYINT
	DECLARE @decDiscount AS FLOAT

	DECLARE @numVendorID AS NUMERIC(18,0)
	DECLARE @vcMinOrderQty AS VARCHAR(5) = '-'
	DECLARE @vcVendorNotes AS VARCHAR(300) = ''
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)
	DECLARE @tintPriceLevel AS TINYINT
	DECLARE @tintDefaultSalesPricing AS TINYINT
	DECLARE @tintPriceBookDiscount AS TINYINT

	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT @vcMinOrderQty=ISNULL(CAST(intMinQty AS VARCHAR),'-'),@vcVendorNotes=ISNULL(vcNotes,'') FROM Vendor WHERE numVendorID=@numDivisionID AND numItemCode=@numItemCode
	END

	SELECT @tintDefaultSalesPricing = numDefaultSalesPricing,@tintPriceBookDiscount=tintPriceBookDiscount FROM Domain WHERE numDomainId=@numDomainID

	IF ISNULL(@numWarehouseID,0) > 0
	BEGIN
		SELECT TOP 1 @numWarehouseItemID=numWareHouseItemID FROM WareHouseItems WHERE numItemID=@numItemCode AND numWareHouseID=@numWarehouseID ORDER BY numWareHouseItemID ASC
	END

	-- GET ORGANIZATION DETAL
	SELECT 
		@numRelationship=numCompanyType,
		@numProfile=vcProfile,
		@tintPriceLevel = ISNULL(D.tintPriceLevel,0)
	FROM 
		DivisionMaster D                  
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID                  
	WHERE 
		numDivisionID =@numDivisionID       

	-- GET ITEM DETAILS
	SELECT
		@vcItemName = vcItemName,
		@bitDropship = bitAllowDropShip,
		@ItemDesc = Item.txtItemDesc,
		@bitMatrix = ISNULL(Item.bitMatrix,0),
		@chrItemType = charItemType,
		@numItemGroup = numItemGroup,
		@bitAssembly = ISNULL(bitAssembly,0),
		@numBaseUnit = ISNULL(numBaseUnit,0),
		@numSaleUnit = (CASE WHEN ISNULL(@numUOMID,0) > 0 THEN @numUOMID ELSE ISNULL(numSaleUnit,0) END),
		@numPurchaseUnit = (CASE WHEN ISNULL(@numUOMID,0) > 0 THEN @numUOMID ELSE ISNULL(numPurchaseUnit,0) END),
		@numItemClassification = numItemClassification,
		@bitAssemblyOrKit = (CASE WHEN ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0)=1 THEN 1 ELSE 0 END),
		@monListPrice = (CASE WHEN Item.charItemType='P' THEN ISNULL(WareHouseItems.monWListPrice,0) ELSE ISNULL(Item.monListPrice,0) END),
		@monVendorCost = ISNULL((SELECT monCost FROM Vendor WHERE numVendorID=Item.numVendorID AND Vendor.numItemCode=Item.numItemCode),0),
		@numVendorID = ISNULL(Item.numVendorID,0),
		@numWarehouseItemID = (CASE WHEN ISNULL(@numWarehouseItemID,0) > 0 THEN @numWarehouseItemID ELSE WareHouseItems.numWareHouseItemID END),
		@bitCalAmtBasedonDepItems = ISNULL(bitCalAmtBasedonDepItems,0)
	FROM
		Item
	OUTER APPLY
	(
		SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems WHERE numItemID=@numItemCode AND (numWareHouseItemID = @numWarehouseItemID OR ISNULL(@numWarehouseItemID,0) = 0)
	) AS WareHouseItems
	WHERE
		Item.numItemCode = @numItemCode

	IF ISNULL(@numDivisionID,0) > 0 AND EXISTS (SELECT * FROM Vendor WHERE numItemCode=@numItemCode AND numVendorID=@numDivisionID)
	BEGIN
		SELECT @monVendorCost=ISNULL(monCost,0) FROM Vendor WHERE numVendorID=@numDivisionID AND numItemCode=@numItemCode
	END


	IF @chrItemType = 'P' AND ISNULL(@numWarehouseItemID,0) = 0
	BEGIN
		RAISERROR('WAREHOUSE_DOES_NOT_EXISTS',16,1)
	END

	SET @fltUOMConversionFactor = dbo.fn_UOMConversion((CASE WHEN  @tintOppType = 1 THEN @numSaleUnit ELSE @numPurchaseUnit END),@numItemCode,@numDomainId,@numBaseUnit)

	IF @tintOppType = 1
	BEGIN
		/*Use Calculated Price When item is Assembly/Kit,Calculate price based on sum of dependent items.*/
		IF @bitCalAmtBasedonDepItems = 1 
		BEGIN
			PRINT 'IN DEPENDED ITEM'

			CREATE TABLE #TEMPSelectedKitChilds
			(
				ChildKitItemID NUMERIC(18,0),
				ChildKitWarehouseItemID NUMERIC(18,0),
				ChildKitChildItemID NUMERIC(18,0),
				ChildKitChildWarehouseItemID NUMERIC(18,0)
			)

			IF ISNULL(@numOppItemID,0) > 0
			BEGIN
				INSERT INTO 
					#TEMPSelectedKitChilds
				SELECT 
					OKI.numChildItemID,
					OKI.numWareHouseItemId,
					OKCI.numItemID,
					OKCI.numWareHouseItemId
				FROM
					OpportunityKitItems OKI
				INNER JOIN
					OpportunityKitChildItems OKCI
				ON
					OKI.numOppChildItemID = OKCI.numOppChildItemID
				WHERE
					OKI.numOppId = @numOppID
					AND OKI.numOppItemID = @numOppItemID
			END
			ELSE IF ISNULL(@vcSelectedKitChildItems,'') <> ''
			BEGIN
				INSERT INTO 
					#TEMPSelectedKitChilds
				SELECT 
					SUBSTRING
					(
						SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)),
						0,
						CHARINDEX('#', SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)))
					) AS numChildKitID,
					SUBSTRING
					(
						SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)),
						CHARINDEX('#', SUBSTRING(OutParam,0,CHARINDEX('-',OutParam))) + 1,
						LEN(SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)))
					) AS numChildKitWarehouseItemID,
					SUBSTRING
					(
						SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)),
						0,
						CHARINDEX('#', SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)))
					) AS numChildKitID,
					SUBSTRING
					(
						SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)),
						CHARINDEX('#', SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam))) + 1,
						LEN(SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)))
					) AS numChildKitWarehouseItemID
		
				FROM 
					dbo.SplitString(@vcSelectedKitChildItems,',')
			END

			;WITH CTE (numItemCode, vcItemName, bitKitParent, numWarehouseItemID,numQtyItemsReq,numUOMId) AS
			(
				SELECT
					ID.numChildItemID,
					I.vcItemName,
					ISNULL(I.bitKitParent,0),
					ISNULL(ID.numWarehouseItemId,0),
					CAST((ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT),
					ISNULL(numUOMId,0)
				FROM 
					[ItemDetails] ID
				INNER JOIN 
					[Item] I 
				ON 
					ID.[numChildItemID] = I.[numItemCode]
				WHERE   
					[numItemKitID] = @numItemCode
				UNION ALL
				SELECT
					ID.numChildItemID,
					I.vcItemName,
					ISNULL(I.bitKitParent,0),
					ISNULL(ID.numWarehouseItemId,0),
					CAST((ISNULL(Temp1.numQtyItemsReq,0) * ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT),
					ISNULL(ID.numUOMId,0)
				FROM 
					CTE As Temp1
				INNER JOIN
					[ItemDetails] ID
				ON
					ID.numItemKitID = Temp1.numItemCode
				INNER JOIN 
					[Item] I 
				ON 
					ID.[numChildItemID] = I.[numItemCode]
				INNER JOIN
					#TEMPSelectedKitChilds
				ON
					ISNULL(Temp1.bitKitParent,0) = 1
					AND ID.numChildItemID = #TEMPSelectedKitChilds.ChildKitChildItemID
					AND ID.numWareHouseItemId = #TEMPSelectedKitChilds.ChildKitChildWarehouseItemID
			)

			SELECT  
					@monListPrice = ISNULL(  SUM(ISNULL(CASE WHEN I.[charItemType]='P' 
					THEN WI.[monWListPrice] 
					ELSE monListPrice END,0) * ID.[numQtyItemsReq] )  ,0)
			FROM    CTE ID
					INNER JOIN [Item] I ON ID.numItemCode = I.[numItemCode]
					left join  WareHouseItems WI
					on WI.numItemID=I.numItemCode and WI.[numWareHouseItemID]=ID.numWarehouseItemID
			WHERE
				ISNULL(ID.bitKitParent,0) = 0

			DROP TABLE #TEMPSelectedKitChilds
		END

		IF @tintDefaultSalesPricing = 1 AND (SELECT COUNT(*) FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0) = 0) > 0 -- PRICE LEVEL
		BEGIN
			SELECT
				@tintDisountType = tintDiscountType,
				@decDiscount = ISNULL(decDiscount,0),
				@tintRuleType = tintRuleType,
				@monPriceLevelPrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost ELSE ISNULL(decDiscount,0) END),
				@monPriceFinalPrice = (CASE tintRuleType
										WHEN 1 -- Deduct From List Price
										THEN
											CASE tintDiscountType 
												WHEN 1 -- PERCENT
												THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN ISNULL(decDiscount,0)
											END
										WHEN 2 -- Add to primary vendor cost
										THEN
											CASE tintDiscountType 
												WHEN 1  -- PERCENT
												THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))  * @fltUOMConversionFactor
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN ISNULL(decDiscount,0)
											END
										WHEN 3 -- Named price
										THEN
											ISNULL(decDiscount,0)
										END
									) 
			FROM
			(
				SELECT 
					ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
					*
				FROM
					PricingTable
				WHERE 
					PricingTable.numItemCode = @numItemCode
					
			) TEMP
			WHERE
				numItemCode=@numItemCode AND ((tintRuleType = 3 AND Id=@tintPriceLevel) OR @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty)
		END
		ELSE IF @tintDefaultSalesPricing = 2 AND (SELECT COUNT(*) FROM PriceBookRules WHERE numDomainID=@numDomainID AND tintRuleFor=@tintOppType) > 0 -- PRICE RULE
		BEGIN
			DECLARE @numPriceRuleID NUMERIC(18,0)
			DECLARE @tintPriceRuleType AS TINYINT
			DECLARE @tintPricingMethod TINYINT
			DECLARE @tintPriceBookDiscountType TINYINT
			DECLARE @decPriceBookDiscount FLOAT
			DECLARE @intQntyItems INT
			DECLARE @decMaxDedPerAmt FLOAT

			SELECT 
				TOP 1
				@numPriceRuleID = numPricRuleID,
				@tintPricingMethod = tintPricingMethod,
				@tintPriceRuleType = p.tintRuleType,
				@tintPriceBookDiscountType = P.[tintDiscountType],
				@decPriceBookDiscount = P.[decDiscount],
				@intQntyItems = P.[intQntyItems],
				@decMaxDedPerAmt = P.[decMaxDedPerAmt]
			FROM   
				PriceBookRules P 
			LEFT JOIN 
				PriceBookRuleDTL PDTL 
			ON 
				P.numPricRuleID = PDTL.numRuleID
			LEFT JOIN 
				PriceBookRuleItems PBI 
			ON 
				P.numPricRuleID = PBI.numRuleID
			LEFT JOIN 
				[PriceBookPriorities] PP 
			ON 
				PP.[Step2Value] = P.[tintStep2] 
				AND PP.[Step3Value] = P.[tintStep3]
			WHERE  
				P.numDomainID=@numDomainID 
				AND tintRuleFor=@tintOppType
				AND (
						((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
						OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
						OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

						OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
						OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
						OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

						OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
						OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 3)) -- Priority 8
						OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
					)
			ORDER BY 
				PP.Priority ASC


			IF ISNULL(@numPriceRuleID,0) > 0
			BEGIN
				IF @tintPricingMethod = 1 -- BASED ON PRICE TABLE
				BEGIN
					SELECT
						@tintDisountType = tintDiscountType,
						@decDiscount = ISNULL(decDiscount,0),
						@tintRuleType = tintRuleType,
						@monPriceRulePrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) ,
						@monPriceRuleFinalPrice = (CASE tintRuleType
												WHEN 1 -- Deduct From List Price
												THEN
													CASE tintDiscountType 
														WHEN 1 -- PERCENT
														THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
														WHEN 2 -- FLAT AMOUNT
														THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
														WHEN 3 -- NAMED PRICE
														THEN ISNULL(decDiscount,0)
													END
												WHEN 2 -- Add to primary vendor cost
												THEN
													CASE tintDiscountType 
														WHEN 1  -- PERCENT
														THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))
														WHEN 2 -- FLAT AMOUNT
														THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
														WHEN 3 -- NAMED PRICE
														THEN ISNULL(decDiscount,0)
													END
												WHEN 3 -- Named price
												THEN
													CASE 
													WHEN ISNULL(@tintPriceLevel,0) > 0
													THEN
														(SELECT 
															 ISNULL(decDiscount,0)
														FROM
														(
															SELECT 
																ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
																decDiscount
															FROM 
																PricingTable 
															WHERE 
																PricingTable.numItemCode = @numItemCode 
																AND tintRuleType = 3 
														) TEMP
														WHERE
															Id = @tintPriceLevel)
													ELSE
														ISNULL(decDiscount,0)
													END
												END
											) 
					FROM
						PricingTable
					WHERE
						numPriceRuleID=@numPriceRuleID AND @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty
				END
				ELSE IF @tintPricingMethod = 2 -- BASED ON RULE
				BEGIN
					SET @tintRuleType = @tintPriceRuleType
					SET @monPriceRulePrice = (CASE @tintPriceRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) 

					IF (@tintPriceBookDiscountType = 1 ) -- Percentage 
					BEGIN
						SET @tintDisountType = @tintPriceBookDiscountType
						SET @decDiscount = @decDiscount * (@numQty/@intQntyItems);
						SET @decDiscount = @decDiscount * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
						SET @decMaxDedPerAmt = @decMaxDedPerAmt * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					
						IF (@decDiscount > @decMaxDedPerAmt)
							SET @decDiscount = @decMaxDedPerAmt;
				
						IF ( @tintPriceRuleType = 1 )
								SET @monPriceRuleFinalPrice = (@monListPrice - @decDiscount);
						IF ( @tintPriceRuleType = 2 )
								SET @monPriceRuleFinalPrice = (@monVendorCost + @decDiscount);
					END
					IF ( @tintPriceBookDiscountType = 2 ) -- Flat discount 
					BEGIN
						SET @decDiscount = @decDiscount * ((@numQty * @fltUOMConversionFactor)/@intQntyItems);

						IF (@decDiscount > @decMaxDedPerAmt)
							SET @decDiscount = @decMaxDedPerAmt;
					
						IF ( @tintPriceRuleType = 1 )
							SET @monPriceRuleFinalPrice =  ((@monListPrice * @numQty * @fltUOMConversionFactor) - @decDiscount)/(@numQty * @fltUOMConversionFactor);
						IF ( @tintPriceRuleType = 2 )
							SET @monPriceRuleFinalPrice = ((@monVendorCost * @numQty * @fltUOMConversionFactor) + @decDiscount)/(@numQty * @fltUOMConversionFactor);
					END
				END
			END
		
		END
	
		-- GET Last Price
		SELECT 
			TOP 1 
			@monLastPrice = monPrice,
			@dtLastOrderDate = dbo.FormatedDateFromDate(OpportunityMaster.bintCreatedDate,@numDomainID)
		FROM 
			OpportunityItems 
		JOIN 
			OpportunityMaster 
		ON 
			OpportunityItems.numOppId = OpportunityMaster.numOppId 
		WHERE 
			OpportunityMaster.numDomainId= @numDomainID
			AND tintOppType = @tintOppType
			AND tintOppStatus = 1
			AND numItemCode=@numItemCode
		ORDER BY
			OpportunityMaster.numOppId DESC

		IF @tintDefaultSalesPricing = 1 AND ISNULL(@monPriceFinalPrice,0) > 0
		BEGIN
			-- WE ARE NOT DISPLAYING DISCOUNT SEPERATELY IN PRICE LEVEL
			SET @tintPricingBasedOn = 1
			SET @monPrice = @monPriceFinalPrice
			SET @tintDisountType = 1
			SET @decDiscount = 0
		END
		ELSE IF @tintDefaultSalesPricing = 2 AND ISNULL(@monPriceRuleFinalPrice,0) > 0
		BEGIN
			SET @tintPricingBasedOn = 2
		
			IF @tintPriceBookDiscountType = 1 -- Display Disocunt Price As Unit Price
			BEGIN
				SET @monPrice = @monPriceRuleFinalPrice
				SET @tintDisountType = 1
				SET @decDiscount = 0
			END
			ELSE -- Display Unit Price And Disocunt Price Seperately
			BEGIN
				SET @monPrice = @monPriceRulePrice
			END
		
		END
		ELSE IF @tintDefaultSalesPricing = 3 AND ISNULL(@monLastPrice,0) > 0
		BEGIN
			SET @tintPricingBasedOn = 3
			SET @monPrice = @monLastPrice
		END
		ELSE
		BEGIN
			SET @tintPricingBasedOn = 0
			SET @tintDisountType = 1
			SET @decDiscount = 0
			SET @tintRuleType = 0

			IF @tintOppType = 1
				SET @monPrice = @monListPrice
			ELSE
				SET @monPrice = @monVendorCost
		END
	END
	ELSE IF @tintOppType = 2
	BEGIN
		SELECT 
			TOP 1
			@numPriceRuleID = numPricRuleID,
			@tintPricingMethod = tintPricingMethod,
			@tintPriceRuleType = p.tintRuleType,
			@tintPriceBookDiscountType = P.[tintDiscountType],
			@decPriceBookDiscount = P.[decDiscount],
			@intQntyItems = P.[intQntyItems],
			@decMaxDedPerAmt = P.[decMaxDedPerAmt]
		FROM   
			PriceBookRules P 
		LEFT JOIN 
			PriceBookRuleDTL PDTL 
		ON 
			P.numPricRuleID = PDTL.numRuleID
		LEFT JOIN 
			PriceBookRuleItems PBI 
		ON 
			P.numPricRuleID = PBI.numRuleID
		LEFT JOIN 
			[PriceBookPriorities] PP 
		ON 
			PP.[Step2Value] = P.[tintStep2] 
			AND PP.[Step3Value] = P.[tintStep3]
		WHERE  
			P.numDomainID=@numDomainID 
			AND tintRuleFor=@tintOppType
			AND (
					((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
					OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
					OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 3)) -- Priority 8
					OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
				)
		ORDER BY 
			PP.Priority ASC


		IF ISNULL(@numPriceRuleID,0) > 0
		BEGIN
			IF @tintPricingMethod = 1 -- BASED ON PRICE TABLE
			BEGIN
				SELECT
					@tintDisountType = tintDiscountType,
					@decDiscount = ISNULL(decDiscount,0),
					@tintRuleType = tintRuleType,
					@monPriceRulePrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) ,
					@monPriceRuleFinalPrice = (CASE tintRuleType
											WHEN 1 -- Deduct From List Price
											THEN
												CASE tintDiscountType 
													WHEN 1 -- PERCENT
													THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN ISNULL(decDiscount,0)
												END
											WHEN 2 -- Add to primary vendor cost
											THEN
												CASE tintDiscountType 
													WHEN 1  -- PERCENT
													THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN ISNULL(decDiscount,0)
												END
											WHEN 3 -- Named price
											THEN
												CASE 
												WHEN ISNULL(@tintPriceLevel,0) > 0
												THEN
													(SELECT 
															ISNULL(decDiscount,0)
													FROM
													(
														SELECT 
															ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
															decDiscount
														FROM 
															PricingTable 
														WHERE 
															PricingTable.numItemCode = @numItemCode 
															AND tintRuleType = 3 
													) TEMP
													WHERE
														Id = @tintPriceLevel)
												ELSE
													ISNULL(decDiscount,0)
												END
											END
										) 
				FROM
					PricingTable
				WHERE
					numPriceRuleID=@numPriceRuleID AND @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty
			END
			ELSE IF @tintPricingMethod = 2 -- BASED ON RULE
			BEGIN
				SET @tintRuleType = @tintPriceRuleType
				SET @monPriceRulePrice = (CASE @tintPriceRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) 

				IF (@tintPriceBookDiscountType = 1 ) -- Percentage 
				BEGIN
					SET @tintDisountType = @tintPriceBookDiscountType
					SET @decDiscount = @decDiscount * (@numQty/@intQntyItems);
					SET @decDiscount = @decDiscount * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					SET @decMaxDedPerAmt = @decMaxDedPerAmt * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					
					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
				
					IF ( @tintPriceRuleType = 1 )
							SET @monPriceRuleFinalPrice = (@monListPrice - @decDiscount);
					IF ( @tintPriceRuleType = 2 )
							SET @monPriceRuleFinalPrice = (@monVendorCost + @decDiscount);
				END
				IF ( @tintPriceBookDiscountType = 2 ) -- Flat discount 
				BEGIN
					SET @decDiscount = @decDiscount * ((@numQty * @fltUOMConversionFactor)/@intQntyItems);

					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
					
					IF ( @tintPriceRuleType = 1 )
						SET @monPriceRuleFinalPrice =  ((@monListPrice * @numQty * @fltUOMConversionFactor) - @decDiscount)/(@numQty * @fltUOMConversionFactor);
					IF ( @tintPriceRuleType = 2 )
						SET @monPriceRuleFinalPrice = ((@monVendorCost * @numQty * @fltUOMConversionFactor) + @decDiscount)/(@numQty * @fltUOMConversionFactor);
				END
			END
		END

		If @monPriceRuleFinalPrice > 0
		BEGIN
			SET @monPrice = @monPriceRuleFinalPrice
		END
		ELSE
		BEGIN
			SET @monPrice = @monVendorCost
		END


		SET @monPriceLevelPrice = 0
		SET @monPriceFinalPrice = 0
		SET @monPriceRulePrice = 0
		SET @monPriceRuleFinalPrice = 0
		SET @monLastPrice = 0
		SET @dtLastOrderDate = NULL
		SET @tintDisountType = 1
		SET @decDiscount =0 
	END


	

	SELECT
		@vcItemName AS vcItemName,
		@bitDropship AS bitDropship,
		@chrItemType AS ItemType,
		@bitMatrix AS bitMatrix,
		@numItemClassification AS ItemClassification,
		@numItemGroup AS ItemGroup,
		@ItemDesc AS ItemDescription,
		@bitAssembly AS bitAssembly,
		case when ISNULL(@bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(@numItemCode) else 0 end AS MaxWorkOrderQty,
		@tintPricingBasedOn AS tintPriceBaseOn,
		@monPrice AS monPrice,
		@fltUOMConversionFactor AS UOMConversionFactor,
		(CASE WHEN @tintOppType=1 THEN @monListPrice ELSE @monVendorCost END) AS monListPrice,
		@monVendorCost AS VendorCost,
		@numVendorID AS numVendorID,
		@tintRuleType AS tintRuleType,
		@monPriceLevelPrice AS monPriceLevel,
		@monPriceFinalPrice AS monPriceLevelFinalPrice,
		@monPriceRulePrice AS monPriceRule,
		@monPriceRuleFinalPrice AS monPriceRuleFinalPrice,
		@monLastPrice AS monLastPrice,
		@dtLastOrderDate AS LastOrderDate,
		@tintDisountType AS DiscountType,
		@decDiscount AS Discount,
		@numBaseUnit AS numBaseUnit,
		@numSaleUnit AS numSaleUnit,
		@numPurchaseUnit AS numPurchaseUnit,
		dbo.fn_GetUOMName(@numBaseUnit) AS vcBaseUOMName,
		dbo.fn_GetUOMName(@numSaleUnit) AS vcSaleUOMName,
		@numPurchaseUnit AS numPurchaseUnit,
		@numWarehouseItemID AS numWarehouseItemID,
		dbo.fn_GetItemTransitCount(@numItemcode,@numDomainID) AS OrdersInTransist,
		(SELECT COUNT(*) FROM SimilarItems SI INNER JOIN Item I ON I.numItemCode = SI.numItemCode WHERE SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numItemCode) AS RelatedItemsCount,
		@tintPriceLevel AS tintPriceLevel,
		@vcMinOrderQty AS vcMinOrderQty,
		@vcVendorNotes AS vcVendorNotes


	-- CHECK IF PROMOTION CAN BE TRIGGERED FROM THIS ITEM
	DECLARE @numPromotionID AS NUMERIC(18,0)

	SELECT TOP 1
		@numPromotionID = numProId 
	FROM 
		PromotionOffer PO
	WHERE 
		numDomainId=@numDomainID 
		AND ISNULL(bitEnabled,0)=1
		AND ISNULL(bitApplyToInternalOrders,0)=1 
		AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
		AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
		AND 1 = (CASE 
					WHEN tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=@numItemCode) > 0 THEN 1 ELSE 0 END)
					WHEN tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=@numItemClassification) > 0 THEN 1 ELSE 0 END)
					ELSE 0
				END)
	ORDER BY
		CASE 
			WHEN ISNULL(PO.bitRequireCouponCode,0)=1 AND PO.txtCouponCode=@vcCoupon THEN 1
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 2
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 3
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 4
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 5
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 6
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 7
		END


	IF ISNULL(@numPromotionID,0) > 0
	BEGIN
		-- Get Top 1 Promotion Based on priority

		SELECT 
			numProId
			,vcProName
			,bitNeverExpires
			,bitRequireCouponCode
			,txtCouponCode
			,tintUsageLimit
			,intCouponCodeUsed
			,bitFreeShiping
			,monFreeShippingOrderAmount
			,numFreeShippingCountry
			,bitFixShipping1
			,monFixShipping1OrderAmount
			,monFixShipping1Charge
			,bitFixShipping2
			,monFixShipping2OrderAmount
			,monFixShipping2Charge
			,tintOfferTriggerValueType
			,fltOfferTriggerValue
			,tintOfferBasedOn
			,fltDiscountValue
			,tintDiscountType
			,tintDiscoutBaseOn
			,dbo.FormatedDateFromDate(dtValidTo,@numDomainID) AS dtExpire
			,(CASE tintOfferBasedOn
				WHEN 1 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 1, ''))
				WHEN 2 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 1, ''))
			END) As vcPromotionItems
			,(CASE tintDiscoutBaseOn
				WHEN 1 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, ''))
				WHEN 2 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, ''))
				WHEN 3 THEN (SELECT Stuff((SELECT CONCAT(',',SimilarItems.numItemCode) FROM SimilarItems WHERE 1 = (CASE 
																														WHEN tintOfferBasedOn=1 THEN CASE WHEN numParentItemCode IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1) THEN 1 ELSE 0 END
																														WHEN tintOfferBasedOn=2 THEN CASE WHEN numParentItemCode IN (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemClassification IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2)) THEN 1 ELSE 0 END
																														ELSE 0
																													END) FOR XML PATH('')), 1, 1, ''))
			END) As vcItems
			,CONCAT('Buy '
					,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
					,CASE tintOfferBasedOn
							WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
							WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
						END
					,' & get '
					, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
					, CASE 
						WHEN tintDiscoutBaseOn = 1 THEN 
							CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 2 THEN 
							CONCAT
							(
								CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(',',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
						ELSE '' 
					END
				) AS vcPromotionDescription
				,CONCAT
				(
					(CASE WHEN ISNULL(bitFixShipping1,0)=1 THEN CONCAT('Spend $',monFixShipping1OrderAmount,' and get $',monFixShipping1Charge,' shipping. ') ELSE '' END)
					,(CASE WHEN ISNULL(bitFixShipping2,0)=1  THEN CONCAT('Spend $',monFixShipping2OrderAmount,' and get $',monFixShipping2Charge,' shipping. ') ELSE '' END)
					,(CASE WHEN (bitFreeShiping=1 AND numFreeShippingCountry=@numShippingCountry) THEN CONCAT('Spend $',monFreeShippingOrderAmount,' and shipping is FREE! ') ELSE '' END) 
				) AS vcShippingDescription,
				CASE 
					WHEN ISNULL(PO.bitRequireCouponCode,0)=1 AND PO.txtCouponCode=@vcCoupon THEN 1
					WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 2
					WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 3
					WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 4
					WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 5
					WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 6
					WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 7
				END AS tintPriority
		FROM
			PromotionOffer PO
		WHERE
			numDomainId=@numDomainID 
			AND numProId = @numPromotionID
	END
END 
GO
/****** Object:  StoredProcedure [dbo].[USP_UpdatedomainDetails]    Script Date: 05/07/2009 21:56:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdatedomainDefaultContactAddress')
DROP PROCEDURE USP_UpdatedomainDefaultContactAddress
GO
CREATE PROCEDURE [dbo].[USP_UpdatedomainDefaultContactAddress]                                      
@numDomainID as numeric(9)=0,                                      
@bitAutoPopulateAddress as bit=null,
@tintPoulateAddressTo as tinyint=null
as                                      
BEGIN                                      
	UPDATE 
		Domain                                       
	SET      
		bitAutoPopulateAddress = @bitAutoPopulateAddress,
		tintPoulateAddressTo = @tintPoulateAddressTo
	WHERE 
		numDomainId=@numDomainID
END
GO
