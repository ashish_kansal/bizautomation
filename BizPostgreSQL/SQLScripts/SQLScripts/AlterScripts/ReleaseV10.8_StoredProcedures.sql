/******************************************************************
Project: Release 10.8 Date: 10.JAN.2019
Comments: STORE PROCEDURES
*******************************************************************/


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='CheckIfAssemblyKitChildItemsIsOnBackorder')
DROP FUNCTION CheckIfAssemblyKitChildItemsIsOnBackorder
GO
CREATE FUNCTION [dbo].[CheckIfAssemblyKitChildItemsIsOnBackorder] 
(
	@numDomainID NUMERIC(18,0),
	@numOppID NUMERIC(18,0),
	@numOppItemID NUMERIC(18,0),
	@numOppChildItemID AS NUMERIC(18,0),
	@numOppKitChildItemID AS NUMERIC(18,0),
	@bitKitParent BIT,
	@bitWorkOrder BIT,
	@numQuantity AS FLOAT
)
RETURNS BIT
AS
BEGIN

    DECLARE @bitBackOrder FLOAT = 0
	DECLARE @numOnHand FLOAT
	DECLARE @numAllocation FLOAT
	DECLARE @numQtyShipped FLOAT = 0
	DECLARE @numWarehouseItemID NUMERIC(18,0)
	DECLARE @numWOStatus NUMERIC(18,0)

	DECLARE @tintCommitAllocation AS TINYINT
	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID 

	IF ISNULL(@numOppChildItemID,0) > 0 AND ISNULL(@numOppKitChildItemID,0) = 0
	BEGIN
		SELECT @numQtyShipped = ISNULL(numQtyShipped,0),@numWarehouseItemID=ISNULL(numWareHouseItemId,0) FROM OpportunityKitItems WHERE numOppChildItemID = @numOppChildItemID
		SELECT @numWOStatus=ISNULL(numWOStatus,0) FROM WorkOrder WHERE numOppId=@numOppID AND numOppItemID=@numOppItemID AND numOppChildItemID=@numOppChildItemID
	END
	ELSE IF ISNULL(@numOppChildItemID,0) > 0 AND ISNULL(@numOppKitChildItemID,0) > 0
	BEGIN
		SELECT @numQtyShipped = ISNULL(numQtyShipped,0),@numWarehouseItemID=ISNULL(numWareHouseItemId,0) FROM OpportunityKitChildItems WHERE numOppKitChildItemID = @numOppKitChildItemID
		SELECT @numWOStatus=ISNULL(numWOStatus,0) FROM WorkOrder WHERE numOppId=@numOppID AND numOppItemID=@numOppItemID AND numOppChildItemID=@numOppChildItemID AND numOppKitChildItemID=@numOppKitChildItemID
	END

	SELECT @numQuantity = @numQuantity - @numQtyShipped
	
	IF @bitWorkOrder = 1
	BEGIN
		IF @numWOStatus = 23184
		BEGIN
			IF @numQuantity > 0
			BEGIN
				IF (SELECT COUNT(numWareHouseItemID) FROM WareHouseItems WHERE numWareHouseItemID=@numWarehouseItemID AND numAllocation >= @numQuantity) > 0
				BEGIN
					SET @bitBackOrder = 0
				END
				ELSE
				BEGIN
					SET @bitBackOrder = 1
				END
			END	
			ELSE 
			BEGIN
				SET @bitBackOrder = 0
			END
		END
		ELSE
		BEGIN
			DECLARE @TEMP TABLE
			(
				ItemLevel INT
				,numParentWOID NUMERIC(18,0)
				,numWOID NUMERIC(18,0)
				,numItemCode NUMERIC(18,0)
				,numQty FLOAT
				,numQtyRequiredToBuild FLOAT
				,numWarehouseItemID NUMERIC(18,0)
				,bitWorkOrder BIT
				,bitReadyToBuild BIT
			)

			;WITH CTEWorkOrder (ItemLevel,numParentWOID,numWOID,numItemCode,numWarehouseItemID,bitWorkOrder,numQty,numQtyRequiredToBuild) AS
			(
				SELECT
					1,
					numParentWOID,
					numWOId,
					numItemCode,
					numWareHouseItemId,
					1 AS bitWorkOrder,
					@numQuantity,
					CAST(1 AS FLOAT)
				FROM
					WorkOrder
				WHERE
					numOppId=@numOppID 
					AND numOppItemID=@numOppItemID
					AND numOppChildItemID=@numOppChildItemID
					AND ISNULL(numOppKitChildItemID,0)=@numOppKitChildItemID
				UNION ALL
				SELECT 
					c.ItemLevel+2,
					c.numWOID,
					WorkOrder.numWOId,
					WorkOrder.numItemCode,
					WorkOrder.numWareHouseItemId,
					1 AS bitWorkOrder,
					c.numQty * WOD.numQtyItemsReq_Orig,
					WOD.numQtyItemsReq_Orig
				FROM 
					WorkOrder
				INNER JOIN 
					CTEWorkOrder c 
				ON 
					WorkOrder.numParentWOID = c.numWOID
				INNER JOIN
					WorkOrderDetails WOD
				ON
					WOD.numWOId = c.numWOID
					AND WOD.numChildItemID = WorkOrder.numItemCode
					AND ISNULL(WOD.numWareHouseItemId,0) = ISNULL(WorkOrder.numWareHouseItemId,0)
			)

			INSERT INTO @TEMP
			(
				ItemLevel
				,numParentWOID
				,numWOID
				,numItemCode
				,numQty
				,numQtyRequiredToBuild
				,numWarehouseItemID
				,bitWorkOrder
				,bitReadyToBuild
			)
			SELECT
				ItemLevel,
				numParentWOID,
				numWOID,
				numItemCode,
				numQty,
				numQtyRequiredToBuild,
				CTEWorkOrder.numWarehouseItemID,
				bitWorkOrder,
				0
			FROM 
				CTEWorkOrder
		
			INSERT INTO @TEMP
			(
				ItemLevel
				,numParentWOID
				,numWOID
				,numItemCode
				,numQty
				,numWarehouseItemID
				,bitWorkOrder
				,bitReadyToBuild
			)
			SELECT
				t1.ItemLevel + 1,
				t1.numWOID,
				NULL,
				WorkOrderDetails.numChildItemID,
				t1.numQty * WorkOrderDetails.numQtyItemsReq_Orig,
				WorkOrderDetails.numWarehouseItemID,
				0 AS bitWorkOrder,
				CASE 
					WHEN Item.charItemType='P'
					THEN
						CASE 
							WHEN @tintCommitAllocation=2 
							THEN (CASE WHEN ISNULL(numQtyItemsReq,0) > WareHouseItems.numOnHand THEN 0 ELSE 1 END)
							ELSE (CASE WHEN ISNULL(numQtyItemsReq,0) > WareHouseItems.numAllocation THEN 0 ELSE 1 END) 
						END
					ELSE 1
				END
			FROM
				WorkOrderDetails
			INNER JOIN
				@TEMP t1
			ON
				WorkOrderDetails.numWOId = t1.numWOID
			INNER JOIN 
				Item
			ON
				WorkOrderDetails.numChildItemID = Item.numItemCode
				AND 1= (CASE WHEN (SELECT COUNT(*) FROM @TEMP TInner WHERE TInner.bitWorkOrder=1 AND TInner.numParentWOID=t1.numWOID AND TInner.numItemCode=Item.numItemCode) > 0 THEN 0 ELSE 1 END)
			LEFT JOIN
				WareHouseItems
			ON
				WorkOrderDetails.numWarehouseItemID = WareHouseItems.numWareHouseItemID
			LEFT JOIN
				Warehouses
			ON
				WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
			LEFT JOIN
				OpportunityMaster 
			ON
				WorkOrderDetails.numPOID = OpportunityMaster.numOppId

			DECLARE @i AS INT = 1
			DECLARE @iCount AS INT
			DECLARE @numTempWOID NUMERIC(18,0)

			DECLARE @TEMPWO TABLE
			(
				ID INT IDENTITY(1,1)
				,numWOID NUMERIC(18,0)
			)
			INSERT INTO @TEMPWO (numWOID) SELECT numWOID FROM @TEMP WHERE bitWorkOrder=1 ORDER BY ItemLevel DESC

			SELECT @iCount=COUNT(*) FROM @TEMPWO

			WHILE @i <= @iCount
			BEGIN
				SELECT @numTempWOID=numWOID FROM @TEMPWO WHERE ID=@i

				UPDATE
					T1
				SET
					bitReadyToBuild = (CASE WHEN (SELECT COUNT(*) FROM @TEMP TInner WHERE TInner.numParentWOID=T1.numWOID AND bitReadyToBuild=0) > 0 THEN 0 ELSE 1 END)
				FROM
					@TEMP T1
				WHERE
					numWOID=@numTempWOID

				SET @i = @i + 1
			END

			IF (SELECT COUNT(*) FROM @TEMP WHERE ISNULL(bitReadyToBuild,0)=0) > 0
			BEGIN
				SET @bitBackOrder = 1
			END
			ELSE
			BEGIN
				SET @bitBackOrder = 0
			END
		END
	END
	ELSE IF @bitKitParent = 1
	BEGIN
		DECLARE @TEMPTABLE TABLE
		(
			numItemCode NUMERIC(18,0),
			numParentItemID NUMERIC(18,0),
			numOppChildItemID NUMERIC(18,0),
			numOppKitChildItemID NUMERIC(18,0),
			vcItemName VARCHAR(300),
			bitAssembly BIT,
			bitKitParent BIT,
			numWarehouseItmsID NUMERIC(18,0),
			numQtyItemsReq FLOAT,
			numQtyItemReq_Orig FLOAT,
			numOnHand FLOAT,
			numAllocation FLOAT,
			numPrentItemBackOrder INT
		)

		IF ISNULL(@numOppChildItemID,0) = 0
		BEGIN
			INSERT INTO 
				@TEMPTABLE
			SELECT
				OKI.numChildItemID,
				0,
				OKI.numOppChildItemID,
				0,
				I.vcItemName,
				I.bitAssembly,
				I.bitKitParent,
				OKI.numWareHouseItemId,
				OKI.numQtyItemsReq_Orig * @numQuantity,
				OKI.numQtyItemsReq_Orig,
				ISNULL(WI.numOnHand,0),
				ISNULL(WI.numAllocation,0),
				0
			FROM
				OpportunityKitItems OKI
			INNER JOIN
				WareHouseItems WI
			ON
				OKI.numWareHouseItemId = WI.numWareHouseItemID
			INNER JOIN
				OpportunityItems OI
			ON
				OKI.numOppItemID = OI.numoppitemtCode
			INNER JOIN
				Item I
			ON
				OKI.numChildItemID = I.numItemCode
			WHERE
				OI.numoppitemtCode = @numOppItemID
				AND ISNULL(I.bitKitParent,0) = 0
		END

		INSERT INTO
			@TEMPTABLE
		SELECT
			OKCI.numItemID,
			t1.numItemCode,
			t1.numOppChildItemID,
			OKCI.numOppKitChildItemID,
			I.vcItemName,
			I.bitAssembly,
			I.bitKitParent,
			OKCI.numWareHouseItemId,
			OKCI.numQtyItemsReq_Orig * t1.numQtyItemsReq,
			OKCI.numQtyItemsReq_Orig,
			ISNULL(WI.numOnHand,0),
			ISNULL(WI.numAllocation,0),
			0
		FROM
			OpportunityKitChildItems OKCI
		INNER JOIN
			WareHouseItems WI
		ON
			OKCI.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			@TEMPTABLE t1
		ON
			OKCI.numOppChildItemID = t1.numOppChildItemID
		INNER JOIN
			Item I
		ON
			OKCI.numItemID = I.numItemCode
			AND ISNULL(I.bitKitParent,0) = 0
		
		-- FIRST GET SUB KIT BACK ORDER NUMBER
		IF EXISTS (
					SELECT
						numItemCode
					FROM
						@TEMPTABLE
					WHERE
						1 = CASE 
								WHEN ISNULL(bitAssembly,0) = 1
								THEN
									dbo.CheckIfAssemblyKitChildItemsIsOnBackorder(@numDomainID,@numOppID,@numOppItemID,numOppChildItemID,numOppKitChildItemID,0,1,numQtyItemsReq)
								WHEN ISNULL(bitKitParent,0) = 1
								THEN
									dbo.CheckIfAssemblyKitChildItemsIsOnBackorder(@numDomainID,@numOppID,@numOppItemID,numOppChildItemID,numOppKitChildItemID,1,0,numQtyItemsReq)
								ELSE
									CASE 
										WHEN @tintCommitAllocation=2 
										THEN (CASE WHEN ISNULL(numQtyItemsReq,0) >= numOnHand THEN 0 ELSE 1 END)
										ELSE (CASE WHEN ISNULL(numQtyItemsReq,0) >= numAllocation THEN 0 ELSE 1 END) 
									END
							END
					)
		BEGIN
			SET @bitBackOrder = 1
		END
		ELSE
		BEGIN
			SET @bitBackOrder = 0
		END
	END
	ELSE
	BEGIN
		SELECT
			@numOnHand = ISNULL(numOnHand,0)
			,@numAllocation = ISNULL(numAllocation,0)
		FROM
			WareHouseItems 
		WHERE
			numWareHouseItemID = @numWarehouseItemID

		IF @tintCommitAllocation=2 
		BEGIn
			IF (@numQuantity - @numQtyShipped) > @numOnHand
				SET @bitBackOrder = 1
			ELSE
				SET @bitBackOrder = 0
		END
		ELSE
		BEGIN
			IF (@numQuantity - @numQtyShipped) > @numAllocation
				SET @bitBackOrder = 1
			ELSE
				SET @bitBackOrder = 0
		END
	END

	RETURN @bitBackOrder

END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='fn_GetKitAssemblyCalculatedPrice')
DROP FUNCTION fn_GetKitAssemblyCalculatedPrice
GO
CREATE FUNCTION [dbo].[fn_GetKitAssemblyCalculatedPrice]
(
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numWarehouseItemID NUMERIC(18,0)
	,@tintKitAssemblyPriceBasedOn TINYINT
	,@numOppID NUMERIC(18,0)
	,@numOppItemID NUMERIC(18,0)
	,@vcSelectedKitChildItems VARCHAR(MAX)
)    
RETURNS @TempPrice TABLE
(
	bitSuccess BIT
	,monPrice DECIMAL(20,5)
)
AS    
BEGIN   
	DECLARE @monListPrice DECIMAL(20,5)

	SELECT 
		@monListPrice=ISNULL(monListPrice,0)
	FROM 
		Item 
	WHERE 
		numItemCode=@numItemCode

	DECLARE @numWarehouseID NUMERIC(18,0)

	IF ISNULL(@numWarehouseItemID,0) > 0
	BEGIN
		SELECT @numWarehouseID=numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID =@numWarehouseItemID
	END
	ELSE
	BEGIN
		SELECt TOP 1 @numWarehouseID=numWareHouseID FROM Warehouses WHERE numDomainID=@numDomainID
	END

	DECLARE @KitAssemblyPrice AS DECIMAL(20,5) = 0

	DECLARE @TEMPitems TABLE
	(
		numItemCode NUMERIC(18,0)
		,numQtyItemsReq FLOAT
		,numUOMId NUMERIC(18,0)
		,bitKitParent BIT
	)

	DECLARE @TEMPSelectedKitChilds TABLE
	(
		ChildKitItemID NUMERIC(18,0),
		ChildKitWarehouseItemID NUMERIC(18,0),
		ChildKitChildItemID NUMERIC(18,0),
		ChildKitChildWarehouseItemID NUMERIC(18,0)
	)

	IF ISNULL(@numOppItemID,0) > 0
	BEGIN
		INSERT INTO 
			@TEMPSelectedKitChilds
		SELECT 
			OKI.numChildItemID,
			OKI.numWareHouseItemId,
			OKCI.numItemID,
			OKCI.numWareHouseItemId
		FROM
			OpportunityKitItems OKI
		INNER JOIN
			OpportunityKitChildItems OKCI
		ON
			OKI.numOppChildItemID = OKCI.numOppChildItemID
		WHERE
			OKI.numOppId = @numOppID
			AND OKI.numOppItemID = @numOppItemID
	END
	ELSE IF ISNULL(@vcSelectedKitChildItems,'') <> ''
	BEGIN
		INSERT INTO @TEMPSelectedKitChilds
		(
			ChildKitItemID
			,ChildKitChildItemID
		)
		SELECT 
			SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)),
			SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam))
		FROM 
			dbo.SplitString(@vcSelectedKitChildItems,',')

		UPDATE 
			@TEMPSelectedKitChilds 
		SET 
			ChildKitWarehouseItemID=(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numItemID=ChildKitItemID AND numWareHouseID=@numWarehouseID)
			,ChildKitChildWarehouseItemID=(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numItemID=ChildKitChildItemID AND numWareHouseID=@numWarehouseID)
	END


	;WITH CTE (numItemCode, vcItemName, bitKitParent, bitCalAmtBasedonDepItems, numQtyItemsReq,numUOMId) AS
	(
		SELECT
			ID.numChildItemID,
			I.vcItemName,
			ISNULL(I.bitKitParent,0),
			ISNULL(I.bitCalAmtBasedonDepItems,0),
			CAST((ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT),
			ISNULL(numUOMId,0)
		FROM 
			[ItemDetails] ID
		INNER JOIN 
			[Item] I 
		ON 
			ID.[numChildItemID] = I.[numItemCode]
		WHERE   
			[numItemKitID] = @numItemCode
			AND 1 = (CASE 
						WHEN ISNULL(I.bitKitParent,0) = 1 AND LEN(ISNULL(@vcSelectedKitChildItems,'')) > 0 THEN 
							(CASE 
								WHEN numChildItemID IN (SELECT ChildKitItemID FROM @TEMPSelectedKitChilds) 
								THEN 1 
								ELSE 0 
							END) 
						ELSE (CASE 
								WHEN (SELECT COUNT(*) FROM @TEMPSelectedKitChilds WHERE ISNULL(ChildKitItemID,0)=0) > 0
								THEN	
									(CASE 
										WHEN numChildItemID IN (SELECT ChildKitChildItemID FROM @TEMPSelectedKitChilds WHERE ISNULL(ChildKitItemID,0)=0) 
										THEN 1 
										ELSE 0 
									END) 
								ELSE 1
							END)
					END)
		UNION ALL
		SELECT
			ID.numChildItemID,
			I.vcItemName,
			ISNULL(I.bitKitParent,0),
			ISNULL(I.bitCalAmtBasedonDepItems,0),
			CAST((ISNULL(Temp1.numQtyItemsReq,0) * ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT),
			ISNULL(ID.numUOMId,0)
		FROM 
			CTE As Temp1
		INNER JOIN
			[ItemDetails] ID
		ON
			ID.numItemKitID = Temp1.numItemCode
		INNER JOIN 
			[Item] I 
		ON 
			ID.[numChildItemID] = I.[numItemCode]
		WHERE
			Temp1.bitKitParent = 1
			AND Temp1.bitCalAmtBasedonDepItems = 1
			AND EXISTS (SELECT ChildKitItemID FROM @TEMPSelectedKitChilds T2 WHERE ISNULL(Temp1.bitKitParent,0) = 1 AND ID.numChildItemID = T2.ChildKitChildItemID)
	)

	INSERT INTO @TEMPitems
	(
		numItemCode
		,numQtyItemsReq
		,numUOMId
		,bitKitParent
	)
	SELECT
		numItemCode
		,numQtyItemsReq
		,numUOMId
		,bitKitParent
	FROM
		CTE

	IF EXISTS (SELECT ID.numItemCode FROM @TEMPitems ID INNER JOIN [Item] I ON ID.numItemCode = I.[numItemCode] WHERE I.charItemType = 'P' AND (SELECT COUNT(*) FROM WareHouseItems WHERE numItemID=I.numItemCode AND numWareHouseID=@numWarehouseID) = 0)
	BEGIN
		INSERT INTO @TempPrice
		(
			bitSuccess
			,monPrice
		)
		VALUES
		(
			0
			,0
		)
	END
	ELSE
	BEGIN
		INSERT INTO @TempPrice
		(
			bitSuccess
			,monPrice
		)
		SELECT
			1
			,(CASE WHEN @tintKitAssemblyPriceBasedOn=4 THEN ISNULL(@monListPrice,0) + ISNULL(SUM(CalculatedPrice),0) ELSE ISNULL(SUM(CalculatedPrice),0) END)
		FROM
		(
			SELECT  
				ISNULL(CASE 
						WHEN I.[charItemType]='P' 
						THEN 
							CASE 
							WHEN ISNULL(I.bitAssembly,0) = 1 AND ISNULL(I.bitCalAmtBasedonDepItems,0) = 1
							THEN
								ISNULL((SELECT monPrice FROM dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,I.numItemCode,WI.numWareHouseItemID,I.tintKitAssemblyPriceBasedOn,0,0,'')),0)
							ELSE
								(CASE @tintKitAssemblyPriceBasedOn 
										WHEN 2 THEN ISNULL(I.monAverageCost,0) 
										WHEN 3 THEN ISNULL(monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit)
										ELSE WI.[monWListPrice]
								END) 
							END
						ELSE (CASE @tintKitAssemblyPriceBasedOn 
								WHEN 2 THEN ISNULL(I.monAverageCost,0)
								WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit)
								ELSE I.[monListPrice] 
							END)
					END,0) * ID.[numQtyItemsReq] AS CalculatedPrice
			FROM 
				@TEMPitems ID
			INNER JOIN 
				[Item] I 
			ON 
				ID.numItemCode = I.[numItemCode]
			LEFT JOIN
				Vendor V
			ON
				I.numVendorID = V.numVendorID
				AND I.numItemCode = V.numItemCode
			OUTER APPLY
			(
				SELECT TOP 1 
					*
				FROM
					WareHouseItems WI
				WHERE 
					WI.numItemID=I.numItemCode 
					AND WI.numWareHouseID = @numWarehouseID
				ORDER BY
					WI.numWareHouseItemID
			) AS WI
		) T1
	END

	RETURN
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetCalculatedPriceForKitAssembly')
DROP FUNCTION GetCalculatedPriceForKitAssembly
GO
CREATE FUNCTION [dbo].[GetCalculatedPriceForKitAssembly]
(
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numWarehouseItemID NUMERIC(18,0)
	,@tintKitAssemblyPriceBasedOn TINYINT
	,@numOppID NUMERIC(18,0)
	,@numOppItemID NUMERIC(18,0)
	,@vcSelectedKitChildItems VARCHAR(MAX)
)    
RETURNS DECIMAL(20,5)
AS    
BEGIN   
	DECLARE @monCalculatedPrice DECIMAL(20,5) = 0

	DECLARE @monListPrice DECIMAL(20,5)

	SELECT 
		@monListPrice=ISNULL(monListPrice,0)
	FROM 
		Item 
	WHERE 
		numItemCode=@numItemCode

	DECLARE @numWarehouseID NUMERIC(18,0)

	IF ISNULL(@numWarehouseItemID,0) > 0
	BEGIN
		SELECT @numWarehouseID=numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID =@numWarehouseItemID
	END
	ELSE
	BEGIN
		SELECt TOP 1 @numWarehouseID=numWareHouseID FROM Warehouses WHERE numDomainID=@numDomainID
	END

	DECLARE @KitAssemblyPrice AS DECIMAL(20,5) = 0

	DECLARE @TEMPitems TABLE
	(
		numItemCode NUMERIC(18,0)
		,numQtyItemsReq FLOAT
		,numUOMId NUMERIC(18,0)
		,bitKitParent BIT
	)

	DECLARE @TEMPSelectedKitChilds TABLE
	(
		ChildKitItemID NUMERIC(18,0),
		ChildKitWarehouseItemID NUMERIC(18,0),
		ChildKitChildItemID NUMERIC(18,0),
		ChildKitChildWarehouseItemID NUMERIC(18,0)
	)

	IF ISNULL(@numOppItemID,0) > 0
	BEGIN
		INSERT INTO 
			@TEMPSelectedKitChilds
		SELECT 
			OKI.numChildItemID,
			OKI.numWareHouseItemId,
			OKCI.numItemID,
			OKCI.numWareHouseItemId
		FROM
			OpportunityKitItems OKI
		INNER JOIN
			OpportunityKitChildItems OKCI
		ON
			OKI.numOppChildItemID = OKCI.numOppChildItemID
		WHERE
			OKI.numOppId = @numOppID
			AND OKI.numOppItemID = @numOppItemID
	END
	ELSE IF ISNULL(@vcSelectedKitChildItems,'') <> ''
	BEGIN
		INSERT INTO @TEMPSelectedKitChilds
		(
			ChildKitItemID
			,ChildKitChildItemID
		)
		SELECT 
			SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)),
			SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam))
		FROM 
			dbo.SplitString(@vcSelectedKitChildItems,',')

		UPDATE 
			@TEMPSelectedKitChilds 
		SET 
			ChildKitWarehouseItemID=(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numItemID=ChildKitItemID AND numWareHouseID=@numWarehouseID)
			,ChildKitChildWarehouseItemID=(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numItemID=ChildKitChildItemID AND numWareHouseID=@numWarehouseID)
	END


	;WITH CTE (numItemCode, vcItemName, bitKitParent, bitCalAmtBasedonDepItems, numQtyItemsReq,numUOMId) AS
	(
		SELECT
			ID.numChildItemID,
			I.vcItemName,
			ISNULL(I.bitKitParent,0),
			ISNULL(I.bitCalAmtBasedonDepItems,0),
			CAST((ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT),
			ISNULL(numUOMId,0)
		FROM 
			[ItemDetails] ID
		INNER JOIN 
			[Item] I 
		ON 
			ID.[numChildItemID] = I.[numItemCode]
		WHERE   
			[numItemKitID] = @numItemCode
			AND 1 = (CASE 
						WHEN ISNULL(I.bitKitParent,0) = 1 AND LEN(ISNULL(@vcSelectedKitChildItems,'')) > 0 THEN 
							(CASE 
								WHEN numChildItemID IN (SELECT ChildKitItemID FROM @TEMPSelectedKitChilds) 
								THEN 1 
								ELSE 0 
							END) 
						ELSE (CASE 
								WHEN (SELECT COUNT(*) FROM @TEMPSelectedKitChilds WHERE ISNULL(ChildKitItemID,0)=0) > 0
								THEN	
									(CASE 
										WHEN numChildItemID IN (SELECT ChildKitChildItemID FROM @TEMPSelectedKitChilds WHERE ISNULL(ChildKitItemID,0)=0) 
										THEN 1 
										ELSE 0 
									END) 
								ELSE 1
							END)
					END)
		UNION ALL
		SELECT
			ID.numChildItemID,
			I.vcItemName,
			ISNULL(I.bitKitParent,0),
			ISNULL(I.bitCalAmtBasedonDepItems,0),
			CAST((ISNULL(Temp1.numQtyItemsReq,0) * ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT),
			ISNULL(ID.numUOMId,0)
		FROM 
			CTE As Temp1
		INNER JOIN
			[ItemDetails] ID
		ON
			ID.numItemKitID = Temp1.numItemCode
		INNER JOIN 
			[Item] I 
		ON 
			ID.[numChildItemID] = I.[numItemCode]
		WHERE
			Temp1.bitKitParent = 1
			AND Temp1.bitCalAmtBasedonDepItems = 1
			AND EXISTS (SELECT ChildKitItemID FROM @TEMPSelectedKitChilds T2 WHERE ISNULL(Temp1.bitKitParent,0) = 1 AND ID.numChildItemID = T2.ChildKitChildItemID)
	)

	INSERT INTO @TEMPitems
	(
		numItemCode
		,numQtyItemsReq
		,numUOMId
		,bitKitParent
	)
	SELECT
		numItemCode
		,numQtyItemsReq
		,numUOMId
		,bitKitParent
	FROM
		CTE

	
	SELECT
		@monCalculatedPrice = (CASE WHEN @tintKitAssemblyPriceBasedOn=4 THEN ISNULL(@monListPrice,0) + ISNULL(SUM(CalculatedPrice),0) ELSE ISNULL(SUM(CalculatedPrice),0) END)
	FROM
	(
		SELECT  
			ISNULL(CASE 
					WHEN I.[charItemType]='P' 
					THEN 
						CASE 
						WHEN ISNULL(I.bitAssembly,0) = 1 AND ISNULL(I.bitCalAmtBasedonDepItems,0) = 1
						THEN
							ISNULL(dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,WI.numWareHouseItemID,I.tintKitAssemblyPriceBasedOn,0,0,''),0)
						ELSE
							(CASE @tintKitAssemblyPriceBasedOn 
									WHEN 2 THEN ISNULL(I.monAverageCost,0) 
									WHEN 3 THEN ISNULL(monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit)
									ELSE WI.[monWListPrice]
							END) 
						END
					ELSE 
						(CASE @tintKitAssemblyPriceBasedOn 
								WHEN 2 THEN ISNULL(I.monAverageCost,0)
								WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit)
								ELSE I.[monListPrice] 
						END)
				END,0) * ID.[numQtyItemsReq] AS CalculatedPrice
		FROM 
			@TEMPitems ID
		INNER JOIN 
			[Item] I 
		ON 
			ID.numItemCode = I.[numItemCode]
		LEFT JOIN
			Vendor V
		ON
			I.numVendorID = V.numVendorID
			AND I.numItemCode = V.numItemCode
		OUTER APPLY
		(
			SELECT TOP 1 
				*
			FROM
				WareHouseItems WI
			WHERE 
				WI.numItemID=I.numItemCode 
				AND WI.numWareHouseID = @numWarehouseID
			ORDER BY
				WI.numWareHouseItemID
		) AS WI
	) T1


	RETURN @monCalculatedPrice
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetCartItemInclusionDetails')
DROP FUNCTION GetCartItemInclusionDetails
GO
CREATE FUNCTION [dbo].[GetCartItemInclusionDetails]
(
	@numCartID NUMERIC(18,0)
)    
RETURNS VARCHAR(MAX)    
AS    
BEGIN   
	DECLARE @vcInclusionDetails AS VARCHAR(MAX)

	DECLARE @TEMPTABLE TABLE
	(
		ID INT IDENTITY(1,1),
		numCartID NUMERIC(18,0),
		vcChildKitItemSelection VARCHAR(MAX),
		numItemCode NUMERIC(18,0),
		bitKitParent BIT,
		numParentItemCode NUMERIC(18,0),
		numTotalQty NUMERIC(18,2),
		numUOMID NUMERIC(18,0),
		tintLevel TINYINT,
		vcInclusionDetail VARCHAR(1000)
	);

	--FIRST LEVEL CHILD
	INSERT INTO @TEMPTABLE
	(
		numCartID,vcChildKitItemSelection,numItemCode,bitKitParent,numParentItemCode,numTotalQty,numUOMID,tintLevel,vcInclusionDetail
	)
	SELECT 
		CI.numCartId,
		CI.vcChildKitItemSelection,
		ID.numChildItemID,
		ISNULL(I.bitKitParent,0),
		0,
		CAST((CI.numUnitHour * ID.numQtyItemsReq) AS FLOAT),
		ISNULL(ID.numUOMID,0),
		1,
		CASE 
			WHEN ISNULL(I.bitKitParent,0)=1
			THEN
				CONCAT('<li><b>',I.vcItemName, ':</b><br/> ') 
			ELSE 
				CONCAT('<li>',I.vcItemName, ' (', CAST((CI.numUnitHour * ID.numQtyItemsReq) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')</li>')
		END
	FROM 
		CartItems CI
	INNER JOIN
		ItemDetails ID
	ON
		CI.numItemCode = ID.numItemKitID
	INNER JOIN
		Item I
	ON
		ID.numChildItemID = I.numItemCode
	LEFT JOIN
		UOM
	ON
		UOM.numUOMId = (CASE WHEN ISNULL(ID.numUOMID,0) > 0 THEN ID.numUOMID ELSE I.numBaseUnit END)
	WHERE
		CI.numCartId = @numCartID
	
	
	INSERT INTO @TEMPTABLE
	(
		numCartID,vcChildKitItemSelection,numItemCode,numParentItemCode,numTotalQty,numUOMID,tintLevel,vcInclusionDetail
	)
	SELECT 
		c.numCartId,
		c.vcChildKitItemSelection,
		ID.numChildItemID,
		c.numItemCode,
		CAST((c.numTotalQty * ID.numQtyItemsReq) AS FLOAT),
		ISNULL(ID.numUOMID,0),
		2,
		CONCAT('<li>',I.vcItemName, ' (', CAST((c.numTotalQty * ID.numQtyItemsReq) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')</li>')
	FROM 
		@TEMPTABLE c
	CROSS APPLY
	(
		SELECT
			SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID
			,SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
		FROM 
			dbo.Split(c.vcChildKitItemSelection,',') 
	) AS t2
	INNER JOIN
		ItemDetails ID
	ON
		ID.numItemKitID = t2.numKitItemID
		AND ID.numChildItemID = t2.numKitChildItemID
		AND c.numItemCode = t2.numKitItemID
	INNER JOIN
		Item I
	ON
		t2.numKitChildItemID = I.numItemCode
	LEFT JOIN
		UOM
	ON
		UOM.numUOMId = (CASE WHEN ISNULL(ID.numUOMID,0) > 0 THEN ID.numUOMID ELSE I.numBaseUnit END)
	WHERE
		LEN(ISNULL(c.vcChildKitItemSelection,'')) > 0

	DECLARE @TEMPTABLEFINAL TABLE
	(
		ID INT IDENTITY(1,1),
		numCartID NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		bitKitParent BIT,
		numParentItemCode NUMERIC(18,0),
		numTotalQty NUMERIC(18,2),
		numUOMID NUMERIC(18,0),
		tintLevel TINYINT,
		vcInclusionDetail VARCHAR(MAX)
	);

	INSERT INTO @TEMPTABLEFINAL
	(
		numCartID,
		numItemCode,
		bitKitParent,
		numParentItemCode,
		numTotalQty,
		numUOMID,
		tintLevel,
		vcInclusionDetail
	)
	SELECT
		numCartID,
		numItemCode,
		bitKitParent,
		numParentItemCode,
		numTotalQty,
		numUOMID,
		tintLevel,
		vcInclusionDetail
	FROM 
		@TEMPTABLE 
	WHERE 
		tintLevel=1
		
	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT

	SELECT @iCount = COUNT(*) FROM @TEMPTABLEFINAL WHERE tintLevel=1

	WHILE @i <= @iCount
	BEGIN
		IF (ISNULL((SELECT bitKitParent FROM @TEMPTABLEFINAL WHERE ID=@i),0) = 0 OR (ISNULL((SELECT bitKitParent FROM @TEMPTABLEFINAL WHERE ID=@i),0) = 1 AND (SELECT COUNT(*) FROM @TEMPTABLE WHERE tintLevel=2 AND numCartID=(SELECT numCartID FROM @TEMPTABLEFINAL WHERE ID=@i) AND numParentItemCode =(SELECT numItemCode FROM @TEMPTABLEFINAL WHERE ID=@i)) > 0))
		BEGIN
			SET @vcInclusionDetails = CONCAT(@vcInclusionDetails,(SELECT vcInclusionDetail FROM @TEMPTABLEFINAL WHERE ID=@i))
		END

		IF (SELECT COUNT(*) FROM @TEMPTABLE WHERE tintLevel=2 AND numCartID=(SELECT numCartID FROM @TEMPTABLEFINAL WHERE ID=@i) AND numParentItemCode =(SELECT numItemCode FROM @TEMPTABLEFINAL WHERE ID=@i)) > 0
		BEGIN
			SET @vcInclusionDetails = CONCAT(@vcInclusionDetails,'<ul class="list-unstyled">')

			SELECT @vcInclusionDetails = COALESCE(@vcInclusionDetails,'') + vcInclusionDetail FROM @TEMPTABLE WHERE tintLevel=2 AND numCartID=(SELECT numCartID FROM @TEMPTABLEFINAL WHERE ID=@i) AND numParentItemCode =(SELECT numItemCode FROM @TEMPTABLEFINAL WHERE ID=@i)

			SET @vcInclusionDetails = CONCAT(@vcInclusionDetails,'</ul></li>')
		END

		SET @i = @i + 1
	END

	SET @vcInclusionDetails = CONCAT('<ul style="text-align:left;margin:0px;padding:0px 0px 0px 14px;">',@vcInclusionDetails,'</ul>')
	
	RETURN @vcInclusionDetails
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetWorkOrderQtyReadyToBuild')
DROP FUNCTION GetWorkOrderQtyReadyToBuild
GO
CREATE FUNCTION [dbo].[GetWorkOrderQtyReadyToBuild] 
(
	@numWOID NUMERIC(18,0)
	,@numQty FLOAT
)
RETURNS FLOAT
AS 
BEGIN
	DECLARE @fltQtyReadyToBuild FLOAT = @numQty

	IF EXISTS (SELECT numWOId FROM WorkOrder WHERE numWOId = @numWOID AND numWOStatus <> 23184)
	BEGIN
		DECLARE @TEMP TABLE
		(
			ItemLevel INT
			,numParentWOID NUMERIC(18,0)
			,numWOID NUMERIC(18,0)
			,numOppID NUMERIC(18,0)
			,numOppItemID NUMERIC(18,0)
			,numOppChildItemID NUMERIC(18,0)
			,numOppKitChildItemID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,vcItemName VARCHAR(MAX)
			,numQty FLOAT
			,numQtyReq_Orig FLOAT
			,numWarehouseItemID NUMERIC(18,0)
			,bitWorkOrder BIT
			,tintBuildStatus BIT
			,numQtyReadyToBuild FLOAT
		)

		DECLARE @TEMPWO TABLE
		(
			ID INT IDENTITY(1,1)
			,numWOID NUMERIC(18,0)
		)

		;WITH CTEWorkOrder (ItemLevel,numParentWOID,numWOID,numOppID,numOppItemID,numOppChildItemID,numOppKitChildItemID,numItemKitID,numQtyItemsReq,numWarehouseItemID,bitWorkOrder,tintBuildStatus,numQtyReadyToBuild) AS
		(
			SELECT
				1,
				numParentWOID,
				numWOId,
				numOppID,
				numOppItemID,
				numOppChildItemID,
				numOppKitChildItemID,
				numItemCode,
				@numQty,
				numWareHouseItemId,
				1 AS bitWorkOrder,
				(CASE WHEN numWOStatus = 23184 THEN 2 ELSE 0 END) AS tintBuildStatus,
				(CASE WHEN numWOStatus = 23184 THEN CAST(numQtyItemsReq AS FLOAT) ELSE CAST(0 AS FLOAT) END)
			FROM
				WorkOrder
			WHERE
				numWOId = @numWOID
			UNION ALL
			SELECT 
				c.ItemLevel+2,
				c.numWOID,
				WorkOrder.numWOId,
				WorkOrder.numOppID,
				WorkOrder.numOppItemID,
				WorkOrder.numOppChildItemID,
				WorkOrder.numOppKitChildItemID,
				WorkOrder.numItemCode,
				CAST(WorkOrder.numQtyItemsReq AS FLOAT),
				WorkOrder.numWareHouseItemId,
				1 AS bitWorkOrder,
				(CASE WHEN numWOStatus = 23184 THEN 2 ELSE 0 END) AS tintBuildStatus,
				(CASE WHEN numWOStatus = 23184 THEN CAST(WorkOrder.numQtyItemsReq AS FLOAT) ELSE CAST(0 AS FLOAT) END)
			FROM 
				WorkOrder
			INNER JOIN 
				CTEWorkOrder c 
			ON 
				WorkOrder.numParentWOID = c.numWOID
		)

		INSERT 
			@TEMP
		SELECT
			ItemLevel,
			numParentWOID,
			numWOID,
			numOppID,
			numOppItemID,
			numOppChildItemID,
			numOppKitChildItemID,
			numItemCode,
			vcItemName,
			CTEWorkOrder.numQtyItemsReq,
			0,
			CTEWorkOrder.numWarehouseItemID,
			bitWorkOrder,
			tintBuildStatus,
			0
		FROM 
			CTEWorkOrder
		INNER JOIN 
			Item
		ON
			CTEWorkOrder.numItemKitID = Item.numItemCode
		LEFT JOIN
			WareHouseItems
		ON
			CTEWorkOrder.numWarehouseItemID = WareHouseItems.numWareHouseItemID

		UPDATE
			T1
		SET
			numQtyReq_Orig = (SELECT WorkOrderDetails.numQtyItemsReq_Orig FROM WorkOrderDetails INNER JOIN WorkOrder ON WorkOrderDetails.numWOId=WorkOrder.numWOId WHERE WorkOrder.numWOId = T1.numParentWOID AND WorkOrderDetails.numChildItemID=T1.numItemCode AND WorkOrderDetails.numWareHouseItemId=T1.numWarehouseItemID AND WorkOrder.numOppId=T1.numOppID AND WorkOrder.numOppItemID=T1.numOppItemID AND WorkOrder.numOppChildItemID=T1.numOppChildItemID AND WorkOrder.numOppKitChildItemID=T1.numOppKitChildItemID)
		FROM
			@TEMP T1
		WHERE	
			numParentWOID > 0

		INSERT INTO 
			@TEMP
		SELECT
			t1.ItemLevel + 1,
			t1.numWOID,
			NULL,
			numOppID,
			numOppItemID,
			numOppChildItemID,
			numOppKitChildItemID,
			WorkOrderDetails.numChildItemID,
			Item.vcItemName,
			WorkOrderDetails.numQtyItemsReq,
			WorkOrderDetails.numQtyItemsReq_Orig,
			WorkOrderDetails.numWarehouseItemID,
			0 AS bitWorkOrder,
			(CASE 
				WHEN t1.tintBuildStatus = 2 
				THEN 2
				ELSE
					(CASE 
						WHEN Item.charItemType='P'
						THEN
							CASE 
								WHEN 1=2  
								THEN (CASE WHEN ISNULL(numQtyItemsReq,0) > WareHouseItems.numOnHand THEN 0 ELSE 1 END)
								ELSE (CASE WHEN ISNULL(numQtyItemsReq,0) > WareHouseItems.numAllocation THEN 0 ELSE 1 END) 
							END
						ELSE 1
					END)
			END),
			(CASE 
				WHEN t1.tintBuildStatus = 2 
				THEN WorkOrderDetails.numQtyItemsReq
				ELSE
					(CASE 
						WHEN Item.charItemType='P'
						THEN
							CASE 
								WHEN 1=2  
								THEN (CASE WHEN ISNULL(numQtyItemsReq,0) <= WareHouseItems.numOnHand THEN (WorkOrderDetails.numQtyItemsReq/numQtyItemsReq_Orig) ELSE CAST(ISNULL(numOnHand,0)/numQtyItemsReq_Orig AS INT) END)
								ELSE (CASE WHEN ISNULL(numQtyItemsReq,0) <= WareHouseItems.numAllocation THEN (WorkOrderDetails.numQtyItemsReq/numQtyItemsReq_Orig) ELSE CAST(ISNULL(numAllocation,0)/numQtyItemsReq_Orig AS INT) END) 
							END
						ELSE (WorkOrderDetails.numQtyItemsReq/numQtyItemsReq_Orig)
					END)
			END)
		FROM
			WorkOrderDetails
		INNER JOIN
			@TEMP t1
		ON
			WorkOrderDetails.numWOId = t1.numWOID
		INNER JOIN 
			Item
		ON
			WorkOrderDetails.numChildItemID = Item.numItemCode
			AND 1= (CASE WHEN (SELECT COUNT(*) FROM @TEMP TInner WHERE TInner.bitWorkOrder=1 AND TInner.numParentWOID=t1.numWOID AND TInner.numItemCode=Item.numItemCode) > 0 THEN 0 ELSE 1 END)
		LEFT JOIN
			WareHouseItems
		ON
			WorkOrderDetails.numWarehouseItemID = WareHouseItems.numWareHouseItemID

		DECLARE @i AS INT = 1
		DECLARE @iCount AS INT
		DECLARE @numTempWOID NUMERIC(18,0)
		INSERT INTO @TEMPWO (numWOID) SELECT numWOID FROM @TEMP WHERE bitWorkOrder=1 ORDER BY ItemLevel DESC

		SELECT @iCount=COUNT(*) FROM @TEMPWO

		WHILE @i <= @iCount
		BEGIN
			SELECT @numTempWOID=numWOID FROM @TEMPWO WHERE ID=@i

			UPDATE
				T1
			SET
				numQtyReadyToBuild = (CASE WHEN ISNULL(numQty,0) <= (SELECT MIN(numQtyReadyToBuild) FROM @TEMP TInner WHERE TInner.numParentWOID=T1.numWOID) THEN (numQty/numQtyReq_Orig) ELSE CAST(ISNULL((SELECT MIN(numQtyReadyToBuild) FROM @TEMP TInner WHERE TInner.numParentWOID=T1.numWOID),0)/numQtyReq_Orig AS INT) END) 
			FROM
				@TEMP T1
			WHERE
				numWOID=@numTempWOID
				AND numParentWOID > 0

			SET @i = @i + 1
		END

		SET @fltQtyReadyToBuild = (SELECT MIN(numQtyReadyToBuild) FROM @TEMP WHERE numParentWOID=@numWOID)
	END

	RETURN @fltQtyReadyToBuild
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='IsSalesOrderReadyToFulfillment')
DROP FUNCTION IsSalesOrderReadyToFulfillment
GO
CREATE FUNCTION [dbo].[IsSalesOrderReadyToFulfillment] 
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@tintCommitAllocation TINYINT
    
)
RETURNS BIT
AS
BEGIN
	DECLARE @bitReadyToShip BIT = 1

	IF EXISTS(SELECT tintshipped FROM dbo.OpportunityMaster WHERE numDomainId=@numDomainID 
			  AND numOppId=@numOppID AND ISNULL(tintshipped,0)=1) OR (SELECT 
																		COUNT(*) 
																	FROM 
																	(
																		SELECT
																			OI.numoppitemtCode,
																			ISNULL(OI.numUnitHour,0) AS OrderedQty,
																			ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
																		FROM
																			OpportunityItems OI
																		INNER JOIN
																			Item I
																		ON
																			OI.numItemCode = I.numItemCode
																		OUTER APPLY
																		(
																			SELECT
																				SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
																			FROM
																				OpportunityBizDocs
																			INNER JOIN
																				OpportunityBizDocItems 
																			ON
																				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																			WHERE
																				OpportunityBizDocs.numOppId = @numOppID
																				AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
																				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
																				AND ISNULL(bitFulFilled,0) = 1
																		) AS TempFulFilled
																		WHERE
																			OI.numOppID = @numOppID
																			AND UPPER(I.charItemType) = 'P'
																			AND ISNULL(OI.bitDropShip,0) = 0
																	) X
																	WHERE
																		X.OrderedQty <> X.FulFilledQty) = 0
	BEGIN
		SET @bitReadyToShip=0
	END
	ELSE
	BEGIN
		DECLARE @TEMP TABLE
		(
			numOppID NUMERIC(18,0)
			,numOppItemID NUMERIC(18,0)
			,numOppChildItemID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numWarehosueItemID NUMERIC(18,0)
			,numQtyToShip FLOAT
			,numOnHand FLOAT
			,numAllocation FLOAT
			,bitWorkOrder BIT
			,bitBuildCompleted BIT
		)

		INSERT INTO @TEMP
		(
			numOppID
			,numOppItemID
			,numOppChildItemID
			,numItemCode
			,numWarehosueItemID
			,numQtyToShip
			,numOnHand
			,numAllocation
			,bitWorkOrder
			,bitBuildCompleted
		)
		SELECT
			OI.numOppId
			,OI.numoppitemtCode
			,0
			,OI.numItemCode
			,OI.numWarehouseItmsID
			,ISNULL(numUnitHour,0) - ISNULL(numQtyShipped,0)
			,ISNULL(WI.numOnHand,0)
			,ISNULL(WI.numAllocation,0)
			,ISNULL(OI.bitWorkOrder,0)
			,(CASE WHEN WO.numWOStatus=23184 THEN 1 ELSE 0 END)
		FROM
			OpportunityItems OI
		INNER JOIN
			WareHouseItems WI
		ON
			OI.numWarehouseItmsID=WI.numWareHouseItemID
		LEFT JOIN
			WorkOrder WO
		ON
			OI.numOppId = WO.numOppId
			AND OI.numoppitemtCode = WO.numOppItemID
			AND ISNULL(numOppChildItemID,0) = 0
			AND ISNULL(numOppKitChildItemID,0) = 0
		WHERE
			OI.numOppId=@numOppID
			AND ISNULL(OI.bitDropShip,0) = 0
		
		INSERT INTO @TEMP
		(
			numOppID
			,numOppItemID
			,numOppChildItemID
			,numItemCode
			,numWarehosueItemID
			,numQtyToShip
			,numOnHand
			,numAllocation
			,bitWorkOrder
			,bitBuildCompleted
		)
		SELECT
			OKI.numOppId
			,OKI.numOppItemID
			,OKI.numOppChildItemID
			,OKI.numChildItemID
			,OKI.numWareHouseItemId
			,OKI.numQtyItemsReq_Orig * (ISNULL(OI.numUnitHour,0) - ISNULL(OI.numQtyShipped,0))
			,ISNULL(WI.numOnHand,0)
			,ISNULL(WI.numAllocation,0)
			,(CASE WHEN ISNULL(WO.numWOId,0) > 0 THEN 1 ELSE 0 END) 
			,(CASE WHEN WO.numWOStatus=23184 THEN 1 ELSE 0 END)
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityKitItems OKI
		ON
			OI.numOppId = OKI.numOppId
			AND OI.numoppitemtCode = OKI.numOppItemID
		INNER JOIN
			WareHouseItems WI
		ON
			OKI.numWareHouseItemId=WI.numWareHouseItemID
		LEFT JOIN
			WorkOrder WO
		ON
			OKI.numOppId = WO.numOppId
			AND OKI.numOppItemID = WO.numOppItemID
			AND ISNULL(WO.numOppChildItemID,0) = OKI.numOppChildItemID
			AND ISNULL(WO.numOppKitChildItemID,0) = 0
		WHERE
			OI.numOppId=@numOppID
		
		INSERT INTO @TEMP
		(
			numOppID
			,numOppItemID
			,numOppChildItemID
			,numItemCode
			,numWarehosueItemID
			,numQtyToShip
			,numOnHand
			,numAllocation
			,bitWorkOrder
			,bitBuildCompleted
		)
		SELECT
			OKCI.numOppId
			,OKCI.numOppItemID
			,0
			,OKCI.numItemID
			,OKCI.numWareHouseItemId
			,OKCI.numQtyItemsReq_Orig * t1.numQtyToShip
			,ISNULL(WI.numOnHand,0)
			,ISNULL(WI.numAllocation,0)
			,(CASE WHEN ISNULL(WO.numWOId,0) > 0 THEN 1 ELSE 0 END) 
			,(CASE WHEN WO.numWOStatus=23184 THEN 1 ELSE 0 END)
		FROM
			OpportunityKitChildItems OKCI
		INNER JOIN
			@TEMP t1
		ON
			OKCI.numOppChildItemID = t1.numOppChildItemID
		INNER JOIN
			WareHouseItems WI
		ON
			OKCI.numWareHouseItemId=WI.numWareHouseItemID
		LEFT JOIN
			WorkOrder WO
		ON
			OKCI.numOppId = WO.numOppId
			AND OKCI.numOppItemID = WO.numOppItemID
			AND ISNULL(WO.numOppChildItemID,0) = OKCI.numOppChildItemID
			AND ISNULL(WO.numOppKitChildItemID,0) = OKCI.numOppKitChildItemID
		WHERE
			OKCI.numOppId=@numOppID

		IF (SELECT COUNT(*) FROM @TEMP WHERE ISNULL(bitWorkOrder,0)=1 AND ISNULL(bitBuildCompleted,0)=0) > 0
		BEGIN
			SET @bitReadyToShip = 0
		END
		ELSE IF @tintCommitAllocation = 2 
		BEGIN
			IF (SELECT COUNT(*) FROM @TEMP WHERE numQtyToShip > numOnHand) > 0
			BEGIN
				SET @bitReadyToShip = 0
			END
		END
		ELSE IF (SELECT COUNT(*) FROM @TEMP WHERE numQtyToShip > numAllocation) > 0
		BEGIN
			SET @bitReadyToShip = 0
		END
	END

	RETURN @bitReadyToShip
END
GO




/****** Object:  UserDefinedFunction [dbo].[Split]    Script Date: 07/26/2008 18:13:14 ******/
SET ANSI_NULLS OFF
GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='split')
DROP FUNCTION split
GO
CREATE FUNCTION [dbo].[Split] (@String nvarchar(MAX), @Delimiter char(1))
RETURNS @Results TABLE (Items nvarchar(MAX)) 
AS
BEGIN
	IF LEN(ISNULL(@String,'')) > 0
	BEGIN
		DECLARE @INDEX INT
		DECLARE @SLICE nvarchar(MAX)
    
		SELECT @INDEX = 1

		WHILE @INDEX !=0
		BEGIN	
			-- GET THE INDEX OF THE FIRST OCCURENCE OF THE SPLIT CHARACTER
			SELECT @INDEX = CHARINDEX(@Delimiter,@STRING)
			-- NOW PUSH EVERYTHING TO THE LEFT OF IT INTO THE SLICE VARIABLE
			IF @INDEX !=0
        		SELECT @SLICE = LEFT(@STRING,@INDEX - 1)
			ELSE
        		SELECT @SLICE = @STRING
			-- PUT THE ITEM INTO THE RESULTS SET
			INSERT INTO @Results(Items) VALUES(@SLICE)
			-- CHOP THE ITEM REMOVED OFF THE MAIN STRING
			SELECT @STRING = RIGHT(@STRING,LEN(@STRING) - @INDEX)
			-- BREAK OUT IF WE ARE DONE
			IF LEN(@STRING) = 0 BREAK
		END
	END
    RETURN
END
GO
/****** Object:  StoredProcedure [dbo].[usp_BusinessProcessList]    Script Date: 07/26/2008 16:14:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_businessprocesslist')
DROP PROCEDURE usp_businessprocesslist
GO
CREATE PROCEDURE [dbo].[usp_BusinessProcessList] 
@numDomainId as numeric(9),
@Mode as int
as 
IF(@Mode=-1)
BEGIN
select Slp_Id,Slp_Name from Sales_process_List_Master where numDomainID= @numDomainId order by slp_name
END
ELSE
BEGIN
select Slp_Id,Slp_Name from Sales_process_List_Master where Pro_Type=@Mode and numDomainID= @numDomainId order by slp_name
END
GO
/****** Object:  StoredProcedure [dbo].[USP_OPPCheckCanBeShipped]    Script Date: 07/26/2008 16:20:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CheckOrderedAndInvoicedOrBilledQty')
DROP PROCEDURE USP_CheckOrderedAndInvoicedOrBilledQty
GO
CREATE PROCEDURE [dbo].[USP_CheckOrderedAndInvoicedOrBilledQty]              
@numOppID AS NUMERIC(18,0)
AS             
BEGIN
	DECLARE @TMEP TABLE
	(
		ID INT,
		vcDescription VARCHAR(100),
		bitTrue BIT
	)

	INSERT INTO 
		@TMEP
	VALUES
		(1,'Fulfillment bizdoc is added but not yet fulfilled',0),
		(2,'Invoice/Bill and Ordered Qty is not same',0),
		(3,'Ordered & Fulfilled Qty is not same',0),
		(4,'Invoice is not generated against diferred income bizDocs.',0),
		(5,'Qty is not available to add in deferred bizdoc.',0),
		(6,'Ordered & Purchase Fulfilled Qty is not same',0)

	DECLARE @tintOppType AS TINYINT
	DECLARE @tintOppStatus AS TINYINT

	SELECT @tintOppType=tintOppType,@tintOppStatus=tintOppStatus FROM OpportunityMaster WHERe numOppID=@numOppID


	-- CHECK IF FULFILLMENT BIZDOC IS ADDED TO SALES ORDER BUT IT IS NOT FULFILLED FROM SALES FULFILLMENT SCREEN
	IF @tintOppType = 1 AND @tintOppStatus = 1  --SALES ORDER
	BEGIN
		IF (SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppId=@numOppID AND numBizDocId=296 AND ISNULL(bitFulFilled,0) = 0) > 0
		BEGIN
			UPDATE @TMEP SET bitTrue = 1 WHERE ID = 1
		END

		-- CHECK IF FULFILLMENT BIZDOC QTY OF ITEMS IS NOT SAME AS ORDERED QTY
		IF (SELECT 
				COUNT(*) 
			FROM 
			(
				SELECT
					OI.numoppitemtCode,
					ISNULL(OI.numUnitHour,0) AS OrderedQty,
					ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
				FROM
					OpportunityItems OI
				INNER JOIN
					Item I
				ON
					OI.numItemCode = I.numItemCode
				OUTER APPLY
				(
					SELECT
						SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
					FROM
						OpportunityBizDocs
					INNER JOIN
						OpportunityBizDocItems 
					ON
						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
					WHERE
						OpportunityBizDocs.numOppId = @numOppID
						AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
						AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				) AS TempFulFilled
				WHERE
					OI.numOppID = @numOppID
					AND ISNULL(OI.numUnitHour,0) > 0
					AND UPPER(I.charItemType) = 'P'
					AND ISNULL(OI.bitDropShip,0) = 0
			) X
			WHERE
				X.OrderedQty <> X.FulFilledQty) > 0
		BEGIN
			UPDATE @TMEP SET bitTrue = 1 WHERE ID = 3
		END

		-- CHECK WHETHER INVOICES ARE GENERATED AGAINST DIFERRED INCOME BIZDOC
		IF (SELECT 
				COUNT(*)
			FROM  
			(
				SELECT 
					OBDI.numOppItemID,
					(ISNULL(SUM(OBDI.numUnitHour),0) - ISNULL(SUM(TEMPInvoiceAgainstDeferred.numUnitHour),0)) AS intQtyRemaining
				FROM
					OpportunityBizDocs OBD
				INNER JOIN
					OpportunityBizDocItems OBDI
				ON
					OBD.numOppBizDocsId = OBDI.numOppBizDocID
				CROSS APPLY
				(
					SELECT 
						SUM(OpportunityBizDocItems.numUnitHour) AS numUnitHour
					FROM
						OpportunityBizDocs
					INNER JOIN
						OpportunityBizDocItems
					ON
						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
					WHERE
						numOppId=@numOppID
						AND ISNULL(numDeferredBizDocID,0) > 0
						AND OpportunityBizDocItems.numOppItemID = OBDI.numOppItemID

				) AS TEMPInvoiceAgainstDeferred
				WHERE
					numOppId=@numOppID 
					AND numBizDocId=304
				GROUP BY
					OBDI.numOppItemID
			) AS TEMP
			WHERE
				intQtyRemaining > 0) > 0
		BEGIN
			UPDATE @TMEP SET bitTrue = 1 WHERE ID = 4
		END

		-- CHECK WHETHER ITEM QTY LEFT TO ADD TO DIFERRED INCOME BIZDOC
		IF (SELECT 
				COUNT(*) 
			FROM 
			(
				SELECT
					OI.numoppitemtCode,
					ISNULL(OI.numUnitHour,0) AS OrderedQty,
					ISNULL(TempQtyLeftForDifferedIncome.intQty,0) AS intQty
				FROM
					OpportunityItems OI
				INNER JOIN
					Item I
				ON
					OI.numItemCode = I.numItemCode
				OUTER APPLY
				(
					SELECT
						SUM(OpportunityBizDocItems.numUnitHour) AS intQty
					FROM
						OpportunityBizDocs
					INNER JOIN
						OpportunityBizDocItems 
					ON
						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
					WHERE
						OpportunityBizDocs.numOppId = @numOppID
						AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
						AND (ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1 OR OpportunityBizDocs.numBizDocID = 304)
				) AS TempQtyLeftForDifferedIncome
				WHERE
					OI.numOppID = @numOppID
			) X
			WHERE
				X.OrderedQty <> X.intQty) = 0
		BEGIN
			UPDATE @TMEP SET bitTrue = 1 WHERE ID = 5
		END
	END
	ELSE IF @tintOppType = 2 AND @tintOppStatus = 1  --SALES ORDER
	BEGIN
		IF (SELECT 
				COUNT(*) 
			FROM 
			(
				SELECT
					OI.numoppitemtCode,
					ISNULL(OI.numUnitHour,0) AS OrderedQty,
					ISNULL(OI.numUnitHourReceived,0) AS FulFilledQty
				FROM
					OpportunityItems OI
				WHERE
					OI.numOppID = @numOppID
					AND ISNULL(OI.numUnitHour,0) > 0
					AND ISNULL(OI.bitDropShip,0) = 0
			) X
			WHERE
				X.OrderedQty <> X.FulFilledQty) > 0
		BEGIN
			UPDATE @TMEP SET bitTrue = 1 WHERE ID = 6
		END
	END

	-- IF INVOICED/BILLED QTY OF ITEMS IS NOT SAME AS ORDERED QTY THEN ORDER CAN NOT BE SHIPPED/RECEIVED
	IF (SELECT 
			COUNT(*) 
		FROM 
		(
			SELECT
				OI.numoppitemtCode,
				ISNULL(OI.numUnitHour,0) AS OrderedQty,
				ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
			FROM
				OpportunityItems OI
			INNER JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			OUTER APPLY
			(
				SELECT
					SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
				FROM
					OpportunityBizDocs
				INNER JOIN
					OpportunityBizDocItems 
				ON
					OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
				WHERE
					OpportunityBizDocs.numOppId = @numOppID
					AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
					AND ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
			) AS TempInvoice
			WHERE
				OI.numOppID = @numOppID
				AND ISNULL(OI.numUnitHour,0) > 0
		) X
		WHERE
			X.OrderedQty <> X.InvoicedQty) > 0
	BEGIN
		UPDATE @TMEP SET bitTrue = 1 WHERE ID = 2
	END

	SELECT * FROM @TMEP

	--GET FULFILLMENT BIZDOCS WHICH ARE NOT FULFILLED YET
	SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE OpportunityBizDocs.numOppId = @numOppID AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296 AND ISNULL(OpportunityBizDocs.bitFulFilled,0) = 0
END

/****** Object:  StoredProcedure [dbo].[usp_DeleteStage]    Script Date: 07/26/2008 16:15:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletestage')
DROP PROCEDURE usp_deletestage
GO
CREATE PROCEDURE [dbo].[usp_DeleteStage]
@numDomainId as numeric(9),
@strStageDetailsIds as varchar(500)
as

delete from stagepercentagedetails where  
	numStageDetailsId in (select Items from dbo.split(@strStageDetailsIds,',') WHERE Items<>'')
and numDomainId=@numDomainId
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DiscountCodes_Validate')
DROP PROCEDURE USP_DiscountCodes_Validate
GO
CREATE PROCEDURE [dbo].[USP_DiscountCodes_Validate]
	@numDomainID NUMERIC(18,0)
	,@numSiteID NUMERIC(18,0)
	,@numDivisionId NUMERIC(18,0)
	,@txtCouponCode VARCHAR(100)
AS
BEGIN
	DECLARE @numRelationship NUMERIC(18,0) = 0
	DECLARE @numProfile NUMERIC(18,0) = 0

	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT 
			@numRelationship=ISNULL(numCompanyType,0)
			,@numProfile=ISNULL(vcProfile,0) 
		FROM 
			DivisionMaster 
		INNER JOIN 
			CompanyInfo 
		ON 
			DivisionMaster.numCompanyID=CompanyInfo.numCompanyId 
		WHERE 
			numDivisionID=@numDivisionID
	END
	ELSE IF ISNULL(@numSiteID,0) > 0
	BEGIN
		SELECT
			@numRelationship=ISNULL(numRelationshipId,0)
			,@numProfile=ISNULL(numProfileId,0)
		FROM
			eCommerceDTL
		WHERE
			numSiteID=@numSiteID
	END

	DECLARE @numPromotionID NUMERIC(18,0)
	DECLARE @bitPromotionNeverExpires BIT
	DECLARE @dtPromotionStartDate DATETIME
	DECLARE @dtPromotionEndDate DATETIME
	DECLARE @intUsageLimit INT
	DECLARE @numDisocuntID NUMERIC(18,0)

	-- FIRST CHECK WHETHER COUNPON CODE IS AVAILABLE
	SELECT 
		@numPromotionID=PO.numProId
		,@intUsageLimit=ISNULL(DC.CodeUsageLimit,0)
		,@bitPromotionNeverExpires=ISNULL(bitNeverExpires,0)
		,@dtPromotionStartDate=PO.dtValidFrom
		,@dtPromotionEndDate=PO.dtValidTo
		,@numDisocuntID=numDiscountId
	FROM 
		PromotionOffer PO
	INNER JOIN 
		PromotionOfferOrganizations PORG
	ON 
		PO.numProId = PORG.numProId
	INNER JOIN 
		DiscountCodes DC
	ON 
		PO.numProId = DC.numPromotionID
	WHERE
		PO.numDomainId = @numDomainID
		AND numRelationship=@numRelationship 
		AND numProfile=@numProfile
		AND DC.vcDiscountCode = @txtCouponCode

	IF ISNULL(@numPromotionID,0) > 0
	BEGIN
		-- NOW CHECK IF PROMOTION EXPIRES OR NOT
		IF ISNULL(@bitPromotionNeverExpires,0) = 0 AND (@dtPromotionStartDate <= GETUTCDATE() AND @dtPromotionEndDate >= GETUTCDATE())
		BEGIN
			IF ISNULL(@intUsageLimit,0) <> 0
			BEGIN
				IF ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE numDiscountId=@numDisocuntID AND numDivisionId=@numDivisionId),0) >= @intUsageLimit
				BEGIN
					RAISERROR('COUPON_USAGE_LIMIT_EXCEEDED',16,1)
					RETURN
				END
			END
		END
		ELSE
		BEGIN
			RAISERROR('COUPON_CODE_EXPIRED',16,1)
			RETURN
		END
	END
	ELSE
	BEGIN
		RAISERROR('INVALID_COUPON_CODE',16,1)
		RETURN
	END

	SELECT 
		@numPromotionID AS numPromotionID
		,@numDisocuntID AS numDisocuntID
		,@txtCouponCode AS vcCouponCode
END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetBizDocTemplate' ) 
    DROP PROCEDURE USP_GetBizDocTemplate
GO
-- USP_GetBizDocTemplate 72,0,0
-- =============================================  
-- Modified by: <Author,,Sachin Sadhu>  
-- Create date: <Create Date,,24thDec2013>  
-- Description: <Description,,add 3 fields:vcBizDocImagePath,vcBizDocFooter,vcPurBizDocFooter>  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_GetBizDocTemplate]
    @numDomainID NUMERIC ,
    @tintTemplateType TINYINT ,
    @numBizDocTempID NUMERIC
AS 
BEGIN
    SET NOCOUNT ON    
    
    SELECT  
		BizDocTemplate.numDomainID,
        [numBizDocID] ,
        [txtBizDocTemplate] ,
        [txtCSS] ,
        ISNULL(bitEnabled, 0) AS bitEnabled ,
        [numOppType] ,
        ISNULL(vcTemplateName, '') AS vcTemplateName ,
        ISNULL(bitDefault, 0) AS bitDefault ,
        BizDocTemplate.vcBizDocFooter ,
        BizDocTemplate.vcPurBizDocFooter,
		numOrientation,
		bitKeepFooterBottom,
		ISNULL(numRelationship,0) AS numRelationship,
		ISNULL(numProfile,0) AS numProfile,
		ISNULL(bitDisplayKitChild,0) AS bitDisplayKitChild,
		ISNULL(numAccountClass,0) AS numAccountClass,
		ISNULL(BizDocTemplate.vcBizDocImagePath,Domain.vcBizDocImagePath) As vcBizDocImagePath
    FROM
		BizDocTemplate
	INNER JOIN
		Domain
	ON
		BizDocTemplate.numDOmainID=Domain.numDomainID
    WHERE 
		BizDocTemplate.numDomainID = @numDomainID
        AND tintTemplateType = @tintTemplateType
        AND (numBizDocTempID = @numBizDocTempID OR (@numBizDocTempID = 0 AND @tintTemplateType != 0))
END
GO
/****** Object:  StoredProcedure [dbo].[usp_BusinessProcessList]    Script Date: 07/26/2008 16:14:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_GetBusinessProcessListConfiguration')
DROP PROCEDURE usp_GetBusinessProcessListConfiguration
GO
CREATE PROCEDURE [dbo].[usp_GetBusinessProcessListConfiguration] 
@numDomainId as numeric(9),
@Mode as tinyint,
@numSlpId as numeric(9)
as 

select * from Sales_process_List_Master 
where Pro_Type=@Mode and numDomainID= @numDomainId and slp_id=@numSlpId
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
         
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetEstimateShipping')
DROP PROCEDURE USP_GetEstimateShipping
GO
CREATE PROCEDURE [dbo].[USP_GetEstimateShipping]
	@numDomainID NUMERIC(18,0)
AS 
BEGIN
	SELECT 
		numServiceTypeID
		,0 AS numRuleID
		,numShippingCompanyID
		,vcServiceName
		,monRate
		,intNsoftEnum
		,intFrom
		,intTo
		,fltMarkup
		,bitMarkUpType
		,bitEnabled
		,1 AS tintFixedShippingQuotesMode
	FROM 
		ShippingServiceTypes 
	WHERE 
		numDomainID=@numDomainID 
		AND ISNULL(bitEnabled,0)=1
END      




SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
         
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetEstimateShippingEcommerce')
DROP PROCEDURE USP_GetEstimateShippingEcommerce
GO
CREATE PROCEDURE [dbo].[USP_GetEstimateShippingEcommerce]
	@numDomainID NUMERIC(18,0)
AS 
BEGIN
	IF @numDomainID IN (187,214)
	BEGIN
		SELECT
			*
		FROM
		(
			SELECT 
				intNsoftEnum AS numServiceTypeID
				,0 AS numRuleID
				,numShippingCompanyID
				,vcServiceName
				,monRate
				,intNsoftEnum
				,intFrom
				,intTo
				,fltMarkup
				,bitMarkUpType
				,bitEnabled
			FROM 
				ShippingServiceTypes 
			WHERE 
				numDomainID=@numDomainID 
				AND ISNULL(bitEnabled,0)=1
			UNION
			SELECT 
				-1 numServiceTypeID
				,0 AS numRuleID
				,92 numShippingCompanyID
				,'Will call' vcServiceName
				,0 monRate
				,0 intNsoftEnum
				,0 intFrom
				,0 intTo
				,0 fltMarkup
				,0 bitMarkUpType
				,1 bitEnabled
		) TEMP
		ORDER BY
			numShippingCompanyID,intNsoftEnum
	END
	ELSE
	BEGIN
		SELECT 
			numServiceTypeID
			,0 AS numRuleID
			,numShippingCompanyID
			,vcServiceName
			,monRate
			,intNsoftEnum
			,intFrom
			,intTo
			,fltMarkup
			,bitMarkUpType
			,bitEnabled
		FROM 
			ShippingServiceTypes 
		WHERE 
			numDomainID=@numDomainID 
			AND ISNULL(bitEnabled,0)=1
	END
END      
GO


--Created by Anoop Jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetFieldRelationship')
DROP PROCEDURE USP_GetFieldRelationship
GO
CREATE PROCEDURE USP_GetFieldRelationship
@byteMode as tinyint,
@numFieldRelID as numeric(9),
@numDomainID as numeric(9),
@numPrimaryListItemID as numeric(9),
@numSecondaryListID as numeric(9),
@bitTaskRelation AS BIT
as
	if @byteMode=1
	begin
	
	IF(@bitTaskRelation=1)
	BEGIN
		SELECT 
			FR.numFieldRelID,
			FR.numPrimaryListID,
			SD.vcStageName as RelationshipName
		FROM
			FieldRelationship AS FR
		LEFT JOIN
			StagePercentageDetails AS SD
		ON
			FR.numPrimaryListID=SD.numStageDetailsId
		WHERE
			FR.numDomainId=@numDomainID AND
			ISNULL(FR.bitTaskRelation,0)=1
	END
	ELSE
	BEGIN
		Select numFieldRelID,L1.vcListName +' - ' + L2.vcListName as RelationshipName from FieldRelationship 
		Join ListMaster L1
		on L1.numListID=numPrimaryListID
		Join ListMaster L2
		on L2.numListID=numSecondaryListID
		where FieldRelationship.numDomainID=@numDomainID AND ISNULL(bitTaskRelation,0)=0
	END
	end
	else if @byteMode=2
	begin

	Select numFieldRelID,numPrimaryListID,numSecondaryListID,L1.vcListName +' - ' + L2.vcListName as RelationshipName,
	L1.vcListName as Item1,  L2.vcListName as Item2 from FieldRelationship 
	Join ListMaster L1
	on L1.numListID=numPrimaryListID
	Join ListMaster L2
	on L2.numListID=numSecondaryListID
	where  numFieldRelID=@numFieldRelID AND ISNULL(bitTaskRelation,0)=0

	if @numPrimaryListItemID >0  AND @bitTaskRelation=1
	begin
		Select FieldRelationship.numFieldRelID,numFieldRelDTLID,SLI1.vcTaskName as PrimaryListItem,SLI2.vcTaskName as SecondaryListItem   from FieldRelationship
		Join FieldRelationshipDTL 
		on FieldRelationshipDTL.numFieldRelID=FieldRelationship.numFieldRelID
		Join StagePercentageDetailsTask SLI1
		on SLI1.numTaskId=numPrimaryListItemID
		Join StagePercentageDetailsTask SLI2
		on SLI2.numTaskId=numSecondaryListItemID
		where FieldRelationship.numFieldRelID=@numFieldRelID and numPrimaryListItemID=@numPrimaryListItemID AND ISNULL(bitTaskRelation,0)=1
	end
	ELSE if @numPrimaryListItemID >0 
	begin
		Select FieldRelationship.numFieldRelID,numFieldRelDTLID,L1.vcData as PrimaryListItem,L2.vcData as SecondaryListItem   from FieldRelationship
		Join FieldRelationshipDTL 
		on FieldRelationshipDTL.numFieldRelID=FieldRelationship.numFieldRelID
		Join ListDetails L1
		on L1.numListItemID=numPrimaryListItemID
		Join ListDetails L2
		on L2.numListItemID=numSecondaryListItemID
		where FieldRelationship.numFieldRelID=@numFieldRelID and numPrimaryListItemID=@numPrimaryListItemID AND ISNULL(bitTaskRelation,0)=0
	end

	end
	else if @byteMode=3
	begin


		Select L2.numListItemID,L2.vcData as SecondaryListItem   from FieldRelationship
		Join FieldRelationshipDTL 
		on FieldRelationshipDTL.numFieldRelID=FieldRelationship.numFieldRelID
		Join ListDetails L2
		on L2.numListItemID=numSecondaryListItemID
		where FieldRelationship.numDomainID=@numDomainID and numPrimaryListItemID=@numPrimaryListItemID
		and FieldRelationship.numSecondaryListID=@numSecondaryListID AND ISNULL(bitTaskRelation,0)=0
	end

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetKitChildsFirstLevel')
DROP PROCEDURE USP_GetKitChildsFirstLevel
GO
CREATE PROCEDURE [dbo].[USP_GetKitChildsFirstLevel]        
	@numDomainID as numeric(9)=0,
	@strItem as varchar(100),
	@numItemCode AS NUMERIC(18,0) = 0,
	@numPageIndex AS INT,
	@numPageSize AS INT,     
	@TotalCount AS INT OUTPUT
AS
BEGIN
	DECLARE @strSQL AS NVARCHAR(MAX) = ''
	DECLARE @vcWareHouseSearch VARCHAR(1000) = ''
	DECLARE @vcVendorSearch VARCHAR(1000) = ''
	DECLARE @TEMPitems TABLE 
	( 
		numItemCode NUMERIC(18,0)
	)
	DECLARE @tintDecimalPoints AS TINYINT

	SELECT  
		@tintDecimalPoints=ISNULL(tintDecimalPoints,4)
	FROM    
		domain
	WHERE  
		numDomainID = @numDomainID
               
	SELECT 
		*
	INTO 
		#Temp1
	FROM
	( 
		SELECT    
			numFieldId ,
			vcDbColumnName ,
			ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
			tintRow AS tintOrder ,
			0 AS Custom
		FROM 
			View_DynamicColumns
		WHERE 
			numFormId = 22
			AND numDomainID = @numDomainID
			AND tintPageType = 1
			AND bitCustom = 0
			AND ISNULL(bitSettingField, 0) = 1
			AND numRelCntType = 0
		UNION
		SELECT 
			numFieldId ,
			vcFieldName ,
			vcFieldName ,
			tintRow AS tintOrder ,
			1 AS Custom
		FROM
			View_DynamicCustomColumns
		WHERE
			Grp_id = 5
			AND numFormId = 22
			AND numDomainID = @numDomainID
			AND tintPageType = 1
			AND bitCustom = 1
			AND numRelCntType = 0
	) X 
  
	IF NOT EXISTS (SELECT * FROM #Temp1)
	BEGIN
		INSERT INTO 
			#Temp1
		SELECT 
			numFieldId
			,vcDbColumnName
			,ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName
			,tintorder
			,0
		FROM
			View_DynamicDefaultColumns
		WHERE 
			numFormId = 22
			AND bitDefault = 1
			AND ISNULL(bitSettingField, 0) = 1
			AND numDomainID = @numDomainID 
	END

	DECLARE @tintOrder AS INT
    DECLARE @Fld_id AS NVARCHAR(20)
    DECLARE @Fld_Name AS VARCHAR(20)
        
		

    SELECT TOP 1
        @tintOrder = tintOrder + 1 ,
        @Fld_id = numFieldId ,
        @Fld_Name = vcFieldName
    FROM
		#Temp1
    WHERE
		Custom = 1
    ORDER BY 
		tintOrder

    WHILE @tintOrder > 0
    BEGIN
        SET @strSQL = @strSQL + ', dbo.GetCustFldValueItem('+ @Fld_id + ', I.numItemCode) as [' + @Fld_Name + ']'

        SELECT TOP 1
            @tintOrder = tintOrder + 1 ,
            @Fld_id = numFieldId ,
            @Fld_Name = vcFieldName
        FROM 
			#Temp1
        WHERE 
			Custom = 1
			AND tintOrder >= @tintOrder
        ORDER BY 
			tintOrder
            
		IF @@rowcount = 0
            SET @tintOrder = 0
    END

	--Temp table for Item Search Configuration

    SELECT 
		*
    INTO
		#tempSearch
    FROM
	(
		SELECT
			numFieldId,
            vcDbColumnName ,
            ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
            tintRow AS tintOrder ,
            0 AS Custom
        FROM
			View_DynamicColumns
        WHERE
			numFormId = 22
            AND numDomainID = @numDomainID
            AND tintPageType = 1
            AND bitCustom = 0
            AND ISNULL(bitSettingField, 0) = 1
            AND numRelCntType = 1
        UNION
        SELECT
			numFieldId,
            vcFieldName ,
            vcFieldName ,
            tintRow AS tintOrder ,
            1 AS Custom
		FROM
			View_DynamicCustomColumns
        WHERE
			Grp_id = 5
            AND numFormId = 22
            AND numDomainID = @numDomainID
            AND tintPageType = 1
            AND bitCustom = 1
            AND numRelCntType = 1
    ) X 
  
    IF NOT EXISTS(SELECT * FROM #tempSearch)
    BEGIN
		INSERT INTO 
			#tempSearch
        SELECT 
			numFieldId ,
            vcDbColumnName ,
            ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
            tintorder ,
            0
        FROM 
			View_DynamicDefaultColumns
        WHERE 
			numFormId = 22
            AND bitDefault = 1
            AND ISNULL(bitSettingField, 0) = 1
            AND numDomainID = @numDomainID 
    END

	DECLARE @strSearch AS NVARCHAR(MAX), @CustomSearch AS NVARCHAR(MAX), @numFieldId AS NUMERIC(18,0)
        
	SET @strSearch = ''

    SELECT TOP 1
        @tintOrder = tintOrder + 1 ,
        @Fld_Name = vcDbColumnName ,
        @numFieldId = numFieldId
    FROM
		#tempSearch
    WHERE
		Custom = 0
    ORDER BY
		tintOrder
        
	WHILE @tintOrder > -1
    BEGIN
        IF @Fld_Name = 'vcPartNo'
		BEGIN
            INSERT INTO @TEMPitems
            SELECT DISTINCT
                    Item.numItemCode
            FROM    Vendor
                    JOIN Item ON Item.numItemCode = Vendor.numItemCode
            WHERE   Item.numDomainID = @numDomainID
                    AND Vendor.numDomainID = @numDomainID
                    AND Vendor.vcPartNo IS NOT NULL
                    AND LEN(Vendor.vcPartNo) > 0
                    AND vcPartNo LIKE '%' + @strItem + '%'
							
			SET @vcVendorSearch = 'dbo.Vendor.vcPartNo LIKE ''%' + @strItem + '%'''
		END
        ELSE IF @Fld_Name = 'vcBarCode'
		BEGIN
            INSERT INTO @TEMPitems
            SELECT DISTINCT
                    Item.numItemCode
            FROM    Item
                    JOIN WareHouseItems WI ON WI.numDomainID = @numDomainID
                                    AND Item.numItemCode = WI.numItemID
                    JOIN Warehouses W ON W.numDomainID = @numDomainID
                                    AND W.numWareHouseID = WI.numWareHouseID
            WHERE   Item.numDomainID = @numDomainID
                    AND ISNULL(Item.numItemGroup,
                                0) > 0
                    AND WI.vcBarCode IS NOT NULL
                    AND LEN(WI.vcBarCode) > 0
                    AND WI.vcBarCode LIKE '%' + @strItem + '%'

			SET @vcWareHouseSearch = 'dbo.WareHouseItems.vcBarCode LIKE ''%' + @strItem + '%'''
		END
        ELSE IF @Fld_Name = 'vcWHSKU'
		BEGIN
            INSERT INTO @TEMPitems
            SELECT DISTINCT
                    Item.numItemCode
            FROM    Item
                    JOIN WareHouseItems WI ON WI.numDomainID = @numDomainID
                                AND Item.numItemCode = WI.numItemID
                    JOIN Warehouses W ON W.numDomainID = @numDomainID
                                AND W.numWareHouseID = WI.numWareHouseID
            WHERE   Item.numDomainID = @numDomainID
                    AND ISNULL(Item.numItemGroup,
                                0) > 0
                    AND WI.vcWHSKU IS NOT NULL
                    AND LEN(WI.vcWHSKU) > 0
                    AND WI.vcWHSKU LIKE '%'
                    + @strItem + '%'

			IF LEN(@vcWareHouseSearch) > 0
				SET @vcWareHouseSearch = @vcWareHouseSearch + ' OR dbo.WareHouseItems.vcWHSKU LIKE ''%' + @strItem + '%'''
			ELSE
				SET @vcWareHouseSearch = 'dbo.WareHouseItems.vcWHSKU LIKE ''%' + @strItem + '%'''
		END
        ELSE
		BEGIN
            SET @strSearch = @strSearch + (CASE @Fld_Name WHEN 'numItemCode' THEN ' I.[' + @Fld_Name ELSE ' [' + @Fld_Name END)  + '] LIKE ''%' + @strItem + '%'''
		END

        SELECT TOP 1
            @tintOrder = tintOrder + 1 ,
            @Fld_Name = vcDbColumnName ,
            @numFieldId = numFieldId
        FROM
			#tempSearch
        WHERE
			tintOrder >= @tintOrder
			AND Custom = 0
        ORDER BY 
			tintOrder
                        
		IF @@rowcount = 0
        BEGIN
            SET @tintOrder = -1
        END
        ELSE
		BEGIN
            IF @Fld_Name != 'vcPartNo' AND @Fld_Name != 'vcBarCode' AND @Fld_Name != 'vcWHSKU'
            BEGIN
                SET @strSearch = @strSearch + ' or '
            END
		END
    END

    IF (SELECT COUNT(*) FROM @TEMPitems) > 0
	BEGIN
        SET @strSearch = @strSearch + ' OR  I.numItemCode IN (select numItemCode from #tempItemCode)' 
	END

	--Custom Search
    SET @CustomSearch = ''
    SELECT TOP 1
        @tintOrder = tintOrder + 1 ,
        @Fld_Name = vcDbColumnName ,
        @numFieldId = numFieldId
    FROM 
		#tempSearch
    WHERE
		Custom = 1
    ORDER BY 
		tintOrder

    WHILE @tintOrder > -1
    BEGIN
        SET @CustomSearch = @CustomSearch + CAST(@numFieldId AS VARCHAR(10)) 

        SELECT TOP 1
            @tintOrder = tintOrder + 1 ,
            @Fld_Name = vcDbColumnName ,
            @numFieldId = numFieldId
        FROM 
			#tempSearch
        WHERE 
			tintOrder >= @tintOrder
			AND Custom = 1
        ORDER BY 
			tintOrder
            
		IF @@rowcount = 0
        BEGIN
            SET @tintOrder = -1
        END
        ELSE
        BEGIN
            SET @CustomSearch = @CustomSearch + ' , '
        END
    END
	
    IF LEN(@CustomSearch) > 0
    BEGIN
        SET @CustomSearch = ' I.numItemCode in (SELECT RecId FROM dbo.CFW_FLD_Values_Item WHERE Fld_ID IN ('
            + @CustomSearch + ') and dbo.GetCustFldValueItem(Fld_ID,RecId) like ''%'
            + @strItem + '%'')'

        IF LEN(@strSearch) > 0
            SET @strSearch = @strSearch + ' OR ' + @CustomSearch
        ELSE
            SET @strSearch = @CustomSearch  
    END


	SET @strSearch = CONCAT(' AND I.numItemCode IN (SELECT 
														IChild.numItemCode 
													FROM
														ItemDetails ID
													INNER JOIN 
														Item IMain
													ON
														ID.numItemKitID = IMain.numItemCode
													INNER JOIN
														Item IChild
													ON
														ID.numChildItemID = IChild.numItemCode
													WHERE
														ID.numItemKitID = ',@numItemCode,'
														AND ISNULL(IMain.bitKitParent,0) = 1
														AND ISNULL((SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE ItemDetails.numItemKitID = ',@numItemCode,' AND ISNULL(Item.bitKitParent,0) = 1),0) = 0)',(CASE WHEN LEN(ISNULL(@strSearch,'')) > 0 THEN ' AND (' + @strSearch + ')' ELSE '' END))

	DECLARE @strSQLCount NVARCHAR(MAX) = ''

	SET @strSQLCount = 'SET @TotalCount = ISNULL((SELECT 
										COUNT(I.numItemCode) 
									FROM 
										Item  I WITH (NOLOCK)
									LEFT JOIN 
										DivisionMaster D              
									ON 
										I.numVendorID=D.numDivisionID  
									LEFT JOIN 
										CompanyInfo C  
									ON 
										C.numCompanyID=D.numCompanyID
									LEFT JOIN 
										CustomerPartNumber  CPN  
									ON 
										I.numItemCode=CPN.numItemCode 
										AND CPN.numCompanyID=C.numCompanyID
									LEFT JOIN
										dbo.WareHouseItems WHT
									ON
										WHT.numItemID = I.numItemCode AND
										WHT.numWareHouseItemID = (' +
																		CASE LEN(@vcWareHouseSearch)
																		WHEN 0 
																		THEN 
																			'SELECT 
																				TOP 1 numWareHouseItemID 
																			FROM 
																				dbo.WareHouseItems 
																			WHERE 
																				dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																				AND dbo.WareHouseItems.numItemID = I.numItemCode '
																		ELSE
																			'CASE 
																				WHEN (SELECT 
																							COUNT(*) 
																						FROM 
																							dbo.WareHouseItems 
																						WHERE 
																							dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																							AND dbo.WareHouseItems.numItemID = I.numItemCode 
																							AND (' + @vcWareHouseSearch + ')) > 0 
																				THEN
																					(SELECT 
																						TOP 1 numWareHouseItemID 
																					FROM 
																						dbo.WareHouseItems 
																					WHERE 
																						dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																						AND dbo.WareHouseItems.numItemID = I.numItemCode 
																						AND (' + @vcWareHouseSearch + '))
																				ELSE
																					(SELECT 
																						TOP 1 numWareHouseItemID 
																					FROM 
																						dbo.WareHouseItems 
																					WHERE 
																						dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																						AND dbo.WareHouseItems.numItemID = I.numItemCode 
																					)
																				END' 
																		END
																	+
																') 
									WHERE ((I.charItemType <> ''P'') OR ((SELECT COUNT(*) FROM dbo.WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
									+ CONVERT(VARCHAR(20), @numDomainID)
									+ ') > 0)) AND ISNULL(I.Isarchieve,0) <> 1 And I.charItemType NOT IN(''A'') AND  I.numDomainID='
									+ CONVERT(VARCHAR(20), @numDomainID) + @strSearch  
						    

					SET @strSQLCount = @strSQLCount + '),0);'

                    SET @strSQL = 'select I.numItemCode, ISNULL(CPN.CustomerPartNo,'''')  AS CustomerPartNo,
									(SELECT TOP 1 vcPathForImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForImage,
									(SELECT TOP 1 vcPathForTImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForTImage,
									isnull(vcItemName,'''') vcItemName,
									CAST(CAST(CASE 
									WHEN I.[charItemType]=''P''
									THEN ISNULL((SELECT TOP 1 monWListPrice FROM [WareHouseItems] WHERE [numItemID]=I.numItemCode),0) 
									ELSE 
									monListPrice 
									END AS DECIMAL(20,' + CAST(@tintDecimalPoints AS VARCHAR) +')) AS VARCHAR) AS monListPrice,
									isnull(vcSKU,'''') vcSKU,
									isnull(numBarCodeId,0) numBarCodeId,
									isnull(vcModelID,'''') vcModelID,
									isnull(txtItemDesc,'''') as txtItemDesc,
									isnull(C.vcCompanyName,'''') as vcCompanyName,
									D.numDivisionID,
									WHT.numWareHouseItemID,
									dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [vcAttributes],
									(CASE 
										WHEN ISNULL(I.bitKitParent,0) = 1 
										THEN 
											(
												CASE
													WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND ISNULL(Item.bitKitParent,0) = 1) > 0
													THEN 1
													ELSE 0
												END
											)
										ELSE 0 
									END) bitHasKitAsChild
									,ISNULL(I.bitMatrix,0) AS bitMatrix
									,ISNULL(I.numItemGroup,0) AS numItemGroup
									,I.charItemType
									,ISNULL(I.bitKitParent,0) bitKitParent
									,ISNULL(I.bitAssembly,0) bitAssembly '
									+ @strSQL
									+ ' INTO #TEMPItems FROM Item  I WITH (NOLOCK)
									Left join DivisionMaster D              
									on I.numVendorID=D.numDivisionID  
									left join CompanyInfo C  
									on C.numCompanyID=D.numCompanyID
									LEFT JOIN CustomerPartNumber  CPN  
									ON I.numItemCode=CPN.numItemCode AND CPN.numCompanyID=C.numCompanyID
									LEFT JOIN
										dbo.WareHouseItems WHT
									ON
										WHT.numItemID = I.numItemCode AND
										WHT.numWareHouseItemID = (' +
																		CASE LEN(@vcWareHouseSearch)
																		WHEN 0 
																		THEN 
																			'SELECT 
																				TOP 1 numWareHouseItemID 
																			FROM 
																				dbo.WareHouseItems 
																			WHERE 
																				dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																				AND dbo.WareHouseItems.numItemID = I.numItemCode '
																		ELSE
																			'CASE 
																				WHEN (SELECT 
																							COUNT(*) 
																						FROM 
																							dbo.WareHouseItems 
																						WHERE 
																							dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																							AND dbo.WareHouseItems.numItemID = I.numItemCode 
																							AND (' + @vcWareHouseSearch + ')) > 0 
																				THEN
																					(SELECT 
																						TOP 1 numWareHouseItemID 
																					FROM 
																						dbo.WareHouseItems 
																					WHERE 
																						dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																						AND dbo.WareHouseItems.numItemID = I.numItemCode 
																						AND (' + @vcWareHouseSearch + '))
																				ELSE
																					(SELECT 
																						TOP 1 numWareHouseItemID 
																					FROM 
																						dbo.WareHouseItems 
																					WHERE 
																						dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																						AND dbo.WareHouseItems.numItemID = I.numItemCode 
																					)
																				END' 
																		END
																	+
																') 
									where ((I.charItemType <> ''P'') OR ((SELECT COUNT(*) FROM dbo.WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
									+ CONVERT(VARCHAR(20), @numDomainID) + ') > 0)) AND ISNULL(I.Isarchieve,0) <> 1 And I.charItemType NOT IN(''A'') AND  I.numDomainID='
									+ CONVERT(VARCHAR(20), @numDomainID) + @strSearch   
						     
						

                       

	SET @strSQL = @strSQL + CONCAT(' ORDER BY vcItemName OFFSET ', (@numPageIndex - 1) * @numPageSize,' ROWS FETCH NEXT ',@numPageSize,' ROWS ONLY ')


	SET @strSQL = @strSQL + ' SELECT SRNO=ROW_NUMBER() OVER( ORDER BY vcItemName),(CASE WHEN (vcSKU=''' + @strItem + ''' OR ' + CAST(ISNULL(@numDomainID,0) AS VARCHAR) + '=209) THEN 1 ELSE 0 END) bitSKUMatch,* INTO #TEMPItemsFinal FROM #TEMPItems;'

	SET @strSQL = @strSQL + @strSQLCount

	SET @strSQL = @strSQL + ' SELECT * FROM #TEMPItemsFinal ORDER BY SRNO; DROP TABLE #TEMPItemsFinal; DROP TABLE #TEMPItems;'
						
	PRINT @strSQL
	EXEC sp_executeSQL @strSQL, N'@TotalCount INT OUTPUT', @TotalCount OUTPUT

	SELECT  *
	FROM    #Temp1
	WHERE   vcDbColumnName NOT IN ( 'vcPartNo', 'vcBarCode', 'vcWHSKU' )ORDER BY tintOrder 

	IF OBJECT_ID('tempdb..#Temp1') IS NOT NULL
	BEGIN
		DROP TABLE #Temp1
	END

	IF OBJECT_ID('tempdb..#tempSearch') IS NOT NULL
	BEGIN
		DROP TABLE #tempSearch
	END

END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetOppItemsForPickPackShip' ) 
    DROP PROCEDURE USP_GetOppItemsForPickPackShip
GO
CREATE PROCEDURE USP_GetOppItemsForPickPackShip
    @numDomainID AS NUMERIC(18,0),
    @numOppID AS NUMERIC(18,0),
	@numBizDocID AS NUMERIC(18,0)
AS 
BEGIN
	IF @numBizDocID = 29397 -- Packing Slip
	BEGIN
		SELECT    
			OI.numoppitemtCode,
			OI.numUnitHour AS QtyOrdered,
			OI.numUnitHour - ISNULL(SUM(OBIPackingSlip.numUnitHour),0) AS QtytoFulFillOriginal,
			(OI.numUnitHour - (CASE WHEN ISNULL(OBDFulfillment.numUnitHour,0) > 0 THEN ISNULL(OBDFulfillment.numUnitHour,0) ELSE ISNULL(SUM(OBIPackingSlip.numUnitHour),0) END)) AS QtytoFulFill
		FROM 
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		LEFT JOIN
			OpportunityBizDocs OBDPackingSlip
		ON
			OM.numOppId = OBDPackingSlip.numOppId
			AND OBDPackingSlip.numBizDocId = @numBizDocID
		LEFT JOIN
			OpportunityBizDocItems OBIPackingSlip
		ON
			OBDPackingSlip.numOppBizDocsId = OBIPackingSlip.numOppBizDocID
			AND OBIPackingSlip.numOppItemID = OI.numoppitemtCode
		OUTER APPLY
		(
			SELECT
				SUM(numUnitHour) AS numUnitHour
			FROM
				OpportunityBizDocs
			LEFT JOIN
				OpportunityBizDocItems 
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
			WHERE
				OpportunityBizDocs.numOppId = OM.numOppId
				AND OpportunityBizDocs.numBizDocId = 296
		) OBDFulfillment
		WHERE     
			OI.numOppId = @numOppID
		GROUP BY
			OI.numoppitemtCode
			,OI.numUnitHour
			,OBDFulfillment.numUnitHour
	END
	ELSE IF @numBizDocID = 296 -- Fulfillment
	BEGIN
		DECLARE @tintCommitAllocation AS TINYINT
		DECLARE @bitAllocateInventoryOnPickList AS BIT

		SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
		SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM OpportunityMaster INNER JOIN DivisionMaster ON OpportunityMaster.numDivisionId=DivisionMaster.numDivisionID WHERE numOppId=@numOppID

		SELECT    
			OI.numoppitemtCode,
			OI.numUnitHour AS QtyOrdered,
			ISNULL(CASE 
				WHEN @numBizDocId = 296 
				THEN
					(CASE 
						WHEN (I.charItemType = 'p' AND ISNULL(OI.bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
						THEN 
							(CASE 
								WHEN (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) <= dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList) 
								THEN (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) 
								ELSE dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList) 
							END)
						ELSE (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0))
					END)
				ELSE
					(OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0))
			END,0) AS QtytoFulFillOriginal,
			(OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) QtyToFulfillWithoutAllocationCheck,
			ISNULL((CASE 
				WHEN ISNULL(SUM(OBI.numUnitHour),0) >= ISNULL(OBDPackingSlip.numUnitHour,0) 
				THEN (CASE 
						WHEN (I.charItemType = 'p' AND ISNULL(OI.bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
						THEN 
							(CASE 
								WHEN (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) <= dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList) 
								THEN (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) 
								ELSE dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList)
							END)
						ELSE (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0))
					END) 
				ELSE (CASE 
						WHEN (I.charItemType = 'p' AND ISNULL(OI.bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
						THEN 
							(CASE 
								WHEN (ISNULL(OBDPackingSlip.numUnitHour,0) - ISNULL(SUM(OBI.numUnitHour),0)) <= dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList)
								THEN (ISNULL(OBDPackingSlip.numUnitHour,0) - ISNULL(SUM(OBI.numUnitHour),0)) 
								ELSE dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList)
							END)
						ELSE ISNULL(OBDPackingSlip.numUnitHour,0) - ISNULL(SUM(OBI.numUnitHour),0)
					END) 
			END),0) AS QtytoFulFill
		FROM 
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		LEFT JOIN
			OpportunityBizDocs OBD
		ON
			OM.numOppId = OBD.numOppId
			AND OBD.numBizDocId = @numBizDocID
		LEFT JOIN
			OpportunityBizDocItems OBI
		ON
			OBD.numOppBizDocsId = OBI.numOppBizDocID
			AND OBI.numOppItemID = OI.numoppitemtCode
		OUTER APPLY
		(
			SELECT
				SUM(numUnitHour) AS numUnitHour
			FROM
				OpportunityBizDocs
			LEFT JOIN
				OpportunityBizDocItems 
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
			WHERE
				OpportunityBizDocs.numOppId = OM.numOppId
				AND OpportunityBizDocs.numBizDocId = 29397
		) OBDPackingSlip
		WHERE     
			OI.numOppId = @numOppID
		GROUP BY
			OI.numoppitemtCode
			,OI.numUnitHour
			,OI.numWarehouseItmsID
			,OI.bitDropShip
			,I.charItemType
			,I.bitAsset
			,I.bitKitParent
			,OBDPackingSlip.numUnitHour
	END
	ELSE IF @numBizDocID = 287 -- Invoice
	BEGIN
		SELECT    
			OI.numoppitemtCode,
			OI.numUnitHour AS QtyOrdered,
			OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0) AS QtytoFulFillOriginal,
			ISNULL((CASE 
				WHEN ISNULL(SUM(OBI.numUnitHour),0) >= ISNULL(OBDFulfillment.numUnitHour,0) 
				THEN OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0) 
				ELSE ISNULL(OBDFulfillment.numUnitHour,0) - ISNULL(SUM(OBI.numUnitHour),0) 
			END),0) AS QtytoFulFill
		FROM 
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		LEFT JOIN
			OpportunityBizDocs OBD
		ON
			OM.numOppId = OBD.numOppId
			AND OBD.numBizDocId = @numBizDocID
		LEFT JOIN
			OpportunityBizDocItems OBI
		ON
			OBD.numOppBizDocsId = OBI.numOppBizDocID
			AND OBI.numOppItemID = OI.numoppitemtCode
		OUTER APPLY
		(
			SELECT
				SUM(numUnitHour) AS numUnitHour
			FROM
				OpportunityBizDocs
			LEFT JOIN
				OpportunityBizDocItems 
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
			WHERE
				OpportunityBizDocs.numOppId = OM.numOppId
				AND OpportunityBizDocs.numBizDocId = 296
		) OBDFulfillment
		WHERE     
			OI.numOppId = @numOppID
		GROUP BY
			OI.numoppitemtCode
			,OI.numUnitHour
			,OBDFulfillment.numUnitHour
	END
END
GO

GO
/****** Object:  StoredProcedure [dbo].[USP_GetOpportunityList1]    Script Date: 03/06/2009 00:37:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- created by anoop jayaraj                                                               
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getopportunitylist1')
DROP PROCEDURE usp_getopportunitylist1
GO
CREATE PROCEDURE [dbo].[USP_GetOpportunityList1]                                                                    
	@numUserCntID numeric(9)=0,
	@numDomainID numeric(9)=0,
	@tintUserRightType tinyint=0,
	@tintSortOrder tinyint=4,
	@dtLastDate datetime,
	@OppType as tinyint,
	@CurrentPage int,
	@PageSize int,
	@TotRecs int output,
	@columnName as Varchar(50),
	@columnSortOrder as Varchar(10),
	@numDivisionID as numeric(9)=0,
	@bitPartner as bit=0,
	@ClientTimeZoneOffset as int,
	@inttype as tinyint,
	@vcRegularSearchCriteria varchar(1000)='',
	@vcCustomSearchCriteria varchar(1000)='',
	@SearchText VARCHAR(300) = '',
	@SortChar char(1)='0',
	@tintDashboardReminderType TINYINT = 0                                                              
AS                
BEGIN
         
	DECLARE @PageId as TINYINT = 0
	DECLARE @numFormId  AS INT
           
	IF @inttype = 1 
	BEGIN           
		SET @PageId =2  
		SET @numFormId=38          
	END
	ELSE IF @inttype = 2            
	BEGIN
		SET @PageId =6 
		SET @numFormId=40           
	END

	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),
		intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE @Nocolumns AS TINYINT = 0  

	SELECT 
		@Nocolumns = ISNULL(SUM(TotalRow),0)  
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
	) TotalRows
	
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	-- IF NoColumns=0 CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormId AND numRelCntType=@numFormId AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,0,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN 
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				@numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=@numFormId 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder ASC  
		END
  

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,
			bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = @numFormId
		UNION
		SELECT 
			tintRow+1 AS tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,0,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = @numFormId
		ORDER BY 
			tintOrder ASC             
	END
	
	DECLARE  @strColumns AS VARCHAR(MAX)
	SET @strColumns = ' ADC.numContactId, DM.numDivisionID, ISNULL(DM.numTerID,0) as numTerID, Opp.numRecOwner, DM.tintCRMType, Opp.numOppID, ISNULL(Opp.numBusinessProcessID,0) AS numBusinessProcessID'
        
	
	DECLARE @tintOrder as tinyint  = 0                                                  
	DECLARE @vcFieldName as varchar(50)                                                      
	DECLARE @vcListItemType as varchar(3)                                                 
	DECLARE @vcListItemType1 as varchar(1)                                                     
	DECLARE @vcAssociatedControlType varchar(10)                                                      
	DECLARE @numListID AS numeric(9)                                                      
	DECLARE @vcDbColumnName varchar(30)                          
	DECLARE @WhereCondition varchar(2000) = ''                          
	DECLARE @vcLookBackTableName varchar(2000)                    
	DECLARE @bitCustom as bit              
	DECLARE @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)   
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @vcColumnName AS VARCHAR(500)                  
	Declare @ListRelID AS NUMERIC(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = ''   

	SELECT TOP 1
		@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName, @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
		@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
		@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder asc            
                                                   
	WHILE @tintOrder>0                                                      
	BEGIN 
		IF @bitCustom = 0            
		BEGIN 
			DECLARE @Prefix as varchar(5)                                               
			
			IF @vcLookBackTableName = 'AdditionalContactsInformation'                    
				SET @Prefix = 'ADC.'                    
			IF @vcLookBackTableName = 'DivisionMaster'                    
				SET @Prefix = 'DM.'                    
			IF @vcLookBackTableName = 'OpportunityMaster'                    
				SET @PreFix ='Opp.'      
			IF @vcLookBackTableName = 'OpportunityRecurring'
				SET @PreFix = 'OPR.'
			IF @vcLookBackTableName = 'CompanyInfo'
				SET @PreFix = 'cmp.'
			IF @vcLookBackTableName = 'ProjectProgress'
				SET @PreFix = 'PP'

			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'
      
			IF @vcAssociatedControlType='SelectBox'                                                      
			BEGIN  
				IF @vcDbColumnName = 'numStatus'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT COUNT(OpportunityItems.numoppitemtCode) FROM OpportunityItems 
					WHERE OpportunityItems.bitItemPriceApprovalRequired=1 AND OpportunityItems.numOppId=Opp.numOppId) ApprovalMarginCount,ISNULL(Opp.intPromotionApprovalStatus,0) AS intPromotionApprovalStatus '
				END
				
				IF @vcDbColumnName='numPartner'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartner) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartner' 
				END
				ELSE IF @vcDbColumnName = 'numPartnerContact'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartenerContact) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT A.vcFirstName+'' ''+ A.vcLastName FROM AdditionalContactsInformation AS A WHERE A.numContactId=Opp.numPartenerContact) AS vcPartnerContact' 
				END
				ELSE IF @vcDbColumnName = 'tintSource'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID) ' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'tintOppStatus'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(CASE ISNULL(Opp.tintOppStatus,0) WHEN 1 THEN ''Deal Won'' WHEN 2 THEN ''Deal Lost'' ELSE ''Open'' END)' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numContactId'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'     
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') LIKE ''%' + @SearchText + '%'''
					END             
				END
				ELSE if @vcListItemType='LI'                                                       
				BEGIN
					SET @strColumns = @strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                      
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                      
				END                                                      
				ELSE IF @vcListItemType='S'                                                       
				BEGIN
					SET @strColumns = @strColumns+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName+']'   
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S'+ convert(varchar(3),@tintOrder)+'.vcState LIKE ''%' + @SearchText + '%'''
					END
					                                                   
					SET @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID='+@vcDbColumnName                                                      
				END     
				ELSE IF @vcListItemType = 'BP'
				BEGIN
					SET @strColumns = @strColumns + ',SPLM.Slp_Name' + ' ['	+ @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'SPLM.Slp_Name LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' LEFT JOIN Sales_process_List_Master SPLM on SPLM.Slp_Id = OPP.numBusinessProcessID '
				END
				ELSE IF @vcListItemType='PP'
				BEGIN
					SET @strColumns = @strColumns+', CASE WHEN PP.numOppID >0 then ISNULL(PP.intTotalProgress,0) ELSE dbo.GetListIemName(OPP.numPercentageComplete) END '+' ['+ @vcColumnName+']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(CASE WHEN PP.numOppID >0 then CONCAT(ISNULL(PP.intTotalProgress,0),''%'') ELSE dbo.GetListIemName(OPP.numPercentageComplete) END) LIKE ''%' + @SearchText + '%'''
					END
				END  
				ELSE IF @vcListItemType='T'                                                       
				BEGIN
					SET @strColumns = @strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']' 
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L'+ convert(varchar(3),@tintOrder)+'.vcData LIKE ''%' + @SearchText + '%'''
					END
					                                                     
					SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                      
				END                     
				ELSE IF @vcListItemType='U'                                                   
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'     
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') LIKE ''%' + @SearchText + '%'''
					END              
				END  
				ELSE IF @vcListItemType = 'WI'
				BEGIN
					SET @strColumns = @strColumns + ', Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end [' + @vcColumnName + ']'
				  
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end) LIKE ''%' + @SearchText + '%'''
					END 

					SET @WhereCondition= @WhereCondition +' left join WebAPI on WebAPI.WebApiId=isnull(OL.numWebApiId,0)
															left join Sites on isnull(OL.numSiteID,0)=Sites.numSIteID and Sites.numDomainID=' + convert(varchar(15),@numDomainID)                                                    
			   END                  
			END               
			ELSE IF @vcAssociatedControlType='DateField'                                                      
			BEGIN  
				SET @strColumns=@strColumns+',case when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
				SET @strColumns=@strColumns+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
				SET @strColumns=@strColumns+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '      
				SET @strColumns=@strColumns+'else dbo.FormatedDateFromDate('+@Prefix+@vcDbColumnName+','+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'               

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.FormatedDateFromDate('+@Prefix+@vcDbColumnName+','+ CONVERT(VARCHAR(10),@numDomainId)+') LIKE ''%' + @SearchText + '%'''
				END
			END              
			ELSE IF @vcAssociatedControlType='TextBox'                                                      
			BEGIN  
				SET @strColumns = @strColumns + ','+ CASE 
														WHEN @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'                   
														WHEN @vcDbColumnName='monPAmount' then 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(DECIMAL(20,5),monPAmount),1)'
														WHEN @vcDbColumnName='numDivisionID' then 'DM.numDivisionID'
														WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
														WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END ' 
														ELSE @Prefix + @vcDbColumnName 
													END +' ['+ @vcColumnName+']'  
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CASE 
														WHEN @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'                   
														WHEN @vcDbColumnName='monPAmount' then 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(DECIMAL(20,5),monPAmount),1)'
														WHEN @vcDbColumnName='numDivisionID' then 'DM.numDivisionID'
														WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
														WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END ' 
														ELSE @Prefix + @vcDbColumnName 
													END + ' LIKE ''%' + @SearchText + '%'''
				END              
			END  
			ELSE IF @vcAssociatedControlType='TextArea'                                              
			BEGIN  
				SET @strColumns=@strColumns+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']' 

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
				END     
			END
			ELSE IF @vcAssociatedControlType='Label'                                              
			BEGIN  
				SET @strColumns = @strColumns + ',' + CASE 
												WHEN @vcDbColumnName = 'CalAmount' THEN '[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
												FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId FOR XML PATH('''')),4,200000)),'''')'
												else @Prefix + @vcDbColumnName 
											END +' ['+ @vcColumnName+']'     
											
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CASE 
												WHEN @vcDbColumnName = 'CalAmount' THEN '[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
												FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId FOR XML PATH('''')),4,200000)),'''')'
												else @Prefix + @vcDbColumnName
											END + ' LIKE ''%' + @SearchText + '%'''
				END           
			END
  			ELSE IF @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
			BEGIN  
				SET @strColumns = @strColumns+',(SELECT SUBSTRING(
				(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
				FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
											JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
						WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
							AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'    
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(SELECT SUBSTRING(
						(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
						FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
													JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
								WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
									AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) LIKE ''%' + @SearchText + '%'''
				END            
			END  
		END               
		Else                          
		BEGIN 
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'
               
			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'          
			BEGIN              
				SET @strColumns= @strColumns+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName + ']'               
				SET @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '             
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid   '                                                     
			END            
			ELSE IF @vcAssociatedControlType = 'CheckBox'         
			BEGIN          
				SET @strColumns = @strColumns + ',case when isnull(CFW'+ CONVERT(VARCHAR(3),@tintOrder)+'.Fld_Value,0)=0 then ''No'' when isnull(CFW'+ CONVERT(VARCHAR(3),@tintOrder)+'.Fld_Value,0)=1 then ''Yes'' end   ['+ @vcColumnName + ']'             
				SET @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '           
					on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid   '                                                   
			END          
			ELSE IF @vcAssociatedControlType = 'DateField'       
			BEGIN            
				set @strColumns= @strColumns+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName + ']'               
				set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '             
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid    '                                                     
			END            
			ELSE IF @vcAssociatedControlType = 'SelectBox'         
			BEGIN            
				SET @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)    
				        
				SET @strColumns = @strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName + ']'                                                      
				SET @WhereCondition = @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '             
									on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid     '                                                     
				SET @WhereCondition = @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value' 
			END
			ELSE
			BEGIN
				SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID)') + ' [' + @vcColumnName + ']'
			END
		   
			IF LEN(@SearchText) > 0
			BEGIN
				SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CONCAT('dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID) LIKE ''%',@SearchText,'%''')
			END        
		END 
  
		SELECT TOP 1 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
			@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 
			SET @tintOrder=0 
	END                            
		
	 
	DECLARE @strShareRedordWith AS VARCHAR(300);
	SET @strShareRedordWith=' Opp.numOppId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' AND SR.numModuleID=3 AND SR.numAssignedTo=' + CONVERT(VARCHAR(15),@numUserCntId) + ') '
                                             
	DECLARE @strSql AS VARCHAR(MAX) = ''                                       
                                                                                 
	SET @strSql = ' FROM 
						OpportunityMaster Opp                                                                     
					INNER JOIN 
						AdditionalContactsInformation ADC                                                                     
					ON
						Opp.numContactId = ADC.numContactId                                  
					INNER JOIN 
						DivisionMaster DM                                                                     
					ON 
						Opp.numDivisionId = DM.numDivisionID AND ADC.numDivisionId = DM.numDivisionID                                                                     
					INNER JOIN 
						CompanyInfo cmp                                                                     
					ON 
						DM.numCompanyID = cmp.numCompanyId                                                                         
					LEFT JOIN 
						AdditionalContactsInformation ADC1 
					ON 
						ADC1.numContactId=Opp.numRecOwner 
					LEFT JOIN 
						ProjectProgress PP 
					ON 
						PP.numOppID = Opp.numOppID
					LEFT JOIN 
						OpportunityLinking OL 
					on 
						OL.numChildOppID=Opp.numOppId ' + @WhereCondition
                                     

	-------Change Row Color-------
	DECLARE @vcCSOrigDbCOlumnName AS VARCHAR(50) = ''
	DECLARE @vcCSLookBackTableName AS VARCHAR(50) = ''
	DECLARE @vcCSAssociatedControlType AS VARCHAR(50) = ''

	CREATE TABLE #tempColorScheme
	(
		vcOrigDbCOlumnName VARCHAR(50),
		vcLookBackTableName VARCHAR(50),
		vcFieldValue VARCHAR(50),
		vcFieldValue1 VARCHAR(50),
		vcColorScheme VARCHAR(50),
		vcAssociatedControlType VARCHAR(50)
	)

	INSERT INTO 
		#tempColorScheme 
	SELECT 
		DFM.vcOrigDbCOlumnName,
		DFM.vcLookBackTableName,
		DFCS.vcFieldValue,
		DFCS.vcFieldValue1,
		DFCS.vcColorScheme,
		DFFM.vcAssociatedControlType 
	FROM 
		DycFieldColorScheme DFCS 
	JOIN 
		DycFormField_Mapping DFFM 
	ON 
		DFCS.numFieldID=DFFM.numFieldID
	JOIN 
		DycFieldMaster DFM 
	ON 
		DFM.numModuleID=DFFM.numModuleID 
		AND DFM.numFieldID=DFFM.numFieldID
	WHERE 
		DFCS.numDomainID=@numDomainID 
		AND DFFM.numFormID=@numFormId 
		AND DFCS.numFormID=@numFormId 
		AND ISNULL(DFFM.bitAllowGridColor,0)=1

	IF(SELECT COUNT(*) FROM #tempColorScheme)>0
	BEGIN
		SELECT TOP 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType FROM #tempColorScheme
	END   

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		SET @strColumns = @strColumns + ',tCS.vcColorScheme'
	END

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		IF @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
			SET @Prefix = 'ADC.'                  
		IF @vcCSLookBackTableName = 'DivisionMaster'                  
			SET @Prefix = 'DM.'
		IF @vcCSLookBackTableName = 'CompanyInfo'                  
			SET @Prefix = 'CMP.' 
		IF @vcCSLookBackTableName = 'OpportunityMaster'                  
			SET @Prefix = 'Opp.'
		
		IF @vcCSAssociatedControlType='DateField'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
					and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
		END
		ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
		END
	END      

    DECLARE @CustomControlType AS VARCHAR(10)      
	IF @columnName like 'CFW.Cust%'             
	BEGIN

		SET @strSql = @strSql +' LEFT JOIN CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid and CFW.fld_id= ' + REPLACE(@columnName,'CFW.Cust','') +' ' 
		
		SET @CustomControlType = (SELECT Fld_type FROM CFW_Fld_Master CFWM where CFWM.Fld_id = REPLACE(@columnName,'CFW.Cust',''))

		IF @CustomControlType = 'DateField'
		BEGIN
			 SET @columnName='CAST(CFW.Fld_Value AS Date)'   
		END
		ELSE
		BEGIN
			SET @columnName='CFW.Fld_Value'   
		END
	END
	
	ELSE IF @columnName like 'DCust%'            
	BEGIN            
		SET @strSql = @strSql + ' left Join CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid   and CFW.fld_id= '+replace(@columnName,'DCust','')                                                                                               
		SET @strSql = @strSql + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '            
		set @columnName='LstCF.vcData'                   
	END
                                        
	IF @bitPartner=1 
	BEGIN
		SET @strSql=@strSql+' LEFT JOIN OpportunityContact OppCont ON OppCont.numOppId=Opp.numOppId'                                                                               
	END

	SET @strSql = @strSql +' WHERE Opp.tintOppstatus=0 AND DM.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' AND Opp.tintOppType= ' + convert(varchar(1),@OppType)                                                              

	IF @SortChar <> '0' 
		SET @strSql = @strSql + ' And Opp.vcPOppName like ''' + @SortChar + '%'''
                                
	IF @numDivisionID <> 0 
	BEGIN
		SET @strSql = @strSql + ' AND DM.numDivisionID =' + CONVERT(VARCHAR(15),@numDivisionID)                              
    END
	                                                                
	IF @tintUserRightType=1 
	BEGIN
		SET @strSql = @strSql + ' AND (Opp.numRecOwner = '
							+ CONVERT(VARCHAR(15),@numUserCntID) 
							+ ' or Opp.numAssignedTo= '
							+ convert(varchar(15),@numUserCntID)  
							+' or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo ='
							+ convert(varchar(15),@numUserCntID)+')'
							+ case when @bitPartner=1 then ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+convert(varchar(15),@numUserCntID)+ ')' else '' end 
							+ ' or ' + @strShareRedordWith +')'
    END                                        
	ELSE IF @tintUserRightType=2 
	BEGIN
		SET @strSql = @strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '
							+ convert(varchar(15),@numUserCntID)+' ) or DM.numTerID=0 or Opp.numAssignedTo= '
							+ convert(varchar(15),@numUserCntID)  +' or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo ='
							+ convert(varchar(15),@numUserCntID)+')' + ' or ' + @strShareRedordWith +')'                                                                        
	END           
	
	                                                  
	IF @tintSortOrder=1 
	BEGIN 
		SET @strSql = @strSql + ' AND (Opp.numRecOwner = ' 
							+ CONVERT(VARCHAR(15),@numUserCntID) 
							+ ' or Opp.numAssignedTo= '+ convert(varchar(15),@numUserCntID) 
							+ ' or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo ='
							+ convert(varchar(15),@numUserCntID)+')'
							+ case when @bitPartner=1 then ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+convert(varchar(15),@numUserCntID)+ ')' else '' end 
							+ ' or ' + @strShareRedordWith +')'                        
    END
	ELSE IF @tintSortOrder=2  
	BEGIN
		SET @strSql = @strSql + 'AND (Opp.numRecOwner in (select A.numContactID from AdditionalContactsInformation A where A.numManagerID='
							+ convert(varchar(15),@numUserCntID) 
							+') or Opp.numAssignedTo in (select A.numContactID from AdditionalContactsInformation A where A.numManagerID='
							+ convert(varchar(15),@numUserCntID)  
							+') or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo in (select A.numContactID from AdditionalContactsInformation A where A.numManagerID='
							+ convert(varchar(15),@numUserCntID)+'))'
							+ case when @bitPartner=1 then ' or ( OppCont.bitPartner=1 and OppCont.numContactId in (select A.numContactID from AdditionalContactsInformation A where A.numManagerID='+ convert(varchar(15),@numUserCntID)+ '))' else '' end 
							+ ' or ' + @strShareRedordWith +')'                                                                    
	END
	ELSE IF @tintSortOrder=4  
	BEGIN
		SET @strSql=@strSql + ' AND Opp.bintCreatedDate> '''+convert(varchar(20),dateadd(day,-8,@dtLastDate))+''''                                                                    
	END
                                                 

	IF @vcRegularSearchCriteria <> '' 
	BEGIN
		SET @strSql = @strSql + ' AND ' + @vcRegularSearchCriteria 
	END

	IF @vcCustomSearchCriteria <> '' 
	BEGIN
		SET @strSql = @strSql + ' AND Opp.numOppid IN (SELECT DISTINCT CFW.RecId FROM CFW_Fld_Values_Opp CFW where ' + @vcCustomSearchCriteria + ')'
	END

	IF LEN(@SearchQuery) > 0
	BEGIN
		SET @strSql = @strSql + ' AND (' + @SearchQuery + ') '
	END

	IF ISNULL(@tintDashboardReminderType,0) = 4
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppID
																FROM 
																	StagePercentageDetails SPDInner
																INNER JOIN 
																	OpportunityMaster OMInner
																ON 
																	SPDInner.numOppID=OMInner.numOppId 
																WHERE 
																	SPDInner.numDomainId=',@numDOmainID,'
																	AND OMInner.numDomainId=',@numDOmainID,' 
																	AND OMInner.tintOppType = 1
																	AND ISNULL(OMInner.tintOppStatus,0) = 0
																	AND tinProgressPercentage <> 100',') ')

	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 18
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppID
																FROM 
																	StagePercentageDetails SPDInner
																INNER JOIN 
																	OpportunityMaster OMInner
																ON 
																	SPDInner.numOppID=OMInner.numOppId 
																WHERE 
																	SPDInner.numDomainId=',@numDOmainID,'
																	AND OMInner.numDomainId=',@numDOmainID,' 
																	AND OMInner.tintOppType = 2
																	AND ISNULL(OMInner.tintOppStatus,0) = 0
																	AND tinProgressPercentage <> 100',') ')

	END

	DECLARE @firstRec AS INTEGER                                                              
	DECLARE @lastRec AS INTEGER    
	                                                 
	SET @firstRec= (@CurrentPage-1) * @PageSize                             
	SET @lastRec= (@CurrentPage*@PageSize+1)                                                                    


    -- GETS ROWS COUNT
	DECLARE @strTotal NVARCHAR(MAX) = 'SELECT @TotalRecords = COUNT(*) ' + @strSql
	exec sp_executesql @strTotal, N'@TotalRecords INT OUT', @TotRecs OUT
               
                              
	-- EXECUTE FINAL QUERY
	DECLARE @strFinal AS VARCHAR(MAX)
	SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, ',@strColumns,' INTO #tempTable',@strSql,'; SELECT * FROM #tempTable WHERE ID > ',@firstRec,' and ID <',@lastRec,' ORDER BY ID; DROP TABLE #tempTable;')
	PRINT @strFinal
	EXEC (@strFinal) 

	SELECT * FROM #tempForm

	DROP TABLE #tempForm
	DROP TABLE #tempColorScheme
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPackingDetail')
DROP PROCEDURE USP_GetPackingDetail
GO
CREATE PROCEDURE USP_GetPackingDetail
    @numDomainID AS NUMERIC(9),
	@vcBizDocsIds VARCHAR(MAX),
	@tintMode AS TINYINT 
AS
BEGIN
	-- PICK LIST
	IF @tintMode = 1
	BEGIN
		SET @vcBizDocsIds = STUFF((SELECT
									CONCAT(',',numOppBizDocsId)
								FROM
									OpportunityBizDocs
								WHERE
									numOppId IN (SELECT Id FROM dbo.SplitIDs(@vcBizDocsIds,','))
									AND numBizDocId = ISNULL(55206,0)
								FOR XML PATH('')),1,1,'')

		SELECT
			ISNULL(vcItemName,'') AS vcItemName,
			ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=@numDomainID AND ItemImages.numItemCode=Item.numItemCode AND bitDefault=1 AND bitIsImage=1),'') AS vcImage,
			ISNULL(vcModelID,'') AS vcModelID,
			ISNULL(txtItemDesc,'') AS txtItemDesc,
			ISNULL(vcWareHouse,'') AS vcWareHouse,
			ISNULL(vcLocation,'') AS vcLocation,
			(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcWHSKU ELSE Item.vcSKU END) AS vcSKU,
			(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcBarCode ELSE Item.numBarCodeId END) AS vcUPC,
			CAST(SUM(numUnitHour) AS FLOAT) AS numUnitHour
		FROM
			OpportunityBizDocItems
		INNER JOIN
			WareHouseItems
		ON
			OpportunityBizDocItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
		INNER JOIN
			Warehouses
		ON
			WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
		INNER JOIN
			Item
		ON
			OpportunityBizDocItems.numItemCode = Item.numItemCode
		WHERE
			numOppBizDocID IN ( SELECT * FROM dbo.splitIds(@vcBizDocsIds,','))
		GROUP BY
			Item.numItemCode,
			vcItemName,
			vcModelID,
			txtItemDesc,
			vcWareHouse,
			vcLocation,
			(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcWHSKU ELSE Item.vcSKU END),
			(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcBarCode ELSE Item.numBarCodeId END)
		ORDER BY
			vcWareHouse ASC,
			vcLocation ASC,
			vcItemName ASC
	END
	-- GROUP LIST
	ELSE IF @tintMode = 2
	BEGIN
		SELECT
			ISNULL(vcItemName,'') AS vcItemName,
			ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=@numDomainID AND ItemImages.numItemCode=Item.numItemCode AND bitDefault=1 AND bitIsImage=1),'') AS vcImage,
			ISNULL(vcModelID,'') AS vcModelID,
			ISNULL(txtItemDesc,'') AS txtItemDesc,
			OpportunityBizDocs.numOppBizDocsId,
			(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcWHSKU ELSE Item.vcSKU END) AS vcSKU,
			CAST(numUnitHour AS FLOAT) AS numUnitHour
		FROM
			OpportunityBizDocs
		INNER JOIN
			OpportunityBizDocItems
		ON
			OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
		INNER JOIN
			WareHouseItems
		ON
			OpportunityBizDocItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
		INNER JOIN
			Warehouses
		ON
			WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
		INNER JOIN
			Item
		ON
			OpportunityBizDocItems.numItemCode = Item.numItemCode
		WHERE
			numOppBizDocID IN ( SELECT * FROM dbo.splitIds(@vcBizDocsIds,','))
		ORDER BY
			vcWareHouse ASC,
			vcLocation ASC,
			vcItemName ASC
	END
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getpromotiondrulefororder')
DROP PROCEDURE usp_getpromotiondrulefororder
GO
CREATE PROCEDURE [dbo].[USP_GetPromotiondRuleForOrder]
	 @numDomainID NUMERIC(18,0)
	,@numSubTotal FLOAT = 0
	,@numDivisionID NUMERIC(18,0)
	,@numSiteID NUMERIC(18,0)
AS
BEGIN	
	DECLARE @numRelationship NUMERIC(18,0) = 0
	DECLARE @numProfile NUMERIC(18,0) = 0

	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT 
			@numRelationship=ISNULL(numCompanyType,0)
			,@numProfile=ISNULL(vcProfile,0) 
		FROM 
			DivisionMaster 
		INNER JOIN 
			CompanyInfo 
		ON 
			DivisionMaster.numCompanyID=CompanyInfo.numCompanyId 
		WHERE 
			numDivisionID=@numDivisionID
	END
	ELSE IF ISNULL(@numSiteID,0) > 0
	BEGIN
		SELECT
			@numRelationship=ISNULL(numRelationshipId,0)
			,@numProfile=ISNULL(numProfileId,0)
		FROM
			eCommerceDTL
		WHERE
			numSiteID=@numSiteID
	END



	DECLARE @numProID AS NUMERIC

	SET @numProID = (SELECT TOP 1 
						PO.numProId		 
					FROM 
						PromotionOffer PO	
					INNER JOIN 
						PromotionOfferOrganizations PORG
					ON 
						PO.numProId = PORG.numProId
					WHERE 
						PO.numDomainId = @numDomainID 
						AND PO.IsOrderBasedPromotion = 1 
						AND ISNULL(bitEnabled,0) = 1
						AND numRelationship=@numRelationship 
						AND numProfile=@numProfile
						AND  1 = (CASE 
									WHEN ISNULL(PO.bitNeverExpires,0) = 1
									THEN 1
									WHEN ISNULL(PO.bitNeverExpires,0) = 0 
									THEN 
										CASE WHEN (PO.dtValidFrom <= GETUTCDATE() AND PO.dtValidTo >= GETUTCDATE())
									THEN 1	
									ELSE 0 END
								ELSE 0 END )
				)


	SELECT * FROM PromotionOffer WHERE numProId = @numProID 
	 	
	SELECT ROW_NUMBER() OVER(ORDER BY numProOfferOrderID ASC) AS RowNum, numOrderAmount,fltDiscountValue FROM PromotionOfferOrder WHERE numPromotionID = @numProID

END 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPromotionOffer')
DROP PROCEDURE USP_GetPromotionOffer
GO
CREATE PROCEDURE [dbo].[USP_GetPromotionOffer]
    @numProId AS NUMERIC(9) = 0,
    @byteMode AS TINYINT = 0,
    @numDomainID AS NUMERIC(9) = 0
AS 
BEGIN
    IF @byteMode = 0 
    BEGIN    
		SELECT  
			[numProId]
			,[vcProName]
			,[numDomainId]
			,[dtValidFrom]
			,[dtValidTo]
			,[bitNeverExpires]
			,[bitApplyToInternalOrders]
			,[bitAppliesToSite]
			,[bitRequireCouponCode]
			,[txtCouponCode]
			,[tintUsageLimit]
			--,[bitFreeShiping]
			--,[monFreeShippingOrderAmount]
			--,[numFreeShippingCountry]
			,[bitDisplayPostUpSell]
			,[intPostSellDiscount]
			--,[bitFixShipping1]
			--,[monFixShipping1OrderAmount]
			--,[monFixShipping1Charge]
			--,[bitFixShipping2]
			--,[monFixShipping2OrderAmount]
			--,[monFixShipping2Charge]
			,[tintOfferTriggerValueType]
			,[fltOfferTriggerValue]
			,[tintOfferBasedOn]
			,[fltDiscountValue]
			,[tintDiscountType]
			,[tintDiscoutBaseOn]
			,[numCreatedBy]
			,[dtCreated]
			,[numModifiedBy]
			,[dtModified]
			,[tintCustomersBasedOn]
			,[tintOfferTriggerValueTypeRange]
			,[fltOfferTriggerValueRange]
			,[IsOrderBasedPromotion]
        FROM 
			PromotionOffer
        WHERE 
			numProId = @numProId
    END    
   ELSE IF @byteMode = 1
    BEGIN  
	   
        SELECT  
			numProId,
            vcProName,
			(CASE WHEN ISNULL(bitNeverExpires,0) = 1 THEN 'Promotion Never Expires' ELSE CONCAT(dbo.FormatedDateFromDate(dtValidFrom,@numDomainID),' to ',dbo.FormatedDateFromDate(dtValidTo,@numDomainID)) END) AS vcDateValidation,

			(CASE WHEN ISNULL(bitRequireCouponCode,0) = 1 THEN
			              (CASE WHEN ISNULL(IsOrderBasedPromotion,0) = 1 THEN
								 STUFF((SELECT CONCAT (',' , vcDiscountCode , ' (', ISNULL(DCU.intCodeUsed,0),')')
									   FROM DiscountCodes  D
									   LEFT JOIN DiscountCodeUsage DCU ON D.numDiscountId = DCU.numDiscountId
									   WHERE D.numPromotionID = PO.numProId
									   FOR XML PATH ('')), 1, 1, '')   
						      ELSE CONCAT(txtCouponCode,' (', ISNULL(intCouponCodeUsed,0),')') END)
						  
			ELSE '' END) AS vcCouponCode			
			
			,(CASE WHEN ISNULL(bitApplyToInternalOrders,0) = 1 THEN 'Yes' ELSE 'No' END) AS vcInternalOrders
			,(CASE WHEN ISNULL(bitAppliesToSite,0)=1 THEN (SELECT Stuff((SELECT CONCAT(', ',Sites.vcSiteName) FROM PromotionOfferSites POS INNER JOIN Sites ON pos.numSiteID=Sites.numSiteID WHERE POS.numPromotionID=PO.numProId FOR XML PATH('')), 1, 2, '')) ELSE '' END) AS vcSites

			,CASE WHEN ISNULL(IsOrderBasedPromotion,0) = 1 THEN
						CONCAT('Buy ', 
							CASE tintDiscountType WHEN 1 THEN
										STUFF((SELECT CONCAT (', ' , '$', numOrderAmount , ' & get ', ISNULL(fltDiscountValue,0),' % off')
										   FROM PromotionOfferOrder POO					   
										   WHERE POO.numPromotionID =  PO.numProId
										   FOR XML PATH ('')), 1, 1, '')  
									WHEN 2 THEN 
										STUFF((SELECT CONCAT (', ','$', numOrderAmount , ' & get $', ISNULL(fltDiscountValue,0),' off')
												   FROM PromotionOfferOrder POO					   
												   WHERE POO.numPromotionID =  PO.numProId
												   FOR XML PATH ('')), 1, 1, '')
							ELSE '' END)
			ELSE
			CONCAT('Buy '
					,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
					,CASE tintOfferBasedOn
							WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
							WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
						END
					,' & get '
					, CASE  
						WHEN (tintDiscountType = 1 AND tintDiscoutBaseOn <> 4) THEN 
							CONCAT(fltDiscountValue,'% off on') 
						WHEN (tintDiscountType = 2 AND tintDiscoutBaseOn <> 4) THEN 
							CONCAT('$',fltDiscountValue, ' off on')
						WHEN (tintDiscountType = 3 AND tintDiscoutBaseOn <> 4) THEN 
						CONCAT(fltDiscountValue,' of') END
					, CASE 
						WHEN tintDiscoutBaseOn = 1 THEN 
							CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 2 THEN 
							CONCAT
							(
								CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
						WHEN tintDiscoutBaseOn = 4 THEN CONCAT(fltDiscountValue,' quantity free for any item of equal or lesser value')
						ELSE '' 
					END)
				 END AS vcPromotionRule,
				(CASE WHEN (SELECT COUNT(*) FROM OpportunityItems JOIN OpportunityMaster ON OpportunityItems.numOppId=OpportunityMaster.numOppId WHERE numDomainId=@numDomainID AND OpportunityItems.numPromotionID=PO.numProId) > 0 THEN 0 ELSE 1 END) AS bitCanEdit
				,bitEnabled, tintCustomersBasedOn, tintOfferTriggerValueTypeRange, fltOfferTriggerValueRange, [IsOrderBasedPromotion]
        FROM  
			PromotionOffer PO
		WHERE 
			numDomainID = @numDomainID
			
		ORDER BY
			dtCreated DESC
    END
	  
   	
END      
  
GO
/****** Object:  StoredProcedure [dbo].[usp_GetSalesFulfillmentOrder]    Script Date: 03/06/2009 00:39:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSalesFulfillmentOrder')
DROP PROCEDURE USP_GetSalesFulfillmentOrder
GO
CREATE PROCEDURE [dbo].[USP_GetSalesFulfillmentOrder]
(
    @numDomainId AS NUMERIC(9) = 0,
    @SortCol VARCHAR(50),
    @SortDirection VARCHAR(4),
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT,
    @numUserCntID NUMERIC,
    @vcBizDocStatus VARCHAR(500),
    @vcBizDocType varchar(500),
    @numDivisionID as numeric(9)=0,
    @ClientTimeZoneOffset INT=0,
    @bitIncludeHistory bit=0,
    @vcOrderStatus VARCHAR(500),
    @vcOrderSource VARCHAR(1000),
	@numOppID AS NUMERIC(18,0) = 0,
	@vcShippingService AS VARCHAR(MAX) = '',
	@numShippingZone NUMERIC(18,0)=0,
	@vcRegularSearchCriteria VARCHAR(MAX) = '',
	@vcCustomSearchCriteria VARCHAr(MAX) = ''
)
AS 
BEGIN

	DECLARE @firstRec AS INTEGER                                                              
	DECLARE @lastRec AS INTEGER                                                     
	SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                             
	SET @lastRec = ( @CurrentPage * @PageSize + 1 )      
	
	DECLARE @tintCommitAllocation TINYINT
	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainId                                                              

	IF LEN(ISNULL(@SortCol,''))=0 OR CHARINDEX('OBD.',@SortCol) > 0 OR @SortCol='dtCreatedDate'
		SET @SortCol = 'Opp.bintCreatedDate'
		
	IF LEN(ISNULL(@SortDirection,''))=0	
		SET @SortDirection = 'DESC'

	CREATE TABLE #tempForm 
	(
		ID INT IDENTITY(1,1), tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0
	DECLARE @numFormId INT = 23

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormId AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1


		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				@numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=@numFormId 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1
		ORDER BY 
			tintOrder asc  
	END

	DECLARE @strColumns VARCHAR(MAX) = ''
    DECLARE @FROM NVARCHAR(MAX) = ''
    DECLARE @WHERE NVARCHAR(MAX) = ''

	SET @FROM =' FROM 
						OpportunityMaster Opp
					INNER JOIN 
						DivisionMaster DM 
					ON 
						Opp.numDivisionId = DM.numDivisionID
					INNER JOIN 
						CompanyInfo cmp 
					ON 
						cmp.numCompanyID=DM.numCompanyID
					LEFT JOIN
						AdditionalContactsInformation ADC
					ON
						Opp.numContactID=ADC.numContactID
					LEFT JOIN
						DivisionMasterShippingConfiguration DMSC
					ON
						DM.numDivisionID=DMSC.numDivisionID'
					
	SET @WHERE = CONCAT(' WHERE 
							(Opp.numOppID = ',@numOppID,' OR ',@numOppID,' = 0) 
							AND Opp.tintopptype = 1 
							AND Opp.tintOppStatus=1 ',(CASE WHEN @numDomainID <> 214 THEN ' AND (CHARINDEX(''Ready to Ship'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 OR CHARINDEX(''Not Applicable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0) ' ELSE CONCAT(' AND 1 = dbo.IsSalesOrderReadyToFulfillment(Opp.numDomainID,Opp.numOppID,',@tintCommitAllocation,')') END), '
							AND Opp.numDomainID = ',@numDomainId)

	IF @SortCol like 'CFW.Cust%'             
	BEGIN            
		SET @FROM = @FROM + ' LEFT JOIN CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid and CFW.fld_id= '+ REPLACE(@SortCol,'CFW.Cust','') +' '                                                     
		SET @SortCol='CFW.Fld_Value'            
	END 
      
    DECLARE @strSql NVARCHAR(MAX)
    DECLARE @SELECT NVARCHAR(MAX) = 'WITH bizdocs AS 
									(SELECT
										Opp.numOppID
										,Opp.numRecOwner
										,Opp.vcpOppName
										,DM.tintCRMType
										,ISNULL(DM.numTerID,0) numTerID
										,DM.numDivisionID
										,ISNULL(Opp.intUsedShippingCompany,ISNULL(DM.intShippingCompany,90)) AS numShipVia
										,ISNULL(Opp.intUsedShippingCompany,ISNULL(DM.intShippingCompany,90)) as intUsedShippingCompany
										,ISNULL(Opp.numShippingService,0) numShippingService
										,CASE WHEN (SELECT COUNT(*) FROM ShippingBox INNER JOIN ShippingReport ON ShippingBox.numShippingReportId=ShippingReport.numShippingReportId WHERE ShippingReport.numOppID=Opp.numOppID AND LEN(ISNULL(vcTrackingNumber,'''')) > 0) > 0 THEN 1 ELSE 0 END AS bitTrackingNumGenerated
										,Opp.monDealAmount
										,(CASE WHEN (SELECT COUNT(*) FROM SalesFulfillmentQueue WHERE numDomainID = Opp.numDomainId AND numOppId=Opp.numOppId AND ISNULL(bitExecuted,0)=0) > 0 THEN 1 ELSE 0 END) as bitPendingExecution
										,ISNULL(Opp.monDealAmount,0) AS monDealAmount1'

	IF CHARINDEX('OBD.monAmountPaid',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('OBD.monAmountPaid like ''%0%''',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%0%''',' 1 = (SELECT ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM  OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)) ')
		IF CHARINDEX('OBD.monAmountPaid like ''%1%''',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%1%''','  1 = (SELECT (CASE WHEN (SELECT (CASE WHEN (SUM(ISNULL(monDealAmount,0)) = SUM(ISNULL(monAmountPaid,0)) AND SUM(ISNULL(monAmountPaid,0)) <> 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE BD.numOppId =  Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END))  ')
		IF CHARINDEX('OBD.monAmountPaid like ''%2%''',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%2%''',' 1 = (SELECT CASE WHEN (SELECT (CASE WHEN ((SUM(ISNULL(monDealAmount,0)) - SUM(ISNULL(monAmountPaid,0))) > 0 AND SUM(ISNULL(monAmountPaid,0)) <> 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('OBD.monAmountPaid like ''%3%''',@vcRegularSearchCriteria) > 0								   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%3%''',' 1 = (SELECT CASE WHEN (SELECT (CASE WHEN (SUM(ISNULL(monAmountPaid,0)) = 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END) ')
	END
	
	IF CHARINDEX('OI.vcInventoryStatus',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('OI.vcInventoryStatus=1',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=1',' CHARINDEX(''Not Applicable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=2',' CHARINDEX(''Allocation Cleared'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=3',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=3',' CHARINDEX(''Back Order'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=4',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=4',' CHARINDEX(''Ready to Ship'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
	END

	IF CHARINDEX('dbo.fn_getOPPAddressState(Opp.numOppId,Opp.numDomainID,2)',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'dbo.fn_getOPPAddressState(Opp.numOppId,Opp.numDomainID,2)','(CASE 
																																		WHEN Opp.tintShipToType IS NULL OR Opp.tintShipToType = 1
																																		THEN
																																			(SELECT  
																																				ISNULL(dbo.fn_GetState(AD.numState),'''')
																																			FROM 
																																				AddressDetails AD 
																																			WHERE 
																																				AD.numDomainID=Opp.numDomainID AND AD.numRecordID=Opp.numDivisionID 
																																				AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1)
																																		WHEN Opp.tintShipToType = 0
																																		THEN
																																			(SELECT 
																																				ISNULL(dbo.fn_GetState(AD.numState),'''')
																																			FROM 
																																				CompanyInfo [Com1] 
																																			JOIN 
																																				DivisionMaster div1 
																																			ON 
																																				com1.numCompanyID = div1.numCompanyID
																																			JOIN Domain D1 ON D1.numDivisionID = div1.numDivisionID
																																			JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 2 AND bitIsPrimary=1
																																			WHERE  D1.numDomainID = Opp.numDomainID)
																																		WHEN Opp.tintShipToType = 2 OR Opp.tintShipToType = 3
																																		THEN
																																			(SELECT
																																				ISNULL(dbo.fn_GetState(numShipState),'''')
																																			FROM 
																																				OpportunityAddress 
																																			WHERE 
																																				numOppID = Opp.numOppId)
																																		ELSE ''''
																																	END)')
	END
	

	IF @vcRegularSearchCriteria<>'' 
	BEGIN
		SET @WHERE = @WHERE + ' AND ' + @vcRegularSearchCriteria 
	END

	IF @vcCustomSearchCriteria<>'' 
	BEGIN
		SET @WHERE = @WHERE +' AND ' +  @vcCustomSearchCriteria
	END

	DECLARE @tintOrder  AS TINYINT;SET @tintOrder = 0
	DECLARE @vcFieldName  AS VARCHAR(50)
	DECLARE @vcListItemType  AS VARCHAR(3)
	DECLARE @vcListItemType1  AS VARCHAR(1)
	DECLARE @vcAssociatedControlType VARCHAR(30)
	DECLARE @numListID  AS NUMERIC(9)
	DECLARE @vcDbColumnName VARCHAR(40)
	DECLARE @vcLookBackTableName VARCHAR(2000)
	DECLARE @bitCustom  AS BIT
	Declare @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)   
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @vcColumnName AS VARCHAR(500)
	DECLARE @ListRelID AS NUMERIC(9) 

	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT 
	SELECT @iCount = COUNT(*) FROM #tempForm

	WHILE @i <= @iCount
	BEGIN
		SELECT 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName ,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
			@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE
			ID=@i

		IF @bitCustom = 0
		BEGIN
			DECLARE  @Prefix  AS VARCHAR(5)

			IF @vcLookBackTableName = 'AdditionalContactsInformation'
				SET @Prefix = 'ADC.'
			IF @vcLookBackTableName = 'DivisionMaster'
				SET @Prefix = 'DM.'
			IF @vcLookBackTableName = 'OpportunityMaster'
				SET @PreFix = 'Opp.'
			IF @vcLookBackTableName = 'CompanyInfo'
				SET @PreFix = 'cmp.'
			IF @vcLookBackTableName = 'DivisionMasterShippingConfiguration'
				SET @PreFix = 'DMSC.'

			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

			IF @vcAssociatedControlType = 'SelectBox'
			BEGIN
				IF @vcDbColumnName = 'numPartner'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartner' 
				END
				ELSE IF @vcDbColumnName = 'tintSource'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'')' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numContactID'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
				END
				ELSE IF @vcListItemType = 'LI'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					SET @FROM = @FROM + ' LEFT JOIN ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'U'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
				END
				ELSE IF @vcListItemType = 'SGT'
				BEGIN
					SET @strColumns = @strColumns + ',(CASE ' + @Prefix + @vcDbColumnName + ' 
														WHEN 0 THEN ''Service Default''
														WHEN 1 THEN ''Adult Signature Required''
														WHEN 2 THEN ''Direct Signature''
														WHEN 3 THEN ''InDirect Signature''
														WHEN 4 THEN ''No Signature Required''
														ELSE ''''
														END) [' + @vcColumnName + ']'
				END
				ELSE IF @vcListItemType = 'PSS'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=Opp.numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = ' + @Prefix + @vcDbColumnName + '),'''') [' + @vcColumnName + ']'
				END
            END
			ELSE IF @vcAssociatedControlType = 'DateField'
			BEGIN
				IF @vcDbColumnName = 'dtReleaseDate'
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),'
                                + @Prefix
                                + @vcDbColumnName
                                + ')= convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate())) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,-1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']'
				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
                                + @Prefix
                                + @vcDbColumnName
                                + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']'
				 END	
            END
            ELSE IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea'
            BEGIN
				IF @vcDbColumnName = 'vcCompanyName'
				BEGIN
					SET @strColumns = @strColumns + ',' + @Prefix + @vcDbColumnName + ' [' + @vcColumnName + ']'
					SET @strColumns = @strColumns + ',dbo.fn_GetListItemName(Opp.[numstatus]) vcOrderStatus'
				END
				ELSE IF @vcDbColumnName = 'numShipState'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_getOPPAddressState(Opp.numOppId,Opp.numDomainID,2)' + ' [' + @vcColumnName + ']'
				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns + ',' + @Prefix + @vcDbColumnName + ' [' + @vcColumnName + ']'
				END
            END
            ELSE IF  @vcAssociatedControlType='Label'                                              
			BEGIN
				SET @strColumns=@strColumns + ',' + CASE   
				WHEN @vcDbColumnName = 'vcFulfillmentStatus' THEN 'ISNULL(STUFF((SELECT CONCAT(''<br />'',vcMessage) FROM SalesFulfillmentLog WHERE numDomainID = Opp.numDomainId AND numOppID=Opp.numOppId AND ISNULL(bitSuccess,0) = 1 FOR XML PATH('''')) ,1,12,''''),'''')'                 
				WHEN @vcDbColumnName = 'vcPricedBoxedTracked' THEN 'STUFF((SELECT CONCAT('', '',OpportunityBizDocs.numOppID,''~'',numOppBizDocsId,''~'',ISNULL(numShippingReportId,0),''~'',(CASE WHEN (SELECT COUNT(*) FROM ShippingBox WHERE numShippingReportId=ShippingReport.numShippingReportId) > 0 THEN 1 ELSE 0 END)) vcText FROM OpportunityBizDocs LEFT JOIN ShippingReport ON ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId WHERE OpportunityBizDocs.numBizDocId IN (SELECT ISNULL(numDefaultSalesShippingDoc,0) FROM Domain WHERE numDomainID=Opp.numDomainID) AND OpportunityBizDocs.numOppId=Opp.numOppID AND OpportunityBizDocs.bitShippingGenerated=1 FOR XML PATH(''''), TYPE).value(''.'',''NVARCHAR(MAX)''),1,2,'' '')'
				WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'--'[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
				WHEN @vcDbColumnName = 'vcInventoryStatus' THEN 'ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),'''')'
				WHEN @vcDbColumnName = 'vcOrderedShipped' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped)'
				WHEN @vcDbColumnName = 'vcOrderedReceived' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				ELSE @Prefix + @vcDbColumnName END +' ['+ @vcColumnName+']'        
			END
        END
		ELSE
        BEGIN
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
            BEGIN
              SET @strColumns = @strColumns
                              + ',CFW'
                              + CONVERT(VARCHAR(3),@tintOrder)
                              + '.Fld_Value  ['
                              + @vcColumnName + ']'
              SET @FROM = @FROM
                                      + ' left Join CFW_FLD_Values_Opp CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + ' on CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.Fld_Id='
                                      + CONVERT(VARCHAR(10),@numFieldId)
                                      + 'and CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.RecId=Opp.numOppid   '
			END
			ELSE
            IF @vcAssociatedControlType = 'CheckBox' 
            BEGIN
                SET @strColumns = @strColumns
                                + ',case when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=0 then 0 when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=1 then 1 end   ['
                                + @vcColumnName + ']'
                SET @FROM = @FROM
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid   '
            END
            ELSE
            IF @vcAssociatedControlType = 'DateField'
            BEGIN
                  SET @strColumns = @strColumns
                                  + ',dbo.FormatedDateFromDate(CFW'
                                  + CONVERT(VARCHAR(3),@tintOrder)
                                  + '.Fld_Value,'
                                  + CONVERT(VARCHAR(10),@numDomainId)
                                  + ')  ['
                                  + @vcColumnName + ']'
                  SET @FROM = @FROM
                                          + ' left Join CFW_FLD_Values_Opp CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + ' on CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.Fld_Id='
                                          + CONVERT(VARCHAR(10),@numFieldId)
                                          + 'and CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.RecId=Opp.numOppid    '
            END
            ELSE
            IF @vcAssociatedControlType = 'SelectBox'
            BEGIN
                SET @vcDbColumnName = 'DCust'
                                            + CONVERT(VARCHAR(10),@numFieldId)
                SET @strColumns = @strColumns
                                + ',L'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.vcData'
                                + ' ['
                                + @vcColumnName + ']'

                SET @FROM = @FROM
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid '

                SET @FROM = @FROM
                                        + ' left Join ListDetails L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.numListItemID=CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Value'
            END
			ELSE
			BEGIN
				SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID)') + ' [' + @vcColumnName + ']'
			END
		END

		SET @i = @i + 1
	END

		
		DECLARE @numShippingServiceItemID AS NUMERIC
		SELECT @numShippingServiceItemID=ISNULL(numShippingServiceItemID,0) FROM domain WHERE numDomainID=@numDomainID
		
		IF  LEN(ISNULL(@vcOrderStatus,''))>0
		BEGIN
			SET @WHERE = @WHERE + ' and isnull(Opp.numStatus,0) in (SELECT Items FROM dbo.Split(''' +@vcOrderStatus + ''','',''))' 			
		END

		IF LEN(ISNULL(@vcShippingService,''))>0
		BEGIN
			SET @WHERE = @WHERE + ' and isnull(Opp.numShippingService,0) in (SELECT Items FROM dbo.Split(''' +@vcShippingService + ''','',''))'
		END

		IF ISNULL(@numShippingZone,0) > 0
		BEGIN
			SET @WHERE = @WHERE + ' AND 1 = (CASE 
												WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)),0) > 0 
												THEN
													CASE 
														WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=' + CAST(@numDomainId AS VARCHAR) + ' AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)) AND numShippingZone=' + CAST(@numShippingZone as VARCHAR) + ' ) > 0 
														THEN 1
														ELSE 0
													END
												ELSE 0 
											END)'
		END
		
		IF LEN(ISNULL(@vcOrderSource,''))>0
		BEGIN
				SET @WHERE = @WHERE + ' and (' + @vcOrderSource +')' 			
		END
		
		SET @WHERE = @WHERE + ' AND (Opp.numDivisionID ='+ CONVERT(VARCHAR(15), @numDivisionID) + ' OR '+ CONVERT(VARCHAR(15), @numDivisionID) + ' = 0) '
		
        SET @strSql = CONCAT(@SELECT,@strColumns,@FROM,@WHERE,' ORDER BY ',@SortCol,' ',@SortDirection,' OFFSET ',((@CurrentPage - 1) * @PageSize),' ROWS FETCH NEXT ',@PageSize, ' ROWS ONLY) SELECT * INTO #temp FROM [bizdocs];')
        
		SET @strSql = @strSql + CONCAT('SELECT 
											OM.*
											,CAST((Case WHEN ISNULL(OI.numoppitemtCode,0)>0 then 1 else 0 end) as bit) as IsShippingServiceItemID
										FROM #temp OM 
											LEFT JOIN (SELECT numOppId,numoppitemtCode,ISNULL(vcItemDesc, '''') vcItemDesc,monTotAmount,
											Row_number() OVER(PARTITION BY numOppId ORDER BY numoppitemtCode) AS row 
											FROM OpportunityItems OI WHERE OI.numOppId in (SELECT numOppId FROM #temp) AND numItemCode=',@numShippingServiceItemID,') 
											OI ON OI.row=1 AND OM.numOppId = OI.numOppId; 
											
											SELECT 
											opp.numOppId
											,Opp.vcitemname AS vcItemName
											,ISNULL(dbo.FormatedDateFromDate(opp.ItemReleaseDate,',@numDomainId,'),'''')  AS vcItemReleaseDate
											,ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=',@numDomainId,' AND ItemImages.numItemCode=i.numItemCode AND bitDefault=1 AND bitIsImage=1),'''') AS vcImage
											,CASE 
												WHEN charitemType = ''P'' THEN ''Product''
												WHEN charitemType = ''S'' THEN ''Service''
											END AS charitemType
											,CASE WHEN charitemType = ''P'' THEN ''Inventory''
												WHEN charitemType = ''N'' THEN ''Non-Inventory''
												WHEN charitemType = ''S'' THEN ''Service''
											END AS vcItemType
											,CASE WHEN ISNULL(numItemGroup,0) > 0 THEN WI.vcWHSKU ELSE i.vcSKU END vcSKU
											,dbo.USP_GetAttributes(opp.numWarehouseItmsID,bitSerialized) AS vcAttributes
											,ISNULL(W.vcWareHouse, '''') AS Warehouse,WL.vcLocation,opp.numUnitHour AS numUnitHourOrig
											,Opp.bitDropShip AS DropShip
											,SUBSTRING((SELECT  
															'' ,'' + vcSerialNo
																				+ CASE WHEN ISNULL(I.bitLotNo, 0) = 1
																					   THEN ''(''
																							+ CONVERT(VARCHAR(15), oppI.numQty)
																							+ '')''
																					   ELSE ''''
																				  END
														FROM    OppWarehouseSerializedItem oppI
																JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
														WHERE   oppI.numOppID = Opp.numOppId
																AND oppI.numOppItemID = Opp.numoppitemtCode
														ORDER BY vcSerialNo
														FOR
														XML PATH('''')
														), 3, 200000) AS SerialLotNo
											,ISNULL(bitSerialized,0) AS bitSerialized
											,ISNULL(bitLotNo,0) AS bitLotNo
											,Opp.numWarehouseItmsID
											,opp.numoppitemtCode
										FROM 
											OpportunityItems opp 
										JOIN 
											#temp OM 
										ON 
											opp.numoppid=OM.numoppid 
										LEFT JOIN 
											Item i 
										ON 
											opp.numItemCode = i.numItemCode
										INNER JOIN 
											dbo.WareHouseItems WI 
										ON 
											opp.numWarehouseItmsID = WI.numWareHouseItemID
											AND WI.numDomainID = ',@numDomainId,'
										INNER JOIN 
											dbo.Warehouses W 
										ON 
											WI.numDomainID = W.numDomainID
											AND WI.numWareHouseID = W.numWareHouseID
										LEFT JOIN 
											dbo.WarehouseLocation WL 
											ON WL.numWLocationID= WI.numWLocationID;
			
										DROP TABLE #temp;')

        PRINT CAST(@strSql AS NTEXT);
        EXEC ( @strSql ) ;
       
      
        
        /*Get total count */
        SET @strSql =  ' (SELECT @TotRecs =COUNT(*) ' + @FROM + @WHERE + ')';
        
        PRINT @strSql ;
        EXEC sp_executesql 
        @query = @strSql, 
        @params = N'@TotRecs INT OUTPUT', 
        @TotRecs = @TotRecs OUTPUT 
        
		SELECT * FROM #tempForm
		DROP TABLE #tempForm
    END
  
--created by anoop jayaraj
--	EXEC USP_GEtSFItemsForImporting 72,'76,318,538,758,772,1061,11409,11410,11628,11629,17089,18151,18193,18195,18196,18197',1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getsfitemsforimporting')
DROP PROCEDURE usp_getsfitemsforimporting
GO
CREATE PROCEDURE USP_GEtSFItemsForImporting
    @numDomainID AS NUMERIC(9),
	@numBizDocId	AS BIGINT,
	@vcBizDocsIds		VARCHAR(MAX),
	@tintMode AS TINYINT 
AS
BEGIN
---------------------------------------------------------------------------------
    DECLARE  @strSql  AS VARCHAR(8000)
	DECLARE  @strFrom  AS VARCHAR(2000)

	SET @vcBizDocsIds = STUFF((SELECT
									CONCAT(',',numOppBizDocsId)
								FROM
									OpportunityBizDocs
								WHERE
									numOppId IN (SELECT Id FROM dbo.SplitIDs(@vcBizDocsIds,','))
									AND numBizDocId = ISNULL(@numBizDocId,0)
								FOR XML PATH('')),1,1,'')

IF @tintMode=1 --PrintPickList
BEGIN
   SELECT
		ISNULL(vcItemName,'') AS vcItemName,
		ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=187 AND bitDefault=1 AND bitIsImage=1),'') AS vcImage,
		ISNULL(vcModelID,'') AS vcModelID,
		ISNULL(txtItemDesc,'') AS txtItemDesc,
		ISNULL(vcWareHouse,'') AS vcWareHouse,
		ISNULL(vcLocation,'') AS vcLocation,
		(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcWHSKU ELSE Item.vcSKU END) AS vcSKU,
		(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcBarCode ELSE Item.numBarCodeId END) AS vcUPC,
		SUM(numUnitHour) AS numUnitHour
	FROM
		OpportunityBizDocItems
	INNER JOIN
		WareHouseItems
	ON
		OpportunityBizDocItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
	INNER JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	INNER JOIN
		Item
	ON
		OpportunityBizDocItems.numItemCode = Item.numItemCode
	WHERE
		numOppBizDocID IN ( SELECT * FROM dbo.splitIds(@vcBizDocsIds,','))
	GROUP BY
		vcItemName,
		vcModelID,
		txtItemDesc,
		vcWareHouse,
		vcLocation,
		(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcWHSKU ELSE Item.vcSKU END),
		(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcBarCode ELSE Item.numBarCodeId END)
	ORDER BY
		vcWareHouse ASC,
		vcLocation ASC,
		vcItemName ASC
END
ELSE IF @tintMode=2 --PrintPackingSlip
BEGIN
		  
		SELECT	
				ROW_NUMBER() OVER(ORDER BY OM.numOppID ASC, OI.numSortOrder ASC) AS SRNO,
				OM.numOppId,
				OM.numDomainID,
				OM.tintTaxOperator,
				I.numItemCode,
				I.vcModelID,
				OM.vcPOppName,
				cast((OBDI.monTotAmtBefDiscount - OBDI.monTotAMount) as varchar(20)) + Case When isnull(OM.fltDiscount,0)> 0 then  '(' + cast(OM.fltDiscount as varchar(10)) + '%)' else '' end AS DiscAmt,
				OBDI.dtRentalStartDate,
				OBDI.dtRentalReturnDate,
				OBDI.monShipCost,
				OBDI.vcShippingMethod,
				OBDI.dtDeliveryDate,
				numItemClassification,
				OBDI.vcNotes,
				OBD.vcTrackingNo,
				I.vcManufacturer,
				--vcItemClassification,
				CASE 
					WHEN charItemType='P' 
					THEN 'Inventory Item' 
					WHEN charItemType='S' 
					THEN 'Service' 
					WHEN charItemType='A' 
					THEN 'Accessory' 
					WHEN charItemType='N' 
					THEN 'Non-Inventory Item' 
				END AS charItemType,charItemType AS [Type],
				OBDI.vcAttributes,
				OI.[numoppitemtCode] ,
				OI.vcItemName,
				OBDI.numUnitHour,
				OI.[numQtyShipped],
				OBDI.vcItemDesc AS txtItemDesc,
				vcUnitofMeasure AS numUOMId,
				monListPrice AS [Price],
				CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4), OBDI.monTotAmount)) Amount,
				CASE 
					WHEN ISNULL(OBD.numShipVia,0) = 0 THEN (CASE WHEN OM.intUsedShippingCompany IS NULL THEN '-' WHEN OM.intUsedShippingCompany = -1 THEN 'Will-call' ELSE dbo.fn_GetListItemName(OM.intUsedShippingCompany) END)
					WHEN OBD.numShipVia = -1 THEN 'Will-call'
					ELSE dbo.fn_GetListItemName(OBD.numShipVia)
				END AS ShipVia,
				ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = OM.numShippingService),'') AS vcShippingService,
				(CASE 
					WHEN ISNULL(OBD.numSourceBizDocId,0) > 0
					THEN ISNULL((SELECT vcBizDocID FROM OpportunityBizDocs WHERE OpportunityBizDocs.numOppBizDocsId=OBD.numSourceBizDocId),'')
					ELSE ''
				END) vcPackingSlip,
				OBDI.monPrice,
				OBDI.monTotAmount,
				vcWareHouse,
				vcLocation,
				vcWStreet,
				vcWCity,
				vcWPinCode,
				(SELECT vcState FROM State WHERE numStateID = W.numWState) AS [vcState],
				dbo.fn_GetListItemName(W.numWCountry) AS [vcWCountry],							
				vcWHSKU,
				vcBarCode,
				SUBSTRING((SELECT ',' + vcSerialNo + CASE 
															WHEN isnull(I.bitLotNo,0)=1 
															THEN ' (' + CONVERT(VARCHAR(15),SERIALIZED_ITEM.numQty) + ')' 
															ELSE '' 
													 END 
							FROM OppWarehouseSerializedItem SERIALIZED_ITEM 
							JOIN WareHouseItmsDTL W_ITEM_DETAIL ON SERIALIZED_ITEM.numWareHouseItmsDTLID = W_ITEM_DETAIL.numWareHouseItmsDTLID 
							WHERE SERIALIZED_ITEM.numOppID = OI.numOppId 
							  AND SERIALIZED_ITEM.numOppItemID = OI.numoppitemtCode 
							  and SERIALIZED_ITEM.numOppBizDocsID=OBD.numOppBizDocsId
							ORDER BY vcSerialNo 
						  FOR XML PATH('')),2,200000) AS SerialLotNo,
				isnull(OBD.vcRefOrderNo,'') AS vcRefOrderNo,
				isnull(OM.bitBillingTerms,0) AS tintBillingTerms,
				isnull(intBillingDays,0) AS numBillingDays,
--				CASE 
--					WHEN ISNUMERIC(ISNULL(dbo.fn_GetListItemName(ISNULL(intBillingDays,0)),0)) = 1 
--					THEN ISNULL(dbo.fn_GetListItemName(ISNULL(intBillingDays,0)),0) 
--					ELSE 0 
--				END AS numBillingDaysName,
				CASE WHEN ISNUMERIC((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0))) = 1
					 THEN (SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0))
					 ELSE 0
				END AS numBillingDaysName,	 
				ISNULL(bitInterestType,0) AS tintInterestType,
				tintOPPType,
				dbo.fn_GetContactName(numApprovedBy) AS ApprovedBy,
				dtApprovedDate,
				OM.bintAccountClosingDate,
				tintShipToType,
				tintBillToType,
				tintshipped,
				dtShippedDate bintShippedDate,
				OM.numDivisionID,							
				ISNULL(numShipVia,0) AS numShipVia,
				ISNULL(vcTrackingURL,'') AS vcTrackingURL,
				ISNULL(C.varCurrSymbol,'') varCurrSymbol,
				isnull(bitPartialFulfilment,0) bitPartialFulfilment,
				dbo.[fn_GetContactName](OM.[numRecOwner])  AS OrderRecOwner,
				dbo.[fn_GetContactName](DM.[numRecOwner]) AS AccountRecOwner,
				dbo.fn_GetContactName(OM.numAssignedTo) AS AssigneeName,
				ISNULL((SELECT vcEmail FROM dbo.AdditionalContactsInformation WHERE numContactID = OM.numAssignedTo),'') AS AssigneeEmail,
				ISNULL((SELECT numPhone FROM dbo.AdditionalContactsInformation WHERE numContactID = OM.numAssignedTo),'') AS AssigneePhone,
				dbo.fn_GetComapnyName(OM.numDivisionId) OrganizationName,
				DM.vcComPhone as OrganizationPhone,
				dbo.fn_GetContactName(OM.numContactID)  OrgContactName,
				dbo.getCompanyAddress(OM.numDivisionId,1,OM.numDomainId) CompanyBillingAddress,
				CASE 
					 WHEN ACI.numPhone<>'' 
					 THEN ACI.numPhone + CASE 
										    WHEN ACI.numPhoneExtension<>'' 
											THEN ' - ' + ACI.numPhoneExtension 
											ELSE '' 
										 END  
					 ELSE '' 
				END OrgContactPhone,
				ISNULL(ACI.vcEmail,'') AS OrgContactEmail,
				dbo.[fn_GetContactName](OM.[numRecOwner]) AS OnlyOrderRecOwner,
				(SELECT TOP 1 SD.vcSignatureFile FROM SignatureDetail SD 
												 JOIN OpportunityBizDocsDetails OBD ON SD.numSignID = OBD.numSignID 
												WHERE SD.numDomainID = @numDomainID 
												  AND OBD.numDomainID = @numDomainID 
												  AND OBD.numSignID IS not null 
				ORDER BY OBD.numBizDocsPaymentDetId DESC) AS vcSignatureFile,
				ISNULL(CMP.txtComments,'') AS vcOrganizationComments,
				OBD.vcRefOrderNo AS [P.O.No],
				OM.txtComments AS [Comments],
				D.vcBizDocImagePath
				,dbo.FormatedDateFromDate(OM.dtReleaseDate,@numDomainID) AS vcReleaseDate
				,(CASE WHEN ISNULL(OM.numPartner,0) > 0 THEN ISNULL((SELECT CONCAT(DivisionMaster.vcPartnerCode,'-',CompanyInfo.vcCompanyName) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE DivisionMaster.numDivisionID=OM.numPartner),'') ELSE '' END) AS vcPartner,
				ISNULL(vcCustomerPO#,'') vcCustomerPO#,
				ISNULL(OM.txtComments,'') vcSOComments,
				dbo.GetOrderAssemblyKitInclusionDetails(OI.numOppID,OI.numoppitemtCode,OBDI.numUnitHour,ISNULL(D.tintCommitAllocation,1),1) AS vcInclusionDetails,
				ISNULL(DM.vcShippersAccountNo,'') AS vcShippersAccountNo,
				dbo.FormatedDateFromDate(OM.bintCreatedDate,@numDomainID) As OrderCreatedDate,
				(CASE WHEN ISNULL(OBD.numSourceBizDocId,0) > 0 THEN ISNULL((SELECT vcVendorInvoice FROM OpportunityBizDocs WHERE OpportunityBizDocs.numOppBizDocsId=OBD.numSourceBizDocId),'') ELSE ISNULL(vcVendorInvoice,'') END) vcVendorInvoice
	INTO #temp_Packing_List
	FROM OpportunityMaster AS OM
	join OpportunityBizDocs OBD on OM.numOppID=OBD.numOppID
	JOIN OpportunityBizDocItems OBDI on OBDI.numOppBizDocID=OBD.numOppBizDocsId
	JOIN OpportunityItems AS OI on OI.numoppitemtCode=OBDI.numOppItemID and OI.numoppID=OI.numOppID
	JOIN Item AS I ON OI.numItemCode = I.numItemCode AND I.numDomainID = @numDomainID
	LEFT OUTER JOIN WareHouseItems WI ON I.numItemCode = WI.numItemID AND WI.numWareHouseItemID = OBDI.numWarehouseItmsID
	LEFT OUTER JOIN Warehouses W ON WI.numWareHouseID = W.numWareHouseID 
	JOIN [DivisionMaster] DM ON DM.numDivisionID = OM.numDivisionID   
	JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = @numDomainID  
	JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = OM.numContactID
	JOIN Domain D ON D.numDomainID = OM.numDomainID
	LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = OM.numCurrencyID
    WHERE OM.numDomainID = @numDomainID 
	  AND OBD.[numOppBizDocsId] IN ( SELECT * FROM dbo.splitIds(@vcBizDocsIds ,',')) 
	
	DECLARE @numFldID AS INT    
	DECLARE @intRowNum AS INT    	
	DECLARE @vcFldname AS VARCHAR(MAX)	
	
	SET @strSQL = ''
	

	/**** START: Added Sales Tax Column ****/

	DECLARE @strSQLUpdate AS VARCHAR(2000);
	
	EXEC ( 'ALTER TABLE #temp_Packing_List ADD [TotalTax] DECIMAL(20,5)'  )
	EXEC ( 'ALTER TABLE #temp_Packing_List ADD [CRV] DECIMAL(20,5)'  )


	DECLARE @i AS INT = 1
	DECLARE @Count AS INT 
	SELECT @Count = COUNT(*) FROM #temp_Packing_List

	DECLARE @numOppId AS BIGINT
	DECLARE @numOppItemCode AS BIGINT
	DECLARE @tintTaxOperator AS INT

	WHILE @i <= @Count
	BEGIN
		SET @strSQLUpdate=''

		SELECT 
			@numDomainID = numDomainID,
			@numOppId = numOppId,
			@tintTaxOperator = tintTaxOperator,
			@numOppItemCode = numoppitemtCode
		FROM
			#temp_Packing_List
		WHERE
			SRNO = @i


		IF @tintTaxOperator = 1 
		BEGIN
			 SET @strSQLUpdate = @strSQLUpdate
				+ ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ',0,'
				+ CONVERT(VARCHAR(20), @numOppId) + ','
				+ CONVERT(VARCHAR(20), @numOppItemCode) + ',1,Amount,numUnitHour)'

			SET @strSQLUpdate = @strSQLUpdate
				+ ',[CRV]= dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ',1,'
				+ CONVERT(VARCHAR(20), @numOppId) + ','
				+ CONVERT(VARCHAR(20), @numOppItemCode) + ',1,Amount,numUnitHour)'
		END
		ELSE IF @tintTaxOperator = 2 -- remove sales tax
		BEGIN
			SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
			SET @strSQLUpdate = @strSQLUpdate + ',[CRV]= 0'
		END
		ELSE
		BEGIN
			 SET @strSQLUpdate = @strSQLUpdate
				+ ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ',0,'
				+ CONVERT(VARCHAR(20), @numOppId) + ','
				+ CONVERT(VARCHAR(20), @numOppItemCode) + ',1,Amount,numUnitHour)'

			SET @strSQLUpdate = @strSQLUpdate
				+ ',[CRV]= dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ',1,'
				+ CONVERT(VARCHAR(20), @numOppId) + ','
				+ CONVERT(VARCHAR(20), @numOppItemCode) + ',1,Amount,numUnitHour)'
		END

		IF @strSQLUpdate <> '' 
		BEGIN
			SET @strSQLUpdate = 'UPDATE #temp_Packing_List SET numItemCode=numItemCode' + @strSQLUpdate + ' WHERE numItemCode > 0 AND numOppId=' + CONVERT(VARCHAR(20), @numOppId) + ' AND numoppitemtCode=' + CONVERT(VARCHAR(20), @numOppItemCode)
			PRINT @strSQLUpdate
			EXEC (@strSQLUpdate)
		END

		SET @i = @i + 1
	END


	SET @strSQLUpdate = ''
	DECLARE @vcTaxName AS VARCHAR(100)
	DECLARE @numTaxItemID AS NUMERIC(9)
	SET @numTaxItemID = 0
	SELECT TOP 1 @vcTaxName = vcTaxName, @numTaxItemID = numTaxItemID FROM TaxItems WHERE numDomainID = @numDomainID

	WHILE @numTaxItemID > 0
	BEGIN

		EXEC ( 'ALTER TABLE #temp_Packing_List ADD [' + @vcTaxName + '] DECIMAL(20,5)' )

		SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName  + ']= dbo.fn_CalBizDocTaxAmt(numDomainID,'
						+ CONVERT(VARCHAR(20), @numTaxItemID) + ',numOppId,numoppitemtCode,1,Amount,numUnitHour)'
           

		SELECT TOP 1 @vcTaxName = vcTaxName, @numTaxItemID = numTaxItemID FROM TaxItems WHERE numDomainID = @numDomainID AND numTaxItemID > @numTaxItemID

		IF @@rowcount = 0 
			SET @numTaxItemID = 0
	END


	IF @strSQLUpdate <> '' 
	BEGIN
		SET @strSQLUpdate = 'UPDATE #temp_Packing_List SET numItemCode=numItemCode' + @strSQLUpdate + ' WHERE numItemCode > 0' 
		PRINT @strSQLUpdate
		EXEC (@strSQLUpdate)
	END

	/**** END: Added Sales Tax Column ****/
	
	PRINT 'START'                                                                                          
	PRINT 'QUERY : ' + @strSQL

	SELECT TOP 1 @numFldID = numFieldId,
				 @vcFldname = Fld_label,
				 @intRowNum = (intRowNum+1) 
	FROM DycFormConfigurationDetails DTL                                                                                                
	JOIN CFW_Fld_Master FIELD_MASTER ON FIELD_MASTER.Fld_id = DTL.numFieldId                                                                                                 
	WHERE DTL.numFormID = 7 
	  AND FIELD_MASTER.Grp_id = 5 
	  AND DTL.numDomainID = @numDomainID 
	  AND numAuthGroupID = @numBizDocId
	ORDER BY intRowNum

	PRINT @intRowNum
	WHILE @intRowNum > 0                                                                                  
	BEGIN    
			PRINT 'FROM LOOP'
			PRINT @vcFldname
			PRINT @numFldID

			EXEC('ALTER TABLE #temp_Packing_List add [' + @vcFldname + '] varchar(100)')
		
			SET @strSQL = 'UPDATE #temp_Packing_List SET [' + @vcFldname + '] = dbo.GetCustFldValueItem(' + CONVERT(VARCHAR(10),@numFldID) + ',numItemCode) 
						   WHERE numItemCode > 0'

			PRINT 'QUERY : ' + @strSQL
			EXEC (@strSQL)
			                                                                                 	                                                                                       
			SELECT TOP 1 @numFldID = numFieldId,
						 @vcFldname = Fld_label,
						 @intRowNum = (intRowNum + 1) 
			FROM DycFormConfigurationDetails DETAIL                                                                                                
			JOIN CFW_Fld_Master MASTER ON MASTER.Fld_id = DETAIL.numFieldId                                                                                                 
			WHERE DETAIL.numFormID = 7 
			  AND MASTER.Grp_id = 5 
			  AND DETAIL.numDomainID = @numDomainID 
			 AND numAuthGroupID = @numBizDocId                                                 
			  AND intRowNum >= @intRowNum 
			ORDER BY intRowNum                                                                  
			           
			IF @@rowcount=0 SET @intRowNum=0                                                                                                                                                
		END	
	PRINT 'END'	
	SELECT * FROM #temp_Packing_List ORDER BY SRNO
END
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetShippingBox')
DROP PROCEDURE USP_GetShippingBox
GO
CREATE PROCEDURE USP_GetShippingBox
    @ShippingReportId NUMERIC(9)
AS 
    BEGIN
        SELECT DISTINCT
                [numBoxID],
                [vcBoxName],
                [numShippingReportId],
                CASE WHEN [fltTotalWeight] = 0 THEN SUM([fltWeight]) ELSE [fltTotalWeight] END AS fltTotalWeight,
                [fltHeight],
                [fltWidth],
                [fltLength],
				dbo.FormatedDateFromDate(dtDeliveryDate,numDomainID) dtDeliveryDate,
				ISNULL(monShippingRate,0) [monShippingRate],
				vcShippingLabelImage,
				ISNULL(vcTrackingNumber,'') AS vcTrackingNumber,
				tintServiceType,--Your Packaging
				tintPayorType,
				vcPayorAccountNo,
				vcPayorZip,
				numPayorCountry,
				numPackageTypeID,
				ISNULL(vcPackageName,'') AS [vcPackageName],
				numServiceTypeID,
				CAST(ROUND(SUM(fltTotalRegularWeight), 2) AS DECIMAL(9,2)) AS [fltTotalWeightForShipping],
				fltDimensionalWeight AS [fltTotalDimensionalWeight],
				ISNULL(fltDimensionalWeight,0) AS [fltDimensionalWeight],
				ISNULL(numShipCompany,0) AS [numShipCompany],
				numOppID
--				,intBoxQty
				--ShippingReportItemId
        FROM    View_ShippingBox
        WHERE   [numShippingReportId] = @ShippingReportId
        GROUP BY [numBoxID],
                [vcBoxName],
                [numShippingReportId],
                [fltHeight],
                [fltWidth],
                [fltLength],
                fltTotalWeight,dtDeliveryDate,numOppID,View_ShippingBox.numOppBizDocID,
				monShippingRate,vcShippingLabelImage,
				vcTrackingNumber,numDomainID,
				tintServiceType,tintPayorType,
				vcPayorAccountNo,
				vcPayorZip,
				numPayorCountry,numPackageTypeID,vcPackageName,numServiceTypeID,fltDimensionalWeight,numShipCompany--,intBoxQty
				                
        SELECT  [numBoxID],
                [vcBoxName],
                [ShippingReportItemId],
                [numShippingReportId],
                fltTotalWeightItem [fltTotalWeight],
                fltHeightItem [fltHeight],
                fltWidthItem [fltWidth],
                fltLengthItem [fltLength],
                [numOppBizDocItemID],
                [numItemCode],
                [numoppitemtCode],
                ISNULL([vcItemName],'') vcItemName,
                [vcModelID],
                CASE WHEN LEN (ISNULL(vcItemDesc,''))> 100 THEN  CONVERT(VARCHAR(100),[vcItemDesc]) + '..'
                ELSE ISNULL(vcItemDesc,'') END vcItemDesc,
                intBoxQty,
                numServiceTypeID,
                vcUnitName,monUnitPrice,
				numOppID
        FROM    [View_ShippingBox]
        WHERE   [numShippingReportId] = @ShippingReportId       
                
                
    END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getshippingexceptionsfororder')
DROP PROCEDURE usp_getshippingexceptionsfororder
GO
CREATE PROCEDURE [dbo].[USP_GetShippingExceptionsForOrder]
	 @numDomainID NUMERIC(18,0)
	,@vcItemCodes VARCHAR(MAX)
	,@ShippingAmt Float
	,@bitFreeShipping BIT
AS
BEGIN	

	--DECLARE @vcItemCodes VARCHAR(MAX) = '8,240'
  
  DECLARE @tempExceptions TABLE
    (
		SNO INT IDENTITY(1,1),
		vcItemName VARCHAR(200),
		bitApplyException BIT,
		FlatAmt FLOAT,
		numItemCode NUMERIC
    )


	INSERT INTO @tempExceptions (vcItemName, bitApplyException, FlatAmt, numItemCode)
		SELECT '',0,0, id 
		FROM  dbo.SplitIDs(@vcItemCodes, ',')  
	
--select * from 	@tempExceptions	

	DECLARE @totalRecords INT
	DECLARE @I INT

	SELECT @I = 1
	SELECT @totalRecords = COUNT(numItemCode) FROM @tempExceptions
	WHILE (@I <= @totalRecords)
	BEGIN
		DECLARE @numShipClass NUMERIC

		SET @numShipClass = (SELECT I.numShipClass FROM @tempExceptions t
								INNER JOIN Item I ON I.numItemCode = t.numItemCode WHERE SNO = @I )

		UPDATE @tempExceptions
		SET vcItemName = (SELECT I.vcItemName FROM @tempExceptions t
								INNER JOIN Item I ON I.numItemCode = t.numItemCode WHERE SNO = @I),

		bitApplyException = (SELECT CASE WHEN  (SELECT COUNT(numClassificationID) FROM ShippingExceptions WHERE numDomainID = @numDomainID AND numClassificationID = @numShipClass) > 0 
											THEN 1
											ELSE 0 END
											),
		FlatAmt = (SELECT 
						CASE WHEN @ShippingAmt > 0 THEN
							(SELECT (@ShippingAmt * PercentAbove)/100 FROM ShippingExceptions WHERE numDomainID = @numDomainID)
						WHEN @ShippingAmt = 0 AND @bitFreeShipping = 1 THEN
						(SELECT FlatAmt FROM ShippingExceptions WHERE numDomainID = @numDomainID)
						ELSE 0 END )
		WHERE SNO = @I

	SET @I = @I + 1 

	END						
				
		SELECT vcItemName, bitApplyException, FlatAmt, numItemCode FROM @tempExceptions WHERE bitApplyException = 1
END 
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetShippingRule' ) 
    DROP PROCEDURE USP_GetShippingRule
GO
CREATE PROCEDURE USP_GetShippingRule
    @numRuleID NUMERIC(9),
    @numDomainID NUMERIC(9),
    @byteMode AS TINYINT,
	@numRelProfileID AS VARCHAR(50)
AS 
BEGIN
    IF @byteMode = 0 
        BEGIN  
            SELECT  numRuleID
                    ,vcRuleName
                    ,vcDescription
                    ,tintBasedOn
                    ,tinShippingMethod
                    ,tintIncludeType
                    ,tintTaxMode
                    ,CASE WHEN tintFixedShippingQuotesMode = 0 THEN 1
                             ELSE tintFixedShippingQuotesMode END 
						AS tintFixedShippingQuotesMode
					,bitFreeShipping
					,FreeShippingOrderAmt
					,numRelationship
					,numProfile
					,numWareHouseID
                    
            FROM    dbo.ShippingRules SR
            WHERE   ( numRuleID = @numRuleID
                      OR @numRuleID = 0
                    )
                    AND numDomainID = @numDomainID
        END
    ELSE IF @byteMode = 1 
    BEGIN 
		DECLARE @numRelationship AS NUMERIC
		DECLARE @numProfile AS NUMERIC
				
		SET @numRelationship=(parsename(replace(@numRelProfileID,'-','.'),2))
		SET @numProfile=(parsename(replace(@numRelProfileID,'-','.'),1))

		SELECT minShippingCost,bitEnableStaticShippingRule,bitEnableShippingExceptions FROM Domain WHERE numDomainID = @numDomainID

		IF @numRelationship > 0 AND @numProfile > 0
		BEGIN
			   SELECT  numRuleID,
                vcRuleName,
                tinShippingMethod,
                CASE WHEN tintTaxMode = 1 THEN 'Do Not Tax'
                        WHEN tintTaxMode = 2 THEN 'Tax when taxable items exists'
                        WHEN tintTaxMode = 3 THEN 'Always Tax'
                        WHEN tintTaxMode = 0 THEN 'Do Not Tax'
                        ELSE 'Tax Mode Not Selected'
                END vcTaxMode,
                CASE WHEN tintFixedShippingQuotesMode= 1 THEN 'Flat rate per item'
                        WHEN tintFixedShippingQuotesMode = 2 THEN 'Ship by weight'
                        WHEN tintFixedShippingQuotesMode = 3 THEN 'Ship by order total'
                        WHEN tintFixedShippingQuotesMode = 4 THEN 'Flat rate per order'
                        WHEN tintFixedShippingQuotesMode= 0 THEN 'Flat rate per item'
                        ELSE 'Fixed rate shipping quotes not selected'
                END  vcFixedShippingQuotesMode
				,(SELECT STUFF((SELECT ', ' + W.vcWareHouse
					FROM   Warehouses W			
					WHERE   W.numWareHouseID  in  ( SELECT  Id
							FROM    dbo.SplitIDs(SR.numWareHouseID, ','))			
					FOR XML PATH ('')),1,2,'')) AS vcShipFrom
                , (SELECT STUFF((SELECT ', ' + dbo.fn_GetState(numStateID) + ' ,' + SS.vcZipPostal
					FROM ShippingRuleStateList SS
					join ShippingRules S on ss.numRuleID = s.numRuleID
					WHERE SR.numRuleID = SS.numRuleID			
					FOR XML PATH ('')),1,2,'')) AS vcShipTo

					,(CASE WHEN ISNULL(SR.bitFreeShipping,0) = 1
						THEN  (SELECT CONCAT ( (SELECT STUFF((SELECT CONCAT (', ' , intfrom , ' - ', intTo, ' pays $',  CAST(monRate AS DECIMAL(20,2)))
								FROM ShippingServiceTypes SS
								join ShippingRules S on ss.numRuleID = s.numRuleID
								WHERE SR.numRuleID = SS.numRuleID			
								FOR XML PATH ('')),1,2,'')) , ' , Free shipping when order amount reaches $' , SR.FreeShippingOrderAmt))
					ELSE (SELECT STUFF((SELECT CONCAT (', ' , intfrom , ' - ', intTo, ' pays $',  CAST(monRate AS DECIMAL(20,2)))
								FROM ShippingServiceTypes SS
								join ShippingRules S on ss.numRuleID = s.numRuleID
								WHERE SR.numRuleID = SS.numRuleID			
								FOR XML PATH ('')),1,2,'')) 
					END ) AS vcShippingCharges
				, (SELECT CONCAT( (SELECT vcData from ListDetails LD 
					JOIN  ShippingRules S ON LD.numListItemID = S.numRelationship 
					WHERE LD.numListItemID = S.numRelationship AND S.numRuleID = SR.numRuleID) , ', ' , (SELECT vcData from ListDetails LD 
					JOIN  ShippingRules S ON LD.numListItemID = S.numProfile 
					WHERE LD.numListItemID = S.numProfile  AND S.numRuleID = SR.numRuleID))) AS vcAppliesTo	
                        
        FROM    dbo.ShippingRules SR
        WHERE  
                numDomainID = @numDomainID AND numRelationship = @numRelationship AND numprofile = @numProfile
		END

		ELSE
		BEGIN
			   SELECT  numRuleID,
                vcRuleName,
                tinShippingMethod,
                CASE WHEN tintTaxMode = 1 THEN 'Do Not Tax'
                        WHEN tintTaxMode = 2 THEN 'Tax when taxable items exists'
                        WHEN tintTaxMode = 3 THEN 'Always Tax'
                        WHEN tintTaxMode = 0 THEN 'Do Not Tax'
                        ELSE 'Tax Mode Not Selected'
                END vcTaxMode,
                CASE WHEN tintFixedShippingQuotesMode= 1 THEN 'Flat rate per item'
                        WHEN tintFixedShippingQuotesMode = 2 THEN 'Ship by weight'
                        WHEN tintFixedShippingQuotesMode = 3 THEN 'Ship by order total'
                        WHEN tintFixedShippingQuotesMode = 4 THEN 'Flat rate per order'
                        WHEN tintFixedShippingQuotesMode= 0 THEN 'Flat rate per item'
                        ELSE 'Fixed rate shipping quotes not selected'
                END  vcFixedShippingQuotesMode

				,(SELECT STUFF((SELECT ', ' + W.vcWareHouse
					FROM   Warehouses W			
					WHERE   W.numWareHouseID  in  ( SELECT  Id
							FROM    dbo.SplitIDs(SR.numWareHouseID, ','))			
					FOR XML PATH ('')),1,2,'')) AS vcShipFrom

                ,  
					STUFF((SELECT  ', ' + dbo.fn_GetListName(numCountryID,0) + ' - ' + 
						STUFF((SELECT ', ' + ISNULL(dbo.fn_GetState(numStateID),'All States') +
								(CASE WHEN LEN( STUFF((SELECT ',' + s.vcZipPostal From ShippingRuleStateList s
														WHERE SR.numRuleID = s.numRuleID AND s.numCountryID =SRSL.numCountryID and s.numStateID = SRSL.numStateID FOR XML PATH ('')),1,1,'')) > 0
						THEN CONCAT (' (',STUFF((SELECT ',' + s.vcZipPostal From ShippingRuleStateList s
															WHERE SR.numRuleID = s.numRuleID AND s.numCountryID =SRSL.numCountryID and s.numStateID = SRSL.numStateID FOR XML PATH ('')),1,1,''), ') ')
									ELSE ''
								END)
						From ShippingRuleStateList SRSL
						WHERE SR.numRuleID = SRSL.numRuleID AND SRSL.numCountryID =SS.numCountryID GROUP BY SRSL.numStateID,numCountryID FOR XML PATH ('')),1,1,'')
										
					FROM ShippingRuleStateList SS					
					WHERE SR.numRuleID = SS.numRuleID
					GROUP BY numCountryID
					FOR XML PATH ('')),1,1,'') AS vcShipTo

				,(CASE WHEN ISNULL(SR.bitFreeShipping,0) = 1
						THEN  (SELECT CONCAT ( (SELECT STUFF((SELECT CONCAT (', ' , intfrom , ' - ', intTo, ' pays $',  CAST(monRate AS DECIMAL(20,2)))
								FROM ShippingServiceTypes SS
								join ShippingRules S on ss.numRuleID = s.numRuleID
								WHERE SR.numRuleID = SS.numRuleID			
								FOR XML PATH ('')),1,2,'')) , ' , Free shipping when order amount reaches $' , SR.FreeShippingOrderAmt))
					ELSE (SELECT STUFF((SELECT CONCAT (', ' , intfrom , ' - ', intTo, ' pays $', CAST(monRate AS DECIMAL(20,2)))
								FROM ShippingServiceTypes SS
								join ShippingRules S on ss.numRuleID = s.numRuleID
								WHERE SR.numRuleID = SS.numRuleID			
								FOR XML PATH ('')),1,2,'')) 
					END ) AS vcShippingCharges
				,(SELECT CONCAT(ISNULL((SELECT vcData from ListDetails LD 
					JOIN  ShippingRules S ON LD.numListItemID = S.numRelationship 
					WHERE LD.numListItemID = S.numRelationship AND S.numRuleID = SR.numRuleID),'All Relationships') , ', ' ,ISNULL((SELECT vcData from ListDetails LD 
					JOIN  ShippingRules S ON LD.numListItemID = S.numProfile 
					WHERE LD.numListItemID = S.numProfile  AND S.numRuleID = SR.numRuleID),'All Profiles'))) AS vcAppliesTo	
                         
        FROM    dbo.ShippingRules SR
        WHERE  
                numDomainID = @numDomainID  
		END

    END 
      ELSE IF @byteMode = 2 --It is for calculating Tax on Shipping Charge (Use it later on Order Summery Page)
            BEGIN
				SELECT tintTaxMode FROM ShippingRules WHERE numRuleID = @numRuleID AND numDomainID = @numDomainID
			END
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getshippingruledetails')
DROP PROCEDURE usp_getshippingruledetails
GO
CREATE PROCEDURE [dbo].[USP_GetShippingRuleDetails]
	 @numDomainID NUMERIC(18,0)
	,@numWareHouseId VARCHAR(20)
	,@ZipCode VARCHAR(20)
	,@StateId NUMERIC(18,0)	
	,@numDivisionID NUMERIC(18,0)
	,@numSiteID NUMERIC(18,0)
	,@numCountryId NUMERIC(18,0)	
AS
BEGIN	
	DECLARE @numRuleID AS NUMERIC

	IF (SELECT ISNULL(bitEnableStaticShippingRule,0) FROM Domain WHERE numDomainID = @numDomainID) = 1
	BEGIN

		IF ISNULL(@numSiteID,0) > 0 AND ISNULL(@numDivisionID,0) = 0
																																													BEGIN
			DECLARE @numRelationship NUMERIC(18,0)
			DECLARE @numProfile NUMERIC(18,0)

			SELECT 
				@numRelationship=ISNULL(numRelationshipId,0)
				,@numProfile=ISNULL(numProfileId,0)
			FROM 
				eCommerceDTL 
			WHERE 
				numSiteId=@numSiteID

			SET @numRuleID = (SELECT TOP 1 
								SR.numRuleID		 
							FROM 
								ShippingRules SR						
							INNER JOIN 
								ShippingRuleStateList SRS
							ON 
								SR.numRuleID = SRS.numRuleID
							WHERE 
								(SR.numProfile = @numProfile OR ISNULL(SR.numProfile,0) = 0)
								AND (SR.numRelationship = @numRelationship OR ISNULL(SR.numRelationship,0) = 0)
								AND (CHARINDEX(@numWareHouseId, SR.numWareHouseID) > 0)
								AND SR.numDomainId = @numDomainID
								AND ISNULL(SRS.numCountryID,0)=ISNULL(@numCountryId,0)
								AND (SRS.numStateID = @StateId OR ISNULL(SRS.numStateID,0) = 0)
								AND (ISNULL(SRS.vcZipPostal,'') = '' OR (@ZipCode LIKE (SRS.vcZipPostal + '%')))
							ORDER BY
								(CASE 
									WHEN ISNULL(SRS.numCountryID,0) > 0 AND ISNULL(SRS.numStateID,0) > 0 AND LEN(ISNULL(SRS.vcZipPostal,'')) > 0 THEN 1
									WHEN ISNULL(SRS.numCountryID,0) > 0 AND ISNULL(SRS.numStateID,0) = 0 AND LEN(ISNULL(SRS.vcZipPostal,'')) > 0 THEN 2
									WHEN ISNULL(SRS.numCountryID,0) > 0 AND ISNULL(SRS.numStateID,0) > 0 AND LEN(ISNULL(SRS.vcZipPostal,'')) = 0 THEN 3
									WHEN ISNULL(SRS.numCountryID,0) > 0 AND ISNULL(SRS.numStateID,0) = 0 AND LEN(ISNULL(SRS.vcZipPostal,'')) = 0 THEN 4
									ELSE 5
								END),
								(CASE
									WHEN ISNULL(SR.numRelationship,0) > 0 AND ISNULL(SR.numProfile,0) > 0 THEN 1
									WHEN ISNULL(SR.numRelationship,0) > 0 AND ISNULL(SR.numProfile,0) = 0 THEN 2
									WHEN ISNULL(SR.numRelationship,0) = 0 AND ISNULL(SR.numProfile,0) > 0 THEN 3
									ELSE 4
								END),
								LEN(ISNULL(SRS.vcZipPostal,''))
						)
		END
			ELSE
																																												BEGIN
			SET @numRuleID = (SELECT TOP 1 
								SR.numRuleID		 
							FROM 
								ShippingRules SR	
							INNER JOIN 
								CompanyInfo C
							ON 
								(SR.numProfile = C.vcProfile OR ISNULL(SR.numProfile,0) = 0) 
								AND (SR.numRelationship = C.numCompanyType OR ISNULL(SR.numRelationship,0) = 0)					
							INNER JOIN 
								ShippingRuleStateList SRS
							ON 
								SR.numRuleID = SRS.numRuleID
							INNER JOIN 
								DivisionMaster DM
							ON 
								C.numCompanyId = DM.numCompanyID
							WHERE 
								DM.numDivisionID = @numDivisionID
								AND (CHARINDEX(@numWareHouseId, SR.numWareHouseID) > 0)
								AND SR.numDomainId = @numDomainID 
								AND ISNULL(SRS.numCountryID,0)=ISNULL(@numCountryId,0)
								AND (SRS.numStateID = @StateId OR ISNULL(SRS.numStateID,0) = 0)
								AND (ISNULL(SRS.vcZipPostal,'') = '' OR (@ZipCode LIKE (SRS.vcZipPostal + '%')))
							ORDER BY
								(CASE 
									WHEN ISNULL(SRS.numCountryID,0) > 0 AND ISNULL(SRS.numStateID,0) > 0 AND LEN(ISNULL(SRS.vcZipPostal,'')) > 0 THEN 1
									WHEN ISNULL(SRS.numCountryID,0) > 0 AND ISNULL(SRS.numStateID,0) = 0 AND LEN(ISNULL(SRS.vcZipPostal,'')) > 0 THEN 2
									WHEN ISNULL(SRS.numCountryID,0) > 0 AND ISNULL(SRS.numStateID,0) > 0 AND LEN(ISNULL(SRS.vcZipPostal,'')) = 0 THEN 3
									WHEN ISNULL(SRS.numCountryID,0) > 0 AND ISNULL(SRS.numStateID,0) = 0 AND LEN(ISNULL(SRS.vcZipPostal,'')) = 0 THEN 4
									ELSE 5
								END),
								(CASE
									WHEN ISNULL(SR.numRelationship,0) > 0 AND ISNULL(SR.numProfile,0) > 0 THEN 1
									WHEN ISNULL(SR.numRelationship,0) > 0 AND ISNULL(SR.numProfile,0) = 0 THEN 2
									WHEN ISNULL(SR.numRelationship,0) = 0 AND ISNULL(SR.numProfile,0) > 0 THEN 3
									ELSE 4
								END),
								LEN(ISNULL(SRS.vcZipPostal,''))
						)
		END

		SELECT * FROM ShippingRules WHERE numRuleID = @numRuleID 

		SELECT ROW_NUMBER() OVER(ORDER BY numServiceTypeID ASC) AS RowNum,
				CAST(monRate AS DECIMAL(20,2)) AS monRate,	
		 * FROM ShippingServiceTypes WHERE numRuleID = @numRuleID 

	 END
END 
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetShippingServiceTypes' ) 
    DROP PROCEDURE USP_GetShippingServiceTypes
GO
CREATE PROCEDURE USP_GetShippingServiceTypes
    @numDomainID NUMERIC,
    @numRuleID NUMERIC,
    @numServiceTypeID NUMERIC = 0,
    @tintMode TINYINT = 0
AS 
BEGIN
    IF @tintMode = 2 
    BEGIN
		IF NOT EXISTS (SELECT 
							* 
						FROM 
							dbo.ShippingServiceTypes
						WHERE 
							numDomainID = @numDomainID
							AND intNsoftEnum > 0 
							AND ISNULL(numRuleID,0) = 0
						) 
		BEGIN
			INSERT INTO dbo.ShippingServiceTypes
			(	numShippingCompanyID,
				vcServiceName,
				intNsoftEnum,
				intFrom,
				intTo,
				fltMarkup,
				bitMarkupType,
				monRate,
				bitEnabled,
				numRuleID,
				numDomainID 
			)
			SELECT  numShippingCompanyID,
							vcServiceName,
							intNsoftEnum,
							intFrom,
							intTo,
							fltMarkup,
							bitMarkupType,
							monRate,
							bitEnabled,
							NULL,
							@numDomainID
			FROM    dbo.ShippingServiceTypes
			WHERE   numDomainID = 0 AND intNsoftEnum > 0 AND ISNULL(numRuleID,0) = 0
		END

        SELECT DISTINCT 
			* 
		FROM 
		(
			SELECT	
				numServiceTypeID,
				vcServiceName,
				intNsoftEnum,
				intFrom,
				intTo,
				fltMarkup,
				bitMarkupType,
				monRate,
				bitEnabled,
				numRuleID,
				numDomainID,
				numShippingCompanyID,
				CASE WHEN numShippingCompanyID = 91 THEN 'Fedex' 
						WHEN numShippingCompanyID = 88 THEN 'UPS'
						WHEN numShippingCompanyID = 90 THEN 'USPS'
						ELSE 'Other'
				END AS vcShippingCompany	 	
			FROM 
				dbo.ShippingServiceTypes
			WHERE 
				numDomainID = @numDomainID
				AND ISNULL(numRuleID,0) = 0
				AND (numServiceTypeID = @numServiceTypeID OR @numServiceTypeID = 0)
				AND intNsoftEnum > 0							
			UNION ALL	
			SELECT	101,'Amazon Standard',101,0,0,0,1,0,1,0,0,0,'Amazon' vcShippingCompany	 						
			UNION ALL
			SELECT	102,'ShippingMethodStandard',102,0,0,0,1,0,1,0,0,0,'E-Bay' vcShippingCompany	 							
			UNION ALL
			SELECT	103,'Other',103,0,0,0,1,0,1,0,0,0,'E-Bay' vcShippingCompany								
		) TABLE1 
		WHERE 
			(numServiceTypeID = @numServiceTypeID OR @numServiceTypeID = 0) 
		ORDER BY 
			vcShippingCompany
    END
    IF @tintMode=1
    BEGIN
	  	SELECT 
			numServiceTypeID,
            vcServiceName,
            intNsoftEnum,
            intFrom,
            intTo,
            fltMarkup,
            bitMarkupType,
            CAST(monRate AS DECIMAL(20,2)) AS monRate,
            bitEnabled,
            numRuleID,
            numDomainID
        FROM
			dbo.ShippingServiceTypes
        WHERE 
			numDomainID = @numDomainID
            AND numRuleID = @numRuleID
            AND (numServiceTypeID = @numServiceTypeID OR @numServiceTypeID = 0)
            AND ISNULL(intNsoftEnum,0)=0
	END
END
GO

GO
/****** Object:  StoredProcedure [dbo].[usp_GetStageItemDetails]    Script Date: 09/23/2010 15:24:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--usp_GetStageItemDetails              
              
--This procedure will return the values from the stagepercentagedetails table. If the stage number is passed then              
--the details pertaining to that stge will be passed else all the records will be passed              
--EXEC usp_GetStageItemDetails 1,133,1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getstageitemdetails')
DROP PROCEDURE usp_getstageitemdetails
GO
CREATE PROCEDURE [dbo].[usp_GetStageItemDetails]
--    @numStageNo NUMERIC = 0,
    @numDomainid NUMERIC = 0,
    @slpid NUMERIC = 0 ,
    @tintMode TINYINT=0,
	@numOppId NUMERIC=0
--              
AS 
IF @tintMode = 0 
BEGIN              
        SELECT  numStageDetailsId,
                numStagePercentageId,
                vcStageName,
                sp.numDomainId,
                sp.numCreatedBy,
                sp.bintCreatedDate,
                sp.numModifiedBy,
                sp.bintModifiedDate,
                slp_id,
                sp.numAssignTo,
                vcMilestoneName,
				ISNULL((select
				   DISTINCT
					stuff((
						select ',' + ST.vcTaskName+'('+ISNULL((SELECT TOP 1 (AU.vcFirstName+' '+AU.vcLastName) FROM AdditionalContactsInformation AS AU WHERE AU.numContactID=ST.numAssignTo),'-')+')'
						from StagePercentageDetailsTask ST
						where ST.numStageDetailsId = STU.numStageDetailsId AND ISNULL(ST.numOppId,0)=0 AND  ISNULL(ST.numProjectId,0)=0
						order by ST.numTaskId
						for xml path('')
					),1,1,'') as TaskName
				from StagePercentageDetailsTask AS STU  WHERE STU.numStageDetailsID=sp.numStageDetailsId AND ISNULL(STU.numOppId,0)=0 AND  ISNULL(STU.numProjectId,0)=0
				group by numStageDetailsId),'-') As TaskName,
				(SELECT ISNULL(SUM(numHours),0) FROM StagePercentageDetailsTask WHERE numStageDetailsId=sp.numStageDetailsId) As numHours,
				(SELECT ISNULL(SUM(numMinutes),0) FROM StagePercentageDetailsTask WHERE numStageDetailsId=sp.numStageDetailsId)  As numMinutes,
                CASE WHEN sp.numAssignTo = 0 THEN '--Select One--'
                     ELSE A.vcFirstName + ' ' + A.vcLastName
                END AS vcAssignTo,
                ISNULL(tintPercentage, '') tintPercentage,
                ISNULL(bitClose, 0) bitClose,
                tintConfiguration,
                ISNULL(vcDescription,'') vcDescription,
                ISNULL(numParentStageID,0) numParentStageID,
				ISNULL(bitIsDueDaysUsed,0) AS bitIsDueDaysUsed,
				ISNULL(numTeamId,0) AS numTeamId,
				ISNULL(bitRunningDynamicMode,0) AS bitRunningDynamicMode,
                ISNULL(intDueDays,0) AS intDueDays,(select count(*) from StageDependency where numStageDetailID=sp.numStageDetailsId) numDependencyCount,
				(SELECT convert(varchar(10),PPD.dtmStartDate, 101) + right(convert(varchar(32),PPD.dtmStartDate,100),8) FROM ProjectProcessStageDetails AS PPD WHERE PPD.numOppId=@numOppId AND numStageDetailsId=Sp.numStageDetailsId) AS StageStartDate,
				(select dbo.fn_calculateTotalHoursMinutesByStageDetails(sp.numStageDetailsId,@numOppId)) as dayshoursTaskDuration
        FROM    stagepercentagedetails sp
                LEFT JOIN AdditionalContactsInformation A ON sp.numAssignTo = A.numContactID
        WHERE  
                sp.numdomainid = @numDomainid
                AND slp_id = @slpid
                AND ISNULL(sp.numProjectID,0)= 0
                AND ISNULL(sp.numOppID,0)= 0
        ORDER BY numstagepercentageid,
                numStageDetailsId ASC              
    END              
ELSE IF @tintMode = 1 -- Fill dropdown
SELECT numStageDetailsId,vcStageName FROM dbo.StagePercentageDetails WHERE slp_id = @slpid AND numDomainId=@numDomainid
	AND LEN(vcStageName) > 1

ELSE IF  @tintMode = 2
SELECT [numStageDetailsId],[numStagePercentageId],tintConfiguration
      ,[vcStageName],[numDomainId],[numCreatedBy],
	(Case bitFromTemplate when 0 then isnull(dbo.fn_GetContactName([numCreatedBy]),'-') else 'Template' end) as vcCreatedBy,dbo.[FormatedDateFromDate](ISNULL(bintCreatedDate,GETDATE()),@numDomainId) bintCreatedDate
      ,[numModifiedBy],isnull(dbo.fn_GetContactName([numModifiedBy]),'-') as vcModifiedBy
      ,dbo.[FormatedDateTimeFromDate](ISNULL(bintModifiedDate,GETDATE()),@numDomainId) bintModifiedDate,[slp_id]
      ,[numAssignTo],isnull(dbo.fn_GetContactName(numAssignTo),'-') as vcAssignTo
      ,[vcMileStoneName],[tintPercentage],isnull([tinProgressPercentage],0) tinProgressPercentage,[bitClose]
      ,dbo.[FormatedDateFromDate](ISNULL(dtStartDate,GETDATE()),@numDomainId) dtStartDate,dbo.[FormatedDateFromDate](ISNULL(dtEndDate,GETDATE()),@numDomainId) dtEndDate,[numParentStageID],[intDueDays]
      ,[numProjectID],[numOppID],[vcDescription], 
  isnull(dbo.fn_GetExpenseDtlsbyProStgID(numProjectID,numStageDetailsId,0),0) as numExpense,                        
  isnull(dbo.fn_GeTimeDtlsbyProStgID_BT_NBT(numProjectID,numStageDetailsId,1),'Billable Time (0)  Non Billable Time (0)') as numTime,
(select count(*) from dbo.GenericDocuments where numRecID=numStageDetailsId and vcDocumentSection='PS') numDocuments,
(select count(*) from [StagePercentageDetails] where numParentStageId=p.numStageDetailsId and numProjectID=p.numProjectID) as numChildCount,
isnull(bitTimeBudget,0) as bitTimeBudget,isnull(bitExpenseBudget,0) as bitExpenseBudget,isnull(monTimeBudget,0) as monTimeBudget,isnull(monExpenseBudget,0) as monExpenseBudget
  FROM [StagePercentageDetails] p where [numStageDetailsId]=@slpid

ELSE IF  @tintMode = 3
SELECT count(*) Total
  FROM [StagePercentageDetails] p where numProjectID=@slpid and numDomainid=@numDomainid

ELSE IF  @tintMode =4
SELECT count(*) Total
  FROM [StagePercentageDetails] p where numOppID=@slpid and numDomainid=@numDomainid

ELSE
BEGIN
	SELECT numStageDetailsId,vcStageName FROM dbo.StagePercentageDetails WHERE slp_id = @slpid AND numDomainId=@numDomainid
	AND LEN(vcStageName) > 1
END
--    BEGIN              
--        SELECT  numStageDetailsId,
--                numStagePercentageId,
--                numStageNumber,
--                vcStageDetail,
--                sp.numDomainId,
--                sp.numCreatedBy,
--                sp.bintCreatedDate,
--                sp.numModifiedBy,
--                sp.bintModifiedDate,
--                slp_id,
--                sp.numAssignTo,
--                vcStagePercentageDtl,
--                CASE WHEN sp.numAssignTo = 0 THEN '--Select One--'
--                     ELSE A.vcFirstName + ' ' + A.vcLastName
--                END AS vcAssignTo,
--                numTemplateId,
--                ISNULL(tintPercentage, '') tintPercentage,
--                ISNULL(bitClose, 0) bitClose
--        FROM    stagepercentagedetails sp
--                LEFT JOIN AdditionalContactsInformation A ON sp.numAssignTo = A.numContactID
--        WHERE   sp.numdomainid = @numDomainid
--                AND slp_id = @slpid
--        ORDER BY numstagepercentageid,
--                numStageDetailsId ASC              
            
            
   --SELECT * FROM stagepercentagedetails             
            
--    END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getuserswithdomains')
DROP PROCEDURE usp_getuserswithdomains
GO
CREATE PROCEDURE [dbo].[usp_GetUsersWithDomains]                            
 @numUserID NUMERIC(18,0)=0,
 @numDomainID NUMERIC(18,0)                       
AS                            
BEGIN                            
	IF @numUserID = 0 AND @numDomainID > 0 --Check if request is made while session is on
	BEGIN                            
		SELECT 
			numUserID
			,vcUserName
			,UserMaster.numGroupID
			,ISNULL(vcGroupName,'-') AS vcGroupName
			,vcUserDesc
			,ISNULL(ADC.vcfirstname,'') +' '+ ISNULL(ADC.vclastname,'') as Name
			,UserMaster.numDomainID
			,vcDomainName
			,UserMaster.numUserDetailId
			,ISNULL(UserMaster.SubscriptionId,'') AS SubscriptionId
			,ISNULL(UserMaster.tintDefaultRemStatus,0) tintDefaultRemStatus
			,ISNULL(UserMaster.numDefaultTaskType,0) numDefaultTaskType
			,ISNULL(UserMaster.bitOutlook,0) bitOutlook
			,UserMaster.vcLinkedinId
			,ISNULL(UserMaster.intAssociate,0) AS intAssociate
			,UserMaster.ProfilePic
			,UserMaster.numDashboardTemplateID
		FROM 
			UserMaster                      
		JOIN 
			Domain                        
		ON 
			UserMaster.numDomainID =  Domain.numDomainID                       
		LEFT JOIN 
			AdditionalContactsInformation ADC                      
		ON 
			ADC.numContactid=UserMaster.numUserDetailId                     
		LEFT JOIN 
			AuthenticationGroupMaster GM                     
		ON 
			Gm.numGroupID= UserMaster.numGroupID                    
		WHERE 
			tintGroupType=1 -- Application Users                    
		ORDER BY 
			Domain.numDomainID, vcUserDesc                            
	END   
	ELSE IF @numUserID = -1 AND @numDomainID > 0 --Support Email
	BEGIN                            
		SELECT 
			'' AS vcEmailId
			,vcImapUserName
			,ISNULL(bitUseUserName,0) bitUseUserName
			,ISNULL(ImapUserDTL.bitImap,0) bitImap
			,ImapUserDTL.vcImapServerUrl
			,ISNULL(ImapUserDTL.vcImapPassword,'') vcImapPassword
			,ImapUserDTL.bitSSl
			,ImapUserDTL.numPort 
		FROM 
			[ImapUserDetails] ImapUserDTL   
		WHERE 
			ImapUserDTL.numUserCntId = @numUserID 
			AND ImapUserDTL.numDomainID = @numDomainID
	END                         
	ELSE                            
	BEGIN   
		DECLARE @vcSMTPServer VARCHAR(100)
			,@numSMTPPort NUMERIC(18,0)
			,@bitSMTPSSL BIT
			,@bitSMTPAuth BIT
			,@vcIMAPServer  VARCHAR(100)
			,@numIMAPPort NUMERIC(18,0)
			,@bitIMAPSSL BIT
			,@bitIMAPAuth BIT
	    
		SELECT
			@vcSMTPServer=vcSMTPServer
			,@numSMTPPort=numSMTPPort
			,@bitSMTPSSL=bitSMTPSSL
			,@bitSMTPAuth=bitSMTPAuth
			,@vcIMAPServer=vcIMAPServer
			,@numIMAPPort=numIMAPPort
			,@bitIMAPSSL=bitIMAPSSL
			,@bitIMAPAuth=bitIMAPAuth
		FROM
			UniversalSMTPIMAP
	    WHERE
			numDomainID=@numDomainID
		             
		SELECT       
			U.numUserID
			,vcUserName
			,numGroupID
			,vcUserDesc
			,U.numDomainID
			,vcDomainName
			,U.numUserDetailId
			,bitHourlyRate
			,bitActivateFlag
			,monHourlyRate
			,bitSalary
			,numDailyHours
			,bitOverTime
			,numLimDailHrs
			,monOverTimeRate
			,bitMainComm
			,fltMainCommPer
			,bitRoleComm
			,(SELECT COUNT(*) FROM UserRoles WHERE UserRoles.numUserCntID=U.numUserDetailId) AS Roles
			,vcEmailid
			,ISNULL(vcEmailAlias,'') vcEmailAlias
			,vcPassword
			,ISNULL(ExcUserDTL.bitExchangeIntegration,0) bitExchangeIntegration
			,ISNULL(ExcUserDTL.bitAccessExchange,0) bitAccessExchange
			,ISNULL(ExcUserDTL.vcExchPassword,'') as vcExchPassword
			,ISNULL(ExcUserDTL.vcExchPath,'') as vcExchPath
			,ISNULL(ExcUserDTL.vcExchDomain,'') as vcExchDomain
			,ISNULL(U.SubscriptionId,'') as SubscriptionId
			,ISNULL(ImapUserDTL.bitImap,0) bitImap
			,ISNULL(ImapUserDTL.vcImapServerUrl,ISNULL(@vcIMAPServer,'')) vcImapServerUrl
			,ISNULL(ImapUserDTL.vcImapPassword,'') vcImapPassword
			,ISNULL(ImapUserDTL.bitSSl,ISNULL(@bitIMAPSSL,0)) bitSSl
			,ISNULL(ImapUserDTL.numPort,ISNULL(@numIMAPPort,0)) numPort
			,ISNULL(ImapUserDTL.bitUseUserName,ISNULL(@bitIMAPAuth,0)) bitUseUserName
			,(CASE WHEN ISNULL([vcSMTPServer],'') <> '' AND ISNULL(numSMTPPort,0) <> 0 THEN ISNULL(bitSMTPAuth,0) ELSE ISNULL(@bitSMTPAuth,0) END) bitSMTPAuth
			,ISNULL([vcSmtpPassword],'')vcSmtpPassword
			,(CASE WHEN ISNULL([vcSMTPServer],'') = '' THEN ISNULL(@vcSMTPServer,'') ELSE [vcSMTPServer] END) vcSMTPServer
			,(CASE WHEN ISNULL(numSMTPPort,0) = 0 THEN ISNULL(@numSMTPPort,0) ELSE numSMTPPort END) numSMTPPort
			,(CASE WHEN ISNULL([vcSMTPServer],'') <> '' AND ISNULL(numSMTPPort,0) <> 0 THEN ISNULL(bitSMTPSSL,0) ELSE ISNULL(@bitSMTPSSL,0) END) bitSMTPSSL
			,ISNULL(bitSMTPServer,0) bitSMTPServer
			,ISNULL(U.tintDefaultRemStatus,0) tintDefaultRemStatus
			,ISNULL(U.numDefaultTaskType,0) numDefaultTaskType
			,ISNULL(U.bitOutlook,0) bitOutlook
			,ISNULL(numDefaultClass,0) numDefaultClass
			,ISNULL(numDefaultWarehouse,0) numDefaultWarehouse
			,U.vcLinkedinId
			,ISNULL(U.intAssociate,0) as intAssociate
			,U.ProfilePic
			,U.numDashboardTemplateID
		FROM 
			UserMaster U                        
		JOIN 
			Domain D                       
		ON 
			U.numDomainID =  D.numDomainID       
		LEFT JOIN 
			ExchangeUserDetails ExcUserDTL      
		ON 
			ExcUserDTL.numUserID=U.numUserID                      
		LEFT JOIN 
			[ImapUserDetails] ImapUserDTL   
		ON 
			ImapUserDTL.numUserCntId = U.numUserDetailId   
			AND ImapUserDTL.numDomainID = D.numDomainID
		WHERE 
			U.numUserID=@numUserID                            
	 END                            
END
/****** Object:  StoredProcedure [dbo].[USP_GetWorkOrder]    Script Date: 07/26/2008 16:16:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWorkOrder')
DROP PROCEDURE USP_GetWorkOrder
GO
CREATE PROCEDURE [dbo].[USP_GetWorkOrder]                             
@numDomainID NUMERIC(18,0),
@ClientTimeZoneOffset  INT,
@numWOStatus NUMERIC(18,0),
@SortExpression AS VARCHAR(50),
@CurrentPage int,                                                                            
@PageSize INT,
@numUserCntID AS NUMERIC(9),
@vcWOId  AS VARCHAR(4000),
@tintFilter TINYINT = 0,
@numOppID NUMERIC(18,0),
@numOppItemID NUMERIC(18,0)                                                                
AS                            
BEGIN
	SET @vcWOId=ISNULL(@vcWOId,'')
	
	DECLARE @tintCommitAllocation AS TINYINT
	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID   
    
	DECLARE @TEMP TABLE
	(
		numID INT IDENTITY(1,1),ItemLevel INT, numParentWOID NUMERIC(18,0), numWOID NUMERIC(18,0), ID VARCHAR(1000), numItemCode NUMERIC(18,0),
		vcItemName VARCHAR(500), numQty FLOAT, numWarehouseItemID NUMERIC(18,0), vcWarehouse VARCHAR(200), numOnHand FLOAT,
		numOnOrder FLOAT, numAllocation FLOAT, numBackOrder FLOAT, vcInstruction VARCHAR(2000),
		numAssignedTo NUMERIC(18,0), vcAssignedTo VARCHAR(500), numCreatedBy NUMERIC(18,0), vcCreated VARCHAR(500),
		bintCompliationDate VARCHAR(50), numWOStatus NUMERIC(18,0), numOppID NUMERIC(18,0), vcOppName VARCHAR(1000),
		numPOID NUMERIC(18,0), vcPOName VARCHAR(1000), bitWorkOrder BIT, bitReadyToBuild BIT
	)

	DECLARE @TEMPWO TABLE
	(
		ID INT IDENTITY(1,1)
		,numWOID NUMERIC(18,0)
	)

	DECLARE @TEMPWORKORDER TABLE
	(
		ItemLevel INT, RowNumber INT, numParentWOID NUMERIC(18,0), numWOID NUMERIC(18,0), ID VARCHAR(1000), numItemCode NUMERIC(18,0),
		numOppID NUMERIC(18,0), numWarehouseItemID NUMERIC(18,0), numQty FLOAT, numWOStatus NUMERIC(18,0), vcInstruction VARCHAR(2000),
		numAssignedTo NUMERIC(18,0), bintCreatedDate DATETIME, bintCompliationDate DATETIME, numCreatedBy NUMERIC(18,0),  
		bitWorkOrder BIT, bitReadyToBuild BIT
	) 

	INSERT INTO 
		@TEMPWORKORDER
	SELECT 
		1 AS ItemLevel,
		ROW_NUMBER() OVER(ORDER BY numWOId) AS RowNumber,
		CAST(0 AS NUMERIC(18,0)) AS numParentId,
		numWOId,
		CAST(CONCAT('#',ISNULL(numWOId,'0'),'#') AS VARCHAR(1000)) AS ID,
		numItemCode,
		numOppId,
		numWareHouseItemId,
		CAST(numQtyItemsReq AS FLOAT),
		numWOStatus,
		vcInstruction,
		numAssignedTo,
		bintCreatedDate,
		bintCompliationDate,
		numCreatedBy,
		1 AS bitWorkOrder,
		(CASE WHEN WorkOrder.numWOStatus = 23184 THEN 1 ELSE 0 END) AS bitReadyToBuild
	FROM 
		WorkOrder
	WHERE  
		numDomainID=@numDomainID
		AND (numOppID=@numOppID OR ISNULL(@numOppID,0)=0)
		AND (numOppItemID=@numOppItemID OR ISNULL(@numOppItemID,0)=0)
		AND 1=(CASE WHEN ISNULL(@numWOStatus,0) = 0 THEN CASE WHEN WorkOrder.numWOStatus <> 23184 THEN 1 ELSE 0 END ELSE CASE WHEN WorkOrder.numWOStatus = @numWOStatus THEN 1 ELSE 0 END END)
		AND 1=(Case when @numUserCntID>0 then case when isnull(numAssignedTo,0)=@numUserCntID then 1 else 0 end else 1 end)
		AND 1=(Case when len(@vcWOId)>0 then Case when numWOId in (SELECT * from dbo.Split(@vcWOId,',')) then 1 else 0 end else 1 end)
		AND (ISNULL(numParentWOID,0) = 0 OR numParentWOID NOT IN (SELECT numWOId FROM WorkOrder WHERE numDomainID=@numDomainID))
	ORDER BY
		bintCreatedDate DESC,
		numWOId ASC

	;WITH CTEWorkOrder (ItemLevel,numParentWOID,numWOID,ID,numItemKitID,numQtyItemsReq,numWarehouseItemID,vcInstruction,numAssignedTo,
						bintCreatedDate,numCreatedBy,bintCompliationDate,numWOStatus,numOppID,bitWorkOrder,bitReadyToBuild) AS
	(
		SELECT
			ItemLevel,
			numParentWOID,
			numWOId,
			ID,
			numItemCode,
			numQty,
			numWareHouseItemId,
			vcInstruction,
			numAssignedTo,
			bintCreatedDate,
			numCreatedBy,
			bintCompliationDate,
			numWOStatus,
			numOppId,
			1 AS bitWorkOrder,
			bitReadyToBuild
		FROM
			@TEMPWORKORDER t
		UNION ALL
		SELECT 
			c.ItemLevel+2,
			c.numWOID,
			WorkOrder.numWOId,
			CAST(CONCAT(c.ID,'-','#',ISNULL(WorkOrder.numWOId,'0'),'#') AS VARCHAR(1000)),
			WorkOrder.numItemCode,
			CAST(WorkOrder.numQtyItemsReq AS FLOAT),
			WorkOrder.numWareHouseItemId,
			CAST(WorkOrder.vcInstruction AS VARCHAR(2000)),
			WorkOrder.numAssignedTo,
			WorkOrder.bintCreatedDate,
			WorkOrder.numCreatedBy,
			WorkOrder.bintCompliationDate,
			WorkOrder.numWOStatus,
			WorkOrder.numOppId,
			1 AS bitWorkOrder,
			CASt((CASE WHEN WorkOrder.numWOStatus = 23184 THEN 1 ELSE 0 END) AS BIT) AS bitReadyToBuild
		FROM 
			WorkOrder
		INNER JOIN 
			CTEWorkOrder c 
		ON 
			WorkOrder.numParentWOID = c.numWOID
	)

	INSERT 
		@TEMP
	SELECT
		ItemLevel,
		numParentWOID,
		numWOID,
		ID,
		numItemCode,
		vcItemName,
		CTEWorkOrder.numQtyItemsReq,
		CTEWorkOrder.numWarehouseItemID,
		Warehouses.vcWareHouse,
		WareHouseItems.numOnHand,
		WareHouseItems.numOnOrder,
		WareHouseItems.numAllocation,
		WareHouseItems.numBackOrder,
		vcInstruction,
		CTEWorkOrder.numAssignedTo,
		dbo.fn_GetContactName(isnull(CTEWorkOrder.numAssignedTo,0)) AS vcAssignedTo,
		CTEWorkOrder.numCreatedBy,
		CAST(ISNULL(dbo.fn_GetContactName(CTEWorkOrder.numCreatedBy), '&nbsp;&nbsp;-') + ',' + convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,CTEWorkOrder.bintCreatedDate)) AS VARCHAR(500)) ,
		convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,bintCompliationDate)) AS bintCompliationDate,
		numWOStatus,
		CTEWorkOrder.numOppID,
		OpportunityMaster.vcPOppName,
		NULL,
		NULL,
		bitWorkOrder,
		bitReadyToBuild
	FROM 
		CTEWorkOrder
	INNER JOIN 
		Item
	ON
		CTEWorkOrder.numItemKitID = Item.numItemCode
	LEFT JOIN
		WareHouseItems
	ON
		CTEWorkOrder.numWarehouseItemID = WareHouseItems.numWareHouseItemID
	LEFT JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	LEFT JOIN 
		OpportunityMaster
	ON
		CTEWorkOrder.numOppID = OpportunityMaster.numOppId

	INSERT INTO 
		@TEMP
	SELECT
		t1.ItemLevel + 1,
		t1.numWOID,
		NULL,
		CONCAT(t1.ID,'-#0#'),
		WorkOrderDetails.numChildItemID,
		Item.vcItemName,
		WorkOrderDetails.numQtyItemsReq,
		WorkOrderDetails.numWarehouseItemID,
		Warehouses.vcWareHouse,
		WareHouseItems.numOnHand,
		WareHouseItems.numOnOrder,
		WareHouseItems.numAllocation,
		WareHouseItems.numBackOrder,
		vcInstruction,
		NULL,
		NULL,
		NULL,
		NULL,
		convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,bintCompliationDate)) AS bintCompliationDate,
		numWOStatus,
		NULL,
		NULL,
		OpportunityMaster.numOppId,
		OpportunityMaster.vcPOppName,
		0 AS bitWorkOrder,
		CASE 
			WHEN Item.charItemType='P' AND ISNULL(t1.bitReadyToBuild,0) = 0
			THEN
				CASE 
					WHEN (CASE WHEN ISNULL(t1.numOppId,0) > 0 THEN 1 ELSE 0 END)=1 AND @tintCommitAllocation=2 
					THEN (CASE WHEN ISNULL(numQtyItemsReq,0) > WareHouseItems.numOnHand THEN 0 ELSE 1 END)
					ELSE (CASE WHEN ISNULL(numQtyItemsReq,0) > WareHouseItems.numAllocation THEN 0 ELSE 1 END) 
				END
			ELSE 1
		END
	FROM
		WorkOrderDetails
	INNER JOIN
		@TEMP t1
	ON
		WorkOrderDetails.numWOId = t1.numWOID
	INNER JOIN 
		Item
	ON
		WorkOrderDetails.numChildItemID = Item.numItemCode
		AND 1= (CASE WHEN (SELECT COUNT(*) FROM @TEMP TInner WHERE TInner.bitWorkOrder=1 AND TInner.numParentWOID=t1.numWOID AND TInner.numItemCode=Item.numItemCode) > 0 THEN 0 ELSE 1 END)
	LEFT JOIN
		WareHouseItems
	ON
		WorkOrderDetails.numWarehouseItemID = WareHouseItems.numWareHouseItemID
	LEFT JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	LEFT JOIN
		OpportunityMaster 
	ON
		WorkOrderDetails.numPOID = OpportunityMaster.numOppId

	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT
	DECLARE @numTempWOID NUMERIC(18,0)
	INSERT INTO @TEMPWO (numWOID) SELECT numWOID FROM @TEMP WHERE bitWorkOrder=1 ORDER BY ItemLevel DESC

	SELECT @iCount=COUNT(*) FROM @TEMPWO

	WHILE @i <= @iCount
	BEGIN
		SELECT @numTempWOID=numWOID FROM @TEMPWO WHERE ID=@i

		UPDATE
			T1
		SET
			bitReadyToBuild = (CASE WHEN (SELECT COUNT(*) FROM @TEMP TInner WHERE TInner.numParentWOID=T1.numWOID AND bitReadyToBuild=0) > 0 THEN 0 ELSE 1 END)
		FROM
			@TEMP T1
		WHERE
			numWOID=@numTempWOID

		SET @i = @i + 1
	END

	If @tintFilter = 2 --Buildable Orders
	BEGIN
		DELETE FROM @TEMP WHERE 1 = (CASE WHEN bitReadyToBuild=1 THEN 0 ELSE 1 END) AND bitWorkOrder=1
	END
	ELSE IF @tintFilter = 3 --Un-Buildable Orders
	BEGIN
		DELETE FROM @TEMP WHERE 1 = (CASE WHEN bitReadyToBuild=1 THEN 1 ELSE 0 END) AND bitWorkOrder=1
	END

	DECLARE @TEMPPagingID TABLE
	(
		ID INT
		,numWOID NUMERIC(18,0)
		,numParentWOID NUMERIC(18,0)
	)
	INSERT INTO @TEMPPagingID
	(
		ID
		,numWOID
		,numParentWOID
	)
	SELECT
		numID
		,numWOID
		,numParentWOID
	FROM	
		@TEMP 
	WHERE 
		ISNULL(numParentWOID,0) = 0
	ORDER BY
		numWOID
	OFFSET
		(@CurrentPage - 1) * @PageSize ROWS FETCH NEXT @PageSize ROWS ONLY

	;WITH CTEID (numID,numWOID,numParentWOID) AS
	(
		SELECT
			ID
			,numWOID
			,numParentWOID
		FROM	
			@TEMPPagingID 
		UNION ALL
		SELECT
			T.numID
			,T.numWOID
			,T.numParentWOID
		FROM
			@TEMP T
		INNER JOIN
			CTEID C
		ON
			T.numParentWOID = C.numWOID
		WHERE
			ISNULL(T.numParentWOID,0) > 0
	)

	SELECT * FROM @TEMP T1 INNER JOIN CTEID ON T1.numID = CTEID.numID ORDER BY T1.numParentWOID,T1.ItemLevel

	DECLARE @TOTALROWCOUNT AS NUMERIC(18,0)

	SELECT @TOTALROWCOUNT=COUNT(*) FROM @TEMP WHERE ISNULL(numParentWOID,0)=0

	SELECT @TOTALROWCOUNT AS TotalRowCount 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertupdatepromotionofferfororder')
DROP PROCEDURE usp_insertupdatepromotionofferfororder
GO
CREATE PROCEDURE [dbo].[USP_InsertUpdatePromotionOfferForOrder]
    @numProId AS NUMERIC(9) = 0,
    @vcProName AS VARCHAR(100),
    @numDomainId AS NUMERIC(18,0),
    @dtValidFrom AS DATETIME,
    @dtValidTo AS DATETIME,
	@bitNeverExpires BIT,
	@bitApplyToInternalOrders BIT,
	@bitAppliesToSite BIT,
	@bitRequireCouponCode BIT,
	@txtCouponCode VARCHAR(MAX),
	@tintUsageLimit TINYINT,
	@numUserCntID NUMERIC(18,0),
	@tintCustomersBasedOn TINYINT,
	@strOrderAmount VARCHAR(MAX),
	@strfltDiscount VARCHAR(MAX),
	@tintDiscountType TINYINT
AS 
BEGIN

	IF @numProId = 0 
	BEGIN
		INSERT INTO PromotionOffer
        (
            vcProName
			,numDomainId
			,numCreatedBy
			,dtCreated
			,bitEnabled
			,IsOrderBasedPromotion 
        )
        VALUES  
		(
			@vcProName
           ,@numDomainId
           ,@numUserCntID
		   ,GETUTCDATE()
		   ,1
		   ,1
        )
            
		SELECT SCOPE_IDENTITY()
	END
	ELSE
	BEGIN	
		IF @tintCustomersBasedOn = 1
		BEGIN
			IF ISNULL(@tintCustomersBasedOn,0) = 1 AND (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = @numProId AND tintType = 1) = 0
			BEGIN
				RAISERROR ( 'SELECT_INDIVIDUAL_CUSTOMER',16, 1 )
 				RETURN;
			END
		END
			
			/* 2- The customer is targeted at the group level (level 2), and the item is specific (level 1). If the user tries to create another rule that targets this same Relationship / Profile combination and that same item in level 1 (L2 customer and L1 item both match), validation would say �You�ve already created a promotion rule that targets this relationship / profile and one or more item specifically�*/
		ELSE IF @tintCustomersBasedOn = 2
		BEGIN
			IF ISNULL(@tintCustomersBasedOn,0) = 2 AND (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = @numProId AND tintType = 2) = 0
			BEGIN
				RAISERROR ( 'SELECT_RELATIONSHIP_PROFILE',16, 1 )
 				RETURN;
			END
		END

		ELSE IF @tintCustomersBasedOn = 0
		BEGIN
			RAISERROR ( 'SELECT_CUSTOMERS',16, 1 )
 			RETURN;
		END

		IF @tintCustomersBasedOn = 3
		BEGIN
			DELETE FROM PromotionOfferorganizations 
				WHERE tintType IN (1, 2) 
					AND numProId = @numProId
		END

		IF (
			SELECT
				COUNT(*)
			FROM
				PromotionOffer
			LEFT JOIN
				PromotionOfferOrganizations
			ON
				PromotionOffer.numProId = PromotionOfferOrganizations.numProId
				AND 1 = (CASE 
							WHEN @tintCustomersBasedOn=1 THEN (CASE WHEN PromotionOfferOrganizations.tintType=1 THEN 1 ELSE 0 END) 
							WHEN @tintCustomersBasedOn=2 THEN (CASE WHEN PromotionOfferOrganizations.tintType=2 THEN 1 ELSE 0 END) 
							ELSE 1
						END)
			
			WHERE
				PromotionOffer.numDomainId=@numDomainID
				AND PromotionOffer.numProId <> @numProId
				AND tintCustomersBasedOn = @tintCustomersBasedOn AND ISNULL(bitEnabled,0) = 1			
				 
				AND 1 = (CASE 							
							WHEN @tintCustomersBasedOn=2 AND IsOrderBasedPromotion = 1 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
										
									THEN 
										1 
									ELSE 
										0 
								END)
							
						END)
			) > 0
		BEGIN
			DECLARE @ERRORMESSAGE AS VARCHAR(50)
			SET @ERRORMESSAGE = 'RELATIONSHIPPROFILE ALREADY EXISTS'							
					
			RAISERROR(@ERRORMESSAGE,16,1)
			RETURN

		END

		IF ISNULL(@bitAppliesToSite,0) = 0
		BEGIN
			DELETE FROM PromotionOfferSites WHERE numPromotionID=@numProId
		END

		UPDATE 
			PromotionOffer
		SET 
			vcProName = @vcProName
			,numDomainId = @numDomainId
			,dtValidFrom = @dtValidFrom
			,dtValidTo = @dtValidTo
			,bitNeverExpires = @bitNeverExpires
			,bitApplyToInternalOrders = @bitApplyToInternalOrders
			,bitAppliesToSite = @bitAppliesToSite
			,bitRequireCouponCode = @bitRequireCouponCode			
			,numModifiedBy = @numUserCntID
			,dtModified = GETUTCDATE()
			,tintCustomersBasedOn = @tintCustomersBasedOn
			,tintDiscountType = @tintDiscountType
		WHERE 
			numProId=@numProId

		IF (SELECT COUNT(*) FROM PromotionOfferOrder WHERE numPromotionID = @numProId AND numDomainId = @numDomainId) > 0
		BEGIN
			DELETE FROM PromotionOfferOrder WHERE numPromotionID = @numProId AND numDomainId = @numDomainId
		END

		DECLARE @Tempdata TABLE
		(
			numPromotionID NUMERIC,
			numDomainID NUMERIC,
			numOrderAmount VARCHAR(MAX),
			fltDiscountValue VARCHAR(MAX)
		)

		INSERT @Tempdata SELECT @numProId, @numDomainID, @strOrderAmount, @strfltDiscount

		;WITH tmp(numPromotionID, numDomainID, numOrderAmount, OrderAmount, numfltDiscountValue,fltDiscountValue) AS
		(
			SELECT
				numPromotionID,
				numDomainID,
				LEFT(numOrderAmount, CHARINDEX(',', numOrderAmount + ',') - 1),
				STUFF(numOrderAmount, 1, CHARINDEX(',', numOrderAmount + ','), ''),
				LEFT(fltDiscountValue, CHARINDEX(',', fltDiscountValue + ',') - 1),
				STUFF(fltDiscountValue, 1, CHARINDEX(',', fltDiscountValue + ','), '')
			FROM @Tempdata
			UNION all

			SELECT
				numPromotionID,
				numDomainID,
				LEFT(OrderAmount, CHARINDEX(',', OrderAmount + ',') - 1),
				STUFF(OrderAmount, 1, CHARINDEX(',', OrderAmount + ','), ''),
				LEFT(fltDiscountValue, CHARINDEX(',', fltDiscountValue + ',') - 1),
				STUFF(fltDiscountValue, 1, CHARINDEX(',', fltDiscountValue + ','), '')
			FROM tmp
			WHERE
				OrderAmount > '' AND fltDiscountValue > ''
		)

		INSERT INTO PromotionOfferOrder
		SELECT
			numPromotionID,				
			numOrderAmount,
			numfltDiscountValue,
			numDomainID

		FROM tmp
				
		--DROP TABLE Tempdata
---------------------------Insert Discount Codes-------------------------------------------------------------------

		--DECLARE @CouponCountEntered AS NUMERIC = (SELECT COUNT(*) FROM DiscountCodes DC WHERE numPromotionID = @numProId )
		--DECLARE @CouponCountINTable AS NUMERIC = (SELECT COUNT(*) FROM DiscountCodes DC 
		--								 WHERE numPromotionID = @numProId 
		--										AND vcDiscountCode IN  (
		--											SELECT  Id
		--											FROM    dbo.SplitIDs(@txtCouponCode, ',') )) 

	--print @CouponCountEntered
	--print @CouponCountINTable

	--IF (@CouponCountEntered <> @CouponCountINTable )

	--BEGIN

			DELETE DC FROM DiscountCodes DC			 
			 WHERE numPromotionID = @numProId 
					AND DC.numDiscountId NOT IN (SELECT numDiscountId FROM DiscountCodeUsage)
		
		IF @bitRequireCouponCode = 1
			BEGIN

				CREATE TABLE DiscountTempdata
				(
					numPromotionID NUMERIC,
					CodeUsageLimit TINYINT,
					vcDiscountCode VARCHAR(MAX)			
				)

				INSERT DiscountTempdata SELECT @numProId, @tintUsageLimit, @txtCouponCode

				;WITH tmp1(numPromotionID, CodeUsageLimit, vcDiscountCode, DiscountCode) AS
			(
				SELECT
					numPromotionID,
					CodeUsageLimit,
					LEFT(vcDiscountCode, CHARINDEX(',', vcDiscountCode + ',') - 1),
					STUFF(vcDiscountCode, 1, CHARINDEX(',', vcDiscountCode + ','), '')
				
				FROM DiscountTempdata
				UNION all

				SELECT
					numPromotionID,
					CodeUsageLimit,
					LEFT(DiscountCode, CHARINDEX(',', DiscountCode + ',') - 1),
					STUFF(DiscountCode, 1, CHARINDEX(',', DiscountCode + ','), '')
				FROM tmp1
				WHERE
					DiscountCode > '' 
			)	

			INSERT INTO DiscountCodes
			SELECT
				tmp1.numPromotionID,				
				tmp1.vcDiscountCode,
				tmp1.CodeUsageLimit
			FROM tmp1
			WHERE tmp1.vcDiscountCode NOT IN ( SELECT vcDiscountCode FROM DiscountCodes DC INNER JOIN DiscountCodeUsage DCU ON DC.numDiscountId = DCU.numDiscountId)
				
			DROP TABLE DiscountTempdata		

		END	
	--END	
	
	--ELSE
	--BEGIN
		UPDATE DiscountCodes
		SET CodeUsageLimit = @tintUsageLimit
		WHERE numPromotionID = @numProId
	--END
		       
        SELECT  @numProId

		---------------------------------------------------------------
--	COMMIT
--END TRY
--BEGIN CATCH
--	DECLARE @ErrorMessage1 NVARCHAR(4000)
--	DECLARE @ErrorNumber INT
--	DECLARE @ErrorSeverity INT
--	DECLARE @ErrorState INT
--	DECLARE @ErrorLine INT
--	DECLARE @ErrorProcedure NVARCHAR(200)

--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	DELETE FROM PromotionOffer WHERE numProId = @numProId
--	DELETE FROM PromotionOfferOrder WHERE numPromotionID = @numProId
--	DELETE FROM DiscountCodes WHERE numPromotionID = @numProId
--	DELETE FROM PromotionOfferOrganizations WHERE numProId = @numProId


--	SELECT 
--		@ErrorMessage1 = ERROR_MESSAGE(),
--		@ErrorNumber = ERROR_NUMBER(),
--		@ErrorSeverity = ERROR_SEVERITY(),
--		@ErrorState = ERROR_STATE(),
--		@ErrorLine = ERROR_LINE(),
--		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

--	RAISERROR (@ErrorMessage1, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
--END CATCH


	END
END


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_SelectedChildItemDetails')
DROP PROCEDURE USP_Item_SelectedChildItemDetails
GO
CREATE PROCEDURE [dbo].[USP_Item_SelectedChildItemDetails]        
	@numDomainID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@vcKitChilds VARCHAR(MAX)
AS
BEGIN
	SELECT
		I.numItemCode
		,ISNULL(I.vcItemName,'') vcItemName
		,ISNULL(I.vcSKU,'') vcSKU
		,CASE 
			WHEN charItemType='P' 
			THEN CONCAT('Inventory',CASE 
										WHEN ISNULL(I.bitKitParent,0)=1 THEN ' (Kit)' 
										WHEN ISNULL(I.bitAssembly,0)=1 THEN ' (Assembly)' 
										WHEN ISNULL(I.numItemGroup,0) > 0 AND (ISNULL(I.bitMatrix,0)=1 OR EXISTS (SELECT numItemGroupDTL FROM ItemGroupsDTL WHERE numItemGroupID=I.numItemGroup AND tintType=2)) THEN ' (Matrix)' 
										ELSE '' 
									END) 
			WHEN charItemType='S' THEN 'Service' 
			WHEN charItemType='A' THEN 'Accessory' 
			WHEN charItemType='N' THEN 'Non-Inventory' 
		END AS vcType
		,CAST(ISNULL(ID.numQtyItemsReq,0) AS FLOAT) AS numQty
	FROM
		dbo.SplitIDs(@vcKitChilds,',') T1
	INNER JOIN
		Item I
	ON
		T1.Id = I.numItemCode
	INNER JOIN
		ItemDetails ID
	ON
		I.numItemCode = ID.numChildItemID
	WHERE
		ID.numItemKitID = @numItemCode
END
GO
GO
/****** Object:  StoredProcedure [dbo].[USP_ItemDetails]    Script Date: 02/07/2010 15:31:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itemdetails')
DROP PROCEDURE usp_itemdetails
GO
CREATE PROCEDURE [dbo].[USP_ItemDetails]                                                  
@numItemCode as numeric(9) ,          
@ClientTimeZoneOffset as int,
@numDivisionId as numeric(18)                                               
AS
BEGIN 
   DECLARE @bitItemIsUsedInOrder AS BIT=0
   DECLARE @bitOppOrderExists AS BIT = 0
   
   DECLARE @numCompanyID NUMERIC(18,0)=0
   IF ISNULL(@numDivisionId,0) > 0
   BEGIN
		SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionId
   END

   DECLARE @numDomainId AS NUMERIC
   DECLARE @numItemGroup AS NUMERIC(18,0)
   SELECT @numDomainId=numDomainId,@numItemGroup=numItemGroup FROM Item WHERE numItemCode=@numItemCode
   
	
	IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
	BEGIN
		SET @bitItemIsUsedInOrder=1
		SET @bitOppOrderExists = 1
	END
	
	IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
	BEGIN
		SET @bitItemIsUsedInOrder=1
		SET @bitOppOrderExists = 1
	END
	
	IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
	BEGIN
		SET @bitItemIsUsedInOrder=1
		SET @bitOppOrderExists = 1
	END
	
	IF EXISTS(SELECT numItemID FROM dbo.General_Journal_Details WHERE numItemID= @numItemCode and numDomainId=@numDomainId AND chBizDocItems='IA1')
	BEGIN
		SET @bitItemIsUsedInOrder =1
    END
	                                      
	SELECT 
		I.numItemCode, vcItemName, ISNULL(txtItemDesc,'') txtItemDesc,CAST(ISNULL(IED.txtDesc,'')  AS VARCHAR(MAX)) vcExtendedDescToAPI, charItemType, 
		CASE 
			WHEN charItemType='P' AND ISNULL(bitAssembly,0) = 1 then 'Assembly'
			WHEN charItemType='P' AND ISNULL(bitKitParent,0) = 1 then 'Kit'
			WHEN charItemType='P' AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=0 then 'Asset'
			WHEN charItemType='P' AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=1 then 'Rental Asset'
			WHEN charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 0  then 'Serialized'
			WHEN charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=0 then 'Serialized Asset'
			WHEN charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=1 then 'Serialized Rental Asset'
			WHEN charItemType='P' AND ISNULL(bitLotNo,0)=1 THEN 'Lot #'
			ELSE CASE WHEN charItemType='P' THEN 'Inventory' ELSE '' END
		END AS InventoryItemType
		,dbo.fn_GetItemChildMembershipCount(@numDomainId,I.numItemCode) AS numChildMembershipCount
		,CASE WHEN ISNULL(bitAssembly,0) = 1 THEN dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode,ISNULL(W.numWareHouseID,0)) ELSE 0 END AS numWOQty
		,CASE WHEN charItemType='P' THEN ISNULL((SELECT TOP 1 monWListPrice FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode AND ISNULL(monWListPrice,0) > 0),0) ELSE monListPrice END monListPrice,                   
		numItemClassification, isnull(bitTaxable,0) as bitTaxable, ISNULL(vcSKU,'') AS vcItemSKU, 
		(CASE WHEN I.numItemGroup > 0 THEN ISNULL(W.[vcWHSKU],'') ELSE ISNULL(vcSKU,'') END) AS vcSKU, 
		ISNULL(bitKitParent,0) as bitKitParent, V.numVendorID, V.monCost, I.numDomainID, I.numCreatedBy, I.bintCreatedDate, I.bintModifiedDate,I.numModifiedBy,
		(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1 ) AS vcPathForImage ,
		(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1) AS  vcPathForTImage
		,ISNULL(bitSerialized,0) as bitSerialized, vcModelID,                   
		(SELECT 
			numItemImageId
			,vcPathForImage
			,vcPathForTImage
			,bitDefault
			,CASE WHEN bitdefault=1 THEN -1 ELSE ISNULL(intDisplayOrder,0) END AS intDisplayOrder 
		FROM 
			ItemImages  
		WHERE 
			numItemCode=@numItemCode 
		ORDER BY 
			CASE WHEN bitdefault=1 THEN -1 ELSE isnull(intDisplayOrder,0) END ASC 
		FOR XML AUTO,ROOT('Images')) AS xmlItemImages,
		(SELECT STUFF((SELECT ',' + CONVERT(VARCHAR(50) , numCategoryID)  FROM dbo.ItemCategory WHERE numItemID = I.numItemCode FOR XML PATH('')), 1, 1, '')) AS vcItemCategories ,
		numItemGroup, 
		(CASE WHEN ISNULL(numItemGroup,0) > 0 AND (SELECT COUNT(*) FROM CFW_Fld_Master CFW JOIN ItemGroupsDTL ON numOppAccAttrID=Fld_id WHERE numItemGroupID=numItemGroup AND tintType=2) > 0 THEN 1 ELSE 0 END) bitItemGroupHasAttributes,
		numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, ISNULL(bitExpenseItem,0) bitExpenseItem, ISNULL(numExpenseChartAcntId,0) numExpenseChartAcntId, (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE monAverageCost END) AS monAverageCost,                   
		monCampaignLabourCost,dbo.fn_GetContactName(I.numCreatedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,I.bintCreatedDate )) as CreatedBy ,                                      
		dbo.fn_GetContactName(I.numModifiedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,I.bintModifiedDate )) as ModifiedBy,                      
		sum(numOnHand) as numOnHand,                      
		sum(numOnOrder) as numOnOrder,                      
		MAX(numReorder)  as numReOrder,                      
		sum(numAllocation)  as numAllocation,                      
		sum(numBackOrder)  as numBackOrder,                   
		isnull(fltWeight,0) as fltWeight,                
		isnull(fltHeight,0) as fltHeight,                
		isnull(fltWidth,0) as fltWidth,                
		isnull(fltLength,0) as fltLength,                
		isnull(bitFreeShipping,0) as bitFreeShipping,              
		isnull(bitAllowBackOrder,0) as bitAllowBackOrder,              
		isnull(vcUnitofMeasure,'Units') as vcUnitofMeasure,      
		isnull(bitShowDeptItem,0) bitShowDeptItem,      
		isnull(bitShowDeptItemDesc,0) bitShowDeptItemDesc,  
		isnull(bitCalAmtBasedonDepItems ,0) bitCalAmtBasedonDepItems,
		isnull(bitAssembly ,0) bitAssembly ,
		isnull(cast(numBarCodeId as varchar(50)),'') as  numBarCodeId ,
		(CASE WHEN ISNULL(I.numManufacturer,0) > 0 THEN ISNULL((SELECT vcCompanyName FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=I.numManufacturer),'')  ELSE ISNULL(I.vcManufacturer,'') END) AS vcManufacturer,
		ISNULL(I.numManufacturer,0) numManufacturer,
		ISNULL(I.numBaseUnit,0) AS numBaseUnit,ISNULL(I.numPurchaseUnit,0) AS numPurchaseUnit,ISNULL(I.numSaleUnit,0) AS numSaleUnit,
		dbo.fn_GetUOMName(ISNULL(I.numBaseUnit,0)) AS vcBaseUnit,dbo.fn_GetUOMName(ISNULL(I.numPurchaseUnit,0)) AS vcPurchaseUnit,dbo.fn_GetUOMName(ISNULL(I.numSaleUnit,0)) AS vcSaleUnit,
		isnull(bitLotNo,0) as bitLotNo,
		ISNULL(IsArchieve,0) AS IsArchieve,
		case when charItemType='P' then 'Inventory Item' when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory Item' end as ItemType,
		isnull(I.numItemClass,0) as numItemClass,
		ISNULL(tintStandardProductIDType,0) tintStandardProductIDType,
		ISNULL(vcExportToAPI,'') vcExportToAPI,
		ISNULL(numShipClass,0) numShipClass,ISNULL(I.bitAllowDropShip,0) AS bitAllowDropShip,
		ISNULL(@bitItemIsUsedInOrder,0) AS bitItemIsUsedInOrder,
		ISNULL((SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID = I.numItemCode),0) AS intParentKitItemCount,
		ISNULL(I.bitArchiveItem,0) AS [bitArchiveItem],
		ISNULL(W.numWareHouseID,0) AS [numWareHouseID],
		ISNULL(W.numWareHouseItemID,0) AS [numWareHouseItemID],
		ISNULL((SELECT COUNT(WI.[numWareHouseItemID]) FROM [dbo].[WareHouseItems] WI WHERE WI.[numItemID] = I.numItemCode AND WI.[numItemID] = @numItemCode),0) AS [intTransCount],
		ISNULL(I.bitAsset,0) AS [bitAsset],
		ISNULL(I.bitRental,0) AS [bitRental],
		ISNULL(W.vcBarCode,ISNULL(I.[numBarCodeId],'')) AS [vcBarCode],
		ISNULL((SELECT COUNT(*) FROM ItemUOMConversion WHERE numItemCode=I.numItemCode),0) AS tintUOMConvCount,
		ISNULL(I.bitVirtualInventory,0) bitVirtualInventory ,
		ISNULL(I.bitContainer,0) bitContainer
		,ISNULL(I.numContainer,0) numContainer
		,ISNULL(I.numNoItemIntoContainer,0) numNoItemIntoContainer
		,ISNULL(I.bitMatrix,0) AS bitMatrix
		,ISNULL(@bitOppOrderExists,0) AS bitOppOrderExists
		,CPN.CustomerPartNo
		,ISNULL(I.vcASIN,'') AS vcASIN
		,ISNULL(I.tintKitAssemblyPriceBasedOn,1) AS tintKitAssemblyPriceBasedOn
		,ISNULL(I.fltReorderQty,0) AS fltReorderQty
		,ISNULL(bitSOWorkOrder,0) bitSOWorkOrder
		,ISNULL(bitKitSingleSelect,0) bitKitSingleSelect
	FROM 
		Item I       
	left join  
		WareHouseItems W                  
	on 
		W.numItemID=I.numItemCode                
	LEFT JOIN 
		ItemExtendedDetails IED 
	ON 
		I.numItemCode = IED.numItemCode
	LEFT JOIN 
		Vendor V 
	ON 
		I.numVendorID = V.numVendorID AND I.numItemCode = V.numItemCode  
	LEFT JOIN 
		CustomerPartNumber CPN 
	ON 
		I.numItemCode=CPN.numItemCode 
		AND CPN.numCompanyID=@numCompanyID
	WHERE 
		I.numItemCode=@numItemCode  
	GROUP BY 
		I.numItemCode, vcItemName, txtItemDesc, charItemType, monListPrice,                   
		numItemClassification, bitTaxable,vcSKU,[W].[vcWHSKU] , bitKitParent,V.numVendorID, V.monCost, I.numDomainID,               
		I.numCreatedBy, I.bintCreatedDate, I.bintModifiedDate,numContainer,   numNoItemIntoContainer,                
		I.numModifiedBy,bitAllowBackOrder, bitSerialized, vcModelID,                   
		numItemGroup, numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, monAverageCost, I.bitVirtualInventory, bitExpenseItem, numExpenseChartAcntId,                  
		monCampaignLabourCost,fltWeight,fltHeight,fltWidth,fltLength,bitFreeShipping,vcUnitofMeasure,bitShowDeptItemDesc,bitShowDeptItem,bitCalAmtBasedonDepItems,bitAssembly,numBarCodeId,I.vcManufacturer
		,I.numBaseUnit,I.numPurchaseUnit,I.numSaleUnit,I.bitContainer,I.bitLotNo,I.IsArchieve,I.numItemClass,I.tintStandardProductIDType,I.vcExportToAPI,I.numShipClass,
		CAST( ISNULL(IED.txtDesc,'')  AS VARCHAR(MAX) ),I.bitAllowDropShip,I.bitArchiveItem,I.bitAsset,I.bitRental,W.[numWarehouseID],W.numWareHouseItemID,W.[vcBarCode],I.bitMatrix
		,I.numManufacturer,CPN.CustomerPartNo,I.vcASIN,I.tintKitAssemblyPriceBasedOn,I.fltReorderQty,bitSOWorkOrder,bitKitSingleSelect
END
---Created By Anoop Jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageFieldRelationship')
DROP PROCEDURE USP_ManageFieldRelationship
GO
CREATE PROCEDURE USP_ManageFieldRelationship
@byteMode as tinyint,
@numFieldRelID as numeric(9),
@numModuleID NUMERIC(18,0),
@numPrimaryListID as numeric(9),
@numSecondaryListID as numeric(9),
@numDomainID as numeric(9),
@numFieldRelDTLID as numeric(9),
@numPrimaryListItemID as numeric(9),
@numSecondaryListItemID as numeric(9),
@bitTaskRelation AS BIT=0
as
BEGIN
if @byteMode =1
BEGIN
IF(@bitTaskRelation=1)
BEGIN
	IF NOT EXISTS(SELECT * FROM dbo.FieldRelationship WHERE numDomainID=@numDomainID AND numPrimaryListID=@numPrimaryListID)
	BEGIN
		insert into FieldRelationship (numPrimaryListID,numSecondaryListID,numDomainID,bitTaskRelation)
		values (@numPrimaryListID,@numSecondaryListID,@numDomainID,@bitTaskRelation)
		set @numFieldRelID=@@identity 
	END
	ELSE 
		set @numFieldRelID=0
END
ELSE
BEGIN
IF NOT EXISTS(SELECT * FROM dbo.FieldRelationship WHERE numDomainID=@numDomainID AND numPrimaryListID=@numPrimaryListID AND numSecondaryListID=@numSecondaryListID)
BEGIN
	insert into FieldRelationship (numPrimaryListID,numSecondaryListID,numDomainID,bitTaskRelation)
    values (@numPrimaryListID,@numSecondaryListID,@numDomainID,@bitTaskRelation)
    set @numFieldRelID=@@identity 
END
ELSE 
	set @numFieldRelID=0
END
END
else if @byteMode =2
begin

	 if not exists(select * from FieldRelationshipDTL where numFieldRelID=@numFieldRelID 
	and numPrimaryListItemID=@numPrimaryListItemID and numSecondaryListItemID=@numSecondaryListItemID)
		insert into FieldRelationshipDTL (numFieldRelID,numPrimaryListItemID,numSecondaryListItemID)
		values (@numFieldRelID,@numPrimaryListItemID,@numSecondaryListItemID)
	else
	set @numFieldRelID=0 -- to identify that there is already one

END
	else if @byteMode =3
	begin
		Delete from FieldRelationshipDTL where numFieldRelDTLID=@numFieldRelDTLID
	end
	else if @byteMode =4
	begin
		Delete from FieldRelationshipDTL where numFieldRelID=@numFieldRelID
		Delete from FieldRelationship where numFieldRelID=@numFieldRelID
	end

	select @numFieldRelID
END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_ManageInventory' ) 
    DROP PROCEDURE usp_ManageInventory
GO
CREATE PROCEDURE [dbo].[usp_ManageInventory]
    @itemcode AS NUMERIC(9) = 0,
    @numWareHouseItemID AS NUMERIC(9) = 0,
    @monAmount AS DECIMAL(20,5),
    @tintOpptype AS TINYINT,
    @numUnits AS FLOAT,
    @QtyShipped AS FLOAT,
    @QtyReceived AS FLOAT,
    @monPrice AS DECIMAL(20,5),
    @tintFlag AS TINYINT,  --1: SO/PO insert/edit 2:SO/PO Close 3:SO/PO Re-Open 4:Sales Fulfillment Release 5:Sales Fulfillment Revert
    @numOppID as numeric(9)=0,
    @numoppitemtCode AS NUMERIC(9)=0,
    @numUserCntID AS NUMERIC(9)=0,
	@bitWorkOrder AS BIT = 0   
AS 
	IF ISNULL(@numUserCntID,0) = 0
	BEGIN
		RAISERROR('INVALID_USERID',16,1)
		RETURN
	END

    DECLARE @onHand AS FLOAT                                    
    DECLARE @onOrder AS FLOAT 
    DECLARE @onBackOrder AS FLOAT
    DECLARE @onAllocation AS FLOAT
	DECLARE @onReOrder AS FLOAT
    DECLARE @monAvgCost AS DECIMAL(20,5) 
    DECLARE @bitKitParent BIT
	DECLARE @numOrigUnits AS NUMERIC		
	DECLARE @description AS VARCHAR(100)
	DECLARE @bitAsset as BIT
	DECLARE @numDomainID AS NUMERIC 
      
	SELECT @bitAsset=ISNULL(bitAsset,0),@numDomainID=numDomainID from Item where numItemCode=@itemcode--Added By Sachin
    
	SELECT  @monAvgCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost, 0) END) ,
            @bitKitParent = ( CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END )
    FROM    Item
    WHERE   numitemcode = @itemcode                                   
    
    IF @numWareHouseItemID>0
    BEGIN                     
		SELECT  @onHand = ISNULL(numOnHand, 0),
				@onAllocation = ISNULL(numAllocation, 0),
				@onOrder = ISNULL(numOnOrder, 0),
				@onBackOrder = ISNULL(numBackOrder, 0),
				@onReOrder = ISNULL(numReorder,0)
		FROM    WareHouseItems
		WHERE   numWareHouseItemID = @numWareHouseItemID 
    END
   
   DECLARE @dtItemReceivedDate AS DATETIME;SET @dtItemReceivedDate=NULL
   
   DECLARE @TotalOnHand AS NUMERIC;SET @TotalOnHand=0  
   SELECT @TotalOnHand=(SUM(ISNULL(numOnHand,0)) + SUM(ISNULL(numAllocation,0))) FROM dbo.WareHouseItems WHERE numItemID=@itemcode
					
   SET @numOrigUnits=@numUnits
            
    IF @tintFlag = 1  --1: SO/PO insert/edit 
    BEGIN
        IF @tintOpptype = 1 
        BEGIN  
		--When change below code also change to USP_ManageWorkOrderStatus  

			/* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
			SET @description='SO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
				
			IF @bitKitParent = 1 
            BEGIN
                EXEC USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,@numOrigUnits, 0,@numOppID,0,@numUserCntID
            END 
			ELSE IF @bitWorkOrder = 1
			BEGIN
				IF EXISTS (SELECT numWOID FROM WorkOrder WHERE numOppID=@numOppID AND numOppItemID=@numoppitemtCode AND numItemCode=@itemcode AND numWareHouseItemId=@numWareHouseItemID AND numWOStatus <> 23184)
				BEGIN
					SET @description='SO-WO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
					-- MANAGE INVENTOY OF ASSEMBLY ITEMS
					EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@numoppitemtCode,0,0,@itemcode,@numWareHouseItemID,@QtyShipped,1
				END
				ELSE
				BEGIN
					SET @description='SO-WO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'

					SET @numUnits = @numUnits - @QtyShipped
                                                        
					IF @onHand >= @numUnits 
					BEGIN                                    
						SET @onHand = @onHand - @numUnits                            
						SET @onAllocation = @onAllocation + @numUnits                                    
					END                                    
					ELSE IF @onHand < @numUnits 
					BEGIN                                    
						SET @onAllocation = @onAllocation + @onHand                                    
						SET @onBackOrder = @onBackOrder + @numUnits
							- @onHand                                    
						SET @onHand = 0                                    
					END    
			                                 
					IF @bitAsset=0--Not Asset
					BEGIN 
						UPDATE  WareHouseItems
						SET     numOnHand = @onHand,
								numAllocation = @onAllocation,
								numBackOrder = @onBackOrder,dtModified = GETDATE() 
						WHERE   numWareHouseItemID = @numWareHouseItemID          
					END
				END
				
			END
            ELSE
			BEGIN       
				SET @numUnits = @numUnits - @QtyShipped
                                                        
                IF @onHand >= @numUnits 
                BEGIN                                    
                    SET @onHand = @onHand - @numUnits                            
                    SET @onAllocation = @onAllocation + @numUnits                                    
                END                                    
                ELSE IF @onHand < @numUnits 
                BEGIN                                    
                    SET @onAllocation = @onAllocation + @onHand                                    
                    SET @onBackOrder = @onBackOrder + @numUnits
                        - @onHand                                    
                    SET @onHand = 0                                    
                END    
			                                 

				IF @bitAsset=0--Not Asset
				BEGIN 
					UPDATE  WareHouseItems
					SET     numOnHand = @onHand,
							numAllocation = @onAllocation,
							numBackOrder = @onBackOrder,dtModified = GETDATE() 
					WHERE   numWareHouseItemID = @numWareHouseItemID          
				END
			END  
        END                       
        ELSE IF @tintOpptype = 2 
        BEGIN  
			SET @description='PO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@QtyReceived AS VARCHAR(10)) + ')'
		            
			/* When QtyReceived has been updated form purchase fulfillment then it need to be deducted */
            SET @numUnits = @numUnits - @QtyReceived   
                                             
            SET @onOrder = @onOrder + @numUnits 
		    
            UPDATE  
				WareHouseItems
            SET     
				numOnHand = @onHand,
                numAllocation = @onAllocation,
                numBackOrder = @onBackOrder,
                numOnOrder = @onOrder,dtModified = GETDATE() 
            WHERE   
				numWareHouseItemID = @numWareHouseItemID   
        END                                                                                                                                         
    END
    ELSE IF @tintFlag = 2 --2:SO/PO Close
    BEGIN
        PRINT '@OppType=' + CONVERT(VARCHAR(5), @tintOpptype)
		    
        IF @tintOpptype = 1 
        BEGIN
			SET @description='SO Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
                    
            IF @bitKitParent = 1 
            BEGIN
                EXEC USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,
                    @numOrigUnits, 2,@numOppID,0,@numUserCntID
            END  
			ELSE IF @bitWorkOrder = 1
			BEGIN
				SET @description='SO-WO Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
				EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@numoppitemtCode,0,0,@itemcode,@numWareHouseItemID,@QtyShipped,2
			END
            ELSE
            BEGIN       
				/* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
				SET @numUnits = @numUnits - @QtyShipped
				IF @bitAsset=0--Not Asset
				BEGIN
					SET @onAllocation = @onAllocation - @numUnits 
				END
				ELSE--Asset
				BEGIN
					SET @onAllocation = @onAllocation  
				END
											           
				UPDATE 
					WareHouseItems
				SET 
					numOnHand = @onHand,
					numAllocation = @onAllocation,
					numBackOrder = @onBackOrder,
					dtModified = GETDATE() 
				WHERE 
					numWareHouseItemID = @numWareHouseItemID   
			END
        END                
        ELSE IF @tintOpptype = 2 
        BEGIN
			SET @description='PO Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@QtyReceived AS VARCHAR(10)) + ')'
                        
			/* When QtyReceived has been updated form purchase fulfillment then it need to be deducted */
            SET @numUnits = @numUnits - @QtyReceived
            
			If @numUnits > 0
			BEGIN
				IF EXISTS(SELECT 1 FROM dbo.OpportunityMaster WHERE numOppID=@numOppID AND ISNULL(bitStockTransfer,0)=0)
				BEGIN
					--Updating the Average Cost
					IF @TotalOnHand + @numUnits <= 0
					BEGIN
						SET @monAvgCost = @monAvgCost
					END
					ELSE
					BEGIN
						SET @monAvgCost = (( @TotalOnHand * @monAvgCost) + (@numUnits * @monPrice)) / ( @TotalOnHand + @numUnits )
					END    
		                            
					UPDATE 
						Item
					SET 
						monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monAvgCost END)
					WHERE 
						numItemCode = @itemcode
				END
		    
				IF @onOrder >= @numUnits 
				BEGIN
					SET @onOrder = @onOrder - @numUnits            
                
					IF @onBackOrder >= @numUnits 
					BEGIN            
						SET @onBackOrder = @onBackOrder - @numUnits            
						SET @onAllocation = @onAllocation + @numUnits            
					END            
					ELSE 
					BEGIN            
						SET @onAllocation = @onAllocation + @onBackOrder            
						SET @numUnits = @numUnits - @onBackOrder            
						SET @onBackOrder = 0            
						SET @onHand = @onHand + @numUnits            
					END         
				END            
				ELSE IF @onOrder < @numUnits 
				BEGIN
					RAISERROR('INVALID ON ORDER',16,1)
					RETURN
				END         

				UPDATE
					WareHouseItems
				SET 
					numOnHand = @onHand,
					numAllocation = @onAllocation,
					numBackOrder = @onBackOrder,
					numOnOrder = @onOrder,dtModified = GETDATE() 
				WHERE 
					numWareHouseItemID = @numWareHouseItemID  
			                
				SELECT @dtItemReceivedDate=dtItemReceivedDate FROM OpportunityMaster WHERE numOppId=@numOppId
			END
		END
    END
	ELSE IF @tintFlag = 3 --3:SO/PO Re-Open
    BEGIN
		PRINT '@OppType=' + CONVERT(VARCHAR(5), @tintOpptype)           
                
		IF @tintOpptype = 1 
        BEGIN
			SET @description='SO Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
                    
            IF @bitKitParent = 1 
            BEGIN
				EXEC USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,@numOrigUnits, 3,@numOppID,0,@numUserCntID
            END 
			ELSE IF @bitWorkOrder = 1
			BEGIN
				SET @description='SO-WO Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
				EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@numoppitemtCode,0,0,@itemcode,@numWareHouseItemID,@QtyShipped,3
			END
            ELSE
            BEGIN
				/* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
                SET @numUnits = @numUnits - @QtyShipped
  
				IF @bitAsset=0--Not Asset
				BEGIN
					SET @onAllocation = @onAllocation + @numUnits
				END
				ELSE--Asset
				BEGIN
					SET @onAllocation = @onAllocation 
				END
                                    
                UPDATE  
					WareHouseItems
                SET 
					numOnHand = @onHand,
                    numAllocation = @onAllocation,
                    numBackOrder = @onBackOrder,dtModified = GETDATE() 
                WHERE 
					numWareHouseItemID = @numWareHouseItemID
			END	               
        END                
        ELSE IF @tintOpptype = 2 
        BEGIN
			DECLARE @numDeletedReceievedQty AS NUMERIC(18,0)
			SELECT @numDeletedReceievedQty=ISNULL(numDeletedReceievedQty,0) FROM OpportunityItems WHERE numoppitemtcode=@numoppitemtCode
			UPDATE OpportunityItems SET numDeletedReceievedQty = 0 WHERE numoppitemtcode=@numoppitemtCode

			/* When QtyReceived has been updated form purchase fulfillment then it need to be deducted */
            SET @numUnits = @numUnits - @QtyReceived
            
			--Updating the Average Cost
			IF EXISTS(SELECT 1 FROM dbo.OpportunityMaster WHERE numOppID=@numOppID AND ISNULL(bitStockTransfer,0)=0)
			BEGIN
				IF @TotalOnHand - @numUnits <= 0
				BEGIN
					SET @monAvgCost = 0
				END
				ELSE
				BEGIN
					SET @monAvgCost = ((@TotalOnHand * @monAvgCost) - (@numUnits * @monPrice)) / ( @TotalOnHand - @numUnits )
				END        
				
				UPDATE  
					item
				SET 
					monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monAvgCost END)
				WHERE 
					numItemCode = @itemcode
			END


			DECLARE @TEMPReceievedItems TABLE
			(
				ID INT,
				numOIRLID NUMERIC(18,0),
				numWarehouseItemID NUMERIC(18,0),
				numUnitReceieved FLOAT,
				numDeletedReceievedQty FLOAT
			)

			INSERT INTO
				@TEMPReceievedItems
			SELECT
				ROW_NUMBER() OVER(ORDER BY ID),
				ID,
				numWarehouseItemID,
				numUnitReceieved,
				ISNULL(numDeletedReceievedQty,0)
			FROM
				OpportunityItemsReceievedLocation 
			WHERE
				numDomainID=@numDomainID
				AND numOppID=@numOppID
				AND numOppItemID=@numoppitemtCode

			DECLARE @i AS INT = 1
			DECLARE @COUNT AS INT
			DECLARE @numOIRLID NUMERIC(18,0)
			DECLARE @numTempWarehouseItemID NUMERIC(18,0)
			DECLARE @numTempUnitReceieved FLOAT
			DECLARE @numTempDeletedReceievedQty FLOAT

			SELECT @COUNT=COUNT(*) FROM @TEMPReceievedItems

			WHILE @i <= @COUNT
			BEGIN
				SELECT 
					@numOIRLID = numOIRLID,
					@numTempWarehouseItemID=ISNULL(numWarehouseItemID,0),
					@numTempUnitReceieved=ISNULL(numUnitReceieved,0),
					@numTempDeletedReceievedQty = numDeletedReceievedQty
				FROM 
					@TEMPReceievedItems 
				WHERE 
					ID=@i

				SET @description='PO Re-Open (Qty:' + CAST(@numTempUnitReceieved AS VARCHAR(10)) + ' Received:' +  CAST(@QtyReceived AS VARCHAR(10)) + ')'

				UPDATE
					WareHouseItems
				SET
					numOnHand = ISNULL(numOnHand,0) - (ISNULL(@numTempUnitReceieved,0) - @numTempDeletedReceievedQty),
					dtModified = GETDATE()
				WHERE
					numWareHouseItemID=@numTempWarehouseItemID

				UPDATE OpportunityItemsReceievedLocation SET numDeletedReceievedQty = 0 WHERE ID=@numOIRLID

				EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numTempWarehouseItemID, --  numeric(9, 0)
					@numReferenceID = @numOppID, --  numeric(9, 0)
					@tintRefType = 4, --  tinyint
					@vcDescription = @description, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@dtRecordDate =  @dtItemReceivedDate,
					@numDomainID = @numDomainID

				SET @i = @i + 1
			END

			DELETE FROM OpportunityItemsReceievedLocation WHERE numOppID=@numOppID AND numOppItemID=@numoppitemtCode

			SET @description='PO Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@QtyReceived AS VARCHAR(10)) + ' Deleted:' + CAST(@numDeletedReceievedQty AS VARCHAR) + ')'
             
			UPDATE  
				WareHouseItems
            SET 
				numOnHand = @onHand - (@numUnits-@numDeletedReceievedQty)
                ,numOnOrder = @onOrder + @numUnits
				,dtModified = GETDATE() 
            WHERE 
				numWareHouseItemID = @numWareHouseItemID
				AND ISNULL(numWLocationID,0) <> -1    
				
			UPDATE  
				WareHouseItems
            SET 
                numOnOrder = @onOrder + @numUnits
				,dtModified = GETDATE() 
            WHERE 
				numWareHouseItemID = @numWareHouseItemID
				AND ISNULL(numWLocationID,0) = -1    
        END
    END
            
            
            
    IF @numWareHouseItemID>0 AND @description <> 'DO NOT ADD WAREHOUSE TRACKING'
	BEGIN
		DECLARE @tintRefType AS TINYINT
					
		IF @tintOpptype = 1 --SO
			SET @tintRefType=3
		ELSE IF @tintOpptype = 2 --PO
			SET @tintRefType=4
					  
		DECLARE @numDomain AS NUMERIC(18,0)
		SET @numDomain = (SELECT TOP 1 [numDomainID] FROM WareHouseItems WHERE [WareHouseItems].[numWareHouseItemID] = @numWareHouseItemID)

		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppID, --  numeric(9, 0)
			@tintRefType = @tintRefType, --  tinyint
			@vcDescription = @description, --  varchar(100)
			@numModifiedBy = @numUserCntID,
			@dtRecordDate =  @dtItemReceivedDate,
			@numDomainID = @numDomain
    END
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageSalesprocessList]    Script Date: 07/26/2008 16:19:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managesalesprocesslist')
DROP PROCEDURE usp_managesalesprocesslist
GO
CREATE PROCEDURE [dbo].[USP_ManageSalesprocessList]
    @numSalesProsID AS NUMERIC(9) = 0,
    @vcSlpName AS VARCHAR(50) = '',
    @numUserCntID AS NUMERIC(9) = 0,
    @tintProType AS TINYINT,
    @numDomainID AS NUMERIC(9) = 0,
    @numStageNumber AS NUMERIC = 0,
	@bitAssigntoTeams AS BIT=0,
	@bitAutomaticStartTimer AS BIT=0
AS 
IF @numSalesProsID = 0 
    BEGIN        
        INSERT  INTO Sales_process_List_Master ( Slp_Name,
                                                 numdomainid,
                                                 pro_type,
                                                 numCreatedby,
                                                 dtCreatedon,
                                                 numModifedby,
                                                 dtModifiedOn,tintConfiguration,bitAutomaticStartTimer,bitAssigntoTeams)
        VALUES  (
                  @vcSlpName,
                  @numDomainID,
                  @tintProType,
                  @numUserCntID,
                  GETUTCDATE(),
                  @numUserCntID,
                  GETUTCDATE(),@numStageNumber,@bitAutomaticStartTimer,@bitAssigntoTeams
                )        
        SET @numSalesProsID = SCOPE_IDENTITY()        
 
        SELECT  @numSalesProsID     
    END        
ELSE 
    BEGIN        
        UPDATE  Sales_process_List_Master
        SET     Slp_Name = @vcSlpName,
                numModifedby = @numUserCntID,
                dtModifiedOn = GETUTCDATE(),
				bitAssigntoTeams=@bitAssigntoTeams,
				bitAutomaticStartTimer=@bitAutomaticStartTimer
        WHERE   Slp_Id = @numSalesProsID        
        SELECT  @numSalesProsID        
        
    END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageShippingGlobalValues' ) 
    DROP PROCEDURE USP_ManageShippingGlobalValues
GO
CREATE PROCEDURE USP_ManageShippingGlobalValues   
     @numDomainID NUMERIC(9)
	,@minShippingCost FLOAT
	,@bitEnableStaticShipping BIT
	,@bitEnableShippingExceptions BIT
AS 
BEGIN
      
	IF EXISTS(
				SELECT * FROM dbo.Domain 
					WHERE numDomainID = @numDomainID
			    )
        BEGIN
	           UPDATE Domain
				   SET	 minShippingCost = @minShippingCost
						,bitEnableStaticShippingRule = @bitEnableStaticShipping 
						,bitEnableShippingExceptions = @bitEnableShippingExceptions
				WHERE numDomainID = @numDomainID  
        END	
		
	ELSE
		BEGIN
			INSERT INTO Domain
			(
				 numDomainID
				,minShippingCost
				,bitEnableStaticShippingRule
				,bitEnableShippingExceptions
			)
			VALUES
			(
				 @numDomainID
				,@minShippingCost
				,@bitEnableStaticShipping
				,@bitEnableShippingExceptions
			)
		END
			
END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageShippingServiceType' ) 
    DROP PROCEDURE USP_ManageShippingServiceType
GO 
CREATE PROCEDURE USP_ManageShippingServiceType
    @byteMode TINYINT = 0,
    @numServiceTypeID NUMERIC,
    @numDomainID NUMERIC,
    @numRuleID NUMERIC,
    @vcServiceName VARCHAR(100),
    @intNsoftEnum INT=0,
    @intFrom INT,
    @intTo INT,
    @fltMarkup FLOAT,
    @bitMarkupType BIT,
    @monRate DECIMAL(20,5),
    @bitEnabled BIT,
    @strItems AS text
AS 
BEGIN
	
    IF @byteMode = 0 
        BEGIN
            IF @numServiceTypeID = 0 
                BEGIN
                    INSERT  INTO dbo.ShippingServiceTypes ( vcServiceName,
                                                            intNsoftEnum,
                                                            intFrom,
                                                            intTo,
                                                            fltMarkup,
                                                            bitMarkupType,
                                                            monRate,
                                                            numRuleID,
                                                            numDomainID )
                    VALUES  (
                              @vcServiceName,
                              @intNsoftEnum,
                              @intFrom,
                              @intTo,
                              @fltMarkup,
                              @bitMarkupType,
                              @monRate,
                              @numRuleID,
                              @numDomainID 
                            ) 
                END
            ELSE 
                BEGIN
                    UPDATE  dbo.ShippingServiceTypes
                    SET     vcServiceName = @vcServiceName,
                            intNsoftEnum = @intNsoftEnum,
                            intFrom = @intFrom,
                            intTo = @intTo,
                            fltMarkup = @fltMarkup,
                            bitMarkupType = @bitMarkupType,
                            monRate = @monRate,
                            bitEnabled=@bitEnabled
                    WHERE   numServiceTypeID = @numServiceTypeID
                            AND numDomainID = @numDomainID
			
                END
		
        END
	IF @byteMode = 1
	BEGIN
		DELETE FROM dbo.ShippingServiceTypes WHERE numServiceTypeID=@numServiceTypeID AND numDomainID=@numDomainID
	END
	
	IF @byteMode = 2 
	BEGIN
	    IF @numServiceTypeID = 2
		BEGIN
		    UPDATE dbo.ShippingServiceTypes SET bitEnabled = 0 WHERE intNsoftEnum = 0 AND numRuleID = @numRuleID
		END
	     
		DECLARE  @hDocItem INT

		IF CONVERT(VARCHAR(10),@strItems) <> ''
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT,@strItems
			
			UPDATE 
				dbo.ShippingServiceTypes
			SET 
				vcServiceName=X.vcServiceName,
				intFrom =X.intFrom,
				intTo=X.intTo,
				fltMarkup=X.fltMarkup,
				bitMarkupType=X.bitMarkupType,
				monRate=X.monRate,
				bitEnabled=X.bitEnabled
			FROM 
			(
				SELECT 
					*
				FROM 
					OPENXML (@hDocItem, '/NewDataSet/Item', 2)
				WITH 
				(
					numServiceTypeID NUMERIC(9),
					vcServiceName VARCHAR(100),
					intFrom INT,
					intTo INT,
					fltMarkup FLOAT,
					bitMarkupType BIT,
					monRate DECIMAL(20,5),
					bitEnabled bit
				)
			) X					
			WHERE 
				dbo.ShippingServiceTypes.numServiceTypeID= X.numServiceTypeID
				AND dbo.ShippingServiceTypes.numDomainID=@numDomainID
				
				EXEC sp_xml_removedocument @hDocItem
		END
	END
END
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageSOWorkOrder')
DROP PROCEDURE USP_ManageSOWorkOrder
GO
CREATE PROCEDURE [dbo].[USP_ManageSOWorkOrder]
@numDomainID AS numeric(18,0)=0,
@numOppID AS numeric(18,0)=0,
@strItems AS TEXT,
@numUserCntID AS numeric(9)=0
AS
BEGIN
	IF (SELECT COUNT(*) FROM WorkOrder WHERE numOppId=@numOppId)=0
	BEGIN
		DECLARE @numItemCode AS NUMERIC(18,0),@numUnitHour AS FLOAT,@numWarehouseItmsID AS NUMERIC(18,0),@vcItemDesc AS VARCHAR(1000)
  
		DECLARE @numWOId AS NUMERIC(18,0),@vcInstruction AS VARCHAR(1000),@bintCompliationDate AS DATETIME,@numWOAssignedTo NUMERIC(18,0), @numWarehouseID NUMERIC(18,0)

		DECLARE @numoppitemtCode NUMERIC(18,0)
		DECLARE @numOppChildItemID NUMERIC(18,0)
		DECLARE @numOppKitChildItemID NUMERIC(18,0)
  
		DECLARE WOCursor CURSOR FOR 
		SELECT 
			numoppitemtCode
			,0 AS numOppChildItemID
			,0 AS numOppKitChildItemID
			,numItemCode
			,numUnitHour
			,numWarehouseItmsID
			,vcItemDesc
			,vcInstruction
			,bintCompliationDate
			,numWOAssignedTo
		FROM 
			OpportunityItems                                                                         
		WHERE 
			numOppID=@numOppID
			AND ISNULL(bitWorkOrder,0)=1
		UNION ALL
		SELECT
			numOppItemID
			,numOppChildItemID
			,0 AS numOppKitChildItemID
			,Item.numItemCode
			,OKI.numQtyItemsReq
			,OKI.numWareHouseItemId
			,vcItemDesc
			,vcInstruction
			,bintCompliationDate
			,numWOAssignedTo
		FROM
			OpportunityKitItems OKI
		INNER JOIN
			OpportunityItems OI
		ON
			OKI.numOppItemID=OI.numoppitemtCode
		INNER JOIN
			Item IMain
		ON
			OI.numItemCode = IMain.numItemCode
		INNER JOIN
			Item 
		ON
			OKI.numChildItemID = Item.numItemCode
		WHERE 
			OKI.numOppID=@numOppID
			AND ISNULL(Item.bitAssembly,0)=1
			AND ISNULL(IMain.bitKitParent,0) = 1
			AND ISNULL(OKI.numWareHouseItemId,0) > 0
		UNION ALL
		SELECT
			numOppItemID
			,OKCI.numOppChildItemID
			,OKCI.numOppKitChildItemID
			,Item.numItemCode
			,OKCI.numQtyItemsReq
			,OKCI.numWareHouseItemId
			,vcItemDesc
			,vcInstruction
			,bintCompliationDate
			,numWOAssignedTo
		FROM
			OpportunityKitChildItems OKCI
		INNER JOIN
			OpportunityItems OI
		ON
			OKCI.numOppItemID=OI.numoppitemtCode
		INNER JOIN
			Item IMain
		ON
			OI.numItemCode = IMain.numItemCode
		INNER JOIN
			Item 
		ON
			OKCI.numItemID = Item.numItemCode
		WHERE 
			OKCI.numOppID=@numOppID
			AND ISNULL(Item.bitAssembly,0)=1
			AND ISNULL(IMain.bitKitParent,0) = 1
			AND ISNULL(OKCI.numWareHouseItemId,0) > 0	

		OPEN WOCursor;

		FETCH NEXT FROM WOCursor INTO @numoppitemtCode,@numOppChildItemID,@numOppKitChildItemID,@numItemCode,@numUnitHour,@numWarehouseItmsID,@vcItemDesc,@vcInstruction,@bintCompliationDate,@numWOAssignedTo

		WHILE @@FETCH_STATUS = 0
		BEGIN

			SET @numWarehouseID = ISNULL((SELECT numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID = @numWarehouseItmsID),0)

			IF EXISTS (SELECT 
						item.numItemCode
					FROM 
						item                                
					INNER JOIN 
						ItemDetails Dtl 
					ON 
						numChildItemID=numItemCode
					WHERE 
						numItemKitID=@numItemCode
						AND charItemType='P'
						AND ISNULL((SELECT TOP 1 numWareHouseItemId FROM WareHouseItems WHERE numItemID=item.numItemCode AND numWareHouseID=@numWarehouseID ORDER BY numWareHouseItemID),0) = 0)
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
				RETURN
			END


			INSERT INTO WorkOrder
			(
				numItemCode
				,numQtyItemsReq
				,numWareHouseItemId
				,numCreatedBy
				,bintCreatedDate
				,numDomainID
				,numWOStatus
				,numOppId
				,vcItemDesc
				,vcInstruction
				,bintCompliationDate
				,numAssignedTo
				,numOppItemID
				,numOppChildItemID
				,numOppKitChildItemID
			)
			VALUES
			(
				@numItemCode
				,@numUnitHour
				,@numWarehouseItmsID
				,@numUserCntID
				,GETUTCDATE()
				,@numDomainID
				,0
				,@numOppID
				,@vcItemDesc
				,@vcInstruction
				,@bintCompliationDate
				,@numWOAssignedTo
				,@numoppitemtCode
				,@numOppChildItemID
				,@numOppKitChildItemID
			)
	
			SET @numWOId = SCOPE_IDENTITY()
	

			IF @numWOAssignedTo>0
			BEGIN
				DECLARE @numDivisionId AS NUMERIC(9) ,@vcItemName AS VARCHAR(300)
	
				SELECT 
					@vcItemName=ISNULL(vcItemName,'') 
				FROM 
					Item 
				WHERE 
					numItemCode=@numItemCode

				SET @vcItemName= 'Work Order for Item : ' + @vcItemName

				SELECT @numDivisionId=numDivisionId FROM AdditionalContactsInformation WHERE numContactId=@numWOAssignedTo

				EXEC dbo.usp_InsertCommunication
					@numCommId = 0, --  numeric(18, 0)
					@bitTask = 972, --  numeric(18, 0)
					@numContactId = @numUserCntID, --  numeric(18, 0)
					@numDivisionId = @numDivisionId, --  numeric(18, 0)
					@txtDetails = @vcItemName, --  text
					@numOppId = 0, --  numeric(18, 0)
					@numAssigned = @numWOAssignedTo, --  numeric(18, 0)
					@numUserCntID = @numUserCntID, --  numeric(18, 0)
					@numDomainId = @numDomainID, --  numeric(18, 0)
					@bitClosed = 0, --  tinyint
					@vcCalendarName = '', --  varchar(100)
					@dtStartTime = @bintCompliationDate, --  datetime
					@dtEndtime = @bintCompliationDate, --  datetime
					@numActivity = 0, --  numeric(9, 0)
					@numStatus = 0, --  numeric(9, 0)
					@intSnoozeMins = 0, --  int
					@tintSnoozeStatus = 0, --  tinyint
					@intRemainderMins = 0, --  int
					@tintRemStaus = 0, --  tinyint
					@ClientTimeZoneOffset = 0, --  int
					@bitOutLook = 0, --  tinyint
					@bitSendEmailTemplate = 0, --  bit
					@bitAlert = 0, --  bit
					@numEmailTemplate = 0, --  numeric(9, 0)
					@tintHours = 0, --  tinyint
					@CaseID = 0, --  numeric(18, 0)
					@CaseTimeId = 0, --  numeric(18, 0)
					@CaseExpId = 0, --  numeric(18, 0)
					@ActivityId = 0, --  numeric(18, 0)
					@bitFollowUpAnyTime = 0, --  bit
					@strAttendee=''
			END

			INSERT INTO [WorkOrderDetails] 
			(
				numWOId,numItemKitID,numChildItemID,numQtyItemsReq,numWareHouseItemId,vcItemDesc,sintOrder,numQtyItemsReq_Orig,numUOMId
			)
			SELECT 
				@numWOId,numItemKitID,numItemCode,
				CAST(((DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(Dtl.numUOMID,item.numItemCode,item.numDomainID,item.numBaseUnit),1)) * @numUnitHour)AS FLOAT),
				isnull((SELECT TOP 1 numWareHouseItemId FROM WareHouseItems WHERE numItemID=item.numItemCode AND numWareHouseID=@numWarehouseID ORDER BY numWareHouseItemID),0),
				ISNULL(Dtl.vcItemDesc,txtItemDesc),
				ISNULL(sintOrder,0),
				DTL.numQtyItemsReq,
				Dtl.numUOMId 
			FROM 
				item                                
			INNER JOIN 
				ItemDetails Dtl 
			ON 
				numChildItemID=numItemCode
			WHERE 
				numItemKitID=@numItemCode
	
			FETCH NEXT FROM WOCursor INTO @numoppitemtCode,@numOppChildItemID,@numOppKitChildItemID,@numItemCode, @numUnitHour,@numWarehouseItmsID,@vcItemDesc,@vcInstruction,@bintCompliationDate,@numWOAssignedTo
		END

		CLOSE WOCursor;
		DEALLOCATE WOCursor;   
	END 
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_MirrorOPPBizDocItems' ) 
    DROP PROCEDURE USP_MirrorOPPBizDocItems
GO
CREATE PROCEDURE [dbo].[USP_MirrorOPPBizDocItems]
    (
      @numOppId AS NUMERIC(9) = NULL,
      @numDomainID AS NUMERIC(9) = 0      
    )
AS 
    DECLARE @DivisionID AS NUMERIC(9)      
    DECLARE @tintTaxOperator AS TINYINT   
    DECLARE @tintType AS TINYINT   
    DECLARE @tintOppStatus AS TINYINT   
    
    DECLARE @tintOppType AS TINYINT   
    DECLARE @numBizDocTempID AS NUMERIC(9) ;
      SET @numBizDocTempID = 0
                                                                                                                                                                                                                                                                                                                                          
    SELECT  @DivisionID = numDivisionID, @tintTaxOperator = [tintTaxOperator]  ,
    @tintType = tintOppType,@tintOppStatus=tintOppStatus,
            @tintOppType = tintOppType   ,@numBizDocTempID = ISNULL(numOppBizDocTempID, 0)      
    FROM    OpportunityMaster
    WHERE   numOppId = @numOppId    
                                                                                                                                                                          
--Decimal points to show in bizdoc 
    DECLARE @DecimalPoint AS TINYINT
	DECLARE @tintCommitAllocation TINYINT

    SELECT 
		@DecimalPoint = tintDecimalPoints
		,@tintCommitAllocation=ISNULL(tintCommitAllocation,1)
    FROM 
		Domain
    WHERE 
		numDomainID = @numDomainID
                                                                                          
    DECLARE @strSQL AS VARCHAR(MAX)                                                                                          
    DECLARE @strSQL1 AS VARCHAR(8000)                                       
    DECLARE @strSQLCusFields VARCHAR(1000)                      
    DECLARE @strSQLEmpFlds VARCHAR(500)                                                                         
    SET @strSQLCusFields = ''                                                                                          
    SET @strSQLEmpFlds = ''                                                                                          
                                                   
    DECLARE @intRowNum AS INT                                                  
    DECLARE @numFldID AS VARCHAR(15)                                          
    DECLARE @vcFldname AS VARCHAR(50)                                            
    DECLARE @numBizDocId AS NUMERIC(9)
                                          
    
       IF @tintOppType=1 AND @tintOppStatus=0
			SELECT  @numBizDocID = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Sales Opportunity' AND constFlag=1
       ELSE IF @tintOppType=1 AND @tintOppStatus=1
			SELECT  @numBizDocID = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Sales Order' AND constFlag=1
       ELSE IF @tintOppType=2 AND @tintOppStatus=0
			SELECT  @numBizDocID = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Purchase Opportunity' AND constFlag=1
       ELSE IF @tintOppType=2 AND @tintOppStatus=1
	 		SELECT  @numBizDocID = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Purchase Order' AND constFlag=1
                                                                              
    IF @tintType = 1 
        SET @tintType = 7
    ELSE 
        SET @tintType = 8                                                                                      
                                                                                
    SELECT  *
    INTO    #Temp1
    FROM    ( SELECT   Opp.vcitemname AS vcItemName,
                        0 OppBizDocItemID,
                        CASE WHEN charitemType = 'P' THEN 'Product'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS charitemType,
                        CASE WHEN bitShowDeptItemDesc = 0
                             THEN CONVERT(VARCHAR(500), Opp.vcitemDesc)
                             WHEN bitShowDeptItemDesc = 1
                                  AND bitKitParent = 1
                                  AND bitAssembly = 0
                             THEN dbo.fn_PopulateKitDesc(opp.numOppId,
                                                         opp.numoppitemtCode)
                             WHEN bitShowDeptItemDesc = 1
                                  AND bitKitParent = 1
                                  AND bitAssembly = 1
                             THEN dbo.fn_PopulateAssemblyDesc(opp.numoppitemtCode, Opp.numUnitHour)
                             ELSE CONVERT(VARCHAR(500), Opp.vcitemDesc)
                        END AS txtItemDesc,                                      
--vcitemdesc as [desc],                                      
                        dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),
                                             i.numItemCode, @numDomainID,
                                             ISNULL(opp.numUOMId, 0))
                        * Opp.numUnitHour AS numUnitHour,
                        CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4), dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0))
                        * Opp.monPrice)) monPrice,
                        CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4), Opp.monTotAmount)) Amount,
                        Opp.monTotAmount/*Fo calculating sum*/,
                        ISNULL(CONVERT(VARCHAR, i.monListPrice), 0) AS monListPrice,
                        i.numItemCode AS ItemCode,i.numItemCode,
                        opp.numoppitemtCode,
                        L.vcdata AS numItemClassification,
                        CASE WHEN bitTaxable = 0 THEN 'No'
                             ELSE 'Yes'
                        END AS Taxable,
                        numIncomeChartAcntId AS itemIncomeAccount,
                        'NI' AS ItemType,
                        0 AS numJournalId,
                        0 AS numTransactionId,
                        ISNULL(Opp.vcModelID, '') vcModelID,
                        dbo.USP_GetAttributes(Opp.numWarehouseItmsID,
                                              bitSerialized) AS vcAttributes,
                        ISNULL(vcPartNo, '') AS vcPartNo,
                        ( ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
                          - Opp.monTotAmount ) AS DiscAmt,
						  (CASE WHEN opp.numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(18, 4),(dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * (ISNULL(opp.monTotAmount,0) / opp.numUnitHour))) END) As monUnitSalePrice,
                        '' vcNotes,
                        '' vcTrackingNo,
                        '' vcShippingMethod,
                        0 monShipCost,
                        NULL dtDeliveryDate,
                        Opp.numWarehouseItmsID,
--ISNULL(I.vcUnitofMeasure,'Units') vcUnitofMeasure,
                        ISNULL(u.vcUnitName, '') numUOMId,
                        ISNULL(Opp.vcManufacturer, '') AS vcManufacturer,
                        ISNULL(V.monCost, 0) AS VendorCost,
                        Opp.vcPathForTImage,
                        i.[fltLength],
                        i.[fltWidth],
                        i.[fltHeight],
                        i.[fltWeight],
                        ISNULL(I.numVendorID, 0) numVendorID,
                        Opp.bitDropShip AS DropShip,
                        SUBSTRING(( SELECT  ' ,' + vcSerialNo
                                            + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                                   THEN ' ('
                                                        + CONVERT(VARCHAR(15), oppI.numQty)
                                                        + ')'
                                                   ELSE ''
                                              END
                                    FROM    OppWarehouseSerializedItem oppI
                                            JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                    WHERE   oppI.numOppID = Opp.numOppId
                                            AND oppI.numOppItemID = Opp.numoppitemtCode
                                    ORDER BY vcSerialNo
                                  FOR
                                    XML PATH('')
                                  ), 3, 200000) AS SerialLotNo,
                        ISNULL(opp.numUOMId, 0) AS numUOM,
                        ISNULL(u.vcUnitName, '') vcUOMName,
                        dbo.fn_UOMConversion(opp.numUOMId, Opp.numItemCode,
                                             @numDomainID, NULL) AS UOMConversionFactor,
                        ISNULL(Opp.numSOVendorId, 0) AS numSOVendorId,
                        NULL dtRentalStartDate,
                        NULL dtRentalReturnDate,
                        ISNULL(W.vcWareHouse, '') AS Warehouse,
                        CASE WHEN charitemType = 'P' THEN WI.vcWHSKU
                             ELSE i.vcSKU
                        END vcSKU,
                        CASE WHEN charitemType = 'P' THEN WI.vcBarCode
                             ELSE i.numBarCodeId
                        END vcBarcode,
                        ISNULL(opp.numClassID, 0) AS numClassID,
                        ISNULL(opp.numProjectID, 0) AS numProjectID,
                        i.numShipClass,
						(CASE WHEN opp.numUnitHour > ISNULL(WI.numAllocation,0) THEN (opp.numUnitHour - ISNULL(WI.numAllocation,0)) * dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(opp.numUOMId, 0))  ELSE 0 END) AS numBackOrder,
						ISNULL(opp.vcPromotionDetail,'') AS vcPromotionDetail,
						ISNULL(dbo.GetOrderAssemblyKitInclusionDetails(opp.numOppID,Opp.numoppitemtCode,opp.numUnitHour,@tintCommitAllocation,1),'') AS vcInclusionDetails,
						(CASE WHEN ISNULL(WI.numWareHouseItemID,0) > 0 THEN CONCAT(W.vcWareHouse,CASE WHEN WL.numWLocationID > 0 THEN CONCAT('(',WL.vcLocation ,')') ELSE '' END) ELSE '' END) vcWarehouse,
						opp.numSortOrder
              FROM      OpportunityItems opp
                        LEFT JOIN item i ON opp.numItemCode = i.numItemCode
                        LEFT JOIN ListDetails L ON i.numItemClassification = L.numListItemID
                        LEFT JOIN Vendor V ON V.numVendorID = @DivisionID
                                              AND V.numItemCode = opp.numItemCode
                        LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId                                                                          
                        LEFT JOIN dbo.WareHouseItems WI ON opp.numWarehouseItmsID = WI.numWareHouseItemID
                                                           AND WI.numDomainID = @numDomainID
                        LEFT JOIN dbo.Warehouses W ON WI.numDomainID = W.numDomainID
                                                      AND WI.numWareHouseID = W.numWareHouseID
						LEFT JOIN 
							dbo.WarehouseLocation WL 
						ON 
							WL.numWLocationID = WI.numWLocationID
              WHERE     opp.numOppId = @numOppId
            ) X


    IF @DecimalPoint = 1 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 1), monPrice),
                    Amount = CONVERT(DECIMAL(18, 1), Amount)
        END
    IF @DecimalPoint = 2 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 2), monPrice),
                    Amount = CONVERT(DECIMAL(18, 2), Amount)
        END
    IF @DecimalPoint = 3 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 3), monPrice),
                    Amount = CONVERT(DECIMAL(18, 3), Amount)
        END
    IF @DecimalPoint = 4 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 4), monPrice),
                    Amount = CONVERT(DECIMAL(18, 4), Amount)
        END

    DECLARE @strSQLUpdate AS VARCHAR(2000);SET @strSQLUpdate=''
   
    EXEC ('ALTER TABLE #Temp1 ADD [TotalTax] DECIMAL(20,5)')
	EXEC ('ALTER TABLE #Temp1 ADD [CRV] DECIMAL(20,5)')

    IF @tintOppType = 1 AND @tintTaxOperator = 1 --add Sales tax
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'

		SET @strSQLUpdate = @strSQLUpdate
            + ',[CRV]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',1,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'
	END
    ELSE IF @tintOppType = 1 AND @tintTaxOperator = 2 -- remove sales tax
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
		SET @strSQLUpdate = @strSQLUpdate + ',[CRV]= 0'
	END
    ELSE IF @tintOppType = 1
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'

		SET @strSQLUpdate = @strSQLUpdate
            + ',[CRV]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',1,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'
	END
    ELSE
	BEGIN 
        SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
		SET @strSQLUpdate = @strSQLUpdate + ',[CRV]= 0'
	END

	DECLARE @vcTaxName AS VARCHAR(100)
    DECLARE @numTaxItemID AS NUMERIC(9)
    SET @numTaxItemID = 0
    SELECT TOP 1
            @vcTaxName = vcTaxName,
            @numTaxItemID = numTaxItemID
    FROM    TaxItems
    WHERE   numDomainID = @numDomainID


    WHILE @numTaxItemID > 0
        BEGIN

            EXEC ( 'alter table #Temp1 add [' + @vcTaxName + '] DECIMAL(20,5)'
                )

            IF @tintOppType = 1 
                SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName
                    + ']= dbo.fn_CalBizDocTaxAmt('
                    + CONVERT(VARCHAR(20), @numDomainID) + ','
                    + CONVERT(VARCHAR(20), @numTaxItemID) + ','
                    + CONVERT(VARCHAR(20), @numOppId) +','+
                    + 'numoppitemtCode,1,Amount,numUnitHour)'
            ELSE 
                SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName + ']= 0'

            SELECT TOP 1
                    @vcTaxName = vcTaxName,
                    @numTaxItemID = numTaxItemID
            FROM    TaxItems
            WHERE   numDomainID = @numDomainID
                    AND numTaxItemID > @numTaxItemID

            IF @@rowcount = 0 
                SET @numTaxItemID = 0


        END

    IF @strSQLUpdate <> '' 
        BEGIN
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode'
                + @strSQLUpdate + ' where ItemCode>0'
            PRINT @strSQLUpdate
            EXEC ( @strSQLUpdate
                )
        END

    DECLARE @vcDbColumnName NVARCHAR(50)
    SELECT TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow                                                                                          
    WHILE @intRowNum > 0                                                                                  
        BEGIN    
            EXEC
                ( 'alter table #Temp1 add [' + @vcDbColumnName
                  + '] varchar(100)'
                )
	
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode,['
                + @vcDbColumnName + ']=dbo.GetCustFldValueItem(' + @numFldID
                + ',ItemCode) where ItemCode>0'
            EXEC ( @strSQLUpdate
                )
		                   
            SELECT TOP 1
                    @numFldID = numfieldID,
                    @vcFldname = vcFieldName,
                    @vcDbColumnName = vcDbColumnName,
                    @intRowNum = ( tintRow + 1 )
            FROM    View_DynamicCustomColumns
            WHERE   numFormID = @tintType
                    AND Grp_id = 5
                    AND numDomainID = @numDomainID
                    AND numAuthGroupID = @numBizDocId
                    AND bitCustom = 1
                    AND tintRow >= @intRowNum
                    AND ISNULL(numRelCntType, 0) = @numBizDocTempID
            ORDER BY tintRow                                                                  
		           
            IF @@rowcount = 0 
                SET @intRowNum = 0                                                                                          
        END


    SELECT  Row_NUmber() OVER ( ORDER BY numoppitemtCode ) RowNumber,
            *
    FROM    #Temp1
    ORDER BY numSortOrder ASC 

    DROP TABLE #Temp1

--    SELECT  ISNULL(tintOppStatus, 0)
--    FROM    OpportunityMaster
--    WHERE   numOppID = @numOppId                                                                                                 

IF (SELECT COUNT(*) FROM View_DynamicColumns WHERE   numFormID = @tintType AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId AND vcFieldType = 'R'AND ISNULL(numRelCntType, 0) = @numBizDocTempID)=0
BEGIN            
	INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType)
	select numFormId,numFieldId,1,tintRow,@numDomainID,@numBizDocId,@numBizDocTempID
	 FROM View_DynamicDefaultColumns
	 where numFormId=@tintType AND bitDefault=1 AND numDomainID=@numDomainID AND vcLookBackTableName = (CASE @numBizDocId WHEN 40911 THEN 'CSGrid' ELSE REPLACE(vcLookBackTableName,'CSGrid','') END) order by tintRow ASC	
	 
	 
	  IF(SELECT COUNT(*) FROM DycFrmConfigBizDocsSumm Ds where numBizDocID=@numBizDocId and Ds.numFormID=@tintType 
	and isnull(Ds.numBizDocTempID,0)=@numBizDocTempID and Ds.numDomainID=@numDomainID)=0
	BEGIN
		INSERT INTO DycFrmConfigBizDocsSumm (numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID)
		 SELECT @tintType,@numBizDocId,DFM.numFieldID,@numDomainID,@numBizDocTempID FROM
		 DycFormField_Mapping DFFM 
			JOIN DycFieldMaster DFM on DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
			where numFormID=16 and DFFM.numDomainID is null
	END
	
END      

    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicColumns
    WHERE   numFormID = @tintType
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND vcFieldType = 'R'
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    UNION
    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            'C' vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow
GO
/****** Object:  StoredProcedure [dbo].[USP_OPPDetails]    Script Date: 03/25/2009 15:10:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By Anoop Jayaraj
-- [dbo].[USP_OPPDetails] 1362,1,-333
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppdetails')
DROP PROCEDURE usp_oppdetails
GO
CREATE PROCEDURE [dbo].[USP_OPPDetails]
(
               @numOppID             AS NUMERIC(9)  = NULL,
               @numDomainID          AS NUMERIC(9)  = 0,
               @ClientTimeZoneOffset AS INT)
AS
BEGIN	
  SELECT Opp.numoppid,
         numCampainID,
         ADC.vcFirstname + ' ' + ADC.vcLastName AS numContactId,
         isnull(ADC.vcEmail,'') AS vcEmail,
         Opp.txtComments,
         Opp.numDivisionID,
         tintOppType,
         ISNULL(dateadd(MINUTE,-@ClientTimeZoneOffset,bintAccountClosingDate),'') AS bintAccountClosingDate,
         Opp.numContactID AS ContactID,
         tintSource,
         C2.vcCompanyName + Case when isnull(D2.numCompanyDiff,0)>0 then  '  ' + dbo.fn_getlistitemname(D2.numCompanyDiff) + ':' + isnull(D2.vcCompanyDiff,'') else '' end as vcCompanyname,
         D2.tintCRMType,
         Opp.vcPoppName,
         intpEstimatedCloseDate,
         [dbo].[GetDealAmount](@numOppID,getutcdate(),0) AS monPAmount,
         lngPConclAnalysis,
         monPAmount AS OppAmount,
         numSalesOrPurType,
         Opp.tintActive,
         dbo.fn_GetContactName(Opp.numCreatedby)
           + ' '
           + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintCreatedDate)) AS CreatedBy,
         dbo.fn_GetContactName(Opp.numModifiedBy)
           + ' '
           + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintModifiedDate)) AS ModifiedBy,
         dbo.fn_GetContactName(Opp.numRecOwner) AS RecordOwner,
         Opp.numRecOwner,
         isnull(tintshipped,0) AS tintshipped,
         isnull(tintOppStatus,0) AS tintOppStatus,
         dbo.OpportunityLinkedItems(@numOppID) AS NoOfProjects,
         Opp.numAssignedTo,
         Opp.numAssignedBy,
         (SELECT COUNT(* )
          FROM   dbo.GenericDocuments
          WHERE  numRecID = @numOppID
                 AND vcDocumentSection = 'O') AS DocumentCount,
         isnull(OPR.numRecurringId,0) AS numRecurringId,
		 OPR.dtRecurringDate AS dtLastRecurringDate,
         D2.numTerID,
         Link.numOppID AS numParentOppID,
         Link.vcPoppName AS ParentOppName,
         Link.vcSource AS Source,
         Link.numParentProjectID,
         Link.vcProjectName,
         ISNULL(C.varCurrSymbol,'') varCurrSymbol,
         ISNULL(Opp.fltExchangeRate,0) AS [fltExchangeRate],
         ISNULL(C.chrCurrency,'') AS [chrCurrency],
         ISNULL(C.numCurrencyID,0) AS [numCurrencyID],
		 (Select Count(*) from OpportunityBizDocs 
		 where numOppId=@numOppID and bitAuthoritativeBizDocs=1) As AuthBizDocCount,
		 (SELECT COUNT(distinct numChildOppID) FROM [OpportunityLinking] OL WHERE OL.[numParentOppID] = Opp.numOppId) AS ChildCount,
		 (SELECT COUNT(*) FROM [RecurringTransactionReport] RTR WHERE RTR.[numRecTranSeedId] = Opp.numOppId)  AS RecurringChildCount
		 ,Opp.bintCreatedDate,
		 ISNULL(Opp.bitStockTransfer ,0) bitStockTransfer,	ISNULL(Opp.tintTaxOperator,0) AS tintTaxOperator, 
         isnull(Opp.intBillingDays,0) AS numBillingDays,isnull(Opp.bitInterestType,0) AS bitInterestType,
         isnull(opp.fltInterest,0) AS fltInterest,  isnull(Opp.fltDiscount,0) AS fltDiscount,
         isnull(Opp.bitDiscountType,0) AS bitDiscountType,ISNULL(Opp.bitBillingTerms,0) AS bitBillingTerms,ISNULL(OPP.numDiscountAcntType,0) AS numDiscountAcntType,
         ISNULL((SELECT TOP 1 numCountry FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)),0) AS [numShipCountry],
		 ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)),0) AS [numShipState],
		 ISNULL((SELECT TOP 1 vcState FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)),'') AS [vcShipState],
		 ISNULL((SELECT TOP 1 vcCountry FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)),'') AS [vcShipCountry],
		 ISNULL((SELECT TOP 1 vcPostalCode FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)),'') AS [vcPostalCode],
		 ISNULL(vcCouponCode,'') AS [vcCouponCode],ISNULL(Opp.bitPPVariance,0) AS bitPPVariance,
		 Opp.dtItemReceivedDate,ISNULL(bitUseShippersAccountNo,0) [bitUseShippersAccountNo], ISNULL(D2.vcShippersAccountNo,'') [vcShippersAccountNo],
		 ISNULL(bitUseMarkupShippingRate,0) [bitUseMarkupShippingRate],
		 ISNULL(numMarkupShippingRate,0) [numMarkupShippingRate],
		 ISNULL(Opp.intUsedShippingCompany,0) AS intUsedShippingCompany,
		 ISNULL(SR.[vcValue2],ISNULL(numShippingService,
		 (SELECT TOP 1 ISNULL([SST].[intNsoftEnum],0) FROM [dbo].[ShippingServiceTypes] AS SST 
		 WHERE [SST].[numDomainID] = @numDomainID 
		 AND [SST].[numRuleID] = 0
		 AND [SST].[vcServiceName] IN (SELECT ISNULL([OI].[vcItemDesc],'') FROM [dbo].[OpportunityItems] AS OI 
									 WHERE [OI].[numOppId] = [Opp].[numOppId] 
									 AND [OI].[vcType] = 'Service Item')))) AS numShippingService,
		 ISNULL(Opp.[monLandedCostTotal],0) AS monLandedCostTotal,opp.[vcLanedCost],
		 ISNULL(Opp.vcRecurrenceType,'') AS vcRecurrenceType,
		 ISNULL(RecurrenceConfiguration.numRecConfigID,0) AS numRecConfigID,
		 (CASE WHEN ISNULL(Opp.vcRecurrenceType,'') = '' THEN 0 ELSE 1 END) AS bitRecur,
		 (CASE WHEN RecurrenceTransaction.numRecTranID IS NULL THEN 0 ELSE 1 END) AS bitRecurred,
		 dbo.FormatedDateFromDate(RecurrenceConfiguration.dtStartDate,@numDomainID) AS dtStartDate,
		 dbo.FormatedDateFromDate(RecurrenceConfiguration.dtEndDate,@numDomainID) AS dtEndDate,
		 RecurrenceConfiguration.vcFrequency AS vcFrequency,
		 ISNULL(RecurrenceConfiguration.bitDisabled,0) AS bitDisabled,
		 ISNULL(Opp.dtReleaseDate,'') AS dtReleaseDate,
		 ISNULL(Opp.numAccountClass,0) AS numAccountClass,D3.vcPartnerCode+'-'+C3.vcCompanyName as numPartenerSource,
		ISNULL(Opp.numPartner,0) AS numPartenerSourceId,
		ISNULL(Opp.numPartenerContact,0) AS numPartenerContactId,
		A.vcFirstName+' '+A.vcLastName AS numPartenerContact,
		numShipFromWarehouse, CPN.CustomerPartNo,
		Opp.numBusinessProcessID
  FROM   OpportunityMaster Opp 
         JOIN divisionMaster D2
           ON Opp.numDivisionID = D2.numDivisionID
		   LEFT JOIN divisionMaster D3 ON Opp.numPartner = D3.numDivisionID
		LEFT JOIN CompanyInfo C3 ON C3.numCompanyID = D3.numCompanyID
		LEFT JOIN AdditionalContactsInformation A ON Opp.numPartenerContact = A.numContactId
         LEFT JOIN CompanyInfo C2
           ON C2.numCompanyID = D2.numCompanyID
         LEFT JOIN (SELECT TOP 1 OM.vcPoppName,
                                 OM.numOppID,
                                 numChildOppID,
                                 vcSource,
                                 numParentProjectID,
                                 vcProjectName
                    FROM   OpportunityLinking
                          LEFT JOIN OpportunityMaster OM
                             ON numOppID = numParentOppID
                          LEFT JOIN [ProjectsMaster]
							 ON numParentProjectID = numProId   
                    WHERE  numChildOppID = @numOppID) Link
           ON Link.numChildOppID = Opp.numOppID
         LEFT OUTER JOIN [Currency] C
					ON C.numCurrencyID = Opp.numCurrencyID
		 LEFT OUTER JOIN [OpportunityRecurring] OPR
					ON OPR.numOppId = Opp.numOppId
		 LEFT OUTER JOIN additionalContactsinformation ADC ON
		 ADC.numdivisionId = D2.numdivisionId AND  ADC.numContactId = Opp.numContactId	
		 LEFT JOIN RecurrenceConfiguration ON Opp.numOppId = RecurrenceConfiguration.numOppID AND RecurrenceConfiguration.numType = 1
		 LEFT JOIN RecurrenceTransaction ON Opp.numOppId = RecurrenceTransaction.numRecurrOppID
		 LEFT JOIN [dbo].[ShippingReport] AS SR ON SR.[numOppID] = [Opp].[numOppId] AND SR.[numDomainID] = [Opp].[numDomainId]
		LEFT JOIN [dbo].CustomerPartNumber CPN ON C3.numCompanyID = CPN.numCompanyID
		WHERE  Opp.numOppId = @numOppID
         AND Opp.numDomainID = @numDomainID
END
GO
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount DECIMAL(20,5) =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(18,0),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0,
  @numShipmentMethod NUMERIC(18,0)=0,
  @dtReleaseDate DATE = NULL,
  @numPartner NUMERIC(18,0)=0,
  @tintClickBtn INT=0,
  @numPartenerContactId NUMERIC(18,0)=0,
  @numAccountClass NUMERIC(18,0) = 0,
  @numWillCallWarehouseID NUMERIC(18,0) = 0,
  @numVendorAddressID NUMERIC(18,0) = 0,
  @numShipFromWarehouse NUMERIC(18,0) = 0,
  @numShippingService NUMERIC(18,0) = 0,
  @ClientTimeZoneOffset INT = 0,
  @vcCustomerPO VARCHAR(100)=NULL,
  @bitDropShipAddress BIT = 0,
  @PromCouponCode AS VARCHAR(100) = NULL,
  @numPromotionId AS NUMERIC(18,0) = 0
  -- USE PARAMETER WITH OPTIONAL VALUE BECAUSE PROCEDURE IS CALLED FROM OTHER PROCEDURE WHICH WILL NOT PASS NEW PARAMETER VALUE
)                                                                          
                                                                          
as  
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @fltExchangeRate float                                 
	DECLARE @hDocItem int                                                                                                                            
	DECLARE @TotalAmount as DECIMAL(20,5)                                                                          
	DECLARE @tintOppStatus as tinyint
	DECLARE @bitNewOrder AS BIT = (CASE WHEN ISNULL(@numOppID,0) = 0 THEN 1 ELSE 0 END)
	
	IF ISNULL(@numContactId,0) = 0
	BEGIN
		RAISERROR('CONCAT_REQUIRED',16,1)
		RETURN
	END  
	
	IF ISNULL(@tintOppType,0) = 0
	BEGIN
		RAISERROR('OPP_TYPE_CAN_NOT_BE_0',16,1)
		RETURN
	END             

	IF ISNULL(@numOppID,0) > 0 AND EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numDomainId=@numDomainId AND numOppId=@numOppID AND ISNULL(tintshipped,0) = 1) 
	BEGIN	 
		RAISERROR('OPP/ORDER_IS_CLOSED',16,1)
		RETURN
	END

	DECLARE @dtItemRelease AS DATE
	SET @dtItemRelease = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())


	IF CONVERT(VARCHAR(10),@strItems) <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

		--VALIDATE PROMOTION OF COUPON CODE
		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1),
			numOppItemID NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numOppItemID,
			numPromotionID
		)
		SELECT
			numoppitemtCode,
			numPromotionID
		FROM
			OPENXML (@hDocItem,'/NewDataSet/Item',2)
		WITH
		(
			numoppitemtCode NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		IF (SELECT
				COUNT(*)
			FROM
				PromotionOffer PO
			WHERE
				numDomainId=@numDomainId
				AND bitRequireCouponCode = 1
				AND ISNULL(bitEnabled,0) = 1
				AND ISNULL(tintUsageLimit,0) > 0
				AND (intCouponCodeUsed 
					- (CASE WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE numOppID=ISNULL(@numOppID,0) AND numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END) 
					+ (CASE WHEN ISNULL((SELECT COUNT(*) FROM @TEMP WHERE numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END)) > tintUsageLimit) > 0
		BEGIN
			RAISERROR('USAGE_OF_COUPON_EXCEED',16,1)
			RETURN
		END
		ELSE
		BEGIN
			UPDATE
				PO
			SET 
				intCouponCodeUsed = ISNULL(intCouponCodeUsed,0) 
									- (CASE WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE numOppID=ISNULL(@numOppID,0) AND numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END) 
									+ (CASE WHEN ISNULL(T1.intUsed,0) > 0 THEN 1 ELSE 0 END) 
			FROM
				PromotionOffer PO
			INNER JOIN
				(
					SELECT
						numPromotionID,
						COUNT(*) AS intUsed
					FROM
						@TEMP
					GROUP BY
						numPromotionID
				) AS T1
			ON
				PO.numProId = T1.numPromotionID
			WHERE
				numDomainId=@numDomainId
				AND bitRequireCouponCode = 1
				AND ISNULL(bitEnabled,0) = 1
		END
	END

	DECLARE @numCompany AS NUMERIC(18)
	SELECT @numCompany = numCompanyID FROM DivisionMaster WHERE numDivisionID = @numDivisionId

	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainId
	SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM DivisionMaster WHERE numDivisionID=@numDivisionId

	--IF NEW OPPERTUNITY
	IF @numOppID = 0                                                                          
	BEGIN
		DECLARE @bitIsInitalSalesOrder BIT
		IF(@DealStatus=1 AND @tintOppType=1)
		BEGIN
			SET @bitIsInitalSalesOrder=1
		END

		DECLARE @intOppTcode AS NUMERIC(9)  
  
		SELECT @intOppTcode = MAX(numOppId) FROM OpportunityMaster                

		SET @intOppTcode = ISNULL(@intOppTcode,0) + 1                                                
		
		SET @vcPOppName = @vcPOppName + '-'+ CONVERT(varchar(4),YEAR(GETUTCDATE()))  +'-A'+ CONVERT(varchar(10),@intOppTcode)                                  
  
		IF ISNULL(@numCurrencyID,0) = 0 
		BEGIN
			SET @fltExchangeRate=1
			SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
		END
		ELSE
		BEGIN 
			SET @fltExchangeRate = dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
        END  
		                                                        
		IF ISNULL(@numAccountClass,0) = 0
		BEGIN                                                  
		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivisionID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
		END

        IF(@DealStatus=1 AND @tintOppType=1 AND @tintClickBtn=1)
		BEGIN
			SET @DealStatus=0
		END         
		--PRINT ('Deal Staus ' +@DealStatus)  

		-------- ORDER BASED Promotion Logic-------------

DECLARE @numDiscountID NUMERIC(18,0) = NULL
IF @numPromotionId > 0
Begin

		IF ISNULL(@PromCouponCode,'') = ''
		BEGIN

			SET @numDiscountID =  (SELECT DC.numDiscountId FROM PromotionOffer PO
								INNER JOIN DiscountCodes DC
								ON PO.numProId = DC.numPromotionID
								WHERE numProId = @numPromotionId AND numDomainId = @numDomainID)
		END

		ELSE IF ISNULL(@PromCouponCode,'') <> ''
		BEGIN
			DECLARE @numCouponCode NUMERIC(18,0)
			DECLARE @intCodeUsageLimit NUMERIC(18,0)
			DECLARE @intCodeUsed INT		
			DECLARE @intUsedCountForDiv INT

			SET @numCouponCode = (SELECT COUNT(DC.vcDiscountCode) FROM PromotionOffer PO
								INNER JOIN DiscountCodes DC
								ON PO.numProId = DC.numPromotionID
								WHERE numProId = @numPromotionId AND numDomainId = @numDomainID AND bitRequireCouponCode = 1 
								AND DC.vcDiscountCode = @PromCouponCode)
		
			IF @numCouponCode = 0 
			BEGIN	 
				RAISERROR('INVALID_CODE',16,1)
				RETURN
			END
			ELSE IF @numCouponCode = 1
			BEGIN 

				SET @numDiscountID =  (SELECT DC.numDiscountId FROM PromotionOffer PO
									INNER JOIN DiscountCodes DC
									ON PO.numProId = DC.numPromotionID
									WHERE numProId = @numPromotionId AND numDomainId = @numDomainID AND bitRequireCouponCode = 1 
									AND DC.vcDiscountCode = @PromCouponCode)

				SET @intCodeUsageLimit = (SELECT DC.CodeUsageLimit FROM PromotionOffer PO
									INNER JOIN DiscountCodes DC
									ON PO.numProId = DC.numPromotionID
									WHERE numProId = @numPromotionId AND numDomainId = @numDomainID AND bitRequireCouponCode = 1 
									AND DC.vcDiscountCode = @PromCouponCode)


				--IF @intCodeUsageLimit > 0
				--BEGIN 
				SET @intUsedCountForDiv = (SELECT COUNT(DCU.numDivisionId) FROM DiscountCodeUsage DCU
										INNER JOIN DiscountCodes DC
										ON  DCU.numDiscountId = DC.numDiscountId
										WHERE DCU.numDivisionId = @numDivisionId AND DC.vcDiscountCode = @PromCouponCode)			

					IF @intUsedCountForDiv = 0
					BEGIN
						INSERT INTO DiscountCodeUsage
						(numDiscountId, numDivisionId, intCodeUsed)
						VALUES
						(@numDiscountID, @numDivisionId, 1)
					END
					ELSE IF @intUsedCountForDiv > 0 AND @intCodeUsageLimit = 0
					BEGIN
						UPDATE DiscountCodeUsage
							SET intCodeUsed = @intCodeUsed + 1
						WHERE numDivisionId = @numDivisionId AND numDiscountId = @numDiscountID
					END
					ELSE IF @intUsedCountForDiv > 0 AND @intCodeUsageLimit > 0
					BEGIN
						SET @intCodeUsed = (SELECT intCodeUsed FROM DiscountCodeUsage DCU
											INNER JOIN DiscountCodes DC
											ON  DCU.numDiscountId = DC.numDiscountId
											WHERE DCU.numDivisionId = @numDivisionId AND DC.vcDiscountCode = @PromCouponCode)

						IF @intCodeUsed < @intCodeUsageLimit
						BEGIN				
							UPDATE DiscountCodeUsage
								SET intCodeUsed = @intCodeUsed + 1
							WHERE numDivisionId = @numDivisionId AND numDiscountId = @numDiscountID
						END
						ELSE
						BEGIN	
							RAISERROR('CODE_USAGE_LIMIT_EXCEEDS',16,1)
							RETURN
						END
					END
			--	END
			END	
		END

				
END

		-------- ORDER BASED Promotion Logic-------------
		                                                    
		INSERT INTO OpportunityMaster
		(                                                                             
			numContactId,numDivisionId,txtComments,numCampainID,bitPublicFlag,tintSource,tintSourceType,vcPOppName,                                                                          
			intPEstimatedCloseDate,monPAmount,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,                                                                                                             
			numRecOwner,lngPConclAnalysis,tintOppType,numSalesOrPurType,numCurrencyID,fltExchangeRate,numAssignedTo,
			numAssignedBy,[tintOppStatus],numStatus,vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,
			bitInterestType,fltInterest,vcCouponCode,bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,
			vcMarketplaceOrderReportId,numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
			bitUseMarkupShippingRate,numMarkupShippingRate,intUsedShippingCompany,numShipmentMethod,numShippingService,numPartner,numPartenerContact,dtReleaseDate,bitIsInitalSalesOrder,numVendorAddressID,numShipFromWarehouse
			,vcCustomerPO#,bitDropShipAddress,numDiscountID
		)                                                                          
		Values                                                                          
		(                                                                          
			@numContactId,@numDivisionId,ISNULL(@Comments,''),@CampaignID,@bitPublicFlag,@tintSource,@tintSourceType,@vcPOppName,                                                                          
			@dtEstimatedCloseDate,@monPAmount,@numUserCntID,(CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END),                                                                          
			@numUserCntID,GETUTCDATE(),@numDomainId,@numUserCntID,@lngPConclAnalysis,@tintOppType,@numSalesOrPurType,@numCurrencyID, 
			@fltExchangeRate,(CASE WHEN @numAssignedTo > 0 THEN @numAssignedTo ELSE NULL END),(CASE WHEN @numAssignedTo>0 THEN @numUserCntID ELSE NULL END),
			@DealStatus,@numStatus,@vcOppRefOrderNo,@bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,
			@vcCouponCode,@bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,@numPercentageComplete,
			@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,@bitUseMarkupShippingRate,@numMarkupShippingRate,@intUsedShippingCompany,@numShipmentMethod,@numShippingService,@numPartner,@numPartenerContactId,@dtReleaseDate,@bitIsInitalSalesOrder,@numVendorAddressID,@numShipFromWarehouse
			,@vcCustomerPO,@bitDropShipAddress,@numDiscountID
		)                                                                                                                      
		
		SET @numOppID=(SELECT SCOPE_IDENTITY())                
                            
		--UPDATE OPPNAME AS PER NAME TEMPLATE
		EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

		SELECT @vcPOppName=ISNULL(vcPOppName,'') FROM OpportunityMaster WHERE numOppId=@numOppID

		--MAP CUSTOM FIELD	
		DECLARE @tintPageID AS TINYINT;
		SET @tintPageID = CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
		EXEC dbo.USP_AddParentChildCustomFieldMap
				@numDomainID = @numDomainID, --  numeric(18, 0)
				@numRecordID = @numOppID, --  numeric(18, 0)
				@numParentRecId = @numDivisionId, --  numeric(18, 0)
				@tintPageID = @tintPageID --  tinyint
 	
		IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
		BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END
  
		-- INSERTING ITEMS                                                                                                                    
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,
				monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
				numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,vcAttrValues,monAvgCost,bitItemPriceApprovalRequired,
				numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,numWOAssignedTo,numShipToAddressID,ItemReleaseDate
				,bitMarkupDiscount,vcChildKitSelectedItems
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
				(CASE X.numWarehouseItmsID 
				WHEN -1 
				THEN 
					CASE 
						WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU= X.vcSKU) > 0)
							THEN
								(SELECT TOP 1 
									numWareHouseItemID 
								FROM 
									WareHouseItems 
								WHERE 
									[numItemID] = X.numItemCode 
									AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
						ELSE
							(SELECT 
								[numWareHouseItemID] 
							FROM 
								[WareHouseItems] 
							WHERE 
								[numItemID] = X.numItemCode 
								AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				WHEN 0 
				THEN 
					CASE 
					WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU=X.vcSKU) > 0)
						THEN
							(SELECT TOP 1 
								numWareHouseItemID 
							FROM 
								WareHouseItems 
							WHERE 
								[numItemID] = X.numItemCode 
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
					ELSE
						(SELECT 
							[numWareHouseItemID] 
						FROM 
							[WareHouseItems] 
						WHERE 
							[numItemID] = X.numItemCode 
							AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
							AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				ELSE  
					X.numWarehouseItmsID 
				END) AS numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
				(SELECT isnull(VN.monCost,0) FROM Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode AND VN.numVendorID=IT.numVendorID WHERE  VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,
				0,
				X.numToWarehouseItemID,X.Attributes,X.AttributeIDs,(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),
				X.bitItemPriceApprovalRequired,X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(X.vcVendorNotes,'')
				,X.vcInstruction,X.bintCompliationDate,X.numWOAssignedTo,X.numShipToAddressID,ISNULL(ItemReleaseDate,@dtItemRelease),X.bitMarkupDiscount,ISNULL(X.KitChildItems,'')
			FROM
				(
					SELECT 
						*
					FROM 
						OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
					WITH                       
					(                                                                          
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9), numUnitHour FLOAT, monPrice DECIMAL(30,16), monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000),                                    
						numWarehouseItmsID NUMERIC(9), ItemType VARCHAR(30), DropShip BIT, bitDiscountType BIT,	fltDiscount DECIMAL(30,16), monTotAmtBefDiscount DECIMAL(20,5),
						vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),
						numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),AttributeIDs VARCHAR(500),vcSKU VARCHAR(100)
						,bitItemPriceApprovalRequired BIT,numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0)
						,numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(18,0)
						,numShipToAddressID  NUMERIC(18,0),ItemReleaseDate DATETIME,bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX)
					)
				)X    
			ORDER BY 
				X.numoppitemtCode
    
			-- UPDATE EXISTING CUSTOMER PART NO
			UPDATE
				CPN
			SET
				CPN.CustomerPartNo = ISNULL(TEMPCPN.CustomerPartNo,'')
			FROM
				CustomerPartNumber CPN
			INNER JOIN
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) AS TEMPCPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.numDomainID=@numDomainID
				AND CPN.numCompanyId = @numCompany
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- INSERT NEW CUSTOMER PART NO
			INSERT INTO CustomerPartNumber
			(
				numCompanyId, numDomainID, CustomerPartNo, numItemCode 
			)
			SELECT 
				@numCompany, @numDomainID, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
			FROM
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) TEMPCPN 
			LEFT JOIN
				CustomerPartNumber CPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.CustomerPartNoID IS NULL
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'

			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,numWarehouseItmsID=X.numWarehouseItmsID,bitDropShip=X.DropShip,
				bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
				bitWorkOrder=X.bitWorkOrder,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,
				numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,vcAttributes=X.Attributes,
				numClassID=0,vcModelID=X.vcModelID,
				bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired,vcAttrValues=X.AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost
				,vcNotes=X.vcVendorNotes,vcInstruction=X.vcInstruction,bintCompliationDate=X.bintCompliationDate,numWOAssignedTo=X.numWOAssignedTo,ItemRequiredDate=X.ItemRequiredDate
				,bitMarkupDiscount=X.bitMarkupDiscount
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,
					monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,
					Attributes,bitItemPriceApprovalRequired,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) numCost
					,ISNULL(vcVendorNotes,'') vcVendorNotes,vcInstruction,bintCompliationDate,numWOAssignedTo,ItemRequiredDate,bitMarkupDiscount
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
				WITH  
					(                      
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000), vcModelID VARCHAR(300),
						numWarehouseItmsID NUMERIC(9),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),
						numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),
						numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000)
						,vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(18,0),ItemRequiredDate DATETIME,bitMarkupDiscount BIT
					)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppID=@numOppID
				
			--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
			DECLARE @TempKitConfiguration TABLE
			(
				numOppItemID NUMERIC(18,0),
				numItemCode NUMERIC(18,0),
				KitChildItems VARCHAR(MAX)
			)

			INSERT INTO 
				@TempKitConfiguration
			SELECT
				numoppitemtCode,
				numItemCode,
				vcChildKitSelectedItems
			FROM
				OpportunityItems
			WHERE
				numOppId = @numOppID
				AND LEN(ISNULL(vcChildKitSelectedItems,'')) > 0

			--INSERT KIT ITEMS 
			IF (SELECT 
					COUNT(*) 
				FROM 
					OpportunityItems OI 
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				JOIN 
					ItemDetails ID 
				ON 
					OI.numItemCode=ID.numItemKitID 
				JOIN
					Item I
				ON
					ID.numChildItemID = I.numItemCode
				WHERE 
					OI.numOppId=@numOppId  
					AND ISNULL(OI.bitDropShip,0) = 0
					AND ISNULL(OI.numWarehouseItmsID,0) > 0
					AND I.charItemType = 'P'
					AND 1 = (CASE 
								WHEN (SELECT 
											COUNT(*) 
										FROM 
											@TempKitConfiguration t1
										CROSS APPLY
										(
											SELECT
												*
											FROM 
												dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=OI.numoppitemtCode AND t3.numItemCode=OI.numItemCode),',') 
										) AS t2 
										WHERE 
											SUBSTRING(items,0,CHARINDEX('-',items)) = '0') > 0 
								THEN 
									(CASE 
										WHEN I.numItemCode IN (SELECT 
																	SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))
																FROM 
																	@TempKitConfiguration t1
																CROSS APPLY
																(
																	SELECT
																		*
																	FROM 
																		dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=OI.numoppitemtCode AND t3.numItemCode=OI.numItemCode),',') 
																) AS t2 
																WHERE 
																	SUBSTRING(items,0,CHARINDEX('-',items)) = '0') 
										THEN 1 
										ELSE 0 
									END)
								ELSE 1 END)
					AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END

			           		        
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND 1 = (CASE 
								WHEN (SELECT 
											COUNT(*) 
										FROM 
											@TempKitConfiguration t1
										CROSS APPLY
										(
											SELECT
												*
											FROM 
												dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=OI.numoppitemtCode AND t3.numItemCode=OI.numItemCode),',') 
										) AS t2 
										WHERE 
											SUBSTRING(items,0,CHARINDEX('-',items)) = '0') > 0 
								THEN 
									(CASE 
										WHEN I.numItemCode IN (SELECT 
																	SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))
																FROM 
																	@TempKitConfiguration t1
																CROSS APPLY
																(
																	SELECT
																		*
																	FROM 
																		dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=OI.numoppitemtCode AND t3.numItemCode=OI.numItemCode),',') 
																) AS t2 
																WHERE 
																	SUBSTRING(items,0,CHARINDEX('-',items)) = '0') 
										THEN 1 
										ELSE 0 
									END)
								ELSE 1 END)

			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				IF (SELECT 
						COUNT(*) 
					FROM 
					(
						SELECT 
							t1.numOppItemID
							,t1.numItemCode
							,SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID
							,SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=t1.numOppItemID AND t3.numItemCode=t1.numItemCode),',') 
						) as t2
					) TempChildItems
					INNER JOIN
						OpportunityKitItems OKI
					ON
						OKI.numOppId=@numOppId
						AND OKI.numOppItemID=TempChildItems.numOppItemID
						AND OKI.numChildItemID=TempChildItems.numKitItemID
					INNER JOIN
						OpportunityItems OI
					ON
						OI.numItemCode = TempChildItems.numItemCode
						AND OI.numoppitemtcode = OKI.numOppItemID
					LEFT JOIN
						WarehouseItems WI
					ON
						OI.numWarehouseItmsID = WI.numWarehouseItemID
					LEFT JOIN
						Warehouses W
					ON
						WI.numWarehouseID = W.numWarehouseID
					INNER JOIN
						ItemDetails
					ON
						TempChildItems.numKitItemID = ItemDetails.numItemKitID
						AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					INNER JOIN
						Item 
					ON
						ItemDetails.numChildItemID = Item.numItemCode
					WHERE
						Item.charItemType = 'P'
						AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
				BEGIN
					RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
				END

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped,
					monAvgCost
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
					(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0,
					ISNULL(Item.monAverageCost,0)
				FROM
				(
					SELECT 
						t1.numOppItemID
						,t1.numItemCode
						,SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID
						,SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
					FROM 
						@TempKitConfiguration t1
					CROSS APPLY
					(
						SELECT
							*
						FROM 
							dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=t1.numOppItemID AND t3.numItemCode=t1.numItemCode),',') 
					) as t2
				) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numOppItemID=TempChildItems.numOppItemID
					AND OKI.numChildItemID=TempChildItems.numKitItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numoppitemtcode = OKI.numOppItemID
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END

			-- UPDATE UOM OF OPPORTUNITY ITEMS HAVE NOT UOM SET WHILE EXPORTED FROM MARKETPLACE (MEANS TINTSOURCETYPE = 3)
			IF ISNULL(@tintSourceType,0) = 3
			BEGIN
				UPDATE 
					OI 
				SET 
					numUOMID = ISNULL(I.numSaleUnit,0), 
					numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
				FROM 
					OpportunityItems OI
				JOIN 
					Item I 
				ON 
					OI.numItemCode = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND I.numDomainID = @numDomainId
			END	
                                     
			INSERT INTO OppWarehouseSerializedItem
			(
				numOppID,
				numWarehouseItmsDTLID,
				numOppItemID,
				numWarehouseItmsID
			)                      
			SELECT 
				@numOppID,
				X.numWarehouseItmsDTLID,
				X.numoppitemtCode,
				X.numWItmsID 
			FROM
			(                                                                          
				SELECT
					* 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
				WITH                        
				(                                               
				numWarehouseItmsDTLID numeric(9),                      
				numoppitemtCode numeric(9),                      
				numWItmsID numeric(9)                                                     
				)
			)X                                    
                                   
			UPDATE 
				OppWarehouseSerializedItem                       
			SET 
				numOppItemID=X.numoppitemtCode                                
			FROM                       
			(
				SELECT 
					numWarehouseItmsDTLID AS DTLID,
					O.numoppitemtCode 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
				WITH
				(
					numWarehouseItmsDTLID NUMERIC(9),numoppitemtCode NUMERIC(9),numWItmsID NUMERIC(9)
				)                                
				JOIN 
					OpportunityItems O 
				ON 
					O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID
			)X                 
			WHERE 
				numOppID=@numOppID 
				AND numWarehouseItmsDTLID=X.DTLID      
    
			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			END
                     
			EXEC sp_xml_removedocument @hDocItem                                     
		END          
	END
	ELSE                                                                                                                          
	BEGIN

		DECLARE @tempAssignedTo AS NUMERIC(9) = NULL                                                    
		SELECT 
			@tintOppStatus = tintOppStatus,
			@tempAssignedTo=ISNULL(numAssignedTo,0) 
		FROM   
			OpportunityMaster 
		WHERE  
			numOppID = @numOppID

		IF @tempAssignedTo <> @numAssignedTo
		BEGIN
			IF (SELECT COUNT(*) FROM BizDocComission WHERE ISNULL(numOppID,0) = @numOppID AND ISNULL(bitCommisionPaid,0) = 1) > 0
			BEGIN
				RAISERROR('COMMISSION_PAID_AGAINST_BIZDOC',16,1)
			END
		END
 
		--REVERTING BACK THE WAREHOUSE ITEMS                  
		IF @tintOppStatus = 1 AND @tintCommitAllocation=1
		BEGIN               
			EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID                  
		END                  

		IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
		BEGIN
			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
		END
		
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
		BEGIN
			EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END 
						
		UPDATE   
			OpportunityMaster
		SET      
			vcPOppName = @vcPOppName,txtComments = ISNULL(@Comments,''),bitPublicFlag = @bitPublicFlag,numCampainID = @CampaignID,tintSource = @tintSource,tintSourceType=@tintSourceType,
			intPEstimatedCloseDate = @dtEstimatedCloseDate,numModifiedBy = @numUserCntID,bintModifiedDate = GETUTCDATE(),lngPConclAnalysis = @lngPConclAnalysis,monPAmount = @monPAmount,
			tintActive = @tintActive,numSalesOrPurType = @numSalesOrPurType,numStatus = @numStatus,vcOppRefOrderNo=@vcOppRefOrderNo,
			bintOppToOrder=(CASE WHEN @tintOppStatus=0 AND @DealStatus=1 THEN GETUTCDATE() ELSE bintOppToOrder END),bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,
			bitBillingTerms=@bitBillingTerms,intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,tintTaxOperator=@tintTaxOperator,
			numDiscountAcntType=@numDiscountAcntType,vcCouponCode = @vcCouponCode,numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
			intUsedShippingCompany = @intUsedShippingCompany, numContactID=@numContactId,numShipmentMethod=@numShipmentMethod,numShippingService=@numShippingService,numPartner=@numPartner,numPartenerContact=@numPartenerContactId
		WHERE
			numOppId = @numOppID   
	   
		-- UPDATING IF ORGANIZATION IS ASSIGNED TO SOMEONE                                                      
		IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN
			UPDATE
				OpportunityMaster 
			SET	
				numAssignedTo = @numAssignedTo, 
				numAssignedBy = @numUserCntID 
			WHERE 
				numOppId = @numOppID                                                    
		END                                                     
		ELSE IF(@numAssignedTo = 0) 
		BEGIN
			UPDATE 
				OpportunityMaster 
			SET 
				numAssignedTo = 0, 
				numAssignedBy = 0 
			WHERE
				numOppId = @numOppID
		END  
			                                                                    
		-- UPDATING OPP ITEMS
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0                                                                         
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   
			IF EXISTS 
			(
				SELECT 
					OI.numoppitemtCode
				FROM 
					OpportunityItems OI
				WHERE 
					numOppID=@numOppID 
					AND OI.numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))
					AND ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) > 0
			)
			BEGIN
				RAISERROR('SERIAL/LOT#_ASSIGNED_TO_ITEM_DELETED',16,1)
			END

			--DELETE CHILD ITEMS OF KIT WITHIN KIT
			DELETE FROM 
				OpportunityKitChildItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   

			--DELETE ITEMS
			DELETE FROM 
				OpportunityKitItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))                      
			                            
			DELETE FROM 
				OpportunityItems 
			WHERE 
				numOppID=@numOppID 
				AND numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   
	          
			-- DELETE CORRESPONSING TIME AND EXPENSE ENTRIES OF DELETED ITEMS
			DELETE FROM
				TimeAndExpense
			WHERE
				numDomainID=@numDomainId
				AND numOppId=@numOppID
				AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9))) 

			--INSERT NEW ADDED ITEMS                        
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,
				[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numToWarehouseItemID,numShipmentMethod,
				numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired,vcAttrValues,numPromotionID,bitPromotionTriggered
				,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,numWOAssignedTo,bitMarkupDiscount,ItemReleaseDate,vcChildKitSelectedItems
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
				(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),
				(select isnull(VN.monCost,0) from Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode and VN.numVendorID=IT.numVendorID where VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numToWarehouseItemID,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
				0,
				(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired, X.AttributeIDs,
				X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(vcVendorNotes,'')
				,X.vcInstruction,X.bintCompliationDate,X.numWOAssignedTo,X.bitMarkupDiscount,@dtItemRelease,ISNULL(KitChildItems,'')
			FROM
			(
				SELECT 
					*
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH  
				(                      
					numoppitemtCode numeric(9),numItemCode numeric(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc varchar(1000),                                    
					numWarehouseItmsID numeric(9),numToWarehouseItemID numeric(9),Op_Flag tinyint,ItemType varchar(30),DropShip bit,bitDiscountType bit,
					fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),
					numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(500), 
					bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000)
					,numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(18,0)
					,bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX)
				)
			)X 
			
			-- UPDATE EXISTING CUSTOMER PART NO
			UPDATE
				CPN
			SET
				CPN.CustomerPartNo = ISNULL(TEMPCPN.CustomerPartNo,'')
			FROM
				CustomerPartNumber CPN
			INNER JOIN
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) AS TEMPCPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.numDomainID=@numDomainID
				AND CPN.numCompanyId = @numCompany
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- INSERT NEW CUSTOMER PART NO
			INSERT INTO CustomerPartNumber
			(
				numCompanyId, numDomainID, CustomerPartNo, numItemCode 
			)
			SELECT 
				@numCompany, @numDomainID, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
			FROM
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) TEMPCPN 
			LEFT JOIN
				CustomerPartNumber CPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.CustomerPartNoID IS NULL
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- UPDATE ITEMS
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,vcModelID=X.vcModelID,numWarehouseItmsID=X.numWarehouseItmsID,numToWarehouseItemID = X.numToWarehouseItemID,             
				bitDropShip=X.DropShip,bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,
				numUOMId=numUOM,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,
				numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired,bitWorkOrder=X.bitWorkOrder,vcAttrValues=AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost,vcNotes=vcVendorNotes
				,bitMarkupDiscount=X.bitMarkupDiscount,ItemRequiredDate=X.ItemRequiredDate
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,numToWarehouseItemID,DropShip,bitDiscountType,
					fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
					bitItemPriceApprovalRequired,bitWorkOrder,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) AS numCost,ISNULL(vcVendorNotes,'') vcVendorNotes,
					ItemRequiredDate,bitMarkupDiscount
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
				WITH  
				(                      
					numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000),vcModelID VARCHAR(300), numWarehouseItmsID NUMERIC(9), 
					numToWarehouseItemID NUMERIC(18,0),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),
					numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),numProjectStageID NUMERIC(9),Attributes VARCHAR(500),
					bitItemPriceApprovalRequired BIT, bitWorkOrder BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),
					ItemRequiredDate DATETIME, bitMarkupDiscount BIT
				)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppId=@numOppID

			--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
			DELETE FROM @TempKitConfiguration

			INSERT INTO @TempKitConfiguration
			SELECT
				numoppitemtCode,
				numItemCode,
				vcChildKitSelectedItems
			FROM
				OpportunityItems OI
			WHERE
				numOppId = @numOppID
				AND LEN(ISNULL(vcChildKitSelectedItems,'')) > 0
				AND OI.numoppitemtCode NOT IN (SELECT ISNULL(numOppItemID,0) FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID)

			--INSERT NEW ADDED KIT ITEMS
			IF (SELECT 
					COUNT(*) 
				FROM 
					OpportunityItems OI 
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				JOIN 
					ItemDetails ID 
				ON 
					OI.numItemCode=ID.numItemKitID 
				JOIN
					Item I
				ON
					ID.numChildItemID = I.numItemCode
				WHERE 
					OI.numOppId=@numOppId  
					AND ISNULL(OI.bitDropShip,0) = 0
					AND ISNULL(OI.numWarehouseItmsID,0) > 0
					AND I.charItemType = 'P'
					AND OI.numoppitemtCode NOT IN (SELECT ISNULL(numOppItemID,0) FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID)
					AND 1 = (CASE 
								WHEN (SELECT 
											COUNT(*) 
										FROM 
											@TempKitConfiguration t1
										CROSS APPLY
										(
											SELECT
												*
											FROM 
												dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=OI.numoppitemtCode AND t3.numItemCode=OI.numItemCode),',') 
										) AS t2 
										WHERE 
											SUBSTRING(items,0,CHARINDEX('-',items)) = '0') > 0 
								THEN 
									(CASE 
										WHEN I.numItemCode IN (SELECT 
																	SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))
																FROM 
																	@TempKitConfiguration t1
																CROSS APPLY
																(
																	SELECT
																		*
																	FROM 
																		dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=OI.numoppitemtCode AND t3.numItemCode=OI.numItemCode),',') 
																) AS t2 
																WHERE 
																	SUBSTRING(items,0,CHARINDEX('-',items)) = '0') 
										THEN 1 
										ELSE 0 
									END)
								ELSE 1 END)
					AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END
			             
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND 1 = (CASE 
								WHEN (SELECT 
											COUNT(*) 
										FROM 
											@TempKitConfiguration t1
										CROSS APPLY
										(
											SELECT
												*
											FROM 
												dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=OI.numoppitemtCode AND t3.numItemCode=OI.numItemCode),',') 
										) AS t2 
										WHERE 
											SUBSTRING(items,0,CHARINDEX('-',items)) = '0') > 0 
								THEN 
									(CASE 
										WHEN I.numItemCode IN (SELECT 
																	SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))
																FROM 
																	@TempKitConfiguration t1
																CROSS APPLY
																(
																	SELECT
																		*
																	FROM 
																		dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=OI.numoppitemtCode AND t3.numItemCode=OI.numItemCode),',') 
																) AS t2 
																WHERE 
																	SUBSTRING(items,0,CHARINDEX('-',items)) = '0') 
										THEN 1 
										ELSE 0 
									END)
								ELSE 1 END)
				AND OI.numoppitemtCode NOT IN (SELECT ISNULL(numOppItemID,0) FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID) 
		
			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				IF (SELECT 
						COUNT(*) 
					FROM 
					(
						SELECT 
							t1.numOppItemID,
							t1.numItemCode,
							SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID,
							SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=t1.numOppItemID AND t3.numItemCode=t1.numItemCode),',') 
						) as t2
					) TempChildItems
					INNER JOIN
						OpportunityKitItems OKI
					ON
						OKI.numOppId=@numOppId
						AND OKI.numOppItemID = TempChildItems.numOppItemID
						AND OKI.numChildItemID=TempChildItems.numKitItemID
					INNER JOIN
						OpportunityItems OI
					ON
						OI.numItemCode = TempChildItems.numItemCode
						AND OI.numoppitemtcode = OKI.numOppItemID
					LEFT JOIN
						WarehouseItems WI
					ON
						OI.numWarehouseItmsID = WI.numWarehouseItemID
					LEFT JOIN
						Warehouses W
					ON
						WI.numWarehouseID = W.numWarehouseID
					INNER JOIN
						ItemDetails
					ON
						TempChildItems.numKitItemID = ItemDetails.numItemKitID
						AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					INNER JOIN
						Item 
					ON
						ItemDetails.numChildItemID = Item.numItemCode
					WHERE
						Item.charItemType = 'P'
						AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
				BEGIN
					RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
				END

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped,
					monAvgCost
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWarehouseItemID),
					(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0,
					ISNULL(Item.monAverageCost,0)
				FROM 
				(
					SELECT 
						t1.numOppItemID,
						t1.numItemCode,
						SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID,
						SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
					FROM 
						@TempKitConfiguration t1
					CROSS APPLY
					(
						SELECT
							*
						FROM 
							dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=t1.numOppItemID AND t3.numItemCode=t1.numItemCode),',') 
					) as t2
				) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numOppItemID = TempChildItems.numOppItemID
					AND OKI.numChildItemID=TempChildItems.numKitItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numoppitemtcode = OKI.numOppItemID
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END                                  
			
			--UPDATE KIT ITEMS                 
			UPDATE 
				OKI 
			SET 
				numQtyItemsReq = numQtyItemsReq_Orig * OI.numUnitHour
			FROM 
				OpportunityKitItems OKI 
			JOIN 
				OpportunityItems OI 
			ON 
				OKI.numOppItemID=OI.numoppitemtCode 
				AND OKI.numOppId=OI.numOppId  
			WHERE 
				OI.numOppId=@numOppId 

			-- UPDATE CHILD ITEMS OF KIT WITHIN KIT
			UPDATE
				OKCI
			SET
				OKCI.numQtyItemsReq = (OKI.numQtyItemsReq * OKCI.numQtyItemsReq_Orig)
			FROM
				OpportunityKitChildItems OKCI
			INNER JOIN
				OpportunityKitItems OKI
			ON
				OKCI.numOppID = @numOppId
				AND OKCI.numOppItemID = OKI.numOppItemID
				AND OKCI.numOppChildItemID = OKI.numOppChildItemID                     
			                                    
			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				IF @tintCommitAllocation = 2
				BEGIN
					DELETE FROM WorkOrderDetails WHERE numWOId IN (SELECT numWOId FROM WorkOrder WHERE numOppId=@numOppID AND numWOStatus <> 23184)
					DELETE FROM WorkOrder WHERE numOppId=@numOppID AND numWOStatus <> 23184
				END

				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			END
			   		                                                                      
			EXEC sp_xml_removedocument @hDocItem                                               
		END     

		IF @tintOppType=1 AND @tintOppStatus=1 --Sales Order
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0) = 0 AND (ISNULL(numUnitHour,0) - ISNULL(numQtyShipped,0)) < 0)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_IS_LESS_THEN_SHIPPED_QTY',16,1)
				RETURN
			END

			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems INNER JOIN WorkOrder ON OpportunityItems.numOppID=WorkOrder.numOppID AND OpportunityItems.numoppitemtCode=WorkOrder.numOppItemID AND ISNULL(WorkOrder.numParentWOID,0)=0 AND WorkOrder.numWOStatus=23184 AND ISNULL(OpportunityItems.numUnitHour,0) > ISNULL(WorkOrder.numQtyItemsReq,0) WHERE OpportunityItems.numOppID=@numOppID)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_CAN_NOT_BE_GREATER_THEN_COMPLETED_WORK_ORDER',16,1)
				RETURN
			END
		END
		ELSE IF @tintOppType=2 AND @tintOppStatus=1 --Purchase Order
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0) = 0 AND (ISNULL(numUnitHour,0) - ISNULL(numUnitHourReceived,0)) < 0)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_IS_LESS_THEN_RECEIVED_QTY',16,1)
				RETURN
			END
		END

		--FOLLOWING DELETE STATEMENT IS COMMENTED BECUASE NOW WE ARE NOT ALLOWING USER TO EIDT SERIAL/LOT ITEM UNTILL USER REMOVED ASSIGNED SERIAL/LOT NUMBERS
	    --Delete previously added serial lot number added for opportunity
		--DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
	END

	IF @tintOppStatus = 1
	BEGIN
		IF (SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0)=0) <>
			(SELECT COUNT(*) FROM OpportunityItems INNER JOIN WareHouseItems ON OpportunityItems.numWarehouseItmsID=WareHouseItems.numWareHouseItemID AND OpportunityItems.numItemCode=WareHouseItems.numItemID WHERE numOppId=@numOppID AND OpportunityItems.numWarehouseItmsID > 0 AND ISNULL(bitDropShip,0)=0)
		BEGIN
			RAISERROR('INVALID_ITEM_WAREHOUSES',16,1)
			RETURN
		END
	END

	IF(SELECT 
			COUNT(*) 
		FROM 
			OpportunityBizDocItems OBDI 
		INNER JOIN 
			OpportunityBizDocs OBD 
		ON 
			OBDI.numOppBizDocID=OBD.numOppBizDocsID 
		WHERE 
			OBD.numOppId=@numOppID AND OBDI.numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)) > 0
	BEGIN
		RAISERROR('ITEM_USED_IN_BIZDOC',16,1)
		RETURN
	END

	IF @tintOppType=1 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
	BEGIN
		IF(SELECT 
				COUNT(*)
			FROM
			(
				SELECT
					numOppItemID
					,SUM(numUnitHour) PackingSlipUnits
				FROM 
					OpportunityBizDocItems OBDI 
				INNER JOIN 
					OpportunityBizDocs OBD 
				ON 
					OBDI.numOppBizDocID=OBD.numOppBizDocsID 
				WHERE 
					OBD.numOppId=@numOppID
					AND OBD.numBizDocId=29397 --Picking Slip
				GROUP BY
					numOppItemID
			) AS TEMP
			INNER JOIN
				OpportunityItems OI
			ON
				TEMP.numOppItemID = OI.numoppitemtCode
			WHERE
				OI.numUnitHour < PackingSlipUnits
			) > 0
		BEGIN
			RAISERROR('ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_PACKING_SLIP_QTY',16,1)
			RETURN
		END
	END

	SET @TotalAmount=0                                                           
	SELECT @TotalAmount = SUM(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID                                                                          
  
	UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numOppID

/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
            
			
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT

			SET @bitIsInitalSalesOrder=(SELECT bitIsInitalSalesOrder FROM OpportunityMaster WHERE numOppId=@numOppID AND numDomainId=@numDomainID)
			--SET @bitViolatePrice=(SELECT bitMarginPriceViolated FROM Domain WHERE numDomainId=@numDomainID)
			IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainId=@numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1)
			BEGIN
				SET @bitViolatePrice = 1
			END
			
			IF(@bitViolatePrice=1 AND @bitIsInitalSalesOrder=1 AND @tintClickBtn = 1)
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus=@DealStatus WHERE numOppId=@numOppID
			END
			ELSE
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus = @DealStatus WHERE numOppId=@numOppID
				DECLARE @tintShipped AS TINYINT               
				SELECT @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID               
				
				IF @tintOppStatus=1 AND @tintCommitAllocation=1             
				BEGIN         
					EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
				END  
				declare @tintCRMType as numeric(9)        
				declare @AccountClosingDate as datetime        

				select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from OpportunityMaster where numOppID=@numOppID         
				select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

				--When deal is won                                         
				IF @DealStatus = 1 
				BEGIN
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID              
					END

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=getutcdate()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
				END         
				--When Deal is Lost
				ELSE IF @DealStatus = 2
				BEGIN
					SELECT @AccountClosingDate=bintAccountClosingDate FROM OpportunityMaster WHERE numOppID=@numOppID         
					
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID
					END
				END 
			END

			/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
			-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
			if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
			begin        
				update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				where numDivisionID=@numDivisionID        
			end        

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50), @vcAltContact VARCHAR(200)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9), @numContact NUMERIC(18,0)
DECLARE @bitIsPrimary BIT, @bitAltContact BIT;
DECLARE @tintAddressOf TINYINT

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

--Bill Address
IF @numBillAddressId>0
BEGIN
	SELECT 
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM 
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numBillAddressId

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numBillAddressId
	END

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END
  
  
--Ship Address
IF @intUsedShippingCompany = 92 -- WILL CALL (PICK UP FROM Warehouse)
BEGIN
	IF EXISTS (SELECT numWareHouseID FROM Warehouses WHERE Warehouses.numDomainID = @numDomainID AND numWareHouseID = @numWillCallWarehouseID AND ISNULL(numAddressID,0) > 0)
	BEGIN
		SELECT  
			@vcStreet=ISNULL(vcStreet,'')
			,@vcCity=ISNULL(vcCity,'')
			,@vcPostalCode=ISNULL(AddressDetails.vcPostalCode,'')
			,@numState=ISNULL(numState,0)
			,@numCountry=ISNULL(numCountry,0)
			,@vcAddressName=vcAddressName
		FROM 
			dbo.Warehouses 
		INNER JOIN
			AddressDetails
		ON
			Warehouses.numAddressID = AddressDetails.numAddressID
		WHERE 
			Warehouses.numDomainID = @numDomainID 
			AND numWareHouseID = @numWillCallWarehouseID
	END
	ELSE
	BEGIN
	SELECT  
		@vcStreet=ISNULL(vcWStreet,'')
		,@vcCity=ISNULL(vcWCity,'')
		,@vcPostalCode=ISNULL(vcWPinCode,'')
		,@numState=ISNULL(numWState,0)
		,@numCountry=ISNULL(numWCountry,0)
		,@vcAddressName=''
	FROM 
		dbo.Warehouses 
	WHERE 
		numDomainID = @numDomainID 
		AND numWareHouseID = @numWillCallWarehouseID
	END

	UPDATE OpportunityMaster SET tintShipToType=2 WHERE numOppId=@numOppID
 
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = '', --  varchar(100)
	@numCompanyId =0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = 0,
	@bitAltContact = 0,
	@vcAltContact = ''
END
ELSE IF @numShipAddressId>0
BEGIN
	SELECT
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM  
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numShipAddressId
 

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numShipAddressId
	END


  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END

-- IMPORTANT:KEEP THIS BELOW ADDRESS UPDATE LOGIC
-- GET TAX APPLICABLE TO DIVISION WHEN ORDER IS CREATED AFTER THAT NOT NEED TO CHANGE IT
IF @bitNewOrder = 1
BEGIN
	--INSERT TAX FOR DIVISION   
	IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
	BEGIN
		INSERT INTO dbo.OpportunityMasterTaxItems 
		(
			numOppId,
			numTaxItemID,
			fltPercentage,
			tintTaxType,
			numTaxID
		) 
		SELECT 
			@numOppID,
			TI.numTaxItemID,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			TaxItems TI 
		JOIN 
			DivisionTaxTypes DTT 
		ON 
			TI.numTaxItemID = DTT.numTaxItemID
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
		) AS TEMPTax
		WHERE 
			DTT.numDivisionID=@numDivisionId 
			AND DTT.bitApplicable=1
		UNION 
		SELECT 
			@numOppID,
			0,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			dbo.DivisionMaster 
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
		) AS TEMPTax
		WHERE 
			bitNoTax=0 
			AND numDivisionID=@numDivisionID	
		UNION 
		SELECT
			@numOppId
			,1
			,decTaxValue
			,tintTaxType
			,numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,@numOppId,1,NULL)		
	END	
END

--INSERT TAX FOR DIVISION   
IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN
--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID
) 
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID = IT.numTaxItemID 
WHERE 
	OI.numOppId=@numOppID 
	AND IT.bitApplicable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	0,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numOppID 
	AND I.bitTaxable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT
	@numOppId,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
FROM
	dbo.OpportunityItems OI 
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
INNER JOIN
	TaxDetails TD
ON
	TD.numTaxID = IT.numTaxID
	AND TD.numDomainId = @numDomainId
WHERE
	OI.numOppId = @numOppID
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

END
  
 UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID

	IF @tintOppType=1 AND @tintOppStatus=1 
	BEGIN
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numStatus
		EXEC USP_OpportunityMaster_CreateBackOrderPO @numDomainID,@numUserCntID,@numOppID
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
/****** Object:  StoredProcedure [dbo].[USP_OPPItemDetails]    Script Date: 09/01/2009 18:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_OPPItemDetails @numItemCode=31455,@OpportunityId=12387,@numDomainId=110
--created by anooop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetItems')
DROP PROCEDURE USP_OpportunityMaster_GetItems
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetItems]
(
    @numUserCntID NUMERIC(9) = 0,
    @OpportunityId NUMERIC(9) = 0,
    @numDomainId NUMERIC(9),
    @ClientTimeZoneOffset  INT
)
AS 
BEGIN
    IF @OpportunityId >0
    BEGIN
        DECLARE @DivisionID AS NUMERIC(9)
        DECLARE @TaxPercentage AS FLOAT
        DECLARE @tintOppType AS TINYINT
        DECLARE @fld_Name AS VARCHAR(500)
        DECLARE @fld_ID AS NUMERIC(9)
        DECLARE @numBillCountry AS NUMERIC(9)
        DECLARE @numBillState AS NUMERIC(9)
    
        SELECT  
			@DivisionID = numDivisionID
			,@tintOppType = tintOpptype
        FROM 
			OpportunityMaster
        WHERE 
			numOppId = @OpportunityId
               

		CREATE TABLE #tempForm 
		(
			ID INT IDENTITY(1,1)
			,tintOrder TINYINT
			,vcDbColumnName NVARCHAR(50)
			,vcOrigDbColumnName NVARCHAR(50)
			,vcFieldName NVARCHAR(50)
			,vcAssociatedControlType NVARCHAR(50)
			,vcListItemType VARCHAR(3)
			,numListID NUMERIC(9)
			,vcLookBackTableName VARCHAR(50)
			,bitCustomField BIT
			,numFieldId NUMERIC
			,bitAllowSorting BIT
			,bitAllowEdit BIT
			,bitIsRequired BIT
			,bitIsEmail BIT
			,bitIsAlphaNumeric BIT
			,bitIsNumeric BIT
			,bitIsLengthValidation BIT
			,intMaxLength INT
			,intMinLength INT
			,bitFieldMessage BIT
			,vcFieldMessage VARCHAR(500)
			,ListRelID NUMERIC(9)
			,intColumnWidth INT
			,intVisible INT
			,vcFieldDataType CHAR(1)
			,numUserCntID NUMERIC(18,0)
		)

		
		IF (
				SELECT 
					COUNT(*)
				FROM 
					View_DynamicColumns DC 
				WHERE 
					DC.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) and DC.numDomainID=@numDomainID AND numUserCntID = @numUserCntID AND
					ISNULL(DC.bitSettingField,0)=1 AND ISNULL(DC.bitDeleted,0)=0 AND ISNULL(DC.bitCustom,0)=0) > 0
			OR
			(
				SELECT 
					COUNT(*)
				FROM 
					View_DynamicCustomColumns
				WHERE 
					numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0 AND ISNULL(bitCustom,0)=1
			) > 0
		BEGIN
			INSERT INTO 
				#tempForm
			SELECT 
				ISNULL(DC.tintRow,0) + 1 as tintOrder
				,DDF.vcDbColumnName
				,DDF.vcOrigDbColumnName
				,ISNULL(DDF.vcCultureFieldName,DDF.vcFieldName)
				,DDF.vcAssociatedControlType
				,DDF.vcListItemType
				,DDF.numListID,DC.vcLookBackTableName                                                 
				,DDF.bitCustom,DC.numFieldId
				,DDF.bitAllowSorting
				,DDF.bitInlineEdit
				,DDF.bitIsRequired
				,DDF.bitIsEmail
				,DDF.bitIsAlphaNumeric
				,DDF.bitIsNumeric
				,DDF.bitIsLengthValidation
				,DDF.intMaxLength
				,DDF.intMinLength
				,DDF.bitFieldMessage
				,DDF.vcFieldMessage vcFieldMessage
				,DDF.ListRelID
				,DC.intColumnWidth
				,CASE WHEN DC.numFieldID IS NULL THEN 0 ELSE 1 END
				,DDF.vcFieldDataType
				,DC.numUserCntID
			FROM  
				View_DynamicDefaultColumns DDF 
			LEFT JOIN 
				View_DynamicColumns DC 
			ON 
				DC.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) 
				AND DDF.numFieldId=DC.numFieldId 
				AND DC.numUserCntID=@numUserCntID 
				AND DC.numDomainID=@numDomainID 
				AND numRelCntType=0 
				AND tintPageType=1 
			WHERE 
				DDF.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) 
				AND DDF.numDomainID=@numDomainID 
				AND ISNULL(DDF.bitSettingField,0)=1 
				AND ISNULL(DDF.bitDeleted,0)=0 
				AND ISNULL(DDF.bitCustom,0)=0
			UNION
			SELECT 
				tintRow + 1 AS tintOrder
				,vcDbColumnName
				,vcFieldName
				,vcFieldName
				,vcAssociatedControlType
				,'' as vcListItemType
				,numListID
				,''                                                 
				,bitCustom
				,numFieldId
				,bitAllowSorting
				,0 as bitAllowEdit
				,bitIsRequired
				,bitIsEmail
				,bitIsAlphaNumeric
				,bitIsNumeric
				,bitIsLengthValidation
				,intMaxLength
				,intMinLength
				,bitFieldMessage
				,vcFieldMessage
				,ListRelID
				,intColumnWidth
				,1
				,''
				,0
			FROM 
				View_DynamicCustomColumns
			WHERE 
				numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) 
				AND numUserCntID=@numUserCntID 
				AND numDomainID=@numDomainID 
				AND tintPageType=1 
				AND numRelCntType=0
				AND ISNULL(bitCustom,0)=1
		END
		ELSE
		BEGIN
			--Check if Master Form Configuration is created by administrator if NoColumns=0
			DECLARE @IsMasterConfAvailable BIT = 0
			DECLARE @numUserGroup NUMERIC(18,0)

			SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

			IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = (CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
			BEGIN
				SET @IsMasterConfAvailable = 1
			END

			--If MasterConfiguration is available then load it otherwise load default columns
			IF @IsMasterConfAvailable = 1
			BEGIN
				INSERT INTO DycFormConfigurationDetails 
				(
					numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
				)
				SELECT 
					(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END),numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,0,intColumnWidth
				FROM 
					View_DynamicColumnsMasterConfig
				WHERE
					View_DynamicColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
					View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
					View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
					View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
					ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
					ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
				UNION
				SELECT 
					(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END),numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,1,intColumnWidth
				FROM 
					View_DynamicCustomColumnsMasterConfig
				WHERE 
					View_DynamicCustomColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
					View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
					View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
					View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
					ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1


				INSERT INTO #tempForm
				SELECT 
					(intRowNum + 1) as tintOrder, vcDbColumnName, vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
					vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
					bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
					bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,1,vcFieldDataType,@numUserCntID
				FROM 
					View_DynamicColumnsMasterConfig
				WHERE
					View_DynamicColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
					View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
					View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
					View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
					ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
					ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
				UNION
				SELECT 
					tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
					 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
					,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
					bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
					intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'',0
				FROM 
					View_DynamicCustomColumnsMasterConfig
				WHERE 
					View_DynamicCustomColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
					View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
					View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
					View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
					ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
				ORDER BY 
					tintOrder ASC  
			END
		END

		
		DECLARE @strSQL VARCHAR(MAX) = ''

		DECLARE @avgCost INT
		DECLARE @tintCommitAllocation TINYINT
		SELECT @avgCost=ISNULL(numCost,0),@tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID

		--------------Make Dynamic Query--------------
		SET @strSQL = @strSQL + 'SELECT 
									Opp.numoppitemtCode
									,Opp.vcNotes
									,WItems.numBackOrder
									,CAST(ISNULL(Opp.numCost,0) AS DECIMAL(20,5)) AS numCost
									,ISNULL(Opp.bitItemPriceApprovalRequired,0) AS bitItemPriceApprovalRequired
									,ISNULL(Opp.numSOVendorID,0) AS numVendorID
									,' 
									+ CONCAT('ISNULL(Opp.monTotAmount,0) - (ISNULL(Opp.numUnitHour,0) * (CASE ',@avgCost,' WHEN 3 THEN ISNULL(V.monCost,0) / dbo.fn_UOMConversion(numPurchaseUnit,I.numItemCode,',@numDomainID,',numBaseUnit) WHEN 2 THEN (CASE WHEN ISNULL(Opp.numSOVendorID,0) > 0 THEN Opp.numCost ELSE ISNULL(V.monCost,0) / dbo.fn_UOMConversion(numPurchaseUnit,I.numItemCode,',@numDomainID,',numBaseUnit) END) ELSE monAverageCost END)) AS vcMargin,') + '
									OM.tintOppType
									,Opp.numItemCode
									,I.charItemType
									,ISNULL(I.bitAsset,0) as bitAsset
									,ISNULL(I.bitRental,0) as bitRental
									,ISNULL(I.bitSerialized,0) bitSerialized
									,ISNULL(I.bitLotNo,0) bitLotNo
									,Opp.numOppId
									,Opp.bitWorkOrder
									,Opp.fltDiscount
									,ISNULL(Opp.bitDiscountType,0) bitDiscountType
									,dbo.fn_UOMConversion(Opp.numUOMId, Opp.numItemCode,' + CAST(@numDomainId AS VARCHAR) +', NULL) AS UOMConversionFactor
									,ISNULL(Opp.bitWorkOrder,0) bitWorkOrder
									,ISNULL(Opp.numUnitHourReceived,0) numUnitHourReceived
									,ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=Opp.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS numSerialNoAssigned
									,(CASE 
										WHEN (SELECT 
												COUNT(*) 
											FROM 
												OpportunityBizDocs OB 
											JOIN 
												dbo.OpportunityBizDocItems OBI 
											ON 
												ob.numOppBizDocsId=obi.numOppBizDocID 
												AND (ob.bitAuthoritativeBizDocs=1 OR ob.numBizDocId=304) 
												AND obi.numOppItemID=Opp.numoppitemtCode 
												AND ob.numOppId=Opp.numOppId)>0 
											THEN 1 
											ELSE 0 
									END) AS bitIsAuthBizDoc
									,(CASE 
										WHEN (SELECT 
													COUNT(*) 
												FROM 
													OpportunityBizDocs OB 
												JOIN 
													dbo.OpportunityBizDocItems OBI 
												ON 
													OB.numOppBizDocsId=OBI.numOppBizDocID 
												WHERE 
													OB.numOppId = Opp.numOppId 
													AND OBI.numOppItemID=Opp.numoppitemtCode 
													AND OB.numBizDocId=296) > 0 
											THEN 1 
											ELSE 0 
										END) AS bitAddedFulFillmentBizDoc
									,ISNULL(numPromotionID,0) AS numPromotionID
									,ISNULL(opp.numUOMId, 0) AS numUOM
									,(SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName]
									,Opp.numUnitHour AS numOriginalUnitHour 
									,ISNULL(M.vcPOppName,''Internal'') as Source
									,numSourceID
									,(CASE WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0 WHEN ISNULL(bitKitParent,0) = 1 THEN 1 ELSE 0 END) as bitKitParent
									,(CASE 
										WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 
										THEN 
											(CASE 
												WHEN (I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
												THEN 
													dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,Opp.numWarehouseItmsID,(CASE WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0  WHEN ISNULL(bitKitParent,0) = 1 THEN 1 ELSE 0 END)) 
												ELSE 0 
											END) 
										ELSE 0 
									END) AS bitBackOrder
									,(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass]
									,ISNULL(Opp.numQtyShipped,0) numQtyShipped
									,ISNULL(Opp.numUnitHourReceived,0) AS numUnitHourReceived
									,ISNULL(OM.fltExchangeRate,0) AS fltExchangeRate
									,ISNULL(OM.numCurrencyID,0) AS numCurrencyID
									,ISNULL(OM.numDivisionID,0) AS numDivisionID
									,(CASE 
										WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 AND UPPER(I.charItemType) = ''P'' AND bitDropShip = 0
										THEN (CASE WHEN OM.tintShipped = 1 THEN CAST(ISNULL(Opp.numUnitHour,0) AS VARCHAR) ELSE CAST(ISNULL(Opp.numQtyShipped,0) AS VARCHAR) END)
										WHEN OM.tintOppType = 2 AND OM.tintOppStatus = 1 AND UPPER(I.charItemType) = ''P'' AND bitDropShip = 0
										THEN (CASE WHEN OM.tintShipped = 1 THEN CAST(ISNULL(Opp.numUnitHour,0) AS VARCHAR) ELSE CAST(ISNULL(Opp.numUnitHourReceived,0) AS VARCHAR) END)
										ELSE '''' 
									END) AS vcShippedReceived
									,ISNULL(I.numIncomeChartAcntId,0) AS itemIncomeAccount
									,ISNULL(I.numAssetChartAcntId,0) AS itemInventoryAsset
									,ISNULL(i.numCOGsChartAcntId,0) AS itemCoGs
									,ISNULL(Opp.numRentalIN,0) as numRentalIN
									,ISNULL(Opp.numRentalLost,0) as numRentalLost
									,ISNULL(Opp.numRentalOut,0) as numRentalOut
									,ISNULL(bitFreeShipping,0) AS [IsFreeShipping]
									,Opp.bitDiscountType,isnull(Opp.monLandedCost,0) as monLandedCost
									, ISNULL(Opp.vcPromotionDetail,'''') AS vcPromotionDetail
									,'''' AS numPurchasedQty
									,Opp.vcPathForTImage
									,Opp.vcItemName
									,Opp.vcModelID
									,vcWarehouse
									,ISNULL(WItems.numWarehouseID,0) AS numWarehouseID
									,ISNULL(Opp.numShipToAddressID,0) AS numShipToAddressID
									,Case when ISNULL(I.bitKitParent,0)=1 then CAST(ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, WItems.numWareHouseID),0) AS DECIMAL(18,2)) ELSE CAST(ISNULL(WItems.numOnHand,0) AS FLOAT) END numOnHand
									,ISNULL(WItems.numAllocation,0) as numAllocation
									,WL.vcLocation
									,ISNULL(Opp.vcType,'''') ItemType
									,ISNULL(Opp.vcType,'''') vcType
									,ISNULL(Opp.vcItemDesc, '''') vcItemDesc
									,Opp.vcAttributes
									,ISNULL(bitDropShip, 0) as DropShip
									,CASE ISNULL(bitDropShip, 0) WHEN 1 THEN ''Yes'' ELSE '''' END AS bitDropShip
									,CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,om.numDomainId, ISNULL(opp.numUOMId, 0))* Opp.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour
									,Opp.monPrice
									,CAST(( dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), Opp.numItemCode,om.numDomainId, ISNULL(I.numBaseUnit, 0)) * Opp.monPrice) AS NUMERIC(18, 6)) AS monPriceUOM
									,Opp.monTotAmount
									,Opp.monTotAmtBefDiscount
									,ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'''')) [vcSKU]
									,Opp.vcManufacturer
									,dbo.[fn_GetListItemName](I.[numItemClassification]) numItemClassification
									,(SELECT [vcItemGroup] FROM [ItemGroups] WHERE [numItemGroupID] = I.numItemGroup) numItemGroup
									,ISNULL(u.vcUnitName, '''') numUOMId
									,I.fltWeight
									,I.fltHeight
									,I.fltWidth
									,I.fltLength
									,I.numBarCodeId
									,I.IsArchieve
									,ISNULL(I.numContainer,0) AS numContainer
									,ISNULL(I.numNoItemIntoContainer,0) AS numContainerQty
									,CASE WHEN Opp.bitDiscountType = 0
										 THEN REPLACE(STR(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
											  - Opp.monTotAmount,20,4), '' '', '''') + '' (''
											  + CAST(FORMAT(Opp.fltDiscount,''0.00##'') AS VARCHAR(50)) + ''%)''
										 ELSE REPLACE(STR(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
											  - Opp.monTotAmount,20,4), '' '', '''')
									END AS DiscAmt
									,ISNULL(Opp.numProjectID, 0) numProjectID
									,CASE WHEN ( SELECT TOP 1
														ISNULL(OBI.numOppBizDocItemID, 0)
												FROM    dbo.OpportunityBizDocs OBD
														INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID
												WHERE   OBI.numOppItemID = Opp.numoppitemtCode
														AND ISNULL(OBD.bitAuthoritativeBizDocs, 0) = 1
														and isnull(OBD.numOppID,0)=OM.numOppID
											  ) > 0 THEN 1
										 ELSE 0
									END AS bitItemAddedToAuthBizDoc
									,ISNULL(Opp.numClassID, 0) numClassID
									,ISNULL(Opp.numProjectStageID, 0) numProjectStageID
									,CASE 
										WHEN OPP.numProjectStageID > 0 THEN dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID)
										WHEN OPP.numProjectStageID = -1 THEN ''T&E''
										WHEN OPP.numProjectId > 0 THEN CASE WHEN OM.tintOppType = 1 THEN ''S.O.'' ELSE ''P.O.'' END
										ELSE ''-''
									END AS vcProjectStageName
									,CAST(Opp.numUnitHour AS FLOAT) numQty
									,ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo
									,Opp.numWarehouseItmsID,CPN.CustomerPartNo
									,(CASE WHEN ISNULL(Opp.bitMarkupDiscount,0) = 0 THEN ''0'' ELSE ''1'' END) AS bitMarkupDiscount
									,CAST(Opp.ItemRequiredDate AS DATE) AS ItemRequiredDate,'


		
			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='vcWorkOrderStatus' AND ISNULL(numUserCntID,0) = @numUserCntID)
			   BEGIN
					SET @strSQL = @strSQL + CONCAT('dbo.GetWorkOrderStatus(om.numDomainID,',@tintCommitAllocation,',Opp.numOppID,Opp.numoppitemtCode) AS vcWorkOrderStatus,')
			   END     

			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='vcBackordered' AND ISNULL(numUserCntID,0) = @numUserCntID)
			   BEGIN
					SET @strSQL = @strSQL + '(CASE 
												WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 
												THEN 
													(CASE 
														WHEN (I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
														THEN 
															CAST(dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,Opp.numWarehouseItmsID,(CASE WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0  WHEN ISNULL(bitKitParent,0) = 1 THEN 1 ELSE 0 END)) AS VARCHAR)
														ELSE ''''
													END) 
												ELSE ''''
											END) vcBackordered, '
			   END


			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='vcItemReleaseDate' AND ISNULL(numUserCntID,0) = @numUserCntID)
			   BEGIN
					SET @strSQL = @strSQL + '(CASE WHEN I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 THEN ISNULL(dbo.FormatedDateFromDate(Opp.ItemReleaseDate,om.numDomainId),''Not Available'') ELSE '''' END)  AS vcItemReleaseDate,'
			   END

			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='SerialLotNo' AND ISNULL(numUserCntID,0) = @numUserCntID)
			   BEGIN
					SET @strSQL = @strSQL + 'SUBSTRING((SELECT  
													'', '' + vcSerialNo + CASE WHEN ISNULL(I.bitLotNo, 0) = 1 THEN '' ('' + CONVERT(VARCHAR(15), oppI.numQty) + '')'' ELSE ''''	END
												FROM 
													OppWarehouseSerializedItem oppI
												JOIN 
													WareHouseItmsDTL whi 
												ON 
													oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
												WHERE 
													oppI.numOppID = Opp.numOppId
													AND oppI.numOppItemID = Opp.numoppitemtCode
												ORDER BY 
													vcSerialNo
												FOR
													XML PATH('''')), 3, 200000) AS vcSerialLotNo,'
			   END
			   
			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='vcInclusionDetails' AND ISNULL(numUserCntID,0) = @numUserCntID)
			   BEGIN
					SET @strSQL = @strSQL + CONCAT('dbo.GetOrderAssemblyKitInclusionDetails(OM.numOppID,Opp.numoppitemtCode,Opp.numUnitHour,',@tintCommitAllocation,',0) AS vcInclusionDetails,')
			   END    
			   
			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='OppItemShipToAddress' )
			   BEGIN
					SET @strSQL = @strSQL +  '(CASE WHEN Opp.numShipToAddressID > 0 THEN (+ isnull((AD.vcStreet)  +'', '','''') + isnull((AD.vcCity)+ '', '','''')  + '' <br>''
                            + isnull(dbo.fn_GetState(AD.numState) + '', '' ,'''') 
                            + isnull(AD.vcPostalCode + '', '' ,'''')  + '' <br>''  +
                            + isnull(dbo.fn_GetListItemName(AD.numCountry),'''') )
							ELSE '''' END ) AS OppItemShipToAddress,'
			   END   
	
			-- (CASE WHEN @tintOppType=1 THEN 26 ELSE 129 END) --

		--------------Add custom fields to query----------------
		SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM #tempForm WHERE ISNULL(bitCustomField,0)=1 ORDER BY numFieldId
		WHILE @fld_ID > 0
		BEGIN 
			SET @strSQL = @strSQL + CONCAT('dbo.GetCustFldValueOppItems(',@fld_ID,',Opp.numoppitemtCode,Opp.numItemCode) as ''',@fld_Name,''',')
       
			SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM #tempForm WHERE ISNULL(bitCustomField,0)=1 AND numFieldId > @fld_ID ORDER BY numFieldId
		 
			IF @@ROWCOUNT=0
				SET @fld_ID = 0
		END

		 SET @strSQL = left(@strSQL,len(@strSQL)-1) + '   
				FROM    OpportunityItems Opp
				LEFT JOIN OpportunityMaster OM ON Opp.numOppID = oM.numOppID
				INNER JOIN DivisionMaster DM ON OM.numDivisionID=DM.numDivisionID
				JOIN item I ON Opp.numItemCode = i.numItemcode
				LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = Opp.numWarehouseItmsID
				LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
				LEFT JOIN WarehouseLocation WL ON WL.numWLocationID = WItems.numWLocationID
				LEFT JOIN OpportunityMaster M ON Opp.numSourceID = M.numOppID
				LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId
				LEFT JOIN Vendor V ON I.numVendorID = V.numVendorID  AND V.numItemCode = I.numItemCode
				LEFT JOIN CustomerPartNumber CPN ON CPN.numItemCode = Opp.numItemCode AND CPN.numCompanyId=DM.numCompanyID
				LEFT JOIN AddressDetails AD ON Opp.numShipToAddressID = AD.numAddressID
		WHERE   om.numDomainId = '+ CONVERT(VARCHAR(10),@numDomainId) + '
				AND Opp.numOppId = '+ CONVERT(VARCHAR(15),@OpportunityId) +'
		ORDER BY opp.numSortOrder,numoppitemtCode ASC '
  
		--PRINT CAST(@strSQL AS NTEXT);
		EXEC (@strSQL);
		SELECT  vcSerialNo,
			   vcComments AS Comments,
				numOppItemID AS numoppitemtCode,
				W.numWarehouseItmsDTLID,
				numWarehouseItmsID AS numWItmsID,
				dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
									 1) Attributes
		FROM    WareHouseItmsDTL W
				JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
		WHERE   numOppID = @OpportunityId


		select * from #tempForm order by tintOrder

		drop table #tempForm

    END         
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PromotionOfferOrder_GetByPromotionID')
DROP PROCEDURE USP_PromotionOfferOrder_GetByPromotionID
GO
CREATE PROCEDURE [dbo].[USP_PromotionOfferOrder_GetByPromotionID]
	@numDomainID NUMERIC(18,0)
	,@numPromotionID NUMERIC(18,0)
AS
BEGIN
	SELECT * FROM PromotionOffer WHERE numDomainId=@numDomainID AND numProId = @numPromotionID 
	 	
	SELECT ROW_NUMBER() OVER(ORDER BY numProOfferOrderID ASC) AS RowNum, numOrderAmount,fltDiscountValue FROM PromotionOfferOrder WHERE numPromotionID = @numPromotionID ORDER BY numProOfferOrderID

	SELECT * FROM DiscountCodes WHERE numPromotionID=@numPromotionID
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportQueryExecute')
DROP PROCEDURE USP_ReportQueryExecute
GO
CREATE PROCEDURE [dbo].[USP_ReportQueryExecute]     
    @numDomainID NUMERIC(18,0),
    @numUserCntID NUMERIC(18,0),
	@numReportID NUMERIC(18,0),
    @ClientTimeZoneOffset INT, 
    @textQuery NTEXT,
	@numCurrentPage NUMERIC(18,0)
AS                 
BEGIN
	DECLARE @tintReportType INT
	SELECT @tintReportType=ISNULL(tintReportType,0) FROM ReportListMaster WHERE numReportID=@numReportID

	IF CHARINDEX('AND WareHouseItems.vcLocation',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'AND WareHouseItems.vcLocation','AND WareHouseItems.numWLocationID')
	END

	IF CHARINDEX('ReturnItems.monPrice',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'ReturnItems.monPrice','CAST(ReturnItems.monPrice AS NUMERIC(18,4))')
	END

	IF CHARINDEX('OpportunityItems.monPrice',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityItems.monPrice','CAST(OpportunityItems.monPrice AS NUMERIC(18,4))')
	END

	IF CHARINDEX('isnull(category.numCategoryID,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(category.numCategoryID,0)' ,'(SELECT ISNULL(STUFF((SELECT '','' + CONVERT(VARCHAR(50) , Category.vcCategoryName) FROM ItemCategory LEFT JOIN Category ON ItemCategory.numCategoryID = Category.numCategoryID WHERE ItemCategory.numItemID = Item.numItemCode FOR XML PATH('''')), 1, 1, ''''),''''))')
	END

	IF ISNULL(@numCurrentPage,0) > 0 
		AND ISNULL(@tintReportType,0) <> 3 
		AND ISNULL(@tintReportType,0) <> 4 
		AND ISNULL(@tintReportType,0) <> 5 
		AND CHARINDEX('Select top',CAST(@textQuery AS VARCHAR(MAX))) <> 1
		AND (SELECT COUNT(*) FROM ReportSummaryGroupList WHERE numReportID=@numReportID) = 0
	BEGIN
		SET @textQuery = CONCAT(CAST(@textQuery AS VARCHAR(MAX)),' OFFSET ',(@numCurrentPage-1) * 100 ,' ROWS FETCH NEXT 100 ROWS ONLY;')
	END
	 
	 print CAST(@textQuery AS ntext)
	EXEC sp_executesql @textQuery,N'@numDomainID numeric(18, 0),@numUserCntID numeric(18,0),@ClientTimeZoneOffset int',@numDomainID=@numDomainID,@numUserCntID=@numUserCntID,@ClientTimeZoneOffset=@ClientTimeZoneOffset
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SalesFulfillmentQueue_UpdateStatus')
DROP PROCEDURE USP_SalesFulfillmentQueue_UpdateStatus
GO
CREATE PROCEDURE [dbo].[USP_SalesFulfillmentQueue_UpdateStatus]
	@numDomainID NUMERIC(18,0),  
	@numSFQID NUMERIC(18,0),
	@vcMessage VARCHAR(MAX),
	@tintRule AS TINYINT,
	@bitSuccess BIT
AS
BEGIN
	DECLARE @numUserCntID NUMERIC(18,0)
	DECLARE @numOppID AS NUMERIC(18,0)
	DECLARE @numRule2SuccessOrderStatus NUMERIC(18,0)
	DECLARE @numRule2FailOrderStatus NUMERIC(18,0)
	DECLARE @numRule3FailOrderStatus NUMERIC(18,0)
	DECLARE @numRule4FailOrderStatus NUMERIC(18,0)
	DECLARE @numRule5OrderStatus NUMERIC(18,0)
	DECLARE @numRule6OrderStatus NUMERIC(18,0)
	DECLARE @bitActive BIT
	DECLARE @bitRule5IsActive BIT
	DECLARE @bitRule6IsActive BIT
	DECLARE @numTempOppID NUMERIC(18,0)
	SET @numTempOppID = @numSFQID

	SELECT
		@bitActive=bitActive
		,@numRule2SuccessOrderStatus=numRule2SuccessOrderStatus
		,@numRule2FailOrderStatus=numRule2FailOrderStatus
		,@numRule3FailOrderStatus=numRule3FailOrderStatus
		,@numRule4FailOrderStatus=numRule4FailOrderStatus
		,@bitRule5IsActive=bitRule5IsActive
		,@numRule5OrderStatus=numRule5OrderStatus
		,@bitRule6IsActive=bitRule6IsActive
		,@numRule6OrderStatus=numRule6OrderStatus
	FROM
		SalesFulfillmentConfiguration
	WHERE
		numDomainID = @numDomainID

	SELECT
		@numOppID=numOppID
		,@numUserCntID=numUserCntID
	FROM
		SalesFulfillmentQueue
	WHERE
		numSFQID=@numSFQID
		AND @tintRule NOT IN (5,6)

	-- CHANGE ORDER STATUS BASED ON SALES FULFILLMENT CONFIGURATION
	IF @tintRule = 2
	BEGIN
		IF @bitSuccess = 1
		BEGIN
			UPDATE OpportunityMaster SET numStatus = @numRule2SuccessOrderStatus WHERE numOppId=@numOppID
			EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numRule2SuccessOrderStatus
		END
		ELSE
		BEGIN
			UPDATE OpportunityMaster SET numStatus = @numRule2FailOrderStatus WHERE numOppId=@numOppID
			EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numRule2FailOrderStatus
		END
	END
	ELSE IF @tintRule = 3 AND ISNULL(@bitSuccess,0) = 0
	BEGIN
		UPDATE OpportunityMaster SET numStatus = @numRule3FailOrderStatus WHERE numOppId=@numOppID
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numRule3FailOrderStatus
	END
	ELSE IF @tintRule = 4 AND ISNULL(@bitSuccess,0) = 0
	BEGIN
		UPDATE OpportunityMaster SET numStatus = @numRule4FailOrderStatus WHERE numOppId=@numOppID
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numRule4FailOrderStatus
	END
	ELSE IF @tintRule = 5 AND ISNULL(@bitRule5IsActive,0) = 1 AND ISNULL(@bitActive,0) = 1
	BEGIN
		UPDATE OpportunityMaster SET numStatus = @numRule5OrderStatus WHERE numOppId=@numSFQID

		IF (
			SELECT 
				COUNT(*) 
			FROM 
				SalesFulfillmentConfiguration 
			WHERE 
				numDomainID=@numDomainId 
				AND ISNULL(bitActive,0)=1
				ANd ISNULL(bitRule5IsActive,0) = 1
		) > 0
		AND NOT EXISTS (SELECT * FROM SalesFulfillmentQueue WHERE numOppID=@numOppID AND numOrderStatus=-1)
		BEGIN
			IF NOT EXISTS (SELECT numSFLID FROM SalesFulfillmentLog WHERE numOppID=@numSFQID AND vcMessage=@vcMessage)
			BEGIN
				INSERT INTO SalesFulfillmentQueue
				( 
					numDomainID
					,numUserCntID
					,numOppID
					,numOrderStatus
					,dtDate
					,bitExecuted
					,intNoOfTimesTried
				)
				VALUES
				(
					@numDomainId
					,0
					,@numSFQID
					,-1
					,GETUTCDATE()
					,1
					,1
				)

				SET @numSFQID = SCOPE_IDENTITY()
			END			
		END
	END
	ELSE IF @tintRule = 6 AND ISNULL(@bitRule6IsActive,0) = 1 AND ISNULL(@bitActive,0) = 1
	BEGIN
		UPDATE OpportunityMaster SET numStatus = @numRule6OrderStatus WHERE numOppId=@numSFQID

		IF (
			SELECT 
				COUNT(*) 
			FROM 
				SalesFulfillmentConfiguration 
			WHERE 
				numDomainID=@numDomainId 
				AND ISNULL(bitActive,0)=1
				ANd ISNULL(bitRule6IsActive,0) = 1
		) > 0
		AND NOT EXISTS (SELECT * FROM SalesFulfillmentQueue WHERE numOppID=@numOppID AND numOrderStatus=-2)
		BEGIN
			INSERT INTO SalesFulfillmentQueue
			( 
				numDomainID
				,numUserCntID
				,numOppID
				,numOrderStatus
				,dtDate
				,bitExecuted
				,intNoOfTimesTried
			)
			VALUES
			(
				@numDomainId
				,0
				,@numSFQID
				,-2
				,GETUTCDATE()
				,1
				,1
			)

			SET @numSFQID = SCOPE_IDENTITY()
		END
	END

	-- MARK RULE AS EXECUTED
	UPDATE
		SalesFulfillmentQueue
	SET
		bitExecuted = 1
	WHERE
		numSFQID=@numSFQID
		AND @tintRule NOT IN (5,6)

	-- MAKE ENTRY IN LOG TABLE
	IF @tintRule = 5
	BEGIN
		IF NOT EXISTS (SELECT numSFLID FROM SalesFulfillmentLog WHERE numSFQID=@numSFQID AND vcMessage=@vcMessage)
		BEGIN
			INSERT INTO SalesFulfillmentLog
			(
				numDomainID,
				numSFQID,
				numOPPID,
				vcMessage,
				dtDate,
				bitSuccess
			)
			SELECT
				numDomainID,
				numSFQID,
				numOppID,
				@vcMessage,
				GETUTCDATE(),
				@bitSuccess
			FROM
				SalesFulfillmentQueue
			WHERE
				numSFQID=@numSFQID
		END
	END
	ELSE IF @tintRule = 6
	BEGIN
		IF NOT EXISTS (SELECT numSFLID FROM SalesFulfillmentLog WHERE numOppID=@numTempOppID AND vcMessage=@vcMessage)
		BEGIN
			INSERT INTO SalesFulfillmentLog
			(
				numDomainID,
				numSFQID,
				numOPPID,
				vcMessage,
				dtDate,
				bitSuccess
			)
			SELECT
				numDomainID,
				numSFQID,
				numOppID,
				@vcMessage,
				GETUTCDATE(),
				@bitSuccess
			FROM
				SalesFulfillmentQueue
			WHERE
				numSFQID=@numSFQID
		END
	END
	ELSE
	BEGIN
		INSERT INTO SalesFulfillmentLog
		(
			numDomainID,
			numSFQID,
			numOPPID,
			vcMessage,
			dtDate,
			bitSuccess
		)
		SELECT
			numDomainID,
			numSFQID,
			numOppID,
			@vcMessage,
			GETUTCDATE(),
			@bitSuccess
		FROM
			SalesFulfillmentQueue
		WHERE
			numSFQID=@numSFQID
	END
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_OPPItemDetails @numItemCode=31455,@OpportunityId=12387,@numDomainId=110
--created by anooop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ShippingReport_Save')
DROP PROCEDURE USP_ShippingReport_Save
GO
CREATE PROCEDURE [dbo].[USP_ShippingReport_Save]
	@numDomainId NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
    ,@numOppBizDocId NUMERIC(18,0)
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @numShippingReportID NUMERIC(18,0)
	DECLARE @IsCOD BIT,@IsDryIce BIT,@IsHoldSaturday BIT,@IsHomeDelivery BIT,@IsInsideDelevery BIT,@IsInsidePickup BIT,@IsReturnShipment BIT,@IsSaturdayDelivery BIT,@IsSaturdayPickup BIT,@IsAdditionalHandling BIT,@IsLargePackage BIT
	DECLARE @numCODAmount NUMERIC(18,2),@vcCODType VARCHAR(50),@numTotalInsuredValue NUMERIC(18,2),@vcDeliveryConfirmation VARCHAR(1000),@vcDescription VARCHAR(MAX),@numTotalCustomsValue NUMERIC(18,2)
	DECLARE @tintSignatureType TINYINT
	DECLARE @numOrderShippingCompany NUMERIC(18,0)
	DECLARE @numOrderShippingService NUMERIC(18,0)
	DECLARE @numDivisionShippingCompany NUMERIC(18,0)
    DECLARE @numDivisionShippingService NUMERIC(18,0)

	SELECT
		@IsCOD = IsCOD
		,@IsHomeDelivery = IsHomeDelivery
		,@IsInsideDelevery = IsInsideDelevery
		,@IsInsidePickup = IsInsidePickup
		,@IsSaturdayDelivery = IsSaturdayDelivery
		,@IsSaturdayPickup = IsSaturdayPickup
		,@IsAdditionalHandling = IsAdditionalHandling
		,@IsLargePackage=IsLargePackage
		,@vcCODType = vcCODType
		,@vcDeliveryConfirmation = vcDeliveryConfirmation
		,@vcDescription = vcDescription
		,@tintSignatureType = CAST(ISNULL(vcSignatureType,'0') AS TINYINT)
		,@numOrderShippingCompany = ISNULL(intUsedShippingCompany,0)
		,@numOrderShippingService = ISNULL(numShippingService,0)
		,@numDivisionShippingCompany = ISNULL(intShippingCompany,0)
		,@numDivisionShippingService = ISNULL(numDefaultShippingServiceID,0)
	FROM
		OpportunityMaster
	INNER JOIN
		DivisionMaster
	ON
		OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
	LEFT JOIN
		DivisionMasterShippingConfiguration
	ON
		DivisionMasterShippingConfiguration.numDomainID=@numDomainId
		AND DivisionMaster.numDivisionID = DivisionMasterShippingConfiguration.numDivisionID
	WHERE
		OpportunityMaster.numDomainId = @numDomainId
		AND DivisionMaster.numDomainID=@numDomainId
		AND OpportunityMaster.numOppId = @numOppID

	DECLARE @vcFromName VARCHAR(1000)
    DECLARE @vcFromCompany VARCHAR(1000)
    DECLARE @vcFromPhone VARCHAR(100)
    DECLARE @vcFromAddressLine1 VARCHAR(50)
    DECLARE @vcFromAddressLine2 VARCHAR(50) = ''
    DECLARE @vcFromCity VARCHAR(50)
	DECLARE @vcFromState VARCHAR(50)
    DECLARE @vcFromZip VARCHAR(50)
    DECLARE @vcFromCountry VARCHAR(50)
	DECLARE @bitFromResidential BIT = 0

	DECLARE @vcToName VARCHAR(1000)
    DECLARE @vcToCompany VARCHAR(1000)
    DECLARE @vcToPhone VARCHAR(100)
    DECLARE @vcToAddressLine1 VARCHAR(50)
    DECLARE @vcToAddressLine2 VARCHAR(50) = ''
	DECLARE @vcToCity VARCHAR(50)
	DECLARE @vcToState VARCHAR(50)
    DECLARE @vcToZip VARCHAR(50)
    DECLARE @vcToCountry VARCHAR(50)
    DECLARE @bitToResidential BIT = 0

	-- GET FROM ADDRESS
	SELECT
		@vcFromName=vcName
		,@vcFromCompany=vcCompanyName
		,@vcFromPhone=vcPhone
		,@vcFromAddressLine1=vcStreet
		,@vcFromCity=vcCity
		,@vcFromState=CAST(vcState AS VARCHAR(18))
		,@vcFromZip=vcZipCode
		,@vcFromCountry=CAST(vcCountry AS VARCHAR(18))
	FROM
		dbo.fn_GetShippingReportAddress(1,@numDomainId,@numOppID)

	-- GET TO ADDRESS
	SELECT
		@vcToName=vcName
		,@vcToCompany=vcCompanyName
		,@vcToPhone=vcPhone
		,@vcToAddressLine1=vcStreet
		,@vcToCity=vcCity
		,@vcToState=CAST(vcState AS VARCHAR(18))
		,@vcToZip=vcZipCode
		,@vcToCountry=CAST(vcCountry AS VARCHAR(18))
	FROM
		dbo.fn_GetShippingReportAddress(2,@numDomainId,@numOppID)

	DECLARE @numShippingCompany NUMERIC(18,0)
	DECLARE @numShippingService NUMERIC(18,0)
	

	-- FIRST CHECK IF ORDER LEVEL SHIPPING FIELDS HAVE VALUES
	IF ISNULL(@numOrderShippingCompany,0) > 0
	BEGIN
		IF ISNULL(@numOrderShippingService,0) = 0
		BEGIN
			SET @numOrderShippingService = CASE @numOrderShippingCompany 
									WHEN 91 THEN 15 --FEDEX
									WHEN 88 THEN 43 --UPS
									WHEN 90 THEN 70 --USPS
								 END
		END

		SET @numShippingCompany = @numOrderShippingCompany
		SET @numShippingService = @numOrderShippingService
	END
	ELSE IF ISNULL(@numDivisionShippingCompany,0) > 0 -- IF DIVISION SHIPPIGN SETTING AVAILABLE
	BEGIN
		IF ISNULL(@numDivisionShippingService,0) = 0
		BEGIN
			SET @numDivisionShippingService = CASE @numDivisionShippingCompany 
									WHEN 91 THEN 15 --FEDEX
									WHEN 88 THEN 43 --UPS
									WHEN 90 THEN 70 --USPS
								 END
		END

		SET @numShippingCompany = @numDivisionShippingCompany
		SET @numShippingService = @numDivisionShippingService
	END
	ELSE -- USE DOMAIN DEFAULT
	BEGIN
		SELECT @numShippingCompany=ISNULL(numShipCompany,0) FROM Domain WHERE numDomainId=@numDomainId
		
		IF @numShippingCompany <> 91 OR @numShippingCompany <> 88 OR @numShippingCompany <> 90
		BEGIN
			SET @numShippingCompany = 91
		END

		SET @numShippingService = CASE @numShippingCompany 
									WHEN 91 THEN 15 --FEDEX
									WHEN 88 THEN 43 --UPS
									WHEN 90 THEN 70 --USPS
								 END
	END



	DECLARE @bitUseBizdocAmount BIT

	SELECT @bitUseBizdocAmount=bitUseBizdocAmount,@numTotalCustomsValue=ISNULL(numTotalInsuredValue,0),@numTotalInsuredValue=ISNULL(numTotalCustomsValue,0) FROM Domain WHERE numDomainId=@numDomainId

	IF @bitUseBizdocAmount = 1
	BEGIN
		SELECT @numTotalCustomsValue=ISNULL(monDealAmount,0),@numTotalInsuredValue=ISNULL(monDealAmount,0),@numCODAmount=ISNULL(monDealAmount,0) FROM OpportunityBizDocs WHERE numOppBizDocsId=@numOppBizDocId 
	END
	ELSE
	BEGIN
		SELECT @numCODAmount=ISNULL(monDealAmount,0) FROM OpportunityBizDocs WHERE numOppBizDocsId=@numOppBizDocId 
	END

	-- MAKE ENTRY IN ShippingReport TABLE
	INSERT INTO ShippingReport
	(
		numDomainId,numCreatedBy,dtCreateDate,numOppID,numOppBizDocId,numShippingCompany,vcValue2
		,vcFromName,vcFromCompany,vcFromPhone,vcFromAddressLine1,vcFromAddressLine2,vcFromCity,vcFromState,vcFromZip,vcFromCountry,bitFromResidential
		,vcToName,vcToCompany,vcToPhone,vcToAddressLine1,vcToAddressLine2,vcToCity,vcToState,vcToZip,vcToCountry,bitToResidential
		,IsCOD,IsDryIce,IsHoldSaturday,IsHomeDelivery,IsInsideDelevery,IsInsidePickup,IsReturnShipment,IsSaturdayDelivery,IsSaturdayPickup
		,numCODAmount,vcCODType,numTotalInsuredValue,IsAdditionalHandling,IsLargePackage,vcDeliveryConfirmation,vcDescription,numTotalCustomsValue,tintSignatureType
	)
	VALUES
	(
		@numDomainId,@numUserCntID,GETUTCDATE(),@numOppID,@numOppBizDocId,@numShippingCompany,CAST(@numShippingService AS VARCHAR)
		,@vcFromName,@vcFromCompany,@vcFromPhone,@vcFromAddressLine1,@vcFromAddressLine2,@vcFromCity,@vcFromState,@vcFromZip,@vcFromCountry,@bitFromResidential
		,@vcToName,@vcToCompany,@vcToPhone,@vcToAddressLine1,@vcToAddressLine2,@vcToCity,@vcToState,@vcToZip,@vcToCountry,@bitToResidential
		,@IsCOD,@IsDryIce,@IsHoldSaturday,@IsHomeDelivery,@IsInsideDelevery,@IsInsidePickup,@IsReturnShipment,@IsSaturdayDelivery,@IsSaturdayPickup
		,@numCODAmount,@vcCODType,@numTotalInsuredValue,@IsAdditionalHandling,@IsLargePackage,@vcDeliveryConfirmation,@vcDescription,@numTotalCustomsValue,@tintSignatureType
	)

	SET @numShippingReportID = SCOPE_IDENTITY()

	-- GET CONTAINER BY WAREHOUSE AND NOOFITEMSCANBEADDED
	DECLARE @TEMPContainer TABLE
	(
		ID INT IDENTITY(1,1)
		,numContainer NUMERIC(18,0)
		,numWareHouseID NUMERIC(18,0)
		,numContainerQty NUMERIC(18,0)
		,numNoItemIntoContainer NUMERIC(18,0)
		,numTotalContainer NUMERIC(18,0)
		,numUsedContainer NUMERIC(18,0)
		,bitItemAdded BIT
		,fltWeight FLOAT
		,fltHeight FLOAT
		,fltWidth FLOAT
		,fltLength FLOAT
	)

	INSERT INTO 
		@TEMPContainer
	SELECT
		T1.numContainer
		,T1.numWareHouseID
		,T1.numNoItemIntoContainer
		,T1.numNoItemIntoContainer
		,T1.numTotalContainer
		,0 AS numTotalContainer
		,0 AS bitItemAdded
		,T2.fltWeight
		,T2.fltHeight
		,T2.fltWidth
		,T2.fltLength
	FROM
	(
		SELECT 
			I.numContainer
			,WI.numWareHouseID
			,I.numNoItemIntoContainer
			,CEILING(SUM(OBI.numUnitHour)/CAST(I.numNoItemIntoContainer AS FLOAT)) AS numTotalContainer
			,0 AS numUsedContainer
		FROM 
			OpportunityBizDocItems OBI
		INNER JOIN 
			Item AS I
		ON 
			OBI.numItemCode = I.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OBI.numWarehouseItmsID = WI.numWareHouseItemID
		WHERE
			numOppBizDocID=@numOppBizDocID
			AND I.charItemType ='P'
			AND ISNULL(numContainer,0) > 0
		GROUP BY
			I.numContainer
			,WI.numWareHouseID
			,I.numNoItemIntoContainer
	) AS T1
	INNER JOIN
	(
		SELECT
			I.numItemCode
			,WI.numWareHouseID
			,ISNULL(I.fltWeight,0) fltWeight
			,ISNULL(I.fltHeight,0) fltHeight
			,ISNULL(I.fltWidth,0) fltWidth
			,ISNULL(I.fltLength,0) fltLength
		FROM 
			OpportunityBizDocItems OBI
		INNER JOIN 
			Item AS I
		ON 
			OBI.numItemCode = I.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OBI.numWarehouseItmsID = WI.numWareHouseItemID
		WHERE
			numOppBizDocID=@numOppBizDocID
			AND I.charItemType ='P'
			AND ISNULL(I.bitContainer,0) = 1
		GROUP BY
			I.numItemCode
			,WI.numWareHouseID
			,ISNULL(I.fltWeight,0)
			,ISNULL(I.fltHeight,0)
			,ISNULL(I.fltWidth,0)
			,ISNULL(I.fltLength,0)
	) AS T2
	ON
		T1.numContainer = T2.numItemCode
		AND T1.numWareHouseID = T2.numWareHouseID

	IF (SELECT COUNT(*) FROM @TEMPContainer) = 0
	BEGIN
		RAISERROR('Container(s) are not available in bizdoc.',16,1)
	END
	ELSE
	BEGIN
		DECLARE @TEMPItems TABLE
		(
			ID INT IDENTITY(1,1)
			,numOppBizDocItemID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numWarehouseID NUMERIC(18,0)
			,numUnitHour FLOAT
			,numContainer NUMERIC(18,0)
			,numNoItemIntoContainer NUMERIC(18,0)
			,fltItemWidth FLOAT
			,fltItemHeight FLOAT
			,fltItemLength FLOAT
			,fltItemWeight FLOAT
		)

		INSERT INTO 
			@TEMPItems
		SELECT
			OBDI.numOppBizDocItemID
			,I.numItemCode
			,ISNULL(WI.numWareHouseID,0)
			,ISNULL(OBDI.numUnitHour,0)
			,ISNULL(I.numContainer,0)
			,ISNULL(I.numNoItemIntoContainer,0)
			,ISNULL(fltWidth,0)
			,ISNULL(fltHeight,0)
			,ISNULL(fltLength,0)
			,ISNULL(fltWeight,0)
		FROM
			OpportunityBizDocs OBD
		INNER JOIN
			OpportunityBizDocItems OBDI
		ON
			OBD.numOppBizDocsId=OBDI.numOppBizDocID
		INNER JOIN
			WareHouseItems WI
		ON
			OBDI.numWarehouseItmsID=WI.numWareHouseItemID
		INNER JOIN
			Item I
		ON
			OBDI.numItemCode=I.numItemCode
		WHERE
			numOppId=@numOppID
			AND numOppBizDocsId=@numOppBizDocId
			AND ISNULL(I.numContainer,0) > 0

		DECLARE @i AS INT = 1
		DEClARE @j AS INT = 1
		DECLARE @iCount as INT
		SELECT @iCount = COUNT(*) FROM @TEMPItems 

		DECLARE @numBoxID NUMERIC(18,0)
		DECLARE @numTempQty AS FLOAT
		DECLARE @numTempOppBizDocItemID AS NUMERIC(18,0)
		DECLARE @numTempContainerQty AS NUMERIC(18,0)
		DECLARE @numtempContainer AS NUMERIC(18,0)
		DECLARE @numTempItemCode AS NUMERIC(18,0)
		DECLARE @numTempWarehouseID AS NUMERIC(18,0)
		DECLARE @fltTempItemWidth AS FLOAT
		DECLARE @fltTempItemHeight AS FLOAT
		DECLARE @fltTempItemLength AS FLOAT
		DECLARE @fltTempItemWeight AS FLOAT

		IF ISNULL(@iCount,0) > 0
		BEGIN
			WHILE @i <= @iCount
			BEGIN
				SELECT 
					@numTempOppBizDocItemID=numOppBizDocItemID
					,@numtempContainer=numContainer
					,@numTempItemCode=numItemCode
					,@numTempWarehouseID=numWarehouseID
					,@numTempQty=numUnitHour
					,@numTempContainerQty=numNoItemIntoContainer
					,@fltTempItemWidth=fltItemWidth
					,@fltTempItemHeight=fltItemHeight
					,@fltTempItemLength=fltItemLength
					,@fltTempItemWeight=fltItemWeight
				FROM 
					@TEMPItems 
				WHERE 
					ID=@i

				IF EXISTS (SELECT * FROM @TEMPContainer WHERE numContainer=@numtempContainer AND numWareHouseID=@numTempWarehouseID AND numContainerQty=@numTempContainerQty AND numUsedContainer < numTotalContainer)
				BEGIN
					DECLARE @numID AS INT
					DECLARE @numTempTotalContainer NUMERIC(18,0)
					DECLARE @numTempMainContainerQty NUMERIC(18,0)
					DECLARE @bitTempItemAdded BIT
					DECLARE @fltTempWeight FLOAT
					DECLARE @fltTempHeight FLOAT
					DECLARE @fltTempWidth FLOAT
					DECLARE @fltTempLength FLOAT

					SELECT
						@numID=ID
						,@numTempTotalContainer=numTotalContainer
						,@numTempMainContainerQty=numNoItemIntoContainer
						,@bitTempItemAdded=bitItemAdded
						,@fltTempWeight=fltWeight
						,@fltTempHeight=fltHeight
						,@fltTempWidth=fltWidth
						,@fltTempLength=fltLength
					FROM
						@TEMPContainer 
					WHERE 
						numContainer=@numtempContainer 
						AND numWareHouseID=@numTempWarehouseID 
						AND numContainerQty=@numTempContainerQty

					WHILE @numTempQty > 0
					BEGIN
						 If @numTempTotalContainer > 0
						 BEGIN
							IF @numTempQty <= @numTempMainContainerQty
							BEGIN
								IF ISNULL(@bitTempItemAdded,0) = 0 OR @numTempMainContainerQty=@numTempContainerQty
								BEGIN
									-- CREATE NEW SHIPING BOX
									INSERT INTO ShippingBox
									(
										vcBoxName
										,numShippingReportId
										,fltTotalWeight
										,fltHeight
										,fltWidth
										,fltLength
										,dtCreateDate
										,numCreatedBy
										,numPackageTypeID
										,numServiceTypeID
										,fltDimensionalWeight
										,numShipCompany
									)
									VALUES
									(
										CONCAT('Box',@j)
										,@numShippingReportID
										,(CASE WHEN ISNULL(@fltTempWeight,0) = 0 THEN CEILING(CAST((CASE WHEN ISNULL(@fltTempHeight,0)=0 THEN 1 ELSE @fltTempHeight END) * (CASE WHEN ISNULL(@fltTempWidth,0)=0 THEN 1 ELSE @fltTempWidth END) * (CASE WHEN ISNULL(@fltTempLength,0)=0 THEN 1 ELSE @fltTempLength END) AS FLOAT)/166) ELSE @fltTempWeight END)
										,@fltTempHeight
										,@fltTempWidth
										,@fltTempLength
										,GETUTCDATE()
										,@numUserCntID
										,31
										,@numShippingService
										,(CASE WHEN ISNULL(@fltTempWeight,0) = 0 THEN CEILING(CAST((CASE WHEN ISNULL(@fltTempHeight,0)=0 THEN 1 ELSE @fltTempHeight END) * (CASE WHEN ISNULL(@fltTempWidth,0)=0 THEN 1 ELSE @fltTempWidth END) * (CASE WHEN ISNULL(@fltTempLength,0)=0 THEN 1 ELSE @fltTempLength END) AS FLOAT)/166) ELSE @fltTempWeight END)
										,@numShippingCompany
										
									)

									SELECT @numBoxID = SCOPE_IDENTITY()

									UPDATE @TEMPContainer SET bitItemAdded = 1 WHERE ID=@numID
									
									SET @j = @j + 1
								END
								ELSE
								BEGIN
									SELECT @numBoxID=numBoxID FROM ShippingBox WHERE numShippingReportId=@numShippingReportID AND vcBoxName=CONCAT('Box',@j)
								END

								SET @fltTempItemWeight = (CASE WHEN ISNULL(@fltTempItemWeight,0) > 0 THEN @fltTempItemWeight ELSE CEILING(CAST(@fltTempItemHeight * @fltTempItemWidth * @fltTempItemLength AS FLOAT)/166) END)

								UPDATE ShippingBox SET fltTotalWeight = ISNULL(fltTotalWeight,0) + (@fltTempItemWeight * @numTempQty) WHERE numBoxID=@numBoxID

								-- MAKE ENTRY IN ShippingReportItems Table
								INSERT INTO ShippingReportItems
								(
									numShippingReportId
									,numItemCode
									,tintServiceType
									,monShippingRate
									,fltTotalWeight
									,intNoOfBox
									,fltHeight
									,fltWidth
									,fltLength
									,dtCreateDate
									,numCreatedBy
									,numBoxID
									,numOppBizDocItemID
									,intBoxQty
								)
								VALUES
								(
									@numShippingReportID
									,@numTempItemCode
									,@numShippingService
									,0
									,@fltTempItemWeight
									,1
									,@fltTempHeight
									,@fltTempItemWidth
									,@fltTempItemLength
									,GETUTCDATE()
									,@numUserCntID
									,@numBoxID
									,@numTempOppBizDocItemID
									,@numTempQty
								)

								UPDATE @TEMPContainer SET numNoItemIntoContainer=numNoItemIntoContainer - @numTempQty WHERE ID=@numID
								SET @numTempMainContainerQty = @numTempMainContainerQty - @numTempQty

								IF @numTempMainContainerQty = 0
								BEGIN
									-- IF NO QTY LEFT TO ADD IN CONTAINER RESET CONAINER QTY AND MAKE FLAG bitItemAdded=0 SO IF numUsedContainer < numTotalContainer THEN NEW SHIPPING BOX WILL BE CREATED
									UPDATE @TEMPContainer SET numNoItemIntoContainer=numContainerQty,bitItemAdded=0,numUsedContainer = numUsedContainer + 1 WHERE ID=@numID
								END

								SET @numTempQty = 0
							END
							ELSE 
							BEGIN
								-- CREATE NEW SHIPING BOX
								INSERT INTO ShippingBox
								(
									vcBoxName
									,numShippingReportId
									,fltTotalWeight
									,fltHeight
									,fltWidth
									,fltLength
									,dtCreateDate
									,numCreatedBy
									,numPackageTypeID
									,numServiceTypeID
									,fltDimensionalWeight
									,numShipCompany
								)
								VALUES
								(
									CONCAT('Box',@j)
									,@numShippingReportID
									,(CASE WHEN ISNULL(@fltTempWeight,0) = 0 THEN CEILING(CAST((CASE WHEN ISNULL(@fltTempHeight,0)=0 THEN 1 ELSE @fltTempHeight END) * (CASE WHEN ISNULL(@fltTempWidth,0)=0 THEN 1 ELSE @fltTempWidth END) * (CASE WHEN ISNULL(@fltTempLength,0)=0 THEN 1 ELSE @fltTempLength END) AS FLOAT)/166) ELSE @fltTempWeight END)
									,@fltTempHeight
									,@fltTempWidth
									,@fltTempLength
									,GETUTCDATE()
									,@numUserCntID
									,31
									,@numShippingService
									,(CASE WHEN ISNULL(@fltTempWeight,0) = 0 THEN CEILING(CAST((CASE WHEN ISNULL(@fltTempHeight,0)=0 THEN 1 ELSE @fltTempHeight END) * (CASE WHEN ISNULL(@fltTempWidth,0)=0 THEN 1 ELSE @fltTempWidth END) * (CASE WHEN ISNULL(@fltTempLength,0)=0 THEN 1 ELSE @fltTempLength END) AS FLOAT)/166) ELSE @fltTempWeight END)
									,@numShippingCompany
										
								)

								SELECT @numBoxID = SCOPE_IDENTITY()

								UPDATE @TEMPContainer SET numNoItemIntoContainer=numContainerQty,bitItemAdded=0,numUsedContainer = numUsedContainer + 1 WHERE ID=@numID
									
								SET @j = @j + 1

								--SELECT @numBoxID=numBoxID FROM ShippingBox WHERE numShippingReportId=@numShippingReportID AND vcBoxName=CONCAT('Box',@j)

								SET @fltTempItemWeight = (CASE WHEN ISNULL(@fltTempItemWeight,0) > CEILING(CAST(@fltTempItemHeight * @fltTempItemWidth * @fltTempItemLength AS FLOAT)/139) THEN @fltTempItemWeight ELSE CEILING(CAST(@fltTempItemHeight * @fltTempItemWidth * @fltTempItemLength AS FLOAT)/139) END)

								UPDATE ShippingBox SET fltTotalWeight = ISNULL(fltTotalWeight,0) + (@fltTempItemWeight * @numTempMainContainerQty) WHERE numBoxID=@numBoxID

								-- MAKE ENTRY IN ShippingReportItems Table
								INSERT INTO ShippingReportItems
								(
									numShippingReportId
									,numItemCode
									,tintServiceType
									,monShippingRate
									,fltTotalWeight
									,intNoOfBox
									,fltHeight
									,fltWidth
									,fltLength
									,dtCreateDate
									,numCreatedBy
									,numBoxID
									,numOppBizDocItemID
									,intBoxQty
								)
								VALUES
								(
									@numShippingReportID
									,@numTempItemCode
									,@numShippingService
									,0
									,@fltTempItemWeight
									,1
									,@fltTempHeight
									,@fltTempItemWidth
									,@fltTempItemLength
									,GETUTCDATE()
									,@numUserCntID
									,@numBoxID
									,@numTempOppBizDocItemID
									,@numTempMainContainerQty
								)

								UPDATE @TEMPContainer SET numNoItemIntoContainer=@numTempContainerQty,numTotalContainer=numTotalContainer-1 WHERE ID=@numID 

								SET @numTempQty = @numTempQty - @numTempMainContainerQty
							END
						 END
						 ELSE
						 BEGIN
							RAISERROR('Sufficient container(s) are not available.',16,1)
							BREAK
						 END
					END
				END
				ELSE
				BEGIN
					RAISERROR('Sufficient container(s) require for item and warehouse are not available.',16,1)
					BREAK
				END


				SET @i = @i + 1
			END
		END
		ELSE
		BEGIN
			RAISERROR('Inventory item(s) are not available in bizdoc.',16,1)
		END
	END

	SELECT @numShippingReportID
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);

END CATCH
END
/****** Object:  StoredProcedure [dbo].[USP_ShoppingCart]    Script Date: 05/07/2009 22:09:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_shoppingcart')
DROP PROCEDURE usp_shoppingcart
GO
CREATE PROCEDURE [dbo].[USP_ShoppingCart]                            
@numDivID as numeric(18),                            
@numOppID as numeric(18)=null output ,                                              
@vcPOppName as varchar(200)='' output,  
@numContactId as numeric(18),                            
@numDomainId as numeric(18),
@vcSource VARCHAR(50)=NULL,
@numCampainID NUMERIC(9)=0,
@numCurrencyID as numeric(9),
@numBillAddressId numeric(9),
@numShipAddressId numeric(9),
@bitDiscountType BIT,
@fltDiscount FLOAT,
@numProId NUMERIC(9),
@numSiteID NUMERIC(9),
@tintOppStatus INT,
@monShipCost DECIMAL(20,5),
@tintSource AS BIGINT ,
@tintSourceType AS TINYINT ,                          
@numPaymentMethodId NUMERIC(9),
@txtComments varchar(1000),
@numPartner NUMERIC(18,0),
@txtFuturePassword VARCHAR(50) = NULL,
@intUsedShippingCompany NUMERIC(18,0)=0,
@vcPartenerContact VARCHAR(200)='',
@numShipmentMethod NUMERIC(18,0)=0,
@vcOppRefOrderNo VARCHAR(200) = '',
@bitBillingTerms as bit,
@intBillingDays as integer,
@bitInterestType as bit,
@fltInterest as float,
@numOrderPromotionID NUMERIC(18,0),
@numOrderPromotionDiscountID NUMERIC(18,0)
as                            
BEGIN TRY
BEGIN TRANSACTION          
	DECLARE @CRMType AS INTEGER               
	DECLARE @numRecOwner AS INTEGER       
	DECLARE @numPartenerContact NUMERIC(18,0)=0
	DECLARE @bitAllocateInventoryOnPickList AS BIT
	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @numRelationship NUMERIC(18,0) = 0
	DECLARE @numProfile NUMERIC(18,0) = 0
	
	SELECT 
		@numDiscountItemID=ISNULL(numDiscountServiceItemID,0) 
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainId

	SET @numPartenerContact=(SELECT TOP 1 numContactId FROM AdditionalContactsInformation WHERE numDomainId=@numDomainID AND numDivisionId=@numPartner AND vcEmail=@vcPartenerContact)
	
	SELECT 
		@CRMType=tintCRMType
		,@numRecOwner=numRecOwner 
		,@bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0)
		,@numRelationship=ISNULL(numCompanyType,0)
		,@numProfile=ISNULL(vcProfile,0) 
	FROM 
		DivisionMaster
	INNER JOIN 
		CompanyInfo 
	ON 
		DivisionMaster.numCompanyID=CompanyInfo.numCompanyId
	WHERE 
		numDivisionID=@numDivID

	IF ISNULL(@numOrderPromotionID,0) > 0
	BEGIN
		DECLARE @numPromotionID NUMERIC(18,0)
		DECLARE @bitPromotionNeverExpires BIT
		DECLARE @bitRequireCouponCode BIT
		DECLARE @dtPromotionStartDate DATETIME
		DECLARE @dtPromotionEndDate DATETIME
		DECLARE @intUsageLimit INT
		DECLARE @bitPromotionExipred BIT

		-- FIRST CHECK WHETHER COUNPON CODE IS AVAILABLE
		SELECT 
			@numPromotionID=PO.numProId
			,@bitPromotionNeverExpires=ISNULL(bitNeverExpires,0)
			,@dtPromotionStartDate=PO.dtValidFrom
			,@dtPromotionEndDate=PO.dtValidTo
			,@bitRequireCouponCode=ISNULL(bitRequireCouponCode,0)
		FROM 
			PromotionOffer PO
		INNER JOIN 
			PromotionOfferOrganizations PORG
		ON 
			PO.numProId = PORG.numProId
		WHERE
			PO.numDomainId = @numDomainID
			AND numRelationship=@numRelationship 
			AND numProfile=@numProfile
			AND PO.numProId = @numOrderPromotionID

		IF ISNULL(@numPromotionID,0) > 0
		BEGIN
			-- NOW CHECK IF PROMOTION EXPIRES OR NOT
			IF ISNULL(@bitPromotionNeverExpires,0) = 0 AND (@dtPromotionStartDate > GETUTCDATE() AND @dtPromotionEndDate < GETUTCDATE())
			BEGIN
				SET @bitPromotionExipred = 1

				IF ISNULL(@bitRequireCouponCode,0) = 1 AND ISNULL(@numOrderPromotionDiscountID,0) > 0
				BEGIN
					RAISERROR('COUPON_CODE_EXPIRED',16,1)
					RETURN
				END
				ELSE
				BEGIN
					DELETE FROM CartItems WHERE numUserCntId =@numContactId AND numItemCode=@numDiscountItemID
				END
			END
			ELSE
			BEGIN
				SET @bitPromotionExipred = 0
			END

			IF ISNULL(@bitPromotionExipred,0) = 0
			BEGIN
				IF ISNULL(@bitRequireCouponCode,0) = 1 AND ISNULL(@numOrderPromotionDiscountID,0) > 0
				BEGIN
					IF ISNULL(@intUsageLimit,0) <> 0
					BEGIN
						IF ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE numDiscountId=@numOrderPromotionDiscountID AND numDivisionId=@numDivID),0) >= @intUsageLimit
						BEGIN
							RAISERROR('COUPON_USAGE_LIMIT_EXCEEDED',16,1)
							RETURN
						END
						ELSE
						BEGIN
							IF NOT EXISTS (SELECT numDiscountCodeUsageId FROM DiscountCodeUsage WHERE numDiscountId=@numOrderPromotionDiscountID AND numDivisionId=@numDivID)
							BEGIN
								INSERT INTO DiscountCodeUsage (numDiscountId,numDivisionId,intCodeUsed) VALUES (@numOrderPromotionDiscountID,@numDivID,1)
							END
							ELSE
							BEGIN
								UPDATE DiscountCodeUsage SET intCodeUsed=ISNULL(intCodeUsed,0) + 1 WHERE numDiscountId=@numOrderPromotionDiscountID AND numDivisionId=@numDivID
							END
						END
					END
					ELSE
					BEGIN
						IF NOT EXISTS (SELECT numDiscountCodeUsageId FROM DiscountCodeUsage WHERE numDiscountId=@numOrderPromotionDiscountID AND numDivisionId=@numDivID)
						BEGIN
							INSERT INTO DiscountCodeUsage (numDiscountId,numDivisionId,intCodeUsed) VALUES (@numOrderPromotionDiscountID,@numDivID,1)
						END
						ELSE
						BEGIN
							UPDATE DiscountCodeUsage SET intCodeUsed=ISNULL(intCodeUsed,0) + 1 WHERE numDiscountId=@numOrderPromotionDiscountID AND numDivisionId=@numDivID
						END
					END
				END
				ELSE
				BEGIN
					-- CLEAR DISOCUNT ITEM IF DISCOUNT ID PARAMETER ID NOT ADDED
					DELETE FROM CartItems WHERE numUserCntId =@numContactId AND numItemCode=@numDiscountItemID
				END
			END
		END
		ELSE
		BEGIN
			IF ISNULL(@bitRequireCouponCode,0) = 1 AND ISNULL(@numOrderPromotionDiscountID,0) > 0
			BEGIN
				RAISERROR('INVALID_COUPON_CODE',16,1)
				RETURN
			END
			ELSE
			BEGIN
				DELETE FROM CartItems WHERE numUserCntId =@numContactId AND numItemCode=@numDiscountItemID
			END
		END
	END
	ELSE
	BEGIN
		-- IF PROMOTION ID NOT AVAILABLE THAN REMOVE ORDER PROMOTION DISCOUNT ITEM
		DELETE FROM CartItems WHERE numUserCntId =@numContactId AND numItemCode=@numDiscountItemID
	END                        
		
	IF @CRMType= 0                             
	BEGIN
		UPDATE 
			DivisionMaster                               
		SET 
			tintCRMType=1                              
		WHERE 
			numDivisionID=@numDivID
	END                            

	DECLARE @TotalAmount AS FLOAT
       

	IF LEN(ISNULL(@txtFuturePassword,'')) > 0
	BEGIN
		DECLARE @numExtranetID NUMERIC(18,0)

		SELECT @numExtranetID=numExtranetID FROM ExtarnetAccounts WHERE numDivisionID=@numDivID

		UPDATE ExtranetAccountsDtl SET vcPassword=@txtFuturePassword WHERE numExtranetID=@numExtranetID
	END                      

	DECLARE @numTemplateID NUMERIC
	SELECT 
		@numTemplateID= numMirrorBizDocTemplateId 
	FROM 
		eCommercePaymentConfig 
	WHERE 
		numSiteId=@numSiteID 
		AND numPaymentMethodId = @numPaymentMethodId 
		AND numDomainID = @numDomainID   

	DECLARE @fltExchangeRate FLOAT                                 
    
	IF @numCurrencyID=0 
	BEGIN
		SELECT 
			@numCurrencyID=ISNULL(numCurrencyID,0) 
		FROM 
			Domain 
		WHERE 
			numDomainID=@numDomainId                            
	END
		
	IF @numCurrencyID=0 
	BEGIN
		SET @fltExchangeRate=1
	END
	ELSE 
	BEGIN
		SET @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)                          
	END                      
                           
	DECLARE @numAccountClass AS NUMERIC(18) = 0
	DECLARE @tintDefaultClassType AS INT = 0
	DECLARE @tintCommitAllocation TINYINT

	SELECT 
		@tintDefaultClassType = ISNULL(tintDefaultClassType,0) 
		,@tintCommitAllocation=ISNULL(tintCommitAllocation,1)
	FROM 
		Domain 
	WHERE 
		numDomainId = @numDomainID 

	IF @tintDefaultClassType <> 0
	BEGIN
		SELECT 
			@numAccountClass=ISNULL(numDefaultClass,0) 
		FROM 
			dbo.UserMaster UM 
		JOIN 
			dbo.Domain D 
		ON 
			UM.numDomainID=D.numDomainId
		WHERE 
			D.numDomainId=@numDomainId 
			AND UM.numUserDetailId=@numContactId
	END
	ELSE IF @tintDefaultClassType = 2 --COMPANY
	BEGIN
		SELECT 
			@numAccountClass=ISNULL(numAccountClassID,0) 
		FROM 
			dbo.DivisionMaster DM 
		WHERE 
			DM.numDomainId=@numDomainId 
			AND DM.numDivisionID=@numDivID
	END
	ELSE
	BEGIN
		SET @numAccountClass = 0
	END

	SET @numAccountClass=(SELECT TOP 1 numDefaultClass FROM dbo.eCommerceDTL WHERE numSiteId=@numSiteID AND numDomainId=@numDomainId)                  

	INSERT INTO OpportunityMaster
	(                              
		numContactId,numDivisionId,txtComments,numCampainID,bitPublicFlag,tintSource,tintSourceType,vcPOppName,monPAmount,                              
		numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,tintOppType,tintOppStatus,intPEstimatedCloseDate,              
		numRecOwner,bitOrder,numCurrencyID,fltExchangeRate,fltDiscountTotal,bitDiscountTypeTotal,monShipCost,numOppBizDocTempID
		,numAccountClass,numPartner,intUsedShippingCompany,numPartenerContact,numShippingService,dtReleaseDate,vcOppRefOrderNo,
		bitBillingTerms,intBillingDays,bitInterestType,fltInterest,numDiscountID
	)                              
	VALUES                              
	(                              
		@numContactId,@numDivID,@txtComments,@numCampainID,0,@tintSource,@tintSourceType,ISNULL(@vcPOppName,'SO'),0,                                
		@numContactId,GETUTCDATE(),@numContactId,GETUTCDATE(),@numDomainId,1,@tintOppStatus,GETUTCDATE(),@numRecOwner,
		1,@numCurrencyID,@fltExchangeRate,@fltDiscount,@bitDiscountType,@monShipCost,@numTemplateID,@numAccountClass,@numPartner
		,@intUsedShippingCompany,@numPartenerContact,@numShipmentMethod,GETDATE(),@vcOppRefOrderNo,@bitBillingTerms,@intBillingDays,
		@bitInterestType,@fltInterest,@numOrderPromotionDiscountID
	)
		
	SET @numOppID=scope_identity()                             

	EXEC dbo.USP_UpdateNameTemplateValue 1,@numDomainId,@numOppID
	SELECT @vcPOppName = vcPOppName FROM OpportunityMaster WHERE numOppID= @numOppID

	INSERT INTO OpportunityItems
	(
		numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,vcAttrValues,[vcItemName],
		[vcModelID],[vcManufacturer],vcPathForTImage, monVendorCost,numUOMId,bitDiscountType,fltDiscount,monTotAmtBefDiscount,numPromotionID,
		bitPromotionTriggered,bitDropShip,vcPromotionDetail,vcChildKitSelectedItems
	)                                                                                                              
	SELECT 
		@numOppID,X.numItemCode,x.numUnitHour * x.decUOMConversionFactor,x.monPrice/x.decUOMConversionFactor,x.monTotAmount,X.vcItemDesc,
		NULLIF(X.numWarehouseItmsID,0),X.vcItemType,X.vcAttributes,X.vcAttrValues,(SELECT vcItemName FROM item WHERE numItemCode = X.numItemCode),
		(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
		(SELECT TOP 1  vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND bitDefault = 1 AND numDomainId = @numDomainId),
		(SELECT ISNULL(VN.monCost,0) FROM Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode and VN.numVendorID=IT.numVendorID WHERE VN.numItemCode=X.numItemCode), 
		numUOM,bitDiscountType,fltDiscount,monTotAmtBefDiscount,X.PromotionID,X.bitParentPromotion
		,ISNULL((SELECT bitAllowDropShip FROM Item WHERE numItemCode=X.numItemCode),0)
		,(CASE 
			WHEN ISNULL(X.PromotionID,0) > 0 
			THEN 
				ISNULL((SELECT CONCAT('Buy '
											,CASE 
												WHEN tintOfferTriggerValueType=1 
												THEN CAST(fltOfferTriggerValue AS VARCHAR) 
												ELSE CONCAT('$',fltOfferTriggerValue) 
											END
											,CASE tintOfferBasedOn
													WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
													WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
												END
											,' & get '
											, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
											, CASE 
												WHEN tintDiscoutBaseOn = 1 THEN 
													CONCAT
													(
														CONCAT(' "',(SELECT Stuff((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, '')), '"')
														,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
														,'.'
													)
												WHEN tintDiscoutBaseOn = 2 THEN 
													CONCAT
													(
														CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(',',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, '')), '"')
														,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
														,'.'
													)
												WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
												ELSE '' 
											END
										) FROM PromotionOffer PO WHERE numProId=X.PromotionID AND numDomainId=@numDomainID),'') 
			ELSE '' 
		END)
		,ISNULL(vcChildKitItemSelection,'')
	FROM 
		dbo.CartItems X 
	WHERE 
		numUserCntId =@numContactId


	--INSERT KIT ITEMS 
	IF (SELECT 
			COUNT(*) 
		FROM 
			OpportunityItems OI 
		LEFT JOIN
			WarehouseItems WI
		ON
			OI.numWarehouseItmsID = WI.numWarehouseItemID
		LEFT JOIN
			Warehouses W
		ON
			WI.numWarehouseID = W.numWarehouseID
		JOIN 
			ItemDetails ID 
		ON 
			OI.numItemCode=ID.numItemKitID 
		JOIN
			Item I
		ON
			ID.numChildItemID = I.numItemCode
		WHERE 
			OI.numOppId=@numOppId  
			AND ISNULL(OI.bitDropShip,0) = 0
			AND ISNULL(OI.numWarehouseItmsID,0) > 0
			AND I.charItemType = 'P'
			AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
	BEGIN
		RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
	END

			                
	INSERT INTO OpportunityKitItems
	(
		numOppId,
		numOppItemID,
		numChildItemID,
		numWareHouseItemId,
		numQtyItemsReq,
		numQtyItemsReq_Orig,
		numUOMId,
		numQtyShipped,
		monAvgCost
	)
	SELECT
		@numOppId,
		OI.numoppitemtCode,
		ID.numChildItemID,
		(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
		(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
		ID.numQtyItemsReq,
		ID.numUOMId,
		0,
		ISNULL(I.monAverageCost,0)
	FROM 
		OpportunityItems OI 
	LEFT JOIN
		WarehouseItems WI
	ON
		OI.numWarehouseItmsID = WI.numWarehouseItemID
	LEFT JOIN
		Warehouses W
	ON
		WI.numWarehouseID = W.numWarehouseID
	JOIN 
		ItemDetails ID 
	ON 
		OI.numItemCode=ID.numItemKitID 
	JOIN
		Item I
	ON
		ID.numChildItemID = I.numItemCode
	WHERE 
		OI.numOppId=@numOppId 

	--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
	DECLARE @TempKitConfiguration TABLE
	(
		numOppItemID NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		KitChildItems VARCHAR(MAX)
	)

	INSERT INTO 
		@TempKitConfiguration
	SELECT
		numoppitemtCode,
		numItemCode,
		vcChildKitSelectedItems
	FROM
		OpportunityItems
	WHERE
		numOppId = @numOppID
		AND LEN(ISNULL(vcChildKitSelectedItems,'')) > 0
					

	IF (SELECT 
			COUNT(*) 
		FROM 
		(
			SELECT 
				t1.numOppItemID
				,t1.numItemCode
				,SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID
				,SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
			FROM 
				@TempKitConfiguration t1
			CROSS APPLY
			(
				SELECT
					*
				FROM 
					dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=t1.numOppItemID AND t3.numItemCode=t1.numItemCode),',') 
			) as t2
		) TempChildItems
		INNER JOIN
			OpportunityKitItems OKI
		ON
			OKI.numOppId=@numOppId
			AND OKI.numOppItemID=TempChildItems.numOppItemID
			AND OKI.numChildItemID=TempChildItems.numKitItemID
		INNER JOIN
			OpportunityItems OI
		ON
			OI.numItemCode = TempChildItems.numItemCode
			AND OI.numoppitemtcode = OKI.numOppItemID
		LEFT JOIN
			WarehouseItems WI
		ON
			OI.numWarehouseItmsID = WI.numWarehouseItemID
		LEFT JOIN
			Warehouses W
		ON
			WI.numWarehouseID = W.numWarehouseID
		INNER JOIN
			ItemDetails
		ON
			TempChildItems.numKitItemID = ItemDetails.numItemKitID
			AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
		INNER JOIN
			Item 
		ON
			ItemDetails.numChildItemID = Item.numItemCode
		WHERE
			Item.charItemType = 'P'
			AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID),0) = 0) > 0
	BEGIN
		RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
	END

	INSERT INTO OpportunityKitChildItems
	(
		numOppID,
		numOppItemID,
		numOppChildItemID,
		numItemID,
		numWareHouseItemId,
		numQtyItemsReq,
		numQtyItemsReq_Orig,
		numUOMId,
		numQtyShipped,
		monAvgCost
	)
	SELECT 
		@numOppID,
		OKI.numOppItemID,
		OKI.numOppChildItemID,
		ItemDetails.numChildItemID,
		(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
		(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
		ItemDetails.numQtyItemsReq,
		ItemDetails.numUOMId,
		0,
		ISNULL(Item.monAverageCost,0)
	FROM
	(
		SELECT 
			t1.numOppItemID
			,t1.numItemCode
			,SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID
			,SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
		FROM 
			@TempKitConfiguration t1
		CROSS APPLY
		(
			SELECT
				*
			FROM 
				dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=t1.numOppItemID AND t3.numItemCode=t1.numItemCode),',') 
		) as t2
	) TempChildItems
	INNER JOIN
		OpportunityKitItems OKI
	ON
		OKI.numOppId=@numOppId
		AND OKI.numOppItemID=TempChildItems.numOppItemID
		AND OKI.numChildItemID=TempChildItems.numKitItemID
	INNER JOIN
		OpportunityItems OI
	ON
		OI.numItemCode = TempChildItems.numItemCode
		AND OI.numoppitemtcode = OKI.numOppItemID
	LEFT JOIN
		WarehouseItems WI
	ON
		OI.numWarehouseItmsID = WI.numWarehouseItemID
	LEFT JOIN
		Warehouses W
	ON
		WI.numWarehouseID = W.numWarehouseID
	INNER JOIN
		ItemDetails
	ON
		TempChildItems.numKitItemID = ItemDetails.numItemKitID
		AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
	INNER JOIN
		Item 
	ON
		ItemDetails.numChildItemID = Item.numItemCode

	IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
	BEGIN
		EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,'',@numContactId
	END


	BEGIN TRY
		DECLARE @TEMPContainer TABLE
		(
			numConainer NUMERIC(18,0)
			,numNoItemIntoContainer NUMERIC(18,0)
			,numWarehouseID NUMERIC(18,0)
			,monPrice DECIMAL(20,5)
			,monAverageCost DECIMAL(20,5)
			,numQtyToFit FLOAT
			,vcItemName VARCHAR(300)
			,vcItemDesc VARCHAR(1000)
			,vcModelID VARCHAR(300)
		)

		INSERT INTO @TEMPContainer
		(
			numConainer
			,numNoItemIntoContainer
			,numWarehouseID
			,monPrice
			,monAverageCost
			,numQtyToFit
			,vcItemName
			,vcItemDesc
			,vcModelID
		)
		SELECT
			I.numContainer
			,I.numNoItemIntoContainer
			,WI.numWarehouseID
			,ISNULL(IContainer.monListPrice,0)
			,ISNULL(IContainer.monAverageCost,0)
			,SUM(numUnitHour)
			,ISNULL(IContainer.vcItemName,'')
			,ISNULL(IContainer.txtItemDesc,'')
			,ISNULL(IContainer.vcModelID,'')
		FROM
			OpportunityItems OI
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		INNER JOIN
			Item IContainer
		ON
			I.numContainer = IContainer.numItemCode
		LEFT JOIN
			WareHouseItems WI
		ON
			OI.numWarehouseItmsID = WI.numWareHouseItemID
		WHERE
			numOppID = @numOppID
			AND ISNULL(OI.bitDropShip,0) = 0
			AND ISNULL(I.bitContainer,0) = 0
			AND ISNULL(IContainer.bitContainer,0) = 1
			AND ISNULL(I.numContainer,0) > 0
			AND ISNULL(I.numNoItemIntoContainer,0) > 0
		GROUP BY
			I.numContainer
			,I.numNoItemIntoContainer
			,IContainer.monListPrice
			,IContainer.monAverageCost
			,WI.numWarehouseID
			,IContainer.vcItemName
			,IContainer.txtItemDesc
			,IContainer.vcModelID

		INSERT INTO OpportunityItems
		(
			numOppId
			,numItemCode
			,numWarehouseItmsID
			,numUnitHour
			,monPrice
			,fltDiscount
			,bitDiscountType
			,monTotAmount
			,monTotAmtBefDiscount
			,monAvgCost
			,vcItemName
			,vcItemDesc
			,vcModelID
		)
		SELECT
			@numOppID
			,numConainer
			,(SELECT TOP 1 numWarehouseItemID FROM WarehouseItems WHERE numItemID=numConainer AND WarehouseItems.numWarehouseID=T1.numWarehouseID ORDER BY numWareHouseItemID)
			,(CASE WHEN numQtyToFit > numNoItemIntoContainer THEN CEILING(numQtyToFit/numNoItemIntoContainer) ELSE 1 END)
			,monPrice
			,0
			,1
			,(CASE WHEN numQtyToFit > numNoItemIntoContainer THEN CEILING(numQtyToFit/numNoItemIntoContainer) ELSE 1 END) * monPrice
			,(CASE WHEN numQtyToFit > numNoItemIntoContainer THEN CEILING(numQtyToFit/numNoItemIntoContainer) ELSE 1 END) * monPrice
			,monAverageCost
			,vcItemName
			,vcItemDesc
			,vcModelID
		FROM
			@TEMPContainer T1

		DECLARE @tintShipped AS TINYINT      
		DECLARE @tintOppType AS TINYINT
	
		SELECT 
			@tintOppStatus=tintOppStatus
			,@tintOppType=tintOppType
			,@tintShipped=tintShipped 
		FROM 
			OpportunityMaster 
		WHERE 
			numOppID=@numOppID
	END TRY
	BEGIN CATCH
		-- DO NOT RAISE ERROR
	END CATCH           
	
	IF @tintOppStatus=1               
	BEGIN         
		EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numContactId
	END                                   
                  
	SELECT
		@TotalAmount=SUM(monTotAmount)
	FROM 
		OpportunityItems 
	WHERE 
		numOppId=@numOppID                            
 
	UPDATE 
		OpportunityMaster 
	SET 
		monPamount = @TotalAmount - @fltDiscount                           
	WHERE 
		numOppId=@numOppID                           
                                               
	IF(@vcSource IS NOT NULL)
	BEGIN
		insert into OpportunityLinking ([numParentOppID],[numChildOppID],[vcSource],[numParentOppBizDocID],numPromotionId,numSiteID)  values(null,@numOppID,@vcSource,NULL,@numProId,@numSiteID);
	END

	--Add/Update Address Details
	DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50), @vcAltContact VARCHAR(200)     
	DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9), @numContact NUMERIC(18,0)
	DECLARE @bitIsPrimary BIT, @bitAltContact BIT;

	--Bill Address
	IF @numBillAddressId>0
	BEGIN
		SELECT
			@vcStreet=ISNULL(vcStreet,'')
			,@vcCity=ISNULL(vcCity,'')
			,@vcPostalCode=ISNULL(vcPostalCode,'')
			,@numState=ISNULL(numState,0)
			,@numCountry=ISNULL(numCountry,0)
			,@bitIsPrimary=bitIsPrimary
			,@vcAddressName=vcAddressName
			,@numContact=numContact
			,@bitAltContact=bitAltContact
			,@vcAltContact=vcAltContact
		FROM 
			dbo.AddressDetails 
		WHERE 
			numDomainID = @numDomainID 
			AND numAddressID = @numBillAddressId 
            
  
  		EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numOppID, --  numeric(9, 0)
		@byteMode = 0, --  tinyint
		@vcStreet = @vcStreet, --  varchar(100)
		@vcCity = @vcCity, --  varchar(50)
		@vcPostalCode = @vcPostalCode, --  varchar(15)
		@numState = @numState, --  numeric(9, 0)
		@numCountry = @numCountry, --  numeric(9, 0)
		@vcCompanyName = '', --  varchar(100)
		@numCompanyId = 0, --  numeric(9, 0)
		@vcAddressName =@vcAddressName,
		@bitCalledFromProcedure=1,
		@numContact = @numContact,
		@bitAltContact = @bitAltContact,
		@vcAltContact = @vcAltContact
        
	END
  
  
	--Ship Address
	IF @numShipAddressId>0
	BEGIN
   		SELECT 
			@vcStreet=ISNULL(vcStreet,'')
			,@vcCity=ISNULL(vcCity,'')
			,@vcPostalCode=ISNULL(vcPostalCode,'')
			,@numState=ISNULL(numState,0)
			,@numCountry=ISNULL(numCountry,0)
			,@bitIsPrimary=bitIsPrimary
			,@vcAddressName=vcAddressName
			,@numContact=numContact
			,@bitAltContact=bitAltContact
			,@vcAltContact=vcAltContact
		FROM 
			dbo.AddressDetails 
		WHERE 
			numDomainID = @numDomainID 
			AND numAddressID = @numShipAddressId 
  
		select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
		join divisionMaster Div                            
		on div.numCompanyID=com.numCompanyID                            
		where div.numdivisionID=@numDivID

  		EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numOppID, --  numeric(9, 0)
		@byteMode = 1, --  tinyint
		@vcStreet = @vcStreet, --  varchar(100)
		@vcCity = @vcCity, --  varchar(50)
		@vcPostalCode = @vcPostalCode, --  varchar(15)
		@numState = @numState, --  numeric(9, 0)
		@numCountry = @numCountry, --  numeric(9, 0)
		@vcCompanyName = @vcCompanyName, --  varchar(100)
		@numCompanyId =@numCompanyId, --  numeric(9, 0)
		@vcAddressName =@vcAddressName,
		@bitCalledFromProcedure=1,
		@numContact = @numContact,
		@bitAltContact = @bitAltContact,
		@vcAltContact = @vcAltContact
   	
	END

	-- IMPORTANT: KEEP THIS BELOW ADDRESS UPDATE LOGIC
	--Insert Tax for Division                       
	INSERT dbo.OpportunityMasterTaxItems 
	(
		numOppId,
		numTaxItemID,
		fltPercentage,
		tintTaxType,
		numTaxID
	) 
	SELECT 
		@numOppID,
		TI.numTaxItemID,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		0
	FROM 
		TaxItems TI 
	JOIN 
		DivisionTaxTypes DTT 
	ON 
		TI.numTaxItemID = DTT.numTaxItemID
	CROSS APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType,
			numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,TI.numTaxItemID,@numOppId,0,NULL)
	) AS TEMPTax
	WHERE 
		DTT.numDivisionID=@numDivID AND DTT.bitApplicable=1
	UNION 
	SELECT 
		@numOppID,
		0,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		0
	FROM 
		dbo.DivisionMaster 
	CROSS APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType,
			numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,0,@numOppId,1,NULL)
	) AS TEMPTax
	WHERE 
		bitNoTax=0 
		AND numDivisionID=@numDivID
	UNION 
	SELECT
		@numOppId
		,1
		,decTaxValue
		,tintTaxType
		,numTaxID
	FROM
		dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,1,@numOppId,1,NULL)
  
	--Delete Tax for Opportunity Items if item deleted 
	DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

	--Insert Tax for Opportunity Items
	INSERT INTO dbo.OpportunityItemsTaxItems 
	(
		numOppId,
		numOppItemID,
		numTaxItemID,
		numTaxID
	) 
	SELECT 
		@numOppId,
		OI.numoppitemtCode,
		TI.numTaxItemID,
		0 
	FROM 
		dbo.OpportunityItems OI 
	JOIN 
		dbo.ItemTax IT 
	ON 
		OI.numItemCode=IT.numItemCode 
	JOIN
		TaxItems TI 
	ON 
		TI.numTaxItemID=IT.numTaxItemID 
	WHERE 
		OI.numOppId=@numOppID 
		AND IT.bitApplicable=1 
		AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
	UNION
	SELECT 
		@numOppId,
		OI.numoppitemtCode,
		0,
		0 
	FROM 
		dbo.OpportunityItems OI 
	JOIN 
		dbo.Item I 
	ON 
		OI.numItemCode=I.numItemCode
	WHERE 
		OI.numOppId=@numOppID 
		AND I.bitTaxable=1 
		AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
	UNION
	SELECT
		@numOppId,
		OI.numoppitemtCode,
		1,
		TD.numTaxID
	FROM
		dbo.OpportunityItems OI 
	INNER JOIN
		ItemTax IT
	ON
		IT.numItemCode = OI.numItemCode
	INNER JOIN
		TaxDetails TD
	ON
		TD.numTaxID = IT.numTaxID
		AND TD.numDomainId = @numDomainId
	WHERE
		OI.numOppId = @numOppID
		AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TaxReports')
DROP PROCEDURE USP_TaxReports
GO
CREATE PROCEDURE [dbo].[USP_TaxReports]     
    @numDomainID numeric(18, 0),
    @dtFromDate DATETIME,
    @dtToDate DATETIME
AS                 
	SELECT 
		T.numOppBizDocsId,
		T.numOppId,
		vcBizDocID,
		TI.vcTaxName,
		T.fltPercentage,
		T.tintTaxType,
		SUM(Case When T.tintOppType=1 then TaxAmount else 0 end) SalesTax,
		SUM(Case When T.tintOppType=2 then TaxAmount else 0 end) PurchaseTax,
		T.INVCreatedDate,
		T.Organization,
		T.vcShipState,
		T.GrandTotal
	FROM 
	(
		SELECT 
			OBD.numOppBizDocsId,
		    OBD.vcBizDocID,
			OMTI.*,
			OM.tintOppType,
			ISNULL(dbo.fn_CalOppItemTotalTaxAmt(OM.numDomainId,OMTI.numTaxItemID,OM.numOppId,OBD.numOppBizDocsId),0) AS TaxAmount,
			[dbo].[FormatedDateFromDate](OBD.dtCreatedDate,CONVERT(VARCHAR(10), @numDomainId)) AS INVCreatedDate,
			CI.vcCompanyName AS Organization,
			ISNULL(dbo.fn_GetState(AD2.numState),'') AS vcShipState,			
			OBD.monDealAmount AS GrandTotal
			
		FROM OpportunityMaster OM 
		JOIN OpportunityBizDocs OBD 
		ON OM.numOppId=OBD.numOppId
		JOIN OpportunityMasterTaxItems OMTI 
		ON OMTI.numOppId=OBD.numOppId
		LEFT JOIN DivisionMaster DM
		ON OM.numDivisionId = DM.numDivisionID
		LEFT JOIN CompanyInfo CI
		ON DM.numCompanyID = CI.numCompanyId
		LEFT JOIN AddressDetails AD2--Shipping address
		ON AD2.numDomainID=OM.numDomainID AND AD2.numRecordID=DM.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary=1		
		WHERE 
			OM.numDomainID=@numDomainID 
			AND OBD.bitAuthoritativeBizDocs=1 
			AND OMTI.fltPercentage>0
			AND OBD.dtFromDate BETWEEN  @dtFromDate AND @dtToDate
	) T 
	JOIN 
	(
		SELECT 
			numTaxItemId,
			vcTaxName 
		FROM 
			TaxItems 
		WHERE 
			numDomainID=@numDomainID 
		UNION 
		SELECT 
			0,
			'Sales Tax'
		UNION 
		SELECT 
			1,
			'CRV'
	) TI 
	ON T.numTaxItemId=TI.numTaxItemID 
	WHERE 
		TaxAmount > 0
	GROUP BY 
		T.vcBizDocID,
		TI.vcTaxName,
		T.fltPercentage,
		T.tintTaxType,
		T.INVCreatedDate,
		T.Organization,
		T.vcShipState,
		T.GrandTotal,
		T.numOppBizDocsId,
		T.numOppId

GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateStagePercentageDetails]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Gangadhar    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatestagepercentagedetails')
DROP PROCEDURE usp_updatestagepercentagedetails
GO
CREATE PROCEDURE [dbo].[usp_UpdateStagePercentageDetails]
    @numStagePercentageId NUMERIC,
    @tintConfiguration TINYINT,
    @vcStageName VARCHAR(1000),
    @slpid NUMERIC,
    @numDomainId NUMERIC,
    @numUserCntID NUMERIC,
    @numAssignTo NUMERIC = 0,
    @vcMileStoneName VARCHAR(1000),
    @tintPercentage AS TINYINT,
    @vcDescription VARCHAR(2000),
    @numStageDetailsIds AS NUMERIC(9),
    @bitClose AS BIT,
    @numParentStageID NUMERIC,
    @intDueDays int,
	@dtStartDate as DATETIME,
	@dtEndDate as DATETIME,
	@bitTimeBudget as bit,
	@bitExpenseBudget as bit,
	@monTimeBudget as DECIMAL(20,5),
	@monExpenseBudget as DECIMAL(20,5),
	@numSetPrevStageProgress AS NUMERIC = 0,
	@bitIsDueDaysUsed AS BIT,
	@numTeamId AS NUMERIC,
	@bitRunningDynamicMode AS BIT
AS 

IF @numSetPrevStageProgress > 0
BEGIN
	UPDATE [StagePercentageDetails] 
		SET tinProgressPercentage = 100
		WHERE   numdomainid = @numDomainid
                AND numStageDetailsId < @numSetPrevStageProgress
                AND slp_id = @slpid
				AND tinProgressPercentage < 100

END


UPDATE  [StagePercentageDetails]
SET     [numStagePercentageId] = @numStagePercentageId,
        [tintConfiguration] = @tintConfiguration,
        [vcStageName] = @vcStageName,
        [numModifiedBy] = @numUserCntID,
        [bintModifiedDate] = GETUTCDATE(),
        [numAssignTo] = (case @numAssignTo when -1 then numAssignTo else @numAssignTo end),
		[tintPercentage] = (case @numAssignTo when -1 then tintPercentage else @tintPercentage end),
		[tinProgressPercentage] = (case @numAssignTo when -1 then @tintPercentage else tinProgressPercentage end), 
        [vcMileStoneName] = @vcMileStoneName,
        [vcDescription] = @vcDescription,
        bitClose = @bitClose,
        numParentStageID = @numParentStageID,
        intDueDays = @intDueDays,
		dtStartDate = @dtStartDate,
		dtEndDate = @dtEndDate,bitTimeBudget=@bitTimeBudget,bitExpenseBudget=@bitExpenseBudget,
		monTimeBudget=@monTimeBudget,monExpenseBudget=@monExpenseBudget,bitIsDueDaysUsed=@bitIsDueDaysUsed,
		numTeamId=@numTeamId,bitRunningDynamicMode=@bitRunningDynamicMode
WHERE   numStageDetailsId = @numStageDetailsIds
        AND slp_id = @slpid
        AND numDomainId = @numDomainId

if @bitClose=1
BEGIN
 update  [StagePercentageDetails] set bitClose = @bitClose,tinProgressPercentage=@tintPercentage where numParentStageID  = @numStageDetailsIds
        AND slp_id = @slpid
        AND numDomainId = @numDomainId
END 


if @numParentStageID>0
BEGIN
Declare @TotalR  numeric,@TotalP  numeric

  select @TotalR = count(*) from StagePercentageDetails where numParentStageID=@numParentStageID AND slp_id = @slpid AND numDomainId = @numDomainId
  select @TotalP = sum(tinProgressPercentage) from StagePercentageDetails where numParentStageID=@numParentStageID AND slp_id = @slpid AND numDomainId = @numDomainId
	
  if @TotalP = @TotalR*100
	Update StagePercentageDetails set tinProgressPercentage=100,bitclose=1 where numStageDetailsId=@numParentStageID AND slp_id = @slpid AND numDomainId = @numDomainId
END


DECLARE @numProjectID NUMERIC
DECLARE @numOppID NUMERIC
SELECT @numProjectID=numProjectID,@numOppID=numOppID FROM dbo.StagePercentageDetails WHERE [numStageDetailsId]=@numStageDetailsIds


--Update total progress
IF @numProjectID>0
	EXEC dbo.USP_GetProjectTotalProgress
	@numDomainID = @numDomainID, --  numeric(9, 0)
	@numProId = @numProjectID, --  numeric(9, 0)
	@tintMode = 1 --  tinyint

IF @numOppID>0
	EXEC dbo.USP_GetProjectTotalProgress
	@numDomainID = @numDomainID, --  numeric(9, 0)
	@numProId = @numOppID, --  numeric(9, 0)
	@tintMode = 0 --  tinyint
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdatingInventoryForEmbeddedKits')
DROP PROCEDURE USP_UpdatingInventoryForEmbeddedKits
GO
CREATE PROCEDURE [dbo].[USP_UpdatingInventoryForEmbeddedKits]    
	@numOppItemID AS NUMERIC(18,0),
	@Units AS FLOAT,
	@Mode AS TINYINT,
	@numOppID AS NUMERIC(9),
	@tintMode AS TINYINT=0, -- 0:Add/Edit,1:Delete,2: DEMOTED TO OPPORTUNITY
	@numUserCntID AS NUMERIC(9)
AS                            
BEGIN

	DECLARE @numDomain AS NUMERIC(18,0)
	DECLARE @tintOppType AS TINYINT = 0

	SELECT 
		@numDomain = numDomainID, 
		@tintOppType=tintOppType 
	FROM 
		OpportunityMaster
	WHERE 
		numOppId = @numOppID

	SELECT 
		OKI.numOppChildItemID,
		I.numItemCode,
		I.bitAssembly,
		numWareHouseItemId,
		numQtyItemsReq,
		numQtyShipped,
		ROW_NUMBER() OVER(ORDER BY numOppChildItemID) AS RowNumber 
	INTO 
		#tempKits
	FROM 
		OpportunityKitItems OKI 
	JOIN 
		Item I 
	ON 
		OKI.numChildItemID=I.numItemCode    
	WHERE 
		charitemtype='P' 
		AND numWareHouseItemId > 0 
		AND numWareHouseItemId IS NOT NULL 
		AND OKI.numOppID=@numOppID 
		AND OKI.numOppItemID=@numOppItemID 
  

	DECLARE @minRowNumber INT
	DECLARE @maxRowNumber INT

	DECLARE @numOppChildItemID AS NUMERIC(18,0)
	DECLARE @numItemCode NUMERIC(18,0)
	DECLARE @bitAssembly BIT
	DECLARE @numWareHouseItemID NUMERIC(18,0)
	
	DECLARE @onHand FLOAT
	DECLARE @onAllocation FLOAT
	DECLARE @onOrder FLOAT
	DECLARE @onBackOrder FLOAT
	DECLARE @onReOrder FLOAT
	DECLARE @numUnits FLOAT
	DECLARE @QtyShipped FLOAT
	

	SELECT  
		@minRowNumber = MIN(RowNumber),
		@maxRowNumber = MAX(RowNumber) 
	FROM 
		#tempKits


	DECLARE @description AS VARCHAR(300)

	WHILE  @minRowNumber <= @maxRowNumber
    BEGIN
		SELECT 
			@numOppChildItemID = numOppChildItemID,
			@numItemCode=numItemCode,
			@bitAssembly=ISNULL(bitAssembly,0),
			@numWareHouseItemID=numWareHouseItemID,
			@numUnits=numQtyItemsReq,
			@QtyShipped=numQtyShipped 
		FROM 
			#tempKits 
		WHERE 
			RowNumber=@minRowNumber
    
		IF @Mode=0
				SET @description='SO KIT insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
		ELSE IF @Mode=1
			IF @tintMode=2
			BEGIN
				SET @description='SO DEMOTED TO OPPORTUNITY KIT (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
			END
			ELSE
			BEGIN
				SET @description='SO KIT Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
			END
		ELSE IF @Mode=2
				SET @description='SO KIT Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
		ELSE IF @Mode=3
				SET @description='SO KIT Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'

		-- IF KIT ITEM CONTAINS ANOTHER KIT AS CHILD THEN WE HAVE TO UPDATE INVENTORY OF ITEMS OF KIT CHILD ITEMS
		IF EXISTS (SELECT [numOppKitChildItemID] FROM OpportunityKitChildItems WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numOppChildItemID = @numOppChildItemID)
		BEGIN
			EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numOppID, --  numeric(9, 0)
				@tintRefType = 3, --  tinyint
				@vcDescription = @description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomain


			EXEC USP_WareHouseItems_ManageInventoryKitWithinKit
				@numOppID = @numOppID,
				@numOppItemID = @numOppItemID,
				@numOppChildItemID = @numOppChildItemID,
				@Mode = @Mode,
				@tintMode = @tintMode,
				@numDomainID = @numDomain,
				@numUserCntID = @numUserCntID,
				@tintOppType = @tintOppType
		END
		ELSE
		BEGIN
			SELECT 
				@onHand=ISNULL(numOnHand,0),
				@onAllocation=ISNULL(numAllocation,0),                                    
				@onOrder=ISNULL(numOnOrder,0),
				@onBackOrder=ISNULL(numBackOrder,0),
				@onReOrder = ISNULL(numReorder,0)                                     
			FROM 
				WareHouseItems 
			WHERE 
				numWareHouseItemID=@numWareHouseItemID

			IF @bitAssembly = 1
			BEGIN
				IF @Mode=0 --insert/edit
    			BEGIN
					IF EXISTS (SELECT numWOID FROM WorkOrder WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numOppChildItemID=@numOppChildItemID AND ISNULL(numOppKitChildItemID,0) = 0 AND numItemCode=@numItemCode AND numWareHouseItemId=@numWareHouseItemID AND numWOStatus <> 23184)
					BEGIN
						SET @description='SO KIT-WO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
						-- MANAGE INVENTOY OF ASSEMBLY ITEMS
						EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@numOppItemID,@numOppChildItemID,0,@numItemCode,@numWareHouseItemID,@QtyShipped,1
					END
					ELSE
					BEGIN
						SET @description='SO KIT-WO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'

						SET @numUnits = @numUnits - @QtyShipped
                                                        
						IF @onHand >= @numUnits 
						BEGIN                                    
							SET @onHand = @onHand - @numUnits                            
							SET @onAllocation = @onAllocation + @numUnits                                    
						END                                    
						ELSE IF @onHand < @numUnits 
						BEGIN                                    
							SET @onAllocation = @onAllocation + @onHand                                    
							SET @onBackOrder = @onBackOrder + @numUnits
								- @onHand                                    
							SET @onHand = 0                                    
						END    
			                                 
						UPDATE 
							WareHouseItems
						SET  
							numOnHand = @onHand,
							numAllocation = @onAllocation,
							numBackOrder = @onBackOrder,dtModified = GETDATE() 
						WHERE 
							numWareHouseItemID = @numWareHouseItemID
					END
				END
				ELSE IF @Mode=1 -- Revert
				BEGIN
					DECLARE @numWOID AS NUMERIC(18,0)
					DECLARE @numWOStatus AS NUMERIC(18,0)
					SELECT @numWOID=numWOId,@numWOStatus=numWOStatus FROM WorkOrder WHERE numOppId=@numOppId AND numOppItemID=@numOppItemID AND numOppChildItemID=@numOppChildItemID AND ISNULL(numOppKitChildItemID,0) = 0 AND numItemCode=@numItemCode AND numWareHouseItemId=@numWareHouseItemID

					IF  @tintMode=2
					BEGIN
						SET @description='SO KIT-WO Deal Lost (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
					END
					ELSE
					BEGIN
						SET @description='SO KIT-WO Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
					END


					IF @QtyShipped>0
					BEGIN
						IF @tintMode=0
							SET @numUnits = @numUnits - @QtyShipped
						ELSE IF @tintmode=1 OR @tintMode=2
							SET @onAllocation = @onAllocation + @QtyShipped 
					END 


					--IF WORK ORDER IS NOT COMPLETED THAN REMOVE ON ORDER QUANTITY
					IF @numWOStatus <> 23184
					BEGIN
						IF @onOrder >= @numUnits
							SET @onOrder = @onOrder - @numUnits
						ELSE
							SET @onOrder = 0
					END

					-- IF BACKORDER QTY IS GREATER THEN QTY TO REVERT THEN
					-- DECREASE BACKORDER QTY BY QTY TO REVERT
					IF @numUnits < @onBackOrder 
					BEGIN                  
						SET @onBackOrder = @onBackOrder - @numUnits
					END 
					-- IF BACKORDER QTY IS LESS THEN OR EQUAL TO QTY TO REVERT THEN
					-- DECREASE QTY TO REVERT BY QTY OF BACK ORDER AND
					-- SET BACKORDER QTY TO 0
					ELSE IF @numUnits >= @onBackOrder 
					BEGIN
						SET @numUnits = @numUnits - @onBackOrder
						SET @onBackOrder = 0
                        
						--REMOVE ITEM FROM ALLOCATION 
						IF (@onAllocation - @numUnits) >= 0
							SET @onAllocation = @onAllocation - @numUnits
						
						--ADD QTY TO ONHAND
						SET @onHand = @onHand + @numUnits
					END

					UPDATE  
						WareHouseItems
					SET 
						numOnHand = @onHand ,
						numAllocation = @onAllocation,
						numBackOrder = @onBackOrder,
						numOnOrder = @onOrder,
						dtModified = GETDATE()
					WHERE 
						numWareHouseItemID = @numWareHouseItemID   

					--IF WORK ORDER IS NOT COMPLETED THAN DELETE WORK ORDER AND ITS CHILDS WORK ORDER
					IF @numWOStatus <> 23184
					BEGIN
						EXEC USP_DeleteAssemblyWOAndInventory  @numDomain,@numUserCntID,@numWOID,@numOppID,1
					END
				END
				ELSE IF @Mode=2 -- Close
				BEGIN
					SET @description='SO KIT-WO Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
					EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@numOppItemID,@numOppChildItemID,0,@numItemCode,@numWareHouseItemID,@QtyShipped,2
				END
				ELSE IF @Mode = 3 --Re-Open
				BEGIN
					SET @description='SO KIT-WO Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
					EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@numOppItemID,@numOppChildItemID,0,@numItemCode,@numWareHouseItemID,@QtyShipped,3
				END

				EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numOppID, --  numeric(9, 0)
					@tintRefType = 3, --  tinyint
					@vcDescription = @description, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@numDomainID = @numDomain
			END
			ELSE
			BEGIN
    			IF @Mode=0 --insert/edit
    			BEGIN
    				SET @numUnits = @numUnits - @QtyShipped
    	
					IF @onHand>=@numUnits                                    
					BEGIN                                    
						SET @onHand=@onHand-@numUnits                            
						SET @onAllocation=@onAllocation+@numUnits                                    
					END                                    
					ELSE IF @onHand<@numUnits                                    
					BEGIN                                    
						SET @onAllocation=@onAllocation+@onHand                                    
						SET @onBackOrder=@onBackOrder+@numUnits-@onHand                                    
						SET @onHand=0                                    
					END                        
    			END
				ELSE IF @Mode=1 --Revert
				BEGIN
					IF @QtyShipped>0
					BEGIN
						IF @tintMode=0
							SET @numUnits = @numUnits - @QtyShipped
						ELSE IF @tintmode=1 OR @tintMode=2
							SET @onAllocation = @onAllocation + @QtyShipped 
					END 
								                    
					IF @numUnits >= @onBackOrder 
					BEGIN
						SET @numUnits = @numUnits - @onBackOrder
						SET @onBackOrder = 0
                            
						IF (@onAllocation - @numUnits >= 0)
							SET @onAllocation = @onAllocation - @numUnits
						SET @onHand = @onHand + @numUnits                                            
					END                                            
					ELSE IF @numUnits < @onBackOrder 
					BEGIN                  
						IF (@onBackOrder - @numUnits >0)
							SET @onBackOrder = @onBackOrder - @numUnits
					END 
				END
				ELSE IF @Mode=2 --Close
				BEGIN
					SET @numUnits = @numUnits - @QtyShipped  
					SET @onAllocation = @onAllocation - @numUnits            
				END
				ELSE IF @Mode=3 --Re-Open
				BEGIN
					SET @numUnits = @numUnits - @QtyShipped
					SET @onAllocation = @onAllocation + @numUnits            
				END

				UPDATE 
					WareHouseItems 
				SET 
					numOnHand=@onHand,
					numAllocation=@onAllocation,                                    
					numBackOrder=@onBackOrder,
					dtModified = GETDATE()  
				WHERE 
					numWareHouseItemID=@numWareHouseItemID 
	
				EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numOppID, --  numeric(9, 0)
					@tintRefType = 3, --  tinyint
					@vcDescription = @description, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@numDomainID = @numDomain
			END
		END
		
		SET @minRowNumber=@minRowNumber + 1
    END	
  
	DROP TABLE #tempKits
END
GO
/* FOR SO:Adds Qty on allocation and deducts from onhand*/
/* FOR PO:Adds Qty on Order and updates average cost */
--exec USP_UpdatingInventoryonCloseDeal 5833      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatinginventoryonclosedeal')
DROP PROCEDURE usp_updatinginventoryonclosedeal
GO
CREATE PROCEDURE [dbo].[USP_UpdatingInventoryonCloseDeal]
@numOppID as numeric(9),
@numUserCntID AS NUMERIC(9)        
AS
BEGIN TRY
   BEGIN TRANSACTION      
		
		declare @tintOpptype as tinyint        
		declare @itemcode as numeric                                    
		declare @numUnits as FLOAT                                      
		declare @numoppitemtCode as numeric(9)         
		declare @numWareHouseItemID as numeric(9) 
		declare @numToWarehouseItemID as numeric(9) --to be used with stock transfer
		declare @monAmount as DECIMAL(20,5) 
		declare @bitStockTransfer as bit
		declare @QtyShipped as FLOAT
		declare @QtyReceived as FLOAT
		DECLARE @fltExchangeRate AS FLOAT 
		DECLARE @bitWorkOrder AS BIT

		DECLARE @numDomain AS NUMERIC(18,0)
		SELECT @numDomain = OM.numDomainID FROM [dbo].[OpportunityMaster] AS OM WHERE [OM].[numOppId] = @numOppID
		select @tintOpptype=tintOppType,@bitStockTransfer=ISNULL(bitStockTransfer,0),@fltExchangeRate=(CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END) from OpportunityMaster where numOppId=@numOppID
               
		 select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=numUnitHour,@qtyShipped=ISNULL([numQtyShipped],0),@qtyReceived=ISNULL([numUnitHourReceived],0),        
		 @numWareHouseItemID=ISNULL(numWarehouseItmsID,0),@numToWarehouseItemID=numToWarehouseItemID,@monAmount=monTotAmount * @fltExchangeRate,@bitWorkOrder=OI.bitWorkOrder
		 FROM OpportunityItems OI join Item I                                                
		 on OI.numItemCode=I.numItemCode   and numOppId=@numOppId                                          
		 where (charitemtype='P' OR 1=(CASE WHEN @tintOpptype=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END)) and (bitDropShip=0 or bitDropShip is null)
		 AND I.[numDomainID] = @numDomain
		 ORDER by OI.numoppitemtCode                                       
		 while @numoppitemtCode>0                                        
		  begin   
	
	
				IF @bitStockTransfer = 1
				BEGIN
					--Make inventory changes as if sales order was placed , using OpportunityItems.numWarehouseItmsID
					EXEC usp_ManageInventory @itemcode,@numWareHouseItemID,@monAmount,1,@numUnits,@QtyShipped,@qtyReceived,0,1,@numOppID,@numoppitemtCode,@numUserCntID,@bitWorkOrder
					--Make inventory changes as if purchase order was placed , using OpportunityItems.numToWarehouseItemID
					EXEC usp_ManageInventory @itemcode,@numToWarehouseItemID,@monAmount,2,@numUnits,@QtyShipped,@qtyReceived,0,1,@numOppID,@numoppitemtCode,@numUserCntID,@bitWorkOrder
				END
				ELSE
				BEGIN
					EXEC usp_ManageInventory @itemcode,@numWareHouseItemID,@monAmount,@tintOpptype,@numUnits,@QtyShipped,@qtyReceived,0,1,@numOppID,@numoppitemtCode,@numUserCntID,@bitWorkOrder
				END
                                                                                                                                      
		   select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=numUnitHour,@qtyShipped=ISNULL([numQtyShipped],0),@qtyReceived=ISNULL([numUnitHourReceived],0),        
		 @numWareHouseItemID=ISNULL(numWarehouseItmsID,0),@numToWarehouseItemID=numToWarehouseItemID,@monAmount=monTotAmount * @fltExchangeRate,@bitWorkOrder=OI.bitWorkOrder
			from OpportunityItems OI join Item I         
		   on OI.numItemCode=I.numItemCode and numOppId=@numOppId               
		   where (charitemtype='P' OR 1=(CASE WHEN @tintOpptype=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END)) and OI.numoppitemtCode>@numoppitemtCode and (bitDropShip=0 or bitDropShip is null)  
			AND I.[numDomainID] = @numDomain
			order by OI.numoppitemtCode                                                
		   if @@rowcount=0 set @numoppitemtCode=0         
		  END

COMMIT
END TRY
BEGIN CATCH
  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  -- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH
        
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UserMaster_UpdateEmailAlias')
DROP PROCEDURE dbo.USP_UserMaster_UpdateEmailAlias
GO
CREATE PROCEDURE [dbo].[USP_UserMaster_UpdateEmailAlias]
(
	@numDomainID NUMERIC(18,0),
	@numUserID NUMERIC(18,0),
    @vcEmailAlias VARCHAR(100),
	@vcSMTPPassword VARCHAR(100)
)
AS
BEGIN
	UPDATE
		UserMaster
	SET
		vcEmailAlias=@vcEmailAlias
		,vcEmailAliasPassword=@vcSMTPPassword
	WHERE
		numDomainID=@numDomainID
		AND numUserId=@numUserID
END
GO


GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateStagePercentageDetails]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Gangadhar    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ConEmpListByTeamId')
DROP PROCEDURE USP_ConEmpListByTeamId
GO
CREATE PROCEDURE [dbo].[USP_ConEmpListByTeamId]       
@numDomainID as numeric(9)=0,    
@numTeamId as numeric(9)=0            
as    
BEGIN   
SELECT 
		A.numContactId AS numContactID,A.vcFirstName+' '+A.vcLastName as vcUserName
	FROM 
		AdditionalContactsInformation AS A  
	WHERE 
		A.numDomainID=@numDomainId AND
		A.numTeam=@numTeamId
	ORDER BY
		vcUserName 

END 
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateStagePercentageDetails]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Prasanta    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteProcessFromOpportunity')
DROP PROCEDURE USP_DeleteProcessFromOpportunity
GO
CREATE PROCEDURE [dbo].[USP_DeleteProcessFromOpportunity] 
@numOppId AS NUMERIC(18,0)=0
as    
BEGIN   
	IF(@numOppId>0)
	BEGIN
		UPDATE dbo.OpportunityMaster SET numBusinessProcessID=NULL WHERE numOppID=@numOppId
	END
	DELETE FROM ProjectProcessStageDetails WHERE numOppId=@numOppId
	
	DELETE FROM StagePercentageDetailsTask WHERE numOppId=@numOppId

END 
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateStagePercentageDetails]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Gangadhar    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteTask')
DROP PROCEDURE USP_DeleteTask
GO
CREATE PROCEDURE [dbo].[USP_DeleteTask]
@numDomainID as numeric(9)=0,    
@numTaskId as numeric(18)=0 
as    
BEGIN   
	DELETE FROM 
		StagePercentageDetailsTask
	WHERE
		numDomainID=@numDomainID AND numParentTaskId=@numTaskId

	DELETE FROM 
		StagePercentageDetailsTask
	WHERE
		numDomainID=@numDomainID AND numTaskId=@numTaskId
	
END 
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateStagePercentageDetails]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Gangadhar    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetDeleteTask')
DROP PROCEDURE USP_GetDeleteTask
GO
CREATE PROCEDURE [dbo].[USP_GetDeleteTask]
@numDomainID as numeric(9)=0,    
@numTaskId as numeric(18)=0 
as    
BEGIN  
	DELETE FROM  StagePercentageDetailsTask WHERE numTaskId=@numTaskId AND numDomainID=@numDomainID
END 
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateStagePercentageDetails]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Gangadhar    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTaskByStageDetailsId')
DROP PROCEDURE USP_GetTaskByStageDetailsId
GO
CREATE PROCEDURE [dbo].[USP_GetTaskByStageDetailsId]
@numDomainID as numeric(9)=0,    
@numStageDetailsId as numeric(18)=0,
@numOppId AS NUMERIC(18,0)=0,   
@numProjectId AS NUMERIC(18,0)=0,
@vcSearchText AS VARCHAR(MAX)=''
as    
BEGIN   
	DECLARE @dynamicQuery AS NVARCHAR(MAX)=NULL
	SET @dynamicQuery = 'SELECT 
		ST.numTaskId,
		ST.vcTaskName,
		ST.numHours,
		ST.numMinutes,
		ST.numAssignTo,
		ISNULL(AC.vcFirstName+'' ''+AC.vcLastName,''-'') As vcContactName,
		ISNULL(ST.bitDefaultTask,0) AS bitDefaultTask,
		ISNULL(ST.bitTaskClosed,0) AS bitTaskClosed,
		ISNULL(ST.bitSavedTask,0) AS bitSavedTask,
		ISNULL(ST.numParentTaskId,0) AS numParentTaskId,
		ISNULL(AU.vcFirstName+'' ''+AU.vcLastName,''-'') As vcUpdatedByContactName,
		convert(varchar(10),ST.dtmUpdatedOn, 105) + right(convert(varchar(32),ST.dtmUpdatedOn,100),8) As dtmUpdatedOn,
		 convert(varchar(10),PPD.dtmStartDate, 101) + right(convert(varchar(32),PPD.dtmStartDate,100),8) AS StageStartDate,
		ST.numStageDetailsId,
		AC.numTeam
	FROM
		StagePercentageDetailsTask AS ST
	LEFT JOIN 
		AdditionalContactsInformation AS AC
	ON
		ST.numAssignTo=AC.numContactId
	LEFT JOIN 
		AdditionalContactsInformation AS AU
	ON
		ST.numCreatedBy=AU.numContactId
	LEFT JOIN
		ProjectProcessStageDetails AS PPD
	ON
		ST.numStageDetailsId=PPD.numStageDetailsId
	WHERE
		ST.numDomainID='+CAST(@numDomainID AS VARCHAR(100))+' '
	IF(@numStageDetailsId>0)
	BEGIN
		SET @dynamicQuery=@dynamicQuery+' AND ST.numStageDetailsId='+CAST(@numStageDetailsId AS VARCHAR(100))+' '
	END
	SET @dynamicQuery=@dynamicQuery+' AND ST.numOppId='+CAST(@numOppId AS VARCHAR(100))+' '
	SET @dynamicQuery=@dynamicQuery+' AND ST.numProjectId='+CAST(@numProjectId AS VARCHAR(100))+' '
	
	IF(LEN(@vcSearchText)>0)
	BEGIN
		SET @dynamicQuery=@dynamicQuery+' AND ST.vcTaskName LIKE ''%'+CAST(@vcSearchText AS VARCHAR(100))+'%'' '
	END
	EXEC(@dynamicQuery)
END 
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateStagePercentageDetails]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Prasanta    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageProcesstoOpportunity')
DROP PROCEDURE USP_ManageProcesstoOpportunity
GO
CREATE PROCEDURE [dbo].[USP_ManageProcesstoOpportunity]
@numDomainID as numeric(9)=0,    
@numProcessId as numeric(18)=0,      
@numCreatedBy as numeric(18)=0,
@numOppId AS NUMERIC(18,0)=0,   
@numProjectId AS NUMERIC(18,0)=0
as    
BEGIN   
	IF(@numOppId>0)
	BEGIN
		UPDATE OpportunityMaster SET numBusinessProcessID=@numProcessId WHERE numOppId=@numOppId
	END

	INSERT INTO ProjectProcessStageDetails(
		numOppId,
		numProjectId,
		numStageDetailsId,
		dtmStartDate
	)
	SELECT 
		@numOppId,
		@numProjectId,
		numStageDetailsId,
		GETDATE() 
	FROM 
		StagePercentageDetails As ST
	WHERE
		ST.slp_id=@numProcessId

	INSERT INTO StagePercentageDetailsTask(
		numStageDetailsId, 
		vcTaskName, 
		numHours, 
		numMinutes, 
		numAssignTo, 
		numDomainID, 
		numCreatedBy, 
		dtmCreatedOn,
		numOppId,
		numProjectId,
		numParentTaskId,
		bitDefaultTask,
		bitSavedTask
	)
	SELECT 
		ST.numStageDetailsId,
		vcTaskName,
		numHours,
		numMinutes,
		ST.numAssignTo,
		@numDomainID,
		@numCreatedBy,
		GETDATE(),
		@numOppId,
		@numProjectId,
		0,
		1,
		CASE WHEN SP.bitRunningDynamicMode=1 THEN 0 ELSE 1 END
	FROM 
		StagePercentageDetailsTask AS ST
	LEFT JOIN
		StagePercentageDetails As SP
	ON
		ST.numStageDetailsId=SP.numStageDetailsId
	WHERE
		ST.numStageDetailsId IN(SELECT numStageDetailsId FROM 
		StagePercentageDetails As ST
	WHERE
		ST.slp_id=@numProcessId)

END 
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateStagePercentageDetails]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Gangadhar    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageStageTask')
DROP PROCEDURE USP_ManageStageTask
GO
CREATE PROCEDURE [dbo].[USP_ManageStageTask]
@numDomainID as numeric(9)=0,    
@numStageDetailsId as numeric(18)=0,           
@vcTaskName as VARCHAR(500)='',           
@numHours as numeric(9)=0,           
@numMinutes as numeric(9)=0,           
@numAssignTo as numeric(18)=0,           
@numCreatedBy as numeric(18)=0,
@numOppId AS NUMERIC(18,0)=0,   
@numProjectId AS NUMERIC(18,0)=0,   
@numParentTaskId AS NUMERIC(18,0)=0,
@bitTaskClosed AS BIT=0,
@numTaskId AS NUMERIC=0,
@bitSavedTask AS BIT=0
as    
BEGIN   
	DECLARE @bitDefaultTask AS BIT=0
	IF(@numTaskId=0)
	BEGIN
		IF(@numOppId>0)
		BEGIN
			SET @bitDefaultTask=0
			SET @bitSavedTask=1
		END
		SET @bitTaskClosed =0
		INSERT INTO StagePercentageDetailsTask(
			numStageDetailsId, 
			vcTaskName, 
			numHours, 
			numMinutes, 
			numAssignTo, 
			numDomainID, 
			numCreatedBy, 
			dtmCreatedOn,
			numOppId,
			numProjectId,
			numParentTaskId,
			bitDefaultTask,
			bitTaskClosed,
			bitSavedTask
		)
		VALUES(
			@numStageDetailsId,
			@vcTaskName,
			@numHours,
			@numMinutes,
			@numAssignTo,
			@numDomainId,
			@numCreatedBy,
			GETDATE(),
			@numOppId,
			@numProjectId,
			@numParentTaskId,
			@bitDefaultTask,
			@bitTaskClosed,
			@bitSavedTask
		)
	END
	ELSE
	BEGIN
		DECLARE @vcRunningTaskName AS VARCHAR(500)=0
		SELECT TOP 1 @numStageDetailsId=numStageDetailsId,@vcRunningTaskName=vcTaskName,@bitDefaultTask=bitDefaultTask
		 FROM StagePercentageDetailsTask WHERE numTaskId=@numTaskId

		DECLARE @bitRunningDynamicMode AS BIT=0
		SET @bitRunningDynamicMode=(SELECT TOP 1 bitRunningDynamicMode FROM StagePercentageDetails WHERE numStageDetailsId=@numStageDetailsId)
		PRINT @bitRunningDynamicMode

		IF(@bitRunningDynamicMode=1 AND @numOppId>0)
		BEGIN

			DECLARE @recordCount AS INT=1
			IF(@bitDefaultTask=1)
			BEGIN
				SET @recordCount=(SELECT COUNT(*) FROM StagePercentageDetailsTask WHERE numParentTaskId=@numTaskId AND numStageDetailsId=@numStageDetailsId)
				UPDATE 
					StagePercentageDetailsTask
				SET 
					bitSavedTask=0
				WHERE
					bitDefaultTask=1 AND numStageDetailsId=@numStageDetailsId
			
			END
			IF(@recordCount=0)
			BEGIN
				DELETE FROM 
					StagePercentageDetailsTask
				WHERE
					numParentTaskId IN(SELECT numTaskId FROM StagePercentageDetailsTask WHERE 
					bitDefaultTask=1 AND numStageDetailsId=@numStageDetailsId)
				INSERT INTO StagePercentageDetailsTask(
				numStageDetailsId, 
				vcTaskName, 
				numHours, 
				numMinutes, 
				numAssignTo, 
				numDomainID, 
				numCreatedBy, 
				dtmCreatedOn,
				numOppId,
				numProjectId,
				numParentTaskId,
				bitDefaultTask,
				bitTaskClosed,
				bitSavedTask
			)
			SELECT 
				numStageDetailsId, 
				vcTaskName, 
				numHours, 
				numMinutes, 
				numAssignTo, 
				@numDomainID, 
				@numCreatedBy, 
				GETDATE(),
				@numOppId,
				@numProjectId,
				@numTaskId,
				0,
				0,
				1 
			FROM
				StagePercentageDetailsTask AS ST
			WHERE
				ST.numTaskId IN(
				SELECT numSecondaryListItemID FROM FieldRelationshipDTL WHERE 
				numPrimaryListItemID=(SELECT TOP 1 numTaskId FROM StagePercentageDetailsTask WHERE numOppId=0 AND numStageDetailsId=@numStageDetailsId AND vcTaskName=@vcRunningTaskName) AND
				numFieldRelID=(SELECT TOP 1 numFieldRelID FROM FieldRelationship WHERE numPrimaryListID=@numStageDetailsId AND ISNULL(bitTaskRelation,0)=1)
				) 					
			END

	
					
		END
	
		IF(@numAssignTo>0)
		BEGIN
			UPDATE 
				StagePercentageDetailsTask
			SET 
				numAssignTo=@numAssignTo
			WHERE
				numTaskId=@numTaskId
		END
		UPDATE 
			StagePercentageDetailsTask
		SET
			bitSavedTask=@bitSavedTask,
			bitTaskClosed=@bitTaskClosed,
			numCreatedBy=@numCreatedBy,
			dtmUpdatedOn=GETDATE()
		WHERE
			numTaskId=@numTaskId
	END
END 
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateStagePercentageDetails]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Prasanta    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateStartDateProcesstoOpportunity')
DROP PROCEDURE USP_UpdateStartDateProcesstoOpportunity
GO
CREATE PROCEDURE [dbo].[USP_UpdateStartDateProcesstoOpportunity]
@numStageDetailsId as numeric(18)=0,   
@numOppId AS NUMERIC(18,0)=0,   
@numProjectId AS NUMERIC(18,0)=0,
@dtmStartDate AS DATETIME=NULL
as    
BEGIN   
	UPDATE 
		ProjectProcessStageDetails
	SET
		dtmStartDate=@dtmStartDate
	WHERE
		numOppId=@numOppId AND numStageDetailsId=@numStageDetailsId
	
END 
