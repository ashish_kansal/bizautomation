/******************************************************************
Project: Release 7.3 Date: 05.APR.2017
Comments: STORE PROCEDURES
*******************************************************************/


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='CheckOrderInventoryStatus')
DROP FUNCTION CheckOrderInventoryStatus
GO
CREATE FUNCTION [dbo].[CheckOrderInventoryStatus] 
 (
      @numOppID AS NUMERIC(9),
      @numDomainID AS NUMERIC(9)
    )
RETURNS NVARCHAR(MAX)
AS BEGIN

	DECLARE @vcInventoryStatus AS NVARCHAR(MAX);SET @vcInventoryStatus='<font color="#000000">Not Applicable</font>';
	
	IF EXISTS(SELECT tintshipped FROM dbo.OpportunityMaster WHERE numDomainId=@numDomainID 
			  AND numOppId=@numOppID AND ISNULL(tintshipped,0)=1)
	BEGIN
		SET @vcInventoryStatus='<font color="#008000">Allocation Cleared</font>';
	END
	ELSE
	BEGIN
		DECLARE @ItemCount AS INT;SET @ItemCount=0
		DECLARE @BackOrderItemCount AS INT;SET @BackOrderItemCount=0

		
		SELECT 
			@ItemCount = ISNULL(COUNT(*),0),
			@BackOrderItemCount = ISNULL(SUM(CAST(bitBackOrder AS INT)),0)
		FROM
		(SELECT
			(CASE 
				WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 
				THEN 0
                WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                ELSE 0
             END) as bitKitParent,
			IIF(dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,isnull(Opp.numWarehouseItmsID,0),bitKitParent) > 0,1,0) AS bitBackOrder
		FROM dbo.OpportunityItems Opp
			JOIN item I ON Opp.numItemCode = i.numItemcode
			JOIN WareHouseItems WItems ON Opp.numItemCode = WItems.numItemID 
			AND WItems.numWareHouseItemID = Opp.numWarehouseItmsID
			WHERE numOppId=@numOppID AND I.numDomainID=@numDomainID AND charItemType='P'
			and (bitDropShip=0 or bitDropShip is null)) AS TEMP    
		
	    
		IF @ItemCount>0 
		BEGIN
			IF @BackOrderItemCount>0
				SET @vcInventoryStatus = '<font color="#FF0000">Back Order (' + CAST(@BackOrderItemCount AS VARCHAR(18)) + '/' + CAST(@ItemCount AS VARCHAR(18)) +')</font>'
			ELSE
				SET @vcInventoryStatus = '<font color="#800080">Ready to Ship</font>';			
		END 
	END

	IF (SELECT 
			COUNT(*) 
		FROM 
			OpportunityItems OI
		INNER JOIN 
			OpportunityMaster OM 
		ON 
			OM.numOppId = OI.numOppId 
		WHERE 
			OI.numOppId = @numOppID
			AND OM.numDomainId = @numDomainId
			AND OM.tintOppType = 1
			AND OM.tintOppStatus=1
			AND ISNULL(OI.bitDropShip, 0) = 1
			AND 1 = (CASE WHEN (SELECT COUNT(*) FROM OpportunityLinking OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numChildOppID=OIInner.numOppId WHERE OMInner.numParentOppID = @numOppID AND OIInner.numItemCode=OI.numItemCode AND ISNULL(OIInner.numWarehouseItmsID,0)=ISNULL(OI.numWarehouseItmsID,0)) > 0 THEN 0 ELSE 1 END)) > 0
	BEGIN
		SET @vcInventoryStatus = CONCAT(@vcInventoryStatus,' <a href=''#'' onclick=''OpenCreateDopshipPOWindow(',@numOppID,')''><img src=''../images/Dropship.png'' style=''height:25px;'' /></a>')
	END

	IF (SELECT 
			COUNT(*) 
		FROM 
			dbo.OpportunityItems OI
			INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
			INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
			INNER JOIN WarehouseItems WI ON OI.numWarehouseItmsID = WI.numWareHouseItemID
			LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
		WHERE   
			OI.numOppId = @numOppID
			AND OM.numDomainId = @numDomainId
			AND ISNULL((SELECT bitReOrderPoint FROM Domain WHERE numDomainId=@numDomainID),0) = 1
			AND (ISNULL(WI.numOnHand,0) + ISNULL(WI.numOnOrder,0) <= ISNULL(WI.numReOrder,0))
			AND OM.tintOppType = 1
			AND OM.tintOppStatus=1
			AND ISNULL(I.bitAssembly, 0) = 0
			AND ISNULL(I.bitKitParent, 0) = 0
			AND ISNULL(OI.bitDropship,0) = 0) > 0
	BEGIN
		SET @vcInventoryStatus = CONCAT(@vcInventoryStatus,' <a href=''#'' onclick=''OpenCreateBackOrderPOWindow(',@numOppID,')''><img src=''../images/BackorderPO.png'' style=''height:25px;'' /></a>')
	END
    
    RETURN ISNULL(@vcInventoryStatus,'')
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='tf'AND NAME ='fn_getOPPAddressDetails')
DROP FUNCTION fn_getOPPAddressDetails
GO
CREATE FUNCTION [dbo].[fn_getOPPAddressDetails] (@numOppId numeric,@numDomainID  NUMERIC,@tintMode AS TINYINT)
RETURNS 
@ParsedList table
(
	numCountry	NUMERIC(18),
	numState	NUMERIC(18),
	vcCountry	VARCHAR(50),
	vcState		VARCHAR(50),
	vcCity		VARCHAR(50),
	vcStreet	VARCHAR(100),
	vcPostalCode VARCHAR(20),
	vcAddressName VARCHAR(50),
	vcCompanyName VARCHAR(100)
)
as
begin
declare @strAddress varchar(1000)

  DECLARE  @tintOppType  AS TINYINT
  DECLARE  @tintBillType  AS TINYINT
  DECLARE  @tintShipType  AS TINYINT
  DECLARE @numBillToAddressID NUMERIC(18,0)
  DECLARE @numShipToAddressID NUMERIC(18,0)
 
DECLARE @numParentOppID AS NUMERIC,@numDivisionID AS NUMERIC 
      
SELECT  @tintOppType = tintOppType,
        @tintBillType = tintBillToType,
        @tintShipType = tintShipToType,
        @numDivisionID = numDivisionID,
		@numBillToAddressID = ISNULL(numBillToAddressID,0)
		,@numShipToAddressID=ISNULL(numShipToAddressID,0)
      FROM   OpportunityMaster WHERE  numOppId = @numOppId

-- When Creating PO from SO and Bill type is Customer selected 
SELECT @numParentOppID=ISNULL(numParentOppID,0) FROM dbo.OpportunityLinking WHERE numChildOppID = @numOppId
            
IF @tintMode=1 --Billing Address
BEGIN
	IF (@tintBillType IS NULL AND @tintOppType = 1) OR (@tintBillType = 1 AND @tintOppType = 1) --Primary Bill Address or When Sales order and bill to is set to customer	 
	BEGIN
		INSERT INTO @ParsedList(numCountry,numState,vcCountry,vcState,vcCity,vcStreet,vcPostalCode,vcAddressName,vcCompanyName)
		SELECT  ISNULL(AD.numCountry,0),
				ISNULL(AD.numState,0),
				ISNULL(dbo.fn_GetListItemName(AD.numCountry),''),
				ISNULL(dbo.fn_GetState(AD.numState),''),
				ISNULL(AD.VcCity,'') ,
			    ISNULL(AD.vcStreet,''),
			    ISNULL(AD.vcPostalCode,''),ISNULL(AD.vcAddressName,'') AS vcAddressName,dbo.fn_GetComapnyName(@numDivisionID) AS vcCompanyName 
        FROM AddressDetails AD 
		WHERE AD.numDomainID = @numDomainID 
		AND AD.numRecordID = @numDivisionID 
		AND AD.tintAddressOf = 2 
		AND AD.tintAddressType = 1 
		AND AD.bitIsPrimary=1                    
	END
	ELSE IF @tintBillType = 1 AND @tintOppType = 2 -- When Create PO from SO and Bill to is set to Customer
	BEGIN
		INSERT INTO @ParsedList(numCountry,numState,vcCountry,vcState,vcCity,vcStreet,vcPostalCode,vcAddressName,vcCompanyName)
		SELECT numCountry,numState,vcCountry,vcState,vcCity,vcStreet,vcPostalCode,vcAddressName,vcCompanyName  
		FROM dbo.fn_getOPPAddressDetails(@numParentOppID,@numDomainID,@tintMode) 
	END
	ELSE IF ISNULL(@tintBillType,0) = 0
	BEGIN
		INSERT INTO @ParsedList(numCountry,numState,vcCountry,vcState,vcCity,vcStreet,vcPostalCode,vcAddressName,vcCompanyName)
		SELECT  ISNULL(AD.numCountry,0),
				ISNULL(AD.numState,0),
				ISNULL(dbo.fn_GetListItemName(AD.numCountry),''),
				ISNULL(dbo.fn_GetState(AD.numState),''),
				ISNULL(AD.VcCity,'') ,
			    ISNULL(AD.vcStreet,''),
			    ISNULL(AD.vcPostalCode,''),ISNULL(AD.vcAddressName,'') AS vcAddressName,Com1.vcCompanyName 
		FROM   companyinfo [Com1] JOIN divisionmaster div1 ON com1.numCompanyID = div1.numCompanyID
		JOIN Domain D1 ON D1.numDivisionID = div1.numDivisionID
		JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary=1
		WHERE  D1.numDomainID = @numDomainID	
		
	END
	ELSE IF @tintBillType = 2 
	BEGIN
		IF ISNULL(@numBillToAddressID,0) > 0
		BEGIN
			INSERT INTO @ParsedList(numCountry,numState,vcCountry,vcState,vcCity,vcStreet,vcPostalCode,vcAddressName,vcCompanyName)
			SELECT  ISNULL(AD.numCountry,0),
					ISNULL(AD.numState,0),
					ISNULL(dbo.fn_GetListItemName(AD.numCountry),''),
					ISNULL(dbo.fn_GetState(AD.numState),''),
					ISNULL(AD.VcCity,'') ,
					ISNULL(AD.vcStreet,''),
					ISNULL(AD.vcPostalCode,''),ISNULL(AD.vcAddressName,'') AS vcAddressName,dbo.fn_GetComapnyName(numRecordID) AS vcCompanyName 
            FROM AddressDetails AD WHERE numAddressID=@numBillToAddressID
		END
		ELSE
		BEGIN
			INSERT INTO @ParsedList(numCountry,numState,vcCountry,vcState,vcCity,vcStreet,vcPostalCode,vcAddressName,vcCompanyName)
			SELECT  numBillCountry,
					numBillState,
					isnull(dbo.fn_GetListItemName(numBillCountry),''),
					isnull(dbo.fn_GetState(numBillState),''),
					VcBillCity,
					vcBillStreet,
					vcBillPostCode,ISNULL(vcAddressName,'') AS vcAddressName,dbo.fn_GetComapnyName(@numDivisionID) AS vcCompanyName 
			FROM   OpportunityAddress WHERE  numOppID = @numOppId
		END
	END
	ELSE IF @tintBillType = 3
	BEGIN
		INSERT INTO @ParsedList(numCountry,numState,vcCountry,vcState,vcCity,vcStreet,vcPostalCode,vcAddressName,vcCompanyName)
		SELECT  numBillCountry,
				numBillState,
				isnull(dbo.fn_GetListItemName(numBillCountry),''),
				isnull(dbo.fn_GetState(numBillState),''),
				VcBillCity,
				vcBillStreet,
				vcBillPostCode,ISNULL(vcAddressName,'') AS vcAddressName,dbo.fn_GetComapnyName(@numDivisionID) AS vcCompanyName 
		FROM   OpportunityAddress WHERE  numOppID = @numOppId
	END
END
ELSE IF @tintMode=2 --Shipping Address
BEGIN
	IF @tintShipType IS NULL OR (@tintShipType = 1 AND @tintOppType = 1)
	BEGIN
			INSERT INTO @ParsedList(numCountry,numState,vcCountry,vcState,vcCity,vcStreet,vcPostalCode,vcAddressName,vcCompanyName)
			SELECT  ISNULL(AD.numCountry,0),
					ISNULL(AD.numState,0),
					ISNULL(dbo.fn_GetListItemName(AD.numCountry),''),
					ISNULL(dbo.fn_GetState(AD.numState),''),
					ISNULL(AD.VcCity,'') ,
					ISNULL(AD.vcStreet,''),
					ISNULL(AD.vcPostalCode,''),ISNULL(AD.vcAddressName,'') AS vcAddressName,dbo.fn_GetComapnyName(@numDivisionID) AS vcCompanyName 
            FROM AddressDetails AD 
				WHERE AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
				AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1
	END
	ELSE IF @tintShipType = 1 AND @tintOppType = 2 -- When Create PO from SO and Ship to is set to Customer 
	BEGIN
		INSERT INTO @ParsedList(numCountry,numState,vcCountry,vcState,vcCity,vcStreet,vcPostalCode,vcAddressName,vcCompanyName)
		SELECT numCountry,numState,vcCountry,vcState,vcCity,vcStreet,vcPostalCode,vcAddressName,vcCompanyName  
		FROM dbo.fn_getOPPAddressDetails(@numParentOppID,@numDomainID,@tintMode) 
	END
	ELSE IF @tintShipType = 0
	BEGIN
		INSERT INTO @ParsedList(numCountry,numState,vcCountry,vcState,vcCity,vcStreet,vcPostalCode,vcAddressName,vcCompanyName)
		SELECT  ISNULL(AD.numCountry,0),
				ISNULL(AD.numState,0),
				ISNULL(dbo.fn_GetListItemName(AD.numCountry),''),
				ISNULL(dbo.fn_GetState(AD.numState),''),
				ISNULL(AD.VcCity,'') ,
				ISNULL(AD.vcStreet,''),
				ISNULL(AD.vcPostalCode,''),ISNULL(AD.vcAddressName,'') AS vcAddressName,Com1.vcCompanyName 
         FROM   companyinfo [Com1] JOIN divisionmaster div1 ON com1.numCompanyID = div1.numCompanyID
         JOIN Domain D1 ON D1.numDivisionID = div1.numDivisionID
         JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 2 AND bitIsPrimary=1
         WHERE  D1.numDomainID = @numDomainID
	END
	ELSE IF @tintShipType = 2 
	BEGIN
		IF ISNULL(@numShipToAddressID,0) > 0
		BEGIN
			INSERT INTO @ParsedList(numCountry,numState,vcCountry,vcState,vcCity,vcStreet,vcPostalCode,vcAddressName,vcCompanyName)
			SELECT  ISNULL(AD.numCountry,0),
					ISNULL(AD.numState,0),
					ISNULL(dbo.fn_GetListItemName(AD.numCountry),''),
					ISNULL(dbo.fn_GetState(AD.numState),''),
					ISNULL(AD.VcCity,'') ,
					ISNULL(AD.vcStreet,''),
					ISNULL(AD.vcPostalCode,''),ISNULL(AD.vcAddressName,'') AS vcAddressName,dbo.fn_GetComapnyName(numRecordID) AS vcCompanyName 
            FROM AddressDetails AD WHERE numAddressID=@numShipToAddressID
		END
		ELSE
		BEGIN
			INSERT INTO @ParsedList(numCountry,numState,vcCountry,vcState,vcCity,vcStreet,vcPostalCode,vcAddressName,vcCompanyName)
			SELECT numShipCountry,numShipState,isnull(dbo.fn_GetListItemName(numShipCountry),''),isnull(dbo.fn_GetState(numShipState),''),VcShipCity,vcShipStreet,vcShipPostCode,ISNULL(vcAddressName,'') AS vcAddressName
			,dbo.fn_GetComapnyName(@numDivisionID) AS vcCompanyName 
			FROM   OpportunityAddress WHERE  numOppID = @numOppId
		END
	END
	ELSE IF @tintShipType = 3
	BEGIN
		INSERT INTO @ParsedList(numCountry,numState,vcCountry,vcState,vcCity,vcStreet,vcPostalCode,vcAddressName,vcCompanyName)
		SELECT numShipCountry,numShipState,isnull(dbo.fn_GetListItemName(numShipCountry),''),isnull(dbo.fn_GetState(numShipState),''),VcShipCity,vcShipStreet,vcShipPostCode,ISNULL(vcAddressName,'') AS vcAddressName
		,dbo.fn_GetComapnyName(@numDivisionID) AS vcCompanyName 
		FROM   OpportunityAddress WHERE  numOppID = @numOppId
	END
END

RETURN
end
GO
/****** Object:  StoredProcedure [dbo].[USP_GetDealsList1]    Script Date: 05/07/2009 22:12:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdealslist1')
DROP PROCEDURE usp_getdealslist1
GO
CREATE PROCEDURE [dbo].[USP_GetDealsList1]
               @numUserCntID              NUMERIC(9)  = 0,
               @numDomainID               NUMERIC(9)  = 0,
               @tintSalesUserRightType    TINYINT  = 0,
               @tintPurchaseUserRightType TINYINT  = 0,
               @tintSortOrder             TINYINT  = 4,
               @CurrentPage               INT,
               @PageSize                  INT,
               @TotRecs                   INT  OUTPUT,
               @columnName                AS VARCHAR(50),
               @columnSortOrder           AS VARCHAR(10),
               @numCompanyID              AS NUMERIC(9)  = 0,
               @bitPartner                AS BIT  = 0,
               @ClientTimeZoneOffset      AS INT,
               @tintShipped               AS TINYINT,
               @intOrder                  AS TINYINT,
               @intType                   AS TINYINT,
               @tintFilterBy              AS TINYINT  = 0,
               @numOrderStatus			  AS NUMERIC,
			   @vcRegularSearchCriteria varchar(MAX)='',
			   @vcCustomSearchCriteria varchar(MAX)='',
			   @SearchText VARCHAR(300) = '',
			   @SortChar char(1)='0'  
AS
	DECLARE  @PageId  AS TINYINT
	DECLARE  @numFormId  AS INT 
  
	SET @PageId = 0

	IF @inttype = 1
	BEGIN
		SET @PageId = 2
		SET @inttype = 3
		SET @numFormId=39
	END
	ELSE IF @inttype = 2
	BEGIN
		SET @PageId = 6
		SET @inttype = 4
		SET @numFormId=41
	END

	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormId AND numRelCntType=@numFormId AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				@numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=@numFormId 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = @numFormId
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = @numFormId
		ORDER BY 
			tintOrder asc  
	END

	DECLARE  @strColumns AS VARCHAR(MAX)
	SET @strColumns = ' ADC.numContactId, Div.numDivisionID,ISNULL(Div.numTerID,0) numTerID, opp.numRecOwner as numRecOwner, Div.tintCRMType, opp.numOppId, opp.monDealAmount, ISNULL(opp.intUsedShippingCompany,0) AS intUsedShippingCompany'


	DECLARE  @tintOrder  AS TINYINT;SET @tintOrder = 0
	DECLARE  @vcFieldName  AS VARCHAR(50)
	DECLARE  @vcListItemType  AS VARCHAR(3)
	DECLARE  @vcListItemType1  AS VARCHAR(1)
	DECLARE  @vcAssociatedControlType VARCHAR(30)
	DECLARE  @numListID  AS NUMERIC(9)
	DECLARE  @vcDbColumnName VARCHAR(40)
	DECLARE  @WhereCondition VARCHAR(2000);SET @WhereCondition = ''
	DECLARE  @vcLookBackTableName VARCHAR(2000)
	DECLARE  @bitCustom  AS BIT
	Declare @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)   
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @vcColumnName AS VARCHAR(500)          
	
	Declare @ListRelID as numeric(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = ''        

	SELECT TOP 1 
		@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
		@numListID=numListID,@vcLookBackTableName=vcLookBackTableName ,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
		@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder ASC            
--	SET @strColumns = @strColumns + ' , (SELECT SUBSTRING((SELECT '', '' + cast(I.vcItemName as varchar)
--                      FROM OpportunityItems AS t
--					  LEFT JOIN OpportunityBizDocs as BC
--					  ON t.numOppId=BC.numOppId
--LEFT JOIN Item as I
--ON t.numItemCode=I.numItemCode
--WHERE t.numOppId=Opp.numOppId AND t.numoppItemtCode NOT IN (SELECT numOppItemId FROM OpportunityBizDocItems WHERE numOppBizDocID=BC.numOppBizDocsId)
--                     FOR XML PATH('''')), 2, 200000)  )[List_Item_Approval_UNIT] '
--SET @strColumns = @strColumns + ' , (SELECT 
-- SUBSTRING((SELECT '', '' + cast(I.vcItemName as varchar)
--FROM 
-- OpportunityItems AS t
--LEFT JOIN 
-- Item as I
--ON 
-- t.numItemCode=I.numItemCode
--WHERE 
-- t.numOppId=Opp.numOppId AND t.numoppItemtCode NOT IN (SELECT numOppItemId FROM OpportunityBizDocs JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId=OpportunityBizDocItems.numOppBizDocID  WHERE OpportunityBizDocs.numOppId=Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1)
--                     FOR XML PATH('''')), 2, 200000)  )[List_Item_Approval_UNIT] '
	WHILE @tintOrder > 0
    BEGIN
		IF @bitCustom = 0
		BEGIN
			DECLARE  @Prefix  AS VARCHAR(5)

			IF @vcLookBackTableName = 'AdditionalContactsInformation'
				SET @Prefix = 'ADC.'
			IF @vcLookBackTableName = 'DivisionMaster'
				SET @Prefix = 'Div.'
			IF @vcLookBackTableName = 'OpportunityMaster'
				SET @PreFix = 'Opp.'
			IF @vcLookBackTableName = 'OpportunityRecurring'
				SET @PreFix = 'OPR.'
			IF @vcLookBackTableName = 'CompanyInfo'
				SET @PreFix = 'cmp.'
			IF @vcLookBackTableName = 'ProjectProgress'
				SET @PreFix = 'PP'
			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

			IF @vcAssociatedControlType = 'SelectBox'
			BEGIN
				IF @vcDbColumnName = 'numPartner'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartner) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartner' 
				END
				IF @vcDbColumnName = 'numPartenerContact'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartenerContact) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT A.vcFirstName+'' ''+ A.vcLastName FROM AdditionalContactsInformation AS A WHERE A.numContactId=Opp.numPartenerContact) AS vcPartenerContact' 
				END
				IF @vcDbColumnName = 'numStatus'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT COUNT(OpportunityItems.numoppitemtCode) FROM OpportunityItems 
WHERE OpportunityItems.bitItemPriceApprovalRequired=1 AND OpportunityItems.numOppId=Opp.numOppId) ApprovalMarginCount'
				END
				IF @vcDbColumnName = 'tintSource'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'')' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'numCampainID'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID) ' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL((SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID),'''') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'LI'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'S'
				BEGIN
					SET @strColumns = @strColumns + ',S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left join State S' + CONVERT(VARCHAR(3),@tintOrder) + ' on S' + CONVERT(VARCHAR(3),@tintOrder) + '.numStateID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'BP'
				BEGIN
					SET @strColumns = @strColumns + ',SPLM.Slp_Name' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'SPLM.Slp_Name LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' LEFT JOIN Sales_process_List_Master SPLM on SPLM.Slp_Id = OPP.numBusinessProcessID '
				END
				ELSE IF @vcListItemType = 'PP'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(PP.intTotalProgress,0)' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL(PP.intTotalProgress,0) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'T'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'U'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'WI'
				BEGIN
					SET @strColumns = @strColumns + ', Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end) LIKE ''%' + @SearchText + '%'''
					END

					set @WhereCondition= @WhereCondition +' left JOIN OpportunityLinking OL on OL.numChildOppID=Opp.numOppId left join WebAPI on WebAPI.WebApiId=isnull(OL.numWebApiId,0)
													left join Sites on isnull(OL.numSiteID,0)=Sites.numSIteID and Sites.numDomainID=' + convert(varchar(15),@numDomainID)                                                       
				END 
            END
			ELSE IF @vcAssociatedControlType = 'DateField'
			BEGIN
				IF @Prefix ='OPR.'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'+convert(varchar(15),-@ClientTimeZoneOffset)+', dtRecurringDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) AS  ['+ @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'+convert(varchar(15),-@ClientTimeZoneOffset)+', dtRecurringDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'dtReleaseDate'
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),'
                                + @Prefix
                                + @vcDbColumnName
                                + ')= convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate())) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,-1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']' 	

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
					END

				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
                                + @Prefix
                                + @vcDbColumnName
                                + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']' 	

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
					END
				 END	
            END
            ELSE
            IF @vcAssociatedControlType = 'TextBox'
            BEGIN
				
                SET @strColumns = @strColumns
                                  + ','
                                  + CASE 
                                      WHEN @vcDbColumnName = 'numAge' THEN 'year(getutcdate())-year(bintDOB)'
                                      WHEN @vcDbColumnName = 'monPAmount' THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(MONEY,monPAmount),1)'
                                      WHEN @vcDbColumnName = 'numDivisionID' THEN 'Div.numDivisionID'
                                      WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
                                      WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
                                      WHEN @vcDbColumnName = 'vcPOppName' THEN 'Opp.vcPOppName'
									  WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId FOR XML PATH('''')),4,200000)),'''')'
                                      ELSE @Prefix + @vcDbColumnName
                                    END
                                  + ' ['
                                  + @vcColumnName + ']'
				IF(@vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList')
				BEGIN
					SET @strColumns=@strColumns+','+' (SELECT COUNT(I.vcItemName) FROM 
													 OpportunityItems AS t LEFT JOIN  Item as I ON 
													 t.numItemCode=I.numItemCode WHERE 
													 t.numOppId=Opp.numOppId AND t.numoppItemtCode NOT IN 
													 (SELECT numOppItemId FROM OpportunityBizDocs JOIN 
													 OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId=OpportunityBizDocItems.numOppBizDocID  
													 WHERE OpportunityBizDocs.numOppId=Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1)) AS [List_Item_Approval_UNIT] '
				END
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + (CASE 
                                      WHEN @vcDbColumnName = 'numAge' THEN 'year(getutcdate())-year(bintDOB)'
                                      WHEN @vcDbColumnName = 'monPAmount' THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(MONEY,monPAmount),1)'
                                      WHEN @vcDbColumnName = 'numDivisionID' THEN 'Div.numDivisionID'
                                      WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
                                      WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
                                      WHEN @vcDbColumnName = 'vcPOppName' THEN 'Opp.vcPOppName'
									  WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId FOR XML PATH('''')),4,200000)),'''')'
                                      ELSE @Prefix + @vcDbColumnName
                                    END) + ' LIKE ''%' + @SearchText + '%'''
				END
            END
            ELSE
            IF @vcAssociatedControlType = 'TextArea'
            BEGIN
				SET @strColumns=@strColumns+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']' 

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
				END
            END
            ELSE IF  @vcAssociatedControlType='Label'                                              
			BEGIN
				SET @strColumns=@strColumns + ',' + CASE                    
				WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'--'[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
				WHEN @vcDbColumnName = 'vcInventoryStatus' THEN 'ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),'''')'
				WHEN @vcDbColumnName = 'vcOrderedShipped' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped)'
				WHEN @vcDbColumnName = 'vcOrderedReceived' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				else @Prefix + @vcDbColumnName END +' ['+ @vcColumnName+']'        
				
				IF @vcDbColumnName='vcOrderedShipped'
				BEGIN
				DECLARE @temp VARCHAR(MAX)
				SET @temp = '(SELECT 
				STUFF((SELECT '', '' + CAST((CAST(numOppBizDocsId AS varchar)+''~''+CAST(ISNULL(numShippingReportId,0) AS varchar) ) AS VARCHAR(MAX)) [text()]
				FROM OpportunityBizDocs
				LEFT JOIN ShippingReport
				ON ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId
				WHERE OpportunityBizDocs.numOppId=Opp.numOppId  AND OpportunityBizDocs.bitShippingGenerated=1  FOR XML PATH(''''), TYPE)
						.value(''.'',''NVARCHAR(MAX)''),1,2,'' ''))'
					SET @strColumns = @strColumns + ' , (SELECT SUBSTRING('+ @temp +', 2, 200000))AS ShippingIcons '
					SET @strColumns = @strColumns + ' , CASE WHEN (SELECT COUNT(*) FROM ShippingBox INNER JOIN ShippingReport ON ShippingBox.numShippingReportId = ShippingReport.numShippingReportId WHERE ShippingReport.numOppID = Opp.numOppID AND LEN(vcTrackingNumber) > 0) > 0 THEN 1 ELSE 0 END [ISTrackingNumGenerated]'
				END

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + (CASE                    
					WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'
					WHEN @vcDbColumnName = 'vcInventoryStatus' THEN 'ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),'''')'
					WHEN @vcDbColumnName = 'vcOrderedShipped' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped)'
					WHEN @vcDbColumnName = 'vcOrderedReceived' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped)'
					WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
					WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
					else @Prefix + @vcDbColumnName END) + ' LIKE ''%' + @SearchText + '%'''
				END    
			END
            ELSE IF @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
			BEGIN
				SET @strColumns = @strColumns + ',(SELECT SUBSTRING(
								(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
								FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
								JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
								WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
								AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
			END 
        END
		ELSE
        BEGIN
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
            BEGIN
              SET @strColumns = @strColumns
                              + ',CFW'
                              + CONVERT(VARCHAR(3),@tintOrder)
                              + '.Fld_Value  ['
                              + @vcColumnName + ']'
              SET @WhereCondition = @WhereCondition
                                      + ' left Join CFW_FLD_Values_Opp CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + ' on CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.Fld_Id='
                                      + CONVERT(VARCHAR(10),@numFieldId)
                                      + 'and CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.RecId=Opp.numOppid   '
			END
			ELSE
            IF @vcAssociatedControlType = 'CheckBox' 
            BEGIN
                SET @strColumns = @strColumns
                                + ',case when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=0 then 0 when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=1 then 1 end   ['
                                + @vcColumnName + ']'
                SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid   '
            END
            ELSE
            IF @vcAssociatedControlType = 'DateField'
            BEGIN
                  SET @strColumns = @strColumns
                                  + ',dbo.FormatedDateFromDate(CFW'
                                  + CONVERT(VARCHAR(3),@tintOrder)
                                  + '.Fld_Value,'
                                  + CONVERT(VARCHAR(10),@numDomainId)
                                  + ')  ['
                                  + @vcColumnName + ']'
                  SET @WhereCondition = @WhereCondition
                                          + ' left Join CFW_FLD_Values_Opp CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + ' on CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.Fld_Id='
                                          + CONVERT(VARCHAR(10),@numFieldId)
                                          + 'and CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.RecId=Opp.numOppid    '
            END
            ELSE
            IF @vcAssociatedControlType = 'SelectBox'
            BEGIN
                SET @vcDbColumnName = 'DCust'
                                            + CONVERT(VARCHAR(10),@numFieldId)
                SET @strColumns = @strColumns
                                + ',L'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.vcData'
                                + ' ['
                                + @vcColumnName + ']'

                SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid '

                SET @WhereCondition = @WhereCondition
                                        + ' left Join ListDetails L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.numListItemID=CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Value'
            END
			ELSE
			BEGIN
				SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID)') + ' [' + @vcColumnName + ']'
			END

			IF LEN(@SearchText) > 0
			BEGIN
				SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CONCAT('dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID) LIKE ''%',@SearchText,'%''')
			END
		END
      
     
		SELECT TOP 1 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
			@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 SET @tintOrder=0 
	END 

	

	DECLARE @strShareRedordWith AS VARCHAR(300);
	SET @strShareRedordWith=' Opp.numOppId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=3 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '


	DECLARE @StrSql AS VARCHAR(MAX) = ''

	SET @StrSql = @StrSql + ' FROM OpportunityMaster Opp            
								 ##PLACEHOLDER##                                                   
                                 INNER JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId                                                               
                                 INNER JOIN DivisionMaster Div ON Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID
								 INNER JOIN CompanyInfo cmp ON Div.numCompanyID = cmp.numCompanyId 
								 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId ' + @WhereCondition

	-------Change Row Color-------
	
	DECLARE @vcCSOrigDbCOlumnName AS VARCHAR(50) = ''
	DECLARE @vcCSLookBackTableName AS VARCHAR(50) = ''
	DECLARE @vcCSAssociatedControlType AS VARCHAR(50)

	CREATE TABLE #tempColorScheme
	(
		vcOrigDbCOlumnName VARCHAR(50),
		vcLookBackTableName VARCHAR(50),
		vcFieldValue VARCHAR(50),
		vcFieldValue1 VARCHAR(50),
		vcColorScheme VARCHAR(50),
		vcAssociatedControlType varchar(50)
	)

	INSERT INTO 
		#tempColorScheme 
	SELECT 
		DFM.vcOrigDbCOlumnName,
		DFM.vcLookBackTableName,
		DFCS.vcFieldValue,
		DFCS.vcFieldValue1,
		DFCS.vcColorScheme,
		DFFM.vcAssociatedControlType 
	FROM 
		DycFieldColorScheme DFCS 
	JOIN 
		DycFormField_Mapping DFFM 
	ON 
		DFCS.numFieldID=DFFM.numFieldID
	JOIN 
		DycFieldMaster DFM 
	ON 
		DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
	WHERE
		DFCS.numDomainID=@numDomainID 
		AND DFFM.numFormID=@numFormId 
		AND DFCS.numFormID=@numFormId 
		AND isnull(DFFM.bitAllowGridColor,0)=1

	IF(SELECT COUNT(*) FROM #tempColorScheme)>0
	BEGIN
		SELECT TOP 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
	END                        

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		SET @strColumns=@strColumns + ',tCS.vcColorScheme'
	END

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		IF @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
			SET @Prefix = 'ADC.'                  
		IF @vcCSLookBackTableName = 'DivisionMaster'                  
			SET @Prefix = 'Div.'
		if @vcCSLookBackTableName = 'CompanyInfo'                  
			SET @Prefix = 'CMP.'   
		if @vcCSLookBackTableName = 'OpportunityMaster'                  
			SET @Prefix = 'Opp.'   
 
		IF @vcCSAssociatedControlType='DateField'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS ON CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
				 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
		END
		ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
		END
	END
                          
	IF @columnName like 'CFW.Cust%'             
	BEGIN            
		SET @strSql = @strSql + ' LEFT JOIN CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid and CFW.fld_id= '+ REPLACE(@columnName,'CFW.Cust','') +' '                                                     
		SET @columnName='CFW.Fld_Value'            
	END 

	IF @bitPartner = 1
		SET @strSql = @strSql + ' LEFT JOIN OpportunityContact OppCont ON OppCont.numOppId=Opp.numOppId AND OppCont.bitPartner=1 AND OppCont.numContactId=' + CONVERT(VARCHAR(15),@numUserCntID)
                    
                
	IF @tintFilterBy = 1 --Partially Fulfilled Orders 
		SET @strSql = @strSql + ' LEFT JOIN AdditionalContactsInformation ADC1 ON ADC1.numContactId=Opp.numRecOwner                                                               
                             LEFT OUTER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = Opp.[numOppId] 
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and (Opp.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped) + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)'
	ELSE
		SET @strSql = @strSql + ' LEFT JOIN AdditionalContactsInformation ADC1 on ADC1.numContactId=Opp.numRecOwner                                                               
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and (Opp.tintShipped='+ CONVERT(VARCHAR(15),@tintShipped)  + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)'
  
  
	IF @intOrder <> 0
    BEGIN
      IF @intOrder = 1
        SET @strSql = @strSql + ' and Opp.bitOrder = 0 '
      IF @intOrder = 2
        SET @strSql = @strSql + ' and Opp.bitOrder = 1'
    END
    
	IF @numCompanyID <> 0
		SET @strSql = @strSql + ' And Div.numCompanyID ='+ CONVERT(VARCHAR(15),@numCompanyID)

	IF @SortChar <> '0' 
		SET @strSql = @strSql + ' And Opp.vcPOppName like ''' + @SortChar + '%'''
  

	IF @inttype = 3
	BEGIN
		IF @tintSalesUserRightType = 0
			SET @strSql = @strSql + ' AND opp.tintOppType=0'
		ELSE IF @tintSalesUserRightType = 1
			SET @strSql = @strSql + ' AND (Opp.numRecOwner= ' + CONVERT(VARCHAR(15),@numUserCntID)
						+ ' or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
						+ CASE WHEN @bitPartner = 1 THEN ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+ CONVERT(VARCHAR(15),@numUserCntID) + ')' ELSE '' END
						+ ' or ' + @strShareRedordWith +')'
		ELSE IF @tintSalesUserRightType = 2
			SET @strSql = @strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) 
									 or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
									+ ' or ' + @strShareRedordWith +')'
	END
	ELSE IF @inttype = 4
	BEGIN
		IF @tintPurchaseUserRightType = 0
			SET @strSql = @strSql + ' AND opp.tintOppType=0'
		ELSE IF @tintPurchaseUserRightType = 1
			SET @strSql = @strSql + ' AND (Opp.numRecOwner= ' + CONVERT(VARCHAR(15),@numUserCntID)
						+ ' or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
						+ CASE WHEN @bitPartner = 1 THEN ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+ CONVERT(VARCHAR(15),@numUserCntID) + ')' ELSE '' END
						+ ' or ' + @strShareRedordWith +')'
		ELSE IF @tintPurchaseUserRightType = 2
			SET @strSql = @strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) 
									 or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
									+ ' or ' + @strShareRedordWith +')'
	END

	
					
	IF @tintSortOrder <> '0'
		SET @strSql = @strSql + '  AND Opp.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder)
    
    
	IF @tintFilterBy = 1 --Partially Fulfilled Orders
		SET @strSql = @strSql + ' AND OB.bitPartialFulfilment = 1 AND OB.bitAuthoritativeBizDocs = 1 '
	ELSE IF @tintFilterBy = 2 --Fulfilled Orders
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT OM.numOppId FROM [OpportunityMaster] OM
                      			INNER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = OM.[numOppId]
                      			INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId]
                      			INNER JOIN [OpportunityBizDocItems] BI ON BI.[numOppBizDocID] = OB.[numOppBizDocsId]
                      			Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + 'AND OB.[bitAuthoritativeBizDocs] = 1 GROUP BY OM.[numOppId]
                      			HAVING   COUNT(OI.[numUnitHour]) = COUNT(BI.[numUnitHour])) '
	ELSE IF @tintFilterBy = 3 --Orders without Authoritative-BizDocs
		SET @strSql = @strSql + ' AND Opp.[numOppId] not IN (SELECT DISTINCT OM.[numOppId]
                      		  FROM [OpportunityMaster] OM JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		  Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OM.tintOppstatus=1 AND (OM.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped) + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)' + ' AND OM.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder) + ' AND isnull(OB.[bitAuthoritativeBizDocs],0)=1) '
	ELSE IF @tintFilterBy = 4 --Orders with items yet to be added to Invoices
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT distinct OPPM.numOppId FROM [OpportunityMaster] OPPM
                                  INNER JOIN [OpportunityItems] OPPI ON OPPM.[numOppId] = OPPI.[numOppId]
                                  Where OPPM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' 
								  and OPPI.numoppitemtCode not in (select numOppItemID from [OpportunityBizDocs] OB INNER JOIN [OpportunityBizDocItems] BI
                      			  ON BI.[numOppBizDocID] = OB.[numOppBizDocsId] where OB.[numOppId] = OPPM.[numOppId]))'      
	ELSE IF @tintFilterBy = 6 --Orders with deferred Income/Invoices
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT DISTINCT OM.[numOppId]
                      		 FROM [OpportunityMaster] OM LEFT OUTER JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		 Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OB.[bitAuthoritativeBizDocs] =1 and OB.tintDeferred=1)'

	IF @numOrderStatus <>0 
		SET @strSql = @strSql + ' AND Opp.numStatus = ' + CONVERT(VARCHAR(15),@numOrderStatus);

	IF CHARINDEX('vcInventoryStatus',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('vcInventoryStatus=1',@vcRegularSearchCriteria) > 0
			SET @strSql = @strSql + ' AND ' + ' CHARINDEX(''Not Applicable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 '
		IF CHARINDEX('vcInventoryStatus=2',@vcRegularSearchCriteria) > 0
			SET @strSql = @strSql + ' AND ' + ' CHARINDEX(''Allocation Cleared'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 '
		IF CHARINDEX('vcInventoryStatus=3',@vcRegularSearchCriteria) > 0
			SET @strSql = @strSql + ' AND ' + ' CHARINDEX(''Back Order'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 '
		IF CHARINDEX('vcInventoryStatus=4',@vcRegularSearchCriteria) > 0
			SET @strSql = @strSql + ' AND ' + ' CHARINDEX(''Ready to Ship'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 '
	END
	ELSE IF @vcRegularSearchCriteria<>'' 
	BEGIN
		SET @strSql=@strSql +' AND ' + @vcRegularSearchCriteria 
	END
	
	IF @vcCustomSearchCriteria<>'' 
	BEGIN
		SET @strSql = @strSql +' AND ' +  @vcCustomSearchCriteria
	END

	IF LEN(@SearchQuery) > 0
	BEGIN
		SET @strSql = @strSql + ' AND (' + @SearchQuery + ') '
	END

	

	DECLARE  @firstRec  AS INTEGER
	DECLARE  @lastRec  AS INTEGER
  
	SET @firstRec = (@CurrentPage - 1) * @PageSize
	SET @lastRec = (@CurrentPage * @PageSize + 1)



	-- EXECUTE FINAL QUERY
	DECLARE @strFinal AS NVARCHAR(MAX)
	SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, Opp.numOppID INTO #temp2', REPLACE(@strSql,'##PLACEHOLDER##',''),'; SELECT ID,',@strColumns,' INTO #tempTable',REPLACE(@strSql,'##PLACEHOLDER##',' JOIN #temp2 tblAllOrders ON Opp.numOppID = tblAllOrders.numOppID '),' AND tblAllOrders.ID > ',@firstRec,' and tblAllOrders.ID <',@lastRec,' ORDER BY ID; SELECT * FROM  #tempTable; DROP TABLE #tempTable; SELECT @TotalRecords = COUNT(*) FROM #temp2; DROP TABLE #temp2;')
	--SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, ',@strColumns,' INTO #tempTable',@strSql,'; SELECT * FROM #tempTable WHERE ID > ',@firstRec,' and ID <',@lastRec,' ORDER BY ID; SELECT @TotalRecords = COUNT(*) FROM #tempTable; DROP TABLE #tempTable;')

	PRINT @strFinal
	EXEC sp_executesql @strFinal, N'@TotalRecords INT OUT', @TotRecs OUT

	SELECT * FROM #tempForm

	DROP TABLE #tempForm

	DROP TABLE #tempColorScheme
--created by anoop jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdomaindetails')
DROP PROCEDURE usp_getdomaindetails
GO
CREATE PROCEDURE [dbo].[USP_GetDomainDetails]                                          
@numDomainID as numeric(9)=0                                          
as           

DECLARE @listIds VARCHAR(MAX)
SELECT @listIds = COALESCE(@listIds+',' ,'') + CAST(numUserID AS VARCHAR(100)) FROM UnitPriceApprover WHERE numDomainID = @numDomainID
   
DECLARE @canChangeDiferredIncomeSelection AS BIT = 1

IF EXISTS(SELECT 
			OB.numOppBizDocsId 
		FROM 
			OpportunityBizDocs OB 
		INNER JOIN 
			OpportunityMaster OM 
		ON 
			OB.numOppId=OM.numOppId 
		WHERE 
			numBizDocId=304 
			AND OM.numDomainId=@numDomainID)
BEGIN
	SET @canChangeDiferredIncomeSelection = 0
END   
   
                               
select                                           
vcDomainName,                                           
isnull(vcDomainDesc,'') vcDomainDesc,                                           
isnull(bitExchangeIntegration,0) bitExchangeIntegration,                                           
isnull(bitAccessExchange,0) bitAccessExchange,                                           
isnull(vcExchUserName,'') vcExchUserName,                                           
isnull(vcExchPassword,'') vcExchPassword,                                           
isnull(vcExchPath,'') vcExchPath,                                           
isnull(vcExchDomain,'') vcExchDomain,                                         
tintCustomPagingRows,                                         
vcDateFormat,                                         
numDefCountry,                                         
tintComposeWindow,                                         
sintStartDate,                                         
sintNoofDaysInterval,                                         
tintAssignToCriteria,                                         
bitIntmedPage,
ISNULL(D.numCost,0) as numCost,                                         
tintFiscalStartMonth,                                    
--isnull(bitDeferredIncome,0) as bitDeferredIncome,                                  
isnull(tintPayPeriod,0) as tintPayPeriod,                                
isnull(numCurrencyID,0) as numCurrencyID,                            
isnull(vcCurrency,'') as vcCurrency,                      
isnull(charUnitSystem,'') as charUnitSystem ,                    
isnull(vcPortalLogo,'') as vcPortalLogo,                  
isnull(tintChrForComSearch,2) as tintChrForComSearch,                
isnull(tintChrForItemSearch,'') as tintChrForItemSearch,            
isnull(intPaymentGateWay,0) as intPaymentGateWay  ,        
 case when bitPSMTPServer= 1 then vcPSMTPServer else '' end as vcPSMTPServer  ,              
 case when bitPSMTPServer= 1 then  isnull(numPSMTPPort,0)  else 0 end as numPSMTPPort          
,isnull(bitPSMTPAuth,0) bitPSMTPAuth        
,isnull([vcPSmtpPassword],'')vcPSmtpPassword        
,isnull(bitPSMTPSSL,0) bitPSMTPSSL     
,isnull(bitPSMTPServer,0) bitPSMTPServer  ,isnull(vcPSMTPUserName,'') vcPSMTPUserName,      
--isnull(numDefaultCategory,0) numDefaultCategory,
isnull(numShipCompany,0)   numShipCompany,
isnull(bitAutoPopulateAddress,0) bitAutoPopulateAddress,
isnull(tintPoulateAddressTo,0) tintPoulateAddressTo,
isnull(bitMultiCompany,0) bitMultiCompany,
isnull(bitMultiCurrency,0) bitMultiCurrency,
ISNULL(bitCreateInvoice,0) bitCreateInvoice,
ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
ISNULL([bitSaveCreditCardInfo],0) bitSaveCreditCardInfo,
ISNULL([intLastViewedRecord],10) intLastViewedRecord,
ISNULL([tintCommissionType],1) tintCommissionType,
ISNULL([tintBaseTax],2) tintBaseTax,
ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
ISNULL(bitEmbeddedCost,0) bitEmbeddedCost,
ISNULL(numDefaultSalesOppBizDocId,0) numDefaultSalesOppBizDocId,
ISNULL(tintSessionTimeOut,1) tintSessionTimeOut,
ISNULL(tintDecimalPoints,2) tintDecimalPoints,
ISNULL(bitCustomizePortal,0) bitCustomizePortal,
ISNULL(tintBillToForPO,0) tintBillToForPO,
ISNULL(tintShipToForPO,0) tintShipToForPO,
ISNULL(tintOrganizationSearchCriteria,1) tintOrganizationSearchCriteria,
ISNULL(tintLogin,0) tintLogin,
lower(ISNULL(vcPortalName,'')) vcPortalName,
ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
D.numDomainID,
ISNULL(vcPSMTPDisplayName,'') vcPSMTPDisplayName,
ISNULL(bitGtoBContact,0) bitGtoBContact,
ISNULL(bitBtoGContact,0) bitBtoGContact,
ISNULL(bitGtoBCalendar,0) bitGtoBCalendar,
ISNULL(bitBtoGCalendar,0) bitBtoGCalendar,
ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
ISNULL(bitInlineEdit,0) bitInlineEdit,
ISNULL(bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
ISNULL(bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
isnull(D.bitRentalItem,0) as bitRentalItem,
isnull(D.numRentalItemClass,0) as numRentalItemClass,
isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
ISNULL(D.bitAutoSerialNoAssign,0) as bitAutoSerialNoAssign,
ISNULL(D.bitIsShowBalance,0) AS [bitIsShowBalance],
ISNULL(numIncomeAccID,0) AS [numIncomeAccID],
ISNULL(numCOGSAccID,0) AS [numCOGSAccID],
ISNULL(numAssetAccID,0) AS [numAssetAccID],
ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
ISNULL(D.vcShipToPhoneNo,0) AS vcShipToPhoneNo,
ISNULL(D.vcGAUserEMail,'') AS vcGAUserEMail,
ISNULL(D.vcGAUserPassword,'') AS vcGAUserPassword,
ISNULL(D.vcGAUserProfileId,0) AS vcGAUserProfileId,
ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
ISNULL(D.vcHideTabs,'') AS vcHideTabs,
ISNULL(D.bitUseBizdocAmount,0) AS bitUseBizdocAmount,
ISNULL(D.bitDefaultRateType,0) AS bitDefaultRateType,
ISNULL(D.bitIncludeTaxAndShippingInCommission,0) AS bitIncludeTaxAndShippingInCommission,
/*,ISNULL(D.bitAllowPPVariance,0) AS bitAllowPPVariance*/
ISNULL(D.bitPurchaseTaxCredit,0) AS bitPurchaseTaxCredit,
ISNULL(D.bitLandedCost,0) AS bitLandedCost,
ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
ISNULL(D.numAbovePercent,0) AS numAbovePercent,
ISNULL(D.numBelowPercent,0) AS numBelowPercent,
ISNULL(D.numBelowPriceField,0) AS numBelowPriceField,
@listIds AS vcUnitPriceApprover,
ISNULL(D.numDefaultSalesPricing,2) AS numDefaultSalesPricing,
ISNULL(D.bitReOrderPoint,0) AS bitReOrderPoint,
ISNULL(D.numReOrderPointOrderStatus,0) AS numReOrderPointOrderStatus,
ISNULL(D.numPODropShipBizDoc,0) AS numPODropShipBizDoc,
ISNULL(D.numPODropShipBizDocTemplate,0) AS numPODropShipBizDocTemplate,
ISNULL(D.IsEnableDeferredIncome,0) AS IsEnableDeferredIncome,
ISNULL(D.bitApprovalforTImeExpense,0) AS bitApprovalforTImeExpense,
ISNULL(D.intTimeExpApprovalProcess,0) AS intTimeExpApprovalProcess,
ISNULL(D.bitApprovalforOpportunity,0) AS bitApprovalforOpportunity,
ISNULL(D.intOpportunityApprovalProcess,0) AS intOpportunityApprovalProcess,
@canChangeDiferredIncomeSelection AS canChangeDiferredIncomeSelection,
ISNULL(CAST(D.numDefaultSalesShippingDoc AS int),0) AS numDefaultSalesShippingDoc,
ISNULL(CAST(D.numDefaultPurchaseShippingDoc AS int),0) AS numDefaultPurchaseShippingDoc,
ISNULL(D.bitchkOverRideAssignto,0) AS bitchkOverRideAssignto,
ISNULL(D.vcPrinterIPAddress,'') vcPrinterIPAddress,
ISNULL(D.vcPrinterPort,'') vcPrinterPort,
ISNULL(bitCostApproval,0) bitCostApproval
,ISNULL(bitListPriceApproval,0) bitListPriceApproval
,ISNULL(bitMarginPriceViolated,0) bitMarginPriceViolated
,ISNULL(vcSalesOrderTabs,'Sales Orders') AS vcSalesOrderTabs
,ISNULL(vcSalesQuotesTabs,'Sales Quotes') AS vcSalesQuotesTabs
,ISNULL(vcItemPurchaseHistoryTabs,'Item Purchase History') AS vcItemPurchaseHistoryTabs
,ISNULL(vcItemsFrequentlyPurchasedTabs,'Items Frequently Purchased') AS vcItemsFrequentlyPurchasedTabs
,ISNULL(vcOpenCasesTabs,'Open Cases') AS vcOpenCasesTabs
,ISNULL(vcOpenRMATabs,'Open RMAs') AS vcOpenRMATabs
,ISNULL(bitSalesOrderTabs,0) AS bitSalesOrderTabs
,ISNULL(bitSalesQuotesTabs,0) AS bitSalesQuotesTabs
,ISNULL(bitItemPurchaseHistoryTabs,0) AS bitItemPurchaseHistoryTabs
,ISNULL(bitItemsFrequentlyPurchasedTabs,0) AS bitItemsFrequentlyPurchasedTabs
,ISNULL(bitOpenCasesTabs,0) AS bitOpenCasesTabs
,ISNULL(bitOpenRMATabs,0) AS bitOpenRMATabs

,ISNULL(numDefaultSiteID,0) AS numDefaultSiteID
,ISNULL(tintOppStautsForAutoPOBackOrder,0) AS tintOppStautsForAutoPOBackOrder
,ISNULL(tintUnitsRecommendationForAutoPOBackOrder,1) AS tintUnitsRecommendationForAutoPOBackOrder
from Domain D  
left join eCommerceDTL eComm  
on eComm.numDomainID=D.numDomainID  
where (D.numDomainID=@numDomainID OR @numDomainID=-1)


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetMirrorBizDocDetails' ) 
    DROP PROCEDURE USP_GetMirrorBizDocDetails
GO

-- EXEC USP_GetMirrorBizDocDetails 53,8,1,1,0
CREATE PROCEDURE [dbo].[USP_GetMirrorBizDocDetails]
    (
      @numReferenceID NUMERIC(9) = 0,
      @numReferenceType TINYINT,
      @numDomainId NUMERIC(9),
      @numUserCntID NUMERIC(9),
      @ClientTimeZoneOffset INT
    )
AS 
    BEGIN
--        DECLARE @numBizDocID AS NUMERIC
        DECLARE @numBizDocType AS NUMERIC
        
        IF @numReferenceType = 1
			SELECT  @numBizDocType = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Sales Order' AND constFlag=1
        ELSE IF @numReferenceType = 2
			SELECT  @numBizDocType = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Purchase Order' AND constFlag=1
        ELSE IF @numReferenceType = 3
			SELECT  @numBizDocType = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Sales Opportunity' AND constFlag=1
        ELSE IF @numReferenceType = 4
			SELECT  @numBizDocType = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Purchase Opportunity' AND constFlag=1
        ELSE IF @numReferenceType = 5 
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='RMA' AND constFlag=1
		ELSE IF @numReferenceType = 6 
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='RMA' AND constFlag=1 
		ELSE IF @numReferenceType = 7 
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='Sales Credit Memo' AND constFlag=1 
		ELSE IF @numReferenceType = 8
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='Purchase Credit Memo' AND constFlag=1 
		ELSE IF @numReferenceType = 9
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='Refund Receipt' AND constFlag=1 
		ELSE IF @numReferenceType = 10
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='Credit Memo' AND constFlag=1 
		ELSE IF @numReferenceType = 11
			SELECT  @numBizDocType = numListItemID FROM dbo.ListDetails WHERE vcData='Invoice' AND constFlag=1
	 
        IF @numReferenceType = 1 OR @numReferenceType = 2 OR @numReferenceType = 3 OR @numReferenceType = 4 
		BEGIN
		      
			---------------------------------------------------------------------------------------
      
				  SELECT Mst.vcPOppName AS vcBizDocID,
						 Mst.vcPOppName AS OppName,Mst.vcPOppName AS BizDcocName,@numBizDocType AS BizDoc,ISNULL(Mst.numRecOwner,0) AS Owner,
						 CMP.VcCompanyName AS CompName,Mst.numContactID,Mst.numCreatedBy AS BizDocOwner,
						 Mst.fltDiscount AS decDiscount,
						 (SELECT SUM(ISNULL(monAmountPaid,0)) FROM OpportunityBizDocs WHERE numOppId=MSt.numOppId) AS monAmountPaid,
						 isnull(Mst.txtComments,'') AS vcComments,
						 CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffset,Mst.bintCreatedDate)) dtCreatedDate,
						 dbo.fn_GetContactName(Mst.numCreatedby) AS numCreatedby,
						 dbo.fn_GetContactName(Mst.numModifiedBy) AS numModifiedBy,
						 Mst.bintModifiedDate AS dtModifiedDate,
						 '' AS numViewedBy,
						 NULL AS dtViewedDate,
						 isnull(Mst.bitBillingTerms,0) AS tintBillingTerms,
						 isnull(Mst.intBillingDays,0) AS numBillingDays,
						 ISNULL(BTR.numNetDueInDays,0) AS numBillingDaysName,
						 BTR.vcTerms AS vcBillingTermsName,
						 isnull(Mst.bitInterestType,0) AS tintInterestType,
						 isnull(Mst.fltInterest,0) AS fltInterest,
						 tintOPPType,
						 @numBizDocType AS numBizDocId,
						 Mst.bintAccountClosingDate,
						 tintShipToType,
						 tintBillToType,
						 tintshipped,
						 NULL bintShippedDate,
						 ISNULL(Mst.monShipCost ,0) AS monShipCost,
						 ISNULL(BT.vcBizDocFooter,D.vcBizDocFooter) AS BizdocFooter,
						 ISNULL(BT.vcBizDocImagePath,D.vcBizDocImagePath) As vcBizDocImagePath, 
						 Mst.numDivisionID,
						  ISNULL(BT.vcPurBizDocFooter,D.vcPurBizDocFooter) AS PurBizdocFooter,
						 isnull(Mst.fltDiscount,0) AS fltDiscount,
						 isnull(Mst.bitDiscountType,0) AS bitDiscountType,
						 0 AS numShipVia,
						 '' AS vcTrackingURL,
						 0 AS numBizDocStatus,
						 '' AS BizDocStatus,
						 '' AS ShipVia,
						 ISNULL(C.varCurrSymbol,'') varCurrSymbol,
						 0 AS ShippingReportCount,
						 0 bitPartialFulfilment,
						 vcPOppName as vcBizDocName,
						 dbo.[fn_GetContactName](Mst.[numModifiedBy])  + ', '
							+ CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Mst.bintModifiedDate)) AS ModifiedBy,
						 dbo.[fn_GetContactName](Mst.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](Mst.[numAssignedTo]) AS OrderRecOwner,
						 dbo.[fn_GetContactName](DM.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](DM.[numAssignedTo]) AS AccountRecOwner,
						 Mst.bintCreatedDate AS dtFromDate,
						 Mst.tintTaxOperator,
						 0 AS monTotalEmbeddedCost,
						 0 AS monTotalAccountedCost,
						 ISNULL(BT.bitEnabled,0) bitEnabled,
						 ISNULL(BT.txtBizDocTemplate,'') txtBizDocTemplate, 
						 ISNULL(BT.txtCSS,'') txtCSS,
						 dbo.fn_GetContactName(Mst.numAssignedTo) AS AssigneeName,
						 ISNULL((SELECT vcEmail FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneeEmail,
						 ISNULL((SELECT numPhone FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneePhone,
						 dbo.fn_GetComapnyName(Mst.numDivisionId) OrganizationName,
						 dbo.fn_GetContactName(Mst.numContactID)  OrgContactName,
						 case when ACI.numPhone<>'' then ACI.numPhone +case when ACI.numPhoneExtension<>'' then ' - ' + ACI.numPhoneExtension else '' end  else '' END OrgContactPhone,
						 DM.vcComPhone as OrganizationPhone,
						 ISNULL(ACI.vcEmail,'') AS OrgContactEmail,
 						 dbo.[fn_GetContactName](Mst.[numRecOwner]) AS OnlyOrderRecOwner,
 						 0 bitAuthoritativeBizDocs,
						0 tintDeferred,0 AS monCreditAmount,
						 0 as bitRentalBizDoc,isnull(BT.numBizDocTempID,0) as numBizDocTempID,isnull(BT.vcTemplateName,'') as vcTemplateName,
						 [dbo].[GetDealAmount](Mst.numOppId ,getutcdate(),0 ) as monPAmount,
						 --(select top 1 SD.vcSignatureFile from SignatureDetail SD join OpportunityBizDocsDetails OBD on SD.numSignID=OBD.numSignID where SD.numDomainID = @numDomainID and OBD.numDomainID = @numDomainID and OBD.numSignID IS not null and OBD.numBizDocsId=opp.numOppBizDocsId order by OBD.numBizDocsPaymentDetId desc) as vcSignatureFile,
						 isnull(CMP.txtComments,'') as vcOrganizationComments,
						 ISNULL((SELECT SUBSTRING((SELECT '$^$' + ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=MSt.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OBD.numShipVia  AND vcFieldName='Tracking URL')),'') +'#^#'+ RTRIM(LTRIM(vcTrackingNo) )  FROM dbo.OpportunityBizDocs OBD WHERE OBD.numOppId=Mst.numOppID FOR XML PATH('')),4,200000)),'') AS  vcTrackingNo,'' AS  vcShippingMethod,
						 NULL AS dtDeliveryDate,ISNULL(Mst.vcOppRefOrderNo,'') AS vcOppRefOrderNo,ISNULL(Mst.numDiscountAcntType,0) AS numDiscountAcntType ,
						 isnull(numOppBizDocTempID,0) as numOppBizDocTempID,
						 com1.vcCompanyName AS EmployerOrganizationName,div1.vcComPhone as EmployerOrganizationPhone,dbo.FormatedDateFromDate(Mst.dtReleaseDate,@numDomainID) AS vcReleaseDate
						 ,(CASE WHEN ISNULL(Mst.numPartner,0) > 0 THEN ISNULL((SELECT CONCAT(DivisionMaster.vcPartnerCode,'-',CompanyInfo.vcCompanyName) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE DivisionMaster.numDivisionID=Mst.numPartner),'') ELSE '' END) AS vcPartner
				  FROM    OpportunityMaster Mst JOIN Domain D ON D.numDomainID = Mst.numDomainID
						  LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = Mst.numCurrencyID
						  LEFT JOIN [DivisionMaster] DM ON DM.numDivisionID = Mst.numDivisionID   
						  LEFT JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = @numDomainID  
						  LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = Mst.numContactID
						  LEFT JOIN [BizDocTemplate] BT ON BT.numDomainID = @numDomainID AND BT.numBizDocID = @numBizDocType 
							 AND ((BT.numBizDocTempID=ISNULL(Mst.numOppBizDocTempID, 0)) OR (ISNULL(BT.bitDefault,0)=1 AND ISNULL(BT.bitEnabled,0)=1))  
						  LEFT JOIN BillingTerms BTR ON BTR.numTermsID = ISNULL(Mst.intBillingDays,0)
						  JOIN divisionmaster div1 ON D.numDivisionID = div1.numDivisionID
						  JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
				  WHERE  Mst.numOppID = @numReferenceID AND Mst.numDomainID = @numDomainID
			             
						 EXEC USP_OPPGetOppAddressDetails @numReferenceID,@numDomainId
             
            END
    
    
        ELSE IF @numReferenceType = 5 OR @numReferenceType = 6 OR @numReferenceType = 7 OR @numReferenceType = 8 
					OR @numReferenceType = 9 OR @numReferenceType = 10
         BEGIN
         
								DECLARE @numBizDocTempID AS NUMERIC(9)
								DECLARE @numDivisionID AS NUMERIC(9)
         
                                SELECT @numDivisionID=numDivisionID,@numBizDocTempID=(CASE WHEN @numReferenceType = 5 OR @numReferenceType = 6 OR @numReferenceType = 9 OR @numReferenceType = 10 THEN numRMATempID ELSE numBizDocTempID END)				
                                FROM    dbo.ReturnHeader RH
                                WHERE   RH.numReturnHeaderID = @numReferenceID
                                
    			---------------------
               
                                SELECT  RH.vcBizDocName AS [vcBizDocID],
                                  RH.vcRMA AS OppName,RH.vcBizDocName AS BizDcocName,@numBizDocType AS BizDoc,ISNULL(RH.numCreatedBy,0) AS Owner,
								   CMP.VcCompanyName AS CompName,RH.numContactID,RH.numCreatedBy AS BizDocOwner,
                                        RH.monTotalDiscount AS decDiscount,
                                        ISNULL(RDA.monAmount + RDA.monTotalTax - RDA.monTotalDiscount, 0) AS monAmountPaid,
                                        ISNULL(RH.vcComments, '') AS vcComments,
                                        CONVERT(VARCHAR(20), DATEADD(minute, -@ClientTimeZoneOffset, RH.dtCreatedDate)) dtCreatedDate,
                                        dbo.fn_GetContactName(RH.numCreatedby) AS numCreatedby,
                                        dbo.fn_GetContactName(RH.numModifiedBy) AS numModifiedBy,
                                        RH.dtModifiedDate,
                                        '' AS numViewedBy,
                                        '' AS dtViewedDate,
                                        0 AS tintBillingTerms,
                                        0 AS numBillingDays,
                                        0 AS [numBillingDaysName],
                                        '' AS vcBillingTermsName,
                                        0 AS tintInterestType,
                                        0 AS fltInterest,
                                        tintReturnType AS tintOPPType,
                                        @numBizDocType AS numBizDocId,
                                        dbo.fn_GetContactName(RH.numCreatedby) AS ApprovedBy,
                                        dtCreatedDate AS dtApprovedDate,
                                        0 AS bintAccountClosingDate,
                                        0 AS tintShipToType,
                                        0 AS tintBillToType,
                                        0 AS tintshipped,
                                        0 AS bintShippedDate,
                                        0 AS monShipCost,
                                        ISNULL(BT.vcBizDocFooter,D.vcBizDocFooter) AS BizdocFooter,
                                        ISNULL(BT.vcBizDocImagePath,D.vcBizDocImagePath) As vcBizDocImagePath,
                                        RH.numDivisionID,
                                         ISNULL(BT.vcPurBizDocFooter,D.vcPurBizDocFooter) AS PurBizdocFooter,
                                        ISNULL(RH.monTotalDiscount, 0) AS fltDiscount,
                                        0 AS bitDiscountType,
                                        0 AS numShipVia,
                                        '' AS vcTrackingURL,
                                        0 AS numBizDocStatus,
                                        '-' AS BizDocStatus,
                                        '-' AS ShipVia,
                                        ISNULL(C.varCurrSymbol, '') varCurrSymbol,
                                        0 AS ShippingReportCount,
                                        0 bitPartialFulfilment,
                                        vcBizDocName,
                                        dbo.[fn_GetContactName](RH.[numModifiedBy])
                                        + ', '
                                        + CONVERT(VARCHAR(20), DATEADD(MINUTE, -@ClientTimeZoneOffset, RH.dtModifiedDate)) AS ModifiedBy,
                                        dbo.[fn_GetContactName](RH.[numCreatedBy]) AS OrderRecOwner,
                                        dbo.[fn_GetContactName](DM.[numRecOwner])
                                        + ', '
                                        + dbo.[fn_GetContactName](DM.[numAssignedTo]) AS AccountRecOwner,
                                        RH.dtcreatedDate AS dtFromDate,
                                        0 AS tintTaxOperator,
                                        1 AS monTotalEmbeddedCost,
                                        2 AS monTotalAccountedCost,
                                        ISNULL(BT.bitEnabled, 0) bitEnabled,
                                        ISNULL(BT.txtBizDocTemplate, '') txtBizDocTemplate,
                                        ISNULL(BT.txtCSS, '') txtCSS,
                                        '' AS AssigneeName,
                                        '' AS AssigneeEmail,
                                        '' AS AssigneePhone,
                                        dbo.fn_GetComapnyName(RH.numDivisionId) OrganizationName,
                                        dbo.fn_GetContactName(RH.numContactID) OrgContactName,
                                        dbo.getCompanyAddress(RH.numDivisionId, 1,RH.numDomainId) CompanyBillingAddress,
                                        CASE WHEN ACI.numPhone <> ''
                                             THEN ACI.numPhone
                                                  + CASE WHEN ACI.numPhoneExtension <> ''
                                                         THEN ' - ' + ACI.numPhoneExtension
                                                         ELSE ''
                                                    END
                                             ELSE ''
                                        END OrgContactPhone,
                                        DM.vcComPhone AS OrganizationPhone,
                                        ISNULL(ACI.vcEmail, '') AS OrgContactEmail,
                                        dbo.[fn_GetContactName](RH.[numCreatedBy]) AS OnlyOrderRecOwner,
                                        1 bitAuthoritativeBizDocs,
                                        0 tintDeferred,
                                        0 AS monCreditAmount,
                                        0 AS bitRentalBizDoc,
                                        ISNULL(BT.numBizDocTempID, 0) AS numBizDocTempID,
                                        ISNULL(BT.vcTemplateName, '') AS vcTemplateName,
                                        0 AS monPAmount,
                                        ISNULL(CMP.txtComments, '') AS vcOrganizationComments,
                                        '' AS vcTrackingNo,
                                        '' AS vcShippingMethod,
                                        '' AS dtDeliveryDate,
                                        '' AS vcOppRefOrderNo,
                                        0 AS numDiscountAcntType ,
										0 AS numOppBizDocTempID
                                        ,com1.vcCompanyName AS EmployerOrganizationName,div1.vcComPhone as EmployerOrganizationPhone
                                FROM    dbo.ReturnHeader RH
                                        JOIN Domain D ON D.numDomainID = RH.numDomainID
                                        JOIN [DivisionMaster] DM ON DM.numDivisionID = RH.numDivisionID
                                        LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = DM.numCurrencyID
                                        LEFT JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId
                                                                     AND CMP.numDomainID = @numDomainID
                                        LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = RH.numContactID
                                        LEFT JOIN [BizDocTemplate] BT ON BT.numDomainID = @numDomainID
                                                                         AND BT.numBizDocID = @numBizDocType	
                                                                         AND ((BT.numBizDocTempID=ISNULL(@numBizDocTempID, 0)) OR (ISNULL(BT.bitDefault,0)=1 AND ISNULL(BT.bitEnabled,0)=1))  
                                         JOIN divisionmaster div1 ON D.numDivisionID = div1.numDivisionID
										 JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID,
                                                                         dbo.GetReturnDealAmount(@numReferenceID,@numReferenceType) RDA
                                WHERE   RH.numReturnHeaderID = @numReferenceID
                                
                                	--************Customer/Vendor Billing Address************
                                		SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ' ,' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
											isnull(AD.vcStreet,'') AS vcStreet,
											isnull(AD.vcCity,'') AS vcCity,
											isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
											isnull(AD.vcPostalCode,'') AS vcPostalCode,
											isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
											ISNULL(AD.vcAddressName,'') AS vcAddressName,
            								CMP.vcCompanyName
										 FROM AddressDetails AD 
										 JOIN [DivisionMaster] DM ON DM.numDivisionID = AD.numRecordID   
										 JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID  
										 --LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = @numContactID AND ACI.numDivisionId=DM.numDivisionID
										 WHERE AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
										 AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1

                                        
                                	--************Customer/Vendor Shipping Address************
                                	SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ' ,' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
											isnull(AD.vcStreet,'') AS vcStreet,
											isnull(AD.vcCity,'') AS vcCity,
											isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
											isnull(AD.vcPostalCode,'') AS vcPostalCode,
											isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
											ISNULL(AD.vcAddressName,'') AS vcAddressName,
            								CMP.vcCompanyName
										 FROM AddressDetails AD 
										 JOIN [DivisionMaster] DM ON DM.numDivisionID = AD.numRecordID   
										 JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID  
										 --LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = @numContactID AND ACI.numDivisionId=DM.numDivisionID
										 WHERE AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
										 AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1

                                --************Employer Shipping Address************ 
									SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ' ,' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
											isnull(AD.vcStreet,'') AS vcStreet,
											isnull(AD.vcCity,'') AS vcCity,
											isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
											isnull(AD.vcPostalCode,'') AS vcPostalCode,
											isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
											ISNULL(AD.vcAddressName,'') AS vcAddressName,vcCompanyName
									 FROM Domain D1 JOIN divisionmaster div1 ON D1.numDivisionID = div1.numDivisionID
										  JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
										  JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID 
									  WHERE  D1.numDomainID = @numDomainID
										AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1


								--************Employer Shipping Address************ 
								SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ' ,' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
										isnull(AD.vcStreet,'') AS vcStreet,
										isnull(AD.vcCity,'') AS vcCity,
										isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
										isnull(AD.vcPostalCode,'') AS vcPostalCode,
										isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
										ISNULL(AD.vcAddressName,'') AS vcAddressName,vcCompanyName
								 FROM Domain D1 JOIN divisionmaster div1 ON D1.numDivisionID = div1.numDivisionID
									  JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
									  JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID 
								 WHERE AD.numDomainID=@numDomainID 
								 AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1
           END
		
        ELSE IF @numReferenceType = 11
		BEGIN
		      
			---------------------------------------------------------------------------------------
      
				  SELECT Mst.vcPOppName AS vcBizDocID,
						 Mst.vcPOppName AS OppName,OBD.vcBizDocID AS BizDcocName,@numBizDocType AS BizDoc,ISNULL(Mst.numRecOwner,0) AS Owner,
						 CMP.VcCompanyName AS CompName,Mst.numContactID,Mst.numCreatedBy AS BizDocOwner,
						 Mst.fltDiscount AS decDiscount,
						 (SELECT SUM(ISNULL(monAmountPaid,0)) FROM OpportunityBizDocs WHERE numOppId=MSt.numOppId) AS monAmountPaid,
						 isnull(Mst.txtComments,'') AS vcComments,
						 CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffset,Mst.bintCreatedDate)) dtCreatedDate,
						 dbo.fn_GetContactName(Mst.numCreatedby) AS numCreatedby,
						 dbo.fn_GetContactName(Mst.numModifiedBy) AS numModifiedBy,
						 Mst.bintModifiedDate AS dtModifiedDate,
						 '' AS numViewedBy,
						 NULL AS dtViewedDate,
						 isnull(Mst.bitBillingTerms,0) AS tintBillingTerms,
						 isnull(Mst.intBillingDays,0) AS numBillingDays,
						 ISNULL(BTR.numNetDueInDays,0) AS numBillingDaysName,
						 BTR.vcTerms AS vcBillingTermsName,
						 isnull(Mst.bitInterestType,0) AS tintInterestType,
						 isnull(Mst.fltInterest,0) AS fltInterest,
						 tintOPPType,
						 @numBizDocType AS numBizDocId,
						 Mst.bintAccountClosingDate,
						 tintShipToType,
						 tintBillToType,
						 tintshipped,
						 NULL bintShippedDate,
						 ISNULL(Mst.monShipCost ,0) AS monShipCost,
						 ISNULL(BT.vcBizDocFooter,D.vcBizDocFooter) AS BizdocFooter,
						 ISNULL(BT.vcBizDocImagePath,D.vcBizDocImagePath) As vcBizDocImagePath, 
						 Mst.numDivisionID,
						  ISNULL(BT.vcPurBizDocFooter,D.vcPurBizDocFooter) AS PurBizdocFooter,
						 isnull(Mst.fltDiscount,0) AS fltDiscount,
						 isnull(Mst.bitDiscountType,0) AS bitDiscountType,
						 0 AS numShipVia,
						 '' AS vcTrackingURL,
						 0 AS numBizDocStatus,
						 '' AS BizDocStatus,
						 '' AS ShipVia,
						 ISNULL(C.varCurrSymbol,'') varCurrSymbol,
						 0 AS ShippingReportCount,
						 0 bitPartialFulfilment,
						 vcPOppName as vcBizDocName,
						 dbo.[fn_GetContactName](Mst.[numModifiedBy])  + ', '
							+ CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Mst.bintModifiedDate)) AS ModifiedBy,
						 dbo.[fn_GetContactName](Mst.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](Mst.[numAssignedTo]) AS OrderRecOwner,
						 dbo.[fn_GetContactName](DM.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](DM.[numAssignedTo]) AS AccountRecOwner,
						 Mst.bintCreatedDate AS dtFromDate,
						 Mst.tintTaxOperator,
						 0 AS monTotalEmbeddedCost,
						 0 AS monTotalAccountedCost,
						 ISNULL(BT.bitEnabled,0) bitEnabled,
						 ISNULL(BT.txtBizDocTemplate,'') txtBizDocTemplate, 
						 ISNULL(BT.txtCSS,'') txtCSS,
						 dbo.fn_GetContactName(Mst.numAssignedTo) AS AssigneeName,
						 ISNULL((SELECT vcEmail FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneeEmail,
						 ISNULL((SELECT numPhone FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneePhone,
						 dbo.fn_GetComapnyName(Mst.numDivisionId) OrganizationName,
						 dbo.fn_GetContactName(Mst.numContactID)  OrgContactName,
						 case when ACI.numPhone<>'' then ACI.numPhone +case when ACI.numPhoneExtension<>'' then ' - ' + ACI.numPhoneExtension else '' end  else '' END OrgContactPhone,
						 DM.vcComPhone as OrganizationPhone,
						 ISNULL(ACI.vcEmail,'') AS OrgContactEmail,
 						 dbo.[fn_GetContactName](Mst.[numRecOwner]) AS OnlyOrderRecOwner,
 						 0 bitAuthoritativeBizDocs,
						0 tintDeferred,0 AS monCreditAmount,
						 0 as bitRentalBizDoc,isnull(BT.numBizDocTempID,0) as numBizDocTempID,isnull(BT.vcTemplateName,'') as vcTemplateName,
						 [dbo].[GetDealAmount](Mst.numOppId ,getutcdate(),0 ) as monPAmount,
						 --(select top 1 SD.vcSignatureFile from SignatureDetail SD join OpportunityBizDocsDetails OBD on SD.numSignID=OBD.numSignID where SD.numDomainID = @numDomainID and OBD.numDomainID = @numDomainID and OBD.numSignID IS not null and OBD.numBizDocsId=opp.numOppBizDocsId order by OBD.numBizDocsPaymentDetId desc) as vcSignatureFile,
						 isnull(CMP.txtComments,'') as vcOrganizationComments,
						 ISNULL((SELECT SUBSTRING((SELECT '$^$' + ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=MSt.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OBD.numShipVia  AND vcFieldName='Tracking URL')),'') +'#^#'+ RTRIM(LTRIM(vcTrackingNo) )  FROM dbo.OpportunityBizDocs OBD WHERE OBD.numOppId=Mst.numOppID FOR XML PATH('')),4,200000)),'') AS  vcTrackingNo,'' AS  vcShippingMethod,
						 NULL AS dtDeliveryDate,ISNULL(Mst.vcOppRefOrderNo,'') AS vcOppRefOrderNo,ISNULL(Mst.numDiscountAcntType,0) AS numDiscountAcntType ,
						 isnull(numOppBizDocTempID,0) as numOppBizDocTempID,
						 com1.vcCompanyName AS EmployerOrganizationName,div1.vcComPhone as EmployerOrganizationPhone
				  FROM    OpportunityMaster Mst JOIN Domain D ON D.numDomainID = Mst.numDomainID
					LEFT JOIN OpportunityBizDocs AS OBD ON Mst.numOppid=OBD.numOppId
						  LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = Mst.numCurrencyID
						  LEFT JOIN [DivisionMaster] DM ON DM.numDivisionID = Mst.numDivisionID   
						  LEFT JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = @numDomainID  
						  LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = Mst.numContactID
						  LEFT JOIN [BizDocTemplate] BT ON BT.numDomainID = @numDomainID AND BT.numBizDocID = @numBizDocType 
							 AND ((BT.numBizDocTempID=ISNULL(Mst.numOppBizDocTempID, 0)) OR (ISNULL(BT.bitDefault,0)=1 AND ISNULL(BT.bitEnabled,0)=1))  
						  LEFT JOIN BillingTerms BTR ON BTR.numTermsID = ISNULL(Mst.intBillingDays,0)
						  JOIN divisionmaster div1 ON D.numDivisionID = div1.numDivisionID
						  JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
				  WHERE  OBD.numOppBizDocsId = @numReferenceID AND  Mst.numDomainID = @numDomainID
				  SET @numReferenceType=1
			      SET @numReferenceID=(SELECT TOP 1 numOppId FROM OpportunityBizDocs WHERE numOppBizDocsId = @numReferenceID)
					EXEC USP_OPPGetOppAddressDetails @numReferenceID,@numDomainId	
             
            END
    END

GO
--exec USP_GetMirrorBizDocItems @numReferenceID=37056,@numReferenceType=1,@numDomainID=1
--created by anoop jayaraj
--	EXEC USP_GEtSFItemsForImporting 72,'76,318,538,758,772,1061,11409,11410,11628,11629,17089,18151,18193,18195,18196,18197',1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getsfitemsforimporting')
DROP PROCEDURE usp_getsfitemsforimporting
GO
CREATE PROCEDURE USP_GEtSFItemsForImporting
    @numDomainID AS NUMERIC(9),
	@numBizDocId	AS BIGINT,
	@vcBizDocsIds		VARCHAR(MAX),
	@tintMode AS TINYINT 
AS
BEGIN
---------------------------------------------------------------------------------
    DECLARE  @strSql  AS VARCHAR(8000)
  DECLARE  @strFrom  AS VARCHAR(2000)

IF @tintMode=1 --PrintPickList
BEGIN
   SELECT
		ISNULL(vcItemName,'') AS vcItemName,
		ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=187 AND bitDefault=1 AND bitIsImage=1),'') AS vcImage,
		ISNULL(vcModelID,'') AS vcModelID,
		ISNULL(txtItemDesc,'') AS txtItemDesc,
		ISNULL(vcWareHouse,'') AS vcWareHouse,
		ISNULL(vcLocation,'') AS vcLocation,
		(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcWHSKU ELSE Item.vcSKU END) AS vcSKU,
		(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcBarCode ELSE Item.numBarCodeId END) AS vcUPC,
		SUM(numUnitHour) AS numUnitHour
	FROM
		OpportunityBizDocItems
	INNER JOIN
		WareHouseItems
	ON
		OpportunityBizDocItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
	INNER JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	INNER JOIN
		Item
	ON
		OpportunityBizDocItems.numItemCode = Item.numItemCode
	WHERE
		numOppBizDocID IN ( SELECT * FROM dbo.splitIds(@vcBizDocsIds,','))
	GROUP BY
		vcItemName,
		vcModelID,
		txtItemDesc,
		vcWareHouse,
		vcLocation,
		(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcWHSKU ELSE Item.vcSKU END),
		(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcBarCode ELSE Item.numBarCodeId END)
	ORDER BY
		vcWareHouse ASC,
		vcLocation ASC,
		vcItemName ASC
END
ELSE IF @tintMode=2 --PrintPackingSlip
BEGIN
		  
		SELECT	
				ROW_NUMBER() OVER(ORDER BY OM.numDomainID ASC) AS SRNO,
				OM.numOppId,
				OM.numDomainID,
				OM.tintTaxOperator,
				I.numItemCode,
				I.vcModelID,
				OM.vcPOppName,
				cast((OBDI.monTotAmtBefDiscount - OBDI.monTotAMount) as varchar(20)) + Case When isnull(OM.fltDiscount,0)> 0 then  '(' + cast(OM.fltDiscount as varchar(10)) + '%)' else '' end AS DiscAmt,
				OBDI.dtRentalStartDate,
				OBDI.dtRentalReturnDate,
				OBDI.monShipCost,
				OBDI.vcShippingMethod,
				OBDI.dtDeliveryDate,
				numItemClassification,
				OBDI.vcNotes,
				OBD.vcTrackingNo,
				I.vcManufacturer,
				--vcItemClassification,
				CASE 
					WHEN charItemType='P' 
					THEN 'Inventory Item' 
					WHEN charItemType='S' 
					THEN 'Service' 
					WHEN charItemType='A' 
					THEN 'Accessory' 
					WHEN charItemType='N' 
					THEN 'Non-Inventory Item' 
				END AS charItemType,charItemType AS [Type],
				OBDI.vcAttributes,
				OI.[numoppitemtCode] ,
				OI.vcItemName,
				OBDI.numUnitHour,
				OI.[numQtyShipped],
				OBDI.vcItemDesc AS txtItemDesc,
				vcUnitofMeasure AS numUOMId,
				monListPrice AS [Price],
				CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4), OBDI.monTotAmount)) Amount,
				dbo.fn_GetListItemName(OBD.numShipVia) AS ShipVai,
				OBDI.monPrice,
				OBDI.monTotAmount,
				vcWareHouse,
				vcLocation,
				vcWStreet,
				vcWCity,
				vcWPinCode,
				(SELECT vcState FROM State WHERE numStateID = W.numWState) AS [vcState],
				dbo.fn_GetListItemName(W.numWCountry) AS [vcWCountry],							
				vcWHSKU,
				vcBarCode,
				SUBSTRING((SELECT ',' + vcSerialNo + CASE 
															WHEN isnull(I.bitLotNo,0)=1 
															THEN ' (' + CONVERT(VARCHAR(15),SERIALIZED_ITEM.numQty) + ')' 
															ELSE '' 
													 END 
							FROM OppWarehouseSerializedItem SERIALIZED_ITEM 
							JOIN WareHouseItmsDTL W_ITEM_DETAIL ON SERIALIZED_ITEM.numWareHouseItmsDTLID = W_ITEM_DETAIL.numWareHouseItmsDTLID 
							WHERE SERIALIZED_ITEM.numOppID = OI.numOppId 
							  AND SERIALIZED_ITEM.numOppItemID = OI.numoppitemtCode 
							  and SERIALIZED_ITEM.numOppBizDocsID=OBD.numOppBizDocsId
							ORDER BY vcSerialNo 
						  FOR XML PATH('')),2,200000) AS SerialLotNo,
				isnull(OBD.vcRefOrderNo,'') AS vcRefOrderNo,
				isnull(OM.bitBillingTerms,0) AS tintBillingTerms,
				isnull(intBillingDays,0) AS numBillingDays,
--				CASE 
--					WHEN ISNUMERIC(ISNULL(dbo.fn_GetListItemName(ISNULL(intBillingDays,0)),0)) = 1 
--					THEN ISNULL(dbo.fn_GetListItemName(ISNULL(intBillingDays,0)),0) 
--					ELSE 0 
--				END AS numBillingDaysName,
				CASE WHEN ISNUMERIC((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0))) = 1
					 THEN (SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0))
					 ELSE 0
				END AS numBillingDaysName,	 
				ISNULL(bitInterestType,0) AS tintInterestType,
				tintOPPType,
				dbo.fn_GetContactName(numApprovedBy) AS ApprovedBy,
				dtApprovedDate,
				OM.bintAccountClosingDate,
				tintShipToType,
				tintBillToType,
				tintshipped,
				dtShippedDate bintShippedDate,
				OM.numDivisionID,							
				ISNULL(numShipVia,0) AS numShipVia,
				ISNULL(vcTrackingURL,'') AS vcTrackingURL,
				CASE 
				    WHEN numShipVia IS NULL 
				    THEN '-'
				    ELSE dbo.fn_GetListItemName(numShipVia)
				END AS ShipVia,
				ISNULL(C.varCurrSymbol,'') varCurrSymbol,
				isnull(bitPartialFulfilment,0) bitPartialFulfilment,
				dbo.[fn_GetContactName](OM.[numRecOwner])  AS OrderRecOwner,
				dbo.[fn_GetContactName](DM.[numRecOwner]) AS AccountRecOwner,
				dbo.fn_GetContactName(OM.numAssignedTo) AS AssigneeName,
				ISNULL((SELECT vcEmail FROM dbo.AdditionalContactsInformation WHERE numContactID = OM.numAssignedTo),'') AS AssigneeEmail,
				ISNULL((SELECT numPhone FROM dbo.AdditionalContactsInformation WHERE numContactID = OM.numAssignedTo),'') AS AssigneePhone,
				dbo.fn_GetComapnyName(OM.numDivisionId) OrganizationName,
				DM.vcComPhone as OrganizationPhone,
				dbo.fn_GetContactName(OM.numContactID)  OrgContactName,
				dbo.getCompanyAddress(OM.numDivisionId,1,OM.numDomainId) CompanyBillingAddress,
				CASE 
					 WHEN ACI.numPhone<>'' 
					 THEN ACI.numPhone + CASE 
										    WHEN ACI.numPhoneExtension<>'' 
											THEN ' - ' + ACI.numPhoneExtension 
											ELSE '' 
										 END  
					 ELSE '' 
				END OrgContactPhone,
				ISNULL(ACI.vcEmail,'') AS OrgContactEmail,
				dbo.[fn_GetContactName](OM.[numRecOwner]) AS OnlyOrderRecOwner,
				(SELECT TOP 1 SD.vcSignatureFile FROM SignatureDetail SD 
												 JOIN OpportunityBizDocsDetails OBD ON SD.numSignID = OBD.numSignID 
												WHERE SD.numDomainID = @numDomainID 
												  AND OBD.numDomainID = @numDomainID 
												  AND OBD.numSignID IS not null 
				ORDER BY OBD.numBizDocsPaymentDetId DESC) AS vcSignatureFile,
				ISNULL(CMP.txtComments,'') AS vcOrganizationComments,
				OBD.vcRefOrderNo AS [P.O.No],
				OM.txtComments AS [Comments],
				D.vcBizDocImagePath
				,dbo.FormatedDateFromDate(OM.dtReleaseDate,@numDomainID) AS vcReleaseDate
				,(CASE WHEN ISNULL(OM.numPartner,0) > 0 THEN ISNULL((SELECT CONCAT(DivisionMaster.vcPartnerCode,'-',CompanyInfo.vcCompanyName) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE DivisionMaster.numDivisionID=OM.numPartner),'') ELSE '' END) AS vcPartner  
	INTO #temp_Packing_List
	FROM OpportunityMaster AS OM
	join OpportunityBizDocs OBD on OM.numOppID=OBD.numOppID
	JOIN OpportunityBizDocItems OBDI on OBDI.numOppBizDocID=OBD.numOppBizDocsId
	JOIN OpportunityItems AS OI on OI.numoppitemtCode=OBDI.numOppItemID and OI.numoppID=OI.numOppID
	JOIN Item AS I ON OI.numItemCode = I.numItemCode AND I.numDomainID = @numDomainID
	LEFT OUTER JOIN WareHouseItems WI ON I.numItemCode = WI.numItemID AND WI.numWareHouseItemID = OBDI.numWarehouseItmsID
	LEFT OUTER JOIN Warehouses W ON WI.numWareHouseID = W.numWareHouseID 
	JOIN [DivisionMaster] DM ON DM.numDivisionID = OM.numDivisionID   
	JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = @numDomainID  
	JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = OM.numContactID
	JOIN Domain D ON D.numDomainID = OM.numDomainID
	LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = OM.numCurrencyID
    WHERE OM.numDomainID = @numDomainID 
	  AND OBD.[numOppBizDocsId] IN ( SELECT * FROM dbo.splitIds(@vcBizDocsIds ,',')) ORDER BY OM.numOppId
	
	DECLARE @numFldID AS INT    
	DECLARE @intRowNum AS INT    	
	DECLARE @vcFldname AS VARCHAR(MAX)	
	
	SET @strSQL = ''
	

	/**** START: Added Sales Tax Column ****/

	DECLARE @strSQLUpdate AS VARCHAR(2000);
	
	EXEC ( 'ALTER TABLE #temp_Packing_List ADD [TotalTax] MONEY'  )
	EXEC ( 'ALTER TABLE #temp_Packing_List ADD [CRV] MONEY'  )


	DECLARE @i AS INT = 1
	DECLARE @Count AS INT 
	SELECT @Count = COUNT(*) FROM #temp_Packing_List

	DECLARE @numOppId AS BIGINT
	DECLARE @numOppItemCode AS BIGINT
	DECLARE @tintTaxOperator AS INT

	WHILE @i <= @Count
	BEGIN
		SET @strSQLUpdate=''

		SELECT 
			@numDomainID = numDomainID,
			@numOppId = numOppId,
			@tintTaxOperator = tintTaxOperator,
			@numOppItemCode = numoppitemtCode
		FROM
			#temp_Packing_List
		WHERE
			SRNO = @i


		IF @tintTaxOperator = 1 
		BEGIN
			 SET @strSQLUpdate = @strSQLUpdate
				+ ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ',0,'
				+ CONVERT(VARCHAR(20), @numOppId) + ','
				+ CONVERT(VARCHAR(20), @numOppItemCode) + ',1,Amount,numUnitHour)'

			SET @strSQLUpdate = @strSQLUpdate
				+ ',[CRV]= dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ',1,'
				+ CONVERT(VARCHAR(20), @numOppId) + ','
				+ CONVERT(VARCHAR(20), @numOppItemCode) + ',1,Amount,numUnitHour)'
		END
		ELSE IF @tintTaxOperator = 2 -- remove sales tax
		BEGIN
			SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
			SET @strSQLUpdate = @strSQLUpdate + ',[CRV]= 0'
		END
		ELSE
		BEGIN
			 SET @strSQLUpdate = @strSQLUpdate
				+ ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ',0,'
				+ CONVERT(VARCHAR(20), @numOppId) + ','
				+ CONVERT(VARCHAR(20), @numOppItemCode) + ',1,Amount,numUnitHour)'

			SET @strSQLUpdate = @strSQLUpdate
				+ ',[CRV]= dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ',1,'
				+ CONVERT(VARCHAR(20), @numOppId) + ','
				+ CONVERT(VARCHAR(20), @numOppItemCode) + ',1,Amount,numUnitHour)'
		END

		IF @strSQLUpdate <> '' 
		BEGIN
			SET @strSQLUpdate = 'UPDATE #temp_Packing_List SET numItemCode=numItemCode' + @strSQLUpdate + ' WHERE numItemCode > 0 AND numOppId=' + CONVERT(VARCHAR(20), @numOppId) + ' AND numoppitemtCode=' + CONVERT(VARCHAR(20), @numOppItemCode)
			PRINT @strSQLUpdate
			EXEC (@strSQLUpdate)
		END

		SET @i = @i + 1
	END


	SET @strSQLUpdate = ''
	DECLARE @vcTaxName AS VARCHAR(100)
	DECLARE @numTaxItemID AS NUMERIC(9)
	SET @numTaxItemID = 0
	SELECT TOP 1 @vcTaxName = vcTaxName, @numTaxItemID = numTaxItemID FROM TaxItems WHERE numDomainID = @numDomainID

	WHILE @numTaxItemID > 0
	BEGIN

		EXEC ( 'ALTER TABLE #temp_Packing_List ADD [' + @vcTaxName + '] MONEY' )

		SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName  + ']= dbo.fn_CalBizDocTaxAmt(numDomainID,'
						+ CONVERT(VARCHAR(20), @numTaxItemID) + ',numOppId,numoppitemtCode,1,Amount,numUnitHour)'
           

		SELECT TOP 1 @vcTaxName = vcTaxName, @numTaxItemID = numTaxItemID FROM TaxItems WHERE numDomainID = @numDomainID AND numTaxItemID > @numTaxItemID

		IF @@rowcount = 0 
			SET @numTaxItemID = 0
	END


	IF @strSQLUpdate <> '' 
	BEGIN
		SET @strSQLUpdate = 'UPDATE #temp_Packing_List SET numItemCode=numItemCode' + @strSQLUpdate + ' WHERE numItemCode > 0' 
		PRINT @strSQLUpdate
		EXEC (@strSQLUpdate)
	END

	/**** END: Added Sales Tax Column ****/
	
	PRINT 'START'                                                                                          
	PRINT 'QUERY : ' + @strSQL

	SELECT TOP 1 @numFldID = numFieldId,
				 @vcFldname = Fld_label,
				 @intRowNum = (intRowNum+1) 
	FROM DycFormConfigurationDetails DTL                                                                                                
	JOIN CFW_Fld_Master FIELD_MASTER ON FIELD_MASTER.Fld_id = DTL.numFieldId                                                                                                 
	WHERE DTL.numFormID = 7 
	  AND FIELD_MASTER.Grp_id = 5 
	  AND DTL.numDomainID = @numDomainID 
	  AND numAuthGroupID = @numBizDocId
	ORDER BY intRowNum

	PRINT @intRowNum
	WHILE @intRowNum > 0                                                                                  
	BEGIN    
			PRINT 'FROM LOOP'
			PRINT @vcFldname
			PRINT @numFldID

			EXEC('ALTER TABLE #temp_Packing_List add [' + @vcFldname + '] varchar(100)')
		
			SET @strSQL = 'UPDATE #temp_Packing_List SET [' + @vcFldname + '] = dbo.GetCustFldValueItem(' + CONVERT(VARCHAR(10),@numFldID) + ',numItemCode) 
						   WHERE numItemCode > 0'

			PRINT 'QUERY : ' + @strSQL
			EXEC (@strSQL)
			                                                                                 	                                                                                       
			SELECT TOP 1 @numFldID = numFieldId,
						 @vcFldname = Fld_label,
						 @intRowNum = (intRowNum + 1) 
			FROM DycFormConfigurationDetails DETAIL                                                                                                
			JOIN CFW_Fld_Master MASTER ON MASTER.Fld_id = DETAIL.numFieldId                                                                                                 
			WHERE DETAIL.numFormID = 7 
			  AND MASTER.Grp_id = 5 
			  AND DETAIL.numDomainID = @numDomainID 
			 AND numAuthGroupID = @numBizDocId                                                 
			  AND intRowNum >= @intRowNum 
			ORDER BY intRowNum                                                                  
			           
			IF @@rowcount=0 SET @intRowNum=0                                                                                                                                                
		END	
	PRINT 'END'	
	SELECT * FROM #temp_Packing_List
END
END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetTransactionHistory' ) 
    DROP PROCEDURE USP_GetTransactionHistory
GO

CREATE PROCEDURE [dbo].[USP_GetTransactionHistory]
    @numTransHistoryID NUMERIC(18, 0),
    @numDomainID NUMERIC(18, 0),
    @numDivisionID NUMERIC(18, 0),
    @numOppID NUMERIC(18, 0)
    
AS 
    SET NOCOUNT ON
    SET TRANSACTION ISOLATION LEVEL READ COMMITTED

    DECLARE @strSQL AS NVARCHAR(MAX)
    DECLARE @strWHERE AS VARCHAR(MAX)
    
    SET @strWHERE = ' WHERE 1=1 AND TH.numDomainID = '
        + CONVERT(VARCHAR(10), @numDomainID) 
    
    IF ( @numOppID > 0 ) 
        BEGIN
            SET @strWHERE = @strWHERE + ' AND TH.numOppID = '
                + CONVERT(VARCHAR(10), @numOppID)
        END
    
    IF ( @numTransHistoryID > 0 ) 
        BEGIN
            SET @strWHERE = @strWHERE + ' AND numTransHistoryID = '
                + CONVERT(VARCHAR(10), @numTransHistoryID)
        END
    
    IF ( @numDivisionID > 0 ) 
        BEGIN
            SET @strWHERE = @strWHERE + ' AND TH.numDivisionID = '
                + CONVERT(VARCHAR(10), @numDivisionID)
        END
    

    SET @strSQL = ' 
SELECT
	[numTransHistoryID],
	TH.[numDivisionID],
	TH.[numContactID],
	TH.[numOppID],
	TH.[numOppBizDocsID],
	[vcTransactionID],
	[tintTransactionStatus],
	(CASE WHEN tintTransactionStatus = 1 THEN ''Authorized/Pending Capture'' 
		 WHEN tintTransactionStatus = 2 THEN ''Captured''
		 WHEN tintTransactionStatus = 3 THEN ''Void''
		 WHEN tintTransactionStatus = 4 THEN ''Failed''
		 WHEN tintTransactionStatus = 5 THEN ''Credited''
		 ELSE ''Not Authorized''
	END) AS vcTransactionStatus,	 
	[vcPGResponse],
	[tintType],
	[monAuthorizedAmt],
	[monCapturedAmt],
	[monRefundAmt],
	[vcCardHolder],
	[vcCreditCardNo],
	isnull((SELECT vcData FROM dbo.ListDetails WHERE numListItemID = isnull([numCardType],0)),''-'') as vcCardType,
	[vcCVV2],
	[tintValidMonth],
	[intValidYear],
	TH.dtCreatedDate,
	ISNULL(OBD.vcBizDocID, ISNULL(OM.vcPOppName,'''')) AS [vcBizOrderID],
	vcSignatureFile,
	ISNULL(tintOppType,0) AS [tintOppType],
	ISNULL(vcResponseCode,'''') vcResponseCode
FROM
	[dbo].[TransactionHistory] TH
LEFT JOIN dbo.OpportunityMaster OM ON OM.numoppID = TH.numOppID AND TH.numDomainID = OM.numDomainId
LEFT JOIN dbo.OpportunityBizDocs OBD ON OM.numOppId = OBD.numOppId AND OBD.numOppBizDocsId = TH.numOppBizDocsID '

    SET @strSQL = @strSQL + @strWHERE
    PRINT @strSQL
    EXEC SP_EXECUTESQL @strSQL
GO

--SELECT * FROM OpportunityMaster
--SELECT vcData FROM dbo.ListDetails WHERE numListItemID = 14883
/****** Object:  StoredProcedure [dbo].[usp_InsertNewChartAccountDetails]    Script Date: 09/25/2009 16:15:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva                                
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_insertnewchartaccountdetails' ) 
    DROP PROCEDURE usp_insertnewchartaccountdetails
GO
CREATE PROCEDURE [dbo].[usp_InsertNewChartAccountDetails]
    @numAcntTypeId NUMERIC(9) = 0,
    @numParntAcntTypeID NUMERIC(9) = 0,
    @vcAccountName VARCHAR(100),
    @vcAccountDescription VARCHAR(100),
    @monOriginalOpeningBal MONEY,
    @monOpeningBal MONEY,
    @dtOpeningDate DATETIME,
    @bitActive BIT,
    @numAccountId NUMERIC(9) = 0 OUTPUT,
    @bitFixed BIT,
    @numDomainId NUMERIC(9) = 0,
    @numListItemID NUMERIC(9) = 0,
    @numUserCntId NUMERIC(9) = 0,
    @bitProfitLoss BIT,
    @bitDepreciation BIT,
    @dtDepreciationCostDate DATETIME,
    @monDepreciationCost MONEY,
    @vcNumber VARCHAR(50),
    @bitIsSubAccount BIT,
    @numParentAccId NUMERIC(18, 0),
    @IsBankAccount			BIT	=0,
	@vcStartingCheckNumber	VARCHAR(50)=NULL ,
	@vcBankRountingNumber	VARCHAR(50)=NULL ,
	@bitIsConnected				BIT =0,
	@numBankDetailID			NUMERIC(18,0)=NULL

AS 
BEGIN 
	BEGIN TRY
	BEGIN TRANSACTION

    
        DECLARE @numAccountId1 AS NUMERIC(9)                              
        DECLARE @strSQl AS VARCHAR(1000)                             
        DECLARE @strSQLBalance AS VARCHAR(1000)                           
        DECLARE @numJournalId AS NUMERIC(9) 
        DECLARE @numDepositId AS NUMERIC(9)     
        DECLARE @numDepositJournalId AS NUMERIC(9)  
        DECLARE @numCashCreditJournalId AS NUMERIC(9)  
        DECLARE @numCashCreditId AS NUMERIC(9)   
        DECLARE @numAccountIdOpeningEquity AS NUMERIC(9)
        DECLARE @vcAccountCode VARCHAR(50)
        DECLARE @intLevel AS INT
        DECLARE @intFlag INT
        DECLARE @vcAccountNameWithNumber AS VARCHAR(100)
        
    
        SET @strSQl = ''                          
        SET @strSQLBalance = ''   
        
        SET @intFlag = 1 
        IF ISNULL(@vcNumber,'') = '-1' --@vcNumber will have -1 value only when its being called from [USP_ChartAcntDefaultValues], as well cursor below creates time out.
		BEGIN
			SET @intFlag = 0 
			SET @vcNumber= ''
			PRINT @intFlag
		END
		  
        IF ISNULL(@vcNumber, '') = '' 
            SET @vcAccountNameWithNumber = ''	
        ELSE 
            SET @vcAccountNameWithNumber = @vcNumber + ' '   
	
		IF @numAcntTypeId = 0 
		BEGIN
			DECLARE @AccountTypeCode VARCHAR(4)
			(SELECT @AccountTypeCode = SUBSTRING(vcAccountCode ,1,4) FROM dbo.AccountTypeDetail WHERE numAccountTypeID = @numParntAcntTypeID)
			SELECT @numAcntTypeId = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID=@numDomainID AND vcAccountCode =  @AccountTypeCode
		END
	                             
		--Check for Current Fiancial Year
        IF (SELECT COUNT([numFinYearId]) FROM [FinancialYear] WHERE [numDomainId] = @numDomainId AND [bitCurrentYear] = 1) = 0 
        BEGIN
            RAISERROR ( 'NoCurrentFinancialYearSet', 16, 1 ) ;
            RETURN ;
        END
	
		  
        IF @numAccountId = 0 
        BEGIN 
			IF ISNULL(@vcNumber, '') <> '' 
			BEGIN
				IF EXISTS(SELECT * FROM Chart_Of_Accounts WHERE vcNumber = @vcNumber AND numDomainID =  @numDomainId)	
				BEGIN
					RAISERROR ( 'DuplicateNumber', 16, 1 ) ;
					RETURN ;
				END
			END 
				 
            IF @numParntAcntTypeID = 0 
			BEGIN
                SET @numParntAcntTypeID = (SELECT TOP 1 numAccountId FROM dbo.Chart_Of_Accounts WHERE Chart_Of_Accounts.numParntAcntTypeID IS NULL AND Chart_Of_Accounts.numDomainId = @numDomainId)
			END

            SET @numAccountIdOpeningEquity = (SELECT TOP 1 numAccountId FROM dbo.Chart_Of_Accounts WHERE bitProfitLoss = 1 AND Chart_Of_Accounts.numDomainId = @numDomainId) 
            
         
            IF (ISNULL(@numAccountIdOpeningEquity, 0) = 0 AND @bitProfitLoss = 0)
            BEGIN
                RAISERROR ( 'NoProfitLossACC', 16, 1 ) ;
                RETURN 
            END
			
            SELECT  @vcAccountCode = dbo.GetAccountTypeCode(@numDomainId, @numParntAcntTypeID, 1)
                
            IF (@bitIsSubAccount=1)
			BEGIN
				SELECT  @vcAccountCode = dbo.GetAccountTypeCode(@numDomainId, @numParentAccId, 3)
				SELECT @numParntAcntTypeID = numParntAcntTypeId FROM Chart_Of_Accounts WHERE numDomainId=@numDomainId AND numAccountId = @numParentAccId
			END
                
            SELECT  @intLevel = CASE WHEN @numParentAccId = 0 AND @bitIsSubAccount = 0 THEN 0 ELSE ISNULL(MAX(intLevel),0) + 1 END  FROM dbo.Chart_Of_Accounts WHERE numAccountId = @numParentAccId 
            PRINT @intLevel
									   
--			PRINT @vcAccountCode
            INSERT  INTO dbo.Chart_Of_Accounts
            (
                numAcntTypeId,
                numParntAcntTypeID,
                vcAccountCode,
                vcAccountName,
                vcAccountDescription,
                numOriginalOpeningBal,
                numOpeningBal,
                dtOpeningDate,
                bitActive,
                bitFixed,
                numDomainId,
                numListItemID,
                bitProfitLoss,
                [bitDepreciation],
                [dtDepreciationCostDate],
                [monDepreciationCost],
                vcNumber,
                bitIsSubAccount,
                numParentAccId,
                intLevel,
                IsBankAccount,
                vcStartingCheckNumber,
                vcBankRountingNumber,
                IsConnected,
                numBankDetailID
            )
            VALUES  
			(
                @numAcntTypeId,
                @numParntAcntTypeID,
                @vcAccountCode,
                @vcAccountNameWithNumber + @vcAccountName,
                @vcAccountDescription,
                @monOriginalOpeningBal,
                @monOpeningBal,
                @dtOpeningDate,
                @bitActive,
                @bitFixed,
                @numDomainId,
                @numListItemID,
                @bitProfitLoss,
                @bitDepreciation,
                @dtDepreciationCostDate,
                @monDepreciationCost,
                @vcNumber,
                @bitIsSubAccount,
                @numParentAccId,
                @intLevel,
                @IsBankAccount,
                @vcStartingCheckNumber,
                @vcBankRountingNumber,
                @bitIsConnected,
                @numBankDetailID
            )
                
				
			SET @numAccountId1 = @@IDENTITY                                                  
            SET @numAccountId = @numAccountId1 --used at bottom
            SELECT @numAccountId1                            
				
			
			
			--Added By Chintan
            EXEC USP_ManageChartAccountOpening @numDomainId, @numAccountId1, @monOriginalOpeningBal, @dtOpeningDate                                    
        END
        ELSE IF @numAccountId <> 0 
        BEGIN
            IF EXISTS(SELECT * FROM Chart_Of_Accounts WHERE numAccountId = @numAccountId AND numDomainId = @numDomainId AND (numAcntTypeId != @numAcntTypeId OR numParntAcntTypeId != @numParntAcntTypeID))
			BEGIN
				IF NOT ((SELECT COUNT(*) FROM AccountTypeDetail WHERE numAccountTypeID=@numParntAcntTypeID AND vcAccountcode LIKE '0106%' AND numDomainId = @numDomainId )=1
					AND (SELECT COUNT(*) FROM AccountTypeDetail WHERE numAccountTypeID=(SELECT numParntAcntTypeId FROM Chart_Of_Accounts WHERE numAccountId = @numAccountId AND numDomainId = @numDomainId) AND vcAccountcode LIKE '0104%' AND numDomainId = @numDomainId)=1)
				BEGIN
					IF EXISTS (SELECT GJD.numJournalId FROM dbo.General_Journal_Details GJD WHERE numDomainId = @numDomainId AND numChartAcntId=@numAccountId)
					BEGIN
						RAISERROR ( 'JournalEntryExists', 16, 1 ) ;
						RETURN ;
					END
				END
			END
					
			IF ISNULL(@vcNumber, '') <> '' 
			BEGIN
				IF EXISTS(SELECT * FROM Chart_Of_Accounts WHERE vcNumber = @vcNumber AND numDomainID =  @numDomainId AND numAccountId <> @numAccountId)	
				BEGIN
					RAISERROR ( 'DuplicateNumber', 16, 1 ) ;
					RETURN ;
				END
			END 

            IF @numParntAcntTypeID = 0 
			BEGIN
                SET @numParntAcntTypeID = (SELECT Chart_Of_Accounts.numAccountId FROM dbo.Chart_Of_Accounts WHERE Chart_Of_Accounts.numParntAcntTypeID IS NULL AND Chart_Of_Accounts.numDomainId = @numDomainId) 
			END
                            
            DECLARE @OldOriginalOpeningBalance AS MONEY 
            DECLARE @OldOpeningBalance AS MONEY 
            DECLARE @NewOpeningBalance AS MONEY
            DECLARE @OldParentAcntTypeID AS NUMERIC
			DECLARE @OldIsSubAccount AS NUMERIC
					
            SELECT  @OldOriginalOpeningBalance = numOriginalOpeningBal,
                    @OldOpeningBalance = numOpeningBal,
                    @OldParentAcntTypeID = numParntAcntTypeId,
                    @vcAccountCode = vcAccountCode,
                    @OldIsSubAccount = ISNULL(bitIsSubAccount,0)
            FROM    dbo.Chart_Of_Accounts
            WHERE   numAccountId = @numAccountID
                    AND numDomainId = @numDomainID
			
            IF ( @OldParentAcntTypeID != @numParntAcntTypeID ) --bug fix 1439
            BEGIN
                SELECT  @vcAccountCode = dbo.GetAccountTypeCode(@numDomainId, @numParntAcntTypeID, 1)
                SET @numParentAccId = 0
                SET @bitIsSubAccount = 0
            END
                        
			IF (@bitIsSubAccount=1)
			BEGIN
				SELECT  @vcAccountCode = dbo.GetAccountTypeCode(@numDomainId, @numParentAccId, 3)
				SELECT @numParntAcntTypeID = numParntAcntTypeId FROM Chart_Of_Accounts WHERE numDomainId=@numDomainId AND numAccountId = @numParentAccId
			END
			
            UPDATE  
				Chart_Of_Accounts
            SET     
				numAcntTypeId = @numAcntTypeId,
                numParntAcntTypeId = @numParntAcntTypeID,
                vcAccountName = @vcAccountNameWithNumber
                + @vcAccountName,
                vcAccountDescription = @vcAccountDescription,
                bitActive = @bitActive,
                bitProfitLoss = @bitProfitLoss,
                [numOriginalOpeningBal] = @monOriginalOpeningBal,
                [dtOpeningDate] = @dtOpeningDate,
                [bitDepreciation] = @bitDepreciation,
                [dtDepreciationCostDate] = @dtDepreciationCostDate,
                [monDepreciationCost] = @monDepreciationCost,
                vcAccountCode = @vcAccountCode,
                bitIsSubAccount = @bitIsSubAccount,
                vcNumber = @vcNumber,
                numParentAccId = @numParentAccId,
				IsBankAccount = @IsBankAccount,
				vcStartingCheckNumber = @vcStartingCheckNumber,
				vcBankRountingNumber = @vcBankRountingNumber,
				IsConnected = @bitIsConnected,
				numBankDetailID = @numBankDetailID
            WHERE   
				(numAccountId = @numAccountId) 
				AND ( numDomainId = @numDomainId )
			
		--UPDATE dbo.Chart_Of_Accounts SET intLevel = 0 WHERE numAccountId = @numAccountId
            UPDATE  
				dbo.Chart_Of_Accounts
            SET 
				intLevel = CASE 
							WHEN numParentAccId = 0 AND bitIsSubAccount = 0 THEN 0
							ELSE ( SELECT ISNULL(intLevel,0) + 1 FROM dbo.Chart_Of_Accounts WHERE numAccountId = @numParentAccId )
							END
            WHERE (bitIsSubAccount = 1 OR bitIsSubAccount = 0) AND numAccountId = @numAccountId

            EXEC USP_ManageChartAccountOpening @numDomainId,@numAccountId, @monOriginalOpeningBal, @dtOpeningDate                                                                      
            
		END
    
    
		--Maintain Sorting by Account code when adding new account with sub account  or  updating Account numeber
		PRINT @intFlag
		if (@intFlag =1) -- @vcNumber will have -1 value only when its being called from [USP_ChartAcntDefaultValues], as well cursor below creates time out.
		BEGIN
			UPDATE dbo.Chart_Of_Accounts SET vcAccountCode='' WHERE numParntAcntTypeId = @numParntAcntTypeID  AND numDomainId=@numDomainID


			DECLARE @TEMP TABLE
			(
				ID INT IDENTITY(1,1)
				,numAccountId NUMERIC(18,0)
				,bitIsSubAccount BIT
				,numParentAccId NUMERIC(18,0)
			)

			;WITH CTE(tintLevel, numAccountId, vcAccountName, bitIsSubAccount, numParentAccId) AS
			(
				SELECT 
					0
					,numAccountId
					,vcAccountName
					,bitIsSubAccount
					,numParentAccId
				FROM 
					dbo.Chart_Of_Accounts 
				WHERE 
					numParntAcntTypeId = @numParntAcntTypeID 
					AND ISNULL(numParentAccId,0) = 0
				UNION ALL
				SELECT 
					c.tintLevel + 1
					,COA.numAccountId
					,COA.vcAccountName
					,COA.bitIsSubAccount
					,COA.numParentAccId
				FROM
					dbo.Chart_Of_Accounts  COA
				INNER JOIN
					CTE c
				ON
					COA.numParentAccId=C.numAccountId
				WHERE 
					numParntAcntTypeId = @numParntAcntTypeID 
			)

			INSERT INTO @TEMP
			(
				numAccountId
				,bitIsSubAccount
				,numParentAccId
			)
			SELECT
				numAccountId
				,bitIsSubAccount
				,numParentAccId
			FROM 
				CTE 
			ORDER BY 
				tintLevel,vcAccountName



			Declare @numTempAccountID NUMERIC
			Declare @bitTempIsSubAccount bit
			Declare @numTempParentAccId NUMERIC
			DECLARE @vcTempAccountCode VARCHAR(100)


			DECLARE @i AS INT  = 1
			DECLARE @iCount AS INT

			SELECT @iCount=COUNT(*) FROM @TEMP

			WHILE @i <= @iCount
			BEGIN
				SELECT @numTempAccountID=numAccountId,@bitTempIsSubAccount=bitIsSubAccount,@numTempParentAccId=numParentAccId  FROM @TEMP WHERE ID=@i
				

				IF @bitTempIsSubAccount =1 
					SELECT @vcTempAccountCode = dbo.GetAccountTypeCode(@numDomainId, @numTempParentAccId, 3)
				ELSE
					SELECT @vcTempAccountCode = dbo.GetAccountTypeCode(@numDomainId, @numParntAcntTypeID, 1)


				UPDATE dbo.Chart_Of_Accounts SET vcAccountCode=@vcTempAccountCode WHERE numAccountId=@numTempAccountID

				SET @i = @i + 1
			END
		END

		--Maintain only one profit and loss account through out system
		IF @bitProfitLoss = 1 
        BEGIN
            UPDATE  dbo.Chart_Of_Accounts
            SET     bitProfitLoss = 0
            WHERE   numAccountId <> @numAccountId
                    AND numDomainId = @numDomainID
        END
	COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorNumber INT
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT
		DECLARE @ErrorLine INT
		DECLARE @ErrorProcedure NVARCHAR(200)

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorNumber = ERROR_NUMBER(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@ErrorLine = ERROR_LINE(),
			@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

		RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
	END CATCH
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageTransactionHistory')
	DROP PROCEDURE USP_ManageTransactionHistory
GO

CREATE PROCEDURE [dbo].[USP_ManageTransactionHistory]
	@numTransHistoryID numeric(18, 0) OUTPUT,
	@numDomainID numeric(18, 0),
	@numDivisionID numeric(18, 0),
	@numContactID numeric(18, 0),
	@numOppID numeric(18, 0),
	@numOppBizDocsID numeric(18, 0),
	@vcTransactionID varchar(100),
	@tintTransactionStatus tinyint,
	@vcPGResponse varchar(200),
	@tintType tinyint,
	@monAuthorizedAmt money,
	@monCapturedAmt money,
	@monRefundAmt money,
	@vcCardHolder varchar(500),
	@vcCreditCardNo varchar(500),
	@vcCVV2 varchar(200),
	@tintValidMonth tinyint,
	@intValidYear int,
	@vcSignatureFile VARCHAR(100),
	@numUserCntID numeric(18, 0),
	@numCardType NUMERIC(9,0),
	@vcResponseCode VARCHAR(500) = ''
AS

 SET NOCOUNT ON
 SET TRANSACTION ISOLATION LEVEL READ COMMITTED


IF EXISTS(SELECT [numTransHistoryID] FROM [dbo].[TransactionHistory] WHERE [numTransHistoryID] = @numTransHistoryID)
BEGIN
	DECLARE @OldCapturedAmt AS MONEY
	DECLARE @OldRefundAmt AS MONEY
	DECLARE @OldAuthorizedAmt AS MONEY
	
	SELECT @OldAuthorizedAmt = ISNULL(monAuthorizedAmt,0), @OldCapturedAmt = ISNULL(monCapturedAmt,0),@OldRefundAmt = ISNULL(monRefundAmt,0) FROM [dbo].[TransactionHistory] WHERE [numTransHistoryID] = @numTransHistoryID
	
	IF @tintTransactionStatus =2 -- captured
		SET @OldAuthorizedAmt = @monCapturedAmt
	IF @tintTransactionStatus =5 -- Credited
		SET @OldRefundAmt = @monRefundAmt + @OldRefundAmt
	
	
	UPDATE [dbo].[TransactionHistory] SET
		[tintTransactionStatus] = @tintTransactionStatus,
		[monAuthorizedAmt] = @OldAuthorizedAmt,
		[monCapturedAmt] = @OldCapturedAmt,
		[monRefundAmt] = @OldRefundAmt,
		
		[numModifiedBy] = @numUserCntID,
		[dtModifiedDate] = GETUTCDATE()
	WHERE
		[numTransHistoryID] = @numTransHistoryID AND numDomainID=@numDomainID
END
ELSE
BEGIN
	INSERT INTO [dbo].[TransactionHistory] (
		[numDomainID],
		[numDivisionID],
		[numContactID],
		[numOppID],
		[numOppBizDocsID],
		[vcTransactionID],
		[tintTransactionStatus],
		[vcPGResponse],
		[tintType],
		[monAuthorizedAmt],
		[monCapturedAmt],
		[monRefundAmt],
		[vcCardHolder],
		[vcCreditCardNo],
		[vcCVV2],
		[tintValidMonth],
		[intValidYear],
		vcSignatureFile,
		[numCreatedBy],
		[dtCreatedDate],
		[numModifiedBy],
		[dtModifiedDate],
		[numCardType],
		[vcResponseCode]
	) VALUES (
		@numDomainID,
		@numDivisionID,
		@numContactID,
		@numOppID,
		@numOppBizDocsID,
		@vcTransactionID,
		@tintTransactionStatus,
		@vcPGResponse,
		@tintType,
		@monAuthorizedAmt,
		@monCapturedAmt,
		@monRefundAmt,
		@vcCardHolder,
		@vcCreditCardNo,
		@vcCVV2,
		@tintValidMonth,
		@intValidYear,
		@vcSignatureFile,
		@numUserCntID,
		GETUTCDATE(),
		@numUserCntID,
		GETUTCDATE(),
		@numCardType,
		@vcResponseCode
	)
	
	SET @numTransHistoryID = @@IDENTITY
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OPPGetINItems')
DROP PROCEDURE USP_OPPGetINItems
GO
-- =============================================  
-- Modified by: <Author,,Sachin Sadhu>  
-- Create date: <Create Date,,24thDec2013>  
-- Description: <Description,,add 3 fields:vcBizDocImagePath,vcBizDocFooter,vcPurBizDocFooter>  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_OPPGetINItems]
(
          @byteMode        AS TINYINT  = NULL,
          @numOppId        AS NUMERIC(9)  = NULL,
          @numOppBizDocsId AS NUMERIC(9)  = NULL,
          @numDomainID     AS NUMERIC(9)  = 0,
          @numUserCntID    AS NUMERIC(9)  = 0,
          @ClientTimeZoneOffset INT=0)
AS
  IF @byteMode = 1
    BEGIN
      DECLARE  @BizDcocName  AS VARCHAR(100)
      DECLARE  @OppName  AS VARCHAR(100)
      DECLARE  @Contactid  AS VARCHAR(100)
      DECLARE  @shipAmount  AS MONEY
      DECLARE  @tintOppType  AS TINYINT
      DECLARE  @numlistitemid  AS VARCHAR(15)
      DECLARE  @RecOwner  AS VARCHAR(15)
      DECLARE  @MonAmount  AS VARCHAR(15)
      DECLARE  @OppBizDocID  AS VARCHAR(100)
      DECLARE  @bizdocOwner  AS VARCHAR(15)
      DECLARE  @tintBillType  AS TINYINT
      DECLARE  @tintShipType  AS TINYINT
      DECLARE  @numCustomerDivID AS NUMERIC 
      
      SELECT @RecOwner = numRecOwner,@MonAmount = CONVERT(VARCHAR(20),monPAmount),
             @Contactid = numContactId,@OppName = vcPOppName,
             @tintOppType = tintOppType,@tintBillType = tintBillToType,@tintShipType = tintShipToType
      FROM   OpportunityMaster WHERE  numOppId = @numOppId
      
      -- When Creating PO from SO and Bill type is Customer selected 
      SELECT @numCustomerDivID = ISNULL(numDivisionID,0) FROM dbo.OpportunityMaster WHERE 
			numOppID IN (SELECT  ISNULL(numParentOppID,0) FROM dbo.OpportunityLinking WHERE numChildOppID = @numOppId)
			
      --select @BizDcocName=vcBizDocID from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsId
      SELECT @BizDcocName = vcdata,
             @shipAmount = monShipCost,
             @numlistitemid = numlistitemid,
             @OppBizDocID = vcBizDocID,
             @bizdocOwner = OpportunityBizDocs.numCreatedBy
      FROM   listdetails JOIN OpportunityBizDocs ON numBizDocId = numlistitemid
      WHERE  numOppBizDocsId = @numOppBizDocsId
      
      SELECT @bizdocOwner AS BizDocOwner,
             @OppBizDocID AS BizDocID,
             @MonAmount AS Amount,
             isnull(@RecOwner,1) AS Owner,
             @BizDcocName AS BizDcocName,
             @numlistitemid AS BizDoc,
             @OppName AS OppName,
             VcCompanyName AS CompName,
             @shipAmount AS ShipAmount,
             dbo.fn_getOPPAddress(@numOppId,@numDomainID,1) AS BillAdd,
             dbo.fn_getOPPAddress(@numOppId,@numDomainID,2) AS ShipAdd,
             AD.VcStreet,
             AD.VcCity,
             isnull(dbo.fn_GetState(AD.numState),'') AS vcBilState,
             AD.vcPostalCode,
             isnull(dbo.fn_GetListItemName(AD.numCountry),
                    '') AS vcBillCountry,
             con.vcFirstName AS ConName,
             ISNULL(con.numPhone,'') numPhone,
             con.vcFax,
             isnull(con.vcEmail,'') AS vcEmail,
             div.numDivisionID,
             numContactid,
             isnull(vcFirstFldValue,'') vcFirstFldValue,
             isnull(vcSecndFldValue,'') vcSecndFldValue,
             isnull(bitTest,0) AS bitTest,
			dbo.fn_getOPPAddress(@numOppId,@numDomainID,1) AS BillToAddressName,
			dbo.fn_getOPPAddress(@numOppId,@numDomainID,1) AS ShipToAddressName,
             CASE 
               WHEN @tintBillType IS NULL THEN Com.vcCompanyName
             	-- When Sales order and bill to is set to customer 
               WHEN @tintBillType = 1 AND @tintOppType = 1 THEN Com.vcCompanyName
             -- When Create PO from SO and Bill to is set to Customer 
			   WHEN @tintBillType = 1 AND @tintOppType = 2 THEN dbo.fn_GetComapnyName(@numCustomerDivID)
               WHEN @tintBillType = 0 THEN (SELECT TOP 1 Com1.vcCompanyName
                                            FROM   companyinfo [Com1]
                                                   JOIN divisionmaster div1
                                                     ON com1.numCompanyID = div1.numCompanyID
                                                   JOIN Domain D1
                                                     ON D1.numDivisionID = div1.numDivisionID
                                                    JOIN dbo.AddressDetails AD1
														ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary=1
                                            WHERE  D1.numDomainID = @numDomainID)
               WHEN @tintBillType = 2 THEN (SELECT vcBillCompanyName
                                            FROM   OpportunityAddress
                                            WHERE  numOppID = @numOppId)
				WHEN @tintBillType = 3 THEN  Com.vcCompanyName
              END AS BillToCompanyName,
             CASE 
               WHEN @tintShipType IS NULL THEN Com.vcCompanyName
               WHEN @tintShipType = 1 AND @tintOppType = 1 THEN Com.vcCompanyName
				-- When Create PO from SO and Ship to is set to Customer 
   			   WHEN @tintShipType = 1 AND @tintOppType = 2 THEN dbo.fn_GetComapnyName(@numCustomerDivID)
               WHEN @tintShipType = 0 THEN (SELECT TOP 1 Com1.vcCompanyName
                                            FROM   companyinfo [Com1]
                                                   JOIN divisionmaster div1
                                                     ON com1.numCompanyID = div1.numCompanyID
                                                   JOIN Domain D1
                                                     ON D1.numDivisionID = div1.numDivisionID
                                                    JOIN dbo.AddressDetails AD1
													 ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 2 AND bitIsPrimary=1
                                            WHERE  D1.numDomainID = @numDomainID)
               WHEN @tintShipType = 2 THEN (SELECT vcShipCompanyName
                                            FROM   OpportunityAddress
                                            WHERE  numOppID = @numOppId)
				WHEN @tintShipType = 3 THEN (SELECT vcShipCompanyName
														FROM   OpportunityAddress
														WHERE  numOppID = @numOppId)
             END AS ShipToCompanyName
      FROM   companyinfo [Com] JOIN divisionmaster div ON com.numCompanyID = div.numCompanyID
             JOIN AdditionalContactsInformation con ON con.numdivisionId = div.numdivisionId
             JOIN Domain D ON D.numDomainID = div.numDomainID
             LEFT JOIN PaymentGatewayDTLID PGDTL ON PGDTL.intPaymentGateWay = D.intPaymentGateWay AND D.numDomainID = PGDTL.numDomainID
             LEFT JOIN AddressDetails AD --Billing add
				ON AD.numDomainID=DIV.numDomainID AND AD.numRecordID=div.numDivisionID AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1
			 LEFT JOIN AddressDetails AD2--Shipping address
				ON AD2.numDomainID=DIV.numDomainID AND AD2.numRecordID=div.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary=1	
      WHERE  numContactid = @Contactid AND Div.numDomainID = @numDomainID
    END
  IF @byteMode = 2
    BEGIN
      SELECT opp.vcBizDocID,
             Mst.fltDiscount AS decDiscount,
             isnull(opp.monAmountPaid,0) AS monAmountPaid,
             isnull(opp.vcComments,'') AS vcComments,
             CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffset,opp.dtCreatedDate)) dtCreatedDate,
             dbo.fn_GetContactName(opp.numCreatedby) AS numCreatedby,
             dbo.fn_GetContactName(opp.numModifiedBy) AS numModifiedBy,
             CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffset,opp.dtModifiedDate)) dtModifiedDate,
             CASE 
               WHEN isnull(opp.numViewedBy,0) = 0 THEN ''
               ELSE dbo.fn_GetContactName(opp.numViewedBy)
             END AS numViewedBy,
             CASE 
               WHEN opp.numViewedBy = 0 THEN NULL
               ELSE opp.dtViewedDate
             END AS dtViewedDate,
             isnull(Mst.bitBillingTerms,0) AS tintBillingTerms,
             isnull(Mst.intBillingDays,0) AS numBillingDays,
			 ISNULL(BTR.numNetDueInDays,0) AS numBillingDaysName,
			 BTR.vcTerms AS vcBillingTermsName,
             isnull(Mst.bitInterestType,0) AS tintInterestType,
             isnull(Mst.fltInterest,0) AS fltInterest,
             tintOPPType,
             Opp.numBizDocId,
             dbo.fn_GetContactName(numApprovedBy) AS ApprovedBy,
             dtApprovedDate,
             Mst.bintAccountClosingDate,
             tintShipToType,
             tintBillToType,
             tintshipped,
             dtShippedDate bintShippedDate,
             isnull(opp.monShipCost,0) AS monShipCost,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND numContactID = @numUserCntID
                     AND cDocType = 'B'
                     AND tintApprove = 0) AS AppReq,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND cDocType = 'B'
                     AND tintApprove = 1) AS Approved,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND cDocType = 'B'
                     AND tintApprove = 2) AS Declined,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND cDocType = 'B'
                     AND tintApprove = 0) AS Pending,
             ISNULL(BT.vcBizDocFooter,D.vcBizDocFooter) AS BizdocFooter,
             ISNULL(BT.vcBizDocImagePath,D.vcBizDocImagePath) As vcBizDocImagePath, 
             Mst.numDivisionID,
             ISNULL(BT.vcPurBizDocFooter,D.vcPurBizDocFooter) AS PurBizdocFooter, 
             isnull(Mst.fltDiscount,0) AS fltDiscount,
             isnull(Mst.bitDiscountType,0) AS bitDiscountType,
             isnull(Opp.numShipVia,ISNULL(Mst.numShipmentMethod,0)) AS numShipVia,
--             isnull(vcTrackingURL,'') AS vcTrackingURL,
			ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=MSt.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=Opp.numShipVia  AND vcFieldName='Tracking URL')),'')		AS vcTrackingURL,
             isnull(Opp.numBizDocStatus,0) AS numBizDocStatus,
             CASE 
               WHEN Opp.numBizDocStatus IS NULL THEN '-'
               ELSE dbo.fn_GetListItemName(Opp.numBizDocStatus)
             END AS BizDocStatus,
             CASE 
               WHEN Opp.numShipVia IS NULL THEN (CASE WHEN Mst.numShipmentMethod IS NULL THEN '-' WHEN Mst.numShipmentMethod = -1 THEN 'Will-call' ELSE dbo.fn_GetListItemName(Opp.numShipVia) END)
			   WHEN Opp.numShipVia = -1 THEN 'Will-call'
               ELSE dbo.fn_GetListItemName(Opp.numShipVia)
             END AS ShipVia,
             ISNULL(C.varCurrSymbol,'') varCurrSymbol,
             (SELECT COUNT(*) FROM [ShippingReport] WHERE numOppBizDocId = opp.numOppBizDocsId )AS ShippingReportCount,
			 isnull(bitPartialFulfilment,0) bitPartialFulfilment,
			 ISNULL(Opp.vcBizDocName,'') vcBizDocName,
			 dbo.[fn_GetContactName](opp.[numModifiedBy])  + ', '
				+ CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.dtModifiedDate)) AS ModifiedBy,
			 dbo.[fn_GetContactName](Mst.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](Mst.[numAssignedTo]) AS OrderRecOwner,
			 dbo.[fn_GetContactName](DM.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](DM.[numAssignedTo]) AS AccountRecOwner,
			 Opp.dtFromDate,
			 Mst.tintTaxOperator,
			 dbo.GetAccountedEmbeddedCost(opp.numOppBizDocsID,1) AS monTotalEmbeddedCost,
			 dbo.GetAccountedEmbeddedCost(opp.numOppBizDocsID,2) AS monTotalAccountedCost,
			 ISNULL(BT.bitEnabled,0) bitEnabled,
			 ISNULL(BT.txtBizDocTemplate,'') txtBizDocTemplate, 
			 ISNULL(BT.txtCSS,'') txtCSS,
			 ISNULL(BT.numOrientation,0) numOrientation,
			 ISNULL(BT.bitKeepFooterBottom,0) bitKeepFooterBottom,
			 dbo.fn_GetContactName(Mst.numAssignedTo) AS AssigneeName,
			 ISNULL((SELECT vcEmail FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneeEmail,
			 ISNULL((SELECT numPhone FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneePhone,
			 dbo.fn_GetComapnyName(Mst.numDivisionId) OrganizationName,
			 dbo.fn_GetContactName(Mst.numContactID)  OrgContactName,

--CASE WHEN Mst.tintOppType = 1 THEN 
--dbo.getCompanyAddress((select ISNULL(DM.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC INNER JOIN DivisionMaster DM 
--on AC.numDivisionID = DM.numDivisionID where numContactID = Mst.numCreatedBy),1,Mst.numDomainId)
--               ELSE dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId)
--             END AS CompanyBillingAddress1,

CASE WHEN Mst.tintOppType = 1 THEN 
dbo.getCompanyAddress((select ISNULL(AC.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC
INNER JOIN Domain D ON D.numDomainID = AC.numDomainID where AC.numContactID = D.numAdminID AND D.numDomainID = Mst.numDomainId),1,Mst.numDomainId)
               ELSE dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId)
             END AS CompanyBillingAddress,

CASE WHEN Mst.tintOppType = 1 THEN 
dbo.getCompanyAddress((select ISNULL(AC.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC
INNER JOIN Domain D ON D.numDomainID = AC.numDomainID where AC.numContactID = D.numAdminID AND D.numDomainID = Mst.numDomainId),1,Mst.numDomainId)
               ELSE dbo.getCompanyAddress(Mst.numDivisionId,2,Mst.numDomainId)
             END AS CompanyShippingAddress,
--
--			 dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId) CompanyBillingAddress,
--			 dbo.getCompanyAddress(Mst.numDivisionId,2,Mst.numDomainId) CompanyShippingAddress,
			 case when ACI.numPhone<>'' then ACI.numPhone +case when ACI.numPhoneExtension<>'' then ' - ' + ACI.numPhoneExtension else '' end  else '' END OrgContactPhone,
			 DM.vcComPhone as OrganizationPhone,
             ISNULL(ACI.vcEmail,'') AS OrgContactEmail,
 			 dbo.[fn_GetContactName](Mst.[numRecOwner]) AS OnlyOrderRecOwner,
 			 ISNULL(Opp.bitAuthoritativeBizDocs,0) bitAuthoritativeBizDocs,
			 ISNULL(Opp.tintDeferred,0) tintDeferred,isnull(Opp.monCreditAmount,0) AS monCreditAmount,
			 isnull(Opp.bitRentalBizDoc,0) as bitRentalBizDoc,isnull(BT.numBizDocTempID,0) as numBizDocTempID,isnull(BT.vcTemplateName,'') as vcTemplateName,
		     [dbo].[GetDealAmount](opp.numOppId ,getutcdate(),opp.numOppBizDocsId ) as monPAmount,
			 --(select top 1 SD.vcSignatureFile from SignatureDetail SD join OpportunityBizDocsDetails OBD on SD.numSignID=OBD.numSignID where SD.numDomainID = @numDomainID and OBD.numDomainID = @numDomainID and OBD.numSignID IS not null and OBD.numBizDocsId=opp.numOppBizDocsId order by OBD.numBizDocsPaymentDetId desc) as vcSignatureFile,
			 isnull(CMP.txtComments,'') as vcOrganizationComments,
			 isnull(Opp.vcTrackingNo,'') AS  vcTrackingNo,isnull(Opp.vcShippingMethod,'') AS  vcShippingMethod,
			 Opp.dtDeliveryDate,
			 CASE WHEN Opp.vcRefOrderNo IS NULL OR LEN(Opp.vcRefOrderNo)=0 THEN ISNULL(Mst.vcOppRefOrderNo,'') ELSE 
			  isnull(Opp.vcRefOrderNo,'') END AS vcRefOrderNo,ISNULL(Mst.numDiscountAcntType,0) AS numDiscountAcntType,
			 ISNULL(opp.numSequenceId,'') AS numSequenceId,
			 CASE WHEN ISNULL(Opp.numBizDocId,0)=296 THEN ISNULL((SELECT SUBSTRING((SELECT '$^$' + dbo.fn_GetListItemName(numBizDocStatus) +'#^#'+ dbo.fn_GetContactName(numUserCntID) +'#^#'+ dbo.FormatedDateTimeFromDate(DateAdd(minute, -@ClientTimeZoneOffset , dtCreatedDate),@numDomainId) + '#^#'+ isnull(DFCS.vcColorScheme,'NoForeColor')
					FROM OppFulfillmentBizDocsStatusHistory OFBS left join DycFieldColorScheme DFCS on OFBS.numBizDocStatus=DFCS.vcFieldValue AND DFCS.numDomainID=@numDomainID
					WHERE numOppId=opp.numOppId and numOppBizDocsId=opp.numOppBizDocsId FOR XML PATH('')),4,200000)),'') ELSE '' END AS vcBizDocsStatusList,ISNULL(Mst.numCurrencyID,0) AS numCurrencyID,
			ISNULL(Opp.fltExchangeRateBizDoc,Mst.fltExchangeRate) AS fltExchangeRateBizDoc,
			com1.vcCompanyName AS EmployerOrganizationName,div1.vcComPhone as EmployerOrganizationPhone,ISNULL(opp.bitAutoCreated,0) AS bitAutoCreated,
			ISNULL(RecurrenceConfiguration.numRecConfigID,0) AS numRecConfigID,
			(CASE WHEN RecurrenceConfiguration.numRecConfigID IS NULL THEN 0 ELSE 1 END) AS bitRecur,
			(CASE WHEN RecurrenceTransaction.numRecTranID IS NULL THEN 0 ELSE 1 END) AS bitRecurred,
			ISNULL(Mst.bitRecurred,0) AS bitOrderRecurred,
			dbo.FormatedDateFromDate(RecurrenceConfiguration.dtStartDate,@numDomainID) AS dtStartDate,
			RecurrenceConfiguration.vcFrequency AS vcFrequency,
			ISNULL(RecurrenceConfiguration.bitDisabled,0) AS bitDisabled,
			ISNULL(opp.bitFulfilled,0) AS bitFulfilled,
			ISNULL(opp.numDeferredBizDocID,0) AS numDeferredBizDocID,
			dbo.GetTotalQtyByUOM(opp.numOppBizDocsId) AS vcTotalQtybyUOM,
			ISNULL(Mst.intUsedShippingCompany,0) AS intUsedShippingCompany,
			dbo.FormatedDateFromDate(Mst.dtReleaseDate,@numDomainID) AS vcReleaseDate,
			(CASE WHEN ISNULL(Mst.numPartner,0) > 0 THEN ISNULL((SELECT CONCAT(DivisionMaster.vcPartnerCode,'-',CompanyInfo.vcCompanyName) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE DivisionMaster.numDivisionID=Mst.numPartner),'') ELSE '' END) AS vcPartner
		FROM   OpportunityBizDocs opp JOIN OpportunityMaster Mst ON Mst.numOppId = opp.numOppId
             JOIN Domain D ON D.numDomainID = Mst.numDomainID
             LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = Mst.numCurrencyID
             LEFT JOIN [DivisionMaster] DM ON DM.numDivisionID = Mst.numDivisionID   
			 LEFT JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = @numDomainID  
             LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = Mst.numContactID
             LEFT JOIN [BizDocTemplate] BT ON BT.numDomainID = @numDomainID AND BT.numBizDocID = Opp.numBizDocID AND BT.numOppType = Mst.tintOppType AND BT.tintTemplateType=0 and BT.numBizDocTempID=opp.numBizDocTempID
             LEFT JOIN BillingTerms BTR ON BTR.numTermsID = ISNULL(Mst.intBillingDays,0)
             JOIN divisionmaster div1 ON D.numDivisionID = div1.numDivisionID
			 JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
			 LEFT JOIN RecurrenceConfiguration ON Opp.numOppBizDocsId = RecurrenceConfiguration.numOppBizDocID
			 LEFT JOIN RecurrenceTransaction ON Opp.numOppBizDocsId = RecurrenceTransaction.numRecurrOppBizDocID
      WHERE  opp.numOppBizDocsId = @numOppBizDocsId
             AND Mst.numDomainID = @numDomainID
    END
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount money =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(9),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0,
  @numShipmentMethod NUMERIC(18,0)=0,
  @dtReleaseDate DATE = NULL,
  @numPartner NUMERIC(18,0)=0,
  @tintClickBtn INT=0,
  @numPartenerContactId NUMERIC(18,0)=0,
  @numAccountClass NUMERIC(18,0) = 0,
  @numWillCallWarehouseID NUMERIC(18,0) = 0,
  @numVendorAddressID NUMERIC(18,0) = 0
)                                                                          
                                                                          
as  
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @fltExchangeRate float                                 
	DECLARE @hDocItem int                                                                                                                            
	DECLARE @TotalAmount as money                                                                          
	DECLARE @tintOppStatus as tinyint  
	
	IF ISNULL(@tintOppType,0) = 0
	BEGIN
		RAISERROR('OPP_TYPE_CAN_NOT_BE_0',16,1)
		RETURN
	END             

	IF CONVERT(VARCHAR(10),@strItems) <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

		--VALIDATE PROMOTION OF COUPON CODE
		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1),
			numOppItemID NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numOppItemID,
			numPromotionID
		)
		SELECT
			numoppitemtCode,
			numPromotionID
		FROM
			OPENXML (@hDocItem,'/NewDataSet/Item',2)
		WITH
		(
			numoppitemtCode NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		IF (SELECT
				COUNT(*)
			FROM
				PromotionOffer PO
			WHERE
				numDomainId=@numDomainId
				AND bitRequireCouponCode = 1
				AND ISNULL(tintUsageLimit,0) > 0
				AND (intCouponCodeUsed 
					- (CASE WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE numOppID=ISNULL(@numOppID,0) AND numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END) 
					+ (CASE WHEN ISNULL((SELECT COUNT(*) FROM @TEMP WHERE numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END)) > tintUsageLimit) > 0
		BEGIN
			RAISERROR('USAGE_OF_COUPON_EXCEED',16,1)
			RETURN
		END
		ELSE
		BEGIN
			UPDATE
				PO
			SET 
				intCouponCodeUsed = ISNULL(intCouponCodeUsed,0) 
									- (CASE WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE numOppID=ISNULL(@numOppID,0) AND numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END) 
									+ (CASE WHEN ISNULL(T1.intUsed,0) > 0 THEN 1 ELSE 0 END) 
			FROM
				PromotionOffer PO
			INNER JOIN
				(
					SELECT
						numPromotionID,
						COUNT(*) AS intUsed
					FROM
						@TEMP
					GROUP BY
						numPromotionID
				) AS T1
			ON
				PO.numProId = T1.numPromotionID
			WHERE
				numDomainId=@numDomainId
				AND bitRequireCouponCode = 1
		END
	END

	--IF NEW OPPERTUNITY
	IF @numOppID = 0                                                                          
	BEGIN
		DECLARE @bitIsInitalSalesOrder BIT
		IF(@DealStatus=1 AND @tintOppType=1)
		BEGIN
			SET @bitIsInitalSalesOrder=1
		END

		DECLARE @intOppTcode AS NUMERIC(9)  
  
		SELECT @intOppTcode = MAX(numOppId) FROM OpportunityMaster                

		SET @intOppTcode = ISNULL(@intOppTcode,0) + 1                                                
		
		SET @vcPOppName = @vcPOppName + '-'+ CONVERT(varchar(4),YEAR(GETUTCDATE()))  +'-A'+ CONVERT(varchar(10),@intOppTcode)                                  
  
		IF ISNULL(@numCurrencyID,0) = 0 
		BEGIN
			SET @fltExchangeRate=1
			SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
		END
		ELSE
		BEGIN 
			SET @fltExchangeRate = dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
        END  
		                                                        
		IF ISNULL(@numAccountClass,0) = 0
		BEGIN                                                  
		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivisionID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
		END

        IF(@DealStatus=1 AND @tintOppType=1 AND @tintClickBtn=1)
		BEGIN
			SET @DealStatus=0
		END         
		--PRINT ('Deal Staus ' +@DealStatus)                                                      
		INSERT INTO OpportunityMaster
		(                                                                             
			numContactId,numDivisionId,txtComments,numCampainID,bitPublicFlag,tintSource,tintSourceType,vcPOppName,                                                                          
			intPEstimatedCloseDate,monPAmount,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,                                                                                                             
			numRecOwner,lngPConclAnalysis,tintOppType,numSalesOrPurType,numCurrencyID,fltExchangeRate,numAssignedTo,
			numAssignedBy,[tintOppStatus],numStatus,vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,
			bitInterestType,fltInterest,vcCouponCode,bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,
			vcMarketplaceOrderReportId,numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
			bitUseMarkupShippingRate,numMarkupShippingRate,intUsedShippingCompany,numShipmentMethod,numPartner,numPartenerContact,dtReleaseDate,bitIsInitalSalesOrder,numVendorAddressID
		)                                                                          
		Values                                                                          
		(                                                                          
			@numContactId,@numDivisionId,ISNULL(@Comments,''),@CampaignID,@bitPublicFlag,@tintSource,@tintSourceType,@vcPOppName,                                                                          
			@dtEstimatedCloseDate,@monPAmount,@numUserCntID,(CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END),                                                                          
			@numUserCntID,GETUTCDATE(),@numDomainId,@numUserCntID,@lngPConclAnalysis,@tintOppType,@numSalesOrPurType,@numCurrencyID, 
			@fltExchangeRate,(CASE WHEN @numAssignedTo > 0 THEN @numAssignedTo ELSE NULL END),(CASE WHEN @numAssignedTo>0 THEN @numUserCntID ELSE NULL END),
			@DealStatus,@numStatus,@vcOppRefOrderNo,@bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,
			@vcCouponCode,@bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,@numPercentageComplete,
			@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,@bitUseMarkupShippingRate,@numMarkupShippingRate,@intUsedShippingCompany,@numShipmentMethod,@numPartner,@numPartenerContactId,@dtReleaseDate,@bitIsInitalSalesOrder,@numVendorAddressID
		)                                                                                                                      
		
		SET @numOppID=(SELECT SCOPE_IDENTITY())                
                            
		
		--UPDATE OPPNAME AS PER NAME TEMPLATE
		EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

		--MAP CUSTOM FIELD	
		DECLARE @tintPageID AS TINYINT;
		SET @tintPageID = CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
		EXEC dbo.USP_AddParentChildCustomFieldMap
				@numDomainID = @numDomainID, --  numeric(18, 0)
				@numRecordID = @numOppID, --  numeric(18, 0)
				@numParentRecId = @numDivisionId, --  numeric(18, 0)
				@tintPageID = @tintPageID --  tinyint
 	
		IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
		BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END
  
		-- INSERTING ITEMS                                                                                                                    
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,
				monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
				numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,vcAttrValues,monAvgCost,bitItemPriceApprovalRequired,
				numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,numCost,vcNotes
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
				(CASE X.numWarehouseItmsID 
				WHEN -1 
				THEN 
					CASE 
						WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU= X.vcSKU) > 0)
							THEN
								(SELECT TOP 1 
									numWareHouseItemID 
								FROM 
									WareHouseItems 
								WHERE 
									[numItemID] = X.numItemCode 
									AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
						ELSE
							(SELECT 
								[numWareHouseItemID] 
							FROM 
								[WareHouseItems] 
							WHERE 
								[numItemID] = X.numItemCode 
								AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				WHEN 0 
				THEN 
					CASE 
					WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU=X.vcSKU) > 0)
						THEN
							(SELECT TOP 1 
								numWareHouseItemID 
							FROM 
								WareHouseItems 
							WHERE 
								[numItemID] = X.numItemCode 
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
					ELSE
						(SELECT 
							[numWareHouseItemID] 
						FROM 
							[WareHouseItems] 
						WHERE 
							[numItemID] = X.numItemCode 
							AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
							AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				ELSE  
					X.numWarehouseItmsID 
				END) AS numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
				(SELECT isnull(VN.monCost,0) FROM Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode AND VN.numVendorID=IT.numVendorID WHERE  VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,
				0,
				X.numToWarehouseItemID,X.Attributes,X.AttributeIDs,(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),
				X.bitItemPriceApprovalRequired,X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(X.vcVendorNotes,'')
			FROM
				(
					SELECT 
						*
					FROM 
						OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
					WITH                       
					(                                                                          
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9), numUnitHour FLOAT, monPrice DECIMAL(30,16), monTotAmount MONEY,vcItemDesc VARCHAR(1000),                                    
						numWarehouseItmsID NUMERIC(9), ItemType VARCHAR(30), DropShip BIT, bitDiscountType BIT,	fltDiscount DECIMAL(30,16), monTotAmtBefDiscount MONEY,
						vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),
						numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),AttributeIDs VARCHAR(500),vcSKU VARCHAR(100),bitItemPriceApprovalRequired BIT,
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost MONEY,vcVendorNotes VARCHAR(300)
					)
				)X    
			ORDER BY 
				X.numoppitemtCode
    
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,numWarehouseItmsID=X.numWarehouseItmsID,bitDropShip=X.DropShip,
				bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
				bitWorkOrder=X.bitWorkOrder,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,
				numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,vcAttributes=X.Attributes,
				numClassID=0,vcModelID=X.vcModelID,
				bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired,vcAttrValues=X.AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost,vcNotes=X.vcVendorNotes
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,
					monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,
					Attributes,bitItemPriceApprovalRequired,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) numCost,ISNULL(vcVendorNotes,'') vcVendorNotes
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
				WITH  
					(                      
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount MONEY,vcItemDesc VARCHAR(1000), vcModelID VARCHAR(300),
						numWarehouseItmsID NUMERIC(9),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount MONEY,vcItemName VARCHAR(300),
						numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),
						numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost MONEY,vcVendorNotes VARCHAR(300)
					)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID   
				
			--INSERT KIT ITEMS                 
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ID.numWareHouseItemId,
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0
			FROM 
				OpportunityItems OI 
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId 

			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
				DECLARE @TempKitConfiguration TABLE
				(
					numItemCode NUMERIC(18,0),
					numWarehouseItemID NUMERIC(18,0),
					KitChildItems VARCHAR(MAX)
				)

				INSERT INTO 
					@TempKitConfiguration
				SELECT
					numItemCode,
					numWarehouseItmsID,
					KitChildItems
				FROM
					OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)
				WITH                       
				(
					numItemCode NUMERIC(9),numWarehouseItmsID NUMERIC(9), KitChildItems VARCHAR(MAX)
				) 

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					ItemDetails.numWareHouseItemId,
					(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0
				FROM
					(
						SELECT 
							t1.numItemCode,
							t1.numWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										0,
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,0,CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										0, 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemWarehouseItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration WHERE numItemCode=t1.numItemCode AND numWarehouseItemID=t1.numWarehouseItemID),',') 
						) as t2
					) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numChildItemID=TempChildItems.numKitItemID
					AND OKI.numWarehouseItemId=TempChildItems.numKitWarehouseItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numWarehouseItmsID = TempChildItems.numWarehouseItemID
					AND OI.numoppitemtcode = OKI.numOppItemID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					AND (TempChildItems.numKitChildItemWarehouseItemID = ItemDetails.numWareHouseItemId  OR ISNULL(TempChildItems.numKitChildItemWarehouseItemID,0) = 0)
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END

			-- UPDATE UOM OF OPPORTUNITY ITEMS HAVE NOT UOM SET WHILE EXPORTED FROM MARKETPLACE (MEANS TINTSOURCETYPE = 3)
			IF ISNULL(@tintSourceType,0) = 3
			BEGIN
				UPDATE 
					OI 
				SET 
					numUOMID = ISNULL(I.numSaleUnit,0), 
					numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
				FROM 
					OpportunityItems OI
				JOIN 
					Item I 
				ON 
					OI.numItemCode = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND I.numDomainID = @numDomainId
			END	
                                     
			INSERT INTO OppWarehouseSerializedItem
			(
				numOppID,
				numWarehouseItmsDTLID,
				numOppItemID,
				numWarehouseItmsID
			)                      
			SELECT 
				@numOppID,
				X.numWarehouseItmsDTLID,
				X.numoppitemtCode,
				X.numWItmsID 
			FROM
			(                                                                          
				SELECT
					* 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
				WITH                        
				(                                               
				numWarehouseItmsDTLID numeric(9),                      
				numoppitemtCode numeric(9),                      
				numWItmsID numeric(9)                                                     
				)
			)X                                    
                                   
			UPDATE 
				OppWarehouseSerializedItem                       
			SET 
				numOppItemID=X.numoppitemtCode                                
			FROM                       
			(
				SELECT 
					numWarehouseItmsDTLID AS DTLID,
					O.numoppitemtCode 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
				WITH
				(
					numWarehouseItmsDTLID NUMERIC(9),numoppitemtCode NUMERIC(9),numWItmsID NUMERIC(9)
				)                                
				JOIN 
					OpportunityItems O 
				ON 
					O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID
			)X                 
			WHERE 
				numOppID=@numOppID 
				AND numWarehouseItmsDTLID=X.DTLID      
    
			EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
                     
			EXEC sp_xml_removedocument @hDocItem                                     
		END          
   
		--INSERT TAX FOR DIVISION   
		IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
		BEGIN
			INSERT INTO dbo.OpportunityMasterTaxItems 
			(
				numOppId,
				numTaxItemID,
				fltPercentage,
				tintTaxType,
				numTaxID
			) 
			SELECT 
				@numOppID,
				TI.numTaxItemID,
				TEMPTax.decTaxValue,
				TEMPTax.tintTaxType,
				0
			FROM 
				TaxItems TI 
			JOIN 
				DivisionTaxTypes DTT 
			ON 
				TI.numTaxItemID = DTT.numTaxItemID
			OUTER APPLY
			(
				SELECT
					decTaxValue,
					tintTaxType
				FROM
					dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
			) AS TEMPTax
			WHERE 
				DTT.numDivisionID=@numDivisionId 
				AND DTT.bitApplicable=1
			UNION 
			SELECT 
				@numOppID,
				0,
				TEMPTax.decTaxValue,
				TEMPTax.tintTaxType,
				0
			FROM 
				dbo.DivisionMaster 
			OUTER APPLY
			(
				SELECT
					decTaxValue,
					tintTaxType
				FROM
					dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
			) AS TEMPTax
			WHERE 
				bitNoTax=0 
				AND numDivisionID=@numDivisionID			
		END	
	END
	ELSE                                                                                                                          
	BEGIN
		DECLARE @tempAssignedTo AS NUMERIC(9) = NULL                                                    
		SELECT 
			@tintOppStatus = tintOppStatus,
			@tempAssignedTo=ISNULL(numAssignedTo,0) 
		FROM   
			OpportunityMaster 
		WHERE  
			numOppID = @numOppID

		IF @tempAssignedTo <> @numAssignedTo
		BEGIN
			IF (SELECT COUNT(*) FROM BizDocComission WHERE ISNULL(numOppID,0) = @numOppID AND ISNULL(bitCommisionPaid,0) = 1) > 0
			BEGIN
				RAISERROR('COMMISSION_PAID_AGAINST_BIZDOC',16,1)
			END
		END
 
		--REVERTING BACK THE WAREHOUSE ITEMS                  
		IF @tintOppStatus = 1 
		BEGIN               
			EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID                  
		END                  

		IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
		BEGIN
			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
		END
		
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
		BEGIN
			EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END 
						
		UPDATE   
			OpportunityMaster
		SET      
			vcPOppName = @vcPOppName,txtComments = ISNULL(@Comments,''),bitPublicFlag = @bitPublicFlag,numCampainID = @CampaignID,tintSource = @tintSource,tintSourceType=@tintSourceType,
			intPEstimatedCloseDate = @dtEstimatedCloseDate,numModifiedBy = @numUserCntID,bintModifiedDate = GETUTCDATE(),lngPConclAnalysis = @lngPConclAnalysis,monPAmount = @monPAmount,
			tintActive = @tintActive,numSalesOrPurType = @numSalesOrPurType,numStatus = @numStatus,vcOppRefOrderNo=@vcOppRefOrderNo,
			bintOppToOrder=(CASE WHEN @tintOppStatus=0 AND @DealStatus=1 THEN GETUTCDATE() ELSE bintOppToOrder END),bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,
			bitBillingTerms=@bitBillingTerms,intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,tintTaxOperator=@tintTaxOperator,
			numDiscountAcntType=@numDiscountAcntType,vcCouponCode = @vcCouponCode,numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
			bitUseMarkupShippingRate = @bitUseMarkupShippingRate,numMarkupShippingRate = @numMarkupShippingRate,intUsedShippingCompany = @intUsedShippingCompany, numContactID=@numContactId,numShipmentMethod=@numShipmentMethod,numPartner=@numPartner,numPartenerContact=@numPartenerContactId
		WHERE
			numOppId = @numOppID   
	   
		-- UPDATING IF ORGANIZATION IS ASSIGNED TO SOMEONE                                                      
		IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN
			UPDATE
				OpportunityMaster 
			SET	
				numAssignedTo = @numAssignedTo, 
				numAssignedBy = @numUserCntID 
			WHERE 
				numOppId = @numOppID                                                    
		END                                                     
		ELSE IF(@numAssignedTo = 0) 
		BEGIN
			UPDATE 
				OpportunityMaster 
			SET 
				numAssignedTo = 0, 
				numAssignedBy = 0 
			WHERE
				numOppId = @numOppID
		END  
			                                                                    
		-- UPDATING OPP ITEMS
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0                                                                         
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   
			--DELETE CHILD ITEMS OF KIT WITHIN KIT
			DELETE FROM 
				OpportunityKitChildItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   

			--DELETE ITEMS
			DELETE FROM 
				OpportunityKitItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))                      
			                            
			DELETE FROM 
				OpportunityItems 
			WHERE 
				numOppID=@numOppID 
				AND numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   
	          
			--INSERT NEW ADDED ITEMS                        
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,
				[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numToWarehouseItemID,numShipmentMethod,
				numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired,vcAttrValues,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,numCost,vcNotes
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
				(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),
				(select isnull(VN.monCost,0) from Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode and VN.numVendorID=IT.numVendorID where VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numToWarehouseItemID,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
				0,
				(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired, X.AttributeIDs,
				X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(vcVendorNotes,'')
			FROM
			(
				SELECT 
					*
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH  
				(                      
					numoppitemtCode numeric(9),numItemCode numeric(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount money,vcItemDesc varchar(1000),                                    
					numWarehouseItmsID numeric(9),numToWarehouseItemID numeric(9),Op_Flag tinyint,ItemType varchar(30),DropShip bit,bitDiscountType bit,
					fltDiscount DECIMAL(30,16),monTotAmtBefDiscount MONEY,vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),
					numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(500), 
					bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000)
					,numSortOrder NUMERIC(18,0),numCost MONEY,vcVendorNotes VARCHAR(300)
				)
			)X 
			
			-- UPDATE ITEMS
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,vcModelID=X.vcModelID,numWarehouseItmsID=X.numWarehouseItmsID,numToWarehouseItemID = X.numToWarehouseItemID,             
				bitDropShip=X.DropShip,bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,
				numUOMId=numUOM,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,
				numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired,bitWorkOrder=X.bitWorkOrder,vcAttrValues=AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost,vcNotes=vcVendorNotes
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,numToWarehouseItemID,DropShip,bitDiscountType,
					fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
					bitItemPriceApprovalRequired,bitWorkOrder,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) AS numCost,ISNULL(vcVendorNotes,'') vcVendorNotes
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
				WITH  
				(                      
					numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount MONEY,vcItemDesc VARCHAR(1000),vcModelID VARCHAR(300), numWarehouseItmsID NUMERIC(9), 
					numToWarehouseItemID NUMERIC(18,0),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount MONEY,vcItemName VARCHAR(300),numUOM NUMERIC(9),
					numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),numProjectStageID NUMERIC(9),Attributes VARCHAR(500),
					bitItemPriceApprovalRequired BIT, bitWorkOrder BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost MONEY,vcVendorNotes VARCHAR(300)                           
				)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID 

			--INSERT NEW ADDED KIT ITEMS                 
			INSERT INTO OpportunityKitItems                                                                          
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ID.numWareHouseItemId,
				ID.numQtyItemsReq * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0
			FROM 
				OpportunityItems OI 
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			WHERE 
				OI.numOppId=@numOppId 
				AND OI.numoppitemtCode NOT IN (SELECT ISNULL(numOppItemID,0) FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID)  
				
			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
				DELETE FROM @TempKitConfiguration

				INSERT INTO 
					@TempKitConfiguration
				SELECT
					numItemCode,
					numWarehouseItmsID,
					KitChildItems
				FROM
					OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)
				WITH                       
				(
					numItemCode NUMERIC(9),numWarehouseItmsID NUMERIC(9), KitChildItems VARCHAR(MAX)
				) 

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					ItemDetails.numWareHouseItemId,
					ItemDetails.numQtyItemsReq * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0
				FROM
					(
						SELECT 
							t1.numItemCode,
							t1.numWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										0,
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,0,CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										0, 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemWarehouseItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration WHERE numItemCode=t1.numItemCode AND numWarehouseItemID=t1.numWarehouseItemID),',') 
						) as t2
					) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numChildItemID=TempChildItems.numKitItemID
					AND OKI.numWarehouseItemId=TempChildItems.numKitWarehouseItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numWarehouseItmsID = TempChildItems.numWarehouseItemID
					AND OI.numoppitemtcode = OKI.numOppItemID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					AND (TempChildItems.numKitChildItemWarehouseItemID = ItemDetails.numWareHouseItemId  OR ISNULL(TempChildItems.numKitChildItemWarehouseItemID,0) = 0)
			END                                  
			
			--UPDATE KIT ITEMS                 
			UPDATE 
				OKI 
			SET 
				numQtyItemsReq = numQtyItemsReq_Orig * OI.numUnitHour
			FROM 
				OpportunityKitItems OKI 
			JOIN 
				OpportunityItems OI 
			ON 
				OKI.numOppItemID=OI.numoppitemtCode 
				AND OKI.numOppId=OI.numOppId  
			WHERE 
				OI.numOppId=@numOppId 

			-- UPDATE CHILD ITEMS OF KIT WITHIN KIT
			UPDATE
				OKCI
			SET
				OKCI.numQtyItemsReq = (OKI.numQtyItemsReq * OKCI.numQtyItemsReq_Orig)
			FROM
				OpportunityKitChildItems OKCI
			INNER JOIN
				OpportunityKitItems OKI
			ON
				OKCI.numOppID = @numOppId
				AND OKCI.numOppItemID = OKI.numOppItemID
				AND OKCI.numOppChildItemID = OKI.numOppChildItemID                     
			                                    
			EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			   		                                                                      
			EXEC sp_xml_removedocument @hDocItem                                               
		END     

		--FOLLOWING DELETE STATEMENT IS COMMENTED BECUASE NOW WE ARE NOT ALLOWING USER TO EIDT SERIAL/LOT ITEM UNTILL USER REMOVED ASSIGNED SERIAL/LOT NUMBERS
	    --Delete previously added serial lot number added for opportunity
		--DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
	END


	SET @TotalAmount=0                                                           
	SELECT @TotalAmount = SUM(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID                                                                          
  
	UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numOppID

/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
            
			declare @bitApprovalforOpportunity bit
			declare @intOpportunityApprovalProcess int
			SELECT @bitApprovalforOpportunity=bitApprovalforOpportunity,@intOpportunityApprovalProcess=intOpportunityApprovalProcess 
					FROM Domain WHERE numDomainId=@numDomainID
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT

			SET @bitIsInitalSalesOrder=(SELECT bitIsInitalSalesOrder FROM OpportunityMaster WHERE numOppId=@numOppID AND numDomainId=@numDomainID)
			SET @bitViolatePrice=(SELECT bitMarginPriceViolated FROM Domain WHERE numDomainId=@numDomainID)
			
			IF(@bitViolatePrice=1 AND @bitIsInitalSalesOrder=1 AND @tintClickBtn = 1)
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus=@DealStatus WHERE numOppId=@numOppID
			END
			ELSE
			BEGIN
				IF(@bitApprovalforOpportunity=1)
				BEGIN
					IF(@DealStatus='1' OR @DealStatus='2')
					BEGIN
						IF(@intOpportunityApprovalProcess=1)
						BEGIN
							UPDATE OpportunityMaster SET intPromotionApprovalStatus=6,intChangePromotionStatus=@DealStatus
							WHERE numOppId=@numOppId
						END
						IF(@intOpportunityApprovalProcess=2)
						BEGIN
							UPDATE OpportunityMaster SET intPromotionApprovalStatus=1,intChangePromotionStatus=@DealStatus
							WHERE numOppId=@numOppId
						END
					END
				END

				UPDATE OpportunityMaster SET tintOppStatus = @DealStatus WHERE numOppId=@numOppID
				declare @tintShipped as tinyint               
				select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID              
				if @tintOppStatus=1               
					begin         
					PRINT 'inside USP_UpdatingInventoryonCloseDeal'
						exec USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
					end  
				declare @tintCRMType as numeric(9)        
				declare @AccountClosingDate as datetime        

				select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
				select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

				--When deal is won                                         
				IF @DealStatus = 1 
				BEGIN
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID              
					END

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=getutcdate()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
				END         
				--When Deal is Lost
				ELSE IF @DealStatus = 2
				BEGIN
					SELECT @AccountClosingDate=bintAccountClosingDate FROM OpportunityMaster WHERE numOppID=@numOppID         
					
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID
					END
				END 
			END

			/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
			-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
			if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
			begin        
				update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				where numDivisionID=@numDivisionID        
			end        

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9) 
DECLARE @bitIsPrimary BIT;
DECLARE @tintAddressOf TINYINT

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

--Bill Address
IF @numBillAddressId>0
BEGIN
 SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName,@tintAddressOf=ISNULL(tintAddressOf,0)
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numBillAddressId

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numBillAddressId
	END

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName
END
  
  
--Ship Address
IF @numShipmentMethod = 92 -- WILL CALL (PICK UP FROM Warehouse)
BEGIN
	SELECT  
		@vcStreet=ISNULL(vcWStreet,'')
		,@vcCity=ISNULL(vcWCity,'')
		,@vcPostalCode=ISNULL(vcWPinCode,'')
		,@numState=ISNULL(numWState,0)
		,@numCountry=ISNULL(numWCountry,0)
		,@vcAddressName=''
	FROM 
		dbo.Warehouses 
	WHERE 
		numDomainID = @numDomainID 
		AND numWareHouseID = @numWillCallWarehouseID

	UPDATE OpportunityMaster SET tintShipToType=2 WHERE numOppId=@numOppID
 
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = '', --  varchar(100)
	@numCompanyId =0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName
END
ELSE IF @numShipAddressId>0
BEGIN
	SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName,@tintAddressOf=ISNULL(tintAddressOf,0)
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numShipAddressId
 

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numShipAddressId
	END


  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName
END

--INSERT TAX FOR DIVISION   
IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN

-- DELETE ITEM LEVEL CRV TAX TYPES
DELETE FROM OpportunityMasterTaxItems WHERE numOppId=@numOppId AND numTaxItemID = 1

-- INSERT ITEM LEVEL CRV TAX TYPES
INSERT INTO OpportunityMasterTaxItems
(
	numOppId,
	numTaxItemID,
	fltPercentage,
	tintTaxType,
	numTaxID
) 
SELECT 
	OI.numOppId,
	1,
	TEMPTax.decTaxValue,
	TEMPTax.tintTaxType,
	TEMPTax.numTaxID
FROM 
	OpportunityItems OI
INNER JOIN
	ItemTax IT
ON
	OI.numItemCode = IT.numItemCode
OUTER APPLY
(
	SELECT
		decTaxValue,
		tintTaxType,
		numTaxID
	FROM
		dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,OI.numItemCode,IT.numTaxItemID,@numOppId,1,NULL)
) AS TEMPTax
WHERE
	OI.numOppId = @numOppID
	AND IT.numTaxItemID = 1 -- CRV TAX
GROUP BY
	OI.numOppId,
	TEMPTax.decTaxValue,
	TEMPTax.tintTaxType,
	TEMPTax.numTaxID

--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID
) 
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID = IT.numTaxItemID 
WHERE 
	OI.numOppId=@numOppID 
	AND IT.bitApplicable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	0,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numOppID 
	AND I.bitTaxable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT
	@numOppId,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
FROM
	dbo.OpportunityItems OI 
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
INNER JOIN
	TaxDetails TD
ON
	TD.numTaxID = IT.numTaxID
	AND TD.numDomainId = @numDomainId
WHERE
	OI.numOppId = @numOppID
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

END
  
 UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID

	IF @tintOppType=1 AND @tintOppStatus=1 
	BEGIN
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numStatus
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                                                        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetSOBackOrderItems')
DROP PROCEDURE USP_OpportunityMaster_GetSOBackOrderItems
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetSOBackOrderItems]
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
)
AS
BEGIN
	IF NOT EXISTS (SELECT * FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppID)
	BEGIN
		RAISERROR('INVALID_OPPID',16,1)
		RETURN
	END 

	DECLARE @tintDefaultCost AS NUMERIC(18,0)
	DECLARE @bitReOrderPoint AS BIT
	DECLARE @numReOrderPointOrderStatus AS NUMERIC(18,0)
	DECLARE @tintOppStautsForAutoPOBackOrder AS TINYINT
	DECLARE @tintUnitsRecommendationForAutoPOBackOrder AS TINYINT

	SELECT 
		@tintDefaultCost = ISNULL(numCost,0)
		,@bitReOrderPoint=ISNULL(bitReOrderPoint,0)
		,@numReOrderPointOrderStatus=ISNULL(numReOrderPointOrderStatus,0)
		,@tintOppStautsForAutoPOBackOrder=ISNULL(tintOppStautsForAutoPOBackOrder,0)
		,@tintUnitsRecommendationForAutoPOBackOrder=ISNULL(tintUnitsRecommendationForAutoPOBackOrder,1)
	FROM 
		Domain 
	WHERE 
		numDomainId = @numDomainID


	SELECT
		@numReOrderPointOrderStatus AS numReOrderPointOrderStatus
		,@tintOppStautsForAutoPOBackOrder AS tintOppStautsForAutoPOBackOrder
		,@tintUnitsRecommendationForAutoPOBackOrder AS tintUnitsRecommendationForAutoPOBackOrder

	SELECT
		OI.numItemCode
		,CONCAT(I.vcItemName, CASE WHEN dbo.fn_GetAttributes(OI.numWarehouseItmsID,0) <> '' THEN CONCAT(' (',dbo.fn_GetAttributes(OI.numWarehouseItmsID,0),')') ELSE '' END) AS vcItemName
		,I.vcModelID
		,(CASE WHEN ISNULL(I.numItemGroup,0) > 0 AND bitMatrix = 0 THEN ISNULL(WI.vcWHSKU,ISNULL(I.vcSKU,''))  ELSE ISNULL(I.vcSKU,'') END) vcSKU
		,V.vcNotes
		,I.charItemType
		,ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=@numDomainID AND numItemCode=I.numItemCode AND bitDefault =1), '') AS vcPathForTImage
		,ISNULL(OI.numWarehouseItmsID,0) AS numWarehouseItemID
		,(CASE 
			WHEN @tintUnitsRecommendationForAutoPOBackOrder = 2 
			THEN CASE WHEN ISNULL(V.intMinQty,0) > ISNULL(WI.numBackOrder,0) THEN ISNULL(V.intMinQty,0) ELSE ISNULL(WI.numBackOrder,0) END 
			ELSE ISNULL(WI.numBackOrder,0) + ISNULL(V.intMinQty,0) 
		 END) AS numUnitHour
		,ISNULL(WI.numBackOrder,0) AS numBackOrder
		,ISNULL(I.numBaseUnit,0) AS numUOMID
		,dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,OM.numDomainId,I.numBaseUnit) AS fltUOMConversionFactor 
		,ISNULL(I.numVendorID, 0) AS numVendorID
		,CASE WHEN @tintDefaultCost = 3 THEN (ISNULL(V.monCost, 0) / dbo.fn_UOMConversion(I.numPurchaseUnit,I.numItemCode,I.numDomainID,I.numBaseUnit)) WHEN ISNULL(@tintDefaultCost,0) = 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END AS monCost
		,ISNULL(V.intMinQty,0) AS intMinQty
		,ISNULL(@tintDefaultCost,0) AS tintDefaultCost
		,ISNULL(dbo.fn_UOMConversion(I.numPurchaseUnit,I.numItemCode,I.numDomainID,I.numBaseUnit),1) AS fltUOMConversionFactorForPurchase
	FROM    
		dbo.OpportunityItems OI
		INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
		INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
		INNER JOIN WarehouseItems WI ON OI.numWarehouseItmsID = WI.numWareHouseItemID
		LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
	WHERE   
		OI.numOppId = @numOppID
		AND OM.numDomainId = @numDomainId
		AND ISNULL(@bitReOrderPoint,0) = 1
		AND (ISNULL(WI.numOnHand,0) + ISNULL(WI.numOnOrder,0) <= ISNULL(WI.numReOrder,0))
		AND OM.tintOppType = 1
		AND OM.tintOppStatus=1
		AND ISNULL(I.bitAssembly, 0) = 0
		AND ISNULL(I.bitKitParent, 0) = 0
		AND ISNULL(OI.bitDropship,0) = 0
END
/****** Object:  StoredProcedure [dbo].[USP_UpdatedomainDetails]    Script Date: 05/07/2009 21:56:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatedomaindetails')
DROP PROCEDURE usp_updatedomaindetails
GO
CREATE PROCEDURE [dbo].[USP_UpdatedomainDetails]                                      
@numDomainID as numeric(9)=0,                                      
@vcDomainName as varchar(50)='',                                      
@vcDomainDesc as varchar(100)='',                                      
@bitExchangeIntegration as bit,                                      
@bitAccessExchange as bit,                                      
@vcExchUserName as varchar(100)='',                                      
@vcExchPassword as varchar(100)='',                                      
@vcExchPath as varchar(200)='',                                      
@vcExchDomain as varchar(100)='',                                    
@tintCustomPagingRows as tinyint,                                    
@vcDateFormat as varchar(30)='',                                    
@numDefCountry as numeric(9),                                    
@tintComposeWindow as tinyint,                                    
@sintStartDate as smallint,                                    
@sintNoofDaysInterval as smallint,                                    
@tintAssignToCriteria as tinyint,                                    
@bitIntmedPage as bit,                                    
@tintFiscalStartMonth as tinyint,                                      
@tintPayPeriod as tinyint,                        
@numCurrencyID as numeric(9) ,                
@charUnitSystem as char(1)='',              
@vcPortalLogo as varchar(100)='',            
@tintChrForComSearch as tinyint  ,            
@intPaymentGateWay as int,    
@tintChrForItemSearch as tinyint,
@ShipCompany as numeric(9),
@bitAutoPopulateAddress as bit=null,
@tintPoulateAddressTo as tinyint=null,
@bitMultiCurrency as bit,
@bitCreateInvoice AS tinyint = null,
@numDefaultSalesBizDocID AS NUMERIC(9)=NULL,
@numDefaultPurchaseBizDocID AS NUMERIC(9)=NULL,
@bitSaveCreditCardInfo AS BIT = NULL,
@intLastViewedRecord AS INT=10,
@tintCommissionType AS TINYINT,
@tintBaseTax AS TINYINT,
@bitEmbeddedCost AS BIT,
@numDefaultSalesOppBizDocId NUMERIC=NULL,
@tintSessionTimeOut TINYINT=1,
@tintDecimalPoints TINYINT,
@bitCustomizePortal BIT=0,
@tintBillToForPO TINYINT,
@tintShipToForPO TINYINT,
@tintOrganizationSearchCriteria TINYINT,
@bitDocumentRepositary BIT=0,
@tintComAppliesTo TINYINT=0,
@bitGtoBContact BIT=0,
@bitBtoGContact BIT=0,
@bitGtoBCalendar BIT=0,
@bitBtoGCalendar BIT=0,
@bitExpenseNonInventoryItem BIT=0,
@bitInlineEdit BIT=0,
@bitRemoveVendorPOValidation BIT=0,
@tintBaseTaxOnArea tinyint=0,
@bitAmountPastDue BIT=0,
@monAmountPastDue money,
@bitSearchOrderCustomerHistory BIT=0,
@bitRentalItem as bit=0,
@numRentalItemClass as NUMERIC(9)=NULL,
@numRentalHourlyUOM as NUMERIC(9)=NULL,
@numRentalDailyUOM as NUMERIC(9)=NULL,
@tintRentalPriceBasedOn as tinyint=NULL,
@tintCalendarTimeFormat as tinyint=NULL,
@tintDefaultClassType as tinyint=NULL,
@tintPriceBookDiscount as tinyint=0,
@bitAutoSerialNoAssign as bit=0,
@bitIsShowBalance AS BIT = 0,
@numIncomeAccID NUMERIC(18, 0) = 0,
@numCOGSAccID NUMERIC(18, 0) = 0,
@numAssetAccID NUMERIC(18, 0) = 0,
@IsEnableProjectTracking AS BIT = 0,
@IsEnableCampaignTracking AS BIT = 0,
@IsEnableResourceScheduling AS BIT = 0,
@ShippingServiceItem  AS NUMERIC(9)=0,
@numSOBizDocStatus AS NUMERIC(9)=0,
@bitAutolinkUnappliedPayment AS BIT=0,
@numDiscountServiceItemID AS NUMERIC(9)=0,
@vcShipToPhoneNumber as VARCHAR(50)='',
@vcGAUserEmailId as VARCHAR(50)='',
@vcGAUserPassword as VARCHAR(50)='',
@vcGAUserProfileId as VARCHAR(50)='',
@bitDiscountOnUnitPrice AS BIT=0,
@intShippingImageWidth INT = 0,
@intShippingImageHeight INT = 0,
@numTotalInsuredValue NUMERIC(10,2) = 0,
@numTotalCustomsValue NUMERIC(10,2) = 0,
@vcHideTabs VARCHAR(1000) = '',
@bitUseBizdocAmount BIT = 0,
@bitDefaultRateType BIT = 0,
@bitIncludeTaxAndShippingInCommission  BIT = 0,
@bitPurchaseTaxCredit BIT=0,
@bitLandedCost BIT=0,
@vcLanedCostDefault VARCHAR(10)='',
@bitMinUnitPriceRule BIT = 0,
@numAbovePercent AS NUMERIC(18,2),
@numBelowPercent AS NUMERIC(18,2),
@numBelowPriceField AS NUMERIC(18,0),
@vcUnitPriceApprover AS VARCHAR(100),
@numDefaultSalesPricing AS TINYINT,
@bitReOrderPoint AS BIT = 0,
@numReOrderPointOrderStatus AS NUMERIC(18,0) = 0,
@numPODropShipBizDoc AS NUMERIC(18,0) = 0,
@numPODropShipBizDocTemplate AS NUMERIC(18,0) = 0,
@IsEnableDeferredIncome AS BIT = 0,
@bitApprovalforTImeExpense AS BIT=0,
@numCost AS Numeric(9,0)=0,
@intTimeExpApprovalProcess int=0,
@bitApprovalforOpportunity AS BIT=0,
@intOpportunityApprovalProcess int=0,
@numDefaultSalesShippingDoc NUMERIC(18,0)=0,
@numDefaultPurchaseShippingDoc NUMERIC(18,0)=0,
@bitchkOverRideAssignto AS BIT=0,
@vcPrinterIPAddress VARCHAR(15) = '',
@vcPrinterPort VARCHAR(5) = '',
@numDefaultSiteID NUMERIC(18,0) = 0,
@vcSalesOrderTabs VARCHAR(200)='',
@vcSalesQuotesTabs VARCHAR(200)='',
@vcItemPurchaseHistoryTabs VARCHAR(200)='',
@vcItemsFrequentlyPurchasedTabs VARCHAR(200)='',
@vcOpenCasesTabs VARCHAR(200)='',
@vcOpenRMATabs VARCHAR(200)='',
@bitSalesOrderTabs BIT=0,
@bitSalesQuotesTabs BIT=0,
@bitItemPurchaseHistoryTabs BIT=0,
@bitItemsFrequentlyPurchasedTabs BIT=0,
@bitOpenCasesTabs BIT=0,
@bitOpenRMATabs BIT=0
as                                      
BEGIN
BEGIN TRY
BEGIN TRANSACTION

	IF ISNULL(@bitMinUnitPriceRule,0) = 1 AND ((SELECT COUNT(*) FROM UnitPriceApprover WHERE numDomainID=@numDomainID) = 0 OR ISNULL(@bitApprovalforOpportunity,0)=0 OR ISNULL(@intOpportunityApprovalProcess,0) = 0)
	BEGIN
		RAISERROR('INCOMPLETE_MIN_UNIT_PRICE_CONFIGURATION',16,1)
		RETURN
	END


	If ISNULL(@bitApprovalforTImeExpense,0) = 1 AND (SELECT
														COUNT(*) 
													FROM
														UserMaster
													LEFT JOIN
														ApprovalConfig
													ON
														ApprovalConfig.numUserId = UserMaster.numUserDetailId
													WHERE
														UserMaster.numDomainID=@numDomainID
														AND ISNULL(bitActivateFlag,0) = 1
														AND ISNULL(numLevel1Authority,0) = 0
														AND ISNULL(numLevel2Authority,0) = 0
														AND ISNULL(numLevel3Authority,0) = 0
														AND ISNULL(numLevel4Authority,0) = 0
														AND ISNULL(numLevel5Authority,0) = 0) > 0
	BEGIN
		RAISERROR('INCOMPLETE_TIMEEXPENSE_APPROVAL',16,1)
		RETURN
	END

                                  
 update Domain                                       
   set                                       
   vcDomainName=@vcDomainName,                                      
   vcDomainDesc=@vcDomainDesc,                                      
   bitExchangeIntegration=@bitExchangeIntegration,                                      
   bitAccessExchange=@bitAccessExchange,                                      
   vcExchUserName=@vcExchUserName,                                      
   vcExchPassword=@vcExchPassword,                                      
   vcExchPath=@vcExchPath,                                      
   vcExchDomain=@vcExchDomain,                                    
   tintCustomPagingRows =@tintCustomPagingRows,                                    
   vcDateFormat=@vcDateFormat,                                    
   numDefCountry =@numDefCountry,                                    
   tintComposeWindow=@tintComposeWindow,                                    
   sintStartDate =@sintStartDate,                                    
   sintNoofDaysInterval=@sintNoofDaysInterval,                                    
   tintAssignToCriteria=@tintAssignToCriteria,                                    
   bitIntmedPage=@bitIntmedPage,                                    
   tintFiscalStartMonth=@tintFiscalStartMonth,                            
--   bitDeferredIncome=@bitDeferredIncome,                          
   tintPayPeriod=@tintPayPeriod,
   vcCurrency=isnull((select varCurrSymbol from Currency Where numCurrencyID=@numCurrencyID  ),'')  ,                      
  numCurrencyID=@numCurrencyID,                
  charUnitSystem= @charUnitSystem,              
  vcPortalLogo=@vcPortalLogo ,            
  tintChrForComSearch=@tintChrForComSearch  ,      
  intPaymentGateWay=@intPaymentGateWay,
 tintChrForItemSearch=@tintChrForItemSearch,
 numShipCompany= @ShipCompany,
 bitAutoPopulateAddress = @bitAutoPopulateAddress,
 tintPoulateAddressTo =@tintPoulateAddressTo,
 --bitMultiCompany=@bitMultiCompany ,
 bitMultiCurrency=@bitMultiCurrency,
 bitCreateInvoice= @bitCreateInvoice,
 [numDefaultSalesBizDocId] =@numDefaultSalesBizDocId,
 bitSaveCreditCardInfo=@bitSaveCreditCardInfo,
 intLastViewedRecord=@intLastViewedRecord,
 [tintCommissionType]=@tintCommissionType,
 [tintBaseTax]=@tintBaseTax,
 [numDefaultPurchaseBizDocId]=@numDefaultPurchaseBizDocID,
 bitEmbeddedCost=@bitEmbeddedCost,
 numDefaultSalesOppBizDocId=@numDefaultSalesOppBizDocId,
 tintSessionTimeOut=@tintSessionTimeOut,
 tintDecimalPoints=@tintDecimalPoints,
 bitCustomizePortal=@bitCustomizePortal,
 tintBillToForPO = @tintBillToForPO,
 tintShipToForPO = @tintShipToForPO,
 tintOrganizationSearchCriteria=@tintOrganizationSearchCriteria,
-- vcPortalName=@vcPortalName,
 bitDocumentRepositary=@bitDocumentRepositary,
 --tintComAppliesTo=@tintComAppliesTo,THIS FIELD IS REMOVED FROM GLOBAL SETTINGS
 bitGtoBContact=@bitGtoBContact,
 bitBtoGContact=@bitBtoGContact,
 bitGtoBCalendar=@bitGtoBCalendar,
 bitBtoGCalendar=@bitBtoGCalendar,
 bitExpenseNonInventoryItem=@bitExpenseNonInventoryItem,
 bitInlineEdit=@bitInlineEdit,
 bitRemoveVendorPOValidation=@bitRemoveVendorPOValidation,
tintBaseTaxOnArea=@tintBaseTaxOnArea,
bitAmountPastDue = @bitAmountPastDue,
monAmountPastDue = @monAmountPastDue,
bitSearchOrderCustomerHistory=@bitSearchOrderCustomerHistory,
bitRentalItem=@bitRentalItem,
numRentalItemClass=@numRentalItemClass,
numRentalHourlyUOM=@numRentalHourlyUOM,
numRentalDailyUOM=@numRentalDailyUOM,
tintRentalPriceBasedOn=@tintRentalPriceBasedOn,
tintCalendarTimeFormat=@tintCalendarTimeFormat,
tintDefaultClassType=@tintDefaultClassType,
tintPriceBookDiscount=@tintPriceBookDiscount,
bitAutoSerialNoAssign=@bitAutoSerialNoAssign,
bitIsShowBalance = @bitIsShowBalance,
numIncomeAccID = @numIncomeAccID,
numCOGSAccID = @numCOGSAccID,
numAssetAccID = @numAssetAccID,
IsEnableProjectTracking = @IsEnableProjectTracking,
IsEnableCampaignTracking = @IsEnableCampaignTracking,
IsEnableResourceScheduling = @IsEnableResourceScheduling,
numShippingServiceItemID = @ShippingServiceItem,
numSOBizDocStatus=@numSOBizDocStatus,
bitAutolinkUnappliedPayment=@bitAutolinkUnappliedPayment,
numDiscountServiceItemID=@numDiscountServiceItemID,
vcShipToPhoneNo = @vcShipToPhoneNumber,
vcGAUserEMail = @vcGAUserEmailId,
vcGAUserPassword = @vcGAUserPassword,
vcGAUserProfileId = @vcGAUserProfileId,
bitDiscountOnUnitPrice=@bitDiscountOnUnitPrice,
intShippingImageWidth = @intShippingImageWidth ,
intShippingImageHeight = @intShippingImageHeight,
numTotalInsuredValue = @numTotalInsuredValue,
numTotalCustomsValue = @numTotalCustomsValue,
vcHideTabs = @vcHideTabs,
bitUseBizdocAmount = @bitUseBizdocAmount,
bitDefaultRateType = @bitDefaultRateType,
bitIncludeTaxAndShippingInCommission = @bitIncludeTaxAndShippingInCommission,
/*,bitAllowPPVariance=@bitAllowPPVariance*/
bitPurchaseTaxCredit=@bitPurchaseTaxCredit,
bitLandedCost=@bitLandedCost,
vcLanedCostDefault=@vcLanedCostDefault,
bitMinUnitPriceRule = @bitMinUnitPriceRule,
numDefaultSalesPricing = @numDefaultSalesPricing,
numPODropShipBizDoc=@numPODropShipBizDoc,
numPODropShipBizDocTemplate=@numPODropShipBizDocTemplate,
IsEnableDeferredIncome=@IsEnableDeferredIncome,
bitApprovalforTImeExpense=@bitApprovalforTImeExpense,
numCost= @numCost,intTimeExpApprovalProcess=@intTimeExpApprovalProcess,
bitApprovalforOpportunity=@bitApprovalforOpportunity,intOpportunityApprovalProcess=@intOpportunityApprovalProcess,
 numDefaultPurchaseShippingDoc=@numDefaultPurchaseShippingDoc,numDefaultSalesShippingDoc=@numDefaultSalesShippingDoc,
 bitchkOverRideAssignto=@bitchkOverRideAssignto,
 vcPrinterIPAddress=@vcPrinterIPAddress,
 vcPrinterPort=@vcPrinterPort,
 numDefaultSiteID=ISNULL(@numDefaultSiteID,0),
  vcSalesOrderTabs=@vcSalesOrderTabs,
 vcSalesQuotesTabs=@vcSalesQuotesTabs,
 vcItemPurchaseHistoryTabs=@vcItemPurchaseHistoryTabs,
 vcItemsFrequentlyPurchasedTabs=@vcItemsFrequentlyPurchasedTabs,
 vcOpenCasesTabs=@vcOpenCasesTabs,
 vcOpenRMATabs=@vcOpenRMATabs,
 bitSalesOrderTabs=@bitSalesOrderTabs,
 bitSalesQuotesTabs=@bitSalesQuotesTabs,
 bitItemPurchaseHistoryTabs=@bitItemPurchaseHistoryTabs,
 bitItemsFrequentlyPurchasedTabs=@bitItemsFrequentlyPurchasedTabs,
 bitOpenCasesTabs=@bitOpenCasesTabs,
 bitOpenRMATabs=@bitOpenRMATabs
 where numDomainId=@numDomainID

COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH

END
