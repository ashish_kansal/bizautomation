
/******************************************************************
Project: Release 4.1 Date: 02.JAN.2015
Comments: STORED PROCEDURES
*******************************************************************/
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetContactCampignAuditDetail')
DROP FUNCTION GetContactCampignAuditDetail
GO
CREATE FUNCTION [dbo].[GetContactCampignAuditDetail] (@numECampaignID NUMERIC(18,0), @numContactID NUMERIC(18,0))
RETURNS VARCHAR(500)
AS
BEGIN
	DECLARE @vcCampaignAudit AS VARCHAR(500) = ''
    DECLARE @CampaignName AS VARCHAR(200)
	DECLARE @NextDrip AS VARCHAR(200)
	DECLARE @CompletedStages AS VARCHAR(200)
    DECLARE @LastDrip AS VARCHAR(200)

	DECLARE @numDomainID NUMERIC(18,0)
	DECLARE @ConECampaignID AS VARCHAR(200)
	DECLARE @bitCompleted AS BIT = 0

	IF (SELECT COUNT(*) FROM ECampaign WHERE numECampaignID = @numECampaignID) > 0
	BEGIN
		SELECT @vcCampaignAudit = vcECampName, @numDomainID=numDomainID FROM ECampaign WHERE numECampaignID = @numECampaignID
		SELECT TOP 1 @ConECampaignID=numConEmailCampID  FROM ConECampaign WHERE numECampaignID = @numECampaignID AND numContactID = @numContactID ORDER BY numConEmailCampID DESC

		SELECT TOP 1 @NextDrip= CONCAT(MONTH(dtExecutionDate),'-',DAY(dtExecutionDate)) FROM ConECampaignDTL WHERE numConECampID = @ConECampaignID AND ISNULL(bitSend,0) = 0 ORDER BY numConECampDTLID ASC
		SELECT TOP 1 @LastDrip= CONCAT(MONTH(bintSentON),'-',DAY(bintSentON)) FROM ConECampaignDTL WHERE numConECampID = @ConECampaignID AND ISNULL(bitSend,0) = 1 ORDER BY numConECampDTLID DESC
		SELECT @CompletedStages=COUNT(*) FROM ConECampaignDTL WHERE numConECampID = @ConECampaignID AND ISNULL(bitSend,0) = 1

		IF (SELECT COUNT(*) FROM ConECampaignDTL WHERE numConECampID = @ConECampaignID AND ISNULL(bitSend,0) = 0) = 0
		BEGIN
			SET @bitCompleted = 1
		END

		SET @vcCampaignAudit = @vcCampaignAudit + ' ' + ISNULL(@NextDrip,'') + ' / Audit (' + ISNULL(@CompletedStages,0) + ') ' +  ISNULL(@LastDrip,'') + ' ' + (CASE WHEN @bitCompleted = 1 THEN '#Completed#' ELSE '#Running#' END)
	END

	RETURN @vcCampaignAudit
END
GO




GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetNewvcEmailString]    Script Date: 11/02/2011 12:21:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
------Function For Returning new string ----------------------
------Created By : Pinkal Patel Date : 06-Oct-2011
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetNewvcEmailString')
DROP FUNCTION fn_GetNewvcEmailString
GO
CREATE FUNCTION [dbo].[fn_GetNewvcEmailString](
@vcEmailId VARCHAR(2000),
@numDomianID NUMERIC(18,0) = NULL
) RETURNS VARCHAR(2000)
As
BEGIN
DECLARE @vcNewEmailId AS VARCHAR(2000)
DECLARE @Email AS VARCHAR(200)
DECLARE @EmailName AS VARCHAR(200)
DECLARE @EmailAdd AS VARCHAR(200)
DECLARE @StartPos AS INTEGER
DECLARE @EndPos AS INTEGER
DECLARE @numEmailid AS NUMERIC(9)
---DECLARE @numDomainId AS NUMERIC(18, 0)

SET @vcNewEmailId =''
	IF @vcEmailId <> '' 
    BEGIN
        WHILE CHARINDEX('#^#', @vcEmailId) > 0	
            BEGIN
                SET @Email = LTRIM(RTRIM(SUBSTRING(@vcEmailId, 1,
                                                   CHARINDEX('#^#', @vcEmailId,
                                                             0)+2)))
                IF CHARINDEX('$^$', @Email) > 0 
                    BEGIN
                        SET @EmailName = LTRIM(RTRIM(SUBSTRING(@Email, 0, CHARINDEX('$^$', @Email))))
                        SET @EmailAdd = LTRIM(RTRIM(SUBSTRING(@Email, LEN(@EmailName) + 1, CHARINDEX('$^$', @Email) + LEN(@Email) - 1)))
                        SET @EmailAdd = REPLACE(@Emailadd, '$^$', '')
                        SET @EmailAdd = REPLACE(@Emailadd, '#^#', '')                
                    END
                   SET @vcNewEmailId =@vcNewEmailId + (SELECT TOP  1 CAST(ISNULL(numEmailid,0) AS VARCHAR) FROM dbo.EmailMaster WHERE dbo.EmailMaster.vcEmailID=@EmailAdd AND numDomainId=@numDomianID) + '$^$' + @EmailName + '$^$' + @EmailAdd   + '#^#'
				--PRINT @Email
				--SET @vcNewEmailId = @vcNewEmailId + ','
                SET @vcEmailId = LTRIM(RTRIM(SUBSTRING(@vcEmailId,
                                                       CHARINDEX('#^#', @vcEmailId)
                                                       + 3, LEN(@vcEmailId))))
	
		       END
            
			END
			IF LEN(@vcNewEmailId) > 0
			BEGIN
				 SET @vcNewEmailId = LEFT(@vcNewEmailId,LEN(@vcNewEmailId)-3)
			END                
                 RETURN @vcNewEmailId
                 
	END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetAlertDetail')
DROP FUNCTION GetAlertDetail
GO
CREATE FUNCTION [dbo].[GetAlertDetail]
(
	@numDomainId NUMERIC(18,0), 
	@numContactId  NUMERIC(18,0),
	@numUserContactId NUMERIC(18,0),
	@numEmailHstrID NUMERIC(18,0)
)
Returns VARCHAR(MAX) 
As
BEGIN
	DECLARE @vcAlert AS VARCHAR(MAX) = ''
	DECLARE @bitOpenActionItem AS BIT
	DECLARE @bitOpencases AS BIT
	DECLARE @bitOpenProject AS BIT
	DECLARE @bitOpenSalesOpp AS BIT
	DECLARE @bitBalancedue AS BIT
	DECLARE @bitUnreadEmail AS BIT
	DECLARE @bitCampaign AS BIT
	DECLARE @numDivisionID NUMERIC(18,0)
	DECLARE @numECampaignID NUMERIC(18,0)

	--GET ALERT CONFIGURATION
	SELECT  
		@bitOpenActionItem = [bitOpenActionItem],
        @bitOpencases = [bitOpenCases],
        @bitOpenProject = [bitOpenProject],
        @bitOpenSalesOpp = [bitOpenSalesOpp],
        @bitBalancedue = [bitBalancedue],
        @bitUnreadEmail = [bitUnreadEmail],
		@bitCampaign = [bitCampaign]
	FROM    
		AlertConfig
	WHERE   
		AlertConfig.numDomainId = @numDomainId AND 
		AlertConfig.numContactId = @numUserContactId

	--GET DIVISION
	SELECT @numDivisionID = numDivisionId,@numECampaignID=numECampaignID FROM dbo.AdditionalContactsInformation WHERE numContactId = @numContactId

	--CHECK AR BALANCE
	IF @bitBalancedue = 1 AND (SELECT SUM(TotalBalanceDue) FROM VIEW_Email_Alert_Config v WHERE v.numDomainid = @numDomainId AND v.numDivisionID = @numDivisionID) > 0 
    BEGIN 
		SET @vcAlert = '<img alt="" src="../images/dollar.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" />&nbsp;'
    END 

	--CHECK OPEN SALES ORDER COUNT
	IF @bitOpenSalesOpp = 1 AND (SELECT SUM(OpenSalesOppCount) FROM VIEW_Email_Alert_Config v WHERE v.numDomainid = @numDomainId AND v.numDivisionID = @numDivisionID) > 0 
    BEGIN 
		SET @vcAlert = @vcAlert + '<img alt="" src="../images/icons/cart.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" />&nbsp;'
    END 

	--CHECK OPEN CASES COUNT
	IF @bitOpencases = 1 AND (SELECT SUM(OpenCaseCount) FROM VIEW_Email_Alert_Config v WHERE v.numDomainid = @numDomainId AND v.numDivisionID = @numDivisionID) > 0 
    BEGIN 
		SET @vcAlert = @vcAlert + '<img alt="" src="../images/icons/headphone_mic.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" />&nbsp;'
    END 

	--CHECK OPEN PROJECT COUNT
	IF @bitOpenProject = 1 AND (SELECT SUM(OpenProjectCount) FROM VIEW_Email_Alert_Config v WHERE v.numDomainid = @numDomainId AND v.numDivisionID = @numDivisionID) > 0 
    BEGIN 
		SET @vcAlert = @vcAlert + '<img alt="" src="../images/Compass-16.gif" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" />&nbsp;'  
    END 

	--CHECK UNREAD MAIL COUNT
	IF @bitUnreadEmail = 1 AND (SELECT SUM(UnreadEmailCount) FROM VIEW_Email_Alert_Config v WHERE v.numDomainid = @numDomainId AND v.numDivisionID = @numDivisionID) > 0 
    BEGIN 
       SET @vcAlert = @vcAlert + '<a title="Recent Correspondance" class="hyperlink" onclick="OpenCorresPondance('+ CAST(@numContactID AS VARCHAR(10)) + ');">(' + CAST((SELECT SUM(UnreadEmailCount) FROM VIEW_Email_Alert_Config v WHERE v.numDomainid = @numDomainId AND v.numDivisionID = @numDivisionID) AS VARCHAR(10)) + ')</a>&nbsp;'
    END
	
	--CHECK ACTIVE CAMPAIGN COUNT
	IF @bitCampaign = 1 AND ISNULL(@numECampaignID,0) > 0
	BEGIN
		DECLARE @ConECampaignID AS VARCHAR(200)
		SELECT TOP 1 @ConECampaignID=numConEmailCampID  FROM ConECampaign WHERE numECampaignID = @numECampaignID AND numContactID = @numContactID ORDER BY numConEmailCampID DESC
		
		IF (SELECT COUNT(*) FROM ConECampaignDTL WHERE numConECampID = @ConECampaignID AND ISNULL(bitSend,0) = 0) = 0
		BEGIN
			SET @vcAlert = @vcAlert + '<img alt="" src="../images/comflag.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" />&nbsp;'  
		END
		ELSE
		BEGIN
			SET @vcAlert = @vcAlert + '<img alt="" src="../images/Circle_Green_16.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" />&nbsp;'  
		END
	END 

	--CHECK COUNT OF ACTION ITEM
	IF @bitOpenActionItem = 1 AND (SELECT SUM(OpenActionItemCount) FROM VIEW_Email_Alert_Config v WHERE v.numDomainid = @numDomainId AND v.numDivisionID = @numDivisionID and v.numContactId=@numContactId) > 0 
	BEGIN
		SET @vcAlert = @vcAlert + '<img alt="" src="../images/MasterList-16.gif" onclick="return NewActionItem('+ CAST(@numContactID AS VARCHAR(10)) +','+ CAST(@numEmailHstrID AS VARCHAR(10)) +')" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" />&nbsp;'
	END

	RETURN @vcAlert
END
GO
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ConECampaignDTL_SetTrackingStatus')
DROP PROCEDURE USP_ConECampaignDTL_SetTrackingStatus
GO
CREATE PROCEDURE [dbo].[USP_ConECampaignDTL_SetTrackingStatus]                            
 @numConECampDTLID NUMERIC(9 )= 0
AS                            
BEGIN                            
	UPDATE ConECampaignDTL SET bitEmailRead = 1 WHERE numConECampDTLID = @numConECampDTLID
END
GO



GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ConECampaignDTLLinks_Save')
DROP PROCEDURE USP_ConECampaignDTLLinks_Save
GO
CREATE PROCEDURE [dbo].[USP_ConECampaignDTLLinks_Save]                            
 @numConECampDTLID NUMERIC(9 )= 0,
 @strBroadcastingLink AS TEXT = ''
AS                            
BEGIN                            
	DECLARE @hDoc4 INT                                                
    EXEC sp_xml_preparedocument @hDoc4 OUTPUT, @strBroadcastingLink

    INSERT INTO ConECampaignDTLLinks
		(
			numConECampaignDTLID,
			vcOriginalLink
		)
    SELECT  
			@numConECampDTLID,
            X.vcOriginalLink
    FROM    
		( 
			SELECT    
				*
            FROM      
				OPENXML (@hDoc4, '/LinkRecords/Table',2)
    WITH 
        ( 
            vcOriginalLink VARCHAR(2000)
        )
        ) X        

	SELECT * FROM ConECampaignDTLLinks WHERE numConECampaignDTLID = @numConECampDTLID
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ConECampaignDTLLinks_UpdateLinkClicked')
DROP PROCEDURE USP_ConECampaignDTLLinks_UpdateLinkClicked
GO
CREATE PROCEDURE [dbo].[USP_ConECampaignDTLLinks_UpdateLinkClicked]                            
 @numLinkID NUMERIC(9 )= 0
AS                            
BEGIN                            
	
	IF (SELECT ISNULL(bitClicked,0) FROM ConECampaignDTLLinks WHERE numLinkID = @numLinkID) = 0
	BEGIN
		UPDATE 
			ConECampaignDTLLinks
		SET
			bitClicked = 1
		WHERE
			numLinkID = @numLinkID
	END

	SELECT ISNULL(vcOriginalLink,'') FROM ConECampaignDTLLinks WHERE numLinkID = @numLinkID
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteFromCampaign')
DROP PROCEDURE USP_DeleteFromCampaign
GO
/****** Object:  StoredProcedure [dbo].[USP_DeleteFromCampaign]    Script Date: 02/18/2009 15:17:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[USP_DeleteFromCampaign]
@numContactID NUMERIC(9),
@numECampaignID NUMERIC(9)
AS
  BEGIN
    DECLARE  @numConEmailCampID NUMERIC(9)
    
    SELECT @numConEmailCampID = [numConEmailCampID]
    FROM   [ConECampaign]
    WHERE  [numContactID] = @numContactID
           AND [numECampaignID] = @numECampaignID
        
	UPDATE [ConECampaign] SET [bitEngaged] = 0 WHERE [numConEmailCampID]=@numConEmailCampID
	UPDATE AdditionalContactsInformation SET numECampaignID = NULL WHERE numContactId = @numContactID
---- Delete child record                 
--    DELETE FROM [ConECampaignDTL]
--    WHERE       [numConECampID] = @numConEmailCampID
---- Delete parent Record           
--    DELETE FROM [ConECampaign]
--    WHERE       [numContactID] = @numContactID
--                AND [numECampaignID] = @numECampaignID
                

  END
/****** Object:  StoredProcedure [dbo].[USP_DeleteJournalDetails]    Script Date: 07/26/2008 16:15:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletejournaldetails')
DROP PROCEDURE usp_deletejournaldetails
GO
CREATE PROCEDURE [dbo].[USP_DeleteJournalDetails]    
@numDomainID as numeric(9)=0,                        
@numJournalID as numeric(9)=0,                          
@numBillPaymentID as numeric(9)=0,                    
@numDepositID as numeric(9)=0,          
@numCheckHeaderID as numeric(9)=0,
@numBillID as numeric(9)=0,
@numCategoryHDRID AS NUMERIC(9)=0,
@numReturnID AS NUMERIC(9)=0,
@numUserCntID AS NUMERIC(9)=0                        
As                          
BEGIN

	           
BEGIN TRY
   BEGIN TRANSACTION 
   
IF @numDepositID>0
BEGIN
	--Throw error messae if Deposite to Default Undeposited Funds Account
	IF EXISTS(SELECT numDepositID FROM dbo.DepositMaster WHERE numDepositID=@numDepositID And numDomainId=@numDomainId 
			AND tintDepositeToType=2 AND tintDepositePage IN(2,3) AND bitDepositedToAcnt=1)
	BEGIN
		raiserror('Undeposited_Account',16,1); 
		RETURN;
	END
	
	--Throw error messae if Deposite use in Refund
	IF EXISTS(SELECT numDepositID FROM dbo.DepositMaster WHERE numDepositID=@numDepositID And monRefundAmount>0)
	BEGIN
		raiserror('Refund_Payment',16,1); 
		RETURN;
	END
	
	DECLARE @datEntry_Date DATETIME
	SELECT @datEntry_Date = ISNULL([DM].[dtDepositDate],[DM].[dtCreationDate]) FROM [dbo].[DepositMaster] AS DM WHERE DM.[numDepositId] = @numDepositID AND [DM].[numDomainId] = @numDomainID
	--Validation of closed financial year
	PRINT @datEntry_Date
	EXEC dbo.USP_ValidateFinancialYearClosingDate @numDomainID,@datEntry_Date

	--Update SO bizdocs amount Paid
	UPDATE OBD SET monAmountPaid= OBD.monAmountPaid - DD.monAmountPaid 
		FROM DepositMaster DM JOIN DepositeDetails DD ON DM.numDepositID=DD.numDepositID 
							  JOIN OpportunityBizDocs OBD ON DD.numOppBizDocsID=OBD.numOppBizDocsID
	WHERE DM.numDomainId=@numDomainId AND DM.numDepositID=@numDepositID AND DM.tintDepositePage IN(2,3)
	
	
	--Reset Integration with accounting
	UPDATE dbo.DepositMaster SET bitDepositedToAcnt =0 WHERE numDepositId IN (SELECT numChildDepositID FROM dbo.DepositeDetails WHERE numDepositID=@numDepositID AND numChildDepositID>0)
	
	--Delete Transaction History
	DELETE FROM TransactionHistory WHERE numTransHistoryID IN (SELECT numTransHistoryID FROM DepositMaster WHERE numDepositID=@numDepositID And numDomainId=@numDomainId)
	
	DELETE FROM DepositeDetails WHERE numDepositID=@numDepositID   
	
	IF EXISTS (SELECT 1 FROM DepositMaster WHERE numDepositID=@numDepositID And numDomainId=@numDomainId AND ISNULL(numReturnHeaderID,0)>0)
	BEGIN
		UPDATE DepositMaster SET monAppliedAmount=0 WHERE numDepositID=@numDepositID And numDomainId=@numDomainId 
	END 
	ELSE
	BEGIN  
		IF @numJournalId=0
		BEGIN
			SELECT @numJournalId=numJournal_Id FROM dbo.General_Journal_Header WHERE numDepositID=@numDepositID And numDomainId=@numDomainId
		END
	 
		Delete From General_Journal_Details Where numJournalId=@numJournalId And numDomainId=@numDomainId                          
		Delete From General_Journal_Header Where numJournal_Id=@numJournalId And numDomainId=@numDomainId  
	
		DELETE FROM DepositMaster WHERE numDepositID=@numDepositID And numDomainId=@numDomainId     
	END
END    

ELSE IF @numBillID>0
BEGIN
	--Throw error message if amount paid
	IF EXISTS (SELECT numBillID FROM BillHeader WHERE numBillID=@numBillID AND ISNULL(monAmtPaid,0)>0)
	BEGIN
		raiserror('BILL_PAID',16,1); --Bill is paid, can not delete bill
		RETURN;
	END

	IF @numJournalId=0
	BEGIN
		 SELECT @numJournalId=numJournal_Id FROM dbo.General_Journal_Header WHERE numBillID=@numBillID And numDomainId=@numDomainId
	END
	
	Delete From General_Journal_Details Where numJournalId=@numJournalId And numDomainId=@numDomainId                          
	Delete From General_Journal_Header Where numJournal_Id=@numJournalId And numDomainId=@numDomainId  

	DELETE FROM ProjectsOpportunities WHERE numBillId=@numBillID AND numDomainId=@numDomainID

	DELETE FROM BillDetails WHERE numBillID=@numBillID     
	DELETE FROM BillHeader WHERE numBillID=@numBillID And numDomainId=@numDomainId     
END 

ELSE IF @numBillPaymentID>0
BEGIN
	--Throw error messae if Bill Payment use in Refund
	IF EXISTS(SELECT numBillPaymentID FROM dbo.BillPaymentHeader WHERE numBillPaymentID=@numBillPaymentID And monRefundAmount>0)
	BEGIN
		raiserror('Refund_Payment',16,1); 
		RETURN;
	END	
		
	UPDATE dbo.BillHeader SET bitIsPaid =0 WHERE numBillID IN (SELECT numBillID FROM dbo.BillPaymentDetails WHERE numBillPaymentID=@numBillPaymentID AND numBillID>0)


	--Update Add Bill amount Paid
	UPDATE BH SET monAmtPaid= monAmtPaid - BPD.monAmount FROM BillPaymentDetails BPD JOIN BillHeader BH ON BPD.numBillID=BH.numBillID
	WHERE numBillPaymentID=@numBillPaymentID AND BPD.tintBillType=2

	--Update PO bizdocs amount Paid
	UPDATE OBD SET monAmountPaid= OBD.monAmountPaid - BPD.monAmount FROM BillPaymentDetails BPD JOIN OpportunityBizDocs OBD ON BPD.numOppBizDocsId=OBD.numOppBizDocsId
	WHERE numBillPaymentID=@numBillPaymentID AND BPD.tintBillType=1
	
	--Delete if check entry
	DELETE FROM CheckHeader WHERE tintReferenceType=8 AND numReferenceID=@numBillPaymentID And numDomainId=@numDomainId     
	
	IF @numJournalId=0
	BEGIN
		SELECT @numJournalId=numJournal_Id FROM dbo.General_Journal_Header WHERE numBillPaymentID=@numBillPaymentID And numDomainId=@numDomainId
	END
     
    Delete From General_Journal_Details Where numJournalId=@numJournalId And numDomainId=@numDomainId                          
	Delete From General_Journal_Header Where numJournal_Id=@numJournalId And numDomainId=@numDomainId 
	 
	DELETE FROM BillPaymentDetails WHERE numBillPaymentID=@numBillPaymentID     
	DELETE FROM BillPaymentHeader WHERE numBillPaymentID=@numBillPaymentID And numDomainId=@numDomainId AND ISNULL(numReturnHeaderID,0)=0
	UPDATE BillPaymentHeader SET monAppliedAmount=0 WHERE numBillPaymentID=@numBillPaymentID And numDomainId=@numDomainId AND ISNULL(numReturnHeaderID,0)!=0
	
END 

ELSE IF @numCheckHeaderID>0
BEGIN
	DELETE FROM CheckDetails WHERE numCheckHeaderID=@numCheckHeaderID
	DELETE FROM CheckHeader WHERE numCheckHeaderID=@numCheckHeaderID And numDomainId=@numDomainId     
	
	IF @numJournalId=0
	BEGIN
	 SELECT @numJournalId=numJournal_Id FROM dbo.General_Journal_Header WHERE numCheckHeaderID=@numCheckHeaderID And numDomainId=@numDomainId
	END
END 

ELSE IF @numCategoryHDRID>0
BEGIN
	EXEC dbo.USP_DeleteTimExpLeave
	@numCategoryHDRID = @numCategoryHDRID, --  numeric(9, 0)
	@numDomainId = @numDomainId --  numeric(9, 0)
END

ELSE IF @numReturnID>0
BEGIN
	EXEC dbo.USP_DeleteSalesReturnDetails
	@numReturnHeaderID = @numReturnID, --  numeric(9, 0)
	@numReturnStatus = 14879, --  numeric(18, 0) Received
	@numDomainId = @numDomainId, --  numeric(9, 0)
	@numUserCntID = @numUserCntID --  numeric(9, 0)
END


IF EXISTS(SELECT numItemID FROM dbo.General_Journal_Details WHERE numJournalId=@numJournalId And numDomainId=@numDomainId AND (chBizDocItems='IA1' OR chBizDocItems='OE1')  )
BEGIN
 		raiserror('InventoryAdjustment',16,1); 
		RETURN;
END

IF EXISTS(SELECT numJournalId FROM dbo.General_Journal_Details WHERE numJournalId=@numJournalId And numDomainId=@numDomainId AND (chBizDocItems='OE')  )
BEGIN
 		raiserror('OpeningBalance',16,1); 
		RETURN;
END


Delete From General_Journal_Details Where numJournalId IN (SELECT numJournal_Id FROM General_Journal_Header WHERE numJournal_Id=@numJournalId And numDomainId=@numDomainId 
				AND isnull(numBillID,0)=0 AND isnull(numBillPaymentID,0)=0 AND isnull(numPayrollDetailID,0)=0
				AND isnull(numCheckHeaderID,0)=0 AND isnull(numReturnID,0)=0 
				AND isnull(numDepositId,0)=0)
				                         
Delete From General_Journal_Header Where numJournal_Id=@numJournalId And numDomainId=@numDomainId 
			    AND isnull(numBillID,0)=0 AND isnull(numBillPaymentID,0)=0 AND isnull(numPayrollDetailID,0)=0
				AND isnull(numCheckHeaderID,0)=0 AND isnull(numReturnID,0)=0 
				AND isnull(numDepositId,0)=0                      

 COMMIT
 
END TRY
BEGIN CATCH
  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  -- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH
 
 
 

	
--DECLARE @numBizDocsPaymentDetId NUMERIC 
--DECLARE @bitIntegratedToAcnt BIT
--DECLARE @tintPaymentType TINYINT
--SELECT @numBizDocsPaymentDetId=ISNULL(numBizDocsPaymentDetId,0) FROM dbo.General_Journal_Header WHERE numJournal_Id = @numJournalId AND numDomainId=@numDomainId
--IF @numBizDocsPaymentDetId>0
--BEGIN
--	SELECT @bitIntegratedToAcnt=bitIntegratedToAcnt,@tintPaymentType=tintPaymentType 
--	From OpportunityBizDocsDetails Where numBizDocsPaymentDetId=@numBizDocsPaymentDetId
--	AND numBillPaymentJournalID <>@numJournalId
--
--	-- @tintPaymentType - 2 means Bill against vendor, 4 Bill Against liability
--	IF (@tintPaymentType =2 OR @tintPaymentType =4) AND @bitIntegratedToAcnt=1
--	BEGIN
--		raiserror('BILL_PAID',16,1); --Bill is paid, can not delete bill
--		RETURN;
--	END
--	
--	
--END
--
--
--
-- -- if bitOpeningBalance=0 then it is not Opening Balance         
-- Declare @bitOpeningBalance as bit        
-- Select @bitOpeningBalance=isnull(bitOpeningBalance,0) From General_Journal_Header Where numJournal_Id=@numJournalId And numDomainId=@numDomainId          
--  Print @bitOpeningBalance    
-- if @bitOpeningBalance=0
-- Begin                       
--   Delete From General_Journal_Details Where numJournalId=@numJournalId And numDomainId=@numDomainId                          
--   Delete From General_Journal_Header Where numJournal_Id=@numJournalId And numDomainId=@numDomainId                       
--                        
--   --To Delete the CheckDetails
--   Delete From CheckDetails  Where numCheckId=@numCheckId And numDomainId=@numDomainId                      
--
--   -- To Delete the CashCreditCardDetails
--   Delete From CashCreditCardDetails Where numCashCreditId=@numCashCreditCardId And numDomainId=@numDomainId             
--            
--   -- To Delete DepositDetails    
--   UPDATE dbo.DepositMaster SET bitDepositedToAcnt = 0 WHERE numDepositId IN (SELECT numChildDepositID FROM dbo.DepositeDetails WHERE numDepositID=@numDepositId)      
--   Delete From dbo.DepositeDetails Where numDepositId=@numDepositId 
--   DELETE FROM dbo.DepositMaster WHERE numDepositId =@numDepositId And numDomainId=@numDomainId       
--   --Update Vendor Payement reference Journal ID if exist
--   UPDATE  dbo.OpportunityBizDocsDetails SET numBillPaymentJournalID=NULL WHERE numBillPaymentJournalID= @numJournalId AND numBizDocsPaymentDetId=@numBizDocsPaymentDetId
--   
-- End          
--Else
-- Begin
--  Update General_Journal_Header Set numAmount=0 Where numJournal_Id=@numJournalId And numDomainId=@numDomainId          
--  Update General_Journal_Details Set numDebitAmt=0,numCreditAmt=0 Where numJournalId=@numJournalId  And numDomainId=@numDomainId       
--  Update dbo.DepositMaster Set monDepositAmount=0 Where numDepositId=@numDepositId And numDomainId=@numDomainId       
--  Update CashCreditCardDetails Set monAmount=0 Where numCashCreditId=@numCashCreditCardId And numDomainId=@numDomainId        
-- End        
End
GO
/****** Object:  StoredProcedure [dbo].[USP_DelInboxItems]    Script Date: 07/26/2008 16:15:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_delinboxitems')
DROP PROCEDURE usp_delinboxitems
GO
CREATE PROCEDURE [dbo].[USP_DelInboxItems]       
@numEmailHstrID as numeric(9)  ,           
@mode as bit,
@numDomainID AS NUMERIC(18,0),
@numUserCntID AS NUMERIC(18,0)            
as              
   
if @mode =1  
begin             
/*Bug reported by Audiogenx that they can not delete mails, cause: Email was added to correspondance of Project*/
 DELETE FROM dbo.Correspondence WHERE numEmailHistoryID = @numEmailHstrID 

 delete emailHistory               
 where numEmailHstrID=@numEmailHstrID         
       
 delete EmailHStrToBCCAndCC               
 where numEmailHstrID=@numEmailHstrID       
       
 delete EmailHstrAttchDtls               
 where numEmailHstrID=@numEmailHstrID      
 end  
else  
begin  
 --OLD NODE ID REQUIRED TO GET FOLDER NAME WHERE EMAIL WAS BEFORE MOVED TO DELETED FOLDER
 --BECAUSE TO DELETE EMAIL IN GAMIL FOLDER NAME IS REQUIRED
 UPDATE EmailHistory SET numOldNodeID=numNodeId WHERE numEmailHstrID=@numEmailHstrID 
 update emailHistory set tinttype=3,numNodeid=ISNULL((SELECT numNodeID FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID=@numUserCntID AND bitSystem=1 AND numFixID=2),2) where numEmailHstrID=@numEmailHstrID     
end
GO
/****** Object:  StoredProcedure [dbo].[USP_EcampaginDTls]    Script Date: 06/04/2009 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ecampagindtls')
DROP PROCEDURE usp_ecampagindtls
GO
CREATE PROCEDURE [dbo].[USP_EcampaginDTls]  
@numECampaignID as numeric(9)=0,  
@byteMode as tinyint=0  
as  
  
if @byteMode=0  
begin  
select vcECampName,txtDesc,ISNULL(dtStartTime,GETDATE()) dtStartTime,ISNULL(fltTimeZone,0) fltTimeZone,ISNULL(tintTimeZoneIndex,0) tintTimeZoneIndex, ISNULL([tintFromField],1) tintFromField,ISNULL([numFromContactID],0) numFromContactID from ECampaign   
where numECampaignID=@numECampaignID  
select numECampDTLId,numEmailTemplate,numActionItemTemplate, tintDays, tintWaitPeriod, ISNULL(numFollowUpID,0) numFollowUpID from ECampaignDTLs  
where numECampID=@numECampaignID  
end  
if @byteMode=1  
begin 
	UPDATE ECampaign SET bitDeleted=1 WHERE numECampaignID=@numECampaignID
end
/****** Object:  StoredProcedure [dbo].[USP_ECampaignHstr]    Script Date: 07/26/2008 16:15:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ecampaignhstr')
DROP PROCEDURE usp_ecampaignhstr
GO
CREATE PROCEDURE [dbo].[USP_ECampaignHstr] 
    @numConatctID AS NUMERIC(9),
    @numConEmailCampID NUMERIC(9)
AS 
		SELECT
			ECampaign.vcECampName,
			(CASE 
				WHEN ISNULL(ECampaign.bitDeleted,0) = 0 
				THEN CASE WHEN ISNULL(ConECampaign.bitEngaged,0) = 0 THEN 'Disengaged' ELSE 'Engaged' END
				ELSE 'Campaign Deleted'
			END) AS [Status],
			(CASE 
				WHEN ISNULL(ECampaignDTLs.numEmailTemplate,0) > 0 THEN GenericDocuments.vcDocName
				WHEN ISNULL(ECampaignDTLs.numActionItemTemplate,0) > 0 THEN tblActionItemData.TemplateName
				ELSE ''
			END) AS Template,
			ISNULL(dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,ISNULL(ECampaign.fltTimeZone,0) * 60,ConECampaignDTL.dtExecutionDate),ACI.[numDomainID]),'') vcExecutionDate,
			(CASE 
				WHEN ISNULL(ConECampaignDTL.bitSend,0)=1 AND ISNULL(ConECampaignDTL.tintDeliveryStatus,0)=0 THEN 'Failed'
				WHEN ISNULL(ConECampaignDTL.bitSend,0)=1 AND ISNULL(ConECampaignDTL.tintDeliveryStatus,0)=1 THEN 'Success'
				ELSE 'Pending'
			END) AS SendStatus,
			ISNULL(dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,ISNULL(ECampaign.fltTimeZone,0)*60, ConECampaignDTL.[bintSentON]),ACI.[numDomainID]),'') vcSendDate,
			ISNULL(ConECampaignDTL.bitEmailRead,0) bitEmailRead,
			(SELECT COUNT(*) FROM ConECampaignDTLLinks WHERE numConECampaignDTLID = ConECampaignDTL.numConECampDTLID) AS NoOfLinks,
			(SELECT COUNT(*) FROM ConECampaignDTLLinks WHERE ISNULL(bitClicked,0) = 1 AND numConECampaignDTLID = ConECampaignDTL.numConECampDTLID) AS NoOfLinkClicked
		FROM ConECampaignDTL
		INNER JOIN ECampaignDTLs ON ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId
		INNER JOIN ConECampaign	ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
		INNER JOIN ECampaign ON	ConECampaign.numECampaignID = ECampaign.numECampaignID
		INNER JOIN [AdditionalContactsInformation] ACI ON ACI.[numContactId] = ConECampaign.[numContactID]
		LEFT JOIN GenericDocuments ON GenericDocuments.numGenericDocID = ECampaignDTLs.numEmailTemplate
		LEFT JOIN tblActionItemData ON tblActionItemData.RowID = ECampaignDTLs.numActionItemTemplate
		WHERE 
			(ConECampaign.numConEmailCampID = @numConEmailCampID OR ISNULL(@numConEmailCampID,0)=0) AND
			(ACI.numContactID = @numConatctID OR ISNULL(@numConatctID,0)=0)
		ORDER BY [numConECampDTLID] ASC 
GO
/****** Object:  StoredProcedure [dbo].[USP_ECampaignList]    Script Date: 07/26/2008 16:15:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ecampaignlist')
DROP PROCEDURE usp_ecampaignlist
GO
CREATE PROCEDURE [dbo].[USP_ECampaignList]            
 @numUserCntID numeric(9)=0,            
 @numDomainID numeric(9)=0,              
 @SortChar char(1)='0',           
 @CurrentPage int,            
 @PageSize int,            
 @TotRecs int output,            
 @columnName as Varchar(50),            
 @columnSortOrder as Varchar(10)              
as            
            
            
               
             
            
create table #tempTable ( ID INT IDENTITY PRIMARY KEY,             
 numECampaignID numeric(9),            
 vcECampName varchar(100),            
 txtDesc text,
 NoOfAssignedUser INT,
 NoOfEmailSent INT,
 NoOfEmailOpened INT,
 NoOfLinks INT,
 NoOfLinkClicked INT        
 )            
            
            
declare @strSql as varchar(8000)            
          
          
            
set @strSql='select 
numECampaignID,          
vcECampName,          
txtDesc, 
ISNULL((SELECT COUNT(*) FROM ConECampaign WHERE numECampaignID = ECampaign.numECampaignID),0) AS NoOfAssignedUser,
ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ECampaignDTLs ON ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId WHERE ISNULL(ECampaignDTLs.numEmailTemplate,0) > 0 AND numECampID = ECampaign.numECampaignID AND ISNULL(bitSend,0) = 1 AND ISNULL(tintDeliveryStatus,0) = 1),0) AS NoOfEmailSent,
ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ECampaignDTLs ON ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId WHERE ISNULL(ECampaignDTLs.numEmailTemplate,0) > 0 AND numECampID = ECampaign.numECampaignID AND ISNULL(bitSend,0) = 1 AND ISNULL(tintDeliveryStatus,0) = 1 AND ISNULL(bitEmailRead,0) = 1),0) AS NoOfEmailOpened,
(SELECT COUNT(*) FROM ConECampaignDTLLinks WHERE numConECampaignDTLID IN (SELECT ISNULL(CECDTL.numConECampDTLID,0) FROM ConECampaignDTL CECDTL WHERE numConECampID IN (SELECT ISNULL(CEC.numConEmailCampID,0) FROM ConECampaign CEC WHERE numECampaignID = ECampaign.numECampaignID))) AS NoOfLinks,
(SELECT COUNT(*) FROM ConECampaignDTLLinks WHERE ISNULL(bitClicked,0) = 1 AND numConECampaignDTLID IN (SELECT ISNULL(CECDTL.numConECampDTLID,0) FROM ConECampaignDTL CECDTL WHERE numConECampID IN (SELECT ISNULL(CEC.numConEmailCampID,0) FROM ConECampaign CEC WHERE numECampaignID = ECampaign.numECampaignID))) AS NoOfLinkClicked
from ECampaign          
where ISNULL(bitDeleted,0) = 0 AND numDomainID= '+ convert(varchar(15),@numDomainID)           
            
if @SortChar <>'0' set @strSql=@strSql + ' And vcECampName like '''+@SortChar+'%'''          
if @columnName<>''  set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder   
  
print @strSql         
insert into #tempTable
(            
	numECampaignID,            
	vcECampName,            
	txtDesc,
	NoOfAssignedUser,
	NoOfEmailSent,
	NoOfEmailOpened,
	NoOfLinks,
	NoOfLinkClicked 
)            
exec 
(
	@strSql
)             
            
  declare @firstRec as integer             
  declare @lastRec as integer            
 set @firstRec= (@CurrentPage-1) * @PageSize            
     set @lastRec= (@CurrentPage*@PageSize+1)            
--Don't change column sequence because its being used for Drip Campaign DropDown in Contact Details
select numECampaignID,vcECampName,txtDesc,NoOfAssignedUser,NoOfEmailSent,NoOfEmailOpened,NoOfLinks,NoOfLinkClicked from #tempTable where ID > @firstRec and ID < @lastRec            
set @TotRecs=(select count(*) from #tempTable)            
drop table #tempTable
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_EmailHistory_Archive' ) 
    DROP PROCEDURE USP_EmailHistory_Archive
GO
CREATE PROCEDURE USP_EmailHistory_Archive
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
    @numEmailHstrID AS VARCHAR(MAX)
AS 
BEGIN
	IF @numEmailHstrID = 'ALL'
	BEGIN
		--Moves all emails which are more than 180 days old to Email Archive folder
		UPDATE t1
			SET t1.numNodeId = ISNULL((SELECT numNodeID FROM InboxTreeSort WHERE numDomainID = t1.numDomainID AND numUserCntID = t1.numUserCntID AND ISNULL(numFixID,0) = 2 AND ISNULL(bitSystem,0) = 1),0),bitArchived=1
		FROM
			EmailHistory t1
		WHERE
			CAST(t1.dtReceivedOn AS DATE) < CAST(DATEADD(D,-180,GETDATE()) AS DATE) AND
			ISNULL(t1.bitArchived,0) = 0
	END
	ELSE
	BEGIN
		DECLARE @numEmailArchiveNodeID NUMERIC(18,0)
		DECLARE @strSQL AS VARCHAR(MAX)

		SELECT @numEmailArchiveNodeID=ISNULL(numNodeID,0) FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntID AND ISNULL(numFixID,0) = 2 AND ISNULL(bitSystem,0) = 1

		SET @strSQL = 'UPDATE EmailHistory SET bitArchived=1, numNodeId = ' + CAST(@numEmailArchiveNodeID AS VARCHAR(18)) + ' WHERE numEmailHstrID IN (' + @numEmailHstrID + ')'

		EXEC(@strSQL)
	END
END





GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_EmailHistory_DeleteArchived' ) 
    DROP PROCEDURE USP_EmailHistory_DeleteArchived
GO
CREATE PROCEDURE USP_EmailHistory_DeleteArchived
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
    @dtFrom AS DATE,
	@dtTo AS DATE
AS 
BEGIN
	DECLARE @ErrorMessage NVARCHAR(4000);SET @ErrorMessage=''
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;
	DECLARE @numEmailArchiveNodeID NUMERIC(18,0)
	DECLARE @strSQL AS VARCHAR(MAX)

	BEGIN TRANSACTION
	BEGIN TRY
		--numFixID = 7 = Email Archive
		SELECT @numEmailArchiveNodeID=ISNULL(numNodeID,0) FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntID AND numFixID = 2 AND ISNULL(bitSystem,0) = 1

		DELETE FROM dbo.Correspondence WHERE numEmailHistoryID IN (SELECT ISNULL(numEmailHstrID,0) FROM EmailHistory WHERE numDomainID = @numDomainID AND numUserCntId = @numUserCntID AND numNodeId = @numEmailArchiveNodeID AND CAST(dtReceivedOn AS DATE) >= @dtFrom AND CAST(dtReceivedOn AS DATE) <= @dtTo)

		DELETE FROM EmailHStrToBCCAndCC WHERE numEmailHstrID IN (SELECT ISNULL(numEmailHstrID,0) FROM EmailHistory WHERE numDomainID = @numDomainID AND numUserCntId = @numUserCntID AND numNodeId = @numEmailArchiveNodeID AND CAST(dtReceivedOn AS DATE) >= @dtFrom AND CAST(dtReceivedOn AS DATE) <= @dtTo)       
       
		DELETE FROM EmailHstrAttchDtls WHERE numEmailHstrID IN (SELECT ISNULL(numEmailHstrID,0) FROM EmailHistory WHERE numDomainID = @numDomainID AND numUserCntId = @numUserCntID AND numNodeId = @numEmailArchiveNodeID AND CAST(dtReceivedOn AS DATE) >= @dtFrom AND CAST(dtReceivedOn AS DATE) <= @dtTo)
	
		DELETE EmailHistory WHERE numEmailHstrID IN (SELECT ISNULL(numEmailHstrID,0) FROM EmailHistory WHERE numDomainID = @numDomainID AND numUserCntId = @numUserCntID AND numNodeId = @numEmailArchiveNodeID AND CAST(dtReceivedOn AS DATE) >= @dtFrom AND CAST(dtReceivedOn AS DATE) <= @dtTo)                   
	END TRY
	BEGIN CATCH
		SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		-- Use RAISERROR inside the CATCH block to return error
		-- information about the original error that caused
		-- execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState);
	END CATCH

	IF @@TRANCOUNT > 0
		COMMIT TRANSACTION
END



GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_EmailHistory_GetByIDsString' ) 
    DROP PROCEDURE USP_EmailHistory_GetByIDsString
GO
CREATE PROCEDURE USP_EmailHistory_GetByIDsString
    @vcEmailHstrID AS VARCHAR(MAX),
	@numDomainID AS NUMERIC(18,0),
	@numUserCntID AS NUMERIC(18,0)
AS 
BEGIN
	DECLARE @strSQL AS VARCHAR(MAX)

	SET @strSQL = 'SELECT numEmailHstrID,numUid,vcNodeName,chrSource FROM EmailHistory LEFT JOIN InboxTreeSort ON EmailHistory.numOldNodeID = InboxTreeSort.numNodeID WHERE EmailHistory.numDomainID=' + CAST(@numDomainID AS VARCHAR(9)) + ' AND EmailHistory.numUserCntId=' + CAST(@numUserCntID AS VARCHAR(9)) + ' AND EmailHistory.numEmailHstrID IN (' + @vcEmailHstrID + ')'
	
	EXEC(@strSQL)
END



GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_EmailHistory_MarkAsSpam' ) 
    DROP PROCEDURE USP_EmailHistory_MarkAsSpam
GO
CREATE PROCEDURE USP_EmailHistory_MarkAsSpam
    @vcEmailHstrID AS VARCHAR(MAX)
AS 
BEGIN
	DECLARE @strSQL AS VARCHAR(MAX)

	SELECT numUserCntId FROM EmailHistory

	SET @strSQL = 'DELETE FROM EmailHStrToBCCAndCC WHERE numEmailHstrID IN (' + @vcEmailHstrID + ')'
	EXEC(@strSQL)
    
	SET @strSQL = 'DELETE FROM EmailHstrAttchDtls WHERE numEmailHstrID IN (' + @vcEmailHstrID + ')'
	EXEC(@strSQL)   

	SET @strSQL = 'DELETE FROM EmailHistory WHERE numEmailHstrID IN (' + @vcEmailHstrID + ')'
	EXEC(@strSQL)
END
/****** Object:  StoredProcedure [dbo].[USP_MoveCopyItems]    Script Date: 07/26/2008 16:20:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_EmailHistory_MoveCopyIDs')
DROP PROCEDURE USP_EmailHistory_MoveCopyIDs
GO
CREATE PROCEDURE [dbo].[USP_EmailHistory_MoveCopyIDs]                      
@numEmailHstrID AS NUMERIC(18,0),
@numNodeId AS NUMERIC(9),
@ModeType AS BIT,
@numUID AS NUMERIC(18,0)
AS          
--COPY
IF @ModeType = 1
BEGIN
	DECLARE @NewEmailHstrID INTEGER;   

	--MAKE A COPY IN EmailHistory TABLE
		INSERT INTO EmailHistory
		(
			vcSubject,vcBody,bintCreatedOn,numNoofTimes,vcItemId,vcChangeKey,bitIsRead,vcSize,bitHasAttachments,dtReceivedOn,
			tintType,chrSource,vcCategory,vcBodyText,numDomainID,numUserCntId,numUid,numNodeId,vcFrom,vcTO,numEMailId
		)                        
		SELECT 
			vcSubject,vcBody,bintCreatedOn,numNoofTimes,vcItemId,vcChangeKey,bitIsRead,vcSize,bitHasAttachments,dtReceivedOn,
			tintType,chrSource,vcCategory,vcBodyText,numDomainID,numUserCntId,@numUID ,@numNodeId,vcFrom,vcTO,numEMailId            
		FROM 
			EmailHistory 
		WHERE 
			numEmailHstrID=@numEmailHstrID              
                  
		SELECT @NewEmailHstrID = SCOPE_IDENTITY();              
        
		--MAKE A COPY IN EmailHStrToBCCAndCC TABLE
		INSERT INTO EmailHStrToBCCAndCC 
		(
			numEmailHstrID,vcName,tintType,numDivisionId,numEmailId
		)              
		SELECT 
			@NewEmailHstrID,vcName,tintType,numDivisionId,numEmailId 
		FROM  
			EmailHStrToBCCAndCC 
		WHERE 
			numEmailHstrID=@numEmailHstrID              

		--MAKE A COPY IN EmailHstrAttchDtls TABLE                    
		INSERT INTO EmailHstrAttchDtls 
		( 
			numEmailHstrID,vcFileName,vcAttachmentItemId,vcAttachmentType
		)              
		SELECT  
			@NewEmailHstrID,vcFileName,vcAttachmentItemId,vcAttachmentType 
		FROM 
			EmailHstrAttchDtls 
		WHERE 
			numEmailHstrID=@numEmailHstrID 
	
END
--MOVE
ELSE
BEGIN
	UPDATE emailHistory SET numNodeId=@numNodeId, numUid = @numUID WHERE numEmailHstrID = @numEmailHstrID
END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_EmailHistory_Restore' ) 
    DROP PROCEDURE USP_EmailHistory_Restore
GO
CREATE PROCEDURE USP_EmailHistory_Restore
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
    @numEmailHstrID AS VARCHAR(MAX)
AS 
BEGIN
	
	DECLARE @numInboxNodeID NUMERIC(18,0)
	DECLARE @strSQL AS VARCHAR(MAX)

	SELECT @numInboxNodeID=ISNULL(numNodeID,0) FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntID AND numFixID = 1 AND ISNULL(bitSystem,0) = 1

	SET @strSQL = 'UPDATE EmailHistory SET bitArchived=0, numNodeId = ' + CAST(@numInboxNodeID AS VARCHAR(18)) + ' WHERE numEmailHstrID IN (' + @numEmailHstrID + ')'

	EXEC(@strSQL)
END




/****** Object:  StoredProcedure [dbo].[USP_GetCartItem]    Script Date: 11/08/2011 18:00:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetCartItem' ) 
                    DROP PROCEDURE USP_GetCartItem
                    Go
CREATE PROCEDURE [dbo].[USP_GetCartItem]
(
		 @numUserCntId numeric(18,0),
		 @numDomainId numeric(18,0),
		 @vcCookieId varchar(100),
		 @bitUserType BIT =0--if 0 then anonomyous user 1 for logi user
)
AS
	IF @numUserCntId <> 0
	BEGIN
		SELECT numCartId,
			   numUserCntId,
			   numDomainId,
			   vcCookieId,
			   numOppItemCode,
			   numItemCode,
			   (SELECT TOP 1 numCategoryId  FROM dbo.ItemCategory WHERE numItemID =  CartItems.numItemCode AND numDomainId =@numDomainId ) AS numCategoryId,
			   numUnitHour,
			   monPrice,
			   numSourceId,
			   vcItemDesc,
			   numWarehouseId,
			   vcItemName,
			   vcWarehouse,
			   numWarehouseItmsID,
			   vcItemType,
			   vcAttributes,
			   vcAttrValues,
			   bitFreeShipping,
			   numWeight,
			   tintOpFlag,
			   bitDiscountType,
			   fltDiscount,
			   monTotAmtBefDiscount,
			   ItemURL,
			   numUOM,
			   vcUOMName,
			   decUOMConversionFactor,
			   numHeight,
			   numLength,
			   numWidth,
			   vcShippingMethod,
			   numServiceTypeId,
			   decShippingCharge,
			   numShippingCompany,
			   tintServicetype,
			   CAST( dtDeliveryDate AS VARCHAR(30)) dtDeliveryDate,
			   monTotAmount,
			   (SELECT vcModelID FROM dbo.Item WHERE Item.numItemCode =  CartItems.numItemCode) AS [vcModelID]
			   ,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = CartItems.numItemCode AND II.numDomainId= CartItems.numDomainId AND II.bitDefault=1) As vcPathForTImage
			   FROM CartItems 
			   WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId 	
	END
	ELSE
	BEGIN
		SELECT numCartId,
			   numUserCntId,
			   numDomainId,
			   vcCookieId,
			   numOppItemCode,
			   numItemCode,
			   (SELECT TOP 1 numCategoryId  FROM dbo.ItemCategory WHERE numItemID =  CartItems.numItemCode AND numDomainId =@numDomainId ) AS numCategoryId,
			   numUnitHour,
			   monPrice,
			   numSourceId,
			   vcItemDesc,
			   numWarehouseId,
			   vcItemName,
			   vcWarehouse,
			   numWarehouseItmsID,
			   vcItemType,
			   vcAttributes,
			   vcAttrValues,
			   bitFreeShipping,
			   numWeight,
			   tintOpFlag,
			   bitDiscountType,
			   fltDiscount,
			   monTotAmtBefDiscount,
			   ItemURL,
			   numUOM,
			   vcUOMName,
			   decUOMConversionFactor,
			   numHeight,
			   numLength,
			   numWidth,
			   vcShippingMethod,
			   numServiceTypeId,
			   decShippingCharge,
			   numShippingCompany,
			   tintServicetype,
			   CAST( dtDeliveryDate AS VARCHAR(30)) dtDeliveryDate,
			   monTotAmount,
			   (SELECT vcModelID FROM dbo.Item WHERE Item.numItemCode =  CartItems.numItemCode) AS [vcModelID]
			   ,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = CartItems.numItemCode AND II.numDomainId= CartItems.numDomainId AND II.bitDefault=1) As vcPathForTImage
			   FROM CartItems WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId AND vcCookieId =@vcCookieId	
	END
	
	

--exec USP_GetCartItem @numUserCntId=1,@numDomainId=1,@vcCookieId='fc3d604b-25fa-4c25-960c-1e317768113e',@bitUserType=NULL


/****** Object:  StoredProcedure [dbo].[usp_GetContactDTlPL]    Script Date: 07/26/2008 16:16:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactdtlpl')
DROP PROCEDURE usp_getcontactdtlpl
GO
CREATE PROCEDURE [dbo].[usp_GetContactDTlPL]                                                              
@numContactID as numeric(9)=0  ,    
@numDomainID as numeric(9)=0   ,  
@ClientTimeZoneOffset as int                                    
--                                                            
AS                                                              
BEGIN                                                              
                                                              
  SELECT                                      
 A.numContactId,            
A.vcFirstName,           
A.vcLastName,                                                                   
 D.numDivisionID,             
 C.numCompanyId,            
 C.vcCompanyName,             
 D.vcDivisionName,             
 D.numDomainID,        
 tintCRMType,            
 A.numPhone,
 A.numPhoneExtension,
 A.vcEmail,  
 A.numTeam, 
 A.numTeam as vcTeam,         
[dbo].[GetListIemName](A.numTeam) as vcTeamName,                                                             
 A.vcFax,   
 A.numContactType as vcContactType,
 A.numContactType,         
[dbo].[GetListIemName](A.numContactType) as vcContactTypeName,                         
 A.charSex,             
 A.bintDOB,            
 dbo.GetAge(A.bintDOB, getutcdate()) as Age,                                                               
 [dbo].[GetListIemName]( A.vcPosition) as vcPositionName, 
 A.vcPosition,            
 A.txtNotes,             
 A.numCreatedBy,                                                              
 A.numCell,            
 A.NumHomePhone,            
 A.vcAsstFirstName,
 A.vcAsstLastName,            
 A.numAsstPhone,
 A.numAsstExtn,                                                              
 A.vcAsstEmail,            
 A.charSex, 
 case when A.charSex='M' then 'Male' when A.charSex='F' then 'Female' else  '-' end as charSexName,            
[dbo].[GetListIemName]( A.vcDepartment) as vcDepartmentName,  
vcDepartment,                                                             
--    case when AddC.vcPStreet is null then '' when AddC.vcPStreet='' then '' else AddC.vcPStreet + ',' end +             
-- case when AddC.vcPCity is null then '' when AddC.vcPCity='' then ''  else AddC.vcPCity end +             
-- case when  AddC.vcPPostalCode is null then '' when  AddC.vcPPostalCode='' then '' else ','+ AddC.vcPPostalCode end +              
-- case when dbo.fn_GetState(AddC.vcPState) is null then '' else  ','+ dbo.fn_GetState(AddC.vcPState) end +              
-- case when dbo.fn_GetListName(AddC.vcPCountry,0) ='' then '' else ',' + dbo.fn_GetListName(AddC.vcPCountry,0) end      
 dbo.getContactAddress(A.numContactId) as [Address],            
-- AddC.vcPState,            
-- AddC.vcPCountry,            
-- AddC.vcContactLocation,                                    
--  AddC.vcStreet,                                                               
--    AddC.vcCity,             
-- AddC.vcState,             
-- AddC.intPostalCode,                                                               
--  AddC.vcCountry,                                                              
    A.bitOptOut,                                                                         
 dbo.fn_GetContactName(a.numManagerId) as ManagerName,
 a.numManagerId   as   Manager,
[dbo].[GetListIemName](A.vcCategory) as vcCategoryName  ,     
 A.vcCategory,        
 dbo.fn_GetContactName(A.numCreatedBy)+' '+ convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,A.bintCreatedDate)) CreatedBy,                                      
 dbo.fn_GetContactName(A.numModifiedBy)+' '+ convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,A.bintModifiedDate)) ModifiedBy,            
 vcTitle,            
 vcAltEmail,                                                          
 dbo.fn_GetContactName(A.numRecOwner) as RecordOwner,
 A.numEmpStatus,                  
[dbo].[GetListIemName]( A.numEmpStatus) as numEmpStatusName,                                                          
(select  count(*) from GenericDocuments   where numRecID=A.numContactId and  vcDocumentSection='C') as DocumentCount,                          
(SELECT count(*)from CompanyAssociations where numDivisionID=A.numContactId and bitDeleted=0 ) as AssociateCountFrom,                            
(SELECT count(*)from CompanyAssociations where numAssociateFromDivisionID=A.numContactId and bitDeleted=0 ) as AssociateCountTo,
 ISNULL((SELECT vcECampName FROM [ECampaign] WHERE numECampaignID = A.numECampaignID),'') vcECampName,
 ISNULL(A.numECampaignID,0) numECampaignID,
 ISNULL( (SELECT MAX([numConEmailCampID]) FROM [ConECampaign] WHERE [numContactID] = A.numContactID AND [numECampaignID]= A.numECampaignID AND ISNULL(bitEngaged,0)=1),0) numConEmailCampID,
 ISNULL(A.bitPrimaryContact,0) AS bitPrimaryContact
 FROM AdditionalContactsInformation A INNER JOIN                                                              
 DivisionMaster D ON A.numDivisionId = D.numDivisionID INNER JOIN                                                              
 CompanyInfo C ON D.numCompanyID = C.numCompanyId                                    
-- left join ContactAddress AddC on A.numContactId=AddC.numContactId                                  
 left join UserMaster U on U.numUserDetailId=A.numContactId                                                             
 WHERE A.numContactId = @numContactID and A.numDomainID=@numDomainID                                                     
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactlist1')
DROP PROCEDURE usp_getcontactlist1
GO
CREATE PROCEDURE [dbo].[USP_GetContactList1]                                                                                   
@numUserCntID numeric(9)=0,                                                                                    
@numDomainID numeric(9)=0,                                                                                    
@tintUserRightType tinyint=0,                                                                                    
@tintSortOrder tinyint=4,                                                                                    
@SortChar char(1)='0',                                                                                   
@FirstName varChar(100)= '',                                                                                  
@LastName varchar(100)='',                                                                                  
@CustName varchar(100)='',                                                                                  
@CurrentPage int,                                                                                  
@PageSize int,                                                                                  
@columnName as Varchar(50),                                                                                  
@columnSortOrder as Varchar(10),                                                                                  
@numDivisionID as numeric(9)=0,                                              
@bitPartner as bit ,                                                   
@inttype as numeric(9)=0,
@ClientTimeZoneOffset as int,
 @vcRegularSearchCriteria varchar(1000)='',
 @vcCustomSearchCriteria varchar(1000)=''                                                                             
--                                                                                    
as                  
                       
declare @join as varchar(400)                  
set @join = ''                 
                        
 if @columnName like 'CFW.Cust%'                 
begin                
                
                 
 set @join= ' left Join CFW_FLD_Values_Cont CFW on CFW.RecId=ADC.numcontactId  and CFW.fld_id= '+replace(@columnName,'CFW.Cust','') +' '                                                         
 set @columnName='CFW.Fld_Value'                
                  
end                                         
if @columnName like 'DCust%'                
begin                
                                                       
 set @join= ' left Join CFW_FLD_Values_Cont CFW on CFW.RecId=ADC.numcontactId  and CFW.fld_id= '+replace(@columnName,'DCust','')                                                                                                   
 set @join= @join +' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '                
 set @columnName='LstCF.vcData'                  
                
end                                                                              
                                                                                  
declare @firstRec as integer                                                                                  
declare @lastRec as integer                                                                                  
set @firstRec= (@CurrentPage-1) * @PageSize                              
set @lastRec= (@CurrentPage*@PageSize+1)                                                                                   
                                                                                  
                                                                                  
declare @strSql as varchar(8000)                                                                            
 set  @strSql='with tblcontacts as (Select '                                                                          
      if (@tintSortOrder=5 or @tintSortOrder=6) set  @strSql= @strSql+' top 20 '                                                
                                     
   set  @strSql= @strSql+ ' ROW_NUMBER() OVER (ORDER BY '+ CASE when @columnName='ADC.numAge' then 'ADC.bintDOB' ELSE @columnName end +' '+ @columnSortOrder+') AS RowNumber,
   COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount,
   ADC.numContactId                                   
  FROM AdditionalContactsInformation ADC                                
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                
  JOIN CompanyInfo cmp ON DM.numCompanyID = cmp.numCompanyId'  +@join+  '                                                                  
  left join ListDetails LD on LD.numListItemID=cmp.numCompanyRating                                                                           
  left join ListDetails LD1 on LD1.numListItemID= ADC.numEmpStatus 
  LEFT JOIN AddressDetails AD ON AD.numRecordID = ADC.numContactID AND AD.tintAddressOf=1 AND AD.tintAddressType=0 AND AD.bitIsPrimary=1 '                                           
  if @bitPartner=1 set @strSql=@strSql + ' left join CompanyAssociations CA on CA.numAssociateFromDivisionID=DM.numDivisionID and bitShareportal=1 and CA.bitDeleted=0                                                 
and CA.numDivisionID=(select numDivisionID from AdditionalContactsInformation where numContactID='+convert(varchar(15),@numUserCntID)+ ')'                                                                   
                                                                    
 if @tintSortOrder= 7 set @strSql=@strSql+' join Favorites F on F.numContactid=ADC.numContactID '                                                                     
                                                       
  set  @strSql= @strSql +'                                                                
        where 
 --DM.tintCRMType<>0     AND 
 cmp.numDomainID=DM.numDomainID   
 and DM.numDomainID=ADC.numDomainID                                             
    and DM.numDomainID  = '+convert(varchar(15),@numDomainID)+''                             
 if @inttype<>0 and @inttype<>101  set @strSql=@strSql+' and ADC.numContactType  = '+convert(varchar(10),@inttype)                            
 if @inttype=101  set @strSql=@strSql+' and ADC.bitPrimaryContact  =1 '                            
    if @FirstName<>'' set @strSql=@strSql+' and ADC.vcFirstName  like '''+@FirstName+'%'''                                                             
    if @LastName<>'' set @strSql=@strSql+' and ADC.vcLastName like '''+@LastName+'%'''                                              
    if @CustName<>'' set @strSql=@strSql+' and cmp.vcCompanyName like '''+@CustName+'%'''                                                                                  
if @SortChar<>'0' set @strSql=@strSql + ' And ADC.vcFirstName like '''+@SortChar+'%'''                                                                                           
if @tintUserRightType=1 set @strSql=@strSql + ' AND (ADC.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ')' + case when @bitPartner=1 then ' or (CA.bitShareportal=1 and  CA.bitDeleted=0)' else '' end                                                  
   
else if @tintUserRightType=2 set @strSql=@strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or DM.numTerID=0) '                                                           
                
if @numDivisionID <> 0  set @strSql=@strSql + ' And DM.numDivisionID ='+ convert(varchar(10),@numDivisionID)                      
                                                                                         
if @tintSortOrder=2  set @strSql=@strSql + ' AND (ADC.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ')'                                                                                      
else if @tintSortOrder=3  set @strSql=@strSql + '  AND ADC.numContactType=92  '                                                                          
else if @tintSortOrder=4  set @strSql=@strSql + ' AND ADC.bintCreatedDate > '''+convert(varchar(20),dateadd(day,-7,getutcdate()))+''''                                    
else if @tintSortOrder=5  set @strSql=@strSql + ' and ADC.numCreatedby='+convert(varchar(15),@numUserCntID)                      
                              
else if @tintSortOrder=6  set @strSql=@strSql + ' and ADC.numModifiedby='+convert(varchar(15),@numUserCntID)                      
                                                     
else if @tintSortOrder=7  set @strSql=@strSql + ' and F.numUserCntID='+convert(varchar(15),@numUserCntID)                                                                             
                    
 if (@tintSortOrder=5 and @columnName!='ADC.bintcreateddate')  set @strSql='select * from ('+@strSql+')X order by '+ @columnName +' '+ @columnSortOrder                        
else if (@tintSortOrder=6 and @columnName!='ADC.bintcreateddate')  set @strSql='select * from ('+@strSql+')X order by '+ @columnName +' '+ @columnSortOrder                                                                
else if @tintSortOrder=7  set @strSql=@strSql + ' and cType=''C'' '                      
    
IF @vcRegularSearchCriteria<>'' set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
IF @vcCustomSearchCriteria<>'' set @strSql=@strSql+' and ADC.numContactId in (select distinct CFW.RecId from CFW_FLD_Values_Cont CFW where ' + @vcCustomSearchCriteria + ')'

set @strSql=@strSql + ')'                      
                                                                                 
declare @tintOrder as tinyint                                                        
declare @vcFieldName as varchar(50)                                                        
declare @vcListItemType as varchar(3)                                                   
declare @vcListItemType1 as varchar(1)                                                       
declare @vcAssociatedControlType varchar(20)                                                        
declare @numListID AS numeric(9)                                                        
declare @vcDbColumnName varchar(20)                            
declare @WhereCondition varchar(2000)                             
declare @vcLookBackTableName varchar(2000)                      
Declare @bitCustom as bit  
DECLARE @bitAllowEdit AS CHAR(1)                 
Declare @numFieldId as numeric  
DECLARE @bitAllowSorting AS CHAR(1)                   
declare @vcColumnName AS VARCHAR(500)                            
set @tintOrder=0                                                        
set @WhereCondition =''                       
                         
declare @Nocolumns as tinyint                      
set @Nocolumns=0                      
  
select @Nocolumns=isnull(sum(TotalRow),0)  from(            
Select count(*) TotalRow from View_DynamicColumns where numFormId=10 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=@inttype 
Union 
Select count(*) TotalRow from View_DynamicCustomColumns where numFormId=10 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=@inttype) TotalRows

                   
--declare @strColumns as varchar(2000)                      
--set @strColumns=''                      
--while @DefaultNocolumns>0                      
--begin                      
--                  
-- set @strColumns=@strColumns+',null'                      
-- set @DefaultNocolumns=@DefaultNocolumns-1                      
--end                   
                     
   CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1))

--Check if Master Form Configuration is created by administrator if NoColumns=0
DECLARE @IsMasterConfAvailable BIT = 0
DECLARE @numUserGroup NUMERIC(18,0)

IF @Nocolumns=0
BEGIN
	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 10 AND numRelCntType=@inttype AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END
END


--If MasterConfiguration is available then load it otherwise load default columns
IF @IsMasterConfAvailable = 1
BEGIN
	INSERT INTO #tempForm
	SELECT 
		(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
		vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
		bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
		ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
	FROM 
		View_DynamicColumnsMasterConfig
	WHERE
		View_DynamicColumnsMasterConfig.numFormId=10 AND 
		View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicColumnsMasterConfig.numRelCntType=@inttype AND 
		View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
		ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	UNION
    SELECT 
		tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
		bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,0,''
	FROM 
		View_DynamicCustomColumnsMasterConfig
	WHERE 
		View_DynamicCustomColumnsMasterConfig.numFormId=10 AND 
		View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicCustomColumnsMasterConfig.numRelCntType=@inttype AND 
		View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
	ORDER BY 
		tintOrder ASC  
END
ELSE                                        
BEGIN                 
     
	if @Nocolumns=0
	BEGIN
	INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
	select 10,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,@inttype,1,0,intColumnWidth
	 FROM View_DynamicDefaultColumns
	 where numFormId=10 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID
	order by tintOrder asc 
	END
                                   
   INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID ,intColumnWidth,bitAllowFiltering,vcFieldDataType
 FROM View_DynamicColumns 
 where numFormId=10 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=@inttype
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
       
       UNION
    
   select tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,0,''
 from View_DynamicCustomColumns
 where numFormId=10 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=@inttype
 AND ISNULL(bitCustom,0)=1
 
  order by tintOrder asc  

END
   
set @strSql=@strSql+' select convert(varchar(10),isnull(ADC.numContactId,0)) as numContactId            
       ,ISNULL(ADC.numECampaignID,0) as numECampaignID
	   ,ISNULL( (SELECT MAX([numConEmailCampID]) FROM [ConECampaign] WHERE [numContactID] = ADC.numContactId AND [numECampaignID]= ADC.numECampaignID AND ISNULL(bitEngaged,0)=1),0) numConEmailCampID
	   ,ISNULL((SELECT ISNULL(VIE.Total,0) FROM View_InboxEmail VIE  WITH (NOEXPAND) where VIE.numDomainID = ADC.numDomainID AND  VIE.numContactId = ADC.numContactId),0) as TotalEmail
	   ,ISNULL((SELECT ISNULL(VOA.OpenActionItemCount,0) AS TotalActionItem FROM VIEW_OPEN_ACTION_ITEMS VOA WITH (NOEXPAND)  where VOA.numDomainID = ADC.numDomainID AND  VOA.numDivisionId = DM.numDivisionID),0) as TotalActionItem
       ,ISNULL(DM.numDivisionID,0) numDivisionID, isnull(DM.numTerID,0)numTerID,isnull( ADC.numRecOwner,0) numRecOwner,isnull( DM.tintCRMType,0) tintCRMType ,RowNumber'                      
                                                    
Declare @ListRelID as numeric(9) 
  
    select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from  #tempForm order by tintOrder asc            

                   
while @tintOrder>0                                                        
begin                                                        
                                            
   if @bitCustom = 0                
 begin           
        
    declare @Prefix as varchar(5)                      
      if @vcLookBackTableName = 'AdditionalContactsInformation'                      
    set @Prefix = 'ADC.'                      
      if @vcLookBackTableName = 'DivisionMaster'                      
    set @Prefix = 'DM.'         
        
                  SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'
                          
 if @vcAssociatedControlType='SelectBox'                                                        
        begin                                                        
                                                        
     if @vcListItemType='LI'                                                         
     begin                                                        
      set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                       
      set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                        
     end                                                        
     else if @vcListItemType='S'                                                         
     begin                                                        
      set @strSql=@strSql+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName+']'                                                        
      set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID='+@vcDbColumnName                                                        
     end                                                        
     else if @vcListItemType='T'                                                         
     begin                                                        
      set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                        
      set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                        
     end                       
     else if   @vcListItemType='U'                                                     
    begin                       
    set @strSql=@strSql+',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'                                                        
    end                      
    end         
 else if @vcAssociatedControlType='DateField'                                                      
 begin                
           
     set @strSql=@strSql+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''          
     set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''          
     set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '          
     set @strSql=@strSql+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'                    
   end        
              
else if @vcAssociatedControlType='TextBox' OR @vcAssociatedControlType='Label'                                                     
begin        
         
     set @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'         
 when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' else @vcDbColumnName end+' ['+ @vcColumnName+']'                   
 end      
else                                                
begin    
	if @vcDbColumnName='vcCampaignAudit'
		set @strSql=@strSql+', dbo.GetContactCampignAuditDetail(ADC.numECampaignID,ADC.numContactId) ['+ @vcColumnName+']'  
	else
		set @strSql=@strSql+', '+ @vcDbColumnName +' ['+ @vcColumnName+']'                 
 end                                               
            
              
                    
end                
else if @bitCustom = 1                
begin                
            
	SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'
       
   select @vcFieldName=FLd_label,@vcAssociatedControlType=fld_type,@vcDbColumnName='Cust'+Convert(varchar(10),Fld_Id)                               
    from CFW_Fld_Master                                                
   where    CFW_Fld_Master.Fld_Id = @numFieldId                 
                
                 
    if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'             
   begin                
                   
    set @strSql= @strSql+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=ADC.numContactId   '                                                         
   end      
    else if @vcAssociatedControlType = 'CheckBox'             
   begin              
                 
    set @strSql= @strSql+',case when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=0 then ''No'' when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=1 then ''Yes'' end   ['+ @vcColumnName +']'              
   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '               
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=ADC.numContactId   '                                                       
   end              
  else if @vcAssociatedControlType = 'DateField'            
   begin                
                   
    set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=ADC.numContactId   '                                                         
   end                
   else if @vcAssociatedControlType = 'SelectBox'            
   begin                
    set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
    set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
     on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=ADC.numContactId   '                                                         
    set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
   end                 
end                    
                    
   select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc            
 
  if @@rowcount=0 set @tintOrder=0 

                                  
end                             
                          
       
-------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID AND DFCS.numFormID=DFFM.numFormID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=10 AND DFCS.numFormID=10 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
---------------------------- 
 
IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
set @strSql=@strSql+',tCS.vcColorScheme'
END                         
                                  
set @strSql=@strSql+' ,TotalRowCount FROM AdditionalContactsInformation ADC                                                                             
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                                            
  JOIN CompanyInfo CI ON DM.numCompanyID = CI.numCompanyId
  LEFT JOIN AddressDetails AD ON AD.numRecordID = ADC.numContactID AND AD.tintAddressOf=1 AND AD.tintAddressType=0 AND AD.bitIsPrimary=1
   '+@WhereCondition+                      
' join tblcontacts T on T.numContactId=ADC.numContactId'                     
  
--' union select convert(varchar(10),count(*)),null,null,null,null,null '+@strColumns+' from tblcontacts order by RowNumber'                                                 

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
	 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
		set @Prefix = 'ADC.'                  
     if @vcCSLookBackTableName = 'DivisionMaster'                  
		set @Prefix = 'DM.'
	 if @vcCSLookBackTableName = 'CompanyInfo'                  
		set @Prefix = 'CI.' 
	 if @vcCSLookBackTableName = 'AddressDetails'                  
		set @Prefix = 'AD.'   
 
	IF @vcCSAssociatedControlType='DateField'
	BEGIN
--		set @strSql=@strSql+' left join #tempColorScheme tCS on ' + @Prefix + @vcCSOrigDbCOlumnName + '> DateAdd(day,CAST(SUBSTRING(tCS.vcFieldValue,1,CHARINDEX(''~'',tCS.vcFieldValue)-1) AS INT),GETDATE())
--			 and ' + @Prefix + @vcCSOrigDbCOlumnName + '< DateAdd(day,CAST(SUBSTRING(tCS.vcFieldValue,CHARINDEX(''~'',tCS.vcFieldValue)+1,len(tCS.vcFieldValue) - PATINDEX(''~'',tCS.vcFieldValue)) AS INT),GETDATE())'
		
		
			set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
			 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'

	END
	ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
	BEGIN
		set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
	END
END
    

set @strSql=@strSql+' WHERE CI.numDomainID=DM.numDomainID   
  and DM.numDomainID=ADC.numDomainID   
  and RowNumber > '+convert(varchar(10),@firstRec)+ ' and RowNumber <'+ convert(varchar(10),@lastRec)+ ' order by RowNumber'

                                                     
print @strSql                              
                      
exec (@strSql)


SELECT * FROM #tempForm

DROP TABLE #tempForm  

drop table #tempColorScheme
/****** Object:  StoredProcedure [dbo].[Usp_getCorrespondance]    Script Date: 07/26/2008 16:17:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcorrespondance')
DROP PROCEDURE usp_getcorrespondance
GO
CREATE PROCEDURE [dbo].[Usp_getCorrespondance]                          
@FromDate as datetime,                          
@toDate as datetime,                          
@numContactId as numeric,                          
@vcMessageFrom as varchar(200)='' ,                        
@tintSortOrder as numeric,                        
@SeachKeyword as varchar(100),                        
@CurrentPage as numeric,                        
@PageSize as numeric,                        
@TotRecs as numeric output,                        
@columnName as varchar(100),                        
@columnSortOrder as varchar(50),                        
@filter as numeric,                        
@numdivisionId as numeric =0,              
@numDomainID as numeric(9)=0,              
@numCaseID as numeric(9)=0,              
@ClientTimeZoneOffset INT,
@numOpenRecordID NUMERIC=0,
@tintMode TINYINT=0,
@bitOpenCommu BIT=0
as 

set @vcMessageFrom=replace(@vcMessageFrom,'''','|')
                         
if @filter>0 set @tintSortOrder=  @filter
               
if ((@filter <=2 and @tintSortOrder<=2) or (@tintSortOrder>2 and @filter>2))                 
begin  
                                                                      
 declare @strSql as varchar(8000)       
                                  
  declare @firstRec as integer                                                        
  declare @lastRec as integer                                                        
  set @firstRec= (@CurrentPage-1) * @PageSize                                                        
  set @lastRec= (@CurrentPage*@PageSize+1)                 
  set @strSql=''                
 if (@filter <=2 and @tintSortOrder<=2 and @bitOpenCommu=0)                 
 begin     
 declare @strCondition as varchar(4500);
 declare @vcContactEmail as varchar(100);
              
  if @numdivisionId =0 and  @vcMessageFrom <> '' 
 BEGIN
  set  @strCondition=' and (vcFrom like ''%'+ @vcMessageFrom+'%'' or vcTo like ''%'+ @vcMessageFrom+'%'')  ' 

 END
  else if @numdivisionId <>0 
 BEGIN

 /* This approach does not work as numEmailId stores who sent email, id of sender
  set @strCondition=' and HDR.numEmailID in (
  SELECT numEmailID FROM dbo.EmailMaster WHERE numDomainID= ' + CONVERT(VARCHAR(15),@numDomainID) +' AND numContactID 
  IN (SELECT numcontactid FROM dbo.AdditionalContactsInformation WHERE numDivisionID = ' + convert(varchar(15),@numDivisionID) + ' AND numDomainID = ' + CONVERT(VARCHAR(15),@numDomainID) +' ))';
*/
-- bug fix id 1591,1628 
  DECLARE @TnumContactID NUMERIC(9);set @strCondition=''

  select TOP 1 @TnumContactID=numContactID,@vcContactEmail=replace(vcEmail,'''','''''') from AdditionalContactsInformation where numdivisionid=@numdivisionId and len(vcEmail)>2
  ORDER BY numContactID ASC
  
  WHILE @TnumContactID>0
  BEGIN
   if len(@strCondition)>0
    SET @strCondition=@strCondition + ' or '
   set @strCondition=@strCondition + ' vcFrom like ''%'+ @vcContactEmail+'%'' or vcTo like ''%'+ @vcContactEmail+'%'' ' 

   select TOP 1 @TnumContactID=numContactID,@vcContactEmail=vcEmail from AdditionalContactsInformation where numdivisionid=@numdivisionId and len(vcEmail)>2 and numContactID>@TnumContactID
   ORDER BY numContactID ASC

   IF @@rowcount = 0 SET @TnumContactID = 0 
  END
 
  if len(@strCondition)>0
   SET @strCondition=' and (' + @strCondition + ')'
  else
   SET @strCondition=' and (1=0)' 
  --set @strCondition=' and DTL.numEmailId  in (select numEmailId from EmailMaster where vcEmailID in (select vcEmail  from AdditionalContactsInformation where numdivisionid='''+ convert(varchar(15),@numdivisionId)+''' ))    '          
  END
  else if @numContactId>0 
 BEGIN
  select @vcContactEmail=isnull(replace(vcEmail,'''',''''''),'') from AdditionalContactsInformation where numContactID=@numContactId and len(vcEmail)>2

  if len(@vcContactEmail)>0
   set  @strCondition=' and (vcFrom like ''%'+ @vcContactEmail+'%'' or vcTo like ''%'+ @vcContactEmail+'%'')' 
  else
   SET @strCondition=' and (1=0)'
 END
 else 
  SET @strCondition=''
    
  set @strSql= '                        
  SELECT distinct(HDR.numEmailHstrID),convert(varchar(15),HDR.numEmailHstrID)+''~1~0''+convert(varchar(15),HDR.numUserCntId) as DelData,HDR.bintCreatedOn,
  dbo.FormatedDateTimeFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', bintCreatedOn),'+convert(varchar(15),@numDomainID)+') as [date],                
  vcSubject as [Subject],                          
  ''Email'' as [type] ,                          
  '''' as phone,'''' as assignedto ,                          
  ''0'' as caseid,                             
  null as vcCasenumber,                                 
  ''0''as caseTimeid,                                
  ''0''as caseExpid ,                          
  hdr.tinttype ,HDR.bintCreatedOn as dtCreatedDate,vcFrom + '','' + vcTo as [From] ,0 as bitClosedflag,ISNULL(HDR.bitHasAttachments,0) bitHasAttachments,
 CASE WHEN LEN(Cast(vcBodyText AS VARCHAR(1000)))>150 THEN SUBSTRING(vcBodyText,0,150) + ''...'' ELSE
   Cast(vcBodyText AS VARCHAR(1000)) END AS vcBody                     
  from EmailHistory HDR                                 
  LEFT JOIN [Correspondence] CR ON CR.numEmailHistoryID = HDR.numEmailHstrID '                    
   set  @strSql=@strSql +' where ISNULL(HDR.bitInvisible,0) = 0 AND HDR.numDomainID='+convert(varchar(15),@numDomainID)+' and  (bintCreatedOn between '''+ convert(varchar(30),@FromDate)+''' and '''+ convert(varchar(30),@ToDate)+''') '
   + ' AND (CR.numOpenRecordID = '+ convert(varchar(15),@numOpenRecordID) +' OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  
 set  @strSql=@strSql+ @strCondition
--  if @numdivisionId =0 and  @vcMessageFrom <> '' set  @strSql=@strSql+ 'and (vcFrom like ''%'+ @vcMessageFrom+'%'' or vcTo like ''%'+ @vcMessageFrom+'%'')  '                      
--  else if @numdivisionId <>0 
-- BEGIN
--  set @strSql=@strSql+ 'and DTL.numEmailId  in (select numEmailId from EmailMaster where vcEmailID in (select vcEmail  from AdditionalContactsInformation where numdivisionid='''+ convert(varchar(15),@numdivisionId)+''' ))    '          
--  END
--  else if @numContactId>0 
-- BEGIN
--  declare @vcContactEmail as varchar(100);
--  select @vcContactEmail=vcEmail from AdditionalContactsInformation where numContactID=@numContactId
--
--  set  @strSql=@strSql+ 'and (vcFrom like ''%'+ @vcContactEmail+'%'' or vcTo like ''%'+ @vcContactEmail+'%'')    ' 
-- END

  IF @tintMode=1 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(1,3) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=2 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(2,4) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=5 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(5) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=6 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(6) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  
  if @filter =0 set  @strSql=@strSql+ ' and (hdr.tinttype=1 or hdr.tinttype=2 or  hdr.tinttype=5)'                  
  else if @filter =1 set  @strSql=@strSql+ ' and hdr.tinttype=1'                       
  else if @filter =2 set  @strSql=@strSql+ ' and hdr.tinttype=2'                        
  if @filter > 2 set  @strSql=@strSql+ ' and hdr.tinttype=0'                 
                        
  if @tintSortOrder =0 and @SeachKeyword <> ''                 
  begin                
   set  @strSql=@strSql+ ' and (vcFrom like ''%'+ @SeachKeyword+'%'' or vcTo like ''%'+ @SeachKeyword+'%'' or vcsubject like ''%'+@SeachKeyword+'%'' or vcBody like ''%'+@SeachKeyword+'%'')'               
  end                
  if @tintSortOrder =1 and @SeachKeyword <> ''                 
  begin                
     set  @strSql=@strSql+ ' and hdr.tinttype=1 and (vcFrom like ''%'+ @SeachKeyword+'%'' or vcTo like ''%'+ @SeachKeyword+'%'' or vcsubject like ''%'+@SeachKeyword+'%'' or  vcBody like ''%'+@SeachKeyword+'%'')'                 
  end                
  if @tintSortOrder =2 and @SeachKeyword <> ''                 
  begin                
     set  @strSql=@strSql+ ' and hdr.tinttype=2 and (vcFrom like ''%'+ @SeachKeyword+'%'' or vcTo like ''%'+ @SeachKeyword+'%'' or vcsubject like ''%'+@SeachKeyword+'%'' or  vcBody like ''%'+@SeachKeyword+'%'')'                 
  end                            
 end                
 if (@filter =0 and @tintSortOrder=0 and @bitOpenCommu=0) set  @strSql=@strSql+ ' union '                
                
 if ((@filter=0 or @filter>2) and (@tintSortOrder=0 or @tintSortOrder>2))                
 begin                
  set  @strSql=@strSql+ '                     
  SELECT c.numCommId as numEmailHstrID,convert(varchar(15),C.numCommId)+''~2~''+convert(varchar(15),C.numCreatedBy) as DelData,CASE WHEN c.bitTask = 973 THEN dtCreatedDate WHEN c.bitTask = 974 THEN dtCreatedDate WHEN c.bitTask = 971 THEN dtStartTime ELSE dtEndTime END AS bintCreatedOn,
  case when c.bitTask=973 then dbo.FormatedDateTimeFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtCreatedDate),'+convert(varchar(15),@numDomainID)+') 
  when c.bitTask=974 then dbo.FormatedDateTimeFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtEndTime),'+convert(varchar(15),@numDomainID)+') 
  when c.bitTask=971 THEN dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtEndTime),'+convert(varchar(15),@numDomainID)+') + '' '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtStartTime]),100),7)  +'' - '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtEndTime]),100),7)
  else  dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtEndTime),'+convert(varchar(15),@numDomainID)+') + '' '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtStartTime]),100),7)  +'' - '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtEndTime]),100),7)  END AS [date],
  convert(varchar(8000),textdetails) as [Subject],                                                                         
 dbo.fn_GetListItemName(c.bitTask)  AS [Type],                                                                 
  A1.vcFirstName +'' ''+ A1.vcLastName +'' ''+                                      
  case when A1.numPhone<>'''' then + A1.numPhone +case when A1.numPhoneExtension<>'''' then '' - '' + A1.numPhoneExtension else '''' end  else '''' end as [Phone],                        
  A2.vcFirstName+ '' ''+A2.vcLastName  as assignedto,                         
  c.caseid,                           
  (select top 1 vcCaseNumber from cases where cases.numcaseid= c.caseid )as vcCasenumber,                               
  isnull(caseTimeid,0)as caseTimeid,                              
  isnull(caseExpid,0)as caseExpid  ,                        
  1 as tinttype ,dtCreatedDate,'''' as [FROM]     ,isnull(C.bitClosedflag,0) as bitClosedflag,0 as bitHasAttachments,
'''' as vcBody
  from  Communication C                                              
  JOIN AdditionalContactsInformation  A1                                           
  ON c.numContactId = A1.numContactId                                                                             
  left join AdditionalContactsInformation A2 on A2.numContactID=c.numAssign
  left join UserMaster UM on A2.numContactid=UM.numUserDetailId and isnull(UM.bitActivateFlag,0)=1                                                                           
  left join listdetails on numActivity=numListID                           
  LEFT JOIN [Correspondence] CR ON CR.numCommID = C.numCommID
  where  C.numDomainID=' +convert(varchar(15),@numDomainID)                       
  + 'AND (numOpenRecordID = '+ convert(varchar(15),@numOpenRecordID) +' OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  if (@numdivisionId =0 and @numContactId<>0) set  @strSql=@strSql+' and C.numContactId='+ convert(varchar(15),@numContactId)                        
  if @numdivisionId <> 0 set  @strSql=@strSql+' and  C.numDivisionId='+ convert(varchar(15),@numdivisionId)              
  if @numCaseID>0  set  @strSql=@strSql+' and  C.CaseId='+ convert(varchar(15),@numCaseID)                       
  if @filter =0  
	SET @strSql=@strSql+ ' and dtStartTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtStartTime  <= '''+ convert(varchar(30),@ToDate)  +''''
  else
    SET @strSql=@strSql+ ' and c.bitTask=' + CONVERT(varchar(12),@filter) + ' and ((dtStartTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtStartTime  <= '''+ convert(varchar(30),@ToDate)  +''') OR (dtEndTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtEndTime  <= '''+ convert(varchar(30),@ToDate)  +'''))'
  
  IF @tintMode=1 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(1,3) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=2 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(2,4) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=5 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(5) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=6 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(6) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
                          
  IF @tintSortOrder =0 and @SeachKeyword <>'' 
	SET @strSql=@strSql+ ' and textdetails like ''%'+@SeachKeyword+'%'''
  ELSE
	SET @strSql=@strSql+ ' and c.bitTask=' + CONVERT(varchar(12),@filter) + ' and textdetails like ''%'+@SeachKeyword+'%'''                     
  
  IF @bitOpenCommu=1  SET @strSql=@strSql+ ' and isnull(C.bitClosedflag,0)=0'
 end                
 set @strSql='With tblCorr as (SELECT ROW_NUMBER() OVER (ORDER BY '+@columnName+' '+ @columnSortOrder+') AS RowNumber,X.* from('+@strSql+')X)'                

 set @strSql=@strSql +'                       
  select  *  from tblCorr
  Where RowNumber > '+convert(varchar(10),@firstRec)+' and RowNumber < '+convert(varchar(10),@lastRec)+'                     
  union select 0,count(*),null,null,null,'','',null,null,null,null,null,null,null,null,null,null,0,0,null from tblCorr order by RowNumber'                 
                
-- set @strSql=@strSql +'                       
--  select  *,CASE WHEN [Type]=''Email'' THEN dbo.GetEmaillAdd(numEmailHstrID,4)+'', '' +dbo.GetEmaillAdd(numEmailHstrID,1) ELSE '''' END as [From] from tblCorr
--  Where RowNumber > '+convert(varchar(10),@firstRec)+' and RowNumber < '+convert(varchar(10),@lastRec)+'                     
--  union select 0,count(*),null,null,null,'','',null,null,null,null,null,null,null,null,null,null from tblCorr order by RowNumber'                 
                
 print @strSql                
 exec (@strSql)                
end                
else                
select 0,0

/****** Object:  StoredProcedure [dbo].[USP_GetECampaignActionItems]    Script Date: 06/04/2009 15:15:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetECampaignActionItems')
DROP PROCEDURE USP_GetECampaignActionItems
GO
CREATE PROCEDURE [dbo].[USP_GetECampaignActionItems]
AS
  BEGIN
  
  	DECLARE @LastDateTime DATETIME
	DECLARE @CurrentDateTime DATETIME
	
	SELECT @LastDateTime = [dtLastDateTimeECampaign] FROM [WindowsServiceHistory]
	SET @CurrentDateTime = DATEADD(minute, DATEpart(minute,GETUTCDATE()) ,DATEADD(hour,DATEpart(hour,GETUTCDATE()),dbo.[GetUTCDateWithoutTime]()))
	PRINT @LastDateTime
	PRINT @CurrentDateTime


    SELECT   C.numContactID,
             C.numRecOwner,
             intStartDate,
             E.numActionItemTemplate,
             A.numDomainID,
             A.numDivisionId,
             DTL.[numConECampDTLID]
    FROM     ConECampaign C
             JOIN ConECampaignDTL DTL
               ON numConEmailCampID = numConECampID
             JOIN ECampaignDTLs E
               ON DTL.numECampDTLId = E.numECampDTLID
             JOIN [ECampaign] EC ON E.[numECampID] = EC.[numECampaignID]
             JOIN AdditionalContactsInformation A
               ON A.numContactID = C.[numContactID]
    WHERE    bitSend IS NULL 
            AND [bitEngaged]= 1
            AND [numActionItemTemplate] > 0
		    AND ((DATEADD(MINUTE, (ISNULL(EC.fltTimeZone,0) * 60), DTL.dtExecutionDate)
            BETWEEN DATEADD(MINUTE,(ISNULL(EC.fltTimeZone,0) - 2) * 60,@LastDateTime) AND DATEADD(MINUTE,ISNULL(EC.fltTimeZone,0) * 60,@CurrentDateTime)) OR
			(SELECT COUNT(*) FROM ConECampaignDTL WHERE numConEmailCampID = C.numConEmailCampID AND ISNULL(bitSend,0) = 1) = 0)
              

  END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetECampaignContactFollowUpList')
DROP PROCEDURE USP_GetECampaignContactFollowUpList
GO
CREATE PROCEDURE USP_GetECampaignContactFollowUpList
AS 
    BEGIN
	DECLARE @LastDateTime DATETIME
	DECLARE @CurrentDateTime DATETIME
	
	SELECT @LastDateTime = [dtLastDateTimeECampaign] FROM [WindowsServiceHistory]
	SET @CurrentDateTime = DATEADD(minute, DATEpart(minute,GETUTCDATE()) ,DATEADD(hour,DATEpart(hour,GETUTCDATE()),dbo.[GetUTCDateWithoutTime]()))

    SELECT  
			E.[numECampaignID],
			ED.[numECampDTLId],
            C.numContactID,
            ED.[numFollowUpID],
            DTL.[numConECampDTLID],
            C.[numConEmailCampID],
                DATEADD(MINUTE, ISNULL(E.fltTimeZone,0) * 60, DTL.dtExecutionDate) AS StartDate,
                DATEADD(MINUTE,(ISNULL(E.fltTimeZone,0)-2)*60,@LastDateTime) Between1,
                DATEADD(MINUTE,ISNULL(E.fltTimeZone,0) * 60,@CurrentDateTime) Between2,
                E.fltTimeZone
    FROM    ConECampaign C
            JOIN ConECampaignDTL DTL ON numConEmailCampID = numConECampID
            JOIN ECampaignDTLs ED ON ED.numECampDTLId = DTL.numECampDTLID
            JOIN [ECampaign] E ON ED.[numECampID] = E.[numECampaignID]
            JOIN AdditionalContactsInformation A ON A.numContactID = C.[numContactID]
    WHERE   ISNULL(bitFollowUpStatus,0) = 0
			AND 
            [bitEngaged] = 1
            AND [numFollowUpID] > 0
           AND DATEADD(MINUTE, ISNULL(E.fltTimeZone,0) * 60, DTL.dtExecutionDate)
            BETWEEN DATEADD(MINUTE,(ISNULL(E.fltTimeZone,0)-2)*60,@LastDateTime) AND DATEADD(MINUTE,ISNULL(E.fltTimeZone,0) * 60,@CurrentDateTime)
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetECampaignDetailsReport]    Script Date: 06/04/2009 16:23:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Pratik  Vasani
-- Create date: 13/DEC/2008
-- Description:	This Stored proc gets the table for display in Ecampaign report
-- =============================================

--declare @p4 int
--set @p4=0
--exec USP_GetECampaignDetailsReport @numDomainId=1,@CurrentPage=1,@PageSize=20,@TotRecs=@p4 output
--select @p4
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getecampaigndetailsreport')
DROP PROCEDURE usp_getecampaigndetailsreport
GO
CREATE PROCEDURE [dbo].[USP_GetECampaignDetailsReport]
               @numDomainId NUMERIC(9,0)  = 0,
               @CurrentPage INT,
               @PageSize    INT,
               @TotRecs     INT  OUTPUT,
			   @numECampaingID NUMERIC(18,0) = 0
AS
  BEGIN
    CREATE TABLE #tempTable (
      ID INT IDENTITY PRIMARY KEY,
	  numECampaignID NUMERIC(9,0),
	  vcECampName VARCHAR(200),
	  numConEmailCampID NUMERIC(9,0),
      numContactID NUMERIC(9,0),
      CompanyName VARCHAR(100),
      ContactName VARCHAR(100),
      [Status] VARCHAR(20),
	  Template VARCHAR(20),
	  dtSentDate VARCHAR(80),
	  SendStatus VARCHAR(20),
	  dtNextStage VARCHAR(80),
	  Stage INT,
	  NoOfStagesCompleted INT,
	  NoOfEmailSent INT,
	  NoOfEmailOpened INT
      )
    INSERT INTO #tempTable
    
   SELECT
		ECampaign.numECampaignID,
		ECampaign.vcECampName,
		ConECampaign.numConEmailCampID,
		ConECampaign.numContactID,
		dbo.GetCompanyNameFromContactID(numContactID,ECampaign.numDomainId) AS CompanyName,
		dbo.fn_GetContactName(numContactID) AS ContactName,
		(CASE 
			WHEN ISNULL(ECampaign.bitDeleted,0) = 0 
			THEN CASE WHEN ISNULL(ConECampaign.bitEngaged,0) = 0 THEN 'Disengaged' ELSE 'Engaged' END
			ELSE 'Campaign Deleted'
		END) AS Status,
		ISNULL(CampaignExecutedDetail.Template,''),
		ISNULL(dbo.FormatedDateFromDate(CampaignExecutedDetail.bintSentON,@numDomainID),''),
		(CASE 
			WHEN ISNULL(CampaignExecutedDetail.bitSend,0)=1 AND ISNULL(CampaignExecutedDetail.tintDeliveryStatus,0)=0 THEN 'Failed'
			WHEN ISNULL(CampaignExecutedDetail.bitSend,0)=1 AND ISNULL(CampaignExecutedDetail.tintDeliveryStatus,0)=1 THEN 'Success'
			ELSE 'Pending'
		END) AS SendStatus
		,
		 dbo.FormatedDateFromDate(DATEADD(MINUTE,ISNULL(ECampaign.fltTimeZone,0) * 60,CampaignNextExecutationDetail.dtExecutionDate),@numDomainID),
		 ISNULL((SELECT COUNT(*) FROM ConECampaignDTL WHERE numConECampID = ConECampaign.numConEmailCampID),0) AS NoOfStages,
		 ISNULL((SELECT COUNT(*) FROM ConECampaignDTL WHERE numConECampID = ConECampaign.numConEmailCampID AND ISNULL(bitSend,0)=1),0) AS NoOfStagesCompleted,
		 (SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ECampaignDTLs ON ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId WHERE ECampaignDTLs.numEmailTemplate IS NOT NULL AND numConECampID = ConECampaign.numECampaignID AND bitSend = 1 AND ISNULL(tintDeliveryStatus,0) = 1) AS NoOfEmailSent,
		 (SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ECampaignDTLs ON ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId WHERE ECampaignDTLs.numEmailTemplate IS NOT NULL AND numConECampID = ConECampaign.numECampaignID AND bitSend = 1 AND ISNULL(tintDeliveryStatus,0) = 1 AND ISNULL(bitEmailRead,0) = 1) AS NoOfEmailOpened
	FROM
		ConECampaign
	INNER JOIN
		ECampaign
	ON
		ConECampaign.numECampaignID = ECampaign.numECampaignID
	OUTER APPLY
		(
			SELECT 
				TOP 1
				(CASE 
					WHEN ECampaignDTLs.numEmailTemplate IS NOT NULL THEN GenericDocuments.vcDocName
					WHEN ECampaignDTLs.numActionItemTemplate IS NOT NULL THEN tblActionItemData.TemplateName
					ELSE ''
				END) AS Template,
				ConECampaignDTL.bitSend,
				ConECampaignDTL.bintSentON,
				ConECampaignDTL.tintDeliveryStatus
			FROM 
				ConECampaignDTL 
			INNER JOIN 
				ECampaignDTLs 
			ON 
				ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId
			LEFT JOIN 
				GenericDocuments 
			ON 
				GenericDocuments.numGenericDocID = ECampaignDTLs.numEmailTemplate
			LEFT JOIN 
				tblActionItemData
			ON
				tblActionItemData.RowID = ECampaignDTLs.numActionItemTemplate
			WHERE
				ECampaignDTLs.numECampID = ECampaign.numECampaignID AND
				ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID AND
				ConECampaignDTL.bitSend IS NOT NULL
			ORDER BY
				numConECampDTLID DESC
		) CampaignExecutedDetail
	OUTER APPLY
		(
			SELECT 
				TOP 1
				ConECampaignDTL.dtExecutionDate
			FROM 
				ConECampaignDTL 
			INNER JOIN 
				ECampaignDTLs 
			ON 
				ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId
			LEFT JOIN 
				GenericDocuments 
			ON 
				ECampaignDTLs.numEmailTemplate = GenericDocuments.numGenericDocID
			LEFT JOIN 
				tblActionItemData
			ON
				ECampaignDTLs.numActionItemTemplate = tblActionItemData.RowID
			WHERE
				ECampaignDTLs.numECampID = ECampaign.numECampaignID AND
				ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID AND
				ConECampaignDTL.bitSend IS NULL
			ORDER BY
				numConECampDTLID
		) CampaignNextExecutationDetail
	WHERE
		ECampaign.numDomainID = @numDomainId AND
		(ECampaign.numECampaignID = @numECampaingID OR ISNULL(@numECampaingID,0) = 0)
    
   
            
    DECLARE  @firstRec  AS INTEGER
    DECLARE  @lastRec  AS INTEGER
    SET @firstRec = (@CurrentPage
                       - 1)
                      * @PageSize
    SET @lastRec = (@CurrentPage
                      * @PageSize
                      + 1)
    SET @TotRecs = (SELECT COUNT(* )
                    FROM   #tempTable)
    SELECT   *
    FROM     #tempTable
    WHERE    ID > @firstRec
             AND ID < @lastRec
    ORDER BY id

    DROP TABLE #tempTable
    SELECT @TotRecs AS TotRecs
  END
/****** Object:  StoredProcedure [dbo].[USP_GetEmailCampaignMails]    Script Date: 06/04/2009 15:10:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetEmailCampaignMails')
DROP PROCEDURE USP_GetEmailCampaignMails
GO
CREATE PROCEDURE [dbo].[USP_GetEmailCampaignMails]
AS 
BEGIN
-- Organization Fields Which Needs To Be Replaced
DECLARE  @vcCompanyName  AS VARCHAR(200)
DECLARE  @OrgNoOfEmployee  AS VARCHAR(12)
DECLARE  @OrgShippingAddress  AS VARCHAR(1000)
DECLARE  @Signature  AS VARCHAR(1000)


-- Contact Fields Which Needs To Be Replaced.
DECLARE @ContactAge AS VARCHAR(3)
DECLARE @ContactAssistantEmail AS VARCHAR(100)
DECLARE @AssistantFirstName AS VARCHAR(100)
DECLARE @AssistantLastName AS VARCHAR(100)
DECLARE @ContactAssistantPhone AS VARCHAR(20)
DECLARE @ContactAssistantPhoneExt AS VARCHAR(10)
DECLARE @ContactCategory AS VARCHAR(100)
DECLARE @ContactCell AS VARCHAR(20)
DECLARE @ContactDepartment AS VARCHAR(100)
DECLARE @ContactDripCampaign AS VARCHAR(500)
DECLARE @ContactEmail AS VARCHAR(100)
DECLARE @ContactFax AS VARCHAR(100)
DECLARE @ContactFirstName AS VARCHAR(100)
DECLARE @ContactGender AS VARCHAR(10)
DECLARE @ContactHomePhone AS VARCHAR(20)
DECLARE @ContactLastName AS VARCHAR(100)
DECLARE @ContactManager AS VARCHAR(100)
DECLARE @ContactPhone AS VARCHAR(20)	
DECLARE @ContactPhoneExt AS VARCHAR(10)
DECLARE @ContactPosition AS VARCHAR(100)
DECLARE @ContactPrimaryAddress AS VARCHAR(1000)
DECLARE @ContactStatus AS VARCHAR(100)
DECLARE @ContactTeam AS VARCHAR(100)
DECLARE @ContactTitle AS VARCHAR(100)
DECLARE @ContactType AS VARCHAR(100)
      
DECLARE  @numConEmailCampID  AS NUMERIC(9)
DECLARE  @intStartDate  AS DATETIME


DECLARE  @numConECampDTLID  AS NUMERIC(9)
declare @numContactID as numeric(9)   
declare @numFromContactID as numeric(9)   
declare @numEmailTemplate as varchar(15)
declare @numDomainId as numeric(9)
DECLARE  @tintFromField  AS TINYINT

declare @vcSubject as varchar(1000)
declare @vcBody as varchar(8000)              
declare @vcFormattedBody as varchar(8000)

DECLARE @LastDateTime DATETIME
DECLARE @CurrentDateTime DATETIME

SELECT @LastDateTime = [dtLastDateTimeECampaign] FROM [WindowsServiceHistory]
SET @CurrentDateTime = DATEADD(minute, DATEpart(minute,GETUTCDATE()) ,DATEADD(hour,DATEpart(hour,GETUTCDATE()),dbo.[GetUTCDateWithoutTime]()))
PRINT @LastDateTime
PRINT @CurrentDateTime


Create table #tempTableSendApplicationMail
(  vcTo varchar(2000),              
   vcSubject varchar(2000),              
   vcBody varchar(Max),              
   vcCC varchar(2000),      
   numDomainID numeric(9),
   numConECampDTLID NUMERIC(9) NULL,
   tintFromField TINYINT,
   numContactID NUMERIC,
   numFromContactID NUMERIC
 )                

-- EC.fltTimeZone-2 is used to fetch all email which are not sent from past 2 housrs of last execution date 
-- in case of amazon ses service is down and not able to send email in first try
SELECT   TOP 1 @numContactID = C.numContactID,
               @numConECampDTLID = numConECampDTLID,
               @numConEmailCampID = numConEmailCampID,
               @intStartDate = intStartDate,
               @numEmailTemplate = numEmailTemplate,
               @numDomainId = EC.numDomainID,
               @tintFromField = EC.[tintFromField],
			   @numFromContactID = numFromContactID
FROM     ConECampaign C
         JOIN ConECampaignDTL DTL
           ON numConEmailCampID = numConECampID
         JOIN ECampaignDTLs E
           ON DTL.numECampDTLId = E.numECampDTLID
         JOIN [ECampaign] EC ON E.[numECampID] = EC.[numECampaignID] 
WHERE    bitSend IS NULL 
         AND bitengaged = 1
         AND numEmailTemplate > 0 
		 AND ISNULL(EC.bitDeleted,0) = 0
		 -- When campaign is first time assigned to contact it may happen that it is assigned after service already executed 
		 -- so first email will be send immediately and after that service will compare execution datetime and current datetime
         AND ((DATEADD(MINUTE, ISNULL(EC.fltTimeZone,0) * 60, DTL.dtExecutionDate)
            BETWEEN DATEADD(MINUTE,(ISNULL(EC.fltTimeZone,0)-2)*60,@LastDateTime) AND DATEADD(MINUTE,ISNULL(EC.fltTimeZone,0) * 60,@CurrentDateTime)) OR
			(SELECT COUNT(*) FROM ConECampaignDTL WHERE numConEmailCampID = C.numConEmailCampID AND ISNULL(bitSend,0) = 1) = 0)
          
ORDER BY numConEmailCampID
PRINT '@numConEmailCampID='
PRINT @numConEmailCampID



WHILE @numConEmailCampID > 0
  BEGIN
	PRINT 'inside @numConEmailCampID='
	PRINT @numConEmailCampID

    SELECT 
		   @ContactFirstName = ISNULL(A.vcFirstName,''),
           @ContactLastName = ISNULL(A.vcLastName,''),
           @vcCompanyName = ISNULL(C.vcCompanyName,''),
		   @OrgNoOfEmployee = ISNULL(dbo.GetListIemName(C.numNoOfEmployeesId),''),
		   @OrgShippingAddress = vcCompanyName + ' ,<br>' + ISNULL(AD2.vcStreet,'') + ' <br>' + ISNULL(AD2.VcCity,'') + ' ,' 
								+ ISNULL(dbo.fn_GetState(AD2.numState),'') + ' ' + ISNULL(AD2.vcPostalCode,'') + ' <br>' + 
								ISNULL(dbo.fn_GetListItemName(AD2.numCountry),''),								   
		   @Signature = ISNULL((select U.txtSignature from UserMaster U where U.numUserDetailId=A.numRecOwner),''),
           @ContactPhone = ISNULL(A.numPhone,''),
           @ContactEmail = ISNULL(A.vcEmail,''),
		   @ContactAge = ISNULL(dbo.GetAge(A.bintDOB, GETUTCDATE()),''),
		   @ContactAssistantEmail = ISNULL(A.vcAsstEmail,''),
		   @AssistantFirstName = ISNULL(A.VcAsstFirstName,''),
		   @AssistantLastName = ISNULL(A.vcAsstLastName,''),
		   @ContactAssistantPhone = ISNULL(A.numAsstPhone,''),
		   @ContactAssistantPhoneExt= ISNULL(A.numAsstExtn,''),
		   @ContactCategory=ISNULL([dbo].[GetListIemName](A.vcCategory),''),
		   @ContactCell=ISNULL(A.numCell,''),
		   @ContactDepartment=ISNULL([dbo].[GetListIemName](A.vcDepartment),''),
		   @ContactDripCampaign=ISNULL((SELECT vcECampName FROM [ECampaign] WHERE numECampaignID = A.numECampaignID), ''),
		   @ContactFax=ISNULL(A.vcFax,''),
		   @ContactGender=(CASE WHEN A.charSex = 'M' THEN 'Male' WHEN A.charSex = 'F' THEN 'Female' ELSE '-' END),
		   @ContactHomePhone=ISNULL(A.numHomePhone,''),
		   @ContactManager=ISNULL(dbo.fn_GetContactName(A.numManagerId),''),
		   @ContactPhoneExt=ISNULL(A.numPhoneExtension,''),
		   @ContactPosition= ISNULL([dbo].[GetListIemName](A.vcPosition),''),
		   @ContactPrimaryAddress=ISNULL(dbo.getContactAddress(A.numContactID),''),
		   @ContactStatus=ISNULL([dbo].[GetListIemName](A.numEmpStatus),''),
		   @ContactTeam=ISNULL([dbo].[GetListIemName](A.numTeam),''),
		   @ContactTitle=ISNULL(A.vcTitle,''),
		   @ContactType=ISNULL(dbo.GetListIemName(A.numContactType),'')
    FROM   AdditionalContactsInformation A
           JOIN DivisionMaster D
             ON A.numDivisionId = D.numDivisionID
           JOIN CompanyInfo C
             ON D.numCompanyID = C.numCompanyId
		   LEFT JOIN 
				dbo.AddressDetails AD2 
			ON 
				AD2.numDomainID=D.numDomainID 
				AND AD2.numRecordID= D.numDivisionID 
				AND AD2.tintAddressOf=2 
				AND AD2.tintAddressType=2 
				AND AD2.bitIsPrimary=1
    WHERE  A.numContactId = @numContactID
    
    SELECT @vcSubject = vcSubject,
           @vcBody = vcDocDesc
    FROM   genericdocuments
    WHERE  numGenericDocID = @numEmailTemplate
    
	--Replace organization fields from body
    SET @vcFormattedBody = REPLACE(@vcBody,'##OrganizationName##',@vcCompanyName)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##OrgNoOfEmployee##',@OrgNoOfEmployee)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##OrgShippingAddress##',@OrgShippingAddress)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##Signature##',@Signature)

	--Replace contact fields from body
    SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactFirstName##',@ContactFirstName)
    SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactLastName##',@ContactLastName)
    SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactPhone##',@ContactPhone)
    SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactEmail##',@ContactEmail)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactAge##',@ContactAge)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactAssistantEmail##',@ContactAssistantEmail)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##AssistantFirstName##',@AssistantFirstName)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##AssistantLastName##',@AssistantLastName)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactAssistantPhone##',@ContactAssistantPhone)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactAssistantPhoneExt##',@ContactAssistantPhoneExt)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactCategory##',@ContactCategory)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactCell##',@ContactCell)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactDepartment##',@ContactDepartment)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactDripCampaign##',@ContactDripCampaign)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactFax##',@ContactFax)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactGender##',@ContactGender)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactHomePhone##',@ContactHomePhone)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactManager##',@ContactManager)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactPhoneExt##',@ContactPhoneExt)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactPosition##',@ContactPosition)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactPrimaryAddress##',@ContactPrimaryAddress)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactStatus##',@ContactStatus)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactTeam##',@ContactTeam)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactTitle##',@ContactTitle)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactType##',@ContactType)
    SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactOpt-OutLink##','')

    
   INSERT INTO #tempTableSendApplicationMail
   VALUES (     @ContactEmail,
                @vcSubject,
                @vcFormattedBody,
                '',
                @numDomainId,
                @numConECampDTLID,
                @tintFromField,
                @numContactID,
                @numFromContactID)
                

    
    SELECT   TOP 1 @numContactID = C.numContactID,
                   @numConECampDTLID = numConECampDTLID,
                   @numConEmailCampID = numConEmailCampID,
                   @intStartDate = intStartDate,
                   @numEmailTemplate = numEmailTemplate,
                   @numDomainId = EC.numDomainID,
                   @tintFromField = EC.[tintFromField],
                   @numFromContactID = numFromContactID
    FROM     ConECampaign C
             JOIN ConECampaignDTL DTL
               ON numConEmailCampID = numConECampID
             JOIN ECampaignDTLs E
               ON DTL.numECampDTLId = E.numECampDTLID
             JOIN [ECampaign] EC ON E.[numECampID] = EC.[numECampaignID] 
--             JOIN AdditionalContactsInformation A
--               ON A.numContactID = C.numRecOwner
    WHERE    bitSend IS NULL 
             AND bitengaged = 1
             AND numConEmailCampID > @numConEmailCampID
			 AND numEmailTemplate > 0 
			 AND ISNULL(EC.bitDeleted,0) = 0
			AND DATEADD(MINUTE, ISNULL(EC.fltTimeZone,0) * 60, DTL.dtExecutionDate)
            BETWEEN DATEADD(MINUTE,(ISNULL(EC.fltTimeZone,0)-2)*60,@LastDateTime) AND DATEADD(MINUTE,ISNULL(EC.fltTimeZone,0) * 60,@CurrentDateTime)
    ORDER BY numConEmailCampID
    
    IF @@ROWCOUNT = 0
      SET @numConEmailCampID = 0
  END
  
  
  SELECT    X.*,
			ISNULL(EmailBroadcastConfiguration.vcAWSDomain,'') vcAWSDomain,
            ISNULL(EmailBroadcastConfiguration.vcAWSAccessKey,'') vcAWSAccessKey,
            ISNULL(EmailBroadcastConfiguration.vcAWSSecretKey,'') vcAWSSecretKey,
            ISNULL(EmailBroadcastConfiguration.vcFrom,'') vcFrom
  FROM      ( SELECT    A.[vcTo],
                        A.[vcSubject],
                        A.[vcBody],
                        A.[vcCC],
                        A.[numDomainID],
                        A.[numConECampDTLID],
                        A.[tintFromField],
                        A.[numContactID],
                        CASE [tintFromField]
                          WHEN 1 THEN --Record Owner of Company
                               DM.[numRecOwner]
                          WHEN 2 THEN --Assignee of Company
                               DM.[numAssignedTo]
                          WHEN 3 THEN numFromContactID
                          ELSE DM.[numRecOwner]
                        END AS numFromContactID
              FROM      #tempTableSendApplicationMail A
                        INNER JOIN [AdditionalContactsInformation] ACI ON ACI.[numContactId] = A.numContactID
                        INNER JOIN [DivisionMaster] DM ON DM.[numDivisionID] = ACI.[numDivisionId]
            ) X
            LEFT JOIN 
				EmailBroadcastConfiguration
			ON
				X.numDomainID = EmailBroadcastConfiguration.numDomainID
  
  DROP TABLE #tempTableSendApplicationMail

  UPDATE [WindowsServiceHistory] SET [dtLastDateTimeECampaign] = GETUTCDATE()
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemOrderDependencies')
DROP PROCEDURE USP_GetItemOrderDependencies
GO
CREATE PROCEDURE [dbo].[USP_GetItemOrderDependencies]         
@numDomainID as numeric(9)=0,    
@numItemCode as numeric(9)=0,
@ClientTimeZoneOffset Int,
@tintOppType as tinyint=0,
--@tintshipped as tinyint=0,
@byteMode as tinyint=0
as        

IF @byteMode=0 
BEGIN

Create table #temp (numoppid numeric(18),vcPOppName varchar(100),tintOppType tinyint,
bintCreatedDate datetime,bintAccountClosingDate datetime,OppType varchar(20),OppStatus varchar(20),vcCompanyname varchar(200),vcItemName varchar(200), numReturnHeaderID numeric(18))

--Regular Item
insert into #temp
select OM.numoppid,OM.vcPoppName,OM.tintOppType,OM.bintCreatedDate,OM.bintAccountClosingDate,
Case When OM.tintOppType=1 THEN CASE WHEN OM.tintOppStatus=0 THEN 'Sales Opportunity' ELSE 'Sales Order' END 
				 else CASE WHEN OM.tintOppStatus=0 THEN 'Purchase Opportunity' ELSE 'Purchase Order' END END as OppType,
 CASE ISNULL(OM.tintshipped, 0) WHEN 0 THEN 'Open' WHEN 1 THEN 'Closed' END OppStatus,
CI.vcCompanyName + Case when isnull(DM.numCompanyDiff,0)>0 then  '  ' + dbo.fn_getlistitemname(DM.numCompanyDiff) + ':' + isnull(DM.vcCompanyDiff,'') else '' end as vcCompanyname,
'', 0
 FROM    OpportunityItems Opp
        JOIN OpportunityMaster OM ON Opp.numOppID = oM.numOppID
        JOIN item I ON Opp.numItemCode = i.numItemcode
		JOIN divisionMaster DM ON OM.numDivisionID = DM.numDivisionID
        LEFT JOIN CompanyInfo CI ON CI.numCompanyID = DM.numCompanyID
WHERE   om.numDomainId = @numDomainID
        AND I.numDomainId = @numDomainID AND I.numItemCode=@numItemCode
		AND 1=(Case When @tintOppType=1 OR @tintOppType=2 then Case WHEN OM.tintOppType=@tintOppType and OM.tintOppStatus = 1 then 1 else 0 end
				 When @tintOppType=3 then Case WHEN OM.tintOppType=1 AND OM.tintOppStatus=0 then 1 else 0 end
				 When @tintOppType=4 then Case WHEN OM.tintOppType=2 AND OM.tintOppStatus=0 then 1 else 0 end 
				 When @tintOppType=5 then 0
				 WHEN @tintOppType=6 then 0
				 ELSE 1 end )
		--AND OM.tintshipped=@tintshipped
ORDER BY OM.bintCreatedDate desc


--Kit/Assembly Item (Parent Item of selected item)
--;WITH CTE(numItemCode,vcItemName)
--AS
--(
--select numItemKitID,vcItemName
--from item                               
--INNER join ItemDetails Dtl on numItemKitID=numItemCode
--where  numChildItemID=@numItemCode
--
--UNION ALL
--
--select dtl.numItemKitID ,i.vcItemName
--from item i                               
--INNER JOIN ItemDetails Dtl on Dtl.numItemKitID=i.numItemCode
--INNER JOIN CTE c ON Dtl.numChildItemID = c.numItemCode
--where Dtl.numChildItemID!=@numItemCode
--)

DECLARE @strSQL AS NVARCHAR(MAX)
DECLARE @strWHERE AS NVARCHAR(MAX)
SET @strWHERE = ' WHERE 1=1 '
IF @tintOppType = 5 
BEGIN
 SET @strWHERE = @strWHERE + ' AND [RH].[tintReturnType] = 1 '
END
ELSE IF @tintOppType = 6
BEGIN
 SET @strWHERE = @strWHERE + ' AND [RH].[tintReturnType] = 2 '
END
ELSE IF @tintOppType = 0
BEGIN
 SET @strWHERE = @strWHERE + ' AND [RH].[tintReturnType] IN (1,2) '
END
ELSE
	SET @strWHERE = @strWHERE + ' AND [RH].[tintReturnType] = -1 '

SET @strSQL = '
insert into #temp
select OM.numoppid,OM.vcPoppName,OM.tintOppType,OM.bintCreatedDate,OM.bintAccountClosingDate,
Case When OM.tintOppType=1 THEN CASE WHEN OM.tintOppStatus=0 THEN ''Sales Opportunity'' ELSE ''Sales Order'' END 
				 else CASE WHEN OM.tintOppStatus=0 THEN ''Purchase Opportunity'' ELSE ''Purchase Order'' END END as OppType,
 CASE ISNULL(OM.tintshipped, 0) WHEN 0 THEN ''Open'' WHEN 1 THEN ''Closed'' END OppStatus,
CI.vcCompanyName + Case when isnull(DM.numCompanyDiff,0)>0 then  ''  '' + dbo.fn_getlistitemname(DM.numCompanyDiff) + '':'' + isnull(DM.vcCompanyDiff,'''') else '''' end as vcCompanyname,
I.vcItemName + Case when I.bitAssembly=1 then '' (Assembly)'' When I.bitKitParent=1 then '' (Kit)'' else '''' end
,0 [numReturnHeaderID]
 FROM   OpportunityKitItems OKI 
		JOIN OpportunityItems Opp ON OKI.numOppItemID=Opp.numoppitemtCode
		join Item I on Opp.numItemCode =I.numItemCode  
        JOIN OpportunityMaster OM ON Opp.numOppID = oM.numOppID AND OKI.numOppID = oM.numOppID
		JOIN divisionMaster DM ON OM.numDivisionID = DM.numDivisionID
        LEFT JOIN CompanyInfo CI ON CI.numCompanyID = DM.numCompanyID
WHERE  OKI.numChildItemID=' + CONVERT(VARCHAR(10),@numItemCode) + '  AND om.numDomainId = ' + CONVERT(VARCHAR(10),@numDomainID) + ' 
        AND I.numDomainId = ' + CONVERT(VARCHAR(10),@numDomainID) + '  
		AND 1=(Case When ' + CONVERT(VARCHAR(10),@tintOppType) + ' =1 OR ' + CONVERT(VARCHAR(10),@tintOppType) + ' =2 then Case WHEN OM.tintOppType=' + CONVERT(VARCHAR(10),@tintOppType) + '  and OM.tintOppStatus = 1 then 1 else 0 end
					When ' + CONVERT(VARCHAR(10),@tintOppType) + ' =3 then Case WHEN OM.tintOppType=1 AND OM.tintOppStatus=0 then 1 else 0 end
					When ' + CONVERT(VARCHAR(10),@tintOppType) + ' =4 then Case WHEN OM.tintOppType=2 AND OM.tintOppStatus=0 then 1 else 0 end 
					When ' + CONVERT(VARCHAR(10),@tintOppType) + ' =5 then 0
					When ' + CONVERT(VARCHAR(10),@tintOppType) + ' =6 then 0
					ELSE 1 
			  END )
UNION 

SELECT  RH.[numReturnHeaderID] numoppid,RH.[vcRMA] vcPoppName, [RH].[tintReturnType] tintOppType,RH.[dtCreatedDate] bintCreatedDate,NULL bintAccountClosingDate,
CASE [RH].[tintReturnType] WHEN 1 THEN ''Sales Return'' WHEN 2 THEN ''Purchase Return'' ELSE '''' END OppType,
'''' OppStatus,
CI.vcCompanyName + CASE WHEN ISNULL(DM.numCompanyDiff,0) > 0 THEN ''  '' + dbo.fn_getlistitemname(DM.numCompanyDiff) + '':'' + ISNULL(DM.vcCompanyDiff,'''') ELSE '''' END AS vcCompanyname,
I.vcItemName + CASE WHEN I.bitAssembly = 1 THEN '' (Assembly)'' WHEN I.bitKitParent = 1 THEN '' (Kit)'' ELSE '''' END
,RH.[numReturnHeaderID]
FROM [dbo].[ReturnHeader] AS RH 
JOIN [dbo].[ReturnItems] AS RI  ON [RH].[numReturnHeaderID] = [RI].[numReturnHeaderID] AND [RH].[tintReturnType] IN (1,2)
JOIN [dbo].[Item] AS I ON [RI].[numItemCode] = [I].[numItemCode] AND [I].[numDomainID] = RH.[numDomainId]
JOIN divisionMaster DM ON RH.numDivisionID = DM.numDivisionID
LEFT JOIN CompanyInfo CI ON CI.numCompanyID = DM.numCompanyID 
' + @strWHERE + ' AND RH.[numDomainId] = ' + CONVERT(VARCHAR(10),@numDomainID) + ' AND [RI].[numItemCode] = ' + CONVERT(VARCHAR(10),@numItemCode) + ' 
 ORDER BY bintCreatedDate desc'


PRINT @strSQL
EXEC SP_EXECUTESQL @strSQL


select numoppid,vcPoppName,tintOppType,
case when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,bintCreatedDate) )= convert(varchar(11),getdate()) then '<b><font color=red>Today</font></b>'
	 when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,bintCreatedDate) )= convert(varchar(11),dateadd(day,-1,getdate())) then '<b><font color=purple>YesterDay</font></b>'
	 when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,bintCreatedDate) )= convert(varchar(11),dateadd(day,1,getdate())) then'<b><font color=orange>Tommorow</font></b>' 
	else dbo.FormatedDateFromDate(DateAdd(minute, -@ClientTimeZoneOffset,bintCreatedDate ),1) end  CreatedDate,
case when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,bintAccountClosingDate) )= convert(varchar(11),getdate()) then '<b><font color=red>Today</font></b>'
	 when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,bintAccountClosingDate) )= convert(varchar(11),dateadd(day,-1,getdate())) then '<b><font color=purple>YesterDay</font></b>'
	 when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,bintAccountClosingDate) )= convert(varchar(11),dateadd(day,1,getdate())) then'<b><font color=orange>Tommorow</font></b>' 
	else dbo.FormatedDateFromDate(DateAdd(minute, -@ClientTimeZoneOffset,bintAccountClosingDate ),1) end  AccountClosingDate,
 OppType,OppStatus,vcCompanyName,vcItemName,numReturnHeaderID from #temp 
  ORDER by bintCreatedDate desc 

drop table #temp
END


ELSE IF @byteMode=1 --Parent Items of selected item
BEGIN

;WITH CTE(numItemCode,vcItemName,bitAssembly,bitKitParent)
AS
(
select numItemKitID,vcItemName,bitAssembly,bitKitParent
from item                                
INNER join ItemDetails Dtl on numItemKitID=numItemCode
where  numChildItemID=@numItemCode

UNION ALL

select dtl.numItemKitID ,i.vcItemName,i.bitAssembly,i.bitKitParent
from item i                               
INNER JOIN ItemDetails Dtl on Dtl.numItemKitID=i.numItemCode
INNER JOIN CTE c ON Dtl.numChildItemID = c.numItemCode
where Dtl.numChildItemID!=@numItemCode
)

select numItemCode,vcItemName,Case when bitAssembly=1 then 'Assembly' When bitKitParent=1 then 'Kit' end as ItemType
from CTE                                
END
GO
GO
/****** Object:  StoredProcedure [dbo].[USP_GetLeadList1]    Script Date: 09/08/2010 14:11:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--- Created By Anoop Jayaraj                                                                
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getleadlist1')
DROP PROCEDURE usp_getleadlist1
GO
CREATE PROCEDURE [dbo].[USP_GetLeadList1]                                                                          
 @numGrpID numeric,                                                                              
 @numUserCntID numeric,
 @tintUserRightType tinyint,
 @CustName varchar(100)='',                                                                              
 @FirstName varChar(100)= '',                                                                              
 @LastName varchar(100)='',                                                                            
 @SortChar char(1)='0' ,                                                                            
 @numFollowUpStatus as numeric(9)=0 ,                                                                            
 @CurrentPage int,                                                                            
 @PageSize int,                                                                            
 @columnName as Varchar(50)='',                                                                            
 @columnSortOrder as Varchar(10)='',                                                    
 @ListType as tinyint=0,                                                    
 @TeamType as tinyint=0,                                                     
 @TeamMemID as numeric(9)=0,                                                
 @numDomainID as numeric(9)=0    ,                  
 @ClientTimeZoneOffset as int  ,              
 @bitPartner as bit,
 @bitActiveInActive as bit,
 @vcRegularSearchCriteria varchar(1000)='',
 @vcCustomSearchCriteria varchar(1000)=''                                                                                  
AS       

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

select numAddressID,numCountry,numState,bitIsPrimary,tintAddressOf,tintAddressType,numRecordID,numDomainID into #tempAddressLead from AddressDetails  where tintAddressOf=2 AND bitIsPrimary=1 and numDomainID=@numDomainID

declare @join as varchar(400)              
set @join = ''       
 if @columnName like 'CFW.Cust%'             
begin            
             
 set @join= ' left Join CFW_FLD_Values CFW on CFW.RecId=DM.numDivisionID and CFW.fld_id= '+replace(@columnName,'CFW.Cust','') +' '                                                     
 set @columnName='CFW.Fld_Value'            
              
end                                     
if @columnName like 'DCust%'            
begin            
                                                   
 set @join= ' left Join CFW_FLD_Values CFW on CFW.RecId=DM.numDivisionID  and CFW.fld_id= '+replace(@columnName,'DCust','')                                                                                               
 set @join= @join +' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '            
 set @columnName='LstCF.vcData'              
            
end      
      
                                                                             
declare @strSql as varchar(8000)                                                                             


declare @firstRec as integer                                                                            
  declare @lastRec as integer                                                                            
 set @firstRec= (@CurrentPage-1) * @PageSize                                                                           
     set @lastRec= (@CurrentPage*@PageSize+1)                                                                            
 
  DECLARE @strShareRedordWith AS VARCHAR(300);
 SET @strShareRedordWith=' DM.numDivisionID in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=1 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '
                                                                                                                           
                                                              
 set @strSql= '
DECLARE @numDomain NUMERIC
SET @numDomain='+ convert(varchar(15),@numDomainID)+';

 with tblLeads as ( SELECT     '         
 set  @strSql= @strSql+ ' ROW_NUMBER() OVER (ORDER BY '+ CASE WHEN PATINDEX('%txt%',@columnName)>0 THEN ' CONVERT(VARCHAR(Max),' + @columnName + ')'  WHEN @columnName='numRecOwner' THEN 'DM.numRecOwner' ELSE @columnName end +' '+ @columnSortOrder+') AS RowNumber,                                                                                            
	COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount,
   DM.numDivisionID 
  FROM                                                                                
   CompanyInfo cmp join divisionmaster DM on cmp.numCompanyID=DM.numCompanyID                                                 
 left join ListDetails lst on lst.numListItemID=DM.numFollowUpStatus           '  +@join+  '                                            
 join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID  
 WHERE                                                                               
   DM.tintCRMType=0  and  ISNULL(ADC.bitPrimaryContact,0)=1   
  AND CMP.numDomainID=DM.numDomainID   
 and DM.numDomainID=ADC.numDomainID                                                        
 AND DM.numGrpID='+convert(varchar(15),@numGrpID)+ '                                                 
 AND DM.numDomainID=@numDomain'               
      
SET @strSql=@strSql + ' AND DM.bitActiveInActive=' + CONVERT(VARCHAR(15), @bitActiveInActive) 
        
if @bitPartner = 1 set @strSql=@strSql+' and (DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+               
           ' or DM.numCreatedBy='+convert(varchar(15),@numUserCntID)+ ') '              
                                                                                    
 if @FirstName<>'' set @strSql=@strSql+' and ADC.vcFirstName  like '''+@FirstName+'%'''                                            
 if @LastName<>'' set @strSql=@strSql+' and ADC.vcLastName like '''+@LastName+'%'''                                            
 if @CustName<>'' set @strSql=@strSql+' and CMP.vcCompanyName like '''+@CustName+'%'''                                                                           
                                                                                   
if @numFollowUpStatus<>0 set @strSql=@strSql + '  AND DM.numFollowUpStatus='+convert(varchar(15),@numFollowUpStatus)                                                                             
if @SortChar<>'0' set @strSql=@strSql + ' And CMP.vcCompanyName like '''+@SortChar+'%'''                            
if @tintUserRightType=1 AND @numGrpID<>2 set @strSql=@strSql + ' AND (DM.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ' or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ' or ' + @strShareRedordWith +')'                                                                             
else if @tintUserRightType=2 set @strSql=@strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or DM.numTerID=0 or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ' or ' + @strShareRedordWith +')' 


if @ListType=0 -- when MySelf radiobutton is checked
begin                                                    
IF @tintUserRightType=1 AND @numGrpID=1 set @strSql=@strSql + ' AND (DM.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ' or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ' or ' + @strShareRedordWith +')'                                                                
if @tintUserRightType=1 AND @numGrpID=5 set @strSql=@strSql + ' AND (DM.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ' or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ' or ' + @strShareRedordWith +')'                                                     
end                                          
                                      
else if @ListType=1 -- when Team Selected radiobutton is checked
begin                                 
if @TeamMemID=0 -- When All team members is selected in dropdown
 begin                                                  
 if @numGrpID<>2 set @strSql=@strSql + '  AND (DM.numRecOwner  in(select numContactID from  AdditionalContactsInformation                                      
		where numTeam in(select numTeam from ForReportsByTeam where numUserCntID='+convert(varchar(15),@numUserCntID)+' and tintType='+convert(varchar(15),@TeamType)+') ) or DM.numAssignedTo in(select numContactID from  AdditionalContactsInformation             
		where numTeam in(select numTeam from ForReportsByTeam where numUserCntID='+convert(varchar(15),@numUserCntID)+' and tintType='+convert(varchar(15),@TeamType)+'))' + ' or ' + @strShareRedordWith +')'                                                                       
                                                   
 end                                                  
else-- When All team member name is selected in dropdown
 begin                                                  
 if @numGrpID<>2 set @strSql=@strSql + '  AND ( DM.numRecOwner='+convert(varchar(15),@TeamMemID) +'  or  DM.numAssignedTo='+convert(varchar(15),@TeamMemID) + ' or ' + @strShareRedordWith +')'                                                         
                                                   
 end                                                   
end

IF @vcRegularSearchCriteria<>'' 
BEGIN
	IF PATINDEX('%numShipCountry%', @vcRegularSearchCriteria)>0
	BEGIN
		set @strSql=@strSql+' and DM.numDivisionID in (select distinct numRecordID from #tempAddressLead AD where tintAddressOf=2 and tintAddressType=2 AND bitIsPrimary=1 AND  ' + replace(@vcRegularSearchCriteria,'numShipCountry','numCountry') + ')'
	END
	ELSE IF PATINDEX('%numBillCountry%', @vcRegularSearchCriteria)>0
	BEGIN
		set @strSql=@strSql+' and DM.numDivisionID in (select distinct numRecordID from #tempAddressLead AD where tintAddressOf=2 and tintAddressType=1 AND bitIsPrimary=1 AND ' + replace(@vcRegularSearchCriteria,'numBillCountry','numCountry') + ')'
	END
	ELSE IF PATINDEX('%numShipState%', @vcRegularSearchCriteria)>0
	BEGIN
		set @strSql=@strSql+' and DM.numDivisionID in (select distinct numRecordID from #tempAddressLead AD where tintAddressOf=2 and tintAddressType=2 AND bitIsPrimary=1 AND ' + replace(@vcRegularSearchCriteria,'numShipState','numState') + ')'
	END
	ELSE IF PATINDEX('%numBillState%', @vcRegularSearchCriteria)>0
	BEGIN
		set @strSql=@strSql+' and DM.numDivisionID in (select distinct numRecordID from #tempAddressLead AD where tintAddressOf=2 and tintAddressType=1 AND bitIsPrimary=1 AND ' + replace(@vcRegularSearchCriteria,'numBillState','numState') + ')'
	END
	ELSE
		set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
END
 
IF @vcCustomSearchCriteria<>'' set @strSql=@strSql+' and DM.numDivisionID in (select distinct CFW.RecId from CFW_FLD_Values CFW where ' + @vcCustomSearchCriteria + ')'

  set @strSql=@strSql+')'
--set @strSql=@strSql+'), AddUnreadMailCount AS 
--             ( SELECT ISNULL(VIE.Total,0) AS TotalEmail,t.* FROM tblLeads t left JOIN View_InboxEmail VIE  WITH (NOEXPAND) ON VIE.numDomainID = @numDomain AND  VIE.numContactId = t.numContactId
--                            )
--             , AddActionCount AS
--             ( SELECT    ISNULL(VOA.OpenActionItemCount,0) AS TotalActionItem,U.* FROM AddUnreadMailCount U LEFT JOIN  VIEW_OPEN_ACTION_ITEMS VOA WITH (NOEXPAND)  ON VOA.numDomainID = @numDomain AND  VOA.numDivisionId = U.numDivisionID 
--                            '                                  
                                                                           
declare @tintOrder as tinyint                                                
declare @vcFieldName as varchar(50)                                                
declare @vcListItemType as varchar(3)                                           
declare @vcAssociatedControlType varchar(20)                                                
declare @numListID AS numeric(9)                                                
declare @vcDbColumnName varchar(20)                    
declare @WhereCondition varchar(2000)                     
declare @vcLookBackTableName varchar(2000)              
Declare @bitCustomField as BIT
DECLARE @bitAllowEdit AS CHAR(1)                 
Declare @numFieldId as numeric  
DECLARE @bitAllowSorting AS CHAR(1)   
declare @vcColumnName AS VARCHAR(500)                  
set @tintOrder=0                                                
set @WhereCondition =''               
                 
declare @Nocolumns as tinyint              
--declare @DefaultNocolumns as tinyint         
set @Nocolumns=0              
 
select @Nocolumns=isnull(sum(TotalRow),0)  from(            
Select count(*) TotalRow from View_DynamicColumns where numFormId=34 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=1 
Union 
Select count(*) TotalRow from View_DynamicCustomColumns where numFormId=34 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=1) TotalRows
 

--Set @DefaultNocolumns=  @Nocolumns
  

CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
)

set @strSql=@strSql+' select ADC.numContactId,DM.numDivisionID,DM.numTerID,ADC.numRecOwner,DM.tintCRMType,RowNumber'      


--Check if Master Form Configuration is created by administrator if NoColumns=0
DECLARE @IsMasterConfAvailable BIT = 0
DECLARE @numUserGroup NUMERIC(18,0)

IF @Nocolumns=0
BEGIN
	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 34 AND numRelCntType=1 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END
END


--If MasterConfiguration is available then load it otherwise load default columns
IF @IsMasterConfAvailable = 1
BEGIN
	INSERT INTO #tempForm
	SELECT 
		(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
		vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
		bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
		ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
	FROM 
		View_DynamicColumnsMasterConfig
	WHERE
		View_DynamicColumnsMasterConfig.numFormId=34 AND 
		View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicColumnsMasterConfig.numRelCntType=1 AND 
		View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
		ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	UNION
    SELECT 
		tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
		bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,0,''
	FROM 
		View_DynamicCustomColumnsMasterConfig
	WHERE 
		View_DynamicCustomColumnsMasterConfig.numFormId=34 AND 
		View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicCustomColumnsMasterConfig.numRelCntType=1 AND 
		View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
	ORDER BY 
		tintOrder ASC  
END
ELSE                                        
BEGIN
	if @Nocolumns=0
	BEGIN
					INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
select 34,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,1,1,0,intColumnWidth
 FROM View_DynamicDefaultColumns
 where numFormId=34 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID
order by tintOrder asc   

	END

	INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
 FROM View_DynamicColumns 
 where numFormId=34 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=1
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
       
 UNION
    
    select tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,0,''
 from View_DynamicCustomColumns
 where numFormId=34 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=1
 AND ISNULL(bitCustom,0)=1
 
  order by tintOrder asc  
END        
Declare @ListRelID as numeric(9)  
  select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustomField=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
@ListRelID=ListRelID
  from  #tempForm order by tintOrder asc            
 


while @tintOrder>0                                                
begin                                                
     
     if @bitCustomField = 0            
 begin             
     declare @Prefix as varchar(5)                  
     if @vcLookBackTableName = 'AdditionalContactsInformation'                  
   set @Prefix = 'ADC.'                  
     if @vcLookBackTableName = 'DivisionMaster'                  
   set @Prefix = 'DM.'       
   
          SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'
                                         
 if @vcAssociatedControlType='SelectBox' or @vcAssociatedControlType='ListBox'                                                    
        begin                                                    
          
      
     if @vcListItemType='LI'                                                     
     begin                                                    
		 IF @numListID=40 
		 BEGIN
			
				SET @strSql = @strSql + ',L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'                                                   
                                            
				IF @vcDbColumnName='numShipCountry'
				BEGIN
					SET @WhereCondition = @WhereCondition
										+ ' left Join #tempAddressLead AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= DM.numDomainID '
										+ ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=AD2.numCountry '
				END
				ELSE
				BEGIN
					SET @WhereCondition = @WhereCondition
									+ ' left Join #tempAddressLead AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= DM.numDomainID '
									+ ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=AD1.numCountry '
				END


		 END
		 ELSE
		 BEGIN
			set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                   
			set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName		 	
		 END
     end                                                    
     else if @vcListItemType='S'                                                     
     begin        
			SET @strSql = @strSql + ',S' + CONVERT(VARCHAR(3), @tintOrder) + '.vcState' + ' [' + @vcColumnName + ']'                                                            
											
											IF @vcDbColumnName='numShipState'
											BEGIN
												SET @WhereCondition = @WhereCondition
													+ ' left Join #tempAddressLead AD3 on AD3.numRecordId= DM.numDivisionID and AD3.tintAddressOf=2 and AD3.tintAddressType=2 AND AD3.bitIsPrimary=1 and AD3.numDomainID= DM.numDomainID '
													+ ' left Join State S' + CONVERT(VARCHAR(3), @tintOrder) + ' on S' + CONVERT(VARCHAR(3), @tintOrder) + '.numStateID=AD3.numState '
											END
											ELSE IF @vcDbColumnName='numBillState'
											BEGIN
												SET @WhereCondition = @WhereCondition
													+ ' left Join #tempAddressLead AD4 on AD4.numRecordId= DM.numDivisionID and AD4.tintAddressOf=2 and AD4.tintAddressType=1 AND AD4.bitIsPrimary=1 and AD4.numDomainID= DM.numDomainID '
													+ ' left Join State S' + CONVERT(VARCHAR(3), @tintOrder) + ' on S' + CONVERT(VARCHAR(3), @tintOrder) + '.numStateID=AD4.numState '
											END
											ELSE  
											BEGIN
												SET @WhereCondition = @WhereCondition + ' left join State S' + CONVERT(VARCHAR(3), @tintOrder) + ' on S' + CONVERT(VARCHAR(3), @tintOrder) + '.numStateID=' + @vcDbColumnName        
											END		                                            
     end                                                    
     else if @vcListItemType='T'                                                     
     begin                                                    
      set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                    
      set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                    
     end   
      else if @vcListItemType='C'                                                     
     begin                                                    
      set @strSql=@strSql+',C'+ convert(varchar(3),@tintOrder)+'.vcCampaignName'+' ['+ @vcColumnName+']'                                                    
      set @WhereCondition= @WhereCondition +' left Join CampaignMaster C'+ convert(varchar(3),@tintOrder)+ ' on C'+convert(varchar(3),@tintOrder)+ '.numCampaignId=DM.'+@vcDbColumnName                                                    
     end  
      else if @vcListItemType='AG'                                                     
     begin                                                    
      set @strSql=@strSql+',AG'+ convert(varchar(3),@tintOrder)+'.vcGrpName'+' ['+ @vcColumnName+']'                                                    
      set @WhereCondition= @WhereCondition +' left Join Groups AG'+ convert(varchar(3),@tintOrder)+ ' on AG'+convert(varchar(3),@tintOrder)+ '.numGrpId=DM.'+@vcDbColumnName                                                    
     end   
      else if @vcListItemType='DC'                                                     
     begin                                                    
      set @strSql=@strSql+',DC'+ convert(varchar(3),@tintOrder)+'.vcECampName'+' ['+ @vcColumnName+']'                                                    
      set @WhereCondition= @WhereCondition +' left Join ECampaign DC'+ convert(varchar(3),@tintOrder)+ ' on DC'+convert(varchar(3),@tintOrder)+ '.numECampaignID=ADC.'+@vcDbColumnName                                                    
     end                   
     else if   @vcListItemType='U'                                                 
    begin                   
    set @strSql=@strSql+',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'                                                    
    end                  
    end                                                    
      else if @vcAssociatedControlType='DateField'                                                  
 begin            
       
     set @strSql=@strSql+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
     set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
     set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ' 
     set @strSql=@strSql+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'
   end    
          
else if @vcAssociatedControlType='TextBox' OR @vcAssociatedControlType='Label'                                                 
begin    
 set @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when    
  @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' else @vcDbColumnName end+' ['+ @vcColumnName+']'              
       
 end 
 else if @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
		begin      
			 set @strSql=@strSql+',(SELECT SUBSTRING(
	(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=DM.numDomainID AND SR.numModuleID=1 AND SR.numRecordID=DM.numDivisionID
				AND UM.numDomainID=DM.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
		 end       
 else                                               
begin    
	if @vcDbColumnName='vcCampaignAudit'
		set @strSql=@strSql+', dbo.GetContactCampignAuditDetail(ADC.numECampaignID,ADC.numContactId) ['+ @vcColumnName+']'  
	else
		set @strSql=@strSql+','+ @vcDbColumnName +' ['+ @vcColumnName+']'
 end          
                
end            
else if @bitCustomField = 1            
begin            
               
     

   SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'
            
   print @vcAssociatedControlType   
   print @numFieldId           
   if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'            
   begin            
               
    set @strSql= @strSql+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'               
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId   '                                                     
   end  
   else if @vcAssociatedControlType = 'CheckBox'         
   begin            
               
    --set @strSql= @strSql+',case when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=0 then ''No'' when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=1 then ''Yes'' end   ['+ @vcColumnName +']'              
    set @strSql= @strSql+',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0)='''' then 0 else  isnull(CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0) end ['+ @vcColumnName + ']'
 
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId   '                                                     
   end            
   else if @vcAssociatedControlType = 'DateField'        
   begin            
               
    set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'              
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId   '                                                     
   end            
   else if @vcAssociatedControlType = 'SelectBox'           
   begin            
    set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)            
    set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                     
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
     on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId    '                                                     
    set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'            
   end   
               
end      
            
    
  select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustomField=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc            
 
  if @@rowcount=0 set @tintOrder=0 
        
    
          
end                     
 
-------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

--select top 1 @vcCSOrigDbCOlumnName=DFM.vcOrigDbCOlumnName,@vcCSLookBackTableName=DFM.vcLookBackTableName from DycFieldColorScheme DFCS join DycFieldMaster DFM on 
--DFCS.numModuleID=DFM.numModuleID and DFCS.numFieldID=DFM.numFieldID
--where DFCS.numDomainID=@numDomainID and DFM.numModuleID=1

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=34 AND DFCS.numFormID=34 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
----------------------------                       

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
set @strSql=@strSql+',tCS.vcColorScheme'
END

  set @strSql=@strSql+' ,TotalRowCount
 ,ISNULL(ADC.numECampaignID,0) as numECampaignID
 ,ISNULL( (SELECT MAX([numConEmailCampID]) FROM [ConECampaign] WHERE [numContactID] = ADC.numContactId AND [numECampaignID]= ADC.numECampaignID AND ISNULL(bitEngaged,0)=1),0) numConEmailCampID
 ,isnull((SELECT ISNULL(VIE.Total,0) FROM View_InboxEmail VIE  WITH (NOEXPAND) where VIE.numDomainID = @numDomain AND  VIE.numContactId = ADC.numContactId),0) as TotalEmail,
 isnull((SELECT ISNULL(VOA.OpenActionItemCount,0) AS TotalActionItem FROM VIEW_OPEN_ACTION_ITEMS VOA WITH (NOEXPAND)  where VOA.numDomainID = @numDomain AND  VOA.numDivisionId = T.numDivisionID ),0) as TotalActionItem
 FROM CompanyInfo cmp join divisionmaster DM on cmp.numCompanyID=DM.numCompanyID                                                        
   left join ListDetails lst on lst.numListItemID=DM.numFollowUpStatus                                                                               
   join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID                                                
   join tblLeads T on T.numDivisionID=DM.numDivisionID   '+@WhereCondition

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
	 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
		set @Prefix = 'ADC.'                  
     if @vcCSLookBackTableName = 'DivisionMaster'                  
		set @Prefix = 'DM.'
	 if @vcCSLookBackTableName = 'CompanyInfo'                  
		set @Prefix = 'CMP.'   
 
	IF @vcCSAssociatedControlType='DateField'
	BEGIN
		--set @strSql=@strSql+' left join #tempColorScheme tCS on convert(varchar(11),DateAdd(day,tCS.vcFieldValue,GETUTCDATE()))=' + @Prefix + 'convert(varchar(11),DateAdd(day,tCS.vcFieldValue,' + @vcCSOrigDbCOlumnName + '))'
					set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111) 
			 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'

	END
	ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
	BEGIN
		set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
	END
END

 set @strSql=@strSql+' WHERE                                               
   DM.tintCRMType=0 and ISNULL(ADC.bitPrimaryContact,0)=1 AND CMP.numDomainID=DM.numDomainID  and DM.numDomainID=ADC.numDomainID                                                  
   and RowNumber > '+convert(varchar(10),@firstRec)+ ' and RowNumber <'+ convert(varchar(10),@lastRec)+ ' order by RowNumber'
                                             
print @strSql                      
              
exec (@strSql)  

SELECT * FROM #tempForm

DROP TABLE #tempForm

drop table #tempColorScheme

IF OBJECT_ID('tempdb..#tempAddressLead') IS NOT NULL
BEGIN
    DROP TABLE #tempAddressLead
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetReportModuleGroupFieldMaster')
DROP PROCEDURE USP_GetReportModuleGroupFieldMaster
GO
CREATE PROCEDURE [dbo].[USP_GetReportModuleGroupFieldMaster]     
    @numDomainID numeric(18, 0),
	@numReportID numeric(18, 0)
as                 

--Get numReportModuleGroupID
DECLARE @numReportModuleGroupID AS NUMERIC(18,0);SET @numReportModuleGroupID=0
SELECT @numReportModuleGroupID=numReportModuleGroupID FROM dbo.ReportListMaster WHERE numDomainID=@numDomainID AND numReportID=@numReportID
PRINT @numReportModuleGroupID

IF @numReportModuleGroupID>0
BEGIN
CREATE TABLE #tempField(numReportFieldGroupID NUMERIC,vcFieldGroupName VARCHAR(100),numFieldID NUMERIC,
vcFieldName NVARCHAR(50),vcDbColumnName NVARCHAR(50),vcOrigDbColumnName NVARCHAR(50),vcFieldDataType CHAR(1),
vcAssociatedControlType NVARCHAR(50),vcListItemType VARCHAR(3),numListID NUMERIC,bitCustom BIT,
bitAllowSorting BIT,bitAllowGrouping BIT,bitAllowAggregate BIT,bitAllowFiltering BIT,vcLookBackTableName NVARCHAR(50))

--Regular Fields
INSERT INTO #tempField
	SELECT RFGM.numReportFieldGroupID,RFGM.vcFieldGroupName,RGMM.numFieldID,RGMM.vcFieldName,
		   DFM.vcDbColumnName,DFM.vcOrigDbColumnName,RGMM.vcFieldDataType,RGMM.vcAssociatedControlType,
		   ISNULL(DFM.vcListItemType,''),ISNULL(DFM.numListID,0),CAST(0 AS BIT) AS bitCustom,RGMM.bitAllowSorting,RGMM.bitAllowGrouping,
		   RGMM.bitAllowAggregate,RGMM.bitAllowFiltering,DFM.vcLookBackTableName
		FROM ReportFieldGroupMaster RFGM 
			JOIN ReportModuleGroupFieldMappingMaster RFMM ON RFGM.numReportFieldGroupID=RFMM.numReportFieldGroupID
			JOIN ReportFieldGroupMappingMaster RGMM ON RGMM.numReportFieldGroupID=RFGM.numReportFieldGroupID
			JOIN dbo.DycFieldMaster DFM ON DFM.numFieldId=RGMM.numFieldID
		WHERE RFMM.numReportModuleGroupID=@numReportModuleGroupID AND RFGM.bitActive=1
			AND ISNULL(RFGM.bitCustomFieldGroup,0)=0

IF @numReportModuleGroupID=9 OR @numReportModuleGroupID=24
BEGIN
INSERT INTO #tempField
SELECT 25 AS numReportFieldGroupID,'BizDocs Items Tax Field' AS vcFieldGroupName,TI.numTaxItemID,TI.vcTaxName,
		   vcTaxName AS vcDbColumnName,vcTaxName AS vcOrigDbColumnName,'M' AS vcFieldDataType,'TextBox' AS vcAssociatedControlType,
		   '' AS vcListItemType,0 AS numListID,CAST(0 AS BIT) AS bitCustom,0 AS bitAllowSorting,0 AS bitAllowGrouping,
		   1 AS bitAllowAggregate,1 AS bitAllowFiltering,'TaxItems' AS vcLookBackTableName
from TaxItems TI 
where TI.numDomainID =@numDomainID
UNION
SELECT 25 AS numReportFieldGroupID,'BizDocs Items Tax Field' AS vcFieldGroupName,0 AS numTaxItemID,'Sales Tax' AS vcTaxName,
		   'Sales Tax' AS vcDbColumnName,'Sales Tax' AS vcOrigDbColumnName,'M' AS vcFieldDataType,'TextBox' AS vcAssociatedControlType,
		   '' AS vcListItemType,0 AS numListID,CAST(0 AS BIT) AS bitCustom,0 AS bitAllowSorting,0 AS bitAllowGrouping,
		   1 AS bitAllowAggregate,1 AS bitAllowFiltering,'TaxItems' AS vcLookBackTableName
END

IF @numReportModuleGroupID=8 
BEGIN
INSERT INTO #tempField
SELECT 26 AS numReportFieldGroupID,'BizDocs Tax Field' AS vcFieldGroupName,TI.numTaxItemID,'Total ' + TI.vcTaxName,
		   'Total ' + vcTaxName AS vcDbColumnName,'Total ' +vcTaxName AS vcOrigDbColumnName,'M' AS vcFieldDataType,'TextBox' AS vcAssociatedControlType,
		   '' AS vcListItemType,0 AS numListID,CAST(0 AS BIT) AS bitCustom,0 AS bitAllowSorting,0 AS bitAllowGrouping,
		   1 AS bitAllowAggregate,1 AS bitAllowFiltering,'TaxItems' AS vcLookBackTableName
from TaxItems TI 
where TI.numDomainID =@numDomainID
UNION
SELECT 26 AS numReportFieldGroupID,'BizDocs Tax Field' AS vcFieldGroupName,0 AS numTaxItemID,'Total Sales Tax' AS vcTaxName,
		   'Total Sales Tax' AS vcDbColumnName,'Total Sales Tax' AS vcOrigDbColumnName,'M' AS vcFieldDataType,'TextBox' AS vcAssociatedControlType,
		   '' AS vcListItemType,0 AS numListID,CAST(0 AS BIT) AS bitCustom,0 AS bitAllowSorting,0 AS bitAllowGrouping,
		   1 AS bitAllowAggregate,1 AS bitAllowFiltering,'TaxItems' AS vcLookBackTableName
--UNION
--SELECT RFGM.numReportFieldGroupID,RFGM.vcFieldGroupName,ISNULL(CFM.Fld_Id,0) as numFieldID,CFM.Fld_label as vcFieldName,
--	   '' as vcDbColumnName,'' AS vcOrigDbColumnName,CASE CFM.Fld_type WHEN 'SelectBox' THEN 'V' WHEN 'DateField' THEN 'D' ELSE 'V' END AS vcFieldDataType,
--	   CFM.Fld_type AS vcAssociatedControlType,CASE WHEN CFM.Fld_type='SelectBox' THEN 'LI' ELSE '' END AS vcListItemType,isnull(CFM.numListID,0) as numListID,
--	   CAST(1 AS BIT) AS bitCustom,1 AS bitAllowSorting,CASE WHEN CFM.Fld_type='SelectBox' THEN 1 ELSE 0 END AS bitAllowGrouping,
--	   0 AS bitAllowAggregate,1 AS bitAllowFiltering,RFGM.vcCustomTableName AS vcLookBackTableName
--FROM ReportFieldGroupMaster RFGM 
--	 JOIN ReportModuleGroupFieldMappingMaster RFMM ON RFGM.numReportFieldGroupID=RFMM.numReportFieldGroupID
--	 JOIN CFW_Fld_Master CFM ON CFM.Grp_id=RFGM.numGroupID 
--	 LEFT JOIN CFw_Grp_Master CGM ON CFM.subgrp=CGM.Grp_id 
--	 --JOIN CFW_Loc_Master L on CFM.GRP_ID=L.Loc_Id
--	 WHERE RFMM.numReportModuleGroupID IN (18,19) AND RFGM.bitActive=1
--			AND ISNULL(RFGM.bitCustomFieldGroup,0)=1 AND ISNULL(RFGM.numGroupID,0)>0
--			AND CFM.numDomainID=@numDomainID  
END

IF @numReportModuleGroupID=9 OR @numReportModuleGroupID=24
BEGIN
INSERT INTO #tempField
SELECT 26 AS numReportFieldGroupID,'BizDocs Tax Field' AS vcFieldGroupName,TI.numTaxItemID,'Total ' + TI.vcTaxName,
		   'Total ' + vcTaxName AS vcDbColumnName,'Total ' +vcTaxName AS vcOrigDbColumnName,'M' AS vcFieldDataType,'TextBox' AS vcAssociatedControlType,
		   '' AS vcListItemType,0 AS numListID,CAST(0 AS BIT) AS bitCustom,0 AS bitAllowSorting,0 AS bitAllowGrouping,
		   1 AS bitAllowAggregate,1 AS bitAllowFiltering,'TaxItems' AS vcLookBackTableName
from TaxItems TI 
where TI.numDomainID =@numDomainID
UNION
SELECT 26 AS numReportFieldGroupID,'BizDocs Tax Field' AS vcFieldGroupName,0 AS numTaxItemID,'Total Sales Tax' AS vcTaxName,
		   'Total Sales Tax' AS vcDbColumnName,'Total Sales Tax' AS vcOrigDbColumnName,'M' AS vcFieldDataType,'TextBox' AS vcAssociatedControlType,
		   '' AS vcListItemType,0 AS numListID,CAST(0 AS BIT) AS bitCustom,0 AS bitAllowSorting,0 AS bitAllowGrouping,
		   1 AS bitAllowAggregate,1 AS bitAllowFiltering,'TaxItems' AS vcLookBackTableName
UNION
SELECT RFGM.numReportFieldGroupID,RFGM.vcFieldGroupName,ISNULL(CFM.Fld_Id,0) as numFieldID,CFM.Fld_label as vcFieldName,
	   '' as vcDbColumnName,'' AS vcOrigDbColumnName,CASE CFM.Fld_type WHEN 'SelectBox' THEN 'V' WHEN 'DateField' THEN 'D' ELSE 'V' END AS vcFieldDataType,
	   CFM.Fld_type AS vcAssociatedControlType,CASE WHEN CFM.Fld_type='SelectBox' THEN 'LI' ELSE '' END AS vcListItemType,isnull(CFM.numListID,0) as numListID,
	   CAST(1 AS BIT) AS bitCustom,1 AS bitAllowSorting,CASE WHEN CFM.Fld_type='SelectBox' THEN 1 ELSE 0 END AS bitAllowGrouping,
	   0 AS bitAllowAggregate,1 AS bitAllowFiltering,RFGM.vcCustomTableName AS vcLookBackTableName
FROM ReportFieldGroupMaster RFGM 
	 JOIN ReportModuleGroupFieldMappingMaster RFMM ON RFGM.numReportFieldGroupID=RFMM.numReportFieldGroupID
	 JOIN CFW_Fld_Master CFM ON CFM.Grp_id=RFGM.numGroupID 
	 LEFT JOIN CFw_Grp_Master CGM ON CFM.subgrp=CGM.Grp_id 
	 --JOIN CFW_Loc_Master L on CFM.GRP_ID=L.Loc_Id
	 WHERE RFMM.numReportModuleGroupID IN (18,19) AND RFGM.bitActive=1
			AND ISNULL(RFGM.bitCustomFieldGroup,0)=1 AND ISNULL(RFGM.numGroupID,0)>0
			AND CFM.numDomainID=@numDomainID  
END
IF @numReportModuleGroupID=19 OR @numReportModuleGroupID=23 --OR @numReportModuleGroupID=24
BEGIN
INSERT INTO #tempField
SELECT RFGM.numReportFieldGroupID,RFGM.vcFieldGroupName,ISNULL(CFM.Fld_Id,0) as numFieldID,CFM.Fld_label as vcFieldName,
	   '' as vcDbColumnName,'' AS vcOrigDbColumnName,CASE CFM.Fld_type WHEN 'SelectBox' THEN 'V' WHEN 'DateField' THEN 'D' ELSE 'V' END AS vcFieldDataType,
	   CFM.Fld_type AS vcAssociatedControlType,CASE WHEN CFM.Fld_type='SelectBox' THEN 'LI' ELSE '' END AS vcListItemType,isnull(CFM.numListID,0) as numListID,
	   CAST(1 AS BIT) AS bitCustom,1 AS bitAllowSorting,CASE WHEN CFM.Fld_type='SelectBox' THEN 1 ELSE 0 END AS bitAllowGrouping,
	   0 AS bitAllowAggregate,1 AS bitAllowFiltering,RFGM.vcCustomTableName AS vcLookBackTableName
FROM ReportFieldGroupMaster RFGM 
	 JOIN ReportModuleGroupFieldMappingMaster RFMM ON RFGM.numReportFieldGroupID=RFMM.numReportFieldGroupID
	 JOIN CFW_Fld_Master CFM ON CFM.Grp_id=RFGM.numGroupID 
	 LEFT JOIN CFw_Grp_Master CGM ON CFM.subgrp=CGM.Grp_id 
	 --JOIN CFW_Loc_Master L on CFM.GRP_ID=L.Loc_Id
	 WHERE RFMM.numReportModuleGroupID IN (18,19) AND RFGM.bitActive=1
			AND ISNULL(RFGM.bitCustomFieldGroup,0)=1 AND ISNULL(RFGM.numGroupID,0)>0
			AND CFM.numDomainID=@numDomainID  
END

--Custom Fields			
IF @numReportModuleGroupID <> 23 AND @numReportModuleGroupID <> 19  AND @numReportModuleGroupID <> 24
BEGIN
INSERT INTO #tempField
SELECT RFGM.numReportFieldGroupID,RFGM.vcFieldGroupName,ISNULL(CFM.Fld_Id,0) as numFieldID,CFM.Fld_label as vcFieldName,
	   '' as vcDbColumnName,'' AS vcOrigDbColumnName,CASE CFM.Fld_type WHEN 'SelectBox' THEN 'V' WHEN 'DateField' THEN 'D' ELSE 'V' END AS vcFieldDataType,
	   CFM.Fld_type AS vcAssociatedControlType,CASE WHEN CFM.Fld_type='SelectBox' THEN 'LI' ELSE '' END AS vcListItemType,isnull(CFM.numListID,0) as numListID,
	   CAST(1 AS BIT) AS bitCustom,1 AS bitAllowSorting,CASE WHEN CFM.Fld_type='SelectBox' THEN 1 ELSE 0 END AS bitAllowGrouping,
	   0 AS bitAllowAggregate,1 AS bitAllowFiltering,RFGM.vcCustomTableName AS vcLookBackTableName
FROM ReportFieldGroupMaster RFGM 
	 JOIN ReportModuleGroupFieldMappingMaster RFMM ON RFGM.numReportFieldGroupID=RFMM.numReportFieldGroupID
	 JOIN CFW_Fld_Master CFM ON CFM.Grp_id=RFGM.numGroupID 
	 LEFT JOIN CFw_Grp_Master CGM ON CFM.subgrp=CGM.Grp_id 
	 --JOIN CFW_Loc_Master L on CFM.GRP_ID=L.Loc_Id
	 WHERE RFMM.numReportModuleGroupID=@numReportModuleGroupID AND RFGM.bitActive=1
			AND ISNULL(RFGM.bitCustomFieldGroup,0)=1 AND ISNULL(RFGM.numGroupID,0)>0
			AND CFM.numDomainID=@numDomainID  

END 

SELECT * FROM #tempField ORDER BY numReportFieldGroupID

DROP TABLE #tempField
		
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSimilarItem')
DROP PROCEDURE USP_GetSimilarItem
GO
CREATE PROCEDURE USP_GetSimilarItem
@numDomainID NUMERIC(9),
@numParentItemCode NUMERIC(9),
@byteMode TINYINT
AS 
BEGIN

If @byteMode=1
BEGIN
 select count(*) as Total from SimilarItems SI INNER JOIN Item I ON I.numItemCode = SI.numItemCode 
 WHERE SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numParentItemCode
END
ELSE IF @byteMode=3
BEGIN
SELECT I.vcItemName AS vcItemName,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1) As vcPathForTImage ,I.txtItemDesc AS txtItemDesc,  SI.* ,Category.vcCategoryName,
ISNULL(CASE WHEN I.[charItemType]='P' THEN CASE WHEN I.bitSerialized = 1 THEN 1.00000 * monListPrice ELSE 1.00000 * W.[monWListPrice] END ELSE 1.00000 * monListPrice END,0) monListPrice,
dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)) as UOMConversionFactor,I.vcSKU,I.vcManufacturer
, ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc],ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell], ISNULL(vcUpSellDesc,'') [vcUpSellDesc]
FROM 
Category INNER JOIN ItemCategory ON Category.numCategoryID = ItemCategory.numCategoryID INNER JOIN
                      Item I ON ItemCategory.numItemID = I.numItemCode INNER JOIN
                      SimilarItems SI ON I.numItemCode = SI.numItemCode LEFT  JOIN
                       WareHouseItems W ON I.numItemCode = W.numItemID
 --FROM SimilarItems SI INNER JOIN Item I ON I.numItemCode = SI.numItemCode  INNER JOIN dbo.Category cat ON cat.numCategoryID = I.num
 WHERE SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numParentItemCode
END
ELSE IF @byteMode=2
BEGIN
SELECT I.vcItemName AS vcItemName,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1) As vcPathForTImage ,I.txtItemDesc AS txtItemDesc,  SI.* ,I.monListPrice
, ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc],ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell], ISNULL(vcUpSellDesc,'') [vcUpSellDesc]
FROM SimilarItems SI INNER JOIN Item I ON I.numItemCode = SI.numItemCode
WHERE SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numParentItemCode
END 
END


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSitesDetail')
DROP PROCEDURE USP_GetSitesDetail
GO
CREATE PROCEDURE [dbo].[USP_GetSitesDetail] @numSiteID NUMERIC(9)
AS 
    SELECT  S.[numDomainID],
            D.[numDivisionID],
            ISNULL(S.[bitIsActive], 1),
            ISNULL(E.[numDefaultWareHouseID],0) numDefaultWareHouseID,
            ISNULL(E.[numRelationshipId],0) numRelationshipId,
            ISNULL(E.[numProfileId],0) numProfileId,
            ISNULL(E.[bitEnableDefaultAccounts],0) bitEnableDefaultAccounts,
            ISNULL(E.[numDefaultCOGSChartAcntId],0) numDefaultCOGSChartAcntId,
            ISNULL(E.[numDefaultIncomeChartAcntId],0) numDefaultIncomeChartAcntId,
            ISNULL(E.[numDefaultAssetChartAcntId],0) numDefaultAssetChartAcntId,
            --ISNULL(E.[bitEnableCreditCart],0) bitEnableCreditCart,
            ISNULL(E.[bitHidePriceBeforeLogin],0) AS bitHidePriceBeforeLogin,
			ISNULL(E.[bitHidePriceAfterLogin],0) AS bitHidePriceAfterLogin,
			ISNULL(E.[numRelationshipIdHidePrice],0) AS numRelationshipIdHidePrice,
			ISNULL(E.[numProfileIDHidePrice],0) AS numProfileIDHidePrice,
			dbo.fn_GetContactEmail(1,0,numAdminID) AS DomainAdminEmail,
			S.vcLiveURL,
			ISNULL(E.bitAuthOnlyCreditCard,0) bitAuthOnlyCreditCard,
			ISNULL(D.numDefCountry,0) numDefCountry,
			ISNULL(E.bitSendEMail,0) bitSendMail,
			ISNULL((SELECT numAuthoritativeSales FROM dbo.AuthoritativeBizDocs WHERE numDomainId=S.numDomainID),0) AuthSalesBizDoc,
			ISNULL((SELECT TOP 1 numBizDocId FROM dbo.eCommercePaymentConfig WHERE numSiteID=@numSiteID AND numPaymentMethodId = 27261 AND bitEnable =1 ),0) numCreditTermBizDocID,
			ISNULL((SELECT TOP 1 numBizDocId FROM dbo.eCommercePaymentConfig WHERE numSiteID=@numSiteID AND numPaymentMethodId =1 AND bitEnable =1 ),0) numCreditCardBizDocID,
			ISNULL((SELECT TOP 1 numBizDocId FROM dbo.eCommercePaymentConfig WHERE numSiteID=@numSiteID AND numPaymentMethodId = 31488 AND bitEnable =1 ),0) numGoogleCheckoutBizDocID,
ISNULL((SELECT TOP 1 numBizDocId FROM dbo.eCommercePaymentConfig WHERE numSiteID=@numSiteID AND numPaymentMethodId = 35141 AND bitEnable =1 ),0) numPaypalCheckoutBizDocID,
ISNULL((SELECT TOP 1 numBizDocId FROM dbo.eCommercePaymentConfig WHERE numSiteID=@numSiteID AND numPaymentMethodId = 84 AND bitEnable =1 ),0) numSalesInquiryBizDocID,
			ISNULL(D.bitSaveCreditCardInfo,0) bitSaveCreditCardInfo,
			ISNULL(bitOnePageCheckout,0) bitOnePageCheckout,
			ISNULL(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
			ISNULL(D.numShippingServiceItemID,0) numShippingServiceItemID,
			ISNULL(D.bitAutolinkUnappliedPayment,0) bitAutolinkUnappliedPayment,
			ISNULL(D.numDiscountServiceItemID,0) numDiscountServiceItemID,
			ISNULL(D.tintBaseTax,0) AS [tintBaseTax],
			ISNULL(E.bitSkipStep2,0) AS [bitSkipStep2],
			ISNULL(E.bitDisplayCategory,0) AS [bitDisplayCategory],
			ISNULL(E.bitShowPriceUsingPriceLevel,0) AS [bitShowPriceUsingPriceLevel],
			ISNULL(E.[bitPreSellUp],0) [bitPreSellUp],
			ISNULL(E.[bitPostSellUp],0) [bitPostSellUp],
			ISNULL(E.[dcPostSellDiscount],0) [dcPostSellDiscount]
    FROM    Sites S
            INNER JOIN [Domain] D ON D.[numDomainId] = S.[numDomainID]
            LEFT OUTER JOIN [eCommerceDTL] E ON D.[numDomainId] = E.[numDomainId]
    WHERE   S.[numSiteID] = @numSiteID
 
 

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_InboxItems' ) 
    DROP PROCEDURE USP_InboxItems
GO
Create PROCEDURE [dbo].[USP_InboxItems]                                                                                              
@PageSize [int] ,                            
@CurrentPage [int],                            
@srch as varchar(100) = '',                            
--@vcSubjectFromTo as varchar(100) ='',
--@srchAttachmenttype as varchar(100) ='',                                   
@ToEmail as varchar (100)  ,        
@columnName as varchar(50) ,        
@columnSortOrder as varchar(4)  ,  
@numDomainId as numeric(9),  
@numUserCntId as numeric(9)  ,    
@numNodeId as numeric(9),  
--@chrSource as char(1),
@ClientTimeZoneOffset AS INT=0,
@tintUserRightType AS INT,
@EmailStatus AS NUMERIC(9)=0,
@srchFrom as varchar(100) = '', 
@srchTo as varchar(100) = '', 
@srchSubject as varchar(100) = '', 
@srchHasWords as varchar(100) = '', 
@srchHasAttachment as bit = 0, 
@srchIsAdvancedsrch as bit = 0,
@srchInNode as numeric(9)=0
as                                                      
                                                      
 ---DECLARE @CRMType NUMERIC 
DECLARE @tintOrder AS TINYINT                                                      
DECLARE @vcFieldName AS VARCHAR(50)                                                      
DECLARE @vcListItemType AS VARCHAR(1)                                                 
DECLARE @vcAssociatedControlType VARCHAR(20)                                                      
DECLARE @numListID AS NUMERIC(9)                                                      
DECLARE @vcDbColumnName VARCHAR(20)                          
DECLARE @WhereCondition VARCHAR(2000)                           
DECLARE @vcLookBackTableName VARCHAR(2000)                    
DECLARE @bitCustom AS BIT
DECLARE @bitAllowEdit AS CHAR(1)                 
DECLARE @numFieldId AS NUMERIC  
DECLARE @bitAllowSorting AS CHAR(1)              
DECLARE @vcColumnName AS VARCHAR(500)  

DECLARE @strSql AS VARCHAR(5000)
DECLARE @column AS VARCHAR(50)                             
DECLARE @join AS VARCHAR(400) 
DECLARE @Nocolumns AS TINYINT               
DECLARE @lastRec AS INTEGER 
DECLARE @firstRec AS INTEGER                   
SET @join = ''           
SET @strSql = ''
--DECLARE @ClientTimeZoneOffset AS INT 
--DECLARE @numDomainId AS NUMERIC(18, 0)
SET @tintOrder = 0  
SET @WhereCondition = ''

SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                                              
SET @lastRec = ( @CurrentPage * @PageSize + 1 )   
SET @column = @columnName                
--DECLARE @join AS VARCHAR(400)                    
SET @join = ''             
IF @columnName LIKE 'Cust%' 
    BEGIN                  
        SET @join = ' left Join CFW_FLD_Values_Email CFW on CFW.RecId=DM.numEmailHstrID and CFW.fld_id= '
            + REPLACE(@columnName, 'Cust', '') + ' '                                                           
        SET @column = 'CFW.Fld_Value'
    END                                           
ELSE 
    IF @columnName LIKE 'DCust%' 
        BEGIN                  
            SET @join = ' left Join CFW_FLD_Values_Email CFW on CFW.RecId=DM.numEmailHstrID  and CFW.fld_id= '
                + REPLACE(@columnName, 'DCust', '')                                                                                                     
            SET @join = @join
                + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '                  
            SET @column = 'LstCF.vcData'                    
                  
        END            
    ELSE 
        BEGIN            
            DECLARE @lookbckTable AS VARCHAR(50)                
            SET @lookbckTable = ''                                                         
            SELECT  @lookbckTable = vcLookBackTableName
            FROM    View_DynamicDefaultColumns
            WHERE   numFormId = 44
                    AND vcDbColumnName = @columnName  and numDomainID=@numDOmainID               
            IF @lookbckTable <> '' 
                BEGIN                
                    IF @lookbckTable = 'EmailHistory' 
                        SET @column = 'ADC.' + @column                
                END                                                              
        END              
--DECLARE @strSql AS VARCHAR(5000)   
--SET @numNodeID = 0  
--SET @srchBody = ''
--SET @vcSubjectFromTo =''
--PRINT 'numnodeid' + CONVERT(VARCHAR, @numNodeId)                                 
SET @strSql = '
with FilterRows as (SELECT TOP('+ CONVERT(varchar,@CurrentPage) + ' * '+ CONVERT(varchar,@PageSize) + ') '                
SET @strSql = @strSql + 'ROW_NUMBER() OVER(ORDER BY EH.bintCreatedOn DESC ) AS RowNumber,COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount,EH.numEmailHstrID,isnull(EM.numContactId,0) as numContactId'
SET @strSql = @strSql + ' 
FROM [EmailHistory] EH JOIN EmailMaster EM on EH.numEMailId=EM.numEMailId'
SET @strSql = @strSql + ' 
WHERE EH.numDomainID ='+ CONVERT(VARCHAR,@numDomainId) +' And [numUserCntId] = '+ CONVERT(VARCHAR,@numUserCntId)+' '
--SET @strSql = @strSql + 'Where [numDomainID] ='+ CONVERT(VARCHAR,@numDomainId) +'  And [numUserCntId] = '+ CONVERT(VARCHAR,@numUserCntId)+' AND numNodeID ='+ CONVERT(VARCHAR,@numNodeId) +''
SET @strSql = @strSql + '
And chrSource IN(''B'',''I'') '

--Simple Search for All Node
IF @srchIsAdvancedsrch=0 
BEGIN
if len(@srch)>0
	BEGIN
		SET @strSql = @strSql + '
			AND (EH.vcSubject LIKE ''%'' + '''+ @srch +''' + ''%'' or EH.vcBodyText LIKE ''%'' + '''+ @srch + ''' + ''%''
			or EH.vcFrom LIKE ''%'' + '''+ @srch + ''' + ''%'' or EH.vcTo LIKE ''%'' + '''+ @srch + ''' + ''%'')'
	END
ELSE
	BEGIN --Particular Node
		SET @strSql = @strSql + ' AND [numNodeID] = '+ CONVERT(VARCHAR,@numNodeID) +' '
	END
END
--Advanced Search for All Node or selected Node
Else IF @srchIsAdvancedsrch=1
BEGIN
declare @strCondition as varchar(2000);set @strCondition=''

if len(@srchSubject)>0
	SET @strCondition='EH.vcSubject LIKE ''%'' + '''+ @srchSubject +''' + ''%'''

if len(@srchFrom)>0
	begin
		if len(@strCondition)>0
			SET @strCondition=@strCondition + ' and '

		SET @strCondition = @strCondition + ' EH.vcFrom LIKE ''%'' + '''+ @srchFrom + ''' + ''%'''
	end

if len(@srchTo)>0
	begin
		if len(@strCondition)>0
			SET @strCondition=@strCondition + ' and '

		SET @strCondition = @strCondition + ' EH.vcTo LIKE ''%'' + '''+ @srchTo + ''' + ''%'''
	end

if len(@srchHasWords)>0
	begin
		if len(@strCondition)>0
			SET @strCondition=@strCondition + ' and '

		SET @strCondition = @strCondition + ' EH.vcBodyText LIKE ''%'' + '''+ @srchHasWords + ''' + ''%'''
	end

IF @srchHasAttachment=1
BEGIN
	if len(@strCondition)>0
			SET @strCondition=@strCondition + ' and '
	
	SET @strCondition = @strCondition + ' EH.bitHasAttachments =  '''+ CONVERT(VARCHAR,@srchHasAttachment) + ''''
END

if len(@strCondition)>0
	BEGIN
		SET @strSql = @strSql +' and (' + @strCondition + ')'
		
		if @srchInNode>-1
			SET @strSql = @strSql + ' AND [numNodeID] = '+ CONVERT(VARCHAR,@srchInNode) +' '
	END
ELSE
	BEGIN --Particular Node
		SET @strSql = @strSql + ' AND [numNodeID] = '+ CONVERT(VARCHAR,@numNodeID) +' '
	END

--SET @strSql = @strSql + '
--AND (EH.vcSubject LIKE ''%'' + '''+ @srchSubject +''' + ''%'' and EH.vcFrom LIKE ''%'' + '''+ @srchFrom + ''' + ''%'' 
--and EH.vcTo LIKE ''%'' + '''+ @srchTo + ''' + ''%'' and EH.vcBodyText LIKE ''%'' + '''+ @srchHasWords + ''' + ''%'''
--
--IF @srchHasAttachment=1
--BEGIN
--SET @strSql = @strSql + ' and EH.bitHasAttachments =  '''+ CONVERT(VARCHAR,@srchHasAttachment) + ''''
--END

END


SET @strSql = @strSql + ' )'
--,FinalResult As
--( SELECT  TOP ('+CONVERT(VARCHAR,@PageSize)+ ') numEmailHstrID'
--SET @strSql = @strSql + ' 
--From FilterRows'
--SET @strSql = @strSql + '
--WHERE RowNumber > (('+ CONVERT(VARCHAR,@CurrentPage) + ' - 1) * '+ CONVERT(VARCHAR,@PageSize) +' ))'
--PRINT @strSql
--EXEC(@strSql)

select @Nocolumns=isnull(sum(TotalRow),0)  from(            
Select count(*) TotalRow from View_DynamicColumns where numFormId=44 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0 
Union 
Select count(*) TotalRow from View_DynamicCustomColumns where numFormId=44 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0) TotalRows

  CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType char(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9))


---Insert number of rows into temp table.
PRINT 'number OF columns :' + CONVERT(VARCHAR, @Nocolumns)
IF  @Nocolumns > 0 
    BEGIN    

INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID 
 FROM View_DynamicColumns 
 where numFormId=44 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
                  
    UNION

     select tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID
 from View_DynamicCustomColumns
 where numFormId=44 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0
 AND ISNULL(bitCustom,0)=1
 
 ORDER BY tintOrder ASC  
   
 END 
ELSE 
    BEGIN

 INSERT INTO #tempForm
select tintOrder,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID 
 FROM View_DynamicDefaultColumns
 where numFormId=44 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 AND numDomainID=@numDomainID
order by tintOrder asc   

    END
      SET @strSql =@strSql + ' Select TotalRowCount, EH.numEmailHstrID ,EH.numListItemId,bitIsRead,CASE WHEN LEN(Cast(EH.vcBodyText AS VARCHAR(1000)))>150 THEN SUBSTRING(EH.vcBodyText,0,150) + ''...'' ELSE
		 EH.vcBodyText END as vcBodyText,EH.numEmailHstrID As [KeyId~numEmailHstrID~0~0~0~0~HiddenField],FR.numContactId AS [ContactId~numContactID~0~0~0~0~HiddenField] ,EH.bitIsRead As [IsRead~bitIsRead~0~0~0~0~Image],isnull(EH.IsReplied,0) As [~IsReplied~0~0~0~0~Image] '
 
Declare @ListRelID as numeric(9) 

   select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from  #tempForm order by tintOrder asc            

WHILE @tintOrder > 0                                                      
    BEGIN                                                      
        IF @bitCustom = 0  
            BEGIN            
                DECLARE @Prefix AS VARCHAR(5)                        
                IF @vcLookBackTableName = 'EmailHistory' 
                    SET @Prefix = 'EH.'                        
                SET @vcColumnName = @vcFieldName + '~' + @vcDbColumnName
                    + '~' + @bitAllowSorting + '~'
                    + CONVERT(VARCHAR(10), @numFieldId) + '~'
                    + @bitAllowEdit + '~' + CONVERT(VARCHAR(1), @bitCustom)
                    + '~' + @vcAssociatedControlType
                PRINT @vcColumnName
                
                IF @vcAssociatedControlType = 'SelectBox' 
                    BEGIN      
                        PRINT @vcListItemType                                                    
                        IF @vcListItemType = 'L' 
                            BEGIN    
                            ---PRINT 'columnName' + @vcColumnName
                                SET @strSql = @strSql + ',L'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.vcData' + ' [' + @vcColumnName + ']'                                                         
                                SET @WhereCondition = @WhereCondition
                                    + ' left Join ListDetails L'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + ' on L' + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.numListItemID=EH.numListItemID'                                                           
                            END 
                           
                    END 
                  ELSE IF (@vcAssociatedControlType = 'TextBox'
                        OR @vcAssociatedControlType = 'TextArea'
                        OR @vcAssociatedControlType = 'Image')  AND @vcDbColumnName<>'AlertPanel' AND @vcDbColumnName<>'RecentCorrespondance'
                        BEGIN
							IF @numNodeId=4 and @vcDbColumnName='vcFrom'  
							BEGIN
								  SET @vcColumnName = 'To' + '~' + @vcDbColumnName
                    + '~' + @bitAllowSorting + '~'
                    + CONVERT(VARCHAR(10), @numFieldId) + '~'
                    + @bitAllowEdit + '~' + CONVERT(VARCHAR(1), @bitCustom)
                    + '~' + @vcAssociatedControlType

								 SET @strSql = @strSql + ',' + @Prefix  + 'vcTo' + ' [' + @vcColumnName + ']'   
							END 
							ELSE
							BEGIN
								SET @strSql = @strSql + ',' + @Prefix  + @vcDbColumnName + ' [' + @vcColumnName + ']'   
							END                        
                             PRINT @Prefix   
                        END 
				Else IF  @vcDbColumnName='AlertPanel'   
							BEGIN 
								--SET @strSql = @strSql + ','+ '(SELECT IsNull(vcMsg,'''') FROM dbo.fn_GetEmailAlert(' + CAST(@numDomainId AS VARCHAR(10)) + ',FR.numContactId,'+ CAST(@numUserCntId AS VARCHAR(18))+ '))' + ' AS  [' + '' + @vcColumnName + '' + ']'  
								SET @strSql = @strSql + ',dbo.GetAlertDetail(EH.numDomainID,FR.numContactId,'+ CAST(@numUserCntId AS VARCHAR(10))+',EH.numEmailHstrID) AS  [' + '' + @vcColumnName + '' + ']'                                                                                                            
                             END 
                 ELSE IF  @vcDbColumnName='RecentCorrespondance'
							BEGIN 
					            --SET @strSql = @strSql + ','+ '(SELECT COUNT(*) FROM dbo.Communication WHERE numContactId = FR.numContactId)' + ' AS  [' + '' + @vcColumnName + '' + ']'                                                       
					            SET @strSql = @strSql + ',0 AS  [' + '' + @vcColumnName + '' + ']'
                             END 
--				else if @vcAssociatedControlType='DateField'                                                  
--					begin            
--							set @strSql=@strSql+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
--							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
--							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ' 
--							set @strSql=@strSql+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'
--				    end    
				else if @vcAssociatedControlType='DateField' AND @vcDbColumnName <> 'dtReceivedOn'                                     
					begin           
							set @strSql=@strSql+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ' 
							set @strSql=@strSql+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'
				    end    
				else if @vcAssociatedControlType='DateField' AND @vcDbColumnName = 'dtReceivedOn'                                                 
					begin    
							set @strSql=@strSql+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ' 
							set @strSql=@strSql+'else CONVERT(VARCHAR(20),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+'),100) end  ['+ @vcColumnName+']'
				    end   
			END 
else if @bitCustom = 1                
begin                
            
               SET @vcColumnName= @vcFieldName+'~'+ @vcDbColumnName+'~0~'+ convert(varchar(10),@numFieldId)+'~1~'+ convert(varchar(1),@bitCustom) +'~'+ @vcAssociatedControlType
       
   select @vcFieldName=FLd_label,@vcAssociatedControlType=fld_type,@vcDbColumnName='Cust'+Convert(varchar(10),Fld_Id)                               
    from CFW_Fld_Master                                                
   where    CFW_Fld_Master.Fld_Id = @numFieldId                 
                
                 
    if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'               
   begin                
                   
    set @strSql= @strSql+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=FR.numContactId   '                                                         
   end      
    else if @vcAssociatedControlType = 'CheckBox'                 
   begin              
                 
    set @strSql= @strSql+',case when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=0 then ''No'' when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=1 then ''Yes'' end   ['+ @vcColumnName +']'              
   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '               
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=FR.numContactId   '                                                       
   end              
  else if @vcAssociatedControlType = 'DateField'              
   begin                
                   
    set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=FR.numContactId   '                                                         
   end                
   else if @vcAssociatedControlType = 'SelectBox'                    
   begin                
    set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
    set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
     on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=FR.numContactId   '                                                         
    set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
   end                 
end     
                   --    SET @vcColumnName= @vcFieldName+'~'+ @vcDbColumnName+'~0~'+ convert(varchar(10),@numFieldId)+'~1~'+ convert(varchar(1),@bitCustom) +'~'+ @vcAssociatedControlType   
  select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc            
 
           IF @@rowcount = 0 SET @tintOrder = 0 
		   PRINT @tintOrder 
    END  
                      
SELECT  * FROM    #tempForm

-------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID AND DFCS.numFormID=DFFM.numFormID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=10 AND DFCS.numFormID=10 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
---------------------------- 
 
IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
set @strSql=@strSql+',tCS.vcColorScheme'
END  

DECLARE @fltTotalSize FLOAT
SELECT @fltTotalSize=isnull(SUM(ISNULL(SpaceOccupied,0)),0) FROM dbo.View_InboxCount where numDomainID=@numDomainID and numUserCntID=@numUserCntID;

SET @strSql = @strSql + ', '+ CONVERT(VARCHAR(18),@fltTotalSize) +' AS TotalSize From EmailHistory EH Inner Join FilterRows FR on  FR.numEmailHstrID = EH.numEmailHstrID '
SET @strSql = @strSql + @WhereCondition

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
	 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
		set @Prefix = 'ADC.'                  
     if @vcCSLookBackTableName = 'DivisionMaster'                  
		set @Prefix = 'DM.'
	 if @vcCSLookBackTableName = 'CompanyInfo'                  
		set @Prefix = 'CI.' 
	 if @vcCSLookBackTableName = 'AddressDetails'                  
		set @Prefix = 'AD.'   

set @strSql=@strSql+' left JOIN AdditionalContactsInformation ADC ON ADC.numcontactId=FR.numContactID                                                                             
 left JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                                            
 left JOIN CompanyInfo CI ON DM.numCompanyID = CI.numCompanyId '
   
	IF @vcCSAssociatedControlType='DateField'
	BEGIN
			set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
			 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
	END
	ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
	BEGIN
		set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
	END
END

SET @strSql = @strSql + ' Where RowNumber >'  + CONVERT(VARCHAR(10), @firstRec) + ' and RowNumber <'
        + CONVERT(VARCHAR(10), @lastRec) 
IF @EmailStatus <> 0 
BEGIN
	SET @strSql = @strSql + ' AND EH.numListItemId = ' + CONVERT(VARCHAR(10), @EmailStatus)
END       
SET @strSql = @strSql + ' order by RowNumber'
PRINT  @strSql 
EXEC(@strSql)   
DROP TABLE #tempForm          
                  
 
 /*
 declare @firstRec as integer                                                      
 declare @lastRec as integer                                                      
 set @firstRec= (@CurrentPage-1) * @PageSize                                                      
 set @lastRec= (@CurrentPage*@PageSize+1)   ;    
 
DECLARE @totalRows  INT
DECLARE @fltTotalSize FLOAT
SELECT @totalRows = RecordCount FROM dbo.View_InboxCount where numDomainID=@numDomainID and numUserCntID=@numUserCntID and numNodeId = @numNodeID ;
SELECT @fltTotalSize=SUM(ISNULL(SpaceOccupied,0)) FROM dbo.View_InboxCount where numDomainID=@numDomainID and numUserCntID=@numUserCntID;



-- Step 1: Get Only rows specific to user and domain 

WITH FilterRows
AS 
(
   SELECT 
   TOP (@CurrentPage * @PageSize)
        ROW_NUMBER() OVER(ORDER BY EH.bintCreatedOn DESC ) AS totrows,
        EH.numEmailHstrID
   FROM [EmailHistory] EH
		--LEFT JOIN EmailHstrAttchDtls A ON EH.numEmailHstrID = A.numEmailHstrID
   WHERE 
	   [numDomainID] = @numDomainID
	AND [numUserCntId] = @numUserCntId
	AND numNodeID = @numNodeId
	AND chrSource IN('B','I')
    AND (EH.vcBodyText LIKE '%' + @srchBody + '%' OR @srchBody = '')
	AND (EH.vcSubject LIKE '%' + @vcSubjectFromTo + '%' OR @vcSubjectFromTo ='')
    --AND (A.[vcAttachmentType] LIKE '%'+ @srchAttachmenttype + '%' OR @srchAttachmenttype='')
   
)
 --select seq1 + totrows1 -1 as TotRows2,* FROM [FilterRows]
,FinalResult
AS 
(
	SELECT  TOP (@PageSize) numEmailHstrID --,seq + totrows -1 as TotRows
	FROM FilterRows
	WHERE totrows > ((@CurrentPage - 1) * @PageSize)
)



-- select * FROM [UserEmail]
select  
		@totalRows TotRows,
		@fltTotalSize AS TotalSize,
		B.[numEmailHstrID],
        B.[numDomainID],
--        B.bintCreatedOn,
--        B.dtReceivedOn,
        B.numEmailHstrID,
        ISNULL(B.numListItemId,-1) AS numListItemId,
        ISNULL(vcSubject, '') AS vcSubject,
        CONVERT(VARCHAR(MAX), vcBody) AS vcBody,
        ISNULL(vcItemId, '') AS ItemId,
        ISNULL(vcChangeKey, '') AS ChangeKey,
        ISNULL(bitIsRead, 'False') AS IsRead,
        ISNULL(vcSize, 0) AS vcSize,
        ISNULL(bitHasAttachments, 'False') AS HasAttachments,
        ISNULL(B.tintType, 1) AS type,
        ISNULL(B.chrSource, 'B') AS chrSource,
        ISNULL(vcCategory, 'white') AS vcCategory,
        CASE B.[numNodeId] WHEN 4 THEN REPLACE(REPLACE(ISNULL(B.[vcTo],''),'<','') , '>','') ELSE B.vcFrom END FromName,
--        dbo.GetEmaillName(B.numEmailHstrID, 4) AS FromName,
--        dbo.GetEmaillName(B.numEmailHstrID, 1) AS ToName,
		CASE B.[numNodeId] WHEN 4 THEN 
			dbo.FormatedDateTimeFromDate(DATEADD(minute, -@ClientTimeZoneOffset,B.bintCreatedOn),B.numDomainId) 
		ELSE 
			dbo.FormatedDateTimeFromDate(B.dtReceivedOn,B.numDomainId)
		END dtReceivedOn,
        ISNULL(CONVERT(VARCHAR(5),B.[numNoofTimes]),'') NoOfTimeOpened
FROM [FinalResult] A INNER JOIN emailHistory B ON B.numEmailHstrID = A.numEmailHstrID;



--WITH UserEmail
--AS
--(
--			   SELECT
--						ROW_NUMBER() OVER ( ORDER BY X.bintCreatedOn DESC ) AS RowNumber,
--                        X.[numEmailHstrID]
--               FROM		[EmailHistory] X
--               WHERE    [numDomainID] = @numDomainID
--						AND [numUserCntId] = @numUserCntId
--						AND numNodeID = @numNodeId
--						AND X.chrSource IN('B','I')
--						
--)
--	SELECT 
--		COUNT(*) 
--   FROM UserEmail UM INNER JOIN [EmailHistory] EH  ON UM.numEmailHstrID =EH.numEmailHstrID
--		LEFT JOIN EmailHstrAttchDtls A ON EH.numEmailHstrID = A.numEmailHstrID
--   WHERE 
--		 (EH.vcBodyText LIKE '%' + @srchBody + '%' OR @srchBody = '')
--	AND (EH.vcSubject LIKE '%' + @vcSubjectFromTo + '%' OR @vcSubjectFromTo ='')
--    AND (A.[vcAttachmentType] LIKE '%'+ @srchAttachmenttype + '%' OR @srchAttachmenttype='')



--SELECT COUNT(*)
--FROM      View_Inbox I INNER JOIN [EmailHistory] EH ON EH.numEmailHstrID = I.numEmailHstrID
--			LEFT JOIN EmailHstrAttchDtls A ON EH.numEmailHstrID = A.numEmailHstrID
-- WHERE     I.numDomainId = @numDomainId
--                                AND I.numUserCntId = @numUserCntId
--                                AND I.numNodeId = @numNodeId
--                                AND I.chrSource IN('B','I')
--                                AND (EH.vcBodyText LIKE '%' + @srchBody + '%' OR @srchBody = '')
--								AND (I.vcSubjectFromTo LIKE '%' + @vcSubjectFromTo + '%' OR @vcSubjectFromTo ='')
--                                AND (A.[vcAttachmentType] LIKE '%'+ @srchAttachmenttype + '%' OR @srchAttachmenttype='')

RETURN ;
*/                                           
/* SELECT Y.[ID],
        Y.[numEmailHstrID],
        emailHistory.[numDomainID],
        emailHistory.bintCreatedOn,
        emailHistory.dtReceivedOn,
        emailHistory.numEmailHstrID,
        ISNULL(vcSubject, '') AS vcSubject,
        CONVERT(VARCHAR(MAX), vcBody) AS vcBody,
        ISNULL(vcItemId, '') AS ItemId,
        ISNULL(vcChangeKey, '') AS ChangeKey,
        ISNULL(bitIsRead, 'False') AS IsRead,
        ISNULL(vcSize, 0) AS vcSize,
        ISNULL(bitHasAttachments, 'False') AS HasAttachments,
        ISNULL(emailHistory.tintType, 1) AS type,
        ISNULL(emailHistory.chrSource, 'B') AS chrSource,
        ISNULL(vcCategory, 'white') AS vcCategory,
        dbo.GetEmaillName(Y.numEmailHstrID, 4) AS FromName,
        dbo.GetEmaillName(Y.numEmailHstrID, 1) AS ToName,
        dbo.FormatedDateFromDate(DATEADD(minute, -@ClientTimeZoneOffset,
                                         emailHistory.bintCreatedOn),
                                 emailHistory.numDomainId) AS bintCreatedOn,
        dbo.FormatedDateFromDate(emailHistory.dtReceivedOn,
                                 emailHistory.numDomainId) AS dtReceivedOn,
        ISNULL(CONVERT(VARCHAR(5),EmailHistory.[numNoofTimes]),'') NoOfTimeOpened
 FROM   ( SELECT   
				ROW_NUMBER() OVER ( ORDER BY X.bintCreatedOn DESC ) AS ID,
				X.[numEmailHstrID]
          FROM      ( SELECT    DISTINCT
                                EH.numEmailHstrID,
                                EH.[bintCreatedOn]
                      FROM      --View_Inbox I INNER JOIN 
                      [EmailHistory] EH --ON EH.numEmailHstrID = I.numEmailHstrID
								LEFT JOIN EmailHstrAttchDtls A ON EH.numEmailHstrID = A.numEmailHstrID
                      WHERE     EH.numDomainId = @numDomainId
                                AND EH.numUserCntId = @numUserCntId
                                AND EH.numNodeId = @numNodeId
                                AND EH.chrSource IN('B','I')
                                AND (EH.vcBodyText LIKE '%' + @srchBody + '%' OR @srchBody = '')
								AND (EH.vcSubject LIKE '%' + @vcSubjectFromTo + '%' OR @vcSubjectFromTo ='')
                                AND (A.[vcAttachmentType] LIKE '%'+ @srchAttachmenttype + '%' OR @srchAttachmenttype='')
                    ) X
          
        ) Y
        INNER JOIN emailHistory ON emailHistory.numEmailHstrID = Y.numEmailHstrID
--        LEFT JOIN EmailHstrAttchDtls ON emailHistory.numEmailHstrID = EmailHstrAttchDtls.numEmailHstrID
        AND Y.ID > @firstRec AND Y.ID < @lastRec
--UNION
--   SELECT   0 AS ID,
--			COUNT(*),
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
----            NULL,
----            NULL,
----            NULL,
--            1,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            ''
--FROM      View_Inbox I INNER JOIN [EmailHistory] EH ON EH.numEmailHstrID = I.numEmailHstrID
--			LEFT JOIN EmailHstrAttchDtls A ON EH.numEmailHstrID = A.numEmailHstrID
-- WHERE     I.numDomainId = @numDomainId
--                                AND I.numUserCntId = @numUserCntId
--                                AND I.numNodeId = @numNodeId
--                                AND I.chrSource IN('B','I')
--                                AND (EH.vcBodyText LIKE '%' + @srchBody + '%' OR @srchBody = '')
--								AND (I.vcSubjectFromTo LIKE '%' + @vcSubjectFromTo + '%' OR @vcSubjectFromTo ='')
--                                AND (A.[vcAttachmentType] LIKE '%'+ @srchAttachmenttype + '%' OR @srchAttachmenttype='')
--   ORDER BY ID

RETURN 
*/
/*
declare @strSql as varchar(8000)                    
   if @columnName = 'FromName'               
 begin        
 set @columnName =  'dbo.GetEmaillName(emailHistory.numEmailHstrID,4)'        
 end        
 if @columnName = 'FromEmail'        
 begin        
 set @columnName = 'dbo.GetEmaillAdd(emailHistory.numEmailHstrID,4)'        
 end        
  if @columnName = 'ToName'               
 begin        
 set @columnName =  'dbo.GetEmaillName(emailHistory.numEmailHstrID,1)'        
 end        
 if @columnName = 'ToEmail'        
 begin        
 set @columnName = 'dbo.GetEmaillAdd(emailHistory.numEmailHstrID,1)'        
 end 
                  
set @strSql='With tblSubscriber AS (SELECT  distinct(emailHistory.numEmailHstrID) ,            
ROW_NUMBER() OVER (ORDER BY '+@columnName+' '+@columnSortOrder+') AS  RowNumber             
from emailHistory                                
--left join EmailHstrAttchDtls on emailHistory.numEmailHstrID=EmailHstrAttchDtls.numEmailHstrID                     
--join EmailHStrToBCCAndCC on emailHistory.numEmailHstrID=EmailHStrToBCCAndCC.numEmailHstrID                     
--join EmailMaster on        EmailMaster.numEmailId=EmailHStrToBCCAndCC.numEmailId                     
where numDomainId='+convert(varchar(15),@numDomainId)+' and numUserCntId='+convert(varchar(15),@numuserCntId)+  
' and numNodeId='+convert(varchar(15),@numNodeId)  
    
 if @chrSource<> '0'   set @strSql=@strSql +' and (chrSource  = ''B'' or chrSource like ''%'+@chrSource+'%'')'   
           
 if @srchBody <> '' set @strSql=@strSql +' and (vcSubject  like ''%'+@srchBody+'%'' or  
  vcBodyText like ''%'+@srchBody+'%''            
  or emailHistory.numEmailHstrID in (select numEmailHstrId from EmailHStrToBCCAndCC where             
 (numemailid in (select numEmailid from emailmaster where vcemailid like ''%'+@srchBody+'%'')) or   
  vcName like ''%'+@srchBody+'%'') )'                
--if @ToEmail <> '' set @strSql=@strSql +'               
--      and (EmailMaster.vcEmailId like '''+@ToEmail+''' and EmailHStrToBCCAndCC.tintType<>4 )  '            
if @srchAttachmenttype <>'' set @strSql=@strSql +'              
  and  emailHistory.numEmailHstrID in  (select numEmailHstrId from EmailHstrAttchDtls                
  where EmailHstrAttchDtls.vcAttachmentType like ''%'+@srchAttachmenttype+'%'')'            */
            
            
/*Please Do not use GetEmaillAdd function as it recursively fetched email address for single mail which create too bad performance*/                 

/*set @strSql=@strSql +')                     
 select RowNumber,            
--dbo.GetEmaillAdd(emailHistory.numEmailHstrID,4) as FromEmail,                  
dbo.GetEmaillName(emailHistory.numEmailHstrID,4) as FromName,                             
--isnull(vcFromEmail,'''') as FromEmail,                                      
--dbo.GetEmaillAdd(emailHistory.numEmailHstrID,1) as ToEmail,                                 
  
--dbo.GetEmaillAdd(emailHistory.numEmailHstrID,2) as CCEmail,                                
--dbo.GetEmaillAdd(emailHistory.numEmailHstrID,3) as BCCEmail,                                     
emailHistory.numEmailHstrID,                            
isnull(vcSubject,'''') as vcSubject,                                      
convert(varchar(max),vcBody) as vcBody,                              
dbo.FormatedDateFromDate( DATEADD(minute,'+ CONVERT(VARCHAR(15),-@ClientTimeZoneOffset)+',bintCreatedOn),numDomainId) as bintCreatedOn,                              
isnull(vcItemId,'''') as ItemId,                              
isnull(vcChangeKey,'''') as ChangeKey,                              
isnull(bitIsRead,''False'') as IsRead,                  
isnull(vcSize,0) as vcSize,                              
isnull(bitHasAttachments,''False'') as HasAttachments,                             
  isnull(EmailHstrAttchDtls.vcFileName,'''') as AttachmentPath,                            
isnull(EmailHstrAttchDtls.vcAttachmentType,'''') as AttachmentType,                      
isnull(EmailHstrAttchDtls.vcAttachmentItemId,'''') as AttachmentItemId,                             
dbo.FormatedDateFromDate(dtReceivedOn,numDomainId) as dtReceivedOn,    
dbo.GetEmaillName(emailHistory.numEmailHstrID,1) as ToName,                          
isnull(emailHistory.tintType,1) as type,                              
isnull(chrSource,''B'') as chrSource,                          
isnull(vcCategory,''white'') as vcCategory                   
 from tblSubscriber T                  
 join emailHistory                   
 on emailHistory.numEmailHstrID=T.numEmailHstrID             
left join EmailHstrAttchDtls on emailHistory.numEmailHstrID=EmailHstrAttchDtls.numEmailHstrID             
where numDomainId='+convert(varchar(15),@numDomainId)+' and numUserCntId='+convert(varchar(15),@numuserCntId)+  
' and numNodeId='+convert(varchar(15),@numNodeId)+' and                
  RowNumber > '+convert(varchar(10),@firstRec)+' and RowNumber < '+convert(varchar(10),@lastRec)+'              
union                 
 select 0 as RowNumber,null,count(*),null,null,            
null,null,null,null,null,null,null,null,null,NULL,           
null,1,null,null from tblSubscriber  order by RowNumber'            
--print    @strSql             
exec (@strSql)
GO
*/               

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_InLineEditDataSave')
DROP PROCEDURE USP_InLineEditDataSave
GO
CREATE PROCEDURE [dbo].[USP_InLineEditDataSave] 
( 
 @numDomainID NUMERIC(9),
 @numUserCntID NUMERIC(9),
 @numFormFieldId NUMERIC(9),
 @bitCustom AS BIT = 0,
 @InlineEditValue VARCHAR(8000),
 @numContactID NUMERIC(9),
 @numDivisionID NUMERIC(9),
 @numWOId NUMERIC(9),
 @numProId NUMERIC(9),
 @numOppId NUMERIC(9),
 @numRecId NUMERIC(9),
 @vcPageName NVARCHAR(100),
 @numCaseId NUMERIC(9),
 @numWODetailId NUMERIC(9),
 @numCommID NUMERIC(9)
 )
AS 

declare @strSql as nvarchar(4000)
DECLARE @vcDbColumnName AS VARCHAR(100),@vcOrigDbColumnName AS VARCHAR(100),@vcLookBackTableName AS varchar(100)
DECLARE @vcListItemType as VARCHAR(3),@numListID AS numeric(9),@vcAssociatedControlType varchar(20)
declare @tempAssignedTo as numeric(9);set @tempAssignedTo=null           
DECLARE @Value1 AS VARCHAR(18),@Value2 AS VARCHAR(18)

IF  @bitCustom =0
BEGIN
	SELECT @vcDbColumnName=vcDbColumnName,@vcLookBackTableName=vcLookBackTableName,
		   @vcListItemType=vcListItemType,@numListID=numListID,@vcAssociatedControlType=vcAssociatedControlType,
		   @vcOrigDbColumnName=vcOrigDbColumnName
	 FROM DycFieldMaster WHERE numFieldId=@numFormFieldId
	 
	 
	if @vcLookBackTableName = 'DivisionMaster' 
	 BEGIN 
	 
	   IF @vcDbColumnName='numFollowUpStatus' --Add to FollowUpHistory
	    Begin
			declare @numFollow as varchar(10),@binAdded as varchar(20),@PreFollow as varchar(10),@RowCount as varchar(2)                            
			                      
			set @PreFollow=(select count(*)  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID)                          
			set @RowCount=@@rowcount  
			                        
			select   @numFollow=numFollowUpStatus, @binAdded=bintModifiedDate from divisionmaster where numDivisionID=@numDivisionID and numDomainId= @numDomainID                    
			
			if @numFollow <>'0' and @numFollow <> @InlineEditValue                          
			begin                          
				if @PreFollow<>0                          
					begin                          
			                   
						if @numFollow <> (select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID order by numFollowUpStatusID desc)                          
							begin                          
			                      	insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
									values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
							end                          
					end                          
				else                          
					begin                          
						insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
						 values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
					end                          
			   end 
			END
		
		 IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from DivisionMaster where numDivisionID = @numDivisionID and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update DivisionMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID where numDivisionID = @numDivisionID  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update DivisionMaster set numAssignedTo=0 ,numAssignedBy=0 where numDivisionID = @numDivisionID  and numDomainId= @numDomainID        
					end    
			END
	    	
		SET @strSql='update DivisionMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'AdditionalContactsInformation' 
	 BEGIN 
			IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='True'
			BEGIN
				DECLARE @bitPrimaryContactCheck AS BIT             
				DECLARE @numID AS NUMERIC(9) 
				
				SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
					FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID 
                   
					IF @@rowcount = 0 SET @numID = 0             
				
					WHILE @numID > 0            
					BEGIN            
						IF @bitPrimaryContactCheck = 1 
							UPDATE  AdditionalContactsInformation SET bitPrimaryContact = 0 WHERE numContactID = @numID  
                              
						SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
						FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactID > @numID    
                                
						IF @@rowcount = 0 SET @numID = 0             
					END 
				END
				ELSE IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='False'
				BEGIN
					IF (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactId!=@numContactId AND ISNULL(bitPrimaryContact,0)=1) = 0
						SET @InlineEditValue='True'
				END 
				
		SET @strSql='update AdditionalContactsInformation set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID and numContactId=@numContactId'
	 END 
	 ELSE if @vcLookBackTableName = 'CompanyInfo' 
	 BEGIN 
		SET @strSql='update CompanyInfo set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCompanyId=(select numCompanyId from divisionmaster where numDivisionID=@numDivisionID and numDomainID=@numDomainID) and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'WorkOrder' 
	 BEGIN 
		DECLARE @numItemCode AS NUMERIC(9)
	 
		IF @numWODetailId>0
		BEGIN
			SET @strSql='update WorkOrderDetails set ' + @vcDbColumnName + '=@InlineEditValue where numWOId=@numWOId and numWODetailId=@numWODetailId'
			
			IF @vcDbColumnName='numQtyItemsReq'
			BEGIN 
				SELECT @numItemCode=numChildItemID FROM WorkOrderDetails WHERE numWOId=@numWOId and numWODetailId=@numWODetailId
				
				EXEC USP_UpdateWorkOrderDetailQuantity @numItemCode,@InlineEditValue,@numWOId
				--UPDATE WorkOrderDetails SET numQtyItemsReq=CONVERT(NUMERIC(9),numQtyItemsReq_Orig * CONVERT(NUMERIC(9,0),@InlineEditValue)) WHERE numWOId=@numWOId
			END 
		END 	
		ELSE
		BEGIN
			SET @strSql='update WorkOrder set ' + @vcDbColumnName + '=@InlineEditValue where numWOId=@numWOId and numDomainID=@numDomainID'
			
			IF @vcDbColumnName='numQtyItemsReq'
			BEGIN 
				SELECT @numItemCode=numItemCode FROM WorkOrder WHERE numWOId=@numWOId and numDomainID=@numDomainID
			
				EXEC USP_UpdateWorkOrderDetailQuantity @numItemCode,@InlineEditValue,@numWOId
				--UPDATE WorkOrderDetails SET numQtyItemsReq=CONVERT(NUMERIC(9),numQtyItemsReq_Orig * CONVERT(NUMERIC(9,0),@InlineEditValue)) WHERE numWOId=@numWOId
			END 
		END	
	 END
	 ELSE if @vcLookBackTableName = 'ProjectsMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if Project is assigned to someone 
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from ProjectsMaster where numProId=@numProId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update ProjectsMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID where numProId = @numProId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update ProjectsMaster set numAssignedTo=0 ,numAssignedBy=0 where numProId = @numProId  and numDomainId= @numDomainID        
					end    
			END
		else IF @vcDbColumnName='numProjectStatus'  ---Updating All Statge to 100% if Completed
			BEGIN
				IF @InlineEditValue='27492'
				BEGIN
					UPDATE PM SET dtCompletionDate=GETUTCDATE(),monTotalExpense=PIE.monExpense,
					monTotalIncome=PIE.monIncome,monTotalGrossProfit=PIE.monBalance,numProjectStatus=27492
					FROM ProjectsMaster PM,dbo.GetProjectIncomeExpense(@numDomainId,@numProId) PIE
					WHERE PM.numProId=@numProId
					
					EXEC USP_ManageProjectCommission @numDomainId,@numProId
						
					Update StagePercentageDetails set tinProgressPercentage=100,bitclose=1 where numProjectID=@numProId

					EXEC dbo.USP_GetProjectTotalProgress
							@numDomainID = @numDomainID, --  numeric(9, 0)
							@numProId = @numProId, --  numeric(9, 0)
							@tintMode = 1 --  tinyint
				END			
				ELSE
				BEGIN
					DELETE from BizDocComission WHERE numDomainID = @numDomainID AND numProId=@numProId AND numComissionID 
						NOT IN (SELECT BC.numComissionID FROM dbo.BizDocComission BC JOIN dbo.PayrollTracking PT 
							ON BC.numComissionID = PT.numComissionID AND BC.numDomainId = PT.numDomainId WHERE BC.numDomainID = @numDomainID AND ISNULL(BC.numProId,0) = @numProId)
				END
			END
			

		SET @strSql='update ProjectsMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numProId=@numProId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'OpportunityMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from OpportunityMaster where numOppId=@numOppId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update OpportunityMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID where numOppId = @numOppId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update OpportunityMaster set numAssignedTo=0 ,numAssignedBy=0 where numOppId = @numOppId  and numDomainId= @numDomainID        
					end    
			END
		ELSE IF @vcDbColumnName='tintActive'
		 BEGIN
		 	SET @InlineEditValue= CASE @InlineEditValue WHEN 'True' THEN 1 ELSE 0 END 
		 END
		 ELSE IF @vcDbColumnName='tintSource'
		 BEGIN
			IF CHARINDEX('~', @InlineEditValue)>0
			BEGIN
				SET @Value1=SUBSTRING(@InlineEditValue, 1,CHARINDEX('~', @InlineEditValue)-1) 
				SET @Value2=SUBSTRING(@InlineEditValue, CHARINDEX('~', @InlineEditValue)+1,LEN(@InlineEditValue)) 
			END	
			ELSE
			BEGIN
				SET @Value1=@InlineEditValue
				SET @Value2=0
			END
			
		 	update OpportunityMaster set tintSourceType=@Value2 where numOppId = @numOppId  and numDomainId= @numDomainID        
		 	
		 	SET @InlineEditValue= @Value1
		 END
		ELSE IF @vcDbColumnName='tintOppStatus' --Update Inventory  0=Open,1=Won,2=Lost
		 BEGIN
			declare @tintOppStatus AS TINYINT
			select @tintOppStatus=tintOppStatus from OpportunityMaster where numOppID=@numOppID              

			if @tintOppStatus=0 and @InlineEditValue='1' --Open to Won
				BEGIN
					select @numDivisionID=numDivisionID from  OpportunityMaster where numOppID=@numOppID   
					DECLARE @tintCRMType AS TINYINT      
					select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					if @tintCRMType=0 --Lead & Order
					begin        
						update divisionmaster set tintCRMType=2,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
						where numDivisionID=@numDivisionID        
					end
					--Promote Prospect to Account
					else if @tintCRMType=1 
					begin        
						update divisionmaster set tintCRMType=2,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
						where numDivisionID=@numDivisionID        
					end    

		 			EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID

					update OpportunityMaster set bintOppToOrder=GETUTCDATE() where numOppId=@numOppId and numDomainID=@numDomainID
				END
			if (@tintOppStatus=1 and @InlineEditValue='2') or (@tintOppStatus=1 and @InlineEditValue='0') --Won to Lost or Won to Open
				EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID 
		 END
		 ELSE IF @vcDbColumnName='vcOppRefOrderNo' --Update vcRefOrderNo OpportunityBizDocs
		 BEGIN
			 IF LEN(ISNULL(@InlineEditValue,''))>0
			 BEGIN
	   			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@InlineEditValue WHERE numOppId=@numOppID
			 END
		 END
		 ELSE IF @vcDbColumnName='numStatus' 
		 BEGIN
		 	 IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @InlineEditValue)
			 BEGIN
	   				EXEC USP_ManageOpportunityAutomationQueue
							@numOppQueueID = 0, -- numeric(18, 0)
							@numDomainID = @numDomainID, -- numeric(18, 0)
							@numOppId = @numOppID, -- numeric(18, 0)
							@numOppBizDocsId = 0, -- numeric(18, 0)
							@numOrderStatus = @InlineEditValue, -- numeric(18, 0)
							@numUserCntID = @numUserCntID, -- numeric(18, 0)
							@tintProcessStatus = 1, -- tinyint
							@tintMode = 1 -- TINYINT
			END 
		 END	
		
		SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
	 END
	 ELSE if @vcLookBackTableName = 'Cases' 
	 BEGIN 
	 		IF @vcDbColumnName='numAssignedTo' ---Updating if Case is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from Cases where numCaseId=@numCaseId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update Cases set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID where numCaseId = @numCaseId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update Cases set numAssignedTo=0 ,numAssignedBy=0 where numCaseId = @numCaseId  and numDomainId= @numDomainID        
					end    
			END
			
		SET @strSql='update Cases set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCaseId=@numCaseId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'Tickler' 
	 BEGIN 	
		SET @strSql='update Communication set ' + @vcOrigDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate() where numCommId=@numCommId and numDomainID=@numDomainID'
	 END
	 
	 
	EXECUTE sp_executeSQL @strSql, N'@numDomainID NUMERIC(9),@numUserCntID NUMERIC(9),@numDivisionID NUMERIC(9),@numContactID NUMERIC(9),@InlineEditValue VARCHAR(8000),@numWOId NUMERIC(9),@numProId NUMERIC(9),@numOppId NUMERIC(9),@numCaseId NUMERIC(9),@numWODetailId NUMERIC(9),@numCommID Numeric(9)',
		     @numDomainID,@numUserCntID,@numDivisionID,@numContactID,@InlineEditValue,@numWOId,@numProId,@numOppId,@numCaseId,@numWODetailId,@numCommID;
		     
	IF @vcAssociatedControlType='SelectBox'                                                    
       BEGIN       
			IF @vcDbColumnName='tintSource' AND @vcLookBackTableName = 'OpportunityMaster'
			BEGIN
				SELECT dbo.fn_GetOpportunitySourceValue(@Value1,@Value2,@numDomainID) AS vcData                                             
			END
			else
				SELECT dbo.fn_getSelectBoxValue(@vcListItemType,@InlineEditValue,@vcDbColumnName) AS vcData                                             
		end 
		ELSE
		BEGIN
			 IF @vcDbColumnName='tintActive'
		  	 BEGIN
			 	SET @InlineEditValue= CASE @InlineEditValue WHEN 1 THEN 'True' ELSE 'False' END 
			 END

			IF @vcAssociatedControlType = 'Check box' or @vcAssociatedControlType = 'Checkbox'
			BEGIN
	 			SET @InlineEditValue= Case @InlineEditValue WHEN 'True' THEN 'Yes' ELSE 'No' END
			END
			
		    SELECT @InlineEditValue AS vcData
		END  
		
	IF @vcLookBackTableName = 'AdditionalContactsInformation' AND @vcDbColumnName = 'numECampaignID'
	BEGIN
		--Manage ECampiagn When Campaign is changed from inline edit
		DECLARE @Date AS DATETIME 
		SELECT @Date = dbo.[GetUTCDateWithoutTime]()
		DECLARE @numECampaignID AS NUMERIC(18,0)
		SET @numECampaignID = @InlineEditValue

		IF @numECampaignID = -1 
		SET @numECampaignID=0
	 
 		EXEC [USP_ManageConEmailCampaign]
		@numConEmailCampID = 0, --  numeric(9, 0)
		@numContactID = @numContactID, --  numeric(9, 0)
		@numECampaignID = @numECampaignID, --  numeric(9, 0)
		@intStartDate = @Date, --  datetime
		@bitEngaged = 1, --  bit
		@numUserCntID = @numUserCntID --  numeric(9, 0)
	END  
END 
ELSE
BEGIN	 

	SET @InlineEditValue=Cast(@InlineEditValue as varchar(1000))
	SELECT @vcAssociatedControlType=Fld_type
	 FROM CFW_Fld_Master WHERE Fld_Id=@numFormFieldId 

	IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	    SET @InlineEditValue=(case when @InlineEditValue='False' then 0 ELSE 1 END)
	END

	IF @vcPageName='organization'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
  
	  END
	ELSE IF @vcPageName='contact'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Cont SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Cont (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END
			
			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Cont_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='project'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Pro SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Pro (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Pro_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END
	ELSE IF @vcPageName='opp'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_Fld_Values_Opp SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_Fld_Values_Opp (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_Fld_Values_Opp_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='case'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Case SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Case (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Case_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END

	if @vcAssociatedControlType = 'SelectBox'            
	 begin            
		 select vcData from ListDetails WHERE numListItemID=@InlineEditValue            
	END
	ELSE IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	 	select CASE @InlineEditValue WHEN 1 THEN 'Yes' ELSE 'No' END AS vcData
	END
	ELSE
		BEGIN
			SELECT @InlineEditValue AS vcData
		END 
END

/****** Object:  StoredProcedure [dbo].[USP_InsertImapNewMails]    Script Date: 07/26/2008 16:19:07 ******/

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertimapnewmails')
DROP PROCEDURE usp_insertimapnewmails
GO
Create PROCEDURE [dbo].[USP_InsertImapNewMails]            
@numUserCntId as numeric(9),            
@numDomainID as numeric(9),            
@numUid NUMERIC(9),
@vcSubject VARCHAR(500),
@vcBody TEXT,
@vcBodyText text,
@dtReceivedOn datetime,
@vcSize varchar(50),
@bitIsRead bit,
@bitHasAttachments bit,
@vcFromName VARCHAR(5000),
@vcToName VARCHAR(5000),
@vcCCName VARCHAR(5000),
@vcBCCName VARCHAR(5000),
@vcAttachmentName VARCHAR(500),
@vcAttachmenttype VARCHAR(500),
@vcNewAttachmentName VARCHAR(500),
@vcAttachmentSize VARCHAR(500)
as             
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON

                                        
DECLARE @Identity AS NUMERIC(9)              
            
if (select count(*) from emailhistory where numUserCntId = @numUserCntId             
   and numDomainid=@numDomainID  and numUid=@numUid)=0            
  begin      
             --Inser into Email Master
             SET @vcToName = @vcToName + '#^#'
             SET @vcFromName = @vcFromName + '#^#'
             SET @vcCCName = @vcCCName + '#^#'
             SET @vcBCCName = @vcBCCName + '#^#'

            -- PRINT 'ToName' + @ToName 
             EXECUTE USP_InsertEmailMaster @vcToName ,@numDomainID
             EXECUTE USP_InsertEmailMaster @vcFromName ,@numDomainID
             EXECUTE USP_InsertEmailMaster @vcCCName ,@numDomainID
             EXECUTE USP_InsertEmailMaster @vcBCCName ,@numDomainID 

             -- Insert Into Email History
                SET ARITHABORT ON
                INSERT  INTO EmailHistory
                        (
                          vcSubject,
                          vcBody,
                          vcBodyText,
                          bintCreatedOn,
                          bitHasAttachments,
                          vcItemId,
                          vcChangeKey,
                          bitIsRead,
                          vcSize,
                          chrSource,
                          tinttype,
                          vcCategory,
                          dtReceivedOn,
                          numDomainid,
                          numUserCntId,
                          numUid,IsReplied,
						  numNodeId
                        )
                VALUES  (
                          @vcSubject,
                          @vcBody,
                          dbo.fn_StripHTML(@vcBodyText),
                          GETUTCDATE(),
                          @bitHasAttachments,
                          '',
                          '',
                          @bitIsRead,
                          CONVERT(VARCHAR(200), @vcSize),
                          'I',
                          1,
                          '',
                          @dtReceivedOn,
                          @numDomainid,
                          @numUserCntId,
                          @numUid,0,
						  ISNULL((SELECT numNodeID FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntId AND numFixID=1 AND ISNULL(bitSystem,0) = 1),0)
                        )                        
                SET @Identity = @@identity        
                

--				To increase performance of Select
                UPDATE  [EmailHistory]
                SET     vcFrom = dbo.fn_GetNewvcEmailString(@vcFromName,@numDomainID),	 --dbo.GetEmaillName(numEmailHstrID, 4),
                        [vcTo] = dbo.fn_GetNewvcEmailString(@vcToName,@numDomainID),    --dbo.GetEmaillName(numEmailHstrID, 1),
                        [vcCC] = dbo.fn_GetNewvcEmailString(@vcCCName,@numDomainID),	 --dbo.GetEmaillName(numEmailHstrID, 3)
                        [vcBCC]= dbo.fn_GetNewvcEmailString(@vcBCCName,@numDomainID)
                WHERE   [numEmailHstrID] = @Identity

				IF(SELECT vcFrom FROM EmailHistory WHERE [numEmailHstrID]=@Identity) IS NOT NULL 
				BEGIN
					update EmailHistory set numEmailId=(SELECT TOP 1 Item FROM dbo.DelimitedSplit8K(vcFrom,'$^$')) 
					where [numEmailHstrID]=@Identity	
				END

                IF @bitHasAttachments = 1 
                    BEGIN                    
                        EXEC USP_InsertAttachment @vcAttachmentName, '',
                            @vcAttachmentType, @Identity, @vcNewAttachmentName,@vcAttachmentSize                       
                    END 
    END            
GO
    
/****** Object:  StoredProcedure [dbo].[USP_InsertIntoEmailHistory]    Script Date: 07/26/2008 16:19:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created BY ANoop Jayaraj                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertintoemailhistory')
DROP PROCEDURE usp_insertintoemailhistory
GO
CREATE PROCEDURE [dbo].[USP_InsertIntoEmailHistory]                          
@vcMessageTo as varchar(5000),                     
@vcMessageToName as varchar(1000)='',              
@vcMessageFrom as varchar(5000),               
@vcMessageFromName as varchar(100)='',              
@vcSubject as varchar(500),                          
@vcBody as text,                       
@vcCC as varchar(5000),                        
@vcCCName as varchar(2000)='',                        
@vcBCC as varchar(5000)='',              
@bitHasAttachment as bit = 0,              
@vcItemId as varchar(1000)='',              
@vcChangeKey as varchar(1000)='',              
@bitIsRead as bit=1,              
@vcSize as numeric=0,              
@chrSource as char(1)= 'O',              
@tinttype as numeric =1,              
@vcCategory as varchar(100)='',          
@vcAttachmentType as varchar(500)= '',          
@vcFileName as varchar (500)= '',              
@vcAttachmentItemId as varchar(1000)=''  ,        
@dtReceived  as datetime  ,      
@numDomainId as numeric  ,    
@vcBodyText as text  ,  
@numUserCntId as numeric(9),
@IsReplied	BIT = FALSE,
@numRepliedMailID	NUMERIC(18) = 0,
@bitInvisible BIT = 0
as                    
declare @Identity as numeric(9)                
declare @FromName as varchar(1000)                
declare @FromEmailAdd as varchar(1000)                
declare @startPos as integer                
declare @EndPos as integer                
declare @EndPos1 as integer                
                
  set @startPos=CHARINDEX( '<', @vcMessageFrom)                
  set @EndPos=CHARINDEX( '>', @vcMessageFrom)                
  if @startPos>0                 
  begin                
   set @FromEmailAdd=  substring(@vcMessageFrom,@startPos+1,@EndPos-@startPos-1)                
   set @startPos=CHARINDEX( '"', @vcMessageFrom)                
   set @EndPos1=CHARINDEX( '"', substring(@vcMessageFrom,@startPos+1,len(@vcMessageFrom)))                
   set @FromName= substring(@vcMessageFrom,@startPos+1,@EndPos1-1)                
                   
                   
  end                 
  else                 
  begin                
   set @FromEmailAdd=@vcMessageFrom                
   set @FromName=@vcMessageFromName                
  end                
declare @updateinsert as bit     
    
if @vcItemId = ''    
begin    
 set @updateinsert  = 1    
end    
else    
begin    
 if (select count(*) from emailhistory  where vcItemId =@vcItemId) > 0    
  begin    
   set @updateinsert  = 0    
  end    
 else    
  begin    
   set @updateinsert  = 1    
  end    
end    
                  
if @updateinsert =0    
begin              
update EmailHistory            
set            
vcSubject=@vcSubject,            
vcBody=@vcBody,      
vcBodyText=dbo.fn_StripHTML(@vcBodyText),          
bintCreatedOn=getutcdate(),            
bitHasAttachments=@bitHasAttachment,            
vcItemId=@vcItemId,            
vcChangeKey=@vcChangeKey,            
bitIsRead=@bitIsRead,            
vcSize=convert(varchar(200),@vcSize),            
chrSource=@chrSource,            
tinttype=@tinttype,            
vcCategory =@vcCategory ,        
dtReceivedOn=@dtReceived --,IsReplied = @IsReplied           
where vcItemId = @vcItemId   and numDomainid= @numDomainId      
              
end                
else             
begin            
declare @node as NUMERIC(18,0)
set @node = case when @vcCategory = 'B' then (SELECT numNodeID FROM InboxTreeSort WHERE numDomainID=@numDomainId AND numUserCntID=@numUserCntId AND bitSystem = 1 AND numFixID = 4) else 0 end 

SET @vcMessageTo = @vcMessageTo + '#^#'
SET @vcMessageFrom = @vcMessageFrom + '#^#'
SET @vcCC = @vcCC + '#^#'
SET @vcBCC = @vcBCC + '#^#'

EXECUTE dbo.USP_InsertEmailMaster @vcMessageTo ,@numDomainID
EXECUTE dbo.USP_InsertEmailMaster @vcMessageFrom ,@numDomainID
EXECUTE dbo.USP_InsertEmailMaster @vcCC ,@numDomainID
EXECUTE dbo.USP_InsertEmailMaster @vcBCC ,@numDomainID 

--IF @vcCategory = 'B'
--	SELECT @vcSize =DATALENGTH(@vcBody)
            
Insert into EmailHistory(vcSubject,vcBody,vcBodyText,bintCreatedOn,bitHasAttachments,vcItemId,vcChangeKey,bitIsRead,vcSize,chrSource,tinttype,vcCategory,dtReceivedOn,numDomainid,numUserCntId,numNodeId,vcFrom,vcTo,vcCC,vcBCC,IsReplied,bitInvisible)                          
values                          
(@vcSubject,@vcBody,dbo.fn_StripHTML(@vcBodyText),getutcdate(),@bitHasAttachment,@vcItemId,@vcChangeKey,@bitIsRead,convert(varchar(200),@vcSize),@chrSource,@tinttype,@vcCategory,@dtReceived,@numDomainid,@numUserCntId,@node,dbo.fn_GetNewvcEmailString(@vcMessageFrom),dbo.fn_GetNewvcEmailString(@vcMessageTo),dbo.fn_GetNewvcEmailString(@vcCC),dbo.fn_GetNewvcEmailString(@vcBCC),@IsReplied,@bitInvisible)              
set @Identity=@@identity                 

update EmailHistory set numEmailId=(SELECT TOP 1 Item FROM dbo.DelimitedSplit8K(vcFrom,'$^$'))
				where [numEmailHstrID]=@Identity                

update EmailHistory SET IsReplied = @IsReplied WHERE [numEmailHstrID] = @numRepliedMailID
                
--exec USP_InsertEmailAddCCAndBCC  @vcMessageTo,1,@Identity , @vcMessageToName               
--exec USP_InsertEmailAddCCAndBCC  @vcBCC,2,@Identity ,''               
--exec USP_InsertEmailAddCCAndBCC  @vcCC,3,@Identity  ,@vcCCName          
--exec USP_InsertEmailAddCCAndBCC  @vcMessageFrom,4,@Identity,  @vcMessageFromName      
  
  --No longer being used .. We are calling same function from front end to add attachment to email.. -- by chintan       
--if @bitHasAttachment  = 1          
--begin          
--exec USP_InsertAttachment   @vcFileName,@vcAttachmentItemId,@vcAttachmentType,@Identity           
--end       

SELECT ISNULL(@Identity,0)
end
GO

GO
/****** Object:  StoredProcedure [dbo].[USP_ManageAlertConfig]    Script Date: 11/02/2011 12:44:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageAlertConfig')
DROP PROCEDURE USP_ManageAlertConfig
GO
CREATE PROC [dbo].[USP_ManageAlertConfig] 
    @numAlertConfigId numeric(18, 0) OUTPUT ,
    @numDomainId numeric(18, 0),
    @numContactId numeric(9, 0),
    @bitOpenActionItem bit,
    @vcOpenActionMsg varchar(15),
    @bitOpenCases bit,
    @vcOpenCasesMsg varchar(15),
    @bitOpenProject bit,
    @vcOpenProjectMsg varchar(15),
    @bitOpenSalesOpp bit,
    @vcOpenSalesOppMsg varchar(15),
    @bitOpenPurchaseOpp bit,
    @vcOpenPurchaseOppMsg varchar(15),
    @bitBalancedue bit,
    @monBalancedue money,
    @vcBalancedueMsg varchar(15),
    @bitUnreadEmail bit,
    @intUnreadEmail int,
    @vcUnreadEmailMsg varchar(15),
    @bitPurchasedPast bit,
    @monPurchasedPast money,
    @vcPurchasedPastMsg varchar(15),
    @bitSoldPast bit,
    @monSoldPast money,
    @vcSoldPastMsg varchar(15),
    @bitCustomField bit,
    @numCustomFieldId numeric(9, 0),
    @vcCustomFieldValue varchar(50),
    @vcCustomFieldMsg varchar(15),
	@bitCampaign BIT
AS 
DECLARE @numCount AS NUMERIC
SET @numCount = 0
SELECT @numCount=COUNT(*) FROM dbo.AlertConfig WHERE numDomainId =@numDomainId AND numContactId=@numContactId
PRINT @numCount 
IF @numCount > 0 
    BEGIN
 UPDATE [dbo].[AlertConfig]
 SET    [numDomainId] = @numDomainId, [numContactId] = @numContactId, [bitOpenActionItem] = @bitOpenActionItem, [vcOpenActionMsg] = @vcOpenActionMsg, 
 [bitOpenCases] = @bitOpenCases, [vcOpenCasesMsg] = @vcOpenCasesMsg, [bitOpenProject] = @bitOpenProject, [vcOpenProjectMsg] = @vcOpenProjectMsg, 
 [bitOpenSalesOpp] = @bitOpenSalesOpp, [vcOpenSalesOppMsg] = @vcOpenSalesOppMsg, [bitOpenPurchaseOpp] = @bitOpenPurchaseOpp,
 [vcOpenPurchaseOppMsg] = @vcOpenPurchaseOppMsg, [bitBalancedue] = @bitBalancedue, [monBalancedue] = @monBalancedue, 
 [vcBalancedueMsg] = @vcBalancedueMsg,[bitUnreadEmail] = @bitUnreadEmail, [intUnreadEmail] = @intUnreadEmail, [vcUnreadEmailMsg] = @vcUnreadEmailMsg, [bitPurchasedPast] = @bitPurchasedPast, [monPurchasedPast] = @monPurchasedPast, [vcPurchasedPastMsg] = @vcPurchasedPastMsg, [bitSoldPast] = @bitSoldPast, [monSoldPast] = @monSoldPast, [vcSoldPastMsg] = @vcSoldPastMsg, [bitCustomField] = @bitCustomField, [numCustomFieldId] = @numCustomFieldId, [vcCustomFieldValue] = @vcCustomFieldValue, [vcCustomFieldMsg] = @vcCustomFieldMsg,
 bitCampaign = @bitCampaign
 WHERE numDomainId =@numDomainId AND numContactId=@numContactId  --[numAlertConfigId] = @numAlertConfigId
     END 
else 
 BEGIN
 INSERT INTO [dbo].[AlertConfig] ([numDomainId], [numContactId], [bitOpenActionItem], [vcOpenActionMsg], [bitOpenCases], [vcOpenCasesMsg], [bitOpenProject], [vcOpenProjectMsg], [bitOpenSalesOpp], [vcOpenSalesOppMsg], [bitOpenPurchaseOpp], [vcOpenPurchaseOppMsg], [bitBalancedue], [monBalancedue], [vcBalancedueMsg], [bitUnreadEmail], [intUnreadEmail], [vcUnreadEmailMsg], [bitPurchasedPast], [monPurchasedPast], [vcPurchasedPastMsg], [bitSoldPast], [monSoldPast], [vcSoldPastMsg], [bitCustomField], [numCustomFieldId], [vcCustomFieldValue], [vcCustomFieldMsg], [bitCampaign])
  SELECT @numDomainId, @numContactId, @bitOpenActionItem, @vcOpenActionMsg, @bitOpenCases, @vcOpenCasesMsg, @bitOpenProject, @vcOpenProjectMsg, @bitOpenSalesOpp, @vcOpenSalesOppMsg, @bitOpenPurchaseOpp, @vcOpenPurchaseOppMsg, @bitBalancedue, @monBalancedue, @vcBalancedueMsg,@bitUnreadEmail, @intUnreadEmail, @vcUnreadEmailMsg, @bitPurchasedPast, @monPurchasedPast, @vcPurchasedPastMsg, @bitSoldPast, @monSoldPast, @vcSoldPastMsg, @bitCustomField, @numCustomFieldId, @vcCustomFieldValue, @vcCustomFieldMsg, @bitCampaign
  
  SET @numAlertConfigId= SCOPE_IDENTITY()
 END 
 
/****** Object:  StoredProcedure [dbo].[USP_ManageCategory]    Script Date: 07/26/2008 16:19:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managecategory')
DROP PROCEDURE usp_managecategory
GO
CREATE PROCEDURE [dbo].[USP_ManageCategory]        
@numCatergoryId as numeric(9),        
@vcCatgoryName as varchar(1000),       
@numDomainID as numeric(9)=0,
@vcDescription as VARCHAR(MAX),
@intDisplayOrder AS INT ,
@vcPathForCategoryImage AS VARCHAR(100),
@numDepCategory AS NUMERIC(9,0)
      
as 

DECLARE @tintLevel AS TINYINT
SET @tintLevel = 1
IF	@numDepCategory <> 0
	BEGIN
		SELECT @tintLevel = (tintLevel + 1) FROM dbo.Category WHERE numCategoryID = @numDepCategory
	END       
       
if @numCatergoryId=0        
begin        
insert into Category (vcCategoryName,tintLevel,numDomainID,vcDescription,intDisplayOrder ,vcPathForCategoryImage,numDepCategory)        
values(@vcCatgoryName,@tintLevel,@numDomainID,@vcDescription,@intDisplayOrder ,@vcPathForCategoryImage,@numDepCategory)        
end        
else if @numCatergoryId>0        
begin        
update Category set vcCategoryName=@vcCatgoryName,vcDescription=@vcDescription ,intDisplayOrder = @intDisplayOrder ,tintLevel = @tintLevel,vcPathForCategoryImage = @vcPathForCategoryImage,numDepCategory = @numDepCategory      
where numCategoryID=@numCatergoryId        
end
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageConEmailCampaign]    Script Date: 07/26/2008 16:19:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--create by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageconemailcampaign')
DROP PROCEDURE usp_manageconemailcampaign
GO
CREATE PROCEDURE [dbo].[USP_ManageConEmailCampaign]        
@numConEmailCampID numeric(9)=0,        
@numContactID numeric(9)=0,        
@numECampaignID numeric(9)=0,        
@intStartDate datetime,        
@bitEngaged bit,    
@numUserCntID numeric(9)=0        
as        
    
--DisEngage Current contact from ECampaign
IF @numECampaignID = 0 
BEGIN
	UPDATE [ConECampaign] SET [bitEngaged]=0 WHERE [numContactID]=@numContactID
	RETURN ;
END
        
        
if @numConEmailCampID= 0         
begin        
	
	-- stop all other campaign for given contact 
	update ConECampaign set bitEngaged= 0 where numContactID=@numContactID AND [numECampaignID] <> @numECampaignID
	
	--Prevent Duplicate entry
	IF NOT EXISTS (SELECT * FROM [ConECampaign] WHERE [numContactID]=@numContactID AND [numECampaignID]=@numECampaignID AND [bitEngaged]=1)
	BEGIN
		
		/*Add Start Time to Given Start Date from ECampaign*/
		DECLARE @StartDateTime DATETIME 	   
		DECLARE @dtECampStartTime1 DATETIME 	   
		SELECT @dtECampStartTime1 = ISNULL(dtStartTime,0) FROM [ECampaign] WHERE [numECampaignID]=@numECampaignID
		--	SELECT @StartDateTime;
		SELECT @StartDateTime = DATEADD(hour,DATEPART(hour,@dtECampStartTime1) ,@intStartDate)
		SELECT @StartDateTime = DATEADD(minute,DATEPART(minute,@dtECampStartTime1) ,@StartDateTime)
			
	
		--Insert To Header
		insert into ConECampaign(numContactID,numECampaignID,intStartDate,bitEngaged,numRecOwner)        
		values (@numContactID,@numECampaignID,@StartDateTime,@bitEngaged,@numUserCntID)
		set @numConEmailCampID=@@identity       
		--Insert To Detail
		DECLARE @TEMP TABLE
		(
			RowNo INT,
			numECampDTLId NUMERIC(18,0),
			tintDays INT,
			tintWaitPeriod TINYINT
		)

		INSERT INTO 
			@TEMP 
		SELECT 
			ROW_NUMBER() OVER(ORDER BY numECampDTLId),
			numECampDTLId,
			tintDays,
			tintWaitPeriod 
		FROM 
			ECampaignDTLs      
		WHERE 
			numECampID=@numECampaignID  AND
			(numEmailTemplate > 0 OR numActionItemTemplate > 0)
		ORDER BY
			numECampDTLId


		DECLARE @i AS INT = 0
		DECLARE @Count AS INT
		SELECT @Count=COUNT(*) FROM @TEMP


		DECLARE @TempDate AS DATETIME
		DECLARE @numECampDTLId AS NUMERIC(18,0)
		DECLARE @TempDays AS INT
		DECLARE @TempWaitPeriod AS INT

		SET @TempDate = @StartDateTime

		WHILE @i <= @Count
		BEGIN

			SELECT @numECampDTLId=numECampDTLId, @TempDays=tintDays, @TempWaitPeriod=tintWaitPeriod FROM @TEMP WHERE RowNo=@i

			INSERT INTO ConECampaignDTL
			(
				numConECampID,
				numECampDTLID,
				dtExecutionDate
			)       
			SELECT 
				@numConEmailCampID,
				@numECampDTLId,
				@TempDate 
			FROM 
				@TEMP      
			WHERE 
				RowNo=@i

			IF (@TempWaitPeriod = 1) --Day
				SELECT @TempDate = DATEADD(D,@TempDays,@TempDate)
			ELSE IF (@TempWaitPeriod = 2) --Week
				SELECT @TempDate = DATEADD(D,(@TempDays * 7),@TempDate)
			
			SET @i = @i + 1
		END
	END
end        
else         
begin        
	update ConECampaign set numContactID=@numContactID,        
	numECampaignID=@numECampaignID,        
	intStartDate=@intStartDate,        
	bitEngaged=@bitEngaged        
	where numConEmailCampID=@numConEmailCampID        
	end        
	        
	select @numConEmailCampID
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageECampaign]    Script Date: 06/04/2009 16:20:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageecampaign')
DROP PROCEDURE usp_manageecampaign
GO
CREATE PROCEDURE [dbo].[USP_ManageECampaign]  
@numECampaignID numeric(9)=0,  
@vcCampaignName as varchar(100)='',  
@txtDesc text='',  
@strECampaignDTLs as text,
@numDomainID as numeric(9)=0 ,
@dtStartTime AS DATETIME,
@fltTimeZone AS FLOAT,
@tintTimeZoneIndex TINYINT,
@tintFromField TINYINT,
@numFromContactID NUMERIC,
@numUserContactID NUMERIC(9)
as  
DECLARE @hDoc int    
if @numECampaignID=0   
  
begin  
insert into ECampaign(vcECampName,txtDesc,numDomainID,dtStartTime,[tintFromField],[numFromContactID],fltTimeZone,tintTimeZoneIndex,numCreatedBy)  
values(@vcCampaignName,@txtDesc,@numDomainID,@dtStartTime,@tintFromField,@numFromContactID,@fltTimeZone,@tintTimeZoneIndex,@numUserContactID)  
set @numECampaignID=@@identity    
  
      
 EXEC sp_xml_preparedocument @hDoc OUTPUT, @strECampaignDTLs        
        
 insert into ECampaignDTLs        
  (numECampID,numEmailTemplate,numActionItemTemplate,tintDays,tintWaitPeriod,[numFollowUpID])        --,[numEmailGroupID],[dtStartDate]  ,@numEmailGroupID,@dtStartDate
         
 select @numECampaignID,X.numEmailTemplate,X.numActionItemTemplate,x.tintDays,X.tintWaitPeriod,X.numFollowUpID from(        
 SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table',2)        
 WITH  (        
  numEmailTemplate numeric(9),
  numActionItemTemplate NUMERIC(9),      
  tintDays TINYINT,
  tintWaitPeriod TINYINT,
  [numFollowUpID] numeric
  ))X        
        
        
 EXEC sp_xml_removedocument @hDoc  
  
end  
else  
begin  
  
update ECampaign set vcECampName=@vcCampaignName,  
txtDesc=@txtDesc ,
dtStartTime=@dtStartTime,
fltTimeZone = @fltTimeZone,
tintTimeZoneIndex=@tintTimeZoneIndex,
[tintFromField] = @tintFromField,
[numFromContactID]=@numFromContactID
where numECampaignID= @numECampaignID  
  
 EXEC sp_xml_preparedocument @hDoc OUTPUT, @strECampaignDTLs        
          
    update ECampaignDTLs set         
     numEmailTemplate=X.numEmailTemplate,        
     numActionItemTemplate=X.numActionItemTemplate,
     tintDays=X.tintDays,
	 tintWaitPeriod = X.tintWaitPeriod,
     [numFollowUpID] = X.numFollowUpID 
      From (SELECT *        
     FROM OPENXML(@hDoc,'/NewDataSet/Table1',2)         
      with(  
      numECampDTLId numeric(9),  
      numEmailTemplate numeric(9),
      numActionItemTemplate numeric(9),
      tintDays TINYINT,
	  tintWaitPeriod TINYINT,
      numFollowUpID NUMERIC ))X         
      where  ECampaignDTLs.numECampDTLId=X.numECampDTLId    
  
 EXEC sp_xml_removedocument @hDoc  
  
end  
  
select @numECampaignID
/****** Object:  StoredProcedure [dbo].[Usp_ManageInboxTree]    Script Date: 07/26/2008 16:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageinboxtree')
DROP PROCEDURE usp_manageinboxtree
GO
CREATE PROCEDURE [dbo].[Usp_ManageInboxTree]    
@numUserCntID as numeric(9),    
@numDomainID as numeric(9),    
@NodeID as numeric(9),    
@NodeName as varchar(30),    
@mode as tinyint,    
@ParentId as numeric(9)    
as      
    
if @mode = 0      
begin      
IF Not Exists (Select 'col1' from  InboxTreeSort where numdomainid=@numDomainID AND numUserCntId=@numUserCntID)
		 BEGIN
			 select numNodeID,numParentId,vcName as vcNodeName from InboxTree where (numdomainid=0 or numdomainid=@numDomainID )       
			   and (numUserCntId=@numUserCntID or numUserCntId=0)      
			 order by numNodeID,numParentId 
		 END
   ELSE 
		BEGIN
			SELECT numNodeID,numParentId,ISNULL(vcNodeName,'') as vcNodeName ,numSortOrder, bitSystem, numFixID 
				FROM InboxTreeSort 
				WHERE (numdomainid=@numDomainID)   
					AND (numUserCntId=@numUserCntID )   
                    
			    ORDER BY numSortOrder ASC    
		END     
end      
if @mode = 1      
begin   
	DECLARE @ErrorMessage NVARCHAR(4000)
	SET @ErrorMessage=''
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
		
	BEGIN TRANSACTION

	BEGIN TRY
		--Changed by sachin sadhu|| Date:21stJan2014
    --Purpose:Bug on creating Folders||VirginDrinks
		DECLARE @numSortOrder NUMERIC
		SELECT @numSortOrder=MAX(numSortOrder)+1 FROM dbo.InboxTreeSort WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID   

		IF(@ParentId=0)
		BEGIN
			SELECT @ParentId=numNodeID FROM dbo.InboxTreeSort WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID AND numParentID IS NULL
		END

		IF EXISTS (SELECT numNodeID FROM InboxTreeSort WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID AND numParentID = @ParentId AND LOWER(vcNodeName) = LOWER(@NodeName))
		BEGIN
			RAISERROR ('FOLDER_ALREADY_EXISTS',16,1)
			RETURN -1
		END 
		ELSE
		BEGIN
			INSERT INTO [InboxTree]      
				(vcName      
				,numParentId      
				,[numDomainId],numUserCntId)      
			VALUES      
				(@NodeName      
				,@ParentId      
				,@numDomainID,@numUserCntId)   
			--To Save values into InboxTree Sort
			INSERT INTO [InboxTreeSort]      
				(vcNodeName      
				,numParentId      
				,[numDomainId],numUserCntId,numSortOrder)      
			VALUES      
				(@NodeName      
				,@ParentId      
				,@numDomainID,@numUserCntId,@numSortOrder) 
		END
	END TRY
	BEGIN CATCH
		SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState);
	END CATCH

	IF @@TRANCOUNT > 0
			COMMIT TRANSACTION;
end      
      
if @mode = 2      
begin      
     
update  emailhistory set numNodeid=2 where numdomainid=@numDomainID and numUserCntId=@numUserCntID and  numNodeid in    
  ( select numNodeId from inboxTree where (numNodeID = @NodeID or numParentId = @NodeID)  and       
   numdomainid=@numDomainID and numUserCntId=@numUserCntID )     
   
 delete [InboxTree] where (numNodeID = @NodeID or numParentId = @NodeID)  and       
   numdomainid=@numDomainID and numUserCntId=@numUserCntID    
--Update Email History with [InboxTreeSort]
update  emailhistory set numNodeid=2 where numdomainid=@numDomainID and numUserCntId=@numUserCntID and  numNodeid in    
  ( select numNodeId from InboxTreeSort where (numNodeID = @NodeID or numParentId = @NodeID)  and       
   numdomainid=@numDomainID and numUserCntId=@numUserCntID )  
      
 delete [InboxTreeSort] where (numNodeID = @NodeID or numParentId = @NodeID)  and       
   numdomainid=@numDomainID and numUserCntId=@numUserCntID    
end  
GO

GO
/****** Object:  StoredProcedure [dbo].[USP_ManagePageElementDetails]    Script Date: 08/08/2009 16:16:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManagePageElementDetails')
DROP PROCEDURE USP_ManagePageElementDetails
GO
CREATE PROCEDURE [dbo].[USP_ManagePageElementDetails]
          @numElementID NUMERIC(9),
          @numSiteID    NUMERIC(9),
          @numDomainID  NUMERIC(9),
          @strItems     TEXT  = NULL
AS
  BEGIN
    DELETE FROM PageElementDetail
    WHERE       numSiteID = @numSiteID
                AND numElementID = @numElementID;
    DECLARE  @hDocItem INT
    IF CONVERT(VARCHAR(10),@strItems) <> ''
      BEGIN
        EXEC sp_xml_preparedocument
          @hDocItem OUTPUT ,
          @strItems
        INSERT INTO PageElementDetail
                   (numElementID,
                    numAttributeID,
                    vcAttributeValue,
                    numSiteID,
                    numDomainID,vcHtml)
        SELECT @numElementID,
               X.numAttributeID,
               X.vcAttributeValue,
               @numSiteID,
               @numDomainID,X.vcHtml
        FROM   (SELECT *
                FROM   OPENXML (@hDocItem, '/NewDataSet/Item', 2)
                          WITH (numAttributeID   NUMERIC(9)  ,
                                vcAttributeValue VARCHAR(500),vcHtml text)) X
        EXEC sp_xml_removedocument
          @hDocItem
      END
 SELECT 0
  END

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_ManageRMAInventory' ) 
    DROP PROCEDURE usp_ManageRMAInventory
GO
CREATE PROCEDURE [dbo].[usp_ManageRMAInventory]
    @numReturnHeaderID AS NUMERIC(9) = 0,
    @numDomainId NUMERIC(9),
    @numUserCntID AS NUMERIC(9)=0,
    @tintFlag AS TINYINT  --1: Receive 2: Revert
AS 
  BEGIN
  	
  	DECLARE @tintReturnType AS TINYINT,@tintReceiveType AS TINYINT
  	DECLARE @numReturnItemID AS NUMERIC,@itemcode as numeric,@numUnits as NUMERIC,@numWareHouseItemID as numeric
	DECLARE @monAmount as money 
	DECLARE @onHand AS NUMERIC,@onOrder AS NUMERIC,@onBackOrder AS NUMERIC,@onAllocation AS NUMERIC
	DECLARE @description AS VARCHAR(100)
    
  	SELECT @tintReturnType=tintReturnType,@tintReceiveType=tintReceiveType FROM dbo.ReturnHeader WHERE numReturnHeaderID=@numReturnHeaderID
  	
  		select top 1 @numReturnItemID=numReturnItemID,@itemcode=RI.numItemCode,@numUnits=RI.numUnitHourReceived,
  		 @numWareHouseItemID=ISNULL(numWareHouseItemID,0),@monAmount=monTotAmount
		 from ReturnItems RI join Item I on RI.numItemCode=I.numItemCode                                          
		 where numReturnHeaderID=@numReturnHeaderID AND (charitemtype='P' OR 1=(CASE WHEN @tintReturnType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END)) order by RI.numReturnItemID                                       
		 while @numReturnItemID>0                                        
		  begin   
			 IF @numWareHouseItemID>0
			 BEGIN                     
				SELECT  @onHand = ISNULL(numOnHand, 0),
						@onAllocation = ISNULL(numAllocation, 0),
						@onOrder = ISNULL(numOnOrder, 0),
						@onBackOrder = ISNULL(numBackOrder, 0)
				FROM    WareHouseItems
				WHERE   numWareHouseItemID = @numWareHouseItemID 
			
				--Receive : SalesReturn 
				IF (@tintFlag=1 AND @tintReturnType=1)
				BEGIN
					SET @description='Sales Return Receive (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ')'
										
                    IF @onBackOrder >= @numUnits 
                    BEGIN            
                         SET @onBackOrder = @onBackOrder - @numUnits            
                         SET @onAllocation = @onAllocation + @numUnits            
                    END            
                    ELSE 
                    BEGIN            
                         SET @onAllocation = @onAllocation + @onBackOrder            
                         SET @numUnits = @numUnits - @onBackOrder            
                         SET @onBackOrder = 0            
                         SET @onHand = @onHand + @numUnits            
                    END         
				END
				--Revert : SalesReturn
				ELSE IF (@tintFlag=2 AND @tintReturnType=1)
				BEGIN
					SET @description='Sales Return Delete (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ')'
					
					IF @onHand  - @numUnits >= 0
						BEGIN						
							SET @onHand = @onHand - @numUnits
						END
					ELSE IF (@onHand + @onAllocation)  - @numUnits >= 0
						BEGIN						
							SET @numUnits = @numUnits - @onHand
                            SET @onHand = 0 
                            
                            SET @onBackOrder = @onBackOrder + @numUnits
                            SET @onAllocation = @onAllocation - @numUnits  
						END	
					ELSE IF  (@onHand + @onBackOrder + @onAllocation) - @numUnits >= 0
						BEGIN
							SET @numUnits = @numUnits - @onHand
                            SET @onHand = 0 
                            
                            SET @numUnits = @numUnits - @onAllocation  
                            SET @onAllocation = 0 
                            
							SET @onBackOrder = @onBackOrder + @numUnits
						END
				END
				--Revert : PurchaseReturn
				ELSE IF (@tintFlag=2 AND @tintReturnType=2) 
				BEGIN
					SET @description='Purchase Return Delete (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ')'
					
					IF @onBackOrder >= @numUnits 
                    BEGIN            
                         SET @onBackOrder = @onBackOrder - @numUnits            
                         SET @onAllocation = @onAllocation + @numUnits            
                    END            
                    ELSE 
                    BEGIN            
                         SET @onAllocation = @onAllocation + @onBackOrder            
                         SET @numUnits = @numUnits - @onBackOrder            
                         SET @onBackOrder = 0            
                         SET @onHand = @onHand + @numUnits            
                    END      
				END
				--Receive : PurchaseReturn 
				ELSE IF (@tintFlag=1 AND @tintReturnType=2) 
				BEGIN
					SET @description='Purchase Return Receive (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ')'
				
                    IF @onHand  - @numUnits >= 0
						BEGIN						
							SET @onHand = @onHand - @numUnits
						END
					ELSE
						BEGIN
						 RAISERROR ( 'PurchaseReturn_Qty', 16, 1 ) ;
						 RETURN ;
						END	
--					ELSE IF (@onHand + @onBackOrder)  - @numUnits >= 0
--						BEGIN						
--							SET @numUnits = @numUnits - @onHand
--                            SET @onHand = 0 
--                            
--                            SET @onBackOrder = @onBackOrder - @numUnits  
--						END	
--					ELSE IF  (@onHand + @onBackOrder + @onAllocation) - @numUnits >= 0
--						BEGIN
--							SET @numUnits = @numUnits - @onHand
--                            SET @onHand = 0 
--                            
--                            SET @numUnits = @numUnits - @onBackOrder  
--                            SET @onBackOrder = 0 
--                            
--							SET @onAllocation = @onAllocation - @numUnits
--						END
				END 
				
				     UPDATE  WareHouseItems
                            SET     numOnHand = @onHand,
                                    numAllocation = @onAllocation,
                                    numBackOrder = @onBackOrder,
                                    numOnOrder = @onOrder,dtModified = GETDATE() 
                            WHERE   numWareHouseItemID = @numWareHouseItemID 
                            
                  IF @numWareHouseItemID>0
				 BEGIN
					DECLARE @tintRefType AS TINYINT;SET @tintRefType=5
					  
						EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
						@numReferenceID = @numReturnHeaderID, --  numeric(9, 0)
						@tintRefType = @tintRefType, --  tinyint
						@vcDescription = @description, --  varchar(100)
						@numModifiedBy = @numUserCntID,
						@numDomainId = @numDomainId
                  END           
			END
		  
		  select top 1 @numReturnItemID=numReturnItemID,@itemcode=RI.numItemCode,@numUnits=RI.numUnitHourReceived,
  		 @numWareHouseItemID=ISNULL(numWareHouseItemID,0),@monAmount=monTotAmount
		 from ReturnItems RI join Item I on RI.numItemCode=I.numItemCode                                          
		 where numReturnHeaderID=@numReturnHeaderID AND (charitemtype='P' OR 1=(CASE WHEN @tintReturnType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END)) and RI.numReturnItemID>@numReturnItemID order by RI.numReturnItemID
							
		   if @@rowcount=0 set @numReturnItemID=0         
		  END
		  
	
  END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageSimilarItem')
DROP PROCEDURE USP_ManageSimilarItem
GO
CREATE PROCEDURE USP_ManageSimilarItem
@numDomainID NUMERIC(9),
@numParentItemCode NUMERIC(9),
@numItemCode NUMERIC(9),
@vcRelationship VARCHAR(500),
@bitPreUpSell BIT,
@bitPostUpSell BIT, 
@vcUpSellDesc VARCHAR(1000)
AS 
BEGIN
IF NOT EXISTS( SELECT * FROM [SimilarItems] WHERE [numDomainID]=@numDomainID AND numItemCode = @numItemCode AND numParentItemCode = @numParentItemCode)
	BEGIN
		INSERT INTO [SimilarItems] (
			[numDomainID],
			[numItemCode],
			[numParentItemCode],
			[vcRelationship],
			[bitPreUpSell], 
			[bitPostUpSell], 
			[vcUpSellDesc]
		) VALUES (  
			/* numDomainID - numeric(18, 0) */ @numDomainID,
			/* vcExportTables - varchar(100) */ @numItemCode,
			/* dtLastExportedOn - datetime */ @numParentItemCode,@vcRelationship,@bitPreUpSell,@bitPostUpSell, @vcUpSellDesc) 
	END
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageUOM')
DROP PROCEDURE USP_ManageUOM
GO
CREATE PROCEDURE [dbo].[USP_ManageUOM]
	@numUOMId NUMERIC(9) OUTPUT,
	@numDomainID NUMERIC(9),
	@vcUnitName VARCHAR(100),
	@tintUnitType TINYINT,
	@bitEnabled BIT
AS

IF @numUOMId = 0 AND (EXISTS(SELECT [vcUnitName] from UOM Where [vcUnitName] = @vcUnitName AND [UOM].[numDomainId] = @numDomainID AND [UOM].[tintUnitType] = @tintUnitType))
BEGIN
		RAISERROR ('UOM_ALREADY_EXISTS',16,1);
		RETURN -1
END
IF @numUOMId = 0
BEGIN
	INSERT INTO UOM 
	(
	  [numDomainId],[vcUnitName],[tintUnitType],[bitEnabled]
	)
	VALUES 
	(
	   @numDomainId,@vcUnitName,@tintUnitType,@bitEnabled
	)
	
	SELECT @numUOMId = SCOPE_IDENTITY();
END
ELSE 
BEGIN
	UPDATE UOM SET 
		[vcUnitName] = @vcUnitName,
		[tintUnitType] = @tintUnitType,
		bitEnabled = @bitEnabled
	WHERE [numUOMId] = @numUOMId
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageWorkFlowMaster')
DROP PROCEDURE USP_ManageWorkFlowMaster
GO
Create PROCEDURE [dbo].[USP_ManageWorkFlowMaster]                                        
	@numWFID numeric(18, 0) OUTPUT,
    @vcWFName varchar(200),
    @vcWFDescription varchar(500),   
    @numDomainID numeric(18, 0),
    @numUserCntID numeric(18, 0),
    @bitActive bit,
    @numFormID NUMERIC(18,0),
    @tintWFTriggerOn NUMERIC(18),
    @vcWFAction varchar(Max),
    @vcDateField VARCHAR(MAX),
    @intDays INT,
    @intActionOn INT,
	@bitCustom bit,
	@numFieldID numeric(18),
    @strText TEXT = ''
as                 
DECLARE @ErrorMessage NVARCHAR(4000);SET @ErrorMessage=''
DECLARE @ErrorSeverity INT;
DECLARE @ErrorState INT;
		
BEGIN TRANSACTION;

BEGIN TRY
	DECLARE @hDocItems INT                                                                                                                                                                
	EXEC sp_xml_preparedocument @hDocItems OUTPUT, @strText


	DECLARE @TEMP TABLE
	(
		tintActionType TINYINT,
		numBizDocTypeID NUMERIC(18,0)
	)
	INSERT INTO 
		@TEMP
	SELECT 
		tintActionType,
		numBizDocTypeID 
	FROM 
		OPENXML (@hDocItems, '/NewDataSet/WorkFlowActionList',2) 
	WITH 
		(
			tintActionType TINYINT, 
			numBizDocTypeID numeric(18,0)
		)

	IF (SELECT COUNT(*) FROM @TEMP WHERE tintActionType = 6 AND (numBizDocTypeID=287 OR numBizDocTypeID=29397)) > 0
		BEGIN
			--CHECK IF CREATE INVOICE OR CREATE PACKING SLIP RULE EXISTS IN MASS SALES FULLFILLMENT
			IF (
				SELECT 
					COUNT(*) 
				FROM 
					OpportunityAutomationRules 
				WHERE 
					numDomainID = @numDomainID AND 
					(numRuleID = 1 OR numRuleID = 2)
				) > 0
			BEGIN
				RAISERROR ('MASS SALES FULLFILLMENT RULE EXISTS',16,1);
				RETURN -1
			END
		END


	IF @numWFID=0
	BEGIN
	
		INSERT INTO [dbo].[WorkFlowMaster] ([numDomainID], [vcWFName], [vcWFDescription], [numCreatedBy], [numModifiedBy], [dtCreatedDate], [dtModifiedDate], [bitActive], [numFormID], [tintWFTriggerOn],[vcWFAction],[vcDateField],[intDays],[intActionOn],[bitCustom],[numFieldID])
		SELECT @numDomainID, @vcWFName, @vcWFDescription, @numUserCntID, @numUserCntID, GETUTCDATE(), GETUTCDATE(), @bitActive, @numFormID, @tintWFTriggerOn,@vcWFAction,@vcDateField,@intDays,@intActionOn,@bitCustom,@numFieldID

		SET @numWFID=@@IDENTITY
		--start of my
		IF DATALENGTH(@strText)>2
		BEGIN                                                                                                         
			--Delete records into WorkFlowTriggerFieldList and Insert
			DELETE FROM WorkFlowTriggerFieldList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowTriggerFieldList (numWFID,numFieldID,bitCustom) 
				SELECT @numWFID,numFieldID,bitCustom
				  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowTriggerFieldList',2) WITH ( numFieldID NUMERIC, bitCustom BIT )
	
	
			--Delete records into WorkFlowConditionList and Insert
			DELETE FROM WorkFlowConditionList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowConditionList (numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare) 
				SELECT @numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare
				  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowConditionList',2) WITH ( numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10),vcFilterANDOR VARCHAR(10),vcFilterData VARCHAR(MAX),intCompare int)


			--Delete records into WorkFlowActionUpdateFields and Insert
			DELETE FROM WorkFlowActionUpdateFields WHERE numWFActionID IN(SELECT numWFActionID FROM WorkFlowActionList WHERE numWFID=@numWFID)

			--Delete records into WorkFlowActionList and Insert
			DELETE FROM WorkFlowActionList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowActionList (numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval,tintSendMail,numBizDocTypeID,numBizDocTemplateID,vcSMSText,vcEmailSendTo,vcMailBody,vcMailSubject) 
				SELECT @numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval ,tintSendMail,numBizDocTypeID,numBizDocTemplateID,vcSMSText,vcEmailSendTo,vcMailBody ,vcMailSubject
				  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowActionList',2) WITH ( tintActionType TINYINT, numTemplateID NUMERIC,vcEmailToType VARCHAR(50),tintTicklerActionAssignedTo TINYINT,vcAlertMessage NTEXT,tintActionOrder TINYINT,tintIsAttachment TINYINT,vcApproval VARCHAR(100),tintSendMail TINYINT,numBizDocTypeID numeric(18,0),numBizDocTemplateID numeric(18,0),vcSMSText NTEXT,vcEmailSendTo varchar(500),vcMailBody ntext,vcMailSubject varchar(500))

			INSERT INTO dbo.WorkFlowActionUpdateFields (numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData) 
				SELECT WFA.numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData
				  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowActionUpdateFields',2) WITH ( numWFActionID NUMERIC, tintModuleType NUMERIC,numFieldID NUMERIC,bitCustom BIT,vcValue VARCHAR(500),vcData VARCHAR(500) ) WFAUF
				  JOIN WorkFlowActionList WFA ON WFA.tintActionOrder=WFAUF.numWFActionID WHERE WFA.numWFID=@numWFID

			EXEC sp_xml_removedocument @hDocItems
		END	
		--end of my
	END
	ELSE
	BEGIN
	
		UPDATE [dbo].[WorkFlowMaster]
		SET    [vcWFName] = @vcWFName, [vcWFDescription] = @vcWFDescription, [numModifiedBy] = @numUserCntID, [dtModifiedDate] = GETUTCDATE(), [bitActive] = @bitActive, [numFormID] = @numFormID, [tintWFTriggerOn] = @tintWFTriggerOn,[vcWFAction]=@vcWFAction,[vcDateField]=@vcDateField,[intDays]=@intDays,[intActionOn]=@intActionOn,[bitCustom]=@bitCustom,[numFieldID]=@numFieldID
		WHERE  [numDomainID] = @numDomainID AND [numWFID] = @numWFID
	
		IF DATALENGTH(@strText)>2
		BEGIN
			DECLARE @hDocItem INT                                                                                                                                                                
	
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strText                                                                                                             
        
			--Delete records into WorkFlowTriggerFieldList and Insert
			DELETE FROM WorkFlowTriggerFieldList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowTriggerFieldList (numWFID,numFieldID,bitCustom) 
				SELECT @numWFID,numFieldID,bitCustom
				  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowTriggerFieldList',2) WITH ( numFieldID NUMERIC, bitCustom BIT )
	
	
			--Delete records into WorkFlowConditionList and Insert
			DELETE FROM WorkFlowConditionList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowConditionList (numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare) 
				SELECT @numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare
				  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowConditionList',2) WITH ( numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10),vcFilterANDOR VARCHAR(10),vcFilterData varchar(MAX),intCompare int)


			--Delete records into WorkFlowActionUpdateFields and Insert
			DELETE FROM WorkFlowActionUpdateFields WHERE numWFActionID IN(SELECT numWFActionID FROM WorkFlowActionList WHERE numWFID=@numWFID)

			--Delete records into WorkFlowActionList and Insert
			DELETE FROM WorkFlowActionList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowActionList (numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval ,tintSendMail,numBizDocTypeID ,numBizDocTemplateID,vcSMSText,vcEmailSendTo,vcMailBody,vcMailSubject ) 
				SELECT @numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval ,tintSendMail,numBizDocTypeID ,numBizDocTemplateID,vcSMSText ,vcEmailSendTo,vcMailBody,vcMailSubject
				  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowActionList',2) WITH ( tintActionType TINYINT, numTemplateID NUMERIC,vcEmailToType VARCHAR(50),tintTicklerActionAssignedTo TINYINT,vcAlertMessage NTEXT,tintActionOrder TINYINT,tintIsAttachment TINYINT,vcApproval VARCHAR(100),tintSendMail TINYINT,numBizDocTypeID numeric(18,0),numBizDocTemplateID numeric(18,0),vcSMSText NTEXT ,vcEmailSendTo varchar(500),vcMailBody ntext,vcMailSubject varchar(500))

			INSERT INTO dbo.WorkFlowActionUpdateFields (numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData) 
				SELECT WFA.numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData
				  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowActionUpdateFields',2) WITH ( numWFActionID NUMERIC, tintModuleType NUMERIC,numFieldID NUMERIC,bitCustom BIT,vcValue VARCHAR(500),vcData VARCHAR(500)) WFAUF
				  JOIN WorkFlowActionList WFA ON WFA.tintActionOrder=WFAUF.numWFActionID WHERE WFA.numWFID=@numWFID

			EXEC sp_xml_removedocument @hDocItem
		END	
	END
END TRY
BEGIN CATCH

	SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

	IF @@TRANCOUNT > 0
	ROLLBACK TRANSACTION;

	-- Use RAISERROR inside the CATCH block to return error
	-- information about the original error that caused
	-- execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState);
END CATCH;

IF @@TRANCOUNT > 0
	COMMIT TRANSACTION;


	

	


	
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MoveSite')
DROP PROCEDURE USP_MoveSite
GO
CREATE PROCEDURE [dbo].[USP_MoveSite]
    @numSiteID NUMERIC(9) ,
    @numFromDomainID NUMERIC(9) ,
    @numToDomainID NUMERIC(9)
AS
    BEGIN

        BEGIN TRY 
            BEGIN TRAN

    
            UPDATE  Category
            SET     numDomainID = @numToDomainID
            WHERE   numDomainID = @numFromDomainID
            UPDATE  RedirectConfig
            SET     numDomainID = @numToDomainID
            WHERE   [numSiteID] = @numSiteID
                    AND numDomainID = @numFromDomainID
            UPDATE  [SiteBreadCrumb]
            SET     numDomainID = @numToDomainID
            WHERE   [numSiteID] = @numSiteID
                    AND numDomainID = @numFromDomainID
            UPDATE  [StyleSheets]
            SET     numDomainID = @numToDomainID
            WHERE   [numSiteID] = @numSiteID
                    AND numDomainID = @numFromDomainID
 --update [StyleSheetDetails] set numDomainID
            UPDATE  [SiteTemplates]
            SET     numDomainID = @numToDomainID
            WHERE   [numSiteID] = @numSiteID
                    AND numDomainID = @numFromDomainID
            UPDATE  [PageElementDetail]
            SET     numDomainID = @numToDomainID
            WHERE   [numSiteID] = @numSiteID
                    AND numDomainID = @numFromDomainID
            UPDATE  [SiteMenu]
            SET     numDomainID = @numToDomainID
            WHERE   [numSiteID] = @numSiteID
                    AND numDomainID = @numFromDomainID
            UPDATE  [SitePages]
            SET     numDomainID = @numToDomainID
            WHERE   [numSiteID] = @numSiteID
                    AND numDomainID = @numFromDomainID
 
 --update [SiteCategories] set numDomainID= @numToDomainID    WHERE       [numSiteID] = @numSiteID and numDomainID=@numFromDomainID
 
            UPDATE  Sites
            SET     numDomainID = @numToDomainID
            WHERE   [numSiteID] = @numSiteID
                    AND numDomainID = @numFromDomainID
 
 --update Ratings set numDomainID= @numToDomainID    WHERE       [numSiteID] = @numSiteID and numDomainID=@numFromDomainID
 --update Review set numDomainID= @numToDomainID    WHERE       [numSiteID] = @numSiteID and numDomainID=@numFromDomainID
 
  DELETE FROM [dbo].[eCommerceDTL] WHERE [eCommerceDTL].[numDomainId] = @numFromDomainID
  AND [eCommerceDTL].[numSiteId] = @numSiteID
 
            COMMIT TRAN
        END TRY 
        BEGIN CATCH  
            IF ( @@TRANCOUNT > 0 )
                BEGIN
                    ROLLBACK TRAN
                    DECLARE @error VARCHAR(1000)
                    SET @error = ERROR_MESSAGE();
                    RAISERROR (@error, 16, 1 );
                    RETURN 1
                END
        END CATCH 
    
                     
    END
/****** Object:  StoredProcedure [dbo].[USP_OPPDetails]    Script Date: 03/25/2009 15:10:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By Anoop Jayaraj
-- [dbo].[USP_OPPDetails] 1362,1,-333
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppdetails')
DROP PROCEDURE usp_oppdetails
GO
CREATE PROCEDURE [dbo].[USP_OPPDetails]
(
               @numOppID             AS NUMERIC(9)  = NULL,
               @numDomainID          AS NUMERIC(9)  = 0,
               @ClientTimeZoneOffset AS INT)
AS
	
  SELECT Opp.numoppid,
         numCampainID,
         ADC.vcFirstname + ' ' + ADC.vcLastName AS numContactId,
         isnull(ADC.vcEmail,'') AS vcEmail,
         Opp.txtComments,
         Opp.numDivisionID,
         tintOppType,
         ISNULL(dateadd(MINUTE,-@ClientTimeZoneOffset,bintAccountClosingDate),'') AS bintAccountClosingDate,
         Opp.numContactID AS ContactID,
         tintSource,
         C2.vcCompanyName + Case when isnull(D2.numCompanyDiff,0)>0 then  '  ' + dbo.fn_getlistitemname(D2.numCompanyDiff) + ':' + isnull(D2.vcCompanyDiff,'') else '' end as vcCompanyname,
         D2.tintCRMType,
         Opp.vcPoppName,
         intpEstimatedCloseDate,
         [dbo].[GetDealAmount](@numOppID,getutcdate(),0) AS monPAmount,
         lngPConclAnalysis,
         monPAmount AS OppAmount,
         numSalesOrPurType,
         Opp.tintActive,
         dbo.fn_GetContactName(Opp.numCreatedby)
           + ' '
           + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintCreatedDate)) AS CreatedBy,
         dbo.fn_GetContactName(Opp.numModifiedBy)
           + ' '
           + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintModifiedDate)) AS ModifiedBy,
         dbo.fn_GetContactName(Opp.numRecOwner) AS RecordOwner,
         Opp.numRecOwner,
         isnull(tintshipped,0) AS tintshipped,
         isnull(tintOppStatus,0) AS tintOppStatus,
         dbo.OpportunityLinkedItems(@numOppID) AS NoOfProjects,
         Opp.numAssignedTo,
         Opp.numAssignedBy,
         (SELECT COUNT(* )
          FROM   dbo.GenericDocuments
          WHERE  numRecID = @numOppID
                 AND vcDocumentSection = 'O') AS DocumentCount,
         isnull(OPR.numRecurringId,0) AS numRecurringId,
		 OPR.dtRecurringDate AS dtLastRecurringDate,
         D2.numTerID,
         Link.numOppID AS numParentOppID,
         Link.vcPoppName AS ParentOppName,
         Link.vcSource AS Source,
         Link.numParentProjectID,
         Link.vcProjectName,
         ISNULL(C.varCurrSymbol,'') varCurrSymbol,
         ISNULL(Opp.fltExchangeRate,0) AS [fltExchangeRate],
         ISNULL(C.chrCurrency,'') AS [chrCurrency],
         ISNULL(C.numCurrencyID,0) AS [numCurrencyID],
		 (Select Count(*) from OpportunityBizDocs 
		 where numOppId=@numOppID and bitAuthoritativeBizDocs=1) As AuthBizDocCount,
		 (SELECT COUNT(distinct numChildOppID) FROM [OpportunityLinking] OL WHERE OL.[numParentOppID] = Opp.numOppId) AS ChildCount,
		 (SELECT COUNT(*) FROM [RecurringTransactionReport] RTR WHERE RTR.[numRecTranSeedId] = Opp.numOppId)  AS RecurringChildCount
		 ,Opp.bintCreatedDate,
		 ISNULL(Opp.bitStockTransfer ,0) bitStockTransfer,	ISNULL(Opp.tintTaxOperator,0) AS tintTaxOperator, 
         isnull(Opp.intBillingDays,0) AS numBillingDays,isnull(Opp.bitInterestType,0) AS bitInterestType,
         isnull(opp.fltInterest,0) AS fltInterest,  isnull(Opp.fltDiscount,0) AS fltDiscount,
         isnull(Opp.bitDiscountType,0) AS bitDiscountType,ISNULL(Opp.bitBillingTerms,0) AS bitBillingTerms,ISNULL(OPP.numDiscountAcntType,0) AS numDiscountAcntType,
         (SELECT TOP 1 numCountry FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)) AS [numShipCountry],
		 (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)) AS [numShipState],
		 (SELECT TOP 1 vcState FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)) AS [vcShipState],
		 (SELECT TOP 1 vcCountry FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)) AS [vcShipCountry],
		 (SELECT TOP 1 vcPostalCode FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)) AS [vcPostalCode],
		 ISNULL(vcCouponCode,'') AS [vcCouponCode],ISNULL(Opp.bitPPVariance,0) AS bitPPVariance,
		 Opp.dtItemReceivedDate,ISNULL(bitUseShippersAccountNo,0) [bitUseShippersAccountNo], ISNULL(vcShippersAccountNo,'') [vcShippersAccountNo],
		 ISNULL(bitUseMarkupShippingRate,0) [bitUseMarkupShippingRate],
		 ISNULL(numMarkupShippingRate,0) [numMarkupShippingRate],
		 ISNULL(SR.[vcValue2],ISNULL(intUsedShippingCompany,-- [intUsedShippingCompany1],
		 (SELECT TOP 1 ISNULL([SST].[intNsoftEnum],0) FROM [dbo].[ShippingServiceTypes] AS SST 
		 WHERE [SST].[numDomainID] = @numDomainID 
		 AND [SST].[numRuleID] = 0
		 AND [SST].[vcServiceName] = (SELECT TOP 1 [OI].[vcItemDesc] FROM [dbo].[OpportunityItems] AS OI 
									 WHERE [OI].[numOppId] = [Opp].[numOppId] 
									 AND [OI].[vcType] = 'Service Item')))) AS [intUsedShippingCompany],
		 ISNULL(Opp.[monLandedCostTotal],0) AS monLandedCostTotal,opp.[vcLanedCost],
		 ISNULL(Opp.vcRecurrenceType,'') AS vcRecurrenceType,
		 ISNULL(RecurrenceConfiguration.numRecConfigID,0) AS numRecConfigID,
		 (CASE WHEN ISNULL(Opp.vcRecurrenceType,'') = '' THEN 0 ELSE 1 END) AS bitRecur,
		 (CASE WHEN RecurrenceTransaction.numRecTranID IS NULL THEN 0 ELSE 1 END) AS bitRecurred,
		 dbo.FormatedDateFromDate(RecurrenceConfiguration.dtStartDate,@numDomainID) AS dtStartDate,
		 dbo.FormatedDateFromDate(RecurrenceConfiguration.dtEndDate,@numDomainID) AS dtEndDate,
		 RecurrenceConfiguration.vcFrequency AS vcFrequency,
		 ISNULL(RecurrenceConfiguration.bitDisabled,0) AS bitDisabled
  FROM   OpportunityMaster Opp 
         JOIN divisionMaster D2
           ON Opp.numDivisionID = D2.numDivisionID
         LEFT JOIN CompanyInfo C2
           ON C2.numCompanyID = D2.numCompanyID
         LEFT JOIN (SELECT TOP 1 OM.vcPoppName,
                                 OM.numOppID,
                                 numChildOppID,
                                 vcSource,
                                 numParentProjectID,
                                 vcProjectName
                    FROM   OpportunityLinking
                          LEFT JOIN OpportunityMaster OM
                             ON numOppID = numParentOppID
                          LEFT JOIN [ProjectsMaster]
							 ON numParentProjectID = numProId   
                    WHERE  numChildOppID = @numOppID) Link
           ON Link.numChildOppID = Opp.numOppID
         LEFT OUTER JOIN [Currency] C
					ON C.numCurrencyID = Opp.numCurrencyID
		 LEFT OUTER JOIN [OpportunityRecurring] OPR
					ON OPR.numOppId = Opp.numOppId
		 LEFT OUTER JOIN additionalContactsinformation ADC ON
		 ADC.numdivisionId = D2.numdivisionId AND  ADC.numContactId = Opp.numContactId	
		 LEFT JOIN RecurrenceConfiguration ON Opp.numOppId = RecurrenceConfiguration.numOppID AND RecurrenceConfiguration.numType = 1
		 LEFT JOIN RecurrenceTransaction ON Opp.numOppId = RecurrenceTransaction.numRecurrOppID
		 LEFT JOIN [dbo].[ShippingReport] AS SR ON SR.[numOppID] = [Opp].[numOppId] AND SR.[numDomainID] = [Opp].[numDomainId]
		WHERE  Opp.numOppId = @numOppID
         AND Opp.numDomainID = @numDomainID
/****** Object:  StoredProcedure [dbo].[USP_OPPItemDetails]    Script Date: 09/01/2009 18:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_OPPItemDetails @numItemCode=31455,@OpportunityId=12387,@numDomainId=110
--created by anooop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppitemdetails')
DROP PROCEDURE usp_oppitemdetails
GO
CREATE PROCEDURE [dbo].[USP_OPPItemDetails]
    (
      @numUserCntID NUMERIC(9) = 0,
      @OpportunityId NUMERIC(9) = 0,
      @numDomainId NUMERIC(9),
      @ClientTimeZoneOffset  INT
    )
AS 
    BEGIN
        IF @OpportunityId >0
            BEGIN
                DECLARE @DivisionID AS NUMERIC(9)
                DECLARE @TaxPercentage AS FLOAT
                DECLARE @tintOppType AS TINYINT
                DECLARE @fld_Name AS VARCHAR(500)
                DECLARE @strSQL AS VARCHAR(8000)
                DECLARE @fld_ID AS NUMERIC(9)
                DECLARE @numBillCountry AS NUMERIC(9)
                DECLARE @numBillState AS NUMERIC(9)

                
                SELECT  @DivisionID = numDivisionID,
                        @tintOppType = tintOpptype
                FROM    OpportunityMaster
                WHERE   numOppId = @OpportunityId


               -- Calculate Tax based on Country and state
             /*  SELECT  @numBillState = ISNULL(numState, 0),
						@numBillCountry = ISNULL(numCountry, 0)
			   FROM     dbo.AddressDetails
			   WHERE    numDomainID = @numDomainID
						AND numRecordID = @DivisionID
						AND tintAddressOf = 2
						AND tintAddressType = 1
						AND bitIsPrimary=1
                SET @TaxPercentage = 0
                IF @numBillCountry > 0
                    AND @numBillState > 0 
                    BEGIN
                        IF @numBillState > 0 
                            BEGIN
                                IF EXISTS ( SELECT  COUNT(*)
                                            FROM    TaxDetails
                                            WHERE   numCountryID = @numBillCountry
                                                    AND numStateID = @numBillState
                                                    AND numDomainID = @numDomainID ) 
                                    SELECT  @TaxPercentage = decTaxPercentage
                                    FROM    TaxDetails
                                    WHERE   numCountryID = @numBillCountry
                                            AND numStateID = @numBillState
                                            AND numDomainID = @numDomainID
                                ELSE 
                                    SELECT  @TaxPercentage = decTaxPercentage
                                    FROM    TaxDetails
                                    WHERE   numCountryID = @numBillCountry
                                            AND numStateID = 0
                                            AND numDomainID = @numDomainID
                            END
                    END
                ELSE 
                    IF @numBillCountry > 0 
                        SELECT  @TaxPercentage = decTaxPercentage
                        FROM    TaxDetails
                        WHERE   numCountryID = @numBillCountry
                                AND numStateID = 0
                                AND numDomainID = @numDomainID
               
               */
               

SET @strSQL = ''

--------------Make Dynamic Query--------------
SET @strSQL = @strSQL + '
Select 
Opp.numoppitemtCode,
ISNULL(Opp.bitItemPriceApprovalRequired,0) AS bitItemPriceApprovalRequired,
OM.tintOppType,
Opp.numItemCode,
I.charItemType,
ISNULL(I.bitAsset,0) as bitAsset,
ISNULL(I.bitRental,0) as bitRental,
Opp.numOppId,
Opp.bitWorkOrder,
Opp.fltDiscount,
ISNULL(( SELECT numReturnID FROM   Returns WHERE  numOppItemCode = Opp.numoppitemtCode AND numOppId = Opp.numOppID), 0) AS numReturnID,
dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) AS ReturrnedQty,
[dbo].fn_GetReturnStatus(Opp.numoppitemtCode) AS ReturnStatus,
ISNULL(opp.numUOMId, 0) AS numUOM,
(SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
Opp.numUnitHour AS numOriginalUnitHour, 
isnull(M.vcPOppName,''Internal'') as Source,
numSourceID,
  CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END as bitKitParent,
(CASE WHEN (I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)THEN dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,isnull(Opp.numWarehouseItmsID,0),(CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END)) ELSE 0 END) AS bitBackOrder,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(Opp.numQtyShipped,0) numQtyShipped,ISNULL(Opp.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(OM.fltExchangeRate,0) AS fltExchangeRate,ISNULL(OM.numCurrencyID,0) AS numCurrencyID,ISNULL(OM.numDivisionID,0) AS numDivisionID,
isnull(i.numIncomeChartAcntId,0) as itemIncomeAccount,isnull(i.numAssetChartAcntId,0) as itemInventoryAsset,isnull(i.numCOGsChartAcntId,0) as itemCoGs,
ISNULL(Opp.numRentalIN,0) as numRentalIN,
ISNULL(Opp.numRentalLost,0) as numRentalLost,
ISNULL(Opp.numRentalOut,0) as numRentalOut,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(bitFreeShipping,0) AS [IsFreeShipping],Opp.bitDiscountType,isnull(Opp.monLandedCost,0) as monLandedCost, '

--	IF @fld_Name = 'vcPathForTImage' 
		SET @strSQL = @strSQL + 'Opp.vcPathForTImage,'
--	ELSE IF @fld_Name = 'vcItemName' 
		SET @strSQL = @strSQL + 'Opp.vcItemName,'
--	ELSE IF @fld_Name = 'vcModelID,' 
		SET @strSQL = @strSQL + 'Opp.vcModelID,'
--	ELSE IF @fld_Name = 'vcWareHouse' 
		SET @strSQL = @strSQL + 'vcWarehouse,'
		SET @strSQL = @strSQL + 'WItems.numOnHand,isnull(WItems.numAllocation,0) as numAllocation,'
		SET @strSQL = @strSQL + 'WL.vcLocation,'
--	ELSE IF @fld_Name = 'ItemType' 
		SET @strSQL = @strSQL + 'Opp.vcType ItemType,Opp.vcType,'
--	ELSE IF @fld_Name = 'vcItemDesc' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.vcItemDesc, '''') vcItemDesc,'
--	ELSE IF @fld_Name = 'Attributes' 
		SET @strSQL = @strSQL + 'Opp.vcAttributes,' --dbo.fn_GetAttributes(Opp.numWarehouseItmsID, I.bitSerialized) AS Attributes,
--	ELSE IF @fld_Name = 'bitDropShip' 
		SET @strSQL = @strSQL + 'ISNULL(bitDropShip, 0) as DropShip,CASE ISNULL(bitDropShip, 0) WHEN 1 THEN ''Yes'' ELSE '''' END AS bitDropShip,'
--	ELSE IF @fld_Name = 'numUnitHour' 
		SET @strSQL = @strSQL + 'CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,om.numDomainId, ISNULL(opp.numUOMId, 0))* Opp.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,'
--	ELSE IF @fld_Name = 'monPrice' 
		SET @strSQL = @strSQL + 'Opp.monPrice,'
--	ELSE IF @fld_Name = 'monTotAmount' 
		SET @strSQL = @strSQL + 'Opp.monTotAmount,'
--	ELSE IF @fld_Name = 'vcSKU' 
		SET @strSQL = @strSQL + 'ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'''')) [vcSKU],'
--	ELSE IF @fld_Name = 'vcManufacturer' 
		SET @strSQL = @strSQL + 'Opp.vcManufacturer,'
--	ELSE IF @fld_Name = 'vcItemClassification' 
		SET @strSQL = @strSQL + ' dbo.[fn_GetListItemName](I.[numItemClassification]) numItemClassification,'
--	ELSE IF @fld_Name = 'numItemGroup' 
		SET @strSQL = @strSQL + '( SELECT    [vcItemGroup] FROM      [ItemGroups] WHERE     [numItemGroupID] = I.numItemGroup ) numItemGroup,'
--	ELSE IF @fld_Name = 'vcUOMName' 
		SET @strSQL = @strSQL + 'ISNULL(u.vcUnitName, '''') numUOMId,'
--	ELSE IF @fld_Name = 'fltWeight' 
		SET @strSQL = @strSQL + 'I.fltWeight,'
--	ELSE IF @fld_Name = 'fltHeight' 
		SET @strSQL = @strSQL + 'I.fltHeight,'
--	ELSE IF @fld_Name = 'fltWidth' 
		SET @strSQL = @strSQL + 'I.fltWidth,'
--	ELSE IF @fld_Name = 'fltLength' 
		SET @strSQL = @strSQL + 'I.fltLength,'
--	ELSE IF @fld_Name = 'numBarCodeId' 
		SET @strSQL = @strSQL + 'I.numBarCodeId,'
		SET @strSQL = @strSQL + 'I.IsArchieve,'
--	ELSE IF @fld_Name = 'vcWorkOrderStatus' 
		SET @strSQL = @strSQL + 'CASE WHEN ISNULL(Opp.bitWorkOrder, 0) = 1
				 THEN ( SELECT  dbo.GetListIemName(numWOStatus)
								+ CASE WHEN ISNULL(numWOStatus, 0) = 23184
									   THEN '' - ''
											+ CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo),
														  '''') + '',''
											+ CONVERT(VARCHAR(20), DATEADD(minute, '+ CONVERT(VARCHAR(15),-@ClientTimeZoneOffset)  +', bintCompletedDate)) AS VARCHAR(100))
									   ELSE ''''
								  END
						FROM    WorkOrder
						WHERE   ISNULL(numOppId, 0) = Opp.numOppId
								AND numItemCode = Opp.numItemCode
					  )
				 ELSE ''''
			END AS vcWorkOrderStatus,'
--	ELSE IF @fld_Name = 'DiscAmt' 
		SET @strSQL = @strSQL + 'CASE WHEN Opp.bitDiscountType = 0
				 THEN CAST(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount AS VARCHAR(10)) + '' (''
					  + CAST(Opp.fltDiscount AS VARCHAR(10)) + ''%)''
				 ELSE CAST(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount AS VARCHAR(10))
			END AS DiscAmt,'	
--	ELSE IF @fld_Name = 'numProjectID' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectID, 0) numProjectID,
		 CASE WHEN ( SELECT TOP 1
								ISNULL(OBI.numOppBizDocItemID, 0)
						FROM    dbo.OpportunityBizDocs OBD
								INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID
						WHERE   OBI.numOppItemID = Opp.numoppitemtCode
								AND ISNULL(OBD.bitAuthoritativeBizDocs, 0) = 1
								and isnull(OBD.numOppID,0)=OM.numOppID
					  ) > 0 THEN 1
				 ELSE 0
			END AS bitItemAddedToAuthBizDoc,'	
--	ELSE IF @fld_Name = 'numClassID' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.numClassID, 0) numClassID,'	
--	ELSE IF @fld_Name = 'vcProjectStageName' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectStageID, 0) numProjectStageID,
				CASE WHEN OPP.numProjectStageID > 0
				 THEN dbo.fn_GetProjectStageName(OPP.numProjectId,
												 OPP.numProjectStageID)
				 WHEN OPP.numProjectStageID = -1 THEN ''T&E''
				 WHEN OPP.numProjectId > 0
				 THEN CASE WHEN OM.tintOppType = 1 THEN ''S.O.''
						   ELSE ''P.O.''
					  END
				 ELSE ''-''
			END AS vcProjectStageName,'	
	        
	
	

--------------Add custom fields to query----------------
SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM View_DynamicCustomColumns
WHERE 
grp_id = 5 AND numDomainID=@numDomainId
AND numFormId=26 AND bitCustom=1 AND numUserCntID = @numUserCntID ORDER BY numFieldId
WHILE @fld_ID > 0
BEGIN ------------------

	 PRINT @fld_Name
	
	       SET @strSQL = @strSQL + 'dbo.GetCustFldValueItem('
                                    + CONVERT(VARCHAR(15), @fld_ID)
                                    + ',Opp.numItemCode) as ''' + @fld_Name + ''','
       

SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM View_DynamicCustomColumns
WHERE 
grp_id = 5 AND numDomainID=@numDomainId
AND numFormId=26 AND bitCustom=1  AND numUserCntID = @numUserCntID AND numFieldId > @fld_ID ORDER BY numFieldId
 IF @@ROWCOUNT=0
 SET @fld_ID = 0
END ------------------


                                
--Purchase Opportunity ?
IF @tintOppType = 2 
    BEGIN
        SET @strSQL =  @strSQL + 'ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo,
		  CAST(Opp.numUnitHour AS NUMERIC(9, 0)) numQty,
		  (SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  isnull(Opp.numWarehouseItmsID,0) as numWarehouseItmsID,'
	END
ELSE
	BEGIN	
        SET @strSQL =  @strSQL + ' ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo,
		   CAST(Opp.numUnitHour AS NUMERIC(9, 0)) numQty,
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  isnull(Opp.numWarehouseItmsID,0) as numWarehouseItmsID,'
	END


        
 SET @strSQL = left(@strSQL,len(@strSQL)-1) + '   
        FROM    OpportunityItems Opp
        LEFT JOIN OpportunityMaster OM ON Opp.numOppID = oM.numOppID
        JOIN item I ON Opp.numItemCode = i.numItemcode
        LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = Opp.numWarehouseItmsID
        LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
        LEFT JOIN WarehouseLocation WL ON WL.numWLocationID = WItems.numWLocationID
        LEFT JOIN OpportunityMaster M ON Opp.numSourceID = M.numOppID
        LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId
WHERE   om.numDomainId = '+ CONVERT(VARCHAR(10),@numDomainId) + '
        AND Opp.numOppId = '+ CONVERT(VARCHAR(15),@OpportunityId) +'
ORDER BY numoppitemtCode ASC '
  
PRINT @strSQL;
EXEC (@strSQL);
SELECT  vcSerialNo,
       vcComments AS Comments,
        numOppItemID AS numoppitemtCode,
        W.numWarehouseItmsDTLID,
        numWarehouseItmsID AS numWItmsID,
        dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                             1) Attributes
FROM    WareHouseItmsDTL W
        JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
WHERE   numOppID = @OpportunityId


CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,intVisible int,vcFieldDataType CHAR(1))


IF
	(
		SELECT 
			COUNT(*)
		FROM 
			View_DynamicColumns DC 
		WHERE 
			DC.numFormId=26 and DC.numDomainID=@numDomainID AND numUserCntID = @numUserCntID AND
			ISNULL(DC.bitSettingField,0)=1 AND ISNULL(DC.bitDeleted,0)=0 AND ISNULL(DC.bitCustom,0)=0) > 0
	OR
	(
		SELECT 
			COUNT(*)
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=26 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0 AND ISNULL(bitCustom,0)=1
	) > 0
BEGIN	

INSERT INTO #tempForm
select isnull(DC.tintRow,0)+1 as tintOrder,DDF.vcDbColumnName,DDF.vcOrigDbColumnName,ISNULL(DDF.vcCultureFieldName,DDF.vcFieldName),                  
 DDF.vcAssociatedControlType,DDF.vcListItemType,DDF.numListID,DDF.vcLookBackTableName                                                 
,DDF.bitCustom,DDF.numFieldId,DDF.bitAllowSorting,DDF.bitInlineEdit,
DDF.bitIsRequired,DDF.bitIsEmail,DDF.bitIsAlphaNumeric,DDF.bitIsNumeric,DDF.bitIsLengthValidation,
DDF.intMaxLength,DDF.intMinLength,DDF.bitFieldMessage,DDF.vcFieldMessage vcFieldMessage,DDF.ListRelID,DDF.intColumnWidth,Case when DC.numFieldID is null then 0 else 1 end,DDF.vcFieldDataType
 FROM View_DynamicDefaultColumns DDF left join View_DynamicColumns DC on DC.numFormId=26 and DDF.numFieldId=DC.numFieldId and DC.numUserCntID=@numUserCntID and DC.numDomainID=@numDomainID and numRelCntType=0 AND tintPageType=1 
 where DDF.numFormId=26 and DDF.numDomainID=@numDomainID AND ISNULL(DDF.bitSettingField,0)=1 AND ISNULL(DDF.bitDeleted,0)=0 AND ISNULL(DDF.bitCustom,0)=0
       
 UNION
    
    select tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
 from View_DynamicCustomColumns
 where numFormId=26 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0
 AND ISNULL(bitCustom,0)=1

END
ELSE
BEGIN
	--Check if Master Form Configuration is created by administrator if NoColumns=0
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 26 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END

	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName, vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
			bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,1,vcFieldDataType
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=26 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
			 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
			,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=26 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
END


select * from #tempForm order by tintOrder

drop table #tempForm

/*
                        SET @strSQL = 'select  Opp.numoppitemtCode,Opp.numOppId,
                               Opp.numItemCode,                                    
                               Opp.vcItemName,                                                                    
                               ISNULL(Opp.vcItemDesc,'''') as [Desc],                                                                    
                               Opp.vcType ItemType,                                   
                               Opp.vcPathForTImage,
                               Opp.vcModelID,
							    CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),Opp.numItemCode,om.numDomainId,ISNULL(opp.numUOMId,0)) * Opp.numUnitHour) as numeric(18,2)) as numUnitHour,   
                               Opp.monPrice,                                                                    
                               Opp.monTotAmount,                                                     
							   ISNULL(Opp.vcItemDesc,'''') vcItemDesc,
							   Opp.vcManufacturer,
							   I.charItemType,
							   I.bitKitParent,
							   I.bitAssembly,
							   I.vcSKU,                                   
							   I.numBarCodeId,
							   dbo.[fn_GetListItemName](I.[numItemClassification]) vcClassification,
							   (SELECT [vcItemGroup] FROM [ItemGroups] WHERE [numItemGroupID] =I.numItemGroup) vcItemGroup,
							   ISNULL(I.vcUnitofMeasure,'''') vcUnitofMeasure,
							   I.fltWeight,I.fltHeight,I.fltWidth,I.fltLength,
							   WItems.numWarehouseID,
                               Opp.numWarehouseItmsID,isnull(Opp.numToWarehouseItemID,0) numToWarehouseItemID,bitDropShip DropShip, CASE ISNULL(bitDropShip,0) when 1 then ''Yes'' else '''' end as vcDropShip,
                               0 as Op_Flag,isnull(Opp.bitDiscountType,0) as bitDiscountType,isnull(Opp.fltDiscount,0) as fltDiscount,isnull(monTotAmtBefDiscount,0) as monTotAmtBefDiscount,                                    
                               vcWarehouse as Warehouse,I.bitSerialized,isnull(I.bitLotNo,0) as bitLotNo,vcAttributes, 
                               dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) as ReturrnedQty,
                               [dbo].fn_GetReturnStatus(Opp.numoppitemtCode) as ReturnStatus,
							   isnull((SELECT numReturnID FROM Returns WHERE numOppItemCode=Opp.numoppitemtCode and numOppId=Opp.numOppID),0) as numReturnID,
                               isnull(v.monCost,0) as VendorCost,
                               bitTaxable,
                               case when bitTaxable =0 then 0 when bitTaxable=1 then '
                            + CONVERT(VARCHAR(15), @TaxPercentage)
                            + ' end as Tax,                                                
                                   isnull(M.vcPOppName,''Internal'') as Source,dbo.fn_GetAttributes(Opp.numWarehouseItmsID,I.bitSerialized) as Attributes  ,numSourceID '
                            + CASE WHEN @strSQL = '' THEN ''
                                   ELSE ',' + @strSQL
                              END
                            + ',isnull(opp.numUOMId,0) AS numUOM,ISNULL(u.vcUnitName,'''') vcUOMName,dbo.fn_UOMConversion(opp.numUOMId,Opp.numItemCode,om.numDomainId,null) AS UOMConversionFactor
                          ,case when Opp.bitDiscountType=0 then  cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) +  '' ('' + CAST(Opp.fltDiscount AS VARCHAR(10)) +  ''%)'' 
                          ELSE cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) end as DiscAmt
                           ,isnull(I.numVendorID,0) numVendorID,OM.tintOppType,cast(Opp.numUnitHour as numeric(9,0)) as numQty,SUBSTRING(
(SELECT '','' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN '' ('' + CONVERT(VARCHAR(15),whi.numQty) + '')'' ELSE '''' end 
FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=Opp.numOppId and oppI.numOppItemID=Opp.numoppitemtCode 
ORDER BY vcSerialNo FOR XML PATH('''')),2,200000) AS SerialLotNo, ISNULL(Opp.bitWorkOrder,0) as bitWorkOrder,
CASE WHEN ISNULL(Opp.bitWorkOrder,0)=1 then (select dbo.GetListIemName(numWOStatus) + CASE WHEN isnull(numWOStatus,0)=23184 THEN
'' - '' + CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo), '''')+'',''+convert(varchar(20),dateadd(minute,' + CONVERT(VARCHAR(15),-@ClientTimeZoneOffset) + ',bintCompletedDate)) AS VARCHAR(100)) ELSE '''' end
 from WorkOrder where isnull(numOppId,0)=Opp.numOppId and numItemCode=Opp.numItemCode) else '''' end as vcWorkOrderStatus,
 ISNULL(Opp.numVendorWareHouse,0) as numVendorWareHouse,ISNULL(Opp.numShipmentMethod,0) as numShipmentMethod,ISNULL(Opp.numSOVendorId,0) as numSOVendorId,Opp.numUnitHour as numOriginalUnitHour,
 isnull(Opp.numProjectID,0) numProjectID,isnull(Opp.numClassID,0) numClassID,isnull(Opp.numProjectStageID,0) numProjectStageID,Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) when OPP.numProjectStageID=-1 then ''T&E'' When OPP.numProjectId>0 then case when OM.tintOppType=1 then ''S.O.'' else ''P.O.'' end else ''-'' end as vcProjectStageName,
 CASE WHEN (SELECT TOP 1 ISNULL(OBI.numOppBizDocItemID,0) FROM dbo.OpportunityBizDocs OBD INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID WHERE OBI.numOppItemID = Opp.numoppitemtCode AND ISNULL(OBD.bitAuthoritativeBizDocs,0)=1 ) > 0
      THEN 1 ELSE 0 END AS bitItemAddedToAuthBizDoc,isnull(I.numItemClass,0) as numItemClass
                              from OpportunityItems Opp                    
                               left join OpportunityMaster OM                                                            
                                  on Opp.numOppID=oM.numOppID                                                                     
                                 join item I                                                                    
                                  on Opp.numItemCode=i.numItemcode                                            
                                left join Vendor v 
									on v.numVendorID= I.numVendorID and I.numItemCode =v.numItemCode
                               left join  WareHouseItems  WItems                                        
                               on   WItems.numWareHouseItemID= Opp.numWarehouseItmsID                                            
                               left join  Warehouses  W                                            
                                  on   W.numWarehouseID= WItems.numWarehouseID                                         
                                  left join OpportunityMaster M                                                            
                                  on Opp.numSourceID=M.numOppID  
                                  LEFT JOIN  UOM u ON u.numUOMId = opp.numUOMId  
                                  where om.numDomainId ='
                            + CONVERT(VARCHAR(15), @numDomainId)
                            + ' and Opp.numOppId='+ CONVERT(VARCHAR(15), @OpportunityId) +  
                            + CASE WHEN @numItemCode > 0 THEN ' AND Opp.numOppItemTCode = ' + CONVERT(VARCHAR(15), @numItemCode)
                                   ELSE ''
                              END
                            +
                            ' order by numoppitemtCode ASC '
                        PRINT @strSQL
                        EXEC ( @strSQL
                            )
                        SELECT  vcSerialNo,
                                vcComments AS Comments,
                                numOppItemID AS numoppitemtCode,
                                W.numWarehouseItmsDTLID,
                                numWarehouseItmsID AS numWItmsID,
                                dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                                                     1) Attributes
                        FROM    WareHouseItmsDTL W
                                JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
                        WHERE   numOppID = @OpportunityId
*/
      /*              END
                ELSE 
                    BEGIN
                        SELECT  @numDomainID = numDomainId
                        FROM    OpportunityMaster
                        WHERE   numOppID = @OpportunityId
                        SET @strSQL = ''
                        SELECT TOP 1
                                @fld_ID = Fld_ID,
                                @fld_Name = Fld_label 
                        FROM    CFW_Fld_Master
                        WHERE   grp_id = 5
                                AND numDomainID = @numDomainID
                        WHILE @fld_ID > 0
                            BEGIN
                                SET @strSQL =@strSQL + 'dbo.GetCustFldValueItem('
                                    + CONVERT(VARCHAR(15), @fld_ID)
                                    + ',Opp.numItemCode) as ''' + @fld_Name + ''''
                                SELECT TOP 1
                                        @fld_ID = Fld_ID,
                                        @fld_Name = Fld_label
                                FROM    CFW_Fld_Master
                                WHERE   grp_id = 5
                                        AND numDomainID = @numDomainID
                                        AND Fld_ID > @fld_ID
                                IF @@ROWCOUNT = 0 
                                    SET @fld_ID = 0
                                IF @fld_ID <> 0 
                                    SET @strSQL = @strSQL + ','
                            END
                        SET @strSQL = 'select  Opp.numoppitemtCode,Opp.numOppId,                                       
                                Opp.numItemCode,                                    
                               Opp.vcItemName,                                                                    
                               ISNULL(Opp.vcItemDesc,'''') as [Desc],                                                                    
                               Opp.vcType ItemType,                                   
                               Opp.vcPathForTImage,
                               Opp.vcModelID,
                              CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),Opp.numItemCode,om.numDomainId,ISNULL(opp.numUOMId,0)) * Opp.numUnitHour) as numeric(18,2)) as numUnitHour,   
							   Opp.monPrice,                                                                    
                               Opp.monTotAmount,                                                     
							   ISNULL(Opp.vcItemDesc,'''') vcItemDesc,
							   Opp.vcManufacturer,
							   I.charItemType,
							   I.bitKitParent,
							   I.bitAssembly,
							   I.vcSKU,
							   I.numBarCodeId,
							   dbo.[fn_GetListItemName](I.[numItemClassification]) vcClassification,
							   (SELECT [vcItemGroup] FROM [ItemGroups] WHERE [numItemGroupID] =I.numItemGroup) vcItemGroup,
							   ISNULL(I.vcUnitofMeasure,'''') vcUnitofMeasure,
							   I.fltWeight,I.fltHeight,I.fltWidth,I.fltLength,
							   WItems.numWarehouseID,
                               Opp.numWarehouseItmsID,isnull(Opp.numToWarehouseItemID,0) numToWarehouseItemID,bitDropShip DropShip, CASE ISNULL(bitDropShip,0) when 1 then ''Yes'' else '''' end as vcDropShip,
                               0 as Op_Flag,isnull(Opp.bitDiscountType,0) bitDiscountType,isnull(Opp.fltDiscount,0) fltDiscount,isnull(monTotAmtBefDiscount,0) monTotAmtBefDiscount,                                           
                               vcWarehouse as Warehouse,I.bitSerialized,isnull(I.bitLotNo,0) as bitLotNo,vcAttributes,
                               dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) as ReturrnedQty,
                               [dbo].fn_GetReturnStatus(Opp.numoppitemtCode) as ReturnStatus,
							   isnull((SELECT numReturnID FROM Returns WHERE numOppItemCode=Opp.numoppitemtCode and numOppId=Opp.numOppID),0) as numReturnID,
                               isnull(v.monCost,0) as VendorCost,
                               bitTaxable,
                               case when bitTaxable =0 then 0 when bitTaxable=1 then '
                            + CONVERT(VARCHAR(15), @TaxPercentage)
                            + ' end as Tax,                                                
                                   isnull(m.vcPOppName,''Internal'') as Source,dbo.fn_GetAttributes(Opp.numWarehouseItmsID,I.bitSerialized) as Attributes ,numSourceID '
                            + CASE WHEN @strSQL = '' THEN ''
                                   ELSE ',' + @strSQL
                              END
                            + ',isnull(opp.numUOMId,0) AS numUOM,ISNULL(u.vcUnitName,'''') vcUOMName,dbo.fn_UOMConversion(opp.numUOMId,Opp.numItemCode,om.numDomainId,null) AS UOMConversionFactor
                          ,case when Opp.bitDiscountType=0 then  cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) +  '' ('' + CAST(Opp.fltDiscount AS VARCHAR(10)) +  ''%)'' 
                          ELSE cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) end as DiscAmt
                         ,isnull(I.numVendorID,0) numVendorID,OM.tintOppType,cast(Opp.numUnitHour as numeric(9,0)) numQty,SUBSTRING(
(SELECT '','' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN '' ('' + CONVERT(VARCHAR(15),whi.numQty) + '')'' ELSE '''' end 
FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=Opp.numOppId and oppI.numOppItemID=Opp.numoppitemtCode 
ORDER BY vcSerialNo FOR XML PATH('''')),2,200000) AS SerialLotNo, ISNULL(Opp.bitWorkOrder,0) as bitWorkOrder,
CASE WHEN ISNULL(Opp.bitWorkOrder,0)=1 then (select dbo.GetListIemName(numWOStatus) + CASE WHEN isnull(numWOStatus,0)=23184 THEN
'' - '' + CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo), '''')+'',''+convert(varchar(20),dateadd(minute,'+ CONVERT(VARCHAR(15),-@ClientTimeZoneOffset) +',bintCompletedDate)) AS VARCHAR(100)) ELSE '''' end
 from WorkOrder where isnull(numOppId,0)=Opp.numOppId and numItemCode=Opp.numItemCode) else '''' end as vcWorkOrderStatus,
 ISNULL(Opp.numVendorWareHouse,0) as numVendorWareHouse,ISNULL(Opp.numShipmentMethod,0) as numShipmentMethod,ISNULL(Opp.numSOVendorId,0) as numSOVendorId,Opp.numUnitHour as numOriginalUnitHour,
 isnull(Opp.numProjectID,0) numProjectID,isnull(Opp.numClassID,0) numClassID,isnull(Opp.numProjectStageID,0) numProjectStageID,Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) when OPP.numProjectStageID=-1 then ''T&E'' When OPP.numProjectId>0 then case when OM.tintOppType=1 then ''S.O.'' else ''P.O.'' end else ''-'' end as vcProjectStageName,
 CASE WHEN (SELECT TOP 1 ISNULL(OBI.numOppBizDocItemID,0) FROM dbo.OpportunityBizDocs OBD INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID WHERE OBI.numOppItemID = Opp.numoppitemtCode AND ISNULL(OBD.bitAuthoritativeBizDocs,0)=1 ) > 0
 THEN 1 ELSE 0 END AS bitItemAddedToAuthBizDoc,isnull(I.numItemClass,0) as numItemClass
              from OpportunityItems Opp                    
                               left join OpportunityMaster OM                                                            
                                  on Opp.numOppID=oM.numOppID                                    
                                 join item I                                                                    
                                  on Opp.numItemCode=i.numItemcode   
                                  left join Vendor v 
									on v.numVendorID= I.numVendorID and I.numItemCode =v.numItemCode
                                left join  WareHouseItems  WItems                                        
                                on   WItems.numWareHouseItemID= Opp.numWarehouseItmsID                                            
                                left join  Warehouses  W                                            
                                  on   W.numWarehouseID= WItems.numWarehouseID                                         
                                  left join OpportunityMaster M                                                            
                                  on Opp.numSourceID=M.numOppID                      
                                        LEFT JOIN  UOM u ON u.numUOMId = opp.numUOMId
                                  where om.numDomainId ='
                            + CONVERT(VARCHAR(15), @numDomainId)
                            + ' and Opp.numOppId='+ CONVERT(VARCHAR(15), @OpportunityId) +  
                            + CASE WHEN @numItemCode > 0 THEN ' AND Opp.numOppItemTCode = ' + CONVERT(VARCHAR(15), @numItemCode)
                                   ELSE ''
                              END
                            +
                            ' order by numoppitemtCode ASC '
                        PRINT @strSQL
                        EXEC ( @strSQL
                            )
                        SELECT  vcSerialNo,
                                vcComments AS Comments,
                                numOppItemID AS numoppitemtCode,
                                W.numWarehouseItmsDTLID,
                                numWarehouseItmsID AS numWItmsID,
                                dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                                                     1) Attributes
                        FROM    OppWarehouseSerializedItem O
                                LEFT JOIN WareHouseItmsDTL W ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
                        WHERE   numOppID = @OpportunityId
                    END
            */
            END
            
    END
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
--2 changes
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount money =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(9),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0
)                                                                          
                                                                          
as  

 DECLARE @fltExchangeRate float                                 
 DECLARE @hDocItem int                                                                                                                            
 DECLARE @TotalAmount as money                                                                          
 DECLARE @tintOppStatus as tinyint               
 DECLARE @tintDefaultClassType NUMERIC
 DECLARE @numDefaultClassID NUMERIC;SET @numDefaultClassID=0
 
 --Get Default Item Class for particular user based on Domain settings  
 SELECT @tintDefaultClassType=isnull(tintDefaultClassType,0) FROM dbo.Domain  WHERE numDomainID = @numDomainID 

 IF @tintDefaultClassType=0
      SELECT @numDefaultClassID=NULLIF(numDefaultClass,0) FROM dbo.UserMaster  WHERE numUserDetailId =@numUserCntID AND numDomainID = @numDomainID 


--If new Oppertunity
if @numOppID = 0                                                                          
 begin     
  declare @intOppTcode as numeric(9)                           
  Select @intOppTcode=max(numOppId)from OpportunityMaster                
  set @intOppTcode =isnull(@intOppTcode,0) + 1                                                
  set @vcPOppName=@vcPOppName+ '-'+convert(varchar(4),year(getutcdate()))  +'-A'+ convert(varchar(10),@intOppTcode)                                  
  
  IF ISNULL(@numCurrencyID,0) = 0 
	BEGIN
	 SET @fltExchangeRate=1
	 SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
	END
  else set @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
                                                                       
  --Set Default Class If enable User Level Class Accountng 
  DECLARE @numAccountClass AS NUMERIC(18);SET @numAccountClass=0
  SELECT @numAccountClass=ISNULL(numDefaultClass,0) FROM dbo.UserMaster UM JOIN dbo.Domain D ON UM.numDomainID=D.numDomainId
  WHERE D.numDomainId=@numDomainId AND UM.numUserDetailId=@numUserCntID AND ISNULL(D.IsEnableUserLevelClassTracking,0)=1
                                                                       
  insert into OpportunityMaster                                                                          
  (                                                                             
  numContactId,                                                                   
  numDivisionId,                                                                          
  txtComments,                                             
  numCampainID,                                                                          
  bitPublicFlag,                                                                          
  tintSource,
  tintSourceType,                                                             
  vcPOppName,                                                                          
  intPEstimatedCloseDate,                                                                          
  monPAmount,                                                                           
  numCreatedBy,                                                                          
  bintCreatedDate,                                                                           
  numModifiedBy,                                                                          
  bintModifiedDate,                                                                          
  numDomainId,                                                                                                             
  numRecOwner,                                                                          
  lngPConclAnalysis,                                                                         
  tintOppType,                                                              
  numSalesOrPurType,
  numCurrencyID,
  fltExchangeRate,
  numAssignedTo,
  numAssignedBy,
  [tintOppStatus],
  numStatus,
  vcOppRefOrderNo,
  --vcWebApiOrderNo,
  bitStockTransfer,bitBillingTerms,intBillingDays,bitInterestType,fltInterest,vcCouponCode,
  bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,vcMarketplaceOrderReportId,
  numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
  bitUseMarkupShippingRate,
  numMarkupShippingRate,
  intUsedShippingCompany
  )                                                                          
  Values                                                                          
  (                                                                          
  @numContactId,                                                                          
  @numDivisionId,                                                                          
  @Comments,                           
  @CampaignID,                                                                          
  @bitPublicFlag,                     
  @tintSource,
  @tintSourceType,                         
  @vcPOppName,                                                                          
  @dtEstimatedCloseDate,                                                                          
  @monPAmount,                                                                                
  @numUserCntID,                                                                          
 CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END,                                                                          
  @numUserCntID,                                                           
  getutcdate(),                                                                          
  @numDomainId,                                                                                   
  @numUserCntID,                                                                          
  @lngPConclAnalysis,                                                                                                                                                                      
  @tintOppType,                                                                                                                                      
  @numSalesOrPurType,              
  @numCurrencyID, 
  @fltExchangeRate,
  case when @numAssignedTo>0 then @numAssignedTo else null end,
  case when @numAssignedTo>0 then @numUserCntID else null   END,
  @DealStatus,
  @numStatus,
  @vcOppRefOrderNo,
 -- @vcWebApiOrderNo,
  @bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,@vcCouponCode,
  @bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,
  @numPercentageComplete,@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,
  @bitUseMarkupShippingRate,
  @numMarkupShippingRate,
  @intUsedShippingCompany
    )                                                                                                                      
  set @numOppID=scope_identity()                                                
  
  --Update OppName as per Name Template
  EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

  --Map Custom Field	
  DECLARE @tintPageID AS TINYINT;
  SET @tintPageID=CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
  EXEC dbo.USP_AddParentChildCustomFieldMap
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@numRecordID = @numOppID, --  numeric(18, 0)
	@numParentRecId = @numDivisionId, --  numeric(18, 0)
	@tintPageID = @tintPageID --  tinyint
 	
  
	IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
	   BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
	   END
  ---- inserting Items                                                                          
   
                                                   
  if convert(varchar(10),@strItems) <>'' AND @numOppID>0
  begin                      
   EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
  insert into                       
   OpportunityItems                                                                          
   (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,
   numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,monAvgCost,bitItemPriceApprovalRequired)
   select @numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,
   x.monPrice,x.monTotAmount,X.vcItemDesc,
   CASE X.numWarehouseItmsID WHEN -1 
							 THEN (SELECT [numWareHouseItemID] FROM [WareHouseItems] 
								   WHERE [numItemID] = X.numItemCode 
								   AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								   AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail 
														  WHERE numDomainID =@numDomainId 
														  AND WebApiId = @WebApiId))  
							 WHEN 0 
							 THEN (SELECT TOP 1 [numWareHouseItemID] FROM [WareHouseItems] 
								   WHERE [numItemID] = X.numItemCode
								   AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')) 
							 ELSE  X.numWarehouseItmsID 
	END AS numWarehouseItmsID,
	X.ItemType,X.DropShip,X.bitDiscountType,
   X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,
   (SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
   (SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
   (SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
   (select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID),
   X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,Case When @tintDefaultClassType=0 then @numDefaultClassID else (SELECT numItemClass FROM item WHERE numItemCode = X.numItemCode) end,X.numToWarehouseItemID,X.Attributes,
   (SELECT ISNULL(monAverageCost,0) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),X.bitItemPriceApprovalRequired from(
   SELECT *FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
   WITH                       
   (                                                                          
   numoppitemtCode numeric(9),                                     
   numItemCode numeric(9),                                                                          
   numUnitHour numeric(9,2),                                                                          
   monPrice money,                                                                       
   monTotAmount money,                                                                          
   vcItemDesc varchar(1000),                                    
   numWarehouseItmsID numeric(9),                          
   ItemType varchar(30),      
   DropShip bit,
   bitDiscountType bit,
   fltDiscount float,
   monTotAmtBefDiscount MONEY,
   vcItemName varchar(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9) ,numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(200),vcSKU VARCHAR(100),bitItemPriceApprovalRequired BIT
   ))X    
   ORDER BY 
   X.numoppitemtCode
    
    
   Update OpportunityItems                       
   set numItemCode=X.numItemCode,    
   numOppId=@numOppID,                     
   numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
   monPrice=x.monPrice,                      
   monTotAmount=x.monTotAmount,                                  
   vcItemDesc=X.vcItemDesc,                      
   numWarehouseItmsID=X.numWarehouseItmsID,                 
   bitDropShip=X.DropShip,
   bitDiscountType=X.bitDiscountType,
   fltDiscount=X.fltDiscount,
   monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,bitWorkOrder=X.bitWorkOrder,
   numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,
vcAttributes=X.Attributes,numClassID=(Case When @tintDefaultClassType=0 then @numDefaultClassID else (SELECT numItemClass FROM item WHERE numItemCode = X.numItemCode) END),bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired
--   ,vcModelID=(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),vcManufacturer=(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode)
--  ,vcPathForTImage=(SELECT vcPathForTImage FROM item WHERE numItemCode = X.numItemCode),monVendorCost=(select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID)
   from (SELECT numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,
   numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,Attributes,bitItemPriceApprovalRequired
   FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
   WITH  (                      
    numoppitemtCode numeric(9),                                     
    numItemCode numeric(9),                                                                          
    numUnitHour numeric(9,2),                                                                          
    monPrice money,                                         
    monTotAmount money,                                                                          
    vcItemDesc varchar(1000),                                    
    numWarehouseItmsID numeric(9),      
    DropShip bit,
	bitDiscountType bit,
    fltDiscount float,
    monTotAmtBefDiscount MONEY,vcItemName varchar(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(200),bitItemPriceApprovalRequired BIT
   ))X where numoppitemtCode=X.numOppItemID                                          
	
	
   -- Update UOM of opportunity items have not UOM set while exported from Marketplace (means tintSourceType = 3)
	IF ISNULL(@tintSourceType,0) = 3
	BEGIN
		--SELECT * FROM OpportunityMaster WHERE numOppId = 81654
		--SELECT * FROM OpportunityItems WHERE numOppId = 81654
		--SELECT * FROM Item WHERE numItemCode = 822078

		UPDATE OI SET numUOMID = ISNULL(I.numSaleUnit,0), 
					  numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
		FROM OpportunityItems OI
		JOIN Item I ON OI.numItemCode = I.numItemCode
		WHERE numOppId = @numOppID
		AND I.numDomainID = @numDomainId

	END	
   --Update OpportunityItems
   --set 
   --FROM (SELECT numItemCode,vcItemName,[txtItemDesc],vcModelID FROM item WHERE numItemCode IN (SELECT [numItemCode] FROM OpportunityItems WHERE numOppID = @numOppID))X
   --WHERE OpportunityItems.[numItemCode] = X.numItemCode 
                                     
   insert into OppWarehouseSerializedItem                                                                          
   (numOppID,numWarehouseItmsDTLID,numOppItemID,numWarehouseItmsID)                      
   select @numOppID,X.numWarehouseItmsDTLID,X.numoppitemtCode,X.numWItmsID from(                                                                          
   SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
   WITH                        
   (                                               
   numWarehouseItmsDTLID numeric(9),                      
   numoppitemtCode numeric(9),                      
   numWItmsID numeric(9)                                                     
   ))X                                    
               
                                 
   update OppWarehouseSerializedItem                       
   set numOppItemID=X.numoppitemtCode                                
   from                       
   (SELECT numWarehouseItmsDTLID as DTLID,O.numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
   WITH(numWarehouseItmsDTLID numeric(9),numoppitemtCode numeric(9),numWItmsID numeric(9))                                
   join OpportunityItems O on O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID)X                 
   where numOppID=@numOppID and numWarehouseItmsDTLID=X.DTLID      
    
--   insert into OpportunityKitItems(numOppKitItem,numChildItem,intQuantity,numWareHouseItemId)    
--   SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/ChildItems',2)                                                                          
--   WITH                        
--   (                                               
--   numoppitemtCode numeric(9),                      
--   numItemCode numeric(9),    
--   numQtyItemsReq integer,  
--   numWarehouseItmsID numeric(9)                                                       
--   )    
      
    
    
    
	EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
    
                              
                          
   EXEC sp_xml_removedocument @hDocItem                                     
        end          
   set @TotalAmount=0                                                           
   select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@numOppID                                                                          
  update OpportunityMaster set  monPamount=@TotalAmount                                                          
  where numOppId=@numOppID                                                 
                      
     
--Insert Tax for Division   

IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN                   
	INSERT dbo.OpportunityMasterTaxItems (
		numOppId,
		numTaxItemID,
		fltPercentage
	) SELECT @numOppID,TI.numTaxItemID,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL) FROM TaxItems TI JOIN DivisionTaxTypes DTT ON TI.numTaxItemID = DTT.numTaxItemID
	 WHERE DTT.numDivisionID=@numDivisionId AND DTT.bitApplicable=1
	   union 
	  select @numOppID,0,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL) 
	  FROM dbo.DivisionMaster WHERE bitNoTax=0 AND numDivisionID=@numDivisionID
END
-- Archieve item based on Item's individual setting
--	UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT * FROM dbo.OpportunityItems WHERE OpportunityItems.numItemCode = I.numItemCode) AND ISNULL(I.bitArchiveItem,0) = 1 
--									  THEN 1 
--									  ELSE 0 
--									  END 
--	FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode
	
 END
 
 else                                                                                                                          
 BEGIN                  
	--Declaration
	 DECLARE @tempAssignedTo AS NUMERIC(9)                                                    
	 SET @tempAssignedTo = NULL 
	 
	 SELECT @tintOppStatus = tintOppStatus,@tempAssignedTo=isnull(numAssignedTo,0) FROM   OpportunityMaster WHERE  numOppID = @numOppID
 
	--Reverting back the warehouse items                  
	 IF @tintOppStatus = 1 
		BEGIN        
		PRINT 'inside revert'          
			EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID                  
		END                  
	-- Update Master table
	
	   IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
	   BEGIN
	   		UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
	   END
		
	   IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
	   BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
	   END 
						
	   UPDATE   OpportunityMaster
	   SET      vcPOppName = @vcPOppName,
				txtComments = @Comments,
				bitPublicFlag = @bitPublicFlag,
				numCampainID = @CampaignID,
				tintSource = @tintSource,
				tintSourceType=@tintSourceType,
				intPEstimatedCloseDate = @dtEstimatedCloseDate,
				numModifiedBy = @numUserCntID,
				bintModifiedDate = GETUTCDATE(),
				lngPConclAnalysis = @lngPConclAnalysis,
				monPAmount = @monPAmount,
				tintActive = @tintActive,
				numSalesOrPurType = @numSalesOrPurType,
				numStatus = @numStatus,
				tintOppStatus = @DealStatus,
				vcOppRefOrderNo=@vcOppRefOrderNo,
				--vcWebApiOrderNo = @vcWebApiOrderNo,
				bintOppToOrder=(Case when @tintOppStatus=0 and @DealStatus=1 then GETUTCDATE() else bintOppToOrder end),
				bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,bitBillingTerms=@bitBillingTerms,
				intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,
				tintTaxOperator=@tintTaxOperator,numDiscountAcntType=@numDiscountAcntType,vcCouponCode = @vcCouponCode,
				numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
				bitUseMarkupShippingRate = @bitUseMarkupShippingRate,
				numMarkupShippingRate = @numMarkupShippingRate,
				intUsedShippingCompany = @intUsedShippingCompany
	   WHERE    numOppId = @numOppID   
	   
	---Updating if organization is assigned to someone                                                      
	   IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN                                   
			UPDATE  OpportunityMaster SET     numAssignedTo = @numAssignedTo, numAssignedBy = @numUserCntID WHERE   numOppId = @numOppID                                                    
		END                                                     
	   ELSE 
	   IF ( @numAssignedTo = 0 ) 
		BEGIN                
			UPDATE  OpportunityMaster SET     numAssignedTo = 0, numAssignedBy = 0 WHERE   numOppId = @numOppID
		END                                                                      
		---- Updating Opp Items
		if convert(varchar(10),@strItems) <>'' AND @numOppID>0                                                                         
		begin
			   EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   --Delete Items
			   delete from OpportunityKitItems where numOppID=@numOppID and numOppItemID not in                       
			   (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
			   WITH  (numoppitemtCode numeric(9)))                      
                                        
--               ---- ADDED BY Manish Anjara : Jun 26,2013 - Archive item based on Item's individual setting
--               UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT * FROM dbo.OpportunityItems WHERE OpportunityItems.numItemCode = I.numItemCode) AND ISNULL(I.bitArchiveItem,0) = 1 
--												 THEN 1 
--												 ELSE 0 
--												 END 
--			   FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode
--					 	
--			   UPDATE Item SET IsArchieve = CASE WHEN ISNULL(I.bitArchiveItem,0) = 1 THEN 0 ELSE 1 END
--			   FROM Item I
--			   JOIN OpportunityItems OI ON I.numItemCode = OI.numItemCode
--			   WHERE numOppID = @numOppID and OI.numItemCode NOT IN (SELECT numItemCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) 
--																	 WITH (numItemCode numeric(9)))

			   delete from OpportunityItems where numOppID=@numOppID and numoppitemtCode not in                       
			   (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
			   WITH  (numoppitemtCode numeric(9)))   
	                                
			   insert into OpportunityItems                                                                          
			   (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numToWarehouseItemID,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired)
			   select @numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),(select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID),X.numUOM,X.bitWorkOrder,
			   X.numVendorWareHouse,X.numToWarehouseItemID,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,Case When @tintDefaultClassType=0 then @numDefaultClassID else (SELECT numItemClass FROM item WHERE numItemCode = X.numItemCode) END,
			   (SELECT ISNULL(monAverageCost,0) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired from(
			   SELECT *FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
			   WITH  (                      
				numoppitemtCode numeric(9),                                     
				numItemCode numeric(9),                                                                          
				numUnitHour numeric(9,2),                                                                          
				monPrice money,                                                                       
				monTotAmount money,                                                                          
				vcItemDesc varchar(1000),                                    
				numWarehouseItmsID numeric(9),
				numToWarehouseItemID numeric(9),                   
				Op_Flag tinyint,                            
				ItemType varchar(30),      
				DropShip bit,
				bitDiscountType bit,
				fltDiscount float,
				monTotAmtBefDiscount MONEY,
				vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(200), bitItemPriceApprovalRequired BIT
				 ))X                                     
				--Update items                 
			   Update OpportunityItems                       
			   set numItemCode=X.numItemCode,    
			   numOppId=@numOppID,                       
			   numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
			   monPrice=x.monPrice,                      
			   monTotAmount=x.monTotAmount,                                  
			   vcItemDesc=X.vcItemDesc,                      
			   numWarehouseItmsID=X.numWarehouseItmsID,
			   numToWarehouseItemID = X.numToWarehouseItemID,             
			   bitDropShip=X.DropShip,
			   bitDiscountType=X.bitDiscountType,
			   fltDiscount=X.fltDiscount,
			   monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
			   numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired
			   from (SELECT numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,numToWarehouseItemID,DropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,bitItemPriceApprovalRequired  FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
			   WITH  (                      
				numoppitemtCode numeric(9),                                     
				numItemCode numeric(9),                                                                          
				numUnitHour numeric(9,2),                                                                          
				monPrice money,                                         
				monTotAmount money,                                                                          
				vcItemDesc varchar(1000),                                    
				numWarehouseItmsID numeric(9), 
				numToWarehouseItemID NUMERIC(18,0),     
				DropShip bit,
				bitDiscountType bit,
				fltDiscount float,
				monTotAmtBefDiscount MONEY,vcItemName varchar(300),numUOM NUMERIC(9),numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(200),bitItemPriceApprovalRequired BIT                                              
			   ))X where numoppitemtCode=X.numOppItemID                           
			                                    
			   EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			   		                                                                      
			   EXEC sp_xml_removedocument @hDocItem                                               
		end     
	    --Delete previously added serial lot number added for opportunity
		DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
END

----------------generic section will be called in both insert and update ---------------------------------
-- Archieve item based on Item's individual setting
--	UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT * FROM dbo.OpportunityItems WHERE OpportunityItems.numItemCode = I.numItemCode) AND ISNULL(I.bitArchiveItem,0) = 1 
--									  THEN 1 
--									  ELSE 0 
--									  END 
--	FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode
	
if convert(varchar(10),@strItems) <>'' AND @numOppID>0                                                                        
  begin 
EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

--delete from OpportunityKitItems where numOppID=@numOppID and numOppItemID 
--	not in (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppID=@numOppID)                      
		
--Update Kit Items                 
Update OKI set numQtyItemsReq=numQtyItemsReq_Orig * OI.numUnitHour
			   FROM OpportunityKitItems OKI JOIN OpportunityItems OI ON OKI.numOppItemID=OI.numoppitemtCode and OKI.numOppId=OI.numOppId  
			   WHERE OI.numOppId=@numOppId 

--Insert Kit Items                 
INSERT into OpportunityKitItems                                                                          
		(numOppId,numOppItemID,numChildItemID,numWareHouseItemId,numQtyItemsReq,numQtyItemsReq_Orig,numUOMId,numQtyShipped)
  SELECT @numOppId,OI.numoppitemtCode,ID.numChildItemID,ID.numWareHouseItemId,ID.numQtyItemsReq * OI.numUnitHour,ID.numQtyItemsReq,ID.numUOMId,0
	 FROM OpportunityItems OI JOIN ItemDetails ID ON OI.numItemCode=ID.numItemKitID WHERE 
	OI.numOppId=@numOppId AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID)
--	OI.numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
--			   WITH  (                      
--				numoppitemtCode numeric(9))X)
			   
EXEC sp_xml_removedocument @hDocItem  
END

/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
declare @tintShipped as tinyint               
select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID              
if @tintOppStatus=1               
	begin         
	PRINT 'inside USP_UpdatingInventoryonCloseDeal'
		exec USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
	end              
            
if @tintOppType=1              
begin              
	if @tintOppStatus=1 Update WareHouseItmsDTL set tintStatus=1 where numWareHouseItmsDTLID in (select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppID=@numOppID)
	if @tintShipped=1 Update WareHouseItmsDTL set tintStatus=2 where numWareHouseItmsDTLID in (select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppID=@numOppID)              
END


declare @tintCRMType as numeric(9)        
declare @AccountClosingDate as datetime        

select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

--When deal is won                                         
if @DealStatus = 1 
begin           
	 if @AccountClosingDate is null                   
	  update OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numOppID              
end         
--When Deal is Lost
else if @DealStatus = 2
begin         
	 select @AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
	 if @AccountClosingDate is null                   
	  update OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numOppID
end                    

/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
begin        
	update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end        
-- Promote Lead to Account when Sales/Purchase Order is created against it
ELSE if @tintCRMType=0 AND @DealStatus = 1 --Lead & Order
begin        
	update divisionmaster set tintCRMType=2,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end
--Promote Prospect to Account
else if @tintCRMType=1 AND @DealStatus = 1 
begin        
	update divisionmaster set tintCRMType=2,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9) 
DECLARE @bitIsPrimary BIT;

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

--Bill Address
IF @numBillAddressId>0
BEGIN
 SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numBillAddressId
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName
END
  
  
  --Ship Address
IF @numShipAddressId>0
BEGIN
	SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numShipAddressId
 



  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName
END

--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems (
	numOppId,
	numOppItemID,
	numTaxItemID
) SELECT @numOppId,OI.numoppitemtCode,TI.numTaxItemID FROM dbo.OpportunityItems OI JOIN dbo.ItemTax IT ON OI.numItemCode=IT.numItemCode JOIN
TaxItems TI ON TI.numTaxItemID=IT.numTaxItemID WHERE OI.numOppId=@numOppID AND IT.bitApplicable=1 AND 
OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
  select @numOppId,OI.numoppitemtCode,0 FROM dbo.OpportunityItems OI JOIN dbo.Item I ON OI.numItemCode=I.numItemCode
   WHERE OI.numOppId=@numOppID  AND I.bitTaxable=1 AND
OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

  
 UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_SalesOrderConfiguration_Save' ) 
    DROP PROCEDURE USP_SalesOrderConfiguration_Save
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 18 Feb 2014
-- Description:	Saves new sales order form configuration
-- =============================================
CREATE PROCEDURE USP_SalesOrderConfiguration_Save
	@numSOCID NUMERIC(18,0) = NULL,
	@numDomainID NUMERIC(18,0) = NULL,
	@bitAutoFocusCustomer BIT,
	@bitAutoFocusItem BIT,
	@bitDisplayRootLocation BIT,
	@bitAutoAssignOrder BIT,
	@bitDisplayLocation BIT,
	@bitDisplayFinancialStamp BIT,
	@bitDisplayItemDetails BIT,
	@bitDisplayUnitCost BIT,
	@bitDisplayProfitTotal BIT,
	@bitDisplayShippingRates BIT,
	@bitDisplayPaymentMethods BIT,
	@bitCreateOpenBizDoc BIT,
	@numListItemID NUMERIC(18,0),
	@vcPaymentMethodIDs VARCHAR(100),
	@bitDisplayCurrency BIT,
	@bitDisplayAssignTo BIT,
	@bitDisplayTemplate BIT,
	@bitDisplayCouponDiscount BIT,
	@bitDisplayShippingCharges BIT,
	@bitDisplayDiscount BIT,
	@bitDisplaySaveNew BIT,
	@bitDisplayAddToCart BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF (SELECT COUNT(*) FROM SalesOrderConfiguration WHERE numDomainID = @numDomainID) = 0
	BEGIN
		INSERT INTO dbo.SalesOrderConfiguration
		(
			numDomainID,
			bitAutoFocusCustomer,
			bitAutoFocusItem,
			bitDisplayRootLocation,
			bitAutoAssignOrder,
			bitDisplayLocation,
			bitDisplayFinancialStamp,
			bitDisplayItemDetails,
			bitDisplayUnitCost,
			bitDisplayProfitTotal,
			bitDisplayShippingRates,
			bitDisplayPaymentMethods,
			bitCreateOpenBizDoc,
			numListItemID,
			vcPaymentMethodIDs,
			bitDisplayCurrency,
			bitDisplayAssignTo,
			bitDisplayTemplate,
			bitDisplayCouponDiscount,
			bitDisplayShippingCharges,
			bitDisplayDiscount,
			bitDisplaySaveNew,
			bitDisplayAddToCart
		)
		VALUES
		(
			@numDomainID,
			@bitAutoFocusCustomer,
			@bitAutoFocusItem,
			@bitDisplayRootLocation,
			@bitAutoAssignOrder,
			@bitDisplayLocation,
			@bitDisplayFinancialStamp,
			@bitDisplayItemDetails,
			@bitDisplayUnitCost,
			@bitDisplayProfitTotal,
			@bitDisplayShippingRates,
			@bitDisplayPaymentMethods,
			@bitCreateOpenBizDoc,
			@numListItemID,
			@vcPaymentMethodIDs,
			@bitDisplayCurrency,
			@bitDisplayAssignTo,
			@bitDisplayTemplate,
			@bitDisplayCouponDiscount,
			@bitDisplayShippingCharges,
			@bitDisplayDiscount,
			@bitDisplaySaveNew,
			@bitDisplayAddToCart
		)
	END
	ELSE
	BEGIN
		UPDATE
			dbo.SalesOrderConfiguration
		SET
			bitAutoFocusCustomer = @bitAutoFocusCustomer,
			bitAutoFocusItem = @bitAutoFocusItem,
			bitDisplayRootLocation = @bitDisplayRootLocation,
			bitAutoAssignOrder = @bitAutoAssignOrder,
			bitDisplayLocation = @bitDisplayLocation,
			bitDisplayFinancialStamp = @bitDisplayFinancialStamp,
			bitDisplayItemDetails = @bitDisplayItemDetails,
			bitDisplayUnitCost = @bitDisplayUnitCost,
			bitDisplayProfitTotal = @bitDisplayProfitTotal,
			bitDisplayShippingRates = @bitDisplayShippingRates,
			bitDisplayPaymentMethods = @bitDisplayPaymentMethods,
			bitCreateOpenBizDoc = @bitCreateOpenBizDoc,
			numListItemID = @numListItemID,
			vcPaymentMethodIDs  = @vcPaymentMethodIDs,
			bitDisplayCurrency = @bitDisplayCurrency,
			bitDisplayAssignTo = @bitDisplayAssignTo,
			bitDisplayTemplate = @bitDisplayTemplate,
			bitDisplayCouponDiscount = @bitDisplayCouponDiscount,
			bitDisplayShippingCharges = @bitDisplayShippingCharges,
			bitDisplayDiscount = @bitDisplayDiscount,
			bitDisplaySaveNew = @bitDisplaySaveNew,
			bitDisplayAddToCart = @bitDisplayAddToCart
		WHERE
			numDomainID = @numDomainID
	END
    
END
GO


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateECampaignAlertStatus')
DROP PROCEDURE USP_UpdateECampaignAlertStatus
GO
CREATE PROCEDURE [dbo].[USP_UpdateECampaignAlertStatus]
    @numConECampDTLID NUMERIC(9),
    @tintDeliveryStatus TINYINT,
    @vcEmailLog VARCHAR(500) = NULL,
    @tintMode TINYINT = 0
AS 
    BEGIN
        IF @tintMode = 0 
		DECLARE @Date AS DATETIME 
		SET @Date = GETUTCDATE()
        BEGIN
            UPDATE  [ConECampaignDTL]
            SET     [vcEmailLog] = @vcEmailLog,
                    [tintDeliveryStatus] = @tintDeliveryStatus,
                    bitSend = 1,
                    bintSentON = @Date
            WHERE   [numConECampDTLID] = @numConECampDTLID
        END

        IF @tintMode = 1 
        BEGIN
            UPDATE  [ConECampaignDTL]
            SET     bitFollowUpStatus = 1
            WHERE   [numConECampDTLID] = @numConECampDTLID      
        END
			
        IF @tintMode = 2 --For Action Item
        BEGIN
            UPDATE  [ConECampaignDTL]
            SET     [vcEmailLog] = @vcEmailLog,
                    bitSend = 1,
					[tintDeliveryStatus] = @tintDeliveryStatus,
                    bintSentON = GETUTCDATE()
            WHERE   [numConECampDTLID] = @numConECampDTLID
        END
    END

