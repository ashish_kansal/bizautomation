/******************************************************************
Project: Release 6.2 Date: 10.OCT.2016
Comments: STORED PROCEDURES
*******************************************************************/


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='fn_GetShippingReportAddress')
DROP FUNCTION fn_GetShippingReportAddress
GO
CREATE FUNCTION [dbo].[fn_GetShippingReportAddress]
(
	@tintType INT -- 1: FROM Addres, 2: TO Address
	,@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
) 
RETURNS @TEMP TABLE
(
	vcName VARCHAR(100)
	,vcCompanyName VARCHAR(100)
	,vcPhone VARCHAR(50)
	,vcStreet VARCHAR(300)
	,vcCity VARCHAR(50)
	,vcState NUMERIC(18,0)
	,vcZipCode VARCHAR(50)
	,vcCountry NUMERIC(18,0)
)
AS
BEGIN
	IF @tintType = 1
	BEGIN
		INSERT INTO 
			@TEMP
		SELECT TOP 1
			CONCAT(ISNULL(A.vcFirstName,''),' ',ISNULL(A.vcLastName,''))
			,ISNULL(C.vcCompanyName,'')
			,ISNULL(D.[vcComPhone],'')
			,ISNULL(AD2.vcstreet,'')
			,ISNULL(AD2.vcCity,'')  
			,ISNULL(AD2.numState,0)
			,ISNULL(AD2.vcPostalCode,'')
			,ISNULL(AD2.numCountry,0)
		FROM 
			DivisionMaster D
		INNER JOIN
			CompanyInfo C
		ON
			D.numCompanyID=C.numCompanyId
		LEFT JOIN
			AdditionalContactsInformation A        
		ON 
			A.numDivisionId = D.numDivisionId
			AND ISNULL(A.bitPrimaryContact,0)=1
		LEFT JOIN 
			dbo.AddressDetails AD2 
		ON 
			AD2.numDomainID=D.numDomainID 
			AND AD2.numRecordID= D.numDivisionID 
			AND AD2.tintAddressOf=2
			AND AD2.tintAddressType=2 
			AND AD2.bitIsPrimary=1
		WHERE
			C.numDomainID=@numDomainID
			AND C.numCompanyType=93
	END
	ELSE IF @tintType = 2
	BEGIN
		DECLARE @tintShipToType AS TINYINT
		DECLARE @numDivisionID AS NUMERIC(18,0)
		DECLARE @numContactID AS NUMERIC(18,0)
		DECLARE @vcName AS VARCHAR(100)
		DECLARE @vcPhone VARCHAR(50)
	
		SELECT  
			@tintShipToType = tintShipToType
			,@numDivisionID = numDivisionID
			,@numContactID = numContactID
        FROM 
			OpportunityMaster
        WHERE 
			numOppId = @numOppID


        SELECT  
			@vcName = ISNULL(vcFirstName, '') + ' ' + ISNULL(vcLastname, '')
			,@vcPhone = (CASE WHEN LEN(ISNULL(numPhone,'')) > 0 THEN  numPhone ELSE (SELECT ISNULL(DM.vcComPhone,0) FROM DivisionMaster DM WHERE DM.numDivisionID=@numDivisionID) END)
        FROM 
			AdditionalContactsInformation
        WHERE 
			numContactID = @numContactID

        IF (@tintShipToType IS NULL OR @tintShipToType = 1) 
		BEGIN
			INSERT INTO 
				@TEMP
			SELECT
				@vcName
				,dbo.fn_GetComapnyName(@numDivisionID)
				,@vcPhone
				,ISNULL(AD.VcStreet,'')
                ,ISNULL(AD.VcCity,'')
                ,ISNULL(AD.numState,0)
                ,ISNULL(AD.vcPostalCode,'')
                ,ISNULL(AD.numCountry,0)
			FROM
				dbo.AddressDetails AD
            WHERE   
				AD.numDomainID=@numDomainID
				AND AD.numRecordID = @numDivisionID
                AND AD.tintAddressOf = 2
                AND AD.tintAddressType = 2
                AND AD.bitIsPrimary = 1
		END
        ELSE IF @tintShipToType = 0 
		BEGIN
			INSERT INTO
				@TEMP
			SELECT
				@vcName
				,dbo.fn_GetComapnyName(@numDivisionID)
				,@vcPhone
				,ISNULL(AD1.VcStreet,'')
                ,ISNULL(AD1.VcCity,'')
                ,ISNULL(AD1.numState,0)
                ,ISNULL(AD1.vcPostalCode,'')
                ,ISNULL(AD1.numCountry,0)
			FROM 
				CompanyInfo Com1
            INNER JOIN 
				DivisionMaster div1 
			ON 
				com1.numCompanyID = div1.numCompanyID
            INNER JOIN 
				Domain D1 
			ON 
				D1.numDivisionID = div1.numDivisionID
            INNER JOIN 
				dbo.AddressDetails AD1 
			ON 
				AD1.numDomainID = div1.numDomainID
				AND AD1.numRecordID = div1.numDivisionID
				AND tintAddressOf = 2
				AND tintAddressType = 2
				AND bitIsPrimary = 1
            WHERE 
				D1.numDomainID = @numDomainID
		END              
        ELSE IF @tintShipToType = 2 OR @tintShipToType = 3
		BEGIN
			INSERT INTO
				@TEMP
			SELECT  
				@vcName
				,dbo.GetCompanyNameFromContactID(@numContactID, @numDomainID)
				,@vcPhone
				,ISNULL(vcShipStreet,'')
                ,ISNULL(vcShipCity,'')
                ,ISNULL(numShipState,0)
                ,ISNULL(vcShipPostCode,'')
                ,ISNULL(numShipCountry,0)
			FROM 
				OpportunityAddress
			WHERE
				numOppID = @numOppID
		END           
	END

	RETURN
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetOppItemReleaseDates')
DROP FUNCTION GetOppItemReleaseDates
GO
CREATE FUNCTION [dbo].[GetOppItemReleaseDates] 
(
	@numDomainID NUMERIC(18,0),
    @numOppID AS NUMERIC(18,0),
	@numOppItemID AS NUMERIC(18,0),
	@bitReturnHtml BIT
)
RETURNS VARCHAR(MAX)
AS 
BEGIN
	DECLARE @vcValue AS VARCHAR(MAX) = ''

	IF EXISTS (SELECT * FROM OpportunityItemsReleaseDates WHERE	numOppID=@numOppID AND numOppItemID=@numOppItemID)
	BEGIN
		IF @bitReturnHtml = 1
		BEGIN
			SET @vcValue = CONCAT('<a href="#" onclick="OpenItemReleaseDates(',@numOppID,',',@numOppItemID,')">',STUFF((SELECT 
								', ',CONCAT(dbo.FormatedDateFromDate(dtReleaseDate,@numDomainID),'(',numQty,')(',CASE tintStatus WHEN 1 THEN 'Open)' ELSE 'Purchased)' END)
							FROM 
								OpportunityItemsReleaseDates
							WHERE
								numOppID=@numOppID
								AND numOppItemID=@numOppItemID
							For XML PATH ('') ), 1, 2, ''),'</a>');
		END
		ELSE
		BEGIN
			SET @vcValue = STUFF((SELECT 
							', ',CONCAT(dbo.FormatedDateFromDate(dtReleaseDate,@numDomainID),'(',numQty,')(',CASE tintStatus WHEN 1 THEN 'Open)' ELSE 'Purchased)' END)
						FROM 
							OpportunityItemsReleaseDates
						WHERE
							numOppID=@numOppID
							AND numOppItemID=@numOppItemID
						For XML PATH ('') ), 1, 2, '');
		END

	END
	ELSE
	BEGIN
		IF @bitReturnHtml = 1
		BEGIN
			SET @vcValue = CONCAT('<a href="#" onclick="OpenItemReleaseDates(',@numOppID,',',@numOppItemID,')">Not Available</a>')
		END
		ELSE
		BEGIN
			SET @vcValue = ''
		END
	END

	RETURN @vcValue
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p' AND NAME ='USP_BizRecurrence_Order')
DROP PROCEDURE USP_BizRecurrence_Order
GO
  
Create PROCEDURE [dbo].[USP_BizRecurrence_Order]  
	@numRecConfigID NUMERIC(18,0), 
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numOppID NUMERIC(18,0),
	@numRecurOppID NUMERIC(18,0) OUT,
	@numFrequency INTEGER,
	@dtEndDate DATE,
	@Date AS DATE
AS  
BEGIN  
BEGIN TRANSACTION;

BEGIN TRY

--DECLARE @Date AS DATE = GETDATE()

DECLARE @numDivisionID AS NUMERIC(18,0)
DECLARE @numOppType AS TINYINT
DECLARE @tintOppStatus AS TINYINT
DECLARE @DealStatus AS TINYINT
DECLARE @numStatus AS NUMERIC(9)
DECLARE @numCurrencyID AS NUMERIC(9)

DECLARE @intOppTcode AS NUMERIC(18,0)
DECLARE @numNewOppID AS NUMERIC(18,0)
DECLARE @fltExchangeRate AS FLOAT


--Get Existing Opportunity Detail
SELECT 
	@numDivisionID = numDivisionId, 
	@numOppType=tintOppType, 
	@tintOppStatus = tintOppStatus,
	@numCurrencyID=numCurrencyID,
	@numStatus = numStatus,
	@DealStatus = tintOppStatus
FROM 
	OpportunityMaster 
WHERE 
	numOppId = @numOppID                                            

IF ISNULL(@numCurrencyID,0) = 0 
BEGIN
	SET @fltExchangeRate=1
	SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
END
ELSE 
BEGIN
	SET @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
END

INSERT INTO OpportunityMaster                                                                          
(                                                                             
	numContactId, numDivisionId, txtComments, numCampainID, bitPublicFlag, tintSource, tintSourceType, vcPOppName, 
	intPEstimatedCloseDate, numCreatedBy, bintCreatedDate, numDomainId, numRecOwner, lngPConclAnalysis, tintOppType, 
	numSalesOrPurType, numCurrencyID, fltExchangeRate, numAssignedTo, numAssignedBy, [tintOppStatus], numStatus, 
	vcOppRefOrderNo, bitStockTransfer,bitBillingTerms, intBillingDays,bitInterestType,fltInterest,vcCouponCode, 
	bitDiscountType,fltDiscount,bitPPVariance, vcMarketplaceOrderID, vcMarketplaceOrderReportId, numPercentageComplete,
	numAccountClass, tintTaxOperator, bitUseShippersAccountNo, bitUseMarkupShippingRate, numMarkupShippingRate, 
	intUsedShippingCompany, bintAccountClosingDate
)                                                                          
SELECT
	numContactId, numDivisionId, txtComments, numCampainID, bitPublicFlag, tintSource, tintSourceType, vcPOppName, 
	@Date, @numUserCntID, @Date, numDomainId, @numUserCntID, lngPConclAnalysis, tintOppType, 
	numSalesOrPurType, @numCurrencyID, @fltExchangeRate, numAssignedTo, numAssignedBy, [tintOppStatus], numStatus, vcOppRefOrderNo, 
	bitStockTransfer,bitBillingTerms, intBillingDays,bitInterestType,fltInterest,vcCouponCode, bitDiscountType, fltDiscount,
	bitPPVariance, vcMarketplaceOrderID, vcMarketplaceOrderReportId, numPercentageComplete, numAccountClass,tintTaxOperator, 
	bitUseShippersAccountNo, bitUseMarkupShippingRate, numMarkupShippingRate, intUsedShippingCompany, @Date 
FROM
	OpportunityMaster
WHERE                                                                        
	numOppId = @numOppID

SET @numNewOppID=SCOPE_IDENTITY() 

EXEC USP_OpportunityMaster_CT @numDomainID=@numDomainID,@numUserCntID=@numUserCntID,@numRecordID=@numNewOppID                                             
  
--Update OppName as per Name Template
EXEC dbo.USP_UpdateNameTemplateValue @numOppType,@numDomainID,@numNewOppID

--Map Custom Field	
DECLARE @tintPageID AS TINYINT;
SET @tintPageID=CASE WHEN @numOppType=1 THEN 2 ELSE 6 END 
  	
EXEC dbo.USP_AddParentChildCustomFieldMap @numDomainID = @numDomainID, @numRecordID = @numNewOppID, @numParentRecId = @numDivisionId, @tintPageID = @tintPageID
 	
IF ISNULL(@numStatus,0) > 0 AND @numOppType=1 AND isnull(@DealStatus,0)=1
BEGIN
	EXEC USP_ManageOpportunityAutomationQueue
			@numOppQueueID = 0, -- numeric(18, 0)
			@numDomainID = @numDomainID, -- numeric(18, 0)
			@numOppId = @numNewOppID, -- numeric(18, 0)
			@numOppBizDocsId = 0, -- numeric(18, 0)
			@numOrderStatus = @numStatus, -- numeric(18, 0)
			@numUserCntID = @numUserCntID, -- numeric(18, 0)
			@tintProcessStatus = 1, -- tinyint
			@tintMode = 1 -- TINYINT
END

IF ISNULL(@numStatus,0) > 0 AND @numOppType=1 AND isnull(@DealStatus,0)=1
BEGIN
	IF @numOppType=1 AND @tintOppStatus=1
	BEGIN
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numStatus
	END
END


IF (SELECT COUNT(*) FROM OpportunityItems WHERE numOppId = @numOppID) > 0
BEGIN
	-- INSERT OPPORTUNITY ITEMS

	INSERT INTO OpportunityItems                                                                          
	(
		numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,
		fltDiscount,monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,
		numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,
		monAvgCost,bitItemPriceApprovalRequired,numRecurParentOppItemID
	)
	SELECT 
		@numNewOppID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,
		fltDiscount,monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,
		numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,
		monAvgCost,bitItemPriceApprovalRequired,OpportunityItems.numoppitemtCode
	FROM
		OpportunityItems
	WHERE
		numOppID = @numOppID 
           
	EXEC USP_BizRecurrence_WorkOrder @numDomainId,@numUserCntID,@numNewOppID,@numOppID

	INSERT INTO OpportunityKitItems                                                                          
	(
		numOppId,numOppItemID,numChildItemID,numWareHouseItemId,numQtyItemsReq,numQtyItemsReq_Orig,numUOMId,numQtyShipped
	)
	SELECT 
		@numNewOppId,OI.numoppitemtCode,ID.numChildItemID,ID.numWareHouseItemId,ID.numQtyItemsReq * OI.numUnitHour,ID.numQtyItemsReq,ID.numUOMId,0
	FROM 
		OpportunityItems OI 
	JOIN 
		ItemDetails ID 
	ON 
		OI.numItemCode=ID.numItemKitID 
	WHERE 
		OI.numOppId=@numNewOppID AND 
		OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numNewOppID)
END

DECLARE @TotalAmount AS FLOAT = 0                                                           
   
SELECT @TotalAmount = sum(monTotAmount) FROM OpportunityItems WHERE numOppId=@numNewOppID                                                                          
UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numNewOppID 

--Insert Tax for Division   

IF @numOppType=1 OR (@numOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN                   
	INSERT dbo.OpportunityMasterTaxItems 
	(
		numOppId, numTaxItemID,	fltPercentage,tintTaxType, numTaxID
	) 
	SELECT 
		@numNewOppID,TI.numTaxItemID,TEMPTax.decTaxValue,TEMPTax.tintTaxType,numTaxID
	FROM 
		TaxItems TI 
	JOIN 
		DivisionTaxTypes DTT 
	ON 
		TI.numTaxItemID = DTT.numTaxItemID
	OUTER APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType,
			numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numNewOppID,0,NULL) 
	) AS TEMPTax
	WHERE 
		DTT.numDivisionID=@numDivisionId AND 
		DTT.bitApplicable=1
	UNION 
	SELECT 
		@numNewOppID,0,TEMPTax.decTaxValue,TEMPTax.tintTaxType,numTaxID
	FROM 
		dbo.DivisionMaster 
	OUTER APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType,
			numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numNewOppID,1,NULL)  
	) AS TEMPTax
	WHERE 
		bitNoTax=0 AND 
		numDivisionID=@numDivisionID
END


--Updating the warehouse items              
DECLARE @tintShipped AS TINYINT = 0                

IF @tintOppStatus=1               
BEGIN       
	EXEC USP_UpdatingInventoryonCloseDeal @numNewOppID,@numUserCntID
END              

DECLARE @tintCRMType AS NUMERIC(9)
       
DECLARE @AccountClosingDate AS DATETIME 
SELECT @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate FROM OpportunityMaster WHERE numOppID=@numNewOppID         
SELECT @tintCRMType=tintCRMType FROM divisionmaster WHERE numDivisionID = @numDivisionID 

                 
--Add/Update Address Details
DECLARE @vcStreet varchar(100), @vcCity varchar(50), @vcPostalCode varchar(15), @vcCompanyName varchar(100), @vcAddressName VARCHAR(50)      
DECLARE @numState numeric(9),@numCountry numeric(9), @numCompanyId numeric(9) 
DECLARE @bitIsPrimary BIT

SELECT  
	@vcCompanyName=vcCompanyName,
	@numCompanyId=div.numCompanyID 
FROM 
	CompanyInfo Com                            
JOIN 
	divisionMaster Div                            
ON 
	div.numCompanyID=com.numCompanyID                            
WHERE 
	div.numdivisionID=@numDivisionId

--Bill Address
IF (SELECT COUNT(*) FROM OpportunityAddress WHERE numOppID = @numOppID) > 0
BEGIN
	SELECT  
		@vcStreet=ISNULL(vcBillStreet,''),
		@vcCity=ISNULL(vcBillCity,''),
		@vcPostalCode=ISNULL(vcBillPostCode,''),
		@numState=ISNULL(numBillState,0),
		@numCountry=ISNULL(numBillCountry,0),
		@vcAddressName=vcAddressName
	FROM  
		dbo.OpportunityAddress 
	WHERE   
		numOppID = @numOppID

  	EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numNewOppID,
		@byteMode = 0,
		@vcStreet = @vcStreet,
		@vcCity = @vcCity,
		@vcPostalCode = @vcPostalCode,
		@numState = @numState,
		@numCountry = @numCountry,
		@vcCompanyName = @vcCompanyName,
		@numCompanyId = 0,
		@vcAddressName = @vcAddressName
END
  
--Ship Address
IF (SELECT COUNT(*) FROM OpportunityAddress WHERE numOppID = @numOppID) > 0
BEGIN
	SELECT  
		@vcStreet=ISNULL(vcShipStreet,''),
		@vcCity=ISNULL(vcShipCity,''),
		@vcPostalCode=ISNULL(vcShipPostCode,''),
		@numState=ISNULL(numShipState,0),
		@numCountry=ISNULL(numShipCountry,0),
		@vcAddressName=vcAddressName
    FROM  
		dbo.OpportunityAddress
	WHERE   
		numOppID = @numOppID
 
  	EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numNewOppID,
		@byteMode = 1,
		@vcStreet = @vcStreet,
		@vcCity = @vcCity,
		@vcPostalCode = @vcPostalCode,
		@numState = @numState,
		@numCountry = @numCountry,
		@vcCompanyName = @vcCompanyName,
		@numCompanyId =@numCompanyId,
		@vcAddressName = @vcAddressName
END


-- DELETE ITEM LEVEL CRV TAX TYPES
DELETE FROM OpportunityMasterTaxItems WHERE numOppId=@numNewOppID AND numTaxItemID = 1

-- INSERT ITEM LEVEL CRV TAX TYPES
INSERT INTO OpportunityMasterTaxItems
(
	numOppId,
	numTaxItemID,
	fltPercentage,
	tintTaxType,
	numTaxID
) 
SELECT 
	OI.numOppId,
	1,
	TEMPTax.decTaxValue,
	TEMPTax.tintTaxType,
	TEMPTax.numTaxID
FROM 
	OpportunityItems OI
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
OUTER APPLY
(
	SELECT
		decTaxValue,
		tintTaxType,
		numTaxID
	FROM
		dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,OI.numItemCode,IT.numTaxItemID,@numNewOppID,1,NULL)
) AS TEMPTax
WHERE
	OI.numOppId = @numNewOppID
	AND IT.numTaxItemID = 1 -- CRV TAX
GROUP BY
	OI.numOppId,
	TEMPTax.decTaxValue,
	TEMPTax.tintTaxType,
	TEMPTax.numTaxID

--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numNewOppID AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numNewOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID
) 
SELECT 
	@numNewOppID,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID=IT.numTaxItemID 
WHERE 
	OI.numOppId=@numNewOppID AND 
	IT.bitApplicable=1 AND 
	OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numNewOppID)
UNION
SELECT 
	@numNewOppID,
	OI.numoppitemtCode,
	0,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numNewOppID AND 
	I.bitTaxable=1 AND
	OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numNewOppID)
UNION
SELECT
	@numNewOppID,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
FROM
	dbo.OpportunityItems OI 
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
INNER JOIN
	TaxDetails TD
ON
	TD.numTaxID = IT.numTaxID
	AND TD.numDomainId = @numDomainId
WHERE
	OI.numOppId = @numNewOppID
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numNewOppID)

  
UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numNewOppID,@Date,0) WHERE numOppId=@numNewOppID


IF @numNewOppID > 0
BEGIN
	-- Insert newly created opportunity to transaction
	INSERT INTO RecurrenceTransaction ([numRecConfigID],[numRecurrOppID],[dtCreatedDate]) VALUES (@numRecConfigID,@numNewOppID,GETDATE())

	-- Insert newly created opportunity to transaction history - Records are not deleted in this table when parent record is delete
	INSERT INTO RecurrenceTransactionHistory ([numRecConfigID],[numRecurrOppID],[dtCreatedDate]) VALUES (@numRecConfigID,@numNewOppID,GETDATE())

	--Get next recurrence date for order 
	DECLARE @dtNextRecurrenceDate DATE = NULL

	SET @dtNextRecurrenceDate = (CASE @numFrequency
								WHEN 1 THEN DATEADD(D,1,@Date) --Daily
								WHEN 2 THEN DATEADD(D,7,@Date) --Weekly
								WHEN 3 THEN DATEADD(M,1,@Date) --Monthly
								WHEN 4 THEN DATEADD(M,4,@Date)  --Quarterly
								END)
	
	--Increase value of number of transaction completed by 1
	UPDATE RecurrenceConfiguration SET numTransaction = ISNULL(numTransaction,0) + 1 WHERE numRecConfigID = @numRecConfigID
			
	PRINT @dtNextRecurrenceDate
	PRINT @dtEndDate

	-- Set recurrence status as completed if next recurrence date is greater than end date
	If @dtNextRecurrenceDate > @dtEndDate
	BEGIN
		UPDATE RecurrenceConfiguration SET bitCompleted = 1 WHERE numRecConfigID = @numRecConfigID
	END
	ELSE -- Set next recurrence date
	BEGIN
		UPDATE RecurrenceConfiguration SET dtNextRecurrenceDate = @dtNextRecurrenceDate WHERE numRecConfigID = @numRecConfigID
	END
END


UPDATE OpportunityMaster SET bitRecurred = 1 WHERE numOppId = @numNewOppID

SET @numRecurOppID = @numNewOppID

END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);

	IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION;
END CATCH

IF @@TRANCOUNT > 0
    COMMIT TRANSACTION;

END  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_createbizdocs')
DROP PROCEDURE usp_createbizdocs
GO
CREATE PROCEDURE [dbo].[USP_CreateBizDocs]                        
(                                              
 @numOppId as numeric(9)=null,                        
 @numBizDocId as numeric(9)=null,                        
 @numUserCntID as numeric(9)=null,                        
 @numOppBizDocsId as numeric(9)=NULL OUTPUT,
 @vcComments as varchar(1000),
 @bitPartialFulfillment as bit,
 @strBizDocItems as text='' ,
 @numShipVia as numeric(9),
 @vcTrackingURL as varchar(1000),
 @dtFromDate AS DATETIME,
 @numBizDocStatus as numeric(9) = 0,
 @bitRecurringBizDoc AS BIT = NULL,
 @numSequenceId AS VARCHAR(50) = '',
 @tintDeferred as tinyint=0,
 @monCreditAmount as money= 0 OUTPUT,
 @ClientTimeZoneOffset as int=0,
 @monDealAmount as money= 0 OUTPUT,
 @bitRentalBizDoc as bit=0,
 @numBizDocTempID as numeric(9)=0,
 @vcTrackingNo AS VARCHAR(500),
 @vcRefOrderNo AS VARCHAR(100),
 @OMP_SalesTaxPercent AS FLOAT = 0, --(joseph) Added to Get Online Marketplace Sales Tax Percentage
 @numFromOppBizDocsId AS NUMERIC(9)=0,
 @bitTakeSequenceId AS BIT=0,
 @bitAllItems AS BIT=0,
 @bitNotValidateBizDocFulfillment AS BIT=0,
 @fltExchangeRateBizDoc AS FLOAT=0,
 @bitRecur BIT = 0,
 @dtStartDate DATE = NULL,
 @dtEndDate DATE = NULL,
 @numFrequency SMALLINT = 0,
 @vcFrequency VARCHAR(20) = '',
 @numRecConfigID NUMERIC(18,0) = 0,
 @bitDisable bit = 0,
 @bitInvoiceForDeferred BIT = 0
)                        
AS 
BEGIN TRY
	DECLARE @hDocItem as INTEGER
	DECLARE @numDivisionID AS NUMERIC(9)
	DECLARE @numDomainID NUMERIC(9)
	DECLARE @tintOppType AS TINYINT	
	DECLARE @numFulfillmentOrderBizDocId NUMERIC(9);
	DECLARE @bitShippingGenerated BIT=0;



	SET @numFulfillmentOrderBizDocId=296 
	    
	IF ISNULL(@fltExchangeRateBizDoc,0)=0
		SELECT @fltExchangeRateBizDoc=fltExchangeRate FROM OpportunityMaster where numOppID=@numOppId
	
	SELECT 
		@numDomainID=numDomainID,
		@numDivisionID=numDivisionID,
		@tintOppType=tintOppType 
	FROM 
		OpportunityMaster 
	WHERE 
		numOppID=@numOppId

	IF((SELECT COUNT(numBizDocId) FROM OpportunityBizDocs WHERE bitShippingGenerated=1 AND numOppId=@numOppId)=0)
	BEGIN
		IF((SELECT TOP 1 CAST(numDefaultSalesShippingDoc AS int) FROM Domain WHERE numDomainId=@numDomainID)=@numBizDocId)
		BEGIN
			SET @bitShippingGenerated=1
		END
	END

	IF @numBizDocTempID=0
	BEGIN
		SELECT 
			@numBizDocTempID=numBizDocTempID 
		FROM 
			BizDocTemplate 
		WHERE 
			numBizDocId=@numBizDocId 
			AND numDomainID=@numDomainID and numOpptype=@tintOppType 
			AND tintTemplateType=0 
			AND isnull(bitDefault,0)=1 
			AND isnull(bitEnabled,0)=1
	END

	IF @numOppBizDocsId=0
	BEGIN
		IF (
			(@bitPartialFulfillment = 1 AND NOT EXISTS (SELECT * FROM   OpportunityBizDocs WHERE  numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 0))
			 OR (NOT EXISTS (SELECT * FROM   OpportunityBizDocs WHERE  numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 0)
				 AND NOT EXISTS (SELECT * FROM   OpportunityBizDocs WHERE  numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 1))
			 OR (@bitRecurringBizDoc = 1) -- Added new flag to add unit qty in case of recurring bizdoc
			 OR (@numFromOppBizDocsId >0)
			)
		BEGIN                      
			DECLARE @tintShipped AS TINYINT	
			DECLARE @dtShipped AS DATETIME
			DECLARE @bitAuthBizdoc AS BIT
        
			SELECT 
				@tintShipped=ISNULL(tintShipped,0),
				@numDomainID=numDomainID 
			FROM 
				OpportunityMaster 
			WHERE numOppId=@numOppId                        
		
			IF @tintOppType = 1 AND @bitNotValidateBizDocFulfillment=0
				EXEC USP_ValidateBizDocFulfillment @numDomainID,@numOppId,@numFromOppBizDocsId,@numBizDocId
		

			IF @tintShipped =1 
				SET @dtShipped = GETUTCDATE();
			ELSE
				SET @dtShipped = null;
			IF @tintOppType = 1 AND (SELECT numAuthoritativeSales FROM [AuthoritativeBizDocs] WHERE numDomainID = @numDomainID) = @numBizDocId
				SET @bitAuthBizdoc = 1
			ELSE if @tintOppType = 2 AND (SELECT numAuthoritativePurchase FROM [AuthoritativeBizDocs] WHERE numDomainID = @numDomainID) = @numBizDocId
				SET @bitAuthBizdoc = 1
			ELSE 
				SET @bitAuthBizdoc = 0
		
			Declare @dtCreatedDate as datetime;SET @dtCreatedDate=Getutcdate()
			IF @tintDeferred=1
			BEGIn
				SET @dtCreatedDate=DateAdd(minute, @ClientTimeZoneOffset,@dtFromDate)
			END
	
			IF @bitTakeSequenceId=1
			BEGIN
				--Sequence #
				CREATE TABLE #tempSequence (numSequenceId bigint )
				INSERT INTO #tempSequence EXEC [dbo].[USP_GetScalerValue] @numDomainID ,33,@numBizDocId,0
				SELECT @numSequenceId =ISNULL(numSequenceId,0) FROM #tempSequence
				DROP TABLE #tempSequence
			
				--BizDoc Template ID
				CREATE TABLE #tempBizDocTempID (numBizDocTempID NUMERIC(9) )
				INSERT INTO #tempBizDocTempID EXEC [dbo].[USP_GetBizDocTemplateList] @numDomainID,@numBizDocId,1,1
				SELECT @numBizDocTempID =ISNULL(numBizDocTempID,0) FROM #tempBizDocTempID
				DROP TABLE #tempBizDocTempID
			END	
			
			
			IF @numFromOppBizDocsId>0 --If Created from Sales Fulfillment Workflow
			BEGIN
				INSERT INTO OpportunityBizDocs
					   (numOppId,numBizDocId,numCreatedBy,dtCreatedDate,numModifiedBy,dtModifiedDate,vcComments,bitPartialFulfilment,
						monShipCost,numShipVia,vcTrackingURL,dtFromDate,numBizDocStatus,[dtShippedDate],
						[bitAuthoritativeBizDocs],tintDeferred,bitRentalBizDoc,numBizDocTempID,/*vcTrackingNo,vcShippingMethod,dtDeliveryDate,*/vcRefOrderNo,numSequenceId,numBizDocStatusOLD,bitAutoCreated,[numMasterBizdocSequenceID],bitShippingGenerated)
				SELECT @numOppId,@numBizDocId,@numUserCntID,Getutcdate(),@numUserCntID,Getutcdate(),@vcComments,@bitPartialFulfillment,
						monShipCost,numShipVia,vcTrackingURL,dtFromDate,0,[dtShippedDate],
						@bitAuthBizdoc,tintDeferred,bitRentalBizDoc,@numBizDocTempID,/*vcTrackingNo,vcShippingMethod,dtDeliveryDate,*/vcRefOrderNo,@numSequenceId,0,1,@numSequenceId,@bitShippingGenerated
				FROM OpportunityBizDocs OBD WHERE numOppId=@numOppId AND OBD.numOppBizDocsId=@numFromOppBizDocsId     
			END
		ELSE
		BEGIN
			INSERT INTO OpportunityBizDocs
                   (numOppId,numBizDocId,
                    --vcBizDocID,
                    numCreatedBy,
                    dtCreatedDate,
                    numModifiedBy,
                    dtModifiedDate,
                    vcComments,
                    bitPartialFulfilment,
                    --bitDiscountType,
                    --fltDiscount,
                    --monShipCost,
                    numShipVia,
                    vcTrackingURL,
                    --bitBillingTerms,
                    --intBillingDays,
                    dtFromDate,
                    --bitInterestType,
                    --fltInterest,
--                    numShipDoc,
                    numBizDocStatus,
                    --vcBizDocName,
                    [dtShippedDate],
                    [bitAuthoritativeBizDocs],tintDeferred,bitRentalBizDoc,numBizDocTempID,vcTrackingNo,
                    --vcShippingMethod,dtDeliveryDate,
                    vcRefOrderNo,numSequenceId,numBizDocStatusOLD,fltExchangeRateBizDoc,bitAutoCreated,[numMasterBizdocSequenceID],bitShippingGenerated)
        VALUES     (@numOppId,
                    @numBizDocId,
                    --@vcBizDocID,
                    @numUserCntID,
                    @dtCreatedDate,
                    @numUserCntID,
                    Getutcdate(),
                    @vcComments,
                    @bitPartialFulfillment,
                    --@bitDiscountType,
                    --@fltDiscount,
                    --@monShipCost,
                    @numShipVia,
                    @vcTrackingURL,
                    --@bitBillingTerms,
                    --@intBillingDays,
                    @dtFromDate,
                    --@bitInterestType,
                    --@fltInterest,
--                    @numShipDoc,
                    @numBizDocStatus,--@numBizDocStatus,--@numBizDocStatus,
                    --@vcBizDocName,
                    @dtShipped,
                    ISNULL(@bitAuthBizdoc,0),@tintDeferred,@bitRentalBizDoc,@numBizDocTempID,@vcTrackingNo,
                    --@vcShippingMethod,@dtDeliveryDate,
                    @vcRefOrderNo,@numSequenceId,0,@fltExchangeRateBizDoc,0,@numSequenceId,@bitShippingGenerated)
        END
        
		SET @numOppBizDocsId = @@IDENTITY
		
	--Added By:Sachin Sadhu||Date:27thAug2014
	--Purpose :MAke entry in WFA queue 
IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 )
	BEGIN
		EXEC 
		USP_ManageOpportunityAutomationQueue			
		@numOppQueueID = 0, -- numeric(18, 0)							
		@numDomainID = @numDomainID, -- numeric(18, 0)							
		@numOppId = @numOppID, -- numeric(18, 0)							
		@numOppBizDocsId = @@IDENTITY, -- numeric(18, 0)							
		@numOrderStatus = 0, -- numeric(18, 0)					
		@numUserCntID = @numUserCntID, -- numeric(18, 0)					
		@tintProcessStatus = 1, -- tinyint						
		@tintMode = 1 -- TINYINT
   END 
   --end of script
		--Deferred BizDocs : Create Recurring entry only for create Account Journal
		if @tintDeferred=1
		BEGIN
			exec USP_RecurringAddOpportunity @numOppId,@numOppBizDocsId,0,4,@dtCreatedDate,0,@numDomainID,0,0,100,''
		END

		-- Update name template set for bizdoc
		--DECLARE @tintType TINYINT
		--SELECT @tintType = CASE @tintOppType WHEN 1 THEN 3 WHEN 2 THEN 4 END;
		EXEC dbo.USP_UpdateBizDocNameTemplate @tintOppType,@numDomainID,@numOppBizDocsId

		IF @numFromOppBizDocsId>0
		BEGIN
			insert into                       
		   OpportunityBizDocItems                                                                          
  		   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes,/*vcTrackingNo,*/bitEmbeddedCost,monEmbeddedCost,/*monShipCost,vcShippingMethod,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate,tintTrackingStatus)
		   select @numOppBizDocsId,OBDI.numOppItemID,OI.numItemCode,OI.numUnitHour,OI.monPrice,OI.monTotAmount,OI.vcItemDesc,OI.numWarehouseItmsID,OBDI.vcType,
		   OBDI.vcAttributes,OBDI.bitDropShip,OBDI.bitDiscountType,OI.fltDiscount,OI.monTotAmtBefDiscount,OBDI.vcNotes,/*OBDI.vcTrackingNo,*/
		   OBDI.bitEmbeddedCost,OBDI.monEmbeddedCost,/*OBDI.monShipCost,OBDI.vcShippingMethod,OBDI.dtDeliveryDate,*/OBDI.dtRentalStartDate,OBDI.dtRentalReturnDate,OBDI.tintTrackingStatus
		   from OpportunityBizDocs OBD JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID 
		   JOIN OpportunityItems OI ON OBDI.numOppItemID=OI.numoppitemtCode
		   JOIN Item I ON OBDI.numItemCode=I.numItemCode
		   where  OBD.numOppId=@numOppId AND OBD.numOppBizDocsId=@numFromOppBizDocsId
		   AND OI.numoppitemtCode NOT IN(SELECT OBDI.numOppItemID FROM OpportunityBizDocs OBD JOIN dbo.OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocId WHERE numOppId=@numOppId AND numBizDocId=@numBizDocId)
		END
		ELSE IF (@bitPartialFulfillment=1 and DATALENGTH(@strBizDocItems)>2)
		BEGIN
			IF DATALENGTH(@strBizDocItems)>2
			BEGIN
				EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strBizDocItems 

					IF @bitRentalBizDoc=1
					BEGIN
						update OI set OI.monPrice=OBZ.monPrice,OI.monTotAmount=OBZ.monTotAmount,OI.monTotAmtBefDiscount=OBZ.monTotAmount
						from OpportunityItems OI
						Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
							WITH ( OppItemID numeric(9),Quantity FLOAT,monPrice money,monTotAmount money)) OBZ
						on OBZ.OppItemID=OI.numoppitemtCode
						where OBZ.OppItemID=OI.numoppitemtCode
					END
	
					insert into                       
				   OpportunityBizDocItems                                                                          
				   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes,/*vcTrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate)
				   
				   select @numOppBizDocsId,numoppitemtCode,numItemCode,Quantity,CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmount/numUnitHour)*Quantity END,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,
				   case when bitDiscountType=0 then fltDiscount when bitDiscountType=1 then (fltDiscount/numUnitHour)*Quantity else fltDiscount end ,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmtBefDiscount/numUnitHour)*Quantity END,Notes,/*TrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate  from OpportunityItems OI
				   Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
				   WITH                       
				   (OppItemID numeric(9),                               
				    Quantity FLOAT,
					Notes varchar(500),
					monPrice MONEY,
					dtRentalStartDate datetime,dtRentalReturnDate datetime
					)) OBZ
				   on OBZ.OppItemID=OI.numoppitemtCode
				   where  numOppId=@numOppId AND ISNULL(OI.numUnitHour,0)>0 AND ISNULL(OI.numUnitHour,0)>0
				   
				EXEC sp_xml_removedocument @hDocItem 
			END
		END
		ELSE
		BEGIN
			IF @numBizDocId <> 304 AND @bitAuthBizdoc = 1 AND @bitInvoiceForDeferred = 1
			BEGIN
				DECLARE @numTop1DeferredBizDocID AS NUMERIC(18,0)

				SELECT TOP 1
					@numTop1DeferredBizDocID = 	OpportunityBizDocs.numOppBizDocsId
				FROM 
					OpportunityBizDocs 
				WHERE 
					numOppId=@numOppId 
					AND numOppBizDocsId NOT IN (SELECT ISNULL(numDeferredBizDocID,0) FROM OpportunityBizDocs WHERE numOppId=@numOppId)
					AND OpportunityBizDocs.numBizDocId = 304 -- 304 IS DEFERRED INVOICE BIZDOC
				ORDER BY
					numOppBizDocsId

				INSERT INTO OpportunityBizDocItems
  				(
					numOppBizDocID,
					numOppItemID,
					numItemCode,
					numUnitHour,
					monPrice,
					monTotAmount,
					vcItemDesc,
					numWarehouseItmsID,
					vcType,
					vcAttributes,
					bitDropShip,
					bitDiscountType,
					fltDiscount,
					monTotAmtBefDiscount
				)
				SELECT 
					@numOppBizDocsId,
					numoppitemtCode,
					OI.numItemCode,
					(CASE @bitRecurringBizDoc WHEN 1 THEN 1 ELSE ISNULL(TempBizDoc.numUnitHour,0) END) AS numUnitHour,
					OI.monPrice,
					(CASE @bitRecurringBizDoc 
						WHEN 1 THEN monPrice * 1 
						ELSE (OI.monTotAmount/OI.numUnitHour) * ISNULL(TempBizDoc.numUnitHour,0)
					END) AS monTotAmount,
					vcItemDesc,
					numWarehouseItmsID,
					vcType,vcAttributes,
					bitDropShip,
					bitDiscountType,
					(CASE 
						WHEN bitDiscountType=0 THEN fltDiscount 
						WHEN bitDiscountType=1 THEN ((fltDiscount/OI.numUnitHour) * ISNULL(TempBizDoc.numUnitHour,0))
						ELSE fltDiscount 
					END),
					(monTotAmtBefDiscount/OI.numUnitHour) * ISNULL(TempBizDoc.numUnitHour,0) 
				FROM 
					OpportunityItems OI
				JOIN 
					[dbo].[Item] AS I 
				ON 
					I.[numItemCode] = OI.[numItemCode]
				INNER JOIN
					(
						SELECT 
							OpportunityBizDocItems.numOppItemID,
							OpportunityBizDocItems.numUnitHour
						FROM 
							OpportunityBizDocs 
						JOIN 
							OpportunityBizDocItems 
						ON 
							OpportunityBizDocs.numOppBizDocsId  = OpportunityBizDocItems.numOppBizDocId
						WHERE 
							numOppId=@numOppId 
							AND OpportunityBizDocs.numOppBizDocsId = @numTop1DeferredBizDocID
					) TempBizDoc
				ON
					TempBizDoc.numOppItemID = OI.numoppitemtCode
				WHERE  
					numOppId=@numOppId 
					AND ISNULL(OI.numUnitHour,0) > 0
					AND (@bitRecurringBizDoc = 1 OR ISNULL(TempBizDoc.numUnitHour,0) > 0)


				UPDATE OpportunityBizDocs SET numDeferredBizDocID = @numTop1DeferredBizDocID WHERE numOppBizDocsId = @numOppBizDocsId
			END
			ELSE
			BEGIN
		   		INSERT INTO OpportunityBizDocItems                                                                          
  				(
					numOppBizDocID,
					numOppItemID,
					numItemCode,
					numUnitHour,
					monPrice,
					monTotAmount,
					vcItemDesc,
					numWarehouseItmsID,
					vcType,
					vcAttributes,
					bitDropShip,
					bitDiscountType,
					fltDiscount,
					monTotAmtBefDiscount
				)
				SELECT 
					@numOppBizDocsId,
					numoppitemtCode,
					OI.numItemCode,
					(CASE @bitRecurringBizDoc 
						WHEN 1 
						THEN 1 
						ELSE (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0)) 
					END) AS numUnitHour,
					OI.monPrice,
					(CASE @bitRecurringBizDoc 
						WHEN 1 THEN monPrice * 1 
						ELSE (OI.monTotAmount/OI.numUnitHour) * (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0))
					END) AS monTotAmount,
					vcItemDesc,
					numWarehouseItmsID,
					vcType,vcAttributes,
					bitDropShip,
					bitDiscountType,
					(CASE 
						WHEN bitDiscountType=0 THEN fltDiscount 
						WHEN bitDiscountType=1 THEN ((fltDiscount/OI.numUnitHour) * (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0)))
						ELSE fltDiscount 
					END),
					(monTotAmtBefDiscount/OI.numUnitHour) * (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0)) 
				FROM 
					OpportunityItems OI
				JOIN 
					[dbo].[Item] AS I 
				ON 
					I.[numItemCode] = OI.[numItemCode]
				OUTER APPLY
					(
						SELECT 
							SUM(OBDI.numUnitHour) AS numUnitHour
						FROM 
							OpportunityBizDocs OBD 
						JOIN 
							dbo.OpportunityBizDocItems OBDI 
						ON 
							OBDI.numOppBizDocId  = OBD.numOppBizDocsId
							AND OBDI.numOppItemID = OI.numoppitemtCode
						WHERE 
							numOppId=@numOppId 
							AND 1 = (CASE 
										WHEN @bitAuthBizdoc = 1 
										THEN (CASE WHEN (numBizDocId = 304 OR (numBizDocId = @numBizDocId AND ISNULL(numDeferredBizDocID,0) = 0)) THEN 1 ELSE 0 END)
										WHEN @numBizDocId = 304 --Deferred Income
										THEN (CASE WHEN (numBizDocId = @numBizDocId OR ISNULL(OBD.bitAuthoritativeBizDocs,0) = 1) THEN 1 ELSE 0 END)
										ELSE (CASE WHEN numBizDocId = @numBizDocId THEN 1 ELSE 0 END) 
									END)
					) TempBizDoc
				WHERE  
					numOppId=@numOppId 
					AND ISNULL(OI.numUnitHour,0) > 0
					AND (@bitRecurringBizDoc = 1 OR (ISNULL(OI.numUnitHour,0) - ISNULL(TempBizDoc.numUnitHour,0)) > 0)
			END
		END

		SET @numOppBizDocsID = @numOppBizDocsId
		--select @numOppBizDocsId
	end
		ELSE
		BEGIN
  			SET @numOppBizDocsId=0
			SET @monDealAmount = 0
			SET @monCreditAmount = 0
			RAISERROR ('NOT_ALLOWED',16,1);
		END
	END
	ELSE
	BEGIN
		UPDATE 
			OpportunityBizDocs
		SET    
			numModifiedBy = @numUserCntID,
			dtModifiedDate = GETUTCDATE(),
			vcComments = @vcComments,
			numShipVia = @numShipVia,
			vcTrackingURL = @vcTrackingURL,
			dtFromDate = @dtFromDate,
			numBizDocStatus = @numBizDocStatus,
			numSequenceId=@numSequenceId,
			numBizDocTempID=@numBizDocTempID,
			vcTrackingNo=@vcTrackingNo,
			vcRefOrderNo=@vcRefOrderNo,
			fltExchangeRateBizDoc=@fltExchangeRateBizDoc,
			bitShippingGenerated=@bitShippingGenerated
		WHERE  
			numOppBizDocsId = @numOppBizDocsId

		--Added By:Sachin Sadhu||Date:27thAug2014
		--Purpose :MAke entry in WFA queue 
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 )
		BEGIN
			EXEC 
			USP_ManageOpportunityAutomationQueue			
			@numOppQueueID = 0, -- numeric(18, 0)							
			@numDomainID = @numDomainID, -- numeric(18, 0)							
			@numOppId = @numOppID, -- numeric(18, 0)							
			@numOppBizDocsId = @numOppBizDocsId, -- numeric(18, 0)							
			@numOrderStatus = 0, -- numeric(18, 0)					
			@numUserCntID = @numUserCntID, -- numeric(18, 0)					
			@tintProcessStatus = 1, -- tinyint						
			@tintMode = 1 -- TINYINT
		END 
		--end of code

   		IF DATALENGTH(@strBizDocItems)>2
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strBizDocItems 

            DELETE FROM 
				OpportunityBizDocItems  
			WHERE 
				numOppItemID NOT IN 
				(
					SELECT 
						OppItemID 
					FROM OPENXML 
						(@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
					WITH                       
					   (OppItemID numeric(9))
				)
				AND numOppBizDocID=@numOppBizDocsId  
                  
                     
			IF @bitRentalBizDoc=1
			BEGIN
				update OI set OI.monPrice=OBZ.monPrice,OI.monTotAmount=OBZ.monTotAmount,OI.monTotAmtBefDiscount=OBZ.monTotAmount
				from OpportunityItems OI
				Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
					WITH ( OppItemID numeric(9),Quantity FLOAT,monPrice money,monTotAmount money)) OBZ
				on OBZ.OppItemID=OI.numoppitemtCode
				where OBZ.OppItemID=OI.numoppitemtCode
			END

			update OBI set OBI.numUnitHour= Quantity,OBI.monPrice=CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,OBI.monTotAmount=CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (OI.monTotAmount/OI.numUnitHour)*Quantity END,OBI.vcNotes= Notes,
			OBI.fltDiscount=case when OI.bitDiscountType=0 then OI.fltDiscount when OI.bitDiscountType=1 then (OI.fltDiscount/OI.numUnitHour)*Quantity else OI.fltDiscount end ,
			OBI.monTotAmtBefDiscount=(OI.monTotAmtBefDiscount/OI.numUnitHour)*Quantity ,                                                                    
			/*OBI.vcTrackingNo=TrackingNo,OBI.vcShippingMethod=OBZ.vcShippingMethod,OBI.monShipCost=OBZ.monShipCost,OBI.dtDeliveryDate=OBZ.dtDeliveryDate*/
			OBI.dtRentalStartDate=OBZ.dtRentalStartDate,OBI.dtRentalReturnDate=OBZ.dtRentalReturnDate
			from OpportunityBizDocItems OBI
			join OpportunityItems OI
			on OBI.numOppItemID=OI.numoppitemtCode
			Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
			WITH                       
			( OppItemID numeric(9),                                     
			Quantity FLOAT,
			Notes varchar(500),
			monPrice MONEY,
			dtRentalStartDate datetime,dtRentalReturnDate datetime
			)) OBZ
			on OBZ.OppItemID=OI.numoppitemtCode
			where  OI.numOppId=@numOppId and OBI.numOppBizDocID=@numOppBizDocsId AND ISNULL(OI.numUnitHour,0)>0


            insert into                       
			OpportunityBizDocItems                                                                          
			(numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes/*,vcTrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate*/)                      
			select @numOppBizDocsId,numoppitemtCode,numItemCode,Quantity,CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmount/numUnitHour)*Quantity END,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,
			case when bitDiscountType=0 then fltDiscount when bitDiscountType=1 then (fltDiscount/numUnitHour)*Quantity else fltDiscount end ,(monTotAmtBefDiscount/numUnitHour)*Quantity,Notes/*,TrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate*/ from OpportunityItems OI
			Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
			WITH                       
			( OppItemID numeric(9),                                     
			Quantity FLOAT,
			Notes varchar(500),
			monPrice MONEY
			--TrackingNo varchar(500),
			--vcShippingMethod varchar(100),
			--monShipCost money,
			--dtDeliveryDate DATETIME
			)) OBZ
			on OBZ.OppItemID=OI.numoppitemtCode and OI.numoppitemtCode not in (select numOppItemID from OpportunityBizDocItems where numOppBizDocID =@numOppBizDocsId)
			where  numOppId=@numOppId AND ISNULL(OI.numUnitHour,0)>0
                 
			EXEC sp_xml_removedocument @hDocItem 
		END
	END

	IF @numOppBizDocsId>0
	BEGIN
		SELECT @monDealAmount=isnull(dbo.[GetDealAmount](@numOppId,GETUTCDATE(),@numOppBizDocsId),0)

		UPDATE 
			OpportunityBizDocs 
		SET 
			vcBizDocID=vcBizDocName + ISNULL(@numSequenceId,''),
			monDealAmount=@monDealAmount
		WHERE  
			numOppBizDocsId = @numOppBizDocsId
	END

	-- 1 if recurrence is enabled for authorative bizdoc
	IF ISNULL(@bitRecur,0) = 1
	BEGIN
		-- First Check if receurrece is already created for sales order because user should not 
		-- be able to create recurrence on both order and invoice
		IF (SELECT COUNT(*) FROM RecurrenceConfiguration WHERE numOppID = @numOppId AND numType=1) > 0 
		BEGIN
			RAISERROR ('RECURRENCE_ALREADY_CREATED_FOR_ORDER',16,1);
		END
		--User can not create recurrence on bizdoc where all items are added to bizdoc
		ELSE IF (SELECT dbo.CheckAllItemQuantityAddedToBizDoc(@numOppId,@numOppBizDocsID)) = 1
		BEGIN
			RAISERROR ('RECURRENCE_NOT_ALLOWED_ALL_ITEM_QUANTITY_ADDED_TO_INVOICE',16,1);
		END
		ELSE
		BEGIN

			EXEC USP_RecurrenceConfiguration_Insert
				@numDomainID = @numDomainId,
				@numUserCntID = @numUserCntId,
				@numOppID = @numOppID,
				@numOppBizDocID = @numOppBizDocsId,
				@dtStartDate = @dtStartDate,
				@dtEndDate =NULL,
				@numType = 2,
				@vcType = 'Invoice',
				@vcFrequency = @vcFrequency,
				@numFrequency = @numFrequency

			UPDATE OpportunityMaster SET vcRecurrenceType = 'Invoice' WHERE numOppId = @numOppID
		END
	END
	ELSE IF ISNULL(@numRecConfigID,0) > 0 AND ISNULL(@bitDisable,0)= 1
	BEGIN
		UPDATE
			RecurrenceConfiguration
		SET
			bitDisabled = 1,
			numDisabledBy = @numUserCntId,
			dtDisabledDate = GETDATE()
		WHERE	
			numRecConfigID = @numRecConfigID
	END 
 
END TRY
BEGIN CATCH
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return error
    -- information about the original error that caused
    -- execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
               );
END CATCH;
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Domain_GetPrinterDetail')
DROP PROCEDURE USP_Domain_GetPrinterDetail
GO
CREATE PROCEDURE [dbo].[USP_Domain_GetPrinterDetail]
(                        
	@numDomainID AS NUMERIC(18,0)
)                        
AS 
BEGIN
	SELECT
		ISNULL(vcPrinterIPAddress,'') vcPrinterIPAddress,
		ISNULL(vcPrinterPort,'') vcPrinterPort
	FROM
		Domain
	WHERE
		numDomainId=@numDomainID
END
/****** Object:  StoredProcedure [dbo].[USP_GetBizDocInventoryItems]    Script Date: 05/07/2009 17:36:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- EXEC USP_GetBizDocInventoryItems 2172,1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetBizDocInventoryItems')
DROP PROCEDURE USP_GetBizDocInventoryItems
GO
CREATE PROCEDURE [dbo].[USP_GetBizDocInventoryItems]
--                @numOppId        AS NUMERIC(9)  = NULL,
                @numOppBizDocID AS NUMERIC(9)  ,
                @numDomainID     AS NUMERIC(9)  = 0
AS
  BEGIN
  
    SELECT 
			BI.[numOppBizDocItemID],
			I.[numItemCode],
			WI.numWareHouseID,
			I.vcItemName,
           I.vcModelID,
           ISNULL(I.fltWeight,0) fltWeight,
           ISNULL(I.fltHeight,0) fltHeight,
           ISNULL(I.fltWidth,0) fltWidth,
           ISNULL(I.fltLength,0) fltLength,
           0 intNoOfBox,
           BI.vcItemDesc,
           (ISNULL(BI.[numUnitHour],0) * ISNULL(fltWeight,0)) AS TotalWeight,
		   ISNULL(BI.[numUnitHour],0) AS numUnitHour,
		   ISNULL(I.bitContainer,0) AS bitContainer,ISNULL(I.numNoItemIntoContainer,0) AS numNoItemIntoContainer,
		   ISNULL(I.numContainer,0) AS numContainer
    FROM   
		OpportunityBizDocItems AS BI
    INNER JOIN 
		Item AS I
    ON 
		BI.numItemCode = I.numItemCode
	INNER JOIN
		WareHouseItems WI
	ON
		BI.numWarehouseItmsID = WI.numWareHouseItemID
    WHERE  
		BI.[numOppBizDocID] = @numOppBizDocID
        AND I.[charItemType] = 'P' --Only Inventory Items
        AND I.[numDomainID] = @numDomainID
        AND ISNULL((SELECT bitDropShip FROM dbo.OpportunityItems WHERE numoppitemtCode = BI.numOppItemID AND numItemCode = I.numItemCode),0) = 0
           
	-- GET CONTAINER BY WAREHOUSE AND NOOFITEMSCANBEADDED
	SELECT
		T1.numContainer
		,T1.numWareHouseID
		,T1.numNoItemIntoContainer
		,T2.numTotalContainer
		,0 AS bitItemAdded
		,T2.fltWeight
		,T2.fltHeight
		,T2.fltWidth
		,T2.fltLength
	FROM
	(
		SELECT 
			I.numContainer
			,WI.numWareHouseID
			,I.numNoItemIntoContainer
		FROM 
			OpportunityBizDocItems OBI
		INNER JOIN 
			Item AS I
		ON 
			OBI.numItemCode = I.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OBI.numWarehouseItmsID = WI.numWareHouseItemID
		WHERE
			numOppBizDocID=@numOppBizDocID
			AND I.charItemType ='P'
			AND ISNULL(numContainer,0) > 0
		GROUP BY
			I.numContainer
			,WI.numWareHouseID
			,I.numNoItemIntoContainer
	) AS T1
	INNER JOIN
	(
		SELECT
			I.numItemCode
			,WI.numWareHouseID
			,SUM(OBI.numUnitHour) numTotalContainer
			,ISNULL(I.fltWeight,0) fltWeight
			,ISNULL(I.fltHeight,0) fltHeight
			,ISNULL(I.fltWidth,0) fltWidth
			,ISNULL(I.fltLength,0) fltLength
		FROM 
			OpportunityBizDocItems OBI
		INNER JOIN 
			Item AS I
		ON 
			OBI.numItemCode = I.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OBI.numWarehouseItmsID = WI.numWareHouseItemID
		WHERE
			numOppBizDocID=@numOppBizDocID
			AND I.charItemType ='P'
			AND ISNULL(I.bitContainer,0) = 1
		GROUP BY
			I.numItemCode
			,WI.numWareHouseID
			,ISNULL(I.fltWeight,0)
			,ISNULL(I.fltHeight,0)
			,ISNULL(I.fltWidth,0)
			,ISNULL(I.fltLength,0)
	) AS T2
	ON
		T1.numContainer = T2.numItemCode
END
--created by anoop jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdomaindetails')
DROP PROCEDURE usp_getdomaindetails
GO
CREATE PROCEDURE [dbo].[USP_GetDomainDetails]                                          
@numDomainID as numeric(9)=0                                          
as           

DECLARE @listIds VARCHAR(MAX)
SELECT @listIds = COALESCE(@listIds+',' ,'') + CAST(numUserID AS VARCHAR(100)) FROM UnitPriceApprover WHERE numDomainID = @numDomainID
   
DECLARE @canChangeDiferredIncomeSelection AS BIT = 1

IF EXISTS(SELECT 
			OB.numOppBizDocsId 
		FROM 
			OpportunityBizDocs OB 
		INNER JOIN 
			OpportunityMaster OM 
		ON 
			OB.numOppId=OM.numOppId 
		WHERE 
			numBizDocId=304 
			AND OM.numDomainId=@numDomainID)
BEGIN
	SET @canChangeDiferredIncomeSelection = 0
END   
   
                               
select                                           
vcDomainName,                                           
isnull(vcDomainDesc,'') vcDomainDesc,                                           
isnull(bitExchangeIntegration,0) bitExchangeIntegration,                                           
isnull(bitAccessExchange,0) bitAccessExchange,                                           
isnull(vcExchUserName,'') vcExchUserName,                                           
isnull(vcExchPassword,'') vcExchPassword,                                           
isnull(vcExchPath,'') vcExchPath,                                           
isnull(vcExchDomain,'') vcExchDomain,                                         
tintCustomPagingRows,                                         
vcDateFormat,                                         
numDefCountry,                                         
tintComposeWindow,                                         
sintStartDate,                                         
sintNoofDaysInterval,                                         
tintAssignToCriteria,                                         
bitIntmedPage,
ISNULL(D.numCost,0) as numCost,                                         
tintFiscalStartMonth,                                    
--isnull(bitDeferredIncome,0) as bitDeferredIncome,                                  
isnull(tintPayPeriod,0) as tintPayPeriod,                                
isnull(numCurrencyID,0) as numCurrencyID,                            
isnull(vcCurrency,'') as vcCurrency,                      
isnull(charUnitSystem,'') as charUnitSystem ,                    
isnull(vcPortalLogo,'') as vcPortalLogo,                  
isnull(tintChrForComSearch,2) as tintChrForComSearch,                
isnull(tintChrForItemSearch,'') as tintChrForItemSearch,            
isnull(intPaymentGateWay,0) as intPaymentGateWay  ,        
 case when bitPSMTPServer= 1 then vcPSMTPServer else '' end as vcPSMTPServer  ,              
 case when bitPSMTPServer= 1 then  isnull(numPSMTPPort,0)  else 0 end as numPSMTPPort          
,isnull(bitPSMTPAuth,0) bitPSMTPAuth        
,isnull([vcPSmtpPassword],'')vcPSmtpPassword        
,isnull(bitPSMTPSSL,0) bitPSMTPSSL     
,isnull(bitPSMTPServer,0) bitPSMTPServer  ,isnull(vcPSMTPUserName,'') vcPSMTPUserName,      
--isnull(numDefaultCategory,0) numDefaultCategory,
isnull(numShipCompany,0)   numShipCompany,
isnull(bitAutoPopulateAddress,0) bitAutoPopulateAddress,
isnull(tintPoulateAddressTo,0) tintPoulateAddressTo,
isnull(bitMultiCompany,0) bitMultiCompany,
isnull(bitMultiCurrency,0) bitMultiCurrency,
ISNULL(bitCreateInvoice,0) bitCreateInvoice,
ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
ISNULL([bitSaveCreditCardInfo],0) bitSaveCreditCardInfo,
ISNULL([intLastViewedRecord],10) intLastViewedRecord,
ISNULL([tintCommissionType],1) tintCommissionType,
ISNULL([tintBaseTax],2) tintBaseTax,
ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
ISNULL(bitEmbeddedCost,0) bitEmbeddedCost,
ISNULL(numDefaultSalesOppBizDocId,0) numDefaultSalesOppBizDocId,
ISNULL(tintSessionTimeOut,1) tintSessionTimeOut,
ISNULL(tintDecimalPoints,2) tintDecimalPoints,
ISNULL(bitCustomizePortal,0) bitCustomizePortal,
ISNULL(tintBillToForPO,0) tintBillToForPO,
ISNULL(tintShipToForPO,0) tintShipToForPO,
ISNULL(tintOrganizationSearchCriteria,1) tintOrganizationSearchCriteria,
ISNULL(tintLogin,0) tintLogin,
lower(ISNULL(vcPortalName,'')) vcPortalName,
ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
D.numDomainID,
ISNULL(vcPSMTPDisplayName,'') vcPSMTPDisplayName,
ISNULL(bitGtoBContact,0) bitGtoBContact,
ISNULL(bitBtoGContact,0) bitBtoGContact,
ISNULL(bitGtoBCalendar,0) bitGtoBCalendar,
ISNULL(bitBtoGCalendar,0) bitBtoGCalendar,
ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
ISNULL(bitInlineEdit,0) bitInlineEdit,
ISNULL(bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
ISNULL(bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
isnull(D.bitRentalItem,0) as bitRentalItem,
isnull(D.numRentalItemClass,0) as numRentalItemClass,
isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
ISNULL(D.bitAutoSerialNoAssign,0) as bitAutoSerialNoAssign,
ISNULL(D.bitIsShowBalance,0) AS [bitIsShowBalance],
ISNULL(numIncomeAccID,0) AS [numIncomeAccID],
ISNULL(numCOGSAccID,0) AS [numCOGSAccID],
ISNULL(numAssetAccID,0) AS [numAssetAccID],
ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
ISNULL(D.vcShipToPhoneNo,0) AS vcShipToPhoneNo,
ISNULL(D.vcGAUserEMail,'') AS vcGAUserEMail,
ISNULL(D.vcGAUserPassword,'') AS vcGAUserPassword,
ISNULL(D.vcGAUserProfileId,0) AS vcGAUserProfileId,
ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
ISNULL(D.vcHideTabs,'') AS vcHideTabs,
ISNULL(D.bitUseBizdocAmount,0) AS bitUseBizdocAmount,
ISNULL(D.bitDefaultRateType,0) AS bitDefaultRateType,
ISNULL(D.bitIncludeTaxAndShippingInCommission,0) AS bitIncludeTaxAndShippingInCommission,
/*,ISNULL(D.bitAllowPPVariance,0) AS bitAllowPPVariance*/
ISNULL(D.bitPurchaseTaxCredit,0) AS bitPurchaseTaxCredit,
ISNULL(D.bitLandedCost,0) AS bitLandedCost,
ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
ISNULL(D.numAbovePercent,0) AS numAbovePercent,
ISNULL(D.numAbovePriceField,0) AS numAbovePriceField,
ISNULL(D.numBelowPercent,0) AS numBelowPercent,
ISNULL(D.numBelowPriceField,0) AS numBelowPriceField,
ISNULL(D.numOrderStatusBeforeApproval,0) AS numOrderStatusBeforeApproval,
ISNULL(D.numOrderStatusAfterApproval,0) AS numOrderStatusAfterApproval,
@listIds AS vcUnitPriceApprover,
ISNULL(D.numDefaultSalesPricing,2) AS numDefaultSalesPricing,
ISNULL(D.bitReOrderPoint,0) AS bitReOrderPoint,
ISNULL(D.numReOrderPointOrderStatus,0) AS numReOrderPointOrderStatus,
ISNULL(D.numPODropShipBizDoc,0) AS numPODropShipBizDoc,
ISNULL(D.numPODropShipBizDocTemplate,0) AS numPODropShipBizDocTemplate,
ISNULL(D.IsEnableDeferredIncome,0) AS IsEnableDeferredIncome,
ISNULL(D.bitApprovalforTImeExpense,0) AS bitApprovalforTImeExpense,
ISNULL(D.intTimeExpApprovalProcess,0) AS intTimeExpApprovalProcess,
ISNULL(D.bitApprovalforOpportunity,0) AS bitApprovalforOpportunity,
ISNULL(D.intOpportunityApprovalProcess,0) AS intOpportunityApprovalProcess,
@canChangeDiferredIncomeSelection AS canChangeDiferredIncomeSelection,
ISNULL(CAST(D.numDefaultSalesShippingDoc AS int),0) AS numDefaultSalesShippingDoc,
ISNULL(CAST(D.numDefaultPurchaseShippingDoc AS int),0) AS numDefaultPurchaseShippingDoc,
ISNULL(D.bitchkOverRideAssignto,0) AS bitchkOverRideAssignto,
ISNULL(D.vcPrinterIPAddress,'') vcPrinterIPAddress,
ISNULL(D.vcPrinterPort,'') vcPrinterPort
from Domain D  
left join eCommerceDTL eComm  
on eComm.numDomainID=D.numDomainID  
where (D.numDomainID=@numDomainID OR @numDomainID=-1)


/****** Object:  StoredProcedure [dbo].[usp_GetDuplicateCompany]    Script Date: 07/26/2008 16:17:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdueamount')
DROP PROCEDURE usp_getdueamount
GO
CREATE PROCEDURE [dbo].[USP_GetDueAmount]                      
@numDivisionID numeric(9)=0                      
as                      
declare @Amt as MONEY;set @Amt=0                        
declare @Paid as MONEY;set @Paid=0      
                 
declare @AmountDueSO as money;set @AmountDueSO=0                    
declare @AmountPastDueSO as money;set @AmountPastDueSO=0 

declare @AmountDuePO as money;set @AmountDuePO=0                    
declare @AmountPastDuePO as money;set @AmountPastDuePO=0 
                   
declare @CompanyCredit as money;set @CompanyCredit=0   
          
declare @PCreditMemo as money;set @PCreditMemo=0            
declare @SCreditMemo as money;set @SCreditMemo=0            

DECLARE @UTCtime AS datetime	       
SET	@UTCtime = dbo.[GetUTCDateWithoutTime]()

--Sales Order Total Amount Due                
select @Amt=isnull(sum(OppBD.[monDealAmount]),0) from OpportunityMaster  Opp
join  OpportunityBizDocs OppBD
on    OppBD.numOppID=Opp.numOppID               
where bitAuthoritativeBizDocs=1 and numDivisionID=@numDivisionID and tintOppStatus=1   and tintOpptype=1              
               
select @Paid=isnull(sum(monAmountPaid),0) from OpportunityMaster  Opp
join  OpportunityBizDocs OppBD
on    OppBD.numOppID=Opp.numOppID               
where bitAuthoritativeBizDocs=1 and numDivisionID=@numDivisionID and tintOppStatus=1   and tintOpptype=1               
                
set @AmountDueSO=@Amt-@Paid                
         
--Sales Order Total Amount Past Due                
select @Amt=isnull(sum(OppBD.[monDealAmount]),0) from OpportunityMaster   Opp
join  OpportunityBizDocs OppBD
on    OppBD.numOppID=Opp.numOppID               
where  bitAuthoritativeBizDocs=1 and numDivisionID=@numDivisionID and tintOppStatus=1   and tintOpptype=1              
AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
					 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)),0))  
					 ELSE 0 
				END, OppBD.[dtFromDate]) < @UTCtime
--AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
--					 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0))  
--					 ELSE 0 
--				END, OppBD.[dtFromDate]) < @UTCtime				

select @Paid=isnull(SUM(OppBD.monAmountPaid),0) from OpportunityMaster  Opp
join  OpportunityBizDocs OppBD on    OppBD.numOppID=Opp.numOppID               
where  OppBD.bitAuthoritativeBizDocs=1 and Opp.numDivisionID=@numDivisionID and tintOppStatus=1  and tintOpptype=1               
AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
					 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)),0)) 
					 ELSE 0 
				END, OppBD.[dtFromDate]) <@UTCtime
--AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
--					 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0)) 
--					 ELSE 0 
--				END, OppBD.[dtFromDate]) <@UTCtime				

set @AmountPastDueSO=@Amt-@Paid                    
                    
   
--Purchase Order Total Amount Due                
select @Amt=isnull(sum(OppBD.[monDealAmount]),0) from OpportunityMaster  Opp
join  OpportunityBizDocs OppBD
on    OppBD.numOppID=Opp.numOppID               
where bitAuthoritativeBizDocs=1 and numDivisionID=@numDivisionID and tintOppStatus=1   and tintOpptype=2              
               
select @Paid=isnull(sum(monAmountPaid),0) from OpportunityMaster  Opp
join  OpportunityBizDocs OppBD
on    OppBD.numOppID=Opp.numOppID               
where bitAuthoritativeBizDocs=1 and numDivisionID=@numDivisionID and tintOppStatus=1   and tintOpptype=2              
                
set @AmountDuePO=@Amt-@Paid                
         
--Purchase Order Total Amount Past Due                
select @Amt=isnull(sum(OppBD.[monDealAmount]),0) from OpportunityMaster   Opp
join  OpportunityBizDocs OppBD
on    OppBD.numOppID=Opp.numOppID               
where  bitAuthoritativeBizDocs=1 and numDivisionID=@numDivisionID and tintOppStatus=1   and tintOpptype=2              
AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
					 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)),0))  
					 ELSE 0 
				END, OppBD.[dtFromDate]) <@UTCtime
--AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
--					 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0))  
--					 ELSE 0 
--				END, OppBD.[dtFromDate]) <@UTCtime
				

select @Paid=isnull(SUM(OppBD.monAmountPaid),0) from OpportunityMaster  Opp
join  OpportunityBizDocs OppBD on    OppBD.numOppID=Opp.numOppID               
where  OppBD.bitAuthoritativeBizDocs=1 and Opp.numDivisionID=@numDivisionID and tintOppStatus=1  and tintOpptype=2               
AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
					 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)),0)) 
					 ELSE 0 
				END, OppBD.[dtFromDate]) <@UTCtime
--AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
--					 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0)) 
--					 ELSE 0 
--				END, OppBD.[dtFromDate]) <@UTCtime

set @AmountPastDuePO=@Amt-@Paid  


--Company Cresit Limit                    
select @CompanyCredit=convert(money,case when isnumeric(dbo.fn_GetListItemName(numCompanyCredit))=1 then dbo.fn_GetListItemName(numCompanyCredit) else '0' end)
	 from companyinfo C                    
join divisionmaster d                    
on d.numCompanyID=C.numCompanyID                    
where d.numdivisionid=@numDivisionID                    
                    
 
--Purchase Credit Memo
select @PCreditMemo=ISNULL(BPH.monPaymentAmount,0) - ISNULL (BPH.monAppliedAmount,0)
	 from BillPaymentHeader BPH          
where BPH.numdivisionid=@numDivisionID AND ISNULL(numReturnHeaderID,0)>0                  

--Sales Credit Memo
select @SCreditMemo=ISNULL(DM.monDepositAmount,0) - ISNULL (DM.monAppliedAmount,0)
	 from dbo.DepositMaster DM          
where DM.numdivisionid=@numDivisionID AND tintDepositePage=3 AND ISNULL(numReturnHeaderID,0)>0  

DECLARE @vcShippersAccountNo AS VARCHAR(100)
DECLARE @numDefaultShippingServiceID AS NUMERIC(18,0)
SELECT @vcShippersAccountNo = ISNULL(vcShippersAccountNo,''), @numDefaultShippingServiceID=ISNULL(numDefaultShippingServiceID,0) FROM dbo.DivisionMaster WHERE numDivisionID = @numDivisionID

select isnull(@AmountDueSO,0) as AmountDueSO,isnull(@AmountPastDueSO,0) as AmountPastDueSO, 
isnull(@AmountDuePO,0) as AmountDuePO,isnull(@AmountPastDuePO,0) as AmountPastDuePO,                      
ISNULL(@CompanyCredit,0) - (isnull(@AmountDueSO,0) + isnull(@AmountDuePO,0)) as RemainingCredit,
isnull(@PCreditMemo,0) as PCreditMemo,
isnull(@SCreditMemo,0) as SCreditMemo,
ISNULL(@CompanyCredit,0) AS CreditLimit,
@vcShippersAccountNo AS vcShippersAccountNo,
@numDefaultShippingServiceID AS numDefaultShippingServiceID

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetGeneralLedger_Virtual')
DROP PROCEDURE USP_GetGeneralLedger_Virtual
GO
CREATE PROCEDURE [dbo].[USP_GetGeneralLedger_Virtual]    
(
      @numDomainID INT,
      @vcAccountId VARCHAR(4000),
      @dtFromDate DATETIME,
      @dtToDate DATETIME,
      @vcTranType VARCHAR(50) = '',
      @varDescription VARCHAR(50) = '',
      @CompanyName VARCHAR(50) = '',
      @vcBizPayment VARCHAR(50) = '',
      @vcCheqNo VARCHAR(50) = '',
      @vcBizDocID VARCHAR(50) = '',
      @vcTranRef VARCHAR(50) = '',
      @vcTranDesc VARCHAR(50) = '',
	  @numDivisionID NUMERIC(9)=0,
      @ClientTimeZoneOffset INT,  --Added by Chintan to enable calculation of date according to client machine
      @charReconFilter CHAR(1)='',
	  @tintMode AS TINYINT=0,
      @numItemID AS NUMERIC(9)=0,
      @CurrentPage INT=0,
	  @PageSize INT=0,
	  @numAccountClass NUMERIC(18,0) = 0
    )
WITH Recompile -- Added by Chandan 26 Feb 2013 to avoid parameter sniffing
AS 
    BEGIN

--IF @IsReport = 1
--BEGIN
--	DECLARE @vcAccountIDs AS VARCHAR(MAX)
--	SELECT @vcAccountIDs = COALESCE(@vcAccountIDs + ',', '') + CAST([COA].[numAccountId] AS VARCHAR(20))
--	FROM [dbo].[Chart_Of_Accounts] AS COA 
--	WHERE [COA].[numDomainId] = @numDomainID
--	PRINT @vcAccountIDs

--	SET @vcAccountId= @vcAccountIDs
--END    

DECLARE @dtFinFromDate DATETIME;
SET @dtFinFromDate = ( SELECT   dtPeriodFrom
                       FROM     FINANCIALYEAR
                       WHERE    dtPeriodFrom <= @dtFromDate
                                AND dtPeriodTo >= @dtFromDate
                                AND numDomainId = @numDomainId
                     );

PRINT @dtFromDate
PRINT @dtToDate

DECLARE @dtFinYearFromJournal DATETIME ;
DECLARE @dtFinYearFrom DATETIME ;
SELECT @dtFinYearFromJournal = MIN(datEntry_Date) FROM view_journal WHERE numDomainID=@numDomainId 
SET @dtFinYearFrom = ( SELECT   dtPeriodFrom
                        FROM     FINANCIALYEAR
                        WHERE    dtPeriodFrom <= @dtFromDate
                                AND dtPeriodTo >= @dtFromDate
                                AND numDomainId = @numDomainId
                        ) ;

SELECT * INTO #view_journal FROM view_journal WHERE numDomainID=@numDomainId 

DECLARE @numMaxFinYearID NUMERIC
DECLARE @Month INT
DECLARE @year  INT
SELECT @numMaxFinYearID = MAX(numFinYearId) FROM dbo.FinancialYear WHERE numDomainId=@numDomainId
SELECT @Month = DATEPART(MONTH,dtPeriodFrom),@year =DATEPART(year,dtPeriodFrom) FROM dbo.FinancialYear WHERE numDomainId=@numDomainId AND numFinYearId=@numMaxFinYearID
PRINT @Month
PRINT @year
		 
DECLARE @first_id NUMERIC, @startRow int
DECLARE @CurrRecord as INT

if @tintMode = 0
BEGIN
SELECT  COA1.numAccountId INTO #Temp1 /*,COA1.vcAccountCode ,COA2.vcAccountCode*/
		FROM    dbo.Chart_Of_Accounts COA1
		WHERE   COA1.numAccountId IN(SELECT  cast(ID AS NUMERIC(9)) FROM dbo.SplitIDs(@vcAccountId, ','))
				AND COA1.numDomainId = @numDomainID
	SELECT @vcAccountId = ISNULL( STUFF((SELECT ',' + CAST(numAccountID AS VARCHAR(10)) FROM #Temp1 FOR XML PATH('')),1, 1, '') , '') 
    
    	--SELECT  @numDomainID AS numDomainID,* FROM dbo.fn_GetOpeningBalance(@vcAccountID,@numDomainID,@dtFromDate,@ClientTimeZoneOffset)
		SELECT  @numDomainID AS numDomainID, numAccountId,vcAccountName, numParntAcntTypeID,	vcAccountDescription,	vcAccountCode,	dtAsOnDate,	
		CASE WHEN ([Table1].[vcAccountCode] LIKE '0103%' OR [Table1].[vcAccountCode] LIKE '0104%' OR [Table1].[vcAccountCode] LIKE '0106%') AND (@year + @Month) = (DATEPART(YEAR,@dtFromDate) + DATEPART(MONTH,@dtFromDate)) THEN 0 
			 WHEN (@year + @Month) > (DATEPART(YEAR,@dtFromDate) + DATEPART(MONTH,@dtFromDate)) THEN isnull(t1.[OPENING],0)
			 WHEN (@year + @Month) < (DATEPART(YEAR,@dtFromDate) + DATEPART(MONTH,@dtFromDate)) THEN isnull(t2.[OPENING],0)
			 ELSE isnull(mnOpeningBalance,0)
		END  [mnOpeningBalance],
							(SELECT ISNULL(SUM(GJD.numDebitAmt),0) - ISNULL(SUM(GJD.numCreditAmt),0)
							FROM
							General_Journal_Details GJD 
							JOIN Domain D ON D.[numDomainId] = GJD.numDomainID AND D.[numDomainId] = @numDomainID
							INNER JOIN General_Journal_Header GJH ON GJD.numJournalId = GJH.numJournal_Id AND 
							GJD.numDomainId = @numDomainID AND 
							GJD.numChartAcntId = [Table1].numAccountId AND 
							GJH.numDomainID = D.[numDomainId] AND 
							GJH.datEntry_Date <= @dtFinFromDate) 
FROM dbo.fn_GetOpeningBalance(@vcAccountID,@numDomainID,@dtFromDate,@ClientTimeZoneOffset) [Table1]
OUTER APPLY (SELECT   SUM(Debit - Credit) AS OPENING
            FROM     #view_journal VJ
            WHERE    VJ.numDomainId = @numDomainId
                    AND VJ.numAccountID = [Table1].numAccountID /*AND VJ.COAvcAccountCode like COA.vcAccountCode + '%'*/ /*VJ.numAccountId = COA.numAccountId*/
					AND (numAccountClass = @numAccountClass OR ISNULL(@numAccountClass,0) = 0)
                    AND datEntry_Date BETWEEN (CASE WHEN [Table1].[vcAccountCode] LIKE '0103%' OR [Table1].[vcAccountCode] LIKE '0104%' OR [Table1].[vcAccountCode] LIKE '0106%' 
													THEN @dtFinYearFrom 
													ELSE @dtFinYearFromJournal 
											   END ) AND  DATEADD(Minute,-1,@dtFromDate)) AS t1
OUTER APPLY (SELECT   SUM(Debit - Credit) AS OPENING
            FROM     #view_journal VJ
            WHERE    VJ.numDomainId = @numDomainId
                    AND VJ.numAccountID = [Table1].numAccountID /*AND VJ.COAvcAccountCode like COA.vcAccountCode + '%'*/ /*VJ.numAccountId = COA.numAccountId*/
					AND (numAccountClass = @numAccountClass OR ISNULL(@numAccountClass,0) = 0)
                    AND datEntry_Date BETWEEN (CASE WHEN [Table1].[vcAccountCode] LIKE '0103%' OR [Table1].[vcAccountCode] LIKE '0104%' OR [Table1].[vcAccountCode] LIKE '0106%' 
													THEN @dtFinYearFrom 
													ELSE @dtFinYearFromJournal 
											   END ) AND  DATEADD(Minute,1,@dtFromDate)) AS t2

		--SET @dtFromDate = DateAdd(minute, @ClientTimeZoneOffset, @dtFromDate);
		--SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate);

SELECT TOP 1
dbo.General_Journal_Details.numDomainId,
 dbo.Chart_Of_Accounts.numAccountId , 
dbo.General_Journal_Header.numJournal_Id,
dbo.[FormatedDateFromDate](dbo.General_Journal_Header.datEntry_Date,dbo.General_Journal_Details.numDomainID) Date,
dbo.General_Journal_Header.varDescription, 
dbo.CompanyInfo.vcCompanyName AS CompanyName,
dbo.General_Journal_Details.numTransactionId,

ISNULL(ISNULL(ISNULL(dbo.VIEW_BIZPAYMENT.Narration,dbo.General_Journal_Header.varDescription),'') + ' ' + 
(CASE WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN ' Chek No: ' + isnull( CAST(CH.numCheckNo AS VARCHAR(50)),'No-Cheq') 
ELSE  dbo.VIEW_JOURNALCHEQ.Narration 
END),'') as Narration,

ISNULL(dbo.General_Journal_Details.vcReference,'') AS TranRef, 
ISNULL(dbo.General_Journal_Details.varDescription,'') AS TranDesc, 
CONVERT(VARCHAR(20),ISNULL(General_Journal_Details.numDebitAmt,0)) numDebitAmt,
CONVERT(VARCHAR(20),ISNULL(General_Journal_Details.numCreditAmt,0)) numCreditAmt,
dbo.Chart_Of_Accounts.vcAccountName,
'' as balance,
CASE 
WHEN isnull(dbo.General_Journal_Header.numCheckHeaderID, 0) <> 0 THEN + 'Checks' 
WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 1 THEN 'Deposit' 
WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 2 THEN 'Receved Pmt' 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
dbo.OpportunityMaster.tintOppType = 1 THEN 'BizDocs Invoice' 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
dbo.OpportunityMaster.tintOppType = 2 THEN 'BizDocs Purchase' 
WHEN isnull(dbo.General_Journal_Header.numCategoryHDRID, 0) <> 0 THEN 'Time And Expenses' 
WHEN ISNULL(General_Journal_Header.numBillId,0) <>0 AND isnull(BH.bitLandedCost,0)=1 then 'Landed Cost' 
WHEN isnull(dbo.General_Journal_Header.numBillID, 0) <> 0 THEN 'Bill'
WHEN isnull(dbo.General_Journal_Header.numBillPaymentID, 0) <> 0 THEN 'Pay Bill'
WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN 
																CASE 
																	WHEN RH.tintReturnType=1 AND RH.tintReceiveType=1 THEN 'Sales Return Refund'
																	WHEN RH.tintReturnType=1 AND RH.tintReceiveType=2 THEN 'Sales Return Credit Memo'
																	WHEN RH.tintReturnType=2 AND RH.tintReceiveType=2 THEN 'Purchase Return Credit Memo'
																	WHEN RH.tintReturnType=3 AND RH.tintReceiveType=2 THEN 'Credit Memo'
																	WHEN RH.tintReturnType=4 AND RH.tintReceiveType=1 THEN 'Refund Against Credit Memo'
																END 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) = 0 THEN 'PO Fulfillment'
WHEN dbo.General_Journal_Header.numJournal_Id <> 0 THEN 'Journal' 
END AS TransactionType,

ISNULL(dbo.General_Journal_Details.bitCleared,0) AS bitCleared,
ISNULL(dbo.General_Journal_Details.bitReconcile,0) AS bitReconcile,
ISNULL(dbo.General_Journal_Details.numItemID,0) AS numItemID,
Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else 0 end AS numLandedCostOppId,
STUFF((SELECT ', ' + CAST([COA].[vcAccountName] AS VARCHAR(4000))
			FROM [dbo].[General_Journal_Details] AS GJD JOIN [dbo].[Chart_Of_Accounts] AS COA ON GJD.[numChartAcntId] = COA.[numAccountId]
			WHERE [GJD].[numJournalId] = dbo.General_Journal_Header.numJournal_Id 
			AND [COA].[numDomainId] = General_Journal_Header.[numDomainId]
			AND [COA].[numAccountId] <> dbo.Chart_Of_Accounts.numAccountId FOR XML PATH('')) ,1,2, '') AS [vcSplitAccountName]
FROM dbo.General_Journal_Header INNER JOIN
dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId INNER JOIN
dbo.Chart_Of_Accounts ON dbo.General_Journal_Details.numChartAcntId = dbo.Chart_Of_Accounts.numAccountId LEFT OUTER JOIN
dbo.TimeAndExpense ON dbo.General_Journal_Header.numCategoryHDRID = dbo.TimeAndExpense.numCategoryHDRID 
LEFT OUTER JOIN dbo.DivisionMaster ON dbo.General_Journal_Details.numCustomerId = dbo.DivisionMaster.numDivisionID and 
dbo.General_Journal_Details.numDomainID = dbo.DivisionMaster.numDomainID
LEFT OUTER JOIN dbo.CompanyInfo ON dbo.DivisionMaster.numCompanyID = dbo.CompanyInfo.numCompanyId 
LEFT OUTER JOIN dbo.OpportunityMaster ON dbo.General_Journal_Header.numOppId = dbo.OpportunityMaster.numOppId LEFT OUTER JOIN
dbo.OpportunityBizDocs ON dbo.General_Journal_Header.numOppBizDocsId = dbo.OpportunityBizDocs.numOppBizDocsId LEFT OUTER JOIN
dbo.VIEW_JOURNALCHEQ ON (dbo.General_Journal_Header.numCheckHeaderID = dbo.VIEW_JOURNALCHEQ.numCheckHeaderID 
or (dbo.General_Journal_Header.numBillPaymentID=dbo.VIEW_JOURNALCHEQ.numReferenceID and dbo.VIEW_JOURNALCHEQ.tintReferenceType=8))LEFT OUTER JOIN
dbo.VIEW_BIZPAYMENT ON dbo.General_Journal_Header.numBizDocsPaymentDetId = dbo.VIEW_BIZPAYMENT.numBizDocsPaymentDetId LEFT OUTER JOIN
dbo.DepositMaster DM ON DM.numDepositId = dbo.General_Journal_Header.numDepositId  LEFT OUTER JOIN
dbo.ReturnHeader RH ON RH.numReturnHeaderID = dbo.General_Journal_Header.numReturnID LEFT OUTER JOIN
dbo.CheckHeader CH ON CH.numReferenceID=RH.numReturnHeaderID AND CH.tintReferenceType=10
LEFT OUTER JOIN BillHeader BH  ON ISNULL(General_Journal_Header.numBillId,0) = BH.numBillId

WHERE   dbo.General_Journal_Header.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate -- Removed Tmezone bug id 1029 
AND dbo.General_Journal_Header.numDomainId = @numDomainID 
 AND (dbo.DivisionMaster.numDivisionID = @numDivisionID OR @numDivisionID = 0)
AND (bitCleared = CASE @charReconFilter WHEN 'C' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A')
AND (bitReconcile = CASE @charReconFilter WHEN 'R' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A') 
AND (dbo.Chart_Of_Accounts.numAccountId IN (SELECT numAccountId FROM #Temp1))
--ORDER BY dbo.General_Journal_Header.numEntryDateSortOrder asc
--ORDER BY dbo.Chart_Of_Accounts.numAccountId ASC,  dbo.General_Journal_Header.datEntry_Date ASC, dbo.General_Journal_Details.numTransactionId asc 
END
ELSE IF @tintMode = 1
BEGIN

SELECT  @numDomainID AS numDomainID, numAccountId,vcAccountName, numParntAcntTypeID,	vcAccountDescription,	vcAccountCode,	dtAsOnDate,	
		CASE WHEN ([Table1].[vcAccountCode] LIKE '0103%' OR [Table1].[vcAccountCode] LIKE '0104%' OR [Table1].[vcAccountCode] LIKE '0106%') AND (@year + @Month) = (DATEPART(YEAR,@dtFromDate) + DATEPART(MONTH,@dtFromDate)) THEN 0 
			 WHEN (@year + @Month) > (DATEPART(YEAR,@dtFromDate) + DATEPART(MONTH,@dtFromDate)) THEN isnull(t1.[OPENING],0)
			 WHEN (@year + @Month) < (DATEPART(YEAR,@dtFromDate) + DATEPART(MONTH,@dtFromDate)) THEN isnull(t2.[OPENING],0)
			 ELSE isnull(mnOpeningBalance,0)
		END  [mnOpeningBalance],
							(SELECT ISNULL(SUM(GJD.numDebitAmt),0) - ISNULL(SUM(GJD.numCreditAmt),0)
							FROM
							General_Journal_Details GJD 
							JOIN Domain D ON D.[numDomainId] = GJD.numDomainID AND D.[numDomainId] = @numDomainID
							INNER JOIN General_Journal_Header GJH ON GJD.numJournalId = GJH.numJournal_Id AND 
							GJD.numDomainId = @numDomainID AND 
							GJD.numChartAcntId = [Table1].numAccountId AND 
							GJH.numDomainID = D.[numDomainId] AND 
							GJH.datEntry_Date <= @dtFinFromDate) 
FROM dbo.fn_GetOpeningBalance(@vcAccountID,@numDomainID,@dtFromDate,@ClientTimeZoneOffset) [Table1]
OUTER APPLY (SELECT   SUM(Debit - Credit) AS OPENING
            FROM     #view_journal VJ
            WHERE    VJ.numDomainId = @numDomainId
                    AND VJ.numAccountID = [Table1].numAccountID /*AND VJ.COAvcAccountCode like COA.vcAccountCode + '%'*/ /*VJ.numAccountId = COA.numAccountId*/
					AND (numAccountClass = @numAccountClass OR ISNULL(@numAccountClass,0) = 0)
                    AND datEntry_Date BETWEEN (CASE WHEN [Table1].[vcAccountCode] LIKE '0103%' OR [Table1].[vcAccountCode] LIKE '0104%' OR [Table1].[vcAccountCode] LIKE '0106%' 
													THEN @dtFinYearFrom 
													ELSE @dtFinYearFromJournal 
											   END ) AND  DATEADD(Minute,-1,@dtFromDate)) AS t1
OUTER APPLY (SELECT   SUM(Debit - Credit) AS OPENING
            FROM     #view_journal VJ
            WHERE    VJ.numDomainId = @numDomainId
                    AND VJ.numAccountID = [Table1].numAccountID /*AND VJ.COAvcAccountCode like COA.vcAccountCode + '%'*/ /*VJ.numAccountId = COA.numAccountId*/
					AND (numAccountClass = @numAccountClass OR ISNULL(@numAccountClass,0) = 0)
                    AND datEntry_Date BETWEEN (CASE WHEN [Table1].[vcAccountCode] LIKE '0103%' OR [Table1].[vcAccountCode] LIKE '0104%' OR [Table1].[vcAccountCode] LIKE '0106%' 
													THEN @dtFinYearFrom 
													ELSE @dtFinYearFromJournal 
											   END ) AND  DATEADD(Minute,1,@dtFromDate)) AS t2

--SET @dtFromDate = DateAdd(minute, @ClientTimeZoneOffset, @dtFromDate);
--SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate);

SET @CurrRecord = ((@CurrentPage - 1) * @PageSize) + 1
PRINT @CurrRecord
SET ROWCOUNT @CurrRecord

SELECT @first_id = dbo.General_Journal_Details.numEntryDateSortOrder1
FROM dbo.General_Journal_Header 
INNER JOIN dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId   
INNER JOIN dbo.Chart_Of_Accounts ON dbo.General_Journal_Details.numChartAcntId = dbo.Chart_Of_Accounts.numAccountId 
LEFT OUTER JOIN dbo.TimeAndExpense ON dbo.General_Journal_Header.numCategoryHDRID = dbo.TimeAndExpense.numCategoryHDRID -- LEFT OUTER JOIN
WHERE dbo.General_Journal_Header.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate -- Removed Tmezone bug id 1029 
AND dbo.General_Journal_Header.numDomainId = @numDomainID 
AND dbo.General_Journal_Details.numDomainId = @numDomainID 
AND dbo.Chart_Of_Accounts.numDomainId = @numDomainID 
AND (bitCleared = CASE @charReconFilter WHEN 'C' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A')
AND (bitReconcile = CASE @charReconFilter WHEN 'R' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A') 
--AND (dbo.Chart_Of_Accounts.numAccountId =@vcAccountId)
AND (dbo.Chart_Of_Accounts.numAccountId= @vcAccountId)
AND (General_Journal_Details.numItemID = @numItemID OR @numItemID = 0)
AND dbo.General_Journal_Details.numEntryDateSortOrder1 > 0
--ORDER BY dbo.Chart_Of_Accounts.numAccountId ASC, dbo.General_Journal_Details.numTransactionId asc 
ORDER BY dbo.General_Journal_Details.numEntryDateSortOrder1 asc

PRINT @first_id
PRINT ROWCOUNT_BIG()
SET ROWCOUNT @PageSize

SELECT dbo.General_Journal_Details.numDomainId, dbo.Chart_Of_Accounts.numAccountId , dbo.General_Journal_Header.numJournal_Id,
dbo.[FormatedDateFromDate](dbo.General_Journal_Header.datEntry_Date,dbo.General_Journal_Details.numDomainID) Date,
dbo.General_Journal_Header.varDescription, 
dbo.CompanyInfo.vcCompanyName AS CompanyName,
dbo.General_Journal_Details.numTransactionId,
ISNULL(ISNULL(ISNULL(dbo.VIEW_BIZPAYMENT.Narration,dbo.General_Journal_Header.varDescription),'') + ' ' + 
(CASE WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN ' Chek No: ' + isnull( CAST(CH.numCheckNo AS VARCHAR(50)),'No-Cheq') 
ELSE  dbo.VIEW_JOURNALCHEQ.Narration 
END),'') as Narration,
ISNULL(dbo.General_Journal_Details.vcReference,'') AS TranRef, 
ISNULL(dbo.General_Journal_Details.varDescription,'') AS TranDesc, 
CONVERT(VARCHAR(20),ISNULL(General_Journal_Details.numDebitAmt,0)) numDebitAmt,
CONVERT(VARCHAR(20),ISNULL(General_Journal_Details.numCreditAmt,0)) numCreditAmt,
dbo.Chart_Of_Accounts.vcAccountName,
'' as balance,
CASE 
WHEN isnull(dbo.General_Journal_Header.numCheckHeaderID, 0) <> 0 THEN + 'Checks' 
WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 1 THEN 'Deposit' 
WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 2 THEN 'Receved Pmt' 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
dbo.OpportunityMaster.tintOppType = 1 THEN 'BizDocs Invoice' 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
dbo.OpportunityMaster.tintOppType = 2 THEN 'BizDocs Purchase' 
WHEN isnull(dbo.General_Journal_Header.numCategoryHDRID, 0) <> 0 THEN 'Time And Expenses' 
WHEN ISNULL(General_Journal_Header.numBillId,0) <>0 AND isnull(BH.bitLandedCost,0)=1 then 'Landed Cost' 
WHEN isnull(dbo.General_Journal_Header.numBillID, 0) <> 0 THEN 'Bill'
WHEN isnull(dbo.General_Journal_Header.numBillPaymentID, 0) <> 0 THEN 'Pay Bill'
WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN 
																CASE 
																	WHEN RH.tintReturnType=1 AND RH.tintReceiveType=1 THEN 'Sales Return Refund'
																	WHEN RH.tintReturnType=1 AND RH.tintReceiveType=2 THEN 'Sales Return Credit Memo'
																	WHEN RH.tintReturnType=2 AND RH.tintReceiveType=2 THEN 'Purchase Return Credit Memo'
																	WHEN RH.tintReturnType=3 AND RH.tintReceiveType=2 THEN 'Credit Memo'
																	WHEN RH.tintReturnType=4 AND RH.tintReceiveType=1 THEN 'Refund Against Credit Memo'
																END 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) = 0 THEN 'PO Fulfillment'
WHEN dbo.General_Journal_Header.numJournal_Id <> 0 THEN 'Journal' 
END AS TransactionType,
ISNULL(dbo.General_Journal_Details.bitCleared,0) AS bitCleared,
ISNULL(dbo.General_Journal_Details.bitReconcile,0) AS bitReconcile,
ISNULL(dbo.General_Journal_Details.numItemID,0) AS numItemID,
dbo.General_Journal_Details.numEntryDateSortOrder1 AS numEntryDateSortOrder,
Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else 0 end AS numLandedCostOppId,
STUFF((SELECT ', ' + CAST([COA].[vcAccountName] AS VARCHAR(4000))
			FROM [dbo].[General_Journal_Details] AS GJD JOIN [dbo].[Chart_Of_Accounts] AS COA ON GJD.[numChartAcntId] = COA.[numAccountId]
			WHERE [GJD].[numJournalId] = dbo.General_Journal_Header.numJournal_Id 
			AND [COA].[numDomainId] = General_Journal_Header.[numDomainId]
			AND [COA].[numAccountId] <> dbo.Chart_Of_Accounts.numAccountId FOR XML PATH('')) ,1,2, '') AS [vcSplitAccountName]
FROM dbo.General_Journal_Header INNER JOIN
dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId INNER JOIN
dbo.Chart_Of_Accounts ON dbo.General_Journal_Details.numChartAcntId = dbo.Chart_Of_Accounts.numAccountId LEFT OUTER JOIN
dbo.TimeAndExpense ON dbo.General_Journal_Header.numCategoryHDRID = dbo.TimeAndExpense.numCategoryHDRID 
LEFT OUTER JOIN dbo.DivisionMaster ON dbo.General_Journal_Details.numCustomerId = dbo.DivisionMaster.numDivisionID and 
dbo.General_Journal_Details.numDomainID = dbo.DivisionMaster.numDomainID
LEFT OUTER JOIN dbo.CompanyInfo ON dbo.DivisionMaster.numCompanyID = dbo.CompanyInfo.numCompanyId 
LEFT OUTER JOIN dbo.OpportunityMaster ON dbo.General_Journal_Header.numOppId = dbo.OpportunityMaster.numOppId LEFT OUTER JOIN
dbo.OpportunityBizDocs ON dbo.General_Journal_Header.numOppBizDocsId = dbo.OpportunityBizDocs.numOppBizDocsId LEFT OUTER JOIN
dbo.VIEW_JOURNALCHEQ ON (dbo.General_Journal_Header.numCheckHeaderID = dbo.VIEW_JOURNALCHEQ.numCheckHeaderID 
or (dbo.General_Journal_Header.numBillPaymentID=dbo.VIEW_JOURNALCHEQ.numReferenceID and dbo.VIEW_JOURNALCHEQ.tintReferenceType=8))LEFT OUTER JOIN
dbo.VIEW_BIZPAYMENT ON dbo.General_Journal_Header.numBizDocsPaymentDetId = dbo.VIEW_BIZPAYMENT.numBizDocsPaymentDetId LEFT OUTER JOIN
dbo.DepositMaster DM ON DM.numDepositId = dbo.General_Journal_Header.numDepositId  LEFT OUTER JOIN
dbo.ReturnHeader RH ON RH.numReturnHeaderID = dbo.General_Journal_Header.numReturnID LEFT OUTER JOIN
dbo.CheckHeader CH ON CH.numReferenceID=RH.numReturnHeaderID AND CH.tintReferenceType=10
LEFT OUTER JOIN BillHeader BH  ON ISNULL(General_Journal_Header.numBillId,0) = BH.numBillId

WHERE   dbo.General_Journal_Header.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate -- Removed Tmezone bug id 1029 
AND dbo.General_Journal_Header.numDomainId = @numDomainID 
AND (dbo.DivisionMaster.numDivisionID = @numDivisionID OR @numDivisionID = 0)
AND (bitCleared = CASE @charReconFilter WHEN 'C' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A')
AND (bitReconcile = CASE @charReconFilter WHEN 'R' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A') 
--AND (dbo.Chart_Of_Accounts.numAccountId =@vcAccountId)
--AND (dbo.Chart_Of_Accounts. IN ( SELECT numAccountId FROM #Temp) OR  @tintMode=1)
AND (dbo.Chart_Of_Accounts.numAccountId= @vcAccountId)
AND (General_Journal_Details.numItemID = @numItemID OR @numItemID = 0)
   AND dbo.General_Journal_Details.numEntryDateSortOrder1  >= @first_id              
ORDER BY dbo.General_Journal_Details.numEntryDateSortOrder1 asc
--AND dbo.General_Journal_Details.numTransactionId >= @first_id
--ORDER BY dbo.Chart_Of_Accounts.numAccountId ASC,   dbo.General_Journal_Details.numTransactionId asc 
SET ROWCOUNT 0

END
	
IF object_id('TEMPDB.DBO.#Temp1') IS NOT NULL drop table #Temp1	
END
/****** Object:  StoredProcedure [dbo].[usp_GetSalesFulfillmentOrder]    Script Date: 03/06/2009 00:39:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSalesFulfillmentOrder')
DROP PROCEDURE USP_GetSalesFulfillmentOrder
GO
CREATE PROCEDURE [dbo].[USP_GetSalesFulfillmentOrder]
    (
      @numDomainId AS NUMERIC(9) = 0,
      @SortCol VARCHAR(50),
      @SortDirection VARCHAR(4),
      @CurrentPage INT,
      @PageSize INT,
      @TotRecs INT OUTPUT,
      @numUserCntID NUMERIC,
      @vcBizDocStatus VARCHAR(500),
      @vcBizDocType varchar(500),
      @numDivisionID as numeric(9)=0,
      @ClientTimeZoneOffset INT=0,
      @bitIncludeHistory bit=0,
      @vcOrderStatus VARCHAR(500),
      @vcOrderSource VARCHAR(1000),
	  @bitFulfilled BIT,
	  @numOppID AS NUMERIC(18,0) = 0,
	  @vcShippingService AS VARCHAR(MAX) = '',
	  @numShippingZone NUMERIC(18,0)=0
     )
AS 
    BEGIN

        DECLARE @firstRec AS INTEGER                                                              
        DECLARE @lastRec AS INTEGER                                                     
        SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                             
        SET @lastRec = ( @CurrentPage * @PageSize + 1 )                                                                    

	  IF LEN(ISNULL(@SortCol,''))=0
		SET  @SortCol = 'dtCreatedDate'
		
	  IF LEN(ISNULL(@SortDirection,''))=0	
		SET @SortDirection = 'desc'
      
        DECLARE @strSql NVARCHAR(4000)
        DECLARE @SELECT NVARCHAR(4000)
        DECLARE @FROM NVARCHAR(4000)
        DECLARE @WHERE NVARCHAR(4000)
		
		DECLARE @numShippingServiceItemID AS NUMERIC
		SELECT @numShippingServiceItemID=ISNULL(numShippingServiceItemID,0) FROM domain WHERE numDomainID=@numDomainID
		
	CREATE TABLE #temp (row INT,vcpoppname VARCHAR(100),numoppid NUMERIC,vcOrderStatus VARCHAR(100), vcInventoryStatus VARCHAR(100),
		numoppbizdocsid NUMERIC,numBizDocId NUMERIC,vcbizdocid VARCHAR(100),vcBizDocType VARCHAR(100),
		numbizdocstatus NUMERIC,vcbizdocstatus VARCHAR(100),dtFromDate VARCHAR(30),numDivisionID NUMERIC,
		vcCompanyName VARCHAR(100),tintCRMType TINYINT)
		
        SET @SELECT = 'WITH bizdocs
         AS (SELECT Row_number() OVER(ORDER BY ' + @SortCol + ' '
            + @SortDirection
            + ') AS row,
			om.vcpoppname,
			om.numoppid,dbo.fn_GetListItemName(om.[numstatus]) vcOrderStatus,
			(CASE WHEN CHARINDEX(''Back Order'',dbo.CheckOrderInventoryStatus(om.numOppID,om.numDomainID)) > 0 THEN REPLACE(dbo.CheckOrderInventoryStatus(om.numOppID,om.numDomainID),''Back Order '','''') ELSE '''' END) As vcInventoryStatus,
			obd.[numoppbizdocsid],OBD.numBizDocId,
			obd.[vcbizdocid],dbo.fn_GetListItemName(OBD.numBizDocId) AS vcBizDocType,
			obd.[numbizdocstatus],
			dbo.fn_GetListItemName(obd.[numbizdocstatus]) vcbizdocstatus,
			CONCAT((CASE WHEN (SELECT COUNT(*) FROM OpportunityItemsReleaseDates WHERE numOppID=om.numOppID)>0 THEN ''* '' ELSE '''' END),[dbo].[FormatedDateFromDate](om.dtReleaseDate,'+ CONVERT(VARCHAR(15), @numDomainId) + ')) dtFromDate,
			om.numDivisionID,
			vcCompanyName,DM.tintCRMType'
			
		SET @FROM =' FROM opportunitymaster om INNER JOIN [opportunitybizdocs] obd ON om.[numoppid] = obd.[numoppid]
                     INNER JOIN DivisionMaster DM ON om.numDivisionId = DM.numDivisionID
					 Inner join CompanyInfo cmp on cmp.numCompanyID=DM.numCompanyID'
					
        SET @WHERE = ' WHERE (om.numOppID = ' +  CAST(@numOppID AS VARCHAR) + ' OR ' +  CAST(ISNULL(@numOppID,0) AS VARCHAR) + ' = 0) AND om.tintopptype = 1 and om.tintOppStatus=1 AND ISNULL(OBD.bitFulfilled,0)=' + CAST(@bitFulfilled AS VARCHAR) + ' AND OBD.numBizDocId=296 AND om.numDomainID = '+ CONVERT(VARCHAR(15), @numDomainId)
		
		IF  LEN(ISNULL(@vcBizDocStatus,''))>0
		BEGIN
			IF @bitIncludeHistory=1
				SET @WHERE = @WHERE + ' and (select count(*) from OppFulfillmentBizDocsStatusHistory WHERE numOppId=om.numOppId and numOppBizDocsId=obd.numOppBizDocsId and isnull(numbizdocstatus,0) in (SELECT Items FROM dbo.Split(''' +@vcBizDocStatus + ''','','')))>0' 			
			ELSE	
				SET @WHERE = @WHERE + ' and obd.numbizdocstatus in (SELECT Items FROM dbo.Split(''' +@vcBizDocStatus + ''','',''))' 			
		END
		
		IF  LEN(ISNULL(@vcOrderStatus,''))>0
		BEGIN
			SET @WHERE = @WHERE + ' and isnull(om.numStatus,0) in (SELECT Items FROM dbo.Split(''' +@vcOrderStatus + ''','',''))' 			
		END

		IF LEN(ISNULL(@vcShippingService,''))>0
		BEGIN
			SET @WHERE = @WHERE + ' and isnull(om.intUsedShippingCompany,0) in (SELECT Items FROM dbo.Split(''' +@vcShippingService + ''','',''))'
		END

		IF ISNULL(@numShippingZone,0) > 0
		BEGIN
			SET @WHERE = @WHERE + ' AND 1 = (CASE 
												WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(om.numOppId,om.numDomainID,2)),0) > 0 
												THEN
													CASE 
														WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=' + CAST(@numDomainId AS VARCHAR) + ' AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(om.numOppId,om.numDomainID,2)) AND numShippingZone=' + CAST(@numShippingZone as VARCHAR) + ' ) > 0 
														THEN 1
														ELSE 0
													END
												ELSE 0 
											END)'
		END
		
		IF LEN(ISNULL(@vcOrderSource,''))>0
		BEGIN
				SET @WHERE = @WHERE + ' and (' + @vcOrderSource +')' 			
		END
		
		IF  LEN(ISNULL(@vcBizDocType,''))>0
		BEGIN
			SET @WHERE = @WHERE + ' and isnull(OBD.numBizDocId,0) in (SELECT Items FROM dbo.Split(''' +@vcBizDocType + ''','',''))' 			
		END
		
		SET @WHERE = @WHERE + ' AND (om.numDivisionID ='+ CONVERT(VARCHAR(15), @numDivisionID) + ' OR '+ CONVERT(VARCHAR(15), @numDivisionID) + ' = 0) '
		
        SET @strSql = @SELECT + @FROM +@WHERE + ') insert into #temp SELECT * FROM [bizdocs] WHERE row > ' + CONVERT(VARCHAR(10), @firstRec) + ' and row <' + CONVERT(VARCHAR(10), @lastRec)
        
        PRINT @strSql ;
        EXEC ( @strSql ) ;
       
       SELECT OM.*,ISNULL((SELECT SUBSTRING((SELECT '$^$' + dbo.fn_GetListItemName(numBizDocStatus) +'#^#'+ dbo.fn_GetContactName(numUserCntID) +'#^#'+ dbo.FormatedDateTimeFromDate(DateAdd(minute,-@ClientTimeZoneOffset, dtCreatedDate),@numDomainId) + '#^#'+ isnull(DFCS.vcColorScheme,'NoForeColor')
	FROM OppFulfillmentBizDocsStatusHistory OFBS left join DycFieldColorScheme DFCS on OFBS.numBizDocStatus=DFCS.vcFieldValue AND DFCS.numDomainID=@numDomainId WHERE numOppId=om.numOppId and numOppBizDocsId=OM.numOppBizDocsId FOR XML PATH('')),4,2000)),'') as vcBizDocsStatusList,
	isnull((select max(numShippingReportId) from ShippingReport SR where /*SR.numOppID=om.numOppID and*/ SR.numOppBizDocId=OM.numoppbizdocsid AND SR.numDomainID = @numDomainID),0) AS numShippingReportId,
	ISNULL((SELECT max(SB.numShippingReportId) FROM [ShippingBox] SB JOIN dbo.ShippingReport SR ON SB.numShippingReportId = SR.numShippingReportId where /*SR.numOppID=om.numOppID and*/ SR.numOppBizDocId=OM.numoppbizdocsid AND SR.numDomainID = @numDomainID AND LEN(ISNULL(SB.vcTrackingNumber,''))>0),0) AS numShippingLabelReportId,
	CAST((Case WHEN ISNULL(OI.numoppitemtCode,0)>0 then 1 else 0 end) as bit) as IsShippingServiceItemID,ISNULL(vcItemDesc,'' ) AS vcItemDesc,ISNULL(monTotAmount,0) AS monTotAmount
	FROM #temp OM 
		LEFT JOIN (SELECT numOppId,numoppitemtCode,ISNULL(vcItemDesc, '') vcItemDesc,monTotAmount,
		Row_number() OVER(PARTITION BY numOppId ORDER BY numoppitemtCode) AS row 
		FROM OpportunityItems OI WHERE OI.numOppId in (SELECT numOppId FROM #temp) AND numItemCode =@numShippingServiceItemID) 
		OI ON OI.row=1 AND OM.numOppId = OI.numOppId
       
       
       SELECT opp.numOppId,OBD.numOppBizDocID,  Opp.vcitemname AS vcItemName, dbo.GetOppItemReleaseDates(@numDomainId,Opp.numOppId,Opp.numoppitemtCode,0) AS vcItemReleaseDate,
				ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=@numDomainID AND ItemImages.numItemCode=i.numItemCode AND bitDefault=1 AND bitIsImage=1),'') AS vcImage,
                        CASE WHEN charitemType = 'P' THEN 'Product'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS charitemType,
                        CASE WHEN charitemType = 'P' THEN 'Inventory'
	                         WHEN charitemType = 'N' THEN 'Non-Inventory'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS vcItemType, CASE WHEN ISNULL(numItemGroup,0) > 0 THEN WI.vcWHSKU
                             ELSE i.vcSKU
                        END vcSKU,  
                        dbo.USP_GetAttributes(OBD.numWarehouseItmsID,bitSerialized) AS vcAttributes,
                        ISNULL(W.vcWareHouse, '') AS Warehouse,WL.vcLocation,OBD.numUnitHour AS numUnitHourOrig,
                        Opp.bitDropShip AS DropShip,SUBSTRING(( SELECT  ' ,' + vcSerialNo
                                            + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                                   THEN '('
                                                        + CONVERT(VARCHAR(15), oppI.numQty)
                                                        + ')'
                                                   ELSE ''
                                              END
                                    FROM    OppWarehouseSerializedItem oppI
                                            JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                    WHERE   oppI.numOppID = Opp.numOppId
                                            AND oppI.numOppItemID = Opp.numoppitemtCode
                                    ORDER BY vcSerialNo
                                  FOR
                                    XML PATH('')
                                  ), 3, 200000) AS SerialLotNo,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,Opp.numWarehouseItmsID,opp.numoppitemtCode
              FROM      OpportunityItems opp 
                        JOIN OpportunityBizDocItems OBD ON OBD.numOppItemID = opp.numoppitemtCode
                        join #temp OM on opp.numoppid=OM.numoppid and OM.numOppBizDocsId=OBD.numOppBizDocID
                        LEFT JOIN item i ON opp.numItemCode = i.numItemCode
                        INNER JOIN dbo.WareHouseItems WI ON opp.numWarehouseItmsID = WI.numWareHouseItemID
                                                           AND WI.numDomainID = @numDomainId
                        INNER JOIN dbo.Warehouses W ON WI.numDomainID = W.numDomainID
                                                      AND WI.numWareHouseID = W.numWareHouseID
						LEFT JOIN dbo.WarehouseLocation WL ON WL.numWLocationID= WI.numWLocationID
              
        
        DROP TABLE #temp 
        
        /*Get total count */
        SET @strSql =  ' (SELECT @TotRecs =COUNT(*) ' + @FROM + @WHERE + ')';
        
        PRINT @strSql ;
        EXEC sp_executesql 
        @query = @strSql, 
        @params = N'@TotRecs INT OUTPUT', 
        @TotRecs = @TotRecs OUTPUT 
        
    END
  
--created by anoop jayaraj
--	EXEC USP_GEtSFItemsForImporting 72,'76,318,538,758,772,1061,11409,11410,11628,11629,17089,18151,18193,18195,18196,18197',1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getsfitemsforimporting')
DROP PROCEDURE usp_getsfitemsforimporting
GO
CREATE PROCEDURE USP_GEtSFItemsForImporting
    @numDomainID AS NUMERIC(9),
	@numBizDocId	AS BIGINT,
	@vcBizDocsIds		VARCHAR(MAX),
	@tintMode AS TINYINT 
AS
BEGIN
---------------------------------------------------------------------------------
    DECLARE  @strSql  AS VARCHAR(8000)
  DECLARE  @strFrom  AS VARCHAR(2000)

IF @tintMode=1 --PrintPickList
BEGIN
   SELECT
		ISNULL(vcItemName,'') AS vcItemName,
		ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=187 AND bitDefault=1 AND bitIsImage=1),'') AS vcImage,
		ISNULL(vcModelID,'') AS vcModelID,
		ISNULL(txtItemDesc,'') AS txtItemDesc,
		ISNULL(vcWareHouse,'') AS vcWareHouse,
		ISNULL(vcLocation,'') AS vcLocation,
		(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcWHSKU ELSE Item.vcSKU END) AS vcSKU,
		(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcBarCode ELSE Item.numBarCodeId END) AS vcUPC,
		SUM(numUnitHour) AS numUnitHour
	FROM
		OpportunityBizDocItems
	INNER JOIN
		WareHouseItems
	ON
		OpportunityBizDocItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
	INNER JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	INNER JOIN
		Item
	ON
		OpportunityBizDocItems.numItemCode = Item.numItemCode
	WHERE
		numOppBizDocID IN ( SELECT * FROM dbo.splitIds(@vcBizDocsIds,','))
	GROUP BY
		vcItemName,
		vcModelID,
		txtItemDesc,
		vcWareHouse,
		vcLocation,
		(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcWHSKU ELSE Item.vcSKU END),
		(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcBarCode ELSE Item.numBarCodeId END)
	ORDER BY
		vcWareHouse ASC,
		vcLocation ASC,
		vcItemName ASC
END
ELSE IF @tintMode=2 --PrintPackingSlip
BEGIN
		  
		SELECT	
				ROW_NUMBER() OVER(ORDER BY OM.numDomainID ASC) AS SRNO,
				OM.numOppId,
				OM.numDomainID,
				OM.tintTaxOperator,
				I.numItemCode,
				I.vcModelID,
				OM.vcPOppName,
				cast((OBDI.monTotAmtBefDiscount - OBDI.monTotAMount) as varchar(20)) + Case When isnull(OM.fltDiscount,0)> 0 then  '(' + cast(OM.fltDiscount as varchar(10)) + '%)' else '' end AS DiscAmt,
				OBDI.dtRentalStartDate,
				OBDI.dtRentalReturnDate,
				OBDI.monShipCost,
				OBDI.vcShippingMethod,
				OBDI.dtDeliveryDate,
				numItemClassification,
				OBDI.vcNotes,
				OBD.vcTrackingNo,
				I.vcManufacturer,
				--vcItemClassification,
				CASE 
					WHEN charItemType='P' 
					THEN 'Inventory Item' 
					WHEN charItemType='S' 
					THEN 'Service' 
					WHEN charItemType='A' 
					THEN 'Accessory' 
					WHEN charItemType='N' 
					THEN 'Non-Inventory Item' 
				END AS charItemType,charItemType AS [Type],
				OBDI.vcAttributes,
				OI.[numoppitemtCode] ,
				OI.vcItemName,
				OBDI.numUnitHour,
				OI.[numQtyShipped],
				OBDI.vcItemDesc AS txtItemDesc,
				vcUnitofMeasure AS numUOMId,
				monListPrice AS [Price],
				CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4), OBDI.monTotAmount)) Amount,
				dbo.fn_GetListItemName(OBD.numShipVia) AS ShipVai,
				OBDI.monPrice,
				OBDI.monTotAmount,
				vcWareHouse,
				vcLocation,
				vcWStreet,
				vcWCity,
				vcWPinCode,
				(SELECT vcState FROM State WHERE numStateID = W.numWState) AS [vcState],
				dbo.fn_GetListItemName(W.numWCountry) AS [vcWCountry],							
				vcWHSKU,
				vcBarCode,
				SUBSTRING((SELECT ',' + vcSerialNo + CASE 
															WHEN isnull(I.bitLotNo,0)=1 
															THEN ' (' + CONVERT(VARCHAR(15),SERIALIZED_ITEM.numQty) + ')' 
															ELSE '' 
													 END 
							FROM OppWarehouseSerializedItem SERIALIZED_ITEM 
							JOIN WareHouseItmsDTL W_ITEM_DETAIL ON SERIALIZED_ITEM.numWareHouseItmsDTLID = W_ITEM_DETAIL.numWareHouseItmsDTLID 
							WHERE SERIALIZED_ITEM.numOppID = OI.numOppId 
							  AND SERIALIZED_ITEM.numOppItemID = OI.numoppitemtCode 
							  and SERIALIZED_ITEM.numOppBizDocsID=OBD.numOppBizDocsId
							ORDER BY vcSerialNo 
						  FOR XML PATH('')),2,200000) AS SerialLotNo,
				isnull(OBD.vcRefOrderNo,'') AS vcRefOrderNo,
				isnull(OM.bitBillingTerms,0) AS tintBillingTerms,
				isnull(intBillingDays,0) AS numBillingDays,
--				CASE 
--					WHEN ISNUMERIC(ISNULL(dbo.fn_GetListItemName(ISNULL(intBillingDays,0)),0)) = 1 
--					THEN ISNULL(dbo.fn_GetListItemName(ISNULL(intBillingDays,0)),0) 
--					ELSE 0 
--				END AS numBillingDaysName,
				CASE WHEN ISNUMERIC((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0))) = 1
					 THEN (SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0))
					 ELSE 0
				END AS numBillingDaysName,	 
				ISNULL(bitInterestType,0) AS tintInterestType,
				tintOPPType,
				dbo.fn_GetContactName(numApprovedBy) AS ApprovedBy,
				dtApprovedDate,
				OM.bintAccountClosingDate,
				tintShipToType,
				tintBillToType,
				tintshipped,
				dtShippedDate bintShippedDate,
				OM.numDivisionID,							
				ISNULL(numShipVia,0) AS numShipVia,
				ISNULL(vcTrackingURL,'') AS vcTrackingURL,
				CASE 
				    WHEN numShipVia IS NULL 
				    THEN '-'
				    ELSE dbo.fn_GetListItemName(numShipVia)
				END AS ShipVia,
				ISNULL(C.varCurrSymbol,'') varCurrSymbol,
				isnull(bitPartialFulfilment,0) bitPartialFulfilment,
				dbo.[fn_GetContactName](OM.[numRecOwner])  AS OrderRecOwner,
				dbo.[fn_GetContactName](DM.[numRecOwner]) AS AccountRecOwner,
				dbo.fn_GetContactName(OM.numAssignedTo) AS AssigneeName,
				ISNULL((SELECT vcEmail FROM dbo.AdditionalContactsInformation WHERE numContactID = OM.numAssignedTo),'') AS AssigneeEmail,
				ISNULL((SELECT numPhone FROM dbo.AdditionalContactsInformation WHERE numContactID = OM.numAssignedTo),'') AS AssigneePhone,
				dbo.fn_GetComapnyName(OM.numDivisionId) OrganizationName,
				DM.vcComPhone as OrganizationPhone,
				dbo.fn_GetContactName(OM.numContactID)  OrgContactName,
				dbo.getCompanyAddress(OM.numDivisionId,1,OM.numDomainId) CompanyBillingAddress,
				CASE 
					 WHEN ACI.numPhone<>'' 
					 THEN ACI.numPhone + CASE 
										    WHEN ACI.numPhoneExtension<>'' 
											THEN ' - ' + ACI.numPhoneExtension 
											ELSE '' 
										 END  
					 ELSE '' 
				END OrgContactPhone,
				ISNULL(ACI.vcEmail,'') AS OrgContactEmail,
				dbo.[fn_GetContactName](OM.[numRecOwner]) AS OnlyOrderRecOwner,
				(SELECT TOP 1 SD.vcSignatureFile FROM SignatureDetail SD 
												 JOIN OpportunityBizDocsDetails OBD ON SD.numSignID = OBD.numSignID 
												WHERE SD.numDomainID = @numDomainID 
												  AND OBD.numDomainID = @numDomainID 
												  AND OBD.numSignID IS not null 
				ORDER BY OBD.numBizDocsPaymentDetId DESC) AS vcSignatureFile,
				ISNULL(CMP.txtComments,'') AS vcOrganizationComments,
				OBD.vcRefOrderNo AS [P.O.No],
				OM.txtComments AS [Comments],
				D.vcBizDocImagePath  
	INTO #temp_Packing_List
	FROM OpportunityMaster AS OM
	join OpportunityBizDocs OBD on OM.numOppID=OBD.numOppID
	JOIN OpportunityBizDocItems OBDI on OBDI.numOppBizDocID=OBD.numOppBizDocsId
	JOIN OpportunityItems AS OI on OI.numoppitemtCode=OBDI.numOppItemID and OI.numoppID=OI.numOppID
	JOIN Item AS I ON OI.numItemCode = I.numItemCode AND I.numDomainID = @numDomainID
	LEFT OUTER JOIN WareHouseItems WI ON I.numItemCode = WI.numItemID AND WI.numWareHouseItemID = OBDI.numWarehouseItmsID
	LEFT OUTER JOIN Warehouses W ON WI.numWareHouseID = W.numWareHouseID 
	JOIN [DivisionMaster] DM ON DM.numDivisionID = OM.numDivisionID   
	JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = @numDomainID  
	JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = OM.numContactID
	JOIN Domain D ON D.numDomainID = OM.numDomainID
	LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = OM.numCurrencyID
    WHERE OM.numDomainID = @numDomainID 
	  AND OBD.[numOppBizDocsId] IN ( SELECT * FROM dbo.splitIds(@vcBizDocsIds ,',')) ORDER BY OM.numOppId
	
	DECLARE @numFldID AS INT    
	DECLARE @intRowNum AS INT    	
	DECLARE @vcFldname AS VARCHAR(MAX)	
	
	SET @strSQL = ''
	SELECT TOP 1 @numFldID = numFormfieldID,
				 @vcFldname = Fld_label,
				 @intRowNum = (intRowNum+1) 
	FROM DynamicFormConfigurationDetails DTL                                                                                                
	JOIN CFW_Fld_Master FIELD_MASTER ON FIELD_MASTER.Fld_id = DTL.numFormFieldID                                                                                                 
	WHERE DTL.numFormID = 7 
	  AND FIELD_MASTER.Grp_id = 5 
	  AND DTL.numDomainID = @numDomainID 
	  AND numAuthGroupID = @numBizDocId
	  AND vcFieldType = 'C' 
	ORDER BY intRowNum

	/**** START: Added Sales Tax Column ****/

	DECLARE @strSQLUpdate AS VARCHAR(2000);
	
	EXEC ( 'ALTER TABLE #temp_Packing_List ADD [TotalTax] MONEY'  )
	EXEC ( 'ALTER TABLE #temp_Packing_List ADD [CRV] MONEY'  )


	DECLARE @i AS INT = 1
	DECLARE @Count AS INT 
	SELECT @Count = COUNT(*) FROM #temp_Packing_List

	DECLARE @numOppId AS BIGINT
	DECLARE @numOppItemCode AS BIGINT
	DECLARE @tintTaxOperator AS INT

	WHILE @i <= @Count
	BEGIN
		SET @strSQLUpdate=''

		SELECT 
			@numDomainID = numDomainID,
			@numOppId = numOppId,
			@tintTaxOperator = tintTaxOperator,
			@numOppItemCode = numoppitemtCode
		FROM
			#temp_Packing_List
		WHERE
			SRNO = @i


		IF @tintTaxOperator = 1 
		BEGIN
			 SET @strSQLUpdate = @strSQLUpdate
				+ ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ',0,'
				+ CONVERT(VARCHAR(20), @numOppId) + ','
				+ CONVERT(VARCHAR(20), @numOppItemCode) + ',1,Amount,numUnitHour)'

			SET @strSQLUpdate = @strSQLUpdate
				+ ',[CRV]= dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ',1,'
				+ CONVERT(VARCHAR(20), @numOppId) + ','
				+ CONVERT(VARCHAR(20), @numOppItemCode) + ',1,Amount,numUnitHour)'
		END
		ELSE IF @tintTaxOperator = 2 -- remove sales tax
		BEGIN
			SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
			SET @strSQLUpdate = @strSQLUpdate + ',[CRV]= 0'
		END
		ELSE
		BEGIN
			 SET @strSQLUpdate = @strSQLUpdate
				+ ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ',0,'
				+ CONVERT(VARCHAR(20), @numOppId) + ','
				+ CONVERT(VARCHAR(20), @numOppItemCode) + ',1,Amount,numUnitHour)'

			SET @strSQLUpdate = @strSQLUpdate
				+ ',[CRV]= dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ',1,'
				+ CONVERT(VARCHAR(20), @numOppId) + ','
				+ CONVERT(VARCHAR(20), @numOppItemCode) + ',1,Amount,numUnitHour)'
		END

		IF @strSQLUpdate <> '' 
		BEGIN
			SET @strSQLUpdate = 'UPDATE #temp_Packing_List SET numItemCode=numItemCode' + @strSQLUpdate + ' WHERE numItemCode > 0 AND numOppId=' + CONVERT(VARCHAR(20), @numOppId) + ' AND numoppitemtCode=' + CONVERT(VARCHAR(20), @numOppItemCode)
			PRINT @strSQLUpdate
			EXEC (@strSQLUpdate)
		END

		SET @i = @i + 1
	END


	SET @strSQLUpdate = ''
	DECLARE @vcTaxName AS VARCHAR(100)
	DECLARE @numTaxItemID AS NUMERIC(9)
	SET @numTaxItemID = 0
	SELECT TOP 1 @vcTaxName = vcTaxName, @numTaxItemID = numTaxItemID FROM TaxItems WHERE numDomainID = @numDomainID

	WHILE @numTaxItemID > 0
	BEGIN

		EXEC ( 'ALTER TABLE #temp_Packing_List ADD [' + @vcTaxName + '] MONEY' )

		SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName  + ']= dbo.fn_CalBizDocTaxAmt(numDomainID,'
						+ CONVERT(VARCHAR(20), @numTaxItemID) + ',numOppId,numoppitemtCode,1,Amount,numUnitHour)'
           

		SELECT TOP 1 @vcTaxName = vcTaxName, @numTaxItemID = numTaxItemID FROM TaxItems WHERE numDomainID = @numDomainID AND numTaxItemID > @numTaxItemID

		IF @@rowcount = 0 
			SET @numTaxItemID = 0
	END


	IF @strSQLUpdate <> '' 
	BEGIN
		SET @strSQLUpdate = 'UPDATE #temp_Packing_List SET numItemCode=numItemCode' + @strSQLUpdate + ' WHERE numItemCode > 0' 
		PRINT @strSQLUpdate
		EXEC (@strSQLUpdate)
	END

	/**** END: Added Sales Tax Column ****/
	
	PRINT 'START'                                                                                          
	PRINT 'QUERY : ' + @strSQL
	PRINT @intRowNum
	WHILE @intRowNum > 0                                                                                  
	BEGIN    
			PRINT 'FROM LOOP'
			PRINT @vcFldname
			PRINT @numFldID

			EXEC('ALTER TABLE #temp_Packing_List add [' + @vcFldname + '] varchar(100)')
		
			SET @strSQL = 'UPDATE #temp_Packing_List SET [' + @vcFldname + '] = dbo.GetCustFldValueItem(' + CONVERT(VARCHAR(10),@numFldID) + ',numItemCode) 
						   WHERE numItemCode > 0'

			PRINT 'QUERY : ' + @strSQL
			EXEC (@strSQL)
			                                                                                 	                                                                                       
			SELECT TOP 1 @numFldID = numFormfieldID,
						 @vcFldname = Fld_label,
						 @intRowNum = (intRowNum + 1) 
			FROM DynamicFormConfigurationDetails DETAIL                                                                                                
			JOIN CFW_Fld_Master MASTER ON MASTER.Fld_id = DETAIL.numFormFieldID                                                                                                 
			WHERE DETAIL.numFormID = 7 
			  AND MASTER.Grp_id = 5 
			  AND DETAIL.numDomainID = @numDomainID 
			 AND numAuthGroupID = @numBizDocId                                                 
			  AND vcFieldType = 'C' 
			  AND intRowNum >= @intRowNum 
			ORDER BY intRowNum                                                                  
			           
			IF @@rowcount=0 SET @intRowNum=0                                                                                                                                                
		END	
	PRINT 'END'	
	SELECT * FROM #temp_Packing_List
END
END

/****** Object:  StoredProcedure [dbo].[USP_GETShippingLabels]    Script Date: 05/07/2009 17:43:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GETShippingLabels' ) 
    DROP PROCEDURE USP_GETShippingLabels
GO
--  EXEC USP_GETShippingLabels 0,'21993,21993,21993,21993,38382,38382',135,1
-- exec USP_GETShippingLabels @numBoxID=0,@vcOrderIds='1758,2304,2344,2344,2346,2346,2347,2347,2347,2347,2653,2653,2883,15152,17257,17257,17260,17752,18139,18597,18597,18597',@numDomainID=1,@tintMode=1
CREATE PROCEDURE [dbo].[USP_GETShippingLabels]
    (
      @numBoxID NUMERIC(9),
      @vcOrderIds VARCHAR(MAX),
      @numDomainID NUMERIC(10, 0),
      @tintMode TINYINT = 0,
      @vcOppBizDocIds AS VARCHAR(MAX) = ''
    )
AS 
    BEGIN
        IF @tintMode = 0 
            BEGIN
                SELECT  [vcShippingLabelImage]
		--		   [vcTrackingNumber]
                FROM    [ShippingBox]
                WHERE   [numBoxID] = @numBoxID
            END
	
        IF @tintMode = 1 
            BEGIN
				
				DECLARE @numBizDocType AS VARCHAR(MAX)
				SELECT @numBizDocType  = ISNULL(numBizDocType,0) FROM dbo.OpportunityOrderStatus WHERE numDomainID = @numDomainID
				PRINT @numBizDocType
	
				SELECT ROW_NUMBER() OVER (ORDER BY numOppBizDocsId) AS [ID], numOppBizDocsId INTO #tempBizDoc
				FROM dbo.OpportunityBizDocs OBD 
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OBD.numOppId
				WHERE 1 = 1
				AND OM.numDomainID = @numDomainID
		        AND OBD.numOppId IN (SELECT ID FROM dbo.SplitIDs(@vcOrderIDs, ','))
		        AND ISNULL(OBD.numBizDocId,0) =  @numBizDocType 
				
				DECLARE @intRow AS INT
				DECLARE @intCnt AS INT
				SET @intRow = 0
				SELECT @intCnt = COUNT(numOppBizDocsId) FROM #tempBizDoc
				
				PRINT @intCnt
				
				CREATE TABLE #tempImages
				(
					numShippingReportId		NUMERIC(18,0),
					numBoxID				NUMERIC(18,0),
					vcBoxName				VARCHAR(100),
					vcShippingLabelImage	VARCHAR(MAX),
					numShippingCompany		NUMERIC(18,0)
				)
				
				WHILE(@intRow < @intCnt)
					BEGIN
						SET @intRow = @intRow + 1
--						SELECT numOppBizDocsId FROM #tempBizDoc WHERE ID = @intRow
--						SELECT TOP 1 numShippingReportId FROM dbo.ShippingReport 
--														WHERE 1=1
--														AND numDomainID = 135
--														AND numOppBizDocId = (SELECT numOppBizDocsId FROM #tempBizDoc WHERE ID = @intRow)
--														ORDER BY numShippingReportId DESC
						
						INSERT INTO #tempImages 
						SELECT SB.numShippingReportId,numBoxID,vcBoxName,ISNULL(vcShippingLabelImage,''),numShippingCompany AS [numShippingCompany] FROM dbo.ShippingBox SB
						INNER JOIN dbo.ShippingReport SR ON SR.numShippingReportId = SB.numShippingReportId 
						WHERE 1=1 
						AND  SB.numShippingReportId = ( 
														SELECT TOP 1 numShippingReportId FROM dbo.ShippingReport 
														WHERE 1=1
														AND numDomainID = @numDomainID
														AND numOppBizDocId = (SELECT numOppBizDocsId FROM #tempBizDoc WHERE ID = @intRow)
														ORDER BY numShippingReportId DESC
													  )
					END
				
				--SELECT * FROM #tempBizDoc
				SELECT * FROM #tempImages
				
				DROP TABLE #tempImages	
				DROP TABLE #tempBizDoc
            END
		
		 IF @tintMode = 2
        BEGIN        
			SELECT  SB.numShippingReportId,numOppBizDocId,
					numBoxID,
					vcBoxName,
					ISNULL(vcShippingLabelImage, '') AS vcShippingLabelImage,
					numShippingCompany AS [numShippingCompany]
			FROM    dbo.ShippingBox SB
					INNER JOIN dbo.ShippingReport SR ON SR.numShippingReportId = SB.numShippingReportId
			WHERE   1 = 1
			AND SR.numOppBizDocId IN ( SELECT * FROM dbo.split(@vcOppBizDocIds,','))       

        END    


		IF @tintMode = 3
        BEGIN        
			SELECT  
				SB.numShippingReportId,
				OM.numOppId,
				ISNULL(vcPOppName,'') vcPOppName,
				numBoxID,
				vcBoxName,
				ISNULL(vcShippingLabelImage, '') AS vcShippingLabelImage,
				numShippingCompany AS [numShippingCompany]
			FROM    
				dbo.ShippingBox SB
			INNER JOIN 
				dbo.ShippingReport SR 
			ON 
				SR.numShippingReportId = SB.numShippingReportId
			INNER JOIN
				OpportunityMaster OM
			ON
				SR.numOppID=OM.numOppId
			WHERE 
				SR.numDomainId=@numDomainID
				AND SR.numOppID IN (SELECT Distinct Items FROM dbo.split(@vcOrderIds,',')) 
				AND CHARINDEX('.zpl',vcShippingLabelImage) = 0

			SELECT  
				SB.numShippingReportId,
				OM.numOppId,
				ISNULL(vcPOppName,'') vcPOppName,
				numBoxID,
				vcBoxName,
				ISNULL(vcShippingLabelImage, '') AS vcShippingLabelImage,
				numShippingCompany AS [numShippingCompany]
			FROM    
				dbo.ShippingBox SB
			INNER JOIN 
				dbo.ShippingReport SR 
			ON 
				SR.numShippingReportId = SB.numShippingReportId
			INNER JOIN
				OpportunityMaster OM
			ON
				SR.numOppID=OM.numOppId
			WHERE 
				SR.numDomainId=@numDomainID
				AND SR.numOppID IN (SELECT Distinct Items FROM dbo.split(@vcOrderIds,',')) 
				AND CHARINDEX('.zpl',vcShippingLabelImage) > 0

        END    
    END
    
/****** Object:  StoredProcedure [dbo].[USP_GetState]    Script Date: 07/26/2008 16:18:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- created By Anoop Jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getstate')
DROP PROCEDURE usp_getstate
GO
CREATE PROCEDURE [dbo].[USP_GetState]       
@numDomainID as numeric(9)=0,    
@numCountryID as numeric(9)=0,
@vcAbbreviations AS VARCHAR(300)=''      
as     
IF(LEN(@vcAbbreviations)>0)
BEGIN
	select numStateID from State  
	join ListDetails on numListItemID=  numCountryID     
	where State.numDomainID=@numDomainID   and numCountryID=@numCountryID and @vcAbbreviations IN (vcAbbreviations,vcState)
	order by  vcState
END
ELSE
BEGIN   
	SELECT 
		numStateID
		,vcState
		,numCountryID
		,LD.vcData
		,vcAbbreviations
		,ISNULL(numShippingZone,0) AS numShippingZone
		,ISNULL(LDShippingZone.vcData,'') AS vcShippingZone
	FROM 
		State  
	JOIN 
		ListDetails LD 
	ON 
		LD.numListItemID = numCountryID     
	LEFT JOIN
		ListDetails LDShippingZone
	ON
		State.numShippingZone = LDShippingZone.numListItemID
	WHERE 
		State.numDomainID = @numDomainID 
		AND numCountryID=@numCountryID
order by  vcState
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_InLineEditDataSave')
DROP PROCEDURE USP_InLineEditDataSave
GO
CREATE PROCEDURE [dbo].[USP_InLineEditDataSave] 
( 
 @numDomainID NUMERIC(9),
 @numUserCntID NUMERIC(9),
 @numFormFieldId NUMERIC(9),
 @bitCustom AS BIT = 0,
 @InlineEditValue VARCHAR(8000),
 @numContactID NUMERIC(9),
 @numDivisionID NUMERIC(9),
 @numWOId NUMERIC(9),
 @numProId NUMERIC(9),
 @numOppId NUMERIC(9),
 @numRecId NUMERIC(9),
 @vcPageName NVARCHAR(100),
 @numCaseId NUMERIC(9),
 @numWODetailId NUMERIC(9),
 @numCommID NUMERIC(9)
 )
AS 

declare @strSql as nvarchar(4000)
DECLARE @vcDbColumnName AS VARCHAR(100),@vcOrigDbColumnName AS VARCHAR(100),@vcLookBackTableName AS varchar(100)
DECLARE @vcListItemType as VARCHAR(3),@numListID AS numeric(9),@vcAssociatedControlType varchar(20)
declare @tempAssignedTo as numeric(9);set @tempAssignedTo=null           
DECLARE @Value1 AS VARCHAR(18),@Value2 AS VARCHAR(18)

IF  @bitCustom =0
BEGIN
	SELECT @vcDbColumnName=vcDbColumnName,@vcLookBackTableName=vcLookBackTableName,
		   @vcListItemType=vcListItemType,@numListID=numListID,@vcAssociatedControlType=vcAssociatedControlType,
		   @vcOrigDbColumnName=vcOrigDbColumnName
	 FROM DycFieldMaster WHERE numFieldId=@numFormFieldId
	 
	IF @vcAssociatedControlType = 'DateField' AND  @InlineEditValue=''
	BEGIN
		SET @InlineEditValue = null
	END

	DECLARE @intExecuteDiv INT=0
	if @vcLookBackTableName = 'DivisionMaster' 
	 BEGIN 
		
	   IF @vcDbColumnName='numFollowUpStatus' --Add to FollowUpHistory
	    Begin
			declare @numFollow as varchar(10),@binAdded as varchar(20),@PreFollow as varchar(10),@RowCount as varchar(2)                            
			                      
			set @PreFollow=(select count(*)  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID)                          
			set @RowCount=@@rowcount  
			                        
			select   @numFollow=numFollowUpStatus, @binAdded=bintModifiedDate from divisionmaster where numDivisionID=@numDivisionID and numDomainId= @numDomainID                    
			
			if @numFollow <>'0' and @numFollow <> @InlineEditValue                          
			begin                          
				if @PreFollow<>0                          
					begin                          
			                   
						if @numFollow <> (select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID order by numFollowUpStatusID desc)                          
							begin                          
			                      	insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
									values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
							end                          
					end                          
				else                          
					begin                          
						insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
						 values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
					end                          
			   end 
			END
		 IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from DivisionMaster where numDivisionID = @numDivisionID and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update DivisionMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID where numDivisionID = @numDivisionID  and numDomainId= @numDomainID  
						UPDATE CompanyInfo SET bintModifiedDate=GETUTCDATE() WHERE numCompanyId IN (SELECT numCompanyId 
																									FROM 
																										DivisionMaster
																									WHERE 
																										numDivisionID = @numDivisionID  and numDomainId= @numDomainID)         
					end					
				else if  (@InlineEditValue ='0')          
					begin          
						update DivisionMaster set numAssignedTo=0 ,numAssignedBy=0 where numDivisionID = @numDivisionID  and numDomainId= @numDomainID
						UPDATE CompanyInfo SET bintModifiedDate=GETUTCDATE() WHERE numCompanyId IN (SELECT numCompanyId 
																									FROM 
																										DivisionMaster
																									WHERE 
																										numDivisionID = @numDivisionID  and numDomainId= @numDomainID)          
					end    
			END
	    UPDATE CompanyInfo SET numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() WHERE numCompanyId=(SELECT TOP 1 numCompanyId FROM DivisionMaster where numDivisionID=@numDivisionID and numDomainID=@numDomainID) and numDomainID=@numDomainID
		
		IF @vcDbColumnName='vcPartnerCode'
		BEGIN
			IF(@InlineEditValue<>'')
			BEGIN
				IF EXISTS(SELECT vcPartnerCode FROM DivisionMaster where numDivisionID<>@numDivisionID and numDomainID=@numDomainID AND vcPartnerCode=@InlineEditValue)
				BEGIN
					SET @intExecuteDiv=1
					SET @InlineEditValue='-#12'
				END
			END
		END
		IF @intExecuteDiv=0
		BEGIN
			SET @strSql='update DivisionMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID'
		END
	 END 
	 ELSE if @vcLookBackTableName = 'AdditionalContactsInformation' 
	 BEGIN 
			IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='True'
			BEGIN
				DECLARE @bitPrimaryContactCheck AS BIT             
				DECLARE @numID AS NUMERIC(9) 
				
				SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
					FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID 
                   
					IF @@rowcount = 0 SET @numID = 0             
				
					WHILE @numID > 0            
					BEGIN            
						IF @bitPrimaryContactCheck = 1 
							UPDATE  AdditionalContactsInformation SET bitPrimaryContact = 0 WHERE numContactID = @numID  
                              
						SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
						FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactID > @numID    
                                
						IF @@rowcount = 0 SET @numID = 0             
					END 
				END
				ELSE IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='False'
				BEGIN
					IF (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactId!=@numContactId AND ISNULL(bitPrimaryContact,0)=1) = 0
						SET @InlineEditValue='True'
				END 
				
		SET @strSql='update AdditionalContactsInformation set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID and numContactId=@numContactId'
	 END 
	 ELSE if @vcLookBackTableName = 'CompanyInfo' 
	 BEGIN 
		SET @strSql='update CompanyInfo set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCompanyId=(select numCompanyId from divisionmaster where numDivisionID=@numDivisionID and numDomainID=@numDomainID) and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'WorkOrder' 
	 BEGIN 
		DECLARE @numItemCode AS NUMERIC(9)
	 
		IF @numWODetailId>0
		BEGIN
			SET @strSql='update WorkOrderDetails set ' + @vcDbColumnName + '=@InlineEditValue where numWOId=@numWOId and numWODetailId=@numWODetailId'
			
			IF @vcDbColumnName='numQtyItemsReq'
			BEGIN 
				SELECT @numItemCode=numChildItemID FROM WorkOrderDetails WHERE numWOId=@numWOId and numWODetailId=@numWODetailId
				
				EXEC USP_UpdateWorkOrderDetailQuantity @numItemCode,@InlineEditValue,@numWOId
				--UPDATE WorkOrderDetails SET numQtyItemsReq=CONVERT(NUMERIC(9),numQtyItemsReq_Orig * CONVERT(NUMERIC(9,0),@InlineEditValue)) WHERE numWOId=@numWOId
			END 
		END 	
		ELSE
		BEGIN
			SET @strSql='update WorkOrder set ' + @vcDbColumnName + '=@InlineEditValue where numWOId=@numWOId and numDomainID=@numDomainID'
			
			IF @vcDbColumnName='numQtyItemsReq'
			BEGIN 
				SELECT @numItemCode=numItemCode FROM WorkOrder WHERE numWOId=@numWOId and numDomainID=@numDomainID
			
				EXEC USP_UpdateWorkOrderDetailQuantity @numItemCode,@InlineEditValue,@numWOId
				--UPDATE WorkOrderDetails SET numQtyItemsReq=CONVERT(NUMERIC(9),numQtyItemsReq_Orig * CONVERT(NUMERIC(9,0),@InlineEditValue)) WHERE numWOId=@numWOId
			END 
		END	
	 END
	 ELSE if @vcLookBackTableName = 'ProjectsMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if Project is assigned to someone 
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from ProjectsMaster where numProId=@numProId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update ProjectsMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numProId = @numProId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update ProjectsMaster set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numProId = @numProId  and numDomainId= @numDomainID        
					end    
			END
		else IF @vcDbColumnName='numProjectStatus'  ---Updating All Statge to 100% if Completed
			BEGIN
				IF @InlineEditValue='27492'
				BEGIN
					UPDATE PM SET dtCompletionDate=GETUTCDATE(),monTotalExpense=PIE.monExpense,
					monTotalIncome=PIE.monIncome,monTotalGrossProfit=PIE.monBalance,numProjectStatus=27492
					FROM ProjectsMaster PM,dbo.GetProjectIncomeExpense(@numDomainId,@numProId) PIE
					WHERE PM.numProId=@numProId
					
					EXEC USP_ManageProjectCommission @numDomainId,@numProId
						
					Update StagePercentageDetails set tinProgressPercentage=100,bitclose=1 where numProjectID=@numProId

					EXEC dbo.USP_GetProjectTotalProgress
							@numDomainID = @numDomainID, --  numeric(9, 0)
							@numProId = @numProId, --  numeric(9, 0)
							@tintMode = 1 --  tinyint
				END			
				ELSE
				BEGIN
					DELETE from BizDocComission WHERE numDomainID = @numDomainID AND numProId=@numProId AND numComissionID 
						NOT IN (SELECT BC.numComissionID FROM dbo.BizDocComission BC JOIN dbo.PayrollTracking PT 
							ON BC.numComissionID = PT.numComissionID AND BC.numDomainId = PT.numDomainId WHERE BC.numDomainID = @numDomainID AND ISNULL(BC.numProId,0) = @numProId)
				END
			END
			

		SET @strSql='update ProjectsMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numProId=@numProId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'OpportunityMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from OpportunityMaster where numOppId=@numOppId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update OpportunityMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update OpportunityMaster set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID        
					end    
			END
		IF @vcDbColumnName='numPartner'
		 BEGIN
			update OpportunityMaster set numPartner=@InlineEditValue ,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID           
		 END
		ELSE IF @vcDbColumnName='tintActive'
		 BEGIN
		 	SET @InlineEditValue= CASE @InlineEditValue WHEN 'True' THEN 1 ELSE 0 END 
		 END
		 ELSE IF @vcDbColumnName='tintSource'
		 BEGIN
			IF CHARINDEX('~', @InlineEditValue)>0
			BEGIN
				SET @Value1=SUBSTRING(@InlineEditValue, 1,CHARINDEX('~', @InlineEditValue)-1) 
				SET @Value2=SUBSTRING(@InlineEditValue, CHARINDEX('~', @InlineEditValue)+1,LEN(@InlineEditValue)) 
			END	
			ELSE
			BEGIN
				SET @Value1=@InlineEditValue
				SET @Value2=0
			END
			
		 	update OpportunityMaster set tintSourceType=@Value2 where numOppId = @numOppId  and numDomainId= @numDomainID        
		 	
		 	SET @InlineEditValue= @Value1
		 END
		ELSE IF @vcDbColumnName='tintOppStatus' --Update Inventory  0=Open,1=Won,2=Lost
		 BEGIN
			declare @bitApprovalforOpportunity bit
			declare @intOpportunityApprovalProcess int
			SELECT @bitApprovalforOpportunity=bitApprovalforOpportunity,@intOpportunityApprovalProcess=intOpportunityApprovalProcess 
					FROM Domain WHERE numDomainId=@numDomainID
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT
		
			SET @bitViolatePrice=(SELECT bitMarginPriceViolated FROM Domain WHERE numDomainId=@numDomainID)
			SET @intPendingApprovePending=(SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppId AND ISNULL(bitItemPriceApprovalRequired,0)=1)
			IF(@intPendingApprovePending>0 AND @bitViolatePrice=1 AND @InlineEditValue='1')
			BEGIN
				SET @intExecuteDiv=1
				SET @InlineEditValue='-#123'
			END
			IF @intExecuteDiv=0
			BEGIN
			IF(@bitApprovalforOpportunity=1)
			BEGIN
				IF(@InlineEditValue='1' OR @InlineEditValue='2')
				BEGIN
					IF(@intOpportunityApprovalProcess=1)
					BEGIN
						UPDATE OpportunityMaster SET intPromotionApprovalStatus=6,intChangePromotionStatus=@InlineEditValue
						WHERE numOppId=@numOppId
					END
					IF(@intOpportunityApprovalProcess=2)
					BEGIN
						UPDATE OpportunityMaster SET intPromotionApprovalStatus=1,intChangePromotionStatus=@InlineEditValue
						WHERE numOppId=@numOppId
					END
				END
			END
			ELSE
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus = @InlineEditValue WHERE numOppId=@numOppID
				declare @tintOppStatus AS TINYINT
				select @tintOppStatus=tintOppStatus from OpportunityMaster where numOppID=@numOppID              

				if @tintOppStatus=0 and @InlineEditValue='1' --Open to Won
					BEGIN
						select @numDivisionID=numDivisionID from  OpportunityMaster where numOppID=@numOppID   
						DECLARE @tintCRMType AS TINYINT      
						select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

						-- Promote Lead to Account when Sales/Purchase Order is created against it
						if @tintCRMType=0 --Lead & Order
						begin        
							update divisionmaster set tintCRMType=2,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
							where numDivisionID=@numDivisionID        
						end
						--Promote Prospect to Account
						else if @tintCRMType=1 
						begin        
							update divisionmaster set tintCRMType=2,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
							where numDivisionID=@numDivisionID        
						end    

		 				EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID

						update OpportunityMaster set bintOppToOrder=GETUTCDATE() where numOppId=@numOppId and numDomainID=@numDomainID
					END
				if (@tintOppStatus=1 and @InlineEditValue='2') or (@tintOppStatus=1 and @InlineEditValue='0') --Won to Lost or Won to Open
					EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID 
			END
			END
		 END
		 ELSE IF @vcDbColumnName='vcOppRefOrderNo' --Update vcRefOrderNo OpportunityBizDocs
		 BEGIN
			 IF LEN(ISNULL(@InlineEditValue,''))>0
			 BEGIN
	   			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@InlineEditValue WHERE numOppId=@numOppID
			 END
		 END
		 ELSE IF @vcDbColumnName='numStatus' 
		 BEGIN
			DECLARE @tintOppType AS TINYINT
			select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType from OpportunityMaster where numOppID=@numOppID

			IF @tintOppType=1 AND @tintOppStatus=1
			BEGIN
				DECLARE @numOppStatus AS NUMERIC(18,0) = CAST(@InlineEditValue AS NUMERIC(18,0))
				EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numOppStatus
			END

		 	 IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @InlineEditValue)
			 BEGIN
	   				EXEC USP_ManageOpportunityAutomationQueue
							@numOppQueueID = 0, -- numeric(18, 0)
							@numDomainID = @numDomainID, -- numeric(18, 0)
							@numOppId = @numOppID, -- numeric(18, 0)
							@numOppBizDocsId = 0, -- numeric(18, 0)
							@numOrderStatus = @InlineEditValue, -- numeric(18, 0)
							@numUserCntID = @numUserCntID, -- numeric(18, 0)
							@tintProcessStatus = 1, -- tinyint
							@tintMode = 1 -- TINYINT
			END 
		 END	
		 ELSE IF @vcDbColumnName = 'dtReleaseDate'
		 BEGIN
			IF EXISTS (SELECT 1 FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND tintOppType=1 AND tintOppStatus=0)
			BEGIN
				IF (SELECT dbo.GetListIemName(ISNULL(numPercentageComplete,0)) FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId) = '100'
				BEGIN
					SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), @InlineEditValue),numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
				END
				ELSE
				BEGIN
					RAISERROR('PERCENT_COMPLETE_MUST_BE_100',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), @InlineEditValue),numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
			END
		END
		ELSE IF @vcDbColumnName = 'numPercentageComplete'
		BEGIN
			IF EXISTS (SELECT 1 FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND tintOppType=1 AND tintOppStatus=0 AND dbo.GetListIemName(ISNULL(numPercentageComplete,0))='100')
			BEGIN
				IF EXISTS(SELECT dtReleaseDate FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND dtReleaseDate IS NULL)
				BEGIN
					SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
				END
				ELSE
				BEGIN
					RAISERROR('REMOVE_RELEASE_DATE',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
			END
		END
		IF(@vcDbColumnName!='tintOppStatus')
		BEGIN
			SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
		END
	 END
	 ELSE if @vcLookBackTableName = 'Cases' 
	 BEGIN 
	 		IF @vcDbColumnName='numAssignedTo' ---Updating if Case is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from Cases where numCaseId=@numCaseId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update Cases set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numCaseId = @numCaseId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update Cases set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numCaseId = @numCaseId  and numDomainId= @numDomainID        
					end    
			END
		UPDATE Cases SET bintModifiedDate=GETUTCDATE() WHERE numCaseId = @numCaseId  and numDomainId= @numDomainID    
		SET @strSql='update Cases set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCaseId=@numCaseId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'Tickler' 
	 BEGIN 	
		SET @strSql='update Communication set ' + @vcOrigDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate() where numCommId=@numCommId and numDomainID=@numDomainID'
	 END
	 
	 
	EXECUTE sp_executeSQL @strSql, N'@numDomainID NUMERIC(9),@numUserCntID NUMERIC(9),@numDivisionID NUMERIC(9),@numContactID NUMERIC(9),@InlineEditValue VARCHAR(8000),@numWOId NUMERIC(9),@numProId NUMERIC(9),@numOppId NUMERIC(9),@numCaseId NUMERIC(9),@numWODetailId NUMERIC(9),@numCommID Numeric(9)',
		     @numDomainID,@numUserCntID,@numDivisionID,@numContactID,@InlineEditValue,@numWOId,@numProId,@numOppId,@numCaseId,@numWODetailId,@numCommID;
		     
	IF @vcAssociatedControlType='SelectBox'                                                    
       BEGIN       
			IF @vcDbColumnName='tintSource' AND @vcLookBackTableName = 'OpportunityMaster'
			BEGIN
				SELECT dbo.fn_GetOpportunitySourceValue(@Value1,@Value2,@numDomainID) AS vcData                                             
			END
			ELSE IF @vcDbColumnName='numContactID'
			BEGIN
				SELECT dbo.fn_GetContactName(@InlineEditValue)
			END
			ELSE IF @vcDbColumnName='numPartner'
			BEGIN
				SELECT D.vcPartnerCode+'-'+C.vcCompanyName  AS vcData FROM DivisionMaster AS D 
				LEFT JOIN CompanyInfo AS C
				ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=@InlineEditValue
			END
			ELSE IF @vcDbColumnName='tintOppStatus' AND @intExecuteDiv=1
			BEGIN
				SELECT @InlineEditValue AS vcData
			END
			else
			BEGIN
				SELECT dbo.fn_getSelectBoxValue(@vcListItemType,@InlineEditValue,@vcDbColumnName) AS vcData                                             
			END
		end 
		ELSE
		BEGIN
			 IF @vcDbColumnName='tintActive'
		  	 BEGIN
			 	SET @InlineEditValue= CASE @InlineEditValue WHEN 1 THEN 'True' ELSE 'False' END 
			 END

			IF @vcAssociatedControlType = 'Check box' or @vcAssociatedControlType = 'Checkbox'
			BEGIN
	 			SET @InlineEditValue= Case @InlineEditValue WHEN 'True' THEN 'Yes' ELSE 'No' END
			END
			
		    SELECT @InlineEditValue AS vcData
		END  
		
	IF @vcLookBackTableName = 'AdditionalContactsInformation' AND @vcDbColumnName = 'numECampaignID'
	BEGIN
		--Manage ECampiagn When Campaign is changed from inline edit
		DECLARE @Date AS DATETIME 
		SELECT @Date = dbo.[GetUTCDateWithoutTime]()
		DECLARE @numECampaignID AS NUMERIC(18,0)
		SET @numECampaignID = @InlineEditValue

		IF @numECampaignID = -1 
		SET @numECampaignID=0
	 
 		EXEC [USP_ManageConEmailCampaign]
		@numConEmailCampID = 0, --  numeric(9, 0)
		@numContactID = @numContactID, --  numeric(9, 0)
		@numECampaignID = @numECampaignID, --  numeric(9, 0)
		@intStartDate = @Date, --  datetime
		@bitEngaged = 1, --  bit
		@numUserCntID = @numUserCntID --  numeric(9, 0)
	END  
END
ELSE
BEGIN	 

	SET @InlineEditValue=Cast(@InlineEditValue as varchar(1000))
	SELECT @vcAssociatedControlType=Fld_type
	 FROM CFW_Fld_Master WHERE Fld_Id=@numFormFieldId 

	IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	    SET @InlineEditValue=(case when @InlineEditValue='False' then 0 ELSE 1 END)
	END

	IF @vcPageName='organization'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
  
	  END
	ELSE IF @vcPageName='contact'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Cont SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Cont (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END
			
			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Cont_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='project'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Pro SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Pro (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Pro_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END
	ELSE IF @vcPageName='opp'
	BEGIN 
		DECLARE @dtDate AS DATETIME = GETUTCDATE()

		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE 
				CFW_Fld_Values_Opp 
			SET 
				Fld_Value=@InlineEditValue ,
				numModifiedBy = @numUserCntID,
				bintModifiedDate = @dtDate
			WHERE 
				FldDTLID= (SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_Fld_Values_Opp (Fld_ID,Fld_Value,RecId,numModifiedBy,bintModifiedDate) VALUES (@numFormFieldId,@InlineEditValue,@numRecId,@numUserCntID,@dtDate)         
		END

		UPDATE OpportunityMaster SET numModifiedBy=@numUserCntID,bintModifiedDate=@dtDate WHERE numOppID = @numRecId 

		--Added By :Sachin Sadhu ||Date:14thOct2014
		--Purpose  :To manage CustomFields in Queue
		EXEC USP_CFW_Fld_Values_Opp_CT
				@numDomainID =@numDomainID,
				@numUserCntID =@numUserCntID,
				@numRecordID =@numRecId ,
				@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='case'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Case SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Case (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Case_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END

	IF @vcAssociatedControlType = 'SelectBox'            
	BEGIN            
		 SELECT vcData FROM ListDetails WHERE numListItemID=@InlineEditValue            
	END
	ELSE IF @vcAssociatedControlType = 'CheckBoxList'
	BEGIN
		DECLARE @str AS VARCHAR(MAX)
		SET @str = 'SELECT STUFF((SELECT CONCAT('','', vcData) FROM ListDetails WHERE numlistitemid IN (' + @InlineEditValue + ') FOR XML PATH('''')), 1, 1, '''')'
		EXEC(@str)
	END
	ELSE IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	 	select CASE @InlineEditValue WHEN 1 THEN 'Yes' ELSE 'No' END AS vcData
	END
	ELSE
	BEGIN
		SELECT @InlineEditValue AS vcData
	END 
END

/****** Object:  StoredProcedure [dbo].[USP_InsertDepositHeaderDet]    Script Date: 07/26/2008 16:19:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertdepositheaderdet')
DROP PROCEDURE usp_insertdepositheaderdet
GO
CREATE PROCEDURE [dbo].[USP_InsertDepositHeaderDet]                              
(@numDepositId AS NUMERIC(9)=0,        
@numChartAcntId AS NUMERIC(9)=0,                             
@datEntry_Date AS DATETIME,                                          
@numAmount AS MONEY,        
@numRecurringId AS NUMERIC(9)=0,                               
@numDomainId AS NUMERIC(9)=0,              
@numUserCntID AS NUMERIC(9)=0,
@strItems TEXT=''
,@numPaymentMethod NUMERIC
,@vcReference VARCHAR(500)
,@vcMemo VARCHAR(1000)
,@tintDepositeToType TINYINT=1 --1 = Direct Deposite to Bank Account , 2= Deposite to Default Undeposited Funds Account
,@numDivisionID NUMERIC
,@tintMode TINYINT =0 --0 = when called from receive payment page, 1 = when called from MakeDeposit Page
,@tintDepositePage TINYINT --1:Make Deposite 2:Receive Payment 3:Credit Memo(Sales Return)
,@numTransHistoryID NUMERIC(9)=0,
@numReturnHeaderID NUMERIC(9)=0,
@numCurrencyID numeric(9) =0,
@fltExchangeRate FLOAT = 1,
@numAccountClass NUMERIC(18,0)=0
)            
AS                                           
BEGIN 

IF ISNULL(@numCurrencyID,0) = 0 
BEGIN
 SET @fltExchangeRate=1
 SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
END
 

--Validation of closed financial year
EXEC dbo.USP_ValidateFinancialYearClosingDate @numDomainID,@datEntry_Date

DECLARE  @hDocItem INT
 
 IF @tintMode != 2
 BEGIN
	 IF @numRecurringId=0 SET @numRecurringId=NULL 
	 IF @numDivisionID=0 SET @numDivisionID=NULL
	 IF @numReturnHeaderID=0 SET @numReturnHeaderID=NULL
	 
	 IF @numDepositId=0                                  
	  BEGIN                              
		  INSERT INTO DepositMaster(numChartAcntId,dtDepositDate,monDepositAmount,numRecurringId,numDomainId,numCreatedBy,dtCreationDate,numPaymentMethod,vcReference,vcMemo,tintDepositeToType,numDivisionID,tintDepositePage,numTransHistoryID,numReturnHeaderID,numCurrencyID,fltExchangeRate,numAccountClass) VALUES            
	   (@numChartAcntId,@datEntry_Date,@numAmount,@numRecurringId,@numDomainId,@numUserCntID,GETUTCDATE(),@numPaymentMethod,@vcReference,@vcMemo,@tintDepositeToType,@numDivisionID,@tintDepositePage,@numTransHistoryID,@numReturnHeaderID,@numCurrencyID,ISNULL(@fltExchangeRate,1),@numAccountClass)                                        
	                         
	   SET @numDepositId = @@IDENTITY                                        
	  END                            
	                        
	 IF @numDepositId<>0                        
	  BEGIN                        
		UPDATE DepositMaster SET numChartAcntId=@numChartAcntId,dtDepositDate=@datEntry_Date,monDepositAmount=@numAmount,numRecurringId=@numRecurringId,
	   numModifiedBy=@numUserCntID,dtModifiedDate=GETUTCDATE(),numPaymentMethod =@numPaymentMethod,vcReference=@vcReference,vcMemo=@vcMemo,tintDepositeToType =@tintDepositeToType,numDivisionID=@numDivisionID,tintDepositePage=@tintDepositePage,numTransHistoryID=@numTransHistoryID ,numCurrencyID=@numCurrencyID,fltExchangeRate=@fltExchangeRate
	   WHERE numDepositId=@numDepositId AND numDomainId=@numDomainId
	  END            
 END 
  
  IF @tintMode = 1
  BEGIN
  
 
      IF CONVERT(VARCHAR(10),@strItems) <> ''
        BEGIN
          EXEC sp_xml_preparedocument
            @hDocItem OUTPUT ,
            @strItems
            
				-- When user unchecks deposit entry of undeposited fund, reset original entry
				UPDATE dbo.DepositMaster 
				SET bitDepositedToAcnt = 0 WHERE numDepositId IN (  
				
				 SELECT numChildDepositID FROM dbo.DepositeDetails WHERE numDepositID = @numDepositId AND numChildDepositID>0 
				 AND numDepositeDetailID NOT IN (SELECT X.numDepositeDetailID 
												FROM OPENXML(@hDocItem,'/NewDataSet/Item [numDepositeDetailID>0]',2)                                                                                                              
												WITH(numDepositeDetailID NUMERIC(9))X)
				
				)
			
			
              DELETE FROM dbo.DepositeDetails WHERE numDepositID = @numDepositId
              AND numDepositeDetailID NOT IN (SELECT X.numDepositeDetailID 
				 FROM OPENXML(@hDocItem,'/NewDataSet/Item [numDepositeDetailID>0]',2)                                                                                                              
				WITH(numDepositeDetailID NUMERIC(9))X)
			
			  INSERT INTO dbo.DepositeDetails
					  ( 
						numDepositID ,
						numOppBizDocsID ,
						numOppID ,
						monAmountPaid,
						numChildDepositID,
						numPaymentMethod,
						vcMemo,
						vcReference,
						numAccountID,
						numClassID,
						numProjectID,
						numReceivedFrom,dtCreatedDate,dtModifiedDate
					  )
			  SELECT 
					 @numDepositId,
					 NULL ,
					 NULL ,
					 X.monAmountPaid,
					 X.numChildDepositID,
					 X.numPaymentMethod,
						X.vcMemo,
						X.vcReference,
						X.numAccountID,
						X.numClassID,
						X.numProjectID,
						X.numReceivedFrom,GETUTCDATE(),GETUTCDATE()
			  FROM   (SELECT *
					  FROM   OPENXML (@hDocItem, '/NewDataSet/Item [numDepositeDetailID=0]', 2)
								WITH (
								numDepositeDetailID NUMERIC,
								numChildDepositID NUMERIC(9),
								monAmountPaid MONEY,
								numPaymentMethod NUMERIC(18, 0),
								vcMemo VARCHAR(1000),
								vcReference VARCHAR(500),
								numClassID	NUMERIC(18, 0),	
								numProjectID	NUMERIC(18, 0),
								numReceivedFrom	NUMERIC(18, 0),
								numAccountID NUMERIC(18, 0)
										)) X 
								
				UPDATE dbo.DepositeDetails
				SET
						[monAmountPaid] = X.monAmountPaid,
						[numChildDepositID] = X.numChildDepositID,
						[numPaymentMethod] = X.numPaymentMethod,
						[vcMemo] = X.vcMemo,
						[vcReference] = X.vcReference,
						[numClassID] = X.numClassID,
						[numProjectID] = X.numProjectID,
						[numReceivedFrom] = X.numReceivedFrom,
						[numAccountID] = X.numAccountID,
						dtModifiedDate=GETUTCDATE()
				FROM (SELECT *
									  FROM   OPENXML (@hDocItem, '/NewDataSet/Item [numDepositeDetailID>0]', 2)
												WITH (
												numDepositeDetailID NUMERIC,
												numChildDepositID NUMERIC(9),
												monAmountPaid MONEY,
												numPaymentMethod NUMERIC(18, 0),
												vcMemo VARCHAR(1000),
												vcReference VARCHAR(500),
												numClassID	NUMERIC(18, 0),	
												numProjectID	NUMERIC(18, 0),
												numReceivedFrom	NUMERIC(18, 0),
												numAccountID NUMERIC(18, 0)
														)) X 
				WHERE X.numDepositeDetailID =DepositeDetails.numDepositeDetailID AND dbo.DepositeDetails.numDepositID = @numDepositId




				UPDATE dbo.DepositMaster SET bitDepositedToAcnt=1 WHERE numDomainId=@numDomainID AND  numDepositId IN (
									   SELECT numChildDepositID
									  FROM   OPENXML (@hDocItem, '/NewDataSet/Item', 2)
												WITH (numChildDepositID NUMERIC(9)) WHERE numChildDepositID > 0
				)
				
								
  								
          EXEC sp_xml_removedocument
            @hDocItem
        END
  
  
  END 
  
  
  
  IF @tintMode = 0 OR @tintMode = 2
  BEGIN
      IF CONVERT(VARCHAR(10),@strItems) <> ''
        BEGIN
				  EXEC sp_xml_preparedocument
					@hDocItem OUTPUT ,
					@strItems
		            
						/*UPDATE dbo.OpportunityBizDocs 
						SET dbo.OpportunityBizDocs.monAmountPaid = ISNULL(dbo.OpportunityBizDocs.monAmountPaid,0) - ISNULL(X.monAmountPaid,0)
						FROM 				(SELECT numOppBizDocsID,monAmountPaid FROM dbo.DepositeDetails WHERE numDepositID = @numDepositId AND numOppBizDocsID>0 
												AND numDepositeDetailID NOT IN (SELECT numDepositeDetailID 
														FROM OPENXML(@hDocItem,'/NewDataSet/Item [numDepositeDetailID>0]',2)                                                                                                              
														WITH(numDepositeDetailID NUMERIC(9)) )) AS X
						WHERE  X.numOppBizDocsID = OpportunityBizDocs.numOppBizDocsId*/
						
					  SELECT 
					  numDepositeDetailID,numOppBizDocsID ,numOppID ,monAmountPaid INTO #temp
					  FROM   (SELECT *
							  FROM   OPENXML (@hDocItem, '/NewDataSet/Item', 2)
										WITH (numDepositeDetailID NUMERIC(9),numOppBizDocsID NUMERIC(9),numOppID NUMERIC(9),monAmountPaid MONEY)) X
				 	
					
					IF @tintMode = 0
					BEGIN
						UPDATE OBD
						SET monAmountPaid = ISNULL(OBD.monAmountPaid,0) - DD.monAmountPaid,OBD.numModifiedBy=@numUserCntID
						FROM OpportunityBizDocs OBD JOIN DepositeDetails DD ON OBD.numOppBizDocsId=DD.numOppBizDocsID
						WHERE DD.numDepositID = @numDepositId AND DD.numOppBizDocsID>0 
							 AND DD.numDepositeDetailID NOT IN (SELECT numDepositeDetailID 
														FROM #temp WHERE numDepositeDetailID>0)
														
													
						  DELETE FROM dbo.DepositeDetails WHERE numDepositID = @numDepositId
						  AND numDepositeDetailID NOT IN (SELECT X.numDepositeDetailID 
									FROM OPENXML(@hDocItem,'/NewDataSet/Item [numDepositeDetailID>0]',2)                                                                                                              
									WITH(numDepositeDetailID NUMERIC(9))X)
--							 AND numDepositeDetailID NOT IN (SELECT numDepositeDetailID 
--														FROM #temp WHERE numDepositeDetailID>0)
					 END
		              
		              
					  UPDATE  DD SET [monAmountPaid] = DD.monAmountPaid +  X.monAmountPaid 
								FROM DepositeDetails DD JOIN #temp X ON DD.numOppBizDocsID=X.numOppBizDocsID AND 
								DD.numOppID = X.numOppID
								WHERE X.numDepositeDetailID = 0 AND DD.numDepositID = @numDepositId
										
										
					  INSERT INTO dbo.DepositeDetails
							  ( numDepositID ,numOppBizDocsID ,numOppID ,monAmountPaid,dtCreatedDate,dtModifiedDate )
					  SELECT @numDepositId, numOppBizDocsID , numOppID , monAmountPaid,GETUTCDATE(),GETUTCDATE()
					  FROM #temp WHERE numDepositeDetailID=0 AND numOppBizDocsID NOT IN (SELECT numOppBizDocsID FROM DepositeDetails WHERE numDepositID = @numDepositId)
										
						
						IF @tintMode = 0
						BEGIN
							
		--				UPDATE OBD
		--				SET monAmountPaid = ISNULL(OBD.monAmountPaid,0) - DD.monAmountPaid
		--				FROM OpportunityBizDocs OBD JOIN DepositeDetails DD ON OBD.numOppBizDocsId=DD.numOppBizDocsID
		--				WHERE DD.numDepositID = @numDepositId AND DD.numOppBizDocsID>0 
		--					 AND DD.numDepositeDetailID IN (SELECT X.numDepositeDetailID 
		--												FROM OPENXML(@hDocItem,'/NewDataSet/Item [numDepositeDetailID>0]',2)                                                                                                              
		--												WITH(numDepositeDetailID NUMERIC(9))X)
					
						
								UPDATE  dbo.DepositeDetails
								SET     [monAmountPaid] = X.monAmountPaid ,
										numOppBizDocsID = X.numOppBizDocsID ,
										numOppID = X.numOppID,
										dtModifiedDate=GETUTCDATE()
								FROM    #temp X
								WHERE   X.numDepositeDetailID = DepositeDetails.numDepositeDetailID
										AND dbo.DepositeDetails.numDepositID = @numDepositId
						END


					UPDATE dbo.OpportunityBizDocs 
					SET dbo.OpportunityBizDocs.monAmountPaid = (SELECT ISNULL(SUM(ISNULL(monAmountPaid,0)),0) FROM DepositeDetails WHERE numOppBizDocsID  = OpportunityBizDocs.numOppBizDocsId AND numOppID  = OpportunityBizDocs.numOppID),numModifiedBy=@numUserCntID
					WHERE  OpportunityBizDocs.numOppBizDocsId IN (SELECT numOppBizDocsId FROM DepositeDetails WHERE numDepositId=@numDepositId)

					--Add to OpportunityAutomationQueue if full Amount Paid	
					INSERT INTO [dbo].[OpportunityAutomationQueue] ([numDomainID], [numOppId], [numOppBizDocsId], [numBizDocStatus], [numCreatedBy], [dtCreatedDate], [tintProcessStatus],numRuleID)
						SELECT OM.numDomainID, OM.numOppId, OBD.numOppBizDocsId, 0, @numUserCntID, GETUTCDATE(), 1,7	
						    FROM OpportunityBizDocs OBD JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
								WHERE OM.numDomainId=@numDomainId AND OBD.numOppBizDocsId IN (SELECT numOppBizDocsId FROM DepositeDetails WHERE numDepositId=@numDepositId)
								  AND ISNULL(OBD.monDealAmount,0)=ISNULL(OBD.monAmountPaid,0) AND ISNULL(OBD.monAmountPaid,0)>0

					--Add to OpportunityAutomationQueue if Balance due
					INSERT INTO [dbo].[OpportunityAutomationQueue] ([numDomainID], [numOppId], [numOppBizDocsId], [numBizDocStatus], [numCreatedBy], [dtCreatedDate], [tintProcessStatus],numRuleID)
						SELECT OM.numDomainID, OM.numOppId, OBD.numOppBizDocsId, 0, @numUserCntID, GETUTCDATE(), 1,14	
						    FROM OpportunityBizDocs OBD JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
								WHERE OM.numDomainId=@numDomainId AND OBD.numOppBizDocsId IN (SELECT numOppBizDocsId FROM DepositeDetails WHERE numDepositId=@numDepositId)
								  AND ISNULL(OBD.monDealAmount,0)>ISNULL(OBD.monAmountPaid,0) AND ISNULL(OBD.monAmountPaid,0)>0

														
  					UPDATE DepositMaster SET monAppliedAmount=(SELECT ISNULL(SUM(ISNULL(monAmountPaid,0)),0) FROM DepositeDetails WHERE numDepositId=@numDepositId)
  							WHERE numDepositId=@numDepositId
		  				
		  								
  					DROP TABLE #temp
		  								
				  EXEC sp_xml_removedocument
					@hDocItem
        END
        
        ELSE
        BEGIN
			--IF @tintMode = 0
			--BEGIN
				UPDATE OBD
				SET monAmountPaid = ISNULL(OBD.monAmountPaid,0) - DD.monAmountPaid,OBD.numModifiedBy=@numUserCntID
				FROM OpportunityBizDocs OBD JOIN DepositeDetails DD ON OBD.numOppBizDocsId=DD.numOppBizDocsID
				WHERE DD.numDepositID = @numDepositId 
											
				DELETE FROM dbo.DepositeDetails WHERE numDepositID = @numDepositId
				  
				UPDATE DepositMaster SET monAppliedAmount=(SELECT ISNULL(SUM(ISNULL(monAmountPaid,0)),0) FROM DepositeDetails WHERE numDepositId=@numDepositId)
  					WHERE numDepositId=@numDepositId
  		  
			--END
        END 
    END
  
  SELECT @numDepositId         
END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_Item_GetAll_BIZAPI' ) 
    DROP PROCEDURE USP_Item_GetAll_BIZAPI
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 9 April 2014
-- Description:	Gets list of items domain wise
-- =============================================
CREATE PROCEDURE USP_Item_GetAll_BIZAPI
	@numDomainID NUMERIC(18,0) = NULL,
	@numPageIndex AS INT = 0,
    @numPageSize AS INT = 0,
    @TotalRecords INT OUTPUT,
	@dtCreatedAfter DateTime = NULL,
	@dtModifiedAfter DateTime = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT 
		@TotalRecords = COUNT(*) 
	FROM 
		Item 
	WHERE 
		Item.numDomainID = @numDomainID
		AND (@dtCreatedAfter IS NULL OR Item.bintCreatedDate > @dtCreatedAfter)
		AND (@dtModifiedAfter IS NULL OR Item.bintModifiedDate > @dtModifiedAfter)

	SELECT
		*
	INTO
		#Temp
    FROM
	(SELECT
		ROW_NUMBER() OVER (ORDER BY Item.vcItemName asc) AS RowNumber,
		Item.numItemCode,
		Item.vcItemName,
		(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = Item.numItemCode AND ISNULL(bitDefault,0) = 1) AS vcPathForImage,
		(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = Item.numItemCode AND ISNULL(bitDefault,0) = 1) AS vcPathForTImage,
		Item.txtItemDesc,
		Item.charItemType,
		CASE 
			WHEN Item.charItemType='P' THEN 'Inventory Item' 
			WHEN Item.charItemType='N' THEN 'Non Inventory Item' 
			WHEN Item.charItemType='S' THEN 'Service' 
			WHEN Item.charItemType='A' THEN 'Accessory' 
		END AS ItemType,
		Item.vcSKU,
		Item.vcModelID,
		Item.numBarCodeId,
		Item.vcManufacturer,
		Item.vcUnitofMeasure,
		Item.monListPrice,
		(CASE WHEN ISNULL(Item.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(Item.monAverageCost,0.00) END) AS monAverageCost,
		Item.monCampaignLabourCost,
		Item.numItemGroup,
		Item.numItemClass,
		Item.numShipClass,
		Item.numItemClassification,
		Item.fltWidth,
		Item.fltWeight,
		Item.fltHeight,
		Item.fltLength,
		Item.numBaseUnit,
		Item.numSaleUnit,
		Item.numPurchaseUnit,
		Item.bitKitParent,
		Item.bitLotNo,
		Item.bitAssembly,
		Item.bitSerialized,
		Item.bitTaxable,
		Item.bitAllowBackOrder,
		Item.bitFreeShipping,
		Item.numCOGsChartAcntId,
		Item.numAssetChartAcntId,
		Item.numIncomeChartAcntId,
		Item.numVendorID,
		SUM(numOnHand) as numOnHand,                      
		SUM(numOnOrder) as numOnOrder,                      
		SUM(numReorder)  as numReorder,                      
		SUM(numAllocation)  as numAllocation,                      
		SUM(numBackOrder)  as numBackOrder,
		bintCreatedDate,
		bintModifiedDate,
		STUFF((SELECT CONCAT(',',numCategoryID) FROM ItemCategory WHERE numItemID=Item.numItemCode GROUP BY numCategoryID FOR XML PATH('')),1,1,'') AS vcCategories
	FROM
		Item
	LEFT JOIN  
		WareHouseItems W                  
	ON 
		W.numItemID=Item.numItemCode
	WHERE 
		Item.numDomainID = @numDomainID
		AND (@dtCreatedAfter IS NULL OR Item.bintCreatedDate > @dtCreatedAfter)
		AND (@dtModifiedAfter IS NULL OR Item.bintModifiedDate > @dtModifiedAfter)
	GROUP BY
		Item.numItemCode,
		Item.vcItemName,
		Item.txtItemDesc,
		Item.charItemType,
		Item.vcSKU,
		Item.vcModelID,
		Item.numBarCodeId,
		Item.vcManufacturer,
		Item.vcUnitofMeasure,
		Item.monListPrice,
		Item.monAverageCost,
		Item.bitVirtualInventory,
		Item.monCampaignLabourCost,
		Item.numItemGroup,
		Item.numItemClass,
		Item.numShipClass,
		Item.numItemClassification,
		Item.fltWidth,
		Item.fltWeight,
		Item.fltHeight,
		Item.fltLength,
		Item.numBaseUnit,
		Item.numSaleUnit,
		Item.numPurchaseUnit,
		Item.bitKitParent,
		Item.bitLotNo,
		Item.bitAssembly,
		Item.bitSerialized,
		Item.bitTaxable,
		Item.bitAllowBackOrder,
		Item.bitFreeShipping,
		Item.numCOGsChartAcntId,
		Item.numAssetChartAcntId,
		Item.numIncomeChartAcntId,
		Item.numVendorID,
		Item.bintCreatedDate,
		Item.bintModifiedDate
		) AS I
	WHERE 
		(@numPageIndex = 0 OR @numPageSize = 0) OR
		(RowNumber > ((@numPageIndex - 1) * @numPageSize) and RowNumber < ((@numPageIndex * @numPageSize) + 1))

	SELECT * FROM #Temp

	--GET CHILD ITEMS
	SELECT
		ItemDetails.numItemKitID,
		ItemDetails.numChildItemID,
		ItemDetails.numQtyItemsReq,
		WareHouseItems.numWareHouseItemID,
		Warehouses.numWareHouseID,
		(SELECT vcUnitName FROM UOM WHERE numUOMId = ItemDetails.numUOMId) AS vcUnitName
	FROM
		ItemDetails
	LEFT JOIN  
		WareHouseItems 
	ON
		ItemDetails.numWareHouseItemId = WareHouseItems.numWareHouseItemID
	LEFT JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	WHERE
		ItemDetails.numItemKitID IN (SELECT numItemCode FROM #Temp)

	--GET CUSTOM FIELDS
	SELECT 
		I.numItemCode,
		CFW_Fld_Master.Fld_id AS Id,
		CFW_Fld_Master.Fld_label AS Name,
		CFW_Fld_Master.Fld_type AS [Type],
		CFW_FLD_Values_Item.Fld_Value AS ValueId,
		CASE 
			WHEN CFW_Fld_Master.Fld_type='TextBox' or CFW_Fld_Master.Fld_type='TextArea' THEN
				(CASE WHEN CFW_FLD_Values_Item.Fld_Value='0' OR CFW_FLD_Values_Item.Fld_Value='' THEN '-' ELSE CFW_FLD_Values_Item.Fld_Value END)
			WHEN CFW_Fld_Master.Fld_type='SelectBox' THEN
				(CASE WHEN CFW_FLD_Values_Item.Fld_Value='' THEN '-' ELSE dbo.GetListIemName(CFW_FLD_Values_Item.Fld_Value) END)
			WHEN CFW_Fld_Master.Fld_type='CheckBox' THEN
				(CASE WHEN CFW_FLD_Values_Item.Fld_Value=0 THEN 'No' ELSE 'Yes' END)
			ELSE
				''
		END AS Value
	FROM 
		Item I
	INNER JOIN
		CFW_FLD_Values_Item 
	ON
		I.numItemCode = CFW_FLD_Values_Item.RecId
	INNER JOIN
		CFW_Fld_Master
	ON
		CFW_FLD_Values_Item.Fld_ID = CFW_Fld_Master.Fld_id
	WHERE 
		I.numDomainID = @numDomainID
		AND I.numItemCode IN (SELECT numItemCode FROM #Temp)


	DROp TABLE #Temp

END
GO


GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_Item_Search_BIZAPI' ) 
    DROP PROCEDURE USP_Item_Search_BIZAPI
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 9 April 2014
-- Description:	Search list of items domain wise
-- =============================================
CREATE PROCEDURE USP_Item_Search_BIZAPI
	@numDomainID NUMERIC(18,0) = NULL,
	@SearchText VARCHAR(100),
	@numPageIndex AS INT = 0,
    @numPageSize AS INT = 0,
    @TotalRecords INT OUTPUT,
	@dtCreatedAfter DateTime = NULL,
	@dtModifiedAfter DateTime = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT 
		@TotalRecords = COUNT(*) 
	FROM 
		Item 
	WHERE 
		Item.numDomainID = @numDomainID 
		AND	Item.vcItemName LIKE '%'+@SearchText+'%'
		AND (@dtCreatedAfter IS NULL OR Item.bintCreatedDate > @dtCreatedAfter)
		AND (@dtModifiedAfter IS NULL OR Item.bintModifiedDate > @dtModifiedAfter)

	SELECT
		*
	INTO
		#Temp
    FROM
	(SELECT
		ROW_NUMBER() OVER (ORDER BY Item.vcItemName asc) AS RowNumber,
		Item.numItemCode,
		Item.vcItemName,
		(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = Item.numItemCode AND ISNULL(bitDefault,0) = 1) AS vcPathForImage,
		(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = Item.numItemCode AND ISNULL(bitDefault,0) = 1) AS vcPathForTImage,
		Item.txtItemDesc,
		Item.charItemType,
		CASE 
			WHEN Item.charItemType='P' THEN 'Inventory Item' 
			WHEN Item.charItemType='N' THEN 'Non Inventory Item' 
			WHEN Item.charItemType='S' THEN 'Service' 
			WHEN Item.charItemType='A' THEN 'Accessory' 
		END AS ItemType,
		Item.vcSKU,
		Item.vcModelID,
		Item.numBarCodeId,
		Item.vcManufacturer,
		Item.vcUnitofMeasure,
		Item.monListPrice,
		(CASE WHEN ISNULL(Item.bitVirtualInventory,0) = 1 THEN 0 ELSE Item.monAverageCost END) monAverageCost,
		Item.monCampaignLabourCost,
		Item.numItemGroup,
		Item.numItemClass,
		Item.numShipClass,
		Item.numItemClassification,
		Item.fltWidth,
		Item.fltWeight,
		Item.fltHeight,
		Item.fltLength,
		Item.numBaseUnit,
		Item.numSaleUnit,
		Item.numPurchaseUnit,
		Item.bitKitParent,
		Item.bitLotNo,
		Item.bitAssembly,
		Item.bitSerialized,
		Item.bitTaxable,
		Item.bitAllowBackOrder,
		Item.bitFreeShipping,
		Item.numCOGsChartAcntId,
		Item.numAssetChartAcntId,
		Item.numIncomeChartAcntId,
		Item.numVendorID,
		SUM(numOnHand) as numOnHand,                      
		SUM(numOnOrder) as numOnOrder,                      
		SUM(numReorder)  as numReorder,                      
		SUM(numAllocation)  as numAllocation,                      
		SUM(numBackOrder)  as numBackOrder,
		bintCreatedDate,
		bintModifiedDate,
		STUFF((SELECT CONCAT(',',numCategoryID) FROM ItemCategory WHERE numItemID=Item.numItemCode GROUP BY numCategoryID FOR XML PATH('')),1,1,'') AS vcCategories
	FROM
		Item
	LEFT JOIN  
		WareHouseItems W                  
	ON 
		W.numItemID=Item.numItemCode
	WHERE 
		Item.numDomainID = @numDomainID AND
		Item.vcItemName LIKE '%'+@SearchText+'%'
		AND (@dtCreatedAfter IS NULL OR Item.bintCreatedDate > @dtCreatedAfter)
		AND (@dtModifiedAfter IS NULL OR Item.bintModifiedDate > @dtModifiedAfter)
	GROUP BY
		Item.numItemCode,
		Item.vcItemName,
		Item.txtItemDesc,
		Item.charItemType,
		Item.vcSKU,
		Item.vcModelID,
		Item.numBarCodeId,
		Item.vcManufacturer,
		Item.vcUnitofMeasure,
		Item.monListPrice,
		Item.monAverageCost,
		Item.bitVirtualInventory,
		Item.monCampaignLabourCost,
		Item.numItemGroup,
		Item.numItemClass,
		Item.numShipClass,
		Item.numItemClassification,
		Item.fltWidth,
		Item.fltWeight,
		Item.fltHeight,
		Item.fltLength,
		Item.numBaseUnit,
		Item.numSaleUnit,
		Item.numPurchaseUnit,
		Item.bitKitParent,
		Item.bitLotNo,
		Item.bitAssembly,
		Item.bitSerialized,
		Item.bitTaxable,
		Item.bitAllowBackOrder,
		Item.bitFreeShipping,
		Item.numCOGsChartAcntId,
		Item.numAssetChartAcntId,
		Item.numIncomeChartAcntId,
		Item.numVendorID,
		Item.bintCreatedDate,
		Item.bintModifiedDate) AS I
	WHERE 
		(@numPageIndex = 0 OR @numPageSize = 0) OR
		(RowNumber > ((@numPageIndex - 1) * @numPageSize) and RowNumber < ((@numPageIndex * @numPageSize) + 1))

	SELECT * FROM #Temp

	--GET CHILD ITEMS
	SELECT
		ItemDetails.numItemKitID,
		ItemDetails.numChildItemID,
		ItemDetails.numQtyItemsReq,
		WareHouseItems.numWareHouseItemID,
		Warehouses.numWareHouseID,
		(SELECT vcUnitName FROM UOM WHERE numUOMId = ItemDetails.numUOMId) AS vcUnitName
	FROM
		ItemDetails
	LEFT JOIN  
		WareHouseItems 
	ON
		ItemDetails.numWareHouseItemId = WareHouseItems.numWareHouseItemID
	LEFT JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	WHERE
		ItemDetails.numItemKitID IN (SELECT numItemCode FROM #Temp)

	--GET CUSTOM FIELDS
	SELECT 
		I.numItemCode,
		CFW_Fld_Master.Fld_id AS Id,
		CFW_Fld_Master.Fld_label AS Name,
		CFW_Fld_Master.Fld_type AS [Type],
		CFW_FLD_Values_Item.Fld_Value AS ValueId,
		CASE 
			WHEN CFW_Fld_Master.Fld_type='TextBox' or CFW_Fld_Master.Fld_type='TextArea' THEN
				(CASE WHEN CFW_FLD_Values_Item.Fld_Value='0' OR CFW_FLD_Values_Item.Fld_Value='' THEN '-' ELSE CFW_FLD_Values_Item.Fld_Value END)
			WHEN CFW_Fld_Master.Fld_type='SelectBox' THEN
				(CASE WHEN CFW_FLD_Values_Item.Fld_Value='' THEN '-' ELSE dbo.GetListIemName(CFW_FLD_Values_Item.Fld_Value) END)
			WHEN CFW_Fld_Master.Fld_type='CheckBox' THEN
				(CASE WHEN CFW_FLD_Values_Item.Fld_Value=0 THEN 'No' ELSE 'Yes' END)
			ELSE
				''
		END AS Value
	FROM 
		Item I
	INNER JOIN
		CFW_FLD_Values_Item 
	ON
		I.numItemCode = CFW_FLD_Values_Item.RecId
	INNER JOIN
		CFW_Fld_Master
	ON
		CFW_FLD_Values_Item.Fld_ID = CFW_Fld_Master.Fld_id
	WHERE 
		I.numDomainID = @numDomainID
		AND I.numItemCode IN (SELECT numItemCode FROM #Temp)


	DROp TABLE #Temp
END
GO

GO
/****** Object:  StoredProcedure [dbo].[usp_ManageAddlContInfo]    Script Date: 04/02/2009 00:17:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified By Anoop jayaraj                  
 GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageaddlcontinfo')
DROP PROCEDURE usp_manageaddlcontinfo
GO
CREATE PROCEDURE [dbo].[usp_ManageAddlContInfo]                                    
 @numcontactId numeric=0,                                    
 @numContactType numeric=0,                                    
 @vcDepartment numeric(9)=0,                                    
 @vcCategory numeric(9)=0,                                    
 @vcGivenName varchar(100)='',                                    
 @vcFirstName varchar(50)='',                                    
 @vcLastName varchar(50)='',                                    
 @numDivisionId numeric,                                    
 @numPhone varchar(15)='',                                    
 @numPhoneExtension varchar(7)='',                                    
 @numCell varchar(15)='',                                    
 @NumHomePhone varchar(15)='',                                    
 @vcFax varchar(15)='',                                    
 @vcEmail varchar(50)='',                                    
 @VcAsstFirstName varchar(50)='',                                    
 @vcAsstLastName varchar(50)='',                                    
 @numAsstPhone varchar(15)='',                                    
 @numAsstExtn varchar(6)='',                                    
 @vcAsstEmail varchar(50)='',                                                      
 @charSex char(1)='',                                    
 @bintDOB datetime,                                    
 @vcPosition numeric(9)=0,                                                     
 @txtNotes text='',                                                     
 @numUserCntID numeric,                                                                                              
 @numDomainID numeric=1,                                                   
 @vcPStreet varchar(100)='',                                    
 @vcPCity varchar(50)='',                                    
 @vcPPostalCode varchar(15)='',                                    
 @vcPState numeric(9)=0,                                    
 @vcPCountry numeric(9)=0,                  
 @numManagerID numeric=0,        
 @numTeam as numeric(9)=0,        
 @numEmpStatus as numeric(9)=0,
 @vcTitle AS VARCHAR(100)='',
 @bitPrimaryContact AS BIT=0,
 @bitOptOut AS BIT=0,
 @vcLinkedinId VARCHAR(30)=NULL,
 @vcLinkedinUrl VARCHAR(300)=NULL,
 @numECampaignID NUMERIC(18)=0
AS   


 SELECT @vcGivenName = @vcLastName + ', ' + @vcFirstName  + '.eml'                                 
   if @bintDOB ='Jan  1 1753 12:00AM' set  @bintDOB=null                      
IF @numContactId=0
     BEGIN   

IF @bitPrimaryContact = 1 
    BEGIN            
        DECLARE @numID AS NUMERIC(9)            
        DECLARE @bitPrimaryContactCheck AS BIT             
             
        SELECT TOP 1
                @numID = numContactID,
                @bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
        FROM    AdditionalContactsInformation
        WHERE   numDivisionID = @numDivisionID 
                   
        IF @@rowcount = 0 
            SET @numID = 0             
        WHILE @numID > 0            
            BEGIN            
                IF @bitPrimaryContactCheck = 1 
                    UPDATE  AdditionalContactsInformation
                    SET     bitPrimaryContact = 0
                    WHERE   numContactID = @numID  
                              
                SELECT TOP 1
                        @numID = numContactID,
                        @bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
                FROM    AdditionalContactsInformation
                WHERE   numDivisionID = @numDivisionID
                        AND numContactID > @numID    
                                
                IF @@rowcount = 0 
                    SET @numID = 0             
            END            
    END  
    ELSE IF @bitPrimaryContact = 0
    BEGIN
		IF (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactId!=@numContactId AND ISNULL(bitPrimaryContact,0)=1)=0
			SET @bitPrimaryContact = 1
    END                 
    
    
declare @bitAutoPopulateAddress bit 
declare @tintPoulateAddressTo tinyint 

select @bitAutoPopulateAddress= isnull(bitAutoPopulateAddress,'0'),@tintPoulateAddressTo=isnull(tintPoulateAddressTo,'0') from Domain where numDomainId = @numDomainId ;
if (@bitAutoPopulateAddress = 1)
	if(@tintPoulateAddressTo = 1) --if primary address is not specified then getting billing address    
	begin
		 
		  select                   
			@vcPStreet = AD1.vcStreet,
			@vcPCity= AD1.vcCity,
			@vcPState= AD1.numState,
			@vcPPostalCode= AD1.vcPostalCode,
			@vcPCountry= AD1.numCountry              
		  from divisionMaster  DM              
		   LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
			AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
		  where numDivisionID=@numDivisionId    
	end  
	else if (@tintPoulateAddressTo = 2)-- Primary Address is Shipping Address
	begin
		  select                   
			@vcPStreet= AD2.vcStreet,
			@vcPCity=AD2.vcCity,
			@vcPState=AD2.numState,
			@vcPPostalCode=AD2.vcPostalCode,
			@vcPCountry=AD2.numCountry                   
		  from divisionMaster DM
		  LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
		  AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
		  where numDivisionID=@numDivisionId    
	
	end                       
                       
 if (@numEmpStatus is null or @numEmpStatus=0) set @numEmpStatus=658        
 
    INSERT into AdditionalContactsInformation                                    
    (                  
   numContactType,                  
   vcDepartment,                  
   vcCategory,                  
   vcGivenName,                  
   vcFirstName,                  
   vcLastName,                   
   numDivisionId ,                  
   numPhone ,                  
   numPhoneExtension,                  
   numCell ,                  
   numHomePhone ,                          
   vcFax ,                  
   vcEmail ,                  
   VcAsstFirstName ,                  
   vcAsstLastName ,                  
   numAsstPhone ,                  
   numAsstExtn ,                  
   vcAsstEmail  ,                  
   charSex ,                  
   bintDOB ,                  
   vcPosition ,                  
   txtNotes ,                  
   numCreatedBy ,                                    
   bintCreatedDate,             
   numModifiedBy,            
   bintModifiedDate,                   
   numDomainID,                  
   numManagerID,                   
   numRecOwner,                  
   numTeam,                  
   numEmpStatus,
   vcTitle,bitPrimaryContact,bitOptOut,
   vcLinkedinId,vcLinkedinUrl,numECampaignID           
   )                                    
    VALUES                                    
      (                  
   @numContactType,                  
   @vcDepartment,                  
   @vcCategory,                  
   @vcGivenName ,                  
   @vcFirstName ,                  
   @vcLastName,                   
   @numDivisionId ,                  
   @numPhone ,                                    
   @numPhoneExtension,                  
   @numCell ,                  
   @NumHomePhone ,                  
   @vcFax ,                  
   @vcEmail ,                  
   @VcAsstFirstName,                         
   @vcAsstLastName,                  
   @numAsstPhone,                  
   @numAsstExtn,                  
   @vcAsstEmail,                  
   @charSex,                  
   @bintDOB ,                  
   @vcPosition,                                   
   @txtNotes,                  
   @numUserCntID,                  
   getutcdate(),             
   @numUserCntID,                  
   getutcdate(),                   
   @numDomainID,                   
   @numManagerID,                   
   @numUserCntID,                   
   @numTeam,                  
   @numEmpStatus,
   @vcTitle,@bitPrimaryContact,@bitOptOut,
   @vcLinkedinId,@vcLinkedinUrl,@numECampaignID                
   )                                    
                     
 set @numcontactId= @@IDENTITY                    

     INSERT INTO dbo.AddressDetails (
	 	vcAddressName,
	 	vcStreet,
	 	vcCity,
	 	vcPostalCode,
	 	numState,
	 	numCountry,
	 	bitIsPrimary,
	 	tintAddressOf,
	 	tintAddressType,
	 	numRecordID,
	 	numDomainID
	 ) VALUES ('Primary', @vcPStreet,@vcPCity,@vcPPostalCode,@vcPState,@vcPCountry,1,1,0,@numcontactId,@numDomainID)
     
	 IF ISNULL(@numECampaignID,0) > 0 
	 BEGIN
		  --Manage ECampiagn When Campaign is changed from inline edit
		DECLARE @Date AS DATETIME 
		SELECT @Date = dbo.[GetUTCDateWithoutTime]()

 		EXEC [USP_ManageConEmailCampaign]
		@numConEmailCampID = 0, --  numeric(9, 0)
		@numContactID = @numcontactId, --  numeric(9, 0)
		@numECampaignID = @numECampaignID, --  numeric(9, 0)
		@intStartDate = @Date, --  datetime
		@bitEngaged = 1, --  bit
		@numUserCntID = @numUserCntID --  numeric(9, 0)     
	END                                         
                               
 select @numcontactId                                                  
  END                                    
 ELSE if @numContactId>0                                                        
    BEGIN                                    
    UPDATE AdditionalContactsInformation SET                                    
      numContactType=@numContactType,                                    
      vcGivenName=@vcGivenName,                                    
      vcFirstName=@vcFirstName ,                                    
      vcLastName =@vcLastName ,                   
      numDivisionId =@numDivisionId ,                                    
      numPhone=@numPhone ,                                    
      numPhoneExtension=@numPhoneExtension,                                    
      numCell =@numCell ,                                    
      NumHomePhone =@NumHomePhone ,                                    
      vcFax=@vcFax ,                                    
      vcEmail=@vcEmail ,                                    
      VcAsstFirstName=@VcAsstFirstName,                                    
      vcAsstLastName=@vcAsstLastName,                                    
      numAsstPhone=@numAsstPhone,                                    
      numAsstExtn=@numAsstExtn,                                    
      vcAsstEmail=@vcAsstEmail,                                                      
      charSex=@charSex,                            
      bintDOB=@bintDOB ,                                    
      vcPosition=@vcPosition,                                                    
      txtNotes=@txtNotes,                                                     
      numModifiedBy=@numUserCntID,                                    
      bintModifiedDate=getutcdate(),                                    
      numManagerID=@numManagerID,
      bitPrimaryContact=@bitPrimaryContact                                    
     WHERE numContactId=@numContactId   
                    
     Update dbo.AddressDetails set                   
       vcStreet=@vcPStreet,                                    
       vcCity =@vcPCity,                                    
       vcPostalCode=@vcPPostalCode,                                     
       numState=@vcPState,                                    
       numCountry=@vcPCountry
	  where numRecordID=@numContactId AND bitIsPrimary=1 AND tintAddressOf=1 AND tintAddressType=0
 
     SELECT @numcontactId   
                                                
  END    
  ------Check the Email id Exist into the Email Master Table

DECLARE @numCount AS NUMERIC(18,0)
SET @numCount = 0
IF @vcEmail <> ''
BEGIN 
SELECT @numCount = COUNT(*) FROM dbo.EmailMaster WHERE vcEmailID =@vcEmail and numDomainId=@numDomainID
IF @numCount = 0 
BEGIN
	INSERT INTO EmailMaster(vcEmailId,vcName,numDomainId,numContactId)
	VALUES(@vcEmail,@vcFirstName,@numDomainID,@numcontactId)
END
else
begin
	update EmailMaster set numContactId=@numcontactId where vcEmailID =@vcEmail and numDomainId=@numDomainID
end
END 
--IF @vcAsstEmail <> ''
--BEGIN
--SELECT @numCount = COUNT(*) FROM dbo.EmailMaster WHERE vcEmailID =@vcAsstEmail
--IF @numCount = 0 
--BEGIN
--	INSERT INTO EmailMaster(vcEmailId,vcName,numDomainId,numContactId)
--	VALUES(@vcAsstEmail,@vcFirstName,@numDomainID,@numcontactId)
--END
--END
---------------------------------------------------------

/****** Object:  StoredProcedure [dbo].[USP_ManageState]    Script Date: 07/26/2008 16:20:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managestate')
DROP PROCEDURE usp_managestate
GO
CREATE PROCEDURE [dbo].[USP_ManageState]   
@numStateID as numeric(9)=0,    
@numDomainID as numeric(9)=0,       
@vcState as varchar(100)='',  
@numCountryID as numeric(9),      
@numUserID as numeric(9)  ,
@vcAbbreviations varchar(200),
@numShippingZone NUMERIC(18,0) = 0
AS
	IF @numStateID = 0     
	BEGIN      
		INSERT INTO STATE
		(
			numCountryID,vcState,vcAbbreviations,numDomainID,numCreatedBy,numModifiedBy,bintCreatedDate,bintModifiedDate,numShippingZone
		)
		VALUES
		(
			@numCountryID,@vcState,@vcAbbreviations,@numDomainID,@numUserID,@numUserID,getutcdate(),getutcdate(),@numShippingZone
		)      
	END      
	ELSE      
	BEGIN      
		UPDATE 
			State 
		SET 
			vcState=@vcState
			,numCountryID=@numCountryID
			,numDomainID=@numDomainID
			,numModifiedBy=@numUserID
			,vcAbbreviations=@vcAbbreviations
			,numShippingZone=@numShippingZone
			,bintModifiedDate=getutcdate()      
		WHERE 
			numStateID=@numStateID 
			AND numDomainID=@numDomainID  
         
	end
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageSubscribers]    Script Date: 07/26/2008 16:20:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                    
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_managesubscribers' ) 
    DROP PROCEDURE usp_managesubscribers
GO
CREATE PROCEDURE [dbo].[USP_ManageSubscribers]
    @numSubscriberID AS NUMERIC(9) OUTPUT,
    @numDivisionID AS NUMERIC(9),
    @intNoofUsersSubscribed AS INTEGER,
    @intNoOfPartners AS INTEGER,
    @dtSubStartDate AS DATETIME,
    @dtSubEndDate AS DATETIME,
    @bitTrial AS BIT,
    @numAdminContactID AS NUMERIC(9),
    @bitActive AS TINYINT,
    @vcSuspendedReason AS VARCHAR(100),
    @numTargetDomainID AS NUMERIC(9) OUTPUT,
    @numDomainID AS NUMERIC(9),
    @numUserContactID AS NUMERIC(9),
    @strUsers AS TEXT = '',
    @bitExists AS BIT = 0 OUTPUT,
    @TargetGroupId AS NUMERIC(9) = 0 OUTPUT,
    @tintLogin TINYINT,
    @intNoofPartialSubscribed AS INTEGER,
    @intNoofMinimalSubscribed AS INTEGER,
	@dtEmailStartDate DATETIME,
	@dtEmailEndDate DATETIME,
	@intNoOfEmail INT,
	@bitEnabledAccountingAudit BIT,
	@bitEnabledNotifyAdmin BIT
AS 
    IF @numSubscriberID = 0 
        BEGIN                                 
            IF NOT EXISTS ( SELECT  *
                            FROM    Subscribers
                            WHERE   numDivisionID = @numDivisionID
                                    AND numDomainID = @numDomainID ) 
                BEGIN              
                    SET @bitExists = 0                              
                    DECLARE @vcCompanyName AS VARCHAR(100)                             
                    DECLARE @numCompanyID AS NUMERIC(9)                                   
                    SELECT  @vcCompanyName = vcCompanyName,
                            @numCompanyID = C.numCompanyID
                    FROM    CompanyInfo C
                            JOIN DivisionMaster D ON D.numCompanyID = C.numCompanyID
                    WHERE   D.numDivisionID = @numDivisionID                                    
                                
                    DECLARE @numNewCompanyID AS NUMERIC(9)                                   
                    DECLARE @numNewDivisionID AS NUMERIC(9)                                    
                    DECLARE @numNewContactID AS NUMERIC(9)                                    
                                      
                    INSERT  INTO Domain
                            (
                              vcDomainName,
                              vcDomainDesc,
                              numDivisionID,
                              numAdminID,
                              tintLogin,tintDecimalPoints,tintChrForComSearch,tintChrForItemSearch, bitIsShowBalance,tintComAppliesTo,tintCommissionType, charUnitSystem
                            )
                    VALUES  (
                              @vcCompanyName,
                              @vcCompanyName,
                              @numDivisionID,
                              @numAdminContactID,
                              @tintLogin,2,1,1,0,3,1,'E'
                            )                                    
                    SET @numTargetDomainID = @@identity                                       
                                       
                    INSERT  INTO CompanyInfo
                            (
                              vcCompanyName,
                              numCompanyType,
                              numCompanyRating,
                              numCompanyIndustry,
                              numCompanyCredit,
                              vcWebSite,
                              vcWebLabel1,
                              vcWebLink1,
                              vcWebLabel2,
                              vcWebLink2,
                              vcWebLabel3,
                              vcWebLink3,
                              vcWeblabel4,
                              vcWebLink4,
                              numAnnualRevID,
                              numNoOfEmployeesId,
                              vcHow,
                              vcProfile,
                              bitPublicFlag,
                              numDomainID
                            )
                            SELECT  vcCompanyName,
                                    93,--i.e employer --numCompanyType,
                                    numCompanyRating,
                                    numCompanyIndustry,
                                    numCompanyCredit,
                                    vcWebSite,
                                    vcWebLabel1,
                                    vcWebLink1,
                                    vcWebLabel2,
                                    vcWebLink2,
                                    vcWebLabel3,
                                    vcWebLink3,
                                    vcWeblabel4,
                                    vcWebLink4,
                                    numAnnualRevID,
                                    numNoOfEmployeesId,
                                    vcHow,
                                    vcProfile,
                                    bitPublicFlag,
                                    numDomainID
                            FROM    CompanyInfo
                            WHERE   numCompanyID = @numCompanyID                            
                    SET @numNewCompanyID = @@identity                            
                                     
                    INSERT  INTO DivisionMaster
                            (
                              numCompanyID,
                              vcDivisionName,
                              numGrpId,
                              bitPublicFlag,
                              tintCRMType,
                              numStatusID,
                              tintBillingTerms,
                              numBillingDays,
                              tintInterestType,
                              fltInterest,
                              vcComPhone,
                              vcComFax,
                              numDomainID
                            )
                            SELECT  @numNewCompanyID,
                                    vcDivisionName,
                                    numGrpId,
                                    bitPublicFlag,
                                    2,
                                    numStatusID,
                                    tintBillingTerms,
                                    numBillingDays,
                                    tintInterestType,
                                    fltInterest,
                                    vcComPhone,
                                    vcComFax,
                                    numDomainID
                            FROM    DivisionMaster
                            WHERE   numDivisionID = @numDivisionID                     
                    SET @numNewDivisionID = @@identity     
                                            
                            INSERT INTO dbo.AddressDetails (
								vcAddressName,
								vcStreet,
								vcCity,
								vcPostalCode,
								numState,
								numCountry,
								bitIsPrimary,
								tintAddressOf,
								tintAddressType,
								numRecordID,
								numDomainID
							) SELECT 
									 vcAddressName,
									 vcStreet,
									 vcCity,
									 vcPostalCode,
									 numState,
									 numCountry,
									 bitIsPrimary,
									 tintAddressOf,
									 tintAddressType,
									 @numNewDivisionID,
									 numDomainID FROM dbo.AddressDetails WHERE numRecordID= @numDivisionID AND tintAddressOf=2
                                
                                    
                    INSERT  INTO AdditionalContactsInformation
                            (
                              vcDepartment,
                              vcCategory,
                              vcGivenName,
                              vcFirstName,
                              vcLastName,
                              numDivisionId,
                              numContactType,
                              numTeam,
                              numPhone,
                              numPhoneExtension,
                              numCell,
                              numHomePhone,
                              vcFax,
                              vcEmail,
                              VcAsstFirstName,
                              vcAsstLastName,
                              numAsstPhone,
                              numAsstExtn,
                              vcAsstEmail,
                              charSex,
                              bintDOB,
                              vcPosition,
                              numEmpStatus,
                              vcTitle,
                              vcAltEmail,
                              numDomainID,
                              bitPrimaryContact
                            )
                            SELECT  vcDepartment,
                                    vcCategory,
                                    vcGivenName,
                                    vcFirstName,
                                    vcLastName,
                                    @numNewDivisionID,
                                    numContactType,
                                    numTeam,
                                    numPhone,
                                    numPhoneExtension,
                                    numCell,
                                    numHomePhone,
                                    vcFax,
                                    vcEmail,
                                    VcAsstFirstName,
                                    vcAsstLastName,
                                    numAsstPhone,
                                    numAsstExtn,
                                    vcAsstEmail,
                                    charSex,
                                    bintDOB,
                                    vcPosition,
                                    numEmpStatus,
                                    vcTitle,
                                    vcAltEmail,
                                    numDomainID,
                                    1
                            FROM    AdditionalContactsInformation
                            WHERE   numContactID = @numAdminContactID                                       
                    SET @numNewContactID = @@identity                             
                             
                    UPDATE  CompanyInfo
                    SET     numCreatedBy = @numNewContactID,
                            bintCreatedDate = GETUTCDATE(),
                            numModifiedBy = @numNewContactID,
                            bintModifiedDate = GETUTCDATE(),
                            numDomainID = @numTargetDomainID
                    WHERE   numCompanyID = @numNewCompanyID                            
                             
                    UPDATE  DivisionMaster
                    SET     numCompanyID = @numNewCompanyID,
                            numCreatedBy = @numNewContactID,
                            bintCreatedDate = GETUTCDATE(),
                            numModifiedBy = @numNewContactID,
                            bintModifiedDate = GETUTCDATE(),
                            numDomainID = @numTargetDomainID,
                            tintCRMType = 2,
                            numRecOwner = @numNewContactID
                    WHERE   numDivisionID = @numNewDivisionID                            
                            
                            
                    UPDATE  AdditionalContactsInformation
                    SET     numDivisionId = @numNewDivisionID,
                            numCreatedBy = @numNewContactID,
                            bintCreatedDate = GETUTCDATE(),
                            numModifiedBy = @numNewContactID,
                            bintModifiedDate = GETUTCDATE(),
                            numDomainID = @numTargetDomainID,
                            numRecOwner = @numNewContactID
                    WHERE   numContactId = @numNewContactID                            
                                
                    DECLARE @vcFirstname AS VARCHAR(50)                            
                    DECLARE @vcEmail AS VARCHAR(100)                            
                    SELECT  @vcFirstname = vcFirstName,
                            @vcEmail = vcEmail
                    FROM    AdditionalContactsInformation
                    WHERE   numContactID = @numNewContactID                            
                    EXEC Resource_Add @vcFirstname, '', @vcEmail, 1,
                        @numTargetDomainID, @numNewContactID   
                        
     -- Added by sandeep to add required fields for New Item form field management in Admnistration section
     
     EXEC USP_ManageNewItemRequiredFields @numDomainID = @numTargetDomainID
     
     --Add Default Subtabs and assign permission to all roles by default --added by chintan

                    EXEC USP_ManageTabsInCuSFields @byteMode = 6, @LocID = 0,
                        @TabName = '', @TabID = 0,
                        @numDomainID = @numTargetDomainID, @vcURL = ''
                        
                    INSERT  INTO RoutingLeads
                            (
                              numDomainId,
                              vcRoutName,
                              tintEqualTo,
                              numValue,
                              numCreatedBy,
                              dtCreatedDate,
                              numModifiedBy,
                              dtModifiedDate,
                              bitDefault,
                              tintPriority
                            )
                    VALUES  (
                              @numTargetDomainID,
                              'Default',
                              0,
                              @numNewContactID,
                              @numNewContactID,
                              GETUTCDATE(),
                              @numNewContactID,
                              GETUTCDATE(),
                              1,
                              0
                            )                          
                    INSERT  INTO RoutingLeadDetails
                            (
                              numRoutID,
                              numEmpId,
                              intAssignOrder
                            )
                    VALUES  (
                              @@identity,
                              @numNewContactID,
                              1
                            )                  
                      
                    IF ( SELECT COUNT(*)
                         FROM   AuthenticationGroupMaster
                         WHERE  numDomainId = @numTargetDomainID
                                AND vcGroupName = 'System Administrator'
                       ) = 0 
                        BEGIN                       
                         
                            DECLARE @numGroupId1 AS NUMERIC(9)                      
                            INSERT  INTO AuthenticationGroupMaster
                                    (
                                      vcGroupName,
                                      numDomainID,
                                      tintGroupType,
                                      bitConsFlag 
                                    )
                            VALUES  (
                                      'System Administrator',
                                      @numTargetDomainID,
                                      1,
                                      0
                                    )                      
                            SET @numGroupId1 = @@identity                        
                            SET @TargetGroupId = @numGroupId1                     
                       
INSERT  INTO GroupAuthorization(numGroupID,numModuleID,numPageID,intExportAllowed,intPrintAllowed,intViewAllowed,
                                      intAddAllowed,intUpdateAllowed,intDeleteAllowed,numDomainID)
select @numGroupId1,numModuleID,numPageID,bitISExportApplicable,0,bitIsViewApplicable,bitIsAddApplicable,bitISUpdateApplicable,bitISDeleteApplicable,@numTargetDomainID
  from PageMaster
--                            INSERT  INTO GroupAuthorization
--                                    (
--                                      numGroupID,
--                                      numModuleID,
--                                      numPageID,
--                                      intExportAllowed,
--                                      intPrintAllowed,
--                                      intViewAllowed,
--                                      intAddAllowed,
--                                      intUpdateAllowed,
--                                      intDeleteAllowed                      
--                       
--                                    )
--                                    SELECT  @numGroupId1,
--                                            x.numModuleID,
--                                            x.numPageID,
--                                            x.intExportAllowed,
--                                            x.intPrintAllowed,
--                                            x.intViewAllowed,
--                                            x.intAddAllowed,
--                                            x.intUpdateAllowed,
--                                            x.intDeleteAllowed
--                                    FROM    ( SELECT    *
--                                              FROM      GroupAuthorization
--                                              WHERE     numGroupid = 1
--                                            ) x            
        
--MainTab From TabMaster 
                            INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId1,
                                            x.numTabId,
                                            0,
                                            1,
                                            x.numOrder,0
                                    FROM    ( SELECT    *,Row_Number() over(Order by numtabid) as numOrder
                                              FROM      tabmaster
                                              WHERE     tintTabtype=1 and bitFixed=1
                                            ) x 

--default Sub tab From cfw_grp_master
INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId1,
                                            x.Grp_id,
                                            0,
                                            1,
                                            x.numOrder,1
                                    FROM    ( SELECT    *,Row_Number() over(Order by Grp_id) as numOrder
                                              FROM      cfw_grp_master
                                              WHERE     tintType=2 AND numDomainID = @numTargetDomainID
                                            ) x 


--Added by Anoop for Dashboard +++++++++++++++++++

                            IF ( SELECT COUNT(*)
                                 FROM   customreport
                                 WHERE  numDomainId = @numTargetDomainID
                               ) = 0 
                                BEGIN                  
                                    EXEC CreateCustomReportsForNewDomain @numTargetDomainID,
                                        @numNewContactID              
                                END 

                            DECLARE @numGroupId2 AS NUMERIC(9)                      
                            INSERT  INTO AuthenticationGroupMaster
                                    (
                                      vcGroupName,
                                      numDomainID,
                                      tintGroupType,
                                      bitConsFlag 
                                    )
                            VALUES  (
                                      'Executive',
                                      @numTargetDomainID,
                                      1,
                                      0
                                    )                      
                            SET @numGroupId2 = @@identity 

INSERT  INTO GroupAuthorization(numGroupID,numModuleID,numPageID,intExportAllowed,intPrintAllowed,intViewAllowed,
                                      intAddAllowed,intUpdateAllowed,intDeleteAllowed,numDomainID)
select @numGroupId2,numModuleID,numPageID,bitISExportApplicable,0,bitIsViewApplicable,bitIsAddApplicable,bitISUpdateApplicable,bitISDeleteApplicable,@numTargetDomainID
  from PageMaster

--                            INSERT  INTO GroupAuthorization
--                                    (
--                                      numGroupID,
--                                      numModuleID,
--                                      numPageID,
--                                      intExportAllowed,
--                                      intPrintAllowed,
--                                      intViewAllowed,
--                                      intAddAllowed,
--                                      intUpdateAllowed,
--                                      intDeleteAllowed                      
--                       
--                                    )
--                                    SELECT  @numGroupId2,
--                                            x.numModuleID,
--                                            x.numPageID,
--                                            x.intExportAllowed,
--                                            x.intPrintAllowed,
--                                            x.intViewAllowed,
--                                            x.intAddAllowed,
--                                            x.intUpdateAllowed,
--                                            x.intDeleteAllowed
--                                    FROM    ( SELECT    *
--                                              FROM      GroupAuthorization
--                                              WHERE     numGroupid = 1
--                                            ) x            
        
                
--MainTab From TabMaster 
                            INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.numTabId,
                                            0,
                                            1,
                                            x.numOrder,0
                                    FROM    ( SELECT    *,Row_Number() over(Order by numtabid) as numOrder
                                              FROM      tabmaster
                                              WHERE     tintTabtype=1 and bitFixed=1
                                            ) x 

--default Sub tab From cfw_grp_master
INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.Grp_id,
                                            0,
                                            1,
                                            x.numOrder,1
                                    FROM    ( SELECT    *,Row_Number() over(Order by Grp_id) as numOrder
                                              FROM      cfw_grp_master
                                              WHERE     tintType=2 AND numDomainID = @numTargetDomainID
                                            ) x 

                            INSERT  INTO DashBoardSize
                                    (
                                      tintColumn,
                                      tintSize,
                                      numGroupUserCntID,
                                      bitGroup
                                    )
                                    SELECT  tintColumn,
                                            tintSize,
                                            @numGroupId2,
                                            1
                                    FROM    DashBoardSize
                                    WHERE   bitGroup = 1
                                            AND numGroupUserCntID = 258
                                    ORDER BY tintColumn 


                            INSERT  INTO Dashboard
                                    (
                                      numReportID,
                                      numGroupUserCntID,
                                      tintRow,
                                      tintColumn,
                                      tintReportType,
                                      vcHeader,
                                      vcFooter,
                                      tintChartType,
                                      bitGroup
                                    )
                                    SELECT  numReportID,
                                            @numGroupId2,
                                            tintRow,
                                            tintColumn,
                                            tintReportType,
                                            vcHeader,
                                            vcFooter,
                                            tintChartType,
                                            1
                                    FROM    Dashboard
                                    WHERE   bitGroup = 1
                                            AND numGroupUserCntID = 258
                                    ORDER BY tintColumn,
                                            tintRow

                            INSERT  INTO DashboardAllowedReports
                                    SELECT  numCustomReportID,
                                            @numGroupId2
                                    FROM    CustomReport
                                    WHERE   numDomainID = @numTargetDomainID 





                            INSERT  INTO AuthenticationGroupMaster
                                    (
                                      vcGroupName,
                                      numDomainID,
                                      tintGroupType,
                                      bitConsFlag 
                                    )
                            VALUES  (
                                      'Sales Staff',
                                      @numTargetDomainID,
                                      1,
                                      0
                                    )                      
                            SET @numGroupId2 = @@identity 

INSERT  INTO GroupAuthorization(numGroupID,numModuleID,numPageID,intExportAllowed,intPrintAllowed,intViewAllowed,
                                      intAddAllowed,intUpdateAllowed,intDeleteAllowed,numDomainID)
select @numGroupId2,numModuleID,numPageID,bitISExportApplicable,0,bitIsViewApplicable,bitIsAddApplicable,bitISUpdateApplicable,bitISDeleteApplicable,@numTargetDomainID
  from PageMaster

--                            INSERT  INTO GroupAuthorization
--                                    (
--                                      numGroupID,
--                                      numModuleID,
--                                      numPageID,
--                                      intExportAllowed,
--                                      intPrintAllowed,
--                                      intViewAllowed,
--                                      intAddAllowed,
--                                      intUpdateAllowed,
--                                      intDeleteAllowed                      
--                       
--                                    )
--                                    SELECT  @numGroupId2,
--                                            x.numModuleID,
--                                            x.numPageID,
--                                            x.intExportAllowed,
--                                            x.intPrintAllowed,
--                                            x.intViewAllowed,
--                                            x.intAddAllowed,
--                                            x.intUpdateAllowed,
--                                            x.intDeleteAllowed
--                                    FROM    ( SELECT    *
--                                              FROM      GroupAuthorization
--                                              WHERE     numGroupid = 1
--                                            ) x            
        
--MainTab From TabMaster 
                            INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.numTabId,
                                            0,
                                            1,
                                            x.numOrder,0
                                    FROM    ( SELECT    *,Row_Number() over(Order by numtabid) as numOrder
                                              FROM      tabmaster
                                              WHERE     tintTabtype=1 and bitFixed=1
                                            ) x 

--default Sub tab From cfw_grp_master
INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.Grp_id,
                                            0,
                                            1,
                                            x.numOrder,1
                                    FROM    ( SELECT    *,Row_Number() over(Order by Grp_id) as numOrder
                                              FROM      cfw_grp_master
                                              WHERE     tintType=2 AND numDomainID = @numTargetDomainID
                                            ) x 

                           

                            INSERT  INTO DashBoardSize
                                    (
                                      tintColumn,
                                      tintSize,
                                      numGroupUserCntID,
                                      bitGroup
                                    )
                                    SELECT  tintColumn,
                                            tintSize,
                                            @numGroupId2,
                                            1
                                    FROM    DashBoardSize
                                    WHERE   bitGroup = 1
                                            AND numGroupUserCntID = 259
                                    ORDER BY tintColumn 


                            INSERT  INTO Dashboard
                                    (
                                      numReportID,
                                      numGroupUserCntID,
                                      tintRow,
                                      tintColumn,
                                      tintReportType,
                                      vcHeader,
                                      vcFooter,
                                      tintChartType,
                                      bitGroup
                                    )
                                    SELECT  numReportID,
                                            @numGroupId2,
                                            tintRow,
                                            tintColumn,
                                            tintReportType,
                                            vcHeader,
                                            vcFooter,
                                            tintChartType,
                                            1
                                    FROM    Dashboard
                                    WHERE   bitGroup = 1
                                            AND numGroupUserCntID = 259
                                    ORDER BY tintColumn,
                                            tintRow

                            INSERT  INTO DashboardAllowedReports
                                    SELECT  numCustomReportID,
                                            @numGroupId2
                                    FROM    CustomReport
                                    WHERE   numDomainID = @numTargetDomainID 






                            INSERT  INTO AuthenticationGroupMaster
                                    (
                                      vcGroupName,
                                      numDomainID,
                                      tintGroupType,
                                      bitConsFlag 
                                    )
                            VALUES  (
                                      'Warehouse Staff',
                                      @numTargetDomainID,
                                      1,
                                      0
                                    )                      
                            SET @numGroupId2 = @@identity 

INSERT  INTO GroupAuthorization(numGroupID,numModuleID,numPageID,intExportAllowed,intPrintAllowed,intViewAllowed,
                                      intAddAllowed,intUpdateAllowed,intDeleteAllowed,numDomainID)
select @numGroupId2,numModuleID,numPageID,bitISExportApplicable,0,bitIsViewApplicable,bitIsAddApplicable,bitISUpdateApplicable,bitISDeleteApplicable,@numTargetDomainID
  from PageMaster

--                            INSERT  INTO GroupAuthorization
--                                    (
--                                      numGroupID,
--                                      numModuleID,
--                                      numPageID,
--                                      intExportAllowed,
--                                      intPrintAllowed,
--                                      intViewAllowed,
--                                      intAddAllowed,
--                                      intUpdateAllowed,
--                                      intDeleteAllowed                      
--                       
--                                    )
--                                    SELECT  @numGroupId2,
--                                            x.numModuleID,
--                                            x.numPageID,
--                                            x.intExportAllowed,
--                                            x.intPrintAllowed,
--                                            x.intViewAllowed,
--                                            x.intAddAllowed,
--                                            x.intUpdateAllowed,
--                                            x.intDeleteAllowed
--                                    FROM    ( SELECT    *
--                                              FROM      GroupAuthorization
--                                              WHERE     numGroupid = 1
--                                            ) x            
        
                           
--MainTab From TabMaster 
                            INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.numTabId,
                                            0,
                                            1,
                                            x.numOrder,0
                                    FROM    ( SELECT    *,Row_Number() over(Order by numtabid) as numOrder
                                              FROM      tabmaster
                                              WHERE     tintTabtype=1 and bitFixed=1
                                            ) x 

--default Sub tab From cfw_grp_master
INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.Grp_id,
                                            0,
                                            1,
                                            x.numOrder,1
                                    FROM    ( SELECT    *,Row_Number() over(Order by Grp_id) as numOrder
                                              FROM      cfw_grp_master
                                              WHERE     tintType=2 AND numDomainID = @numTargetDomainID
                                            ) x 

                           


                            INSERT  INTO DashBoardSize
                                    (
                                      tintColumn,
                                      tintSize,
                                      numGroupUserCntID,
                                      bitGroup
                                    )
                                    SELECT  tintColumn,
                                            tintSize,
                                            @numGroupId2,
                                            1
                                    FROM    DashBoardSize
                                    WHERE   bitGroup = 1
                                            AND numGroupUserCntID = 260
                                    ORDER BY tintColumn 


                            INSERT  INTO Dashboard
                                    (
                                      numReportID,
                                      numGroupUserCntID,
                                      tintRow,
                                      tintColumn,
                                      tintReportType,
                                      vcHeader,
                                      vcFooter,
                                      tintChartType,
                                      bitGroup
                                    )
                                    SELECT  numReportID,
                                            @numGroupId2,
                                            tintRow,
                                            tintColumn,
                                            tintReportType,
                                            vcHeader,
                                            vcFooter,
                                            tintChartType,
                                            1
                                    FROM    Dashboard
                                    WHERE   bitGroup = 1
                                            AND numGroupUserCntID = 260
                                    ORDER BY tintColumn,
                                            tintRow

                            INSERT  INTO DashboardAllowedReports
                                    SELECT  numCustomReportID,
                                            @numGroupId2
                                    FROM    CustomReport
                                    WHERE   numDomainID = @numTargetDomainID 


                            UPDATE  Table2
                            SET     Table2.numReportID = Table1.numOrgReportID
                            FROM    dashboard Table2
                                    JOIN ( SELECT   numReportID,
                                                    ( SELECT TOP 1
                                                                C1.numCustomReportID
                                                      FROM      Customreport C1
                                                                JOIN Customreport C2 ON C1.vcReportName = C2.vcReportName
                                                      WHERE     C1.numDomainID = @numTargetDomainID
                                                                AND C2.numCustomReportID = Dashboard.numReportID
                                                                AND C2.numDomainID = 1
                                                    ) numOrgReportID
                                           FROM     Dashboard
                                           WHERE    bitGroup = 1
                                                    AND numGroupUserCntID = 258
                                         ) Table1 ON Table2.numReportID = Table1.numReportID
                            WHERE   bitGroup = 1
                                    AND numGroupUserCntID = ( SELECT TOP 1
                                                                        numGroupID
                                                              FROM      AuthenticationGroupMaster
                                                              WHERE     numDomainID = @numTargetDomainID
                                                                        AND vcGroupName = 'Executive'
                                                            )

                            UPDATE  Table2
                            SET     Table2.numReportID = Table1.numOrgReportID
                            FROM    dashboard Table2
                                    JOIN ( SELECT   numReportID,
                                                    ( SELECT TOP 1
                                                                C1.numCustomReportID
                                                      FROM      Customreport C1
                                                                JOIN Customreport C2 ON C1.vcReportName = C2.vcReportName
                                                      WHERE     C1.numDomainID = @numTargetDomainID
                                                                AND C2.numCustomReportID = Dashboard.numReportID
                                                                AND C2.numDomainID = 1
                                                    ) numOrgReportID
                                           FROM     Dashboard
                                           WHERE    bitGroup = 1
                                                    AND numGroupUserCntID = 259
                                         ) Table1 ON Table2.numReportID = Table1.numReportID
                            WHERE   bitGroup = 1
                                    AND numGroupUserCntID = ( SELECT TOP 1
                                                                        numGroupID
                                                              FROM      AuthenticationGroupMaster
                                                              WHERE     numDomainID = @numTargetDomainID
                                                                        AND vcGroupName = 'Sales Staff'
                                                            )


                            UPDATE  Table2
                            SET     Table2.numReportID = Table1.numOrgReportID
                            FROM    dashboard Table2
                                    JOIN ( SELECT   numReportID,
                                                    ( SELECT TOP 1
                                                                C1.numCustomReportID
                                                      FROM      Customreport C1
                                                                JOIN Customreport C2 ON C1.vcReportName = C2.vcReportName
                                                      WHERE     C1.numDomainID = @numTargetDomainID
                                                                AND C2.numCustomReportID = Dashboard.numReportID
                                                                AND C2.numDomainID = 1
                                                    ) numOrgReportID
                                           FROM     Dashboard
                                           WHERE    bitGroup = 1
                                                    AND numGroupUserCntID = 260
                                         ) Table1 ON Table2.numReportID = Table1.numReportID
                            WHERE   bitGroup = 1
                                    AND numGroupUserCntID = ( SELECT TOP 1
                                                                        numGroupID
                                                              FROM      AuthenticationGroupMaster
                                                              WHERE     numDomainID = @numTargetDomainID
                                                                        AND vcGroupName = 'Warehouse Staff'
                                                            )



------ Added by Anoop for Dashboard-----------       
                        END                      
                    IF ( SELECT COUNT(*)
                         FROM   AuthenticationGroupMaster
                         WHERE  numDomainId = @numTargetDomainID
                                AND vcGroupName = 'Default Extranet'
                       ) = 0 
                        BEGIN                       
                                             
                            INSERT  INTO AuthenticationGroupMaster
                                    (
                                      vcGroupName,
                                      numDomainID,
                                      tintGroupType,
                                      bitConsFlag 
                                    )
                            VALUES  (
                                      'Default Extranet',
                                      @numTargetDomainID,
                                      2,
                                      0
                                    )                      
                            SET @numGroupId2 = @@identity                        
                      
                       
                            INSERT  INTO GroupAuthorization
                                    (
                                      numGroupID,
                                      numModuleID,
                                      numPageID,
                                      intExportAllowed,
                                      intPrintAllowed,
                                      intViewAllowed,
                                      intAddAllowed,
                                      intUpdateAllowed,
                                      intDeleteAllowed,
                                      numDomainID                      
                       
                                    )
                                    SELECT  @numGroupId2,
                                            x.numModuleID,
                                            x.numPageID,
                                            x.intExportAllowed,
                                            x.intPrintAllowed,
                                            x.intViewAllowed,
                                            x.intAddAllowed,
                                            x.intUpdateAllowed,
                                            x.intDeleteAllowed,
                                            @numTargetDomainID
                                    FROM    ( SELECT    *
                                              FROM      GroupAuthorization
                                              WHERE     numGroupid = 2
                                            ) x           
        
                          
--MainTab From TabMaster for Customer:46
                            INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.numTabId,
                                            46,
                                            1,
                                            x.numOrder,0
                                    FROM    ( SELECT    *,Row_Number() over(Order by numtabid) as numOrder
                                              FROM      tabmaster
                                              WHERE     tintTabtype=2 and bitFixed=1
                                            ) x 

--default Sub tab From cfw_grp_master for Customer:46
INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.Grp_id,
                                            46,
                                            1,
                                            x.numOrder,1
                                    FROM    ( SELECT    *,Row_Number() over(Order by Grp_id) as numOrder
                                              FROM      cfw_grp_master
                                              WHERE     tintType=2 AND numDomainID = @numTargetDomainID
                                            ) x 


                          
--MainTab From TabMaster  for Employer:93
                            INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.numTabId,
                                           93,
                                            1,
                                            x.numOrder,0
                                    FROM    ( SELECT    *,Row_Number() over(Order by numtabid) as numOrder
                                              FROM      tabmaster
                                              WHERE     tintTabtype=2 and bitFixed=1
                                            ) x 

--default Sub tab From cfw_grp_master for Employer:93
INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.Grp_id,
                                            93,
                                            1,
                                            x.numOrder,1
                                    FROM    ( SELECT    *,Row_Number() over(Order by Grp_id) as numOrder
                                              FROM      cfw_grp_master
                                              WHERE     tintType=2 AND numDomainID = @numTargetDomainID
                                            ) x 
      
                   
                        END                      
----                    IF ( SELECT COUNT(*)
----                         FROM   AuthenticationGroupMaster
----                         WHERE  numDomainId = @numTargetDomainID
----                                AND vcGroupName = 'Default PRM'
----                       ) = 0 
----                        BEGIN                       
----                         
----                            DECLARE @numGroupId3 AS NUMERIC(9)                      
----                            INSERT  INTO AuthenticationGroupMaster
----                                    (
----                                      vcGroupName,
----                                      numDomainID,
----                                      tintGroupType,
----                                      bitConsFlag 
----                                    )
----                            VALUES  (
----                                      'Default PRM',
----                                      @numTargetDomainID,
----                                      3,
----                                      0
----                                    )                      
----                            SET @numGroupId3 = @@identity                        
----                      
----                       
----                            INSERT  INTO GroupAuthorization
----                                    (
----                                      numGroupID,
----                                      numModuleID,
----                                      numPageID,
----                                      intExportAllowed,
----                                      intPrintAllowed,
----                                      intViewAllowed,
----                                      intAddAllowed,
----                                      intUpdateAllowed,
----                                      intDeleteAllowed,
----                                      numDomainID                   
----                   
----                                    )
----                                    SELECT  @numGroupId3,
----                                            x.numModuleID,
----                                            x.numPageID,
----                                            x.intExportAllowed,
----                                            x.intPrintAllowed,
----                                            x.intViewAllowed,
----                                            x.intAddAllowed,
----                                            x.intUpdateAllowed,
----                                            x.intDeleteAllowed,
----                                            @numTargetDomainID
----                                    FROM    ( SELECT    *
----                                              FROM      GroupAuthorization
----                                              WHERE     numGroupid = 46
----                                            ) x             
----        
------                            INSERT  INTO GroupTabDetails
------                                    (
------                                      numGroupId,
------                                      numTabId,
------                                      numRelationShip,
------                                      bitallowed,
------                                      numOrder
------                                    )
------                                    SELECT  @numGroupId3,
------                                            x.numTabId,
------                                            x.numRelationShip,
------                                            x.bitallowed,
------                                            x.numOrder
------                                    FROM    ( SELECT    *
------                                              FROM      GroupTabDetails
------                                              WHERE     numGroupid = 46
------                                            ) x         
----                 
----                        END                      
                    
       

      
--- Set default Chart of Accounts 
                    EXEC USP_ChartAcntDefaultValues @numDomainId = @numTargetDomainID,
                        @numUserCntId = @numNewContactID      
    
    
-- Dashboard Size    
                    INSERT  INTO DashBoardSize
                            SELECT  1,
                                    1,
                                    @numGroupId1,
                                    1
                            UNION
                            SELECT  2,
                                    1,
                                    @numGroupId1,
                                    1
                            UNION
                            SELECT  3,
                                    2,
                                    @numGroupId1,
                                    1     
    
----inserting all the Custome Reports for the newly created group    
    
                    INSERT  INTO DashboardAllowedReports
                            SELECT  numCustomReportID,
                                    @numGroupId1
                            FROM    CustomReport
                            WHERE   numDomainID = @numTargetDomainID    
    
    
           
 --inserting Default BizDocs for new domain
INSERT INTO dbo.AuthoritativeBizDocs
        ( numAuthoritativePurchase ,
          numAuthoritativeSales ,
          numDomainId
        )
SELECT  
ISNULL((SELECT numListItemID  FROM dbo.ListDetails WHERE numListID=27 AND constFlag =1 AND vcData = 'bill'),0),
ISNULL((SELECT numListItemID  FROM dbo.ListDetails WHERE numListID=27 AND constFlag =1 AND vcData = 'invoice'),0),
@numTargetDomainID


IF NOT EXISTS(SELECT * FROM PortalBizDocs WHERE numDomainID=@numTargetDomainID)
BEGIN
	--delete from PortalBizDocs WHERE numDomainID=176
	INSERT INTO dbo.PortalBizDocs
        ( numBizDocID, numDomainID )
	SELECT numListItemID,@numTargetDomainID FROM dbo.ListDetails WHERE numListID=27 AND constFlag=1
END



---Set BizDoc Type Filter for Sales and Purchase BizDoc Type
INSERT INTO dbo.BizDocFilter( numBizDoc ,tintBizocType ,numDomainID)
SELECT numListItemID,1,@numTargetDomainID FROM dbo.ListDetails WHERE numListID=27 AND constFlag=1 AND vcData IN ('Invoice','Sales Opportunity','Sales Order','Sales Credit Memo','Fulfillment Order','Credit Memo','Refund Receipt','RMA','Packing Slip','Pick List')

INSERT INTO dbo.BizDocFilter( numBizDoc ,tintBizocType ,numDomainID)
SELECT numListItemID,2,@numTargetDomainID FROM dbo.ListDetails WHERE numListID=27 AND constFlag=1 AND vcData IN ('Purchase Opportunity','Bill','Purchase Order','Purchase Credit Memo','RMA')



--- Give permission of all tree node to all Groups
exec USP_CreateTreeNavigationForDomain @numTargetDomainID,0

--Create Default BizDoc template for all system bizdocs, Css and template html will be updated though same SP from code.
EXEC dbo.USP_CreateBizDocTemplateByDefault @numDomainID = @numTargetDomainID, -- numeric
    @txtBizDocTemplate = '', -- text
    @txtCss = '', -- text
    @tintMode = 2,
    @txtPackingSlipBizDocTemplate = ''
 

          
 --inserting the details of BizForms Wizard          
                    INSERT  INTO DycFormConfigurationDetails(numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,numAuthGroupID
								,numDomainId,numSurId)
                            SELECT  numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,@numGroupId1,
									@numTargetDomainID,0
                            FROM    DycFormConfigurationDetails
                            WHERE   numDomainID = 0
                                    AND numFormID IN ( 1, 2, 6 )          
          
                    INSERT  INTO DycFormConfigurationDetails(numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,numAuthGroupID
								,numDomainId,numSurId)
                            SELECT  numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,0,
									@numTargetDomainID,0
                            FROM    DycFormConfigurationDetails
                            WHERE   numDomainID = 0
                                    AND numFormID IN ( 3, 4, 5 )          
          
                    INSERT  INTO DycFormConfigurationDetails(numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,numAuthGroupID
								,numDomainId,numSurId)
                            SELECT  numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,numAuthGroupID,
									@numTargetDomainID,0
                            FROM    DycFormConfigurationDetails
                            WHERE   numDomainID = 0
                                    AND numFormID IN ( 7, 8 )          
          
          
          
          ---Insert Default Add Relationship Field Lead/Prospect/Account and Other
          INSERT INTO DycFormConfigurationDetails(numFormId,numFieldId,intColumnNum,intRowNum,
								numDomainId,numUserCntID,numRelCntType,tintPageType,bitCustom)
               SELECT  numFormId,numFieldId,intColumnNum,intRowNum,@numTargetDomainID,0,
								numRelCntType,tintPageType,bitCustom
                     FROM DycFormConfigurationDetails
                     WHERE numDomainID = 0 AND numFormID IN (34,35,36) AND tintPageType=2
          
          ---Set Default Validation for Lead/Prospect/Account
         INSERT INTO DynamicFormField_Validation(numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,
   bitIsRequired,bitIsNumeric,bitIsAlphaNumeric,bitIsEmail,bitIsLengthValidation,intMaxLength,intMinLength,
   bitFieldMessage,vcFieldMessage)
    SELECT numFormId,numFormFieldId,@numTargetDomainID,vcNewFormFieldName,
   bitIsRequired,bitIsNumeric,bitIsAlphaNumeric,bitIsEmail,bitIsLengthValidation,intMaxLength,intMinLength,
   bitFieldMessage,vcFieldMessage
                     FROM DynamicFormField_Validation
                     WHERE numDomainID = 0 AND numFormID IN (34,35,36)
                              
                    
                    DECLARE @numListItemID AS NUMERIC(9)                    
                    DECLARE @numNewListItemID AS NUMERIC(9)                    
                    SELECT TOP 1
                            @numListItemID = numListItemID
                    FROM    ListDetails
                    WHERE   numDomainID = 1
                            AND numListID = 40                    
                    
                    WHILE @numListItemID > 0                    
                        BEGIN                    
                    
                            INSERT  INTO ListDetails
                                    (
                                      numListID,
                                      vcData,
                                      numCreatedBY,
                                      bintCreatedDate,
                                      numModifiedBy,
                                      bintModifiedDate,
                                      bitDelete,
                                      numDomainID,
                                      constFlag,
                                      sintOrder
                                    )
                                    SELECT  numListID,
                                            vcData,
                                            @numNewContactID,
                                            GETUTCDATE(),
                                            @numNewContactID,
                                            GETUTCDATE(),
                                            bitDelete,
                                            @numTargetDomainID,
                                            constFlag,
                                            sintOrder
                                    FROM    ListDetails
                                    WHERE   numListItemID = @numListItemID                    
                            SELECT  @numNewListItemID = @@identity                    
                     
                            INSERT  INTO [state]
                                    (
                                      numCountryID,
                                      vcState,
                                      numCreatedBy,
                                      bintCreatedDate,
                                      numModifiedBy,
                                      bintModifiedDate,
                                      numDomainID,
                                      constFlag,
									  numShippingZone
                                    )
                                    SELECT  @numNewListItemID,
                                            vcState,
                                            @numNewContactID,
                                            GETUTCDATE(),
                                            @numNewContactID,
                                            GETUTCDATE(),
                                            @numTargetDomainID,
                                            constFlag,
											numShippingZone
                                    FROM    [State]
                                    WHERE   numCountryID = @numListItemID                    
                            SELECT TOP 1
                                    @numListItemID = numListItemID
                            FROM    ListDetails
                            WHERE   numDomainID = 1
                                    AND numListID = 40
                                    AND numListItemID > @numListItemID                    
                            IF @@rowcount = 0 
                                SET @numListItemID = 0                    
                        END                    
                    
                    --INsert Net Terms
                   	INSERT INTO [ListDetails] ([numListID],[vcData],[numCreatedBY],[bitDelete],[numDomainID],[constFlag],[sintOrder])
								  SELECT 296,'0',1,0,@numTargetDomainID,0,1
						UNION ALL SELECT 296,'7',1,0,@numTargetDomainID,0,2
						UNION ALL SELECT 296,'15',1,0,@numTargetDomainID,0,3
						UNION ALL SELECT 296,'30',1,0,@numTargetDomainID,0,4
						UNION ALL SELECT 296,'45',1,0,@numTargetDomainID,0,5
						UNION ALL SELECT 296,'60',1,0,@numTargetDomainID,0,6
                    --Insert Default email templates
                    INSERT INTO dbo.GenericDocuments (
						VcFileName,
						vcDocName,
						numDocCategory,
						cUrlType,
						vcFileType,
						numDocStatus,
						vcDocDesc,
						numDomainID,
						numCreatedBy,
						bintCreatedDate,
						numModifiedBy,
						bintModifiedDate,
						vcSubject,
						vcDocumentSection,
						numRecID,
						tintCheckOutStatus,
						intDocumentVersion,
						numLastCheckedOutBy,
						dtCheckOutDate,
						dtCheckInDate,
						tintDocumentType,
						numOldSpecificDocID,
						numModuleID
					)  
					SELECT  VcFileName,
							vcDocName,
							numDocCategory,
							cUrlType,
							vcFileType,
							numDocStatus,
							vcDocDesc,
							@numTargetDomainID,
							@numNewContactID,
							GETUTCDATE(),
							@numNewContactID,
							GETUTCDATE(),
							vcSubject,
							vcDocumentSection,
							numRecID,
							tintCheckOutStatus,
							intDocumentVersion,
							numLastCheckedOutBy,
							dtCheckOutDate,
							dtCheckInDate,
							tintDocumentType,
							numOldSpecificDocID,
							numModuleID
					FROM    dbo.GenericDocuments
					WHERE   numDomainID = 0
                       
                    INSERT  INTO UserMaster
                            (
                              vcUserName,
                              vcUserDesc,
                              vcMailNickName,
                              numGroupID,
                              numDomainID,
                              numCreatedBy,
                              bintCreatedDate,
                              numModifiedBy,
                              bintModifiedDate,
                              bitActivateFlag,
                              numUserDetailId,
                              vcEmailID
                            )
                            SELECT  vcFirstName,
                                    vcFirstName,
                                    vcFirstName,
                                    @numGroupId1,
                                    @numTargetDomainID,
                                    @numUserContactID,
                                    GETUTCDATE(),
                                    @numUserContactID,
                                    GETUTCDATE(),
                                    0,
                                    @numNewContactID,
                                    vcEmail
                            FROM    AdditionalContactsInformation
                            WHERE   numContactID = @numAdminContactID                                            
                          
                           INSERT INTO UOM (numDomainId,vcUnitName,tintUnitType,bitEnabled)
			                SELECT @numTargetDomainID, vcUnitName,tintUnitType,bitEnabled FROM UOM WHERE numDomainId=0    
                    
                    if not exists (select * from Currency where numDomainID=@numTargetDomainID)
						insert into Currency (vcCurrencyDesc,chrCurrency,varCurrSymbol,numDomainID,fltExchangeRate,bitEnabled)
							select vcCurrencyDesc, chrCurrency,varCurrSymbol, @numTargetDomainID, fltExchangeRate, bitEnabled from Currency where numDomainID=0
                    
                    ------- Insert Detail For Default Currency In New Domain Entry -------------------------
					DECLARE @numCountryID AS NUMERIC(18,0)
					SELECT @numCountryID = numListItemID FROM dbo.ListDetails WHERE numListID = 40 AND vcData = 'United States' AND numDomainID = @numTargetDomainID

					--IF NOT EXISTS(SELECT * FROM Currency WHERE numCountryId = @numCountryID AND numDomainID = @numTargetDomainID AND vcCurrencyDesc = 'USD-U.S. Dollar')
					IF NOT EXISTS(SELECT * FROM Currency WHERE numDomainID = @numTargetDomainID AND vcCurrencyDesc = 'USD-U.S. Dollar')
					BEGIN
					PRINT 1
						INSERT  INTO dbo.Currency(vcCurrencyDesc,chrCurrency,varCurrSymbol,numDomainID,fltExchangeRate,bitEnabled,numCountryId)
						SELECT  TOP 1 vcCurrencyDesc,chrCurrency,varCurrSymbol,@numTargetDomainID,ISNULL(fltExchangeRate,1),1 AS bitEnabled,@numCountryId FROM dbo.Currency 
						WHERE vcCurrencyDesc = 'USD-U.S. Dollar' AND numDomainID=0
						UNION 
						SELECT  vcCurrencyDesc,chrCurrency,varCurrSymbol,@numTargetDomainID,fltExchangeRate,0 AS bitEnabled,NULL FROM dbo.Currency 
						WHERE vcCurrencyDesc <> 'USD-U.S. Dollar' AND numDomainID=0
					END
					ELSE
						BEGIN
						PRINT 2
							UPDATE Currency SET numCountryId = @numCountryID, bitEnabled = 1, fltExchangeRate=1
							WHERE numDomainID = @numTargetDomainID
							AND vcCurrencyDesc = 'USD-U.S. Dollar'                        
						PRINT 3							
						END
						
					--Set Default Currency
                    DECLARE @numCurrencyID AS NUMERIC(18)
                    SET @numCurrencyID=(SELECT TOP 1 numCurrencyID FROM Currency WHERE (bitEnabled=1 or chrCurrency='USD') AND numDomainID=@numTargetDomainID)
                   
					--Set Default Country
					DECLARE @numDefCountry AS NUMERIC(18)
                    SET @numDefCountry=(SELECT TOP 1 numListItemID FROM ListDetails where numlistid=40 AND numDomainID=@numTargetDomainID and vcdata like '%United States%')		
					                    
                    UPDATE  Domain
                    SET     numDivisionID = @numNewDivisionID,
                            numAdminID = @numNewContactID,numCurrencyID=@numCurrencyID,numDefCountry=isnull(@numDefCountry,0)
                    WHERE   numdomainID = @numTargetDomainID                                      
                         	
					------------------


                         
                    INSERT  INTO Subscribers
                            (
                              numDivisionID,
                              numAdminContactID,
                              numTargetDomainID,
                              numDomainID,
                              numCreatedBy,
                              dtCreatedDate,
                              numModifiedBy,
                              dtModifiedDate,
                              dtEmailStartDate,
                              dtEmailEndDate,
							  bitEnabledAccountingAudit,
							  bitEnabledNotifyAdmin
                            )
                    VALUES  (
                              @numDivisionID,
                              @numNewContactID,
                              @numTargetDomainID,
                              @numDomainID,
                              @numUserContactID,
                              GETUTCDATE(),
                              @numUserContactID,
                              GETUTCDATE(),
                              dbo.GetUTCDateWithoutTime(),
                              dbo.GetUTCDateWithoutTime(),
							  @bitEnabledAccountingAudit,
							  @bitEnabledNotifyAdmin
                            )                                    
                    SET @numSubscriberID = @@identity                        
                      
                      
                              
                END                                
            ELSE 
                BEGIN                        
                    SELECT  @numSubscriberID = numSubscriberID
                    FROM    Subscribers
                    WHERE   numDivisionID = @numDivisionID
                            AND numDomainID = @numDomainID                                
                    UPDATE  Subscribers
                    SET     bitDeleted = 0
                    WHERE   numSubscriberID = @numSubscriberID                                
                END                                 
                                   
                     
        END                                    
    ELSE 
        BEGIN                                    
                                  
            DECLARE @bitOldStatus AS BIT                                  
            SET @bitExists = 1                                 
            SELECT  @bitOldStatus = bitActive,
                    @numTargetDomainID = numTargetDomainID
            FROM    Subscribers
            WHERE   numSubscriberID = @numSubscriberID                                    
                                  
            IF ( @bitOldStatus = 1
                 AND @bitActive = 0
               ) 
                UPDATE  Subscribers
                SET     dtSuspendedDate = GETUTCDATE()
                WHERE   numSubscriberID = @numSubscriberID                                  
                                  
            UPDATE  Subscribers
            SET     intNoofUsersSubscribed = @intNoofUsersSubscribed,
                    intNoOfPartners = @intNoOfPartners,
                    dtSubStartDate = @dtSubStartDate,
                    dtSubEndDate = @dtSubEndDate,
                    bitTrial = @bitTrial,
                    numAdminContactID = @numAdminContactID,
                    bitActive = @bitActive,
                    vcSuspendedReason = @vcSuspendedReason,
                    numModifiedBy = @numUserContactID,
                    dtModifiedDate = GETUTCDATE(),
                    intNoofPartialSubscribed = @intNoofPartialSubscribed,
                    intNoofMinimalSubscribed = @intNoofMinimalSubscribed,
					dtEmailStartDate= @dtEmailStartDate,
					dtEmailEndDate= @dtEmailEndDate,
					intNoOfEmail=@intNoOfEmail,
					bitEnabledAccountingAudit = @bitEnabledAccountingAudit,
					bitEnabledNotifyAdmin = @bitEnabledNotifyAdmin
            WHERE   numSubscriberID = @numSubscriberID                 
                       
                         UPDATE  Domain
                    SET     tintLogin = @tintLogin,
							numAdminID = @numAdminContactID
                    WHERE   numdomainID = @numTargetDomainID   
                           
            DECLARE @hDoc1 INT                                                              
            EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strUsers                                                              
                                                               
            INSERT  INTO UserMaster
                    (
                      vcUserName,
                      vcMailNickName,
                      vcUserDesc,
                      numGroupID,
                      numDomainID,
                      numCreatedBy,
                      bintCreatedDate,
                      numModifiedBy,
                      bintModifiedDate,
                      bitActivateFlag,
                      numUserDetailId,
                      vcEmailID,
                      vcPassword
                    )
                    SELECT  UserName,
                            UserName,
                            UserName,
                            0,
                            @numTargetDomainID,
                            @numUserContactID,
                            GETUTCDATE(),
                            @numUserContactID,
                            GETUTCDATE(),
                            Active,
                            numContactID,
                            Email,
                            vcPassword
                    FROM    ( SELECT    *
                              FROM      OPENXML (@hDoc1, '/NewDataSet/Users[numUserID=0]',2)
                                        WITH ( numContactID NUMERIC(9), numUserID NUMERIC(9), Email VARCHAR(100), vcPassword VARCHAR(100), UserName VARCHAR(100), Active TINYINT )
                            ) X                                
                
                              
            UPDATE  UserMaster
            SET     vcUserName = X.UserName,
                    numModifiedBy = @numUserContactID,
                    bintModifiedDate = GETUTCDATE(),
                    vcEmailID = X.Email,
                    vcPassword = X.vcPassword,
                    bitActivateFlag = Active
            FROM    ( SELECT    numUserID AS UserID,
                                numContactID,
                                Email,
                                vcPassword,
                                UserName,
                                Active
                      FROM      OPENXML (@hDoc1, '/NewDataSet/Users[numUserID>0]',2)
                                WITH ( numContactID NUMERIC(9), numUserID NUMERIC(9), Email VARCHAR(100), vcPassword VARCHAR(100), UserName VARCHAR(100), Active TINYINT )
                    ) X
            WHERE   numUserID = X.UserID                                                              
                                                               
                                                               
            EXEC sp_xml_removedocument @hDoc1                               
                                   
        END                                    
    SELECT  @numSubscriberID
GO
/****** Object:  StoredProcedure [dbo].[USP_MasterList]    Script Date: 07/26/2008 16:20:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_masterlist')
DROP PROCEDURE usp_masterlist
GO
CREATE PROCEDURE [dbo].[USP_MasterList]      
(      
 @ListId numeric(9)=null,       
 @bitDeleted as bit=null      
)      
as      
begin      
        
  SELECT  Ld.numListItemID, vcData FROM listdetails LD  
 left join listorder LO on Ld.numListItemID= LO.numListItemID 
 WHERE  Ld.numListID=@ListId  order by intSortOrder, numListItemID       
      
end
GO
/****** Object:  StoredProcedure [dbo].[USP_OPPItemDetails]    Script Date: 09/01/2009 18:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_OPPItemDetails @numItemCode=31455,@OpportunityId=12387,@numDomainId=110
--created by anooop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppitemdetails')
DROP PROCEDURE usp_oppitemdetails
GO
CREATE PROCEDURE [dbo].[USP_OPPItemDetails]
    (
      @numUserCntID NUMERIC(9) = 0,
      @OpportunityId NUMERIC(9) = 0,
      @numDomainId NUMERIC(9),
      @ClientTimeZoneOffset  INT
    )
AS 
    BEGIN
        IF @OpportunityId >0
            BEGIN
                DECLARE @DivisionID AS NUMERIC(9)
                DECLARE @TaxPercentage AS FLOAT
                DECLARE @tintOppType AS TINYINT
                DECLARE @fld_Name AS VARCHAR(500)
                DECLARE @strSQL AS VARCHAR(MAX)
                DECLARE @fld_ID AS NUMERIC(9)
                DECLARE @numBillCountry AS NUMERIC(9)
                DECLARE @numBillState AS NUMERIC(9)

                
                SELECT  @DivisionID = numDivisionID,
                        @tintOppType = tintOpptype
                FROM    OpportunityMaster
                WHERE   numOppId = @OpportunityId
               

SET @strSQL = ''

--------------Make Dynamic Query--------------
SET @strSQL = @strSQL + '
Select 
Opp.numoppitemtCode,Opp.vcNotes,WItems.numBackOrder,Opp.numCost,
ISNULL(Opp.bitItemPriceApprovalRequired,0) AS bitItemPriceApprovalRequired,
OM.tintOppType,
Opp.numItemCode,
I.charItemType,
ISNULL(I.bitAsset,0) as bitAsset,
ISNULL(I.bitRental,0) as bitRental,
Opp.numOppId,
Opp.bitWorkOrder,
Opp.fltDiscount,
ISNULL(( SELECT numReturnID FROM   Returns WHERE  numOppItemCode = Opp.numoppitemtCode AND numOppId = Opp.numOppID), 0) AS numReturnID,
dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) AS ReturrnedQty,
[dbo].fn_GetReturnStatus(Opp.numoppitemtCode) AS ReturnStatus,
ISNULL(opp.numUOMId, 0) AS numUOM,
(SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
Opp.numUnitHour AS numOriginalUnitHour, 
isnull(M.vcPOppName,''Internal'') as Source,
numSourceID,
  CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END as bitKitParent,
(CASE WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 THEN 
(CASE WHEN (I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)THEN dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,isnull(Opp.numWarehouseItmsID,0),(CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END)) ELSE 0 END) ELSE 0 END) AS bitBackOrder,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(Opp.numQtyShipped,0) numQtyShipped,ISNULL(Opp.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(OM.fltExchangeRate,0) AS fltExchangeRate,ISNULL(OM.numCurrencyID,0) AS numCurrencyID,ISNULL(OM.numDivisionID,0) AS numDivisionID,
(CASE 
	WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 AND UPPER(I.charItemType) = ''P'' AND bitDropShip = 0
	THEN (CASE WHEN OM.tintShipped = 1 THEN CAST(ISNULL(Opp.numUnitHour,0) AS VARCHAR) ELSE CAST(ISNULL(Opp.numQtyShipped,0) AS VARCHAR) END)
	WHEN OM.tintOppType = 2 AND OM.tintOppStatus = 1 AND UPPER(I.charItemType) = ''P'' AND bitDropShip = 0
	THEN (CASE WHEN OM.tintShipped = 1 THEN CAST(ISNULL(Opp.numUnitHour,0) AS VARCHAR) ELSE CAST(ISNULL(Opp.numUnitHourReceived,0) AS VARCHAR) END)
	ELSE '''' END) AS vcShippedReceived,
isnull(i.numIncomeChartAcntId,0) as itemIncomeAccount,isnull(i.numAssetChartAcntId,0) as itemInventoryAsset,isnull(i.numCOGsChartAcntId,0) as itemCoGs,
ISNULL(Opp.numRentalIN,0) as numRentalIN,
ISNULL(Opp.numRentalLost,0) as numRentalLost,
ISNULL(Opp.numRentalOut,0) as numRentalOut,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(bitFreeShipping,0) AS [IsFreeShipping],Opp.bitDiscountType,isnull(Opp.monLandedCost,0) as monLandedCost, ISNULL(Opp.vcPromotionDetail,'''') AS vcPromotionDetail,
(CASE WHEN I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 THEN dbo.GetOppItemReleaseDates(om.numDomainId,Opp.numOppId,Opp.numoppitemtCode,1) ELSE '''' END) AS vcItemReleaseDate,'''' AS numPurchasedQty, '

--	IF @fld_Name = 'vcPathForTImage' 
		SET @strSQL = @strSQL + 'Opp.vcPathForTImage,'
--	ELSE IF @fld_Name = 'vcItemName' 
		SET @strSQL = @strSQL + 'Opp.vcItemName,'
--	ELSE IF @fld_Name = 'vcModelID,' 
		SET @strSQL = @strSQL + 'Opp.vcModelID,'
--	ELSE IF @fld_Name = 'vcWareHouse' 
		SET @strSQL = @strSQL + 'vcWarehouse,'
		SET @strSQL = @strSQL + 'WItems.numOnHand,isnull(WItems.numAllocation,0) as numAllocation,'
		SET @strSQL = @strSQL + 'WL.vcLocation,'
--	ELSE IF @fld_Name = 'ItemType' 
		SET @strSQL = @strSQL + 'Opp.vcType ItemType,Opp.vcType,'
--	ELSE IF @fld_Name = 'vcItemDesc' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.vcItemDesc, '''') vcItemDesc,'
--	ELSE IF @fld_Name = 'Attributes' 
		SET @strSQL = @strSQL + 'Opp.vcAttributes,' --dbo.fn_GetAttributes(Opp.numWarehouseItmsID, I.bitSerialized) AS Attributes,
--	ELSE IF @fld_Name = 'bitDropShip' 
		SET @strSQL = @strSQL + 'ISNULL(bitDropShip, 0) as DropShip,CASE ISNULL(bitDropShip, 0) WHEN 1 THEN ''Yes'' ELSE '''' END AS bitDropShip,'
--	ELSE IF @fld_Name = 'numUnitHour' 
		SET @strSQL = @strSQL + 'CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,om.numDomainId, ISNULL(opp.numUOMId, 0))* Opp.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,'
--	ELSE IF @fld_Name = 'monPrice' 
		SET @strSQL = @strSQL + 'Opp.monPrice,'
--	ELSE IF @fld_Name = 'monPriceUOM' 
		SET @strSQL = @strSQL + 'CAST(( dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), Opp.numItemCode,om.numDomainId, ISNULL(I.numBaseUnit, 0)) * Opp.monPrice) AS NUMERIC(18, 4)) AS monPriceUOM,'
--	ELSE IF @fld_Name = 'monTotAmount' 
		SET @strSQL = @strSQL + 'Opp.monTotAmount,'
--	ELSE IF @fld_Name = 'vcSKU' 
		SET @strSQL = @strSQL + 'ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'''')) [vcSKU],'
--	ELSE IF @fld_Name = 'vcManufacturer' 
		SET @strSQL = @strSQL + 'Opp.vcManufacturer,'
--	ELSE IF @fld_Name = 'vcItemClassification' 
		SET @strSQL = @strSQL + ' dbo.[fn_GetListItemName](I.[numItemClassification]) numItemClassification,'
--	ELSE IF @fld_Name = 'numItemGroup' 
		SET @strSQL = @strSQL + '( SELECT    [vcItemGroup] FROM      [ItemGroups] WHERE     [numItemGroupID] = I.numItemGroup ) numItemGroup,'
--	ELSE IF @fld_Name = 'vcUOMName' 
		SET @strSQL = @strSQL + 'ISNULL(u.vcUnitName, '''') numUOMId,'
--	ELSE IF @fld_Name = 'fltWeight' 
		SET @strSQL = @strSQL + 'I.fltWeight,'
--	ELSE IF @fld_Name = 'fltHeight' 
		SET @strSQL = @strSQL + 'I.fltHeight,'
--	ELSE IF @fld_Name = 'fltWidth' 
		SET @strSQL = @strSQL + 'I.fltWidth,'
--	ELSE IF @fld_Name = 'fltLength' 
		SET @strSQL = @strSQL + 'I.fltLength,'
--	ELSE IF @fld_Name = 'numBarCodeId' 
		SET @strSQL = @strSQL + 'I.numBarCodeId,'
		SET @strSQL = @strSQL + 'I.IsArchieve,'
--	ELSE IF @fld_Name = 'vcWorkOrderStatus' 
		SET @strSQL = @strSQL + 'CASE WHEN ISNULL(Opp.bitWorkOrder, 0) = 1
				 THEN ( SELECT  dbo.GetListIemName(numWOStatus)
								+ CASE WHEN ISNULL(numWOStatus, 0) = 23184
									   THEN '' - ''
											+ CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo),
														  '''') + '',''
											+ CONVERT(VARCHAR(20), DATEADD(minute, '+ CONVERT(VARCHAR(15),-@ClientTimeZoneOffset)  +', bintCompletedDate)) AS VARCHAR(100))
									   ELSE ''''
								  END
						FROM    WorkOrder
						WHERE   ISNULL(numOppId, 0) = Opp.numOppId
								AND numItemCode = Opp.numItemCode AND ISNULL(numParentWOID,0)=0
					  )
				 ELSE ''''
			END AS vcWorkOrderStatus,'
--	ELSE IF @fld_Name = 'DiscAmt' 
		SET @strSQL = @strSQL + 'CASE WHEN Opp.bitDiscountType = 0
				 THEN REPLACE(STR(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount,20,4), '' '', '''') + '' (''
					  + CAST(FORMAT(Opp.fltDiscount,''0.00##'') AS VARCHAR(50)) + ''%)''
				 ELSE REPLACE(STR(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount,20,4), '' '', '''')
			END AS DiscAmt,'	
--	ELSE IF @fld_Name = 'numProjectID' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectID, 0) numProjectID,
		 CASE WHEN ( SELECT TOP 1
								ISNULL(OBI.numOppBizDocItemID, 0)
						FROM    dbo.OpportunityBizDocs OBD
								INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID
						WHERE   OBI.numOppItemID = Opp.numoppitemtCode
								AND ISNULL(OBD.bitAuthoritativeBizDocs, 0) = 1
								and isnull(OBD.numOppID,0)=OM.numOppID
					  ) > 0 THEN 1
				 ELSE 0
			END AS bitItemAddedToAuthBizDoc,'	
--	ELSE IF @fld_Name = 'numClassID' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.numClassID, 0) numClassID,'	
--	ELSE IF @fld_Name = 'vcProjectStageName' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectStageID, 0) numProjectStageID,
				CASE WHEN OPP.numProjectStageID > 0
				 THEN dbo.fn_GetProjectStageName(OPP.numProjectId,
												 OPP.numProjectStageID)
				 WHEN OPP.numProjectStageID = -1 THEN ''T&E''
				 WHEN OPP.numProjectId > 0
				 THEN CASE WHEN OM.tintOppType = 1 THEN ''S.O.''
						   ELSE ''P.O.''
					  END
				 ELSE ''-''
			END AS vcProjectStageName,'	
	        
	
	

--------------Add custom fields to query----------------
SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM View_DynamicCustomColumns
WHERE 
grp_id = 5 AND numDomainID=@numDomainId
AND numFormId= (CASE WHEN @tintOppType=1 THEN 26 ELSE 129 END) AND bitCustom=1 AND numUserCntID = @numUserCntID ORDER BY numFieldId
WHILE @fld_ID > 0
BEGIN ------------------

	 PRINT @fld_Name
	
	       SET @strSQL = @strSQL + 'dbo.GetCustFldValueItem('
                                    + CONVERT(VARCHAR(15), @fld_ID)
                                    + ',Opp.numItemCode) as ''' + @fld_Name + ''','
       

SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM View_DynamicCustomColumns
WHERE 
grp_id = 5 AND numDomainID=@numDomainId
AND numFormId=(CASE WHEN @tintOppType=1 THEN 26 ELSE 129 END) AND bitCustom=1  AND numUserCntID = @numUserCntID AND numFieldId > @fld_ID ORDER BY numFieldId
 IF @@ROWCOUNT=0
 SET @fld_ID = 0
END ------------------


                                
--Purchase Opportunity ?
IF @tintOppType = 2 
    BEGIN
        SET @strSQL =  @strSQL + 'ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo,
		  SUBSTRING(( SELECT  '', '' + vcSerialNo
                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                THEN '' (''
                                    + CONVERT(VARCHAR(15), oppI.numQty)
                                    + '')''
                                ELSE ''''
                            END
                FROM    OppWarehouseSerializedItem oppI
                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                WHERE   oppI.numOppID = Opp.numOppId
                        AND oppI.numOppItemID = Opp.numoppitemtCode
                ORDER BY vcSerialNo
                FOR
                XML PATH('''')
                ), 3, 200000) AS vcSerialLotNo,
		  CAST(Opp.numUnitHour AS FLOAT) numQty,
		  (SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  isnull(Opp.numWarehouseItmsID,0) as numWarehouseItmsID,'
	END
ELSE
	BEGIN	
        SET @strSQL =  @strSQL + ' ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo,
		   SUBSTRING(( SELECT  '', '' + vcSerialNo
                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                THEN '' (''
                                    + CONVERT(VARCHAR(15), oppI.numQty)
                                    + '')''
                                ELSE ''''
                            END
                FROM    OppWarehouseSerializedItem oppI
                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                WHERE   oppI.numOppID = Opp.numOppId
                        AND oppI.numOppItemID = Opp.numoppitemtCode
                ORDER BY vcSerialNo
                FOR
                XML PATH('''')
                ), 3, 200000) AS vcSerialLotNo,
		   CAST(Opp.numUnitHour AS FLOAT) numQty,
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  isnull(Opp.numWarehouseItmsID,0) as numWarehouseItmsID,'
	END


        
 SET @strSQL = left(@strSQL,len(@strSQL)-1) + '   
        FROM    OpportunityItems Opp
        LEFT JOIN OpportunityMaster OM ON Opp.numOppID = oM.numOppID
        JOIN item I ON Opp.numItemCode = i.numItemcode
        LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = Opp.numWarehouseItmsID
        LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
        LEFT JOIN WarehouseLocation WL ON WL.numWLocationID = WItems.numWLocationID
        LEFT JOIN OpportunityMaster M ON Opp.numSourceID = M.numOppID
        LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId
WHERE   om.numDomainId = '+ CONVERT(VARCHAR(10),@numDomainId) + '
        AND Opp.numOppId = '+ CONVERT(VARCHAR(15),@OpportunityId) +'
ORDER BY opp.numSortOrder,numoppitemtCode ASC '
  
PRINT @strSQL;
EXEC (@strSQL);
SELECT  vcSerialNo,
       vcComments AS Comments,
        numOppItemID AS numoppitemtCode,
        W.numWarehouseItmsDTLID,
        numWarehouseItmsID AS numWItmsID,
        dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                             1) Attributes
FROM    WareHouseItmsDTL W
        JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
WHERE   numOppID = @OpportunityId


CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,intVisible int,vcFieldDataType CHAR(1))


IF
	(
		SELECT 
			COUNT(*)
		FROM 
			View_DynamicColumns DC 
		WHERE 
			DC.numFormId=(CASE WHEN @tintOppType=1 THEN 26 ELSE 129 END) and DC.numDomainID=@numDomainID AND numUserCntID = @numUserCntID AND
			ISNULL(DC.bitSettingField,0)=1 AND ISNULL(DC.bitDeleted,0)=0 AND ISNULL(DC.bitCustom,0)=0) > 0
	OR
	(
		SELECT 
			COUNT(*)
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=(CASE WHEN @tintOppType=1 THEN 26 ELSE 129 END) AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0 AND ISNULL(bitCustom,0)=1
	) > 0
BEGIN	

INSERT INTO #tempForm
select isnull(DC.tintRow,0)+1 as tintOrder,DDF.vcDbColumnName,DDF.vcOrigDbColumnName,ISNULL(DDF.vcCultureFieldName,DDF.vcFieldName),                  
 DDF.vcAssociatedControlType,DDF.vcListItemType,DDF.numListID,DDF.vcLookBackTableName                                                 
,DDF.bitCustom,DDF.numFieldId,DDF.bitAllowSorting,DDF.bitInlineEdit,
DDF.bitIsRequired,DDF.bitIsEmail,DDF.bitIsAlphaNumeric,DDF.bitIsNumeric,DDF.bitIsLengthValidation,
DDF.intMaxLength,DDF.intMinLength,DDF.bitFieldMessage,DDF.vcFieldMessage vcFieldMessage,DDF.ListRelID,DDF.intColumnWidth,Case when DC.numFieldID is null then 0 else 1 end,DDF.vcFieldDataType
 FROM View_DynamicDefaultColumns DDF left join View_DynamicColumns DC on DC.numFormId=(CASE WHEN @tintOppType=1 THEN 26 ELSE 129 END) and DDF.numFieldId=DC.numFieldId and DC.numUserCntID=@numUserCntID and DC.numDomainID=@numDomainID and numRelCntType=0 AND tintPageType=1 
 where DDF.numFormId=(CASE WHEN @tintOppType=1 THEN 26 ELSE 129 END) and DDF.numDomainID=@numDomainID AND ISNULL(DDF.bitSettingField,0)=1 AND ISNULL(DDF.bitDeleted,0)=0 AND ISNULL(DDF.bitCustom,0)=0
       
 UNION
    
    select tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
 from View_DynamicCustomColumns
 where numFormId=(CASE WHEN @tintOppType=1 THEN 26 ELSE 129 END) and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0
 AND ISNULL(bitCustom,0)=1

END
ELSE
BEGIN
	--Check if Master Form Configuration is created by administrator if NoColumns=0
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = (CASE WHEN @tintOppType=1 THEN 26 ELSE 129 END) AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END

	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName, vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
			bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,1,vcFieldDataType
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType=1 THEN 26 ELSE 129 END) AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
			 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
			,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType=1 THEN 26 ELSE 129 END) AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
END


select * from #tempForm order by tintOrder

drop table #tempForm

            END
            
    END
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount money =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(9),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0,
  @numShipmentMethod NUMERIC(18,0)=0,
  @dtReleaseDate DATE = NULL,
  @numPartner NUMERIC(18,0)=0,
  @tintClickBtn INT=0
)                                                                          
                                                                          
as  
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @fltExchangeRate float                                 
	DECLARE @hDocItem int                                                                                                                            
	DECLARE @TotalAmount as money                                                                          
	DECLARE @tintOppStatus as tinyint               

	IF CONVERT(VARCHAR(10),@strItems) <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

		--VALIDATE PROMOTION OF COUPON CODE
		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1),
			numOppItemID NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numOppItemID,
			numPromotionID
		)
		SELECT
			numoppitemtCode,
			numPromotionID
		FROM
			OPENXML (@hDocItem,'/NewDataSet/Item',2)
		WITH
		(
			numoppitemtCode NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		IF (SELECT
				COUNT(*)
			FROM
				PromotionOffer PO
			WHERE
				numDomainId=@numDomainId
				AND bitRequireCouponCode = 1
				AND ISNULL(tintUsageLimit,0) > 0
				AND (intCouponCodeUsed 
					- (CASE WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE numOppID=ISNULL(@numOppID,0) AND numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END) 
					+ (CASE WHEN ISNULL((SELECT COUNT(*) FROM @TEMP WHERE numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END)) > tintUsageLimit) > 0
		BEGIN
			RAISERROR('USAGE_OF_COUPON_EXCEED',16,1)
			RETURN
		END
		ELSE
		BEGIN
			UPDATE
				PO
			SET 
				intCouponCodeUsed = ISNULL(intCouponCodeUsed,0) 
									- (CASE WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE numOppID=ISNULL(@numOppID,0) AND numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END) 
									+ (CASE WHEN ISNULL(T1.intUsed,0) > 0 THEN 1 ELSE 0 END) 
			FROM
				PromotionOffer PO
			INNER JOIN
				(
					SELECT
						numPromotionID,
						COUNT(*) AS intUsed
					FROM
						@TEMP
					GROUP BY
						numPromotionID
				) AS T1
			ON
				PO.numProId = T1.numPromotionID
			WHERE
				numDomainId=@numDomainId
				AND bitRequireCouponCode = 1
		END
	END

	--IF NEW OPPERTUNITY
	IF @numOppID = 0                                                                          
	BEGIN
		DECLARE @bitIsInitalSalesOrder BIT
		IF(@DealStatus=1 AND @tintOppType=1)
		BEGIN
			SET @bitIsInitalSalesOrder=1
		END

		DECLARE @intOppTcode AS NUMERIC(9)  
  
		SELECT @intOppTcode = MAX(numOppId) FROM OpportunityMaster                

		SET @intOppTcode = ISNULL(@intOppTcode,0) + 1                                                
		
		SET @vcPOppName = @vcPOppName + '-'+ CONVERT(varchar(4),YEAR(GETUTCDATE()))  +'-A'+ CONVERT(varchar(10),@intOppTcode)                                  
  
		IF ISNULL(@numCurrencyID,0) = 0 
		BEGIN
			SET @fltExchangeRate=1
			SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
		END
		ELSE
		BEGIN 
			SET @fltExchangeRate = dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
        END  
		                                                        
		-- GET ACCOUNT CLASS IF ENABLED
		DECLARE @numAccountClass AS NUMERIC(18) = 0

		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivisionID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
        IF(@DealStatus=1 AND @tintOppType=1 AND @tintClickBtn=1)
		BEGIN
			SET @DealStatus=0
		END         
		--PRINT ('Deal Staus ' +@DealStatus)                                                      
		INSERT INTO OpportunityMaster
		(                                                                             
			numContactId,numDivisionId,txtComments,numCampainID,bitPublicFlag,tintSource,tintSourceType,vcPOppName,                                                                          
			intPEstimatedCloseDate,monPAmount,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,                                                                                                             
			numRecOwner,lngPConclAnalysis,tintOppType,numSalesOrPurType,numCurrencyID,fltExchangeRate,numAssignedTo,
			numAssignedBy,[tintOppStatus],numStatus,vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,
			bitInterestType,fltInterest,vcCouponCode,bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,
			vcMarketplaceOrderReportId,numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
			bitUseMarkupShippingRate,numMarkupShippingRate,intUsedShippingCompany,numShipmentMethod,numPartner,dtReleaseDate,bitIsInitalSalesOrder
		)                                                                          
		Values                                                                          
		(                                                                          
			@numContactId,@numDivisionId,ISNULL(@Comments,''),@CampaignID,@bitPublicFlag,@tintSource,@tintSourceType,@vcPOppName,                                                                          
			@dtEstimatedCloseDate,@monPAmount,@numUserCntID,(CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END),                                                                          
			@numUserCntID,GETUTCDATE(),@numDomainId,@numUserCntID,@lngPConclAnalysis,@tintOppType,@numSalesOrPurType,@numCurrencyID, 
			@fltExchangeRate,(CASE WHEN @numAssignedTo > 0 THEN @numAssignedTo ELSE NULL END),(CASE WHEN @numAssignedTo>0 THEN @numUserCntID ELSE NULL END),
			@DealStatus,@numStatus,@vcOppRefOrderNo,@bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,
			@vcCouponCode,@bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,@numPercentageComplete,
			@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,@bitUseMarkupShippingRate,@numMarkupShippingRate,@intUsedShippingCompany,@numShipmentMethod,@numPartner,@dtReleaseDate,@bitIsInitalSalesOrder
		)                                                                                                                      
		
		SET @numOppID=(SELECT SCOPE_IDENTITY())                
                            
		
		--UPDATE OPPNAME AS PER NAME TEMPLATE
		EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

		--MAP CUSTOM FIELD	
		DECLARE @tintPageID AS TINYINT;
		SET @tintPageID = CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
		EXEC dbo.USP_AddParentChildCustomFieldMap
				@numDomainID = @numDomainID, --  numeric(18, 0)
				@numRecordID = @numOppID, --  numeric(18, 0)
				@numParentRecId = @numDivisionId, --  numeric(18, 0)
				@tintPageID = @tintPageID --  tinyint
 	
		IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
		BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END
  
		-- INSERTING ITEMS                                                                                                                    
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,
				monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
				numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,vcAttrValues,monAvgCost,bitItemPriceApprovalRequired,
				numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,vcSKU
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
				(CASE X.numWarehouseItmsID 
				WHEN -1 
				THEN 
					CASE 
						WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU= X.vcSKU) > 0)
							THEN
								(SELECT TOP 1 
									numWareHouseItemID 
								FROM 
									WareHouseItems 
								WHERE 
									[numItemID] = X.numItemCode 
									AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
						ELSE
							(SELECT 
								[numWareHouseItemID] 
							FROM 
								[WareHouseItems] 
							WHERE 
								[numItemID] = X.numItemCode 
								AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				WHEN 0 
				THEN 
					CASE 
					WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU=X.vcSKU) > 0)
						THEN
							(SELECT TOP 1 
								numWareHouseItemID 
							FROM 
								WareHouseItems 
							WHERE 
								[numItemID] = X.numItemCode 
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
					ELSE
						(SELECT 
							[numWareHouseItemID] 
						FROM 
							[WareHouseItems] 
						WHERE 
							[numItemID] = X.numItemCode 
							AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
							AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				ELSE  
					X.numWarehouseItmsID 
				END) AS numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
				(SELECT isnull(VN.monCost,0) FROM Vendor VN, Item IT WHERE IT.numItemCode=VN.numItemCode AND VN.numItemCode=X.numItemCode AND VN.numVendorID=IT.numVendorID),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,
				0,
				X.numToWarehouseItemID,X.Attributes,X.AttributeIDs,(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),
				X.bitItemPriceApprovalRequired,X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,X.vcSKU
			FROM
				(
					SELECT 
						*
					FROM 
						OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
					WITH                       
					(                                                                          
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9), numUnitHour FLOAT, monPrice DECIMAL(30,16), monTotAmount MONEY,vcItemDesc VARCHAR(1000),                                    
						numWarehouseItmsID NUMERIC(9), ItemType VARCHAR(30), DropShip BIT, bitDiscountType BIT,	fltDiscount DECIMAL(30,16), monTotAmtBefDiscount MONEY,
						vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),
						numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),AttributeIDs VARCHAR(500),vcSKU VARCHAR(100),bitItemPriceApprovalRequired BIT,
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0)
					)
				)X    
			ORDER BY 
				X.numoppitemtCode
    
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,numWarehouseItmsID=X.numWarehouseItmsID,bitDropShip=X.DropShip,
				bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
				bitWorkOrder=X.bitWorkOrder,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,
				numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,vcAttributes=X.Attributes,
				numClassID=0,
				bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired,vcAttrValues=X.AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,
					monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,
					Attributes,bitItemPriceApprovalRequired,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
				WITH  
					(                      
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount MONEY,vcItemDesc VARCHAR(1000),                                    
						numWarehouseItmsID NUMERIC(9),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount MONEY,vcItemName VARCHAR(300),
						numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),
						numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0)
					)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID   
				
			--INSERT KIT ITEMS                 
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ID.numWareHouseItemId,
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0
			FROM 
				OpportunityItems OI 
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId 

			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
				DECLARE @TempKitConfiguration TABLE
				(
					numItemCode NUMERIC(18,0),
					numWarehouseItemID NUMERIC(18,0),
					KitChildItems VARCHAR(MAX)
				)

				INSERT INTO 
					@TempKitConfiguration
				SELECT
					numItemCode,
					numWarehouseItmsID,
					KitChildItems
				FROM
					OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)
				WITH                       
				(
					numItemCode NUMERIC(9),numWarehouseItmsID NUMERIC(9), KitChildItems VARCHAR(MAX)
				) 

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					ItemDetails.numWareHouseItemId,
					(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0
				FROM
					(
						SELECT 
							t1.numItemCode,
							t1.numWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										0,
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,0,CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										0, 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemWarehouseItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration WHERE numItemCode=t1.numItemCode AND numWarehouseItemID=t1.numWarehouseItemID),',') 
						) as t2
					) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numChildItemID=TempChildItems.numKitItemID
					AND OKI.numWarehouseItemId=TempChildItems.numKitWarehouseItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numWarehouseItmsID = TempChildItems.numWarehouseItemID
					AND OI.numoppitemtcode = OKI.numOppItemID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					AND (TempChildItems.numKitChildItemWarehouseItemID = ItemDetails.numWareHouseItemId  OR ISNULL(TempChildItems.numKitChildItemWarehouseItemID,0) = 0)
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END

			-- UPDATE UOM OF OPPORTUNITY ITEMS HAVE NOT UOM SET WHILE EXPORTED FROM MARKETPLACE (MEANS TINTSOURCETYPE = 3)
			IF ISNULL(@tintSourceType,0) = 3
			BEGIN
				UPDATE 
					OI 
				SET 
					numUOMID = ISNULL(I.numSaleUnit,0), 
					numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
				FROM 
					OpportunityItems OI
				JOIN 
					Item I 
				ON 
					OI.numItemCode = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND I.numDomainID = @numDomainId
			END	
                                     
			INSERT INTO OppWarehouseSerializedItem
			(
				numOppID,
				numWarehouseItmsDTLID,
				numOppItemID,
				numWarehouseItmsID
			)                      
			SELECT 
				@numOppID,
				X.numWarehouseItmsDTLID,
				X.numoppitemtCode,
				X.numWItmsID 
			FROM
			(                                                                          
				SELECT
					* 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
				WITH                        
				(                                               
				numWarehouseItmsDTLID numeric(9),                      
				numoppitemtCode numeric(9),                      
				numWItmsID numeric(9)                                                     
				)
			)X                                    
                                   
			UPDATE 
				OppWarehouseSerializedItem                       
			SET 
				numOppItemID=X.numoppitemtCode                                
			FROM                       
			(
				SELECT 
					numWarehouseItmsDTLID AS DTLID,
					O.numoppitemtCode 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
				WITH
				(
					numWarehouseItmsDTLID NUMERIC(9),numoppitemtCode NUMERIC(9),numWItmsID NUMERIC(9)
				)                                
				JOIN 
					OpportunityItems O 
				ON 
					O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID
			)X                 
			WHERE 
				numOppID=@numOppID 
				AND numWarehouseItmsDTLID=X.DTLID      
    
			EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
                     
			EXEC sp_xml_removedocument @hDocItem                                     
		END          
   
		SET @TotalAmount=0                                                           
		SELECT @TotalAmount = SUM(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID                                                                          
  
		UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numOppID                                                 
                      
		--INSERT TAX FOR DIVISION   
		IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
		BEGIN
			INSERT INTO dbo.OpportunityMasterTaxItems 
			(
				numOppId,
				numTaxItemID,
				fltPercentage,
				tintTaxType,
				numTaxID
			) 
			SELECT 
				@numOppID,
				TI.numTaxItemID,
				TEMPTax.decTaxValue,
				TEMPTax.tintTaxType,
				0
			FROM 
				TaxItems TI 
			JOIN 
				DivisionTaxTypes DTT 
			ON 
				TI.numTaxItemID = DTT.numTaxItemID
			OUTER APPLY
			(
				SELECT
					decTaxValue,
					tintTaxType
				FROM
					dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
			) AS TEMPTax
			WHERE 
				DTT.numDivisionID=@numDivisionId 
				AND DTT.bitApplicable=1
			UNION 
			SELECT 
				@numOppID,
				0,
				TEMPTax.decTaxValue,
				TEMPTax.tintTaxType,
				0
			FROM 
				dbo.DivisionMaster 
			OUTER APPLY
			(
				SELECT
					decTaxValue,
					tintTaxType
				FROM
					dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
			) AS TEMPTax
			WHERE 
				bitNoTax=0 
				AND numDivisionID=@numDivisionID			
		END	
	END
	ELSE                                                                                                                          
	BEGIN
		DECLARE @tempAssignedTo AS NUMERIC(9) = NULL                                                    
		SELECT 
			@tintOppStatus = tintOppStatus,
			@tempAssignedTo=ISNULL(numAssignedTo,0) 
		FROM   
			OpportunityMaster 
		WHERE  
			numOppID = @numOppID

		IF @tempAssignedTo <> @numAssignedTo
		BEGIN
			IF (SELECT COUNT(*) FROM BizDocComission WHERE ISNULL(numOppID,0) = @numOppID AND ISNULL(bitCommisionPaid,0) = 1) > 0
			BEGIN
				RAISERROR('COMMISSION_PAID_AGAINST_BIZDOC',16,1)
			END
		END
 
		--REVERTING BACK THE WAREHOUSE ITEMS                  
		IF @tintOppStatus = 1 
		BEGIN               
			EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID                  
		END                  

		IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
		BEGIN
			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
		END
		
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
		BEGIN
			EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END 
						
		UPDATE   
			OpportunityMaster
		SET      
			vcPOppName = @vcPOppName,txtComments = ISNULL(@Comments,''),bitPublicFlag = @bitPublicFlag,numCampainID = @CampaignID,tintSource = @tintSource,tintSourceType=@tintSourceType,
			intPEstimatedCloseDate = @dtEstimatedCloseDate,numModifiedBy = @numUserCntID,bintModifiedDate = GETUTCDATE(),lngPConclAnalysis = @lngPConclAnalysis,monPAmount = @monPAmount,
			tintActive = @tintActive,numSalesOrPurType = @numSalesOrPurType,numStatus = @numStatus,vcOppRefOrderNo=@vcOppRefOrderNo,
			bintOppToOrder=(CASE WHEN @tintOppStatus=0 AND @DealStatus=1 THEN GETUTCDATE() ELSE bintOppToOrder END),bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,
			bitBillingTerms=@bitBillingTerms,intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,tintTaxOperator=@tintTaxOperator,
			numDiscountAcntType=@numDiscountAcntType,vcCouponCode = @vcCouponCode,numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
			bitUseMarkupShippingRate = @bitUseMarkupShippingRate,numMarkupShippingRate = @numMarkupShippingRate,intUsedShippingCompany = @intUsedShippingCompany, numContactID=@numContactId,numShipmentMethod=@numShipmentMethod,numPartner=@numPartner
		WHERE
			numOppId = @numOppID   
	   
		-- UPDATING IF ORGANIZATION IS ASSIGNED TO SOMEONE                                                      
		IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN
			UPDATE
				OpportunityMaster 
			SET	
				numAssignedTo = @numAssignedTo, 
				numAssignedBy = @numUserCntID 
			WHERE 
				numOppId = @numOppID                                                    
		END                                                     
		ELSE IF(@numAssignedTo = 0) 
		BEGIN
			UPDATE 
				OpportunityMaster 
			SET 
				numAssignedTo = 0, 
				numAssignedBy = 0 
			WHERE
				numOppId = @numOppID
		END  
			                                                                    
		-- UPDATING OPP ITEMS
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0                                                                         
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   
			--DELETE CHILD ITEMS OF KIT WITHIN KIT
			DELETE FROM 
				OpportunityKitChildItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   

			--DELETE ITEMS
			DELETE FROM 
				OpportunityKitItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))                      
			                            
			DELETE FROM 
				OpportunityItems 
			WHERE 
				numOppID=@numOppID 
				AND numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   
	          
			--INSERT NEW ADDED ITEMS                        
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,
				[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numToWarehouseItemID,numShipmentMethod,
				numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired,vcAttrValues,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
				(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),
				(select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numToWarehouseItemID,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
				0,
				(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired, X.AttributeIDs,
				X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder
			FROM
			(
				SELECT 
					*
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH  
				(                      
					numoppitemtCode numeric(9),numItemCode numeric(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount money,vcItemDesc varchar(1000),                                    
					numWarehouseItmsID numeric(9),numToWarehouseItemID numeric(9),Op_Flag tinyint,ItemType varchar(30),DropShip bit,bitDiscountType bit,
					fltDiscount DECIMAL(30,16),monTotAmtBefDiscount MONEY,vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),
					numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(500), 
					bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0)
				)
			)X 
			
			-- UPDATE ITEMS
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,numWarehouseItmsID=X.numWarehouseItmsID,numToWarehouseItemID = X.numToWarehouseItemID,             
				bitDropShip=X.DropShip,bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,
				numUOMId=numUOM,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,
				numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired,bitWorkOrder=X.bitWorkOrder,vcAttrValues=AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,numToWarehouseItemID,DropShip,bitDiscountType,
					fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
					bitItemPriceApprovalRequired,bitWorkOrder,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
				WITH  
				(                      
					numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount MONEY,vcItemDesc VARCHAR(1000),numWarehouseItmsID NUMERIC(9), 
					numToWarehouseItemID NUMERIC(18,0),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount MONEY,vcItemName VARCHAR(300),numUOM NUMERIC(9),
					numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),numProjectStageID NUMERIC(9),Attributes VARCHAR(500),
					bitItemPriceApprovalRequired BIT, bitWorkOrder BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0)                                
				)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID 

			--INSERT NEW ADDED KIT ITEMS                 
			INSERT INTO OpportunityKitItems                                                                          
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ID.numWareHouseItemId,
				ID.numQtyItemsReq * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0
			FROM 
				OpportunityItems OI 
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			WHERE 
				OI.numOppId=@numOppId 
				AND OI.numoppitemtCode NOT IN (SELECT ISNULL(numOppItemID,0) FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID)  
				
			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
				DELETE FROM @TempKitConfiguration

				INSERT INTO 
					@TempKitConfiguration
				SELECT
					numItemCode,
					numWarehouseItmsID,
					KitChildItems
				FROM
					OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)
				WITH                       
				(
					numItemCode NUMERIC(9),numWarehouseItmsID NUMERIC(9), KitChildItems VARCHAR(MAX)
				) 

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					ItemDetails.numWareHouseItemId,
					ItemDetails.numQtyItemsReq * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0
				FROM
					(
						SELECT 
							t1.numItemCode,
							t1.numWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										0,
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,0,CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										0, 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemWarehouseItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration WHERE numItemCode=t1.numItemCode AND numWarehouseItemID=t1.numWarehouseItemID),',') 
						) as t2
					) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numChildItemID=TempChildItems.numKitItemID
					AND OKI.numWarehouseItemId=TempChildItems.numKitWarehouseItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numWarehouseItmsID = TempChildItems.numWarehouseItemID
					AND OI.numoppitemtcode = OKI.numOppItemID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					AND (TempChildItems.numKitChildItemWarehouseItemID = ItemDetails.numWareHouseItemId  OR ISNULL(TempChildItems.numKitChildItemWarehouseItemID,0) = 0)
			END                                  
			
			--UPDATE KIT ITEMS                 
			UPDATE 
				OKI 
			SET 
				numQtyItemsReq = numQtyItemsReq_Orig * OI.numUnitHour
			FROM 
				OpportunityKitItems OKI 
			JOIN 
				OpportunityItems OI 
			ON 
				OKI.numOppItemID=OI.numoppitemtCode 
				AND OKI.numOppId=OI.numOppId  
			WHERE 
				OI.numOppId=@numOppId 

			-- UPDATE CHILD ITEMS OF KIT WITHIN KIT
			UPDATE
				OKCI
			SET
				OKCI.numQtyItemsReq = (OKI.numQtyItemsReq * OKCI.numQtyItemsReq_Orig)
			FROM
				OpportunityKitChildItems OKCI
			INNER JOIN
				OpportunityKitItems OKI
			ON
				OKCI.numOppID = @numOppId
				AND OKCI.numOppItemID = OKI.numOppItemID
				AND OKCI.numOppChildItemID = OKI.numOppChildItemID                     
			                                    
			EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			   		                                                                      
			EXEC sp_xml_removedocument @hDocItem                                               
		END     

		--FOLLOWING DELETE STATEMENT IS COMMENTED BECUASE NOW WE ARE NOT ALLOWING USER TO EIDT SERIAL/LOT ITEM UNTILL USER REMOVED ASSIGNED SERIAL/LOT NUMBERS
	    --Delete previously added serial lot number added for opportunity
		--DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
	END


/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
            
			declare @bitApprovalforOpportunity bit
			declare @intOpportunityApprovalProcess int
			SELECT @bitApprovalforOpportunity=bitApprovalforOpportunity,@intOpportunityApprovalProcess=intOpportunityApprovalProcess 
					FROM Domain WHERE numDomainId=@numDomainID
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT

			SET @bitIsInitalSalesOrder=(SELECT bitIsInitalSalesOrder FROM OpportunityMaster WHERE numOppId=@numOppID AND numDomainId=@numDomainID)
			SET @bitViolatePrice=(SELECT bitMarginPriceViolated FROM Domain WHERE numDomainId=@numDomainID)
			
			IF(@bitViolatePrice=1 AND @bitIsInitalSalesOrder=1)
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus=@DealStatus WHERE numOppId=@numOppID
			END
			ELSE
			BEGIN
			IF(@bitApprovalforOpportunity=1)
			BEGIN
				IF(@DealStatus='1' OR @DealStatus='2')
				BEGIN
					IF(@intOpportunityApprovalProcess=1)
					BEGIN
						UPDATE OpportunityMaster SET intPromotionApprovalStatus=6,intChangePromotionStatus=@DealStatus
						WHERE numOppId=@numOppId
					END
					IF(@intOpportunityApprovalProcess=2)
					BEGIN
						UPDATE OpportunityMaster SET intPromotionApprovalStatus=1,intChangePromotionStatus=@DealStatus
						WHERE numOppId=@numOppId
					END
				END
			END
			ELSE
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus = @DealStatus WHERE numOppId=@numOppID
				declare @tintShipped as tinyint               
				select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID              
				if @tintOppStatus=1               
					begin         
					PRINT 'inside USP_UpdatingInventoryonCloseDeal'
						exec USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
					end  
				declare @tintCRMType as numeric(9)        
				declare @AccountClosingDate as datetime        

				select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
				select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

				--When deal is won                                         
				if @DealStatus = 1 
				begin           
					 if @AccountClosingDate is null                   
					  update OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numOppID              
				end         
				--When Deal is Lost
				else if @DealStatus = 2
				begin         
					 select @AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
					 if @AccountClosingDate is null                   
					  update OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numOppID
				end                    

				/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
				-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
				if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
				begin        
					update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
					where numDivisionID=@numDivisionID        
				end        
				-- Promote Lead to Account when Sales/Purchase Order is created against it
				ELSE if @tintCRMType=0 AND @DealStatus = 1 --Lead & Order
				begin        
					update divisionmaster set tintCRMType=2,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
					where numDivisionID=@numDivisionID        
				end
				--Promote Prospect to Account
				else if @tintCRMType=1 AND @DealStatus = 1 
				begin        
					update divisionmaster set tintCRMType=2,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
					where numDivisionID=@numDivisionID        
				end
		END
		END

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9) 
DECLARE @bitIsPrimary BIT;

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

--Bill Address
IF @numBillAddressId>0
BEGIN
 SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numBillAddressId
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName
END
  
  
  --Ship Address
IF @numShipAddressId>0
BEGIN
	SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numShipAddressId
 



  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName
END

--INSERT TAX FOR DIVISION   
IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN

-- DELETE ITEM LEVEL CRV TAX TYPES
DELETE FROM OpportunityMasterTaxItems WHERE numOppId=@numOppId AND numTaxItemID = 1

-- INSERT ITEM LEVEL CRV TAX TYPES
INSERT INTO OpportunityMasterTaxItems
(
	numOppId,
	numTaxItemID,
	fltPercentage,
	tintTaxType,
	numTaxID
) 
SELECT 
	OI.numOppId,
	1,
	TEMPTax.decTaxValue,
	TEMPTax.tintTaxType,
	TEMPTax.numTaxID
FROM 
	OpportunityItems OI
INNER JOIN
	ItemTax IT
ON
	OI.numItemCode = IT.numItemCode
OUTER APPLY
(
	SELECT
		decTaxValue,
		tintTaxType,
		numTaxID
	FROM
		dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,OI.numItemCode,IT.numTaxItemID,@numOppId,1,NULL)
) AS TEMPTax
WHERE
	OI.numOppId = @numOppID
	AND IT.numTaxItemID = 1 -- CRV TAX
GROUP BY
	OI.numOppId,
	TEMPTax.decTaxValue,
	TEMPTax.tintTaxType,
	TEMPTax.numTaxID

--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID
) 
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID = IT.numTaxItemID 
WHERE 
	OI.numOppId=@numOppID 
	AND IT.bitApplicable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	0,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numOppID 
	AND I.bitTaxable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT
	@numOppId,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
FROM
	dbo.OpportunityItems OI 
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
INNER JOIN
	TaxDetails TD
ON
	TD.numTaxID = IT.numTaxID
	AND TD.numDomainId = @numDomainId
WHERE
	OI.numOppId = @numOppID
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

END
  
 UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID

	IF @tintOppType=1 AND @tintOppStatus=1 
	BEGIN
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numStatus
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_GetByOppIDAndBizDocType')
DROP PROCEDURE USP_OpportunityBizDocs_GetByOppIDAndBizDocType
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_GetByOppIDAndBizDocType]
(                        
 @numDomainID AS NUMERIC(18,0),
 @numOppId AS NUMERIC(18,0),
 @numBizDocId AS NUMERIC(18,0)
)                        
AS 
BEGIN
	SELECT
		numOppBizDocsId
	FROM
		OpportunityBizDocs
	INNER JOIN
		OpportunityMaster
	ON
		OpportunityBizDocs.numOppId = OpportunityMaster.numOppId
	WHERE
		OpportunityMaster.numDomainId=@numDomainID
		AND OpportunityBizDocs.numOppId=@numOppId
		AND numBizDocId = @numBizDocId
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_GetUnPaidInvoices')
DROP PROCEDURE USP_OpportunityBizDocs_GetUnPaidInvoices
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_GetUnPaidInvoices]
(                        
 @numDomainID AS NUMERIC(18,0),
 @numOppId AS NUMERIC(18,0)
)                        
AS 
BEGIN
	SELECT
		numOppBizDocsId,
		ISNULL(OpportunityBizDocs.monDealAmount,0) AS monDealAmount,
		ISNULL(monAmountPaid,0) AS monAmountPaid,
		ISNULL(OpportunityBizDocs.monDealAmount,0) -ISNULL(OpportunityBizDocs.monAmountPaid,0) AS monAmountToPay,
		ISNULL(OpportunityMaster.numCurrencyID,0) AS numCurrencyID
	FROM
		OpportunityBizDocs
	INNER JOIN
		OpportunityMaster
	ON
		OpportunityBizDocs.numOppId = OpportunityMaster.numOppId
	WHERE
		OpportunityMaster.numDomainId=@numDomainID
		AND OpportunityBizDocs.numOppId=@numOppId
		AND numBizDocId = 287
		AND bitAuthoritativeBizDocs=1
		AND ISNULL(OpportunityBizDocs.monDealAmount,0) -ISNULL(OpportunityBizDocs.monAmountPaid,0) > 0
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_QtyAddedToBizDocType')
DROP PROCEDURE USP_OpportunityMaster_QtyAddedToBizDocType
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_QtyAddedToBizDocType]
(                        
 @numDomainID AS NUMERIC(18,0),
 @numOppId AS NUMERIC(18,0),
 @numBizDocId AS NUMERIC(18,0)
)                        
AS 
BEGIN
	DECLARE @bitAllQtyAddedToBizDocType AS BIT = 1

	IF (SELECT 
			COUNT(*) 
		FROM 
		(
			SELECT
				OI.numoppitemtCode,
				ISNULL(OI.numUnitHour,0) AS OrderedQty,
				ISNULL(TempInvoice.QtyAddedToBizDocType,0) AS QtyAddedToBizDocType
			FROM
				OpportunityItems OI
			INNER JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			OUTER APPLY
			(
				SELECT
					SUM(OpportunityBizDocItems.numUnitHour) AS QtyAddedToBizDocType
				FROM
					OpportunityBizDocs
				INNER JOIN
					OpportunityBizDocItems 
				ON
					OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
				WHERE
					OpportunityBizDocs.numOppId = @numOppID
					AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
					AND ISNULL(OpportunityBizDocs.numBizDocId,0) = @numBizDocId
			) AS TempInvoice
			WHERE
				OI.numOppID = @numOppID
		) X
		WHERE

			X.OrderedQty <> X.QtyAddedToBizDocType) > 0
	BEGIN
		SET @bitAllQtyAddedToBizDocType = 0
	END

	SELECT @bitAllQtyAddedToBizDocType
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_ValidateContainerQty')
DROP PROCEDURE USP_OpportunityMaster_ValidateContainerQty
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_ValidateContainerQty]
(                        
 @numDomainID AS NUMERIC(18,0),
 @numOppId AS NUMERIC(18,0)
)                        
AS 
BEGIN
	DECLARE @bitAddedEnoughContainer AS BIT = 1

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1),
		numContainerItem NUMERIC(18,0),
		fltRequiredQty FLOAT
	)

	DECLARE @TEMPItems TABLE
	(
		ID INT IDENTITY(1,1),
		numItemCode NUMERIC(18,0),
		numContainer NUMERIC(18,0),
		numItemQty FLOAT,
		numUnitHour FLOAT,
		bitContainer BIT
	)

	INSERT INTO 
		@TEMPItems
	SELECT
		OI.numItemCode,
		ISNULL(I.numContainer,0),
		ISNULL(I.numNoItemIntoContainer,0),
		OI.numUnitHour,
		(CASE WHEN ISNULL(I.bitContainer,0) = 1 THEN 1 ELSE 0 END)
	FROM 
		OpportunityMaster OM
	INNER JOIN
		OpportunityItems OI
	ON
		OM.numOppId=OI.numOppId
	INNER JOIN
		Item I
	ON
		OI.numItemCode = I.numItemCode
		AND ISNULL(I.charItemType,0) = 'P'
	WHERE
		OM.numDomainId=@numDomainID
		AND OM.numOppId=@numOppId
	ORDER BY
		ISNULL(I.bitContainer,0)

	IF (SELECT COUNT(*) FROM @TEMPItems WHERE bitContainer=0 AND numContainer=0) > 0
	BEGIN
		SET @bitAddedEnoughContainer = 0
	END
	ELSE
	BEGIN
		INSERT INTO 
			@TEMP
		SELECT
			T1.numContainer
			,SUM(T1.numNoItemIntoContainer)
		FROM
		(
			SELECT
				I.numContainer,
				CEILING(SUM(OI.numUnitHour) / CAST(I.numNoItemIntoContainer AS FLOAT)) AS numNoItemIntoContainer
			FROM
				OpportunityMaster OM
			INNER JOIN
				OpportunityItems OI
			ON
				OM.numOppId=OI.numOppId
			INNER JOIN
				WareHouseItems WI
			ON
				OI.numWarehouseItmsID=WI.numWareHouseItemID
			INNER JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
				AND ISNULL(I.charItemType,0) = 'P'
			WHERE
				OM.numDomainId=@numDomainID
				AND OM.numOppId=@numOppId
				AND numContainer > 0
			GROUP BY
				I.numContainer
				,I.numNoItemIntoContainer
		) AS T1
		GROUP BY
			T1.numContainer



		DECLARE @i AS INT = 1
		DECLARE @COUNT AS INT
		DECLARE @numTempContainer NUMERIC(18,0)
		DECLARE @numRequiredQty FLOAT

		SELECT @COUNT = COUNT(*) FROM @TEMP

		WHILE @i <= @COUNT
		BEGIN
			SELECT
				@numTempContainer=numContainerItem
				,@numRequiredQty=fltRequiredQty
			FROM
				@TEMP
			WHERE
				ID = @i


			IF ISNULL((SELECT SUM(numUnitHour) FROM OpportunityItems WHERE numOppId=@numOppId AND numItemCode=@numTempContainer),0) < @numRequiredQty
			BEGIN
				SET @bitAddedEnoughContainer = 0
				BREAK
			END

			SET @i = @i + 1
		END
	END

	SELECT @bitAddedEnoughContainer
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SalesFulfillmentQueue_Get')
DROP PROCEDURE USP_SalesFulfillmentQueue_Get
GO
CREATE PROCEDURE [dbo].[USP_SalesFulfillmentQueue_Get]  
	
AS
BEGIN
	-- FIRST DELETE ENTRIES FROM QUEUE WHICH ARE NOT VALID BECAUSE MASS FULFILLMENT RULE MAY BE CHANGED
	DELETE FROM 
		SalesFulfillmentQueue
	WHERE
		ISNULL(bitExecuted,0) = 0
		AND numSFQID NOT IN (
								SELECT 
									SFQ.numSFQID 
								FROM 
									SalesFulfillmentQueue SFQ
								INNER JOIN
									SalesFulfillmentConfiguration SFC
								ON
									SFQ.numDomainID = SFC.numDomainID
								WHERE
									(SFC.bitRule1IsActive = 1 AND SFC.tintRule1Type = 2 AND SFQ.numOrderStatus=SFC.numRule1OrderStatus)
									OR (SFC.bitRule2IsActive = 1 AND SFQ.numOrderStatus=SFC.numRule2OrderStatus)
									OR (SFC.bitRule3IsActive = 1 AND SFQ.numOrderStatus=SFC.numRule3OrderStatus)
									OR (SFC.bitRule4IsActive = 1 AND SFQ.numOrderStatus=SFC.numRule4OrderStatus)
							)
	
	-- GET TOP 25 entries
	SELECT TOP 25 
		SFQ.numSFQID,
		SFQ.numDomainID,
		SFQ.numUserCntID,
		SFQ.numOppID,
		OM.numDivisionId,
		OM.numContactID,
		D.numCurrencyID,
		OM.tintOppStatus,
		OM.tintshipped,
		D.numDefaultSalesShippingDoc,
		D.IsEnableDeferredIncome,
		D.bitAutolinkUnappliedPayment,
		ISNULL(OM.intUsedShippingCompany,ISNULL(D.numShipCompany,91)) numShipCompany,
		CASE 
			WHEN (SFC.bitRule1IsActive = 1 AND SFC.tintRule1Type = 2 AND SFQ.numOrderStatus=SFC.numRule1OrderStatus) THEN 1
			WHEN (SFC.bitRule2IsActive = 1 AND SFQ.numOrderStatus=SFC.numRule2OrderStatus) THEN 2
			WHEN (SFC.bitRule3IsActive = 1 AND SFQ.numOrderStatus=SFC.numRule3OrderStatus) THEN 3
			WHEN (SFC.bitRule4IsActive = 1 AND SFQ.numOrderStatus=SFC.numRule4OrderStatus) THEN 4
			ELSE 0
		END	AS tintRule
	FROM 
		SalesFulfillmentQueue SFQ
	INNER JOIN
		SalesFulfillmentConfiguration SFC
	ON
		SFQ.numDomainID = SFC.numDomainID
	INNER JOIN
		OpportunityMaster OM
	ON
		SFQ.numOppID = OM.numOppId
	INNER JOIN
		Domain D
	ON
		SFQ.numDomainID = D.numDomainID
	WHERE
		ISNULL(SFQ.bitExecuted,0) = 0
		AND ((SFC.bitRule1IsActive = 1 AND SFC.tintRule1Type = 2 AND SFQ.numOrderStatus=SFC.numRule1OrderStatus)
		OR (SFC.bitRule2IsActive = 1 AND SFQ.numOrderStatus=SFC.numRule2OrderStatus)
		OR (SFC.bitRule3IsActive = 1 AND SFQ.numOrderStatus=SFC.numRule3OrderStatus)
		OR (SFC.bitRule4IsActive = 1 AND SFQ.numOrderStatus=SFC.numRule4OrderStatus))
	
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SalesFulfillmentQueue_Save')
DROP PROCEDURE USP_SalesFulfillmentQueue_Save
GO
CREATE PROCEDURE [dbo].[USP_SalesFulfillmentQueue_Save]      
	@numDomainId NUMERIC(18,0),
	@numUserCntId NUMERIC(18,0),
	@numOppID NUMERIC(18,0),
	@numStatus NUMERIC(18,0)
AS
BEGIN
	IF (
			SELECT 
				COUNT(*) 
			FROM 
				SalesFulfillmentConfiguration 
			WHERE 
				numDomainID=@numDomainId 
				AND ISNULL(bitActive,0)=1
				AND 1 = (CASE 
							WHEN ISNULL(bitRule1IsActive,0)=1 AND tintRule1Type=2 AND numRule1OrderStatus=@numStatus THEN 1
							WHEN ISNULL(bitRule2IsActive,0)=1 AND numRule2OrderStatus=@numStatus THEN 1
							WHEN ISNULL(bitRule3IsActive,0)=1 AND numRule3OrderStatus=@numStatus THEN 1
							WHEN ISNULL(bitRule4IsActive,0)=1 AND numRule4OrderStatus=@numStatus THEN 1
							ELSE 0
						END)
		) > 0
		AND NOT EXISTS (SELECT * FROM SalesFulfillmentQueue WHERE numOppID=@numOppID AND numOrderStatus=@numStatus)
	BEGIN
		INSERT INTO SalesFulfillmentQueue
		( 
			numDomainID
			,numUserCntID
			,numOppID
			,numOrderStatus
			,dtDate
			,bitExecuted
			,intNoOfTimesTried
		)
		VALUES
		(
			@numDomainId
			,@numUserCntId
			,@numOppID
			,@numStatus
			,GETUTCDATE()
			,0
			,0
		)
	END
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SalesFulfillmentQueue_UpdateStatus')
DROP PROCEDURE USP_SalesFulfillmentQueue_UpdateStatus
GO
CREATE PROCEDURE [dbo].[USP_SalesFulfillmentQueue_UpdateStatus]
	@numDomainID NUMERIC(18,0),  
	@numSFQID NUMERIC(18,0),
	@vcMessage TEXT,
	@tintRule AS TINYINT,
	@bitSuccess BIT
AS
BEGIN
	DECLARE @numOppID AS NUMERIC(18,0)
	DECLARE @numRule2SuccessOrderStatus NUMERIC(18,0)
	DECLARE @numRule2FailOrderStatus NUMERIC(18,0)
	DECLARE @numRule3FailOrderStatus NUMERIC(18,0)
	DECLARE @numRule4FailOrderStatus NUMERIC(18,0)
	DECLARE @numRule5OrderStatus NUMERIC(18,0)
	DECLARE @numRule6OrderStatus NUMERIC(18,0)
	DECLARE @bitActive BIT
	DECLARE @bitRule5IsActive BIT
	DECLARE @bitRule6IsActive BIT

	SELECT
		@bitActive=bitActive
		,@numRule2SuccessOrderStatus=numRule2SuccessOrderStatus
		,@numRule2FailOrderStatus=numRule2FailOrderStatus
		,@numRule3FailOrderStatus=numRule3FailOrderStatus
		,@numRule4FailOrderStatus=numRule4FailOrderStatus
		,@bitRule5IsActive=bitRule5IsActive
		,@numRule5OrderStatus=numRule5OrderStatus
		,@bitRule6IsActive=bitRule6IsActive
		,@numRule6OrderStatus=numRule6OrderStatus
	FROM
		SalesFulfillmentConfiguration
	WHERE
		numDomainID = @numDomainID

	SELECT
		@numOppID=numOppID
	FROM
		SalesFulfillmentQueue
	WHERE
		numSFQID=@numSFQID
		AND @tintRule NOT IN (5,6)

	-- CHANGE ORDER STATUS BASED ON SALES FULFILLMENT CONFIGURATION
	IF @tintRule = 2
	BEGIN
		IF @bitSuccess = 1
		BEGIN
			UPDATE OpportunityMaster SET numStatus = @numRule2SuccessOrderStatus WHERE numOppId=@numOppID
		END
		ELSE
		BEGIN
			UPDATE OpportunityMaster SET numStatus = @numRule2FailOrderStatus WHERE numOppId=@numOppID
		END
	END
	ELSE IF @tintRule = 3 AND ISNULL(@bitSuccess,0) = 0
	BEGIN
		UPDATE OpportunityMaster SET numStatus = @numRule3FailOrderStatus WHERE numOppId=@numOppID
	END
	ELSE IF @tintRule = 4 AND ISNULL(@bitSuccess,0) = 0
	BEGIN
		UPDATE OpportunityMaster SET numStatus = @numRule4FailOrderStatus WHERE numOppId=@numOppID
	END
	ELSE IF @tintRule = 5 AND ISNULL(@bitRule5IsActive,0) = 1 AND ISNULL(@bitActive,0) = 1
	BEGIN
		UPDATE OpportunityMaster SET numStatus = @numRule5OrderStatus WHERE numOppId=@numSFQID
	END
	ELSE IF @tintRule = 6 AND ISNULL(@bitRule6IsActive,0) = 1 AND ISNULL(@bitActive,0) = 1
	BEGIN
		UPDATE OpportunityMaster SET numStatus = @numRule6OrderStatus WHERE numOppId=@numSFQID
	END

	-- MARK RULE AS EXECUTED
	UPDATE
		SalesFulfillmentQueue
	SET
		bitExecuted = 1
	WHERE
		numSFQID=@numSFQID
		AND @tintRule NOT IN (5,6)

	-- MAKE ENTRY IN LOG TABLE
	INSERT INTO SalesFulfillmentLog
	(
		numDomainID,
		numSFQID,
		numOPPID,
		vcMessage,
		dtDate
	)
	SELECT
		numDomainID,
		numSFQID,
		numOppID,
		@vcMessage,
		GETUTCDATE()
	FROM
		SalesFulfillmentQueue
	WHERE
		numSFQID=@numSFQID
		AND @tintRule NOT IN (5,6)
END
/****** Object:  StoredProcedure [dbo].[USP_SaveJournalDetails]    Script Date: 07/26/2008 16:21:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Create by Siva                                                                                        
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_SaveJournalDetails' ) 
    DROP PROCEDURE USP_SaveJournalDetails
GO
CREATE PROCEDURE [dbo].[USP_SaveJournalDetails]
    @numJournalId AS NUMERIC(9) = 0 ,
    @strRow AS TEXT = '' ,
    @Mode AS TINYINT = 0 ,
    @numDomainId AS NUMERIC(9) = 0                       
--@RecurringMode as tinyint=0,    
AS 
BEGIN           
                
BEGIN TRY 
		-- INCLUDE ENCODING OTHER WISE IT WILL THROW ERROR WHEN SPECIAL CHARACTERS ARE AVAILABLE IN DESCRIPT FIELDS
		IF CHARINDEX('<?xml',@strRow) = 0
		BEGIN
			SET @strRow = CONCAT('<?xml version="1.0" encoding="ISO-8859-1"?>',@strRow)
		END

        BEGIN TRAN  
        
        ---Always set default Currency ID as base currency ID of domain, and Rate =1
        DECLARE @numBaseCurrencyID NUMERIC
        SELECT @numBaseCurrencyID = ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId
        
        IF CONVERT(VARCHAR(100), @strRow) <> '' 
            BEGIN                                                                                                            
                DECLARE @hDoc3 INT                                                                                                                                                                
                EXEC sp_xml_preparedocument @hDoc3 OUTPUT, @strRow                                                                                                             
--                PRINT '---Insert xml into temp table ---'
                    SELECT numTransactionId ,numDebitAmt ,numCreditAmt ,numChartAcntId ,varDescription ,numCustomerId ,/*numDomainId ,*/bitMainDeposit ,bitMainCheck ,bitMainCashCredit ,numoppitemtCode ,chBizDocItems ,vcReference ,numPaymentMethod ,bitReconcile , 
                    ISNULL(NULLIF(numCurrencyID,0),@numBaseCurrencyID) AS numCurrencyID  ,ISNULL(NULLIF(fltExchangeRate,0.0000),1) AS fltExchangeRate ,numTaxItemID ,numBizDocsPaymentDetailsId ,numcontactid ,numItemID ,numProjectID ,numClassID ,numCommissionID ,numReconcileID ,bitCleared ,tintReferenceType ,numReferenceID ,numCampaignID 
                     INTO #temp FROM      OPENXML (@hdoc3,'/ArrayOfJournalEntryNew/JournalEntryNew',2)
												WITH (
												numTransactionId numeric(10, 0),numDebitAmt FLOAT,numCreditAmt FLOAT,numChartAcntId numeric(18, 0),varDescription varchar(1000),numCustomerId numeric(18, 0),/*numDomainId numeric(18, 0),*/bitMainDeposit bit,bitMainCheck bit,bitMainCashCredit bit,numoppitemtCode numeric(18, 0),chBizDocItems nchar(10),vcReference varchar(500),numPaymentMethod numeric(18, 0),bitReconcile bit,numCurrencyID numeric(18, 0),fltExchangeRate float,numTaxItemID numeric(18, 0),numBizDocsPaymentDetailsId numeric(18, 0),numcontactid numeric(9, 0),numItemID numeric(18, 0),numProjectID numeric(18, 0),numClassID numeric(18, 0),numCommissionID numeric(9, 0),numReconcileID numeric(18, 0),bitCleared bit,tintReferenceType tinyint,numReferenceID numeric(18, 0),numCampaignID NUMERIC(18,0)
													)

                 EXEC sp_xml_removedocument @hDoc3 
                                    

/*-----------------------Validation of balancing entry*/

IF EXISTS(SELECT * FROM #temp)
BEGIN
	DECLARE @SumTotal MONEY 
	SELECT @SumTotal = CAST(SUM(numDebitAmt) AS MONEY)-CAST(SUM(numCreditAmt) AS MONEY) FROM #temp

--	PRINT cast( @SumTotal AS DECIMAL(10,4))
	IF cast( @SumTotal AS DECIMAL(10,4)) <> 0.0000
	BEGIN
		 INSERT INTO [dbo].[GenealEntryAudit]
           ([numDomainID]
           ,[numJournalID]
           ,[numTransactionID]
           ,[dtCreatedDate]
           ,[varDescription])
		 SELECT @numDomainID,
				@numJournalId,
				0,
				GETUTCDATE(),
				'IM_BALANCE'

		 RAISERROR ( 'IM_BALANCE', 16, 1 ) ;
		 RETURN;
	END

	
END



DECLARE @numEntryDateSortOrder AS NUMERIC
DECLARE @datEntry_Date AS DATETIME
--Combine Date from datentryDate with time part of current time
SELECT @datEntry_Date=( CAST( CONVERT(VARCHAR,DATEPART(year, datEntry_Date)) + '-' + RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(month, datEntry_Date)),2)+ 
+ '-' + CONVERT(VARCHAR,DATEPART(day, datEntry_Date))
+ ' ' + CONVERT(VARCHAR,DATEPART(hour, GETDATE()))
+ ':' + CONVERT(VARCHAR,DATEPART(minute, GETDATE()))
+ ':' + CONVERT(VARCHAR,DATEPART(second, GETDATE())) AS DATETIME)  
) 
 from dbo.General_Journal_Header WHERE numJournal_Id=@numJournalId AND numDomainId=@numDomainId

SET @numEntryDateSortOrder = convert(numeric,
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(year, @datEntry_Date)),4)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(month, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(day, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(hour, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(minute, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(second, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(millisecond, @datEntry_Date)),3))

/*-----------------------Validation of balancing entry*/

			

                IF @Mode = 1  /*Edit Journal entries*/
                    BEGIN             
						PRINT '---delete removed transactions ---'
                        DELETE  FROM General_Journal_Details
                        WHERE   numJournalId = @numJournalId
                                AND numTransactionId NOT IN (SELECT numTransactionId FROM #temp as X )
			                                                           
						--Update transactions
						PRINT '---updated existing transactions ---'
                        UPDATE  dbo.General_Journal_Details 
                        SET    		[numDebitAmt] = X.numDebitAmt,
									[numCreditAmt] = X.numCreditAmt,
									[numChartAcntId] = X.numChartAcntId,
									[varDescription] = X.varDescription,
									[numCustomerId] = NULLIF(X.numCustomerId,0),
									[bitMainDeposit] = X.bitMainDeposit,
									[bitMainCheck] = X.bitMainCheck,
									[bitMainCashCredit] = X.bitMainCashCredit,
									[numoppitemtCode] = NULLIF(X.numoppitemtCode,0),
									[chBizDocItems] = X.chBizDocItems,
									[vcReference] = X.vcReference,
									[numPaymentMethod] = X.numPaymentMethod,
									[numCurrencyID] = NULLIF(X.numCurrencyID,0),
									[fltExchangeRate] = X.fltExchangeRate,
									[numTaxItemID] = NULLIF(X.numTaxItemID,0),
									[numBizDocsPaymentDetailsId] = NULLIF(X.numBizDocsPaymentDetailsId,0),
									[numcontactid] = NULLIF(X.numcontactid,0),
									[numItemID] = NULLIF(X.numItemID,0),
									[numProjectID] =NULLIF( X.numProjectID,0),
									[numClassID] = NULLIF(X.numClassID,0),
									[numCommissionID] = NULLIF(X.numCommissionID,0),
									numCampaignID=NULLIF(X.numCampaignID,0),
									numEntryDateSortOrder1=@numEntryDateSortOrder
--									[tintReferenceType] = X.tintReferenceType,
--									[numReferenceID] = X.numReferenceID
                          FROM    #temp as X 
						  WHERE   X.numTransactionId = General_Journal_Details.numTransactionId
			               
		               
					    INSERT INTO [dbo].[GenealEntryAudit]
									([numDomainID]
									,[numJournalID]
									,[numTransactionID]
									,[dtCreatedDate]
									,[varDescription])
						SELECT GJD.numDomainID, GJD.[numJournalId], GJD.[numTransactionId], GETUTCDATE(),[GJD].[varDescription]
						FROM General_Journal_Details AS GJD                                             
						JOIN #temp AS X ON  X.numTransactionId = GJD.numTransactionId
						
                    END 
                IF @Mode = 0 OR @Mode = 1 /*-- Insert journal only*/
                    BEGIN    
						PRINT '---insert existing transactions ---'                                                                                               
                        INSERT  INTO [dbo].[General_Journal_Details]
                                ( [numJournalId] ,
                                  [numDebitAmt] ,
                                  [numCreditAmt] ,
                                  [numChartAcntId] ,
                                  [varDescription] ,
                                  [numCustomerId] ,
                                  [numDomainId] ,
                                  [bitMainDeposit] ,
                                  [bitMainCheck] ,
                                  [bitMainCashCredit] ,
                                  [numoppitemtCode] ,
                                  [chBizDocItems] ,
                                  [vcReference] ,
                                  [numPaymentMethod] ,
                                  [bitReconcile] ,
                                  [numCurrencyID] ,
                                  [fltExchangeRate] ,
                                  [numTaxItemID] ,
                                  [numBizDocsPaymentDetailsId] ,
                                  [numcontactid] ,
                                  [numItemID] ,
                                  [numProjectID] ,
                                  [numClassID] ,
                                  [numCommissionID] ,
                                  [numReconcileID] ,
                                  [bitCleared],
                                  [tintReferenceType],
								  [numReferenceID],[numCampaignID],numEntryDateSortOrder1
	                                
                                )
                                SELECT  @numJournalId ,
                                        [numDebitAmt] ,
                                        [numCreditAmt] ,
                                        CASE WHEN [numChartAcntId]=0 THEN NULL ELSE numChartAcntId END  ,
                                        [varDescription] ,
                                        NULLIF(numCustomerId, 0) ,
                                        @numDomainId ,
                                        [bitMainDeposit] ,
                                        [bitMainCheck] ,
                                        [bitMainCashCredit] ,
                                        NULLIF([numoppitemtCode], 0) ,
                                        [chBizDocItems] ,
                                        [vcReference] ,
                                        [numPaymentMethod] ,
                                        [bitReconcile] ,
                                        NULLIF([numCurrencyID], 0) ,
                                        [fltExchangeRate] ,
                                        NULLIF([numTaxItemID], 0) ,
                                        NULLIF([numBizDocsPaymentDetailsId], 0) ,
                                        NULLIF([numcontactid], 0) ,
                                        NULLIF([numItemID], 0) ,
                                        NULLIF([numProjectID], 0) ,
                                        NULLIF([numClassID], 0) ,
                                        NULLIF([numCommissionID], 0) ,
                                        NULLIF([numReconcileID], 0) ,
                                        [bitCleared],
                                        tintReferenceType,
										NULLIF(numReferenceID,0),NULLIF(numCampaignID,0),@numEntryDateSortOrder
                                        
                                FROM #temp as X    
                                WHERE X.numTransactionId =0

						--Set Default Class If enable User Level Class Accountng 
						DECLARE @numAccountClass AS NUMERIC(18)=0                             
                        SELECT @numAccountClass=ISNULL(numAccountClass,0) FROM General_Journal_Header WHERE numDomainId=@numDomainId AND numJournal_Id=@numJournalId
                        IF @numAccountClass>0
                        BEGIN
							UPDATE General_Journal_Details SET numClassID=@numAccountClass WHERE numJournalId=@numJournalId AND numDomainId=@numDomainId AND ISNULL(numClassID,0) = 0
						END

						INSERT INTO [dbo].[GenealEntryAudit]
									([numDomainID]
									,[numJournalID]
									,[numTransactionID]
									,[dtCreatedDate]
									,[varDescription])
						SELECT @numDomainID, @numJournalId, X.numTransactionId, GETUTCDATE(),X.varDescription
						FROM #temp AS X WHERE X.numTransactionId = 0

                    END 
                    
                    
                    UPDATE dbo.General_Journal_Header 
						SET numAmount=(SELECT SUM(numDebitAmt) FROM dbo.General_Journal_Details WHERE numJournalID=@numJournalID)
						WHERE numJournal_Id=@numJournalId AND numDomainId=@numDomainId
						
						
                    drop table #temp
                    
                    -- Update leads,prospects to Accounts
					DECLARE @numDivisionID AS NUMERIC(18)
					DECLARE @numUserCntID AS NUMERIC(18)
					
					SELECT TOP 1 @numDivisionID = ISNULL(numCustomerId,0), @numUserCntID = ISNULL(numCreatedBy,0)
					FROM dbo.General_Journal_Details GJD
					JOIN dbo.General_Journal_Header GJH ON GJD.numJournalId = GJH.numJournal_Id AND GJD.numDomainId = GJH.numDomainId
					WHERE numJournalId = @numJournalId 
					AND GJH.numDomainId = @numDomainID
					
					IF @numDivisionID > 0 AND @numUserCntID > 0
					BEGIN
						EXEC dbo.USP_UpdateCRMTypeToAccounts @numDivisionID ,@numDomainID ,@numUserCntID
					END
            END
            
SELECT GJD.numTransactionId,GJD.numEntryDateSortOrder1,Row_number() OVER(ORDER BY GJD.numTransactionId) AS NewSortId  INTO #Temp1 
FROM General_Journal_Details GJD WHERE GJD.numDomainId = @numDomainID and  GJD.numEntryDateSortOrder1  LIKE SUBSTRING(CAST(@numEntryDateSortOrder AS VARCHAR(25)),1,12) + '%'

UPDATE  GJD SET GJD.numEntryDateSortOrder1 = (Temp1.numEntryDateSortOrder1 + Temp1.NewSortId) 
FROM General_Journal_Details GJD INNER JOIN #Temp1 AS Temp1 ON GJD.numTransactionId = Temp1.numTransactionId

IF object_id('TEMPDB.DBO.#Temp1') IS NOT NULL DROP TABLE #Temp1

        
        COMMIT TRAN 
    END TRY 
    BEGIN CATCH		
        DECLARE @strMsg VARCHAR(200)
        SET @strMsg = ERROR_MESSAGE()
        IF ( @@TRANCOUNT > 0 ) 
            BEGIN
				INSERT INTO [dbo].[GenealEntryAudit]
									([numDomainID]
									,[numJournalID]
									,[numTransactionID]
									,[dtCreatedDate]
									,[varDescription])
						SELECT @numDomainID, @numJournalId,0, GETUTCDATE(),@strMsg

                RAISERROR ( @strMsg, 16, 1 ) ;
                ROLLBACK TRAN
                RETURN 1
            END
    END CATCH	

            
--End                                              
---For Recurring Transaction                                              
                                              
--if @RecurringMode=1                                              
--Begin    
--  print 'SIVA--Recurring'    
--  print  '@numDomainId=============='+Convert(varchar(10),@numDomainId)                                     
--  Declare @numMaxJournalId as numeric(9)                                              
--  Select @numMaxJournalId=max(numJournal_Id) From General_Journal_Header Where numDomainId=@numDomainId                                              
--  insert into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,numCustomerId,numDomainId,bitMainCheck,bitMainCashCredit)                                              
--  Select @numMaxJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,NULLIF(numCustomerId,0),numDomainId,bitMainCheck,bitMainCashCredit from General_Journal_Details Where numJournalId=@numJournalId  And numDomainId=@numDomainId                         
--   
--                   
--/*Commented by chintan Opening balance Will be updated by Trigger*/
----Exec USP_UpdateChartAcntOpnBalanceForJournalEntry @JournalId=@numMaxJournalId,@numDomainId=@numDomainId                                              
--End           
    END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_OPPItemDetails @numItemCode=31455,@OpportunityId=12387,@numDomainId=110
--created by anooop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ShippingReport_Save')
DROP PROCEDURE USP_ShippingReport_Save
GO
CREATE PROCEDURE [dbo].[USP_ShippingReport_Save]
	@numDomainId NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
    ,@numOppBizDocId NUMERIC(18,0)
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @numShippingReportID NUMERIC(18,0)

	IF (SELECT COUNT(*) FROM OpportunityBizDocItems OBI INNER JOIN Item I ON OBI.numItemCode=I.numItemCode WHERE OBI.numOppBizDocID=@numOppBizDocId AND I.charItemType='P' AND ISNULL(numContainer,0) > 0) = 0
	BEGIN
		RAISERROR('Inventory item(s) are not available in bizdoc.',16,1)
	END

	DECLARE @vcFromName VARCHAR(1000)
    DECLARE @vcFromCompany VARCHAR(1000)
    DECLARE @vcFromPhone VARCHAR(100)
    DECLARE @vcFromAddressLine1 VARCHAR(50)
    DECLARE @vcFromAddressLine2 VARCHAR(50) = ''
    DECLARE @vcFromCity VARCHAR(50)
	DECLARE @vcFromState VARCHAR(50)
    DECLARE @vcFromZip VARCHAR(50)
    DECLARE @vcFromCountry VARCHAR(50)
	DECLARE @bitFromResidential BIT = 0

	DECLARE @vcToName VARCHAR(1000)
    DECLARE @vcToCompany VARCHAR(1000)
    DECLARE @vcToPhone VARCHAR(100)
    DECLARE @vcToAddressLine1 VARCHAR(50)
    DECLARE @vcToAddressLine2 VARCHAR(50) = ''
	DECLARE @vcToCity VARCHAR(50)
	DECLARE @vcToState VARCHAR(50)
    DECLARE @vcToZip VARCHAR(50)
    DECLARE @vcToCountry VARCHAR(50)
    DECLARE @bitToResidential BIT = 0

	-- GET FROM ADDRESS
	SELECT
		@vcFromName=vcName
		,@vcFromCompany=vcCompanyName
		,@vcFromPhone=vcPhone
		,@vcFromAddressLine1=vcStreet
		,@vcFromCity=vcCity
		,@vcFromState=CAST(vcState AS VARCHAR(18))
		,@vcFromZip=vcZipCode
		,@vcFromCountry=CAST(vcCountry AS VARCHAR(18))
	FROM
		dbo.fn_GetShippingReportAddress(1,@numDomainId,@numOppID)

	-- GET TO ADDRESS
	SELECT
		@vcToName=vcName
		,@vcToCompany=vcCompanyName
		,@vcToPhone=vcPhone
		,@vcToAddressLine1=vcStreet
		,@vcToCity=vcCity
		,@vcToState=CAST(vcState AS VARCHAR(18))
		,@vcToZip=vcZipCode
		,@vcToCountry=CAST(vcCountry AS VARCHAR(18))
	FROM
		dbo.fn_GetShippingReportAddress(2,@numDomainId,@numOppID)

	DECLARE @numShippingCompany NUMERIC(18,0)
    DECLARE @vcShippingService NUMERIC(18,0)

	SELECT @numShippingCompany=ISNULL(numShipmentMethod,0) FROM OpportunityMaster WHERE numDomainId=@numDomainId AND numOppId=@numOppID

	IF ISNULL(@numShippingCompany,0) = 0
	BEGIN
		SELECT @numShippingCompany=ISNULL(numShipCompany,0) FROM Domain WHERE numDomainId=@numDomainId
		
		IF @numShippingCompany <> 91 OR @numShippingCompany <> 88 OR @numShippingCompany <> 90
		BEGIN
			SET @numShippingCompany = 91
		END

		SET @vcShippingService = CASE @numShippingCompany 
									WHEN 91 THEN 15 --FEDEX
									WHEN 88 THEN 43 --UPS
									WHEN 90 THEN 70 --USPS
								 END
	END
	ELSE
	BEGIN
		If @numShippingCompany >= 10 And @numShippingCompany <= 28
		BEGIN
             SET @numShippingCompany = 91
			 SET @vcShippingService = 15
		END
        Else If (@numShippingCompany = 40 Or
                @numShippingCompany = 42 Or
                @numShippingCompany = 43 Or
                @numShippingCompany = 48 Or
                @numShippingCompany = 49 Or
                @numShippingCompany = 50 Or
                @numShippingCompany = 51 Or
                @numShippingCompany = 55) 
		BEGIN
            SET @numShippingCompany = 88
			SET @vcShippingService = 43
		END
        Else If @numShippingCompany >= 70 And @numShippingCompany <= 76
		BEGIN
            SET @numShippingCompany = 90
			SET @vcShippingService = 70
		END
		ELSE
		BEGIN
			SET @numShippingCompany = 91
			SET @vcShippingService = 15
        END
	END

	DECLARE @bitUseBizdocAmount BIT
	DECLARE @numTotalInsuredValue NUMERIC(18,0)
    DECLARE @numTotalCustomsValue NUMERIC(18,2)

	SELECT @bitUseBizdocAmount=bitUseBizdocAmount,@numTotalCustomsValue=ISNULL(numTotalInsuredValue,0),@numTotalInsuredValue=ISNULL(numTotalCustomsValue,0) FROM Domain WHERE numDomainId=@numDomainId

	IF @bitUseBizdocAmount = 1
	BEGIN
		SELECT @numTotalCustomsValue=ISNULL(monDealAmount,0),@numTotalInsuredValue=ISNULL(monDealAmount,0) FROM OpportunityBizDocs WHERE numOppBizDocsId=@numOppBizDocId 
	END

	-- MAKE ENTRY IN ShippingReport TABLE
	INSERT INTO ShippingReport
	(
		numDomainId,numCreatedBy,dtCreateDate,numOppID,numOppBizDocId,numShippingCompany,vcValue2
		,vcFromName,vcFromCompany,vcFromPhone,vcFromAddressLine1,vcFromAddressLine2,vcFromCity,vcFromState,vcFromZip,vcFromCountry,bitFromResidential
		,vcToName,vcToCompany,vcToPhone,vcToAddressLine1,vcToAddressLine2,vcToCity,vcToState,vcToZip,vcToCountry,bitToResidential
		,IsCOD,IsDryIce,IsHoldSaturday,IsHomeDelivery,IsInsideDelevery,IsInsidePickup,IsReturnShipment,IsSaturdayDelivery,IsSaturdayPickup
		,numCODAmount,vcCODType,numTotalInsuredValue,IsAdditionalHandling,IsLargePackage,vcDeliveryConfirmation,vcDescription,numTotalCustomsValue
	)
	VALUES
	(
		@numDomainId,@numUserCntID,GETUTCDATE(),@numOppID,@numOppBizDocId,@numShippingCompany,CAST(@vcShippingService AS VARCHAR)
		,@vcFromName,@vcFromCompany,@vcFromPhone,@vcFromAddressLine1,@vcFromAddressLine2,@vcFromCity,@vcFromState,@vcFromZip,@vcFromCountry,@bitFromResidential
		,@vcToName,@vcToCompany,@vcToPhone,@vcToAddressLine1,@vcToAddressLine2,@vcToCity,@vcToState,@vcToZip,@vcToCountry,@bitToResidential
		,0,0,0,0,0,0,0,0,0
		,0,'',@numTotalInsuredValue,0,0,'','',@numTotalCustomsValue
	)

	SET @numShippingReportID = SCOPE_IDENTITY()

	-- GET CONTAINER BY WAREHOUSE AND NOOFITEMSCANBEADDED
	DECLARE @TEMPContainer TABLE
	(
		ID INT IDENTITY(1,1)
		,numContainer NUMERIC(18,0)
		,numWareHouseID NUMERIC(18,0)
		,numContainerQty NUMERIC(18,0)
		,numNoItemIntoContainer NUMERIC(18,0)
		,numTotalContainer NUMERIC(18,0)
		,bitItemAdded BIT
		,fltWeight FLOAT
		,fltHeight FLOAT
		,fltWidth FLOAT
		,fltLength FLOAT
	)

	INSERT INTO 
		@TEMPContainer
	SELECT
		T1.numContainer
		,T1.numWareHouseID
		,T1.numNoItemIntoContainer
		,T1.numNoItemIntoContainer
		,T2.numTotalContainer
		,0 AS bitItemAdded
		,T2.fltWeight
		,T2.fltHeight
		,T2.fltWidth
		,T2.fltLength
	FROM
	(
		SELECT 
			I.numContainer
			,WI.numWareHouseID
			,I.numNoItemIntoContainer
		FROM 
			OpportunityBizDocItems OBI
		INNER JOIN 
			Item AS I
		ON 
			OBI.numItemCode = I.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OBI.numWarehouseItmsID = WI.numWareHouseItemID
		WHERE
			numOppBizDocID=@numOppBizDocID
			AND I.charItemType ='P'
			AND ISNULL(numContainer,0) > 0
		GROUP BY
			I.numContainer
			,WI.numWareHouseID
			,I.numNoItemIntoContainer
	) AS T1
	INNER JOIN
	(
		SELECT
			I.numItemCode
			,WI.numWareHouseID
			,SUM(OBI.numUnitHour) numTotalContainer
			,ISNULL(I.fltWeight,0) fltWeight
			,ISNULL(I.fltHeight,0) fltHeight
			,ISNULL(I.fltWidth,0) fltWidth
			,ISNULL(I.fltLength,0) fltLength
		FROM 
			OpportunityBizDocItems OBI
		INNER JOIN 
			Item AS I
		ON 
			OBI.numItemCode = I.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OBI.numWarehouseItmsID = WI.numWareHouseItemID
		WHERE
			numOppBizDocID=@numOppBizDocID
			AND I.charItemType ='P'
			AND ISNULL(I.bitContainer,0) = 1
		GROUP BY
			I.numItemCode
			,WI.numWareHouseID
			,ISNULL(I.fltWeight,0)
			,ISNULL(I.fltHeight,0)
			,ISNULL(I.fltWidth,0)
			,ISNULL(I.fltLength,0)
	) AS T2
	ON
		T1.numContainer = T2.numItemCode

	IF (SELECT COUNT(*) FROM @TEMPContainer) = 0
	BEGIN
		RAISERROR('Container(s) are not available in bizdoc.',16,1)
	END
	ELSE
	BEGIN
		DECLARE @TEMPItems TABLE
		(
			ID INT IDENTITY(1,1)
			,numOppBizDocItemID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numWarehouseID NUMERIC(18,0)
			,numUnitHour FLOAT
			,numContainer NUMERIC(18,0)
			,numNoItemIntoContainer NUMERIC(18,0)
			,fltItemWidth FLOAT
			,fltItemHeight FLOAT
			,fltItemLength FLOAT
			,fltItemWeight FLOAT
		)

		INSERT INTO 
			@TEMPItems
		SELECT
			OBDI.numOppBizDocItemID
			,I.numItemCode
			,ISNULL(WI.numWareHouseID,0)
			,ISNULL(OBDI.numUnitHour,0)
			,ISNULL(I.numContainer,0)
			,ISNULL(I.numNoItemIntoContainer,0)
			,ISNULL(fltWidth,0)
			,ISNULL(fltHeight,0)
			,ISNULL(fltLength,0)
			,ISNULL(fltWeight,0)
		FROM
			OpportunityBizDocs OBD
		INNER JOIN
			OpportunityBizDocItems OBDI
		ON
			OBD.numOppBizDocsId=OBDI.numOppBizDocID
		INNER JOIN
			WareHouseItems WI
		ON
			OBDI.numWarehouseItmsID=WI.numWareHouseItemID
		INNER JOIN
			Item I
		ON
			OBDI.numItemCode=I.numItemCode
		WHERE
			numOppId=@numOppID
			AND numOppBizDocsId=@numOppBizDocId
			AND ISNULL(I.numContainer,0) > 0

		DECLARE @i AS INT = 1
		DEClARE @j AS INT = 1
		DECLARE @iCount as INT
		SELECT @iCount = COUNT(*) FROM @TEMPItems 

		DECLARE @numBoxID NUMERIC(18,0)
		DECLARE @numTempQty AS FLOAT
		DECLARE @numTempOppBizDocItemID AS NUMERIC(18,0)
		DECLARE @numTempContainerQty AS NUMERIC(18,0)
		DECLARE @numtempContainer AS NUMERIC(18,0)
		DECLARE @numTempItemCode AS NUMERIC(18,0)
		DECLARE @numTempWarehouseID AS NUMERIC(18,0)
		DECLARE @fltTempItemWidth AS FLOAT
		DECLARE @fltTempItemHeight AS FLOAT
		DECLARE @fltTempItemLength AS FLOAT
		DECLARE @fltTempItemWeight AS FLOAT

		IF ISNULL(@iCount,0) > 0
		BEGIN
			WHILE @i <= @iCount
			BEGIN
				SELECT 
					@numTempOppBizDocItemID=numOppBizDocItemID
					,@numtempContainer=numContainer
					,@numTempItemCode=numItemCode
					,@numTempWarehouseID=numWarehouseID
					,@numTempQty=numUnitHour
					,@numTempContainerQty=numNoItemIntoContainer
					,@fltTempItemWidth=fltItemWidth
					,@fltTempItemHeight=fltItemHeight
					,@fltTempItemLength=fltItemLength
					,@fltTempItemWeight=fltItemWeight
				FROM 
					@TEMPItems 
				WHERE 
					ID=@i

				IF EXISTS (SELECT * FROM @TEMPContainer WHERE numContainer=@numtempContainer AND numWareHouseID=@numTempWarehouseID AND numContainerQty=@numTempContainerQty)
				BEGIN
					DECLARE @numID AS INT
					DECLARE @numTempTotalContainer NUMERIC(18,0)
					DECLARE @numTempMainContainerQty NUMERIC(18,0)
					DECLARE @bitTempItemAdded BIT
					DECLARE @fltTempWeight FLOAT
					DECLARE @fltTempHeight FLOAT
					DECLARE @fltTempWidth FLOAT
					DECLARE @fltTempLength FLOAT

					SELECT
						@numID=ID
						,@numTempTotalContainer=numTotalContainer
						,@numTempMainContainerQty=numNoItemIntoContainer
						,@bitTempItemAdded=bitItemAdded
						,@fltTempWeight=fltWeight
						,@fltTempHeight=fltHeight
						,@fltTempWidth=fltWidth
						,@fltTempLength=fltLength
					FROM
						@TEMPContainer 
					WHERE 
						numContainer=@numtempContainer 
						AND numWareHouseID=@numTempWarehouseID 
						AND numContainerQty=@numTempContainerQty

					WHILE @numTempQty > 0
					BEGIN
						 If @numTempTotalContainer > 0
						 BEGIN
							IF @numTempQty <= @numTempMainContainerQty
							BEGIN
								IF ISNULL(@bitTempItemAdded,0) = 0 OR @numTempMainContainerQty=@numTempContainerQty
								BEGIN
									-- CREATE NEW SHIPING BOX
									INSERT INTO ShippingBox
									(
										vcBoxName
										,numShippingReportId
										,fltTotalWeight
										,fltHeight
										,fltWidth
										,fltLength
										,dtCreateDate
										,numCreatedBy
										,numPackageTypeID
										,numServiceTypeID
										,fltDimensionalWeight
										,numShipCompany
									)
									VALUES
									(
										CONCAT('Box',@j)
										,@numShippingReportID
										,(CASE WHEN ISNULL(@fltTempWeight,0) > CEILING(CAST(@fltTempHeight * @fltTempWidth * @fltTempLength AS FLOAT)/139) THEN @fltTempWeight ELSE CEILING(CAST(@fltTempHeight * @fltTempWidth * @fltTempLength AS FLOAT)/139) END)
										,@fltTempHeight
										,@fltTempWidth
										,@fltTempLength
										,GETUTCDATE()
										,@numUserCntID
										,31
										,@vcShippingService
										,(CASE WHEN ISNULL(@fltTempWeight,0) > CEILING(CAST(@fltTempHeight * @fltTempWidth * @fltTempLength AS FLOAT)/139) THEN @fltTempWeight ELSE CEILING(CAST(@fltTempHeight * @fltTempWidth * @fltTempLength AS FLOAT)/139) END)
										,@numShippingCompany
										
									)

									SELECT @numBoxID = SCOPE_IDENTITY()

									UPDATE @TEMPContainer SET bitItemAdded = 1 WHERE ID=@numID
									
									SET @j = @j + 1
								END
								ELSE
								BEGIN
									SELECT @numBoxID=numBoxID FROM ShippingBox WHERE numShippingReportId=@numShippingReportID AND vcBoxName=CONCAT('Box',@j)
								END

								SET @fltTempItemWeight = (CASE WHEN ISNULL(@fltTempItemWeight,0) > CEILING(CAST(@fltTempItemHeight * @fltTempItemWidth * @fltTempItemLength AS FLOAT)/139) THEN @fltTempItemWeight ELSE CEILING(CAST(@fltTempItemHeight * @fltTempItemWidth * @fltTempItemLength AS FLOAT)/139) END)

								UPDATE ShippingBox SET fltTotalWeight = ISNULL(fltTotalWeight,0) + (@fltTempItemWeight * @numTempQty) WHERE numBoxID=@numBoxID

								-- MAKE ENTRY IN ShippingReportItems Table
								INSERT INTO ShippingReportItems
								(
									numShippingReportId
									,numItemCode
									,tintServiceType
									,monShippingRate
									,fltTotalWeight
									,intNoOfBox
									,fltHeight
									,fltWidth
									,fltLength
									,dtCreateDate
									,numCreatedBy
									,numBoxID
									,numOppBizDocItemID
									,intBoxQty
								)
								VALUES
								(
									@numShippingReportID
									,@numTempItemCode
									,@vcShippingService
									,0
									,@fltTempItemWeight * @numTempQty
									,1
									,@fltTempHeight
									,@fltTempItemWidth
									,@fltTempItemLength
									,GETUTCDATE()
									,@numUserCntID
									,@numBoxID
									,@numTempOppBizDocItemID
									,@numTempQty
								)

								UPDATE @TEMPContainer SET numNoItemIntoContainer=numNoItemIntoContainer - @numTempQty WHERE ID=@numID
								SET @numTempMainContainerQty = @numTempMainContainerQty - @numTempQty
								SET @numTempQty = 0
							END
							ELSE 
							BEGIN
								SELECT @numBoxID=numBoxID FROM ShippingBox WHERE numShippingReportId=@numShippingReportID AND vcBoxName=CONCAT('Box',@j)

								SET @fltTempItemWeight = (CASE WHEN ISNULL(@fltTempItemWeight,0) > CEILING(CAST(@fltTempItemHeight * @fltTempItemWidth * @fltTempItemLength AS FLOAT)/139) THEN @fltTempItemWeight ELSE CEILING(CAST(@fltTempItemHeight * @fltTempItemWidth * @fltTempItemLength AS FLOAT)/139) END)

								UPDATE ShippingBox SET fltTotalWeight = ISNULL(fltTotalWeight,0) + (@fltTempItemWeight * @numTempMainContainerQty) WHERE numBoxID=@numBoxID

								-- MAKE ENTRY IN ShippingReportItems Table
								INSERT INTO ShippingReportItems
								(
									numShippingReportId
									,numItemCode
									,tintServiceType
									,monShippingRate
									,fltTotalWeight
									,intNoOfBox
									,fltHeight
									,fltWidth
									,fltLength
									,dtCreateDate
									,numCreatedBy
									,numBoxID
									,numOppBizDocItemID
									,intBoxQty
								)
								VALUES
								(
									@numShippingReportID
									,@numTempItemCode
									,@vcShippingService
									,0
									,@fltTempItemWeight * @numTempMainContainerQty
									,1
									,@fltTempHeight
									,@fltTempItemWidth
									,@fltTempItemLength
									,GETUTCDATE()
									,@numUserCntID
									,@numBoxID
									,@numTempOppBizDocItemID
									,@numTempMainContainerQty
								)

								UPDATE @TEMPContainer SET numNoItemIntoContainer=@numTempContainerQty,numTotalContainer=numTotalContainer-1 WHERE ID=@numID 

								SET @numTempQty = @numTempQty - @numTempMainContainerQty
							END
						 END
						 ELSE
						 BEGIN
							RAISERROR('Sufficient container(s) are not available.',16,1)
							BREAK
						 END
					END
				END
				ELSE
				BEGIN
					RAISERROR('Sufficient container(s) require for item and warehouse are not available.',16,1)
					BREAK
				END


				SET @i = @i + 1
			END
		END
		ELSE
		BEGIN
			RAISERROR('Inventory item(s) are not available in bizdoc.',16,1)
		END
	END

	SELECT @numShippingReportID
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);

END CATCH
END
/****** Object:  StoredProcedure [dbo].[USP_ShoppingCart]    Script Date: 05/07/2009 22:09:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_shoppingcart')
DROP PROCEDURE usp_shoppingcart
GO
CREATE PROCEDURE [dbo].[USP_ShoppingCart]                            
@numDivID as numeric(18),                            
@numOppID as numeric(18)=null output ,                                              
@vcPOppName as varchar(200)='' output,  
@numContactId as numeric(18),                            
@numDomainId as numeric(18),
@vcSource VARCHAR(50)=NULL,
@numCampainID NUMERIC(9)=0,
@numCurrencyID as numeric(9),
@numBillAddressId numeric(9),
@numShipAddressId numeric(9),
@bitDiscountType BIT,
@fltDiscount FLOAT,
@numProId NUMERIC(9),
@numSiteID NUMERIC(9),
@tintOppStatus INT,
@monShipCost MONEY,
@tintSource AS BIGINT ,
@tintSourceType AS TINYINT ,                          
@numPaymentMethodId NUMERIC(9),
@txtComments varchar(1000),
@numPartner NUMERIC(18,0),
@txtFuturePassword VARCHAR(50) = NULL,
@intUsedShippingCompany NUMERIC(18,0)=0
AS                            
	DECLARE @CRMType AS INTEGER               
	DECLARE @numRecOwner AS INTEGER                            
	
	IF @numOppID=  0                             
	BEGIN                            
		SELECT @CRMType=tintCRMType,@numRecOwner=numRecOwner from DivisionMaster where numDivisionID=@numDivID                            
	
		IF @CRMType= 0                             
		BEGIN                            
			UPDATE DivisionMaster SET tintCRMType=1 WHERE numDivisionID=@numDivID                                        
		END                            
		
		DECLARE @TotalAmount as FLOAT

     

		IF LEN(ISNULL(@txtFuturePassword,'')) > 0
		BEGIN
			DECLARE @numExtranetID NUMERIC(18,0)

			SELECT @numExtranetID=numExtranetID FROM ExtarnetAccounts WHERE numDivisionID=@numDivID

			UPDATE ExtranetAccountsDtl SET vcPassword=@txtFuturePassword WHERE numExtranetID=@numExtranetID
		END                      

		DECLARE @numTemplateID NUMERIC
		SELECT  @numTemplateID= numMirrorBizDocTemplateId FROM eCommercePaymentConfig WHERE  numSiteId=@numSiteID AND numPaymentMethodId = @numPaymentMethodId AND numDomainID = @numDomainID   

		Declare @fltExchangeRate float                                 
    
		IF @numCurrencyID=0 
			SELECT @numCurrencyID=ISNULL(numCurrencyID,0) FROM Domain WHERE numDomainID=@numDomainId
			                       
		IF @numCurrencyID=0 
			SET @fltExchangeRate=1
		ELSE 
			SET @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)                          
                            
  
		-- GET ACCOUNT CLASS IF ENABLED
		DECLARE @numAccountClass AS NUMERIC(18) = 0

		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numContactId
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END

		SET @numAccountClass=(SELECT 
								  TOP 1 
								  numDefaultClass from dbo.eCommerceDTL 
								  where 
								  numSiteId=@numSiteID 
								  and numDomainId=@numDomainId)                  

		insert into OpportunityMaster                              
		  (                              
		  numContactId,                              
		  numDivisionId,                              
		  txtComments,                              
		  numCampainID,                              
		  bitPublicFlag,                              
		  tintSource, 
		  tintSourceType ,                               
		  vcPOppName,                              
		  monPAmount,                              
		  numCreatedBy,                              
		  bintCreatedDate,                               
		  numModifiedBy,                              
		  bintModifiedDate,                              
		  numDomainId,                               
		  tintOppType, 
		  tintOppStatus,                   
		  intPEstimatedCloseDate,              
		  numRecOwner,
		  bitOrder,
		  numCurrencyID,
		  fltExchangeRate,fltDiscountTotal,bitDiscountTypeTotal,
		  monShipCost,
		  numOppBizDocTempID,numAccountClass,numPartner,intUsedShippingCompany
		  )                              
		 Values                              
		  (                              
		  @numContactId,                              
		  @numDivID,                              
		  @txtComments,                              
		  @numCampainID,--  0,
		  0,                              
		  @tintSource,   
		  @tintSourceType,                           
		  ISNULL(@vcPOppName,'SO'),                             
		  0,                                
		  @numContactId,                              
		  getutcdate(),                              
		  @numContactId,                              
		  getutcdate(),        
		  @numDomainId,                              
		  1,             
		  @tintOppStatus,       
		  getutcdate(),                                 
		  @numRecOwner ,
		  1,
		  @numCurrencyID,
		  @fltExchangeRate,@fltDiscount,@bitDiscountType
		  ,@monShipCost,
		  @numTemplateID,@numAccountClass,@numPartner,@intUsedShippingCompany
  
		  )        
		                         
		SET @numOppID=scope_identity()                             

		EXEC dbo.USP_UpdateNameTemplateValue 1,@numDomainId,@numOppID
		SELECT @vcPOppName = vcPOppName FROM OpportunityMaster WHERE numOppID= @numOppID
	END

	DELETE FROM OpportunityItems WHERE numOppId= @numOppID                               

	UPDATE 
		P
	SET
		intCouponCodeUsed=ISNULL(intCouponCodeUsed,0)+1
	FROM 
		PromotionOffer AS P
	LEFT JOIN
		CartItems AS C
	ON
		P.numProId=C.PromotionID
	WHERE
		C.vcCoupon=P.txtCouponCode 
		AND P.numProId = C.PromotionID
		AND ISNULL(C.bitParentPromotion,0)=1
		AND numUserCntId =@numContactId


	INSERT INTO OpportunityItems                                                        
	(
		numOppId
		,numItemCode
		,numUnitHour
		,monPrice
		,monTotAmount
		,vcItemDesc
		,numWarehouseItmsID
		,vcType
		,vcAttributes
		,vcAttrValues
		,[vcItemName]
		,[vcModelID]
		,[vcManufacturer]
		,vcPathForTImage
		,monVendorCost
		,numUOMId
		,bitDiscountType
		,fltDiscount
		,monTotAmtBefDiscount
		,numPromotionID
		,bitPromotionTriggered
		,vcPromotionDetail
	)                                                        
	SELECT 
		@numOppID
		,X.numItemCode
		,x.numUnitHour * x.decUOMConversionFactor
		,x.monPrice/x.decUOMConversionFactor
		,x.monTotAmount
		,X.vcItemDesc
		,NULLIF(X.numWarehouseItmsID,0)
		,X.vcItemType
		,X.vcAttributes
		,X.vcAttrValues
		,(SELECT vcItemName FROM item WHERE numItemCode = X.numItemCode)
		,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode)
		,(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode)
		,(SELECT TOP 1  vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND bitDefault = 1 AND numDomainId = @numDomainId)
		,(SELECT ISNULL(VN.monCost,0) FROM Vendor VN, Item IT WHERE IT.numItemCode=VN.numItemCode AND VN.numItemCode=X.numItemCode AND VN.numVendorID=IT.numVendorID)
		,numUOM
		,bitDiscountType
		,fltDiscount
		,monTotAmtBefDiscount
		,X.PromotionID
		,X.bitParentPromotion
		,x.PromotionDesc
	FROM 
		dbo.CartItems X 
	WHERE 
		numUserCntId =@numContactId

	declare @tintShipped as tinyint      
	DECLARE @tintOppType AS TINYINT
	DECLARE @numStatus AS NUMERIC(18,0)
      
	SELECT 
		@tintOppStatus=tintOppStatus,
		@tintOppType=tintOppType,
		@tintShipped=tintShipped,
		@numStatus=numStatus
	FROM 
		OpportunityMaster 
	WHERE 
		numOppID=@numOppID              
	
	if @tintOppStatus=1               
	begin         
	PRINT 'inside USP_UpdatingInventoryonCloseDeal'
		exec USP_UpdatingInventoryonCloseDeal @numOppID,@numContactId
	end              

                  
	select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@numOppID                            
	update OpportunityMaster set  monPamount=@TotalAmount where numOppId=@numOppID                           
                          
	IF(@vcSource IS NOT NULL)
	BEGIN
		insert into OpportunityLinking ([numParentOppID],[numChildOppID],[vcSource],[numParentOppBizDocID],numPromotionId,numSiteID)  values(null,@numOppID,@vcSource,NULL,@numProId,@numSiteID);
	END

	--Add/Update Address Details
	DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50)      
	DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9) 
	DECLARE @bitIsPrimary BIT;

	--Bill Address
	IF @numBillAddressId>0
	BEGIN
	 SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
			@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
			@vcAddressName=vcAddressName
				FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numBillAddressId 
            
  
  		EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numOppID, --  numeric(9, 0)
		@byteMode = 0, --  tinyint
		@vcStreet = @vcStreet, --  varchar(100)
		@vcCity = @vcCity, --  varchar(50)
		@vcPostalCode = @vcPostalCode, --  varchar(15)
		@numState = @numState, --  numeric(9, 0)
		@numCountry = @numCountry, --  numeric(9, 0)
		@vcCompanyName = '', --  varchar(100)
		@numCompanyId = 0, --  numeric(9, 0)
		@vcAddressName =@vcAddressName
	
        
	END
  
  
	--Ship Address
	IF @numShipAddressId>0
	BEGIN
   		SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
			@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
			@vcAddressName=vcAddressName
				FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numShipAddressId 
  
  
		select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
		join divisionMaster Div                            
		on div.numCompanyID=com.numCompanyID                            
		where div.numdivisionID=@numDivID

  		EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numOppID, --  numeric(9, 0)
		@byteMode = 1, --  tinyint
		@vcStreet = @vcStreet, --  varchar(100)
		@vcCity = @vcCity, --  varchar(50)
		@vcPostalCode = @vcPostalCode, --  varchar(15)
		@numState = @numState, --  numeric(9, 0)
		@numCountry = @numCountry, --  numeric(9, 0)
		@vcCompanyName = @vcCompanyName, --  varchar(100)
		@numCompanyId =@numCompanyId, --  numeric(9, 0)
		@vcAddressName =@vcAddressName
	
   	
	END

	--Insert Tax for Division                       
	INSERT dbo.OpportunityMasterTaxItems 
	(
		numOppId,
		numTaxItemID,
		fltPercentage,
		tintTaxType,
		numTaxID
	) 
	SELECT 
		@numOppID,
		TI.numTaxItemID,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		numTaxID
	FROM 
		TaxItems TI 
	JOIN 
		DivisionTaxTypes DTT 
	ON 
		TI.numTaxItemID = DTT.numTaxItemID
	OUTER APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType,
			numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,TI.numTaxItemID,@numOppId,0,NULL)
	) AS TEMPTax
	WHERE 
		DTT.numDivisionID=@numDivID AND DTT.bitApplicable=1
	UNION 
	SELECT 
		@numOppID,
		0,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		numTaxID
	FROM 
		dbo.DivisionMaster 
	OUTER APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType,
			numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,0,@numOppId,1,NULL)
	) AS TEMPTax
	WHERE 
		bitNoTax=0 
		AND numDivisionID=@numDivID

	-- DELETE ITEM LEVEL CRV TAX TYPES
	DELETE FROM OpportunityMasterTaxItems WHERE numOppId=@numOppId AND numTaxItemID = 1

	-- INSERT ITEM LEVEL CRV TAX TYPES
	INSERT INTO OpportunityMasterTaxItems
	(
		numOppId,
		numTaxItemID,
		fltPercentage,
		tintTaxType,
		numTaxID
	) 
	SELECT 
		OI.numOppId,
		1,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		TEMPTax.numTaxID
	FROM 
		OpportunityItems OI
	INNER JOIN
		ItemTax IT
	ON
		IT.numItemCode = OI.numItemCode
	OUTER APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType,
			numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,OI.numItemCode,IT.numTaxItemID,@numOppId,1,NULL)
	) AS TEMPTax
	WHERE
		OI.numOppId = @numOppID
		AND IT.numTaxItemID = 1 -- CRV TAX
	GROUP BY
		OI.numOppId,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		TEMPTax.numTaxID
  
	--Delete Tax for Opportunity Items if item deleted 
	DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

	--Insert Tax for Opportunity Items
	INSERT INTO dbo.OpportunityItemsTaxItems 
	(
		numOppId,
		numOppItemID,
		numTaxItemID,
		numTaxID
	) 
	SELECT 
		@numOppId,
		OI.numoppitemtCode,
		TI.numTaxItemID,
		0 
	FROM 
		dbo.OpportunityItems OI 
	JOIN 
		dbo.ItemTax IT 
	ON 
		OI.numItemCode=IT.numItemCode 
	JOIN
		TaxItems TI 
	ON 
		TI.numTaxItemID=IT.numTaxItemID 
	WHERE 
		OI.numOppId=@numOppID 
		AND IT.bitApplicable=1 
		AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
	UNION
	SELECT 
		@numOppId,
		OI.numoppitemtCode,
		0,
		0 
	FROM 
		dbo.OpportunityItems OI 
	JOIN 
		dbo.Item I 
	ON 
		OI.numItemCode=I.numItemCode
	WHERE 
		OI.numOppId=@numOppID 
		AND I.bitTaxable=1 
		AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
	UNION
	SELECT
		@numOppId,
		OI.numoppitemtCode,
		1,
		TD.numTaxID
	FROM
		dbo.OpportunityItems OI 
	INNER JOIN
		ItemTax IT
	ON
		IT.numItemCode = OI.numItemCode
	INNER JOIN
		TaxDetails TD
	ON
		TD.numTaxID = IT.numTaxID
		AND TD.numDomainId = @numDomainId
	WHERE
		OI.numOppId = @numOppID
		AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)


	IF @tintOppType=1 AND @tintOppStatus=1 
	BEGIN
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numContactId,@numOppID,@numStatus
	END
  
 
/****** Object:  StoredProcedure [dbo].[USP_UpdatedomainDetails]    Script Date: 05/07/2009 21:56:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatedomaindetails')
DROP PROCEDURE usp_updatedomaindetails
GO
CREATE PROCEDURE [dbo].[USP_UpdatedomainDetails]                                      
@numDomainID as numeric(9)=0,                                      
@vcDomainName as varchar(50)='',                                      
@vcDomainDesc as varchar(100)='',                                      
@bitExchangeIntegration as bit,                                      
@bitAccessExchange as bit,                                      
@vcExchUserName as varchar(100)='',                                      
@vcExchPassword as varchar(100)='',                                      
@vcExchPath as varchar(200)='',                                      
@vcExchDomain as varchar(100)='',                                    
@tintCustomPagingRows as tinyint,                                    
@vcDateFormat as varchar(30)='',                                    
@numDefCountry as numeric(9),                                    
@tintComposeWindow as tinyint,                                    
@sintStartDate as smallint,                                    
@sintNoofDaysInterval as smallint,                                    
@tintAssignToCriteria as tinyint,                                    
@bitIntmedPage as bit,                                    
@tintFiscalStartMonth as tinyint,                                      
@tintPayPeriod as tinyint,                        
@numCurrencyID as numeric(9) ,                
@charUnitSystem as char(1)='',              
@vcPortalLogo as varchar(100)='',            
@tintChrForComSearch as tinyint  ,            
@intPaymentGateWay as int,    
@tintChrForItemSearch as tinyint,
@ShipCompany as numeric(9),
@bitAutoPopulateAddress as bit=null,
@tintPoulateAddressTo as tinyint=null,
@bitMultiCurrency as bit,
@bitCreateInvoice AS tinyint = null,
@numDefaultSalesBizDocID AS NUMERIC(9)=NULL,
@numDefaultPurchaseBizDocID AS NUMERIC(9)=NULL,
@bitSaveCreditCardInfo AS BIT = NULL,
@intLastViewedRecord AS INT=10,
@tintCommissionType AS TINYINT,
@tintBaseTax AS TINYINT,
@bitEmbeddedCost AS BIT,
@numDefaultSalesOppBizDocId NUMERIC=NULL,
@tintSessionTimeOut TINYINT=1,
@tintDecimalPoints TINYINT,
@bitCustomizePortal BIT=0,
@tintBillToForPO TINYINT,
@tintShipToForPO TINYINT,
@tintOrganizationSearchCriteria TINYINT,
@bitDocumentRepositary BIT=0,
@tintComAppliesTo TINYINT=0,
@bitGtoBContact BIT=0,
@bitBtoGContact BIT=0,
@bitGtoBCalendar BIT=0,
@bitBtoGCalendar BIT=0,
@bitExpenseNonInventoryItem BIT=0,
@bitInlineEdit BIT=0,
@bitRemoveVendorPOValidation BIT=0,
@tintBaseTaxOnArea tinyint=0,
@bitAmountPastDue BIT=0,
@monAmountPastDue money,
@bitSearchOrderCustomerHistory BIT=0,
@bitRentalItem as bit=0,
@numRentalItemClass as NUMERIC(9)=NULL,
@numRentalHourlyUOM as NUMERIC(9)=NULL,
@numRentalDailyUOM as NUMERIC(9)=NULL,
@tintRentalPriceBasedOn as tinyint=NULL,
@tintCalendarTimeFormat as tinyint=NULL,
@tintDefaultClassType as tinyint=NULL,
@tintPriceBookDiscount as tinyint=0,
@bitAutoSerialNoAssign as bit=0,
@bitIsShowBalance AS BIT = 0,
@numIncomeAccID NUMERIC(18, 0) = 0,
@numCOGSAccID NUMERIC(18, 0) = 0,
@numAssetAccID NUMERIC(18, 0) = 0,
@IsEnableProjectTracking AS BIT = 0,
@IsEnableCampaignTracking AS BIT = 0,
@IsEnableResourceScheduling AS BIT = 0,
@ShippingServiceItem  AS NUMERIC(9)=0,
@numSOBizDocStatus AS NUMERIC(9)=0,
@bitAutolinkUnappliedPayment AS BIT=0,
@numDiscountServiceItemID AS NUMERIC(9)=0,
@vcShipToPhoneNumber as VARCHAR(50)='',
@vcGAUserEmailId as VARCHAR(50)='',
@vcGAUserPassword as VARCHAR(50)='',
@vcGAUserProfileId as VARCHAR(50)='',
@bitDiscountOnUnitPrice AS BIT=0,
@intShippingImageWidth INT = 0,
@intShippingImageHeight INT = 0,
@numTotalInsuredValue NUMERIC(10,2) = 0,
@numTotalCustomsValue NUMERIC(10,2) = 0,
@vcHideTabs VARCHAR(1000) = '',
@bitUseBizdocAmount BIT = 0,
@bitDefaultRateType BIT = 0,
@bitIncludeTaxAndShippingInCommission  BIT = 0,
@bitPurchaseTaxCredit BIT=0,
@bitLandedCost BIT=0,
@vcLanedCostDefault VARCHAR(10)='',
@bitMinUnitPriceRule BIT = 0,
@numAbovePercent AS NUMERIC(18,2),
@numAbovePriceField AS NUMERIC(18,0),
@numBelowPercent AS NUMERIC(18,2),
@numBelowPriceField AS NUMERIC(18,0),
@numOrderStatusBeforeApproval AS NUMERIC(18,0),
@numOrderStatusAfterApproval AS NUMERIC(18,0),
@vcUnitPriceApprover AS VARCHAR(100),
@numDefaultSalesPricing AS TINYINT,
@bitReOrderPoint AS BIT = 0,
@numReOrderPointOrderStatus AS NUMERIC(18,0) = 0,
@numPODropShipBizDoc AS NUMERIC(18,0) = 0,
@numPODropShipBizDocTemplate AS NUMERIC(18,0) = 0,
@IsEnableDeferredIncome AS BIT = 0,
@bitApprovalforTImeExpense AS BIT=0,
@numCost AS Numeric(9,0)=0,
@intTimeExpApprovalProcess int=0,
@bitApprovalforOpportunity AS BIT=0,
@intOpportunityApprovalProcess int=0,
@numDefaultSalesShippingDoc NUMERIC(18,0)=0,
@numDefaultPurchaseShippingDoc NUMERIC(18,0)=0,
@bitchkOverRideAssignto AS BIT=0,
@vcPrinterIPAddress VARCHAR(15) = '',
@vcPrinterPort VARCHAR(5) = ''
as                                      
                                      
 update Domain                                       
   set                                       
   vcDomainName=@vcDomainName,                                      
   vcDomainDesc=@vcDomainDesc,                                      
   bitExchangeIntegration=@bitExchangeIntegration,                                      
   bitAccessExchange=@bitAccessExchange,                                      
   vcExchUserName=@vcExchUserName,                                      
   vcExchPassword=@vcExchPassword,                                      
   vcExchPath=@vcExchPath,                                      
   vcExchDomain=@vcExchDomain,                                    
   tintCustomPagingRows =@tintCustomPagingRows,                                    
   vcDateFormat=@vcDateFormat,                                    
   numDefCountry =@numDefCountry,                                    
   tintComposeWindow=@tintComposeWindow,                                    
   sintStartDate =@sintStartDate,                                    
   sintNoofDaysInterval=@sintNoofDaysInterval,                                    
   tintAssignToCriteria=@tintAssignToCriteria,                                    
   bitIntmedPage=@bitIntmedPage,                                    
   tintFiscalStartMonth=@tintFiscalStartMonth,                            
--   bitDeferredIncome=@bitDeferredIncome,                          
   tintPayPeriod=@tintPayPeriod,
   vcCurrency=isnull((select varCurrSymbol from Currency Where numCurrencyID=@numCurrencyID  ),'')  ,                      
  numCurrencyID=@numCurrencyID,                
  charUnitSystem= @charUnitSystem,              
  vcPortalLogo=@vcPortalLogo ,            
  tintChrForComSearch=@tintChrForComSearch  ,      
  intPaymentGateWay=@intPaymentGateWay,
 tintChrForItemSearch=@tintChrForItemSearch,
 numShipCompany= @ShipCompany,
 bitAutoPopulateAddress = @bitAutoPopulateAddress,
 tintPoulateAddressTo =@tintPoulateAddressTo,
 --bitMultiCompany=@bitMultiCompany ,
 bitMultiCurrency=@bitMultiCurrency,
 bitCreateInvoice= @bitCreateInvoice,
 [numDefaultSalesBizDocId] =@numDefaultSalesBizDocId,
 bitSaveCreditCardInfo=@bitSaveCreditCardInfo,
 intLastViewedRecord=@intLastViewedRecord,
 [tintCommissionType]=@tintCommissionType,
 [tintBaseTax]=@tintBaseTax,
 [numDefaultPurchaseBizDocId]=@numDefaultPurchaseBizDocID,
 bitEmbeddedCost=@bitEmbeddedCost,
 numDefaultSalesOppBizDocId=@numDefaultSalesOppBizDocId,
 tintSessionTimeOut=@tintSessionTimeOut,
 tintDecimalPoints=@tintDecimalPoints,
 bitCustomizePortal=@bitCustomizePortal,
 tintBillToForPO = @tintBillToForPO,
 tintShipToForPO = @tintShipToForPO,
 tintOrganizationSearchCriteria=@tintOrganizationSearchCriteria,
-- vcPortalName=@vcPortalName,
 bitDocumentRepositary=@bitDocumentRepositary,
 --tintComAppliesTo=@tintComAppliesTo,THIS FIELD IS REMOVED FROM GLOBAL SETTINGS
 bitGtoBContact=@bitGtoBContact,
 bitBtoGContact=@bitBtoGContact,
 bitGtoBCalendar=@bitGtoBCalendar,
 bitBtoGCalendar=@bitBtoGCalendar,
 bitExpenseNonInventoryItem=@bitExpenseNonInventoryItem,
 bitInlineEdit=@bitInlineEdit,
 bitRemoveVendorPOValidation=@bitRemoveVendorPOValidation,
tintBaseTaxOnArea=@tintBaseTaxOnArea,
bitAmountPastDue = @bitAmountPastDue,
monAmountPastDue = @monAmountPastDue,
bitSearchOrderCustomerHistory=@bitSearchOrderCustomerHistory,
bitRentalItem=@bitRentalItem,
numRentalItemClass=@numRentalItemClass,
numRentalHourlyUOM=@numRentalHourlyUOM,
numRentalDailyUOM=@numRentalDailyUOM,
tintRentalPriceBasedOn=@tintRentalPriceBasedOn,
tintCalendarTimeFormat=@tintCalendarTimeFormat,
tintDefaultClassType=@tintDefaultClassType,
tintPriceBookDiscount=@tintPriceBookDiscount,
bitAutoSerialNoAssign=@bitAutoSerialNoAssign,
bitIsShowBalance = @bitIsShowBalance,
numIncomeAccID = @numIncomeAccID,
numCOGSAccID = @numCOGSAccID,
numAssetAccID = @numAssetAccID,
IsEnableProjectTracking = @IsEnableProjectTracking,
IsEnableCampaignTracking = @IsEnableCampaignTracking,
IsEnableResourceScheduling = @IsEnableResourceScheduling,
numShippingServiceItemID = @ShippingServiceItem,
numSOBizDocStatus=@numSOBizDocStatus,
bitAutolinkUnappliedPayment=@bitAutolinkUnappliedPayment,
numDiscountServiceItemID=@numDiscountServiceItemID,
vcShipToPhoneNo = @vcShipToPhoneNumber,
vcGAUserEMail = @vcGAUserEmailId,
vcGAUserPassword = @vcGAUserPassword,
vcGAUserProfileId = @vcGAUserProfileId,
bitDiscountOnUnitPrice=@bitDiscountOnUnitPrice,
intShippingImageWidth = @intShippingImageWidth ,
intShippingImageHeight = @intShippingImageHeight,
numTotalInsuredValue = @numTotalInsuredValue,
numTotalCustomsValue = @numTotalCustomsValue,
vcHideTabs = @vcHideTabs,
bitUseBizdocAmount = @bitUseBizdocAmount,
bitDefaultRateType = @bitDefaultRateType,
bitIncludeTaxAndShippingInCommission = @bitIncludeTaxAndShippingInCommission,
/*,bitAllowPPVariance=@bitAllowPPVariance*/
bitPurchaseTaxCredit=@bitPurchaseTaxCredit,
bitLandedCost=@bitLandedCost,
vcLanedCostDefault=@vcLanedCostDefault,
bitMinUnitPriceRule = @bitMinUnitPriceRule,
numAbovePercent = @numAbovePercent,
numAbovePriceField = @numAbovePriceField,
numBelowPercent = @numBelowPercent,
numBelowPriceField = @numBelowPriceField,
numOrderStatusBeforeApproval = @numOrderStatusBeforeApproval,
numOrderStatusAfterApproval = @numOrderStatusAfterApproval,
numDefaultSalesPricing = @numDefaultSalesPricing,
bitReOrderPoint=@bitReOrderPoint,
numReOrderPointOrderStatus=@numReOrderPointOrderStatus,
numPODropShipBizDoc=@numPODropShipBizDoc,
numPODropShipBizDocTemplate=@numPODropShipBizDocTemplate,
IsEnableDeferredIncome=@IsEnableDeferredIncome,
bitApprovalforTImeExpense=@bitApprovalforTImeExpense,
numCost= @numCost,intTimeExpApprovalProcess=@intTimeExpApprovalProcess,
bitApprovalforOpportunity=@bitApprovalforOpportunity,intOpportunityApprovalProcess=@intOpportunityApprovalProcess,
 numDefaultPurchaseShippingDoc=@numDefaultPurchaseShippingDoc,numDefaultSalesShippingDoc=@numDefaultSalesShippingDoc,
 bitchkOverRideAssignto=@bitchkOverRideAssignto,
 vcPrinterIPAddress=@vcPrinterIPAddress,
 vcPrinterPort=@vcPrinterPort
 where numDomainId=@numDomainID

DELETE FROM UnitPriceApprover WHERE numDomainID = @numDomainID

INSERT INTO UnitPriceApprover
	(
		numDomainID,
		numUserID
	)
SELECT
     @numDomainID,
	 Split.a.value('.', 'VARCHAR(100)') AS String
FROM  
(
SELECT 
        CAST ('<M>' + REPLACE(@vcUnitPriceApprover, ',', '</M><M>') + '</M>' AS XML) AS String  
) AS A 
CROSS APPLY 
	String.nodes ('/M') AS Split(a); 

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WarehouseItems_GetByItemAndExternalLocation')
DROP PROCEDURE dbo.USP_WarehouseItems_GetByItemAndExternalLocation
GO
CREATE PROCEDURE [dbo].[USP_WarehouseItems_GetByItemAndExternalLocation]
@numDomainID NUMERIC(18,0),
@numWareHouseID NUMERIC(18,0),
@numItemCode NUMERIC(18,0)                                             
AS
BEGIN
	SELECT
		numWareHouseID,
		numWareHouseItemID
	FROM
		WareHouseItems
	WHERE
		numDomainID=@numDomainID
		AND numItemID=@numItemCode
		AND numWareHouseID=@numWareHouseID
	ORDER BY
		numWareHouseItemID
END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_WarehouseItems_InventoryTransfer' ) 
    DROP PROCEDURE USP_WarehouseItems_InventoryTransfer
GO
CREATE PROCEDURE [dbo].[USP_WarehouseItems_InventoryTransfer]
@numDomainID NUMERIC(18,0),
@numUserCntID NUMERIC(18,0),
@numFromWarehouseItemID NUMERIC(18,0),
@numToWarehouseItemID	NUMERIC(18,0),
@numQty NUMERIC(18,0),
@strItems as VARCHAR(3000)=''
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @numOnHand FLOAT
	DECLARE @bitSerial BIT
	DECLARE @bitLot BIT
	DECLARE @numItemCode AS NUMERIC(18,0)
	DECLARE @vcTemp AS VARCHAR(MAX) = ''

	SELECT @numOnHand=numOnHand,@numItemCode=numItemID FROM WarehouseItems WHERE numDomainID=@numDomainID AND numWareHouseItemID=@numFromWarehouseItemID 

	IF ISNULL(@numOnHand,0) < @numQty
	BEGIN
		RAISERROR('INSUFFICIENT_ONHAND_QUANTITY',16,1)
	END
	ELSE 
	BEGIN
		-- IF SERIAL/LOT ITEM THAN CHECK SERIAL/LOT NUMBER ARE VALID AND ITS QTY ARE VALID
		SELECT @bitSerial=bitSerialized,@bitLot=bitLotNo FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode
		
		IF @bitSerial = 1 OR @bitLot = 1
		BEGIN
			DECLARE @posComma int, @strKeyVal varchar(20)

			SET @strItems=RTRIM(@strItems)
			IF RIGHT(@strItems, 1)!=',' SET @strItems=@strItems+','

			SET @posComma=PatIndex('%,%', @strItems)
			WHILE @posComma>1
			BEGIN
				SET @strKeyVal=ltrim(rtrim(substring(@strItems, 1, @posComma-1)))
	
				DECLARE @posBStart INT,@posBEnd int, @strQty INT,@strName NUMERIC(18,0)

				IF @bitLot=1 
				BEGIN
					SET @posBStart=PatIndex('%(%', @strKeyVal)
					SET @posBEnd=PatIndex('%)%', @strKeyVal)
					IF( @posBStart>1 AND @posBEnd>1)
					BEGIN
						SET @strName=LTRIM(RTRIM(SUBSTRING(@strKeyVal, 1, @posBStart-1)))
						SET	@strQty=LTRIM(RTRIM(SUBSTRING(@strKeyVal, @posBStart+1,LEN(@strKeyVal)-@posBStart-1)))
					END
					ELSE
					BEGIN
						SET @strName=@strKeyVal
						SET	@strQty=1
					END

					IF EXISTS (SELECT * FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numFromWarehouseItemID AND numWareHouseItmsDTLID=@strName AND numQty >= @strQty)
					BEGIN
						
						UPDATE 
							WareHouseItmsDTL 
						SET 
							numQty = numQty - @strQty,
							@vcTemp = CONCAT(@vcTemp,(CASE WHEN LEN(@vcTemp) = 0 THEN '' ELSE ',' END),ISNULL(vcSerialNo,'') ,'(', @strQty ,')')
						WHERE 
							numWareHouseItemID=@numFromWarehouseItemID 
							AND numWareHouseItmsDTLID=@strName

						IF EXISTS (SELECT * FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numToWarehouseItemID AND numWareHouseItmsDTLID=@strName)
						BEGIN
							UPDATE WareHouseItmsDTL SET numQty = ISNULL(numQty,0) + @strQty WHERE numWareHouseItemID=@numToWarehouseItemID AND numWareHouseItmsDTLID=@strName
						END
						ELSE
						BEGIN
							INSERT INTO WareHouseItmsDTL
							(
								numWareHouseItemID,
								vcSerialNo,
								numQty,
								dExpirationDate,
								bitAddedFromPO
							)
							SELECT
								@numToWarehouseItemID,
								vcSerialNo,
								@strQty,
								dExpirationDate,
								bitAddedFromPO
							FROM 
								WareHouseItmsDTL
							WHERE
								numWareHouseItemID=@numFromWarehouseItemID AND numWareHouseItmsDTLID=@strName
						END
					END
					ELSE
					BEGIN
						RAISERROR('INVALID SERIAL/LOT OR INSUFFICIENT SERIAL/LOT QUANTITY',16,1)
					END
				END
				ELSE
				BEGIN
					SET @strName=@strKeyVal
					SET	@strQty=1

					IF EXISTS (SELECT * FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numFromWarehouseItemID AND numWareHouseItmsDTLID=@strName AND numQty = 1)
					BEGIN
						UPDATE 
							WareHouseItmsDTL 
						SET 
							numWareHouseItemID=@numToWarehouseItemID
							,@vcTemp = @vcTemp + (CASE WHEN LEN(@vcTemp) = 0 THEN '' ELSE ',' END) + ISNULL(vcSerialNo,'') 
						WHERE 
							numWareHouseItemID=@numFromWarehouseItemID 
							AND numWareHouseItmsDTLID=@strName
					END
					ELSE
					BEGIN
						RAISERROR('INVALID SERIAL/LOT OR INSUFFICIENT SERIAL/LOT QUANTITY',16,1)
					END
				END  
	  
				SET @strItems=substring(@strItems, @posComma +1, len(@strItems)-@posComma)
				SET @posComma=PatIndex('%,%',@strItems)
			END
		END

		DECLARE @description VARCHAR(MAX) = ''

		IF @bitSerial = 1 OR @bitLot = 1
		BEGIN
			SET @description = 'Inventory Transfer - Transferred (Qty:' + CAST(@numQty AS VARCHAR(10)) + ', Serial/Lot#:' + @vcTemp + ')'
		END
		ELSE
		BEGIN
			SET @description = 'Inventory Transfer - Transferred (Qty:' + CAST(@numQty AS VARCHAR(10)) + ')'
		END

		UPDATE
			WarehouseItems
		SET
			numOnHand = ISNULL(numOnHand,0) - @numQty
			,dtModified = GETDATE() 
		WHERE
			numWareHouseItemID=@numFromWarehouseItemID

		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numFromWarehouseItemID,
			@numReferenceID = @numItemCode,
			@tintRefType = 1,
			@vcDescription = @description,
			@numModifiedBy = @numUserCntID,
			@numDomainID = @numDomainID


		IF @bitSerial = 1 OR @bitLot = 1
		BEGIN
			SET @description = 'Inventory Transfer - Receieved (Qty:' + CAST(@numQty AS VARCHAR(10)) + ', Serial/Lot#:' + @vcTemp + ')'
		END
		ELSE
		BEGIN
			SET @description = 'Inventory Transfer - Receieved (Qty:' + CAST(@numQty AS VARCHAR(10)) + ')'
		END

		UPDATE
			WarehouseItems
		SET
			numOnHand = (CASE WHEN numBackOrder >= @numQty THEN numOnHand ELSE ISNULL(numOnHand,0) + (@numQty - ISNULL(numBackOrder,0)) END) 
			,numAllocation = (CASE WHEN numBackOrder >= @numQty THEN ISNULL(numAllocation,0) + @numQty ELSE ISNULL(numAllocation,0) + ISNULL(numBackOrder,0) END) 
			,numBackOrder = (CASE WHEN numBackOrder >= @numQty THEN ISNULL(numBackOrder,0) - @numQty ELSE 0 END) 
			,dtModified = GETDATE() 
		WHERE
			numWareHouseItemID=@numToWarehouseItemID

		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numToWarehouseItemID,
			@numReferenceID = @numItemCode,
			@tintRefType = 1,
			@vcDescription = @description,
			@numModifiedBy = @numUserCntID,
			@numDomainID = @numDomainID

	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WFConditionQueryExecute')
DROP PROCEDURE USP_WFConditionQueryExecute
GO
CREATE PROCEDURE [dbo].[USP_WFConditionQueryExecute]     
    @numDomainID numeric(18, 0),
    @numRecordID numeric(18, 0),
    @textQuery NTEXT,
    @boolCondition BIT OUTPUT,
    @tintMode AS TINYINT
as                 

IF @tintMode=1 --Condition Check
BEGIN
	DECLARE @TotalRecords AS int
	
	EXEC sp_executesql @textQuery,N'@numDomainID numeric(18, 0),@numRecordID numeric(18,0),@TotalRecords int OUTPUT',
	@numDomainID=@numDomainID,@numRecordID=@numRecordID,@TotalRecords=@TotalRecords OUTPUT

	SET @boolCondition = CASE WHEN @TotalRecords=0 THEN 0 ELSE 1 END	
END
ELSE IF @tintMode=2 --Field Update
BEGIN
	EXEC sp_executesql @textQuery,N'@numDomainID numeric(18, 0),@numRecordID numeric(18,0)',
	@numDomainID=@numDomainID,@numRecordID=@numRecordID

	If CHARINDEX('OpportunityMaster',@textQuery) > 0 AND CHARINDEX('numStatus',@textQuery) > 0
	BEGIN
		DECLARE @numOppType TINYINT
		DECLARE @tintOppStatus TINYINT
		DECLARE @numStatus NUMERIC(18,0)

		SELECT
			@numOppType=tintOppType
			,@tintOppStatus=tintOppStatus
			,@numStatus=@numStatus
		FROM
			OpportunityMaster
		WHERE
			numDomainId=@numDomainID
			AND numOppId=@numRecordID


		IF @numOppType=1 AND @tintOppStatus=1
		BEGIN
			EXEC USP_SalesFulfillmentQueue_Save @numDomainID,0,@numRecordID,@numStatus
		END
	END
END
