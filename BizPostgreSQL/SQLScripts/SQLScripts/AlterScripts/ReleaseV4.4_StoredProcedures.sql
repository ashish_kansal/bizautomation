/******************************************************************
Project: Release 4.4 Date: 31.March.2015
Comments: STORED PROCEDURES
*******************************************************************/

-------------------- STORE PROCEDURES -------------------------

/****** Object:  StoredProcedure [dbo].[USP_AdvancedSearch]    Script Date: 05/07/2009 22:04:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_advancedsearch')
DROP PROCEDURE usp_advancedsearch
GO
CREATE PROCEDURE [dbo].[USP_AdvancedSearch] 
@WhereCondition as varchar(4000)='',                              
@ViewID as tinyint,                              
@AreasofInt as varchar(100)='',                              
@tintUserRightType as tinyint,                              
@numDomainID as numeric(9)=0,                              
@numUserCntID as numeric(9)=0,                              
@numGroupID as numeric(9)=0,                              
@CurrentPage int,                                                                    
@PageSize int,                                                                    
@TotRecs int output,                                                                                                             
@columnSortOrder as Varchar(10),                              
@ColumnSearch as varchar(100)='',                           
@ColumnName as varchar(20)='',                             
@GetAll as bit,                              
@SortCharacter as char(1),                        
@SortColumnName as varchar(20)='',        
@bitMassUpdate as bit,        
@LookTable as varchar(20)='' ,        
@strMassUpdate as varchar(2000)=''                             
as                              
DECLARE  @tintOrder  AS TINYINT
DECLARE  @vcFormFieldName  AS VARCHAR(50)
DECLARE  @vcListItemType  AS VARCHAR(3)
DECLARE  @vcListItemType1  AS VARCHAR(3)
DECLARE  @vcAssociatedControlType VARCHAR(10)
DECLARE  @numListID  AS NUMERIC(9)
DECLARE  @vcDbColumnName VARCHAR(20)
DECLARE  @vcLookBackTableName VARCHAR(50)
DECLARE  @WCondition VARCHAR(1000)
DECLARE @bitContactAddressJoinAdded AS BIT;
DECLARE @bitBillAddressJoinAdded AS BIT;
DECLARE @bitShipAddressJoinAdded AS BIT;

Declare @zipCodeSearch as varchar(100);SET @zipCodeSearch=''


if CHARINDEX('$SZ$', @WhereCondition)>0
BEGIN

select @zipCodeSearch=SUBSTRING(@WhereCondition, CHARINDEX('$SZ$', @WhereCondition)+4,CHARINDEX('$EZ$', @WhereCondition) - CHARINDEX('$SZ$', @WhereCondition)-4) 

CREATE TABLE #tempAddress(
	[numAddressID] [numeric](18, 0) NOT NULL,
	[vcAddressName] [varchar](50) NULL,
	[vcStreet] [varchar](100) NULL,
	[vcCity] [varchar](50) NULL,
	[vcPostalCode] [varchar](15) NULL,
	[numState] [numeric](18, 0) NULL,
	[numCountry] [numeric](18, 0) NULL,
	[bitIsPrimary] [bit] NOT NULL CONSTRAINT [DF_AddressMaster_bitIsPrimary]  DEFAULT ((0)),
	[tintAddressOf] [tinyint] NOT NULL,
	[tintAddressType] [tinyint] NOT NULL CONSTRAINT [DF_AddressMaster_tintAddressType]  DEFAULT ((0)),
	[numRecordID] [numeric](18, 0) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL)

IF Len(@zipCodeSearch)>0
BEGIN
	
	Declare @Strtemp as nvarchar(500);
	SET @Strtemp='insert into #tempAddress select * from AddressDetails  where numDomainID=@numDomainID1  
					and vcPostalCode in (SELECT REPLICATE(''0'', 5 - LEN(ZipCode)) + ZipCode  FROM ' + @zipCodeSearch +')';
	
	PRINT '@Strtemp: ' + @Strtemp
	EXEC sp_executesql @Strtemp,N'@numDomainID1 numeric(18)',@numDomainID1=@numDomainID

SET @WhereCondition = REPLACE(@WhereCondition,'$SZ$' + @zipCodeSearch + '$EZ$',
' (ADC.numContactID in (select  numRecordID from #tempAddress where tintAddressOf=1 AND tintAddressType=0)   
or (DM.numDivisionID in (select  numRecordID from #tempAddress where tintAddressOf=2 AND tintAddressType in(1,2))))')
END
END

SET @WCondition = ''
   
IF (@SortCharacter <> '0' AND @SortCharacter <> '')
  SET @ColumnSearch = @SortCharacter
CREATE TABLE #temptable (
  id           INT   IDENTITY   PRIMARY KEY,
  numcontactid VARCHAR(15))


---------------------Find Duplicate Code Start-----------------------------
DECLARE  @Sql           VARCHAR(8000),
         @numMaxFieldId NUMERIC(9),
         @Where         NVARCHAR(1000),
         @OrderBy       NVARCHAR(1000),
         @GroupBy       NVARCHAR(1000),
         @SelctFields   NVARCHAR(4000),
         @InneJoinOn    NVARCHAR(1000)
SET @InneJoinOn = ''
SET @OrderBy = ''
SET @Sql = ''
SET @Where = ''
SET @GroupBy = ''
SET @SelctFields = ''
    
IF (@WhereCondition = 'DUP' OR @WhereCondition = 'DUP-PRIMARY')
BEGIN
	SELECT @numMaxFieldId= MAX([numFieldID]) FROM View_DynamicColumns
	WHERE [numFormID] =24 AND [numDomainId] = @numDomainID
	--PRINT @numMaxFieldId
	WHILE @numMaxFieldId > 0
	BEGIN
		SELECT @vcFormFieldName=[vcFieldName],@vcDbColumnName=[vcDbColumnName],@vcLookBackTableName=[vcLookBackTableName] 
		FROM View_DynamicColumns 
		WHERE [numFieldID] = @numMaxFieldId and [numFormID] =24 AND [numDomainId] = @numDomainID
		--	PRINT @vcDbColumnName
			SET @SelctFields = @SelctFields +  @vcDbColumnName + ','
			IF(@vcLookBackTableName ='AdditionalContactsInformation')
			BEGIN
				SET @InneJoinOn = @InneJoinOn + 'ADC.'+ @vcDbColumnName +' = a1.' + @vcDbColumnName + ' and '
				SET @GroupBy = @GroupBy + 'ADC.'+  @vcDbColumnName + ','
			END
			ELSE IF (@vcLookBackTableName ='DivisionMaster')
			BEGIN
				SET @InneJoinOn = @InneJoinOn + 'DM.'+ @vcDbColumnName +' = a1.' + @vcDbColumnName + ' and '
				SET @GroupBy = @GroupBy + 'DM.'+  @vcDbColumnName + ','
			END
			ELSE
			BEGIN
				SET @InneJoinOn = @InneJoinOn + 'C.' + @vcDbColumnName +' = a1.' + @vcDbColumnName + ' and '
				SET @GroupBy = @GroupBy + 'C.' + @vcDbColumnName + ','
			END
			SET @Where = @Where + 'and LEN(' + @vcDbColumnName + ')>0 '

	
		SELECT @numMaxFieldId = MAX([numFieldID]) FROM View_DynamicColumns 
		WHERE [numFormID] =24 AND [numDomainId] = @numDomainID AND [numFieldID] < @numMaxFieldId
	END
			
			IF(LEN(@Where)>2)
			BEGIN
				SET @Where =RIGHT(@Where,LEN(@Where)-3)
				SET @OrderBy =  LEFT(@GroupBy,(Len(@GroupBy)- 1))
				SET @GroupBy =  LEFT(@GroupBy,(Len(@GroupBy)- 1))
				SET @InneJoinOn = LEFT(@InneJoinOn,LEN(@InneJoinOn)-3)
				SET @SelctFields = LEFT(@SelctFields,(Len(@SelctFields)- 1))
				SET @Sql = @Sql + ' INSERT INTO [#tempTable] ([numContactID])SELECT  ADC.numContactId FROM additionalcontactsinformation ADC JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId  
					JOIN (SELECT   '+ @SelctFields + '
				   FROM  additionalcontactsinformation ADC JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId  
				   WHERE  ' + @Where + ' AND C.[numDomainID] = '+convert(varchar(15),@numDomainID)+ ' AND ADC.[numDomainID] = '+convert(varchar(15),@numDomainID)+ ' AND DM.[numDomainID] = '+convert(varchar(15),@numDomainID)+ 
				   ' GROUP BY ' + @GroupBy + '
				   HAVING   COUNT(* ) > 1) a1
					ON  '+ @InneJoinOn + ' where ADC.[numDomainID] = '+convert(varchar(15),@numDomainID) + ' and DM.[numDomainID] = '+convert(varchar(15),@numDomainID) + ' ORDER BY '+ @OrderBy
			END	
			ELSE
			BEGIN
				SET @Where = ' 1=0 '
				SET @Sql = @Sql + ' INSERT INTO [#tempTable] ([numContactID])SELECT  ADC.numContactId FROM additionalcontactsinformation ADC  '
							+ ' Where ' + @Where
			END 	
		   
		   PRINT '@Sql: ' + @Sql
		   EXEC( @Sql)
		   SET @Sql = ''
		   SET @GroupBy = ''
		   SET @SelctFields = ''
		   SET @Where = ''
	
	IF (@WhereCondition = 'DUP-PRIMARY')
      BEGIN
        SET @WCondition = ' AND ADC.numContactId IN (SELECT DISTINCT numContactId FROM #tempTable) and ISNULL(ADC.bitPrimaryContact,0)=1'
      END
    ELSE
      BEGIN
        SET @WCondition = ' AND ADC.numContactId IN (SELECT DISTINCT numContactId FROM #tempTable)'
      END
    
	SET @Sql = ''
	SET @WhereCondition =''
	SET @Where= 'DUP' 
END
-----------------------------End Find Duplicate ------------------------

DECLARE @strSql AS VARCHAR(8000)                                                              
SET @strSql = 'Select ADC.numContactId                    
  FROM AdditionalContactsInformation ADC                                                   
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                  
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId'   

IF @SortColumnName='vcStreet' OR @SortColumnName='vcCity' OR @SortColumnName='vcPostalCode' OR @SortColumnName ='numCountry' OR @SortColumnName ='numState'
   OR @ColumnName='vcStreet' OR @ColumnName='vcCity' OR @ColumnName='vcPostalCode' OR @ColumnName ='numCountry' OR @ColumnName ='numState'	
BEGIN 
	SET @strSql= @strSql +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
END 
ELSE IF @SortColumnName='vcBillStreet' OR @SortColumnName='vcBillCity' OR @SortColumnName='vcBillPostCode' OR @SortColumnName ='numBillCountry' OR @SortColumnName ='numBillState'
     OR @ColumnName='vcBillStreet' OR @ColumnName='vcBillCity' OR @ColumnName='vcBillPostCode' OR @ColumnName ='numBillCountry' OR @ColumnName ='numBillState'
BEGIN 	
	SET @strSql= @strSql +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
END	
ELSE IF @SortColumnName='vcShipStreet' OR @SortColumnName='vcShipCity' OR @SortColumnName='vcShipPostCode' OR @SortColumnName ='numShipCountry' OR @SortColumnName ='numShipState'
     OR @ColumnName='vcShipStreet' OR @ColumnName='vcShipCity' OR @ColumnName='vcShipPostCode' OR @ColumnName ='numShipCountry' OR @ColumnName ='numShipState'
BEGIN
	SET @strSql= @strSql +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
END	
                         
  IF (@ColumnName<>'' and  @ColumnSearch<>'')                        
  BEGIN                          
  SELECT @vcListItemType=vcListItemType,@numListID=numListID FROM View_DynamicDefaultColumns WHERE vcDbColumnName=@ColumnName and numFormID=1 and numDomainId=@numDomainId                         
    IF @vcListItemType='LI'                               
    BEGIN                      
		IF @numListID=40--Country
		  BEGIN
			IF @ColumnName ='numCountry'
			BEGIN
					SET @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD.numCountry' 
			END
			ELSE IF @ColumnName ='numBillCountry'
			BEGIN
					SET @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD1.numCountry' 
			END
			ELSE IF @ColumnName ='numShipCountry'
			BEGIN
					SET @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD2.numCountry' 
			END
		  END                        
		  ELSE
		  BEGIN
					SET @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID='+@ColumnName                              
		  END
		            
    END                              
    ELSE IF @vcListItemType='S'                               
    BEGIN
       IF @ColumnName ='numState'
		BEGIN
   			SET @strSql= @strSql +' left join State S1 on S1.numStateID=AD.numState'                            
		END
		ELSE IF @ColumnName ='numBillState'
		BEGIN
   			SET @strSql= @strSql +' left join State S1 on S1.numStateID=AD1.numState'                            
		END
	    ELSE IF @ColumnName ='numShipState'
	    BEGIN
   			SET @strSql= @strSql +' left join State S1 on S1.numStateID=AD2.numState'                           
		END
    END                              
    ELSE IF @vcListItemType='T'                               
    BEGIN
      SET @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=' + @ColumnName                             
    END                         
   END      
           
   IF (@SortColumnName<>'')                        
  BEGIN                          
  SELECT @vcListItemType1=vcListItemType,@numListID=numListID FROM View_DynamicDefaultColumns WHERE vcDbColumnName=@SortColumnName and numFormID=1 and numDomainId=@numDomainId                         
    IF @vcListItemType1='LI'                   
    BEGIN     
    IF @numListID=40--Country
		  BEGIN
			IF @SortColumnName ='numCountry'
			BEGIN
					SET @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD.numCountry' 
			END
			ELSE IF @SortColumnName ='numBillCountry'
			BEGIN
					SET @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD1.numCountry' 
			END
			ELSE IF @SortColumnName ='numShipCountry'
			BEGIN
					SET @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD2.numCountry' 
			END
		  END                        
		  ELSE
		  BEGIN
				SET @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                              
		  END
    END                              
    ELSE IF @vcListItemType1='S'           
    BEGIN               
        IF @SortColumnName ='numState'
		BEGIN
   			SET @strSql= @strSql +' left join State S2 on S2.numStateID=AD.numState'                            
		END
		ELSE IF @SortColumnName ='numBillState'
		BEGIN
   			SET @strSql= @strSql +' left join State S2 on S2.numStateID=AD1.numState'                            
		END
	    ELSE IF @SortColumnName ='numShipState'
	    BEGIN
   			SET @strSql= @strSql +' left join State S2 on S2.numStateID=AD2.numState'                           
		END
    END                              
    ELSE IF @vcListItemType1='T'                  
    BEGIN                            
      SET @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                               
    END                         
  END                        
  SET @strSql=@strSql+' where DM.numDomainID  = ' + CONVERT(VARCHAR(15),@numDomainID)+   @WhereCondition                                  
                 
--if @tintUserRightType=1 set @strSql=@strSql + ' AND (ADC.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ')'                                       
--else if @tintUserRightType=2 set @strSql=@strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or DM.numTerID=0) '
                        
IF @AreasofInt<>'' SET @strSql=@strSql+ ' and ADC.numContactID in (select numContactID from  AOIContactLink where numAOIID in('+ @AreasofInt+'))'                                 
IF (@ColumnName<>'' AND @ColumnSearch<>'')                         
       BEGIN     
  IF @vcListItemType='LI'                               
    BEGIN 
      SET @strSql= @strSql +' and L1.vcData like '''+@ColumnSearch +'%'''                             
    END                             
    ELSE IF @vcListItemType='S'                               
    BEGIN                                 
      SET @strSql= @strSql +' and S1.vcState like '''+@ColumnSearch+'%'''                          
    END
    ELSE IF @vcListItemType='T'                               
    BEGIN
      SET @strSql= @strSql +' and T1.vcTerName like '''+@ColumnSearch  +'%'''                            
    END
    ELSE
       BEGIN     
		   IF @ColumnName='vcStreet' OR @ColumnName='vcBillStreet' OR @ColumnName='vcShipStreet'
				SET @strSql= @strSql +' and vcStreet like '''+@ColumnSearch  +'%'''                            
		   ELSE IF @ColumnName='vcCity' OR @ColumnName='vcBillCity' OR @ColumnName='vcShipCity'
				SET @strSql= @strSql +' and vcCity like '''+@ColumnSearch  +'%'''                            
		   ELSE IF @ColumnName='vcPostalCode' OR @ColumnName='vcBillPostCode' OR @ColumnName='vcShipPostCode'
				SET @strSql= @strSql +' and vcPostalCode like '''+@ColumnSearch  +'%'''                            
		   ELSE
				SET @strSql= @strSql +' and '+@ColumnName+' like '''+@ColumnSearch  +'%'''                            
        END   
                          
END
  IF @SortColumnName<>''
       BEGIN     
    IF @vcListItemType1 = 'LI'
		BEGIN                   
		  SET @strSql= @strSql +' order by L2.vcData '+@columnSortOrder                              
		END                            
    ELSE IF @vcListItemType1='S'                               
		BEGIN                                  
		  SET @strSql= @strSql +' order by S2.vcState '+@columnSortOrder                              
		END
    ELSE IF @vcListItemType1='T'                               
		BEGIN                              
		  SET @strSql= @strSql +' order by T2.vcTerName '+@columnSortOrder                              
		END
    ELSE 
       BEGIN     
		   IF @SortColumnName='vcStreet' OR @SortColumnName='vcBillStreet' OR @SortColumnName='vcShipStreet'
				SET @strSql= @strSql +' order by vcStreet ' +@columnSortOrder                            
		   ELSE IF @SortColumnName='vcCity' OR @SortColumnName='vcBillCity' OR @SortColumnName='vcShipCity'
				SET @strSql= @strSql +' order by vcCity ' +@columnSortOrder                            
		   ELSE IF @SortColumnName='vcPostalCode' OR @SortColumnName='vcBillPostCode' OR @SortColumnName='vcShipPostCode'
				SET @strSql= @strSql +' order by vcPostalCode ' +@columnSortOrder
		   ELSE IF @SortColumnName='numRecOwner'
				SET @strSql= @strSql + ' order by dbo.fn_GetContactName(ADC.numRecOwner) ' +@columnSortOrder	                            
		   ELSE                      
				SET @strSql= @strSql +' order by '+ @SortColumnName +' ' +@columnSortOrder                            
        END  
                                
  END
  ELSE 
	  SET @strSql= @strSql +' order by  ADC.numContactID asc '

	  PRINT '@Where:' + @Where

IF(@Where <>'DUP')
BEGIN
    PRINT ' FROM WHERE CONDITION OF "DUP" @Sql: ' + @Sql

	INSERT INTO #temptable (numcontactid)
	EXEC( @strSql)
	
	SET @strSql = ''
END

SET @bitBillAddressJoinAdded=0;
SET @bitShipAddressJoinAdded=0;
SET @bitContactAddressJoinAdded=0;
set @tintOrder=0;
set @WhereCondition ='';
set @strSql='select ADC.numContactId,DM.numDivisionID,DM.numTerID,ADC.numRecOwner,DM.tintCRMType '                              
DECLARE @Prefix AS VARCHAR(5)
DECLARE @Table AS VARCHAR(200)

SELECT TOP 1 @tintOrder=A.tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFormFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@Table=vcLookBackTableName
FROM AdvSerViewConf A                              
JOIN View_DynamicDefaultColumns D                               
ON D.numFieldId=A.numFormFieldId   and d.numformId = 1                           
WHERE D.numDomainID = @numDomainID AND D.numFormID = 1 AND A.numFormID = 1  AND tintViewID=@ViewID and numGroupID=@numGroupID and A.numDomainID=@numDomainID 
AND d.vcDbColumnName NOT IN ('numBillShipCountry','numBillShipState') ORDER BY A.tintOrder ASC 
	
WHILE @tintOrder>0                              
BEGIN                          
    IF @Table = 'AdditionalContactsInformation' 
        SET @Prefix = 'ADC.'
    IF @Table = 'DivisionMaster' 
        SET @Prefix = 'DM.'
                               
IF @vcAssociatedControlType = 'SelectBox'                              
BEGIN                                              
	  IF @vcListItemType='LI'                               
	  BEGIN    
		  IF @numListID=40--Country
		  BEGIN
	  		SET @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                              
			IF @vcDbColumnName ='numCountry'
			BEGIN
				IF @bitContactAddressJoinAdded=0 
			  BEGIN
					SET @WhereCondition= @WhereCondition +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
					SET @bitContactAddressJoinAdded=1;
			  END
			  SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ CONVERT(VARCHAR(3),@tintOrder)+ ' on L'+CONVERT(VARCHAR(3),@tintOrder)+ '.numListItemID=AD.numCountry'	
			END
			ELSE IF @vcDbColumnName ='numBillCountry'
			BEGIN
				IF @bitBillAddressJoinAdded=0 
				BEGIN
					SET @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
					
					SET @bitBillAddressJoinAdded=1;
				END
				SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+  CONVERT(VARCHAR(3),@tintOrder)+ ' on L' +  CONVERT(VARCHAR(3),@tintOrder)+ '.numListItemID=AD1.numCountry'
			END
			ELSE IF @vcDbColumnName ='numShipCountry'
			BEGIN
				IF @bitShipAddressJoinAdded=0 
				BEGIN
					SET @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
					
					SET @bitShipAddressJoinAdded=1;
				END
				SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+  CONVERT(VARCHAR(3),@tintOrder)+ ' on L' +  CONVERT(VARCHAR(3),@tintOrder)+ '.numListItemID=AD2.numCountry'
			END
		  END                        
		  ELSE
		  BEGIN
				SET @strSql=@strSql+',L' +  CONVERT(VARCHAR(3),@tintOrder)+'.vcData'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                              
				SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+  CONVERT(VARCHAR(3),@tintOrder)+ ' on L' +  CONVERT(VARCHAR(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName	
		  END
	  end                              
	                      
	  ELSE IF @vcListItemType='T'                               
	  BEGIN
		  SET @strSql=@strSql+',L'+  CONVERT(VARCHAR(3),@tintOrder)+'.vcData'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                              
		  SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+  CONVERT(VARCHAR(3),@tintOrder)+ ' on L' +  CONVERT(VARCHAR(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                              
	  END  
	  ELSE IF @vcListItemType='U'                                                     
		BEGIN           
			SET @strSql=@strSql+',dbo.fn_GetContactName('+ @Prefix + @vcDbColumnName+') ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'
		END
	
end  
ELSE IF @vcAssociatedControlType='ListBox'   
BEGIN
	IF @vcListItemType='S'                               
	  BEGIN
		SET @strSql=@strSql+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'
		IF @vcDbColumnName ='numState'
		BEGIN
			IF @bitContactAddressJoinAdded=0 
			   BEGIN
					SET @WhereCondition= @WhereCondition +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
					SET @bitContactAddressJoinAdded=1;
			  END	
			  SET @WhereCondition= @WhereCondition +' left join State S'+  CONVERT(VARCHAR(3),@tintOrder)+ ' on S' +  CONVERT(VARCHAR(3),@tintOrder)+ '.numStateID=AD.numState'
		END
		ELSE IF @vcDbColumnName ='numBillState'
		BEGIN
			IF @bitBillAddressJoinAdded=0 
			BEGIN
				SET @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
				SET @bitBillAddressJoinAdded=1;
			END
			SET @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S' +  CONVERT(VARCHAR(3),@tintOrder)+ '.numStateID=AD1.numState'
		END
	    ELSE IF @vcDbColumnName ='numShipState'
	    BEGIN
			IF @bitShipAddressJoinAdded=0 
			BEGIN
				SET @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
				SET @bitShipAddressJoinAdded=1;
			END
			SET @WhereCondition= @WhereCondition +' left join State S'+  CONVERT(VARCHAR(3),@tintOrder)+ ' on S' +  CONVERT(VARCHAR(3),@tintOrder)+ '.numStateID=AD2.numState'
		END
	  end         
END 
ELSE IF @vcAssociatedControlType='TextBox'   
BEGIN
	IF @vcDbColumnName='vcStreet' OR @vcDbColumnName='vcCity' OR @vcDbColumnName='vcPostalCode'
	BEGIN                              
	   SET @strSql=@strSql+',AD.'+ @vcDbColumnName+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                              
	   IF @bitContactAddressJoinAdded=0 
	   BEGIN
			SET @WhereCondition= @WhereCondition +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
			SET @bitContactAddressJoinAdded=1;
	   END
	end 
	ELSE IF @vcDbColumnName='vcBillStreet' OR @vcDbColumnName='vcBillCity' OR @vcDbColumnName='vcBillPostCode'
	BEGIN                              
	   SET @strSql=@strSql+',AD1.'+ REPLACE(REPLACE(@vcDbColumnName,'Bill',''),'PostCode','PostalCode') +' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                              
	   IF @bitBillAddressJoinAdded=0 
	   BEGIN 
	   		SET @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
	   		SET @bitBillAddressJoinAdded=1;
	   END
	END 
	ELSE IF @vcDbColumnName='vcShipStreet' OR @vcDbColumnName='vcShipCity' OR @vcDbColumnName='vcShipPostCode'
	BEGIN                              
	   SET @strSql=@strSql+',AD2.'+ REPLACE(REPLACE(@vcDbColumnName,'Ship',''),'PostCode','PostalCode') +' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                              
	   IF @bitShipAddressJoinAdded=0 
	   BEGIN
	   		SET @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2  AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
	   		SET @bitShipAddressJoinAdded=1;
	   END
	END 
	ELSE
	BEGIN
		SET @strSql=@strSql+','+ CASE WHEN @vcDbColumnName='numAge' THEN 'year(getutcdate())-year(bintDOB)' WHEN @vcDbColumnName='numDivisionID' THEN 'DM.numDivisionID' WHEN @vcDbColumnName='bintCreatedDate' THEN 'dbo.[FormatedDateFromDate](DM.bintCreatedDate,DM.numDomainID)' ELSE @vcDbColumnName END +' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'		
	END
END                       
ELSE SET @strSql=@strSql+','+ CASE WHEN @vcDbColumnName='numAge' THEN 'year(getutcdate())-year(bintDOB)' WHEN @vcDbColumnName='numDivisionID' THEN 'DM.numDivisionID' WHEN @vcDbColumnName='bintCreatedDate' THEN 'dbo.[FormatedDateFromDate](DM.bintCreatedDate,DM.numDomainID)' ELSE @vcDbColumnName END + ' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'
		
		
  
        
SELECT TOP 1 @tintOrder=A.tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFormFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@Table=vcLookBackTableName
FROM AdvSerViewConf A                              
JOIN View_DynamicDefaultColumns D                               
ON D.numFieldId=A.numFormFieldId   and d.numformId = 1                           
WHERE D.numDomainID = @numDomainID AND D.numFormID = 1 AND A.numFormID = 1 AND tintViewID=@ViewID AND numGroupID=@numGroupID AND A.numDomainID=@numDomainID 
AND A.tintOrder > @tintOrder-1
AND d.vcDbColumnName NOT IN ('numBillShipCountry','numBillShipState') ORDER BY A.tintOrder ASC
  
IF @@rowcount=0 SET @tintOrder=0                              
END              
                                      

DECLARE @firstRec AS INTEGER
DECLARE @lastRec AS INTEGER
SET @firstRec= (@CurrentPage-1) * @PageSize                      
SET @lastRec= (@CurrentPage*@PageSize+1)                                                                     
SET @TotRecs=(SELECT COUNT(*) FROM #tempTable)                                                   
                                                                  
IF @GetAll=0         
BEGIN        
                                          
IF @bitMassUpdate=1            
BEGIN
        
   DECLARE @strReplace AS VARCHAR(MAX)      
   SET @strReplace = CASE WHEN @LookTable='DivisionMaster' THEN 'DM.numDivisionID' 
						  WHEN @LookTable='AdditionalContactsInformation' THEN 'ADC.numContactId' 
						  WHEN @LookTable='ContactAddress' THEN 'ADC.numContactId' 
						  WHEN @LookTable='ShipAddress' THEN 'DM.numDivisionID' 
						  WHEN @LookTable='BillAddress' THEN 'DM.numDivisionID' 
						  WHEN @LookTable='CompanyInfo' THEN 'C.numCompanyId' END           
					 + ' FROM AdditionalContactsInformation ADC JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId '
					 + @WhereCondition        

	PRINT '@strReplace:' + @strReplace
    SET @strMassUpdate=replace(@strMassUpdate,'@$replace',@strReplace)      
    PRINT '@strMassUpdate:' + @strMassUpdate
  EXEC (@strMassUpdate)         
 END         
 
 IF (@Where = 'DUP')
 BEGIN
	SET @strSql=@strSql+'FROM AdditionalContactsInformation ADC
	  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID
	  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                      
	  inner join #tempTable T on T.numContactId=ADC.numContactId ' 
	  + @WhereCondition +'
	  WHERE ID > '+convert(varchar(10),@firstRec)+ ' and ID <'+ convert(varchar(10),@lastRec) +  @WCondition + ' order by T.ID' -- @OrderBy
	  
	  PRINT 'FROM WHERE CONDOTION OF "DUP" @strSql: ' + @strSql
	  EXEC(@strSql)                                                                 
	 DROP TABLE #tempTable
	 RETURN	
 END
ELSE
BEGIN
	SET @strSql=@strSql+'FROM AdditionalContactsInformation ADC                                                   
	  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                  
	  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                              
	   '+@WhereCondition+' inner join #tempTable T on T.numContactId=ADC.numContactId                                                               
	  WHERE ID > ' + CONVERT(VARCHAR(10),@firstRec)+ ' and ID <'+ CONVERT(VARCHAR(10),@lastRec) + ' order by T.ID' 
END
             
END                           
 ELSE                              
     SET @strSql=@strSql+'FROM AdditionalContactsInformation ADC                                                   
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                  
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                              
  '+@WhereCondition+' join #tempTable T on T.numContactId=ADC.numContactId'                 

PRINT '@strSql:' + @strSql
                                        
EXEC (@strSql) 
DROP TABLE #tempTable

SELECT  
	A.tintOrder+1,
	vcDbColumnName,
	vcFieldName,
	vcAssociatedControlType,
	vcListItemType,
	numListID,
	vcLookBackTableName,
	numFieldID,
	numFormFieldID,
	ISNULL(A.intColumnWidth,0) AS intColumnWidth,
	ISNULL(D.bitCustom,0) AS bitCustom
FROM 
	AdvSerViewConf A                              
JOIN 
	View_DynamicDefaultColumns D                               
ON 
	D.numFieldId=A.numFormFieldId   and d.numformId = 1                           
WHERE  
	D.numDomainID = @numDomainID 
	AND D.numFormID = 1 
	AND A.numFormID = 1  
	AND tintViewID=@ViewID 
	AND numGroupID=@numGroupID 
	AND A.numDomainID=@numDomainID 
	AND d.vcDbColumnName not in ('numBillShipCountry','numBillShipState') 
ORDER BY 
	A.tintOrder asc 

IF OBJECT_ID('tempdb..#tempAddress') IS NOT NULL
BEGIN
    DROP TABLE #tempAddress
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CalculateCommissionPaidBizDoc')
DROP PROCEDURE USP_CalculateCommissionPaidBizDoc
GO
Create PROCEDURE [dbo].[USP_CalculateCommissionPaidBizDoc]
	-- Add the parameters for the stored procedure here
	 @numComPayPeriodID AS NUMERIC(18,0),
	 @numDomainID AS NUMERIC(18,0),
	 @ClientTimeZoneOffset INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

BEGIN TRY
BEGIN TRANSACTION
	DECLARE @dtPayStart DATE 
	DECLARE @dtPayEnd DATE

	SELECT @dtPayStart=dtStart,@dtPayEnd=dtEnd FROM CommissionPayPeriod WHERE numComPayPeriodID = @numComPayPeriodID

	--GET COMMISSION RULE OF DOMAIN
	DECLARE @TempCommissionRule TABLE
	(
		ID INT IDENTITY(1,1),
		numComRuleID NUMERIC(18,0),
		tintComBasedOn TINYINT,
		tintComType TINYINT,
		tintComAppliesTo TINYINT,
		tintComOrgType TINYINT,
		tintAssignTo TINYINT
	)

	INSERT INTO
		@TempCommissionRule
	SELECT
		CR.numComRuleID,
		CR.tintComBasedOn,
		CR.tintComType,
		CR.tintComAppliesTo,
		CR.tintComOrgType,
		CR.tintAssignTo
	FROM
		CommissionRules CR
	LEFT JOIN
		PriceBookPriorities PBP
	ON	
		CR.tintComAppliesTo = PBP.Step2Value 
		AND CR.tintComOrgType = PBP.Step3Value
	WHERE
		CR.numDomainID = @numDomainID
		AND ISNULL(PBP.[Priority],0) > 0

	--GET COMMISSION GLOBAL SETTINGS
	DECLARE @numDiscountServiceItemID NUMERIC(18,0)
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @tintCommissionType TINYINT
	DECLARE @bitIncludeTaxAndShippingInCommission BIT

	SELECT 
		@numDiscountServiceItemID=numDiscountServiceItemID, 
		@numShippingServiceItemID = numShippingServiceItemID, 
		@tintCommissionType=tintCommissionType, 
		@bitIncludeTaxAndShippingInCommission=bitIncludeTaxAndShippingInCommission 
	FROM 
		Domain 
	WHERE 
		numDomainID=@numDomainID

	--TEMPORARY TABLES TO HOLD DATA
	DECLARE @TempBizCommission TABLE
	(
		numUserCntID NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		monCommission MONEY,
		numComRuleID NUMERIC(18,0),
		tintComType TINYINT,
		tintComBasedOn TINYINT,
		decCommission FLOAT
	)

	DECLARE @TABLEPAID TABLE
	(
		ID INT IDENTITY(1,1),
		numDivisionID NUMERIC(18,0),
		numRelationship NUMERIC(18,0),
		numProfile NUMERIC(18,0),
		numAssignedTo NUMERIC(18,0),
		numRecordOwner NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		numItemClassification NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		numUnitHour INT,
		monTotAmount MONEY,
		monVendorCost MONEY,
		monAvgCost MONEY,
		bitCommissionCalculated BIT
	)

	-- GET FULLY PAID INVOICES WITHIN PAY PERIOD
	INSERT INTO 
		@TABLEPAID
	SELECT 
		OppM.numDivisionID,
		ISNULL(CompanyInfo.numCompanyType,0),
		ISNULL(CompanyInfo.vcProfile,0),
		OppM.numAssignedTo,
		OppM.numRecOwner,
		OppBizItems.numItemCode,
		Item.numItemClassification,
		OppM.numOppID,
		OppBizItems.numOppBizDocID,
		OppBizItems.numOppBizDocItemID,
		ISNULL(OppBizItems.numUnitHour,0),
		ISNULL(OppBizItems.monTotAmount,0),
		(ISNULL(OppMItems.monVendorCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
		(ISNULL(OppMItems.monAvgCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
		0
	FROM 
		OpportunityBizDocs OppBiz
	INNER JOIN
		OpportunityMaster OppM
	ON
		OppBiz.numOppID = OppM.numOppID
	INNER JOIN
		DivisionMaster 
	ON
		OppM.numDivisionID = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	INNER JOIN
		OpportunityBizDocItems OppBizItems
	ON
		OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
	INNER JOIN
		OpportunityItems OppMItems 
	ON
		OppBizItems.numOppItemID = OppMItems.numoppitemtCode
	INNER JOIN
		Item
	ON
		OppBizItems.numItemCode = Item.numItemCode
	LEFT JOIN
		BizDocComission BDC
	ON
		OppM.numOppId = BDC.numOppID 
		AND OppBiz.numOppBizDocsId = BDC.numOppBizDocId
		AND OppBizItems.numOppBizDocItemID = BDC.numOppBizDocItemID
	CROSS APPLY
	(
		SELECT 
			MAX(DM.dtDepositDate) dtDepositDate
		FROM 
			DepositMaster DM 
		JOIN 
			dbo.DepositeDetails DD
		ON 
			DM.numDepositId=DD.numDepositID 
		WHERE 
			DM.tintDepositePage=2 
			AND DM.numDomainId=@numDomainId
			AND DD.numOppID=OppBiz.numOppID 
			AND DD.numOppBizDocsID =OppBiz.numOppBizDocsID
	) TempDepositMaster
	WHERE
		OppM.numDomainId = @numDomainID 
		AND OppM.tintOppType = 1
		AND OppM.tintOppStatus = 1
		AND OppBiz.bitAuthoritativeBizDocs = 1
		AND ISNULL(OppBiz.monAmountPaid,0) >= OppBiz.monDealAmount
		AND (TempDepositMaster.dtDepositDate IS NOT NULL AND (CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,TempDepositMaster.dtDepositDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd))
		AND OppBizItems.numItemCode NOT IN ( @numDiscountServiceItemID )  /*DO NOT INCLUDE DISCOUNT ITEM*/
		AND 1 = (
					CASE 
						WHEN OppBizItems.numItemCode = @numShippingServiceItemID 
						THEN 
							CASE 
								WHEN ISNULL(@bitIncludeTaxAndShippingInCommission,0) = 1
								THEN	
									1
								ELSE	
									0
							END
						ELSE 
							1 
					END
				)/*CONDITION TO INCLUDE/EXCLUDE INCLUDE SHIPPING ITEM */
		AND BDC.numOppBizDocItemID IS NULL --NO ENTRY AVAILABLE IN BizDocComission TABLE FOR BIZDOC ITEM


		-- LOOP ALL COMMISSION RULES
		DECLARE @i INT = 1
		DECLARE @COUNT INT = 0

		SELECT @COUNT=COUNT(*) FROM @TempCommissionRule

		DECLARE @TEMPITEM TABLE
		(
			ID INT,
			UniqueID INT,
			numUserCntID NUMERIC(18,0),
			numOppID NUMERIC(18,0),
			numOppBizDocID NUMERIC(18,0),
			numOppBizDocItemID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numUnitHour INT,
			monTotAmount MONEY,
			monVendorCost MONEY,
			monAvgCost MONEY
		)

		WHILE @i <= @COUNT
		BEGIN
			DECLARE @numComRuleID NUMERIC(18,0)
			DECLARE @tintComBasedOn TINYINT
			DECLARE @tintComAppliesTo TINYINT
			DECLARE @tintComOrgType TINYINT
			DECLARE @tintComType TINYINT
			DECLARE @tintAssignTo TINYINT
			DECLARE @decCommission MONEY
			DECLARE @numTotalAmount AS MONEY
			DECLARE @numTotalUnit AS INT
			DECLARE @RowCount AS INT

			--CLEAR PREVIOUS VALUES FROM TEMP TABLE
			DELETE FROM @TEMPITEM

			--FETCH COMMISSION RULE
			SELECT @numComRuleID=numComRuleID,@tintComBasedOn=tintComBasedOn,@tintComType=tintComType,@tintComAppliesTo=tintComAppliesTo,@tintComOrgType=tintComOrgType,@tintAssignTo=tintAssignTo FROM @TempCommissionRule WHERE ID=@i

			--FIND ALL BIZ DOC ITEMS WHERE COMMISSION IS NOT PAID AND CURRENT RULE ITEMS, ORGANIZATION AND ASSINGED TO FILTER CAN BE APPLIED
			INSERT INTO
				@TEMPITEM
			SELECT
				ROW_NUMBER() OVER (ORDER BY ID),
				ID,
				(CASE @tintAssignTo WHEN 1 THEN numAssignedTo ELSE numRecordOwner END),
				numOppID,
				numOppBizDocID,
				numOppBizDocItemID,
				numItemCode,
				numUnitHour,
				monTotAmount,
				monVendorCost,
				monAvgCost
			FROM
				@TABLEPAID
			WHERE
				1 = (CASE @tintComAppliesTo
						--SPECIFIC ITEMS
						WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemCode) > 0 THEN 1 ELSE 0 END
						-- ITEM WITH SPECIFIC CLASSIFICATIONS
						WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemClassification) > 0 THEN 1 ELSE 0 END
						-- ALL ITEMS
						ELSE 1
					END)
				AND 1 = (CASE @tintComOrgType
							-- SPECIFIC ORGANIZATION
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numDivisionID) > 0 THEN 1 ELSE 0 END
							-- ORGANIZATION WITH SPECIFIC RELATIONSHIP AND PROFILE
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numRelationship AND ISNULL(numProfile,0) = numProfile) > 0 THEN 1 ELSE 0 END
							-- ALL ORGANIZATIONS
							ELSE 1
						END)
				AND 1 = (CASE @tintAssignTo 
							-- ORDER ASSIGNED TO
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numAssignedTo) > 0 THEN 1 ELSE 0 END
							-- ORDER OWNER
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numRecordOwner) > 0 THEN 1 ELSE 0 END
							-- NO OPTION SELECTED IN RULE
							ELSE 0
						END)
				AND bitCommissionCalculated = 0

			-- IF BIZDOC ITEMS ARE FOUND AFTER FILTER
			IF (SELECT COUNT(*) FROM @TEMPITEM) > 0
			BEGIN
				--CHECK COMMISSION BASED ON OPTION OF RULE
				--BASED ON AMOUNT SOLD
				IF @tintComBasedOn = 1
				BEGIN
					SELECT @numTotalAmount = SUM(monTotAmount) FROM @TEMPITEM

					IF (SELECT COUNT(*) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalAmount BETWEEN intFrom AND intTo)) > 0
					BEGIN
						SELECT TOP 1 @decCommission=ISNULL(decCommission,0) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalAmount BETWEEN intFrom AND intTo)
					END
				END
				--BASED ON UNITS SOLD
				ELSE IF @tintComBasedOn = 2
				BEGIN
					SELECT @numTotalUnit = SUM(numUnitHour) FROM @TEMPITEM

					IF (SELECT COUNT(*) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalUnit BETWEEN intFrom AND intTo)) > 0
					BEGIN
						SELECT TOP 1 @decCommission=ISNULL(decCommission,0) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalUnit BETWEEN intFrom AND intTo)
					END
				END

				--IF COMMISSION AMOUNT FOUND AFTER CHECKING COMMISSION FROM AND TO RANGE IN ABOVE STEP
				IF ISNULL(@decCommission,0) > 0
				BEGIN
					SELECT @RowCount=COUNT(*) FROM @TEMPITEM

					--CALCULATE COMMISSION AND STORE IN TEMPARORY TABLE
					INSERT INTO @TempBizCommission 
					(
						numUserCntID,
						numOppID,
						numOppBizDocID,
						numOppBizDocItemID,
						monCommission,
						numComRuleID,
						tintComType,
						tintComBasedOn,
						decCommission
					)
					SELECT
						numUserCntID,
						numOppID,
						numOppBizDocID,
						numOppBizDocItemID,
						CASE @tintComType 
							WHEN 1 --PERCENT
								THEN 
									CASE @tintCommissionType
										--TOTAL AMOUNT PAID
										WHEN 1 THEN monTotAmount * (@decCommission / 100)
										--ITEM GROSS PROFIT (VENDOR COST)
										WHEN 2 THEN (ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) * (@decCommission / 100)
										--ITEM GROSS PROFIT (AVERAGE COST)
										WHEN 3 THEN(ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) * (@decCommission / 100)
									END
							ELSE  --FLAT: COMMISSION AMOUNT IS EQUALLY DISTRIBUTED AMONG FOUND ENTERIES
								(@decCommission / @RowCount)
						END,
						@numComRuleID,
						@tintComType,
						@tintComBasedOn,
						@decCommission
					FROM 
						@TEMPITEM
					WHERE
						1 = (CASE @tintCommissionType 
								--TOTAL PAID INVOICE
								WHEN 1 THEN CASE WHEN (ISNULL(monTotAmount,0) > 0) THEN 1 ELSE 0 END 
								-- ITEM GROSS PROFIT (VENDOR COST)
								WHEN 2 THEN CASE WHEN ((ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) > 0) THEN 1 ELSE 0 END 
								-- ITEM GROSS PROFIT (AVERAGE COST)
								WHEN 3 THEN CASE WHEN ((ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) > 0) THEN 1 ELSE 0 END
								ELSE
									0 
							END) 
					
					--MARK ENTERIES AS COMMISSION CALCULATED IN TOP TEMP TABLE
					UPDATE 
						@TABLEPAID 
					SET 
						bitCommissionCalculated = 1
					WHERE 
						ID IN (
								SELECT 
									UniqueID 
								FROM
									@TEMPITEM
								WHERE
									1 = (CASE @tintCommissionType 
											--TOTAL PAID INVOICE
											WHEN 1 THEN CASE WHEN (ISNULL(monTotAmount,0) > 0) THEN 1 ELSE 0 END 
											-- ITEM GROSS PROFIT (VENDORCOST)
											WHEN 2 THEN CASE WHEN ((ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) > 0) THEN 1 ELSE 0 END 
											-- ITEM GROSS PROFIT (AVERAGECOST)
											WHEN 2 THEN CASE WHEN ((ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) > 0) THEN 1 ELSE 0 END
											ELSE
												0 
										END) 
							)

				END
			END

			SET @i = @i + 1

			SET @numComRuleID = 0
			SET @tintComBasedOn = 0
			SET @tintComAppliesTo = 0
			SET @tintComOrgType = 0
			SET @tintComType = 0
			SET @tintAssignTo = 0
			SET @decCommission = 0
			SET @numTotalAmount = 0
			SET @numTotalUnit = 0
			SET @RowCount = 0
		END

		--INSERT CALCULATED ENTERIES IN BIZDOCCOMMISSION TABLE
		INSERT INTO BizDocComission
		(	
			numDomainID,
			numUserCntID,
			numOppBizDocId,
			numComissionAmount,
			numOppBizDocItemID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			numOppID,
			bitFullPaidBiz,
			numComPayPeriodID
		)
		SELECT
			@numDomainID,
			numUserCntID,
			numOppBizDocID,
			monCommission,
			numOppBizDocItemID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			numOppID,
			1,
			@numComPayPeriodID
		FROM
			@TempBizCommission
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CalculateCommissionUnPaidBizDoc')
DROP PROCEDURE USP_CalculateCommissionUnPaidBizDoc
GO
Create PROCEDURE [dbo].[USP_CalculateCommissionUnPaidBizDoc]
	-- Add the parameters for the stored procedure here
	 @numComPayPeriodID AS NUMERIC(18,0),
	 @numDomainID AS NUMERIC(18,0),
	 @ClientTimeZoneOffset INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

BEGIN TRY
BEGIN TRANSACTION
	DECLARE @dtPayStart DATE 
	DECLARE @dtPayEnd DATE

	SELECT @dtPayStart=dtStart,@dtPayEnd=dtEnd FROM CommissionPayPeriod WHERE numComPayPeriodID = @numComPayPeriodID

	--GET COMMISSION RULE OF DOMAIN
	DECLARE @TempCommissionRule TABLE
	(
		ID INT IDENTITY(1,1),
		numComRuleID NUMERIC(18,0),
		tintComBasedOn TINYINT,
		tintComType TINYINT,
		tintComAppliesTo TINYINT,
		tintComOrgType TINYINT,
		tintAssignTo TINYINT
	)

	INSERT INTO
		@TempCommissionRule
	SELECT
		CR.numComRuleID,
		CR.tintComBasedOn,
		CR.tintComType,
		CR.tintComAppliesTo,
		CR.tintComOrgType,
		CR.tintAssignTo
	FROM
		CommissionRules CR
	LEFT JOIN
		PriceBookPriorities PBP
	ON	
		CR.tintComAppliesTo = PBP.Step2Value 
		AND CR.tintComOrgType = PBP.Step3Value
	WHERE
		CR.numDomainID = @numDomainID
		AND ISNULL(PBP.[Priority],0) > 0

	--GET COMMISSION GLOBAL SETTINGS
	DECLARE @numDiscountServiceItemID NUMERIC(18,0)
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @tintCommissionType TINYINT
	DECLARE @bitIncludeTaxAndShippingInCommission BIT

	SELECT 
		@numDiscountServiceItemID=numDiscountServiceItemID, 
		@numShippingServiceItemID = numShippingServiceItemID, 
		@tintCommissionType=tintCommissionType, 
		@bitIncludeTaxAndShippingInCommission=bitIncludeTaxAndShippingInCommission 
	FROM 
		Domain 
	WHERE 
		numDomainID=@numDomainID

	--TEMPORARY TABLES TO HOLD DATA
	DECLARE @TempBizCommission TABLE
	(
		numUserCntID NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		monCommission MONEY,
		numComRuleID NUMERIC(18,0),
		tintComType TINYINT,
		tintComBasedOn TINYINT,
		decCommission FLOAT
	)

	DECLARE @TABLEPAID TABLE
	(
		ID INT IDENTITY(1,1),
		numDivisionID NUMERIC(18,0),
		numRelationship NUMERIC(18,0),
		numProfile NUMERIC(18,0),
		numAssignedTo NUMERIC(18,0),
		numRecordOwner NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		numItemClassification NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		numUnitHour INT,
		monTotAmount MONEY,
		monVendorCost MONEY,
		monAvgCost MONEY,
		bitCommissionCalculated BIT
	)

	-- GET FULLY PAID INVOICES WITHIN PAY PERIOD
	INSERT INTO 
		@TABLEPAID
	SELECT 
		OppM.numDivisionID,
		ISNULL(CompanyInfo.numCompanyType,0),
		ISNULL(CompanyInfo.vcProfile,0),
		OppM.numAssignedTo,
		OppM.numRecOwner,
		OppBizItems.numItemCode,
		Item.numItemClassification,
		OppM.numOppID,
		OppBizItems.numOppBizDocID,
		OppBizItems.numOppBizDocItemID,
		ISNULL(OppBizItems.numUnitHour,0),
		ISNULL(OppBizItems.monTotAmount,0),
		(ISNULL(OppMItems.monVendorCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
		(ISNULL(OppMItems.monAvgCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
		0
	FROM 
		OpportunityBizDocs OppBiz
	INNER JOIN
		OpportunityMaster OppM
	ON
		OppBiz.numOppID = OppM.numOppID
	INNER JOIN
		DivisionMaster 
	ON
		OppM.numDivisionID = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	INNER JOIN
		OpportunityBizDocItems OppBizItems
	ON
		OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
	INNER JOIN
		OpportunityItems OppMItems 
	ON
		OppBizItems.numOppItemID = OppMItems.numoppitemtCode
	INNER JOIN
		Item
	ON
		OppBizItems.numItemCode = Item.numItemCode
	LEFT JOIN
		BizDocComission BDC
	ON
		OppM.numOppId = BDC.numOppID 
		AND OppBiz.numOppBizDocsId = BDC.numOppBizDocId
		AND OppBizItems.numOppBizDocItemID = BDC.numOppBizDocItemID
	WHERE
		OppM.numDomainId = @numDomainID 
		AND OppM.tintOppType = 1
		AND OppM.tintOppStatus = 1
		AND OppBiz.bitAuthoritativeBizDocs = 1
		AND ISNULL(OppBiz.monAmountPaid,0) < OppBiz.monDealAmount
		AND (OppBiz.dtFromDate IS NOT NULL AND (CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,OppBiz.dtFromDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd))
		AND OppBizItems.numItemCode NOT IN ( @numDiscountServiceItemID )  /*DO NOT INCLUDE DISCOUNT ITEM*/
		AND 1 = (
					CASE 
						WHEN OppBizItems.numItemCode = @numShippingServiceItemID 
						THEN 
							CASE 
								WHEN ISNULL(@bitIncludeTaxAndShippingInCommission,0) = 1
								THEN	
									1
								ELSE	
									0
							END
						ELSE 
							1 
					END
				)/*CONDITION TO INCLUDE/EXCLUDE INCLUDE SHIPPING ITEM */
		AND BDC.numOppBizDocItemID IS NULL --NO ENTRY AVAILABLE IN BizDocComission TABLE FOR BIZDOC ITEM


		-- LOOP ALL COMMISSION RULES
		DECLARE @i INT = 1
		DECLARE @COUNT INT = 0

		SELECT @COUNT=COUNT(*) FROM @TempCommissionRule

		DECLARE @TEMPITEM TABLE
		(
			ID INT,
			UniqueID INT,
			numUserCntID NUMERIC(18,0),
			numOppID NUMERIC(18,0),
			numOppBizDocID NUMERIC(18,0),
			numOppBizDocItemID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numUnitHour INT,
			monTotAmount MONEY,
			monVendorCost MONEY,
			monAvgCost MONEY
		)

		WHILE @i <= @COUNT
		BEGIN
			DECLARE @numComRuleID NUMERIC(18,0)
			DECLARE @tintComBasedOn TINYINT
			DECLARE @tintComAppliesTo TINYINT
			DECLARE @tintComOrgType TINYINT
			DECLARE @tintComType TINYINT
			DECLARE @tintAssignTo TINYINT
			DECLARE @decCommission MONEY
			DECLARE @numTotalAmount AS MONEY
			DECLARE @numTotalUnit AS INT
			DECLARE @RowCount AS INT

			--CLEAR PREVIOUS VALUES FROM TEMP TABLE
			DELETE FROM @TEMPITEM

			--FETCH COMMISSION RULE
			SELECT @numComRuleID=numComRuleID,@tintComBasedOn=tintComBasedOn,@tintComType=tintComType,@tintComAppliesTo=tintComAppliesTo,@tintComOrgType=tintComOrgType,@tintAssignTo=tintAssignTo FROM @TempCommissionRule WHERE ID=@i

			--FIND ALL BIZ DOC ITEMS WHERE COMMISSION IS NOT PAID AND CURRENT RULE ITEMS, ORGANIZATION AND ASSINGED TO FILTER CAN BE APPLIED
			INSERT INTO
				@TEMPITEM
			SELECT
				ROW_NUMBER() OVER (ORDER BY ID),
				ID,
				(CASE @tintAssignTo WHEN 1 THEN numAssignedTo ELSE numRecordOwner END),
				numOppID,
				numOppBizDocID,
				numOppBizDocItemID,
				numItemCode,
				numUnitHour,
				monTotAmount,
				monVendorCost,
				monAvgCost
			FROM
				@TABLEPAID
			WHERE
				1 = (CASE @tintComAppliesTo
						--SPECIFIC ITEMS
						WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemCode) > 0 THEN 1 ELSE 0 END
						-- ITEM WITH SPECIFIC CLASSIFICATIONS
						WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemClassification) > 0 THEN 1 ELSE 0 END
						-- ALL ITEMS
						ELSE 1
					END)
				AND 1 = (CASE @tintComOrgType
							-- SPECIFIC ORGANIZATION
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numDivisionID) > 0 THEN 1 ELSE 0 END
							-- ORGANIZATION WITH SPECIFIC RELATIONSHIP AND PROFILE
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numRelationship AND ISNULL(numProfile,0) = numProfile) > 0 THEN 1 ELSE 0 END
							-- ALL ORGANIZATIONS
							ELSE 1
						END)
				AND 1 = (CASE @tintAssignTo 
							-- ORDER ASSIGNED TO
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numAssignedTo) > 0 THEN 1 ELSE 0 END
							-- ORDER OWNER
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numRecordOwner) > 0 THEN 1 ELSE 0 END
							-- NO OPTION SELECTED IN RULE
							ELSE 0
						END)
				AND bitCommissionCalculated = 0

			-- IF BIZDOC ITEMS ARE FOUND AFTER FILTER
			IF (SELECT COUNT(*) FROM @TEMPITEM) > 0
			BEGIN
				--CHECK COMMISSION BASED ON OPTION OF RULE
				--BASED ON AMOUNT SOLD
				IF @tintComBasedOn = 1
				BEGIN
					SELECT @numTotalAmount = SUM(monTotAmount) FROM @TEMPITEM

					IF (SELECT COUNT(*) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalAmount BETWEEN intFrom AND intTo)) > 0
					BEGIN
						SELECT TOP 1 @decCommission=ISNULL(decCommission,0) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalAmount BETWEEN intFrom AND intTo)
					END
				END
				--BASED ON UNITS SOLD
				ELSE IF @tintComBasedOn = 2
				BEGIN
					SELECT @numTotalUnit = SUM(numUnitHour) FROM @TEMPITEM

					IF (SELECT COUNT(*) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalUnit BETWEEN intFrom AND intTo)) > 0
					BEGIN
						SELECT TOP 1 @decCommission=ISNULL(decCommission,0) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalUnit BETWEEN intFrom AND intTo)
					END
				END

				--IF COMMISSION AMOUNT FOUND AFTER CHECKING COMMISSION FROM AND TO RANGE IN ABOVE STEP
				IF ISNULL(@decCommission,0) > 0
				BEGIN
					SELECT @RowCount=COUNT(*) FROM @TEMPITEM

					--CALCULATE COMMISSION AND STORE IN TEMPARORY TABLE
					INSERT INTO @TempBizCommission 
					(
						numUserCntID,
						numOppID,
						numOppBizDocID,
						numOppBizDocItemID,
						monCommission,
						numComRuleID,
						tintComType,
						tintComBasedOn,
						decCommission
					)
					SELECT
						numUserCntID,
						numOppID,
						numOppBizDocID,
						numOppBizDocItemID,
						CASE @tintComType 
							WHEN 1 --PERCENT
								THEN 
									CASE @tintCommissionType
										--TOTAL AMOUNT PAID
										WHEN 1 THEN monTotAmount * (@decCommission / 100)
										--ITEM GROSS PROFIT (VENDOR COST)
										WHEN 2 THEN (ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) * (@decCommission / 100)
										--ITEM GROSS PROFIT (AVERAGE COST)
										WHEN 3 THEN(ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) * (@decCommission / 100)
									END
							ELSE  --FLAT: COMMISSION AMOUNT IS EQUALLY DISTRIBUTED AMONG FOUND ENTERIES
								(@decCommission / @RowCount)
						END,
						@numComRuleID,
						@tintComType,
						@tintComBasedOn,
						@decCommission
					FROM 
						@TEMPITEM
					WHERE
						1 = (CASE @tintCommissionType 
								--TOTAL PAID INVOICE
								WHEN 1 THEN CASE WHEN (ISNULL(monTotAmount,0) > 0) THEN 1 ELSE 0 END 
								-- ITEM GROSS PROFIT (VENDOR COST)
								WHEN 2 THEN CASE WHEN ((ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) > 0) THEN 1 ELSE 0 END 
								-- ITEM GROSS PROFIT (AVERAGE COST)
								WHEN 3 THEN CASE WHEN ((ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) > 0) THEN 1 ELSE 0 END
								ELSE
									0 
							END) 
					
					--MARK ENTERIES AS COMMISSION CALCULATED IN TOP TEMP TABLE
					UPDATE 
						@TABLEPAID 
					SET 
						bitCommissionCalculated = 1
					WHERE 
						ID IN (
								SELECT 
									UniqueID 
								FROM
									@TEMPITEM
								WHERE
									1 = (CASE @tintCommissionType 
											--TOTAL PAID INVOICE
											WHEN 1 THEN CASE WHEN (ISNULL(monTotAmount,0) > 0) THEN 1 ELSE 0 END 
											-- ITEM GROSS PROFIT (VENDORCOST)
											WHEN 2 THEN CASE WHEN ((ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) > 0) THEN 1 ELSE 0 END 
											-- ITEM GROSS PROFIT (AVERAGECOST)
											WHEN 2 THEN CASE WHEN ((ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) > 0) THEN 1 ELSE 0 END
											ELSE
												0 
										END) 
							)

				END
			END

			SET @i = @i + 1

			SET @numComRuleID = 0
			SET @tintComBasedOn = 0
			SET @tintComAppliesTo = 0
			SET @tintComOrgType = 0
			SET @tintComType = 0
			SET @tintAssignTo = 0
			SET @decCommission = 0
			SET @numTotalAmount = 0
			SET @numTotalUnit = 0
			SET @RowCount = 0
		END

		--INSERT CALCULATED ENTERIES IN BIZDOCCOMMISSION TABLE
		INSERT INTO BizDocComission
		(	
			numDomainID,
			numUserCntID,
			numOppBizDocId,
			numComissionAmount,
			numOppBizDocItemID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			numOppID,
			bitFullPaidBiz,
			numComPayPeriodID
		)
		SELECT
			@numDomainID,
			numUserCntID,
			numOppBizDocID,
			monCommission,
			numOppBizDocItemID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			numOppID,
			0,
			@numComPayPeriodID
		FROM
			@TempBizCommission
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
GO


/****** Object:  StoredProcedure [dbo].[USP_CheckAndBilledAmount]    Script Date: 03/20/2015 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CheckAndBilledAmount')
DROP PROCEDURE USP_CheckAndBilledAmount
GO
CREATE PROCEDURE [dbo].[USP_CheckAndBilledAmount]
(
	@numDivisionID			NUMERIC(18,0),
	@numOppBizDocId			NUMERIC(18,0),
	@numBillID				NUMERIC(18,0),
	@numAmount				MONEY,
	@numDomainID			NUMERIC(18,0)
)
AS 
BEGIN
	
	IF @numBillID > 0
	BEGIN

		SELECT  CASE WHEN ISNULL(BH.[monAmountDue],0) = @numAmount THEN 1
					 WHEN ISNULL(BH.[monAmountDue],0) > ISNULL(SUM(monAmount),0) + @numAmount THEN 2
					 WHEN ISNULL(BH.[monAmountDue],0) < ISNULL(SUM(monAmount),0) + @numAmount THEN 3
				END [IsPaidInFull]
		FROM [dbo].[BillPaymentDetails] AS BPD 
		JOIN [dbo].[BillPaymentHeader] AS BPH ON [BPH].[numBillPaymentID] = BPD.[numBillPaymentID]
		JOIN [dbo].[BillHeader] AS BH ON BH.[numBillID] = BPD.[numBillID]
		WHERE [BH].[numBillID] = @numBillID
		AND [BH].[numDomainID] = @numDomainID
		AND [BH].[numDivisionID] = @numDivisionID
		GROUP BY [monAmountDue]

	END

	ELSE IF @numOppBizDocId > 0
	BEGIN

		SELECT  CASE WHEN ISNULL([OBD].[monDealAmount],0) = @numAmount THEN 1
					 WHEN ISNULL([OBD].[monDealAmount],0) = ISNULL(SUM(BPD.monAmount),0) + @numAmount THEN 2
					 WHEN ISNULL([OBD].[monDealAmount],0) > ISNULL(SUM(BPD.monAmount),0) + @numAmount THEN 3
					 WHEN ISNULL([OBD].[monDealAmount],0) < ISNULL(SUM(BPD.monAmount),0) + @numAmount THEN 4
				END [IsPaidInFull]
		FROM [dbo].[BillPaymentDetails] AS BPD 
		JOIN [dbo].[BillPaymentHeader] AS BPH ON [BPH].[numBillPaymentID] = BPD.[numBillPaymentID]
		JOIN [dbo].[OpportunityBizDocs] AS OBD ON OBD.[numOppBizDocsId] = [BPD].[numOppBizDocsID]
		JOIN [dbo].[OpportunityMaster] AS OM ON OM.[numOppId] = [OBD].[numOppId] AND [BPH].[numDomainID] = OM.[numDomainId]
		WHERE [OBD].[numOppBizDocsId] = @numOppBizDocId
		AND [OM].[numDomainID] = @numDomainID
		AND [OM].[numDivisionID] = @numDivisionID
		GROUP BY [OBD].[monDealAmount]

	END

		
	--SELECT CASE WHEN ISNULL([OBD].[monDealAmount],0) = @numAmount THEN 1 -- If paid amount + new paid amount is equal to total deal amount, Fully Paid
	--			WHEN ISNULL([OBD].[monDealAmount],0) > SUM(ISNULL(DD.[monAmountPaid],0)) + @numAmount THEN 2 -- If paid amount + new paid amount is greater than total deal amount, Due Amounts
	--			WHEN ISNULL([OBD].[monDealAmount],0) < SUM(ISNULL(DD.[monAmountPaid],0)) + @numAmount THEN 3 -- If paid amount + new paid amount is less than total deal   amount, Unapplied Amounts
	--			ELSE 0
	--	   END AS [IsPaidInFull],
	--	   [DM].[numDivisionID],
	--	   [DM].[numDomainId],
	--	   [DD].[numOppBizDocsID],
	--	   ISNULL([OBD].[monDealAmount],0)  [monDealAmount],
	--	   SUM(ISNULL(DD.[monAmountPaid],0)) [monAmountPaid],
	--	   SUM(ISNULL(DD.[monAmountPaid],0)) + @numAmount [NewAmountPaid],
	--	   (SUM(ISNULL(DD.[monAmountPaid],0)) + @numAmount) - ISNULL([OBD].[monDealAmount],0) [OverPaidAmount]
	--FROM [dbo].[DepositMaster] AS DM 
	--JOIN [dbo].[DepositeDetails] AS DD ON [DM].[numDepositId] = [DD].[numDepositID]
	--JOIN [dbo].[OpportunityMaster] AS OM ON OM.[numOppId] = DD.[numOppID] AND OM.[numDomainId] = DM.[numDomainId]
	--JOIN [dbo].[OpportunityBizDocs] AS OBD ON [OBD].[numOppBizDocsId] = DD.[numOppBizDocsID] AND [OM].[numOppId] = [OBD].[numOppId]
	--WHERE [DM].[numDomainId] = @numDomainID
	--AND [DM].[numDivisionID] = @numDivisionID
	--AND DD.[numOppBizDocsID] = @numOppBizDocId
	--GROUP BY [OBD].[monDealAmount],
	--		 [DM].[numDivisionID],
	--		 [DM].[numDomainId],
	--		 [DD].[numOppBizDocsID]

END

GO

/****** Object:  StoredProcedure [dbo].[USP_CheckAndDepositAmountForBizdoc]    Script Date: 02/19/2010 17:44:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CheckAndDepositAmountForBizdoc')
DROP PROCEDURE USP_CheckAndDepositAmountForBizdoc
GO
CREATE PROCEDURE [dbo].[USP_CheckAndDepositAmountForBizdoc]
(
	@numDivisionID			NUMERIC(18,0),
	@numOppBizDocId			NUMERIC(18,0),
	@numAmount				MONEY,
	@numDomainID			NUMERIC(18,0)
)
AS 
BEGIN
		
	SELECT CASE WHEN ISNULL([OBD].[monDealAmount],0) = @numAmount THEN 1 -- If paid amount + new paid amount is equal to total deal amount, Fully Paid
				WHEN ISNULL([OBD].[monDealAmount],0) > SUM(ISNULL(DD.[monAmountPaid],0)) + @numAmount THEN 2 -- If paid amount + new paid amount is greater than total deal amount, Due Amounts
				WHEN ISNULL([OBD].[monDealAmount],0) < SUM(ISNULL(DD.[monAmountPaid],0)) + @numAmount THEN 3 -- If paid amount + new paid amount is less than total deal   amount, Unapplied Amounts
				ELSE 0
		   END AS [IsPaidInFull],
		   [DM].[numDivisionID],
		   [DM].[numDomainId],
		   [DD].[numOppBizDocsID],
		   ISNULL([OBD].[monDealAmount],0)  [monDealAmount],
		   SUM(ISNULL(DD.[monAmountPaid],0)) [monAmountPaid],
		   SUM(ISNULL(DD.[monAmountPaid],0)) + @numAmount [NewAmountPaid],
		   (SUM(ISNULL(DD.[monAmountPaid],0)) + @numAmount) - ISNULL([OBD].[monDealAmount],0) [OverPaidAmount]
	FROM [dbo].[DepositMaster] AS DM 
	JOIN [dbo].[DepositeDetails] AS DD ON [DM].[numDepositId] = [DD].[numDepositID]
	JOIN [dbo].[OpportunityMaster] AS OM ON OM.[numOppId] = DD.[numOppID] AND OM.[numDomainId] = DM.[numDomainId]
	JOIN [dbo].[OpportunityBizDocs] AS OBD ON [OBD].[numOppBizDocsId] = DD.[numOppBizDocsID] AND [OM].[numOppId] = [OBD].[numOppId]
	WHERE [DM].[numDomainId] = @numDomainID
	AND [DM].[numDivisionID] = @numDivisionID
	AND DD.[numOppBizDocsID] = @numOppBizDocId
	GROUP BY [OBD].[monDealAmount],
			 [DM].[numDivisionID],
			 [DM].[numDomainId],
			 [DD].[numOppBizDocsID]

END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CheckCommissionAppliesTo')
DROP PROCEDURE USP_CheckCommissionAppliesTo
GO
CREATE PROCEDURE [dbo].[USP_CheckCommissionAppliesTo]      
(@numDomainId as numeric(9)=0,      
 @tintComAppliesTo as TINYINT,
 @tintCommissionType AS TINYINT      
)      
As      
Begin      
	DECLARE @tintCommissionType_Old AS TINYINT,@Total AS INT 
	
	SELECT @tintCommissionType_Old=ISNULL(tintCommissionType,1) FROM dbo.Domain WHERE numDomainId=@numDomainId
	
	Select @Total=count(*) From CommissionRules Where numDomainID=@numDomainID
	
	IF (@tintCommissionType!=@tintCommissionType_Old) 
	BEGIN
		SELECT @Total
	END
	ELSE
	BEGIN
		SELECT 0		
	END		
End
GO
/****** Object:  StoredProcedure [dbo].[USP_CheckDuplicateRefIDForJournal]    Script Date: 10th Mar,2015 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_CheckDuplicateRefIDForJournal' ) 
    DROP PROCEDURE USP_CheckDuplicateRefIDForJournal
GO
CREATE PROCEDURE dbo.USP_CheckDuplicateRefIDForJournal
(
	@numJournalReferenceNo NUMERIC(18,0),
	@numDomainID NUMERIC(18,0)
)

AS 
BEGIN
	
	SELECT COUNT([GJH].[numJournal_Id]) AS [RowCount]
	FROM [dbo].[General_Journal_Header] AS GJH 
	WHERE [GJH].[numDomainId] = @numDomainID
	AND [GJH].[numJournalReferenceNo] = @numJournalReferenceNo
END
--Created by anoop jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_checkuseratlogin')
DROP PROCEDURE usp_checkuseratlogin
GO
CREATE PROCEDURE [dbo].[USP_CheckUserAtLogin]                                                              
(                                                                                    
@UserName as varchar(100)='',                                    
@vcPassword as varchar(100)=''                                                                           
)                                                              
as                                                              

DECLARE @listIds VARCHAR(MAX)
SELECT 
	@listIds = COALESCE(@listIds+',' ,'') + CAST(UPA.numUserID AS VARCHAR(100)) 
FROM 
	UserMaster U                              
Join 
	Domain D                              
on 
	D.numDomainID=U.numDomainID
Join  
	Subscribers S                            
on 
	S.numTargetDomainID=D.numDomainID
join
	UnitPriceApprover UPA
ON
	D.numDomainID = UPA.numDomainID
WHERE 
	vcEmailID=@UserName and vcPassword=@vcPassword

/*check subscription validity */
IF EXISTS( SELECT *
FROM UserMaster U                              
 Join Domain D                              
 on D.numDomainID=U.numDomainID
 Join  Subscribers S                            
 on S.numTargetDomainID=D.numDomainID
 WHERE vcEmailID=@UserName and vcPassword=@vcPassword and bitActive=1 AND getutcdate() > dtSubEndDate)
BEGIN
	RAISERROR('SUB_RENEW',16,1)
	RETURN;
END



                                                                
 SELECT top 1 U.numUserID,numUserDetailId,isnull(vcEmailID,'') vcEmailID,isnull(numGroupID,0) numGroupID,bitActivateFlag,                              
 isnull(vcMailNickName,'') vcMailNickName,isnull(txtSignature,'') txtSignature,isnull(U.numDomainID,0) numDomainID,                              
 isnull(Div.numDivisionID,0) numDivisionID,isnull(vcCompanyName,'') vcCompanyName,isnull(E.bitExchangeIntegration,0) bitExchangeIntegration,                              
 isnull(E.bitAccessExchange,0) bitAccessExchange,isnull(E.vcExchPath,'') vcExchPath,isnull(E.vcExchDomain,'') vcExchDomain,                              
 isnull(D.vcExchUserName,'') vcExchUserName ,isnull(vcFirstname,'')+' '+isnull(vcLastName,'') as ContactName,                              
 Case when E.bitAccessExchange=0 then  E.vcExchPassword when E.bitAccessExchange=1 then D.vcExchPassword end as vcExchPassword,                          
 tintCustomPagingRows,                         
 vcDateFormat,                         
 numDefCountry,                     
 tintComposeWindow,
 dateadd(day,-sintStartDate,getutcdate()) as StartDate,                        
 dateadd(day,sintNoofDaysInterval,dateadd(day,-sintStartDate,getutcdate())) as EndDate,                        
 tintAssignToCriteria,                         
 bitIntmedPage,                         
 tintFiscalStartMonth,numAdminID,isnull(A.numTeam,0) numTeam ,                
 isnull(D.vcCurrency,'') as vcCurrency, 
 isnull(D.numCurrencyID,0) as numCurrencyID, 
 isnull(D.bitMultiCurrency,0) as bitMultiCurrency,              
 bitTrial,isnull(tintChrForComSearch,0) as tintChrForComSearch,  
isnull(tintChrForItemSearch,0) as tintChrForItemSearch,              
 case when bitSMTPServer= 1 then vcSMTPServer else '' end as vcSMTPServer  ,          
 case when bitSMTPServer= 1 then  isnull(numSMTPPort,0)  else 0 end as numSMTPPort      
,isnull(bitSMTPAuth,0) bitSMTPAuth    
,isnull([vcSmtpPassword],'') vcSmtpPassword    
,isnull(bitSMTPSSL,0) bitSMTPSSL   ,isnull(bitSMTPServer,0) bitSMTPServer,       
isnull(IM.bitImap,0) bitImapIntegration, isnull(charUnitSystem,'E')   UnitSystem, datediff(day,getutcdate(),dtSubEndDate) as NoOfDaysLeft,
ISNULL(bitCreateInvoice,0) bitCreateInvoice,
ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
vcDomainName,
S.numSubscriberID,
D.[tintBaseTax],
ISNULL(D.[numShipCompany],0) numShipCompany,
ISNULL(D.[bitEmbeddedCost],0) bitEmbeddedCost,
(Select isnull(numAuthoritativeSales,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativeSales,
(Select isnull(numAuthoritativePurchase,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativePurchase,
ISNULL(D.[numDefaultSalesOppBizDocId],0) numDefaultSalesOppBizDocId,
ISNULL(tintDecimalPoints,2) tintDecimalPoints,
ISNULL(D.tintBillToForPO,0) tintBillToForPO, 
ISNULL(D.tintShipToForPO,0) tintShipToForPO,
ISNULL(D.tintOrganizationSearchCriteria,0) tintOrganizationSearchCriteria,
ISNULL(D.tintLogin,0) tintLogin,
ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
ISNULL(D.vcPortalName,'') vcPortalName,
(SELECT COUNT(*) FROM dbo.Subscribers WHERE getutcdate() between dtSubStartDate and dtSubEndDate and bitActive=1),
ISNULL(D.bitGtoBContact,0) bitGtoBContact,
ISNULL(D.bitBtoGContact,0) bitBtoGContact,
ISNULL(D.bitGtoBCalendar,0) bitGtoBCalendar,
ISNULL(D.bitBtoGCalendar,0) bitBtoGCalendar,
ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
ISNULL(D.bitInlineEdit,0) bitInlineEdit,
ISNULL(U.numDefaultClass,0) numDefaultClass,
ISNULL(D.bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
ISNULL(D.bitTrakDirtyForm,1) bitTrakDirtyForm,
isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
ISNULL(D.bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
isnull(U.tintTabEvent,0) as tintTabEvent,
isnull(D.bitRentalItem,0) as bitRentalItem,
isnull(D.numRentalItemClass,0) as numRentalItemClass,
isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
ISNULL(U.numDefaultWarehouse,0) numDefaultWarehouse,
ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
ISNULL(D.bitAutoSerialNoAssign,0) bitAutoSerialNoAssign,
ISNULL(IsEnableClassTracking,0) AS [IsEnableClassTracking],
ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
ISNULL(D.tintCommissionType,1) AS tintCommissionType,
ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
ISNULL(D.IsEnableUserLevelClassTracking,0) AS IsEnableUserLevelClassTracking,
ISNULL(D.bitUseBizdocAmount,1) AS [bitUseBizdocAmount],
ISNULL(D.bitDefaultRateType,1) AS [bitDefaultRateType],
ISNULL(D.bitIncludeTaxAndShippingInCommission,1) AS [bitIncludeTaxAndShippingInCommission],
ISNULL(D.[bitLandedCost],0) AS bitLandedCost,
ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
ISNULL(D.numAbovePercent,0) AS numAbovePercent,
ISNULL(D.numAbovePriceField,0) AS numAbovePriceField,
ISNULL(D.numBelowPercent,0) AS numBelowPercent,
ISNULL(D.numBelowPriceField,0) AS numBelowPriceField,
ISNULL(D.numOrderStatusBeforeApproval,0) AS numOrderStatusBeforeApproval,
ISNULL(D.numOrderStatusAfterApproval,0) AS numOrderStatusAfterApproval,
ISNULL(@listIds,'') AS vcUnitPriceApprover
/*,ISNULL(D.bitAllowPPVariance,0) AS bitAllowPPVariance*/
 FROM UserMaster U                              
 left join ExchangeUserDetails E                              
 on E.numUserID=U.numUserID                              
left join ImapUserDetails IM                              
 on IM.numUserCntID=U.numUserDetailID          
 Join Domain D                              
 on D.numDomainID=U.numDomainID                                           
 left join DivisionMaster Div                                            
 on D.numDivisionID=Div.numDivisionID                               
 left join CompanyInfo C                              
 on C.numCompanyID=Div.numDivisionID                               
 left join AdditionalContactsInformation A                              
 on  A.numContactID=U.numUserDetailId                            
 Join  Subscribers S                            
 on S.numTargetDomainID=D.numDomainID                             
 WHERE vcEmailID=@UserName and vcPassword=@vcPassword and bitActive IN (1,2) AND getutcdate() between dtSubStartDate and dtSubEndDate
             
select numTerritoryId from UserTerritory UT                              
join UserMaster U                              
on numUserDetailId =UT.numUserCntID                                         
where vcEmailID=@UserName and vcPassword=@vcPassword
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CommissionPayPeriod_Get')
DROP PROCEDURE USP_CommissionPayPeriod_Get
GO
Create PROCEDURE [dbo].[USP_CommissionPayPeriod_Get]
	-- Add the parameters for the stored procedure here
	 @numDomainID AS NUMERIC(18,0),
	 @dtStart DATE,
	 @dtEnd DATE  
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

SELECT * FROM CommissionPayPeriod WHERE numDomainID=@numDomainID AND dtStart=@dtStart AND dtEnd=@dtEnd

END
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CommissionPayPeriod_Save')
DROP PROCEDURE USP_CommissionPayPeriod_Save
GO
Create PROCEDURE [dbo].[USP_CommissionPayPeriod_Save]
	-- Add the parameters for the stored procedure here
	 @numComPayPeriodID AS NUMERIC(18,0) OUTPUT,
	 @numDomainID AS NUMERIC(18,0),
	 @dtStart DATE,
	 @dtEnd DATE,
	 @tintPayPeriod INT,
	 @numUserID NUMERIC(18,0),
	 @ClientTimeZoneOffset INT     
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

BEGIN TRY
BEGIN TRANSACTION
	--NEW PAY PERIOD COMMISSION CALCULATION
	IF @numComPayPeriodID = 0
	BEGIN
		INSERT INTO CommissionPayPeriod
		(
			numDomainID,
			dtStart,
			dtEnd,
			tintPayPeriod,
			numCreatedBy,
			dtCreated
		)
		VALUES
		(
			@numDomainID,
			@dtStart,
			@dtEnd,
			@tintPayPeriod,
			@numUserID,
			GETDATE()
		)

		SET @numComPayPeriodID = SCOPE_IDENTITY()
	END
	-- RECALCULATE PAY PERIOD COMMISSION
	ELSE
	BEGIN
		UPDATE 
			CommissionPayPeriod
		SET 
			numModifiedBy = @numUserID,
			dtModified = GETDATE()
		WHERE
			numComPayPeriodID = @numComPayPeriodID

		--DELETE ALL PREVIOUSLY CALCULATE COMMISSSION ENTRIES AGAINST WHICH COMMISSION IS NOT PAID
		DELETE FROM BizDocComission WHERE ISNULL(numComPayPeriodID,0)=@numComPayPeriodID AND ISNULL(bitCommisionPaid,0) = 0
	END

	--CALCULATE COMMISSION ENTERIES FOR PAID BIZDOC
	EXEC USP_CalculateCommissionPaidBizDoc @numComPayPeriodID=@numComPayPeriodID,@numDomainID=@numDomainID,@ClientTimeZoneOffset=@ClientTimeZoneOffset
	--CALCULATE COMMISSION ENTERIES FOR UNPAID OR PARTIALLY BIZDOC
	EXEC USP_CalculateCommissionUnPaidBizDoc @numComPayPeriodID=@numComPayPeriodID,@numDomainID=@numDomainID,@ClientTimeZoneOffset=@ClientTimeZoneOffset

	SELECT @numComPayPeriodID
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
GO

/****** Object:  StoredProcedure [dbo].[USP_EditCntInfo]    Script Date: 07/26/2008 16:15:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_editcntinfo')
DROP PROCEDURE usp_editcntinfo
GO
CREATE PROCEDURE [dbo].[USP_EditCntInfo]                                                
@numContactID NUMERIC(9) ,    
@numDomainID as numeric(9)  
 ,  
@ClientTimeZoneOffset as int                     
                                              
AS                                                
BEGIN                                                
                                                
  SELECT                        
   A.numContactId, A.vcGivenName, A.vcFirstName,                                                 
                      A.vcLastName, D.numDivisionID, C.numCompanyId,                                                 
                      C.vcCompanyName, D.vcDivisionName, D.numDomainID,  
					  ISNULL((select vcdata from listdetails where numListItemID = C.numCompanyType),'') as vcCompanyTypeName,                                               
                      A.numPhone, A.numPhoneExtension, A.vcEmail,  A.numTeam,                                               
                      A.vcFax, A.numContactType,  A.charSex, A.bintDOB,                                                 
                      A.vcPosition, A.txtNotes, A.numCreatedBy,                                                
                      A.numCell,A.NumHomePhone,A.vcAsstFirstName,                                                
                      A.vcAsstLastName,A.numAsstPhone,A.numAsstExtn,                                                
                      A.vcAsstEmail,A.charSex, A.vcDepartment,                                                 
                      AD.vcStreet AS vcpStreet,                                            
                      AD.vcCity AS vcPCity, 
                      dbo.fn_GetState(AD.numState) as State, dbo.fn_GetListName(AD.numCountry,0) as Country,
                      AD.vcPostalCode AS vcPPostalCode,A.bitOptOut,vcDepartment,                                                
        A.numManagerID, A.vcCategory  , dbo.fn_GetContactName(A.numCreatedBy)+' '+ convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,A.bintCreatedDate)) CreatedBy,                                            
          dbo.fn_GetContactName(A.numModifiedBy)+' '+ convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,A.bintModifiedDate)) ModifiedBy,vcTitle,vcAltEmail,                                            
  dbo.fn_GetContactName(A.numRecOwner) as RecordOwner,                                      
  C.vcCompanyName,C.vcWebSite, 
--  dbo.fn_GetState(AddC.vcPState) as State, dbo.fn_GetListName(AddC.vcPCountry,0) as Country,
   A.numEmpStatus,                          
  isnull((select top 1 case when bitEngaged=0 then 'Disengaged' when bitEngaged=1 then 'Engaged' end as ECampStatus  from ConECampaign                          
where  numContactID=@numContactID order by numConEmailCampID desc),'-') as CampStatus ,                        
dbo.GetECamLastAct(@numContactID) as Activity ,       
(select  count(*) from dbo.GenericDocuments   where numRecID=@numContactID and  vcDocumentSection='C') as DocumentCount,
(SELECT count(*)from CompanyAssociations where numDivisionID=@numContactID and bitDeleted=0 ) as AssociateCountFrom,              
(SELECT count(*)from CompanyAssociations where numAssociateFromDivisionID=@numContactID and bitDeleted=0 ) as AssociateCountTo    ,        
vcItemId ,        
vcChangeKey,              
vcCompanyName,      
tintCRMType ,isnull((select top 1 numUserId from  UserMaster where numUserDetailId=A.numContactId),0) as numUserID,
dbo.fn_GetListName(C.numNoOfEmployeesId,0) AS NoofEmp
FROM         AdditionalContactsInformation A     
INNER JOIN  DivisionMaster D ON A.numDivisionId = D.numDivisionID     
INNER JOIN  CompanyInfo C ON D.numCompanyID = C.numCompanyId                      
LEFT JOIN AddressDetails AD ON  A.numDomainID = AD.numDomainID AND AD.numRecordID= A.numContactId AND AD.tintAddressOf=1 AND AD.tintAddressType=0 AND AD.bitIsPrimary=1
WHERE     A.numContactId = @numContactID and A.numDomainID=@numDomainID                  
                                   
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAccountReceivableAging_Invoice]    Script Date: 05/24/2010 03:48:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAccountReceivableAging_Invoice')
DROP PROCEDURE USP_GetAccountReceivableAging_Invoice
GO
CREATE PROCEDURE [dbo].[USP_GetAccountReceivableAging_Invoice](
               @numDomainId   AS NUMERIC(9)  = 0,
			   @numDivisionId AS NUMERIC(9)  = NULL,
			   @vcFlag        VARCHAR(20)
              )
AS
  BEGIN
    DECLARE  @AuthoritativeSalesBizDocId  AS INTEGER
    SELECT @AuthoritativeSalesBizDocId = isnull(numAuthoritativeSales,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId = @numDomainId;--287
    ------------------------------------------      
    DECLARE @baseCurrency AS NUMERIC
	SELECT @baseCurrency=numCurrencyID FROM dbo.Domain WHERE numDomainId=@numDomainId
	/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */
	------------------------------------------      

    DECLARE @strSql VARCHAR(MAX);
	Declare @strSql1 VARCHAR(MAX);
	Declare @strCredit varchar(MAX); SET @strCredit=''
    
    
    SET @strSql = ' SELECT DISTINCT OM.[numOppId],
                        OM.[vcPOppName],
                        OM.[tintOppType],
                        OB.[numOppBizDocsId],
                        OB.[vcBizDocID],
                        isnull(OB.monDealAmount
                                 * OM.fltExchangeRate,0) TotalAmount,
                        (OB.[monAmountPaid]
                           * OM.fltExchangeRate) AmountPaid,
                        isnull(OB.monDealAmount
                                 * OM.fltExchangeRate
                                 - OB.[monAmountPaid]
                                     * OM.fltExchangeRate,0) BalanceDue,
                        CASE ISNULL(bitBillingTerms,0) 
                          --WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0)),OB.dtFromDate),
                          WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate),
                                                                   '+ CONVERT(VARCHAR(15),@numDomainId) +')
                          WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],'+ CONVERT(VARCHAR(15),@numDomainId) +')
                        END AS DueDate,
                        bitBillingTerms,
                        intBillingDays,
                        OB.dtCreatedDate,
                        ISNULL(C.[varCurrSymbol],'''') [varCurrSymbol],CO.vcCompanyName,ADC.vcFirstname + '' ''+ ADC.vcLastName AS vcContactName,
                           dbo.fn_GetContactName(OM.numRecOwner) AS RecordOwner,
						   dbo.fn_GetContactName(OM.numAssignedTo) AS AssignedTo
						   ,CASE WHEN ADC.numPhone IS NULL THEN '''' ELSE + ADC.numPhone + (CASE WHEN numPhoneExtension IS NULL THEN '''' WHEN numPhoneExtension='''' THEN '''' ELSE '','' + ADC.numPhoneExtension END) END AS vcCustPhone,
						   OM.numCurrencyID,OB.vcRefOrderNo,[dbo].[FormatedDateFromDate](ob.[dtFromDate],'+ CONVERT(VARCHAR(15),@numDomainId) +') AS dtFromDate,
						    -1 AS [numJournal_Id],-1 as numReturnHeaderID,
							CASE WHEN ISNULL(BT.[numDiscount],0) > 0 AND ISNULL(BT.[numInterestPercentage],0) > 0 
								 THEN '+ '''-''+' + 'CONVERT(VARCHAR(10),ISNULL(BT.[numDiscount],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numDiscountPaidInDays],0)) +' + ''' days early / ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentage],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentageIfPaidAfterDays],0)) +' + ''' days or later ''' +
								 ' WHEN ISNULL(BT.[numDiscount],0) > 0 THEN '+ '''-''+' + 'CONVERT(VARCHAR(10),ISNULL(BT.[numDiscount],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numDiscountPaidInDays],0)) +' + ''' days early ''' + 
								 ' WHEN ISNULL(BT.[numInterestPercentage],0) > 0  THEN +CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentage],0))+' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentageIfPaidAfterDays],0))+' + ''' days or later ''' +
								' END AS [vcTerms],
							ISNULL(BT.[numDiscount],0) AS [numDiscount],
							ISNULL(BT.[numInterestPercentage],0) AS [numInterest] 
        INTO #TempRecords                
        FROM   [OpportunityMaster] OM
               INNER JOIN [DivisionMaster] DM
                 ON OM.[numDivisionId] = DM.[numDivisionID]
                  INNER JOIN CompanyInfo CO ON CO.numCompanyId = DM.numCompanyID
                  INNER JOIN AdditionalContactsInformation ADC ON ADC.numDivisionId=DM.numDivisionID AND ADC.numContactId=OM.numContactId
               LEFT OUTER JOIN OpportunityBizDocs OB
                 ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN [Currency] C ON OM.[numCurrencyID] = C.[numCurrencyID]
			   LEFT JOIN [dbo].[BillingTerms] AS BT ON [OM].[intBillingDays] = [BT].[numTermsID] 
        WHERE  OM.[tintOppType] = 1
               AND OM.[tintOppStatus] = 1
               AND om.[numDomainId] = '+ CONVERT(VARCHAR(15),@numDomainId) +'
               AND OB.[numBizDocId] = '+ CONVERT(VARCHAR(15),ISNULL(@AuthoritativeSalesBizDocId,0)) +' 
               AND OB.bitAuthoritativeBizDocs=1
               AND ISNULL(OB.tintDeferred,0) <> 1
               AND isnull(OB.monDealAmount * OM.fltExchangeRate,0) != 0 '
               
		IF isnull(@numDivisionID,0)>0
			SET @strSql = @strSql + 'AND OM.numDivisionId = '+ CONVERT(VARCHAR(15),@numDivisionID)
       
       SET @strSql1 = '  UNION 
		 SELECT 
				-1 [numOppId],
				''Journal'' [vcPOppName],
				0 [tintOppType],
				0 [numOppBizDocsId],
				''Journal'' [vcBizDocID],
				CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END TotalAmount,
				0 AmountPaid,
				0 BalanceDue,
				[dbo].[FormatedDateFromDate](GJH.datEntry_Date,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate,
				0 bitBillingTerms,
				0 intBillingDays,
				GJH.datCreatedDate,
                ISNULL(C.[varCurrSymbol], '''') [varCurrSymbol]
                ,CO.vcCompanyName,ADC.vcFirstname + '' ''+ ADC.vcLastName AS vcContactName,
                           '''' AS RecordOwner,
						   '''' AS AssignedTo
						   ,CASE WHEN ADC.numPhone IS NULL THEN '''' ELSE + ADC.numPhone + (CASE WHEN numPhoneExtension IS NULL THEN '''' WHEN numPhoneExtension='''' THEN '''' ELSE '','' + ADC.numPhoneExtension END) END AS vcCustPhone,
						   GJD.numCurrencyID,'''' as vcRefOrderNo,NULL, ISNULL(GJH.numJournal_Id,0) AS [numJournal_Id],-1 as numReturnHeaderID,
							CASE WHEN ISNULL(BT.[numDiscount],0) > 0 AND ISNULL(BT.[numInterestPercentage],0) > 0 
								 THEN '+ '''-''+' + 'CONVERT(VARCHAR(10),ISNULL(BT.[numDiscount],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numDiscountPaidInDays],0)) +' + ''' days early / ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentage],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentageIfPaidAfterDays],0)) +' + ''' days or later ''' +
								 ' WHEN ISNULL(BT.[numDiscount],0) > 0 THEN '+ '''-''+' + 'CONVERT(VARCHAR(10),ISNULL(BT.[numDiscount],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numDiscountPaidInDays],0)) +' + ''' days early ''' + 
								 ' WHEN ISNULL(BT.[numInterestPercentage],0) > 0  THEN +CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentage],0))+' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentageIfPaidAfterDays],0))+' + ''' days or later ''' +
								' END AS [vcTerms],
							ISNULL(BT.[numDiscount],0) AS [numDiscount],
							ISNULL(BT.[numInterestPercentage],0) AS [numInterest]	 
		 FROM   dbo.General_Journal_Header GJH
				INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
															  AND GJH.numDomainId = GJD.numDomainId
				INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
				INNER JOIN [DivisionMaster] DM ON GJD.numCustomerID = DM.[numDivisionID]
				INNER JOIN CompanyInfo CO ON CO.numCompanyId = DM.numCompanyID
                Left JOIN AdditionalContactsInformation ADC ON ADC.numContactId=GJD.numContactId 
				LEFT OUTER JOIN [Currency] C ON GJD.[numCurrencyID] = C.[numCurrencyID]
				LEFT JOIN [dbo].[BillingTerms] AS BT ON [DM].[numBillingDays] = [BT].[numTermsID]
		 WHERE  GJH.numDomainId ='+ CONVERT(VARCHAR(15),@numDomainId) +'
				AND COA.vcAccountCode LIKE ''01010105%'' 
				AND ISNULL(numOppId,0)=0 AND ISNULL(numOppBizDocsId,0)=0 AND ISNULL(GJH.numDepositID,0)=0  AND ISNULL(GJH.numReturnID,0)=0'
        IF isnull(@numDivisionID,0)>0
                SET @strSql1 = @strSql1 + ' AND GJD.numCustomerId = '+ CONVERT(VARCHAR(15),@numDivisionID) + ' '
   
	 SET @strCredit = ' UNION SELECT -1 [numOppId],
									CASE WHEN tintDepositePage=2 THEN ''Unapplied Payment'' ELSE ''Credit Memo'' END AS [vcPOppName],
									0 [tintOppType],
									0 [numOppBizDocsId],
									''''  [vcBizDocID],
									(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) * -1 AS TotalAmount,
									0 AmountPaid,
									(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) * -1 AS BalanceDue,
									[dbo].[FormatedDateFromDate](DM.dtDepositDate,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate,
									0 bitBillingTerms,
									0 intBillingDays,
									DM.dtCreationDate AS [datCreatedDate],
									'''' [varCurrSymbol],
									dbo.fn_GetComapnyName(DM.numDivisionId) vcCompanyName,
									'''' vcContactName,
									'''' AS RecordOwner,
									'''' AS AssignedTo,
									'''' AS vcCustPhone,
									ISNULL(DM.numCurrencyID,0) AS [numCurrencyID],
									'''' as vcRefOrderNo,
									NULL, 
									-1 AS [numJournal_Id],isnull(numReturnHeaderID,0) as numReturnHeaderID,
									'''' AS [vcTerms],
									0 AS [numDiscount],
									0 AS [numInterest]
					FROM  DepositMaster DM 
					WHERE DM.numDomainId='+ CONVERT(VARCHAR(15),@numDomainId) +' AND tintDepositePage IN(2,3)  
					AND ((ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) > 0)'  
	
	IF isnull(@numDivisionID,0)>0
          SET @strCredit = @strCredit + ' AND DM.numDivisionId = '+ CONVERT(VARCHAR(15),@numDivisionID) + ' '
                	
    IF (@vcFlag = '0+30')
      BEGIN
      
      SET @strSql =@strSql +
						'AND dateadd(DAY,CASE 
                                 --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                 WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                 ELSE 0
                               END,OB.dtFromDate) BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())
               AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                ELSE 0
                                                              END,OB.dtFromDate)'
                                                              
		SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())'
		
		SET @strCredit =@strCredit + 'AND DM.dtDepositDate BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())'
      END
    ELSE
      IF (@vcFlag = '30+60')
        BEGIN
         SET @strSql =@strSql +
                 'AND dateadd(DAY,CASE 
                                   --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                   WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                   ELSE 0
                                 END,OB.dtFromDate) BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())
                 AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                  --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                  WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                  ELSE 0
                                                                END,OB.dtFromDate)'
		SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())'
        
        SET @strCredit =@strCredit + 'AND DM.dtDepositDate BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())'
                 
        END
      ELSE
        IF (@vcFlag = '60+90')
          BEGIN
          SET @strSql =@strSql +
                   'AND dateadd(DAY,CASE 
                                     --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                     WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                     ELSE 0
                                   END,OB.dtFromDate) BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                   AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                    --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                    WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                    ELSE 0
                                                                  END,OB.dtFromDate)'
			SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
			
			SET @strCredit =@strCredit + 'AND DM.dtDepositDate BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
			
          END
        ELSE
          IF (@vcFlag = '90+')
            BEGIN
             SET @strSql =@strSql +
                     'AND dateadd(DAY,CASE 
                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                       ELSE 0
                                     END,OB.dtFromDate) > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                     AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                      --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                      WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                      ELSE 0
                                                                    END,OB.dtFromDate)'
			SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date  > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
            
            SET @strCredit =@strCredit + 'AND DM.dtDepositDate  > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
                     
            END
          ELSE
            IF (@vcFlag = '0-30')
              BEGIN
               SET @strSql =@strSql +
                       'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                       ELSE 0
                                                                     END,OB.dtFromDate)
                       AND datediff(DAY,dateadd(DAY,CASE 
                                                      --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                      WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                      ELSE 0
                                                    END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
				SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
				
				SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
				
              END
            ELSE
              IF (@vcFlag = '30-60')
                BEGIN
                  SET @strSql =@strSql +
                         'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                         --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                         WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                         ELSE 0
                                                                       END,OB.dtFromDate)
                         AND datediff(DAY,dateadd(DAY,CASE 
                                                        --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                        WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                        ELSE 0
                                                      END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60'
                SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60 '
                
                SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60 '
                
                END
              ELSE
                IF (@vcFlag = '60-90')
                  BEGIN
					SET @strSql =@strSql +
                           'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                           --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                           WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                           ELSE 0
                                                                         END,OB.dtFromDate)
                           AND datediff(DAY,dateadd(DAY,CASE 
                                                          --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                          WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                          ELSE 0
                                                        END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90'
					SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 '
                    
                    SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 '
                           
                  END
                ELSE
                  IF (@vcFlag = '90-')
                    BEGIN
                      SET @strSql =@strSql +
                             'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                             --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                             WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																															   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                             ELSE 0
                                                                           END,OB.dtFromDate)
                             AND datediff(DAY,dateadd(DAY,CASE 
                                                            --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                            WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                            ELSE 0
                                                          END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) > 90'
					SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) > 90'
                     
                    SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) > 90'        
                    END
     
            
    SET @strSql =@strSql + @strSql1 +  @strCredit
    SET @strSql=@strSql + ' SELECT *,CASE WHEN DATEDIFF (day ,[dbo].[FormatedDateFromDate](DueDate,' + CONVERT(VARCHAR(10),@numDomainID) + ') ,GETDATE()) > 0 
										  THEN DATEDIFF (day ,[dbo].[FormatedDateFromDate](DueDate,' + CONVERT(VARCHAR(10),@numDomainID) + ') ,GETDATE()) 
										  ELSE NULL 
									 END AS DaysLate,
									 CASE WHEN DATEDIFF (day ,[dbo].[FormatedDateFromDate](DueDate,' + CONVERT(VARCHAR(10),@numDomainID) + ') ,GETDATE()) > 0 
										  THEN + ''' + ' Past Due (' +  ''' + CAST(DATEDIFF (day ,[dbo].[FormatedDateFromDate](DueDate,' + CONVERT(VARCHAR(10),@numDomainID) + ') ,GETDATE()) AS VARCHAR(10))+''' + ')' + '''
										  ELSE NULL 
									 END AS [Status(DaysLate)],
									 CASE WHEN '+ CONVERT(VARCHAR(15),@baseCurrency) +' <> numCurrencyID THEN 2 ELSE 1 END AS CurrencyCount 
							FROM #TempRecords WHERE (ISNULL(BalanceDue,0) > 0 or [numOppId]=-1)
 
   DROP TABLE #TempRecords'
   
	PRINT @strSql
	EXEC(@strSql)
	        
     
  END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAlertPanelDetail')
DROP PROCEDURE dbo.USP_GetAlertPanelDetail
GO
CREATE PROCEDURE [dbo].[USP_GetAlertPanelDetail]
    (
	  @numAlertType INTEGER,
      @numDivisionID NUMERIC(18,0),
      @numContactID NUMERIC(18,0),
	  @numDomainID NUMERIC(18,0)
    )
AS 
BEGIN
	-- A/R BALANCE
	IF @numAlertType = 1
	BEGIN
		SELECT    
			ISNULL(OBD.vcBizDocID,'') AS [Invoice Or Bill],    
			ISNULL(OBD.monDealAmount,0) AS [Amount],
			ISNULL(DATEADD(D,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(OM.intBillingDays,0)),0),OBD.dtFromDate),'') AS [Due Date]
		FROM            
			dbo.OpportunityBizDocs OBD 
		INNER JOIN
			dbo.OpportunityMaster OM 
		ON 
			OBD.numOppId = OM.numOppId
		WHERE        
			OM.numDomainID = @numDomainID
			AND (OM.tintOppType = 1 OR OM.tintOppType = 2)
			AND OM.tintOppStatus = 1
			AND OM.numDivisionId = @numDivisionID
			AND ISNULL(tintshipped,0)=0
		ORDER BY
			ISNULL(DATEADD(D,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(OM.intBillingDays,0)),0),OBD.dtFromDate),'') DESC
	END
	-- OPEN ORDERS
	ELSE IF @numAlertType = 2
	BEGIN
		SELECT
			TOP 20 
			OM.vcPOppName [Order],
			ISNULL((SELECT vcData FROM ListDetails WHERE numListItemID = ISNULL(OM.numStatus,0)),'') AS [Order Status],
			ISNULL(OM.intPEstimatedCloseDate,'') AS [Due Date],
			CAST(ISNULL(numPercentageComplete,0) AS VARCHAR(3)) + ' %' As [Total Progress]
		FROM
			OpportunityMaster OM
		WHERE
			OM.numDomainID = @numDomainID
			AND OM.numDivisionID = @numDivisionID
			AND (OM.tintOppType = 1 OR OM.tintOppType = 2)
			AND OM.tintOppStatus = 1
			AND ISNULL(tintshipped,0)=0
		ORDER BY
			OM.intPEstimatedCloseDate DESC
	END	
	-- OPEN CASES
	ELSE IF @numAlertType = 3
	BEGIN
		SELECT    
			TOP 20 
			vcCaseNumber AS [Case #],
			textSubject AS [Subject],
			textDesc AS [Description],
			ISNULL((SELECT vcData FROM ListDetails WHERE numListItemID = ISNULL(Cases.numStatus,0)),'') AS [Status] 
		FROM            
			dbo.Cases
		WHERE        
			Cases.numstatus <> 136
			AND numDomainID = @numDomainID
			AND numDivisionID = @numDivisionID
		ORDER BY
			Cases.bintCreatedDate DESC
	END
	-- OPEN PROJECT
	ELSE IF @numAlertType = 4
	BEGIN
		SELECT        
			TOP 20
			vcProjectID AS [Project],
			ISNULL((SELECT vcData FROM ListDetails WHERE numListItemID = ISNULL(ProjectsMaster.numProjectStatus,0)),'') AS [Status],
			CAST(ISNULL((SELECT TOP 1 intTotalProgress FROM ProjectProgress WHERE numProId=numProId),0) AS VARCHAR(3)) + ' %' As [Total Progress],
			ISNULL(intDueDate,'') AS [Due Date],
			ISNULL(txtComments,'') AS [Comments]
		FROM            
			dbo.ProjectsMaster
		WHERE        
			numDomainId=@numDomainID
			AND numDivisionId = @numDivisionID
		ORDER BY
			bintCreatedDate DESC
			END
	-- OPEN ACTION ITEM
	ELSE IF @numAlertType = 5
	BEGIN
		SELECT     
			TOP 20 
			ISNULL((SELECT vcData FROM ListDetails WHERE numListItemID = ISNULL(bitTask,0)),'') AS [Type],
			CONCAT(CAST(dtStartTime AS DATE), ' ' ,FORMAT(dtStartTime,'hh:mm tt'),'-',FORMAT(dtEndTime,'hh:mm tt')) AS [Date, From, To],
			ISNULL(textDetails,'') AS [Comments]
		FROM            
			dbo.Communication
		WHERE        
			bitClosedFlag = 0
			AND numDomainID = @numDomainID
			AND numDivisionId = @numDivisionID
			AND numContactId = @numContactID
		ORDER BY
			Communication.dtCreatedDate DESC
	END
END
/****** Object:  StoredProcedure [dbo].[USP_GetChartAcntDetails]    Script Date: 09/25/2009 15:47:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Created by Siva        
-- [USP_GetChartAcntDetails] @numDomainId=72,@dtFromDate='2007-01-01 00:00:00:000',@dtToDate='2009-09-25 00:00:00:000'                            
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getchartacntdetails' ) 
    DROP PROCEDURE usp_getchartacntdetails
GO
CREATE PROCEDURE [dbo].[USP_GetChartAcntDetails]
    @numDomainId AS NUMERIC(9),
    @dtFromDate AS DATETIME,
    @dtToDate AS DATETIME,
    @ClientTimeZoneOffset INT, --Added by Chintan to enable calculation of date according to client machine
    @numAccountClass AS NUMERIC(9)=0
AS 
    BEGIN    
--        DECLARE @numFinYear INT ;
        DECLARE @dtFinYearFromJournal DATETIME ;
		DECLARE @dtFinYearFrom DATETIME ;

--        SET @numFinYear = ( SELECT  numFinYearId
--                            FROM    FINANCIALYEAR
--                            WHERE   dtPeriodFrom <= @dtFromDate
--                                    AND dtPeriodTo >= @dtFromDate
--                                    AND numDomainId = @numDomainId
--                          ) ;
SELECT @dtFinYearFromJournal = MIN(datEntry_Date) FROM view_journal WHERE numDomainID=@numDomainId AND (numAccountClass=@numAccountClass OR @numAccountClass=0)
SET @dtFinYearFrom = ( SELECT   dtPeriodFrom
                        FROM     FINANCIALYEAR
                        WHERE    dtPeriodFrom <= @dtFromDate
                                AND dtPeriodTo >= @dtFromDate
                                AND numDomainId = @numDomainId
                        ) ;

SELECT * INTO #view_journal FROM view_journal WHERE numDomainID=@numDomainId AND (numAccountClass=@numAccountClass OR @numAccountClass=0);

SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate);
/*Timezone Logic.. Stored Date are in UTC/GMT  */
--SET @dtToDate = DATEADD(DAY, DATEDIFF(DAY, '19000101',  @dtToDate), '23:59:59');
--SELECT @dtFromDate,@ClientTimeZoneOffset,DATEADD(minute, @ClientTimeZoneOffset, @dtFromDate)
--        SET @dtFromDate = DATEADD(minute, @ClientTimeZoneOffset, @dtFromDate) ;

        /*Comment by chintan
        Reason: Standard US was facing balance not matching for undeposited func(check video for more info)
        We do not need timezone settings since all journal entry date does not store time. we only store date. 
        */
--        SET @dtToDate = DATEADD(minute, @ClientTimeZoneOffset, @dtToDate) ;


--PRINT @dtFromDate
--PRINT @dtToDate
--PRINT @numFinYear 
--PRINT @dtFinYearFrom 

--select * from view_journal where numDomainid=72

		
        CREATE TABLE #PLSummary
            (
              numAccountId NUMERIC(9),
              vcAccountName VARCHAR(250),
              numParntAcntTypeID NUMERIC(9),
              vcAccountDescription VARCHAR(250),
              vcAccountCode VARCHAR(50) COLLATE Database_Default,
              Opening MONEY,
              Debit MONEY,
              Credit MONEY
              ,bitIsSubAccount BIT,numParentAccID NUMERIC(9),Balance MONEY 
            ) ;

        INSERT  INTO #PLSummary
                SELECT  COA.numAccountId,
                        vcAccountName,
                        numParntAcntTypeID,
                        vcAccountDescription,
                        vcAccountCode,
                      /*  ISNULL(( SELECT SUM(ISNULL(monOpening, 0))
                                 FROM   CHARTACCOUNTOPENING CAO
                                 WHERE  numFinYearId = @numFinYear
                                        AND numDomainID = @numDomainId
                                        AND CAO.numAccountId IN (SELECT numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountCode like COA.vcAccountCode + '%' )
                               ), 0)
                        + */ 
						CASE WHEN COA.[vcAccountCode] LIKE '0103%' OR 
							 	  COA.[vcAccountCode] LIKE '0104%' OR 
							 	  COA.[vcAccountCode] LIKE '0106%' 
							 THEN 0 
							 ELSE isnull(t1.OPENING,0) 
						END as OPENING,
                        ISNULL(t.DEBIT, 0) AS DEBIT,
                        ISNULL(t.CREDIT, 0) AS CREDIT
                               ,ISNULL(COA.bitIsSubAccount,0),ISNULL(COA.numParentAccID,0) AS numParentAccID,0
                FROM    Chart_of_Accounts COA
					OUTER APPLY (SELECT   SUM(Debit - Credit) AS OPENING
                                   FROM     #view_journal VJ
                                   WHERE    VJ.numDomainId = @numDomainId
                                            AND VJ.numAccountID = COA.numAccountID /*AND VJ.COAvcAccountCode like COA.vcAccountCode + '%'*/ /*VJ.numAccountId = COA.numAccountId*/
                                            AND datEntry_Date BETWEEN (CASE WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN @dtFinYearFrom ELSE @dtFinYearFromJournal END )
                                                              AND  DATEADD(Minute,-1,@dtFromDate) AND (VJ.numAccountClass=@numAccountClass OR @numAccountClass=0)) AS t1
					OUTER APPLY(SELECT   SUM(Debit) as DEBIT,SUM(Credit) as CREDIT
                                   FROM   #view_journal VJ
                                 WHERE  VJ.numDomainId = @numDomainId
                                        AND VJ.numAccountID = COA.numAccountID /*AND VJ.COAvcAccountCode like COA.vcAccountCode + '%'*/ /*VJ.numAccountId = COA.numAccountId*/
                                        AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate AND (VJ.numAccountClass=@numAccountClass OR @numAccountClass=0)
                                 ) as t
                    WHERE   COA.numDomainId = @numDomainId  AND COA.bitActive = 1 ;
--SELECT @dtFinYearFrom,@dtFromDate - 1,* FROM #PLSummary
        CREATE TABLE #PLOutPut
            (
              numAccountId NUMERIC(9),
              vcAccountName VARCHAR(250),
              numParntAcntTypeID NUMERIC(9),
              vcAccountDescription VARCHAR(250),
              vcAccountCode VARCHAR(50) COLLATE Database_Default,
              Opening MONEY,
              Debit MONEY,
              Credit MONEY,numParentAccID NUMERIC(9),Balance MONEY 
            ) ;

        INSERT  INTO #PLOutPut
                SELECT  ATD.numAccountTypeID,
                        ATD.vcAccountType,
                        ATD.numParentID,
                        '',
                        ATD.vcAccountCode,
                        ISNULL(SUM(Opening), 0) AS Opening,
                        ISNULL(SUM(Debit), 0) AS Debit,
                        ISNULL(SUM(Credit), 0) AS Credit,0,0
                FROM    /*Chart_Of_Accounts c JOIN [AccountTypeDetail] ATD ON ATD.[numAccountTypeID] = c.[numParntAcntTypeId]*/
						[AccountTypeDetail] ATD
                        RIGHT OUTER JOIN #PLSummary PL ON PL.vcAccountCode LIKE ATD.vcAccountCode + '%' AND LEN(PL.vcAccountCode)>LEN(ATD.vcAccountCode)
                        --(PL.vcAccountCode LIKE c.vcAccountCode OR (PL.vcAccountCode LIKE c.vcAccountCode + '%' AND PL.numParentAccID>0))
                                                          AND ATD.numDomainId = @numDomainId
				--WHERE  PL.bitIsSubAccount=0
                GROUP BY ATD.numAccountTypeID,
                        ATD.vcAccountCode,
                        ATD.vcAccountType,
                        ATD.numParentID ;

ALTER TABLE #PLSummary
DROP COLUMN bitIsSubAccount


-- GETTING P&L VALUE

DECLARE @CURRENTPL MONEY ;
DECLARE @PLOPENING MONEY;
DECLARE @PLCHARTID NUMERIC(8)
DECLARE @TotalIncome MONEY;
DECLARE @TotalExpense MONEY;
DECLARE @TotalCOGS MONEY;
DECLARE  @CurrentPL_COA money
SET @CURRENTPL =0;	
SET @PLOPENING=0;


SELECT @PLCHARTID=COA.numAccountId FROM Chart_of_Accounts COA WHERE numDomainID=@numDomainId and
bitProfitLoss=1;

SELECT @CurrentPL_COA =  ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID
AND numAccountId=@PLCHARTID AND datEntry_Date between  @dtFromDate AND @dtToDate;

SELECT  @TotalIncome= ISNULL(sum(Opening),0) + ISNULL(sum(Credit),0) -ISNULL(sum(Debit),0) FROM
#PLOutPut P WHERE 
vcAccountCode IN ('0103')

SELECT  @TotalExpense=ISNULL(sum(Opening),0)+ ISNULL(sum(Credit),0) -ISNULL(sum(Debit),0) FROM
#PLOutPut P WHERE 
vcAccountCode IN ('0104')

SELECT  @TotalCOGS= ISNULL(sum(Opening),0) + ISNULL(sum(Credit),0) -ISNULL(sum(Debit),0) FROM
#PLOutPut P WHERE 
vcAccountCode IN ('0106')

PRINT 'TotalIncome=									' + CAST(@TotalIncome AS VARCHAR(20))
PRINT 'TotalExpense=									'+ CAST(@TotalExpense AS VARCHAR(20))
PRINT 'TotalCOGS=										'+ CAST(@TotalCOGS AS VARCHAR(20))

SELECT @CURRENTPL = /*@CurrentPL_COA +*/ @TotalIncome + @TotalExpense + @TotalCOGS 
PRINT 'Current Profit/Loss = (Income - expense - cogs)= ' + CAST( @CURRENTPL AS VARCHAR(20))

set @CURRENTPL=@CURRENTPL * (-1)

SELECT @PLOPENING = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID 
AND ( vcAccountCode  LIKE '0103%' or  vcAccountCode  LIKE '0104%' or  vcAccountCode  LIKE '0106%')
AND datEntry_Date <=  DATEADD(Minute,-1,@dtFromDate);

SELECT @PLOPENING = ISNULL(@PLOPENING,0) + ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID
AND numAccountId=@PLCHARTID AND datEntry_Date <=  DATEADD(Minute,-1,@dtFromDate);

PRINT '@PLOPENING='+ CAST(@PLOPENING AS VARCHAR(20))
PRINT '@CURRENTPL='+ CAST(@CURRENTPL AS VARCHAR(20))
PRINT '-(@PLOPENING + @CURRENTPL)='+ CAST((-(@PLOPENING + @CURRENTPL)) AS VARCHAR(20))


set @CURRENTPL=@CURRENTPL * (-1)

UPDATE #PLSummary SET Opening = CASE WHEN vcAccountCode = '0105010101' THEN -@PLOPENING ELSE Opening END,
Balance = CASE WHEN vcAccountCode = '0105010101' THEN -(@PLOPENING + @CURRENTPL) ELSE Opening + Debit - Credit END
--WHERE vcAccountCode = '0105010101'
                        

UPDATE PO SET Opening = T.Opening,
Debit = T.Debit,
Credit = T.Credit,
Balance = T.Balance
FROM #PLOutPut PO JOIN (SELECT PO.vcAccountCode,ISNULL(SUM(PL.Opening), 0) AS Opening,ISNULL(SUM(PL.Debit), 0) AS Debit,ISNULL(SUM(PL.Credit), 0) AS Credit,
/*ISNULL(SUM(PL.Opening), 0) + ISNULL(SUM(PL.Debit), 0) - ISNULL(SUM(PL.Credit), 0)*/ISNULL(SUM(PL.Balance), 0) AS Balance FROM #PLOutPut PO
RIGHT OUTER JOIN #PLSummary PL ON PL.vcAccountCode LIKE PO.vcAccountCode + '%' AND LEN(PL.vcAccountCode)>LEN(PO.vcAccountCode)
GROUP BY PO.vcAccountCode) T ON T.vcAccountCode = PO.vcAccountCode

--SELECT PO.vcAccountCode,ISNULL(SUM(PL.Opening), 0) AS Opening,ISNULL(SUM(PL.Debit), 0) AS Debit,ISNULL(SUM(PL.Credit), 0) AS Credit,
--ISNULL(SUM(PL.Balance), 0) AS Balance FROM #PLOutPut PO
--RIGHT OUTER JOIN #PLSummary PL ON PL.vcAccountCode LIKE PO.vcAccountCode + '%' AND LEN(PL.vcAccountCode)>LEN(PO.vcAccountCode)
--GROUP BY PO.vcAccountCoder

--WHERE vcAccountCode = '0105010101'
--------------------------------------------------------
		--UPDATE #PLSummary SET Opening = @PLOPENING WHERE #PLSummary.vcAccountCode = '0105010101'

		SELECT  P.numAccountId,
				P.vcAccountName,
				P.numParntAcntTypeID, 
				P.vcAccountDescription,
				P.vcAccountCode,
				/*(CASE WHEN P.[vcAccountCode] = '0105010101' THEN -@PLOPENING ELSE P.Opening END) AS*/ P.Opening,
				P.Debit ,
				P.Credit,
				P.numParentAccID,
                /*(CASE WHEN P.[vcAccountCode] = '0105010101' THEN -(@PLOPENING + @CURRENTPL) ELSE Opening + Debit - Credit END) AS*/ P.Balance,
                CASE WHEN LEN(P.[vcAccountCode])>4 THEN REPLICATE('&nbsp;', LEN(P.[vcAccountCode])-4 ) + P.[vcAccountCode] 
				ELSE P.[vcAccountCode]
                END AS AccountCode1,
                CASE WHEN LEN(P.[vcAccountCode]) > 4 THEN REPLICATE('&nbsp;', LEN(P.[vcAccountCode]) - 4 ) + P.[vcAccountName] 
                     ELSE P.[vcAccountName]
                END AS vcAccountName1,
                1 AS TYPE
        FROM    #PLSummary P
                LEFT JOIN dbo.Chart_Of_Accounts C ON C.numAccountId = P.numAccountId
        WHERE   LEN(ISNULL(P.[vcAccountCode], '')) > 2
        UNION
        SELECT  O.numAccountId,
				O.vcAccountName,
				O.numParntAcntTypeID, 
				O.vcAccountDescription,
				O.vcAccountCode,
				/*(CASE WHEN O.[vcAccountCode] = '0105010101' THEN -@PLOPENING ELSE O.Opening END) AS*/ Opening,
				O.Debit ,
				O.Credit,
				O.numParentAccID,
                /*(CASE WHEN O.[vcAccountCode] = '0105010101' THEN -(@PLOPENING + @CURRENTPL) ELSE Opening + Debit - Credit END) AS*/ Balance,
                CASE WHEN LEN(O.[vcAccountCode]) > 4
                     THEN REPLICATE('&nbsp;',LEN(O.[vcAccountCode]) - 4) + O.[vcAccountCode]
                     ELSE O.[vcAccountCode]
                END AS AccountCode1,
                CASE WHEN LEN(O.[vcAccountCode]) > 4
                      THEN REPLICATE('&nbsp;',LEN(O.[vcAccountCode]) - 4) + O.[vcAccountName]
                     ELSE O.[vcAccountName]
                END AS vcAccountName1,
                2 AS TYPE
                --CAST(O.[vcAccountCode] AS deci)  vcSortableAccountCode
        FROM    #PLOutPut O
--                LEFT JOIN dbo.Chart_Of_Accounts C ON C.numAccountId = O.numAccountId
        WHERE   LEN(ISNULL(O.[vcAccountCode], '')) > 2
        ORDER BY vcAccountCode,TYPE 
 
    --SELECT * FROM #PLOutPut ;
    --SELECT * FROM #PLSummary ;

        SELECT  (SUM(Opening) - @PLOPENING) AS OpeningTotal,
                SUM(Debit) AS DebitTotal,
                SUM(Credit) AS CreditTotal,
                (SUM(Opening) + SUM(Debit) - SUM(Credit) - @PLOPENING)AS BalanceTotal
        FROM    [#PLSummary]

        DROP TABLE #PLOutPut ;
        DROP TABLE #PLSummary ;


    END
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

-- Created by Siva    

-- [USP_GetChartAcntDetailsForBalanceSheet] 72,'2008-09-28 17:09:54.263','2009-09-28 17:09:54.263'
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getchartacntdetailsforbalancesheet')
DROP PROCEDURE usp_getchartacntdetailsforbalancesheet
GO
CREATE PROCEDURE [dbo].[USP_GetChartAcntDetailsForBalanceSheet]                                 
@numDomainId as numeric(9),                                                  
@dtFromDate as datetime,                                                
@dtToDate as DATETIME,
@ClientTimeZoneOffset INT, --Added by Chintan to enable calculation of date according to client machine                                                   
@numFRID AS NUMERIC=0,
@numAccountClass AS NUMERIC(9)=0

--@tintByteMode as tinyint                                                            
As                                                                
Begin                                                                
DECLARE @CURRENTPL MONEY ;
DECLARE @PLCHARTID NUMERIC(8);
DECLARE @PLOPENING MONEY;
--DECLARE @numFinYear INT;
--DECLARE @dtFinYearFrom datetime;
DECLARE @TotalIncome MONEY;
DECLARE @TotalExpense MONEY;
DECLARE @TotalCOGS MONEY;


select * into #VIEW_JOURNALBS from VIEW_JOURNALBS where numdomainid=@numDomainId AND (numAccountClass=@numAccountClass OR @numAccountClass=0) ; 
SELECT * INTO #view_journal FROM view_journal WHERE numDomainID=@numDomainId AND (numAccountClass=@numAccountClass OR @numAccountClass=0) ;

--Select Financial Year and From Date
--SELECT @numFinYear=numFinYearId,@dtFinYearFrom=dtPeriodFrom FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND  
--dtPeriodTo >=@dtFromDate AND numDomainId=@numDomainId

--SELECT @dtFromDate=MIN(datEntry_Date) FROM dbo.VIEW_JOURNAL WHERE numDomainID=@numDomainId
--Reflact timezone difference in From and To date
--SET @dtFromDate = DateAdd(minute, @ClientTimeZoneOffset, @dtFromDate);

  /*Comment by chintan
        Reason: Standard US was facing balance not matching for undeposited func(check video for more info)
        We do not need timezone settings since all journal entry date does not store time. we only store date. 
        */
SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate);
PRINT '@dtToDate :' 
PRINT @dtToDate
--PRINT @numFinYear
--PRINT @dtFinYearFrom
PRINT @dtFromDate
--RETURN
--select * from VIEW_JOURNALPL where numDomainid=72

--------------------------------------------------------
-- GETTING P&L VALUE
SET @CURRENTPL =0;	
SET @PLOPENING=0;

--SELECT @CURRENTPL = /*ISNULL(SUM(Opening),0)+*/
--+
--ISNULL(sum(Credit),0)- ISNULL(sum(Debit),0) FROM
--VIEW_DAILY_INCOME_EXPENSE P WHERE 
--numDomainID=@numDomainId AND datEntry_Date between @dtFromDate and @dtToDate ;


SELECT @TotalIncome = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID AND vcAccountCode  LIKE '0103%' AND datEntry_Date between @dtFromDate and @dtToDate ;
SELECT @TotalExpense = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID AND vcAccountCode  LIKE '0104%' AND datEntry_Date between @dtFromDate and @dtToDate ;
SELECT @TotalCOGS =  ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID AND vcAccountCode  LIKE '0106%' AND datEntry_Date between @dtFromDate and @dtToDate ;
PRINT 'TotalIncome=									' + CAST(@TotalIncome AS VARCHAR(20))
PRINT 'TotalExpense=									'+ CAST(@TotalExpense AS VARCHAR(20))
PRINT 'TotalCOGS=										'+ CAST(@TotalCOGS AS VARCHAR(20))
SET @CURRENTPL = (@TotalIncome + @TotalExpense + @TotalCOGS)
PRINT 'Current Profit/Loss = (Income - expense - cogs)= ' + CAST( @CURRENTPL AS VARCHAR(20))



PRINT @CURRENTPL


SELECT @PLCHARTID = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId=@numDomainID AND bitProfitLoss=1
PRINT 'PL Account ID=' +CAST( @PLCHARTID AS VARCHAR(20))
SELECT @PLOPENING = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID 
/* AND numAccountId=@PLCHARTID*/
AND ( vcAccountCode  LIKE '0103%' or  vcAccountCode  LIKE '0104%' or  vcAccountCode  LIKE '0106%')
AND datEntry_Date <=  DATEADD(Minute,-1,@dtFromDate);

SELECT @PLOPENING = ISNULL(@PLOPENING,0) + ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID
AND numAccountId=@PLCHARTID /*AND datEntry_Date <=  DATEADD(Minute,-1,@dtFromDate);*/


PRINT 'PL Account Opening=' +CAST( @PLOPENING AS VARCHAR(20))
SET @CURRENTPL = @PLOPENING + @CURRENTPL
PRINT 'PL = ' + CAST( @CURRENTPL AS VARCHAR(20))






CREATE TABLE #PLSummary (numAccountId numeric(9),vcAccountName varchar(250),
numParntAcntTypeID numeric(9),vcAccountDescription varchar(250),
vcAccountCode varchar(50) COLLATE Database_Default,Opening money,Debit money,Credit Money,bitIsSubAccount Bit);

INSERT INTO  #PLSummary
SELECT COA.numAccountId,vcAccountName,numParntAcntTypeID,vcAccountDescription,vcAccountCode,


/*isnull((SELECT SUM(isnull(monOpening,0)) from CHARTACCOUNTOPENING CAO WHERE
numFinYearId=@numFinYear and numDomainID=@numDomainId 
AND CAO.numAccountId IN (SELECT numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountCode like COA.vcAccountCode + '%' )  ),0) 

+*/

/*ISNULL((SELECT sum(ISNULL(Debit,0)-ISNULL(Credit,0)) FROM #VIEW_JOURNALBS VJ
WHERE VJ.numDomainId=@numDomainId AND
	VJ.COAvcAccountCode like COA.vcAccountCode + '%' /*VJ.numAccountId=COA.numAccountId*/ AND
	datEntry_Date <= DATEADD(Minute,-1,@dtFromDate)),0)*/ 0 AS OPENING,

ISNULL((SELECT sum(ISNULL(Debit,0)) FROM #VIEW_JOURNALBS VJ
WHERE VJ.numDomainId=@numDomainId AND
	VJ.COAvcAccountCode like COA.vcAccountCode + '%' /*VJ.numAccountId=COA.numAccountId*/ AND
	datEntry_Date <= @dtToDate ),0) as DEBIT,

ISNULL((SELECT sum(ISNULL(Credit,0)) FROM #VIEW_JOURNALBS VJ
WHERE VJ.numDomainId=@numDomainId AND
	VJ.COAvcAccountCode like COA.vcAccountCode + '%' /*VJ.numAccountId=COA.numAccountId*/ AND
	datEntry_Date <= @dtToDate ),0) as CREDIT
,ISNULL(COA.bitIsSubAccount,0)
FROM Chart_of_Accounts COA
WHERE COA.numDomainId=@numDomainId  AND COA.bitActive = 1  AND
		ISNULL(bitProfitLoss,0)=0 AND
      (COA.vcAccountCode LIKE '0101%' OR
       COA.vcAccountCode LIKE '0102%' OR
       COA.vcAccountCode LIKE '0105%') 

UNION


/*(* -1) added by chintan to fix bug 2148 #3  */
SELECT @PLCHARTID,vcAccountName,numParntAcntTypeID,vcAccountDescription,
vcAccountCode,
@CURRENTPL,0,0,ISNULL(COA.bitIsSubAccount,0)
 FROM Chart_of_Accounts COA WHERE numDomainID=@numDomainId AND COA.bitActive = 1 and
bitProfitLoss=1 AND COA.numAccountId=@PLCHARTID;


/*Added by chintan bugid 962 #3, For presentation do follwoing Show Credit amount in Posite and Debit Amount in Negative (only for accounts under quaity except PL account)*/
UPDATE #PLSummary SET 
Opening =Opening * (-1),--CASE WHEN Opening <0 THEN Opening * (-1) ELSE Opening END ,
Debit = Debit * (-1),--CASE WHEN Debit >0 THEN Debit * (-1) ELSE Debit END,
Credit = Credit * (-1)--CASE WHEN Credit <0 THEN Credit * (-1) ELSE Credit END 
WHERE vcAccountCode LIKE '0105%' AND numAccountId <> @PLCHARTID;



CREATE TABLE #PLOutPut (numAccountId numeric(9),vcAccountName varchar(250),
numParntAcntTypeID numeric(9),vcAccountDescription varchar(250),
vcAccountCode varchar(50) COLLATE Database_Default,Opening money,Debit money,Credit Money);
--insert account types and balances
INSERT INTO #PLOutPut
SELECT ATD.numAccountTypeID,ATD.vcAccountType,ATD.numParentID, '',ATD.vcAccountCode,
 ISNULL(SUM(ISNULL(Opening,0)),0) as Opening,
ISNUlL(Sum(ISNULL(Debit,0)),0) as Debit,ISNULL(Sum(ISNULL(Credit,0)),0) as Credit
FROM 
 AccountTypeDetail ATD RIGHT OUTER JOIN 
#PLSummary PL ON
PL.vcAccountCode LIKE ATD.vcAccountCode + '%'
AND ATD.numDomainId=@numDomainId AND
(ATD.vcAccountCode LIKE '0101%' OR
       ATD.vcAccountCode LIKE '0102%' OR
       ATD.vcAccountCode LIKE '0105%')
WHERE PL.bitIsSubAccount=0
GROUP BY 
ATD.numAccountTypeID,ATD.vcAccountCode,ATD.vcAccountType,ATD.numParentID;

ALTER TABLE #PLSummary
DROP COLUMN bitIsSubAccount

CREATE TABLE #PLShow (numAccountId numeric(9),vcAccountName varchar(250),
numParntAcntTypeID numeric(9),vcAccountDescription varchar(250),
vcAccountCode varchar(50),Opening money,Debit money,Credit Money,
Balance Money,AccountCode1  varchar(100),vcAccountName1 varchar(250),[Type] INT);





INSERT INTO #PLShow(#PLShow.numAccountId,
					#PLShow.vcAccountName,
					#PLShow.numParntAcntTypeID,
					#PLShow.vcAccountDescription,
					#PLShow.vcAccountCode,
					#PLShow.Opening,
					#PLShow.Debit,
					#PLShow.Credit,
					#PLShow.Balance,
					#PLShow.AccountCode1,
					#PLShow.vcAccountName1,
					#PLShow.[Type])	

 
SELECT numAccountId,
	   vcAccountName,
	   numParntAcntTypeID,
	   vcAccountDescription,
	   vcAccountCode,
	   Opening,
	   Debit,
	   Credit,(Opening * case substring(P.[vcAccountCode],1,4) when '0102' then (-1) else 1 end)
+(Debit * case substring(P.[vcAccountCode],1,4) when '0102' then (-1) else 1 end )
-(Credit * case substring(P.[vcAccountCode],1,4) when '0102' then (-1) else 1 end) as Balance,
CASE 
 WHEN LEN(P.[vcAccountCode])>4 THEN REPLICATE('&nbsp;', LEN(P.[vcAccountCode])-4) + P.[vcAccountCode] 
 ELSE  P.[vcAccountCode]
 END AS AccountCode1,
 CASE 
 WHEN LEN(P.[vcAccountCode])>4 THEN REPLICATE('&nbsp;', LEN(P.[vcAccountCode])-4) + P.[vcAccountName]
 ELSE P.[vcAccountName]
 END AS vcAccountName1, 1 AS Type
 FROM #PLSummary P

UNION

SELECT numAccountId,
	   vcAccountName,
	   numParntAcntTypeID,
	   vcAccountDescription,
	   vcAccountCode,
	   Opening,
	   Debit,
	   Credit,(Opening  * case substring(O.[vcAccountCode],1,4) when '0102' then (-1) else 1 end)+
(Debit * case substring(O.[vcAccountCode],1,4) when '0102' then (-1) else 1 end)-
(Credit * case substring(O.[vcAccountCode],1,4) when '0102' then (-1) else 1 end) as Balance,

CASE 
 WHEN LEN(O.[vcAccountCode])>4 THEN REPLICATE('&nbsp;', LEN(O.[vcAccountCode])-4) +O.[vcAccountCode] 
 ELSE  O.[vcAccountCode]
 END AS AccountCode1,
 CASE 
 WHEN LEN(O.[vcAccountCode])>4 THEN REPLICATE('&nbsp;', LEN(O.[vcAccountCode])-4) + O.[vcAccountName]
 ELSE O.[vcAccountName]
 END AS vcAccountName1, 2 as Type
 FROM #PLOutPut O;

UPDATE #PLShow SET [TYPE]=3 WHERE numAccountId=@PLCHARTID;




select  A.numAccountId ,
        A.vcAccountName ,
        A.numParntAcntTypeID ,
        A.vcAccountDescription ,
        A.vcAccountCode ,
        A.Opening ,
        A.Debit ,
        A.Credit ,
        A.Balance ,
        A.AccountCode1 ,
        CASE WHEN LEN(A.[vcAccountCode]) > 4
                     THEN REPLICATE('&nbsp;',LEN(A.[vcAccountCode]) - 4 )+ A.[vcAccountName]
                     ELSE A.[vcAccountName]
                END AS vcAccountName1, A.vcAccountCode ,
        A.Type	from #PLShow A ORDER BY A.vcAccountCode;

DROP TABLE #PLOutPut;
DROP TABLE #PLSummary;
DROP TABLE #PLShow;
DROP TABLE #VIEW_JOURNALBS;
 End

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCommissionRule')
DROP PROCEDURE USP_GetCommissionRule
GO
CREATE PROCEDURE [dbo].[USP_GetCommissionRule]
    @numComRuleID AS NUMERIC(9) = 0,
    @byteMode AS TINYINT,
    @numDomainID AS NUMERIC(9) = 0
AS 
   IF @byteMode = 0 
        BEGIN    
            SELECT  
				[numDomainID],
				[numComRuleID],
                [vcCommissionName],
                ISNULL([tinComDuration], 0) tinComDuration,
                ISNULL([tintComBasedOn], 0) tintComBasedOn,
                ISNULL([tintComType], 0) tintComType,
				ISNULL([tintComAppliesTo], 0) tintComAppliesTo,
				CASE ISNULL([tintComAppliesTo], 0)
					WHEN 1 THEN (SELECT COUNT(*) FROM [CommissionRuleItems] CI WHERE CI.[numComRuleID] = CR.numComRuleID AND CI.tintType=1)
					WHEN 2 THEN (SELECT COUNT(*) FROM [CommissionRuleItems] CI WHERE CI.[numComRuleID] = CR.numComRuleID AND CI.tintType=2)
                END AS ItemCount,
				ISNULL([tintComOrgType],0) tintComOrgType,
                CASE WHEN (ISNULL([tintComOrgType], 0) = 1 OR ISNULL([tintComOrgType], 0) = 2) THEN
					(SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID= @numComRuleID)
                END AS OrganizationCount,
				ISNULL(tintAssignTo,0) AS tintAssignTo
            FROM    CommissionRules CR
            WHERE   numComRuleID = @numComRuleID    
        END    
    ELSE IF @byteMode = 1
            BEGIN    
                SELECT  
					PB.numComRuleID,
                    PB.vcCommissionName,
					ISNULL(PP.Priority,'') AS [Priority],
					PB.tintComBasedOn,
					PB.tinComDuration,
					PB.tintComType,
                    CASE 
						WHEN tintComAppliesTo = 3 THEN 'All Items'
						WHEN tintComAppliesTo IN (1,2) THEN dbo.GetCommissionContactItemList(PB.numComRuleID,PB.tintComAppliesTo)
						ELSE ''
                    END AS ItemList,
                    CASE WHEN tintComOrgType = 3 THEN 'All Customers'
                            ELSE dbo.GetCommissionContactItemList(PB.numComRuleID, CASE [tintComOrgType]
                                                                            WHEN 1 THEN 3
                                                                            WHEN 2 THEN 4
                                                                        END)
					END CustomerList,
                    dbo.GetCommissionContactItemList(PB.[numComRuleID],0) AS ContactList
                FROM    CommissionRules PB 
				LEFT OUTER JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = PB.tintComAppliesTo AND PP.[Step3Value] = PB.[tintComOrgType]
                WHERE   PB.numDomainID = @numDomainID
                ORDER BY vcCommissionName ASC
            END
     
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_GetCommissionRuleContacts')
DROP PROCEDURE usp_GetCommissionRuleContacts
GO
CREATE PROCEDURE [dbo].[usp_GetCommissionRuleContacts]
@numComRuleID AS NUMERIC
AS
  
   select cast(CC.numValue AS VARCHAR(10)) + '~' + CAST(ISNULL(CC.bitCommContact,0) AS VARCHAR(1)) AS numContactID,
   CONCAT(dbo.fn_GetContactName(numValue),CASE CC.bitCommContact WHEN 1 THEN '(' + ISNULL((SELECT CompanyInfo.vcCompanyName FROM AdditionalContactsInformation ACI INNER JOIN DivisionMaster DM ON ACI.numDivisionId=DM.numDivisionID INNER JOIN CompanyInfo ON DM.numCompanyID = CompanyInfo.numCompanyId WHERE ACI.numContactId=CC.numValue),'') + ')' ELSE '' END) as vcUserName
 from CommissionRuleContacts CC where CC.numComRuleID=@numComRuleID   
	
GO
/****** Object:  StoredProcedure [dbo].[USP_GetCommissionRuleItems]    Script Date: 07/26/2008 16:17:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
-- USP_GetCommissionRuleItems 366,72,1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCommissionRuleItems')
DROP PROCEDURE USP_GetCommissionRuleItems
GO
CREATE PROCEDURE [dbo].[USP_GetCommissionRuleItems]    
@numRuleID as numeric(9)=0,    
@numDomainID as numeric(9)=0,    
@tintRuleAppType as tinyint    
as   
  
     
  
if @tintRuleAppType=1    
begin   

 select [numComRuleItemID],[numComRuleID], vcItemName,[txtItemDesc],[vcModelID], dbo.[GetListIemName]([numItemClassification]) ItemClassification from item   
 join [CommissionRuleItems]  
 on numValue=numItemCode  
 where numDomainID=@numDomainID and numComRuleID=@numRuleID and tintType=1
    
end    
if @tintRuleAppType=2    
begin    

  SELECT [numListItemID] numItemClassification,ISNULL([vcData],'-') ItemClassification  FROM [ListDetails] WHERE [numListID]=36 AND [numDomainID]=@numDomainID AND [numListItemID] NOT IN (select numValue from [CommissionRuleItems] where tintType=2 and numComRuleID=@numRuleID)
  
 select [numComRuleItemID],[numComRuleID],[numValue],dbo.[GetListIemName]([numValue]) ItemClassification,(SELECT COUNT(*) FROM item I1 WHERE I1.numItemClassification=[numValue]) AS ItemsCount from 
 [CommissionRuleItems]   
 where numComRuleID=@numRuleID and tintType=2
 
end
IF @tintRuleAppType=3
BEGIN
select numComRuleOrgID,[numComRuleID],vcCompanyName,dbo.[GetListIemName](dbo.CompanyInfo.numCompanyType) +',' + dbo.[GetListIemName](dbo.CompanyInfo.vcProfile) Relationship,
 (SELECT ISNULL(vcFirstName,'') + ' ' + ISNULL(vcLastName,'') + ', ' + ISNULL(vcEmail,'')  FROM [AdditionalContactsInformation] A WHERE A.[numDivisionID]=[DivisionMaster].[numDivisionID] AND ISNULL(A.bitPrimaryContact,0)=1) AS PrimaryContact  from DivisionMaster  
 join Companyinfo on DivisionMaster.numCompanyID=Companyinfo.numCompanyID  
 JOIN CommissionRuleOrganization ON numValue=DivisionMaster.numDivisionID  
 WHERE DivisionMaster.numDomainID=@numDomainID and numComRuleID=@numRuleID
END
IF @tintRuleAppType=4
BEGIN
 select numComRuleOrgID,numComRuleID,L1.vcData+' - '+L2.vcData as RelProfile from CommissionRuleOrganization DTL
 Join ListDetails L1  
 on L1.numListItemID=DTL.numValue  
 Join ListDetails L2  
 on L2.numListItemID=DTL.numProfile  
 where numComRuleID=@numRuleID
END  
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCommunicationInfo]    Script Date: 07/26/2008 16:16:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Modified By Anoop Jayaraj                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcommunicationinfo')
DROP PROCEDURE usp_getcommunicationinfo
GO
CREATE PROCEDURE [dbo].[USP_GetCommunicationInfo]                                              
 @numCommid numeric(9)=0,                            
 @ClientTimeZoneOffset Int                        
AS                          
DECLARE @numFollowUpStatus NUMERIC
SELECT @numFollowUpStatus = [numFollowUpStatus] FROM [DivisionMaster] WHERE [numDivisionID] IN (SELECT [numDivisionID] FROM [Communication] WHERE [numCommId]=@numCommid)
SELECT comm.numCommId, comm.bitTask, bitOutlook,                        
comm.numContactId,                        
comm.numDivisionId, ISNULL(comm.textDetails,'') AS [textDetails], comm.intSnoozeMins, comm.intRemainderMins,                        
comm.numStatus, comm.numActivity, comm.numAssign, comm.tintSnoozeStatus,                        
comm.tintRemStatus, comm.numOppId, comm.numCreatedBy,                        
comm.numModifiedBy, comm.bitClosedFlag, comm.vcCalendarName,                         
DateAdd(minute, -@ClientTimeZoneOffset,dtStartTime ) as dtStartTime,            
DateAdd(minute, -@ClientTimeZoneOffset,dtEndTime ) as dtEndTime,                        
'-' as AssignTo,                            
isnull(dbo.fn_GetContactName(comm.numCreatedBy),'-')+' '+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,Comm.dtCreatedDate )) as CreatedBy,                            
isnull(dbo.fn_GetContactName(comm.numModifiedBy),'-')+' '+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,Comm.dtModifiedDate )) as ModifiedBy,                            
isnull(dbo.fn_GetContactName(comm.numCreatedBy),'-') as RecOwner,                            
convert(varchar(20),dtModifiedDate) as ModifiedDate,        
isnull(bitSendEmailTemp,0) as bitSendEmailTemp,        
isnull(numEmailTemplate,0) as numEmailTemplate,        
isnull(tintHours,0) as tintHours,        
isnull(bitAlert,0) as bitAlert,      
Comm.caseid,        
(select top 1 vcCaseNumber from cases where cases.numcaseid= Comm.caseid )as vcCaseName,             
isnull(caseTimeid,0)as caseTimeid,        
isnull(caseExpid,0)as caseExpid   ,    
numActivityId,
isnull(C.[numCorrespondenceID],0) numCorrespondenceID,
ISNULL(C.[numOpenRecordID],0) numOpenRecordID,
ISNULL(C.[tintCorrType],0) tintCorrType,
ISNULL(@numFollowUpStatus,0) AS numFollowUpStatus,ISNULL(Comm.bitFollowUpAnyTime,0) bitFollowUpAnyTime,isnull(C.monMRItemAmount,0) as monMRItemAmount,
ISNULL(clo.numDivisionID,0) AS numLinkedOrganization,
ISNULL(clo.numContactID,0) AS numLinkedContact
FROM communication Comm LEFT OUTER JOIN [Correspondence] C ON C.[numCommID]=Comm.[numCommId]
LEFT JOIN CommunicationLinkedOrganization clo ON Comm.numCommId = clo.numCommID
WHERE  comm.numCommid=@numCommid
GO
/****** Object:  StoredProcedure [dbo].[Usp_getCorrespondance]    Script Date: 07/26/2008 16:17:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcorrespondance')
DROP PROCEDURE usp_getcorrespondance
GO
CREATE PROCEDURE [dbo].[Usp_getCorrespondance]                          
@FromDate as datetime,                          
@toDate as datetime,                          
@numContactId as numeric,                          
@vcMessageFrom as varchar(200)='' ,                        
@tintSortOrder as numeric,                        
@SeachKeyword as varchar(100),                        
@CurrentPage as numeric,                        
@PageSize as numeric,                        
@TotRecs as numeric output,                        
@columnName as varchar(100),                        
@columnSortOrder as varchar(50),                        
@filter as numeric,                        
@numdivisionId as numeric =0,              
@numDomainID as numeric(9)=0,              
@numCaseID as numeric(9)=0,              
@ClientTimeZoneOffset INT,
@numOpenRecordID NUMERIC=0,
@tintMode TINYINT=0,
@bitOpenCommu BIT=0
as 

set @vcMessageFrom=replace(@vcMessageFrom,'''','|')
                         
if @filter>0 set @tintSortOrder=  @filter
               
if ((@filter <=2 and @tintSortOrder<=2) or (@tintSortOrder>2 and @filter>2))                 
begin  
                                                                      
 declare @strSql as varchar(8000)       
                                  
  declare @firstRec as integer                                                        
  declare @lastRec as integer                                                        
  set @firstRec= (@CurrentPage-1) * @PageSize                                                        
  set @lastRec= (@CurrentPage*@PageSize+1)                 
  set @strSql=''                
 if (@filter <=2 and @tintSortOrder<=2 and @bitOpenCommu=0)                 
 begin     
 declare @strCondition as varchar(4500);
 declare @vcContactEmail as varchar(100);
              
  if @numdivisionId =0 and  @vcMessageFrom <> '' 
 BEGIN 
	If @filter = 1 --ReceivedMessages
		set  @strCondition=' and (vcFrom like ''%'+ @vcMessageFrom+'%'')  ' 
	ELSE IF @filter=2 --SentMessages
		set  @strCondition=' and (vcTo like ''%'+ @vcMessageFrom+'%'')  ' 
	ELSE
		set  @strCondition=' and (vcFrom like ''%'+ @vcMessageFrom+'%'' or vcTo like ''%'+ @vcMessageFrom+'%'')  ' 
 END
  else if @numdivisionId <>0 
 BEGIN

 /* This approach does not work as numEmailId stores who sent email, id of sender
  set @strCondition=' and HDR.numEmailID in (
  SELECT numEmailID FROM dbo.EmailMaster WHERE numDomainID= ' + CONVERT(VARCHAR(15),@numDomainID) +' AND numContactID 
  IN (SELECT numcontactid FROM dbo.AdditionalContactsInformation WHERE numDivisionID = ' + convert(varchar(15),@numDivisionID) + ' AND numDomainID = ' + CONVERT(VARCHAR(15),@numDomainID) +' ))';
*/
-- bug fix id 1591,1628 
  DECLARE @TnumContactID NUMERIC(9);set @strCondition=''

  select TOP 1 @TnumContactID=numContactID,@vcContactEmail=replace(vcEmail,'''','''''') from AdditionalContactsInformation where numdivisionid=@numdivisionId and len(vcEmail)>2
  ORDER BY numContactID ASC
  
  WHILE @TnumContactID>0
  BEGIN
   if len(@strCondition)>0
    SET @strCondition=@strCondition + ' or '

	If @filter = 1 --ReceivedMessages
		set  @strCondition=' and (vcFrom like ''%'+ @vcContactEmail+'%'')  ' 
	ELSE IF @filter=2 --SentMessages
		set  @strCondition=' and (vcTo like ''%'+ @vcContactEmail+'%'')  ' 
	ELSE
		set  @strCondition=' and (vcFrom like ''%'+ @vcContactEmail+'%'' or vcTo like ''%'+ @vcContactEmail+'%'')  '

   select TOP 1 @TnumContactID=numContactID,@vcContactEmail=vcEmail from AdditionalContactsInformation where numdivisionid=@numdivisionId and len(vcEmail)>2 and numContactID>@TnumContactID
   ORDER BY numContactID ASC

   IF @@rowcount = 0 SET @TnumContactID = 0 
  END
 
  if len(@strCondition)>0
   SET @strCondition= @strCondition
  else
   SET @strCondition=' and (1=0)' 
  --set @strCondition=' and DTL.numEmailId  in (select numEmailId from EmailMaster where vcEmailID in (select vcEmail  from AdditionalContactsInformation where numdivisionid='''+ convert(varchar(15),@numdivisionId)+''' ))    '          
  END
  else if @numContactId>0 
 BEGIN
  select @vcContactEmail=isnull(replace(vcEmail,'''',''''''),'') from AdditionalContactsInformation where numContactID=@numContactId and len(vcEmail)>2

  if len(@vcContactEmail)>0
	If @filter = 1 --ReceivedMessages
		set  @strCondition=' and (vcFrom like ''%'+ @vcContactEmail +'%'')  ' 
	ELSE IF @filter=2 --SentMessages
		set  @strCondition=' and (vcTo like ''%'+ @vcContactEmail+'%'')  ' 
	ELSE
		set  @strCondition=' and (vcFrom like ''%'+ @vcContactEmail+'%'' or vcTo like ''%'+ @vcContactEmail+'%'')' 
  else
   SET @strCondition=' and (1=0)'
 END
 else 
  SET @strCondition=''
    
  set @strSql= '                        
  SELECT distinct(HDR.numEmailHstrID),convert(varchar(15),HDR.numEmailHstrID)+''~1~0''+convert(varchar(15),HDR.numUserCntId) as DelData,HDR.bintCreatedOn,
  dbo.FormatedDateTimeFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', bintCreatedOn),'+convert(varchar(15),@numDomainID)+') as [date],                
  vcSubject as [Subject],                          
  ''Email'' as [type] ,                          
  '''' as phone,'''' as assignedto ,                          
  ''0'' as caseid,                             
  null as vcCasenumber,                                 
  ''0''as caseTimeid,                                
  ''0''as caseExpid ,                          
  hdr.tinttype ,HDR.bintCreatedOn as dtCreatedDate,vcFrom + '','' + vcTo as [From] ,0 as bitClosedflag,ISNULL(HDR.bitHasAttachments,0) bitHasAttachments,
 CASE WHEN LEN(Cast(vcBodyText AS VARCHAR(1000)))>150 THEN SUBSTRING(vcBodyText,0,150) + ''...'' ELSE
   Cast(vcBodyText AS VARCHAR(1000)) END AS vcBody                     
  from EmailHistory HDR                                 
  LEFT JOIN [Correspondence] CR ON CR.numEmailHistoryID = HDR.numEmailHstrID '                    
   set  @strSql=@strSql +' where ISNULL(HDR.bitInvisible,0) = 0 AND HDR.numDomainID='+convert(varchar(15),@numDomainID)+' and  (bintCreatedOn between '''+ convert(varchar(30),@FromDate)+''' and '''+ convert(varchar(30),@ToDate)+''') '
   + ' AND (CR.numOpenRecordID = '+ convert(varchar(15),@numOpenRecordID) +' OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  
 set  @strSql=@strSql+ @strCondition
--  if @numdivisionId =0 and  @vcMessageFrom <> '' set  @strSql=@strSql+ 'and (vcFrom like ''%'+ @vcMessageFrom+'%'' or vcTo like ''%'+ @vcMessageFrom+'%'')  '                      
--  else if @numdivisionId <>0 
-- BEGIN
--  set @strSql=@strSql+ 'and DTL.numEmailId  in (select numEmailId from EmailMaster where vcEmailID in (select vcEmail  from AdditionalContactsInformation where numdivisionid='''+ convert(varchar(15),@numdivisionId)+''' ))    '          
--  END
--  else if @numContactId>0 
-- BEGIN
--  declare @vcContactEmail as varchar(100);
--  select @vcContactEmail=vcEmail from AdditionalContactsInformation where numContactID=@numContactId
--
--  set  @strSql=@strSql+ 'and (vcFrom like ''%'+ @vcContactEmail+'%'' or vcTo like ''%'+ @vcContactEmail+'%'')    ' 
-- END

  IF @tintMode=1 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(1,3) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=2 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(2,4) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=5 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(5) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=6 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(6) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  
  if @filter =0 set  @strSql=@strSql+ ' and (hdr.tinttype=1 or hdr.tinttype=2 or  hdr.tinttype=5)'                  
  else if @filter =1 OR @filter =2 set  @strSql=@strSql+ ' and (hdr.tinttype=1 OR hdr.tinttype=2)'                                             
  if @filter > 2 set  @strSql=@strSql+ ' and hdr.tinttype=0'                 
                        
  if @tintSortOrder =0 and @SeachKeyword <> ''                 
  begin                
   set  @strSql=@strSql+ ' and (vcFrom like ''%'+ @SeachKeyword+'%'' or vcTo like ''%'+ @SeachKeyword+'%'' or vcsubject like ''%'+@SeachKeyword+'%'' or vcBody like ''%'+@SeachKeyword+'%'')'               
  end                
  if @tintSortOrder =1 and @SeachKeyword <> ''                 
  begin                
     set  @strSql=@strSql+ ' and (vcFrom like ''%'+ @SeachKeyword+'%'' or vcsubject like ''%'+@SeachKeyword+'%'' or  vcBody like ''%'+@SeachKeyword+'%'')'                 
  end                
  if @tintSortOrder =2 and @SeachKeyword <> ''                 
  begin                
     set  @strSql=@strSql+ ' and (vcTo like ''%'+ @SeachKeyword+'%'' or vcsubject like ''%'+@SeachKeyword+'%'' or  vcBody like ''%'+@SeachKeyword+'%'')'                 
  end                            
 end                
 if (@filter =0 and @tintSortOrder=0 and @bitOpenCommu=0) set  @strSql=@strSql+ ' union '                
                
 if ((@filter=0 or @filter>2) and (@tintSortOrder=0 or @tintSortOrder>2))                
 begin                
  set  @strSql=@strSql+ '                     
  SELECT c.numCommId as numEmailHstrID,convert(varchar(15),C.numCommId)+''~2~''+convert(varchar(15),C.numCreatedBy) as DelData,CASE WHEN c.bitTask = 973 THEN dtCreatedDate WHEN c.bitTask = 974 THEN dtCreatedDate WHEN c.bitTask = 971 THEN dtStartTime ELSE dtEndTime END AS bintCreatedOn,
  case when c.bitTask=973 then dbo.FormatedDateTimeFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtCreatedDate),'+convert(varchar(15),@numDomainID)+') 
  when c.bitTask=974 then dbo.FormatedDateTimeFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtEndTime),'+convert(varchar(15),@numDomainID)+') 
  when c.bitTask=971 THEN dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtEndTime),'+convert(varchar(15),@numDomainID)+') + '' '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtStartTime]),100),7)  +'' - '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtEndTime]),100),7)
  else  dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtEndTime),'+convert(varchar(15),@numDomainID)+') + '' '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtStartTime]),100),7)  +'' - '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtEndTime]),100),7)  END AS [date],
  convert(varchar(8000),textdetails) as [Subject],                                                                         
 dbo.fn_GetListItemName(c.bitTask)  AS [Type],                                                                 
  A1.vcFirstName +'' ''+ A1.vcLastName +'' ''+                                      
  case when A1.numPhone<>'''' then + A1.numPhone +case when A1.numPhoneExtension<>'''' then '' - '' + A1.numPhoneExtension else '''' end  else '''' end as [Phone],                        
  A2.vcFirstName+ '' ''+A2.vcLastName  as assignedto,                         
  c.caseid,                           
  (select top 1 vcCaseNumber from cases where cases.numcaseid= c.caseid )as vcCasenumber,                               
  isnull(caseTimeid,0)as caseTimeid,                              
  isnull(caseExpid,0)as caseExpid  ,                        
  1 as tinttype ,dtCreatedDate,'''' as [FROM]     ,isnull(C.bitClosedflag,0) as bitClosedflag,0 as bitHasAttachments,
'''' as vcBody
  from  Communication C                  
  JOIN AdditionalContactsInformation  A1                                           
  ON c.numContactId = A1.numContactId
  LEFT JOIN CommunicationLinkedOrganization clo ON C.numCommID=clo.numCommID                                                                             
  left join AdditionalContactsInformation A2 on A2.numContactID=c.numAssign
  left join UserMaster UM on A2.numContactid=UM.numUserDetailId and isnull(UM.bitActivateFlag,0)=1                                                                           
  left join listdetails on numActivity=numListID                           
  LEFT JOIN [Correspondence] CR ON CR.numCommID = C.numCommID
  where  C.numDomainID=' +convert(varchar(15),@numDomainID)                       
  + 'AND (numOpenRecordID = '+ convert(varchar(15),@numOpenRecordID) +' OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  if (@numdivisionId =0 and @numContactId<>0) set  @strSql=@strSql+' and C.numContactId='+ convert(varchar(15),@numContactId)                        
  if @numdivisionId <> 0 set  @strSql=@strSql+' and  (C.numDivisionId='+ convert(varchar(15),@numdivisionId) + ' OR clo.numDivisionID='+ convert(varchar(15),@numdivisionId) + ') '             
  if @numCaseID>0  set  @strSql=@strSql+' and  C.CaseId='+ convert(varchar(15),@numCaseID)                       
  if @filter =0  
  BEGIN
	IF @tintMode <> 9
	BEGIN
		PRINT @tintMode
		SET @strSql=@strSql+ ' and dtStartTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtStartTime  <= '''+ convert(varchar(30),@ToDate)  +''''
	END
  END
  else
    SET @strSql=@strSql+ ' and c.bitTask=' + CONVERT(varchar(12),@filter) + ' and ((dtStartTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtStartTime  <= '''+ convert(varchar(30),@ToDate)  +''') OR (dtEndTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtEndTime  <= '''+ convert(varchar(30),@ToDate)  +'''))'
  
  IF @tintMode=1 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(1,3) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=2 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(2,4) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=5 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(5) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=6 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(6) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
                          
  IF @tintSortOrder =0 and @SeachKeyword <>'' 
	SET @strSql=@strSql+ ' and textdetails like ''%'+@SeachKeyword+'%'''
  ELSE
	IF ISNULL(@filter,0) = 0 
	BEGIN
		SET @strSql=@strSql+ ' and textdetails like ''%'+@SeachKeyword+'%'''
	END
	ELSE
	BEGIN
		SET @strSql=@strSql+ ' and c.bitTask=' + CONVERT(varchar(12),@filter) + ' and textdetails like ''%'+@SeachKeyword+'%'''                     
	END
  IF @bitOpenCommu=1  SET @strSql=@strSql+ ' and isnull(C.bitClosedflag,0)=0'
 end                
 set @strSql='With tblCorr as (SELECT ROW_NUMBER() OVER (ORDER BY '+@columnName+' '+ @columnSortOrder+') AS RowNumber,X.* from('+@strSql+')X)'                

 set @strSql=@strSql +'                       
  select  *  from tblCorr
  Where RowNumber > '+convert(varchar(10),@firstRec)+' and RowNumber < '+convert(varchar(10),@lastRec)+'                     
  union select 0,count(*),null,null,null,'','',null,null,null,null,null,null,null,null,null,null,0,0,null from tblCorr order by RowNumber'                 
                
-- set @strSql=@strSql +'                       
--  select  *,CASE WHEN [Type]=''Email'' THEN dbo.GetEmaillAdd(numEmailHstrID,4)+'', '' +dbo.GetEmaillAdd(numEmailHstrID,1) ELSE '''' END as [From] from tblCorr
--  Where RowNumber > '+convert(varchar(10),@firstRec)+' and RowNumber < '+convert(varchar(10),@lastRec)+'                     
--  union select 0,count(*),null,null,null,'','',null,null,null,null,null,null,null,null,null,null from tblCorr order by RowNumber'                 
                
 print @strSql                
 exec (@strSql)                
end                
else                
select 0,0
--created by anoop jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdomaindetails')
DROP PROCEDURE usp_getdomaindetails
GO
CREATE PROCEDURE [dbo].[USP_GetDomainDetails]                                          
@numDomainID as numeric(9)=0                                          
as           

DECLARE @listIds VARCHAR(MAX)
SELECT @listIds = COALESCE(@listIds+',' ,'') + CAST(numUserID AS VARCHAR(100)) FROM UnitPriceApprover WHERE numDomainID = @numDomainID
                               
select                                           
vcDomainName,                                           
isnull(vcDomainDesc,'') vcDomainDesc,                                           
isnull(bitExchangeIntegration,0) bitExchangeIntegration,                                           
isnull(bitAccessExchange,0) bitAccessExchange,                                           
isnull(vcExchUserName,'') vcExchUserName,                                           
isnull(vcExchPassword,'') vcExchPassword,                                           
isnull(vcExchPath,'') vcExchPath,                                           
isnull(vcExchDomain,'') vcExchDomain,                                         
tintCustomPagingRows,                                         
vcDateFormat,                                         
numDefCountry,                                         
tintComposeWindow,                                         
sintStartDate,                                         
sintNoofDaysInterval,                                         
tintAssignToCriteria,                                         
bitIntmedPage,                                         
tintFiscalStartMonth,                                    
--isnull(bitDeferredIncome,0) as bitDeferredIncome,                                  
isnull(tintPayPeriod,0) as tintPayPeriod,                                
isnull(numCurrencyID,0) as numCurrencyID,                            
isnull(vcCurrency,'') as vcCurrency,                      
isnull(charUnitSystem,'') as charUnitSystem ,                    
isnull(vcPortalLogo,'') as vcPortalLogo,                  
isnull(tintChrForComSearch,2) as tintChrForComSearch,                
isnull(tintChrForItemSearch,'') as tintChrForItemSearch,            
isnull(intPaymentGateWay,0) as intPaymentGateWay  ,        
 case when bitPSMTPServer= 1 then vcPSMTPServer else '' end as vcPSMTPServer  ,              
 case when bitPSMTPServer= 1 then  isnull(numPSMTPPort,0)  else 0 end as numPSMTPPort          
,isnull(bitPSMTPAuth,0) bitPSMTPAuth        
,isnull([vcPSmtpPassword],'')vcPSmtpPassword        
,isnull(bitPSMTPSSL,0) bitPSMTPSSL     
,isnull(bitPSMTPServer,0) bitPSMTPServer  ,isnull(vcPSMTPUserName,'') vcPSMTPUserName,      
--isnull(numDefaultCategory,0) numDefaultCategory,
isnull(numShipCompany,0)   numShipCompany,
isnull(bitAutoPopulateAddress,0) bitAutoPopulateAddress,
isnull(tintPoulateAddressTo,0) tintPoulateAddressTo,
isnull(bitMultiCompany,0) bitMultiCompany,
isnull(bitMultiCurrency,0) bitMultiCurrency,
ISNULL(bitCreateInvoice,0) bitCreateInvoice,
ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
ISNULL([bitSaveCreditCardInfo],0) bitSaveCreditCardInfo,
ISNULL([intLastViewedRecord],10) intLastViewedRecord,
ISNULL([tintCommissionType],1) tintCommissionType,
ISNULL([tintBaseTax],2) tintBaseTax,
ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
ISNULL(bitEmbeddedCost,0) bitEmbeddedCost,
ISNULL(numDefaultSalesOppBizDocId,0) numDefaultSalesOppBizDocId,
ISNULL(tintSessionTimeOut,1) tintSessionTimeOut,
ISNULL(tintDecimalPoints,2) tintDecimalPoints,
ISNULL(bitCustomizePortal,0) bitCustomizePortal,
ISNULL(tintBillToForPO,0) tintBillToForPO,
ISNULL(tintShipToForPO,0) tintShipToForPO,
ISNULL(tintOrganizationSearchCriteria,1) tintOrganizationSearchCriteria,
ISNULL(tintLogin,0) tintLogin,
lower(ISNULL(vcPortalName,'')) vcPortalName,
ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
D.numDomainID,
ISNULL(vcPSMTPDisplayName,'') vcPSMTPDisplayName,
ISNULL(bitGtoBContact,0) bitGtoBContact,
ISNULL(bitBtoGContact,0) bitBtoGContact,
ISNULL(bitGtoBCalendar,0) bitGtoBCalendar,
ISNULL(bitBtoGCalendar,0) bitBtoGCalendar,
ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
ISNULL(bitInlineEdit,0) bitInlineEdit,
ISNULL(bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
ISNULL(bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
isnull(D.bitRentalItem,0) as bitRentalItem,
isnull(D.numRentalItemClass,0) as numRentalItemClass,
isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
ISNULL(D.bitAutoSerialNoAssign,0) as bitAutoSerialNoAssign,
ISNULL(D.bitIsShowBalance,0) AS [bitIsShowBalance],
ISNULL(numIncomeAccID,0) AS [numIncomeAccID],
ISNULL(numCOGSAccID,0) AS [numCOGSAccID],
ISNULL(numAssetAccID,0) AS [numAssetAccID],
ISNULL(IsEnableClassTracking,0) AS [IsEnableClassTracking],
ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
ISNULL(D.vcShipToPhoneNo,0) AS vcShipToPhoneNo,
ISNULL(D.vcGAUserEMail,'') AS vcGAUserEMail,
ISNULL(D.vcGAUserPassword,'') AS vcGAUserPassword,
ISNULL(D.vcGAUserProfileId,0) AS vcGAUserProfileId,
ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
ISNULL(D.vcHideTabs,'') AS vcHideTabs,
ISNULL(D.bitUseBizdocAmount,0) AS bitUseBizdocAmount,
ISNULL(D.IsEnableUserLevelClassTracking,0) AS IsEnableUserLevelClassTracking,
ISNULL(D.bitDefaultRateType,0) AS bitDefaultRateType,
ISNULL(D.bitIncludeTaxAndShippingInCommission,0) AS bitIncludeTaxAndShippingInCommission,
/*,ISNULL(D.bitAllowPPVariance,0) AS bitAllowPPVariance*/
ISNULL(D.bitPurchaseTaxCredit,0) AS bitPurchaseTaxCredit,
ISNULL(D.bitLandedCost,0) AS bitLandedCost,
ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
ISNULL(D.numAbovePercent,0) AS numAbovePercent,
ISNULL(D.numAbovePriceField,0) AS numAbovePriceField,
ISNULL(D.numBelowPercent,0) AS numBelowPercent,
ISNULL(D.numBelowPriceField,0) AS numBelowPriceField,
ISNULL(D.numOrderStatusBeforeApproval,0) AS numOrderStatusBeforeApproval,
ISNULL(D.numOrderStatusAfterApproval,0) AS numOrderStatusAfterApproval,
@listIds AS vcUnitPriceApprover,
ISNULL(D.numDefaultSalesPricing,2) AS numDefaultSalesPricing
from Domain D  
left join eCommerceDTL eComm  
on eComm.numDomainID=D.numDomainID  
where (D.numDomainID=@numDomainID OR @numDomainID=-1)


/****** Object:  StoredProcedure [dbo].[usp_GetInitialPageDetails]    Script Date: 07/26/2008 16:17:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getinitialpagedetails')
DROP PROCEDURE usp_getinitialpagedetails
GO
CREATE PROCEDURE [dbo].[usp_GetInitialPageDetails]  
    @numGroupType NUMERIC,
    @numDomainId NUMERIC,
    @numContactId NUMERIC,
	@numTabId NUMERIC
as  

if (select count(*) from ShortCutUsrCnf where numGroupId=@numGroupType and numDomainId=@numDomainId and numContactID=@numContactID and isnull(numTabId,0)=@numTabId and isnull(bitInitialPage,0)=1)>0    
begin    
   Select top 1 Hdr.id,Hdr.vclinkname as [Name],Hdr.link,Hdr.numTabId
   from ShortCutUsrcnf Sconf join  ShortCutBar Hdr on  Hdr.id =Sconf.numLinkId  and Sconf.numTabId=Hdr.numTabId         
   where sconf.numDomainId = @numDomainId and sconf.numGroupID =@numGroupType  and sconf.numContactId =@numContactId and isnull(Hdr.numTabId,0)=@numTabId and isnull(Sconf.bitInitialPage,0)=1 and isnull(Sconf.tintLinkType,0)=0        
	UNION
  Select top 1 LD.numListItemID,LD.vcData  as [Name],'../prospects/frmCompanyList.aspx?RelId='+convert(varchar(10),LD.numListItemID) as link,Sconf.numTabId
   from ShortCutUsrcnf Sconf join  ListDetails LD on  LD.numListItemID =Sconf.numLinkId           
   where LD.numListId = 5 and LD.numListItemID<>46 and(LD.numDomainID=@numDomainID or constflag=1)  and 
		sconf.numDomainId = @numDomainId and sconf.numGroupID =@numGroupType  and sconf.numContactId =@numContactId and isnull(Sconf.numTabId,0)=@numTabId and isnull(Sconf.bitInitialPage,0)=1 and isnull(Sconf.tintLinkType,0)=5        

end    
else IF (select count(*) from ShortCutGrpConf where numDomainId = @numDomainId and numGroupID =@numGroupType  and isnull(numTabId,0)=@numTabId and isnull(bitInitialPage,0)=1) > 0              
 begin    
  select top 1 Hdr.id,Hdr.vclinkname as [Name],Hdr.link,Hdr.numTabId
  from ShortCutGrpConf  Sconf  join  ShortCutBar Hdr  on  Hdr.id=Sconf.numLinkId and Sconf.numTabId=Hdr.numTabId 
  where Sconf.numGroupID =@numGroupType and Sconf.numDomainId = @numDomainId   and Hdr.numContactId =0 and isnull(Hdr.numTabId,0)=@numTabId and isnull(Sconf.bitInitialPage,0)=1 and isnull(Sconf.tintLinkType,0)=0  
		UNION
  Select top 1 LD.numListItemID,LD.vcData as [Name],'../prospects/frmCompanyList.aspx?RelId='+convert(varchar(10),LD.numListItemID) as link,Sconf.numTabId
   from ShortCutGrpConf Sconf join  ListDetails LD on  LD.numListItemID =Sconf.numLinkId           
   where LD.numListId = 5 and LD.numListItemID<>46 and(LD.numDomainID=@numDomainID or constflag=1)  and 
		sconf.numDomainId = @numDomainId and sconf.numGroupID =@numGroupType and isnull(Sconf.numTabId,0)=@numTabId and isnull(Sconf.bitInitialPage,0)=1 and isnull(Sconf.tintLinkType,0)=5        
 end  
ELSE IF (SELECT COUNT(*) FROM ShortCutBar WHERE isnull(numTabId,0)=@numTabId and isnull(bitInitialPage,0)=1) > 0
	select top 1 Hdr.id,Hdr.vclinkname as [Name],Hdr.link,Hdr.numTabId
	from ShortCutBar Hdr  
	where isnull(numTabId,0)=@numTabId and isnull(bitInitialPage,0)=1 
ELSE
BEGIN
	--NO INITIAL LINK AVAILABLE
	IF @numTabId = 80
	BEGIN
		SELECT 
			0 AS id,'All Items' as [Name],'../Items/frmItemList.aspx?Page=All Items&ItemGroup=0' AS Link,80 AS numTabId
	END
END  
-- else    
-- begin    
--  select id,vclinkname  as [Name],link,numTabId,vcImage,isnull(bitNew,0) as bitNew,isnull(bitNewPopup,0) as bitNewPopup,NewLink from ShortCutBar where bitdefault = 1 and numContactId =0 and isnull(numTabId,0)=@numTabId
-- end


--declare @strSql as varchar(500)
--set @strSql= '
--select * from RelationsDefaultFilter   
-- WHERE numUserCntId='+ convert(varchar(10),@numUserCntId)+ 'and numDomainId= '+ convert(varchar(10),@numDomainId)+'
--	 and bitInitialPage=1 '
--
--if @FormId = 14 
-- set @strSql=@strSql+' and numformId in(38,39,40,41) '
--if @FormId = 0 
--set @strSql=@strSql+' and numformId in (34,35,36,10) '
--PRINT @strSql
--exec(@strSql)
GO
GO
/****** Object:  StoredProcedure [dbo].[USP_GetOpportuntityCommission]    Script Date: 02/28/2009 13:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva
-- [dbo].[USP_GetOpportuntityCommission] 1,1,1,-333,'1/1/2009','1/15/2009'
-- exec USP_GetOpportuntityCommission @numUserId=372,@numUserCntID=82979,@numDomainId=1,@ClientTimeZoneOffset=-330,@dtStartDate='2010-03-01 00:00:00:000',@dtEndDate='2010-04-01 00:00:00:000'
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getopportuntitycommission')
DROP PROCEDURE usp_getopportuntitycommission
GO
CREATE PROCEDURE [dbo].[USP_GetOpportuntityCommission]
(
               @numUserId            AS NUMERIC(9)  = 0,
               @numUserCntID         AS NUMERIC(9)  = 0,
               @numDomainId          AS NUMERIC(9)  = 0,
               @ClientTimeZoneOffset AS INT,
               @dtStartDate          AS DATETIME,
               @dtEndDate            AS DATETIME,
               @numOppBizDocsId AS NUMERIC(9) =0,
               @tintMode AS tinyint,
			   @numComRuleID AS NUMERIC(18,0) = NULL
)
AS
  BEGIN
  
  DECLARE @bitIncludeShippingItem AS BIT
  DECLARE @numShippingItemID AS NUMERIC(18,0)
  SELECT @bitIncludeShippingItem=bitIncludeTaxAndShippingInCommission,@numShippingItemID=numShippingServiceItemID FROM Domain WHERE numDomainID = @numDomainId


  IF @tintMode=0  -- Paid Invoice
  BEGIN
		SELECT 
			LD.vcData as BizDoc,
			OBD.numBizDocID,
			OM.numOppId,
			OBD.numOppBizDocsId,
			isnull(OBD.monAmountPaid,0) monAmountPaid,
			OM.bintCreatedDate,
			OM.vcPOppName AS Name,
			Case when OM.tintOppStatus=0 then 'Open' Else 'Close' End as OppStatus, 
			isnull(OBD.monDealAmount,0) as DealAmount,
			Case When (OM.numRecOwner=@numUserCntID and OM.numAssignedTo=@numUserCntID) then  'Deal Owner/Assigned'
				 When OM.numRecOwner=@numUserCntID then 'Deal Owner' 
				 When OM.numAssignedTo=@numUserCntID then 'Deal Assigned' 
			end as EmpRole,
			ISNULL(TempBizDocComission.numComissionAmount,0) as decTotalCommission,
			OBD.vcBizDocID,
			(SELECT SUM(ISNULL(monTotAmount,0)) FROM dbo.OpportunityBizDocItems WHERE numOppBizDocID=OBD.numOppBizDocsId AND 1 = (CASE @bitIncludeShippingItem WHEN 1 THEN 1 ELSE CASE WHEN OpportunityBizDocItems.numItemCode = @numShippingItemID THEN 0 ELSE 1 END END)) AS monSubTotAMount
		FROM
		OpportunityBizDocs OBD 
		INNER JOIN OpportunityMaster OM ON OBD.numOppId =OM.numOppId
		LEFT JOIN ListDetails LD on LD.numListItemID = OBD.numBizDocId 
		CROSS APPLY
		(
			SELECT 
				MAX(DM.dtDepositDate) dtDepositDate
			FROM 
				DepositMaster DM 
			JOIN dbo.DepositeDetails DD	ON DM.numDepositId=DD.numDepositID 
			WHERE 
				DM.tintDepositePage=2 
				AND DM.numDomainId=@numDomainId
				AND DD.numOppID=OBD.numOppID 
				AND DD.numOppBizDocsID =OBD.numOppBizDocsID
		) TEMPDEPOSIT
		CROSS APPLY
		(
			SELECT
				SUM(ISNULL(numComissionAmount,0)) numComissionAmount,
				COUNT(*) AS intCount
			FROM
				BizDocComission BDC
			WHERE
				BDC.numDomainId = @numDomainID
				AND BDC.numUserCntID = @numUserCntID
				AND BDC.numOppBizDocId = OBD.numOppBizDocsId
				AND bitCommisionPaid=0
				AND (ISNULL(@numComRuleID,0)=0 OR BDC.numComRuleID = @numComRuleID)
		) TempBizDocComission
		WHERE 
			OM.numDomainId=@numDomainId 
			AND OM.tintOppStatus=1 
			AND OM.tintOppType=1 
			AND (OM.numRecOwner=@numUserCntID OR OM.numAssignedTo=@numUserCntID)
			AND OBD.bitAuthoritativeBizDocs = 1 
			AND isnull(OBD.monAmountPaid,0) >= OBD.monDealAmount 
			AND OBD.numOppBizDocsId IN (SELECT numOppBizDocId FROM BizDocComission WHERE numUserCntId=@numUserCntID AND numDomainId=@numDomainId AND bitCommisionPaid=0)
			AND CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,TEMPDeposit.dtDepositDate) AS DATE) BETWEEN CAST(@dtStartDate AS DATE) AND CAST(@dtEndDate AS DATE)
			AND TempBizDocComission.intCount > 0
  
	END
  ELSE IF @tintMode=1 -- Non Paid Invoice
  BEGIN
		Select 
			vcData as BizDoc,
			oppBiz.numBizDocID,
			Opp.numOppId,
			oppBiz.numOppBizDocsId,
			isnull(oppBiz.monAmountPaid,0) monAmountPaid,
			Opp.bintCreatedDate,
			Opp.vcPOppName AS Name,
			Case when Opp.tintOppStatus=0 then 'Open' Else 'Close' End as OppStatus, 
			isnull(oppBiz.monDealAmount,0) as DealAmount,
            Case 
				When (Opp.numRecOwner=@numUserCntID and Opp.numAssignedTo=@numUserCntID) then  'Deal Owner/Assigned'
				When Opp.numRecOwner=@numUserCntID then 'Deal Owner' 
				When Opp.numAssignedTo=@numUserCntID then 'Deal Assigned' 
			end as EmpRole,
			ISNULL(TempBizDocComission.numComissionAmount,0) as decTotalCommission,oppBiz.vcBizDocID,
			(SELECT SUM(ISNULL(monTotAmount,0)) FROM dbo.OpportunityBizDocItems WHERE numOppBizDocID=oppBiz.numOppBizDocsId AND 1 = (CASE @bitIncludeShippingItem WHEN 1 THEN 1 ELSE CASE WHEN OpportunityBizDocItems.numItemCode = @numShippingItemID THEN 0 ELSE 1 END END)) AS monSubTotAMount
        From OpportunityMaster Opp 
		join OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
        left join ListDetails LD on LD.numListItemID=OPPBIZ.numBizDocId 
		CROSS APPLY
		(
			select 
				SUM(ISNULL(numComissionAmount,0)) numComissionAmount,
				COUNT(*) AS intCount
			from 
				BizDocComission 
			WHERE 
				numUserCntId=@numUserCntID 
				AND numDomainId=@numDomainId 
				AND bitCommisionPaid=0 
				and numOppBizDocId=oppBiz.numOppBizDocsId
				AND (ISNULL(@numComRuleID,0)=0 OR numComRuleID = @numComRuleID)
		) TempBizDocComission
        Where 
			oppBiz.bitAuthoritativeBizDocs = 1 AND Opp.tintOppStatus=1 And Opp.tintOppType=1 
			And (Opp.numRecOwner=@numUserCntID or Opp.numAssignedTo=@numUserCntID)
            And Opp.numDomainId=@numDomainId AND oppBiz.bitAuthoritativeBizDocs = 1 AND isnull(oppBiz.monAmountPaid,0) <oppBiz.monDealAmount
			AND ((dateadd(minute,-@ClientTimeZoneOffset,oppBiz.dtFromDate) between @dtStartDate And @dtEndDate)) 
			AND oppBiz.numOppBizDocsId IN (SELECT DISTINCT numOppBizDocId FROM BizDocComission WHERE numUserCntId=@numUserCntID AND numDomainId=@numDomainId AND bitCommisionPaid=0) --added by chintan BugID 982
			AND TempBizDocComission.intCount > 0
  END
   ELSE IF @tintMode=2  -- Paid Invoice Detail
   BEGIN
        SELECT * FROM (Select BC.numComissionID,oppBiz.numOppBizDocsId,BC.decCommission,Case when BC.tintComBasedOn = 1 then 'Amount' when BC.tintComBasedOn = 2 then 'Units' end AS BasedOn,
			Case when BC.tintComType = 1 then 'Percentage' when BC.tintComType = 2 then 'Flat' end as CommissionType, 
                        numComissionAmount  as CommissionAmt,I.vcItemName,BDI.numUnitHour,BDI.monPrice,BDI.monTotAmount,ISNULL(OT.monVendorCost, 0)* ISNULL(BDI.[numUnitHour],0) AS VendorCost,
						ISNULL(OT.monAvgCost, 0)* ISNULL(BDI.[numUnitHour],0) AS monAvgCost,
                        (SELECT MAX(DM.dtDepositDate) FROM DepositMaster DM JOIN dbo.DepositeDetails DD
									ON DM.numDepositId=DD.numDepositID WHERE DM.tintDepositePage=2 AND DM.numDomainId=@numDomainId
									AND DD.numOppID=oppBiz.numOppID AND DD.numOppBizDocsID =oppBiz.numOppBizDocsID) AS dtDepositDate          
                              From OpportunityMaster Opp 
							  INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
                              INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId and BC.numUserCntId=@numUserCntID
                              INNER JOIN OpportunityBizDocItems BDI ON oppBiz.numOppBizDocsId=BDI.numOppBizDocID AND BC.numOppBizDocItemID=BDI.numOppBizDocItemID
							  INNER JOIN OpportunityItems OT ON OT.numOppItemtCode=BDI.numOppItemID
							  INNER JOIN Item I ON BDI.numItemCode=I.numItemCode
                              Where oppBiz.bitAuthoritativeBizDocs = 1 and Opp.tintOppStatus=1 And Opp.tintOppType=1 And (Opp.numRecOwner=@numUserCntID OR Opp.numAssignedTo=@numUserCntID)
                              And Opp.numDomainId=@numDomainId 
							  AND oppBiz.numOppBizDocsId=@numOppBizDocsId AND BC.bitCommisionPaid=0 --added by chintan BugID 982
							  AND (ISNULL(@numComRuleID,0)=0 OR BC.numComRuleID=@numComRuleID)
							  AND oppBiz.bitAuthoritativeBizDocs = 1 AND 
							  1=(CASE WHEN @tintMode=2 THEN CASE WHEN isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount THEN 1 ELSE 0 END
							       WHEN @tintMode=3 THEN CASE WHEN isnull(oppBiz.monAmountPaid,0) <oppBiz.monDealAmount THEN 1 ELSE 0 END END)) A
						WHERE ((dateadd(minute,-@ClientTimeZoneOffset,dtDepositDate) between @dtStartDate And @dtEndDate))  
							     
    END
    ELSE IF @tintMode=3 -- Non Paid Invoice Detail
   BEGIN
        Select BC.numComissionID,oppBiz.numOppBizDocsId,BC.decCommission,Case when BC.tintComBasedOn = 1 then 'Amount' when BC.tintComBasedOn = 2 then 'Units' end AS BasedOn,
			Case when BC.tintComType = 1 then 'Percentage' when BC.tintComType = 2 then 'Flat' end as CommissionType, 
                        numComissionAmount  as CommissionAmt,I.vcItemName,BDI.numUnitHour,BDI.monPrice,BDI.monTotAmount,ISNULL(OT.monVendorCost, 0)* ISNULL(BDI.[numUnitHour],0) AS VendorCost,
						ISNULL(OT.monAvgCost, 0)* ISNULL(BDI.[numUnitHour],0) AS monAvgCost       
                              From OpportunityMaster Opp 
							  INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
                              INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId and BC.numUserCntId=@numUserCntID
                              INNER JOIN OpportunityBizDocItems BDI ON oppBiz.numOppBizDocsId=BDI.numOppBizDocID AND BC.numOppBizDocItemID=BDI.numOppBizDocItemID
							  INNER JOIN OpportunityItems OT ON OT.numOppItemtCode=BDI.numOppItemID
							  INNER JOIN Item I ON BDI.numItemCode=I.numItemCode
                              Where oppBiz.bitAuthoritativeBizDocs = 1 and Opp.tintOppStatus=1 And Opp.tintOppType=1 And (Opp.numRecOwner=@numUserCntID OR Opp.numAssignedTo=@numUserCntID)
                              And Opp.numDomainId=@numDomainId 
							  AND ((dateadd(minute,-@ClientTimeZoneOffset,oppBiz.dtFromDate) between @dtStartDate And @dtEndDate)) 
							  AND oppBiz.numOppBizDocsId=@numOppBizDocsId AND BC.bitCommisionPaid=0 --added by chintan BugID 982
							  AND (ISNULL(@numComRuleID,0)=0 OR BC.numComRuleID=@numComRuleID)
							  AND oppBiz.bitAuthoritativeBizDocs = 1 AND 
							  1=(CASE WHEN @tintMode=2 THEN CASE WHEN isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount THEN 1 ELSE 0 END
							       WHEN @tintMode=3 THEN CASE WHEN isnull(oppBiz.monAmountPaid,0) <oppBiz.monDealAmount THEN 1 ELSE 0 END END)
    END
  END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOppSerialLotNumber')
DROP PROCEDURE USP_GetOppSerialLotNumber
GO
CREATE PROCEDURE [dbo].[USP_GetOppSerialLotNumber]
	@numOppID AS NUMERIC(18,0),
	@numOppItemID AS NUMERIC(18,0),
	@numWarehouseItemID AS NUMERIC(18,0)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @numDomainID AS NUMERIC(18,0)
	SELECT @numDomainID= numDomainId FROM OpportunityMaster WHERE numOppId = @numOppID


	-- Available Serail Lot Numbers
	SELECT
		ROW_NUMBER() OVER(ORDER BY dExpirationDate DESC) AS OrderNo,
		numWareHouseItmsDTLID,
		vcSerialNo,
		numQty,
		dbo.FormatedDateFromDate(dExpirationDate,@numDomainID) AS dExpirationDate
	FROM
	(
		SELECT
			
			numWareHouseItmsDTLID,
			vcSerialNo,
			numQty - isnull((select sum(w.numQty) from OppWarehouseSerializedItem w INNER JOIN OpportunityMaster opp ON w.numOppId=opp.numOppId where ISNULL(opp.tintOppType,0) <> 2 AND 1 = (CASE WHEN ISNULL(opp.bitStockTransfer,0) = 1 THEN CASE WHEN ISNULL(w.bitTransferComplete,0) = 0 THEN 1 ELSE 0 END ELSE 1 END) AND w.numWarehouseItmsDTLID=WareHouseItmsDTL.numWarehouseItmsDTLID),0) AS numQty,
			dExpirationDate
		FROM
			WareHouseItmsDTL
		WHERE
			numWareHouseItemID = @numWarehouseItemID AND
			ISNULL(numQty,0) > 0 
		
	) Temp
	WHERE
		Temp.numQty > 0
	ORDER BY
		dExpirationDate DESC

    -- Selected Serail Lot Numbers
	SELECT
		ROW_NUMBER() OVER(ORDER BY WareHouseItmsDTL.dExpirationDate DESC) AS OrderNo,
		WareHouseItmsDTL.numWareHouseItmsDTLID,
		WareHouseItmsDTL.vcSerialNo,
		dbo.FormatedDateFromDate(WareHouseItmsDTL.dExpirationDate,@numDomainID) AS dExpirationDate,
		OppWarehouseSerializedItem.numQty
	FROM
		OppWarehouseSerializedItem
	JOIN
		WareHouseItmsDTL
	ON
		OppWarehouseSerializedItem.numWarehouseItmsDTLID = WareHouseItmsDTL.numWareHouseItmsDTLID
	WHERE
		numOppID = @numOppID AND
		numOppItemID = @numOppItemID AND
		numWareHouseItemID = @numWarehouseItemID
	ORDER BY
		WareHouseItmsDTL.dExpirationDate DESC

END



GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetOrderItems' ) 
    DROP PROCEDURE USP_GetOrderItems
GO
CREATE PROCEDURE USP_GetOrderItems ( @tintMode TINYINT = 0,
                                     @numOppItemID NUMERIC(9) = 0,
                                     @numOppID NUMERIC(9) = 0,
                                     @numDomainId NUMERIC(9),
                                     @ClientTimeZoneOffset INT,
									 @vcOppItemIDs VARCHAR(2000)='' )
AS 
BEGIN
	
    IF @tintMode = 1 --Add Return grid data & For Recurring Get Original Units to split
        BEGIN
            SELECT  OI.numoppitemtCode,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcItemDesc,
                    OI.numUnitHour numOriginalUnitHour,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					OI.monPrice
            FROM    dbo.OpportunityMaster OM
                    INNER JOIN dbo.OpportunityItems OI ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
            WHERE   OM.numDomainId = @numDomainId
                    AND OM.numOppId = @numOppID
                    
        END
 
    IF @tintMode = 2 -- Get single item based on PK, or all items
		-- Used by Sales/Purchase Template Load items in New Sales Order
        BEGIN 
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
                    OI.numItemCode,
					OI.bitItemPriceApprovalRequired,
                    OI.numUnitHour numOriginalUnitHour,
                    OI.monPrice,
                    OI.monTotAmount,
                    OI.numSourceID,
                    OI.vcItemDesc,
                    OI.numWarehouseItmsID,
                    OI.vcType,
                    OI.vcAttributes,
                    OI.bitDropShip,
                    ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,
                    OI.bitDiscountType,
                    OI.fltDiscount,
                    OI.monTotAmtBefDiscount,
                    ISNULL(OI.numQtyShipped,0) AS numQtyShipped,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcPathForTImage,
                    OI.vcManufacturer,
                    OI.monVendorCost,
                    ISNULL(OI.numUOMId, 0) AS numUOMId,
                    ISNULL(OI.numUOMId, 0) numUOM, -- used from add edit order
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL) AS UOMConversionFactor,
                    OI.bitWorkOrder,
                    isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
                    OI.numShipmentMethod,
                    OI.numSOVendorId,
                    OI.numProjectID,
                    OI.numClassID,
                    OI.numProjectStageID,
                    OI.numToWarehouseItemID,
                    WItems.numWarehouseID,
                    W.vcWareHouse AS Warehouse,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,
					isnull(OI.numProjectID,0) as numProjectID,
					dbo.fn_GetUOMName(I.numBaseUnit) AS vcBaseUOMName,
					OI.bitDropShip AS bitAllowDropShip,
					I.numVendorID,
					ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.txtItemDesc,'') txtItemDesc,
					ISNULL(I.vcModelID,'') vcModelID,
					ISNULL(I.vcManufacturer,'') vcManufacturer,
					ISNULL(I.numBarCodeId,'') numBarCodeId,
					ISNULL(I.bitSerialized,0) bitSerialized,
					ISNULL(I.bitKitParent,0) bitKitParent,
					ISNULL(I.bitAssembly,0) bitAssembly,
					ISNULL(I.IsArchieve,0) IsArchieve,
					ISNULL(I.bitLotNo,0) bitLotNo,
					ISNULL(WItems.monWListPrice,0) monWListPrice,
					ISNULL(I.bitTaxable, 0) bitTaxable,
                    I.charItemType,
                    I.bitKitParent,
					I.bitAssembly,
					I.bitSerialized,
					ISNULL(I.fltWeight,0) AS [fltWeight],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) AS [vcSKU],
					ISNULL(I.monAverageCost,0) AS monAverageCost,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
                    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
            WHERE   OI.numOppId = @numOppID
                    AND ( OI.numoppitemtCode = @numOppItemID
                          OR @numOppItemID = 0
                        )
                    AND OM.numDomainId = @numDomainId
                    
        END
 
    IF @tintMode = 3 -- Get item for SO to PO and vice versa creation
        BEGIN
            
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
					OI.bitItemPriceApprovalRequired,
                    OI.vcItemDesc,
                    ISNULL(OI.vcPathForTImage, '') vcPathForTImage,
                    ISNULL(OI.numWarehouseItmsID, 0) numWarehouseItmsID,
                    OI.vcItemName,
                    OI.vcModelID,
                    ISNULL(OI.vcAttributes, '') vcAttributes,
                    OI.vcType,
                    ISNULL(OI.bitDropShip, 0) bitDropShip,
                    OI.numItemCode,
                    OI.numUnitHour numOriginalUnitHour,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL) AS UOMConversionFactor,
                    ISNULL(I.numVendorID, 0) numVendorID,
                    ISNULL(V.monCost, 0) monVendorCost,
                    ISNULL(V.vcPartNo, '') vcPartNo,
                    OI.monPrice,
                    ISNULL(OI.numSOVendorId, 0) numSOVendorId,
                    ISNULL(I.charItemType, 'N') charItemType,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL((SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numWarehouseItemID = OI.numWarehouseItmsID ),ISNULL(I.vcSKU,'')) AS [vcSKU]
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID
                                          AND I.numItemCode = V.numItemCode
            WHERE   OI.numOppId = @numOppID
                    AND OM.numDomainId = @numDomainId
            
        END
        IF @tintMode = 4 -- Add Edit Order items
        BEGIN 
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
                    OI.numItemCode,
					OI.bitItemPriceApprovalRequired,
                    OI.numUnitHour,
                    OI.monPrice,
                    OI.monTotAmount,
                    OI.numSourceID,
                    OI.vcItemDesc,
                    OI.numWarehouseItmsID,
                    OI.vcType ItemType,
                    OI.vcAttributes Attributes,
                    OI.bitDropShip DropShip,
                    OI.numUnitHourReceived,
                    OI.bitDiscountType,
                    OI.fltDiscount,
                    OI.monTotAmtBefDiscount,
                    OI.numQtyShipped,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcPathForTImage,
                    OI.vcManufacturer,
                    OI.monVendorCost,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL) AS UOMConversionFactor,
                    OI.bitWorkOrder,
                    isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
                    OI.numShipmentMethod,
                    OI.numSOVendorId,
                    OI.numProjectID,
                    OI.numClassID,
                    OI.numProjectStageID,
                    OI.numToWarehouseItemID,
                    ISNULL(I.bitTaxable, 0) bitTaxable,
                   UPPER(I.charItemType) charItemType,
                    WItems.numWarehouseID,
                    W.vcWareHouse AS Warehouse,
                    0 as Op_Flag,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,
					CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON ob.numOppBizDocsId=obi.numOppBizDocID
					AND ob.bitAuthoritativeBizDocs=1 AND obi.numOppItemID=oi.numoppitemtCode AND ob.numOppId=OM.numOppId)>0 THEN 1 ELSE 0 END  AS bitIsAuthBizDoc,
					ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(numQtyShipped,0) AS numQtyShipped,dbo.fn_GetUOMName(I.numBaseUnit) AS vcBaseUOMName,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) AS [vcSKU],
					ISNULL(I.monAverageCost,0) AS monAverageCost,
					ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.txtItemDesc,'') txtItemDesc,
					ISNULL(I.vcModelID,'') vcModelID,
					ISNULL(I.vcManufacturer,'') vcManufacturer,
					ISNULL(I.numBarCodeId,'') numBarCodeId,
					ISNULL(I.bitSerialized,0) bitSerialized,
					ISNULL(I.bitKitParent,0) bitKitParent,
					ISNULL(I.bitAssembly,0) bitAssembly,
					ISNULL(I.IsArchieve,0) IsArchieve,
					ISNULL(I.bitLotNo,0) bitLotNo,
					ISNULL(WItems.monWListPrice,0) monWListPrice,
					ISNULL(I.bitTaxable, 0) bitTaxable,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder,
					case when ISNULL(I.bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode) else 0 end AS numMaxWorkOrderQty,
					ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) AS numSerialNoAssigned
--					0 AS [Tax0],0 AS bitTaxable0
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
                    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
            WHERE   OI.numOppId = @numOppID
                    AND OM.numDomainId = @numDomainId
                    
		SELECT  vcSerialNo,
				vcComments AS Comments,
				numOppItemID AS numoppitemtCode,
				W.numWarehouseItmsDTLID,
				numWarehouseItmsID AS numWItmsID,
				dbo.Fn_getattributes(W.numWarehouseItmsDTLID, 1) Attributes
		FROM    WareHouseItmsDTL W
				JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
		WHERE   numOppID = @numOppID                
                   
        END
        
        
		IF @tintMode = 5 -- Get WebApi Order Items to enter WebApi Order line Item Details 
		BEGIN 
			SELECT OI.numOppItemtCode,OI.numOppId,OI.numItemCode,
			ISNULL((SELECT vcWHSKU FROM dbo.WareHouseItems WHERE WareHouseItems.numWarehouseItemID = OI.numWarehouseItmsID),ISNULL(IT.vcSKU,'')) AS [vcSKU],
			OI.vcItemDesc,OMAPI.vcAPIOppId
			FROM OpportunityItems OI 
			INNER JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId 
			INNER JOIN dbo.OpportunityMasterAPI OMAPI ON OM.numOppId = OMAPI.numOppId
			INNER JOIN dbo.Item IT ON OI.numItemCode = IT.numItemCode
			WHERE OI.numOppId = @numOppID and OM.numDomainID = @numDomainId 
        END

END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOrganizationTransaction')
DROP PROCEDURE USP_GetOrganizationTransaction
GO
CREATE PROCEDURE [dbo].[USP_GetOrganizationTransaction]
    (
      @numDivisionID NUMERIC(9)=0,
      @numDomainID NUMERIC,
      @CurrentPage INT=0,
	  @PageSize INT=0,
      @TotRecs INT=0  OUTPUT,
      @vcTransactionType AS VARCHAR(500),
      @dtFromDate DATETIME,
      @dtToDate DATETIME,
      @ClientTimeZoneOffset INT,
	  @SortCreatedDate TINYINT
    )
AS 
    BEGIN
		
		SELECT Items INTO #temp FROM dbo.Split(@vcTransactionType,',')

		CREATE TABLE #tempTransaction(intTransactionType INT,vcTransactionType VARCHAR(30),dtCreatedDate DATETIME,numRecordID NUMERIC,
		vcRecordName VARCHAR(100),monTotalAmount MONEY,monPaidAmount MONEY,dtDueDate DATETIME,vcMemo VARCHAR(1000),vcDescription VARCHAR(500),
		vcStatus VARCHAR(100),numRecordID2 NUMERIC,numStatus NUMERIC, vcBizDocId VARCHAR(1000))
		 
		 --Sales Order
		 IF EXISTS (SELECT 1 FROM #temp WHERE Items=1)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 1,'Sales Order',OM.bintCreatedDate,OM.numOppId,
		 	OM.vcPOppName,
		 	[dbo].[getdealamount](OM.numOppId,Getutcdate(),0),
		 	ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OM.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0),
		 	NULL,'','',dbo.GetListIemName(numStatus),0,numStatus,
		 	ISNULL(OM.vcOppRefOrderNo,'')
		 	 FROM OpportunityMaster OM where OM.numDomainID = @numDomainID AND OM.numDivisionId=@numDivisionId
                        AND OM.tintOppType = 1  AND OM.tintOppStatus =  1
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,OM.bintCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Sales Order Invoices(BizDocs)
		 IF EXISTS (SELECT 1 FROM #temp WHERE Items=2)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 2,'SO Invoice',OBD.dtCreatedDate,OBD.numOppBizDocsId,
		 	OBD.vcBizDocID,[dbo].[getdealamount](OM.numOppId,Getutcdate(),OBD.numOppBizDocsId),
		 	ISNULL(OBD.monAmountPaid,0),
		 	NULL,'','',dbo.GetListIemName(OBD.numBizDocStatus),OM.numOppId,numBizDocStatus,
		 	ISNULL(vcRefOrderNo,'')
		 	 FROM OpportunityMaster OM INNER JOIN OpportunityBizDocs OBD ON OBD.numOppId = OM.numOppId 
		 	 where OM.numDomainID = @numDomainID AND OM.numDivisionId=@numDivisionId
                        AND OM.tintOppType = 1  AND OM.tintOppStatus =  1 AND OBD.[bitAuthoritativeBizDocs] =1
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,OBD.dtCreatedDate) between @dtFromDate And @dtToDate))
        END
		 
		 --Sales Opportunity
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=3)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 3,'Sales Opportunity',OM.bintCreatedDate,OM.numOppId,
		 	OM.vcPOppName,[dbo].[getdealamount](OM.numOppId,Getutcdate(),0),
		 	ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OM.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0),
		 	NULL,'','',dbo.GetListIemName(numStatus),0,numStatus,''
		 	 FROM OpportunityMaster OM where OM.numDomainID = @numDomainID AND OM.numDivisionId=@numDivisionId
                        AND OM.tintOppType = 1  AND OM.tintOppStatus =  0
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,OM.bintCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Purchase Order
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=4)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 4,'Purchase Order',OM.bintCreatedDate,OM.numOppId,
		 	OM.vcPOppName,
		 	[dbo].[getdealamount](OM.numOppId,Getutcdate(),0),
		 	ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OM.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0),
		 	NULL,'','',dbo.GetListIemName(numStatus),0,numStatus,
		 	ISNULL(OM.vcOppRefOrderNo,'')
		 	 FROM OpportunityMaster OM where OM.numDomainID = @numDomainID AND OM.numDivisionId=@numDivisionId
                        AND OM.tintOppType = 2  AND OM.tintOppStatus =  1
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,OM.bintCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Purchase Order Bill(BizDocs)
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=5)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 5,'PO Bill',OBD.dtCreatedDate,OBD.numOppBizDocsId,
		 	OBD.vcBizDocID,[dbo].[getdealamount](OM.numOppId,Getutcdate(),OBD.numOppBizDocsId),
		 	ISNULL(OBD.monAmountPaid,0),
		 	NULL,'','',dbo.GetListIemName(OBD.numBizDocStatus),OM.numOppId,numBizDocStatus,
		 	ISNULL(vcRefOrderNo,'')
--		 	(SELECT SUBSTRING((SELECT vcBizDocID + ',' FROM OpportunityBizDocs 
--														   WHERE numOppId = OM.numOppId
--														   AND ISNULL(bitAuthoritativeBizDocs,0) = 1 
--										 FOR XML PATH('')), 0, 200000))
		 	 FROM OpportunityMaster OM INNER JOIN OpportunityBizDocs OBD ON OBD.numOppId = OM.numOppId 
		 	 where OM.numDomainID = @numDomainID AND OM.numDivisionId=@numDivisionId
                        AND OM.tintOppType = 2  AND OM.tintOppStatus =  1 AND OBD.[bitAuthoritativeBizDocs] =1
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,OBD.dtCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Purchase Opportunity
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=6)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 6,'Purchase Opportunity',OM.bintCreatedDate,OM.numOppId,
		 	OM.vcPOppName,[dbo].[getdealamount](OM.numOppId,Getutcdate(),0),
		 	ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OM.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0),
		 	NULL,'','',dbo.GetListIemName(numStatus),0,numStatus,''
		 	 FROM OpportunityMaster OM where OM.numDomainID = @numDomainID AND OM.numDivisionId=@numDivisionId
                        AND OM.tintOppType = 2  AND OM.tintOppStatus =  0
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,OM.bintCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Sales Return
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=7)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 7,'Sales Return',RH.dtCreatedDate,RH.numReturnHeaderID,
		 	RH.vcRMA,ISNULL(RH.monAmount,0),
		 	CASE ISNULL(RH.tintReceiveType,0) WHEN 1 THEN ISNULL((SELECT ISNULL(SUM(CH.monAmount),0) FROM dbo.CheckHeader CH WHERE CH.numDomainID=1 AND CH.tintReferenceType=10 AND CH.numReferenceID=RH.numReturnHeaderID AND ISNULL(CH.bitIsPrint,0)=1),0)
		 		 	WHEN 2 THEN ISNULL((SELECT SUM(ISNULL(monAppliedAmount,0)) AS monAmtPaid FROM DepositMaster DM WHERE DM.numDomainID=RH.numDomainID AND DM.numReturnHeaderID=RH.numReturnHeaderID),0)
		 		 	ELSE 0 END,
		 	NULL,'','',dbo.GetListIemName(RH.numReturnStatus),0,RH.numReturnStatus,''
		 	 FROM ReturnHeader RH where RH.numDomainID = @numDomainID AND RH.numDivisionId=@numDivisionId
                        AND RH.tintReturnType = 1
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Purchase Return
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=8)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 8,'Purchase Return',RH.dtCreatedDate,RH.numReturnHeaderID,
		 	RH.vcRMA,ISNULL(RH.monAmount,0),
		 	ISNULL((SELECT SUM(ISNULL(monAppliedAmount,0)) AS monAmtPaid FROM BillPaymentHeader BPH WHERE BPH.numDomainID=RH.numDomainID AND BPH.numReturnHeaderID=RH.numReturnHeaderID),0),
		 	NULL,'','',dbo.GetListIemName(RH.numReturnStatus),0,RH.numReturnStatus,''
		 	 FROM ReturnHeader RH where RH.numDomainID = @numDomainID AND RH.numDivisionId=@numDivisionId
                        AND RH.tintReturnType = 2
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) between @dtFromDate And @dtToDate))

		 END
		 
		 --Credit Memo
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=9)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 9,'Credit Memo',RH.dtCreatedDate,RH.numReturnHeaderID,
		 	RH.vcRMA,ISNULL(RH.monAmount,0),
		 	ISNULL((SELECT SUM(ISNULL(monAppliedAmount,0)) AS monAmtPaid FROM DepositMaster DM WHERE DM.numDomainID=RH.numDomainID AND DM.numReturnHeaderID=RH.numReturnHeaderID),0),
		 	NULL,'','',dbo.GetListIemName(RH.numReturnStatus),0,RH.numReturnStatus,''
		 	 FROM ReturnHeader RH where RH.numDomainID = @numDomainID AND RH.numDivisionId=@numDivisionId
                        AND RH.tintReturnType = 3
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Refund Receipt
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=10)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 10,'Refund Receipt',RH.dtCreatedDate,RH.numReturnHeaderID,
		 	RH.vcRMA,ISNULL(RH.monAmount,0),
		 	ISNULL((SELECT ISNULL(SUM(CH.monAmount),0) FROM dbo.CheckHeader CH WHERE CH.numDomainID=1 AND CH.tintReferenceType=10 AND CH.numReferenceID=RH.numReturnHeaderID AND ISNULL(CH.bitIsPrint,0)=1),0),
		 	NULL,'','',dbo.GetListIemName(RH.numReturnStatus),0,RH.numReturnStatus,''
		 	 FROM ReturnHeader RH where RH.numDomainID = @numDomainID AND RH.numDivisionId=@numDivisionId
                        AND RH.tintReturnType = 4
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Bills
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=11)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 11,'Bills',BH.dtCreatedDate,BH.numBillID,
		 	'Bill-' + CAST(BH.numBillID AS VARCHAR(18)),ISNULL(BH.monAmountDue,0),
		 	ISNULL(BH.monAmtPaid,0),
		 	NULL,BH.vcMemo,BH.vcReference,'',0,0,''
		 	 FROM BillHeader BH where BH.numDomainID = @numDomainID AND BH.numDivisionId=@numDivisionId
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,BH.dtCreatedDate) between @dtFromDate And @dtToDate))
						AND ISNULL(BH.[bitLandedCost],0) = 0

		 END
		 
		  --Bills Landed Cost
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=16)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 16,'Landed Cost',BH.dtCreatedDate,BH.[numOppId],
		 	'Landed Cost Bill-' + CAST(BH.numBillID AS VARCHAR(18)),ISNULL(BH.monAmountDue,0),
		 	ISNULL(BH.monAmtPaid,0),
		 	NULL,BH.vcMemo,BH.vcReference,'',0,0,''
		 	 FROM BillHeader BH where BH.numDomainID = @numDomainID AND BH.numDivisionId=@numDivisionId
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,BH.dtCreatedDate) between @dtFromDate And @dtToDate))
						AND ISNULL(BH.[bitLandedCost],0) = 1
		 END

		 --UnApplied Bill Payments
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=12)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 12,'Unapplied Bill Payments',BPH.dtCreateDate,BPH.numBillPaymentID,
		 	CASE WHEN ISNULL(numReturnHeaderID,0)>0 THEN 'Return Credit #' + CAST(numReturnHeaderID AS VARCHAR(20)) ELSE 'Unapplied Bill Payment' END
			,ISNULL(BPH.monPaymentAmount,0),
		 	ISNULL(BPH.monAppliedAmount,0),
		 	NULL,'','','',ISNULL(numReturnHeaderID,0),0,''
		 	 FROM BillPaymentHeader BPH where BPH.numDomainID = @numDomainID AND BPH.numDivisionId=@numDivisionId
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,BPH.dtCreateDate) between @dtFromDate And @dtToDate))
                        AND (ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0)) > 0
		 END
		 
		 --Checks
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=13)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 13,'Checks',CH.dtCreatedDate,CH.numCheckHeaderID,
		 	CASE CH.tintReferenceType WHEN 1 THEN 'Checks' 
				  WHEN 8 THEN 'Bill Payment'
				  WHEN 10 THEN 'RMA'
				  WHEN 11 THEN 'Payroll'
				  ELSE 'Check' END + CASE WHEN ISNULL(CH.numCheckNo,0) > 0 THEN ' - #' + CAST(CH.numCheckNo AS VARCHAR(18)) ELSE '' END 
			,ISNULL(CH.monAmount,0),
		 	CASE WHEN ISNULL(bitIsPrint,0)=1 THEN ISNULL(CH.monAmount,0) ELSE 0 END,
		 	NULL,CH.vcMemo,'','',0,0,''
		 	 FROM dbo.CheckHeader CH where CH.numDomainID = @numDomainID AND CH.numDivisionId=@numDivisionId
		 				--AND ch.tintReferenceType=1
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,CH.dtCreatedDate) between @dtFromDate And @dtToDate))

		 END
		 
		 --Deposits
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=14)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 14,'Deposits',DM.dtCreationDate,DM.numDepositId,
		 	'Deposit',
		 	ISNULL(DM.monDepositAmount,0),
		 	ISNULL(DM.monAppliedAmount,0),
		 	NULL,DM.vcMemo,DM.vcReference,'',0,tintDepositePage,''
		 	 FROM DepositMaster DM where DM.numDomainID = @numDomainID AND DM.numDivisionId=@numDivisionId
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,DM.dtCreationDate) between @dtFromDate And @dtToDate))
                        AND tintDepositePage IN (1,2)
		 END
		 
		 --Unapplied Payments
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=15)
		 BEGIN
			INSERT INTO #tempTransaction 
		 	SELECT 15,'Unapplied Payments',DM.dtCreationDate,DM.numDepositId,
		 	CASE WHEN tintDepositePage=3 THEN 'Credit Memo #' + CAST(numReturnHeaderID AS VARCHAR(20)) ELSE 'Unapplied Payment' END,
		 	ISNULL(DM.monDepositAmount,0),
		 	ISNULL(DM.monAppliedAmount,0),
		 	NULL,DM.vcMemo,DM.vcReference,'',ISNULL(numReturnHeaderID,0),tintDepositePage,''
		 	 FROM DepositMaster DM where DM.numDomainID = @numDomainID AND DM.numDivisionId=@numDivisionId
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,DM.dtCreationDate) between @dtFromDate And @dtToDate))
                        AND (ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) > 0 AND tintDepositePage IN(2,3)
		 END
		 
		 DECLARE  @firstRec  AS INTEGER
		 DECLARE  @lastRec  AS INTEGER

         SET @firstRec = (@CurrentPage - 1) * @PageSize
		 SET @lastRec = (@CurrentPage * @PageSize + 1)
		 SET @TotRecs = (SELECT COUNT(*) FROM   #tempTransaction)
         
		 IF @SortCreatedDate = 1 -- Ascending 
		 BEGIN
			SELECT * FROM 
         (SELECT intTransactionType ,vcTransactionType ,dbo.FormatedDateFromDate(dtCreatedDate,@numDomainID) AS [dtCreatedDate],numRecordID ,
				vcRecordName,monTotalAmount,monPaidAmount,dtDueDate E,vcMemo,vcDescription,
				vcStatus ,numRecordID2 ,numStatus, vcBizDocId ,
				ROW_NUMBER() OVER(ORDER BY dtCreatedDate ASC) AS RowNumber FROM #tempTransaction) a
          WHERE RowNumber > @firstRec and RowNumber < @lastRec order BY  intTransactionType
		 END
		 ELSE --By Default Descending
		 BEGIN
			 SELECT * FROM 
         (SELECT intTransactionType ,vcTransactionType ,dbo.FormatedDateFromDate(dtCreatedDate,@numDomainID) AS [dtCreatedDate],numRecordID ,
				vcRecordName,monTotalAmount,monPaidAmount,dtDueDate E,vcMemo,vcDescription,
				vcStatus ,numRecordID2 ,numStatus, vcBizDocId ,
				ROW_NUMBER() OVER(ORDER BY dtCreatedDate DESC) AS RowNumber FROM #tempTransaction) a
          WHERE RowNumber > @firstRec and RowNumber < @lastRec order BY  intTransactionType
         END

		 DROP TABLE #tempTransaction
		 
		 DROP TABLE #temp
    END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetPageNavigationAuthorizationDetails' ) 
    DROP PROCEDURE USP_GetPageNavigationAuthorizationDetails
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- EXEC USP_GetPageNavigationAuthorizationDetails 1,1
CREATE PROCEDURE USP_GetPageNavigationAuthorizationDetails
    (
      @numGroupID NUMERIC(18, 0),
      @numTabID NUMERIC(18, 0),
      @numDomainID NUMERIC(18, 0)
    )
AS 
    SET NOCOUNT ON
    SET TRANSACTION ISOLATION LEVEL READ COMMITTED
  
    BEGIN
		/*
		SELECT * FROM dbo.TabMaster
		SELECT * FROM dbo.ModuleMaster
		SELECT * FROM 
		*/
        PRINT @numTabID 
        DECLARE @numModuleID AS NUMERIC(18, 0)
        SELECT TOP 1
                @numModuleID = numModuleID
        FROM    dbo.PageNavigationDTL
        WHERE   numTabID = @numTabID 
        PRINT @numModuleID
        
        IF @numModuleID = 10 
            BEGIN
                PRINT 10
                SELECT  *
                FROM    ( SELECT    ISNULL(( SELECT TOP 1
                                                    TNA.[numPageAuthID]
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = PND.[numPageNavID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 0) AS [numPageAuthID],
                                    ISNULL(@numGroupID, 0) AS [numGroupID],
                                    ISNULL(PND.[numTabID], 0) AS [numTabID],
                                    ISNULL(PND.[numPageNavID], 0) AS [numPageNavID],
                                    ISNULL(( SELECT TOP 1
                                                    TNA.[bitVisible]
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = PND.[numPageNavID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 0) AS [bitVisible],
                                    @numDomainID AS [numDomainID],
                                    ( SELECT TOP 1
                                                vcPageNavName
                                      FROM      dbo.PageNavigationDTL
                                      WHERE     PageNavigationDTL.numPageNavID = PND.numPageNavID
                                    ) AS [vcNodeName],
                                    ISNULL(( SELECT TOP 1
                                                    TNA.tintType
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = PND.[numPageNavID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 1) AS [tintType]
                          FROM      dbo.PageNavigationDTL PND
                          WHERE     ISNULL(PND.bitVisible, 0) = 1
                                    AND PND.numTabID = @numTabID
                                    AND numModuleID = @numModuleID
                          UNION ALL
                          SELECT    ISNULL(( SELECT TOP 1
                                                    TNA.[numPageAuthID]
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = LD.[numListItemID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 0) AS [numPageAuthID],
                                    @numGroupID AS [numGroupID],
                                    1 AS [numTabID],
                                    ISNULL(LD.[numListItemID], 0) AS [numPageNavID],
                                    ISNULL(( SELECT TOP 1
                                                    TNA.[bitVisible]
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = LD.[numListItemID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 0) AS [bitVisible],
                                    ISNULL(LD.[numDomainID], 0) AS [numDomainID],
                                    ISNULL(vcData, '') + 's' AS [vcNodeName],
                                    ISNULL(( SELECT TOP 1
                                                    TNA.tintType
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = LD.[numListItemID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 1) AS [tintType]
                          FROM      dbo.ListDetails LD
						--LEFT JOIN dbo.TreeNavigationAuthorization TNA ON LD.numListItemID = TNA.numPageNavID 
                          WHERE     numListID = 27
                                    AND LD.numDomainID = @numDomainID
                                    AND ISNULL(constFlag, 0) = 0
						--AND ( numTabID = (CASE WHEN @numModuleID = 10 THEN 1 ELSE 0 END) OR ISNULL(numTabID,0) = 0)
                          UNION ALL
                          SELECT    ISNULL(( SELECT TNA.[numPageAuthID]
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = LD.[numListItemID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 0) AS [numPageAuthID],
                                    @numGroupID AS [numGroupID],
                                    1 AS [numTabID],
                                    ISNULL(LD.[numListItemID], 0) AS [numPageNavID],
                                    ISNULL(( SELECT TNA.[bitVisible]
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = LD.[numListItemID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 0) AS [bitVisible],
                                    @numDomainID AS [numDomainID],
                                    ISNULL(vcData, '') + 's' AS [vcNodeName],
                                    ISNULL(( SELECT TNA.tintType
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = LD.[numListItemID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 1) AS [tintType]
                          FROM      dbo.ListDetails LD
						--LEFT JOIN dbo.TreeNavigationAuthorization TNA ON LD.numListItemID = TNA.numPageNavID AND ISNULL(TNA.[numDomainID], 0) = @numDomainID
                          WHERE     numListID = 27
                                    AND ISNULL(constFlag, 0) = 1
                        ) TABLE1
                ORDER BY numPageNavID
            END
		
        IF @numModuleID = 14 
            BEGIN
                IF NOT EXISTS ( SELECT  *
                                FROM    TreeNavigationAuthorization
                                WHERE   numTabID = 8
                                        AND numDomainID = 1
                                        AND numGroupID = 1 ) 
                    BEGIN
                        SELECT  TNA.numPageAuthID,
                                TNA.numGroupID,
                                TNA.numTabID,
                                TNA.numPageNavID,
                                TNA.bitVisible,
                                TNA.numDomainID,
                                ISNULL(PND.vcPageNavName, '') AS [vcNodeName],
                                tintType
                        FROM    PageNavigationDTL PND
                                JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
                        WHERE   1 = 1
                                --AND ISNULL(TNA.bitVisible, 0) = 1
                                AND ISNULL(PND.bitVisible, 0) = 1
                                AND numModuleID = @numModuleID
                                AND TNA.numDomainID = @numDomainID
                                AND numGroupID = @numGroupID
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                1111 numPageNavID,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                'Regular Documents' AS [vcNodeName],
                                1 tintType
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                Ld.numListItemId,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                ISNULL(vcData, '') AS [vcNodeName],
                                1 tintType
                        FROM    listdetails Ld
                                LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
                                                          AND Lo.numDomainId = @numDomainID
                        WHERE   Ld.numListID = 29
                                AND ( constFlag = 1
                                      OR Ld.numDomainID = @numDomainID
                                    )
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                ld.numListItemID numPageNavID,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                ISNULL(vcData, '') AS [vcNodeName],
                                1 tintType
                        FROM    listdetails Ld
                                LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
                                                          AND lo.numDomainId = @numDomainID
                                INNER JOIN AuthoritativeBizDocs AB ON ( LD.numListItemID = AB.numAuthoritativeSales )
                        WHERE   LD.numListID = 27
                                AND ( constFlag = 1
                                      OR LD.numDomainID = @numDomainID
                                    )
                                AND AB.numDomainID = @numDomainID
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                ld.numListItemID numPageNavID,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                ISNULL(vcData, '') AS [vcNodeName],
                                1 tintType
                        FROM    listdetails Ld
                                LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
                                                          AND lo.numDomainId = @numDomainID
                                INNER JOIN AuthoritativeBizDocs AB ON ( ld.numListItemID = AB.numAuthoritativePurchase )
                        WHERE   LD.numListID = 27
                                AND ( constFlag = 1
                                      OR LD.numDomainID = @numDomainID
                                    )
                                AND AB.numDomainID = @numDomainID
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                0 numPageNavID,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                ISNULL(LT.vcData, '') AS [vcNodeName],
                                1 tintType
                        FROM    ListDetails LT,
                                listdetails ld
                        WHERE   LT.numListID = 11
                                AND Lt.numDomainID = @numDomainID
                                AND LD.numListID = 27
                                AND ( ld.constFlag = 1
                                      OR LD.numDomainID = @numDomainID
                                    )
                                AND ( LD.numListItemID IN (
                                      SELECT    AB.numAuthoritativeSales
                                      FROM      AuthoritativeBizDocs AB
                                      WHERE     AB.numDomainId = @numDomainID )
                                      OR ld.numListItemID IN (
                                      SELECT    AB.numAuthoritativePurchase
                                      FROM      AuthoritativeBizDocs AB
                                      WHERE     AB.numDomainId = @numDomainID )
                                    )
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                2222 numPageNavID,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                'Other BizDocs' AS [vcNodeName],
                                1 tintType
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                ld.numListItemID numPageNavID,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                ISNULL(vcData, '') AS [vcNodeName],
                                1 tintType
                        FROM    listdetails Ld
                        WHERE   ld.numListID = 27
                                AND ( constFlag = 1
                                      OR LD.numDomainID = @numDomainID
                                    )
                                AND LD.numListItemID NOT IN (
                                SELECT  AB.numAuthoritativeSales
                                FROM    AuthoritativeBizDocs AB
                                WHERE   AB.numDomainID = @numDomainID )
                                AND LD.numListItemID NOT IN (
                                SELECT  AB.numAuthoritativePurchase
                                FROM    AuthoritativeBizDocs AB
                                WHERE   AB.numDomainID = @numDomainID )
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                ls.numListItemID numPageNavID,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                ISNULL(ls.vcData, '') AS [vcNodeName],
                                1 tintType
                        FROM    listdetails Ld,
                                ( SELECT    *
                                  FROM      ListDetails LS
                                  WHERE     LS.numDomainID = @numDomainID
                                            AND LS.numListID = 11
                                ) LS
                        WHERE   ld.numListID = 27
                                AND ( ld.constFlag = 1
                                      OR LD.numDomainID = @numDomainID
                                    )
                                AND LD.numListItemID NOT IN (
                                SELECT  AB.numAuthoritativeSales
                                FROM    AuthoritativeBizDocs AB
                                WHERE   AB.numDomainID = @numDomainID
                                UNION
                                SELECT  AB.numAuthoritativePurchase
                                FROM    AuthoritativeBizDocs AB
                                WHERE   AB.numDomainID = @numDomainID )
                        ORDER BY tintType,
                                numPageNavID
                    END
            END
        ELSE 
            BEGIN
				
                SELECT  *
                FROM    ( SELECT    TNA.numPageAuthID,
                                    TNA.numGroupID,
                                    TNA.numTabID,
                                    TNA.numPageNavID,
                                    TNA.bitVisible,
                                    TNA.numDomainID,
                                    ISNULL(PND.vcPageNavName, '') AS [vcNodeName],
                                    tintType
                          FROM      PageNavigationDTL PND
                                    JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
                          WHERE     1 = 1
                                    --AND ISNULL(TNA.bitVisible, 0) = 1
                                    AND ISNULL(PND.bitVisible, 0) = 1
                                    AND numModuleID = @numModuleID
									AND TNA.numTabID = @numTabID
                                    AND TNA.numDomainID = @numDomainID
                                    AND numGroupID = @numGroupID
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    1111 numPageNavID,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    'Regular Documents' AS [vcNodeName],
                                    1 tintType
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    Ld.numListItemId,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    ISNULL(vcData, '') AS [vcNodeName],
                                    1 tintType
                          FROM      listdetails Ld
                                    LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
                                                              AND Lo.numDomainId = @numDomainID
                          WHERE     Ld.numListID = 29
                                    AND ( constFlag = 1
                                          OR Ld.numDomainID = @numDomainID
                                        )
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    ld.numListItemID numPageNavID,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    ISNULL(vcData, '') AS [vcNodeName],
                                    1 tintType
                          FROM      listdetails Ld
                                    LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
                                                              AND lo.numDomainId = @numDomainID
                                    INNER JOIN AuthoritativeBizDocs AB ON ( LD.numListItemID = AB.numAuthoritativeSales )
                          WHERE     LD.numListID = 27
                                    AND ( constFlag = 1
                                          OR LD.numDomainID = @numDomainID
                                        )
                                    AND AB.numDomainID = @numDomainID
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    ld.numListItemID numPageNavID,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    ISNULL(vcData, '') AS [vcNodeName],
                                    1 tintType
                          FROM      listdetails Ld
                                    LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
                                                              AND lo.numDomainId = @numDomainID
                                    INNER JOIN AuthoritativeBizDocs AB ON ( ld.numListItemID = AB.numAuthoritativePurchase )
                          WHERE     LD.numListID = 27
                                    AND ( constFlag = 1
                                          OR LD.numDomainID = @numDomainID
                                        )
                                    AND AB.numDomainID = @numDomainID
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    0 numPageNavID,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    ISNULL(LT.vcData, '') AS [vcNodeName],
                                    1 tintType
                          FROM      ListDetails LT,
                                    listdetails ld
                          WHERE     LT.numListID = 11
                                    AND Lt.numDomainID = @numDomainID
                                    AND LD.numListID = 27
                                    AND ( ld.constFlag = 1
                                          OR LD.numDomainID = @numDomainID
                                        )
                                    AND ( LD.numListItemID IN (
                                          SELECT    AB.numAuthoritativeSales
                                          FROM      AuthoritativeBizDocs AB
                                          WHERE     AB.numDomainId = @numDomainID )
                                          OR ld.numListItemID IN (
                                          SELECT    AB.numAuthoritativePurchase
                                          FROM      AuthoritativeBizDocs AB
                                          WHERE     AB.numDomainId = @numDomainID )
                                        )
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    2222 numPageNavID,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    'Other BizDocs' AS [vcNodeName],
                                    1 tintType
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    ld.numListItemID numPageNavID,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    ISNULL(vcData, '') AS [vcNodeName],
                                    1 tintType
                          FROM      listdetails Ld
                          WHERE     ld.numListID = 27
                                    AND ( constFlag = 1
                                          OR LD.numDomainID = @numDomainID
                                        )
                                    AND LD.numListItemID NOT IN (
                                    SELECT  AB.numAuthoritativeSales
                                    FROM    AuthoritativeBizDocs AB
                                    WHERE   AB.numDomainID = @numDomainID )
                                    AND LD.numListItemID NOT IN (
                                    SELECT  AB.numAuthoritativePurchase
                                    FROM    AuthoritativeBizDocs AB
                                    WHERE   AB.numDomainID = @numDomainID )
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    ls.numListItemID numPageNavID,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    ISNULL(ls.vcData, '') AS [vcNodeName],
                                    1 tintType
                          FROM      listdetails Ld,
                                    ( SELECT    *
                                      FROM      ListDetails LS
                                      WHERE     LS.numDomainID = @numDomainID
                                                AND LS.numListID = 11
                                    ) LS
                          WHERE     ld.numListID = 27
                                    AND ( ld.constFlag = 1
                                          OR LD.numDomainID = @numDomainID
                                        )
                                    AND LD.numListItemID NOT IN (
                                    SELECT  AB.numAuthoritativeSales
                                    FROM    AuthoritativeBizDocs AB
                                    WHERE   AB.numDomainID = @numDomainID
                                    UNION
                                    SELECT  AB.numAuthoritativePurchase
                                    FROM    AuthoritativeBizDocs AB
                                    WHERE   AB.numDomainID = @numDomainID )
                        ) PageNavTable
                        JOIN dbo.TreeNavigationAuthorization TreeNav ON PageNavTable.numPageNavID = TreeNav.numPageNavID
                                                                        AND PageNavTable.numGroupID = TreeNav.numGroupID
                                                                        AND PageNavTable.numTabID = TreeNav.numTabID
                ORDER BY TreeNav.tintType,
                        TreeNav.numPageNavID

            END

        SELECT  *
        FROM    ( SELECT    ISNULL(( SELECT TOP 1
                                            TNA.[numPageAuthID]
                                     FROM   dbo.TreeNavigationAuthorization TNA
                                     WHERE  TNA.numPageNavID = PND.[numPageNavID]
                                            AND TNA.numDomainID = @numDomainID
                                            AND TNA.numGroupID = @numGroupID
                                            AND TNA.numTabID = @numTabID
                                   ), 0) AS [numPageAuthID],
                            ISNULL(@numGroupID, 0) AS [numGroupID],
                            ISNULL(PND.[numTabID], 0) AS [numTabID],
                            ISNULL(PND.[numPageNavID], 0) AS [numPageNavID],
                            ISNULL(( SELECT TOP 1
                                            TNA.[bitVisible]
                                     FROM   dbo.TreeNavigationAuthorization TNA
                                     WHERE  TNA.numPageNavID = PND.[numPageNavID]
                                            AND TNA.numDomainID = @numDomainID
                                            AND TNA.numGroupID = @numGroupID
                                            AND TNA.numTabID = @numTabID
                                   ), 0) AS [bitVisible],
                            @numDomainID AS [numDomainID],
                            ( SELECT TOP 1
                                        vcPageNavName
                              FROM      dbo.PageNavigationDTL
                              WHERE     PageNavigationDTL.numPageNavID = PND.numPageNavID
                            ) AS [vcNodeName],
                            ISNULL(( SELECT TOP 1
                                            TNA.tintType
                                     FROM   dbo.TreeNavigationAuthorization TNA
                                     WHERE  TNA.numPageNavID = PND.[numPageNavID]
                                            AND TNA.numDomainID = @numDomainID
                                            AND TNA.numGroupID = @numGroupID
                                            AND TNA.numTabID = @numTabID
                                   ), 1) AS [tintType]
                  FROM      dbo.PageNavigationDTL PND
                  WHERE     ISNULL(PND.bitVisible, 0) = 1
                            AND PND.numTabID = @numTabID
                            AND numModuleID = @numModuleID
                  UNION ALL
                  SELECT    ISNULL([numPageAuthID], 0) AS [numPageAuthID],
                            @numGroupID AS [numGroupID],
                            @numTabID AS [numTabID],
                            ISNULL(LD.[numListItemID], 0) AS [numPageNavID],
                            ISNULL(TNA.[bitVisible], 0) AS [bitVisible],
                            ISNULL(LD.[numDomainID], 0) AS [numDomainID],
                            vcData AS [vcNodeName],
                            1 AS [tintType]
                  FROM      dbo.ListDetails LD
                            LEFT JOIN dbo.TreeNavigationAuthorization TNA ON LD.numListItemID = TNA.numPageNavID
                  WHERE     numListID = 27
                            AND LD.numDomainID = @numDomainID
                            AND ISNULL(constFlag, 0) = 0
                            AND numTabID = ( CASE WHEN @numModuleID = 10
                                                  THEN 1
                                                  ELSE 0
                                             END )
                  UNION ALL
                  SELECT    ISNULL([numPageAuthID], 0) AS [numPageAuthID],
                            @numGroupID AS [numGroupID],
                            @numTabID AS [numTabID],
                            ISNULL(LD.[numListItemID], 0) AS [numPageNavID],
                            ISNULL(TNA.[bitVisible], 0) AS [bitVisible],
                            ISNULL(TNA.[numDomainID], 0) AS [numDomainID],
                            vcData AS [vcNodeName],
                            1 AS [tintType]
                  FROM      dbo.ListDetails LD
                            LEFT JOIN dbo.TreeNavigationAuthorization TNA ON LD.numListItemID = TNA.numPageNavID
                                                                             AND ISNULL(TNA.[numDomainID], 0) = @numDomainID
                  WHERE     numListID = 27
                            AND ISNULL(constFlag, 0) = 1
                            AND numTabID = @numTabID
                ) TABLE2
        ORDER BY numPageNavID  
    END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPayrollDetail')
DROP PROCEDURE USP_GetPayrollDetail
GO
CREATE PROCEDURE [dbo].[USP_GetPayrollDetail]
(
    @numDomainId AS NUMERIC(9) = 0,
    @numPayrollHeaderID AS NUMERIC(9)=0,
    @ClientTimeZoneOffset INT
)
AS
  BEGIN
  	DECLARE @dtStartDate AS DATETIME
DECLARE @dtEndDate AS DATETIME
 
SELECT  numPayrollHeaderID,
		numPayrolllReferenceNo,
		numDomainId,
		dtFromDate,
		dtToDate,
		dtPaydate,
		numCreatedBy,
		dtCreatedDate,
		numModifiedBy,
		dtModifiedDate,
		ISNULL(numPayrollStatus,0) AS [numPayrollStatus] 
FROM  PayrollHeader WHERE numPayrollHeaderID=@numPayrollHeaderID AND numDomainId=@numDomainId

SELECT @dtStartDate=dtFromDate,@dtEndDate=dtToDate FROM dbo.PayrollHeader WHERE numPayrollHeaderID=@numPayrollHeaderID AND numDomainId=@numDomainId

CREATE TABLE #tempHrs (numPayrollDetailID NUMERIC,numCheckStatus NUMERIC,numUserId NUMERIC,vcUserName VARCHAR(100)
,numUserCntID NUMERIC,vcEmployeeId VARCHAR(50),monHourlyRate MONEY,bitOverTime BIT,bitOverTimeHrsDailyOrWeekly BIT,numLimDailHrs FLOAT,monOverTimeRate MONEY,
 decRegularHrs decimal(10,2),decActualRegularHrs decimal(10,2),decOvertimeHrs decimal(10,2),decActualOvertimeHrs decimal(10,2),
 decPaidLeaveHrs decimal(10,2),decActualPaidLeaveHrs decimal(10,2),decTotalHrs decimal(10,2),decActualTotalHrs decimal(10,2),
 monExpense MONEY,monActualExpense MONEY,monReimburse MONEY,monActualReimburse MONEY,
 monCommPaidInvoice MONEY,monActualCommPaidInvoice MONEY,monCommUNPaidInvoice MONEY,monActualCommUNPaidInvoice MONEY,monDeductions MONEY)

 INSERT INTO #tempHrs(numPayrollDetailID,numCheckStatus,numUserId,vcUserName,numUserCntID,vcEmployeeId,monHourlyRate,bitOverTime,bitOverTimeHrsDailyOrWeekly,numLimDailHrs,monOverTimeRate,monDeductions)
 SELECT ISNULL(PD.numPayrollDetailID,0) AS numPayrollDetailID,ISNULL(PD.numCheckStatus,0) AS numCheckStatus,UM.numUserId,Isnull(ADC.vcfirstname + ' ' + adc.vclastname,'-') vcUserName,
           adc.numcontactid AS numUserCntID,Isnull(um.vcEmployeeId,'') AS vcEmployeeId,
 CASE WHEN ISNULL(PD.numPayrollDetailID,0)>0 THEN ISNULL(PD.monHourlyRate,0) ELSE ISNULL(UM.monHourlyRate,0) END AS monHourlyRate,
 CASE WHEN ISNULL(PD.numPayrollDetailID,0)>0 THEN ISNULL(PD.bitOverTime,0) ELSE ISNULL(UM.bitOverTime,0) END AS bitOverTime,
 CASE WHEN ISNULL(PD.numPayrollDetailID,0)>0 THEN ISNULL(PD.bitOverTimeHrsDailyOrWeekly,0) ELSE ISNULL(UM.bitOverTimeHrsDailyOrWeekly,0) END AS bitOverTimeHrsDailyOrWeekly,
 CASE WHEN ISNULL(PD.numPayrollDetailID,0)>0 THEN ISNULL(PD.numLimDailHrs,0) ELSE ISNULL(UM.numLimDailHrs,0) END AS numLimDailHrs,
 CASE WHEN ISNULL(PD.numPayrollDetailID,0)>0 THEN ISNULL(PD.monOverTimeRate,0) ELSE ISNULL(UM.monOverTimeRate,0) END AS monOverTimeRate,ISNULL(PD.monDeductions,0) AS monDeductions
 FROM  dbo.UserMaster UM JOIN dbo.AdditionalContactsInformation adc ON adc.numcontactid = um.numuserdetailid
	   LEFT JOIN PayrollDetail PD ON PD.numUserCntID=ADC.numcontactid AND PD.numPayrollHeaderID=@numPayrollHeaderID
			WHERE UM.numDomainId=@numDomainId 

	
  Declare @decTotalHrsWorked as decimal(10,2),@decActualTotalHrsWorked as decimal(10,2);       
  Declare @decTotalOverTimeHrsWorked as decimal(10,2),@decActualTotalOverTimeHrsWorked as decimal(10,2);    
  Declare @decTotalHrs as decimal(10,2),@decActualTotalHrs as decimal(10,2);  
  DECLARE @decTotalPaidLeaveHrs AS DECIMAL(10,2),@decActualTotalPaidLeaveHrs AS DECIMAL(10,2)     
  DECLARE @bitOverTime bit,@bitOverTimeHrsDailyOrWeekly bit,@decOverTime decimal          
  DECLARE @monExpense MONEY,@monReimburse MONEY,@monCommPaidInvoice MONEY,@monCommUNPaidInvoice MONEY 
  DECLARE @monActualExpense MONEY,@monActualReimburse MONEY,@monActualCommPaidInvoice MONEY,@monActualCommUNPaidInvoice MONEY 
    
DECLARE  @minID NUMERIC(9),@maxID NUMERIC(9)

SELECT * INTO #tempPayrollTracking FROM PayrollTracking WHERE numDomainId=@numDomainId

SELECT @maxID = max(numUserCntID),@minID = min(numUserCntID) FROM #tempHrs

  WHILE @minID <= @maxID
  BEGIN
  
  SELECT @decTotalHrsWorked=0,@decTotalOverTimeHrsWorked=0,@decTotalHrs=0,@decTotalPaidLeaveHrs=0,
		@monExpense=0,@monReimburse=0,@monCommPaidInvoice=0,@monCommUNPaidInvoice=0,
		@decActualTotalHrsWorked=0,@decActualTotalOverTimeHrsWorked=0,@decActualTotalHrs=0,
		@decActualTotalPaidLeaveHrs=0,@monActualExpense=0,@monActualReimburse=0,@monActualCommPaidInvoice=0,@monActualCommUNPaidInvoice=0
   
  SELECT @bitOverTime=bitOverTime,@bitOverTimeHrsDailyOrWeekly=bitOverTimeHrsDailyOrWeekly,@decOverTime=numLimDailHrs
   FROM #tempHrs WHERE numUserCntID=@minID
  
  
  --Regular Hrs
  Select @decTotalHrsWorked=sum(x.Hrs) From (Select  isnull(Sum(Cast(datediff(minute,dtfromdate,dttodate) as float)/Cast(60 as float)),0) as Hrs            
  from TimeAndExpense Where (numType=1 Or numType=2)  And numCategory=1                 
--  And ((dateadd(minute,-@ClientTimeZoneOffset,dtFromDate) between @dtStartDate And 	@dtEndDate )
--	Or (dateadd(minute,-@ClientTimeZoneOffset,dtToDate) between @dtStartDate And @dtEndDate)) 
	And             
  numUserCntID=@minID  And numDomainID=@numDomainId 
  AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE numPayrollHeaderID!=@numPayrollHeaderID AND ISNULL(numCategoryHDRID,0)>0)) x           
  
  --Actual Regular Hrs
  Select @decActualTotalHrsWorked=sum(x.Hrs) From (Select  isnull(Sum(Cast(datediff(minute,dtfromdate,dttodate) as float)/Cast(60 as float)),0) as Hrs            
  from TimeAndExpense Where (numType=1 Or numType=2)  And numCategory=1                 
--  And ((dateadd(minute,-@ClientTimeZoneOffset,dtFromDate) between @dtStartDate And 	@dtEndDate )
--	Or (dateadd(minute,-@ClientTimeZoneOffset,dtToDate) between @dtStartDate And @dtEndDate)) 
	And             
  numUserCntID=@minID  And numDomainID=@numDomainId 
  AND numCategoryHDRID IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE numPayrollHeaderID=@numPayrollHeaderID AND ISNULL(numCategoryHDRID,0)>0)) x           

 --OrverTime Hrs         
 if @bitOverTime=1  AND @bitOverTimeHrsDailyOrWeekly=1 AND @decTotalHrsWorked > @decOverTime                     
 Begin                      
	SET @decTotalOverTimeHrsWorked=@decTotalHrsWorked - @decOverTime
 END
 
  if @bitOverTime=1  AND @bitOverTimeHrsDailyOrWeekly=1 AND @decActualTotalHrsWorked > @decOverTime                     
 Begin                      
	SET @decActualTotalOverTimeHrsWorked= @decActualTotalHrsWorked - @decOverTime
 END
    
 --Paid Leave Hrs
 SELECT @decTotalPaidLeaveHrs=ISNULL(SUM(CASE WHEN dtfromdate = dttodate THEN 24 - (CASE WHEN bitfromfullday = 0 THEN 12 ELSE 0 END)
           ELSE (Datediff(DAY,dtfromdate,dttodate) + 1) * 24 
                                - (CASE WHEN bitfromfullday = 0 THEN 12 ELSE 0 END)
                                - (CASE WHEN bittofullday = 0 THEN 12 ELSE 0 END) END),0)
            FROM   timeandexpense
            WHERE  numtype = 3 AND numcategory = 3 AND numusercntid = @minID AND numdomainid = @numDomainId 
--                   And ((dateadd(minute,-@ClientTimeZoneOffset,dtFromDate) between @dtStartDate And @dtEndDate )
--					Or (dateadd(minute,-@ClientTimeZoneOffset,dtToDate) between @dtStartDate And @dtEndDate))
					AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE numPayrollHeaderID!=@numPayrollHeaderID AND ISNULL(numCategoryHDRID,0)>0)
 
 --Actual Paid Leave Hrs
 SELECT @decActualTotalPaidLeaveHrs=ISNULL(SUM(CASE WHEN dtfromdate = dttodate THEN 24 - (CASE WHEN bitfromfullday = 0 THEN 12 ELSE 0 END)
           ELSE (Datediff(DAY,dtfromdate,dttodate) + 1) * 24 
                                - (CASE WHEN bitfromfullday = 0 THEN 12 ELSE 0 END)
                                - (CASE WHEN bittofullday = 0 THEN 12 ELSE 0 END) END),0)
            FROM   timeandexpense
            WHERE  numtype = 3 AND numcategory = 3 AND numusercntid = @minID AND numdomainid = @numDomainId 
--                   And ((dateadd(minute,-@ClientTimeZoneOffset,dtFromDate) between @dtStartDate And 	@dtEndDate )
--					Or (dateadd(minute,-@ClientTimeZoneOffset,dtToDate) between @dtStartDate And @dtEndDate))
					AND numCategoryHDRID IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE numPayrollHeaderID=@numPayrollHeaderID AND ISNULL(numCategoryHDRID,0)>0)

 --Expenses
Select @monExpense=isnull((Sum(Cast(monAmount as float)))   ,0)    
from TimeAndExpense Where numCategory=2 And numType in (1,2) 
And numUserCntID=@minID And numDomainID=@numDomainId 
--AND (dtFromDate between @dtStartDate And @dtEndDate Or dtToDate between @dtStartDate And @dtEndDate) 
AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE numPayrollHeaderID!=@numPayrollHeaderID AND ISNULL(numCategoryHDRID,0)>0)    

 --Actual Expenses
Select @monActualExpense=isnull((Sum(Cast(monAmount as float)))   ,0)    
from TimeAndExpense Where numCategory=2 And numType in (1,2) 
And numUserCntID=@minID And numDomainID=@numDomainId 
--AND (dtFromDate between @dtStartDate And @dtEndDate Or dtToDate between @dtStartDate And @dtEndDate) 
AND numCategoryHDRID IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE numPayrollHeaderID=@numPayrollHeaderID AND ISNULL(numCategoryHDRID,0)>0)    


 --Reimbursable Expenses
SELECT @monReimburse=Isnull((SUM(CAST(monamount AS FLOAT))),0)
            FROM   timeandexpense 
            WHERE  bitreimburse = 1 AND numcategory = 2 
                   AND numusercntid = @minID AND numdomainid = @numDomainId
                   --AND dtfromdate BETWEEN @dtStartDate AND @dtEndDate
                   AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE numPayrollHeaderID!=@numPayrollHeaderID AND ISNULL(numCategoryHDRID,0)>0)

 --Actual Reimbursable Expenses
SELECT @monActualReimburse=Isnull((SUM(CAST(monamount AS FLOAT))),0)
            FROM   timeandexpense 
            WHERE  bitreimburse = 1 AND numcategory = 2 
                   AND numusercntid = @minID AND numdomainid = @numDomainId
                   --AND dtfromdate BETWEEN @dtStartDate AND @dtEndDate
                   AND numCategoryHDRID IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE numPayrollHeaderID=@numPayrollHeaderID AND ISNULL(numCategoryHDRID,0)>0)

--LOGIC OF COMMISSION TYPE 3 (PROEJCT GROSS PROFIT) IS REMOVED BECAUSE THIS OPTION IS REMOVED FROM GLOBAL SETTINGS 

	--Commission Paid Invoice 
   SELECT @monCommPaidInvoice=isnull(sum(BC.numComissionAmount - ISNULL(PT.monCommissionAmt,0)),0) 
   FROM OpportunityMaster Opp 
		INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
        INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId 
        LEFT JOIN #tempPayrollTracking PT ON PT.numComissionID=BC.numComissionID AND PT.numPayrollHeaderID!=@numPayrollHeaderID
   where Opp.numDomainId=@numDomainId and
   BC.numUserCntId=@minID AND BC.bitCommisionPaid=0 
   AND oppBiz.bitAuthoritativeBizDocs = 1 AND isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount
   --AND ((dateadd(minute,-@ClientTimeZoneOffset,oppBiz.dtCreatedDate) between @dtStartDate And @dtEndDate))


--Actual Commission Paid Invoice 
   SELECT @monActualCommPaidInvoice=isnull(sum(PT.monCommissionAmt),0) 
   FROM OpportunityMaster Opp 
		INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
        INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId 
        JOIN #tempPayrollTracking PT ON PT.numComissionID=BC.numComissionID AND PT.numPayrollHeaderID=@numPayrollHeaderID
   where Opp.numDomainId=@numDomainId and
   BC.numUserCntId=@minID AND BC.bitCommisionPaid=0 
   AND oppBiz.bitAuthoritativeBizDocs = 1 AND isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount
   --AND ((dateadd(minute,-@ClientTimeZoneOffset,oppBiz.dtCreatedDate) between @dtStartDate And @dtEndDate))
   

--Commission UnPaid Invoice 
 SELECT @monCommUNPaidInvoice=isnull(sum(BC.numComissionAmount - ISNULL(PT.monCommissionAmt,0)),0) 
   FROM OpportunityMaster Opp 
		INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
        INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId
        LEFT JOIN #tempPayrollTracking PT ON PT.numComissionID=BC.numComissionID AND PT.numPayrollHeaderID!=@numPayrollHeaderID
   where Opp.numDomainId=@numDomainId and
   BC.numUserCntId=@minID AND BC.bitCommisionPaid=0 
   AND oppBiz.bitAuthoritativeBizDocs = 1 AND isnull(oppBiz.monAmountPaid,0) <oppBiz.monDealAmount
   --AND ((dateadd(minute,-@ClientTimeZoneOffset,oppBiz.dtCreatedDate) between @dtStartDate And @dtEndDate)) 
    
	   
--Actual Commission UnPaid Invoice 
 SELECT @monActualCommUNPaidInvoice=isnull(sum(PT.monCommissionAmt),0) 
   FROM OpportunityMaster Opp 
		INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
        INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId
        JOIN #tempPayrollTracking PT ON PT.numComissionID=BC.numComissionID AND PT.numPayrollHeaderID=@numPayrollHeaderID
   where Opp.numDomainId=@numDomainId and
   BC.numUserCntId=@minID AND BC.bitCommisionPaid=0 
   AND oppBiz.bitAuthoritativeBizDocs = 1 AND isnull(oppBiz.monAmountPaid,0) <oppBiz.monDealAmount
   --AND ((dateadd(minute,-@ClientTimeZoneOffset,oppBiz.dtCreatedDate) between @dtStartDate And @dtEndDate))                                   

                                           
 UPDATE #tempHrs SET    decRegularHrs=@decTotalHrsWorked-@decTotalOverTimeHrsWorked,
						decOvertimeHrs=@decTotalOverTimeHrsWorked,
						decTotalHrs=@decTotalHrsWorked ,
						decPaidLeaveHrs=@decTotalPaidLeaveHrs,
						monExpense=@monExpense,
						monReimburse=@monReimburse,
						monCommPaidInvoice=@monCommPaidInvoice,
						monCommUNPaidInvoice=@monCommUNPaidInvoice,
						
						decActualRegularHrs=ISNULL(@decActualTotalHrsWorked,0)-ISNULL(@decActualTotalOverTimeHrsWorked,0),
						decActualOvertimeHrs=ISNULL(@decActualTotalOverTimeHrsWorked,0),
						decActualTotalHrs=ISNULL(@decActualTotalHrsWorked,0),
						decActualPaidLeaveHrs=ISNULL(@decActualTotalPaidLeaveHrs,0),
						monActualExpense=ISNULL(@monActualExpense,0),
						monActualReimburse=ISNULL(@monActualReimburse,0),
						monActualCommPaidInvoice=ISNULL(@monActualCommPaidInvoice,0),
						monActualCommUNPaidInvoice=ISNULL(@monActualCommUNPaidInvoice,0)
          WHERE numUserCntID=@minID                  

    SELECT @minID = min(numUserCntID) FROM #tempHrs WHERE numUserCntID > @minID 
  END

SELECT * FROM #tempHrs
DROP TABLE #tempPayrollTracking
DROP TABLE #tempHrs

SELECT MAX(numPayrolllReferenceNo) + 1 AS [numMaxPayrolllReferenceNo] FROM dbo.PayrollHeader WHERE numDomainId = @numDomainId

END
GO
--Created By                                                          
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_gettableinfodefault' ) 
    DROP PROCEDURE usp_gettableinfodefault
GO
CREATE PROCEDURE [dbo].[usp_GetTableInfoDefault]                                                                        
@numUserCntID numeric=0,                        
@numRecordID numeric=0,                                    
@numDomainId numeric=0,                                    
@charCoType char(1) = 'a',                        
@pageId as numeric,                        
@numRelCntType as NUMERIC,                        
@numFormID as NUMERIC,                        
@tintPageType TINYINT             
                             
                                    
--                                                           
                                               
AS                                      
                                    
IF (
	SELECT 
		ISNULL(sum(TotalRow),0)  
	FROM
		(            
			Select count(*) TotalRow FROM View_DynamicColumns WHERE numUserCntId=@numuserCntid AND numDomainId = @numDomainId AND numFormID=@numFormID AND numRelCntType=@numRelCntType AND tintPageType=@tintPageType
			Union 
			Select count(*) TotalRow from View_DynamicCustomColumns WHERE numUserCntId=@numuserCntid AND numDomainId = @numDomainId AND numFormID=@numFormID AND numRelCntType=@numRelCntType AND tintPageType=@tintPageType
		) TotalRows
	) <> 0              
BEGIN                           
          
                
if @pageid=1 or @pageid=4 or @pageid= 12 or  @pageid= 13 or  @pageid= 14
 begin              
      
        SELECT numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
        ,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
         '' as vcValue,bitCustom AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
         numListID,0 numListItemID,PopupFunctionName,
Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
,(SELECT TOP 1 FldDTLID from CFW_FLD_Values where  RecId=@numRecordID and Fld_Id=numFieldId) as FldDTLID
,'' as Value,vcDbColumnName,vcToolTip,intFieldMaxLength
FROM View_DynamicColumns
  where numFormId=@numFormID and numUserCntID=@numUserCntID and  numDomainID=@numDomainID  
  and ISNULL(bitCustom,0)=0 and numRelCntType=@numRelCntType AND tintPageType=@tintPageType AND 
   1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
		  WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
		  ELSE 0 END) 
 union          
    
    
    select numFieldId,vcfieldName ,vcURL,vcAssociatedControlType as fld_type,               
   tintRow,tintcolumn as intcoulmn,
 case when vcAssociatedControlType = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID))                
 else dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID) end as vcValue,                    
 convert(bit,1) bitCustomField,'' vcPropertyName,  
 convert(bit,1) as bitCanBeUpdated,numListID,case when vcAssociatedControlType = 'SelectBox' THEN dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage
,isnull((SELECT TOP 1 FldDTLID from CFW_FLD_Values where  RecId=@numRecordID and Fld_Id=numFieldID),0) as FldDTLID
,dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID) as Value,'' AS vcDbColumnName,vcToolTip,0 intFieldMaxLength
from View_DynamicCustomColumns_RelationShip
 where grp_id=@PageId and numRelation=@numRelCntType and numDomainID=@numDomainID and subgrp=0
 and numUserCntID=@numUserCntID 
 AND numFormId=@numFormID and numRelCntType=@numRelCntType AND tintPageType=@tintPageType
      
 order by tintRow,intcoulmn   
 end                
                
if @PageId= 2 or  @PageId= 3 or @PageId= 5 or @PageId= 6 or @PageId= 7 or @PageId= 8   or @PageId= 11                          
 begin            
		IF @numFormID = 123 -- Add/Edit Order - Item Grid Column Settings
		BEGIN
			SELECT numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
					'' as vcValue,ISNULL(bitCustom,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
					numListID,0 numListItemID,PopupFunctionName,
		Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
		Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,vcDbColumnName,vcToolTip,intFieldMaxLength
		FROM View_DynamicColumns
			where numFormId=@numFormID and numUserCntID=@numUserCntID and  numDomainID=@numDomainID  
			and ISNULL(bitCustom,0)=0 and numRelCntType=@numRelCntType AND tintPageType=@tintPageType 

			union          
           
			select numFieldId,vcfieldName ,vcURL,vcAssociatedControlType as fld_type,               
			tintRow,tintcolumn as intcoulmn,
			case when vcAssociatedControlType = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID))                
			else dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) end as vcValue,                    
			convert(bit,1) bitCustomField,'' vcPropertyName,  
			convert(bit,1) as bitCanBeUpdated,numListID,case when vcAssociatedControlType = 'SelectBox' THEN dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
		Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
		Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,'' AS vcDbColumnName
		,vcToolTip,0 intFieldMaxLength
		from View_DynamicCustomColumns
			where grp_id=@PageId and numDomainID=@numDomainID and subgrp=0
			and numUserCntID=@numUserCntID  
			AND numFormId=@numFormID and ISNULL(bitCustom,0)=1 and numRelCntType=@numRelCntType AND tintPageType=@tintPageType
    
			order by tintrow,intcoulmn      
		END
		ELSE
		BEGIN    
				SELECT numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
					'' as vcValue,ISNULL(bitCustom,0) AS bitCustomField,vcPropertyName,
					--DO NOT ALLOW CHANGE OF ASSIGNED TO FIELD AFTER COMMISSION IS PAID
					CAST((CASE 
					WHEN @charCoType='O' AND numFieldID=100
					THEN 
						CASE 
						WHEN (SELECT COUNT(*) FROM BizDocComission WHERE numOppID=@numRecordID AND ISNULL(bitCommisionPaid,0)=1) > 0 
						THEN 
							0 
						ELSE 
							ISNULL(bitAllowEdit,0) 
						END 
					ELSE 
						ISNULL(bitAllowEdit,0) 
					END) AS BIT) AS bitCanBeUpdated,
					numListID,0 numListItemID,PopupFunctionName,
		Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
		Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,vcDbColumnName,vcToolTip,intFieldMaxLength
		FROM View_DynamicColumns
			where numFormId=@numFormID and numUserCntID=@numUserCntID and  numDomainID=@numDomainID  
			and ISNULL(bitCustom,0)=0 and numRelCntType=@numRelCntType AND tintPageType=@tintPageType 
			AND  1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
					WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
					ELSE 0 END) 

			union          
           
			select numFieldId,vcfieldName ,vcURL,vcAssociatedControlType as fld_type,               
			tintRow,tintcolumn as intcoulmn,
			case when vcAssociatedControlType = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID))                
			else dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) end as vcValue,                    
			convert(bit,1) bitCustomField,'' vcPropertyName,  
			convert(bit,1) as bitCanBeUpdated,numListID,case when vcAssociatedControlType = 'SelectBox' THEN dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
		Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
		Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,'' AS vcDbColumnName
		,vcToolTip,0 intFieldMaxLength
		from View_DynamicCustomColumns
			where grp_id=@PageId and numDomainID=@numDomainID and subgrp=0
			and numUserCntID=@numUserCntID  
			AND numFormId=@numFormID and ISNULL(bitCustom,0)=1 and numRelCntType=@numRelCntType AND tintPageType=@tintPageType
    
			order by tintrow,intcoulmn      
	END                          
                             
 end  
               
if @PageId= 0            
  begin         
  SELECT HDR.numFieldId,HDR.vcFieldName,'' as vcURL,'' as fld_type,DTL.tintRow,DTL.intcoulmn,          
  HDR.vcDBColumnName,convert(char(1),DTL.bitCustomField)as bitCustomField,
  0 as bitIsRequired,0 bitIsEmail,0 bitIsAlphaNumeric,0 bitIsNumeric, 0 bitIsLengthValidation,0 bitFieldMessage,0 intMaxLength,0 intMinLength, 0 bitFieldMessage,'' vcFieldMessage
   from PageLayoutdtl DTL                                    
  join pagelayout HDR on DTl.numFieldId= HDR.numFieldId                                    
   where HDR.Ctype = @charCoType and numUserCntId=@numUserCntId  and bitCustomField=0 and            
  numDomainId = @numDomainId and DTL.numRelCntType=@numRelCntType   order by DTL.tintrow,intcoulmn          
  end                
end                 



ELSE IF @tintPageType=2 and @numRelCntType>3 and 
((select isnull(sum(TotalRow),0) from (Select count(*) TotalRow from View_DynamicColumns where numFormId=@numFormID and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=@tintPageType AND numFormID IN(36) AND numRelCntType=2 
Union 
Select count(*) TotalRow from View_DynamicCustomColumns where numFormId=@numFormID and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=@tintPageType AND numFormID IN(36) AND numRelCntType=2) TotalRows
) <> 0 )
BEGIN
	exec usp_GetTableInfoDefault @numUserCntID=@numUserCntID,@numRecordID=@numRecordID,@numDomainId=@numDomainId,@charCoType=@charCoType,@pageId=@pageId,
			@numRelCntType=2,@numFormID=@numFormID,@tintPageType=@tintPageType
END
                           
ELSE IF @charCoType<>'b'/*added by kamal to prevent showing of all  fields on checkout by default*/                  
BEGIN                 

	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)                                    

	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID   
       
	IF @pageid=1 or @pageid=4 or @pageid= 12 or  @pageid= 13 or  @pageid= 14                
	BEGIN   
		--Check if Master Form Configuration is created by administrator if NoColumns=0

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormID AND numRelCntType=@numRelCntType AND bitGridConfiguration = 0 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END

		--If MasterConfiguration is available then load it otherwise load default columns
		IF @IsMasterConfAvailable = 1
		BEGIN
			SELECT 
				numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
				 '' as vcValue,convert(bit,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
				 0 Tabletype,numListID,0 numListItemID,PopupFunctionName,
				Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
				Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
				bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
				bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
				,vcDbColumnName,vcToolTip,intFieldMaxLength
			FROM 
				View_DynamicColumnsMasterConfig
			WHERE
				View_DynamicColumnsMasterConfig.numFormId=@numFormID AND 
				View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
				View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
				View_DynamicColumnsMasterConfig.numRelCntType=@numRelCntType AND 
				View_DynamicColumnsMasterConfig.bitGridConfiguration = 0 AND
				ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
			UNION
			SELECT 
				numFieldId as numFieldId,vcFieldName as vcfieldName ,vcURL,vcFieldType,               
				convert(tinyint,0) as tintrow,convert(int,0) as intcoulmn,  
				case when vcFieldType = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID))                
				 else dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) end as vcValue,                    
				 convert(bit,1) bitCustomField,'' vcPropertyName,  
				 convert(bit,1) as bitCanBeUpdated,1 Tabletype,numListID,case when vcFieldType = 'SelectBox' THEN dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
				Case when vcFieldType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
				Case when vcFieldType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
				ISNULL(bitIsRequired,0) bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
				,'' AS vcDbColumnName,vcToolTip,0 intFieldMaxLength
			FROM 
				View_DynamicCustomColumnsMasterConfig
			WHERE 
				View_DynamicCustomColumnsMasterConfig.numFormId=@numFormID AND 
				View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
				View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
				View_DynamicCustomColumnsMasterConfig.numRelCntType=@numRelCntType AND 
				View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 0 AND
				ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
			ORDER BY 
				Tabletype,tintrow,intcoulmn  
		END
		ELSE                                        
		BEGIN
			SELECT numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
        ,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
         '' as vcValue,convert(bit,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
         0 Tabletype,numListID,0 numListItemID,PopupFunctionName,
Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
,vcDbColumnName,vcToolTip,intFieldMaxLength
FROM View_DynamicDefaultColumns 
  where numFormId=@numFormID AND numDomainID=@numDomainID AND
   1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
		  WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
		  ELSE 0 END) 
                   
   union     
  
     select fld_id as numFieldId,fld_label as vcfieldName ,vcURL,fld_type,               
   convert(tinyint,0) as tintrow,convert(int,0) as intcoulmn,  
 case when fld_type = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(fld_id,@PageId,@numRecordID))                
 else dbo.GetCustFldValue(fld_id,@PageId,@numRecordID) end as vcValue,                    
 convert(bit,1) bitCustomField,'' vcPropertyName,  
 convert(bit,1) as bitCanBeUpdated,1 Tabletype,CFM.numListID,case when fld_type = 'SelectBox' THEN dbo.GetCustFldValue(fld_id,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
Case when fld_type='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=CFM.numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
Case when fld_type='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=CFM.numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
ISNULL(V.bitIsRequired,0) bitIsRequired,V.bitIsEmail,V.bitIsAlphaNumeric,V.bitIsNumeric,V.bitIsLengthValidation,V.bitFieldMessage,V.intMaxLength,V.intMinLength,V.bitFieldMessage,ISNULL(V.vcFieldMessage,'') vcFieldMessage
,'' AS vcDbColumnName,CFM.vcToolTip,0 intFieldMaxLength
from CFW_Fld_Master CFM join CFW_Fld_Dtl CFD on Fld_id=numFieldId                                          
 left join CFw_Grp_Master CFG on subgrp=CFG.Grp_id                                          
 LEFT JOIN CFW_Validation V ON V.numFieldID = CFM.Fld_id
 where CFM.grp_id=@PageId and CFD.numRelation=@numRelCntType and CFM.numDomainID=@numDomainID and subgrp=0

 order by Tabletype,tintrow,intcoulmn                        
		END     
	END      
             
	IF @PageId= 2 or  @PageId= 3 or @PageId= 5 or @PageId= 6 or @PageId= 7 or @PageId= 8  or @PageId= 11                          
	BEGIN  
		--Check if Master Form Configuration is created by administrator if NoColumns=0

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormID AND numRelCntType=@numRelCntType AND bitGridConfiguration = 0 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END

		--If MasterConfiguration is available then load it otherwise load default columns
		IF @IsMasterConfAvailable = 1
		BEGIN
			SELECT 
				numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
				 '' as vcValue,convert(bit,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
				 0 Tabletype,numListID,0 numListItemID,PopupFunctionName,
				Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
				Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
				bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
				bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
				,vcDbColumnName,vcToolTip,intFieldMaxLength
			FROM 
				View_DynamicColumnsMasterConfig
			WHERE
				View_DynamicColumnsMasterConfig.numFormId=@numFormID AND 
				View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
				View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
				View_DynamicColumnsMasterConfig.numRelCntType=@numRelCntType AND 
				View_DynamicColumnsMasterConfig.bitGridConfiguration = 0 AND
				ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
			UNION
			SELECT 
				numFieldId as numFieldId,vcFieldName as vcfieldName ,vcURL,vcFieldType,               
				convert(tinyint,0) as tintrow,convert(int,0) as intcoulmn,  
				case when vcFieldType = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID))                
				 else dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) end as vcValue,                    
				 convert(bit,1) bitCustomField,'' vcPropertyName,  
				 convert(bit,1) as bitCanBeUpdated,1 Tabletype,numListID,case when vcFieldType = 'SelectBox' THEN dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
				Case when vcFieldType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
				Case when vcFieldType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
				ISNULL(bitIsRequired,0) bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
				,'' AS vcDbColumnName,vcToolTip,0 intFieldMaxLength
			FROM 
				View_DynamicCustomColumnsMasterConfig
			WHERE 
				View_DynamicCustomColumnsMasterConfig.numFormId=@numFormID AND 
				View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
				View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
				View_DynamicCustomColumnsMasterConfig.numRelCntType=@numRelCntType AND 
				View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 0 AND
				ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
			ORDER BY 
				Tabletype,tintrow,intcoulmn  
		END
		ELSE                                        
		BEGIN
			SELECT numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
			,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintColumn as intcoulmn,          
			 '' as vcValue,convert(bit,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
			 0 Tabletype,numListID,0 numListItemID,PopupFunctionName,
			Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
			Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage
			,vcDbColumnName,vcToolTip,intFieldMaxLength
			FROM View_DynamicDefaultColumns DFV
			where numFormId=@numFormID AND numDomainID=@numDomainID AND
			1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
			  WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
			  ELSE 0 END)   
			UNION 
			select fld_id as numFieldId,fld_label as vcfieldName ,vcURL,fld_type,               
			convert(tinyint,0) as tintRow,convert(int,0) as intcoulmn,
			case when fld_type = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(fld_id,@PageId,@numRecordID))                
			else dbo.GetCustFldValue(fld_id,@PageId,@numRecordID) end as vcValue,                    
			convert(bit,1) bitCustomField,'' vcPropertyName,  
			convert(bit,1) as bitCanBeUpdated,1 Tabletype,CFM.numListID,case when fld_type = 'SelectBox' THEN dbo.GetCustFldValue(fld_id,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
			Case when fld_type='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=CFM.numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
			Case when fld_type='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=CFM.numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
			ISNULL(V.bitIsRequired,0) bitIsRequired,V.bitIsEmail,V.bitIsAlphaNumeric,V.bitIsNumeric,V.bitIsLengthValidation,V.bitFieldMessage,V.intMaxLength,V.intMinLength,V.bitFieldMessage,ISNULL(V.vcFieldMessage,'') vcFieldMessage,'' AS vcDbColumnName
			,CFM.vcToolTip,0 intFieldMaxLength
			FROM CFW_Fld_Master CFM left join CFW_Fld_Dtl CFD on Fld_id=numFieldId  
			left join CFw_Grp_Master CFG on subgrp=CFG.Grp_id                                    
			LEFT JOIN CFW_Validation V ON V.numFieldID = CFM.Fld_id
			WHERE CFM.grp_id=@PageId and CFM.numDomainID=@numDomainID   and subgrp=0           
			ORDER BY Tabletype,tintrow,intcoulmn                             
		END
	END   
	      
	IF @PageId= 0            
	BEGIN         
	   SELECT numFieldID,vcFieldName,'' as vcURL,'' as fld_type,vcDBColumnName,tintRow,intcolumn as intcoulmn,          
		'0' as bitCustomField,Ctype,'0' as tabletype,
		0 as bitIsRequired,0 bitIsEmail,0 bitIsAlphaNumeric,0 bitIsNumeric, 0 bitIsLengthValidation,0 bitFieldMessage,0 intMaxLength,0 intMinLength, 0 bitFieldMessage,'' vcFieldMessage
		from PageLayout where Ctype = @charCoType    order by tintrow,intcoulmn           
	END                       
END  
/****** Object:  StoredProcedure [dbo].[USP_GetUserCommission_CommRule]    Script Date: 02/28/2009 13:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva
-- [dbo].[USP_GetUserCommission_CommRule] 1,1,1,-333,'1/1/2009','1/15/2009'
-- exec USP_GetUserCommission_CommRule @numUserId=372,@numUserCntID=82979,@numDomainId=1,@ClientTimeZoneOffset=-330,@dtStartDate='2010-03-01 00:00:00:000',@dtEndDate='2010-04-01 00:00:00:000'
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetUserCommission_CommRule')
DROP PROCEDURE USP_GetUserCommission_CommRule
GO
CREATE PROCEDURE [dbo].[USP_GetUserCommission_CommRule]
(
               @numDomainId          AS NUMERIC(9)  = 0,
               @numUserCntID         AS NUMERIC(9)  = 0,
			   @numComRuleID            AS NUMERIC(9)  = 0,
               @numItemCode numeric(9),
               @bitCommContact AS BIT 
)
AS
  BEGIN

DECLARE @tintComAppliesTo TINYINT 
DECLARE @tintAssignTo TINYINT,@tinComDuration TINYINT 
DECLARE @dFrom DATETIME,@dTo Datetime    

SELECT TOP 1 @tinComDuration=CR.tinComDuration,@tintAssignTo=CR.tintAssignTo, @tintComAppliesTo = ISNULL(CR.tintComAppliesTo,0)
			FROM CommissionRules CR WHERE CR.numDomainID=@numDomainID AND CR.numComRuleID=@numComRuleID
	
						--Get From and To date for Commission Calculation
					IF @tinComDuration=1 --Month
							SELECT @dFrom=DATEADD(m, DATEDIFF(m, 0, GETDATE()), 0),
									@dTo=DATEADD(d, -1, DATEADD(m, DATEDIFF(m, 0, GETDATE()) + 1, 0)) 
	
						ELSE IF @tinComDuration=2 --Quarter
							SELECT @dFrom=DATEADD(q, DATEDIFF(q, 0, GETDATE()), 0),
									@dTo=DATEADD(d, -1, DATEADD(q, DATEDIFF(q, 0, GETDATE()) + 1, 0)) 
									
						ELSE IF @tinComDuration=3 --Year
							SELECT @dFrom=DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0),
									@dTo=DATEADD(d, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) + 1, 0)) 
																
 
 Select oppBiz.numBizDocID,oppBiz.vcBizDocID,Opp.numOppId,oppBiz.numOppBizDocsId,isnull(oppBiz.monAmountPaid,0) monAmountPaid,Opp.bintCreatedDate, Opp.vcPOppName AS Name ,Case when Opp.tintOppStatus=0 then 'Open' Else 'Close' End as OppStatus, isnull(OppBiz.monAmountPaid,0) as DealAmount,
		BC.numComissionID,BC.decCommission,Case when BC.tintComBasedOn = 1 then 'Amount' when BC.tintComBasedOn = 2 then 'Units' end AS BasedOn,
		Case when BC.tintComType = 1 then 'Percentage' when BC.tintComType = 2 then 'Flat' end as CommissionType, 
                        numComissionAmount  as CommissionAmt,I.vcItemName,BDI.numUnitHour,BDI.monTotAmount,ISNULL(OT.monVendorCost, 0)* ISNULL(BDI.[numUnitHour],0) AS VendorCost          
From OpportunityMaster Opp left join OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId
JOIN OpportunityBizDocItems BDI ON oppBiz.numOppBizDocsId=BDI.numOppBizDocID AND BC.numOppBizDocItemID=BDI.numOppBizDocItemID
INNER JOIN OpportunityItems OT ON OT.numOppItemtCode=BDI.numOppItemID 
INNER JOIN Item I ON BDI.numItemCode=I.numItemCode
Where oppBiz.bitAuthoritativeBizDocs = 1 AND oppbiz.monAmountPaid > 0 and Opp.tintOppStatus=1 And Opp.tintOppType=1 And 
1= (CASE @tintAssignTo WHEN 1 THEN CASE @bitCommContact WHEN 0 THEN CASE WHEN Opp.numAssignedTo=@numUserCntID THEN 1 ELSE 0 END 
										                WHEN 1 THEN CASE WHEN Opp.numAssignedTo IN (SELECT numContactId FROM AdditionalContactsInformation AC WHERE AC.numDivisionID = @numUserCntID) THEN 1 ELSE 0 END  END
				       WHEN 2 THEN CASE @bitCommContact WHEN 0 THEN CASE WHEN Opp.numRecOwner=@numUserCntID THEN 1 ELSE 0 END 
														WHEN 1 THEN CASE WHEN Opp.numRecOwner IN (SELECT numContactId FROM AdditionalContactsInformation AC WHERE AC.numDivisionID = @numUserCntID) THEN 1 ELSE 0 END END END)	 				 	 

AND 1=(CASE @bitCommContact WHEN 0 THEN CASE WHEN BC.numUserCntId=@numUserCntID THEN 1 else 0 END 
							WHEN 1 THEN CASE WHEN BC.numUserCntId IN (SELECT numContactId FROM AdditionalContactsInformation AC WHERE AC.numDivisionID = @numUserCntID) THEN 1 ELSE 0 END  END)
									
And Opp.numDomainId=@numDomainId AND BC.numComRuleID=@numComRuleID 
AND 1 = (CASE @tintComAppliesTo WHEN 1 THEN CASE WHEN BDI.numItemCode=@numItemCode THEN 1 ELSE 0 END
								WHEN 2 THEN CASE WHEN BDI.numItemCode IN (SELECT numItemCode FROM Item WHERE numItemClassification=@numItemCode AND numDomainID=@numDomainID) THEN 1 ELSE 0 END
								WHEN 3 THEN 1 ELSE 0 END)	
AND oppBiz.numOppBizDocsId IN (SELECT OBD.numOppBizDocsId FROM OpportunityBizDocsDetails BDD
INNER JOIN dbo.OpportunityBizDocs OBD ON BDD.numBizDocsId = OBD.numOppBizDocsId
INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OBD.numOppId
WHERE tintopptype = 1 AND tintoppstatus = 1 AND OM.numdomainid = @numDomainId 
AND OBD.bitAuthoritativeBizDocs = 1 AND BDD.dtCreationDate BETWEEN @dFrom AND @dTo AND 
1= (CASE @tintAssignTo WHEN 1 THEN CASE @bitCommContact WHEN 0 THEN CASE WHEN OM.numAssignedTo=@numUserCntID THEN 1 ELSE 0 END 
										                WHEN 1 THEN CASE WHEN OM.numAssignedTo IN (SELECT numContactId FROM AdditionalContactsInformation AC WHERE AC.numDivisionID = @numUserCntID) THEN 1 ELSE 0 END  END
				       WHEN 2 THEN CASE @bitCommContact WHEN 0 THEN CASE WHEN OM.numRecOwner=@numUserCntID THEN 1 ELSE 0 END 
														WHEN 1 THEN CASE WHEN OM.numRecOwner IN (SELECT numContactId FROM AdditionalContactsInformation AC WHERE AC.numDivisionID = @numUserCntID) THEN 1 ELSE 0 END END END)	 				 	 
								
GROUP BY OBD.numOppBizDocsId HAVING SUM(BDD.monAmount)>=MAX(OBD.monDealAmount))
ORDER BY  oppBiz.numOppBizDocsId desc
  END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_InboxItems' ) 
    DROP PROCEDURE USP_InboxItems
GO
Create PROCEDURE [dbo].[USP_InboxItems]                                                                                              
@PageSize [int] ,                            
@CurrentPage [int],                            
@srch as varchar(100) = '',                            
--@vcSubjectFromTo as varchar(100) ='',
--@srchAttachmenttype as varchar(100) ='',                                   
@ToEmail as varchar (100)  ,        
@columnName as varchar(50) ,        
@columnSortOrder as varchar(4)  ,  
@numDomainId as numeric(9),  
@numUserCntId as numeric(9)  ,    
@numNodeId as numeric(9),  
--@chrSource as char(1),
@ClientTimeZoneOffset AS INT=0,
@tintUserRightType AS INT,
@EmailStatus AS NUMERIC(9)=0,
@srchFrom as varchar(100) = '', 
@srchTo as varchar(100) = '', 
@srchSubject as varchar(100) = '', 
@srchHasWords as varchar(100) = '', 
@srchHasAttachment as bit = 0, 
@srchIsAdvancedsrch as bit = 0,
@srchInNode as numeric(9)=0,
@FromDate AS DATE = NULL,
@ToDate AS DATE = NULL
as                                                      

--GET ENTERIES FROM PERTICULAR DOMAIN FROM VIEW_Email_Alert_Config
SELECT numDivisionID,numContactId,TotalBalanceDue,OpenSalesOppCount, OpenCaseCount, OpenProjectCount, UnreadEmailCount,OpenActionItemCount INTO #TEMP FROM VIEW_Email_Alert_Config WHERE numDomainId=@numDomainId

/************* START - GET ALERT CONFIGURATION **************/
DECLARE @bitOpenActionItem AS BIT = 1
DECLARE @bitOpencases AS BIT = 1
DECLARE @bitOpenProject AS BIT = 1
DECLARE @bitOpenSalesOpp AS BIT = 1
DECLARE @bitBalancedue AS BIT = 1
DECLARE @bitUnreadEmail AS BIT = 1
DECLARE @bitCampaign AS BIT = 1

SELECT  
	@bitOpenActionItem = ISNULL([bitOpenActionItem],1),
    @bitOpencases = ISNULL([bitOpenCases],1),
    @bitOpenProject = ISNULL([bitOpenProject],1),
    @bitOpenSalesOpp = ISNULL([bitOpenSalesOpp],1),
    @bitBalancedue = ISNULL([bitBalancedue],1),
    @bitUnreadEmail = ISNULL([bitUnreadEmail],1),
	@bitCampaign = ISNULL([bitCampaign],1)
FROM    
	AlertConfig
WHERE   
	AlertConfig.numDomainId = @numDomainId AND 
	AlertConfig.numContactId = @numUserCntId

/************* END - GET ALERT CONFIGURATION **************/
                                                      
 ---DECLARE @CRMType NUMERIC 
DECLARE @tintOrder AS TINYINT                                                      
DECLARE @vcFieldName AS VARCHAR(50)                                                      
DECLARE @vcListItemType AS VARCHAR(1)                                                 
DECLARE @vcAssociatedControlType VARCHAR(20)                                                      
DECLARE @numListID AS NUMERIC(9)                                                      
DECLARE @vcDbColumnName VARCHAR(20)                          
DECLARE @WhereCondition VARCHAR(2000)                           
DECLARE @vcLookBackTableName VARCHAR(2000)                    
DECLARE @bitCustom AS BIT
DECLARE @bitAllowEdit AS CHAR(1)                 
DECLARE @numFieldId AS NUMERIC  
DECLARE @bitAllowSorting AS CHAR(1)              
DECLARE @vcColumnName AS VARCHAR(500)  

DECLARE @strSql AS VARCHAR(5000)
DECLARE @column AS VARCHAR(50)                             
DECLARE @join AS VARCHAR(400) 
DECLARE @Nocolumns AS TINYINT               
DECLARE @lastRec AS INTEGER 
DECLARE @firstRec AS INTEGER                   
SET @join = ''           
SET @strSql = ''
--DECLARE @ClientTimeZoneOffset AS INT 
--DECLARE @numDomainId AS NUMERIC(18, 0)
SET @tintOrder = 0  
SET @WhereCondition = ''

SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                                              
SET @lastRec = ( @CurrentPage * @PageSize + 1 )   
SET @column = @columnName                
--DECLARE @join AS VARCHAR(400)                    
SET @join = ''             
IF @columnName LIKE 'Cust%' 
    BEGIN                  
        SET @join = ' left Join CFW_FLD_Values_Email CFW on CFW.RecId=DM.numEmailHstrID and CFW.fld_id= '
            + REPLACE(@columnName, 'Cust', '') + ' '                                                           
        SET @column = 'CFW.Fld_Value'
    END                                           
ELSE 
    IF @columnName LIKE 'DCust%' 
        BEGIN                  
            SET @join = ' left Join CFW_FLD_Values_Email CFW on CFW.RecId=DM.numEmailHstrID  and CFW.fld_id= '
                + REPLACE(@columnName, 'DCust', '')                                                                                                     
            SET @join = @join
                + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '                  
            SET @column = 'LstCF.vcData'                    
                  
        END            
    ELSE 
        BEGIN            
            DECLARE @lookbckTable AS VARCHAR(50)                
            SET @lookbckTable = ''                                                         
            SELECT  @lookbckTable = vcLookBackTableName
            FROM    View_DynamicDefaultColumns
            WHERE   numFormId = 44
                    AND vcDbColumnName = @columnName  and numDomainID=@numDOmainID               
            IF @lookbckTable <> '' 
                BEGIN                
                    IF @lookbckTable = 'EmailHistory' 
                        SET @column = 'ADC.' + @column                
                END                                                              
        END              
--DECLARE @strSql AS VARCHAR(5000)   
--SET @numNodeID = 0  
--SET @srchBody = ''
--SET @vcSubjectFromTo =''
--PRINT 'numnodeid' + CONVERT(VARCHAR, @numNodeId)                                 
SET @strSql = '
with FilterRows as (SELECT TOP('+ CONVERT(varchar,@CurrentPage) + ' * '+ CONVERT(varchar,@PageSize) + ') '                
SET @strSql = @strSql + 'ROW_NUMBER() OVER(ORDER BY EH.bintCreatedOn DESC ) AS RowNumber,COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount,EH.numEmailHstrID,isnull(EM.numContactId,0) as numContactId'
SET @strSql = @strSql + ' FROM [EmailHistory] EH JOIN EmailMaster EM on EM.numEMailId=(CASE numNodeID WHEN 4 THEN SUBSTRING(EH.vcTo, 1, CHARINDEX(''$^$'', EH.vcTo, 1) - 1) ELSE EH.numEmailId END)'
SET @strSql = @strSql + ' 
WHERE EH.numDomainID ='+ CONVERT(VARCHAR,@numDomainId) +' And [numUserCntId] = '+ CONVERT(VARCHAR,@numUserCntId)+' '
--SET @strSql = @strSql + 'Where [numDomainID] ='+ CONVERT(VARCHAR,@numDomainId) +'  And [numUserCntId] = '+ CONVERT(VARCHAR,@numUserCntId)+' AND numNodeID ='+ CONVERT(VARCHAR,@numNodeId) +''
SET @strSql = @strSql + '
And chrSource IN(''B'',''I'') '

--Simple Search for All Node
IF @srchIsAdvancedsrch=0 
BEGIN
if len(@srch)>0
	BEGIN
		SET @strSql = @strSql + '
			AND (EH.vcSubject LIKE ''%'' + '''+ @srch +''' + ''%'' or EH.vcBodyText LIKE ''%'' + '''+ @srch + ''' + ''%''
			or EH.vcFrom LIKE ''%'' + '''+ @srch + ''' + ''%'' or EH.vcTo LIKE ''%'' + '''+ @srch + ''' + ''%'')'
	END
ELSE
	BEGIN --Particular Node
		SET @strSql = @strSql + ' AND [numNodeID] = '+ CONVERT(VARCHAR,@numNodeID) +' '
	END
END
--Advanced Search for All Node or selected Node
Else IF @srchIsAdvancedsrch=1
BEGIN
declare @strCondition as varchar(2000);set @strCondition=''

if len(@srchSubject)>0
	SET @strCondition='EH.vcSubject LIKE ''%'' + '''+ @srchSubject +''' + ''%'''

if len(@srchFrom)>0
	begin
		if len(@strCondition)>0
			SET @strCondition=@strCondition + ' and '

		SET @strCondition = @strCondition + ' EH.vcFrom LIKE ''%'' + '''+ @srchFrom + ''' + ''%'''
	end

if len(@srchTo)>0
	begin
		if len(@strCondition)>0
			SET @strCondition=@strCondition + ' and '

		SET @strCondition = @strCondition + ' EH.vcTo LIKE ''%'' + '''+ @srchTo + ''' + ''%'''
	end

if len(@srchHasWords)>0
	begin
		if len(@strCondition)>0
			SET @strCondition=@strCondition + ' and '

		SET @strCondition = @strCondition + ' EH.vcBodyText LIKE ''%'' + '''+ @srchHasWords + ''' + ''%'''
	end

IF @srchHasAttachment=1
BEGIN
	if len(@strCondition)>0
			SET @strCondition=@strCondition + ' and '
	
	SET @strCondition = @strCondition + ' EH.bitHasAttachments =  '''+ CONVERT(VARCHAR,@srchHasAttachment) + ''''
END

IF (LEN(@FromDate) > 0 AND LEN(@ToDate) > 0 AND ISDATE(CAST(@FromDate AS VARCHAR(100))) = 1 AND ISDATE(CAST(@ToDate AS VARCHAR(100))) = 1)
BEGIN
	IF LEN(@strCondition)>0
		SET @strCondition=@strCondition + ' AND '

	SET @strCondition = @strCondition + ' EH.dtReceivedOn >= '''+ CONVERT(VARCHAR,@FromDate) + ''' AND EH.dtReceivedOn <= ''' + CONVERT(VARCHAR,@ToDate) + ''''
END

if len(@strCondition)>0
	BEGIN
		SET @strSql = @strSql +' and (' + @strCondition + ')'
		
		if @numNodeId>-1
			SET @strSql = @strSql + ' AND [numNodeID] = '+ CONVERT(VARCHAR,@numNodeId) +' '
	END
ELSE
	BEGIN --Particular Node
		SET @strSql = @strSql + ' AND [numNodeID] = '+ CONVERT(VARCHAR,@numNodeID) +' '
	END



--SET @strSql = @strSql + '
--AND (EH.vcSubject LIKE ''%'' + '''+ @srchSubject +''' + ''%'' and EH.vcFrom LIKE ''%'' + '''+ @srchFrom + ''' + ''%'' 
--and EH.vcTo LIKE ''%'' + '''+ @srchTo + ''' + ''%'' and EH.vcBodyText LIKE ''%'' + '''+ @srchHasWords + ''' + ''%'''
--
--IF @srchHasAttachment=1
--BEGIN
--SET @strSql = @strSql + ' and EH.bitHasAttachments =  '''+ CONVERT(VARCHAR,@srchHasAttachment) + ''''
--END

END


SET @strSql = @strSql + ' )'
--,FinalResult As
--( SELECT  TOP ('+CONVERT(VARCHAR,@PageSize)+ ') numEmailHstrID'
--SET @strSql = @strSql + ' 
--From FilterRows'
--SET @strSql = @strSql + '
--WHERE RowNumber > (('+ CONVERT(VARCHAR,@CurrentPage) + ' - 1) * '+ CONVERT(VARCHAR,@PageSize) +' ))'
--PRINT @strSql
--EXEC(@strSql)

select @Nocolumns=isnull(sum(TotalRow),0)  from(            
Select count(*) TotalRow from View_DynamicColumns where numFormId=44 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0 
Union 
Select count(*) TotalRow from View_DynamicCustomColumns where numFormId=44 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0) TotalRows

  CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType char(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9))


---Insert number of rows into temp table.
PRINT 'number OF columns :' + CONVERT(VARCHAR, @Nocolumns)
IF  @Nocolumns > 0 
    BEGIN    

INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID 
 FROM View_DynamicColumns 
 where numFormId=44 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
                  
    UNION

     select tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID
 from View_DynamicCustomColumns
 where numFormId=44 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0
 AND ISNULL(bitCustom,0)=1
 
 ORDER BY tintOrder ASC  
   
 END 
ELSE 
    BEGIN

 INSERT INTO #tempForm
select tintOrder,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID 
 FROM View_DynamicDefaultColumns
 where numFormId=44 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 AND numDomainID=@numDomainID
order by tintOrder asc   

    END
      SET @strSql =@strSql + ' Select TotalRowCount, EH.numEmailHstrID ,EH.numListItemId,bitIsRead,CASE WHEN LEN(Cast(EH.vcBodyText AS VARCHAR(1000)))>150 THEN SUBSTRING(EH.vcBodyText,0,150) + ''...'' ELSE
		 EH.vcBodyText END as vcBodyText,EH.numEmailHstrID As [KeyId~numEmailHstrID~0~0~0~0~HiddenField],FR.numContactId AS [ContactId~numContactID~0~0~0~0~HiddenField] ,EH.bitIsRead As [IsRead~bitIsRead~0~0~0~0~Image],isnull(EH.IsReplied,0) As [~IsReplied~0~0~0~0~Image] '
 
Declare @ListRelID as numeric(9) 

   select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from  #tempForm order by tintOrder asc            

WHILE @tintOrder > 0                                                      
    BEGIN                                                      
        IF @bitCustom = 0  
            BEGIN            
                DECLARE @Prefix AS VARCHAR(5)                        
                IF @vcLookBackTableName = 'EmailHistory' 
                    SET @Prefix = 'EH.'                        
                SET @vcColumnName = @vcFieldName + '~' + @vcDbColumnName
                    + '~' + @bitAllowSorting + '~'
                    + CONVERT(VARCHAR(10), @numFieldId) + '~'
                    + @bitAllowEdit + '~' + CONVERT(VARCHAR(1), @bitCustom)
                    + '~' + @vcAssociatedControlType
                PRINT @vcColumnName
                
                IF @vcAssociatedControlType = 'SelectBox' 
                    BEGIN      
                        PRINT @vcListItemType                                                    
                        IF @vcListItemType = 'L' 
                            BEGIN    
                            ---PRINT 'columnName' + @vcColumnName
                                SET @strSql = @strSql + ',L'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.vcData' + ' [' + @vcColumnName + ']'                                                         
                                SET @WhereCondition = @WhereCondition
                                    + ' left Join ListDetails L'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + ' on L' + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.numListItemID=EH.numListItemID'                                                           
                            END 
                           
                    END 
                  ELSE IF (@vcAssociatedControlType = 'TextBox'
                        OR @vcAssociatedControlType = 'TextArea'
                        OR @vcAssociatedControlType = 'Image')  AND @vcDbColumnName<>'AlertPanel' AND @vcDbColumnName<>'RecentCorrespondance'
                        BEGIN
							IF @numNodeId=4 and @vcDbColumnName='vcFrom'  
							BEGIN
								  SET @vcColumnName = 'To' + '~' + @vcDbColumnName
                    + '~' + @bitAllowSorting + '~'
                    + CONVERT(VARCHAR(10), @numFieldId) + '~'
                    + @bitAllowEdit + '~' + CONVERT(VARCHAR(1), @bitCustom)
                    + '~' + @vcAssociatedControlType

								 SET @strSql = @strSql + ',' + @Prefix  + 'vcTo' + ' [' + @vcColumnName + ']'   
							END 
							ELSE
							BEGIN
								SET @strSql = @strSql + ',' + @Prefix  + @vcDbColumnName + ' [' + @vcColumnName + ']'   
							END                        
                             PRINT @Prefix   
                        END 
				Else IF  @vcDbColumnName='AlertPanel'   
							BEGIN 
								SET @strSql = @strSql +  ', dbo.GetAlertDetail(EH.numEmailHstrID, ACI.numDivisionID, ACI.numECampaignID, FR.numContactId,' 
													  + CAST(@bitOpenActionItem AS VARCHAR(10)) + ',' 
													  + CAST(@bitOpencases AS VARCHAR(10)) + ',' 
													  + CAST(@bitOpenProject AS VARCHAR(10)) + ',' 
													  + CAST(@bitOpenSalesOpp AS VARCHAR(10)) + ',' 
													  + CAST(@bitBalancedue AS VARCHAR(10)) + ',' 
													  + CAST(@bitUnreadEmail AS VARCHAR(10)) + ',' 
													  + CAST(@bitCampaign AS VARCHAR(10)) + ',' +
													  'V1.TotalBalanceDue,V1.OpenSalesOppCount,V1.OpenCaseCount,V1.OpenProjectCount,V1.UnreadEmailCount,V2.OpenActionItemCount,V3.CampaignDTLCount) AS  [' + '' + @vcColumnName + '' + ']' 
							 END 
                 ELSE IF  @vcDbColumnName='RecentCorrespondance'
							BEGIN 
					            --SET @strSql = @strSql + ','+ '(SELECT COUNT(*) FROM dbo.Communication WHERE numContactId = FR.numContactId)' + ' AS  [' + '' + @vcColumnName + '' + ']'                                                       
					            SET @strSql = @strSql + ',0 AS  [' + '' + @vcColumnName + '' + ']'
                             END 
--				else if @vcAssociatedControlType='DateField'                                                  
--					begin            
--							set @strSql=@strSql+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
--							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
--							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ' 
--							set @strSql=@strSql+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'
--				    end    
				else if @vcAssociatedControlType='DateField' AND @vcDbColumnName <> 'dtReceivedOn'                                     
					begin           
							set @strSql=@strSql+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ' 
							set @strSql=@strSql+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'
				    end    
				else if @vcAssociatedControlType='DateField' AND @vcDbColumnName = 'dtReceivedOn'                                                 
					begin    
							set @strSql=@strSql+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ' 
							set @strSql=@strSql+'else CONVERT(VARCHAR(20),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+'),100) end  ['+ @vcColumnName+']'
				    end   
			END 
else if @bitCustom = 1                
begin                
            
               SET @vcColumnName= @vcFieldName+'~'+ @vcDbColumnName+'~0~'+ convert(varchar(10),@numFieldId)+'~1~'+ convert(varchar(1),@bitCustom) +'~'+ @vcAssociatedControlType
       
   select @vcFieldName=FLd_label,@vcAssociatedControlType=fld_type,@vcDbColumnName='Cust'+Convert(varchar(10),Fld_Id)                               
    from CFW_Fld_Master                                                
   where    CFW_Fld_Master.Fld_Id = @numFieldId                 
                
                 
    if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'               
   begin                
                   
    set @strSql= @strSql+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=FR.numContactId   '                                                         
   end      
    else if @vcAssociatedControlType = 'CheckBox'                 
   begin              
                 
    set @strSql= @strSql+',case when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=0 then ''No'' when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=1 then ''Yes'' end   ['+ @vcColumnName +']'              
   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '               
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=FR.numContactId   '                                                       
   end              
  else if @vcAssociatedControlType = 'DateField'              
   begin                
                   
    set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=FR.numContactId   '                                                         
   end                
   else if @vcAssociatedControlType = 'SelectBox'                    
   begin                
    set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
    set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
     on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=FR.numContactId   '                                                         
    set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
   end                 
end     
                   --    SET @vcColumnName= @vcFieldName+'~'+ @vcDbColumnName+'~0~'+ convert(varchar(10),@numFieldId)+'~1~'+ convert(varchar(1),@bitCustom) +'~'+ @vcAssociatedControlType   
  select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc            
 
           IF @@rowcount = 0 SET @tintOrder = 0 
		   PRINT @tintOrder 
    END  
                     
SELECT  * FROM    #tempForm

-------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID AND DFCS.numFormID=DFFM.numFormID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=10 AND DFCS.numFormID=10 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
---------------------------- 

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
set @strSql=@strSql+',tCS.vcColorScheme'
END  

DECLARE @fltTotalSize FLOAT
SELECT @fltTotalSize=isnull(SUM(ISNULL(SpaceOccupied,0)),0) FROM dbo.View_InboxCount where numDomainID=@numDomainID and numUserCntID=@numUserCntID;

SET @strSql = @strSql + ', '+ CONVERT(VARCHAR(18),@fltTotalSize) +' AS TotalSize From EmailHistory EH Inner Join FilterRows FR on  FR.numEmailHstrID = EH.numEmailHstrID '
SET @strSql=@strSql + ' LEFT JOIN
	AdditionalContactsInformation ACI
ON 
	ACI.numContactId = FR.numContactId
CROSS APPLY
(
	SELECT
		SUM(TotalBalanceDue) TotalBalanceDue,
		SUM(OpenSalesOppCount) OpenSalesOppCount,
		SUM(OpenCaseCount) OpenCaseCount,
		SUM(OpenProjectCount) OpenProjectCount,
		SUM(UnreadEmailCount) UnreadEmailCount
	FROM
		#TEMP
	WHERE
		#TEMP.numDivisionId = ACI.numDivisionId
) AS V1
CROSS APPLY
(
	SELECT
		SUM(OpenActionItemCount) OpenActionItemCount
	FROM
		#TEMP
	WHERE
		#TEMP.numDivisionId = ACI.numDivisionId AND
		#TEMP.numContactId = FR.numContactId
) AS V2
CROSS APPLY
(
	SELECT 
		COUNT(*) AS CampaignDTLCount
	FROM 
		ConECampaignDTL 
	WHERE 
		numConECampID = (SELECT TOP 1 ISNULL(numConEmailCampID,0)  FROM ConECampaign WHERE numECampaignID = ACI.numECampaignID AND numContactID = ACI.numContactId ORDER BY numConEmailCampID DESC) AND 
		ISNULL(bitSend,0) = 0
) AS V3 '
SET @strSql = @strSql + @WhereCondition

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
	 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
		set @Prefix = 'ADC.'                  
     if @vcCSLookBackTableName = 'DivisionMaster'                  
		set @Prefix = 'DM.'
	 if @vcCSLookBackTableName = 'CompanyInfo'                  
		set @Prefix = 'CI.' 
	 if @vcCSLookBackTableName = 'AddressDetails'                  
		set @Prefix = 'AD.'   

set @strSql=@strSql+' left JOIN AdditionalContactsInformation ADC ON ADC.numcontactId=FR.numContactID                                                                             
 left JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                                            
 left JOIN CompanyInfo CI ON DM.numCompanyID = CI.numCompanyId '
   
	IF @vcCSAssociatedControlType='DateField'
	BEGIN
			set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
			 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
	END
	ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
	BEGIN
		set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
	END
END

SET @strSql = @strSql + ' Where RowNumber >'  + CONVERT(VARCHAR(10), @firstRec) + ' and RowNumber <'
        + CONVERT(VARCHAR(10), @lastRec) 
IF @EmailStatus <> 0 
BEGIN
	SET @strSql = @strSql + ' AND EH.numListItemId = ' + CONVERT(VARCHAR(10), @EmailStatus)
END       
SET @strSql = @strSql + ' order by RowNumber'

PRINT @strSql 
EXEC(@strSql)   
DROP TABLE #tempForm          
DROP TABLE #TEMP
 
 /*
 declare @firstRec as integer                                                      
 declare @lastRec as integer                                                      
 set @firstRec= (@CurrentPage-1) * @PageSize                                                      
 set @lastRec= (@CurrentPage*@PageSize+1)   ;    
 
DECLARE @totalRows  INT
DECLARE @fltTotalSize FLOAT
SELECT @totalRows = RecordCount FROM dbo.View_InboxCount where numDomainID=@numDomainID and numUserCntID=@numUserCntID and numNodeId = @numNodeID ;
SELECT @fltTotalSize=SUM(ISNULL(SpaceOccupied,0)) FROM dbo.View_InboxCount where numDomainID=@numDomainID and numUserCntID=@numUserCntID;



-- Step 1: Get Only rows specific to user and domain 

WITH FilterRows
AS 
(
   SELECT 
   TOP (@CurrentPage * @PageSize)
        ROW_NUMBER() OVER(ORDER BY EH.bintCreatedOn DESC ) AS totrows,
        EH.numEmailHstrID
   FROM [EmailHistory] EH
		--LEFT JOIN EmailHstrAttchDtls A ON EH.numEmailHstrID = A.numEmailHstrID
   WHERE 
	   [numDomainID] = @numDomainID
	AND [numUserCntId] = @numUserCntId
	AND numNodeID = @numNodeId
	AND chrSource IN('B','I')
    AND (EH.vcBodyText LIKE '%' + @srchBody + '%' OR @srchBody = '')
	AND (EH.vcSubject LIKE '%' + @vcSubjectFromTo + '%' OR @vcSubjectFromTo ='')
    --AND (A.[vcAttachmentType] LIKE '%'+ @srchAttachmenttype + '%' OR @srchAttachmenttype='')
   
)
 --select seq1 + totrows1 -1 as TotRows2,* FROM [FilterRows]
,FinalResult
AS 
(
	SELECT  TOP (@PageSize) numEmailHstrID --,seq + totrows -1 as TotRows
	FROM FilterRows
	WHERE totrows > ((@CurrentPage - 1) * @PageSize)
)



-- select * FROM [UserEmail]
select  
		@totalRows TotRows,
		@fltTotalSize AS TotalSize,
		B.[numEmailHstrID],
        B.[numDomainID],
--        B.bintCreatedOn,
--        B.dtReceivedOn,
        B.numEmailHstrID,
        ISNULL(B.numListItemId,-1) AS numListItemId,
        ISNULL(vcSubject, '') AS vcSubject,
        CONVERT(VARCHAR(MAX), vcBody) AS vcBody,
        ISNULL(vcItemId, '') AS ItemId,
        ISNULL(vcChangeKey, '') AS ChangeKey,
        ISNULL(bitIsRead, 'False') AS IsRead,
        ISNULL(vcSize, 0) AS vcSize,
        ISNULL(bitHasAttachments, 'False') AS HasAttachments,
        ISNULL(B.tintType, 1) AS type,
        ISNULL(B.chrSource, 'B') AS chrSource,
        ISNULL(vcCategory, 'white') AS vcCategory,
        CASE B.[numNodeId] WHEN 4 THEN REPLACE(REPLACE(ISNULL(B.[vcTo],''),'<','') , '>','') ELSE B.vcFrom END FromName,
--        dbo.GetEmaillName(B.numEmailHstrID, 4) AS FromName,
--        dbo.GetEmaillName(B.numEmailHstrID, 1) AS ToName,
		CASE B.[numNodeId] WHEN 4 THEN 
			dbo.FormatedDateTimeFromDate(DATEADD(minute, -@ClientTimeZoneOffset,B.bintCreatedOn),B.numDomainId) 
		ELSE 
			dbo.FormatedDateTimeFromDate(B.dtReceivedOn,B.numDomainId)
		END dtReceivedOn,
        ISNULL(CONVERT(VARCHAR(5),B.[numNoofTimes]),'') NoOfTimeOpened
FROM [FinalResult] A INNER JOIN emailHistory B ON B.numEmailHstrID = A.numEmailHstrID;



--WITH UserEmail
--AS
--(
--			   SELECT
--						ROW_NUMBER() OVER ( ORDER BY X.bintCreatedOn DESC ) AS RowNumber,
--                        X.[numEmailHstrID]
--               FROM		[EmailHistory] X
--               WHERE    [numDomainID] = @numDomainID
--						AND [numUserCntId] = @numUserCntId
--						AND numNodeID = @numNodeId
--						AND X.chrSource IN('B','I')
--						
--)
--	SELECT 
--		COUNT(*) 
--   FROM UserEmail UM INNER JOIN [EmailHistory] EH  ON UM.numEmailHstrID =EH.numEmailHstrID
--		LEFT JOIN EmailHstrAttchDtls A ON EH.numEmailHstrID = A.numEmailHstrID
--   WHERE 
--		 (EH.vcBodyText LIKE '%' + @srchBody + '%' OR @srchBody = '')
--	AND (EH.vcSubject LIKE '%' + @vcSubjectFromTo + '%' OR @vcSubjectFromTo ='')
--    AND (A.[vcAttachmentType] LIKE '%'+ @srchAttachmenttype + '%' OR @srchAttachmenttype='')



--SELECT COUNT(*)
--FROM      View_Inbox I INNER JOIN [EmailHistory] EH ON EH.numEmailHstrID = I.numEmailHstrID
--			LEFT JOIN EmailHstrAttchDtls A ON EH.numEmailHstrID = A.numEmailHstrID
-- WHERE     I.numDomainId = @numDomainId
--                                AND I.numUserCntId = @numUserCntId
--                                AND I.numNodeId = @numNodeId
--                                AND I.chrSource IN('B','I')
--                                AND (EH.vcBodyText LIKE '%' + @srchBody + '%' OR @srchBody = '')
--								AND (I.vcSubjectFromTo LIKE '%' + @vcSubjectFromTo + '%' OR @vcSubjectFromTo ='')
--                                AND (A.[vcAttachmentType] LIKE '%'+ @srchAttachmenttype + '%' OR @srchAttachmenttype='')

RETURN ;
*/                                           
/* SELECT Y.[ID],
        Y.[numEmailHstrID],
        emailHistory.[numDomainID],
        emailHistory.bintCreatedOn,
        emailHistory.dtReceivedOn,
        emailHistory.numEmailHstrID,
        ISNULL(vcSubject, '') AS vcSubject,
        CONVERT(VARCHAR(MAX), vcBody) AS vcBody,
        ISNULL(vcItemId, '') AS ItemId,
        ISNULL(vcChangeKey, '') AS ChangeKey,
        ISNULL(bitIsRead, 'False') AS IsRead,
        ISNULL(vcSize, 0) AS vcSize,
        ISNULL(bitHasAttachments, 'False') AS HasAttachments,
        ISNULL(emailHistory.tintType, 1) AS type,
        ISNULL(emailHistory.chrSource, 'B') AS chrSource,
        ISNULL(vcCategory, 'white') AS vcCategory,
        dbo.GetEmaillName(Y.numEmailHstrID, 4) AS FromName,
        dbo.GetEmaillName(Y.numEmailHstrID, 1) AS ToName,
        dbo.FormatedDateFromDate(DATEADD(minute, -@ClientTimeZoneOffset,
                                         emailHistory.bintCreatedOn),
                                 emailHistory.numDomainId) AS bintCreatedOn,
        dbo.FormatedDateFromDate(emailHistory.dtReceivedOn,
                                 emailHistory.numDomainId) AS dtReceivedOn,
        ISNULL(CONVERT(VARCHAR(5),EmailHistory.[numNoofTimes]),'') NoOfTimeOpened
 FROM   ( SELECT   
				ROW_NUMBER() OVER ( ORDER BY X.bintCreatedOn DESC ) AS ID,
				X.[numEmailHstrID]
          FROM      ( SELECT    DISTINCT
                                EH.numEmailHstrID,
                                EH.[bintCreatedOn]
                      FROM      --View_Inbox I INNER JOIN 
                      [EmailHistory] EH --ON EH.numEmailHstrID = I.numEmailHstrID
								LEFT JOIN EmailHstrAttchDtls A ON EH.numEmailHstrID = A.numEmailHstrID
                      WHERE     EH.numDomainId = @numDomainId
                                AND EH.numUserCntId = @numUserCntId
                                AND EH.numNodeId = @numNodeId
                                AND EH.chrSource IN('B','I')
                                AND (EH.vcBodyText LIKE '%' + @srchBody + '%' OR @srchBody = '')
								AND (EH.vcSubject LIKE '%' + @vcSubjectFromTo + '%' OR @vcSubjectFromTo ='')
                                AND (A.[vcAttachmentType] LIKE '%'+ @srchAttachmenttype + '%' OR @srchAttachmenttype='')
                    ) X
          
        ) Y
        INNER JOIN emailHistory ON emailHistory.numEmailHstrID = Y.numEmailHstrID
--        LEFT JOIN EmailHstrAttchDtls ON emailHistory.numEmailHstrID = EmailHstrAttchDtls.numEmailHstrID
        AND Y.ID > @firstRec AND Y.ID < @lastRec
--UNION
--   SELECT   0 AS ID,
--			COUNT(*),
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
----            NULL,
----            NULL,
----            NULL,
--            1,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            ''
--FROM      View_Inbox I INNER JOIN [EmailHistory] EH ON EH.numEmailHstrID = I.numEmailHstrID
--			LEFT JOIN EmailHstrAttchDtls A ON EH.numEmailHstrID = A.numEmailHstrID
-- WHERE     I.numDomainId = @numDomainId
--                                AND I.numUserCntId = @numUserCntId
--                                AND I.numNodeId = @numNodeId
--                                AND I.chrSource IN('B','I')
--                                AND (EH.vcBodyText LIKE '%' + @srchBody + '%' OR @srchBody = '')
--								AND (I.vcSubjectFromTo LIKE '%' + @vcSubjectFromTo + '%' OR @vcSubjectFromTo ='')
--                                AND (A.[vcAttachmentType] LIKE '%'+ @srchAttachmenttype + '%' OR @srchAttachmenttype='')
--   ORDER BY ID

RETURN 
*/
/*
declare @strSql as varchar(8000)                    
   if @columnName = 'FromName'               
 begin        
 set @columnName =  'dbo.GetEmaillName(emailHistory.numEmailHstrID,4)'        
 end        
 if @columnName = 'FromEmail'        
 begin        
 set @columnName = 'dbo.GetEmaillAdd(emailHistory.numEmailHstrID,4)'        
 end        
  if @columnName = 'ToName'               
 begin        
 set @columnName =  'dbo.GetEmaillName(emailHistory.numEmailHstrID,1)'        
 end        
 if @columnName = 'ToEmail'        
 begin        
 set @columnName = 'dbo.GetEmaillAdd(emailHistory.numEmailHstrID,1)'        
 end 
                  
set @strSql='With tblSubscriber AS (SELECT  distinct(emailHistory.numEmailHstrID) ,            
ROW_NUMBER() OVER (ORDER BY '+@columnName+' '+@columnSortOrder+') AS  RowNumber             
from emailHistory                                
--left join EmailHstrAttchDtls on emailHistory.numEmailHstrID=EmailHstrAttchDtls.numEmailHstrID                     
--join EmailHStrToBCCAndCC on emailHistory.numEmailHstrID=EmailHStrToBCCAndCC.numEmailHstrID                     
--join EmailMaster on        EmailMaster.numEmailId=EmailHStrToBCCAndCC.numEmailId                     
where numDomainId='+convert(varchar(15),@numDomainId)+' and numUserCntId='+convert(varchar(15),@numuserCntId)+  
' and numNodeId='+convert(varchar(15),@numNodeId)  
    
 if @chrSource<> '0'   set @strSql=@strSql +' and (chrSource  = ''B'' or chrSource like ''%'+@chrSource+'%'')'   
           
 if @srchBody <> '' set @strSql=@strSql +' and (vcSubject  like ''%'+@srchBody+'%'' or  
  vcBodyText like ''%'+@srchBody+'%''            
  or emailHistory.numEmailHstrID in (select numEmailHstrId from EmailHStrToBCCAndCC where             
 (numemailid in (select numEmailid from emailmaster where vcemailid like ''%'+@srchBody+'%'')) or   
  vcName like ''%'+@srchBody+'%'') )'                
--if @ToEmail <> '' set @strSql=@strSql +'               
--      and (EmailMaster.vcEmailId like '''+@ToEmail+''' and EmailHStrToBCCAndCC.tintType<>4 )  '            
if @srchAttachmenttype <>'' set @strSql=@strSql +'              
  and  emailHistory.numEmailHstrID in  (select numEmailHstrId from EmailHstrAttchDtls                
  where EmailHstrAttchDtls.vcAttachmentType like ''%'+@srchAttachmenttype+'%'')'            */
            
            
/*Please Do not use GetEmaillAdd function as it recursively fetched email address for single mail which create too bad performance*/                 

/*set @strSql=@strSql +')                     
 select RowNumber,            
--dbo.GetEmaillAdd(emailHistory.numEmailHstrID,4) as FromEmail,                  
dbo.GetEmaillName(emailHistory.numEmailHstrID,4) as FromName,                             
--isnull(vcFromEmail,'''') as FromEmail,                                      
--dbo.GetEmaillAdd(emailHistory.numEmailHstrID,1) as ToEmail,                                 
  
--dbo.GetEmaillAdd(emailHistory.numEmailHstrID,2) as CCEmail,                                
--dbo.GetEmaillAdd(emailHistory.numEmailHstrID,3) as BCCEmail,                                     
emailHistory.numEmailHstrID,                            
isnull(vcSubject,'''') as vcSubject,                                      
convert(varchar(max),vcBody) as vcBody,                              
dbo.FormatedDateFromDate( DATEADD(minute,'+ CONVERT(VARCHAR(15),-@ClientTimeZoneOffset)+',bintCreatedOn),numDomainId) as bintCreatedOn,                              
isnull(vcItemId,'''') as ItemId,                              
isnull(vcChangeKey,'''') as ChangeKey,                              
isnull(bitIsRead,''False'') as IsRead,                  
isnull(vcSize,0) as vcSize,                              
isnull(bitHasAttachments,''False'') as HasAttachments,                             
  isnull(EmailHstrAttchDtls.vcFileName,'''') as AttachmentPath,                            
isnull(EmailHstrAttchDtls.vcAttachmentType,'''') as AttachmentType,                      
isnull(EmailHstrAttchDtls.vcAttachmentItemId,'''') as AttachmentItemId,                             
dbo.FormatedDateFromDate(dtReceivedOn,numDomainId) as dtReceivedOn,    
dbo.GetEmaillName(emailHistory.numEmailHstrID,1) as ToName,                          
isnull(emailHistory.tintType,1) as type,                              
isnull(chrSource,''B'') as chrSource,                          
isnull(vcCategory,''white'') as vcCategory                   
 from tblSubscriber T                  
 join emailHistory                   
 on emailHistory.numEmailHstrID=T.numEmailHstrID             
left join EmailHstrAttchDtls on emailHistory.numEmailHstrID=EmailHstrAttchDtls.numEmailHstrID             
where numDomainId='+convert(varchar(15),@numDomainId)+' and numUserCntId='+convert(varchar(15),@numuserCntId)+  
' and numNodeId='+convert(varchar(15),@numNodeId)+' and                
  RowNumber > '+convert(varchar(10),@firstRec)+' and RowNumber < '+convert(varchar(10),@lastRec)+'              
union                 
 select 0 as RowNumber,null,count(*),null,null,            
null,null,null,null,null,null,null,null,null,NULL,           
null,1,null,null from tblSubscriber  order by RowNumber'            
--print    @strSql             
exec (@strSql)
GO
*/               

/****** Object:  StoredProcedure [dbo].[usp_InsertCommunication]    Script Date: 07/26/2008 16:19:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified By Anoop jayaraj                 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertcommunication')
DROP PROCEDURE usp_insertcommunication
GO
CREATE PROCEDURE [dbo].[usp_InsertCommunication]                                                
@numCommId numeric=0,                            
@bitTask numeric,                                        
@numContactId numeric,                            
@numDivisionId numeric,                            
@txtDetails text,                            
@numOppId numeric=0,                            
@numAssigned numeric=0,                                                
@numUserCntID numeric,                                                                                                                                               
@numDomainId numeric,                                                
@bitClosed tinyint,                                                
@vcCalendarName varchar(100)='',                                                                                                     
@dtStartTime datetime,                 
@dtEndtime datetime,                                                                             
@numActivity as numeric(9),                                              
@numStatus as numeric(9),                                              
@intSnoozeMins as int,                                              
@tintSnoozeStatus as tinyint,                                            
@intRemainderMins as int,                                            
@tintRemStaus as tinyint,                                  
@ClientTimeZoneOffset Int,                  
@bitOutLook tinyint,            
@bitSendEmailTemplate bit,            
@bitAlert bit,            
@numEmailTemplate numeric(9)=0,            
@tintHours tinyint=0,          
@CaseID  numeric=0 ,        
@CaseTimeId numeric =0,    
@CaseExpId numeric = 0  ,                     
@ActivityId numeric = 0 ,
@bitFollowUpAnyTime BIT,
@strAttendee TEXT,
@numLinkedOrganization NUMERIC(18,0) = 0,
@numLinkedContact NUMERIC(18,0) = 0
--                                                
AS                       
--	IF @numDivisionId = 0                         
--	BEGIN
--		RAISERROR('COMPANY_NOT_FOUND',16,1)
--		RETURN
--	END
--  
--	IF @numContactId = 0 
--	BEGIN
--		RAISERROR('CONTACT_NOT_FOUND',16,1)
--		RETURN
--	END           
    
	declare @CheckRecord as bit   
	set @CheckRecord = 1                

  if not exists (select * from communication  WHERE numcommid=@numCommId   )                                                
	set @CheckRecord = 0                                                

  IF @numCommId = 0    or @CheckRecord = 0                                            

  BEGIN
  
	IF ISNULL(@numDivisionId,0) = 0                         
		BEGIN
			IF ISNULL(@numContactId,0) > 0
				BEGIN
					SELECT @numDivisionId = numDivisionId FROM AdditionalContactsInformation WHERE numContactId = @numContactId
					
					IF ISNULL(@numDivisionId,0) = 0 
					BEGIN
						RAISERROR('COMPANY_NOT_FOUND_FOR_CONTACT' ,16,1)
						RETURN
					END                   
				END
			ELSE
				BEGIN
					RAISERROR('COMPANY_NOT_FOUND',16,1)
					RETURN	
				END
		END
  
	IF ISNULL(@numContactId,0) = 0 
		BEGIN
			RAISERROR('CONTACT_NOT_FOUND',16,1)
			RETURN
		END   
		                   
   IF @numAssigned=0  SET @numAssigned=@numUserCntID                                                
                                             
   Insert into communication                  
   (                
   bitTask,                
   numContactId,                
   numDivisionId,                
   textDetails,                
   intSnoozeMins,                
   intRemainderMins,                
   numStatus,                
   numActivity,                
   numAssign,                
   tintSnoozeStatus,                
   tintRemStatus,                
   numOppId,                
   numCreatedby,                
   dtCreatedDate,                
   numModifiedBy,                
   dtModifiedDate,                
   numDomainID,                
   bitClosedFlag,                
   vcCalendarName,                
   bitOutlook,                
   dtStartTime,                
   dtEndTime,              
   numAssignedBy,            
   bitSendEmailTemp,            
   numEmailTemplate,            
   tintHours,            
   bitAlert,          
CaseId,        
CaseTimeId,                                                                                 
CaseExpId,  
numActivityId ,bitFollowUpAnyTime            
   )                 
 values                
   (                                              
   @bitTask,                
   @numcontactId,                                              
   @numDivisionId,                
   ISNULL(@txtdetails,''),                
   @intSnoozeMins,                
   @intRemainderMins,                
   @numStatus,                
   @numActivity,                                            
   @numAssigned,                
   @tintSnoozeStatus,                
   @tintRemStaus,                                             
   @numoppid,                
   @numUserCntID,                
   getutcdate(),                
   @numUserCntID,                                              
   getutcdate(),                
   @numDomainId,                
   @bitClosed,                
   @vcCalendarName,                
   @bitOutLook,                                              
   DateAdd(minute, @ClientTimeZoneOffset, @dtStartTime),                
   DateAdd(minute, @ClientTimeZoneOffset,@dtEndtime),              
   @numUserCntID,            
   @bitSendEmailTemplate,            
   @numEmailTemplate,            
   @tintHours,            
   @bitAlert,          
@CaseID,        
@CaseTimeId,                                                                                 
@CaseExpId,  
@ActivityId ,@bitFollowUpAnyTime 
 )                                                             
   set @numCommId= @@IDENTITY 
   
	IF ISNULL(@numLinkedOrganization,0) > 0
	BEGIN
		INSERT INTO CommunicationLinkedOrganization VALUES (@numCommId,@numLinkedOrganization,@numLinkedContact)
	END
                                                    
  END                                                
 ELSE                    
  BEGIN                
    ---Updating if Action Item is assigned to someone                      
  declare @tempAssignedTo as numeric(9)                    
  set @tempAssignedTo=null                     
  select @tempAssignedTo=isnull(numAssign,0) from communication where numcommid=@numCommId                     
print @tempAssignedTo                    
  if (@tempAssignedTo<>@numAssigned and  @numAssigned<>'0')                    
  begin                      
    update communication set numAssignedBy=@numUserCntID where numcommid=@numCommId              
  end                     
  else if  (@numAssigned =0)                    
  begin                    
   update communication set numAssignedBy=0 where numcommid=@numCommId                  
  end                
              
              
                                              
    if @vcCalendarName=''                                                 
    begin                                                
      UPDATE communication SET                 
    bittask=@bittask,                                                                              
      textdetails=@txtdetails,                                               
      intSnoozeMins=@intSnoozeMins,                                               
      numStatus=@numStatus,                                              
      numActivity=@numActivity,                                             
      tintSnoozeStatus=@tintSnoozeStatus,                                            
      intRemainderMins= @intRemainderMins,                                            
      tintRemStatus= @tintRemStaus,                                              
      numassign=@numAssigned,                                                
      nummodifiedby=@numUserCntID,                                                
      dtModifiedDate=getutcdate(),                                                
      bitClosedFlag=@bitClosed,                                                        
      bitOutLook=@bitOutLook,                
    dtStartTime=DateAdd(minute, @ClientTimeZoneOffset, @dtStartTime),                
    dtEndTime=DateAdd(minute, @ClientTimeZoneOffset,@dtEndtime),            
    bitSendEmailTemp=@bitSendEmailTemplate,            
    numEmailTemplate=@numEmailTemplate,            
    tintHours=@tintHours,            
    bitAlert=@bitAlert,       
CaseID=@CaseID,        
CaseTimeId=@CaseTimeId,                                                                                 
CaseExpId=@CaseExpId  ,  
numActivityId=@ActivityId ,bitFollowUpAnyTime=@bitFollowUpAnyTime,
dtEventClosedDate=Case When isnull(@bitClosed,0)=1 then getutcdate() else null end                                 
      WHERE numcommid=@numCommId                                                
    END                                                
    ELSE                                                
    BEGIN                                                
       UPDATE communication SET                 
    bittask=@bittask,                                                                              
      textdetails=@txtdetails,                                               
      intSnoozeMins=@intSnoozeMins,                                
      numStatus=@numStatus,                                              
      numActivity=@numActivity,                                             
      tintSnoozeStatus=@tintSnoozeStatus,                                            
      intRemainderMins= @intRemainderMins,                                            
      tintRemStatus= @tintRemStaus,        
      numassign=@numAssigned,                                                
      nummodifiedby=@numUserCntID,                                                
      dtModifiedDate=getutcdate(),                                                
      bitClosedFlag=@bitClosed,                                                        
      bitOutLook=@bitOutLook,                
    dtStartTime=DateAdd(minute, @ClientTimeZoneOffset, @dtStartTime),                
    dtEndTime=DateAdd(minute, @ClientTimeZoneOffset,@dtEndtime),                                                 
     vcCalendarName=@vcCalendarName,            
      bitSendEmailTemp=@bitSendEmailTemplate,            
    numEmailTemplate=@numEmailTemplate,            
    tintHours=@tintHours,            
    bitAlert=@bitAlert ,      
CaseID=@CaseID,        
CaseTimeId=@CaseTimeId,                                                                                 
CaseExpId=@CaseExpId  ,  
numActivityId=@ActivityId  ,bitFollowUpAnyTime=@bitFollowUpAnyTime,
dtEventClosedDate=Case When isnull(@bitClosed,0)=1 then getutcdate() else null end 
   WHERE numcommid=@numCommId                                                
   END  
   
	IF ISNULL(@numLinkedOrganization,0) > 0
	BEGIN
		IF EXISTS (SELECT numCLOID FROM CommunicationLinkedOrganization WHERE numCommID=@numCommId)
		BEGIN
			UPDATE CommunicationLinkedOrganization SET numDivisionID=@numLinkedOrganization,numContactID=@numLinkedContact WHERE numCommID=@numCommId
		END
		ELSE
		BEGIN
			INSERT INTO CommunicationLinkedOrganization VALUES (@numCommId,@numLinkedOrganization,@numLinkedContact)
		END
	END
	ELSE
	BEGIN
		DELETE FROM CommunicationLinkedOrganization WHERE numCommID=@numCommId
	END
                                                 
END 

 DECLARE @hDocItem int                                                                                                                            
  IF convert(varchar(10),@strAttendee) <>''                                                                          
  BEGIN                      
      EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strAttendee  
      
      SELECT numContactID,ActivityID INTO #tempAttendees FROM OPENXML (@hDocItem,'/NewDataSet/AttendeeTable',2)                                                                          
			   WITH  (numContactID NUMERIC(9),ActivityID NUMERIC(9))
      
      delete from CommunicationAttendees where numCommId=@numCommId and numContactId not in                       
			   (SELECT numContactID from #tempAttendees)   
	
		 SELECT * FROM #tempAttendees
		 
	  INSERT INTO CommunicationAttendees (numCommId,numContactId,ActivityID)  
		  SELECT @numCommId,numContactId,ActivityID FROM #tempAttendees WHERE numContactId NOT IN(SELECT numContactId
		  FROM CommunicationAttendees WHERE numCommId=@numCommId)  
		  
	DROP TABLE #tempAttendees	   
  END
   
select @numCommId
GO
/*
declare @p7 int
set @p7=0
exec USP_ItemList1 @ItemClassification=0,@KeyWord='',@IsKit=0,@SortChar='0',@CurrentPage=1,@PageSize=10,@TotRecs=@p7 
output,@columnName='vcItemName',@columnSortOrder='Asc',@numDomainID=1,@ItemType=' ',@bitSerialized=0,@numItemGroup=0,@bitAssembly=0,@numUserCntID=1,@Where='',@IsArchive=0
select @p7
*/

GO
/****** Object:  StoredProcedure [dbo].[USP_ItemList1]    Script Date: 05/07/2009 18:14:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop Jayaraj                                        
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_itemlist1' ) 
    DROP PROCEDURE usp_itemlist1
GO
CREATE PROCEDURE [dbo].[USP_ItemList1]
    @ItemClassification AS NUMERIC(9) = 0,
    @KeyWord AS VARCHAR(1000) = '',
    @IsKit AS TINYINT,
    @SortChar CHAR(1) = '0',
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT, 
    @columnName AS VARCHAR(50),
    @columnSortOrder AS VARCHAR(10),
    @numDomainID AS NUMERIC(9) = 0,
    @ItemType AS CHAR(1),
    @bitSerialized AS BIT,
    @numItemGroup AS NUMERIC(9),
    @bitAssembly AS BIT,
    @numUserCntID AS NUMERIC(9),
    @Where	AS VARCHAR(MAX),
    @IsArchive AS BIT = 0,
    @byteMode AS TINYINT,
	@bitAsset as BIT=0,
	@bitRental as BIT=0
AS 
	
	 DECLARE @Nocolumns AS TINYINT               
    SET @Nocolumns = 0                
 
    SELECT  @Nocolumns = ISNULL(SUM(TotalRow), 0)
    FROM    ( SELECT    COUNT(*) TotalRow
              FROM      View_DynamicColumns
              WHERE     numFormId = 21
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND tintPageType = 1 AND ISNULL(numRelCntType,0)=0
              UNION
              SELECT    COUNT(*) TotalRow
              FROM      View_DynamicCustomColumns
              WHERE     numFormId = 21
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND tintPageType = 1 AND ISNULL(numRelCntType,0)=0
            ) TotalRows
               
 
 if @Nocolumns=0
BEGIN
INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth,numViewID)
select 21,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth,0
 FROM    View_DynamicDefaultColumns
                    WHERE   numFormId = 21
                            AND bitDefault = 1
                            AND ISNULL(bitSettingField, 0) = 1
                            AND numDomainID = @numDomainID
                    ORDER BY tintOrder ASC 
END

    CREATE TABLE #tempForm
        (
          tintOrder TINYINT,
          vcDbColumnName NVARCHAR(50),
          vcFieldName NVARCHAR(50),
          vcAssociatedControlType NVARCHAR(50),
          vcListItemType CHAR(3),
          numListID NUMERIC(9),
          vcLookBackTableName VARCHAR(50),
          bitCustomField BIT,
          numFieldId NUMERIC,
          bitAllowSorting BIT,
          bitAllowEdit BIT,
          bitIsRequired BIT,
          bitIsEmail BIT,
          bitIsAlphaNumeric BIT,
          bitIsNumeric BIT,
          bitIsLengthValidation BIT,
          intMaxLength INT,
          intMinLength INT,
          bitFieldMessage BIT,
          vcFieldMessage VARCHAR(500),
          ListRelID NUMERIC(9),
		  intColumnWidth INT
        )


          

            INSERT  INTO #tempForm
                    SELECT  tintRow + 1 AS tintOrder,
                            vcDbColumnName,
                            ISNULL(vcCultureFieldName, vcFieldName),
                            vcAssociatedControlType,
                            vcListItemType,
                            numListID,
                            vcLookBackTableName,
                            bitCustom,
                            numFieldId,
                            bitAllowSorting,
                            bitAllowEdit,
                            bitIsRequired,
                            bitIsEmail,
                            bitIsAlphaNumeric,
                            bitIsNumeric,
                            bitIsLengthValidation,
                            intMaxLength,
                            intMinLength,
                            bitFieldMessage,
                            vcFieldMessage vcFieldMessage,
                            ListRelID,
							intColumnWidth
                    FROM    View_DynamicColumns
                    WHERE   numFormId = 21
                            AND numUserCntID = @numUserCntID
                            AND numDomainID = @numDomainID
                            AND tintPageType = 1
                            AND ISNULL(bitSettingField, 0) = 1
                            AND ISNULL(bitCustom, 0) = 0
                            AND ISNULL(numRelCntType,0)=0
                    UNION
                    SELECT  tintRow + 1 AS tintOrder,
                            vcDbColumnName,
                            vcFieldName,
                            vcAssociatedControlType,
                            '' AS vcListItemType,
                            numListID,
                            '',
                            bitCustom,
                            numFieldId,
                            bitAllowSorting,
                            bitAllowEdit,
                            bitIsRequired,
                            bitIsEmail,
                            bitIsAlphaNumeric,
                            bitIsNumeric,
                            bitIsLengthValidation,
                            intMaxLength,
                            intMinLength,
                            bitFieldMessage,
                            vcFieldMessage,
                            ListRelID,
							intColumnWidth
                    FROM    View_DynamicCustomColumns
                    WHERE   numFormId = 21
                            AND numUserCntID = @numUserCntID
                            AND numDomainID = @numDomainID
                            AND tintPageType = 1
                            AND ISNULL(bitCustom, 0) = 1
                            AND ISNULL(numRelCntType,0)=0
                    ORDER BY tintOrder ASC      
            
    
            
	IF @byteMode=0
	BEGIN
		
	IF @columnName = 'OnHand' 
        SET @columnName = 'numOnHand'
    ELSE IF @columnName = 'Backorder' 
        SET @columnName = 'numBackOrder'
    ELSE IF @columnName = 'OnOrder' 
        SET @columnName = 'numOnOrder'
    ELSE IF @columnName = 'OnAllocation' 
        SET @columnName = 'numAllocation'
    ELSE IF @columnName = 'Reorder' 
        SET @columnName = 'numReorder'
    ELSE IF @columnName = 'monStockValue' 
        SET @columnName = 'sum(numOnHand) * Isnull(monAverageCost,0)'
    ELSE IF @columnName = 'ItemType' 
        SET @columnName = 'charItemType'

  
    DECLARE @column AS VARCHAR(50)              
    SET @column = @columnName              
    DECLARE @join AS VARCHAR(400)                  
    SET @join = ''           
    IF @columnName LIKE '%Cust%' 
        BEGIN                
			DECLARE @CustomFieldType AS VARCHAR(10)
			DECLARE @fldId AS VARCHAR(10)
			
			SET @fldId = REPLACE(@columnName, 'Cust', '')
			SELECT TOP 1 @CustomFieldType = ISNULL(vcAssociatedControlType,'') FROM View_DynamicCustomColumns WHERE numFieldID = @fldId
            
            IF ISNULL(@CustomFieldType,'') = 'SelectBox' OR ISNULL(@CustomFieldType,'') = 'ListBox'
				BEGIN
					SET @join = @join + ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=item.numItemCode and CFW.fld_id= ' + @fldId + ' ' 
					SET @join = @join + ' left Join ListDetails LstCF on LstCF.numListItemID = CFW.Fld_Value  '            
					SET @columnName = ' LstCF.vcData ' 	
				END
			ELSE
				BEGIN
					SET @join = ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=item.numItemCode and CFW.fld_id= ' + @fldId + ' '                                                         
					SET @columnName = 'CFW.Fld_Value'                	
				END
        END                                                
           
                                      
    DECLARE @firstRec AS INTEGER                                        
    DECLARE @lastRec AS INTEGER                                        
    SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                        
    SET @lastRec = ( @CurrentPage * @PageSize + 1 )   

    DECLARE @bitLocation AS BIT  
    SELECT  @bitLocation = ( CASE WHEN COUNT(*) > 0 THEN 1
                                  ELSE 0
                             END )
    FROM    View_DynamicColumns
    WHERE   numFormId = 21
            AND numUserCntID = @numUserCntID
            AND numDomainID = @numDomainID
            AND tintPageType = 1
            AND bitCustom = 0
            AND ISNULL(bitSettingField, 0) = 1
            AND vcDbColumnName = 'vcWareHouse'
                                                                   
                                        
    DECLARE @strSql AS VARCHAR(8000)
    DECLARE @strWhere AS VARCHAR(8000)
    SET @strWhere = ' AND 1=1'
    
      SET @strSql = '
declare @numDomain numeric
set @numDomain = ' + CONVERT(VARCHAR(15), @numDomainID) + ';
    INSERT INTO #tempItemList select numItemCode'
  
--    SET @strSql = @strSql + ' from item            

	IF @ItemType = 'P'
		SET @strWhere = @strWhere + ' AND charItemType= ''P''' 
	ELSE IF @ItemType = 'S'       
		SET @strWhere = @strWhere + ' AND charItemType= ''S'''
    ELSE IF @ItemType = 'N'    
		SET @strWhere = @strWhere + ' AND charItemType= ''N'''
        
    IF @bitAssembly = '1' 
	BEGIN
        SET @strWhere = @strWhere + ' AND bitAssembly= ' + CONVERT(VARCHAR(2), @bitAssembly) + ''  
	END 
	ELSE IF @ItemType = 'P'
	BEGIN
		SET @strWhere = @strWhere + ' AND ISNULL(bitAssembly,0)=0 '
	END

    IF @IsKit = '1' 
	BEGIN
        SET @strWhere = @strWhere + ' AND ISNULL(bitAssembly,0)=0 AND bitKitParent= ' + CONVERT(VARCHAR(2), @IsKit) + ''  
	END           
	ELSE IF @ItemType = 'P'
	BEGIN
		SET @strWhere = @strWhere + ' AND ISNULL(bitKitParent,0)=0 '
	END

    IF @bitSerialized = 1 
	BEGIN
        SET @strWhere = @strWhere + ' AND (bitSerialized=1 OR bitLotNo=1)'
	END
	ELSE IF @ItemType = 'P'
	BEGIN
		SET @strWhere = @strWhere + ' AND ISNULL(bitSerialized,0)=0 AND ISNULL(bitLotNo,0)=0 '
	END

	IF @bitAsset = 1 
	BEGIN
		SET @strWhere = @strWhere + ' AND ISNULL(Item.bitAsset,0) = 1'
	END
	ELSE IF @ItemType = 'P'
	BEGIN
		SET @strWhere = @strWhere + ' AND ISNULL(bitAsset,0) = 0 '
	END

	IF @bitRental = 1
	BEGIN
		SET @strWhere = @strWhere + ' AND ISNULL(Item.bitRental,0) = 1 AND ISNULL(Item.bitAsset,0) = 1 '
	END
	ELSE IF @ItemType = 'P'
	BEGIN
		SET @strWhere = @strWhere + ' AND ISNULL(bitRental,0) = 0 '
	END

    IF @SortChar <> '0' 
        SET @strWhere = @strWhere + ' AND vcItemName like ''' + @SortChar + '%'''                                       
        
    IF @ItemClassification <> '0' 
        SET @strWhere = @strWhere + ' AND numItemClassification=' + CONVERT(VARCHAR(15), @ItemClassification)    
    
    IF @IsArchive = 0 
        SET @strWhere = @strWhere + ' AND ISNULL(Item.IsArchieve,0) = 0'       
		
    IF @KeyWord <> '' 
        BEGIN 
            IF CHARINDEX('vcCompanyName', @KeyWord) > 0 
                BEGIN
                    SET @strWhere = @strWhere + ' and item.numItemCode in (select Vendor.numItemCode from Vendor join 
											  divisionMaster div on div.numdivisionid=Vendor.numVendorid 
											  join companyInfo com  on com.numCompanyID=div.numCompanyID  
											  WHERE Vendor.numDomainID=' + CONVERT(VARCHAR(15), @numDomainID) + '  
											  and Vendor.numItemCode= item.numItemCode and com.' + @KeyWord + ')' 
											  
					SET @join = @join + ' LEFT JOIN divisionmaster div on numVendorid=div.numDivisionId '   		
                END
            ELSE 
                SET @strWhere = @strWhere + ' and ' + @KeyWord 
        END
        
    IF @numItemGroup > 0 
        SET @strWhere = @strWhere + ' AND numItemGroup = ' + CONVERT(VARCHAR(20), @numItemGroup)  
               
    IF @numItemGroup = -1 
        SET @strWhere = @strWhere + ' and numItemGroup in (select numItemGroupID from ItemGroups where numDomainID=' + CONVERT(VARCHAR(20), @numDomainID) + ')'                                      
        
    SET @strWhere = @strWhere + ISNULL(@Where,'') + ' group by numItemCode '  
  
    IF @bitLocation = 1 
        SET @strWhere = @strWhere + '  ,vcWarehouse'
        
        SET @join = @join + ' LEFT JOIN WareHouseItems ON numItemID = numItemCode '
        SET @join = @join + ' LEFT JOIN Warehouses W ON W.numWareHouseID = WareHouseItems.numWareHouseID '
        
    IF @columnName <> 'sum(numOnHand) * Isnull(monAverageCost,0)' 
        BEGIN
            SET @strWhere = @strWhere + ',' + @columnName   
            
            IF CHARINDEX('LEFT JOIN WareHouseItems',@join) = 0
            BEGIN
				SET @join = @join + ' LEFT JOIN WareHouseItems ON numItemID = numItemCode '
			END	
        END
    
  
    SET @strSql = @strSql + ' FROM Item  ' + @join + '   
	LEFT JOIN ListDetails LD ON LD.numListItemID = Item.numShipClass
	LEFT JOIN ItemCategory IC ON IC.numItemID = Item.numItemCode
	WHERE Item.numDomainID= @numDomain
	AND ( WareHouseItems.numDomainID = ' + CONVERT(VARCHAR(15), @numDomainID) + ' OR WareHouseItems.numWareHouseItemID IS NULL) ' + @strWhere
    
    SET @strSql = @strSql + ' ORDER BY ' + @columnName + ' ' + @columnSortOrder 
 
	PRINT @strSql

	CREATE TABLE #tempItemList (RowNo INT IDENTITY(1,1) NOT NULL,numItemCode NUMERIC(18,0))
	 EXEC (@strSql)
	DELETE FROM #tempItemList WHERE RowNo NOT IN (SELECT MIN(RowNo) FROM #tempItemList GROUP BY numItemCode)

	CREATE TABLE #tempItemList1 (RunningCount INT IDENTITY(1,1) NOT NULL,numItemCode NUMERIC(18,0),TotalRowCount INT)
	INSERT INTO #tempItemList1 (numItemCode) SELECT numItemCode FROM #tempItemList
	UPDATE #tempItemList1 SET TotalRowCount=(SELECT COUNT(*) FROM #tempItemList1)
	
	SET @strSql = ' select                    
 min(RunningCount) as RunningCount,
 min(TotalRowCount) as TotalRowCount,I.numItemCode,I.vcItemName,I.txtItemDesc,I.charItemType,                                     
case when charItemType=''P'' then 
case 
when ISNULL(bitAssembly,0) = 1 then ''Assembly''
when ISNULL(bitKitParent,0) = 1 then ''Kit''
when ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=0 then ''Asset''
when ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=1 then ''Rental Asset''
when ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 0  then ''Serialized''
when ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=0 then ''Serialized Asset''
when ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=1 then ''Serialized Rental Asset''
when ISNULL(bitLotNo,0)=1 THEN ''Lot #''
else ''Inventory Item'' end
when charItemType=''N'' then ''Non Inventory Item'' when charItemType=''S'' then ''Service'' when charItemType=''A'' then ''Accessory'' end as ItemType,                                      
CASE WHEN bitKitParent = 1 THEN ISNULL(dbo.fn_GetKitInventory(I.numItemCode),0) ELSE isnull(sum(numOnHand),0) END  as numOnHand,                  
isnull(sum(numOnOrder),0) as numOnOrder,                                      
isnull(sum(numReorder),0) as numReorder,                  
isnull(sum(numBackOrder),0) as numBackOrder,                  
isnull(sum(numAllocation),0) as numAllocation,                  
vcCompanyName,  
CAST(ISNULL(monAverageCost,0) AS DECIMAL(10,2)) AS monAverageCost ,
CAST(ISNULL(monAverageCost,0) * SUM(numOnHand) AS DECIMAL(10,2)) AS monStockValue,
ISNULL((select vcItemGroup from ItemGroups where numItemGroupID = I.numItemGroup),'''') AS numItemGroup,
I.vcModelID,I.monListPrice,I.vcManufacturer,I.numBarCodeId,I.fltLength,I.fltWidth,I.fltHeight,I.fltWeight,I.vcSKU,LD.vcData AS numShipClass,ISNULL(v.monCost,0) AS monCost,ISNULL(v.intMinQty,0) AS intMinQty,
ISNULL(UOMBase.vcUnitName,'''') AS numBaseUnit,
ISNULL(UOMSale.vcUnitName,'''') AS numSaleUnit,
ISNULL(UOMPurchase.vcUnitName,'''') AS numPurchaseUnit ' 

   IF @bitLocation = 1 
        SET @strSql = @strSql + '  ,vcWarehouse'  

SET @strSql = @strSql + ' INTO #tblItem FROM #tempItemList1 as tblItem JOIN ITEM I ON tblItem.numItemCode=I.numItemCode 
left join WareHouseItems on numItemID=I.numItemCode                                  
left join divisionmaster div  on I.numVendorid=div.numDivisionId 
left join Vendor v on I.numVendorID=V.numVendorID AND I.numItemCode = V.numItemCode                                   
left join companyInfo com on com.numCompanyid=div.numcompanyID   
left join Warehouses W  on W.numWareHouseID=WareHouseItems.numWareHouseID 
LEFT JOIN ListDetails LD ON LD.numListItemID = i.numShipClass
LEFT JOIN UOM UOMBase ON UOMBase.numUOMId = i.numBaseUnit
LEFT JOIN UOM UOMSale ON UOMSale.numUOMId = i.numSaleUnit
LEFT JOIN UOM UOMPurchase ON UOMPurchase.numUOMId = i.numPurchaseUnit
    where  RunningCount >' + CONVERT(VARCHAR(15), @firstRec)
        + ' and RunningCount < ' + CONVERT(VARCHAR(15), @lastRec)
          + ' group by I.numItemCode,vcItemName,txtItemDesc,charItemType,vcCompanyName,vcModelID,monListPrice,vcManufacturer,numBarCodeId,fltLength,fltWidth,fltHeight,fltWeight,monAverageCost,I.vcSKU,LD.vcData,bitKitParent,bitAssembly,bitAsset,bitRental,bitSerialized,bitLotNo,numItemGroup,monCost,intMinQty,UOMBase.vcUnitName,UOMSale.vcUnitName,UOMPurchase.vcUnitName '  
          
              IF @bitLocation = 1 
        SET @strSql = @strSql + '  ,vcWarehouse'  
        
        SET @strSql = @strSql + ' order by RunningCount '   
          
    DECLARE @tintOrder AS TINYINT                                                  
    DECLARE @vcFieldName AS VARCHAR(50)                                                  
    DECLARE @vcListItemType AS VARCHAR(3)                                             
    DECLARE @vcListItemType1 AS VARCHAR(1)                                                 
    DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
    DECLARE @numListID AS NUMERIC(9)                                                  
    DECLARE @vcDbColumnName VARCHAR(20)                      
    DECLARE @WhereCondition VARCHAR(2000)                       
    DECLARE @vcLookBackTableName VARCHAR(2000)                
    DECLARE @bitCustom AS BIT                  
    DECLARE @numFieldId AS NUMERIC  
    DECLARE @bitAllowSorting AS CHAR(1)   
    DECLARE @bitAllowEdit AS CHAR(1)                   
                 
    SET @tintOrder = 0                                                  
    SET @WhereCondition = ''                 
                   
   
   
    SET @strSql = @strSql
        + ' select TotalRowCount,RunningCount,temp.numItemCode,vcItemName,txtItemDesc,charItemType,ItemType,numOnHand, LD.vcData AS numShipClass,                  
				   numOnOrder,numReorder,numBackOrder,numAllocation,vcCompanyName,vcModelID,monListPrice,vcManufacturer,numBarCodeId,fltLength,fltWidth,fltHeight,
				   fltWeight,(Select SUM(WO.numQtyItemsReq) from WorkOrder WO where WO.numItemCode=temp.numItemCode and WO.numWOStatus=0) as WorkOrder,
				   monAverageCost, monStockValue,vcSKU, numItemGroup,monCost,intMinQty,numBaseUnit,numSaleUnit,numPurchaseUnit '     
  
    IF @bitLocation = 1 
        SET @strSql = @strSql + '  ,vcWarehouse'                                               

    DECLARE @ListRelID AS NUMERIC(9) 
  
    SELECT TOP 1
            @tintOrder = tintOrder + 1,
            @vcDbColumnName = vcDbColumnName,
            @vcFieldName = vcFieldName,
            @vcAssociatedControlType = vcAssociatedControlType,
            @vcListItemType = vcListItemType,
            @numListID = numListID,
            @vcLookBackTableName = vcLookBackTableName,
            @bitCustom = bitCustomField,
            @numFieldId = numFieldId,
            @bitAllowSorting = bitAllowSorting,
            @bitAllowEdit = bitAllowEdit,
            @ListRelID = ListRelID
    FROM    #tempForm --WHERE bitCustomField=1
    ORDER BY tintOrder ASC            

    WHILE @tintOrder > 0                                                  
        BEGIN                                                  
            IF @bitCustom = 0
               BEGIN
               PRINT @vcDbColumnName
			   		IF @vcDbColumnName = 'vcPathForTImage'
		   			BEGIN
					   	SET @strSql = @strSql + ',ISNULL(II.vcPathForTImage,'''') AS [vcPathForTImage]'                   
                        SET @WhereCondition = @WhereCondition + ' LEFT JOIN ItemImages II ON II.numItemCode = temp.numItemCode AND bitDefault = 1'
					END
			   END                              
			   
            ELSE IF @bitCustom = 1 
                BEGIN      
                  
                    SELECT  @vcFieldName = FLd_label,
                            @vcAssociatedControlType = fld_type,
                            @vcDbColumnName = 'Cust'
                            + CONVERT(VARCHAR(10), Fld_Id)
                    FROM    CFW_Fld_Master
                    WHERE   CFW_Fld_Master.Fld_Id = @numFieldId                 
     
              
--    print @vcAssociatedControlType                
                    IF @vcAssociatedControlType = 'TextBox'
                        OR @vcAssociatedControlType = 'TextArea' 
                        BEGIN                
                   
                            SET @strSql = @strSql + ',CFW'  + CONVERT(VARCHAR(3), @tintOrder)  + '.Fld_Value  [' + @vcFieldName + '~' + @vcDbColumnName + ']'                   
                            SET @WhereCondition = @WhereCondition + ' left Join CFW_FLD_Values_Item CFW' + CONVERT(VARCHAR(3), @tintOrder)
                                + '                 
    on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                + CONVERT(VARCHAR(10), @numFieldId)
                                + 'and CFW' + CONVERT(VARCHAR(3), @tintOrder)
                                + '.RecId=temp.numItemCode   '                                                         
                        END   
                    ELSE 
                        IF @vcAssociatedControlType = 'CheckBox' 
                            BEGIN            
               
                                SET @strSql = @strSql
                                    + ',case when isnull(CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.Fld_Value,0)=0 then ''No'' when isnull(CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.Fld_Value,0)=1 then ''Yes'' end   ['
                                    + @vcFieldName + '~' + @vcDbColumnName
                                    + ']'              
 
                                SET @WhereCondition = @WhereCondition
                                    + ' left Join CFW_FLD_Values_Item CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '             
    on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                    + CONVERT(VARCHAR(10), @numFieldId)
                                    + 'and CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.RecId=temp.numItemCode   '                                                     
                            END                
                        ELSE 
                            IF @vcAssociatedControlType = 'DateField' 
                                BEGIN              
                   
                                    SET @strSql = @strSql
                                        + ',dbo.FormatedDateFromDate(CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.Fld_Value,'
                                        + CONVERT(VARCHAR(10), @numDomainId)
                                        + ')  [' + @vcFieldName + '~'
                                        + @vcDbColumnName + ']'                   
                                    SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Item CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '                 
    on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                        + CONVERT(VARCHAR(10), @numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.RecId=temp.numItemCode   '                                                         
                                END                
                            ELSE 
                                IF @vcAssociatedControlType = 'SelectBox' 
                                    BEGIN                
                                        SET @vcDbColumnName = 'Cust'
                                            + CONVERT(VARCHAR(10), @numFieldId)                
                                        SET @strSql = @strSql + ',L'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.vcData' + ' [' + @vcFieldName
                                            + '~' + @vcDbColumnName + ']'                                                          
                                        SET @WhereCondition = @WhereCondition
                                            + ' left Join CFW_FLD_Values_Item CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '                 
     on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                            + CONVERT(VARCHAR(10), @numFieldId)
                                            + 'and CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.RecId=temp.numItemCode    '                                                         
                                        SET @WhereCondition = @WhereCondition
                                            + ' left Join ListDetails L'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + ' on L'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.numListItemID=CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.Fld_Value'                
                                    END                 
                END          
  
            SELECT TOP 1
                    @tintOrder = tintOrder + 1,
                    @vcDbColumnName = vcDbColumnName,
                    @vcFieldName = vcFieldName,
                    @vcAssociatedControlType = vcAssociatedControlType,
                    @vcListItemType = vcListItemType,
                    @numListID = numListID,
                    @vcLookBackTableName = vcLookBackTableName,
                    @bitCustom = bitCustomField,
                    @numFieldId = numFieldId,
                    @bitAllowSorting = bitAllowSorting,
                    @bitAllowEdit = bitAllowEdit,
                    @ListRelID = ListRelID
            FROM    #tempForm
            WHERE   tintOrder > @tintOrder - 1 --AND bitCustomField=1
            ORDER BY tintOrder ASC            
 
            IF @@rowcount = 0 
                SET @tintOrder = 0 
            
        END                       
      
    PRINT @bitLocation
    SET @strSql = @strSql + ' from #tblItem temp
			LEFT JOIN ListDetails LD ON LD.vcData = temp.numShipClass AND LD.numListID=461 and LD.numDomainID = '  + CONVERT(VARCHAR(20), @numDomainID) + ' '
		+ @WhereCondition
        + ' where  RunningCount >' + CONVERT(VARCHAR(15), @firstRec)
        + ' and RunningCount < ' + CONVERT(VARCHAR(15), @lastRec)
        + ' order by RunningCount'   

    SET @strSql = REPLACE(@strSql, '|', ',') 
    PRINT @strSql
    EXEC ( @strSql)  

    
                              
 
DROP TABLE #tempItemList
DROP TABLE #tempItemList1

END

UPDATE  #tempForm
    SET     vcDbColumnName = CASE WHEN bitCustomField = 1
                                  THEN vcFieldName + '~' + vcDbColumnName
                                  ELSE vcDbColumnName END
                                  
   SELECT  * FROM #tempForm

    DROP TABLE #tempForm
--SELECT * FROM Category
--- Alterd By Chintan Prajapati
-- EXEC dbo.USP_ItemsForECommerce 9,60,1,1,15,0,''
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemsForECommerce' ) 
    DROP PROCEDURE USP_ItemsForECommerce
GO
CREATE PROCEDURE [dbo].[USP_ItemsForECommerce]
    @numSiteID AS NUMERIC(9) = 0,
    @numCategoryID AS NUMERIC(18) = 0,
    @numWareHouseID AS NUMERIC(9) = 0,
    @SortBy NVARCHAR(MAX) = 0,
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT = 0 OUTPUT,
    @SearchText AS NVARCHAR(MAX) = '',--Comma seperated item ids
    @FilterQuery AS NVARCHAR(MAX) = '',----Comma seperated Attributes Ids
    @FilterRegularCondition AS NVARCHAR(MAX),
    --@FilterCustomCondition AS NVARCHAR(MAX),
    @FilterCustomFields AS NVARCHAR(MAX),
    @FilterCustomWhere AS NVARCHAR(MAX)
   
AS 
    BEGIN
		
		DECLARE @numDomainID AS NUMERIC(9)
		SELECT  @numDomainID = numDomainID
        FROM    [Sites]
        WHERE   numSiteID = @numSiteID
        
        DECLARE @tintDisplayCategory AS TINYINT
		SELECT @tintDisplayCategory = ISNULL(bitDisplayCategory,0) FROM dbo.eCommerceDTL WHERE numSiteID = @numSiteId
		PRINT @tintDisplayCategory 
		
		CREATE TABLE #tmpItemCat(numCategoryID NUMERIC(18,0))
		CREATE TABLE #tmpItemCode(numItemCode NUMERIC(18,0))
		IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
		BEGIN
--			DECLARE @strCustomSql AS NVARCHAR(MAX)
--			SET @strCustomSql = 'INSERT INTO #tmpItemCode(numItemCode)
--								 SELECT DISTINCT t2.RecId FROM CFW_Fld_Master t1 
--								 JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID 
--								 WHERE t1.Grp_id = 5 
--								 AND t1.numDomainID = (SELECT numDomainID FROM dbo.Sites WHERE numSiteID = ' + CONVERT(VARCHAR(10),@numSiteID) + ') 
--								 AND t1.fld_type <> ''Link'' AND ' + @FilterCustomCondition
			
			DECLARE @FilterCustomFieldIds AS NVARCHAR(MAX)
			SET @FilterCustomFieldIds = Replace(@FilterCustomFields,'[','')
			SET @FilterCustomFieldIds = Replace(@FilterCustomFieldIds,']','')
			
			DECLARE @strCustomSql AS NVARCHAR(MAX)
			SET @strCustomSql = 'INSERT INTO #tmpItemCode(numItemCode)
								 SELECT DISTINCT T.RecId FROM 
															(
																SELECT * FROM 
																	(
																		SELECT  t2.RecId, t1.fld_id, t2.Fld_Value FROM CFW_Fld_Master t1 
																		JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID 
																		WHERE t1.Grp_id = 5 
																		AND t1.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
																		AND t1.fld_type <> ''Link'' 
																		AND t1.fld_id IN (' + @FilterCustomFieldIds + ')
																	) p  PIVOT (MAX([Fld_Value]) FOR fld_id IN (' + @FilterCustomFields + ')) AS pvt 
															) T  WHERE 1=1 AND ' + @FilterCustomWhere
								 
			PRINT @strCustomSql
			EXEC SP_EXECUTESQL @strCustomSql								 					 
		END
		
        DECLARE @strSQL NVARCHAR(MAX)
        DECLARE @firstRec AS INTEGER
        DECLARE @lastRec AS INTEGER
        DECLARE @Where NVARCHAR(MAX)
        DECLARE @SortString NVARCHAR(MAX)
        DECLARE @searchPositionColumn NVARCHAR(MAX)
        DECLARE @row AS INTEGER
        DECLARE @data AS VARCHAR(100)
        DECLARE @dynamicFilterQuery NVARCHAR(MAX)
        DECLARE @checkFldType VARCHAR(100)
        DECLARE @count NUMERIC(9,0)
        DECLARE @whereNumItemCode NVARCHAR(MAX)
        DECLARE @filterByNumItemCode VARCHAR(100)
        
		SET @SortString = ''
        set @searchPositionColumn = ''
        SET @strSQL = ''
		SET @whereNumItemCode = ''
		SET @dynamicFilterQuery = ''
		 
        SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
        SET @lastRec = ( @CurrentPage * @PageSize + 1 )
                
        DECLARE @bitAutoSelectWarehouse AS BIT
		SELECT @bitAutoSelectWarehouse = ISNULL([ECD].[bitAutoSelectWarehouse],0) FROM [dbo].[eCommerceDTL] AS ECD WHERE [ECD].[numSiteId] = @numSiteID
		AND [ECD].[numDomainId] = @numDomainID

		DECLARE @vcWarehouseIDs AS VARCHAR(1000)
        IF (@numWareHouseID = 0 OR @bitAutoSelectWarehouse = 1)
            BEGIN				
				IF @bitAutoSelectWarehouse = 1
					BEGIN
						SELECT @vcWarehouseIDs = ISNULL( STUFF((SELECT DISTINCT ',' + CAST(numWarehouseID AS VARCHAR(10)) FROM [dbo].[Warehouses] WHERE [Warehouses].[numDomainID] = @numDomainID FOR XML PATH('')),1, 1, '') , '') 
					END
				
				ELSE
					BEGIN
						SELECT  @numWareHouseID = ISNULL(numDefaultWareHouseID,0)
						FROM    [eCommerceDTL]
						WHERE   [numDomainID] = @numDomainID	

						SET @vcWarehouseIDs = @numWareHouseID
					END
            END
		ELSE
		BEGIN
			SET @vcWarehouseIDs = @numWareHouseID
		END

		PRINT @vcWarehouseIDs

		--PRIAMRY SORTING USING CART DROPDOWN
		DECLARE @Join AS NVARCHAR(MAX)
		DECLARE @SortFields AS NVARCHAR(MAX)
		DECLARE @vcSort AS VARCHAR(MAX)

		SET @Join = ''
		SET @SortFields = ''
		SET @vcSort = ''

		IF @SortBy = ''
			BEGIN
				SET @SortString = ' vcItemName Asc '
			END
		ELSE IF CHARINDEX('~0',@SortBy) > 0 OR CHARINDEX('~1',@SortBy) > 0
			BEGIN
				DECLARE @fldID AS NUMERIC(18,0)
				DECLARE @RowNumber AS VARCHAR(MAX)
				
				SELECT * INTO #fldValues FROM dbo.SplitIDsWithPosition(@SortBy,'~')
				SELECT @RowNumber = RowNumber, @fldID = id FROM #fldValues WHERE RowNumber = 1
				SELECT @vcSort = CASE WHEN Id = 0 THEN 'ASC' ELSE 'DESC' END FROM #fldValues WHERE RowNumber = 2
				
				SET @Join = @Join + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber + ' ON I.numItemCode = CFVI' + @RowNumber + '.RecID AND CFVI' + @RowNumber + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID)
				---SET @SortString = 'CFVI' + @RowNumber + '.fld_Value' 
				SET @SortString = '[fld_Value' + @RowNumber + '] ' --+ @vcSort
				SET @SortFields = ', CFVI' + @RowNumber + '.fld_Value [fld_Value' + @RowNumber + ']'
			END
		ELSE
			BEGIN
				--SET @SortString = @SortBy	
				
				IF CHARINDEX('monListPrice',@SortBy) > 0
				BEGIN
					DECLARE @bitSortPriceMode AS BIT
					SELECT @bitSortPriceMode = ISNULL(bitSortPriceMode,0) FROM dbo.eCommerceDTL WHERE numSiteId = @numSiteID
					PRINT @bitSortPriceMode
					IF @bitSortPriceMode = 1
					BEGIN
						SET @SortString = ' '
					END
					ELSE
					BEGIN
						SET @SortString = @SortBy	
					END
				END
				ELSE
				BEGIN
					SET @SortString = @SortBy	
				END
				
			END	

		--SECONDARY SORTING USING DEFAULT SETTINGS FROM ECOMMERCE
		SELECT ROW_NUMBER() OVER (ORDER BY Custom,vcFieldName) AS [RowID],* INTO #tempSort FROM (
		SELECT  CONVERT(VARCHAR(9),numFieldID)+'~0' AS numFieldID,
				ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
				0 AS Custom,
				vcDbColumnName 
		FROM View_DynamicColumns
		WHERE numFormID = 84 
		AND ISNULL(bitSettingField,0) = 1 
		AND ISNULL(bitDeleted,0)=0 
		AND ISNULL(bitCustom,0) = 0
		AND numDomainID = @numDomainID
					       
		UNION     

		SELECT  CONVERT(VARCHAR(9),numFieldID)+'~1' AS numFieldID ,
				vcFieldName ,
				1 AS Custom,
				'' AS vcDbColumnName
		FROM View_DynamicCustomColumns          
		WHERE numFormID = 84
		AND numDomainID = @numDomainID
		AND grp_id = 5 
		AND vcAssociatedControlType = 'TextBox' 
		AND ISNULL(bitCustom,0) = 1 
		AND tintPageType=1  
		) TABLE1 ORDER BY Custom,vcFieldName
		
		--SELECT * FROM #tempSort
		DECLARE @DefaultSort AS NVARCHAR(MAX)
		DECLARE @DefaultSortFields AS NVARCHAR(MAX)
		DECLARE @DefaultSortJoin AS NVARCHAR(MAX)
		DECLARE @intCnt AS INT
		DECLARE @intTotalCnt AS INT	
		DECLARE @strColumnName AS VARCHAR(100)
		DECLARE @bitCustom AS BIT
		DECLARE @FieldID AS VARCHAR(100)				

		SET @DefaultSort = ''
		SET @DefaultSortFields = ''
		SET @DefaultSortJoin = ''		
		SET @intCnt = 0
		SET @intTotalCnt = ( SELECT COUNT(*) FROM #tempSort )
		SET @strColumnName = ''
		
		IF (SELECT ISNULL(bitEnableSecSorting,0) FROM EcommerceDtl WHERE numSiteID = @numSiteID ) = 1
		BEGIN
			CREATE TABLE #fldDefaultValues(RowNumber INT,Id VARCHAR(1000))

			WHILE(@intCnt < @intTotalCnt)
			BEGIN
				SET @intCnt = @intCnt + 1
				SELECT @FieldID = numFieldID, @strColumnName = vcDbColumnName, @bitCustom = Custom FROM #tempSort WHERE RowID = @intCnt
				
				--PRINT @FieldID
				--PRINT @strColumnName
				--PRINT @bitCustom

				IF @bitCustom = 0
					BEGIN
						SET @DefaultSort =  @strColumnName + ',' + @DefaultSort 
					END

				ELSE
					BEGIN
						DECLARE @fldID1 AS NUMERIC(18,0)
						DECLARE @RowNumber1 AS VARCHAR(MAX)
						DECLARE @vcSort1 AS VARCHAR(10)
						DECLARE @vcSortBy AS VARCHAR(1000)
						SET @vcSortBy = CONVERT(VARCHAR(100),@FieldID) + '~' + '0'
						--PRINT @vcSortBy

						INSERT INTO #fldDefaultValues(RowNumber,Id) SELECT RowNumber, id FROM dbo.SplitIDsWithPosition(@vcSortBy,'~')
						SELECT @RowNumber1 = RowNumber + 100 + @intCnt, @fldID1 = id FROM #fldDefaultValues WHERE RowNumber = 1
						SELECT @vcSort1 = 'ASC' 
						
						SET @DefaultSortJoin = @DefaultSortJoin + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber1 + ' ON I.numItemCode = CFVI' + @RowNumber1 + '.RecID AND CFVI' + @RowNumber1 + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID1)
						SET @DefaultSort = '[fld_Value' + @RowNumber1 + '] ' + ',' + @DefaultSort 
						SET @DefaultSortFields = @DefaultSortFields + ', CFVI' + @RowNumber1 + '.fld_Value [fld_Value' + @RowNumber1 + ']'
					END

			END
			IF LEN(@DefaultSort) > 0
			BEGIN
				SET  @DefaultSort = LEFT(@DefaultSort,LEN(@DefaultSort) - 1)
				SET @SortString = @SortString + ',' 
			END
			--PRINT @SortString
			--PRINT @DefaultSort
			--PRINT @DefaultSortFields
		END
--        IF @SortBy = 1 
--            SET @SortString = ' vcItemName Asc '
--        ELSE IF @SortBy = 2 
--            SET @SortString = ' vcItemName Desc '
--        ELSE IF @SortBy = 3 
--            SET @SortString = ' monListPrice Asc '
--        ELSE IF @SortBy = 4 
--            SET @SortString = ' monListPrice Desc '
--        ELSE IF @SortBy = 5 
--            SET @SortString = ' bintCreatedDate Desc '
--        ELSE IF @SortBy = 6 
--            SET @SortString = ' bintCreatedDate ASC '	
--        ELSE IF @SortBy = 7 --sort by relevance, invokes only when search
--            BEGIN
--                 IF LEN(@SearchText) > 0 
--		            BEGIN
--						SET @SortString = ' SearchOrder ASC ';
--					    set @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 						
--					END
--			END
--        ELSE 
--            SET @SortString = ' vcItemName Asc '
		
                                		
        IF LEN(@SearchText) > 0
			BEGIN
				PRINT 1
				IF CHARINDEX('SearchOrder',@SortString) > 0
				BEGIN
					SET @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 
				END

				SET @Where = ' INNER JOIN dbo.SplitIDsWithPosition('''+ @SearchText +''','','') AS SearchItems 
                        ON SearchItems.Id = I.numItemCode  and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID) + ' WHERE ISNULL(IsArchieve,0) = 0'
				END

		ELSE IF @SearchText = ''

			SET @Where = ' WHERE 1=2 '
			+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
			+ ' AND ISNULL(IsArchieve,0) = 0'
		ELSE
			BEGIN
				PRINT 2
				SET @Where = ' WHERE 1=1 '
							+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
							+ ' AND ISNULL(IsArchieve,0) = 0'


				PRINT @tintDisplayCategory							
				PRINT @Where

				IF @tintDisplayCategory = 1
				BEGIN
					
					;WITH CTE AS(
					SELECT C.numCategoryID,C.tintLevel,C.numDepCategory,CASE WHEN tintLevel=1 THEN 1 ELSE 0 END AS bitChild 
						FROM Category C WHERE (numCategoryID = @numCategoryID OR ISNULL(@numCategoryID,0) = 0) AND numDomainID = @numDomainID
					UNION ALL
					SELECT  C.numCategoryID,C.tintLevel,C.numDepCategory,CTE.bitChild FROM Category C JOIN CTE ON 
					C.numDepCategory=CTE.numCategoryID AND CTE.bitChild=1 AND numDomainID = @numDomainID)
					
					INSERT INTO #tmpItemCat(numCategoryID)												 
					SELECT DISTINCT C.numCategoryID FROM CTE C JOIN dbo.ItemCategory IC ON C.numCategoryID = IC.numCategoryID 
														WHERE numItemID IN ( 
																						
													 						SELECT numItemID FROM WareHouseItems WI
																			INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																			GROUP BY numItemID 
																			HAVING SUM(WI.numOnHand) > 0 
																			UNION ALL
													 						SELECT numItemID FROM WareHouseItems WI
																			INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																			GROUP BY numItemID 
																			HAVING SUM(WI.numAllocation) > 0
																		 )		
					SELECT numItemID INTO #tmpOnHandItems FROM 
					(													 					
						SELECT numItemID FROM WareHouseItems WI 
						INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
						GROUP BY numItemID 
						HAVING SUM(WI.numOnHand) > 0
						UNION ALL
						SELECT numItemID FROM WareHouseItems WI 
						INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
						GROUP BY numItemID 
						HAVING SUM(WI.numAllocation) > 0				
					) TABLE1
				END

			END
		
			--SELECT * FROM  #tmpOnHandItems
			--SELECT * FROM  #tmpItemCat
			
----		IF (SELECT COUNT(*) FROM #tmpItemCat) = 0 AND ISNULL(@numCategoryID,0) = 0
----		BEGIN
----			
----			INSERT INTO #tmpItemCat(numCategoryID)
----			SELECT DISTINCT C.numCategoryID FROM Category C
----			JOIN dbo.ItemCategory IC ON C.numCategoryID = IC.numCategoryID 
----							WHERE C.numDepCategory = (SELECT TOP 1 numCategoryID FROM dbo.Category 
----													  WHERE numDomainID = @numDomainID
----													  AND tintLevel = 1)
----							AND tintLevel <> 1
----							AND numDomainID = @numDomainID
----							AND numItemID IN (
----											 	SELECT numItemID FROM WareHouseItems WI
----												INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID
----												GROUP BY numItemID 
----												HAVING SUM(WI.numOnHand) > 0
----											 )
----			--SELECT * FROM #tmpItemCat								 
----		END
----		ELSE
----		BEGIN
----			INSERT INTO #tmpItemCat(numCategoryID)VALUES(@numCategoryID)
----		END

		       SET @strSQL = @strSQL + ' WITH Items AS 
	                                     ( 
	                                     SELECT   
                                               I.numItemCode,
                                               ROW_NUMBER() OVER ( Partition by I.numItemCode Order by I.numItemCode) RowID,
                                               vcItemName,
                                               I.bintCreatedDate,
                                               vcManufacturer,
                                               C.vcCategoryName,
                                               ISNULL(txtItemDesc,'''') txtItemDesc,
                                               ISNULL(CASE  WHEN I.[charItemType] = ''P''
															THEN ( UOM * ISNULL(W1.[monWListPrice], 0) )
															ELSE ( UOM * monListPrice )
													  END, 0) monListPrice,
                                               vcSKU,
                                               fltHeight,
                                               fltWidth,
                                               fltLength,
                                               IM.vcPathForImage,
                                               IM.vcPathForTImage,
                                               vcModelID,
                                               fltWeight,
                                               bitFreeShipping,
                                               UOM AS UOMConversionFactor,
												( CASE WHEN charItemType <> ''S'' AND charItemType <> ''N''
													   THEN ( CASE WHEN bitShowInStock = 1
																   THEN ( CASE WHEN ( ISNULL(W.numOnHand,0)<=0) THEN ''<font color=red>Out Of Stock</font>''
																			   WHEN bitAllowBackOrder = 1 THEN ''In Stock''
																			   ELSE ''In Stock'' 
																		  END )
																   ELSE ''''
															  END )
													   ELSE ''''
												  END ) AS InStock, C.vcDescription as CategoryDesc, ISNULL(W1.numWareHouseItemID,0) [numWareHouseItemID] ' 
								               +	@searchPositionColumn + @SortFields + @DefaultSortFields + '
								
                                         FROM      
                                                      Item AS I
                                         INNER JOIN   ItemCategory IC   ON   I.numItemCode = IC.numItemID
                                         LEFT JOIN    ItemImages IM     ON   I.numItemCode = IM.numItemCode AND IM.bitdefault=1 AND IM.bitIsImage = 1
                                         INNER JOIN   Category C        ON   IC.numCategoryID = C.numCategoryID
                                         INNER JOIN   SiteCategories SC ON   IC.numCategoryID = SC.numCategoryID
                                         INNER JOIN   eCommerceDTL E    ON   E.numDomainID = I.numDomainID ' + @Join + ' ' + @DefaultSortJoin + ' 
                                         CROSS APPLY (SELECT SUM(numOnHand) numOnHand FROM WareHouseItems W 
													  WHERE  W.numItemID = I.numItemCode
                                                      AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
                                                      AND W.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + ') AS W  
										 CROSS APPLY (SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems W 
													  WHERE  W.numItemID = I.numItemCode 
													  AND W.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
													  AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))) AS W1
										 CROSS APPLY (SELECT SUM(numAllocation) numAllocation FROM WareHouseItems W2 
													  WHERE  W2.numItemID = I.numItemCode
                                                      AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
                                                      AND W2.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + ') AS W2  			  
										 CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numSaleUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOM) AS UOM '

			IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
			BEGIN
				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCode TP ON TP.numItemCode = I.numItemCode ' 
			END
			
			IF (SELECT COUNT(*) FROM #tmpItemCat) > 0
			BEGIN
				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCat TC ON TC.numCategoryID = IC.numCategoryID '
				SET @strSQL = @strSQL + ' LEFT JOIN #tmpOnHandItems TOIC ON TOIC.numItemID = I.numItemCode '
			END
			ELSE IF @numCategoryID>0
			BEGIN
				SET @Where = REPLACE(@Where,'WHERE',' WHERE IC.numCategoryID = ' + CONVERT(VARCHAR(10),@numCategoryID) + ' AND ')
			END 
			
			IF @tintDisplayCategory = 1
			BEGIN
				SET @Where = @Where + ' AND (ISNULL(W.numOnHand, 0) > 0 OR ISNULL(W2.numAllocation, 0) > 0) '
			END
			
			--PRINT @Where
			SET @strSQL = @strSQL + @Where
			
			IF LEN(@FilterRegularCondition) > 0
			BEGIN
				SET @strSQL = @strSQL + @FilterRegularCondition
			END                                         
			
---PRINT @SortString + ',' + @DefaultSort + ' ' + @vcSort 
	
            IF LEN(@whereNumItemCode) > 0
                                        BEGIN
								              SET @strSQL = @strSQL + @filterByNumItemCode	
								    	END
            SET @strSQL = @strSQL +     '   )
                                        , ItemSorted AS (SELECT  ROW_NUMBER() OVER ( ORDER BY  ' + @SortString + @DefaultSort + ' ' + @vcSort + ' ) AS Rownumber,* FROM  Items WHERE RowID = 1)'
            
            DECLARE @fldList AS NVARCHAR(MAX)
			SELECT @fldList=COALESCE(@fldList + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_C') FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=5
            
            SET @strSQL = @strSQL +' SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,InStock,CategoryDesc,numWareHouseItemID
                                     INTO #tempItem FROM ItemSorted ' 
--                                        + 'WHERE Rownumber > ' + CONVERT(VARCHAR(10), @firstRec) + '
--                                        AND   Rownumber < ' + CONVERT(VARCHAR(10), @lastRec) + '
--                                        order by Rownumber;' 
            
            SET @strSQL = @strSQL + ' SELECT * INTO #tmpPagedItems FROM #tempItem WHERE Rownumber > ' + CONVERT(VARCHAR(10), @firstRec) + '
                                      AND   Rownumber < ' + CONVERT(VARCHAR(10), @lastRec) + '
                                      order by Rownumber '
                                                                  
            IF LEN(ISNULL(@fldList,''))>0
            BEGIN
				PRINT 'flds1'
				SET @strSQL = @strSQL +'SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,InStock,CategoryDesc,numWareHouseItemID,' + @fldList + '
                                        FROM #tmpPagedItems I left join (
			SELECT  t2.RecId, t1.Fld_label + ''_C'' as Fld_label, 
				CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
								 WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
								 WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
								 ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
			JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 5 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
			AND t2.RecId IN (SELECT numItemCode FROM #tmpPagedItems )
			) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList + ')) AS pvt on I.numItemCode=pvt.RecId order by Rownumber;' 	
			END 
			ELSE
			BEGIN
				PRINT 'flds2'
				SET @strSQL = @strSQL +' SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,InStock,CategoryDesc,numWareHouseItemID
                                         FROM  #tmpPagedItems order by Rownumber;'
			END               
                                        
            
--            PRINT(@strSQL)        
--            EXEC ( @strSQL) ; 
			                           
            SET @strSQL = @strSQL + ' SELECT @TotRecs = COUNT(*) FROM #tempItem '                            
--             SET @strSQL = 'SELECT @TotRecs = COUNT(*) 
--                           FROM Item AS I
--                           INNER JOIN   ItemCategory IC ON I.numItemCode = IC.numItemID
--                           INNER JOIN   Category C ON IC.numCategoryID = C.numCategoryID
--                           INNER JOIN   SiteCategories SC ON IC.numCategoryID = SC.numCategoryID ' --+ @Where
--             
--			 IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
--			 BEGIN
--				 SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCode TP ON TP.numItemCode = I.numItemCode ' 
--			 END
--			
--			 IF (SELECT COUNT(*) FROM #tmpItemCat) > 0
--			 BEGIN
--				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCat TC ON TC.numCategoryID = IC.numCategoryID '
--				SET @strSQL = @strSQL + ' INNER JOIN #tmpOnHandItems TOIC ON TOIC.numItemID = I.numItemCode '
--			 END
--			 ELSE IF @numCategoryID>0
--			 BEGIN
--				SET @Where = REPLACE(@Where,'WHERE',' WHERE IC.numCategoryID = ' + CONVERT(VARCHAR(10),@numCategoryID) + ' AND ')
--			 END 
--			
--			 SET @strSQL = @strSQL + @Where  
--			 
--			 IF LEN(@FilterRegularCondition) > 0
--			 BEGIN
--				SET @strSQL = @strSQL + @FilterRegularCondition
--			 END 
			
		--			--SELECT * FROM  #tempAvailableFields
		--SELECT * FROM  #tmpItemCode
		--SELECT * FROM  #tmpItemCat
		----SELECT * FROM  #fldValues
		--SELECT * FROM  #tempSort
		--SELECT * FROM  #fldDefaultValues
		--SELECT * FROM  #tmpOnHandItems
		----SELECT * FROM  #tmpPagedItems

			 PRINT  LEN(@strSQL)
			 DECLARE @tmpSQL AS VARCHAR(MAX)
			 SET @tmpSQL = @strSQL
			 PRINT  @tmpSQL
			 
             EXEC sp_executeSQL @strSQL, N'@TotRecs INT OUTPUT', @TotRecs OUTPUT
             
             
        DECLARE @tintOrder AS TINYINT                                                  
		DECLARE @vcFormFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(1)                                             
		DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
		DECLARE @numListID AS NUMERIC(9)                                                  
		DECLARE @WhereCondition VARCHAR(MAX)                       
		DECLARE @numFormFieldId AS NUMERIC  
		DECLARE @vcFieldType CHAR(1)
		                  
		SET @tintOrder=0                                                  
		SET @WhereCondition =''                 
                   
              
		CREATE TABLE #tempAvailableFields(numFormFieldId  NUMERIC(9),vcFormFieldName NVARCHAR(50),
		vcFieldType CHAR(1),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),
		vcListItemType CHAR(1),intRowNum INT,vcItemValue VARCHAR(3000))
		   
		INSERT INTO #tempAvailableFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
				,numListID,vcListItemType,intRowNum)                         
		SELECT  Fld_id,REPLACE(Fld_label,' ','') AS vcFormFieldName,
			   CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType,
				fld_type as vcAssociatedControlType,
				ISNULL(C.numListID, 0) numListID,
				CASE WHEN C.numListID > 0 THEN 'L'
					 ELSE ''
				END vcListItemType,ROW_NUMBER() OVER (ORDER BY Fld_id ASC) AS intRowNum
		FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
		WHERE   C.numDomainID = @numDomainId
				AND GRP_ID IN (5)

		  
		SELECT * FROM #tempAvailableFields
		
		/*
		DROP TABLE #tempAvailableFields
		DROP TABLE #tmpItemCode
		DROP TABLE #tmpItemCat
		*/	

		IF OBJECT_ID('tempdb..#tempAvailableFields') IS NOT NULL
		DROP TABLE #tempAvailableFields
		
		IF OBJECT_ID('tempdb..#tmpItemCode') IS NOT NULL
		DROP TABLE #tmpItemCode
		
		IF OBJECT_ID('tempdb..#tmpItemCat') IS NOT NULL
		DROP TABLE #tmpItemCat
		
		IF OBJECT_ID('tempdb..#fldValues') IS NOT NULL
		DROP TABLE #fldValues
		
		IF OBJECT_ID('tempdb..#tempSort') IS NOT NULL
		DROP TABLE #tempSort
		
		IF OBJECT_ID('tempdb..#fldDefaultValues') IS NOT NULL
		DROP TABLE #fldDefaultValues
		
		IF OBJECT_ID('tempdb..#tmpOnHandItems') IS NOT NULL
		DROP TABLE #tmpOnHandItems

		IF OBJECT_ID('tempdb..#tmpPagedItems') IS NOT NULL
		DROP TABLE #tmpPagedItems
    END


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageCommissionRule')
DROP PROCEDURE USP_ManageCommissionRule
GO
CREATE PROCEDURE USP_ManageCommissionRule
    @numComRuleID NUMERIC,
    @vcCommissionName VARCHAR(100),
    @tintComAppliesTo TINYINT,
    @tintComBasedOn TINYINT,
    @tinComDuration TINYINT,
    @tintComType TINYINT,
    @numDomainID NUMERIC,
    @strItems TEXT,
    @tintAssignTo TINYINT,
    @tintComOrgType TINYINT
AS 
BEGIN TRY
BEGIN TRANSACTION
    IF @numComRuleID = 0 
    BEGIN
		INSERT  INTO CommissionRules
				(
					[vcCommissionName],
					[numDomainID]
				)
		VALUES  (
					@vcCommissionName,
					@numDomainID
				)
		SET @numComRuleID=@@identity
		Select @numComRuleID;
    END
    ELSE 
    BEGIN		
		UPDATE  
				CommissionRules
            SET     
				[vcCommissionName] = @vcCommissionName,
                [tintComBasedOn] = @tintComBasedOn,
                [tinComDuration] = @tinComDuration,
                [tintComType] = @tintComType,
				[tintComAppliesTo] = @tintComAppliesTo,
				[tintComOrgType] = @tintComOrgType,
				[tintAssignTo] = @tintAssignTo
            WHERE   
				[numComRuleID] = @numComRuleID 
				AND [numDomainID] = @numDomainID

		IF @tintComAppliesTo = 3
		BEGIN
			DELETE FROM CommissionRuleItems WHERE numComRuleID=@numComRuleID
		END
				
		DELETE FROM [CommissionRuleDtl] WHERE [numComRuleID] = @numComRuleID
		DELETE FROM [CommissionRuleContacts] WHERE [numComRuleID] = @numComRuleID

		DECLARE @hDocItem INT
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
        
		IF CONVERT(VARCHAR(10), @strItems) <> '' 
        BEGIN
            INSERT  INTO [CommissionRuleDtl]
                    (
                        numComRuleID,
                        [intFrom],
                        [intTo],
                        [decCommission]
                    )
                    SELECT  @numComRuleID,
                            X.[intFrom],
                            X.[intTo],
                            X.[decCommission]
                    FROM    ( SELECT    *
                                FROM      OPENXML (@hDocItem, '/NewDataSet/CommissionTable', 2)
                                        WITH ( intFrom DECIMAL(18,2), intTo DECIMAL(18,2), decCommission DECIMAL(18,2) )
                            ) X  
            INSERT  INTO [CommissionRuleContacts]
                    (
                        numComRuleID,
                        numValue,bitCommContact
                    )
                    SELECT  @numComRuleID,
                            X.[numValue],X.[bitCommContact]
                    FROM    ( SELECT    *
                                FROM      OPENXML (@hDocItem, '/NewDataSet/ContactTable', 2)
                                        WITH ( numValue NUMERIC,bitCommContact bit)
                            ) X
            EXEC sp_xml_removedocument @hDocItem
        END
        

		DECLARE @bitItemDuplicate AS BIT = 0
		DECLARE @bitOrgDuplicate AS BIT = 0
		DECLARE @bitContactDuplicate AS BIT = 0

		/*Duplicate Rule values checking */
		-- CHECK IF OTHER COMMISSION RULE IS EXISTS IN ACCOUNT WIRH SAME OPTION IN SETP 2,3,4 and 5
		IF (SELECT COUNT(*) FROM CommissionRules WHERE numDomainID=@numDomainID AND tintComAppliesTo=@tintComAppliesTo AND tintComOrgType=@tintComOrgType AND tintAssignTo = @tintAssignTo AND numComRuleID <> @numComRuleID) > 0
		BEGIN
			-- ITEM
			IF @tintComAppliesTo = 1 OR @tintComAppliesTo = 2
			BEGIN
				-- ITEM ID OR ITEM CLASSIFICATION
				IF (SELECT COUNT(*) FROM CommissionRuleItems WHERE numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleItems WHERE numComRuleID=@numComRuleID) AND numComRuleID IN (SELECT ISNULL(numComRuleID,0) FROM CommissionRules WHERE numDomainID=@numDomainID AND numComRuleID <> @numComRuleID)) > 0
				BEGIN
					SET @bitItemDuplicate = 1
				END
			END
			ELSE IF @tintComAppliesTo = 3
			BEGIN
				-- ALL ITEMS
				SET @bitItemDuplicate = 1
			END

			--ORGANIZATION
			IF @tintComOrgType = 1
			BEGIN
				-- ORGANIZATION ID
				IF (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleOrganization WHERE numComRuleID=@numComRuleID) AND numComRuleID IN (SELECT ISNULL(numComRuleID,0) FROM CommissionRules WHERE numDomainID=@numDomainID AND numComRuleID <> @numComRuleID)) > 0
				BEGIN
					SET @bitOrgDuplicate = 1
				END
			END
			ELSE IF @tintComOrgType = 2
			BEGIN
				-- ORGANIZATION RELATIONSHIP OR PROFILE
				IF (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleOrganization WHERE numComRuleID=@numComRuleID) AND numProfile IN (SELECT ISNULL(numProfile,0) FROM CommissionRuleOrganization WHERE numComRuleID=@numComRuleID) AND numComRuleID IN (SELECT ISNULL(numComRuleID,0) FROM CommissionRules WHERE numDomainID=@numDomainID AND numComRuleID <> @numComRuleID)) > 0
				BEGIN
					SET @bitOrgDuplicate = 1
				END
			END
			ELSE IF  @tintComOrgType = 3
			BEGIN
				-- ALL ORGANIZATIONS
				SET @bitOrgDuplicate = 1
			END

			--CONTACT
			IF (
				SELECT 
					COUNT(*) 
				FROM 
					CommissionRuleContacts 
				WHERE 
					numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleContacts WHERE numComRuleID=@numComRuleID) 
					AND numComRuleID IN (SELECT ISNULL(numComRuleID,0) FROM CommissionRules WHERE numDomainID=@numDomainID AND numComRuleID <> @numComRuleID)) > 0
			BEGIN
				SET @bitContactDuplicate = 1
			END
		END

		IF (ISNULL(@bitItemDuplicate,0) = 1 AND ISNULL(@bitOrgDuplicate,0) = 1 AND ISNULL(@bitContactDuplicate,0) = 1)
		BEGIN
			RAISERROR ( 'DUPLICATE',16, 1 )
			RETURN ;
		END

		SELECT  @numComRuleID
    END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageCommissionRuleItems')
DROP PROCEDURE USP_ManageCommissionRuleItems
GO
CREATE PROCEDURE [dbo].[USP_ManageCommissionRuleItems]    
@numComRuleID as numeric(9)=0,    
@tintAppRuleType as tinyint,  
@numValue as numeric(9)=0,  
@numComRuleItemID as numeric(9)=0,  
@byteMode as TINYINT,
@numProfile AS NUMERIC(18,0)
as    
BEGIN TRY
BEGIN TRANSACTION
	IF @byteMode=0  
	BEGIN
		IF @numValue<>0
		BEGIN  
			DECLARE @numDomainID as numeric(9)
			DECLARE @tintAssignTo AS TINYINT
			DECLARE @tintComAppliesTo TINYINT
			DECLARE @tintComOrgType TINYINT

		
			SELECT 
				@numDomainID=numDomainID,
				@tintAssignTo=tintAssignTo,
				@tintComAppliesTo = ISNULL(tintComAppliesTo,0),
				@tintComOrgType = ISNULL(tintComOrgType,0)
			FROM 
				[CommissionRules] 
			WHERE 
				numComRuleID = @numComRuleID

			DECLARE @bitItemDuplicate AS BIT = 0
			DECLARE @bitOrgDuplicate AS BIT = 0
			DECLARE @bitContactDuplicate AS BIT = 0

			/*Duplicate Rule values checking */
			-- CHECK IF OTHER COMMISSION RULE IS EXISTS IN ACCOUNT WIRH SAME OPTION IN SETP 2,3,4 and 5
			IF (SELECT COUNT(*) FROM CommissionRules WHERE numDomainID=@numDomainID AND tintComAppliesTo=@tintComAppliesTo AND tintComOrgType=@tintComOrgType AND tintAssignTo = @tintAssignTo AND numComRuleID <> @numComRuleID) > 0
			BEGIN
				-- ITEM
				IF @tintComAppliesTo = 1 OR @tintComAppliesTo = 2
				BEGIN
					-- ITEM ID OR ITEM CLASSIFICATION
					IF (SELECT COUNT(*) FROM CommissionRuleItems WHERE numValue = @numValue AND numComRuleID IN (SELECT ISNULL(numComRuleID,0) FROM CommissionRules WHERE numDomainID=@numDomainID AND numComRuleID <> @numComRuleID)) > 0
					BEGIN
						SET @bitItemDuplicate = 1
					END
				END
				ELSE IF @tintComAppliesTo = 3
				BEGIN
					-- ALL ITEMS
					SET @bitItemDuplicate = 1
				END

				--ORGANIZATION
				IF @tintComOrgType = 1
				BEGIN
					-- ORGANIZATION ID
					IF (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numValue = @numValue AND numComRuleID IN (SELECT ISNULL(numComRuleID,0) FROM CommissionRules WHERE numDomainID=@numDomainID AND numComRuleID <> @numComRuleID)) > 0
					BEGIN
						SET @bitOrgDuplicate = 1
					END
				END
				ELSE IF @tintComOrgType = 2
				BEGIN
					-- ORGANIZATION RELATIONSHIP OR PROFILE
					IF (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numValue=@numValue AND numProfile = @numProfile AND numComRuleID IN (SELECT ISNULL(numComRuleID,0) FROM CommissionRules WHERE numDomainID=@numDomainID AND numComRuleID <> @numComRuleID)) > 0
					BEGIN
						SET @bitOrgDuplicate = 1
					END
				END
				ELSE IF  @tintComOrgType = 3
				BEGIN
					-- ALL ORGANIZATIONS
					SET @bitOrgDuplicate = 1
				END

				--CONTACT
				IF (
					SELECT 
						COUNT(*) 
					FROM 
						CommissionRuleContacts 
					WHERE  
						numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleContacts WHERE numComRuleID=@numComRuleID) 
						AND numComRuleID IN (SELECT ISNULL(numComRuleID,0) FROM CommissionRules WHERE numDomainID=@numDomainID AND numComRuleID <> @numComRuleID)) > 0
				BEGIN
					SET @bitContactDuplicate = 1
				END
			END

			IF (ISNULL(@bitItemDuplicate,0) = 1 AND ISNULL(@bitOrgDuplicate,0) = 1 AND ISNULL(@bitContactDuplicate,0) = 1)
			BEGIN
				RAISERROR ( 'DUPLICATE',16, 1 )
				RETURN ;
			END
				
			IF NOT EXISTS(SELECT * FROM CommissionRuleItems WHERE numComRuleID=@numComRuleID and numValue=@numValue and tintType=@tintAppRuleType)
			BEGIN
				IF @tintAppRuleType > 2
				BEGIN
					IF @tintAppRuleType=3 
					BEGIN
						DELETE FROM CommissionRuleOrganization WHERE numComRuleID=@numComRuleID AND tintType = 4
						UPDATE CommissionRules SET tintComOrgType = 1 WHERE [numComRuleID]=@numComRuleID
						INSERT INTO CommissionRuleOrganization (numComRuleID,numValue,numProfile,tintType)  values(@numComRuleID,@numValue,0,@tintAppRuleType)
					END
					ELSE IF @tintAppRuleType=4 
					BEGIN
						DELETE FROM CommissionRuleOrganization WHERE numComRuleID=@numComRuleID AND tintType = 3
						UPDATE CommissionRules SET tintComOrgType = 2 WHERE [numComRuleID]=@numComRuleID
						INSERT INTO CommissionRuleOrganization (numComRuleID,numValue,numProfile,tintType)  values(@numComRuleID,@numValue,@numProfile,@tintAppRuleType)
					END
				END
				ELSE
				BEGIN
					IF @tintAppRuleType=1 
					BEGIN
						delete from CommissionRuleItems where tintType = 2 and numComRuleID=@numComRuleID
						UPDATE [CommissionRules] SET [tintComAppliesTo] = 1 WHERE [numComRuleID]=@numComRuleID
					END
					ELSE IF @tintAppRuleType=2 
						BEGIN
							delete from CommissionRuleItems where tintType = 1 and numComRuleID=@numComRuleID
							UPDATE [CommissionRules] SET [tintComAppliesTo] = 2 WHERE [numComRuleID]=@numComRuleID
						END
			
					INSERT INTO [CommissionRuleItems] (numComRuleID,numValue,tintType)	VALUES (@numComRuleID,@numValue,@tintAppRuleType)
				END
			END
		END
	END  
	ELSE IF @byteMode=1  
	BEGIN  
		IF (@tintAppRuleType=1 OR @tintAppRuleType=2)
			DELETE FROM [CommissionRuleItems] WHERE numComRuleItemID = @numComRuleItemID
		ELSE 
			DELETE FROM [CommissionRuleOrganization] WHERE numComRuleOrgID = @numComRuleItemID
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManagePayrollHeader')
DROP PROCEDURE USP_ManagePayrollHeader
GO
CREATE PROCEDURE [dbo].[USP_ManagePayrollHeader]
    @numDomainId numeric(18, 0),
    @numUserCntID numeric(18, 0),
    @numPayrollHeaderID numeric(18, 0) OUTPUT,
    @numPayrolllReferenceNo numeric(18, 0),
    @dtFromDate datetime,
    @dtToDate datetime,
    @dtPaydate DATETIME,
    @strItems TEXT,
    @tintMode TINYINT=0,
    @ClientTimeZoneOffset INT,
    @numPayrollStatus INT
AS
BEGIN

	IF(SELECT COUNT(*) FROM dbo.PayrollHeader WHERE numPayrolllReferenceNo = @numPayrolllReferenceNo AND numDomainId = @numDomainId AND numPayrollHeaderID <> @numPayrollHeaderID)>0
	BEGIN
 		RAISERROR ( 'DUPLICATE',16, 1 )
 		RETURN ;
	END
	 
	IF @numPayrollHeaderID=0
	BEGIN
		
		INSERT INTO [dbo].[PayrollHeader] ([numPayrolllReferenceNo], [numDomainId], [dtFromDate], [dtToDate], [dtPaydate], [numCreatedBy], [dtCreatedDate], numPayrollStatus )
		SELECT @numPayrolllReferenceNo, @numDomainId, @dtFromDate, @dtToDate, @dtPaydate, @numUserCntID, GETUTCDATE(),@numPayrollStatus
		
		SET @numPayrollHeaderID=SCOPE_IDENTITY()
	END
	ELSE
	BEGIN
		UPDATE [dbo].[PayrollHeader]
		SET    [numPayrolllReferenceNo] = @numPayrolllReferenceNo, [dtFromDate] = @dtFromDate, [dtToDate] = @dtToDate, [dtPaydate] = @dtPaydate, [numModifiedBy] = @numUserCntID, [dtModifiedDate] = GETUTCDATE(), numPayrollStatus = @numPayrollStatus
		WHERE  [numPayrollHeaderID] = @numPayrollHeaderID AND [numDomainId] = @numDomainId
		
		
			DECLARE @hDocItem INT
				IF CONVERT(VARCHAR(10), @strItems) <> '' 
				BEGIN
					EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
					
					SELECT  numUserCntID,decRegularHrs,decOvertimeHrs,decPaidLeaveHrs,monCommissionAmt,
					 monExpenses,monReimbursableExpenses,monDeductions,monTotalAmt INTO #temp
								FROM    ( SELECT    * FROM      OPENXML (@hDocItem, '/NewDataSet/Table1', 2)
										WITH ( numUserCntID NUMERIC,decRegularHrs FLOAT,decOvertimeHrs FLOAT,
										decPaidLeaveHrs FLOAT,monCommissionAmt MONEY,monExpenses MONEY,
										monReimbursableExpenses MONEY,monDeductions MONEY,monTotalAmt MONEY)
								) X
					
					 EXEC sp_xml_removedocument @hDocItem
					 
					 UPDATE PD SET decRegularHrs=T.decRegularHrs,decOvertimeHrs=T.decOvertimeHrs,
						decPaidLeaveHrs=T.decPaidLeaveHrs,monCommissionAmt=T.monCommissionAmt,
						monExpenses=T.monExpenses,monReimbursableExpenses=T.monReimbursableExpenses,
						monDeductions=T.monDeductions,monTotalAmt=T.monTotalAmt
					 FROM PayrollDetail PD JOIN #temp T ON PD.numUserCntID=T.numUserCntID
					 WHERE PD.numDomainId=@numDomainId AND PD.numPayrollHeaderID=@numPayrollHeaderID
					 
					 INSERT INTO PayrollDetail([numPayrollHeaderID],[numDomainId],[numUserCntID]
							,[monHourlyRate],[bitOverTime],[bitOverTimeHrsDailyOrWeekly],[numLimDailHrs]
							,[monOverTimeRate],[decRegularHrs],[decOvertimeHrs],[decPaidLeaveHrs]
							,[monCommissionAmt],[monExpenses],[monReimbursableExpenses],[monDeductions],[monTotalAmt],numCheckStatus)
					 SELECT @numPayrollHeaderID,@numDomainId,T.numUserCntID,
							monHourlyRate,bitOverTime,bitOverTimeHrsDailyOrWeekly,numLimDailHrs,monOverTimeRate,
							T.decRegularHrs,T.decOvertimeHrs,T.decPaidLeaveHrs,T.monCommissionAmt,
							T.monExpenses,T.monReimbursableExpenses,T.monDeductions,T.monTotalAmt,0
					 FROM #temp T JOIN UserMaster UM ON T.numUserCntID=UM.numUserDetailId 
						WHERE UM.numDomainId=@numDomainId AND T.numUserCntID NOT IN (SELECT numUserCntID FROM PayrollDetail WHERE numDomainId=@numDomainId AND numPayrollHeaderID=@numPayrollHeaderID)
					 
					--LOGIC OF COMMISSION TYPE IS REMOVED BECAUSE WE hAVE REMOVES PROJECT COMMISSION(tintCommissionType=3) OPTION FROM GLOBAL SETTINGS
					IF @tintMode!=4
					BEGIN
						DELETE FROM PayrollTracking WHERE numPayrollHeaderID=@numPayrollHeaderID
						AND ISNULL(numComissionID,0)>0
						AND numPayrollDetailID IN (SELECT numPayrollDetailID FROM PayrollDetail PD JOIN #temp T ON PD.numUserCntID=T.numUserCntID
						WHERE PD.numDomainId=@numDomainId AND PD.numPayrollHeaderID=@numPayrollHeaderID)  				 	
							
						IF @tintMode=1 --Only Paid
						BEGIN
							INSERT INTO PayrollTracking (numDomainId,numPayrollHeaderID,numPayrollDetailID,numComissionID,monCommissionAmt)
							SELECT @numDomainId,@numPayrollHeaderID,numPayrollDetailID,numComissionID,monCommissionAmt
							FROM 
							(SELECT PD.numPayrollDetailID,numComissionID,
								BC.numComissionAmount - ISNULL((SELECT SUM(ISNULL(monCommissionAmt,0))
								FROM PayrollTracking WHERE ISNULL(BC.numComissionID,0)=ISNULL(numComissionID,0) AND numPayrollHeaderID!=@numPayrollHeaderID),0) AS monCommissionAmt
								FROM PayrollDetail PD JOIN #temp T ON PD.numUserCntID=T.numUserCntID
									INNER JOIN BizDocComission BC ON BC.numUserCntId=PD.numUserCntID
									INNER JOIN OpportunityBizDocs oppBiz on BC.numOppBizDocId=oppBiz.numOppBizDocsId 
									INNER JOIN OpportunityMaster Opp ON oppBiz.numOppId=opp.numOppId 
									WHERE PD.numDomainId=@numDomainId AND PD.numPayrollHeaderID=@numPayrollHeaderID AND
									Opp.numDomainId=@numDomainId AND BC.bitCommisionPaid=0 
									AND oppBiz.bitAuthoritativeBizDocs = 1 AND isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount
									AND ((dateadd(minute,-@ClientTimeZoneOffset,oppBiz.dtCreatedDate) between @dtFromDate And @dtToDate))) T
									WHERE monCommissionAmt>0
						END
						ELSE IF @tintMode=2 --Only UNPaid
						BEGIN
							INSERT INTO PayrollTracking (numDomainId,numPayrollHeaderID,numPayrollDetailID,numComissionID,monCommissionAmt)
							SELECT @numDomainId,@numPayrollHeaderID,numPayrollDetailID,numComissionID,monCommissionAmt
							FROM 
							(SELECT PD.numPayrollDetailID,numComissionID,
								BC.numComissionAmount - ISNULL((SELECT SUM(ISNULL(monCommissionAmt,0))
								FROM PayrollTracking WHERE ISNULL(BC.numComissionID,0)=ISNULL(numComissionID,0) AND numPayrollHeaderID!=@numPayrollHeaderID),0) AS monCommissionAmt
								FROM PayrollDetail PD JOIN #temp T ON PD.numUserCntID=T.numUserCntID
									INNER JOIN BizDocComission BC ON BC.numUserCntId=PD.numUserCntID
									INNER JOIN OpportunityBizDocs oppBiz on BC.numOppBizDocId=oppBiz.numOppBizDocsId 
									INNER JOIN OpportunityMaster Opp ON oppBiz.numOppId=opp.numOppId 
									WHERE PD.numDomainId=@numDomainId AND PD.numPayrollHeaderID=@numPayrollHeaderID AND
									Opp.numDomainId=@numDomainId AND BC.bitCommisionPaid=0 
									AND oppBiz.bitAuthoritativeBizDocs = 1 AND isnull(oppBiz.monAmountPaid,0) <oppBiz.monDealAmount
									AND ((dateadd(minute,-@ClientTimeZoneOffset,oppBiz.dtCreatedDate) between @dtFromDate And @dtToDate))) T
									WHERE monCommissionAmt>0
						END
						ELSE IF @tintMode=3 --Both Paid & UNPaid
						BEGIN
							INSERT INTO PayrollTracking (numDomainId,numPayrollHeaderID,numPayrollDetailID,numComissionID,monCommissionAmt)
								SELECT @numDomainId,@numPayrollHeaderID,numPayrollDetailID,numComissionID,monCommissionAmt
							FROM 
							(SELECT PD.numPayrollDetailID,numComissionID,
								BC.numComissionAmount - ISNULL((SELECT SUM(ISNULL(monCommissionAmt,0))
								FROM PayrollTracking WHERE ISNULL(BC.numComissionID,0)=ISNULL(numComissionID,0) AND numPayrollHeaderID!=@numPayrollHeaderID),0) AS monCommissionAmt
								FROM PayrollDetail PD JOIN #temp T ON PD.numUserCntID=T.numUserCntID
									INNER JOIN BizDocComission BC ON BC.numUserCntId=PD.numUserCntID
									INNER JOIN OpportunityBizDocs oppBiz on BC.numOppBizDocId=oppBiz.numOppBizDocsId 
									INNER JOIN OpportunityMaster Opp ON oppBiz.numOppId=opp.numOppId 
									WHERE PD.numDomainId=@numDomainId AND PD.numPayrollHeaderID=@numPayrollHeaderID AND
									Opp.numDomainId=@numDomainId AND BC.bitCommisionPaid=0 
									AND oppBiz.bitAuthoritativeBizDocs = 1 
									AND ((dateadd(minute,-@ClientTimeZoneOffset,oppBiz.dtCreatedDate) between @dtFromDate And @dtToDate))) T
									WHERE monCommissionAmt>0
						END
					END
					 
					 
					 DROP TABLE #temp
				END
	 END
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageWorkOrder')
DROP PROCEDURE USP_ManageWorkOrder
GO
CREATE PROCEDURE [dbo].[USP_ManageWorkOrder]
@numItemCode AS NUMERIC(18,0),
@numWareHouseItemID AS NUMERIC(18,0),
@Units AS INT,
@numDomainID AS NUMERIC(18,0),
@numUserCntID AS NUMERIC(18,0),
@vcInstruction AS VARCHAR(1000),
@bintCompliationDate AS DATETIME,
@numAssignedTo AS NUMERIC(9)
AS
BEGIN
BEGIN TRY
BEGIN TRAN
	DECLARE @numWOId AS NUMERIC(9),@numDivisionId AS NUMERIC(9) 
	DECLARE @txtItemDesc AS varchar(1000),@vcItemName AS VARCHAR(300)
	
	SELECT @txtItemDesc=ISNULL(txtItemDesc,''),@vcItemName=ISNULL(vcItemName,'') FROM Item WHERE numItemCode=@numItemCode

	INSERT INTO WorkOrder
	(
		numItemCode,numQtyItemsReq,numWareHouseItemId,numCreatedBy,
		bintCreatedDate,numDomainID,numWOStatus,vcItemDesc,vcInstruction,
		bintCompliationDate,numAssignedTo
	)
	VALUES
	(
		@numItemCode,@Units,@numWareHouseItemID,@numUserCntID,
		getutcdate(),@numDomainID,0,@txtItemDesc,@vcInstruction,
		@bintCompliationDate,@numAssignedTo
	)
	
	SET @numWOId=@@IDENTITY

	IF @numAssignedTo>0
	BEGIN

		SET @vcItemName= 'Work Order for Item : ' + @vcItemName
		SELECT @numDivisionId=numDivisionId FROM AdditionalContactsInformation WHERE numContactId=@numAssignedTo

		EXEC dbo.usp_InsertCommunication
		@numCommId = 0, --  numeric(18, 0)
		@bitTask = 972, --  numeric(18, 0)
		@numContactId = @numUserCntID, --  numeric(18, 0)
		@numDivisionId = @numDivisionId, --  numeric(18, 0)
		@txtDetails = @vcItemName, --  text
		@numOppId = 0, --  numeric(18, 0)
		@numAssigned = @numAssignedTo, --  numeric(18, 0)
		@numUserCntID = @numUserCntID, --  numeric(18, 0)
		@numDomainId = @numDomainID, --  numeric(18, 0)
		@bitClosed = 0, --  tinyint
		@vcCalendarName = '', --  varchar(100)
		@dtStartTime = @bintCompliationDate, --  datetime
		@dtEndtime = @bintCompliationDate, --  datetime
		@numActivity = 0, --  numeric(9, 0)
		@numStatus = 0, --  numeric(9, 0)
		@intSnoozeMins = 0, --  int
		@tintSnoozeStatus = 0, --  tinyint
		@intRemainderMins = 0, --  int
		@tintRemStaus = 0, --  tinyint
		@ClientTimeZoneOffset = 0, --  int
		@bitOutLook = 0, --  tinyint
		@bitSendEmailTemplate = 0, --  bit
		@bitAlert = 0, --  bit
		@numEmailTemplate = 0, --  numeric(9, 0)
		@tintHours = 0, --  tinyint
		@CaseID = 0, --  numeric(18, 0)
		@CaseTimeId = 0, --  numeric(18, 0)
		@CaseExpId = 0, --  numeric(18, 0)
		@ActivityId = 0, --  numeric(18, 0)
		@bitFollowUpAnyTime = 0, --  bit
		@strAttendee=''
END
	
	INSERT INTO [WorkOrderDetails] 
	(
		numWOId,numItemKitID,numChildItemID,numQtyItemsReq,numWareHouseItemId,vcItemDesc,sintOrder,numQtyItemsReq_Orig,numUOMId
	)
	SELECT 
		@numWOId,numItemKitID,numItemCode,
		CAST((DTL.numQtyItemsReq * @Units)AS NUMERIC(9,0)),
		isnull(Dtl.numWareHouseItemId,0),
		ISNULL(Dtl.vcItemDesc,txtItemDesc),
		ISNULL(sintOrder,0),
		DTL.numQtyItemsReq,
		Dtl.numUOMId 
	FROM 
		item                                
	INNER JOIN 
		ItemDetails Dtl 
	ON 
		numChildItemID=numItemCode
	WHERE 
		numItemKitID=@numItemCode

	--UPDATE ON ORDER OF ASSEMBLY
	UPDATE 
		WareHouseItems
	SET    
		numOnOrder= ISNULL(numOnOrder,0) + @Units,
		dtModified = GETDATE() 
	WHERE   
		numWareHouseItemID = @numWareHouseItemID 
	
	--MAKE ENTRY IN WAREHOUSE TRACKING TABLE
	DECLARE @Description AS VARCHAR(1000)
	SET @Description='Work Order Created (Qty:' + CAST(@Units AS VARCHAR(10)) + ')'

	EXEC dbo.USP_ManageWareHouseItems_Tracking
	@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
	@numReferenceID = @numWOId, --  numeric(9, 0)
	@tintRefType = 2, --  tinyint
	@vcDescription = @Description, --  varchar(100)
	@numModifiedBy = @numUserCntID,
	@numDomainID = @numDomainID

	EXEC USP_ManageInventoryWorkOrder @numWOId,@numDomainID,@numUserCntID
COMMIT TRAN 
END TRY 
BEGIN CATCH		
    IF ( @@TRANCOUNT > 0 ) 
    BEGIN
        ROLLBACK TRAN
    END

	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH
END
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
--2 changes
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount money =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(9),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0
)                                                                          
                                                                          
as  
BEGIN TRY
BEGIN TRANSACTION
 DECLARE @fltExchangeRate float                                 
 DECLARE @hDocItem int                                                                                                                            
 DECLARE @TotalAmount as money                                                                          
 DECLARE @tintOppStatus as tinyint               
 DECLARE @tintDefaultClassType NUMERIC
 DECLARE @numDefaultClassID NUMERIC;SET @numDefaultClassID=0
 
 --Get Default Item Class for particular user based on Domain settings  
 SELECT @tintDefaultClassType=isnull(tintDefaultClassType,0) FROM dbo.Domain  WHERE numDomainID = @numDomainID 

 IF @tintDefaultClassType=0
      SELECT @numDefaultClassID=NULLIF(numDefaultClass,0) FROM dbo.UserMaster  WHERE numUserDetailId =@numUserCntID AND numDomainID = @numDomainID 


--If new Oppertunity
if @numOppID = 0                                                                          
 begin     
  declare @intOppTcode as numeric(9)                           
  Select @intOppTcode=max(numOppId)from OpportunityMaster                
  set @intOppTcode =isnull(@intOppTcode,0) + 1                                                
  set @vcPOppName=@vcPOppName+ '-'+convert(varchar(4),year(getutcdate()))  +'-A'+ convert(varchar(10),@intOppTcode)                                  
  
  IF ISNULL(@numCurrencyID,0) = 0 
	BEGIN
	 SET @fltExchangeRate=1
	 SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
	END
  else set @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
                                                                       
  --Set Default Class If enable User Level Class Accountng 
  DECLARE @numAccountClass AS NUMERIC(18);SET @numAccountClass=0
  SELECT @numAccountClass=ISNULL(numDefaultClass,0) FROM dbo.UserMaster UM JOIN dbo.Domain D ON UM.numDomainID=D.numDomainId
  WHERE D.numDomainId=@numDomainId AND UM.numUserDetailId=@numUserCntID AND ISNULL(D.IsEnableUserLevelClassTracking,0)=1
                                                                       
  insert into OpportunityMaster                                                                          
  (                                                                             
  numContactId,                                                                   
  numDivisionId,                                                                          
  txtComments,                                             
  numCampainID,                                                                          
  bitPublicFlag,                                                                          
  tintSource,
  tintSourceType,                                                             
  vcPOppName,                                                                          
  intPEstimatedCloseDate,                                                                          
  monPAmount,                                                                           
  numCreatedBy,                                                                          
  bintCreatedDate,                                                                           
  numModifiedBy,                                                                          
  bintModifiedDate,                                                                          
  numDomainId,                                                                                                             
  numRecOwner,                                                                          
  lngPConclAnalysis,                                                                         
  tintOppType,                                                              
  numSalesOrPurType,
  numCurrencyID,
  fltExchangeRate,
  numAssignedTo,
  numAssignedBy,
  [tintOppStatus],
  numStatus,
  vcOppRefOrderNo,
  --vcWebApiOrderNo,
  bitStockTransfer,bitBillingTerms,intBillingDays,bitInterestType,fltInterest,vcCouponCode,
  bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,vcMarketplaceOrderReportId,
  numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
  bitUseMarkupShippingRate,
  numMarkupShippingRate,
  intUsedShippingCompany
  )                                                                          
  Values                                                                          
  (                                                                          
  @numContactId,                                                                          
  @numDivisionId,                                                                          
  @Comments,                           
  @CampaignID,                                                                          
  @bitPublicFlag,                     
  @tintSource,
  @tintSourceType,                         
  @vcPOppName,                                                                          
  @dtEstimatedCloseDate,                                                                          
  @monPAmount,                                                                                
  @numUserCntID,                                                                          
 CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END,                                                                          
  @numUserCntID,                                                           
  getutcdate(),                                                                          
  @numDomainId,                                                                                   
  @numUserCntID,                                                                          
  @lngPConclAnalysis,                                                                                                                                                                      
  @tintOppType,                                                                                                                                      
  @numSalesOrPurType,              
  @numCurrencyID, 
  @fltExchangeRate,
  case when @numAssignedTo>0 then @numAssignedTo else null end,
  case when @numAssignedTo>0 then @numUserCntID else null   END,
  @DealStatus,
  @numStatus,
  @vcOppRefOrderNo,
 -- @vcWebApiOrderNo,
  @bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,@vcCouponCode,
  @bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,
  @numPercentageComplete,@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,
  @bitUseMarkupShippingRate,
  @numMarkupShippingRate,
  @intUsedShippingCompany
    )                                                                                                                      
  set @numOppID=scope_identity()                                                
  
  --Update OppName as per Name Template
  EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

  --Map Custom Field	
  DECLARE @tintPageID AS TINYINT;
  SET @tintPageID=CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
  EXEC dbo.USP_AddParentChildCustomFieldMap
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@numRecordID = @numOppID, --  numeric(18, 0)
	@numParentRecId = @numDivisionId, --  numeric(18, 0)
	@tintPageID = @tintPageID --  tinyint
 	
  
	IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
	   BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
	   END
  ---- inserting Items                                                                          
   
                                                   
  if convert(varchar(10),@strItems) <>'' AND @numOppID>0
  begin                      
   EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
  insert into                       
   OpportunityItems                                                                          
   (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,
   numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,monAvgCost,bitItemPriceApprovalRequired)
   select @numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,
   x.monPrice,x.monTotAmount,X.vcItemDesc,
   CASE X.numWarehouseItmsID WHEN -1 
							 THEN (SELECT [numWareHouseItemID] FROM [WareHouseItems] 
								   WHERE [numItemID] = X.numItemCode 
								   AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								   AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail 
														  WHERE numDomainID =@numDomainId 
														  AND WebApiId = @WebApiId))  
							 WHEN 0 
							 THEN (SELECT TOP 1 [numWareHouseItemID] FROM [WareHouseItems] 
								   WHERE [numItemID] = X.numItemCode
								   AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')) 
							 ELSE  X.numWarehouseItmsID 
	END AS numWarehouseItmsID,
	X.ItemType,X.DropShip,X.bitDiscountType,
   X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,
   (SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
   (SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
   (SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
   (select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID),
   X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,Case When @tintDefaultClassType=0 then @numDefaultClassID else (SELECT numItemClass FROM item WHERE numItemCode = X.numItemCode) end,X.numToWarehouseItemID,X.Attributes,
   (SELECT ISNULL(monAverageCost,0) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),X.bitItemPriceApprovalRequired from(
   SELECT *FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
   WITH                       
   (                                                                          
   numoppitemtCode numeric(9),                                     
   numItemCode numeric(9),                                                                          
   numUnitHour numeric(9,2),                                                                          
   monPrice money,                                                                       
   monTotAmount money,                                                                          
   vcItemDesc varchar(1000),                                    
   numWarehouseItmsID numeric(9),                          
   ItemType varchar(30),      
   DropShip bit,
   bitDiscountType bit,
   fltDiscount float,
   monTotAmtBefDiscount MONEY,
   vcItemName varchar(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9) ,numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(200),vcSKU VARCHAR(100),bitItemPriceApprovalRequired BIT
   ))X    
   ORDER BY 
   X.numoppitemtCode
    
    
   Update OpportunityItems                       
   set numItemCode=X.numItemCode,    
   numOppId=@numOppID,                     
   numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
   monPrice=x.monPrice,                      
   monTotAmount=x.monTotAmount,                                  
   vcItemDesc=X.vcItemDesc,                      
   numWarehouseItmsID=X.numWarehouseItmsID,                 
   bitDropShip=X.DropShip,
   bitDiscountType=X.bitDiscountType,
   fltDiscount=X.fltDiscount,
   monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,bitWorkOrder=X.bitWorkOrder,
   numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,
vcAttributes=X.Attributes,numClassID=(Case When @tintDefaultClassType=0 then @numDefaultClassID else (SELECT numItemClass FROM item WHERE numItemCode = X.numItemCode) END),bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired
--   ,vcModelID=(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),vcManufacturer=(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode)
--  ,vcPathForTImage=(SELECT vcPathForTImage FROM item WHERE numItemCode = X.numItemCode),monVendorCost=(select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID)
   from (SELECT numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,
   numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,Attributes,bitItemPriceApprovalRequired
   FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
   WITH  (                      
    numoppitemtCode numeric(9),                                     
    numItemCode numeric(9),                                                                          
    numUnitHour numeric(9,2),                                                                          
    monPrice money,                                         
    monTotAmount money,                                                                          
    vcItemDesc varchar(1000),                                    
    numWarehouseItmsID numeric(9),      
    DropShip bit,
	bitDiscountType bit,
    fltDiscount float,
    monTotAmtBefDiscount MONEY,vcItemName varchar(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(200),bitItemPriceApprovalRequired BIT
   ))X where numoppitemtCode=X.numOppItemID                                          
	
	
   -- Update UOM of opportunity items have not UOM set while exported from Marketplace (means tintSourceType = 3)
	IF ISNULL(@tintSourceType,0) = 3
	BEGIN
		--SELECT * FROM OpportunityMaster WHERE numOppId = 81654
		--SELECT * FROM OpportunityItems WHERE numOppId = 81654
		--SELECT * FROM Item WHERE numItemCode = 822078

		UPDATE OI SET numUOMID = ISNULL(I.numSaleUnit,0), 
					  numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
		FROM OpportunityItems OI
		JOIN Item I ON OI.numItemCode = I.numItemCode
		WHERE numOppId = @numOppID
		AND I.numDomainID = @numDomainId

	END	
   --Update OpportunityItems
   --set 
   --FROM (SELECT numItemCode,vcItemName,[txtItemDesc],vcModelID FROM item WHERE numItemCode IN (SELECT [numItemCode] FROM OpportunityItems WHERE numOppID = @numOppID))X
   --WHERE OpportunityItems.[numItemCode] = X.numItemCode 
                                     
   insert into OppWarehouseSerializedItem                                                                          
   (numOppID,numWarehouseItmsDTLID,numOppItemID,numWarehouseItmsID)                      
   select @numOppID,X.numWarehouseItmsDTLID,X.numoppitemtCode,X.numWItmsID from(                                                                          
   SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
   WITH                        
   (                                               
   numWarehouseItmsDTLID numeric(9),                      
   numoppitemtCode numeric(9),                      
   numWItmsID numeric(9)                                                     
   ))X                                    
               
                                 
   update OppWarehouseSerializedItem                       
   set numOppItemID=X.numoppitemtCode                                
   from                       
   (SELECT numWarehouseItmsDTLID as DTLID,O.numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
   WITH(numWarehouseItmsDTLID numeric(9),numoppitemtCode numeric(9),numWItmsID numeric(9))                                
   join OpportunityItems O on O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID)X                 
   where numOppID=@numOppID and numWarehouseItmsDTLID=X.DTLID      
    
--   insert into OpportunityKitItems(numOppKitItem,numChildItem,intQuantity,numWareHouseItemId)    
--   SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/ChildItems',2)                                                                          
--   WITH                        
--   (                                               
--   numoppitemtCode numeric(9),                      
--   numItemCode numeric(9),    
--   numQtyItemsReq integer,  
--   numWarehouseItmsID numeric(9)                                                       
--   )    
      
    
    
    
	EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
    
                              
                          
   EXEC sp_xml_removedocument @hDocItem                                     
        end          
   set @TotalAmount=0                                                           
   select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@numOppID                                                                          
  update OpportunityMaster set  monPamount=@TotalAmount                                                          
  where numOppId=@numOppID                                                 
                      
     
--Insert Tax for Division   

IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN                   
	INSERT dbo.OpportunityMasterTaxItems (
		numOppId,
		numTaxItemID,
		fltPercentage
	) SELECT @numOppID,TI.numTaxItemID,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL) FROM TaxItems TI JOIN DivisionTaxTypes DTT ON TI.numTaxItemID = DTT.numTaxItemID
	 WHERE DTT.numDivisionID=@numDivisionId AND DTT.bitApplicable=1
	   union 
	  select @numOppID,0,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL) 
	  FROM dbo.DivisionMaster WHERE bitNoTax=0 AND numDivisionID=@numDivisionID
END
-- Archieve item based on Item's individual setting
--	UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT * FROM dbo.OpportunityItems WHERE OpportunityItems.numItemCode = I.numItemCode) AND ISNULL(I.bitArchiveItem,0) = 1 
--									  THEN 1 
--									  ELSE 0 
--									  END 
--	FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode
	
 END
 
 else                                                                                                                          
 BEGIN                  
	--Declaration
	 DECLARE @tempAssignedTo AS NUMERIC(9)                                                    
	 SET @tempAssignedTo = NULL 
	 
	 SELECT @tintOppStatus = tintOppStatus,@tempAssignedTo=isnull(numAssignedTo,0) FROM   OpportunityMaster WHERE  numOppID = @numOppID

	 IF @tempAssignedTo <> @numAssignedTo
	 BEGIN
		IF (SELECT COUNT(*) FROM BizDocComission WHERE ISNULL(numOppID,0) = @numOppID AND ISNULL(bitCommisionPaid,0) = 1) > 0
		BEGIN
			RAISERROR('COMMISSION_PAID_AGAINST_BIZDOC',16,1)
		END
	 END
 
	--Reverting back the warehouse items                  
	 IF @tintOppStatus = 1 
		BEGIN        
		PRINT 'inside revert'          
			EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID                  
		END                  
	-- Update Master table
	
	   IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
	   BEGIN
	   		UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
	   END
		
	   IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
	   BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
	   END 
						
	   UPDATE   OpportunityMaster
	   SET      vcPOppName = @vcPOppName,
				txtComments = @Comments,
				bitPublicFlag = @bitPublicFlag,
				numCampainID = @CampaignID,
				tintSource = @tintSource,
				tintSourceType=@tintSourceType,
				intPEstimatedCloseDate = @dtEstimatedCloseDate,
				numModifiedBy = @numUserCntID,
				bintModifiedDate = GETUTCDATE(),
				lngPConclAnalysis = @lngPConclAnalysis,
				monPAmount = @monPAmount,
				tintActive = @tintActive,
				numSalesOrPurType = @numSalesOrPurType,
				numStatus = @numStatus,
				tintOppStatus = @DealStatus,
				vcOppRefOrderNo=@vcOppRefOrderNo,
				--vcWebApiOrderNo = @vcWebApiOrderNo,
				bintOppToOrder=(Case when @tintOppStatus=0 and @DealStatus=1 then GETUTCDATE() else bintOppToOrder end),
				bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,bitBillingTerms=@bitBillingTerms,
				intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,
				tintTaxOperator=@tintTaxOperator,numDiscountAcntType=@numDiscountAcntType,vcCouponCode = @vcCouponCode,
				numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
				bitUseMarkupShippingRate = @bitUseMarkupShippingRate,
				numMarkupShippingRate = @numMarkupShippingRate,
				intUsedShippingCompany = @intUsedShippingCompany
	   WHERE    numOppId = @numOppID   
	   
	---Updating if organization is assigned to someone                                                      
	   IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN                                   
			UPDATE  OpportunityMaster SET     numAssignedTo = @numAssignedTo, numAssignedBy = @numUserCntID WHERE   numOppId = @numOppID                                                    
		END                                                     
	   ELSE 
	   IF ( @numAssignedTo = 0 ) 
		BEGIN                
			UPDATE  OpportunityMaster SET     numAssignedTo = 0, numAssignedBy = 0 WHERE   numOppId = @numOppID
		END                                                                      
		---- Updating Opp Items
		if convert(varchar(10),@strItems) <>'' AND @numOppID>0                                                                         
		begin
			   EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   --Delete Items
			   delete from OpportunityKitItems where numOppID=@numOppID and numOppItemID not in                       
			   (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
			   WITH  (numoppitemtCode numeric(9)))                      
                                        
--               ---- ADDED BY Manish Anjara : Jun 26,2013 - Archive item based on Item's individual setting
--               UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT * FROM dbo.OpportunityItems WHERE OpportunityItems.numItemCode = I.numItemCode) AND ISNULL(I.bitArchiveItem,0) = 1 
--												 THEN 1 
--												 ELSE 0 
--												 END 
--			   FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode
--					 	
--			   UPDATE Item SET IsArchieve = CASE WHEN ISNULL(I.bitArchiveItem,0) = 1 THEN 0 ELSE 1 END
--			   FROM Item I
--			   JOIN OpportunityItems OI ON I.numItemCode = OI.numItemCode
--			   WHERE numOppID = @numOppID and OI.numItemCode NOT IN (SELECT numItemCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) 
--																	 WITH (numItemCode numeric(9)))

			   delete from OpportunityItems where numOppID=@numOppID and numoppitemtCode not in                       
			   (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
			   WITH  (numoppitemtCode numeric(9)))   
	                                
			   insert into OpportunityItems                                                                          
			   (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numToWarehouseItemID,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired)
			   select @numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),(select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID),X.numUOM,X.bitWorkOrder,
			   X.numVendorWareHouse,X.numToWarehouseItemID,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,Case When @tintDefaultClassType=0 then @numDefaultClassID else (SELECT numItemClass FROM item WHERE numItemCode = X.numItemCode) END,
			   (SELECT ISNULL(monAverageCost,0) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired from(
			   SELECT *FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
			   WITH  (                      
				numoppitemtCode numeric(9),                                     
				numItemCode numeric(9),                                                                          
				numUnitHour numeric(9,2),                                                                          
				monPrice money,                                                                       
				monTotAmount money,                                                                          
				vcItemDesc varchar(1000),                                    
				numWarehouseItmsID numeric(9),
				numToWarehouseItemID numeric(9),                   
				Op_Flag tinyint,                            
				ItemType varchar(30),      
				DropShip bit,
				bitDiscountType bit,
				fltDiscount float,
				monTotAmtBefDiscount MONEY,
				vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(200), bitItemPriceApprovalRequired BIT
				 ))X                                     
				--Update items                 
			   Update OpportunityItems                       
			   set numItemCode=X.numItemCode,    
			   numOppId=@numOppID,                       
			   numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
			   monPrice=x.monPrice,                      
			   monTotAmount=x.monTotAmount,                                  
			   vcItemDesc=X.vcItemDesc,                      
			   numWarehouseItmsID=X.numWarehouseItmsID,
			   numToWarehouseItemID = X.numToWarehouseItemID,             
			   bitDropShip=X.DropShip,
			   bitDiscountType=X.bitDiscountType,
			   fltDiscount=X.fltDiscount,
			   monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
			   numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired,bitWorkOrder=X.bitWorkOrder
			   from (SELECT numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,numToWarehouseItemID,DropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,bitItemPriceApprovalRequired,bitWorkOrder  FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
			   WITH  (                      
				numoppitemtCode numeric(9),                                     
				numItemCode numeric(9),                                                                          
				numUnitHour numeric(9,2),                                                                          
				monPrice money,                                         
				monTotAmount money,                                                                          
				vcItemDesc varchar(1000),                                    
				numWarehouseItmsID numeric(9), 
				numToWarehouseItemID NUMERIC(18,0),     
				DropShip bit,
				bitDiscountType bit,
				fltDiscount float,
				monTotAmtBefDiscount MONEY,vcItemName varchar(300),numUOM NUMERIC(9),numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(200),bitItemPriceApprovalRequired BIT, bitWorkOrder BIT                                              
			   ))X where numoppitemtCode=X.numOppItemID                           
			                                    
			   EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			   		                                                                      
			   EXEC sp_xml_removedocument @hDocItem                                               
		end     

		--FOLLOWING DELETE STATEMENT IS COMMENTED BECUASE NOW WE ARE NOT ALLOWING USER TO EIDT SERIAL/LOT ITEM UNTILL USER REMOVED ASSIGNED SERIAL/LOT NUMBERS
	    --Delete previously added serial lot number added for opportunity
		--DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
END

----------------generic section will be called in both insert and update ---------------------------------
-- Archieve item based on Item's individual setting
--	UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT * FROM dbo.OpportunityItems WHERE OpportunityItems.numItemCode = I.numItemCode) AND ISNULL(I.bitArchiveItem,0) = 1 
--									  THEN 1 
--									  ELSE 0 
--									  END 
--	FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode
	
if convert(varchar(10),@strItems) <>'' AND @numOppID>0                                                                        
  begin 
EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

--delete from OpportunityKitItems where numOppID=@numOppID and numOppItemID 
--	not in (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppID=@numOppID)                      
		
--Update Kit Items                 
Update OKI set numQtyItemsReq=numQtyItemsReq_Orig * OI.numUnitHour
			   FROM OpportunityKitItems OKI JOIN OpportunityItems OI ON OKI.numOppItemID=OI.numoppitemtCode and OKI.numOppId=OI.numOppId  
			   WHERE OI.numOppId=@numOppId 

--Insert Kit Items                 
INSERT into OpportunityKitItems                                                                          
		(numOppId,numOppItemID,numChildItemID,numWareHouseItemId,numQtyItemsReq,numQtyItemsReq_Orig,numUOMId,numQtyShipped)
  SELECT @numOppId,OI.numoppitemtCode,ID.numChildItemID,ID.numWareHouseItemId,ID.numQtyItemsReq * OI.numUnitHour,ID.numQtyItemsReq,ID.numUOMId,0
	 FROM OpportunityItems OI JOIN ItemDetails ID ON OI.numItemCode=ID.numItemKitID WHERE 
	OI.numOppId=@numOppId AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID)
--	OI.numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
--			   WITH  (                      
--				numoppitemtCode numeric(9))X)
			   
EXEC sp_xml_removedocument @hDocItem  
END

/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
declare @tintShipped as tinyint               
select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID              
if @tintOppStatus=1               
	begin         
	PRINT 'inside USP_UpdatingInventoryonCloseDeal'
		exec USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
	end              
            
if @tintOppType=1              
begin              
	if @tintOppStatus=1 Update WareHouseItmsDTL set tintStatus=1 where numWareHouseItmsDTLID in (select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppID=@numOppID)
	if @tintShipped=1 Update WareHouseItmsDTL set tintStatus=2 where numWareHouseItmsDTLID in (select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppID=@numOppID)              
END


declare @tintCRMType as numeric(9)        
declare @AccountClosingDate as datetime        

select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

--When deal is won                                         
if @DealStatus = 1 
begin           
	 if @AccountClosingDate is null                   
	  update OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numOppID              
end         
--When Deal is Lost
else if @DealStatus = 2
begin         
	 select @AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
	 if @AccountClosingDate is null                   
	  update OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numOppID
end                    

/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
begin        
	update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end        
-- Promote Lead to Account when Sales/Purchase Order is created against it
ELSE if @tintCRMType=0 AND @DealStatus = 1 --Lead & Order
begin        
	update divisionmaster set tintCRMType=2,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end
--Promote Prospect to Account
else if @tintCRMType=1 AND @DealStatus = 1 
begin        
	update divisionmaster set tintCRMType=2,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9) 
DECLARE @bitIsPrimary BIT;

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

--Bill Address
IF @numBillAddressId>0
BEGIN
 SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numBillAddressId
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName
END
  
  
  --Ship Address
IF @numShipAddressId>0
BEGIN
	SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numShipAddressId
 



  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName
END

--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems (
	numOppId,
	numOppItemID,
	numTaxItemID
) SELECT @numOppId,OI.numoppitemtCode,TI.numTaxItemID FROM dbo.OpportunityItems OI JOIN dbo.ItemTax IT ON OI.numItemCode=IT.numItemCode JOIN
TaxItems TI ON TI.numTaxItemID=IT.numTaxItemID WHERE OI.numOppId=@numOppID AND IT.bitApplicable=1 AND 
OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
  select @numOppId,OI.numoppitemtCode,0 FROM dbo.OpportunityItems OI JOIN dbo.Item I ON OI.numItemCode=I.numItemCode
   WHERE OI.numOppId=@numOppID  AND I.bitTaxable=1 AND
OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

  
 UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppWarehouseSerializedItem_Insert')
DROP PROCEDURE USP_OppWarehouseSerializedItem_Insert
GO
CREATE PROCEDURE [dbo].[USP_OppWarehouseSerializedItem_Insert]
	@numOppID AS NUMERIC(18,0),
	@numOppItemID AS NUMERIC(18,0),
	@numWarehouseItemID AS NUMERIC(18,0),
	@vcSelectedItems AS VARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numWarehouseItmsID=@numWarehouseItemID


	IF LEN(@vcSelectedItems) > 0
	BEGIN
		INSERT INTO OppWarehouseSerializedItem
		(
			numWarehouseItmsDTLID,
			numOppID,
			numOppItemID,
			numWarehouseItmsID,
			numQty
		)
		SELECT 
			ParseName(items,2),
			@numOppID,
			@numOppItemID,
			@numWarehouseItemID,
			ParseName(items,1) 
		FROM 
			dbo.Split(Replace(@vcSelectedItems, '-', '.'),',')
	END
END








GO
/****** Object:  StoredProcedure [dbo].[USP_PayrollLiabilityList]    Script Date: 02/28/2009 13:55:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_payrollliabilitylist')
DROP PROCEDURE usp_payrollliabilitylist
GO
CREATE PROCEDURE [dbo].[USP_PayrollLiabilityList]
(
               @numDomainId          AS NUMERIC(9)  = 0,
               @numDepartmentId      AS NUMERIC(9)  = 0,
               @dtStartDate          AS DATETIME,
               @dtEndDate            AS DATETIME,
               @ClientTimeZoneOffset INT)
AS
BEGIN
	
	-- TEMPORARY TABLE TO HOLD DATA
	CREATE TABLE #tempHrs 
	(
		ID INT IDENTITY(1,1),
		numUserId NUMERIC,
		vcUserName VARCHAR(100),
		vcdepartment VARCHAR(100),
		bitpayroll BIT,
		numUserCntID NUMERIC,
		vcEmployeeId VARCHAR(50),
		monHourlyRate MONEY,
		decRegularHrs decimal(10,2),
		decPaidLeaveHrs decimal(10,2),
		monExpense MONEY,
		monReimburse MONEY,
		monCommPaidInvoice MONEY,
		monCommUNPaidInvoice MONEY,
		monTotalAmountDue MONEY,
		monAmountPaid MONEY, 
		bitCommissionContact BIT
	)

	-- GET EMPLOYEE OF DOMAIN
	INSERT INTO #tempHrs
	(
		numUserId,
		vcUserName,
		vcdepartment,
		bitpayroll,
		numUserCntID,
		vcEmployeeId,
		monHourlyRate,
		bitCommissionContact
	)
	SELECT 
		UM.numUserId,
		Isnull(ADC.vcfirstname + ' ' + adc.vclastname,'-') vcUserName,
		dbo.fn_GetListItemName(adc.vcdepartment),
		Isnull(um.bitpayroll,0) AS bitpayroll,
		adc.numcontactid AS numUserCntID,
		Isnull(um.vcEmployeeId,'') AS vcEmployeeId,
		ISNULL(UM.monHourlyRate,0),
		0
	FROM  
		dbo.UserMaster UM 
	JOIN 
		dbo.AdditionalContactsInformation adc ON adc.numcontactid = um.numuserdetailid
	WHERE 
		UM.numDomainId=@numDomainId AND (adc.vcdepartment=@numDepartmentId OR @numDepartmentId=0)

	-- GET COMMISSION CONTACTS OF DOMAIN
	INSERT INTO #tempHrs 
	(
		numUserId,
		vcUserName,
		numUserCntID,
		vcEmployeeId,
		vcdepartment,
		bitpayroll,
		monHourlyRate,
		bitCommissionContact
	)
	SELECT  
		0,
		ISNULL(A.vcFirstName +' '+ A.vcLastName + '(' + C.vcCompanyName +')','-'),
		A.numContactId,
		'',
		'-',
		0,
		0,
		1
	FROM 
		CommissionContacts CC 
	JOIN DivisionMaster D ON CC.numDivisionID=D.numDivisionID
	JOIN AdditionalContactsInformation A ON D.numDivisionID=A.numDivisionID
	JOIN CompanyInfo C ON D.numCompanyID = C.numCompanyId
	WHERE 
		CC.numDomainID=@numDomainID 

	-- FETCH ALLREADY PAID COMMISSION DATA AND STORE IN TEMPORARY TABLE
	SELECT * INTO #tempPayrollTracking FROM PayrollTracking WHERE numDomainId=@numDomainId

	Declare @decTotalHrsWorked as decimal(10,2)
	DECLARE @decTotalPaidLeaveHrs AS DECIMAL(10,2)
	DECLARE @monExpense MONEY,@monReimburse MONEY,@monCommPaidInvoice MONEY,@monCommUNPaidInvoice MONEY 

	DECLARE @i INT = 1
	DECLARE @COUNT INT = 0
	DECLARE @numUserCntID NUMERIC(18,0)
	DECLARE @bitCommissionContact BIT
    
	SELECT @COUNT=COUNT(*) FROM #tempHrs

	-- LOOP OVER EACH EMPLOYEE
	WHILE @i <= @COUNT
	BEGIN
		SELECT @decTotalHrsWorked=0,@decTotalPaidLeaveHrs=0,@monExpense=0,@monReimburse=0,@monCommPaidInvoice=0,@monCommUNPaidInvoice=0, @numUserCntID=0,@bitCommissionContact=0
		
		SELECT
			@numUserCntID = ISNULL(numUserCntID,0),
			@bitCommissionContact = ISNULL(bitCommissionContact,0)
		FROM
			#tempHrs
		WHERE
			ID = @i
		
		-- EMPLOYEE (ONLY COMMISSION AMOUNT IS NEEDED FOR COMMISSION CONTACTS)
		IF @bitCommissionContact = 1
		BEGIN
			-- CALCULATE REGULAR HRS
			SELECT 
				@decTotalHrsWorked=sum(x.Hrs) 
			FROM 
				(
					SELECT  
						ISNULL(SUM(CAST(DATEDIFF(MINUTE,dtfromdate,dttodate) AS FLOAT)/CAST(60 AS FLOAT)),0) AS Hrs            
					FROM 
						TimeAndExpense 
					WHERE 
						(numType=1 OR numType=2) 
						AND numCategory=1                 
						AND (
								(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtFromDate) BETWEEN @dtStartDate AND @dtEndDate) 
								OR 
								(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtToDate) BETWEEN @dtStartDate AND @dtEndDate)
							) 
						AND numUserCntID=@numUserCntID  
						AND numDomainID=@numDomainId 
						AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE ISNULL(numCategoryHDRID,0)>0)
				) x

			-- CALCULATE PAID LEAVE HRS
			SELECT 
				@decTotalPaidLeaveHrs=ISNULL(
												SUM( 
														CASE 
														WHEN dtfromdate = dttodate 
														THEN 
															24 - (CASE WHEN bitfromfullday = 0 THEN 12 ELSE 0 END)
														ELSE 
															(Datediff(DAY,dtfromdate,dttodate) + 1) * 24 - (CASE WHEN bitfromfullday = 0 THEN 12 ELSE 0 END) - (CASE WHEN bittofullday = 0 THEN 12 ELSE 0 END)
														 END
													)
											 ,0)
            FROM   
				TimeAndExpense
            WHERE  
				numtype = 3 
				AND numcategory = 3 
				AND numusercntid = @numUserCntID 
				AND numdomainid = @numDomainId 
                AND (
						(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtFromDate) BETWEEN @dtStartDate AND @dtEndDate)
						Or 
						(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtToDate) BETWEEN @dtStartDate AND @dtEndDate)
					)
				AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE ISNULL(numCategoryHDRID,0)>0)

			-- CALCULATE EXPENSES
			SELECT 
				@monExpense=ISNULL((SUM(CAST(monAmount AS FLOAT))),0)    
			FROM 
				TimeAndExpense 
			WHERE 
				numCategory=2 
				AND numType in (1,2) 
				AND numUserCntID=@numUserCntID 
				AND numDomainID=@numDomainId 
				AND ((dtFromDate BETWEEN @dtStartDate And @dtEndDate) Or (dtToDate BETWEEN @dtStartDate And @dtEndDate)) 
				AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE ISNULL(numCategoryHDRID,0)>0)

			-- CALCULATE REIMBURSABLE EXPENSES
			SELECT 
				@monReimburse=Isnull((SUM(CAST(monamount AS FLOAT))),0)
            FROM   
				TimeAndExpense 
            WHERE  
				bitreimburse = 1 
				AND numcategory = 2 
                AND numusercntid = @numUserCntID 
				AND numdomainid = @numDomainId
                AND dtfromdate BETWEEN @dtStartDate AND @dtEndDate
                AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE ISNULL(numCategoryHDRID,0)>0)

		END

		-- CALCULATE COMMISSION PAID INVOICE
		SELECT 
			@monCommPaidInvoice=ISNULL(SUM(Amount),0) 
		FROM 
			(
				SELECT 
					oppBiz.numOppID,
					oppBiz.numOppBizDocsID,
					ISNULL(SUM(BC.numComissionAmount - ISNULL(PT.monCommissionAmt,0)),0) AS Amount
				FROM 
					OpportunityMaster Opp 
				INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
				INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId 
				LEFT JOIN #tempPayrollTracking PT ON PT.numComissionID=BC.numComissionID
				CROSS APPLY
				(
					SELECT 
						MAX(DM.dtDepositDate) dtDepositDate
					FROM 
						DepositMaster DM 
					JOIN dbo.DepositeDetails DD	ON DM.numDepositId=DD.numDepositID 
					WHERE 
						DM.tintDepositePage=2 
						AND DM.numDomainId=@numDomainId
						AND DD.numOppID=oppBiz.numOppID 
						AND DD.numOppBizDocsID =oppBiz.numOppBizDocsID
				) TEMPDeposit
				WHERE 
					Opp.numDomainId=@numDomainId 
					AND BC.numUserCntId=@numUserCntID 
					AND BC.bitCommisionPaid=0 
					AND oppBiz.bitAuthoritativeBizDocs = 1 
					AND isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount 
					AND CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,TEMPDeposit.dtDepositDate) AS DATE) BETWEEN CAST(@dtStartDate AS DATE) AND CAST(@dtEndDate AS DATE)
				GROUP BY 
					oppBiz.numOppID,
					oppBiz.numOppBizDocsID
			) A

		-- CALCULATE COMMISSION UNPAID INVOICE 
		SELECT 
			@monCommUNPaidInvoice=isnull(sum(BC.numComissionAmount - ISNULL(PT.monCommissionAmt,0)),0) 
		FROM 
			OpportunityMaster Opp 
		INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
        INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId
        LEFT JOIN #tempPayrollTracking PT ON PT.numComissionID=BC.numComissionID 
		WHERE 
			Opp.numDomainId=@numDomainId 
			AND BC.numUserCntId=@numUserCntID 
			AND BC.bitCommisionPaid=0 
			AND oppBiz.bitAuthoritativeBizDocs = 1 
			AND ISNULL(oppBiz.monAmountPaid,0) <oppBiz.monDealAmount
			AND ((DATEADD(MINUTE,-@ClientTimeZoneOffset,oppBiz.dtFromDate) BETWEEN @dtStartDate AND @dtEndDate))	

		UPDATE 
			#tempHrs 
		SET    
			decRegularHrs=@decTotalHrsWorked,
			decPaidLeaveHrs=@decTotalPaidLeaveHrs,
			monExpense=@monExpense,
			monReimburse=@monReimburse,
			monCommPaidInvoice=@monCommPaidInvoice,
			monCommUNPaidInvoice=@monCommUNPaidInvoice,
			monTotalAmountDue= @decTotalHrsWorked * monHourlyRate + @decTotalPaidLeaveHrs * monHourlyRate - @monExpense + @monReimburse + @monCommPaidInvoice + @monCommUNPaidInvoice
        WHERE 
			numUserCntID=@numUserCntID     

		SET @i = @i + 1
	END

	-- CALCULATE PAID AMOUNT
	UPDATE 
		temp 
	SET 
		monAmountPaid=ISNULL(PD.monTotalAmt,0) 
	FROM 
		#tempHrs temp  
	JOIN dbo.PayrollDetail PD ON PD.numUserCntID=temp.numUserCntID
	JOIN dbo.PayrollHeader PH ON PH.numPayrollHeaderID=PD.numPayrollHeaderID
	WHERE 
		ISNULL(PD.numCheckStatus,0)=2  
		AND ((dtFromDate BETWEEN @dtStartDate AND @dtEndDate) OR (dtToDate BETWEEN @dtStartDate And @dtEndDate))
 
	SELECT * FROM #tempHrs
	DROP TABLE #tempPayrollTracking
	DROP TABLE #tempHrs
END

--  BEGIN
----    SET @dtEndDate ='1/31/2008'
----  SET @dtStartDate ='1/1/2008'
--    SELECT um.numuserid,
--           Isnull(adc.vcfirstname
--                    + ' '
--                    + adc.vclastname,'-') vcusername,
--           adc.numcontactid AS numusercntid,
--           Isnull(adc.vcfirstname
--                    + ' '
--                    + adc.vclastname,'-') AS name,
--           Isnull(ld.vcdata,'') AS vcdepartment,
--           Isnull(um.vcemployeeid,'') AS employeeid,
--           Isnull(um.vcemergencycontact,'') AS vcemergencycontact,
--           CAST(dbo.Fn_gettotalhrs(um.bitovertime,Isnull(um.bitovertimehrsdailyorweekly,0),
--                                   Isnull(um.numlimdailhrs,0),@dtStartDate,@dtEndDate,
--                                   adc.numcontactid,@numDomainId,0,@ClientTimeZoneOffset) AS FLOAT) AS hrsworked,
--           CAST(dbo.Getoverhrsworked(um.bitovertime,Isnull(um.bitovertimehrsdailyorweekly,0),
--                                     Isnull(um.numlimdailhrs,0),@dtStartDate,@dtEndDate,
--                                     adc.numcontactid,@numDomainId,@ClientTimeZoneOffset) AS FLOAT) AS overtimehrsworked,
--           (SELECT SUM(CASE 
--                         WHEN dtfromdate = dttodate THEN 24
--                                                           - (CASE 
--                                                                WHEN bitfromfullday = 0 THEN 12
--                                                                ELSE 0
--                                                              END)
--                         ELSE (Datediff(DAY,dtfromdate,dttodate)
--                                 + 1)
--                                * 24
--                                - (CASE 
--                                     WHEN bitfromfullday = 0 THEN 12
--                                     ELSE 0
--                                   END)
--                                - (CASE 
--                                     WHEN bittofullday = 0 THEN 12
--                                     ELSE 0
--                                   END)
--                       END)
--            FROM   timeandexpense
--            WHERE  numtype = 3
--                   AND numcategory = 3
--                   AND numusercntid = adc.numcontactid
--                   AND numdomainid = @numDomainId
--                   AND (dtfromdate BETWEEN @dtStartDate AND @dtEndDate
--                         OR dttodate BETWEEN @dtStartDate AND @dtEndDate)) AS paidleavehrs,
--           CAST(dbo.Fn_gettotalhrs(um.bitovertime,Isnull(um.bitovertimehrsdailyorweekly,0),
--                                   Isnull(um.numlimdailhrs,0),@dtStartDate,@dtEndDate,
--                                   adc.numcontactid,@numDomainId,1,@ClientTimeZoneOffset) AS FLOAT) AS totalhrs,
--           dbo.Getcommissionamountofduration(um.numuserid,adc.numcontactid,@numDomainId,
--                                             @dtStartDate,@dtEndDate,0) AS BizDocPaidCommAmt,
--           dbo.Getcommissionamountofduration(um.numuserid,adc.numcontactid,@numDomainId,
--                                             @dtStartDate,@dtEndDate,1) AS BizDocNonPaidCommAmt,
--           dbo.Gettotalexpense(adc.numcontactid,@numDomainId,@dtStartDate,
--                               @dtEndDate) AS totalexpenses,
--           (SELECT Isnull((SUM(CAST(monamount AS FLOAT))),0)
--            FROM   timeandexpense
--            WHERE  bitreimburse = 1
--                   AND numcategory = 2
--                   AND numtype IN (1,2)
--                   AND numusercntid = adc.numcontactid
--                   AND numdomainid = @numDomainId
--                   AND dtfromdate BETWEEN @dtStartDate AND @dtEndDate) AS totalremburseexpenses,
--           dbo.Gettotalamountdue(um.numuserid,@dtStartDate,@dtEndDate,adc.numcontactid,
--                                 @numDomainId,@ClientTimeZoneOffset) AS totalamountdue,
--           NULL AS timelineregistry,
--           Isnull(um.bitpayroll,0) AS bitpayroll,
--           CASE 
--             WHEN Isnull(um.bitempnetaccount,0) = 1 THEN 'COGS'
--           END AS empnetaccount,0 AS bContractBased,adc.numDivisionID
--    FROM   usermaster um
--           JOIN additionalcontactsinformation adc
--             ON adc.numcontactid = um.numuserdetailid
--           LEFT OUTER JOIN listdetails ld
--             ON adc.vcdepartment = ld.numlistitemid
--                AND ld.numlistid = 19
--    WHERE  
----um.bitactivateflag = 1 AND 
--	um.numdomainid = @numDomainId
--           AND CASE 
--                 WHEN @numDepartmentId > 0 THEN ld.numlistitemid
--                 ELSE @numDepartmentId
--               END = @numDepartmentId
--               
--               
--               UNION ALL
--               
-- select  0,Isnull(adc.vcfirstname  + ' ' + adc.vclastname,'-') + '(' + C.vcCompanyName +')' as vcusername ,adc.numcontactid AS numusercntid,
--           Isnull(adc.vcfirstname  + ' ' + adc.vclastname,'-') AS name, '','',
--           '',
--          0,
--          0,
--           0,
--           0,
--           dbo.Getcommissionamountofduration(0,adc.numcontactid,@numDomainId,
--                                             @dtStartDate,@dtEndDate,0) AS BizDocPaidCommAmt,
--           dbo.Getcommissionamountofduration(0,adc.numcontactid,@numDomainId,
--                                             @dtStartDate,@dtEndDate,1) AS BizDocNonPaidCommAmt,                                  
--           dbo.Gettotalexpense(adc.numcontactid,@numDomainId,@dtStartDate,
--                               @dtEndDate) AS totalexpenses,
--           (SELECT Isnull((SUM(CAST(monamount AS FLOAT))),0)
--            FROM   timeandexpense
--            WHERE  bitreimburse = 1
--                   AND numcategory = 2
--                   AND numtype IN (1,2)
--                   AND numusercntid = adc.numcontactid
--                   AND numdomainid = @numDomainId
--                   AND dtfromdate BETWEEN @dtStartDate AND @dtEndDate) AS totalremburseexpenses,
--         dbo.Getcommissionamountofduration(0,adc.numcontactid,@numDomainId,
--                                             @dtStartDate,@dtEndDate,0) AS totalamountdue,
--           NULL AS timelineregistry,
--           0,
--          '',1 AS bContractBased,D.numDivisionID
-- from CommissionContacts CC JOIN DivisionMaster D ON CC.numDivisionID=D.numDivisionID
-- join CompanyInfo C on C.numCompanyID=D.numCompanyID  
-- JOIN AdditionalContactsInformation adc ON D.numDivisionID=adc.numDivisionID 
-- --AND CC.numContactID=adc.numContactID
-- where CC.numDomainID=@numDomainID 
-- 
--  END
/****** Object:  StoredProcedure [dbo].[USP_RevertDetailsOpp]    Script Date: 07/26/2008 16:20:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
/* deducts Qty from allocation and Adds back to onhand */
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_RevertDetailsOpp')
DROP PROCEDURE USP_RevertDetailsOpp
GO
CREATE PROCEDURE [dbo].[USP_RevertDetailsOpp] 
@numOppId NUMERIC,
@tintMode AS TINYINT=0, -- 0:Add/Edit 1:Delete
@numUserCntID AS NUMERIC(9)
AS ---reverting back to previous state if deal is being edited 	DECLARE @numDomain AS NUMERIC(18,0)	SELECT @numDomain = numDomainID FROM [dbo].[OpportunityMaster] AS OM WHERE [OM].[numOppId] = @numOppID    	DECLARE @OppType AS VARCHAR(2)                  DECLARE @itemcode AS NUMERIC           DECLARE @numWareHouseItemID AS NUMERIC                                       DECLARE @numToWarehouseItemID AS NUMERIC     DECLARE @numUnits AS NUMERIC                                                  DECLARE @onHand AS NUMERIC                                                DECLARE @onOrder AS NUMERIC                                                DECLARE @onBackOrder AS NUMERIC                                                  DECLARE @onAllocation AS NUMERIC    DECLARE @numQtyShipped AS NUMERIC    DECLARE @numUnitHourReceived AS NUMERIC    DECLARE @numoppitemtCode AS NUMERIC(9)     DECLARE @monAmount AS MONEY     DECLARE @monAvgCost AS MONEY       DECLARE @Kit AS BIT                                            DECLARE @bitKitParent BIT    DECLARE @bitStockTransfer BIT     DECLARE @numOrigUnits AS NUMERIC			    DECLARE @description AS VARCHAR(100)	DECLARE @bitWorkOrder AS BIT	--Added by :Sachin Sadhu||Date:18thSept2014	--For Rental/Asset Project	Declare @numRentalIN as Numeric	Declare @numRentalOut as Numeric	Declare @numRentalLost as Numeric	DECLARE @bitAsset as BIT	--end sachin    						    SELECT TOP 1            @numoppitemtCode = numoppitemtCode,            @itemcode = OI.numItemCode,            @numUnits = ISNULL(numUnitHour,0),            @numWareHouseItemID = ISNULL(numWarehouseItmsID,0),            @Kit = ( CASE WHEN bitKitParent = 1                               AND bitAssembly = 1 THEN 0                          WHEN bitKitParent = 1 THEN 1                          ELSE 0                     END ),            @monAmount = ISNULL(monTotAmount,0) * (CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END),            @monAvgCost = ISNULL(monAverageCost,0),            @numQtyShipped = ISNULL(numQtyShipped,0),            @numUnitHourReceived = ISNULL(numUnitHourReceived,0),            @bitKitParent=ISNULL(bitKitParent,0),            @numToWarehouseItemID =OI.numToWarehouseItemID,            @bitStockTransfer = ISNULL(OM.bitStockTransfer,0),            @OppType = tintOppType,		    @numRentalIN=ISNULL(oi.numRentalIN,0),			@numRentalOut=Isnull(oi.numRentalOut,0),			@numRentalLost=Isnull(oi.numRentalLost,0),			@bitAsset =ISNULL(I.bitAsset,0),			@bitWorkOrder = ISNULL(OI.bitWorkOrder,0)    FROM    OpportunityItems OI			JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId            JOIN Item I ON OI.numItemCode = I.numItemCode    WHERE   (charitemtype='P' OR 1=(CASE WHEN tintOppType=1 THEN 							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1								 ELSE 0 END 							ELSE 0 END)) AND OI.numOppId = @numOppId                           AND ( bitDropShip = 0                                 OR bitDropShip IS NULL                               )     ORDER BY OI.numoppitemtCode	PRINT '@numoppitemtCode:' + CAST(@numoppitemtCode AS VARCHAR(10))	PRINT '@numUnits:' + CAST(@numUnits AS VARCHAR(10))	PRINT '@numWareHouseItemID:' + CAST(@numWareHouseItemID AS VARCHAR(10))	PRINT '@numQtyShipped:' + CAST(@numQtyShipped AS VARCHAR(10))	PRINT '@numUnitHourReceived:' + CAST(@numUnitHourReceived AS VARCHAR(10))	PRINT '@numToWarehouseItemID:' + CAST(@numToWarehouseItemID AS VARCHAR(10))	PRINT '@bitStockTransfer:' + CAST(@bitStockTransfer AS VARCHAR(10))	    WHILE @numoppitemtCode > 0                                          BEGIN            --Kamal : Item Group take as inline Item--            IF @Kit = 1 --                BEGIN  --                    EXEC USP_UpdateKitItems @numoppitemtCode, @numUnits,--                        @OppType, 1,@numOppID  --                END                                      SET @numOrigUnits=@numUnits                        IF @bitStockTransfer=1            BEGIN				SET @OppType = 1			END                        PRINT '@OppType:' + CAST(@OppType AS VARCHAR(10))                  IF @numWareHouseItemID>0            BEGIN     				PRINT 'FROM REVERT : CONDITION : @numWareHouseItemID>0'                             				SELECT  @onHand = ISNULL(numOnHand, 0),						@onAllocation = ISNULL(numAllocation, 0),						@onOrder = ISNULL(numOnOrder, 0),						@onBackOrder = ISNULL(numBackOrder, 0)				FROM    WareHouseItems				WHERE   numWareHouseItemID = @numWareHouseItemID       								PRINT '@onHand:' + CAST(@onHand AS VARCHAR(10))				PRINT '@onAllocation:' + CAST(@onAllocation AS VARCHAR(10))				PRINT '@onOrder:' + CAST(@onOrder AS VARCHAR(10))				PRINT '@onBackOrder:' + CAST(@onBackOrder AS VARCHAR(10))                                                     END                        IF @OppType = 1                 BEGIN					SET @description='SO Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'                                IF @Kit = 1				BEGIN					exec USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,@numOrigUnits,1,@numOppID,@tintMode,@numUserCntID				END				ELSE IF @bitWorkOrder = 1				BEGIN										IF @tintMode=0 -- EDIT					BEGIN						SET @description='SO-WO Edited (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'						-- UPDATE WORK ORDER - REVERT INVENTOY OF ASSEMBLY ITEMS						EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@itemcode,@numWareHouseItemID,@numQtyShipped,5					END					ELSE IF @tintMode=1 -- DELETE					BEGIN						DECLARE @numWOID AS NUMERIC(18,0)						DECLARE @numWOStatus AS NUMERIC(18,0)						SELECT @numWOID=numWOId,@numWOStatus=numWOStatus FROM WorkOrder WHERE numOppId=@numOppId AND numItemCode=@itemcode AND numWareHouseItemId=@numWareHouseItemID						SET @description='SO-WO Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'						--IF WORK ORDER IS NOT COMPLETED THAN REMOVE ON ORDER QUANTITY						IF @numWOStatus <> 23184						BEGIN							IF @onOrder >= @numUnits								SET @onOrder = @onOrder - @numUnits							ELSE								SET @onOrder = 0						END						-- IF BACKORDER QTY IS GREATER THEN QTY TO REVERT THEN
						-- DECREASE BACKORDER QTY BY QTY TO REVERT
						IF @numUnits < @onBackOrder 
						BEGIN                  
							SET @onBackOrder = @onBackOrder - @numUnits
						END 
						-- IF BACKORDER QTY IS LESS THEN OR EQUAL TO QTY TO REVERT THEN
						-- DECREASE QTY TO REVERT BY QTY OF BACK ORDER AND
						-- SET BACKORDER QTY TO 0
						ELSE IF @numUnits >= @onBackOrder 
						BEGIN
							SET @numUnits = @numUnits - @onBackOrder
							SET @onBackOrder = 0
                        
							--REMOVE ITEM FROM ALLOCATION 
							IF (@onAllocation - @numUnits) >= 0
								SET @onAllocation = @onAllocation - @numUnits
						
							--ADD QTY TO ONHAND
							SET @onHand = @onHand + @numUnits
						END						UPDATE  WareHouseItems						SET     numOnHand = @onHand ,								numAllocation = @onAllocation,								numBackOrder = @onBackOrder,								numOnOrder = @onOrder,								dtModified = GETDATE()						WHERE   numWareHouseItemID = @numWareHouseItemID   						--IF WORK ORDER IS NOT COMPLETED THAN DELETE WORK ORDER AND ITS CHILDS WORK ORDER						IF @numWOStatus <> 23184						BEGIN							EXEC USP_DeleteAssemblyWOAndInventory  @numDomain,@numUserCntID,@numWOID,@numOppID						END					END				END					ELSE					BEGIN	                          IF @numQtyShipped>0								 BEGIN									IF @tintMode=0											SET @numUnits = @numUnits - @numQtyShipped									ELSE IF @tintmode=1											SET @onAllocation = @onAllocation + @numQtyShipped 								  END 								                                        IF @numUnits >= @onBackOrder                         BEGIN                            SET @numUnits = @numUnits - @onBackOrder                            SET @onBackOrder = 0                                                        IF (@onAllocation - @numUnits >= 0)								SET @onAllocation = @onAllocation - @numUnits								IF @bitAsset=1--Not Asset										BEGIN											  SET @onHand = @onHand +@numRentalIN+@numRentalLost+@numRentalOut     										END								ELSE										BEGIN											 SET @onHand = @onHand + @numUnits     										END                                                                                         END                                                                ELSE                         IF @numUnits < @onBackOrder                             BEGIN                  								IF (@onBackOrder - @numUnits >0)									SET @onBackOrder = @onBackOrder - @numUnits									--								IF @tintmode=1--									SET @onAllocation = @onAllocation + (@numUnits - @numQtyShipped)                            END                  							UPDATE  WareHouseItems						SET     numOnHand = @onHand ,								numAllocation = @onAllocation,								numBackOrder = @onBackOrder,								dtModified = GETDATE()						WHERE   numWareHouseItemID = @numWareHouseItemID       										                                                                                 				END								IF @numWareHouseItemID>0				BEGIN 						EXEC dbo.USP_ManageWareHouseItems_Tracking						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)						@numReferenceID = @numOppId, --  numeric(9, 0)						@tintRefType = 3, --  tinyint						@vcDescription = @description, --  varchar(100)						@numModifiedBy = @numUserCntID,						@numDomainID = @numDomain 				END		          END                          IF @bitStockTransfer=1            BEGIN				SET @numWareHouseItemID = @numToWarehouseItemID;				SET @OppType = 2				SET @numUnits = @numOrigUnits				SELECT  @onHand = ISNULL(numOnHand, 0),                    @onAllocation = ISNULL(numAllocation, 0),                    @onOrder = ISNULL(numOnOrder, 0),                    @onBackOrder = ISNULL(numBackOrder, 0)				FROM    WareHouseItems				WHERE   numWareHouseItemID = @numWareHouseItemID   			END                    IF @OppType = 2                     BEGIN 					SET @description='PO Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@numUnitHourReceived AS VARCHAR(10)) + ')'						--Updating the Average Cost--                        IF @onHand + @onOrder - @numUnits <> 0 --                            BEGIN--                                SET @monAvgCost = ( ( @onHand + @onOrder )--                                                    * @monAvgCost - @monAmount )--                                    / ( @onHand + @onOrder - @numUnits )--                            END--                        ELSE --                            SET @monAvgCost = 0--                        UPDATE  item--                        SET     monAverageCost = @monAvgCost--                        WHERE   numItemCode = @itemcode										    --Partial Fulfillment						IF @tintmode=1 and  @onHand >= @numUnitHourReceived						BEGIN							SET @onHand= @onHand - @numUnitHourReceived							--SET @onOrder= @onOrder + @numUnitHourReceived						END											    SET @numUnits = @numUnits - @numUnitHourReceived 						IF (@onOrder - @numUnits)>=0						BEGIN							--Causing Negative Inventory Bug ID:494							SET @onOrder = @onOrder - @numUnits							END						ELSE IF (@onHand + @onOrder) - @numUnits >= 0						BEGIN													SET @onHand = @onHand - (@numUnits-@onOrder)							SET @onOrder = 0						END						ELSE IF  (@onHand + @onOrder + @onAllocation) - @numUnits >= 0						BEGIN							Declare @numDiff numeric								SET @numDiff = @numUnits - @onOrder							SET @onOrder = 0							SET @numDiff = @numDiff - @onHand							SET @onHand = 0							SET @onAllocation = @onAllocation - @numDiff							SET @onBackOrder = @onBackOrder + @numDiff						END					                            UPDATE  WareHouseItems                        SET     numOnHand = @onHand,                                numAllocation = @onAllocation,                                numBackOrder = @onBackOrder,                                numOnOrder = @onOrder,							dtModified = GETDATE()                        WHERE   numWareHouseItemID = @numWareHouseItemID                                                   EXEC dbo.USP_ManageWareHouseItems_Tracking						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)						@numReferenceID = @numOppId, --  numeric(9, 0)						@tintRefType = 4, --  tinyint						@vcDescription = @description, --  varchar(100)						@numModifiedBy = @numUserCntID,						@numDomainID = @numDomain                                                              END                                                                               SELECT TOP 1                    @numoppitemtCode = numoppitemtCode,                    @itemcode = OI.numItemCode,                    @numUnits = numUnitHour,                    @numWareHouseItemID = ISNULL(numWarehouseItmsID,0),                    @Kit = ( CASE WHEN bitKitParent = 1                                       AND bitAssembly = 1 THEN 0                                  WHEN bitKitParent = 1 THEN 1                                  ELSE 0                             END ),                    @monAmount = ISNULL(monTotAmount,0)  * (CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END),                    @monAvgCost = monAverageCost,                    @numQtyShipped = ISNULL(numQtyShipped,0),					@numUnitHourReceived = ISNULL(numUnitHourReceived,0),					@bitKitParent=ISNULL(bitKitParent,0),					@numToWarehouseItemID =OI.numToWarehouseItemID,					@bitStockTransfer = ISNULL(OM.bitStockTransfer,0),					@OppType = tintOppType            FROM    OpportunityItems OI					JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId                    JOIN Item I ON OI.numItemCode = I.numItemCode                                               WHERE   (charitemtype='P' OR 1=(CASE WHEN tintOppType=1 THEN 							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1								 ELSE 0 END 							ELSE 0 END))  							AND OI.numOppId = @numOppId                     AND OI.numoppitemtCode > @numoppitemtCode                    AND ( bitDropShip = 0                          OR bitDropShip IS NULL                        )            ORDER BY OI.numoppitemtCode                                                          IF @@rowcount = 0                 SET @numoppitemtCode = 0              END
/****** Object:  StoredProcedure [dbo].[USP_SaveGridColumnWidth]    Script Date: 07/26/2008 16:14:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SaveAdvanceSearchGridColumnWidth')
DROP PROCEDURE USP_SaveAdvanceSearchGridColumnWidth
GO
CREATE PROCEDURE [dbo].[USP_SaveAdvanceSearchGridColumnWidth]  
@numDomainID as numeric(9)=0,  
@numUserCntID as numeric(9)=0,
@str as text,
@numViewID int
as  

DECLARE @numGroupID AS NUMERIC(18,0)
SELECT @numGroupID=numGroupID FROM UserMaster WHERE numUserDetailId=@numUserCntID

declare @hDoc as int     
EXEC sp_xml_preparedocument @hDoc OUTPUT, @str                                                                        

Create table #tempTable(numFormId numeric(9),numFieldId numeric(9),bitCustom bit,intColumnWidth int)

  insert into #tempTable (numFormId,numFieldId,bitCustom,intColumnWidth)                                        
  SELECT * FROM OPENXML (@hDoc,'/NewDataSet/GridColumnWidth',2)                                                  
  WITH (numFormId numeric(9),numFieldId numeric(9),bitCustom bit,intColumnWidth int)                                           
     
UPDATE
	t1
SET 
	intColumnWidth=temp.intColumnWidth
FROM
	AdvSerViewConf t1
JOIN
	#tempTable temp
ON
	t1.numFormFieldID = temp.numFieldId 
WHERE
	t1.numFormId = 1
	AND t1.tintViewID = @numViewID
	AND t1.numDomainID = @numDomainID
	AND t1.numGroupID = @numGroupID      
	AND ISNULL(t1.bitCustom,0) = temp.bitCustom -- RIGHT NOW CUSTOM FIELDS ARE NOT AVAILABLE FOR SELECTION IN ADVANCE SEARCH GRID
	                                             

  drop table #tempTable                                        
 EXEC sp_xml_removedocument @hDoc  
/****** Object:  StoredProcedure [dbo].[USP_UpdatedomainDetails]    Script Date: 05/07/2009 21:56:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatedomaindetails')
DROP PROCEDURE usp_updatedomaindetails
GO
CREATE PROCEDURE [dbo].[USP_UpdatedomainDetails]                                      
@numDomainID as numeric(9)=0,                                      
@vcDomainName as varchar(50)='',                                      
@vcDomainDesc as varchar(100)='',                                      
@bitExchangeIntegration as bit,                                      
@bitAccessExchange as bit,                                      
@vcExchUserName as varchar(100)='',                                      
@vcExchPassword as varchar(100)='',                                      
@vcExchPath as varchar(200)='',                                      
@vcExchDomain as varchar(100)='',                                    
@tintCustomPagingRows as tinyint,                                    
@vcDateFormat as varchar(30)='',                                    
@numDefCountry as numeric(9),                                    
@tintComposeWindow as tinyint,                                    
@sintStartDate as smallint,                                    
@sintNoofDaysInterval as smallint,                                    
@tintAssignToCriteria as tinyint,                                    
@bitIntmedPage as bit,                                    
@tintFiscalStartMonth as tinyint,                            
--@bitDeferredIncome as bit,                               
@tintPayPeriod as tinyint,                        
@numCurrencyID as numeric(9) ,                
@charUnitSystem as char(1)='',              
@vcPortalLogo as varchar(100)='',            
@tintChrForComSearch as tinyint  ,            
@intPaymentGateWay as int,    
@tintChrForItemSearch as tinyint,
@ShipCompany as numeric(9),
@bitAutoPopulateAddress as bit=null,
@tintPoulateAddressTo as tinyint=null,
@bitMultiCurrency as bit,
--@bitMultiCompany as BIT,
@bitCreateInvoice AS tinyint = null,
@numDefaultSalesBizDocID AS NUMERIC(9)=NULL,
@numDefaultPurchaseBizDocID AS NUMERIC(9)=NULL,
@bitSaveCreditCardInfo AS BIT = NULL,
@intLastViewedRecord AS INT=10,
@tintCommissionType AS TINYINT,
@tintBaseTax AS TINYINT,
@bitEmbeddedCost AS BIT,
@numDefaultSalesOppBizDocId NUMERIC=NULL,
@tintSessionTimeOut TINYINT=1,
@tintDecimalPoints TINYINT,
@bitCustomizePortal BIT=0,
@tintBillToForPO TINYINT,
@tintShipToForPO TINYINT,
@tintOrganizationSearchCriteria TINYINT,
@bitDocumentRepositary BIT=0,
@tintComAppliesTo TINYINT=0,
@bitGtoBContact BIT=0,
@bitBtoGContact BIT=0,
@bitGtoBCalendar BIT=0,
@bitBtoGCalendar BIT=0,
@bitExpenseNonInventoryItem BIT=0,
@bitInlineEdit BIT=0,
@bitRemoveVendorPOValidation BIT=0,
@tintBaseTaxOnArea tinyint=0,
@bitAmountPastDue BIT=0,
@monAmountPastDue money,
@bitSearchOrderCustomerHistory BIT=0,
@bitRentalItem as bit=0,
@numRentalItemClass as NUMERIC(9)=NULL,
@numRentalHourlyUOM as NUMERIC(9)=NULL,
@numRentalDailyUOM as NUMERIC(9)=NULL,
@tintRentalPriceBasedOn as tinyint=NULL,
@tintCalendarTimeFormat as tinyint=NULL,
@tintDefaultClassType as tinyint=NULL,
@tintPriceBookDiscount as tinyint=0,
@bitAutoSerialNoAssign as bit=0,
@bitIsShowBalance AS BIT = 0,
@numIncomeAccID NUMERIC(18, 0) = 0,
@numCOGSAccID NUMERIC(18, 0) = 0,
@numAssetAccID NUMERIC(18, 0) = 0,
@IsEnableClassTracking AS BIT = 0,
@IsEnableProjectTracking AS BIT = 0,
@IsEnableCampaignTracking AS BIT = 0,
@IsEnableResourceScheduling AS BIT = 0,
@ShippingServiceItem  AS NUMERIC(9)=0,
@numSOBizDocStatus AS NUMERIC(9)=0,
@bitAutolinkUnappliedPayment AS BIT=0,
@numDiscountServiceItemID AS NUMERIC(9)=0,
@vcShipToPhoneNumber as VARCHAR(50)='',
@vcGAUserEmailId as VARCHAR(50)='',
@vcGAUserPassword as VARCHAR(50)='',
@vcGAUserProfileId as VARCHAR(50)='',
@bitDiscountOnUnitPrice AS BIT=0,
@intShippingImageWidth INT = 0,
@intShippingImageHeight INT = 0,
@numTotalInsuredValue NUMERIC(10,2) = 0,
@numTotalCustomsValue NUMERIC(10,2) = 0,
@vcHideTabs VARCHAR(1000) = '',
@bitUseBizdocAmount BIT = 0,
@IsEnableUserLevelClassTracking BIT = 0,
@bitDefaultRateType BIT = 0,
@bitIncludeTaxAndShippingInCommission  BIT = 0,
/*,@bitAllowPPVariance AS BIT=0*/
@bitPurchaseTaxCredit BIT=0,
@bitLandedCost BIT=0,
@vcLanedCostDefault VARCHAR(10)='',
@bitMinUnitPriceRule BIT = 0,
@numAbovePercent AS NUMERIC(18,2),
@numAbovePriceField AS NUMERIC(18,0),
@numBelowPercent AS NUMERIC(18,2),
@numBelowPriceField AS NUMERIC(18,0),
@numOrderStatusBeforeApproval AS NUMERIC(18,0),
@numOrderStatusAfterApproval AS NUMERIC(18,0),
@vcUnitPriceApprover AS VARCHAR(100),
@numDefaultSalesPricing AS TINYINT
as                                      
                                      
 update Domain                                       
   set                                       
   vcDomainName=@vcDomainName,                                      
   vcDomainDesc=@vcDomainDesc,                                      
   bitExchangeIntegration=@bitExchangeIntegration,                                      
   bitAccessExchange=@bitAccessExchange,                                      
   vcExchUserName=@vcExchUserName,                                      
   vcExchPassword=@vcExchPassword,                                      
   vcExchPath=@vcExchPath,                                      
   vcExchDomain=@vcExchDomain,                                    
   tintCustomPagingRows =@tintCustomPagingRows,                                    
   vcDateFormat=@vcDateFormat,                                    
   numDefCountry =@numDefCountry,                                    
   tintComposeWindow=@tintComposeWindow,                                    
   sintStartDate =@sintStartDate,                                    
   sintNoofDaysInterval=@sintNoofDaysInterval,                                    
   tintAssignToCriteria=@tintAssignToCriteria,                                    
   bitIntmedPage=@bitIntmedPage,                                    
   tintFiscalStartMonth=@tintFiscalStartMonth,                            
--   bitDeferredIncome=@bitDeferredIncome,                          
   tintPayPeriod=@tintPayPeriod,
   vcCurrency=isnull((select varCurrSymbol from Currency Where numCurrencyID=@numCurrencyID  ),'')  ,                      
  numCurrencyID=@numCurrencyID,                
  charUnitSystem= @charUnitSystem,              
  vcPortalLogo=@vcPortalLogo ,            
  tintChrForComSearch=@tintChrForComSearch  ,      
  intPaymentGateWay=@intPaymentGateWay,
 tintChrForItemSearch=@tintChrForItemSearch,
 numShipCompany= @ShipCompany,
 bitAutoPopulateAddress = @bitAutoPopulateAddress,
 tintPoulateAddressTo =@tintPoulateAddressTo,
 --bitMultiCompany=@bitMultiCompany ,
 bitMultiCurrency=@bitMultiCurrency,
 bitCreateInvoice= @bitCreateInvoice,
 [numDefaultSalesBizDocId] =@numDefaultSalesBizDocId,
 bitSaveCreditCardInfo=@bitSaveCreditCardInfo,
 intLastViewedRecord=@intLastViewedRecord,
 [tintCommissionType]=@tintCommissionType,
 [tintBaseTax]=@tintBaseTax,
 [numDefaultPurchaseBizDocId]=@numDefaultPurchaseBizDocID,
 bitEmbeddedCost=@bitEmbeddedCost,
 numDefaultSalesOppBizDocId=@numDefaultSalesOppBizDocId,
 tintSessionTimeOut=@tintSessionTimeOut,
 tintDecimalPoints=@tintDecimalPoints,
 bitCustomizePortal=@bitCustomizePortal,
 tintBillToForPO = @tintBillToForPO,
 tintShipToForPO = @tintShipToForPO,
 tintOrganizationSearchCriteria=@tintOrganizationSearchCriteria,
-- vcPortalName=@vcPortalName,
 bitDocumentRepositary=@bitDocumentRepositary,
 --tintComAppliesTo=@tintComAppliesTo,THIS FIELD IS REMOVED FROM GLOBAL SETTINGS
 bitGtoBContact=@bitGtoBContact,
 bitBtoGContact=@bitBtoGContact,
 bitGtoBCalendar=@bitGtoBCalendar,
 bitBtoGCalendar=@bitBtoGCalendar,
 bitExpenseNonInventoryItem=@bitExpenseNonInventoryItem,
 bitInlineEdit=@bitInlineEdit,
 bitRemoveVendorPOValidation=@bitRemoveVendorPOValidation,
tintBaseTaxOnArea=@tintBaseTaxOnArea,
bitAmountPastDue = @bitAmountPastDue,
monAmountPastDue = @monAmountPastDue,
bitSearchOrderCustomerHistory=@bitSearchOrderCustomerHistory,
bitRentalItem=@bitRentalItem,
numRentalItemClass=@numRentalItemClass,
numRentalHourlyUOM=@numRentalHourlyUOM,
numRentalDailyUOM=@numRentalDailyUOM,
tintRentalPriceBasedOn=@tintRentalPriceBasedOn,
tintCalendarTimeFormat=@tintCalendarTimeFormat,
tintDefaultClassType=@tintDefaultClassType,
tintPriceBookDiscount=@tintPriceBookDiscount,
bitAutoSerialNoAssign=@bitAutoSerialNoAssign,
bitIsShowBalance = @bitIsShowBalance,
numIncomeAccID = @numIncomeAccID,
numCOGSAccID = @numCOGSAccID,
numAssetAccID = @numAssetAccID,
IsEnableClassTracking = @IsEnableClassTracking,
IsEnableProjectTracking = @IsEnableProjectTracking,
IsEnableCampaignTracking = @IsEnableCampaignTracking,
IsEnableResourceScheduling = @IsEnableResourceScheduling,
numShippingServiceItemID = @ShippingServiceItem,
numSOBizDocStatus=@numSOBizDocStatus,
bitAutolinkUnappliedPayment=@bitAutolinkUnappliedPayment,
numDiscountServiceItemID=@numDiscountServiceItemID,
vcShipToPhoneNo = @vcShipToPhoneNumber,
vcGAUserEMail = @vcGAUserEmailId,
vcGAUserPassword = @vcGAUserPassword,
vcGAUserProfileId = @vcGAUserProfileId,
bitDiscountOnUnitPrice=@bitDiscountOnUnitPrice,
intShippingImageWidth = @intShippingImageWidth ,
intShippingImageHeight = @intShippingImageHeight,
numTotalInsuredValue = @numTotalInsuredValue,
numTotalCustomsValue = @numTotalCustomsValue,
vcHideTabs = @vcHideTabs,
bitUseBizdocAmount = @bitUseBizdocAmount,
IsEnableUserLevelClassTracking = @IsEnableUserLevelClassTracking,
bitDefaultRateType = @bitDefaultRateType,
bitIncludeTaxAndShippingInCommission = @bitIncludeTaxAndShippingInCommission,
/*,bitAllowPPVariance=@bitAllowPPVariance*/
bitPurchaseTaxCredit=@bitPurchaseTaxCredit,
bitLandedCost=@bitLandedCost,
vcLanedCostDefault=@vcLanedCostDefault,
bitMinUnitPriceRule = @bitMinUnitPriceRule,
numAbovePercent = @numAbovePercent,
numAbovePriceField = @numAbovePriceField,
numBelowPercent = @numBelowPercent,
numBelowPriceField = @numBelowPriceField,
numOrderStatusBeforeApproval = @numOrderStatusBeforeApproval,
numOrderStatusAfterApproval = @numOrderStatusAfterApproval,
numDefaultSalesPricing = @numDefaultSalesPricing
 where numDomainId=@numDomainID

DELETE FROM UnitPriceApprover WHERE numDomainID = @numDomainID

INSERT INTO UnitPriceApprover
	(
		numDomainID,
		numUserID
	)
SELECT
     @numDomainID,
	 Split.a.value('.', 'VARCHAR(100)') AS String
FROM  
(
SELECT 
        CAST ('<M>' + REPLACE(@vcUnitPriceApprover, ',', '</M><M>') + '</M>' AS XML) AS String  
) AS A 
CROSS APPLY 
	String.nodes ('/M') AS Split(a); 

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_InsertRecursive')
DROP PROCEDURE USP_WorkOrder_InsertRecursive
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_InsertRecursive]
	@numOppID AS NUMERIC(9)=0,
	@numItemCode AS NUMERIC(18,0),
	@numQty AS NUMERIC(18,0),
	@numWarehouseItemID AS NUMERIC(18,0),
	@numDomainID AS NUMERIC(18,0),
	@numUserCntID AS NUMERIC(18,0)=0,
	@numQtyShipped AS NUMERIC(18,0),
	@numParentWOID AS NUMERIC(18,0),
	@bitFromWorkOrderScreen AS BIT
AS
BEGIN
	DECLARE @numWOID NUMERIC(18,0)
	DECLARE @vcInstruction VARCHAR(2000)
	DECLARE @numAssignedTo NUMERIC(18,0)
	DECLARE @bintCompletionDate DATETIME

	SELECT @vcInstruction=vcInstruction,@numAssignedTo=numAssignedTo,@bintCompletionDate=bintCompliationDate FROM WorkOrder WHERE numWOId=@numParentWOID

	INSERT INTO WorkOrder
	(
		numItemCode,numQtyItemsReq,numWareHouseItemId,numCreatedBy,bintCreatedDate,numDomainID,numWOStatus,numOppId, numParentWOID,vcInstruction,numAssignedTo,bintCompliationDate
	)
	VALUES
	(
		@numItemCode,@numQty,@numWarehouseItemID,@numUserCntID,getutcdate(),@numDomainID,0,@numOppID,@numParentWOID,@vcInstruction,@numAssignedTo,@bintCompletionDate
	)

	SELECT @numWOID = SCOPE_IDENTITY()

	INSERT INTO [WorkOrderDetails] 
	(
		numWOId,numItemKitID,numChildItemID,numQtyItemsReq,numWareHouseItemId,vcItemDesc,sintOrder,numQtyItemsReq_Orig,numUOMId
	)
	SELECT 
		@numWOId,numItemKitID,numItemCode,
		CAST((DTL.numQtyItemsReq * @numQty)AS NUMERIC(9,0)),
		isnull(Dtl.numWareHouseItemId,0),
		ISNULL(Dtl.vcItemDesc,txtItemDesc),
		ISNULL(sintOrder,0),
		DTL.numQtyItemsReq,
		Dtl.numUOMId 
	FROM 
		item                                
	INNER JOIN 
		ItemDetails Dtl 
	ON 
		numChildItemID=numItemCode
	WHERE 
		numItemKitID=@numItemCode

	DECLARE @Description AS VARCHAR(1000)

	--UPDATE ON ORDER OF ASSEMBLY
	UPDATE 
		WareHouseItems
	SET    
		numOnOrder= ISNULL(numOnOrder,0) + @numQty,
		dtModified = GETDATE() 
	WHERE   
		numWareHouseItemID = @numWareHouseItemID 

	IF ISNULL(@bitFromWorkOrderScreen,0) = 0
	BEGIN
		
		SET @Description='SO-WO Work Order Created (Qty:' + CAST(@numQty AS VARCHAR(10)) + ')'

		--MAKE ENTRY IN WAREHOUSE TRACKING TABLE
		EXEC dbo.USP_ManageWareHouseItems_Tracking
		@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
		@numReferenceID = @numOppID, --  numeric(9, 0)
		@tintRefType = 3, --  tinyint
		@vcDescription = @Description, --  varchar(100)
		@numModifiedBy = @numUserCntID,
		@numDomainID = @numDomainID

		EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@numItemCode,@numWarehouseItemID,@numQtyShipped,1
	END
	ELSE IF ISNULL(@bitFromWorkOrderScreen,0) = 1
	BEGIN
		SET @Description='Work Order Created (Qty:' + CAST(@numQty AS VARCHAR(10)) + ')'

		--MAKE ENTRY IN WAREHOUSE TRACKING TABLE
		EXEC dbo.USP_ManageWareHouseItems_Tracking
		@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
		@numReferenceID = @numWOId, --  numeric(9, 0)
		@tintRefType = 2, --  tinyint
		@vcDescription = @Description, --  varchar(100)
		@numModifiedBy = @numUserCntID,
		@numDomainID = @numDomainID

		EXEC USP_ManageInventoryWorkOrder @numWOID,@numDomainID,@numUserCntID
	END
END


------------------------------- FUNCTIONS ----------------------------------


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetAssemblyPossibleWOQty')
DROP FUNCTION fn_GetAssemblyPossibleWOQty
GO
CREATE FUNCTION [dbo].[fn_GetAssemblyPossibleWOQty]
(
	@numItemCode as numeric(18,0)
)     
RETURNS INT 
AS      
BEGIN      
	DECLARE @numCount AS INT = 0;

	WITH CTE (numParentItemCode,numItemCode,vcItemName,numWarehouseItemID,numOnHand,numRequired) AS
	(
		SELECT 
			CAST(0 AS NUMERIC(18,0)),
			ItemDetails.numChildItemID,
			Item.vcItemName,
			WareHouseItems.numWareHouseItemID,
			WareHouseItems.numOnHand,
			ItemDetails.numQtyItemsReq
		FROM 
			ItemDetails 
		INNER JOIN
			Item
		ON
			ItemDetails.numChildItemID = Item.numItemCode
		INNER JOIN 
			WareHouseItems 
		ON 
			ItemDetails.numWareHouseItemId = WareHouseItems.numWareHouseItemID 
		WHERE 
			ItemDetails.numItemKitID = @numItemCode
		UNION ALL
		SELECT 
			CTE.numItemCode,
			ItemDetails.numChildItemID,
			Item.vcItemName,
			WareHouseItems.numWareHouseItemID,
			WareHouseItems.numOnHand,
			CAST((ItemDetails.numQtyItemsReq * CTE.numRequired) AS NUMERIC(18,0))
		FROM 
			CTE
		INNER JOIN
			ItemDetails 
		ON
			CTE.numItemCode = ItemDetails.numItemKitID
		INNER JOIN
			Item
		ON
			ItemDetails.numChildItemID = Item.numItemCode
		INNER JOIN 
			WareHouseItems 
		ON 
			ItemDetails.numWareHouseItemId = WareHouseItems.numWareHouseItemID 
	)

	SELECT @numCount = MIN(CAST((ISNULL(numOnHand,0)/(CASE WHEN ISNULL(numRequired,0) = 0 THEN 1 ELSE ISNULL(numRequired,0) END)) AS INT)) FROM CTE WHERE numItemCode NOT IN (SELECT numParentItemCode FROM CTE)

	RETURN ISNULL(@numCount,0)
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='fn_GetItemPriceLevel')
DROP FUNCTION fn_GetItemPriceLevel
GO
CREATE FUNCTION fn_GetItemPriceLevel
    (
      @numItemCode NUMERIC,
	  @numWareHouseItemID NUMERIC(18,0)
    )
RETURNS @PriceBookRules table
(
	intFromQty int,intToQty int,vcRuleType VARCHAR(50),vcDiscountType VARCHAR(50),
	decDiscount DECIMAL(18,2),monPriceLevelPrice MONEY,vcPriceLevelType VARCHAR(50)
)                               
AS 
BEGIN

DECLARE @monListPrice AS MONEY;SET @monListPrice=0
DECLARE @monVendorCost AS MONEY;SET @monVendorCost=0

if ((@numWareHouseItemID>0) and exists(select * from item where numItemCode=@numItemCode and  charItemType='P'))      
	begin      
		select @monListPrice=isnull(monWListPrice,0) from WareHouseItems where numWareHouseItemID=@numWareHouseItemID      
		if @monListPrice=0 select @monListPrice=monListPrice from Item where numItemCode=@numItemCode       
	end      
	else      
	begin      
		 select @monListPrice=monListPrice from Item where numItemCode=@numItemCode      
	end 


SELECT @monVendorCost = dbo.[fn_GetVendorCost](@numItemCode)


    INSERT INTO @PriceBookRules
	SELECT [intFromQty],[intToQty],CASE tintRuleType WHEN 1 THEN 'Deduct from List price'
												WHEN 2 THEN 'Add to Primary Vendor Cost'
												WHEN 3 THEN 'Named Price' END vcRuleType,
	CASE WHEN (tintRuleType=1 OR tintRuleType=2) AND tintDiscountType=1 THEN 'Percentage'
		 WHEN (tintRuleType=1 OR tintRuleType=2) AND tintDiscountType=2 THEN 'Flat'
		ELSE '' END vcDiscountType,CASE WHEN tintRuleType=3 THEN 0 ELSE decDiscount END AS decDiscount,
		CASE WHEN tintRuleType=1 AND tintDiscountType=1 --Deduct from List price & Percentage
					THEN @monListPrice - (@monListPrice * ( decDiscount /100))
                 WHEN tintRuleType=1 AND tintDiscountType=2 --Deduct from List price & Flat discount
					THEN @monListPrice - decDiscount
                 WHEN tintRuleType=2 AND tintDiscountType=1 --Add to Primary Vendor Cost & Percentage
					THEN @monVendorCost + (@monVendorCost * ( decDiscount /100))
                 WHEN tintRuleType=2 AND tintDiscountType=2 --Add to Primary Vendor Cost & Flat discount
					THEN @monVendorCost + decDiscount
                 WHEN tintRuleType=3 --Named Price
					THEN decDiscount
                END AS monPriceLevelPrice,'Item Price Level'											
        FROM    [PricingTable]
        WHERE   ISNULL(numItemCode,0)=@numItemCode
        ORDER BY [numPricingID] 


  declare @numRelationship as numeric(9);SET @numRelationship=0                
declare @numProfile as numeric(9);SET @numProfile=0  
  declare @numDivisionID as numeric(9);SET @numDivisionID=0                
                

INSERT INTO @PriceBookRules
	SELECT PT.[intFromQty],PT.[intToQty],CASE PT.tintRuleType WHEN 1 THEN 'Deduct from List price'
												WHEN 2 THEN 'Add to Primary Vendor Cost'
												WHEN 3 THEN 'Named Price' END vcRuleType,
	CASE WHEN (PT.tintRuleType=1 OR PT.tintRuleType=2) AND PT.tintDiscountType=1 THEN 'Percentage'
		 WHEN (PT.tintRuleType=1 OR PT.tintRuleType=2) AND PT.tintDiscountType=2 THEN 'Flat'
		ELSE '' END vcDiscountType,CASE WHEN PT.tintRuleType=3 THEN 0 ELSE PT.decDiscount END AS decDiscount,
		CASE WHEN PT.tintRuleType=1 AND PT.tintDiscountType=1 --Deduct from List price & Percentage
					THEN @monListPrice - (@monListPrice * ( PT.decDiscount /100))
                 WHEN PT.tintRuleType=1 AND PT.tintDiscountType=2 --Deduct from List price & Flat discount
					THEN @monListPrice - PT.decDiscount
                 WHEN PT.tintRuleType=2 AND PT.tintDiscountType=1 --Add to Primary Vendor Cost & Percentage
					THEN @monVendorCost + (@monVendorCost * ( PT.decDiscount /100))
                 WHEN PT.tintRuleType=2 AND PT.tintDiscountType=2 --Add to Primary Vendor Cost & Flat discount
					THEN @monVendorCost + PT.decDiscount
                 WHEN PT.tintRuleType=3 --Named Price
					THEN PT.decDiscount
                END AS monPriceLevelPrice,'Price Rule'	
FROM    Item I
        JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
       LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
         LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
        LEFT JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = P.[tintStep2] AND PP.[Step3Value] = P.[tintStep3]
        JOIN [PricingTable] PT ON P.numPricRuleID = PT.numPriceRuleID
WHERE   I.numItemCode = @numItemCode AND P.tintRuleFor=1 AND P.tintPricingMethod = 1
AND 
(
((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
)
ORDER BY PP.Priority ASC

Return
   END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetAlertDetail')
DROP FUNCTION GetAlertDetail
GO
CREATE FUNCTION [dbo].[GetAlertDetail]
(
	@numEmailHstrID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0),
	@numECampaignID NUMERIC(18,0),
	@numContactID NUMERIC(18,0),
	@bitOpenActionItem BIT,
	@bitOpencases BIT,
	@bitOpenProject BIT,
	@bitOpenSalesOpp BIT,
	@bitBalancedue BIT,
	@bitUnreadEmail BIT,
	@bitCampaign BIT,
	@TotalBalanceDue BIGINT,
	@OpenSalesOppCount BIGINT,
	@OpenCaseCount BIGINT,
	@OpenProjectCount BIGINT,
	@UnreadEmailCount BIGINT,
	@OpenActionItemCount BIGINT,
	@CampaignDTLCount BIGINT
)
Returns VARCHAR(MAX) 
As
BEGIN
	DECLARE @vcAlert AS VARCHAR(MAX) = ''
	
	--CHECK AR BALANCE
	IF @bitBalancedue = 1 AND  @TotalBalanceDue > 0 
    BEGIN 
		SET @vcAlert = '<a href="#" onclick="OpenAlertDetail(1,'+ CAST(@numDivisionID AS VARCHAR(18)) +',0)"><img alt="" src="../images/dollar.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" /></a>&nbsp;'
    END 

	--CHECK OPEN SALES ORDER COUNT
	IF @bitOpenSalesOpp = 1 AND @OpenSalesOppCount > 0 
    BEGIN 
		SET @vcAlert = @vcAlert + '<a href="#" onclick="OpenAlertDetail(2,'+ CAST(@numDivisionID AS VARCHAR(18)) +',0)"><img alt="" src="../images/icons/cart.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" /></a>&nbsp;'
    END 

	--CHECK OPEN CASES COUNT
	IF @bitOpencases = 1 AND @OpenCaseCount > 0 
    BEGIN 
		SET @vcAlert = @vcAlert + '<a href="#" onclick="OpenAlertDetail(3,'+ CAST(@numDivisionID AS VARCHAR(18)) +',0)"><img alt="" src="../images/icons/headphone_mic.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" /></a>&nbsp;'
    END 

	--CHECK OPEN PROJECT COUNT
	IF @bitOpenProject = 1 AND @OpenProjectCount > 0 
    BEGIN 
		SET @vcAlert = @vcAlert + '<a href="#" onclick="OpenAlertDetail(4,'+ CAST(@numDivisionID AS VARCHAR(18)) +',0)"><img alt="" src="../images/Compass-16.gif" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" /></a>&nbsp;'  
    END 

	--CHECK UNREAD MAIL COUNT
	IF @bitUnreadEmail = 1 AND @UnreadEmailCount > 0 
    BEGIN 
       SET @vcAlert = @vcAlert + '<a title="Recent Correspondance" class="hyperlink" onclick="OpenCorresPondance('+ CAST(@numContactID AS VARCHAR(10)) + ');">(' + CAST(@UnreadEmailCount AS VARCHAR(10)) + ')</a>&nbsp;'
    END
	
	--CHECK ACTIVE CAMPAIGN COUNT
	IF @bitCampaign = 1 AND ISNULL(@numECampaignID,0) > 0
	BEGIN
		
		IF @CampaignDTLCount = 0
		BEGIN
			SET @vcAlert = @vcAlert + '<img alt="" src="../images/comflag.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" />&nbsp;'  
		END
		ELSE
		BEGIN
			SET @vcAlert = @vcAlert + '<img alt="" src="../images/Circle_Green_16.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" />&nbsp;'  
		END
	END 

	--CHECK COUNT OF ACTION ITEM
	IF @bitOpenActionItem = 1 AND @OpenActionItemCount > 0 
	BEGIN
		SET @vcAlert = @vcAlert + '<a href="#" onclick="OpenAlertDetail(5,'+ CAST(@numDivisionID AS VARCHAR(18)) +',' + CAST(@numContactID AS VARCHAR(18)) + ')"><img alt="" src="../images/MasterList-16.gif" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" /></a>&nbsp;'
	END

	RETURN @vcAlert
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetCommissionContactItemList')
DROP FUNCTION GetCommissionContactItemList
GO
CREATE FUNCTION dbo.GetCommissionContactItemList
    (
      @numRuleID NUMERIC,
      @tintMode TINYINT 
    )
RETURNS VARCHAR(2000)
AS BEGIN
    DECLARE @ItemList VARCHAR(2000)

	IF @tintMode = 0
        BEGIN

            SELECT TOP 10
                    @ItemList = COALESCE(@ItemList + ', ', '') + CASE CC.bitCommContact WHEN 1 THEN (SELECT vcCompanyName FROM DivisionMaster D join CompanyInfo C on C.numCompanyID=D.numCompanyID  where D.numDivisionID=CC.numValue) + '(Commission Contact)' ELSE dbo.fn_GetContactName(numValue) END
             FROM    [CommissionRuleContacts] CC
					--INNER JOIN AdditionalContactsInformation A ON  CC.numValue=A.numContactID
            WHERE   CC.[numComRuleID] = @numRuleID
        END
        
    IF @tintMode = 1 
        BEGIN

            SELECT TOP 10
                    @ItemList = COALESCE(@ItemList + ', ', '') + vcItemName
            FROM    [CommissionRuleItems] CI
                    INNER JOIN Item I ON I.[numItemCode] = CI.[numValue]
            WHERE   CI.[numComRuleID] = @numRuleID
        END
      
      IF @tintMode = 2
        BEGIN

            SELECT TOP 10
                    @ItemList = COALESCE(@ItemList + ', ', '') + vcData
            FROM    [CommissionRuleItems] CI
                    INNER JOIN [ListDetails] LI ON LI.[numListItemID] = CI.[numValue]
            WHERE   CI.[numComRuleID] = @numRuleID
        END
       IF @tintMode = 3
        BEGIN

            SELECT TOP 10
                    @ItemList = COALESCE(@ItemList + ', ', '') + C.[vcCompanyName]
            FROM    [CommissionRuleOrganization] PD
                    INNER JOIN [DivisionMaster] DM ON DM.[numDivisionID] = PD.[numValue]
                    LEFT OUTER JOIN [CompanyInfo] C ON C.[numCompanyId] = DM.[numCompanyID]
                    LEFT OUTER JOIN [CommissionRules] PB ON PB.[numComRuleID] = PD.[numComRuleID]
            WHERE   PB.tintComOrgType = 1
                    AND PB.[numComRuleID] = @numRuleID
        END
        
        IF @tintMode = 4
        BEGIN

             SELECT TOP 10
                    @ItemList = COALESCE(@ItemList + ', ', '') + (LI.vcData +'/' + LI1.vcData)
            FROM    [CommissionRuleOrganization] PD
                    INNER JOIN [ListDetails] LI ON LI.[numListItemID] = PD.[numValue]
                    Inner JOIN [ListDetails] LI1 ON LI1.[numListItemID] = PD.[numProfile]
                    LEFT OUTER JOIN [CommissionRules] PB ON PB.[numComRuleID] = PD.[numComRuleID]
            WHERE   PB.tintComOrgType = 2
                    AND PB.[numComRuleID] = @numRuleID
        END
        
    RETURN ISNULL(@ItemList, '')
   END 
/****** Object:  UserDefinedFunction [dbo].[getContractRemainingHrsAmt]    Script Date: 07/26/2008 18:12:58 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getcontractremaininghrsamt')
DROP FUNCTION getcontractremaininghrsamt
GO
CREATE FUNCTION [dbo].[getContractRemainingHrsAmt](@mode as bit,@numContractId as numeric)                  
RETURNS DECIMAL(10,2)                  
BEGIN                  
 ------@mode = 1 for Amount ,0 for Hours                  
	DECLARE @Hours decimal(10,2) = 0                 
	DECLARE @Amount  decimal(10,2) = 0                 
	DECLARE @ContractHours  decimal(10,2) = 0                
	DECLARE @ContractAmount  decimal(10,2)  = 0        
             
        
	SELECT 
		@ContractHours=isnull(numHours,0),
		@ContractAmount=isnull(numAmount,0)         
	FROM 
		contractmanagement 
	WHERE 
		numcontractid = @numContractId          

	SELECT 
		@ContractAmount=@ContractAmount+isnull(sum(monAmount),0) 
	FROM 
		OpportunityBizDocsDetails  
	WHERE 
		numContractId =  @numContractId 
        
	IF @mode = 0             
	BEGIN            
		SELECT                   
			@Hours= isnull(sum(convert(decimal(10,2),datediff(minute,dtFromDate,dtToDate))/60 ),0)                               
		FROM 
			timeandexpense 
		WHERE 
			numType =1 
			AND numCategory =1 
			AND numContractid=@numContractId      
       
		IF @ContractHours > @Hours                   
		BEGIN              
			SET @Hours= @ContractHours - @Hours            
		END                  
		ELSE                  
		BEGIN                  
			SET @Hours= 0                  
		END                 
	END         
           
	IF @mode = 1                
	BEGIN                
		SELECT 
			@Amount= ISNULL(SUM(ISNULL(monAmount,0)),0)
		FROM 
			timeandexpense 
		WHERE 
			numType = 1 
			AND numCategory = 2
			AND numContractid=@numContractId                 
  
		SELECT 
			@Amount = @Amount + ISNULL(sum(monDealAmount),0) 
		FROM 
			(
				SELECT 
					CASE 
					WHEN PO.numOppBizDocID >0 
						THEN dbo.[GetDealAmount](PO.numoppID,GETDATE(),PO.numOppBizDocID) 
					WHEN PO.numBillID >0 
						THEN ISNULL((SELECT ISNULL(monAmount,0) FROM [OpportunityBizDocsDetails] WHERE numBizDocsPaymentDetId = PO.numBillID),0)
					ELSE 0
					END AS monDealAmount 
				FROM 
					ProjectsOpportunities PO 
				WHERE 
					numProId in (select numProId from ProjectsMaster where numContractId=@numContractId)) AS X
        
		IF @ContractAmount > @Amount                   
		BEGIN               
			SET @Amount = @ContractAmount - @Amount   
		END                     
		ELSE            
		BEGIN            
			SET @Amount= 0            
		END                            
	END          
                        
	DECLARE @ret as decimal(10,2)                  
              
	IF @mode = 1                  
		SET @ret= @Amount                  
	ELSE                   
		SET @ret= @Hours                  
	
	RETURN @ret                  
END
GO

