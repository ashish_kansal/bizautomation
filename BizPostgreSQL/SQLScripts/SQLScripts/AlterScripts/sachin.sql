------CREATE TABLE [dbo].[InboxTreeSort](
------	[numNodeID] [int] IDENTITY(1,1) NOT NULL,
------	[numParentID] [int] NULL,
------	[vcNodeName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
------	[numDomainID] [int] NULL,
------	[numUserCntID] [int] NULL,
------	[numSortOrder] [int] NULL,
------ CONSTRAINT [PK_InboxTreeSort] PRIMARY KEY CLUSTERED 
------(
------	[numNodeID] ASC
------)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
------) ON [PRIMARY]
------
------*----------------------------ON Second Update:4thJan2014-----------------------------*
------ALTER TABLE dbo.BizDocTemplate ADD
------	vcBizDocImagePath varchar(100) NULL,
------	vcBizDocFooter varchar(100) NULL,
------	vcPurBizDocFooter varchar(100) NULL
------	
--------INSERT INTO CFw_Grp_Master
--------                      (Grp_Name, Loc_Id, numDomainID, tintType, vcURLF)
--------VALUES     ('Bought & Sold',15,1,2,NULL)
--------
--------INSERT INTO CFw_Grp_Master
--------                      (Grp_Name, Loc_Id, numDomainID, tintType, vcURLF)
--------VALUES     ('Vended',15,1,2,NULL)
------
------INSERT INTO CFW_Loc_Master
------                      (Loc_name, vcFieldType, vcCustomLookBackTableName)
------VALUES     ('Items','D','CFW_FLD_Values')
------
------BEGIN TRANSACTION
------
------DELETE  FROM  dbo.CFw_Grp_Master WHERE numDomainID = 1 AND Loc_Id = 15
------DELETE  FROM  dbo.CFw_Grp_Master WHERE numDomainID = 1 AND Loc_Id = 13 AND Grp_Name='Items'
--------SELECT * FROM  domain
------DECLARE @numDomainID AS NUMERIC(18)
------SELECT ROW_NUMBER() OVER (ORDER BY numDomainID)AS [RowID], numDomainID  INTO #tempDomains FROM dbo.Domain WHERE numDomainId > 0
------DECLARE @intCnt AS INT
------DECLARE @intCntDomain AS INT
------SET @intCnt = 0
------SET @intCntDomain = (SELECT COUNT(*) FROM #tempDomains)
------
------IF @intCntDomain > 0
------BEGIN
------WHILE(@intCnt < @intCntDomain)
------BEGIN
------
------SET @intCnt = @intCnt + 1
------SELECT @numDomainID = numDomainID FROM #tempDomains WHERE RowID = @intCnt
------PRINT @numDomainID
--------SELECT * FROM #tempDomains
------ 
------
------exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Items',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
------exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=15,@TabName='Bought & Sold',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
------exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=15,@TabName='Vended',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
------
------END 
------
------END
------
--------exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=15,@TabName='Bought & Sold',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
--------exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=15,@TabName='Vended',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
------
------DROP TABLE #tempDomains 
------
------ROLLBACK

----------------------*E-Commerce*-----------------------
-------Task:1:Price,by default:New Arrival,Page size---
------ALTER TABLE dbo.eCommerceDTL ADD
------	bitSortPriceMode bit NULL,
------	numPageSize int NULL,
------	numPageVariant int NULL
------	
------UPDATE eCommerceDTL SET numPageSize=15,numPageVariant=6
--------End--
------SET IDENTITY_INSERT DycFieldMaster ON
------
------	INSERT INTO	dbo.DycFieldMaster (numFieldId,
------	numModuleID,
------	numDomainID,
------	vcFieldName,
------	vcDbColumnName,
------	vcOrigDbColumnName,
------	vcPropertyName,
------	vcLookBackTableName,
------	vcFieldDataType,
------	vcFieldType,
------	vcAssociatedControlType,
------	vcToolTip,
------	vcListItemType,
------	numListID,
------	PopupFunctionName,
------	[order],
------	tintRow,
------	tintColumn,
------	bitInResults,
------	bitDeleted,
------	bitAllowEdit,
------	bitDefault,
------	bitSettingField,
------	bitAddField,
------	bitDetailField,
------	bitAllowSorting,
------	bitWorkFlowField,
------	bitImport,
------	bitExport,
------	bitAllowFiltering,
------	bitInlineEdit,
------	bitRequired,
------	intColumnWidth,
------	intFieldMaxLength
------) VALUES (
------    /*numFieldId*/	507,
------	/* numModuleID - numeric(18, 0) */ 4,
------	/* numDomainID - numeric(18, 0) */ NULL,
------	/* vcFieldName - nvarchar(50) */'Title:A-Z',
------	/* vcDbColumnName - nvarchar(50) */ 'Title',
------	/* vcOrigDbColumnName - nvarchar(50) */ 'Title',
------	/* vcPropertyName - varchar(100) */ 'Title',
------	/* vcLookBackTableName - nvarchar(50) */ 'Item',
------	/* vcFieldDataType - char(1) */ 'V',
------	/* vcFieldType - char(1) */ 'R',
------	/* vcAssociatedControlType - nvarchar(50) */ 'TextBox',
------	/* vcToolTip - nvarchar(1000) */ NULL,
------	/* vcListItemType - varchar(3) */ NULL,
------	/* numListID - numeric(18, 0) */ 0,
------	/* PopupFunctionName - varchar(100) */ NULL,
------	/* order - tinyint */ 1,
------	/* tintRow - tinyint */ NULL,
------	/* tintColumn - tinyint */ NULL,
------	/* bitInResults - bit */ 1,
------	/* bitDeleted - bit */ 0,
------	/* bitAllowEdit - bit */ 1,
------	/* bitDefault - bit */ 1,
------	/* bitSettingField - bit */ 1,
------	/* bitAddField - bit */ NULL,
------	/* bitDetailField - bit */ NULL,
------	/* bitAllowSorting - bit */ 1,
------	/* bitWorkFlowField - bit */ NULL,
------	/* bitImport - bit */ NULL,
------	/* bitExport - bit */ NULL,
------	/* bitAllowFiltering - bit */ 1,
------	/* bitInlineEdit - bit */ NULL,
------	/* bitRequired - bit */ NULL,
------	/* intColumnWidth - int */ NULL,
------	/* intFieldMaxLength - int */ NULL ) 
------	
------	INSERT INTO	dbo.DycFieldMaster (numFieldId,
------	numModuleID,
------	numDomainID,
------	vcFieldName,
------	vcDbColumnName,
------	vcOrigDbColumnName,
------	vcPropertyName,
------	vcLookBackTableName,
------	vcFieldDataType,
------	vcFieldType,
------	vcAssociatedControlType,
------	vcToolTip,
------	vcListItemType,
------	numListID,
------	PopupFunctionName,
------	[order],
------	tintRow,
------	tintColumn,
------	bitInResults,
------	bitDeleted,
------	bitAllowEdit,
------	bitDefault,
------	bitSettingField,
------	bitAddField,
------	bitDetailField,
------	bitAllowSorting,
------	bitWorkFlowField,
------	bitImport,
------	bitExport,
------	bitAllowFiltering,
------	bitInlineEdit,
------	bitRequired,
------	intColumnWidth,
------	intFieldMaxLength
------) VALUES (
------    /*numFieldId*/	508,
------	/* numModuleID - numeric(18, 0) */ 4,
------	/* numDomainID - numeric(18, 0) */ NULL,
------	/* vcFieldName - nvarchar(50) */'Title:Z-A',
------	/* vcDbColumnName - nvarchar(50) */ 'Title',
------	/* vcOrigDbColumnName - nvarchar(50) */ 'Title',
------	/* vcPropertyName - varchar(100) */ 'Title',
------	/* vcLookBackTableName - nvarchar(50) */ 'Item',
------	/* vcFieldDataType - char(1) */ 'V',
------	/* vcFieldType - char(1) */ 'R',
------	/* vcAssociatedControlType - nvarchar(50) */ 'TextBox',
------	/* vcToolTip - nvarchar(1000) */ NULL,
------	/* vcListItemType - varchar(3) */ NULL,
------	/* numListID - numeric(18, 0) */ 0,
------	/* PopupFunctionName - varchar(100) */ NULL,
------	/* order - tinyint */ 1,
------	/* tintRow - tinyint */ NULL,
------	/* tintColumn - tinyint */ NULL,
------	/* bitInResults - bit */ 1,
------	/* bitDeleted - bit */ 0,
------	/* bitAllowEdit - bit */ 1,
------	/* bitDefault - bit */ 1,
------	/* bitSettingField - bit */ 1,
------	/* bitAddField - bit */ NULL,
------	/* bitDetailField - bit */ NULL,
------	/* bitAllowSorting - bit */ 1,
------	/* bitWorkFlowField - bit */ NULL,
------	/* bitImport - bit */ NULL,
------	/* bitExport - bit */ NULL,
------	/* bitAllowFiltering - bit */ 1,
------	/* bitInlineEdit - bit */ NULL,
------	/* bitRequired - bit */ NULL,
------	/* intColumnWidth - int */ NULL,
------	/* intFieldMaxLength - int */ NULL ) 
------
------INSERT INTO	dbo.DycFieldMaster (numFieldId,
------	numModuleID,
------	numDomainID,
------	vcFieldName,
------	vcDbColumnName,
------	vcOrigDbColumnName,
------	vcPropertyName,
------	vcLookBackTableName,
------	vcFieldDataType,
------	vcFieldType,
------	vcAssociatedControlType,
------	vcToolTip,
------	vcListItemType,
------	numListID,
------	PopupFunctionName,
------	[order],
------	tintRow,
------	tintColumn,
------	bitInResults,
------	bitDeleted,
------	bitAllowEdit,
------	bitDefault,
------	bitSettingField,
------	bitAddField,
------	bitDetailField,
------	bitAllowSorting,
------	bitWorkFlowField,
------	bitImport,
------	bitExport,
------	bitAllowFiltering,
------	bitInlineEdit,
------	bitRequired,
------	intColumnWidth,
------	intFieldMaxLength
------) VALUES (
------    /*numFieldId*/	509,
------	/* numModuleID - numeric(18, 0) */ 4,
------	/* numDomainID - numeric(18, 0) */ NULL,
------	/* vcFieldName - nvarchar(50) */'Price:Low to High',
------	/* vcDbColumnName - nvarchar(50) */ 'Price',
------	/* vcOrigDbColumnName - nvarchar(50) */ 'Price',
------	/* vcPropertyName - varchar(100) */ 'Price',
------	/* vcLookBackTableName - nvarchar(50) */ 'Price',
------	/* vcFieldDataType - char(1) */ 'V',
------	/* vcFieldType - char(1) */ 'R',
------	/* vcAssociatedControlType - nvarchar(50) */ 'TextBox',
------	/* vcToolTip - nvarchar(1000) */ NULL,
------	/* vcListItemType - varchar(3) */ NULL,
------	/* numListID - numeric(18, 0) */ 0,
------	/* PopupFunctionName - varchar(100) */ NULL,
------	/* order - tinyint */ 1,
------	/* tintRow - tinyint */ NULL,
------	/* tintColumn - tinyint */ NULL,
------	/* bitInResults - bit */ 1,
------	/* bitDeleted - bit */ 0,
------	/* bitAllowEdit - bit */ 1,
------	/* bitDefault - bit */ 1,
------	/* bitSettingField - bit */ 1,
------	/* bitAddField - bit */ NULL,
------	/* bitDetailField - bit */ NULL,
------	/* bitAllowSorting - bit */ 1,
------	/* bitWorkFlowField - bit */ NULL,
------	/* bitImport - bit */ NULL,
------	/* bitExport - bit */ NULL,
------	/* bitAllowFiltering - bit */ 1,
------	/* bitInlineEdit - bit */ NULL,
------	/* bitRequired - bit */ NULL,
------	/* intColumnWidth - int */ NULL,
------	/* intFieldMaxLength - int */ NULL ) 
------	
------	INSERT INTO	dbo.DycFieldMaster (numFieldId,
------	numModuleID,
------	numDomainID,
------	vcFieldName,
------	vcDbColumnName,
------	vcOrigDbColumnName,
------	vcPropertyName,
------	vcLookBackTableName,
------	vcFieldDataType,
------	vcFieldType,
------	vcAssociatedControlType,
------	vcToolTip,
------	vcListItemType,
------	numListID,
------	PopupFunctionName,
------	[order],
------	tintRow,
------	tintColumn,
------	bitInResults,
------	bitDeleted,
------	bitAllowEdit,
------	bitDefault,
------	bitSettingField,
------	bitAddField,
------	bitDetailField,
------	bitAllowSorting,
------	bitWorkFlowField,
------	bitImport,
------	bitExport,
------	bitAllowFiltering,
------	bitInlineEdit,
------	bitRequired,
------	intColumnWidth,
------	intFieldMaxLength
------) VALUES (
------    /*numFieldId*/	510,
------	/* numModuleID - numeric(18, 0) */ 4,
------	/* numDomainID - numeric(18, 0) */ NULL,
------	/* vcFieldName - nvarchar(50) */'Price:High to Low',
------	/* vcDbColumnName - nvarchar(50) */ 'Price',
------	/* vcOrigDbColumnName - nvarchar(50) */ 'Price',
------	/* vcPropertyName - varchar(100) */ 'Price',
------	/* vcLookBackTableName - nvarchar(50) */ 'Price',
------	/* vcFieldDataType - char(1) */ 'V',
------	/* vcFieldType - char(1) */ 'R',
------	/* vcAssociatedControlType - nvarchar(50) */ 'TextBox',
------	/* vcToolTip - nvarchar(1000) */ NULL,
------	/* vcListItemType - varchar(3) */ NULL,
------	/* numListID - numeric(18, 0) */ 0,
------	/* PopupFunctionName - varchar(100) */ NULL,
------	/* order - tinyint */ 1,
------	/* tintRow - tinyint */ NULL,
------	/* tintColumn - tinyint */ NULL,
------	/* bitInResults - bit */ 1,
------	/* bitDeleted - bit */ 0,
------	/* bitAllowEdit - bit */ 1,
------	/* bitDefault - bit */ 1,
------	/* bitSettingField - bit */ 1,
------	/* bitAddField - bit */ NULL,
------	/* bitDetailField - bit */ NULL,
------	/* bitAllowSorting - bit */ 1,
------	/* bitWorkFlowField - bit */ NULL,
------	/* bitImport - bit */ NULL,
------	/* bitExport - bit */ NULL,
------	/* bitAllowFiltering - bit */ 1,
------	/* bitInlineEdit - bit */ NULL,
------	/* bitRequired - bit */ NULL,
------	/* intColumnWidth - int */ NULL,
------	/* intFieldMaxLength - int */ NULL ) 
------	
------	INSERT INTO	dbo.DycFieldMaster (numFieldId,
------	numModuleID,
------	numDomainID,
------	vcFieldName,
------	vcDbColumnName,
------	vcOrigDbColumnName,
------	vcPropertyName,
------	vcLookBackTableName,
------	vcFieldDataType,
------	vcFieldType,
------	vcAssociatedControlType,
------	vcToolTip,
------	vcListItemType,
------	numListID,
------	PopupFunctionName,
------	[order],
------	tintRow,
------	tintColumn,
------	bitInResults,
------	bitDeleted,
------	bitAllowEdit,
------	bitDefault,
------	bitSettingField,
------	bitAddField,
------	bitDetailField,
------	bitAllowSorting,
------	bitWorkFlowField,
------	bitImport,
------	bitExport,
------	bitAllowFiltering,
------	bitInlineEdit,
------	bitRequired,
------	intColumnWidth,
------	intFieldMaxLength
------) VALUES (
------    /*numFieldId*/	511,
------	/* numModuleID - numeric(18, 0) */ 4,
------	/* numDomainID - numeric(18, 0) */ NULL,
------	/* vcFieldName - nvarchar(50) */'New Arrival',
------	/* vcDbColumnName - nvarchar(50) */ 'New Arrival',
------	/* vcOrigDbColumnName - nvarchar(50) */ 'New Arrival',
------	/* vcPropertyName - varchar(100) */ 'New Arrival',
------	/* vcLookBackTableName - nvarchar(50) */ 'New Arrival',
------	/* vcFieldDataType - char(1) */ 'V',
------	/* vcFieldType - char(1) */ 'R',
------	/* vcAssociatedControlType - nvarchar(50) */ 'TextBox',
------	/* vcToolTip - nvarchar(1000) */ NULL,
------	/* vcListItemType - varchar(3) */ NULL,
------	/* numListID - numeric(18, 0) */ 0,
------	/* PopupFunctionName - varchar(100) */ NULL,
------	/* order - tinyint */ 1,
------	/* tintRow - tinyint */ NULL,
------	/* tintColumn - tinyint */ NULL,
------	/* bitInResults - bit */ 1,
------	/* bitDeleted - bit */ 0,
------	/* bitAllowEdit - bit */ 1,
------	/* bitDefault - bit */ 1,
------	/* bitSettingField - bit */ 1,
------	/* bitAddField - bit */ NULL,
------	/* bitDetailField - bit */ NULL,
------	/* bitAllowSorting - bit */ 1,
------	/* bitWorkFlowField - bit */ NULL,
------	/* bitImport - bit */ NULL,
------	/* bitExport - bit */ NULL,
------	/* bitAllowFiltering - bit */ 1,
------	/* bitInlineEdit - bit */ NULL,
------	/* bitRequired - bit */ NULL,
------	/* intColumnWidth - int */ NULL,
------	/* intFieldMaxLength - int */ NULL )
------	
------	INSERT INTO	dbo.DycFieldMaster (numFieldId,
------	numModuleID,
------	numDomainID,
------	vcFieldName,
------	vcDbColumnName,
------	vcOrigDbColumnName,
------	vcPropertyName,
------	vcLookBackTableName,
------	vcFieldDataType,
------	vcFieldType,
------	vcAssociatedControlType,
------	vcToolTip,
------	vcListItemType,
------	numListID,
------	PopupFunctionName,
------	[order],
------	tintRow,
------	tintColumn,
------	bitInResults,
------	bitDeleted,
------	bitAllowEdit,
------	bitDefault,
------	bitSettingField,
------	bitAddField,
------	bitDetailField,
------	bitAllowSorting,
------	bitWorkFlowField,
------	bitImport,
------	bitExport,
------	bitAllowFiltering,
------	bitInlineEdit,
------	bitRequired,
------	intColumnWidth,
------	intFieldMaxLength
------) VALUES (
------    /*numFieldId*/	512,
------	/* numModuleID - numeric(18, 0) */ 4,
------	/* numDomainID - numeric(18, 0) */ NULL,
------	/* vcFieldName - nvarchar(50) */'Oldest',
------	/* vcDbColumnName - nvarchar(50) */ 'Oldest',
------	/* vcOrigDbColumnName - nvarchar(50) */ 'Oldest',
------	/* vcPropertyName - varchar(100) */ 'Oldest',
------	/* vcLookBackTableName - nvarchar(50) */ 'Oldest',
------	/* vcFieldDataType - char(1) */ 'V',
------	/* vcFieldType - char(1) */ 'R',
------	/* vcAssociatedControlType - nvarchar(50) */ 'TextBox',
------	/* vcToolTip - nvarchar(1000) */ NULL,
------	/* vcListItemType - varchar(3) */ NULL,
------	/* numListID - numeric(18, 0) */ 0,
------	/* PopupFunctionName - varchar(100) */ NULL,
------	/* order - tinyint */ 1,
------	/* tintRow - tinyint */ NULL,
------	/* tintColumn - tinyint */ NULL,
------	/* bitInResults - bit */ 1,
------	/* bitDeleted - bit */ 0,
------	/* bitAllowEdit - bit */ 1,
------	/* bitDefault - bit */ 1,
------	/* bitSettingField - bit */ 1,
------	/* bitAddField - bit */ NULL,
------	/* bitDetailField - bit */ NULL,
------	/* bitAllowSorting - bit */ 1,
------	/* bitWorkFlowField - bit */ NULL,
------	/* bitImport - bit */ NULL,
------	/* bitExport - bit */ NULL,
------	/* bitAllowFiltering - bit */ 1,
------	/* bitInlineEdit - bit */ NULL,
------	/* bitRequired - bit */ NULL,
------	/* intColumnWidth - int */ NULL,
------	/* intFieldMaxLength - int */ NULL )
------	
------	SET IDENTITY_INSERT DycFieldMaster OFF
------	
------	--Insert into FieldMapping
------	
------INSERT INTO	dbo.DycFormField_Mapping (
------		numModuleID,
------		numFieldID,
------		numDomainID,
------		numFormID,
------		bitAllowEdit,
------		bitInlineEdit,
------		vcFieldName,
------		vcAssociatedControlType,
------		vcPropertyName,
------		PopupFunctionName,
------		[order],
------		tintRow,
------		tintColumn,
------		bitInResults,
------		bitDeleted,
------		bitDefault,
------		bitSettingField,
------		bitAddField,
------		bitDetailField,
------		bitAllowSorting,
------		bitWorkFlowField,
------		bitImport,
------		bitExport,
------		bitAllowFiltering,
------		bitRequired,
------		numFormFieldID,
------		intSectionID,
------		bitAllowGridColor
------	) VALUES 
------	 (	
------		/* numModuleID - numeric(18, 0) */ 4,
------		/* numFieldID - numeric(18, 0) */ 507,
------		/* numDomainID - numeric(18, 0) */ NULL,
------		/* numFormID - numeric(18, 0) */ 85,
------		/* bitAllowEdit - bit */ 1,
------		/* bitInlineEdit - bit */ 1,
------		/* vcFieldName - nvarchar(50) */ 'Title:A-Z',
------		/* vcAssociatedControlType - nvarchar(50) */ 'TextBox',
------		/* vcPropertyName - varchar(100) */ NULL,
------		/* PopupFunctionName - varchar(100) */ NULL,
------		/* order - tinyint */ 1,
------		/* tintRow - tinyint */ NULL,
------		/* tintColumn - tinyint */ NULL,
------		/* bitInResults - bit */ 1,
------		/* bitDeleted - bit */ 0,
------		/* bitDefault - bit */ 0,
------		/* bitSettingField - bit */ 1,
------		/* bitAddField - bit */ NULL,
------		/* bitDetailField - bit */ NULL,
------		/* bitAllowSorting - bit */ NULL,
------		/* bitWorkFlowField - bit */ NULL,
------		/* bitImport - bit */ 0,
------		/* bitExport - bit */ NULL,
------		/* bitAllowFiltering - bit */ 1,
------		/* bitRequired - bit */ NULL,
------		/* numFormFieldID - numeric(18, 0) */ NULL,
------		/* intSectionID - int */ 1,
------		/* bitAllowGridColor - bit */ NULL ) 
------		
------		
------		INSERT INTO	dbo.DycFormField_Mapping (
------		numModuleID,
------		numFieldID,
------		numDomainID,
------		numFormID,
------		bitAllowEdit,
------		bitInlineEdit,
------		vcFieldName,
------		vcAssociatedControlType,
------		vcPropertyName,
------		PopupFunctionName,
------		[order],
------		tintRow,
------		tintColumn,
------		bitInResults,
------		bitDeleted,
------		bitDefault,
------		bitSettingField,
------		bitAddField,
------		bitDetailField,
------		bitAllowSorting,
------		bitWorkFlowField,
------		bitImport,
------		bitExport,
------		bitAllowFiltering,
------		bitRequired,
------		numFormFieldID,
------		intSectionID,
------		bitAllowGridColor
------	) VALUES 
------	 (	
------		/* numModuleID - numeric(18, 0) */ 4,
------		/* numFieldID - numeric(18, 0) */ 508,
------		/* numDomainID - numeric(18, 0) */ NULL,
------		/* numFormID - numeric(18, 0) */ 85,
------		/* bitAllowEdit - bit */ 1,
------		/* bitInlineEdit - bit */ 1,
------		/* vcFieldName - nvarchar(50) */ 'Title:Z-A',
------		/* vcAssociatedControlType - nvarchar(50) */ 'TextBox',
------		/* vcPropertyName - varchar(100) */ NULL,
------		/* PopupFunctionName - varchar(100) */ NULL,
------		/* order - tinyint */ 1,
------		/* tintRow - tinyint */ NULL,
------		/* tintColumn - tinyint */ NULL,
------		/* bitInResults - bit */ 1,
------		/* bitDeleted - bit */ 0,
------		/* bitDefault - bit */ 0,
------		/* bitSettingField - bit */ 1,
------		/* bitAddField - bit */ NULL,
------		/* bitDetailField - bit */ NULL,
------		/* bitAllowSorting - bit */ NULL,
------		/* bitWorkFlowField - bit */ NULL,
------		/* bitImport - bit */ 0,
------		/* bitExport - bit */ NULL,
------		/* bitAllowFiltering - bit */ 1,
------		/* bitRequired - bit */ NULL,
------		/* numFormFieldID - numeric(18, 0) */ NULL,
------		/* intSectionID - int */ 1,
------		/* bitAllowGridColor - bit */ NULL ) 
------		
------			
------		INSERT INTO	dbo.DycFormField_Mapping (
------		numModuleID,
------		numFieldID,
------		numDomainID,
------		numFormID,
------		bitAllowEdit,
------		bitInlineEdit,
------		vcFieldName,
------		vcAssociatedControlType,
------		vcPropertyName,
------		PopupFunctionName,
------		[order],
------		tintRow,
------		tintColumn,
------		bitInResults,
------		bitDeleted,
------		bitDefault,
------		bitSettingField,
------		bitAddField,
------		bitDetailField,
------		bitAllowSorting,
------		bitWorkFlowField,
------		bitImport,
------		bitExport,
------		bitAllowFiltering,
------		bitRequired,
------		numFormFieldID,
------		intSectionID,
------		bitAllowGridColor
------	) VALUES 
------	 (	
------		/* numModuleID - numeric(18, 0) */ 4,
------		/* numFieldID - numeric(18, 0) */ 509,
------		/* numDomainID - numeric(18, 0) */ NULL,
------		/* numFormID - numeric(18, 0) */ 85,
------		/* bitAllowEdit - bit */ 1,
------		/* bitInlineEdit - bit */ 1,
------		/* vcFieldName - nvarchar(50) */ 'Price:Low to High',
------		/* vcAssociatedControlType - nvarchar(50) */ 'TextBox',
------		/* vcPropertyName - varchar(100) */ NULL,
------		/* PopupFunctionName - varchar(100) */ NULL,
------		/* order - tinyint */ 1,
------		/* tintRow - tinyint */ NULL,
------		/* tintColumn - tinyint */ NULL,
------		/* bitInResults - bit */ 1,
------		/* bitDeleted - bit */ 0,
------		/* bitDefault - bit */ 0,
------		/* bitSettingField - bit */ 1,
------		/* bitAddField - bit */ NULL,
------		/* bitDetailField - bit */ NULL,
------		/* bitAllowSorting - bit */ NULL,
------		/* bitWorkFlowField - bit */ NULL,
------		/* bitImport - bit */ 0,
------		/* bitExport - bit */ NULL,
------		/* bitAllowFiltering - bit */ 1,
------		/* bitRequired - bit */ NULL,
------		/* numFormFieldID - numeric(18, 0) */ NULL,
------		/* intSectionID - int */ 1,
------		/* bitAllowGridColor - bit */ NULL ) 
------		
------		INSERT INTO	dbo.DycFormField_Mapping (
------		numModuleID,
------		numFieldID,
------		numDomainID,
------		numFormID,
------		bitAllowEdit,
------		bitInlineEdit,
------		vcFieldName,
------		vcAssociatedControlType,
------		vcPropertyName,
------		PopupFunctionName,
------		[order],
------		tintRow,
------		tintColumn,
------		bitInResults,
------		bitDeleted,
------		bitDefault,
------		bitSettingField,
------		bitAddField,
------		bitDetailField,
------		bitAllowSorting,
------		bitWorkFlowField,
------		bitImport,
------		bitExport,
------		bitAllowFiltering,
------		bitRequired,
------		numFormFieldID,
------		intSectionID,
------		bitAllowGridColor
------	) VALUES 
------	 (	
------		/* numModuleID - numeric(18, 0) */ 4,
------		/* numFieldID - numeric(18, 0) */ 510,
------		/* numDomainID - numeric(18, 0) */ NULL,
------		/* numFormID - numeric(18, 0) */ 85,
------		/* bitAllowEdit - bit */ 1,
------		/* bitInlineEdit - bit */ 1,
------		/* vcFieldName - nvarchar(50) */ 'Price:High to Low',
------		/* vcAssociatedControlType - nvarchar(50) */ 'TextBox',
------		/* vcPropertyName - varchar(100) */ NULL,
------		/* PopupFunctionName - varchar(100) */ NULL,
------		/* order - tinyint */ 1,
------		/* tintRow - tinyint */ NULL,
------		/* tintColumn - tinyint */ NULL,
------		/* bitInResults - bit */ 1,
------		/* bitDeleted - bit */ 0,
------		/* bitDefault - bit */ 0,
------		/* bitSettingField - bit */ 1,
------		/* bitAddField - bit */ NULL,
------		/* bitDetailField - bit */ NULL,
------		/* bitAllowSorting - bit */ NULL,
------		/* bitWorkFlowField - bit */ NULL,
------		/* bitImport - bit */ 0,
------		/* bitExport - bit */ NULL,
------		/* bitAllowFiltering - bit */ 1,
------		/* bitRequired - bit */ NULL,
------		/* numFormFieldID - numeric(18, 0) */ NULL,
------		/* intSectionID - int */ 1,
------		/* bitAllowGridColor - bit */ NULL ) 
------		
------		INSERT INTO	dbo.DycFormField_Mapping (
------		numModuleID,
------		numFieldID,
------		numDomainID,
------		numFormID,
------		bitAllowEdit,
------		bitInlineEdit,
------		vcFieldName,
------		vcAssociatedControlType,
------		vcPropertyName,
------		PopupFunctionName,
------		[order],
------		tintRow,
------		tintColumn,
------		bitInResults,
------		bitDeleted,
------		bitDefault,
------		bitSettingField,
------		bitAddField,
------		bitDetailField,
------		bitAllowSorting,
------		bitWorkFlowField,
------		bitImport,
------		bitExport,
------		bitAllowFiltering,
------		bitRequired,
------		numFormFieldID,
------		intSectionID,
------		bitAllowGridColor
------	) VALUES 
------	 (	
------		/* numModuleID - numeric(18, 0) */ 4,
------		/* numFieldID - numeric(18, 0) */ 511,
------		/* numDomainID - numeric(18, 0) */ NULL,
------		/* numFormID - numeric(18, 0) */ 85,
------		/* bitAllowEdit - bit */ 1,
------		/* bitInlineEdit - bit */ 1,
------		/* vcFieldName - nvarchar(50) */ 'New Arrival',
------		/* vcAssociatedControlType - nvarchar(50) */ 'TextBox',
------		/* vcPropertyName - varchar(100) */ NULL,
------		/* PopupFunctionName - varchar(100) */ NULL,
------		/* order - tinyint */ 1,
------		/* tintRow - tinyint */ NULL,
------		/* tintColumn - tinyint */ NULL,
------		/* bitInResults - bit */ 1,
------		/* bitDeleted - bit */ 0,
------		/* bitDefault - bit */ 0,
------		/* bitSettingField - bit */ 1,
------		/* bitAddField - bit */ NULL,
------		/* bitDetailField - bit */ NULL,
------		/* bitAllowSorting - bit */ NULL,
------		/* bitWorkFlowField - bit */ NULL,
------		/* bitImport - bit */ 0,
------		/* bitExport - bit */ NULL,
------		/* bitAllowFiltering - bit */ 1,
------		/* bitRequired - bit */ NULL,
------		/* numFormFieldID - numeric(18, 0) */ NULL,
------		/* intSectionID - int */ 1,
------		/* bitAllowGridColor - bit */ NULL ) 
------		
------		INSERT INTO	dbo.DycFormField_Mapping (
------		numModuleID,
------		numFieldID,
------		numDomainID,
------		numFormID,
------		bitAllowEdit,
------		bitInlineEdit,
------		vcFieldName,
------		vcAssociatedControlType,
------		vcPropertyName,
------		PopupFunctionName,
------		[order],
------		tintRow,
------		tintColumn,
------		bitInResults,
------		bitDeleted,
------		bitDefault,
------		bitSettingField,
------		bitAddField,
------		bitDetailField,
------		bitAllowSorting,
------		bitWorkFlowField,
------		bitImport,
------		bitExport,
------		bitAllowFiltering,
------		bitRequired,
------		numFormFieldID,
------		intSectionID,
------		bitAllowGridColor
------	) VALUES 
------	 (	
------		/* numModuleID - numeric(18, 0) */ 4,
------		/* numFieldID - numeric(18, 0) */ 512,
------		/* numDomainID - numeric(18, 0) */ NULL,
------		/* numFormID - numeric(18, 0) */ 85,
------		/* bitAllowEdit - bit */ 1,
------		/* bitInlineEdit - bit */ 1,
------		/* vcFieldName - nvarchar(50) */ 'Oldest',
------		/* vcAssociatedControlType - nvarchar(50) */ 'TextBox',
------		/* vcPropertyName - varchar(100) */ NULL,
------		/* PopupFunctionName - varchar(100) */ NULL,
------		/* order - tinyint */ 1,
------		/* tintRow - tinyint */ NULL,
------		/* tintColumn - tinyint */ NULL,
------		/* bitInResults - bit */ 1,
------		/* bitDeleted - bit */ 0,
------		/* bitDefault - bit */ 0,
------		/* bitSettingField - bit */ 1,
------		/* bitAddField - bit */ NULL,
------		/* bitDetailField - bit */ NULL,
------		/* bitAllowSorting - bit */ NULL,
------		/* bitWorkFlowField - bit */ NULL,
------		/* bitImport - bit */ 0,
------		/* bitExport - bit */ NULL,
------		/* bitAllowFiltering - bit */ 1,
------		/* bitRequired - bit */ NULL,
------		/* numFormFieldID - numeric(18, 0) */ NULL,
------		/* intSectionID - int */ 1,
------		/* bitAllowGridColor - bit */ NULL ) 
------	

----/*********************************************************************************************************************************************************/
------TO Insert default tab entry
------Run For EasterBikes issue
------SELECT * FROM domain
------DECLARE @numDomainID AS NUMERIC(18)
------SET @numDomainID = 176
------SELECT * FROM CFw_Grp_Master WHERE numDomainID = @numDomainID
--------SELECT * FROM CFW_Loc_Master WHERE numDomainID = @numDomainID
------
------
------INSERT INTO CFw_Grp_Master
------                      (Grp_Name, Loc_Id, numDomainID, tintType, vcURLF)
------VALUES     ('Items',13,@numDomainID,2,NULL)
------
------INSERT INTO CFw_Grp_Master
------                      (Grp_Name, Loc_Id, numDomainID, tintType, vcURLF)
------VALUES     ('Bought & Sold',15,@numDomainID,2,NULL)
------
------INSERT INTO CFw_Grp_Master
------                      (Grp_Name, Loc_Id, numDomainID, tintType, vcURLF)

----/*********************************************************************************************************************************************************/
----/*Date:5thFeb2014||ISSUE:# Fields could not be replaced using code(On Each New Subscription)*/
------To Update Domains 
------trace defected Domains
------SELECT * FROM dbo.Domain WHERE numDomainId IN (SELECT DISTINCT(numDomainId) FROM dbo.BizDocTemplate
------WHERE txtBizDocTemplate LIKE '%#Customer/VendorBill-toAddress#%' )--AND numOppType = 1)=>#Customer/VendorBillToAddress#
------
------SELECT * FROM dbo.Domain WHERE numDomainId IN (SELECT DISTINCT(numDomainId) FROM dbo.BizDocTemplate
------WHERE txtBizDocTemplate LIKE '%#Customer/VendorShip-toAddress#%' )--AND numOppType = 1)=>#Customer/VendorShipToAddress#      
------
------SELECT * FROM dbo.Domain WHERE numDomainId IN (SELECT DISTINCT(numDomainId) FROM dbo.BizDocTemplate
------WHERE txtBizDocTemplate LIKE '%#OrganizationContactPhone#%' )--AND numOppType = 1)=>#Customer/VendorOrganizationContactPhone#
------ 
------SELECT * FROM dbo.Domain WHERE numDomainId IN (SELECT DISTINCT(numDomainId) FROM dbo.BizDocTemplate
------WHERE txtBizDocTemplate LIKE '%#ChangeBillToAddress#%' )--AND numOppType = 1)=>#Customer/VendorChangeBillToHeader#
------ 
------SELECT * FROM dbo.Domain WHERE numDomainId IN (SELECT DISTINCT(numDomainId) FROM dbo.BizDocTemplate
------WHERE txtBizDocTemplate LIKE '%#ChangeShipToAddress#%' )--AND numOppType = 1) =>#Customer/VendorChangeShipToHeader#
------
--------Update the Following Fields from Table Doamin wise
------UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
------'#Customer/VendorBill-toAddress#','#Customer/VendorBillToAddress#')
------WHERE txtBizDocTemplate LIKE '%#Customer/VendorBill-toAddress#%'
------
------UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
------'#Customer/VendorShip-toAddress#','#Customer/VendorShipToAddress#')
------WHERE txtBizDocTemplate LIKE '%#Customer/VendorShip-toAddress#%' --AND numOppType = 1
------
------UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
------'#OrganizationContactPhone#','#Customer/VendorOrganizationContactPhone#')
------WHERE txtBizDocTemplate LIKE '%#OrganizationContactPhone#%' --AND numOppType = 1
------
------UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
------'#ChangeBillToAddress#','#Customer/VendorChangeBillToHeader#')
--------WHERE txtBizDocTemplate LIKE '%#ChangeBillToAddress#%'
--------
--------UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--------'#ChangeShipToAddress#','#Customer/VendorChangeShipToHeader#')
------WHERE txtBizDocTemplate LIKE '%#ChangeShipToAddress#%'


-------Table changes has been made to Demo server ,pending for production server


------ALTER TABLE dbo.WorkFlowMaster ADD
------	vcWFAction varchar(MAX) NULL
	
------	ALTER TABLE dbo.WorkFlowConditionList ADD
------	vcFilterData varchar(MAX) NULL
	
------	ALTER TABLE dbo.WorkFlowActionUpdateFields ADD
------	vcData varchar(500) NULL
	
------	ALTER TABLE dbo.WorkFlowMaster ADD
------	vcDateField varchar(MAX) NULL
	
------	ALTER TABLE dbo.WorkFlowMaster ADD
------	intDays int NULL
	
------	ALTER TABLE dbo.WorkFlowMaster ADD
------	intActionOn int NULL
	 
----	 --For adding Module to proj
----	 -- INSERT INTO dbo.TabMaster	
----        -- ( numTabName ,
----          -- Remarks ,
----          -- tintTabType ,
----          -- vcURL ,
----          -- bitFixed ,
----          -- numDomainID ,
----          -- vcImage
----        -- )
------ VALUES  ( 'Work Flow Automation' , -- numTabName - varchar(50)
----          -- 'Work Flow Automation' , -- Remarks - varchar(50)
----          -- 1 , -- tintTabType - int
----          -- 'WorkFlow/frmWFList.aspx' , -- vcURL - varchar(1000)
----          -- 1 , -- bitFixed - bit
----          -- 1 , -- numDomainID - numeric
----          -- ''  -- vcImage - varchar(200)
----        -- )
	
	
----	CREATE TABLE dbo.WorkFlowAlertList
----	(
----	numWFAlertID numeric(18, 0) NOT NULL IDENTITY (1, 1),
----	vcAlertMessage ntext NULL,
----	numDomainID numeric(18, 0) NULL
----	)  ON [PRIMARY]
----	 TEXTIMAGE_ON [PRIMARY]
----GO
----ALTER TABLE dbo.WorkFlowAlertList ADD CONSTRAINT
----	PK_Table_1 PRIMARY KEY CLUSTERED 
----	(
----	numWFAlertID
----	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

----GO

------select * from DynamicFormMaster WHERE numFormId = 89
------numWFID,intAlertStatus
----ALTER TABLE dbo.WorkFlowAlertList ADD
----	numWFID numeric(18,0) NULL

----	ALTER TABLE dbo.WorkFlowAlertList ADD
----	intAlertStatus int NULL

------yet ,this is pending for Demo server
------New fields master

------INSERT INTO dbo.DynamicFormMaster	
------        ( numFormId ,
------          vcFormName ,
------          cCustomFieldsAssociated ,
------          cAOIAssociated ,
------          bitDeleted ,
------          tintFlag ,
------          bitWorkFlow ,
------          vcLocationID ,
------          bitAllowGridColor
------        )
------VALUES  ( 94 , -- numFormId - numeric
------          'Sales/Purchase/Project Process' , -- vcFormName - nvarchar(50)
------          'Y' , -- cCustomFieldsAssociated - char(1)
------          'N' , -- cAOIAssociated - char(1)
------          0 , -- bitDeleted - bit
------          3 , -- tintFlag - tinyint
------          0 , -- bitWorkFlow - bit
------          '2,6' , -- vcLocationID - varchar(50)
------          0  -- bitAllowGridColor - bit
------        )


------INSERT INTO dbo.DynamicFormMaster	
------        ( numFormId ,
------          vcFormName ,
------          cCustomFieldsAssociated ,
------          cAOIAssociated ,
------          bitDeleted ,
------          tintFlag ,
------          bitWorkFlow ,
------          vcLocationID ,
------          bitAllowGridColor
------        )
------VALUES  ( 95 , -- numFormId - numeric
------          'Contract' , -- vcFormName - nvarchar(50)
------          'Y' , -- cCustomFieldsAssociated - char(1)
------          'N' , -- cAOIAssociated - char(1)
------          0 , -- bitDeleted - bit
------          3 , -- tintFlag - tinyint
------          0 , -- bitWorkFlow - bit
------          '2,6' , -- vcLocationID - varchar(50)
------          0  -- bitAllowGridColor - bit
------        )

------added one field-workflowAction list :for check attachment (bizdocs)
------ALTER TABLE dbo.WorkFlowActionList ADD
------	tintIsAttachment tinyint NULL

------ALTER TABLE dbo.WorkFlowActionList ADD
------	vcApproval varchar(100) NULL

------	ALTER TABLE dbo.WorkFlowActionList ADD
------	tintSendMail tinyint NULL
	
------	ALTER TABLE dbo.workFlowActionList ADD
------	numBizDocTypeID numeric(18,0) NULL

------	ALTER TABLE dbo.workFlowActionList ADD
------	numBizDocTemplateID numeric(18,0) NULL
	
	
	
	
----	Insert into ModuleMaster(vcModuleName,tintGrouptype) values('Sales/Purchase/Project Process',1)
	
	
------519
----INSERT INTO DycFieldMaster
----                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
----                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
----                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
----                         intColumnWidth, intFieldMaxLength)
----VALUES        (41,Null,'Start Date','dtStartDate','dtStartDate',Null,'StagePercentageDetails','V','R','DateField',Null,Null,0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null)


------520
----INSERT INTO DycFieldMaster
----                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
----                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
----                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
----                         intColumnWidth, intFieldMaxLength)
----VALUES        (41,Null,'End Date','dtEndDate','dtEndDate',Null,'StagePercentageDetails','V','R','DateField',Null,Null,0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null)

------521
----INSERT INTO DycFieldMaster
----                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
----                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
----                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
----                         intColumnWidth, intFieldMaxLength)
----VALUES        (41,Null,'Stage Progress','tinProgressPercentage','tinProgressPercentage',Null,'StagePercentageDetails','N','R','SelectBox',Null,'SP',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null)


------522
----INSERT INTO DycFieldMaster
----                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
----                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
----                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
----                         intColumnWidth, intFieldMaxLength)
----VALUES        (41,Null,'Assigned To','numAssignTo','numAssignTo',Null,'StagePercentageDetails','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null)


----INSERT INTO DycFieldMaster
----                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
----                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
----                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
----                         intColumnWidth, intFieldMaxLength)
----VALUES        (41,Null,'Total Progress','intTotalProgress','intTotalProgress',Null,'ProjectProgress','N','R','TextBox',Null,'TP',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null)



----INSERT INTO DycFormField_Mapping
----                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
----                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
----                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
----VALUES        (41,521,Null,94,1,0,'Stage Progress','SelectBox','tinProgressPercentage',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)

----INSERT INTO DycFormField_Mapping
----                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
----                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
----                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
----VALUES        (41,519,Null,94,1,0,'Start Date','DateField','dtStartDate',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)

----INSERT INTO DycFormField_Mapping
----                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
----                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
----                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
----VALUES        (41,520,Null,94,1,0,'End Date','DateField','dtEndDate',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)

----INSERT INTO DycFormField_Mapping
----                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
----                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
----                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
----VALUES        (41,522,Null,94,1,0,'Assigned To','SelectBox','numAssignTo',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


----INSERT INTO DycFormField_Mapping
----                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
----                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
----                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
----VALUES        (41,523,Null,94,1,0,'Total Progress','TextBox','intTotalProgress',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



------Add WFA Link
----select * from PageNavigationDTL where numPageNavID=196--Automation Rules

----Update PageNavigationDTL set vcNavURL='../WorkFlow/frmWFList.aspx' where numPageNavID=196


----ALTER DATABASE [Production.2014]
----SET CHANGE_TRACKING = ON 
----(CHANGE_RETENTION = 2 DAYS, AUTO_CLEANUP = ON)

----ALTER TABLE dbo.DivisionMaster
----ENABLE CHANGE_TRACKING 
----WITH (TRACK_COLUMNS_UPDATED = ON);

----ALTER TABLE dbo.StagePercentageDetails
----ENABLE CHANGE_TRACKING 
----WITH (TRACK_COLUMNS_UPDATED = ON);


----ALTER TABLE dbo.OpportunityBizDocs
----ENABLE CHANGE_TRACKING 
----WITH (TRACK_COLUMNS_UPDATED = ON);

----ALTER TABLE dbo.OpportunityMaster
----ENABLE CHANGE_TRACKING 
----WITH (TRACK_COLUMNS_UPDATED = ON);


------Stored Procedures

----USE [Prod17122013]
----GO
----/****** Object:  StoredProcedure [dbo].[USP_GetWFRecordData]    Script Date: 31-May-14 5:59:21 PM ******/
----SET ANSI_NULLS ON
----GO
----SET QUOTED_IDENTIFIER ON
----GO
----ALTER PROCEDURE [dbo].[USP_GetWFRecordData]     
----    @numDomainID numeric(18, 0),
----    @numRecordID numeric(18, 0),
----    @numFormID numeric(18, 0)
----as                 

----DECLARE @numContactId AS NUMERIC(18),@numRecOwner AS NUMERIC(18),@numAssignedTo AS NUMERIC(18),@numDivisionId AS NUMERIC(18), @txtSignature VARCHAR(8000),@vcDateFormat VARCHAR(30),@numInternalPM numeric(18,0),@numExternalPM numeric(18,0),@numNextAssignedTo numeric(18,0)

----IF @numFormID=70 --Opportunities & Orders
----BEGIN
----	SELECT @numContactId=ISNULL(numContactId,0),@numRecOwner=ISNULL(numRecOwner,0),
----		   @numAssignedTo=ISNULL(numAssignedTo,0),@numDivisionId=ISNULL(numDivisionId,0)
----	FROM dbo.OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numRecordID
----END
----IF @numFormID=49 --BizDocs
----BEGIN
----	SELECT @numContactId=ISNULL(om.numContactId,0),@numRecOwner=ISNULL(om.numRecOwner,0),
----		   @numAssignedTo=ISNULL(om.numAssignedTo,0),@numDivisionId=ISNULL(om.numDivisionId,0),@vcDateFormat=D.vcDateFormat
----	FROM  dbo.OpportunityBizDocs AS  obd 
----	INNER JOIN dbo.OpportunityMaster AS OM 
----	on obd.numOppId=om.numOppId 
----	INNER JOIN  dbo.Domain AS d ON d.numDomainId=om.numDomainId
----	WHERE om.numDomainId=@numDomainID 
----		AND obd.numOppBizDocsId=@numRecordID
----END
----IF @numFormID=94 --Sales/Purchase/Project
----BEGIN
----declare @numProjectID numeric(18,0)
----declare @numOppId numeric(18,0)
----select  @numProjectID=ISNULL(numProjectID,0),@numOppId=Isnull(numOppId,0) from StagePercentageDetails where numStageDetailsId=@numRecordID

------@numInternalPM numeric(18,0),@numExternalPM numeric(18,0),@numNextAssignedTo numeric(18,0)
----If(@numProjectID=0)--Order
----Begin
----	SELECT @numContactId=ISNULL(om.numContactId,0),@numRecOwner=ISNULL(om.numRecOwner,0),
----		   @numAssignedTo=ISNULL(obd.numAssignTo,0),@numDivisionId=ISNULL(om.numDivisionId,0),@vcDateFormat=D.vcDateFormat
----	FROM  dbo.StagePercentageDetails AS  obd 
----	INNER JOIN dbo.OpportunityMaster AS OM 
----	on obd.numOppId=om.numOppId 
----	INNER JOIN  dbo.Domain AS d ON d.numDomainId=om.numDomainId
----	WHERE om.numDomainId=@numDomainID 
----	AND obd.numStageDetailsId=@numRecordID
----	End
----	Else
----	Begin
	
----	SELECT @numNextAssignedTo=numAssignTo  from StagePercentageDetails where numStageDetailsId=@numRecordID+1

----	SELECT @numContactId=ISNULL(om.numIntPrjMgr,0),@numRecOwner=ISNULL(om.numRecOwner,0),
----		   @numAssignedTo=ISNULL(obd.numAssignTo,0),@numDivisionId=ISNULL(om.numDivisionId,0),@numInternalPM=ISNULL(om.numIntPrjMgr,0),@numExternalPM=ISNULL(om.numCustPrjMgr,0)
----	FROM  dbo.StagePercentageDetails AS  obd 
----	INNER JOIN dbo.ProjectsMaster AS OM 
----	on obd.numProjectID=om.numProId 	
----	WHERE om.numDomainId=@numDomainID 
----	AND obd.numStageDetailsId=@numRecordID

----	END
----END
----IF @numFormID=68 --Organization
----BEGIN
----	SELECT @numContactId=ISNULL(adc.numContactId,0),@numRecOwner=ISNULL(obd.numRecOwner,0),
----		   @numAssignedTo=ISNULL(obd.numAssignedTo,0),@numDivisionId=ISNULL(obd.numDivisionId,0),@vcDateFormat=d.vcDateFormat
----	FROM  dbo.DivisionMaster AS  obd 	
----	INNER join dbo.AdditionalContactsInformation as adc on adc.numDivisionId=obd.numDivisionID
----	INNER JOIN  dbo.Domain AS d ON d.numDomainId=obd.numDomainId
----	WHERE obd.numDomainId=@numDomainID 
----		 AND obd.numDivisionID=@numRecordID
----END

----DECLARE @vcEmailID AS VARCHAR(100),@ContactName AS VARCHAR(150),@bitSMTPServer AS bit,@vcSMTPServer AS VARCHAR(100),
----		@numSMTPPort AS NUMERIC,@bitSMTPAuth AS BIT,@vcSmtpPassword AS VARCHAR(100),@bitSMTPSSL BIT 

----IF @numRecOwner>0
----BEGIN
----	SELECT @vcEmailID=vcEmailID,@ContactName=isnull(vcFirstname,'')+' '+isnull(vcLastName,''),@bitSMTPServer=isnull(bitSMTPServer,0),
----		   @vcSMTPServer=case when bitSMTPServer= 1 then vcSMTPServer else '' end,
----		   @numSMTPPort=case when bitSMTPServer= 1 then  isnull(numSMTPPort,0)  else 0 end,
----		   @bitSMTPAuth=isnull(bitSMTPAuth,0),@vcSmtpPassword=isnull([vcSmtpPassword],''),@bitSMTPSSL=isnull(bitSMTPSSL,0),@txtSignature=u.txtSignature
----		  FROM UserMaster U JOIN AdditionalContactsInformation A ON A.numContactID=U.numUserDetailId 
----		  WHERE U.numDomainID=@numDomainID AND numUserDetailId=@numRecOwner
----END

----SELECT @numContactId AS numContactId,@numRecOwner AS numRecOwner,@numAssignedTo AS numAssignedTo,@numDivisionId AS numDivisionId,
----	 @vcEmailID AS vcEmailID,@ContactName AS ContactName,@bitSMTPServer AS bitSMTPServer,@vcSMTPServer AS vcSMTPServer,@numSMTPPort AS numSMTPPort,
----	 @bitSMTPAuth AS bitSMTPAuth,@vcSmtpPassword AS vcSmtpPassword,@bitSMTPSSL AS bitSMTPSSL,@txtSignature AS Signature,@numInternalPM as numInternalPM,@numExternalPM as numExternalPM,@numNextAssignedTo as numNextAssignedTo
----GO
----/****** Object:  StoredProcedure [dbo].[USP_GetWorkflowAutomationRules]    Script Date: 31-May-14 5:59:21 PM ******/
----SET ANSI_NULLS ON
----GO
----SET QUOTED_IDENTIFIER ON
----GO


----GO
----/****** Object:  StoredProcedure [dbo].[USP_GetWorkFlowFormFieldMaster]    Script Date: 31-May-14 5:59:21 PM ******/
----SET ANSI_NULLS ON
----GO
----SET QUOTED_IDENTIFIER ON
----GO
----CREATE PROCEDURE [dbo].[USP_GetWorkFlowFormFieldMaster]     
----    @numDomainID numeric(18, 0),
----	@numFormID numeric(18, 0)
----as                 

----CREATE TABLE #tempField(numFieldID NUMERIC,
----vcFieldName NVARCHAR(50),vcDbColumnName NVARCHAR(50),vcOrigDbColumnName NVARCHAR(50),vcFieldDataType CHAR(1),
----vcAssociatedControlType NVARCHAR(50),vcListItemType VARCHAR(3),numListID NUMERIC,bitCustom BIT,vcLookBackTableName NVARCHAR(50),bitAllowFiltering BIT,bitAllowEdit BIT)

------Regular Fields
----INSERT INTO #tempField
----	SELECT numFieldID,vcFieldName,
----		   vcDbColumnName,vcOrigDbColumnName,vcFieldDataType,vcAssociatedControlType,
----		   vcListItemType,numListID,CAST(0 AS BIT) AS bitCustom,vcLookBackTableName,bitAllowFiltering,bitAllowEdit
----		FROM View_DynamicDefaultColumns
----		where numFormId=@numFormId and numDomainID=@numDomainID 
----		AND ISNULL(bitWorkFlowField,0)=1 AND ISNULL(bitCustom,0)=0


----DECLARE @vcLocationID VARCHAR(100);SET @vcLocationID='0'
----Select @vcLocationID =isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=@numFormId

------Custom Fields			
----INSERT INTO #tempField
----SELECT ISNULL(CFM.Fld_Id,0) as numFieldID,CFM.Fld_label as vcFieldName,
----	   'Cust'+Convert(varchar(10),Fld_Id) as vcDbColumnName,'Cust'+Convert(varchar(10),Fld_Id) AS vcOrigDbColumnName,CASE CFM.Fld_type WHEN 'SelectBox' THEN 'V' WHEN 'DateField' THEN 'D' ELSE 'V' END AS vcFieldDataType,
----	   CFM.Fld_type AS vcAssociatedControlType,CASE WHEN CFM.Fld_type='SelectBox' THEN 'LI' ELSE '' END AS vcListItemType,isnull(CFM.numListID,0) as numListID,
----	   CAST(1 AS BIT) AS bitCustom,L.vcCustomLookBackTableName AS vcLookBackTableName,0 AS bitAllowFiltering,0 AS bitAllowEdit
----FROM CFW_Fld_Master CFM LEFT JOIN CFW_Validation V ON V.numFieldID = CFM.Fld_id
----					JOIN CFW_Loc_Master L on CFM.GRP_ID=L.Loc_Id
----            WHERE   CFM.numDomainID = @numDomainId
----					AND GRP_ID IN(	SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
 

----SELECT *,/*vcOrigDbColumnName + '~' +*/ CAST(numFieldID AS VARCHAR(18)) + '_' + CAST(CASE WHEN bitCustom=1 THEN 'True' ELSE 'False' END AS VARCHAR(10)) AS ID  FROM #tempField
----ORDER BY bitCustom,vcFieldName

----DROP TABLE #tempField
		

----GO
----/****** Object:  StoredProcedure [dbo].[USP_GetWorkFlowMasterDetail]    Script Date: 31-May-14 5:59:21 PM ******/
----SET ANSI_NULLS ON
----GO
----SET QUOTED_IDENTIFIER ON
----GO
----Create PROCEDURE [dbo].[USP_GetWorkFlowMasterDetail]     
----    @numDomainID numeric(18, 0),
----	@numWFID numeric(18, 0)
----as                 

----SELECT * FROM WorkFlowMaster WF WHERE WF.numDomainID=@numDomainID AND WF.numWFID=@numWFID
------please

----CREATE TABLE #tempField(numFieldID NUMERIC,  
----vcFieldName NVARCHAR(50),vcDbColumnName NVARCHAR(50),vcOrigDbColumnName NVARCHAR(50),vcFieldDataType CHAR(1),  
----vcAssociatedControlType NVARCHAR(50),vcListItemType VARCHAR(3),numListID NUMERIC,bitCustom BIT,vcLookBackTableName NVARCHAR(50),bitAllowFiltering BIT,bitAllowEdit BIT)  
----DECLARE @numFormId AS NUMERIC(18,0)
----SELECT @numFormId=numFormID from dbo.WorkFlowMaster WHERE numDomainID=@numDomainID
------Regular Fields  
----INSERT INTO #tempField  
---- SELECT numFieldID,vcFieldName,  
----     vcDbColumnName,vcOrigDbColumnName,vcFieldDataType,vcAssociatedControlType,  
----     vcListItemType,numListID,CAST(0 AS BIT) AS bitCustom,vcLookBackTableName,bitAllowFiltering,bitAllowEdit  
----  FROM View_DynamicDefaultColumns  
----  where numFormId=@numFormId and numDomainID=@numDomainID   
----  AND ISNULL(bitWorkFlowField,0)=1 AND ISNULL(bitCustom,0)=0  
  
  
----DECLARE @vcLocationID VARCHAR(100);SET @vcLocationID='0'  
----Select @vcLocationID =isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=@numFormId  
  
------Custom Fields     
----INSERT INTO #tempField  
----SELECT ISNULL(CFM.Fld_Id,0) as numFieldID,CFM.Fld_label as vcFieldName,  
----    'Cust'+Convert(varchar(10),Fld_Id) as vcDbColumnName,'Cust'+Convert(varchar(10),Fld_Id) AS vcOrigDbColumnName,CASE CFM.Fld_type WHEN 'SelectBox' THEN 'V' WHEN 'DateField' THEN 'D' ELSE 'V' END AS vcFieldDataType,  
----    CFM.Fld_type AS vcAssociatedControlType,CASE WHEN CFM.Fld_type='SelectBox' THEN 'LI' ELSE '' END AS vcListItemType,isnull(CFM.numListID,0) as numListID,  
----    CAST(1 AS BIT) AS bitCustom,L.vcCustomLookBackTableName AS vcLookBackTableName,0 AS bitAllowFiltering,0 AS bitAllowEdit  
----FROM CFW_Fld_Master CFM LEFT JOIN CFW_Validation V ON V.numFieldID = CFM.Fld_id  
----     JOIN CFW_Loc_Master L on CFM.GRP_ID=L.Loc_Id  
----            WHERE   CFM.numDomainID = @numDomainId  
----     AND GRP_ID IN( SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))  



------Fire Trigger Events on Fields
----SELECT numFieldID,bitCustom,numWFID,numWFTriggerFieldID,CONVERT(NVARCHAR(80),CONVERT(NVARCHAR(20),numFieldId) +'_' + CASE WHEN bitCustom=0 THEN 'False' ELSE 'True' End) AS FieldID,(SELECT #tempField.vcFieldName FROM #tempField WHERE #tempField.numFieldID=WorkFlowTriggerFieldList.numFieldID)AS FieldName FROM WorkFlowTriggerFieldList WHERE numWFID=@numWFID

----SELECT * ,CONVERT(NVARCHAR(80),CONVERT(NVARCHAR(20),numFieldId) +'_' + CASE WHEN bitCustom=0 THEN 'False' ELSE 'True' End) AS FieldID,(SELECT #tempField.vcFieldName FROM #tempField WHERE #tempField.numFieldID=WorkFlowConditionList.numFieldID)AS FieldName ,CASE WHEN  vcFilterOperator='eq' THEN 'equals' WHEN vcFilterOperator='ne' THEN 'not equal to' WHEN vcFilterOperator='gt' THEN 'greater than' WHEN vcFilterOperator='ge' THEN 'greater or equal' WHEN vcFilterOperator='lt' THEN 'less than' WHEN vcFilterOperator='le' THEN 'less or equal' ELSE 'NA' END AS FilterOperator  FROM WorkFlowConditionList WHERE numWFID=@numWFID

----SELECT *,CASE WHEN tintActionType=1 THEN 
----				   (SELECT  vcDocName             
----					FROM    GenericDocuments  
----					WHERE   numDocCategory = 369  
----					AND tintDocumentType =1 --1 =generic,2=specific  
----					AND ISNULL(vcDocumentSection,'') <> 'M'  
----					AND numDomainId = @numDomainID  AND ISNULL(VcFileName,'') not LIKE '#SYS#%' AND numGenericDocID=numTemplateID)                
----             WHEN	tintActionType=2 then (Select TemplateName From tblActionItemData  where numdomainId=@numDomainID AND RowID=numTemplateID) 
----              ELSE '0'  END EmailTemplate ,
----         CASE WHEN vcEmailToType='1' THEN 'Owner of trigger record' WHEN vcEmailToType='2,' THEN 'Assignee of trigger record'  WHEN vcEmailToType='1,2' THEN 'Owner of trigger record,Assignee of trigger record'  WHEN   vcEmailToType='3' THEN 'Primary contact of trigger record' WHEN   vcEmailToType='1,3,' THEN 'Owner of trigger record,Primary contact of trigger record' WHEN vcEmailToType='2,3' THEN 'Primary contact of trigger record,Assignee of trigger record'  WHEN vcEmailToType='1,2,3' THEN  'Owner of trigger record,Primary contact of trigger record,Assignee of trigger record' ELSE NULL END AS EmailToType,
----         CASE WHEN tintTicklerActionAssignedTo=1 THEN 'Owner of trigger record' ELSE 'Assignee of trigger record' END AS TicklerAssignedTo,
----		 (select vcTemplateName from BizDocTemplate where numBizDocTempID=numBizDocTemplateID ) as  vcBizDocTemplate, 
----		 (SELECT isnull(vcRenamedListName,vcData) as vcData FROM listdetails Ld        
----left join listorder LO 
----on Ld.numListItemID= LO.numListItemID and Lo.numDomainId = @numDomainID 
----WHERE Ld.numListID=27 and (constFlag=1 or Ld.numDomainID=@numDomainID )  
----            and  Ld.numListItemID=numBizDocTypeID) as vcBizDoc ,
----		 (select Case When numOpptype=2 then 'Purchase' else 'Sales' end As BizDocType from BizDocTemplate where numBizDocTempID=numBizDocTemplateID) as vcBizDocType              
----FROM dbo.WorkFlowActionList WHERE numWFID=@numWFID

 
----SELECT *,CONVERT(NVARCHAR(80),CONVERT(NVARCHAR(20),numFieldId) +'_' + CASE WHEN bitCustom=0 THEN 'False' ELSE 'True' End) AS FieldID,(SELECT #tempField.vcFieldName FROM #tempField WHERE #tempField.numFieldID=WorkFlowActionUpdateFields.numFieldID)AS FieldName  FROM WorkFlowActionUpdateFields WHERE numWFActionID IN(SELECT numWFActionID FROM WorkFlowActionList WHERE numWFID=@numWFID)
----DROP TABLE #tempField
----GO
----/****** Object:  StoredProcedure [dbo].[USP_GetWorkFlowMasterList]    Script Date: 31-May-14 5:59:21 PM ******/
----SET ANSI_NULLS ON
----GO
----SET QUOTED_IDENTIFIER ON
----GO
----CREATE PROCEDURE [dbo].[USP_GetWorkFlowMasterList]      
----@numDomainId as numeric(9),  
----@numFormID NUMERIC(18,0), 
----@CurrentPage int,                                                        
----@PageSize int,                                                        
----@TotRecs int output,     
----@SortChar char(1)='0' ,                                                       
----@columnName as Varchar(50),                                                        
----@columnSortOrder as Varchar(50)  ,
----@SearchStr  as Varchar(50)   
    
----as       
----    SET NOCOUNT ON
     
----Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                         
----      numWFID NUMERIC(18,0)                                                       
---- )     
----declare @strSql as varchar(8000)                                                  
    
----set  @strSql='Select numWFID from WorkFlowMaster where numdomainid='+ convert(varchar(15),@numDomainID) 
    
----if @SortChar<>'0' set @strSql=@strSql + ' And vcWFName like '''+@SortChar+'%'''     

----if @numFormID <> 0 set @strSql=@strSql + ' And numFormID = '+ convert(varchar(15),@numFormID) +''   

----if @SearchStr<>'' set @strSql=@strSql + ' And (vcWFName like ''%'+@SearchStr+'%'' or 
----vcWFDescription like ''%'+@SearchStr+'%'') ' 
    
----set  @strSql=@strSql +'ORDER BY ' + @columnName +' '+ @columnSortOrder    
    
----    PRINT @strSql
----insert into #tempTable(numWFID) exec(@strSql)    
    
---- declare @firstRec as integer                                                        
---- declare @lastRec as integer                                                        
---- set @firstRec= (@CurrentPage-1) * @PageSize                                                        
---- set @lastRec= (@CurrentPage*@PageSize+1)                                                         

---- SELECT  @TotRecs = COUNT(*)  FROM #tempTable	
	
----Select WM.numWFID,WM.numDomainID,WM.vcWFName,WM.vcWFDescription,dbo.fn_StripHTML(WM.vcWFAction) AS vcWFAction ,CASE WHEN wm.bitActive=1 THEN 'Active' ELSE 'InActive' END AS Status,WM.numCreatedBy,
----		WM.vcDateField,WM.intDays,WM.intActionOn,WM.numModifiedBy,WM.dtCreatedDate,WM.dtModifiedDate,WM.bitActive,WM.numFormID,WM.tintWFTriggerOn,DFM.vcFormName,CASE WHEN (WM.tintWFTriggerOn=1 AND wm.intDays=0)THEN 'Create' WHEN WM.tintWFTriggerOn=2 THEN 'Edit' WHEN WM.tintWFTriggerOn=3 THEN 'Create or Edit' WHEN WM.tintWFTriggerOn=4 THEN 'Fields Update' WHEN WM.tintWFTriggerOn=5 THEN 'Delete' WHEN wm.intDays>0 THEN 'Date Field' ELSE 'NA' end AS TriggeredOn
----from WorkFlowMaster WM join #tempTable T on T.numWFID=WM.numWFID
----JOIN DynamicFormMaster DFM ON WM.numFormID=DFM.numFormID AND DFM.tintFlag=3
----   WHERE ID > @firstRec and ID < @lastRec order by ID
   
----   DROP TABLE #tempTable

----GO
----/****** Object:  StoredProcedure [dbo].[USP_GetWorkFlowQueue]    Script Date: 31-May-14 5:59:21 PM ******/
----SET ANSI_NULLS ON
----GO
----SET QUOTED_IDENTIFIER ON
----GO
----CREATE PROCEDURE [dbo].[USP_GetWorkFlowQueue]
----AS 
----BEGIN
----			  --IF Workflow is not active or deleted
----			  UPDATE WFQ SET tintProcessStatus=5 FROM WorkFlowQueue WFQ 
----				WHERE WFQ.numWFID NOT IN (SELECT numWFID FROM dbo.WorkFlowMaster WF WHERE WF.bitActive=1 AND WF.numFormID=WFQ.numFormID 
----				AND WFQ.numDomainID=WF.numDomainID AND WF.tintWFTriggerOn=WFQ.tintWFTriggerOn) 
			  
----			  --Select top 25 WorkFlow which are Pending Execution			  	
----			  SELECT TOP 25 WFQ.[numWFQueueID],WFQ.[numDomainID],WFQ.[numRecordID],WFQ.[numFormID],
----						WFQ.[tintProcessStatus],WFQ.[tintWFTriggerOn],WFQ.[numWFID]
----				 FROM WorkFlowQueue WFQ JOIN dbo.WorkFlowMaster WF ON WFQ.numWFID=WF.numWFID and WFQ.numFormID=WF.numFormID
----					  AND WFQ.numDomainID=WF.numDomainID AND WF.tintWFTriggerOn=WFQ.tintWFTriggerOn
----				 WHERE WFQ.tintProcessStatus IN (1) AND WF.bitActive=1 ORDER BY numWFQueueID
----END




----GO
----/****** Object:  StoredProcedure [dbo].[USP_ManageWorkFlowMaster]    Script Date: 31-May-14 5:59:21 PM ******/
----SET ANSI_NULLS ON
----GO
----SET QUOTED_IDENTIFIER ON
----GO
----Create PROCEDURE [dbo].[USP_ManageWorkFlowMaster]                                        
----	@numWFID numeric(18, 0) OUTPUT,
----    @vcWFName varchar(200),
----    @vcWFDescription varchar(500),   
----    @numDomainID numeric(18, 0),
----    @numUserCntID numeric(18, 0),
----    @bitActive bit,
----    @numFormID NUMERIC(18,0),
----    @tintWFTriggerOn NUMERIC(18),
----    @vcWFAction varchar(Max),
----    @vcDateField VARCHAR(MAX),
----    @intDays INT,
----    @intActionOn INT,
----    @strText TEXT = ''
----as                 

----IF @numWFID=0
----BEGIN
	
----	INSERT INTO [dbo].[WorkFlowMaster] ([numDomainID], [vcWFName], [vcWFDescription], [numCreatedBy], [numModifiedBy], [dtCreatedDate], [dtModifiedDate], [bitActive], [numFormID], [tintWFTriggerOn],[vcWFAction],[vcDateField],[intDays],[intActionOn])
----	SELECT @numDomainID, @vcWFName, @vcWFDescription, @numUserCntID, @numUserCntID, GETUTCDATE(), GETUTCDATE(), @bitActive, @numFormID, @tintWFTriggerOn,@vcWFAction,@vcDateField,@intDays,@intActionOn

----	SET @numWFID=@@IDENTITY
----	--start of my
----	IF DATALENGTH(@strText)>2
----	BEGIN
----		DECLARE @hDocItems INT                                                                                                                                                                
	
----		EXEC sp_xml_preparedocument @hDocItems OUTPUT, @strText                                                                                                             
        
----        --Delete records into WorkFlowTriggerFieldList and Insert
----        DELETE FROM WorkFlowTriggerFieldList WHERE  [numWFID] = @numWFID

----		INSERT INTO dbo.WorkFlowTriggerFieldList (numWFID,numFieldID,bitCustom) 
----			SELECT @numWFID,numFieldID,bitCustom
----			  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowTriggerFieldList',2) WITH ( numFieldID NUMERIC, bitCustom BIT )
	
	
----		--Delete records into WorkFlowConditionList and Insert
----		DELETE FROM WorkFlowConditionList WHERE  [numWFID] = @numWFID

----		INSERT INTO dbo.WorkFlowConditionList (numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData) 
----			SELECT @numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData
----			  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowConditionList',2) WITH ( numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10),vcFilterANDOR VARCHAR(10),vcFilterData VARCHAR(MAX))


----		--Delete records into WorkFlowActionUpdateFields and Insert
----		DELETE FROM WorkFlowActionUpdateFields WHERE numWFActionID IN(SELECT numWFActionID FROM WorkFlowActionList WHERE numWFID=@numWFID)

----		--Delete records into WorkFlowActionList and Insert
----		DELETE FROM WorkFlowActionList WHERE  [numWFID] = @numWFID

----		INSERT INTO dbo.WorkFlowActionList (numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval,tintSendMail,numBizDocTypeID,numBizDocTemplateID) 
----			SELECT @numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval ,tintSendMail,numBizDocTypeID,numBizDocTemplateID 
----			  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowActionList',2) WITH ( tintActionType TINYINT, numTemplateID NUMERIC,vcEmailToType VARCHAR(50),tintTicklerActionAssignedTo TINYINT,vcAlertMessage NTEXT,tintActionOrder TINYINT,tintIsAttachment TINYINT,vcApproval VARCHAR(100),tintSendMail TINYINT,numBizDocTypeID numeric(18,0),numBizDocTemplateID numeric(18,0))

----		INSERT INTO dbo.WorkFlowActionUpdateFields (numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData) 
----			SELECT WFA.numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData
----			  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowActionUpdateFields',2) WITH ( numWFActionID NUMERIC, tintModuleType NUMERIC,numFieldID NUMERIC,bitCustom BIT,vcValue VARCHAR(500),vcData VARCHAR(500) ) WFAUF
----			  JOIN WorkFlowActionList WFA ON WFA.tintActionOrder=WFAUF.numWFActionID WHERE WFA.numWFID=@numWFID

----		EXEC sp_xml_removedocument @hDocItems
----	END	
----	--end of my
----END
----ELSE
----BEGIN
	
----	UPDATE [dbo].[WorkFlowMaster]
----	SET    [vcWFName] = @vcWFName, [vcWFDescription] = @vcWFDescription, [numModifiedBy] = @numUserCntID, [dtModifiedDate] = GETUTCDATE(), [bitActive] = @bitActive, [numFormID] = @numFormID, [tintWFTriggerOn] = @tintWFTriggerOn,[vcWFAction]=@vcWFAction,[vcDateField]=@vcDateField,[intDays]=@intDays,[intActionOn]=@intActionOn
----	WHERE  [numDomainID] = @numDomainID AND [numWFID] = @numWFID
	
----	IF DATALENGTH(@strText)>2
----	BEGIN
----		DECLARE @hDocItem INT                                                                                                                                                                
	
----		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strText                                                                                                             
        
----        --Delete records into WorkFlowTriggerFieldList and Insert
----        DELETE FROM WorkFlowTriggerFieldList WHERE  [numWFID] = @numWFID

----		INSERT INTO dbo.WorkFlowTriggerFieldList (numWFID,numFieldID,bitCustom) 
----			SELECT @numWFID,numFieldID,bitCustom
----			  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowTriggerFieldList',2) WITH ( numFieldID NUMERIC, bitCustom BIT )
	
	
----		--Delete records into WorkFlowConditionList and Insert
----		DELETE FROM WorkFlowConditionList WHERE  [numWFID] = @numWFID

----		INSERT INTO dbo.WorkFlowConditionList (numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData) 
----			SELECT @numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData
----			  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowConditionList',2) WITH ( numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10),vcFilterANDOR VARCHAR(10),vcFilterData varchar(MAX))


----		--Delete records into WorkFlowActionUpdateFields and Insert
----		DELETE FROM WorkFlowActionUpdateFields WHERE numWFActionID IN(SELECT numWFActionID FROM WorkFlowActionList WHERE numWFID=@numWFID)

----		--Delete records into WorkFlowActionList and Insert
----		DELETE FROM WorkFlowActionList WHERE  [numWFID] = @numWFID

----		INSERT INTO dbo.WorkFlowActionList (numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval ,tintSendMail,numBizDocTypeID ,numBizDocTemplateID  ) 
----			SELECT @numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval ,tintSendMail,numBizDocTypeID ,numBizDocTemplateID 
----			  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowActionList',2) WITH ( tintActionType TINYINT, numTemplateID NUMERIC,vcEmailToType VARCHAR(50),tintTicklerActionAssignedTo TINYINT,vcAlertMessage NTEXT,tintActionOrder TINYINT,tintIsAttachment TINYINT,vcApproval VARCHAR(100),tintSendMail TINYINT,numBizDocTypeID numeric(18,0),numBizDocTemplateID numeric(18,0) )

----		INSERT INTO dbo.WorkFlowActionUpdateFields (numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData) 
----			SELECT WFA.numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData
----			  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowActionUpdateFields',2) WITH ( numWFActionID NUMERIC, tintModuleType NUMERIC,numFieldID NUMERIC,bitCustom BIT,vcValue VARCHAR(500),vcData VARCHAR(500)) WFAUF
----			  JOIN WorkFlowActionList WFA ON WFA.tintActionOrder=WFAUF.numWFActionID WHERE WFA.numWFID=@numWFID

----		EXEC sp_xml_removedocument @hDocItem
----	END	
----END

	


	

	


	

----GO
----/****** Object:  StoredProcedure [dbo].[USP_ManageWorkFlowQueue]    Script Date: 31-May-14 5:59:21 PM ******/
----SET ANSI_NULLS ON
----GO
----SET QUOTED_IDENTIFIER ON
----GO
----CREATE PROCEDURE [dbo].[USP_ManageWorkFlowQueue]
----    @numWFQueueID numeric(18, 0)=0,
----    @numDomainID numeric(18, 0)=0,
----    @numUserCntID numeric(18, 0)=0,
----    @numRecordID numeric(18, 0)=0,
----    @numFormID numeric(18, 0)=0,
----    @tintProcessStatus TINYINT=1,
----    @tintWFTriggerOn TINYINT,
----    @tintMode TINYINT,
----    @vcColumnsUpdated VARCHAR(1000)='',
----    @numWFID NUMERIC(18,0)=0,
----    @bitSuccess BIT=0,
----    @vcDescription VARCHAR(1000)=''
    
----AS 
----BEGIN
----	IF @tintMode=1
----	BEGIN
----		IF @numWFQueueID>0
----		   BEGIN
----	  		 UPDATE [dbo].[WorkFlowQueue] SET [tintProcessStatus] = @tintProcessStatus WHERE  [numWFQueueID] = @numWFQueueID AND [numDomainID] = @numDomainID
----		   END
----		ELSE
----		   BEGIN
----	 		 IF EXISTS(SELECT 1 FROM dbo.WorkFlowMaster WHERE numDomainID=@numDomainID AND numFormID=@numFormID AND (tintWFTriggerOn=@tintWFTriggerOn OR (@tintWFTriggerOn IN (1,2) AND tintWFTriggerOn=3)) AND bitActive=1)
----			 BEGIN
			 
----			          --PRINT 'schin'
----					INSERT INTO [dbo].[WorkFlowQueue] ([numDomainID], [numCreatedBy], [dtCreatedDate], [numRecordID], [numFormID], [tintProcessStatus], [tintWFTriggerOn], [numWFID])
----					SELECT @numDomainID, @numUserCntID, GETUTCDATE(), @numRecordID, @numFormID, @tintProcessStatus, tintWFTriggerOn, numWFID
----					FROM dbo.WorkFlowMaster WHERE numDomainID=@numDomainID AND numFormID=@numFormID AND (tintWFTriggerOn=@tintWFTriggerOn OR (@tintWFTriggerOn IN (1,2) AND tintWFTriggerOn=3)) AND bitActive=1
----					AND numWFID NOT IN (SELECT numWFID FROM WorkFlowQueue WHERE numDomainID=@numDomainID AND numRecordID=@numRecordID AND numFormID=@numFormID AND (tintWFTriggerOn=@tintWFTriggerOn OR (@tintWFTriggerOn IN (1,2) AND tintWFTriggerOn=3)) AND tintProcessStatus IN(1,2))
					
----					IF @tintWFTriggerOn=2 AND LEN(@vcColumnsUpdated)>0
----					BEGIN
----						SET @tintWFTriggerOn=4
						
----						INSERT INTO [dbo].[WorkFlowQueue] ([numDomainID], [numCreatedBy], [dtCreatedDate], [numRecordID], [numFormID], [tintProcessStatus], [tintWFTriggerOn], [numWFID])
----						SELECT DISTINCT @numDomainID, @numUserCntID, GETUTCDATE(), @numRecordID, @numFormID, @tintProcessStatus, @tintWFTriggerOn, WF.numWFID
----						FROM dbo.WorkFlowMaster WF JOIN WorkFlowTriggerFieldList WTFL ON WF.numWFID=WTFL.numWFID
----						JOIN dbo.DycFieldMaster DFM ON WTFL.numFieldID=DFM.numFieldId 
----						WHERE WF.numDomainID=@numDomainID AND WF.numFormID=@numFormID AND WF.tintWFTriggerOn=4 AND WF.bitActive=1 AND WTFL.bitCustom=0
----						AND DFM.vcOrigDbColumnName IN (SELECT Items FROM dbo.Split(@vcColumnsUpdated,','))					
----						AND WF.numWFID NOT IN (SELECT numWFID FROM WorkFlowQueue WHERE numDomainID=@numDomainID AND numRecordID=@numRecordID AND numFormID=@numFormID AND tintWFTriggerOn=4 AND tintProcessStatus IN(1,2))
----					END
----			 END
----			 --Added By:Sachin Sadhu||Date:10thFeb2014
----			 --Description:@tintWFTriggerOn value is  coming from Trigger,I=1,U=2,D=3
----			 --case of Field(s) Update
----			 IF EXISTS(SELECT 1 FROM dbo.WorkFlowMaster WHERE numDomainID=@numDomainID AND numFormID=@numFormID AND (tintWFTriggerOn=@tintWFTriggerOn OR (@tintWFTriggerOn IN (1,2) AND tintWFTriggerOn=4)) AND bitActive=1)
----			 BEGIN
----			 --PRINT 'schindas'
----					INSERT INTO [dbo].[WorkFlowQueue] ([numDomainID], [numCreatedBy], [dtCreatedDate], [numRecordID], [numFormID], [tintProcessStatus], [tintWFTriggerOn], [numWFID])
----					SELECT @numDomainID, @numUserCntID, GETUTCDATE(), @numRecordID, @numFormID, @tintProcessStatus, tintWFTriggerOn, numWFID
----					FROM dbo.WorkFlowMaster WHERE numDomainID=@numDomainID AND numFormID=@numFormID AND (tintWFTriggerOn=@tintWFTriggerOn OR (@tintWFTriggerOn IN (1,2) AND tintWFTriggerOn=3)) AND bitActive=1
----					AND numWFID NOT IN (SELECT numWFID FROM WorkFlowQueue WHERE numDomainID=@numDomainID AND numRecordID=@numRecordID AND numFormID=@numFormID AND (tintWFTriggerOn=@tintWFTriggerOn OR (@tintWFTriggerOn IN (1,2) AND tintWFTriggerOn=3)) AND tintProcessStatus IN(1,2))
					
----					IF @tintWFTriggerOn=2 AND LEN(@vcColumnsUpdated)>0
----					BEGIN
----						SET @tintWFTriggerOn=4
						
----						INSERT INTO [dbo].[WorkFlowQueue] ([numDomainID], [numCreatedBy], [dtCreatedDate], [numRecordID], [numFormID], [tintProcessStatus], [tintWFTriggerOn], [numWFID])
----						SELECT DISTINCT @numDomainID, @numUserCntID, GETUTCDATE(), @numRecordID, @numFormID, @tintProcessStatus, @tintWFTriggerOn, WF.numWFID
----						FROM dbo.WorkFlowMaster WF JOIN WorkFlowTriggerFieldList WTFL ON WF.numWFID=WTFL.numWFID
----						 JOIN dbo.DycFieldMaster DFM ON WTFL.numFieldID=DFM.numFieldId 
----						WHERE WF.numDomainID=@numDomainID AND WF.numFormID=@numFormID AND WF.tintWFTriggerOn=4 AND WF.bitActive=1 AND WTFL.bitCustom=0
----						AND DFM.vcOrigDbColumnName IN (SELECT items FROM dbo.Split(@vcColumnsUpdated,','))					
----						AND WF.numWFID NOT IN (SELECT numWFID FROM WorkFlowQueue WHERE numDomainID=@numDomainID AND numRecordID=@numRecordID AND numFormID=@numFormID AND tintWFTriggerOn=4 AND tintProcessStatus IN(1,2))
----					END
----			 END
----			 --End of Script:Sachin
----		  END
----	 END
----	  ELSE IF @tintMode = 2
----        BEGIN
----			INSERT INTO [dbo].[WorkFlowQueueExecution] ([numWFQueueID], [numWFID], [dtExecutionDate], [bitSuccess], [vcDescription])
----			SELECT @numWFQueueID, @numWFID, GETUTCDATE(), @bitSuccess, @vcDescription
----        END    
----END




----GO
----/****** Object:  StoredProcedure [dbo].[USP_OpportunityBizDocs_CT]    Script Date: 31-May-14 5:59:21 PM ******/
----SET ANSI_NULLS ON
----GO
----SET QUOTED_IDENTIFIER ON
----GO
------ =============================================
------ Author:		<Author,,Sachin Sadhu>
------ Create date: <Create Date,,23April2014>
------ Description:	<Description,,Change Tracking>
------ =============================================
----CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_CT]
----	-- Add the parameters for the stored procedure here
----    @numDomainID numeric(18, 0)=0,
----    @numUserCntID numeric(18, 0)=0,
----    @numRecordID numeric(18, 0)=0 
----AS
----BEGIN
------ SET NOCOUNT ON added to prevent extra result sets from
------ interfering with SELECT statements.
----SET NOCOUNT ON;
---- -- Insert statements for procedure here
---- declare @tblFields as table (

---- FieldName varchar(200),
---- FieldValue varchar(200)
----)

----Declare @Type char(1)
----Declare @ChangeVersion bigint
----Declare @CreationVersion bigint
----Declare @last_synchronization_version bigInt=null

----SELECT 			
----@ChangeVersion=Ct.Sys_Change_version,@CreationVersion=Ct.Sys_Change_Creation_version
----FROM 
----CHANGETABLE(CHANGES dbo.OpportunityBizDocs, 0) AS CT
----WHERE numOppBizDocsId=@numRecordID

----Begin

----SET @last_synchronization_version=@ChangeVersion-1     
	
----		--Fields Update & for Type :Insert or Update:
----			Declare @UFFields as table 
----			(
----			    vcBizDocID varchar(70),
----				monAmountPaid varchar(70),
----				numBizDocStatus varchar(70),
----				numBizDocId varchar(70),
----				monDealAmount varchar(70),
----				numModifiedBy varchar(70),
----				numCreatedBy varchar(70)
----			)

----	INSERT into @UFFields(vcBizDocID,monAmountPaid,numBizDocStatus,numBizDocId,monDealAmount,numModifiedBy,numCreatedBy)
----		SELECT
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityBizDocs'), 'vcBizDocID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcBizDocID ,
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityBizDocs'), 'monAmountPaid', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS monAmountPaid, 
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityBizDocs'), 'numBizDocStatus', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numBizDocStatus, 
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityBizDocs'), 'numBizDocId', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numBizDocId, 
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityBizDocs'), 'monDealAmount', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS monDealAmount	,
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityBizDocs'), 'numModifiedBy', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS  numModifiedBy ,
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityBizDocs'), 'numCreatedBy', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numCreatedBy 
----		FROM 
----		CHANGETABLE(CHANGES dbo.OpportunityBizDocs, @last_synchronization_version) AS CT
----		WHERE 
----		ct.numOppBizDocsId=@numRecordID

----		--sp_helptext USP_GetWorkFlowFormFieldMaster
----	--exec USP_GetWorkFlowFormFieldMaster 1,49
----INSERT INTO @tblFields(FieldName,FieldValue)
----		SELECT
----		FieldName,
----		FieldValue
----		FROM
----			(
----				SELECT  vcBizDocID,monAmountPaid,numBizDocStatus,numBizDocId,monDealAmount
----				FROM @UFFields	
----			) AS UP
----		UNPIVOT
----		(
----		FieldValue FOR FieldName IN (vcBizDocID,monAmountPaid,numBizDocStatus,numBizDocId,monDealAmount)
----		) AS upv
----		where FieldValue<>0
 

----End

----select 'col1' from @UFFields where numCreatedBy='0' and numModifiedBy='0'
---- If Exists (select 'col1' from @UFFields where numCreatedBy='0' and numModifiedBy='0')
---- Begin
---- set @Type='I'
---- End
---- Else
---- Begin
---- set @Type='U'
---- End
---- select @Type
----DECLARE @tintWFTriggerOn AS TINYINT
----SET @tintWFTriggerOn=CASE @Type WHEN 'I' THEN 1 WHEN 'U' THEN 2 WHEN 'D' THEN 5 END

------For Fields Update Exection Point
----DECLARE @Columns_Updated VARCHAR(1000)
----SELECT @Columns_Updated = COALESCE(@Columns_Updated + ',', '') + FieldName FROM @tblFields 
----SET @Columns_Updated=ISNULL(@Columns_Updated,'')

----SELECT @Columns_Updated

------Adding data As per Opertaion
----	EXEC dbo.USP_ManageWorkFlowQueue
----	@numWFQueueID = 0, --  numeric(18, 0)
----	@numDomainID = @numDomainID, --  numeric(18, 0)
----	@numUserCntID = @numUserCntID, --  numeric(18, 0)
----	@numRecordID = @numRecordID, --  numeric(18, 0)
----	@numFormID = 71, --  numeric(18, 0)
----	@tintProcessStatus = 1, --  tinyint
----	@tintWFTriggerOn = @tintWFTriggerOn, --  tinyint
----	@tintMode = 1, --  tinyint
----	@vcColumnsUpdated = @Columns_Updated

	
----/***************************************************Commented SP for Checking RowVersion with Change Tracking:Date:23Apr||Sachin********************************************************/



------DECLARE @synchronization_version BIGINT 



------SET @synchronization_version = CHANGE_TRACKING_CURRENT_VERSION();











------SELECT 



------    CT.numOppID, CT.SYS_CHANGE_OPERATION, 



------    CT.SYS_CHANGE_COLUMNS, CT.SYS_CHANGE_CONTEXT, 



------    CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numOppID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numOppID, 



------    CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'vcPOppName', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcPOppName 



------FROM 



------    CHANGETABLE(CHANGES dbo.OpportunityMaster, @synchronization_version) AS CT











------SET @synchronization_version = CHANGE_TRACKING_CURRENT_VERSION();



----/***************************************************End of Comment||Sachin********************************************************/



----END

----GO
----/****** Object:  StoredProcedure [dbo].[USP_OpportunityMaster_CT]    Script Date: 31-May-14 5:59:21 PM ******/
----SET ANSI_NULLS ON
----GO
----SET QUOTED_IDENTIFIER ON
----GO
------ =============================================
------ Author:		<Author,,Sachin Sadhu>
------ Create date: <Create Date,,23April2014>
------ Description:	<Description,,Change Tracking>
------ =============================================
----CREATE PROCEDURE [dbo].[USP_OpportunityMaster_CT]
----	-- Add the parameters for the stored procedure here
----    @numDomainID numeric(18, 0)=0,
----    @numUserCntID numeric(18, 0)=0,
----    @numRecordID numeric(18, 0)=0 
----AS
----BEGIN
------ SET NOCOUNT ON added to prevent extra result sets from
------ interfering with SELECT statements.
----SET NOCOUNT ON;
---- -- Insert statements for procedure here
---- declare @tblFields as table (

---- FieldName varchar(200),
---- FieldValue varchar(200)
----)

----Declare @Type char(1)
----Declare @ChangeVersion bigint
----Declare @CreationVersion bigint
----Declare @last_synchronization_version bigInt=null

----SELECT 			
----@ChangeVersion=Ct.Sys_Change_version,@CreationVersion=Ct.Sys_Change_Creation_version
----FROM 
----CHANGETABLE(CHANGES dbo.OpportunityMaster, 0) AS CT
----WHERE numOppId=@numRecordID

----IF (@ChangeVersion=@CreationVersion)--Insert
----Begin
----        SELECT 
----		@Type = CT.SYS_CHANGE_OPERATION
----		FROM 
----		CHANGETABLE(CHANGES dbo.OpportunityMaster, @last_synchronization_version) AS CT
----		WHERE  CT.numOppID=@numRecordID

----End
----Else--Update
----Begin

----SET @last_synchronization_version=@ChangeVersion-1
        
----		--Temporary commmented
----		--SELECT 
----		--CT.numOppID, CT.SYS_CHANGE_OPERATION, 
----		--CT.SYS_CHANGE_COLUMNS, CT.SYS_CHANGE_CONTEXT, 
----		--CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numOppID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numOppID, 
----		--CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'tintActive', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS tintActive, 
----		--CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'monPAmount', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS monPAmount, 
----		--CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numAssignedBy', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numAssignedBy, 
----		--CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numAssignedTo', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numAssignedTo,
----		--CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numCampainID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numCampainID, 
----		--CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'bintAccountClosingDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bintAccountClosingDate, 
----		--CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'txtComments', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS txtComments, 
----		--CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'lngPConclAnalysis', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS lngPConclAnalysis, 
----		--CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'bintCreatedDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bintCreatedDate, 
----		--CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'vcOppRefOrderNo', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcOppRefOrderNo, 
----		--CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'tintOppStatus', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS tintOppStatus, 
----		--CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'intpEstimatedCloseDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS intpEstimatedCloseDate, 
----		----new
----		----(OpportunityBizDocs)CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'monDealAmount', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS monDealAmount, 
----		--CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'vcMarketplaceOrderID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcMarketplaceOrderID, 
----		--CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'tintOppType', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS tintOppType, 
----		--CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numStatus', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numStatus,
----		--CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'monDealAmount', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS monDealAmount, 
----		--CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numRecOwner', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numRecOwner, 
----		--CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'vcPoppName', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcPoppName, 
----		--CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numSalesOrPurType', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numSalesOrPurType, 
----		--CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'tintSource', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS tintSource  
----		--FROM 
----		--CHANGETABLE(CHANGES dbo.OpportunityMaster, @last_synchronization_version) AS CT
----		--WHERE  CT.numOppID=@numRecordID
        

----        SELECT 
----		@Type = CT.SYS_CHANGE_OPERATION
----		FROM 
----		CHANGETABLE(CHANGES dbo.OpportunityMaster, @last_synchronization_version) AS CT
----		WHERE  CT.numOppID=@numRecordID

		
		
----		--Fields Update:
----			Declare @UFFields as table 
----			(
----					tintActive varchar(70),
----					monPAmount varchar(70),
----					numAssignedBy varchar(70),
----					numAssignedTo varchar(70),
----					numCampainID varchar(70),
----					bintAccountClosingDate varchar(70),
----					txtComments varchar(70),
----					lngPConclAnalysis varchar(70),
----					bintCreatedDate varchar(70),
----					vcOppRefOrderNo varchar(70),
----					tintOppStatus varchar(70),
----					intpEstimatedCloseDate varchar(70),
----					--monDealAmount varchar(70),
----					vcMarketplaceOrderID varchar(70),
----					tintOppType varchar(70),
----					numStatus varchar(70),
----					monDealAmount varchar(70),
----					numRecOwner varchar(70),
----					vcPoppName varchar(70),
----					numSalesOrPurType varchar(70),
----					tintSource varchar(70)
----					--monAmountPaid varchar(70),
----					--Cust921	 varchar(70)	
----			)

----	INSERT into @UFFields(tintActive,monPAmount,numAssignedBy,numAssignedTo,numCampainID,bintAccountClosingDate,txtComments,lngPConclAnalysis,bintCreatedDate,vcOppRefOrderNo,tintOppStatus,intpEstimatedCloseDate,vcMarketplaceOrderID,tintOppType,numStatus,monDealAmount,numRecOwner,vcPoppName,numSalesOrPurType,tintSource)
----		SELECT

----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'tintActive', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS tintActive, 
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'monPAmount', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS monPAmount, 
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numAssignedBy', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numAssignedBy, 
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numAssignedTo', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numAssignedTo,
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numCampainID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numCampainID, 
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'bintAccountClosingDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bintAccountClosingDate, 
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'txtComments', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS txtComments, 
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'lngPConclAnalysis', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS lngPConclAnalysis, 
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'bintCreatedDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bintCreatedDate, 
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'vcOppRefOrderNo', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcOppRefOrderNo, 
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'tintOppStatus', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS tintOppStatus, 
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'intpEstimatedCloseDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS intpEstimatedCloseDate, 
----		--new
----		--(OpportunityBizDocs)CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'monDealAmount', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS monDealAmount, 
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'vcMarketplaceOrderID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcMarketplaceOrderID, 
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'tintOppType', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS tintOppType, 
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numStatus', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numStatus,
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'monDealAmount', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS monDealAmount, 
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numRecOwner', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numRecOwner, 
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'vcPoppName', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcPoppName, 
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numSalesOrPurType', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numSalesOrPurType, 
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'tintSource', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS tintSource 
----		--(OpportunityBizDocs)CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'monAmountPaid', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS monAmountPaid
----		--(CFW_Fld_Values_Opp)CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'Cust921', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS Cust921, 
		

		
----		FROM 
----		CHANGETABLE(CHANGES dbo.OpportunityMaster, @last_synchronization_version) AS CT
----		WHERE 
----		ct.numOppId=@numRecordID

----		--exec USP_GetWorkFlowFormFieldMaster 1,70
----INSERT INTO @tblFields(FieldName,FieldValue)
----		SELECT
----		FieldName,
----		FieldValue
----		FROM
----			(
----				SELECT tintActive,monPAmount,numAssignedBy,numAssignedTo,numCampainID,bintAccountClosingDate,txtComments,lngPConclAnalysis,bintCreatedDate,vcOppRefOrderNo,tintOppStatus,intpEstimatedCloseDate,vcMarketplaceOrderID,tintOppType,numStatus,monDealAmount,numRecOwner,vcPoppName,numSalesOrPurType,tintSource
----				FROM @UFFields	
----			) AS UP
----		UNPIVOT
----		(
----		FieldValue FOR FieldName IN (tintActive,monPAmount,numAssignedBy,numAssignedTo,numCampainID,bintAccountClosingDate,txtComments,lngPConclAnalysis,bintCreatedDate,vcOppRefOrderNo,tintOppStatus,intpEstimatedCloseDate,vcMarketplaceOrderID,tintOppType,numStatus,monDealAmount,numRecOwner,vcPoppName,numSalesOrPurType,tintSource)
----		) AS upv
----		where FieldValue<>0
 

----End

----DECLARE @tintWFTriggerOn AS TINYINT
----SET @tintWFTriggerOn=CASE @Type WHEN 'I' THEN 1 WHEN 'U' THEN 2 WHEN 'D' THEN 5 END

------For Fields Update Exection Point
----DECLARE @Columns_Updated VARCHAR(1000)
----SELECT @Columns_Updated = COALESCE(@Columns_Updated + ',', '') + FieldName FROM @tblFields 
----SET @Columns_Updated=ISNULL(@Columns_Updated,'')
----IF(	@Columns_Updated='monDealAmount')
----BEGIN

----set @tintWFTriggerOn=1
----END

----select  @Columns_Updated
----select @tintWFTriggerOn

------Adding data As per Opertaion
----	EXEC dbo.USP_ManageWorkFlowQueue
----	@numWFQueueID = 0, --  numeric(18, 0)
----	@numDomainID = @numDomainID, --  numeric(18, 0)
----	@numUserCntID = @numUserCntID, --  numeric(18, 0)
----	@numRecordID = @numRecordID, --  numeric(18, 0)
----	@numFormID = 70, --  numeric(18, 0)
----	@tintProcessStatus = 1, --  tinyint
----	@tintWFTriggerOn = @tintWFTriggerOn, --  tinyint
----	@tintMode = 1, --  tinyint
----	@vcColumnsUpdated = @Columns_Updated

	
----/***************************************************Commented SP for Checking RowVersion with Change Tracking:Date:23Apr||Sachin********************************************************/



------DECLARE @synchronization_version BIGINT 



------SET @synchronization_version = CHANGE_TRACKING_CURRENT_VERSION();











------SELECT 



------    CT.numOppID, CT.SYS_CHANGE_OPERATION, 



------    CT.SYS_CHANGE_COLUMNS, CT.SYS_CHANGE_CONTEXT, 



------    CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numOppID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numOppID, 



------    CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'vcPOppName', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcPOppName 



------FROM 



------    CHANGETABLE(CHANGES dbo.OpportunityMaster, @synchronization_version) AS CT











------SET @synchronization_version = CHANGE_TRACKING_CURRENT_VERSION();



----/***************************************************End of Comment||Sachin********************************************************/



----END

----GO
----/****** Object:  StoredProcedure [dbo].[USP_Organization_CT]    Script Date: 31-May-14 5:59:21 PM ******/
----SET ANSI_NULLS ON
----GO
----SET QUOTED_IDENTIFIER ON
----GO
------ =============================================
------ Author:		<Author,,Sachin Sadhu>
------ Create date: <Create Date,,23April2014>
------ Description:	<Description,,Change Tracking>
------ =============================================
----CREATE PROCEDURE [dbo].[USP_Organization_CT]
----	-- Add the parameters for the stored procedure here
----    @numDomainID numeric(18, 0)=0,
----    @numUserCntID numeric(18, 0)=0,
----    @numRecordID numeric(18, 0)=0 
----AS
----BEGIN
------ SET NOCOUNT ON added to prevent extra result sets from
------ interfering with SELECT statements.
----SET NOCOUNT ON;
---- -- Insert statements for procedure here
---- declare @tblFields as table (

---- FieldName varchar(200),
---- FieldValue varchar(200)
----)

----Declare @Type char(1)
----Declare @ChangeVersion bigint
----Declare @CreationVersion bigint
----Declare @last_synchronization_version bigInt=null

----SELECT 			
----@ChangeVersion=Ct.Sys_Change_version,@CreationVersion=Ct.Sys_Change_Creation_version
----FROM 
----CHANGETABLE(CHANGES dbo.DivisionMaster, 0) AS CT
----WHERE numDivisionID=@numRecordID

----Begin

----SET @last_synchronization_version=@ChangeVersion-1     
	
----		--Fields Update & for Type :Insert or Update:
----			Declare @UFFields as table 
----			(
----			    bitActiveInActive varchar(70),
----				numAssignedBy varchar(70),
----				numAssignedTo varchar(70),
----				numCampaignID varchar(70),
----				numCompanyDiff varchar(70),
----				vcCompanyDiff varchar(70), 
----				numCurrencyID varchar(70),
----				numFollowUpStatus1 varchar(70),
----				numFollowUpStatus varchar(70),
----				numGrpID varchar(70),
----				bintCreatedDate varchar(70),
----				vcComFax varchar(70),
----				bitPublicFlag varchar(70),
----				tintCRMType varchar(70),
----				bitNoTax varchar(70),
----				numTerID varchar(70),
----				numStatusID varchar(70),
----				vcComPhone varchar(70)
----			)

----	INSERT into @UFFields(bitActiveInActive,numAssignedBy,numAssignedTo,numCampaignID,numCompanyDiff,vcCompanyDiff,numCurrencyID,numFollowUpStatus1,numFollowUpStatus,numGrpID,bintCreatedDate,vcComFax,bitPublicFlag,tintCRMType,bitNoTax,numTerID,numStatusID,vcComPhone)
----		SELECT
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'bitActiveInActive', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bitActiveInActive ,
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numAssignedBy', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numAssignedBy, 
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numAssignedTo', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numAssignedTo, 
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numCampaignID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numCampaignID, 
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numCompanyDiff', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numCompanyDiff	,
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'vcCompanyDiff', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS  vcCompanyDiff ,
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numCurrencyID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numCurrencyID, 
----        CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numFollowUpStatus1', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numFollowUpStatus1,
----        CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numFollowUpStatus', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numFollowUpStatus,
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numGrpID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numGrpID,
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'bintCreatedDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bintCreatedDate,
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'vcComFax', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcComFax,
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'bitPublicFlag', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bitPublicFlag,
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'tintCRMType', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS tintCRMType,
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'bitNoTax', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bitNoTax,
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numTerID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numTerID,
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numStatusID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numStatusID,
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'vcComPhone', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcComPhone
----		FROM 
----		CHANGETABLE(CHANGES dbo.DivisionMaster, @last_synchronization_version) AS CT
----		WHERE 
----		ct.numDivisionID=@numRecordID

----		--sp_helptext USP_GetWorkFlowFormFieldMaster
----	--exec USP_GetWorkFlowFormFieldMaster 1,68
----INSERT INTO @tblFields(FieldName,FieldValue)
----		SELECT
----		FieldName,
----		FieldValue
----		FROM
----			(
----				SELECT  bitActiveInActive,numAssignedBy,numAssignedTo,numCampaignID,numCompanyDiff,vcCompanyDiff,numCurrencyID,numFollowUpStatus1,numFollowUpStatus,numGrpID,bintCreatedDate,vcComFax,bitPublicFlag,tintCRMType,bitNoTax,numTerID,numStatusID,vcComPhone
----				FROM @UFFields	
----			) AS UP
----		UNPIVOT
----		(
----		FieldValue FOR FieldName IN (bitActiveInActive,numAssignedBy,numAssignedTo,numCampaignID,numCompanyDiff,vcCompanyDiff,numCurrencyID,numFollowUpStatus1,numFollowUpStatus,numGrpID,bintCreatedDate,vcComFax,bitPublicFlag,tintCRMType,bitNoTax,numTerID,numStatusID,vcComPhone)
----		) AS upv
----		where FieldValue<>0
 

----End

------select 'col1' from @UFFields where numCreatedBy='0' and numModifiedBy='0'
------ If Exists (select 'col1' from @UFFields where numCreatedBy='0' and numModifiedBy='0')
------ Begin
------ set @Type='I'
------ End
------ Else
------ Begin
------ set @Type='U'
------ End
------ select @Type

----DECLARE @tintWFTriggerOn AS TINYINT
----SET @tintWFTriggerOn=CASE @Type WHEN 'I' THEN 1 WHEN 'U' THEN 2 WHEN 'D' THEN 5 END

----Declare @numAssignedTo numeric(18,0)
----Declare @numAssignedBy numeric(18,0)

----Select @numAssignedTo=numAssignedTo,@numAssignedBy=numAssignedBy from DivisionMaster where numDivisionID=@numRecordID

------For Fields Update Exection Point
----DECLARE @Columns_Updated VARCHAR(1000)
----SELECT @Columns_Updated = COALESCE(@Columns_Updated + ',', '') + FieldName FROM @tblFields 
----SET @Columns_Updated=ISNULL(@Columns_Updated,'')

----if (@Columns_Updated='numAssignedTo,numAssignedBy')
----BEGIN

----If	(@numAssignedTo=0 AND @numAssignedBy=0)
---- BEGIN
----  SET @type=1
---- END
---- Else
---- BEGIN
----  SET @type=2
---- END
----END
----Else 
----BEGIN
---- SET @type=2
----END

------Adding data As per Opertaion
----	EXEC dbo.USP_ManageWorkFlowQueue
----	@numWFQueueID = 0, --  numeric(18, 0)
----	@numDomainID = @numDomainID, --  numeric(18, 0)
----	@numUserCntID = @numUserCntID, --  numeric(18, 0)
----	@numRecordID = @numRecordID, --  numeric(18, 0)
----	@numFormID = 68, --  numeric(18, 0)
----	@tintProcessStatus = 1, --  tinyint
----	@tintWFTriggerOn = @tintWFTriggerOn, --  tinyint
----	@tintMode = 1, --  tinyint
----	@vcColumnsUpdated = @Columns_Updated

	
----/***************************************************Commented SP for Checking RowVersion with Change Tracking:Date:23Apr||Sachin********************************************************/



------DECLARE @synchronization_version BIGINT 



------SET @synchronization_version = CHANGE_TRACKING_CURRENT_VERSION();











------SELECT 



------    CT.numOppID, CT.SYS_CHANGE_OPERATION, 



------    CT.SYS_CHANGE_COLUMNS, CT.SYS_CHANGE_CONTEXT, 



------    CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numOppID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numOppID, 



------    CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'vcPOppName', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcPOppName 



------FROM 



------    CHANGETABLE(CHANGES dbo.OpportunityMaster, @synchronization_version) AS CT











------SET @synchronization_version = CHANGE_TRACKING_CURRENT_VERSION();



----/***************************************************End of Comment||Sachin********************************************************/



----END

----GO
----/****** Object:  StoredProcedure [dbo].[USP_StagePercentageDetails_CT]    Script Date: 31-May-14 5:59:21 PM ******/
----SET ANSI_NULLS ON
----GO
----SET QUOTED_IDENTIFIER ON
----GO
----Create PROCEDURE [dbo].[USP_StagePercentageDetails_CT]
----	-- Add the parameters for the stored procedure here
----    @numDomainID numeric(18, 0)=0,
----    @numUserCntID numeric(18, 0)=0,
----    @numRecordID numeric(18, 0)=0 
----AS
----BEGIN
------ SET NOCOUNT ON added to prevent extra result sets from
------ interfering with SELECT statements.
----SET NOCOUNT ON;
---- -- Insert statements for procedure here
---- declare @tblFields as table (

---- FieldName varchar(200),
---- FieldValue varchar(200)
----)

----Declare @Type char(1)
----Declare @ChangeVersion bigint
----Declare @CreationVersion bigint
----Declare @last_synchronization_version bigInt=null

----SELECT 			
----@ChangeVersion=Ct.Sys_Change_version,@CreationVersion=Ct.Sys_Change_Creation_version
----FROM 
----CHANGETABLE(CHANGES dbo.StagePercentageDetails, 0) AS CT
----WHERE numStageDetailsId=@numRecordID

----IF (@ChangeVersion=@CreationVersion)--Insert
----Begin
----        SELECT 
----		@Type = CT.SYS_CHANGE_OPERATION
----		FROM 
----		CHANGETABLE(CHANGES dbo.StagePercentageDetails, @last_synchronization_version) AS CT
----		WHERE  CT.numStageDetailsId=@numRecordID

----End

----Else
----Begin

----SET @last_synchronization_version=@ChangeVersion-1     
	
----	  SELECT 
----		@Type = CT.SYS_CHANGE_OPERATION
----		FROM 
----		CHANGETABLE(CHANGES dbo.StagePercentageDetails, @last_synchronization_version) AS CT
----		WHERE  CT.numStageDetailsId=@numRecordID


----		--Fields Update & for Type :Insert or Update:
----			Declare @UFFields as table 
----			(
----			   	numStageDetailsId varchar(70),
----				dtStartDate varchar(70),
----				dtEndDate varchar(70),
----				tintPercentage varchar(70),
----				tinProgressPercentage varchar(70),
----				numAssignTo varchar(70),
----				intDueDays varchar(70),
----				numModifiedBy varchar(70),
----				numCreatedBy varchar(70)

----			)


----	INSERT into @UFFields(numStageDetailsId,dtStartDate,dtEndDate,tintPercentage,tinProgressPercentage,numAssignTo,intDueDays,numModifiedBy,numCreatedBy)
----		SELECT
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.StagePercentageDetails'), 'numStageDetailsId', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numStageDetailsId ,
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.StagePercentageDetails'), 'dtStartDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS dtStartDate, 
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.StagePercentageDetails'), 'dtEndDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS dtEndDate, 
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.StagePercentageDetails'), 'tintPercentage', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS tintPercentage, 
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.StagePercentageDetails'), 'tinProgressPercentage', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS tinProgressPercentage	,
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.StagePercentageDetails'), 'numAssignTo', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS  numAssignTo ,
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.StagePercentageDetails'), 'intDueDays', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS intDueDays,
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.StagePercentageDetails'), 'numModifiedBy', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numModifiedBy, 
----		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.StagePercentageDetails'), 'numCreatedBy', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numCreatedBy  
----		FROM 
----		CHANGETABLE(CHANGES dbo.StagePercentageDetails, @last_synchronization_version) AS CT
----		WHERE 
----		ct.numStageDetailsId=@numRecordID

----		--sp_helptext USP_GetWorkFlowFormFieldMaster
----	--exec USP_GetWorkFlowFormFieldMaster 1,90
----INSERT INTO @tblFields(FieldName,FieldValue)
----		SELECT
----		FieldName,
----		FieldValue
----		FROM
----			(
----				SELECT numStageDetailsId,dtStartDate,dtEndDate,tintPercentage,tinProgressPercentage,numAssignTo,intDueDays,numModifiedBy,numCreatedBy
----				FROM @UFFields	
----			) AS UP
----		UNPIVOT
----		(
----		FieldValue FOR FieldName IN (numStageDetailsId,dtStartDate,dtEndDate,tintPercentage,tinProgressPercentage,numAssignTo,intDueDays,numModifiedBy,numCreatedBy)
----		) AS upv
----		where FieldValue<>0
 

----End

------select 'col1' from @UFFields where numCreatedBy='0' and numModifiedBy='0'
------ If Exists (select 'col1' from @UFFields where numCreatedBy='0' and numModifiedBy='0')
------ Begin
------ set @Type='I'
------ End
------ Else
------ Begin
------ set @Type='U'
------ End
------ select @Type
----DECLARE @tintWFTriggerOn AS TINYINT
----SET @tintWFTriggerOn=CASE @Type WHEN 'I' THEN 1 WHEN 'U' THEN 2 WHEN 'D' THEN 5 END

------For Fields Update Exection Point
----DECLARE @Columns_Updated VARCHAR(1000)
----SELECT @Columns_Updated = COALESCE(@Columns_Updated + ',', '') + FieldName FROM @tblFields 
----SET @Columns_Updated=ISNULL(@Columns_Updated,'')

----SELECT @Columns_Updated

------Adding data As per Opertaion
----	EXEC dbo.USP_ManageWorkFlowQueue
----	@numWFQueueID = 0, --  numeric(18, 0)
----	@numDomainID = @numDomainID, --  numeric(18, 0)
----	@numUserCntID = @numUserCntID, --  numeric(18, 0)
----	@numRecordID = @numRecordID, --  numeric(18, 0)
----	@numFormID = 94, --  numeric(18, 0)
----	@tintProcessStatus = 1, --  tinyint
----	@tintWFTriggerOn = @tintWFTriggerOn, --  tinyint
----	@tintMode = 1, --  tinyint
----	@vcColumnsUpdated = @Columns_Updated

	
----/***************************************************Commented SP for Checking RowVersion with Change Tracking:Date:23Apr||Sachin********************************************************/



------DECLARE @synchronization_version BIGINT 



------SET @synchronization_version = CHANGE_TRACKING_CURRENT_VERSION();











------SELECT 



------    CT.numOppID, CT.SYS_CHANGE_OPERATION, 



------    CT.SYS_CHANGE_COLUMNS, CT.SYS_CHANGE_CONTEXT, 



------    CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numOppID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numOppID, 



------    CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'vcPOppName', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcPOppName 



------FROM 



------    CHANGETABLE(CHANGES dbo.OpportunityMaster, @synchronization_version) AS CT











------SET @synchronization_version = CHANGE_TRACKING_CURRENT_VERSION();



----/***************************************************End of Comment||Sachin********************************************************/



----END
	




----END

----GO
----/****** Object:  StoredProcedure [dbo].[USP_WFConditionQueryExecute]    Script Date: 31-May-14 5:59:21 PM ******/
----SET ANSI_NULLS ON
----GO
----SET QUOTED_IDENTIFIER ON
----GO
----CREATE PROCEDURE [dbo].[USP_WFConditionQueryExecute]     
----    @numDomainID numeric(18, 0),
----    @numRecordID numeric(18, 0),
----    @textQuery NTEXT,
----    @boolCondition BIT OUTPUT,
----    @tintMode AS TINYINT
----as                 

----IF @tintMode=1 --Condition Check
----BEGIN
----	DECLARE @TotalRecords AS int
	
----	EXEC sp_executesql @textQuery,N'@numDomainID numeric(18, 0),@numRecordID numeric(18,0),@TotalRecords int OUTPUT',
----	@numDomainID=@numDomainID,@numRecordID=@numRecordID,@TotalRecords=@TotalRecords OUTPUT

----	SET @boolCondition = CASE WHEN @TotalRecords=0 THEN 0 ELSE 1 END	
----END
----ELSE IF @tintMode=2 --Field Update
----BEGIN
----	EXEC sp_executesql @textQuery,N'@numDomainID numeric(18, 0),@numRecordID numeric(18,0)',
----	@numDomainID=@numDomainID,@numRecordID=@numRecordID
----END
----/****** Object:  StoredProcedure [dbo].[USP_GetCaseListForPortal]    Script Date: 15/10/2013 ******/
----SET ANSI_NULLS ON

----GO
----/****** Object:  StoredProcedure [dbo].[USP_WorkFlow_TimeBasdAction]    Script Date: 31-May-14 5:59:21 PM ******/
----SET ANSI_NULLS ON
----GO
----SET QUOTED_IDENTIFIER ON
----GO
------ =============================================    
------ Author:  <Author,,Sachin Sadhu>    
------ Create date: <Create Date,,27thMarch2014>    
------ Description: <Description,,To Implement Time based action> 
------EXEC USP_WorkFlow_TimeBasdAction 95587,'OpportunityMaster','BINTCREATEDDATE',2,2,1,@result OUTPUT       
------ =============================================       
----CREATE PROCEDURE [dbo].[USP_WorkFlow_TimeBasdAction]     
---- -- Add the parameters for the stored procedure here    
     
---- @numRecordID numeric(18, 0)=0,      
---- @TableName VARCHAR(50),    
---- @columnname   VARCHAR(50),    
---- @intDays INT,    
---- @intActionOn INT,    
---- @numDomainID numeric(18, 0)=0    
----AS    
----BEGIN    
---- -- SET NOCOUNT ON added to prevent extra result sets from    
---- -- interfering with SELECT statements.    
---- --SET NOCOUNT ON;    
----    -- Insert statements for procedure here    
---- DECLARE @MatchingDate DATETIME    
---- DECLARE @CurrentDate DATETIME    
----    SET @CurrentDate=DATEADD(DD,0, DATEDIFF(DD,0, GETDATE()))    
---- PRINT 'sachin'    
---- DECLARE @SQL NVARCHAR(4000)    
---- SET @SQL='SELECT @MatchingDate=['+replace(@columnname,' ','')+'] FROM '+@TableName+' WHERE numOppID='+ CAST(@numRecordID as varchar(50))    
---- PRINT @SQL    
---- EXEC SP_EXECUTESQL @SQL, N'@MatchingDate DATETIME OUTPUT',@MatchingDate OUTPUT    
----    DECLARE @result VARCHAR(50)    
----    SET @result=0    
----   IF (@intActionOn=1)    
----   BEGIN    
----   IF (@CurrentDate=(DATEADD(DD,0, DATEDIFF(DD,@intDays, @MatchingDate))))    
----   BEGIN    
----    set @result=1    
----   END    
----  ELSE    
----   BEGIN    
----    set @result=0    
----   END    
       
----   END    
----   ELSE    
----   BEGIN    
----   IF (@CurrentDate=(DATEADD(DD,@intDays, DATEDIFF(DD,0, @MatchingDate))))    
----   BEGIN    
----    set @result=1    
----   END    
----  ELSE    
----   BEGIN    
----    set @result=0    
----   END    
----   END    
       
---- SELECT @result AS TimeMatchStatus      
    
    
    
----END 

----GO
----/****** Object:  StoredProcedure [dbo].[USP_WorkFlowAlertList_Insert]    Script Date: 31-May-14 5:59:21 PM ******/
----SET ANSI_NULLS ON
----GO
----SET QUOTED_IDENTIFIER ON
----GO
  
------ =============================================  
------ Author:  <Author,,Sachin Sadhu>  
------ Create date: <Create Date,,1stApr2014>  
------ Description: <Description,,To Save Alert Message on condition match>  
------ =============================================  
----CREATE PROCEDURE [dbo].[USP_WorkFlowAlertList_Insert]   
---- -- Add the parameters for the stored procedure here  
---- @vcAlertMessage NTEXT=Null,   
---- @numWFID NUMERIC(18,0),  
---- @intAlertStatus int,  
---- @numDomainID numeric(18,0)  
----AS  
----BEGIN  
---- -- SET NOCOUNT ON added to prevent extra result sets from  
---- -- interfering with SELECT statements.  
---- SET NOCOUNT ON;  
  
----    -- Insert statements for procedure here  
  
----   INSERT INTO WorkFlowAlertList  
----                      (vcAlertMessage,numDomainID,numWFID,intAlertStatus)  
----     VALUES     (@vcAlertMessage,@numDomainID,@numWFID,@intAlertStatus)   
----END  
----GO
----/****** Object:  StoredProcedure [dbo].[USP_WorkflowAlertList_SelectAll]    Script Date: 31-May-14 5:59:21 PM ******/
----SET ANSI_NULLS ON
----GO
----SET QUOTED_IDENTIFIER ON
----GO
  
 
------ =============================================  
------ Author:  <Author,,Sachin Sadhu>  
------ Create date: <Create Date,,1stApr2014>  
------ Description: <Description,,Get WorkFlow Alerts as per domain>  
------ =============================================  
----Create PROCEDURE [dbo].[USP_WorkflowAlertList_SelectAll] 
---- -- Add the parameters for the stored procedure here  
---- @numDomainId numeric(18,0)  
----AS  
----BEGIN  
---- -- SET NOCOUNT ON added to prevent extra result sets from  
---- -- interfering with SELECT statements.  
---- SET NOCOUNT ON;  
  
----    -- Insert statements for procedure here  
---- SELECT  Row_Number() over ( order by a.numWFAlertID) as Number, a.numWFAlertID, a.vcAlertMessage,a.numDomainId ,a.intAlertStatus,a.numWFID  FROM WorkflowAlertList AS a   WHERE a.numDomainId=@numDomainId AND a.intAlertStatus=1  
----END  
  
  
----GO
----/****** Object:  StoredProcedure [dbo].[USP_WorkflowAlertList_UpdateStatus]    Script Date: 31-May-14 5:59:21 PM ******/
----SET ANSI_NULLS ON
----GO
----SET QUOTED_IDENTIFIER ON
----GO
  
------ =============================================  
------ Author:  <Author,,Sachin Sadhu>  
------ Create date: <Create Date,,2ndApril2014>  
------ Description: <Description,,To update Alert Message Status>  
------ =============================================  
----CREATE PROCEDURE [dbo].[USP_WorkflowAlertList_UpdateStatus]    
---- @numWFAlertID NUMERIC(18,0),  
---- @intAlertStatus  INT  
----AS    
  
----BEGIN     
---- UPDATE dbo.WorkFlowAlertList SET intAlertStatus=@intAlertStatus WHERE numWFAlertID=@numWFAlertID  
----END   
----GO
----/****** Object:  StoredProcedure [dbo].[USP_WorkFlowMaster_History]    Script Date: 31-May-14 5:59:21 PM ******/
----SET ANSI_NULLS ON
----GO
----SET QUOTED_IDENTIFIER ON
----GO
------ =============================================      
------ Author:  <Author,,Sachin Sadhu>      
------ Create date: <Create Date,,3rdApril2014>      
------ Description: <Description,,To maintain Work rule execution history>      
------ =============================================      
----CREATE PROCEDURE [dbo].[USP_WorkFlowMaster_History]       
---- -- Add the parameters for the stored procedure here      
----@numDomainID numeric(18,0),      
----@numFormID INT  ,    
----@CurrentPage int,                                                              
----@PageSize int,                                                              
----@TotRecs int output,           
----@SortChar char(1)='0' ,                                                             
----@columnName as Varchar(50),                                                              
----@columnSortOrder as Varchar(50)  ,      
----@SearchStr  as Varchar(50)         
----AS      
----BEGIN      
---- -- SET NOCOUNT ON added to prevent extra result sets from      
---- -- interfering with SELECT statements.      
---- SET NOCOUNT ON;      
      
----    -- Insert statements for procedure here      
----    Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                               
----        numWFID NUMERIC(18,0) ,    
----        numFormID NUMERIC(18,0),    
----        vcWFName VARCHAR(50),      
----        vcPOppName VARCHAR(500),      
----        vcWFDescription VARCHAR(max),      
----        numRecordID NUMERIC(18,0),      
----        tintProcessStatus int,      
----        bitSuccess int,      
----        vcDescription VARCHAR(max),      
----        vcFormName VARCHAR(50),      
----        dtExecutionDate DATETIME,    
----        numOppId NUMERIC(18,0),      
----        STATUS VARCHAR(50),    
----        TriggeredOn VARCHAR(50) ,  
----        vcUserName VARCHAR(200)                                                           
---- )           
----declare @strSql as varchar(8000)                                                        
          
----set  @strSql='  SELECT       
      
----   m.numWFID,      
----   m.numFormID,    
----   m.vcWFName,      
----   o.vcPOppName,      
----   m.vcWFDescription,      
----   q.numRecordID,      
----   q.tintProcessStatus,      
----   e.bitSuccess,      
----   e.vcDescription ,      
----   DFM.vcFormName,      
----   e.dtExecutionDate,    
----   o.numOppId,      
----   CASE WHEN q.tintProcessStatus=1 THEN ''Pending Execution'' WHEN q.tintProcessStatus= 2 THEN ''In Progress''  WHEN q.tintProcessStatus=3 THEN ''Success'' WHEN q.tintProcessStatus=4 then ''Failed'' else ''No WF Found'' END AS STATUS,      
----   CASE WHEN (m.tintWFTriggerOn=1 AND m.intDays=0)THEN ''Create'' WHEN m.tintWFTriggerOn=2 THEN ''Edit'' WHEN m.tintWFTriggerOn=3 THEN ''Create or Edit'' WHEN m.tintWFTriggerOn=4 THEN ''Fields Update'' WHEN m.tintWFTriggerOn=5 THEN ''Delete'' WHEN m.intDays>0 THEN     
----''Date Field'' ELSE ''NA'' end AS TriggeredOn,  
---- dbo.fn_GetContactName(ISNULL(q.numCreatedBy,0)) AS vcUserName      
----  FROM dbo.WorkFlowMaster as m       
----  INNER JOIN dbo.WorkFlowQueue AS q ON m.numWFID=q.numWFID       
----  LEFT JOIN dbo.WorkFlowQueueExecution AS e ON e.numWFQueueID=q.numWFQueueID      
----  INNER JOIN dbo.OpportunityMaster o ON q.numRecordID =o.numOppId      
----  JOIN DynamicFormMaster DFM ON m.numFormID=DFM.numFormID AND DFM.tintFlag=3    WHERE m.numDomainID='+ convert(varchar(15),@numDomainID)       
          
----if @SortChar<>'0' set @strSql=@strSql + ' And m.vcWFName like '''+@SortChar+'%'''           
      
----if @numFormID <> 0 set @strSql=@strSql + ' And m.numFormID = '+ convert(varchar(15),@numFormID) +''         
      
----if @SearchStr<>'' set @strSql=@strSql + ' And (m.vcWFName like ''%'+@SearchStr+'%'' or       
----m.vcWFDescription like ''%'+@SearchStr+'%'') '       
          
----set  @strSql=@strSql +'ORDER BY ' + @columnName +' '+ @columnSortOrder          
     
----insert into #tempTable( numWFID ,    
----            numFormID ,    
----            vcWFName ,    
----            vcPOppName ,    
----            vcWFDescription ,    
----            numRecordID ,    
----            tintProcessStatus ,    
----            bitSuccess ,    
----            vcDescription ,    
----            vcFormName ,    
----            dtExecutionDate ,    
----            numOppId ,    
----            STATUS ,    
----            TriggeredOn,vcUserName) exec(@strSql)          
          
---- declare @firstRec as integer                                                              
---- declare @lastRec as integer                                                   
---- set @firstRec= (@CurrentPage-1) * @PageSize                                                              
---- set @lastRec= (@CurrentPage*@PageSize+1)                                                               
      
---- SELECT  @TotRecs = COUNT(*)  FROM #tempTable       
       
----  SELECT       
----   ID AS RowNo,*    
----   From #tempTable T        
----   WHERE ID > @firstRec and ID < @lastRec  order by ID      
       
       
------   SELECT       
------   ROW_NUMBER() OVER(ORDER BY m.numWFID ASC) AS RowNo,    
------   m.numWFID,      
------   m.vcWFName,      
------   o.vcPOppName,      
------   m.vcWFDescription,      
------   q.numRecordID,      
------   q.tintProcessStatus,      
------   e.bitSuccess,      
------   e.vcDescription ,      
------   DFM.vcFormName,      
------   e.dtExecutionDate,    
------   o.numOppId,      
------   CASE WHEN q.tintProcessStatus=1 THEN 'Pending Execution' WHEN q.tintProcessStatus= 2 THEN 'In Progress' WHEN q.tintProcessStatus=3 THEN 'Success' WHEN q.tintProcessStatus=4 then 'Failed' else 'No WF Found' END AS STATUS,      
------   CASE WHEN (m.tintWFTriggerOn=1 AND m.intDays=0)THEN 'Create' WHEN m.tintWFTriggerOn=2 THEN 'Edit' WHEN m.tintWFTriggerOn=3 THEN 'Create or Edit' WHEN m.tintWFTriggerOn=4 THEN 'Fields Update' WHEN m.tintWFTriggerOn=5 THEN 'Delete' WHEN m.intDays>0 TH
  
------E--N     
------'Date Field' ELSE 'NA' end AS TriggeredOn      
------  FROM dbo.WorkFlowMaster as m       
------  INNER JOIN dbo.WorkFlowQueue AS q ON m.numWFID=q.numWFID       
------  LEFT JOIN dbo.WorkFlowQueueExecution AS e ON e.numWFQueueID=q.numWFQueueID      
------  INNER JOIN dbo.OpportunityMaster o ON q.numRecordID =o.numOppId      
------  JOIN DynamicFormMaster DFM ON m.numFormID=DFM.numFormID AND DFM.tintFlag=3  join #tempTable T on T.numWFID=m.numWFID      
------    
------   WHERE ID > @firstRec and ID < @lastRec AND m.numDomainID=@numDomainID order by ID      
         
----   DROP TABLE #tempTable      
        
        
        
        
        
        
        
        
        
        
     
    
      
      
----END 
----GO
----ALTER PROCEDURE [dbo].[USP_GetDropDownValue]     
----    @numDomainID numeric(18, 0),
----    @numListID numeric(18, 0),
----    @vcListItemType AS VARCHAR(5),
----    @vcDbColumnName VARCHAR(50)
----as                 

----CREATE TABLE #temp(numID VARCHAR(50),vcData VARCHAR(100))

----			 IF @vcListItemType='LI' OR @vcListItemType='T'                                                    
----				BEGIN  
----					INSERT INTO #temp                                                   
----					SELECT Ld.numListItemID,vcData from ListDetails LD          
----					left join listorder LO on Ld.numListItemID= LO.numListItemID  and lo.numDomainId = @numDomainID
----					where (Ld.numDomainID=@numDomainID or Ld.constFlag=1) and Ld.numListID=@numListID order by intSortOrder
----				end  
---- 			 ELSE IF @vcListItemType='C'                                                     
----				begin    
----		   		    INSERT INTO #temp                                                   
----	  			    SELECT  numCampaignID,vcCampaignName + CASE bitIsOnline WHEN 1 THEN ' (Online)' ELSE ' (Offline)' END AS vcCampaignName
----					FROM CampaignMaster WHERE numDomainID = @numDomainID
----				end  
----			 ELSE IF @vcListItemType='AG'                                                     
----				begin      
----		   		    INSERT INTO #temp                                                   
----					SELECT numGrpID, vcGrpName FROM groups       
----					ORDER BY vcGrpName                                               
----				end   
----			else if @vcListItemType='DC'                                                     
----				begin    
----		   		    INSERT INTO #temp                                                   
----					select numECampaignID,vcECampName from ECampaign where numDomainID= @numDomainID                                                       
----				end 
----			else if @vcListItemType='U' OR @vcDbColumnName='numManagerID'
----			    BEGIN
----		   		     INSERT INTO #temp                                                   
----			    	 SELECT A.numContactID,ISNULL(A.vcFirstName,'') +' '+ ISNULL(A.vcLastName,'') as vcUserName          
----					 from UserMaster UM join AdditionalContactsInformation A        
----					 on UM.numUserDetailId=A.numContactID          
----					 where UM.numDomainID=@numDomainID and UM.numDomainID=A.numDomainID  ORDER BY A.vcFirstName,A.vcLastName 
----				END
----			else if @vcListItemType='S'
----			    BEGIN
----		   		     INSERT INTO #temp                                                   
----			    	 SELECT S.numStateID,S.vcState          
----					 from State S      
----					 where S.numDomainID=@numDomainID ORDER BY S.vcState
----				END
----			ELSE IF @vcListItemType = 'OC' 
----				BEGIN
----					INSERT INTO #temp   
----					SELECT DISTINCT C.numCurrencyID,C.vcCurrencyDesc FROM dbo.Currency C WHERE C.numDomainID=@numDomainID
----				END
----			ELSE IF @vcListItemType = 'UOM' 
----				BEGIN
----					INSERT INTO #temp   
----					SELECT  numUOMId AS numItemID,vcUnitName AS vcItemName FROM UOM WHERE numDomainID = @numDomainID
----				END	
----			ELSE IF @vcListItemType = 'IG' 
----				BEGIN
----					INSERT INTO #temp   
----					SELECT numItemGroupID,vcItemGroup FROM dbo.ItemGroups WHERE numDomainID=@numDomainID   	
----				END
----			ELSE IF @vcListItemType = 'SYS'
----				BEGIN
----					INSERT INTO #temp
----					SELECT 0 AS numItemID,'Lead' AS vcItemName
----					UNION ALL
----					SELECT 1 AS numItemID,'Prospect' AS vcItemName
----					UNION ALL
----					SELECT 2 AS numItemID,'Account' AS vcItemName
----				END
----			ELSE IF @vcListItemType = 'PP' 
----				BEGIN
----					INSERT INTO #temp 
----					SELECT 'P','Inventory Item'
----					UNION 
----					SELECT 'N','Non-Inventory Item'
----					UNION 
----					SELECT 'S','Service'
----				END

----				--<asp:ListItem Value="0" Selected="True">0</asp:ListItem>
----    --                                <asp:ListItem Value="25" Selected="True">25%</asp:ListItem>
----    --                                <asp:ListItem Value="50">50%</asp:ListItem>
----    --                                <asp:ListItem Value="75">75%</asp:ListItem>
----    --                                <asp:ListItem Value="100">100%</asp:ListItem>
----				ELSE IF @vcListItemType = 'SP' 
----				BEGIN
----					INSERT INTO #temp 
----					SELECT '0','0%'
----					UNION 
----					SELECT '25','25%'
----					UNION 
----					SELECT '50','50%'
----					UNION 
----					SELECT '75','75%'
----					UNION 
----					SELECT '100','100%'
----				END

----		    ELSE IF @vcDbColumnName='charSex'
----				BEGIN
----					 INSERT INTO #temp
----					 SELECT 'M','Male' UNION
----					 SELECT 'F','Female'
----				 END
----			ELSE IF @vcDbColumnName='tintOppStatus'
----			  BEGIN
----					 INSERT INTO #temp
----					 SELECT 1,'Deal Won' UNION
----					 SELECT 2,'Deal Lost'
----			  END
----			ELSE IF @vcDbColumnName='tintOppType'
----			  BEGIN
----					 INSERT INTO #temp
----					 SELECT 1,'Sales Order' UNION
----					 SELECT 2,'Purchase Order' UNION
----					 SELECT 3,'Sales Opportunity' UNION
----					 SELECT 4,'Purchase Opportunity' 
----			  END
			 
------			Else                                                     
------				BEGIN                                                     
------					select @listName=vcData from ListDetails WHERE numListItemID=@numlistID
------				end 	

---- INSERT INTO #temp  SELECT 0,'--None--'				
        
----		SELECT * FROM #temp 
----		DROP TABLE #temp	

/**************************************------------- Adding Notes Field :Date-4thJune2014--------***********************************************************************************************/

--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength)
--VALUES        (3,Null,'Notes','vcNotes','vcNotes','vcNotes','OpportunityBizDocItems','V','R','TextBox',Null,NULL,0,Null,1,1,1,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null)


--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,523,Null,15,0,0,'Notes','TextBox','vcNotes',Null,Null,Null,Null,Null,Null,Null,0,0,0,0,0,0,0,1,Null,0,3,Null)

	
	
-----Adding nunListType to Add bizdocStatus with BizDocType	
	
--ALTER TABLE dbo.ListDetails ADD
--	numListType numeric(9,0) NULL
		

----Parent-child tracking ...
----1.BizDocs
--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength)
--VALUES        (3,Null,'Order Status','numStatus','numStatus',Null,'OpportunityMaster','N','R','SelectBox',Null,'LI',176,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,10532,Null,49,1,0,'Order Status','numStatus','SelectBox',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)


--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength)
--VALUES        (3,Null,'Deal Status','tintOppStatus','tintOppStatus',Null,'OpportunityMaster','N','R','SelectBox',Null,NULL,0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,10535,Null,49,1,0,'Deal Status','tintOppStatus','SelectBox',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)


--/*-------------------------------------------------------------------------------------------------------Advance Search--------------------------*/


--/*Dated:11July2014*/

--	/*ALTER TABLE dbo.workFlowActionList ADD
--vcSMSText ntext NULL*/

--	/*After date:23rdJuly2014*/

--	update DynamicFormMaster set tintFlag=0 where numFormId in (74,95) --(changed 3 to 0)

----contacts
	
--ALTER TABLE dbo.AdditionalContactsInformation
--ENABLE CHANGE_TRACKING 
--WITH (TRACK_COLUMNS_UPDATED = ON);


----Projects
----ALTER TABLE dbo.ProjectsMaster
----ENABLE CHANGE_TRACKING 
----WITH (TRACK_COLUMNS_UPDATED = ON);

----Communication
--INSERT INTO dbo.DynamicFormMaster	
--        ( numFormId ,
--          vcFormName ,
--          cCustomFieldsAssociated ,
--          cAOIAssociated ,
--          bitDeleted ,
--          tintFlag ,
--          bitWorkFlow ,
--          vcLocationID ,
--          bitAllowGridColor
--        )
--VALUES  ( 124 , -- numFormId - numeric
--          'Action Items' , -- vcFormName - nvarchar(50)
--          'Y' , -- cCustomFieldsAssociated - char(1)
--          'N' , -- cAOIAssociated - char(1)
--          0 , -- bitDeleted - bit
--          3 , -- tintFlag - tinyint
--          0 , -- bitWorkFlow - bit
--          '2,6' , -- vcLocationID - varchar(50)
--          0  -- bitAllowGridColor - bit
--        )


--ALTER TABLE dbo.Communication
--ENABLE CHANGE_TRACKING 
--WITH (TRACK_COLUMNS_UPDATED = ON);

--ALTER TABLE dbo.PackagingRules ADD
--	numShippingRuleID numeric(18,0) NULL
--Update  PageNavigationDTL set vcPageNavName= 'Packaging Automation' where numPageNavID=199
--Update  PageNavigationDTL set vcPageNavName= 'Packaging/Box Rules' where numPageNavID=201
--Update  PageNavigationDTL set bitVisible=0 where numPageNavID=202

-----Adding nunListType to Add bizdocStatus with BizDocType	
	
--ALTER TABLE dbo.ListDetails ADD
--	numListType numeric(9,0) NULL

-------////------------------------------------------------------------------------------------------------------------------------------------------------


--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength)
--VALUES        (1,Null,'Assigned To','numAssign','numAssign',Null,'Communication','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null)

----10542




--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (1,10542,Null,124,1,0,'Assigned To','SelectBox','numAssign',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



----numAssignedBy

--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength)
--VALUES        (1,Null,'Assigned By','numAssignedBy','numAssignedBy',Null,'Communication','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null)

----10542




--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (1,10543,Null,124,1,0,'Assigned By','SelectBox','numAssignedBy',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



----numActivity

--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength)
--VALUES        (1,Null,'Activity','numActivity','numActivity',Null,'Communication','N','R','SelectBox',Null,'LI',32,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null)

----10542




--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (1,10544,Null,124,1,0,'Activity','SelectBox','numActivity',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




----numPrioirty

--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength)
--VALUES        (1,Null,'Priority','numStatus','numStatus',Null,'Communication','N','R','SelectBox',Null,'LI',447,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null)

----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (1,10545,Null,124,1,0,'Priority','SelectBox','numStatus',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




----Task Type

--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength)
--VALUES        (1,Null,'Task','bitTask','bitTask',Null,'Communication','N','R','SelectBox',Null,'LI',73,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null)

----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (1,10546,Null,124,1,0,'Task','SelectBox','bitTask',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



----Follow up Status

--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength)
--VALUES        (1,Null,'Follow-Up Status','numFollowUpstatus','numFollowUpstatus',Null,'FollowUpHistory','N','R','SelectBox',Null,'LI',30,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null)

----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (1,10547,Null,124,1,0,'Follow-Up Status','SelectBox','bitTask',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



----Snooze

--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength)
--VALUES        (1,Null,'Snooze','intSnoozeMins','intSnoozeMins',Null,'Communication','N','R','SelectBox',Null,'SM',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null)

----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (1,10548,Null,124,1,0,'Snooze','SelectBox','intSnoozeMins',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


----intRemainderMins

--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength)
--VALUES        (1,Null,'Reminder','intRemainderMins','intRemainderMins',Null,'Communication','N','R','SelectBox',Null,'SM',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null)

----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (1,10549,Null,124,1,0,'Reminder','SelectBox','intRemainderMins',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


----tintCorrType (Pin To)
--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength)
--VALUES        (1,Null,'Pin To','tintCorrType','tintCorrType',Null,'Correspondence','N','R','SelectBox',Null,'PT',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null)

----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (1,10550,Null,124,1,0,'Pin To','SelectBox','tintCorrType',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




----due date

--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength)
--VALUES        (1,Null,'Due Date','dtStartTime','dtStartTime',Null,'Communication','V','R','DateField',Null,Null,0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (1,10551,Null,124,1,0,'Due Date','DateField','dtStartTime',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


----Customer

--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength)
--VALUES        (1,Null,'Customer','numDivisionID','numDivisionID',Null,'Communication','N','R','SelectBox',Null,'DV',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null)

----10542




--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (1,10552,Null,124,1,0,'Customer','SelectBox','numDivisionID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



----BizDocs
--exec USP_GetWorkFlowFormFieldMaster 1,49
--exec USP_GetWorkFlowFormFieldMaster 1,70
--select * from DyCFieldMaster where numFieldID=20542

--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength)
--VALUES        (3,Null,'Customer','numDivisionID','numDivisionID',Null,'OpportunityMaster','N','R','SelectBox',Null,'DV',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null)

----10542




--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,20546,Null,49,1,0,'Customer','SelectBox','numDivisionID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


----Orders

--exec USP_GetWorkFlowFormFieldMaster 1,70
--select * from DyCFieldMaster where numFieldID=105

--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength)
--VALUES        (3,Null,'Credit Limit','numCompanyCredit','numCompanyCredit',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',3,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null)

----10542




--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,20547,Null,70,1,0,'Credit Limit','SelectBox','numCompanyCredit',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


----orders

----bitOnCreditHold


--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength)
--VALUES        (3,Null,'On Credit Hold','bitOnCreditHold','bitOnCreditHold',Null,'DivisionMaster','Y','R','CheckBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null)

----10542

----update DycFieldMaster set vcFieldDataType='Y' where numFieldID=20548

--select * from DycFormField_Mapping where  numFieldID=20548
----update DycFormField_Mapping set vcAssociatedControlType='CheckBox' where  numFieldID=20548

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,20548,Null,70,1,0,'On Credit Hold','CheckBox','bitOnCreditHold',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*Date:8thAug2014*/

--ALTER TABLE dbo.WorkFlowActionList ADD
--	vcEmailSendTo varchar(500) NULL

--	update dycfieldMaster set vcGroup='Organization Fields' where vcGroup='Order Details'

--	UPdate DycFieldMaster set vcListItemType='CR' ,vcGroup='Org. | Accounting Fields' where vcDbColumnName='numCurrencyID' and vcListItemType='U' 
	
--	ALTER TABLE dbo.WorkFlowActionList ADD
--	vcMailSubject varchar(500) NULL

--	ALTER TABLE dbo.WorkFlowActionList ADD
--	vcMailBody ntext NULL

--	ALTER TABLE dbo.WorkFlowconditionList ADD
--	intCompare  int NULL


	
--	ALTER TABLE dbo.DycFieldMaster ADD
--	intWFCompare int NULL


--update dycFieldMaster set intWFCompare=1 where numFieldID in (268,269)


--exec USP_GetWorkFlowFormFieldMaster 1,68
--exec USP_GetWorkFlowFormFieldMaster 1,70

--select * from DycFieldMaster where  vcLookBackTableName ='CFW_FLD_Values'
--update DycFieldMaster set vcGroup='Organization Details' where vcLookBackTableName='AddressDetails'
--update DycFieldMaster set vcGroup='Organization Details' where vcLookBackTableName='CompanyInfo'
--update DycFieldMaster set vcGroup='Org. | Accounting Fields' where numFieldId in (20547,20548)

--update DycFieldMaster set vcGroup='Organization Fields' where numFieldId in (10530,30549)


--select * from ModuleMaster

--select * from PageNavigationDTL where vcPageNavName='Auto Routing Rules'

--select * from PageNavigationDTL where vcPageNavName='Automation Rules'


--Update PageNavigationDTL set numParentID=66,vcImageURL=NULL where numPageNavID=65 --'../images/RoundLink.gif'

--select * from PageNavigationDTL where numPageNavID=65

--select * from PageNavigationDTL where vcPageNavName='e-Mail Broadcast'
--Update PageNavigationDTL set vcNavURL='../Marketing/frmEmailBroadcastConfigDetail.aspx' where numPageNavID=17 


--select * from PageNavigationDTL where vcPageNavName='Packaging Automation'

--Update PageNavigationDTL set vcNavURL='../Ecommerce/frmShippingLabelBatchProcessingList.aspx' where numPageNavID=199 

--select * from PageNavigationDTL where vcPageNavName='Custom Box' --../Marketing/frmEmailBroadcastConfigDetail.aspx 203,201
--select * from PageNavigationDTL where vcPageNavName='Packaging/Box Rules'
--select * from PageNavigationDTL where vcPageNavName='Pay Bill WorkFlow'
--Update PageNavigationDTL set bitVisible=0 where numPageNavID in (201,203,235)

--select * from PageNavigationDTL  where numPageNavID in (201,203,235) 



--	ALTER TABLE dbo.PageNavigationDTL ADD
--	intSortOrder int NULL

--select * from PageNavigationDTL where numParentID=73
--select * from PageNavigationDTL where numPageNavID=120
--Update PageNavigationDTL set vcPageNavName='Global Settings',intsortOrder=1 where numPageNavID=120 --Ecommerce Settings
--Update PageNavigationDTL set intsortOrder=2 where numPageNavID=103 --Categories
--Update PageNavigationDTL set vcPageNavName='Location Mapping',intsortOrder=3 where numPageNavID=119 --Warehouse Mapping
--Update PageNavigationDTL set vcPageNavName='Shipping',intsortOrder=4 where numPageNavID=000 --Shipping Company(new-shipping)
--Update PageNavigationDTL set vcPageNavName='Payment Gateways',intsortOrder=5 where numPageNavID=143 --Payment Gateways
--Update PageNavigationDTL set vcPageNavName='Marketplaces',intsortOrder=6 where numPageNavID=150 --Market Places(ol-e-Commerce API)
--Update PageNavigationDTL set vcPageNavName='Promotions',intsortOrder=7 where numPageNavID=187 --Promotions & Offers
--Update PageNavigationDTL set vcPageNavName='Error Codes',intsortOrder=8 where numPageNavID=197 --Error Message 
--Update PageNavigationDTL set vcPageNavName='Reviews & Ratings',intsortOrder=9 where numPageNavID=217 --Reviews & Ratings
--Update PageNavigationDTL set vcPageNavName='Design',intsortOrder=10 where numPageNavID=157 --Design Cart

--Update PageNavigationDTL set numParentID=236 where numPageNavID in (121,193) --under Shipping

--Update PageNavigationDTL set vcPageNavName='Shipping Carrier Setup' where numPageNavID in (121)
--Update PageNavigationDTL set vcPageNavName='Shipping Rules' where numPageNavID in (193)

----Ecommerce settings main
--update PageNavigationDTL set vcNavURL='../Items/frmECommerceSettings.aspx',vcPageNavName='e-Commerce' where numPageNavID=73

----Global Settings
--Update  PageNavigationDTL set vcPageNavName= 'Global Settings' where numPageNavID=79

--Update PageNavigationDTL set intSortOrder=101 where numPageNavID=35 --AdminiStration
--Update PageNavigationDTL set intSortOrder=102 where numPageNavID=57 --User Administration
--Update PageNavigationDTL set intSortOrder=103 where numPageNavID=61 --Master List Admin
--Update PageNavigationDTL set intSortOrder=104 where numPageNavID=64 --BizForms Wizard
--Update PageNavigationDTL set intSortOrder=105 where numPageNavID=66 --Workflow Automation
--Update PageNavigationDTL set intSortOrder=106 where numPageNavID=73 --Price & Cost Rules
--Update PageNavigationDTL set intSortOrder=107 where numPageNavID=78 --e-Commerce
--Update PageNavigationDTL set intSortOrder=108 where numPageNavID=101 --Recurring Template List
--Update PageNavigationDTL set intSortOrder=109 where numPageNavID=109 --Field Management
--Update PageNavigationDTL set intSortOrder=110 where numPageNavID=205 --Import Wizard


--Update PageNavigationDTL set intSortOrder=2000 where numPageNavID=79 --AdminiStration
--update PageNavigationDTL set vcImageURL='../images/Globe.png' where numPageNavID=79 


----for next update
--select * from PageNavigationDTL where numPageNavID=171

--update PageNavigationDTL set vcNavURL='../Items/frmItemList.aspx?Page=Asset Items&ItemGroup=0' where numPageNavID=171

----Organization Details for ORder Module



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength)
--VALUES        (3,Null,'Annual Revenue','numAnnualRevID','numAnnualRevID',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',6,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null)


----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,70,1,0,'Annual Revenue','SelectBox','numAnnualRevID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)





--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Assigned By','numAssignedBy','numAssignedBy',Null,'DivisionMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Organization Fields')


----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,70,1,0,'Assigned By','SelectBox','numAssignedBy',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Assigned To','numAssignedTo','numAssignedTo',Null,'DivisionMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Organization Fields')


----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,70,1,0,'Assigned To','SelectBox','numAssignedTo',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


----exec USP_GetWorkFlowFormFieldMaster 1,68


--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Campaign','numCampaignID','numCampaignID',Null,'DivisionMaster','N','R','SelectBox',Null,'C',24,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Organization Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,70,1,0,'Campaign','SelectBox','numCampaignID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)

--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Company Differentiation','numCompanyDiff','numCompanyDiff',Null,'DivisionMaster','N','R','SelectBox',Null,'LI',438,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Organization Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,70,1,0,'Company Differentiation','SelectBox','numCompanyDiff',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


----Group
--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Group','numGrpID','numGrpID',Null,'DivisionMaster','N','R','SelectBox',Null,'AG',38,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Organization Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,70,1,0,'Group','SelectBox','numGrpID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Follow-up Status','numFollowUpStatus','numFollowUpStatus',Null,'DivisionMaster','N','R','SelectBox',Null,'LI',30,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Organization Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,70,1,0,'Follow-up Status','SelectBox','numFollowUpStatus',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Default Trading Currency','numCurrencyID','numCurrencyID',Null,'DivisionMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Organization Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,70,1,0,'Default Trading Currency','SelectBox','numCurrencyID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Territory','numTerID','numTerID',Null,'DivisionMaster','N','R','SelectBox',Null,'T',78,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Organization Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,70,1,0,'Territory','SelectBox','numTerID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Organization Phone','vcComPhone','vcComPhone',Null,'DivisionMaster','V','R','TextBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Organization Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,70,1,0,'Organization Phone','TextBox','vcComPhone',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Organization Fax','vcComFax','vcComFax',Null,'DivisionMaster','V','R','TextBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Organization Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,70,1,0,'Organization Fax','TextBox','vcComFax',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Relationship Type','tintCRMType','tintCRMType',Null,'DivisionMaster','N','R','SelectBox',Null,'SYS',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Organization Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,70,1,0,'Relationship Type','SelectBox','tintCRMType',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Organization Status','numStatusID','numStatusID',Null,'DivisionMaster','N','R','SelectBox',Null,'LI',1,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Organization Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,70,1,0,'Organization Status','SelectBox','numStatusID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Department Name','vcDepartment','vcDepartment',Null,'AdditionalContactsInformation','N','R','SelectBox',Null,'LI',19,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Organization Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,70,1,0,'Department Name','SelectBox','vcDepartment',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Organization Rating','numCompanyRating','numCompanyRating',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',2,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Organization Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,70,1,0,'Organization Rating','SelectBox','numCompanyRating',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Lead Source','vcHow','vcHow',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',18,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Organization Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,70,1,0,'Lead Source','SelectBox','vcHow',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)






--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Net Days','numBillingDays','numBillingDays',Null,'DivisionMaster','N','R','SelectBox',Null,'BD',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org. | Accounting Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,70,1,0,'Net Days','SelectBox','numBillingDays',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)





--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Assigned To','numAssignedTo','numAssignedTo',Null,'OpportunityMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Assigned To','SelectBox','numAssignedTo',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)


--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Assigned By','numAssignedBy','numAssignedBy',Null,'OpportunityMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Assigned By','SelectBox','numAssignedBy',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Conclusion Reason','lngPConclAnalysis','lngPConclAnalysis',Null,'OpportunityMaster','N','R','SelectBox',Null,'LI',12,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Conclusion Reason','SelectBox','lngPConclAnalysis',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)




--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Opp Type','tintOppType','tintOppType',Null,'OpportunityMaster','N','R','SelectBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Opp Type','SelectBox','tintOppType',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Order Sub-total','monDealAmount','monDealAmount',Null,'OpportunityMaster','V','R','TextBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Order Sub-total','TextBox','monDealAmount',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Comments','txtComments','txtComments',Null,'OpportunityMaster','V','R','TextArea',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Comments','TextArea','txtComments',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)




--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Net Days','intBillingDays','intBillingDays',Null,'OpportunityMaster','V','R','SelectBox',Null,'BD',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Net Days','SelectBox','intBillingDays',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)


--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Estimated close date','intpEstimatedCloseDate','intpEstimatedCloseDate',Null,'OpportunityMaster','V','R','DateField',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Estimated close date','DateField','intpEstimatedCloseDate',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)

--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Closing Date','bintClosedDate','bintClosedDate',Null,'OpportunityMaster','V','R','DateField',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Closing Date','DateField','bintClosedDate',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)







----Organization Details for Projects

--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (5,Null,'Annual Revenue','numAnnualRevID','numAnnualRevID',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',6,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster order by numFieldId desc

----10542
----update DycFieldMaster set vcGroup='Org.Details Fields' where numFieldId in (40549)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (5,(select MAX(numFieldID) from DycFieldMaster),Null,73,1,0,'Annual Revenue','SelectBox','numAnnualRevID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)





--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (5,Null,'Assigned By','numAssignedBy','numAssignedBy',Null,'DivisionMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')


----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (5,(select MAX(numFieldID) from DycFieldMaster),Null,73,1,0,'Assigned By','SelectBox','numAssignedBy',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


----select * from DycFieldMaster order by numFieldId desc


--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (5,Null,'Assigned To','numAssignedTo','numAssignedTo',Null,'DivisionMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')


----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (5,(select MAX(numFieldID) from DycFieldMaster),Null,73,1,0,'Assigned To','SelectBox','numAssignedTo',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



----exec USP_GetWorkFlowFormFieldMaster 1,68


--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (5,Null,'Campaign','numCampaignID','numCampaignID',Null,'DivisionMaster','N','R','SelectBox',Null,'C',24,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (5,(select MAX(numFieldID) from DycFieldMaster),Null,73,1,0,'Campaign','SelectBox','numCampaignID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (5,Null,'Company Differentiation','numCompanyDiff','numCompanyDiff',Null,'DivisionMaster','N','R','SelectBox',Null,'LI',438,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (5,(select MAX(numFieldID) from DycFieldMaster),Null,70,1,0,'Company Differentiation','SelectBox','numCompanyDiff',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


----Group
--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (5,Null,'Group','numGrpID','numGrpID',Null,'DivisionMaster','N','R','SelectBox',Null,'AG',38,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (5,(select MAX(numFieldID) from DycFieldMaster),Null,73,1,0,'Group','SelectBox','numGrpID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (5,Null,'Follow-up Status','numFollowUpStatus','numFollowUpStatus',Null,'DivisionMaster','N','R','SelectBox',Null,'LI',30,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster order by numFieldId
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (5,(select MAX(numFieldID) from DycFieldMaster),Null,73,1,0,'Follow-up Status','SelectBox','numFollowUpStatus',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (5,Null,'Default Trading Currency','numCurrencyID','numCurrencyID',Null,'DivisionMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (5,(select MAX(numFieldID) from DycFieldMaster),Null,73,1,0,'Default Trading Currency','SelectBox','numCurrencyID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (5,Null,'Territory','numTerID','numTerID',Null,'DivisionMaster','N','R','SelectBox',Null,'T',78,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

----select * from DycFormField_Mapping order by numFieldID desc
----update DycFormField_Mapping set numModuleID=5 where numFieldID=40558

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (5,(select MAX(numFieldID) from DycFieldMaster),Null,73,1,0,'Territory','SelectBox','numTerID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (5,Null,'Organization Phone','vcComPhone','vcComPhone',Null,'DivisionMaster','V','R','TextBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster order by numFieldId desc
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (5,(select MAX(numFieldID) from DycFieldMaster),Null,73,1,0,'Organization Phone','TextBox','vcComPhone',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (5,Null,'Organization Fax','vcComFax','vcComFax',Null,'DivisionMaster','V','R','TextBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (5,(select MAX(numFieldID) from DycFieldMaster),Null,73,1,0,'Organization Fax','TextBox','vcComFax',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (5,Null,'Relationship Type','tintCRMType','tintCRMType',Null,'DivisionMaster','N','R','SelectBox',Null,'SYS',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (5,(select MAX(numFieldID) from DycFieldMaster),Null,73,1,0,'Relationship Type','SelectBox','tintCRMType',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (5,Null,'Organization Status','numStatusID','numStatusID',Null,'DivisionMaster','N','R','SelectBox',Null,'LI',1,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')




--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (5,(select MAX(numFieldID) from DycFieldMaster),Null,73,1,0,'Organization Status','SelectBox','numStatusID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)







--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (5,Null,'Department Name','vcDepartment','vcDepartment',Null,'AdditionalContactsInformation','N','R','SelectBox',Null,'LI',19,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)
--update DycFormField_Mapping set numFormID=73 where numFieldID=40562
--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,73,1,0,'Department Name','SelectBox','vcDepartment',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (5,Null,'Organization Rating','numCompanyRating','numCompanyRating',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',2,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (5,(select MAX(numFieldID) from DycFieldMaster),Null,73,1,0,'Organization Rating','SelectBox','numCompanyRating',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (5,Null,'Lead Source','vcHow','vcHow',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',18,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (5,(select MAX(numFieldID) from DycFieldMaster),Null,73,1,0,'Lead Source','SelectBox','vcHow',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)






--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (5,Null,'Net Days','numBillingDays','numBillingDays',Null,'DivisionMaster','N','R','SelectBox',Null,'BD',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org. | Accounting Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (5,(select MAX(numFieldID) from DycFieldMaster),Null,73,1,0,'Net Days','SelectBox','numBillingDays',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




----Organization Details for Cases

--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (7,Null,'Annual Revenue','numAnnualRevID','numAnnualRevID',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',6,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster order by numFieldId desc

----10542
----update DycFieldMaster set vcGroup='Org.Details Fields' where numFieldId in (40549)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Annual Revenue','SelectBox','numAnnualRevID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)





--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (7,Null,'Assigned By','numAssignedBy','numAssignedBy',Null,'DivisionMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')


----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Assigned By','SelectBox','numAssignedBy',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


----select * from DycFieldMaster order by numFieldId desc


--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (7,Null,'Assigned To','numAssignedTo','numAssignedTo',Null,'DivisionMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')


----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Assigned To','SelectBox','numAssignedTo',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



----exec USP_GetWorkFlowFormFieldMaster 1,68


--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (7,Null,'Campaign','numCampaignID','numCampaignID',Null,'DivisionMaster','N','R','SelectBox',Null,'C',24,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Campaign','SelectBox','numCampaignID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (7,Null,'Company Differentiation','numCompanyDiff','numCompanyDiff',Null,'DivisionMaster','N','R','SelectBox',Null,'LI',438,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Company Differentiation','SelectBox','numCompanyDiff',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


----Group
--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (7,Null,'Group','numGrpID','numGrpID',Null,'DivisionMaster','N','R','SelectBox',Null,'AG',38,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Group','SelectBox','numGrpID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (7,Null,'Follow-up Status','numFollowUpStatus','numFollowUpStatus',Null,'DivisionMaster','N','R','SelectBox',Null,'LI',30,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster order by numFieldId
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Follow-up Status','SelectBox','numFollowUpStatus',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (7,Null,'Default Trading Currency','numCurrencyID','numCurrencyID',Null,'DivisionMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Default Trading Currency','SelectBox','numCurrencyID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (7,Null,'Territory','numTerID','numTerID',Null,'DivisionMaster','N','R','SelectBox',Null,'T',78,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

----select * from DycFieldMaster order by numFieldID desc
----update DycFormField_Mapping set numModuleID=5 where numFieldID=40558

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Territory','SelectBox','numTerID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (7,Null,'Organization Phone','vcComPhone','vcComPhone',Null,'DivisionMaster','V','R','TextBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster order by numFieldId desc
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Organization Phone','TextBox','vcComPhone',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (7,Null,'Organization Fax','vcComFax','vcComFax',Null,'DivisionMaster','V','R','TextBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Organization Fax','TextBox','vcComFax',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (7,Null,'Relationship Type','tintCRMType','tintCRMType',Null,'DivisionMaster','N','R','SelectBox',Null,'SYS',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Relationship Type','SelectBox','tintCRMType',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (7,Null,'Organization Status','numStatusID','numStatusID',Null,'DivisionMaster','N','R','SelectBox',Null,'LI',1,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')




--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Organization Status','SelectBox','numStatusID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)







--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (7,Null,'Department Name','vcDepartment','vcDepartment',Null,'AdditionalContactsInformation','N','R','SelectBox',Null,'LI',19,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)
--update DycFormField_Mapping set numFormID=73 where numFieldID=40562
--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Department Name','SelectBox','vcDepartment',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (7,Null,'Organization Rating','numCompanyRating','numCompanyRating',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',2,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Organization Rating','SelectBox','numCompanyRating',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (7,Null,'Lead Source','vcHow','vcHow',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',18,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Lead Source','SelectBox','vcHow',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)






--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (7,Null,'Net Days','numBillingDays','numBillingDays',Null,'DivisionMaster','N','R','SelectBox',Null,'BD',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org. | Accounting Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Net Days','SelectBox','numBillingDays',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


----Organization Details for Tickker

--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (1,Null,'Annual Revenue','numAnnualRevID','numAnnualRevID',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',6,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')




----10542
----update DycFieldMaster set vcGroup='Org.Details Fields' where numFieldId in (40549)
--select * from DycFieldMaster  order by numFieldId desc
--select * from DycFormField_Mapping  order by numFieldId desc

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'Annual Revenue','SelectBox','numAnnualRevID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)






--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (1,Null,'Assigned By','numAssignedBy','numAssignedBy',Null,'DivisionMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')


----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'Assigned By','SelectBox','numAssignedBy',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


----select * from DycFieldMaster order by numFieldId desc


--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (1,Null,'Assigned To','numAssignedTo','numAssignedTo',Null,'DivisionMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')


----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'Assigned To','SelectBox','numAssignedTo',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



----exec USP_GetWorkFlowFormFieldMaster 1,68


--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (1,Null,'Campaign','numCampaignID','numCampaignID',Null,'DivisionMaster','N','R','SelectBox',Null,'C',24,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'Campaign','SelectBox','numCampaignID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (1,Null,'Company Differentiation','numCompanyDiff','numCompanyDiff',Null,'DivisionMaster','N','R','SelectBox',Null,'LI',438,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'Company Differentiation','SelectBox','numCompanyDiff',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


----Group
--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (1,Null,'Group','numGrpID','numGrpID',Null,'DivisionMaster','N','R','SelectBox',Null,'AG',38,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'Group','SelectBox','numGrpID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (1,Null,'Follow-up Status','numFollowUpStatus','numFollowUpStatus',Null,'DivisionMaster','N','R','SelectBox',Null,'LI',30,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster order by numFieldId
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'Follow-up Status','SelectBox','numFollowUpStatus',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (1,Null,'Default Trading Currency','numCurrencyID','numCurrencyID',Null,'DivisionMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'Default Trading Currency','SelectBox','numCurrencyID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (1,Null,'Territory','numTerID','numTerID',Null,'DivisionMaster','N','R','SelectBox',Null,'T',78,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

----select * from DycFieldMaster order by numFieldID desc
----update DycFormField_Mapping set numModuleID=5 where numFieldID=40558

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'Territory','SelectBox','numTerID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (1,Null,'Organization Phone','vcComPhone','vcComPhone',Null,'DivisionMaster','V','R','TextBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster order by numFieldId desc
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'Organization Phone','TextBox','vcComPhone',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (1,Null,'Organization Fax','vcComFax','vcComFax',Null,'DivisionMaster','V','R','TextBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'Organization Fax','TextBox','vcComFax',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (1,Null,'Relationship Type','tintCRMType','tintCRMType',Null,'DivisionMaster','N','R','SelectBox',Null,'SYS',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'Relationship Type','SelectBox','tintCRMType',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (1,Null,'Organization Status','numStatusID','numStatusID',Null,'DivisionMaster','N','R','SelectBox',Null,'LI',1,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')




--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'Organization Status','SelectBox','numStatusID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)







--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (1,Null,'Department Name','vcDepartment','vcDepartment',Null,'AdditionalContactsInformation','N','R','SelectBox',Null,'LI',19,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)
--update DycFormField_Mapping set numFormID=73 where numFieldID=40562
--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'Department Name','SelectBox','vcDepartment',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (1,Null,'Organization Rating','numCompanyRating','numCompanyRating',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',2,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'Organization Rating','SelectBox','numCompanyRating',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (1,Null,'Lead Source','vcHow','vcHow',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',18,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'Lead Source','SelectBox','vcHow',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)






--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (1,Null,'Net Days','numBillingDays','numBillingDays',Null,'DivisionMaster','N','R','SelectBox',Null,'BD',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org. | Accounting Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'Net Days','SelectBox','numBillingDays',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




----stage Details for Projects

--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (5,Null,'Stage','vcStageName','vcStageName',Null,'StagePercentageDetails','V','R','SelectBox',Null,'SG',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Stage Details fields')



--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (5,(select MAX(numFieldID) from DycFieldMaster),Null,73,1,0,'Stage','SelectBox','vcStageName',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength)
--VALUES        (5,Null,'Stage Progress','tinProgressPercentage','tinProgressPercentage',Null,'StagePercentageDetails','N','R','SelectBox',Null,'SP',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null)


----update DycFieldMaster set  vcGroup='Stage Details fields'  where numFieldId =40601

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (5,(select MAX(numFieldID) from DycFieldMaster),Null,73,1,0,'Stage Progress','SelectBox','tinProgressPercentage',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)






--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (5,Null,'Assigned To','numAssignTo','numAssignTo',Null,'StagePercentageDetails','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Stage Details fields')

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (5,(select MAX(numFieldID) from DycFieldMaster),Null,73,1,0,'Assigned To','SelectBox','numAssignTo',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




----stage Details for Orders

--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Stage','vcStageName','vcStageName',Null,'StagePercentageDetails','V','R','SelectBox',Null,'SG',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Stage Details fields')



--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,70,1,0,'Stage','SelectBox','vcStageName',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Stage Progress','tinProgressPercentage','tinProgressPercentage',Null,'StagePercentageDetails','N','R','SelectBox',Null,'SP',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Stage Details fields')


----update DycFieldMaster set  vcGroup='Stage Details fields'  where numFieldId =40601

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,70,1,0,'Stage Progress','SelectBox','tinProgressPercentage',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)






--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Assigned To','numAssignTo','numAssignTo',Null,'StagePercentageDetails','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Stage Details fields')

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,70,1,0,'Assigned To','SelectBox','numAssignTo',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)





----cases for ORders
--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (7,Null,'Order Status','numStatus','numStatus',Null,'OpportunityMaster','N','R','SelectBox',Null,'LI',176,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Order Status','SelectBox','numStatus',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)


--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (7,Null,'Deal Status','tintOppStatus','tintOppStatus',Null,'OpportunityMaster','N','R','SelectBox',Null,NULL,0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Deal Status','SelectBox','tintOppStatus',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)




--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (7,Null,'Opp Type','tintOppType','tintOppType',Null,'OpportunityMaster','N','R','SelectBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Opp Type','SelectBox','tintOppType',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (7,Null,'Order Sub-total','monDealAmount','monDealAmount',Null,'OpportunityMaster','V','R','TextBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Order Sub-total','TextBox','monDealAmount',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (7,Null,'Assigned To','numAssignedTo','numAssignedTo',Null,'OpportunityMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Assigned To','SelectBox','numAssignedTo',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)


--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (7,Null,'Assigned By','numAssignedBy','numAssignedBy',Null,'OpportunityMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Assigned By','SelectBox','numAssignedBy',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (7,Null,'Comments','txtComments','txtComments',Null,'OpportunityMaster','V','R','TextArea',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Comments','TextArea','txtComments',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)




--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (7,Null,'Net Days','intBillingDays','intBillingDays',Null,'OpportunityMaster','V','R','SelectBox',Null,'BD',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Net Days','SelectBox','intBillingDays',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)


--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (7,Null,'Estimated close date','intpEstimatedCloseDate','intpEstimatedCloseDate',Null,'OpportunityMaster','V','R','DateField',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Estimated close date','DateField','intpEstimatedCloseDate',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)

--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (7,Null,'Closing Date','bintClosedDate','bintClosedDate',Null,'OpportunityMaster','V','R','DateField',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Order Fields')

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Closing Date','DateField','bintClosedDate',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,0,Null,Null,Null,0)






----Organization Details for BiZdocs

--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Annual Revenue','numAnnualRevID','numAnnualRevID',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',6,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')




----10542
----update DycFieldMaster set vcGroup='Org.Details Fields' where numFieldId in (40549)
--select * from DycFieldMaster  order by numFieldId desc
--select * from DycFormField_Mapping  order by numFieldId desc

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Annual Revenue','SelectBox','numAnnualRevID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)






--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Assigned By','numAssignedBy','numAssignedBy',Null,'DivisionMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')


----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Assigned By','SelectBox','numAssignedBy',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


----select * from DycFieldMaster order by numFieldId desc


--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Assigned To','numAssignedTo','numAssignedTo',Null,'DivisionMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')


----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Assigned To','SelectBox','numAssignedTo',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



----exec USP_GetWorkFlowFormFieldMaster 1,68


--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Campaign','numCampaignID','numCampaignID',Null,'DivisionMaster','N','R','SelectBox',Null,'C',24,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Campaign','SelectBox','numCampaignID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Company Differentiation','numCompanyDiff','numCompanyDiff',Null,'DivisionMaster','N','R','SelectBox',Null,'LI',438,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Company Differentiation','SelectBox','numCompanyDiff',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


----Group
--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Group','numGrpID','numGrpID',Null,'DivisionMaster','N','R','SelectBox',Null,'AG',38,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Group','SelectBox','numGrpID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Follow-up Status','numFollowUpStatus','numFollowUpStatus',Null,'DivisionMaster','N','R','SelectBox',Null,'LI',30,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster order by numFieldId
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Follow-up Status','SelectBox','numFollowUpStatus',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Default Trading Currency','numCurrencyID','numCurrencyID',Null,'DivisionMaster','N','R','SelectBox',Null,'U',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Default Trading Currency','SelectBox','numCurrencyID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Territory','numTerID','numTerID',Null,'DivisionMaster','N','R','SelectBox',Null,'T',78,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

----select * from DycFieldMaster order by numFieldID desc
----update DycFormField_Mapping set numModuleID=5 where numFieldID=40558

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Territory','SelectBox','numTerID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Organization Phone','vcComPhone','vcComPhone',Null,'DivisionMaster','V','R','TextBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster order by numFieldId desc
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Organization Phone','TextBox','vcComPhone',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Organization Fax','vcComFax','vcComFax',Null,'DivisionMaster','V','R','TextBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Organization Fax','TextBox','vcComFax',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Relationship Type','tintCRMType','tintCRMType',Null,'DivisionMaster','N','R','SelectBox',Null,'SYS',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
--select * from DycFormField_Mapping order by numFieldID desc
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Relationship Type','SelectBox','tintCRMType',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Organization Status','numStatusID','numStatusID',Null,'DivisionMaster','N','R','SelectBox',Null,'LI',1,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')




--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Organization Status','SelectBox','numStatusID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)







--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Department Name','vcDepartment','vcDepartment',Null,'AdditionalContactsInformation','N','R','SelectBox',Null,'LI',19,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)
----update DycFormField_Mapping set numFormID=73 where numFieldID=40562
--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Department Name','SelectBox','vcDepartment',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Organization Rating','numCompanyRating','numCompanyRating',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',2,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFormField_Mapping order by numFieldID desc
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Organization Rating','SelectBox','numCompanyRating',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Lead Source','vcHow','vcHow',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',18,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org.Details Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Lead Source','SelectBox','vcHow',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)






--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Net Days','numBillingDays','numBillingDays',Null,'DivisionMaster','N','R','SelectBox',Null,'BD',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org. | Accounting Fields')

--select * from DycFieldMaster
----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Net Days','SelectBox','numBillingDays',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


--update DycFormField_Mapping set bitAllowEdit=1 where numFormID=68






--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'BizDoc Template','numBizDocTempID','numBizDocTempID',Null,'OpportunityBizDocs','N','R','SelectBox',Null,'BT',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'BizDoc Fields')

----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'BizDoc Template','SelectBox','numBizDocTempID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)





--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'BizDoc Template','numBizDocTempID','numBizDocTempID',Null,'OpportunityBizDocs','N','R','SelectBox',Null,'BT',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'BizDoc Fields')

----10542
----update DycFieldMaster set vcListItemType='LI' where numFieldId in (10545,10544)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,70,1,0,'BizDoc Template','SelectBox','numBizDocTempID',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)





--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Credit Limit','numCompanyCredit','numCompanyCredit',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',3,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org. | Accounting Fields')

----10542




--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'Credit Limit','SelectBox','numCompanyCredit',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


----orders

----bitOnCreditHold


--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'On Credit Hold','bitOnCreditHold','bitOnCreditHold',Null,'DivisionMaster','Y','R','CheckBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org. | Accounting Fields')

----10542

----update DycFieldMaster set vcFieldDataType='Y' where numFieldID=20548

----select * from DycFormField_Mapping where  numFieldID=20548
----update DycFormField_Mapping set vcAssociatedControlType='CheckBox' where  numFieldID=20548

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,49,1,0,'On Credit Hold','CheckBox','bitOnCreditHold',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)





--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (7,Null,'Credit Limit','numCompanyCredit','numCompanyCredit',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',3,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org. | Accounting Fields')

----10542




--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'Credit Limit','SelectBox','numCompanyCredit',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


----orders

----bitOnCreditHold


--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (7,Null,'On Credit Hold','bitOnCreditHold','bitOnCreditHold',Null,'DivisionMaster','Y','R','CheckBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org. | Accounting Fields')

----10542

----update DycFieldMaster set vcFieldDataType='Y' where numFieldID=20548

----select * from DycFormField_Mapping where  numFieldID=20548
----update DycFormField_Mapping set vcAssociatedControlType='CheckBox' where  numFieldID=20548

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (7,(select MAX(numFieldID) from DycFieldMaster),Null,72,1,0,'On Credit Hold','CheckBox','bitOnCreditHold',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (1,Null,'Credit Limit','numCompanyCredit','numCompanyCredit',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',3,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org. | Accounting Fields')

----10542




--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'Credit Limit','SelectBox','numCompanyCredit',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


----orders

----bitOnCreditHold


--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (1,Null,'On Credit Hold','bitOnCreditHold','bitOnCreditHold',Null,'DivisionMaster','Y','R','CheckBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org. | Accounting Fields')

----10542

----update DycFieldMaster set vcFieldDataType='Y' where numFieldID=20548

----select * from DycFormField_Mapping where  numFieldID=20548
----update DycFormField_Mapping set vcAssociatedControlType='CheckBox' where  numFieldID=20548

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,124,1,0,'On Credit Hold','CheckBox','bitOnCreditHold',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (5,Null,'Credit Limit','numCompanyCredit','numCompanyCredit',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',3,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org. | Accounting Fields')

----10542




--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (5,(select MAX(numFieldID) from DycFieldMaster),Null,73,1,0,'Credit Limit','SelectBox','numCompanyCredit',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


----orders

----bitOnCreditHold


--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (5,Null,'On Credit Hold','bitOnCreditHold','bitOnCreditHold',Null,'DivisionMaster','Y','R','CheckBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org. | Accounting Fields')

----10542

----update DycFieldMaster set vcFieldDataType='Y' where numFieldID=20548

----select * from DycFormField_Mapping where  numFieldID=20548
----update DycFormField_Mapping set vcAssociatedControlType='CheckBox' where  numFieldID=20548

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (5,(select MAX(numFieldID) from DycFieldMaster),Null,73,1,0,'On Credit Hold','CheckBox','bitOnCreditHold',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)



--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (1,Null,'Credit Limit','numCompanyCredit','numCompanyCredit',Null,'CompanyInfo','N','R','SelectBox',Null,'LI',3,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org. | Accounting Fields')

----10542




--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,68,1,0,'Credit Limit','SelectBox','numCompanyCredit',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


----orders

----bitOnCreditHold


--INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (1,Null,'On Credit Hold','bitOnCreditHold','bitOnCreditHold',Null,'DivisionMaster','Y','R','CheckBox',Null,'',0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,'Org. | Accounting Fields')

----10542

----update DycFieldMaster set vcFieldDataType='Y' where numFieldID=20548

----select * from DycFormField_Mapping where  numFieldID=20548
----update DycFormField_Mapping set vcAssociatedControlType='CheckBox' where  numFieldID=20548

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (1,(select MAX(numFieldID) from DycFieldMaster),Null,68,1,0,'On Credit Hold','CheckBox','bitOnCreditHold',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)





----Adding Page to Admin Tree


----BEGIN TRANSACTION
--	DECLARE @numPageNavID numeric 
--	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
--	INSERT INTO dbo.PageNavigationDTL
--			( numPageNavID ,
--			  numModuleID ,
--			  numParentID ,
--			  vcPageNavName ,
--			  vcNavURL ,
--			  vcImageURL ,
--			  bitVisible ,
--			  numTabID
--			)
--	SELECT @numPageNavID ,13,66,'Sales Fulfillment Workflow','../opportunity/frmSalesFulfillmentWorkflowSettings.aspx',NULL,1,-1
	

--	DECLARE CursorTreeNavigation CURSOR FOR
--	SELECT numDomainId FROM Domain WHERE numDomainId <> -255
	
--	OPEN CursorTreeNavigation;

--	DECLARE @numDomainId NUMERIC(18,0);

--	FETCH NEXT FROM CursorTreeNavigation INTO @numDomainId;
--	WHILE (@@FETCH_STATUS = 0)
--	BEGIN
	  
--	   ---- Give permission for each domain to tree node
	   
--   		DELETE FROM TreeNavigationAuthorization WHERE numPageNavID=@numPageNavID AND numDomainID = @numDomainId
	
--		SELECT * INTO #tempData1 FROM
--		(
--		 SELECT    T.numTabID,
--                        CASE WHEN bitFixed = 1
--                             THEN CASE WHEN EXISTS ( SELECT numTabName
--                                                     FROM   TabDefault
--                                                     WHERE  numTabId = T.numTabId
--                                                            AND tintTabType = T.tintTabType AND numDomainID =D.numDomainID)
--                                       THEN ( SELECT TOP 1
--                                                        numTabName
--                                              FROM      TabDefault
--                                              WHERE     numTabId = T.numTabId
--                                                        AND tintTabType = T.tintTabType AND numDomainID = D.numDomainID
--                                            )
--                                       ELSE T.numTabName
--                                  END
--                             ELSE T.numTabName
--                        END numTabname,
--                        vcURL,
--                        bitFixed,
--                        1 AS tintType,
--                        T.vcImage,
--                        D.numDomainID,
--                        A.numGroupID
--              FROM      TabMaster T
--                        CROSS JOIN Domain D --ON D.numDomainID = T.numDomainID
--                        LEFT JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
--              WHERE     bitFixed = 1
--                        AND D.numDomainId <> -255
--                        AND t.tintTabType = 1
              
--              UNION ALL
--              SELECT    -1 AS [numTabID],
--                        'Administration' numTabname,
--                        '' vcURL,
--                        1 bitFixed,
--                        1 AS tintType,
--                        '' AS vcImage,
--                        D.numDomainID,
--                        A.numGroupID
--              FROM      Domain D
--                        JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
--                        WHERE D.numDomainID = @numDomainID
			  
--			  UNION ALL
--			  SELECT    -3 AS [numTabID],
--						'Advanced Search' numTabname,
--						'' vcURL,
--						1 bitFixed,
--						1 AS tintType,
--						'' AS vcImage,
--						D.numDomainID,
--						A.numGroupID
--			  FROM      Domain D
--						JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId          
--            ) TABLE2       		
                        
--		INSERT  INTO dbo.TreeNavigationAuthorization
--            (
--              numGroupID,
--              numTabID,
--              numPageNavID,
--              bitVisible,
--              numDomainID,
--              tintType
--            )
--            SELECT  numGroupID,
--                    PND.numTabID,
--                    numPageNavID,
--                    bitVisible,
--                    numDomainID,
--                    tintType
--            FROM    dbo.PageNavigationDTL PND
--                    JOIN #tempData1 TD ON TD.numTabID = PND.numTabID 
--                    WHERE PND.numPageNavID= @numPageNavID
		
--          DROP TABLE #tempData1
	   
	   

		

		
--	   FETCH NEXT FROM CursorTreeNavigation INTO @numDomainId;
--	END;

--	CLOSE CursorTreeNavigation;
--	DEALLOCATE CursorTreeNavigation;


--	--ROLLbaCk Transaction




--	--PAy Bill


	
----BEGIN TRANSACTION
--	DECLARE @numPageNavID numeric 
--	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
--	INSERT INTO dbo.PageNavigationDTL
--			( numPageNavID ,
--			  numModuleID ,
--			  numParentID ,
--			  vcPageNavName ,
--			  vcNavURL ,
--			  vcImageURL ,
--			  bitVisible ,
--			  numTabID
--			)
--	SELECT @numPageNavID ,13,66,'Pay Bill Workflow','../opportunity/frmPurchaseWorkflowSettings.aspx',NULL,1,-1
	

--	DECLARE CursorTreeNavigation CURSOR FOR
--	SELECT numDomainId FROM Domain WHERE numDomainId <> -255
	
--	OPEN CursorTreeNavigation;

--	DECLARE @numDomainId NUMERIC(18,0);

--	FETCH NEXT FROM CursorTreeNavigation INTO @numDomainId;
--	WHILE (@@FETCH_STATUS = 0)
--	BEGIN
	  
--	   ---- Give permission for each domain to tree node
	   
--   		DELETE FROM TreeNavigationAuthorization WHERE numPageNavID=@numPageNavID AND numDomainID = @numDomainId
	
--		SELECT * INTO #tempData1 FROM
--		(
--		 SELECT    T.numTabID,
--                        CASE WHEN bitFixed = 1
--                             THEN CASE WHEN EXISTS ( SELECT numTabName
--                                                     FROM   TabDefault
--                                                     WHERE  numTabId = T.numTabId
--                                                            AND tintTabType = T.tintTabType AND numDomainID =D.numDomainID)
--                                       THEN ( SELECT TOP 1
--                                                        numTabName
--                                              FROM      TabDefault
--                                              WHERE     numTabId = T.numTabId
--                                                        AND tintTabType = T.tintTabType AND numDomainID = D.numDomainID
--                                            )
--                                       ELSE T.numTabName
--                                  END
--                             ELSE T.numTabName
--                        END numTabname,
--                        vcURL,
--                        bitFixed,
--                        1 AS tintType,
--                        T.vcImage,
--                        D.numDomainID,
--                        A.numGroupID
--              FROM      TabMaster T
--                        CROSS JOIN Domain D --ON D.numDomainID = T.numDomainID
--                        LEFT JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
--              WHERE     bitFixed = 1
--                        AND D.numDomainId <> -255
--                        AND t.tintTabType = 1
              
--              UNION ALL
--              SELECT    -1 AS [numTabID],
--                        'Administration' numTabname,
--                        '' vcURL,
--                        1 bitFixed,
--                        1 AS tintType,
--                        '' AS vcImage,
--                        D.numDomainID,
--                        A.numGroupID
--              FROM      Domain D
--                        JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
--                        WHERE D.numDomainID = @numDomainID
			  
--			  UNION ALL
--			  SELECT    -3 AS [numTabID],
--						'Advanced Search' numTabname,
--						'' vcURL,
--						1 bitFixed,
--						1 AS tintType,
--						'' AS vcImage,
--						D.numDomainID,
--						A.numGroupID
--			  FROM      Domain D
--						JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId          
--            ) TABLE2       		
                        
--		INSERT  INTO dbo.TreeNavigationAuthorization
--            (
--              numGroupID,
--              numTabID,
--              numPageNavID,
--              bitVisible,
--              numDomainID,
--              tintType
--            )
--            SELECT  numGroupID,
--                    PND.numTabID,
--                    numPageNavID,
--                    bitVisible,
--                    numDomainID,
--                    tintType
--            FROM    dbo.PageNavigationDTL PND
--                    JOIN #tempData1 TD ON TD.numTabID = PND.numTabID 
--                    WHERE PND.numPageNavID= @numPageNavID
		
--          DROP TABLE #tempData1
	   
	   

		

		
--	   FETCH NEXT FROM CursorTreeNavigation INTO @numDomainId;
--	END;

--	CLOSE CursorTreeNavigation;
--	DEALLOCATE CursorTreeNavigation;


--	--ROLLbaCk Transaction

--	DECLARE @numPageNavID numeric 
--	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
--	INSERT INTO dbo.PageNavigationDTL
--			( numPageNavID ,
--			  numModuleID ,
--			  numParentID ,
--			  vcPageNavName ,
--			  vcNavURL ,
--			  vcImageURL ,
--			  bitVisible ,
--			  numTabID,
--			  intSortOrder
--			)
--	SELECT @numPageNavID ,13,73,'Shipping',NULL,NULL,1,-1,4
	

--	DECLARE CursorTreeNavigation CURSOR FOR
--	SELECT numDomainId FROM Domain WHERE numDomainId <> -255
	
--	OPEN CursorTreeNavigation;

--	DECLARE @numDomainId NUMERIC(18,0);

--	FETCH NEXT FROM CursorTreeNavigation INTO @numDomainId;
--	WHILE (@@FETCH_STATUS = 0)
--	BEGIN
	  
--	   ---- Give permission for each domain to tree node
	   
--   		DELETE FROM TreeNavigationAuthorization WHERE numPageNavID=@numPageNavID AND numDomainID = @numDomainId
	
--		SELECT * INTO #tempData1 FROM
--		(
--		 SELECT    T.numTabID,
--                        CASE WHEN bitFixed = 1
--                             THEN CASE WHEN EXISTS ( SELECT numTabName
--                                                     FROM   TabDefault
--                                                     WHERE  numTabId = T.numTabId
--                                                            AND tintTabType = T.tintTabType AND numDomainID =D.numDomainID)
--                                       THEN ( SELECT TOP 1
--                                                        numTabName
--                                              FROM      TabDefault
--                                              WHERE     numTabId = T.numTabId
--                                                        AND tintTabType = T.tintTabType AND numDomainID = D.numDomainID
--                                            )
--                                       ELSE T.numTabName
--                                  END
--                             ELSE T.numTabName
--                        END numTabname,
--                        vcURL,
--                        bitFixed,
--                        1 AS tintType,
--                        T.vcImage,
--                        D.numDomainID,
--                        A.numGroupID
--              FROM      TabMaster T
--                        CROSS JOIN Domain D --ON D.numDomainID = T.numDomainID
--                        LEFT JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
--              WHERE     bitFixed = 1
--                        AND D.numDomainId <> -255
--                        AND t.tintTabType = 1
              
--              UNION ALL
--              SELECT    -1 AS [numTabID],
--                        'Administration' numTabname,
--                        '' vcURL,
--                        1 bitFixed,
--                        1 AS tintType,
--                        '' AS vcImage,
--                        D.numDomainID,
--                        A.numGroupID
--              FROM      Domain D
--                        JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
--                        WHERE D.numDomainID = @numDomainID
			  
--			  UNION ALL
--			  SELECT    -3 AS [numTabID],
--						'Advanced Search' numTabname,
--						'' vcURL,
--						1 bitFixed,
--						1 AS tintType,
--						'' AS vcImage,
--						D.numDomainID,
--						A.numGroupID
--			  FROM      Domain D
--						JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId          
--            ) TABLE2       		
                        
--		INSERT  INTO dbo.TreeNavigationAuthorization
--            (
--              numGroupID,
--              numTabID,
--              numPageNavID,
--              bitVisible,
--              numDomainID,
--              tintType
--            )
--            SELECT  numGroupID,
--                    PND.numTabID,
--                    numPageNavID,
--                    bitVisible,
--                    numDomainID,
--                    tintType
--            FROM    dbo.PageNavigationDTL PND
--                    JOIN #tempData1 TD ON TD.numTabID = PND.numTabID 
--                    WHERE PND.numPageNavID= @numPageNavID
		
--          DROP TABLE #tempData1
	   
	   

		

		
--	   FETCH NEXT FROM CursorTreeNavigation INTO @numDomainId;
--	END;

--	CLOSE CursorTreeNavigation;
--	DEALLOCATE CursorTreeNavigation;


	--ROLLbaCk Transaction
	--select * from DycFieldMaster where vcFieldName='Rental Status'

--	INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Rental Status','bitAsset','bitAsset',Null,'Item','N','R','',Null,Null,0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,Null)

----10542



--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,26,1,0,'Rental Status','','bitAsset',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)

--Update DycFieldMaster set bitAllowFiltering=1,vcFieldDataType='T',bitAllowEdit=1,bitDefault=1,bitSettingField=1  where numFieldId=80549
--Update DycFormField_Mapping set bitInlineEdit=1,bitAllowEdit=1,bitDefault=1,bitSettingField=1 ,bitAllowFiltering=1,numFormFieldID=1382 where numFieldId=80549



--Update PageNavigationDTL set vcNavURL='../Items/frmItemList.aspx?Page=Asset Items&ItemGroup=0' where numPageNavID=171 and vcPageNavName='Asset Items'
----select * from ShortCutBar where id=150
--update ShortCutBar set  Link='../Items/frmItemList.aspx?Page=Asset Items&ItemGroup=0' where id=150

--ALTER TABLE Item ADD
--bitAsset BIT,
--bitRental BIT


--ALTER TABLE dbo.OpportunityItems ADD
--	numRentalIN numeric(18,0) NULL,
--	numRentalOut numeric(18,0) NULL,
--	numRentalLost numeric(18,0) NULL





--	INSERT INTO DycFieldMaster
--                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
--                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
--                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
--                         intColumnWidth, intFieldMaxLength,vcGroup)
--VALUES        (3,Null,'Allocation','numAllocation','numAllocation',Null,'WareHouseItems','V','R','',Null,Null,0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,Null)


CFw_Grp_Master


--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,26,1,0,'Allocation','','numAllocation',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)




--Update DycFieldMaster set bitAllowFiltering=1,vcFieldDataType='T',bitAllowEdit=1,bitDefault=1,bitSettingField=1  where numFieldId=90549
--Update DycFormField_Mapping set bitInlineEdit=1,bitAllowEdit=1,bitDefault=1,bitSettingField=1 ,bitAllowFiltering=1,numFormFieldID=1382 where numFieldId=90549





--new



--exec USP_GetCustomFieldsTab @LocID=3,@numDomainID=1,@Type=2,@numGroupID=1

--select * from [GroupTabDetails] where numGroupId=2 and numTabId in (109,6473)

--INSERT INTO CFw_Grp_Master
--                      (Grp_Name, Loc_Id, numDomainID, tintType, vcURLF)
--VALUES     ('Items Affected',3,1,2,NULL)
--select * from CFw_Grp_Master where Grp_id in (109,110,6473)

--update CFw_Grp_Master set Grp_id
--update [GroupTabDetails] set tintType=1,numGroupId=1 where numTabId=6473
	
--SELECT TOP 1 [bitallowed] FROM [GroupTabDetails] GTD WHERE ISNULL(GTD.tintType,0) = 1 AND GTD.[numGroupId] = 2 AND GTD.numTabID = 6473
----Insert into GroupTabDetails(numGroupId,numTabId,numRelationShip,bitallowed,numOrder,numProfileID,tintType) Values(2,6473,0,1,4,Null,Null)





----CFW_FLD_Values_Case
----SPs -- USP_ManageWorkFlowQueueCF
----Sps--USP_CFW_FLD_Values_CT
----select * from CFW_FLD_Values where recid=346810

--select FldDTLID,Fld_ID,Fld_Value,RecId,* from CFW_FLD_Values where FldDTLID=812451


--select * from CFW_FLD_Values where RecId= 356806 
	

--select * from WorkFlowTriggerFieldList
--select * from WorkFlowQueue

--exec USP_CFW_FLD_Values_CT  1,1,356806,880


 


 


ALTER TABLE dbo.WorkFlowMaster ADD
	bitCustom bit NULL

ALTER TABLE dbo.WorkFlowMaster ADD
	numFieldID numeric(18,0) NULL

ALTER TABLE dbo.CFW_FLD_Values
ENABLE CHANGE_TRACKING 
WITH (TRACK_COLUMNS_UPDATED = ON);

ALTER TABLE dbo.CFW_Fld_Values_Opp
ENABLE CHANGE_TRACKING 
WITH (TRACK_COLUMNS_UPDATED = ON);


ALTER TABLE dbo.CFW_FLD_Values_Pro
ENABLE CHANGE_TRACKING 
WITH (TRACK_COLUMNS_UPDATED = ON);

ALTER TABLE dbo.CFW_FLD_Values_Case
ENABLE CHANGE_TRACKING 
WITH (TRACK_COLUMNS_UPDATED = ON);

ALTER TABLE dbo.CFW_FLD_Values_Cont
ENABLE CHANGE_TRACKING 
WITH (TRACK_COLUMNS_UPDATED = ON);
