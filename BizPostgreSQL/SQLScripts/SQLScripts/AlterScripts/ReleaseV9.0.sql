/******************************************************************
Project: Release 9.0 Date: 31.JANUARY.2018
Comments: ALTER SCRIPTS
*******************************************************************/

/*************************   NEELAM  *****************************/


USE [Production.2014]
GO

/****** Object:  Table [dbo].[ApprovalProcessItemsClassification]    Script Date: 31-01-2018 11:56:37 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalProcessItemsClassification](
	[numApprovalRuleID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numListItemID] [numeric](18, 0)  NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numAbovePercent] [numeric](18, 2) NULL,
	[numBelowPercent] [numeric](18, 2) NULL,
	[numBelowPriceField] [numeric](18, 0) NULL,
	[bitCostApproval] [bit] NULL,
	[bitListPriceApproval] [bit] NULL,
	[bitMarginPriceViolated] [bit] NULL,
 CONSTRAINT [PK_ApprovalProcessItemsClassification] PRIMARY KEY CLUSTERED 
(
	[numApprovalRuleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ApprovalProcessItemsClassification]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalProcessItemsClassification_ApprovalProcessItemsClassification] FOREIGN KEY([numListItemID])
REFERENCES [dbo].[ListDetails] ([numListItemID])
GO

ALTER TABLE [dbo].[ApprovalProcessItemsClassification] CHECK CONSTRAINT [FK_ApprovalProcessItemsClassification_ApprovalProcessItemsClassification]
GO

ALTER TABLE [dbo].[ApprovalProcessItemsClassification]  WITH CHECK ADD  CONSTRAINT [FK_ApprovalProcessItemsClassification_Domain] FOREIGN KEY([numDomainID])
REFERENCES [dbo].[Domain] ([numDomainId])
GO

ALTER TABLE [dbo].[ApprovalProcessItemsClassification] CHECK CONSTRAINT [FK_ApprovalProcessItemsClassification_Domain]
GO


INSERT INTO ApprovalProcessItemsClassification (numDomainID, numAbovePercent, numBelowPercent, 
 bitCostApproval, bitListPriceApproval, bitMarginPriceViolated)
SELECT numDomainID, numAbovePercent, numBelowPercent,  bitCostApproval, bitListPriceApproval, bitMarginPriceViolated
FROM Domain

ALTER TABLE Domain DROP COLUMN [numAbovePercent]
ALTER TABLE Domain DROP COLUMN [numBelowPercent]
ALTER TABLE Domain DROP COLUMN [numBelowPriceField]
ALTER TABLE Domain DROP COLUMN [bitCostApproval]
ALTER TABLE Domain DROP COLUMN [bitListPriceApproval]
ALTER TABLE Domain DROP COLUMN [bitMarginPriceViolated]