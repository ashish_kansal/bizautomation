/******************************************************************
Project: Release 13.3 Date: 02.APR.2020
Comments: STORE PROCEDURES
*******************************************************************/

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetWorkOrderStatus')
DROP FUNCTION GetWorkOrderStatus
GO
CREATE FUNCTION [dbo].[GetWorkOrderStatus] 
(
	@numDomainID NUMERIC(18,0)
    ,@tintCommitAllocation TINYINT
	,@numOppID NUMERIC(18,0)
	,@numOppItemID NUMERIC(18,0)
)
RETURNS VARCHAR(MAX)
AS 
BEGIN
	DECLARE @vcWoStatus VARCHAR(500) = ''

	IF (SELECT COUNT(*) FROM WorkOrder WHERE numOppId=@numOppID AND numOppItemID=@numOppItemID) > 0
	BEGIN
		SET @vcWoStatus = CONCAT('<a href="#" onClick="return OpenWorkOrder(',(SELECT numWOId FROM WorkOrder WHERE numOppId=@numOppID AND numOppItemID=@numOppItemID AND ISNULL(numParentWOID,0)=0),');"><img src="../images/Icon/workorder.png" /></a> ')

		DECLARE @TEMP TABLE
		(
			ItemLevel INT
			,numParentWOID NUMERIC(18,0)
			,numWOID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numQty FLOAT
			,numWarehouseItemID NUMERIC(18,0)
			,bitWorkOrder BIT
			,tintBuildStatus TINYINT
		)

		DECLARE @TEMPWO TABLE
		(
			ID INT IDENTITY(1,1)
			,numWOID NUMERIC(18,0)
		)

		;WITH CTEWorkOrder (ItemLevel,numParentWOID,numWOID,numItemKitID,numQtyItemsReq,numWarehouseItemID,bitWorkOrder,tintBuildStatus) AS
		(
			SELECT
				1,
				numParentWOID,
				numWOId,
				numItemCode,
				CAST(numQtyItemsReq AS FLOAT),
				numWareHouseItemId,
				1 AS bitWorkOrder,
				(CASE WHEN numWOStatus = 23184 THEN 2 ELSE 0 END) AS tintBuildStatus
			FROM
				WorkOrder
			WHERE
				numDomainID=@numDomainID
				AND numOppId=@numOppID 
				AND numOppItemID=@numOppItemID
			UNION ALL
			SELECT 
				c.ItemLevel+2,
				c.numWOID,
				WorkOrder.numWOId,
				WorkOrder.numItemCode,
				CAST(WorkOrder.numQtyItemsReq AS FLOAT),
				WorkOrder.numWareHouseItemId,
				1 AS bitWorkOrder,
				(CASE WHEN numWOStatus = 23184 THEN 2 ELSE 0 END) AS tintBuildStatus
			FROM 
				WorkOrder
			INNER JOIN 
				CTEWorkOrder c 
			ON 
				WorkOrder.numParentWOID = c.numWOID
		)


		INSERT 
			@TEMP
		SELECT
			ItemLevel,
			numParentWOID,
			numWOID,
			numItemCode,
			CTEWorkOrder.numQtyItemsReq,
			CTEWorkOrder.numWarehouseItemID,
			bitWorkOrder,
			tintBuildStatus
		FROM 
			CTEWorkOrder
		INNER JOIN 
			Item
		ON
			CTEWorkOrder.numItemKitID = Item.numItemCode
		LEFT JOIN
			WareHouseItems
		ON
			CTEWorkOrder.numWarehouseItemID = WareHouseItems.numWareHouseItemID

		IF EXISTS (SELECT numWOID FROM @TEMP WHERE tintBuildStatus <> 2)
		BEGIN
			INSERT INTO 
				@TEMP
			SELECT
				t1.ItemLevel + 1,
				t1.numWOID,
				NULL,
				WorkOrderDetails.numChildItemID,
				WorkOrderDetails.numQtyItemsReq,
				WorkOrderDetails.numWarehouseItemID,
				0 AS bitWorkOrder,
				(CASE 
					WHEN t1.tintBuildStatus = 2 
					THEN 2
					ELSE
						(CASE 
							WHEN Item.charItemType='P'
							THEN
								CASE 
									WHEN @tintCommitAllocation=2  
									THEN (CASE WHEN ISNULL(numQtyItemsReq,0) >= WareHouseItems.numOnHand THEN 0 ELSE 1 END)
									ELSE (CASE WHEN ISNULL(numQtyItemsReq,0) >= WareHouseItems.numAllocation THEN 0 ELSE 1 END) 
								END
							ELSE 1
						END)
				END)
			FROM
				WorkOrderDetails
			INNER JOIN
				@TEMP t1
			ON
				WorkOrderDetails.numWOId = t1.numWOID
			INNER JOIN 
				Item
			ON
				WorkOrderDetails.numChildItemID = Item.numItemCode
				AND 1= (CASE WHEN (SELECT COUNT(*) FROM @TEMP TInner WHERE TInner.bitWorkOrder=1 AND TInner.numParentWOID=t1.numWOID AND TInner.numItemCode=Item.numItemCode) > 0 THEN 0 ELSE 1 END)
			LEFT JOIN
				WareHouseItems
			ON
				WorkOrderDetails.numWarehouseItemID = WareHouseItems.numWareHouseItemID


			DECLARE @i AS INT = 1
			DECLARE @iCount AS INT
			DECLARE @numTempWOID NUMERIC(18,0)
			INSERT INTO @TEMPWO (numWOID) SELECT numWOID FROM @TEMP WHERE bitWorkOrder=1 ORDER BY ItemLevel DESC

			SELECT @iCount=COUNT(*) FROM @TEMPWO

			WHILE @i <= @iCount
			BEGIN
				SELECT @numTempWOID=numWOID FROM @TEMPWO WHERE ID=@i

				UPDATE
					T1
				SET
					tintBuildStatus = (CASE WHEN (SELECT COUNT(*) FROM @TEMP TInner WHERE TInner.numParentWOID=T1.numWOID AND tintBuildStatus=0) > 0 THEN 0 ELSE 1 END)
				FROM
					@TEMP T1
				WHERE
					numWOID=@numTempWOID
					AND T1.tintBuildStatus <> 2

				SET @i = @i + 1
			END

			IF (SELECT COUNT(*) FROM @TEMP WHERE bitWorkOrder=1 AND tintBuildStatus=2) = (SELECT COUNT(*) FROM @TEMP WHERE bitWorkOrder=1)
			BEGIN
				SET @vcWoStatus = CONCAT(@vcWoStatus,'Build completed')
			END
			ELSE IF (SELECT COUNT(*) FROM @TEMP WHERE tintBuildStatus=0) > 0
			BEGIN
				SET @vcWoStatus = CONCAT(@vcWoStatus,'Not ready to build')
			END
			ELSE
			BEGIN
				SET @vcWoStatus = CONCAT(@vcWoStatus,'Ready to build')
			END
		END
		ELSE
		BEGIN
			SET @vcWoStatus = CONCAT(@vcWoStatus,'Build completed')
		END
	END

	RETURN @vcWoStatus
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='tf'AND NAME ='GetWOStatus')
DROP FUNCTION GetWOStatus
GO
CREATE FUNCTION [dbo].[GetWOStatus] 
(
	@numDomainID NUMERIC(18,0)
	,@vcWorkOrderIds VARCHAR(1000)
)
RETURNS @TEMPWorkOrder TABLE
(
	ID INT IDENTITY(1,1)
	,numWOID NUMERIC(18,0)
	,numItemCode NUMERIC(18,0)
	,numQtyToBuild FLOAT
	,numWarehouseID NUMERIC(18,0)
	,numWorkOrderStatus TINYINT
	,vcWorkOrderStatus VARCHAR(300)
)

AS
BEGIN
	INSERT INTO @TEMPWorkOrder
	(
		numWOID
		,numItemCode
		,numQtyToBuild
		,numWarehouseID 
		,numWorkOrderStatus
		,vcWorkOrderStatus
	)
	SELECT
		WorkOrder.numWOId
		,WorkOrder.numItemCode
		,ISNULL(WorkOrder.numQtyItemsReq,0)
		,WareHouseItems.numWareHouseID
		,(CASE WHEN WorkOrder.numWOStatus = 23184 THEN 0 ELSE NULL END)
		,(CASE WHEN WorkOrder.numWOStatus = 23184 THEN '<span style="color:#00b050">Build Completed</span>' ELSE NULL END)
	FROM
		WorkOrder
	INNER JOIN
		WareHouseItems
	ON
		WorkOrder.numWareHouseItemId = WareHouseItems.numWareHouseItemID
	LEFT JOIN
		OpportunityItems
	ON
		WorkOrder.numOppItemID = OpportunityItems.numoppitemtCode
	WHERE
		WorkOrder.numDomainID = @numDomainID
	ORDER BY
		ISNULL(WorkOrder.dtmEndDate,OpportunityItems.ItemReleaseDate) ASC
		,WorkOrder.dtmStartDate ASC
		,WorkOrder.bintCreatedDate ASC
		,ISNULL(WorkOrder.numParentWOID,0) DESC

	DECLARE @WorkOrderItems TABLE
	(
		numItemCode NUMERIC(18,0)
		,numWarehouseID NUMERIC(18,0)		
		,numAvailable FLOAT
	)

	INSERT INTO @WorkOrderItems
	(
		numItemCode
		,numWarehouseID
		,numAvailable
	)
	SELECT
		TEMP.numItemCode
		,WI.numWareHouseID
		,SUM(WI.numOnHand) + SUM(WI.numAllocation)
	FROM
	(
		SELECT
			I.numItemCode
			,TWO.numWareHouseID
		FROM
			@TEMPWorkOrder TWO
		INNER JOIN
			WorkOrderDetails WOD
		ON
			TWO.numWOID = WOD.numWOId
		INNER JOIN
			Item I
		ON
			WOD.numChildItemID = I.numItemCode
			AND ISNULL(WOD.numQtyItemsReq_Orig,0) > 0
		GROUP BY
			I.numItemCode
			,TWO.numWareHouseID
	) TEMP
	INNER JOIN
		WareHouseItems WI
	ON
		TEMP.numItemCode = WI.numItemID
		AND TEMP.numWarehouseID = WI.numWareHouseID
	GROUP BY
		TEMP.numItemCode
		,WI.numWareHouseID
		

	DECLARE @i INT = 1
	DECLARE @iCount INT
	SELECT @iCount = COUNT(*) FROM @TEMPWorkOrder

	DECLARE @numWOID NUMERIC(18,0)
	DECLARE @numQtyToBuild FLOAT
	DECLARE @numQtyCanBeBuild FLOAT
	DECLARE @numQtyBuildable FLOAT
	DECLARE @numWarehouseID NUMERIC(18,0)
	DECLARE @vcWorkOrderStatus VARCHAR(100)

	WHILE @i <= @iCount
	BEGIN
		SELECT 
			@numWOID=numWOID
			,@numQtyToBuild=numQtyToBuild
			,@numWarehouseID=numWarehouseID
			,@vcWorkOrderStatus=vcWorkOrderStatus
		FROM 
			@TEMPWorkOrder 
		WHERE 
			ID = @i

		IF @vcWorkOrderStatus IS NULL
		BEGIN
			SET @numQtyCanBeBuild = ISNULL((SELECT 
											MIN(numQtyCanBeBuild)
										FROM
										(
											SELECT
												I.numItemCode
												,WOD.numQtyItemsReq
												,WOD.numQtyItemsReq_Orig
												,WOI.numAvailable
												,(CASE 
													WHEN ISNULL(WOI.numAvailable,0) >= (ISNULL(WOD.numQtyItemsReq,0) - ISNULL((SELECT SUM(numPickedQty) FROM WorkOrderPickedItems WOPI WHERE WOPI.numWODetailId=WOD.numWODetailId),0))
													THEN @numQtyToBuild - FLOOR(ISNULL((SELECT SUM(numPickedQty) FROM WorkOrderPickedItems WOPI WHERE WOPI.numWODetailId=WOD.numWODetailId),0)/WOD.numQtyItemsReq_Orig)
													WHEN ISNULL(WOI.numAvailable,0) > 0 
													THEN FLOOR(ISNULL(WOI.numAvailable,0)/WOD.numQtyItemsReq_Orig)
													ELSE 0
												END) numQtyCanBeBuild
											FROM
												WorkOrderDetails WOD
											INNER JOIN
												Item I
											ON
												WOD.numChildItemID = I.numItemCode
											INNER JOIN
												@WorkOrderItems WOI
											ON
												I.numItemCode = WOI.numItemCode
												AND WOI.numWarehouseID = @numWarehouseID
											WHERE
												numWOId = @numWOID
												AND ISNULL(WOD.numWareHouseItemId,0) > 0
												AND ISNULL(WOD.numQtyItemsReq_Orig,0) > 0
										) TEMP1),0)

			SET @numQtyBuildable = ISNULL((SELECT 
												MIN(numQtyBuildable)
											FROM
											(
												SELECT
													FLOOR(SUM(WOPI.numPickedQty)/WOD.numQtyItemsReq_Orig) numQtyBuildable
												FROM
													WorkOrderDetails WOD
												INNER JOIN
													WorkOrderPickedItems WOPI
												ON
													WOD.numWODetailId = WOPI.numWODetailId
												WHERE
													numWOId = @numWOID
													AND ISNULL(WOD.numWareHouseItemId,0) > 0
													AND ISNULL(WOD.numQtyItemsReq_Orig,0) > 0
												GROUP BY
													WOD.numWODetailId
													,WOD.numQtyItemsReq_Orig
											) TEMP1),0)

			UPDATE
				@TEMPWorkOrder
			SET 
				numWorkOrderStatus = (CASE 
										WHEN @numQtyBuildable = @numQtyToBuild
										THEN 3
										WHEN @numQtyCanBeBuild >= @numQtyToBuild 
										THEN 1 
										WHEN @numQtyCanBeBuild > 0
										THEN 2
										ELSE 0
									END)
				,vcWorkOrderStatus = (CASE 
										WHEN @numQtyBuildable = @numQtyToBuild
										THEN '<span style="color:#00b050">Fully Buildable</span>'
										WHEN @numQtyCanBeBuild >= @numQtyToBuild AND @numQtyBuildable = 0
										THEN '<span style="color:#7030a0">Fully Pickable</span>' 
										ELSE
										CONCAT((CASE WHEN @numQtyCanBeBuild > 0 THEN CONCAT('<span style="color:#7030a0">Pickable(',@numQtyCanBeBuild,')</span>')  ELSE '' END)
										,(CASE WHEN @numQtyBuildable > 0 THEN CONCAT(' <span style="color:#00b050">Buildable(',@numQtyBuildable,')</span>') ELSE '' END)
										,(CASE WHEN (@numQtyToBuild - ISNULL((SELECT
																					MIN(numPickedQty)
																				FROM
																				(
																					SELECT 
																						WODInner.numWODetailId
																						,FLOOR(SUM(WOPI.numPickedQty)/WODInner.numQtyItemsReq_Orig) numPickedQty
																					FROM
																						WorkOrderDetails WODInner
																					LEFT JOIN
																						WorkOrderPickedItems WOPI
																					ON
																						WODInner.numWODetailId=WOPI.numWODetailId
																					WHERE
																						WODInner.numWOId=@numWOID
																						AND ISNULL(WODInner.numWareHouseItemId,0) > 0
																						AND ISNULL(WODInner.numQtyItemsReq_Orig,0) > 0
																					GROUP BY
																						WODInner.numWODetailId
																						,WODInner.numQtyItemsReq_Orig
																				) TEMP),0) -@numQtyCanBeBuild) > 0 THEN CONCAT(' <span style="color:#ff0000">BO(',@numQtyToBuild -@numQtyCanBeBuild,')</span>') ELSE '' END))
									END)
			WHERE
				numWOID = @numWOID


			UPDATE
				WOI
			SET
				WOI.numAvailable = ISNULL(WOI.numAvailable,0) - ISNULL(WOD.numQtyItemsReq,0)
			FROM
				WorkOrderDetails WOD
			INNER JOIN
				Item I
			ON
				WOD.numChildItemID = I.numItemCode
			LEFT JOIN
				@WorkOrderItems WOI
			ON
				I.numItemCode = WOI.numItemCode
				AND WOI.numWarehouseID = @numWarehouseID
				AND ISNULL(WOD.numQtyItemsReq_Orig,0) > 0
			WHERE
				numWOId = @numWOID

		END

		SET @i = @i + 1
	END

	RETURN
END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ApproveOpportunityItemsPrice' ) 
    DROP PROCEDURE USP_ApproveOpportunityItemsPrice
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 14 July 2014
-- Description:	sets unit price approval required flag to false for all items added in order
-- =============================================
CREATE PROCEDURE USP_ApproveOpportunityItemsPrice
	@numOppId numeric(18,0),
	@numDomainID numeric(18,0),
	@ApprovalType AS INT = 0
AS
BEGIN
	--	 SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--SET NOCOUNT ON;
	IF(@ApprovalType=2)
	BEGIN
		EXEC USP_ItemUnitPriceApprovalUpdateConfigurePrice @numOppId=@numOppId,@numDomainID=@numDomainID
	END


	UPDATE
		OpportunityMaster
	SET
		tintOppStatus=CASE WHEN bitIsInitalSalesOrder=1 THEN 1 ELSE tintOppStatus END
	WHERE
		numOppId = @numOppId
	
	UPDATE
		OpportunityItems
	SET
		bitItemPriceApprovalRequired = 0
	WHERE
		numOppId = @numOppId
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_BizRecurrence_WorkOrder')
DROP PROCEDURE USP_BizRecurrence_WorkOrder
GO
  
Create PROCEDURE [dbo].[USP_BizRecurrence_WorkOrder]   
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numOppID NUMERIC(18,0),
	@numParentOppID NUMERIC(18,0)
AS  
BEGIN  

IF (SELECT COUNT(*) FROM WorkOrder WHERE numOppId=@numOppID) = 0 AND
	(SELECT COUNT(*) FROM WorkOrder WHERE numOppId=@numParentOppID) > 0
BEGIN
   
	DECLARE @numItemCode AS numeric(9),@numUnitHour AS FLOAT,@numWarehouseItmsID AS numeric(9),@vcItemDesc AS VARCHAR(1000), @numWarehouseID NUMERIC(18,0)
  
	DECLARE @numWOId AS NUMERIC(9),@vcInstruction AS VARCHAR(1000),@bintCompliationDate AS DATETIME,@numWOAssignedTo NUMERIC(9)

	DECLARE @numoppitemtCode NUMERIC(18,0)

	DECLARE @TEMPitem TABLE
	(
		numItemKitID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numWarehouseItmsID NUMERIC(18,0)
		,numCalculatedQty FLOAT
		,txtItemDesc VARCHAR(500)
		,sintOrder INT
		,numUOMId NUMERIC(18,0)
		,numQtyItemsReq FLOAT
		,charItemType CHAR
	)


	DECLARE WOCursor CURSOR FOR 
		SELECT 
			X.numoppitemtCode,
			X.numItemCode,
			dbo.fn_UOMConversion(X.numUOMId,numItemCode,@numDomainId,null) * x.numUnitHour AS numUnitHour,
			X.numWarehouseItmsID,
			X.vcItemDesc,
			X.vcInstruction,
			X.bintCompliationDate,
			X.numAssignedTo
		FROM
			(
				SELECT
					OpportunityItems.numoppitemtCode,
					OpportunityItems.numItemCode,
					OpportunityItems.bitWorkOrder,
					OpportunityItems.numUOMId,
					OpportunityItems.numUnitHour,
					OpportunityItems.numWarehouseItmsID,
					OpportunityItems.vcItemDesc,
					WorkOrder.vcInstruction,
					WorkOrder.bintCompliationDate,
					WorkOrder.numAssignedTo
				FROM
					OpportunityItems
				INNER JOIN	
					WorkOrder
				ON
					WorkOrder.numItemCode = OpportunityItems.numItemCode AND
					WorkOrder.numWareHouseItemId = OpportunityItems.numWarehouseItmsID AND
					WorkOrder.numOppId = OpportunityItems.numOppId
				WHERE
					OpportunityItems.numOppId = @numParentOppID
			)X  
		WHERE 
			X.bitWorkOrder=1

	OPEN WOCursor;

	FETCH NEXT FROM WOCursor INTO @numoppitemtCode,@numItemCode, @numUnitHour,@numWarehouseItmsID,@vcItemDesc,@vcInstruction,@bintCompliationDate,@numWOAssignedTo

	WHILE @@FETCH_STATUS = 0
	BEGIN
		DELETE FROM @TEMPitem
		SET @numWarehouseID = ISNULL((SELECT numWareHouseID FROM WarehouseItems WHERE numWarehouseitemID=@numWarehouseItmsID),0)

		;WITH CTE(numItemKitID,numItemCode,numCalculatedQty,txtItemDesc,sintOrder,numUOMId,numQtyItemsReq,charItemType)
		AS
		(
			select CAST(@numItemCode AS NUMERIC(9)),numItemCode
			,CAST(((DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(Dtl.numUOMID,item.numItemCode,item.numDomainID,item.numBaseUnit),1)) * @numUnitHour)AS FLOAT) AS numCalculatedQty,ISNULL(Dtl.vcItemDesc,txtItemDesc) AS txtItemDesc,ISNULL(sintOrder,0) AS sintOrder,
			Dtl.numUOMId,DTL.numQtyItemsReq,item.charItemType
			from item                                
			INNER join ItemDetails Dtl on numChildItemID=numItemCode
			where  numItemKitID=@numItemCode 
			UNION ALL
			select CAST(Dtl.numItemKitID AS NUMERIC(9)),i.numItemCode,
			CAST(((DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(Dtl.numUOMID,i.numItemCode,i.numDomainID,i.numBaseUnit),1)) * c.numCalculatedQty) AS FLOAT) AS numCalculatedQty,ISNULL(Dtl.vcItemDesc,i.txtItemDesc) AS txtItemDesc,ISNULL(Dtl.sintOrder,0) AS sintOrder,
			Dtl.numUOMId,DTL.numQtyItemsReq,i.charItemType
			from item i                               
			INNER JOIN ItemDetails Dtl on Dtl.numChildItemID=i.numItemCode
			INNER JOIN CTE c ON Dtl.numItemKitID = c.numItemCode 
			--WHERE i.charItemType IN ('P','I')
			where Dtl.numChildItemID!=@numItemCode
		)

		INSERT INTO @TEMPitem
		(
			numItemKitID
			,numItemCode
			,numCalculatedQty
			,numWarehouseItmsID
			,txtItemDesc
			,sintOrder
			,numQtyItemsReq
			,numUOMId 
			,charItemType
		)
		SELECT 
			numItemKitID
			,numItemCode
			,numCalculatedQty
			,ISNULL((SELECT TOP 1 numWareHouseItemId FROM WarehouseItems WHERE numItemID=numItemCode AND numWareHouseID=@numWarehouseID ORDER BY numWareHouseItemID),0)
			,txtItemDesc
			,sintOrder
			,numQtyItemsReq
			,numUOMId
			,charItemType
		FROM 
			CTE

		IF EXISTS (SELECT numItemKitID FROM @TEMPitem WHERE charItemType='P' AND ISNULL(numWarehouseItmsID,0) = 0)
		BEGIN
			RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			RETURN
		END

	INSERT INTO WorkOrder
	(
		numItemCode, numQtyItemsReq, numWareHouseItemId, numCreatedBy, bintCreatedDate,	numDomainID,
		numWOStatus, numOppId, vcItemDesc, vcInstruction, bintCompliationDate, numAssignedTo, numOppItemID
	)
	VALUES
	(
		@numItemCode,@numUnitHour,@numWarehouseItmsID,@numUserCntID,getutcdate(),@numDomainID,
		0,@numOppID,@vcItemDesc,@vcInstruction,@bintCompliationDate,@numWOAssignedTo, @numoppitemtCode
	)
	
	set @numWOId=@@IDENTITY		
	

	INSERT INTO [WorkOrderDetails] 
	(
		numWOId,numItemKitID,numChildItemID,numQtyItemsReq,numWareHouseItemId,vcItemDesc,sintOrder,numQtyItemsReq_Orig,numUOMId
	)
	SELECT 
		@numWOId,numItemKitID,numItemCode,numCalculatedQty,numWarehouseItmsID,txtItemDesc,sintOrder,numQtyItemsReq,numUOMId FROM CTE
	
	FETCH NEXT FROM WOCursor INTO @numoppitemtCode, @numItemCode, @numUnitHour,@numWarehouseItmsID,@vcItemDesc,@vcInstruction,@bintCompliationDate,@numWOAssignedTo
END

	CLOSE WOCursor;
	DEALLOCATE WOCursor;
 
END 

END  
--Created by anoop jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_checkuseratlogin')
DROP PROCEDURE usp_checkuseratlogin
GO
CREATE PROCEDURE [dbo].[USP_CheckUserAtLogin]                                                              
(                                                                                    
	@UserName as varchar(100)='',                                                                        
	@vcPassword as varchar(100)='',
	@vcLinkedinId as varchar(300)=''                                                                           
)                                                              
AS             
BEGIN
	DECLARE @listIds VARCHAR(MAX)

	SELECT 
		@listIds = COALESCE(@listIds+',' ,'') + CAST(UDTL.numUserId AS VARCHAR(100)) 
	FROM 
		UnitPriceApprover U
	Join 
		Domain D                              
	on 
		D.numDomainID=U.numDomainID
	Join  
		Subscribers S                            
	on 
		S.numTargetDomainID=D.numDomainID
	LEFT JOIN UserMaster As UDTL
	ON
		U.numUserID=UDTL.numUserDetailId
	WHERE 
		1 = (CASE 
			WHEN @vcLinkedinId='N' THEN (CASE WHEN UDTL.vcEmailID=@UserName and UDTL.vcPassword=@vcPassword THEN 1 ELSE 0 END)
			ELSE (CASE WHEN UDTL.vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
			END)
       
	IF (SELECT 
			COUNT(*)
		FROM 
			UserMaster U                              
		JOIN 
			Domain D                              
		ON 
			D.numDomainID=U.numDomainID
		JOIN 
			Subscribers S                            
		ON 
			S.numTargetDomainID=D.numDomainID
		WHERE 
			1 = (CASE 
					WHEN @vcLinkedinId='N' THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
					ELSE (CASE WHEN vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
				END) 
			AND bitActive=1) > 1
	BEGIN
		RAISERROR('MULTIPLE_RECORDS_WITH_SAME_EMAIL',16,1)
		RETURN;
	END   
	                                          
	/*check subscription validity */
	IF EXISTS(SELECT 
					*
			FROM 
				UserMaster U                              
			JOIN 
				Domain D                              
			ON 
				D.numDomainID=U.numDomainID
			JOIN 
				Subscribers S                            
			ON 
				S.numTargetDomainID=D.numDomainID
			WHERE 
				1 = (CASE 
						WHEN @vcLinkedinId='N' THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
						ELSE (CASE WHEN vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
					END) 
				AND bitActive=1 
				AND getutcdate() > dtSubEndDate)
	BEGIN
		RAISERROR('SUB_RENEW',16,1)
		RETURN;
	END

	SELECT TOP 1 
		U.numUserID
		,numUserDetailId
		,D.numCost
		,isnull(vcEmailID,'') vcEmailID
		,ISNULL(vcEmailAlias,'') vcEmailAlias
		,ISNULL(vcEmailAliasPassword,'') vcEmailAliasPassword
		,isnull(numGroupID,0) numGroupID
		,bitActivateFlag
		,isnull(vcMailNickName,'') vcMailNickName
		,isnull(txtSignature,'') txtSignature
		,isnull(U.numDomainID,0) numDomainID
		,isnull(Div.numDivisionID,0) numDivisionID
		,isnull(vcCompanyName,'') vcCompanyName
		,isnull(E.bitExchangeIntegration,0) bitExchangeIntegration
		,isnull(E.bitAccessExchange,0) bitAccessExchange
		,isnull(E.vcExchPath,'') vcExchPath
		,isnull(E.vcExchDomain,'') vcExchDomain
		,isnull(D.vcExchUserName,'') vcExchUserName
		,isnull(vcFirstname,'') + ' ' + isnull(vcLastName,'') as ContactName
		,Case when E.bitAccessExchange=0 then  E.vcExchPassword when E.bitAccessExchange=1 then D.vcExchPassword end as vcExchPassword
		,tintCustomPagingRows
		,vcDateFormat
		,numDefCountry
		,tintComposeWindow
		,dateadd(day,-sintStartDate,getutcdate()) as StartDate
		,dateadd(day,sintNoofDaysInterval,dateadd(day,-sintStartDate,getutcdate())) as EndDate
		,tintAssignToCriteria
		,bitIntmedPage
		,tintFiscalStartMonth
		,numAdminID
		,isnull(A.numTeam,0) numTeam
		,isnull(D.vcCurrency,'') as vcCurrency
		,isnull(D.numCurrencyID,0) as numCurrencyID, 
		isnull(D.bitMultiCurrency,0) as bitMultiCurrency,              
		bitTrial,isnull(tintChrForComSearch,0) as tintChrForComSearch,  
		isnull(tintChrForItemSearch,0) as tintChrForItemSearch,              
		case when bitSMTPServer= 1 then vcSMTPServer else '' end as vcSMTPServer  ,          
		case when bitSMTPServer= 1 then  isnull(numSMTPPort,0)  else 0 end as numSMTPPort      
		,isnull(bitSMTPAuth,0) bitSMTPAuth    
		,isnull([vcSmtpPassword],'') vcSmtpPassword    
		,isnull(bitSMTPSSL,0) bitSMTPSSL   ,isnull(bitSMTPServer,0) bitSMTPServer,       
		isnull(IM.bitImap,0) bitImapIntegration, isnull(charUnitSystem,'E')   UnitSystem, datediff(day,getutcdate(),dtSubEndDate) as NoOfDaysLeft,
		ISNULL(bitCreateInvoice,0) bitCreateInvoice,
		ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
		ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
		vcDomainName,
		S.numSubscriberID,
		D.[tintBaseTax],
		u.ProfilePic,
		ISNULL(D.[numShipCompany],0) numShipCompany,
		ISNULL(D.[bitEmbeddedCost],0) bitEmbeddedCost,
		(Select isnull(numAuthoritativeSales,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativeSales,
		(Select isnull(numAuthoritativePurchase,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativePurchase,
		ISNULL(D.[numDefaultSalesOppBizDocId],0) numDefaultSalesOppBizDocId,
		ISNULL(tintDecimalPoints,2) tintDecimalPoints,
		ISNULL(D.tintBillToForPO,0) tintBillToForPO, 
		ISNULL(D.tintShipToForPO,0) tintShipToForPO,
		ISNULL(D.tintOrganizationSearchCriteria,0) tintOrganizationSearchCriteria,
		ISNULL(D.tintLogin,0) tintLogin,
		ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
		ISNULL(D.vcPortalName,'') vcPortalName,
		(SELECT COUNT(*) FROM dbo.Subscribers WHERE getutcdate() between dtSubStartDate and dtSubEndDate and bitActive=1),
		ISNULL(D.bitGtoBContact,0) bitGtoBContact,
		ISNULL(D.bitBtoGContact,0) bitBtoGContact,
		ISNULL(D.bitGtoBCalendar,0) bitGtoBCalendar,
		ISNULL(D.bitBtoGCalendar,0) bitBtoGCalendar,
		ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
		ISNULL(D.bitInlineEdit,0) bitInlineEdit,
		ISNULL(U.numDefaultClass,0) numDefaultClass,
		ISNULL(D.bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
		ISNULL(D.bitTrakDirtyForm,1) bitTrakDirtyForm,
		isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
		ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
		CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
		ISNULL(D.bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
		isnull(U.tintTabEvent,0) as tintTabEvent,
		isnull(D.bitRentalItem,0) as bitRentalItem,
		isnull(D.numRentalItemClass,0) as numRentalItemClass,
		isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
		isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
		isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
		isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
		isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
		ISNULL(U.numDefaultWarehouse,0) numDefaultWarehouse,
		ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
		ISNULL(D.bitAutoSerialNoAssign,0) bitAutoSerialNoAssign,
		ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
		ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
		ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
		ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
		ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
		ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
		ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
		ISNULL(D.tintCommissionType,1) AS tintCommissionType,
		ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
		ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
		ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
		ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
		ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
		ISNULL(D.bitUseBizdocAmount,1) AS [bitUseBizdocAmount],
		ISNULL(D.bitDefaultRateType,1) AS [bitDefaultRateType],
		ISNULL(D.bitIncludeTaxAndShippingInCommission,1) AS [bitIncludeTaxAndShippingInCommission],
		ISNULL(D.[bitLandedCost],0) AS bitLandedCost,
		ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
		ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
		ISNULL(@listIds,'') AS vcUnitPriceApprover,
		ISNULL(D.numPODropShipBizDoc,0) AS numPODropShipBizDoc,
		ISNULL(D.numPODropShipBizDocTemplate,0) AS numPODropShipBizDocTemplate,
		ISNULL(D.IsEnableDeferredIncome,0) AS IsEnableDeferredIncome,
		ISNULL(D.bitEnableItemLevelUOM,0) AS bitEnableItemLevelUOM,
		ISNULL(D.bitPurchaseTaxCredit,0) AS bitPurchaseTaxCredit,
		ISNULL(U.bintCreatedDate,0) AS bintCreatedDate,
		(CASE WHEN LEN(ISNULL(TempDefaultPage.vcDefaultNavURL,'')) = 0 THEN ISNULL(TempDefaultTab.vcDefaultNavURL,'Items/frmItemList.aspx?Page=All Items&ItemGroup=0') ELSE ISNULL(TempDefaultPage.vcDefaultNavURL,'') END) vcDefaultNavURL,
		ISNULL(D.bitApprovalforTImeExpense,0) AS bitApprovalforTImeExpense,
		ISNULL(D.intTimeExpApprovalProcess,0) AS intTimeExpApprovalProcess,ISNULL(D.bitApprovalforOpportunity,0) AS bitApprovalforOpportunity,
		ISNULL(D.intOpportunityApprovalProcess,0) AS intOpportunityApprovalProcess,
		ISNULL(D.numDefaultSalesShippingDoc,0) AS numDefaultSalesShippingDoc,
		ISNULL(D.numDefaultPurchaseShippingDoc,0) AS numDefaultPurchaseShippingDoc,
		ISNULL((SELECT TOP 1 bitMarginPriceViolated FROM ApprovalProcessItemsClassification  WHERE numDomainID=D.numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1),0) AS bitMarginPriceViolated,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=2 AND numPageID=2),0) AS tintLeadRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=3 AND numPageID=2),0) AS tintProspectRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=4 AND numPageID=2),0) AS tintAccountRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=10 AND numPageID=10),0) AS tintSalesOrderListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=10 AND numPageID=11),0) AS tintPurchaseOrderListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=10 AND numPageID=2),0) AS tintSalesOpportunityListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=10 AND numPageID=8),0) AS tintPurchaseOpportunityListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=11 AND numPageID=2),0) AS tintContactListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=7 AND numPageID=2),0) AS tintCaseListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=12 AND numPageID=2),0) AS tintProjectListRights,
		ISNULL((SELECT TOP 1 numCriteria FROM TicklerListFilterConfiguration WHERE CHARINDEX(CONCAT(',',U.numUserDetailID,','),CONCAT(',',vcSelectedEmployee,',')) > 0),0) AS tintActionItemRights,
		ISNULL(D.numAuthorizePercentage,0) AS numAuthorizePercentage,
		ISNULL(D.bitAllowDuplicateLineItems,0) bitAllowDuplicateLineItems,
		ISNULL(D.bitLogoAppliedToBizTheme,0) bitLogoAppliedToBizTheme,
		ISNULL(D.vcLogoForBizTheme,'') vcLogoForBizTheme,
		ISNULL(D.bitLogoAppliedToLoginBizTheme,0) bitLogoAppliedToLoginBizTheme,
		ISNULL(D.vcLogoForLoginBizTheme,'') vcLogoForLoginBizTheme,
		ISNULL(D.vcThemeClass,'1473b4') vcThemeClass,
		ISNULL(D.vcLoginURL,'') AS vcLoginURL,
		ISNULL(D.bitDisplayCustomField,0) AS bitDisplayCustomField,
		ISNULL(D.bitFollowupAnytime,0) AS bitFollowupAnytime,
		ISNULL(D.bitpartycalendarTitle,0) AS bitpartycalendarTitle,
		ISNULL(D.bitpartycalendarLocation,0) AS bitpartycalendarLocation,
		ISNULL(D.bitpartycalendarDescription,0) AS bitpartycalendarDescription,
		ISNULL(D.bitREQPOApproval,0) AS bitREQPOApproval,
		ISNULL(D.bitARInvoiceDue,0) AS bitARInvoiceDue,
		ISNULL(D.bitAPBillsDue,0) AS bitAPBillsDue,
		ISNULL(D.bitItemsToPickPackShip,0) AS bitItemsToPickPackShip,
		ISNULL(D.bitItemsToInvoice,0) AS bitItemsToInvoice,
		ISNULL(D.bitSalesOrderToClose,0) AS bitSalesOrderToClose,
		ISNULL(D.bitItemsToPutAway,0) AS bitItemsToPutAway,
		ISNULL(D.bitItemsToBill,0) AS bitItemsToBill,
		ISNULL(D.bitPosToClose,0) AS bitPosToClose,
		ISNULL(D.bitPOToClose,0) AS bitPOToClose,
		ISNULL(D.bitBOMSToPick,0) AS bitBOMSToPick,

		ISNULL(D.vchREQPOApprovalEmp,'') AS vchREQPOApprovalEmp,
		ISNULL(D.vchARInvoiceDue,'') AS vchARInvoiceDue,
		ISNULL(D.vchAPBillsDue,'') AS vchAPBillsDue,
		ISNULL(D.vchItemsToPickPackShip,'') AS vchItemsToPickPackShip,
		ISNULL(D.vchItemsToInvoice,'') AS vchItemsToInvoice,
		ISNULL(D.vchPriceMarginApproval,'') AS vchPriceMarginApproval,
		ISNULL(D.vchSalesOrderToClose,'') AS vchSalesOrderToClose,
		ISNULL(D.vchItemsToPutAway,'') AS vchItemsToPutAway,
		ISNULL(D.vchItemsToBill,'') AS vchItemsToBill,
		ISNULL(D.vchPosToClose,'') AS vchPosToClose,
		ISNULL(D.vchPOToClose,'') AS vchPOToClose,
		ISNULL(D.vchBOMSToPick,'') AS vchBOMSToPick,
		ISNULL(D.decReqPOMinValue,0) AS decReqPOMinValue,
		ISNULL(U.tintPayrollType,1) tintPayrollType,
		(CASE WHEN EXISTS (SELECT numCategoryHDRID FROM TimeAndExpense WHERE numUserCntID=U.numUserDetailId AND numCategory=1 AND numType=7 AND dtToDate IS NULL) THEN 1 ELSE 0 END) bitUserClockedIn
	FROM 
		UserMaster U                              
	left join ExchangeUserDetails E                              
	on E.numUserID=U.numUserID                              
	left join ImapUserDetails IM                              
	on IM.numUserCntID=U.numUserDetailID          
	Join Domain D                              
	on D.numDomainID=U.numDomainID                                           
	left join DivisionMaster Div                                            
	on D.numDivisionID=Div.numDivisionID                               
	left join CompanyInfo C                              
	on C.numCompanyID=Div.numCompanyID                               
	left join AdditionalContactsInformation A                              
	on  A.numContactID=U.numUserDetailId                            
	Join  Subscribers S                            
	on S.numTargetDomainID=D.numDomainID    
	 OUTER APPLY
	 (
		SELECT  
			TOP 1 REPLACE(SCB.Link,'../','') vcDefaultNavURL
		FROM    
			TabMaster T
		JOIN 
			GroupTabDetails G ON G.numTabId = T.numTabId
		LEFT JOIN
			ShortCutGrpConf SCUC ON G.numTabId=SCUC.numTabId AND SCUC.numDomainId=U.numDomainID AND SCUC.numGroupId=U.numGroupID AND bitDefaultTab=1
		LEFT JOIN
			ShortCutBar SCB
		ON
			SCB.Id=SCUC.numLinkId
		WHERE   
			(T.numDomainID = U.numDomainID OR bitFixed = 1)
			AND G.numGroupID = U.numGroupID
			AND ISNULL(G.[tintType], 0) <> 1
			AND tintTabType =1
			AND T.numTabID NOT IN (2,68)
		ORDER BY 
			SCUC.bitInitialPage DESC
	 )  TempDefaultPage 
	 OUTER APPLY
	 (
		SELECT  
			TOP 1 REPLACE(T.vcURL,'../','') vcDefaultNavURL
		FROM    
			TabMaster T
		JOIN 
			GroupTabDetails G 
		ON 
			G.numTabId = T.numTabId
		WHERE   
			(T.numDomainID = U.numDomainID OR bitFixed = 1)
			AND G.numGroupID = U.numGroupID
			AND ISNULL(G.[tintType], 0) <> 1
			AND tintTabType =1
			AND T.numTabID NOT IN (2,68)
			AND ISNULL(G.bitInitialTab,0) = 1 
			AND ISNULL(bitallowed,0)=1
		ORDER BY 
			G.bitInitialTab DESC
	 ) TEMPDefaultTab                   
	WHERE 1 = (CASE 
		WHEN @vcLinkedinId='N' THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
		ELSE (CASE WHEN U.vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
		END) and bitActive IN (1,2) AND getutcdate() between dtSubStartDate and dtSubEndDate
             
	SELECT 
		numTerritoryId 
	FROM
		UserTerritory UT                              
	JOIN 
		UserMaster U                              
	ON 
		numUserDetailId =UT.numUserCntID                                         
	WHERE 
		1 = (CASE 
			WHEN @vcLinkedinId='N' THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
			ELSE (CASE WHEN vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
			END)

END
GO
/****** Object:  StoredProcedure [dbo].[Usp_CreateCloneItem]    Script Date: 18/06/2013 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Manish Anjara
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Usp_CreateCloneItem')
DROP PROCEDURE Usp_CreateCloneItem
GO
CREATE PROCEDURE [dbo].[Usp_CreateCloneItem]
(
	@numItemCode		BIGINT OUTPUT,
	@numDomainID		NUMERIC(18,0)
)
AS 

BEGIN
--Item 	
--ItemAPI-
--ItemCategory
--ItemDetails
--ItemImages
--ItemTax
--Vendor,
--WareHouseItems
--CFW_FLD_Values_Item
BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numItemGroup AS NUMERIC(18,0)
	DECLARE @bitMatrix AS NUMERIC(18,0)
	DECLARE @vcItemName AS VARCHAR(300)

	SELECT @numItemGroup=numItemGroup,@bitMatrix=bitMatrix,@vcItemName=vcItemName FROM Item WHERE numItemCode = @numItemCode AND numDomainID = @numDomainID

	DECLARE @numNewItemCode AS BIGINT
	
	INSERT INTO dbo.Item 
	(vcItemName,txtItemDesc,charItemType,monListPrice,numItemClassification,bitTaxable,vcSKU,bitKitParent,numVendorID,numDomainID,numCreatedBy,bintCreatedDate,
	bintModifiedDate,numModifiedBy,bitAllowBackOrder,bitSerialized,vcModelID,numItemGroup,numCOGsChartAcntId,numAssetChartAcntId,numIncomeChartAcntId,
	monAverageCost,monCampaignLabourCost,bitFreeShipping,fltLength,fltWidth,fltHeight,fltWeight,vcUnitofMeasure,bitCalAmtBasedonDepItems,bitShowDeptItemDesc,
	bitShowDeptItem,bitAssembly,numBarCodeId,vcManufacturer,numBaseUnit,numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,vcPathForImage,
	vcPathForTImage,tintStandardProductIDType,numShipClass,vcExportToAPI,bitAllowDropShip,bitVirtualInventory,bitContainer,bitMatrix,numManufacturer,vcASIN,tintKitAssemblyPriceBasedOn,
	bitSOWorkOrder,bitKitSingleSelect,numExpenseChartAcntId,bitExpenseItem,numBusinessProcessId)
	SELECT vcItemName,txtItemDesc,charItemType,monListPrice,numItemClassification,bitTaxable,vcSKU,bitKitParent,numVendorID,numDomainID,numCreatedBy,GETDATE(),
	GETDATE(),numModifiedBy,bitAllowBackOrder,bitSerialized,vcModelID,numItemGroup,numCOGsChartAcntId,numAssetChartAcntId,numIncomeChartAcntId,
	0,monCampaignLabourCost,bitFreeShipping,fltLength,fltWidth,fltHeight,fltWeight,vcUnitofMeasure,bitCalAmtBasedonDepItems,bitShowDeptItemDesc,
	bitShowDeptItem,bitAssembly,numBarCodeId,vcManufacturer,numBaseUnit,numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,vcPathForImage,
	vcPathForTImage,tintStandardProductIDType,numShipClass,vcExportToAPI,bitAllowDropShip,bitVirtualInventory,bitContainer,bitMatrix,numManufacturer,vcASIN,tintKitAssemblyPriceBasedOn,
	bitSOWorkOrder,bitKitSingleSelect,numExpenseChartAcntId,bitExpenseItem,numBusinessProcessId
	FROM Item WHERE numItemCode = @numItemCode AND numDomainID = @numDomainID
	 
	SELECT @numNewItemCode  = SCOPE_IDENTITY()


	IF ISNULL(@bitMatrix,0) = 1
	BEGIN
		DECLARE @vcProductVariation VARCHAR(300)

		WITH data (ProductId, ProductOptionGroupId, ProductOptionId) AS 
		(
			SELECT
				@numItemCode
				,CFW_Fld_Master.Fld_id
				,ListDetails.numListItemID
			FROM
				ItemGroupsDTL
			INNER JOIN
				CFW_Fld_Master
			ON
				ItemGroupsDTL.numOppAccAttrID = CFW_Fld_Master.Fld_id
			INNER JOIN
				ListDetails
			ON
				CFW_Fld_Master.numlistid = ListDetails.numListID
			WHERE
				numItemGroupID = @numItemGroup
		),
		ranked AS (
		/* ranking the group IDs */
			SELECT
				ProductId,
				ProductOptionGroupId,
				ProductOptionId,
				GroupRank = DENSE_RANK() OVER (PARTITION BY ProductId ORDER BY ProductOptionGroupId)
			FROM 
				data
		),
		crossjoined AS (
			/* obtaining all possible combinations */
			SELECT
				ProductId,
				ProductOptionGroupId,
				GroupRank,
				ProductVariant = CAST(CONCAT(ProductOptionGroupId,':',ProductOptionId) AS varchar(250))
			FROM 
				ranked
			WHERE 
				GroupRank = 1
			UNION ALL
			SELECT
				r.ProductId,
				r.ProductOptionGroupId,
				r.GroupRank,
				ProductVariant = CAST(CONCAT(c.ProductVariant,',',r.ProductOptionGroupId,':',r.ProductOptionId) AS VARCHAR(250))
			FROM 
				ranked r
			INNER JOIN 
				crossjoined c 
			ON 
				r.ProductId = c.ProductId
			AND 
				r.GroupRank = c.GroupRank + 1
		  ),
		  maxranks AS (
			/* getting the maximum group rank value for every product */
			SELECT
				ProductId,
				MaxRank = MAX(GroupRank)
			FROM 
				ranked
			GROUP BY 
				ProductId
		  )
		
		SELECT TOP 1  
			@vcProductVariation = c.ProductVariant 
		FROM 
			crossjoined c 
		INNER JOIN 
			maxranks m 
		ON 
			c.ProductId = m.ProductId
			AND c.GroupRank = m.MaxRank
			AND c.ProductVariant NOT IN (SELECT 
											STUFF((SELECT ',' + CONCAT(FLD_ID,':',FLD_Value) FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=IR.numItemCode FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)'),1,1,'') AS NameValues
										FROM 
											Item
										INNER JOIN
											ItemAttributes IR
										ON
											IR.numDomainID=@numDomainID
											AND Item.numItemCode =IR.numItemCode
										WHERE 
											Item.numDomainID=@numDomainID
											AND bitMatrix = 1
											AND numItemGroup = @numItemGroup
											AND vcItemName = @vcItemName
										GROUP BY
											IR.numItemCode)

		IF LEN(ISNULL(@vcProductVariation,'')) = 0
		BEGIN
			RAISERROR('ATTRIBUTES_ARE_NOT_AVAILABLE_OR_NO_COMBINATION_LEFT_TO_ADD',16,1)
		END
		ELSE
		BEGIN
			INSERT INTO ItemAttributes
			(
				numDomainID
				,numItemCode
				,FLD_ID
				,FLD_Value
			)
			SELECT
				@numDomainID
				,@numNewItemCode
				,SUBSTRING(OutParam,0,CHARINDEX(':',OutParam,0))
				,SUBSTRING(OutParam,CHARINDEX(':',OutParam,0) + 1,LEN(OutParam))
			FROM
				dbo.SplitString(@vcProductVariation,',')
		END
	END
	 
	INSERT INTO dbo.ItemAPI 
	(WebApiId,numDomainId,numItemID,vcAPIItemID,numCreatedby,dtCreated,numModifiedby,dtModified)
	SELECT WebApiId,numDomainId,@numNewItemCode,vcAPIItemID,numCreatedby,GETDATE(),numModifiedby,GETDATE()
	FROM dbo.ItemAPI WHERE numItemID = @numItemCode AND numDomainId = @numDomainId
	
	INSERT INTO dbo.ItemCategory ( numItemID,numCategoryID )
	SELECT @numNewItemCode, numCategoryID FROM ItemCategory WHERE numItemID = @numItemCode
	
	INSERT INTO dbo.ItemDetails (numItemKitID,numChildItemID,numQtyItemsReq,numUOMId,vcItemDesc,sintOrder)
	SELECT @numNewItemCode,numChildItemID,numQtyItemsReq,numUOMId,vcItemDesc,sintOrder FROM dbo.ItemDetails WHERE numItemKitID = @numItemCode
	
	INSERT INTO dbo.ItemImages (numItemCode,vcPathForImage,vcPathForTImage,bitDefault,intDisplayOrder,numDomainId,bitIsImage)
	SELECT 
		@numNewItemCode
		,CONCAT(SUBSTRING(vcPathForImage, 0, CHARINDEX('.',vcPathForImage,0)),'_',@numNewItemCode,SUBSTRING(vcPathForImage, charindex('.',vcPathForImage,0), LEN(vcPathForImage)))
		,CONCAT(SUBSTRING(vcPathForTImage, 0, CHARINDEX('.',vcPathForTImage,0)),'_',@numNewItemCode,SUBSTRING(vcPathForTImage, charindex('.',vcPathForTImage,0), LEN(vcPathForTImage)))
		,bitDefault
		,intDisplayOrder
		,numDomainId
		,bitIsImage 
	FROM dbo.ItemImages WHERE numItemCode = @numItemCode  AND numDomainId = @numDomainID
	
	INSERT INTO dbo.ItemTax (numItemCode,numTaxItemID,bitApplicable)
	SELECT @numNewItemCode,numTaxItemID,bitApplicable FROM dbo.ItemTax WHERE numItemCode = @numItemCode
	
	INSERT INTO dbo.Vendor (numVendorID,vcPartNo,numDomainID,monCost,numItemCode,intMinQty)
	SELECT numVendorID,vcPartNo,numDomainID,monCost,@numNewItemCode,intMinQty 
	FROM dbo.Vendor WHERE numItemCode = @numItemCode AND numDomainID = @numDomainID
	
	INSERT INTO dbo.WareHouseItems (numItemID,numWareHouseID,numOnHand,numOnOrder,numReorder,numAllocation,numBackOrder,monWListPrice,vcLocation,vcWHSKU,
	vcBarCode,numDomainID,dtModified,numWLocationID) 
	SELECT @numNewItemCode,numWareHouseID,0,0,0,0,0,monWListPrice,vcLocation,vcWHSKU,vcBarCode,numDomainID,GETDATE(),numWLocationID 
	FROM dbo.WareHouseItems  WHERE numItemID = @numItemCode AND numDomainID = @numDomainID

	IF ISNULL(@bitMatrix,0) = 1 AND (SELECT COUNT(*) FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numNewItemCode) > 0
	BEGIN
		-- INSERT NEW WAREHOUSE ATTRIBUTES
		INSERT INTO CFW_Fld_Values_Serialized_Items
		(
			Fld_ID,
			Fld_Value,
			RecId,
			bitSerialized
		)                                                  
		SELECT 
			Fld_ID,
			ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
			TEMPWarehouse.numWarehouseItemID,
			0 
		FROM 
			ItemAttributes  
		OUTER APPLY
		(
			SELECT
				numWarehouseItemID
			FROM 
				WarehouseItems 
			WHERE 
				numDomainID=@numDomainID 
				AND numItemID=@numNewItemCode
		) AS TEMPWarehouse
		WHERE
			numDomainID=@numDomainID 
			AND numItemCode=@numNewItemCode
	END
	
	INSERT INTO CFW_FLD_Values_Item (Fld_ID,Fld_Value,RecId)  
	SELECT Fld_ID,Fld_Value,@numNewItemCode FROM CFW_FLD_Values_Item  WHERE RecId = @numItemCode 


	SET @numItemCode = @numNewItemCode
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH	
END
/****** Object:  StoredProcedure [dbo].[usp_GetLayoutInfoDdl]    Script Date: 07/26/2008 16:17:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
exec usp_GetLayoutInfoDdl @numUserCntId=85098,@intcoulmn=0,@numDomainID=110,@Ctype='P',@PageId=1,@numRelation=46
exec usp_GetLayoutInfoDdl @numUserCntId=85098,@intcoulmn=1,@numDomainID=110,@Ctype='P',@PageId=1,@numRelation=46
*/
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getlayoutinfoddl')
DROP PROCEDURE usp_getlayoutinfoddl
GO
CREATE PROCEDURE [dbo].[usp_GetLayoutInfoDdl]                                
@numUserCntId as numeric(9)=0,                              
@intcoulmn as numeric(9),                              
@numDomainID as numeric(9)=0 ,                    
--@Ctype as char ,              
@PageId as numeric(9)=4,              
@numRelation as numeric(9)=0,
@numFormID as numeric(9)=0,
@tintPageType TINYINT             
as                        
                      
 if( @intcoulmn <> 0 )--Fields added to layout
begin                         
IF(@tintPageType=3)
BEGIN
IF((SELECT COUNT(*) FROM View_DynamicColumns WHERE      numUserCntID=@numUserCntID and  numDomainID=@numDomainID
 and numFormId=@numFormID and numRelCntType=@numRelation 
 AND tintPageType=@tintPageType AND 
  1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
		  WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
		  ELSE 0 END))=0)
BEGIN
		INSERT INTO DycFormConfigurationDetails
			(
				numFormID,
				numFieldID,
				intColumnNum,
				intRowNum,
				numDomainID,
				numAuthGroupID,
				numRelCntType,
				tintPageType,
				bitCustom,
				numUserCntID,
				numFormFieldGroupId,
				bitDefaultByAdmin
			)
		SELECT numFormID,numFieldID,intColumnNum,intRowNum,@numDomainID,numGroupID,@numRelation,@tintPageType,bitCustom,@numUserCntId,numFormFieldGroupId,1 FROM 
		BizFormWizardMasterConfiguration WHERE numDomainID=@numDomainID
		AND numFormID=@numFormID AND tintPageType=@tintPageType
		AND numRelCntType=@numRelation
		AND numGroupID=(SELECT TOP 1 numGroupID FROM  UserMaster WHERE  numDomainID =  @numDomainID AND numUserDetailID = @numUserCntId)
END
END
SELECT ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,numFieldId,
convert(char(1),bitCustom) as bitCustomField,tintRow,bitRequired,ISNULL(bitDefaultByAdmin,0) AS bitDefaultByAdmin
FROM View_DynamicColumns
WHERE numUserCntID=@numUserCntID and  numDomainID=@numDomainID  and tintColumn = @intcoulmn      
 and numFormId=@numFormID and ISNULL(bitCustom,0)=0 and numRelCntType=@numRelation 
 AND tintPageType=@tintPageType AND 
  1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
		  WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
		  ELSE 0 END)  order by tintRow  
       
if @pageid =1 or @pageid =4  OR @pageid =12 OR @pageid =13 OR @pageid =14
 begin    
 
 select vcfieldName,numFieldId,'1' as bitCustomField,tintRow,bitIsRequired AS bitRequired,ISNULL(bitDefaultByAdmin,0) AS bitDefaultByAdmin
  from View_DynamicCustomColumns_RelationShip
 where grp_id=@PageId and numDomainID=@numDomainID and subgrp=0           
 and numUserCntID=@numUserCntID and tintColumn = @intcoulmn
 AND numFormId=@numFormID and ISNULL(bitCustom,0)=1 and numRelCntType=@numRelation AND tintPageType=@tintPageType
  
end      
      
if @PageId= 2 or  @PageId= 3 or @PageId= 5 or @PageId= 6 or @PageId= 7 or @PageId= 8    or @PageId= 11               
begin 

 select vcfieldName,numFieldId,'1' as bitCustomField,tintRow,bitIsRequired AS bitRequired,ISNULL(bitDefaultByAdmin,0) AS bitDefaultByAdmin
  from View_DynamicCustomColumns
 where grp_id=@PageId and numDomainID=@numDomainID and subgrp=0           
 and numUserCntID=@numUserCntID and  tintColumn = @intcoulmn
 AND numFormId=@numFormID and ISNULL(bitCustom,0)=1 and numRelCntType=@numRelation AND tintPageType=@tintPageType

end       
                      
end                        
                        
                          
if @intcoulmn = 0 --Available Fields
begin                          
                      
 
 SELECT ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,numFieldId,'0' as bitCustomField,bitRequired, 0 AS bitDefaultByAdmin
FROM View_DynamicDefaultColumns
 WHERE numFormId=@numFormID AND numDomainID=@numDomainID  AND
   1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
		 WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END
		 ELSE 0 END)
 AND numFieldId NOT IN 
 (SELECT numFieldId FROM View_DynamicColumns
WHERE numUserCntID=@numUserCntID and  numDomainID=@numDomainID       
 and numFormId=@numFormID and ISNULL(bitCustom,0)=0 and numRelCntType=@numRelation 
 AND tintPageType=@tintPageType AND 1=(CASE WHEN @tintPageType=2 THEN Case WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
		 WHEN @tintPageType=3 THEN Case WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
		 ELSE 0 END)) ORDER BY ISNULL(vcCultureFieldName,vcFieldName)

       
if @pageid=4 or @pageid=1  OR @pageid =12 OR @pageid =13  OR @pageid =14                
begin           

 select fld_label as vcfieldName ,fld_id as numFieldId,'1' as bitCustomField,CAST(0 as bit) AS bitRequired, 0 AS bitDefaultByAdmin
  from CFW_Fld_Master CFM 
						  left join CFw_Grp_Master on subgrp=CFM.Grp_id                              
 where CFM.grp_id=@PageId and CFM.numDomainID=@numDomainID and subgrp=0 
 AND fld_id not IN (SELECT numFieldId FROM View_DynamicCustomColumns_RelationShip where           
 numUserCntID=@numUserCntID and  numDomainID=@numDomainID  
 AND numFormId=@numFormID and ISNULL(bitCustom,0)=1 and numRelCntType=@numRelation AND tintPageType=@tintPageType)
 order by subgrp 

end        
      
if @PageId= 2 or  @PageId= 3 or @PageId= 5 or @PageId= 6 or @PageId= 7 or @PageId= 8   or @PageId= 11                
begin 

 select fld_label as vcfieldName ,fld_id as numFieldId,'1' as bitCustomField,CAST(0 as bit) AS bitRequired, 0 AS bitDefaultByAdmin
  from CFW_Fld_Master CFM left join CFw_Grp_Master on subgrp=CFM.Grp_id                              
 where CFM.grp_id=@PageId and CFM.numDomainID=@numDomainID and subgrp=0 
 AND fld_id not IN (SELECT numFieldId FROM View_DynamicCustomColumns where           
 numUserCntID=@numUserCntID and  numDomainID=@numDomainID  
 AND numFormId=@numFormID and ISNULL(bitCustom,0)=1 and numRelCntType=@numRelation AND tintPageType=@tintPageType)
 order by subgrp 
                          
end       
      
end
GO
/****** Object:  StoredProcedure [dbo].[usp_GetRecentItems]    Script Date: 07/26/2008 16:18:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
--EXEC usp_GetRecentItems 1 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getrecentitems')
DROP PROCEDURE usp_getrecentitems
GO
CREATE PROCEDURE [dbo].[usp_GetRecentItems]
(   
	@numUserCntID as numeric(9)      
)
AS
BEGIN
	IF @numUserCntID = 0
	BEGIN
		--get blank table to avoid error
		select 0 numRecId 
		from communication    
		where  1=0
		RETURN
	END
	

	declare @v1 int
	if exists(select  ISNULL([intLastViewedRecord],10) FROM [Domain] WHERE [numDomainId] IN (SELECT numDomainID FROM [AdditionalContactsInformation] WHERE [numContactId]=@numUserCntID))
		begin
			select @v1 = ISNULL([intLastViewedRecord],10) FROM [Domain] WHERE [numDomainId] IN (SELECT numDomainID FROM [AdditionalContactsInformation] WHERE [numContactId]=@numUserCntID)
		end
	else
		begin
			set @v1=10
		end


	set rowcount @v1;

	DECLARE @TEMPTABLE TABLE
	(
		numRecordId numeric(18,0),
		bintvisiteddate DATETIME,
		numRecentItemsID numeric(18,0),
		chrRecordType char(10)
	)


	INSERT INTO 
		@TEMPTABLE
	SELECT DISTINCT
		numrecordId,
		bintvisiteddate,
		numRecentItemsID,
		chrRecordType	
	FROM    
		RECENTITEMS
	WHERE   
		numUserCntId = @numUserCntID
		AND bitdeleted = 0
	ORDER BY 
		bintvisiteddate DESC
	set rowcount 0

	SELECT  DM.numDivisionID AS numRecID ,isnull(CMP.vcCompanyName,'' ) as RecName,                           
		 DM.tintCRMType,                             
		 CMP.numCompanyID AS numCompanyID,                                           
		 ADC.numContactID AS numContactID,          
		 DM.numDivisionID,'C'   as Type,bintvisiteddate  ,0 as caseId,0 as caseTimeId,0 as caseExpId,
		 0 as numOppID,
		'~/images/Icon/organization.png' AS vcImageURL
		FROM  CompanyInfo CMP                            
		join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID                  
		LEFT join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID AND ISNULL(ADC.bitPrimaryContact,0)=1         
		join @TEMPTABLE R on R.numRecordID=DM.numDivisionID                                             
		WHERE chrRecordType='C'    
	union     
         
	SELECT  DM.numDivisionID AS numRecID,    
	isnull(ISNULL(vcFirstName,'-') + ' '+ISNULL(vcLastName,'-'),'') as RecName,                     
		 DM.tintCRMType,                             
		 CMP.numCompanyID AS numCompanyID,                                           
		 ADC.numContactID AS numContactID,          
		 DM.numDivisionID,'U',bintvisiteddate     ,0 as caseId,0 as caseTimeId,0 as caseExpId,
		 0 as numOppID,
		 '~/images/Icon/contact.png' AS vcImageURL                
		FROM  CompanyInfo CMP                            
		join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID                  
		join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID          
		join @TEMPTABLE R on numRecordID=ADC.numContactID   where chrRecordType='U'                                         
    
    
	union           
    
	select  numOppId as numRecID,isnull(vcPOppName,'') as RecName,          
	0,0,0,numDivisionID,'O',bintvisiteddate  ,0 as caseId,0 as caseTimeId,0 as caseExpId,
	0 as numOppID,
	CASE 
		WHEN tintOppType=1 AND tintOppStatus=0 THEN '~/images/Icon/Sales opportunity.png'
		WHEN tintOppType=1 AND tintOppStatus=2 THEN '~/images/Icon/Sales opportunity.png'
		WHEN tintOppType=1 AND tintOppStatus=1 THEN '~/images/Icon/Sales order.png'
		WHEN tintOppType=2 AND tintOppStatus=0 THEN '~/images/Icon/Purchase opportunity.png'
		WHEN tintOppType=2 AND tintOppStatus=1 THEN '~/images/Icon/Purchase order.png'
	ELSE '~/images/icons/cart.png' 
	END AS vcImageURL from OpportunityMaster          
	join @TEMPTABLE  on numRecordID=numOppId    where  chrRecordType='O'      
    
          
	union       
        
	select  numProId as numRecID,isnull(vcProjectName,'') as RecName,          
	0,0,0,numDivisionID,'P',bintvisiteddate  ,0 as caseId,0 as caseTimeId,0 as caseExpId,0 as numOppID,'~/images/Icon/project.png' AS vcImageURL from ProjectsMaster          
	join @TEMPTABLE  on numRecordID=numProId  where  chrRecordType='P'    
       
    
	union    
           
	select  numCaseId as numRecID,    
	isnull(vcCaseNumber,'')  as RecName,          
	0,0,0,numDivisionID,'S',bintvisiteddate ,0 as caseId,0 as caseTimeId,0 as caseExpId,0 as numOppID,'~/images/Icon/case.png' AS vcImageURL  from Cases          
	join @TEMPTABLE  on numRecordID=numCaseId   where  chrRecordType='S'      
    
	union    
    
	select numCommId as numRecId,    
	isnull(    
	case when bitTask =971 then 'C - '    
	  when bitTask =972 then 'T - '    
	  when bitTask =973 then 'N - '    
	  when bitTask =974 then 'F - '   
	 else convert(varchar(2),dbo.fn_GetListItemName(bitTask))  
	end + CONVERT(VARCHAR(100),textDetails),'')    
	as RecName,0,0,0,numDivisionID,'A',bintvisiteddate     
	 , caseId, caseTimeId, caseExpId,0 as numOppID,'~/images/Icon/action.png' AS vcImageURL  
	from communication    
	join @TEMPTABLE  on numRecordID=numCommId   where  chrRecordType='A'    
    
    
		union    
        

	select  numItemCode as numRecID,    
	isnull(vcItemName,'') as RecName, 
	0,0,0,0,'I',bintvisiteddate ,0 as caseId,0 as caseTimeId,0 as caseExpId,0 as numOppID,'~/images/Icon/item.png' AS vcImageURL  from Item          
	join @TEMPTABLE  on numRecordID=numItemCode   where  chrRecordType='I'  

	  union    
           
	select  numItemCode as numRecID,    
	isnull(vcItemName,'') as RecName, 
	0,0,0,0,'AI',bintvisiteddate ,0 as caseId,0 as caseTimeId,0 as caseExpId,0 as numOppID,'~/images/Icon/item.png' AS vcImageURL  from Item          
	join @TEMPTABLE  on numRecordID=numItemCode   where  chrRecordType='AI'  
                                    
	union    
           
	select  numGenericDocId as numRecID,    
	isnull(vcDocName,'')  as RecName,          
	0,0,0,0,'D',bintvisiteddate ,0 as caseId,0 as caseTimeId,0 as caseExpId,0 as numOppID,'~/images/Icon/Doc.png' AS vcImageURL  from GenericDocuments          
	join @TEMPTABLE  on numRecordID=numGenericDocId   where  chrRecordType='D'      

	union 

	select  numCampaignId as numRecID,isnull(vcCampaignName,'') as RecName,          
	0,0,0,0 AS numDivisionID,'M',bintvisiteddate  ,0 as caseId,0 as caseTimeId,0 as caseExpId,0 as numOppID,'~/images/Icon/campaign.png' AS vcImageURL from dbo.CampaignMaster        
	join @TEMPTABLE  on numRecordID=numCampaignId    where  chrRecordType='M'      
    
	union 

	select  numReturnHeaderID as numRecID,isnull(vcRMA,'') as RecName,          
	0,0,0,numDivisionID,'R',bintvisiteddate  ,0 as caseId,0 as caseTimeId,0 as caseExpId,0 as numOppID, CASE 
																											WHEN tintReturnType=1 THEN '~/images/Icon/Sales Return.png'
																											WHEN tintReturnType=2 THEN '~/images/Icon/Purchase Return.png'
																											WHEN tintReturnType=3 THEN '~/images/Icon/CreditMemo.png'
																											WHEN tintReturnType=4 THEN '~/images/Icon/Refund.png'
																										ELSE 
																											'~/images/Icon/ReturnLastView.png' 
																										END AS vcImageURL from ReturnHeader          
	join @TEMPTABLE  on numRecordID=numReturnHeaderID  where  chrRecordType='R'   

	UNION

	SELECT
		numOppBizDocsId as numRecID,
		isnull(vcBizDocID,'') as RecName,
		0,0,0,0 AS numDivisionID,'B',bintvisiteddate,0 as caseId,0 as caseTimeId,0 as caseExpId,OpportunityMaster.numOppId as numOppID,
		(CASE 
			WHEN OpportunityMaster.tintOppType = 1 THEN '~/images/Icon/Sales BizDoc.png' 
			WHEN OpportunityMaster.tintOppType = 2 THEN '~/images/Icon/Purchase BizDoc.png' 
			ELSE 
				'~/images/Icon/Doc.png' 
		END) AS vcImageURL
	FROM
		OpportunityBizDocs
	JOIN
		OpportunityMaster
	ON
		OpportunityBizDocs.numOppId = OpportunityMaster.numOppId
	JOIN
		@TEMPTABLE
	ON
		numOppBizDocsId = numRecordId
	WHERE
		chrRecordType = 'B'
	union 

	SELECT 
		numWOId AS numRecID
		,ISNULL(vcWorkOrderName,'') as RecName
		,0
		,0
		,0
		,0 AS numDivisionID
		,'W'
		,bintvisiteddate
		,0 as caseId
		,0 as caseTimeId
		,0 as caseExpId
		,0 as numOppID
		,'~/images/Icon/WorkOrder.png' AS vcImageURL 
	FROM 
		dbo.WorkOrder        
	JOIN
		@TEMPTABLE 
	ON 
		numRecordID=numWOId
	WHERE 
		chrRecordType='W'  

	order by bintVisitedDate desc     
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON


GO

IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWareHouseItems')
DROP PROCEDURE USP_GetWareHouseItems
GO



CREATE PROCEDURE [dbo].[USP_GetWareHouseItems]                              
@numItemCode as numeric(9)=0,
@byteMode as tinyint=0,
@numWarehouseItemID AS NUMERIC(18,0) = NULL,
@numWarehouseID numeric(18,0)=0,
@numWLocationID AS NUMERIC(18,0)=0 --05052018 Change: Warehouse Location added                    
AS
BEGIN                                                  
	DECLARE @bitSerialize AS BIT
	DECLARE @str AS NVARCHAR(MAX)                 
	DECLARE @str1 AS NVARCHAR(MAX)
	DECLARE @ColName AS VARCHAR(500)                      
	SET @str=''                       

	DECLARE @numDomainID AS INT 
	DECLARE @numBaseUnit AS NUMERIC(18,0)
	DECLARE @vcUnitName as VARCHAR(100) 
	DECLARE @numSaleUnit AS NUMERIC(18,0)
	DECLARE @vcSaleUnitName as VARCHAR(100) 
	DECLARE @numPurchaseUnit AS NUMERIC(18,0)
	DECLARE @vcPurchaseUnitName as VARCHAR(100)
	DECLARE @bitLot AS BIT                     
	DECLARE @bitKitParent BIT
	DECLARE @bitAssembly BIT
	DECLARE @bitMatrix BIT
	DECLARE @numItemGroupID as numeric(9)                        
                        
	SELECT 
		@numDomainID=numDomainID,
		@numBaseUnit = ISNULL(numBaseUnit,0),
		@numSaleUnit = ISNULL(numSaleUnit,0),
		@numPurchaseUnit = ISNULL(numPurchaseUnit,0),
		@numItemGroupID=numItemGroup,
		@bitLot = bitLotNo,
		@bitSerialize=CASE WHEN bitSerialized=0 THEN bitLotNo ELSE bitSerialized END,
		@bitKitParent = ( CASE 
							WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0
							WHEN ISNULL(bitKitParent,0) = 1 THEN 1
							ELSE 0
						 END )
		,@bitMatrix=bitMatrix
		,@bitAssembly=ISNULL(bitAssembly,0)
	FROM 
		Item 
	WHERE 
		numItemCode=@numItemCode     
	
	SELECT @vcUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numBaseUnit
	SELECT @vcSaleUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numSaleUnit
	SELECT @vcPurchaseUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numPurchaseUnit
	
	DECLARE @numSaleUOMFactor AS DECIMAL(28,14)
	DECLARE @numPurchaseUOMFactor AS DECIMAL(28,14)

	SELECT @numSaleUOMFactor = dbo.fn_UOMConversion(@numBaseUnit,@numItemCode,@numDOmainID,@numSaleUnit)
	SELECT @numPurchaseUOMFactor = dbo.fn_UOMConversion(@numBaseUnit,@numItemCode,@numDOmainID,@numPurchaseUnit)


	DECLARE @ID AS NUMERIC(18,0)  = 0                       
	DECLARE @numCusFlDItemID AS VARCHAR(20)                        
	DECLARE @fld_label AS VARCHAR(100),@fld_type AS VARCHAR(100)                        
	DECLARE @Fld_ValueMatrix NUMERIC(18,0)
	DECLARE @Fld_ValueNameMatrix VARCHAR(300)

	IF @bitMatrix = 1
	BEGIN
		SET @ColName='I.numItemCode,1' 

		SELECT 
			ROW_NUMBER() OVER(ORDER BY CFW_Fld_Master.fld_id) ID
			,CFW_Fld_Master.Fld_label
			,CFW_Fld_Master.fld_id
			,CFW_Fld_Master.fld_type
			,CFW_Fld_Master.numlistid
			,CFW_Fld_Master.vcURL
			,ItemAttributes.FLD_Value
			,CASE 
				WHEN CFW_Fld_Master.fld_type='SelectBox' THEN REPLACE(dbo.GetListIemName(Fld_Value),'''','''''')
				WHEN CFW_Fld_Master.fld_type='CheckBox' THEN CASE WHEN isnull(Fld_Value,0)=1 THEN 'Yes' ELSE 'No' END
				ELSE CAST(Fld_Value AS VARCHAR)
			END AS FLD_ValueName
		INTO 
			#tempTableMatrix
		FROM
			CFW_Fld_Master 
		INNER JOIN
			ItemGroupsDTL 
		ON 
			CFW_Fld_Master.Fld_id = ItemGroupsDTL.numOppAccAttrID
			AND ItemGroupsDTL.tintType = 2
		LEFT JOIN
			ItemAttributes
		ON
			CFW_Fld_Master.Fld_id = ItemAttributes.FLD_ID
			AND ItemAttributes.numItemCode = @numItemCode
		LEFT JOIN
			ListDetails LD
		ON
			CFW_Fld_Master.numlistid = LD.numListID
		WHERE
			CFW_Fld_Master.numDomainID = @numDomainId
			AND ItemGroupsDTL.numItemGroupID = @numItemGroupID
		GROUP BY
			CFW_Fld_Master.Fld_label
			,CFW_Fld_Master.fld_id
			,CFW_Fld_Master.fld_type
			,CFW_Fld_Master.bitAutocomplete
			,CFW_Fld_Master.numlistid
			,CFW_Fld_Master.vcURL
			,ItemAttributes.FLD_Value 

	
		DECLARE @i AS INT = 1
		DECLARE @iCount AS INT
		SELECT @iCount = COUNT(*) FROM #tempTableMatrix
                         
		WHILE @i <= @iCount
		 BEGIN                        
			SELECT @fld_label=Fld_label,@Fld_ValueMatrix=FLD_Value,@Fld_ValueNameMatrix=FLD_ValueName FROM #tempTableMatrix WHERE ID=@i
                          
			SET @str = @str+ CONCAT (',''',@Fld_ValueMatrix,''' as [',@fld_label,']')
	
			IF @byteMode=1                                        
				SET @str = @str+ CONCAT (',''',@Fld_ValueNameMatrix,''' as [',@fld_label,'Value]')                                   
                          
			SET @i = @i + 1
		 END
	END
	ELSE
	BEGIN
		SET @ColName='WareHouseItems.numWareHouseItemID,0' 

		--Create a Temporary table to hold data                                                            
		create table #tempTable 
		( 
			ID INT IDENTITY PRIMARY KEY,                                                                      
			numCusFlDItemID NUMERIC(9),
			Fld_Value VARCHAR(20)                                                         
		)

		INSERT INTO #tempTable                         
		(
			numCusFlDItemID
			,Fld_Value
		)                                                            
		SELECT DISTINCT
			numOppAccAttrID
			,''
		FROM 
			ItemGroupsDTL 
		WHERE 
			numItemGroupID=@numItemGroupID 
			AND tintType=2    

		SELECT TOP 1 
			@ID=ID
			,@numCusFlDItemID=numCusFlDItemID
			,@fld_label=fld_label
			,@fld_type=fld_type 
		FROM 
			#tempTable                         
		JOIN 
			CFW_Fld_Master 
		ON 
			numCusFlDItemID=Fld_ID                        
                         
		 WHILE @ID>0                        
		 BEGIN                        
                          
			SET @str = @str+',  dbo.GetCustFldItems('+@numCusFlDItemID+',9,'+@ColName+') as ['+ @fld_label+']'
	
			IF @byteMode=1                                        
				SET @str = @str+',  dbo.GetCustFldItemsValue('+@numCusFlDItemID+',9,'+@ColName+','''+@fld_type+''') as ['+ @fld_label+'Value]'                                        
                          
		   select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempTable                         
		   join CFW_Fld_Master on numCusFlDItemID=Fld_ID and ID >@ID                        
		   if @@rowcount=0 set @ID=0                        
                          
		 END
	END          

	DECLARE @KitOnHand AS NUMERIC(9);SET @KitOnHand=0      
  
	set @str1='select '

	IF @byteMode=1                                        
		set @str1 =@str1 +'I.numItemCode,'
	
	set @str1 = CONCAT(@str1,'numWareHouseItemID,WareHouseItems.numWareHouseID,
	ISNULL(vcWarehouse,'''') + (CASE WHEN ISNULL(WL.vcLocation,'''') = '''' THEN '''' ELSE '', '' + isnull(WL.vcLocation,'''') END) vcWarehouse,
	ISNULL(vcWarehouse,'''') AS vcExternalLocation,
	(CASE WHEN WareHouseItems.numWLocationID = -1 THEN ''Global'' ELSE ISNULL(WL.vcLocation,'''') END) AS vcInternalLocation,
	W.numWareHouseID,
	ISNULL(WareHouseItems.numOnHand,0) + ISNULL(WareHouseItems.numAllocation,0) AS TotalOnHand,
	(ISNULL(WareHouseItems.numOnHand,0) * (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END)) AS monCurrentValue,
	Case when @bitKitParent=1 then CAST(ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) AS DECIMAL(18,2)) ELSE CAST(ISNULL(numOnHand,0) AS FLOAT) END AS [OnHand],
	CAST((@numPurchaseUOMFactor * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as FLOAT) as PurchaseOnHand,
	CAST((@numSaleUOMFactor * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as FLOAT) as SalesOnHand,
	CAST(ISNULL(numReorder,0) AS FLOAT) as [Reorder] 
	,Case when @bitKitParent=1 THEN 0 ELSE CAST(ISNULL(numOnOrder,0) AS FLOAT) END as [OnOrder]
	,Case when @bitKitParent=1 OR @bitAssembly=1 THEN 0 ELSE CAST(ISNULL((SELECT SUM(numUnitHour) FROM OpportunityItems OI INNER JOIN OpportunityMaster OM ON OI.numOppID=OM.numOppID WHERE OM.numDomainID=',ISNULL(@numDomainID,0),' AND tintOppType=1 AND tintOppStatus=0 AND OI.numItemCode=WareHouseItems.numItemID AND OI.numWarehouseItmsID=WareHouseItems.numWareHouseItemID),0) AS FLOAT) END as [Requisitions]
	,Case when @bitKitParent=1 THEN 0 ELSE CAST(ISNULL(numAllocation,0) AS FLOAT) END as [Allocation] 
	,Case when @bitKitParent=1 THEN 0 ELSE CAST(ISNULL(numBackOrder,0) AS FLOAT) END as [BackOrder],
	CAST((@numSaleUOMFactor * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as numeric(18,2)) as OnHandUOM,
	CAST((@numPurchaseUOMFactor * Case when @bitKitParent=1 THEN 0 ELSE isnull(numOnOrder,0) END) AS DECIMAL(18,2)) as OnOrderUOM,
	CAST((@numPurchaseUOMFactor * Case when @bitKitParent=1 THEN 0 ELSE isnull(numReorder,0) END) AS DECIMAL(18,2)) as ReorderUOM,
	CAST((@numSaleUOMFactor * Case when @bitKitParent=1 THEN 0 ELSE isnull(numAllocation,0) END) AS DECIMAL(18,2)) as AllocationUOM,
	CAST((@numSaleUOMFactor * Case when @bitKitParent=1 THEN 0 ELSE isnull(numBackOrder,0) END) AS DECIMAL(18,2)) as BackOrderUOM,
	@vcUnitName As vcBaseUnit,
	@vcSaleUnitName As vcSaleUnit,
	@vcPurchaseUnitName As vcPurchaseUnit
	,0 as Op_Flag , isnull(WL.vcLocation,'''') Location,isnull(WL.numWLocationID,0) as numWLocationID,
	round(isnull(monWListPrice,0),2) Price,isnull(vcWHSKU,'''') as SKU,isnull(vcBarCode,'''') as BarCode ',@str,'                   
	,(SELECT CASE WHEN ISNULL(subI.bitKitParent,0) = 1 AND ISNULL((SELECT COUNT(*) FROM OpportunityItems 
																   WHERE OpportunityItems.numItemCode = WareHouseItems.numItemID
																   AND WareHouseItems.numWareHouseItemID = OpportunityItems.numWarehouseItmsID),0) = 0 THEN 1
				ELSE 0 
			END 
	FROM Item subI WHERE subI.numItemCode = WareHouseItems.numItemID) [IsDeleteKitWarehouse],@bitKitParent [bitKitParent],
	(CASE WHEN (SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID=WareHouseItems.numItemID) > 0 THEN 1 ELSE 0 END) AS bitChildItemWarehouse
	,',(CASE WHEN @bitSerialize=1 THEN CONCAT('dbo.GetWarehouseSerialLot(',ISNULL(@numDomainID,0),',WareHouseItems.numWareHouseItemID,',ISNULL(@bitLot,0),')') ELSE '''''' END) ,' AS vcSerialLot,
	CASE 
		WHEN ISNULL(I.numItemGroup,0) > 0 
		THEN dbo.fn_GetAttributes(WareHouseItems.numWareHouseItemID,0)
		ELSE ''''
	END AS vcAttribute,
	I.numItemcode,
	ISNULL(I.bitKitParent,0) AS bitKitParent,
	ISNULL(I.bitSerialized,0) AS bitSerialized,
	ISNULL(I.bitLotNo,0) as bitLotNo,
	ISNULL(I.bitAssembly,0) as bitAssembly,
	ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 461 AND numListItemID = I.numItemClass) ,'''') AS numShipClass,
			(
				CASE 
					WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE OpportunityItems.numItemCode = WareHouseItems.numItemID AND WareHouseItems.numWareHouseItemID = OpportunityItems.numWarehouseItmsID),0) = 0 
					THEN 1
				ELSE 
						0 
				END 
			) [IsDeleteKitWarehouse],
	CASE WHEN (SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID=WareHouseItems.numItemID) > 0 THEN 1 ELSE 0 END AS bitChildItemWarehouse,
	I.numAssetChartAcntId,
	(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) monAverageCost 
	from WareHouseItems                           
	join Warehouses W                             
	on W.numWareHouseID=WareHouseItems.numWareHouseID 
	left join WarehouseLocation WL on WL.numWLocationID =WareHouseItems.numWLocationID
	join Item I on I.numItemCode=WareHouseItems.numItemID and WareHouseItems.numDomainId=I.numDomainId
	where numItemID=',@numItemCode
	,' AND (WareHouseItems.numWarehouseItemID = ',ISNULL(@numWarehouseItemID,0),' OR ',ISNULL(@numWarehouseItemID,0),' = 0 )'    
	,' AND (WareHouseItems.numWareHouseID=',ISNULL(@numWarehouseID,0),' OR ',ISNULL(@numWarehouseID,0),' = 0)' 
	,' AND (WareHouseItems.numWLocationID=',ISNULL(@numWLocationID,0),' OR ',ISNULL(@numWLocationID,0),' = 0)')        
   
	print CAST(@str1 AS NTEXT)           

	EXECUTE sp_executeSQL @str1, N'@bitKitParent bit,@bitAssembly bit,@numSaleUOMFactor DECIMAL(28,14), @numPurchaseUOMFactor DECIMAL(28,14), @vcUnitName VARCHAR(100), @vcSaleUnitName VARCHAR(100), @vcPurchaseUnitName VARCHAR(100)', @bitKitParent, @bitAssembly, @numSaleUOMFactor, @numPurchaseUOMFactor, @vcUnitName, @vcSaleUnitName, @vcPurchaseUnitName
                       
                        
	set @str1='select numWareHouseItmsDTLID
	,WDTL.numWareHouseItemID
	,vcSerialNo
	,WDTL.vcComments as Comments
	,WDTL.numQty
	,WDTL.numQty as OldQty
	,ISNULL(W.vcWarehouse,'''') + (CASE WHEN ISNULL(WL.vcLocation,'''') = '''' THEN '''' ELSE '', '' + isnull(WL.vcLocation,'''') END) vcWarehouse '+ case when @bitSerialize=1 then @str else '' end +'
	,WDTL.dExpirationDate, WDTL.bitAddedFromPO
	from WareHouseItmsDTL WDTL                             
	join WareHouseItems                             
	on WDTL.numWareHouseItemID=WareHouseItems.numWareHouseItemID                              
	join Warehouses W                             
	on W.numWareHouseID=WareHouseItems.numWareHouseID  
	left join WarehouseLocation WL on WL.numWLocationID =WareHouseItems.numWLocationID
	where ISNULL(numQty,0) > 0 and numItemID='+ convert(varchar(15),@numItemCode) 
	+ ' AND (WareHouseItems.numWarehouseItemID = ' + CAST(@numWarehouseItemID AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@numWarehouseItemID,0) AS VARCHAR(20)) + ' = 0 )' 
	+ ' AND (WareHouseItems.numWareHouseID='+ CONVERT(VARCHAR(20),@numWarehouseID) + ' OR ' + CONVERT(VARCHAR(20),@numWarehouseID) + ' = 0)' 
	+ ' AND (WareHouseItems.numWLocationID='+ CONVERT(VARCHAR(20),@numWLocationID) + ' OR ' + CONVERT(VARCHAR(20),@numWLocationID) + ' = 0)'   
                         
	print CAST(@str1 AS NTEXT)                      
	exec (@str1)                       
                      
	IF @bitMatrix = 1
	BEGIN
		SELECT
			Fld_label
			,fld_id
			,fld_type
			,numlistid
			,vcURL
			,Fld_Value 
		FROM 
			#tempTableMatrix

		DROP TABLE #tempTableMatrix
	END
	ELSE
	BEGIN
		SELECT 
			Fld_label
			,fld_id
			,fld_type
			,numlistid
			,vcURL
			,Fld_Value 
		FROM 
			#tempTable                         
		JOIN 
			CFW_Fld_Master
		ON 
			numCusFlDItemID=Fld_ID                      

		DROP TABLE #tempTable
	END
END
GO            
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_InLineEditDataSave')
DROP PROCEDURE USP_InLineEditDataSave
GO
CREATE PROCEDURE [dbo].[USP_InLineEditDataSave] 
( 
 @numDomainID NUMERIC(18,0),
 @numUserCntID NUMERIC(18,0),
 @numFormFieldId NUMERIC(9),
 @bitCustom AS BIT = 0,
 @InlineEditValue VARCHAR(8000),
 @numContactID NUMERIC(9),
 @numDivisionID NUMERIC(9),
 @numWOId NUMERIC(9),
 @numProId NUMERIC(9),
 @numOppId NUMERIC(9),
 @numRecId NUMERIC(9),
 @vcPageName NVARCHAR(100),
 @numCaseId NUMERIC(9),
 @numWODetailId NUMERIC(9),
 @numCommID NUMERIC(9)
 )
AS 

declare @strSql as nvarchar(4000)
DECLARE @vcDbColumnName AS VARCHAR(100),@vcOrigDbColumnName AS VARCHAR(100),@vcLookBackTableName AS varchar(100)
DECLARE @vcListItemType as VARCHAR(3),@numListID AS numeric(9),@vcAssociatedControlType varchar(20)
declare @tempAssignedTo as numeric(9);set @tempAssignedTo=null           
DECLARE @Value1 AS VARCHAR(18),@Value2 AS VARCHAR(18)

IF  @bitCustom =0
BEGIN
	SELECT @vcDbColumnName=vcDbColumnName,@vcLookBackTableName=vcLookBackTableName,
		   @vcListItemType=vcListItemType,@numListID=numListID,@vcAssociatedControlType=vcAssociatedControlType,
		   @vcOrigDbColumnName=vcOrigDbColumnName
	 FROM DycFieldMaster WHERE numFieldId=@numFormFieldId
	 
	IF @vcAssociatedControlType = 'DateField' AND  @InlineEditValue=''
	BEGIN
		SET @InlineEditValue = null
	END

	DECLARE @intExecuteDiv INT=0
	if @vcLookBackTableName = 'DivisionMaster' 
	 BEGIN 
		
	   IF @vcDbColumnName='numFollowUpStatus' --Add to FollowUpHistory
	    Begin
			IF(@numCommID>0 AND @numDivisionID=0)
			BEGIN
				SET @numDivisionID=(SELECT TOP 1 numDivisionId FROM Communication WHERE numCommId=@numCommID)
			END
			declare @numFollow as varchar(10),@binAdded as varchar(20),@PreFollow as varchar(10),@RowCount as varchar(2)                            
			                      
			set @PreFollow=(select count(*)  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID)                          
			set @RowCount=@@rowcount  
			                        
			select   @numFollow=numFollowUpStatus, @binAdded=bintModifiedDate from divisionmaster where numDivisionID=@numDivisionID and numDomainId= @numDomainID                    
			
			if @numFollow <>'0' and @numFollow <> @InlineEditValue                          
			begin                          
				if @PreFollow<>0                          
					begin                          
			                   
						if @numFollow <> (select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID order by numFollowUpStatusID desc)                          
							begin                          
			                      	insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
									values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
							end                          
					end                          
				else                          
					begin                          
						insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
						 values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
					end                          
			   end 
			   
			
			UPDATE  DivisionMaster SET  dtLastFollowUp=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID
		END
		 IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from DivisionMaster where numDivisionID = @numDivisionID and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update DivisionMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID where numDivisionID = @numDivisionID  and numDomainId= @numDomainID  
						UPDATE CompanyInfo SET bintModifiedDate=GETUTCDATE() WHERE numCompanyId IN (SELECT numCompanyId 
																									FROM 
																										DivisionMaster
																									WHERE 
																										numDivisionID = @numDivisionID  and numDomainId= @numDomainID)         
					end					
				else if  (@InlineEditValue ='0')          
					begin          
						update DivisionMaster set numAssignedTo=0 ,numAssignedBy=0 where numDivisionID = @numDivisionID  and numDomainId= @numDomainID
						UPDATE CompanyInfo SET bintModifiedDate=GETUTCDATE() WHERE numCompanyId IN (SELECT numCompanyId 
																									FROM 
																										DivisionMaster
																									WHERE 
																										numDivisionID = @numDivisionID  and numDomainId= @numDomainID)          
					end    
			END
	    UPDATE CompanyInfo SET numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() WHERE numCompanyId=(SELECT TOP 1 numCompanyId FROM DivisionMaster where numDivisionID=@numDivisionID and numDomainID=@numDomainID) and numDomainID=@numDomainID
		
		IF @vcDbColumnName='vcPartnerCode'
		BEGIN
			IF(@InlineEditValue<>'')
			BEGIN
				IF EXISTS(SELECT vcPartnerCode FROM DivisionMaster where numDivisionID<>@numDivisionID and numDomainID=@numDomainID AND vcPartnerCode=@InlineEditValue)
				BEGIN
					SET @intExecuteDiv=1
					SET @InlineEditValue='-#12'
				END
			END
		END
		IF @intExecuteDiv=0
		BEGIN
			SET @strSql='update DivisionMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID'
			
		END
	 END 
	 ELSE if @vcLookBackTableName = 'AdditionalContactsInformation' 
	 BEGIN 
			IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='True'
			BEGIN
				DECLARE @bitPrimaryContactCheck AS BIT             
				DECLARE @numID AS NUMERIC(9) 
				
				SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
					FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID 
                   
					IF @@rowcount = 0 SET @numID = 0             
				
					WHILE @numID > 0            
					BEGIN            
						IF @bitPrimaryContactCheck = 1 
							UPDATE  AdditionalContactsInformation SET bitPrimaryContact = 0 WHERE numContactID = @numID  
                              
						SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
						FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactID > @numID    
                                
						IF @@rowcount = 0 SET @numID = 0             
					END 
				END
				ELSE IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='False'
				BEGIN
					IF (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactId!=@numContactId AND ISNULL(bitPrimaryContact,0)=1) = 0
						SET @InlineEditValue='True'
				END 
				-- Added by Priya(to check no Campaign got engaged wothout contacts email
				ELSE IF @vcDbColumnName = 'numECampaignID' 
				BEGIN
					
					IF EXISTS (select 1 from AdditionalContactsInformation WHERE numContactId = @numContactID AND (vcEmail IS NULL OR vcEmail = ''))
					BEGIN
						RAISERROR('EMAIL_ADDRESS_REQUIRED',16,1)
								RETURN
					END

				END
				
		SET @strSql='update AdditionalContactsInformation set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID and numContactId=@numContactId'
	 END 
	 ELSE if @vcLookBackTableName = 'CompanyInfo' 
	 BEGIN 
		SET @strSql='update CompanyInfo set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCompanyId=(select numCompanyId from divisionmaster where numDivisionID=@numDivisionID and numDomainID=@numDomainID) and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'WorkOrder' 
	 BEGIN 
		DECLARE @numItemCode AS NUMERIC(9)
	 
		IF @numWODetailId>0 AND @vcDbColumnName <> 'numQtyItemsReq'
		BEGIN
			SET @strSql='update WorkOrderDetails set ' + @vcDbColumnName + '=@InlineEditValue where numWOId=@numWOId and numWODetailId=@numWODetailId' 
		END 	
		ELSE
		BEGIN
			SET @strSql='update WorkOrder set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numWOId=@numWOId and numDomainID=@numDomainID'
			
			IF @vcDbColumnName='numQtyItemsReq'
			BEGIN 
				DECLARE @numPickedQty FLOAT = 0
				
				SET @numPickedQty = ISNULL((SELECT
												MAX(numPickedQty)
											FROM
											(
												SELECT 
													WorkOrderDetails.numWODetailId
													,CEILING(SUM(WorkOrderPickedItems.numPickedQty)/numQtyItemsReq_Orig) numPickedQty
												FROM
													WorkOrderDetails
												INNER JOIN
													WorkOrderPickedItems
												ON
													WorkOrderDetails.numWODetailId=WorkOrderPickedItems.numWODetailId
												WHERE
													WorkOrderDetails.numWOId=@numWOID
													AND ISNULL(WorkOrderDetails.numWareHouseItemId,0) > 0
												GROUP BY
													WorkOrderDetails.numWODetailId
													,WorkOrderDetails.numQtyItemsReq_Orig
											) TEMP),0)

				IF @InlineEditValue < ISNULL((@numPickedQty),0)
				BEGIN
					RAISERROR('WORKORDR_QUANITY_CAN_NOT_BE_LESS_THEN_PICKED_QTY',16,1)
					RETURN
				END

				IF EXISTS (SELECT 
								SPDTTL.ID 
							FROM 
								StagePercentageDetailsTask SPDT
							INNER JOIN
								StagePercentageDetailsTaskTimeLog SPDTTL
							ON
								SPDT.numTaskId = SPDTTL.numTaskID 
							WHERE 
								SPDT.numDomainID=@numDomainID 
								AND SPDT.numWorkOrderId = @numWOId)
				BEGIN
					RAISERROR('TASKS_ARE_ALREADY_STARTED',16,1)
					RETURN
				END

				IF EXISTS (SELECT numWOId FROM WorkOrder WHERE numDomainID=@numDomainID AND numWOId=@numWOId AND numWOStatus=23184)
				BEGIN
					RAISERROR('WORKORDR_COMPLETED',16,1)
					RETURN
				END

				SELECT @numItemCode=numItemCode FROM WorkOrder WHERE numWOId=@numWOId and numDomainID=@numDomainID
			
				EXEC USP_UpdateWorkOrderDetailQuantity @numDomainID,@numUserCntID,@numItemCode,@InlineEditValue,@numWOId
			END 
		END	
	 END
	 ELSE if @vcLookBackTableName = 'ProjectsMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if Project is assigned to someone 
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from ProjectsMaster where numProId=@numProId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update ProjectsMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numProId = @numProId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update ProjectsMaster set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numProId = @numProId  and numDomainId= @numDomainID        
					end    
			END
		else IF @vcDbColumnName='numProjectStatus'  ---Updating All Statge to 100% if Completed
			BEGIN
				IF @InlineEditValue='27492'
				BEGIN
					UPDATE PM SET dtCompletionDate=GETUTCDATE(),monTotalExpense=PIE.monExpense,
					monTotalIncome=PIE.monIncome,monTotalGrossProfit=PIE.monBalance,numProjectStatus=27492
					FROM ProjectsMaster PM,dbo.GetProjectIncomeExpense(@numDomainId,@numProId) PIE
					WHERE PM.numProId=@numProId
					
					EXEC USP_ManageProjectCommission @numDomainId,@numProId
						
					Update StagePercentageDetails set tinProgressPercentage=100,bitclose=1 where numProjectID=@numProId

					EXEC dbo.USP_GetProjectTotalProgress
							@numDomainID = @numDomainID, --  numeric(9, 0)
							@numProId = @numProId, --  numeric(9, 0)
							@tintMode = 1 --  tinyint
				END			
				ELSE
				BEGIN
					DELETE from BizDocComission WHERE numDomainID = @numDomainID AND numProId=@numProId AND numComissionID 
						NOT IN (SELECT BC.numComissionID FROM dbo.BizDocComission BC JOIN dbo.PayrollTracking PT 
							ON BC.numComissionID = PT.numComissionID AND BC.numDomainId = PT.numDomainId WHERE BC.numDomainID = @numDomainID AND ISNULL(BC.numProId,0) = @numProId)
				END
			END
			

		SET @strSql='update ProjectsMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numProId=@numProId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'OpportunityMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from OpportunityMaster where numOppId=@numOppId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update OpportunityMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update OpportunityMaster set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID        
					end    
			END
		ELSE IF @vcDbColumnName='numPartner' OR @vcDbColumnName='numPartenerSource'
		 BEGIN
			update OpportunityMaster set numPartner=@InlineEditValue ,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID           
		 END
		
		ELSE IF @vcDbColumnName='tintActive'
		 BEGIN
		 	SET @InlineEditValue= CASE @InlineEditValue WHEN 'True' THEN 1 ELSE 0 END 
		 END
		 ELSE IF @vcDbColumnName='tintSource'
		 BEGIN
			IF CHARINDEX('~', @InlineEditValue)>0
			BEGIN
				SET @Value1=SUBSTRING(@InlineEditValue, 1,CHARINDEX('~', @InlineEditValue)-1) 
				SET @Value2=SUBSTRING(@InlineEditValue, CHARINDEX('~', @InlineEditValue)+1,LEN(@InlineEditValue)) 
			END	
			ELSE
			BEGIN
				SET @Value1=@InlineEditValue
				SET @Value2=0
			END
			
		 	update OpportunityMaster set tintSourceType=@Value2 where numOppId = @numOppId  and numDomainId= @numDomainID        
		 	
		 	SET @InlineEditValue= @Value1
		 END
		ELSE IF @vcDbColumnName='tintOppStatus' --Update Inventory  0=Open,1=Won,2=Lost
		 BEGIN			
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT
		
			IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainId=@numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1)
			BEGIN
				SET @bitViolatePrice = 1
			END
			SET @intPendingApprovePending=(SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppId AND ISNULL(bitItemPriceApprovalRequired,0)=1)

			IF(@intPendingApprovePending > 0 AND @bitViolatePrice=1 AND @InlineEditValue='1')
			BEGIN
				SET @intExecuteDiv=1
				SET @InlineEditValue='-#123'
			END

			IF ISNULL(@intExecuteDiv,0)=0
			BEGIN
				DECLARE @tintOppStatus AS TINYINT
				SELECT @tintOppStatus=tintOppStatus FROM OpportunityMaster WHERE numOppID=@numOppID              

				IF @tintOppStatus=0 AND @InlineEditValue='1' --Open to Won
				BEGIN
					SELECT @numDivisionID=numDivisionID FROM OpportunityMaster WHERE numOppID=@numOppID   
					
					DECLARE @tintCRMType AS TINYINT      
					SELECT @tintCRMType=tintCRMType FROM divisionmaster WHERE numDivisionID =@numDivisionID 

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN        
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=GETUTCDATE()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=GETUTCDATE()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN        
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=GETUTCDATE()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END    

		 			EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID

					UPDATE OpportunityMaster SET bintOppToOrder=GETUTCDATE() WHERE numOppId=@numOppId and numDomainID=@numDomainID
				END
				ELSE IF (@InlineEditValue='2') -- Win to Lost
				BEGIN
					UPDATE OpportunityMaster SET dtDealLost=GETUTCDATE() WHERE numOppId=@numOppId and numDomainID=@numDomainID
				END
				
				IF (@tintOppStatus=1 and @InlineEditValue='2') or (@tintOppStatus=1 and @InlineEditValue='0') --Won to Lost or Won to Open
						EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID 

				UPDATE OpportunityMaster SET tintOppStatus = @InlineEditValue WHERE numOppId=@numOppID
				
			END
		 END
		 ELSE IF @vcDbColumnName='vcOppRefOrderNo' --Update vcRefOrderNo OpportunityBizDocs
		 BEGIN
			 IF LEN(ISNULL(@InlineEditValue,''))>0
			 BEGIN
	   			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@InlineEditValue WHERE numOppId=@numOppID
			 END
		 END
		 ELSE IF @vcDbColumnName='numStatus' 
		 BEGIN
			DECLARE @tintOppType AS TINYINT
			select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType from OpportunityMaster where numOppID=@numOppID

			IF @tintOppType=1 AND @tintOppStatus=1
			BEGIN
				DECLARE @numOppStatus AS NUMERIC(18,0) = CAST(@InlineEditValue AS NUMERIC(18,0))
				EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numOppStatus
			END

		 	 IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @InlineEditValue)
			 BEGIN
	   				EXEC USP_ManageOpportunityAutomationQueue
							@numOppQueueID = 0, -- numeric(18, 0)
							@numDomainID = @numDomainID, -- numeric(18, 0)
							@numOppId = @numOppID, -- numeric(18, 0)
							@numOppBizDocsId = 0, -- numeric(18, 0)
							@numOrderStatus = @InlineEditValue, -- numeric(18, 0)
							@numUserCntID = @numUserCntID, -- numeric(18, 0)
							@tintProcessStatus = 1, -- tinyint
							@tintMode = 1 -- TINYINT
			END 
		 END	
		 ELSE IF @vcDbColumnName = 'dtReleaseDate'
		 BEGIN
			IF EXISTS (SELECT 1 FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND tintOppType=1 AND tintOppStatus=0)
			BEGIN
				IF (SELECT dbo.GetListIemName(ISNULL(numPercentageComplete,0)) FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId) = '100'
				BEGIN
					SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), @InlineEditValue),numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
				END
				ELSE
				BEGIN
					RAISERROR('PERCENT_COMPLETE_MUST_BE_100',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), @InlineEditValue),numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
			END
		END
		ELSE IF @vcDbColumnName = 'numPercentageComplete'
		BEGIN
			IF EXISTS (SELECT 1 FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND tintOppType=1 AND tintOppStatus=0 AND dbo.GetListIemName(ISNULL(numPercentageComplete,0))='100')
			BEGIN
				IF EXISTS(SELECT dtReleaseDate FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND dtReleaseDate IS NULL)
				BEGIN
					SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
				END
				ELSE
				BEGIN
					RAISERROR('REMOVE_RELEASE_DATE',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
			END
		END
		ELSE IF @vcDbColumnName = 'numProjectID'
		BEGIN
			UPDATE OpportunityItems SET numProjectID=@InlineEditValue WHERE numOppId=@numOppId
		END

		IF(@vcDbColumnName!='tintOppStatus')
		BEGIN
			SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
		END
	 END
	 ELSE if @vcLookBackTableName = 'Cases' 
	 BEGIN 
	 		IF @vcDbColumnName='numAssignedTo' ---Updating if Case is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from Cases where numCaseId=@numCaseId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update Cases set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numCaseId = @numCaseId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update Cases set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numCaseId = @numCaseId  and numDomainId= @numDomainID        
					end    
			END
		UPDATE Cases SET bintModifiedDate=GETUTCDATE() WHERE numCaseId = @numCaseId  and numDomainId= @numDomainID    
		SET @strSql='update Cases set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCaseId=@numCaseId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'Tickler' 
	 BEGIN 	
		SET @strSql='update Communication set ' + @vcOrigDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate() where numCommId=@numCommId and numDomainID=@numDomainID'
	 END
	 ELSE IF @vcLookBackTableName = 'ExtarnetAccounts'
	 BEGIN
		IF @vcDbColumnName='bitEcommerceAccess'
		BEGIN
			IF LOWER(@InlineEditValue) = 'yes' OR LOWER(@InlineEditValue) = 'true'
			BEGIN
				IF NOT EXISTS (SELECT * FROM ExtarnetAccounts where numDivisionID=@numDivisionID) 
				BEGIN
					DECLARE @numGroupID NUMERIC
					DECLARE @numCompanyID NUMERIC(18,0)
					SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionID

					SELECT TOP 1 @numGroupID=numGroupID FROM dbo.AuthenticationGroupMaster WHERE numDomainID=@numDomainID AND tintGroupType=2
				
					IF @numGroupID IS NULL SET @numGroupID = 0 
					INSERT INTO ExtarnetAccounts (numCompanyID,numDivisionID,numGroupID,numDomainID)                                                
					VALUES(@numCompanyID,@numDivisionID,@numGroupID,@numDomainID)
				END
			END
			ELSE
			BEGIN
				DELETE FROM ExtranetAccountsDtl WHERE numExtranetID IN (SELECT ISNULL(numExtranetID,0) FROM ExtarnetAccounts where numDivisionID=@numDivisionID)
				DELETE FROM ExtarnetAccounts where numDivisionID=@numDivisionID
			END

			SELECT (CASE WHEN LOWER(@InlineEditValue) = 'yes' OR LOWER(@InlineEditValue) = 'true' THEN 'Yes' ELSE 'No' END) AS vcData

			RETURN
		END
	 END
	ELSE IF @vcLookBackTableName = 'ExtranetAccountsDtl'
	BEGIN
		IF @vcDbColumnName='vcPassword'
		BEGIN
			IF LEN(@InlineEditValue) = 0
			BEGIN
				DELETE FROM ExtranetAccountsDtl WHERE numContactID=@numContactID
			END
			ELSE
			BEGIN
				IF EXISTS (SELECT numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID)
				BEGIN
					IF EXISTS (SELECT numExtranetDtlID FROM ExtranetAccountsDtl WHERE numContactID=@numContactID)
					BEGIN
						UPDATE
							ExtranetAccountsDtl
						SET 
							vcPassword=@InlineEditValue
							,tintAccessAllowed=1
						WHERE
							numContactID=@numContactID
					END
					ELSE
					BEGIN
						DECLARE @numExtranetID NUMERIC(18,0)
						SELECT @numExtranetID=numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID

						INSERT INTO ExtranetAccountsDtl (numExtranetID,numContactID,vcPassword,tintAccessAllowed,numDomainID)
						VALUES (@numExtranetID,@numContactID,@InlineEditValue,1,@numDomainID)
					END
				END
				ELSE
				BEGIN
					RAISERROR('Organization e-commerce access is not enabled',16,1)
				END
			END

			SELECT @InlineEditValue AS vcData

			RETURN
		END
	END
	ELSE IF @vcLookBackTableName = 'CustomerPartNumber'
	BEGIN
		IF @vcDbColumnName='CustomerPartNo'
		BEGIN
			SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionID
			
			IF EXISTS(SELECT CustomerPartNoID FROM CustomerPartNumber WHERE numItemCode = @numRecId AND numCompanyID = @numCompanyID)
			BEGIN
				UPDATE CustomerPartNumber SET CustomerPartNo = @InlineEditValue WHERE numItemCode = @numRecId AND numCompanyId = @numCompanyID AND numDomainID = @numDomainId 
			END
			ELSE
			BEGIN
				INSERT INTO CustomerPartNumber
				(numItemCode, numCompanyId, numDomainID, CustomerPartNo)
				VALUES
				(@numRecId, @numCompanyID, @numDomainID, @InlineEditValue)
			END

			SELECT @InlineEditValue AS vcData

			RETURN
		END
		
	END
	ELSE IF @vcLookBackTableName = 'CheckHeader'
	BEGIN
		IF EXISTS (SELECT 1 FROM CheckHeader WHERE numDomainID=@numDomainID AND numCheckNo=@InlineEditValue AND numCheckHeaderID <> @numRecId)
		BEGIN
			RAISERROR('DUPLICATE_CHECK_NUMBER',16,1)
			RETURN
		END

		SET @strSql='UPDATE CheckHeader SET ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate() WHERE numCheckHeaderID=@numRecId and numDomainID=@numDomainID'
	END
	--ELSE IF @vcLookBackTableName = 'Item'
	--BEGIN
	--	IF @vcDbColumnName = 'monListPrice'
	--	BEGIN
	--		IF EXISTS (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numRecId AND 1 = (CASE WHEN ISNULL(numItemGroup,0) > 0 THEN (CASE WHEN ISNULL(bitMatrix,0)=1 THEN 1 ELSE 0 END) ELSE 1 END))
	--		BEGIN
	--			UPDATE WareHouseItems SET monWListPrice=@InlineEditValue WHERE numDomainID=@numDomainID AND numItemID=@numRecId
	--			UPDATE Item SET monListPrice=@InlineEditValue WHERE numDomainID=@numDomainID AND numItemCode=@numRecId

	--			SELECT @InlineEditValue AS vcData
	--		END
	--		ELSE
	--		BEGIN
	--			RAISERROR('You are not allowed to edit list price for warehouse level matrix item.',16,1)
	--		END

	--		RETURN
	--	END

	--	SET @strSql='UPDATE Item set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numItemCode=@numRecId AND numDomainID=@numDomainID'
	--END

	EXECUTE sp_executeSQL @strSql, N'@numDomainID NUMERIC(9),@numUserCntID NUMERIC(9),@numDivisionID NUMERIC(9),@numContactID NUMERIC(9),@InlineEditValue VARCHAR(8000),@numWOId NUMERIC(9),@numProId NUMERIC(9),@numOppId NUMERIC(9),@numCaseId NUMERIC(9),@numWODetailId NUMERIC(9),@numCommID Numeric(9),@numRecId NUMERIC(18,0)',
		     @numDomainID,@numUserCntID,@numDivisionID,@numContactID,@InlineEditValue,@numWOId,@numProId,@numOppId,@numCaseId,@numWODetailId,@numCommID,@numRecId;
		     
	IF @vcAssociatedControlType='SelectBox'                                                    
       BEGIN       
			IF @vcDbColumnName='tintSource' AND @vcLookBackTableName = 'OpportunityMaster'
			BEGIN
				SELECT dbo.fn_GetOpportunitySourceValue(@Value1,@Value2,@numDomainID) AS vcData                                             
			END
			ELSE IF @vcDbColumnName='numContactID'
			BEGIN
				SELECT dbo.fn_GetContactName(@InlineEditValue)
			END
			ELSE IF @vcDbColumnName='numPartner' OR @vcDbColumnName='numPartenerSource'
			BEGIN
				SET @intExecuteDiv=1
				SELECT D.vcPartnerCode+'-'+C.vcCompanyName  AS vcData FROM DivisionMaster AS D 
				LEFT JOIN CompanyInfo AS C
				ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=@InlineEditValue
			END
			
			ELSE IF @vcDbColumnName='tintOppStatus' AND @intExecuteDiv=1
			BEGIN
				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numReleaseStatus'
			BEGIN
				If @InlineEditValue = '1'
				BEGIN
					SELECT 'Open'
				END
				ELSE IF @InlineEditValue = '2'
				BEGIN
					SELECT 'Purchased'
				END
				ELSE
				BEGIN
					SELECT ''
				END
			END
			ELSE IF @vcDbColumnName='numPartenerContact'
			BEGIN
				SET @InlineEditValue=(SELECT TOP 1 A.vcGivenName FROM AdditionalContactsInformation AS A 
										LEFT JOIN DivisionMaster AS D 
										ON D.numDivisionID=A.numDivisionId
										WHERE D.numDomainID=@numDomainID AND A.numCOntactId=@InlineEditValue )
				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numPartenerSource'
			BEGIN
				
				SET @InlineEditValue=(SELECT D.vcPartnerCode+'-'+C.vcCompanyName  AS vcData FROM DivisionMaster AS D 
				LEFT JOIN CompanyInfo AS C
				ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=@InlineEditValue)

				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numShippingService'
			BEGIN
				SELECT ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = @InlineEditValue),'') AS vcData
			END
			else
			BEGIN
				SELECT dbo.fn_getSelectBoxValue(@vcListItemType,@InlineEditValue,@vcDbColumnName) AS vcData                                             
			END
		end 
		ELSE
		BEGIN
			 IF @vcDbColumnName='tintActive'
		  	 BEGIN
			 	SET @InlineEditValue= CASE @InlineEditValue WHEN 1 THEN 'True' ELSE 'False' END 
			 END

			IF @vcAssociatedControlType = 'Check box' or @vcAssociatedControlType = 'Checkbox'
			BEGIN
	 			SET @InlineEditValue= Case @InlineEditValue WHEN 'True' THEN 'Yes' ELSE 'No' END
			END
			
		    SELECT @InlineEditValue AS vcData
		END  
		
	IF @vcLookBackTableName = 'AdditionalContactsInformation' AND @vcDbColumnName = 'numECampaignID'
	BEGIN
		--Manage ECampiagn When Campaign is changed from inline edit
		DECLARE @Date AS DATETIME 
		SELECT @Date = dbo.[GetUTCDateWithoutTime]()
		DECLARE @numECampaignID AS NUMERIC(18,0)
		SET @numECampaignID = @InlineEditValue

		IF @numECampaignID = -1 
		SET @numECampaignID=0
	 
 		EXEC [USP_ManageConEmailCampaign]
		@numConEmailCampID = 0, --  numeric(9, 0)
		@numContactID = @numContactID, --  numeric(9, 0)
		@numECampaignID = @numECampaignID, --  numeric(9, 0)
		@intStartDate = @Date, --  datetime
		@bitEngaged = 1, --  bit
		@numUserCntID = @numUserCntID --  numeric(9, 0)
	END  
END
ELSE
BEGIN	 

	SET @InlineEditValue=Cast(@InlineEditValue as varchar(1000))
	SELECT @vcAssociatedControlType=Fld_type
	 FROM CFW_Fld_Master WHERE Fld_Id=@numFormFieldId 

	IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	    SET @InlineEditValue=(case when @InlineEditValue='False' then 0 ELSE 1 END)
	END

	IF @vcPageName='organization'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
  
	  END
	ELSE IF @vcPageName='contact'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Cont SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Cont (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END
			
			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Cont_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='project'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Pro SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Pro (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Pro_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END
	ELSE IF @vcPageName='opp'
	BEGIN 
		DECLARE @dtDate AS DATETIME = GETUTCDATE()

		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE 
				CFW_Fld_Values_Opp 
			SET 
				Fld_Value=@InlineEditValue ,
				numModifiedBy = @numUserCntID,
				bintModifiedDate = @dtDate
			WHERE 
				FldDTLID= (SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_Fld_Values_Opp (Fld_ID,Fld_Value,RecId,numModifiedBy,bintModifiedDate) VALUES (@numFormFieldId,@InlineEditValue,@numRecId,@numUserCntID,@dtDate)         
		END

		UPDATE OpportunityMaster SET numModifiedBy=@numUserCntID,bintModifiedDate=@dtDate WHERE numOppID = @numRecId 

		--Added By :Sachin Sadhu ||Date:14thOct2014
		--Purpose  :To manage CustomFields in Queue
		EXEC USP_CFW_Fld_Values_Opp_CT
				@numDomainID =@numDomainID,
				@numUserCntID =@numUserCntID,
				@numRecordID =@numRecId ,
				@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='oppitems'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_OppItems CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE 
				CFW_Fld_Values_OppItems 
			SET 
				Fld_Value=@InlineEditValue
			WHERE 
				FldDTLID= (SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_OppItems CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_Fld_Values_OppItems(Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
		END
	END
	ELSE IF @vcPageName='case'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Case SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Case (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Case_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END
	ELSE IF @vcPageName='item'
	BEGIN
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Item CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE CFW_FLD_Values_Item SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Item CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_FLD_Values_Item (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
		END
	END


	IF @vcAssociatedControlType = 'SelectBox'            
	BEGIN            
		    
		 SELECT vcData FROM ListDetails WHERE numListItemID=@InlineEditValue            
	END
	ELSE IF @vcAssociatedControlType = 'CheckBoxList'
	BEGIN
		DECLARE @str AS VARCHAR(MAX)
		SET @str = 'SELECT STUFF((SELECT CONCAT('','', vcData) FROM ListDetails WHERE numlistitemid IN (' + (CASE WHEN LEN(@InlineEditValue) > 0 THEN @InlineEditValue ELSE '0' END) + ') FOR XML PATH('''')), 1, 1, '''')'
		EXEC(@str)
	END
	ELSE IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	 	select CASE @InlineEditValue WHEN 1 THEN 'Yes' ELSE 'No' END AS vcData
	END
	ELSE
	BEGIN
		SELECT @InlineEditValue AS vcData
	END 
END

/****** Object:  StoredProcedure [dbo].[usp_InsertCommunication]    Script Date: 07/26/2008 16:19:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified By Anoop jayaraj                 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertcommunication')
DROP PROCEDURE usp_insertcommunication
GO
CREATE PROCEDURE [dbo].[usp_InsertCommunication]                                                
@numCommId numeric=0,                            
@bitTask numeric,                                        
@numContactId numeric,                            
@numDivisionId numeric,                            
@txtDetails text,                            
@numOppId numeric=0,                            
@numAssigned numeric=0,                                                
@numUserCntID numeric,                                                                                                                                               
@numDomainId numeric,                                                
@bitClosed tinyint,                                                
@vcCalendarName varchar(100)='',                                                                                                     
@dtStartTime datetime,                 
@dtEndtime datetime,                                                                             
@numActivity as numeric(9),                                              
@numStatus as numeric(9),                                              
@intSnoozeMins as int,                                              
@tintSnoozeStatus as tinyint,                                            
@intRemainderMins as int,                                            
@tintRemStaus as tinyint,                                  
@ClientTimeZoneOffset Int,                  
@bitOutLook tinyint,            
@bitSendEmailTemplate bit,            
@bitAlert bit,            
@numEmailTemplate numeric(9)=0,            
@tintHours tinyint=0,          
@CaseID  numeric=0 ,        
@CaseTimeId numeric =0,    
@CaseExpId numeric = 0  ,                     
@ActivityId numeric = 0 ,
@bitFollowUpAnyTime BIT,
@strAttendee TEXT,
@numLinkedOrganization NUMERIC(18,0) = 0,
@numLinkedContact NUMERIC(18,0) = 0,
@strContactIds VARCHAR(500)=''
--                                                
AS                       
--	IF @numDivisionId = 0                         
--	BEGIN
--		RAISERROR('COMPANY_NOT_FOUND',16,1)
--		RETURN
--	END
--  
--	IF @numContactId = 0 
--	BEGIN
--		RAISERROR('CONTACT_NOT_FOUND',16,1)
--		RETURN
--	END           
    
	declare @CheckRecord as bit   
	set @CheckRecord = 1                

  if not exists (select * from communication  WHERE numcommid=@numCommId   )                                                
	set @CheckRecord = 0                                                

  IF @numCommId = 0    or @CheckRecord = 0                                            

  BEGIN
	IF(@strContactIds='')
	BEGIN
	IF ISNULL(@numDivisionId,0) = 0                         
		BEGIN
			IF ISNULL(@numContactId,0) > 0
				BEGIN
					SELECT @numDivisionId = numDivisionId FROM AdditionalContactsInformation WHERE numContactId = @numContactId
					
					IF ISNULL(@numDivisionId,0) = 0 
					BEGIN
						RAISERROR('COMPANY_NOT_FOUND_FOR_CONTACT' ,16,1)
						RETURN
					END                   
				END
			ELSE
				BEGIN
					RAISERROR('COMPANY_NOT_FOUND',16,1)
					RETURN	
				END
		END
  
	IF ISNULL(@numContactId,0) = 0 
		BEGIN
			RAISERROR('CONTACT_NOT_FOUND',16,1)
			RETURN
		END   
	END         
   IF @numAssigned=0  SET @numAssigned=@numUserCntID                                                
    IF(@strContactIds='')
	BEGIN                                         
		Insert into communication                  
   (                
   bitTask,                
   numContactId,                
   numDivisionId,                
   textDetails,                
   intSnoozeMins,                
   intRemainderMins,                
   numStatus,                
   numActivity,                
   numAssign,                
   tintSnoozeStatus,                
   tintRemStatus,                
   numOppId,                
   numCreatedby,                
   dtCreatedDate,                
   numModifiedBy,                
   dtModifiedDate,                
   numDomainID,                
   bitClosedFlag,                
   vcCalendarName,                
   bitOutlook,                
   dtStartTime,                
   dtEndTime,              
   numAssignedBy,            
   bitSendEmailTemp,            
   numEmailTemplate,            
   tintHours,            
   bitAlert,          
CaseId,        
CaseTimeId,                                                                                 
CaseExpId,  
numActivityId ,bitFollowUpAnyTime            
   )                 
 values                
   (                                              
   @bitTask,                
   @numcontactId,                                              
   @numDivisionId,                
   ISNULL(@txtdetails,''),                
   @intSnoozeMins,                
   @intRemainderMins,                
   @numStatus,                
   @numActivity,                                            
   @numAssigned,                
   @tintSnoozeStatus,                
   @tintRemStaus,                                             
   @numoppid,                
   @numUserCntID,                
   getutcdate(),                
   @numUserCntID,                                              
   getutcdate(),                
   @numDomainId,                
   @bitClosed,                
   @vcCalendarName,                
   @bitOutLook,                                              
   DateAdd(minute, @ClientTimeZoneOffset, @dtStartTime),                
   DateAdd(minute, @ClientTimeZoneOffset,@dtEndtime),              
   @numUserCntID,            
   @bitSendEmailTemplate,            
   @numEmailTemplate,            
   @tintHours,            
   @bitAlert,          
@CaseID,        
@CaseTimeId,                                                                                 
@CaseExpId,  
@ActivityId ,@bitFollowUpAnyTime 
 )  
	END
	ELSE
	BEGIN
		Insert into communication                  
		   (                
		   bitTask,                
		   numContactId,                
		   numDivisionId,                
		   textDetails,                
		   intSnoozeMins,                
		   intRemainderMins,                
		   numStatus,                
		   numActivity,                
		   numAssign,                
		   tintSnoozeStatus,                
		   tintRemStatus,                
		   numOppId,                
		   numCreatedby,                
		   dtCreatedDate,                
		   numModifiedBy,                
		   dtModifiedDate,                
		   numDomainID,                
		   bitClosedFlag,                
		   vcCalendarName,                
		   bitOutlook,                
		   dtStartTime,                
		   dtEndTime,              
		   numAssignedBy,            
		   bitSendEmailTemp,            
		   numEmailTemplate,            
		   tintHours,            
		   bitAlert,          
		CaseId,        
		CaseTimeId,                                                                                 
		CaseExpId,  
		numActivityId ,bitFollowUpAnyTime            
		   )                 
		 SELECT                                          
		   @bitTask,                
		   CAST(Items AS NUMERIC(9)),                                              
		   (SELECT TOP 1 numDivisionId FROM AdditionalContactsInformation WHERE numContactId=CAST(Items AS NUMERIC(9))),                
		   ISNULL(@txtdetails,''),                
		   @intSnoozeMins,                
		   @intRemainderMins,                
		   @numStatus,                
		   @numActivity,                                            
		   @numAssigned,                
		   @tintSnoozeStatus,                
		   @tintRemStaus,                                             
		   @numoppid,                
		   @numUserCntID,                
		   getutcdate(),                
		   @numUserCntID,                                              
		   getutcdate(),                
		   @numDomainId,                
		   @bitClosed,                
		   @vcCalendarName,                
		   @bitOutLook,                                              
		   DateAdd(minute, @ClientTimeZoneOffset, @dtStartTime),                
		   DateAdd(minute, @ClientTimeZoneOffset,@dtEndtime),              
		   @numUserCntID,            
		   @bitSendEmailTemplate,            
		   @numEmailTemplate,            
		   @tintHours,            
		   @bitAlert,          
		@CaseID,        
		@CaseTimeId,                                                                                 
		@CaseExpId,  
		@ActivityId ,@bitFollowUpAnyTime 
		 FROM dbo.Split(@strContactIds,',') WHERE Items<>'' AND Items<>'0'
	END                                                           
   set @numCommId= SCOPE_IDENTITY() 
   
	IF ISNULL(@numLinkedOrganization,0) > 0
	BEGIN
		INSERT INTO CommunicationLinkedOrganization VALUES (@numCommId,@numLinkedOrganization,@numLinkedContact)
	END
                                                    
  END                                                
 ELSE                    
  BEGIN                
    ---Updating if Action Item is assigned to someone                      
  declare @tempAssignedTo as numeric(9)                    
  set @tempAssignedTo=null                     
  select @tempAssignedTo=isnull(numAssign,0) from communication where numcommid=@numCommId                     
print @tempAssignedTo                    
  if (@tempAssignedTo<>@numAssigned and  @numAssigned<>'0')                    
  begin                      
    update communication set numAssignedBy=@numUserCntID where numcommid=@numCommId              
  end                     
  else if  (@numAssigned =0)                    
  begin                    
   update communication set numAssignedBy=0 where numcommid=@numCommId                  
  end                
              
              
                                              
    if @vcCalendarName=''                                                 
    begin                                                
      UPDATE communication SET                 
    bittask=@bittask,                                                                              
      textdetails=@txtdetails,                                               
      intSnoozeMins=@intSnoozeMins,                                               
      numStatus=@numStatus,                                              
      numActivity=@numActivity,                                             
      tintSnoozeStatus=@tintSnoozeStatus,                                            
      intRemainderMins= @intRemainderMins,                                            
      tintRemStatus= @tintRemStaus,                                              
      numassign=@numAssigned,                                                
      nummodifiedby=@numUserCntID,                                                
      dtModifiedDate=getutcdate(),                                                
      bitClosedFlag=@bitClosed,                                                        
      bitOutLook=@bitOutLook,                
    dtStartTime=DateAdd(minute, @ClientTimeZoneOffset, @dtStartTime),                
    dtEndTime=DateAdd(minute, @ClientTimeZoneOffset,@dtEndtime),            
    bitSendEmailTemp=@bitSendEmailTemplate,            
    numEmailTemplate=@numEmailTemplate,            
    tintHours=@tintHours,            
    bitAlert=@bitAlert,       
CaseID=@CaseID,        
CaseTimeId=@CaseTimeId,                                                                                 
CaseExpId=@CaseExpId  ,  
numActivityId=@ActivityId ,bitFollowUpAnyTime=@bitFollowUpAnyTime,
dtEventClosedDate=Case When isnull(@bitClosed,0)=1 then getutcdate() else null end                                 
      WHERE numcommid=@numCommId                                                
    END                                                
    ELSE                                                
    BEGIN                                                
       UPDATE communication SET                 
    bittask=@bittask,                                                                              
      textdetails=@txtdetails,                                               
      intSnoozeMins=@intSnoozeMins,                                
      numStatus=@numStatus,                                              
      numActivity=@numActivity,                                             
      tintSnoozeStatus=@tintSnoozeStatus,                                            
      intRemainderMins= @intRemainderMins,                                            
      tintRemStatus= @tintRemStaus,        
      numassign=@numAssigned,                                                
      nummodifiedby=@numUserCntID,                                                
      dtModifiedDate=getutcdate(),                                                
      bitClosedFlag=@bitClosed,                                                        
      bitOutLook=@bitOutLook,                
    dtStartTime=DateAdd(minute, @ClientTimeZoneOffset, @dtStartTime),                
    dtEndTime=DateAdd(minute, @ClientTimeZoneOffset,@dtEndtime),                                                 
     vcCalendarName=@vcCalendarName,            
      bitSendEmailTemp=@bitSendEmailTemplate,            
    numEmailTemplate=@numEmailTemplate,            
    tintHours=@tintHours,            
    bitAlert=@bitAlert ,      
CaseID=@CaseID,        
CaseTimeId=@CaseTimeId,                                                                                 
CaseExpId=@CaseExpId  ,  
numActivityId=@ActivityId  ,bitFollowUpAnyTime=@bitFollowUpAnyTime,
dtEventClosedDate=Case When isnull(@bitClosed,0)=1 then getutcdate() else null end 
   WHERE numcommid=@numCommId                                                
   END  
   
	IF ISNULL(@numLinkedOrganization,0) > 0
	BEGIN
		IF EXISTS (SELECT numCLOID FROM CommunicationLinkedOrganization WHERE numCommID=@numCommId)
		BEGIN
			UPDATE CommunicationLinkedOrganization SET numDivisionID=@numLinkedOrganization,numContactID=@numLinkedContact WHERE numCommID=@numCommId
		END
		ELSE
		BEGIN
			INSERT INTO CommunicationLinkedOrganization VALUES (@numCommId,@numLinkedOrganization,@numLinkedContact)
		END
	END
	ELSE
	BEGIN
		DELETE FROM CommunicationLinkedOrganization WHERE numCommID=@numCommId
	END
                                                 
END 

 DECLARE @hDocItem int                                                                                                                            
  IF convert(varchar(10),@strAttendee) <>''                                                                          
  BEGIN                      
      EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strAttendee  
      
      SELECT numContactID,ActivityID INTO #tempAttendees FROM OPENXML (@hDocItem,'/NewDataSet/AttendeeTable',2)                                                                          
			   WITH  (numContactID NUMERIC(9),ActivityID NUMERIC(9))
      
      delete from CommunicationAttendees where numCommId=@numCommId and numContactId not in                       
			   (SELECT numContactID from #tempAttendees)   
	
		 SELECT * FROM #tempAttendees
		 
	  INSERT INTO CommunicationAttendees (numCommId,numContactId,ActivityID)  
		  SELECT @numCommId,numContactId,ActivityID FROM #tempAttendees WHERE numContactId NOT IN(SELECT numContactId
		  FROM CommunicationAttendees WHERE numCommId=@numCommId)  
		  
	DROP TABLE #tempAttendees	   
  END
   
select @numCommId
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManagegeReportDashboard')
DROP PROCEDURE USP_ManagegeReportDashboard
GO
CREATE PROCEDURE [dbo].[USP_ManagegeReportDashboard]
@numDomainID AS NUMERIC(18,0),
@numDashBoardID AS NUMERIC(18,0),
@numReportID AS NUMERIC(18,0),
@numUserCntID AS NUMERIC(9)=0,
@tintReportType AS TINYINT,
@tintChartType AS TINYINT,
@vcHeaderText AS VARCHAR(100)='',
@vcFooterText AS VARCHAR(100)='',
@vcXAxis AS VARCHAR(50),
@vcYAxis AS VARCHAR(50),
@vcAggType AS VARCHAR(50),
@tintReportCategory AS TINYINT,
@numDashboardTemplateID NUMERIC(18,0),
@vcTimeLine VARCHAR(50)
,@vcGroupBy VARCHAR(50)
,@vcTeritorry VARCHAR(MAX)
,@vcFilterBy TINYINT
,@vcFilterValue VARCHAR(MAX)
,@dtFromDate DATE
,@dtToDate DATE
,@numRecordCount INT
,@tintControlField INT
,@vcDealAmount VARCHAR(MAX)
,@tintOppType INT
,@lngPConclAnalysis VARCHAR(MAX)
,@bitTask VARCHAR(MAX)
,@tintTotalProgress INT
,@tintMinNumber INT
,@tintMaxNumber INT
,@tintQtyToDisplay INT
,@vcDueDate VARCHAR(MAX)
,@bitDealWon bit
,@bitGrossRevenue bit
,@bitRevenue bit
,@bitGrossProfit bit
,@bitCustomTimeline bit
,@bitBilledButNotReceived bit 
,@bitReceviedButNotBilled bit
,@bitSoldButNotShipped bit
AS
BEGIN
	IF @numDashBoardID=0 
	BEGIN
		DECLARE @tintRow AS TINYINT;SET @tintRow=0
		SELECT @tintRow = ISNULL(MAX(tintRow),0) + 1 FROM ReportDashboard WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID AND ISNULL(numDashboardTemplateID,0)=ISNULL(@numDashboardTemplateID,0)
		
		INSERT INTO ReportDashboard 
		(
			numDomainID
			,numReportID
			,numUserCntID
			,tintReportType
			,tintChartType
			,vcHeaderText
			,vcFooterText
			,tintRow
			,tintColumn
			,vcXAxis
			,vcYAxis
			,vcAggType
			,tintReportCategory
			,intHeight
			,intWidth
			,numDashboardTemplateID
			,bitNewAdded
			,vcTimeLine
			,vcGroupBy
			,vcTeritorry
			,vcFilterBy
			,vcFilterValue
			,dtFromDate
			,dtToDate
			,numRecordCount
			,tintControlField
			,vcDealAmount
			,tintOppType
			,lngPConclAnalysis
			,bitTask
			,tintTotalProgress
			,tintMinNumber
			,tintMaxNumber
			,tintQtyToDisplay
			,vcDueDate
			,bitDealWon
			,bitGrossRevenue
			,bitRevenue
			,bitGrossProfit
			,bitCustomTimeline
			,bitBilledButNotReceived  
			,bitReceviedButNotBilled
			,bitSoldButNotShipped
		)
		VALUES
		(
			@numDomainID
			,@numReportID
			,@numUserCntID
			,@tintReportType
			,@tintChartType
			,@vcHeaderText
			,@vcFooterText
			,@tintRow
			,1
			,@vcXAxis
			,@vcYAxis
			,@vcAggType
			,@tintReportCategory
			,7
			,8
			,@numDashboardTemplateID
			,1
			,@vcTimeLine
			,@vcGroupBy
			,@vcTeritorry
			,@vcFilterBy
			,@vcFilterValue
			,@dtFromDate
			,@dtToDate
			,@numRecordCount
			,@tintControlField
			,@vcDealAmount
			,@tintOppType
			,@lngPConclAnalysis
			,@bitTask
			,@tintTotalProgress
			,@tintMinNumber
			,@tintMaxNumber
			,@tintQtyToDisplay
			,@vcDueDate
			,@bitDealWon
			,@bitGrossRevenue
			,@bitRevenue
			,@bitGrossProfit
			,@bitCustomTimeline
			,@bitBilledButNotReceived 
			,@bitReceviedButNotBilled
			,@bitSoldButNotShipped
		)
	END
	ELSE IF @numDashBoardID>0
	BEGIN
		UPDATE 
			ReportDashboard 
		SET
			numReportID=@numReportID, 
			tintReportType=@tintReportType, 
			tintChartType=@tintChartType, 
			vcHeaderText=@vcHeaderText, 
			vcFooterText=@vcFooterText,
			vcXAxis=@vcXAxis,
			vcYAxis=@vcYAxis,
			vcAggType=@vcAggType,
			tintReportCategory=@tintReportCategory
			,vcTimeLine=@vcTimeLine
			,vcGroupBy=@vcGroupBy
			,vcTeritorry=@vcTeritorry
			,vcFilterBy=@vcFilterBy
			,vcFilterValue=@vcFilterValue	
			,dtFromDate=@dtFromDate
			,dtToDate=@dtToDate
			,numRecordCount=@numRecordCount
			,tintControlField=@tintControlField
			,vcDealAmount=@vcDealAmount
			,tintOppType = @tintOppType
			,lngPConclAnalysis= @lngPConclAnalysis
			,bitTask=@bitTask
			,tintTotalProgress=@tintTotalProgress
			,tintMinNumber=@tintMinNumber
			,tintMaxNumber=@tintMaxNumber
			,tintQtyToDisplay=@tintQtyToDisplay
			,vcDueDate=@vcDueDate
			,bitDealWon =@bitDealWon
			,bitGrossRevenue = @bitGrossRevenue
			,bitRevenue = @bitRevenue
			,bitGrossProfit = @bitGrossProfit
			,bitCustomTimeline = @bitCustomTimeline
			,bitBilledButNotReceived = @bitBilledButNotReceived 
			,bitReceviedButNotBilled = @bitReceviedButNotBilled
			,bitSoldButNotShipped = @bitSoldButNotShipped
		WHERE
			numDashBoardID=@numDashBoardID 
			AND numDomainID=@numDomainID
	END
END	
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageSalesprocessList]    Script Date: 07/26/2008 16:19:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managesalesprocesslist')
DROP PROCEDURE usp_managesalesprocesslist
GO
CREATE PROCEDURE [dbo].[USP_ManageSalesprocessList]
    @numSalesProsID AS NUMERIC(9) = 0,
    @vcSlpName AS VARCHAR(50) = '',
    @numUserCntID AS NUMERIC(9) = 0,
    @tintProType AS TINYINT,
    @numDomainID AS NUMERIC(9) = 0,
    @numStageNumber AS NUMERIC = 0,
	@bitAssigntoTeams AS BIT=0,
	@bitAutomaticStartTimer AS BIT=0,
	@numOppId AS NUMERIC(18,0)=0,
	@numTaskValidatorId AS NUMERIC(18,0)=0,
	@numBuildManager NUMERIC(18,0) = 0
AS 
IF @numSalesProsID = 0 
    BEGIN        
        INSERT  INTO Sales_process_List_Master ( Slp_Name,
                                                 numdomainid,
                                                 pro_type,
                                                 numCreatedby,
                                                 dtCreatedon,
                                                 numModifedby,
                                                 dtModifiedOn,tintConfiguration,bitAutomaticStartTimer,bitAssigntoTeams,numOppId,numTaskValidatorId,numBuildManager)
        VALUES  (
                  @vcSlpName,
                  @numDomainID,
                  @tintProType,
                  @numUserCntID,
                  GETUTCDATE(),
                  @numUserCntID,
                  GETUTCDATE(),@numStageNumber,@bitAutomaticStartTimer,@bitAssigntoTeams,@numOppId,@numTaskValidatorId,@numBuildManager
                )        
        SET @numSalesProsID = SCOPE_IDENTITY()        
 
        SELECT  @numSalesProsID     
    END        
ELSE 
    BEGIN   
		IF(@numOppId>0)
		BEGIN
			UPDATE  Sales_process_List_Master
			SET     Slp_Name = @vcSlpName
			WHERE   Slp_Id = @numSalesProsID        
			SELECT  @numSalesProsID        
		END
		ELSE
		BEGIN  
			UPDATE  Sales_process_List_Master
			SET     Slp_Name = @vcSlpName,
					numModifedby = @numUserCntID,
					dtModifiedOn = GETUTCDATE(),
					bitAssigntoTeams=@bitAssigntoTeams,
					bitAutomaticStartTimer=@bitAutomaticStartTimer,
					numTaskValidatorId=@numTaskValidatorId,
					numBuildManager=@numBuildManager
			WHERE   Slp_Id = @numSalesProsID        
			SELECT  @numSalesProsID        
		END
    END
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageSOWorkOrder')
DROP PROCEDURE USP_ManageSOWorkOrder
GO
CREATE PROCEDURE [dbo].[USP_ManageSOWorkOrder]
@numDomainID AS numeric(18,0)=0,
@numOppID AS numeric(18,0)=0,
@numUserCntID AS numeric(9)=0
AS
BEGIN
	DECLARE @numItemCode AS NUMERIC(18,0)
	DECLARE @numUnitHour AS FLOAT
	DECLARE @numWarehouseItmsID AS NUMERIC(18,0)
	DECLARE @vcItemDesc AS VARCHAR(1000)
  
	DECLARE @numWOId AS NUMERIC(18,0)
	DECLARE @vcInstruction AS VARCHAR(1000)
	DECLARE @dtRequestedFinish AS DATETIME
	DECLARE @numWOAssignedTo NUMERIC(18,0) 
	DECLARE @numWarehouseID NUMERIC(18,0)

	DECLARE @numBuildProcessID NUMERIC(18,0)
	DECLARE @ItemReleaseDate DATE

	DECLARE @numoppitemtCode NUMERIC(18,0)
	DECLARE @numOppChildItemID NUMERIC(18,0)
	DECLARE @numOppKitChildItemID NUMERIC(18,0)

	DECLARE @TEMPItems TABLE
	(
		ID INT IDENTITY(1,1)
		,numoppitemtCode NUMERIC(18,0)
		,numOppChildItemID NUMERIC(18,0)
		,numOppKitChildItemID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numUnitHour FLOAT
		,numWarehouseItmsID NUMERIC(18,0)
		,vcItemDesc VARCHAR(3000)
		,vcInstruction VARCHAR(3000)
		,bintCompliationDate DATETIME
		,ItemReleaseDate DATETIME
		,numBusinessProcessId NUMERIC(18,0)
	)
  
	INSERT INTO @TEMPItems
	(
		numoppitemtCode
		,numOppChildItemID
		,numOppKitChildItemID
		,numItemCode
		,numUnitHour
		,numWarehouseItmsID
		,vcItemDesc
		,vcInstruction
		,bintCompliationDate
		,ItemReleaseDate
		,numBusinessProcessId
	)
	SELECT 
		numoppitemtCode
		,0 AS numOppChildItemID
		,0 AS numOppKitChildItemID
		,OpportunityItems.numItemCode
		,ISNULL(numWOQty,0) numUnitHour
		,numWarehouseItmsID
		,vcItemDesc
		,vcInstruction
		,bintCompliationDate
		,ItemReleaseDate
		,ISNULL(Item.numBusinessProcessId,0)
	FROM 
		OpportunityItems
	INNER JOIN
		Item
	ON
		OpportunityItems.numItemCode = Item.numItemCode
	WHERE 
		numOppID=@numOppID
		AND ISNULL(bitWorkOrder,0)=1
		AND ISNULL(numWOQty,0) > 0


	DECLARE @i INT = 1
	DECLARE @iCount INT

	SELECT @iCount=COUNT(*) FROM @TEMPItems

	WHILE @i <= @iCount
	BEGIN
		SELECT
			@numoppitemtCode=numoppitemtCode
			,@numOppChildItemID=numOppChildItemID
			,@numOppKitChildItemID=numOppKitChildItemID
			,@numItemCode=numItemCode
			,@numUnitHour=numUnitHour
			,@numWarehouseItmsID=numWarehouseItmsID
			,@vcItemDesc=vcItemDesc
			,@vcInstruction=vcInstruction
			,@dtRequestedFinish=bintCompliationDate
			,@numWOAssignedTo=ISNULL((SELECT numBuildManager FROM Sales_process_List_Master WHERE Slp_Id=numBusinessProcessId),0)
			,@ItemReleaseDate=ItemReleaseDate
			,@numBuildProcessID=numBusinessProcessId
		FROM
			@TEMPItems
		WHERE
			ID = @i 

		IF NOT EXISTS (SELECT numWOId FROM WorkOrder WHERE numDomainID=@numDomainID AND numOppId=@numOppID AND numOppItemID=@numoppitemtCode AND ISNULl(numOppChildItemID,0) = 0 AND ISNULL(numOppKitChildItemID,0) = 0)
		BEGIN
			SET @numWarehouseID = ISNULL((SELECT numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID = @numWarehouseItmsID),0)

			IF EXISTS (SELECT 
						item.numItemCode
					FROM 
						item                                
					INNER JOIN 
						ItemDetails Dtl 
					ON 
						numChildItemID=numItemCode
					WHERE 
						numItemKitID=@numItemCode
						AND charItemType='P'
						AND NOT EXISTS (SELECT numWareHouseItemId FROM WareHouseItems WHERE numItemID=item.numItemCode AND numWareHouseID=@numWarehouseID))
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
				RETURN
			END

			INSERT INTO WorkOrder
			(
				numItemCode
				,numQtyItemsReq
				,numWareHouseItemId
				,numCreatedBy
				,bintCreatedDate
				,numDomainID
				,numWOStatus
				,numOppId
				,vcItemDesc
				,vcInstruction
				,bintCompliationDate
				,numAssignedTo
				,numOppItemID
				,numOppChildItemID
				,numOppKitChildItemID
				,numBuildProcessId
				,dtmStartDate
				,dtmEndDate
			)
			VALUES
			(
				@numItemCode
				,@numUnitHour
				,@numWarehouseItmsID
				,@numUserCntID
				,GETUTCDATE()
				,@numDomainID
				,0
				,@numOppID
				,@vcItemDesc
				,@vcInstruction
				,@dtRequestedFinish
				,@numWOAssignedTo
				,@numoppitemtCode
				,@numOppChildItemID
				,@numOppKitChildItemID
				,@numBuildProcessID
				,GETUTCDATE()
				,@dtRequestedFinish
			)
	
			SET @numWOId = SCOPE_IDENTITY()

			EXEC dbo.USP_UpdateNameTemplateValueForWorkOrder 3,@numDomainID,@numWOId

			IF(@numBuildProcessId>0)
			BEGIN
				INSERT INTO Sales_process_List_Master
				(
					Slp_Name,
					numdomainid,
					pro_type,
					numCreatedby,
					dtCreatedon,
					numModifedby,
					dtModifiedOn,
					tintConfiguration,
					bitAutomaticStartTimer,
					bitAssigntoTeams,
					numOppId,
					numTaskValidatorId,
					numProjectId,
					numWorkOrderId
				)
					SELECT 
					Slp_Name,
					numdomainid,
					pro_type,
					numCreatedby,
					dtCreatedon,
					numModifedby,
					dtModifiedOn,
					tintConfiguration,
					bitAutomaticStartTimer,
					bitAssigntoTeams,
					0,
					numTaskValidatorId,
					0,
					@numWOId 
				FROM 
					Sales_process_List_Master 
				WHERE 
					Slp_Id=@numBuildProcessId    
					
				DECLARE @numNewProcessId AS NUMERIC(18,0)=0
				SET @numNewProcessId=(SELECT SCOPE_IDENTITY())  
					
				UPDATE WorkOrder SET numBuildProcessId=@numNewProcessId WHERE numWOId=@numWOId

				INSERT INTO StagePercentageDetails
				(
					numStagePercentageId, 
					tintConfiguration, 
					vcStageName, 
					numDomainId, 
					numCreatedBy, 
					bintCreatedDate, 
					numModifiedBy, 
					bintModifiedDate, 
					slp_id, 
					numAssignTo, 
					vcMileStoneName, 
					tintPercentage, 
					tinProgressPercentage, 
					dtStartDate, 
					numParentStageID, 
					intDueDays, 
					numProjectID, 
					numOppID, 
					vcDescription, 
					bitIsDueDaysUsed,
					numTeamId, 
					bitRunningDynamicMode,
					numStageOrder,
					numWorkOrderId
				)
				SELECT
					numStagePercentageId, 
					tintConfiguration, 
					vcStageName, 
					numDomainId, 
					@numUserCntID, 
					GETDATE(), 
					@numUserCntID, 
					GETDATE(), 
					@numNewProcessId, 
					numAssignTo, 
					vcMileStoneName, 
					tintPercentage, 
					tinProgressPercentage, 
					GETDATE(), 
					numParentStageID, 
					intDueDays, 
					0, 
					0, 
					vcDescription, 
					bitIsDueDaysUsed,
					numTeamId, 
					bitRunningDynamicMode,
					numStageOrder,
					@numWOId
				FROM
					StagePercentageDetails
				WHERE
					slp_id=@numBuildProcessId	

				INSERT INTO StagePercentageDetailsTask
				(
					numStageDetailsId, 
					vcTaskName, 
					numHours, 
					numMinutes, 
					numAssignTo, 
					numDomainID, 
					numCreatedBy, 
					dtmCreatedOn,
					numOppId,
					numProjectId,
					numParentTaskId,
					bitDefaultTask,
					bitSavedTask,
					numOrder,
					numReferenceTaskId,
					numWorkOrderId
				)
				SELECT 
					(SELECT TOP 1 SFD.numStageDetailsId FROM StagePercentageDetails AS SFD WHERE SFD.vcStageName=SP.vcStageName AND SFD.numWorkOrderId=@numWOId),
					vcTaskName,
					numHours,
					numMinutes,
					ST.numAssignTo,
					@numDomainID,
					@numUserCntID,
					GETDATE(),
					0,
					0,
					0,
					1,
					CASE WHEN SP.bitRunningDynamicMode=1 THEN 0 ELSE 1 END,
					numOrder,
					numTaskId,
					@numWOId
				FROM 
					StagePercentageDetailsTask AS ST
				LEFT JOIN
					StagePercentageDetails As SP
				ON
					ST.numStageDetailsId=SP.numStageDetailsId
				WHERE
					ISNULL(ST.numOppId,0)=0 AND ISNULL(ST.numProjectId,0)=0 AND
					ST.numStageDetailsId IN(SELECT numStageDetailsId FROM 
					StagePercentageDetails As ST
				WHERE
					ST.slp_id=@numBuildProcessId)
				ORDER BY ST.numOrder
			END

			INSERT INTO [WorkOrderDetails] 
			(
				numWOId,numItemKitID,numChildItemID,numQtyItemsReq,numWareHouseItemId,vcItemDesc,sintOrder,numQtyItemsReq_Orig,numUOMId
			)
			SELECT 
				@numWOId,numItemKitID,numItemCode,
				CAST(((DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(Dtl.numUOMID,item.numItemCode,item.numDomainID,item.numBaseUnit),1)) * @numUnitHour)AS FLOAT),
				isnull((SELECT TOP 1 numWareHouseItemId FROM WareHouseItems WHERE numItemID=item.numItemCode AND numWareHouseID=@numWarehouseID ORDER BY ISNULL(numWLocationID,0),numWareHouseItemID),0),
				ISNULL(Dtl.vcItemDesc,txtItemDesc),
				ISNULL(sintOrder,0),
				DTL.numQtyItemsReq,
				Dtl.numUOMId 
			FROM 
				item                                
			INNER JOIN 
				ItemDetails Dtl 
			ON 
				numChildItemID=numItemCode
			WHERE 
				numItemKitID=@numItemCode

			--UPDATE ON ORDER OF ASSEMBLY
			UPDATE 
				WareHouseItems
			SET    
				numOnOrder= ISNULL(numOnOrder,0) + @numUnitHour,
				dtModified = GETDATE() 
			WHERE   
				numWareHouseItemID = @numWarehouseItmsID 
	
			--MAKE ENTRY IN WAREHOUSE TRACKING TABLE
			DECLARE @Description AS VARCHAR(1000)
			SET @Description=CONCAT('Work Order Created (Qty:',@numUnitHour,')')

			EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWarehouseItmsID, --  numeric(9, 0)
			@numReferenceID = @numWOId, --  numeric(9, 0)
			@tintRefType = 2, --  tinyint
			@vcDescription = @Description, --  varchar(100)
			@numModifiedBy = @numUserCntID,
			@numDomainID = @numDomainID

			EXEC USP_ManageInventoryWorkOrder @numWOId,@numDomainID,@numUserCntID
		END

		SET @i = @i + 1
	END
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageWorkOrder')
DROP PROCEDURE USP_ManageWorkOrder
GO
CREATE PROCEDURE [dbo].[USP_ManageWorkOrder]
@numItemCode AS NUMERIC(18,0),
@numWareHouseItemID AS NUMERIC(18,0),
@Units AS INT,
@numDomainID AS NUMERIC(18,0),
@numUserCntID AS NUMERIC(18,0),
@vcInstruction AS VARCHAR(1000),
@bintCompliationDate AS DATETIME,
@numAssignedTo AS NUMERIC(9),
@numSalesOrderId AS NUMERIC(18,0)=0,
@numBuildProcessId AS NUMERIC(18,0)=0,
@dtmProjectFinishDate AS DATETIME=NULL,
@dtmEndDate AS DATETIME=NULL,
@dtmStartDate AS DATETIME=NULL
AS
BEGIN
BEGIN TRY
BEGIN TRAN
	DECLARE @numWarehouseID NUMERIC(18,0)
	SET @numWarehouseID = ISNULL((SELECT numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID = @numWareHouseItemID),0)

	IF EXISTS (SELECT 
					item.numItemCode
				FROM 
					item                                
				INNER JOIN 
					ItemDetails Dtl 
				ON 
					numChildItemID=numItemCode
				WHERE 
					numItemKitID=@numItemCode
					AND charItemType='P'
					AND NOT EXISTS (SELECT numWareHouseItemId FROM WareHouseItems WHERE numItemID=item.numItemCode AND numWareHouseID=@numWarehouseID))
	BEGIN
		RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
		RETURN
	END

	DECLARE @numWOId AS NUMERIC(9),@numDivisionId AS NUMERIC(9) 
	DECLARE @txtItemDesc AS varchar(1000),@vcItemName AS VARCHAR(300)
	
	SELECT @txtItemDesc=ISNULL(txtItemDesc,''),@vcItemName=ISNULL(vcItemName,'') FROM Item WHERE numItemCode=@numItemCode

	IF ISNULL(@numAssignedTo,0) = 0
	BEGIN
		SET @numAssignedTo = ISNULL((SELECT numBuildManager FROM Sales_process_List_Master WHERE Slp_Id=@numBuildProcessId),0)
	END

	INSERT INTO WorkOrder
	(
		numItemCode,numQtyItemsReq,numWareHouseItemId,numCreatedBy,
		bintCreatedDate,numDomainID,numWOStatus,vcItemDesc,vcInstruction,
		bintCompliationDate,numAssignedTo,numOppId,numBuildProcessId,dtmStartDate,dtmEndDate
	)
	VALUES
	(
		@numItemCode,@Units,@numWareHouseItemID,@numUserCntID,
		getutcdate(),@numDomainID,0,@txtItemDesc,@vcInstruction,
		@bintCompliationDate,@numAssignedTo,@numSalesOrderId,@numBuildProcessId,@dtmStartDate,@dtmEndDate
	)
	
	SET @numWOId=@@IDENTITY

	EXEC dbo.USP_UpdateNameTemplateValueForWorkOrder 3,@numDomainID,@numWOId
	IF(@numBuildProcessId>0)
	BEGIN
			INSERT  INTO Sales_process_List_Master ( Slp_Name,
                                                 numdomainid,
                                                 pro_type,
                                                 numCreatedby,
                                                 dtCreatedon,
                                                 numModifedby,
                                                 dtModifiedOn,tintConfiguration,bitAutomaticStartTimer,bitAssigntoTeams,numOppId,numTaskValidatorId,
												 numProjectId,numWorkOrderId)
		 SELECT Slp_Name,
				numdomainid,
				pro_type,
				numCreatedby,
				dtCreatedon,
				numModifedby,
				dtModifiedOn,tintConfiguration,bitAutomaticStartTimer,bitAssigntoTeams,0,numTaskValidatorId,0,@numWOId FROM Sales_process_List_Master WHERE Slp_Id=@numBuildProcessId    
			DECLARE @numNewProcessId AS NUMERIC(18,0)=0
			SET @numNewProcessId=(SELECT SCOPE_IDENTITY())
			UPDATE WorkOrder SET numBuildProcessId=@numNewProcessId WHERE numWOId=@numWOId
			INSERT INTO StagePercentageDetails(
			numStagePercentageId, 
			tintConfiguration, 
			vcStageName, 
			numDomainId, 
			numCreatedBy, 
			bintCreatedDate, 
			numModifiedBy, 
			bintModifiedDate, 
			slp_id, 
			numAssignTo, 
			vcMileStoneName, 
			tintPercentage, 
			tinProgressPercentage, 
			dtStartDate, 
			numParentStageID, 
			intDueDays, 
			numProjectID, 
			numOppID, 
			vcDescription, 
			bitIsDueDaysUsed,
			numTeamId, 
			bitRunningDynamicMode,
			numStageOrder,
			numWorkOrderId
		)
		SELECT
			numStagePercentageId, 
			tintConfiguration, 
			vcStageName, 
			numDomainId, 
			@numUserCntID, 
			GETDATE(), 
			@numUserCntID, 
			GETDATE(), 
			@numNewProcessId, 
			numAssignTo, 
			vcMileStoneName, 
			tintPercentage, 
			tinProgressPercentage, 
			GETDATE(), 
			numParentStageID, 
			intDueDays, 
			0, 
			0, 
			vcDescription, 
			bitIsDueDaysUsed,
			numTeamId, 
			bitRunningDynamicMode,
			numStageOrder,
			@numWOId
		FROM
			StagePercentageDetails
		WHERE
			slp_id=@numBuildProcessId	

			INSERT INTO StagePercentageDetailsTask(
			numStageDetailsId, 
			vcTaskName, 
			numHours, 
			numMinutes, 
			numAssignTo, 
			numDomainID, 
			numCreatedBy, 
			dtmCreatedOn,
			numOppId,
			numProjectId,
			numParentTaskId,
			bitDefaultTask,
			bitSavedTask,
			numOrder,
			numReferenceTaskId,
			numWorkOrderId
		)
		SELECT 
			(SELECT TOP 1 SFD.numStageDetailsId FROM StagePercentageDetails AS SFD WHERE SFD.vcStageName=SP.vcStageName AND SFD.numWorkOrderId=@numWOId),
			vcTaskName,
			numHours,
			numMinutes,
			ST.numAssignTo,
			@numDomainID,
			@numUserCntID,
			GETDATE(),
			0,
			0,
			0,
			1,
			CASE WHEN SP.bitRunningDynamicMode=1 THEN 0 ELSE 1 END,
			numOrder,
			numTaskId,
			@numWOId
		FROM 
			StagePercentageDetailsTask AS ST
		LEFT JOIN
			StagePercentageDetails As SP
		ON
			ST.numStageDetailsId=SP.numStageDetailsId
		WHERE
			ISNULL(ST.numOppId,0)=0 AND ISNULL(ST.numProjectId,0)=0 AND
			ST.numStageDetailsId IN(SELECT numStageDetailsId FROM 
			StagePercentageDetails As ST
		WHERE
			ST.slp_id=@numBuildProcessId)
		ORDER BY ST.numOrder
	END

	INSERT INTO [WorkOrderDetails] 
	(
		numWOId,numItemKitID,numChildItemID,numQtyItemsReq,numWareHouseItemId,vcItemDesc,sintOrder,numQtyItemsReq_Orig,numUOMId
	)
	SELECT 
		@numWOId,numItemKitID,numItemCode,
		CAST(((DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(Dtl.numUOMID,item.numItemCode,item.numDomainID,item.numBaseUnit),1)) * @Units) AS FLOAT),
		isnull((SELECT TOP 1 numWareHouseItemId FROM WareHouseItems WHERE numItemID=item.numItemCode AND numWareHouseID=@numWarehouseID ORDER BY ISNULL(numWLocationID,0),numWareHouseItemID),0),
		ISNULL(Dtl.vcItemDesc,txtItemDesc),
		ISNULL(sintOrder,0),
		DTL.numQtyItemsReq,
		Dtl.numUOMId 
	FROM 
		item                                
	INNER JOIN 
		ItemDetails Dtl 
	ON 
		numChildItemID=numItemCode
	WHERE 
		numItemKitID=@numItemCode

	--UPDATE ON ORDER OF ASSEMBLY
	UPDATE 
		WareHouseItems
	SET    
		numOnOrder= ISNULL(numOnOrder,0) + @Units,
		dtModified = GETDATE() 
	WHERE   
		numWareHouseItemID = @numWareHouseItemID 
	
	--MAKE ENTRY IN WAREHOUSE TRACKING TABLE
	DECLARE @Description AS VARCHAR(1000)
	SET @Description=CONCAT('Work Order Created (Qty:',@Units,')')

	EXEC dbo.USP_ManageWareHouseItems_Tracking
	@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
	@numReferenceID = @numWOId, --  numeric(9, 0)
	@tintRefType = 2, --  tinyint
	@vcDescription = @Description, --  varchar(100)
	@numModifiedBy = @numUserCntID,
	@numDomainID = @numDomainID

	EXEC USP_ManageInventoryWorkOrder @numWOId,@numDomainID,@numUserCntID
COMMIT TRAN 
END TRY 
BEGIN CATCH		
    IF ( @@TRANCOUNT > 0 ) 
    BEGIN
        ROLLBACK TRAN
    END

	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO

-- exec USP_ManageWorkOrderStatus @numWOId=10086,@numWOStatus=23184,@numUserCntID=1,@numDomainID=1
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageWorkOrderStatus')
DROP PROCEDURE USP_ManageWorkOrderStatus
GO
CREATE PROCEDURE [dbo].[USP_ManageWorkOrderStatus]
@numWOId as numeric(9)=0,
@numWOStatus as numeric(9),
@numUserCntID AS NUMERIC(9)=0,
@numDomainID AS NUMERIC(18,0)
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @numDomain AS NUMERIC(18,0)
	DECLARE @CurrentWorkOrderStatus AS NUMERIC(18,0)
	SELECT @numDomain = @numDomainID

	--GET CURRENT STATUS OF WORK ORDER
	SELECT @CurrentWorkOrderStatus=numWOStatus FROM WorkOrder WHERE numWOId=@numWOId AND [numDomainID] = @numDomainID

	IF ISNULL(@CurrentWorkOrderStatus,0) = 23184 AND @numWOStatus=23184
	BEGIN
		RAISERROR('WORKORDER_IS_ALREADY_COMPLETED',16,1)
		RETURN
	END

	--ASSIGN NEW STATUS TO WORK ORDER
	UPDATE WorkOrder SET numWOStatus=@numWOStatus WHERE numWOId=@numWOId AND [numDomainID] = @numDomainID AND numWOStatus <> 23184 

	--IF CURRENT WORK ORDER STATUS IS NOT COMPLETED AND NEW WORK ORDER STATUS IS COMPLETED THEN MAKE INVENTORY CHANGES
	IF ISNULL(@CurrentWorkOrderStatus,0) <> 23184 AND @numWOStatus=23184
	BEGIN
		DECLARE @PendingChildWO NUMERIC(18,0)

		;WITH CTE (numWOID) AS
		(
			SELECT
				numWOID 
			FROM 
				WorkOrder
			WHERE 
				numParentWOID = @numWOId 
				AND numWOStatus <> 23184
			UNION ALL
			SELECT 
				WorkOrder.numWOID 
			FROM 
				WorkOrder
			INNER JOIN
				CTE c
			ON
				 WorkOrder.numParentWOID = c.numWOID
			WHERE 
				WorkOrder.numWOStatus <> 23184
		)

		SELECT @PendingChildWO = COUNT(*) FROM CTE

		--WE HAVE IMPLEMENTED CODE IN SUCH WAY THAT FIRST CHILD WORK ORDER GET COMPLETED
		--SO IF SOME REASON CHILD WORK ORDER IS NOT COMPLETED THAN RAISERROR
		IF ISNULL(@PendingChildWO,0) > 0
		BEGIN
			RAISERROR('CHILD WORK ORDER STILL NOT COMPLETED',16,1);
		END

		IF EXISTS (SELECT 
						WorkOrderDetails.numWODetailId
					FROM
						WorkOrderDetails
					OUTER APPLY
					(
						SELECT
							SUM(numPickedQty) numPickedQty
						FROM
							WorkOrderPickedItems
						WHERE
							WorkOrderPickedItems.numWODetailId = WorkOrderDetails.numWODetailId
					) TEMP
					WHERE
						WorkOrderDetails.numWOId=@numWOId
						AND ISNULL(numWareHouseItemId,0) > 0
						AND ISNULL(WorkOrderDetails.numQtyItemsReq,0) <> ISNULL(TEMP.numPickedQty,0))
		BEGIN
			RAISERROR('BOM_LEFT_TO_BE_PICKED',16,1);
			RETURN
		END

		DECLARE @numWareHouseItemID AS NUMERIC(9),@numQtyItemsReq AS FLOAT,@numQtyBuilt FLOAT,@numOppId AS NUMERIC(9), @numOppItemID NUMERIC(18,0)
		SELECT 
			@numWareHouseItemID=numWareHouseItemID
			,@numQtyItemsReq=numQtyItemsReq
			,@numQtyBuilt=ISNULL(numQtyBuilt,0)
			,@numOppId=ISNULL(numOppId,0) 
		FROM 
			WorkOrder 
		WHERE 
			numWOId=@numWOId 
			AND [numDomainID] = @numDomainID

		UPDATE WorkOrder SET bintCompletedDate=GETDATE(),numQtyBuilt=numQtyItemsReq WHERE numWOId=@numWOId AND [numDomainID] = @numDomainID

		-- COMMENTED BECAUSE WORK ORDER BUILD COMPLETION IS NOW MANAGED THROUGH PURCHASE FULFILLMENT
		--IF @numQtyItemsReq > @numQtyBuilt
		--BEGIN
		--	SET @numQtyItemsReq = @numQtyItemsReq - @numQtyBuilt
		--	EXEC USP_UpdatingInventory 0,@numWareHouseItemID,@numQtyItemsReq,@numWOId,@numUserCntID,@numOppId
		--END
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassPurchaseFulfillment_GetRecords')
DROP PROCEDURE dbo.USP_MassPurchaseFulfillment_GetRecords
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassPurchaseFulfillment_GetRecords]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@numViewID NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@bitGroupByOrder BIT
	,@tintFlag tinyint -- if 1 then ShowBilled = true, 2 then received but not billed
	,@bitIncludeSearch BIT
	,@vcOrderStauts VARCHAR(MAX)
	,@vcCustomSearchValue VARCHAR(MAX)
	,@numPageIndex INT
	,@vcSortColumn VARCHAR(100) OUTPUT
	,@vcSortOrder VARCHAR(4) OUTPUT
	,@numTotalRecords NUMERIC(18,0) OUTPUT
)
AS 
BEGIN
	DECLARE @numDivisionID NUMERIC(18,0)
	DECLARE @bitShowOnlyFullyReceived BIT = 0
	DECLARE @tintBillType TINYINT = 1
	
	SELECT @numDivisionID=ISNULL(numDivisionID,0) FROM Domain WHERE numDomainId=@numDomainID
	
	IF EXISTS (SELECT ID FROM MassPurchaseFulfillmentConfiguration WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID)
	BEGIN
		SELECT 
			@bitShowOnlyFullyReceived=ISNULL(bitShowOnlyFullyReceived,0)
			,@tintBillType = ISNULL(tintBillType,1)
		FROM 
			MassPurchaseFulfillmentConfiguration 
		WHERE 
			numDomainID=@numDomainID 
			AND numUserCntID=@numUserCntID
	END

	IF @numViewID = 4
	BEGIN
		SET @bitGroupByOrder = 1
	END

	DECLARE @TempFieldsLeft TABLE
	(
		numPKID INT IDENTITY(1,1)
		,ID VARCHAR(50)
		,numFieldID NUMERIC(18,0)
		,vcFieldName VARCHAR(100)
		,vcOrigDbColumnName VARCHAR(100)
		,vcListItemType VARCHAR(10)
		,numListID NUMERIC(18,0)
		,bitAllowSorting BIT
		,bitAllowFiltering BIT
		,vcAssociatedControlType VARCHAR(20)
		,vcLookBackTableName VARCHAR(100)
		,bitCustomField BIT
		,intRowNum INT
		,intColumnWidth FLOAT
		,bitIsRequired BIT
		,bitIsEmail BIT
		,bitIsAlphaNumeric BIT
		,bitIsNumeric BIT
		,bitIsLengthValidation BIT
		,intMaxLength INT
		,intMinLength INT
		,bitFieldMessage BIT
		,vcFieldMessage VARCHAR(MAX)
	)

	IF @bitGroupByOrder = 1
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName','OpportunityMaster.bintCreatedDate','OpportunityMaster.vcPoppName','OpportunityMaster.numStatus','OpportunityMaster.numAssignedTo','OpportunityMaster.numRecOwner')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'DESC'
		END
	END
	ELSE 
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName'
								,'OpportunityMaster.bintCreatedDate'
								,'OpportunityMaster.vcPoppName'
								,'OpportunityMaster.numStatus'
								,'OpportunityMaster.numAssignedTo'
								,'OpportunityMaster.numRecOwner'
								,'WarehouseItems.numOnHand'
								,'WarehouseItems.numOnOrder'
								,'WarehouseItems.numAllocation'
								,'WarehouseItems.numBackOrder'
								,'Item.numItemCode'
								,'Item.vcItemName'
								,'OpportunityMaster.numUnitHour'
								,'OpportunityMaster.numUnitHourReceived')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'DESC'
		END
	END

	INSERT INTO @TempFieldsLeft
	(
		ID
		,numFieldID
		,vcFieldName
		,vcOrigDbColumnName
		,vcListItemType
		,numListID
		,bitAllowSorting
		,bitAllowFiltering
		,vcAssociatedControlType
		,vcLookBackTableName
		,bitCustomField
		,intRowNum
		,intColumnWidth
		,bitIsRequired
		,bitIsEmail
		,bitIsAlphaNumeric
		,bitIsNumeric
		,bitIsLengthValidation
		,intMaxLength
		,intMinLength
		,bitFieldMessage
		,vcFieldMessage
	)
	SELECT
		CONCAT(135,'~',DFM.numFieldID,'~',0)
		,DFM.numFieldID
		,(CASE WHEN @numViewID=6 AND DFM.numFieldID=248 THEN 'BizDoc' ELSE DFFM.vcFieldName END)
		,DFM.vcOrigDbColumnName
		,ISNULL(DFM.vcListItemType,'')
		,ISNULL(DFM.numListID,0)
		,DFFM.bitAllowSorting
		,DFFM.bitAllowFiltering
		,DFM.vcAssociatedControlType
		,DFM.vcLookBackTableName
		,0
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
		,0
		,0
		,0
		,0
		,0
		,0
		,0
		,0
		,''
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		DycFormField_Mapping DFFM
	ON
		DFCD.numFormId = DFFM.numFormID
		AND DFCD.numFieldId = DFFM.numFieldID
	INNER JOIN
		DycFieldMaster DFM
	ON
		DFFM.numFieldID = DFM.numFieldId
	WHERE 
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=135 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFFM.bitDeleted,0) = 0
		AND ISNULL(DFFM.bitSettingField,0) = 1
		AND ISNULL(DFCD.bitCustom,0) = 0
	UNION
	SELECT
		CONCAT(135,'~',CFM.Fld_id,'~',0)
		,CFM.Fld_id
		,CFM.Fld_label
		,CONCAT('CUST',CFM.Fld_id)
		,''
		,ISNULL(CFM.numListID,0)
		,0
		,0
		,CFM.Fld_type
		,''
		,1
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
		,ISNULL(bitIsRequired,0)
		,ISNULL(bitIsEmail,0)
		,ISNULL(bitIsAlphaNumeric,0)
		,ISNULL(bitIsNumeric,0)
		,ISNULL(bitIsLengthValidation,0)
		,ISNULL(intMaxLength,0)
		,ISNULL(intMinLength,0)
		,ISNULL(bitFieldMessage,0)
		,ISNULL(vcFieldMessage,'')
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		CFW_Fld_Master CFM
	ON
		DFCD.numFieldId = CFM.Fld_id
	LEFT JOIN
		CFW_Validation CV
	ON
		CFM.Fld_id = CV.numFieldID
	WHERE
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=135 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFCD.bitCustom,0) = 1

	IF (SELECT COUNT(*) FROM @TempFieldsLeft) = 0
	BEGIN
		INSERT INTO @TempFieldsLeft
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,vcListItemType
			,numListID
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,vcLookBackTableName
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(135,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,ISNULL(DFM.vcListItemType,'')
			,ISNULL(DFM.numListID,0)
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,DFM.vcLookBackTableName
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=135 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND ISNULL(DFFM.bitDefault,0) = 1
	END

	DECLARE @j INT = 0
	DECLARE @jCount INT
	DECLARE @numFieldId NUMERIC(18,0)
	DECLARE @vcFieldName AS VARCHAR(50)                                                                                                
	DECLARE @vcAssociatedControlType VARCHAR(10)  
	DECLARE @vcDbColumnName VARCHAR(100)                                                
	--DECLARE @numListID AS NUMERIC(9)       
	DECLARE @bitCustomField BIT       
	SET @jCount = (SELECT COUNT(*) FROM @TempFieldsLeft)
	DECLARE @vcCustomFields VARCHAR(MAX) = ''
	DECLARE @vcCustomWhere VARCHAR(MAX) = ''

	WHILE @j <= @jCount
	BEGIN
		SELECT
			@numFieldId=numFieldID,
			@vcFieldName = vcFieldName,
			@vcAssociatedControlType = vcAssociatedControlType,
			@vcDbColumnName = CONCAT('Cust',numFieldID),
			@bitCustomField=ISNULL(bitCustomField,0)
		FROM 
			@TempFieldsLeft
		WHERE 
			numPKID = @j     
		
		
			
		IF @bitCustomField = 1
		BEGIN         
			IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea' 
			BEGIN
				SET @vcCustomFields = CONCAT(@vcCustomFields,',CFW',@numFieldId,'.Fld_Value  [',@vcDbColumnName,']')
             
				SET @vcCustomWhere = CONCAT(@vcCustomWhere,' left Join CFW_FLD_Values_Opp CFW',@numFieldId
								,' ON CFW' , @numFieldId , '.Fld_Id='
								,@numFieldId
								, ' and CFW' , @numFieldId
								, '.RecId=T1.numOppID')                                                         
			END   
			ELSE IF @vcAssociatedControlType = 'CheckBox' 
			BEGIN
				SET @vcCustomFields =CONCAT( @vcCustomFields
					, ',case when isnull(CFW'
					, @numFieldId
					, '.Fld_Value,0)=0 then 0 when isnull(CFW'
					, @numFieldId
					, '.Fld_Value,0)=1 then 1 end   ['
					,  @vcDbColumnName
					, ']')               
 
				SET @vcCustomWhere = CONCAT(@vcCustomWhere
					, ' left Join CFW_FLD_Values_Opp CFW'
					, @numFieldId
					, ' ON CFW',@numFieldId,'.Fld_Id='
					, @numFieldId
					, ' and CFW'
					, @numFieldId
					, '.RecId=T1.numOppID   ')                                                    
			END                
			ELSE IF @vcAssociatedControlType = 'DateField' 
			BEGIN
				SET @vcCustomFields = CONCAT(@vcCustomFields
					, ',dbo.FormatedDateFromDate(CFW'
					, @numFieldId
					, '.Fld_Value,'
					, @numDomainId
					, ')  [',@vcDbColumnName ,']' )  
					                  
				SET @vcCustomWhere = CONCAT(@vcCustomWhere
					, ' left Join CFW_FLD_Values_Opp CFW'
					, @numFieldId
					, ' on CFW', @numFieldId, '.Fld_Id='
					, @numFieldId
					, ' and CFW'
					, @numFieldId
					, '.RecId=T1.numOppID   ' )                                                        
			END                
			ELSE IF @vcAssociatedControlType = 'SelectBox' 
			BEGIN
				SET @vcCustomFields = CONCAT(@vcCustomFields,',L',@numFieldId,'.vcData',' [',@vcDbColumnName,']')
					                                                    
				SET @vcCustomWhere = CONCAT(@vcCustomWhere
					, ' left Join CFW_FLD_Values_Opp CFW'
					, @numFieldId
					, ' on CFW',@numFieldId ,'.Fld_Id='
					, @numFieldId
					, ' and CFW'
					, @numFieldId
					, '.RecId=T1.numOppID    ')     
					                                                    
				SET @vcCustomWhere = CONCAT(@vcCustomWhere
					, ' left Join ListDetails L'
					, @numFieldId
					, ' on L'
					, @numFieldId
					, '.numListItemID=CFW'
					, @numFieldId
					, '.Fld_Value' )              
			END
			ELSE
			BEGIN
				SET @vcCustomFields = CONCAT(@vcCustomFields,CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',T1.numOppID)'),' [',@vcDbColumnName,']')
			END 
		END

		SET @j = @j + 1
	END
	
	CREATE TABLE #TEMPMSRecords
	(
		numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numWOID NUMERIC(18,0)
	)

	DECLARE @vcSQL NVARCHAR(MAX)
	DECLARE @vcSQLFinal NVARCHAR(MAX)
	
	SET @vcSQL = CONCAT('INSERT INTO #TEMPMSRecords
						(
							numOppID
							,numOppItemID
							,numWOID
						)
						SELECT
							OpportunityMaster.numOppId
							,',(CASE WHEN @bitGroupByOrder = 1 THEN '0' ELSE 'OpportunityItems.numoppitemtCode' END),'
							,0
						FROM
							OpportunityMaster
						INNER JOIN
							DivisionMaster
						ON
							OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
						INNER JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						INNER JOIN 
							OpportunityItems
						ON
							OpportunityMaster.numOppId = OpportunityItems.numOppId
						INNER JOIN
							Item
						ON
							OpportunityItems.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
						WHERE
							OpportunityMaster.numDomainId=@numDomainID
							AND OpportunityMaster.tintOppType = 2
							AND OpportunityMaster.tintOppStatus = 1
							AND ISNULL(OpportunityMaster.tintshipped,0) = 0
							AND 1 = (CASE 
										WHEN @numViewID=1 
										THEN (CASE WHEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyReceived,0) > 0 THEN 1 ELSE 0 END)
										WHEN @numViewID=2
										THEN (CASE WHEN ISNULL(OpportunityItems.numQtyReceived,0) > 0 AND ISNULL(OpportunityItems.numQtyReceived,0) - ISNULL(OpportunityItems.numUnitHourReceived,0) > 0 THEN 1 ELSE 0 END)
										WHEN @numViewID=3
										THEN (CASE 
												WHEN (CASE 
														WHEN @tintBillType=2 
														THEN ISNULL(OpportunityItems.numQtyReceived,0) 
														WHEN @tintBillType=3 THEN ISNULL(OpportunityItems.numUnitHourReceived,0) 
														ELSE ISNULL(OpportunityItems.numUnitHour,0) 
														END)  - ISNULL((SELECT 
																			SUM(OpportunityBizDocItems.numUnitHour)
																		FROM
																			OpportunityBizDocs
																		INNER JOIN
																			OpportunityBizDocItems
																		ON
																			OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																		WHERE
																			OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																			AND OpportunityBizDocs.numBizDocId=644
																			AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0 
												THEN 1
												ELSE 0 
											END)
										WHEN @numViewID=4 AND ISNULL(@bitShowOnlyFullyReceived,0) = 1
										THEN (CASE 
												WHEN (SELECT COUNT(*) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppID AND ISNULL(OpportunityItems.numUnitHour,0) <> ISNULL(OpportunityItems.numUnitHourReceived,0) ) > 0 
												THEN 0 
												ELSE 1 
											END)
										ELSE 1
									END)
							AND 1 = (CASE WHEN @numViewID IN (1,2) AND ISNULL(@numWarehouseID,0) > 0 AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID THEN 1 ELSE 0 END) ELSE 1 END)
							AND 1 = (CASE WHEN LEN(ISNULL(@vcOrderStauts,'''')) > 0 THEN (CASE WHEN OpportunityMaster.numStatus ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) ELSE 1 END)'
							,@vcCustomSearchValue
							,(CASE WHEN @bitGroupByOrder = 1 THEN ' GROUP BY OpportunityMaster.numOppId' ELSE '' END));

	EXEC sp_executesql @vcSQL, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @numViewID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @bitShowOnlyFullyReceived BIT, @tintBillType TINYINT', @numDomainID, @vcOrderStauts, @numViewID, @numWarehouseID, @bitShowOnlyFullyReceived, @tintBillType;


	SET @vcSQL = CONCAT('INSERT INTO #TEMPMSRecords
						(
							numOppID
							,numOppItemID
							,numWOID
						)
						SELECT
							0
							,0
							,WorkOrder.numWOID
						FROM
							WorkOrder
						LEFT JOIN
							OpportunityMaster
						ON
							WorkOrder.numOppID = OpportunityMaster.numOppID
						LEFT JOIN
							DivisionMaster
						ON
							OpportunityMaster.numDivisionId = @numDivisionID
						LEFT JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						INNER JOIN
							Item
						ON
							WorkOrder.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							WorkOrder.numWarehouseItemID = WareHouseItems.numWareHouseItemID
						WHERE
							WorkOrder.numDomainId=@numDomainID
							AND ISNULL(WorkOrder.numWOStatus,0) <> 23184
							AND 1 = (CASE 
										WHEN @numViewID=1 
										THEN (CASE WHEN ISNULL(WorkOrder.numQtyBuilt,0) - ISNULL(WorkOrder.numQtyReceived,0) > 0 THEN 1 ELSE 0 END)
										WHEN @numViewID=2
										THEN (CASE WHEN ISNULL(WorkOrder.numQtyReceived,0) > 0 AND ISNULL(WorkOrder.numQtyReceived,0) - ISNULL(WorkOrder.numUnitHourReceived,0) > 0 THEN 1 ELSE 0 END)
										ELSE 0
									END)
							AND 1 = (CASE WHEN @numViewID IN (1,2) AND ISNULL(@numWarehouseID,0) > 0 AND ISNULL(WorkOrder.numWareHouseItemId,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID THEN 1 ELSE 0 END) ELSE 1 END)'
							,@vcCustomSearchValue);

	EXEC sp_executesql @vcSQL, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @numViewID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @bitShowOnlyFullyReceived BIT, @tintBillType TINYINT, @numDivisionID NUMERIC(18,0)', @numDomainID, @vcOrderStauts, @numViewID, @numWarehouseID, @bitShowOnlyFullyReceived, @tintBillType, @numDivisionID;



	SET @numTotalRecords = (SELECT COUNT(*) FROM #TEMPMSRecords)
	
	CREATE TABLE #TEMPResult
	(
		numOppID NUMERIC(18,0)
		,numWOID NUMERIC(18,0)
		,numDivisionID NUMERIC(18,0)
		,numTerID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(18,0)
		,vcCompanyName VARCHAR(300)
		,vcPoppName VARCHAR(300)
		,numStatus VARCHAR(300)
		,bintCreatedDate VARCHAR(50)
		,numAssignedTo VARCHAR(100)
		,numRecOwner VARCHAR(100)
		,vcItemName VARCHAR(300)
		,numOnHand FLOAT
		,numOnOrder FLOAT
		,numAllocation FLOAT
		,numBackOrder FLOAT
		,numBarCodeId VARCHAR(50)
		,vcLocation VARCHAR(100)
		,vcModelID VARCHAR(200)
		,vcItemDesc VARCHAR(2000)
		,numUnitHour FLOAT
		,numUOMId VARCHAR(50)
		,vcAttributes VARCHAR(300)
		,vcPathForTImage VARCHAR(300)
		,numUnitHourReceived FLOAT
		,numQtyReceived FLOAT
		,vcNotes VARCHAR(MAX)
		,SerialLotNo VARCHAR(MAX)
		,vcSKU VARCHAR(50)
		,numRemainingQty FLOAT
		,numQtyToShipReceive FLOAT
		,monPrice DECIMAL(20,5)
		,monTotAmount DECIMAL(20,5)
		,vcBilled FLOAT
	)
	
	IF @bitGroupByOrder = 1
	BEGIN
		SET @vcSQLFinal = CONCAT('INSERT INTO #TEMPResult
								(
									numOppID
									,numWOID
									,numDivisionID
									,numTerID
									,vcCompanyName
									,vcPoppName
									,bintCreatedDate
									,numStatus
									,numAssignedTo
									,numRecOwner
								)
								SELECT
									OpportunityMaster.numOppID
									,WorkOrder.numWOID
									,DivisionMaster.numDivisionID
									,ISNULL(DivisionMaster.numTerID,0) numTerID
									,ISNULL(CompanyInfo.vcCompanyName,'''')
									,(CASE WHEN WorkOrder.numWOID IS NOT NULL THEN CONCAT(ISNULL(WorkOrder.vcWorkOrderName,''-''),(CASE WHEN (ISNULL(WorkOrder.numQtyItemsReq,0) - ISNULL(WorkOrder.numQtyBuilt,0)) > 0 THEN CONCAT('' Remaining build qty:'',ISNULL(WorkOrder.numQtyItemsReq,0) - ISNULL(WorkOrder.numQtyBuilt,0)) ELSE '''' END)) ELSE ISNULL(OpportunityMaster.vcPoppName,'''') END)
									,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,(CASE WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.bintCreatedDate  ELSE OpportunityMaster.bintCreatedDate)),@numDomainID)
									,dbo.GetListIemName(OpportunityMaster.numStatus)
									,(CASE WHEN WorkOrder.numWOID IS NOT NULL THEN dbo.fn_GetContactName(WorkOrder.numAssignedTo) ELSE dbo.fn_GetContactName(OpportunityMaster.numAssignedTo) END
									,(CASE WHEN WorkOrder.numWOID IS NOT NULL THEN dbo.fn_GetContactName(WorkOrder.numCreatedBy) ELSE dbo.fn_GetContactName(OpportunityMaster.numRecOwner) END
								FROM
									#TEMPMSRecords TEMPOrder
								LEFT JOIN
									OpportunityMaster
								ON
									TEMPOrder.numOppID = OpportunityMaster.numOppId
								LEFT JOIN
									WorkOrder
								ON
									TEMPOrder.numWOID = WorkOrder.numWOID
								INNER JOIN
									DivisionMaster
								ON
									DivisionMaster.numDivisionID = (CASE WHEN WorkOrder.numWOID IS NOT NULL THEN @numDivisionID ELSE OpportunityMaster.numDivisionId END)
								INNER JOIN
									CompanyInfo
								ON
									DivisionMaster.numCompanyID = CompanyInfo.numCompanyId ORDER BY ',
								(CASE @vcSortOrder
									WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName '
									WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName'
									WHEN 'OpportunityMaster.bintCreatedDate' THEN 'OpportunityMaster.bintCreatedDate'
									WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(OpportunityMaster.numStatus)'
									WHEN 'OpportunityMaster.numAssignedTo' THEN 'dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)'
									WHEN 'OpportunityMaster.numRecOwner' THEN 'dbo.fn_GetContactName(OpportunityMaster.numRecOwner)'
									ELSE 'OpportunityMaster.bintCreatedDate'
								END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'DESC' END),' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY')


		EXEC sp_executesql @vcSQLFinal, N'@numDomainID NUMERIC(18,0), @ClientTimeZoneOffset INT, @numPageIndex INT, @numDivisionID NUMERIC(18,0)', @numDomainID, @ClientTimeZoneOffset, @numPageIndex, @numDivisionID;
	END
	ELSE
	BEGIN
		SET @vcSQLFinal = CONCAT('INSERT INTO #TEMPResult
								(
									numOppID
									,numWOID
									,numDivisionID
									,numTerID
									,vcCompanyName
									,vcPoppName
									,bintCreatedDate
									,numStatus
									,numAssignedTo
									,numRecOwner
									,numOppItemID
									,numWarehouseItemID
									,numItemCode
									,vcItemName
									,numOnHand
									,numOnOrder
									,numAllocation
									,numBackOrder
									,numBarCodeId
									,vcLocation
									,vcModelID
									,vcItemDesc
									,numUnitHour
									,numUOMId
									,vcAttributes
									,vcPathForTImage
									,numUnitHourReceived
									,numQtyReceived
									,vcNotes
									,SerialLotNo
									,vcSKU
									,numRemainingQty
									,numQtyToShipReceive
									,monPrice
									,monTotAmount
									,vcBilled
								)
								SELECT
									OpportunityMaster.numOppID
									,WorkOrder.numWOID
									,DivisionMaster.numDivisionID
									,ISNULL(DivisionMaster.numTerID,0) numTerID
									,ISNULL(CompanyInfo.vcCompanyName,'''')
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN ISNULL(WorkOrder.vcWorkOrderName,'''')
										ELSE ISNULL(OpportunityMaster.vcPoppName,'''')
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,WorkOrder.bintCreatedDate),@numDomainID)
										ELSE dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate),@numDomainID)
									END)
									,dbo.GetListIemName(OpportunityMaster.numStatus)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN dbo.fn_GetContactName(WorkOrder.numAssignedTo)
										ELSE dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN dbo.fn_GetContactName(WorkOrder.numCreatedBy)
										ELSE dbo.fn_GetContactName(OpportunityMaster.numRecOwner)
									END) 
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN 0
										ELSE OpportunityItems.numoppitemtCode
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.numWareHouseItemId
										ELSE ISNULL(OpportunityItems.numWarehouseItmsID,0)
									END)
									,Item.numItemCode
									,ISNULL(Item.vcItemName,'''')
									,ISNULL(numOnHand,0) + ISNULL(numAllocation,0)
									,ISNULL(numOnOrder,0)
									,ISNULL(numAllocation,0)
									,ISNULL(numBackOrder,0)
									,ISNULL(Item.numBarCodeId,'''')
									,ISNULL(WarehouseLocation.vcLocation,'''')
									,ISNULL(Item.vcModelID,'''')
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN ISNULL(WorkOrder.vcInstruction,'''')
										ELSE ISNULL(OpportunityItems.vcItemDesc,'''')
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN ISNULL(WorkOrder.numQtyItemsReq,0)
										ELSE ISNULL(OpportunityItems.numUnitHour,0)
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN ''Units''
										ELSE dbo.fn_GetUOMName(OpportunityItems.numUOMId)
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN ''''
										ELSE ISNULL(OpportunityItems.vcAttributes,'''')
									END)
									,ISNULL(Item.vcPathForTImage,'''')
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN ISNULL(WorkOrder.numUnitHourReceived,0)
										ELSE ISNULL(OpportunityItems.numUnitHourReceived,0)
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN ISNULL(WorkOrder.numQtyReceived,0)
										ELSE ISNULL(OpportunityItems.numQtyReceived,0)
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN ''''
										ELSE ISNULL(OpportunityItems.vcNotes,'''')
									END)
									,SUBSTRING((SELECT '','' + vcSerialNo + CASE WHEN isnull(Item.bitLotNo,0)=1 THEN '' ('' + CONVERT(VARCHAR(15),whi.numQty) + '')'' ELSE '''' end 
									FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=OpportunityItems.numOppId and oppI.numOppItemID=OpportunityItems.numoppitemtCode 
									ORDER BY vcSerialNo FOR XML PATH('''')),2,200000)
									,ISNULL(Item.vcSKU,'''')
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN (CASE 
																					WHEN @numViewID=1 THEN ISNULL(WorkOrder.numQtyBuilt,0) - ISNULL(WorkOrder.numQtyReceived,0) 
																					WHEN @numViewID=2 THEN ISNULL(WorkOrder.numQtyReceived,0) - ISNULL(WorkOrder.numUnitHourReceived,0) 
																					ELSE 0 
																				END)
										ELSE (CASE 
										WHEN @numViewID=1 THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyReceived,0) 
										WHEN @numViewID=2 THEN ISNULL(OpportunityItems.numQtyReceived,0) - ISNULL(OpportunityItems.numUnitHourReceived,0) 
										WHEN @numViewID=3 THEN (CASE 
																	WHEN @tintBillType=2 
																	THEN ISNULL(OpportunityItems.numQtyReceived,0) 
																	WHEN @tintBillType=3 THEN ISNULL(OpportunityItems.numUnitHourReceived,0) 
																	ELSE ISNULL(OpportunityItems.numUnitHour,0) 
																END)  - ISNULL((SELECT 
																					SUM(OpportunityBizDocItems.numUnitHour)
																				FROM
																					OpportunityBizDocs
																				INNER JOIN
																					OpportunityBizDocItems
																				ON
																					OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																				WHERE
																					OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																					AND OpportunityBizDocs.numBizDocId=644
																					AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
										ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numUnitHourReceived,0) 
									END)
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN (CASE 
																					WHEN @numViewID=1 THEN ISNULL(WorkOrder.numQtyBuilt,0) - ISNULL(WorkOrder.numQtyReceived,0) 
																					WHEN @numViewID=2 THEN ISNULL(WorkOrder.numQtyReceived,0) - ISNULL(WorkOrder.numUnitHourReceived,0) 
																					ELSE 0 
																				END)
										ELSE (CASE 
										WHEN @numViewID=1 THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyReceived,0) 
										WHEN @numViewID=2 THEN ISNULL(OpportunityItems.numQtyReceived,0) - ISNULL(OpportunityItems.numUnitHourReceived,0) 
										ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numUnitHourReceived,0) 
									END)
									END)
									,ISNULL(OpportunityItems.monPrice,0)
									,ISNULL(OpportunityItems.monTotAmount,0)
									,ISNULL((SELECT 
												SUM(OpportunityBizDocItems.numUnitHour)
											FROM
												OpportunityBizDocs
											INNER JOIN
												OpportunityBizDocItems
											ON
												OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
											WHERE
												OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
												AND OpportunityBizDocs.numBizDocId=644
												AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
												
								FROM
									#TEMPMSRecords TEMPOrder
								LEFT JOIN
									OpportunityMaster
								ON
									TEMPOrder.numOppID = OpportunityMaster.numOppId
								LEFT JOIN
									WorkOrder
								ON
									TEMPOrder.numWOID = WorkOrder.numWOID
								INNER JOIN
									DivisionMaster
								ON
									DivisionMaster.numDivisionID = (CASE WHEN WorkOrder.numWOID IS NOT NULL THEN @numDivisionID ELSE OpportunityMaster.numDivisionId END)
								INNER JOIN
									CompanyInfo
								ON
									DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
								LEFT JOIN 
									OpportunityItems
								ON
									OpportunityMaster.numOppId = OpportunityItems.numOppId
									AND TEMPOrder.numOppItemID = OpportunityItems.numoppitemtCode
								INNER JOIN
									Item
								ON
									(CASE WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.numItemCode ELSE OpportunityItems.numItemCode END) = Item.numItemCode
								INNER JOIN
									WareHouseItems
								ON
									(CASE WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.numWareHouseItemId ELSE OpportunityItems.numWarehouseItmsID END) = WareHouseItems.numWareHouseItemID
								LEFT JOIN
									WarehouseLocation
								ON
									WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID ORDER BY ',
								(CASE @vcSortOrder
									WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName '
									WHEN 'OpportunityMaster.vcPoppName' THEN '(CASE 
																					WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.vcWorkOrderName 
																					ELSE OpportunityMaster.vcPoppName
																				END)'
									WHEN 'OpportunityMaster.bintCreatedDate' THEN '(CASE 
																						WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.bintCreatedDate 
																						ELSE OpportunityMaster.bintCreatedDate
																					END)'
									WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(OpportunityMaster.numStatus)'
									WHEN 'OpportunityMaster.numAssignedTo' THEN '(CASE 
																						WHEN WorkOrder.numWOID IS NOT NULL THEN dbo.fn_GetContactName(WorkOrder.numAssignedTo) 
																						ELSE dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)
																					END)'
									WHEN 'OpportunityMaster.numRecOwner' THEN '(CASE 
																					WHEN WorkOrder.numWOID IS NOT NULL THEN dbo.fn_GetContactName(WorkOrder.numCreatedBy) 
																					ELSE dbo.fn_GetContactName(OpportunityMaster.numRecOwner)
																				END)'
									WHEN 'numOnHand' THEN 'ISNULL(WareHouseItems.numOnHand,0) + ISNULL(WareHouseItems.numAllocation,0)'
									WHEN 'numOnOrder' THEN 'ISNULL(WareHouseItems.numOnOrder,0)'
									WHEN 'numAllocation' THEN 'ISNULL(WareHouseItems.numAllocation,0)'
									WHEN 'numBackOrder' THEN 'ISNULL(WareHouseItems.numBackOrder,0)'
									WHEN 'numItemCode' THEN 'Item.numItemCode'
									WHEN 'vcItemName' THEN 'Item.vcItemName'
									WHEN 'numUnitHour' THEN '(CASE 
																WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.numQtyItemsReq
																ELSE OpportunityItems.numUnitHour
															END)'
									WHEN 'numUnitHourReceived' THEN '(CASE 
																		WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.numUnitHourReceived
																		ELSE OpportunityItems.numUnitHourReceived
																	END)'
									ELSE '(CASE 
												WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.bintCreatedDate 
												ELSE OpportunityMaster.bintCreatedDate
											END)'
								END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'DESC' END),' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY')

		EXEC sp_executesql @vcSQLFinal, N'@numDomainID NUMERIC(18,0), @numViewID NUMERIC(18,0), @ClientTimeZoneOffset INT, @numPageIndex INT, @tintBillType TINYINT, @numDivisionID NUMERIC(18,0)', @numDomainID, @numViewID, @ClientTimeZoneOffset, @numPageIndex, @tintBillType, @numDivisionID;
	END
	
	/*Show records which have been billed*/
	IF @numViewID=1 AND  @tintFlag=1 
	BEGIN
	
		SET @vcSQLFinal = CONCAT('SELECT T1.*',@vcCustomFields,' FROM #TEMPResult T1 where vcBilled>0',@vcCustomWhere)
		SET @numTotalRecords = (SELECT COUNT(*) FROM #TEMPResult where vcBilled>0 )
	END
	ELSE IF @numViewID= 3 AND  @tintFlag=2 /* Received but not billed*/
	BEGIN
	SET @vcSQLFinal = CONCAT('SELECT T1.*',@vcCustomFields,' FROM #TEMPResult T1 where numQtyReceived>0',@vcCustomWhere)
	SET @numTotalRecords = (SELECT COUNT(*) FROM #TEMPResult where numQtyReceived>0 )
	END
	else
	BEGIN 
		SET @vcSQLFinal = CONCAT('SELECT T1.*',@vcCustomFields,' FROM #TEMPResult T1 ',@vcCustomWhere)
		END
	


	EXEC sp_executesql @vcSQLFinal

	SELECT * FROM @TempFieldsLeft ORDER BY intRowNum

	IF OBJECT_ID('tempdb..#TEMPMSRecords') IS NOT NULL DROP TABLE #TEMPMSRecords
	IF OBJECT_ID('tempdb..#TEMPResult') IS NOT NULL DROP TABLE #TEMPResult
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassPurchaseFulfillment_GetRecordsForPutAway')
DROP PROCEDURE dbo.USP_MassPurchaseFulfillment_GetRecordsForPutAway
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassPurchaseFulfillment_GetRecordsForPutAway]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@tintMode TINYINT
	,@vcSelectedRecords VARCHAR(MAX)
)
AS
BEGIN
	DECLARE @numDomainDivisionID NUMERIC(18,0)
	SELECT @numDomainDivisionID=ISNULL(@numDomainDivisionID,0) FROM Domain WHERE numDomainId=@numDomainID

	DECLARE @TempFieldsRight TABLE
	(
		ID VARCHAR(50)
		,numFieldID NUMERIC(18,0)
		,vcFieldName VARCHAR(100)
		,vcOrigDbColumnName VARCHAR(100)
		,bitAllowSorting BIT
		,bitAllowFiltering BIT
		,vcAssociatedControlType VARCHAR(20)
		,bitCustomField BIT
		,intRowNum INT
		,intColumnWidth FLOAT
	)

	INSERT INTO @TempFieldsRight
	(
		ID
		,numFieldID
		,vcFieldName
		,vcOrigDbColumnName
		,bitAllowSorting
		,bitAllowFiltering
		,vcAssociatedControlType
		,bitCustomField
		,intRowNum
		,intColumnWidth
	)
	SELECT
		CONCAT(143,'~',DFM.numFieldID,'~',0)
		,DFM.numFieldID
		,DFFM.vcFieldName
		,DFM.vcOrigDbColumnName
		,DFFM.bitAllowSorting
		,DFFM.bitAllowFiltering
		,DFM.vcAssociatedControlType
		,0
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		DycFormField_Mapping DFFM
	ON
		DFCD.numFormId = DFFM.numFormID
		AND DFCD.numFieldId = DFFM.numFieldID
	INNER JOIN
		DycFieldMaster DFM
	ON
		DFFM.numFieldID = DFM.numFieldId
	WHERE 
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=143 
		AND ISNULL(DFFM.bitDeleted,0) = 0
		AND ISNULL(DFFM.bitSettingField,0) = 1
		AND ISNULL(DFCD.bitCustom,0) = 0

	IF (SELECT COUNT(*) FROM @TempFieldsRight) = 0
	BEGIN
		INSERT INTO @TempFieldsRight
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(143,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=143 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND (ISNULL(DFFM.bitDefault,0) = 1 OR ISNULL(DFFM.bitRequired,0) = 1)
	END
	ELSE
	BEGIN
		INSERT INTO @TempFieldsRight
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(143,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=143 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND ISNULL(DFFM.bitRequired,0) = 1
			AND DFM.numFieldId NOT IN (SELECT T1.numFieldID FROM @TempFieldsRight T1)
	END

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppID NUMERIC(18,0)
		,numWOID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
	)

	DECLARE @hDocItem int
	EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcSelectedRecords

	INSERT INTO @TEMP
	(
		numOppID
		,numWOID
		,numOppItemID
	)
	SELECT
		ISNULL(OppID,0)
		,ISNULL(WOID,0)
		,ISNULL(OppItemID,0)
	FROM
		OPENXML (@hDocItem,'/NewDataSet/Table1',2)
	WITH
	(
		OppID NUMERIC(18,0)
		,WOID NUMERIC(18,0)
		,OppItemID NUMERIC(18,0)
	)
	
	EXEC sp_xml_removedocument @hDocItem

	DECLARE @TEMPItems TABLE
	(	
		ID NUMERIC(18,0)
		,numSortOrder INT
		,numOppID NUMERIC(18,0)
		,numWOID NUMERIC(18,0)
		,bitStockTransfer BIT
		,numDivisionID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(18,0)
		,vcPoppName VARCHAR(300)
		,numBarCodeId VARCHAR(300)
		,vcItemName VARCHAR(300)
		,vcItemDesc VARCHAR(MAX)
		,vcSKU VARCHAR(300)
		,vcPathForTImage VARCHAR(MAX)
		,vcAttributes VARCHAR(MAX)
		,numRemainingQty FLOAT
		,numQtyToShipReceive FLOAT
		,bitSerialized BIT
		,bitLotNo BIT
		,bitPPVariance BIT
		,numCurrencyID NUMERIC(18,0)
		,fltExchangeRate FLOAT
		,charItemType VARCHAR(3)
		,vcItemType VARCHAR(50)
		,monPrice DECIMAL(20,5)
		,bitDropShip BIT
		,numProjectID NUMERIC(18,0)
		,numClassID NUMERIC(18,0)
		,numAssetChartAcntId NUMERIC(18,0)
		,numCOGsChartAcntId NUMERIC(18,0)
		,numIncomeChartAcntId NUMERIC(18,0)
		,SerialLotNo VARCHAR(MAX)
		,vcWarehouses VARCHAR(MAX)
	)

	INSERT INTO @TEMPItems
	(	
		ID
		,numSortOrder
		,numOppID
		,numWOID
		,bitStockTransfer
		,numDivisionID
		,numOppItemID 
		,numItemCode
		,numWarehouseItemID
		,vcPoppName
		,numBarCodeId
		,vcItemName
		,vcItemDesc
		,vcSKU
		,vcPathForTImage
		,vcAttributes 
		,numRemainingQty
		,numQtyToShipReceive
		,bitSerialized
		,bitLotNo
		,bitPPVariance
		,numCurrencyID
		,fltExchangeRate
		,charItemType
		,vcItemType
		,monPrice
		,bitDropShip
		,numProjectID
		,numClassID
		,numAssetChartAcntId
		,numCOGsChartAcntId
		,numIncomeChartAcntId
		,SerialLotNo
		,vcWarehouses
	)
	SELECT 
		T1.ID
		,ISNULL(OpportunityItems.numSortOrder,0)
		,OpportunityMaster.numOppID
		,WorkOrder.numWOId
		,ISNULL(OpportunityMaster.bitStockTransfer,0)
		,(CASE WHEN WorkOrder.numWOId IS NOT NULL THEN @numDomainDivisionID ELSE OpportunityMaster.numDivisionId END)
		,OpportunityItems.numoppitemtCode
		,Item.numItemCode
		,WareHouseItems.numWareHouseItemID
		,(CASE WHEN WorkOrder.numWOId IS NOT NULL THEN ISNULL(WorkOrder.vcWorkOrderName,'-') ELSE OpportunityMaster.vcPoppName END)
		,Item.numBarCodeId
		,Item.vcItemName
		,Item.txtItemDesc AS vcItemDesc
		,Item.vcSKU
		,ISNULL(ItemImages.vcPathForTImage,'') vcPathForTImage
		,ISNULL(OpportunityItems.vcAttributes,'') AS vcAttributes
		,(CASE 
				WHEN @tintMode=1 THEN ISNULL((CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numQtyBuilt ELSE OpportunityItems.numUnitHour END),0) - ISNULL((CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numUnitHourReceived ELSE OpportunityItems.numUnitHourReceived END),0)
				WHEN @tintMode=3 THEN ISNULL((CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numQtyReceived ELSE OpportunityItems.numQtyReceived END),0) - ISNULL((CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numUnitHourReceived ELSE OpportunityItems.numUnitHourReceived END),0)
				ELSE 0
			END) numRemainingQty
		,0 numQtyToShipReceive
		,ISNULL(bitSerialized,0)
		,ISNULL(bitLotNo,0)
		,ISNULL(OpportunityMaster.bitPPVariance,0)
		,ISNULL(OpportunityMaster.numCurrencyID,0)
		,ISNULL(OpportunityMaster.fltExchangeRate,0)
		,Item.charItemType
		,OpportunityItems.vcType
		,ISNULL(OpportunityItems.monPrice,0)
		,ISNULL(OpportunityItems.bitDropShip,0)
		,ISNULL(OpportunityItems.numProjectID,0)
		,ISNULL(OpportunityItems.numClassID,0)
		,ISNULL(Item.numAssetChartAcntId,0)
		,ISNULL(Item.numCOGsChartAcntId,0)
		,ISNULL(Item.numIncomeChartAcntId,0)
		,(CASE 
			WHEN WorkOrder.numWOId IS NOT NULL THEN '' 
			ELSE SUBSTRING((SELECT 
								CONCAT(',',vcSerialNo,(CASE WHEN isnull(Item.bitLotNo,0)=1 THEN ' (' + CONVERT(VARCHAR(15),whi.numQty) + ')' ELSE '' END))
							FROM 
								OppWarehouseSerializedItem oppI 
							JOIN 
								WareHouseItmsDTL whi
							ON 
								oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID 
							WHERE 
								oppI.numOppID=OpportunityItems.numOppId 
								and oppI.numOppItemID=OpportunityItems.numoppitemtCode 
							ORDER BY vcSerialNo FOR XML PATH('')),2,200000) END) 
		,(CASE 
			WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner INNER JOIN WarehouseLocation WL ON WIInner.numWLocationID=WL.numWLocationID WHERE WIInner.numDomainID=@numDomainID AND WIInner.numItemID=Item.numItemCode AND WIInner.numWareHouseID = WareHouseItems.numWareHouseID)
			THEN CONCAT('[',STUFF((SELECT 
										CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "Location":"',ISNULL(WL.vcLocation,''),'"}') 
									FROM 
										WareHouseItems WIInner
									INNER JOIN
										WarehouseLocation WL
									ON
										WIInner.numWLocationID = WL.numWLocationID
									WHERE 
										WIInner.numDomainID=@numDomainID 
										AND WIInner.numItemID=Item.numItemCode 
										AND WIInner.numWareHouseID = WareHouseItems.numWareHouseID
									ORDER BY
										WL.vcLocation ASC
									FOR XML PATH('')),1,1,''),']')
			ELSE CONCAT('[',STUFF((SELECT 
								CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "Location":"',ISNULL(WL.vcLocation,''),'"}') 
							FROM 
								WareHouseItems WIInner
							LEFT JOIN
								WarehouseLocation WL
							ON
								WIInner.numWLocationID = WL.numWLocationID
							WHERE 
								WIInner.numDomainID=@numDomainID 
								AND WIInner.numItemID=Item.numItemCode 
								AND WIInner.numWareHouseItemID=WareHouseItems.numWareHouseItemID
							ORDER BY
								WL.vcLocation ASC
							FOR XML PATH('')),1,1,''),']')
		END) 
	FROM
		@TEMP T1
	LEFT JOIN
		WorkOrder
	ON
		T1.numWOID = WorkOrder.numWOId
	LEFT JOIN
		OpportunityMaster
	ON
		OpportunityMaster.numOppID= T1.numOppID
	LEFT JOIN
		OpportunityItems
	ON
		OpportunityMaster.numOppID = OpportunityItems.numOppID
		AND (OpportunityItems.numOppItemtcode = T1.numOppItemID OR ISNULL(T1.numOppItemID,0) = 0)
	LEFT JOIN
		WareHouseItems 
	ON
		WareHouseItems.numWareHouseItemID = (CASE 
												WHEN WorkOrder.numWOId IS NOT NULL 
												THEN WorkOrder.numWareHouseItemId 
												ELSE (CASE WHEN ISNULL(OpportunityMaster.bitStockTransfer,0) = 1 THEN OpportunityItems.numToWarehouseItemID ELSE OpportunityItems.numWarehouseItmsID END) 
											END)
	INNER JOIN
		Item 
	ON
		(CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numItemCode ELSE OpportunityItems.numItemCode END) = Item.numItemCode
	LEFT JOIN
		ItemImages
	ON
		ItemImages.numDomainId=@numDomainID
		AND ItemImages.numItemCode = Item.numItemCode
		AND ItemImages.bitDefault=1 
		AND ItemImages.bitIsImage=1
	WHERE
		1 = (CASE 
				WHEN WorkOrder.numWOId IS NOT NULL 
				THEN (CASE WHEN WorkOrder.numDomainID=@numDomainID THEN 1 ELSE 0 END) 
				ELSE (CASE WHEN OpportunityMaster.numDomainID=@numDomainID THEN 1 ELSE 0 END)
			END)
		AND 1 = (CASE 
					WHEN WorkOrder.numWOId IS NOT NULL 
					THEN (CASE WHEN ISNULL(WorkOrder.numWOStatus,0) <> 23184 THEN 1 ELSE 0 END) 
					ELSE (CASE WHEN ISNULL(OpportunityMaster.tintshipped,0) = 0 THEN 1 ELSE 0 END)
				END)
		AND (WorkOrder.numWOId IS NOT NULL OR (OpportunityMaster.numOppId IS NOT NULL AND OpportunityItems.numoppitemtCode IS NOT NULL))
		AND (CASE 
				WHEN @tintMode=1 THEN ISNULL((CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numQtyBuilt ELSE OpportunityItems.numUnitHour END),0) - ISNULL((CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numUnitHourReceived ELSE OpportunityItems.numUnitHourReceived END),0)
				WHEN @tintMode=3 THEN ISNULL((CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numQtyReceived ELSE OpportunityItems.numQtyReceived END),0) - ISNULL((CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numUnitHourReceived ELSE OpportunityItems.numUnitHourReceived END),0)
				ELSE 0
			END) > 0
	ORDER BY
		T1.ID, OpportunityItems.numSortOrder

	SELECT * FROM @TEMPItems ORDER BY ID	
	SELECT * FROM @TempFieldsRight ORDER BY intRowNum
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassPurchaseFulfillment_ReceiveOnlyItems')
DROP PROCEDURE dbo.USP_MassPurchaseFulfillment_ReceiveOnlyItems
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassPurchaseFulfillment_ReceiveOnlyItems]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@vcSelectedRecords VARCHAR(MAX)
)
AS 
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @hDocItem INT

	DECLARE @Temp TABLE
	(
		ID INT IDENTITY(1,1)
		,OppID NUMERIC(18,0)
		,WOID NUMERIC(18,0)
		,OppItemID NUMERIC(18,0)
		,numQtyToReceive FLOAT
		,IsSuccess BIT
		,ErrorMessage VARCHAR(300)
	)

	EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcSelectedRecords

	INSERT INTO @Temp
	(
		OppID
		,WOID
		,OppItemID
		,numQtyToReceive
		,IsSuccess
	)
	SELECT
		ISNULL(OppID,0)
		,ISNULL(WOID,0)
		,ISNULL(OppItemID,0)
		,QtyToReceive
		,IsSuccess
	FROM
		OPENXML (@hDocItem,'/NewDataSet/Table1',2)
	WITH
	(
		OppID NUMERIC(18,0),
		WOID NUMERIC(18,0),
		OppItemID NUMERIC(18,0),
		QtyToReceive FLOAT,
		IsSuccess BIT
	)

	EXEC sp_xml_removedocument @hDocItem 

	DECLARE @i INT = 1
	DECLARE @iCount INT
	DECLARE @numOppID NUMERIC(18,0)
	DECLARE @numWOID NUMERIC(18,0)
	DECLARE @numOppItemID NUMERIC(18,0)
	DECLARE @numQtyToReceive FLOAT
	SELECT @iCount = COUNT(*) FROM @Temp

	WHILE @i <= @iCount
	BEGIN
		SELECT 
			@numOppID = OppID
			,@numWOID=WOID
			,@numOppItemID = ISNULL(OppItemID,0)
			,@numQtyToReceive = ISNULL(numQtyToReceive,0)
		FROM 
			@Temp 
		WHERE 
			ID = @i

		IF @numWOID	> 0
		BEGIN
			IF EXISTS (SELECT numWOId FROM WorkOrder WHERE numDomainID=@numDomainID AND numWOId=@numWOID AND ISNULL(numQtyBuilt,0) >= (ISNULL(numQtyReceived,0) + @numQtyToReceive))
			BEGIN
				UPDATE
					WorkOrder
				SET
					numQtyReceived = ISNULL(numQtyReceived,0) + @numQtyToReceive
				WHERE
					numDomainID=@numDomainID 
					AND numWOId=@numWOID

				UPDATE 
					@Temp
				SET
					IsSuccess = 1
				WHERE
					ID=@i
			END
			ELSE
			BEGIN
				UPDATE 
					@Temp
				SET
					IsSuccess = 0
					,ErrorMessage = 'Received quantity can not be greater then quantity built'
				WHERE
					ID=@i
			END
		END
		ELSE IF @numOppItemID > 0
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND numoppitemtCode=@numOppItemID AND ISNULL(numUnitHour,0) >= (ISNULL(numQtyReceived,0) + @numQtyToReceive))
			BEGIN
				UPDATE
					OpportunityItems
				SET
					numQtyReceived = ISNULL(numQtyReceived,0) + @numQtyToReceive
				WHERE
					numOppId=@numOppID 
					AND numoppitemtCode=@numOppItemID

				UPDATE 
					@Temp
				SET
					IsSuccess = 1
				WHERE
					ID=@i
			END
			ELSE
			BEGIN
				UPDATE 
					@Temp
				SET
					IsSuccess = 0
					,ErrorMessage = 'Received quantity can not be greater than ordered quantity'
				WHERE
					ID=@i
			END
		END
		ELSE
		BEGIN
			UPDATE
				OpportunityItems
			SET
				numQtyReceived = numUnitHour
			WHERE
				numOppId=@numOppID 
				AND ISNULL(OpportunityItems.bitDropShip,0)=0
				AND ISNULL(numUnitHour,0) <> ISNULL(numQtyReceived,0)

			UPDATE 
				@Temp
			SET
				IsSuccess = 1
			WHERE
				ID=@i
		END
		

		SET @i = @i + 1
	END

	UPDATE
		OM
	SET
		bintModifiedDate=GETUTCDATE()
		,numModifiedBy = @numUserCntID
	FROM
		OpportunityMaster OM
	INNER JOIN
		@Temp T1
	ON
		OM.numOppId = T1.OppID
		AND T1.IsSuccess = 1
	WHERE
		OM.numDomainId = @numDomainID

	SELECT * FROM @Temp
COMMIT
END TRY
BEGIN CATCH
DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);

END CATCH
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetRecords')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetRecords
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetRecords]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@numViewID NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@bitGroupByOrder BIT
	,@bitRemoveBO BIT
	,@numShippingZone NUMERIC(18,0)
	,@bitIncludeSearch BIT
	,@vcOrderStauts VARCHAR(MAX)
	,@tintFilterBy TINYINT
	,@vcFilterValue VARCHAR(MAX)
	,@vcCustomSearchValue VARCHAR(MAX)
	,@numBatchID NUMERIC(18,0)
	,@tintPackingViewMode TINYINT
	,@tintPrintBizDocViewMode TINYINT
	,@tintPendingCloseFilter TINYINT
	,@numPageIndex INT
	,@vcSortColumn VARCHAR(100) OUTPUT
	,@vcSortOrder VARCHAR(4) OUTPUT
	,@numTotalRecords NUMERIC(18,0) OUTPUT
)
AS 
BEGIN
	IF @numViewID = 4 OR @numViewID = 5 OR @numViewID = 6
	BEGIN
		SET @bitGroupByOrder = 1
	END

	SET @vcCustomSearchValue = REPLACE(@vcCustomSearchValue,'OpportunityMaster.dtItemReceivedDate','(SELECT MAX(OMInner.dtItemReceivedDate) FROM OpportunityMaster OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numOppID=OIInner.numOppID WHERE OMInner.tintOppType=2 AND OMInner.tintOppStatus=1 AND OIInner.numItemCode=OpportunityItems.numItemCode)')

	If @numViewID = 6 AND CHARINDEX('OpportunityMaster.bintCreatedDate',@vcCustomSearchValue) > 0
	BEGIN
		SET @vcCustomSearchValue = REPLACE(@vcCustomSearchValue,'OpportunityMaster.bintCreatedDate','OpportunityBizDocs.dtCreatedDate')
	END

	DECLARE @TempFieldsLeft TABLE
	(
		numPKID INT IDENTITY(1,1)
		,ID VARCHAR(50)
		,numFieldID NUMERIC(18,0)
		,vcFieldName VARCHAR(100)
		,vcOrigDbColumnName VARCHAR(100)
		,vcListItemType VARCHAR(10)
		,numListID NUMERIC(18,0)
		,bitAllowSorting BIT
		,bitAllowFiltering BIT
		,vcAssociatedControlType VARCHAR(20)
		,vcLookBackTableName VARCHAR(100)
		,bitCustomField BIT
		,intRowNum INT
		,intColumnWidth FLOAT
		,bitIsRequired BIT
		,bitIsEmail BIT
		,bitIsAlphaNumeric BIT
		,bitIsNumeric BIT
		,bitIsLengthValidation BIT
		,intMaxLength INT
		,intMinLength INT
		,bitFieldMessage BIT
		,vcFieldMessage VARCHAR(MAX)
		,Grp_id INT
	)

	CREATE TABLE #TEMPResult
	(
		numOppID NUMERIC(18,0)
		,numDivisionID NUMERIC(18,0)
		,numTerID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numOppBizDocID NUMERIC(18,0)
		,bintCreatedDate VARCHAR(50)
		,numItemCode NUMERIC(18,0)
		,vcCompanyName VARCHAR(300)
		,vcPoppName VARCHAR(300)
		,numAssignedTo VARCHAR(100)
		,numRecOwner VARCHAR(100)
		,vcBizDocID VARCHAR(300)
		,numStatus VARCHAR(300)
		,txtComments VARCHAR(MAX)
		,numBarCodeId VARCHAR(50)
		,vcLocation VARCHAR(100)
		,vcItemName VARCHAR(300)
		,numItemGroup VARCHAR(300)
		,vcItemDesc VARCHAR(2000)
		,numQtyShipped FLOAT
		,numUnitHour FLOAT
		,monAmountPaid DECIMAL(20,5)
		,monAmountToPay DECIMAL(20,5)
		,numItemClassification VARCHAR(100)
		,vcSKU VARCHAR(50)
		,charItemType VARCHAR(50)
		,vcAttributes VARCHAR(300)
		,vcNotes VARCHAR(MAX)
		,vcItemReleaseDate DATE
		,dtItemReceivedDate VARCHAR(50)
		,intUsedShippingCompany VARCHAR(300)
		,intShippingCompany VARCHAR(300)
		,numPreferredShipVia NUMERIC(18,0)
		,numPreferredShipService NUMERIC(18,0)
		,vcInvoiced FLOAT
		,vcInclusionDetails VARCHAR(MAX)
		,bitPaidInFull BIT
		,numRemainingQty FLOAT
		,numAge VARCHAR(50)
		,dtAnticipatedDelivery  DATE
		,vcShipStatus VARCHAR(MAX)
		,dtExpectedDate VARCHAR(20)
		,dtExpectedDateOrig DATE
		,numQtyPicked FLOAT
		,numQtyPacked FLOAT
		,vcPaymentStatus VARCHAR(MAX)
		,numShipRate VARCHAR(40)
		,vcShippingLabel VARCHAR(MAX)
	)

	IF @bitGroupByOrder = 1
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName','OpportunityBizDocs.monAmountPaid','OpportunityBizDocs.vcBizDocID','OpportunityMaster.bintCreatedDate','OpportunityMaster.bitPaidInFull','OpportunityMaster.numAge','OpportunityMaster.numStatus','OpportunityMaster.vcPoppName','OpportunityMaster.dtItemReceivedDate','OpportunityMaster.numAssignedTo','OpportunityMaster.numRecOwner')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'DESC'
		END
	END
	ELSE 
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName'
								,'Item.charItemType'
								,'Item.numBarCodeId'
								,'Item.numItemClassification'
								,'Item.numItemGroup'
								,'Item.vcSKU'
								,'OpportunityBizDocs.monAmountPaid'
								,'OpportunityMaster.dtExpectedDate'
								,'OpportunityItems.numQtyPacked'
								,'OpportunityItems.numQtyPicked'
								,'OpportunityItems.numQtyShipped'
								,'OpportunityItems.numRemainingQty'
								,'OpportunityItems.numUnitHour'
								,'OpportunityItems.vcInvoiced'
								,'OpportunityItems.vcItemName'
								,'OpportunityItems.vcItemReleaseDate'
								,'OpportunityMaster.bintCreatedDate'
								,'OpportunityMaster.bitPaidInFull'
								,'OpportunityMaster.numAge'
								,'OpportunityMaster.numStatus'
								,'OpportunityMaster.vcPoppName'
								,'WareHouseItems.vcLocation'
								,'OpportunityMaster.dtItemReceivedDate'
								,'OpportunityBizDocs.vcBizDocID')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'DESC'
		END
	END

	DECLARE @tintPackingMode TINYINT
	DECLARE @tintInvoicingType TINYINT
	DECLARE @tintCommitAllocation TINYINT
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @numDefaultSalesShippingDoc NUMERIC(18,0)
	SELECT 
		@tintCommitAllocation=ISNULL(tintCommitAllocation,1) 
		,@numShippingServiceItemID=ISNULL(numShippingServiceItemID,0)
		,@numDefaultSalesShippingDoc=ISNULL(numDefaultSalesShippingDoc,0)
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID 

	IF @numViewID = 2 AND @tintPackingViewMode=3 AND @numDefaultSalesShippingDoc = 0
	BEGIN
		RAISERROR('DEFAULT_SHIPPING_BIZDOC_NOT_CONFIGURED',16,1)
		RETURN
	END

	SELECT
		@tintPackingMode=tintPackingMode
		,@tintInvoicingType=tintInvoicingType
	FROM
		MassSalesFulfillmentConfiguration
	WHERE
		numDomainID = @numDomainID 
		AND numUserCntID = @numUserCntID

	INSERT INTO @TempFieldsLeft
	(
		ID
		,numFieldID
		,vcFieldName
		,vcOrigDbColumnName
		,vcListItemType
		,numListID
		,bitAllowSorting
		,bitAllowFiltering
		,vcAssociatedControlType
		,vcLookBackTableName
		,bitCustomField
		,intRowNum
		,intColumnWidth
		,bitIsRequired
		,bitIsEmail
		,bitIsAlphaNumeric
		,bitIsNumeric
		,bitIsLengthValidation
		,intMaxLength
		,intMinLength
		,bitFieldMessage
		,vcFieldMessage
		,Grp_id
	)
	SELECT
		CONCAT(141,'~',DFM.numFieldID,'~',0)
		,DFM.numFieldID
		,(CASE WHEN @numViewID=6 AND DFM.numFieldID=248 THEN 'BizDoc' ELSE DFFM.vcFieldName END)
		,DFM.vcOrigDbColumnName
		,ISNULL(DFM.vcListItemType,'')
		,ISNULL(DFM.numListID,0)
		,DFFM.bitAllowSorting
		,DFFM.bitAllowFiltering
		,DFM.vcAssociatedControlType
		,DFM.vcLookBackTableName
		,0
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
		,0
		,0
		,0
		,0
		,0
		,0
		,0
		,0
		,''
		,0
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		DycFormField_Mapping DFFM
	ON
		DFCD.numFormId = DFFM.numFormID
		AND DFCD.numFieldId = DFFM.numFieldID
	INNER JOIN
		DycFieldMaster DFM
	ON
		DFFM.numFieldID = DFM.numFieldId
	WHERE 
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=141 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFFM.bitDeleted,0) = 0
		AND ISNULL(DFFM.bitSettingField,0) = 1
		AND ISNULL(DFCD.bitCustom,0) = 0
	UNION
	SELECT
		CONCAT(141,'~',CFM.Fld_id,'~',0)
		,CFM.Fld_id
		,CFM.Fld_label
		,CONCAT('CUST',CFM.Fld_id)
		,''
		,ISNULL(CFM.numListID,0)
		,0
		,0
		,CFM.Fld_type
		,''
		,1
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
		,ISNULL(bitIsRequired,0)
		,ISNULL(bitIsEmail,0)
		,ISNULL(bitIsAlphaNumeric,0)
		,ISNULL(bitIsNumeric,0)
		,ISNULL(bitIsLengthValidation,0)
		,ISNULL(intMaxLength,0)
		,ISNULL(intMinLength,0)
		,ISNULL(bitFieldMessage,0)
		,ISNULL(vcFieldMessage,'')
		,CFM.Grp_id
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		CFW_Fld_Master CFM
	ON
		DFCD.numFieldId = CFM.Fld_id
	LEFT JOIN
		CFW_Validation CV
	ON
		CFM.Fld_id = CV.numFieldID
	WHERE
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=141 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFCD.bitCustom,0) = 1

	IF (SELECT COUNT(*) FROM @TempFieldsLeft) = 0
	BEGIN
		INSERT INTO @TempFieldsLeft
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,vcListItemType
			,numListID
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,vcLookBackTableName
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(141,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,ISNULL(DFM.vcListItemType,'')
			,ISNULL(DFM.numListID,0)
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,DFM.vcLookBackTableName
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=141 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND ISNULL(DFFM.bitDefault,0) = 1
	END

	DECLARE @j INT = 0
	DECLARE @jCount INT
	DECLARE @numFieldId NUMERIC(18,0)
	DECLARE @vcFieldName AS VARCHAR(50)                                                                                                
	DECLARE @vcAssociatedControlType VARCHAR(10)  
	DECLARE @vcDbColumnName VARCHAR(100)
	DECLARE @Grp_id INT                                          
	--DECLARE @numListID AS NUMERIC(9)       
	DECLARE @bitCustomField BIT       
	SET @jCount = (SELECT COUNT(*) FROM @TempFieldsLeft)
	DECLARE @vcCustomFields VARCHAR(MAX) = ''
	DECLARE @vcCustomWhere VARCHAR(MAX) = ''

	WHILE @j <= @jCount
	BEGIN
		SELECT
			@numFieldId=numFieldID,
			@vcFieldName = vcFieldName,
			@vcAssociatedControlType = vcAssociatedControlType,
			@vcDbColumnName = CONCAT('Cust',numFieldID),
			@bitCustomField=ISNULL(bitCustomField,0),
			@Grp_id = ISNULL(Grp_id,0)
		FROM 
			@TempFieldsLeft
		WHERE 
			numPKID = @j     
		
		
			
		IF @bitCustomField = 1
		BEGIN         
			IF @Grp_id = 5
			BEGIN
				SET @vcCustomFields = CONCAT(@vcCustomFields,CONCAT(',dbo.GetCustFldValueOppItems(',@numFieldId,',T1.numOppItemID,T1.numItemCode)'),' [',@vcDbColumnName,']')
			END
			ELSE
			BEGIN
				IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea' 
				BEGIN
					SET @vcCustomFields = CONCAT(@vcCustomFields,',CFW',@numFieldId,'.Fld_Value  [',@vcDbColumnName,']')
             
					SET @vcCustomWhere = CONCAT(@vcCustomWhere,' left Join CFW_FLD_Values_Opp CFW',@numFieldId
									,' ON CFW' , @numFieldId , '.Fld_Id='
									,@numFieldId
									, ' and CFW' , @numFieldId
									, '.RecId=T1.numOppID')                                                         
				END   
				ELSE IF @vcAssociatedControlType = 'CheckBox' 
				BEGIN
					SET @vcCustomFields =CONCAT( @vcCustomFields
						, ',case when isnull(CFW'
						, @numFieldId
						, '.Fld_Value,0)=0 then 0 when isnull(CFW'
						, @numFieldId
						, '.Fld_Value,0)=1 then 1 end   ['
						,  @vcDbColumnName
						, ']')               
 
					SET @vcCustomWhere = CONCAT(@vcCustomWhere
						, ' left Join CFW_FLD_Values_Opp CFW'
						, @numFieldId
						, ' ON CFW',@numFieldId,'.Fld_Id='
						, @numFieldId
						, ' and CFW'
						, @numFieldId
						, '.RecId=T1.numOppID   ')                                                    
				END                
				ELSE IF @vcAssociatedControlType = 'DateField' 
				BEGIN
					SET @vcCustomFields = CONCAT(@vcCustomFields
						, ',dbo.FormatedDateFromDate(CFW'
						, @numFieldId
						, '.Fld_Value,'
						, @numDomainId
						, ')  [',@vcDbColumnName ,']' )  
					                  
					SET @vcCustomWhere = CONCAT(@vcCustomWhere
						, ' left Join CFW_FLD_Values_Opp CFW'
						, @numFieldId
						, ' on CFW', @numFieldId, '.Fld_Id='
						, @numFieldId
						, ' and CFW'
						, @numFieldId
						, '.RecId=T1.numOppID   ' )                                                        
				END                
				ELSE IF @vcAssociatedControlType = 'SelectBox' 
				BEGIN
					SET @vcCustomFields = CONCAT(@vcCustomFields,',L',@numFieldId,'.vcData',' [',@vcDbColumnName,']')
					                                                    
					SET @vcCustomWhere = CONCAT(@vcCustomWhere
						, ' left Join CFW_FLD_Values_Opp CFW'
						, @numFieldId
						, ' on CFW',@numFieldId ,'.Fld_Id='
						, @numFieldId
						, ' and CFW'
						, @numFieldId
						, '.RecId=T1.numOppID    ')     
					                                                    
					SET @vcCustomWhere = CONCAT(@vcCustomWhere
						, ' left Join ListDetails L'
						, @numFieldId
						, ' on L'
						, @numFieldId
						, '.numListItemID=CFW'
						, @numFieldId
						, '.Fld_Value' )              
				END
				ELSE
				BEGIN
					SET @vcCustomFields = CONCAT(@vcCustomFields,CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',T1.numOppID)'),' [',@vcDbColumnName,']')
				END 
			END
			
		END

		SET @j = @j + 1
	END

	CREATE TABLE #TEMPMSRecords
	(
		numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numOppBizDocID NUMERIC(18,0)
	)

	DECLARE @vcSQL NVARCHAR(MAX)
	DECLARE @vcSQLFinal NVARCHAR(MAX)
	
	SET @vcSQL = CONCAT('INSERT INTO #TEMPMSRecords
						(
							numOppID
							,numOppItemID
							,numOppBizDocID
						)
						SELECT
							OpportunityMaster.numOppId
							,',(CASE WHEN @bitGroupByOrder = 1 THEN '0' ELSE 'OpportunityItems.numoppitemtCode' END),'
							,',(CASE 
									WHEN @numViewID = 4 OR @numViewID = 6
									THEN 'OpportunityBizDocs.numOppBizDocsId'  
									ELSE '0' 
								END),'
						FROM
							OpportunityMaster
						INNER JOIN
							DivisionMaster
						ON
							OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
						INNER JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						INNER JOIN 
							OpportunityItems
						ON
							OpportunityMaster.numOppId = OpportunityItems.numOppId
						INNER JOIN
							Item
						ON
							OpportunityItems.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
						',
						(CASE 
							WHEN ISNULL(@numBatchID,0) > 0 
							THEN 
								' INNER JOIN 
										MassSalesFulfillmentBatchOrders
								ON
									OpportunityMaster.numOppId = MassSalesFulfillmentBatchOrders.numOppID
									AND (OpportunityItems.numOppItemtCode = MassSalesFulfillmentBatchOrders.numOppItemID OR ISNULL(MassSalesFulfillmentBatchOrders.numOppItemID,0) = 0)'
							ELSE 
								'' 
						END)	
						,
						(CASE 
							WHEN @numViewID = 4 
							THEN
								'INNER JOIN 
									OpportunityBizDocs
								ON
									OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
									AND OpportunityBizDocs.numBizDocID=287'
							WHEN @numViewID = 6
							THEN
								CONCAT('INNER JOIN 
											OpportunityBizDocs
										ON
											OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
											AND OpportunityBizDocs.numBizDocID=',(CASE @tintPrintBizDocViewMode WHEN 2 THEN 29397 WHEN 3 THEN 296 WHEN 4 THEN 287 ELSE 55206 END))
							ELSE ''

						END)
						,'
						WHERE
							OpportunityMaster.numDomainId=@numDomainID
							AND OpportunityMaster.tintOppType = 1
							AND OpportunityMaster.tintOppStatus = 1
							AND ISNULL(OpportunityMaster.tintshipped,0) = 0
							AND 1 = (CASE 
										WHEN @numViewID IN (1,2) AND ISNULL(@bitRemoveBO,0) = 1
										THEN (CASE 
												WHEN (Item.charItemType = ''p'' AND ISNULL(OpportunityItems.bitDropShip, 0) = 0 AND ISNULL(Item.bitAsset,0)=0)
												THEN 
													(CASE WHEN dbo.CheckOrderItemInventoryStatus(OpportunityItems.numItemCode,(CASE 
				WHEN @numViewID = 1
				THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 2
				THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) END)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 3
				THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL((SELECT 
																																											SUM(OpportunityBizDocItems.numUnitHour)
																																										FROM
																																											OpportunityBizDocs
																																										INNER JOIN
																																											OpportunityBizDocItems
																																										ON
																																											OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																										WHERE
																																											OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																											AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
					 																																					AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
				
				ELSE OpportunityItems.numUnitHour
			END),OpportunityItems.numoppitemtCode,OpportunityItems.numWarehouseItmsID,(CASE WHEN ISNULL(Item.bitKitParent,0) = 1 THEN 1 ELSE 0 END)) = 1 THEN 0 ELSE 1 END)
												ELSE 1
											END) 
										ELSE 1
									END)		
							AND 1 = (CASE WHEN LEN(ISNULL(@vcOrderStauts,'''')) > 0 THEN (CASE WHEN OpportunityMaster.numStatus ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) ELSE 1 END)
							AND 1 = (CASE 
										WHEN LEN(ISNULL(@vcFilterValue,'''')) > 0
										THEN
											CASE 
												WHEN @tintFilterBy=1 --Item Classification
												THEN (CASE WHEN Item.numItemClassification ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END)
												ELSE 1
											END
										ELSE 1
									END)
							AND 1 = (CASE
										WHEN @numViewID = 1 THEN (CASE WHEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) > 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 THEN (CASE 
																		WHEN @tintPackingViewMode = 1 
																		THEN (CASE 
																				WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=ISNULL(@numShippingServiceItemID,0)),0) = 0 
																				THEN 1 
																				ELSE 0 
																			END)
																		WHEN @tintPackingViewMode = 2
																		THEN (CASE 
																				WHEN ',(CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN 'ISNULL(OpportunityItems.numQtyPicked,0)' ELSE 'ISNULL(OpportunityItems.numUnitHour,0)' END),' - ISNULL(OpportunityItems.numQtyShipped,0) > 0
																				THEN 1 
																				ELSE 0 
																			END)
																		WHEN @tintPackingViewMode = 3
																		THEN (CASE 
																				WHEN ',(CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN 'ISNULL(OpportunityItems.numQtyPicked,0)' ELSE 'ISNULL(OpportunityItems.numUnitHour,0)' END),' - ISNULL((SELECT 
																																																													SUM(OpportunityBizDocItems.numUnitHour)
																																																												FROM
																																																													OpportunityBizDocs
																																																												INNER JOIN
																																																													OpportunityBizDocItems
																																																												ON
																																																													OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																																												WHERE
																																																													OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																																													AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
																																																													AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
																				THEN 1 
																				ELSE 0 
																			END)','
																		ELSE 0
																	END)
										WHEN @numViewID = 3
										THEN (CASE 
												WHEN (CASE 
														WHEN ISNULL(@tintInvoicingType,0)=1 
														THEN ISNULL(OpportunityItems.numQtyShipped,0) 
														ELSE ISNULL(OpportunityItems.numUnitHour,0) 
													END) - ISNULL((SELECT 
																		SUM(OpportunityBizDocItems.numUnitHour)
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																		AND OpportunityBizDocs.numBizDocId=287
																		AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0 
												THEN 1
												ELSE 0 
											END)
										WHEN @numViewID = 4 THEN 1
										WHEN @numViewID = 5 THEN 1
										WHEN @numViewID = 6 THEN 1
									END)
							AND 1 = (CASE
										WHEN @numViewID = 1 THEN (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 AND @tintPackingViewMode <> 2 THEN (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 AND @tintPackingViewMode = 2 THEN 1
										WHEN @numViewID = 3 THEN 1
										WHEN @numViewID = 4 THEN 1
										WHEN @numViewID = 5 THEN 1
										WHEN @numViewID = 6 THEN 1
										ELSE 0
									END)',
							(CASE 
								WHEN @numViewID = 5 AND ISNULL(@tintPendingCloseFilter,0) = 2
								THEN ' AND (SELECT 
												COUNT(*) 
											FROM 
											(
												SELECT
													OI.numoppitemtCode,
													ISNULL(OI.numUnitHour,0) AS OrderedQty,
													ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
												FROM
													OpportunityItems OI
												INNER JOIN
													Item I
												ON
													OI.numItemCode = I.numItemCode
												OUTER APPLY
												(
													SELECT
														SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
													FROM
														OpportunityBizDocs
													INNER JOIN
														OpportunityBizDocItems 
													ON
														OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
													WHERE
														OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
														AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
														AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
												) AS TempFulFilled
												WHERE
													OI.numOppID = OpportunityMaster.numOppID
													AND ISNULL(OI.numUnitHour,0) > 0
													AND ISNULL(OI.bitDropShip,0) = 0
											) X
											WHERE
												X.OrderedQty <> X.FulFilledQty) = 0
										AND (SELECT 
												COUNT(*) 
											FROM 
											(
												SELECT
													OI.numoppitemtCode,
													ISNULL(OI.numUnitHour,0) AS OrderedQty,
													ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
												FROM
													OpportunityItems OI
												INNER JOIN
													Item I
												ON
													OI.numItemCode = I.numItemCode
												OUTER APPLY
												(
													SELECT
														SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
													FROM
														OpportunityBizDocs
													INNER JOIN
														OpportunityBizDocItems 
													ON
														OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
													WHERE
														OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
														AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
														AND ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
												) AS TempInvoice
												WHERE
													OI.numOppID = OpportunityMaster.numOppID
													AND ISNULL(OI.numUnitHour,0) > 0
											) X
											WHERE
												X.OrderedQty > X.InvoicedQty) = 0'
								ELSE '' 
							END)
							,(CASE WHEN @numViewID = 4 THEN 'AND 1 = (CASE WHEN ISNULL(OpportunityBizDocs.monDealAmount,0) - ISNULL(OpportunityBizDocs.monAmountPaid,0) > 0 THEN 1 ELSE 0 END)' ELSE '' END)
							,' AND 1 = (CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 3 THEN (CASE WHEN ISNULL(Item.bitContainer,0)=1 THEN 0 ELSE 1 END) ELSE 1 END)
								AND 1 = (CASE WHEN ISNULL(@numWarehouseID,0) > 0 AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID THEN 1 ELSE 0 END) ELSE 1 END)'
								,(CASE WHEN ISNULL(@numShippingZone,0) > 0 THEN ' AND 1 = (CASE 
																				WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),0) > 0 
																				THEN
																					CASE 
																						WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=@numDomainId AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)) AND numShippingZone=@numShippingZone) > 0 
																						THEN 1
																						ELSE 0
																					END
																				ELSE 0 
																			END)' ELSE '' END),
								@vcCustomSearchValue,(CASE WHEN @bitGroupByOrder = 1 THEN CONCAT('GROUP BY OpportunityMaster.numOppId',(CASE WHEN @numViewID = 4 THEN ',OpportunityBizDocs.numOppBizDocsId,OpportunityBizDocs.monAmountPaid' WHEN @numViewID = 6 THEN ',OpportunityBizDocs.numOppBizDocsId'  ELSE '' END)) ELSE '' END));

	EXEC sp_executesql @vcSQL, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @tintPackingMode TINYINT, @bitRemoveBO BIT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @tintPackingMode, @bitRemoveBO;

	If @numViewID = 2 AND @tintPackingViewMode = 3
	BEGIN
		SET @vcSQL = CONCAT('INSERT INTO #TEMPMSRecords
						(
							numOppID
							,numOppItemID
							,numOppBizDocID
						)
						SELECT
							OpportunityMaster.numOppId
							,',(CASE WHEN @bitGroupByOrder = 1 THEN '0' ELSE 'OpportunityItems.numoppitemtCode' END),'
							,OpportunityBizDocs.numOppBizDocsId
						FROM
							OpportunityMaster
						INNER JOIN
							DivisionMaster
						ON
							OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
						INNER JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						INNER JOIN 
							OpportunityBizDocs
						ON
							OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
							AND OpportunityBizDocs.numBizDocID=@numDefaultSalesShippingDoc
						INNER JOIN
							OpportunityBizDocItems
						ON
							OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
						INNER JOIN 
							OpportunityItems
						ON
							OpportunityMaster.numOppId = OpportunityItems.numOppId
							AND OpportunityItems.numoppitemtCode = OpportunityBizDocItems.numOppItemID
						INNER JOIN
							Item
						ON
							OpportunityItems.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID					
						',
						(CASE 
							WHEN ISNULL(@numBatchID,0) > 0 
							THEN 
								' INNER JOIN 
										MassSalesFulfillmentBatchOrders
								ON
									OpportunityMaster.numOppId = MassSalesFulfillmentBatchOrders.numOppID
									AND (OpportunityItems.numOppItemtCode = MassSalesFulfillmentBatchOrders.numOppItemID OR ISNULL(MassSalesFulfillmentBatchOrders.numOppItemID,0) = 0)'
							ELSE 
								'' 
						END)	
						,'
						WHERE
							OpportunityMaster.numDomainId=@numDomainID
							AND OpportunityMaster.tintOppType = 1
							AND OpportunityMaster.tintOppStatus = 1
							AND ISNULL(OpportunityMaster.tintshipped,0) = 0		
							AND 1 = (CASE WHEN LEN(ISNULL(@vcOrderStauts,'''')) > 0 THEN (CASE WHEN OpportunityMaster.numStatus ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) ELSE 1 END)
							AND 1 = (CASE 
										WHEN LEN(ISNULL(@vcFilterValue,'''')) > 0
										THEN
											CASE 
												WHEN @tintFilterBy=1 --Item Classification
												THEN (CASE WHEN Item.numItemClassification ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END)
												ELSE 1
											END
										ELSE 1
									END)
							AND 1 = (CASE 
										WHEN ISNULL((SELECT COUNT(*) FROM ShippingBox WHERE ISNULL(vcShippingLabelImage,'''') <> '''' AND ISNULL(vcTrackingNumber,'''') <> '''' AND numShippingReportId IN (SELECT numShippingReportId FROM ShippingReport WHERE ShippingReport.numOppID=OpportunityMaster.numOppId AND ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId)),0) = 0 
										THEN 1 
										ELSE 0 
									END)
										
							AND 1 = (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)',
							' AND 1 = (CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 3 THEN (CASE WHEN ISNULL(Item.bitContainer,0)=1 THEN 0 ELSE 1 END) ELSE 1 END)
								AND 1 = (CASE WHEN ISNULL(@numWarehouseID,0) > 0 AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID THEN 1 ELSE 0 END) ELSE 1 END)'
								,(CASE WHEN ISNULL(@numShippingZone,0) > 0 THEN ' AND 1 = (CASE 
																				WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),0) > 0 
																				THEN
																					CASE 
																						WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=@numDomainId AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)) AND numShippingZone=@numShippingZone) > 0 
																						THEN 1
																						ELSE 0
																					END
																				ELSE 0 
																			END)' ELSE '' END),
								@vcCustomSearchValue,(CASE WHEN @bitGroupByOrder = 1 THEN ' GROUP BY OpportunityMaster.numOppId,OpportunityBizDocs.numOppBizDocsId,OpportunityBizDocs.monAmountPaid' ELSE '' END));

		EXEC sp_executesql @vcSQL, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @tintPackingMode TINYINT, @bitRemoveBO BIT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @tintPackingMode, @bitRemoveBO;
	END

	SET @numTotalRecords = (SELECT COUNT(*) FROM #TEMPMSRecords)

	IF @bitGroupByOrder = 1
	BEGIN
		
		SET @vcSQLFinal = CONCAT('

		INSERT INTO #TEMPResult
		(
			numOppID
			,numDivisionID
			,numTerID
			,numOppItemID
			,numOppBizDocID
			,bintCreatedDate
			,vcCompanyName
			,vcPoppName
			,numAssignedTo
			,numRecOwner
			,vcBizDocID
			,numStatus
			,txtComments
			,monAmountPaid
			,monAmountToPay
			,intUsedShippingCompany
			,intShippingCompany
			,numPreferredShipVia
			,numPreferredShipService
			,bitPaidInFull
			,numAge
			,dtAnticipatedDelivery
			,vcShipStatus
			,dtExpectedDate
			,dtExpectedDateOrig
			,vcPaymentStatus
			,numShipRate
			,vcShippingLabel
		)
		SELECT
			OpportunityMaster.numOppId
			,OpportunityMaster.numDivisionID
			,ISNULL(DivisionMaster.numTerID,0) numTerID
			,0
			,TEMPOrder.numOppBizDocID
			,(CASE WHEN @numViewID = 6 THEN dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityBizDocs.dtCreatedDate),@numDomainID) ELSE dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate),@numDomainID) END) 
			,ISNULL(CompanyInfo.vcCompanyName,'''')
			,ISNULL(OpportunityMaster.vcPoppName,'''')
			,dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)
			,dbo.fn_GetContactName(OpportunityMaster.numRecOwner)
			,(CASE WHEN @numViewID = 4 OR @numViewID = 6 THEN ISNULL(OpportunityBizDocs.vcBizDocID,''-'') ELSE '''' END)
			,dbo.GetListIemName(OpportunityMaster.numStatus)
			,ISNULL(OpportunityMaster.txtComments,'''')
			,ISNULL(TempPaid.monAmountPaid,0)
			,ISNULL(OpportunityBizDocs.monDealAmount,0) - ISNULL(OpportunityBizDocs.monAmountPaid,0)
			,CONCAT(ISNULL(LDOpp.vcData,''-''),'', '',ISNULL(SSOpp.vcShipmentService,''-'')) 
			,CONCAT(ISNULL(LDDiv.vcData,''-''),'', '',ISNULL(SSDiv.vcShipmentService,''-''))
			,ISNULL(DivisionMaster.intShippingCompany,0)
			,ISNULL(DivisionMaster.numDefaultShippingServiceID,0)
			,(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)
			,CONCAT(''<span style="color:'',(CASE
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 4  
						THEN ''#00ff00''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 4 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 8)
						THEN ''#00b050''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 8 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 12)
						THEN ''#3399ff''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 12 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 24)
						THEN ''#ff9900''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) BETWEEN 1 AND 2  
						THEN ''#9b3596''
						ELSE ''#ff0000''
					END),''">'',CONCAT(DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24,''d '',DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24,''h''),''</span>'')
			,'''' dtAnticipatedDelivery
			,'''' vcShipStatus
			,dbo.FormatedDateFromDate(OpportunityMaster.dtExpectedDate,@numDomainID)
			,OpportunityMaster.dtExpectedDate
			,'''' vcPaymentStatus
			,ISNULL(CAST((SELECT SUM(monTotAmount) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=@numShippingServiceItemID) AS VARCHAR),'''')
			,(CASE WHEN @numViewID=6 THEN STUFF((SELECT 
													CONCAT('', '',''<a style="text-decoration: none" href="javascript:void(0);" onclick="openReportList('',ShippingReport.numOppID ,'','',ShippingReport.numOppBizDocId,'','',ShippingReport.numShippingReportId,'');"><img src="../images/barcode_generated_22x22.png"></a>'')
												FROM 
													ShippingReport
												INNER JOIN
													ShippingBox 
												ON 
													ShippingReport.numShippingReportId=ShippingBox.numShippingReportId 
												WHERE 
													ShippingReport.numOppID= OpportunityMaster.numOppId
													AND ShippingReport.numOppBizDocId= OpportunityBizDocs.numOppBizDocsId
													AND LEN(ISNULL(vcShippingLabelImage,'''')) > 0
												for xml path('''')),1,2,'''') ELSE '''' END)
		FROM
			#TEMPMSRecords TEMPOrder
		INNER JOIN
			OpportunityMaster
		ON
			TEMPOrder.numOppID = OpportunityMaster.numOppId
		INNER JOIN
			DivisionMaster
		ON
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		LEFT JOIN
			OpportunityBizDocs
		ON
			OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
			AND OpportunityBizDocs.numOppBizDocsId = ISNULL(TEMPOrder.numOppBizDocID,0)
		LEFT JOIN
			ListDetails  LDOpp
		ON
			LDOpp.numListID = 82
			AND (LDOpp.numDomainID = @numDomainID OR LDOpp.constFlag=1)
			AND ISNULL(OpportunityMaster.intUsedShippingCompany,0) = LDOpp.numListItemID
		LEFT JOIN
			ShippingService AS SSOpp
		ON
			(SSOpp.numDomainID=@numDomainID OR ISNULL(SSOpp.numDomainID,0)=0) 
			AND OpportunityMaster.numShippingService = SSOpp.numShippingServiceID
		LEFT JOIN
			ListDetails  LDDiv
		ON
			LDDiv.numListID = 82
			AND (LDDiv.numDomainID = @numDomainID OR LDDiv.constFlag=1)
			AND ISNULL(DivisionMaster.intShippingCompany,0) = LDDiv.numListItemID
		LEFT JOIN
			ShippingService AS SSDiv
		ON
			(SSDiv.numDomainID=@numDomainID OR ISNULL(SSDiv.numDomainID,0)=0) 
			AND ISNULL(DivisionMaster.numDefaultShippingServiceID,0) = SSDiv.numShippingServiceID
		OUTER APPLY
		(
			SELECT 
				SUM(OpportunityBizDocs.monAmountPaid) monAmountPaid
			FROM 
				OpportunityBizDocs 
			WHERE 
				OpportunityBizDocs.numOppId=OpportunityMaster.numOppId 
				AND OpportunityBizDocs.numBizDocId=287
		) TempPaid
		ORDER BY ',
		(CASE @vcSortColumn
			WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName '
			WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(OpportunityMaster.numStatus)'
			WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName'
			WHEN 'OpportunityMaster.numAssignedTo' THEN 'dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)'
			WHEN 'OpportunityMaster.numRecOwner' THEN 'dbo.fn_GetContactName(OpportunityMaster.numRecOwner)'
			WHEN 'OpportunityMaster.numAge' THEN 'OpportunityMaster.bintCreatedDate'
			WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TempPaid.monAmountPaid'
			WHEN 'OpportunityBizDocs.vcBizDocID' THEN 'OpportunityBizDocs.vcBizDocID'
			WHEN 'OpportunityMaster.bitPaidInFull' THEN '(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)'
			ELSE (CASE WHEN @numViewID = 6 THEN 'OpportunityBizDocs.dtCreatedDate' ELSE 'OpportunityMaster.bintCreatedDate' END)
		END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'DESC' END),' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY')

		--PRINT CAST(@vcSQLFinal AS NTEXT)

		EXEC sp_executesql @vcSQLFinal, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @ClientTimeZoneOffset INT, @tintPackingMode TINYINT, @numPageIndex INT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @ClientTimeZoneOffset, @tintPackingMode, @numPageIndex;
	END
	ELSE
	BEGIN
		SET @vcSQLFinal = CONCAT('

		INSERT INTO #TEMPResult
		(
			numOppID
			,numDivisionID
			,numTerID
			,numOppItemID
			,numOppBizDocID
			,bintCreatedDate
			,numItemCode
			,vcCompanyName
			,vcPoppName
			,numAssignedTo
			,numRecOwner
			,vcBizDocID
			,numStatus
			,txtComments
			,numBarCodeId
			,vcLocation
			,vcItemName
			,vcItemDesc
			,numQtyShipped
			,numUnitHour
			,monAmountPaid
			,numItemGroup
			,numItemClassification
			,vcSKU
			,charItemType
			,vcAttributes
			,vcNotes
			,vcItemReleaseDate
			,dtItemReceivedDate
			,intUsedShippingCompany
			,intShippingCompany
			,numPreferredShipVia
			,numPreferredShipService
			,vcInvoiced
			,vcInclusionDetails
			,bitPaidInFull
			,numAge
			,dtAnticipatedDelivery
			,vcShipStatus
			,numRemainingQty
			,dtExpectedDate
			,dtExpectedDateOrig
			,numQtyPicked
			,numQtyPacked
			,vcPaymentStatus
			,numShipRate
			,vcShippingLabel
		)
		SELECT
			OpportunityMaster.numOppId
			,OpportunityMaster.numDivisionID
			,ISNULL(DivisionMaster.numTerID,0) numTerID
			,OpportunityItems.numoppitemtCode
			,TEMPOrder.numOppBizDocID
			,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate),@numDomainID)
			,Item.numItemCode
			,ISNULL(CompanyInfo.vcCompanyName,'''')
			,ISNULL(OpportunityMaster.vcPoppName,'''')
			,dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)
			,dbo.fn_GetContactName(OpportunityMaster.numRecOwner)
			,ISNULL(OpportunityBizDocs.vcBizDocID,'''')
			,dbo.GetListIemName(OpportunityMaster.numStatus)
			,ISNULL(OpportunityMaster.txtComments,'''')
			,ISNULL(Item.numBarCodeId,'''')
			,ISNULL(WarehouseLocation.vcLocation,'''')
			,ISNULL(Item.vcItemName,'''')
			,ISNULL(OpportunityItems.vcItemDesc,'''')
			,ISNULL(OpportunityItems.numQtyShipped,0)
			,ISNULL(OpportunityItems.numUnitHour,0)
			,ISNULL(TempPaid.monAmountPaid,0)
			,ISNULL(ItemGroups.vcItemGroup,'''')
			,dbo.GetListIemName(Item.numItemClassification)
			,ISNULL(Item.vcSKU,'''')
			,(CASE 
				WHEN Item.charItemType=''P''  THEN ''Inventory Item''
				WHEN Item.charItemType=''N'' THEN ''Non Inventory Item'' 
				WHEN Item.charItemType=''S'' THEN ''Service'' 
				WHEN Item.charItemType=''A'' THEN ''Accessory'' 
			END)
			,ISNULL(OpportunityItems.vcAttributes,'''')
			,ISNULL(OpportunityItems.vcNotes,'''')
			,dbo.FormatedDateFromDate(OpportunityItems.ItemReleaseDate,@numDomainID)
			,dbo.FormatedDateFromDate((SELECT MAX(OMInner.dtItemReceivedDate) FROM OpportunityMaster OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numOppID=OIInner.numOppID WHERE OMInner.tintOppType=2 AND OMInner.tintOppStatus=1 AND OIInner.numItemCode=OpportunityItems.numItemCode),@numDomainID)
			,CONCAT(ISNULL(LDOpp.vcData,''-''),'', '',ISNULL(SSOpp.vcShipmentService,''-'')) 
			,CONCAT(ISNULL(LDDiv.vcData,''-''),'', '',ISNULL(SSDiv.vcShipmentService,''-''))
			,ISNULL(DivisionMaster.intShippingCompany,0)
			,ISNULL(DivisionMaster.numDefaultShippingServiceID,0)
			,ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)
			,ISNULL(OpportunityItems.vcInclusionDetail,'''')
			,(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)
			,CONCAT(''<span style="color:'',(CASE
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 4  
						THEN ''#00ff00''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 4 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 8)
						THEN ''#00b050''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 8 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 12)
						THEN ''#3399ff''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 12 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 24)
						THEN ''#ff9900''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) BETWEEN 1 AND 2  
						THEN ''#9b3596''
						ELSE ''#ff0000''
					END),''">'',CONCAT(DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24,''d '',DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24,''h''),''</span>'')
			,'''' dtAnticipatedDelivery
			,'''' vcShipStatus
			,(CASE 
				WHEN @numViewID = 1
				THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 2
				THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) END)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 3 AND ISNULL(TEMPOrder.numOppBizDocID,0) = 0
				THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL((SELECT 
																																											SUM(OpportunityBizDocItems.numUnitHour)
																																										FROM
																																											OpportunityBizDocs
																																										INNER JOIN
																																											OpportunityBizDocItems
																																										ON
																																											OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																										WHERE
																																											OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																											AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
					 																																					AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 3 AND ISNULL(OpportunityBizDocs.numOppBizDocsId,0) > 0
				THEN ISNULL(OpportunityBizDocItems.numUnitHour,0)
				WHEN @numViewID = 3
				THEN (CASE 
						WHEN ISNULL(@tintInvoicingType,0)=1 
						THEN ISNULL(OpportunityItems.numQtyShipped,0) 
						ELSE ISNULL(OpportunityItems.numUnitHour,0) 
					END) - ISNULL((SELECT 
										SUM(OpportunityBizDocItems.numUnitHour)
									FROM
										OpportunityBizDocs
									INNER JOIN
										OpportunityBizDocItems
									ON
										OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
									WHERE
										OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
										AND OpportunityBizDocs.numBizDocId=287
										AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
				ELSE 0
			END) numRemainingQty
			,dbo.FormatedDateFromDate(OpportunityMaster.dtExpectedDate,@numDomainID)
			,OpportunityMaster.dtExpectedDate
			,ISNULL(OpportunityItems.numQtyPicked,0)
			,ISNULL(TempInvoiced.numInvoicedQty,0)
			,'''' vcPaymentStatus
			,ISNULL(CAST((SELECT SUM(monTotAmount) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=@numShippingServiceItemID) AS VARCHAR),'''')
			,(CASE WHEN @numViewID=6 THEN STUFF((SELECT 
													CONCAT('', '',''<a style="text-decoration: none" href="javascript:void(0);" onclick="openReportList('',ShippingReport.numOppID ,'','',ShippingReport.numOppBizDocId,'','',ShippingReport.numShippingReportId,'');"><img src="../images/barcode_generated_22x22.png"></a>'')
												FROM 
													ShippingReport
												INNER JOIN
													ShippingBox 
												ON 
													ShippingReport.numShippingReportId=ShippingBox.numShippingReportId 
												WHERE 
													ShippingReport.numOppID= OpportunityMaster.numOppId
													AND ShippingReport.numOppBizDocId= OpportunityBizDocs.numOppBizDocsId
													AND LEN(ISNULL(vcShippingLabelImage,'''')) > 0
												for xml path('''')),1,2,'''') ELSE '''' END)
		FROM
			#TEMPMSRecords TEMPOrder
		INNER JOIN
			OpportunityMaster
		ON
			TEMPOrder.numOppID = OpportunityMaster.numOppId
		INNER JOIN
			DivisionMaster
		ON
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		INNER JOIN 
			OpportunityItems
		ON
			OpportunityMaster.numOppId = OpportunityItems.numOppId
			AND TEMPOrder.numOppItemID = OpportunityItems.numoppitemtCode
		LEFT JOIN
			OpportunityBizDocs 
		ON
			TEMPOrder.numOppId = OpportunityBizDocs.numOppID
			AND TEMPOrder.numOppBizDocID = OpportunityBizDocs.numOppBizDocsId
		LEFT JOIN
			OpportunityBizDocItems
		ON
			OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocId
			AND OpportunityItems.numoppitemtCode = OpportunityBizDocItems.numOppItemID
		INNER JOIN
			Item
		ON
			OpportunityItems.numItemCode = Item.numItemCode
		LEFT JOIN
			ItemGroups
		ON
			Item.numItemGroup = ItemGroups.numItemGroupID
		LEFT JOIN
			ListDetails  LDOpp
		ON
			LDOpp.numListID = 82
			AND (LDOpp.numDomainID = @numDomainID OR LDOpp.constFlag=1)
			AND ISNULL(OpportunityMaster.intUsedShippingCompany,0) = LDOpp.numListItemID
		LEFT JOIN
			ShippingService AS SSOpp
		ON
			(SSOpp.numDomainID=@numDomainID OR ISNULL(SSOpp.numDomainID,0)=0) 
			AND OpportunityMaster.numShippingService = SSOpp.numShippingServiceID
		LEFT JOIN
			ListDetails  LDDiv
		ON
			LDDiv.numListID = 82
			AND (LDDiv.numDomainID = @numDomainID OR LDDiv.constFlag=1)
			AND ISNULL(DivisionMaster.intShippingCompany,0) = LDDiv.numListItemID
		LEFT JOIN
			ShippingService AS SSDiv
		ON
			(SSDiv.numDomainID=@numDomainID OR ISNULL(SSDiv.numDomainID,0)=0) 
			AND ISNULL(DivisionMaster.numDefaultShippingServiceID,0) = SSDiv.numShippingServiceID
		LEFT JOIN
			WareHouseItems
		ON
			OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
		LEFT JOIN
			WarehouseLocation
		ON
			WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
		OUTER APPLY
		(
			SELECT 
				SUM(OpportunityBizDocItems.numUnitHour) numInvoicedQty
			FROM 
				OpportunityBizDocs
			INNER JOIN
				OpportunityBizDocItems
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE 
				OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
				AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
				AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode
		) TempInvoiced
		OUTER APPLY
		(
			SELECT 
				SUM(OpportunityBizDocs.monAmountPaid) monAmountPaid
			FROM 
				OpportunityBizDocs 
			WHERE 
				OpportunityBizDocs.numOppId=OpportunityMaster.numOppId 
				AND OpportunityBizDocs.numBizDocId=287
		) TempPaid
		WHERE
			1 = (CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 3 THEN (CASE WHEN ISNULL(Item.bitContainer,0)=1 THEN 0 ELSE 1 END) ELSE 1 END)
			AND 1 = (CASE WHEN ISNULL(TEMPOrder.numOppBizDocID,0) > 0 THEN (CASE WHEN OpportunityBizDocItems.numOppItemID > 0 THEN 1 ELSE 0 END) ELSE 1 END)',
			(CASE WHEN @numViewID=2 AND @tintPackingViewMode=3 THEN ' AND 1 = (CASE WHEN ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 AND ISNULL(OpportunityItems.bitDropShip,0) = 0 THEN 1 ELSE 0 END)' ELSE '' END),'
		ORDER BY ',
		(CASE @vcSortColumn
			WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName' 
			WHEN 'Item.charItemType' THEN '(CASE 
												WHEN Item.charItemType=''P''  THEN ''Inventory Item''
												WHEN Item.charItemType=''N'' THEN ''Non Inventory Item'' 
												WHEN Item.charItemType=''S'' THEN ''Service''
												WHEN Item.charItemType=''A'' THEN ''Accessory''
											END)'
			WHEN 'Item.numBarCodeId' THEN 'Item.numBarCodeId'
			WHEN 'Item.numItemClassification' THEN 'dbo.GetListIemName(Item.numItemClassification)'
			WHEN 'Item.numItemGroup' THEN 'ISNULL(ItemGroups.vcItemGroup,'''')'
			WHEN 'Item.vcSKU' THEN 'Item.vcSKU'
			WHEN 'OpportunityItems.vcItemName' THEN 'Item.vcItemName'
			WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(OpportunityMaster.numStatus)'
			WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName'
			WHEN 'OpportunityMaster.numAssignedTo' THEN 'dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)'
			WHEN 'OpportunityMaster.numRecOwner' THEN 'dbo.fn_GetContactName(OpportunityMaster.numRecOwner)'
			WHEN 'WareHouseItems.vcLocation' THEN 'WarehouseLocation.vcLocation'
			WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TempPaid.monAmountPaid'
			WHEN 'OpportunityBizDocs.vcBizDocID' THEN 'OpportunityBizDocs.vcBizDocID'
			WHEN 'OpportunityItems.numQtyPacked' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)'
			WHEN 'OpportunityItems.numQtyPicked' THEN 'OpportunityItems.numQtyPicked'
			WHEN 'OpportunityItems.numQtyShipped' THEN 'OpportunityItems.numQtyShipped'
			WHEN 'OpportunityItems.numRemainingQty' THEN '(CASE 
																WHEN @numViewID = 1
																THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
																WHEN @numViewID = 2
																THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)
																ELSE 0
															END)'
			WHEN 'OpportunityItems.numUnitHour' THEN 'OpportunityItems.numUnitHour'
			WHEN 'OpportunityItems.vcInvoiced' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)'
			WHEN 'OpportunityMaster.dtExpectedDate' THEN 'OpportunityMaster.dtExpectedDate'
			WHEN 'OpportunityItems.vcItemReleaseDate' THEN 'OpportunityItems.ItemReleaseDate'
			WHEN 'OpportunityMaster.bintCreatedDate' THEN 'OpportunityMaster.bintCreatedDate'
			WHEN 'OpportunityMaster.numAge' THEN 'OpportunityMaster.bintCreatedDate'
			WHEN 'OpportunityMaster.bitPaidInFull' THEN '(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)'
			WHEN 'OpportunityMaster.dtItemReceivedDate' THEN '(SELECT MAX(OMInner.dtItemReceivedDate) FROM OpportunityMaster OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numOppID=OIInner.numOppID WHERE OMInner.tintOppType=2 AND OMInner.tintOppStatus=1 AND OIInner.numItemCode=OpportunityItems.numItemCode)'
			ELSE 'OpportunityMaster.bintCreatedDate'
		END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'ASC' END),' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY')

		PRINT CAST(@vcSQLFinal AS NTEXT)

		EXEC sp_executesql @vcSQLFinal, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @ClientTimeZoneOffset INT, @tintPackingMode TINYINT, @numPageIndex INT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @ClientTimeZoneOffset, @tintPackingMode, @numPageIndex;
	END

	SET @vcSQLFinal = CONCAT('SELECT T1.*',@vcCustomFields,' FROM #TEMPResult T1',@vcCustomWhere)

	EXEC sp_executesql @vcSQLFinal

	SELECT * FROM @TempFieldsLeft ORDER BY intRowNum

	IF OBJECT_ID('tempdb..#TEMPMSRecords') IS NOT NULL DROP TABLE #TEMPMSRecords
	IF OBJECT_ID('tempdb..#TEMPResult') IS NOT NULL DROP TABLE #TEMPResult
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetRecordsForPicking')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetRecordsForPicking
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetRecordsForPicking]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@bitGroupByOrder BIT
	,@numBatchID NUMERIC(18,0)
	,@vcSelectedRecords VARCHAR(MAX)
)
AS
BEGIN
	DECLARE @bitGeneratePickListByOrder BIT = 0

	IF EXISTS (SELECT ID FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID AND numUserCntID = @numUserCntID)
	BEGIN
		SELECT @bitGeneratePickListByOrder=ISNULL(bitGeneratePickListByOrder,0) FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID AND numUserCntID = @numUserCntID
	END
	

	DECLARE @TempFieldsRight TABLE
	(
		ID VARCHAR(50)
		,numFieldID NUMERIC(18,0)
		,vcFieldName VARCHAR(100)
		,vcOrigDbColumnName VARCHAR(100)
		,bitAllowSorting BIT
		,bitAllowFiltering BIT
		,vcAssociatedControlType VARCHAR(20)
		,bitCustomField BIT
		,intRowNum INT
		,intColumnWidth FLOAT
	)

	INSERT INTO @TempFieldsRight
	(
		ID
		,numFieldID
		,vcFieldName
		,vcOrigDbColumnName
		,bitAllowSorting
		,bitAllowFiltering
		,vcAssociatedControlType
		,bitCustomField
		,intRowNum
		,intColumnWidth
	)
	SELECT
		CONCAT(142,'~',DFM.numFieldID,'~',0)
		,DFM.numFieldID
		,DFFM.vcFieldName
		,DFM.vcOrigDbColumnName
		,DFFM.bitAllowSorting
		,DFFM.bitAllowFiltering
		,DFM.vcAssociatedControlType
		,0
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		DycFormField_Mapping DFFM
	ON
		DFCD.numFormId = DFFM.numFormID
		AND DFCD.numFieldId = DFFM.numFieldID
	INNER JOIN
		DycFieldMaster DFM
	ON
		DFFM.numFieldID = DFM.numFieldId
	WHERE 
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=142 
		AND ISNULL(DFFM.bitDeleted,0) = 0
		AND ISNULL(DFFM.bitSettingField,0) = 1
		AND ISNULL(DFCD.bitCustom,0) = 0

	IF (SELECT COUNT(*) FROM @TempFieldsRight) = 0
	BEGIN
		INSERT INTO @TempFieldsRight
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(142,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=142 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND (ISNULL(DFFM.bitDefault,0) = 1 OR ISNULL(DFFM.bitRequired,0) = 1)
	END
	ELSE
	BEGIN
		INSERT INTO @TempFieldsRight
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(142,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=142 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND ISNULL(DFFM.bitRequired,0) = 1
			AND DFM.numFieldId NOT IN (SELECT T1.numFieldID FROM @TempFieldsRight T1)
	END


	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
	)

	IF ISNULL(@numBatchID,0) > 0
	BEGIN
		INSERT INTO @TEMP
		(
			numOppID
			,numOppItemID
		)
		SELECT
			numOppID
			,ISNULL(numOppItemID,0)
		FROM
			MassSalesFulfillmentBatchOrders
		WHERE
			numBatchID=@numBatchID
	END
	ELSE
	BEGIN
		INSERT INTO @TEMP
		(
			numOppID
			,numOppItemID
		)
		SELECT
			CAST(SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)) AS NUMERIC(18,0)) numOppID
			,CAST(SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1,LEN(OutParam)) AS NUMERIC(18,0)) numOppItemID
		FROM
			dbo.SplitString(@vcSelectedRecords,',')
	END

	DECLARE @TEMPItems TABLE
	(	
		ID NUMERIC(18,0)
		,numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numWarehouseID NUMERIC(18,0)
		,vcWarehouse VARCHAR(300)
		,vcLocation VARCHAR(300)
		,vcKitChildItems VARCHAR(MAX)
		,vcKitChildKitItems VARCHAR(MAX)
		,vcPoppName VARCHAR(300)
		,numBarCodeId VARCHAR(300)
		,vcItemName VARCHAR(300)
		,vcItemDesc VARCHAR(MAX)
		,vcSKU VARCHAR(300)
		,vcPathForTImage VARCHAR(MAX)
		,vcInclusionDetails VARCHAR(MAX)
		,numRemainingQty FLOAT
		,numQtyPicked FLOAT
	)

	INSERT INTO @TEMPItems
	(	
		ID
		,numOppID
		,numOppItemID 
		,numItemCode
		,numWarehouseID
		,vcWarehouse 
		,vcLocation
		,vcKitChildItems
		,vcKitChildKitItems
		,vcPoppName
		,numBarCodeId
		,vcItemName
		,vcItemDesc
		,vcSKU
		,vcPathForTImage
		,vcInclusionDetails
		,numRemainingQty
		,numQtyPicked
	)
	SELECT 
		T1.ID
		,OpportunityMaster.numOppID
		,OpportunityItems.numoppitemtCode
		,OpportunityItems.numItemCode
		,WareHouseItems.numWareHouseID
		,Warehouses.vcWareHouse
		,WarehouseLocation.vcLocation
		,STUFF((SELECT CONCAT(', ',OpportunityItems.numItemCode,'-',OKI.numChildItemID) FROM OpportunityKitItems OKI WHERE OKI.numOppItemID=1 ORDER BY OKI.numChildItemID FOR XML PATH('')),1,1,'') AS vcKitChildItems
		,STUFF((SELECT CONCAT(', ',OpportunityItems.numItemCode,'-',OKI.numChildItemID,'-',OKCI.numItemID) FROM OpportunityKitChildItems OKCI INNER JOIN OpportunityKitItems OKI ON OKCI.numOppChildItemID = OKI.numOppChildItemID  WHERE OKCI.numOppItemID=1 ORDER BY OKI.numChildItemID,OKCI.numItemID FOR XML PATH('')),1,1,'') vcKitChildKitItems
		,OpportunityMaster.vcPoppName
		,Item.numBarCodeId
		,Item.vcItemName
		,Item.txtItemDesc AS vcItemDesc
		,Item.vcSKU
		,ISNULL(ItemImages.vcPathForTImage,'') vcPathForTImage
		,ISNULL(OpportunityItems.vcInclusionDetail,'') AS vcInclusionDetails
		,ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) numRemainingQty
		,0 numQtyPicked
	FROM
		@TEMP T1
	INNER JOIN
		OpportunityMaster
	ON
		OpportunityMaster.numOppID= T1.numOppID
	INNER JOIN
		OpportunityItems
	ON
		OpportunityMaster.numOppID = OpportunityItems.numOppID
		AND (OpportunityItems.numOppItemtcode = T1.numOppItemID OR ISNULL(T1.numOppItemID,0) = 0)
	INNER JOIN
		Item 
	ON
		OpportunityItems.numItemCode = Item.numItemCode
	LEFT JOIN
		ItemImages
	ON
		ItemImages.numDomainId=@numDomainID
		AND ItemImages.numItemCode = Item.numItemCode
		AND ItemImages.bitDefault=1 
		AND ItemImages.bitIsImage=1
	INNER JOIN
		WareHouseItems
	ON
		OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
	INNER JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	LEFT JOIN
		WarehouseLocation
	ON
		WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
	WHERE
		OpportunityMaster.numDomainID=@numDomainID
		AND ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) > 0
	ORDER BY
		T1.ID, OpportunityItems.numSortOrder

	IF @bitGeneratePickListByOrder = 1
	BEGIN
		SELECT
			TEMP.numOppId
			,TEMP.numOppItemID 
			,TEMP.numItemCode
			,TEMP.numWareHouseID
			,TEMP.vcKitChildItems
			,TEMP.vcKitChildKitItems
			,CONCAT(TEMP.numOppItemID,'(',TEMP.numRemainingQty,')') vcOppItemIDs
			,vcPoppName
			,numBarCodeId
			,CONCAT(TEMP.vcWarehouse, (CASE WHEN LEN(ISNULL(TEMP.vcLocation,'')) > 0 THEN CONCAT(' (',TEMP.vcLocation,')') ELSE '' END)) vcLocation
			,vcItemName
			,vcItemDesc
			,vcSKU
			,dbo.fn_GetItemAttributes(@numDomainID,TEMP.numItemCode) vcAttributes
			,vcPathForTImage
			,TEMP.vcInclusionDetails
			,TEMP.numRemainingQty
			,0 numQtyPicked
			,(CASE 
					WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner INNER JOIN WarehouseLocation WL ON WIInner.numWLocationID=WL.numWLocationID WHERE WIInner.numDomainID=@numDomainID AND WIInner.numItemID=TEMP.numItemCode AND WIInner.numWareHouseID = TEMP.numWareHouseID)
					THEN CONCAT('[',STUFF((SELECT 
												CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "Available":',(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)),', "Location":"',WL.vcLocation,'"}') 
											FROM 
												WareHouseItems WIInner
											INNER JOIN
												WarehouseLocation WL
											ON
												WIInner.numWLocationID = WL.numWLocationID
											WHERE 
												WIInner.numDomainID=@numDomainID 
												AND WIInner.numItemID=TEMP.numItemCode 
												AND WIInner.numWareHouseID = TEMP.numWareHouseID
											ORDER BY
												WL.vcLocation
											FOR XML PATH('')),1,1,''),']')
					ELSE CONCAT('[',STUFF((SELECT 
										CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "Available":',(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)),', "Location":''''}')
									FROM 
										WareHouseItems WIInner
									WHERE 
										WIInner.numDomainID=@numDomainID 
										AND WIInner.numItemID=TEMP.numItemCode
										AND WIInner.numWareHouseID = TEMP.numWareHouseID
									FOR XML PATH('')),1,1,''),']')
				END)  vcWarehouseLocations
		FROM
			@TEMPItems TEMP
		ORDER BY
			ID
	END
	ELSE
	BEGIN
		IF ISNULL(@bitGroupByOrder,0) = 1
		BEGIN
			SELECT
				TEMP.numOppId
				,0  AS numOppItemID
				,TEMP.numItemCode
				,TEMP.numWareHouseID
				,TEMP.vcKitChildItems
				,TEMP.vcKitChildKitItems
				,STUFF((SELECT 
							CONCAT(',',T1.numOppItemID,'(',T1.numRemainingQty,')') 
						FROM 
							@TEMPItems T1 
						WHERE 
							T1.numOppId = TEMP.numOppId
							AND T1.numItemCode = TEMP.numItemCode
							AND T1.numWareHouseID = TEMP.numWareHouseID
							AND ISNULL(T1.vcKitChildItems,'') = ISNULL(TEMP.vcKitChildItems,'')
							AND ISNULL(T1.vcKitChildKitItems,'') = ISNULL(TEMP.vcKitChildKitItems ,'')
						FOR XML PATH('')),1,1,'') vcOppItemIDs
				,vcPoppName
				,numBarCodeId
				,CONCAT(TEMP.vcWarehouse, (CASE WHEN LEN(ISNULL(TEMP.vcLocation,'')) > 0 THEN CONCAT(' (',TEMP.vcLocation,')') ELSE '' END)) vcLocation
				,vcItemName
				,vcItemDesc
				,vcSKU
				,dbo.fn_GetItemAttributes(@numDomainID,TEMP.numItemCode) vcAttributes
				,vcPathForTImage
				,MIN(vcInclusionDetails) vcInclusionDetails
				,SUM(TEMP.numRemainingQty) numRemainingQty
				,0 numQtyPicked
				,(CASE 
					WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner INNER JOIN WarehouseLocation WL ON WIInner.numWLocationID=WL.numWLocationID WHERE WIInner.numDomainID=@numDomainID AND WIInner.numItemID=TEMP.numItemCode AND WIInner.numWareHouseID = TEMP.numWareHouseID)
					THEN CONCAT('[',STUFF((SELECT 
												CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "Available":',(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)),', "Location":"',WL.vcLocation,'"}') 
											FROM 
												WareHouseItems WIInner
											INNER JOIN
												WarehouseLocation WL
											ON
												WIInner.numWLocationID = WL.numWLocationID
											WHERE 
												WIInner.numDomainID=@numDomainID 
												AND WIInner.numItemID=TEMP.numItemCode 
												AND WIInner.numWareHouseID = TEMP.numWareHouseID
											ORDER BY
												WL.vcLocation
											FOR XML PATH('')),1,1,''),']')
					ELSE CONCAT('[',STUFF((SELECT 
										CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "Available":',(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)),', "Location":''''}')
									FROM 
										WareHouseItems WIInner
									WHERE 
										WIInner.numDomainID=@numDomainID 
										AND WIInner.numItemID=TEMP.numItemCode
										AND WIInner.numWareHouseID = TEMP.numWareHouseID
									FOR XML PATH('')),1,1,''),']')
				END)  vcWarehouseLocations
			FROM
				@TEMPItems TEMP
			GROUP BY
				TEMP.numOppId
				,TEMP.numItemCode
				,TEMP.numWareHouseID
				,TEMP.vcKitChildItems
				,TEMP.vcKitChildKitItems
				,TEMP.vcPoppName
				,TEMP.vcPathForTImage
				,TEMP.vcItemName
				,TEMP.vcItemDesc
				,TEMP.vcSKU
				,TEMP.numBarCodeId
				,TEMP.vcWarehouse
				,TEMP.vcLocation
			ORDER BY
				MIN(ID)
		END
		ELSE
		BEGIN
			SELECT
				0 AS numOppId
				,0 AS numOppItemID
				,TEMP.numItemCode
				,TEMP.numWareHouseID
				,TEMP.vcKitChildItems
				,TEMP.vcKitChildKitItems
				,STUFF((SELECT 
							CONCAT(', ',T1.numOppItemID,'(',T1.numRemainingQty,')') 
						FROM 
							@TEMPItems T1 
						WHERE 
							T1.numItemCode = TEMP.numItemCode
							AND T1.numWareHouseID = TEMP.numWareHouseID
							AND ISNULL(T1.vcKitChildItems,'') = ISNULL(TEMP.vcKitChildItems,'')
							AND ISNULL(T1.vcKitChildKitItems,'') = ISNULL(TEMP.vcKitChildKitItems ,'')
						FOR XML PATH('')),1,1,'') vcOppItemIDs
				,'' AS vcPoppName
				,numBarCodeId
				,CONCAT(TEMP.vcWarehouse, (CASE WHEN LEN(ISNULL(TEMP.vcLocation,'')) > 0 THEN CONCAT(' (',TEMP.vcLocation,')') ELSE '' END)) vcLocation
				,vcItemName
				,vcItemDesc
				,vcSKU
				,dbo.fn_GetItemAttributes(@numDomainID,TEMP.numItemCode) vcAttributes
				,vcPathForTImage
				,MIN(vcInclusionDetails) vcInclusionDetails
				,SUM(TEMP.numRemainingQty) numRemainingQty
				,0 numQtyPicked
				,(CASE 
					WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner INNER JOIN WarehouseLocation WL ON WIInner.numWLocationID=WL.numWLocationID WHERE WIInner.numDomainID=@numDomainID AND WIInner.numItemID=TEMP.numItemCode AND WIInner.numWareHouseID = TEMP.numWareHouseID)
					THEN CONCAT('[',STUFF((SELECT 
												CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "Available":',(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)),', "Location":"',WL.vcLocation,'"}') 
											FROM 
												WareHouseItems WIInner
											INNER JOIN
												WarehouseLocation WL
											ON
												WIInner.numWLocationID = WL.numWLocationID
											WHERE 
												WIInner.numDomainID=@numDomainID 
												AND WIInner.numItemID=TEMP.numItemCode 
												AND WIInner.numWareHouseID = TEMP.numWareHouseID
											ORDER BY
												WL.vcLocation
											FOR XML PATH('')),1,1,''),']')
					ELSE CONCAT('[',STUFF((SELECT 
										CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "Available":',(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)),', "Location":''''}')
									FROM 
										WareHouseItems WIInner
									WHERE 
										WIInner.numDomainID=@numDomainID 
										AND WIInner.numItemID=TEMP.numItemCode
										AND WIInner.numWareHouseID = TEMP.numWareHouseID
									FOR XML PATH('')),1,1,''),']')
				END)  vcWarehouseLocations
			FROM
				@TEMPItems TEMP
			GROUP BY
				TEMP.numItemCode
				,TEMP.numWareHouseID
				,TEMP.vcKitChildItems
				,TEMP.vcKitChildKitItems
				,TEMP.vcPathForTImage
				,TEMP.vcItemName
				,TEMP.vcItemDesc
				,TEMP.vcSKU
				,TEMP.numBarCodeId
				,TEMP.vcWarehouse
				,TEMP.vcLocation
			ORDER BY
				MIN(ID)
		END
	END
	
	SELECT * FROM @TempFieldsRight ORDER BY intRowNum
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_UpdateItemsPickedQty')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_UpdateItemsPickedQty
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_UpdateItemsPickedQty]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@vcSelectedRecords VARCHAR(MAX)
)
AS
BEGIN
	DECLARE @numStatus NUMERIC(18,0) = 0
	IF EXISTS (SELECT ID FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID)
	BEGIN
		SELECT @numStatus = ISNULL(numOrderStatusPicked,0) FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID
	END

	DECLARE @hDocItem INT

	DECLARE @Temp TABLE
	(
		ID INT IDENTITY(1,1)
		,numPickedQTY FLOAT
		,vcWarehouseItemIDs VARCHAR(MAX)
		,vcOppItems VARCHAR(MAX)
	)

	EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcSelectedRecords

	INSERT INTO @Temp
	(
		numPickedQty,
		vcWarehouseItemIDs,
		vcOppItems
	)
	SELECT
		numPickedQty,
		WarehouseItemIDs,
		vcOppItems
	FROM
		OPENXML (@hDocItem,'/NewDataSet/Table1',2)
	WITH
	(
		numPickedQty FLOAT,
		WarehouseItemIDs VARCHAR(MAX),
		vcOppItems VARCHAR(MAX)
	)

	EXEC sp_xml_removedocument @hDocItem 

	DECLARE @TEMPItems TABLE
	(
		ID INT
		,numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(18,0)
		,numQtyLeftToPick FLOAT
	)

	DECLARE @TEMPWarehouseLocations TABLE
	(
		ID INT
		,numWarehouseItemID NUMERIC(18,0)
		,numPickedQty FLOAT
	)


	DECLARE @i INT = 1
	DECLARE @iCount INT
	DECLARE @j INT = 1
	DECLARE @jCount INT
	DECLARE @numPickedQty FLOAT
	DECLARE @numOppID NUMERIC(18,0)
	DECLARE @numOppItemID NUMERIC(18,0)
	DECLARE @numWarehouseItemID NUMERIC(18,0)
	DECLARE @numQtyLeftToPick FLOAT
	DECLARE @vcWarehouseItemIDs VARCHAR(MAX)
	DECLARE @vcOppItems VARCHAR(MAX)
	DECLARE @numTempWarehouseItemID NUMERIC(18,0)
	DECLARE @numWarehousePickedQty FLOAT
	DECLARE @numFromOnHand FLOAT
	DECLARE @numFromOnOrder FLOAT
	DECLARE @numFromAllocation FLOAT
	DECLARE @numFromBackOrder FLOAT
	DECLARE @numToOnHand FLOAT
	DECLARE @numToOnOrder FLOAT
	DECLARE @numToAllocation FLOAT
	DECLARE @numToBackOrder FLOAT
	DECLARE @vcDescription VARCHAR(300)

	SET @iCount = (SELECT COUNT(*) FROM @TEMP)

	BEGIN TRY
	BEGIN TRANSACTION

		WHILE @i <= @iCount
		BEGIN
			SELECT
				@numPickedQty=ISNULL(numPickedQty,0)
				,@vcWarehouseItemIDs = ISNULL(vcWarehouseItemIDs,'')
				,@vcOppItems=ISNULL(vcOppItems,'')
			FROM 
				@TEMP 
			WHERE 
				ID=@i

		
			DELETE FROM @TEMPItems
			DELETE FROM @TEMPWarehouseLocations

			INSERT INTO @TEMPItems
			(
				ID
				,numOppID
				,numOppItemID
				,numWarehouseItemID
				,numQtyLeftToPick
			)
			SELECT 
				ROW_NUMBER() OVER(ORDER BY numOppItemID ASC)
				,OpportunityItems.numOppId
				,TEMP.numOppItemID
				,OpportunityItems.numWarehouseItmsID
				,ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
			FROM
			(
				SELECT
					CAST(SUBSTRING(OutParam,0,CHARINDEX('(',OutParam)) AS NUMERIC) numOppItemID
				FROM
					dbo.SplitString(@vcOppItems,',')
			) TEMP
			INNER JOIN
				OpportunityItems
			ON
				Temp.numOppItemID = OpportunityItems.numoppitemtCode

			IF LEN(ISNULL(@vcWarehouseItemIDs,'')) > 0
			BEGIN
				INSERT INTO @TEMPWarehouseLocations
				(
					ID
					,numWarehouseItemID
					,numPickedQty
				)
				SELECT 
					ROW_NUMBER() OVER(ORDER BY WareHouseItems.numWLocationID DESC)
					,TEMP.numWarehouseItemID
					,TEMP.numPickedQty
				FROM
				(
					SELECT
						CAST(SUBSTRING(OutParam,0,CHARINDEX('(',OutParam)) AS NUMERIC) numWarehouseItemID
						,CAST(SUBSTRING(OutParam,CHARINDEX('(',OutParam) + 1,LEN(OutParam) - CHARINDEX('(',OutParam) - 1) AS VARCHAR) numPickedQty
					FROM
						dbo.SplitString(@vcWarehouseItemIDs,',')
				) TEMP
				INNER JOIN
					WareHouseItems
				ON
					TEMP.numWarehouseItemID = WareHouseItems.numWareHouseItemID
			END


			SET @j = 1
			SET @jCount = (SELECT COUNT(*) FROM @TEMPItems)

			WHILE @j <= @jCount
			BEGIN
				SELECT 
					@numOppID=numOppID
					,@numOppItemID=numOppItemID
					,@numWarehouseItemID=numWarehouseItemID
					,@numQtyLeftToPick=numQtyLeftToPick
				FROM
					@TEMPItems
				WHERE
					ID=@j

				UPDATE
					OpportunityItems
				SET 
					numQtyPicked = ISNULL(numQtyPicked,0) + (CASE WHEN @numPickedQty > @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numPickedQty END)
				WHERE 
					numoppitemtCode=@numOppItemID

				SET @numPickedQty = @numPickedQty - (CASE WHEN @numPickedQty > @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numPickedQty END)


				IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numoppitemtCode=@numOppItemID AND ISNULL(numQtyPicked,0) > ISNULL(numUnitHour,0))
				BEGIN
					RAISERROR('PICKED_QTY_IS_GREATER_THEN_REMAINING_QTY',16,1)
				END

				IF EXISTS (SELECT ID FROM @TEMPWarehouseLocations WHERE numWarehouseItemID=@numWarehouseItemID)
				BEGIN
					SELECT 
						@numTempWarehouseItemID = numWarehouseItemID
						,@numWarehousePickedQty = ISNULL(numPickedQty,0)
					FROM 
						@TEMPWarehouseLocations
					WHERE 
						numWarehouseItemID=@numWarehouseItemID

					UPDATE 
						@TEMPWarehouseLocations
					SET
						numPickedQty = ISNULL(numPickedQty,0) - (CASE WHEN numPickedQty > @numQtyLeftToPick THEN @numQtyLeftToPick ELSE 0 END)
					WHERE
						numWarehouseItemID=@numTempWarehouseItemID

					SET @numQtyLeftToPick = @numQtyLeftToPick - (CASE WHEN @numWarehousePickedQty >= @numQtyLeftToPick THEN 0 ELSE @numWarehousePickedQty END)
				END
				
				IF @numQtyLeftToPick > 0
				BEGIN
					DECLARE @k INT = 1
					DECLARE @kCount INT 

					SELECT @kCount=COUNT(*) FROM @TEMPWarehouseLocations

					WHILE @k <= @kCount
					BEGIN
						SELECT 
							@numTempWarehouseItemID = numWarehouseItemID
							,@numWarehousePickedQty = ISNULL(numPickedQty,0)
						FROM 
							@TEMPWarehouseLocations 
						WHERE 
							ID = @k

						IF @numWarehousePickedQty > 0 AND (@numWarehouseItemID <> @numTempWarehouseItemID)
						BEGIN
							SELECT
								@numFromOnHand=ISNULL(numOnHand,0)
								,@numFromOnOrder=ISNULL(numOnOrder,0)
								,@numFromAllocation=ISNULL(numAllocation,0)
								,@numFromBackOrder=ISNULL(numBackOrder,0)
							FROM
								WareHouseItems
							WHERE
								numWareHouseItemID=@numTempWarehouseItemID

							SELECT
								@numToOnHand=ISNULL(numOnHand,0)
								,@numToOnOrder=ISNULL(numOnOrder,0)
								,@numToAllocation=ISNULL(numAllocation,0)
								,@numToBackOrder=ISNULL(numBackOrder,0)
							FROM
								WareHouseItems
							WHERE
								numWareHouseItemID=@numWarehouseItemID

							IF @numWarehousePickedQty >= @numQtyLeftToPick
							BEGIN
								SET @vcDescription = CONCAT('Items picked Qty(:',@numQtyLeftToPick,')') 

								IF (@numFromOnHand+@numFromAllocation) >= @numQtyLeftToPick
								BEGIN
									IF @numFromOnHand >= @numQtyLeftToPick
									BEGIN
										SET @numFromOnHand = @numFromOnHand - @numQtyLeftToPick
									END
									ELSE 
									BEGIN
										SET @numFromOnHand = 0
										SET @numFromAllocation = @numFromAllocation - (@numQtyLeftToPick - @numFromOnHand)
										SET @numFromBackOrder = @numFromBackOrder + (@numQtyLeftToPick - @numFromOnHand)
									END

									UPDATE @TEMPWarehouseLocations SET numPickedQty = numPickedQty - @numQtyLeftToPick WHERE ID = @k
								END
								ELSE
								BEGIN
									RAISERROR('AVAILABLE_QUANTITY_LESS_THEN_PICKED_QTY',16,1)
								END

								IF @numToBackOrder >= @numQtyLeftToPick
								BEGIN
									SET @numToBackOrder = @numToBackOrder - @numQtyLeftToPick
									SET @numToAllocation = @numToAllocation + @numQtyLeftToPick
								END
								ELSE
								BEGIN
									SET @numToBackOrder = 0
									SET @numToAllocation = @numToAllocation + @numToBackOrder
									SET @numToOnHand = @numToOnHand + (@numQtyLeftToPick - @numToBackOrder)
								END

								SET @numQtyLeftToPick = 0
							END
							ELSE
							BEGIN
								SET @vcDescription = CONCAT('Items picked Qty(:',@numWarehousePickedQty,')') 

								IF (@numFromOnHand+@numFromAllocation) >= @numWarehousePickedQty
								BEGIN
									IF @numFromOnHand >= @numWarehousePickedQty
									BEGIN
										SET @numFromOnHand = @numFromOnHand - @numWarehousePickedQty
									END
									ELSE 
									BEGIN
										SET @numFromOnHand = 0
										SET @numFromAllocation = @numFromAllocation - (@numWarehousePickedQty - @numFromOnHand)
										SET @numFromBackOrder = @numFromBackOrder + (@numWarehousePickedQty - @numFromOnHand)
									END

									UPDATE @TEMPWarehouseLocations SET numPickedQty = numPickedQty - @numWarehousePickedQty WHERE ID = @k
								END
								ELSE
								BEGIN
									RAISERROR('AVAILABLE_QUANTITY_LESS_THEN_PICKED_QTY',16,1)
								END

								IF @numToBackOrder >= @numWarehousePickedQty
								BEGIN
									SET @numToBackOrder = @numToBackOrder - @numWarehousePickedQty
									SET @numToAllocation = @numToAllocation + @numWarehousePickedQty
								END
								ELSE
								BEGIN
									SET @numToBackOrder = 0
									SET @numToAllocation = @numToAllocation + @numToBackOrder
									SET @numToOnHand = @numToOnHand + (@numWarehousePickedQty - @numToBackOrder)
								END
								

								SET @numQtyLeftToPick = @numQtyLeftToPick - @numWarehousePickedQty
							END

							--UPDATE INVENTORY AND WAREHOUSE TRACKING
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numFromOnHand,
							@numOnAllocation=@numFromAllocation,
							@numOnBackOrder=@numFromBackOrder,
							@numOnOrder=@numFromOnOrder,
							@numWarehouseItemID=@numTempWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@vcDescription 

							SET @vcDescription = REPLACE(@vcDescription,'picked','receieved')
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numToOnHand,
							@numOnAllocation=@numToAllocation,
							@numOnBackOrder=@numToBackOrder,
							@numOnOrder=@numToOnOrder,
							@numWarehouseItemID=@numWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@vcDescription 

						END

						IF @numQtyLeftToPick <= 0
							BREAK

						SET @k = @k + 1
					END
				END

				IF @numPickedQty <= 0
				BEGIN
					BREAK
				END

				SET @j = @j + 1
			END

			IF @numPickedQty > 0
			BEGIN
				RAISERROR('PICKED_QTY_IS_GREATER_THEN_REMAINING_QTY',16,1)
			END

			SET @i = @i + 1
		END

		IF ISNULL(@numStatus,0) > 0
		BEGIN
			UPDATE
				OpportunityMaster
			SET
				numStatus=@numStatus
			WHERE
				numDomainId=@numDomainID
				AND numOppId IN (SELECT DISTINCT OI.numOppId FROM @TEMPItems T1 INNER JOIN OpportunityItems OI ON T1.numOppItemID = OI.numoppitemtCode)
				AND NOT EXISTS (SELECT
									numOppItemtcode
								FROM
									OpportunityItems
								WHERE
									numOppId = OpportunityMaster.numOppId
									AND ISNULL(numWarehouseItmsID,0) > 0
									AND ISNULL(bitDropShip,0) = 0
									AND ISNULL(numUnitHour,0) <> ISNULL(numQtyPicked,0))
		END
	COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorNumber INT
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT
		DECLARE @ErrorLine INT
		DECLARE @ErrorProcedure NVARCHAR(200)

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorNumber = ERROR_NUMBER(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@ErrorLine = ERROR_LINE(),
			@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

		RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
	END CATCH
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppdetailsdtlpl')
DROP PROCEDURE usp_oppdetailsdtlpl
GO
CREATE PROCEDURE [dbo].[USP_OPPDetailsDTLPL]
(
               @OpportunityId        AS NUMERIC(9)  = NULL,
               @numDomainID          AS NUMERIC(9),
               @ClientTimeZoneOffset AS INT)
AS
	DECLARE @tintDecimalPoints TINYINT

	SELECT
		@tintDecimalPoints=ISNULL(tintDecimalPoints,0)
	FROM
		Domain 
	WHERE 
		numDomainId=@numDomainID

  SELECT Opp.numoppid,numCampainID,
		 CAMP.vcCampaignName AS [numCampainIDName],
         dbo.Fn_getcontactname(Opp.numContactId) numContactIdName,
         isnull(ADC.vcEmail,'') AS vcEmail,
         isnull(ADC.numPhone,'') AS Phone,
         isnull(ADC.numPhoneExtension,'') AS PhoneExtension,
		 Opp.numContactId,
         Opp.txtComments,
         Opp.numDivisionID,
         tintOppType,
         Dateadd(MINUTE,-@ClientTimeZoneOffset,bintAccountClosingDate) AS bintAccountClosingDate,
         dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID) AS tintSourceName,
          tintSource,
         Opp.VcPoppName,
         intpEstimatedCloseDate,
		 dbo.FormatedDateFromDate(DATEADD(MINUTE,-@ClientTimeZoneOffset,Opp.bintOppToOrder),@numDomainID) bintOppToOrder,
         [dbo].[getdealamount](@OpportunityId,Getutcdate(),0) AS CalAmount,
         --monPAmount,
         CONVERT(DECIMAL(10,2),Isnull(monPAmount,0)) AS monPAmount,
		lngPConclAnalysis,
         (SELECT vcdata
          FROM   listdetails
          WHERE  numlistitemid = lngPConclAnalysis) AS lngPConclAnalysisName,
         (SELECT vcdata
          FROM   listdetails
          WHERE  numlistitemid = numSalesOrPurType) AS numSalesOrPurTypeName,
		  numSalesOrPurType,
		 Opp.vcOppRefOrderNo,
		 Opp.vcMarketplaceOrderID,
         C2.vcCompanyName,
         D2.tintCRMType,
         Opp.tintActive,
         dbo.Fn_getcontactname(Opp.numCreatedby)
           + ' '
           + CONVERT(VARCHAR(20),Dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintCreatedDate)) AS CreatedBy,
         dbo.Fn_getcontactname(Opp.numModifiedBy)
           + ' '
           + CONVERT(VARCHAR(20),Dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintModifiedDate)) AS ModifiedBy,
         dbo.Fn_getcontactname(Opp.numRecOwner) AS RecordOwner,
         Opp.numRecOwner,
         Isnull(tintshipped,0) AS tintshipped,
         Isnull(tintOppStatus,0) AS tintOppStatus,
         [dbo].[GetOppStatus](Opp.numOppId) AS vcDealStatus,
         dbo.Opportunitylinkeditems(@OpportunityId) +
         (SELECT COUNT(*) FROM  dbo.Communication Comm JOIN Correspondence Corr ON Comm.numCommId=Corr.numCommID
			WHERE Comm.numDomainId=Opp.numDomainId AND Corr.numDomainID=Comm.numDomainID AND Corr.tintCorrType IN (1,2,3,4) AND Corr.numOpenRecordID=Opp.numOppId) AS NoOfProjects,
         (SELECT A.vcFirstName
                   + ' '
                   + A.vcLastName
          FROM   AdditionalContactsInformation A
          WHERE  A.numcontactid = opp.numAssignedTo) AS numAssignedToName,
		  opp.numAssignedTo,
         Opp.numAssignedBy,
         (SELECT COUNT(* )
          FROM   GenericDocuments
          WHERE  numRecID = @OpportunityId
                 AND vcDocumentSection = 'O') AS DocumentCount,
         CASE 
           WHEN tintOppStatus = 1 THEN 100
           ELSE isnull((SELECT SUM(tintPercentage)
                 FROM   OpportunityStageDetails
                 WHERE  numOppId = @OpportunityId
                        AND bitStageCompleted = 1),0)
         END AS TProgress,
         (SELECT TOP 1 Isnull(varRecurringTemplateName,'')
          FROM   RecurringTemplate
          WHERE  numRecurringId = OPR.numRecurringId) AS numRecurringIdName,
		 OPR.numRecurringId,
         Link.numOppID AS numParentOppID,
         Link.vcPoppName AS ParentOppName,
         Link.vcSource AS Source,
         ISNULL(C.varCurrSymbol,'') varCurrSymbol,
          ISNULL(Opp.fltExchangeRate,0) AS [fltExchangeRate],
           ISNULL(C.chrCurrency,'') AS [chrCurrency],
         ISNULL(C.numCurrencyID,0) AS [numCurrencyID],
         (SELECT COUNT(*) FROM [OpportunityLinking] OL WHERE OL.[numParentOppID] = Opp.numOppId) AS ChildCount,
         (SELECT COUNT(*) FROM [RecurringTransactionReport] RTR WHERE RTR.[numRecTranSeedId] = Opp.numOppId)  AS RecurringChildCount,
         (SELECT COUNT(*) FROM [ShippingReport] WHERE numOppBizDocId IN (SELECT numOppBizDocsId FROM   OpportunityBizDocs BD WHERE  BD.numOppId = @OpportunityId))AS ShippingReportCount,
         ISNULL(Opp.numStatus ,0) numStatus,
         (SELECT vcdata
          FROM   listdetails
          WHERE  numlistitemid = Opp.numStatus) AS numStatusName,
          (SELECT SUBSTRING(
	(SELECT ','+  A.vcFirstName+' ' +A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN '(' + dbo.fn_GetListItemName(SR.numContactType) + ')' ELSE '' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
				AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('')),2,200000)) AS numShareWith,
		ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = @OpportunityId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) AS monDealAmount,
		ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = @OpportunityId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) AS monAmountPaid,
		ISNULL(Opp.tintSourceType,0) AS tintSourceType,
		ISNULL(Opp.tintTaxOperator,0) AS tintTaxOperator,  
             isnull(Opp.intBillingDays,0) AS numBillingDays,isnull(Opp.bitInterestType,0) AS bitInterestType,
             isnull(opp.fltInterest,0) AS fltInterest,  isnull(Opp.fltDiscount,0) AS fltDiscount,
             isnull(Opp.bitDiscountType,0) AS bitDiscountType,ISNULL(Opp.bitBillingTerms,0) AS bitBillingTerms,
             ISNULL(OPP.numDiscountAcntType,0) AS numDiscountAcntType,
              dbo.fn_getOPPAddress(Opp.numOppId,@numDomainID,1) AS BillingAddress,
            dbo.fn_getOPPAddress(Opp.numOppId,@numDomainID,2) AS ShippingAddress,

            -- ISNULL((SELECT SUBSTRING((SELECT '$^$' + ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=Opp.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OBD.numShipVia  AND vcFieldName='Tracking URL')),'') +'#^#'+ RTRIM(LTRIM(vcTrackingNo) )  FROM dbo.OpportunityBizDocs OBD WHERE OBD.numOppId=Opp.numOppID FOR XML PATH('')),4,200000)),'') AS  vcTrackingNo,
			 (SELECT  
				SUBSTRING(
							(SELECT '$^$' + 
							(SELECT vcShipFieldValue FROM dbo.ShippingFieldValues 
								WHERE numDomainID=Opp.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OBD.numShipVia  AND vcFieldName='Tracking URL')
							) +'#^#'+ RTRIM(LTRIM(vcTrackingNo))						
							FROM dbo.OpportunityBizDocs OBD WHERE OBD.numOppId= Opp.numOppID and ISNULL(OBD.numShipVia,0) <> 0
				FOR XML PATH('')),4,200000)
						
			 ) AS  vcTrackingNo,
			 STUFF((SELECT 
						CONCAT('<br/>',OpportunityBizDocs.vcBizDocID,': ',vcTrackingDetail)
					FROM 
						ShippingReport 
					INNER JOIN 
						OpportunityBizDocs 
					ON 
						ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId 
					WHERE 
						ShippingReport.numOppId=Opp.numOppID
						AND ISNULL(ShippingReport.vcTrackingDetail,'') <> ''
					FOR XML PATH(''), TYPE).value('(./text())[1]','varchar(max)'), 1, 5, '') AS vcTrackingDetail,
            ISNULL(numPercentageComplete,0) numPercentageComplete,
            dbo.GetListIemName(ISNULL(numPercentageComplete,0)) AS PercentageCompleteName,
            ISNULL(Opp.numBusinessProcessID,0) AS [numBusinessProcessID],
            CASE WHEN opp.tintOppType=1 THEN dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID) ELSE '' END AS vcInventoryStatus
            ,ISNULL(bitUseShippersAccountNo,0) [bitUseShippersAccountNo], ISNULL((SELECT DMSA.vcAccountNumber FROm [dbo].[DivisionMasterShippingAccount] DMSA WHERE DMSA.numDivisionID=D2.numDivisionID AND DMSA.numShipViaID=opp.intUsedShippingCompany),'') [vcShippersAccountNo] ,
			(SELECT SUM((ISNULL(monTotAmtBefDiscount, ISNULL(monTotAmount,0))- ISNULL(monTotAmount,0))) FROM OpportunityBizDocItems WHERE numOppBizDocID IN (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppId = @OpportunityId AND bitAuthoritativeBizDocs = 1)) AS numTotalDiscount,
			ISNULL(Opp.vcRecurrenceType,'') AS vcRecurrenceType,
			ISNULL(Opp.bitRecurred,0) AS bitRecurred,
			(
				CASE 
				WHEN opp.tintOppType=1 AND opp.tintOppStatus = 1 
				THEN  
					dbo.GetOrderShipReceiveStatus(opp.numOppId,1,opp.tintShipped,@tintDecimalPoints)
				ELSE 
						'' 
				END
			) AS vcOrderedShipped,
			(
				CASE 
				WHEN opp.tintOppType = 2 AND opp.tintOppStatus = 1 
				THEN  
					dbo.GetOrderShipReceiveStatus(opp.numOppId,2,opp.tintShipped,@tintDecimalPoints)
				ELSE 
						'' 
				END
			) AS vcOrderedReceived,
			ISNULL(C2.numCompanyType,0) AS numCompanyType,
		ISNULL(C2.vcProfile,0) AS vcProfile,
		ISNULL(Opp.numAccountClass,0) AS numAccountClass,D3.vcPartnerCode+'-'+C3.vcCompanyName as numPartner,
		ISNULL(Opp.numPartner,0) AS numPartnerId,
		ISNULL(Opp.bitIsInitalSalesOrder,0) AS bitIsInitalSalesOrder,
		ISNULL(Opp.dtReleaseDate,'') AS dtReleaseDate,
		ISNULL(Opp.numPartenerContact,0) AS numPartenerContactId,
		A.vcGivenName AS numPartenerContact,
		Opp.numReleaseStatus,
		(CASE Opp.numReleaseStatus WHEN 1 THEN 'Open' WHEN 2 THEN 'Purchased' ELSE '' END) numReleaseStatusName,
		ISNULL(Opp.intUsedShippingCompany,0) intUsedShippingCompany,
		CASE WHEN ISNULL(Opp.intUsedShippingCompany,0) = 0 THEN '' ELSE ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 82 AND numListItemID=ISNULL(Opp.intUsedShippingCompany,0)),'') END AS vcShippingCompany,
		ISNULL(Opp.numShipmentMethod,0) numShipmentMethod
		,CASE WHEN ISNULL(Opp.numShipmentMethod,0) = 0 THEN '' ELSE ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 338 AND numListItemID=ISNULL(Opp.numShipmentMethod,0)),'') END AS vcShipmentMethod
		,ISNULL(Opp.vcCustomerPO#,'') vcCustomerPO#,
		CONCAT(CASE tintEDIStatus 
			--WHEN 1 THEN '850 Received'
			WHEN 2 THEN '850 Acknowledged'
			WHEN 3 THEN '940 Sent'
			WHEN 4 THEN '940 Acknowledged'
			WHEN 5 THEN '856 Received'
			WHEN 6 THEN '856 Acknowledged'
			WHEN 7 THEN '856 Sent'
			WHEN 8 THEN 'Send 940 Failed'
			WHEN 9 THEN 'Send 856 & 810 Failed'
			WHEN 11 THEN '850 Partially Created'
			WHEN 12 THEN '850 SO Created'
			ELSE '' 
		END,'  ','<a onclick="return Open3PLEDIHistory(',Opp.numOppID,')"><img src="../images/GLReport.png" border="0"></a>') tintEDIStatusName,
		ISNULL(Opp.numShippingService,0) numShippingService,
		ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = Opp.numShippingService),'') AS vcShippingService,
		CASE 
			WHEN vcSignatureType = 0 THEN 'Service Default'
			WHEN vcSignatureType = 1 THEN 'Adult Signature Required'
			WHEN vcSignatureType = 2 THEN 'Direct Signature'
			WHEN vcSignatureType = 3 THEN 'InDirect Signature'
			WHEN vcSignatureType = 4 THEN 'No Signature Required'
			ELSE ''
		END AS vcSignatureType
		,dbo.fn_getOPPState(Opp.numOppId,Opp.numDomainID,2) vcShipState
		,Opp.dtExpectedDate
		,ISNULL(Opp.numProjectID,0) numProjectID
		,ISNULL(PM.vcProjectName,'') AS numProjectIDName
		,ISNULL(Opp.intReqPOApproved,0) AS intReqPOApproved
		,CASE WHEN ISNULL(Opp.intReqPOApproved,0)=0 THEN 'Pending Approval' WHEN ISNULL(Opp.intReqPOApproved,0)=1 THEN 'Approved'  WHEN ISNULL(Opp.intReqPOApproved,0)=2 THEN 'Rejected' ELSE '' END AS vcReqPOApproved
  FROM   OpportunityMaster Opp
         JOIN divisionMaster D2 ON Opp.numDivisionID = D2.numDivisionID
         JOIN CompanyInfo C2 ON C2.numCompanyID = D2.numCompanyID
		 LEFT JOIN DivisionMasterShippingConfiguration ON D2.numDivisionID=DivisionMasterShippingConfiguration.numDivisionID
		 LEFT JOIN divisionMaster D3 ON Opp.numPartner = D3.numDivisionID
		 LEFT JOIN AdditionalContactsInformation A ON Opp.numPartenerContact = A.numContactId
		 LEFT JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId
         LEFT JOIN CompanyInfo C3 ON C3.numCompanyID = D3.numCompanyID
		 LEFT JOIN ProjectsMaster PM ON Opp.numProjectID = PM.numProId
         LEFT JOIN (SELECT TOP 1 vcPoppName,
                                 numOppID,
                                 numChildOppID,
                                 vcSource,numParentOppID,OpportunityMaster.numDivisionID AS numParentDivisionID 
                    FROM   OpportunityLinking
                          left JOIN OpportunityMaster
                             ON numOppID = numParentOppID
                    WHERE  numChildOppID = @OpportunityId) Link
           ON Link.numChildOppID = Opp.numOppID
           LEFT OUTER JOIN [Currency] C
					ON C.numCurrencyID = Opp.numCurrencyID
			LEFT OUTER JOIN [OpportunityRecurring] OPR
					ON OPR.numOppId = Opp.numOppId
					 LEFT JOIN AddressDetails AD --Billing add
				ON AD.numDomainID=D2.numDomainID AND AD.numRecordID=D2.numDivisionID AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1
			 LEFT JOIN AddressDetails AD2--Shipping address
				ON AD2.numDomainID=D2.numDomainID AND AD2.numRecordID=D2.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary=1	
		LEFT JOIN dbo.CampaignMaster CAMP ON CAMP.numCampaignID = Opp.numCampainID 
  WHERE  Opp.numOppId = @OpportunityId
         AND Opp.numDomainID = @numDomainID
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO

IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_PutAway')
DROP PROCEDURE USP_OpportunityMaster_PutAway
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_PutAway]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@numOppItemID NUMERIC(18,0)
	,@numQtyReceived FLOAT
	,@vcSerialLot# VARCHAR(MAX)
	,@dtItemReceivedDate DATETIME
	,@vcWarehouses VARCHAR(MAX)
	,@numVendorInvoiceBizDocID NUMERIC(18,0)
	,@tintMode TINYINT
)
AS
BEGIN
	IF ISNULL(@numQtyReceived,0) <= 0 
	BEGIN
		RAISERROR('QTY_TO_RECEIVE_NOT_PROVIDED',16,1)
		RETURN
	END 

	IF LEN(ISNULL(@vcWarehouses,'')) = 0
	BEGIN
		RAISERROR('LOCATION_NOT_SELECTED',16,1)
		RETURN
	END

	DECLARE @TEMPWarehouseLot TABLE
	(
		ID INT
		,numWareHouseItmsDTLID NUMERIC(18,0)
		,vcSerialNo VARCHAR(300)
		,numQty FLOAT
	)

	DECLARE @TEMPSerialLot TABLE
	(
		ID INT IDENTITY(1,1)
		,vcSerialNo VARCHAR(300)
		,numQty FLOAT
	)

	DECLARE @TEMPWarehouseLocations TABLE
	(
		ID INT IDENTITY(1,1)
		,numWarehouseItemID NUMERIC(18,0)
		,numReceivedQty FLOAT
	)

	INSERT INTO @TEMPWarehouseLocations
	(
		numWarehouseItemID
		,numReceivedQty
	)
	SELECT
		CAST(SUBSTRING(OutParam,0,CHARINDEX('(',OutParam)) AS NUMERIC) numWarehouseItemID
		,CAST(SUBSTRING(OutParam,CHARINDEX('(',OutParam) + 1,LEN(OutParam) - CHARINDEX('(',OutParam) - 1) AS VARCHAR) numReceivedQty
	FROM
		dbo.SplitString(@vcWarehouses,',')

	IF @numQtyReceived <> ISNULL((SELECT SUM(numReceivedQty) FROM @TEMPWarehouseLocations),0)
	BEGIN
		RAISERROR('INALID_QTY_RECEIVED',16,1)
		RETURN
	END

	IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
	BEGIN
		-- IF RECEIVED AGAINST VENDOR INVOICE MAKE SURE ENOUGH QTY IS PENDING TO RECIEVED AGAINST VEDOR INVOICE BECAUSE USER CAN RECEIVE QTY WITHOUT SELECTING VENDOR INVOCIE ALSO
		IF (SELECT
				COUNT(*)
			FROM
				OpportunityBizDocItems OBDI
			INNER JOIN
				OpportunityItems OI
			ON
				OBDI.numOppItemID=OI.numoppitemtCode
			WHERE
				numOppBizDocID=@numVendorInvoiceBizDocID
				AND OBDI.numOppItemID = @numOppItemID
				AND @numQtyReceived > (ISNULL(OI.numUnitHour,0) - ISNULL(OI.numUnitHourReceived,0))
			) > 0
		BEGIN
			RAISERROR('INVALID_VENDOR_INVOICE_RECEIVE_QTY',16,1)
			RETURN
		END
	END

	DECLARE @description AS VARCHAR(100)
	DECLARE @numWarehouseItemID AS NUMERIC(18,0)
	DECLARE @numFromWarehouseID AS NUMERIC(18,0)
	DECLARE @numFromWarehouseItemID NUMERIC(18,0)
	DECLARE @numWLocationID AS NUMERIC(18,0)     
	DECLARE @numOldQtyReceived AS FLOAT
	DECLARE @bitStockTransfer BIT
	DECLARE @numToWarehouseItemID NUMERIC
	DECLARE @numUnits FLOAT
	DECLARE @monPrice AS DECIMAL(20,5) 
	DECLARE @numItemCode NUMERIC(18,0)
	DECLARE @bitSerial BIT
	DECLARE @bitLot BIT
		
	SELECT  
		@numWarehouseItemID = ISNULL([numWarehouseItmsID], 0),
		@numFromWarehouseID=ISNULL(WI.numWareHouseID,0),
		@numFromWarehouseItemID = ISNULL([numWarehouseItmsID], 0),
		@numWLocationID = ISNULL(WI.numWLocationID,0),
		@numOldQtyReceived = ISNULL([numUnitHourReceived], 0),
		@bitStockTransfer = ISNULL(OM.bitStockTransfer,0),
		@numToWarehouseItemID= ISNULL(numToWarehouseItemID,0),
		@numOppId=OM.numOppId,
		@numUnits=OI.numUnitHour,
		@monPrice=isnull(monPrice,0) * (CASE WHEN ISNULL(OM.fltExchangeRate,0)=0 THEN 1 ELSE OM.fltExchangeRate END),
		@numItemCode=OI.numItemCode,
		@bitSerial=ISNULL(I.bitSerialized,0),
		@bitLot=ISNULL(I.bitLotNo,0)
	FROM 
		[OpportunityItems] OI 
	INNER JOIN 
		dbo.OpportunityMaster OM
	ON 
		OI.numOppId = OM.numOppId
	INNER JOIN
		Item I
	ON
		OI.numItemCode = I.numItemCode
	LEFT JOIN
		WareHouseItems WI
	ON
		OI.numWarehouseItmsID = WI.numWareHouseItemID
	WHERE 
		[numoppitemtCode] = @numOppItemID

	IF (ISNULL(@bitLot,0)=1 OR ISNULL(@bitSerial,0)=1)
	BEGIN
		IF LEN(ISNULL(@vcSerialLot#,'')) = 0
		BEGIN
			RAISERROR('SERIALLOT_REQUIRED',16,1)
			RETURN
		END 
		ELSE
		BEGIN	
			DECLARE @hDoc AS INT                                            
			EXEC sp_xml_preparedocument @hDoc OUTPUT, @vcSerialLot#

			INSERT INTO @TEMPSerialLot
			(
				vcSerialNo
				,numQty
			)
			SELECT 
				vcSerialNo
				,numQty
			FROM 
				OPENXML(@hDoc, '/NewDataSet/SerializedItems',2)
			WITH 
				(vcSerialNo VARCHAR(300), numQty FLOAT)

			EXEC sp_xml_removedocument @hDoc

			IF @numQtyReceived <> ISNULL((SELECT SUM(numQty) FROM @TEMPSerialLot),0)
			BEGIN
				RAISERROR('INALID_SERIALLOT',16,1)
				RETURN
			END
		END
	END

	IF (@numQtyReceived + @numOldQtyReceived) > @numUnits
	BEGIN
		RAISERROR('RECEIVED_QTY_MUST_BE_LESS_THEN_OR_EQUAL_TO_ORDERED_QTY',16,1)
		RETURN
	END

	IF @bitStockTransfer = 1 --added by chintan
	BEGIN
		DECLARE @p3 VARCHAR(MAX) = ''
		
		EXEC USP_UpdateQtyShipped @numQtyReceived,@numOppItemID,@numUserCntID,@vcError=@p3 OUTPUT

		IF LEN(@p3)>0 
		BEGIN
			RAISERROR ( @p3,16, 1 )
			RETURN ;
		END    

		SET @numWarehouseItemID = @numToWarehouseItemID
	END
	ELSE
	BEGIN
		--Updating the Average Cost
		DECLARE @TotalOnHand AS FLOAT = 0
		DECLARE @monAvgCost AS DECIMAL(20,5) 

		SELECT 
			@TotalOnHand=(SUM(ISNULL(numOnHand,0)) + SUM(ISNULL(numAllocation,0))) 
		FROM 
			dbo.WareHouseItems 
		WHERE 
			numItemID=@numItemCode 
						
		SELECT 
			@monAvgCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost, 0) END) 
		FROM 
			Item 
		WHERE 
			numitemcode = @numItemCode  
    
  		SET @monAvgCost = ((@TotalOnHand * @monAvgCost) + (@numQtyReceived * @monPrice)) / ( @TotalOnHand + @numQtyReceived )
                            
		UPDATE  
			Item
		SET 
			monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monAvgCost END)
		WHERE 
			numItemCode = @numItemCode
    END	

	UPDATE
		WareHouseItems
	SET 
		numOnOrder = ISNULL(numOnOrder,0) - @numQtyReceived,
		dtModified = GETDATE()
	WHERE
		numWareHouseItemID=@numWareHouseItemID
		
	SET @description = CONCAT('PO Qty Received (Qty:',@numQtyReceived,')')

	EXEC dbo.USP_ManageWareHouseItems_Tracking
		@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
		@numReferenceID = @numOppId, --  numeric(9, 0)
		@tintRefType = 4, --  tinyint
		@vcDescription = @description,
		@numModifiedBy = @numUserCntID,
		@dtRecordDate =  @dtItemReceivedDate,
		@numDomainID = @numDomainID


	DECLARE @i INT
	DECLARE @j INT
	DECLARE @k INT
	DECLARE @iCount INT = 0
	DECLARE @jCount INT = 0
	DECLARE @kCount INT = 0
	DECLARE @numWareHouseItmsDTLID NUMERIC(18,0)
	DECLARE @vcSerailLotNumber VARCHAR(300)
	DECLARE @numSerialLotQty FLOAT
	DECLARE @numTempWarehouseItemID NUMERIC(18,0)
	DECLARE @numTempReceivedQty FLOAT
	DECLARE @numLotWareHouseItmsDTLID NUMERIC(18,0)
	DECLARE @numLotQty FLOAT
	
	SET @i = 1
	SET @iCount = (SELECT COUNT(*) FROM @TEMPWarehouseLocations)

	WHILE @i <= @iCount
	BEGIN
		SELECT 
			@numTempWarehouseItemID=ISNULL(numWarehouseItemID,0)
			,@numTempReceivedQty=ISNULL(numReceivedQty,0)
		FROM 
			@TEMPWarehouseLocations 
		WHERE 
			ID = @i

		IF NOT EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode AND numWareHouseItemID=@numTempWarehouseItemID)
		BEGIN
			RAISERROR('INVALID_WAREHOUSE',16,1)
			RETURN
		END
		ELSE
		BEGIN
			-- INCREASE THE OnHand Of Destination Location
			UPDATE
				WareHouseItems
			SET
				numBackOrder = (CASE WHEN numBackOrder > @numTempReceivedQty THEN ISNULL(numBackOrder,0) - @numTempReceivedQty ELSE 0 END),         
				numAllocation = (CASE WHEN numBackOrder > @numTempReceivedQty THEN ISNULL(numAllocation,0) + @numTempReceivedQty ELSE ISNULL(numAllocation,0) + ISNULL(numBackOrder,0) END),
				numOnHand = (CASE WHEN numBackOrder > @numTempReceivedQty THEN numOnHand ELSE ISNULL(numOnHand,0) + (@numTempReceivedQty - ISNULL(numBackOrder,0)) END),
				dtModified = GETDATE()
			WHERE
				numWareHouseItemID=@numTempWarehouseItemID

			SET @description = CONCAT('PO Qty Put-Away (Qty:',@numTempReceivedQty,')')

			EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numTempWarehouseItemID, --  numeric(9, 0)
				@numReferenceID = @numOppId, --  numeric(9, 0)
				@tintRefType = 4, --  tinyint
				@vcDescription = @description,
				@numModifiedBy = @numUserCntID,
				@dtRecordDate =  @dtItemReceivedDate,
				@numDomainID = @numDomainID

			IF ISNULL(@numTempWarehouseItemID,0) > 0 AND ISNULL(@numTempWarehouseItemID,0) <> @numWareHouseItemID		
			BEGIN
				INSERT INTO OpportunityItemsReceievedLocation
				(
					numDomainID,
					numOppID,
					numOppItemID,
					numWarehouseItemID,
					numUnitReceieved
				)
				VALUES
				(
					@numWarehouseItemID,
					@numOppId,
					@numOppItemID,
					@numTempWarehouseItemID,
					@numTempReceivedQty
				)
			END

			IF (ISNULL(@bitLot,0)=1 OR ISNULL(@bitSerial,0)=1)
			BEGIN
				SET @j = 1
				SET @jCount = (SELECT COUNT(*) FROM @TEMPSerialLot)

				WHILE (@j <= @jCount AND @numTempReceivedQty > 0)
				BEGIN
					SELECT 
						@vcSerailLotNumber=ISNULL(vcSerialNo,'')
						,@numSerialLotQty=ISNULL(numQty,0)
					FROM 
						@TEMPSerialLot 
					WHERE 
						ID=@j

					IF @numSerialLotQty > 0
					BEGIN
						IF @bitStockTransfer = 1
						BEGIN
							IF (SELECT COUNT(*) FROM WareHouseItems INNER JOIN WareHouseItmsDTL ON WareHouseItems.numWareHouseItemID = WareHouseItmsDTL.numWareHouseItemID WHERE WareHouseItems.numItemID=@numItemCode AND WareHouseItems.numWareHouseID=@numFromWarehouseID AND LOWER(vcSerialNo)=LOWER(@vcSerailLotNumber)) = 0
							BEGIN
								RAISERROR('INVALID_SERIAL_NO',16,1)
								RETURN
							END
					
							IF @bitLot = 1
							BEGIN
								IF (SELECT SUM(WareHouseItmsDTL.numQty) FROM WareHouseItems INNER JOIN WareHouseItmsDTL ON WareHouseItems.numWareHouseItemID = WareHouseItmsDTL.numWareHouseItemID WHERE WareHouseItems.numItemID=@numItemCode AND WareHouseItems.numWareHouseID=@numFromWarehouseID AND LOWER(vcSerialNo)=LOWER(@vcSerailLotNumber)) < @numTempReceivedQty
								BEGIN
									RAISERROR('INVALID_LOT_QUANTITY',16,1)
									RETURN
								END

								DELETE FROM @TEMPWarehouseLot

								INSERT INTO @TEMPWarehouseLot
								(
									ID
									,numWareHouseItmsDTLID
									,numQty
								)
								SELECT
									ROW_NUMBER() OVER(ORDER BY WareHouseItmsDTL.numQty)
									,WareHouseItmsDTL.numWareHouseItmsDTLID
									,ISNULL(WareHouseItmsDTL.numQty,0)
								FROM
									WareHouseItems 
								INNER JOIN 
									WareHouseItmsDTL 
								ON 
									WareHouseItems.numWareHouseItemID = WareHouseItmsDTL.numWareHouseItemID 
								WHERE 
									WareHouseItems.numItemID=@numItemCode 
									AND WareHouseItems.numWareHouseID=@numFromWarehouseID 
									AND LOWER(vcSerialNo)=LOWER(@vcSerailLotNumber)

								SET @k = 1
								SELECT @kCount=COUNT(*) FROM @TEMPWarehouseLot

								WHILE @k <= @kCount AND @numTempReceivedQty > 0
								BEGIN
									SELECT
										@numLotWareHouseItmsDTLID=numWareHouseItmsDTLID
										,@numLotQty = numQty
									FROM
										@TEMPWarehouseLot
									WHERE
										ID=@k

									IF @numTempReceivedQty > @numLotQty
									BEGIN
										UPDATE
											WareHouseItmsDTL
										SET
											numWareHouseItemID = @numTempWarehouseItemID
										WHERE 
											numWareHouseItmsDTLID=@numLotWareHouseItmsDTLID

										UPDATE @TEMPSerialLot SET numQty = numQty-@numLotQty WHERE ID=@j
										SET @numTempReceivedQty = @numTempReceivedQty - @numLotQty
									END
									ELSE
									BEGIN
										UPDATE 
											WareHouseItmsDTL 
										SET 
											numQty = numQty - @numTempReceivedQty
										WHERE 
											numWareHouseItmsDTLID=@numLotWareHouseItmsDTLID

										INSERT INTO WareHouseItmsDTL
										(
											numWareHouseItemID,
											vcSerialNo,
											numQty,
											dExpirationDate,
											bitAddedFromPO
										)
										SELECT
											@numTempWarehouseItemID,
											vcSerialNo,
											@numTempReceivedQty,
											dExpirationDate,
											bitAddedFromPO
										FROM 
											WareHouseItmsDTL
										WHERE
											numWareHouseItmsDTLID=@numLotWareHouseItmsDTLID

										UPDATE @TEMPSerialLot SET numQty = numQty-@numTempReceivedQty WHERE ID=@j
										SET @numTempReceivedQty = 0
									END
									

									SET @k = @k + 1
								END
							END
							ELSE
							BEGIN
								IF NOT EXISTS (SELECT WareHouseItmsDTL.numWareHouseItmsDTLID FROM WareHouseItems INNER JOIN WareHouseItmsDTL ON WareHouseItems.numWareHouseItemID = WareHouseItmsDTL.numWareHouseItemID WHERE WareHouseItems.numItemID=@numItemCode AND WareHouseItems.numWareHouseID=@numFromWarehouseID AND LOWER(vcSerialNo)=LOWER(@vcSerailLotNumber) AND ISNULL(WareHouseItmsDTL.numQty,0) = 1)
								BEGIN
									RAISERROR('INVALID_SERIAL_QUANTITY',16,1)
									RETURN
								END

								UPDATE
									WHID
								SET
									WHID.numWareHouseItemID = @numTempWarehouseItemID
								FROM
									WareHouseItems WI
								INNER JOIN
									WareHouseItmsDTL WHID
								ON 
									WI.numWareHouseItemID = WHID.numWareHouseItemID 
								WHERE 
									WI.numItemID=@numItemCode 
									AND WI.numWareHouseID=@numFromWarehouseID 
									AND LOWER(WHID.vcSerialNo)=LOWER(@vcSerailLotNumber)

								UPDATE @TEMPSerialLot SET numQty = 0 WHERE ID=@j
								SET @numTempReceivedQty = @numTempReceivedQty - 1
							END
						END
						ELSE
						BEGIN
							IF @bitSerial = 1 AND (SELECT COUNT(*) FROM WareHouseItems INNER JOIN WareHouseItmsDTL ON WareHouseItems.numWareHouseItemID = WareHouseItmsDTL.numWareHouseItemID WHERE WareHouseItems.numItemID=@numItemCode AND LOWER(vcSerialNo)=LOWER(@vcSerailLotNumber)) > 0
							BEGIN
								RAISERROR('DUPLICATE_SERIAL_NO',16,1)
								RETURN
							END

							INSERT INTO WareHouseItmsDTL
							(
								numWareHouseItemID
								,vcSerialNo
								,numQty
								,bitAddedFromPO
							)  
							VALUES
							( 
								@numTempWarehouseItemID
								,@vcSerailLotNumber
								,(CASE WHEN @numTempReceivedQty >= @numSerialLotQty THEN @numSerialLotQty ELSE @numTempReceivedQty END)
								,1 
							)

		    
							SET @numWareHouseItmsDTLID=SCOPE_IDENTITY()
		
							INSERT INTO OppWarehouseSerializedItem
							(
								numWarehouseItmsDTLID
								,numOppId
								,numOppItemId
								,numWarehouseItmsID
								,numQty
							)  
							VALUES
							( 
								@numWareHouseItmsDTLID
								,@numOppID
								,@numOppItemID
								,@numTempWarehouseItemID
								,(CASE WHEN @numTempReceivedQty >= @numSerialLotQty THEN @numSerialLotQty ELSE @numTempReceivedQty END)
							)

							IF @numTempReceivedQty >= @numSerialLotQty
							BEGIN
								SET @numTempReceivedQty = @numTempReceivedQty - @numSerialLotQty
								UPDATE @TEMPSerialLot SET numQty = 0  WHERE ID = @j
							END
							ELSE
							BEGIN
								UPDATE @TEMPSerialLot SET numQty = numQty - @numTempReceivedQty  WHERE ID = @j
								SET @numTempReceivedQty = 0
							END
						END
					END

					SET @j = @j + 1
				END

				IF @numTempReceivedQty > 0
				BEGIN
					RAISERROR('INALID_SERIALLOT',16,1)
					RETURN
				END
			END
		END


		SET @i = @i + 1
	END

	UPDATE  
		[OpportunityItems]
	SET 
		numUnitHourReceived = ISNULL(numUnitHourReceived,0) + ISNULL(@numQtyReceived,0)
		,numQtyReceived = (CASE WHEN ISNULL(@tintMode,0)=1 THEN ISNULL(numQtyReceived,0) + ISNULL(@numQtyReceived,0) ELSE ISNULL(numQtyReceived,0) END)
	WHERE
		[numoppitemtCode] = @numOppItemID


	IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
	BEGIN
		UPDATE  
			OpportunityBizDocItems
		SET 
			numVendorInvoiceUnitReceived = ISNULL(numVendorInvoiceUnitReceived,0) +  ISNULL(@numQtyReceived,0)
		WHERE
			numOppBizDocID=@numVendorInvoiceBizDocID
			AND numOppItemID = @numOppItemID
	END

	UPDATE dbo.OpportunityMaster SET dtItemReceivedDate=@dtItemReceivedDate WHERE numOppId=@numOppId

END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PickListManageSOWorkOrder')
DROP PROCEDURE USP_PickListManageSOWorkOrder
GO
CREATE PROCEDURE [dbo].[USP_PickListManageSOWorkOrder]
	@numDomainID AS numeric(18,0),
	@numUserCntID AS NUMERIC(18,0),
	@numOppID AS NUMERIC(18,0),
	@numOppBizDocID AS NUMERIC(18,0)
AS
BEGIN
	DECLARE @numItemCode AS NUMERIC(18,0),@numUnitHour AS FLOAT,@numWarehouseItmsID AS NUMERIC(18,0),@vcItemDesc AS VARCHAR(1000)
	DECLARE @numWOId AS NUMERIC(18,0),@vcInstruction AS VARCHAR(1000),@bintCompliationDate AS DATETIME, @numWOAssignedTo NUMERIC(18,0)
	DECLARE @numoppitemtCode NUMERIC(18,0)
	DECLARE @numWarehouseID NUMERIC(18,0)
	DECLARE @numBuildProcessID NUMERIC(18,0)
  
	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numoppitemtCode NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numUnitHour FLOAT
		,numWarehouseItmsID NUMERIC(18,0)
		,vcItemDesc VARCHAR(1000)
		,vcInstruction VARCHAR(1000)
		,bintCompliationDate DATETIME
		,numWOAssignedTo NUMERIC(18,0)
		,numBuildProcessID NUMERIC(18,0)
	)

	INSERT INTO @TEMP
	(
		numoppitemtCode
		,numItemCode
		,numUnitHour
		,numWarehouseItmsID
		,vcItemDesc
		,vcInstruction
		,bintCompliationDate
		,numBuildProcessID
	)
	SELECT 
		numoppitemtCode
		,OpportunityItems.numItemCode
		,OpportunityBizDocItems.numUnitHour
		,OpportunityItems.numWarehouseItmsID
		,OpportunityItems.vcItemDesc
		,vcInstruction
		,bintCompliationDate
		,ISNULL(numBusinessProcessId,0)
	FROM
		OpportunityBizDocItems
	INNER JOIN 
		OpportunityItems             
	ON
		OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode 
	INNER JOIN
		Item
	ON
		OpportunityItems.numItemCode = Item.numItemCode                                      
	WHERE 
		numOppID=@numOppID
		AND numOppBizDocID = @numOppBizDocID
		AND ISNULL(bitWorkOrder,0)=1
		AND ISNULL(OpportunityBizDocItems.numUnitHour,0) > 0

	DECLARE @i INT = 1
	DECLARE @iCount AS INT

	SELECT @iCount=COUNT(*) FROM @TEMP

	WHILE @i <= @iCount
	BEGIN
		SELECT 
			@numItemCode=numItemCode
			,@numUnitHour=numUnitHour
			,@numWarehouseItmsID=numWarehouseItmsID
			,@vcItemDesc=vcItemDesc
			,@vcInstruction=vcInstruction
			,@bintCompliationDate=bintCompliationDate
			,@numoppitemtCode=numoppitemtCode
			,@numBuildProcessID=numBuildProcessID
		FROM 
			@TEMP                                                                         
		WHERE 
			ID=@i

		SET @numWarehouseID = ISNULL((SELECT numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID = @numWarehouseItmsID),0)

		IF EXISTS (SELECT 
						item.numItemCode
					FROM 
						item                                
					INNER JOIN 
						ItemDetails Dtl 
					ON 
						numChildItemID=numItemCode
					WHERE 
						numItemKitID=@numItemCode
						AND charItemType='P'
						AND NOT EXISTS (SELECT numWareHouseItemId FROM WareHouseItems WHERE numItemID=item.numItemCode AND numWareHouseID=@numWarehouseID))
		BEGIN
			RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			RETURN
		END
		ELSE
		BEGIN
			INSERT INTO WorkOrder
			(
				numItemCode
				,numQtyItemsReq
				,numWareHouseItemId
				,numCreatedBy
				,bintCreatedDate
				,numDomainID
				,numWOStatus
				,numOppId
				,vcItemDesc
				,vcInstruction
				,bintCompliationDate
				,numAssignedTo
				,numOppItemID
			)
			VALUES
			(
				@numItemCode
				,@numUnitHour
				,@numWarehouseItmsID
				,@numUserCntID
				,GETUTCDATE()
				,@numDomainID
				,0
				,@numOppID
				,@vcItemDesc
				,@vcInstruction
				,@bintCompliationDate
				,ISNULL((SELECT numBuildManager FROM Sales_process_List_Master WHERE Slp_Id=@numBuildProcessId),0)
				,@numoppitemtCode
			)
	
			SET @numWOId = SCOPE_IDENTITY()
			EXEC dbo.USP_UpdateNameTemplateValueForWorkOrder 3,@numDomainID,@numWOId

			IF(@numBuildProcessId>0)
			BEGIN
					INSERT  INTO Sales_process_List_Master ( Slp_Name,
														 numdomainid,
														 pro_type,
														 numCreatedby,
														 dtCreatedon,
														 numModifedby,
														 dtModifiedOn,tintConfiguration,bitAutomaticStartTimer,bitAssigntoTeams,numOppId,numTaskValidatorId,
														 numProjectId,numWorkOrderId)
				 SELECT Slp_Name,
						numdomainid,
						pro_type,
						numCreatedby,
						dtCreatedon,
						numModifedby,
						dtModifiedOn,tintConfiguration,bitAutomaticStartTimer,bitAssigntoTeams,0,numTaskValidatorId,0,@numWOId FROM Sales_process_List_Master WHERE Slp_Id=@numBuildProcessId    
					DECLARE @numNewProcessId AS NUMERIC(18,0)=0
					SET @numNewProcessId=(SELECT SCOPE_IDENTITY())
					UPDATE WorkOrder SET numBuildProcessId=@numNewProcessId WHERE numWOId=@numWOId
					INSERT INTO StagePercentageDetails(
					numStagePercentageId, 
					tintConfiguration, 
					vcStageName, 
					numDomainId, 
					numCreatedBy, 
					bintCreatedDate, 
					numModifiedBy, 
					bintModifiedDate, 
					slp_id, 
					numAssignTo, 
					vcMileStoneName, 
					tintPercentage, 
					tinProgressPercentage, 
					dtStartDate, 
					numParentStageID, 
					intDueDays, 
					numProjectID, 
					numOppID, 
					vcDescription, 
					bitIsDueDaysUsed,
					numTeamId, 
					bitRunningDynamicMode,
					numStageOrder,
					numWorkOrderId
				)
				SELECT
					numStagePercentageId, 
					tintConfiguration, 
					vcStageName, 
					numDomainId, 
					@numUserCntID, 
					GETDATE(), 
					@numUserCntID, 
					GETDATE(), 
					@numNewProcessId, 
					numAssignTo, 
					vcMileStoneName, 
					tintPercentage, 
					tinProgressPercentage, 
					GETDATE(), 
					numParentStageID, 
					intDueDays, 
					0, 
					0, 
					vcDescription, 
					bitIsDueDaysUsed,
					numTeamId, 
					bitRunningDynamicMode,
					numStageOrder,
					@numWOId
				FROM
					StagePercentageDetails
				WHERE
					slp_id=@numBuildProcessId	

					INSERT INTO StagePercentageDetailsTask(
					numStageDetailsId, 
					vcTaskName, 
					numHours, 
					numMinutes, 
					numAssignTo, 
					numDomainID, 
					numCreatedBy, 
					dtmCreatedOn,
					numOppId,
					numProjectId,
					numParentTaskId,
					bitDefaultTask,
					bitSavedTask,
					numOrder,
					numReferenceTaskId,
					numWorkOrderId
				)
				SELECT 
					(SELECT TOP 1 SFD.numStageDetailsId FROM StagePercentageDetails AS SFD WHERE SFD.vcStageName=SP.vcStageName AND SFD.numWorkOrderId=@numWOId),
					vcTaskName,
					numHours,
					numMinutes,
					ST.numAssignTo,
					@numDomainID,
					@numUserCntID,
					GETDATE(),
					0,
					0,
					0,
					1,
					CASE WHEN SP.bitRunningDynamicMode=1 THEN 0 ELSE 1 END,
					numOrder,
					numTaskId,
					@numWOId
				FROM 
					StagePercentageDetailsTask AS ST
				LEFT JOIN
					StagePercentageDetails As SP
				ON
					ST.numStageDetailsId=SP.numStageDetailsId
				WHERE
					ISNULL(ST.numOppId,0)=0 AND ISNULL(ST.numProjectId,0)=0 AND
					ST.numStageDetailsId IN(SELECT numStageDetailsId FROM 
					StagePercentageDetails As ST
				WHERE
					ST.slp_id=@numBuildProcessId)
				ORDER BY ST.numOrder
			END


			INSERT INTO [WorkOrderDetails] 
			(
				numWOId,numItemKitID,numChildItemID,numQtyItemsReq,numWareHouseItemId,vcItemDesc,sintOrder,numQtyItemsReq_Orig,numUOMId
			)
			SELECT 
				@numWOId,numItemKitID,numItemCode,
				CAST(((DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(Dtl.numUOMID,item.numItemCode,item.numDomainID,item.numBaseUnit),1)) * @numUnitHour)AS FLOAT),
				isnull((SELECT TOP 1 numWareHouseItemId FROM WareHouseItems WHERE numItemID=item.numItemCode AND numWareHouseID=@numWarehouseID ORDER BY ISNULL(numWLocationID,0),numWareHouseItemID),0),
				ISNULL(Dtl.vcItemDesc,txtItemDesc),
				ISNULL(sintOrder,0),
				DTL.numQtyItemsReq,
				Dtl.numUOMId 
			FROM 
				item                                
			INNER JOIN 
				ItemDetails Dtl 
			ON 
				numChildItemID=numItemCode
			WHERE 
				numItemKitID=@numItemCode
		END
				
		

		SET @i = @i + 1
	END	
END
GO
/****** Object:  StoredProcedure [dbo].[USP_ProsContacts]    Script Date: 07/26/2008 16:20:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_proscontacts')
DROP PROCEDURE usp_proscontacts
GO
CREATE PROCEDURE [dbo].[USP_ProsContacts]            
@numDivisionID as numeric(9)=0,  
@numDomainID as numeric(9)=0,
@numUserCntID numeric(9)=0,
@ClientTimeZoneOffset as int,
@numUserGroupID numeric(9)=0                                                                                   
as            
            
-- select case when (isnull(vcFirstName,'')='' and isnull(vcLastName,'')='') then '-' else vcFirstName+' '+ vcLastName end   as [Name], dbo.GetListIemName(vcPosition)+','+ dbo.GetListIemName(numContacttype)  as Pos, vcEmail as  Email,             
-- case when numPhone<>'' then + numPhone +case when numPhoneExtension<>'' then ' - ' + numPhoneExtension else '' end  else '' end as [Phone],                                                                                                              
-- isnull(vcAsstFirstName,'')+' '+isnull(vcAsstLastName,'') as Asst,    
-- case when numAsstPhone<>'' then + numAsstPhone +case when numAsstExtn<>'' then ' - ' + numAsstExtn else '' end  else '' end as [AsstPhone],                                                                                                              
-- numContactID,ac.numCreatedBy,dM.numTerid,isnull(AC.bitPrimaryContact,0) as bitPrimaryContact
-- FROM AdditionalContactsInformation AC join               
-- divisionmaster DM on DM.numDivisionID=AC.numDivisionID                  
-- WHERE AC.numDivisionID = @numDivisionID and AC.numDomainID=  @numDomainID

declare @numRelCntType as numeric(9)

select @numRelCntType=isnull(CI.numCompanyType,0) from  DivisionMaster DM JOIN CompanyInfo CI ON DM.numCompanyID = CI.numCompanyId 
	where DM.numDomainID=@numDomainID and DM.numDivisionID=@numDivisionID


declare @strSql as varchar(8000) 

declare @tintOrder as tinyint                                                        
declare @vcFieldName as varchar(50)                                                        
declare @vcListItemType as varchar(3)                                                   
declare @vcListItemType1 as varchar(1)                                                       
declare @vcAssociatedControlType varchar(20)                                                        
declare @numListID AS numeric(9)                                                        
declare @vcDbColumnName varchar(20)                            
declare @WhereCondition varchar(2000)                             
declare @vcLookBackTableName varchar(2000)                      
Declare @bitCustom as bit  
DECLARE @bitAllowEdit AS CHAR(1)                 
Declare @numFieldId as numeric  
DECLARE @bitAllowSorting AS CHAR(1)                   
declare @vcColumnName AS VARCHAR(500)                            
set @tintOrder=0                                                        
set @WhereCondition =''                       
                         
declare @Nocolumns as tinyint                      
set @Nocolumns=0                      
  
select @Nocolumns=isnull(sum(TotalRow),0)  from(            
Select count(*) TotalRow from View_DynamicColumns where numFormId=56 and numDomainID=@numDomainID AND numAuthGroupID=@numUserGroupID AND numRelCntType=@numRelCntType 
Union 
Select count(*) TotalRow from View_DynamicCustomColumns where numFormId=56 and numDomainID=@numDomainID AND numAuthGroupID=@numUserGroupID AND numRelCntType=@numRelCntType) TotalRows

   CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int)
                 
     
if @Nocolumns=0
BEGIN
INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType,bitCustom,intColumnWidth)
select 56,numFieldId,1,Row_number() over(order by tintRow desc),@numDomainID,@numUserGroupID,@numRelCntType,0,intColumnWidth
 FROM View_DynamicDefaultColumns
 where numFormId=56 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID
order by tintOrder asc      

END
                 
set @strSql=' select convert(varchar(10),isnull(ADC.numContactId,0)) as numContactId            
                    
       ,isnull(DM.numDivisionID,0) numDivisionID, isnull(DM.numTerID,0)numTerID,isnull( ADC.numRecOwner,0) numRecOwner,isnull( DM.tintCRMType,0) tintCRMType,
	   isnull(ADC.bitPrimaryContact,0) as bitPrimaryContact '                      
                         
   INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID ,intColumnWidth
 FROM View_DynamicColumns 
 where numFormId=56 and numDomainID=@numDomainID AND numAuthGroupID=@numUserGroupID AND numRelCntType=@numRelCntType
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
       
       UNION
    
   select tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth
 from View_DynamicCustomColumns
 where numFormId=56 and numDomainID=@numDomainID AND numAuthGroupID=@numUserGroupID AND numRelCntType=@numRelCntType
 AND ISNULL(bitCustom,0)=1
 
  order by tintOrder asc  
   
                                                      
Declare @ListRelID as numeric(9) 
  
    select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from  #tempForm order by tintOrder asc            

                   
while @tintOrder>0                                                        
begin                                                        
                                            
   if @bitCustom = 0                
 begin           
        
    declare @Prefix as varchar(5)                      
      if @vcLookBackTableName = 'AdditionalContactsInformation'                      
    set @Prefix = 'ADC.'                      
      if @vcLookBackTableName = 'DivisionMaster'                      
    set @Prefix = 'DM.'         
        
                  SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'
                          
 if @vcAssociatedControlType='SelectBox'                                                        
        begin                                                        
                                                        
     if @vcListItemType='LI'                                                         
     begin                                                        
      set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                       
      set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                        
     end                                                        
     else if @vcListItemType='S'                                                         
     begin                                                        
      set @strSql=@strSql+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName+']'                                                        
      set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID='+@vcDbColumnName                                                        
     end                                                        
     else if @vcListItemType='T'                                                         
     begin                                                        
      set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                        
      set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                        
     end                       
     else if   @vcListItemType='U'                                                     
    begin                       
    set @strSql=@strSql+',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'                                                        
    end                      
    end         
 else if @vcAssociatedControlType='DateField'                                                      
 begin                
           
     set @strSql=@strSql+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''          
     set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''          
     set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '          
     set @strSql=@strSql+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'                    
   end        
              
else if @vcAssociatedControlType='TextBox' OR @vcAssociatedControlType='Label'                                                     
begin        
         
     set @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'         
 when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' else @vcDbColumnName end+' ['+ @vcColumnName+']'                   
 end      
else                                                
begin      
 set @strSql=@strSql+', '+ @vcDbColumnName +' ['+ @vcColumnName+']'                
         
 end                                               
            
              
                    
end                
else if @bitCustom = 1                
begin                
            
	SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'
       
   select @vcFieldName=FLd_label,@vcAssociatedControlType=fld_type,@vcDbColumnName='Cust'+Convert(varchar(10),Fld_Id)                               
    from CFW_Fld_Master                                                
   where    CFW_Fld_Master.Fld_Id = @numFieldId                 
                
                 
    if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'             
   begin                
                   
    set @strSql= @strSql+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=ADC.numContactId   '                                                         
   end      
    else if @vcAssociatedControlType = 'CheckBox'             
   begin              
                 
    set @strSql= @strSql+',case when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=0 then ''No'' when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=1 then ''Yes'' end   ['+ @vcColumnName +']'              
   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '               
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=ADC.numContactId   '                                                       
   end              
  else if @vcAssociatedControlType = 'DateField'            
   begin                
                   
    set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=ADC.numContactId   '                                                         
   end                
   else if @vcAssociatedControlType = 'SelectBox'            
   begin                
    set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
    set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
     on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=ADC.numContactId   '                                                         
    set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
   end                 
end                    
                    
   select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc            
 
  if @@rowcount=0 set @tintOrder=0 

                                  
end                             
                          
                          
                                  
set @strSql=@strSql+' FROM AdditionalContactsInformation ADC                                                                             
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                                            
  JOIN CompanyInfo CI ON DM.numCompanyID = CI.numCompanyId
  LEFT JOIN AddressDetails AD ON AD.numRecordID = ADC.numContactID AND AD.tintAddressOf=1 AND AD.tintAddressType=0 AND AD.bitIsPrimary=1
   '+@WhereCondition+ '                     
  WHERE CI.numDomainID=DM.numDomainID   
  and DM.numDomainID=ADC.numDomainID  and ADC.numDivisionID = '+convert(varchar(18),@numDivisionID)+ ' and ADC.numDomainID=  '+convert(varchar(18),@numDomainID)
--' union select convert(varchar(10),count(*)),null,null,null,null,null '+@strColumns+' from tblcontacts order by RowNumber'                                                 
                                                     
print @strSql                              
                      
exec (@strSql)


SELECT * FROM #tempForm

DROP TABLE #tempForm  

GO
/****** Object:  StoredProcedure [dbo].[USP_SaveLayoutList]    Script Date: 07/26/2008 16:21:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_savelayoutlist')
DROP PROCEDURE usp_savelayoutlist
GO
CREATE PROCEDURE [dbo].[USP_SaveLayoutList]                                  
 @numUserCntID Numeric=0,                                  
 @numDomainID Numeric=0,                         
@strRow as text='',                      
@intcoulmn numeric=0,   
@numFormID as numeric=0,                          
@numRelCntType numeric=0,
  @tintPageType TINYINT      
AS                            
                        
delete  DycFormConfigurationDetails where numUserCntId=@numUserCntID and numDomainId=@numDomainId 
			and intColumnNum >= @intcoulmn and  numRelCntType=@numRelCntType AND numFormId=@numFormID
			AND tintPageType=@tintPageType  AND ISNULL(bitDefaultByAdmin,0)=0
  
                        
if convert(varchar(10),@strRow) <> ''                            
begin                                                        
   DECLARE @hDoc4 int                                                                
   EXEC sp_xml_preparedocument @hDoc4 OUTPUT, @strRow                                                                
                                                                  
    insert into  DycFormConfigurationDetails(numFormId,numFieldId,intRowNum,intColumnNum,
    numUserCntId,numDomainId,numRelCntType,bitCustom,tintPageType)                        
 select @numFormId,*,@tintPageType from (                           
SELECT * FROM OPENXML (@hDoc4,'/NewDataSet/Table',2)                                                                
 WITH  (                                                            
  numFieldID numeric(9),                        
  tintRow numeric(9),                        
  intColumn numeric(9),                        
  numUserCntId numeric(9),                        
  numDomainID numeric(9),        
numRelCntType numeric(9),        
bitCustomField bit))X WHERE numFieldID NOT IN (SELECT numFieldID FROM DycFormConfigurationDetails where numUserCntId=@numUserCntID and numDomainId=@numDomainId 
			and intColumnNum >= @intcoulmn and  numRelCntType=@numRelCntType AND numFormId=@numFormID
			AND tintPageType=@tintPageType  AND ISNULL(bitDefaultByAdmin,0)=1)
  
  
UPDATE D  SET D.intRowNum=tintRow,D.intColumnNum=intColumn 
FROM OPENXML (@hDoc4,'/NewDataSet/Table',2)                                                                
 WITH  (                                                            
  numFieldID numeric(9),                        
  tintRow numeric(9),                        
  intColumn numeric(9),                        
  numUserCntId numeric(9),                        
  numDomainID numeric(9),        
numRelCntType numeric(9),        
bitCustomField bit)X LEFT JOIN DycFormConfigurationDetails AS D ON X.numFieldID=D.numFieldID WHERE X.numFieldID IN (SELECT numFieldID FROM DycFormConfigurationDetails where numUserCntId=@numUserCntID and numDomainId=@numDomainId 
			 and  numRelCntType=@numRelCntType AND numFormId=@numFormID
			AND tintPageType=@tintPageType  AND ISNULL(bitDefaultByAdmin,0)=1) AND tintPageType=@tintPageType AND 
			 D.numUserCntID=@numUserCntID and D.numDomainId=@numDomainId 
			 and  D.numRelCntType=@numRelCntType AND numFormId=@numFormID
			AND tintPageType=@tintPageType  AND ISNULL(bitDefaultByAdmin,0)=1

                           
end                          
                       
                
EXEC sp_xml_removedocument @hDoc4
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_StagePercentageDetailsTask_ChangeAssignee')
DROP PROCEDURE dbo.USP_StagePercentageDetailsTask_ChangeAssignee
GO
CREATE PROCEDURE [dbo].[USP_StagePercentageDetailsTask_ChangeAssignee]
(
	@numDomainID NUMERIC(18,0)
	,@numTaskID NUMERIC(18,0)
	,@numAssignedTo NUMERIC(18,0)
)
AS 
BEGIN
	IF EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID = @numTaskID AND tintAction=4)
	BEGIN
		RAISERROR('TASK_IS_MARKED_AS_FINISHED',16,1)
	END
	ELSE IF ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID = @numTaskID ORDER BY tintAction),0) IN (1,3)
	BEGIN
		RAISERROR('TASK_IS_ALREADY_STARTED',16,1)
	END
	ELSE
	BEGIN
		UPDATE StagePercentageDetailsTask SET numAssignTo=@numAssignedTo WHERE numDomainID=@numDomainID AND numTaskId=@numTaskID
	END	
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TicklerActItems')
DROP PROCEDURE USP_TicklerActItems
GO
CREATE Proc [dbo].[USP_TicklerActItems]                                                                       
@numUserCntID as numeric(9)=null,                                                                          
@numDomainID as numeric(9)=null,                                                                        
@startDate as datetime,                                                                        
@endDate as datetime,                                                                    
@ClientTimeZoneOffset Int,                                                                 
@bitFilterRecord bit=0,
@columnName varchar(50), 
@columnSortOrder varchar(50),
@vcBProcessValue varchar(500),
@RegularSearchCriteria VARCHAR(MAX)='',
@CustomSearchCriteria VARCHAR(MAX)='', 
@OppStatus int,
@CurrentPage int,                                                              
@PageSize int                                        
As     

SET @RegularSearchCriteria=REPLACE(@RegularSearchCriteria, '.numFollowUpStatus', 'Div.numFollowUpStatus'); 

	                                                                    
  
DECLARE @tintPerformanceFilter AS TINYINT
DECLARE @tintActionItemsViewRights TINYINT  = 3

IF EXISTS (SELECT * FROM GroupAuthorization WHERE numDomainID=@numDomainID AND numModuleID=1 AND numPageID=2 AND numGroupID IN (SELECT ISNULL(numGroupID,0) FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailId=@numUserCntID))
BEGIN
	SELECT @tintActionItemsViewRights=ISNULL(intViewAllowed,0) FROM GroupAuthorization WHERE numDomainID=@numDomainID AND numModuleID=1 AND numPageID=2 AND numGroupID IN (SELECT ISNULL(numGroupID,0) FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailId=@numUserCntID)
END

IF PATINDEX('%cmp.vcPerformance=1%', @RegularSearchCriteria)>0 --Last 3 Months
BEGIN
	SET @tintPerformanceFilter = 1
END
ELSE IF PATINDEX('%cmp.vcPerformance=2%', @RegularSearchCriteria)>0 --Last 6 Months
BEGIN
	SET @tintPerformanceFilter = 2
END
ELSE IF PATINDEX('%cmp.vcPerformance=3%', @RegularSearchCriteria)>0 --Last 1 Year
BEGIN
	SET @tintPerformanceFilter = 3
END
ELSE
BEGIN
	SET @tintPerformanceFilter = 0
END

IF @columnName = 'vcPerformance'   
BEGIN
	SET @columnName = 'monDealAmount'
END

DECLARE @vcSelectedEmployess VARCHAR(100)
DECLARE @numCriteria INT
DECLARE @vcActionTypes VARCHAR(100)

SELECT 
	@vcSelectedEmployess = vcSelectedEmployee,
	@numCriteria = numCriteria,
	@vcActionTypes = vcActionTypes
FROM 
	TicklerListFilterConfiguration
WHERE 
	numDomainID = @numDomainID AND
	numUserCntID = @numUserCntID


Create table #tempRecords (numContactID numeric(9),numDivisionID numeric(9),tintCRMType tinyint,
Id numeric(9),bitTask varchar(100),Startdate datetime,EndTime datetime,
itemDesc text,[Name] varchar(200),vcFirstName varchar(100),vcLastName varchar(100),numPhone varchar(15),numPhoneExtension varchar(7),
[Phone] varchar(30),vcCompanyName varchar(100),vcProfile VARCHAR(100), vcEmail varchar(50),Task varchar(100),Activity varchar(MAX),
Status varchar(100),numCreatedBy numeric(9),numRecOwner VARCHAR(500), numModifiedBy VARCHAR(500), numOrgTerId NUMERIC(18,0), numTerId VARCHAR(200),numAssignedTo varchar(1000),numAssignedBy varchar(50),
 caseid numeric(9),vcCasenumber varchar(50),casetimeId numeric(9),caseExpId numeric(9),type int,itemid varchar(50),
changekey varchar(50),DueDate varchar(50),bitFollowUpAnyTime bit,numNoOfEmployeesId varchar(50),vcComPhone varchar(50),numFollowUpStatus varchar(200),dtLastFollowUp varchar(500),vcLastSalesOrderDate VARCHAR(100),monDealAmount DECIMAL(20,5), vcPerformance VARCHAR(100),vcColorScheme VARCHAR(50),
Slp_Name varchar(100),intTotalProgress INT,vcPoppName varchar(100), numOppId NUMERIC(18,0),numBusinessProcessID numeric(18,0),numCompanyRating varchar(500),numStatusID varchar(500),numCampaignID varchar(500),[Action-Item Participants] NVARCHAR(MAX),HtmlLink varchar(500),Location nvarchar(150),OrignalDescription text)                                                         
 
 
declare @strSql as varchar(MAX)                                                            
declare @strSql1 as varchar(MAX)                         
declare @strSql2 as varchar(MAX)                                                            
declare @vcCustomColumnName AS VARCHAR(8000)
declare @vcnullCustomColumnName AS VARCHAR(8000)
   -------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

declare @tintOrder as tinyint                                                
declare @vcFieldName as varchar(50)                                                
declare @vcListItemType as varchar(3)                                           
declare @vcAssociatedControlType varchar(20)                                                
declare @numListID AS numeric(9)                                                
declare @vcDbColumnName varchar(20)                    
declare @WhereCondition varchar(2000)                     
declare @vcLookBackTableName varchar(2000)              
Declare @bitCustomField as BIT
DECLARE @bitAllowEdit AS CHAR(1)                 
Declare @numFieldId as numeric  
DECLARE @bitAllowSorting AS CHAR(1)   
declare @vcColumnName AS VARCHAR(500)       
       
Declare @ListRelID as numeric(9)             
set @tintOrder=0               


	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcFieldDataType nvarchar(50),vcOrigDbColumnName nvarchar(50),bitAllowFiltering BIT,vcFieldName nvarchar(50)
		,vcAssociatedControlType nvarchar(50),vcListItemType char(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC
		,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int
		,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth NUMERIC(18,2)
	)
                
	--Custom Fields related to Organization
	INSERT INTO 
		#tempForm
	SELECT  
		tintRow + 1 AS tintOrder,vcDbColumnName,'',vcDbColumnName,1,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'' as vcLookBackTableName                                                 
		,bitCustom,numFieldId,bitAllowSorting,0,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength
		,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
	FROM 
		View_DynamicCustomColumns
	WHERE 
		numFormId=43 
		AND numUserCntID=@numUserCntID 
		AND numDomainID=@numDomainID 
		AND tintPageType=1
		AND ISNULL(bitCustom,0)=1
 
	SELECT TOP 1 
		@tintOrder = tintOrder + 1
		,@vcDbColumnName=vcDbColumnName
		,@vcFieldName=vcFieldName
		,@vcAssociatedControlType=vcAssociatedControlType
		,@vcListItemType=vcListItemType
		,@numListID=numListID
		,@vcLookBackTableName=vcLookBackTableName                                               
		,@bitCustomField=bitCustomField
		,@numFieldId=numFieldId
		,@bitAllowSorting=bitAllowSorting
		,@bitAllowEdit=0
		,@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder ASC     
  
	SET @vcCustomColumnName=''
	SET @vcnullCustomColumnName=''

	WHILE @tintOrder>0      
	BEGIN  
		SET @vcColumnName= @vcDbColumnName
		SET @strSql = 'ALTER TABLE #tempRecords ADD [' + @vcColumnName + '] NVARCHAR(MAX)'
		EXEC(@strSql)
		
		IF @bitCustomField = 1
		BEGIN 
			IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea'            
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+', ISNULL((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),'''') ['+ @vcColumnName +']'
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+','''' AS  ['+ @vcColumnName +']'
			END  
			ELSE IF @vcAssociatedControlType = 'CheckBox'         
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+',ISNULL((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),0)  ['+ @vcColumnName + ']'
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+',0 AS ['+ @vcColumnName + ']'
			END            
			ELSE IF @vcAssociatedControlType = 'DateField'        
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+',dbo.FormatedDateFromDate((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'              
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+','''' AS  ['+ @vcColumnName +']'
			END            
			ELSE IF @vcAssociatedControlType = 'SelectBox'           
			BEGIN            
				SET @vcCustomColumnName=@vcCustomColumnName+',(SELECT TOP 1 ISNULL(L.vcData,'''') FROM CFW_FLD_Values CFW left Join ListDetails L on L.numListItemID=CFW.Fld_Value WHERE  CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID)'+' ['+ @vcColumnName +']'                                                         
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']'                                                         
			END
			ELSE IF @vcAssociatedControlType = 'CheckBoxList'
			BEGIN
				SET @vcCustomColumnName=@vcCustomColumnName+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(ISNULL((SELECT CFWInner.Fld_Value FROM CFW_FLD_Values CFWInner WHERE CFWInner.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFWInner.RecId=Div.numDivisionID),''''),'','')) FOR XML PATH('''')), 1, 1, ''''))'+' ['+ @vcColumnName +']'
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']'
			END
			ELSE
			BEGIN
				SET @vcCustomColumnName= CONCAT(@vcCustomColumnName,',(SELECT dbo.fn_GetCustFldStringValue(',@numFieldId,',CFW.RecId,CFW.Fld_Value',') FROM CFW_FLD_Values CFW left Join ListDetails L on L.numListItemID=CFW.Fld_Value WHERE  CFW.Fld_ID=',@numFieldId,' AND CFW.RecId=Div.numDivisionID)',' [', @vcColumnName ,']')
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']' 
			END
		END
		  
		SELECT TOP 1 
			@tintOrder=tintOrder + 1
			,@vcDbColumnName=vcDbColumnName
			,@vcFieldName=vcFieldName
			,@vcAssociatedControlType=vcAssociatedControlType
			,@vcListItemType=vcListItemType
			,@numListID=numListID
			,@vcLookBackTableName=vcLookBackTableName                                               
			,@bitCustomField=bitCustomField
			,@numFieldId=numFieldId
			,@bitAllowSorting=bitAllowSorting
			,@bitAllowEdit=0
			,@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 set @tintOrder=0 
	END

 SET @strSql=''
 PRINT @vcCustomColumnName
--select top 1 @vcCSOrigDbCOlumnName=DFM.vcOrigDbCOlumnName,@vcCSLookBackTableName=DFM.vcLookBackTableName from DycFieldColorScheme DFCS join DycFieldMaster DFM on 
--DFCS.numModuleID=DFM.numModuleID and DFCS.numFieldID=DFM.numFieldID
--where DFCS.numDomainID=@numDomainID and DFM.numModuleID=1

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=43 AND DFCS.numFormID=43 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
----------------------------                   


SET @strSql = @strSql+'insert into #tempRecords
Select * from (                                                    
 SELECT Addc.numContactID,Addc.numDivisionID,Div.tintCRMType,Comm.numCommId AS Id,convert(varchar(15),bitTask) as bitTask,                                               
 DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtStartTime) as Startdate,                        
 DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtEndTime) as EndTime ,                                                                        
 Comm.textDetails AS itemDesc,                                                                                                               
 AddC.vcFirstName + '' '' + AddC.vcLastName  as [Name], 
 AddC.vcFirstName,AddC.vcLastName,Addc.numPhone,AddC.numPhoneExtension,                               
 case when Addc.numPhone<>'''' then + AddC.numPhone +case when AddC.numPhoneExtension<>'''' then '' - '' + AddC.numPhoneExtension else '''' end  else '''' end as [Phone],                                                                       
 Comp.vcCompanyName As vcCompanyName , ISNULL((SELECT vcData FROM ListDetails WHERE numListID =21 AND numDomainID='+ CAST(@numDomainID AS VARCHAR) +' AND  numListItemID=Comp.vcProfile ) ,'''') vcProfile ,                                                        
 AddC.vcEmail As vcEmail ,         
dbo.GetListIemName(Comm.bitTask)AS Task,                
 listdetailsActivity.VcData As Activity ,                                                    
 listdetailsStatus.VcData As Status   ,  -- Priority column                                                                                             
 Comm.numCreatedBy,   
 dbo.fn_GetContactName(Comm.numCreatedBy) AS numRecOwner,
 dbo.fn_GetContactName(Comm.numModifiedBy) AS numModifiedBy,                                                                                                          
 ISNULL(Div.numTerId,0) AS numOrgTerId,
 (select TOP 1 vcData from ListDetails where numListItemID=Div.numTerId) AS numTerId,                                           
Substring( 
dbo.fn_GetContactName(numAssign) + 
ISNULL((SELECT SUBSTRING((SELECT '','' + dbo.fn_GetContactName(numContactId) FROM CommunicationAttendees WHERE numCommId=Comm.numCommId FOR XML PATH('''')),1,200000)),''''),0,50)
As numAssignedTo , 
dbo.fn_GetContactName(Comm.numAssignedBy) AS numAssignedBy,        
convert(varchar(50),comm.caseid) as caseid,                          
(select top 1 vcCaseNumber from cases where cases.numcaseid= comm.caseid )as vcCasenumber,                          
ISNULL(comm.casetimeId,0) casetimeId,                          
ISNULL(comm.caseExpId,0) caseExpId,                        
0 as type   ,                    
'''' as itemid ,                    
'''' as changekey ,cast(Null as datetime) as DueDate ,ISNULL(bitFollowUpAnyTime,0) bitFollowUpAnyTime,
dbo.GetListIemName(Comp.numNoOfEmployeesId) AS numNoOfEmployeesId,Div.vcComPhone,
dbo.GetListIemName(Div.numFollowUpStatus) AS numFollowUpStatus,' + 
CONCAT('ISNULL(dbo.FormatedDateFromDate(Div.dtLastFollowUp,',@numDomainID,'),'''')') + ' AS dtLastFollowUp,' 
+ CONCAT('ISNULL(dbo.FormatedDateFromDate(TempLastOrder.bintCreatedDate,',@numDomainID,'),'''')') + ' AS vcLastSalesOrderDate,ISNULL(TempPerformance.monDealAmount,0) monDealAmount,(CASE WHEN ISNULL(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(FORMAT(TempPerformance.monDealAmount,''##,####.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) AS vcPerformance '

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	set @strSql=@strSql+',tCS.vcColorScheme'
ELSE
	set @strSql=@strSql+','''' as vcColorScheme'

SET @strSql =@strSql + ' ,ISNULL(Slp_Name,'''') AS Slp_Name,ISNULL(intTotalProgress,0) AS [intTotalProgress],ISNULL(vcPoppName,'''') AS [vcPoppName], ISNULL(Opp.numOppId,0) AS [numOppId],ISNULL(Opp.numBusinessProcessID,0) AS numBusinessProcessID'
SET @strSql =@strSql + ' ,(select TOP 1 vcData from ListDetails where numListItemID=Comp.numCompanyRating) AS numCompanyRating ,(select TOP 1 vcData from ListDetails where numListItemID=Div.numStatusID) AS numStatusID,(SELECT TOP 1 vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID) AS numCampaignID,(SELECT DISTINCT  
replace(replace(STUFF((SELECT ''^<br>'' 
+ CASE(ISNULL(Comp.vcCompanyName,'''')) WHEN '''' THEN '''' else ''<a href=javascript:OpenCompany('' +  cast(isnull(Addc.numDivisionID,0) as varchar(20)) + '','' +  cast(isnull(Div.tintCRMType,0) as varchar(20)) + '',1) style=''''COLOR:black;TEXT-DECORATION:underline;''''>'' + ltrim(RTRIM(vcCompanyName)) +  ''</a> ''  END
+ CASE(isnull(AddC.vcFirstName,'''')) when '''' THEN ''''	else ''<a href=javascript:OpenContact('' +  cast(isnull(AddC.numContactId,0) as varchar(20)) + '',1) style=''''COLOR:#dd8047;''''>'' + ltrim(RTRIM(AddC.vcFirstName)) + ''</a> ''  END
+ CASE(isnull(addc.vcLastName,'''')) when '''' THEN '''' else ''<a href=javascript:OpenContact('' +  cast(isnull(AddC.numContactId,0) as varchar(20)) + '',1) style=''''COLOR:#dd8047;''''>'' + ltrim(RTRIM(AddC.vcLastName)) + ''</a> ''  END 
+ CASE(isnull(AddC.numPhone,'''')) WHEN '''' then '''' ELSE AddC.numPhone + '' '' END
+ CASE(isnull(AddC.numPhoneExtension,'''')) when '''' then '''' else '' ('' + AddC.numPhoneExtension + '') '' END 
+ CASE(isnull(addc.vcEmail,'''')) when '''' THEN '''' ELSE ''<a href=''''#'''' onclick=javascript:OpemEmail(''''../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail='' + ltrim(RTRIM(AddC.vcEmail)) + '''''') style=''''COLOR:#3c8dbc;''''>'' + ltrim(RTRIM(AddC.vcEmail)) + ''</a> ''  END 
FOR XML PATH('''')
), 1, 1, ''''),''&lt;'', ''<''), ''&gt;'', ''>'')) AS [Action-Item Participants],'''' AS HtmlLink, '''' AS Location,'''' As OrignalDescription'
SET @strSql =@strSql+ @vcCustomColumnName
SET @strSql =@strSql + ' FROM  Communication Comm                              
 JOIN AdditionalContactsInformation AddC                                                  
 ON Comm.numContactId = AddC.numContactId                                  
 JOIN DivisionMaster Div                                                                         
 ON AddC.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                                                                         
 ON Div.numCompanyID = Comp.numCompanyId     
 OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=Div.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder
 OUTER APPLY(' + CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = Div.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		AND 1 = (CASE ',@tintPerformanceFilter,' 
																				WHEN 1 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-3,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 2 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-6,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 3 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(YEAR,-1,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				ELSE 1
																				END)
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') + ' AS TempPerformance
 Left Join listdetails listdetailsActivity                                                  
 On Comm.numActivity = listdetailsActivity.NumlistItemID                                               
 Left Join listdetails listdetailsStatus                                                 
 On Comm.numStatus = listdetailsStatus.NumlistItemID
 LEFT JOIN OpportunityMaster Opp ON Opp.numDivisionId = Div.numDivisionID AND Opp.numContactId = AddC.numContactId  AND Opp.numOppID = Comm.numOppID 
 LEFT JOIN Sales_process_List_Master splm ON splm.Slp_Id = Opp.numBusinessProcessID 
 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId '
 

DECLARE @Prefix AS VARCHAR(5)
 
 IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
	 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
		set @Prefix = 'AddC.'                  
  else if @vcCSLookBackTableName = 'DivisionMaster'                  
		set @Prefix = 'Div.'
	else if @vcCSLookBackTableName = 'CompanyInfo'                  
		set @Prefix = 'Comp.'  
	ELSE IF @vcCSLookBackTableName='Tickler'	
		set @Prefix = 'Comm.'  
--	else if @vcCSLookBackTableName = 'ProjectProgress'                  
--		set @Prefix = 'pp.'  	
--	else if @vcCSLookBackTableName = 'OpportunityMaster'                  
--		set @Prefix = 'om.'  
--	else if @vcCSLookBackTableName = 'Sales_process_List_Master'                  
--		set @Prefix = 'splm.'  		
	else
		set @Prefix = ''   
	
	
	IF @vcCSAssociatedControlType='DateField'
	BEGIN
	IF @vcCSOrigDbCOlumnName='FormattedDate'
			SET @vcCSOrigDbCOlumnName='dtStartTime'
	
	
			set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',' + @Prefix + @vcCSOrigDbCOlumnName + '),111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
			 and CONVERT(VARCHAR(10),DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',' + @Prefix + @vcCSOrigDbCOlumnName + '),111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
	END
	ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'  OR @vcCSAssociatedControlType='TextBox'
	BEGIN
		--PRINT 1
		set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
	END
END

set @strSql=@strSql+'  WHERE Comm.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (Comm.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or Comm.numAssign= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END) '
 
 IF LEN(ISNULL(@vcBProcessValue,''))>0
BEGIN
	SET @strSql = @strSql + ' and ISNULL(Opp.numBusinessProcessID,0) in (SELECT Items FROM dbo.Split(''' +@vcBProcessValue + ''','',''))' 			
END

IF LEN(@vcActionTypes) > 0 
BEGIN
	SET @strSql = @strSql + ' AND Comm.bitTask IN (' + @vcActionTypes + ')'  
END
  
IF @numCriteria  = 1 --Action Items	Assigned
BEGIN
	set @strSql=@strSql+' AND 1=(Case when Comm.numAssign IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +') then 1 else 0 end)'
END
ELSE IF @numCriteria = 2 --Action Items Owned
BEGIN
	set @strSql=@strSql+' AND 1=(Case when Comm.numCreatedBy IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +' ) then 1 else 0 end)'
END
ELSE --Action Items Assigned Or Owned
BEGIN
	set @strSql=@strSql+' AND 1=(Case when (Comm.numAssign IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')  or Comm.numCreatedBy IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')) then 1 else 0 end)'
END

        
  
IF(LEN(@RegularSearchCriteria)>0)
BEGIN
IF PATINDEX('%cmp.vcPerformance%', @RegularSearchCriteria)>0
BEGIN
	-- WE ARE MANAGING CONDITION IN OUTER APPLY
	set @strSql=@strSql
END
ELSE
BEGIN
	SET @strSql=@strSql+' AND '+@RegularSearchCriteria
END
END

IF LEN(@CustomSearchCriteria) > 0
BEGIN
	SET @strSql=@strSql +' AND '+ @CustomSearchCriteria
END

set @strSql=@strSql+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) <= '''+Cast(@endDate as varchar(30))+''')'                                             

IF @OppStatus  = 0 --OPEN
BEGIN
	set @strSql=@strSql+'  AND Comm.bitclosedflag=0 AND Comm.bitTask <> 973  ) As X '
END
ELSE IF @OppStatus = 1 --CLOSED
BEGIN
	set @strSql=@strSql+' AND Comm.bitclosedflag=1 AND Comm.bitTask <> 973  ) As X '
END
ELSE 
BEGIN
	set @strSql=@strSql+' AND Comm.bitclosedflag in (0,1) AND Comm.bitTask <> 973  ) As X '
END
                                
              

IF LEN(ISNULL(@vcBProcessValue,''))=0 AND LEN(@RegularSearchCriteria) = 0 AND LEN(@CustomSearchCriteria) = 0
BEGIN
	If LEN(ISNULL(@vcActionTypes,'')) = 0 OR EXISTS (SELECT OutParam FROM dbo.SplitString(@vcActionTypes,',') WHERE OutParam='982')
	BEGIN
		SET @strSql1 =  ' insert into #tempRecords 
select                          
 ACI.numContactID,ACI.numDivisionID,Div.tintCRMType,ac.activityid as id ,case when alldayevent = 1 then ''5'' else ''0'' end as bitTask,                        
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) startdate,                        
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dateadd(second,duration,startdatetimeutc)) as endtime,                        
(SELECT DISTINCT  
replace(replace((SELECT 
CASE(ISNULL(bitTitle,1)) WHEN 1 THEN ''<b><font color=red>Title</font></b><br>'' + CASE(ISNULL(ac.Subject,'''')) WHEN '''' THEN '''' else ac.Subject END + ''<br>'' else ''''  END  
+ CASE(ISNULL(bitLocation,1)) WHEN 1 THEN ''<b><font color=green>Location</font></b><br>'' + CASE(ISNULL(ac.location,'''')) WHEN '''' THEN '''' else ac.location END + ''<br>'' else ''''  END  
+ CASE(isnull(bitDescription,1)) when 1 THEN ''<b><font color=#3c8dbc>Description</font></b><br>$'' else ''''  END  
FROM ActivityDisplayConfiguration where numDomainId='+ CAST(@numDomainID AS VARCHAR) +' and numUserCntId='+ CAST(@numUserCntID AS VARCHAR) +'
FOR XML PATH('''')
),''&lt;'', ''<''), ''&gt;'', ''>'')) AS itemDesc,                        
ACI.vcFirstName + '' '' + ACI.vcLastName name, 
 ACI.vcFirstName,ACI.vcLastName,ACI.numPhone,ACI.numPhoneExtension,                   
 case when ACI.numPhone<>'''' then + ACI.numPhone +case when ACI.numPhoneExtension<>'''' then '' - '' + ACI.numPhoneExtension else '''' end  else '''' end as [Phone],                        
 Substring ( Comp.vcCompanyName , 0 , 100 ) + ''..'' As vcCompanyName , ISNULL((SELECT vcData FROM ListDetails WHERE numListID =21 AND numDomainID='+ CAST(@numDomainID AS VARCHAR) +' AND  numListItemID=Comp.vcProfile ) ,'''') vcProfile ,                      
vcEmail,                        
''Calendar'' as task,   
[subject] as Activity,                   
case when importance =0 then ''Low'' when importance =1 then ''Normal'' when importance =2 then ''High'' end as status,                        
0 as numcreatedby,
'''' as numRecOwner, 
'''' AS numModifiedBy,                  
0 as numorgterid,       
'''' as numterid,                 
''-/-'' as numAssignedTo, 
''-'' AS numAssignedBy,        
''0'' as caseid,                        
null as vccasenumber,                        
0 as casetimeid,                        
0 as caseexpid,                        
1 as type ,                    
isnull(itemid,'''') as itemid  ,                    
changekey  ,cast(Null as datetime) as DueDate,cast(0 as bit) as bitFollowUpAnyTime,
dbo.GetListIemName(Comp.numNoOfEmployeesId) AS numNoOfEmployeesId,Div.vcComPhone,
dbo.GetListIemName(Div.numFollowUpStatus) AS numFollowUpStatus,' + 
CONCAT('ISNULL(dbo.FormatedDateFromDate(Div.dtLastFollowUp,',@numDomainID,'),'''')') + ' AS dtLastFollowUp,' 
+ CONCAT('ISNULL(dbo.FormatedDateFromDate(TempLastOrder.bintCreatedDate,',@numDomainID,'),'''')') + ' AS vcLastSalesOrderDate,ISNULL(TempPerformance.monDealAmount,0) monDealAmount, (CASE WHEN ISNULL(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(FORMAT(TempPerformance.monDealAmount,''##,####.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) vcPerformance,'''' as vcColorScheme,'''',0,'''',0 AS [numOppId],0 AS numBusinessProcessID '
SET @strSql1 =@strSql1 + ' ,(select TOP 1 vcData from ListDetails where numListItemID=Comp.numCompanyRating) AS numCompanyRating ,(select TOP 1 vcData from ListDetails where numListItemID=Comp.numCompanyRating) AS numCompanyRating,''-'' AS numCampaignID '+@vcnullCustomColumnName 



SET @strSql1=@strSql1 + ' ,(SELECT DISTINCT  
replace(replace(STUFF((SELECT ''^<br>'' 
+ CASE(ISNULL(CI.vcCompanyName,'''')) WHEN '''' THEN ''''		else ''<a href=javascript:OpenCompany('' +  cast(isnull(ATM.DivisionID,0) as varchar(20)) + '','' +  cast(isnull(ATM.tintCRMType,0) as varchar(20)) + '',1) style=''''COLOR:black;TEXT-DECORATION:underline;''''>'' + ltrim(RTRIM(CI.vcCompanyName)) + ''</a> ''  END  
+ CASE(isnull(ATM.AttendeeFirstName,'''')) when '''' THEN ''''	else ''<a href=javascript:OpenContact('' +  cast(isnull(ATM.ContactID,0) as varchar(20)) + '',1) style=''''COLOR:#dd8047;''''>'' + ltrim(RTRIM(ATM.AttendeeFirstName)) + ''</a> ''  END
+ CASE(isnull(ATM.AttendeeLastName,'''')) when '''' THEN '''' else ''<a href=javascript:OpenContact('' +  cast(isnull(ATM.ContactID,0) as varchar(20)) + '',1) style=''''COLOR:#dd8047;''''>'' + ltrim(RTRIM(ATM.AttendeeLastName)) + ''</a> ''  END 
+ CASE(isnull(ATM.AttendeePhone,'''')) WHEN '''' then '''' ELSE ATM.AttendeePhone + '' '' END
+ CASE(isnull(ATM.AttendeePhoneExtension,'''')) when '''' then '''' else '' ('' + ATM.AttendeePhoneExtension + '') '' END 
+ CASE(isnull(ATM.AttendeeEmail,''''))    when '''' THEN '''' ELSE ''<a href=''''#'''' onclick=javascript:OpemEmail(''''../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail='' + ltrim(RTRIM(ATM.AttendeeEmail)) + '''''') style=''''COLOR:#3c8dbc;''''>'' + ltrim(RTRIM(ATM.AttendeeEmail)) + ''</a> ''  END 
+ CASE(isnull(ATM.ResponseStatus,'''')) WHEN '''' THEN '''' WHEN  ''needsAction''  then '''' ELSE ''<img src=''''../images/'' + ATM.ResponseStatus + ''.gif'''' align=''''BASELINE'''' style=''''float:none;vertical-align:middle;width: 15px;''''/>'' END
FROM ActivityAttendees ATM
Left Join CompanyInfo CI on ATM.CompanyNameinBiz=CI.numCompanyId
WHERE ATM.ActivityID = ac.ActivityID and ATM.AttendeeEmail is not null
FOR XML PATH('''')
), 1, 1, ''''),''&lt;'', ''<''), ''&gt;'', ''>'')) AS [Action-Item Participants],ac.HtmlLink AS HtmlLink,ac.Location As Location,ac.ActivityDescription As OrignalDescription'



SET @strSql1 = @strSql1+' from activity ac join             
activityresource ar on  ac.activityid=ar.activityid             
join resource res on ar.resourceid=res.resourceid                         
--join usermaster UM on um.numUserId = res.numUserCntId                
join AdditionalContactsInformation ACI on ACI.numContactId = res.numUserCntId                
 JOIN DivisionMaster Div                                                          
 ON ACI.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                             
 ON Div.numCompanyID = Comp.numCompanyId
 OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=Div.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder                 
 OUTER APPLY(' + CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = Div.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		AND 1 = (CASE ',@tintPerformanceFilter,' 
																				WHEN 1 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-3,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 2 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-6,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 3 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(YEAR,-1,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				ELSE 1
																				END)
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') + ' AS TempPerformance
 where  res.numUserCntId = ' + Cast(@numUserCntID as varchar(10)) +'             
and ac.OriginalStartDateTimeUtc IS NULL            
and res.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  and 
(DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) <= '''+Cast(@endDate as varchar(30))+''')
--(startdatetimeutc between dateadd(month,-1,getutcdate()) and dateadd(month,1,getutcdate())) 
and [status] <> -1                                                     
and ac.activityid not in 
(select distinct(numActivityId) from communication Comm where Comm.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  
AND 1=(Case ' + Cast(@bitFilterRecord as varchar(10)) +' when 1 then Case when Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  then 1 else 0 end 
		else  Case when (Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  or Comm.numCreatedBy=' + Cast(@numUserCntID as varchar(10)) +' ) then 1 else 0 end end)   
		AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (Comm.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or Comm.numAssign= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END)                                                                   
-- AND (dtStartTime >= '''+Cast(@startDate as varchar(30))+''' and dtStartTime <= '''+Cast(@endDate as varchar(30))+''') 
) 
 Order by endtime OFFSET ' + CAST(((@CurrentPage - 1) * @PageSize) AS VARCHAR(50)) + ' ROWS FETCH NEXT ' + CAST(@PageSize AS VARCHAR(50)) + ' ROWS ONLY'
 
	END

-- btDocType =2 for Document approval request, =1 for bizdoc
SET @strSql2 = ' insert into #tempRecords
select 
B.numContactID,B.numDivisionID,E.tintCRMType,CASE BA.btDocType WHEN 2 THEN BA.numOppBizDocsId WHEN 3 THEN BA.numOppBizDocsId ELSE A.numBizActionID END as ID,
convert(varchar(15),A.bitTask) as bitTask,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) as Startdate,                        
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) as EndTime ,  
'''' as itemDesc,
vcFirstName + '' '' + vcLastName  as [Name],
B.vcFirstName , B.vcLastName ,B.numPhone,B.numPhoneExtension,  
case when B.numPhone<>'''' then + B.numPhone +case when B.numPhoneExtension<>'''' then '' - '' + B.numPhoneExtension else '''' end  else '''' end as [Phone],
vcCompanyName, ISNULL((SELECT vcData FROM ListDetails WHERE numListID =21 AND numDomainID='+ CAST(@numDomainID AS VARCHAR) +' AND  numListItemID=D.vcProfile ) ,'''') vcProfile ,
vcEmail,
CASE BA.btDocType WHEN 2  THEN  ''Document Approval Request'' ELSE ''BizDoc Approval Request'' END as Task,
dbo.GetListIemName(bitTask) as Activity,
'''' as Status,
A.numCreatedBy,
dbo.fn_GetContactName(A.numCreatedBy) as numRecOwner,
dbo.fn_GetContactName(A.numModifiedBy) as numModifiedBy,
 ISNULL(E.numTerId,0) AS numOrgTerId, 
 (select TOP 1 vcData from ListDetails where numListItemID=E.numTerId) AS numTerId, 
case When Len( dbo.fn_GetContactName(numAssign) ) > 12                                           
  Then Substring( dbo.fn_GetContactName(numAssign) , 0  ,  12 ) + ''..''                                           
  Else dbo.fn_GetContactName(numAssign)                                           
 end                                          
 numAssignedTo ,  
 ''-'' AS numAssignedBy,        
''0'' as caseid,
''0'' as vcCasenumber,
0 as CaseTimeId,
0 as CaseExpId,
CASE BA.btDocType WHEN 2 THEN 2 ELSE 3 END as [type], 
'''' AS itemid  ,      
 '''' AS changekey ,   
dtCreatedDate as DueDate  ,cast(0 as bit) as bitFollowUpAnyTime,
dbo.GetListIemName(D.numNoOfEmployeesId) AS numNoOfEmployeesId,E.vcComPhone,
dbo.GetListIemName(E.numFollowUpStatus) AS numFollowUpStatus,' + 
CONCAT('ISNULL(dbo.FormatedDateFromDate(E.dtLastFollowUp,',@numDomainID,'),'''')') + ' AS dtLastFollowUp,' + 
CONCAT('ISNULL(dbo.FormatedDateFromDate(TempLastOrder.bintCreatedDate,',@numDomainID,'),'''')') + ' AS vcLastSalesOrderDate,ISNULL(TempPerformance.monDealAmount,0) monDealAmount, (CASE WHEN ISNULL(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(FORMAT(TempPerformance.monDealAmount,''##,####.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) AS vcPerformance,'''' as vcColorScheme,'''',0,'''', 0 AS [numOppId],0 AS numBusinessProcessID '
SET @strSql2 =@strSql2 + ' ,(select TOP 1 vcData from ListDetails where numListItemID=D.numCompanyRating) AS numCompanyRating ,(select TOP 1 vcData from ListDetails where numListItemID=D.numCompanyRating) AS numCompanyRating,''-'' AS numCampaignID '+@vcnullCustomColumnName 
SET @strSql2 =@strSql2 +',(SELECT DISTINCT  
replace(replace(STUFF((SELECT ''^<br>'' 
+ CASE(ISNULL(vcCompanyName,'''')) WHEN '''' THEN '''' else ''<a href=javascript:OpenCompany('' +  cast(isnull(B.numDivisionID,0) as varchar(20)) + '','' +  cast(isnull(E.tintCRMType,0) as varchar(20)) + '',1) style=''''COLOR:black;TEXT-DECORATION:underline;''''>'' + ltrim(RTRIM(vcCompanyName)) +  ''</a> ''  END
+ CASE(isnull(B.vcFirstName,'''')) when '''' THEN ''''	else ''<a href=javascript:OpenContact('' +  cast(isnull(B.numContactId,0) as varchar(20)) + '',1) style=''''COLOR:#dd8047;''''>'' + ltrim(RTRIM(B.vcFirstName)) + ''</a> ''  END
+ CASE(isnull(B.vcLastName,'''')) when '''' THEN '''' else ''<a href=javascript:OpenContact('' +  cast(isnull(B.numContactId,0) as varchar(20)) + '',1) style=''''COLOR:#dd8047;''''>'' + ltrim(RTRIM(B.vcLastName)) + ''</a> ''  END 
+ CASE(isnull(B.numPhone,'''')) WHEN '''' then '''' ELSE B.numPhone + '' '' END
+ CASE(isnull(B.numPhoneExtension,'''')) when '''' then '''' else '' ('' + B.numPhoneExtension + '') '' END 
+ CASE(isnull(B.vcEmail,'''')) when '''' THEN '''' ELSE ''<a href=''''#'''' onclick=javascript:OpemEmail(''''../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail='' + ltrim(RTRIM(B.vcEmail)) + '''''') style=''''COLOR:#3c8dbc;''''>'' + ltrim(RTRIM(B.vcEmail)) + ''</a> ''  END 
FOR XML PATH('''')
), 1, 1, ''''),''&lt;'', ''<''), ''&gt;'', ''>'')) AS [Action-Item Participants],'''' AS HtmlLink,'''' As Location,'''' As OrignalDescription
FROM 
	BizDocAction A 
LEFT JOIN 
	dbo.BizActionDetails BA 
ON 
	BA.numBizActionId = A.numBizActionId, 
AdditionalContactsInformation B, 
CompanyInfo D,
DivisionMaster E
OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=E.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder
OUTER APPLY(' + CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = E.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		AND 1 = (CASE ',@tintPerformanceFilter,' 
																				WHEN 1 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-3,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 2 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-6,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 3 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(YEAR,-1,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				ELSE 1
																				END)
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') + ' AS TempPerformance
WHERE A.numContactId=b.numContactId and a.numDomainID=b.numDomainID  and 
D.numCompanyId=E.numCompanyId and 
D.numDomainID=a.numDomainID and 
E.numDivisionId=B.numDivisionId and 
a.numDomainID= ' + Cast(@numDomainID as varchar(10)) +' and a.numContactId = ' + Cast(@numUserCntID as varchar(10)) +'  and  
A.numStatus=0 
and (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) <= '''+Cast(@endDate as varchar(30))+''')'

END


declare @Nocolumns as tinyint         
set @Nocolumns=0       
         
Select @Nocolumns=isnull(count(*),0) from View_DynamicColumns where numFormId=43 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  
  
if @Nocolumns > 0            
begin      
INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcFieldDataType,vcOrigDbColumnName,bitAllowFiltering,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
 FROM View_DynamicColumns 
 where numFormId=43 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
  order by tintOrder asc  
  
end            
else            
begin 

	--Check if Master Form Configuration is created by administrator if NoColumns=0
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 43 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END

	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			43,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,NULL,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=43 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0

		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,vcFieldDataType,vcOrigDbColumnName,bitAllowFiltering, ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType,numListID,vcLookBackTableName, bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=43 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	ORDER BY 
		tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		 INSERT INTO #tempForm
		select tintOrder,vcDbColumnName,vcFieldDataType
		,vcOrigDbColumnName,bitAllowFiltering,ISNULL(vcCultureFieldName,vcFieldName),                  
		 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
		,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
		 FROM View_DynamicDefaultColumns
		 where numFormId=43 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 AND numDomainID=@numDomainID
		order by tintOrder asc  
	END 
end                                            

DECLARE @SQLtemp varchar(max)
SET @SQLtemp= 'update #tempRecords
SET [Action-Item Participants] = SUBSTRING([Action-Item Participants], 5, LEN([Action-Item Participants]))'

exec (@strSql + @strSql1 + @strSql2 + @SQLtemp )

DECLARE @strSql3 AS NVARCHAR(MAX)



SET @strSql3=   'SELECT COUNT(*) OVER() TotalRecords, * FROM #tempRecords '


IF CHARINDEX(@columnName,@strSql) > 0 OR CHARINDEX(@columnName,@strSql1) > 0 OR CHARINDEX(@columnName,@strSql2) > 0
BEGIN
	SET @strSql3= CONCAT(@strSql3,' order by ',CASE WHEN @columnName = 'Action-Item Participants' THEN CONCAT('[','Action-Item Participants',']') ELSE @columnName END,' ',@columnSortOrder,' OFFSET ',( @CurrentPage - 1 ) * @PageSize,' ROWS FETCH NEXT ',@PageSize,' ROWS ONLY')
END
ELSE
BEGIN
	SET @strSql3= CONCAT(@strSql3,' order by EndTime Asc OFFSET ',( @CurrentPage - 1 ) * @PageSize,' ROWS FETCH NEXT ',@PageSize,' ROWS ONLY')
END

     	exec sp_executesql @strSql3
	---- EXECUTE FINAL QUERY
	--DECLARE @strFinal AS VARCHAR(MAX)
	--SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, ',@strColumns,' INTO #tempTable',@strSql,'; SELECT * FROM #tempTable WHERE ID > ',@firstRec,' and ID <',@lastRec,' ORDER BY ID; DROP TABLE #tempTable;')


	--PRINT @strFinal

	

drop table #tempRecords

SELECT * FROM #tempForm order by tintOrder

DROP TABLE #tempForm
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO                 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype = 'p' AND NAME ='USP_UpdatedomainMinimumPriceUpdate')
DROP PROCEDURE USP_UpdatedomainMinimumPriceUpdate
GO
CREATE PROCEDURE [dbo].[USP_UpdatedomainMinimumPriceUpdate]
	@numApprovalRuleID AS NUMERIC(18) = 0, 
	@numListItemID AS NUMERIC(18) = 0,                                    
	@numDomainID AS NUMERIC(18) = 0,
	@numAbovePercent AS NUMERIC(18,2),
	@numBelowPercent AS NUMERIC(18,2),
	@numBelowPriceField AS NUMERIC(18,0),
	@bitCostApproval AS BIT,
	@bitListPriceApproval AS BIT,
	@bitMarginPriceViolated AS BIT,
	@vcUnitPriceApprover AS VARCHAR,
	@byteMode INT ,
	@IsMinUnitPriceRule AS BIT
AS                                      
BEGIN 
	UPDATE Domain SET bitMinUnitPriceRule =  @IsMinUnitPriceRule WHERE numDomainId=@numDomainID                      
	IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainID=@numDomainID AND ISNULL(numListItemID,0)=ISNULL(@numListItemID,0))
	BEGIN
		UPDATE 
			ApprovalProcessItemsClassification
		SET 
			numAbovePercent = @numAbovePercent,
			numBelowPercent = @numBelowPercent,
			numBelowPriceField = @numBelowPriceField,
			bitCostApproval = @bitCostApproval,
			bitListPriceApproval = @bitListPriceApproval,
			bitMarginPriceViolated = @bitMarginPriceViolated
		WHERE 
			numDomainId = @numDomainID
			AND ISNULL(numListItemID,0)=ISNULL(@numListItemID,0)
	END
	ELSE
	BEGIN
		INSERT INTO ApprovalProcessItemsClassification
		(
			numListItemID
			,numDomainID
			,numAbovePercent
			,numBelowPercent
			,numBelowPriceField
			,bitCostApproval
			,bitListPriceApproval
			,bitMarginPriceViolated
		)
		VALUES
		(
			@numListItemID
			,@numDomainID
			,@numAbovePercent
			,@numBelowPercent
			,@numBelowPriceField
			,@bitCostApproval
			,@bitListPriceApproval
			,@bitMarginPriceViolated
		)
	END
END

GO
/****** Object:  StoredProcedure [dbo].[USP_UpdatingInventory]    Script Date: 06/01/2009 23:48:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj       
--exec usp_updatinginventory 0,181193,1,0,1       
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatinginventory')
DROP PROCEDURE usp_updatinginventory
GO
CREATE PROCEDURE [dbo].[USP_UpdatingInventory]        
	@byteMode AS TINYINT=0,  
	@numWareHouseItemID AS NUMERIC(18,0)=0,  
	@Units AS FLOAT,
	@numWOId AS NUMERIC(9)=0,
	@numUserCntID AS NUMERIC(9)=0,
	@numOppId AS NUMERIC(9)=0,
	@numAssembledItemID AS NUMERIC(18,0) = 0,
	@fltQtyRequiredForSingleBuild AS FLOAT = 0   
AS      
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @Description AS VARCHAR(200)
	DECLARE @numItemCode NUMERIC(18)
	DECLARE @numDomain AS NUMERIC(18,0)
	DECLARE @ParentWOID AS NUMERIC(18,0)
	DECLARE @numDivisionID NUMERIC(18,0)
	DECLARE @numOverheadServiceItemID NUMERIC(18,0)

	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @ParentWOID=numParentWOID FROM WorkOrder WHERE numWOId=@numWOId
	SELECT @numItemCode=numItemID,@numDomain = numDomainID from WareHouseItems where numWareHouseItemID = @numWareHouseItemID

	SELECT @numOverheadServiceItemID=ISNULL(numOverheadServiceItemID,0) FROM Domain WHERE numDomainId=@numDomain

	IF ISNULL(@numOppId,0) > 0
	BEGIN
		SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomain
		SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM OpportunityMaster INNER JOIN DivisionMaster ON OpportunityMaster.numDivisionId=DivisionMaster.numDivisionID WHERE numOppId=@numOppId
	END
  
	IF @byteMode=0  -- Aseeembly Item
	BEGIN  
		DECLARE @CurrentAverageCost DECIMAL(20,5)
		DECLARE @TotalCurrentOnHand FLOAT
		DECLARE @newAverageCost DECIMAL(20,5)
	
		SELECT @CurrentAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE monAverageCost END) FROM dbo.Item WHERE numItemCode =@numItemCode 
		SELECT @TotalCurrentOnHand = ISNULL((SUM(numOnHand) + SUM(numAllocation)),0) FROM dbo.WareHouseItems WHERE numItemID =@numItemCode 
		PRINT @CurrentAverageCost
		PRINT @TotalCurrentOnHand

		IF @numWOId > 0
		BEGIN
			DECLARE @monOverheadCost DECIMAL(20,5) 
			DECLARE @monLabourCost DECIMAL(20,5)

			-- Store hourly rate for all task assignee
			UPDATE 
				SPDT
			SET
				SPDT.monHourlyRate = ISNULL(UM.monHourlyRate,0)
			FROM
				StagePercentageDetailsTask SPDT
			INNER JOIN
				UserMaster UM
			ON
				SPDT.numAssignTo = UM.numUserDetailId
			WHERE
				SPDT.numDomainID = @numDomain
				AND SPDT.numWorkOrderId = @numWOID

			SELECT 
				@newAverageCost = SUM(numQtyItemsReq * ISNULL(I.monAverageCost,0))
			FROM 
				WorkOrderDetails WOD
			LEFT JOIN 
				dbo.Item I 
			ON 
				I.numItemCode = WOD.numChildItemID
			WHERE 
				WOD.numWOId = @numWOId
				AND I.charItemType = 'P'

			IF @numOverheadServiceItemID > 0 AND EXISTS (SELECT numWODetailId FROM WorkOrderDetails WHERE numWOId=@numWOId AND numChildItemID=@numOverheadServiceItemID)
			BEGIN
				SET @monOverheadCost = (SELECT SUM(numQtyItemsReq * ISNULL(Item.monListPrice,0)) FROM WorkOrderDetails WOD INNER JOIN Item ON WOD.numChildItemID=Item.numItemCode WHERE WOD.numWOId=@numWOId AND WOD.numChildItemID=@numOverheadServiceItemID)
			END

			SET @monLabourCost = dbo.GetWorkOrderLabourCost(@numDomain,@numWOId)			

			UPDATE WorkOrder SET monAverageCost=@newAverageCost,monLabourCost=ISNULL(@monLabourCost,0),monOverheadCost=ISNULL(@monOverheadCost,0) WHERE numWOId=@numWOId

			SET @newAverageCost = ((@CurrentAverageCost * @TotalCurrentOnHand)  + (@newAverageCost + ISNULL(@monOverheadCost,0) + ISNULL(@monLabourCost,0))) / (@TotalCurrentOnHand + @Units)
		END
		ELSE
		BEGIN
			SELECT 
				@newAverageCost = SUM((numQtyItemsReq * dbo.fn_UOMConversion(ID.numUOMID,I.numItemCode,I.numDomainId,I.numBaseUnit)) * I.monAverageCost )
			FROM 
				dbo.ItemDetails ID
			LEFT JOIN 
				dbo.Item I 
			ON 
				I.numItemCode = ID.numChildItemID
			WHERE 
				numItemKitID = @numItemCode
				AND I.charItemType = 'P'

			UPDATE AssembledItem SET monAverageCost=@newAverageCost WHERE ID=@numAssembledItemID

			SET @newAverageCost = ((@CurrentAverageCost * @TotalCurrentOnHand)  + (@newAverageCost * @Units)) / (@TotalCurrentOnHand + @Units)
		END		
	
		UPDATE item SET monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @newAverageCost END) WHERE numItemCode = @numItemCode

		IF ISNULL(@numWOId,0) > 0
		BEGIN
			UPDATE 
				WareHouseItems 
			SET 
				numOnHand = ISNULL(numOnHand,0) + (CASE WHEN ISNULL(numBackOrder,0) >= @Units THEN 0 ELSE @Units - ISNULL(numBackOrder,0) END)
				,numAllocation = ISNULL(numAllocation,0) + (CASE WHEN ISNULL(numBackOrder,0) > @Units THEN @Units ELSE ISNULL(numBackOrder,0) END)
				,numBackOrder = (CASE WHEN ISNULL(numBackOrder,0) > @Units THEN ISNULL(numBackOrder,0) - @Units ELSE 0 END)  
				,numOnOrder = (CASE WHEN @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=0 AND @numOppId > 0 THEN numOnOrder ELSE (CASE WHEN ISNULL(numOnOrder,0) < @Units THEN 0 ELSE ISNULL(numOnOrder,0) - @Units END) END) 
				,dtModified = GETDATE()  
			WHERE 
				numWareHouseItemID=@numWareHouseItemID  
		END
		ELSE
		BEGIN
			UPDATE 
				WareHouseItems 
			SET 
				numOnHand = ISNULL(numOnHand,0) + (CASE WHEN ISNULL(numBackOrder,0) >= @Units THEN  0 ELSE @Units - ISNULL(numBackOrder,0) END)
				,numAllocation = ISNULL(numAllocation,0) + (CASE WHEN ISNULL(numBackOrder,0) > @Units THEN @Units ELSE ISNULL(numBackOrder,0) END)
				,numBackOrder = (CASE WHEN ISNULL(numBackOrder,0) > @Units THEN ISNULL(numBackOrder,0) - @Units ELSE 0 END)
				,dtModified = GETDATE() 
			WHERE 
				numWareHouseItemID=@numWareHouseItemID  
		END
	END  
	ELSE IF @byteMode=1  --Aseembly Child item
	BEGIN  
		DECLARE @onHand as FLOAT                                    
		DECLARE @onOrder as FLOAT                                    
		DECLARE @onBackOrder as FLOAT                                   
		DECLARE @onAllocation as FLOAT    
		DECLARE @monAverageCost AS DECIMAL(20,5)


		SELECT @monAverageCost= (CASE WHEN ISNULL(bitVirtualInventory,0)=1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE  numItemCode=@numItemCode

		IF @numWOId > 0
		BEGIN
			UPDATE
				WorkOrderDetails
			SET
				monAverageCost=@monAverageCost
			WHERE
				numWOId=@numWOId
				AND numChildItemID=@numItemCode
				AND numWareHouseItemId=@numWareHouseItemID
		END
		ELSE
		BEGIN
			-- KEEP HISTRIY OF AT WHICH PRICE CHILD ITEM IS USED IN ASSEMBLY BUILDING
			-- USEFUL IN CALCULATING AVERAGECOST WHEN DISASSEMBLING ITEM
			INSERT INTO [dbo].[AssembledItemChilds]
			(
				numAssembledItemID,
				numItemCode,
				numQuantity,
				numWarehouseItemID,
				monAverageCost,
				fltQtyRequiredForSingleBuild
			)
			VALUES
			(
				@numAssembledItemID,
				@numItemCode,
				@Units,
				@numWareHouseItemID,
				@monAverageCost,
				@fltQtyRequiredForSingleBuild
			)
		END

		select                                     
			@onHand=isnull(numOnHand,0),                                    
			@onAllocation=isnull(numAllocation,0),                                    
			@onOrder=isnull(numOnOrder,0),                                    
			@onBackOrder=isnull(numBackOrder,0)                                     
		from 
			WareHouseItems 
		where 
			numWareHouseItemID=@numWareHouseItemID  


		IF ISNULL(@numWOId,0) > 0
		BEGIN
			IF @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=0 AND @numOppId > 0 -- Allocation When Fulfillment Order (Packing Slip) is added
			BEGIN
				IF @onHand >= @Units
				BEGIN
					--RELEASE QTY FROM ON HAND
					SET @onHand=@onHand-@Units
				END
				ELSE
				BEGIN
					RAISERROR('NOTSUFFICIENTQTY_ONHAND',16,1)
				END
			END
			ELSE
			BEGIN
				IF @onAllocation >= @Units
				BEGIN
					--RELEASE QTY FROM ALLOCATION
					SET @onAllocation=@onAllocation-@Units
				END
				ELSE
				BEGIN
					RAISERROR('NOTSUFFICIENTQTY_ALLOCATION',16,1)
				END
			END
		END
		ELSE
		BEGIN
			--DECREASE QTY FROM ONHAND
			SET @onHand=@onHand-@Units
		END

		 --UPDATE INVENTORY
		UPDATE 
			WareHouseItems 
		SET      
			numOnHand=@onHand,
			numOnOrder=@onOrder,                         
			numAllocation=@onAllocation,
			numBackOrder=@onBackOrder,
			dtModified = GETDATE()                                    
		WHERE 
			numWareHouseItemID=@numWareHouseItemID   
	END

	IF @numWOId>0
	BEGIN
    
		DECLARE @numReferenceID NUMERIC(18,0)
		DECLARE @tintRefType NUMERIC(18,0)

		IF ISNULL(@numOppId,0)>0 --FROM SALES ORDER 
		BEGIN
			IF @byteMode = 0 --Aseeembly Item
				SET @Description = 'Work Order Completed (Qty: ' + CONVERT(varchar(10),@Units) + ')'
			ELSE IF @byteMode =1 --Aseembly Child item
				IF ISNULL(@ParentWOID,0) = 0
				BEGIN
					SET @Description = 'Items Used In SO-WO (Qty: ' + CONVERT(varchar(10),@Units) + ')'
				END
				ELSE
				BEGIN
					SET @Description = 'Items Used In SO-WO Work Order (Qty: ' + CONVERT(varchar(10),@Units) + ')'
				END

			SET @numReferenceID = @numWOId
			SET @tintRefType = 2
		END
		ELSE --FROM CREATE ASSEMBLY SCREEN FROM ITEM
		BEGIN
			IF @byteMode = 0 --Aseeembly Item
				SET @Description = 'Work Order Completed (Qty: ' + CONVERT(varchar(10),@Units) + ')'
			ELSE IF @byteMode =1 --Aseembly Child item
				SET @Description = 'Items Used In Work Order (Qty: ' + CONVERT(varchar(10),@Units) + ')'

			SET @numReferenceID = @numWOId
			SET @tintRefType=2
		END

		EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numReferenceID, --  numeric(9, 0)
					@tintRefType = @tintRefType, --  tinyint
					@vcDescription = @Description, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@numDomainID = @numDomain
	END
	ELSE if @byteMode=0
	BEGIN
	DECLARE @desc AS VARCHAR(100)
	SET @desc= 'Create Assembly:' + CONVERT(varchar(10),@Units) + ' Units';

	  EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numItemCode, --  numeric(9, 0)
					@tintRefType = 1, --  tinyint
					@vcDescription = @desc, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@numDomainID = @numDomain
	END
	ELSE if @byteMode=1
	BEGIN
	SET @Description = 'Item Used in Assembly (Qty: ' + CONVERT(varchar(10),@Units) + ')' 

	EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numItemCode, --  numeric(9, 0)
					@tintRefType = 1, --  tinyint
					@vcDescription = @Description, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@numDomainID = @numDomain
	END 
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WarehouseItems_GetByItem')
DROP PROCEDURE USP_WarehouseItems_GetByItem
GO
CREATE PROCEDURE [dbo].[USP_WarehouseItems_GetByItem]
(   
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@numWLocationID NUMERIC(18,0)
)
AS
BEGIN
	DECLARE @numBaseUnit AS NUMERIC(18,0)
	DECLARE @numSaleUnit AS NUMERIC(18,0)
	DECLARE @numPurchaseUnit AS NUMERIC(18,0)
	DECLARE @numSaleUOMFactor AS FLOAT
	DECLARE @numPurchaseUOMFactor AS FLOAT

	SELECT 
		@numBaseUnit = ISNULL(numBaseUnit,0),
		@numSaleUnit = ISNULL(numSaleUnit,0),
		@numPurchaseUnit = ISNULL(numPurchaseUnit,0)
	FROM 
		Item 
	WHERE 
		numItemCode=@numItemCode   

	SELECT @numSaleUOMFactor = dbo.fn_UOMConversion(@numBaseUnit,@numItemCode,@numDOmainID,@numSaleUnit)
	SELECT @numPurchaseUOMFactor = dbo.fn_UOMConversion(@numBaseUnit,@numItemCode,@numDOmainID,@numPurchaseUnit)

	SELECT
		WarehouseItems.numWareHouseItemID
		,WarehouseItems.numWarehouseID
		,WarehouseItems.numWLocationID
		,ISNULL(Warehouses.vcWareHouse,'') vcExternalLocation
		,numItemID
		,Item.numAssetChartAcntId
		,CASE 
			WHEN ISNULL(Item.bitKitParent,0)=1 then CAST(ISNULL(dbo.fn_GetKitInventoryByWarehouse(Item.numItemCode, WareHouseItems.numWareHouseID),0) AS FLOAT) 
			ELSE (CASE 
					WHEN EXISTS (SELECT WI.numWareHouseItemID FROM WareHouseItems WI INNER JOIN WarehouseLocation WL ON WI.numWLocationID=WL.numWLocationID WHERE WI.numDomainID=@numDomainID AND WI.numItemID=@numItemCode AND WI.numWareHouseID=WareHouseItems.numWareHouseID)
					THEN ISNULL((SELECT SUM(WI.numOnHand) FROM WareHouseItems WI INNER JOIN WarehouseLocation WL ON WI.numWLocationID=WL.numWLocationID WHERE WI.numDomainID=@numDomainID AND WI.numItemID=@numItemCode AND WI.numWareHouseID=WareHouseItems.numWareHouseID),0)
					ELSE ISNULL(numOnHand,0)
				END)  
		END AS [OnHand]
		,CASE WHEN ISNULL(Item.bitKitParent,0)=1 THEN 0 ELSE CAST(ISNULL(numOnOrder,0) AS FLOAT) END as [OnOrder]
		,CASE 
			WHEN ISNULL(Item.bitKitParent,0)=1 OR ISNULL(Item.bitAssembly,0)=1 
			THEN 0 
			ELSE CAST(ISNULL((SELECT 
									SUM(numUnitHour) 
								FROM 
									OpportunityItems OI 
								INNER JOIN 
									OpportunityMaster OM 
								ON 
									OI.numOppID=OM.numOppID 
								WHERE 
									OM.numDomainID=@numDomainID 
									AND tintOppType=1 
									AND tintOppStatus=0 
									AND OI.numItemCode=WareHouseItems.numItemID 
									AND OI.numWarehouseItmsID=WareHouseItems.numWareHouseItemID),0) AS FLOAT) 
		END AS [Requisitions]
		,CASE WHEN ISNULL(Item.bitKitParent,0)=1 THEN 0 ELSE CAST(ISNULL(numAllocation,0) AS FLOAT) END as [Allocation] 
		,CASE WHEN ISNULL(Item.bitKitParent,0)=1 THEN 0 ELSE CAST(ISNULL(numBackOrder,0) AS FLOAT) END as [BackOrder]
		,(CASE 
			WHEN EXISTS (SELECT WI.numWareHouseItemID FROM WareHouseItems WI INNER JOIN WarehouseLocation WL ON WI.numWLocationID=WL.numWLocationID WHERE WI.numDomainID=@numDomainID AND WI.numItemID=@numItemCode AND WI.numWareHouseID=WareHouseItems.numWareHouseID)
			THEN ISNULL((SELECT SUM(ISNULL(WI.numOnHand,0) + ISNULL(WI.numAllocation,0)) FROM WareHouseItems WI INNER JOIN WarehouseLocation WL ON WI.numWLocationID=WL.numWLocationID WHERE WI.numDomainID=@numDomainID AND WI.numItemID=@numItemCode AND WI.numWareHouseID=WareHouseItems.numWareHouseID),0)
			ELSE ISNULL(WareHouseItems.numOnHand,0) + ISNULL(WareHouseItems.numAllocation,0)
		END) AS TotalOnHand
		,ISNULL(Item.monAverageCost,0) monAverageCost
		,((ISNULL(WareHouseItems.numOnHand,0) + ISNULL(WareHouseItems.numAllocation,0)) * (CASE WHEN ISNULL(Item.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(Item.monAverageCost,0) END)) AS monCurrentValue
		,ROUND(ISNULL(WareHouseItems.monWListPrice,0),2) Price
		,ISNULL(WareHouseItems.vcWHSKU,'') as SKU
		,ISNULL(WareHouseItems.vcBarCode,'') as BarCode
		,CASE 
			WHEN ISNULL(Item.numItemGroup,0) > 0 
			THEN dbo.fn_GetAttributes(WareHouseItems.numWareHouseItemID,0)
			ELSE ''
		END AS vcAttribute
		,dbo.fn_GetUOMName(Item.numBaseUnit) As vcBaseUnit
		,dbo.fn_GetUOMName(Item.numSaleUnit) As vcSaleUnit
		,dbo.fn_GetUOMName(Item.numPurchaseUnit) As vcPurchaseUnit
		,CAST((@numSaleUOMFactor * Case when ISNULL(Item.bitKitParent,0)=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(Item.numItemCode, Warehouses.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as numeric(18,2)) as OnHandUOM
		,CAST((@numPurchaseUOMFactor * Case when ISNULL(Item.bitKitParent,0)=1 THEN 0 ELSE isnull(numOnOrder,0) END) AS DECIMAL(18,2)) as OnOrderUOM
		,CAST((@numPurchaseUOMFactor * Case when ISNULL(Item.bitKitParent,0)=1 THEN 0 ELSE isnull(numReorder,0) END) AS DECIMAL(18,2)) as ReorderUOM
		,CAST((@numSaleUOMFactor * Case when ISNULL(Item.bitKitParent,0)=1 THEN 0 ELSE isnull(numAllocation,0) END) AS DECIMAL(18,2)) as AllocationUOM
		,CAST((@numSaleUOMFactor * Case when ISNULL(Item.bitKitParent,0)=1 THEN 0 ELSE isnull(numBackOrder,0) END) AS DECIMAL(18,2)) as BackOrderUOM
		,CONCAT('[',STUFF((SELECT 
								CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,',"WarehouseLocationID":',WIInner.numWLocationID,',"Warehouse":"',ISNULL(W.vcWareHouse,''),'", "OnHand":',ISNULL(WIInner.numOnHand,0),', "Location":"',ISNULL(WL.vcLocation,''),'"}') 
							FROM 
								WareHouseItems WIInner
							INNER JOIN
								Warehouses W
							ON
								WIInner.numWareHouseID = W.numWareHouseID
							INNER JOIN
								WarehouseLocation WL
							ON
								WIInner.numWLocationID = WL.numWLocationID
							WHERE 
								WIInner.numDomainID=@numDomainID 
								AND WIInner.numItemID=@numItemCode
								AND WIInner.numWareHouseID=WareHouseItems.numWareHouseID
							ORDER BY
								WL.vcLocation ASC
							FOR XML PATH('')),1,1,''),']') vcInternalLocations
	FROM
		WareHouseItems
	INNER JOIN
		Item
	ON
		WareHouseItems.numItemID = Item.numItemCode
	INNER JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	WHERE
		WareHouseItems.numDomainID=@numDomainID
		AND WareHouseItems.numItemID=@numItemCode
		AND ISNULL(WareHouseItems.numWLocationID,0) = 0
		AND (ISNULL(@numWarehouseID,0) = 0 OR WareHouseItems.numWareHouseID=@numWarehouseID)
		AND (ISNULL(@numWLocationID,0) = 0 OR EXISTS (SELECT WI.numWareHouseItemID FROM WareHouseItems WI WHERE WI.numDomainID=@numDomainID AND WI.numItemID=@numItemCode AND WI.numWareHouseID=@numWarehouseID AND ISNULL(WI.numWLocationID,0) = @numWLocationID))
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_GetBOMForPick')
DROP PROCEDURE USP_WorkOrder_GetBOMForPick
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_GetBOMForPick]                             
	@numDomainID NUMERIC(18,0)
	,@vcWorkOrders VARCHAR(MAX)
	,@ClientTimeZoneOffset INT               
AS                            
BEGIN
	SELECT
		WorkOrder.numWOId
		,WorkOrderDetails.numWODetailId
		,ISNULL(WorkOrder.vcWorkOrderName,'-') vcWorkOrderName
		,CONCAT(Item.vcItemName, ' (',WorkOrder.numQtyItemsReq,')') vcAssembly
		,(CASE 
			WHEN OpportunityItems.ItemReleaseDate IS NOT NULL 
			THEN dbo.FormatedDateFromDate(OpportunityItems.ItemReleaseDate,@numDomainID) 
			ELSE dbo.FormatedDateFromDate(WorkOrder.dtmEndDate,@numDomainID)
		END) vcRequestedDate
		,ItemChild.numItemCode
		,CONCAT(ItemChild.vcItemName, ' (',WorkOrderDetails.numQtyItemsReq,')') vcBOM
		,WorkOrderDetails.numQtyItemsReq numRequiredQty
		,ISNULL((SELECT SUM(numPickedQty) FROM WorkOrderPickedItems WOPI WHERE WOPI.numWODetailId=WorkOrderDetails.numWODetailId),0) numPickedQty
		,WareHouseItems.numWareHouseID
		,WareHouseItems.numWareHouseItemID
		,ISNULL(Warehouses.vcWareHouse,'') vcWarehouse
		,ISNULL(WareHouseItems.numAllocation,0) numAllocation
		,(CASE 
			WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner INNER JOIN WarehouseLocation WL ON WIInner.numWLocationID=WL.numWLocationID WHERE WIInner.numDomainID=@numDomainID AND WIInner.numItemID=ItemChild.numItemCode AND WIInner.numWareHouseID = WareHouseItems.numWareHouseID)
			THEN CONCAT('[',STUFF((SELECT 
										CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "Available":',(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)),', "Location":"',WL.vcLocation,'"}') 
									FROM 
										WareHouseItems WIInner
									INNER JOIN
										WarehouseLocation WL
									ON
										WIInner.numWLocationID = WL.numWLocationID
									WHERE 
										WIInner.numDomainID=@numDomainID 
										AND WIInner.numItemID=ItemChild.numItemCode
										AND WIInner.numWareHouseID = WareHouseItems.numWareHouseID
									ORDER BY
										WL.vcLocation
									FOR XML PATH('')),1,1,''),']')
			ELSE CONCAT('[',STUFF((SELECT 
								CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "Available":',(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)),', "Location":''''}')
							FROM 
								WareHouseItems WIInner
							WHERE 
								WIInner.numDomainID=@numDomainID 
								AND WIInner.numItemID=ItemChild.numItemCode
								AND WIInner.numWareHouseID = WareHouseItems.numWareHouseID
							FOR XML PATH('')),1,1,''),']')
		END)  vcWarehouseLocations
	FROM
		WorkOrder
	INNER JOIN
		Item
	ON
		WorkOrder.numItemCode = Item.numItemCode
	LEFT JOIN
		OpportunityItems
	ON
		WorkOrder.numOppId = OpportunityItems.numOppId
		AND WorkOrder.numOppItemID = OpportunityItems.numoppitemtCode
	INNER JOIN
		WorkOrderDetails
	ON
		WorkOrder.numWOId = WorkOrderDetails.numWOId
	INNER JOIN
		Item ItemChild
	ON
		WorkOrderDetails.numChildItemID = ItemChild.numItemCode
	INNER JOIN
		WareHouseItems
	ON
		WorkOrderDetails.numWareHouseItemId = WareHouseItems.numWareHouseItemID
	INNER JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	WHERE
		WorkOrder.numDomainID = @numDomainID
		AND WorkOrder.numWOId IN (SELECT Id FROM dbo.SplitIDs(@vcWorkOrders,','))
		AND (WorkOrderDetails.numQtyItemsReq - ISNULL((SELECT SUM(numPickedQty) FROM WorkOrderPickedItems WOPI WHERE WOPI.numWODetailId=WorkOrderDetails.numWODetailId),0)) > 0
	ORDER BY
		WorkOrder.numWOId
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_GetStatus')
DROP PROCEDURE USP_WorkOrder_GetStatus
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_GetStatus]                             
	@numDomainID NUMERIC(18,0)
	,@vcWorkOrderIds VARCHAR(1000)                   
AS                            
BEGIN
	DECLARE @TEMPWorkOrder TABLE
	(
		ID INT IDENTITY(1,1)
		,numWOID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numQtyToBuild FLOAT
		,numWarehouseID NUMERIC(18,0)
		,numWorkOrderStatus TINYINT
		,vcWorkOrderStatus VARCHAR(300)
	)

	INSERT INTO @TEMPWorkOrder
	(
		numWOID
		,numItemCode
		,numQtyToBuild
		,numWarehouseID
		,numWorkOrderStatus
		,vcWorkOrderStatus
	)
	SELECT
		numWOID
		,numItemCode
		,numQtyToBuild
		,numWarehouseID
		,numWorkOrderStatus
		,vcWorkOrderStatus
	FROM
		dbo.GetWOStatus(@numDomainID,@vcWorkOrderIds)
	

	SELECT * FROM @TEMPWorkOrder WHERE numWOID IN (SELECT Id FROM dbo.SplitIDs(@vcWorkOrderIds,','))
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_InsertRecursive')
DROP PROCEDURE USP_WorkOrder_InsertRecursive
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_InsertRecursive]
	@numOppID AS NUMERIC(18,0),
	@numOppItemID NUMERIC(18,0),
	@numOppChildItemID NUMERIC(18,0),
	@numOppKitChildItemID NUMERIC(18,0),
	@numItemCode AS NUMERIC(18,0),
	@numQty AS FLOAT,
	@numWarehouseItemID AS NUMERIC(18,0),
	@numDomainID AS NUMERIC(18,0),
	@numUserCntID AS NUMERIC(18,0)=0,
	@numQtyShipped AS FLOAT,
	@numParentWOID AS NUMERIC(18,0),
	@bitFromWorkOrderScreen AS BIT
AS
BEGIN
	DECLARE @numWOID NUMERIC(18,0)
	DECLARE @vcInstruction VARCHAR(2000)
	DECLARE @numAssignedTo NUMERIC(18,0)
	DECLARE @bintCompletionDate DATETIME
	DECLARE @numWarehouseID NUMERIC(18,0)
	DECLARE @numBuildProcessId NUMERIC(18,0)
	DECLARE @dtmStartDate DATETIME
	DECLARE @dtmEndDate DATETIME

	IF NOT EXISTS (SELECT 
						numWOId
					FROM 
						WorkOrder 
					WHERE 
						numDomainID=@numDomainID 
						AND numItemCode=@numItemCode
						AND numParentWOID=@numParentWOID)
	BEGIN
		SELECT @numWarehouseID=numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID = @numWarehouseItemID

		IF (SELECT 
				COUNT(*)
			FROM 
				item                                
			INNER JOIN 
				ItemDetails Dtl 
			ON 
				numChildItemID=numItemCode
			WHERE 
				numItemKitID=@numItemCode
				AND item.charItemType = 'P'
				AND NOT EXISTS (SELECT numWareHouseItemId FROM WarehouseItems WHERE numItemID=Dtl.numChildItemID AND numWareHouseID=@numWarehouseID)) > 0
		BEGIN
			RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			RETURN
		END

		SELECT 
			@numBuildProcessId = ISNULL(Item.numBusinessProcessId,0) 
			,@numAssignedTo=ISNULL(Sales_process_List_Master.numBuildManager,0)
		FROM 
			Item 
		INNER JOIN 
			Sales_process_List_Master 
		ON 
			Item.numBusinessProcessId=Sales_process_List_Master.Slp_Id 
		WHERE 
			numItemCode=@numItemCode

		SELECT 
			@vcInstruction=vcInstruction
			,@bintCompletionDate=bintCompliationDate 
			,@dtmStartDate=dtmStartDate
			,@dtmEndDate=dtmEndDate
		FROM 
			WorkOrder 
		WHERE 
			numWOId=@numParentWOID

		INSERT INTO WorkOrder
		(
			numItemCode,numQtyItemsReq,numWareHouseItemId,numCreatedBy,bintCreatedDate,numDomainID,numWOStatus,numOppId,numOppItemID,numOppChildItemID,numOppKitChildItemID,numParentWOID,vcInstruction,numAssignedTo,bintCompliationDate,numBuildProcessId,dtmStartDate,dtmEndDate
		)
		VALUES
		(
			@numItemCode,@numQty,@numWarehouseItemID,@numUserCntID,getutcdate(),@numDomainID,0,@numOppID,@numOppItemID,@numOppChildItemID,@numOppKitChildItemID,@numParentWOID,@vcInstruction,@numAssignedTo,@bintCompletionDate,@numBuildProcessId,@dtmStartDate,@dtmEndDate
		)

		SELECT @numWOID = SCOPE_IDENTITY()

		EXEC dbo.USP_UpdateNameTemplateValueForWorkOrder 3,@numDomainID,@numWOId
		IF(@numBuildProcessId>0)
		BEGIN
				INSERT  INTO Sales_process_List_Master ( Slp_Name,
													 numdomainid,
													 pro_type,
													 numCreatedby,
													 dtCreatedon,
													 numModifedby,
													 dtModifiedOn,tintConfiguration,bitAutomaticStartTimer,bitAssigntoTeams,numOppId,numTaskValidatorId,
													 numProjectId,numWorkOrderId)
			 SELECT Slp_Name,
					numdomainid,
					pro_type,
					numCreatedby,
					dtCreatedon,
					numModifedby,
					dtModifiedOn,tintConfiguration,bitAutomaticStartTimer,bitAssigntoTeams,0,numTaskValidatorId,0,@numWOId FROM Sales_process_List_Master WHERE Slp_Id=@numBuildProcessId    
				DECLARE @numNewProcessId AS NUMERIC(18,0)=0
				SET @numNewProcessId=(SELECT SCOPE_IDENTITY())  
				UPDATE WorkOrder SET numBuildProcessId=@numNewProcessId WHERE numWOId=@numWOId
				INSERT INTO StagePercentageDetails(
				numStagePercentageId, 
				tintConfiguration, 
				vcStageName, 
				numDomainId, 
				numCreatedBy, 
				bintCreatedDate, 
				numModifiedBy, 
				bintModifiedDate, 
				slp_id, 
				numAssignTo, 
				vcMileStoneName, 
				tintPercentage, 
				tinProgressPercentage, 
				dtStartDate, 
				numParentStageID, 
				intDueDays, 
				numProjectID, 
				numOppID, 
				vcDescription, 
				bitIsDueDaysUsed,
				numTeamId, 
				bitRunningDynamicMode,
				numStageOrder,
				numWorkOrderId
			)
			SELECT
				numStagePercentageId, 
				tintConfiguration, 
				vcStageName, 
				numDomainId, 
				@numUserCntID, 
				GETDATE(), 
				@numUserCntID, 
				GETDATE(), 
				@numNewProcessId, 
				numAssignTo, 
				vcMileStoneName, 
				tintPercentage, 
				tinProgressPercentage, 
				GETDATE(), 
				numParentStageID, 
				intDueDays, 
				0, 
				0, 
				vcDescription, 
				bitIsDueDaysUsed,
				numTeamId, 
				bitRunningDynamicMode,
				numStageOrder,
				@numWOId
			FROM
				StagePercentageDetails
			WHERE
				slp_id=@numBuildProcessId	

				INSERT INTO StagePercentageDetailsTask(
				numStageDetailsId, 
				vcTaskName, 
				numHours, 
				numMinutes, 
				numAssignTo, 
				numDomainID, 
				numCreatedBy, 
				dtmCreatedOn,
				numOppId,
				numProjectId,
				numParentTaskId,
				bitDefaultTask,
				bitSavedTask,
				numOrder,
				numReferenceTaskId,
				numWorkOrderId
			)
			SELECT 
				(SELECT TOP 1 SFD.numStageDetailsId FROM StagePercentageDetails AS SFD WHERE SFD.vcStageName=SP.vcStageName AND SFD.numWorkOrderId=@numWOId),
				vcTaskName,
				numHours,
				numMinutes,
				ST.numAssignTo,
				@numDomainID,
				@numUserCntID,
				GETDATE(),
				0,
				0,
				0,
				1,
				CASE WHEN SP.bitRunningDynamicMode=1 THEN 0 ELSE 1 END,
				numOrder,
				numTaskId,
				@numWOId
			FROM 
				StagePercentageDetailsTask AS ST
			LEFT JOIN
				StagePercentageDetails As SP
			ON
				ST.numStageDetailsId=SP.numStageDetailsId
			WHERE
				ISNULL(ST.numOppId,0)=0 AND ISNULL(ST.numProjectId,0)=0 AND
				ST.numStageDetailsId IN(SELECT numStageDetailsId FROM 
				StagePercentageDetails As ST
			WHERE
				ST.slp_id=@numBuildProcessId)
			ORDER BY ST.numOrder
		END

		INSERT INTO [WorkOrderDetails] 
		(
			numWOId,numItemKitID,numChildItemID,numQtyItemsReq,numWareHouseItemId,vcItemDesc,sintOrder,numQtyItemsReq_Orig,numUOMId
		)
		SELECT 
			@numWOId,numItemKitID,numItemCode,
			CAST(((DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(Dtl.numUOMId,item.numItemCode,item.numDomainID,item.numBaseUnit),1)) * @numQty)AS FLOAT),
			isnull((SELECT TOP 1 numWareHouseItemId FROM WarehouseItems WHERE numItemID=item.numItemCode AND numWareHouseID=@numWarehouseID ORDER BY ISNULL(numWLocationID,0),numWareHouseItemID),0),
			ISNULL(Dtl.vcItemDesc,txtItemDesc),
			ISNULL(sintOrder,0),
			DTL.numQtyItemsReq,
			Dtl.numUOMId 
		FROM 
			item                                
		INNER JOIN 
			ItemDetails Dtl 
		ON 
			numChildItemID=numItemCode
		WHERE 
			numItemKitID=@numItemCode

		DECLARE @Description AS VARCHAR(1000)

		--UPDATE ON ORDER OF ASSEMBLY
		UPDATE 
			WareHouseItems
		SET    
			numOnOrder= ISNULL(numOnOrder,0) + @numQty,
			dtModified = GETDATE() 
		WHERE   
			numWareHouseItemID = @numWareHouseItemID 

		SET @Description=CONCAT('Work Order Created (Qty:',@numQty,')')

		--MAKE ENTRY IN WAREHOUSE TRACKING TABLE
		EXEC dbo.USP_ManageWareHouseItems_Tracking
		@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
		@numReferenceID = @numWOId, --  numeric(9, 0)
		@tintRefType = 2, --  tinyint
		@vcDescription = @Description, --  varchar(100)
		@numModifiedBy = @numUserCntID,
		@numDomainID = @numDomainID

		EXEC USP_ManageInventoryWorkOrder @numWOID,@numDomainID,@numUserCntID
	END
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_PickItems')
DROP PROCEDURE USP_WorkOrder_PickItems
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_PickItems]                             
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numWOID NUMERIC(18,0)
	,@vcPickItems VARCHAR(MAX)                   
AS                            
BEGIN
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	DECLARE @vcWorkOrder VARCHAR(100)
	DECLARE @vcItemName VARCHAR(300)
	DECLARE @numQtyToBuild FLOAT
	DECLARE @numBuildManager NUMERIC(18,0)
	DECLARE @numWarehouseID NUMERIC(18,0)
	DECLARE @numBuildPlan NUMERIC(18,0)

	DECLARE @numOldPickedQty FLOAT = 0
	DECLARE @numNewPickedQty FLOAT = 0

	IF EXISTS (SELECT numWOId FROM WorkOrder WHERE numDomainID=@numDomainID AND numWOId=@numWOID AND numWOStatus=23184)
	BEGIN
		RAISERROR('WORKORDER_COMPLETED',16,1)
		RETURN
	END
	
	SET @numOldPickedQty = ISNULL((SELECT
									MIN(numPickedQty)
								FROM
								(
									SELECT 
										WorkOrderDetails.numWODetailId
										,FLOOR(SUM(WorkOrderPickedItems.numPickedQty)/numQtyItemsReq_Orig) numPickedQty
									FROM
										WorkOrderDetails
									LEFT JOIN
										WorkOrderPickedItems
									ON
										WorkOrderDetails.numWODetailId=WorkOrderPickedItems.numWODetailId
									WHERE
										WorkOrderDetails.numWOId=@numWOID
										AND ISNULL(WorkOrderDetails.numWareHouseItemId,0) > 0
									GROUP BY
										WorkOrderDetails.numWODetailId
										,WorkOrderDetails.numQtyItemsReq_Orig
								) TEMP),0)

	SELECT
		@vcWorkOrder=ISNULL(vcWorkOrderName,'-')
		,@vcItemName=vcItemName
		,@numQtyToBuild=numQtyItemsReq
		,@numBuildManager=ISNULL(numAssignedTo,0)
		,@numWarehouseID = WareHouseItems.numWareHouseID
		,@numBuildPlan=ISNULL(numBuildPlan,0)
	FROM 
		WorkOrder 
	INNER JOIN
		Item
	ON
		WorkOrder.numItemCode = Item.numItemCode
	INNER JOIN 
		WareHouseItems 
	ON 
		WorkOrder.numWareHouseItemId = WareHouseItems.numWareHouseItemID 
	WHERE 
		WorkOrder.numDomainID=@numDomainID 
		AND WorkOrder.numWOId=@numWOID

	DECLARE @TableWOItems TABLE
	(
		ID INT IDENTITY(1,1)
		,numWOID NUMERIC(18,0)
		,numWODetailID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(18,0)
		,numQtyItemsReq FLOAT
		,numQtyItemsReq_Orig FLOAT
		,numQtyLeftToPick FLOAT
		,numPickedQty FLOAT
		,numFromWarehouseItemID NUMERIC(18,0)
		,bitSuccess BIT
		,vcErrorMessage VARCHAR(300)
	)

	DECLARE @numWODetailID NUMERIC(18,0)
	DECLARE @numItemCode NUMERIC(18,0)
	DECLARE @numWarehouseItemID NUMERIC(18,0)
	DECLARE @numFromWarehouseItemID NUMERIC(18,0)
	DECLARE @numQtyItemsReq FLOAT
	DECLARE @numQtyItemsReq_Orig FLOAT
	DECLARE @numQtyLeftToPicked FLOAT
	DECLARE @numQtyPicked FLOAT
	DECLARE @numOnHand AS FLOAT
	DECLARE @numOnOrder AS FLOAT
	DECLARE @numOnAllocation AS FLOAT
	DECLARE @numOnBackOrder AS FLOAT
	DECLARE @Description AS VARCHAR(1000)
	DECLARE @i INT = 1
	DECLARE @iCount INT

	IF LEN(ISNULL(@vcPickItems,'')) = 0
	BEGIN
		INSERT INTO @TableWOItems
		(
			numWOID
			,numWODetailID
			,numItemCode
			,numWareHouseItemId
			,numQtyItemsReq
			,numQtyItemsReq_Orig
			,numQtyLeftToPick
		)
		SELECT
			numWOId
			,numWODetailID
			,numChildItemID
			,numWareHouseItemId
			,numQtyItemsReq
			,numQtyItemsReq_Orig
			,numQtyItemsReq - ISNULL((SELECT SUM(numPickedQty) FROM WorkOrderPickedItems WHERE WorkOrderPickedItems.numWODetailId=WorkOrderDetails.numWODetailID),0)
		FROM
			WorkOrderDetails
		WHERE
			numWOId=@numWOID
			AND ISNULL(numWareHouseItemId,0) > 0

		SELECT @iCount = COUNT(*) FROM @TableWOItems

		BEGIN TRY
		BEGIN TRANSACTION

			WHILE @i <= @iCount
			BEGIN
				SELECT
					@numWODetailID=numWODetailID
					,@numItemCode=numItemCode
					,@numWarehouseItemID=numWarehouseItemID
					,@numQtyItemsReq=numQtyItemsReq
					,@numQtyItemsReq_Orig=numQtyItemsReq_Orig
					,@numQtyLeftToPicked=numQtyLeftToPick
				FROM
					@TableWOItems 
				WHERE 
					ID = @i

				--GET CHILD ITEM CURRENT INVENTORY DETAIL
				SELECT  
					@numOnHand = ISNULL(numOnHand, 0),
					@numOnAllocation = ISNULL(numAllocation, 0),
					@numOnOrder = ISNULL(numOnOrder, 0),
					@numOnBackOrder = ISNULL(numBackOrder, 0)
				FROM    
					WareHouseItems
				WHERE   
					numWareHouseItemID = @numWarehouseItemID	

				IF @numOnAllocation > 0 AND @numQtyLeftToPicked > 0
				BEGIN
					IF @numOnAllocation >= @numQtyLeftToPicked
					BEGIN
						SET @Description=CONCAT('Items Picked For Work Order (Qty:',@numQtyLeftToPicked,')')
						SET @numQtyPicked = @numQtyLeftToPicked
						SET @numOnAllocation = @numOnAllocation - @numQtyLeftToPicked
					END
					ELSE
					BEGIN
						SET @numQtyPicked = FLOOR(@numOnAllocation/@numQtyItemsReq_Orig) * @numQtyItemsReq_Orig
						SET @Description=CONCAT('Items Picked For Work Order (Qty:',@numQtyPicked,')')
						SET @numOnAllocation = @numOnAllocation - (FLOOR(@numOnAllocation/@numQtyItemsReq_Orig) * @numQtyItemsReq_Orig)
					END
				
					EXEC USP_UpdateInventoryAndTracking 
						@numOnHand=@numOnHand,
						@numOnAllocation=@numOnAllocation,
						@numOnBackOrder=@numOnBackOrder,
						@numOnOrder=@numOnOrder,
						@numWarehouseItemID=@numWarehouseItemID,
						@numReferenceID=@numWOId,
						@tintRefType=2,
						@numDomainID=@numDomainID,
						@numUserCntID=@numUserCntID,
						@Description=@Description

					INSERT INTO WorkOrderPickedItems
					(
						[numWODetailId]
						,[numPickedQty]
						,[numFromWarehouseItemID]
						,[numToWarehouseItemID]
						,[dtCreatedDate]
						,[numCreatedBy]
					)
					VALUES
					(
						@numWODetailID
						,@numQtyPicked
						,@numWarehouseItemID
						,@numWarehouseItemID
						,GETUTCDATE()
						,@numUserCntID
					)
				END

				SET @i = @i + 1
			END

		COMMIT
		END TRY
		BEGIN CATCH
			IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION;

			SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorNumber = ERROR_NUMBER(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorLine = ERROR_LINE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

			RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
		END CATCH
	END
	ELSE
	BEGIN
		DECLARE @hDocItem int
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcPickItems

		INSERT INTO @TableWOItems
		(
			numWOId
			,numWODetailID
			,numItemCode
			,numWareHouseItemId
			,numQtyItemsReq
			,numQtyItemsReq_Orig
			,numPickedQty
			,numFromWarehouseItemID
			,bitSuccess
			,vcErrorMessage
		)
		SELECT
			WorkOrderDetails.numWOId
			,TEMP.numWODetailID
			,TEMP.numItemCode
			,TEMP.numWareHouseItemId
			,WorkOrderDetails.numQtyItemsReq
			,WorkOrderDetails.numQtyItemsReq_Orig
			,TEMP.numPickedQty
			,TEMP.numFromWarehouseItemID
			,1
			,''
		FROM
		(
			SELECT
				numWODetailID
				,numItemCode
				,numWareHouseItemId
				,numPickedQty
				,numFromWarehouseItemID
			FROM
				OPENXML (@hDocItem,'/NewDataSet/Table1',2)
			WITH
			(
				numWODetailID NUMERIC(18,0)
				,numItemCode NUMERIC(18,0)
				,numWarehouseItemID NUMERIC(18,0)
				,numPickedQty FLOAT
				,numFromWarehouseItemID NUMERIC(18,0)
			)
		) TEMP
		INNER JOIN
			WorkOrderDetails 
		ON
			TEMP.numWODetailID = WorkOrderDetails.numWODetailId

		EXEC sp_xml_removedocument @hDocItem

		SELECT @iCount = COUNT(*) FROM @TableWOItems

		BEGIN TRY
		BEGIN TRANSACTION

			WHILE @i <= @iCount
			BEGIN
				SELECT
					@numWOID=numWOID
					,@numWODetailID=numWODetailID
					,@numItemCode=numItemCode
					,@numWarehouseItemID=numWarehouseItemID
					,@numQtyItemsReq=numQtyItemsReq
					,@numQtyItemsReq_Orig=numQtyItemsReq_Orig
					,@numFromWarehouseItemID=numFromWarehouseItemID
					,@numQtyPicked=numPickedQty
				FROM
					@TableWOItems 
				WHERE 
					ID = @i

				IF (@numQtyPicked + ISNULL((SELECT SUM(numPickedQty) FROM WorkOrderPickedItems WHERE WorkOrderPickedItems.numWODetailId=@numWODetailID),0)) > @numQtyItemsReq
				BEGIN
					UPDATE @TableWOItems SET bitSuccess=0,vcErrorMessage='Picked quantity can not be greater then quantity left to be picked.' WHERE ID=@i
				END
				ELSE
				BEGIN
					--GET CHILD ITEM CURRENT INVENTORY DETAIL
					SELECT  
						@numOnHand = ISNULL(numOnHand, 0),
						@numOnAllocation = ISNULL(numAllocation, 0),
						@numOnOrder = ISNULL(numOnOrder, 0),
						@numOnBackOrder = ISNULL(numBackOrder, 0)
					FROM    
						WareHouseItems
					WHERE   
						numWareHouseItemID = @numFromWarehouseItemID
				
					IF @numFromWarehouseItemID = @numWarehouseItemID
					BEGIN
						IF @numOnAllocation > 0 AND @numOnAllocation >= @numQtyPicked
						BEGIN
							SET @Description=CONCAT('Items Picked For Work Order (Qty:',@numQtyPicked,')')
							SET @numOnAllocation = @numOnAllocation - @numQtyPicked

							EXEC USP_UpdateInventoryAndTracking 
								@numOnHand=@numOnHand,
								@numOnAllocation=@numOnAllocation,
								@numOnBackOrder=@numOnBackOrder,
								@numOnOrder=@numOnOrder,
								@numWarehouseItemID=@numFromWarehouseItemID,
								@numReferenceID=@numWOId,
								@tintRefType=2,
								@numDomainID=@numDomainID,
								@numUserCntID=@numUserCntID,
								@Description=@Description

							INSERT INTO WorkOrderPickedItems
							(
								[numWODetailId]
								,[numPickedQty]
								,[numFromWarehouseItemID]
								,[numToWarehouseItemID]
								,[dtCreatedDate]
								,[numCreatedBy]
							)
							VALUES
							(
								@numWODetailID
								,@numQtyPicked
								,@numWarehouseItemID
								,@numWarehouseItemID
								,GETUTCDATE()
								,@numUserCntID
							)
						END
						ELSE
						BEGIN
							UPDATE @TableWOItems SET bitSuccess=0,vcErrorMessage='Picked quantity can not be greater then quantity on allocation.' WHERE ID=@i
						END
					END
					ELSE
					BEGIN
						IF (@numOnHand + @numOnAllocation) > 0 AND (@numOnHand + @numOnAllocation) >= @numQtyPicked
						BEGIN
							SET @Description=CONCAT('Items Transferred For Work Order (Qty:',@numQtyPicked,')')
							IF @numOnHand >= @numQtyPicked
							BEGIN
								SET @numOnHand = @numOnHand - @numQtyPicked
							END
							ELSE
							BEGIN
								SET @numOnBackOrder = @numOnBackOrder + (@numQtyPicked - @numOnHand)
								SET @numOnAllocation = @numOnAllocation - (@numQtyPicked - @numOnHand)
								SET @numOnHand = 0
							END
							

							EXEC USP_UpdateInventoryAndTracking 
								@numOnHand=@numOnHand,
								@numOnAllocation=@numOnAllocation,
								@numOnBackOrder=@numOnBackOrder,
								@numOnOrder=@numOnOrder,
								@numWarehouseItemID=@numFromWarehouseItemID,
								@numReferenceID=@numWOId,
								@tintRefType=2,
								@numDomainID=@numDomainID,
								@numUserCntID=@numUserCntID,
								@Description=@Description

							SELECT  
								@numOnHand = ISNULL(numOnHand, 0),
								@numOnAllocation = ISNULL(numAllocation, 0),
								@numOnOrder = ISNULL(numOnOrder, 0),
								@numOnBackOrder = ISNULL(numBackOrder, 0)
							FROM    
								WareHouseItems
							WHERE   
								numWareHouseItemID = @numWarehouseItemID

							IF @numOnBackOrder > 0
							BEGIN
								IF @numOnBackOrder > @numQtyPicked
								BEGIN
									SET @numOnBackOrder = @numOnBackOrder - @numQtyPicked
								END
								ELSE
								BEGIN
									SET @numOnBackOrder = 0
								END
							END

							SET @numOnAllocation = @numOnAllocation + @numQtyPicked
							SET @Description=CONCAT('Items Received For Work Order (Qty:',@numQtyPicked,')')

							EXEC USP_UpdateInventoryAndTracking 
								@numOnHand=@numOnHand,
								@numOnAllocation=@numOnAllocation,
								@numOnBackOrder=@numOnBackOrder,
								@numOnOrder=@numOnOrder,
								@numWarehouseItemID=@numWarehouseItemID,
								@numReferenceID=@numWOId,
								@tintRefType=2,
								@numDomainID=@numDomainID,
								@numUserCntID=@numUserCntID,
								@Description=@Description


							SET @numOnAllocation = @numOnAllocation - @numQtyPicked
							SET @Description=CONCAT('Items Picked For Work Order (Qty:',@numQtyPicked,')')

							EXEC USP_UpdateInventoryAndTracking 
								@numOnHand=@numOnHand,
								@numOnAllocation=@numOnAllocation,
								@numOnBackOrder=@numOnBackOrder,
								@numOnOrder=@numOnOrder,
								@numWarehouseItemID=@numWarehouseItemID,
								@numReferenceID=@numWOId,
								@tintRefType=2,
								@numDomainID=@numDomainID,
								@numUserCntID=@numUserCntID,
								@Description=@Description

							INSERT INTO WorkOrderPickedItems
							(
								[numWODetailId]
								,[numPickedQty]
								,[numFromWarehouseItemID]
								,[numToWarehouseItemID]
								,[dtCreatedDate]
								,[numCreatedBy]
							)
							VALUES
							(
								@numWODetailID
								,@numQtyPicked
								,@numFromWarehouseItemID
								,@numWarehouseItemID
								,GETUTCDATE()
								,@numUserCntID
							)
						END
						ELSE
						BEGIN
							UPDATE @TableWOItems SET bitSuccess=0,vcErrorMessage='Picked quantity can not be greater then available quantity.' WHERE ID=@i
						END
					END	
				END

				SET @i = @i + 1
			END

		COMMIT
		END TRY
		BEGIN CATCH
			IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION;

			SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorNumber = ERROR_NUMBER(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorLine = ERROR_LINE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

			RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
		END CATCH
	END

	--SET @numNewPickedQty = ISNULL((SELECT
	--								MIN(numPickedQty)
	--							FROM
	--							(
	--								SELECT 
	--									WorkOrderDetails.numWODetailId
	--									,FLOOR(SUM(WorkOrderPickedItems.numPickedQty)/numQtyItemsReq_Orig) numPickedQty
	--								FROM
	--									WorkOrderDetails
	--								LEFT JOIN
	--									WorkOrderPickedItems
	--								ON
	--									WorkOrderDetails.numWODetailId=WorkOrderPickedItems.numWODetailId
	--								WHERE
	--									WorkOrderDetails.numWOId=@numWOID
	--									AND ISNULL(WorkOrderDetails.numWareHouseItemId,0) > 0
	--								GROUP BY
	--									WorkOrderDetails.numWODetailId
	--									,WorkOrderDetails.numQtyItemsReq_Orig
	--							) TEMP),0)


	--IF (@numBuildPlan = 202 AND @numNewPickedQty = @numQtyToBuild) OR (@numBuildPlan = 201 AND @numNewPickedQty <> @numOldPickedQty)
	--BEGIN
	--	DECLARE @dtActionDate DATETIME = GETUTCDATE()
	--	DECLARE @vcDescription VARCHAR(300) = CONCAT('Work Order BOM Picked: ',@vcWorkOrder,' (',@vcItemName,')')

	--	IF @numBuildManager > 0
	--	BEGIN
	--		DECLARE @numDivisionId NUMERIC(18,0)
			
	--		SELECT @numDivisionId=numDivisionId FROM AdditionalContactsInformation WHERE numContactId=@numBuildManager

	--		EXEC dbo.usp_InsertCommunication
	--		@numCommId = 0, --  numeric(18, 0)
	--		@bitTask = 972, --  numeric(18, 0)
	--		@numContactId = @numUserCntID, --  numeric(18, 0)
	--		@numDivisionId = @numDivisionId, --  numeric(18, 0)
	--		@txtDetails = @vcDescription, --  text
	--		@numOppId = 0, --  numeric(18, 0)
	--		@numAssigned = @numBuildManager, --  numeric(18, 0)
	--		@numUserCntID = @numUserCntID, --  numeric(18, 0)
	--		@numDomainId = @numDomainID, --  numeric(18, 0)
	--		@bitClosed = 0, --  tinyint
	--		@vcCalendarName = '', --  varchar(100)
	--		@dtStartTime = @dtActionDate, --  datetime
	--		@dtEndtime =@dtActionDate, --  datetime
	--		@numActivity = 0, --  numeric(9, 0)
	--		@numStatus = 0, --  numeric(9, 0)
	--		@intSnoozeMins = 0, --  int
	--		@tintSnoozeStatus = 0, --  tinyint
	--		@intRemainderMins = 0, --  int
	--		@tintRemStaus = 0, --  tinyint
	--		@ClientTimeZoneOffset = 0, --  int
	--		@bitOutLook = 0, --  tinyint
	--		@bitSendEmailTemplate = 0, --  bit
	--		@bitAlert = 0, --  bit
	--		@numEmailTemplate = 0, --  numeric(9, 0)
	--		@tintHours = 0, --  tinyint
	--		@CaseID = 0, --  numeric(18, 0)
	--		@CaseTimeId = 0, --  numeric(18, 0)
	--		@CaseExpId = 0, --  numeric(18, 0)
	--		@ActivityId = 0, --  numeric(18, 0)
	--		@bitFollowUpAnyTime = 0, --  bit
	--		@strAttendee='',
	--		@numWOId=@numWOId,
	--		@numTaskID=0
	--	END

	--	IF EXISTS (SELECT numTaskId FROM StagePercentageDetailsTask WHERE numWorkOrderId=@numWOID AND ISNULL(numAssignTo,0) > 0)
	--	BEGIN
	--		DECLARE @TEMPTasks TABLE
	--		(
	--			ID INT IDENTITY(1,1)
	--			,numTaskID NUMERIC(18,0)
	--			,numAssignedTo NUMERIC
	--		)
	--		INSERT INTO @TEMPTasks
	--		(
	--			numTaskID
	--			,numAssignedTo
	--		)
	--		SELECT DISTINCT
	--			numTaskID
	--			,numAssignTo
	--		FROM
	--			StagePercentageDetailsTask 
	--		WHERE 
	--			numWorkOrderId=@numWOID 
	--			AND ISNULL(numAssignTo,0) > 0

	--		DECLARE @j INT = 1
	--		DECLARE @jCount INT
	--		DECLARE @numTaskID NUMERIC(18,0)
	--		SELECT @jCount=COUNT(*) FROM @TEMPTasks

	--		WHILE @j <= @jCount
	--		BEGIN
	--			SELECT @numBuildManager = numAssignedTo,@numTaskID=numTaskID FROM @TEMPTasks WHERE ID=@j

	--			SELECT @numDivisionId=numDivisionId FROM AdditionalContactsInformation WHERE numContactId=@numBuildManager

	--			EXEC dbo.usp_InsertCommunication
	--			@numCommId = 0, --  numeric(18, 0)
	--			@bitTask = 972, --  numeric(18, 0)
	--			@numContactId = @numUserCntID, --  numeric(18, 0)
	--			@numDivisionId = @numDivisionId, --  numeric(18, 0)
	--			@txtDetails = @vcDescription, --  text
	--			@numOppId = 0, --  numeric(18, 0)
	--			@numAssigned = @numBuildManager, --  numeric(18, 0)
	--			@numUserCntID = @numUserCntID, --  numeric(18, 0)
	--			@numDomainId = @numDomainID, --  numeric(18, 0)
	--			@bitClosed = 0, --  tinyint
	--			@vcCalendarName = '', --  varchar(100)
	--			@dtStartTime = @dtActionDate, --  datetime
	--			@dtEndtime =@dtActionDate, --  datetime
	--			@numActivity = 0, --  numeric(9, 0)
	--			@numStatus = 0, --  numeric(9, 0)
	--			@intSnoozeMins = 0, --  int
	--			@tintSnoozeStatus = 0, --  tinyint
	--			@intRemainderMins = 0, --  int
	--			@tintRemStaus = 0, --  tinyint
	--			@ClientTimeZoneOffset = 0, --  int
	--			@bitOutLook = 0, --  tinyint
	--			@bitSendEmailTemplate = 0, --  bit
	--			@bitAlert = 0, --  bit
	--			@numEmailTemplate = 0, --  numeric(9, 0)
	--			@tintHours = 0, --  tinyint
	--			@CaseID = 0, --  numeric(18, 0)
	--			@CaseTimeId = 0, --  numeric(18, 0)
	--			@CaseExpId = 0, --  numeric(18, 0)
	--			@ActivityId = 0, --  numeric(18, 0)
	--			@bitFollowUpAnyTime = 0, --  bit
	--			@strAttendee='',
	--			@numWOId=@numWOId,
	--			@numTaskID=@numTaskID

	--			SET @j = @j + 1
	--		END
	--	END

	--END


	SELECT * FROM @TableWOItems
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO

IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_PutAway')
DROP PROCEDURE USP_WorkOrder_PutAway
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_PutAway]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numWOID NUMERIC(18,0)
	,@numQtyReceived FLOAT
	,@vcSerialLot# VARCHAR(MAX)
	,@dtItemReceivedDate DATETIME
	,@vcWarehouses VARCHAR(MAX)
	,@tintMode TINYINT
)
AS
BEGIN
	IF ISNULL(@numQtyReceived,0) <= 0 
	BEGIN
		RAISERROR('QTY_TO_RECEIVE_NOT_PROVIDED',16,1)
		RETURN
	END 

	IF LEN(ISNULL(@vcWarehouses,'')) = 0
	BEGIN
		RAISERROR('LOCATION_NOT_SELECTED',16,1)
		RETURN
	END

	DECLARE @TEMPWarehouseLot TABLE
	(
		ID INT
		,numWareHouseItmsDTLID NUMERIC(18,0)
		,vcSerialNo VARCHAR(300)
		,numQty FLOAT
	)

	DECLARE @TEMPSerialLot TABLE
	(
		ID INT IDENTITY(1,1)
		,vcSerialNo VARCHAR(300)
		,numQty FLOAT
	)

	DECLARE @TEMPWarehouseLocations TABLE
	(
		ID INT IDENTITY(1,1)
		,numWarehouseItemID NUMERIC(18,0)
		,numReceivedQty FLOAT
	)

	INSERT INTO @TEMPWarehouseLocations
	(
		numWarehouseItemID
		,numReceivedQty
	)
	SELECT
		CAST(SUBSTRING(OutParam,0,CHARINDEX('(',OutParam)) AS NUMERIC) numWarehouseItemID
		,CAST(SUBSTRING(OutParam,CHARINDEX('(',OutParam) + 1,LEN(OutParam) - CHARINDEX('(',OutParam) - 1) AS VARCHAR) numReceivedQty
	FROM
		dbo.SplitString(@vcWarehouses,',')

	IF @numQtyReceived <> ISNULL((SELECT SUM(numReceivedQty) FROM @TEMPWarehouseLocations),0)
	BEGIN
		RAISERROR('INALID_QTY_RECEIVED',16,1)
		RETURN
	END

	DECLARE @numItemCode NUMERIC(18,0)
	DECLARE @numWarehouseItemID NUMERIC(18,0)
	DECLARE @numOverheadServiceItemID NUMERIC(18,0)
	DECLARE @monAverageCost AS DECIMAL(20,5)
	DECLARE @newAverageCost AS DECIMAL(20,5)
	DECLARE @monOverheadCost DECIMAL(20,5) 
	DECLARE @monLabourCost DECIMAL(20,5)
	DECLARE @CurrentAverageCost DECIMAL(20,5)
	DECLARE @TotalCurrentOnHand FLOAT
	DECLARE @bitLot BIT
	DECLARE @bitSerial BIT
	DECLARE @numOldQtyReceived FLOAT
	DECLARE @numUnits FLOAT
	DECLARE @description VARCHAR(300)

	IF EXISTS (SELECT numWOId FROM WorkOrder WHERE numDomainID=@numDomainID AND numWOId=@numWOID)
	BEGIN
		SELECT 
			@numItemCode=Item.numItemCode
			,@numWarehouseItemID=WareHouseItems.numWareHouseItemID
			,@bitLot=ISNULL(bitLotNo,0)
			,@bitSerial=ISNULL(bitSerialized,0)
			,@numUnits=ISNULL(numQtyItemsReq,0)
			,@numOldQtyReceived=ISNULL(numUnitHourReceived,0)
		FROM 
			WorkOrder 
		INNER JOIN
			Item
		ON
			WorkOrder.numItemCode = Item.numItemCode
		INNER JOIN
			WareHouseItems
		ON
			WareHouseItems.numWareHouseItemID = WorkOrder.numWareHouseItemId
		WHERE 
			WorkOrder.numDomainID=@numDomainID 
			AND WorkOrder.numWOId=@numWOID

		IF (ISNULL(@bitLot,0)=1 OR ISNULL(@bitSerial,0)=1)
		BEGIN
			IF LEN(ISNULL(@vcSerialLot#,'')) = 0
			BEGIN
				RAISERROR('SERIALLOT_REQUIRED',16,1)
				RETURN
			END 
			ELSE
			BEGIN	
				DECLARE @hDoc AS INT                                            
				EXEC sp_xml_preparedocument @hDoc OUTPUT, @vcSerialLot#

				INSERT INTO @TEMPSerialLot
				(
					vcSerialNo
					,numQty
				)
				SELECT 
					vcSerialNo
					,numQty
				FROM 
					OPENXML(@hDoc, '/NewDataSet/SerializedItems',2)
				WITH 
					(vcSerialNo VARCHAR(300), numQty FLOAT)

				EXEC sp_xml_removedocument @hDoc

				IF @numQtyReceived <> ISNULL((SELECT SUM(numQty) FROM @TEMPSerialLot),0)
				BEGIN
					RAISERROR('INALID_SERIALLOT',16,1)
					RETURN
				END
			END
		END

		IF (@numQtyReceived + @numOldQtyReceived) > @numUnits
		BEGIN
			RAISERROR('RECEIVED_QTY_MUST_BE_LESS_THEN_OR_EQUAL_TO_ORDERED_QTY',16,1)
			RETURN
		END

		SELECT @numOverheadServiceItemID=ISNULL(numOverheadServiceItemID,0) FROM Domain WHERE numDomainId=@numDomainID
	
		SELECT @CurrentAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE monAverageCost END) FROM dbo.Item WHERE numItemCode =@numItemCode 
		SELECT @TotalCurrentOnHand = ISNULL((SUM(numOnHand) + SUM(numAllocation)),0) FROM dbo.WareHouseItems WHERE numItemID =@numItemCode 
		PRINT @CurrentAverageCost
		PRINT @TotalCurrentOnHand

		-- Store hourly rate for all task assignee
		UPDATE 
			SPDT
		SET
			SPDT.monHourlyRate = ISNULL(UM.monHourlyRate,0)
		FROM
			StagePercentageDetailsTask SPDT
		INNER JOIN
			UserMaster UM
		ON
			SPDT.numAssignTo = UM.numUserDetailId
		WHERE
			SPDT.numDomainID = @numDomainID
			AND SPDT.numWorkOrderId = @numWOID

		SELECT 
			@newAverageCost = SUM(numQtyItemsReq * ISNULL(I.monAverageCost,0))
		FROM 
			WorkOrderDetails WOD
		LEFT JOIN 
			dbo.Item I 
		ON 
			I.numItemCode = WOD.numChildItemID
		WHERE 
			WOD.numWOId = @numWOId
			AND I.charItemType = 'P'

		IF @numOverheadServiceItemID > 0 AND EXISTS (SELECT numWODetailId FROM WorkOrderDetails WHERE numWOId=@numWOId AND numChildItemID=@numOverheadServiceItemID)
		BEGIN
			SET @monOverheadCost = (SELECT SUM(numQtyItemsReq * ISNULL(Item.monListPrice,0)) FROM WorkOrderDetails WOD INNER JOIN Item ON WOD.numChildItemID=Item.numItemCode WHERE WOD.numWOId=@numWOId AND WOD.numChildItemID=@numOverheadServiceItemID)
		END

		SET @monLabourCost = dbo.GetWorkOrderLabourCost(@numDomainID,@numWOId)			

		UPDATE WorkOrder SET monAverageCost=@newAverageCost,monLabourCost=ISNULL(@monLabourCost,0),monOverheadCost=ISNULL(@monOverheadCost,0) WHERE numWOId=@numWOId

		SET @newAverageCost = ((@CurrentAverageCost * @TotalCurrentOnHand)  + (@newAverageCost + ISNULL(@monOverheadCost,0) + ISNULL(@monLabourCost,0))) / (@TotalCurrentOnHand + @numQtyReceived)

		UPDATE
			WareHouseItems
		SET 
			numOnOrder = ISNULL(numOnOrder,0) - @numQtyReceived,
			dtModified = GETDATE()
		WHERE
			numWareHouseItemID=@numWareHouseItemID
		
		SET @description = CONCAT('Work Order Received (Qty:',@numQtyReceived,')')

		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numWOID, --  numeric(9, 0)
			@tintRefType = 2, --  tinyint
			@vcDescription = @description,
			@numModifiedBy = @numUserCntID,
			@dtRecordDate =  @dtItemReceivedDate,
			@numDomainID = @numDomainID

		DECLARE @i INT
		DECLARE @j INT
		DECLARE @k INT
		DECLARE @iCount INT = 0
		DECLARE @jCount INT = 0
		DECLARE @kCount INT = 0
		DECLARE @numWareHouseItmsDTLID NUMERIC(18,0)
		DECLARE @vcSerailLotNumber VARCHAR(300)
		DECLARE @numSerialLotQty FLOAT
		DECLARE @numTempWarehouseItemID NUMERIC(18,0)
		DECLARE @numTempReceivedQty FLOAT
		DECLARE @numLotWareHouseItmsDTLID NUMERIC(18,0)
		DECLARE @numLotQty FLOAT
	
		SET @i = 1
		SET @iCount = (SELECT COUNT(*) FROM @TEMPWarehouseLocations)

		WHILE @i <= @iCount
		BEGIN
			SELECT 
				@numTempWarehouseItemID=ISNULL(numWarehouseItemID,0)
				,@numTempReceivedQty=ISNULL(numReceivedQty,0)
			FROM 
				@TEMPWarehouseLocations 
			WHERE 
				ID = @i

			IF NOT EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode AND numWareHouseItemID=@numTempWarehouseItemID)
			BEGIN
				RAISERROR('INVALID_WAREHOUSE',16,1)
				RETURN
			END
			ELSE
			BEGIN
				-- INCREASE THE OnHand Of Destination Location
				UPDATE
					WareHouseItems
				SET
					numBackOrder = (CASE WHEN numBackOrder > @numTempReceivedQty THEN ISNULL(numBackOrder,0) - @numTempReceivedQty ELSE 0 END),         
					numAllocation = (CASE WHEN numBackOrder > @numTempReceivedQty THEN ISNULL(numAllocation,0) + @numTempReceivedQty ELSE ISNULL(numAllocation,0) + ISNULL(numBackOrder,0) END),
					numOnHand = (CASE WHEN numBackOrder > @numTempReceivedQty THEN numOnHand ELSE ISNULL(numOnHand,0) + (@numTempReceivedQty - ISNULL(numBackOrder,0)) END),
					dtModified = GETDATE()
				WHERE
					numWareHouseItemID=@numTempWarehouseItemID

				SET @description = CONCAT('Work Order Qty Put-Away (Qty:',@numTempReceivedQty,')')

				EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numTempWarehouseItemID, --  numeric(9, 0)
					@numReferenceID = @numWOID, --  numeric(9, 0)
					@tintRefType = 2, --  tinyint
					@vcDescription = @description,
					@numModifiedBy = @numUserCntID,
					@dtRecordDate =  @dtItemReceivedDate,
					@numDomainID = @numDomainID

				IF (ISNULL(@bitLot,0)=1 OR ISNULL(@bitSerial,0)=1)
				BEGIN
					SET @j = 1
					SET @jCount = (SELECT COUNT(*) FROM @TEMPSerialLot)

					WHILE (@j <= @jCount AND @numTempReceivedQty > 0)
					BEGIN
						SELECT 
							@vcSerailLotNumber=ISNULL(vcSerialNo,'')
							,@numSerialLotQty=ISNULL(numQty,0)
						FROM 
							@TEMPSerialLot 
						WHERE 
							ID=@j

						IF @numSerialLotQty > 0
						BEGIN
							IF @bitSerial = 1 AND (SELECT COUNT(*) FROM WareHouseItems INNER JOIN WareHouseItmsDTL ON WareHouseItems.numWareHouseItemID = WareHouseItmsDTL.numWareHouseItemID WHERE WareHouseItems.numItemID=@numItemCode AND LOWER(vcSerialNo)=LOWER(@vcSerailLotNumber)) > 0
							BEGIN
								RAISERROR('DUPLICATE_SERIAL_NO',16,1)
								RETURN
							END

							INSERT INTO WareHouseItmsDTL
							(
								numWareHouseItemID
								,vcSerialNo
								,numQty
								,bitAddedFromPO
							)  
							VALUES
							( 
								@numTempWarehouseItemID
								,@vcSerailLotNumber
								,(CASE WHEN @numTempReceivedQty >= @numSerialLotQty THEN @numSerialLotQty ELSE @numTempReceivedQty END)
								,1 
							)

							SET @numWareHouseItmsDTLID=SCOPE_IDENTITY()
		
							INSERT INTO OppWarehouseSerializedItem
							(
								numWarehouseItmsDTLID
								,numWOID
								,numWarehouseItmsID
								,numQty
							)  
							VALUES
							( 
								@numWareHouseItmsDTLID
								,@numWOID
								,@numTempWarehouseItemID
								,(CASE WHEN @numTempReceivedQty >= @numSerialLotQty THEN @numSerialLotQty ELSE @numTempReceivedQty END)
							)

							IF @numTempReceivedQty >= @numSerialLotQty
							BEGIN
								SET @numTempReceivedQty = @numTempReceivedQty - @numSerialLotQty
								UPDATE @TEMPSerialLot SET numQty = 0  WHERE ID = @j
							END
							ELSE
							BEGIN
								UPDATE @TEMPSerialLot SET numQty = numQty - @numTempReceivedQty  WHERE ID = @j
								SET @numTempReceivedQty = 0
							END
						END

						SET @j = @j + 1
					END

					IF @numTempReceivedQty > 0
					BEGIN
						RAISERROR('INALID_SERIALLOT',16,1)
						RETURN
					END
				END
			END


			SET @i = @i + 1
		END

		UPDATE  
			WorkOrder
		SET 
			numUnitHourReceived = ISNULL(numUnitHourReceived,0) + ISNULL(@numQtyReceived,0)
			,numQtyReceived = (CASE WHEN ISNULL(@tintMode,0)=1 THEN ISNULL(numQtyReceived,0) + ISNULL(@numQtyReceived,0) ELSE ISNULL(numQtyReceived,0) END)
		WHERE
			numDomainID = @numDomainID
			AND numWOID = @numWOID
	END
	ELSE
	BEGIN
		RAISERROR('WORKORDER_DOES_NOT_EXISTS',16,1)
	END

	
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_UpdateQtyBuilt')
DROP PROCEDURE USP_WorkOrder_UpdateQtyBuilt
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_UpdateQtyBuilt]  
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numWOID NUMERIC(18,0)
AS
BEGIN
	-- CODE LEVEL TRANSACTION IS USED

	DECLARE @numQtyBuilt FLOAT
	DECLARE @numQtyItemsReq FLOAT 
	DECLARE @numProcessedQty FLOAT = 0
	DECLARE @numWareHouseItemID NUMERIC(18,0)
	
	SELECT 
		@numQtyItemsReq = ISNULL(numQtyItemsReq,0)
		,@numQtyBuilt = ISNULL(numQtyBuilt,0)
		,@numWareHouseItemID=numWareHouseItemId
	FROM 
		WorkOrder 
	WHERE 
		WorkOrder.numDomainID = @numDomainID 
		AND WorkOrder.numWOId = @numWOID

	SET @numProcessedQty = ISNULL((SELECT
										MIN(numQtyBuilt) numQtyBuilt
									FROM
									(SELECT	
										numTaskId
										,(CASE 
											WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction = 4) 
											THEN ISNULL(WorkOrder.numQtyItemsReq,0)
											ELSE ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId),0)
										END) numQtyBuilt
									FROM
										StagePercentageDetailsTask SPDT
									INNER JOIN
										WorkOrder
									ON
										SPDT.numWorkOrderId = WorkOrder.numWOId
									WHERE
										numWorkOrderId=@numWOID) TEMP
									),0)

	SELECT
		Item.numItemCode
		,ISNULL(Item.numAssetChartAcntId,0) numAssetChartAcntId
		,ISNULL(Item.monAverageCost,0) monAverageCost
		,(CASE 
			WHEN ISNULL(@numProcessedQty,0) > @numQtyBuilt AND ISNULL(@numProcessedQty,0) <= @numQtyItemsReq  THEN ISNULL(@numProcessedQty,0) - @numQtyBuilt
			ELSE 0 
		END) numQtyBuilt
	FROM
		WorkOrder
	INNER JOIN
		Item
	ON
		WorkOrder.numItemCode = Item.numItemCode
	WHERE
		WorkOrder.numDomainID = @numDomainID
		AND WorkOrder.numWOId = @numWOID

	UPDATE
		WorkOrder
	SET 
		numQtyBuilt = ISNULL(numQtyBuilt,0) + (CASE 
													WHEN ISNULL(@numProcessedQty,0) > @numQtyBuilt AND ISNULL(@numProcessedQty,0) <= @numQtyItemsReq THEN (ISNULL(@numProcessedQty,0) - @numQtyBuilt)
													ELSE 0 
												END)
	WHERE
		WorkOrder.numDomainID = @numDomainID
		AND WorkOrder.numWOId = @numWOID

	-- COMMENTED BECAUSE WORK ORDER BUILD COMPLETION IS NOW MANAGED THROUGH PURCHASE FULFILLMENT
	--IF (CASE 
	--		WHEN ISNULL(@numProcessedQty,0) > @numQtyItemsReq THEN 0
	--		WHEN ISNULL(@numProcessedQty,0) > @numQtyBuilt THEN ISNULL(@numProcessedQty,0) - @numQtyBuilt
	--		ELSE 0 
	--	END) > 0
	--BEGIN
	--	SET @numQtyItemsReq = (CASE 
	--								WHEN ISNULL(@numProcessedQty,0) > @numQtyItemsReq THEN 0
	--								WHEN ISNULL(@numProcessedQty,0) > @numQtyBuilt THEN ISNULL(@numProcessedQty,0) - @numQtyBuilt
	--								ELSE 0 
	--							END) 
	--	EXEC USP_UpdatingInventory 0,@numWareHouseItemID,@numQtyItemsReq,@numWOId,@numUserCntID,0
	--END
END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ApprovePORequistion' ) 
    DROP PROCEDURE USP_ApprovePORequistion
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 14 July 2014
-- Description:	sets unit price approval required flag to false for all items added in order
-- =============================================
CREATE PROCEDURE USP_ApprovePORequistion
	@numOppId numeric(18,0),
	@numDomainID numeric(18,0),
	@ApprovalType AS INT = 0
AS
BEGIN
	UPDATE
		OpportunityMaster
	SET
		intReqPOApproved = @ApprovalType
	WHERE
		numOppId = @numOppId
	
END
GO
/****** Object:  StoredProcedure [dbo].[USP_ConEmpListBasedonTeam]    Script Date: 07/26/2008 16:15:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By Anoop Jayaraj          
-- exec USP_ConEmpListBasedonTeam @numDomainID=72,@numUserCntID=17
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ConEmpListByGroupId')
DROP PROCEDURE USP_ConEmpListByGroupId
GO
CREATE PROCEDURE [dbo].[USP_ConEmpListByGroupId]
    @numDomainID AS NUMERIC(9) = 0,
    @numGroupId AS NUMERIC(9) = 0
AS 
BEGIN
	 SELECT A.numContactID,A.vcFirstName+' '+A.vcLastName as vcUserName          
 from UserMaster UM         
 join AdditionalContactsInformation A        
 on UM.numUserDetailId=A.numContactID          
 where UM.numDomainID=@numDomainID and UM.numDomainID=A.numDomainID and UM.intAssociate=1  AND UM.numGroupID=@numGroupId

END

GO      
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetApprovalForTickler')
DROP PROCEDURE USP_GetApprovalForTickler
GO
CREATE PROCEDURE [dbo].[USP_GetApprovalForTickler]
    @numDomainID AS NUMERIC(9) = 0,
	@startDate datetime =null,
	@endDate datetime=null,
    @numUserCntID AS NUMERIC(9) = 0,
    @ClientTimeZoneOffset AS INT=0
AS 
BEGIN
 IF(@startDate<>'')
 BEGIN
 SELECT  CAST(numCategoryHDRID AS varchar) as RecordId,
                    CASE WHEN numCategory = 1 AND numType=1 THEN 'Billable Time'
						 WHEN numCategory = 1 AND numType=2 THEN 'Non-Billable Time'
                         WHEN numCategory = 2 AND ISNULL(bitReimburse,0) = 1 AND numType = 2  THEN 'Reimbursable Expense'
                         WHEN numCategory = 2 AND ISNULL(bitReimburse,0) = 1 AND numType = 6 THEN 'Billable + Reimbursable Expense'
                         WHEN numCategory = 2 AND ISNULL(bitReimburse,0) = 0 AND numType = 1 THEN 'Billable Expense'
                    END AS [ApprovalRequests], ISNULL(ACIC.vcFirstName,'') + ' ' + ISNULL(ACIC.vcLastName,'') +','+ CONVERT(VARCHAR,(DateAdd(minute, -@ClientTimeZoneOffset, TE.dtTCreatedOn))) as CreatedDate,
					CASE WHEN TE.numApprovalComplete=-1 THEN 'Declined' ELSE 'Pending Approval' END as [status],TE.numApprovalComplete AS ApprovalStatus,
					TE.txtDesc as Notes,
					 OM.vcPOppName as vcPOppName,
					 CASE WHEN I.vcItemName='' THEN '' ELSE '('+I.vcItemName+')' END as vcItemName,
					 ISNULL(ACI.vcFirstName,'') + ' ' + ISNULL(ACI.vcLastName,'') AS [vcEmployee],
					 Case WHEN numCategory = 1 AND numType=1 AND OM.numOppId>0 
						THEN ((ISNULL(CAST(DATEDIFF(MINUTE,TE.dtfromdate,TE.dttodate) AS FLOAT)/CAST(60 AS FLOAT),0))*TE.monAmount)
					 WHEN  OM.numOppId>0 
						THEN TE.monAmount 
					ELSE '0'
						END [ClientCharge],
					Case WHEN numCategory = 1 AND (numType=1 or numType=2)
						THEN ((ISNULL(CAST(DATEDIFF(MINUTE,TE.dtfromdate,TE.dttodate) AS FLOAT)/CAST(60 AS FLOAT),0))*
						(select ISNULL(monHourlyRate,0) from UserMaster where numUserDetailId=TE.numUserCntID    
						and UserMaster.numDomainID=@numDomainID))
					 WHEN numCategory = 2
						THEN TE.monAmount 
					ELSE  0
						END [EmployeeCharge],
					ISNULL(OM.numOppId,0) AS numOppId,
					'1' AS TypeApproval
			FROM    TimeAndExpense TE
            LEFT JOIN dbo.OpportunityMaster OM ON om.numoppId = TE.numOppId AND TE.numDomainID = OM.numDomainID AND OM.tintOppType = 1
			LEFT JOIN Item as I ON TE.numServiceItemID=I.numItemCode
            LEFT JOIN dbo.ProjectsMaster PM ON PM.numProId = TE.numProID AND TE.numDomainID = PM.numDomainID
            LEFT JOIN dbo.Chart_Of_Accounts CA ON CA.numAccountId = TE.numExpId AND TE.numDomainID = CA.numDomainID
            LEFT JOIN AdditionalContactsInformation ACI ON ACI.numContactID = TE.numUserCntID
            LEFT JOIN AdditionalContactsInformation ACIC ON ACIC.numContactID = TE.numTCreatedBy
			LEFT JOIN ApprovalConfig AS A on TE.numUserCntID=A.numUserId AND A.numDomainID=@numDomainId
            WHERE  A.numModule=1 AND (A.numLevel1Authority=@numUserCntID OR A.numLevel2Authority=@numUserCntID OR A.numLevel3Authority=@numUserCntID
			OR A.numLevel4Authority=@numUserCntID OR A.numLevel5Authority=@numUserCntID) AND TE.numApprovalComplete NOT IN(0)
			AND dateadd(minute,-@ClientTimeZoneOffset,TE.dtTCreatedOn)>= @startDate AND dateadd(minute,-@ClientTimeZoneOffset,TE.dtTCreatedOn)<=@endDate 
			
	UNION ALL
		SELECT M.numOppId as RecordId,'Price Margin Approval' AS [ApprovalRequests],
				 ISNULL(ACIC.vcFirstName,'') + ' ' + ISNULL(ACIC.vcLastName,'') +','+CONVERT(VARCHAR,(DateAdd(minute, -@ClientTimeZoneOffset, M.bintCreatedDate))) as CreatedDate
				,'Pending Approval' as [status],0 as ApprovalStatus,''  as Notes,M.vcPOppName,
				'('+(SELECT SUBSTRING((SELECT ',' + CAST(OpportunityItems.vcItemName AS VARCHAR) FROM OpportunityItems 
				WHERE OpportunityItems.bitItemPriceApprovalRequired=1 AND OpportunityItems.numOppId=M.numOppId FOR XML PATH('')), 2,10000) )+')'
				 AS vcItemName,  
				 '' AS [vcEmployee],0 AS  [ClientCharge],0 AS [EmployeeCharge],M.numOppId
				 ,'2' AS TypeApproval
				 FROM  OpportunityMaster AS M
					LEFT JOIN AdditionalContactsInformation ACIC ON ACIC.numContactID = M.numCreatedBy
					WHERE M.numDomainId=@numDomainID
					AND (SELECT COUNT(OpportunityItems.numoppitemtCode) FROM OpportunityItems 
						WHERE OpportunityItems.bitItemPriceApprovalRequired=1 AND OpportunityItems.numOppId=M.numOppId) >0
					AND @numUserCntID IN (SELECT numUserID FROM UnitPriceApprover WHERE numDomainId=@numDomainID)
					AND dateadd(minute,-@ClientTimeZoneOffset,M.bintCreatedDate)>= @startDate AND dateadd(minute,-@ClientTimeZoneOffset,M.bintCreatedDate)<=@endDate 
					AND M.tintOppType = 1
	UNION ALL
		SELECT M.numOppId as RecordId,'Promotion Approval' AS [ApprovalRequests],
				 ISNULL(ACIC.vcFirstName,'') + ' ' + ISNULL(ACIC.vcLastName,'') +','+CONVERT(VARCHAR,(DateAdd(minute, -@ClientTimeZoneOffset, M.bintCreatedDate))) as CreatedDate
				,'Pending Approval' as [status],M.intPromotionApprovalStatus as ApprovalStatus,''  as Notes,M.vcPOppName,
				'('+(SELECT SUBSTRING((SELECT ',' + CAST(OpportunityItems.vcItemName AS VARCHAR) FROM OpportunityItems 
				WHERE OpportunityItems.bitItemPriceApprovalRequired=1 AND OpportunityItems.numOppId=M.numOppId FOR XML PATH('')), 2,10000) )+')'
				 AS vcItemName,  
				 '' AS [vcEmployee],0 AS  [ClientCharge],0 AS [EmployeeCharge],M.numOppId
				 ,'3' AS TypeApproval
				 FROM  OpportunityMaster AS M
					LEFT JOIN AdditionalContactsInformation ACIC ON ACIC.numContactID = M.numCreatedBy
					LEFT JOIN ApprovalConfig AS A on M.numCreatedBy=A.numUserId WHERE  A.numModule=2 AND
					M.intPromotionApprovalStatus NOT IN(0,-1) AND A.numDomainID=@numDomainId
					AND (A.numLevel1Authority=@numUserCntID OR A.numLevel2Authority=@numUserCntID OR A.numLevel3Authority=@numUserCntID
					OR A.numLevel4Authority=@numUserCntID OR A.numLevel5Authority=@numUserCntID)
					AND dateadd(minute,-@ClientTimeZoneOffset,M.bintCreatedDate)>= @startDate AND dateadd(minute,-@ClientTimeZoneOffset,M.bintCreatedDate)<=@endDate 
					AND M.tintOppType = 1
END
ELSE
BEGIN
	SELECT  CAST(numCategoryHDRID AS varchar) as RecordId,
                    CASE WHEN numCategory = 1 AND numType=1 THEN 'Billable Time'
						 WHEN numCategory = 1 AND numType=2 THEN 'Non-Billable Time'
                         WHEN numCategory = 2 AND ISNULL(bitReimburse,0) = 1 AND numType = 2  THEN 'Reimbursable Expense'
                         WHEN numCategory = 2 AND ISNULL(bitReimburse,0) = 1 AND numType = 6 THEN 'Billable + Reimbursable Expense'
                         WHEN numCategory = 2 AND ISNULL(bitReimburse,0) = 0 AND numType = 1 THEN 'Billable Expense'
                    END AS [ApprovalRequests], ISNULL(ACIC.vcFirstName,'') + ' ' + ISNULL(ACIC.vcLastName,'') +','+ CONVERT(VARCHAR,(DateAdd(minute, -@ClientTimeZoneOffset, TE.dtTCreatedOn))) as CreatedDate,
					CASE WHEN TE.numApprovalComplete=-1 THEN 'Declined' ELSE 'Pending Approval' END as [status],TE.numApprovalComplete AS ApprovalStatus,
					TE.txtDesc as Notes,
					 OM.vcPOppName as vcPOppName,
					 CASE WHEN I.vcItemName='' THEN '' ELSE '('+I.vcItemName+')' END as vcItemName,
					 ISNULL(ACI.vcFirstName,'') + ' ' + ISNULL(ACI.vcLastName,'') AS [vcEmployee],
					 Case WHEN numCategory = 1 AND numType=1 AND OM.numOppId>0 
						THEN ((ISNULL(CAST(DATEDIFF(MINUTE,TE.dtfromdate,TE.dttodate) AS FLOAT)/CAST(60 AS FLOAT),0))*TE.monAmount)
					 WHEN  OM.numOppId>0 
						THEN TE.monAmount 
					ELSE '0'
						END [ClientCharge],
					Case WHEN numCategory = 1 AND (numType=1 or numType=2)
						THEN ((ISNULL(CAST(DATEDIFF(MINUTE,TE.dtfromdate,TE.dttodate) AS FLOAT)/CAST(60 AS FLOAT),0))*
						(select ISNULL(monHourlyRate,0) from UserMaster where numUserDetailId=TE.numUserCntID    
						and UserMaster.numDomainID=@numDomainID))
					 WHEN numCategory = 2
						THEN TE.monAmount 
					ELSE  0
						END [EmployeeCharge],
					ISNULL(OM.numOppId,0) AS numOppId
			FROM    TimeAndExpense TE
            LEFT JOIN dbo.OpportunityMaster OM ON om.numoppId = TE.numOppId AND TE.numDomainID = OM.numDomainID  AND OM.tintOppType = 1
			LEFT JOIN Item as I ON TE.numServiceItemID=I.numItemCode
            LEFT JOIN dbo.ProjectsMaster PM ON PM.numProId = TE.numProID AND TE.numDomainID = PM.numDomainID
            LEFT JOIN dbo.Chart_Of_Accounts CA ON CA.numAccountId = TE.numExpId AND TE.numDomainID = CA.numDomainID
            LEFT JOIN AdditionalContactsInformation ACI ON ACI.numContactID = TE.numUserCntID
            LEFT JOIN AdditionalContactsInformation ACIC ON ACIC.numContactID = TE.numTCreatedBy
			LEFT JOIN ApprovalConfig AS A on TE.numUserCntID=A.numUserId AND A.numDomainID=@numDomainId AND A.numModule=1
            WHERE 
				TE.numDomainID = @numDomainID 
				AND @numUserCntID IN (A.numLevel1Authority,A.numLevel2Authority,A.numLevel3Authority,A.numLevel4Authority,A.numLevel5Authority) 
				AND TE.numApprovalComplete NOT IN (0)
END
END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetWarehousesForSelectedItem]    Script Date: 04-05-2018 10:59:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWarehousesForSelectedItem')
DROP PROCEDURE USP_GetWarehousesForSelectedItem
GO

CREATE PROCEDURE [dbo].[USP_GetWarehousesForSelectedItem]    
(
	@numItemCode AS VARCHAR(20)=''  ,
	@numWareHouseID AS NUMERIC(18,0) = 0
)
AS
BEGIN  
	DECLARE @numDomainID AS NUMERIC(18,0)
	DECLARE @numBaseUnit AS NUMERIC(18,0)
	DECLARE @vcUnitName as VARCHAR(100) 
	DECLARE @numSaleUnit AS NUMERIC(18,0)
	DECLARE @vcSaleUnitName as VARCHAR(100) 
	DECLARE @numPurchaseUnit AS NUMERIC(18,0)
	DECLARE @vcPurchaseUnitName as VARCHAR(100) 
	declare @bitSerialize as bit     
	DECLARE @bitKitParent BIT
	DECLARE @bitAssembly BIT
 
	SELECT 
		@numDomainID = numDomainID,
		@numBaseUnit = ISNULL(numBaseUnit,0),
		@numSaleUnit = ISNULL(numSaleUnit,0),
		@numPurchaseUnit = ISNULL(numPurchaseUnit,0),
		@bitSerialize=bitSerialized,
		@bitAssembly=ISNULL(bitAssembly,0),
		@bitKitParent = ( CASE WHEN ISNULL(bitKitParent,0) = 1
								   AND ISNULL(bitAssembly,0) = 1 THEN 0
							  WHEN ISNULL(bitKitParent,0) = 1 THEN 1
							  ELSE 0
						 END ) 
	FROM 
		Item 
	WHERE 
		numItemCode=@numItemCode    


	SELECT @vcUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numBaseUnit
	SELECT @vcSaleUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numSaleUnit
	SELECT @vcPurchaseUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numPurchaseUnit

	DECLARE @numSaleUOMFactor AS DECIMAL(28,14)
	DECLARE @numPurchaseUOMFactor AS DECIMAL(28,14)

	SELECT @numSaleUOMFactor = dbo.fn_UOMConversion(@numSaleUnit,@numItemCode,@numDOmainID,@numBaseUnit)
	SELECT @numPurchaseUOMFactor = dbo.fn_UOMConversion(@numPurchaseUnit,@numItemCode,@numDOmainID,@numBaseUnit)

	SELECT 
		(WareHouseItems.numWareHouseItemId),
		ISNULL(WareHouseItems.numWLocationID,0) numWLocationID,
		ISNULL(vcWareHouse,'') AS vcWareHouse, 
		dbo.fn_GetAttributes(WareHouseItems.numWareHouseItemId,@bitSerialize) as Attr, 
		WareHouseItems.monWListPrice,
		CASE 
			WHEN @bitKitParent=1 
			THEN dbo.fn_GetKitInventory(@numItemCode,WareHouseItems.numWareHouseID) 
			ELSE ISNULL((SELECT SUM(WI.numOnHand) FROM WareHouseItems WI INNER JOIN WarehouseLocation WL ON WI.numWLocationID = WL.numWLocationID WHERE WI.numItemID=WareHouseItems.numItemID AND WI.numWarehouseID=WareHouseItems.numWarehouseID),0) + ISNULL(numOnHand,0) 
		END numOnHand,
		CASE WHEN @bitKitParent=1 THEN 0 ELSE ISNULL(numOnOrder,0) END numOnOrder,
		CASE WHEN @bitKitParent=1 THEN 0 ELSE ISNULL(numReorder,0) END numReorder,
		CASE WHEN @bitKitParent=1 THEN 0 ELSE ISNULL(numAllocation,0) END numAllocation,
		CASE WHEN @bitKitParent=1 THEN 0 ELSE ISNULL(numBackOrder,0) END numBackOrder,
		Warehouses.numWareHouseID,
		@vcUnitName AS vcUnitName,
		@vcSaleUnitName AS vcSaleUnitName,
		@vcPurchaseUnitName AS vcPurchaseUnitName,
		CASE WHEN @numBaseUnit > 0 AND @numSaleUnit > 0
		THEN
			CASE WHEN @bitKitParent=1 THEN CAST(CAST(dbo.fn_GetKitInventory(@numItemCode,WareHouseItems.numWareHouseID)/@numSaleUOMFactor AS INT) AS VARCHAR) else CAST(CAST(ISNULL((SELECT SUM(WI.numOnHand) FROM WareHouseItems WI INNER JOIN WarehouseLocation WL ON WI.numWLocationID = WL.numWLocationID WHERE WI.numItemID=WareHouseItems.numItemID AND WI.numWarehouseID=WareHouseItems.numWarehouseID),0) + ISNULL(numOnHand,0) /@numSaleUOMFactor AS INT) AS VARCHAR) END
		ELSE
			'-'
		END AS numOnHandUOM,
		CASE WHEN @numBaseUnit > 0 AND @numSaleUnit > 0
		THEN
			CASE WHEN @bitKitParent=1 THEN '0' else CAST(CAST(ISNULL(numOnOrder,0)/@numPurchaseUOMFactor AS INT) AS VARCHAR) END
		ELSE
			'-'
		END AS numOnOrderUOM,
		CASE WHEN @numBaseUnit > 0 AND @numSaleUnit > 0
		THEN
			CASE WHEN @bitKitParent=1 THEN '0' else CAST(CAST(ISNULL(numReorder,0)/@numSaleUOMFactor AS INT) AS VARCHAR) END
		ELSE
			'-'
		END AS numReorderUOM,
		CASE WHEN @numBaseUnit > 0 AND @numSaleUnit > 0
		THEN
			CASE WHEN @bitKitParent=1 THEN '0' else CAST(CAST(ISNULL(numAllocation,0)/@numSaleUOMFactor AS INT) AS VARCHAR) END
		ELSE
			'-'
		END AS numAllocationUOM,
		CASE WHEN @numBaseUnit > 0 AND @numSaleUnit > 0
		THEN
			CASE WHEN @bitKitParent=1 THEN '0' else CAST(CAST(ISNULL(numBackOrder,0)/@numSaleUOMFactor AS INT) AS VARCHAR) END
		ELSE
			'-'
		END AS numBackOrderUOM
	FROM 
		WareHouseItems    
	JOIN 
		Warehouses 
	ON 
		Warehouses.numWareHouseID=WareHouseItems.numWareHouseID  
	WHERE 
		numItemID=@numItemCode
		AND ISNULL(WareHouseItems.numWLocationID,0) = 0
		AND (ISNULL(@numWareHouseID,0) = 0 OR WareHouseItems.numWareHouseID=@numWareHouseID)
	ORDER BY
		Warehouses.vcWarehouse
END
GO



GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ItemUnitPriceApprovalUpdateConfigurePrice')
DROP PROCEDURE USP_ItemUnitPriceApprovalUpdateConfigurePrice
GO
CREATE PROCEDURE USP_ItemUnitPriceApprovalUpdateConfigurePrice (
@numOppId NUMERIC(18,0),
@numDomainID NUMERIC(18,0)
)
AS
BEGIN
DECLARE @returnFormattedItems VARCHAR(MAX)=''
SET @returnFormattedItems=''
DECLARE  @tempTableData TABLE
(
	ID INT IDENTITY(1, 1) primary key ,
	numItemCode NUMERIC(18,0),
	numUnitHour DECIMAL(18,2),
	monPrice DECIMAL(18,2),
	monTotAmount DECIMAL(18,2),
	monCost DECIMAL(18,2),
	numWarehouseItmsID NUMERIC(18,2),
	ItemName VARCHAR(MAX),
	numProductCost NUMERIC(18,0),
	numoppitemtCode NUMERIC(18,0)

)
DECLARE @numDivisionID AS NUMERIC(18,0)
DECLARE @numWarehouseItemID AS NUMERIC(18,0)
DECLARE @ItemName AS VARCHAR(MAX)
SELECT TOP 1 @numDivisionID=numDivisionId FROM OpportunityMaster WHERE numOppId=@numOppId
INSERT INTO @tempTableData
SELECT I.numItemCode,O.numUnitHour,monPrice,monTotAmount,ISNULL(V.monCost,0) AS monCost,O.numWarehouseItmsID,I.vcItemName,I.monListPrice,O.numoppitemtCode FROM OpportunityItems As O LEFT JOIN
Item AS I ON O.numItemCode=I.numItemCode
LEFT JOIN 
		Vendor V 
	ON 
		I.numVendorID = V.numVendorID AND I.numItemCode = V.numItemCode  
where numOppId=@numOppId AND O.bitItemPriceApprovalRequired=1

DECLARE @numItemClassification NUMERIC(18,0)
	DECLARE @numAbovePercent FLOAT
	DECLARE @numAboveField FLOAT
	DECLARE @numBelowPercent FLOAT
	DECLARE @numBelowField FLOAT
	DECLARE @numDefaultCost TINYINT
	DECLARE @bitCostApproval BIT
	DECLARE @bitListPriceApproval BIT
	DECLARE @numCostDomain INT
	DECLARE @tintKitAssemblyPriceBasedOn TINYINT
	DECLARE @bitCalAmtBasedonDepItems BIT
	DECLARE @numItemCode NUMERIC(18,0)
	DECLARE @numQuantity NUMERIC(18,0)
	DECLARE @numUnitPrice NUMERIC(18,0)
	DECLARE @numTotalAmount NUMERIC(18,0)
	DECLARE @monCost NUMERIC(18,0)
	DECLARE @numProductCost DECIMAL(18,2)=0
	DECLARE @numoppitemtCode DECIMAL(18,2)=0
	DECLARE @bitDiscountType BIT = 0
	DECLARE @fltDiscount NUMERIC(18,0) = 0
	SELECT @numCostDomain = numCost FROM Domain WHERE numDomainId = @numDomainID


DECLARE @i AS INT=1
DECLARE @RowCount AS INT=0
DECLARE @IsApprovalRequired BIT
	DECLARE @ItemAbovePrice FLOAT
	DECLARE @ItemBelowPrice FLOAT
DECLARE @numProposedPercentage AS DECIMAL(18,2)=0
SET @RowCount = (SELECT COUNT(*) FROM @tempTableData)
DECLARE @TEMPPrice TABLE
			(
				bitSuccess BIT
				,monPrice DECIMAL(20,5)
			)
WHILE (@i<=@RowCount)
BEGIN	
		SET @numProposedPercentage = 0
		SELECT 
			@numItemCode=numItemCode,
			@numQuantity=numUnitHour,
			@numUnitPrice=monPrice,
			@numTotalAmount=monTotAmount,
			@monCost=monCost,
			@numWarehouseItemID = numWarehouseItmsID,
			@ItemName=ItemName,
			@numProductCost=numProductCost,
			@numoppitemtCode =numoppitemtCode
		FROM
			@tempTableData
		WHERE
			ID=@i

		SELECT 
			@numItemClassification=ISNULL(numItemClassification,0)
			,@tintKitAssemblyPriceBasedOn=ISNULL(tintKitAssemblyPriceBasedOn,1)
			,@bitCalAmtBasedonDepItems=ISNULL(bitCalAmtBasedonDepItems,0) 
		FROM 
			Item 
		WHERE 
			numItemCode=@numItemCode

		IF ISNULL(@numItemClassification,0) > 0 AND EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainID=@numDomainID AND numListItemID=@numItemClassification)
		BEGIN
			SELECT
				@numAbovePercent = ISNULL(numAbovePercent,0)
				,@numBelowPercent = ISNULL(numBelowPercent,0)
				,@numBelowField = ISNULL(numBelowPriceField,0)
				,@numAboveField=ISNULL(@numCostDomain,1)
				,@bitCostApproval=ISNULL(bitCostApproval,0)
				,@bitListPriceApproval=ISNULL(bitListPriceApproval,0)
			FROM
				ApprovalProcessItemsClassification
			WHERE
				numDomainId=@numDomainID
				AND ISNULL(numListItemID,0)=@numItemClassification
		END
		ELSE
		BEGIN
			SELECT
				@numAbovePercent = ISNULL(numAbovePercent,0)
				,@numBelowPercent = ISNULL(numBelowPercent,0)
				,@numBelowField = ISNULL(numBelowPriceField,0)
				,@numAboveField=ISNULL(@numCostDomain,1)
				,@bitCostApproval=ISNULL(bitCostApproval,0)
				,@bitListPriceApproval=ISNULL(bitListPriceApproval,0)
			FROM
				ApprovalProcessItemsClassification
			WHERE
				numDomainId=@numDomainID
				AND ISNULL(numListItemID,0)=0
		END	


			SET @IsApprovalRequired = 0
			SET @ItemAbovePrice = 0
			SET @ItemBelowPrice = 0

			IF @numQuantity > 0
				SET @numUnitPrice = (@numTotalAmount / @numQuantity)
			ELSE
				SET @numUnitPrice = 0

			
			DELETE FROM @TEMPPrice
			IF @numAboveField > 0 AND ISNULL(@bitCostApproval,0) = 1
				BEGIN
			  
				IF (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode  AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 AND @bitCalAmtBasedonDepItems=1
				BEGIN
					DELETE FROM @TEMPPrice

					INSERT INTO @TEMPPrice
					(
						bitSuccess
						,monPrice
					)
					SELECT
						bitSuccess
						,monPrice
					FROM
						dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,@numDivisionID,@numItemCode,@numQuantity,@numWarehouseItemID,@tintKitAssemblyPriceBasedOn,0,0,'')
					
					IF (SELECT bitSuccess FROM @TEMPPrice) = 1
					BEGIN
						SET @ItemAbovePrice = (SELECT monPrice FROM @TEMPPrice)
					END
				END
				ELSE
				BEGIN
					IF @numAboveField = 3 -- Primaty Vendor Cost
						SELECT @ItemAbovePrice = ISNULL(monCost,0) FROM Vendor WHERE numItemCode = @numItemCode AND numVendorID = (SELECT numVendorID FROM Item WHERE numItemCode = @numItemCode)
					ELSE IF @numAboveField = 2 -- Product & Sevices Cost
						SELECT @ItemAbovePrice = ISNULL(@numProductCost,0)
					ELSE IF @numAboveField = 1 -- Average Cost
						SELECT @ItemAbovePrice = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode
				END
				IF @ItemAbovePrice > 0
				BEGIN
					IF @numUnitPrice < (@ItemAbovePrice + (@ItemAbovePrice * (@numAbovePercent/100)))
						SET @numProposedPercentage = (((@numUnitPrice - @ItemAbovePrice) /@ItemAbovePrice)*100)
						PRINT 'ABOVE CASE'
						--UPDATE 
						--	OpportunityItems 
						--SET 
						--	monPrice=(@ItemAbovePrice + (@ItemAbovePrice * (@numAbovePercent/100))) ,
						--	monTotAmtBefDiscount = (@numQuantity * (@ItemAbovePrice + (@ItemAbovePrice * (@numAbovePercent/100)))),
						--	monTotAmount = (@numQuantity * (@ItemAbovePrice + (@ItemAbovePrice * (@numAbovePercent/100)))),
						--	bitDiscountType = 0,
						--	fltDiscount = 0,
						--	bitItemPriceApprovalRequired =0 
						--WHERE 
						--	numOppId=@numOppId AND 
						--	numoppitemtCode=@numoppitemtCode

						--SET @returnFormattedItems= @returnFormattedItems + '<b>' + @ItemName + '</b>' +'('+CAST(@numQuantity As VARCHAR(500))+') Proposed '+CAST(@numProposedPercentage AS VARCHAR(100))+'%, Minimum Allowed '+CAST(@numAbovePercent As VARCHAR(500))+'%, '
						--SET @IsApprovalRequired = 1
				END
				ELSE IF @ItemAbovePrice = 0
				BEGIN
					SET @IsApprovalRequired = 1
				END
			END

			IF @IsApprovalRequired = 0 AND @numBelowField > 0  AND ISNULL(@bitListPriceApproval,0) = 1
			BEGIN
				IF @numBelowField = 1 -- List Price
				BEGIN
					IF (SELECT ISNULL(charItemType,'') AS charItemType FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode) = 'P' -- Inventory Item
						IF (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode  AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 AND @bitCalAmtBasedonDepItems=1
						BEGIN
							DELETE FROM @TEMPPrice

							INSERT INTO @TEMPPrice
							(
								bitSuccess
								,monPrice
							)
							SELECT
								bitSuccess
								,monPrice
							FROM
								dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,@numDivisionID,@numItemCode,@numQuantity,@numWarehouseItemID,@tintKitAssemblyPriceBasedOn,0,0,'')

							IF (SELECT bitSuccess FROM @TEMPPrice) = 1
							BEGIN
								SET @ItemBelowPrice = (SELECT monPrice FROM @TEMPPrice)
							END
						END
						ELSE
						BEGIN
							SELECT @ItemBelowPrice = ISNULL(monWListPrice,0) FROM WareHouseItems WHERE numDomainID = @numDomainID AND numItemID = @numItemCode AND numWareHouseItemID = @numWarehouseItemID
						END
					ELSE
						SELECT @ItemBelowPrice = ISNULL(monListPrice,0) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode
				END
				ELSE IF @numBelowField = 2 -- Price Rule
				BEGIN
					/* Check if valid price book rules exists for sales in domain */
					IF (SELECT COUNT(*) FROM PriceBookRules WHERE numDomainId = @numDomainID AND tintRuleFor = 1 AND tintStep2 > 0 AND tintStep3 > 0) > 0
					BEGIN
						DECLARE @j INT = 0
						DECLARE @Count int = 0
						DECLARE @tempPriority INT
						DECLARE @tempNumPriceRuleID NUMERIC(18,0)
						DECLARE @numPriceRuleIDApplied NUMERIC(18,0) = 0
						DECLARE @TempTable TABLE (numID INT, numRuleID numeric(18,0), numPriority INT)

						INSERT INTO 
							@TempTable
						SELECT 
							ROW_NUMBER() OVER (ORDER BY PriceBookPriorities.Priority) AS id,
							numPricRuleID,
							PriceBookPriorities.Priority 
						FROM 
							PriceBookRules
						INNER JOIN
							PriceBookPriorities
						ON
							PriceBookRules.tintStep2 = PriceBookPriorities.Step2Value AND
							PriceBookRules.tintStep3 = PriceBookPriorities.Step3Value
						WHERE 
							PriceBookRules.numDomainId = @numDomainID AND 
							PriceBookRules.tintRuleFor = 1 AND 
							PriceBookRules.tintStep2 > 0 AND PriceBookRules.tintStep3 > 0
						ORDER BY
							PriceBookPriorities.Priority

						SELECT @Count = COUNT(*) FROM @TempTable

						/* Loop all price rule with priority */
						WHILE (@j < @count)
						BEGIN
							SELECT @tempNumPriceRuleID = numRuleID, @tempPriority = numPriority FROM @TempTable WHERE numID = (@i + 1)
					
							/* IF proprity is 9 then price rule is applied to all items and all customers. 
							So price rule must be applied to item or not.*/
							IF @tempPriority = 9
							BEGIN
								SET @numPriceRuleIDApplied = @tempNumPriceRuleID
								BREAK
							END
							/* Check if current item exists in rule. if eixts then exit loop with rule id else continie loop. if item not exist in any rule then nothing to check */
							ELSE
							BEGIN
								DECLARE @isRuleApplicable BIT = 0
								EXEC @isRuleApplicable = dbo.CheckIfPriceRuleApplicableToItem @numRuleID = @tempNumPriceRuleID, @numItemID = @numItemCode, @numDivisionID = @numDivisionID 

								IF @isRuleApplicable = 1
								BEGIN
									SET @numPriceRuleIDApplied = @tempNumPriceRuleID
									BREAK
								END
							END

							SET @j = @j + 1
						END

						/* If @numPriceRuleIDApplied > 0 Get final unit price limit after applying below rule */

						IF @numPriceRuleIDApplied > 0
						BEGIN
							SET @ItemBelowPrice = dbo.GetUnitPriceAfterPriceRuleApplication(@numPriceRuleIDApplied,@numDomainID,@numItemCode,@numQuantity,@numWarehouseItemID,@numDivisionID)
						END
					END
				END
				ELSE IF @numBelowField = 3 -- Price Level
				BEGIN
					EXEC @ItemBelowPrice = GetUnitPriceAfterPriceLevelApplication @numDomainID = @numDomainID, @numItemCode = @numItemCode, @numQuantity = @numQuantity, @numWarehouseItemID = @numWarehouseItemID, @isPriceRule = 0, @numPriceRuleID = 0, @numDivisionID = @numDivisionID
				END
		
				IF @numUnitPrice < (@ItemBelowPrice - (@ItemBelowPrice * (@numBelowPercent/100)))
				SET @numProposedPercentage = (((@ItemBelowPrice- @numUnitPrice) /@ItemBelowPrice)*100)
				PRINT 'NEW CASE'
					UPDATE 
							OpportunityItems 
						SET 
							monPrice=(@ItemBelowPrice - (@ItemBelowPrice * (@numBelowPercent/100))) ,
							monTotAmtBefDiscount = (@numQuantity * (@ItemBelowPrice - (@ItemBelowPrice * (@numBelowPercent/100)))),
							monTotAmount = (@numQuantity * (@ItemBelowPrice - (@ItemBelowPrice * (@numBelowPercent/100)))),
							bitDiscountType = 0,
							fltDiscount = 0,
							bitItemPriceApprovalRequired =0 
						WHERE 
							numOppId=@numOppId AND 
							numoppitemtCode=@numoppitemtCode
				--SET @returnFormattedItems=@returnFormattedItems + '<b>' + @ItemName + '</b>' +'('+CAST(@numQuantity As VARCHAR(500))+') Proposed '+CAST(@numProposedPercentage AS VARCHAR(500))+'%, Minimum Allowed '+CAST(@numBelowPercent As VARCHAR(500))+'%, '
				--		SET @IsApprovalRequired = 1
			END



	SET @i=@i+1
END
	UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID
    --RETURN ISNULL(@returnFormattedItems,'')
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TicklerActItemsButtonCount')
DROP PROCEDURE USP_TicklerActItemsButtonCount
GO
CREATE Proc [dbo].[USP_TicklerActItemsButtonCount]     
@numUserCntID as numeric(9)=null,                                                                        
@numDomainID as numeric(9)=null,                                                                        
@startDate as datetime,                                                                        
@endDate as datetime,                                                                    
@ClientTimeZoneOffset Int
--,
--@numViewID AS INT,
--@numTotalRecords AS NUMERIC(18,0)=0 OUTPUT
AS
BEGIN
   DECLARE @ItemsToPick AS NUMERIC(18,0)=0
   DECLARE @ItemsToPackShip AS NUMERIC(18,0)=0
   DECLARE @ItemsToInvoice AS NUMERIC(18,0)=0
   DECLARE @SalesOrderToClose AS NUMERIC(18,0)=0
   DECLARE @ItemsToPutAway AS NUMERIC(18,0)=0
   DECLARE @ItemsToBill AS NUMERIC(18,0)=0
   DECLARE @ItemsPOClose AS NUMERIC(18,0)=0
   DECLARE @ItemsBOMPick AS NUMERIC(18,0)=0

   DECLARE @bitItemsToPickPackShip AS BIT
   DECLARE @bitItemsToInvoice AS BIT
   DECLARE @bitSalesOrderToClose AS BIT
   DECLARE @bitItemsToPutAway AS BIT
   DECLARE @bitItemsToBill AS BIT
   DECLARE @bitPosToClose AS BIT
   DECLARE @bitBOMSToPick AS BIT

   DECLARE @vchItemsToPickPackShip AS VARCHAR(500)
   DECLARE @vchItemsToInvoice AS VARCHAR(500)
   DECLARE @vchSalesOrderToClose AS VARCHAR(500)
   DECLARE @vchItemsToPutAway AS VARCHAR(500)
   DECLARE @vchItemsToBill AS VARCHAR(500)
   DECLARE @vchPosToClose AS VARCHAR(500)
   DECLARE @vchBOMSToPick AS VARCHAR(500)

   SELECT 
		@bitItemsToPickPackShip=bitItemsToPickPackShip ,
		@bitItemsToInvoice=bitItemsToInvoice, 
		@bitSalesOrderToClose=bitSalesOrderToClose ,
		@bitItemsToPutAway=bitItemsToPutAway ,
		@bitItemsToBill=bitItemsToBill ,
		@bitPosToClose=bitPosToClose ,
		@bitBOMSToPick = bitBOMSToPick,
		@vchItemsToPickPackShip = vchItemsToPickPackShip,
		@vchItemsToInvoice = vchItemsToInvoice,
		@vchSalesOrderToClose = vchSalesOrderToClose,
		@vchItemsToPutAway = vchItemsToPutAway,
		@vchItemsToBill = vchItemsToBill,
		@vchPosToClose = vchPosToClose,
		@vchBOMSToPick = vchBOMSToPick
   FROM 
		Domain
   WHERE
		numDomainId=@numDomainID
   IF(@bitItemsToPickPackShip=1 AND @numUserCntID IN(SELECT Items FROM Split(@vchItemsToPickPackShip,',') WHERE Items<>''))
   BEGIN
	EXEC USP_TicklerActItemsV2TopCount @numUserCntID = @numUserCntID, @numDomainID = @numDomainID,@startDate = @startDate, @endDate = @endDate,@ClientTimeZoneOffset = @ClientTimeZoneOffset,@numViewID =1,@numTotalRecords=@ItemsToPick OUTPUT
   END
   IF(@bitItemsToPickPackShip=1 AND @numUserCntID IN(SELECT Items FROM Split(@vchItemsToPickPackShip,',') WHERE Items<>''))
   BEGIN
	EXEC USP_TicklerActItemsV2TopCount @numUserCntID = @numUserCntID, @numDomainID = @numDomainID,@startDate = @startDate, @endDate = @endDate,@ClientTimeZoneOffset = @ClientTimeZoneOffset,@numViewID =2,@numTotalRecords=@ItemsToPackShip OUTPUT
   END
   IF(@bitItemsToInvoice=1 AND @numUserCntID IN(SELECT Items FROM Split(@vchItemsToInvoice,',') WHERE Items<>''))
   BEGIN
		EXEC USP_TicklerActItemsV2TopCount @numUserCntID = @numUserCntID, @numDomainID = @numDomainID,@startDate = @startDate, @endDate = @endDate,@ClientTimeZoneOffset = @ClientTimeZoneOffset,@numViewID =3,@numTotalRecords=@ItemsToInvoice OUTPUT
   END
   IF(@bitSalesOrderToClose=1 AND @numUserCntID IN(SELECT Items FROM Split(@vchSalesOrderToClose,',') WHERE Items<>''))
   BEGIN
		EXEC USP_TicklerActItemsV2TopCount @numUserCntID = @numUserCntID, @numDomainID = @numDomainID,@startDate = @startDate, @endDate = @endDate,@ClientTimeZoneOffset = @ClientTimeZoneOffset,@numViewID =5,@numTotalRecords=@SalesOrderToClose OUTPUT
   END
   IF(@bitItemsToPutAway=1 AND @numUserCntID IN(SELECT Items FROM Split(@vchItemsToPutAway,',') WHERE Items<>''))
   BEGIN
	EXEC USP_TicklerActItemsV2TopPurchaseRecordCount @numUserCntID = @numUserCntID, @numDomainID = @numDomainID,@startDate = @startDate, @endDate = @endDate,@ClientTimeZoneOffset = @ClientTimeZoneOffset,@numViewID =2,@numTotalRecords=@ItemsToPutAway OUTPUT
   END
    IF(@bitItemsToBill=1 AND @numUserCntID IN(SELECT Items FROM Split(@vchItemsToBill,',') WHERE Items<>''))
	BEGIN
		EXEC USP_TicklerActItemsV2TopPurchaseRecordCount @numUserCntID = @numUserCntID, @numDomainID = @numDomainID,@startDate = @startDate, @endDate = @endDate,@ClientTimeZoneOffset = @ClientTimeZoneOffset,@numViewID =3,@numTotalRecords=@ItemsToBill OUTPUT
	END
 IF(@bitPosToClose=1 AND @numUserCntID IN(SELECT Items FROM Split(@vchPosToClose,',') WHERE Items<>''))
	BEGIN
   EXEC USP_TicklerActItemsV2TopPurchaseRecordCount @numUserCntID = @numUserCntID, @numDomainID = @numDomainID,@startDate = @startDate, @endDate = @endDate,@ClientTimeZoneOffset = @ClientTimeZoneOffset,@numViewID =4,@numTotalRecords=@ItemsPOClose OUTPUT
END
   SELECT 
	@ItemsToPick AS ItemsToPick,
	@ItemsToPackShip AS ItemsToPackShip,
	@ItemsToInvoice AS ItemsToInvoice,
	@SalesOrderToClose AS SalesOrderToClose,
	@ItemsToPutAway AS ItemsToPutAway,
	@ItemsToBill AS ItemsToBill,
	@ItemsPOClose AS ItemsPOClose,
	@ItemsBOMPick AS ItemsBOMPick
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TicklerActItemsV2')
DROP PROCEDURE USP_TicklerActItemsV2
GO
CREATE Proc [dbo].[USP_TicklerActItemsV2]       
@numUserCntID as numeric(9)=null,                                                                        
@numDomainID as numeric(9)=null,                                                                        
@startDate as datetime,                                                                        
@endDate as datetime,                                                                    
@ClientTimeZoneOffset Int,  --Added by Debasish to enable calculation of date according to client machine             
@bitFilterRecord bit=0,
@columnName varchar(50), 
@columnSortOrder varchar(50),
@vcBProcessValue varchar(500),
@RegularSearchCriteria VARCHAR(MAX)='',
@CustomSearchCriteria VARCHAR(MAX)='', 
@OppStatus int,
@CurrentPage int,                                                              
@PageSize int
AS
BEGIN
DECLARE @tintActionItemsViewRights TINYINT  = 3
DECLARE @bitREQPOApproval AS BIT
DECLARE @bitARInvoiceDue AS BIT
DECLARE @bitAPBillsDue AS BIT
DECLARE @vchARInvoiceDue AS VARCHAR(500)
DECLARE @vchAPBillsDue AS VARCHAR(500)
DECLARE @decReqPOMinValue AS DECIMAL(18,2)
DECLARE @vchREQPOApprovalEmp AS VARCHAR(500)=''
DECLARE @numDomainDivisionID NUMERIC(18,0)
DECLARE @dynamicPartQuery AS NVARCHAR(MAX)=''
SELECT TOP 1 
	@bitREQPOApproval=bitREQPOApproval,
	@vchREQPOApprovalEmp=vchREQPOApprovalEmp,
	@decReqPOMinValue=ISNULL(decReqPOMinValue,0),
	@bitARInvoiceDue=bitARInvoiceDue,
	@bitAPBillsDue = bitAPBillsDue,
	@vchARInvoiceDue = vchARInvoiceDue,
	@vchAPBillsDue = vchAPBillsDue,
	@numDomainDivisionID=numDivisionID
FROM 
	Domain 
WHERE 
	numDomainId=@numDomainID

IF EXISTS (SELECT * FROM GroupAuthorization WHERE numDomainID=@numDomainID AND numModuleID=1 AND numPageID=2 AND numGroupID IN (SELECT ISNULL(numGroupID,0) FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailId=@numUserCntID))
BEGIN
	SELECT @tintActionItemsViewRights=ISNULL(intViewAllowed,0) FROM GroupAuthorization WHERE numDomainID=@numDomainID AND numModuleID=1 AND numPageID=2 AND numGroupID IN (SELECT ISNULL(numGroupID,0) FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailId=@numUserCntID)
END
--12 - Communication task
--1 - Case Task
--2 - Project Task
--3 - Opportunity Task
--4 - Sales Order Task
--5 - Work Order Management
--6 - Work Center Task
--7 - Requisition Approval
--8 - PO Approval
--9 - Price Margin Approval
--10 - A/R Invoice Due
--11 - A/P Bill Due

DECLARE @dynamicQuery AS NVARCHAR(MAX)=''
DECLARE @dynamicQueryCount AS NVARCHAR(MAX)=''

-------------------- 0 - TASK----------------------------

SET @dynamicQuery = @dynamicQuery + ' SELECT 
Comm.numCommId AS RecordId,
Comm.numCreatedBy AS numCreatedBy, 
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
bitClosedFlag AS IsTaskClosed,
''<a onclick="openCommTask(''+CAST(Comm.numCommId AS VARCHAR)+'',0)" href="javascript:void(0)">''+dbo.GetListIemName(Comm.bitTask)+''</a>'' As TaskTypeName,
12 AS TaskType,
'''' AS OrigDescription,
comm.textDetails AS Description,
cast(Comm.dtStartTime as datetime) as DueDate,
''-'' As TotalProgress,
Substring( 
dbo.fn_GetContactName(numAssign) + 
ISNULL((SELECT SUBSTRING((SELECT '','' + dbo.fn_GetContactName(numContactId) FROM CommunicationAttendees WHERE numCommId=Comm.numCommId FOR XML PATH('''')),1,200000)),''''),0,50)
As numAssignedTo,
numAssign As numAssignToId,
listdetailsStatus.VcData As Priority, -- Priority column   
 listdetailsActivity.VcData As Activity ,
listdetailsStatus.NumlistItemID As PriorityId, -- Priority column   
 listdetailsActivity.NumlistItemID As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Comp.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM  Communication Comm                              
 JOIN AdditionalContactsInformation AddC                                                  
 ON Comm.numContactId = AddC.numContactId                                  
 JOIN DivisionMaster Div                                                                         
 ON AddC.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                                                                         
 ON Div.numCompanyID = Comp.numCompanyId  
 OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=Div.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder
 OUTER APPLY(' + CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = Div.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') + ' AS TempPerformance
 LEFT JOIN OpportunityMaster Opp ON Opp.numDivisionId = Div.numDivisionID AND Opp.numContactId = AddC.numContactId  AND Opp.numOppID = Comm.numOppID 
 LEFT JOIN Sales_process_List_Master splm ON splm.Slp_Id = Opp.numBusinessProcessID 
 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId
  Left Join listdetails listdetailsStatus                                                 
 On Comm.numStatus = listdetailsStatus.NumlistItemID
  Left Join listdetails listdetailsActivity                                                  
 On Comm.numActivity = listdetailsActivity.NumlistItemID
 LEFT JOIN ListDetails AS listCompanyRating
 ON Comp.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	Comm.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (Comm.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or Comm.numAssign= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END)  AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) <= '''+Cast(@endDate as varchar(30))+''') '
SET @dynamicPartQuery = @dynamicQuery
-------------------- 0 - Email Communication----------------------------

DECLARE @FormattedItems As VARCHAR(MAX)
SET @dynamicQuery = @dynamicQuery + 'UNION SELECT 
ac.activityid AS RecordId,
0 AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
-2 AS numTaskId,
case when alldayevent = 1 then ''1'' else ''0'' end AS IsTaskClosed,
''<a onclick="openCommTask(''+CAST(ac.activityid AS VARCHAR)+'',1)" href="javascript:void(0)">Calendar</a>'' As TaskTypeName,
12 AS TaskType,
ac.ActivityDescription AS OrigDescription,
(SELECT DISTINCT  
replace(replace((SELECT 
CASE(ISNULL(bitpartycalendarTitle,1)) WHEN 1 THEN ''<b><font color=red>Title</font></b><br>'' + CASE(ISNULL(ac.Subject,'''')) WHEN '''' THEN '''' else ac.Subject END + ''<br>'' else ''''  END  
+ CASE(ISNULL(bitpartycalendarLocation,1)) WHEN 1 THEN ''<b><font color=green>Location</font></b><br>'' + CASE(ISNULL(ac.location,'''')) WHEN '''' THEN '''' else ac.location END + ''<br>'' else ''''  END  
+ CASE(isnull(bitpartycalendarDescription,1)) when 1 THEN ''<b><font color=#3c8dbc>Description</font></b><br>$'' else ''''  END  
FROM Domain where numDomainId='+ CAST(@numDomainID AS VARCHAR) +' 
FOR XML PATH('''')
),''&lt;'', ''<''), ''&gt;'', ''>''))  AS Description,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dateadd(second,duration,startdatetimeutc)) as DueDate,
''0'' As TotalProgress,
''-'' As numAssignedTo,
''0'' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Comp.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 From activity ac join             
activityresource ar on  ac.activityid=ar.activityid             
join resource res on ar.resourceid=res.resourceid                         
--join usermaster UM on um.numUserId = res.numUserCntId                
join AdditionalContactsInformation ACI on ACI.numContactId = res.numUserCntId                
 JOIN DivisionMaster Div                                                          
 ON ACI.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                             
 ON Div.numCompanyID = Comp.numCompanyId
  LEFT JOIN ListDetails AS listCompanyRating
 ON Comp.numCompanyRating=listCompanyRating.NumlistItemID
 OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=Div.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder                 
 OUTER APPLY(' + CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = Div.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') + ' AS TempPerformance
 where  res.numUserCntId = ' + Cast(@numUserCntID as varchar(10)) +'             
and ac.OriginalStartDateTimeUtc IS NULL            
and res.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  and 
(DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) <= '''+Cast(@endDate as varchar(30))+''')
--(startdatetimeutc between dateadd(month,-1,getutcdate()) and dateadd(month,1,getutcdate())) 
and [status] <> -1                                                     
and ac.activityid not in 
(select distinct(numActivityId) from communication Comm where Comm.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  
AND 1=(Case ' + Cast(@bitFilterRecord as varchar(10)) +' when 1 then Case when Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  then 1 else 0 end 
		else  Case when (Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  or Comm.numCreatedBy=' + Cast(@numUserCntID as varchar(10)) +' ) then 1 else 0 end end)   
		AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (Comm.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or Comm.numAssign= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END)                                                                   
-- AND (dtStartTime >= '''+Cast(@startDate as varchar(30))+''' and dtStartTime <= '''+Cast(@endDate as varchar(30))+''') 
)                       '



-------------------- 1 - CASE----------------------------

SET @dynamicQuery = @dynamicQuery + 'UNION SELECT 
C.numCaseId AS RecordId,
C.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
CASE WHEN C.numStatus=1008 THEN 1 ELSE 0 END AS IsTaskClosed,
''<a onclick="openCaseDetails(''+CAST(C.numCaseId AS VARCHAR)+'')" href="javascript:void(0)">Case</a>'' As TaskTypeName,
1 AS TaskType,
'''' AS OrigDescription,
C.textSubject AS Description,
cast(C.intTargetResolveDate as datetime) as DueDate,
''0'' As TotalProgress,
dbo.fn_GetContactName(C.numAssignedTo) As numAssignedTo,
C.numAssignedTo As numAssignToId,
listdetailsStatus.VcData As Priority, -- Priority column   
'''' As Activity ,
listdetailsStatus.NumlistItemID As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM 
	Cases  C                          
INNER JOIN  
	AdditionalContactsInformation  AddC                          
ON 
	C.numContactId = AddC.numContactId                           
INNER JOIN 
	DivisionMaster Div                           
ON 
	AddC.numDivisionId = Div.numDivisionID AND  
	AddC.numDivisionId = Div.numDivisionID                           
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
 LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
  Left Join listdetails listdetailsStatus                                                 
 On C.numPriority = listdetailsStatus.NumlistItemID
WHERE
	C.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND C.numStatus <> 136  AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (C.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or C.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END)  AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',intTargetResolveDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',intTargetResolveDate) <= '''+Cast(@endDate as varchar(30))+''') '

-------------------- 2 - Project Task ----------------------------

SET @dynamicQuery = @dynamicQuery + 'UNION SELECT * FROM (SELECT
T.numProjectId AS RecordId,
T.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
T.bitTaskClosed AS IsTaskClosed,
''<a onclick="openProjectTask(''+CAST(T.numProjectId AS VARCHAR)+'')" href="javascript:void(0)">Project Task</a>'' As TaskTypeName,
2 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END as DueDate,
ISNULL((SELECT TOP 1 intTotalProgress FROM ProjectProgress WHERE numProId = T.numProjectId),0) As TotalProgress,
dbo.fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM 
	StagePercentageDetailsTask AS T
	LEFT JOIN StagePercentageDetails AS S
	ON S.numStageDetailsId=T.numStageDetailsId AND S.numProjectID=T.numProjectID
	LEFT JOIN ProjectsMaster AS OP 
	ON T.numProjectID=OP.numProId
	LEFT JOIN StagePercentageMaster AS SM
	ON S.numStagePercentageId=SM.numStagePercentageId                        
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	T.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND T.numProjectId>0 AND s.numProjectId>0 AND T.bitSavedTask=1 AND T.bitTaskClosed=0 AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (T.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or T.numAssignTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END) ) Q WHERE (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') '


------------------ 3 - Opportunity  Task ----------------------------

SET @dynamicQuery = @dynamicQuery + 'UNION SELECT * FROM (SELECT 
T.numOppId AS RecordId,
T.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
T.bitTaskClosed AS IsTaskClosed,
''<a onclick="openTask(''+CAST(T.numOppId AS VARCHAR)+'',''+CAST(S.numStagePercentageId AS VARCHAR)+'',''+CAST(S.tinProgressPercentage AS VARCHAR)+'',''+CAST(S.tintConfiguration AS VARCHAR)+'')" href="javascript:void(0)">Opportunity Task</a>'' As TaskTypeName,
3 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END as DueDate,
ISNULL((SELECT TOP 1 intTotalProgress FROM ProjectProgress WHERE numOppId = T.numOppId),0) As TotalProgress,
dbo.fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM 
	StagePercentageDetailsTask AS T
	LEFT JOIN StagePercentageDetails AS S
	ON S.numStageDetailsId=T.numStageDetailsId AND S.numOppId=T.numOppId
	LEFT JOIN OpportunityMaster AS OP 
	ON T.numOppId=OP.numOppId
	LEFT JOIN StagePercentageMaster AS SM
	ON S.numStagePercentageId=SM.numStagePercentageId                        
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	T.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType=1 AND OP.tintOppStatus=0 AND T.numOppId>0 AND s.numOppId>0 AND T.bitSavedTask=1 AND T.bitTaskClosed=0 AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (T.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or T.numAssignTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END) ) Q WHERE (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') '

------------------ 4 -  Sales Order Task ----------------------------

SET @dynamicQuery = @dynamicQuery + 'UNION SELECT * FROM (SELECT 
T.numOppId AS RecordId,
T.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
T.bitTaskClosed AS IsTaskClosed,
''<a onclick="openTask(''+CAST(T.numOppId AS VARCHAR)+'',''+CAST(S.numStagePercentageId AS VARCHAR)+'',''+CAST(S.tinProgressPercentage AS VARCHAR)+'',''+CAST(S.tintConfiguration AS VARCHAR)+'')" href="javascript:void(0)">Sales Order Task</a>'' As TaskTypeName,
4 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END as DueDate,
ISNULL((SELECT TOP 1 intTotalProgress FROM ProjectProgress WHERE numOppId = T.numOppId),0) As TotalProgress,
dbo.fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM 
	StagePercentageDetailsTask AS T
	LEFT JOIN StagePercentageDetails AS S
	ON S.numStageDetailsId=T.numStageDetailsId AND S.numOppId=T.numOppId
	LEFT JOIN OpportunityMaster AS OP 
	ON T.numOppId=OP.numOppId
	LEFT JOIN StagePercentageMaster AS SM
	ON S.numStagePercentageId=SM.numStagePercentageId                        
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	T.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType=1 AND OP.tintOppStatus=1 AND T.numOppId>0 AND s.numOppId>0 AND T.bitSavedTask=1 AND T.bitTaskClosed=0 AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (T.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or T.numAssignTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END) ) Q WHERE (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') '

------------------ 5 -  Work Order Task ----------------------------

SET @dynamicQuery = @dynamicQuery + ' UNION SELECT 
WorkOrder.numWOID AS RecordId,
WorkOrder.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
WorkOrder.numWOID AS numTaskId,
(CASE WHEN WorkOrder.numWOStatus = 23184 THEN 1 ELSE 0 END) AS IsTaskClosed,
CONCAT(''<a href="../items/frmWorkOrder.aspx?WOID='',WorkOrder.numWOID,''" target="_blank">Work Order Management</a>'') As TaskTypeName,
5 AS TaskType,
'''' AS OrigDescription,
CONCAT(''<b>'',Item.vcItemName,''</b>'','' ('',WorkOrder.numQtyItemsReq,'')'','' <b>Work Order Status :'', ''</b>'',''<label class="lblWorkOrderStatus" id="'',WorkOrder.numWOID,''"><i class="fa fa-refresh fa-spin"></i></lable>'') AS Description,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) as DueDate,
dbo.GetTotalProgress(WorkOrder.numDomainID,WorkOrder.numWOID,1,1,'''',0) As TotalProgress,
dbo.fn_GetContactName(WorkOrder.numAssignedTo) As numAssignedTo,
WorkOrder.numAssignedTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM 
	WorkOrder
INNER JOIN 
	Item
ON
	WorkOrder.numItemCode = Item.numItemCode
LEFT JOIN 
	OpportunityMaster
ON 
	WorkOrder.numOppId=OpportunityMaster.numOppId
INNER JOIN 
	DivisionMaster Div                           
ON 
	Div.numDivisionId = (CASE WHEN OpportunityMaster.numOppID IS NOT NULL THEN OpportunityMaster.numDivisionID ELSE ' + CAST(@numDomainDivisionID AS VARCHAR) + ' END)                        
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN 
	ListDetails AS listCompanyRating
ON 
	Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	WorkOrder.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' 
	AND WorkOrder.numWOStatus <> 23184
	AND WorkOrder.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + '
	AND ((DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmStartDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmStartDate) <= '''+Cast(@endDate as varchar(30))+''') OR (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) <= '''+Cast(@endDate as varchar(30))+'''))'


------------------ 6 -  Work Center Task ----------------------------

SET @dynamicQuery = @dynamicQuery + 'UNION SELECT 
T.numTaskId AS RecordId,
T.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
(CASE WHEN EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID AND SPDTTL.tintAction=4) THEN 1 ELSE 0 END) AS IsTaskClosed,
CONCAT(''<a href="../items/frmWorkOrder.aspx?WOID='',W.numWOID,''" target="_blank">Work Center Task</a>'') As TaskTypeName,
6 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) as DueDate,
(CASE 
	WHEN EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID AND SPDTTL.tintAction=4) THEN CAST(100 AS INT)
	ELSE CAST(((ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID),0) / W.numQtyItemsReq) * 100) AS INT)
END) As TotalProgress,
dbo.fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM 
	StagePercentageDetailsTask AS T
	INNER JOIN WorkOrder AS W 
	ON T.numWorkOrderId=W.numWOId     
LEFT JOIN OpportunityMaster AS OP 
	ON W.numOppId=OP.numOppId
INNER JOIN 
	DivisionMaster Div                           
ON 
	Div.numDivisionId = (CASE WHEN OP.numOppID IS NOT NULL THEN OP.numDivisionID ELSE ' + CAST(@numDomainDivisionID AS VARCHAR) + ' END)                         
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	W.numDomainID = ' + Cast(@numDomainID as varchar(10))  + '
	AND W.numWOStatus <> 23184 
	AND NOT EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID AND SPDTTL.tintAction=4) 
	AND T.numAssignTo = ' + CAST(@numUserCntID AS VARCHAR) + '
	AND ((DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmStartDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmStartDate) <= '''+Cast(@endDate as varchar(30))+''') OR (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) <= '''+Cast(@endDate as varchar(30))+'''))'


------------------ 7 -  Requisition Approval ----------------------------

IF(@bitREQPOApproval=1 AND @numUserCntID IN(SELECT * FROM Split(@vchREQPOApprovalEmp,',') WHERE Items<>''))
BEGIN
SET @dynamicQuery = @dynamicQuery  + 'UNION SELECT * FROM (SELECT 
OP.numOppId AS RecordId,
OP.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
CASE WHEN ISNULL(OP.intReqPOApproved,0)=0 THEN 0 ELSE 1 END AS IsTaskClosed,
''<a  onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')" href="javascript:void(0)">Requisition Approval</a>'' As TaskTypeName,
7 AS TaskType,
'''' AS OrigDescription,
''<b>For :</b> <a class="underLineHyperLink" onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')" href="javascript:void(0)">''+OP.vcPOppName+''</a> &nbsp;<i onclick="ApproveRejectPORequistion(''+CAST(OP.numOppId AS VARCHAR)+'',1)"  class="fa fa-thumbs-up cursor"></i>  &nbsp;<i onclick="ApproveRejectPORequistion(''+CAST(OP.numOppId AS VARCHAR)+'',2)"  class="fa fa-thumbs-down cursor"></i>'' AS Description,
OP.intPEstimatedCloseDate as DueDate,
0 As TotalProgress,
dbo.fn_GetContactName('+CAST(@numUserCntID AS VARCHAR(200))+') As numAssignedTo,
'+CAST(@numUserCntID AS VARCHAR(200))+' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM  
 OpportunityMaster AS OP 
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	OP.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType = 2 AND OP.tintOppStatus=0 
	AND 1 = (CASE WHEN '+CAST(@decReqPOMinValue AS VARCHAR(100))+'>0 AND Op.monPAmount>'+CAST(@decReqPOMinValue AS VARCHAR(100))+' THEN 1  WHEN '+CAST(@decReqPOMinValue AS VARCHAR(100))+'=0 THEN 1  ELSE 0 END ) ) AS Q
WHERE (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') '
END
------------------ 8 -  PO Approval ----------------------------
IF(@bitREQPOApproval=1 AND @numUserCntID IN(SELECT * FROM Split(@vchREQPOApprovalEmp,',') WHERE Items<>''))
BEGIN
SET @dynamicQuery = @dynamicQuery + 'UNION SELECT * FROM (SELECT
OP.numOppId AS RecordId,
OP.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
CASE WHEN ISNULL(OP.intReqPOApproved,0)=0 THEN 0 ELSE 1 END AS IsTaskClosed,
''<a onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')" href="javascript:void(0)">PO Approval</a>'' As TaskTypeName,
8 AS TaskType,
'''' AS OrigDescription,
''<b>For:</b> <a href="javascript:void(0)"  class="underLineHyperLink" onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')">''+OP.vcPOppName+''</a> &nbsp;<i onclick="ApproveRejectPORequistion(''+CAST(OP.numOppId AS VARCHAR)+'',1)"  class="fa fa-thumbs-up cursor"></i>  &nbsp;<i onclick="ApproveRejectPORequistion(''+CAST(OP.numOppId AS VARCHAR)+'',2)"  class="fa fa-thumbs-down cursor"></i> '' AS Description,
OP.intPEstimatedCloseDate as DueDate,
0 As TotalProgress,
dbo.fn_GetContactName('+CAST(@numUserCntID AS VARCHAR(200))+') As numAssignedTo,
'+CAST(@numUserCntID AS VARCHAR(200))+' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM  
 OpportunityMaster AS OP 
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	OP.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType = 2 AND OP.tintOppStatus=1 
	AND 1 = (CASE WHEN '+CAST(@decReqPOMinValue AS VARCHAR(100))+'>0 AND Op.monPAmount>'+CAST(@decReqPOMinValue AS VARCHAR(100))+' THEN 1  WHEN '+CAST(@decReqPOMinValue AS VARCHAR(100))+'=0 THEN 1  ELSE 0 END )) AS Q
	WHERE (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') '
END

------------------ 9 -  Price Margin Approval ----------------------------

SET @dynamicQuery = @dynamicQuery + 'UNION SELECT * FROM (SELECT
OP.numOppId AS RecordId,
OP.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
ISNULL(OP.bitReqPOApproved,0) AS IsTaskClosed,
''<a onclick="OpenMarginApproval(''+CAST(OP.numOppId AS VARCHAR)+'')" href="javascript:void(0)">Price Margin Approval</a>'' As TaskTypeName,
9 AS TaskType,
'''' AS OrigDescription,
''<b>For : </b> <a href="javascript:void(0)"  onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')">''+OP.vcPOppName+''</a> : '' +dbo.ItemUnitPriceApproval_CheckAndGetItemDetails(OP.numOppId,OP.numDomainId)+''  &nbsp;<i onclick="ApproveRejectPriceMarginApproval(''+CAST(OP.numOppId AS VARCHAR)+'',1)" class="fa fa-thumbs-up cursor"></i>  &nbsp;<i onclick="ApproveRejectPriceMarginApproval(''+CAST(OP.numOppId AS VARCHAR)+'',2)" class="fa fa-thumbs-down cursor"></i>''  AS Description,
OP.bintCreatedDate as DueDate,
0 As TotalProgress,
dbo.fn_GetContactName('+CAST(@numUserCntID AS VARCHAR(200))+') As numAssignedTo,
'+CAST(@numUserCntID AS VARCHAR(200))+' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM  
 OpportunityMaster AS OP 
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	OP.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType = 1 AND (SELECT COUNT(OpportunityItems.numoppitemtCode) FROM OpportunityItems 
						WHERE OpportunityItems.bitItemPriceApprovalRequired=1 AND OpportunityItems.numOppId=OP.numOppId) >0
	AND '+CAST(@numUserCntID AS VARCHAR(100))+' IN (SELECT numUserID FROM UnitPriceApprover WHERE numDomainId= ' + Cast(@numDomainID as varchar(10))  + ' )) AS Q
	WHERE (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') '


------------------------10 - A/R Invoice Due------------------------------------
IF(@bitARInvoiceDue=1 AND @numUserCntID IN(SELECT * FROM Split(@vchARInvoiceDue,',') WHERE Items<>''))
BEGIN
 DECLARE  @AuthoritativeSalesBizDocId  AS INTEGER
    SELECT @AuthoritativeSalesBizDocId = isnull(numAuthoritativeSales,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId = @numDomainId;
SET @dynamicQuery=@dynamicQuery+  'UNION SELECT * FROM (SELECT
OM.numOppId AS RecordId,
OM.numCreatedBy AS numCreatedBy,
ISNULL(DM.numTerId,0) AS numOrgTerId,
OB.numOppBizDocsId AS numTaskId,
0 AS IsTaskClosed,
''<a href="javascript:void(0)"  onclick="openBizDoc(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'')">A/R Invoice Due</a>'' As TaskTypeName,
10 AS TaskType,
'''' AS OrigDescription,
''<b>For : </b> <a href="javascript:void(0)" class="underLineHyperLink" onclick="openBizDoc(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'')">''+CAST(OB.[vcBizDocID] AS VARCHAR)+''</a> of <a href="javascript:void(0)" class="underLineHyperLink" onclick="openSalesOrder(''+CAST(OM.numOppId AS VARCHAR)+'')">''+CAST(OM.[vcPOppName] AS VARCHAR)+''</a> <b>Balance Due</b> <a href="javascript:void(0)" onclick="OpenAmtPaid(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'',''+CAST(OM.[numDivisionId] AS VARCHAR)+'')"   class="underLineHyperLink">'' + CAST(ISNULL(C.[varCurrSymbol],'''') AS VARCHAR) + '''' + CAST(CAST(isnull(OB.monDealAmount  - ISNULL(TablePayments.monPaidAmount,0),0) AS DECIMAL(18,2)) AS VARCHAR)+''</a>''  AS Description,
 CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate),
                                                                   1)
                          WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],1)
                        END as DueDate,
0 As TotalProgress,
'''' As numAssignedTo,
0 As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(DM.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM   [OpportunityMaster] OM INNER JOIN [DivisionMaster] DM ON OM.[numDivisionId] = DM.[numDivisionID]
				LEFT JOIN CompanyInfo  Com ON DM.numCompanyID = Com.numCompanyId    
				LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
               LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN [Currency] C ON OM.[numCurrencyID] = C.[numCurrencyID]
			   OUTER APPLY
			   (
					SELECT 
						SUM(monAmountPaid) AS monPaidAmount
					FROM
						DepositeDetails DD
					INNER JOIN
						DepositMaster DM
					ON
						DD.numDepositID=DM.numDepositId
					WHERE 
						numOppID = OM.numOppID
						AND numOppBizDocsID=OB.numOppBizDocsID
						AND CONVERT(DATE,DM.dtDepositDate) <= DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +','''+Cast(@endDate as varchar(100))+''')
				) TablePayments
        WHERE  OM.[tintOppType] = 1
               AND OM.[tintOppStatus] = 1
               AND om.[numDomainId] = ' + Cast(@numDomainId as varchar(10))  + '
               AND OB.[numBizDocId] = '+CAST(@AuthoritativeSalesBizDocId As VARCHAR)+'
               AND OB.bitAuthoritativeBizDocs=1
               AND ISNULL(OB.tintDeferred,0) <> 1
			   AND (OM.numAccountClass= 0 OR 0=0)
			   AND CONVERT(DATE,OB.[dtCreatedDate]) <= DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +','''+Cast(@endDate as varchar(100))+''')
               AND isnull(OB.monDealAmount
                            ,0) > 0  AND isnull(OB.monDealAmount * OM.fltExchangeRate
                        - ISNULL(TablePayments.monPaidAmount,0) * OM.fltExchangeRate,0) != 0
	) AS Q
	WHERE (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') '
END
------------------------11 - A/P Bill Due------------------------------------
IF(@bitAPBillsDue=1 AND @numUserCntID IN(SELECT * FROM Split(@vchAPBillsDue,',') WHERE Items<>''))
BEGIN   
   DECLARE  @AuthoritativePurchaseBizDocId  AS INTEGER
    SELECT  @AuthoritativePurchaseBizDocId= isnull(numAuthoritativePurchase,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId =  @numDomainId
	

SET @dynamicQuery = @dynamicQuery + 'UNION SELECT * FROM (SELECT
OM.numOppId AS RecordId,
OM.numCreatedBy AS numCreatedBy,
ISNULL(DM.numTerId,0) AS numOrgTerId,
OB.numOppBizDocsId AS numTaskId,
0 AS IsTaskClosed,
''<a href="javascript:void(0)" onclick="openPurchaseBill(''+CAST(DM.[numDivisionID] As VARCHAR)+'')">A/P Bill Due<a/>'' As TaskTypeName,
11 AS TaskType,
'''' AS OrigDescription,
''<b>For : </b> <a href="javascript:void(0)" class="underLineHyperLink" onclick="openBizDoc(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'')">''+CAST(OB.[vcBizDocID] AS VARCHAR)+''</a> of <a href="javascript:void(0)" class="underLineHyperLink" onclick="openSalesOrder(''+CAST(OM.numOppId AS VARCHAR)+'')">''+CAST(OM.[vcPOppName] AS VARCHAR)+''</a> <b>Balance Due : </b> <a href="javascript:void(0)" onclick="openBizDoc(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'')"   class="underLineHyperLink">'' + CAST(ISNULL(C.[varCurrSymbol],'''') AS VARCHAR) + '''' + CAST(CAST(isnull((OB.monDealAmount * OM.fltExchangeRate)  - (ISNULL(TablePayment.monPaidAmount,0)* OM.fltExchangeRate),0) AS DECIMAL(18,2))  AS VARCHAR)+''</a>''  AS Description,
 CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate),
                                                                   1)
                          WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],1)
                        END as DueDate,
0 As TotalProgress,
'''' As numAssignedTo,
0 As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(DM.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

   FROM   [OpportunityMaster] OM
               INNER JOIN [DivisionMaster] DM
                 ON OM.[numDivisionId] = DM.[numDivisionID]
				 LEFT JOIN CompanyInfo  Com ON DM.numCompanyID = Com.numCompanyId    
				LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
               LEFT OUTER JOIN OpportunityBizDocs OB
                 ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN [Currency] C
                 ON OM.[numCurrencyID] = C.[numCurrencyID]
				 OUTER APPLY
				(
					SELECT
						SUm(BPD.monAmount) monPaidAmount
					FROM	
						BillPaymentDetails BPD
					INNER JOIN
						BillPaymentHeader BPH
					ON
						BPD.numBillPaymentID=BPH.numBillPaymentID
					WHERE
						BPD.numOppBizDocsID = OB.numOppBizDocsId
						AND CAST(BPH.dtPaymentDate AS DATE) <= DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +','''+Cast(@endDate as varchar(100))+''')
				) TablePayment
        WHERE  OM.[tintOppType] = 2
               AND OM.[tintOppStatus] = 1
               AND om.[numDomainId] = 1
               AND OB.[numBizDocId] = '+CAST(@AuthoritativePurchaseBizDocId AS VARCHAR)+' 
               AND OB.bitAuthoritativeBizDocs=1
               AND ISNULL(OB.tintDeferred,0) <> 1
			   AND (OM.numAccountClass= 0 OR 0=0)
			   AND CONVERT(DATE,OB.[dtCreatedDate]) <= DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +','''+Cast(@endDate as varchar(100))+''')
               AND  isnull(OB.monDealAmount * OM.fltExchangeRate,0) > 0 
			   AND isnull(OB.monDealAmount * OM.fltExchangeRate
                        - ISNULL(TablePayment.monPaidAmount,0) * OM.fltExchangeRate,0) > 0
	) AS Q
	WHERE (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') '
END
IF(@RegularSearchCriteria='')
BEGIN
	SET @RegularSearchCriteria =@RegularSearchCriteria + ' 1=1'
END
if(@OppStatus=0)
BEGIN
SET @RegularSearchCriteria =@RegularSearchCriteria + ' AND IsTaskClosed = 0'
END
if(@OppStatus=1)
BEGIN
SET @RegularSearchCriteria = @RegularSearchCriteria + ' AND IsTaskClosed = 1'
END


SET @RegularSearchCriteria  = REPLACE(@RegularSearchCriteria,'TaskTypeName','TaskType')

SET @dynamicQuery = 'SELECT COUNT(*) OVER() TotalRecords,T.RecordId,T.numCreatedBy,T.numOrgTerId,T.numTaskId,T.IsTaskClosed,T.TaskTypeName,T.TaskType,T.OrigDescription,T.Description,T.TotalProgress,T.numAssignedTo,T.numAssignToId,T.Priority,T.Activity,T.OrgName,T.CompanyRating
,CASE WHEN convert(varchar(11),T.DueDate)=convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate())) then ''<b><font color=red>Today</font></b>''
WHEN convert(varchar(11),T.DueDate)=convert(varchar(11),DateAdd(day,-1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then ''<b><font color=purple>Yesterday</font></b>''
WHEN convert(varchar(11),T.DueDate)=convert(varchar(11),DateAdd(day,1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then ''<b><font color=orange>Tommorow</font></b>''
ELSE  dbo.FormatedDateFromDate(T.DueDate,'+ CONVERT(VARCHAR(10),@numDomainId)+') END
AS DueDate FROM ( ' + @dynamicQuery + ') T WHERE '+@RegularSearchCriteria+' ORDER BY T.DueDate OFFSET '+CAST(((@CurrentPage - 1 ) * @PageSize) AS VARCHAR) +' ROWS FETCH NEXT '+CAST(@PageSize AS VARCHAR)+' ROWS ONLY'

PRINT CAST(@dynamicPartQuery As NVARCHAR(MAX))
EXEC(@dynamicQuery)
DECLARE @TempColumns TABLE
	(
        vcFieldName VARCHAR(200)
        ,vcDbColumnName VARCHAR(200)
        ,vcOrigDbColumnName VARCHAR(200)
        ,bitAllowSorting BIT
        ,numFieldId INT
        ,bitAllowEdit BIT
        ,bitCustomField BIT
        ,vcAssociatedControlType VARCHAR(50)
        ,vcListItemType  VARCHAR(200)
        ,numListID INT
        ,ListRelID INT
        ,vcLookBackTableName VARCHAR(200)
        ,bitAllowFiltering BIT
        ,vcFieldDataType  VARCHAR(20)
		,intColumnWidth INT
		,bitClosedColumn BIT
		,bitFieldMessage BIT DEFAULT 0
		,vcFieldMessage VARCHAR(500)
		,bitIsRequired BIT DEFAULT 0
		,bitIsNumeric BIT DEFAULT 0
		,bitIsAlphaNumeric BIT DEFAULT 0
		,bitIsEmail BIT DEFAULT 0
		,bitIsLengthValidation  BIT DEFAULT 0
		,intMaxLength INT DEFAULT 0
		,intMinLength INT DEFAULT 0
	)

INSERT INTO @TempColumns
		(
			vcFieldName
			,vcDbColumnName
			,vcOrigDbColumnName
			,bitAllowSorting
			,numFieldId
			,bitAllowEdit
			,bitCustomField
			,vcAssociatedControlType
			,vcListItemType
			,numListID
			,ListRelID
			,vcLookBackTableName
			,bitAllowFiltering
			,vcFieldDataType
			,bitClosedColumn
			
		)
		VALUES
		('Type','TaskTypeName','TaskTypeName',0,1,0,0,'SelectBox','',0,0,'T',1,'V',0)
		,('Descriptions & Comments','Description','Description',0,184,1,0,'TextArea','',0,0,'T',1,'V',0)
		,('Due Date','DueDate','DueDate',0,3,0,0,'DateField','',0,0,'T',0,'V',0)
		,('Total Progress','TotalProgress','TotalProgress',1,4,0,0,'TextBox','',0,0,'T',0,'V',0)
		,('Assigned to','numAssignedTo','numAssignedTo',0,5,0,0,'SelectBox','',0,0,'T',1,'V',0)
		,('Priority','Priority','Priority',0,183,1,0,'SelectBox','',0,0,'T',1,'V',0)
		,('Activity','Activity','Activity',0,182,1,0,'SelectBox','',0,0,'T',1,'V',0)
		,('Organization (Rating)','OrgName','OrgName',0,8,0,0,'TextBox','',0,0,'T',1,'V',0)


UPDATE
		TC
	SET
		TC.intColumnWidth = ISNULL(DFCD.intColumnWidth,0)
	FROM
		@TempColumns TC
	INNER JOIN
		DycFormConfigurationDetails DFCD
	ON
		TC.numFieldId = DFCD.numFieldId
	WHERE
		DFCD.numDomainId = @numDomainID
		AND DFCD.numUserCntID = @numUserCntID
		AND DFCD.numFormId = 43
		AND DFCD.tintPageType = 1


	SELECT * FROM @TempColumns
END
GO

