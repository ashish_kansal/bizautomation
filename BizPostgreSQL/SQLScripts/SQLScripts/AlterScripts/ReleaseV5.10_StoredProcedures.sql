/******************************************************************
Project: Release 5.10 Date: 17.JULY.2016
Comments: STORED PROCEDURES
*******************************************************************/

/****** Object:  StoredProcedure [dbo].[USP_GetItemsAndKits]    Script Date: 07/26/2008 16:17:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getitemsandkits')
DROP PROCEDURE usp_getitemsandkits
GO
CREATE PROCEDURE [dbo].[USP_GetItemsAndKits]        
@numDomainID as numeric(9)=0,
@strItem as varchar(10),
@type AS int,
@bitAssembly AS BIT = 0,
@numItemCode AS NUMERIC(18,0) = 0      
as   
IF @type=0 
BEGIN   
	select numItemCode,vcItemName from Item where numDomainID =@numDomainID and bitAssembly=@bitAssembly  and vcItemName like @strItem+'%'
END

ELSE IF @type=1 
BEGIN   
	IF (SELECT ISNULL(bitAssembly,0) FROM Item  WHERE numItemCode = @numItemCode) = 1
	BEGIN
		--ONLY NON INVENTORY, SERVICE AND INVENTORY ITEMS WITH PRIMARY VENDOR CAN BE ADDED TO ASSEMBLY
		SELECT 
			numItemCode,vcItemName,bitKitParent,bitAssembly 
		FROM 
			Item 
		WHERE 
			Item.numDomainID =@numDomainID
			AND vcItemName like @strItem+'%' 
			AND ISNULL(bitLotNo,0)<>1 
			AND ISNULL(bitSerialized,0)<>1 
			AND ISNULL(bitKitParent,0)<>1 /*and ISNULL(bitAssembly,0)<>1 and  */
			AND ISNULL(bitRental,0) <> 1
			AND ISNULL(bitAsset,0) <> 1
			AND 1=(CASE 
				   WHEN charitemtype='P' THEN
						CASE 
							WHEN ((SELECT COUNT(numWareHouseItemID) FROM WareHouseItems WHERE numDomainID=Item.numDomainID AND numItemID=Item.numItemCode) > 0 AND 
							     ((SELECT COUNT(numDivisionID) FROM DivisionMaster WHERE numDivisionID=Item.numVendorID) > 0 OR ISNULL(bitAssembly,0) = 1)) THEN 1 
							ELSE 
								0 
						END 
					ELSE 
						1 
					END) 
			AND numItemCode NOT IN (@numItemCode)
			AND bitAssembly=@bitAssembly
	END
	ELSE
	BEGIN
		--ONLY NON INVENTORY, SERVICE, INVENTORY ITEMS AND KIT ITEMS WITHOUT HAVING ANOTHER KIT AS SUB ITEM WITH PRIMARY VENDOR CAN BE ADDED TO ASSEMBLY

		SELECT 
			numItemCode,
			vcItemName,
			bitKitParent,
			bitAssembly 
		FROM 
			Item 
		WHERE 
			numDomainID =@numDomainID
			AND vcItemName like @strItem+'%' 
			AND bitAssembly=@bitAssembly
			AND ISNULL(bitLotNo,0)<>1 and ISNULL(bitSerialized,0)<>1 
			AND numItemCode <> @numItemCode
			AND 1= (
					CASE 
						WHEN charitemtype='P' THEN
						CASE 
							WHEN (SELECT COUNT(numWareHouseItemID) FROM WareHouseItems WHERE numDomainID=Item.numDomainID AND numItemID=Item.numItemCode)>0 
							THEN 1 
							ELSE 0 
						END 
						ELSE 1 
					END)
			AND 1 = (CASE 
						WHEN Item.bitKitParent = 1 
						THEN 
							(
								CASE 
									WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item I ON ItemDetails.numChildItemID = I.numItemCode WHERE numItemKitID=Item.numItemCode AND ISNULL(I.bitKitParent,0) = 1) > 0
									THEN 0
									ELSE 1
								END
							)
						ELSE 1 
					END)
	END
END				
GO





/****** Object:  StoredProcedure [dbo].[USP_ShoppingCart]    Script Date: 05/07/2009 22:09:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_shoppingcart')
DROP PROCEDURE usp_shoppingcart
GO
CREATE PROCEDURE [dbo].[USP_ShoppingCart]                            
@numDivID as numeric(18),                            
@numOppID as numeric(18)=null output ,                                              
@vcPOppName as varchar(200)='' output,  
@numContactId as numeric(18),                            
@numDomainId as numeric(18),
@vcSource VARCHAR(50)=NULL,
@numCampainID NUMERIC(9)=0,
@numCurrencyID as numeric(9),
@numBillAddressId numeric(9),
@numShipAddressId numeric(9),
@bitDiscountType BIT,
@fltDiscount FLOAT,
@numProId NUMERIC(9),
@numSiteID NUMERIC(9),
@tintOppStatus INT,
@monShipCost MONEY,
@tintSource AS BIGINT ,
@tintSourceType AS TINYINT ,                          
@numPaymentMethodId NUMERIC(9),
@txtComments varchar(1000),
@numPartner NUMERIC(18,0)

as                            
declare @CRMType as integer               
declare @numRecOwner as integer                            
if @numOppID=  0                             
begin                            
select @CRMType=tintCRMType,@numRecOwner=numRecOwner from DivisionMaster where numDivisionID=@numDivID                            
if @CRMType= 0                             
begin                            
UPDATE DivisionMaster                               
   SET tintCRMType=1                              
   WHERE numDivisionID=@numDivID                              
                                                   
end                            
declare @TotalAmount as FLOAT
--declare @intOppTcode as numeric(9) 
--declare @numCurrencyID as numeric(9)
--declare @fltExchangeRate as float                          
--Select @intOppTcode=max(numOppId)from OpportunityMaster                              
--set @intOppTcode =isnull(@intOppTcode,0) + 1                              

DECLARE @numTemplateID NUMERIC
SELECT  @numTemplateID= numMirrorBizDocTemplateId FROM eCommercePaymentConfig WHERE  numSiteId=@numSiteID AND numPaymentMethodId = @numPaymentMethodId AND numDomainID = @numDomainID   

   Declare @fltExchangeRate float                                 
    
   IF @numCurrencyID=0 select @numCurrencyID=isnull(numCurrencyID,0) from Domain where numDomainID=@numDomainId                            
   if @numCurrencyID=0 set   @fltExchangeRate=1
  else set @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)                          
                            
  
	 -- GET ACCOUNT CLASS IF ENABLED
	DECLARE @numAccountClass AS NUMERIC(18) = 0

	DECLARE @tintDefaultClassType AS INT = 0
	SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

	IF @tintDefaultClassType = 1 --USER
	BEGIN
		SELECT 
			@numAccountClass=ISNULL(numDefaultClass,0) 
		FROM 
			dbo.UserMaster UM 
		JOIN 
			dbo.Domain D 
		ON 
			UM.numDomainID=D.numDomainId
		WHERE 
			D.numDomainId=@numDomainId 
			AND UM.numUserDetailId=@numContactId
	END
	ELSE IF @tintDefaultClassType = 2 --COMPANY
	BEGIN
		SELECT 
			@numAccountClass=ISNULL(numAccountClassID,0) 
		FROM 
			dbo.DivisionMaster DM 
		WHERE 
			DM.numDomainId=@numDomainId 
			AND DM.numDivisionID=@numDivID
	END
	ELSE
	BEGIN
		SET @numAccountClass = 0
	END
    SET @numAccountClass=(SELECT 
					      TOP 1 
						  numDefaultClass from dbo.eCommerceDTL 
						  where 
						  numSiteId=@numSiteID 
						  and numDomainId=@numDomainId)                  
--set @vcPOppName=@vcPOppName+ '-'+convert(varchar(4),year(getutcdate()))  +'-A'+ convert(varchar(10),@intOppTcode)                              
  --SELECT tintSource , tintSourceType FROM dbo.OpportunityMaster
insert into OpportunityMaster                              
  (                              
  numContactId,                              
  numDivisionId,                              
  txtComments,                              
  numCampainID,                              
  bitPublicFlag,                              
  tintSource, 
  tintSourceType ,                               
  vcPOppName,                              
  monPAmount,                              
  numCreatedBy,                              
  bintCreatedDate,                               
  numModifiedBy,                              
  bintModifiedDate,                              
  numDomainId,                               
  tintOppType, 
  tintOppStatus,                   
  intPEstimatedCloseDate,              
  numRecOwner,
  bitOrder,
  numCurrencyID,
  fltExchangeRate,fltDiscountTotal,bitDiscountTypeTotal,
  monShipCost,
  numOppBizDocTempID,numAccountClass,numPartner
  )                              
 Values                              
  (                              
  @numContactId,                              
  @numDivID,                              
  @txtComments,                              
  @numCampainID,--  0,
  0,                              
  @tintSource,   
  @tintSourceType,                           
  ISNULL(@vcPOppName,'SO'),                             
  0,                                
  @numContactId,                              
  getutcdate(),                              
  @numContactId,                              
  getutcdate(),        
  @numDomainId,                              
  1,             
  @tintOppStatus,       
  getutcdate(),                                 
  @numRecOwner ,
  1,
  @numCurrencyID,
  @fltExchangeRate,@fltDiscount,@bitDiscountType
  ,@monShipCost,
  @numTemplateID,@numAccountClass,@numPartner
  
  )                               
set @numOppID=scope_identity()                             
--Update OppName as per Name Template
EXEC dbo.USP_UpdateNameTemplateValue 1,@numDomainId,@numOppID
SELECT @vcPOppName = vcPOppName FROM OpportunityMaster WHERE numOppID= @numOppID
end
delete  from    OpportunityItems where   numOppId= @numOppID                               

--if convert(varchar(10),@strItems) <>''                
--begin               
-- DECLARE @hDocItem int                
-- EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                
                
UPDATE 
	P
SET
	intCouponCodeUsed=ISNULL(intCouponCodeUsed,0)+1
FROM 
	PromotionOffer AS P
LEFT JOIN
	CartItems AS C
ON
	P.numProId=C.PromotionID
WHERE
	C.vcCoupon=P.txtCouponCode AND 
	(C.fltDiscount>0 OR C.numCartId IN (SELECT numCartId FROM CartItems AS CG WHERE CG.PromotionID=P.numProId AND CG.fltDiscount>0))
	AND numUserCntId =@numContactId


insert into OpportunityItems                                                        
  (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,vcAttrValues,[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage, monVendorCost,numUOMId,bitDiscountType,fltDiscount,monTotAmtBefDiscount,numPromotionID,bitPromotionTriggered)                                                        
  --kishan                                                       
 select @numOppID,X.numItemCode,x.numUnitHour * x.decUOMConversionFactor,x.monPrice/x.decUOMConversionFactor,x.monTotAmount,X.vcItemDesc, NULLIF(X.numWarehouseItmsID,0),X.vcItemType,X.vcAttributes,X.vcAttrValues,(SELECT vcItemName FROM item WHERE numItemCode = X.numItemCode),(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
		(SELECT TOP 1  vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND bitDefault = 1 AND numDomainId = @numDomainId),   (select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID), 
		numUOM,bitDiscountType,fltDiscount,monTotAmtBefDiscount,X.PromotionID,X.bitParentPromotion
FROM dbo.CartItems X WHERE numUserCntId =@numContactId

declare @tintShipped as tinyint      
DECLARE @tintOppType AS TINYINT
      
select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID              
if @tintOppStatus=1               
	begin         
	PRINT 'inside USP_UpdatingInventoryonCloseDeal'
		exec USP_UpdatingInventoryonCloseDeal @numOppID,@numContactId
	end              

/*Commented by Chintan..reason:tobe executed only when order is confirmed*/
--exec USP_UpdatingInventoryonCloseDeal @numOppID,@numContactId

-- SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/Table',2)                                                        
-- WITH  (                                                        
--  numoppitemtCode numeric(9),                   
--  numItemCode numeric(9),                                                        
--  numUnitHour numeric(9),                                                        
--  monPrice money,                                                     
--  monTotAmount money,                                                        
--  vcItemDesc varchar(1000),                  
--  numWarehouseItmsID numeric(9),        
--  vcItemType varchar(30),    
--  vcAttrValues varchar(50) ,
--  numUOM NUMERIC(18,0),
--  decUOMConversionFactor DECIMAL(18,5),bitDiscountType BIT,fltDiscount FLOAT,monTotAmtBefDiscount MONEY,
--  vcShippingMethod VARCHAR(100),decShippingCharge MONEY,dtDeliveryDate datetime
--  ))X                
--                
                
-- EXEC sp_xml_removedocument @hDocItem                
                
--end
                             
                            
select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@numOppID                            
 update OpportunityMaster set  monPamount=@TotalAmount - @fltDiscount                           
 where numOppId=@numOppID                           
                          
--set @vcPOppName=(select  ISNULL(vcPOppName,numOppId) from OpportunityMaster where numOppId=@numOppID )

IF(@vcSource IS NOT NULL)
BEGIN
	insert into OpportunityLinking ([numParentOppID],[numChildOppID],[vcSource],[numParentOppBizDocID],numPromotionId,numSiteID)  values(null,@numOppID,@vcSource,NULL,@numProId,@numSiteID);
END

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9) 
DECLARE @bitIsPrimary BIT;

--Bill Address
IF @numBillAddressId>0
BEGIN
 SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numBillAddressId 
            
  
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = '', --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName =@vcAddressName
	
        
END
  
  
  --Ship Address
IF @numShipAddressId>0
BEGIN
   	SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numShipAddressId 
  
  
	select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
	join divisionMaster Div                            
	on div.numCompanyID=com.numCompanyID                            
	where div.numdivisionID=@numDivID

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName =@vcAddressName
	
   	
END

--Insert Tax for Division                       
INSERT dbo.OpportunityMasterTaxItems 
(
	numOppId,
	numTaxItemID,
	fltPercentage,
	tintTaxType,
	numTaxID
) 
SELECT 
	@numOppID,
	TI.numTaxItemID,
	TEMPTax.decTaxValue,
	TEMPTax.tintTaxType,
	numTaxID
FROM 
	TaxItems TI 
JOIN 
	DivisionTaxTypes DTT 
ON 
	TI.numTaxItemID = DTT.numTaxItemID
OUTER APPLY
(
	SELECT
		decTaxValue,
		tintTaxType,
		numTaxID
	FROM
		dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,TI.numTaxItemID,@numOppId,0,NULL)
) AS TEMPTax
WHERE 
	DTT.numDivisionID=@numDivID AND DTT.bitApplicable=1
UNION 
SELECT 
	@numOppID,
	0,
	TEMPTax.decTaxValue,
	TEMPTax.tintTaxType,
	numTaxID
FROM 
	dbo.DivisionMaster 
OUTER APPLY
(
	SELECT
		decTaxValue,
		tintTaxType,
		numTaxID
	FROM
		dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,0,@numOppId,1,NULL)
) AS TEMPTax
WHERE 
	bitNoTax=0 
	AND numDivisionID=@numDivID

-- DELETE ITEM LEVEL CRV TAX TYPES
DELETE FROM OpportunityMasterTaxItems WHERE numOppId=@numOppId AND numTaxItemID = 1

-- INSERT ITEM LEVEL CRV TAX TYPES
INSERT INTO OpportunityMasterTaxItems
(
	numOppId,
	numTaxItemID,
	fltPercentage,
	tintTaxType,
	numTaxID
) 
SELECT 
	OI.numOppId,
	1,
	TEMPTax.decTaxValue,
	TEMPTax.tintTaxType,
	TEMPTax.numTaxID
FROM 
	OpportunityItems OI
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
OUTER APPLY
(
	SELECT
		decTaxValue,
		tintTaxType,
		numTaxID
	FROM
		dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,OI.numItemCode,IT.numTaxItemID,@numOppId,1,NULL)
) AS TEMPTax
WHERE
	OI.numOppId = @numOppID
	AND IT.numTaxItemID = 1 -- CRV TAX
GROUP BY
	OI.numOppId,
	TEMPTax.decTaxValue,
	TEMPTax.tintTaxType,
	TEMPTax.numTaxID
  
  --Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID
) 
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0 
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID=IT.numTaxItemID 
WHERE 
	OI.numOppId=@numOppID 
	AND IT.bitApplicable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	0,
	0 
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numOppID 
	AND I.bitTaxable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT
	@numOppId,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
FROM
	dbo.OpportunityItems OI 
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
INNER JOIN
	TaxDetails TD
ON
	TD.numTaxID = IT.numTaxID
	AND TD.numDomainId = @numDomainId
WHERE
	OI.numOppId = @numOppID
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
  
 

 /****** Object:  StoredProcedure [dbo].[USP_UpdatedomainDetails]    Script Date: 05/07/2009 21:56:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatedomaindetails')
DROP PROCEDURE usp_updatedomaindetails
GO
CREATE PROCEDURE [dbo].[USP_UpdatedomainDetails]                                      
@numDomainID as numeric(9)=0,                                      
@vcDomainName as varchar(50)='',                                      
@vcDomainDesc as varchar(100)='',                                      
@bitExchangeIntegration as bit,                                      
@bitAccessExchange as bit,                                      
@vcExchUserName as varchar(100)='',                                      
@vcExchPassword as varchar(100)='',                                      
@vcExchPath as varchar(200)='',                                      
@vcExchDomain as varchar(100)='',                                    
@tintCustomPagingRows as tinyint,                                    
@vcDateFormat as varchar(30)='',                                    
@numDefCountry as numeric(9),                                    
@tintComposeWindow as tinyint,                                    
@sintStartDate as smallint,                                    
@sintNoofDaysInterval as smallint,                                    
@tintAssignToCriteria as tinyint,                                    
@bitIntmedPage as bit,                                    
@tintFiscalStartMonth as tinyint,                                      
@tintPayPeriod as tinyint,                        
@numCurrencyID as numeric(9) ,                
@charUnitSystem as char(1)='',              
@vcPortalLogo as varchar(100)='',            
@tintChrForComSearch as tinyint  ,            
@intPaymentGateWay as int,    
@tintChrForItemSearch as tinyint,
@ShipCompany as numeric(9),
@bitAutoPopulateAddress as bit=null,
@tintPoulateAddressTo as tinyint=null,
@bitMultiCurrency as bit,
@bitCreateInvoice AS tinyint = null,
@numDefaultSalesBizDocID AS NUMERIC(9)=NULL,
@numDefaultPurchaseBizDocID AS NUMERIC(9)=NULL,
@bitSaveCreditCardInfo AS BIT = NULL,
@intLastViewedRecord AS INT=10,
@tintCommissionType AS TINYINT,
@tintBaseTax AS TINYINT,
@bitEmbeddedCost AS BIT,
@numDefaultSalesOppBizDocId NUMERIC=NULL,
@tintSessionTimeOut TINYINT=1,
@tintDecimalPoints TINYINT,
@bitCustomizePortal BIT=0,
@tintBillToForPO TINYINT,
@tintShipToForPO TINYINT,
@tintOrganizationSearchCriteria TINYINT,
@bitDocumentRepositary BIT=0,
@tintComAppliesTo TINYINT=0,
@bitGtoBContact BIT=0,
@bitBtoGContact BIT=0,
@bitGtoBCalendar BIT=0,
@bitBtoGCalendar BIT=0,
@bitExpenseNonInventoryItem BIT=0,
@bitInlineEdit BIT=0,
@bitRemoveVendorPOValidation BIT=0,
@tintBaseTaxOnArea tinyint=0,
@bitAmountPastDue BIT=0,
@monAmountPastDue money,
@bitSearchOrderCustomerHistory BIT=0,
@bitRentalItem as bit=0,
@numRentalItemClass as NUMERIC(9)=NULL,
@numRentalHourlyUOM as NUMERIC(9)=NULL,
@numRentalDailyUOM as NUMERIC(9)=NULL,
@tintRentalPriceBasedOn as tinyint=NULL,
@tintCalendarTimeFormat as tinyint=NULL,
@tintDefaultClassType as tinyint=NULL,
@tintPriceBookDiscount as tinyint=0,
@bitAutoSerialNoAssign as bit=0,
@bitIsShowBalance AS BIT = 0,
@numIncomeAccID NUMERIC(18, 0) = 0,
@numCOGSAccID NUMERIC(18, 0) = 0,
@numAssetAccID NUMERIC(18, 0) = 0,
@IsEnableProjectTracking AS BIT = 0,
@IsEnableCampaignTracking AS BIT = 0,
@IsEnableResourceScheduling AS BIT = 0,
@ShippingServiceItem  AS NUMERIC(9)=0,
@numSOBizDocStatus AS NUMERIC(9)=0,
@bitAutolinkUnappliedPayment AS BIT=0,
@numDiscountServiceItemID AS NUMERIC(9)=0,
@vcShipToPhoneNumber as VARCHAR(50)='',
@vcGAUserEmailId as VARCHAR(50)='',
@vcGAUserPassword as VARCHAR(50)='',
@vcGAUserProfileId as VARCHAR(50)='',
@bitDiscountOnUnitPrice AS BIT=0,
@intShippingImageWidth INT = 0,
@intShippingImageHeight INT = 0,
@numTotalInsuredValue NUMERIC(10,2) = 0,
@numTotalCustomsValue NUMERIC(10,2) = 0,
@vcHideTabs VARCHAR(1000) = '',
@bitUseBizdocAmount BIT = 0,
@bitDefaultRateType BIT = 0,
@bitIncludeTaxAndShippingInCommission  BIT = 0,
@bitPurchaseTaxCredit BIT=0,
@bitLandedCost BIT=0,
@vcLanedCostDefault VARCHAR(10)='',
@bitMinUnitPriceRule BIT = 0,
@numAbovePercent AS NUMERIC(18,2),
@numAbovePriceField AS NUMERIC(18,0),
@numBelowPercent AS NUMERIC(18,2),
@numBelowPriceField AS NUMERIC(18,0),
@numOrderStatusBeforeApproval AS NUMERIC(18,0),
@numOrderStatusAfterApproval AS NUMERIC(18,0),
@vcUnitPriceApprover AS VARCHAR(100),
@numDefaultSalesPricing AS TINYINT,
@bitReOrderPoint AS BIT = 0,
@numReOrderPointOrderStatus AS NUMERIC(18,0) = 0,
@numPODropShipBizDoc AS NUMERIC(18,0) = 0,
@numPODropShipBizDocTemplate AS NUMERIC(18,0) = 0,
@IsEnableDeferredIncome AS BIT = 0,
@bitApprovalforTImeExpense AS BIT=0,
@numCost AS Numeric(9,0)=0,
@intTimeExpApprovalProcess int=0,
@bitApprovalforOpportunity AS BIT=0,
@intOpportunityApprovalProcess int=0,
@numDefaultSalesShippingDoc NUMERIC(18,0)=0,
@numDefaultPurchaseShippingDoc NUMERIC(18,0)=0,
@bitchkOverRideAssignto AS BIT=0
as                                      
                                      
 update Domain                                       
   set                                       
   vcDomainName=@vcDomainName,                                      
   vcDomainDesc=@vcDomainDesc,                                      
   bitExchangeIntegration=@bitExchangeIntegration,                                      
   bitAccessExchange=@bitAccessExchange,                                      
   vcExchUserName=@vcExchUserName,                                      
   vcExchPassword=@vcExchPassword,                                      
   vcExchPath=@vcExchPath,                                      
   vcExchDomain=@vcExchDomain,                                    
   tintCustomPagingRows =@tintCustomPagingRows,                                    
   vcDateFormat=@vcDateFormat,                                    
   numDefCountry =@numDefCountry,                                    
   tintComposeWindow=@tintComposeWindow,                                    
   sintStartDate =@sintStartDate,                                    
   sintNoofDaysInterval=@sintNoofDaysInterval,                                    
   tintAssignToCriteria=@tintAssignToCriteria,                                    
   bitIntmedPage=@bitIntmedPage,                                    
   tintFiscalStartMonth=@tintFiscalStartMonth,                            
--   bitDeferredIncome=@bitDeferredIncome,                          
   tintPayPeriod=@tintPayPeriod,
   vcCurrency=isnull((select varCurrSymbol from Currency Where numCurrencyID=@numCurrencyID  ),'')  ,                      
  numCurrencyID=@numCurrencyID,                
  charUnitSystem= @charUnitSystem,              
  vcPortalLogo=@vcPortalLogo ,            
  tintChrForComSearch=@tintChrForComSearch  ,      
  intPaymentGateWay=@intPaymentGateWay,
 tintChrForItemSearch=@tintChrForItemSearch,
 numShipCompany= @ShipCompany,
 bitAutoPopulateAddress = @bitAutoPopulateAddress,
 tintPoulateAddressTo =@tintPoulateAddressTo,
 --bitMultiCompany=@bitMultiCompany ,
 bitMultiCurrency=@bitMultiCurrency,
 bitCreateInvoice= @bitCreateInvoice,
 [numDefaultSalesBizDocId] =@numDefaultSalesBizDocId,
 bitSaveCreditCardInfo=@bitSaveCreditCardInfo,
 intLastViewedRecord=@intLastViewedRecord,
 [tintCommissionType]=@tintCommissionType,
 [tintBaseTax]=@tintBaseTax,
 [numDefaultPurchaseBizDocId]=@numDefaultPurchaseBizDocID,
 bitEmbeddedCost=@bitEmbeddedCost,
 numDefaultSalesOppBizDocId=@numDefaultSalesOppBizDocId,
 tintSessionTimeOut=@tintSessionTimeOut,
 tintDecimalPoints=@tintDecimalPoints,
 bitCustomizePortal=@bitCustomizePortal,
 tintBillToForPO = @tintBillToForPO,
 tintShipToForPO = @tintShipToForPO,
 tintOrganizationSearchCriteria=@tintOrganizationSearchCriteria,
-- vcPortalName=@vcPortalName,
 bitDocumentRepositary=@bitDocumentRepositary,
 --tintComAppliesTo=@tintComAppliesTo,THIS FIELD IS REMOVED FROM GLOBAL SETTINGS
 bitGtoBContact=@bitGtoBContact,
 bitBtoGContact=@bitBtoGContact,
 bitGtoBCalendar=@bitGtoBCalendar,
 bitBtoGCalendar=@bitBtoGCalendar,
 bitExpenseNonInventoryItem=@bitExpenseNonInventoryItem,
 bitInlineEdit=@bitInlineEdit,
 bitRemoveVendorPOValidation=@bitRemoveVendorPOValidation,
tintBaseTaxOnArea=@tintBaseTaxOnArea,
bitAmountPastDue = @bitAmountPastDue,
monAmountPastDue = @monAmountPastDue,
bitSearchOrderCustomerHistory=@bitSearchOrderCustomerHistory,
bitRentalItem=@bitRentalItem,
numRentalItemClass=@numRentalItemClass,
numRentalHourlyUOM=@numRentalHourlyUOM,
numRentalDailyUOM=@numRentalDailyUOM,
tintRentalPriceBasedOn=@tintRentalPriceBasedOn,
tintCalendarTimeFormat=@tintCalendarTimeFormat,
tintDefaultClassType=@tintDefaultClassType,
tintPriceBookDiscount=@tintPriceBookDiscount,
bitAutoSerialNoAssign=@bitAutoSerialNoAssign,
bitIsShowBalance = @bitIsShowBalance,
numIncomeAccID = @numIncomeAccID,
numCOGSAccID = @numCOGSAccID,
numAssetAccID = @numAssetAccID,
IsEnableProjectTracking = @IsEnableProjectTracking,
IsEnableCampaignTracking = @IsEnableCampaignTracking,
IsEnableResourceScheduling = @IsEnableResourceScheduling,
numShippingServiceItemID = @ShippingServiceItem,
numSOBizDocStatus=@numSOBizDocStatus,
bitAutolinkUnappliedPayment=@bitAutolinkUnappliedPayment,
numDiscountServiceItemID=@numDiscountServiceItemID,
vcShipToPhoneNo = @vcShipToPhoneNumber,
vcGAUserEMail = @vcGAUserEmailId,
vcGAUserPassword = @vcGAUserPassword,
vcGAUserProfileId = @vcGAUserProfileId,
bitDiscountOnUnitPrice=@bitDiscountOnUnitPrice,
intShippingImageWidth = @intShippingImageWidth ,
intShippingImageHeight = @intShippingImageHeight,
numTotalInsuredValue = @numTotalInsuredValue,
numTotalCustomsValue = @numTotalCustomsValue,
vcHideTabs = @vcHideTabs,
bitUseBizdocAmount = @bitUseBizdocAmount,
bitDefaultRateType = @bitDefaultRateType,
bitIncludeTaxAndShippingInCommission = @bitIncludeTaxAndShippingInCommission,
/*,bitAllowPPVariance=@bitAllowPPVariance*/
bitPurchaseTaxCredit=@bitPurchaseTaxCredit,
bitLandedCost=@bitLandedCost,
vcLanedCostDefault=@vcLanedCostDefault,
bitMinUnitPriceRule = @bitMinUnitPriceRule,
numAbovePercent = @numAbovePercent,
numAbovePriceField = @numAbovePriceField,
numBelowPercent = @numBelowPercent,
numBelowPriceField = @numBelowPriceField,
numOrderStatusBeforeApproval = @numOrderStatusBeforeApproval,
numOrderStatusAfterApproval = @numOrderStatusAfterApproval,
numDefaultSalesPricing = @numDefaultSalesPricing,
bitReOrderPoint=@bitReOrderPoint,
numReOrderPointOrderStatus=@numReOrderPointOrderStatus,
numPODropShipBizDoc=@numPODropShipBizDoc,
numPODropShipBizDocTemplate=@numPODropShipBizDocTemplate,
IsEnableDeferredIncome=@IsEnableDeferredIncome,
bitApprovalforTImeExpense=@bitApprovalforTImeExpense,
numCost= @numCost,intTimeExpApprovalProcess=@intTimeExpApprovalProcess,
bitApprovalforOpportunity=@bitApprovalforOpportunity,intOpportunityApprovalProcess=@intOpportunityApprovalProcess,
 numDefaultPurchaseShippingDoc=@numDefaultPurchaseShippingDoc,numDefaultSalesShippingDoc=@numDefaultSalesShippingDoc,
 bitchkOverRideAssignto=@bitchkOverRideAssignto
 where numDomainId=@numDomainID

DELETE FROM UnitPriceApprover WHERE numDomainID = @numDomainID

INSERT INTO UnitPriceApprover
	(
		numDomainID,
		numUserID
	)
SELECT
     @numDomainID,
	 Split.a.value('.', 'VARCHAR(100)') AS String
FROM  
(
SELECT 
        CAST ('<M>' + REPLACE(@vcUnitPriceApprover, ',', '</M><M>') + '</M>' AS XML) AS String  
) AS A 
CROSS APPLY 
	String.nodes ('/M') AS Split(a); 





/****** Object:  StoredProcedure [dbo].[USP_GetCartItem]    Script Date: 11/08/2011 18:00:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetCartItem' ) 
                    DROP PROCEDURE USP_GetCartItem
                    Go
CREATE PROCEDURE [dbo].[USP_GetCartItem]
(
		 @numUserCntId numeric(18,0),
		 @numDomainId numeric(18,0),
		 @vcCookieId varchar(100),
		 @bitUserType BIT =0--if 0 then anonomyous user 1 for logi user
)
AS
	IF @numUserCntId <> 0
	BEGIN
		SELECT DISTINCT numCartId,
			   numUserCntId,
			   [dbo].[CartItems].numDomainId,
			   vcCookieId,
			   numOppItemCode,
			   [dbo].[CartItems].numItemCode,
			   (SELECT TOP 1 numCategoryId  FROM dbo.ItemCategory WHERE numItemID =  CartItems.numItemCode AND [dbo].[CartItems].numDomainId = @numDomainId ) AS numCategoryId,
			   (SELECT vcCategoryName FROM [dbo].[Category] WHERE [Category].[numCategoryID]  = (SELECT TOP 1 numCategoryId  FROM dbo.ItemCategory WHERE numItemID =  CartItems.numItemCode AND [dbo].[CartItems].numDomainId = @numDomainId )) AS vcCategoryName,
			   numUnitHour,
			   monPrice,
			   monPrice AS [monListPrice],
			   numSourceId,
			   vcItemDesc,
			   vcItemDesc AS txtItemDesc,
			   [dbo].[CartItems].numWarehouseId,
			   [dbo].[CartItems].vcItemName,
			   vcWarehouse,
			   numWarehouseItmsID,
			   vcItemType,
			   vcAttributes,
			   vcAttrValues,
			   [dbo].[CartItems].bitFreeShipping,
			   numWeight,
			   tintOpFlag,
			   bitDiscountType,
			   fltDiscount,
			   monTotAmtBefDiscount,
			   ItemURL,
			   numUOM,
			   vcUOMName,
			   decUOMConversionFactor,
			   numHeight,
			   numLength,
			   numWidth,
			   vcShippingMethod,
			   numServiceTypeId,
			   decShippingCharge,
			   numShippingCompany,
			   vcCoupon,
			   tintServicetype,CAST( dtDeliveryDate AS VARCHAR(30)) dtDeliveryDate,monTotAmount,
			   (SELECT vcModelID FROM dbo.Item WHERE Item.numItemCode =  CartItems.numItemCode) AS [vcModelID]
			   ,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = CartItems.numItemCode AND II.numDomainId= CartItems.numDomainId AND II.bitDefault=1) As vcPathForTImage
			   , ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc],ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell], ISNULL(vcUpSellDesc,'') [vcUpSellDesc]
			   ,ISNULL([I].[vcSKU],'') AS [vcSKU]
			   ,I.vcManufacturer, CASE WHEN (fltDiscount>0 or (SELECT COUNT(*) FROM PromotionOffer AS P WHERE PromotionID=P.numProId AND (fltDiscount>0 or 1=(CASE WHEN P.tintOfferTriggerValueType=1 AND numUnitHour>=fltOfferTriggerValue AND ISNULL(bitParentPromotion,0)=1 THEN 1 WHEN P.tintOfferTriggerValueType=2 AND monTotAmount>=fltOfferTriggerValue AND ISNULL(bitParentPromotion,0)=1 THEN 1 ELSE 0 END)) AND numDomainId = @numDomainId AND numUserCntId = @numUserCntId)>0) THEN PromotionID ELSE 0 END AS PromotionID ,
			   CASE WHEN (fltDiscount>0 or (SELECT COUNT(*) FROM PromotionOffer AS P WHERE PromotionID=P.numProId AND (fltDiscount>0 or 1=(CASE WHEN P.tintOfferTriggerValueType=1 AND numUnitHour>=fltOfferTriggerValue AND ISNULL(bitParentPromotion,0)=1  AND 1=(CASE WHEN P.bitRequireCouponCode=1 AND P.txtCouponCode=vcCoupon THEN 1  WHEN P.bitRequireCouponCode=0 THEN 1 ELSE 0 END) THEN 1 WHEN P.tintOfferTriggerValueType=2 AND monTotAmount>=fltOfferTriggerValue AND ISNULL(bitParentPromotion,0)=1  AND 1=(CASE WHEN P.bitRequireCouponCode=1 AND P.txtCouponCode=vcCoupon THEN 1  WHEN P.bitRequireCouponCode=0 THEN 1 ELSE 0 END) THEN 1 ELSE 0 END)) AND numDomainId = @numDomainId AND numUserCntId = @numUserCntId)>0) THEN (SELECT TOP 1 vcProName FROM PromotionOffer AS PC WHERE PC.numProId=PromotionID) ELSE '' END AS PromotionDesc
			   FROM CartItems 
			   LEFT JOIN [dbo].[SimilarItems] AS SI ON SI.[numItemCode] = [dbo].[CartItems].[numItemCode]
			   LEFT JOIN [dbo].[Item] AS I ON I.[numItemCode] = [dbo].[CartItems].[numItemCode]
			   --LEFT JOIN [dbo].[WareHouseItems] AS WHI ON WHI.[numItemID] = I.[numItemCode]
			   WHERE [dbo].[CartItems].numDomainId = @numDomainId AND numUserCntId = @numUserCntId 	
	END
	ELSE
	BEGIN
		SELECT DISTINCT numCartId,
			   numUserCntId,
			   [dbo].[CartItems].numDomainId,
			   vcCookieId,
			   numOppItemCode,
			   [dbo].[CartItems].numItemCode,
			   (SELECT TOP 1 numCategoryId  FROM dbo.ItemCategory WHERE numItemID =  CartItems.numItemCode AND CartItems.numDomainId =@numDomainId ) AS numCategoryId,
			   (SELECT vcCategoryName FROM [dbo].[Category] WHERE [Category].[numCategoryID]  = (SELECT TOP 1 numCategoryId  FROM dbo.ItemCategory WHERE numItemID =  CartItems.numItemCode AND [dbo].[CartItems].numDomainId = @numDomainId )) AS vcCategoryName,
			   numUnitHour,
			   monPrice,
			   monPrice AS [monListPrice],
			   numSourceId,
			   vcItemDesc,
			   vcItemDesc AS txtItemDesc,
			   [dbo].[CartItems].numWarehouseId,
			   [dbo].[CartItems].vcItemName,
			   vcWarehouse,
			   numWarehouseItmsID,
			   vcItemType,
			   vcAttributes,
			   vcAttrValues,
			   [dbo].[CartItems].bitFreeShipping,
			   numWeight,
			   tintOpFlag,
			   bitDiscountType,
			   fltDiscount,
			   monTotAmtBefDiscount,
			   ItemURL,
			   numUOM,
			   vcUOMName,
			   decUOMConversionFactor,
			   numHeight,
			   numLength,
			   numWidth,
			   vcShippingMethod,
			   numServiceTypeId,
			   decShippingCharge,
			   numShippingCompany,
			   tintServicetype,
			   vcCoupon,
			   CAST( dtDeliveryDate AS VARCHAR(30)) dtDeliveryDate,
			   monTotAmount,
			   (SELECT vcModelID FROM dbo.Item WHERE Item.numItemCode =  CartItems.numItemCode) AS [vcModelID]
			   ,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = CartItems.numItemCode AND II.numDomainId= CartItems.numDomainId AND II.bitDefault=1) As vcPathForTImage
			   , ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc],ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell], ISNULL(vcUpSellDesc,'') [vcUpSellDesc]
			   , ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc],ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell], ISNULL(vcUpSellDesc,'') [vcUpSellDesc]
			   ,ISNULL([I].[vcSKU],'') AS [vcSKU]
			   ,I.vcManufacturer, CASE WHEN (fltDiscount>0 or (SELECT COUNT(*) FROM PromotionOffer AS P WHERE PromotionID=P.numProId AND (fltDiscount>0 or 1=(CASE WHEN P.tintOfferTriggerValueType=1 AND numUnitHour>=fltOfferTriggerValue AND ISNULL(bitParentPromotion,0)=1 THEN 1 WHEN P.tintOfferTriggerValueType=2 AND monTotAmount>=fltOfferTriggerValue AND ISNULL(bitParentPromotion,0)=1 THEN 1 ELSE 0 END)) AND numDomainId = @numDomainId AND numUserCntId = @numUserCntId)>0) THEN PromotionID ELSE 0 END AS PromotionID ,
			   CASE WHEN (fltDiscount>0 or (SELECT COUNT(*) FROM PromotionOffer AS P WHERE PromotionID=P.numProId AND (fltDiscount>0 or 1=(CASE WHEN P.tintOfferTriggerValueType=1 AND numUnitHour>=fltOfferTriggerValue AND ISNULL(bitParentPromotion,0)=1 AND 1=(CASE WHEN P.bitRequireCouponCode=1 AND P.txtCouponCode=vcCoupon THEN 1  WHEN P.bitRequireCouponCode=0 THEN 1 ELSE 0 END) THEN 1 WHEN P.tintOfferTriggerValueType=2 AND monTotAmount>=fltOfferTriggerValue AND 1=(CASE WHEN P.bitRequireCouponCode=1 AND P.txtCouponCode=vcCoupon THEN 1  WHEN P.bitRequireCouponCode=0 THEN 1 ELSE 0 END) AND ISNULL(bitParentPromotion,0)=1 THEN 1 ELSE 0 END)) AND numDomainId = @numDomainId AND numUserCntId = @numUserCntId)>0) THEN (SELECT TOP 1 vcProName FROM PromotionOffer AS PC WHERE PC.numProId=PromotionID) ELSE '' END AS PromotionDesc
			   FROM CartItems
			   LEFT JOIN [dbo].[SimilarItems] AS SI ON SI.[numItemCode] = [dbo].[CartItems].[numItemCode]
			   LEFT JOIN [dbo].[Item] AS I ON I.[numItemCode] = [dbo].[CartItems].[numItemCode]
			   --LEFT JOIN [dbo].[WareHouseItems] AS WHI ON WHI.[numItemID] = I.[numItemCode]
			   WHERE [dbo].[CartItems].numDomainId = @numDomainId AND numUserCntId = @numUserCntId AND vcCookieId =@vcCookieId	
	END
	
	

--exec USP_GetCartItem @numUserCntId=1,@numDomainId=1,@vcCookieId='fc3d604b-25fa-4c25-960c-1e317768113e',@bitUserType=NULL



/****** Object:  StoredProcedure [dbo].[USP_InsertCartItem]    Script Date: 11/08/2011 17:53:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_InsertCartItem' ) 
                    DROP PROCEDURE USP_InsertCartItem
                    Go
CREATE  PROCEDURE [dbo].[USP_InsertCartItem]
    (
      @numCartId AS NUMERIC OUTPUT,
      @numUserCntId NUMERIC(18, 0),
      @numDomainId NUMERIC(18, 0),
      @vcCookieId VARCHAR(100),
      @numOppItemCode NUMERIC(18, 0),
      @numItemCode NUMERIC(18, 0),
      @numUnitHour NUMERIC(18, 0),
      @monPrice MONEY,
      @numSourceId NUMERIC(18, 0),
      @vcItemDesc VARCHAR(2000),
      @numWarehouseId NUMERIC(18, 0),
      @vcItemName VARCHAR(200),
      @vcWarehouse VARCHAR(200),
      @numWarehouseItmsID NUMERIC(18, 0),
      @vcItemType VARCHAR(200),
      @vcAttributes VARCHAR(100),
      @vcAttrValues VARCHAR(100),
      @bitFreeShipping BIT,
      @numWeight NUMERIC(18, 2),
      @tintOpFlag TINYINT,
      @bitDiscountType BIT,
      @fltDiscount DECIMAL(18,2),
      @monTotAmtBefDiscount MONEY,
      @ItemURL VARCHAR(200),
      @numUOM NUMERIC(18, 0),
      @vcUOMName VARCHAR(200),
      @decUOMConversionFactor DECIMAL(18, 0),
      @numHeight NUMERIC(18, 0),
      @numLength NUMERIC(18, 0),
      @numWidth NUMERIC(18, 0),
      @vcShippingMethod VARCHAR(200),
      @numServiceTypeId NUMERIC(18, 0),
      @decShippingCharge DECIMAL(18, 2),
      @numShippingCompany NUMERIC(18, 0),
      @tintServicetype TINYINT,
      @dtDeliveryDate DATETIME,
      @monTotAmount money,
	  @PromotionID NUMERIC(18,0),
	  @PromotionDesc VARCHAR(MAX),
	  @postselldiscount INT=0,
	  @vcCoupon VARCHAR(200)=0
    )
AS 

BEGIN TRY
    BEGIN TRANSACTION;

	DECLARE @numPromotionID AS NUMERIC(18,0)=0
	DECLARE @chrItemType AS CHAR(1)
	DECLARE @numItemGroup AS NUMERIC(18,0)
	DECLARE @ItemDesc AS VARCHAR(1000)
	DECLARE @numItemClassification AS NUMERIC(18,0)

	DECLARE @numWarehouseItemID NUMERIC(18,0)

	IF(@postselldiscount=0)
	BEGIN
	-- GET ITEM DETAILS
	SELECT
		@ItemDesc = Item.txtItemDesc,
		@chrItemType = charItemType,
		@numItemGroup = numItemGroup,
		@numItemClassification = numItemClassification,
		@numWarehouseItemID = (CASE WHEN ISNULL(@numWarehouseItemID,0) > 0 THEN @numWarehouseItemID ELSE WareHouseItems.numWareHouseItemID END)
	FROM
		Item
	OUTER APPLY
	(
		SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems WHERE numItemID=@numItemCode AND (numWareHouseItemID = @numWarehouseItemID OR ISNULL(@numWarehouseItemID,0) = 0)
	) AS WareHouseItems
	WHERE
		Item.numItemCode = @numItemCode

	Declare @existpromotion numeric=0
	SET @existpromotion=(SELECT COUNT(PT.numValue) FROM PromotionOfferItems as PT LEFT JOIN PromotionOffer AS P ON P.numProId=PT.numProId LEFT JOIN CartItems AS C ON C.PromotionID=P.numProId 
	WHERE ISNULL(C.bitParentPromotion,0)=1 AND C.vcCookieId=@vcCookieId AND C.numUserCntId=@numUserCntId AND PT.numValue IN(@numItemCode))

	PRINT @existpromotion
	IF(@existpromotion=0)
	BEGIN
		SELECT TOP 1
		@numPromotionID = numProId,
		--@vcCoupon=txtCouponCode,
		@PromotionDesc=(SELECT CONCAT('Buy '
					,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
					,CASE tintOfferBasedOn
							WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
							WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
						END
					,' & get '
					, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
					, CASE 
						WHEN tintDiscoutBaseOn = 1 THEN 
							CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 2 THEN 
							CONCAT
							(
								CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(',',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
						ELSE '' 
					END
				) AS vcPromotionDescription)
	FROM 
		PromotionOffer PO
	WHERE 
		numDomainId=@numDomainID 
		AND ISNULL(bitEnabled,0)=1 
		AND ISNULL(bitAppliesToSite,0)=1 
		AND 1= (CASE WHEN bitRequireCouponCode = 1 AND PO.txtCouponCode=@vcCoupon THEN 1 WHEN bitRequireCouponCode = 0 THEN 1 ELSE 0 END)
		AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
		AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
		AND 1 = (CASE 
					WHEN tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=@numItemCode) > 0 THEN 1 ELSE 0 END)
					WHEN tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=@numItemClassification) > 0 THEN 1 ELSE 0 END)
					ELSE 0
				END)
	ORDER BY
		CASE 
			WHEN PO.txtCouponCode IN(@vcCoupon) THEN 1
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 2
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 3
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 4
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 5
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 6
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 7
		END
	END

	DECLARE @bitParentPromotion BIT=0
	PRINT @numPromotionID
	IF(@numPromotionID>0)
	BEGIN
		SET @bitParentPromotion=1
	END

	PRINT @bitParentPromotion
	END
INSERT  INTO [dbo].[CartItems]
            (
              [numUserCntId],
              [numDomainId],
              [vcCookieId],
              [numOppItemCode],
              [numItemCode],
              [numUnitHour],
              [monPrice],
              [numSourceId],
              [vcItemDesc],
              [numWarehouseId],
              [vcItemName],
              [vcWarehouse],
              [numWarehouseItmsID],
              [vcItemType],
              [vcAttributes],
              [vcAttrValues],
              [bitFreeShipping],
              [numWeight],
              [tintOpFlag],
              [bitDiscountType],
              [fltDiscount],
              [monTotAmtBefDiscount],
              [ItemURL],
              [numUOM],
              [vcUOMName],
              [decUOMConversionFactor],
              [numHeight],
              [numLength],
              [numWidth],
              [vcShippingMethod],
              [numServiceTypeId],
              [decShippingCharge],
              [numShippingCompany],
              [tintServicetype],
              [monTotAmount],
			  PromotionID,
			  PromotionDesc,
			  bitParentPromotion,
			  vcCoupon
         )
VALUES  (
              @numUserCntId,
              @numDomainId,
              @vcCookieId,
              @numOppItemCode,
              @numItemCode,
              @numUnitHour,
              @monPrice,
              @numSourceId,
              @vcItemDesc,
              @numWarehouseId,
              @vcItemName,
              @vcWarehouse,
              @numWarehouseItmsID,
              @vcItemType,
              @vcAttributes,
              @vcAttrValues,
              @bitFreeShipping,
              @numWeight,
              @tintOpFlag,
              @bitDiscountType,
              @fltDiscount,
              @monTotAmtBefDiscount,
              @ItemURL,
              @numUOM,
              @vcUOMName,
              @decUOMConversionFactor,
              @numHeight,
              @numLength,
              @numWidth,
              @vcShippingMethod,
              @numServiceTypeId,
              @decShippingCharge,
              @numShippingCompany,
              @tintServicetype,
              @monTotAmount,
			  @numPromotionID,
			  @PromotionDesc,
			  @bitParentPromotion,
			  @vcCoupon
            )
IF(@postselldiscount=0)
	BEGIN
DECLARE @tintOfferTriggerValueType int
DECLARE @fltOfferTriggerValue float
DECLARE @tintOfferBasedOn int
DECLARE @fltDiscountValue float
DECLARE @tintDiscountType int
DECLARE @tintDiscoutBaseOn int
DECLARE @bitCouponRequired BIT
DECLARE @vcItems varchar(MAX)
DECLARE @vcPromotionDescription  varchar(MAX)

	create table #TempPrmotion
	(
		PRowID int IDENTITY(1,1), 
		numPromotionId numeric,
		numUnitHour numeric,
		monTotAmount money,
		vcCoupon VARCHAR(300)
	)
	DECLARE @Pcount INT=0
	DECLARE @PIcount INT=1
	INSERT INTO #TempPrmotion
		SELECT PromotionID,numUnitHour,monTotAmount,vcCoupon FROM CartItems WHERE vcCookieId=@vcCookieId  AND numUserCntId=@numUserCntId AND ISNULL(bitParentPromotion,0)=1 order by numCartId

	SET @Pcount=(SELECT COUNT(*) FROM #TempPrmotion)
	DECLARE @count INT=0
	DECLARE @ICOUNT INT=1
	DECLARE @numACartId NUMERIC
	DECLARE @vcPCoupon VARCHAR(300)
	IF(@Pcount>0)
	BEGIN
		WHILE(@PIcount<=@Pcount)
		BEGIN
			
			SET @numPromotionID=(SELECT TOP 1 numPromotionId FROM #TempPrmotion WHERE PRowID=@PIcount)
			SET @numUnitHour=(SELECT TOP 1 numUnitHour FROM #TempPrmotion WHERE PRowID=@PIcount)
			SET @monTotAmount=(SELECT TOP 1 monTotAmount FROM #TempPrmotion WHERE PRowID=@PIcount)

			SET @vcPCoupon=(SELECT TOP 1 vcCoupon FROM #TempPrmotion WHERE PRowID=@PIcount)

			CREATE TABLE #tempitems(
				item VARCHAR(MAX)
			)
			



			SELECT 
			@tintOfferTriggerValueType=tintOfferTriggerValueType
			,@fltOfferTriggerValue=fltOfferTriggerValue
			,@tintOfferBasedOn=tintOfferBasedOn
			,@fltDiscountValue=fltDiscountValue
			,@tintDiscountType=tintDiscountType
			,@tintDiscoutBaseOn=tintDiscoutBaseOn
			,@bitCouponRequired=bitRequireCouponCode
			--,@vcCoupon=txtCouponCode
			,@vcPromotionDescription=(SELECT CONCAT('Buy '
					,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
					,CASE tintOfferBasedOn
							WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
							WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
						END
					,' & get '
					, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
					, CASE 
						WHEN tintDiscoutBaseOn = 1 THEN 
							CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 2 THEN 
							CONCAT
							(
								CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(',',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
						ELSE '' 
					END
				) AS vcPromotionDescription)
		FROM
			PromotionOffer PO
		WHERE
			numDomainId=@numDomainID 
			AND numProId = @numPromotionID
			IF(@tintOfferBasedOn=2)
			BEGIN
				SET @numUnitHour=(SELECT SUM(numUnitHour) FROM #TempPrmotion WHERE numPromotionId=@numPromotionID)
				SET @monTotAmount=(SELECT SUM(monTotAmount) FROM #TempPrmotion WHERE numPromotionId=@numPromotionID)
			END
			--IF(@tintDiscoutBaseOn=1)
			--BEGIN
			--	INSERT INTO #tempitems SELECT numValue FROM PromotionOfferItems WHERE numProId=@numPromotionID AND tintRecordType=6 AND tintType=1
			--END

			--IF(@tintDiscoutBaseOn=2)
			--BEGIN
			--	INSERT INTO #tempitems SELECT numValue FROM PromotionOfferItems WHERE numProId=@numPromotionID AND tintRecordType=6 AND tintType=2
			--END

			--IF(@tintDiscoutBaseOn=3)
			--BEGIN
			--	INSERT INTO #tempitems SELECT SimilarItems.numItemCode FROM SimilarItems WHERE 1 = (CASE 
			--																											WHEN @tintDiscoutBaseOn=1 THEN CASE WHEN numParentItemCode IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=@numPromotionID AND tintRecordType=5 AND tintType=1) THEN 1 ELSE 0 END
			--																											WHEN @tintDiscoutBaseOn=2 THEN CASE WHEN numParentItemCode IN (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemClassification IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=@numPromotionID AND tintRecordType=5 AND tintType=2)) THEN 1 ELSE 0 END
			--																											ELSE 0
			--																										END)
			--END
			IF(@tintDiscoutBaseOn=1)
			BEGIN
				INSERT INTO #tempitems SELECT numValue FROM PromotionOfferItems WHERE numProId=@numPromotionID AND tintRecordType=6 AND tintType=1
			END

			IF(@tintDiscoutBaseOn=2)
			BEGIN
				INSERT INTO #tempitems SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemClassification IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=@numPromotionID AND tintRecordType=6)
			END

			IF(@tintDiscoutBaseOn=3 AND @tintOfferBasedOn=1)
			BEGIN
				INSERT INTO #tempitems SELECT SimilarItems.numItemCode FROM SimilarItems WHERE numParentItemCode IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=@numPromotionID AND tintRecordType=5 AND tintType=1)
			END
			
			IF(@tintDiscoutBaseOn=3 AND @tintOfferBasedOn=2)
			BEGIN
				INSERT INTO #tempitems SELECT SimilarItems.numItemCode FROM SimilarItems WHERE numParentItemCode IN (SELECT numItemCode FROM Item WHERE numItemClassification IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=@numPromotionID AND tintRecordType=5 AND tintType=2))
			END
			create table #Temp
			(
				RowID int IDENTITY(1,1), 
				numCartId numeric
			)
			SET @ICOUNT =1
			SET @count =0
			SET @count=(SELECT COUNT(*) FROM CartItems WHERE vcCookieId=@vcCookieId AND numUserCntId=@numUserCntId AND numItemCode IN (SELECT item FROM #tempitems) 
			--AND  1=(CASE WHEN @bitCouponRequired=1 AND vcCoupon=@vcCoupon THEN 1 WHEN @bitCouponRequired=0 THEN 1 ELSE 0 END)
			)
			IF(1=(CASE WHEN @bitCouponRequired=1 AND @vcPCoupon<>'' THEN 0 ELSE 1 END))
			BEGIN
			IF(@count>0)
				BEGIN
					INSERT INTO 
						#Temp 
					SELECT 
						numCartId
					FROM 
						CartItems
					WHERE
						vcCookieId=@vcCookieId AND numUserCntId=@numUserCntId and  
						numItemCode IN (SELECT item FROM #tempitems) 
						--AND
						--1=(CASE WHEN @bitCouponRequired=1 AND vcCoupon=@vcCoupon THEN 1 WHEN @bitCouponRequired=0 THEN 1 ELSE 0 END)
						--AND
						--ISNULL(PromotionID,0)=0

					SET @count=(SELECT COUNT(*) FROM #Temp)

					
					WHILE(@ICOUNT<=@count)
					BEGIN
			
						SET @numACartId=(SELECT TOP 1 numCartId FROM #Temp WHERE RowID=@ICOUNT)
						IF(@tintOfferTriggerValueType=1 AND @numUnitHour>=@fltOfferTriggerValue)
						BEGIN
							if(@tintDiscountType=1)
							BEGIN
								UPDATE 
									CartItems 
								SET 
									monTotAmount=(monTotAmount*((100-@fltDiscountValue)/100)),
									PromotionID=@numPromotionID,
									PromotionDesc=@vcPromotionDescription,
									bitDiscountType=0,
									fltDiscount=@fltDiscountValue
								WHERE 
									numCartId=@numACartId
								BREAK;
							END
							if(@tintDiscountType=2)
							BEGIN
								UPDATE 
									CartItems 
								SET 
									monTotAmount=(monTotAmount-@fltDiscountValue),
									PromotionID=@numPromotionID ,
									PromotionDesc=@vcPromotionDescription,
									bitDiscountType=1,
									fltDiscount=@fltDiscountValue
								WHERE 
									numCartId=@numACartId
								BREAK;
							END
							if(@tintDiscountType=3)
							BEGIN
								UPDATE 
									CartItems 
								SET 
									monTotAmount=(monTotAmount-(@fltDiscountValue*monPrice)),
									PromotionID=@numPromotionID ,
									PromotionDesc=@vcPromotionDescription,
									bitDiscountType=1,
									fltDiscount=(@fltDiscountValue*monPrice)
								WHERE 
									numCartId=@numACartId
								BREAK;
							END
						END
						IF(@tintOfferTriggerValueType=2 AND @monTotAmount>=@fltOfferTriggerValue)
						BEGIN
							if(@tintDiscountType=1)
							BEGIN
								UPDATE 
									CartItems 
								SET 
									monTotAmount=(monTotAmount*((100-@fltDiscountValue)/100)),
									PromotionID=@numPromotionID,
									PromotionDesc=@vcPromotionDescription,
									bitDiscountType=0,
									fltDiscount=@fltDiscountValue
								WHERE 
									numCartId=@numACartId
								BREAK;
							END
							if(@tintDiscountType=2)
							BEGIN
								UPDATE 
									CartItems 
								SET 
									monTotAmount=(monTotAmount-@fltDiscountValue),
									PromotionID=@numPromotionID ,
									PromotionDesc=@vcPromotionDescription,
									bitDiscountType=1,
									fltDiscount=@fltDiscountValue
								WHERE 
									numCartId=@numACartId
								BREAK;
							END
							if(@tintDiscountType=3)
							BEGIN
								UPDATE 
									CartItems 
								SET 
									monTotAmount=(monTotAmount-(@fltDiscountValue*monPrice)),
									PromotionID=@numPromotionID ,
									PromotionDesc=@vcPromotionDescription,
									bitDiscountType=1,
									fltDiscount=(@fltDiscountValue*monPrice)
								WHERE 
									numCartId=@numACartId
								BREAK;
							END
						END
						SET @ICOUNT=@ICOUNT+1
					END
				END
			END
			DROP TABLE #Temp
			SET @PIcount=@PIcount+1
			DROP TABLE #tempitems
		END
	END

	END

SET @numCartId = (SELECT MAX(numCartID) FROM dbo.CartItems WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId)
SELECT @numCartId

 COMMIT TRANSACTION;
  END TRY
  BEGIN CATCH
    IF @@TRANCOUNT > 0
    ROLLBACK TRANSACTION;

    DECLARE @ErrorNumber INT = ERROR_NUMBER();
    DECLARE @ErrorLine INT = ERROR_LINE();
    DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
    DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
    DECLARE @ErrorState INT = ERROR_STATE();

    PRINT 'Actual error number: ' + CAST(@ErrorNumber AS VARCHAR(10));
    PRINT 'Actual line number: ' + CAST(@ErrorLine AS VARCHAR(10));

    RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
  END CATCH
            

--SELECT * FROM Category
--- Alterd By Chintan Prajapati
-- EXEC dbo.USP_ItemsForECommerce 9,60,1,1,15,0,''
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemsForECommerce' ) 
    DROP PROCEDURE USP_ItemsForECommerce
GO
CREATE PROCEDURE [dbo].[USP_ItemsForECommerce]
    @numSiteID AS NUMERIC(9) = 0,
    @numCategoryID AS NUMERIC(18) = 0,
    @numWareHouseID AS NUMERIC(9) = 0,
    @SortBy NVARCHAR(MAX) = 0,
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT = 0 OUTPUT,
    @SearchText AS NVARCHAR(MAX) = '',--Comma seperated item ids
    @FilterQuery AS NVARCHAR(MAX) = '',----Comma seperated Attributes Ids
    @FilterRegularCondition AS NVARCHAR(MAX),
    --@FilterCustomCondition AS NVARCHAR(MAX),
    @FilterCustomFields AS NVARCHAR(MAX),
    @FilterCustomWhere AS NVARCHAR(MAX)
   
AS 
    BEGIN
		
		DECLARE @numDomainID AS NUMERIC(9)
		SELECT  @numDomainID = numDomainID
        FROM    [Sites]
        WHERE   numSiteID = @numSiteID
        
        DECLARE @tintDisplayCategory AS TINYINT
		SELECT @tintDisplayCategory = ISNULL(bitDisplayCategory,0) FROM dbo.eCommerceDTL WHERE numSiteID = @numSiteId
		PRINT @tintDisplayCategory 
		
		CREATE TABLE #tmpItemCat(numCategoryID NUMERIC(18,0))
		CREATE TABLE #tmpItemCode(numItemCode NUMERIC(18,0))
		IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
		BEGIN
--			DECLARE @strCustomSql AS NVARCHAR(MAX)
--			SET @strCustomSql = 'INSERT INTO #tmpItemCode(numItemCode)
--								 SELECT DISTINCT t2.RecId FROM CFW_Fld_Master t1 
--								 JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID 
--								 WHERE t1.Grp_id = 5 
--								 AND t1.numDomainID = (SELECT numDomainID FROM dbo.Sites WHERE numSiteID = ' + CONVERT(VARCHAR(10),@numSiteID) + ') 
--								 AND t1.fld_type <> ''Link'' AND ' + @FilterCustomCondition
			
			DECLARE @FilterCustomFieldIds AS NVARCHAR(MAX)
			SET @FilterCustomFieldIds = Replace(@FilterCustomFields,'[','')
			SET @FilterCustomFieldIds = Replace(@FilterCustomFieldIds,']','')
			
			DECLARE @strCustomSql AS NVARCHAR(MAX)
			SET @strCustomSql = 'INSERT INTO #tmpItemCode(numItemCode)
								 SELECT DISTINCT T.RecId FROM 
															(
																SELECT * FROM 
																	(
																		SELECT  t2.RecId, t1.fld_id, t2.Fld_Value FROM CFW_Fld_Master t1 
																		JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID 
																		WHERE t1.Grp_id = 5 
																		AND t1.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
																		AND t1.fld_type <> ''Link'' 
																		AND t1.fld_id IN (' + @FilterCustomFieldIds + ')
																	) p  PIVOT (MAX([Fld_Value]) FOR fld_id IN (' + @FilterCustomFields + ')) AS pvt 
															) T  WHERE 1=1 AND ' + @FilterCustomWhere
								 
			PRINT @strCustomSql
			EXEC SP_EXECUTESQL @strCustomSql								 					 
		END
		
        DECLARE @strSQL NVARCHAR(MAX)
        DECLARE @firstRec AS INTEGER
        DECLARE @lastRec AS INTEGER
        DECLARE @Where NVARCHAR(MAX)
        DECLARE @SortString NVARCHAR(MAX)
        DECLARE @searchPositionColumn NVARCHAR(MAX)
        DECLARE @row AS INTEGER
        DECLARE @data AS VARCHAR(100)
        DECLARE @dynamicFilterQuery NVARCHAR(MAX)
        DECLARE @checkFldType VARCHAR(100)
        DECLARE @count NUMERIC(9,0)
        DECLARE @whereNumItemCode NVARCHAR(MAX)
        DECLARE @filterByNumItemCode VARCHAR(100)
        
		SET @SortString = ''
        set @searchPositionColumn = ''
        SET @strSQL = ''
		SET @whereNumItemCode = ''
		SET @dynamicFilterQuery = ''
		 
        SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
        SET @lastRec = ( @CurrentPage * @PageSize + 1 )
                
        DECLARE @bitAutoSelectWarehouse AS BIT
		SELECT @bitAutoSelectWarehouse = ISNULL([ECD].[bitAutoSelectWarehouse],0) FROM [dbo].[eCommerceDTL] AS ECD WHERE [ECD].[numSiteId] = @numSiteID
		AND [ECD].[numDomainId] = @numDomainID

		DECLARE @vcWarehouseIDs AS VARCHAR(1000)
        IF (@numWareHouseID = 0 OR @bitAutoSelectWarehouse = 1)
            BEGIN				
				IF @bitAutoSelectWarehouse = 1
					BEGIN
						SELECT @vcWarehouseIDs = ISNULL( STUFF((SELECT DISTINCT ',' + CAST(numWarehouseID AS VARCHAR(10)) FROM [dbo].[Warehouses] WHERE [Warehouses].[numDomainID] = @numDomainID FOR XML PATH('')),1, 1, '') , '') 
					END
				
				ELSE
					BEGIN
						SELECT  @numWareHouseID = ISNULL(numDefaultWareHouseID,0)
						FROM    [eCommerceDTL]
						WHERE   [numDomainID] = @numDomainID	

						SET @vcWarehouseIDs = @numWareHouseID
					END
            END
		ELSE
		BEGIN
			SET @vcWarehouseIDs = @numWareHouseID
		END

		PRINT @vcWarehouseIDs

		--PRIAMRY SORTING USING CART DROPDOWN
		DECLARE @Join AS NVARCHAR(MAX)
		DECLARE @SortFields AS NVARCHAR(MAX)
		DECLARE @vcSort AS VARCHAR(MAX)

		SET @Join = ''
		SET @SortFields = ''
		SET @vcSort = ''

		IF @SortBy = ''
			BEGIN
				SET @SortString = ' vcItemName Asc '
			END
		ELSE IF CHARINDEX('~0',@SortBy) > 0 OR CHARINDEX('~1',@SortBy) > 0
			BEGIN
				DECLARE @fldID AS NUMERIC(18,0)
				DECLARE @RowNumber AS VARCHAR(MAX)
				
				SELECT * INTO #fldValues FROM dbo.SplitIDsWithPosition(@SortBy,'~')
				SELECT @RowNumber = RowNumber, @fldID = id FROM #fldValues WHERE RowNumber = 1
				SELECT @vcSort = CASE WHEN Id = 0 THEN 'ASC' ELSE 'DESC' END FROM #fldValues WHERE RowNumber = 2
				
				SET @Join = @Join + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber + ' ON I.numItemCode = CFVI' + @RowNumber + '.RecID AND CFVI' + @RowNumber + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID)
				---SET @SortString = 'CFVI' + @RowNumber + '.fld_Value' 
				SET @SortString = '[fld_Value' + @RowNumber + '] ' --+ @vcSort
				SET @SortFields = ', CFVI' + @RowNumber + '.fld_Value [fld_Value' + @RowNumber + ']'
			END
		ELSE
			BEGIN
				--SET @SortString = @SortBy	
				
				IF CHARINDEX('monListPrice',@SortBy) > 0
				BEGIN
					DECLARE @bitSortPriceMode AS BIT
					SELECT @bitSortPriceMode = ISNULL(bitSortPriceMode,0) FROM dbo.eCommerceDTL WHERE numSiteId = @numSiteID
					PRINT @bitSortPriceMode
					IF @bitSortPriceMode = 1
					BEGIN
						SET @SortString = ' '
					END
					ELSE
					BEGIN
						SET @SortString = @SortBy	
					END
				END
				ELSE
				BEGIN
					SET @SortString = @SortBy	
				END
				
			END	

		--SECONDARY SORTING USING DEFAULT SETTINGS FROM ECOMMERCE
		SELECT ROW_NUMBER() OVER (ORDER BY Custom,vcFieldName) AS [RowID],* INTO #tempSort FROM (
		SELECT  CONVERT(VARCHAR(9),numFieldID)+'~0' AS numFieldID,
				ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
				0 AS Custom,
				vcDbColumnName 
		FROM View_DynamicColumns
		WHERE numFormID = 84 
		AND ISNULL(bitSettingField,0) = 1 
		AND ISNULL(bitDeleted,0)=0 
		AND ISNULL(bitCustom,0) = 0
		AND numDomainID = @numDomainID
					       
		UNION     

		SELECT  CONVERT(VARCHAR(9),numFieldID)+'~1' AS numFieldID ,
				vcFieldName ,
				1 AS Custom,
				'' AS vcDbColumnName
		FROM View_DynamicCustomColumns          
		WHERE numFormID = 84
		AND numDomainID = @numDomainID
		AND grp_id = 5 
		AND vcAssociatedControlType = 'TextBox' 
		AND ISNULL(bitCustom,0) = 1 
		AND tintPageType=1  
		) TABLE1 ORDER BY Custom,vcFieldName
		
		--SELECT * FROM #tempSort
		DECLARE @DefaultSort AS NVARCHAR(MAX)
		DECLARE @DefaultSortFields AS NVARCHAR(MAX)
		DECLARE @DefaultSortJoin AS NVARCHAR(MAX)
		DECLARE @intCnt AS INT
		DECLARE @intTotalCnt AS INT	
		DECLARE @strColumnName AS VARCHAR(100)
		DECLARE @bitCustom AS BIT
		DECLARE @FieldID AS VARCHAR(100)				

		SET @DefaultSort = ''
		SET @DefaultSortFields = ''
		SET @DefaultSortJoin = ''		
		SET @intCnt = 0
		SET @intTotalCnt = ( SELECT COUNT(*) FROM #tempSort )
		SET @strColumnName = ''
		
		IF (SELECT ISNULL(bitEnableSecSorting,0) FROM EcommerceDtl WHERE numSiteID = @numSiteID ) = 1
		BEGIN
			CREATE TABLE #fldDefaultValues(RowNumber INT,Id VARCHAR(1000))

			WHILE(@intCnt < @intTotalCnt)
			BEGIN
				SET @intCnt = @intCnt + 1
				SELECT @FieldID = numFieldID, @strColumnName = vcDbColumnName, @bitCustom = Custom FROM #tempSort WHERE RowID = @intCnt
				
				--PRINT @FieldID
				--PRINT @strColumnName
				--PRINT @bitCustom

				IF @bitCustom = 0
					BEGIN
						SET @DefaultSort =  @strColumnName + ',' + @DefaultSort 
					END

				ELSE
					BEGIN
						DECLARE @fldID1 AS NUMERIC(18,0)
						DECLARE @RowNumber1 AS VARCHAR(MAX)
						DECLARE @vcSort1 AS VARCHAR(10)
						DECLARE @vcSortBy AS VARCHAR(1000)
						SET @vcSortBy = CONVERT(VARCHAR(100),@FieldID) + '~' + '0'
						--PRINT @vcSortBy

						INSERT INTO #fldDefaultValues(RowNumber,Id) SELECT RowNumber, id FROM dbo.SplitIDsWithPosition(@vcSortBy,'~')
						SELECT @RowNumber1 = RowNumber + 100 + @intCnt, @fldID1 = id FROM #fldDefaultValues WHERE RowNumber = 1
						SELECT @vcSort1 = 'ASC' 
						
						SET @DefaultSortJoin = @DefaultSortJoin + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber1 + ' ON I.numItemCode = CFVI' + @RowNumber1 + '.RecID AND CFVI' + @RowNumber1 + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID1)
						SET @DefaultSort = '[fld_Value' + @RowNumber1 + '] ' + ',' + @DefaultSort 
						SET @DefaultSortFields = @DefaultSortFields + ', CFVI' + @RowNumber1 + '.fld_Value [fld_Value' + @RowNumber1 + ']'
					END

			END
			IF LEN(@DefaultSort) > 0
			BEGIN
				SET  @DefaultSort = LEFT(@DefaultSort,LEN(@DefaultSort) - 1)
				SET @SortString = @SortString + ',' 
			END
			--PRINT @SortString
			--PRINT @DefaultSort
			--PRINT @DefaultSortFields
		END
--        IF @SortBy = 1 
--            SET @SortString = ' vcItemName Asc '
--        ELSE IF @SortBy = 2 
--            SET @SortString = ' vcItemName Desc '
--        ELSE IF @SortBy = 3 
--            SET @SortString = ' monListPrice Asc '
--        ELSE IF @SortBy = 4 
--            SET @SortString = ' monListPrice Desc '
--        ELSE IF @SortBy = 5 
--            SET @SortString = ' bintCreatedDate Desc '
--        ELSE IF @SortBy = 6 
--            SET @SortString = ' bintCreatedDate ASC '	
--        ELSE IF @SortBy = 7 --sort by relevance, invokes only when search
--            BEGIN
--                 IF LEN(@SearchText) > 0 
--		            BEGIN
--						SET @SortString = ' SearchOrder ASC ';
--					    set @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 						
--					END
--			END
--        ELSE 
--            SET @SortString = ' vcItemName Asc '
		
                                		
        IF LEN(@SearchText) > 0
			BEGIN
				PRINT 1
				IF CHARINDEX('SearchOrder',@SortString) > 0
				BEGIN
					SET @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 
				END

				SET @Where = ' INNER JOIN dbo.SplitIDsWithPosition('''+ @SearchText +''','','') AS SearchItems 
                        ON SearchItems.Id = I.numItemCode  and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID) + ' WHERE ISNULL(IsArchieve,0) = 0'
				END

		ELSE IF @SearchText = ''

			SET @Where = ' WHERE 1=2 '
			+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
			+ ' AND ISNULL(IsArchieve,0) = 0'
		ELSE
			BEGIN
				PRINT 2
				SET @Where = ' WHERE 1=1 '
							+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
							+ ' AND ISNULL(IsArchieve,0) = 0'


				PRINT @tintDisplayCategory							
				PRINT @Where

				IF @tintDisplayCategory = 1
				BEGIN
					
					;WITH CTE AS(
					SELECT C.numCategoryID,C.tintLevel,C.numDepCategory,CASE WHEN tintLevel=1 THEN 1 ELSE 0 END AS bitChild 
						FROM Category C WHERE (numCategoryID = @numCategoryID OR ISNULL(@numCategoryID,0) = 0) AND numDomainID = @numDomainID
					UNION ALL
					SELECT  C.numCategoryID,C.tintLevel,C.numDepCategory,CTE.bitChild FROM Category C JOIN CTE ON 
					C.numDepCategory=CTE.numCategoryID AND CTE.bitChild=1 AND numDomainID = @numDomainID)
					
					INSERT INTO #tmpItemCat(numCategoryID)												 
					SELECT DISTINCT C.numCategoryID FROM CTE C JOIN dbo.ItemCategory IC ON C.numCategoryID = IC.numCategoryID 
														WHERE numItemID IN ( 
																						
													 						SELECT numItemID FROM WareHouseItems WI
																			INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																			GROUP BY numItemID 
																			HAVING SUM(WI.numOnHand) > 0 
																			UNION ALL
													 						SELECT numItemID FROM WareHouseItems WI
																			INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																			GROUP BY numItemID 
																			HAVING SUM(WI.numAllocation) > 0
																		 )		
					SELECT numItemID INTO #tmpOnHandItems FROM 
					(													 					
						SELECT numItemID FROM WareHouseItems WI 
						INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
						GROUP BY numItemID 
						HAVING SUM(WI.numOnHand) > 0
						UNION ALL
						SELECT numItemID FROM WareHouseItems WI 
						INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
						GROUP BY numItemID 
						HAVING SUM(WI.numAllocation) > 0				
					) TABLE1
				END

			END
		
			--SELECT * FROM  #tmpOnHandItems
			--SELECT * FROM  #tmpItemCat
			
----		IF (SELECT COUNT(*) FROM #tmpItemCat) = 0 AND ISNULL(@numCategoryID,0) = 0
----		BEGIN
----			
----			INSERT INTO #tmpItemCat(numCategoryID)
----			SELECT DISTINCT C.numCategoryID FROM Category C
----			JOIN dbo.ItemCategory IC ON C.numCategoryID = IC.numCategoryID 
----							WHERE C.numDepCategory = (SELECT TOP 1 numCategoryID FROM dbo.Category 
----													  WHERE numDomainID = @numDomainID
----													  AND tintLevel = 1)
----							AND tintLevel <> 1
----							AND numDomainID = @numDomainID
----							AND numItemID IN (
----											 	SELECT numItemID FROM WareHouseItems WI
----												INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID
----												GROUP BY numItemID 
----												HAVING SUM(WI.numOnHand) > 0
----											 )
----			--SELECT * FROM #tmpItemCat								 
----		END
----		ELSE
----		BEGIN
----			INSERT INTO #tmpItemCat(numCategoryID)VALUES(@numCategoryID)
----		END

		       SET @strSQL = @strSQL + ' WITH Items AS 
	                                     ( 
	                                     SELECT   
                                               I.numItemCode,
                                               ROW_NUMBER() OVER ( Partition by I.numItemCode Order by I.numItemCode) RowID,
                                               vcItemName,
                                               I.bintCreatedDate,
                                               vcManufacturer,
                                               C.vcCategoryName,
                                               ISNULL(txtItemDesc,'''') txtItemDesc,
                                               ISNULL(CASE  WHEN I.[charItemType] = ''P''
															THEN ( UOM * ISNULL(W1.[monWListPrice], 0) )
															ELSE ( UOM * monListPrice )
													  END, 0) monListPrice,
                                               vcSKU,
                                               fltHeight,
                                               fltWidth,
                                               fltLength,
                                               IM.vcPathForImage,
                                               IM.vcPathForTImage,
                                               vcModelID,
                                               fltWeight,
                                               bitFreeShipping,
                                               UOM AS UOMConversionFactor,
											   UOMPurchase AS UOMPurchaseConversionFactor,
												( CASE WHEN charItemType <> ''S'' AND charItemType <> ''N''
													   THEN ( CASE WHEN bitShowInStock = 1
																   THEN ( CASE WHEN ( ISNULL(W.numOnHand,0)<=0) THEN CASE WHEN ' + CONVERT(VARCHAR(10),@numDomainID) + ' = 172 THEN ''<font color=red>ON HOLD</font>'' ELSE ''<font color=red>Out Of Stock</font>'' END
																			   WHEN bitAllowBackOrder = 1 THEN CASE WHEN ' + CONVERT(VARCHAR(10),@numDomainID) + ' = 172 THEN ''<font color=green>AVAILABLE</font>'' ELSE ''<font color=green>In Stock</font>'' END 
																			   ELSE CASE WHEN ' + CONVERT(VARCHAR(10),@numDomainID) + ' = 172 THEN ''<font color=green>AVAILABLE</font>'' ELSE ''<font color=green>In Stock</font>'' END  
																		  END )
																   ELSE ''''
															  END )
													   ELSE ''''
												  END ) AS InStock, C.vcDescription as CategoryDesc, ISNULL(W1.numWareHouseItemID,0) [numWareHouseItemID] ' 
								               +	@searchPositionColumn + @SortFields + @DefaultSortFields + ',
											   (SELECT COUNT(numProId) 
													FROM 
														PromotionOffer PO
													WHERE 
														numDomainId='+CAST(@numDomainID AS VARCHAR(20))+' 
														AND ISNULL(bitEnabled,0)=1
														AND ISNULL(bitAppliesToSite,0)=1 
														AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
														AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
														AND (1 = (CASE 
																	WHEN tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID IN (SELECT numItemGroupID FROM ItemGroups WHERE numProfileItem=I.numItemCode AND numDomainID='+CAST(@numDomainID AS VARCHAR(20))+' ) UNION SELECT I.numItemCode)) > 0 THEN 1 ELSE 0 END)
																	WHEN tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=I.numItemClassification) > 0 THEN 1 ELSE 0 END)
																	ELSE 0
																END)
															OR
															1 = (CASE 
																	WHEN tintDiscoutBaseOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 AND numValue IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID IN (SELECT numItemGroupID FROM ItemGroups WHERE numProfileItem=I.numItemCode AND numDomainID='+CAST(@numDomainID AS VARCHAR(20))+' ) UNION SELECT I.numItemCode)) > 0 THEN 1 ELSE 0 END)
																	WHEN tintDiscoutBaseOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=2 AND numValue=I.numItemClassification) > 0 THEN 1 ELSE 0 END)
																	WHEN tintDiscoutBaseOn = 3 AND tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(SimilarItems.numItemCode) FROM SimilarItems WHERE numParentItemCode IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND SimilarItems.numItemCode=I.numItemCode))>0 THEN 1 ELSE 0 END)
																	WHEN tintDiscoutBaseOn = 3 AND tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(SimilarItems.numItemCode) FROM SimilarItems WHERE SimilarItems.numItemCode IN(I.numItemCode) AND numParentItemCode IN (SELECT numItemCode FROM Item WHERE numItemClassification IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2)))>0 THEN 1 ELSE 0 END)
																	ELSE 0
																END)
															)
													)  IsOnSale
								
                                         FROM      
                                                      Item AS I
                                         INNER JOIN   ItemCategory IC   ON   I.numItemCode = IC.numItemID
                                         LEFT JOIN    ItemImages IM     ON   I.numItemCode = IM.numItemCode AND IM.bitdefault=1 AND IM.bitIsImage = 1
                                         INNER JOIN   Category C        ON   IC.numCategoryID = C.numCategoryID
                                         INNER JOIN   SiteCategories SC ON   IC.numCategoryID = SC.numCategoryID
                                         INNER JOIN   eCommerceDTL E    ON   E.numDomainID = I.numDomainID AND E.numSiteId = SC.numSiteID ' + @Join + ' ' + @DefaultSortJoin + ' 
                                         OUTER APPLY (SELECT SUM(numOnHand) numOnHand FROM WareHouseItems W 
													  WHERE  W.numItemID = I.numItemCode
                                                      AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
                                                      AND W.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + ') AS W  
										 OUTER APPLY (SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems W 
													  WHERE  W.numItemID = I.numItemCode 
													  AND W.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
													  AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))) AS W1
										 OUTER APPLY (SELECT SUM(numAllocation) numAllocation FROM WareHouseItems W2 
													  WHERE  W2.numItemID = I.numItemCode
                                                      AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
                                                      AND W2.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + ') AS W2  			  
										 CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numSaleUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOM) AS UOM
										 CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numPurchaseUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOMPurchase) AS UOMPurchase '

			IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
			BEGIN
				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCode TP ON TP.numItemCode = I.numItemCode ' 
			END
			
			IF (SELECT COUNT(*) FROM #tmpItemCat) > 0
			BEGIN
				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCat TC ON TC.numCategoryID = IC.numCategoryID '
				SET @strSQL = @strSQL + ' LEFT JOIN #tmpOnHandItems TOIC ON TOIC.numItemID = I.numItemCode '
			END
			ELSE IF @numCategoryID>0
			BEGIN
				SET @Where = REPLACE(@Where,'WHERE',' WHERE IC.numCategoryID = ' + CONVERT(VARCHAR(10),@numCategoryID) + ' AND ')
			END 
			
			IF @tintDisplayCategory = 1
			BEGIN
				SET @Where = @Where + ' AND (ISNULL(W.numOnHand, 0) > 0 OR ISNULL(W2.numAllocation, 0) > 0) '
			END
			
			--PRINT @Where
			SET @strSQL = @strSQL + @Where
			
			IF LEN(@FilterRegularCondition) > 0
			BEGIN
				SET @strSQL = @strSQL + @FilterRegularCondition
			END                                         
			
---PRINT @SortString + ',' + @DefaultSort + ' ' + @vcSort 
	
            IF LEN(@whereNumItemCode) > 0
                                        BEGIN
								              SET @strSQL = @strSQL + @filterByNumItemCode	
								    	END
            SET @strSQL = @strSQL +     '   )
                                        , ItemSorted AS (SELECT  ROW_NUMBER() OVER ( ORDER BY  ' + @SortString + @DefaultSort + ' ' + @vcSort + ' ) AS Rownumber,* FROM  Items WHERE RowID = 1)'
            
            DECLARE @fldList AS NVARCHAR(MAX)
			SELECT @fldList=COALESCE(@fldList + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_C') FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=5
            
            SET @strSQL = @strSQL +' SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,UOMPurchaseConversionFactor,InStock,CategoryDesc,numWareHouseItemID,IsOnSale
                                     INTO #tempItem FROM ItemSorted ' 
--                                        + 'WHERE Rownumber > ' + CONVERT(VARCHAR(10), @firstRec) + '
--                                        AND   Rownumber < ' + CONVERT(VARCHAR(10), @lastRec) + '
--                                        order by Rownumber;' 
            
            SET @strSQL = @strSQL + ' SELECT * INTO #tmpPagedItems FROM #tempItem WHERE Rownumber > ' + CONVERT(VARCHAR(10), @firstRec) + '
                                      AND   Rownumber < ' + CONVERT(VARCHAR(10), @lastRec) + '
                                      order by Rownumber '
                                                                  
            IF LEN(ISNULL(@fldList,''))>0
            BEGIN
				PRINT 'flds1'
				SET @strSQL = @strSQL +'SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,UOMPurchaseConversionFactor,InStock,CategoryDesc,numWareHouseItemID,IsOnSale,' + @fldList + '
                                        FROM #tmpPagedItems I left join (
			SELECT  t2.RecId, REPLACE(t1.Fld_label,'' '','''') + ''_C'' as Fld_label, 
				CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
								 WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
								 WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
								 ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
			JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 5 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
			AND t2.RecId IN (SELECT numItemCode FROM #tmpPagedItems )
			) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList + ')) AS pvt on I.numItemCode=pvt.RecId order by Rownumber;' 	
			END 
			ELSE
			BEGIN
				PRINT 'flds2'
				SET @strSQL = @strSQL +' SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,UOMPurchaseConversionFactor,InStock,CategoryDesc,numWareHouseItemID
                                         FROM  #tmpPagedItems order by Rownumber;'
			END               
                                        
            
--            PRINT(@strSQL)        
--            EXEC ( @strSQL) ; 
			                           
            SET @strSQL = @strSQL + ' SELECT @TotRecs = COUNT(*) FROM #tempItem '                            
--             SET @strSQL = 'SELECT @TotRecs = COUNT(*) 
--                           FROM Item AS I
--                           INNER JOIN   ItemCategory IC ON I.numItemCode = IC.numItemID
--                           INNER JOIN   Category C ON IC.numCategoryID = C.numCategoryID
--                           INNER JOIN   SiteCategories SC ON IC.numCategoryID = SC.numCategoryID ' --+ @Where
--             
--			 IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
--			 BEGIN
--				 SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCode TP ON TP.numItemCode = I.numItemCode ' 
--			 END
--			
--			 IF (SELECT COUNT(*) FROM #tmpItemCat) > 0
--			 BEGIN
--				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCat TC ON TC.numCategoryID = IC.numCategoryID '
--				SET @strSQL = @strSQL + ' INNER JOIN #tmpOnHandItems TOIC ON TOIC.numItemID = I.numItemCode '
--			 END
--			 ELSE IF @numCategoryID>0
--			 BEGIN
--				SET @Where = REPLACE(@Where,'WHERE',' WHERE IC.numCategoryID = ' + CONVERT(VARCHAR(10),@numCategoryID) + ' AND ')
--			 END 
--			
--			 SET @strSQL = @strSQL + @Where  
--			 
--			 IF LEN(@FilterRegularCondition) > 0
--			 BEGIN
--				SET @strSQL = @strSQL + @FilterRegularCondition
--			 END 
			
		--			--SELECT * FROM  #tempAvailableFields
		--SELECT * FROM  #tmpItemCode
		--SELECT * FROM  #tmpItemCat
		----SELECT * FROM  #fldValues
		--SELECT * FROM  #tempSort
		--SELECT * FROM  #fldDefaultValues
		--SELECT * FROM  #tmpOnHandItems
		----SELECT * FROM  #tmpPagedItems

			 PRINT  LEN(@strSQL)
			 DECLARE @tmpSQL AS VARCHAR(MAX)
			 SET @tmpSQL = @strSQL
			 PRINT  @tmpSQL
			 
             EXEC sp_executeSQL @strSQL, N'@TotRecs INT OUTPUT', @TotRecs OUTPUT
             
             
        DECLARE @tintOrder AS TINYINT                                                  
		DECLARE @vcFormFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(1)                                             
		DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
		DECLARE @numListID AS NUMERIC(9)                                                  
		DECLARE @WhereCondition VARCHAR(MAX)                       
		DECLARE @numFormFieldId AS NUMERIC  
		DECLARE @vcFieldType CHAR(1)
		                  
		SET @tintOrder=0                                                  
		SET @WhereCondition =''                 
                   
              
		CREATE TABLE #tempAvailableFields(numFormFieldId  NUMERIC(9),vcFormFieldName NVARCHAR(50),
		vcFieldType CHAR(1),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),
		vcListItemType CHAR(1),intRowNum INT,vcItemValue VARCHAR(3000))
		   
		INSERT INTO #tempAvailableFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
				,numListID,vcListItemType,intRowNum)                         
		SELECT  Fld_id,REPLACE(Fld_label,' ','') AS vcFormFieldName,
			   CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType,
				fld_type as vcAssociatedControlType,
				ISNULL(C.numListID, 0) numListID,
				CASE WHEN C.numListID > 0 THEN 'L'
					 ELSE ''
				END vcListItemType,ROW_NUMBER() OVER (ORDER BY Fld_id ASC) AS intRowNum
		FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
		WHERE   C.numDomainID = @numDomainId
				AND GRP_ID IN (5)

		  
		SELECT * FROM #tempAvailableFields
		
		/*
		DROP TABLE #tempAvailableFields
		DROP TABLE #tmpItemCode
		DROP TABLE #tmpItemCat
		*/	

		IF OBJECT_ID('tempdb..#tempAvailableFields') IS NOT NULL
		DROP TABLE #tempAvailableFields
		
		IF OBJECT_ID('tempdb..#tmpItemCode') IS NOT NULL
		DROP TABLE #tmpItemCode
		
		IF OBJECT_ID('tempdb..#tmpItemCat') IS NOT NULL
		DROP TABLE #tmpItemCat
		
		IF OBJECT_ID('tempdb..#fldValues') IS NOT NULL
		DROP TABLE #fldValues
		
		IF OBJECT_ID('tempdb..#tempSort') IS NOT NULL
		DROP TABLE #tempSort
		
		IF OBJECT_ID('tempdb..#fldDefaultValues') IS NOT NULL
		DROP TABLE #fldDefaultValues
		
		IF OBJECT_ID('tempdb..#tmpOnHandItems') IS NOT NULL
		DROP TABLE #tmpOnHandItems

		IF OBJECT_ID('tempdb..#tmpPagedItems') IS NOT NULL
		DROP TABLE #tmpPagedItems
    END



/****** Object:  StoredProcedure [dbo].[USP_UpdateCartItem]    Script Date: 11/08/2011 17:49:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_UpdateCartItem' ) 
                    DROP PROCEDURE USP_UpdateCartItem
                    Go
CREATE  PROCEDURE [dbo].[USP_UpdateCartItem]
(
		 @numUserCntId numeric(9,0),
	 @numDomainId numeric(9,0),
	 @vcCookieId varchar(100),
		-- @bitUserType BIT, --if 0 then anonomyous user 1 for logi user
		 @strXML text,
		 @postselldiscount INT=0
)
AS
if convert(varchar(10),@strXML) <>''
BEGIN
		declare @i int
		EXEC sp_xml_preparedocument @i OUTPUT, @strXML 
		IF @numUserCntId <> 0
		BEGIN
		update CartItems
		set CartItems.monPrice = ox.monPrice,CartItems.numUnitHour = ox.numUnitHour,CartItems.monTotAmtBefDiscount = ox.monTotAmtBefDiscount,
		CartItems.monTotAmount = ox.monTotAmount,CartItems.bitDiscountType =ISNULL(ox.bitDiscountType,0),
		CartItems.vcShippingMethod =ox.vcShippingMethod,CartItems.numServiceTypeID =ox.numServiceTypeID,
		CartItems.decShippingCharge =ox.decShippingCharge,CartItems.tintServiceType =ox.tintServiceType,
		CartItems.numShippingCompany =ox.numShippingCompany,CartItems.dtDeliveryDate =ox.dtDeliveryDate,
		CartItems.fltDiscount =ox.fltDiscount,CartItems.vcCookieId = ox.vcCookieId,CartItems.vcCoupon=(CASE WHEN CartItems.vcCoupon<>'' THEN CartItems.vcCoupon ELSE ox.vcCoupon END)


		from OpenXml(@i, '/NewDataSet/Table1',2)
		with (numDomainId NUMERIC(9,0),numUserCntId NUMERIC(9,0),vcCookieId VARCHAR(200),numItemCode NUMERIC(9,0), monPrice money, numUnitHour FLOAT,
		monTotAmtBefDiscount MONEY,monTotAmount MONEY ,bitDiscountType BIT,vcShippingMethod VARCHAR(200),numServiceTypeID NUMERIC(9,0),decShippingCharge money,
		tintServiceType TINYINT,numShippingCompany NUMERIC(9,0),dtDeliveryDate DATETIME,fltDiscount DECIMAL(18,2),vcCoupon VARCHAR(200) ) ox
		where CartItems.numDomainId = ox.numDomainId AND CartItems.numUserCntId = ox.numUserCntId  AND 
		CartItems.numItemCode = ox.numItemCode
		END 
		ELSE
		BEGIN
		update CartItems
		set CartItems.monPrice = ox.monPrice,CartItems.numUnitHour = ox.numUnitHour,CartItems.monTotAmtBefDiscount = ox.monTotAmtBefDiscount,
		CartItems.monTotAmount = ox.monTotAmount,CartItems.bitDiscountType =ISNULL(ox.bitDiscountType,0),
		CartItems.vcShippingMethod =ox.vcShippingMethod,CartItems.numServiceTypeID =ox.numServiceTypeID,
		CartItems.decShippingCharge =ox.decShippingCharge,CartItems.tintServiceType =ox.tintServiceType,
		CartItems.numShippingCompany =ox.numShippingCompany,CartItems.dtDeliveryDate =ox.dtDeliveryDate,
		CartItems.fltDiscount =ox.fltDiscount,CartItems.vcCoupon=(CASE WHEN CartItems.vcCoupon<>'' THEN CartItems.vcCoupon ELSE ox.vcCoupon END)


		from OpenXml(@i, '/NewDataSet/Table1',2)
		with (numDomainId NUMERIC(9,0),numUserCntId NUMERIC(9,0),vcCookieId VARCHAR(200),numItemCode NUMERIC(9,0), monPrice money, numUnitHour FLOAT,
		monTotAmtBefDiscount MONEY,monTotAmount MONEY ,bitDiscountType BIT,vcShippingMethod VARCHAR(200),numServiceTypeID NUMERIC(9,0),decShippingCharge MONEY,
		tintServiceType TINYINT,numShippingCompany NUMERIC(9,0),dtDeliveryDate DATETIME,fltDiscount DECIMAL(18,2),vcCoupon VARCHAR(200) ) ox
		where CartItems.numDomainId = ox.numDomainId AND CartItems.numUserCntId = 0  AND 
		CartItems.numItemCode = ox.numItemCode
		 And CartItems.vcCookieId = ox.vcCookieId
		END

		exec sp_xml_removedocument @i

	IF(@postselldiscount=0)

	BEGIN
	------Apply Promotion Code
	DECLARE @numPromotionID AS NUMERIC(18,0)=0
	DECLARE @chrItemType AS CHAR(1)
	DECLARE @numItemGroup AS NUMERIC(18,0)
	DECLARE @ItemDesc AS VARCHAR(1000)
	DECLARE @numItemClassification AS NUMERIC(18,0)

	DECLARE @numWarehouseItemID NUMERIC(18,0)
	DECLARE @tintOfferTriggerValueType int
DECLARE @fltOfferTriggerValue float
DECLARE @tintOfferBasedOn int
DECLARE @fltDiscountValue float
DECLARE @tintDiscountType int
DECLARE @tintDiscoutBaseOn int
DECLARE @vcItems varchar(MAX)
DECLARE @vcPromotionDescription  varchar(MAX)
DECLARE  @numItemCode NUMERIC(18, 0)
DECLARE @monTotAmount money
DECLARE @numUnitHour NUMERIC(18,0)
DECLARE @PromotionDesc VARCHAR(MAX)
DECLARE @bitCouponRequired BIT
DECLARE @vcCoupon VARCHAR(200)
	--create table #TempPrmotionP
	--(
	--	PRowID int IDENTITY(1,1), 
	--	numPromotionId numeric,
	--	numUnitHour numeric,
	--	monTotAmount money,
	--	numItemCode numeric,
	--	numCartId numeric
	--)
	DECLARE @Pcount INT=0
	DECLARE @PIcount INT=1
	--INSERT INTO #TempPrmotionP
	--	SELECT PromotionID,numUnitHour,monTotAmount,numItemCode,numCartId FROM CartItems WHERE vcCookieId=@vcCookieId AND numUserCntId=@numUserCntId order by numCartId


	--SET @Pcount=(SELECT COUNT(*) FROM #TempPrmotionP)
	DECLARE @count INT=0
	DECLARE @ICOUNT INT=1
	DECLARE @numACartId NUMERIC
	DECLARE @bitParentPromotion BIT=0
	---Update Parent Promotion
	--IF(@Pcount>0)
	--BEGIN
	--	WHILE(@PIcount<=@Pcount)
	--	BEGIN
		
	--			SET @numItemCode=(SELECT TOP 1 numItemCode FROM #TempPrmotionP WHERE PRowID=@PIcount)
	--			SET @numACartId=(SELECT TOP 1 numCartId FROM #TempPrmotionP WHERE PRowID=@PIcount)
	--			SELECT
	--			@ItemDesc = Item.txtItemDesc,
	--			@chrItemType = charItemType,
	--			@numItemGroup = numItemGroup,
	--			@numItemClassification = numItemClassification,
	--			@numWarehouseItemID = (CASE WHEN ISNULL(@numWarehouseItemID,0) > 0 THEN @numWarehouseItemID ELSE WareHouseItems.numWareHouseItemID END)
	--		FROM
	--			Item
	--		OUTER APPLY
	--		(
	--			SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems WHERE numItemID=@numItemCode AND (numWareHouseItemID = @numWarehouseItemID OR ISNULL(@numWarehouseItemID,0) = 0)
	--		) AS WareHouseItems
	--		WHERE
	--			Item.numItemCode = @numItemCode
	--		SET @numPromotionID=0
	--		SELECT TOP 1
	--			@numPromotionID = numProId,
	--			@PromotionDesc=(SELECT CONCAT('Buy '
	--						,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
	--						,CASE tintOfferBasedOn
	--								WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
	--								WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
	--							END
	--						,' & get '
	--						, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
	--						, CASE 
	--							WHEN tintDiscoutBaseOn = 1 THEN 
	--								CONCAT
	--								(
	--									CONCAT(' "',(SELECT Stuff((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, '')), '"')
	--									,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
	--									,'.'
	--								)
	--							WHEN tintDiscoutBaseOn = 2 THEN 
	--								CONCAT
	--								(
	--									CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(',',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, '')), '"')
	--									,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
	--									,'.'
	--								)
	--							WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
	--							ELSE '' 
	--						END
	--					) AS vcPromotionDescription)
	--		FROM 
	--			PromotionOffer PO
	--		WHERE 
	--			numDomainId=@numDomainID 
	--			AND ISNULL(bitEnabled,0)=1
	--			AND ISNULL(bitAppliesToSite,0)=1 
	--			AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
	--			AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
	--			AND 1 = (CASE 
	--						WHEN tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=@numItemCode) > 0 THEN 1 ELSE 0 END)
	--						WHEN tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=@numItemClassification) > 0 THEN 1 ELSE 0 END)
	--						ELSE 0
	--					END)
	--		ORDER BY
	--			CASE 
	--				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
	--				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
	--				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
	--				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
	--				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
	--				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
	--			END
	--			PRINT(@numItemCode)
	--			PRINT(@numPromotionID)
	--		--IF(ISNULL(@numPromotionID,0)>0)
	--		--BEGIN
	--			SET @bitParentPromotion=0
	--			IF(ISNULL(@numPromotionID,0)>0)
	--			BEGIN
	--				SET @bitParentPromotion=1
	--			END
	--			UPDATE 
	--				CartItems
	--			SET 
	--				bitParentPromotion=@bitParentPromotion,
	--				PromotionID=@numPromotionID,
	--				PromotionDesc=@PromotionDesc
	--			WHERE numCartId=@numACartId

	--			SET @PIcount=@PIcount+1
	--		--END
	--	END
	--END
	--SET @PIcount=1
	create table #TempPrmotion
	(
		PRowID int IDENTITY(1,1), 
		numPromotionId numeric,
		numUnitHour numeric,
		monTotAmount money,
		numItemCode numeric,
		numCartId numeric,
		vcCoupon VARCHAR(300)
	)
	SET @Pcount =0
	SET @PIcount=1
	INSERT INTO #TempPrmotion
		SELECT PromotionID,numUnitHour,monTotAmount,numItemCode,numCartId,vcCoupon FROM CartItems WHERE vcCookieId=@vcCookieId  AND numUserCntId=@numUserCntId AND ISNULL(bitParentPromotion,0)=1 order by numCartId
	SET @Pcount=(SELECT COUNT(*) FROM #TempPrmotion)
	DECLARE @vcPCoupon VARCHAR(300)
	---Update Triggered Promotion
	UPDATE CartItems SET fltDiscount=0,monTotAmount=(monPrice*numUnitHour) WHERE vcCookieId=@vcCookieId  AND numUserCntId=@numUserCntId AND ISNULL(fltDiscount,0)>0

	IF(@Pcount>0)
	BEGIN
		WHILE(@PIcount<=@Pcount)
		BEGIN
			
			SET @numPromotionID=(SELECT TOP 1 numPromotionId FROM #TempPrmotion WHERE PRowID=@PIcount)
			SET @numUnitHour=(SELECT TOP 1 numUnitHour FROM #TempPrmotion WHERE PRowID=@PIcount)
			SET @monTotAmount=(SELECT TOP 1 monTotAmount FROM #TempPrmotion WHERE PRowID=@PIcount)
			
			SET @vcPCoupon=(SELECT TOP 1 vcCoupon FROM #TempPrmotion WHERE PRowID=@PIcount)

			CREATE TABLE #tempitems(
				item VARCHAR(MAX)
			)
			


			SELECT 
			@tintOfferTriggerValueType=tintOfferTriggerValueType
			,@fltOfferTriggerValue=fltOfferTriggerValue
			,@tintOfferBasedOn=tintOfferBasedOn
			,@fltDiscountValue=fltDiscountValue
			,@tintDiscountType=tintDiscountType
			,@tintDiscoutBaseOn=tintDiscoutBaseOn
			,@bitCouponRequired=bitRequireCouponCode
			,@vcPromotionDescription=(SELECT CONCAT('Buy '
					,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
					,CASE tintOfferBasedOn
							WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
							WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
						END
					,' & get '
					, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
					, CASE 
						WHEN tintDiscoutBaseOn = 1 THEN 
							CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 2 THEN 
							CONCAT
							(
								CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(',',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
						ELSE '' 
					END
				) AS vcPromotionDescription)
		FROM
			PromotionOffer PO
		WHERE
			numDomainId=@numDomainID 
			AND numProId = @numPromotionID

			IF(@tintOfferBasedOn=2)
			BEGIN
				SET @numUnitHour=(SELECT SUM(numUnitHour) FROM #TempPrmotion WHERE numPromotionId=@numPromotionID)
				SET @monTotAmount=(SELECT SUM(monTotAmount) FROM #TempPrmotion WHERE numPromotionId=@numPromotionID)
			END
			IF(@tintDiscoutBaseOn=1)
			BEGIN
				INSERT INTO #tempitems SELECT numValue FROM PromotionOfferItems WHERE numProId=@numPromotionID AND tintRecordType=6 AND tintType=1
			END
			IF(@tintDiscoutBaseOn=2)
			BEGIN
				INSERT INTO #tempitems SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemClassification IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=@numPromotionID AND tintRecordType=6)
			END

			IF(@tintDiscoutBaseOn=3 AND @tintOfferBasedOn=1)
			BEGIN
				INSERT INTO #tempitems SELECT SimilarItems.numItemCode FROM SimilarItems WHERE numParentItemCode IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=@numPromotionID AND tintRecordType=5 AND tintType=1)
			END

			IF(@tintDiscoutBaseOn=3 AND @tintOfferBasedOn=2)
			BEGIN
				INSERT INTO #tempitems SELECT SimilarItems.numItemCode FROM SimilarItems WHERE numParentItemCode IN (SELECT numItemCode FROM Item WHERE numItemClassification IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=@numPromotionID AND tintRecordType=5 AND tintType=2))
			END

			create table #Temp
			(
				RowID int IDENTITY(1,1), 
				numCartId numeric
			)
	
			SET @count =0
			SET @count=(SELECT COUNT(*) FROM CartItems WHERE vcCookieId=@vcCookieId  AND numUserCntId=@numUserCntId AND numItemCode IN (SELECT item FROM #tempitems) 
				-- AND 
				--1=(CASE WHEN @bitCouponRequired=1 AND vcCoupon=@vcCoupon THEN 1 WHEN @bitCouponRequired=0 THEN 1 ELSE 0 END)
			)
			IF(1=(CASE WHEN @bitCouponRequired=1 AND @vcPCoupon='' THEN 0 ELSE 1 END))
			BEGIN
			IF(@count>0)
				BEGIN
					INSERT INTO 
						#Temp 
					SELECT 
						numCartId
					FROM 
						CartItems
					WHERE
						vcCookieId=@vcCookieId  AND numUserCntId=@numUserCntId and  
						numItemCode IN (SELECT item FROM #tempitems) 
						-- AND 
						--1=(CASE WHEN @bitCouponRequired=1 AND vcCoupon=@vcCoupon THEN 1 WHEN @bitCouponRequired=0 THEN 1 ELSE 0 END)
						--AND
						--ISNULL(bitParentPromotion,0)=0
					SET @count=(SELECT COUNT(*) FROM #Temp)
					PRINT @count
					SET @ICOUNT=1
					IF(@numPromotionID>0)
					BEGIN
					WHILE(@ICOUNT<=@count)
					BEGIN
						
						SET @numACartId=(SELECT TOP 1 numCartId FROM #Temp WHERE RowID=@ICOUNT)
						IF(@tintOfferTriggerValueType=1 AND @numUnitHour>=@fltOfferTriggerValue)
						BEGIN
							if(@tintDiscountType=1)
							BEGIN
								
								UPDATE 
									CartItems 
								SET 
									monTotAmount=(monTotAmount-((monTotAmount*@fltDiscountValue)/100)),
									PromotionID=@numPromotionID,
									PromotionDesc=@vcPromotionDescription,
									bitDiscountType=0,
									fltDiscount=(monTotAmount*@fltDiscountValue)/100
								WHERE 
									numCartId=@numACartId  AND monTotAmount=(monPrice*numUnitHour)
								BREAK;
							END
							if(@tintDiscountType=2)
							BEGIN
								IF((SELECT monTotAmount FROM CartItems WHERE numCartId=@numACartId)>=@fltDiscountValue)
								BEGIN
									UPDATE 
										CartItems 
									SET 
										monTotAmount=(monTotAmount-@fltDiscountValue),
										PromotionID=@numPromotionID ,
										PromotionDesc=@vcPromotionDescription,
										bitDiscountType=1,
										fltDiscount=@fltDiscountValue
									WHERE 
										numCartId=@numACartId  AND monTotAmount=(monPrice*numUnitHour)
									BREAK;
								END
							END
							if(@tintDiscountType=3)
							BEGIN
								IF((SELECT numUnitHour FROM CartItems WHERE numCartId=@numACartId)>=@fltDiscountValue)
								BEGIN
								UPDATE 
									CartItems 
								SET 
									monTotAmount=(monTotAmount-(@fltDiscountValue*monPrice)),
									PromotionID=@numPromotionID ,
									PromotionDesc=@vcPromotionDescription,
									bitDiscountType=1,
									fltDiscount=(@fltDiscountValue*monPrice)
								WHERE 
									numCartId=@numACartId  AND monTotAmount=(monPrice*numUnitHour)
								BREAK;
								END
							END
						END
						IF(@tintOfferTriggerValueType=2 AND @monTotAmount>=@fltOfferTriggerValue)
						BEGIN
							if(@tintDiscountType=1)
							BEGIN
								UPDATE 
									CartItems 
								SET 
									monTotAmount=(monTotAmount-((monTotAmount*@fltDiscountValue)/100)),
									PromotionID=@numPromotionID,
									PromotionDesc=@vcPromotionDescription,
									bitDiscountType=0,
									fltDiscount=((monTotAmount*@fltDiscountValue)/100)
								WHERE 
									numCartId=@numACartId  AND monTotAmount=(monPrice*numUnitHour)
								BREAK;
							END
							if(@tintDiscountType=2)
							BEGIN
								IF((SELECT monTotAmount FROM CartItems WHERE numCartId=@numACartId)>=@fltDiscountValue)
								BEGIN
								UPDATE 
									CartItems 
								SET 
									monTotAmount=(monTotAmount-@fltDiscountValue),
									PromotionID=@numPromotionID ,
									PromotionDesc=@vcPromotionDescription,
									bitDiscountType=1,
									fltDiscount=@fltDiscountValue
								WHERE 
									numCartId=@numACartId AND
									monTotAmount>=@fltDiscountValue  AND monTotAmount=(monPrice*numUnitHour)
								BREAK;
								END
							END
							if(@tintDiscountType=3)
							BEGIN
								IF((SELECT numUnitHour FROM CartItems WHERE numCartId=@numACartId)>=@fltDiscountValue)
								BEGIN
								UPDATE 
									CartItems 
								SET 
									monTotAmount=(monTotAmount-(@fltDiscountValue*monPrice)),
									PromotionID=@numPromotionID ,
									PromotionDesc=@vcPromotionDescription,
									bitDiscountType=1,
									fltDiscount=(@fltDiscountValue*monPrice)
								WHERE 
									numCartId=@numACartId AND
									numUnitHour>=@fltDiscountValue   AND monTotAmount=(monPrice*numUnitHour)
								BREAK;
								END
							END
						END
						SET @ICOUNT=@ICOUNT+1
					END
					END
				END
			END
			SET @PIcount=@PIcount+1
			DROP TABLE #Temp
			
			DROP TABLE #tempitems
		END
	END

	END
END



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ApplyCouponCodetoCart')
DROP PROCEDURE USP_ApplyCouponCodetoCart
GO
CREATE PROCEDURE [dbo].[USP_ApplyCouponCodetoCart]
	@numDomainID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0) OUTPUT,
	@cookieId VARCHAR(MAX)=NULL,
	@numUserCntId numeric=0,
	@vcSendCoupon VARCHAR(200)=0
AS
BEGIN
	DECLARE @PromotionID NUMERIC(18,0)
	DECLARE @PromotionDesc VARCHAR(500)
	DECLARE @tintOfferBasedOn INT
	IF((SELECT COUNT(numCartId) FROM CartItems WHERE numDomainId=@numDomainID AND vcCookieId=@cookieId AND numUserCntId=@numUserCntId AND vcCoupon=@vcSendCoupon)=0)
	BEGIN
		SET @numItemCode=1
		 SElECT 
			TOP 1 @PromotionID=PO.numProId,
			@tintOfferBasedOn=PO.tintOfferBasedOn,
			@PromotionDesc=(CONCAT('Buy '
					,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
					,CASE tintOfferBasedOn
							WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
							WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
						END
					,' & get '
					, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
					, CASE 
						WHEN tintDiscoutBaseOn = 1 THEN 
							CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 2 THEN 
							CONCAT
							(
								CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(',',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
						ELSE '' 
					END
				))
		 FROM
			PromotionOffer  AS PO
		 WHERE
			PO.numDomainId=@numDomainID AND
			PO.txtCouponCode=@vcSendCoupon

		UPDATE
			CartItems
		SET
			PromotionID=@PromotionID,
			PromotionDesc=@PromotionDesc,
			bitParentPromotion=1,
			vcCoupon=@vcSendCoupon
		WHERE
			numDomainId=@numDomainID AND
			vcCookieId=@cookieId AND
			numUserCntId=@numUserCntId AND
			numItemCode=(SELECT TOP 1 
							C.numItemCode 
						 FROM 
							CartItems  AS C
						 LEFT JOIN 
							Item AS I
						 ON
							C.numItemCode=I.numItemCode
						 WHERE
							C.numDomainId=@numDomainID AND
							C.vcCookieId=@cookieId AND
							C.numUserCntId=@numUserCntId AND 
							1 = (CASE 
								WHEN @tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@PromotionID AND tintRecordType=5 AND tintType=1 AND numValue=C.numItemCode) > 0 THEN 1 ELSE 0 END)
								WHEN @tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@PromotionID AND tintRecordType=5 AND tintType=2 AND numValue=I.numItemClassification) > 0 THEN 1 ELSE 0 END)
								ELSE 0
							END)
						 )
	END
	ELSE
	BEGIN
		SET @numItemCode=2
	END
	SELECT @numItemCode AS ResOutput
END


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCartItemPromotionDetail')
DROP PROCEDURE USP_GetCartItemPromotionDetail
GO
CREATE PROCEDURE [dbo].[USP_GetCartItemPromotionDetail]
	@numDomainID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@numUOMID NUMERIC(18,0),
	@numShippingCountry AS NUMERIC(18,0),
	@cookieId VARCHAR(MAX)=NULL,
	@numUserCntId numeric=0,
	@vcSendCoupon VARCHAR(200)=0
AS
BEGIN
	DECLARE @vPromotionDesc VARCHAR(MAX)
	IF(@vcSendCoupon='0')
	BEGIN

	DECLARE @vcItemName AS VARCHAR(500)
	DECLARE @chrItemType AS CHAR(1)
	DECLARE @numItemGroup AS NUMERIC(18,0)
	DECLARE @ItemDesc AS VARCHAR(1000)
	DECLARE @numItemClassification AS NUMERIC(18,0)

	DECLARE @numWarehouseItemID NUMERIC(18,0)

	
	DECLARE @decmPrice DECIMAL(18,2)
	SET @decmPrice=(select ISNULL(SUM(monTotAmount),0) from CartItems where numDomainId=@numDomainID AND vcCookieId=@cookieId AND numUserCntId=@numUserCntId)

	DECLARE @numShippingPromotionID AS NUMERIC(18,0)=0
	DECLARE @numPromotionID AS NUMERIC(18,0)=0

	SET @numShippingPromotionID=(SELECT TOP 1 PromotionID from CartItems AS C 
	LEFT JOIN PromotionOffer AS P 
	ON C.PromotionID=P.numProId
	where C.numDomainId=@numDomainID AND C.vcCookieId=@cookieId AND C.numUserCntId=@numUserCntId
	AND (ISNULL(bitFixShipping1,0)=1 OR ISNULL(bitFixShipping2,0)=1 OR ISNULL(bitFreeShiping,0)=1)  AND ISNULL(C.bitParentPromotion,0)=1
	AND 1=(CASE WHEN P.tintOfferTriggerValueType=1 AND C.numUnitHour>=fltOfferTriggerValue THEN 1 WHEN P.tintOfferTriggerValueType=2 AND monTotAmount>=fltOfferTriggerValue THEN 1 ELSE 0 END))
	PRINT(@decmPrice)

	IF(ISNULL(@numPromotionID,0)=0)
	BEGIN
	-- GET ITEM DETAILS
	SELECT
		@vcItemName = vcItemName,
		@ItemDesc = Item.txtItemDesc,
		@chrItemType = charItemType,
		@numItemGroup = numItemGroup,
		@numItemClassification = numItemClassification,
		@numWarehouseItemID = (CASE WHEN ISNULL(@numWarehouseItemID,0) > 0 THEN @numWarehouseItemID ELSE WareHouseItems.numWareHouseItemID END)
	FROM
		Item
	OUTER APPLY
	(
		SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems WHERE numItemID=@numItemCode AND (numWareHouseItemID = @numWarehouseItemID OR ISNULL(@numWarehouseItemID,0) = 0)
	) AS WareHouseItems
	WHERE
		Item.numItemCode = @numItemCode


	--IF @chrItemType = 'P' AND ISNULL(@numWarehouseItemID,0) = 0
	--BEGIN
	--	RAISERROR('WAREHOUSE_DOES_NOT_EXISTS',16,1)
	--END

	

	-- CHECK IF PROMOTION CAN BE TRIGGERED FROM THIS ITEM
	
	DECLARE @bitCoupon bit
	DECLARE @vcCouponCode VARCHAR(200)
	SELECT TOP 1
		@numPromotionID = numProId 
	FROM 
		PromotionOffer PO
	WHERE 
		numDomainId=@numDomainID 
		AND ISNULL(bitEnabled,0)=1
		AND ISNULL(bitAppliesToSite,0)=1 
		AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
		AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
		AND (1 = (CASE 
					WHEN tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=@numItemCode) > 0 THEN 1 ELSE 0 END)
					WHEN tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=@numItemClassification) > 0 THEN 1 ELSE 0 END)
					ELSE 0
				END)
			OR
			1 = (CASE 
					WHEN tintDiscoutBaseOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 AND numValue=@numItemCode) > 0 THEN 1 ELSE 0 END)
					WHEN tintDiscoutBaseOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=2 AND numValue=@numItemClassification) > 0 THEN 1 ELSE 0 END)
					WHEN tintDiscoutBaseOn = 3 AND tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(SimilarItems.numItemCode) FROM SimilarItems WHERE numParentItemCode IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND SimilarItems.numItemCode=@numItemCode))>0 THEN 1 ELSE 0 END)
					WHEN tintDiscoutBaseOn = 3 AND tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(SimilarItems.numItemCode) FROM SimilarItems WHERE SimilarItems.numItemCode IN(@numItemCode) AND numParentItemCode IN (SELECT numItemCode FROM Item WHERE numItemClassification IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2)))>0 THEN 1 ELSE 0 END)
					ELSE 0
				END)
			)
	ORDER BY
		CASE 
			WHEN PO.txtCouponCode IN(SELECT vcCoupon FROM CartItems WHERE numUserCntId=@numUserCntId AND vcCookieId=@cookieId) THEN 1
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 2
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 3
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 4
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 5
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 6
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 7
		END

		END

	
	--IF ISNULL(@numPromotionID,0) > 0
	--BEGIN
		-- Get Top 1 Promotion Based on priority
		

		DECLARE @bitRequireCouponCode VARCHAR(200)

		DECLARE @tintOfferTriggerValueType INT
		DECLARE @fltOfferTriggerValue DECIMAL
		
		SET @vcCouponCode=(SELECT  STUFF((SELECT 
			','+txtCouponCode
		FROM 
			PromotionOffer PO
		WHERE 
			numDomainId=@numDomainID 
			AND ISNULL(bitRequireCouponCode,0)=1
			AND ISNULL(bitEnabled,0)=1
			AND ISNULL(bitAppliesToSite,0)=1 
			AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
			AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
			AND (1 = (CASE 
						WHEN tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=@numItemCode) > 0 THEN 1 ELSE 0 END)
						WHEN tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=@numItemClassification) > 0 THEN 1 ELSE 0 END)
						ELSE 0
					END)
				)
			AND (SELECT COUNT(numCartId) FROM CartItems WHERE vcCoupon=PO.txtCouponCode AND numDomainId=@numDomainID AND numUserCntId=@numUserCntId AND vcCookieId=@cookieId)=0
		ORDER BY
			CASE 
				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
			END
		FOR
                XML PATH('')
              ), 1, 0, ''))
		SET @vcCouponCode= (SELECT SUBSTRING(@vcCouponCode,2,(LEN(@vcCouponCode))))

		

		IF(LEN(@vcCouponCode)>0)
		BEGIN
			SET @bitRequireCouponCode=1
		END

		SELECT @vPromotionDesc=( 
			CASE WHEN PO.bitRequireCouponCode=0 OR
			(PO.bitRequireCouponCode=1 AND (SELECT COUNT(*) FROM CartItems WHERE PromotionID=PO.numProId AND bitParentPromotion=1 AND vcCookieId=@cookieId AND numUserCntId=@numUserCntId)>0)
			
			THEN CONCAT('Buy '
					,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
					,CASE tintOfferBasedOn
							WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
							WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
						END
					,' & get '
					, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
					, CASE 
						WHEN tintDiscoutBaseOn = 1 THEN 
							CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 2 THEN 
							CONCAT
							(
								CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(',',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
						ELSE '' 
					END
				)  ELSE 'Items on Sale' END) 
				
		FROM
			PromotionOffer PO
		WHERE
			numDomainId=@numDomainID 
			AND numProId =@numPromotionID

		IF(LEN(@vcCouponCode)>0)
		BEGIN
			SET @bitRequireCouponCode=1
			SET @vPromotionDesc='Item is on sale'
		END


		IF (@numShippingPromotionID=0)
		BEGIN
			SET @numShippingPromotionID=@numPromotionID
		END
		
		DECLARE @vcShippingDescription VARCHAR(MAX)
		DECLARE @vcQualifiedShipping VARCHAR(MAX)


		SELECT @vcShippingDescription=(CONCAT
				(
					(CASE WHEN ISNULL(bitFixShipping1,0)=1 AND @decmPrice < monFixShipping1OrderAmount THEN CONCAT('Spend $',(monFixShipping1OrderAmount-@decmPrice), CASE WHEN @decmPrice>0 THEN ' more ' ELSE '' END+' and get $',monFixShipping1Charge,' shipping. ') ELSE '' END)
					,(CASE WHEN ISNULL(bitFixShipping2,0)=1 AND @decmPrice < monFixShipping2OrderAmount THEN CONCAT('Spend $',monFixShipping2OrderAmount-@decmPrice, CASE WHEN @decmPrice>0 THEN ' more ' ELSE '' END+' and get $',monFixShipping2Charge,' shipping. ') ELSE '' END)
					,(CASE WHEN (bitFreeShiping=1 AND numFreeShippingCountry=@numShippingCountry) AND @decmPrice<monFreeShippingOrderAmount THEN CONCAT('Spend $',monFreeShippingOrderAmount-@decmPrice, CASE WHEN @decmPrice>0 THEN ' more ' ELSE '' END+' and shipping is FREE! ') ELSE '' END) 
					
				)),
				@vcQualifiedShipping=((CASE WHEN ISNULL(bitFixShipping1,0)=1 AND @decmPrice > monFixShipping1OrderAmount AND 1=(CASE WHEN ISNULL(bitFixShipping2,0)=1 AND @decmPrice<monFixShipping2OrderAmount THEN 1 WHEN ISNULL(bitFixShipping2,0)=0 THEN 1 ELSE 0 END) THEN 'You have qualified for $'+CAST(monFixShipping1Charge AS varchar)+' shipping'
						   WHEN ISNULL(bitFixShipping2,0)=1 AND @decmPrice > monFixShipping2OrderAmount  AND 1=(CASE WHEN ISNULL(bitFreeShiping,0)=1 AND @decmPrice<monFreeShippingOrderAmount THEN 1 WHEN ISNULL(bitFreeShiping,0)=0 THEN 1 ELSE 0 END) THEN 'You have qualified for $'+CAST(monFixShipping2Charge AS varchar)+' shipping'
						   WHEN (ISNULL(bitFreeShiping,0)=1 AND numFreeShippingCountry=@numShippingCountry AND @decmPrice>monFreeShippingOrderAmount) THEN 'You have qualified for Free Shipping' END
					))
		FROM
			PromotionOffer PO
		WHERE
			numDomainId=@numDomainID 
			AND numProId = @numShippingPromotionID

		IF((SELECT COUNT(*) FROM CartItems WHERE numItemCode=@numItemCode AND numDomainId=@numDomainID AND numUserCntId=@numUserCntId AND vcCookieId=@cookieId AND PromotionID>0 AND @vcCouponCode<>'')>0)
		BEGIN
			SET @bitRequireCouponCode='APPLIED';
			SET @vcCouponCode=(SELECT TOP 1 vcCoupon FROM CartItems WHERE numItemCode=@numItemCode AND vcCookieId=@cookieId AND numUserCntId=@numUserCntId AND numDomainId=@numDomainID AND PromotionID>0 AND @vcCouponCode<>'')

			SELECT @vPromotionDesc=( 
				CONCAT('Buy '
						,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
						,CASE tintOfferBasedOn
								WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
								WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
							END
						,' & get '
						, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
						, CASE 
							WHEN tintDiscoutBaseOn = 1 THEN 
								CONCAT
								(
									CONCAT(' "',(SELECT Stuff((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, '')), '"')
									,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
									,'.'
								)
							WHEN tintDiscoutBaseOn = 2 THEN 
								CONCAT
								(
									CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(',',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, '')), '"')
									,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
									,'.'
								)
							WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
							ELSE '' 
						END
					))
				
			FROM
				PromotionOffer PO
			WHERE
				numDomainId=@numDomainID 
				AND txtCouponCode=@vcCouponCode
			PRINT @vPromotionDesc
		END

		SELECT @vPromotionDesc AS vcPromotionDescription,@vcShippingDescription AS vcShippingDescription,@vcQualifiedShipping AS vcQualifiedShipping,ISNULL(@bitRequireCouponCode,'False') AS bitRequireCouponCode,@vcCouponCode AS vcCouponCode,
		ISNULL(@tintOfferTriggerValueType,0) as tintOfferTriggerValueType,ISNULL(@fltOfferTriggerValue,0) as fltOfferTriggerValue

			PRINT @decmPrice
	END
	ELSE
	BEGIN
		SELECT 
			@bitRequireCouponCode=PO.bitRequireCouponCode,
			@tintOfferTriggerValueType=PO.tintOfferTriggerValueType,
			@fltOfferTriggerValue=PO.fltOfferTriggerValue

		FROM 
			PromotionOffer PO
		WHERE 
			numDomainId=@numDomainID 
			AND ISNULL(bitEnabled,0)=1
			AND ISNULL(bitAppliesToSite,0)=1 
			AND txtCouponCode=@vcSendCoupon

		SELECT @vPromotionDesc=( 
			CONCAT('Buy '
					,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
					,CASE tintOfferBasedOn
							WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
							WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
						END
					,' & get '
					, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
					, CASE 
						WHEN tintDiscoutBaseOn = 1 THEN 
							CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 2 THEN 
							CONCAT
							(
								CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(',',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
						ELSE '' 
					END
				))
				
		FROM
			PromotionOffer PO
		WHERE
			numDomainId=@numDomainID 
			AND txtCouponCode=@vcSendCoupon

		SELECT @bitRequireCouponCode AS bitRequireCouponCode,@tintOfferTriggerValueType AS tintOfferTriggerValueType,@vPromotionDesc AS vPromotionDesc,
		@fltOfferTriggerValue AS fltOfferTriggerValue
	END
	--END
END


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetShippingCharge')
DROP PROCEDURE USP_GetShippingCharge
GO
CREATE PROCEDURE [dbo].[USP_GetShippingCharge]
	@numDomainID NUMERIC(18,0),
	@numShippingCountry AS NUMERIC(18,0),
	@cookieId VARCHAR(MAX)=NULL,
	@numUserCntId numeric=0,
	@monShippingAmount money OUTPUT
AS
BEGIN
	DECLARE @numPromotionID AS NUMERIC(18,0)=0

	DECLARE @decmPrice DECIMAL(18,2)
	SET @decmPrice=(select ISNULL(SUM(monTotAmount),0) from CartItems where numDomainId=@numDomainID AND vcCookieId=@cookieId AND numUserCntId=@numUserCntId)

	SET @numPromotionID=(SELECT TOP 1 PromotionID from CartItems AS C 
	LEFT JOIN PromotionOffer AS P 
	ON C.PromotionID=P.numProId
	where C.numDomainId=@numDomainID AND C.vcCookieId=@cookieId AND C.numUserCntId=@numUserCntId
	AND (ISNULL(bitFixShipping1,0)=1 OR ISNULL(bitFixShipping2,0)=1 OR ISNULL(bitFreeShiping,0)=1) order by numCartId )

	IF(@numPromotionID>0)
	BEGIN
		DECLARE @bitFreeShiping BIT
		DECLARE @monFreeShippingOrderAmount money
		DECLARE @numFreeShippingCountry money
		DECLARE @bitFixShipping1 BIT
		DECLARE @monFixShipping1OrderAmount money
		DECLARE @monFixShipping1Charge money
		DECLARE @bitFixShipping2 BIT
		DECLARE @monFixShipping2OrderAmount money
		DECLARE @monFixShipping2Charge money
		SELECT 
			@bitFreeShiping=bitFreeShiping
			,@monFreeShippingOrderAmount=monFreeShippingOrderAmount
			,@numFreeShippingCountry=numFreeShippingCountry
			,@bitFixShipping1=bitFixShipping1
			,@monFixShipping1OrderAmount=monFixShipping1OrderAmount
			,@monFixShipping1Charge=monFixShipping1Charge
			,@bitFixShipping2=bitFixShipping2
			,@monFixShipping2OrderAmount=monFixShipping2OrderAmount
			,@monFixShipping2Charge=monFixShipping2Charge
			--CONCAT
			--	(
			--		(CASE WHEN ISNULL(bitFixShipping1,0)=1 AND @decmPrice < monFixShipping1OrderAmount THEN CONCAT('Spend $',(monFixShipping1OrderAmount-@decmPrice), CASE WHEN @decmPrice>0 THEN ' more ' ELSE '' END+' and get $',monFixShipping1Charge,' shipping. ') ELSE '' END)
			--		,(CASE WHEN ISNULL(bitFixShipping2,0)=1 AND @decmPrice < monFixShipping2OrderAmount THEN CONCAT('Spend $',monFixShipping2OrderAmount-@decmPrice, CASE WHEN @decmPrice>0 THEN ' more ' ELSE '' END+' and get $',monFixShipping2Charge,' shipping. ') ELSE '' END)
			--		,(CASE WHEN (bitFreeShiping=1 AND numFreeShippingCountry=@numShippingCountry) AND @decmPrice<monFreeShippingOrderAmount THEN CONCAT('Spend $',monFreeShippingOrderAmount-@decmPrice, CASE WHEN @decmPrice>0 THEN ' more ' ELSE '' END+' and shipping is FREE! ') ELSE '' END) 
					
			--	) AS vcShippingDescription,
			--	(CASE WHEN ISNULL(bitFixShipping1,0)=1 AND @decmPrice > monFixShipping1OrderAmount AND  @decmPrice<monFixShipping2OrderAmount  THEN 'You have qualified for $'+CAST(monFixShipping1Charge AS varchar)+' shipping'
			--			   WHEN ISNULL(bitFixShipping2,0)=1 AND @decmPrice > monFixShipping2OrderAmount  AND  @decmPrice<monFreeShippingOrderAmount THEN 'You have qualified for $'+CAST(monFixShipping2Charge AS varchar)+' shipping'
			--			   WHEN (ISNULL(bitFreeShiping,0)=1 AND numFreeShippingCountry=@numShippingCountry AND @decmPrice>monFreeShippingOrderAmount) THEN 'You have qualified for Free Shipping' END
			--		)AS vcQualifiedShipping
		FROM
			PromotionOffer PO
		WHERE
			numDomainId=@numDomainID 
			AND numProId = @numPromotionID

		IF(ISNULL(@bitFixShipping1,0)=1 AND @decmPrice > @monFixShipping1OrderAmount AND 1=(CASE WHEN ISNULL(@bitFixShipping2,0)=1 AND @decmPrice<@monFixShipping2OrderAmount THEN 1 WHEN ISNULL(@bitFixShipping2,0)=0 THEN 1 ELSE 0 END))
		BEGIN
			SET @monShippingAmount=@monFixShipping1Charge
		END

		ELSE IF(ISNULL(@bitFixShipping2,0)=1 AND @decmPrice > @monFixShipping2OrderAmount AND  1=(CASE WHEN ISNULL(@bitFreeShiping,0)=1 AND @decmPrice<@monFreeShippingOrderAmount THEN 1 WHEN ISNULL(@bitFreeShiping,0)=0 THEN 1 ELSE 0 END))
		BEGIN
			SET @monShippingAmount=@monFixShipping2Charge
		END

		ELSE IF((ISNULL(@bitFreeShiping,0)=1 AND @numFreeShippingCountry=@numShippingCountry AND @decmPrice>@monFreeShippingOrderAmount))
		BEGIN
			SET @monShippingAmount=0
		END
		ELSE
		BEGIN
			SET @monShippingAmount=-1
		END

	END
	--END
END


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_RemoveCouponCode')
DROP PROCEDURE USP_RemoveCouponCode
GO
CREATE PROCEDURE [dbo].[USP_RemoveCouponCode]
	@numDomainID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@cookieId VARCHAR(MAX)=NULL,
	@numUserCntId numeric=0,
	@vcSendCoupon VARCHAR(200)=0
AS
BEGIN

	UPDATE
		CartItems
	SET
		PromotionID=0,
		PromotionDesc='',
		monTotAmount=monTotAmtBefDiscount,
		fltDiscount=0,
		bitParentPromotion=0,
		vcCoupon=''
	WHERE
		numCartId IN(SELECT numCartId FROM CartItems WHERE bitParentPromotion=0 AND PromotionID IN(
		SELECT PromotionID FROM CartItems 
		WHERE numDomainId=@numDomainID AND
		vcCookieId=@cookieId AND
		vcCoupon=@vcSendCoupon AND
		numUserCntId=@numUserCntId AND bitParentPromotion=1))

	UPDATE
		CartItems
	SET
		PromotionID=0,
		PromotionDesc='',
		monTotAmount=monTotAmtBefDiscount,
		fltDiscount=0,
		vcCoupon=''
	WHERE
		numDomainId=@numDomainID AND
		vcCookieId=@cookieId AND
		vcCoupon=@vcSendCoupon AND
		numUserCntId=@numUserCntId AND 
		bitParentPromotion=1

	
		
END