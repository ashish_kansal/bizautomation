/******************************************************************
Project: Release 4.5 Date: 04.May.2015
Comments: STORED PROCEDURES
*******************************************************************/

-------------------- STORE PROCEDURES -------------------------

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AdvancedSearchItems')
DROP PROCEDURE USP_AdvancedSearchItems
GO
CREATE PROCEDURE USP_AdvancedSearchItems
@WhereCondition as varchar(4000)='',                              
@ViewID as tinyint,                              
@numDomainID as numeric(9)=0,                              
@numGroupID as numeric(9)=0,                              
@CurrentPage int,
@PageSize int,                                                                    
@TotRecs int output,                                                                                                             
@columnSortOrder as Varchar(10),                              
@ColumnSearch as varchar(100)='',                           
@ColumnName as varchar(50)='',                             
@GetAll as bit,                              
@SortCharacter as char(1),                        
@SortColumnName as varchar(50)='',        
@bitMassUpdate as bit,        
@LookTable as varchar(10)='' ,        
@strMassUpdate as varchar(2000)=''    
AS 
BEGIN
 
 
   
IF (@SortCharacter <> '0' AND @SortCharacter <> '')
  SET @ColumnSearch = @SortCharacter
CREATE TABLE #temptable
  (
  id           INT   IDENTITY   PRIMARY KEY,
  numItemID VARCHAR(15))
  
------------Declaration---------------------  
DECLARE  @strSql           VARCHAR(8000),
         @numMaxFieldId NUMERIC(9),
         @from         NVARCHAR(1000),
         @Where         NVARCHAR(4000),
         @OrderBy       NVARCHAR(1000),
         @GroupBy       NVARCHAR(1000),
         @SelectFields   NVARCHAR(4000),
         @InneJoinOn    NVARCHAR(1000)
         
 DECLARE @tintOrder AS TINYINT,
		 @vcFormFieldName AS VARCHAR(50),
		 @vcListItemType AS VARCHAR(3),
		 @vcListItemType1 AS VARCHAR(3),
		 @vcAssociatedControlType VARCHAR(10),
		 @numListID AS NUMERIC(9),
		 @vcDbColumnName VARCHAR(30),
		 @vcLookBackTableName VARCHAR(50),
		 @WCondition VARCHAR(1000)
SET @InneJoinOn = ''
SET @OrderBy = ''
SET @Where = ''
SET @from = ' FROM item I '
SET @GroupBy = ''
SET @SelectFields = ''  
SET @WCondition = ''


	
--	SELECT I.numItemCode,* FROM item I 
--	LEFT JOIN ItemGroups IG ON I.numDomainID = IG.numDomainID AND I.numItemGroup = IG.numItemGroupID
--	LEFT JOIN dbo.Vendor V ON I.numDomainID = V.numDomainID AND I.numItemCode = V.numItemCode --AND I.numVendorID = V.numVendorID
	
	
SET @Where = @Where + ' WHERE I.charItemType <>''A'' and I.numDomainID=' + convert(varchar(15),@numDomainID)
if (@ColumnSearch<>'') SET @Where = @Where + ' and I.vcItemName like ''' + @ColumnSearch + '%'''

CREATE TABLE #temp(ItemID INT PRIMARY KEY)
IF CHARINDEX('SplitString',@WhereCondition) > 0
	BEGIN
		DECLARE @strTemp AS NVARCHAR(MAX)
		SET @WhereCondition	= REPLACE(@WhereCondition,' AND I.numItemCode IN ','')
		SET @WhereCondition = RIGHT(@WhereCondition,LEN(@WhereCondition) - 1)
		SET @WhereCondition = LEFT(@WhereCondition,LEN(@WhereCondition) - 1)
		
		SET @strTemp = 'INSERT INTO #temp ' + @WhereCondition	
		PRINT @strTemp
		EXEC SP_EXECUTESQL @strTemp
		
		set @InneJoinOn= @InneJoinOn + ' JOIN #temp SP ON SP.ItemID = I.numItemCode '
	END
ELSE
	BEGIN
		SET @Where = @Where + @WhereCondition	
	END

	
	
	
set @tintOrder=0
set @WhereCondition =''
set @strSql='SELECT  I.numItemCode '

select top 1 @tintOrder=A.tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFormFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName
from AdvSerViewConf A
join View_DynamicDefaultColumns D
on D.numFieldId=A.numFormFieldId   and d.numformId = 29
where D.numDomainID = @numDomainID AND D.numFormID = 29 AND A.numformId = 29 AND tintViewID=@ViewID and numGroupID=@numGroupID and A.numDomainID=@numDomainID 
order by A.tintOrder asc
while @tintOrder>0
begin


 if @vcAssociatedControlType='SelectBox'                              
        begin                              
		  if @vcListItemType='IG' --Item group
		  begin                              
			   set @SelectFields=@SelectFields+',IG.vcItemGroup ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'                              
			   set @InneJoinOn= @InneJoinOn +' LEFT JOIN ItemGroups IG ON I.numDomainID = IG.numDomainID AND I.numItemGroup = IG.numItemGroupID '
			   SET @GroupBy = @GroupBy + 'IG.vcItemGroup,'
		  end
		  else if @vcListItemType='PP'--Item Type
		  begin
			   set @SelectFields=@SelectFields+', CASE I.charItemType WHEN ''p'' THEN ''Inventory Item'' WHEN ''n'' THEN  ''Non-Inventory Item'' WHEN ''s'' THEN ''Service'' ELSE '''' END   ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'                              
			   SET @GroupBy = @GroupBy + 'I.charItemType,'
		  END
		  else if @vcListItemType='UOM'--Unit of Measure
		  BEGIN
		  		SET @SelectFields = @SelectFields + ' ,dbo.fn_GetUOMName(I.'+ @vcDbColumnName +') ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'
				SET @GroupBy = @GroupBy + 'I.' + @vcDbColumnName + ','
		  END
		  else if @vcListItemType='LI'--Any List Item
		  BEGIN
			IF @vcDbColumnName='vcUnitofMeasure'
			BEGIN 		
				SET @SelectFields = @SelectFields + ' ,I.'+ @vcDbColumnName +' ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'
				SET @GroupBy = @GroupBy + 'I.' + @vcDbColumnName + ','
			END 
			ELSE 
				BEGIN
					set @SelectFields=@SelectFields+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'                              
					set @InneJoinOn= @InneJoinOn +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                              
					SET @GroupBy = @GroupBy + 'L' + convert(varchar(3),@tintOrder)+'.vcData,'
					IF @SortColumnName ='numItemClassification'
						SET @SortColumnName = 'L'+ convert(varchar(3),@tintOrder)+'.vcData'
					
				END
			END                              
			
		END
 ELSE if @vcAssociatedControlType='ListBox'
	 BEGIN
	 	 if @vcListItemType='V'--Vendor
		  BEGIN
				set @SelectFields=@SelectFields+', dbo.fn_GetComapnyName(I.numVendorID)  ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'
				SET @GroupBy = @GroupBy + 'I.' + @vcDbColumnName + ','
		  END
	 END
 ELSE
	  BEGIN
		 IF @vcLookBackTableName ='WareHouseItems'
		 BEGIN
			IF @vcDbColumnName = 'vcBarCode'
			BEGIN
				SET @SelectFields = @SelectFields + ', STUFF((SELECT N'', '' + vcBarCode FROM WareHouseItems WHERE numDomainID = ' + convert(varchar(15),@numDomainID) + ' AND numItemID = I.numItemCode FOR XML PATH ('''')), 1, 2, '''') ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'
			END
			ELSE IF @vcDbColumnName = 'vcWHSKU'
			BEGIN
				SET @SelectFields = @SelectFields + ', STUFF((SELECT N'', '' + vcWHSKU FROM WareHouseItems WHERE numDomainID = ' + convert(varchar(15),@numDomainID) + ' AND numItemID = I.numItemCode FOR XML PATH ('''')), 1, 2, '''') ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'
			END
			ELSE
			BEGIN
		 		SET @SelectFields = @SelectFields + ' ,SUM(WI.'+ @vcDbColumnName +') ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'
		 		IF CHARINDEX('dbo.WareHouseItems WI',@InneJoinOn)=0
		 			SET @InneJoinOn= @InneJoinOn +' left JOIN dbo.WareHouseItems WI ON WI.numItemID = I.numItemCode '
		 	END
		 END
		 else IF @vcDbColumnName='vcSerialNo'
		  BEGIN
--			SET @InneJoinOn= @InneJoinOn +'LEFT JOIN dbo.WareHouseItems WI ON I.numItemCode = WI.numItemID AND I.numDomainID = WI.numDomainID
--			LEFT JOIN dbo.WareHouseItmsDTL WD ON WI.numWareHouseItemID = WD.numWareHouseItemID'	
			SET @SelectFields = @SelectFields + ' ,0 ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'
			IF CHARINDEX('dbo.WareHouseItems WI',@InneJoinOn)=0
		 		set @InneJoinOn= @InneJoinOn +' left JOIN dbo.WareHouseItems WI ON WI.numItemID = I.numItemCode '
		  END
		  ELSE IF @vcDbColumnName='vcPartNo'
		  BEGIN
			SET @SelectFields = @SelectFields + ' ,(SELECT top 1 ISNULL(vcPartNo,'''') FROM dbo.Vendor V1 WHERE V1.numVendorID =I.numVendorID) as  ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'
		  END
		  ELSE 
		  BEGIN
		  	SET @SelectFields = @SelectFields + ' ,I.'+ @vcDbColumnName +' ['+ @vcFormFieldName + '~' + @vcDbColumnName+']'
		  	SET @GroupBy = @GroupBy + 'I.' + @vcDbColumnName + ','
		  END
		   
	  END
	  PRINT @vcDbColumnName
	  
	  IF CHARINDEX('WareHouseItmsDTL',@Where)>0
	  BEGIN
	  	 IF CHARINDEX('dbo.WareHouseItems WI',@InneJoinOn)=0
		 	set @InneJoinOn= @InneJoinOn +' left JOIN dbo.WareHouseItems WI ON WI.numItemID = I.numItemCode '
	  END
--IF ((@vcDbColumnName='vcPartNo'	OR @vcDbColumnName='numVendorID') AND CHARINDEX('dbo.Vendor V',@InneJoinOn)=0)
--BEGIN
--	set @InneJoinOn= @InneJoinOn +' LEFT JOIN dbo.Vendor V ON I.numDomainID = V.numDomainID AND I.numItemCode = V.numItemCode  AND v.numVendorID = I.numVendorID
--			LEFT JOIN dbo.DivisionMaster DM ON DM.numDivisionID=V.numVendorID 
--			left JOIN dbo.CompanyInfo C ON C.numCompanyId = DM.numCompanyID '	
--END
  	
        
          
               
                                      
 select top 1 @tintOrder=A.tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFormFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName
 from AdvSerViewConf A                              
 join View_DynamicDefaultColumns D                               
 on D.numFieldId=A.numFormFieldId    and d.numformId = 29
 where D.numDomainID = @numDomainID AND D.numFormID = 29 AND A.numformId = 29 AND tintViewID=@ViewID and numGroupID=@numGroupID and A.numDomainID=@numDomainID and A.tintOrder > @tintOrder-1
 order by A.tintOrder asc  
                            
 if @@rowcount=0 set @tintOrder=0                              
end                              

/*--------------Sorting logic-----------*/
	
	IF @SortColumnName=''
		SET @SortColumnName = 'vcItemName'
	ELSE IF @SortColumnName ='numItemGroup'
		SET @SortColumnName = 'IG.vcItemGroup'
	ELSE IF @SortColumnName ='charItemType'
		SET @SortColumnName = 'I.charItemType'
	ELSE IF @SortColumnName = 'vcPartNo'
		SET @SortColumnName = 'vcItemName'
		
	 IF exists(SELECT * FROM dbo.AdvSerViewConf WHERE numDomainID=@numDomainID AND numFormId=29 AND numGroupID=@numGroupID AND tintViewID=@ViewID)
		SET @OrderBy = ' ORDER BY ' + @SortColumnName + ' '+ @columnSortOrder	
	
/*--------------Sorting logic ends-----------*/
	
	

	
	SET @strSql =  @strSql /*+ @SelectFields */+ @from + @InneJoinOn + @Where + @OrderBy
	PRINT @strSql
	INSERT INTO #temptable (
		numItemID
	) EXEC(@strSql)

/*-----------------------------Mass Update-----------------------------*/
if @bitMassUpdate=1
BEGIN
	PRINT @strMassUpdate
	exec (@strMassUpdate)
END

	
/*-----------------------------Paging Logic-----------------------------*/
	declare @firstRec as integer
	declare @lastRec as integer
	set @firstRec = ( @CurrentPage - 1 ) * @PageSize
	set @lastRec = ( @CurrentPage * @PageSize + 1 )
	set @TotRecs=(select count(*) from #tempTable)
	
	SET @InneJoinOn =@InneJoinOn+ ' join #temptable T on T.numItemID = I.numItemCode '
	SET @strSql = 'SELECT distinct I.numItemCode  [numItemCode],T.ID [ID~ID],isnull(I.bitSerialized,0) as [bitSerialized],isnull(I.bitLotNo,0) as [bitLotNo] ' +  @SelectFields + @from + @InneJoinOn + @Where 
	IF @GetAll=0
	BEGIN
		SET @strSql = @strSql +' and T.ID > '+convert(varchar(10),@firstRec)+ ' and T.ID <'+ convert(varchar(10),@lastRec) 
	END
	
	-- enable or disble group by based on search result view
	IF CHARINDEX('dbo.WareHouseItems WI',@InneJoinOn)=0
		SET @strSql = @strSql + ' order by ID; '
	ELSE 
		SET @strSql = @strSql + ' GROUP BY I.numItemCode,T.ID,I.bitSerialized,I.bitLotNo,I.numVendorID,' + SUBSTRING(@GroupBy, 1, LEN(@GroupBy)-1);
	
	
	EXEC (@strSql)
	PRINT @strSql
	
	DROP TABLE #temptable;
-------------------------------------------------	

select D.vcDbColumnName,D.vcFieldName AS vcFormFieldName,A.tintOrder AS tintOrder,isnull(D.bitAllowSorting,0) AS bitAllowSorting
from AdvSerViewConf A
join View_DynamicDefaultColumns D
on D.numFieldId=A.numFormFieldId   and d.numformId = 29
where D.numDomainID = @numDomainID AND D.numFormID = 29 AND A.numformId = 29 AND tintViewID=@ViewID and numGroupID=@numGroupID and A.numDomainID=@numDomainID 
UNION 
SELECT 'ID','numItemCode',0 tintOrder,0 
order by A.tintOrder ASC


DROP TABLE #temp
END

/****** Object:  StoredProcedure [dbo].[USP_CheckAndBilledAmount]    Script Date: 03/20/2015 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CheckAndBilledAmount')
DROP PROCEDURE USP_CheckAndBilledAmount
GO
CREATE PROCEDURE [dbo].[USP_CheckAndBilledAmount]
(
	@numDivisionID			NUMERIC(18,0),
	@numOppBizDocId			NUMERIC(18,0),
	@numBillID				NUMERIC(18,0),
	@numAmount				MONEY,
	@numDomainID			NUMERIC(18,0)
)
AS 
BEGIN
	
	IF @numBillID > 0
	BEGIN

		SELECT  CASE WHEN ISNULL(BH.[monAmountDue],0) = @numAmount THEN 1
					 WHEN ISNULL(BH.[monAmountDue],0) >= ISNULL(SUM(monAmount),0) + @numAmount THEN 2
					 WHEN ISNULL(BH.[monAmountDue],0) < ISNULL(SUM(monAmount),0) + @numAmount THEN 3
				END [IsPaidInFull]
		FROM [dbo].[BillPaymentDetails] AS BPD 
		JOIN [dbo].[BillPaymentHeader] AS BPH ON [BPH].[numBillPaymentID] = BPD.[numBillPaymentID]
		JOIN [dbo].[BillHeader] AS BH ON BH.[numBillID] = BPD.[numBillID]
		WHERE [BH].[numBillID] = @numBillID
		AND [BH].[numDomainID] = @numDomainID
		AND [BH].[numDivisionID] = @numDivisionID
		GROUP BY [monAmountDue]

	END

	ELSE IF @numOppBizDocId > 0
	BEGIN

		SELECT  CASE WHEN ISNULL([OBD].[monDealAmount],0) = @numAmount THEN 1
					 WHEN ISNULL([OBD].[monDealAmount],0) = ISNULL(SUM(BPD.monAmount),0) + @numAmount THEN 2
					 WHEN ISNULL([OBD].[monDealAmount],0) > ISNULL(SUM(BPD.monAmount),0) + @numAmount THEN 3
					 WHEN ISNULL([OBD].[monDealAmount],0) < ISNULL(SUM(BPD.monAmount),0) + @numAmount THEN 4
				END [IsPaidInFull]
		FROM [dbo].[BillPaymentDetails] AS BPD 
		JOIN [dbo].[BillPaymentHeader] AS BPH ON [BPH].[numBillPaymentID] = BPD.[numBillPaymentID]
		JOIN [dbo].[OpportunityBizDocs] AS OBD ON OBD.[numOppBizDocsId] = [BPD].[numOppBizDocsID]
		JOIN [dbo].[OpportunityMaster] AS OM ON OM.[numOppId] = [OBD].[numOppId] AND [BPH].[numDomainID] = OM.[numDomainId]
		WHERE [OBD].[numOppBizDocsId] = @numOppBizDocId
		AND [OM].[numDomainID] = @numDomainID
		AND [OM].[numDivisionID] = @numDivisionID
		GROUP BY [OBD].[monDealAmount]

	END

		
	--SELECT CASE WHEN ISNULL([OBD].[monDealAmount],0) = @numAmount THEN 1 -- If paid amount + new paid amount is equal to total deal amount, Fully Paid
	--			WHEN ISNULL([OBD].[monDealAmount],0) > SUM(ISNULL(DD.[monAmountPaid],0)) + @numAmount THEN 2 -- If paid amount + new paid amount is greater than total deal amount, Due Amounts
	--			WHEN ISNULL([OBD].[monDealAmount],0) < SUM(ISNULL(DD.[monAmountPaid],0)) + @numAmount THEN 3 -- If paid amount + new paid amount is less than total deal   amount, Unapplied Amounts
	--			ELSE 0
	--	   END AS [IsPaidInFull],
	--	   [DM].[numDivisionID],
	--	   [DM].[numDomainId],
	--	   [DD].[numOppBizDocsID],
	--	   ISNULL([OBD].[monDealAmount],0)  [monDealAmount],
	--	   SUM(ISNULL(DD.[monAmountPaid],0)) [monAmountPaid],
	--	   SUM(ISNULL(DD.[monAmountPaid],0)) + @numAmount [NewAmountPaid],
	--	   (SUM(ISNULL(DD.[monAmountPaid],0)) + @numAmount) - ISNULL([OBD].[monDealAmount],0) [OverPaidAmount]
	--FROM [dbo].[DepositMaster] AS DM 
	--JOIN [dbo].[DepositeDetails] AS DD ON [DM].[numDepositId] = [DD].[numDepositID]
	--JOIN [dbo].[OpportunityMaster] AS OM ON OM.[numOppId] = DD.[numOppID] AND OM.[numDomainId] = DM.[numDomainId]
	--JOIN [dbo].[OpportunityBizDocs] AS OBD ON [OBD].[numOppBizDocsId] = DD.[numOppBizDocsID] AND [OM].[numOppId] = [OBD].[numOppId]
	--WHERE [DM].[numDomainId] = @numDomainID
	--AND [DM].[numDivisionID] = @numDivisionID
	--AND DD.[numOppBizDocsID] = @numOppBizDocId
	--GROUP BY [OBD].[monDealAmount],
	--		 [DM].[numDivisionID],
	--		 [DM].[numDomainId],
	--		 [DD].[numOppBizDocsID]

END

GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_FeaturedItem' ) 
    DROP PROCEDURE USP_FeaturedItem
GO
CREATE PROCEDURE USP_FeaturedItem
    @numNewDomainID NUMERIC(9) ,
    @numReportId NUMERIC(9) ,
    @numSiteId NUMERIC(9)
AS 
    BEGIN

        DECLARE @strsql VARCHAR(5000) 
        DECLARE @DefaultWarehouseID NUMERIC(9)
       
        SELECT  @DefaultWarehouseID = numDefaultWareHouseID
        FROM    dbo.eCommerceDTL
        WHERE   numDomainId = @numNewDomainID
        SET @strsql = 'DECLARE @numDomainId NUMERIC;
       SET @numDomainid =' + CONVERT(VARCHAR, @numNewDomainID)
            + ';
     select X.##Items## from ( '
        DECLARE @query AS VARCHAR(2000)
        SELECT  @query = textQuery
        FROM    dbo.CustomReport
        WHERE   dbo.CustomReport.numCustomReportID = @numReportId 
        SET @strsql = @strsql + ' ' + @query + ') X'
        PRINT @strSql

 
        CREATE TABLE #temptable ( ItemCode NUMERIC(9) ) 
        INSERT  INTO #temptable
                EXEC ( @strsql)
SELECT X.*, ( SELECT TOP 1
                            vcCategoryName
                  FROM      dbo.Category
                  WHERE     numCategoryID = X.numCategoryID
                ) AS vcCategoryName
                
  FROM (        
  SELECT  I.numItemCode ,
                I.vcItemName ,
                I.txtItemDesc ,
                ( SELECT TOP 1
                            numCategoryID
                  FROM      ItemCategory
                  WHERE     numItemCode = I.numItemCode
                ) AS numCategoryID ,
                CASE WHEN EXISTS ( SELECT TOP 1
                                            vcPathForTImage
                                   FROM     dbo.ItemImages II
                                   WHERE    II.bitDefault = 1
                                            AND II.numItemCode = I.numItemCode )
                     THEN ( SELECT TOP 1  vcPathForTImage
                            FROM    dbo.ItemImages II
                            WHERE   II.bitDefault = 1
                                    AND II.numItemCode = I.numItemCode
                          )
                     WHEN EXISTS ( SELECT TOP 1
                                            vcPathForTImage
                                   FROM     dbo.ItemImages II
                                   WHERE    II.numItemCode = I.numItemCode )
                     THEN ( SELECT TOP 1  vcPathForTImage
                            FROM    dbo.ItemImages II
                            WHERE   II.numItemCode = I.numItemCode
                          )
                     ELSE ''
                END AS vcPathForTImage ,
                ISNULL(CASE WHEN I.[charItemType] = 'P'
                            THEN CASE WHEN I.bitSerialized = 1
                                      THEN 1.00000 * monListPrice
                                      ELSE 1.00000
                                           * ISNULL(( SELECT TOP 1
                                                              W.[monWListPrice]
                                                      FROM    WarehouseItems W
                                                      WHERE   W.numDomainID = @numNewDomainID
															  AND W.numItemID = I.numItemCode
                                                              AND W.numWarehouseID = @DefaultWarehouseID
                                                    ), NULL)
                                 END
                            ELSE 1.00000 * monListPrice
                       END, 0) AS monListPrice ,
                dbo.fn_UOMConversion(ISNULL(I.numSaleUnit, 0), I.numItemCode,
                                     I.numDomainID, ISNULL(I.numBaseUnit, 0)) AS UOMConversionFactor ,
                I.vcSKU ,
                I.vcManufacturer 
               
        FROM    Item AS I
        WHERE   I.numItemCode IN ( SELECT   ItemCode
                                   FROM     #tempTable )
                AND I.numDomainId = @numNewDomainID
                
) AS X
    DROP TABLE #tempTable
    END
--EXEC USP_FeaturedItem @numNewDomainID= 1 , @numReportId = 0 ,@numSiteId = 18
--exec USP_FeaturedItem @numNewDomainID=1,@numReportId=6774,@numSiteId=106
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAlertPanelDetail')
DROP PROCEDURE dbo.USP_GetAlertPanelDetail
GO
CREATE PROCEDURE [dbo].[USP_GetAlertPanelDetail]
    (
	  @numAlertType INTEGER,
      @numDivisionID NUMERIC(18,0),
      @numContactID NUMERIC(18,0),
	  @numDomainID NUMERIC(18,0)
    )
AS 
BEGIN
	-- A/R BALANCE
	IF @numAlertType = 1
	BEGIN
		DECLARE  @AuthoritativePurchaseBizDocId  AS INTEGER
		SELECT  @AuthoritativePurchaseBizDocId= isnull(numAuthoritativePurchase,0) FROM AuthoritativeBizDocs WHERE  numDomainId =  @numDomainId

		DECLARE  @AuthoritativeSalesBizDocId  AS INTEGER
		SELECT @AuthoritativeSalesBizDocId = isnull(numAuthoritativeSales,0) FROM AuthoritativeBizDocs WHERE  numDomainId = @numDomainId

		SELECT
			TOP 10 
			vcPOppName AS [Order],
			vcBizDocID AS [Authoritive BizDoc],
			TotalAmount AS [Total Amount],
			AmountPaid AS [Amount Paid],
			BalanceDue AS [Balance Due],
			[dbo].[FormatedDateFromDate](DueDate,1) AS [Due Date]
		FROM
		(
		SELECT DISTINCT 
			OM.[numOppId], 
			OM.[vcPOppName],
			OB.[numOppBizDocsId],
			OB.[vcBizDocID],
			isnull(OB.monDealAmount,0) TotalAmount,--OM.[monPAmount]
			(OB.[monAmountPaid]) AmountPaid,
			isnull(OB.monDealAmount - OB.[monAmountPaid],0) BalanceDue,
			CASE ISNULL(bitBillingTerms,0)        
				WHEN 1 THEN DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate)
				WHEN 0 THEN ob.[dtFromDate]
			END AS DueDate       
		FROM   
			[OpportunityMaster] OM 
		INNER JOIN [DivisionMaster] DM ON OM.[numDivisionId] = DM.[numDivisionID]
		LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppId = OM.numOppID
		LEFT OUTER JOIN [Currency] C ON OM.[numCurrencyID] = C.[numCurrencyID]
		WHERE  
			OM.[tintOppType] = 1
			AND OM.[tintOppStatus] = 1
			AND om.[numDomainId] = @numDomainID
			AND OB.[numBizDocId] = ISNULL(@AuthoritativeSalesBizDocId,0)
			AND OB.bitAuthoritativeBizDocs=1
			AND ISNULL(OB.tintDeferred,0) <> 1
			AND DM.[numDivisionID] = @numDivisionID
			AND isnull(OB.monDealAmount,0) > 0  AND isnull(OB.monDealAmount * OM.fltExchangeRate - OB.[monAmountPaid] * OM.fltExchangeRate,0) != 0    	
		UNION 
		SELECT 
				-1 AS [numOppId],
				'Journal' [vcPOppName],
				0 [numOppBizDocsId],
				'' [vcBizDocID],
				CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END TotalAmount,
				0 AmountPaid,
				0 BalanceDue,
				GJH.datEntry_Date AS DueDate
		FROM   
			dbo.General_Journal_Header GJH
		INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId														  AND GJH.numDomainId = GJD.numDomainId
		INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
		LEFT OUTER JOIN [Currency] C ON GJD.[numCurrencyID] = C.[numCurrencyID]
		WHERE  
			GJH.numDomainId = @numDomainID
			AND GJD.numCustomerID =@numDivisionID
			AND COA.vcAccountCode LIKE '01010105%' 
			AND ISNULL(numOppId,0)=0 AND ISNULL(numOppBizDocsId,0)=0 
			AND ISNULL(GJH.numDepositID,0)=0 AND ISNULL(GJH.numReturnID,0)=0 
		UNION 
			SELECT 
				-1 AS [numOppId],
				CASE WHEN tintDepositePage=2 THEN 'Unapplied Payment' ELSE 'Credit Memo' END AS [vcPOppName],
				0 [numOppBizDocsId],
				'' [vcBizDocID],
				(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) * -1 AS TotalAmount,
				0 AmountPaid,
				(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) * -1 AS BalanceDue,
				DM.dtDepositDate AS DueDate
		FROM  
			DepositMaster DM 
		WHERE 
			DM.numDomainId=@numDomainID
			AND tintDepositePage IN(2,3)  
			AND DM.numDivisionId = @numDivisionID
			AND ((ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) > 0)
		UNION
		SELECT  
			Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else 0 end AS numOppId,
			'Bill' vcPOppName,	   
			0 numOppBizDocsId,
			CASE WHEN len(BH.vcReference)=0 THEN '' ELSE BH.vcReference END AS [vcBizDocID],
			BH.monAmountDue as TotalAmount,
			ISNULL(BH.monAmtPaid, 0) as AmountPaid,
			ISNULL(BH.monAmountDue, 0) - ISNULL(BH.monAmtPaid, 0) as BalanceDue,
			BH.dtDueDate AS DueDate
		FROM    
			BillHeader BH 
		WHERE   
			BH.numDomainID = @numDomainID
			AND BH.numDivisionId = @numDivisionID
			AND ISNULL(BH.monAmountDue, 0) - ISNULL(BH.monAmtPaid, 0) > 0 
		UNION
		SELECT  
			0 AS numOppId,
			'Check' vcPOppName,
			0 numOppBizDocsId,
			'' AS [vcBizDocID],
			ISNULL(CD.monAmount, 0) as TotalAmount,
			ISNULL(CD.monAmount, 0) as AmountPaid,
			0 as BalanceDue,
			CH.dtCheckDate AS DueDate
		FROM   
			CheckHeader CH
			JOIN  dbo.CheckDetails CD ON CH.numCheckHeaderID=CD.numCheckHeaderID
			INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = CD.numChartAcntId
		WHERE 
			CH.numDomainID = @numDomainID 
			AND CH.tintReferenceType=1
			AND COA.vcAccountCode LIKE '01020102%'
			AND CD.numCustomerId = @numDivisionID
		UNION 
		SELECT 
			OM.[numOppId],
			OM.[vcPOppName],
			OB.[numOppBizDocsId],
			OB.[vcBizDocID],
			isnull(OB.monDealAmount * OM.fltExchangeRate,0) TotalAmount,--OM.[monPAmount]
			(OB.[monAmountPaid] * OM.fltExchangeRate) AmountPaid,
			isnull(OB.monDealAmount * OM.fltExchangeRate - OB.[monAmountPaid] * OM.fltExchangeRate,0)  BalanceDue,
			CASE ISNULL(OM.bitBillingTerms,0)  
				WHEN 1 THEN DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(OM.intBillingDays,0)), 0)),OB.dtFromDate)
				WHEN 0 THEN ob.[dtFromDate]
			END AS DueDate
		FROM   
			[OpportunityMaster] OM
		INNER JOIN [DivisionMaster] DM ON OM.[numDivisionId] = DM.[numDivisionID]
		LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppId = OM.numOppID
		LEFT OUTER JOIN [Currency] C ON OM.[numCurrencyID] = C.[numCurrencyID]
		WHERE  
			OM.[tintOppType] = 2
			AND OM.[tintOppStatus] = 1
			AND om.[numDomainId] = @numDomainID
			AND OB.[numBizDocId] = ISNULL(@AuthoritativePurchaseBizDocId,0) 
			AND DM.[numDivisionID] = @numDivisionID
			AND OB.bitAuthoritativeBizDocs=1
			AND ISNULL(OB.tintDeferred,0) <> 1
			AND  isnull(OB.monDealAmount * OM.fltExchangeRate,0) > 0 
			AND isnull(OB.monDealAmount * OM.fltExchangeRate - OB.[monAmountPaid] * OM.fltExchangeRate,0) > 0 
		UNION 
		SELECT  
			OM.[numOppId],
			OM.[vcPOppName],
			OB.[numOppBizDocsId],
			OB.[vcBizDocID],
			BDC.numComissionAmount,
			0 AmountPaid,
			BDC.numComissionAmount BalanceDue,
			CASE ISNULL(OM.bitBillingTerms,0) 
				WHEN 1 THEN DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(OM.intBillingDays,0)), 0)),OB.dtFromDate)
				WHEN 0 THEN ob.[dtFromDate]
			END AS DueDate
		FROM    
			BizDocComission BDC 
		JOIN OpportunityBizDocs OB ON BDC.numOppBizDocId = OB.numOppBizDocsId
		join OpportunityMaster OM on OM.numOppId=OB.numOppId
		JOIN Domain D on D.numDomainID=BDC.numDomainID
		LEFT JOIN OpportunityBizDocsDetails OBDD ON BDC.numBizDocsPaymentDetId=isnull(OBDD.numBizDocsPaymentDetId,0)        
		WHERE   
			OM.numDomainID = @numDomainID
			AND BDC.numDomainID = @numDomainID
			AND OB.bitAuthoritativeBizDocs = 1
			AND isnull(OBDD.bitIntegratedToAcnt,0) = 0
			AND D.numDivisionId = @numDivisionID    	
		UNION 
		SELECT 
			GJH.numJournal_Id [numOppId],
			'Journal' [vcPOppName],
			0 [numOppBizDocsId],
			'' [vcBizDocID],
			CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END TotalAmount,
			0 AmountPaid,
			1 BalanceDue,
			GJH.datEntry_Date AS DueDate
		FROM   
			dbo.General_Journal_Header GJH
			INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId AND GJH.numDomainId = GJD.numDomainId
			INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
		WHERE  
			GJH.numDomainId = @numDomainID
			AND GJD.numCustomerID =@numDivisionID
			AND COA.vcAccountCode LIKE '01020102%'
			AND ISNULL(numOppId,0)=0 AND ISNULL(numOppBizDocsId,0)=0 
			AND ISNULL(GJH.numBillID,0)=0 AND ISNULL(GJH.numBillPaymentID,0)=0 AND ISNULL(GJH.numCheckHeaderID,0)=0 AND ISNULL(GJH.numReturnID,0)=0
			AND GJH.numJournal_Id NOT IN (
			--Exclude Bill and Bill payment entries
			SELECT GH.numJournal_Id FROM dbo.General_Journal_Header GH INNER JOIN dbo.OpportunityBizDocsDetails OBD ON GH.numBizDocsPaymentDetId = OBD.numBizDocsPaymentDetId 
			WHERE GH.numDomainId=@numDivisionID and OBD.tintPaymentType IN (2,4,6) and GH.numBizDocsPaymentDetId>0
			) AND ISNULL(GJH.numBillID,0)=0 AND ISNULL(GJH.numBillPaymentID,0)=0 AND ISNULL(GJH.numCheckHeaderID,0)=0) TEMP
		ORDER BY
			TEMP.DueDate DESC
	END
	-- OPEN ORDERS
	ELSE IF @numAlertType = 2
	BEGIN
		SELECT
			TOP 20 
			OM.vcPOppName [Order],
			ISNULL((SELECT vcData FROM ListDetails WHERE numListItemID = ISNULL(OM.numStatus,0)),'') AS [Order Status],
			ISNULL(OM.intPEstimatedCloseDate,'') AS [Due Date],
			CAST(ISNULL(numPercentageComplete,0) AS VARCHAR(3)) + ' %' As [Total Progress]
		FROM
			OpportunityMaster OM
		WHERE
			OM.numDomainID = @numDomainID
			AND OM.numDivisionID = @numDivisionID
			AND (OM.tintOppType = 1 OR OM.tintOppType = 2)
			AND OM.tintOppStatus = 1
			AND ISNULL(tintshipped,0)=0
		ORDER BY
			OM.intPEstimatedCloseDate DESC
	END	
	-- OPEN CASES
	ELSE IF @numAlertType = 3
	BEGIN
		SELECT    
			TOP 20 
			vcCaseNumber AS [Case #],
			textSubject AS [Subject],
			textDesc AS [Description],
			ISNULL((SELECT vcData FROM ListDetails WHERE numListItemID = ISNULL(Cases.numStatus,0)),'') AS [Status] 
		FROM            
			dbo.Cases
		WHERE        
			Cases.numstatus <> 136
			AND numDomainID = @numDomainID
			AND numDivisionID = @numDivisionID
		ORDER BY
			Cases.bintCreatedDate DESC
	END
	-- OPEN PROJECT
	ELSE IF @numAlertType = 4
	BEGIN
		SELECT        
			TOP 20
			vcProjectID AS [Project],
			ISNULL((SELECT vcData FROM ListDetails WHERE numListItemID = ISNULL(ProjectsMaster.numProjectStatus,0)),'') AS [Status],
			CAST(ISNULL((SELECT TOP 1 intTotalProgress FROM ProjectProgress WHERE numProId=numProId),0) AS VARCHAR(3)) + ' %' As [Total Progress],
			ISNULL(intDueDate,'') AS [Due Date],
			ISNULL(txtComments,'') AS [Comments]
		FROM            
			dbo.ProjectsMaster
		WHERE        
			numDomainId=@numDomainID
			AND numDivisionId = @numDivisionID
		ORDER BY
			bintCreatedDate DESC
			END
	-- OPEN ACTION ITEM
	ELSE IF @numAlertType = 5
	BEGIN
		SELECT     
			TOP 20 
			ISNULL((SELECT vcData FROM ListDetails WHERE numListItemID = ISNULL(bitTask,0)),'') AS [Type],
			CONCAT(CAST(dtStartTime AS DATE), ' ' ,FORMAT(dtStartTime,'hh:mm tt'),'-',FORMAT(dtEndTime,'hh:mm tt')) AS [Date, From, To],
			ISNULL(textDetails,'') AS [Comments]
		FROM            
			dbo.Communication
		WHERE        
			bitClosedFlag = 0
			AND numDomainID = @numDomainID
			AND numDivisionId = @numDivisionID
			AND numContactId = @numContactID
		ORDER BY
			Communication.dtCreatedDate DESC
	END
END
/****** Object:  StoredProcedure [dbo].[USP_GetColumnConfiguration1]    Script Date: 07/26/2008 16:16:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getcolumnconfiguration1' )
    DROP PROCEDURE usp_getcolumnconfiguration1
GO
CREATE PROCEDURE [dbo].[USP_GetColumnConfiguration1]
    @numDomainID NUMERIC(9) ,
    @numUserCntId NUMERIC(9) ,
    @FormId TINYINT ,
    @numtype NUMERIC(9) ,
    @numViewID NUMERIC(9) = 0
AS
    DECLARE @PageId AS TINYINT         
    IF @FormId = 10
        OR @FormId = 44
        SET @PageId = 4          
    ELSE
        IF @FormId = 11
            OR @FormId = 34
            OR @FormId = 35
            OR @FormId = 36
            OR @FormId = 96
            OR @FormId = 97
            SET @PageId = 1          
        ELSE
            IF @FormId = 12
                SET @PageId = 3          
            ELSE
                IF @FormId = 13
                    SET @PageId = 11       
                ELSE
                    IF ( @FormId = 14
                         AND ( @numtype = 1
                               OR @numtype = 3
                             )
                       )
                        OR @FormId = 33
                        OR @FormId = 38
                        OR @FormId = 39
                        SET @PageId = 2          
                    ELSE
                        IF @FormId = 14
                            AND ( @numtype = 2
                                  OR @numtype = 4
                                )
                            OR @FormId = 40
                            OR @FormId = 41
                            SET @PageId = 6  
                        ELSE
                            IF @FormId = 21
                                OR @FormId = 26
                                OR @FormId = 32
								OR @FormId = 126
                                SET @PageId = 5   
                            ELSE
                                IF @FormId = 22
                                    OR @FormId = 30
                                    SET @PageId = 5    
                                ELSE
                                    IF @FormId = 76
                                        SET @PageId = 10 
                                    ELSE
                                        IF @FormId = 84
                                            OR @FormId = 85
                                            SET @PageId = 5
                     
    IF @PageId = 1
        OR @PageId = 4
        BEGIN          
            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                    0 AS Custom
            FROM    View_DynamicDefaultColumns
            WHERE   numFormID = @FormId
                    AND ISNULL(bitSettingField, 0) = 1
                    AND ISNULL(bitDeleted, 0) = 0
                    AND numDomainID = @numDomainID
            UNION
            SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                    fld_label AS vcFieldName ,
                    1 AS Custom
            FROM    CFW_Fld_Master
                    LEFT JOIN CFW_Fld_Dtl ON Fld_id = numFieldId
                                             AND numRelation = CASE
                                                              WHEN @numtype IN (
                                                              1, 2, 3 )
                                                              THEN numRelation
                                                              ELSE @numtype
                                                              END
                    LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
            WHERE   CFW_Fld_Master.grp_id = @PageId
                    AND CFW_Fld_Master.numDomainID = @numDomainID
                    AND Fld_type <> 'Link'
            ORDER BY Custom ,
                    vcFieldName                                       
        END  
    ELSE IF @PageId = 10
    BEGIN          
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        0 AS Custom
                FROM    View_DynamicDefaultColumns
                WHERE   numFormID = @FormId
                        AND ISNULL(bitSettingField, 0) = 1
                        AND ISNULL(bitDeleted, 0) = 0
                        AND numDomainID = @numDomainID
                UNION
                SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                        fld_label AS vcFieldName ,
                        1 AS Custom
                FROM    CFW_Fld_Master
                        LEFT JOIN CFW_Fld_Dtl ON Fld_id = numFieldId
                                                 AND numRelation = CASE
                                                              WHEN @numtype IN (
                                                              1, 2, 3 )
                                                              THEN numRelation
                                                              ELSE @numtype
                                                              END
                        LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
                WHERE   CFW_Fld_Master.grp_id = 5
                        AND CFW_Fld_Master.numDomainID = @numDomainID
                        AND Fld_Type = 'SelectBox'
                ORDER BY Custom ,
                        vcFieldName       
	                                     
            END 
	ELSE IF @FormId = 125 AND @PageId=0
	BEGIN
			SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                    0 AS Custom
            FROM    View_DynamicDefaultColumns
            WHERE   numFormID = @FormId
                    AND ISNULL(bitSettingField, 0) = 1
                    AND ISNULL(bitDeleted, 0) = 0
                    AND numDomainID = @numDomainID
	END

        ELSE
            IF @PageId = 5
                AND ( @FormId = 84
                      OR @FormId = 85
                    )
                BEGIN          
                    SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                            ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                            0 AS Custom ,
                            vcDbColumnName
                    FROM    View_DynamicDefaultColumns
                    WHERE   numFormID = @FormId
                            AND ISNULL(bitSettingField, 0) = 1
                            AND ISNULL(bitDeleted, 0) = 0
                            AND numDomainID = @numDomainID
                    UNION
                    SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                            fld_label AS vcFieldName ,
                            1 AS Custom ,
                            CONVERT(VARCHAR(10), fld_id) AS vcDbColumnName
                    FROM    CFW_Fld_Master
                            LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
                    WHERE   CFW_Fld_Master.grp_id = @PageId
                            AND CFW_Fld_Master.numDomainID = @numDomainID
                            AND Fld_type = 'TextBox'
                    ORDER BY Custom ,
                            vcFieldName             
                END  
             
            ELSE
                BEGIN          
      
                    SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                            ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                            0 AS Custom ,
                            vcDbColumnName
                    FROM    View_DynamicDefaultColumns
                    WHERE   numFormID = @FormId
                            AND ISNULL(bitSettingField, 0) = 1
                            AND ISNULL(bitDeleted, 0) = 0
                            AND numDomainID = @numDomainID
                    UNION
                    SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                            fld_label AS vcFieldName ,
                            1 AS Custom ,
                            CONVERT(VARCHAR(10), fld_id) AS vcDbColumnName
                    FROM    CFW_Fld_Master
                            LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
                    WHERE   CFW_Fld_Master.grp_id = @PageId
                            AND CFW_Fld_Master.numDomainID = @numDomainID
                            AND Fld_type <> 'Link'
                    ORDER BY Custom ,
                            vcFieldName             
                END                      
                      
                   
    IF @PageId = 1
        OR @PageId = 4
        BEGIN
 
            IF ( @FormId = 96
                 OR @FormId = 97
               )
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        vcDbColumnName ,
                        0 AS Custom ,
                        tintRow AS tintOrder
                FROM    View_DynamicColumns
                WHERE   numFormId = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND ISNULL(bitCustom, 0) = 0
                        AND tintPageType = 1
                        AND ISNULL(bitSettingField, 0) = 1
                        AND numRelCntType = @numtype
                        AND ISNULL(bitDeleted, 0) = 0
                UNION
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                        vcFieldName ,
                        vcFieldName AS vcDbColumnName ,
                        1 AS Custom ,
                        tintRow AS tintOrder
                FROM    View_DynamicCustomColumns
                WHERE   numFormID = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND grp_id = @PageId
                        AND numDomainID = @numDomainID
                        AND vcAssociatedControlType <> 'Link'
                        AND ISNULL(bitCustom, 0) = 1
                        AND tintPageType = 1
                        AND numRelCntType = @numtype
                ORDER BY tintOrder                
            ELSE
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        vcDbColumnName ,
                        0 AS Custom ,
                        tintRow AS tintOrder
                FROM    View_DynamicColumns
                WHERE   numFormId = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND ISNULL(bitCustom, 0) = 0
                        AND tintPageType = 1
                        AND ISNULL(bitSettingField, 0) = 1
                        AND numRelCntType = @numtype
                        AND ISNULL(bitDeleted, 0) = 0
                UNION
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                        vcFieldName ,
                        '' AS vcDbColumnName ,
                        1 AS Custom ,
                        tintRow AS tintOrder
                FROM    View_DynamicCustomColumns
                WHERE   numFormID = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND grp_id = @PageId
                        AND numDomainID = @numDomainID
                        AND vcAssociatedControlType <> 'Link'
                        AND ISNULL(bitCustom, 0) = 1
                        AND tintPageType = 1
                        AND numRelCntType = @numtype
                ORDER BY tintOrder          
        END     
    ELSE
        IF @PageId = 10
            BEGIN
	
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        vcDbColumnName ,
                        0 AS Custom ,
                        tintRow AS tintOrder ,
                        numListID ,
                        numFieldID AS [numFieldID1]
                FROM    View_DynamicColumns
                WHERE   numFormId = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND ISNULL(bitCustom, 0) = 0
                        AND tintPageType = 1
                        AND ISNULL(bitSettingField, 0) = 1
                        AND numRelCntType = @numtype
                        AND ISNULL(bitDeleted, 0) = 0
                UNION
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                        vcFieldName ,
                        '' AS vcDbColumnName ,
                        1 AS Custom ,
                        tintRow AS tintOrder ,
                        numListID ,
                        numFieldID AS [numFieldID1]
                FROM    View_DynamicCustomColumns
                WHERE   numFormID = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID   
   --and grp_id=@PageId    
                        AND numDomainID = @numDomainID
                        AND vcAssociatedControlType = 'SelectBox'
                        AND ISNULL(bitCustom, 0) = 1
                        AND tintPageType = 1
                        AND numRelCntType = @numtype
                ORDER BY tintOrder    
--   
--    SELECT DISTINCT numFieldID as numFieldID,'Archived Items' AS vcFieldName,bitCustom as Custom 
--	   FROM DycFormConfigurationDetails
--	   WHERE 1=1
--	   AND numFormID = @FormId
--	   AND ISNULL(numFieldID,0)= -1
--	   AND numDomainID = @numDomainID	
            END	   
--ELSE IF @PageId = 12
--	BEGIN
--		SELECT  CONVERT(VARCHAR(9),numFieldID) + '~0' numFieldID,
--				ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
--				vcDbColumnName ,
--				0 AS Custom,
--				tintRow AS tintOrder,
--				vcAssociatedControlType,
--				numlistid,
--				vcLookBackTableName,
--				numFieldID AS FieldId,
--				bitAllowEdit                    
--		FROM View_DynamicColumns
--		WHERE numFormId = @FormId 
--		AND numUserCntID = @numUserCntID 
--		AND  numDomainID = @numDomainID  
--	    AND ISNULL(bitCustom,0) = 0 
--		AND tintPageType = 1 
--		AND ISNULL(bitSettingField,0) = 1 
--		AND ISNULL(numRelCntType,0) = @numtype 
--		AND ISNULL(bitDeleted,0) = 0
--	    AND  ISNULL(numViewID,0) = @numViewID   
--	   
--	    UNION
--	   
--	    SELECT  CONVERT(VARCHAR(9),numFieldID)+'~1' AS numFieldID ,
--				vcFieldName ,'' AS vcDbColumnName,
--				1 AS Custom,
--				tintRow AS tintOrder,
--				vcAssociatedControlType,
--				numlistid,'' AS vcLookBackTableName,
--				numFieldID AS FieldId,
--				0 AS bitAllowEdit           
--	    FROM View_DynamicCustomColumns          
--	    WHERE numFormID = @FormId 
--		AND numUserCntID = @numUserCntID 
--		--AND numDomainID=@numDomainID
--		AND grp_id = @PageId  
--		AND numDomainID = @numDomainID  
--		AND vcAssociatedControlType <> 'Link' 
--		AND ISNULL(bitCustom,0) = 1 
--		--AND tintPageType = 1  
--		---AND ISNULL(numRelCntType,0)= @numtype     
--		--AND  ISNULL(numViewID,0) = @numViewID
--	    ORDER BY tintOrder     
--
--	END 
        ELSE
            IF @PageId = 5
                AND ( @FormId = 84
                      OR @FormId = 85
                    )
                BEGIN

                    IF EXISTS ( SELECT  'col1'
                                FROM    DycFormConfigurationDetails
                                WHERE   numDomainID = @numDomainID
                                        AND numFormId = @FormID
                                        AND numFieldID IN ( 507, 508, 509, 510,
                                                            511, 512 ) )
                        BEGIN
                            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                                    vcDbColumnName ,
                                    0 AS Custom ,
                                    tintRow AS tintOrder ,
                                    vcAssociatedControlType ,
                                    numlistid ,
                                    vcLookBackTableName ,
                                    numFieldID AS FieldId ,
                                    bitAllowEdit ,
                                    0 AS AZordering
                            FROM    View_DynamicColumns
                            WHERE   numFormId = @FormID
                                    AND numDomainID = @numDomainID
                                    AND ISNULL(bitCustom, 0) = 0   
		 --AND tintPageType = 1   
                                    AND ISNULL(bitSettingField, 0) = 1
                                    AND ISNULL(bitDeleted, 0) = 0
                            UNION
                            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                                    vcFieldName ,
                                    '' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    tintRow AS tintOrder ,
                                    vcAssociatedControlType ,
                                    numlistid ,
                                    '' AS vcLookBackTableName ,
                                    numFieldID AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    0 AS AZordering
                            FROM    View_DynamicCustomColumns
                            WHERE   numFormID = @FormID
                                    AND numDomainID = @numDomainID
                                    AND grp_id = @PageID
                                    AND vcAssociatedControlType = 'TextBox'
                                    AND ISNULL(bitCustom, 0) = 1
                            ORDER BY tintOrder 
                        END
                    ELSE
                        BEGIN
                            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                                    vcDbColumnName ,
                                    0 AS Custom ,
                                    tintRow AS tintOrder ,
                                    vcAssociatedControlType ,
                                    numlistid ,
                                    vcLookBackTableName ,
                                    numFieldID AS FieldId ,
                                    bitAllowEdit ,
                                    0 AS AZordering
                            FROM    View_DynamicColumns
                            WHERE   numFormId = @FormID
                                    AND numDomainID = @numDomainID
                                    AND ISNULL(bitCustom, 0) = 0   
		 --AND tintPageType = 1   
                                    AND ISNULL(bitSettingField, 0) = 1
                                    AND ISNULL(bitDeleted, 0) = 0
                            UNION
                            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                                    vcFieldName ,
                                    '' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    tintRow AS tintOrder ,
                                    vcAssociatedControlType ,
                                    numlistid ,
                                    '' AS vcLookBackTableName ,
                                    numFieldID AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    0 AS AZordering
                            FROM    View_DynamicCustomColumns
                            WHERE   numFormID = @FormID
                                    AND numDomainID = @numDomainID
                                    AND grp_id = @PageID
                                    AND vcAssociatedControlType = 'TextBox'
                                    AND ISNULL(bitCustom, 0) = 1
                            UNION
                            SELECT  '507~0' AS numFieldID ,
                                    'Title:A-Z' AS vcFieldName ,
                                    'Title:A-Z' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    0 AS tintOrder ,
                                    ' ' AS vcAssociatedControlType ,
                                    1 AS numlistid ,
                                    '' AS vcLookBackTableName ,
                                    507 AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    1 AS IsFixed
                            UNION
                            SELECT  '508~0' AS numFieldID ,
                                    'Title:Z-A' AS vcFieldName ,
                                    'Title:Z-A' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    0 AS tintOrder ,
                                    ' ' AS vcAssociatedControlType ,
                                    1 AS numlistid ,
                                    '' AS vcLookBackTableName ,
                                    508 AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    1 AS IsFixed
                            UNION
                            SELECT  '509~0' AS numFieldID ,
                                    'Price:Low to High' AS vcFieldName ,
                                    'Price:Low to High' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    0 AS tintOrder ,
                                    ' ' AS vcAssociatedControlType ,
                                    1 AS numlistid ,
                                    '' AS vcLookBackTableName ,
                                    509 AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    1 AS IsFixed
                            UNION
                            SELECT  '510~0' AS numFieldID ,
                                    'Price:High to Low' AS vcFieldName ,
                                    'Price:High to Low' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    0 AS tintOrder ,
                                    ' ' AS vcAssociatedControlType ,
                                    1 AS numlistid ,
                                    '' AS vcLookBackTableName ,
                                    510 AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    1 AS IsFixed
                            UNION
                            SELECT  '511~0' AS numFieldID ,
                                    'New Arrival' AS vcFieldName ,
                                    'New Arrival' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    0 AS tintOrder ,
                                    ' ' AS vcAssociatedControlType ,
                                    1 AS numlistid ,
                                    '' AS vcLookBackTableName ,
                                    511 AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    1 AS IsFixed
                            UNION
                            SELECT  '512~0' AS numFieldID ,
                                    'Oldest' AS vcFieldName ,
                                    'Oldest' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    0 AS tintOrder ,
                                    ' ' AS vcAssociatedControlType ,
                                    1 AS numlistid ,
                                    '' AS vcLookBackTableName ,
                                    512 AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    1 AS IsFixed
                            ORDER BY tintOrder 

                        END



    

                END
            ELSE
                BEGIN  
 
                    SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                            ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                            vcDbColumnName ,
                            0 AS Custom ,
                            tintRow AS tintOrder ,
                            vcAssociatedControlType ,
                            numlistid ,
                            vcLookBackTableName ,
                            numFieldID AS FieldId ,
                            bitAllowEdit
                    FROM    View_DynamicColumns
                    WHERE   numFormId = @FormId
                            AND numUserCntID = @numUserCntID
                            AND numDomainID = @numDomainID
                            AND ISNULL(bitCustom, 0) = 0
                            AND tintPageType = 1
                            AND ISNULL(bitSettingField, 0) = 1
                            AND ISNULL(numRelCntType, 0) = @numtype
                            AND ISNULL(bitDeleted, 0) = 0
                            AND ISNULL(numViewID, 0) = @numViewID
                    UNION
                    SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                            vcFieldName ,
                            '' AS vcDbColumnName ,
                            1 AS Custom ,
                            tintRow AS tintOrder ,
                            vcAssociatedControlType ,
                            numlistid ,
                            '' AS vcLookBackTableName ,
                            numFieldID AS FieldId ,
                            0 AS bitAllowEdit
                    FROM    View_DynamicCustomColumns
                    WHERE   numFormID = @FormId
                            AND numUserCntID = @numUserCntID
                            AND numDomainID = @numDomainID
                            AND grp_id = @PageId
                            AND numDomainID = @numDomainID
                            AND vcAssociatedControlType <> 'Link'
                            AND ISNULL(bitCustom, 0) = 1
                            AND tintPageType = 1
                            AND ISNULL(numRelCntType, 0) = @numtype
                            AND ISNULL(numViewID, 0) = @numViewID
                    ORDER BY tintOrder          
                END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetDealsList1]    Script Date: 05/07/2009 22:12:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj

--exec USP_GetDealsList1 
--@numUserCntID=85098,
--@numDomainID=110,
--@tintSalesUserRightType=3,
--@tintPurchaseUserRightType=3,
--@tintSortOrder=1,
--@SortChar=0,
--@FirstName='',
--@LastName='',
--@CustName='',
--@CurrentPage=1,
--@PageSize=50,
--@TotRecs=0,
--@columnName='vcCompanyName',
--@columnSortOrder='Asc',
--@numCompanyID=0,
--@bitPartner=0,
--@ClientTimeZoneOffset=-180,
--@tintShipped=0,
--@intOrder=0,
--@intType=1,
--@tintFilterBy=0


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdealslist1')
DROP PROCEDURE usp_getdealslist1
GO
CREATE PROCEDURE [dbo].[USP_GetDealsList1]
               @numUserCntID              NUMERIC(9)  = 0,
               @numDomainID               NUMERIC(9)  = 0,
               @tintSalesUserRightType    TINYINT  = 0,
               @tintPurchaseUserRightType TINYINT  = 0,
               @tintSortOrder             TINYINT  = 4,
               @SortChar                  CHAR(1)  = '0',
               @FirstName                 VARCHAR(100)  = '',
               @LastName                  VARCHAR(100)  = '',
               @CustName                  VARCHAR(100)  = '',
               @CurrentPage               INT,
               @PageSize                  INT,
               @TotRecs                   INT  OUTPUT,
               @columnName                AS VARCHAR(50),
               @columnSortOrder           AS VARCHAR(10),
               @numCompanyID              AS NUMERIC(9)  = 0,
               @bitPartner                AS BIT  = 0,
               @ClientTimeZoneOffset      AS INT,
               @tintShipped               AS TINYINT,
               @intOrder                  AS TINYINT,
               @intType                   AS TINYINT,
               @tintFilterBy              AS TINYINT  = 0,
               @numOrderStatus			  AS NUMERIC,
			   @vcRegularSearchCriteria varchar(1000)='',
			   @vcCustomSearchCriteria varchar(1000)='' 
AS
  DECLARE  @PageId  AS TINYINT
  DECLARE  @numFormId  AS INT 
  
  SET @PageId = 0
  IF @inttype = 1
  BEGIN
  	SET @PageId = 2
  	SET @inttype = 3
  	SET @numFormId=39
  END
  IF @inttype = 2
  BEGIN
  	SET @PageId = 6
    SET @inttype = 4
    SET @numFormId=41
  END
  --Create a Temporary table to hold data
  CREATE TABLE #tempTable (
    ID              INT   IDENTITY   PRIMARY KEY,
    numOppId        NUMERIC(9),intTotalProgress INT,
	vcInventoryStatus	VARCHAR(100))
    
 DECLARE @strShareRedordWith AS VARCHAR(300);
 SET @strShareRedordWith=' Opp.numOppId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=3 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '

  DECLARE  @strSql  AS VARCHAR(8000)
  SET @strSql = 'SELECT  Opp.numOppId,PP.intTotalProgress,RTRIM(LTRIM(REPLACE(SUBSTRING(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),CHARINDEX(''>'',dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) + 1, CHARINDEX(''<font>'',dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID))),''<font>'',''''))) AS [vcInventoryStatus]'
  
  DECLARE  @strOrderColumn  AS VARCHAR(100);SET @strOrderColumn=@columnName
--  
--   IF LEN(@columnName)>0
--   BEGIN
--	 IF CHARINDEX(@columnName,@strSql)=0
--	 BEGIN
--		SET @strSql= @strSql + ',' + @columnName
--	 	SET @strOrderColumn=@columnName
--	 END
--   END
   
  SET @strSql = @strSql + ' FROM OpportunityMaster Opp                                                               
                                 INNER JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId                                                               
                                 INNER JOIN DivisionMaster Div ON Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID
								 INNER JOIN CompanyInfo cmp ON Div.numCompanyID = cmp.numCompanyId 
								 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId '
                                 --left JOIN OpportunityLinking OL on OL.numChildOppID=Opp.numOppId '
								 
  IF @bitPartner = 1
    SET @strSql = @strSql + ' left join OpportunityContact OppCont on OppCont.numOppId=Opp.numOppId and OppCont.bitPartner=1 and OppCont.numContactId=' + CONVERT(VARCHAR(15),@numUserCntID)
                    
   
   if @columnName like 'CFW.Cust%'             
	begin            
		SET @strSql = @strSql + ' left Join CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid  and CFW.fld_id= '+replace(@columnName,'CFW.Cust','') 
		SET @strOrderColumn = 'CFW.Fld_Value'   
	end                                     
  ELSE if @columnName like 'DCust%'            
	begin            
	 SET @strSql = @strSql + ' left Join CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid   and CFW.fld_id= '+replace(@columnName,'DCust','')                                                                                               
	 SET @strSql = @strSql + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '            
	 SET @strOrderColumn = 'LstCF.vcData'  
	end       

             
  IF @tintFilterBy = 1 --Partially Fulfilled Orders 
    SET @strSql = @strSql + ' left join AdditionalContactsInformation ADC1 on ADC1.numContactId=Opp.numRecOwner                                                               
                             LEFT OUTER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = Opp.[numOppId] 
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and Opp.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped)
  ELSE
    SET @strSql = @strSql + ' left join AdditionalContactsInformation ADC1 on ADC1.numContactId=Opp.numRecOwner                                                               
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and Opp.tintShipped='+ CONVERT(VARCHAR(15),@tintShipped) 
            
                    
  IF @FirstName <> ''
    SET @strSql = @strSql + ' and ADC.vcFirstName  like ''' + @FirstName + '%'''
    
  IF @LastName <> '' 
    SET @strSql = @strSql + ' and ADC.vcLastName like ''' + @LastName + '%'''
  
  IF @CustName <> ''
    SET @strSql = @strSql + ' and cmp.vcCompanyName like ''' + @CustName + '%'''
  
  
  IF @intOrder <> 0
    BEGIN
      IF @intOrder = 1
        SET @strSql = @strSql + ' and Opp.bitOrder = 0 '
      IF @intOrder = 2
        SET @strSql = @strSql + ' and Opp.bitOrder = 1'
    END
    
  IF @numCompanyID <> 0
    SET @strSql = @strSql + ' And Div.numCompanyID ='+ CONVERT(VARCHAR(15),@numCompanyID)
    
  IF @SortChar <> '0'
    SET @strSql = @strSql + ' And Opp.vcPOppName like ''' + @SortChar + '%'''
  
  
  IF @tintSalesUserRightType = 0 OR @tintPurchaseUserRightType = 0
    SET @strSql = @strSql + ' AND opp.tintOppType =0'
  ELSE IF @tintSalesUserRightType = 1 OR @tintPurchaseUserRightType = 1
    SET @strSql = @strSql + ' AND (Opp.numRecOwner= ' + CONVERT(VARCHAR(15),@numUserCntID)
                    + ' or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
                    --+ ' or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where  numAssignTo ='+ CONVERT(VARCHAR(15),@numUserCntID)+ ')'
                    + CASE WHEN @bitPartner = 1 THEN ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+ CONVERT(VARCHAR(15),@numUserCntID) + ')' ELSE '' END
                    + ' or ' + @strShareRedordWith +')'
  ELSE IF @tintSalesUserRightType = 2 OR @tintPurchaseUserRightType = 2
      SET @strSql = @strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) 
								or div.numTerID=0 or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
								 --+ ' or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo ='+ CONVERT(VARCHAR(15),@numUserCntID)+ ')' 
								+ ' or ' + @strShareRedordWith +')'
	
								
  IF @tintSortOrder <> '0'
    SET @strSql = @strSql + '  AND Opp.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder)
    
    
 IF @tintFilterBy = 1 --Partially Fulfilled Orders
    SET @strSql = @strSql + ' AND OB.bitPartialFulfilment = 1 AND OB.bitAuthoritativeBizDocs = 1 '
 ELSE IF @tintFilterBy = 2 --Fulfilled Orders
    SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT OM.numOppId FROM [OpportunityMaster] OM
                      			INNER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = OM.[numOppId]
                      			INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId]
                      			INNER JOIN [OpportunityBizDocItems] BI ON BI.[numOppBizDocID] = OB.[numOppBizDocsId]
                      			Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + 'AND OB.[bitAuthoritativeBizDocs] = 1 GROUP BY OM.[numOppId]
                      			HAVING   COUNT(OI.[numUnitHour]) = COUNT(BI.[numUnitHour])) '
 ELSE IF @tintFilterBy = 3 --Orders without Authoritative-BizDocs
    SET @strSql = @strSql + ' AND Opp.[numOppId] not IN (SELECT DISTINCT OM.[numOppId]
                      		  FROM [OpportunityMaster] OM JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		  Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OM.tintOppstatus=1 AND OM.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped) + ' AND OM.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder) + ' AND isnull(OB.[bitAuthoritativeBizDocs],0)=1) '
--ELSE IF @tintFilterBy = 4 --Orders with child records
--    SET @strSql = @strSql
--                    + ' AND Opp.[numOppId] IN (SELECT numParentOppID FROM dbo.OpportunityLinking OL INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OL.numParentOppID WHERE OM.numDomainId='+ CONVERT(VARCHAR(15),@numDomainID) + ' )  '

 ELSE IF @tintFilterBy = 4 --Orders with items yet to be added to Invoices
    SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT distinct OPPM.numOppId FROM [OpportunityMaster] OPPM
                                  INNER JOIN [OpportunityItems] OPPI ON OPPM.[numOppId] = OPPI.[numOppId]
                                  Where OPPM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' 
								  and OPPI.numoppitemtCode not in (select numOppItemID from [OpportunityBizDocs] OB INNER JOIN [OpportunityBizDocItems] BI
                      			  ON BI.[numOppBizDocID] = OB.[numOppBizDocsId] where OB.[numOppId] = OPPM.[numOppId]))'      
 ELSE IF @tintFilterBy = 6 --Orders with deferred Income/Invoices
    SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT DISTINCT OM.[numOppId]
                      		 FROM [OpportunityMaster] OM LEFT OUTER JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		 Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OB.[bitAuthoritativeBizDocs] =1 and OB.tintDeferred=1)'

IF @numOrderStatus <>0 
	SET @strSql = @strSql + ' AND Opp.numStatus = ' + CONVERT(VARCHAR(15),@numOrderStatus);

IF CHARINDEX('vcInventoryStatus',@vcRegularSearchCriteria) > 0
BEGIN
	IF CHARINDEX('vcInventoryStatus=1',@vcRegularSearchCriteria) > 0
		SET @strSql = @strSql + ' and ' + ' CHARINDEX(''Not Applicable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 '
	IF CHARINDEX('vcInventoryStatus=2',@vcRegularSearchCriteria) > 0
		SET @strSql = @strSql + ' and ' + ' CHARINDEX(''Allocation Cleared'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 '
	IF CHARINDEX('vcInventoryStatus=3',@vcRegularSearchCriteria) > 0
		SET @strSql = @strSql + ' and ' + ' CHARINDEX(''Back Order'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 '
	IF CHARINDEX('vcInventoryStatus=4',@vcRegularSearchCriteria) > 0
		SET @strSql = @strSql + ' and ' + ' CHARINDEX(''Ready to Ship'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 '
END
ELSE IF @vcRegularSearchCriteria<>'' 
BEGIN
	SET @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
END
	
IF @vcCustomSearchCriteria<>'' 
	set @strSql=@strSql+' and Opp.numOppid in (select distinct CFW.RecId from CFW_Fld_Values_Opp CFW where ' + @vcCustomSearchCriteria + ')'

 --SET @strSql = 'SELECT numOppId from (' + @strSql + ') a'

 IF LEN(@strOrderColumn)>0
	IF @strOrderColumn = 'OI.vcInventoryStatus'
		BEGIN
			SET @strSql =  @strSql + ' ORDER BY  RTRIM(LTRIM(REPLACE(SUBSTRING(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),CHARINDEX(''>'',dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) + 1, CHARINDEX(''<font>'',dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID))),''<font>'','''')))'  + ' ' + @columnSortOrder    
		END
	ELSE
		BEGIN
	SET @strSql =  @strSql + ' ORDER BY  ' + @strOrderColumn + ' ' + @columnSortOrder    
		END

	
  PRINT @strSql
  INSERT INTO #tempTable (numOppId,intTotalProgress,vcInventoryStatus) EXEC( @strSql)
  
  DECLARE  @firstRec  AS INTEGER
  DECLARE  @lastRec  AS INTEGER
  SET @firstRec = (@CurrentPage - 1) * @PageSize
  SET @lastRec = (@CurrentPage * @PageSize + 1)
  
  SET @TotRecs = (SELECT COUNT(* ) FROM #tempTable)
  
  DECLARE  @tintOrder  AS TINYINT;SET @tintOrder = 0
  DECLARE  @vcFieldName  AS VARCHAR(50)
  DECLARE  @vcListItemType  AS VARCHAR(3)
  DECLARE  @vcListItemType1  AS VARCHAR(1)
  DECLARE  @vcAssociatedControlType VARCHAR(30)
  DECLARE  @numListID  AS NUMERIC(9)
  DECLARE  @vcDbColumnName VARCHAR(40)
  DECLARE  @WhereCondition VARCHAR(2000);SET @WhereCondition = ''
  DECLARE  @vcLookBackTableName VARCHAR(2000)
  DECLARE  @bitCustom  AS BIT
  Declare @numFieldId as numeric  
  DECLARE @bitAllowSorting AS CHAR(1)   
  DECLARE @bitAllowEdit AS CHAR(1)                 
  DECLARE @vcColumnName AS VARCHAR(500)                  
  DECLARE  @Nocolumns  AS TINYINT;SET @Nocolumns = 0

  SELECT @Nocolumns=isnull(sum(TotalRow),0) from(            
	SELECT count(*) TotalRow from View_DynamicColumns where numFormId=@numFormId and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType = @numFormId
	UNION 
	SELECT count(*) TotalRow from View_DynamicCustomColumns where numFormId=@numFormId and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType = @numFormId) TotalRows


  CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1))
    
  SET @strSql = ''
  SET @strSql = 'select ADC.numContactId,DM.numDivisionID,isnull(DM.numTerID,0) numTerID,opp.numRecOwner as numRecOwner,DM.tintCRMType,opp.numOppId'

--Check if Master Form Configuration is created by administrator if NoColumns=0
DECLARE @IsMasterConfAvailable BIT = 0
DECLARE @numUserGroup NUMERIC(18,0)

IF @Nocolumns=0
BEGIN
	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormId AND numRelCntType=@numFormId AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END
END


--If MasterConfiguration is available then load it otherwise load default columns
IF @IsMasterConfAvailable = 1
BEGIN
	INSERT INTO #tempForm
	SELECT 
		(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
		vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
		bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
		ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
	FROM 
		View_DynamicColumnsMasterConfig
	WHERE
		View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
		View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
		View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
		ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	UNION
    SELECT 
		tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
		bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,0,''
	FROM 
		View_DynamicCustomColumnsMasterConfig
	WHERE 
		View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
		View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
		View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
	ORDER BY 
		tintOrder ASC  
END
ELSE                                        
BEGIN
if @Nocolumns=0
BEGIN
INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
select @numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
 FROM View_DynamicDefaultColumns
 where numFormId=@numFormId and bitDefault=1 AND ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID
order by tintOrder asc   

END

    
    INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
 FROM View_DynamicColumns 
 where numFormId=@numFormId and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0  AND numRelCntType = @numFormId
 
 UNION
    
     select tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,0,'' 
 from View_DynamicCustomColumns
 where numFormId=@numFormId and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
 AND ISNULL(bitCustom,0)=1  AND numRelCntType = @numFormId
 
  order by tintOrder asc  
END


--  SET @DefaultNocolumns = @Nocolumns
Declare @ListRelID as numeric(9) 

  select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
@ListRelID=ListRelID
  from  #tempForm order by tintOrder asc            

  WHILE @tintOrder > 0
    BEGIN
      IF @bitCustom = 0
        BEGIN
          DECLARE  @Prefix  AS VARCHAR(5)
          IF @vcLookBackTableName = 'AdditionalContactsInformation'
            SET @Prefix = 'ADC.'
          IF @vcLookBackTableName = 'DivisionMaster'
            SET @Prefix = 'DM.'
          IF @vcLookBackTableName = 'OpportunityMaster'
            SET @PreFix = 'Opp.'
          IF @vcLookBackTableName = 'OpportunityRecurring'
            SET @PreFix = 'OPR.'
            
		 SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'
            
          IF @vcAssociatedControlType = 'SelectBox'
            BEGIN
            IF @vcDbColumnName = 'tintSource'
            BEGIN
              SET @strSql = @strSql + ',ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'')' + ' [' + @vcColumnName + ']'
            END
            
			IF @vcDbColumnName = 'vcInventoryStatus'
            BEGIN
			SET @strSql = @strSql + ', ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),'''') ' + ' [' + @vcColumnName + ']'
			END

            ELSE IF @vcDbColumnName = 'numCampainID'
            BEGIN
              SET @strSql = @strSql + ', (SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID) ' + ' [' + @vcColumnName + ']'
            END
            
            ELSE IF @vcListItemType = 'LI'
            BEGIN
                  SET @strSql = @strSql + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
                  SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
            END
            ELSE IF @vcListItemType = 'S'
            BEGIN
                    SET @strSql = @strSql + ',S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState' + ' [' + @vcColumnName + ']'
                    SET @WhereCondition = @WhereCondition + ' left join State S' + CONVERT(VARCHAR(3),@tintOrder) + ' on S' + CONVERT(VARCHAR(3),@tintOrder) + '.numStateID=' + @vcDbColumnName
            END
            ELSE IF @vcListItemType = 'BP'
            BEGIN
                    SET @strSql = @strSql + ',SPLM.Slp_Name' + ' [' + @vcColumnName + ']'
                    SET @WhereCondition = @WhereCondition + ' LEFT JOIN Sales_process_List_Master SPLM on SPLM.Slp_Id = OPP.numBusinessProcessID '
                  END
            ELSE IF @vcListItemType = 'PP'
            BEGIN
                              SET @strSql = @strSql + ',ISNULL(T.intTotalProgress,0)' + ' [' + @vcColumnName + ']'

--                SET @strSql = @strSql 
--                                + ',ISNULL(PP.intTotalProgress,0)'
--                                + ' [' +
--                                @vcColumnName + ']'
--                SET @WhereCondition = @WhereCondition
--                                        + ' LEFT JOIN ProjectProgress PP ON PP.numDomainID='+ convert(varchar(15),@numDomainID )+' and OPP.numOppID = PP.numOppID '
            END
            ELSE IF @vcListItemType = 'T'
                    BEGIN
                      SET @strSql = @strSql + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
                      SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
                    END
             ELSE IF @vcListItemType = 'U'
                      BEGIN
                        SET @strSql = @strSql + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
                      END
              ELSE IF @vcListItemType = 'WI'
				BEGIN
					SET @strSql = @strSql + ', Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end [' + @vcColumnName + ']'
					set @WhereCondition= @WhereCondition +' left JOIN OpportunityLinking OL on OL.numChildOppID=Opp.numOppId left join WebAPI on WebAPI.WebApiId=isnull(OL.numWebApiId,0)
													left join Sites on isnull(OL.numSiteID,0)=Sites.numSIteID and Sites.numDomainID=' + convert(varchar(15),@numDomainID)                                                       
				END 
            END
          ELSE
            IF @vcAssociatedControlType = 'DateField'
              BEGIN
              if @Prefix ='OPR.'
				 BEGIN
				  	SET @strSql = @strSql + ', (SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'+convert(varchar(15),-@ClientTimeZoneOffset)+', dtRecurringDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) AS  ['+ @vcColumnName + ']'
				 END
				 else
				 BEGIN
					SET @strSql = @strSql
                                + ',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
                                + @Prefix
                                + @vcDbColumnName
                                + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''
					SET @strSql = @strSql
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''
					SET @strSql = @strSql
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strSql = @strSql
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']' 	
				 END
                
                                
					
						
              END
            ELSE
              IF @vcAssociatedControlType = 'TextBox'
                BEGIN
                  SET @strSql = @strSql
                                  + ','
                                  + CASE 
                                      WHEN @vcDbColumnName = 'numAge' THEN 'year(getutcdate())-year(bintDOB)'
                                      WHEN @vcDbColumnName = 'monPAmount' THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(MONEY,monPAmount),1)'
                                      WHEN @vcDbColumnName = 'numDivisionID' THEN 'DM.numDivisionID'
                                      WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
                                      WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
                                      WHEN @vcDbColumnName = 'vcPOppName' THEN 'Opp.vcPOppName'
									  WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
	FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId FOR XML PATH('''')),4,200000)),'''')'
                                      ELSE @vcDbColumnName
                                    END
                                  + ' ['
                                  + @vcColumnName + ']'
                END
              ELSE
                IF @vcAssociatedControlType = 'TextArea'
                  BEGIN
						set @strSql=@strSql+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']'  
                  END
                   else if  @vcAssociatedControlType='Label'                                              
begin  
 set @strSql=@strSql+','+ case                    
	WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'--'[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
    WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
    WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
    else @vcDbColumnName END +' ['+ @vcColumnName+']'            
 END

                   	else if @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
		begin      
			 set @strSql=@strSql+',(SELECT SUBSTRING(
	(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
				AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
		 end 
        END
      ELSE
        BEGIN
        
            SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

          IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
            BEGIN
              SET @strSql = @strSql
                              + ',CFW'
                              + CONVERT(VARCHAR(3),@tintOrder)
                              + '.Fld_Value  ['
                              + @vcColumnName + ']'
              SET @WhereCondition = @WhereCondition
                                      + ' left Join CFW_FLD_Values_Opp CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '               
                                                                                                                            on CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.Fld_Id='
                                      + CONVERT(VARCHAR(10),@numFieldId)
                                      + 'and CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.RecId=Opp.numOppid   '
            END
          ELSE
            IF @vcAssociatedControlType = 'CheckBox' 
              BEGIN
                SET @strSql = @strSql
                                + ',case when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=0 then ''No'' when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=1 then ''Yes'' end   ['
                                + @vcColumnName + ']'
                SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '           
                                                                                                                                  on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid   '
              END
            ELSE
              IF @vcAssociatedControlType = 'DateField'
                BEGIN
                  SET @strSql = @strSql
                                  + ',dbo.FormatedDateFromDate(CFW'
                                  + CONVERT(VARCHAR(3),@tintOrder)
                                  + '.Fld_Value,'
                                  + CONVERT(VARCHAR(10),@numDomainId)
                                  + ')  ['
                                  + @vcColumnName + ']'
                  SET @WhereCondition = @WhereCondition
                                          + ' left Join CFW_FLD_Values_Opp CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '               
                                                                                                                                        on CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.Fld_Id='
                                          + CONVERT(VARCHAR(10),@numFieldId)
                                          + 'and CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.RecId=Opp.numOppid    '
                END
              ELSE
                IF @vcAssociatedControlType = 'SelectBox'
                  BEGIN
                    SET @vcDbColumnName = 'DCust'
                                            + CONVERT(VARCHAR(10),@numFieldId)
                    SET @strSql = @strSql
                                    + ',L'
                                    + CONVERT(VARCHAR(3),@tintOrder)
                                    + '.vcData'
                                    + ' ['
                                    + @vcColumnName + ']'
                    SET @WhereCondition = @WhereCondition
                                            + ' left Join CFW_FLD_Values_Opp CFW'
                                            + CONVERT(VARCHAR(3),@tintOrder)
                                            + '               
                                                                                                                                               on CFW'
                                            + CONVERT(VARCHAR(3),@tintOrder)
                                            + '.Fld_Id='
                                            + CONVERT(VARCHAR(10),@numFieldId)
                                            + 'and CFW'
                                            + CONVERT(VARCHAR(3),@tintOrder)
                                            + '.RecId=Opp.numOppid     '
                    SET @WhereCondition = @WhereCondition
                                            + ' left Join ListDetails L'
                                            + CONVERT(VARCHAR(3),@tintOrder)
                                            + ' on L'
                                            + CONVERT(VARCHAR(3),@tintOrder)
                                            + '.numListItemID=CFW'
                                            + CONVERT(VARCHAR(3),@tintOrder)
                                            + '.Fld_Value'
                  END
        END
      
     
       select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
@ListRelID=ListRelID
  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc            
 
  if @@rowcount=0 set @tintOrder=0 
 END 
  
  -------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

--select top 1 @vcCSOrigDbCOlumnName=DFM.vcOrigDbCOlumnName,@vcCSLookBackTableName=DFM.vcLookBackTableName from DycFieldColorScheme DFCS join DycFieldMaster DFM on 
--DFCS.numModuleID=DFM.numModuleID and DFCS.numFieldID=DFM.numFieldID
--where DFCS.numDomainID=@numDomainID and DFM.numModuleID=1

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=@numFormId AND DFCS.numFormID=@numFormId and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
----------------------------                       

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
set @strSql=@strSql+',tCS.vcColorScheme'
END

  
  SET @strSql = @strSql + ' FROM OpportunityMaster Opp                                                               
                            INNER JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId                                                               
							INNER JOIN DivisionMaster DM ON Opp.numDivisionId = DM.numDivisionID AND ADC.numDivisionId = DM.numDivisionID                                                               
                            INNER JOIN CompanyInfo cmp ON DM.numCompanyID = cmp.numCompanyId '
  
  IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
	 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
		set @Prefix = 'ADC.'                  
     if @vcCSLookBackTableName = 'DivisionMaster'                  
		set @Prefix = 'DM.'
	 if @vcCSLookBackTableName = 'CompanyInfo'                  
		set @Prefix = 'CMP.'   
	 if @vcCSLookBackTableName = 'OpportunityMaster'                  
		set @Prefix = 'Opp.'   
 
	IF @vcCSAssociatedControlType='DateField'
	BEGIN
			set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
			 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
	END
	ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
	BEGIN
		set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
	END
END
                          
if @columnName like 'CFW.Cust%'             
begin            
 SET @strSql = @strSql + ' left Join CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid and CFW.fld_id= '+replace(@columnName,'CFW.Cust','') +' '                                                     
 set @columnName='CFW.Fld_Value'            
end 

SET @strSql = @strSql   + @WhereCondition + ' join (select ID,numOppId,intTotalProgress,vcInventoryStatus FROM  #tempTable )T on T.numOppId=Opp.numOppId                                              
                          WHERE ID > ' + CONVERT(VARCHAR(10),@firstRec) + ' and ID <' + CONVERT(VARCHAR(10),@lastRec)
    
IF (@tintFilterBy = 5) --Sort alphabetically
   SET @strSql = @strSql +' order by ID '
ELSE IF @columnName = 'OI.vcInventoryStatus'  
   SET @strSql =  @strSql + ' ORDER BY  T.vcInventoryStatus '  + ' ' + @columnSortOrder     
else  IF (@columnName = 'numAssignedBy' OR @columnName = 'numAssignedTo')
   SET @strSql = @strSql + ' ORDER BY  opp.' +  @columnName + ' ' + @columnSortOrder 
else  IF (@columnName = 'PP.intTotalProgress')
   SET @strSql = @strSql + ' ORDER BY  T.intTotalProgress ' + @columnSortOrder 
ELSE IF @columnName <> 'opp.bintCreatedDate' and @columnName <> 'vcPOppName' and @columnName <> 'bintCreatedDate' and @columnName <> 'vcCompanyName'
   SET @strSql = @strSql + ' ORDER BY  ' +  @columnName + ' '+ @columnSortOrder
ELSE 
   SET @strSql = @strSql +' order by ID '

print @strSql                      
              
exec (@strSql)  

SELECT * FROM #tempForm

DROP TABLE #tempForm

drop table #tempColorScheme

DROP TABLE #tempTable
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetHeaderReturnDetails' ) 
    DROP PROCEDURE USP_GetHeaderReturnDetails
GO

-- EXEC USP_GetHeaderReturnDetails 53, 1
CREATE PROCEDURE [dbo].[USP_GetHeaderReturnDetails]
    (
      @numReturnHeaderID NUMERIC(9) = 0,
      @numDomainId NUMERIC(9),
      @tintReceiveType NUMERIC(9)=0
    )
AS 
    BEGIN
    
		DECLARE @tintType TINYINT,@tintReturnType TINYINT;SET @tintType=0
        
		SELECT @tintReturnType=tintReturnType FROM ReturnHeader WHERE numReturnHeaderID = @numReturnHeaderID    
   
		DECLARE @monEstimatedBizDocAmount AS money
    
		SET @tintType=CASE When @tintReturnType=1 AND @tintReceiveType=1 THEN 9 
				       When @tintReturnType=1 AND @tintReceiveType=2 THEN 7 
				       When @tintReturnType=1 AND @tintReceiveType=3 THEN 9 
				       When @tintReturnType=2 AND @tintReceiveType=2 THEN 8
				       When @tintReturnType=3 THEN 10
				       When @tintReturnType=4 THEN 9 END 
				       
		SELECT @monEstimatedBizDocAmount=monAmount + monTotalTax - monTotalDiscount FROM dbo.GetReturnDealAmount(@numReturnHeaderID,@tintType)
	
        SELECT  RH.numReturnHeaderID,
                vcRMA,
                ISNULL(vcBizDocName, '') AS vcBizDocName,
                RH.numDomainId,
                ISNULL(RH.numDivisionId, 0) AS [numDivisionId],
                ISNULL(RH.numContactId, 0) AS [numContactId],
                ISNULL(RH.numOppId,0) AS numOppId,
                tintReturnType,
                numReturnReason,
                numReturnStatus,
                monAmount,
                ISNULL(monBizDocAmount,0) AS monBizDocAmount,ISNULL(monBizDocUsedAmount,0) AS monAmountUsed,
                monTotalTax,
                monTotalDiscount,
                tintReceiveType,
                vcComments,
                RH.numCreatedBy,
                dtCreatedDate,
                RH.numModifiedBy,
                dtModifiedDate,
                numReturnItemID,
                RI.numItemCode,
                I.vcItemName,
				ISNULL(I.vcModelID,'') AS vcModelID,
				ISNULL(I.vcSKU,'') AS vcSKU,
                numUnitHour,
                numUnitHourReceived,
                monPrice,
                monTotAmount,
                vcItemDesc,
                RI.numWareHouseItemID,
                WItems.numWarehouseID,
                ISNULL(W.vcWareHouse,'') AS vcWareHouse,
                I.vcModelID,
                I.vcManufacturer,
                numUOMId,
                dbo.fn_GetListItemName(numReturnStatus) AS [vcStatus],
                dbo.fn_GetContactName(RH.numCreatedby) AS CreatedBy,
                dbo.fn_GetContactName(RH.numModifiedBy) AS ModifiedBy,
                ISNULL(C2.vcCompanyName, '')
                + CASE WHEN ISNULL(D.numCompanyDiff, 0) > 0
                       THEN '  '
                            + ISNULL(dbo.fn_getlistitemname(D.numCompanyDiff),
                                     '') + ':' + ISNULL(D.vcCompanyDiff, '')
                       ELSE ''
                  END AS vcCompanyname,
                ISNULL(D.tintCRMType, 0) AS [tintCRMType],
                ISNULL(numAccountID, 0) AS [numAccountID],
                ISNULL(vcCheckNumber, '') AS [vcCheckNumber],
                ISNULL(IsCreateRefundReceipt, 0) AS [IsCreateRefundReceipt],
                ( SELECT    vcData
                  FROM      dbo.ListDetails
                  WHERE     numListItemID = numReturnStatus
                ) AS [ReturnStatusName],
                ( SELECT    vcData
                  FROM      dbo.ListDetails
                  WHERE     numListItemID = numReturnReason
                ) AS [ReturnReasonName],
                ISNULL(con.vcEmail, '') AS vcEmail,
                ISNULL(RH.numBizDocTempID, 0) AS numBizDocTempID,
                case when charItemType='P' then 'Inventory Item' when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory Item' end as ItemType,
                charItemType,ISNULL(I.numCOGsChartAcntId,0) AS ItemCoGs
                ,ISNULL(I.numAssetChartAcntId,0) AS ItemInventoryAsset
                ,ISNULL(I.numIncomeChartAcntId,0) AS ItemIncomeAccount
                ,isnull(i.monAverageCost,'0') as AverageCost
                ,ISNULL(RH.numItemCode,0) AS [numItemCodeForAccount],
                CASE WHEN ISNULL(RH.numOppId,0) <> 0 THEN vcPOppName 
					 ELSE ''
				END AS [Source],ISNULL(I.bitSerialized,0) AS bitSerialized,ISNULL(I.bitLotNo,0) AS bitLotNo,
				SUBSTRING(
	(SELECT ',' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN ' (' + CONVERT(VARCHAR(15),oppI.numQty) + ')' ELSE '' end 
	FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numReturnHeaderID=RH.numReturnHeaderID and oppI.numReturnItemID=RI.numReturnItemID
	ORDER BY vcSerialNo FOR XML PATH('')),2,200000) AS SerialLotNo,@monEstimatedBizDocAmount AS monEstimatedBizDocAmount,ISNULL(RH.numBillPaymentIDRef,0) AS numBillPaymentIDRef,ISNULL(RH.numDepositIDRef,0) AS numDepositIDRef
        FROM    dbo.ReturnHeader RH
                LEFT JOIN dbo.ReturnItems RI ON RH.numReturnHeaderID = RI.numReturnHeaderID
                LEFT JOIN dbo.OpportunityMaster OM ON OM.numOppID = RH.numOppID AND RH.numDomainID = OM.numDomainID 
                LEFT JOIN Item I ON I.numItemCode = RI.numItemCode
                LEFT JOIN divisionMaster D ON RH.numDivisionID = D.numDivisionID
                LEFT JOIN CompanyInfo C2 ON C2.numCompanyID = D.numCompanyID
                LEFT JOIN dbo.OpportunityAddress OA ON RH.numReturnHeaderID = OA.numReturnHeaderID
                LEFT JOIN AdditionalContactsInformation con ON con.numdivisionId = RH.numdivisionId
                                                               AND con.numContactid = RH.numContactId
                LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = RI.numWareHouseItemID
				LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
	    WHERE  RH.numDomainID = @numDomainId
                AND RH.numReturnHeaderID = @numReturnHeaderID
		
    END

GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetIncomeExpenseStatementNew_Kamal' ) 
    DROP PROCEDURE USP_GetIncomeExpenseStatementNew_Kamal
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE USP_GetIncomeExpenseStatementNew_Kamal
@numDomainId AS NUMERIC(9),                                          
@dtFromDate AS DATETIME = NULL,                                        
@dtToDate AS DATETIME = NULL,
@ClientTimeZoneOffset INT, --Added by Chintan to enable calculation of date according to client machine   
@numAccountClass AS NUMERIC(9)=0,
@DateFilter AS VARCHAR(20),
@ReportColumn AS VARCHAR(20)

AS                                                          
BEGIN 


IF @DateFilter = 'CurYear'
BEGIN
	select @dtFromDate = DATEADD(yy,DATEDIFF(yy,0,GETDATE()),0),
	   @dtToDate = DATEADD(ms,-3,DATEADD(yy,0,DATEADD(yy,DATEDIFF(yy,0,GETDATE())+1,0)));   
END
ELSE IF @DateFilter = 'PreYear'
BEGIN
	select @dtFromDate = DATEADD(yy,-1,DATEADD(yy,DATEDIFF(yy,0,GETDATE()),0)),
	   @dtToDate = DATEADD(ms,-3,DATEADD(yy,0,DATEADD(yy,DATEDIFF(yy,0,GETDATE()),0)));  
END
ELSE IF @DateFilter = 'Pre2Year'
BEGIN
select @dtFromDate = DATEADD(yy,-2,DATEADD(yy,DATEDIFF(yy,0,GETDATE()),0)),
       @dtToDate = DATEADD(ms,-3,DATEADD(yy,0,DATEADD(yy,DATEDIFF(yy,0,GETDATE()),0))); 
END
ELSE IF @DateFilter = 'CurPreYear'
BEGIN
	select @dtFromDate = DATEADD(yy,-1,DATEADD(yy,DATEDIFF(yy,0,GETDATE()),0)),
	   @dtToDate = DATEADD(ms,-3,DATEADD(yy,0,DATEADD(yy,DATEDIFF(yy,0,GETDATE())+1,0)));
END
ELSE IF @DateFilter = 'CurPre2Year'
BEGIN
	select @dtFromDate = DATEADD(yy,-2,DATEADD(yy,DATEDIFF(yy,0,GETDATE()),0)),
       @dtToDate = DATEADD(ms,-3,DATEADD(yy,0,DATEADD(yy,DATEDIFF(yy,0,GETDATE())+1,0)));  
END
ELSE IF @DateFilter = 'CuQur'
BEGIN
	SELECT @dtFromDate = Dateadd(qq, Datediff(qq,0,GetDate()), 0),
		@dtToDate = Dateadd(ms,-3,Dateadd(qq, Datediff(qq,0,GetDate())+1, 0))
END
ELSE IF @DateFilter = 'CurPreQur'
BEGIN
	SELECT @dtFromDate = Dateadd(qq, Datediff(qq,0,GetDate())-1, 0),
		@dtToDate = Dateadd(ms,-3,Dateadd(qq, Datediff(qq,0,GetDate())+1, 0))
END
ELSE IF @DateFilter = 'PreQur'
BEGIN
	SELECT @dtFromDate = Dateadd(qq, Datediff(qq,0,GetDate())-1, 0),
		@dtToDate = Dateadd(ms,-3,Dateadd(qq, Datediff(qq,0,GetDate()), 0))
END
ELSE IF @DateFilter = 'LastMonth'
BEGIN
	select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
			@dtToDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)));  
END
ELSE IF @DateFilter = 'ThisMonth'
BEGIN
	select @dtFromDate = DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0),
			@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0)));  
END
ELSE IF @DateFilter = 'CurPreMonth'
BEGIN
	select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
			@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0))); 
END
 

--SET @dtFromDate = DATEADD(minute, @ClientTimeZoneOffset, @dtFromDate);  
--SET @dtToDate = DATEADD(minute, @ClientTimeZoneOffset, @dtToDate);  

;WITH DirectReport (ParentId, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,Struc)
AS
(
  -- anchor
  SELECT CAST(-1 AS NUMERIC(18)) AS ParentId, [ATD].[numAccountTypeID], [ATD].[vcAccountType],[ATD].[vcAccountCode],
   1 AS LEVEL, cast(cast([ATD].[vcAccountCode] AS varchar) AS varchar (100))  AS Struc
  FROM [dbo].[AccountTypeDetail] AS ATD
  WHERE [ATD].[numDomainID]=@numDomainId AND  [ATD].[vcAccountCode] IN('0103','0104','0106')
  UNION ALL
  -- recursive
  SELECT ATD.[numParentID] AS ParentId, [ATD].[numAccountTypeID], [ATD].[vcAccountType],[ATD].[vcAccountCode],
   LEVEL + 1, cast(d.Struc + '#' + cast([ATD].[vcAccountCode] AS varchar) AS varchar(100))  AS Struc
  FROM [dbo].[AccountTypeDetail] AS ATD JOIN [DirectReport] D ON D.[numAccountTypeID] = [ATD].[numParentID]
  WHERE [ATD].[numDomainID]=@numDomainId 
  ),
  DirectReport1 (ParentId, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,numAccountId,Struc)
AS
(
  -- anchor
  SELECT ParentId, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL ,CAST(NULL AS NUMERIC(18)),Struc
  FROM DirectReport 
  UNION ALL
  -- recursive
  SELECT COA.numParntAcntTypeId AS ParentId,CAST(NULL AS NUMERIC(18)), COA.[vcAccountName],COA.[vcAccountCode],
   LEVEL + 1,[COA].[numAccountId], cast(d.Struc + '#' + cast(COA.[vcAccountCode] AS varchar) AS varchar(100))  AS Struc
  FROM [dbo].[Chart_of_Accounts] AS COA JOIN [DirectReport1] D ON D.[numAccountTypeID] = COA.[numParntAcntTypeId]
  WHERE [COA].[numDomainID]=@numDomainId AND COA.bitActive = 1
  )

  
SELECT ParentId, numAccountTypeID, vcAccountType, LEVEL, vcAccountCode,numAccountId,Struc INTO #tempDirectReport
FROM DirectReport1 

INSERT INTO #tempDirectReport
SELECT 0,-1,'Ordinary Income/Expense',0,NULL,NULL,'-1'
UNION ALL
SELECT 0,-2,'Other Income and Expenses',0,NULL,NULL,'-2'

UPDATE #tempDirectReport SET Struc='-1#' + Struc WHERE [vcAccountCode] NOT LIKE '010302%' AND 
[vcAccountCode] NOT LIKE '010402%' 

UPDATE #tempDirectReport SET Struc='-2#' + Struc WHERE [vcAccountCode] LIKE '010302%' OR 
[vcAccountCode] LIKE '010402%'

UPDATE #tempDirectReport SET [ParentId]=-2 WHERE [vcAccountCode] IN('010302','010402')


Select COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId, COA.Struc,
CASE WHEN COA.vcAccountCode LIKE '010301%' OR COA.vcAccountCode LIKE '010302%' THEN (ISNULL(Credit,0) - ISNULL(Debit,0))
     ELSE (ISNULL(Debit,0) - ISNULL(Credit,0)) END AS Amount,V.datEntry_Date
INTO #tempViewData
FROM #tempDirectReport COA JOIN View_Journal V ON  /*V.[numAccountId] = COA.[numAccountId]*/ V.COAvcAccountCode like COA.vcAccountCode + '%' 
AND V.numDomainID=@numDomainId AND datEntry_Date BETWEEN  @dtFromDate AND @dtToDate
AND (V.numAccountClass=@numAccountClass OR @numAccountClass=0) 
WHERE COA.[numAccountId] IS NOT NULL

DECLARE @columns VARCHAR(8000);--SET @columns = '';
DECLARE @SUMColumns VARCHAR(8000);--SET @SUMColumns = '';
DECLARE @PivotColumns VARCHAR(8000);--SET @PivotColumns = '';
DECLARE @Select VARCHAR(8000)
DECLARE @Where VARCHAR(8000)
SET @Select = 'SELECT ParentId, numAccountTypeID, vcAccountType, LEVEL, vcAccountCode, numAccountId,Struc'

IF @ReportColumn = 'Year' OR @ReportColumn = 'Quarter'
BEGIN
; WITH CTE AS (
	SELECT
		YEAR(@dtFromDate) AS 'yr',
		MONTH(@dtFromDate) AS 'mm',
		DATENAME(mm, @dtFromDate) AS 'mon',
		DATEPART(d,@dtFromDate) AS 'dd',
		DATEPART(qq,@dtFromDate) AS 'qq',
		@dtFromDate 'new_date'
	UNION ALL
	SELECT
		YEAR(new_date) AS 'yr',
		MONTH(new_date) AS 'mm',
		DATENAME(mm, new_date) AS 'mon',
		DATEPART(d,@dtFromDate) AS 'dd',
		DATEPART(qq,new_date) AS 'qq',
		DATEADD(d,1,new_date) 'new_date'
	FROM CTE
	WHERE new_date < @dtToDate
	)
	
SELECT yr AS 'Year1',qq AS 'Quarter1', mm as 'Month1', mon AS 'MonthName1', count(dd) AS 'Days1' into #tempYearMonth
FROM CTE
GROUP BY mon, yr, mm, qq
ORDER BY yr, mm, qq
OPTION (MAXRECURSION 5000)

	IF @ReportColumn = 'Year'
		BEGIN
		SELECT
			@columns = COALESCE(@columns + ',ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
			',ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']'),
			@SUMColumns = COALESCE(@SUMColumns + '+ ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)',
			',ISNULL([' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)'),
			@PivotColumns = COALESCE(@PivotColumns + ',[' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
			'[' + cast(monthname1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
		FROM #tempYearMonth ORDER BY Year1,MONTH1

		SET @SUMColumns = @SUMColumns + ' AS Total, (CASE WHEN ISNULL(numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type]'
		SET @Where = '	FROM (Select COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,
				SUM(Case 
					When (COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2)
					THEN (Case WHEN V.vcAccountCode LIKE ''0104%'' OR V.vcAccountCode LIKE ''0106%'' THEN Amount * -1
					ELSE Amount END)
 				ELSE Amount END) AS Amount,
				DATENAME(mm,datEntry_Date) + '' '' + CAST(YEAR(datEntry_Date) AS VARCHAR(4)) AS MonthYear 
				FROM #tempDirectReport COA LEFT OUTER JOIN #tempViewData V ON  
				V.Struc like COA.Struc + ''%'' 
				GROUP BY YEAR(datEntry_Date),MONTH(datEntry_Date),DATENAME(mm,datEntry_Date),COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc) AS t
				PIVOT
				(
				  SUM(Amount)
				  FOR [MonthYear] IN( ' + @PivotColumns +  ')
				) AS p ORDER BY Struc'

	END
	Else IF @ReportColumn = 'Quarter'
		BEGIN
		SELECT
		    @columns = COALESCE(@columns + ',ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
			',ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0) AS [Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']'),
			@SUMColumns = COALESCE(@SUMColumns + '+ ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)',
			',ISNULL([Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + '],0)'),
			@PivotColumns = COALESCE(@PivotColumns + ',[Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']',
			'[Q' + cast(Quarter1 as varchar) + ' ' + cast(Year1 as varchar) + ']')
		FROM (SELECT DISTINCT Year1,Quarter1  FROM #tempYearMonth)T
		ORDER BY Year1,Quarter1

		SET @SUMColumns = @SUMColumns + ' AS Total, (CASE WHEN ISNULL(numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type]'
		SET @Where = '	FROM (Select COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,
				SUM(Case 
						When(COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2) 
						THEN (Case WHEN V.vcAccountCode LIKE ''0104%'' OR V.vcAccountCode LIKE ''0106%'' THEN Amount * -1
						ELSE Amount END)
					ELSE Amount END) AS Amount,
				''Q'' + cast(DATEPART(qq,datEntry_Date) as varchar) + '' '' + CAST(YEAR(datEntry_Date) AS VARCHAR(4)) AS MonthYear 
				FROM #tempDirectReport COA LEFT OUTER JOIN #tempViewData V ON  
				V.Struc like COA.Struc + ''%'' 
				GROUP BY YEAR(datEntry_Date),DATEPART(qq,datEntry_Date),COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc) AS t
				PIVOT
				(
				  SUM(Amount)
				  FOR [MonthYear] IN( ' + @PivotColumns +  ')
				) AS p ORDER BY Struc'
	END

	DROP TABLE #tempYearMonth
END
ELSE
BEGIN
	SET @columns = ',ISNULL(Amount,0) as Total, (CASE WHEN ISNULL(numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type]';
	SET @SUMColumns = '';
	SET @PivotColumns = '';
	SET @Where = ' FROM (Select COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,
				SUM(Case 
						When (COA.numAccountTypeID = -1 OR COA.numAccountTypeID = -2)
						THEN (Case WHEN V.vcAccountCode LIKE ''0104%'' OR V.vcAccountCode LIKE ''0106%'' THEN Amount * -1
						ELSE Amount END)
					ELSE Amount END) AS Amount
				FROM #tempDirectReport COA LEFT OUTER JOIN #tempViewData V ON  
				V.Struc like COA.Struc + ''%'' 
				GROUP BY COA.ParentId, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc) AS t
				ORDER BY Struc'
END

PRINT @Select
PRINT @columns
PRINT @SUMColumns
PRINT @Where


EXECUTE (@Select + @columns + @SUMColumns  + ' ' + @Where)

DROP TABLE #tempViewData 
DROP TABLE #tempDirectReport

END
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetNewOppItemsForReturn')
DROP PROCEDURE USP_GetNewOppItemsForReturn
GO
CREATE PROCEDURE [dbo].[USP_GetNewOppItemsForReturn]      
@tintOppType as tinyint,      
@numDomainID as numeric(9),      
@numDivisionID as numeric(9)=0,      
@str as varchar(20) ,
@numUserCntID as numeric(9),
@tintSearchOrderCustomerHistory as tinyint=0     
as   

--IF @tintOppType=0
--BEGIN
--	SELECT '' AS NoValue
--	SELECT '' AS NoValue
--	RETURN 
--END

 select * into #Temp1 from (select numFieldId,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,tintRow AS tintOrder,0 as Custom
    from View_DynamicColumns
   where numFormId=22 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
        AND bitCustom=0 AND ISNULL(bitSettingField,0)=1 AND numRelCntType=0
union   
 select numFieldId,vcFieldName,vcFieldName,tintRow AS tintOrder,1 as Custom
    from View_DynamicCustomColumns  
   where Grp_id=5 AND numFormId=22 and numUserCntID=@numUserCntID and numDomainID=@numDomainID 
   AND tintPageType=1 AND bitCustom=1 AND numRelCntType=0)X 
  
  if not exists(select * from #Temp1)
  begin
    insert into #Temp1
     select numFieldId,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,tintorder,0 from View_DynamicDefaultColumns                                                  
     where numFormId=22 and bitDefault = 1 and ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID 
  end

Create table #tempItemCode (numItemCode numeric(9))

if @tintOppType>0       
begin 
DECLARE @bitRemoveVendorPOValidation AS BIT

SELECT @bitRemoveVendorPOValidation=ISNULL(bitRemoveVendorPOValidation,0) FROM domain WHERE numDomainID=@numDomainID

Declare @strSQL as varchar(8000)
declare @tintOrder as int
declare @Fld_id as varchar(20)
declare @Fld_Name as varchar(20)
set @strSQL=''
	select top 1 @tintOrder=tintOrder+1,@Fld_id=numFieldId,@Fld_Name=vcFieldName from #Temp1 where Custom=1 order by tintOrder
	while @tintOrder>0
    begin
	set @strSQL=@strSQL+', dbo.GetCustFldValueItem('+@Fld_id+', I.numItemCode) as ['+ @Fld_Name+']'

	select top 1 @tintOrder=tintOrder+1,@Fld_id=numFieldId,@Fld_Name=vcFieldName from #Temp1 where Custom=1 and tintOrder>=@tintOrder order by tintOrder
	if @@rowcount=0 set @tintOrder=0
	end


--Temp table for Item Search Configuration

 select * into #tempSearch from (select numFieldId,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,tintRow AS tintOrder,0 as Custom
    from View_DynamicColumns
   where numFormId=22 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
        AND bitCustom=0  AND ISNULL(bitSettingField,0)=1 AND numRelCntType=1
union   
  select numFieldId,vcFieldName,vcFieldName,tintRow AS tintOrder,1 as Custom
    from View_DynamicCustomColumns   
   where Grp_id=5 AND numFormId=22 and numUserCntID=@numUserCntID and numDomainID=@numDomainID 
   AND tintPageType=1 AND bitCustom=1 AND numRelCntType=1)X 
  
  if not exists(select * from #tempSearch)
  begin
    insert into #tempSearch
    select numFieldId,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,tintorder,0 from View_DynamicDefaultColumns                                                  
     where numFormId=22 and bitDefault = 1 and ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID 
  END

--Regular Search
Declare @strSearch as varchar(8000),@CustomSearch AS VARCHAR(4000),@numFieldId AS NUMERIC(9)
SET @strSearch=''
	select top 1 @tintOrder=tintOrder+1,@Fld_Name=vcDbColumnName,@numFieldId=numFieldId from #tempSearch WHERE Custom=0 order by tintOrder
	while @tintOrder>-1
    BEGIN
	IF @Fld_Name='vcPartNo'
		insert into #tempItemCode SELECT distinct Item.numItemCode FROM Vendor join Item on Item.numItemCode=Vendor.numItemCode 
										where Item.numDomainID=@numDomainID and Vendor.numDomainID=@numDomainID 
										and Vendor.vcPartNo is not null and len(Vendor.vcPartNo)>0 and vcPartNo LIKE '%'+@str+'%'

-- 		set @strSearch=@strSearch+ ' I.numItemCode in (SELECT distinct Item.numItemCode FROM Vendor join Item on Item.numItemCode=Vendor.numItemCode 
--										where Item.numDomainID='+convert(varchar(15),@numDomainID) + ' and Vendor.numDomainID='+convert(varchar(15),@numDomainID) + ' 
--										and Vendor.vcPartNo is not null and len(Vendor.vcPartNo)>0 and vcPartNo LIKE ''%'+@str+'%'')'
	ELSE IF @Fld_Name='vcBarCode'
		insert into #tempItemCode SELECT distinct Item.numItemCode FROM Item join WareHouseItems WI on WI.numDomainID=@numDomainID and Item.numItemCode=WI.numItemID join Warehouses W on W.numDomainID=@numDomainID and W.numWareHouseID=WI.numWareHouseID  
									 where Item.numDomainID=@numDomainID and isnull(Item.numItemGroup,0)>0 and WI.vcBarCode is not null and len(WI.vcBarCode)>0 and WI.vcBarCode LIKE '%'+@str+'%'

-- 		set @strSearch=@strSearch+ ' I.numItemCode in (SELECT distinct Item.numItemCode FROM Item join WareHouseItems WI on WI.numDomainID='+convert(varchar(15),@numDomainID) + ' and Item.numItemCode=WI.numItemID join Warehouses W on W.numDomainID='+convert(varchar(15),@numDomainID) + ' and W.numWareHouseID=WI.numWareHouseID  
--									 where Item.numDomainID='+convert(varchar(15),@numDomainID) + ' and isnull(Item.numItemGroup,0)>0 and WI.vcBarCode is not null and len(WI.vcBarCode)>0 and WI.vcBarCode LIKE ''%'+@str+'%'')'
	ELSE IF @Fld_Name='vcWHSKU'
		insert into #tempItemCode SELECT distinct Item.numItemCode FROM Item join WareHouseItems WI on WI.numDomainID=@numDomainID and Item.numItemCode=WI.numItemID join Warehouses W on W.numDomainID=@numDomainID and W.numWareHouseID=WI.numWareHouseID  
									 where Item.numDomainID=@numDomainID and isnull(Item.numItemGroup,0)>0 and WI.vcWHSKU is not null and len(WI.vcWHSKU)>0 and WI.vcWHSKU LIKE '%'+@str+'%'

-- 		set @strSearch=@strSearch+ ' I.numItemCode in (SELECT distinct Item.numItemCode FROM Item join WareHouseItems WI on WI.numDomainID='+convert(varchar(15),@numDomainID) + ' and Item.numItemCode=WI.numItemID join Warehouses W on W.numDomainID='+convert(varchar(15),@numDomainID) + ' and W.numWareHouseID=WI.numWareHouseID  
--									 where Item.numDomainID='+convert(varchar(15),@numDomainID) + ' and isnull(Item.numItemGroup,0)>0 and WI.vcWHSKU is not null and len(WI.vcWHSKU)>0 and WI.vcWHSKU LIKE ''%'+@str+'%'')'
	else    
		set @strSearch=@strSearch+' ['+ @Fld_Name + '] LIKE ''%'+@str+'%'''

	select top 1 @tintOrder=tintOrder+1,@Fld_Name=vcDbColumnName,@numFieldId=numFieldId from #tempSearch where tintOrder>=@tintOrder AND Custom=0 order by tintOrder
		if @@rowcount=0 
		begin
			set @tintOrder=-1
		END
		ELSE IF @Fld_Name!='vcPartNo' and @Fld_Name!='vcBarCode' and @Fld_Name!='vcWHSKU'
		begin
			set @strSearch=@strSearch + ' or '
		END
	END

IF (select count(*) from #tempItemCode)>0
	set @strSearch=@strSearch + ' or  I.numItemCode in (select numItemCode from #tempItemCode)' 
--Custom Search
SET @CustomSearch=''
select top 1 @tintOrder=tintOrder+1,@Fld_Name=vcDbColumnName,@numFieldId=numFieldId from #tempSearch WHERE Custom=1 order by tintOrder
	while @tintOrder>-1
    BEGIN
    
	set @CustomSearch=@CustomSearch + CAST(@numFieldId AS VARCHAR(10)) 

	select top 1 @tintOrder=tintOrder+1,@Fld_Name=vcDbColumnName,@numFieldId=numFieldId from #tempSearch where tintOrder>=@tintOrder AND Custom=1 order by tintOrder
		if @@rowcount=0 
		begin
			set @tintOrder=-1
		END
		ELSE
		begin
			set @CustomSearch=@CustomSearch + ' , '
		END
	END
	
IF LEN(@CustomSearch)>0
BEGIN
 	SET @CustomSearch= ' I.numItemCode in (SELECT RecId FROM dbo.CFW_FLD_Values_Item WHERE Fld_ID IN (' + @CustomSearch  + ') and Fld_Value like ''%' + @str + '%'')'

IF LEN(@strSearch)>0
       SET @strSearch=@strSearch + ' OR ' +  @CustomSearch
ELSE
       SET @strSearch=  @CustomSearch  
END


if @tintOppType=1 OR (@bitRemoveVendorPOValidation=1 AND @tintOppType=2)       
begin      
  set @strSQL='select TOP 20 I.numItemCode,isnull(vcItemName,'''') vcItemName,CASE WHEN I.[charItemType]=''P'' AND ISNULL(I.bitSerialized,0) = 0 AND  ISNULL(I.bitLotNo,0) = 0 THEN convert(varchar(100),ISNULL((SELECT TOP 1 ROUND(monWListPrice,2) FROM [WareHouseItems] WHERE [numItemID]=I.numItemCode),0)) ELSE CONVERT(VARCHAR(100), ROUND(monListPrice, 2)) END AS monListPrice,isnull(vcSKU,'''') vcSKU,isnull(numBarCodeId,0) numBarCodeId,isnull(vcModelID,'''') vcModelID,isnull(txtItemDesc,'''') as txtItemDesc,isnull(C.vcCompanyName,'''') as vcCompanyName'+@strSQL+' from Item  I     
  Left join DivisionMaster D              
  on I.numVendorID=D.numDivisionID  
  left join CompanyInfo C  
  on C.numCompanyID=D.numCompanyID  
  where ISNULL(I.bitSerialized,0) = 0 AND ISNULL(I.bitLotNo,0) = 0 AND ISNULL(I.Isarchieve,0) <> 1 And I.charItemType NOT IN(''A'') AND  I.numDomainID='+convert(varchar(20),@numDomainID)+' and (' +@strSearch+ ') '         
--- added Asset validation  by sojan

IF @bitRemoveVendorPOValidation=1 AND @tintOppType=2
	set @strSQL=@strSQL + ' and ISNULL(I.bitKitParent,0) = 0 '

if @tintSearchOrderCustomerHistory=1 and @numDivisionID>0
BEGIN
	set @strSQL=@strSQL + ' and I.numItemCode in (select distinct oppI.numItemCode from 
							OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='+convert(varchar(20),@numDomainID)+' and oppM.numDivisionId='+convert(varchar(20),@numDivisionId)+')'
END

set @strSQL=@strSQL + ' order by  vcItemName'
 print @strSQL
 exec (@strSQL)
end      
else if @tintOppType=2       
begin      
 set @strSQL='SELECT TOP 20 I.numItemCode,isnull(vcItemName,'''') vcItemName,convert(varchar(200),round(monListPrice,2)) as monListPrice,isnull(vcSKU,'''') vcSKU,isnull(numBarCodeId,0) numBarCodeId,isnull(vcModelID,'''') vcModelID,isnull(txtItemDesc,'''') as txtItemDesc,isnull(C.vcCompanyName,'''') as vcCompanyName'+@strSQL+'  from item I              
 join Vendor V              
 on V.numItemCode=I.numItemCode   
 Left join DivisionMaster D              
 on V.numVendorID=D.numDivisionID  
 left join CompanyInfo C  
 on C.numCompanyID=D.numCompanyID             
 where ISNULL(I.bitSerialized,0) = 0 AND ISNULL(I.bitLotNo,0) = 0 AND ISNULL(I.Isarchieve,0) <> 1 and ISNULL(I.bitKitParent,0) = 0 And I.charItemType NOT IN(''A'') AND V.numVendorID='+convert(varchar(20),@numDivisionID)+' and (' +@strSearch+ ') ' 
--- added Asset validation  by sojan

if @tintSearchOrderCustomerHistory=1 and @numDivisionID>0
BEGIN
	set @strSQL=@strSQL + ' and I.numItemCode in (select distinct oppI.numItemCode from 
							OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='+convert(varchar(20),@numDomainID)+' and oppM.numDivisionId='+convert(varchar(20),@numDivisionId)+')'
END

set @strSQL=@strSQL + ' order by  vcItemName'
 print @strSQL
 exec (@strSQL)
end
else
select 0  
end
ELSE
	select 0  

select * from  #Temp1 where vcDbColumnName not in ('vcPartNo','vcBarCode','vcWHSKU')ORDER BY tintOrder 

drop table #Temp1
         
drop table #tempItemCode
  
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

--created by anoop jayaraj   
--EXEC [USP_GetPageNavigation] @numModuleID=9,@numDomainID=1

         
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getpagenavigation')
DROP PROCEDURE usp_getpagenavigation
GO
CREATE PROCEDURE [dbo].[USP_GetPageNavigation]            
@numModuleID as numeric(9),      
@numDomainID as numeric(9),
@numGroupID NUMERIC(18, 0)        
as            
if @numModuleID = 14      
begin
           
select  PND.numPageNavID,
        numModuleID,
        numParentID,
        vcPageNavName,
        isnull(vcNavURL, '') as vcNavURL,
        vcImageURL,
        0 intSortOrder
from    PageNavigationDTL PND
        JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
 WHERE  1 = 1
        AND ( ISNULL(TNA.bitVisible, 0) = 1
              --OR ISNULL(numParentID, 0) = 0
              --OR ISNULL(TNA.bitVisible, 0) = 0
            )
        AND ISNULL(PND.bitVisible, 0) = 1
        AND numModuleID = 14
        AND TNA.numDomainID = @numDomainID
        AND numGroupID = @numGroupID
union
select  1111 numPageNavID,
        14 numModuleID,
        133 numParentID,
        'Regular Documents' as vcPageNavName,
        '../Documents/frmRegularDocList.aspx?Category=0',
        '../images/tf_note.gif' vcImageURL,
        1
union

--select numListItemId,9 numModuleID,1111 numParentID,vcData vcPageNavName,'../Documents/frmDocList.aspx?TabID=1&Category=' + cast(numListItemId as varchar(10)) vcImageURL ,'../images/tf_note.gif',-2 vcImageURL
-- from ListDetails where numListid=29 and constFlag=1 
--UNION
--
--select numListItemId,9 numModuleID,1111 numParentID,vcData vcPageNavName,'../Documents/frmDocList.aspx?TabID=1&Category=' + cast(numListItemId as varchar(10)) vcImageURL ,'../images/tf_note.gif',-2 vcImageURL
-- from ListDetails where numListid=29 and numDomainID=@numDomainID
-- 
SELECT  Ld.numListItemId,
        14 numModuleID,
        1111 numParentID,
        vcData vcPageNavName,
        '../Documents/frmRegularDocList.aspx?Category='
        + cast(Ld.numListItemId as varchar(10)) vcImageURL,
        '../images/tf_note.gif',
        ISNULL(intSortOrder, LD.sintOrder) SortOrder
FROM    listdetails Ld
        left join listorder LO on Ld.numListItemID = LO.numListItemID
                                  and Lo.numDomainId = @numDomainID
WHERE   Ld.numListID = 29
        and ( constFlag = 1
              or Ld.numDomainID = @numDomainID
            )
--order by ISNULL(intSortOrder,LD.sintOrder)
--union
--   
-- SELECT 0 numPageNavID,9 numModuleID,1111 numParentID, vcData vcPageNavName,'../Documents/frmDocList.aspx?TabID=1&Category='+convert(varchar(10), LD.numListItemID) vcNavURL      
-- ,'../images/Text.gif' vcImageURL,0      
-- FROM listdetails Ld   
-- left join listorder LO on Ld.numListItemID= LO.numListItemID  and lo.numDomainId = @numDomainId        
-- WHERE  LD.numListID=28 and (constFlag=1 or  LD.numDomainID=@numDomainID)    
union
SELECT  ld.numListItemID AS numPageNavID,
        14 numModuleID,
        133 numParentID,
        vcData vcPageNavName,
        '../Documents/frmDocList.aspx?Category='
        + convert(varchar(10), LD.numListItemID) vcNavURL,
        '../images/tf_note.gif' vcImageURL,
        -3
FROM    listdetails Ld
        left join listorder LO on Ld.numListItemID = LO.numListItemID
                                  and lo.numDomainId = @numDomainId
        inner join AuthoritativeBizDocs AB on ( LD.numListItemID = AB.numAuthoritativeSales )
WHERE   LD.numListID = 27
        and ( constFlag = 1
              or LD.numDomainID = @numDomainID
            )
        and AB.numDomainID = @numDomainID
union
SELECT  ld.numListItemID AS numPageNavID,
        14 numModuleID,
        133 numParentID,
        vcData vcPageNavName,
        '../Documents/frmDocList.aspx?Category='
        + convert(varchar(10), LD.numListItemID) vcNavURL,
        '../images/tf_note.gif' vcImageURL,
        -3
FROM    listdetails Ld
        left join listorder LO on Ld.numListItemID = LO.numListItemID
                                  and lo.numDomainId = @numDomainId
        inner join AuthoritativeBizDocs AB on ( ld.numListItemID = AB.numAuthoritativePurchase )
WHERE   LD.numListID = 27
        and ( constFlag = 1
              or LD.numDomainID = @numDomainID
            )
        and AB.numDomainID = @numDomainID
UNION
select  0 numPageNavID,
        14 numModuleID,
        LD.numListItemID AS numParentID,
        LT.vcData as vcPageNavName,
        '../Documents/frmDocList.aspx?Category='
        + convert(varchar(10), Ld.numListItemID) + '&Status='
        + convert(varchar(10), LT.numListItemID) vcNavURL,
        '../images/Text.gif' vcImageURL,
        0
from    ListDetails LT,
        listdetails ld
where   LT.numListID = 11
        and Lt.numDomainID = @numDomainID
        AND LD.numListID = 27
        AND ( ld.constFlag = 1
              or LD.numDomainID = @numDomainID
            )
        and ( LD.numListItemID in ( select  AB.numAuthoritativeSales
                                    from    AuthoritativeBizDocs AB
                                    where   AB.numDomainId = @numDomainID )
              or ld.numListItemID in ( select   AB.numAuthoritativePurchase
                                       from     AuthoritativeBizDocs AB
                                       where    AB.numDomainId = @numDomainID )
            )


--SELECT * FROM AuthoritativeBizDocs where numDomainId=72

 
--UNION
--	SELECT 0 numPageNavID,9 numModuleID,133 numParentID, 'BizDoc Approval Rule' as  vcPageNavName,'../Documents/frmBizDocAppRule.aspx' as vcNavURL      
-- ,'../images/tf_note.gif' vcImageURL,0 
--
--UNION
--	SELECT 0 numPageNavID,9 numModuleID,133 numParentID, 'Order Auto Rule' as  vcPageNavName,'../Documents/frmOrderAutoRules.aspx' as vcNavURL      
-- ,'../images/tf_note.gif' vcImageURL,1  
union
select  2222 numPageNavID,
        14 numModuleID,
        133 numParentID,
        'Other BizDocs' as vcPageNavName,
        '',
        '../images/tf_note.gif' vcImageURL,
        -1
union
SELECT  ld.numListItemID AS numPageNavID,
        14 numModuleID,
        2222 numParentID,
        vcData vcPageNavName,
        '../Documents/frmDocList.aspx?Category='
        + convert(varchar(10), LD.numListItemID) vcNavURL,
        '../images/Text.gif' vcImageURL,
        -1
FROM    listdetails Ld
where   ld.numListID = 27
        and ( constFlag = 1
              or LD.numDomainID = @numDomainID
            )
        and LD.numListItemID not in ( select    AB.numAuthoritativeSales
                                      from      AuthoritativeBizDocs AB
                                      where     AB.numDomainID = @numDomainID )
        and LD.numListItemID not in ( select    AB.numAuthoritativePurchase
                                      from      AuthoritativeBizDocs AB
                                      where     AB.numDomainID = @numDomainID )
union
SELECT  ls.numListItemID AS numPageNavID,
        14 numModuleID,
        ld.numListItemID as numParentID,
        ls.vcData vcPageNavName,
        '../Documents/frmDocList.aspx?Category='
        + convert(varchar(10), LD.numListItemID) vcNavURL,
        null vcImageURL,
        0
FROM    listdetails Ld,
        ( select    *
          from      ListDetails LS
          where     LS.numDomainID = @numDomainID
                    and LS.numListID = 11
        ) LS
where   ld.numListID = 27
        and ( ld.constFlag = 1
              or LD.numDomainID = @numDomainID
            )
        and LD.numListItemID not in ( select    AB.numAuthoritativeSales
                                      from      AuthoritativeBizDocs AB
                                      where     AB.numDomainID = @numDomainID
                                      union
                                      select    AB.numAuthoritativePurchase
                                      from      AuthoritativeBizDocs AB
                                      where     AB.numDomainID = @numDomainID )
order by numParentID,
        intSortOrder,
        numPageNavID

END
ELSE IF @numModuleID = 35 
BEGIN
	select PND.numPageNavID,numModuleID,numParentID,vcPageNavName,isnull(vcNavURL,'') as vcNavURL,vcImageURL ,0 sintOrder      
	from PageNavigationDTL PND
	JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
	WHERE  1 = 1
        AND ( ISNULL(TNA.bitVisible, 0) = 1
              --OR ISNULL(numParentID, 0) = 0
              --OR ISNULL(TNA.bitVisible, 0) = 0
            ) 
        AND ISNULL(PND.bitVisible, 0) = 1
	AND numModuleID=@numModuleID --AND bitVisible=1  
	AND TNA.numDomainID = @numDomainID
	AND numGroupID = @numGroupID
	    
	UNION 
	 SELECT numFRID,35,CASE numFinancialViewID WHEN 26770 THEN 96 /*Profit & Loss*/
	WHEN 26769 THEN 176 /*Income & Expense*/
	WHEN 26768 THEN 98 /*Cash Flow Statement*/
	WHEN 26767 THEN 97 /*Balance Sheet*/
	WHEN 26766 THEN 81 /*A/R & A/P Aging*/
	WHEN 26766 THEN 82 /*A/R & A/P Aging*/
	ELSE 0
	END 
	  ,vcReportName,CASE numFinancialViewID 
	WHEN 26770 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =96 AND numModuleID=35)  /*Profit & Loss*/
	WHEN 26769 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =176 AND numModuleID=35) /*Income & Expense*/
	WHEN 26768 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =98 AND numModuleID=35) /*Cash Flow Statement*/
	WHEN 26767 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =97 AND numModuleID=35) /*Balance Sheet*/
	WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =81 AND numModuleID=35) /*A/R & A/P Aging*/
	WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =82 AND numModuleID=35) /*A/R & A/P Aging*/
	ELSE ''
	END + 
	CASE CHARINDEX('?',(CASE numFinancialViewID 
	WHEN 26770 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =96 AND numModuleID=35)  /*Profit & Loss*/
	WHEN 26769 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =176 AND numModuleID=35) /*Income & Expense*/
	WHEN 26768 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =98 AND numModuleID=35) /*Cash Flow Statement*/
	WHEN 26767 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =97 AND numModuleID=35) /*Balance Sheet*/
	WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =81 AND numModuleID=35) /*A/R & A/P Aging*/
	WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =82 AND numModuleID=35) /*A/R & A/P Aging*/
	ELSE ''
	END)) 
	WHEN 0 THEN '?FRID='
	ELSE '&FRID='
	END
	 + CAST(numFRID AS VARCHAR)
	 'url',NULL,0 FROM dbo.FinancialReport WHERE numDomainID=@numDomainID
	order by numParentID,numPageNavID  
END
-------------- ADDED BY MANISH ANJARA ON : 6th April,2012
ELSE IF @numModuleID = -1 AND @numDomainID = -1 
BEGIN 
	SELECT PND.numPageNavID,numModuleID,numParentID,vcPageNavName,isnull(vcNavURL,'') as vcNavURL,vcImageURL ,0 sintOrder      
	FROM PageNavigationDTL PND
	JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
	WHERE  1 = 1
        AND ( ISNULL(TNA.bitVisible, 0) = 1
              --OR ISNULL(numParentID, 0) = 0
              --OR ISNULL(TNA.bitVisible, 0) = 0
            )
        AND ISNULL(PND.bitVisible, 0) = 1
	--AND TNA.numDomainID = @numDomainID
	AND numGroupID = @numGroupID
	--WHERE bitVisible=1  
	ORDER BY numParentID,numPageNavID 
END
--------------------------------------------------------- 
-------------- ADDED BY MANISH ANJARA ON : 12th Oct,2012
ELSE IF @numModuleID = 10 
BEGIN 
	DECLARE @numParentID AS NUMERIC(18,0)
	SELECT TOP 1 @numParentID = numParentID FROM dbo.PageNavigationDTL WHERE numModuleID = 10
	PRINT @numParentID

	SELECT * FROM (
	SELECT PND.numPageNavID,numModuleID,numParentID,vcPageNavName,isnull(vcNavURL,'') as vcNavURL,vcImageURL ,0 sintOrder      
	FROM PageNavigationDTL PND
	JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
	WHERE  1 = 1
        AND ( ISNULL(TNA.bitVisible, 0) = 1
              --OR ISNULL(numParentID, 0) = 0
              --OR ISNULL(TNA.bitVisible, 0) = 0
            )
        AND ISNULL(PND.bitVisible, 0) = 1
	AND numModuleID = @numModuleID
	AND TNA.numDomainID = @numDomainID
	AND numGroupID = @numGroupID
	
	UNION ALL

	SELECT  numListItemID AS numPageNavID,
		    (SELECT numModuleID FROM ModuleMaster WHERE vcModuleName = 'Opportunities') AS [numModuleID],
			@numParentID AS numParentID,
			ISNULL(vcData,'') + 's' [vcPageNavName],
			'../Opportunity/frmOpenBizDocs.aspx?BizDocName=' + vcData + '&BizDocID=' + CONVERT(VARCHAR(10),LD.numListItemID) AS [vcNavURL],
			'../images/Text.gif' AS [vcImageURL],
			1 AS [bitVisible] 
	FROM dbo.ListDetails LD
	JOIN dbo.TreeNavigationAuthorization TNA ON LD.numListItemID = TNA.numPageNavID  AND ISNULL(bitVisible,0) = 1  
	WHERE numListID = 27 
	AND ISNULL(TNA.[numDomainID], 0) = @numDomainID
	AND ISNULL(TNA.[numGroupID], 0) = @numGroupID) AS [FINAL]
	ORDER BY numParentID,FINAL.numPageNavID 
END
--------------------------------------------------------- 
--ELSE IF @numModuleID = 13
--	BEGIN
--		select PND.numPageNavID,
--        numModuleID,
--        numParentID,
--        vcPageNavName,
--        isnull(vcNavURL, '') as vcNavURL,
--        vcImageURL,
--        0 sintOrder
-- from   PageNavigationDTL PND
-- WHERE  1 = 1
--        AND ISNULL(PND.bitVisible, 0) = 1
--        AND numModuleID = @numModuleID
-- order by numParentID,
--        numPageNavID 
--	END
--	
--	ELSE IF @numModuleID = 9
--	BEGIN
--			select PND.numPageNavID,
--        numModuleID,
--        numParentID,
--        vcPageNavName,
--        isnull(vcNavURL, '') as vcNavURL,
--        vcImageURL,
--        0 sintOrder
-- from   PageNavigationDTL PND
-- WHERE  1 = 1
--        AND ISNULL(PND.bitVisible, 0) = 1
--        AND numModuleID = @numModuleID
-- order by numParentID,
--        numPageNavID 
--	END

ELSE IF (@numModuleID = 7 OR @numModuleID = 36)
BEGIN
  select    PND.numPageNavID,
            numModuleID,
            numParentID,
            vcPageNavName,
            isnull(vcNavURL, '') as vcNavURL,
            vcImageURL,
            0 sintOrder
  from      PageNavigationDTL PND
  WHERE     1 = 1
            AND ISNULL(PND.bitVisible, 0) = 1
            AND numModuleID = @numModuleID
  order by  numParentID,
            numPageNavID 
END	
ELSE IF @numModuleID = 13
BEGIN
	select PND.numPageNavID,
        PND.numModuleID,
        numParentID,
        vcPageNavName,
        isnull(vcNavURL, '') as vcNavURL,
        vcImageURL,
        ISNULL(PND.intSortOrder,0) sintOrder
	 from   PageNavigationDTL PND
			JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
	 WHERE  1 = 1
			AND ( ISNULL(TNA.bitVisible, 0) = 1
				  --OR ISNULL(numParentID, 0) = 0
				  --OR ISNULL(TNA.bitVisible, 0) = 0
				)
			AND ISNULL(PND.bitVisible, 0) = 1
			AND PND.numModuleID = @numModuleID --AND bitVisible=1  
			AND numGroupID = @numGroupID
	 order by 
	    sintOrder,
		(CASE WHEN (@numModuleID = 13 AND PND.numPageNavID=79) THEN 2000 ELSE 0 END),
		numParentID,
        numPageNavID   
END
else      
begin            
 select PND.numPageNavID,
        PND.numModuleID,
        numParentID,
        vcPageNavName,
        isnull(vcNavURL, '') as vcNavURL,
        vcImageURL,
        0 sintOrder
 from   PageNavigationDTL PND
        JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
 WHERE  1 = 1
        AND ( ISNULL(TNA.bitVisible, 0) = 1
              --OR ISNULL(numParentID, 0) = 0
              --OR ISNULL(TNA.bitVisible, 0) = 0
            )
        AND ISNULL(PND.bitVisible, 0) = 1
        AND PND.numModuleID = @numModuleID --AND bitVisible=1  
        AND numGroupID = @numGroupID
 order by numParentID,
		intSortOrder,
        numPageNavID
		    
END

--select * from TreeNavigationAuthorization where numModuleID=13 
--select * from TreeNavigationAuthorization where numTabID=-1 AND numDomainID = 1 AND numGroupID = 1
--select * from TreeNavigationAuthorization where numListID=11 and numDomainId=72
--select * from AuthoritativeBizDocs where numDomainId=72











/****** Object:  StoredProcedure [dbo].[USP_GetReturns]    Script Date: 01/22/2009 01:39:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetReturns' ) 
    DROP PROCEDURE USP_GetReturns
GO
CREATE PROCEDURE [dbo].[USP_GetReturns]
    @numReturnId NUMERIC(9) = NULL,
    @numDomainId NUMERIC(9) = 0,
    @numReturnStatus NUMERIC(9) = NULL,
    @numReturnReason NUMERIC(9) = NULL,
    @vcOrgSearchText VARCHAR(50) = NULL,
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT,
    @ClientTimeZoneOffset AS INT,
    @tintReturnType AS TINYINT,
	@vcRMASearchText VARCHAR(50) = NULL,
	@vcSortColumn AS VARCHAR(50) = NULL,
    @tintSortDirection AS TINYINT = 1 --1 = ASC, 2 = DESC
AS 
BEGIN
	DECLARE @firstRec AS INTEGER
    DECLARE @lastRec AS INTEGER
    
	SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
    SET @lastRec = ( @CurrentPage * @PageSize + 1 )
       
	CREATE TABLE #TempSalesReturn
	(
		numOppId NUMERIC(18,0),
        numReturnHeaderID NUMERIC(18,0),
        vcRMA VARCHAR(300),
        numReturnStatus NUMERIC(18,0),
        numReturnReason NUMERIC(18,0),
        numDivisionId NUMERIC(18,0),
		dtCreateDate VARCHAR(50),
		dtOriginal DATETIME,
        tintReturnType TINYINT,
		vcCompanyName VARCHAR(300),
		[numAccountID] NUMERIC(18,0),
		reasonforreturn VARCHAR(200),
		[status] VARCHAR(200),
		vcCreditFromAccount VARCHAR(300),
		IsEditable BIT
	)
	
		INSERT INTO 
			#TempSalesReturn
		SELECT
			RH.numoppid,
			RH.numReturnHeaderID,
			vcRMA,
			RH.numReturnStatus,
			RH.numReturnReason,
			RH.numDivisionId,
			CASE WHEN CONVERT(VARCHAR(11), DATEADD(minute, -@ClientTimeZoneOffset, RH.dtCreatedDate)) = CONVERT(VARCHAR(11), GETDATE())
					THEN '<b><font color=red>Today</font></b>'
					WHEN CONVERT(VARCHAR(11), DATEADD(minute, -@ClientTimeZoneOffset, RH.dtCreatedDate)) = CONVERT(VARCHAR(11), DATEADD(day, -1, GETDATE()))
					THEN '<b><font color=purple>YesterDay</font></b>'
					WHEN CONVERT(VARCHAR(11), DATEADD(minute, -@ClientTimeZoneOffset, RH.dtCreatedDate)) = CONVERT(VARCHAR(11), DATEADD(day, 1, GETDATE()))
					THEN '<b><font color=orange>Tommorow</font></b>'
					ELSE dbo.FormatedDateFromDate(RH.dtCreatedDate, RH.[numdomainid])
			END dtCreateDate,
			RH.dtCreatedDate,
			RH.tintReturnType, SUBSTRING(com.vcCompanyName, 0, 30) AS vcCompanyName
			,RH.[numAccountID]
			,dbo.[getlistiemname](RH.numReturnReason) reasonforreturn
			,dbo.[getlistiemname](RH.numReturnStatus) AS [status]
			,COA.vcAccountName AS vcCreditFromAccount,
			CASE dbo.[getlistiemname](numreturnstatus)
				WHEN 'Pending' THEN 'True'
				WHEN 'Confirmed' THEN 'True'
				ELSE 'True'
			END
		FROM    	
			dbo.ReturnHeader AS RH 
		JOIN divisionMaster div ON div.numdivisionid = RH.numdivisionid
		JOIN companyInfo com ON com.numCompanyID = div.numCompanyID
		LEFT JOIN Chart_Of_Accounts COA ON RH.numAccountID = COA.numAccountId AND COA.numDomainId = RH.numDomainId
		WHERE 
			(RH.numReturnReason = @numReturnReason OR @numReturnReason IS NULL OR @numReturnReason = 0)
			AND (RH.numReturnStatus = @numReturnStatus OR @numReturnStatus IS NULL OR @numReturnStatus = 0)
			AND ( RH.numReturnHeaderID = @numReturnId OR @numReturnId IS NULL)
			AND RH.[numdomainid] = @numDomainId
			AND RH.tintReturnType = @tintReturnType
			AND ( ( RH.vcRMA LIKE '%' + @vcRMASearchText+ '%') OR @vcRMASearchText IS NULL OR LEN(@vcRMASearchText)=0) 
			AND (com.vcCompanyName LIKE ('%' + @vcOrgSearchText + '%') OR @vcOrgSearchText IS NULL OR LEN(@vcOrgSearchText)=0)

		--SELECT * FROM  #TempSalesReturn ORDER BY vcCreditFromAccount DESC OFFSET ((1 - 1) * 20) ROWS FETCH NEXT 20 ROWS ONLY
		SELECT  @TotRecs = COUNT(*) FROM #TempSalesReturn
      
		DECLARE @strSQL AS VARCHAR(500) = ''
		DECLARE @vcSort AS VARCHAR(500) = ''

		SET @vcSort = (CASE
						WHEN @vcSortColumn = 'vcCompanyName' THEN
							'vcCompanyName ' + (CASE WHEN @tintSortDirection = 1 THEN 'ASC' ELSE 'DESC' END)
						WHEN @vcSortColumn = 'vcRMA' THEN
							'vcRMA ' + (CASE WHEN @tintSortDirection = 1 THEN 'ASC' ELSE 'DESC' END)
						WHEN @vcSortColumn = 'vcCreditFromAccount' THEN
							'vcCreditFromAccount ' + (CASE WHEN @tintSortDirection = 1 THEN 'ASC' ELSE 'DESC' END)
						WHEN @vcSortColumn = 'ReasonforReturn' THEN
							'reasonforreturn ' + (CASE WHEN @tintSortDirection = 1 THEN 'ASC' ELSE 'DESC' END)
						WHEN @vcSortColumn = 'Status' THEN
							'[status] ' + (CASE WHEN @tintSortDirection = 1 THEN 'ASC' ELSE 'DESC' END)
						ELSE
							'dtOriginal ' + (CASE WHEN @tintSortDirection = 1 THEN 'ASC' ELSE 'DESC' END)
					END) 
	                   
		SET  @strSQL = 'SELECT * FROM (SELECT Row_Number() OVER (ORDER BY ' + @vcSort + ',numReturnHeaderID ASC) AS row,* FROM  #TempSalesReturn) t WHERE row > ' + CAST(@firstRec AS VARCHAR(10)) + ' AND row < ' + CAST(@lastRec AS VARCHAR(10))

		EXEC(@strSQL)
	
		DROP TABLE #TempSalesReturn
END
  
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetTreeNodesForRelationship' ) 
    DROP PROCEDURE USP_GetTreeNodesForRelationship
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- EXEC USP_GetPageNavigationAuthorizationDetails 1,1
CREATE PROCEDURE USP_GetTreeNodesForRelationship
    (
	  @numGroupID NUMERIC(18,0),
	  @numTabID NUMERIC(18,0),
      @numDomainID NUMERIC(18, 0),
	  @bitVisibleOnly bit
    )
AS 
    SET NOCOUNT ON
    SET TRANSACTION ISOLATION LEVEL READ COMMITTED
  
    BEGIN		
        DECLARE @numModuleID AS NUMERIC(18, 0)

        SELECT TOP 1
                @numModuleID = numModuleID
        FROM    dbo.PageNavigationDTL
        WHERE   numTabID = @numTabID 
 

SELECT
	ROW_NUMBER() OVER (ORDER BY numOrder) AS ID,
	*
INTO
	#RelationshipTree 
FROM
	(
	SELECT    
		PND.numPageNavID AS numPageNavID,
		NULL AS numListItemID,
		ISNULL(TNA.bitVisible,1) AS bitVisible,
		ISNULL(PND.vcPageNavName, '') AS [vcNodeName],
		ISNULL(vcNavURL, '') AS vcNavURL,
        vcImageURL,
		ISNULL(TNA.tintType,1) AS tintType,
		PND.numParentID AS numParentID,
		PND.numParentID AS numOriginalParentID,
		ISNULL(TreeNodeOrder.numOrder,1000) AS numOrder,
		0 AS isUpdated
	FROM      
		PageNavigationDTL PND
	LEFT JOIN 
		dbo.TreeNavigationAuthorization TNA 
	ON 
		TNA.numPageNavID = PND.numPageNavID AND
		TNA.numDomainID = @numDomainID AND 
		TNA.numGroupID = @numGroupID
	LEFT JOIN
		dbo.TreeNodeOrder 
	ON
		dbo.TreeNodeOrder.numTabID = @numTabID AND
		dbo.TreeNodeOrder.numDomainID = @numDomainID AND
		dbo.TreeNodeOrder.numGroupID = @numGroupID AND
		dbo.TreeNodeOrder.numPageNavID = PND.numPageNavID
	WHERE     
		1 = 1
		AND ISNULL(PND.bitVisible, 0) = 1
		AND numModuleID = @numModuleID
	UNION
	SELECT
		NULL AS numPageNavID,
		ListDetails.numListItemID,
		ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
		ListDetails.vcData AS [vcNodeName],
		'../prospects/frmCompanyList.aspx?RelId='+ convert(varchar(10),ListDetails.numListItemID) vcNavURL,
		NULL as vcImageURL,
		1 AS tintType,
		6 AS numParentID,
		6 AS numOriginalParentID,
		ISNULL(TreeNodeOrder.numOrder,2000) AS numOrder,
		0 AS isUpdated
	FROM
		ListDetails
	LEFT JOIN 
		dbo.TreeNodeOrder 
	ON
		dbo.TreeNodeOrder.numListItemID = ListDetails.numListItemID AND
		dbo.TreeNodeOrder.numParentID = 6 AND
		dbo.TreeNodeOrder.numTabID = @numTabID AND
		dbo.TreeNodeOrder.numDomainID = @numDomainID AND
		dbo.TreeNodeOrder.numGroupID = @numGroupID AND
		dbo.TreeNodeOrder.numPageNavID IS NULL
	WHERE
		numListID = 5 AND
		(ListDetails.numDomainID = @numDomainID  OR constFlag = 1) AND
		(ISNULL(bitDelete,0) = 0 OR constFlag = 1) AND
		ListDetails.numListItemID <> 46 
	UNION       
	SELECT 
		NULL AS numPageNavID,
		L2.numListItemID,   
		ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
		L2.vcData AS [vcNodeName],
		'../prospects/frmCompanyList.aspx?RelId=' 
		+ convert(varchar(10), FRDTL.numPrimaryListItemID) + '&profileid='
        + convert(varchar(10), numSecondaryListItemID) as vcNavURL,
        NULL as vcImageURL,
		1 AS tintType,
		FRDTL.numPrimaryListItemID AS numParentID,
		FRDTL.numPrimaryListItemID AS numOriginalParentID,
		ISNULL(TreeNodeOrder.numOrder,3000) AS numOrder,
		0 AS isUpdated
	FROM      
		FieldRelationship FR
    join 
		FieldRelationshipDTL FRDTL on FRDTL.numFieldRelID = FR.numFieldRelID
    join 
		ListDetails L1 on numPrimaryListItemID = L1.numListItemID
    join 
		ListDetails L2 on numSecondaryListItemID = L2.numListItemID
	LEFT JOIN 
		dbo.TreeNodeOrder 
	ON
		dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
		dbo.TreeNodeOrder.numParentID = FRDTL.numPrimaryListItemID AND
		dbo.TreeNodeOrder.numTabID = @numTabID AND
		dbo.TreeNodeOrder.numDomainID = @numDomainID AND
		dbo.TreeNodeOrder.numGroupID = @numGroupID AND
		dbo.TreeNodeOrder.numPageNavID IS NULL
	WHERE     
		numPrimaryListItemID <> 46
        and FR.numPrimaryListID = 5
		and FR.numSecondaryListID = 21
		and FR.numDomainID = @numDomainID
        and L2.numDomainID = @numDomainID
	UNION
	SELECT 
		NULL AS numPageNavID,
		L2.numListItemID,
		ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
		L2.vcData AS [vcNodeName],
		'../prospects/frmProspectList.aspx?numProfile=' + CONVERT(VARCHAR(10), L2.numListItemID) AS vcNavURL,
        NULL AS vcImageURL,
		1 AS tintType,
		11 AS numParentID,
		11 AS numOriginalParentID,
		ISNULL(TreeNodeOrder.numOrder,4000) AS numOrder,
		0 AS isUpdated
	FROM    
		FieldRelationship AS FR
	INNER JOIN 
		FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
	INNER JOIN 
		ListDetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
	INNER JOIN 
		ListDetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
	LEFT JOIN 
		dbo.TreeNodeOrder 
	ON
		dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
		dbo.TreeNodeOrder.numParentID = 11 AND
		dbo.TreeNodeOrder.numTabID = @numTabID AND
		dbo.TreeNodeOrder.numDomainID = @numDomainID AND
		dbo.TreeNodeOrder.numGroupID = @numGroupID AND
		dbo.TreeNodeOrder.numPageNavID IS NULL
	WHERE   ( FRD.numPrimaryListItemID = 46 )
			AND ( FR.numDomainID = @numDomainID )
	UNION
	SELECT 
		NULL AS numPageNavID,
		L2.numListItemID,
		ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
		L2.vcData AS [vcNodeName],
		'../account/frmAccountList.aspx?numProfile=' + CONVERT(VARCHAR(10), L2.numListItemID) AS vcNavURL,
        NULL AS vcImageURL,
		1 AS tintType,
		12 AS numParentID,
		12 AS numOriginalParentID,
		ISNULL(TreeNodeOrder.numOrder,5000) AS numOrder,
		0 AS isUpdated
	FROM    
		FieldRelationship AS FR
	INNER JOIN 
		FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
	INNER JOIN 
		ListDetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
	INNER JOIN 
		ListDetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
	LEFT JOIN 
		dbo.TreeNodeOrder 
	ON
		dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
		dbo.TreeNodeOrder.numParentID = 12 AND
		dbo.TreeNodeOrder.numTabID = @numTabID AND
		dbo.TreeNodeOrder.numDomainID = @numDomainID AND
		dbo.TreeNodeOrder.numGroupID = @numGroupID AND
		dbo.TreeNodeOrder.numPageNavID IS NULL
	WHERE   ( FRD.numPrimaryListItemID = 46 )
			AND ( FR.numDomainID = @numDomainID )
	UNION    
	--Select Contact
	SELECT 
		NULL AS numPageNavID,
		listdetails.numListItemID,
		ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
		listdetails.vcData AS [vcNodeName],
		'../contact/frmContactList.aspx?ContactType=' + CONVERT(VARCHAR(10), listdetails.numListItemID) AS vcNavURL,
        NULL AS vcImageURL,
		1 AS tintType,
		13 AS numParentID,
		13 AS numOriginalParentID,
		ISNULL(TreeNodeOrder.numOrder,6000) AS numOrder,
		0 AS isUpdated
	FROM    
		listdetails
	LEFT JOIN 
		dbo.TreeNodeOrder 
	ON
		dbo.TreeNodeOrder.numListItemID = listdetails.numListItemID AND
		dbo.TreeNodeOrder.numParentID = 13 AND
		dbo.TreeNodeOrder.numTabID = @numTabID AND
		dbo.TreeNodeOrder.numDomainID = @numDomainID AND
		dbo.TreeNodeOrder.numGroupID = @numGroupID AND
		dbo.TreeNodeOrder.numPageNavID IS NULL
	WHERE   numListID = 8
			AND ( constFlag = 1
				  OR listdetails.numDomainID = @numDomainID
				)
	UNION
	SELECT 
		NULL AS numPageNavID,
		101 AS numListItemID,
		ISNULL((
					SELECT 
						bitVisible 
					FROM 
						dbo.TreeNodeOrder 
					WHERE
						dbo.TreeNodeOrder.numListItemID = 101 AND
						dbo.TreeNodeOrder.numParentID = 13 AND
						dbo.TreeNodeOrder.numTabID = @numTabID AND
						dbo.TreeNodeOrder.numDomainID = @numDomainID AND
						dbo.TreeNodeOrder.numGroupID = @numGroupID AND
						dbo.TreeNodeOrder.numPageNavID IS NULL
				),1) AS bitVisible,
		'Primary Contact' AS [vcNodeName],
		'../Contact/frmcontactList.aspx?ContactType=101',
        NULL AS vcImageURL,
		1 AS tintType,
		13 AS numParentID,
		13 AS numOriginalParentID,
		ISNULL((
					SELECT 
						numOrder 
					FROM 
						dbo.TreeNodeOrder 
					WHERE
						dbo.TreeNodeOrder.numListItemID = 101 AND
						dbo.TreeNodeOrder.numParentID = 13 AND
						dbo.TreeNodeOrder.numTabID = @numTabID AND
						dbo.TreeNodeOrder.numDomainID = @numDomainID AND
						dbo.TreeNodeOrder.numGroupID = @numGroupID AND
						dbo.TreeNodeOrder.numPageNavID IS NULL
				),7000) AS numOrder,
		 0 AS isUpdated
	) AS TEMP
	ORDER BY
		TEMP.numOrder

	DECLARE @i AS INT = 1
	DECLARE @Count AS INT
	DECLARE @oldParentID AS INT
	SELECT @COUNT=COUNT(ID) FROM #RelationshipTree

	WHILE @i <= @COUNT
	BEGIN
		IF (SELECT ISNULL(numPageNavID,0) FROM #RelationshipTree WHERE ID = @i) <> 0
			SELECT @oldParentID = numPageNavID FROM #RelationshipTree WHERE ID = @i
		ELSE
			SELECT @oldParentID = numListItemID FROM #RelationshipTree WHERE ID = @i


		UPDATE #RelationshipTree SET numParentID = @i, isUpdated=1 WHERE numParentID = @oldParentID AND isUpdated = 0

		SET @i = @i + 1
	END


	IF @bitVisibleOnly = 1
		SELECT 
			CAST(ID AS INT) As ID,
			numPageNavID,
			numListItemID,
			bitVisible,
			vcNodeName, 
			vcNavURL, 
			vcImageURL,
			tintType,
			(CASE numParentID WHEN 0 THEN NULL ELSE CAST(numParentID AS INT) END) AS numParentID,
			numOriginalParentID, 
			numOrder 
		FROM
			#RelationshipTree 
		WHERE 
			bitVisible = 1
			AND (numParentID IN (SELECT ID FROM #RelationshipTree WHERE bitVisible = 1) OR numParentID = 0)
	ELSE
		SELECT CAST(ID AS INT) As ID,numPageNavID,numListItemID,bitVisible,vcNodeName,tintType,(CASE numParentID WHEN 0 THEN NULL ELSE CAST(numParentID AS INT) END) AS numParentID,numOriginalParentID, numOrder FROM #RelationshipTree WHERE numParentID IN (SELECT ID FROM #RelationshipTree) OR ISNULL(numParentID,0) = 0
	
	DROP TABLE #RelationshipTree
      
    END
/****** Object:  StoredProcedure [dbo].[USP_GetWareHouseItems]    Script Date: 07/26/2008 16:18:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_GetWareHouseItems 6                            
--created by anoop jayaraj                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getwarehouseitems')
DROP PROCEDURE usp_getwarehouseitems
GO
CREATE PROCEDURE [dbo].[USP_GetWareHouseItems]                              
@numItemCode as numeric(9)=0,
@byteMode as tinyint=0                              
as                              
                              
                          
                              
declare @bitSerialize as bit                      
declare @str as nvarchar(max)                 
declare @str1 as nvarchar(max)               
declare @ColName as varchar(25)                      
set @str=''                       
                        
DECLARE @bitKitParent BIT
declare @numItemGroupID as numeric(9)                        
                        
select @numItemGroupID=numItemGroup,@bitSerialize=CASE WHEN bitSerialized=0 THEN bitLotNo ELSE bitSerialized END,
@bitKitParent = ( CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END ) from Item where numItemCode=@numItemCode                        
if @bitSerialize=1 set @ColName='numWareHouseItmsDTLID,1'              
else set @ColName='numWareHouseItemID,0'              
              
--Create a Temporary table to hold data                                                            
create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                      
 numCusFlDItemID numeric(9)                                                         
 )                         
                        
insert into #tempTable                         
(numCusFlDItemID)                                                            
select distinct(numOppAccAttrID) from ItemGroupsDTL where numItemGroupID=@numItemGroupID and tintType=2                       
                          
                 
 declare @ID as numeric(9)                        
 declare @numCusFlDItemID as varchar(20)                        
 declare @fld_label as varchar(100),@fld_type as varchar(100)                        
 set @ID=0                        
 select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label,@fld_type=fld_type from #tempTable                         
 join CFW_Fld_Master on numCusFlDItemID=Fld_ID                        
                         
 while @ID>0                        
 begin                        
                          
   set @str=@str+',  dbo.GetCustFldItems('+@numCusFlDItemID+',9,'+@ColName+') as ['+ @fld_label+']'
	
	IF @byteMode=1                                        
		set @str=@str+',  dbo.GetCustFldItemsValue('+@numCusFlDItemID+',9,'+@ColName+','''+@fld_type+''') as ['+ @fld_label+'Value]'                                        
                          
   select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempTable                         
   join CFW_Fld_Master on numCusFlDItemID=Fld_ID and ID >@ID                        
   if @@rowcount=0 set @ID=0                        
                          
 end                        
                       
--      

DECLARE @KitOnHand AS NUMERIC(9);SET @KitOnHand=0

--IF @bitKitParent=1
	--SELECT @KitOnHand=ISNULL(dbo.fn_GetKitInventory(@numItemCode),0)          
  
set @str1='select '

IF @byteMode=1                                        
		set @str1 =@str1 +'I.numItemCode,'
	
set @str1 =@str1 + 'numWareHouseItemID,isnull(vcWarehouse,'''') + '': '' + isnull(WL.vcLocation,'''') vcWarehouse,W.numWareHouseID,
Case when @bitKitParent=1 then ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) else isnull(numOnHand,0) end as [OnHand],
CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numPurchaseUnit,0)) * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as numeric(18,2)) as PurchaseOnHand,
CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numSaleUnit,0)) * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as numeric(18,2)) as SalesOnHand,
Case when @bitKitParent=1 THEN 0 ELSE isnull(numReorder,0) END as [Reorder] 
,Case when @bitKitParent=1 THEN 0 ELSE isnull(numOnOrder,0) END as [OnOrder]
,Case when @bitKitParent=1 THEN 0 ELSE isnull(numAllocation,0) END as [Allocation] 
,Case when @bitKitParent=1 THEN 0 ELSE isnull(numBackOrder,0) END as [BackOrder]
,0 as Op_Flag , isnull(WL.vcLocation,'''') Location,isnull(WL.numWLocationID,0) as numWLocationID,
round(isnull(monWListPrice,0),2) Price,isnull(vcWHSKU,'''') as SKU,isnull(vcBarCode,'''') as BarCode '+ case when @bitSerialize=1 then  ' ' else  @str end +'                   
,(SELECT CASE WHEN ISNULL(subI.bitKitParent,0) = 1 AND ISNULL((SELECT COUNT(*) FROM OpportunityItems 
															   WHERE OpportunityItems.numItemCode = WareHouseItems.numItemID
															   AND WareHouseItems.numWareHouseItemID = OpportunityItems.numWarehouseItmsID),0) = 0 THEN 1
			ELSE 0 
	    END 
FROM Item subI WHERE subI.numItemCode = WareHouseItems.numItemID) [IsDeleteKitWarehouse],@bitKitParent [bitKitParent]
from WareHouseItems                           
join Warehouses W                             
on W.numWareHouseID=WareHouseItems.numWareHouseID 
left join WarehouseLocation WL on WL.numWLocationID =WareHouseItems.numWLocationID
join Item I on I.numItemCode=WareHouseItems.numItemID and WareHouseItems.numDomainId=I.numDomainId
where numItemID='+convert(varchar(15),@numItemCode)                  
   
print(@str1)           

EXECUTE sp_executeSQL @str1, N'@bitKitParent bit',@bitKitParent
                       
                        
set @str1='select numWareHouseItmsDTLID,WDTL.numWareHouseItemID, vcSerialNo,0 as Op_Flag,WDTL.vcComments as Comments,WDTL.numQty,WDTL.numQty as OldQty,W.vcWarehouse '+ case when @bitSerialize=1 then @str else '' end +'
,WDTL.dExpirationDate 
from WareHouseItmsDTL WDTL                             
join WareHouseItems WI                             
on WDTL.numWareHouseItemID=WI.numWareHouseItemID                              
join Warehouses W                             
on W.numWareHouseID=WI.numWareHouseID  
where 1 = (CASE WHEN ' + CAST(@byteMode AS VARCHAR(1)) + ' = ''1'' THEN CASE WHEN ISNULL(numQty,0) > 0 THEN 1 ELSE 0 END ELSE 1 END) and  numItemID='+ convert(varchar(15),@numItemCode)                          
print @str1                       
exec (@str1)                       
                      
                      
select Fld_label,fld_id,fld_type,numlistid,vcURL from #tempTable                         
 join CFW_Fld_Master on numCusFlDItemID=Fld_ID                      
drop table #tempTable


-----------------For Kendo UI testting purpose
--create table #tempColumnConfig ( field varchar(100),                                                                      
-- title varchar(100) , format varchar(100)                                                      
-- )  
--
--insert into #tempColumnConfig
--	select 'vcWarehouse','WareHouse','' union all
--	select 'OnHand','On Hand','' union all
--	select 'Reorder','Re Order','' union all
--	select 'Allocation','Allocation','' union all
--	select 'BackOrder','Back Order','' union all
--	select 'Price','Price','{0:c}'
--
--select * from #tempColumnConfig
--
--drop table #tempColumnConfig

-----------------

--set @str1='select case when COUNT(*)=1 then MIN(numWareHouseItmsDTLID) else 0 end as numWareHouseItmsDTLID,MIN(WDTL.numWareHouseItemID) as numWareHouseItemID, vcSerialNo,0 as Op_Flag,WDTL.vcComments as Comments'+ case when @bitSerialize=1 then @str else '' end +',W.vcWarehouse,COUNT(*) Qty
--from WareHouseItmsDTL WDTL                             
--join WareHouseItems WI                             
--on WDTL.numWareHouseItemID=WI.numWareHouseItemID                              
--join Warehouses W                             
--on W.numWareHouseID=WI.numWareHouseID  
--where (tintStatus is null or tintStatus=0)  and  numItemID='+ convert(varchar(15),@numItemCode) +'
--GROUP BY vcSerialNo,WDTL.vcComments,W.vcWarehouse'
--                          
--print @str1                       
--exec (@str1)                       
              
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertNewChartAccountDetails]    Script Date: 09/25/2009 16:15:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva                                
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_insertnewchartaccountdetails' ) 
    DROP PROCEDURE usp_insertnewchartaccountdetails
GO
CREATE PROCEDURE [dbo].[usp_InsertNewChartAccountDetails]
    @numAcntTypeId NUMERIC(9) = 0,
    @numParntAcntTypeID NUMERIC(9) = 0,
    @vcAccountName VARCHAR(100),
    @vcAccountDescription VARCHAR(100),
    @monOriginalOpeningBal MONEY,
    @monOpeningBal MONEY,
    @dtOpeningDate DATETIME,
    @bitActive BIT,
    @numAccountId NUMERIC(9) = 0 OUTPUT,
    @bitFixed BIT,
    @numDomainId NUMERIC(9) = 0,
    @numListItemID NUMERIC(9) = 0,
    @numUserCntId NUMERIC(9) = 0,
    @bitProfitLoss BIT,
    @bitDepreciation BIT,
    @dtDepreciationCostDate DATETIME,
    @monDepreciationCost MONEY,
    @vcNumber VARCHAR(50),
    @bitIsSubAccount BIT,
    @numParentAccId NUMERIC(18, 0),
    @IsBankAccount			BIT	=0,
	@vcStartingCheckNumber	VARCHAR(50)=NULL ,
	@vcBankRountingNumber	VARCHAR(50)=NULL ,
	@bitIsConnected				BIT =0,
	@numBankDetailID			NUMERIC(18,0)=NULL

AS 
    BEGIN 
    
        DECLARE @numAccountId1 AS NUMERIC(9)                              
        DECLARE @strSQl AS VARCHAR(1000)                             
        DECLARE @strSQLBalance AS VARCHAR(1000)                           
        DECLARE @numJournalId AS NUMERIC(9) 
        DECLARE @numDepositId AS NUMERIC(9)     
        DECLARE @numDepositJournalId AS NUMERIC(9)  
        DECLARE @numCashCreditJournalId AS NUMERIC(9)  
        DECLARE @numCashCreditId AS NUMERIC(9)   
        DECLARE @numAccountIdOpeningEquity AS NUMERIC(9)
        DECLARE @vcAccountCode VARCHAR(50)
        DECLARE @intLevel AS INT
        DECLARE @intFlag INT
        DECLARE @vcAccountNameWithNumber AS VARCHAR(100)
        
    
        SET @strSQl = ''                          
        SET @strSQLBalance = ''   
        
        SET @intFlag = 1 
        IF ISNULL(@vcNumber,'') = '-1' --@vcNumber will have -1 value only when its being called from [USP_ChartAcntDefaultValues], as well cursor below creates time out.
		BEGIN
		
			SET @intFlag = 0 
			SET @vcNumber= ''
			PRINT @intFlag
		END
		  
        
        IF ISNULL(@vcNumber, '') = '' 
            SET @vcAccountNameWithNumber = ''	
        ELSE 
            SET @vcAccountNameWithNumber = @vcNumber + ' '   
	
		IF @numAcntTypeId = 0 
		BEGIN
			DECLARE @AccountTypeCode VARCHAR(4)
			(SELECT @AccountTypeCode = SUBSTRING(vcAccountCode ,1,4) FROM dbo.AccountTypeDetail WHERE numAccountTypeID = @numParntAcntTypeID)
			SELECT @numAcntTypeId = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID=@numDomainID AND vcAccountCode =  @AccountTypeCode
		END
	                             
 --Check for Current Fiancial Year
        IF ( SELECT COUNT([numFinYearId])             FROM   [FinancialYear]             WHERE  [numDomainId] = @numDomainId AND [bitCurrentYear] = 1) = 0 
            BEGIN
                RAISERROR ( 'NoCurrentFinancialYearSet', 16, 1 ) ;
                RETURN ;
            END
	
		  
        IF @numAccountId = 0 
            BEGIN 
				IF ISNULL(@vcNumber, '') <> '' 
				BEGIN
					IF EXISTS(SELECT * FROM Chart_Of_Accounts WHERE vcNumber = @vcNumber AND numDomainID =  @numDomainId)	
					BEGIN
						RAISERROR ( 'DuplicateNumber', 16, 1 ) ;
						RETURN ;
					END
				END 
				 
                IF @numParntAcntTypeID = 0 
                    SET @numParntAcntTypeID = ( SELECT TOP 1
                                                        numAccountId
                                                FROM    dbo.Chart_Of_Accounts
                                                WHERE   Chart_Of_Accounts.numParntAcntTypeID IS NULL
                                                        AND Chart_Of_Accounts.numDomainId = @numDomainId
                                              ) --and numAccountId = 1 
        
                SET @numAccountIdOpeningEquity = ( SELECT TOP 1
                                                            numAccountId
                                                   FROM     dbo.Chart_Of_Accounts
                                                   WHERE    /*bitOpeningBalanceEquity=1  Commnted by chintan bug id 738*/
                                                            bitProfitLoss = 1
                                                            AND Chart_Of_Accounts.numDomainId = @numDomainId
                                                 ) 
            
         
                IF ( ISNULL(@numAccountIdOpeningEquity, 0) = 0
                     AND @bitProfitLoss = 0
                   )--Added and condition, bug id 882 #2
                    BEGIN
                        RAISERROR ( 'NoProfitLossACC', 16, 1 ) ;
                        RETURN 
                    END
			
                SELECT  @vcAccountCode = dbo.GetAccountTypeCode(@numDomainId, @numParntAcntTypeID, 1)
                
                 IF (@bitIsSubAccount=1)
				 BEGIN
						SELECT  @vcAccountCode = dbo.GetAccountTypeCode(@numDomainId, @numParentAccId, 3)
						SELECT @numParntAcntTypeID = numParntAcntTypeId FROM Chart_Of_Accounts WHERE numDomainId=@numDomainId AND numAccountId = @numParentAccId
				 END
                
                SELECT  @intLevel = CASE WHEN @numParentAccId = 0 AND @bitIsSubAccount = 0 THEN 0 ELSE ISNULL(MAX(intLevel),0) + 1 END  FROM dbo.Chart_Of_Accounts WHERE numAccountId = @numParentAccId 
                PRINT @intLevel
									   
--			PRINT @vcAccountCode
                INSERT  INTO dbo.Chart_Of_Accounts
                        (
                          numAcntTypeId,
                          numParntAcntTypeID,
                          vcAccountCode,
                          vcAccountName,
                          vcAccountDescription,
                          numOriginalOpeningBal,
                          numOpeningBal,
                          dtOpeningDate,
                          bitActive,
                          bitFixed,
                          numDomainId,
                          numListItemID,
                          bitProfitLoss,
                          [bitDepreciation],
                          [dtDepreciationCostDate],
                          [monDepreciationCost],
                          vcNumber,
                          bitIsSubAccount,
                          numParentAccId,
                          intLevel,
                          IsBankAccount,
                          vcStartingCheckNumber,
                          vcBankRountingNumber,
                          IsConnected,
                          numBankDetailID
                        )
                VALUES  (
                          @numAcntTypeId,
                          @numParntAcntTypeID,
                          @vcAccountCode,
                          @vcAccountNameWithNumber + @vcAccountName,
                          @vcAccountDescription,
                          @monOriginalOpeningBal,
                          @monOpeningBal,
                          @dtOpeningDate,
                          @bitActive,
                          @bitFixed,
                          @numDomainId,
                          @numListItemID,
                          @bitProfitLoss,
                          @bitDepreciation,
                          @dtDepreciationCostDate,
                          @monDepreciationCost,
                          @vcNumber,
                          @bitIsSubAccount,
                          @numParentAccId,
                          @intLevel,
                          @IsBankAccount,
                          @vcStartingCheckNumber,
                          @vcBankRountingNumber,
                          @bitIsConnected,
                          @numBankDetailID
                        )
                SET @numAccountId1 = @@IDENTITY                                                  
                SET @numAccountId = @numAccountId1 --used at bottom
                SELECT  @numAccountId1                            
				
			
			
			--Added By Chintan
                EXEC USP_ManageChartAccountOpening @numDomainId,
                    @numAccountId1, @monOriginalOpeningBal, @dtOpeningDate
    
			
	                                              
            END                                                    
                                                
	
        ELSE 
            IF @numAccountId <> 0 
                BEGIN
					
                   IF EXISTS(SELECT * FROM Chart_Of_Accounts WHERE numAccountId = @numAccountId AND numDomainId = @numDomainId 
							AND (numAcntTypeId != @numAcntTypeId OR numParntAcntTypeId != @numParntAcntTypeID))
			 		BEGIN
						IF NOT ((SELECT COUNT(*) FROM AccountTypeDetail WHERE numAccountTypeID=@numParntAcntTypeID AND vcAccountcode LIKE '0106%' AND numDomainId = @numDomainId )=1
							AND (SELECT COUNT(*) FROM AccountTypeDetail WHERE numAccountTypeID=(SELECT numParntAcntTypeId FROM Chart_Of_Accounts WHERE numAccountId = @numAccountId AND numDomainId = @numDomainId) AND vcAccountcode LIKE '0104%' AND numDomainId = @numDomainId)=1)
						BEGIN
							IF EXISTS (SELECT GJD.numJournalId FROM dbo.General_Journal_Details GJD WHERE numDomainId = @numDomainId AND numChartAcntId=@numAccountId)
							BEGIN
								RAISERROR ( 'JournalEntryExists', 16, 1 ) ;
								RETURN ;
							END
						END
					END
					
					IF ISNULL(@vcNumber, '') <> '' 
					BEGIN
						IF EXISTS(SELECT * FROM Chart_Of_Accounts WHERE vcNumber = @vcNumber AND numDomainID =  @numDomainId AND numAccountId <> @numAccountId)	
						BEGIN
							RAISERROR ( 'DuplicateNumber', 16, 1 ) ;
							RETURN ;
						END
					END 
                    IF @numParntAcntTypeID = 0 
                        SET @numParntAcntTypeID = ( SELECT  Chart_Of_Accounts.numAccountId
                                                    FROM    dbo.Chart_Of_Accounts
                                                    WHERE   Chart_Of_Accounts.numParntAcntTypeID IS NULL
                                                            AND Chart_Of_Accounts.numDomainId = @numDomainId
                                                  ) --and numAccountId = 1 
--Commented by chintan, reason:Do not allow changing of account codes when someone changes just description. as they are depending on given account code(might be used for Third party accounting software accounts mapping).
--			SELECT @vcAccountCode=dbo.GetAccountTypeCode(@numDomainId,@numParntAcntTypeID,1)                              
			
			
                    DECLARE @OldOriginalOpeningBalance AS MONEY 
                    DECLARE @OldOpeningBalance AS MONEY 
                    DECLARE @NewOpeningBalance AS MONEY
                    DECLARE @OldParentAcntTypeID AS NUMERIC
					DECLARE @OldIsSubAccount AS NUMERIC
					
                    SELECT  @OldOriginalOpeningBalance = numOriginalOpeningBal,
                            @OldOpeningBalance = numOpeningBal,
                            @OldParentAcntTypeID = numParntAcntTypeId,
                            @vcAccountCode = vcAccountCode,
                            @OldIsSubAccount = ISNULL(bitIsSubAccount,0)
                    FROM    dbo.Chart_Of_Accounts
                    WHERE   numAccountId = @numAccountID
                            AND numDomainId = @numDomainID
			
--                    IF ( @OldOriginalOpeningBalance < @monOriginalOpeningBal ) 
--                        SET @NewOpeningBalance = @OldOpeningBalance
--                            + ( @monOriginalOpeningBal
--                                - @OldOriginalOpeningBalance )
--                    ELSE 
--                        IF ( @OldOriginalOpeningBalance > @monOriginalOpeningBal ) 
--                            SET @NewOpeningBalance = @OldOpeningBalance
--                                - ( @OldOriginalOpeningBalance
--                                    - @monOriginalOpeningBal )
--                        ELSE 
--                            SET @NewOpeningBalance = @OldOpeningBalance ;
			
			
                    IF ( @OldParentAcntTypeID != @numParntAcntTypeID ) --bug fix 1439
                    BEGIN
                    	SELECT  @vcAccountCode = dbo.GetAccountTypeCode(@numDomainId, @numParntAcntTypeID, 1)
                    	SET @numParentAccId = 0
                    	SET @bitIsSubAccount = 0
                    END
                        
--				
--					IF (@OldIsSubAccount = 0 AND  @bitIsSubAccount  =1 )
--						SELECT  @vcAccountCode = dbo.GetAccountTypeCode(@numDomainId, @numParentAccId, 3)
--						
--					IF (@OldIsSubAccount = 1 AND  @bitIsSubAccount  =0 )
--					BEGIN
--						UPDATE dbo.Chart_Of_Accounts SET vcAccountCode='' WHERE numAccountId=@numAccountId
--						SELECT  @vcAccountCode = dbo.GetAccountTypeCode(@numDomainId, @numParntAcntTypeID, 1)
--					END
--						
				




			
			
                    UPDATE  Chart_Of_Accounts
                    SET     numAcntTypeId = @numAcntTypeId,
                            numParntAcntTypeId = @numParntAcntTypeID,
                            vcAccountName = @vcAccountNameWithNumber
                            + @vcAccountName,
                            vcAccountDescription = @vcAccountDescription,
                            bitActive = @bitActive,
                            bitProfitLoss = @bitProfitLoss,
--                            numOpeningBal = @NewOpeningBalance,
                            [numOriginalOpeningBal] = @monOriginalOpeningBal,
                            [dtOpeningDate] = @dtOpeningDate,
                            [bitDepreciation] = @bitDepreciation,
                            [dtDepreciationCostDate] = @dtDepreciationCostDate,
                            [monDepreciationCost] = @monDepreciationCost,
                            vcAccountCode = @vcAccountCode,
                            bitIsSubAccount = @bitIsSubAccount,
                            vcNumber = @vcNumber,
                            numParentAccId = @numParentAccId,
							IsBankAccount = @IsBankAccount,
							vcStartingCheckNumber = @vcStartingCheckNumber,
							vcBankRountingNumber = @vcBankRountingNumber,
							IsConnected = @bitIsConnected,
							numBankDetailID = @numBankDetailID
                    WHERE   ( numAccountId = @numAccountId )
                            AND ( numDomainId = @numDomainId )
			
			--UPDATE dbo.Chart_Of_Accounts SET intLevel = 0 WHERE numAccountId = @numAccountId
                    UPDATE  dbo.Chart_Of_Accounts
                    SET     intLevel = CASE WHEN numParentAccId = 0 AND bitIsSubAccount = 0 THEN 0
											ELSE ( SELECT ISNULL(intLevel,0) + 1 FROM dbo.Chart_Of_Accounts WHERE numAccountId = @numParentAccId )
									   END
                    WHERE   (bitIsSubAccount = 1 OR bitIsSubAccount = 0)
                            AND numAccountId = @numAccountId
			--Added By Chintan

					
			
			--Added By Chintan
                    EXEC USP_ManageChartAccountOpening @numDomainId,
                        @numAccountId, @monOriginalOpeningBal, @dtOpeningDate
			
                                                                      
                END 
		
			
		
    END
    
    
--Maintain Sorting by Account code when adding new account with sub account  or  updating Account numeber
PRINT @intFlag
 if (@intFlag =1) -- @vcNumber will have -1 value only when its being called from [USP_ChartAcntDefaultValues], as well cursor below creates time out.
 BEGIN
		/*SELECT vcAccountCode,vcAccountName,bitIsSubAccount,numAccountId FROM dbo.Chart_Of_Accounts WHERE numParntAcntTypeId = @numParntAcntTypeID  
		ORDER BY vcAccountName ASC ,bitIsSubAccount DESC */
 
		UPDATE dbo.Chart_Of_Accounts SET vcAccountCode='' WHERE numParntAcntTypeId = @numParntAcntTypeID  AND numDomainId=@numDomainID

		Declare @numTempAccountID NUMERIC
		Declare @bitTempIsSubAccount bit
		Declare @numTempParentAccId NUMERIC
		DECLARE @vcTempNumber varchar(50)
		DECLARE @vcTempAccountCode VARCHAR(100)
		DECLARE @getAccountID CURSOR
		SET @getAccountID = CURSOR FOR
		SELECT  numAccountId, bitIsSubAccount , numParentAccId , vcNumber FROM dbo.Chart_Of_Accounts WHERE numParntAcntTypeId = @numParntAcntTypeID ORDER BY bitIsSubAccount,vcAccountName asc--vcAccountName ASC,bitIsSubAccount DESC  -- ,bitIsSubAccount DESC
		OPEN @getAccountID
		FETCH NEXT
			FROM @getAccountID INTO @numTempAccountID, @bitTempIsSubAccount, @numTempParentAccId, @vcTempNumber 
		WHILE @@FETCH_STATUS = 0
		BEGIN
			PRINT 'inside cursor---------'
				
				IF @bitTempIsSubAccount =1 
					SELECT @vcTempAccountCode = dbo.GetAccountTypeCode(@numDomainId, @numTempParentAccId, 3)
				ELSE
					SELECT @vcTempAccountCode = dbo.GetAccountTypeCode(@numDomainId, @numParntAcntTypeID, 1)
				SELECT @numTempAccountID numTempAccountID, @bitTempIsSubAccount bitTempIsSubAccount, @numParntAcntTypeID numParntAcntTypeID, @numTempParentAccId numTempParentAccId, @vcTempAccountCode vcTempAccountCode

					UPDATE dbo.Chart_Of_Accounts SET vcAccountCode=@vcTempAccountCode WHERE numAccountId=@numTempAccountID
		--			SELECT vcAccountCode FROM dbo.Chart_Of_Accounts WHERE numAccountId = @numTempAccountID

		FETCH NEXT
		FROM @getAccountID INTO @numTempAccountID, @bitTempIsSubAccount, @numTempParentAccId, @vcTempNumber 
		END
		CLOSE @getAccountID
		DEALLOCATE @getAccountID
END
--SELECT vcAccountCode,vcAccountName,numAccountId,bitIsSubAccount FROM dbo.Chart_Of_Accounts WHERE numParntAcntTypeId = @numParntAcntTypeID  ORDER BY vcAccountCode ASC 

    
    
    
--Maintain only one profit and loss account through out system
    IF @bitProfitLoss = 1 
        BEGIN
            UPDATE  dbo.Chart_Of_Accounts
            SET     bitProfitLoss = 0
            WHERE   numAccountId <> @numAccountId
                    AND numDomainId = @numDomainID
        END
--Created By Anoop Jayaraj          
--exec USP_ItemDetailsForEcomm @numItemCode=859064,@numWareHouseID=1074,@numDomainID=172,@numSiteId=90
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itemdetailsforecomm')
DROP PROCEDURE USP_ItemDetailsForEcomm
GO
CREATE PROCEDURE [dbo].[USP_ItemDetailsForEcomm]
@numItemCode as numeric(9),          
@numWareHouseID as numeric(9),    
@numDomainID as numeric(9),
@numSiteId as numeric(9)                                              
as     
/*Removed warehouse parameter, and taking Default warehouse by default from DB*/
    
declare @bitShowInStock as bit        
declare @bitShowQuantity as BIT
declare @bitAutoSelectWarehouse as bit        
--declare @tintColumns as tinyint        
DECLARE @numDefaultWareHouseID as numeric(9)

set @bitShowInStock=0        
set @bitShowQuantity=0        
--set @tintColumns=1        
        
select @bitShowInStock=isnull(bitShowInStock,0),@bitShowQuantity=isnull(bitShowQOnHand,0),@bitAutoSelectWarehouse=ISNULL(bitAutoSelectWarehouse,0)--,@tintColumns=isnull(tintItemColumns,1)        
from eCommerceDTL where numDomainID=@numDomainID
IF @numWareHouseID=0
BEGIN
	SELECT @numDefaultWareHouseID=ISNULL(numDefaultWareHouseID,0) FROM [eCommerceDTL] WHERE [numDomainID]=@numDomainID	
	SET @numWareHouseID =@numDefaultWareHouseID;
END
/*If qty is not found in default warehouse then choose next warehouse with sufficient qty */
IF @bitAutoSelectWarehouse = 1 AND ( SELECT COUNT(*) FROM dbo.WareHouseItems WHERE  numItemID = @numItemCode AND numWarehouseID= @numWareHouseID ) = 0 
BEGIN
	SELECT TOP 1 @numDefaultWareHouseID = numWareHouseID FROM dbo.WareHouseItems WHERE  numItemID = @numItemCode AND numOnHand>0 /*TODO: Pass actuall qty from cart*/ ORDER BY numOnHand DESC 
	IF (ISNULL(@numDefaultWareHouseID,0)>0)
		SET @numWareHouseID =@numDefaultWareHouseID;
END

      DECLARE @UOMConversionFactor AS DECIMAL(18,5)
      
      Select @UOMConversionFactor= ISNULL(dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)),0)
      FROM Item I where numItemCode=@numItemCode                                
 
 
declare @strSql as varchar(5000)
set @strSql=' With tblItem AS (                  
select I.numItemCode, vcItemName, txtItemDesc, charItemType,
ISNULL(CASE WHEN I.[charItemType]=''P'' 
			THEN CASE WHEN I.bitSerialized = 1 
					  THEN '+ convert(varchar(15),@UOMConversionFactor) + ' * monListPrice 
					  ELSE '+ convert(varchar(15),@UOMConversionFactor) + ' * W.[monWListPrice] 
				 END 
			ELSE '+ convert(varchar(15),@UOMConversionFactor) + ' * monListPrice 
	   END,0) monListPrice,
numItemClassification, bitTaxable, vcSKU, bitKitParent,              
I.numModifiedBy, (SELECT numItemImageId,vcPathForImage,vcPathForTImage,bitDefault,case when bitdefault=1  then -1 else isnull(intDisplayOrder,0) end as intDisplayOrder FROM ItemImages  WHERE numItemCode=I.numItemCode and bitIsImage = 1 order by case when bitdefault=1 then -1 else isnull(intDisplayOrder,0) end  asc FOR XML AUTO,ROOT(''Images''))  vcImages,
(SELECT vcPathForImage FROM ItemImages  WHERE numItemCode=I.numItemCode and bitIsImage = 0 order by intDisplayOrder  asc FOR XML AUTO,ROOT(''Documents''))  vcDocuments ,
 isnull(bitSerialized,0) as bitSerialized, vcModelID,                 
numItemGroup,                   
(isnull(sum(numOnHand),0) / '+ convert(varchar(15),@UOMConversionFactor) + ') as numOnHand,                    
sum(numOnOrder) as numOnOrder,                    
sum(numReorder)  as numReorder,                    
sum(numAllocation)  as numAllocation,                    
sum(numBackOrder)  as numBackOrder,              
isnull(fltWeight,0) as fltWeight,              
isnull(fltHeight,0) as fltHeight,              
isnull(fltWidth,0) as fltWidth,              
isnull(fltLength,0) as fltLength,              
case when charItemType=''P''then isnull(bitFreeShipping,0) else 1 end as bitFreeShipping,
Case When (case when charItemType=''P''then isnull(bitFreeShipping,0) else 1 end )=1 then ''Free shipping'' else ''Calculated at checkout'' end as Shipping,              
isnull(bitAllowBackOrder,0) as bitAllowBackOrder,bitShowInStock,bitShowQOnHand,isnull(numWareHouseItemID,0)  as numWareHouseItemID,    
(case when charItemType<>''S'' and charItemType<>''N'' then         
 (Case when '+ convert(varchar(15),@bitShowInStock) + '=1  then ( Case when numWareHouseItemID is null then ''<font color=red>Out Of Stock</font>'' when bitAllowBackOrder=1 then ''In Stock'' else         
 (Case when isnull(sum(numOnHand),0)>0 then ''In Stock''         
 else ''<font color=red>Out Of Stock</font>'' end ) end)    else '''' end) else '''' end) as InStock,
ISNULL(numSaleUnit,0) AS numUOM,
ISNULL(vcUnitName,'''') AS vcUOMName,
 '+ convert(varchar(15),@UOMConversionFactor) + ' AS UOMConversionFactor,
I.numCreatedBy,
I.[vcManufacturer],
(SELECT  COUNT(*) FROM  Review where vcReferenceId = CONVERT(VARCHAR(50) ,I.numItemCode) and numDomainId = I.numDomainId and numSiteId = '+ CONVERT(VARCHAR(20) , @numSiteId)  +' AND bitHide = 0 ) as ReviewCount ,
(SELECT  COUNT(*) FROM  Ratings where vcReferenceId = CONVERT(VARCHAR(50) ,I.numItemCode) and numDomainId = I.numDomainId and numSiteId = '+ CONVERT(VARCHAR(20) , @numSiteId)  +'  ) as RatingCount ,
(select top 1 vcMetaKeywords  from MetaTags where numReferenceID = I.numItemCode and tintMetaTagFor =2) as vcMetaKeywords,
(select top 1 vcMetaDescription  from MetaTags where numReferenceID = I.numItemCode and tintMetaTagFor =2) as vcMetaDescription ,
(SELECT vcCategoryName  FROM  dbo.Category WHERE numCategoryID =(SELECT TOP 1 numCategoryID FROM dbo.ItemCategory WHERE numItemID = I.numItemCode)) as vcCategoryName

from Item I                  
left join  WareHouseItems W                
on W.numItemID=I.numItemCode and numWareHouseID= '+ convert(varchar(15),@numWareHouseID) + '
left join  eCommerceDTL E          
on E.numDomainID=I.numDomainID     
LEFT JOIN UOM u ON u.numUOMId=I.numSaleUnit    
where I.numItemCode='+ convert(varchar(15),@numItemCode) + '             
group by I.numItemCode, vcItemName, txtItemDesc, charItemType, monListPrice,numItemClassification, bitTaxable, vcSKU, bitKitParent,           
I.numDomainID,I.numModifiedBy,  bitSerialized, vcModelID,numItemGroup, fltWeight,fltHeight,fltWidth,          
fltLength,bitFreeShipping,bitAllowBackOrder,bitShowInStock,bitShowQOnHand ,numWareHouseItemID,vcUnitName,I.numCreatedBy,W.[monWListPrice],I.[vcManufacturer],
numSaleUnit 

)'

--,
--


set @strSql=@strSql+' select numItemCode,vcItemName,txtItemDesc,charItemType,monListPrice,numItemClassification, bitTaxable, vcSKU, bitKitParent,                  
tblItem.numModifiedBy, vcImages,vcDocuments, bitSerialized, vcModelID, numItemGroup,numOnHand,numOnOrder,numReorder,numAllocation, 
numBackOrder, fltWeight, fltHeight, fltWidth, fltLength, bitFreeShipping,bitAllowBackOrder,bitShowInStock,
bitShowQOnHand,numWareHouseItemID,InStock,numUOM,vcUOMName,UOMConversionFactor,tblItem.numCreatedBy,vcManufacturer,ReviewCount,RatingCount,Shipping,isNull(vcMetaKeywords,'''') as vcMetaKeywords ,isNull(vcMetaDescription,'''') as vcMetaDescription,isNull(vcCategoryName,'''') as vcCategoryName'     

set @strSql=@strSql+ ' from tblItem'
PRINT @strSql
exec (@strSql)


declare @tintOrder as tinyint                                                  
declare @vcFormFieldName as varchar(50)                                                  
declare @vcListItemType as varchar(1)                                             
declare @vcAssociatedControlType varchar(10)                                                  
declare @numListID AS numeric(9)                                                  
declare @WhereCondition varchar(2000)                       
Declare @numFormFieldId as numeric  
DECLARE @vcFieldType CHAR(1)
                  
set @tintOrder=0                                                  
set @WhereCondition =''                 
                   
              
  CREATE TABLE #tempAvailableFields(numFormFieldId  numeric(9),vcFormFieldName NVARCHAR(50),
        vcFieldType CHAR(1),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),
        vcListItemType CHAR(1),intRowNum INT,vcItemValue VARCHAR(3000))
   
  INSERT INTO #tempAvailableFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
        ,numListID,vcListItemType,intRowNum)                         
            SELECT  Fld_id,REPLACE(Fld_label,' ','') AS vcFormFieldName,
                   CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType,
                    fld_type as vcAssociatedControlType,
                    ISNULL(C.numListID, 0) numListID,
                    CASE WHEN C.numListID > 0 THEN 'L'
                         ELSE ''
                    END vcListItemType,ROW_NUMBER() OVER (ORDER BY Fld_id asc) AS intRowNum
            FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
            WHERE   C.numDomainID = @numDomainId
                    AND GRP_ID IN (5)

     select top 1 @tintOrder=intRowNum,@numFormFieldId=numFormFieldId,@vcFormFieldName=vcFormFieldName,
				  @vcFieldType=vcFieldType,@vcAssociatedControlType=vcAssociatedControlType,@numListID=numListID,
				  @vcListItemType=vcListItemType
	from #tempAvailableFields order by intRowNum ASC
   
while @tintOrder>0                                                  
begin                                                  
    if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
    begin  
		UPDATE #tempAvailableFields SET vcItemValue=(SELECT Fld_Value FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
    end   
    else if @vcAssociatedControlType = 'CheckBox'
	begin      
		UPDATE #tempAvailableFields SET vcItemValue=(SELECT case when isnull(Fld_Value,0)=0 then 'No' when isnull(Fld_Value,0)=1 then 'Yes' END FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
   end                
   else if @vcAssociatedControlType = 'DateField'           
   begin   
		UPDATE #tempAvailableFields SET vcItemValue=(SELECT dbo.FormatedDateFromDate(Fld_Value,@numDomainId) FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
   end                
    else if @vcAssociatedControlType = 'SelectBox'           
   begin 
		UPDATE #tempAvailableFields SET vcItemValue=(SELECT L.vcData FROM CFW_FLD_Values_Item CFW left Join ListDetails L
			on L.numListItemID=CFW.Fld_Value                
			WHERE CFW.Fld_Id=@numFormFieldId AND CFW.RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
end          
               
 
    select top 1 @tintOrder=intRowNum,@numFormFieldId=numFormFieldId,@vcFormFieldName=vcFormFieldName,
				  @vcFieldType=vcFieldType,@vcAssociatedControlType=vcAssociatedControlType,@numListID=numListID,
				  @vcListItemType=vcListItemType
	from #tempAvailableFields WHERE intRowNum > @tintOrder order by intRowNum ASC
 
   if @@rowcount=0 set @tintOrder=0                                                  
end   


  
SELECT * FROM #tempAvailableFields

DROP TABLE #tempAvailableFields

--exec USP_ItemDetailsForEcomm @numItemCode=197611,@numWareHouseID=58,@numDomainID=1,@numSiteId=18
--exec USP_ItemDetailsForEcomm @numItemCode=735364,@numWareHouseID=1039,@numDomainID=156,@numSiteId=104
/****** Object:  StoredProcedure [dbo].[USP_OPPItemDetails]    Script Date: 09/01/2009 18:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_OPPItemDetails @numItemCode=31455,@OpportunityId=12387,@numDomainId=110
--created by anooop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppitemdetails')
DROP PROCEDURE usp_oppitemdetails
GO
CREATE PROCEDURE [dbo].[USP_OPPItemDetails]
    (
      @numUserCntID NUMERIC(9) = 0,
      @OpportunityId NUMERIC(9) = 0,
      @numDomainId NUMERIC(9),
      @ClientTimeZoneOffset  INT
    )
AS 
    BEGIN
        IF @OpportunityId >0
            BEGIN
                DECLARE @DivisionID AS NUMERIC(9)
                DECLARE @TaxPercentage AS FLOAT
                DECLARE @tintOppType AS TINYINT
                DECLARE @fld_Name AS VARCHAR(500)
                DECLARE @strSQL AS VARCHAR(8000)
                DECLARE @fld_ID AS NUMERIC(9)
                DECLARE @numBillCountry AS NUMERIC(9)
                DECLARE @numBillState AS NUMERIC(9)

                
                SELECT  @DivisionID = numDivisionID,
                        @tintOppType = tintOpptype
                FROM    OpportunityMaster
                WHERE   numOppId = @OpportunityId


               -- Calculate Tax based on Country and state
             /*  SELECT  @numBillState = ISNULL(numState, 0),
						@numBillCountry = ISNULL(numCountry, 0)
			   FROM     dbo.AddressDetails
			   WHERE    numDomainID = @numDomainID
						AND numRecordID = @DivisionID
						AND tintAddressOf = 2
						AND tintAddressType = 1
						AND bitIsPrimary=1
                SET @TaxPercentage = 0
                IF @numBillCountry > 0
                    AND @numBillState > 0 
                    BEGIN
                        IF @numBillState > 0 
                            BEGIN
                                IF EXISTS ( SELECT  COUNT(*)
                                            FROM    TaxDetails
                                            WHERE   numCountryID = @numBillCountry
                                                    AND numStateID = @numBillState
                                                    AND numDomainID = @numDomainID ) 
                                    SELECT  @TaxPercentage = decTaxPercentage
                                    FROM    TaxDetails
                                    WHERE   numCountryID = @numBillCountry
                                            AND numStateID = @numBillState
                                            AND numDomainID = @numDomainID
                                ELSE 
                                    SELECT  @TaxPercentage = decTaxPercentage
                                    FROM    TaxDetails
                                    WHERE   numCountryID = @numBillCountry
                                            AND numStateID = 0
                                            AND numDomainID = @numDomainID
                            END
                    END
                ELSE 
                    IF @numBillCountry > 0 
                        SELECT  @TaxPercentage = decTaxPercentage
                        FROM    TaxDetails
                        WHERE   numCountryID = @numBillCountry
                                AND numStateID = 0
                                AND numDomainID = @numDomainID
               
               */
               

SET @strSQL = ''

--------------Make Dynamic Query--------------
SET @strSQL = @strSQL + '
Select 
Opp.numoppitemtCode,
ISNULL(Opp.bitItemPriceApprovalRequired,0) AS bitItemPriceApprovalRequired,
OM.tintOppType,
Opp.numItemCode,
I.charItemType,
ISNULL(I.bitAsset,0) as bitAsset,
ISNULL(I.bitRental,0) as bitRental,
Opp.numOppId,
Opp.bitWorkOrder,
Opp.fltDiscount,
ISNULL(( SELECT numReturnID FROM   Returns WHERE  numOppItemCode = Opp.numoppitemtCode AND numOppId = Opp.numOppID), 0) AS numReturnID,
dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) AS ReturrnedQty,
[dbo].fn_GetReturnStatus(Opp.numoppitemtCode) AS ReturnStatus,
ISNULL(opp.numUOMId, 0) AS numUOM,
(SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
Opp.numUnitHour AS numOriginalUnitHour, 
isnull(M.vcPOppName,''Internal'') as Source,
numSourceID,
  CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END as bitKitParent,
(CASE WHEN (I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)THEN dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,isnull(Opp.numWarehouseItmsID,0),(CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END)) ELSE 0 END) AS bitBackOrder,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(Opp.numQtyShipped,0) numQtyShipped,ISNULL(Opp.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(OM.fltExchangeRate,0) AS fltExchangeRate,ISNULL(OM.numCurrencyID,0) AS numCurrencyID,ISNULL(OM.numDivisionID,0) AS numDivisionID,
isnull(i.numIncomeChartAcntId,0) as itemIncomeAccount,isnull(i.numAssetChartAcntId,0) as itemInventoryAsset,isnull(i.numCOGsChartAcntId,0) as itemCoGs,
ISNULL(Opp.numRentalIN,0) as numRentalIN,
ISNULL(Opp.numRentalLost,0) as numRentalLost,
ISNULL(Opp.numRentalOut,0) as numRentalOut,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(bitFreeShipping,0) AS [IsFreeShipping],Opp.bitDiscountType,isnull(Opp.monLandedCost,0) as monLandedCost, '

--	IF @fld_Name = 'vcPathForTImage' 
		SET @strSQL = @strSQL + 'Opp.vcPathForTImage,'
--	ELSE IF @fld_Name = 'vcItemName' 
		SET @strSQL = @strSQL + 'Opp.vcItemName,'
--	ELSE IF @fld_Name = 'vcModelID,' 
		SET @strSQL = @strSQL + 'Opp.vcModelID,'
--	ELSE IF @fld_Name = 'vcWareHouse' 
		SET @strSQL = @strSQL + 'vcWarehouse,'
		SET @strSQL = @strSQL + 'WItems.numOnHand,isnull(WItems.numAllocation,0) as numAllocation,'
		SET @strSQL = @strSQL + 'WL.vcLocation,'
--	ELSE IF @fld_Name = 'ItemType' 
		SET @strSQL = @strSQL + 'Opp.vcType ItemType,Opp.vcType,'
--	ELSE IF @fld_Name = 'vcItemDesc' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.vcItemDesc, '''') vcItemDesc,'
--	ELSE IF @fld_Name = 'Attributes' 
		SET @strSQL = @strSQL + 'Opp.vcAttributes,' --dbo.fn_GetAttributes(Opp.numWarehouseItmsID, I.bitSerialized) AS Attributes,
--	ELSE IF @fld_Name = 'bitDropShip' 
		SET @strSQL = @strSQL + 'ISNULL(bitDropShip, 0) as DropShip,CASE ISNULL(bitDropShip, 0) WHEN 1 THEN ''Yes'' ELSE '''' END AS bitDropShip,'
--	ELSE IF @fld_Name = 'numUnitHour' 
		SET @strSQL = @strSQL + 'CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,om.numDomainId, ISNULL(opp.numUOMId, 0))* Opp.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,'
--	ELSE IF @fld_Name = 'monPrice' 
		SET @strSQL = @strSQL + 'Opp.monPrice,'
--	ELSE IF @fld_Name = 'monTotAmount' 
		SET @strSQL = @strSQL + 'Opp.monTotAmount,'
--	ELSE IF @fld_Name = 'vcSKU' 
		SET @strSQL = @strSQL + 'ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'''')) [vcSKU],'
--	ELSE IF @fld_Name = 'vcManufacturer' 
		SET @strSQL = @strSQL + 'Opp.vcManufacturer,'
--	ELSE IF @fld_Name = 'vcItemClassification' 
		SET @strSQL = @strSQL + ' dbo.[fn_GetListItemName](I.[numItemClassification]) numItemClassification,'
--	ELSE IF @fld_Name = 'numItemGroup' 
		SET @strSQL = @strSQL + '( SELECT    [vcItemGroup] FROM      [ItemGroups] WHERE     [numItemGroupID] = I.numItemGroup ) numItemGroup,'
--	ELSE IF @fld_Name = 'vcUOMName' 
		SET @strSQL = @strSQL + 'ISNULL(u.vcUnitName, '''') numUOMId,'
--	ELSE IF @fld_Name = 'fltWeight' 
		SET @strSQL = @strSQL + 'I.fltWeight,'
--	ELSE IF @fld_Name = 'fltHeight' 
		SET @strSQL = @strSQL + 'I.fltHeight,'
--	ELSE IF @fld_Name = 'fltWidth' 
		SET @strSQL = @strSQL + 'I.fltWidth,'
--	ELSE IF @fld_Name = 'fltLength' 
		SET @strSQL = @strSQL + 'I.fltLength,'
--	ELSE IF @fld_Name = 'numBarCodeId' 
		SET @strSQL = @strSQL + 'I.numBarCodeId,'
		SET @strSQL = @strSQL + 'I.IsArchieve,'
--	ELSE IF @fld_Name = 'vcWorkOrderStatus' 
		SET @strSQL = @strSQL + 'CASE WHEN ISNULL(Opp.bitWorkOrder, 0) = 1
				 THEN ( SELECT  dbo.GetListIemName(numWOStatus)
								+ CASE WHEN ISNULL(numWOStatus, 0) = 23184
									   THEN '' - ''
											+ CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo),
														  '''') + '',''
											+ CONVERT(VARCHAR(20), DATEADD(minute, '+ CONVERT(VARCHAR(15),-@ClientTimeZoneOffset)  +', bintCompletedDate)) AS VARCHAR(100))
									   ELSE ''''
								  END
						FROM    WorkOrder
						WHERE   ISNULL(numOppId, 0) = Opp.numOppId
								AND numItemCode = Opp.numItemCode AND ISNULL(numParentWOID,0)=0
					  )
				 ELSE ''''
			END AS vcWorkOrderStatus,'
--	ELSE IF @fld_Name = 'DiscAmt' 
		SET @strSQL = @strSQL + 'CASE WHEN Opp.bitDiscountType = 0
				 THEN CAST(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount AS VARCHAR(10)) + '' (''
					  + CAST(Opp.fltDiscount AS VARCHAR(10)) + ''%)''
				 ELSE CAST(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount AS VARCHAR(10))
			END AS DiscAmt,'	
--	ELSE IF @fld_Name = 'numProjectID' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectID, 0) numProjectID,
		 CASE WHEN ( SELECT TOP 1
								ISNULL(OBI.numOppBizDocItemID, 0)
						FROM    dbo.OpportunityBizDocs OBD
								INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID
						WHERE   OBI.numOppItemID = Opp.numoppitemtCode
								AND ISNULL(OBD.bitAuthoritativeBizDocs, 0) = 1
								and isnull(OBD.numOppID,0)=OM.numOppID
					  ) > 0 THEN 1
				 ELSE 0
			END AS bitItemAddedToAuthBizDoc,'	
--	ELSE IF @fld_Name = 'numClassID' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.numClassID, 0) numClassID,'	
--	ELSE IF @fld_Name = 'vcProjectStageName' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectStageID, 0) numProjectStageID,
				CASE WHEN OPP.numProjectStageID > 0
				 THEN dbo.fn_GetProjectStageName(OPP.numProjectId,
												 OPP.numProjectStageID)
				 WHEN OPP.numProjectStageID = -1 THEN ''T&E''
				 WHEN OPP.numProjectId > 0
				 THEN CASE WHEN OM.tintOppType = 1 THEN ''S.O.''
						   ELSE ''P.O.''
					  END
				 ELSE ''-''
			END AS vcProjectStageName,'	
	        
	
	

--------------Add custom fields to query----------------
SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM View_DynamicCustomColumns
WHERE 
grp_id = 5 AND numDomainID=@numDomainId
AND numFormId=26 AND bitCustom=1 AND numUserCntID = @numUserCntID ORDER BY numFieldId
WHILE @fld_ID > 0
BEGIN ------------------

	 PRINT @fld_Name
	
	       SET @strSQL = @strSQL + 'dbo.GetCustFldValueItem('
                                    + CONVERT(VARCHAR(15), @fld_ID)
                                    + ',Opp.numItemCode) as ''' + @fld_Name + ''','
       

SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM View_DynamicCustomColumns
WHERE 
grp_id = 5 AND numDomainID=@numDomainId
AND numFormId=26 AND bitCustom=1  AND numUserCntID = @numUserCntID AND numFieldId > @fld_ID ORDER BY numFieldId
 IF @@ROWCOUNT=0
 SET @fld_ID = 0
END ------------------


                                
--Purchase Opportunity ?
IF @tintOppType = 2 
    BEGIN
        SET @strSQL =  @strSQL + 'ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo,
		  SUBSTRING(( SELECT  '', '' + vcSerialNo
                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                THEN '' (''
                                    + CONVERT(VARCHAR(15), oppI.numQty)
                                    + '')''
                                ELSE ''''
                            END
                FROM    OppWarehouseSerializedItem oppI
                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                WHERE   oppI.numOppID = Opp.numOppId
                        AND oppI.numOppItemID = Opp.numoppitemtCode
                ORDER BY vcSerialNo
                FOR
                XML PATH('''')
                ), 3, 200000) AS vcSerialLotNo,
		  CAST(Opp.numUnitHour AS NUMERIC(9, 0)) numQty,
		  (SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  isnull(Opp.numWarehouseItmsID,0) as numWarehouseItmsID,'
	END
ELSE
	BEGIN	
        SET @strSQL =  @strSQL + ' ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo,
		   SUBSTRING(( SELECT  '', '' + vcSerialNo
                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                THEN '' (''
                                    + CONVERT(VARCHAR(15), oppI.numQty)
                                    + '')''
                                ELSE ''''
                            END
                FROM    OppWarehouseSerializedItem oppI
                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                WHERE   oppI.numOppID = Opp.numOppId
                        AND oppI.numOppItemID = Opp.numoppitemtCode
                ORDER BY vcSerialNo
                FOR
                XML PATH('''')
                ), 3, 200000) AS vcSerialLotNo,
		   CAST(Opp.numUnitHour AS NUMERIC(9, 0)) numQty,
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  isnull(Opp.numWarehouseItmsID,0) as numWarehouseItmsID,'
	END


        
 SET @strSQL = left(@strSQL,len(@strSQL)-1) + '   
        FROM    OpportunityItems Opp
        LEFT JOIN OpportunityMaster OM ON Opp.numOppID = oM.numOppID
        JOIN item I ON Opp.numItemCode = i.numItemcode
        LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = Opp.numWarehouseItmsID
        LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
        LEFT JOIN WarehouseLocation WL ON WL.numWLocationID = WItems.numWLocationID
        LEFT JOIN OpportunityMaster M ON Opp.numSourceID = M.numOppID
        LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId
WHERE   om.numDomainId = '+ CONVERT(VARCHAR(10),@numDomainId) + '
        AND Opp.numOppId = '+ CONVERT(VARCHAR(15),@OpportunityId) +'
ORDER BY numoppitemtCode ASC '
  
PRINT @strSQL;
EXEC (@strSQL);
SELECT  vcSerialNo,
       vcComments AS Comments,
        numOppItemID AS numoppitemtCode,
        W.numWarehouseItmsDTLID,
        numWarehouseItmsID AS numWItmsID,
        dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                             1) Attributes
FROM    WareHouseItmsDTL W
        JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
WHERE   numOppID = @OpportunityId


CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,intVisible int,vcFieldDataType CHAR(1))


IF
	(
		SELECT 
			COUNT(*)
		FROM 
			View_DynamicColumns DC 
		WHERE 
			DC.numFormId=26 and DC.numDomainID=@numDomainID AND numUserCntID = @numUserCntID AND
			ISNULL(DC.bitSettingField,0)=1 AND ISNULL(DC.bitDeleted,0)=0 AND ISNULL(DC.bitCustom,0)=0) > 0
	OR
	(
		SELECT 
			COUNT(*)
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=26 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0 AND ISNULL(bitCustom,0)=1
	) > 0
BEGIN	

INSERT INTO #tempForm
select isnull(DC.tintRow,0)+1 as tintOrder,DDF.vcDbColumnName,DDF.vcOrigDbColumnName,ISNULL(DDF.vcCultureFieldName,DDF.vcFieldName),                  
 DDF.vcAssociatedControlType,DDF.vcListItemType,DDF.numListID,DDF.vcLookBackTableName                                                 
,DDF.bitCustom,DDF.numFieldId,DDF.bitAllowSorting,DDF.bitInlineEdit,
DDF.bitIsRequired,DDF.bitIsEmail,DDF.bitIsAlphaNumeric,DDF.bitIsNumeric,DDF.bitIsLengthValidation,
DDF.intMaxLength,DDF.intMinLength,DDF.bitFieldMessage,DDF.vcFieldMessage vcFieldMessage,DDF.ListRelID,DDF.intColumnWidth,Case when DC.numFieldID is null then 0 else 1 end,DDF.vcFieldDataType
 FROM View_DynamicDefaultColumns DDF left join View_DynamicColumns DC on DC.numFormId=26 and DDF.numFieldId=DC.numFieldId and DC.numUserCntID=@numUserCntID and DC.numDomainID=@numDomainID and numRelCntType=0 AND tintPageType=1 
 where DDF.numFormId=26 and DDF.numDomainID=@numDomainID AND ISNULL(DDF.bitSettingField,0)=1 AND ISNULL(DDF.bitDeleted,0)=0 AND ISNULL(DDF.bitCustom,0)=0
       
 UNION
    
    select tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
 from View_DynamicCustomColumns
 where numFormId=26 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0
 AND ISNULL(bitCustom,0)=1

END
ELSE
BEGIN
	--Check if Master Form Configuration is created by administrator if NoColumns=0
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 26 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END

	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName, vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
			bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,1,vcFieldDataType
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=26 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
			 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
			,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=26 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
END


select * from #tempForm order by tintOrder

drop table #tempForm

/*
                        SET @strSQL = 'select  Opp.numoppitemtCode,Opp.numOppId,
                               Opp.numItemCode,                                    
                               Opp.vcItemName,                                                                    
                               ISNULL(Opp.vcItemDesc,'''') as [Desc],                                                                    
                               Opp.vcType ItemType,                                   
                               Opp.vcPathForTImage,
                               Opp.vcModelID,
							    CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),Opp.numItemCode,om.numDomainId,ISNULL(opp.numUOMId,0)) * Opp.numUnitHour) as numeric(18,2)) as numUnitHour,   
                               Opp.monPrice,                                                                    
                               Opp.monTotAmount,                                                     
							   ISNULL(Opp.vcItemDesc,'''') vcItemDesc,
							   Opp.vcManufacturer,
							   I.charItemType,
							   I.bitKitParent,
							   I.bitAssembly,
							   I.vcSKU,                                   
							   I.numBarCodeId,
							   dbo.[fn_GetListItemName](I.[numItemClassification]) vcClassification,
							   (SELECT [vcItemGroup] FROM [ItemGroups] WHERE [numItemGroupID] =I.numItemGroup) vcItemGroup,
							   ISNULL(I.vcUnitofMeasure,'''') vcUnitofMeasure,
							   I.fltWeight,I.fltHeight,I.fltWidth,I.fltLength,
							   WItems.numWarehouseID,
                               Opp.numWarehouseItmsID,isnull(Opp.numToWarehouseItemID,0) numToWarehouseItemID,bitDropShip DropShip, CASE ISNULL(bitDropShip,0) when 1 then ''Yes'' else '''' end as vcDropShip,
                               0 as Op_Flag,isnull(Opp.bitDiscountType,0) as bitDiscountType,isnull(Opp.fltDiscount,0) as fltDiscount,isnull(monTotAmtBefDiscount,0) as monTotAmtBefDiscount,                                    
                               vcWarehouse as Warehouse,I.bitSerialized,isnull(I.bitLotNo,0) as bitLotNo,vcAttributes, 
                               dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) as ReturrnedQty,
                               [dbo].fn_GetReturnStatus(Opp.numoppitemtCode) as ReturnStatus,
							   isnull((SELECT numReturnID FROM Returns WHERE numOppItemCode=Opp.numoppitemtCode and numOppId=Opp.numOppID),0) as numReturnID,
                               isnull(v.monCost,0) as VendorCost,
                               bitTaxable,
                               case when bitTaxable =0 then 0 when bitTaxable=1 then '
                            + CONVERT(VARCHAR(15), @TaxPercentage)
                            + ' end as Tax,                                                
                                   isnull(M.vcPOppName,''Internal'') as Source,dbo.fn_GetAttributes(Opp.numWarehouseItmsID,I.bitSerialized) as Attributes  ,numSourceID '
                            + CASE WHEN @strSQL = '' THEN ''
                                   ELSE ',' + @strSQL
                              END
                            + ',isnull(opp.numUOMId,0) AS numUOM,ISNULL(u.vcUnitName,'''') vcUOMName,dbo.fn_UOMConversion(opp.numUOMId,Opp.numItemCode,om.numDomainId,null) AS UOMConversionFactor
                          ,case when Opp.bitDiscountType=0 then  cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) +  '' ('' + CAST(Opp.fltDiscount AS VARCHAR(10)) +  ''%)'' 
                          ELSE cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) end as DiscAmt
                           ,isnull(I.numVendorID,0) numVendorID,OM.tintOppType,cast(Opp.numUnitHour as numeric(9,0)) as numQty,SUBSTRING(
(SELECT '','' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN '' ('' + CONVERT(VARCHAR(15),whi.numQty) + '')'' ELSE '''' end 
FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=Opp.numOppId and oppI.numOppItemID=Opp.numoppitemtCode 
ORDER BY vcSerialNo FOR XML PATH('''')),2,200000) AS SerialLotNo, ISNULL(Opp.bitWorkOrder,0) as bitWorkOrder,
CASE WHEN ISNULL(Opp.bitWorkOrder,0)=1 then (select dbo.GetListIemName(numWOStatus) + CASE WHEN isnull(numWOStatus,0)=23184 THEN
'' - '' + CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo), '''')+'',''+convert(varchar(20),dateadd(minute,' + CONVERT(VARCHAR(15),-@ClientTimeZoneOffset) + ',bintCompletedDate)) AS VARCHAR(100)) ELSE '''' end
 from WorkOrder where isnull(numOppId,0)=Opp.numOppId and numItemCode=Opp.numItemCode) else '''' end as vcWorkOrderStatus,
 ISNULL(Opp.numVendorWareHouse,0) as numVendorWareHouse,ISNULL(Opp.numShipmentMethod,0) as numShipmentMethod,ISNULL(Opp.numSOVendorId,0) as numSOVendorId,Opp.numUnitHour as numOriginalUnitHour,
 isnull(Opp.numProjectID,0) numProjectID,isnull(Opp.numClassID,0) numClassID,isnull(Opp.numProjectStageID,0) numProjectStageID,Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) when OPP.numProjectStageID=-1 then ''T&E'' When OPP.numProjectId>0 then case when OM.tintOppType=1 then ''S.O.'' else ''P.O.'' end else ''-'' end as vcProjectStageName,
 CASE WHEN (SELECT TOP 1 ISNULL(OBI.numOppBizDocItemID,0) FROM dbo.OpportunityBizDocs OBD INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID WHERE OBI.numOppItemID = Opp.numoppitemtCode AND ISNULL(OBD.bitAuthoritativeBizDocs,0)=1 ) > 0
      THEN 1 ELSE 0 END AS bitItemAddedToAuthBizDoc,isnull(I.numItemClass,0) as numItemClass
                              from OpportunityItems Opp                    
                               left join OpportunityMaster OM                                                            
                                  on Opp.numOppID=oM.numOppID                                                                     
                                 join item I                                                                    
                                  on Opp.numItemCode=i.numItemcode                                            
                                left join Vendor v 
									on v.numVendorID= I.numVendorID and I.numItemCode =v.numItemCode
                               left join  WareHouseItems  WItems                                        
                               on   WItems.numWareHouseItemID= Opp.numWarehouseItmsID                                            
                               left join  Warehouses  W                                            
                                  on   W.numWarehouseID= WItems.numWarehouseID                                         
                                  left join OpportunityMaster M                                                            
                                  on Opp.numSourceID=M.numOppID  
                                  LEFT JOIN  UOM u ON u.numUOMId = opp.numUOMId  
                                  where om.numDomainId ='
                            + CONVERT(VARCHAR(15), @numDomainId)
                            + ' and Opp.numOppId='+ CONVERT(VARCHAR(15), @OpportunityId) +  
                            + CASE WHEN @numItemCode > 0 THEN ' AND Opp.numOppItemTCode = ' + CONVERT(VARCHAR(15), @numItemCode)
                                   ELSE ''
                              END
                            +
                            ' order by numoppitemtCode ASC '
                        PRINT @strSQL
                        EXEC ( @strSQL
                            )
                        SELECT  vcSerialNo,
                                vcComments AS Comments,
                                numOppItemID AS numoppitemtCode,
                                W.numWarehouseItmsDTLID,
                                numWarehouseItmsID AS numWItmsID,
                                dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                                                     1) Attributes
                        FROM    WareHouseItmsDTL W
                                JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
                        WHERE   numOppID = @OpportunityId
*/
      /*              END
                ELSE 
                    BEGIN
                        SELECT  @numDomainID = numDomainId
                        FROM    OpportunityMaster
                        WHERE   numOppID = @OpportunityId
                        SET @strSQL = ''
                        SELECT TOP 1
                                @fld_ID = Fld_ID,
                                @fld_Name = Fld_label 
                        FROM    CFW_Fld_Master
                        WHERE   grp_id = 5
                                AND numDomainID = @numDomainID
                        WHILE @fld_ID > 0
                            BEGIN
                                SET @strSQL =@strSQL + 'dbo.GetCustFldValueItem('
                                    + CONVERT(VARCHAR(15), @fld_ID)
                                    + ',Opp.numItemCode) as ''' + @fld_Name + ''''
                                SELECT TOP 1
                                        @fld_ID = Fld_ID,
                                        @fld_Name = Fld_label
                                FROM    CFW_Fld_Master
                                WHERE   grp_id = 5
                                        AND numDomainID = @numDomainID
                                        AND Fld_ID > @fld_ID
                                IF @@ROWCOUNT = 0 
                                    SET @fld_ID = 0
                                IF @fld_ID <> 0 
                                    SET @strSQL = @strSQL + ','
                            END
                        SET @strSQL = 'select  Opp.numoppitemtCode,Opp.numOppId,                                       
                                Opp.numItemCode,                                    
                               Opp.vcItemName,                                                                    
                               ISNULL(Opp.vcItemDesc,'''') as [Desc],                                                                    
                               Opp.vcType ItemType,                                   
                               Opp.vcPathForTImage,
                               Opp.vcModelID,
                              CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),Opp.numItemCode,om.numDomainId,ISNULL(opp.numUOMId,0)) * Opp.numUnitHour) as numeric(18,2)) as numUnitHour,   
							   Opp.monPrice,                                                                    
                               Opp.monTotAmount,                                                     
							   ISNULL(Opp.vcItemDesc,'''') vcItemDesc,
							   Opp.vcManufacturer,
							   I.charItemType,
							   I.bitKitParent,
							   I.bitAssembly,
							   I.vcSKU,
							   I.numBarCodeId,
							   dbo.[fn_GetListItemName](I.[numItemClassification]) vcClassification,
							   (SELECT [vcItemGroup] FROM [ItemGroups] WHERE [numItemGroupID] =I.numItemGroup) vcItemGroup,
							   ISNULL(I.vcUnitofMeasure,'''') vcUnitofMeasure,
							   I.fltWeight,I.fltHeight,I.fltWidth,I.fltLength,
							   WItems.numWarehouseID,
                               Opp.numWarehouseItmsID,isnull(Opp.numToWarehouseItemID,0) numToWarehouseItemID,bitDropShip DropShip, CASE ISNULL(bitDropShip,0) when 1 then ''Yes'' else '''' end as vcDropShip,
                               0 as Op_Flag,isnull(Opp.bitDiscountType,0) bitDiscountType,isnull(Opp.fltDiscount,0) fltDiscount,isnull(monTotAmtBefDiscount,0) monTotAmtBefDiscount,                                           
                               vcWarehouse as Warehouse,I.bitSerialized,isnull(I.bitLotNo,0) as bitLotNo,vcAttributes,
                               dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) as ReturrnedQty,
                               [dbo].fn_GetReturnStatus(Opp.numoppitemtCode) as ReturnStatus,
							   isnull((SELECT numReturnID FROM Returns WHERE numOppItemCode=Opp.numoppitemtCode and numOppId=Opp.numOppID),0) as numReturnID,
                               isnull(v.monCost,0) as VendorCost,
                               bitTaxable,
                               case when bitTaxable =0 then 0 when bitTaxable=1 then '
                            + CONVERT(VARCHAR(15), @TaxPercentage)
                            + ' end as Tax,                                                
                                   isnull(m.vcPOppName,''Internal'') as Source,dbo.fn_GetAttributes(Opp.numWarehouseItmsID,I.bitSerialized) as Attributes ,numSourceID '
                            + CASE WHEN @strSQL = '' THEN ''
                                   ELSE ',' + @strSQL
                              END
                            + ',isnull(opp.numUOMId,0) AS numUOM,ISNULL(u.vcUnitName,'''') vcUOMName,dbo.fn_UOMConversion(opp.numUOMId,Opp.numItemCode,om.numDomainId,null) AS UOMConversionFactor
                          ,case when Opp.bitDiscountType=0 then  cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) +  '' ('' + CAST(Opp.fltDiscount AS VARCHAR(10)) +  ''%)'' 
                          ELSE cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) end as DiscAmt
                         ,isnull(I.numVendorID,0) numVendorID,OM.tintOppType,cast(Opp.numUnitHour as numeric(9,0)) numQty,SUBSTRING(
(SELECT '','' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN '' ('' + CONVERT(VARCHAR(15),whi.numQty) + '')'' ELSE '''' end 
FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=Opp.numOppId and oppI.numOppItemID=Opp.numoppitemtCode 
ORDER BY vcSerialNo FOR XML PATH('''')),2,200000) AS SerialLotNo, ISNULL(Opp.bitWorkOrder,0) as bitWorkOrder,
CASE WHEN ISNULL(Opp.bitWorkOrder,0)=1 then (select dbo.GetListIemName(numWOStatus) + CASE WHEN isnull(numWOStatus,0)=23184 THEN
'' - '' + CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo), '''')+'',''+convert(varchar(20),dateadd(minute,'+ CONVERT(VARCHAR(15),-@ClientTimeZoneOffset) +',bintCompletedDate)) AS VARCHAR(100)) ELSE '''' end
 from WorkOrder where isnull(numOppId,0)=Opp.numOppId and numItemCode=Opp.numItemCode) else '''' end as vcWorkOrderStatus,
 ISNULL(Opp.numVendorWareHouse,0) as numVendorWareHouse,ISNULL(Opp.numShipmentMethod,0) as numShipmentMethod,ISNULL(Opp.numSOVendorId,0) as numSOVendorId,Opp.numUnitHour as numOriginalUnitHour,
 isnull(Opp.numProjectID,0) numProjectID,isnull(Opp.numClassID,0) numClassID,isnull(Opp.numProjectStageID,0) numProjectStageID,Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) when OPP.numProjectStageID=-1 then ''T&E'' When OPP.numProjectId>0 then case when OM.tintOppType=1 then ''S.O.'' else ''P.O.'' end else ''-'' end as vcProjectStageName,
 CASE WHEN (SELECT TOP 1 ISNULL(OBI.numOppBizDocItemID,0) FROM dbo.OpportunityBizDocs OBD INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID WHERE OBI.numOppItemID = Opp.numoppitemtCode AND ISNULL(OBD.bitAuthoritativeBizDocs,0)=1 ) > 0
 THEN 1 ELSE 0 END AS bitItemAddedToAuthBizDoc,isnull(I.numItemClass,0) as numItemClass
              from OpportunityItems Opp                    
                               left join OpportunityMaster OM                                                            
                                  on Opp.numOppID=oM.numOppID                                    
                                 join item I                                                                    
                                  on Opp.numItemCode=i.numItemcode   
                                  left join Vendor v 
									on v.numVendorID= I.numVendorID and I.numItemCode =v.numItemCode
                                left join  WareHouseItems  WItems                                        
                                on   WItems.numWareHouseItemID= Opp.numWarehouseItmsID                                            
                                left join  Warehouses  W                                            
                                  on   W.numWarehouseID= WItems.numWarehouseID                                         
                                  left join OpportunityMaster M                                                            
                                  on Opp.numSourceID=M.numOppID                      
                                        LEFT JOIN  UOM u ON u.numUOMId = opp.numUOMId
                                  where om.numDomainId ='
                            + CONVERT(VARCHAR(15), @numDomainId)
                            + ' and Opp.numOppId='+ CONVERT(VARCHAR(15), @OpportunityId) +  
                            + CASE WHEN @numItemCode > 0 THEN ' AND Opp.numOppItemTCode = ' + CONVERT(VARCHAR(15), @numItemCode)
                                   ELSE ''
                              END
                            +
                            ' order by numoppitemtCode ASC '
                        PRINT @strSQL
                        EXEC ( @strSQL
                            )
                        SELECT  vcSerialNo,
                                vcComments AS Comments,
                                numOppItemID AS numoppitemtCode,
                                W.numWarehouseItmsDTLID,
                                numWarehouseItmsID AS numWItmsID,
                                dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                                                     1) Attributes
                        FROM    OppWarehouseSerializedItem O
                                LEFT JOIN WareHouseItmsDTL W ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
                        WHERE   numOppID = @OpportunityId
                    END
            */
            END
            
    END
/****** Object:  StoredProcedure [dbo].[USP_TicklerActItems]    Script Date: 07/26/2008 16:21:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By Anoop jayaraj                                                        
                                                    
-- Modified by Tarun Juneja                                                    
-- date 26-08-2006                                                    
-- Reason:- Enhancement In Tickler                                                    
                                                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_tickleractitems')
DROP PROCEDURE usp_tickleractitems
GO
CREATE PROCEDURE [dbo].[USP_TicklerActItems]                                                                        
@numUserCntID as numeric(9)=null,                                                                        
@numDomainID as numeric(9)=null,                                                                        
@startDate as datetime,                                                                        
@endDate as datetime,                                                                    
@ClientTimeZoneOffset Int,  --Added by Debasish to enable calculation of date according to client machine                                                                      
@bitFilterRecord bit=0,
@columnName varchar(50),
@columnSortOrder varchar(50),
@vcBProcessValue varchar(500)
                                                  
As                                                                         
  
DECLARE @vcSelectedEmployess VARCHAR(100)
DECLARE @numCriteria INT
DECLARE @vcActionTypes VARCHAR(100)

SELECT 
	@vcSelectedEmployess = vcSelectedEmployee,
	@numCriteria = numCriteria,
	@vcActionTypes = vcActionTypes
FROM 
	TicklerListFilterConfiguration
WHERE
	numDomainID = @numDomainID AND
	numUserCntID = @numUserCntID


Create table #tempRecords (numContactID numeric(9),numDivisionID numeric(9),tintCRMType tinyint,
Id numeric(9),bitTask varchar(100),Startdate datetime,EndTime datetime,
itemDesc text,[Name] varchar(200),vcFirstName varchar(100),vcLastName varchar(100),numPhone varchar(15),numPhoneExtension varchar(7),
[Phone] varchar(30),vcCompanyName varchar(100),vcEmail varchar(50),Task varchar(100),Activity varchar(500),
Status varchar(100),numCreatedBy numeric(9),numTerId numeric(9),numAssignedTo varchar(1000),numAssignedBy varchar(50),
 caseid numeric(9),vcCasenumber varchar(50),casetimeId numeric(9),caseExpId numeric(9),type int,itemid varchar(50),
changekey varchar(50),DueDate varchar(50),bitFollowUpAnyTime bit,numNoOfEmployeesId varchar(50),vcComPhone varchar(50),numFollowUpStatus varchar(200),vcColorScheme VARCHAR(50),
Slp_Name varchar(100),intTotalProgress INT,vcPoppName varchar(100), numOppId NUMERIC(18,0),numBusinessProcessID numeric(18,0) )                                                         
 
declare @strSql as varchar(8000)                                                            
declare @strSql1 as varchar(8000)                                                            
declare @strSql2 as varchar(8000)                                                            
 
   -------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

--select top 1 @vcCSOrigDbCOlumnName=DFM.vcOrigDbCOlumnName,@vcCSLookBackTableName=DFM.vcLookBackTableName from DycFieldColorScheme DFCS join DycFieldMaster DFM on 
--DFCS.numModuleID=DFM.numModuleID and DFCS.numFieldID=DFM.numFieldID
--where DFCS.numDomainID=@numDomainID and DFM.numModuleID=1

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=43 AND DFCS.numFormID=43 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
----------------------------                   


SET @strSql = 'insert into #tempRecords
Select * from (                                                    
 SELECT Addc.numContactID,Addc.numDivisionID,Div.tintCRMType,Comm.numCommId AS Id,convert(varchar(15),bitTask) as bitTask,                                               
 DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtStartTime) as Startdate,                        
 DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtEndTime) as EndTime ,                                                                                                                                                                  
 --DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtStartTime) AS CloseDate,                                                                         
 Comm.textDetails AS itemDesc,                                                                                                               
 AddC.vcFirstName + '' '' + AddC.vcLastName  as [Name], 
 AddC.vcFirstName,AddC.vcLastName,Addc.numPhone,AddC.numPhoneExtension,                               
 case when Addc.numPhone<>'''' then + AddC.numPhone +case when AddC.numPhoneExtension<>'''' then '' - '' + AddC.numPhoneExtension else '''' end  else '''' end as [Phone],                                                                       
 Comp.vcCompanyName As vcCompanyName , --changed by bharat so that the complete name is displayed                                                                
 AddC.vcEmail As vcEmail ,                                                                         
-- case when Comm.bitTask=971 then  ''Communication'' when Comm.bitTask=2 then  ''Opportunity''       
--when Comm.bitTask = 972 then  ''Task'' when Comm.bitTask=973 then  ''Notes'' When Comm.bitTask = 974 then        
--''Follow-up'' else ''Unknown'' end         
dbo.GetListIemName(Comm.bitTask)AS Task,                
 listdetailsActivity.VcData As Activity ,                                                    
 listdetailsStatus.VcData As Status   ,  -- Priority column                                                                                             
 Comm.numCreatedBy,                                                                                                             
 Div.numTerId,                                           
-- case When Len( dbo.fn_GetContactName(numAssign) ) > 12                                           
--  Then Substring( dbo.fn_GetContactName(numAssign) , 0  ,  12 ) + ''..''                                           
--  Else dbo.fn_GetContactName(numAssign)                                           
-- end                                          
-- +                                          
-- '' / ''                                             
-- +                                              
-- case                                                
--  When Len( dbo.fn_GetContactName(Comm.numAssignedBy) ) > 12                                           
--  Then Substring( dbo.fn_GetContactName(Comm.numAssignedBy) , 0  ,  12 ) + ''..''                                        Else dbo.fn_GetContactName(Comm.numAssignedBy)                                          
-- end  

Substring( 
dbo.fn_GetContactName(numAssign) + 
ISNULL((SELECT SUBSTRING((SELECT '','' + dbo.fn_GetContactName(numContactId) FROM CommunicationAttendees WHERE numCommId=Comm.numCommId FOR XML PATH('''')),1,200000)),''''),0,50)
As numAssignedTo , 
dbo.fn_GetContactName(Comm.numAssignedBy) AS numAssignedBy,        
convert(varchar(50),comm.caseid) as caseid,                          
(select top 1 vcCaseNumber from cases where cases.numcaseid= comm.caseid )as vcCasenumber,                          
comm.casetimeId,                          
comm.caseExpId ,                        
0 as type   ,                    
'''' as itemid ,                    
'''' as changekey ,cast(Null as datetime) as DueDate ,bitFollowUpAnyTime,
dbo.GetListIemName(Comp.numNoOfEmployeesId) AS numNoOfEmployeesId,Div.vcComPhone,
dbo.GetListIemName(Div.numFollowUpStatus) AS numFollowUpStatus'

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	set @strSql=@strSql+',tCS.vcColorScheme'
ELSE
	set @strSql=@strSql+','''' as vcColorScheme'

SET @strSql =@strSql + ' ,ISNULL(Slp_Name,'''') AS Slp_Name,ISNULL(intTotalProgress,0) AS [intTotalProgress],ISNULL(vcPoppName,'''') AS [vcPoppName], ISNULL(Opp.numOppId,0) AS [numOppId],ISNULL(Opp.numBusinessProcessID,0) AS numBusinessProcessID'

SET @strSql =@strSql + ' FROM  Communication Comm                              
 JOIN AdditionalContactsInformation AddC                                                  
 ON Comm.numContactId = AddC.numContactId                                  
 JOIN DivisionMaster Div                                                                         
 ON AddC.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                                                                         
 ON Div.numCompanyID = Comp.numCompanyId                                                       
 Left Join listdetails listdetailsActivity                                                  
 On Comm.numActivity = listdetailsActivity.NumlistItemID                                               
 Left Join listdetails listdetailsStatus                                                 
 On Comm.numStatus = listdetailsStatus.NumlistItemID
 LEFT JOIN OpportunityMaster Opp ON Opp.numDivisionId = Div.numDivisionID AND Opp.numContactId = AddC.numContactId  AND Opp.numOppID = Comm.numOppID 
 LEFT JOIN Sales_process_List_Master splm ON splm.Slp_Id = Opp.numBusinessProcessID 
 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId '
 
 
DECLARE @Prefix AS VARCHAR(5)
 
 IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
	 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
		set @Prefix = 'AddC.'                  
    else if @vcCSLookBackTableName = 'DivisionMaster'                  
		set @Prefix = 'Div.'
	else if @vcCSLookBackTableName = 'CompanyInfo'                  
		set @Prefix = 'Comp.'  
	ELSE IF @vcCSLookBackTableName='Tickler'	
		set @Prefix = 'Comm.'  
--	else if @vcCSLookBackTableName = 'ProjectProgress'                  
--		set @Prefix = 'pp.'  	
--	else if @vcCSLookBackTableName = 'OpportunityMaster'                  
--		set @Prefix = 'om.'  
--	else if @vcCSLookBackTableName = 'Sales_process_List_Master'                  
--		set @Prefix = 'splm.'  		
	else
		set @Prefix = ''   
	
	
	IF @vcCSAssociatedControlType='DateField'
	BEGIN
	IF @vcCSOrigDbCOlumnName='FormattedDate'
			SET @vcCSOrigDbCOlumnName='dtStartTime'
	
	
			set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',' + @Prefix + @vcCSOrigDbCOlumnName + '),111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
			 and CONVERT(VARCHAR(10),DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',' + @Prefix + @vcCSOrigDbCOlumnName + '),111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
	END
	ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'  OR @vcCSAssociatedControlType='TextBox'
	BEGIN
		PRINT 1
		set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
	END
END

set @strSql=@strSql+'  WHERE Comm.numDomainID = ' + Cast(@numDomainID as varchar(10)) 
 
 IF LEN(ISNULL(@vcBProcessValue,''))>0
BEGIN
	SET @strSql = @strSql + ' and ISNULL(Opp.numBusinessProcessID,0) in (SELECT Items FROM dbo.Split(''' +@vcBProcessValue + ''','',''))' 			
END

IF LEN(@vcActionTypes) > 0 
BEGIN
	SET @strSql = @strSql + ' AND Comm.bitTask IN (' + @vcActionTypes + ')'  
END
  
IF @numCriteria  = 1 --Action Items	Assigned
BEGIN
	set @strSql=@strSql+' AND 1=(Case when Comm.numAssign IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +') then 1 else 0 end)'
END
ELSE IF @numCriteria = 2 --Action Items Owned
BEGIN
	set @strSql=@strSql+' AND 1=(Case when Comm.numCreatedBy IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +' ) then 1 else 0 end)'
END
ELSE --Action Items Assigned Or Owned
BEGIN
	set @strSql=@strSql+' AND 1=(Case when (Comm.numAssign IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')  or Comm.numCreatedBy IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')) then 1 else 0 end)'
END
  
                                   
set @strSql=@strSql+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) <= '''+Cast(@endDate as varchar(30))+''')                                                     
 AND Comm.bitclosedflag=0                                                                         
 and Comm.bitTask <> 973  ) As X '              

IF LEN(ISNULL(@vcBProcessValue,''))=0
BEGIN
	If LEN(ISNULL(@vcActionTypes,'')) = 0 OR CHARINDEX('98989898989898',@vcActionTypes) > 0
	BEGIN
		SET @strSql1 =  ' insert into #tempRecords 
select                          
 ACI.numContactID,ACI.numDivisionID,Div.tintCRMType,ac.activityid as id ,case when alldayevent = 1 then ''5'' else ''0'' end as bitTask,                        
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) startdate,                        
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dateadd(second,duration,startdatetimeutc)) as endtime,                        
activitydescription as itemdesc,                        
ACI.vcFirstName + '' '' + ACI.vcLastName name, 
 ACI.vcFirstName,ACI.vcLastName,ACI.numPhone,ACI.numPhoneExtension,                   
 case when ACI.numPhone<>'''' then + ACI.numPhone +case when ACI.numPhoneExtension<>'''' then '' - '' + ACI.numPhoneExtension else '''' end  else '''' end as [Phone],                        
 Substring ( Comp.vcCompanyName , 0 , 12 ) + ''..'' As vcCompanyName ,                      
vcEmail,                        
''Calendar'' as task,                        
[subject] as Activity,                        
case when importance =0 then ''Low'' when importance =1 then ''Normal'' when importance =2 then ''High'' end as status,                        
0 as numcreatedby,                        
0 as numterid,                        
''-/-'' as numAssignedTo, 
''-'' AS numAssignedBy,        
''0'' as caseid,                        
null as vccasenumber,                        
0 as casetimeid,                        
0 as caseexpid,                        
1 as type ,                    
isnull(itemid,'''') as itemid  ,                    
changekey  ,cast(Null as datetime) as DueDate,cast(0 as bit) as bitFollowUpAnyTime,
dbo.GetListIemName(Comp.numNoOfEmployeesId) AS numNoOfEmployeesId,Div.vcComPhone,
dbo.GetListIemName(Div.numFollowUpStatus) AS numFollowUpStatus,'''' as vcColorScheme,'''',0,'''',0 AS [numOppId],0 AS numBusinessProcessID
from activity ac join             
activityresource ar on  ac.activityid=ar.activityid             
join resource res on ar.resourceid=res.resourceid                         
--join usermaster UM on um.numUserId = res.numUserCntId                
join AdditionalContactsInformation ACI on ACI.numContactId = res.numUserCntId                
 JOIN DivisionMaster Div                                                          
 ON ACI.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                             
 ON Div.numCompanyID = Comp.numCompanyId                 
 where  res.numUserCntId = ' + Cast(@numUserCntID as varchar(10)) +'             
and ac.OriginalStartDateTimeUtc IS NULL            
and res.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  and 
(DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) <= '''+Cast(@endDate as varchar(30))+''')
--(startdatetimeutc between dateadd(month,-1,getutcdate()) and dateadd(month,1,getutcdate())) 
and [status] <> -1                                                     
and ac.activityid not in 
(select distinct(numActivityId) from communication Comm where Comm.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  
AND 1=(Case ' + Cast(@bitFilterRecord as varchar(10)) +' when 1 then Case when Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  then 1 else 0 end 
		else  Case when (Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  or Comm.numCreatedBy=' + Cast(@numUserCntID as varchar(10)) +' ) then 1 else 0 end end)                                                                      
-- AND (dtStartTime >= '''+Cast(@startDate as varchar(30))+''' and dtStartTime <= '''+Cast(@endDate as varchar(30))+''') 
)
 Order by endtime'
	END

-- btDocType =2 for Document approval request, =1 for bizdoc
SET @strSql2 = ' insert into #tempRecords
select 
B.numContactID,B.numDivisionID,E.tintCRMType,CASE BA.btDocType WHEN 2 THEN BA.numOppBizDocsId WHEN 3 THEN BA.numOppBizDocsId ELSE A.numBizActionID END as ID,
convert(varchar(15),A.bitTask) as bitTask,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) as Startdate,                        
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) as EndTime ,  
'''' as itemDesc,
vcFirstName + '' '' + vcLastName  as [Name],
B.vcFirstName , B.vcLastName ,B.numPhone,B.numPhoneExtension,  
case when B.numPhone<>'''' then + B.numPhone +case when B.numPhoneExtension<>'''' then '' - '' + B.numPhoneExtension else '''' end  else '''' end as [Phone],
vcCompanyName,
vcEmail,
CASE BA.btDocType WHEN 2  THEN  ''Document Approval Request'' ELSE ''BizDoc Approval Request'' END as Task,
dbo.GetListIemName(bitTask) as Activity,
'''' as Status,
A.numCreatedBy,
E.numTerId, 
case When Len( dbo.fn_GetContactName(numAssign) ) > 12                                           
  Then Substring( dbo.fn_GetContactName(numAssign) , 0  ,  12 ) + ''..''                                           
  Else dbo.fn_GetContactName(numAssign)                                           
 end                                          
 numAssignedTo ,  
 ''-'' AS numAssignedBy,        
''0'' as caseid,
''0'' as vcCasenumber,
0 as CaseTimeId,
0 as CaseExpId,
CASE BA.btDocType WHEN 2 THEN 2 ELSE 3 END as [type], 
'''' AS itemid  ,      
 '''' AS changekey ,   
dtCreatedDate as DueDate  ,cast(0 as bit) as bitFollowUpAnyTime,
dbo.GetListIemName(D.numNoOfEmployeesId) AS numNoOfEmployeesId,E.vcComPhone,
dbo.GetListIemName(E.numFollowUpStatus) AS numFollowUpStatus,'''' as vcColorScheme,'''',0,'''', 0 AS [numOppId],0 AS numBusinessProcessID
from BizDocAction A LEFT JOIN dbo.BizActionDetails BA ON BA.numBizActionId = A.numBizActionId, 
AdditionalContactsInformation B, 
CompanyInfo D,
DivisionMaster E
WHERE A.numContactId=b.numContactId and a.numDomainID=b.numDomainID  and 
D.numCompanyId=E.numCompanyId and 
D.numDomainID=a.numDomainID and 
E.numDivisionId=B.numDivisionId and 
a.numDomainID= ' + Cast(@numDomainID as varchar(10)) +' and a.numContactId = ' + Cast(@numUserCntID as varchar(10)) +'  and  
A.numStatus=0 
and (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) <= '''+Cast(@endDate as varchar(30))+''')'

END


declare @Nocolumns as tinyint                
set @Nocolumns=0       
         
Select @Nocolumns=isnull(count(*),0) from View_DynamicColumns where numFormId=43 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  
  CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType char(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth NUMERIC(18,2))
                 
if @Nocolumns > 0            
begin      
INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
 FROM View_DynamicColumns 
 where numFormId=43 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
  order by tintOrder asc  
  
end            
else            
begin 

	--Check if Master Form Configuration is created by administrator if NoColumns=0
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 43 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END

	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName, ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType,numListID,vcLookBackTableName, bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=43 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	ORDER BY 
		tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		 INSERT INTO #tempForm
		select tintOrder,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
		 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
		,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
		 FROM View_DynamicDefaultColumns
		 where numFormId=43 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 AND numDomainID=@numDomainID
		order by tintOrder asc  
	END 
end                                            

DECLARE @strSql3 AS VARCHAR(8000)

SET @strSql3=   ' select * from #tempRecords order by ' + @columnName +' ' + @columnSortOrder  

print @strSql
exec (@strSql + @strSql1 + @strSql2 + @strSql3 )
drop table #tempRecords

SELECT * FROM #tempForm

DROP TABLE #tempForm
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON


GO
-- USP_GetItemsForInventoryAdjustment 1 , 58 , 0 , ''
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemsForInventoryAdjustment')
DROP PROCEDURE USP_GetItemsForInventoryAdjustment
GO
CREATE PROCEDURE [dbo].[USP_GetItemsForInventoryAdjustment]
@numDomainID numeric(18,0),
@numWarehouseID numeric(18,0),
@numItemGroupID NUMERIC = 0,
@KeyWord AS VARCHAR(1000) = '',
@numCurrentPage as numeric(9)=0,
@PageSize AS INT=0,
@numTotalPage as numeric(9) OUT,    
@SortChar CHAR(1) = '0',
@numUserCntID NUMERIC(18,0)

as

declare @strsql as varchar(8000)
declare @strRowCount as varchar(8000)
    /*paging logic*/
    DECLARE  @firstRec  AS INTEGER
    DECLARE  @lastRec  AS INTEGER
    SET @firstRec = (@numCurrentPage
                       - 1)
                      * @PageSize
    SET @lastRec = (@numCurrentPage
                      * @PageSize
                      + 1);

DECLARE @strColumns VARCHAR(MAX) = ''
DECLARE @strJoin VARCHAR(MAX) = ''
DECLARE @numFormID NUMERIC(18,0) = 126

DECLARE @TempForm TABLE  
(
	ID INT IDENTITY(1,1),tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),
	vcAssociatedControlType nvarchar(50), vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),
	bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
	bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,
	vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
)


DECLARE @Nocolumns AS TINYINT;
SET @Nocolumns = 0

SELECT 
	@Nocolumns=ISNULL(SUM(TotalRow),0) 
FROM
	(            
		SELECT	
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
	) TotalRows

IF @Nocolumns=0
BEGIN
	INSERT INTO DycFormConfigurationDetails 
	(
		numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,tintPageType,bitCustom,intColumnWidth
	)
	SELECT 
		@numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,1,0,intColumnWidth
	FROM 
		View_DynamicDefaultColumns
	WHERE 
		numFormId=@numFormId AND bitDefault=1 AND ISNULL(bitSettingField,0)=1 AND numDomainID=@numDomainID
	ORDER BY 
		tintOrder ASC 
END


INSERT INTO @TempForm
SELECT 
	tintRow+1 as tintOrder,	vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
	vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit, bitIsRequired,bitIsEmail,
	bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
	ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
FROM 
	View_DynamicColumns 
WHERE 
	numFormId=@numFormId 
	AND numUserCntID=@numUserCntID 
	AND numDomainID=@numDomainID 
	AND tintPageType=1 
	AND ISNULL(bitSettingField,0)=1 
	AND ISNULL(bitCustom,0)=0  
UNION
    SELECT 
		tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,
		'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,bitIsRequired,bitIsEmail,
		bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,
		ListRelID,intColumnWidth,0,'' 
 from View_DynamicCustomColumns
 where numFormId=@numFormId and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
 AND ISNULL(bitCustom,0)=1  

DECLARE @bitCustom AS BIT
DECLARE @tintOrder AS TINYINT
DECLARE @numFieldId AS NUMERIC(18,0)
DECLARE @vcFieldName AS VARCHAR(200)
DECLARE @vcAssociatedControlType AS VARCHAR(200)
DECLARE @vcDbColumnName AS VARCHAR(200)

DECLARE @i AS INT = 1
DECLARE @COUNT AS INT = 0
SELECT @COUNT=COUNT(*) FROM @TempForm

WHILE @i <= @COUNT
BEGIN
	SELECT  
		@bitCustom = bitCustomField,
		@tintOrder = tintOrder,
		@vcFieldName = vcFieldName,
		@numFieldId=numFieldId,
        @vcAssociatedControlType = vcAssociatedControlType,
        @vcDbColumnName = vcDbColumnName
    FROM    
		@TempForm
    WHERE   
		ID = @i        

	IF @bitCustom = 1 
    BEGIN      
        IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea' 
        BEGIN                    
			SET @strColumns = @strColumns + ',CFW'  + CONVERT(VARCHAR(3), @tintOrder)  + '.Fld_Value  [' + @vcDbColumnName + ']'                   
            SET @strJoin = @strJoin + ' LEFT JOIN CFW_FLD_Values_Item CFW' + CONVERT(VARCHAR(3), @tintOrder) + ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                + CONVERT(VARCHAR(10), @numFieldId)
                                + 'and CFW' + CONVERT(VARCHAR(3), @tintOrder)
                                + '.RecId=I.numItemCode '                                                         
        END   
        ELSE IF @vcAssociatedControlType = 'CheckBox' 
        BEGIN            
            SET @strColumns = @strColumns + ',CASE WHEN ISNULL(CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Value,0)=0 then ''No'' when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.Fld_Value,0)=1 then ''Yes'' end   ['
                                    + @vcDbColumnName
                                    + ']'              
            SET @strJoin = @strJoin + ' LEFT JOIN CFW_FLD_Values_Item CFW' + CONVERT(VARCHAR(3), @tintOrder) + ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                    + CONVERT(VARCHAR(10), @numFieldId)
                                    + 'and CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.RecId=I.numItemCode   '                                                     
        END                
        ELSE IF @vcAssociatedControlType = 'DateField' 
        BEGIN              
            SET @strColumns = @strColumns + ',dbo.FormatedDateFromDate(CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Value,' + CONVERT(VARCHAR(10), @numDomainId) 
									+ ')  [' + @vcDbColumnName + ']'                   
            SET @strJoin = @strJoin + ' LEFT JOIN CFW_FLD_Values_Item CFW' + CONVERT(VARCHAR(3), @tintOrder) + ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) 
										+ '.Fld_Id='
                                        + CONVERT(VARCHAR(10), @numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.RecId=I.numItemCode   '                                                         
        END                
        ELSE IF @vcAssociatedControlType = 'SelectBox' 
        BEGIN                
            SET @vcDbColumnName = 'Cust' + CONVERT(VARCHAR(10), @numFieldId)                
            SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcData' + ' [' + @vcDbColumnName + ']'                                                          
            SET @strJoin = @strJoin + ' LEFT JOIN CFW_FLD_Values_Item CFW' + CONVERT(VARCHAR(3), @tintOrder) + ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) 
									+ '.Fld_Id=' + CONVERT(VARCHAR(10), @numFieldId) + ' and CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.RecId=I.numItemCode    '                                                         
            SET @strJoin = @strJoin + ' LEFT JOIN ListDetails L'+ CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.numListItemID=CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.Fld_Value'                
        END                 
    END       


	SET @i = @i + 1
END


SET @strsql = ''
SET @strRowCount = ''

SET @strsql = @strsql +
'WITH WarehouseItems AS (
	SELECT 
		ROW_NUMBER() OVER(ORDER by WI.numWareHouseItemID) AS RowNumber,
		WI.numWareHouseItemID,
		W.vcWareHouse,
		WI.numOnHand,
		I.monAverageCost,
		(ISNULL(WI.numOnHand,0) * ISNULL(I.monAverageCost,0)) AS monCurrentValue,
		CASE WHEN I.bitSerialized =1 THEN 1 WHEN I.bitLotNo =1 THEN 1 ELSE 0 END AS IsLotSerializedItem,
		I.numAssetChartAcntId,
		CASE 
			WHEN '+convert(varchar(20), @numItemGroupID)+' > 0 
			THEN dbo.fn_GetAttributes(WI.numWareHouseItemID,I.bitSerialized) + CONVERT(VARCHAR(10),I.numItemGroup)
			ELSE ''''
		END AS vcAttribute,
		isnull(I.bitSerialized,0) as bitSerialized,
		isnull(I.bitLotNo,0) as bitLotNo,
		isnull(WL.vcLocation,'''') Location,
		I.numItemcode,
		I.vcItemName,
		ISNULL(CompanyInfo.vcCompanyName,'''') AS vcCompanyName,
		ISNULL(I.monListPrice,0) AS monListPrice,
		ISNULL(I.txtItemDesc,'''') AS txtItemDesc,
		ISNULL(I.vcModelID,'''') AS vcModelID,
		ISNULL(I.numBarCodeId,'''') AS numBarCodeId,
		ISNULL(I.vcManufacturer,'''') AS vcManufacturer,
		ISNULL(I.fltHeight,'''') AS fltHeight,
		ISNULL(I.fltLength,'''') AS fltLength,
		ISNULL(I.fltWeight,'''') AS fltWeight,
		ISNULL(I.fltWidth,'''') AS fltWidth,
		ISNULL(ItemGroups.vcItemGroup,'''') AS numItemGroup,
		ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 175 AND numListItemID = I.numPurchaseUnit) ,'''') AS numPurchaseUnit,
		ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 175 AND numListItemID = I.numSaleUnit) ,'''') AS numSaleUnit,
		ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 175 AND numListItemID = I.numBaseUnit) ,'''') AS numBaseUnit,
		ISNULL(I.vcSKU,'''') AS vcSKU,
		ISNULL(Vendor.monCost,0) AS monCost,
		ISNULL(Vendor.intMinQty,0) AS intMinQty,
		ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 461 AND numListItemID = I.numItemClass) ,'''') AS numShipClass'
		+
		ISNULL(@strColumns,'')
		+ ' FROM 
		dbo.WareHouseItems WI 
		INNER JOIN dbo.Warehouses W ON WI.numDomainID = W.numDomainID AND WI.numWareHouseID = W.numWareHouseID
		LEFT JOIN WarehouseLocation WL on WL.numWLocationID =WI.numWLocationID
		INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID 
		LEFT JOIN Vendor ON I.numItemCode = Vendor.numItemCode AND I.numVendorID = Vendor.numVendorID
		LEFT JOIN DivisionMaster ON Vendor.numVendorID = DivisionMaster.numDivisionID
		LEFT JOIN CompanyInfo ON DivisionMaster.numCompanyID  = CompanyInfo.numCompanyId
		LEFT JOIN ItemGroups ON I.numItemGroup = ItemGroups.numItemGroupID '
		+ 
		ISNULL(@strJoin,'')
		+ ' WHERE 
		I.charItemType = ''P'' AND 
		ISNULL(bitAssembly,0)=0 AND 
		I.numDomainID = '+ convert(varchar(20), @numDomainID) + 
		' AND  WI.numDomainID=' + convert(varchar(20), @numDomainID) +
		' AND WI.numWareHouseID='+ CONVERT(VARCHAR(20),@numWarehouseID) +
		' AND (ISNULL(I.numItemGroup,0) ='+CONVERT(VARCHAR(20), @numItemGroupID)+')'

SET @strRowCount = @strRowCount + 
' SELECT 
COUNT(*) AS TotalRowCount
FROM dbo.WareHouseItems WI INNER JOIN dbo.Warehouses W ON WI.numDomainID = W.numDomainID AND WI.numWareHouseID = W.numWareHouseID
left join WarehouseLocation WL on WL.numWLocationID =WI.numWLocationID
INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID
WHERE 
I.charItemType = ''P'' and ISNULL(bitAssembly,0)=0  AND I.numDomainID = '+ convert(varchar(20), @numDomainID)  +' AND  WI.numDomainID=' + convert(varchar(20), @numDomainID)+'  AND WI.numWareHouseID='+ CONVERT(VARCHAR(20),@numWarehouseID) +
' AND (ISNULL(I.numItemGroup,0) ='+CONVERT(VARCHAR(20), @numItemGroupID)+') '


 IF @KeyWord <> '' 
        BEGIN 
            IF CHARINDEX('vcCompanyName', @KeyWord) > 0 
                BEGIN
                    SET @strSql = @strSql
                        + ' and I.numItemCode in (select Vendor.numItemCode
								from Vendor join
divisionMaster div on div.numdivisionid=Vendor.numVendorid        
join companyInfo com  on com.numCompanyID=div.numCompanyID  WHERE Vendor.numDomainID='
                        + CONVERT(VARCHAR(15), @numDomainID)
                        + '  
									and Vendor.numItemCode= I.numItemCode '
                        + @KeyWord + ')'
                         
					SET @strRowCount =  @strRowCount  +	 ' and I.numItemCode in (select Vendor.numItemCode
								from Vendor join
divisionMaster div on div.numdivisionid=Vendor.numVendorid        
join companyInfo com  on com.numCompanyID=div.numCompanyID  WHERE Vendor.numDomainID='
                        + CONVERT(VARCHAR(15), @numDomainID)
                        + '  
									and Vendor.numItemCode= I.numItemCode '
                        + @KeyWord + ')' 		
                END
            ELSE 
            BEGIN
            	SET @strSql = @strSql + ' and  1 = 1 ' + @KeyWord 
                SET @strRowCount =  @strRowCount  + ' and  1 = 1 ' + @KeyWord 
            END
                
        END


if @SortChar<>'0'
	BEGIN
	  set @strSql=@strSql + ' AND vcItemName like '''+@SortChar+'%'')'  
	  set @strRowCount=@strRowCount + ' AND vcItemName like '''+@SortChar+'%'''  
	END
ELSE
   BEGIN
      set @strSql=@strSql + ' ) '  
   END


 





   set @strSql=@strSql + ' SELECT * FROM WarehouseItems WHERE 
                            RowNumber > '+CONVERT(VARCHAR(20),@firstRec) +' AND RowNumber < '+CONVERT(VARCHAR(20),@lastRec) + @strRowCount
 
   
  
PRINT(@strSql)
exec(@strSql)

SELECT * FROM @TempForm

GO


