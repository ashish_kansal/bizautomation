/******************************************************************
Project: Release 7.0 Date: 27.FEB.2017
Comments: ALTER SCRIPTS
*******************************************************************/

/************************** SANDEEP ************************/

SET IDENTITY_INSERT dbo.PaymentGateway ON 

INSERT INTO PaymentGateway 
(
	intPaymentGateWay,vcGateWayName,vcFirstFldName,vcSecndFldName
)
VALUES
(
	9,'BluePay','Login','Password'
)

SET IDENTITY_INSERT dbo.PaymentGateway OFF 


----------------------------------------

ALTER TABLE [dbo].[eCommerceDTL] DROP CONSTRAINT [DF_eCommerceDTL_bitHidePriceUser]
ALTER TABLE eCommerceDTL DROP COLUMN bitHidePriceAfterLogin
ALTER TABLE eCommerceDTL DROP COLUMN numRelationshipIdHidePrice
ALTER TABLE eCommerceDTL DROP COLUMN numProfileIDHidePrice
ALTER TABLE eCommerceDTL DROP COLUMN bitEnableDefaultAccounts
ALTER TABLE eCommerceDTL DROP COLUMN numDefaultAssetChartAcntId
ALTER TABLE eCommerceDTL DROP COLUMN numDefaultIncomeChartAcntId
ALTER TABLE eCommerceDTL DROP COLUMN numDefaultCOGSChartAcntId

ALTER TABLE Item ADD bitMatrix BIT

----------------------------------------------------------------
USE [Production.2014]
GO

/****** Object:  Table [dbo].[ItemAttributes]    Script Date: 27-Feb-17 2:41:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ItemAttributes](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numItemCode] [numeric](18, 0) NOT NULL,
	[FLD_ID] [numeric](18, 0) NOT NULL,
	[FLD_Value] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_ItemAttributes] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

-----------------------------------------------


USE [Production.2014]
GO

/****** Object:  Index [NCIX_20170220-202158]    Script Date: 27-Feb-17 2:51:43 PM ******/
CREATE NONCLUSTERED INDEX [NCIX_20170220-202158] ON [dbo].[ItemAttributes]
(
	[numDomainID] ASC,
	[numItemCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO


----------------------------------------------


BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numFieldID INT

	INSERT INTO DycFieldMaster
	(
		numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcPropertyName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting
	)
	VALUES
	(
		4,'Matrix','bitMatrix','bitMatrix','Item','IsMatrix','Y','R','CheckBox','',0,1,0,1,1,1,1,1,1
	) 


	SELECT @numFieldID = SCOPE_IDENTITY()


	INSERT INTO DycFormField_Mapping
	(
		numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcPropertyName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,bitAllowGridColor,bitImport,intSectionID
	)
	VALUES
	(
		4,@numFieldID,20,1,1,'Matrix','IsMatrix','CheckBox',NULL,1,0,0,1,0,1,1,1,0,1,1
	)

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH