/******************************************************************
Project: Release 6.5 Date: 26.DEC.2016
Comments: ALTER SCRIPTS
*******************************************************************/

/************************** SANDEEP ************************/

--CHECK Field ID ON SERVER

UPDATE DycFormField_Mapping SET bitAllowFiltering = 0 WHERE numFieldID=351 AND numFormID=21 --vcPathForImage
UPDATE DycFormField_Mapping SET bitAllowFiltering = 0 WHERE numFieldID=195 AND numFormID=21 --OnHand
UPDATE DycFormField_Mapping SET bitAllowFiltering = 0 WHERE numFieldID=196 AND numFormID=21 --OnOrder
UPDATE DycFormField_Mapping SET bitAllowFiltering = 0 WHERE numFieldID=197 AND numFormID=21 --On Allocation
UPDATE DycFormField_Mapping SET bitAllowFiltering = 0 WHERE numFieldID=198 AND numFormID=21 --On Backorder
UPDATE DycFormField_Mapping SET bitAllowFiltering = 0 WHERE numFieldID=199 AND numFormID=21 --Location
UPDATE DycFormField_Mapping SET bitAllowFiltering = 0 WHERE numFieldID=200 AND numFormID=21 --Reorder
UPDATE DycFormField_Mapping SET vcAssociatedControlType='CheckBox' WHERE numFieldID=295 AND numFormID=21 --In Work Order
UPDATE DycFormField_Mapping SET bitAllowFiltering = 0 WHERE numFieldID=469 AND numFormID=21 -- Price Level
UPDATE DycFormField_Mapping SET bitAllowFiltering = 0 WHERE numFieldID=210 AND numFormID=21 -- Stock Value
UPDATE DycFormField_Mapping SET bitAllowFiltering = 0 WHERE numFieldID=192 AND numFormID=21 -- Item Type
UPDATE DycFormField_Mapping SET bitAllowFiltering=1 WHERE numFieldID=40730 --Release Date

-----------------------------

BEGIN TRY
BEGIN TRANSACTION
begin
 --add new values for slide 1
 DECLARE @numFieldID AS NUMERIC(18,0) = 349
 DECLARE @numFormFieldID AS NUMERIC(18,0)

 SELECT TOP 1 @numFieldID=numFieldID  FROM DycFieldMaster WHERE vcFieldName = 'Vendor' AND vcDbColumnName = 'numVendorID'

 INSERT INTO DynamicFormFieldMaster
 (numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
  bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
 VALUES
 (26,'Vendor','R','SelectBox','numVendorID',0,0,'V','numVendorID',1,'Vendor',0,47,0,'',47,1,0,0,0,0)

 SELECT @numFormFieldID = SCOPE_IDENTITY()

 INSERT INTO DycFormField_Mapping
 (numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
 bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
 VALUES
 (4,@numFieldID,26,1,1,'Vendor','SelectBox','',47,47,1,1,0,0,1,0,0,0,0,@numFormFieldID)

end
COMMIT
END TRY
BEGIN CATCH
 IF @@TRANCOUNT > 0
  ROLLBACK TRANSACTION;

 SELECT 
  ERROR_MESSAGE(),
  ERROR_NUMBER(),
  ERROR_SEVERITY(),
  ERROR_STATE(),
  ERROR_LINE(),
  ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

--------------------------------------------------------------------


BEGIN TRY
BEGIN TRANSACTION
begin
 --add new values for slide 1
 DECLARE @numFieldID AS NUMERIC(18,0)
 DECLARE @numFormFieldID AS NUMERIC(18,0)

 INSERT INTO DycFieldMaster
 (numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
 bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
 VALUES
 (4,'Margin','vcMargin','vcMargin','','OpportunityItems','V','R','Label',48,1,1,1,0,0,0,1,0,0,1,1,0)

 SELECT @numFieldID = SCOPE_IDENTITY()

 INSERT INTO DynamicFormFieldMaster
 (numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
  bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
 VALUES
 (26,'Margin','R','Label','vcMargin',0,0,'V','vcMargin',1,'OpportunityItems',0,47,1,'',47,1,1,0,0,1)

 SELECT @numFormFieldID = SCOPE_IDENTITY()

 INSERT INTO DycFormField_Mapping
 (numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
 bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
 VALUES
 (4,@numFieldID,26,0,0,'Margin','Label','',47,47,1,1,0,0,1,0,0,0,0,@numFormFieldID)

end
COMMIT
END TRY
BEGIN CATCH
 IF @@TRANCOUNT > 0
  ROLLBACK TRANSACTION;

 SELECT 
  ERROR_MESSAGE(),
  ERROR_NUMBER(),
  ERROR_SEVERITY(),
  ERROR_STATE(),
  ERROR_LINE(),
  ISNULL(ERROR_PROCEDURE(), '-');
END CATCH


-------------------------------------------------------------------

ALTER TABLE OpportunityItems ADD numDeletedReceievedQty FLOAT 
ALTER TABLE OpportunityItemsReceievedLocation ADD numDeletedReceievedQty FLOAT

----------------------------------------------------------------

UPDATE DycFieldMaster SET vcDbColumnName='vcLastSalesOrderDate',vcLookBackTableName='OpportunityMaster',vcOrigDbColumnName='vcLastSalesOrderDate' WHERE vcFieldName = 'Last Sales Order Date'

----------------------------------------------------------------

BEGIN TRY
BEGIN TRANSACTION
begin
 --add new values for slide 1
 DECLARE @numFieldID AS NUMERIC(18,0)
 DECLARE @numFormFieldID AS NUMERIC(18,0)

 SELECT @numFieldID=numFieldID  FROM DycFieldMaster WHERE vcFieldName = 'Last Sales Order Date'

 INSERT INTO DynamicFormFieldMaster
 (numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
  bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
 VALUES
 (36,'Last Sales Order Date','R','Label','',0,0,'V','',0,'',0,47,1,'',47,1,1,0,0,0)

 SELECT @numFormFieldID = SCOPE_IDENTITY()

 INSERT INTO DycFormField_Mapping
 (numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
 bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
 VALUES
 (1,@numFieldID,36,0,0,'Last Sales Order Date','Label','',47,47,1,1,0,0,1,0,0,1,0,@numFormFieldID)

end
COMMIT
END TRY
BEGIN CATCH
 IF @@TRANCOUNT > 0
  ROLLBACK TRANSACTION;

 SELECT 
  ERROR_MESSAGE(),
  ERROR_NUMBER(),
  ERROR_SEVERITY(),
  ERROR_STATE(),
  ERROR_LINE(),
  ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

----------------------------------------------------------------

BEGIN TRY
BEGIN TRANSACTION
begin
 --add new values for slide 1
 DECLARE @numFieldID AS NUMERIC(18,0)
 DECLARE @numFormFieldID AS NUMERIC(18,0)

SELECT @numFieldID=numFieldID  FROM DycFieldMaster WHERE vcFieldName = 'Last Sales Order Date'

 INSERT INTO DynamicFormFieldMaster
 (numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
  bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
 VALUES
 (43,'Last Sales Order Date','R','Label','',0,0,'V','',0,'',0,47,1,'',47,1,1,0,0,0)

 SELECT @numFormFieldID = SCOPE_IDENTITY()

 INSERT INTO DycFormField_Mapping
 (numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
 bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
 VALUES
 (1,@numFieldID,43,0,0,'Last Sales Order Date','Label','',47,47,1,1,0,0,1,0,0,1,0,@numFormFieldID)

end
COMMIT
END TRY
BEGIN CATCH
 IF @@TRANCOUNT > 0
  ROLLBACK TRANSACTION;

 SELECT 
  ERROR_MESSAGE(),
  ERROR_NUMBER(),
  ERROR_SEVERITY(),
  ERROR_STATE(),
  ERROR_LINE(),
  ISNULL(ERROR_PROCEDURE(), '-');
END CATCH


----------------------------------------------------------------


BEGIN TRY
BEGIN TRANSACTION
begin
 --add new values for slide 1
 DECLARE @numFieldID AS NUMERIC(18,0)
 DECLARE @numFormFieldID AS NUMERIC(18,0)

 INSERT INTO DycFieldMaster
 (numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
 bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
 VALUES
 (1,'Performance','vcPerformance','vcPerformance','','CompanyInfo','V','R','Label',48,1,1,1,0,0,0,1,0,0,1,1,0)

 SELECT @numFieldID = SCOPE_IDENTITY()

 INSERT INTO DynamicFormFieldMaster
 (numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
  bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
 VALUES
 (36,'Performance','R','Label','',0,0,'V','',0,'',0,48,1,'',47,1,1,0,0,1)

 SELECT @numFormFieldID = SCOPE_IDENTITY()

 INSERT INTO DycFormField_Mapping
 (numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
 bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
 VALUES
 (1,@numFieldID,36,0,0,'Performance','Label','',48,1,1,1,0,0,1,0,0,1,1,@numFormFieldID)

  INSERT INTO DynamicFormFieldMaster
 (numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
  bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
 VALUES
 (43,'Performance','R','Label','',0,0,'V','',0,'',0,48,1,'',47,1,1,0,0,1)

 SELECT @numFormFieldID = SCOPE_IDENTITY()

 INSERT INTO DycFormField_Mapping
 (numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
 bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
 VALUES
 (1,@numFieldID,43,0,0,'Performance','Label','',48,1,1,1,0,0,1,0,0,1,1,@numFormFieldID)

end
COMMIT
END TRY
BEGIN CATCH
 IF @@TRANCOUNT > 0
  ROLLBACK TRANSACTION;

 SELECT 
  ERROR_MESSAGE(),
  ERROR_NUMBER(),
  ERROR_SEVERITY(),
  ERROR_STATE(),
  ERROR_LINE(),
  ISNULL(ERROR_PROCEDURE(), '-');
END CATCH


----------------------------------------------------------------

BEGIN TRY
BEGIN TRANSACTION

	INSERT INTO PageNavigationDTL 
	(numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID)
	VALUES
	(258,35,83,'Matching Rules','../Accounting/frmBankReconcileMatchRulesList.aspx',1,45)

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		45,
		258,
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		tintGroupType=1
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

---------------------------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[BankReconcileMatchRule]    Script Date: 01-Dec-16 5:43:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[BankReconcileMatchRule](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[vcName] [varchar](200) NOT NULL,
	[vcBankAccounts] [varchar](max) NOT NULL,
	[bitMatchAllConditions] [bit] NOT NULL,
	[numCreatedBy] [numeric](18, 0) NOT NULL,
	[dtCreatedDate] [datetime] NOT NULL,
	[tintOrder] [int] NOT NULL,
 CONSTRAINT [PK_BankReconcileMathRule] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO


---------------------------------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[BankReconcileMatchRuleCondition]    Script Date: 01-Dec-16 5:43:43 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[BankReconcileMatchRuleCondition](
	[numConditionID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numRuleID] [numeric](18, 0) NOT NULL,
	[tintColumn] [tinyint] NOT NULL,
	[tintConditionOperator] [tinyint] NOT NULL,
	[vcTextToMatch] [varchar](max) NOT NULL,
	[numDivisionID] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_BankReconcileMathRuleCondition] PRIMARY KEY CLUSTERED 
(
	[numConditionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

ALTER TABLE [dbo].[BankReconcileMatchRuleCondition]  WITH CHECK ADD  CONSTRAINT [FK_BankReconcileMathRuleCondition_BankReconcileMathRule] FOREIGN KEY([numRuleID])
REFERENCES [dbo].[BankReconcileMatchRule] ([ID])
GO

ALTER TABLE [dbo].[BankReconcileMatchRuleCondition] CHECK CONSTRAINT [FK_BankReconcileMathRuleCondition_BankReconcileMathRule]
GO


----------------------------------------------------


/* ENABLES CHAGE DATA CAPTURE ON DATABASE */

USE [Production.2014]
GO
EXECUTE sys.sp_cdc_enable_db;
GO

=============================================================================================

ALTER TABLE CFW_Fld_Values_Opp ADD
numModifiedBy NUMERIC(18,0),
bintModifiedDate DATETIME

=============================================================================================
/* CREATES CLEANUP JOB TO CLEAR CHAGE CAPTURE DATA EVERY 2 DAYS */

EXEC sys.sp_cdc_add_job
     @job_type = N'cleanup'
    ,@start_job = 0
    ,@retention = 2880; -- 2 DAYS


=============================================================================================
/* ENABLES CHAGE DATA TRACKING ON OPP/ORDER RELATED DATA TABLE */

EXECUTE sys.sp_cdc_enable_table
    @source_schema = N'dbo'
  , @source_name = N'OpportunityMaster'
  , @role_name = N'cdc_Admin';
GO

EXECUTE sys.sp_cdc_enable_table
    @source_schema = N'dbo'
  , @source_name = N'OpportunityItems'
  , @role_name = N'cdc_Admin';
GO

EXECUTE sys.sp_cdc_enable_table
    @source_schema = N'dbo'
  , @source_name = N'OpportunityBizDocs'
  , @role_name = N'cdc_Admin';
GO

EXECUTE sys.sp_cdc_enable_table
    @source_schema = N'dbo'
  , @source_name = N'OpportunityBizDocItems'
  , @role_name = N'cdc_Admin';
GO

EXECUTE sys.sp_cdc_enable_table
    @source_schema = N'dbo'
  , @source_name = N'CFW_Fld_Values_Opp'
  , @role_name = N'cdc_Admin';
GO

=============================================================================================

/****** Object:  Table [dbo].[RecordHistoryModuleMaster]    Script Date: 15-Jan-16 1:52:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[RecordHistoryModuleMaster](
	[numRHModuleMasterID] [int] NOT NULL,
	[vcRHModuleName] [varchar](100) NOT NULL,
 CONSTRAINT [PK_RecordHistoryModuleMaster] PRIMARY KEY CLUSTERED 
(
	[numRHModuleMasterID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

INSERT INTO [dbo].[RecordHistoryModuleMaster] 
(numRHModuleMasterID,vcRHModuleName) 
VALUES
(1,'Organization'),
(2,'Contact'),
(3,'Opp/Order'),
(4,'Case'),
(5,'Project'),
(6,'Item')

=============================================================================================

USE [Production.2014]
GO

/****** Object:  Table [dbo].[RecordHistory]    Script Date: 18-Jan-16 12:39:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[RecordHistory](
	[numRHID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numUserCntID] [numeric](18, 0) NULL,
	[dtDate] [datetime] NULL,
	[numRecordID] [numeric](18, 0) NOT NULL,
	[numRHModuleMasterID] [int] NOT NULL,
	[vcEvent] [varchar](200) NULL,
	[numFieldID] [numeric](18, 0) NULL,
	[vcFieldName] [varchar](200) NULL,
	[bitCustomField] [bit] NULL,
	[vcOldValue] [varchar](max) NULL,
	[vcNewValue] [varchar](max) NULL,
	[vcDescription] [varchar](max) NULL,
	[vcHiddenDescription] [varchar](max) NULL,
 CONSTRAINT [PK_RecordHistory] PRIMARY KEY CLUSTERED 
(
	[numRHID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

ALTER TABLE [dbo].[RecordHistory]  WITH CHECK ADD  CONSTRAINT [FK_RecordHistory_RecordHistory] FOREIGN KEY([numRHModuleMasterID])
REFERENCES [dbo].[RecordHistoryModuleMaster] ([numRHModuleMasterID])
GO

ALTER TABLE [dbo].[RecordHistory] CHECK CONSTRAINT [FK_RecordHistory_RecordHistory]

=============================================================================================

USE [Production.2014]
GO

/****** Object:  Index [NonClusteredIndex-20160119-124733]    Script Date: 19-Jan-16 12:48:48 PM ******/
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20160119-124733] ON [dbo].[RecordHistory]
(
	[numDomainID] ASC,
	[dtDate] DESC,
	[numRecordID] ASC,
	[numRHModuleMasterID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO




=============================================================================================
USE [Production.2014]
GO

/****** Object:  Table [dbo].[RecordHistoryStatus]    Script Date: 18-Jan-16 12:38:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RecordHistoryStatus](
	[Id] [int] NOT NULL,
	[dtLastExecuted] [datetime] NOT NULL,
	[intNoOfTimesTried] [int] NOT NULL,
	[bitSuccess] [bit] NOT NULL,
 CONSTRAINT [PK_RecordHistoryStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


INSERT INTO RecordHistoryStatus (Id,dtLastExecuted,intNoOfTimesTried,bitFailureNotificationSent) VALUES (1,GETDATE(),0,0)
=========================================================================================================================