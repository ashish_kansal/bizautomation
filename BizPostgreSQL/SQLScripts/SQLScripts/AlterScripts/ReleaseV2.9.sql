/******************************************************************
Project: Release 2.9 Date: 01-Marcha-2014
Comments: Done by chintan 
*******************************************************************/

/******************************************************************
 Chintan : Project: Release 2.9 Date: 19.02.2014
Comments: Changes for adding ItemID searchable in bizcart
*******************************************************************/

BEGIN TRANSACTION




--25.02.2014 updated on demo and production server do not run again
ALTER TABLE dbo.UserMaster
ALTER column txtSignature varchar(8000)
INSERT INTO dbo.EmailMergeFields (
	vcMergeField,
	vcMergeFieldValue,
	numModuleID,
	tintModuleType
) VALUES ( 
	/* vcMergeField - varchar(100) */ 'Item Attributes',
	/* vcMergeFieldValue - varchar(2000) */ '##ItemAttributes##',
	/* numModuleID - numeric(18, 0) */ 7,
	/* tintModuleType - tinyint */ 1 ) 



usp_UpdateSignature
USP_ItemDetailsForEcomm
USP_EcommerceSettings
USP_GetECommerceDetails

/*Below script is already executed on production DB as on 19.2.2014*/
INSERT INTO dbo.DycFormField_Mapping (
	numModuleID,
	numFieldID,
	numDomainID,
	numFormID,
	bitAllowEdit,
	bitInlineEdit,
	vcFieldName,
	vcAssociatedControlType,
	vcPropertyName,
	PopupFunctionName,
	[order],
	tintRow,
	tintColumn,
	bitInResults,
	bitDeleted,
	bitDefault,
	bitSettingField,
	bitAddField,
	bitDetailField,
	bitAllowSorting,
	bitWorkFlowField,
	bitImport,
	bitExport,
	bitAllowFiltering,
	bitRequired,
	numFormFieldID,
	intSectionID,
	bitAllowGridColor
) 
SELECT numModuleID,numFieldId,numDomainID,30,0 AS bitAllowEdit,0 AS bitInlineEdit,
vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,6 AS [Order],NULL,NULL,
1 AS bitInResults,0,0,1,NULL,1,NULL,NULL,NULL,
NULL,1,NULL,null,NULL,NULL  FROM dbo.DycFieldMaster WHERE numModuleID=4 AND vcFieldName like 'Item ID'
------------------------------


GO
ALTER TABLE dbo.eCommerceDTL ADD
	bitAutoSelectWarehouse bit NULL
GO
ALTER TABLE dbo.eCommerceDTL ADD CONSTRAINT
	DF_eCommerceDTL_bitAutoSelectWarehouse DEFAULT 0 FOR bitAutoSelectWarehouse
GO

ROLLBACK 


----------------------------------------------------------------
--Sandeep---------  Administration - Field Management - New Item --------
----------------------------------------------------------------

BEGIN TRANSACTION

	
	--- Inserts Dynamic Forms for creating new inventory, non - invemtory and serialized items
	
	INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag]) VALUES (86,'Inventory Item','Y','N',0,0)
	INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag]) VALUES (87,'Non-Inventory Item','Y','N',0,0)
	INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag]) VALUES (88,'Serialized Item','Y','N',0,0)

	-- Following script creates tree menu New Item in Field Management Section

	DECLARE @numPageNavID numeric 
	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
	INSERT INTO dbo.PageNavigationDTL
			( numPageNavID ,
			  numModuleID ,
			  numParentID ,
			  vcPageNavName ,
			  vcNavURL ,
			  vcImageURL ,
			  bitVisible ,
			  numTabID
			)
	SELECT @numPageNavID ,13,109,'New Item','../pagelayout/frmCustomisePageLayout.aspx?Ctype=I&type=1&PType=2&FormId=86',NULL,1,-1
	

	DECLARE CursorTreeNavigation CURSOR FOR
	SELECT numDomainId FROM Domain WHERE numDomainId <> -255
	
	OPEN CursorTreeNavigation;

	DECLARE @numDomainId NUMERIC(18,0);

	FETCH NEXT FROM CursorTreeNavigation INTO @numDomainId;
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
	  
	   ---- Give permission for each domain to tree node
	   
   		DELETE FROM TreeNavigationAuthorization WHERE numPageNavID=@numPageNavID AND numDomainID = @numDomainId
	
		SELECT * INTO #tempData1 FROM
		(
		 SELECT    T.numTabID,
                        CASE WHEN bitFixed = 1
                             THEN CASE WHEN EXISTS ( SELECT numTabName
                                                     FROM   TabDefault
                                                     WHERE  numTabId = T.numTabId
                                                            AND tintTabType = T.tintTabType AND numDomainID =D.numDomainID)
                                       THEN ( SELECT TOP 1
                                                        numTabName
                                              FROM      TabDefault
                                              WHERE     numTabId = T.numTabId
                                                        AND tintTabType = T.tintTabType AND numDomainID = D.numDomainID
                                            )
                                       ELSE T.numTabName
                                  END
                             ELSE T.numTabName
                        END numTabname,
                        vcURL,
                        bitFixed,
                        1 AS tintType,
                        T.vcImage,
                        D.numDomainID,
                        A.numGroupID
              FROM      TabMaster T
                        CROSS JOIN Domain D --ON D.numDomainID = T.numDomainID
                        LEFT JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
              WHERE     bitFixed = 1
                        AND D.numDomainId <> -255
                        AND t.tintTabType = 1
              
              UNION ALL
              SELECT    -1 AS [numTabID],
                        'Administration' numTabname,
                        '' vcURL,
                        1 bitFixed,
                        1 AS tintType,
                        '' AS vcImage,
                        D.numDomainID,
                        A.numGroupID
              FROM      Domain D
                        JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
                        WHERE D.numDomainID = @numDomainID
			  
			  UNION ALL
			  SELECT    -3 AS [numTabID],
						'Advanced Search' numTabname,
						'' vcURL,
						1 bitFixed,
						1 AS tintType,
						'' AS vcImage,
						D.numDomainID,
						A.numGroupID
			  FROM      Domain D
						JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId          
            ) TABLE2       		
                        
		INSERT  INTO dbo.TreeNavigationAuthorization
            (
              numGroupID,
              numTabID,
              numPageNavID,
              bitVisible,
              numDomainID,
              tintType
            )
            SELECT  numGroupID,
                    PND.numTabID,
                    numPageNavID,
                    bitVisible,
                    numDomainID,
                    tintType
            FROM    dbo.PageNavigationDTL PND
                    JOIN #tempData1 TD ON TD.numTabID = PND.numTabID 
                    WHERE PND.numPageNavID= @numPageNavID
		
          DROP TABLE #tempData1
	   
	   
	    ---- Inserts required field mapping in DycFormConfigurationDetails table so it can not be removed form configuration wizard for new inventory item form
	    IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 86 AND numFieldId = 189 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (86,189,1,1,@numDomainId,0,0,2,0)
		END
		IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 86 AND numFieldId = 270 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (86,270,1,2,@numDomainId,0,0,2,0)
		END
		IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 86 AND numFieldId = 271 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (86,271,2,1,@numDomainId,0,0,2,0)
		END
		IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 86 AND numFieldId = 272 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (86,272,2,2,@numDomainId,0,0,2,0)
		END
		
		--- Inserts required field mapping in DynamicFormField_Validation for validation on form new inventory item form
		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 86 AND numFormFieldId = 189 AND numDomainId = @numDomainId)
		BEGIN
			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (86,189,@numDomainId,'Item',1)
		END
		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 86 AND numFormFieldId = 270 AND numDomainId = @numDomainId)
		BEGIN
			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (86,270,@numDomainId,'Income Account',1)
		END
		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 86 AND numFormFieldId = 271 AND numDomainId = @numDomainId)
		BEGIN
			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (86,271,@numDomainId,'Asset Account',1)
		END
		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 86 AND numFormFieldId = 272 AND numDomainId = @numDomainId)
		BEGIN
			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (86,272,@numDomainId,'COGS Account',1)
		END
		
		---- Inserts required field mapping in DycFormConfigurationDetails table so it can not be removed form configuration wizard for new non inventory item form
		IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 87 AND numFieldId = 189 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (87,189,1,1,@numDomainId,0,0,2,0)
		END
		IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 87 AND numFieldId = 270 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (87,270,1,2,@numDomainId,0,0,2,0)
		END
		IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 87 AND numFieldId = 272 AND numDomainId = @numDomainId)
			BEGIN
		INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (87,272,2,1,@numDomainId,0,0,2,0)
		END
		
		--- Inserts resuired field mapping in DynamicFormField_Validation for validation on form
		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 87 AND numFormFieldId = 189 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (87,189,@numDomainId,'Item',1)
		END
		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 87 AND numFormFieldId = 270 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (87,270,@numDomainId,'Income Account',1)
		END
		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 87 AND numFormFieldId = 272 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (87,272,@numDomainId,'COGS Account',1)
		END
		
		 ---- Inserts required field mapping in DycFormConfigurationDetails table so it can not be removed form configuration wizard for new service/lot item form
		 IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 88 AND numFieldId = 189 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (88,189,1,1,@numDomainId,0,0,2,0)
		END
		IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 88 AND numFieldId = 270 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (88,270,1,2,@numDomainId,0,0,2,0)
		END
		IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 88 AND numFieldId = 271 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (88,271,2,1,@numDomainId,0,0,2,0)
		END
		IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 88 AND numFieldId = 272 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (88,272,2,2,@numDomainId,0,0,2,0)
		END
		
		--- Inserts required field mapping in DynamicFormField_Validation for validation on form new service lot item form
		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 88 AND numFormFieldId = 189 AND numDomainId = @numDomainId)
		BEGIN
			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (88,189,@numDomainId,'Item',1)
		END
		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 88 AND numFormFieldId = 270 AND numDomainId = @numDomainId)
		BEGIN
			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (88,270,@numDomainId,'Income Account',1)
		END
		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 88 AND numFormFieldId = 271 AND numDomainId = @numDomainId)
		BEGIN
			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (88,271,@numDomainId,'Asset Account',1)
		END
		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 88 AND numFormFieldId = 272 AND numDomainId = @numDomainId)
		BEGIN
			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (88,272,@numDomainId,'COGS Account',1)
		END
	   
	   FETCH NEXT FROM CursorTreeNavigation INTO @numDomainId;
	END;

	CLOSE CursorTreeNavigation;
	DEALLOCATE CursorTreeNavigation;

---- Maps fields with new inventory item form

	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 189 AND numFormID = 86)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit]) VALUES (4,189,86,1,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 270 AND numFormID = 86)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit]) VALUES (4,270,86,1,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 271 AND numFormID = 86)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit]) VALUES (4,271,86,1,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 272 AND numFormID = 86)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit]) VALUES (4,272,86,1,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 216 AND numFormID = 86)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,216,86,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 212 AND numFormID = 86)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,212,86,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 203 AND numFormID = 86)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,203,86,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 273 AND numFormID = 86)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,273,86,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 193 AND numFormID = 86)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,193,86,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 281 AND numFormID = 86)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,281,86,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 202 AND numFormID = 86)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,202,86,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 274 AND numFormID = 86)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,274,86,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 277 AND numFormID = 86)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,277,86,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 399 AND numFormID = 86)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,399,86,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 215 AND numFormID = 86)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,215,86,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 191 AND numFormID = 86)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,191,86,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 206 AND numFormID = 86)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,206,86,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 207 AND numFormID = 86)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,207,86,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 205 AND numFormID = 86)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,205,86,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 204 AND numFormID = 86)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,204,86,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 209 AND numFormID = 86)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,209,86,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 313 AND numFormID = 86)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,313,86,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 280 AND numFormID = 86)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,280,86,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 276 AND numFormID = 86)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,276,86,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 275 AND numFormID = 86)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,275,86,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 293 AND numFormID = 86)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,293,86,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 233 AND numFormID = 86)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,233,86,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 234 AND numFormID = 86)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,234,86,1,1,1)
	END

---- Maps fields with new non inventory item form

	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 189 AND numFormID = 87)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit]) VALUES (4,189,87,1,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 270 AND numFormID = 87)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit]) VALUES (4,270,87,1,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 272 AND numFormID = 87)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit]) VALUES (4,272,87,1,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 193 AND numFormID = 87)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,193,87,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 202 AND numFormID = 87)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,202,87,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 273 AND numFormID = 87)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,273,87,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 274 AND numFormID = 87)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,274,87,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 190 AND numFormID = 87)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,190,87,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 277 AND numFormID = 87)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,277,87,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 399 AND numFormID = 87)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,399,87,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 215 AND numFormID = 87)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,215,87,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 191 AND numFormID = 87)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,191,87,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 206 AND numFormID = 87)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,206,87,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 204 AND numFormID = 87)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,204,87,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 207 AND numFormID = 87)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,207,87,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 205 AND numFormID = 87)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,205,87,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 313 AND numFormID = 87)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,313,87,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 276 AND numFormID = 87)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,276,87,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 275 AND numFormID = 87)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,275,87,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 280 AND numFormID = 87)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,280,87,1,1,1)
	END

---- Maps fields with new Serialized/Lot item form

	
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 189 AND numFormID = 88)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit]) VALUES (4,189,88,1,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 270 AND numFormID = 88)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit]) VALUES (4,270,88,1,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 271 AND numFormID = 88)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit]) VALUES (4,271,88,1,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 272 AND numFormID = 88)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit]) VALUES (4,272,88,1,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 216 AND numFormID = 88)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,216,88,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 212 AND numFormID = 88)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,212,88,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 203 AND numFormID = 88)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,203,88,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 273 AND numFormID = 88)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,273,88,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 193 AND numFormID = 88)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,193,88,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 281 AND numFormID = 88)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,281,88,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 202 AND numFormID = 88)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,202,88,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 274 AND numFormID = 88)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,274,88,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 277 AND numFormID = 88)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,277,88,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 399 AND numFormID = 88)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,399,88,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 215 AND numFormID = 88)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,215,88,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 191 AND numFormID = 88)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,191,88,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 206 AND numFormID = 88)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,206,88,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 207 AND numFormID = 88)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,207,88,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 205 AND numFormID = 88)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,205,88,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 204 AND numFormID = 88)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,204,88,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 209 AND numFormID = 88)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,209,88,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 313 AND numFormID = 88)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,313,88,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 280 AND numFormID = 88)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,280,88,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 276 AND numFormID = 88)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,276,88,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 275 AND numFormID = 88)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,275,88,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 293 AND numFormID = 88)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,293,88,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 233 AND numFormID = 88)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,233,88,1,1,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 190 AND numFormID = 88)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,190,88,1,1,1)
	END



------------------------------------------------------------------