/******************************************************************
Project: Release 13.8 Date: 05.AUG.2020
Comments: STORE PROCEDURES
*******************************************************************/

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetVendorTransitCount')
DROP FUNCTION fn_GetVendorTransitCount
GO
CREATE FUNCTION [dbo].[fn_GetVendorTransitCount]
(
	@numVendorID NUMERIC,
	@numAddressID NUMERIC,
	@numDomainID NUMERIC
)
RETURNS INT 
AS
BEGIN

	DECLARE @VendorTransitCount INT
 
	SELECT 
		@VendorTransitCount=COUNT(*) 
	FROM 
		OpportunityMaster Opp 
	INNER JOIN 
		OpportunityItems OppI 
	ON 
		opp.numOppId=OppI.numOppId
    WHERE 
		Opp.numDomainId=@numDomainID
		AND Opp.numDivisionId=@numVendorID
		AND Opp.tintOppType= 2 
		AND Opp.tintOppstatus=1 
		AND Opp.tintShipped=0 
		AND OppI.numVendorWareHouse=@numAddressID
		AND (isnull(OppI.numUnitHour,0)-isnull(OppI.numUnitHourReceived,0)>0)

RETURN @VendorTransitCount

END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetItemWOPlannedStartDate')
DROP FUNCTION GetItemWOPlannedStartDate
GO
CREATE FUNCTION [dbo].[GetItemWOPlannedStartDate]
(
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@numQtyToBuild FLOAT
	,@dtPlannedStartDate DATETIME
)    
RETURNS DATETIME
AS
BEGIN
	DECLARE @dtVendorShipDate DATETIME 
	
	SET @dtVendorShipDate = (SELECT 
								MAX(dtVendorShipDate)
							FROM
							(
								SELECT 
									Item.numItemCode
									,(CASE 
										WHEN ISNULL(Item.bitAssembly,0) = 1
										THEN [dbo].[GetItemWOPlannedStartDate](@numDomainID
																			,Item.numItemCode
																			,@numWarehouseID
																			,(CASE 
																				WHEN ISNULL((SELECT TOP 1 ISNULL(numOnHand,0) - ISNULL(numBackOrder,0) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=Item.numItemCode AND numWarehouseID=@numWarehouseID AND ISNULL(numWLocationID,0) = 0 ORDER BY numWarehouseItemID),0) > 0
																				THEN CAST((@numQtyToBuild * ISNULL(ItemDetails.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ItemDetails.numUOMID,0), Item.numItemCode, @numDomainID, ISNULL(Item.numBaseUnit, 0)),1)) AS FLOAT) - ISNULL((SELECT TOP 1 ISNULL(numOnHand,0) - ISNULL(numBackOrder,0) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=Item.numItemCode AND numWarehouseID=@numWarehouseID AND ISNULL(numWLocationID,0) = 0 ORDER BY numWarehouseItemID),0)
																				ELSE CAST((@numQtyToBuild * ISNULL(ItemDetails.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ItemDetails.numUOMID,0), Item.numItemCode, @numDomainID, ISNULL(Item.numBaseUnit, 0)),1)) AS FLOAT)
																			END)
																			,@dtPlannedStartDate)    
										ELSE DATEADD(DAY,ISNULL(dbo.GetVendorPreferredMethodLeadTime(Item.numVendorID,@numWarehouseID),0),@dtPlannedStartDate)
									END) dtVendorShipDate
								FROM 
									ItemDetails
								INNER JOIN
									Item
								ON
									ItemDetails.numChildItemID = Item.numItemCode
								LEFT JOIN
									Vendor
								ON
									Item.numVendorID = Vendor.numVendorID
									AND Item.numItemCode = Vendor.numItemCode
								WHERE
									numItemKitID=@numItemCode
									AND Item.charItemType = 'P'
									AND 1= (CASE 
												WHEN Item.bitAssembly = 1 
												THEN (CASE WHEN ISNULL((SELECT TOP 1 ISNULL(numOnHand,0) - ISNULL(numBackOrder,0) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=Item.numItemCode AND numWarehouseID=@numWarehouseID AND ISNULL(numWLocationID,0) = 0 ORDER BY numWarehouseItemID),0) < CAST((@numQtyToBuild * ISNULL(ItemDetails.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ItemDetails.numUOMID,0), Item.numItemCode, @numDomainID, ISNULL(Item.numBaseUnit, 0)),1)) AS FLOAT) THEN 1 ELSE 0 END)
												ELSE (CASE WHEN ISNULL((SELECT SUM(numOnHand) - SUM(numBackOrder) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=Item.numItemCode AND numWarehouseID=@numWarehouseID),0) < CAST((@numQtyToBuild * ISNULL(ItemDetails.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ItemDetails.numUOMID,0), Item.numItemCode, @numDomainID, ISNULL(Item.numBaseUnit, 0)),1)) AS FLOAT) THEN 1 ELSE 0 END)
											END)
						) TEMP)

	RETURN (CASE WHEN @dtVendorShipDate IS NOT NULL THEN @dtVendorShipDate ELSE @dtPlannedStartDate END)
END
GO
--select dbo.GetDealAmount(1133,getdate(),1610)
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetVendorPreferredMethodLeadTime')
DROP FUNCTION GetVendorPreferredMethodLeadTime
GO
CREATE FUNCTION [dbo].[GetVendorPreferredMethodLeadTime]
(
	@numVendorID NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
)
RETURNS INT    
AS    
BEGIN   
	DECLARE @LeadTime AS INT = NULL

	IF EXISTS (SELECT * FROM VendorShipmentMethod WHERE numVendorID=@numVendorID AND ISNULL(numWarehouseID,0)=@numWarehouseID) 
	BEGIN
		SET @LeadTime = ISNULL((SELECT TOP 1 numListValue FROM VendorShipmentMethod WHERE numVendorID=@numVendorID AND ISNULL(numWarehouseID,0)=@numWarehouseID ORDER BY bitPrimary DESC,bitPreferredMethod DESC),0)
	END
	ELSE
	BEGIN
		SET @LeadTime = ISNULL((SELECT TOP 1 numListValue FROM VendorShipmentMethod WHERE numVendorID=@numVendorID ORDER BY bitPrimary DESC,bitPreferredMethod DESC),0)
	END

	RETURN ISNULL(@LeadTime,0)
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetWorkOrderPlannedStartDate')
DROP FUNCTION GetWorkOrderPlannedStartDate
GO
CREATE FUNCTION [dbo].[GetWorkOrderPlannedStartDate]
(
	@numDomainID NUMERIC(18,0)
	,@numWOID NUMERIC(18,0)
)    
RETURNS DATETIME
AS
BEGIN
	DECLARE @dtCreatedDate DATETIME
	DECLARE @dtMaxItemExpectedDate DATETIME
	DECLARE @dtPlannedStartDate DATETIME
	DECLARE @numWarehouseID NUMERIC(18,0)

	SELECT
		@dtCreatedDate = bintCreatedDate
		,@dtPlannedStartDate = ISNULL(dtmStartDate,bintCreatedDate)
		,@numWarehouseID=ISNULL(WareHouseItems.numWareHouseID,0)
	FROM
		WorkOrder
	LEFT JOIN
		WareHouseItems
	ON
		WorkOrder.numWareHouseItemId = WareHouseItems.numWareHouseItemID
	WHERE
		WorkOrder.numDomainID=@numDomainID
		AND WorkOrder.numWOId = @numWOID

	SET @dtMaxItemExpectedDate = (SELECT 
										MAX(dtItemExpectedDate)
									FROM 
									(
										SELECT 
											(CASE 
												WHEN EXISTS (SELECT WOInner.numWOId FROM WorkOrder WOInner WHERE WOInner.numDomainID=@numDomainID AND WOInner.numItemCode = Item.numItemCode AND WOInner.numParentWOID = @numWOID)
												THEN [dbo].[GetWorkOrderPlannedStartDate](@numDomainID,(SELECT WOInner.numWOId FROM WorkOrder WOInner WHERE WOInner.numDomainID=@numDomainID AND WOInner.numItemCode = Item.numItemCode AND WOInner.numParentWOID = @numWOID))
												ELSE DATEADD(DAY,ISNULL(dbo.GetVendorPreferredMethodLeadTime(Item.numVendorID,@numWarehouseID),0),@dtCreatedDate)
											END) dtItemExpectedDate
										FROM
											WorkOrderDetails
										INNER JOIN
											Item
										ON
											WorkOrderDetails.numChildItemID = Item.numItemCode
										INNER JOIN
											Vendor
										ON
											Item.numVendorID = Vendor.numVendorID
											AND Item.numItemCode = Vendor.numItemCode
										INNER JOIN
											WareHouseItems 
										ON
											WorkOrderDetails.numWareHouseItemId = WareHouseItems.numWareHouseItemID
										WHERE
											numWOId = @numWOID
											AND (EXISTS (SELECT WOInner.numWOId FROM WorkOrder WOInner WHERE WOInner.numDomainID=@numDomainID AND WOInner.numItemCode = Item.numItemCode AND WOInner.numParentWOID = @numWOID) OR (WorkOrderDetails.numQtyItemsReq > ISNULL(WareHouseItems.numAllocation,0)))
									)TEMP)

	IF @dtMaxItemExpectedDate IS NOT NULL AND @dtMaxItemExpectedDate > @dtPlannedStartDate
	BEGIN
		SET @dtPlannedStartDate = @dtMaxItemExpectedDate
	END

	RETURN @dtPlannedStartDate
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p' AND NAME ='USP_BizRecurrence_BizDoc')
DROP PROCEDURE USP_BizRecurrence_BizDoc
GO
  
Create PROCEDURE [dbo].[USP_BizRecurrence_BizDoc]  
	@numRecConfigID NUMERIC(18,0), 
	@numUserCntID NUMERIC(18,0),
	@numOppID NUMERIC(18,0),
	@numOppBizDocID NUMERIC(18,0),
	@numRecurBizDocID NUMERIC(18,0) OUT,
	@bitAuthorative BIT OUT,
	@bitBizDocRecur BIT,
	@numFrequency SMALLINT,
	@numRecurParentOppID NUMERIC(18,0),
	@Date AS DATE
AS  
BEGIN  
BEGIN TRANSACTION;

BEGIN TRY
	--DECLARE @Date AS DATE = GETDATE()
	DECLARE @numBizDocId AS INT
	DECLARE @numAuthBizDocId AS INT
	DECLARE @numDivisionID as numeric(9)
	DECLARE @numDomainID NUMERIC(9)
	DECLARE @tintOppType AS TINYINT	
	DECLARE @numNewOppBizDocID AS NUMERIC(18,0)
	DECLARE @fltExchangeRateBizDoc AS FLOAT
	DECLARE @numSequenceId AS NUMERIC(18,0)
	
	SELECT @fltExchangeRateBizDoc=fltExchangeRate FROM OpportunityMaster where numOppID=@numOppId
	SELECT @numDomainID=numDomainID,@numDivisionID=numDivisionID,@tintOppType=tintOppType FROM OpportunityMaster WHERE numOppID=@numOppId
	SELECT @numBizDocId = numBizDocId FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocID

	SELECT 
		@numSequenceId = ISNULL(MAX(CAST(ISNULL(OBD.numSequenceId,0) AS BigInt)),0) + 1 
	FROM 
		dbo.OpportunityMaster OM 
	JOIN 
		dbo.OpportunityBizDocs OBD 
	ON 
		OM.numOppId=OBD.numOppId
	WHERE 
		OM.numDomainId=@numDomainID AND OBD.numBizDocId=@numBizDocId

	--GET Authorative Biz Doc ID
	SELECT @numAuthBizDocId = CASE @tintOppType 
							WHEN 1 THEN [numAuthoritativeSales]
							WHEN 2 THEN [numAuthoritativePurchase]
							ELSE [numAuthoritativePurchase]
						  END
    FROM   
		[AuthoritativeBizDocs]
    WHERE  
		[numDomainId] =@numDomainId


	DECLARE @tintShipped AS TINYINT	
    DECLARE @dtShipped AS DATETIME
    DECLARE @bitAuthBizdoc AS BIT
        
	SELECT @tintShipped=ISNULL(tintShipped,0),@numDomainID=numDomainID FROM OpportunityMaster WHERE numOppId=@numOppId

	IF @tintShipped =1 
			SET @dtShipped = GETUTCDATE();
	ELSE
		SET @dtShipped = null;

	IF @tintOppType = 1 AND @numBizDocId = @numAuthBizDocId
		SET @bitAuthBizdoc = 1
	ELSE if @tintOppType = 2 AND @numBizDocId = @numAuthBizDocId
		SET @bitAuthBizdoc = 1
	ELSE 
		SET @bitAuthBizdoc = 0


	-- CREATE COPY OF PARENT BIZDOC
	INSERT INTO OpportunityBizDocs
    (
		numOppId,numBizDocId,numCreatedBy,dtCreatedDate,numModifiedBy,dtModifiedDate,vcComments,bitPartialFulfilment,
		dtFromDate,numBizDocStatus,[bitAuthoritativeBizDocs],numBizDocTempID,vcRefOrderNo,
		numSequenceId,numBizDocStatusOLD,bitAutoCreated,[numMasterBizdocSequenceID],fltExchangeRateBizDoc
	)
    SELECT 
		@numOppId,@numBizDocId,@numUserCntID,@Date,@numUserCntID,@Date,'',OBD.bitPartialFulfilment,
		DATETIMEFROMPARTS(YEAR(@Date),MONTH(@Date),DAY(@Date),23, 59, 59, 0),0,@bitAuthBizdoc,numBizDocTempID,vcRefOrderNo,
		@numSequenceId,0,1,@numSequenceId,@fltExchangeRateBizDoc
	FROM 
		OpportunityBizDocs OBD 
	WHERE   
		OBD.numOppBizDocsId=@numOppBizDocID

	SET @numNewOppBizDocID = SCOPE_IDENTITY()

	EXEC USP_OpportunityBizDocs_CT @numDomainID=@numDomainID,@numUserCntID=@numUserCntID,@numRecordID=@numNewOppBizDocID

	-- UPDATE BIZ DOC NAME 
	EXEC dbo.USP_UpdateBizDocNameTemplate @tintOppType,@numDomainID,@numNewOppBizDocID

	IF @numBizDocId = 297 OR @numBizDocId = 299
	BEGIN
		IF NOT EXISTS(SELECT * FROM dbo.NameTemplate WHERE numDomainID=@numDomainID AND numRecordID=@numBizDocId AND tintModuleID=2)
		BEGIN
			INSERT INTO NameTemplate
			(numDomainId,tintModuleID,numRecordID,vcNameTemplate,numSequenceId,numMinLength)
			SELECT 
				@numDomainId,2,@numBizDocId,UPPER(SUBSTRING(dbo.GetListIemName(@numBizDocId),0,4)) + '-',1,4

			SET @numSequenceId = 1
		END
		ELSE
		BEGIN
			SELECT @numSequenceId=numSequenceId FROM NameTemplate WHERE numDomainID=@numDomainID AND numRecordID=@numBizDocId AND tintModuleID=2
			UPDATE NameTemplate SET numSequenceId=ISNULL(numSequenceId,0) + 1 WHERE numDomainID=@numDomainID AND numRecordID=@numBizDocId AND tintModuleID=2
		END
	END

	UPDATE OpportunityBizDocs SET vcBizDocID=vcBizDocName + ISNULL(CAST(@numSequenceId AS VARCHAR(10)),''),monDealAmount=ISNULL(dbo.[GetDealAmount](@numOppId,@Date,@numNewOppBizDocID),0) WHERE  numOppBizDocsId = @numNewOppBizDocID

	IF @bitBizDocRecur = 1
	BEGIN
		-- Insert newly created bizdoc to transaction 
		INSERT INTO RecurrenceTransaction ([numRecConfigID],[numRecurrOppBizDocID],[dtCreatedDate])	VALUES (@numRecConfigID,@numNewOppBizDocID,GETDATE())

		-- Insert newly created bizdoc to transaction histrory table. Records are not deleted in this table when opp or bizdoc is deleted 
		INSERT INTO RecurrenceTransactionHistory ([numRecConfigID],[numRecurrOppBizDocID],[dtCreatedDate])	VALUES (@numRecConfigID,@numNewOppBizDocID,GETDATE())

		--Increase value of number of transaction completed by 1
		UPDATE RecurrenceConfiguration SET numTransaction = ISNULL(numTransaction,0) + 1 WHERE numRecConfigID = @numRecConfigID

		INSERT INTO OpportunityBizDocItems                                                                          
  		(
			numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,
			vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes,bitEmbeddedCost,
			monEmbeddedCost,dtRentalStartDate,dtRentalReturnDate,tintTrackingStatus
		)
		SELECT 
			@numNewOppBizDocID,
			OBDI.numOppItemID,
			OBDI.numItemCode,
			(CASE 
			WHEN OBDI.numUnitHour > TEMP.QtytoFulFill THEN TEMP.QtytoFulFill
			ELSE OBDI.numUnitHour
			END) AS numUnitHour,
			OBDI.monPrice,
			(CASE 
			WHEN OBDI.numUnitHour > TEMP.QtytoFulFill THEN ((OI.monTotAmount / OI.numUnitHour) * TEMP.QtytoFulFill)
			ELSE OBDI.monTotAmount
			END) AS monTotAmount,
			OBDI.vcItemDesc,
			OBDI.numWarehouseItmsID,
			OBDI.vcType,
			OBDI.vcAttributes,
			OBDI.bitDropShip,
			OBDI.bitDiscountType,
			(CASE 
			WHEN OBDI.numUnitHour > TEMP.QtytoFulFill 
			THEN 
				(CASE 
				WHEN OBDI.bitDiscountType = 0 THEN OBDI.fltDiscount
				WHEN OBDI.bitDiscountType = 1 THEN ((OI.fltDiscount/OI.numUnitHour) * TEMP.QtytoFulFill)
				END)
			ELSE OBDI.fltDiscount
			END) As fltDiscount,
			OBDI.monTotAmtBefDiscount,
			OBDI.vcNotes,
			OBDI.bitEmbeddedCost,
			OBDI.monEmbeddedCost,
			OBDI.dtRentalStartDate,
			OBDI.dtRentalReturnDate,
			OBDI.tintTrackingStatus
		FROM
			OpportunityBizDocs OBD 
		JOIN 
			OpportunityBizDocItems OBDI 
		ON 
			OBD.numOppBizDocsId=OBDI.numOppBizDocID 
		JOIN
			OpportunityItems OI
		ON
			OBDI.numOppItemID = OI.numoppitemtCode
		INNER JOIN
		(
			SELECT
				*
			FROM
				(SELECT 
					OI.numoppitemtCode,   
					OI.numUnitHour AS QtyOrdered,
					(OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour), 0)) AS QtytoFulFill                                   
				FROM      
					OpportunityItems OI
				LEFT JOIN 
					OpportunityMaster OM 
				ON 
					OI.numOppID = oM.numOppID 
				LEFT JOIN 
					OpportunityBizDocItems OBI 
				ON 
					OBI.numOppItemID = OI.numoppitemtCode AND OBI.numOppBizDocId IN (SELECT numOppBizDocsId FROM OpportunityBizDocs OB WHERE OB.numOppId = OI.numOppId AND ISNULL(OB.bitAuthoritativeBizDocs,0) = 1)
				LEFT JOIN 
					WareHouseItems WI 
				ON 
					WI.numWareHouseItemID = OI.numWarehouseItmsID
				INNER JOIN 
					Item I 
				ON 
					I.[numItemCode] = OI.[numItemCode]
				WHERE     
					OI.numOppId = @numOppId
				GROUP BY  
					OI.numoppitemtCode,
					OI.numUnitHour) X
				WHERE   
					X.QtytoFulFill > 0
				) AS TEMP
		ON
			OBDI.numOppItemID = TEMP.numoppitemtCode 
		WHERE
			OBDI.numOppBizDocID = @numOppBizDocID	
	
		--Set isCompleted to true if total quantity of all items is used in recurring bizdoc
		IF(SELECT 
				COUNT(*)
		   FROM
				OpportunityBizDocItems OBDI 
		   INNER JOIN
				(
					SELECT 
						*
					FROM
						(SELECT 
							OI.numoppitemtCode,   
							OI.numUnitHour AS QtyOrdered,
							(OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour), 0)) AS QtytoFulFill                                   
						FROM      
							OpportunityItems OI
						LEFT JOIN 
							OpportunityMaster OM 
						ON 
							OI.numOppID = oM.numOppID 
						LEFT JOIN 
							OpportunityBizDocItems OBI 
						ON 
							OBI.numOppItemID = OI.numoppitemtCode AND OBI.numOppBizDocId IN (SELECT numOppBizDocsId FROM OpportunityBizDocs OB WHERE OB.numOppId = OI.numOppId AND ISNULL(OB.bitAuthoritativeBizDocs,0) = 1)
						LEFT JOIN 
							WareHouseItems WI 
						ON 
							WI.numWareHouseItemID = OI.numWarehouseItmsID
						INNER JOIN 
							Item I 
						ON 
							I.[numItemCode] = OI.[numItemCode]
						WHERE     
							OI.numOppId = @numOppId
						GROUP BY  
							OI.numoppitemtCode,
							OI.numUnitHour) X
						WHERE   
							X.QtytoFulFill > 0
				) AS TEMP
			ON
				OBDI.numOppItemID = TEMP.numoppitemtCode
			WHERE
				OBDI.numOppBizDocID=@numOppBizDocID) > 0
			BEGIN
				--Get next recurrence date for bizdoc 
				DECLARE @dtNextRecurrenceDate DATE = NULL

				SET @dtNextRecurrenceDate = (CASE @numFrequency
											WHEN 1 THEN DATEADD(D,1,@Date) --Daily
											WHEN 2 THEN DATEADD(D,7,@Date) --Weekly
											WHEN 3 THEN DATEADD(M,1,@Date) --Monthly
											WHEN 4 THEN DATEADD(M,4,@Date)  --Quarterly
											END)

				UPDATE RecurrenceConfiguration SET dtNextRecurrenceDate = @dtNextRecurrenceDate WHERE numRecConfigID = @numRecConfigID
			END
			ELSE
			BEGIN
				UPDATE RecurrenceConfiguration SET bitCompleted = 1 WHERE numRecConfigID = @numRecConfigID
			END

		UPDATE OpportunityBizDocs SET bitRecurred = 1 WHERE numOppBizDocsId = @numNewOppBizDocID
	END
	ELSE
	BEGIN
		-- INSERTS BizDoc Items
		INSERT INTO OpportunityBizDocItems                                                                          
  		(
			numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,
			vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes,bitEmbeddedCost,
			monEmbeddedCost,dtRentalStartDate,dtRentalReturnDate,tintTrackingStatus
		)
		SELECT 
			@numNewOppBizDocID,OINew.numoppitemtCode,OBDI.numItemCode,OBDI.numUnitHour,OBDI.monPrice,OBDI.monTotAmount,OBDI.vcItemDesc,OBDI.numWarehouseItmsID,OBDI.vcType,
			OBDI.vcAttributes,OBDI.bitDropShip,OBDI.bitDiscountType,OBDI.fltDiscount,OBDI.monTotAmtBefDiscount,OBDI.vcNotes,OBDI.bitEmbeddedCost,
			OBDI.monEmbeddedCost,OBDI.dtRentalStartDate,OBDI.dtRentalReturnDate,OBDI.tintTrackingStatus
		FROM 
			OpportunityBizDocs OBD 
		JOIN 
			OpportunityBizDocItems OBDI 
		ON 
			OBD.numOppBizDocsId=OBDI.numOppBizDocID 
		JOIN 
			OpportunityItems OI 
		ON 
			OBDI.numOppItemID=OI.numoppitemtCode
		JOIN
			OpportunityItems OINew
		ON
			OINew.numRecurParentOppItemID = OI.numoppitemtCode
		JOIN 
			Item I 
		ON 
			OBDI.numItemCode=I.numItemCode
		WHERE  
			OBD.numOppId=@numRecurParentOppID 
			AND OBD.numOppBizDocsId=@numOppBizDocID
	END

	IF ISNULL((SELECT numBizDocID FROM OpportunityBizDocs WHERE numOppBizDocsID=@numNewOppBizDocID),0) = 29397
	BEGIN
		UPDATE
			OI
		SET
			OI.numQtyPicked = ISNULL((SELECT 
											SUM(OBDI.numUnitHour)
										FROM 
											OpportunityBizDocs OBD 
										INNER JOIN 
											OpportunityBizDocItems OBDI
										ON
											OBD.numOppBizDocsID = OBDI.numOppBizDocID
											AND OI.numoppitemtCode = OBDI.numOppItemID 
										WHERE 
											OBD.numOppID = OI.numOppID 
											AND OBD.numBizDocID = 29397),0)
		FROM	
			OpportunityItems OI
		WHERE
			OI.numOppID = @numOppId
	END

	SET @numRecurBizDocID = @numNewOppBizDocID
	SET @bitAuthorative = @bitAuthBizdoc
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);

	IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION;
END CATCH

IF @@TRANCOUNT > 0
    COMMIT TRANSACTION;

END  
/****** Object:  StoredProcedure [dbo].[USP_CaseDetails]    Script Date: 07/26/2008 16:15:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_CaseDetails @numCaseId = 34              
              
              
--sp_helptext USP_CaseDetails              
              
              
 --created by anoop jayaraj                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_casedetails')
DROP PROCEDURE usp_casedetails
GO
CREATE PROCEDURE [dbo].[USP_CaseDetails]                                  
@numCaseId as numeric(9) ,    
@numDomainID as numeric(9),    
@ClientTimeZoneOffset as int                                   
                                  
as                                  
                                  
select C.numContactID,vcCaseNumber,
 intTargetResolveDate,textSubject,Div.numDivisionID,                                  
 numStatus,dbo.GetListIemName(numStatus) AS vcCaseStatus, numPriority,textDesc,tintCRMType,                                  
 textInternalComments,numReason,C.numContractID,
 ISNULL((SELECT cm.vcContractName FROM dbo.ContractManagement cm WHERE cm.numContractId = C.numContractId),'') AS vcContractName,
 numOrigin,numType,                       
 dbo.fn_GetContactName(C.numCreatedby)+' ' + convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,C.bintCreatedDate)) as CreatedBy,                                  
 dbo.fn_GetContactName(C.numModifiedBy)+' ' + convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,C.bintModifiedDate)) as ModifiedBy,                      
 dbo.fn_GetContactName(C.numRecOwner) as RecOwner,                                
 C.numRecOwner,
 tintSupportKeyType,                                  
 C.numAssignedTo,C.numAssignedBy,isnull(Addc.vcFirstName,'') + ' ' + isnull(Addc.vcLastName,'') as vcName,                                  
 isnull(vcEmail,'') as vcEmail,isnull(numPhone,'') + case when numPhoneExtension is null then '' when numPhoneExtension ='' then '' else ', '+ numPhoneExtension end  as Phone,   
         isnull(numPhone,'') AS numPhone,
         isnull(numPhoneExtension,'') AS PhoneExtension,
		 		 (SELECT TOP 1 ISNULL(vcData,'') FROM ListDetails WHERE numListID=5 AND numListItemID=com.numCompanyType AND numDomainId=@numDomainID) AS vcCompanyType,
 Com.numCompanyId,Com.vcCompanyName,(select count(*) from Comments where numCaseID =@numCaseId)  as NoofCases,
 (SELECT COUNT(*) FROM   dbo.GenericDocuments WHERE  numRecID = @numCaseId AND vcDocumentSection = 'CS') AS DocumentCount, Div.numTerID,
 ISNULL((SELECT (ISNULL(C.numIncidents,0)  - ISNULL(C.numIncidentsUsed,0)) FROM Contracts AS C WHERE C.numDivisonId=Div.numDivisionID AND numDomainId=@numDomainID AND intType=3),0) AS numIncidentLeft,

  ISNULL(((SELECT TOP 1 CASE WHEN  ISNULL((SELECT numWarrantyDays FROM Contracts AS C WHERE  numDomainId=@numDomainID AND I.numItemClassification=C.vcItemClassification AND intType=2),0)>0 THEN ISNULL((SELECT vcNotes FROM Contracts AS C WHERE  numDomainId=@numDomainID AND I.numItemClassification=C.vcItemClassification AND intType=2),'-') ELSE '-' END  FROM OpportunityItems AS OI LEFT JOIN OpportunityMaster AS O ON O.numOppId=OI.numOppId LEFT JOIN Item I ON OI.numItemCode=I.numItemCode WHERE O.numDivisionId=Addc.numDivisionId AND O.numDomainId=@numDomainID AND
 1 =(CASE WHEN OI.numoppitemtCode IN (SELECT numOppItemID FROM CaseOpportunities WHERE numCaseId=C.numCaseId) THEN 1
 ELSE 0 END
 )
AND O.tintOppType=1 AND O.tintOppStatus=1 AND O.tintshipped=1 ORDER BY O.bintCreatedDate DESC)),'') AS vcContractNotes,
 ISNULL((SELECT TOP 1 CASE WHEN ISNULL((SELECT numWarrantyDays FROM Contracts AS C WHERE  numDomainId=@numDomainID AND I.numItemClassification=C.vcItemClassification  AND intType=2),0)>0 THEN ISNULL((SELECT numWarrantyDays FROM Contracts AS C WHERE  numDomainId=@numDomainID AND I.numItemClassification=C.vcItemClassification AND intType=2),0)-ISNULL(DATEDIFF(DAY, O.bintClosedDate, GETDATE()),0) ELSE 0 END  FROM OpportunityItems AS OI LEFT JOIN OpportunityMaster AS O ON O.numOppId=OI.numOppId LEFT JOIN Item I ON OI.numItemCode=I.numItemCode WHERE O.numDivisionId=Addc.numDivisionId AND O.numDomainId=@numDomainID AND
 1 =(CASE WHEN OI.numoppitemtCode IN (SELECT numOppItemID FROM CaseOpportunities WHERE numCaseId=C.numCaseId) THEN 1
 ELSE 0 END
 )
  AND O.tintOppType=1 AND O.tintOppStatus=1 AND O.tintshipped=1 ORDER BY O.bintCreatedDate DESC),0) AS numWarrantyDaysLeft,
  ISNULL((SELECT TOP 1 I.vcItemName FROM OpportunityItems AS OI LEFT JOIN OpportunityMaster AS O ON O.numOppId=OI.numOppId LEFT JOIN Item I ON OI.numItemCode=I.numItemCode WHERE O.numDivisionId=Addc.numDivisionId AND O.numDomainId=@numDomainID AND
 1 =(CASE WHEN OI.numoppitemtCode IN (SELECT numOppItemID FROM CaseOpportunities WHERE numCaseId=C.numCaseId) THEN 1
 ELSE 0 END
 )
  AND O.tintOppType=1 AND O.tintOppStatus=1 AND O.tintshipped=1 ORDER BY O.bintCreatedDate DESC),'-') AS vcWarrantyItemName,
  ISNULL((SELECT TOP 1 I.numItemCode FROM OpportunityItems AS OI LEFT JOIN OpportunityMaster AS O ON O.numOppId=OI.numOppId LEFT JOIN Item I ON OI.numItemCode=I.numItemCode WHERE O.numDivisionId=Addc.numDivisionId AND O.numDomainId=@numDomainID AND
 1 =(CASE WHEN OI.numoppitemtCode IN (SELECT numOppItemID FROM CaseOpportunities WHERE numCaseId=C.numCaseId) THEN 1
 ELSE 0 END
 )
  AND O.tintOppType=1 AND O.tintOppStatus=1 AND O.tintshipped=1 ORDER BY O.bintCreatedDate DESC),0) AS numWarrantyItemCode
 from Cases C                                  
 left join AdditionalContactsInformation Addc                                  
 on Addc.numContactId=C.numContactID                                   
 join DivisionMaster Div                                  
 on Div.numDivisionId=Addc.numDivisionId                                  
 join CompanyInfo Com                                   
 on Com.numCompanyID=div.numCompanyID                                  
                                 
 where numCaseId=@numCaseId and C.numDomainID=@numDomainID
GO
--created by anoop jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_casemanage')
DROP PROCEDURE usp_casemanage
GO
CREATE PROCEDURE [dbo].[USP_CaseManage]                      
@numCaseId as numeric(9)=0 output,                      
@numDivisionID as numeric(9)=null,                      
@numContactId as numeric(9)=null,                      
@vcCaseNumber as varchar(50)='',                      
@textSubject as varchar(2000)='',                      
@textDesc as text='',                      
@numUserCntID as numeric(9)=null,                                  
@numDomainID as numeric(9)=null,                      
@tintSupportKeyType as tinyint,                      
@intTargetResolveDate as datetime,                      
@numStatus as numeric(9)=null,                      
@numPriority as numeric(9)=null,                      
@textInternalComments as text='',                      
@numReason as numeric(9)=null,                      
@numOrigin  as numeric(9)=null,                      
@numType as numeric(9)=null,                      
@numAssignedTo as numeric(9)=0,              
@numContractId as numeric(9)=0                   
as                      
                
if @intTargetResolveDate='Jan  1 1753 12:00:00:000AM' set @intTargetResolveDate=null                        
if @numCaseId = 0                      
begin                              
insert into Cases (                      
 numDivisionID,                      
 numContactId,                      
 vcCaseNumber,                      
 textSubject,                      
 textDesc,                      
 numCreatedBy,                      
 bintCreatedDate,                      
 numModifiedBy,                      
 bintModifiedDate,                      
 numDomainID,                      
 tintSupportKeyType,                      
 intTargetResolveDate,          
 numRecOwner ,    
 numContractId,
 numStatus,numPriority,numOrigin,numType,numReason,textInternalComments                 
 )                      
 values                      
 (                      
 @numDivisionID,                      
 @numContactId,                      
 @vcCaseNumber,                      
 @textSubject,                      
 @textDesc,                      
 @numUserCntID,                      
 getutcdate(),                      
 @numUserCntID,                      
 getutcdate(),                      
 @numDomainID,                      
 @tintSupportKeyType,                      
 @intTargetResolveDate,          
 @numUserCntID ,    
 @numContractId,
 @numStatus,@numPriority,@numOrigin,@numType,@numReason,@textInternalComments
 )                      
set @numCaseId=SCOPE_IDENTITY()              

IF EXISTS(SELECT * FROM Contracts WHERE numDivisonId=@numDivisionID AND numDomainId=@numDomainID AND intType=3 AND (ISNULL(numIncidents,0) - ISNULL(numIncidentsUsed,0))>0)
BEGIN
	UPDATE 
		Contracts
	SET 
		numIncidentsUsed = ISNULL(numIncidentsUsed,0)+1,
		numModifiedBy=@numUserCntID,
		dtmModifiedOn=GETUTCDATE()
	WHERE
		numDomainId=@numDomainID AND numDivisonId=@numDivisionID  AND intType=3
	INSERT INTO ContractsLog (
		intType, numDivisionID, numReferenceId, dtmCreatedOn, numCreatedBy,vcDetails,numUsedTime,numBalance,numContractId
	)
	SELECT 
		3,@numDivisionID,@numCaseId,GETUTCDATE(),@numUserCntID,@vcCaseNumber,1,(ISNULL(numIncidents,0) - ISNULL(numIncidentsUsed,0)),numContractId
	FROM Contracts WHERE numDivisonId=@numDivisionID AND numDomainId=@numDomainID AND intType=3 
END



      EXEC dbo.usp_CaseDefaultAssociateContacts @numCaseId=@numCaseId,@numDomainId=@numDomainId
  --Map Custom Field	
  DECLARE @tintPageID AS TINYINT;SET @tintPageID=3
  	
  EXEC dbo.USP_AddParentChildCustomFieldMap
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@numRecordID = @numCaseId, --  numeric(18, 0)
	@numParentRecId = @numDivisionId, --  numeric(18, 0)
	@tintPageID = @tintPageID --  tinyint
	               
end                      
                       
else                      
                      
begin                      
		-- Checking For Incidents
		declare @update as bit 
		set @update = 0
		if @numContractId <> 0
		begin
		IF (SELECT bitIncidents FROM [ContractManagement] WHERE numdomainId = @numDomainID  and numcontractId = @numContractId) = 1
		BEGIN
			
			declare @Usedincidents as numeric(9)
			set @Usedincidents = 0
			declare @Availableincidents as numeric(9)
			

			select @Usedincidents=@Usedincidents+count(*) from cases 
				where numcontractId = @numContractId and numdomainId = @numDomainID and numCaseId <> @numCaseId

			select @Usedincidents=@Usedincidents+count(*) from projectsmaster 
				where numcontractId = @numContractId and numdomainId = @numDomainID 


			select @Availableincidents=isnull(numincidents,0) from contractManagement 
			where numdomainId = @numDomainID  and numcontractId = @numContractId
print '@Usedincidents-----'
print @Usedincidents
print @Availableincidents
			if @Usedincidents >=@Availableincidents
				set @update = 1
			END
		END 	
    
	if @update = 0
	begin 
			update Cases      
			set                      
			                      
			intTargetResolveDate=@intTargetResolveDate,                      
			textSubject=@textSubject,                      
			numStatus=@numStatus,                      
			numPriority=@numPriority,                      
			textDesc=@textDesc,                      
			textInternalComments=@textInternalComments,                      
			numReason=@numReason,                      
			numOrigin=@numOrigin,                      
			numType=@numType,                      
			numModifiedBy=@numUserCntID,                      
			bintModifiedDate=getutcdate() ,    
			 numContractId = @numContractId,
			 numContactID = @numContactID                              
			where numCaseId=@numCaseId 
			set @numCaseId =@numCaseId
	end       
	else
			set @numCaseId = 0	          
                                
end         
        
        
          
 ---Updating if Case is assigned to someone              
  declare @tempAssignedTo as numeric(9)            
  set @tempAssignedTo=null             
  select @tempAssignedTo=isnull(numAssignedTo,0) from Cases where  numCaseId=@numCaseId              
          
  if (@tempAssignedTo<>@numAssignedTo and  @numAssignedTo<>'0')            
  begin              
    update Cases set numAssignedTo=@numAssignedTo ,numAssignedBy=@numUserCntID where  numCaseId=@numCaseId             
  end    
  else if  (@numAssignedTo =0)            
  begin            
   update Cases set numAssignedTo=0 ,numAssignedBy=0 where  numCaseId=@numCaseId          
  end
GO
/****** Object:  StoredProcedure [dbo].[Usp_cflDeleteField]    Script Date: 07/26/2008 16:15:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec Usp_cflDeleteField 2  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_cfldeletefield')
DROP PROCEDURE usp_cfldeletefield
GO
CREATE PROCEDURE [dbo].[Usp_cflDeleteField]      
@fld_id as numeric(9)=NULL,
@numDomainID AS NUMERIC
as      
      
IF @numDomainID >0
BEGIN

	IF(EXISTS(SELECT 1 FROM eCommerceDTL WHERE numDomainId = @numDomainID AND bitElasticSearch = 1))
	BEGIN
		
		IF (EXISTS (SELECT 1 FROM CFW_Fld_Master where fld_id=@fld_id AND Grp_id IN (5,9)))
		BEGIN
			INSERT INTO ElasticSearchBizCart (numDomainID,vcValue,vcAction,vcModule)
			SELECT @numDomainID,
			CASE 
				WHEN Grp_id=5 THEN REPLACE(Fld_label,' ','') + '_C'
				WHEN Grp_id=9 THEN REPLACE(Fld_label,' ','') + '_' + CAST(Fld_Id AS VARCHAR)
			END
			,'Delete'
			,'CustomField'
			FROM CFW_Fld_Master where fld_id=@fld_id AND Grp_id IN (5,9)
		END
	END


	DELETE FROM DycFormConfigurationDetails WHERE bitCustom =1 AND numFieldID = @fld_id AND numDomainId=@numDomainID -- Bug id 278 permenant fix 

	delete from listdetails where numlistID=(select numlistID from  CFW_Fld_Master where fld_id=@fld_id)    
	delete from listmaster where numListID=(select numlistID from  CFW_Fld_Master where fld_id=@fld_id)
	delete from CFW_Fld_Values_Product where Fld_ID=@fld_id   
	delete from CFW_FLD_Values where Fld_ID=@fld_id    
	delete from CFW_FLD_Values_Case where fld_id=@fld_id    
	delete from CFW_FLD_Values_Cont where fld_id=@fld_id    
	delete from CFW_FLD_Values_Item where fld_id=@fld_id      
	delete from CFW_Fld_Values_Opp where fld_id=@fld_id 
	DELETE FROM [dbo].[CFW_Fld_Values_OppItems] where fld_id=@fld_id
	DELETE FROM ItemAttributes WHERE FLD_ID=@fld_id
	
	DELETE FROM dbo.CFW_Validation WHERE numFieldID = @fld_id 
	delete from CFW_Fld_Dtl where numFieldId=@fld_id     
	delete from CFW_Fld_Master where fld_id=@fld_id
	
				

END       
GO
/****** Object:  StoredProcedure [dbo].[USP_cflManage]    Script Date: 07/26/2008 16:15:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_cflmanage')
DROP PROCEDURE usp_cflmanage
GO
CREATE PROCEDURE [dbo].[USP_cflManage]          
@locid as tinyint=0,          
@fldtype as varchar (30)='',          
@fldlbl as varchar(50)='',          
@FieldOdr as tinyint=0,          
@userId as numeric(9)=0,          
@TabId as numeric(9)=0,          
@fldId as numeric(9) OUTPUT,      
@numDomainID as numeric(9)=0,  
@vcURL as varchar(1000)='',
@vcToolTip AS VARCHAR(1000)='',         
@ListId as numeric(9) OUTPUT,
@bitAutocomplete AS BIT
AS
BEGIN          
     
	 DECLARE @bitElasticSearch BIT

	SET @bitElasticSearch = ISNULL((SELECT TOP 1 bitElasticSearch FROM 
	eCommerceDTL WHERE numDomainId = @numDomainID AND bitElasticSearch = 1),0)


if @fldId =0          
begin          
  --declare @ListId as numeric(9)          
            
            
  if @fldtype='SelectBox' OR @fldtype='CheckBoxList'          
  begin          
  INSERT INTO ListMaster           
   (          
   vcListName,           
   numCreatedBy,           
   bintCreatedDate,           
   bitDeleted,           
   bitFixed ,    
   numModifiedBy,    
   bintModifiedDate,    
   numDomainID,    
   bitFlag,numModuleId         
   )           
   values          
   (          
   @fldlbl,          
   1,          
   getutcdate(),          
   0,          
   0,    
   1,    
   getutcdate(),     
   @numDomainID,    
   0,8        
   )          
            
  SET @ListId = SCOPE_IDENTITY()
         
  end          
            
  Insert into cfw_fld_master          
   (          
   Fld_type,          
   Fld_label,          
   Fld_tab_ord,          
   Grp_id,          
   subgrp,          
   numlistid,      
   numDomainID,  
	   vcURL,vcToolTip,bitAutocomplete         
   )           
   values          
   (          
   @fldtype,          
   @fldlbl,          
   @FieldOdr,          
   @locid,          
   @TabId,          
   @ListId,      
   @numDomainID,  
   @vcURL,@vcToolTip, @bitAutoComplete          
   )       
   
  SET @fldId = SCOPE_IDENTITY() --(SELECT MAX(fld_id) FROM dbo.CFW_Fld_Master WHERE numDomainID = @numDomainID)
   
   IF(@bitElasticSearch = 1)
   BEGIN
		IF (EXISTS (SELECT 1 FROM CFW_Fld_Master where fld_id=@fldId AND Grp_id IN (5,9)))
		BEGIN
			INSERT INTO ElasticSearchBizCart (numDomainID,vcValue,vcAction,vcModule)
			SELECT @numDomainID,
			CASE 
				WHEN Grp_id=5 THEN REPLACE(Fld_label,' ','') + '_C'
				WHEN Grp_id=9 THEN REPLACE(Fld_label,' ','') + '_' + CAST(Fld_Id AS VARCHAR)
			END
			,'Insert'
			,'CustomField'
			FROM CFW_Fld_Master where fld_id=@fldId AND Grp_id IN (5,9)
		END
	END

end          
          
else           
begin          
 if @fldtype='SelectBox' OR @fldtype='CheckBoxList'          
  begin         
    
    declare @ddlName as varchar(100)        
  select @ddlName=Fld_label from cfw_fld_master where Fld_id=@fldId  and numDomainID =@numDomainID     
         
  update  ListMaster set  vcListName=@fldlbl where vcListName=@ddlName  and    numDomainID =@numDomainID     
        
  end  
  
   IF(@bitElasticSearch = 1)
   BEGIN
		IF (EXISTS (SELECT 1 FROM CFW_Fld_Master where fld_id=@fldId AND Grp_id IN (5,9)))
		BEGIN
			IF(@locid NOT IN (5,9))
			BEGIN
				INSERT INTO ElasticSearchBizCart (numDomainID,vcValue,vcAction,vcModule)
				SELECT @numDomainID,
				CASE 
					WHEN Grp_id=5 THEN REPLACE(Fld_label,' ','') + '_C'
					WHEN Grp_id=9 THEN REPLACE(Fld_label,' ','') + '_' + CAST(Fld_Id AS VARCHAR)
				END
				,'Delete'
				,'CustomField'
				FROM CFW_Fld_Master where fld_id=@fldId AND Grp_id IN (5,9)
			END
			ELSE
			BEGIN
				IF(NOT EXISTS (SELECT 1 FROM CFW_Fld_Master where fld_id=@fldId AND Grp_id = @locid AND Fld_label = @fldlbl))
				BEGIN
					DECLARE @UpdateCustomField AS VARCHAR(100)  
					SELECT @UpdateCustomField = CASE 
						WHEN @locid=5 THEN REPLACE(@fldlbl,' ','') + '_C' 
						WHEN @locid=9 THEN REPLACE(@fldlbl,' ','') + '_' + CAST(@fldId AS VARCHAR)
					END

					INSERT INTO ElasticSearchBizCart (numDomainID,vcValue,vcAction,vcModule)
					SELECT @numDomainID,
					CASE 
						WHEN Grp_id=5 THEN REPLACE(Fld_label,' ','') + '_C'  
						WHEN Grp_id=9 THEN REPLACE(Fld_label,' ','') + '_' + CAST(Fld_Id AS VARCHAR) 
					END + '||' + @UpdateCustomField
					,'Update'
					,'CustomField'
					FROM CFW_Fld_Master where fld_id=@fldId AND Grp_id IN (5,9)
				END
			END
		END	
		ELSE IF(@locid IN (5,9))
		BEGIN
			INSERT INTO ElasticSearchBizCart (numDomainID,vcValue,vcAction,vcModule)
			SELECT @numDomainID,
			CASE 
				WHEN @locid=5 THEN REPLACE(@fldlbl,' ','') + '_C'
				WHEN @locid=9 THEN REPLACE(@fldlbl,' ','') + '_' + CAST(@fldId AS VARCHAR)
			END
			,'Insert'
			,'CustomField'
		END
    END

 update cfw_fld_master set           
          
  Fld_label=@fldlbl,          
  Grp_id=@locid,          
  subgrp=@TabId,  
  vcURL= @vcURL,vcToolTip=@vcToolTip, bitAutoComplete = @bitAutoComplete        
 where Fld_id=@fldId and   numDomainID =@numDomainID         
            
    
END
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_createbizdocs')
DROP PROCEDURE usp_createbizdocs
GO
CREATE PROCEDURE [dbo].[USP_CreateBizDocs]                        
(                                              
 @numOppId as numeric(9)=null,                        
 @numBizDocId as numeric(9)=null,                        
 @numUserCntID as numeric(9)=null,                        
 @numOppBizDocsId as numeric(9)=NULL OUTPUT,
 @vcComments as varchar(1000),
 @bitPartialFulfillment as bit,
 @strBizDocItems as text='' ,
 @numShipVia as numeric(9),
 @vcTrackingURL as varchar(1000),
 @dtFromDate AS DATETIME,
 @numBizDocStatus as numeric(9) = 0,
 @bitRecurringBizDoc AS BIT = NULL,
 @numSequenceId AS VARCHAR(50) = '',
 @tintDeferred as tinyint=0,
 @monCreditAmount as DECIMAL(20,5)= 0 OUTPUT,
 @ClientTimeZoneOffset as int=0,
 @monDealAmount as DECIMAL(20,5)= 0 OUTPUT,
 @bitRentalBizDoc as bit=0,
 @numBizDocTempID as numeric(9)=0,
 @vcTrackingNo AS VARCHAR(500),
 @vcRefOrderNo AS VARCHAR(100),
 @OMP_SalesTaxPercent AS FLOAT = 0, --(joseph) Added to Get Online Marketplace Sales Tax Percentage
 @numFromOppBizDocsId AS NUMERIC(9)=0,
 @bitTakeSequenceId AS BIT=0,
 @bitAllItems AS BIT=0,
 @bitNotValidateBizDocFulfillment AS BIT=0,
 @fltExchangeRateBizDoc AS FLOAT=0,
 @bitRecur BIT = 0,
 @dtStartDate DATE = NULL,
 @dtEndDate DATE = NULL,
 @numFrequency SMALLINT = 0,
 @vcFrequency VARCHAR(20) = '',
 @numRecConfigID NUMERIC(18,0) = 0,
 @bitDisable bit = 0,
 @bitInvoiceForDeferred BIT = 0,
 @numSourceBizDocId Numeric(18,0)=0,
 @bitShippingBizDoc BIT=0,
 @vcVendorInvoiceName  VARCHAR(100) = NULL,
 @numARAccountID NUMERIC(18,0) = 0
)                        
AS 
BEGIN TRY
	IF ISNULL(@numSequenceId,'') = ''
	BEGIN
		SET @bitTakeSequenceId = 1
	END

	DECLARE @hDocItem as INTEGER
	DECLARE @numDivisionID AS NUMERIC(9)
	DECLARE @numDomainID NUMERIC(9)
	DECLARE @tintOppType AS TINYINT	
	DECLARE @numFulfillmentOrderBizDocId NUMERIC(9);
	DECLARE @bitShippingGenerated BIT=0;

	SELECT 
		@numDomainID=numDomainID,
		@numDivisionID=numDivisionID,
		@tintOppType=tintOppType 
	FROM 
		OpportunityMaster 
	WHERE 
		numOppID=@numOppId

	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
	SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM DivisionMaster WHERE numDivisionID=@numDivisionID

	IF ISNULL(@numFromOppBizDocsId,0) > 0
	BEGIN
		IF (SELECT 
				COUNT(*) 
			FROM 
			(
				SELECT
					OBDI.numOppItemID
					,OI.numUnitHour AS OrderedQty
					,OBDI.numUnitHour AS FromBizDocQty
					,OtherSameBizDocType.BizDocQty
				FROM
					OpportunityBizDocs OBD
				INNER JOIN
					OpportunityBizDocItems OBDI
				ON
					OBD.numOppBizDocsId = OBDI.numOppBizDocID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numoppitemtCode = OBDI.numOppItemID
				OUTER APPLY
				(
					SELECT
						SUM(OpportunityBizDocItems.numUnitHour) BizDocQty
					FROM
						OpportunityBizDocs
					INNER JOIN
						OpportunityBizDocItems
					ON
						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
					WHERE
						OpportunityBizDocs.numOppId = OBD.numOppId
						AND OpportunityBizDocItems.numOppItemID=OBDI.numOppItemID
						AND OpportunityBizDocs.numBizDocId = @numBizDocId
				) AS OtherSameBizDocType
				WHERE
					OBD.numOppBizDocsId = @numFromOppBizDocsId
			) X
			WHERE
				ISNULL(X.FromBizDocQty,0) > (ISNULL(X.OrderedQty,0) - ISNULL(X.BizDocQty,0))) > 0
		BEGIN
			-- IF SOMEONE HAS ALREADY CREATED OTHE SAME BIZDOC TYPE AND ENOUGHT QTY IS NOT LEFT FOR ALL ITEMS OF BIZDOC THAN BIZDOC CAN NOT BE CREATED AGAINST VENDOR INVOICE
			RAISERROR('BIZDOC_ALREADY_GENERATED_AGAINST_SOURCE',16,1)
		END
	END

	SET @numFulfillmentOrderBizDocId=296 
	    
	IF ISNULL(@fltExchangeRateBizDoc,0)=0
		SELECT @fltExchangeRateBizDoc=fltExchangeRate FROM OpportunityMaster where numOppID=@numOppId
	
	IF(@bitShippingBizDoc=1)
	BEGIN
		SET @bitShippingGenerated=1
	END
	ELSE
	BEGIN
	IF((SELECT COUNT(numBizDocId) FROM OpportunityBizDocs WHERE bitShippingGenerated=1 AND numOppId=@numOppId)=0)
	BEGIN
		IF((SELECT TOP 1 CAST(numDefaultSalesShippingDoc AS int) FROM Domain WHERE numDomainId=@numDomainID)=@numBizDocId)
		BEGIN
			SET @bitShippingGenerated=1
		END
	END
	END

	IF @numBizDocTempID=0
	BEGIN
		SELECT 
			@numBizDocTempID=numBizDocTempID 
		FROM 
			BizDocTemplate 
		WHERE 
			numBizDocId=@numBizDocId 
			AND numDomainID=@numDomainID and numOpptype=@tintOppType 
			AND tintTemplateType=0 
			AND isnull(bitDefault,0)=1 
			AND isnull(bitEnabled,0)=1
	END

	IF @numOppBizDocsId > 0 AND @numBizDocId = 29397 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
	BEGIN
		-- Revert Allocation
		EXEC USP_RevertAllocationPickList @numDomainID,@numOppID,@numOppBizDocsId,@numUserCntID
	END

	IF @numOppBizDocsId = 0 AND @numBizDocId = 296 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
	BEGIN
		-- Revert Allocation
		IF ISNULL(@numFromOppBizDocsId,0) = 0 AND ISNULL(@numSourceBizDocId,0) = 0
		BEGIN
			RAISERROR('FULFILLMENT_BIZDOC_REQUIRED_PICKLIST_FOR_ALLOCATE_ON_PICKLIST_SETTING',16,1)
		END
		ELSE IF ISNULL(@numFromOppBizDocsId,0) > 0 AND ISNULL(@numSourceBizDocId,0) = 0
		BEGIN
			SET @numSourceBizDocId = @numFromOppBizDocsId
		END
	END

	IF @numOppBizDocsId=0
	BEGIN
		IF (
			(@bitPartialFulfillment = 1 AND NOT EXISTS (SELECT * FROM OpportunityBizDocs WHERE numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 0))
			 OR (NOT EXISTS (SELECT * FROM OpportunityBizDocs WHERE numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 0)
				 AND NOT EXISTS (SELECT * FROM OpportunityBizDocs WHERE numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 1))
			 OR (@bitRecurringBizDoc = 1) -- Added new flag to add unit qty in case of recurring bizdoc
			 OR (@numFromOppBizDocsId > 0)
			)
		BEGIN
			DECLARE @tintShipped AS TINYINT	
			DECLARE @dtShipped AS DATETIME
			DECLARE @bitAuthBizdoc AS BIT
        
			SELECT 
				@tintShipped=ISNULL(tintShipped,0),
				@numDomainID=numDomainID 
			FROM 
				OpportunityMaster 
			WHERE numOppId=@numOppId                        
		
			IF @tintOppType = 1 AND @bitNotValidateBizDocFulfillment=0
				EXEC USP_ValidateBizDocFulfillment @numDomainID,@numOppId,@numFromOppBizDocsId,@numBizDocId
		

			IF @tintShipped =1 
				SET @dtShipped = GETUTCDATE();
			ELSE
				SET @dtShipped = null;
			IF @tintOppType = 1 AND (SELECT numAuthoritativeSales FROM [AuthoritativeBizDocs] WHERE numDomainID = @numDomainID) = @numBizDocId
				SET @bitAuthBizdoc = 1
			ELSE if @tintOppType = 2 AND (SELECT numAuthoritativePurchase FROM [AuthoritativeBizDocs] WHERE numDomainID = @numDomainID) = @numBizDocId
				SET @bitAuthBizdoc = 1
			ELSE 
				SET @bitAuthBizdoc = 0
		
			Declare @dtCreatedDate as datetime;SET @dtCreatedDate=Getutcdate()
			IF @tintDeferred=1
			BEGIn
				SET @dtCreatedDate=DateAdd(minute, @ClientTimeZoneOffset,@dtFromDate)
			END
	
			IF @bitTakeSequenceId=1
			BEGIN
				--Sequence #
				CREATE TABLE #tempSequence (numSequenceId bigint )
				INSERT INTO #tempSequence EXEC [dbo].[USP_GetScalerValue] @numDomainID ,33,@numBizDocId,0
				SELECT @numSequenceId =ISNULL(numSequenceId,0) FROM #tempSequence
				DROP TABLE #tempSequence
			END	
			
			--To insert Tracking Id of previous record into the new record
			IF @vcTrackingNo IS NULL OR @vcTrackingNo = ''
			BEGIN
				SELECT TOP 1 @vcTrackingNo = vcTrackingNo FROM OpportunityBizDocs WHERE numOppId = @numOppId ORDER BY numOppBizDocsId DESC 
			END
			INSERT INTO OpportunityBizDocs
			(
				numOppId
				,numBizDocId
				,numCreatedBy
				,dtCreatedDate
				,numModifiedBy
				,dtModifiedDate
				,vcComments
				,bitPartialFulfilment
				,numShipVia
				,vcTrackingURL
				,dtFromDate
				,numBizDocStatus
				,[dtShippedDate]
				,[bitAuthoritativeBizDocs]
				,tintDeferred
				,bitRentalBizDoc
				,numBizDocTempID
				,vcTrackingNo
				,vcRefOrderNo
				,numSequenceId
				,numBizDocStatusOLD
				,fltExchangeRateBizDoc
				,bitAutoCreated
				,[numMasterBizdocSequenceID]
				,bitShippingGenerated
				,numSourceBizDocId
				,vcVendorInvoice
				,numARAccountID
			)
			VALUES     
			(
				@numOppId
				,@numBizDocId
				,@numUserCntID
				,@dtCreatedDate
				,@numUserCntID
				,GETUTCDATE()
				,@vcComments
				,@bitPartialFulfillment
				,@numShipVia
				,@vcTrackingURL
				,@dtFromDate
				,@numBizDocStatus
				,@dtShipped
				,ISNULL(@bitAuthBizdoc,0)
				,@tintDeferred
				,@bitRentalBizDoc
				,@numBizDocTempID
				,@vcTrackingNo
				,@vcRefOrderNo
				,@numSequenceId
				,0
				,@fltExchangeRateBizDoc
				,0
				,@numSequenceId
				,@bitShippingGenerated
				,@numSourceBizDocId
				,@vcVendorInvoiceName
				,@numARAccountID
			)
			
        
			SET @numOppBizDocsId = SCOPE_IDENTITY()
		
			--Added By:Sachin Sadhu||Date:27thAug2014
			--Purpose :MAke entry in WFA queue 
			IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 )
			BEGIN
				EXEC 
				USP_ManageOpportunityAutomationQueue			
				@numOppQueueID = 0, -- numeric(18, 0)							
				@numDomainID = @numDomainID, -- numeric(18, 0)							
				@numOppId = @numOppID, -- numeric(18, 0)							
				@numOppBizDocsId = @numOppBizDocsId, -- numeric(18, 0)							
				@numOrderStatus = 0, -- numeric(18, 0)					
				@numUserCntID = @numUserCntID, -- numeric(18, 0)					
				@tintProcessStatus = 1, -- tinyint						
				@tintMode = 1 -- TINYINT
			END 
		   --end of script

			--Deferred BizDocs : Create Recurring entry only for create Account Journal
			if @tintDeferred=1
			BEGIN
				exec USP_RecurringAddOpportunity @numOppId,@numOppBizDocsId,0,4,@dtCreatedDate,0,@numDomainID,0,0,100,''
			END

			-- Update name template set for bizdoc
			--DECLARE @tintType TINYINT
			--SELECT @tintType = CASE @tintOppType WHEN 1 THEN 3 WHEN 2 THEN 4 END;
			EXEC dbo.USP_UpdateBizDocNameTemplate @tintOppType,@numDomainID,@numOppBizDocsId

			IF @numFromOppBizDocsId>0
			BEGIN
				INSERT INTO OpportunityBizDocItems
  				(
					numOppBizDocID
					,numOppItemID
					,numItemCode
					,numUnitHour
					,monPrice
					,monTotAmount
					,vcItemDesc
					,numWarehouseItmsID
					,vcType
					,vcAttributes
					,bitDropShip
					,bitDiscountType
					,fltDiscount
					,monTotAmtBefDiscount
					,vcNotes
					,bitEmbeddedCost
					,monEmbeddedCost
					,dtRentalStartDate
					,dtRentalReturnDate
					,tintTrackingStatus
				)
				SELECT 
					@numOppBizDocsId
					,OBDI.numOppItemID
					,OBDI.numItemCode
					,OBDI.numUnitHour
					,OBDI.monPrice
					,OBDI.monTotAmount
					,OBDI.vcItemDesc
					,OBDI.numWarehouseItmsID
					,OBDI.vcType
					,OBDI.vcAttributes
					,OBDI.bitDropShip
					,OBDI.bitDiscountType
					,OBDI.fltDiscount
					,OBDI.monTotAmtBefDiscount
					,OBDI.vcNotes
					,OBDI.bitEmbeddedCost
					,OBDI.monEmbeddedCost
					,OBDI.dtRentalStartDate
					,OBDI.dtRentalReturnDate
					,OBDI.tintTrackingStatus
				FROM 
					OpportunityBizDocs OBD 
				JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID 
				JOIN OpportunityItems OI ON OBDI.numOppItemID=OI.numoppitemtCode
				JOIN Item I ON OBDI.numItemCode=I.numItemCode
				WHERE 
					OBD.numOppId=@numOppId 
					AND OBD.numOppBizDocsId=@numFromOppBizDocsId
			END
			ELSE IF (@bitPartialFulfillment=1 and DATALENGTH(@strBizDocItems)>2)
			BEGIN
				IF DATALENGTH(@strBizDocItems)>2
				BEGIN
					EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strBizDocItems 

						IF @bitRentalBizDoc=1
						BEGIN
							update OI set OI.monPrice=OBZ.monPrice,OI.monTotAmount=OBZ.monTotAmount,OI.monTotAmtBefDiscount=OBZ.monTotAmount
							from OpportunityItems OI
							Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
								WITH ( OppItemID numeric(9),Quantity FLOAT,monPrice DECIMAL(20,5),monTotAmount DECIMAL(20,5))) OBZ
							on OBZ.OppItemID=OI.numoppitemtCode
							where OBZ.OppItemID=OI.numoppitemtCode
						END
	
						insert into                       
					   OpportunityBizDocItems                                                                          
					   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes,/*vcTrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate)
				   
					   select @numOppBizDocsId,numoppitemtCode,numItemCode,Quantity,CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmount/numUnitHour)*Quantity END,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,
					   case when bitDiscountType=0 then fltDiscount when bitDiscountType=1 then (fltDiscount/numUnitHour)*Quantity else fltDiscount end ,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmtBefDiscount/numUnitHour)*Quantity END,Notes,/*TrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate  from OpportunityItems OI
					   Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
					   WITH                       
					   (OppItemID numeric(9),                               
						Quantity FLOAT,
						Notes varchar(500),
						monPrice DECIMAL(20,5),
						dtRentalStartDate datetime,dtRentalReturnDate datetime
						)) OBZ
					   on OBZ.OppItemID=OI.numoppitemtCode
					   where  numOppId=@numOppId AND ISNULL(OI.numUnitHour,0)>0 AND ISNULL(OI.numUnitHour,0)>0
				   
					EXEC sp_xml_removedocument @hDocItem 
				END
			END
			ELSE
			BEGIN
				IF @numBizDocId <> 304 AND @bitAuthBizdoc = 1 AND @bitInvoiceForDeferred = 1
				BEGIN
					DECLARE @numTop1DeferredBizDocID AS NUMERIC(18,0)

					SELECT TOP 1
						@numTop1DeferredBizDocID = 	OpportunityBizDocs.numOppBizDocsId
					FROM 
						OpportunityBizDocs 
					WHERE 
						numOppId=@numOppId 
						AND numOppBizDocsId NOT IN (SELECT ISNULL(numDeferredBizDocID,0) FROM OpportunityBizDocs WHERE numOppId=@numOppId)
						AND OpportunityBizDocs.numBizDocId = 304 -- 304 IS DEFERRED INVOICE BIZDOC
					ORDER BY
						numOppBizDocsId

					INSERT INTO OpportunityBizDocItems
  					(
						numOppBizDocID,
						numOppItemID,
						numItemCode,
						numUnitHour,
						monPrice,
						monTotAmount,
						vcItemDesc,
						numWarehouseItmsID,
						vcType,
						vcAttributes,
						bitDropShip,
						bitDiscountType,
						fltDiscount,
						monTotAmtBefDiscount,
						vcNotes
					)
					SELECT 
						@numOppBizDocsId,
						numoppitemtCode,
						OI.numItemCode,
						(CASE @bitRecurringBizDoc WHEN 1 THEN 1 ELSE ISNULL(TempBizDoc.numUnitHour,0) END) AS numUnitHour,
						OI.monPrice,
						(CASE @bitRecurringBizDoc 
							WHEN 1 THEN monPrice * 1 
							ELSE (OI.monTotAmount/OI.numUnitHour) * ISNULL(TempBizDoc.numUnitHour,0)
						END) AS monTotAmount,
						vcItemDesc,
						numWarehouseItmsID,
						vcType,vcAttributes,
						bitDropShip,
						bitDiscountType,
						(CASE 
							WHEN bitDiscountType=0 THEN fltDiscount 
							WHEN bitDiscountType=1 THEN ((fltDiscount/OI.numUnitHour) * ISNULL(TempBizDoc.numUnitHour,0))
							ELSE fltDiscount 
						END),
						(monTotAmtBefDiscount/OI.numUnitHour) * ISNULL(TempBizDoc.numUnitHour,0),
						OI.vcNotes
					FROM 
						OpportunityItems OI
					JOIN 
						[dbo].[Item] AS I 
					ON 
						I.[numItemCode] = OI.[numItemCode]
					INNER JOIN
						(
							SELECT 
								OpportunityBizDocItems.numOppItemID,
								OpportunityBizDocItems.numUnitHour
							FROM 
								OpportunityBizDocs 
							JOIN 
								OpportunityBizDocItems 
							ON 
								OpportunityBizDocs.numOppBizDocsId  = OpportunityBizDocItems.numOppBizDocId
							WHERE 
								numOppId=@numOppId 
								AND OpportunityBizDocs.numOppBizDocsId = @numTop1DeferredBizDocID
						) TempBizDoc
					ON
						TempBizDoc.numOppItemID = OI.numoppitemtCode
					WHERE  
						numOppId=@numOppId 
						AND ISNULL(OI.numUnitHour,0) > 0
						AND (@bitRecurringBizDoc = 1 OR ISNULL(TempBizDoc.numUnitHour,0) > 0)


					UPDATE OpportunityBizDocs SET numDeferredBizDocID = @numTop1DeferredBizDocID WHERE numOppBizDocsId = @numOppBizDocsId
				END
				ELSE
				BEGIN
		   			INSERT INTO OpportunityBizDocItems                                                                          
  					(
						numOppBizDocID,
						numOppItemID,
						numItemCode,
						numUnitHour,
						monPrice,
						monTotAmount,
						vcItemDesc,
						numWarehouseItmsID,
						vcType,
						vcAttributes,
						bitDropShip,
						bitDiscountType,
						fltDiscount,
						monTotAmtBefDiscount,
						vcNotes
					)
					SELECT 
						@numOppBizDocsId,
						numoppitemtCode,
						OI.numItemCode,
						(CASE @bitRecurringBizDoc 
							WHEN 1 
							THEN 1 
							ELSE (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0)) 
						END) AS numUnitHour,
						OI.monPrice,
						(CASE @bitRecurringBizDoc 
							WHEN 1 THEN monPrice * 1 
							ELSE (OI.monTotAmount/OI.numUnitHour) * (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0))
						END) AS monTotAmount,
						vcItemDesc,
						numWarehouseItmsID,
						vcType,vcAttributes,
						bitDropShip,
						bitDiscountType,
						(CASE 
							WHEN bitDiscountType=0 THEN fltDiscount 
							WHEN bitDiscountType=1 THEN ((fltDiscount/OI.numUnitHour) * (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0)))
							ELSE fltDiscount 
						END),
						(monTotAmtBefDiscount/OI.numUnitHour) * (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0)),
						OI.vcNotes
					FROM 
						OpportunityItems OI
					JOIN 
						[dbo].[Item] AS I 
					ON 
						I.[numItemCode] = OI.[numItemCode]
					OUTER APPLY
						(
							SELECT 
								SUM(OBDI.numUnitHour) AS numUnitHour
							FROM 
								OpportunityBizDocs OBD 
							JOIN 
								dbo.OpportunityBizDocItems OBDI 
							ON 
								OBDI.numOppBizDocId  = OBD.numOppBizDocsId
								AND OBDI.numOppItemID = OI.numoppitemtCode
							WHERE 
								numOppId=@numOppId 
								AND 1 = (CASE 
											WHEN @bitAuthBizdoc = 1 
											THEN (CASE WHEN (numBizDocId = 304 OR (numBizDocId = @numBizDocId AND ISNULL(numDeferredBizDocID,0) = 0)) THEN 1 ELSE 0 END)
											WHEN @numBizDocId = 304 --Deferred Income
											THEN (CASE WHEN (numBizDocId = @numBizDocId OR ISNULL(OBD.bitAuthoritativeBizDocs,0) = 1) THEN 1 ELSE 0 END)
											ELSE (CASE WHEN numBizDocId = @numBizDocId THEN 1 ELSE 0 END) 
										END)
						) TempBizDoc
					WHERE  
						numOppId=@numOppId 
						AND ISNULL(OI.numUnitHour,0) > 0
						AND (@bitRecurringBizDoc = 1 OR (ISNULL(OI.numUnitHour,0) - ISNULL(TempBizDoc.numUnitHour,0)) > 0)
				END
			END

			SET @numOppBizDocsID = @numOppBizDocsId
			--select @numOppBizDocsId
		END
		ELSE
		BEGIN
  			SET @numOppBizDocsId=0
			SET @monDealAmount = 0
			SET @monCreditAmount = 0
			RAISERROR ('NOT_ALLOWED',16,1);
		END
	END
	ELSE
	BEGIN
		UPDATE 
			OpportunityBizDocs
		SET    
			numModifiedBy = @numUserCntID,
			dtModifiedDate = GETUTCDATE(),
			vcComments = @vcComments,
			numShipVia = @numShipVia,
			vcTrackingURL = @vcTrackingURL,
			dtFromDate = @dtFromDate,
			numBizDocStatus = @numBizDocStatus,
			numSequenceId=@numSequenceId,
			numBizDocTempID=@numBizDocTempID,
			vcTrackingNo=@vcTrackingNo,
			vcRefOrderNo=@vcRefOrderNo,
			fltExchangeRateBizDoc=@fltExchangeRateBizDoc,
			bitShippingGenerated=@bitShippingGenerated,
			vcVendorInvoice=@vcVendorInvoiceName,
			numARAccountID=@numARAccountID
		WHERE  
			numOppBizDocsId = @numOppBizDocsId

		--Added By:Sachin Sadhu||Date:27thAug2014
		--Purpose :MAke entry in WFA queue 
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 )
		BEGIN
			EXEC 
			USP_ManageOpportunityAutomationQueue			
			@numOppQueueID = 0, -- numeric(18, 0)							
			@numDomainID = @numDomainID, -- numeric(18, 0)							
			@numOppId = @numOppID, -- numeric(18, 0)							
			@numOppBizDocsId = @numOppBizDocsId, -- numeric(18, 0)							
			@numOrderStatus = 0, -- numeric(18, 0)					
			@numUserCntID = @numUserCntID, -- numeric(18, 0)					
			@tintProcessStatus = 1, -- tinyint						
			@tintMode = 1 -- TINYINT
		END 
		--end of code

   		IF DATALENGTH(@strBizDocItems)>2
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strBizDocItems 

            DELETE FROM 
				OpportunityBizDocItems  
			WHERE 
				numOppItemID NOT IN 
				(
					SELECT 
						OppItemID 
					FROM OPENXML 
						(@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
					WITH                       
					   (OppItemID numeric(9))
				)
				AND numOppBizDocID=@numOppBizDocsId  
                  
                     
			IF @bitRentalBizDoc=1
			BEGIN
				update OI set OI.monPrice=OBZ.monPrice,OI.monTotAmount=OBZ.monTotAmount,OI.monTotAmtBefDiscount=OBZ.monTotAmount
				from OpportunityItems OI
				Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
					WITH ( OppItemID numeric(9),Quantity FLOAT,monPrice DECIMAL(20,5),monTotAmount DECIMAL(20,5))) OBZ
				on OBZ.OppItemID=OI.numoppitemtCode
				where OBZ.OppItemID=OI.numoppitemtCode
			END

			update OBI set OBI.numUnitHour= Quantity,OBI.monPrice=CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,OBI.monTotAmount=CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (OI.monTotAmount/OI.numUnitHour)*Quantity END,OBI.vcNotes= Notes,
			OBI.fltDiscount=case when OI.bitDiscountType=0 then OI.fltDiscount when OI.bitDiscountType=1 then (OI.fltDiscount/OI.numUnitHour)*Quantity else OI.fltDiscount end ,
			OBI.monTotAmtBefDiscount=(OI.monTotAmtBefDiscount/OI.numUnitHour)*Quantity ,                                                                    
			/*OBI.vcTrackingNo=TrackingNo,OBI.vcShippingMethod=OBZ.vcShippingMethod,OBI.monShipCost=OBZ.monShipCost,OBI.dtDeliveryDate=OBZ.dtDeliveryDate*/
			OBI.dtRentalStartDate=OBZ.dtRentalStartDate,OBI.dtRentalReturnDate=OBZ.dtRentalReturnDate
			from OpportunityBizDocItems OBI
			join OpportunityItems OI
			on OBI.numOppItemID=OI.numoppitemtCode
			Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
			WITH                       
			( OppItemID numeric(9),                                     
			Quantity FLOAT,
			Notes varchar(500),
			monPrice DECIMAL(20,5),
			dtRentalStartDate datetime,dtRentalReturnDate datetime
			)) OBZ
			on OBZ.OppItemID=OI.numoppitemtCode
			where  OI.numOppId=@numOppId and OBI.numOppBizDocID=@numOppBizDocsId AND ISNULL(OI.numUnitHour,0)>0


            insert into                       
			OpportunityBizDocItems                                                                          
			(numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes/*,vcTrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate*/)                      
			select @numOppBizDocsId,numoppitemtCode,numItemCode,Quantity,CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmount/numUnitHour)*Quantity END,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,
			case when bitDiscountType=0 then fltDiscount when bitDiscountType=1 then (fltDiscount/numUnitHour)*Quantity else fltDiscount end ,(monTotAmtBefDiscount/numUnitHour)*Quantity,Notes/*,TrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate*/ from OpportunityItems OI
			Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
			WITH                       
			( OppItemID numeric(9),                                     
			Quantity FLOAT,
			Notes varchar(500),
			monPrice DECIMAL(20,5)
			--TrackingNo varchar(500),
			--vcShippingMethod varchar(100),
			--monShipCost DECIMAL(20,5),
			--dtDeliveryDate DATETIME
			)) OBZ
			on OBZ.OppItemID=OI.numoppitemtCode and OI.numoppitemtCode not in (select numOppItemID from OpportunityBizDocItems where numOppBizDocID =@numOppBizDocsId)
			where  numOppId=@numOppId AND ISNULL(OI.numUnitHour,0)>0
                 
			EXEC sp_xml_removedocument @hDocItem 
		END
	END

	IF ISNULL((SELECT numBizDocID FROM OpportunityBizDocs WHERE numOppBizDocsID=@numOppBizDocsId),0) = 29397
	BEGIN
		UPDATE
			OI
		SET
			OI.numQtyPicked = ISNULL((SELECT 
											SUM(OBDI.numUnitHour)
										FROM 
											OpportunityBizDocs OBD 
										INNER JOIN 
											OpportunityBizDocItems OBDI
										ON
											OBD.numOppBizDocsID = OBDI.numOppBizDocID
											AND OI.numoppitemtCode = OBDI.numOppItemID 
										WHERE 
											OBD.numOppID = OI.numOppID 
											AND OBD.numBizDocID = 29397),0)
		FROM	
			OpportunityItems OI
		WHERE
			OI.numOppID = @numOppID
	END

	IF EXISTS (SELECT
					OBDI.numOppItemID
				FROM
					OpportunityBizDocs OBD
				INNER JOIN
					OpportunityBizDocItems OBDI
				ON
					OBD.numOppBizDocsId = OBDI.numOppBizDocID
				LEFT JOIN
					OpportunityItems OI
				ON
					OBDI.numOppItemID = OI.numoppitemtCode
				WHERE
					OBD.numOppId=@numOppID
					AND OBD.numBizDocId = @numBizDocId
					AND OBD.numBizDocId IN (287,296,29397)
				GROUP BY
					OBDI.numOppItemID
					,OI.numUnitHour
				HAVING 
					SUM(ISNULL(OBDI.numUnitHour,0)) > ISNULL(OI.numUnitHour,0))
	BEGIN
		RAISERROR('BIZDOC_QTY_MORE_THEN_ORDERED_QTY',16,1)
	END

	IF @numOppBizDocsId>0
	BEGIN
		SELECT @monDealAmount=isnull(dbo.[GetDealAmount](@numOppId,GETUTCDATE(),@numOppBizDocsId),0)

		IF @numBizDocId = 297 OR @numBizDocId = 299
		BEGIN
			IF NOT EXISTS(SELECT * FROM dbo.NameTemplate WHERE numDomainID=@numDomainID AND numRecordID=@numBizDocId AND tintModuleID=2)
			BEGIN
				INSERT INTO NameTemplate
				(numDomainId,tintModuleID,numRecordID,vcNameTemplate,numSequenceId,numMinLength)
				SELECT 
					@numDomainId,2,@numBizDocId,UPPER(SUBSTRING(dbo.GetListIemName(@numBizDocId),0,4)) + '-',1,4

				SET @numSequenceId = 1
			END
			ELSE
			BEGIN
				SELECT @numSequenceId=numSequenceId FROM NameTemplate WHERE numDomainID=@numDomainID AND numRecordID=@numBizDocId AND tintModuleID=2
				UPDATE NameTemplate SET numSequenceId=ISNULL(numSequenceId,0) + 1 WHERE numDomainID=@numDomainID AND numRecordID=@numBizDocId AND tintModuleID=2
			END
		END

		UPDATE 
			OpportunityBizDocs 
		SET 
			vcBizDocID=vcBizDocName + ISNULL(@numSequenceId,''),
			numSequenceId = CASE WHEN (numBizDocId = 297 OR numBizDocId = 299) THEN @numSequenceId ELSE numSequenceId END,
			monDealAmount=@monDealAmount
		WHERE  
			numOppBizDocsId = @numOppBizDocsId
	END

	-- 1 if recurrence is enabled for authorative bizdoc
	IF ISNULL(@bitRecur,0) = 1
	BEGIN
		-- First Check if receurrece is already created for sales order because user should not 
		-- be able to create recurrence on both order and invoice
		IF (SELECT COUNT(*) FROM RecurrenceConfiguration WHERE numOppID = @numOppId AND numType=1) > 0 
		BEGIN
			RAISERROR ('RECURRENCE_ALREADY_CREATED_FOR_ORDER',16,1);
		END
		--User can not create recurrence on bizdoc where all items are added to bizdoc
		ELSE IF (SELECT dbo.CheckAllItemQuantityAddedToBizDoc(@numOppId,@numOppBizDocsID)) = 1
		BEGIN
			RAISERROR ('RECURRENCE_NOT_ALLOWED_ALL_ITEM_QUANTITY_ADDED_TO_INVOICE',16,1);
		END
		ELSE
		BEGIN

			EXEC USP_RecurrenceConfiguration_Insert
				@numDomainID = @numDomainId,
				@numUserCntID = @numUserCntId,
				@numOppID = @numOppID,
				@numOppBizDocID = @numOppBizDocsId,
				@dtStartDate = @dtStartDate,
				@dtEndDate =NULL,
				@numType = 2,
				@vcType = 'Invoice',
				@vcFrequency = @vcFrequency,
				@numFrequency = @numFrequency

			UPDATE OpportunityMaster SET vcRecurrenceType = 'Invoice' WHERE numOppId = @numOppID
		END
	END
	ELSE IF ISNULL(@numRecConfigID,0) > 0 AND ISNULL(@bitDisable,0)= 1
	BEGIN
		UPDATE
			RecurrenceConfiguration
		SET
			bitDisabled = 1,
			numDisabledBy = @numUserCntId,
			dtDisabledDate = GETDATE()
		WHERE	
			numRecConfigID = @numRecConfigID
	END 
 
	IF @numOppBizDocsId > 0 AND @numBizDocId = 29397 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
	BEGIN
		EXEC USP_PickListManageSOWorkOrder @numDomainID,@numUserCntID,@numOppID,@numOppBizDocsId
		-- Allocate Inventory
		EXEC USP_ManageInventoryPickList @numDomainID,@numOppID,@numOppBizDocsId,@numUserCntID
	END
END TRY
BEGIN CATCH
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return error
    -- information about the original error that caused
    -- execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
               );
END CATCH;
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_Execute')
DROP PROCEDURE dbo.USP_DemandForecast_Execute
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_Execute]
	@numDFID NUMERIC (18,0)
	,@vcSelectedIDs VARCHAR(MAX)
	,@numUserCntID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@CurrentPage INT
    ,@PageSize INT
	,@columnName VARCHAR(MAX)                                                          
	,@columnSortOrder VARCHAR(10)
	,@numDomainID NUMERIC(18,0)
	,@vcRegularSearchCriteria VARCHAR(MAX)=''
	,@bitShowAllItems BIT
	,@bitShowHistoricSales BIT
	,@numHistoricalAnalysisPattern BIT
	,@bitBasedOnLastYear BIT
	,@bitIncludeOpportunity BIT
	,@numOpportunityPercentComplete NUMERIC(18,0)
	,@tintDemandPlanBasedOn TINYINT
	,@tintPlanType TINYINT
AS 
BEGIN
	DECLARE @dtLastExecution DATETIME
	DECLARE @bitWarehouseFilter BIT = 0
	DECLARE @bitItemClassificationFilter BIT = 0
	DECLARE @bitItemGroupFilter BIT = 0

	IF (SELECT COUNT(*) FROM DemandForecast WHERE numDFID = @numDFID) > 0
	BEGIN
		IF (SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = @numDFID) > 0
		BEGIN
			SET @bitWarehouseFilter = 1
		END

		IF (SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = @numDFID) > 0
		BEGIN
			SET @bitItemClassificationFilter = 1
		END

		IF (SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = @numDFID) > 0
		BEGIN
			SET @bitItemGroupFilter = 1
		END				
	END

	DECLARE @tintUnitsRecommendationForAutoPOBackOrder TINYINT
	DECLARE @bitIncludeRequisitions BIT

	SELECT 
		@tintUnitsRecommendationForAutoPOBackOrder=ISNULL(tintUnitsRecommendationForAutoPOBackOrder,1)
		,@bitIncludeRequisitions=ISNULL(bitIncludeRequisitions,0)
	FROM 
		Domain 
	WHERE 
		numDomainID = @numDomainID

	DECLARE @TEMP TABLE
	(
		numItemCode NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(18,0)
		,numUnitHour FLOAT
		,dtReleaseDate DATE
	)

	INSERT INTO @TEMP
	(
		numItemCode
		,numWarehouseItemID
		,numUnitHour
		,dtReleaseDate
	)
	SELECT
		Item.numItemCode
		,OpportunityItems.numWarehouseItmsID
		,OpportunityItems.numUnitHour - ISNULL(OpportunityItems.numQtyShipped,0)
		,ISNULL(OpportunityItems.ItemReleaseDate,OpportunityMaster.dtReleaseDate)
	FROM
		OpportunityMaster
	INNER JOIN
		OpportunityItems
	ON
		OpportunityMaster.numOppId = OpportunityItems.numOppId
	INNER JOIN
		Item
	ON
		OpportunityItems.numItemCode = Item.numItemCode
	INNER JOIN
		WareHouseItems
	ON
		OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
	WHERE
		OpportunityMaster.numDomainId=@numDomainID
		AND OpportunityMaster.tintOppType=1
		AND OpportunityMaster.tintOppStatus=1
		AND ISNULL(OpportunityMaster.tintshipped,0)=0
		AND (ISNULL(OpportunityItems.numUnitHour,0)  - ISNULL(OpportunityItems.numQtyShipped,0)) > 0
		AND ISNULL(OpportunityItems.bitDropShip,0) = 0
		AND ISNULL(Item.IsArchieve,0) = 0
		AND ISNULL(Item.bitAssembly,0) = (CASE WHEN @tintPlanType=2 THEN 1 ELSE 0 END)
		AND ISNULL(Item.bitKitParent,0) = 0
		AND (OpportunityItems.ItemReleaseDate IS NOT NULL OR OpportunityMaster.dtReleaseDate IS NOT NULL)
		AND 1 = (CASE WHEN LEN(ISNULL(@vcSelectedIDs,'')) > 0 
					THEN (CASE WHEN Item.numItemCode IN (SELECT Id FROM dbo.SplitIDs(@vcSelectedIDs,',')) THEN 1 ELSE 0 END) 
					ELSE 1 
				END)
		AND
		(
			@bitWarehouseFilter = 0 OR
			WareHouseItems.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
		)
		AND
		(
			@bitItemClassificationFilter = 0 OR
			Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
		)
		AND
		(
			@bitItemGroupFilter = 0 OR
			Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
		)

	INSERT INTO @TEMP
	(
		numItemCode
		,numWarehouseItemID
		,numUnitHour
		,dtReleaseDate
	)
	SELECT
		Item.numItemCode
		,OKI.numWareHouseItemId
		,OKI.numQtyItemsReq - ISNULL(OKI.numQtyShipped,0)
		,ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate)
	FROM
		OpportunityMaster OM
	INNER JOIN
		OpportunityItems OI
	ON
		OM.numOppId = OI.numOppId
	INNER JOIN
		OpportunityKitItems OKI
	ON
		OI.numoppitemtCode=OKI.numOppItemID
	INNER JOIN
		Item
	ON
		OKI.numChildItemID = Item.numItemCode
	INNER JOIN
		WareHouseItems WI
	ON
		OKI.numWareHouseItemId = WI.numWareHouseItemID
	WHERE
		OM.numDomainId=@numDomainID
		AND OM.tintOppType=1
		AND OM.tintOppStatus=1
		AND ISNULL(OM.tintshipped,0)=0
		AND ISNULL(OI.bitDropShip,0) = 0
		AND ISNULL(OI.bitWorkOrder,0) = 0
		AND ISNULL(Item.IsArchieve,0) = 0
		AND ISNULL(Item.bitAssembly,0) = (CASE WHEN @tintPlanType=2 THEN 1 ELSE 0 END)
		AND ISNULL(Item.bitKitParent,0) = 0
		AND (ISNULL(OKI.numQtyItemsReq,0) - ISNULL(OKI.numQtyShipped,0)) > 0
		AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
		AND 1 = (CASE WHEN LEN(ISNULL(@vcSelectedIDs,'')) > 0 
					THEN (CASE WHEN Item.numItemCode IN (SELECT Id FROM dbo.SplitIDs(@vcSelectedIDs,',')) THEN 1 ELSE 0 END) 
					ELSE 1 
				END)
		AND
		(
			@bitWarehouseFilter = 0 OR
			WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
		)
		AND
		(
			@bitItemClassificationFilter = 0 OR
			Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
		)
		AND
		(
			@bitItemGroupFilter = 0 OR
			Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
		)

	INSERT INTO @TEMP
	(
		numItemCode
		,numWarehouseItemID
		,numUnitHour
		,dtReleaseDate
	)
	SELECT
		Item.numItemCode
		,OKCI.numWareHouseItemId
		,OKCI.numQtyItemsReq - ISNULL(OKCI.numQtyShipped,0)
		,ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate)
	FROM
		OpportunityMaster OM
	INNER JOIN
		OpportunityItems OI
	ON
		OM.numOppId = OI.numOppId
	INNER JOIN
		OpportunityKitChildItems OKCI
	ON
		OI.numoppitemtCode = OKCI.numOppItemID
	INNER JOIN
		Item
	ON
		OKCI.numItemID = Item.numItemCode
	INNER JOIN
		WareHouseItems WI
	ON
		OKCI.numWareHouseItemId = WI.numWareHouseItemID
	WHERE
		OM.numDomainId=@numDomainID
		AND OM.tintOppType=1
		AND OM.tintOppStatus=1
		AND ISNULL(OM.tintshipped,0)=0
		AND ISNULL(OI.bitDropShip,0) = 0
		AND ISNULL(OI.bitWorkOrder,0) = 0
		AND ISNULL(Item.IsArchieve,0) = 0
		AND ISNULL(Item.bitAssembly,0) = (CASE WHEN @tintPlanType=2 THEN 1 ELSE 0 END)
		AND ISNULL(Item.bitKitParent,0) = 0
		AND (ISNULL(OKCI.numQtyItemsReq,0) - ISNULL(OKCI.numQtyShipped,0)) > 0
		AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
		AND 1 = (CASE WHEN LEN(ISNULL(@vcSelectedIDs,'')) > 0 
					THEN (CASE WHEN Item.numItemCode IN (SELECT Id FROM dbo.SplitIDs(@vcSelectedIDs,',')) THEN 1 ELSE 0 END) 
					ELSE 1 
				END)
		AND
		(
			@bitWarehouseFilter = 0 OR
			WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
		)
		AND
		(
			@bitItemClassificationFilter = 0 OR
			Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
		)
		AND
		(
			@bitItemGroupFilter = 0 OR
			Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
		)

	INSERT INTO @TEMP
	(
		numItemCode
		,numWarehouseItemID
		,numUnitHour
		,dtReleaseDate
	)
	SELECT
		WOD.numChildItemID
		,WOD.numWareHouseItemId
		,WOD.numQtyItemsReq
		,(CASE WHEN OM.numOppId IS NOT NULL THEN ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) ELSE WO.dtmEndDate END)
	FROM
		WorkOrderDetails WOD
	INNER JOIN
		Item
	ON
		WOD.numChildItemID = Item.numItemCode
	INNER JOIN
		WareHouseItems WI
	ON
		WOD.numWareHouseItemId = WI.numWareHouseItemID
	INNER JOIN
		WorkOrder WO
	ON
		WOD.numWOId=WO.numWOId
	LEFT JOIN
		OpportunityItems OI
	ON
		WO.numOppItemID = OI.numoppitemtCode
	LEFT JOIN
		OpportunityMaster OM
	ON
		OI.numOppId=OM.numOppId
	WHERE
		WO.numDomainID=@numDomainID
		AND WO.numWOStatus <> 23184 -- NOT COMPLETED
		AND ISNULL(Item.IsArchieve,0) = 0
		AND ISNULL(Item.bitAssembly,0) = (CASE WHEN @tintPlanType=2 THEN 1 ELSE 0 END)
		AND ISNULL(Item.bitKitParent,0) = 0
		AND ISNULL(WOD.numQtyItemsReq,0) > 0
		AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL OR WO.dtmEndDate IS NOT NULL)
		AND 1 = (CASE WHEN OM.numOppId IS NOT NULL THEN (CASE WHEN OM.tintOppStatus=1 THEN 1 ELSE 0 END) ELSE 1 END)
		AND 1 = (CASE WHEN LEN(ISNULL(@vcSelectedIDs,'')) > 0 
					THEN (CASE WHEN Item.numItemCode IN (SELECT Id FROM dbo.SplitIDs(@vcSelectedIDs,',')) THEN 1 ELSE 0 END) 
					ELSE 1 
				END)
		AND
		(
			@bitWarehouseFilter = 0 OR
			WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
		)
		AND
		(
			@bitItemClassificationFilter = 0 OR
			Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
		)
		AND
		(
			@bitItemGroupFilter = 0 OR
			Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
		)

	CREATE TABLE #TEMPItems
	(
		numItemCode NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(18,0)
		,numUnitHour FLOAT
		,dtReleaseDate VARCHAR(MAX)
		,dtReleaseDateHidden VARCHAR(MAX)
	)

	INSERT INTO #TEMPItems
	(
		numItemCode
		,numWarehouseItemID
		,numUnitHour
		,dtReleaseDate
		,dtReleaseDateHidden
	)
	SELECT
		numItemCode
		,numWarehouseItemID
		,SUM(numUnitHour)
		,STUFF((SELECT 
					CONCAT(', <a href="#" onclick="return OpenDFReleaseDateRecords(',numItemCode,',',numWarehouseItemID,',''',CONVERT(VARCHAR(10), dtReleaseDate, 101),''');">',CONVERT(VARCHAR(10), dtReleaseDate, 101),'</a> (',SUM(numUnitHour),')')
				FROM 
					@TEMP
				WHERE 
					numItemCode=T1.numItemCode
					AND numWarehouseItemID=T1.numWarehouseItemID
				GROUP BY
					numItemCode
					,numWarehouseItemID
					,dtReleaseDate
				FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)')
				,1,2,'')
		,STUFF((SELECT 
					CONCAT(', ',CONVERT(VARCHAR(10), dtReleaseDate, 101),' (',SUM(numUnitHour),')')
				FROM 
					@TEMP
				WHERE 
					numItemCode=T1.numItemCode
					AND numWarehouseItemID=T1.numWarehouseItemID
				GROUP BY
					numItemCode
					,numWarehouseItemID
					,dtReleaseDate
				FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)')
				,1,2,'')
	FROM
		@TEMP T1
	GROUP BY
		numItemCode
		,numWarehouseItemID

	---------------------------- Dynamic Query -------------------------------
		
	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0

	SELECT
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=139 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = 0
			AND 1 = (CASE 
						WHEN vcOrigDbColumnName='numBuildableQty' 
						THEN (CASE WHEN @tintPlanType=2 THEN 1 ELSE 0 END) 
						ELSE 1 
					END)
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=139 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = 0
	) TotalRows

	IF @Nocolumns=0
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			139,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth
		FROM 
			View_DynamicDefaultColumns
		WHERE 
			numFormId=139 
			AND bitDefault=1 
			AND ISNULL(bitSettingField,0)=1 
			AND numDomainID=@numDomainID
		ORDER BY 
			tintOrder asc   
	END

	INSERT INTO 
		#tempForm
	SELECT 
		tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
		vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
		vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
	FROM 
		View_DynamicColumns 
	WHERE 
		numFormId=139 
		AND numUserCntID=@numUserCntID 
		AND numDomainID=@numDomainID 
		AND tintPageType=1 
		AND ISNULL(bitSettingField,0)=1 
		AND ISNULL(bitCustom,0)=0  
		AND numRelCntType = 0
		AND 1 = (CASE 
						WHEN vcOrigDbColumnName='numBuildableQty' 
						THEN (CASE WHEN @tintPlanType=2 THEN 1 ELSE 0 END) 
						ELSE 1 
					END)
	UNION
	SELECT 
		tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
	FROM 
		View_DynamicCustomColumns
	WHERE 
		numFormId=139 
		AND numUserCntID=@numUserCntID 
		AND numDomainID=@numDomainID 
		AND tintPageType=1 
		AND ISNULL(bitCustom,0)=1 
		AND numRelCntType = 0
	ORDER BY 
		tintOrder asc
					
	DECLARE  @strColumns AS VARCHAR(MAX) = ''
	SET @strColumns = CONCAT(' COUNT(*) OVER() TotalRowsCount, Item.numItemCode, Item.vcItemName, Item.vcSKU
	,ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = Item.numBaseUnit),''-'') AS vcUOM
	,Item.charItemType AS vcItemType
	,Item.numAssetChartAcntId
	,Item.monAverageCost
	,Item.numBusinessProcessId
	,SPLM.Slp_Name
	,SPLM.numBuildManager
	,dbo.fn_GetContactName(SPLM.numBuildManager) vcBuildManager
	,(SELECT TOP 1 numContactId FROM AdditionalContactsInformation WHERE numDivisionId = Item.numVendorID ORDER BY ISNULL(bitPrimaryContact,0) DESC) AS numContactID
	,ISNULL(Item.numPurchaseUnit,0) AS numUOM
	,dbo.fn_UOMConversion(ISNULL(numBaseUnit, 0), Item.numItemCode, Item.numDomainId, ISNULL(Item.numPurchaseUnit, 0)) fltUOMConversionfactor
	,dbo.fn_GetAttributes(WI.numWareHouseItemId,1) AS vcAttributes
	,WI.numWareHouseID
	,WI.numWareHouseItemID
	,W.vcWarehouse
	,ISNULL(V.numVendorID,0) numVendorID
	,ISNULL(V.intMinQty,0) intMinQty
	,ISNULL(monCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,Item.numDomainID,Item.numPurchaseUnit) AS monVendorCost
	,T1.numUnitHour AS numQtyToPurchase, T1.dtReleaseDateHidden, T1.dtReleaseDate,(CASE
		WHEN ',@tintUnitsRecommendationForAutoPOBackOrder,'= 3
		THEN 
			(ISNULL(numBackOrder,0) - ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  
											THEN ISNULL((SELECT 
														SUM(numUnitHour) 
													FROM 
														OpportunityItems 
													INNER JOIN 
														OpportunityMaster 
													ON 
														OpportunityItems.numOppID=OpportunityMaster.numOppId 
													WHERE 
														OpportunityMaster.numDomainId=',@numDomainID,'
														AND OpportunityMaster.tintOppType=2 
														AND OpportunityMaster.tintOppStatus=0 
														AND OpportunityItems.numItemCode=Item.numItemCode 
														AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) 
											ELSE 0 
											END)) +
			(CASE WHEN ISNULL(Item.fltReorderQty,0) >= ISNULL(V.intMinQty,0) THEN ISNULL(Item.fltReorderQty,0) ELSE ISNULL(V.intMinQty,0) END) 
		WHEN ',@tintUnitsRecommendationForAutoPOBackOrder,'=2
		THEN 
			(CASE
				WHEN ISNULL(Item.fltReorderQty,0) >= ISNULL(V.intMinQty,0) AND ISNULL(Item.fltReorderQty,0) >= ISNULL(numBackOrder,0) THEN ISNULL(Item.fltReorderQty,0)
				WHEN ISNULL(V.intMinQty,0) >= ISNULL(Item.fltReorderQty,0) AND ISNULL(V.intMinQty,0) >= ISNULL(numBackOrder,0) THEN ISNULL(V.intMinQty,0)
				WHEN ISNULL(numBackOrder,0) >= ISNULL(Item.fltReorderQty,0) AND ISNULL(numBackOrder,0) >= ISNULL(V.intMinQty,0) THEN ISNULL(numBackOrder,0)
				ELSE ISNULL(Item.fltReorderQty,0)
			END) - (ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  THEN ISNULL((SELECT 
														SUM(numUnitHour) 
													FROM 
														OpportunityItems 
													INNER JOIN 
														OpportunityMaster 
													ON 
														OpportunityItems.numOppID=OpportunityMaster.numOppId 
													WHERE 
														OpportunityMaster.numDomainId=',@numDomainID,'
														AND OpportunityMaster.tintOppType=2 
														AND OpportunityMaster.tintOppStatus=0 
														AND OpportunityItems.numItemCode=Item.numItemCode 
														AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END))
		ELSE
			(CASE WHEN ISNULL(Item.fltReorderQty,0) > ISNULL(V.intMinQty,0) THEN ISNULL(Item.fltReorderQty,0) ELSE ISNULL(V.intMinQty,0) END) 
			+ ISNULL(numBackOrder,0) - (ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  THEN ISNULL((SELECT 
																													SUM(numUnitHour) 
																												FROM 
																													OpportunityItems 
																												INNER JOIN 
																													OpportunityMaster 
																												ON 
																													OpportunityItems.numOppID=OpportunityMaster.numOppId 
																												WHERE 
																													OpportunityMaster.numDomainId=',@numDomainID,'
																													AND OpportunityMaster.tintOppType=2 
																													AND OpportunityMaster.tintOppStatus=0 
																													AND OpportunityItems.numItemCode=Item.numItemCode 
																													AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END)) 
	END) AS numQtyBasedOnPurchasePlan ')

	--Custom field 
	DECLARE @tintOrder AS TINYINT = 0                                                 
	DECLARE @vcFieldName AS VARCHAR(50)                                                  
	DECLARE @vcListItemType AS VARCHAR(3)                                             
	DECLARE @vcAssociatedControlType VARCHAR(20)     
	declare @numListID AS numeric(9)
	DECLARE @vcDbColumnName VARCHAR(50)                      
	DECLARE @WhereCondition VARCHAR(MAX) = ''                   
	DECLARE @vcLookBackTableName VARCHAR(2000)                
	DECLARE @bitCustom AS BIT
	DECLARE @bitAllowEdit AS CHAR(1)                   
	Declare @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)    
	DECLARE @vcColumnName AS VARCHAR(500)             
	Declare @ListRelID as numeric(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = '' 
	   
	SELECT TOP 1 
		@tintOrder=tintOrder+1
		,@vcDbColumnName=vcDbColumnName
		,@vcFieldName=vcFieldName
		,@vcAssociatedControlType=vcAssociatedControlType
		,@vcListItemType=vcListItemType
		,@numListID=numListID
		,@vcLookBackTableName=vcLookBackTableName
		,@bitCustom=bitCustomField
		,@numFieldId=numFieldId
		,@bitAllowSorting=bitAllowSorting
		,@bitAllowEdit=bitAllowEdit
		,@ListRelID=ListRelID
	FROM  
		#tempForm 
	ORDER BY 
		tintOrder ASC   

	WHILE @tintOrder>0                                                  
	BEGIN
	
		IF @bitCustom = 0  
		BEGIN
			DECLARE @Prefix AS VARCHAR(10)

			IF @vcLookBackTableName = 'AdditionalContactsInformation'
				SET @Prefix = 'ADC.'
			ELSE IF @vcLookBackTableName = 'DivisionMaster'
				SET @Prefix = 'Div.'
			ELSE IF @vcLookBackTableName = 'OpportunityMaster'
				SET @PreFix = 'Opp.'			
			ELSE IF @vcLookBackTableName = 'CompanyInfo'
				SET @PreFix = 'cmp.'
			ELSE IF @vcLookBackTableName = 'DemandForecastGrid'
				SET @PreFix = 'DF.'
			ELSE IF @vcLookBackTableName = 'Warehouses'
				SET @PreFix = 'W.'
			ELSE IF @vcLookBackTableName = 'WareHouseItems'
				SET @PreFix = 'WI.'
			ELSE 
				SET @PreFix = CONCAT(@vcLookBackTableName,'.')
			
			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

			IF @vcAssociatedControlType = 'TextBox'
			BEGIN
				IF @vcDbColumnName = 'numQtyToPurchase'
				BEGIN
					SET @strColumns=@strColumns+',' + ' (CASE WHEN ISNULL(WI.numBackOrder,0) > ISNULL(WI.numOnOrder,0) THEN  CEILING(ISNULL(WI.numBackOrder,0) - ISNULL(WI.numOnOrder,0))  ELSE 0 END) '  + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'moncost'
				BEGIN
					SET @strColumns=@strColumns+',' + ' ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = Item.numBaseUnit),''-'') '  + ' [' + @vcColumnName + ']'
				END
				ELSE
				BEGIN
					SET @strColumns=@strColumns+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']' 
				END
			END
			ELSE IF  @vcAssociatedControlType='Label'                                              
			BEGIN
				IF @vcDbColumnName = 'vc7Days'
				BEGIN
					SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,7,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
				END
				ELSE IF @vcDbColumnName = 'vc15Days'
				BEGIN
					SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,15,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
				END
				ELSE IF @vcDbColumnName = 'vc30Days'
				BEGIN
					SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,30,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
				END
				ELSE IF @vcDbColumnName = 'vc60Days'
				BEGIN
					SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,60,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
				END
				ELSE IF @vcDbColumnName = 'vc90Days'
				BEGIN
					SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,90,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
				END
				ELSE IF @vcDbColumnName = 'vc180Days'
				BEGIN
					SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,180,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
				END
				ELSE IF (@vcDbColumnName = 'vcBuyUOM')
				BEGIN
					SET @strColumns=@strColumns+',' + ' ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = ISNULL(Item.numPurchaseUnit,0)),'''')' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numOnOrderReq'
				BEGIN
					SET @strColumns=@strColumns+',' + ' (ISNULL(WI.numOnOrder,0) + ISNULL(WI.numReorder,0)) '  + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numAvailable'
				BEGIN
					SET @strColumns=@strColumns+',' + 'CONCAT(ISNULL(WI.numOnHand,0) + ISNULL(WI.numAllocation,0),'' ('',ISNULL(WI.numOnHand,0),'')'')'  + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numTotalOnHand'
				BEGIN
					SET @strColumns=@strColumns+',' + 'CONCAT(ISNULL((SELECT SUM(WIInner.numOnHand) + SUM(WIInner.numAllocation) FROM WarehouseItems WIInner WHERE WIInner.numItemID=Item.numItemCode),0),'' ('',ISNULL((SELECT SUM(WIInner.numOnHand) FROM WarehouseItems WIInner WHERE WIInner.numItemID=Item.numItemCode),0),'')'')'  + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numBuildableQty'
				BEGIN
					SET @strColumns=@strColumns+ CONCAT(',dbo.fn_GetAssemblyPossibleWOQty(Item.numItemCode,WI.numWareHouseID) [',@vcColumnName,']')
				END
				ELSE
				BEGIN
					SET @strColumns=@strColumns + ',' + @Prefix + @vcDbColumnName  +' ['+ @vcColumnName+']' 
				END   
			END
			ELSE IF  @vcAssociatedControlType='SelectBox' or @vcAssociatedControlType='ListBox'  
			BEGIN
				IF @vcDbColumnName = 'numVendorID'
				BEGIN
					SET @strColumns=@strColumns+',' + ' V.numVendorID '  + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'ShipmentMethod'
				BEGIN
					SET @strColumns=@strColumns+',' + ' '''' '  + ' [' + @vcColumnName + ']'
				END
				ELSE
				BEGIN
					SET @strColumns=@strColumns + ',' + @Prefix + @vcDbColumnName  +' ['+ @vcColumnName+']' 
				END
			END
			ELSE IF @vcAssociatedControlType='DateField'   
			BEGIN
				IF @vcDbColumnName = 'dtExpectedDelivery'
				BEGIN
					SET @strColumns=@strColumns+',' + ' GETDATE() '  + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'ItemRequiredDate'
				BEGIN
					SET @strColumns=@strColumns+',T1.dtReleaseDate [' + @vcColumnName + ']'
				END
				ELSE
				BEGIN
					SET @strColumns=@strColumns + ',' + @Prefix + @vcDbColumnName  +' ['+ @vcColumnName+']' 
				END					
			END
		END

		SELECT TOP 1 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,
			@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
		FROM
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 SET @tintOrder=0 

		END
	  
	DECLARE @StrSql AS VARCHAR(MAX) = ''
	SET @StrSql = CONCAT('SELECT ',@strColumns,' 
						FROM
							#TEMPItems T1
						INNER JOIN
							Item
						ON
							T1.numItemCode = Item.numItemCode
						LEFT JOIN
							Sales_process_List_Master SPLM
						ON
							Item.numBusinessProcessId = SPLM.Slp_Id
						INNER JOIN
							WarehouseItems WI
						ON
							Item.numItemCode=WI.numItemID
							AND T1.numWarehouseItemID=WI.numWarehouseItemID
						INNER JOIN
							Warehouses W
						ON
							WI.numWarehouseID = W.numWareHouseID
						LEFT JOIN
							Vendor V
						ON
							Item.numVendorID = V.numVendorID
							AND Item.numItemCode = V.numItemCode
						OUTER APPLY
						(
							SELECT TOP 1
								numListValue AS numLeadDays
							FROM
								VendorShipmentMethod VSM
							WHERE
								VSM.numVendorID = V.numVendorID
							ORDER BY
								(CASE WHEN VSM.numWarehouseID=W.numWareHouseID THEN 1 ELSE 0 END) DESC, ISNULL(VSM.numWarehouseID,0) DESC, ISNULL(bitPrimary,0) DESC, bitPreferredMethod DESC, numListItemID ASC
						) TempLeadDays
						WHERE
							Item.numDomainID=',@numDomainID,'
							AND 1 = (CASE 
										WHEN ',@bitShowAllItems,' = 1 
										THEN 1 
										ELSE 
											(CASE 
												WHEN ', ISNULL(@tintDemandPlanBasedOn,1),' = 2
												THEN (CASE 
														WHEN (CASE
																WHEN ',@tintUnitsRecommendationForAutoPOBackOrder,'= 3
																THEN 
																	(ISNULL(numBackOrder,0) - ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  
																									THEN ISNULL((SELECT 
																												SUM(numUnitHour) 
																											FROM 
																												OpportunityItems 
																											INNER JOIN 
																												OpportunityMaster 
																											ON 
																												OpportunityItems.numOppID=OpportunityMaster.numOppId 
																											WHERE 
																												OpportunityMaster.numDomainId=',@numDomainID,'
																												AND OpportunityMaster.tintOppType=2 
																												AND OpportunityMaster.tintOppStatus=0 
																												AND OpportunityItems.numItemCode=Item.numItemCode 
																												AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) 
																									ELSE 0 
																									END)) +
																	(CASE WHEN ISNULL(Item.fltReorderQty,0) >= ISNULL(V.intMinQty,0) THEN ISNULL(Item.fltReorderQty,0) ELSE ISNULL(V.intMinQty,0) END) 
																WHEN ',@tintUnitsRecommendationForAutoPOBackOrder,'=2
																THEN 
																	(CASE
																		WHEN ISNULL(Item.fltReorderQty,0) >= ISNULL(V.intMinQty,0) AND ISNULL(Item.fltReorderQty,0) >= ISNULL(numBackOrder,0) THEN ISNULL(Item.fltReorderQty,0)
																		WHEN ISNULL(V.intMinQty,0) >= ISNULL(Item.fltReorderQty,0) AND ISNULL(V.intMinQty,0) >= ISNULL(numBackOrder,0) THEN ISNULL(V.intMinQty,0)
																		WHEN ISNULL(numBackOrder,0) >= ISNULL(Item.fltReorderQty,0) AND ISNULL(numBackOrder,0) >= ISNULL(V.intMinQty,0) THEN ISNULL(numBackOrder,0)
																		ELSE ISNULL(Item.fltReorderQty,0)
																	END) - (ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  THEN ISNULL((SELECT 
																												SUM(numUnitHour) 
																											FROM 
																												OpportunityItems 
																											INNER JOIN 
																												OpportunityMaster 
																											ON 
																												OpportunityItems.numOppID=OpportunityMaster.numOppId 
																											WHERE 
																												OpportunityMaster.numDomainId=',@numDomainID,'
																												AND OpportunityMaster.tintOppType=2 
																												AND OpportunityMaster.tintOppStatus=0 
																												AND OpportunityItems.numItemCode=Item.numItemCode 
																												AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END))
																ELSE
																	(CASE WHEN ISNULL(Item.fltReorderQty,0) > ISNULL(V.intMinQty,0) THEN ISNULL(Item.fltReorderQty,0) ELSE ISNULL(V.intMinQty,0) END) 
																	+ ISNULL(numBackOrder,0) - (ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  THEN ISNULL((SELECT 
																																											SUM(numUnitHour) 
																																										FROM 
																																											OpportunityItems 
																																										INNER JOIN 
																																											OpportunityMaster 
																																										ON 
																																											OpportunityItems.numOppID=OpportunityMaster.numOppId 
																																										WHERE 
																																											OpportunityMaster.numDomainId=',@numDomainID,'
																																											AND OpportunityMaster.tintOppType=2 
																																											AND OpportunityMaster.tintOppStatus=0 
																																											AND OpportunityItems.numItemCode=Item.numItemCode 
																																											AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END)) 
															END) > 0
														THEN 1
														ELSE 0
													END)
												ELSE
													(CASE 
														WHEN 
															T1.numUnitHour > 0 
														THEN 1 
														ELSE 0 
													END)
											END)
									END)
						')

	IF CHARINDEX('I.',@vcRegularSearchCriteria) > 0
	BEGIN			   
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'I.','Item.')
	END
	IF CHARINDEX('WH.',@vcRegularSearchCriteria) > 0
	BEGIN			   
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WH.','W.')
	END
	
	IF @vcRegularSearchCriteria<>'' 
	BEGIN
		SET @strSql=@strSql +' AND ' + @vcRegularSearchCriteria 
	END
	  
	DECLARE  @firstRec  AS INTEGER
	DECLARE  @lastRec  AS INTEGER
  
	SET @firstRec = (@CurrentPage - 1) * @PageSize
	SET @lastRec = (@CurrentPage * @PageSize + 1)

	-- EXECUTE FINAL QUERY
	DECLARE @strFinal AS NVARCHAR(MAX)
	SET @strFinal = CONCAT(@StrSql,' ORDER BY ',CASE WHEN CHARINDEX(@columnName,@strColumns) > 0 THEN @columnName ELSE 'Item.numItemCode' END,' OFFSET ',(@CurrentPage-1) * @PageSize,' ROWS FETCH NEXT ',@PageSize,' ROWS ONLY')

	PRINT CAST(@strFinal AS NTEXT)
	EXEC sp_executesql @strFinal

	SELECT * FROM #tempForm

	DROP TABLE #tempForm

	DROP TABLE #TEMPItems


	UPDATE DemandForecast SET dtExecutionDate = dateadd(MINUTE,-@ClientTimeZoneOffset,GETDATE()) WHERE numDFID = @numDFID

	SELECT CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,@dtLastExecution)) AS dtLastExecution
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_GetByItemVendor')
DROP PROCEDURE dbo.USP_DemandForecast_GetByItemVendor
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_GetByItemVendor]
	 @numDomainID AS NUMERIC(9),
	 @numItemCode AS NUMERIC(9),
	 @numVendorID AS NUMERIC(18,0)
AS 
BEGIN
	

	SELECT 
		ISNULL((SELECT TOP 1 monCost FROM Vendor WHERE numVendorID = @numVendorID AND numItemCode = @numItemCode),0) AS monCost
		,(SELECT TOP 1 numContactId FROM AdditionalContactsInformation WHERE numDivisionId = @numVendorID ORDER BY ISNULL(bitPrimaryContact,0) DESC) AS numContactID
	SELECT 
		LD.numListItemID
		,LD.vcData
		,ISNULL(numListValue,0) AS numListValue
		,CONCAT('(',ISNULL(numListValue,'-'),' Days)') AS vcLeadDays
	FROM 
		ListDetails  LD
	OUTER APPLY
	(
		SELECT TOP 1
			*
		FROM
			VendorShipmentMethod VSM
		WHERE
			VSM.numListItemID = LD.numListItemID
			AND VSM.numVendorID = @numVendorID
		ORDER BY
			ISNULL(bitPrimary,0) DESC, ISNULL(bitPreferredMethod,0) DESC
	) VSMO
	WHERE 
		(LD.numDomainID=@numDomainID OR ISNULL(LD.constFlag,0) = 1)
		AND LD.numListID = 338
	ORDER BY
		bitPreferredMethod DESC,numListItemID ASC
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Domain_GetColumnValue')
DROP PROCEDURE USP_Domain_GetColumnValue
GO
CREATE PROCEDURE [dbo].[USP_Domain_GetColumnValue]
(
    @numDomainID AS NUMERIC(18,0),
	@vcDbColumnName AS VARCHAR(300)
)
AS 
BEGIN
	IF @vcDbColumnName = 'tintCommitAllocation'
	BEGIN
		SELECT ISNULL(tintCommitAllocation,1) AS tintCommitAllocation FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'bitEDI'
	BEGIN
		SELECT ISNULL(bitEDI,0) AS bitEDI FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'bitReOrderPoint'
	BEGIN
		SELECT ISNULL(bitReOrderPoint,0) AS bitReOrderPoint FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'tintMarkupDiscountOption'
	BEGIN
		SELECT ISNULL(tintMarkupDiscountOption,0) AS tintMarkupDiscountOption FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'tintMarkupDiscountValue'
	BEGIN
		SELECT ISNULL(tintMarkupDiscountValue,0) AS tintMarkupDiscountValue FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'bitDoNotShowDropshipPOWindow'
	BEGIN
		SELECT ISNULL(bitDoNotShowDropshipPOWindow,0) AS bitDoNotShowDropshipPOWindow FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'tintReceivePaymentTo'
	BEGIN
		SELECT ISNULL(tintReceivePaymentTo,0) AS tintReceivePaymentTo FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'numReceivePaymentBankAccount'
	BEGIN
		SELECT ISNULL(numReceivePaymentBankAccount,0) AS numReceivePaymentBankAccount FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'bitCloneLocationWithItem'
	BEGIN
		SELECT ISNULL(bitCloneLocationWithItem,0) AS bitCloneLocationWithItem FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'vcBluePaySuccessURL'
	BEGIN
		SELECT ISNULL(vcBluePaySuccessURL,0) AS vcBluePaySuccessURL FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'vcBluePayDeclineURL'
	BEGIN
		SELECT ISNULL(vcBluePayDeclineURL,0) AS vcBluePayDeclineURL FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'bitUseDeluxeCheckStock'
	BEGIN
		SELECT ISNULL(bitUseDeluxeCheckStock,0) AS bitUseDeluxeCheckStock FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'vcPOHiddenColumns'
	BEGIN
		SELECT ISNULL(vcPOHiddenColumns,0) AS vcPOHiddenColumns FROM SalesOrderConfiguration WHERE numDomainId=@numDomainID
	END
END
GO

/*
-- Added Support for Bills in AP Aging summary,by chintan
*/

-- [dbo].[USP_GetAccountPayableAging] 1,null 6719
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getaccountpayableaging')
DROP PROCEDURE usp_getaccountpayableaging
GO
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

CREATE PROCEDURE [dbo].[USP_GetAccountPayableAging]
(
    @numDomainId AS NUMERIC(9) = 0,
    @numDivisionId AS NUMERIC(9) = NULL,
    @numAccountClass AS NUMERIC(9)=0,
	@dtFromDate AS DATETIME,
	@dtToDate AS DATETIME,
	@ClientTimeZoneOffset INT = 0
)
AS 
BEGIN 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @numTempDomainId NUMERIC(18,0)
	DECLARE @numTempDivisionId NUMERIC(18,0)
	DECLARE @numTempAccountClass NUMERIC(18,0)
	DECLARE @dtTempFromDate AS DATETIME
	DECLARE @dtTempToDate AS DATETIME


	SET @numTempDomainId = @numDomainId
	SET @numTempDivisionId = @numDivisionId
	SET @numTempAccountClass = @numAccountClass
	SET @dtTempFromDate = @dtFromDate
	SET @dtTempToDate = @dtToDate

		IF @dtTempFromDate IS NULL
			SET @dtTempFromDate = '1990-01-01 00:00:00.000'
		IF @dtTempToDate IS NULL
			SET @dtTempToDate = GETUTCDATE()

        CREATE TABLE #TempAPRecord
            (
              [ID] [int] IDENTITY(1, 1)
                         NOT NULL,
              [numUserCntID] [numeric](18, 0) NOT NULL,
              [numOppId] [numeric](18, 0) NULL,
              [numDivisionId] [numeric](18, 0) NULL,
              [numOppBizDocsId] [numeric](18, 0) NULL,
              [DealAmount] [float] NULL,
              [numBizDocId] [numeric](18, 0) NULL,
              dtDueDate DATETIME NULL,
              [numCurrencyID] [numeric](18, 0) NULL,AmountPaid FLOAT NULL,monUnAppliedAmount DECIMAL(20,5) NULL 
            ) ;
  
  
        CREATE TABLE #TempAPRecord1
            (
              [numID] [numeric](18, 0) IDENTITY(1, 1)
                                       NOT NULL,
              [numUserCntID] [numeric](18, 0) NOT NULL,
              [numDivisionId] [numeric](18, 0) NULL,
              [tintCRMType] [tinyint] NULL,
              [vcCompanyName] [varchar](200) NULL,
              [vcCustPhone] [varchar](50) NULL,
              [numContactId] [numeric](18, 0) NULL,
              [vcEmail] [varchar](100) NULL,
              [numPhone] [varchar](50) NULL,
              [vcContactName] [varchar](100) NULL,
			  [numCurrentDays] [numeric](18, 2) NULL,
              [numThirtyDays] [numeric](18, 2) NULL,
              [numSixtyDays] [numeric](18, 2) NULL,
              [numNinetyDays] [numeric](18, 2) NULL,
              [numOverNinetyDays] [numeric](18, 2) NULL,
              [numThirtyDaysOverDue] [numeric](18, 2) NULL,
              [numSixtyDaysOverDue] [numeric](18, 2) NULL,
              [numNinetyDaysOverDue] [numeric](18, 2) NULL,
              [numOverNinetyDaysOverDue] [numeric](18, 2) NULL,
              [numCompanyID] [numeric](18, 2) NULL,
              [numDomainID] [numeric](18, 2) NULL,
			  [intCurrentDaysCount] [int] NULL,
              [intThirtyDaysCount] [int] NULL,
              [intThirtyDaysOverDueCount] [int] NULL,
              [intSixtyDaysCount] [int] NULL,
              [intSixtyDaysOverDueCount] [int] NULL,
              [intNinetyDaysCount] [int] NULL,
              [intOverNinetyDaysCount] [int] NULL,
              [intNinetyDaysOverDueCount] [int] NULL,
              [intOverNinetyDaysOverDueCount] [int] NULL,
			  [numCurrentDaysPaid] [numeric](18, 2) NULL,
              [numThirtyDaysPaid] [numeric](18, 2) NULL,
              [numSixtyDaysPaid] [numeric](18, 2) NULL,
              [numNinetyDaysPaid] [numeric](18, 2) NULL,
              [numOverNinetyDaysPaid] [numeric](18, 2) NULL,
              [numThirtyDaysOverDuePaid] [numeric](18, 2) NULL,
              [numSixtyDaysOverDuePaid] [numeric](18, 2) NULL,
              [numNinetyDaysOverDuePaid] [numeric](18, 2) NULL,
              [numOverNinetyDaysOverDuePaid] [numeric](18, 2) NULL
              ,monUnAppliedAmount DECIMAL(20,5) NULL,numTotal DECIMAL(20,5) NULL 
            ) ;
  
  
  
        DECLARE @AuthoritativePurchaseBizDocId AS INTEGER
        SELECT  @AuthoritativePurchaseBizDocId = isnull(numAuthoritativePurchase,
                                                        0)
        FROM    AuthoritativeBizDocs
        WHERE   numDomainId = @numTempDomainId ;

		DECLARE @TEMPBillPayments TABLE
		(
			numBillID NUMERIC(18,0)
			,numOppBizDocsID NUMERIC(18,0)
			,monAmount DECIMAL(20,5)
		)

		INSERT INTO @TEMPBillPayments
		(
			numBillID
			,numOppBizDocsID
			,monAmount
		)
		SELECT 
			BPD.numBillID
			,BPD.numOppBizDocsID
			,BPD.monAmount
		FROM
			BillPaymentHeader BPH
		INNER JOIN
			BillPaymentDetails BPD
		ON
			BPH.numBillPaymentID=BPD.numBillPaymentID
		WHERE
			BPH.numDomainID=@numTempDomainId
			AND CAST(BPH.dtPaymentDate AS DATE) <= @dtTempToDate        
     
        INSERT  INTO [#TempAPRecord]
                (
                  numUserCntID,
                  [numOppId],
                  [numDivisionId],
                  [numOppBizDocsId],
                  [DealAmount],
                  [numBizDocId],
                  dtDueDate,
                  [numCurrencyID],AmountPaid
                )
                SELECT  @numTempDomainId,
                        OB.numOppId,
                        Opp.numDivisionId,
                        OB.numOppBizDocsId,
                        ISNULL(OB.monDealAmount --dbo.GetDealAmount(OB.numOppID,getutcdate(),OB.numOppBizDocsId)
                               * ISNULL(Opp.fltExchangeRate, 1), 0)
                        - ISNULL(ISNULL(SUM(TBP.monAmount),0)
                                 * ISNULL(Opp.fltExchangeRate, 1), 0) DealAmount,
                        OB.numBizDocId,
                        DATEADD(DAY,
                                CASE WHEN Opp.bitBillingTerms = 1
                                     --THEN convert(int, ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays, 0)), 0))
                                     THEN convert(int, ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)), 0))
                                     ELSE 0
                                END, dtFromDate) AS dtDueDate,
                        ISNULL(Opp.numCurrencyID, 0) numCurrencyID,
                        ISNULL(ISNULL(SUM(TBP.monAmount),0) * ISNULL(Opp.fltExchangeRate, 1), 0) AmountPaid
                FROM    OpportunityMaster Opp
                        JOIN OpportunityBizDocs OB ON OB.numOppId = Opp.numOppID
                        LEFT OUTER JOIN [Currency] C ON Opp.[numCurrencyID] = C.[numCurrencyID]
						LEFT JOIN @TEMPBillPayments TBP ON OB.numOppBizDocsId=TBP.numOppBizDocsID
                WHERE   tintOpptype = 2
                        AND tintoppStatus = 1
                        AND opp.numDomainID = @numTempDomainId
                        AND OB.bitAuthoritativeBizDocs = 1
                        AND ISNULL(OB.tintDeferred, 0) <> 1
                        AND numBizDocId = @AuthoritativePurchaseBizDocId
                        AND ( Opp.numDivisionId = @numTempDivisionId
                              OR @numTempDivisionId IS NULL
                            )
                        AND (Opp.numAccountClass=@numTempAccountClass OR @numTempAccountClass=0)
						AND OB.[dtCreatedDate] <= @dtTempToDate
                GROUP BY OB.numOppId,
                        Opp.numDivisionId,
                        OB.numOppBizDocsId,
                        Opp.fltExchangeRate,
                        OB.numBizDocId,
                        OB.dtCreatedDate,
                        Opp.bitBillingTerms,
                        Opp.intBillingDays,
                        OB.monDealAmount,
                        Opp.numCurrencyID,
                        OB.[dtFromDate]
                HAVING  ( ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate,
                                                           1), 0)
                          - ISNULL(ISNULL(SUM(TBP.monAmount),0)
                                   * ISNULL(Opp.fltExchangeRate, 1), 0) ) > 0
                UNION ALL 
		--Add Bill Amount
                SELECT
					@numTempDomainId,
					0 numOppId,
					numDivisionId,
					0 numBizDocsId,
					ISNULL(SUM(monAmountDue), 0) - ISNULL(SUM(monAmountPaid),0) DealAmount,
					0 numBizDocId,
					dtDueDate AS dtDueDate,
					0 numCurrencyID
					,ISNULL(SUM(monAmountPaid),0) AS AmountPaid
				FROM
				(
					SELECT 
						BH.numBillID
						,BH.numDivisionId
						,BH.dtDueDate
						,BH.monAmountDue
						,ISNULL(SUM(TEMPPaid.monAmount),0) monAmountPaid
					FROM
						BillHeader BH
					LEFT JOIN
						@TEMPBillPayments TEMPPaid
					ON
						BH.numBillID = TEMPPaid.numBillID
					WHERE
						BH.numDomainID = @numTempDomainId
						AND (BH.numDivisionId = @numTempDivisionId OR @numTempDivisionId IS NULL)
						AND (BH.numAccountClass=@numTempAccountClass OR @numTempAccountClass=0)
						AND BH.[dtBillDate] <= @dtTempToDate
					GROUP BY
						BH.numBillID
						,BH.numDivisionId
						,BH.dtDueDate
						,BH.monAmountDue
					HAVING
						 ISNULL(BH.monAmountDue, 0) - ISNULL(SUM(TEMPPaid.monAmount),0) > 0  
				) TEMP
				GROUP BY 
					numDivisionId,
					dtDueDate
				HAVING
					ISNULL(SUM(monAmountDue), 0) - ISNULL(SUM(monAmountPaid),0) > 0 
                UNION ALL
		--Add Write Check entry (As Amount Paid)
                SELECT  @numTempDomainId,
                        0 numOppId,
                        CD.numCustomerId as numDivisionId,
                        0 numBizDocsId,
                        0 DealAmount,
                        0 numBizDocId,
                        CH.dtCheckDate AS dtDueDate,
                        0 numCurrencyID,ISNULL(SUM(CD.monAmount), 0) AS AmountPaid
                FROM    CheckHeader CH
						JOIN  dbo.CheckDetails CD ON CH.numCheckHeaderID=CD.numCheckHeaderID
                        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = CD.numChartAcntId
                WHERE   CH.numDomainID = @numTempDomainId AND CH.tintReferenceType=1
						AND COA.vcAccountCode LIKE '01020102%'
                        AND ( CD.numCustomerId = @numTempDivisionId
                              OR @numTempDivisionId IS NULL
                            )
                        AND (CH.numAccountClass=@numTempAccountClass OR @numTempAccountClass=0)
						AND CH.[dtCheckDate] <= @dtTempToDate
                GROUP BY CD.numCustomerId,
                        CH.dtCheckDate
		--Show Impact of AP journal entries in report as well 
                UNION ALL
                SELECT  @numTempDomainId,
                        0,
                        GJD.numCustomerId,
                        0,
                        CASE WHEN GJD.numDebitAmt > 0 THEN -1 * GJD.numDebitAmt
                             ELSE GJD.numCreditAmt
                        END,
                        0,
                        GJH.datEntry_Date,
                        GJD.[numCurrencyID],0 AS AmountPaid
                FROM    dbo.General_Journal_Header GJH
                        INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
                                                                      AND GJH.numDomainId = GJD.numDomainId
                        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
                WHERE   GJH.numDomainId = @numTempDomainId 
                        AND COA.vcAccountCode LIKE '01020102%'
                        AND ISNULL(numOppId, 0) = 0
                        AND ISNULL(numOppBizDocsId, 0) = 0
                        AND ISNULL(GJH.numBillID,0)=0 AND ISNULL(GJH.numBillPaymentID,0)=0 AND ISNULL(GJH.numCheckHeaderID,0)=0 AND ISNULL(GJH.numReturnID,0)=0
                        AND ISNULL(GJH.numPayrollDetailID,0)=0
                         AND ( GJD.numCustomerId = @numTempDivisionId
                              OR @numTempDivisionId IS NULL
                            )
                            AND (GJH.numAccountClass=@numTempAccountClass OR @numTempAccountClass=0)
						AND [GJH].[datEntry_Date] <= @dtTempToDate
                UNION ALL
				--Add Commission Amount
                SELECT  @numTempDomainId,
                        OBD.numOppId numOppId,
                        D.numDivisionId,
                        BDC.numOppBizDocId numBizDocsId,
                       isnull(sum(BDC.numComissionAmount - ISNULL(PT.monCommissionAmt,0)),0)  DealAmount,
                        0 numBizDocId,
                        OBD.dtCreatedDate AS dtDueDate,
                        ISNULL(OPP.numCurrencyID, 0) numCurrencyID,0 AS AmountPaid
                FROM    BizDocComission BDC
                        JOIN OpportunityBizDocs OBD ON BDC.numOppBizDocId = OBD.numOppBizDocsId
                        join OpportunityMaster Opp on OPP.numOppId = OBD.numOppId
                        JOIN Domain D on D.numDomainID = BDC.numDomainID
                        LEFT JOIN dbo.PayrollTracking PT ON BDC.numComissionID=PT.numComissionID
                WHERE   OPP.numDomainID = @numTempDomainId
                        and BDC.numDomainID = @numTempDomainId
                        AND (D.numDivisionId = @numTempDivisionId OR @numTempDivisionId IS NULL)
                        AND (Opp.numAccountClass=@numTempAccountClass OR @numTempAccountClass=0)
						AND OBD.[dtCreatedDate] <= @dtTempToDate
						AND ISNULL(BDC.bitCommisionPaid,0)=0
                        AND OBD.bitAuthoritativeBizDocs = 1
                GROUP BY D.numDivisionId,
                        OBD.numOppId,
                        BDC.numOppBizDocId,
                        OPP.numCurrencyID,
                        BDC.numComissionAmount,
                        OBD.dtCreatedDate
                HAVING  ISNULL(BDC.numComissionAmount, 0) > 0
			UNION ALL --Standalone Refund against Account Receivable
			 SELECT @numTempDomainId,
					0 AS numOppID,
					RH.numDivisionId,
					0,
					CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt * -1 ELSE GJD.numCreditAmt END DealAmount,
					0 numBizDocId,
					GJH.datEntry_Date,GJD.[numCurrencyID],0 AS AmountPaid
			 FROM   dbo.ReturnHeader RH JOIN dbo.General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
			JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId 
			INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
			WHERE RH.tintReturnType=4 AND RH.numDomainId=@numTempDomainId  AND COA.vcAccountCode LIKE '01020102%'
					AND (RH.numDivisionId = @numTempDivisionId OR @numTempDivisionId IS NULL)
					AND (monBizDocAmount > 0) AND ISNULL(RH.numBillPaymentIDRef,0)>0
					AND (RH.numAccountClass=@numTempAccountClass OR @numTempAccountClass=0)
					AND GJH.datEntry_Date <= @dtTempToDate
					--GROUP BY DM.numDivisionId,DM.dtDepositDate
		
		
---SELECT  SUM(monUnAppliedAmount) FROM    [#TempAPRecord]

   INSERT  INTO [#TempAPRecord]
        (
          numUserCntID,
          [numOppId],
          [numDivisionId],
          [numOppBizDocsId],
          [DealAmount],
          [numBizDocId],
          [numCurrencyID],
          [dtDueDate],AmountPaid,monUnAppliedAmount
        )
SELECT @numTempDomainId,0, BPH.numDivisionId,0,0,0,0,NULL,0,sum(ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0))       
 FROM BillPaymentHeader BPH
		WHERE BPH.numDomainId=@numTempDomainId   
		AND (BPH.numDivisionId = @numTempDivisionId
                            OR @numTempDivisionId IS NULL)
		AND (ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0)) > 0
		AND (BPH.numAccountClass=@numTempAccountClass OR @numTempAccountClass=0)
		AND [BPH].[dtPaymentDate] <= @dtTempToDate
		GROUP BY BPH.numDivisionId
		

--SELECT  GETDATE()
        INSERT  INTO [#TempAPRecord1]
                (
                  [numUserCntID],
                  [numDivisionId],
                  [tintCRMType],
                  [vcCompanyName],
                  [vcCustPhone],
                  [numContactId],
                  [vcEmail],
                  [numPhone],
                  [vcContactName],
				  [numCurrentDays],
                  [numThirtyDays],
                  [numSixtyDays],
                  [numNinetyDays],
                  [numOverNinetyDays],
                  [numThirtyDaysOverDue],
                  [numSixtyDaysOverDue],
                  [numNinetyDaysOverDue],
                  [numOverNinetyDaysOverDue],
                  numCompanyID,
                  numDomainID,
				  [intCurrentDaysCount],
                  [intThirtyDaysCount],
                  [intThirtyDaysOverDueCount],
                  [intSixtyDaysCount],
                  [intSixtyDaysOverDueCount],
                  [intNinetyDaysCount],
                  [intOverNinetyDaysCount],
                  [intNinetyDaysOverDueCount],
                  [intOverNinetyDaysOverDueCount],
				  [numCurrentDaysPaid],
                  [numThirtyDaysPaid],
                  [numSixtyDaysPaid],
                  [numNinetyDaysPaid],
                  [numOverNinetyDaysPaid],
                  [numThirtyDaysOverDuePaid],
                  [numSixtyDaysOverDuePaid],
                  [numNinetyDaysOverDuePaid],
                  [numOverNinetyDaysOverDuePaid],monUnAppliedAmount
                )
                SELECT DISTINCT
                        @numTempDomainId,
                        Div.numDivisionID,
                        tintCRMType AS tintCRMType,
                        vcCompanyName AS vcCompanyName,
                        '',
                        NULL,
                        '',
                        '',
                        '',
						0 numCurrentDays,
                        0 numThirtyDays,
                        0 numSixtyDays,
                        0 numNinetyDays,
                        0 numOverNinetyDays,
                        0 numThirtyDaysOverDue,
                        0 numSixtyDaysOverDue,
                        0 numNinetyDaysOverDue,
                        0 numOverNinetyDaysOverDue,
                        C.numCompanyID,
                        Div.numDomainID,
						0 [intCurrentDaysCount],
                        0 [intThirtyDaysCount],
                        0 [intThirtyDaysOverDueCount],
                        0 [intSixtyDaysCount],
                        0 [intSixtyDaysOverDueCount],
                        0 [intNinetyDaysCount],
                        0 [intOverNinetyDaysCount],
                        0 [intNinetyDaysOverDueCount],
                        0 [intOverNinetyDaysOverDueCount],
						0 numCurrentDaysPaid,
                         0 numThirtyDaysPaid,
                        0 numSixtyDaysPaid,
                        0 numNinetyDaysPaid,
                        0 numOverNinetyDaysPaid,
                        0 numThirtyDaysOverDuePaid,
                        0 numSixtyDaysOverDuePaid,
                        0 numNinetyDaysOverDuePaid,
                        0 numOverNinetyDaysOverDuePaid,0 monUnAppliedAmount
                FROM    Divisionmaster Div
                        INNER JOIN CompanyInfo C ON C.numCompanyId = Div.numCompanyID
                    --JOIN [#TempAPRecord] t ON Div.[numDivisionID] = t.[numDivisionId]
                WHERE   Div.[numDomainID] = @numTempDomainId
                        AND Div.[numDivisionID] IN ( SELECT DISTINCT
                                                            [numDivisionID]
                                                     FROM   [#TempAPRecord] )


--Update Custome Phone 
        UPDATE  #TempAPRecord1
        SET     [vcCustPhone] = X.[vcCustPhone]
        FROM    ( SELECT    CASE WHEN numPhone IS NULL THEN ''
                                 WHEN numPhone = '' THEN ''
                                 ELSE ', ' + numPhone
                            END AS vcCustPhone,
                            numDivisionId
                  FROM      AdditionalContactsInformation
                  WHERE     bitPrimaryContact = 1
                            AND numDivisionId IN (
                            SELECT DISTINCT
                                    numDivisionID
                            FROM    [#TempAPRecord1]
                            WHERE   numUserCntID = @numTempDomainId )
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID

--Update Customer info 
        UPDATE  #TempAPRecord1
        SET     [numContactId] = X.[numContactID],
                [vcEmail] = X.[vcEmail],
                [vcContactName] = X.[vcContactName],
                numPhone = X.numPhone
        FROM    ( SELECT  numContactId AS numContactID,
                            ISNULL(vcEmail, '') AS vcEmail,
                            ISNULL(vcFirstname, '') + ' ' + ISNULL(vcLastName, '') AS vcContactName,
                            CAST(ISNULL(CASE WHEN numPhone IS NULL THEN ''
                                             WHEN numPhone = '' THEN ''
                                             ELSE ', ' + numPhone
                                        END, '') AS VARCHAR) AS numPhone,
                            [numDivisionId]
                  FROM      AdditionalContactsInformation
                  WHERE     (vcPosition = 843 or isnull(bitPrimaryContact,0)=1)
                            AND numDivisionId IN (
                            SELECT DISTINCT
                                    numDivisionID
                            FROM    [#TempAPRecord1]
                            WHERE   numUserCntID = @numTempDomainId )
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID

                    
--SELECT  GETDATE()
------------------------------------------      
        DECLARE @baseCurrency AS NUMERIC
        SELECT  @baseCurrency = numCurrencyID
        FROM    dbo.Domain
        WHERE   numDomainId = @numTempDomainId
/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */

------------------------------------------

-----------------------------------
        UPDATE  #TempAPRecord1
        SET     numCurrentDays = X.numCurrentDays
        FROM    ( SELECT    SUM(DealAmount) AS numCurrentDays,
                            [numDivisionId]
                  FROM      [#TempAPRecord]
                  WHERE     numUserCntID = @numTempDomainId AND
                            DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) >= 0

                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
         UPDATE  #TempAPRecord1
        SET     numCurrentDaysPaid = X.numCurrentDaysPaid
        FROM    ( SELECT    SUM(AmountPaid) AS numCurrentDaysPaid,
                            [numDivisionId]
                  FROM      [#TempAPRecord]
                  WHERE     numUserCntID = @numTempDomainId
                            AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) >= 0
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intCurrentDaysCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numTempDomainId
                        AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) >= 0
                        AND numCurrencyID <> @baseCurrency )
------------------------------------------

        UPDATE  #TempAPRecord1
        SET     numThirtyDays = X.numThirtyDays
        FROM    ( SELECT    SUM(DealAmount) AS numThirtyDays,
                            [numDivisionId]
                  FROM      [#TempAPRecord]
                  WHERE     numUserCntID = @numTempDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 1
                                                      AND     30
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
         UPDATE  #TempAPRecord1
        SET     numThirtyDaysPaid = X.numThirtyDaysPaid
        FROM    ( SELECT    SUM(AmountPaid) AS numThirtyDaysPaid,
                            [numDivisionId]
                  FROM      [#TempAPRecord]
                  WHERE     numUserCntID = @numTempDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 1
                                                      AND     30
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intThirtyDaysCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numTempDomainId
                        AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                     [dtDueDate]) BETWEEN 1
                                                  AND     30
                        AND numCurrencyID <> @baseCurrency )
------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numSixtyDays = X.numSixtyDays
        FROM    ( SELECT    SUM(DealAmount) AS numSixtyDays,
                            numDivisionId
                  FROM      [#TempAPRecord]
                  WHERE     numUserCntID = @numTempDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 31
                                                      AND     60
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     numSixtyDaysPaid = X.numSixtyDaysPaid
        FROM    ( SELECT    SUM(AmountPaid) AS numSixtyDaysPaid,
                            numDivisionId
                  FROM      [#TempAPRecord]
                  WHERE     numUserCntID = @numTempDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 31
                                                      AND     60
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intSixtyDaysCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numTempDomainId
                        AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                     [dtDueDate]) BETWEEN 31
                                                  AND     60
                        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                        AND numCurrencyID <> @baseCurrency )

------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numNinetyDays = X.numNinetyDays
        FROM    ( SELECT    SUM(DealAmount) AS numNinetyDays,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numTempDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 61
                                                      AND     90
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
         UPDATE  #TempAPRecord1
        SET     numNinetyDaysPaid = X.numNinetyDaysPaid
        FROM    ( SELECT    SUM(AmountPaid) AS numNinetyDaysPaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numTempDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 61
                                                      AND     90
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intNinetyDaysCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numTempDomainId
                        AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                     [dtDueDate]) BETWEEN 61
                                                  AND     90
                        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                        AND numCurrencyID <> @baseCurrency )

------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numOverNinetyDays = X.numOverNinetyDays
        FROM    ( SELECT    SUM(DealAmount) AS numOverNinetyDays,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numTempDomainId
                            AND [dtDueDate] >= DATEADD(DAY, 91,
                                                       dbo.GetUTCDateWithoutTime())
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     numOverNinetyDaysPaid = X.numOverNinetyDaysPaid
        FROM    ( SELECT    SUM(AmountPaid) AS numOverNinetyDaysPaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numTempDomainId
                            AND [dtDueDate] >= DATEADD(DAY, 91,
                                                       dbo.GetUTCDateWithoutTime())
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intOverNinetyDaysCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numTempDomainId
                        AND [dtDueDate] >= DATEADD(DAY, 91,
                                                   dbo.GetUTCDateWithoutTime())
                        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                        AND numCurrencyID <> @baseCurrency )

------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numThirtyDaysOverDue = X.numThirtyDaysOverDue
        FROM    ( SELECT    SUM(DealAmount) AS numThirtyDaysOverDue,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numTempDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 1
                                                                      AND     30
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
         UPDATE  #TempAPRecord1
        SET     numThirtyDaysOverDuePaid = X.numThirtyDaysOverDuePaid
        FROM    ( SELECT    SUM(AmountPaid) AS numThirtyDaysOverDuePaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numTempDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 1
                                                                      AND     30
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intThirtyDaysOverDueCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numTempDomainId
                        AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                        AND DATEDIFF(DAY, [dtDueDate],
                                     dbo.GetUTCDateWithoutTime()) BETWEEN 1
                                                                  AND     30
                        AND numCurrencyID <> @baseCurrency )

----------------------------------
        UPDATE  #TempAPRecord1
        SET     numSixtyDaysOverDue = X.numSixtyDaysOverDue
        FROM    ( SELECT    SUM(DealAmount) AS numSixtyDaysOverDue,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numTempDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                                      AND     60
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
          UPDATE  #TempAPRecord1
        SET     numSixtyDaysOverDuePaid = X.numSixtyDaysOverDuePaid
        FROM    ( SELECT    SUM(AmountPaid) AS numSixtyDaysOverDuePaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numTempDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                                      AND     60
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intSixtyDaysOverDueCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numTempDomainId
                        AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                        AND DATEDIFF(DAY, [dtDueDate],
                                     dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                                  AND     60
                        AND numCurrencyID <> @baseCurrency )
------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numNinetyDaysOverDue = X.numNinetyDaysOverDue
        FROM    ( SELECT    SUM(DealAmount) AS numNinetyDaysOverDue,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numTempDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 61
                                                                      AND     90
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
         UPDATE  #TempAPRecord1
        SET     numNinetyDaysOverDuePaid = X.numNinetyDaysOverDuePaid
        FROM    ( SELECT    SUM(AmountPaid) AS numNinetyDaysOverDuePaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numTempDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 61
                                                                      AND     90
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intNinetyDaysOverDueCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numTempDomainId
                        AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                        AND DATEDIFF(DAY, [dtDueDate],
                                     dbo.GetUTCDateWithoutTime()) BETWEEN 61
                                                                  AND     90
                        AND numCurrencyID <> @baseCurrency )
------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numOverNinetyDaysOverDue = X.numOverNinetyDaysOverDue
        FROM    ( SELECT    SUM(DealAmount) AS numOverNinetyDaysOverDue,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numTempDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) > 90
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
           UPDATE  #TempAPRecord1
        SET     numOverNinetyDaysOverDuePaid = X.numOverNinetyDaysOverDuePaid
        FROM    ( SELECT    SUM(AmountPaid) AS numOverNinetyDaysOverDuePaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numTempDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) > 90
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intOverNinetyDaysOverDueCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numTempDomainId
                        AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                        AND DATEDIFF(DAY, [dtDueDate],
                                     dbo.GetUTCDateWithoutTime()) > 90
                        AND numCurrencyID <> @baseCurrency )
             


UPDATE  
	TR1
SET
	monUnAppliedAmount = ISNULL((SELECT SUM(ISNULL(monUnAppliedAmount,0)) FROM  #TempAPRecord TR2 WHERE TR2.numDivisionId=TR1.numDivisionId),0)
FROM
	#TempAPRecord1 TR1

UPDATE  #TempAPRecord1 SET numTotal=  isnull(numCurrentDays, 0) + --isnull(numThirtyDays, 0) + 
				isnull(numThirtyDaysOverDue, 0)
                --+ isnull(numSixtyDays, 0) 
				+ isnull(numSixtyDaysOverDue, 0)
                --+ isnull(numNinetyDays, 0) 
				+ isnull(numNinetyDaysOverDue, 0)
                --+ isnull(numOverNinetyDays, 0)
                + isnull(numOverNinetyDaysOverDue, 0) 			
             
        SELECT  [numDivisionId],
                [numContactId],
                [tintCRMType],
                [vcCompanyName],
                [vcCustPhone],
                [vcContactName] as vcApName,
                [vcEmail],
                [numPhone] as vcPhone,
				[numCurrentDays],
                [numThirtyDays],
                [numSixtyDays],
                [numNinetyDays],
                [numOverNinetyDays],
                [numThirtyDaysOverDue],
                [numSixtyDaysOverDue],
                [numNinetyDaysOverDue],
                [numOverNinetyDaysOverDue],
             ISNULL(numTotal,0) - ISNULL(monUnAppliedAmount,0) AS numTotal,
                numCompanyID,
                numDomainID,
				[intCurrentDaysCount],
                [intThirtyDaysCount],
                [intThirtyDaysOverDueCount],
                [intSixtyDaysCount],
                [intSixtyDaysOverDueCount],
                [intNinetyDaysCount],
                [intOverNinetyDaysCount],
                [intNinetyDaysOverDueCount],
                [intOverNinetyDaysOverDueCount],
				[numCurrentDaysPaid],
                [numThirtyDaysPaid],
                [numSixtyDaysPaid],
                [numNinetyDaysPaid],
                [numOverNinetyDaysPaid],
                [numThirtyDaysOverDuePaid],
                [numSixtyDaysOverDuePaid],
                [numNinetyDaysOverDuePaid],
                [numOverNinetyDaysOverDuePaid],ISNULL(monUnAppliedAmount,0) AS monUnAppliedAmount
        FROM    #TempAPRecord1
        WHERE   numUserCntID = @numTempDomainId AND (numDivisionId = @numTempDivisionId OR @numTempDivisionId IS NULL)
        ORDER BY [vcCompanyName]

        drop table #TempAPRecord1
        drop table #TempAPRecord 

END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAccountReceivableAging')
DROP PROCEDURE USP_GetAccountReceivableAging
GO
CREATE PROCEDURE [dbo].[USP_GetAccountReceivableAging]
(
	@numDomainId   AS NUMERIC(9)  = 0,
	@numDivisionId AS NUMERIC(9)  = NULL,
	@numUserCntID AS NUMERIC(9),
	@numAccountClass AS NUMERIC(9)=0,
	@dtFromDate AS DATETIME = NULL,
	@dtToDate AS DATETIME = NULL,
	@ClientTimeZoneOffset INT = 0
)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF @dtFromDate IS NULL
		SET @dtFromDate = '1990-01-01 00:00:00.000'
	IF @dtToDate IS NULL
		SET @dtToDate = GETUTCDATE()

	--SET @dtFromDate = DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), @dtFromDate);
	--SET @dtToDate =  DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), @dtToDate);

	PRINT @dtFromDate
	PRINT @dtToDate

	DECLARE  @AuthoritativeSalesBizDocId  AS INTEGER
    SELECT @AuthoritativeSalesBizDocId = isnull(numAuthoritativeSales,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId = @numDomainId;
  
INSERT  INTO [TempARRecord]
        (
          numUserCntID,
          [numOppId],
          [numDivisionId],
          [numOppBizDocsId],
          [DealAmount],
          [numBizDocId],
          [numCurrencyID],
          [dtDueDate],AmountPaid
        )
        SELECT  @numUserCntID,
                OB.numOppId,
                Opp.numDivisionId,
                OB.numOppBizDocsId,
                ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate, 1), 0) - ISNULL(ISNULL(TablePayments.monPaidAmount,0) * ISNULL(Opp.fltExchangeRate, 1),0) DealAmount,
                OB.numBizDocId,
                ISNULL(Opp.numCurrencyID, 0) numCurrencyID,
				DATEADD(DAY,CASE WHEN Opp.bitBillingTerms = 1 
								 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)), 0))
								 ELSE 0 
							END, dtFromDate) AS dtDueDate,
                ISNULL(ISNULL(TablePayments.monPaidAmount,0) * ISNULL(Opp.fltExchangeRate, 1),0) as AmountPaid
        FROM    OpportunityMaster Opp
                JOIN OpportunityBizDocs OB ON OB.numOppId = Opp.numOppID
                LEFT OUTER JOIN [Currency] C ON Opp.[numCurrencyID] = C.[numCurrencyID]
				JOIN [dbo].[DivisionMaster] AS DIV ON DIV.[numDivisionID] = Opp.[numDivisionId]
				OUTER APPLY
			   (
					SELECT 
						SUM(monAmountPaid) AS monPaidAmount
					FROM
						DepositeDetails DD
					INNER JOIN
						DepositMaster DM
					ON
						DD.numDepositID=DM.numDepositId
					WHERE 
						numOppID = Opp.numOppID
						AND numOppBizDocsID=OB.numOppBizDocsID
						AND CONVERT(DATE,DM.dtDepositDate) <= CAST(@dtToDate AS DATE)
				) TablePayments
        WHERE   tintOpptype = 1
                AND tintoppStatus = 1
                --AND dtShippedDate IS NULL --Commented by chintan reason: Account Transaction and AR values was not matching
                AND opp.numDomainID = @numDomainId
                AND numBizDocId = @AuthoritativeSalesBizDocId
                AND OB.bitAuthoritativeBizDocs=1
                AND ISNULL(OB.tintDeferred,0) <> 1
                AND (Opp.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
                            AND (Opp.numAccountClass=@numAccountClass OR @numAccountClass=0)
				AND CONVERT(DATE,DATEADD(DAY,CASE WHEN Opp.bitBillingTerms = 1 
								 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)), 0))
								 ELSE 0 
							END, dtFromDate)) <= @dtToDate
        GROUP BY OB.numOppId,
                Opp.numDivisionId,
                OB.numOppBizDocsId,
                Opp.fltExchangeRate,
                OB.numBizDocId,
                OB.dtCreatedDate,
                Opp.bitBillingTerms,
                Opp.intBillingDays,
                OB.monDealAmount,
                Opp.numCurrencyID,
                OB.dtFromDate
				,TablePayments.monPaidAmount
        HAVING  ( ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate, 1), 0)
                  - ISNULL(ISNULL(TablePayments.monPaidAmount,0) * ISNULL(Opp.fltExchangeRate, 1),
                           0) ) != 0
                           
	--Show Impact of AR journal entries in report as well 
UNION 
 SELECT @numUserCntID,
        0,
        GJD.numCustomerId,
        0,
        CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END,
        0,
        GJD.[numCurrencyID],
        GJH.datEntry_Date ,0 AS AmountPaid
 FROM   dbo.General_Journal_Header GJH
        INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
                                                      AND GJH.numDomainId = GJD.numDomainId
        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
		JOIN [dbo].[DivisionMaster] AS DIV ON DIV.[numDivisionID] = [GJD].[numCustomerId]
 WHERE  GJH.numDomainId = @numDomainId
        AND COA.vcAccountCode LIKE '01010105%'
        AND ISNULL(numOppId,0)=0 AND ISNULL(numOppBizDocsId,0)=0
        AND (GJD.numCustomerId = @numDivisionId OR @numDivisionId IS NULL)
		AND ISNULL(GJH.numDepositID,0)=0 AND ISNULL(GJH.numReturnID,0)=0
		AND (GJH.numAccountClass=@numAccountClass OR @numAccountClass=0)                            
		AND CONVERT(DATE,GJH.[datEntry_Date]) <= @dtToDate
UNION ALL
 SELECT @numUserCntID,
        0,
        DM.numDivisionId,
        0,
        (ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0) /*- ISNULL(monRefundAmount,0)*/) * -1,
        0,
        DM.numCurrencyID,
        DM.dtDepositDate ,0 AS AmountPaid
 FROM   DepositMaster DM 
 JOIN [dbo].[DivisionMaster] AS DIV ON [DM].[numDivisionID] = DIV.[numDivisionID]
 WHERE DM.numDomainId=@numDomainId AND tintDepositePage IN(2,3)  
		AND (DM.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
		AND ((ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0) /*- ISNULL(monRefundAmount,0)*/) > 0)
		AND (DM.numAccountClass=@numAccountClass OR @numAccountClass=0)
		--GROUP BY DM.numDivisionId,DM.dtDepositDate
		AND CONVERT(DATE,DM.[dtDepositDate]) <= @dtToDate
UNION ALL --Standalone Refund against Account Receivable
SELECT @numUserCntID,
        0,
        RH.numDivisionId,
        0,
        CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END,
		--(ISNULL(RH.[monBizDocAmount],0) - (CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END)),
        0,
        GJD.[numCurrencyID],
        GJH.datEntry_Date ,0 AS AmountPaid
 FROM   dbo.ReturnHeader RH JOIN dbo.General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
 JOIN [dbo].[DivisionMaster] AS DIV ON DIV.[numDivisionID] = RH.[numDivisionId]
JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId 
JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId AND COA.vcAccountCode LIKE '0101010501'
WHERE RH.tintReturnType=4 AND RH.numDomainId=@numDomainId  
AND ISNULL(RH.numParentID,0) = 0
AND ISNULL(RH.IsUnappliedPayment,0) = 0
		AND (RH.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
		AND (monBizDocAmount > 0) AND ISNULL(RH.numBillPaymentIDRef,0)=0 --AND ISNULL(RH.numDepositIDRef,0) > 0
		AND (RH.numAccountClass=@numAccountClass OR @numAccountClass=0)
		AND CONVERT(DATE,GJH.[datEntry_Date]) <= @dtToDate
		--AND RH.numDivisionId <> 235216
		--AND (ISNULL(RH.[monBizDocAmount],0) - (CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END)) != 0
		--GROUP BY DM.numDivisionId,DM.dtDepositDate

INSERT  INTO [TempARRecord]
        (
          numUserCntID,
          [numOppId],
          [numDivisionId],
          [numOppBizDocsId],
          [DealAmount],
          [numBizDocId],
          [numCurrencyID],
          [dtDueDate],AmountPaid,monUnAppliedAmount
        )
SELECT @numUserCntID,0, DM.numDivisionId,0,0,0,0,NULL,0,sum(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0))       
 FROM DepositMaster DM 
 JOIN [dbo].[DivisionMaster] AS DIV ON DIV.[numDivisionID] = DM.[numDivisionID]
 		WHERE DM.numDomainId=@numDomainId AND tintDepositePage IN(2,3)  
		AND (DM.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
		AND ((ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) > 0)
		AND (DM.numAccountClass=@numAccountClass OR @numAccountClass=0)
		AND CONVERT(DATE,DM.[dtDepositDate]) <= @dtToDate
		GROUP BY DM.numDivisionId


--SELECT  * FROM    [TempARRecord]


--SELECT  GETDATE()
INSERT  INTO [TempARRecord1] (
	[numUserCntID],
	[numDivisionId],
	[tintCRMType],
	[vcCompanyName],
	[vcCustPhone],
	[numContactId],
	[vcEmail],
	[numPhone],
	[vcContactName],
	[numCurrentDays],
	[numThirtyDays],
	[numSixtyDays],
	[numNinetyDays],
	[numOverNinetyDays],
	[numThirtyDaysOverDue],
	[numSixtyDaysOverDue],
	[numNinetyDaysOverDue],
	[numOverNinetyDaysOverDue],
	numCompanyID,
	numDomainID,
	[intCurrentDaysCount],
	[intThirtyDaysCount],
	[intThirtyDaysOverDueCount],
	[intSixtyDaysCount],
	[intSixtyDaysOverDueCount],
	[intNinetyDaysCount],
	[intOverNinetyDaysCount],
	[intNinetyDaysOverDueCount],
	[intOverNinetyDaysOverDueCount],
	[numCurrentDaysPaid],
	[numThirtyDaysPaid],
	[numSixtyDaysPaid],
	[numNinetyDaysPaid],
	[numOverNinetyDaysPaid],
	[numThirtyDaysOverDuePaid],
	[numSixtyDaysOverDuePaid],
	[numNinetyDaysOverDuePaid],
	[numOverNinetyDaysOverDuePaid],monUnAppliedAmount
) 
        SELECT DISTINCT
                @numUserCntID,
                Div.numDivisionID,
                tintCRMType AS tintCRMType,
                vcCompanyName AS vcCompanyName,
                '',
                NULL,
                '',
                '',
                '',
				0 numCurrentDays,
                0 numThirtyDays,
                0 numSixtyDays,
                0 numNinetyDays,
                0 numOverNinetyDays,
                0 numThirtyDaysOverDue,
                0 numSixtyDaysOverDue,
                0 numNinetyDaysOverDue,
                0 numOverNinetyDaysOverDue,
                C.numCompanyID,
				Div.numDomainID,
				0 [intCurrentDaysCount],
                0 [intThirtyDaysCount],
				0 [intThirtyDaysOverDueCount],
				0 [intSixtyDaysCount],
				0 [intSixtyDaysOverDueCount],
				0 [intNinetyDaysCount],
				0 [intOverNinetyDaysCount],
				0 [intNinetyDaysOverDueCount],
				0 [intOverNinetyDaysOverDueCount],
				0 numCurrentDaysPaid,
				0 numThirtyDaysPaid,
                0 numSixtyDaysPaid,
                0 numNinetyDaysPaid,
                0 numOverNinetyDaysPaid,
                0 numThirtyDaysOverDuePaid,
                0 numSixtyDaysOverDuePaid,
                0 numNinetyDaysOverDuePaid,
                0 numOverNinetyDaysOverDuePaid,0 monUnAppliedAmount
        FROM    Divisionmaster Div
                INNER JOIN CompanyInfo C ON C.numCompanyId = Div.numCompanyID
                    --JOIN [TempARRecord] t ON Div.[numDivisionID] = t.[numDivisionId]
        WHERE   Div.[numDomainID] = @numDomainId
                AND Div.[numDivisionID] IN ( SELECT DISTINCT
                                                    [numDivisionID]
                                             FROM   [TempARRecord] )



--Update Custome Phone 
UPDATE  TempARRecord1
SET     [vcCustPhone] = X.[vcCustPhone]
FROM    ( SELECT 
                            CASE WHEN numPhone IS NULL THEN ''
                                 WHEN numPhone = '' THEN ''
                                 ELSE ', ' + numPhone
                            END AS vcCustPhone,
                            numDivisionId
                  FROM      AdditionalContactsInformation
                  WHERE     bitPrimaryContact = 1
                            AND numDivisionId  IN (SELECT DISTINCT numDivisionID FROM [TempARRecord1] WHERE numUserCntID =@numUserCntID)

        ) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID

--Update Customer info 
UPDATE  TempARRecord1
SET     [numContactId] = X.[numContactID],[vcEmail] = X.[vcEmail],[vcContactName] = X.[vcContactName],numPhone=X.numPhone
FROM    ( SELECT numContactId AS numContactID,
                     ISNULL(vcEmail,'') AS vcEmail,
                     ISNULL(vcFirstname,'') + ' ' + ISNULL(vcLastName,'') AS vcContactName,
                     CAST(ISNULL( CASE WHEN numPhone IS NULL THEN '' WHEN numPhone = '' THEN '' ELSE ', ' + numPhone END,'') AS VARCHAR) AS numPhone,
                     [numDivisionId]
                  FROM      AdditionalContactsInformation
                  WHERE     (vcPosition = 844 or isnull(bitPrimaryContact,0)=1)
                            AND numDivisionId  IN (SELECT DISTINCT numDivisionID FROM [TempARRecord1] WHERE numUserCntID =@numUserCntID)
    ) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID

                    
--SELECT  GETDATE()
------------------------------------------      
DECLARE @baseCurrency AS NUMERIC
SELECT @baseCurrency=numCurrencyID FROM dbo.Domain WHERE numDomainId=@numDomainId
/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */
------------------------------------------

UPDATE  TempARRecord1
SET     numCurrentDays = X.numCurrentDays
FROM    ( SELECT    SUM(DealAmount) AS numCurrentDays,
                    [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND
					DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) >= 0
					--AND 
					--dbo.GetUTCDateWithoutTime() <= [dtDueDate]
          GROUP BY  numDivisionId
        ) X WHERE   TempARRecord1.numDivisionID = X.numDivisionID

UPDATE  TempARRecord1
SET     numCurrentDaysPaid = X.numCurrentDaysPaid
FROM    ( SELECT    SUM(AmountPaid) AS numCurrentDaysPaid,
                    [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND
					DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate])>= 0 
					--AND 
					--dbo.GetUTCDateWithoutTime() <= [dtDueDate]
          GROUP BY  numDivisionId
        ) X WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intCurrentDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
    FROM      [TempARRecord]
    WHERE     numUserCntID =@numUserCntID AND 
			 DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) >= 0
			  -- AND 
			  --dbo.GetUTCDateWithoutTime() <= [dtDueDate] 
			  AND 
			  numCurrencyID <>@baseCurrency )

---------------------------------------------------------------------------------------------


-----------------------Below ThirtyDays to OverNinetyDays is not being Used Now----------------
UPDATE  TempARRecord1
SET     numThirtyDays = X.numThirtyDays
FROM    ( SELECT    SUM(DealAmount) AS numThirtyDays,
                    [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND
		DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 0 AND 30	
          GROUP BY  numDivisionId
        ) X WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numThirtyDaysPaid = X.numThirtyDaysPaid
FROM    ( SELECT    SUM(AmountPaid) AS numThirtyDaysPaid,
                    [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND
		DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 0 AND 30	
          GROUP BY  numDivisionId
        ) X WHERE   TempARRecord1.numDivisionID = X.numDivisionID

                
UPDATE  TempARRecord1 SET intThirtyDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 0 AND 30	
                                                AND numCurrencyID <>@baseCurrency )
------------------------------------------
UPDATE  TempARRecord1
SET     numSixtyDays = X.numSixtyDays
FROM    ( SELECT  SUM(DealAmount) AS numSixtyDays,
        numDivisionId
FROM    [TempARRecord]
WHERE   numUserCntID =@numUserCntID AND 
	DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 31 AND 60	
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numSixtyDaysPaid = X.numSixtyDaysPaid
FROM    ( SELECT  SUM(AmountPaid) AS numSixtyDaysPaid,
        numDivisionId
FROM    [TempARRecord]
WHERE   numUserCntID =@numUserCntID AND 
	DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 31 AND 60	
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intSixtyDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 31 AND 60
		AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                                                AND numCurrencyID <>@baseCurrency )
------------------------------------------
UPDATE  TempARRecord1
SET     numNinetyDays = X.numNinetyDays
FROM    (SELECT  SUM(DealAmount) AS numNinetyDays,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 61 AND 90
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numNinetyDaysPaid = X.numNinetyDaysPaid
FROM    (SELECT  SUM(AmountPaid) AS numNinetyDaysPaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 61 AND 90
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intNinetyDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 61 AND 90
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                                                AND numCurrencyID <>@baseCurrency )
------------------------------------------
UPDATE  TempARRecord1
SET     numOverNinetyDays = X.numOverNinetyDays
FROM    (SELECT  SUM(DealAmount) AS numOverNinetyDays,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) > 90
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numOverNinetyDaysPaid = X.numOverNinetyDaysPaid
FROM    (SELECT  SUM(AmountPaid) AS numOverNinetyDaysPaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) > 90
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intOverNinetyDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND [dtDueDate] >= DATEADD(DAY, 91,
                                                    dbo.GetUTCDateWithoutTime())
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                                                AND numCurrencyID <>@baseCurrency )

-----------------------Above ThirtyDays to OverNinetyDays is not being Used Now----------------

------------------------------------------
UPDATE  TempARRecord1
SET     numThirtyDaysOverDue = X.numThirtyDaysOverDue
FROM    (SELECT  SUM(DealAmount) AS numThirtyDaysOverDue,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 1 AND 30
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numThirtyDaysOverDuePaid = X.numThirtyDaysOverDuePaid
FROM    (SELECT  SUM(AmountPaid) AS numThirtyDaysOverDuePaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 1 AND 30
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intThirtyDaysOverDueCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 1 AND 30
                                                AND numCurrencyID <>@baseCurrency )

----------------------------------
UPDATE  TempARRecord1
SET     numSixtyDaysOverDue = X.numSixtyDaysOverDue
FROM    ( SELECT    SUM(DealAmount) AS numSixtyDaysOverDue,
                    numDivisionId
          FROM      TempARRecord
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                    AND DATEDIFF(DAY,
                                 [dtDueDate],
                                 dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                              AND     60
          GROUP BY  numDivisionId
        ) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID

UPDATE  TempARRecord1
SET     numSixtyDaysOverDuePaid = X.numSixtyDaysOverDuePaid
FROM    ( SELECT    SUM(AmountPaid) AS numSixtyDaysOverDuePaid,
                    numDivisionId
          FROM      TempARRecord
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                    AND DATEDIFF(DAY,
                                 [dtDueDate],
                                 dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                              AND     60
          GROUP BY  numDivisionId
        ) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intSixtyDaysOverDueCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                    AND DATEDIFF(DAY,
                                 [dtDueDate],
                                 dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                              AND     60
                                                AND numCurrencyID <>@baseCurrency )

------------------------------------------
UPDATE  TempARRecord1
SET     numNinetyDaysOverDue = X.numNinetyDaysOverDue
FROM    (SELECT  SUM(DealAmount) AS numNinetyDaysOverDue,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numNinetyDaysOverDuePaid = X.numNinetyDaysOverDuePaid
FROM    (SELECT  SUM(AmountPaid) AS numNinetyDaysOverDuePaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intNinetyDaysOverDueCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90
                                                AND numCurrencyID <>@baseCurrency )
------------------------------------------
UPDATE  TempARRecord1
SET     numOverNinetyDaysOverDue = X.numOverNinetyDaysOverDue
FROM    (SELECT  SUM(DealAmount) AS numOverNinetyDaysOverDue,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY, [dtDueDate], dbo.GetUTCDateWithoutTime()) > 90
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numOverNinetyDaysOverDuePaid = X.numOverNinetyDaysOverDuePaid
FROM    (SELECT  SUM(AmountPaid) AS numOverNinetyDaysOverDuePaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY, [dtDueDate], dbo.GetUTCDateWithoutTime()) > 90
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intOverNinetyDaysOverDueCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY, [dtDueDate], dbo.GetUTCDateWithoutTime()) > 90
                                                AND numCurrencyID <>@baseCurrency )


UPDATE  TempARRecord1
SET     monUnAppliedAmount = ISNULL(X.monUnAppliedAmount,0)
FROM    (SELECT  SUM(ISNULL(monUnAppliedAmount,0)) AS monUnAppliedAmount,numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID 
GROUP BY numDivisionId
) X WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET numTotal=  isnull(numCurrentDays,0) + isnull(numThirtyDaysOverDue,0)
      + isnull(numSixtyDaysOverDue,0)
      + isnull(numNinetyDaysOverDue,0)
      --+ isnull(numOverNinetyDays,0)
      + isnull(numOverNinetyDaysOverDue,0) /*- ISNULL(monUnAppliedAmount,0) */
           
SELECT  
		[numDivisionId],
		[numContactId],
		[tintCRMType],
		[vcCompanyName],
		[vcCustPhone],
		[vcContactName],
		[vcEmail],
		[numPhone],
		[numCurrentDays],
		[numThirtyDays],
		[numSixtyDays],
		[numNinetyDays],
		[numOverNinetyDays],
		[numThirtyDaysOverDue],
		[numSixtyDaysOverDue],
		[numNinetyDaysOverDue],
		[numOverNinetyDaysOverDue],
--		CASE WHEN (ISNULL(numTotal,0) - ISNULL(monUnAppliedAmount,0))>=0
--		 THEN ISNULL(numTotal,0) - ISNULL(monUnAppliedAmount,0) ELSE ISNULL(numTotal,0) END AS numTotal ,
		ISNULL(numTotal,0) AS numTotal,
		numCompanyID,
		numDomainID,
		[intCurrentDaysCount],
		[intThirtyDaysCount],
		[intThirtyDaysOverDueCount],
		[intSixtyDaysCount],
		[intSixtyDaysOverDueCount],
		[intNinetyDaysCount],
		[intOverNinetyDaysCount],
		[intNinetyDaysOverDueCount],
		[intOverNinetyDaysOverDueCount],
		[numCurrentDaysPaid],
		[numThirtyDaysPaid],
		[numSixtyDaysPaid],
		[numNinetyDaysPaid],
		[numOverNinetyDaysPaid],
		[numThirtyDaysOverDuePaid],
		[numSixtyDaysOverDuePaid],
		[numNinetyDaysOverDuePaid],
		[numOverNinetyDaysOverDuePaid],ISNULL(monUnAppliedAmount,0) AS monUnAppliedAmount
FROM    TempARRecord1 WHERE numUserCntID =@numUserCntID AND ISNULL(numTotal,0)!=0
ORDER BY [vcCompanyName] 
DELETE  FROM TempARRecord1 WHERE   numUserCntID = @numUserCntID ;
DELETE  FROM TempARRecord WHERE   numUserCntID = @numUserCntID ;

END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAccountReceivableAging_Details]    Script Date: 05/24/2010 03:48:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAccountReceivableAging_Details')
DROP PROCEDURE USP_GetAccountReceivableAging_Details
GO
CREATE PROCEDURE [dbo].[USP_GetAccountReceivableAging_Details]
(
    @numDomainId AS NUMERIC(9)  = 0,
    @numDivisionID NUMERIC(9),
    @vcFlag VARCHAR(20),
	@numAccountClass AS NUMERIC(9)=0,
	@dtFromDate AS DATE = NULL,
	@dtToDate AS DATE = NULL,
	@ClientTimeZoneOffset INT = 0
)
AS
  BEGIN
	DECLARE @strSql VARCHAR(8000);
	Declare @strSql1 varchar(8000);
	Declare @strCredit varchar(8000); SET @strCredit=''
	
    DECLARE  @AuthoritativeSalesBizDocId  AS INTEGER
    SELECT @AuthoritativeSalesBizDocId = isnull(numAuthoritativeSales,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId = @numDomainId;--287
    ------------------------------------------      
    DECLARE @baseCurrency AS NUMERIC
	SELECT @baseCurrency=numCurrencyID FROM dbo.Domain WHERE numDomainId=@numDomainId
	/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */
	------------------------------------------      
    IF @dtFromDate IS NULL
		SET @dtFromDate = '1990-01-01 00:00:00.000'
	IF @dtToDate IS NULL
		SET @dtToDate = GETUTCDATE()
    --Get Master data
	 
	 SET @strSql = '
     SELECT DISTINCT OM.[numOppId],
					 -1 AS numJournal_Id,
					 -1 as numDepositId, -1 as tintDepositePage,-1 as numReturnHeaderID,
                        OM.[vcPOppName],
                        OB.[numOppBizDocsId],
                        OB.[vcBizDocID],
                        isnull(OB.monDealAmount
                                 ,0) TotalAmount,--OM.[monPAmount]
                        (ISNULL(TablePayments.monPaidAmount,0)
                           ) AmountPaid,
                        ISNULL(OB.monDealAmount * ISNULL(OM.fltExchangeRate, 1), 0) - ISNULL(ISNULL(TablePayments.monPaidAmount,0) * ISNULL(OM.fltExchangeRate, 1),0) BalanceDue,
                        CASE ISNULL(bitBillingTerms,0) 
                          --WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0)),OB.dtFromDate),
                          WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate),
                                                                   '+ CONVERT(VARCHAR(15),@numDomainId) +')
                          WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],'+ CONVERT(VARCHAR(15),@numDomainId) +')
                        END AS DueDate,
                        bitBillingTerms,
                        intBillingDays,
                        OB.dtCreatedDate,
                        ISNULL(C.[varCurrSymbol],'''') [varCurrSymbol],
                        CASE WHEN '+ CONVERT(VARCHAR(15),@baseCurrency) +' <> OM.numCurrencyID THEN 2 ELSE 1 END AS CurrencyCount,
                        OB.vcRefOrderNo ,OM.[numDivisionId]
        FROM   [OpportunityMaster] OM INNER JOIN [DivisionMaster] DM ON OM.[numDivisionId] = DM.[numDivisionID]
               LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN [Currency] C ON OM.[numCurrencyID] = C.[numCurrencyID]
			   OUTER APPLY
			   (
					SELECT 
						SUM(monAmountPaid) AS monPaidAmount
					FROM
						DepositeDetails DD
					INNER JOIN
						DepositMaster DM
					ON
						DD.numDepositID=DM.numDepositId
					WHERE 
						numOppID = OM.numOppID
						AND numOppBizDocsID=OB.numOppBizDocsID
						AND CONVERT(DATE,DM.dtDepositDate) <= ''' + CAST(@dtToDate AS VARCHAr) + '''
				) TablePayments
        WHERE  OM.[tintOppType] = 1
               AND OM.[tintOppStatus] = 1
               AND om.[numDomainId] = '+ CONVERT(VARCHAR(15),@numDomainId) +'
               AND OB.[numBizDocId] = '+ CONVERT(VARCHAR(15),ISNULL(@AuthoritativeSalesBizDocId,0)) +'
               AND OB.bitAuthoritativeBizDocs=1
               AND ISNULL(OB.tintDeferred,0) <> 1
               AND (DM.[numDivisionID] = '+ CONVERT(VARCHAR(15),@numDivisionID) + ' OR ' + CAST(@numDivisionID AS VARCHAR) + '=0)
			   AND (OM.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
			   AND CONVERT(DATE,DATEADD(DAY,CASE WHEN OM.bitBillingTerms = 1 
								 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(OM.intBillingDays,0)), 0))
								 ELSE 0 
							END, dtFromDate)) <= ''' + CAST(@dtToDate AS VARCHAr) + '''
               AND isnull(OB.monDealAmount
                            ,0) > 0  AND isnull(OB.monDealAmount * OM.fltExchangeRate
                        - ISNULL(TablePayments.monPaidAmount,0) * OM.fltExchangeRate,0) != 0'
    
    	--Show Impact of AR journal entries in report as well 
    	
SET @strSql1 = '    	
		UNION 
		 SELECT 
				-1 AS [numOppId],
				GJH.numJournal_Id AS numJournal_Id,
				-1 as numDepositId, -1 as tintDepositePage,-1 as numReturnHeaderID,
				''Journal'' [vcPOppName],
				0 [numOppBizDocsId],
				'''' [vcBizDocID],
				CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END TotalAmount,
				0 AmountPaid,
				0 BalanceDue,
				[dbo].[FormatedDateFromDate](GJH.datEntry_Date,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate,
				0 bitBillingTerms,
				0 intBillingDays,
				GJH.datCreatedDate,
				ISNULL(C.[varCurrSymbol], '''') [varCurrSymbol],
				1 AS CurrencyCount,'''' as vcRefOrderNo,0 as numDivisionId
		 FROM   dbo.General_Journal_Header GJH
				INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
															  AND GJH.numDomainId = GJD.numDomainId
				INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
				LEFT OUTER JOIN [Currency] C ON GJD.[numCurrencyID] = C.[numCurrencyID]
		 WHERE  GJH.numDomainId = '+ CONVERT(VARCHAR(15),@numDomainId) +'
				and GJD.numCustomerID =' + CONVERT(VARCHAR(15),@numDivisionID) +'
				AND COA.vcAccountCode LIKE ''01010105%'' 
				AND ISNULL(numOppId,0)=0 AND ISNULL(numOppBizDocsId,0)=0 
				AND ISNULL(GJH.numDepositID,0)=0 AND ISNULL(GJH.numReturnID,0)=0
				AND (GJH.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
				AND CONVERT(DATE,GJH.[datEntry_Date]) <= ''' + CAST(@dtToDate AS VARCHAr) + ''''				
    
   SET @strCredit = ' UNION SELECT -1 AS [numOppId],
								-1 AS numJournal_Id,
								numDepositId,tintDepositePage,isnull(numReturnHeaderID,0) as numReturnHeaderID,
                      			CASE WHEN tintDepositePage=2 THEN ''Unapplied Payment'' ELSE ''Credit Memo'' END AS [vcPOppName],
								0 [numOppBizDocsId],
								'''' [vcBizDocID],
								(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) * -1 AS TotalAmount,
								0 AmountPaid,
								(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) * -1 AS BalanceDue,
								[dbo].[FormatedDateFromDate](DM.dtDepositDate,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate,
								0 bitBillingTerms,
								0 intBillingDays,
								DM.dtCreationDate,
								'''' [varCurrSymbol],
								1 AS CurrencyCount,'''' as vcRefOrderNo,0 as numDivisionId
		 FROM  DepositMaster DM 
		WHERE DM.numDomainId='+ CONVERT(VARCHAR(15),@numDomainId) +' AND tintDepositePage IN(2,3)  
		AND DM.numDivisionId = ' + CONVERT(VARCHAR(15),@numDivisionID) +'
		AND (DM.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
		AND CONVERT(DATE,DM.dtDepositDate)  <= ''' + CAST(@dtToDate AS VARCHAr) + '''
		AND ((ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) > 0)'  
    
    IF (@vcFlag = '0+30')
      BEGIN
      
      SET @strSql =@strSql +
						'AND dateadd(DAY,CASE 
                                 --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                 WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                 ELSE 0
                               END,OB.dtFromDate) BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())
               AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                ELSE 0
                                                              END,OB.dtFromDate)'
                                                              
		SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())'
		
		SET @strCredit =@strCredit + 'AND DM.dtDepositDate BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())'
		
      END
    ELSE
      IF (@vcFlag = '30+60')
        BEGIN
         SET @strSql =@strSql +
                 'AND dateadd(DAY,CASE 
                                   --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                   WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                   ELSE 0
                                 END,OB.dtFromDate) BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())
                 AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                  --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                  WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                  ELSE 0
                                                                END,OB.dtFromDate)'
		SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())'

		SET @strCredit =@strCredit + 'AND DM.dtDepositDate BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())'
        END
      ELSE
        IF (@vcFlag = '60+90')
          BEGIN
          SET @strSql =@strSql +
                   'AND dateadd(DAY,CASE 
                                     --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                     WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                     ELSE 0
                                   END,OB.dtFromDate) BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                   AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                    --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                    WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                    ELSE 0
                                                                  END,OB.dtFromDate)'
			SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
			
			SET @strCredit =@strCredit + 'AND DM.dtDepositDate BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
			
          END
        ELSE
          IF (@vcFlag = '90+')
            BEGIN
             SET @strSql =@strSql +
                     'AND dateadd(DAY,CASE 
                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                       ELSE 0
                                     END,OB.dtFromDate) > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                     AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                      --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                      WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                      ELSE 0
                                                                    END,OB.dtFromDate)'
			SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date  > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
                
			SET @strCredit =@strCredit + 'AND DM.dtDepositDate  > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
                     
            END
          ELSE
            IF (@vcFlag = '0-30')
              BEGIN
               SET @strSql =@strSql +
                       'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                       ELSE 0
                                                                     END,OB.dtFromDate)
                       AND datediff(DAY,dateadd(DAY,CASE 
                                                      --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                      WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                      ELSE 0
                                                    END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
				SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
				
				SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
				
              END
            ELSE
              IF (@vcFlag = '30-60')
                BEGIN
                  SET @strSql =@strSql +
                         'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                         --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                         WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                         ELSE 0
                                                                       END,OB.dtFromDate)
                         AND datediff(DAY,dateadd(DAY,CASE 
                                                        --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                        WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                        ELSE 0
                                                      END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60'
                SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60 '
                
                SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60 '
                
                END
              ELSE
                IF (@vcFlag = '60-90')
                  BEGIN
					SET @strSql =@strSql +
                           'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                           --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                           WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                           ELSE 0
                                                                         END,OB.dtFromDate)
                           AND datediff(DAY,dateadd(DAY,CASE 
                                                          --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                          WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                          ELSE 0
                                                        END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90'
					SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 '
					
					SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 '
					
                           
                  END
                ELSE
                  IF (@vcFlag = '90-')
                    BEGIN
                      SET @strSql =@strSql +
                             'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                             --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                             WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                             ELSE 0
                                                                           END,OB.dtFromDate)
                             AND datediff(DAY,dateadd(DAY,CASE 
                                                            --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                            WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                            ELSE 0
                                                          END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) > 90'
					SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) > 90'
					
					SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) > 90'
                             
                    END
                  ELSE
                    BEGIN
                      SET @strSql =@strSql +''
                    END
                    



	SET @strSql =@strSql + @strSql1 + @strCredit
	PRINT @strSql
	EXEC(@strSql)
  END
/****** Object:  StoredProcedure [dbo].[USP_GetAccountReceivableAging_Invoice]    Script Date: 05/24/2010 03:48:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAccountReceivableAging_Invoice')
DROP PROCEDURE USP_GetAccountReceivableAging_Invoice
GO
CREATE PROCEDURE [dbo].[USP_GetAccountReceivableAging_Invoice]
(
    @numDomainId AS NUMERIC(9) = 0,
	@numDivisionId AS NUMERIC(9) = NULL,
	@vcFlag VARCHAR(20),
	@numAccountClass AS NUMERIC(9)=0,
	@dtFromDate AS DATE = NULL,
	@dtToDate AS DATE = NULL,
	@ClientTimeZoneOffset INT = 0,
	@vcRegularSearch VARCHAR(MAX) = ''
)
AS
BEGIN
	DECLARE @numDefaultARAccount NUMERIC(18,0)
	SET @numDefaultARAccount = ISNULL((SELECT numAccountID FROM AccountingCharges WHERE numDomainID=@numDomainId AND numChargeTypeId=4),0)

    DECLARE  @AuthoritativeSalesBizDocId  AS INTEGER
    SELECT @AuthoritativeSalesBizDocId = isnull(numAuthoritativeSales,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId = @numDomainId;--287
    ------------------------------------------      
    DECLARE @baseCurrency AS NUMERIC
	SELECT @baseCurrency=numCurrencyID FROM dbo.Domain WHERE numDomainId=@numDomainId
	/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */
	------------------------------------------      
	IF @dtFromDate IS NULL
		SET @dtFromDate = '1990-01-01 00:00:00.000'
	IF @dtToDate IS NULL
		SET @dtToDate = GETUTCDATE()

	SET @numDivisionId = ISNULL(@numDivisionId,0)

    DECLARE @strSql VARCHAR(MAX);
	Declare @strSql1 VARCHAR(MAX);
	Declare @strCredit varchar(MAX); SET @strCredit=''
    
    
    SET @strSql = ' SELECT DISTINCT OM.[numOppId],
                        OM.[vcPOppName],
                        OM.[tintOppType],
                        OB.[numOppBizDocsId],
                        OB.[vcBizDocID],
                        isnull(OB.monDealAmount * OM.fltExchangeRate,0) TotalAmount,
                        (ISNULL(TablePayments.monPaidAmount,0) * OM.fltExchangeRate) AmountPaid,
                        isnull(OB.monDealAmount
                                 * OM.fltExchangeRate
                                 - ISNULL(TablePayments.monPaidAmount,0)
                                     * OM.fltExchangeRate,0) BalanceDue,
                        CASE ISNULL(bitBillingTerms,0) 
                          --WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0)),OB.dtFromDate),
                          WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate),
                                                                   '+ CONVERT(VARCHAR(15),@numDomainId) +')
                          WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],'+ CONVERT(VARCHAR(15),@numDomainId) +')
                        END AS DueDate,
                        bitBillingTerms,
                        intBillingDays,
                        OB.dtCreatedDate,
                        ISNULL(C.[varCurrSymbol],'''') [varCurrSymbol],CO.vcCompanyName,ADC.vcFirstname + '' ''+ ADC.vcLastName AS vcContactName,
                           dbo.fn_GetContactName(OM.numRecOwner) AS RecordOwner,
						   dbo.fn_GetContactName(OM.numAssignedTo) AS AssignedTo
						   ,CASE WHEN ADC.numPhone IS NULL THEN '''' ELSE + ADC.numPhone + (CASE WHEN numPhoneExtension IS NULL THEN '''' WHEN numPhoneExtension='''' THEN '''' ELSE '','' + ADC.numPhoneExtension END) END AS vcCustPhone,
						   OM.numCurrencyID,OB.vcRefOrderNo,[dbo].[FormatedDateFromDate](ob.[dtFromDate],'+ CONVERT(VARCHAR(15),@numDomainId) +') AS dtFromDate,
						    -1 AS [numJournal_Id],-1 as numReturnHeaderID,
							CASE WHEN ISNULL(BT.[numDiscount],0) > 0 AND ISNULL(BT.[numInterestPercentage],0) > 0 
								 THEN '+ '''-''+' + 'CONVERT(VARCHAR(10),ISNULL(BT.[numDiscount],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numDiscountPaidInDays],0)) +' + ''' days early / ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentage],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentageIfPaidAfterDays],0)) +' + ''' days or later ''' +
								 ' WHEN ISNULL(BT.[numDiscount],0) > 0 THEN '+ '''-''+' + 'CONVERT(VARCHAR(10),ISNULL(BT.[numDiscount],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numDiscountPaidInDays],0)) +' + ''' days early ''' + 
								 ' WHEN ISNULL(BT.[numInterestPercentage],0) > 0  THEN +CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentage],0))+' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentageIfPaidAfterDays],0))+' + ''' days or later ''' +
								' END AS [vcTerms],
							ISNULL(BT.[numDiscount],0) AS [numDiscount],
							ISNULL(BT.[numInterestPercentage],0) AS [numInterest],
							0 AS tintLevel
							,ISNULL(OB.numARAccountID,'+ CAST(@numDefaultARAccount AS VARCHAR) +') AS numChartAcntId
        INTO #TempRecords                
        FROM   [OpportunityMaster] OM
               INNER JOIN [DivisionMaster] DM
                 ON OM.[numDivisionId] = DM.[numDivisionID]
                  INNER JOIN CompanyInfo CO ON CO.numCompanyId = DM.numCompanyID
                  LEFT JOIN AdditionalContactsInformation ADC ON ADC.numDivisionId=DM.numDivisionID AND ADC.numContactId=OM.numContactId
               LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN [Currency] C ON OM.[numCurrencyID] = C.[numCurrencyID]
			   LEFT JOIN [dbo].[BillingTerms] AS BT ON [OM].[intBillingDays] = [BT].[numTermsID] 
			   OUTER APPLY
			   (
					SELECT 
						SUM(monAmountPaid) AS monPaidAmount
					FROM
						DepositeDetails DD
					INNER JOIN
						DepositMaster DM
					ON
						DD.numDepositID=DM.numDepositId
					WHERE 
						numOppID = OM.numOppID
						AND numOppBizDocsID=OB.numOppBizDocsID
						AND CONVERT(DATE,DM.dtDepositDate) <= ''' + CAST(@dtToDate AS VARCHAr) + '''
				) TablePayments
        WHERE  OM.[tintOppType] = 1
               AND OM.[tintOppStatus] = 1
               AND om.[numDomainId] = '+ CONVERT(VARCHAR(15),@numDomainId) +'
               AND OB.[numBizDocId] = '+ CONVERT(VARCHAR(15),ISNULL(@AuthoritativeSalesBizDocId,0)) +' 
			   AND (OM.numDivisionId = ' + CAST(@numDivisionId AS VARCHAR) + ' OR ' + CAST(@numDivisionId AS VARCHAR) + ' = 0)
				AND (OM.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
				AND CONVERT(DATE,DATEADD(DAY,CASE WHEN OM.bitBillingTerms = 1 
								 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(OM.intBillingDays,0)), 0))
								 ELSE 0 
							END, dtFromDate)) <= ''' + CAST(@dtToDate AS VARCHAr) + '''
               AND OB.bitAuthoritativeBizDocs=1
               AND ISNULL(OB.tintDeferred,0) <> 1
               AND isnull(OB.monDealAmount * OM.fltExchangeRate,0) != 0 '     
       
       SET @strSql1 = '  UNION 
		 SELECT 
				-1 [numOppId],
				''Journal'' [vcPOppName],
				0 [tintOppType],
				0 [numOppBizDocsId],
				''Journal'' [vcBizDocID],
				CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END TotalAmount,
				0 AmountPaid,
				CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END BalanceDue,
				[dbo].[FormatedDateFromDate](GJH.datEntry_Date,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate,
				0 bitBillingTerms,
				0 intBillingDays,
				GJH.datCreatedDate,
                ISNULL(C.[varCurrSymbol], '''') [varCurrSymbol]
                ,CO.vcCompanyName,ADC.vcFirstname + '' ''+ ADC.vcLastName AS vcContactName,
                           '''' AS RecordOwner,
						   '''' AS AssignedTo
						   ,CASE WHEN ADC.numPhone IS NULL THEN '''' ELSE + ADC.numPhone + (CASE WHEN numPhoneExtension IS NULL THEN '''' WHEN numPhoneExtension='''' THEN '''' ELSE '','' + ADC.numPhoneExtension END) END AS vcCustPhone,
						   GJD.numCurrencyID,'''' as vcRefOrderNo,NULL, ISNULL(GJH.numJournal_Id,0) AS [numJournal_Id],-1 as numReturnHeaderID,
							CASE WHEN ISNULL(BT.[numDiscount],0) > 0 AND ISNULL(BT.[numInterestPercentage],0) > 0 
								 THEN '+ '''-''+' + 'CONVERT(VARCHAR(10),ISNULL(BT.[numDiscount],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numDiscountPaidInDays],0)) +' + ''' days early / ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentage],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentageIfPaidAfterDays],0)) +' + ''' days or later ''' +
								 ' WHEN ISNULL(BT.[numDiscount],0) > 0 THEN '+ '''-''+' + 'CONVERT(VARCHAR(10),ISNULL(BT.[numDiscount],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numDiscountPaidInDays],0)) +' + ''' days early ''' + 
								 ' WHEN ISNULL(BT.[numInterestPercentage],0) > 0  THEN +CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentage],0))+' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentageIfPaidAfterDays],0))+' + ''' days or later ''' +
								' END AS [vcTerms],
							ISNULL(BT.[numDiscount],0) AS [numDiscount],
							ISNULL(BT.[numInterestPercentage],0) AS [numInterest],
							0 AS tintLevel,
							GJD.numChartAcntId
		 FROM   dbo.General_Journal_Header GJH
				INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
															  AND GJH.numDomainId = GJD.numDomainId
				INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
				INNER JOIN [DivisionMaster] DM ON GJD.numCustomerID = DM.[numDivisionID]
				INNER JOIN CompanyInfo CO ON CO.numCompanyId = DM.numCompanyID
                Left JOIN AdditionalContactsInformation ADC ON ADC.numContactId=GJD.numContactId 
				LEFT OUTER JOIN [Currency] C ON GJD.[numCurrencyID] = C.[numCurrencyID]
				LEFT JOIN [dbo].[BillingTerms] AS BT ON [DM].[numBillingDays] = [BT].[numTermsID]
		 WHERE  GJH.numDomainId ='+ CONVERT(VARCHAR(15),@numDomainId) +'
				AND COA.vcAccountCode LIKE ''01010105%'' 
				AND ISNULL(numOppId,0)=0 
				AND ISNULL(numOppBizDocsId,0)=0 
				AND ISNULL(GJH.numDepositID,0)=0 
				AND ISNULL(GJH.numReturnID,0)=0
				AND (GJD.numCustomerId = ' + CAST(@numDivisionId AS VARCHAR) + ' OR ' + CAST(@numDivisionId AS VARCHAR) + '  = 0)
				AND (GJH.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
				AND CONVERT(DATE,GJH.[datEntry_Date]) <= ''' + CAST(@dtToDate AS VARCHAr) + ''''
   
	 SET @strCredit = ' UNION SELECT -1 [numOppId],
									CASE WHEN tintDepositePage=2 THEN ''Unapplied Payment'' ELSE ''Credit Memo'' END AS [vcPOppName],
									0 [tintOppType],
									0 [numOppBizDocsId],
									''''  [vcBizDocID],
									(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) * -1 AS TotalAmount,
									0 AmountPaid,
									(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) * -1 AS BalanceDue,
									[dbo].[FormatedDateFromDate](DM.dtDepositDate,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate,
									0 bitBillingTerms,
									0 intBillingDays,
									DM.dtCreationDate AS [datCreatedDate],
									'''' [varCurrSymbol],
									dbo.fn_GetComapnyName(DM.numDivisionId) vcCompanyName,
									'''' vcContactName,
									'''' AS RecordOwner,
									'''' AS AssignedTo,
									'''' AS vcCustPhone,
									ISNULL(DM.numCurrencyID,0) AS [numCurrencyID],
									'''' as vcRefOrderNo,
									NULL, 
									-1 AS [numJournal_Id],isnull(numReturnHeaderID,0) as numReturnHeaderID,
									'''' AS [vcTerms],
									0 AS [numDiscount],
									0 AS [numInterest],
									0 AS tintLeve,
									0 AS numChartAcntId
					FROM  DepositMaster DM 
					WHERE DM.numDomainId='+ CONVERT(VARCHAR(15),@numDomainId) +' AND tintDepositePage IN(2,3)  
					AND ((ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) > 0)
					AND (DM.numDivisionId = ' + CAST(@numDivisionId AS VARCHAR) + ' OR ' + CAST(@numDivisionId AS VARCHAR) + ' = 0)
					AND (DM.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
					AND CONVERT(DATE,DM.dtDepositDate) <= ''' + CAST(@dtToDate AS VARCHAr) + '''
					UNION
					SELECT 
						-1 [numOppId]
						,''Refund'' AS [vcPOppName]
						,0 [tintOppType]
						,0 [numOppBizDocsId]
						,''''  [vcBizDocID]
						,(CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1 * GJD.numCreditAmt END) AS TotalAmount
						,0 AmountPaid
						,(CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1 * GJD.numCreditAmt END) AS BalanceDue
						,[dbo].[FormatedDateFromDate](GJH.datEntry_Date,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate
						,0 bitBillingTerms
						,0 intBillingDays
						,RH.dtCreatedDate AS [datCreatedDate]
						,'''' [varCurrSymbol]
						,dbo.fn_GetComapnyName(DIV.numDivisionId) vcCompanyName
						,'''' vcContactName
						,'''' AS RecordOwner
						,'''' AS AssignedTo
						,'''' AS vcCustPhone
						,ISNULL(DIV.numCurrencyID,0) AS [numCurrencyID]
						,'''' as vcRefOrderNo
						,NULL
						,GJH.numJournal_Id AS [numJournal_Id]
						,isnull(numReturnHeaderID,0) as numReturnHeaderID
						,'''' AS [vcTerms]
						,0 AS [numDiscount]
						,0 AS [numInterest]
						,0 AS tintLevel
						,GJD.numChartAcntId
					 FROM   dbo.ReturnHeader RH JOIN dbo.General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
					 JOIN [dbo].[DivisionMaster] AS DIV ON DIV.[numDivisionID] = RH.[numDivisionId]
					JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId 
					JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId AND COA.vcAccountCode LIKE ''0101010501''
					WHERE 
						RH.tintReturnType=4 
						AND RH.numDomainId=' + CAST(@numDomainId AS VARCHAR) + '  
						AND ISNULL(RH.numParentID,0) = 0
						AND ISNULL(RH.IsUnappliedPayment,0) = 0
						AND (monBizDocAmount > 0) AND ISNULL(RH.numBillPaymentIDRef,0)=0
						AND (RH.numDivisionId = ' + CAST(@numDivisionId AS VARCHAR) + ' OR ' + CAST(@numDivisionId AS VARCHAR) + ' = 0)
						AND (RH.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
						AND CONVERT(DATE,GJH.[datEntry_Date]) <= ''' + CAST(@dtToDate AS VARCHAr) + ''''
	            	
    IF (@vcFlag = '0+30')
      BEGIN
      
      SET @strSql =@strSql +
						'AND dateadd(DAY,CASE 
                                 --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                 WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                 ELSE 0
                               END,OB.dtFromDate) BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())
               AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                ELSE 0
                                                              END,OB.dtFromDate)'
                                                              
		SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())'
		
		SET @strCredit =@strCredit + 'AND DM.dtDepositDate BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())'
      END
    ELSE
      IF (@vcFlag = '30+60')
        BEGIN
         SET @strSql =@strSql +
                 'AND dateadd(DAY,CASE 
                                   --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                   WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                   ELSE 0
                                 END,OB.dtFromDate) BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())
                 AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                  --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                  WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                  ELSE 0
                                                                END,OB.dtFromDate)'
		SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())'
        
        SET @strCredit =@strCredit + 'AND DM.dtDepositDate BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())'
                 
        END
      ELSE
        IF (@vcFlag = '60+90')
          BEGIN
          SET @strSql =@strSql +
                   'AND dateadd(DAY,CASE 
                                     --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                     WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                     ELSE 0
                                   END,OB.dtFromDate) BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                   AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                    --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                    WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                    ELSE 0
                                                                  END,OB.dtFromDate)'
			SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
			
			SET @strCredit =@strCredit + 'AND DM.dtDepositDate BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
			
          END
        ELSE
          IF (@vcFlag = '90+')
            BEGIN
             SET @strSql =@strSql +
                     'AND dateadd(DAY,CASE 
                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                       ELSE 0
                                     END,OB.dtFromDate) > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                     AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                      --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                      WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                      ELSE 0
                                                                    END,OB.dtFromDate)'
			SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date  > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
            
            SET @strCredit =@strCredit + 'AND DM.dtDepositDate  > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
                     
            END
          ELSE
            IF (@vcFlag = '0-30')
              BEGIN
               SET @strSql =@strSql +
                       'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                       ELSE 0
                                                                     END,OB.dtFromDate)
                       AND datediff(DAY,dateadd(DAY,CASE 
                                                      --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                      WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                      ELSE 0
                                                    END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
				SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
				
				SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
				
              END
            ELSE
              IF (@vcFlag = '30-60')
                BEGIN
                  SET @strSql =@strSql +
                         'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                         --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                         WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                         ELSE 0
                                                                       END,OB.dtFromDate)
                         AND datediff(DAY,dateadd(DAY,CASE 
                                                        --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                        WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                        ELSE 0
                                                      END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60'
                SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60 '
                
                SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60 '
                
                END
              ELSE
                IF (@vcFlag = '60-90')
                  BEGIN
					SET @strSql =@strSql +
                           'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                           --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                           WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                           ELSE 0
                                                                         END,OB.dtFromDate)
                           AND datediff(DAY,dateadd(DAY,CASE 
                                                          --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                          WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                          ELSE 0
                                                        END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90'
					SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 '
                    
                    SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 '
                           
                  END
                ELSE
                  IF (@vcFlag = '90-')
                    BEGIN
                      SET @strSql =@strSql +
                             'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                             --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                             WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																															   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                             ELSE 0
                                                                           END,OB.dtFromDate)
                             AND datediff(DAY,dateadd(DAY,CASE 
                                                            --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                            WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                            ELSE 0
                                                          END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) > 90'
					SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) > 90'
                     
                    SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) > 90'        
                    END
     
	        
    SET @strSql =@strSql + @strSql1 +  @strCredit

	

    SET @strSql = CONCAT(@strSql,' SELECT *,CASE WHEN DATEDIFF (day ,[dbo].[FormatedDateFromDate](DueDate,',@numDomainID,') ,GETDATE()) > 0 
										  THEN DATEDIFF (day ,[dbo].[FormatedDateFromDate](DueDate,',@numDomainID,') ,GETDATE()) 
										  ELSE NULL 
									 END AS DaysLate,
									 CASE WHEN DATEDIFF (day ,[dbo].[FormatedDateFromDate](DueDate,',@numDomainID,') ,GETDATE()) > 0 
										  THEN + ''',' Past Due (', ''' + CAST(DATEDIFF (day ,[dbo].[FormatedDateFromDate](DueDate,',@numDomainID,') ,GETDATE()) AS VARCHAR(10))+''',')','''
										  ELSE NULL 
									 END AS [Status(DaysLate)],
									 CASE WHEN ',@baseCurrency,' <> numCurrencyID THEN 2 ELSE 1 END AS CurrencyCount,
									 dbo.fn_GetChart_Of_AccountsName(numChartAcntId) vcARAccount
							FROM #TempRecords WHERE (ISNULL(BalanceDue,0) <> 0 or [numOppId]=-1)')
	
	IF ISNULL(@vcRegularSearch,'') <> ''
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND ',@vcRegularSearch)
	END

   SET @strSql = CONCAT(@strSql,' DROP TABLE #TempRecords')
   
	PRINT CAST(@strSql AS NTEXt)
	EXEC(@strSql)
	        
     
  END
/****** Object:  StoredProcedure [dbo].[usp_GetCompanyInfoDtlPl]    Script Date: 07/26/2008 16:16:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--usp_GetCompanyInfoDtlPl 91099,72,-333
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcompanyinfodtlpl')
DROP PROCEDURE usp_getcompanyinfodtlpl
GO
CREATE PROCEDURE [dbo].[usp_GetCompanyInfoDtlPl]                                                                                                                   
@numDivisionID numeric(9) ,                      
@numDomainID numeric(9) ,        
@ClientTimeZoneOffset as int                              
    --                                                                           
AS                                                                            
BEGIN                                                                            
 SELECT CMP.numCompanyID,               
 CMP.vcCompanyName,                
 (select vcdata from listdetails where numListItemID = DM.numStatusID) as numCompanyStatusName,
 DM.numStatusID as numCompanyStatus,                                                                           
 (select vcdata from listdetails where numListItemID = CMP.numCompanyRating) as numCompanyRatingName, 
 CMP.numCompanyRating,             
 (select vcdata from listdetails where numListItemID = CMP.numCompanyType) as numCompanyTypeName,
 CMP.numCompanyType,              
 CMP.numCompanyType as numCmptype,               
 DM.bitPublicFlag,                 
 DM.numDivisionID,                                   
 DM. vcDivisionName,        
dbo.getCompanyAddress(dm.numdivisionId,1,dm.numDomainID) as Address,
dbo.getCompanyAddress(dm.numdivisionId,2,dm.numDomainID) as ShippingAddress,          
  isnull(AD2.vcStreet,'') as vcShipStreet,
  isnull(AD2.vcCity,'') as vcShipCity,
  isnull(dbo.fn_GetState(AD2.numState),'')  as vcShipState,
  ISNULL((SELECT numShippingZone FROM [State] WHERE (numDomainID=@numDomainID OR ConstFlag=1) AND numStateID=AD2.numState),0) AS numShippingZone,
 (select vcdata from listdetails where numListItemID = DM.numFollowUpStatus) as numFollowUpStatusName,
 DM.numFollowUpStatus,                
 CMP.vcWebLabel1,               
 CMP.vcWebLink1,               
 CMP.vcWebLabel2,               
 CMP.vcWebLink2,                                                                             
 CMP.vcWebLabel3,               
 CMP.vcWebLink3,               
 CMP.vcWeblabel4,               
 CMP.vcWebLink4,                                                                       
 DM.tintBillingTerms,              
 DM.numBillingDays,              
 DM.tintInterestType,              
 DM.fltInterest,                                                  
 isnull(AD2.vcPostalCode,'') as vcShipPostCode, 
 AD2.numCountry vcShipCountry,                          
 DM.bitPublicFlag,              
 (select vcdata from listdetails where numListItemID = DM.numTerID)  as numTerIDName,
 DM.numTerID,                                                                         
 (select vcdata from listdetails where numListItemID = CMP.numCompanyIndustry) as numCompanyIndustryName, 
  CMP.numCompanyIndustry,              
 (select vcdata from listdetails where numListItemID = CMP.numCompanyCredit) as  numCompanyCreditName,
 CMP.numCompanyCredit,                                                                             
 isnull(CMP.txtComments,'') as txtComments,               
 isnull(CMP.vcWebSite,'') vcWebSite,              
 (select vcdata from listdetails where numListItemID = CMP.numAnnualRevID) as  numAnnualRevIDName,
 CMP.numAnnualRevID,                
 (select vcdata from listdetails where numListItemID = CMP.numNoOfEmployeesID) as numNoOfEmployeesIDName,               
CMP.numNoOfEmployeesID,                                                                           
    (select vcdata from listdetails where numListItemID = CMP.vcProfile) as vcProfileName, 
 CMP.vcProfile,              
 DM.numCreatedBy,               
 dbo.fn_GetContactName(DM.numRecOwner) as RecOwner,               
 dbo.fn_GetContactName(DM.numCreatedBy)+' ' + convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintCreatedDate)) as vcCreatedBy,               
 DM.numRecOwner,                
 dbo.fn_GetContactName(DM.numModifiedBy)+' ' +convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintModifiedDate)) as vcModifiedBy,                    
 isnull(DM.vcComPhone,'') as vcComPhone, 
 DM.vcComFax,                   
 DM.numGrpID,  
 (select vcGrpName from Groups where numGrpId= DM.numGrpID) as numGrpIDName,              
 (select vcdata from listdetails where numListItemID = CMP.vcHow) as vcInfoSourceName,
 CMP.vcHow as vcInfoSource,                                                                            
 DM.tintCRMType,                                                                         
 (SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= DM.numCampaignID) as  numCampaignIDName,
 DM.numCampaignID,              
 DM.numAssignedBy, isnull(DM.bitNoTax,0) bitNoTax,                     
 (select A.vcFirstName+' '+A.vcLastName                       
 from AdditionalContactsInformation A                  
 join DivisionMaster D                        
 on D.numDivisionID=A.numDivisionID
 where A.numContactID=DM.numAssignedTo) as numAssignedToName,DM.numAssignedTo,
 (select  count(*) from dbo.GenericDocuments where numRecID= @numDivisionID and vcDocumentSection='A' AND tintDocumentType=2) as DocumentCount ,
 (SELECT count(*)from CompanyAssociations where numDivisionID=@numDivisionID and bitDeleted=0 ) as AssociateCountFrom,                            
 (SELECT count(*)from CompanyAssociations where numAssociateFromDivisionID=@numDivisionID and bitDeleted=0 ) as AssociateCountTo,
 (select vcdata from listdetails where numListItemID = DM.numCompanyDiff) as numCompanyDiffName,               
 DM.numCompanyDiff,DM.vcCompanyDiff,isnull(DM.bitActiveInActive,1) as bitActiveInActive,
(SELECT SUBSTRING(
	(SELECT ','+  A.vcFirstName+' ' +A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN '(' + dbo.fn_GetListItemName(SR.numContactType) + ')' ELSE '' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=DM.numDomainID AND SR.numModuleID=1 AND SR.numRecordID=DM.numDivisionID
				AND UM.numDomainID=DM.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('')),2,200000)) AS numShareWith,
 DM.bitOnCreditHold,
 DM.numDefaultPaymentMethod,
 DM.numAccountClassID,
 DM.tintPriceLevel,DM.vcPartnerCode as vcPartnerCode  ,
  ISNULL(DM.numPartenerSource,0) AS numPartenerSourceId,
 D3.vcPartnerCode+'-'+C3.vcCompanyName as numPartenerSource,
 ISNULL(DM.numPartenerContact,0) AS numPartenerContactId,
A2.vcFirstName+' '+A2.vcLastName AS numPartenerContact,
ISNULL(DM.bitEmailToCase,0) bitEmailToCase,
'' AS vcCreditCards,
----Fetch Last Followup(Template) and EmailSentDate(if any) Value
		  
	dbo.FormatedDateFromDate(DM.dtLastFollowUp,@numDomainID) vcLastFollowup,

		----Fetch Last Followup(Template) Value

		----Fetch Next Followup(Template) and ExecutionDate Value
	(CASE WHEN A2.numECampaignID > 0 THEN dbo.fn_FollowupDetailsInOrgList(A2.numContactID,2,@numDomainID) ELSE '' END) vcNextFollowup,										 

		----Fetch Next Followup(Template) Value
(CASE WHEN (SELECt COUNT(*) FROM ExtarnetAccounts WHERE numDivisionID=DM.numDivisionID) >0 THEN 1 ELSE 0 END) AS bitEcommerceAccess,

ISNULL(DM.numDefaultShippingServiceID,0) numShippingService,
ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = DM.numDefaultShippingServiceID),'') AS vcShippingService,
		CASE 
			WHEN vcSignatureType = 0 THEN 'Service Default'
			WHEN vcSignatureType = 1 THEN 'Adult Signature Required'
			WHEN vcSignatureType = 2 THEN 'Direct Signature'
			WHEN vcSignatureType = 3 THEN 'InDirect Signature'
			WHEN vcSignatureType = 4 THEN 'No Signature Required'
			ELSE ''
		END AS vcSignatureType,
		ISNULL(DM.intShippingCompany,0) intShippingCompany,
		CASE WHEN ISNULL(DM.intShippingCompany,0) = 0 THEN '' ELSE ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 82 AND numListItemID=ISNULL(DM.intShippingCompany,0)),'') END AS vcShipVia,
        A2.vcFirstName AS vcFirstName,
		A2.vcLastName  AS vcLastName,
		A2.vcFirstname + ' ' + A2.vcLastName AS vcGivenName,
		A2.numPhone AS numPhone,
		A2.numPhoneExtension As numPhoneExtension,
		A2.vcEmail As vcEmail	,
 ISNULL(DM.bitAutoCheckCustomerPart,0) AS  bitAutoCheckCustomerPart	    ,
STUFF((SELECT '<br/>' +' <b>Buy</b> '+CASE WHEN u.intType=1 THEN '$'+CAST(u.vcBuyingQty AS VARCHAR(MAX)) ELSE CAST(u.vcBuyingQty AS VARCHAR(MAX)) END+' ' + CASE WHEN u.intType=1 THEN 'Amount' WHEN u.intType=2 THEN 'Quantity' WHEN u.intType=3 THEN 'Lbs' ELSE '' END+' <b>Get</b> ' + (CASE WHEN u.tintDiscountType = 1 THEN CONCAT('$',u.vcIncentives,' off') ELSE CONCAT(u.vcIncentives,'% off') END)
        from PurchaseIncentives u
        where u.numPurchaseIncentiveId = numPurchaseIncentiveId AND numDomainID=@numDomainID AND numDivisionId=@numDivisionID
        order by u.numPurchaseIncentiveId
        for xml path('')),6,6,'') AS Purchaseincentives,
ISNULL(DM.intDropShip,0) as intDropShip,
CASE WHEN ISNULL(DM.intDropShip,0)=1 THEN 'Not Available'
	 WHEN ISNULL(DM.intDropShip,0)=2 THEN 'Blank Available'
	 WHEN ISNULL(DM.intDropShip,0)=3 THEN 'Vendor Label'
	 WHEN ISNULL(DM.intDropShip,0)=4 THEN 'Private Label'
	 ELSE 'Select One'
END AS vcFropShip
		                       
 FROM  CompanyInfo CMP    
  join DivisionMaster DM    
  on DM.numCompanyID=CMP.numCompanyID                                                                                                                                        
   LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
   AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
   LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
   AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
    LEFT JOIN divisionMaster D3 ON DM.numPartenerSource = D3.numDivisionID
  LEFT JOIN CompanyInfo C3 ON C3.numCompanyID = D3.numCompanyID    
  LEft join   AdditionalContactsInformation A2 on   DM.numDivisionID = A2.numDivisionId and A2.bitPrimaryContact = 1
  LEFT JOIN DivisionMasterShippingConfiguration ON DM.numDivisionID=DivisionMasterShippingConfiguration.numDivisionID
 where DM.numDivisionID=@numDivisionID   and DM.numDomainID=@numDomainID                                                                                          
END

GO




/****** Object:  StoredProcedure [dbo].[Usp_getCorrespondance]    Script Date: 07/26/2008 16:17:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcorrespondance')
DROP PROCEDURE usp_getcorrespondance
GO
CREATE PROCEDURE [dbo].[Usp_getCorrespondance]                          
@FromDate as datetime,                          
@toDate as datetime,                          
@numContactId as numeric,                          
@vcMessageFrom as varchar(200)='' ,                        
@tintSortOrder as numeric,                        
@SeachKeyword as varchar(100),                        
@CurrentPage as numeric,                        
@PageSize as numeric,                        
@TotRecs as numeric output,                        
@columnName as varchar(100),                        
@columnSortOrder as varchar(50),                        
@filter as numeric,                        
@numdivisionId as numeric =0,              
@numDomainID as numeric(9)=0,              
@numCaseID as numeric(9)=0,              
@ClientTimeZoneOffset INT,
@numOpenRecordID NUMERIC=0,
@tintMode TINYINT=0,
@bitOpenCommu BIT=0
as 

set @vcMessageFrom=replace(@vcMessageFrom,'''','|')
                         
if @filter>0 set @tintSortOrder=  @filter
               
if ((@filter <=2 and @tintSortOrder<=2) or (@tintSortOrder>2 and @filter>2))                 
begin  
                                                                      
 declare @strSql as varchar(MAX)       
                                  
  declare @firstRec as integer                                                        
  declare @lastRec as integer                                                        
  set @firstRec= (@CurrentPage-1) * @PageSize                                                        
  set @lastRec= (@CurrentPage*@PageSize+1)                 
  set @strSql=''                
 if (@filter <=2 and @tintSortOrder<=2 and @bitOpenCommu=0)                 
 begin     
 declare @strCondition as varchar(MAX);
 declare @vcContactEmail as varchar(MAX);
              
  if @numdivisionId =0 and  @vcMessageFrom <> '' 
 BEGIN 
	If @filter = 1 --ReceivedMessages
		set  @strCondition=' and (vcFrom like ''%'+ REPLACE(@vcMessageFrom,'''','''''')+'%'')  ' 
	ELSE IF @filter=2 --SentMessages
		set  @strCondition=' and (vcTo like ''%'+ REPLACE(@vcMessageFrom,'''','''''')+'%'')  ' 
	ELSE
		set  @strCondition=' and (vcFrom like ''%'+ REPLACE(@vcMessageFrom,'''','''''')+'%'' or vcTo like ''%'+ REPLACE(@vcMessageFrom,'''','''''')+'%'')  ' 
 END
  else if @numdivisionId <>0 
 BEGIN

 /* This approach does not work as numEmailId stores who sent email, id of sender
  set @strCondition=' and HDR.numEmailID in (
  SELECT numEmailID FROM dbo.EmailMaster WHERE numDomainID= ' + CONVERT(VARCHAR(15),@numDomainID) +' AND numContactID 
  IN (SELECT numcontactid FROM dbo.AdditionalContactsInformation WHERE numDivisionID = ' + convert(varchar(15),@numDivisionID) + ' AND numDomainID = ' + CONVERT(VARCHAR(15),@numDomainID) +' ))';
*/
-- bug fix id 1591,1628 
  DECLARE @TnumContactID NUMERIC(9);set @strCondition=''

  select TOP 1 @TnumContactID=numContactID,@vcContactEmail=replace(vcEmail,'''','''''') from AdditionalContactsInformation where numdivisionid=@numdivisionId and len(vcEmail)>2
  ORDER BY numContactID ASC
  
  WHILE @TnumContactID>0
  BEGIN
   if len(@strCondition)>0
    SET @strCondition=@strCondition + ' or '

	If @filter = 1 --ReceivedMessages
		set  @strCondition = @strCondition + ' (vcFrom like ''%'+ REPLACE(@vcContactEmail,'''','''''') +'%'')  ' 
	ELSE IF @filter=2 --SentMessages
		set  @strCondition= @strCondition + ' (vcTo like ''%'+ REPLACE(@vcContactEmail,'''','''''') +'%'')  ' 
	ELSE
		set  @strCondition= @strCondition + ' (vcFrom like ''%'+ REPLACE(@vcContactEmail,'''','''''')+'%'' or vcTo like ''%'+ REPLACE(@vcContactEmail,'''','''''')+'%'')  '

   select TOP 1 @TnumContactID=numContactID,@vcContactEmail=vcEmail from AdditionalContactsInformation where numdivisionid=@numdivisionId and len(vcEmail)>2 and numContactID>@TnumContactID
   ORDER BY numContactID ASC

   IF @@rowcount = 0 SET @TnumContactID = 0 
  END
 
  if len(@strCondition)>0
   SET @strCondition= 'and (' + @strCondition + ')'
  else if @tintMode <> 6 AND @tintMode <> 5
   SET @strCondition=' and (1=0)' 
  --set @strCondition=' and DTL.numEmailId  in (select numEmailId from EmailMaster where vcEmailID in (select vcEmail  from AdditionalContactsInformation where numdivisionid='''+ convert(varchar(15),@numdivisionId)+''' ))    '          
  END
  else if @numContactId>0 
 BEGIN
  select @vcContactEmail=isnull(replace(vcEmail,'''',''''''),'') from AdditionalContactsInformation where numContactID=@numContactId and len(vcEmail)>2

  if len(@vcContactEmail)>0
	If @filter = 1 --ReceivedMessages
		set  @strCondition=' and (vcFrom like ''%'+ REPLACE(@vcContactEmail,'''','''''') +'%'')  ' 
	ELSE IF @filter=2 --SentMessages
		set  @strCondition=' and (vcTo like ''%'+ REPLACE(@vcContactEmail,'''','''''')+'%'')  ' 
	ELSE
		set  @strCondition=' and (vcFrom like ''%'+ REPLACE(@vcContactEmail,'''','''''')+'%'' or vcTo like ''%'+ REPLACE(@vcContactEmail,'''','''''')+'%'')' 
  else if @tintMode <> 6 AND @tintMode <> 5
   SET @strCondition=' and (1=0)'
 END
 else 
  SET @strCondition=''
    PRINT @vcContactEmail
  set @strSql= '                        
  SELECT distinct(HDR.numEmailHstrID),convert(varchar(15),HDR.numEmailHstrID)+''~1~0''+convert(varchar(15),HDR.numUserCntId) as DelData,HDR.dtReceivedOn AS bintCreatedOn,
  dbo.FormatedDateTimeFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtReceivedOn),'+convert(varchar(15),@numDomainID)+') as [date],                
  vcSubject as [Subject],                          
  CASE WHEN vcFrom like ''%'+CAST(REPLACE(@vcContactEmail,'''','''''') AS VARCHAR(300))+'%'' THEN ''Email Out'' ELSE ''Email In'' END as [type] ,                          
  '''' as phone,'''' as assignedto ,                          
  ''0'' as caseid,                             
  null as vcCasenumber,                                 
  ''0''as caseTimeid,                                
  ''0''as caseExpid ,                          
  hdr.tinttype ,HDR.dtReceivedOn as dtCreatedDate,vcFrom + '','' + vcTo as [From] ,0 as bitClosedflag,ISNULL(HDR.bitHasAttachments,0) bitHasAttachments,
 CASE WHEN LEN(Cast(vcBodyText AS VARCHAR(1000)))>150 THEN SUBSTRING(vcBodyText,0,150) + ''...'' ELSE
   Cast(vcBodyText AS VARCHAR(1000)) END AS vcBody          ,
 ''''  AS InlineEdit,
HDR.numUserCntId AS numCreatedBy,
0 AS numOrgTerId,
  CASE WHEN vcFrom like ''%'+CAST(REPLACE(@vcContactEmail,'''','''''') AS VARCHAR(300))+'%'' THEN ''redColorLabel'' ELSE ''greenColorLabel'' END as TypeClass
  from EmailHistory HDR                                 
  LEFT JOIN [Correspondence] CR ON CR.numEmailHistoryID = HDR.numEmailHstrID '        
  
   set  @strSql=@strSql +' where ISNULL(HDR.bitInvisible,0) = 0 AND HDR.numDomainID='+convert(varchar(15),@numDomainID)
   + ' AND (CR.numOpenRecordID = '+ convert(varchar(15),@numOpenRecordID) +' OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
   IF(@FromDate IS NOT NULL AND @FromDate<>'')
   BEGIN
  SET @strSql = @strSql +' and  (dtReceivedOn between '''+ convert(varchar(30),@FromDate)+''' and '''+ convert(varchar(30),@ToDate)+''') '
  END
 set  @strSql=@strSql+ @strCondition
--  if @numdivisionId =0 and  @vcMessageFrom <> '' set  @strSql=@strSql+ 'and (vcFrom like ''%'+ @vcMessageFrom+'%'' or vcTo like ''%'+ @vcMessageFrom+'%'')  '                      
--  else if @numdivisionId <>0 
-- BEGIN
--  set @strSql=@strSql+ 'and DTL.numEmailId  in (select numEmailId from EmailMaster where vcEmailID in (select vcEmail  from AdditionalContactsInformation where numdivisionid='''+ convert(varchar(15),@numdivisionId)+''' ))    '          
--  END
--  else if @numContactId>0 
-- BEGIN
--  declare @vcContactEmail as varchar(100);
--  select @vcContactEmail=vcEmail from AdditionalContactsInformation where numContactID=@numContactId
--
--  set  @strSql=@strSql+ 'and (vcFrom like ''%'+ @vcContactEmail+'%'' or vcTo like ''%'+ @vcContactEmail+'%'')    ' 
-- END

  IF @tintMode=1 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(1,3) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=2 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(2,4) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=5 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(5) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=6 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(6) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  
  if @filter =0 set  @strSql=@strSql+ ' and (hdr.tinttype=1 or hdr.tinttype=2 or  hdr.tinttype=5)'                  
  else if @filter =1 OR @filter =2 set  @strSql=@strSql+ ' and (hdr.tinttype=1 OR hdr.tinttype=2)'                                             
  if @filter > 2 set  @strSql=@strSql+ ' and hdr.tinttype=0'                 
                        
  if @tintSortOrder =0 and @SeachKeyword <> ''                 
  begin                
   set  @strSql=@strSql+ ' and (vcFrom like ''%'+ @SeachKeyword+'%'' or vcTo like ''%'+ @SeachKeyword+'%'' or vcsubject like ''%'+@SeachKeyword+'%'' or vcBody like ''%'+@SeachKeyword+'%'')'               
  end                
  if @tintSortOrder =1 and @SeachKeyword <> ''                 
  begin                
     set  @strSql=@strSql+ ' and (vcFrom like ''%'+ @SeachKeyword+'%'' or vcsubject like ''%'+@SeachKeyword+'%'' or  vcBody like ''%'+@SeachKeyword+'%'')'                 
  end                
  if @tintSortOrder =2 and @SeachKeyword <> ''                 
  begin                
     set  @strSql=@strSql+ ' and (vcTo like ''%'+ @SeachKeyword+'%'' or vcsubject like ''%'+@SeachKeyword+'%'' or  vcBody like ''%'+@SeachKeyword+'%'')'                 
  end                            
 end                
 if (@filter =0 and @tintSortOrder=0 and @bitOpenCommu=0) set  @strSql=@strSql+ ' union '                
                
 if ((@filter=0 or @filter>2) and (@tintSortOrder=0 or @tintSortOrder>2))                
 begin                
  set  @strSql=@strSql+ '                     
  SELECT c.numCommId as numEmailHstrID,convert(varchar(15),C.numCommId)+''~2~''+convert(varchar(15),C.numCreatedBy) as DelData,CASE WHEN c.bitTask = 973 THEN dtCreatedDate WHEN c.bitTask = 974 THEN dtCreatedDate WHEN c.bitTask = 971 THEN dtStartTime ELSE dtEndTime END AS bintCreatedOn,
  case when c.bitTask=973 then dbo.FormatedDateTimeFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtCreatedDate),'+convert(varchar(15),@numDomainID)+') 
  when c.bitTask=974 then dbo.FormatedDateTimeFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtEndTime),'+convert(varchar(15),@numDomainID)+') 
  when c.bitTask=971 THEN dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtEndTime),'+convert(varchar(15),@numDomainID)+') + '' '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtStartTime]),100),7)  +'' - '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtEndTime]),100),7)
  else  dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtEndTime),'+convert(varchar(15),@numDomainID)+') + '' '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtStartTime]),100),7)  +'' - '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtEndTime]),100),7)  END AS [date],
  convert(varchar(8000),textdetails) as [Subject],                                                                         
 dbo.fn_GetListItemName(c.bitTask)  AS [Type],                                                                 
  A1.vcFirstName +'' ''+ A1.vcLastName +'' ''+                                      
  case when A1.numPhone<>'''' then + A1.numPhone +case when A1.numPhoneExtension<>'''' then '' - '' + A1.numPhoneExtension else '''' end  else '''' end as [Phone],                        
  A2.vcFirstName+ '' ''+A2.vcLastName  as assignedto,                         
  c.caseid,                           
  (select top 1 vcCaseNumber from cases where cases.numcaseid= c.caseid )as vcCasenumber,                               
  isnull(caseTimeid,0)as caseTimeid,                              
  isnull(caseExpid,0)as caseExpid  ,                        
  1 as tinttype ,dtCreatedDate,'''' as [FROM]     ,isnull(C.bitClosedflag,0) as bitClosedflag,0 as bitHasAttachments,
'''' as vcBody,
''id="Tickler~184~False~''+CAST(c.bitTask AS VARCHAR)+''~''+CAST(c.numCommId AS VARCHAR)+''" class="editable_textarea" onmousemove="bgColor=''''lightgoldenRodYellow''''"  title="Double click to edit..." bgcolor="lightgoldenRodYellow" '' AS InlineEdit,
C.numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
''blueColorLabel'' as TypeClass
  from  Communication C                  
  JOIN AdditionalContactsInformation  A1                                           
  ON c.numContactId = A1.numContactId
  LEFT JOIN CommunicationLinkedOrganization clo ON C.numCommID=clo.numCommID                                                                             
  left join AdditionalContactsInformation A2 on A2.numContactID=c.numAssign
  left join UserMaster UM on A2.numContactid=UM.numUserDetailId and isnull(UM.bitActivateFlag,0)=1                                                                           
  left join listdetails on numActivity=numListID                           
  LEFT JOIN [Correspondence] CR ON CR.numCommID = C.numCommID
  JOIN DivisionMaster Div                                                                         
 ON A1.numDivisionId = Div.numDivisionID    
  where  C.numDomainID=' +convert(varchar(15),@numDomainID)                       
  + 'AND (numOpenRecordID = '+ convert(varchar(15),@numOpenRecordID) +' OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  if (@numdivisionId =0 and @numContactId<>0) set  @strSql=@strSql+' and C.numContactId='+ convert(varchar(15),@numContactId)                        
  if @numdivisionId <> 0 set  @strSql=@strSql+' and  (C.numDivisionId='+ convert(varchar(15),@numdivisionId) + ' OR clo.numDivisionID='+ convert(varchar(15),@numdivisionId) + ') '             
  if @numCaseID>0  set  @strSql=@strSql+' and  C.CaseId='+ convert(varchar(15),@numCaseID)                       
  if @filter =0  
  BEGIN
	IF @tintMode <> 9
	BEGIN
		PRINT @tintMode
		IF(@FromDate IS NOT NULL AND @FromDate<>'')
		BEGIN
			SET @strSql=@strSql+ ' and dtStartTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtStartTime  <= '''+ convert(varchar(30),@ToDate)  +''''
		END
	END
  END
  else
    SET @strSql=@strSql+ ' and c.bitTask=' + CONVERT(varchar(12),@filter)  
		IF(@FromDate IS NOT NULL AND @FromDate<>'')
		BEGIN
			SET @strSql=@strSql+ ' and ((dtStartTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtStartTime  <= '''+ convert(varchar(30),@ToDate)  +''') OR (dtEndTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtEndTime  <= '''+ convert(varchar(30),@ToDate)  +'''))'
		END
  IF @tintMode=1 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(1,3) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=2 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(2,4) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=5 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(5) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=6 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(6) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
                          
  IF @tintSortOrder =0 and @SeachKeyword <>'' 
	SET @strSql=@strSql+ ' and textdetails like ''%'+@SeachKeyword+'%'''
  ELSE
	IF ISNULL(@filter,0) = 0 
	BEGIN
		SET @strSql=@strSql+ ' and textdetails like ''%'+@SeachKeyword+'%'''
	END
	ELSE
	BEGIN
		SET @strSql=@strSql+ ' and c.bitTask=' + CONVERT(varchar(12),@filter) + ' and textdetails like ''%'+@SeachKeyword+'%'''                     
	END
  IF @bitOpenCommu=1  SET @strSql=@strSql+ ' and isnull(C.bitClosedflag,0)=0'
 end                
 set @strSql='With tblCorr as (SELECT ROW_NUMBER() OVER (ORDER BY '+@columnName+' '+ @columnSortOrder+') AS RowNumber,X.* from('+@strSql+')X)'                

 set @strSql=@strSql +'                       
  select  *  from tblCorr
  Where RowNumber > '+convert(varchar(10),@firstRec)+' and RowNumber < '+convert(varchar(10),@lastRec)+'                     
  union select 0,count(*),null,null,null,'','',null,null,null,null,null,null,null,null,null,null,0,0,null,'''',0,0,'''' from tblCorr order by RowNumber'                 
                
-- set @strSql=@strSql +'                       
--  select  *,CASE WHEN [Type]=''Email'' THEN dbo.GetEmaillAdd(numEmailHstrID,4)+'', '' +dbo.GetEmaillAdd(numEmailHstrID,1) ELSE '''' END as [From] from tblCorr
--  Where RowNumber > '+convert(varchar(10),@firstRec)+' and RowNumber < '+convert(varchar(10),@lastRec)+'                     
--  union select 0,count(*),null,null,null,'','',null,null,null,null,null,null,null,null,null,null from tblCorr order by RowNumber'                 
                
 print @strSql                
 exec (@strSql)                
end                
else                
select 0,0
/****** Object:  StoredProcedure [dbo].[usp_getCustomFormFields]    Script Date: 07/26/2008 16:17:23 ******/
SET ANSI_NULLS ON
--created by anoop jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdomaindetails')
DROP PROCEDURE usp_getdomaindetails
GO
CREATE PROCEDURE [dbo].[USP_GetDomainDetails]                                          
@numDomainID as numeric(9)=0                                          
as           

DECLARE @listIds VARCHAR(MAX)
SELECT @listIds = COALESCE(@listIds+',' ,'') + CAST(numUserID AS VARCHAR(100)) FROM UnitPriceApprover WHERE numDomainID = @numDomainID
   
DECLARE @canChangeDiferredIncomeSelection AS BIT = 1

IF EXISTS(SELECT 
			OB.numOppBizDocsId 
		FROM 
			OpportunityBizDocs OB 
		INNER JOIN 
			OpportunityMaster OM 
		ON 
			OB.numOppId=OM.numOppId 
		WHERE 
			numBizDocId=304 
			AND OM.numDomainId=@numDomainID)
BEGIN
	SET @canChangeDiferredIncomeSelection = 0
END   
   
                               
select                                           
vcDomainName,                                           
isnull(vcDomainDesc,'') vcDomainDesc,                                           
isnull(bitExchangeIntegration,0) bitExchangeIntegration,                                           
isnull(bitAccessExchange,0) bitAccessExchange,                                           
isnull(vcExchUserName,'') vcExchUserName,                                           
isnull(vcExchPassword,'') vcExchPassword,                                           
isnull(vcExchPath,'') vcExchPath,                                           
isnull(vcExchDomain,'') vcExchDomain,                                         
tintCustomPagingRows,                                         
vcDateFormat,                                         
numDefCountry,                                         
tintComposeWindow,                                         
sintStartDate,                                         
sintNoofDaysInterval,                                         
tintAssignToCriteria,                                         
bitIntmedPage,
ISNULL(D.numCost,0) as numCost,                                         
tintFiscalStartMonth,                                    
--isnull(bitDeferredIncome,0) as bitDeferredIncome,                                  
isnull(tintPayPeriod,0) as tintPayPeriod,                                
isnull(numCurrencyID,0) as numCurrencyID,                            
isnull(vcCurrency,'') as vcCurrency,                      
isnull(charUnitSystem,'') as charUnitSystem ,                    
isnull(vcPortalLogo,'') as vcPortalLogo,                  
isnull(tintChrForComSearch,2) as tintChrForComSearch,                
isnull(tintChrForItemSearch,'') as tintChrForItemSearch,            
isnull(intPaymentGateWay,0) as intPaymentGateWay  ,        
 case when bitPSMTPServer= 1 then vcPSMTPServer else '' end as vcPSMTPServer  ,              
 case when bitPSMTPServer= 1 then  isnull(numPSMTPPort,0)  else 0 end as numPSMTPPort          
,isnull(bitPSMTPAuth,0) bitPSMTPAuth        
,isnull([vcPSmtpPassword],'')vcPSmtpPassword        
,isnull(bitPSMTPSSL,0) bitPSMTPSSL     
,isnull(bitPSMTPServer,0) bitPSMTPServer  ,isnull(vcPSMTPUserName,'') vcPSMTPUserName,      
--isnull(numDefaultCategory,0) numDefaultCategory,
isnull(numShipCompany,0)   numShipCompany,
isnull(bitAutoPopulateAddress,0) bitAutoPopulateAddress,
isnull(tintPoulateAddressTo,0) tintPoulateAddressTo,
isnull(bitMultiCompany,0) bitMultiCompany,
isnull(bitMultiCurrency,0) bitMultiCurrency,
ISNULL(bitCreateInvoice,0) bitCreateInvoice,
ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
ISNULL([bitSaveCreditCardInfo],0) bitSaveCreditCardInfo,
ISNULL([intLastViewedRecord],10) intLastViewedRecord,
ISNULL([tintCommissionType],1) tintCommissionType,
ISNULL([tintBaseTax],2) tintBaseTax,
ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
ISNULL(bitEmbeddedCost,0) bitEmbeddedCost,
ISNULL(numDefaultSalesOppBizDocId,0) numDefaultSalesOppBizDocId,
ISNULL(tintSessionTimeOut,1) tintSessionTimeOut,
ISNULL(tintDecimalPoints,2) tintDecimalPoints,
ISNULL(bitCustomizePortal,0) bitCustomizePortal,
ISNULL(tintBillToForPO,0) tintBillToForPO,
ISNULL(tintShipToForPO,0) tintShipToForPO,
ISNULL(tintOrganizationSearchCriteria,1) tintOrganizationSearchCriteria,
ISNULL(tintLogin,0) tintLogin,
lower(ISNULL(vcPortalName,'')) vcPortalName,
ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
D.numDomainID,
ISNULL(vcPSMTPDisplayName,'') vcPSMTPDisplayName,
ISNULL(bitGtoBContact,0) bitGtoBContact,
ISNULL(bitBtoGContact,0) bitBtoGContact,
ISNULL(bitGtoBCalendar,0) bitGtoBCalendar,
ISNULL(bitBtoGCalendar,0) bitBtoGCalendar,
ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
ISNULL(bitInlineEdit,0) bitInlineEdit,
ISNULL(bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
ISNULL(bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
isnull(D.bitRentalItem,0) as bitRentalItem,
isnull(D.numRentalItemClass,0) as numRentalItemClass,
isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
ISNULL(D.bitAutoSerialNoAssign,0) as bitAutoSerialNoAssign,
ISNULL(D.bitIsShowBalance,0) AS [bitIsShowBalance],
ISNULL(numIncomeAccID,0) AS [numIncomeAccID],
ISNULL(numCOGSAccID,0) AS [numCOGSAccID],
ISNULL(numAssetAccID,0) AS [numAssetAccID],
ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
ISNULL(D.vcShipToPhoneNo,0) AS vcShipToPhoneNo,
ISNULL(D.vcGAUserEMail,'') AS vcGAUserEMail,
ISNULL(D.vcGAUserPassword,'') AS vcGAUserPassword,
ISNULL(D.vcGAUserProfileId,0) AS vcGAUserProfileId,
ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
ISNULL(D.vcHideTabs,'') AS vcHideTabs,
ISNULL(D.bitUseBizdocAmount,0) AS bitUseBizdocAmount,
ISNULL(D.bitDefaultRateType,0) AS bitDefaultRateType,
ISNULL(D.bitIncludeTaxAndShippingInCommission,0) AS bitIncludeTaxAndShippingInCommission,
/*,ISNULL(D.bitAllowPPVariance,0) AS bitAllowPPVariance*/
ISNULL(D.bitPurchaseTaxCredit,0) AS bitPurchaseTaxCredit,
ISNULL(D.bitLandedCost,0) AS bitLandedCost,
ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
ISNULL(AP.numAbovePercent,0) AS numAbovePercent,
ISNULL(AP.numBelowPercent,0) AS numBelowPercent,
ISNULL(AP.numBelowPriceField,0) AS numBelowPriceField,
@listIds AS vcUnitPriceApprover,
ISNULL(D.numDefaultSalesPricing,2) AS numDefaultSalesPricing,
ISNULL(D.bitReOrderPoint,0) AS bitReOrderPoint,
ISNULL(D.numReOrderPointOrderStatus,0) AS numReOrderPointOrderStatus,
ISNULL(D.numPODropShipBizDoc,0) AS numPODropShipBizDoc,
ISNULL(D.numPODropShipBizDocTemplate,0) AS numPODropShipBizDocTemplate,
ISNULL(D.IsEnableDeferredIncome,0) AS IsEnableDeferredIncome,
ISNULL(D.bitApprovalforTImeExpense,0) AS bitApprovalforTImeExpense,
ISNULL(D.intTimeExpApprovalProcess,0) AS intTimeExpApprovalProcess,
ISNULL(D.bitApprovalforOpportunity,0) AS bitApprovalforOpportunity,
ISNULL(D.intOpportunityApprovalProcess,0) AS intOpportunityApprovalProcess,
@canChangeDiferredIncomeSelection AS canChangeDiferredIncomeSelection,
ISNULL(CAST(D.numDefaultSalesShippingDoc AS int),0) AS numDefaultSalesShippingDoc,
ISNULL(CAST(D.numDefaultPurchaseShippingDoc AS int),0) AS numDefaultPurchaseShippingDoc,
ISNULL(D.bitchkOverRideAssignto,0) AS bitchkOverRideAssignto,
ISNULL(D.vcPrinterIPAddress,'') vcPrinterIPAddress,
ISNULL(D.vcPrinterPort,'') vcPrinterPort,
ISNULL(AP.bitCostApproval,0) bitCostApproval
,ISNULL(AP.bitListPriceApproval,0) bitListPriceApproval
,ISNULL(AP.bitMarginPriceViolated,0) bitMarginPriceViolated
,ISNULL(vcSalesOrderTabs,'Sales Orders') AS vcSalesOrderTabs
,ISNULL(vcSalesQuotesTabs,'Sales Quotes') AS vcSalesQuotesTabs
,ISNULL(vcItemPurchaseHistoryTabs,'Item Purchase History') AS vcItemPurchaseHistoryTabs
,ISNULL(vcItemsFrequentlyPurchasedTabs,'Items Frequently Purchased') AS vcItemsFrequentlyPurchasedTabs
,ISNULL(vcOpenCasesTabs,'Open Cases') AS vcOpenCasesTabs
,ISNULL(vcOpenRMATabs,'Open RMAs') AS vcOpenRMATabs
,ISNULL(bitSalesOrderTabs,0) AS bitSalesOrderTabs
,ISNULL(bitSalesQuotesTabs,0) AS bitSalesQuotesTabs
,ISNULL(bitItemPurchaseHistoryTabs,0) AS bitItemPurchaseHistoryTabs
,ISNULL(bitItemsFrequentlyPurchasedTabs,0) AS bitItemsFrequentlyPurchasedTabs
,ISNULL(bitOpenCasesTabs,0) AS bitOpenCasesTabs
,ISNULL(bitOpenRMATabs,0) AS bitOpenRMATabs
,ISNULL(bitSupportTabs,0) AS bitSupportTabs
,ISNULL(vcSupportTabs,'Support') AS vcSupportTabs
,ISNULL(D.numDefaultSiteID,0) AS numDefaultSiteID
,ISNULL(tintOppStautsForAutoPOBackOrder,0) AS tintOppStautsForAutoPOBackOrder
,ISNULL(tintUnitsRecommendationForAutoPOBackOrder,1) AS tintUnitsRecommendationForAutoPOBackOrder
,ISNULL(bitRemoveGlobalLocation,0) AS bitRemoveGlobalLocation
,ISNULL(numAuthorizePercentage,0) AS numAuthorizePercentage
,ISNULL(bitEDI,0) AS bitEDI
,ISNULL(bit3PL,0) AS bit3PL
,ISNULL(numListItemID,0) AS numListItemID
,ISNULL(bitAllowDuplicateLineItems,0) bitAllowDuplicateLineItems
,ISNULL(tintCommitAllocation,1) tintCommitAllocation
,ISNULL(tintInvoicing,1) tintInvoicing
,ISNULL(tintMarkupDiscountOption,1) tintMarkupDiscountOption
,ISNULL(tintMarkupDiscountValue,1) tintMarkupDiscountValue
,ISNULL(bitIncludeRequisitions,0) bitIncludeRequisitions
,ISNULL(bitCommissionBasedOn,0) bitCommissionBasedOn
,ISNULL(tintCommissionBasedOn,0) tintCommissionBasedOn
,ISNULL(bitDoNotShowDropshipPOWindow,0) bitDoNotShowDropshipPOWindow
,ISNULL(tintReceivePaymentTo,2) tintReceivePaymentTo
,ISNULL(numReceivePaymentBankAccount,0) numReceivePaymentBankAccount
,ISNULL(numOverheadServiceItemID,0) numOverheadServiceItemID,
ISNULL(D.bitDisplayCustomField,0) AS bitDisplayCustomField,
ISNULL(D.bitFollowupAnytime,0) AS bitFollowupAnytime,
ISNULL(D.bitpartycalendarTitle,0) AS bitpartycalendarTitle,
ISNULL(D.bitpartycalendarLocation,0) AS bitpartycalendarLocation,
ISNULL(D.bitpartycalendarDescription,0) AS bitpartycalendarDescription,
ISNULL(D.bitREQPOApproval,0) AS bitREQPOApproval,
ISNULL(D.bitARInvoiceDue,0) AS bitARInvoiceDue,
ISNULL(D.bitAPBillsDue,0) AS bitAPBillsDue,
ISNULL(D.bitItemsToPickPackShip,0) AS bitItemsToPickPackShip,
ISNULL(D.bitItemsToInvoice,0) AS bitItemsToInvoice,
ISNULL(D.bitSalesOrderToClose,0) AS bitSalesOrderToClose,
ISNULL(D.bitItemsToPutAway,0) AS bitItemsToPutAway,
ISNULL(D.bitItemsToBill,0) AS bitItemsToBill,
ISNULL(D.bitPosToClose,0) AS bitPosToClose,
ISNULL(D.bitPOToClose,0) AS bitPOToClose,
ISNULL(D.bitBOMSToPick,0) AS bitBOMSToPick,

ISNULL(D.vchREQPOApprovalEmp,'') AS vchREQPOApprovalEmp,
ISNULL(D.vchARInvoiceDue,'') AS vchARInvoiceDue,
ISNULL(D.vchAPBillsDue,'') AS vchAPBillsDue,
ISNULL(D.vchItemsToPickPackShip,'') AS vchItemsToPickPackShip,
ISNULL(D.vchItemsToInvoice,'') AS vchItemsToInvoice,
ISNULL(D.vchPriceMarginApproval,'') AS vchPriceMarginApproval,
ISNULL(D.vchSalesOrderToClose,'') AS vchSalesOrderToClose,
ISNULL(D.vchItemsToPutAway,'') AS vchItemsToPutAway,
ISNULL(D.vchItemsToBill,'') AS vchItemsToBill,
ISNULL(D.vchPosToClose,'') AS vchPosToClose,
ISNULL(D.vchPOToClose,'') AS vchPOToClose,
ISNULL(D.vchBOMSToPick,'') AS vchBOMSToPick,
ISNULL(D.decReqPOMinValue,0) AS decReqPOMinValue,
ISNULL(D.bitUseOnlyActionItems,0) AS bitUseOnlyActionItems,
ISNULL(D.vcElectiveItemFields,'') AS vcElectiveItemFields,
ISNULL(tintMailProvider,3) tintMailProvider,
ISNULL(D.bitDisplayContractElement,0) AS bitDisplayContractElement,
ISNULL(D.vcEmployeeForContractTimeElement,'') AS vcEmployeeForContractTimeElement,
ISNULL(numARContactPosition,0) numARContactPosition,
ISNULL(bitShowCardConnectLink,0) bitShowCardConnectLink,
ISNULL(vcBluePayFormName,'') vcBluePayFormName,
ISNULL(vcBluePaySuccessURL,'') vcBluePaySuccessURL,
ISNULL(vcBluePayDeclineURL,'') vcBluePayDeclineURL,
ISNULL(bitUseDeluxeCheckStock,0) bitUseDeluxeCheckStock
from Domain D  
LEFT JOIN eCommerceDTL eComm  on eComm.numDomainID=D.numDomainID  
LEFT JOIN ApprovalProcessItemsClassification AP ON AP.numDomainID = D.numDomainID
where (D.numDomainID=@numDomainID OR @numDomainID=-1)


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemTransit')
DROP PROCEDURE USP_GetItemTransit
GO
CREATE PROCEDURE [dbo].[USP_GetItemTransit] 
( 
@numDomainID as numeric(9)=0,    
@numItemcode AS NUMERIC(9)=0,
@numVendorID AS NUMERIC(9)=0,
@numAddressID AS NUMERIC(9)=0,
@tintMode AS TINYINT,
@ClientTimeZoneOffset      AS INT
)
AS 
--tintOppStatus = 1 -> Won 
--[tintOppType] = 2 AND [tintOppStatus] = 1 -> Purchase Order 
--tintShipped = 0 

IF @tintMode=1
BEGIN
select dbo.fn_GetItemTransitCount(@numItemcode,@numDomainID) AS Total
--	select COUNT(*) AS Total
--      FROM OpportunityMaster Opp INNER JOIN OpportunityItems OppI ON opp.numOppId=OppI.numOppId 
--		   INNER JOIN item I on OppI.numItemCode=i.numItemcode
--		   LEFT JOIN  UOM u ON u.numUOMId = OppI.numUOMId  
--           WHERE Opp.tintOppType= 2 and Opp.tintOppstatus=1 and Opp.tintShipped=0 
--				 and Opp.numDomainId=@numDomainID AND OppI.numItemcode=@numItemcode
--				 and (isnull(OppI.numUnitHour,0)-isnull(OppI.numUnitHourReceived,0)>0)

END

ELSE IF @tintMode=2
BEGIN 
select opp.numOppId,Opp.vcPOppName,
case when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate) )= convert(varchar(11),getdate()) then '<b><font color=red>Today</font></b>'when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate) )= convert(varchar(11),dateadd(day,-1,getdate())) then '<b><font color=purple>YesterDay</font></b>'when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate) )= convert(varchar(11),dateadd(day,1,getdate())) then'<b><font color=orange>Tommorow</font></b>' else dbo.FormatedDateFromDate(Opp.bintCreatedDate,1) end  bintCreatedDate,
dbo.fn_GetContactName(Opp.numContactId) AS vcOrderedBy,OppI.vcItemName,
CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),OppI.numItemCode,OPP.numDomainId,ISNULL(OppI.numUOMId,0)) * (isnull(OppI.numUnitHour,0)-isnull(OppI.numUnitHourReceived,0))) as numeric(18,0)) as numUnitHour
,ISNULL(u.vcUnitName,'') vcUOMName,OppI.vcManufacturer,ISNULL(OppI.vcItemDesc,'') as vcItemDesc,OppI.vcModelID,
dbo.fn_GetListItemName(Opp.intUsedShippingCompany) vcShipmentMethod,
CASE WHEN ISNULL(VSM.numListValue,0)>0 THEN 
case when convert(varchar(11),DATEADD(DAY,VSM.numListValue,DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate)))= convert(varchar(11),getdate()) then '<b><font color=red>Today</font></b>'
	 when convert(varchar(11),DATEADD(DAY,VSM.numListValue,DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate)))= convert(varchar(11),dateadd(day,-1,getdate())) then '<b><font color=purple>YesterDay</font></b>'
	 when convert(varchar(11),DATEADD(DAY,VSM.numListValue,DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate)))= convert(varchar(11),dateadd(day,1,getdate())) then'<b><font color=orange>Tommorow</font></b>' 
	 else dbo.FormatedDateFromDate(DATEADD(DAY,VSM.numListValue,DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate)),@numDomainID) end 
	 ELSE '' end bintExpectedDelivery
      FROM OpportunityMaster Opp INNER JOIN OpportunityItems OppI ON opp.numOppId=OppI.numOppId  LEFT JOIN WareHouseItems WI ON OppI.numWarehouseItmsID=WI.numWareHouseItemID
		   INNER JOIN item I on OppI.numItemCode=i.numItemcode
		   LEFT JOIN  UOM u ON u.numUOMId = OppI.numUOMId  
		   OUTER APPLY (SELECT TOP 1 * FROM VendorShipmentMethod VSM WHERE VSM.numAddressID=Opp.numVendorAddressID AND VSM.numVendorID=Opp.numDivisionId ORDER BY (CASE WHEN VSM.numWarehouseID=WI.numWareHouseID THEN 1 ELSE 0 END) DESC, ISNULL(bitPrimary,0) DESC, ISNULL(bitPreferredMethod,0) DESC) VSM
           WHERE Opp.tintOppType= 2 and Opp.tintOppstatus=1 and Opp.tintShipped=0 
				 and Opp.numDomainId=@numDomainID AND OppI.numItemcode=@numItemcode
				 and (isnull(OppI.numUnitHour,0)-isnull(OppI.numUnitHourReceived,0)>0)
END

ELSE IF @tintMode=3
BEGIN 
select opp.numOppId,Opp.vcPOppName,case when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate) )= convert(varchar(11),getdate()) then '<b><font color=red>Today</font></b>'when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate) )= convert(varchar(11),dateadd(day,-1,getdate())) then '<b><font color=purple>YesterDay</font></b>'when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate) )= convert(varchar(11),dateadd(day,1,getdate())) then'<b><font color=orange>Tommorow</font></b>' else dbo.FormatedDateFromDate(Opp.bintCreatedDate,1) end  bintCreatedDate,
dbo.fn_GetContactName(Opp.numContactId) AS vcOrderedBy,OppI.vcItemName,
CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),OppI.numItemCode,OPP.numDomainId,ISNULL(OppI.numUOMId,0)) * (isnull(OppI.numUnitHour,0)-isnull(OppI.numUnitHourReceived,0))) as numeric(18,0)) as numUnitHour
,ISNULL(u.vcUnitName,'') vcUOMName,OppI.vcManufacturer,ISNULL(OppI.vcItemDesc,'') as vcItemDesc,OppI.vcModelID,
dbo.fn_GetListItemName(OppI.numShipmentMethod) vcShipmentMethod,
CASE WHEN ISNULL(VSM.numListValue,0)>0 THEN 
case when convert(varchar(11),DATEADD(DAY,VSM.numListValue,DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate)))= convert(varchar(11),getdate()) then '<b><font color=red>Today</font></b>'
	 when convert(varchar(11),DATEADD(DAY,VSM.numListValue,DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate)))= convert(varchar(11),dateadd(day,-1,getdate())) then '<b><font color=purple>YesterDay</font></b>'
	 when convert(varchar(11),DATEADD(DAY,VSM.numListValue,DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate)))= convert(varchar(11),dateadd(day,1,getdate())) then'<b><font color=orange>Tommorow</font></b>' 
	 else dbo.FormatedDateFromDate(DATEADD(DAY,VSM.numListValue,DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate)),1) end 
	 ELSE '' end bintExpectedDelivery
      FROM OpportunityMaster Opp INNER JOIN OpportunityItems OppI ON opp.numOppId=OppI.numOppId LEFT JOIN WareHouseItems WI ON OppI.numWarehouseItmsID=WI.numWareHouseItemID
		   INNER JOIN item I on OppI.numItemCode=i.numItemcode
		   LEFT JOIN  UOM u ON u.numUOMId = OppI.numUOMId  
		   OUTER APPLY (SELECT TOP 1 * FROM VendorShipmentMethod VSM WHERE VSM.numAddressID=Opp.numVendorAddressID AND VSM.numVendorID=Opp.numDivisionId ORDER BY (CASE WHEN VSM.numWarehouseID=WI.numWareHouseID THEN 1 ELSE 0 END) DESC, ISNULL(bitPrimary,0) DESC, ISNULL(bitPreferredMethod,0) DESC) VSM
           WHERE Opp.tintOppType= 2 and Opp.tintOppstatus=1 and Opp.tintShipped=0 
				 and Opp.numDomainId=@numDomainID AND Opp.numDivisionId=@numVendorID AND OppI.numVendorWareHouse=@numAddressID
				 and (isnull(OppI.numUnitHour,0)-isnull(OppI.numUnitHourReceived,0)>0)
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetNavigation')
DROP PROCEDURE dbo.USP_GetNavigation
GO
CREATE PROCEDURE [dbo].[USP_GetNavigation]
(
	@numDomainID NUMERIC(18,0),
	@numGroupID NUMERIC(18,0)
)
AS 
BEGIN

	/********  MAIN MENU  ********/

	SELECT  
		T.numTabID,
		ISNULL((SELECT TOP 1 numTabName FROM TabDefault WHERE numDomainid = @numDomainID AND numTabId = T.numTabId AND tintTabType = T.tintTabType),T.numTabName) vcTabName,
		CASE 
			WHEN (SELECT COUNT(*) FROM ShortCutGrpConf WHERE numDomainID=@numDomainID AND numGroupId=@numGroupID AND numTabId=T.numTabId AND ISNULL(bitInitialPage,0)=1) > 0
			THEN
				REPLACE(ISNULL((SELECT TOP 1 Link FROM ShortCutBar WHERE (numDomainid = @numDomainID OR bitDefault=1) AND numTabId = T.numTabId AND id=(SELECT TOP 1 numLinkId FROM ShortCutGrpConf WHERE numDomainID=@numDomainID AND numGroupId=@numGroupID AND numTabId=T.numTabId AND ISNULL(bitInitialPage,0)=1)),''),'../','')
			ELSE 
				ISNULL(vcURL,'')
		END vcURL,
		ISNULL(vcAddURL,'') vcAddURL,
		ISNULL(bitAddIsPopUp,0) AS bitAddIsPopUp,
		numOrder
	FROM    
		TabMaster T
	JOIN 
		GroupTabDetails G ON G.numTabId = T.numTabId
	WHERE   
		(numDomainID = @numDomainID OR bitFixed = 1)
		AND numGroupID = @numGroupID
		AND ISNULL(G.[tintType], 0) <> 1
		AND tintTabType =1
		AND T.numTabID NOT IN (2,68)
	ORDER BY 
		numOrder   


	/********  SUB MENU  ********/

	DECLARE @numTabID NUMERIC(18,0)
	DECLARE @numModuleID AS NUMERIC(18, 0)

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1),
		numOrigParentID NUMERIC(18,0),
		numParentID NUMERIC(18,0),
		numListItemID NUMERIC(18,0),
		numTabID NUMERIC(18,0),
		numPageNavID NUMERIC(18,0),
		vcNodeName VARCHAR(500),
		vcURL VARCHAR(1000),
		vcAddURL VARCHAR(1000),
		bitAddIsPopUp BIT,
		[order] int,
		bitVisible BIT,
		vcImageURL VARCHAR(1000)
	)

	BEGIN /******************** OPPORTUNITY/ORDER ***********************/

		SET @numTabID = 1
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID

		INSERT INTO
			@TEMP
		SELECT
			0,
			0,
			0,
			@numTabID,
			PND.numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL,'') AS vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM 
			PageNavigationDTL PND
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID  
			AND PND.numTabID = TNA.numTabID       
		WHERE  
			ISNULL(TNA.bitVisible, 0) = 1
			AND ISNULL(PND.bitVisible, 0) = 1
			AND numModuleID = @numModuleID
			AND TNA.numDomainID = @numDomainID
			AND numGroupID = @numGroupID
			AND PND.numParentID <> 0
		ORDER BY 
			numPageNavID 

		INSERT INTO
			@TEMP
		SELECT  
			0,
			0,
			0,
			@numTabID,
			0,
			ISNULL(vcData,'') + 's' [vcPageNavName],
			'../Opportunity/frmOpenBizDocs.aspx?BizDocName=' + vcData + '&BizDocID=' + CONVERT(VARCHAR(10),LD.numListItemID) AS [vcNavURL],
			'',
			0,
			0,
			1 ,
			''
		FROM 
			dbo.ListDetails LD
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			TNA.numTabID=1
			AND LD.numListItemID = TNA.numPageNavID 
			AND ISNULL(bitVisible,0) = 1  
		WHERE 
			numListID = 27 
			AND ISNULL(TNA.[numDomainID], 0) = @numDomainID
			AND ISNULL(TNA.[numGroupID], 0) = @numGroupID
		ORDER BY 
			numPageNavID 

	END

	BEGIN /******************** Email ***************************/
		SET @numTabID = 44
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
		    ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 0) = 1      
			AND ISNULL(PND.bitVisible, 0) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID
	END

	BEGIN /******************** MARKETING ***************************/
		SET @numTabID = 3
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
		    ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 0) = 1      
			AND ISNULL(PND.bitVisible, 0) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID
	END

	BEGIN /******************** SUPPORT/CASE ***************************/

		SET @numTabID = 4
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
			ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

	END

	BEGIN /******************** REPORTS ***************************/

		SET @numTabID = 6
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
		    ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

	END

	BEGIN /******************** RELATIONSHIP ***********************/

		SET @numTabID = 7
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO 
			@TEMP
		SELECT    
			ISNULL(PND.numParentID,0),
			PND.numParentID AS numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			ISNULL(PND.vcPageNavName, '') AS [vcNodeName],
			ISNULL(vcNavURL, '') AS vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			ISNULL(TreeNodeOrder.numOrder,1000) AS numOrder,
			ISNULL(TNA.bitVisible,1) AS bitVisible,
			ISNULL(PND.vcImageURL,'')
		FROM      
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			TNA.numPageNavID = PND.numPageNavID AND
			TNA.numDomainID = @numDomainID AND 
			TNA.numGroupID = @numGroupID
			AND TNA.numTabID=@numTabID
		LEFT JOIN
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID = PND.numPageNavID
		WHERE     
			ISNULL(PND.bitVisible, 0) = 1
			AND ISNULL(TNA.bitVisible, 0) = 1
			AND PND.numParentID <> 0
			AND numModuleID = @numModuleID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

		INSERT INTO 
			@TEMP
		SELECT
			0,
			0 AS numParentID,
			ListDetails.numListItemID,
			@numTabID,
			0 AS numPageNavID,
			ListDetails.vcData AS [vcNodeName],
			'../prospects/frmCompanyList.aspx?RelId='+ convert(varchar(10),ListDetails.numListItemID),
			CASE WHEN ListDetails.numListItemID = 93 THEN '' ELSE CONCAT('~/include/frmAddOrganization.aspx?RelID=',ListDetails.numListItemID,'&FormID=36') END,
			CASE WHEN ListDetails.numListItemID = 93 THEN 0 ELSE 1 END,
			ISNULL(TreeNodeOrder.numOrder,2000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM
			ListDetails
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = ListDetails.numListItemID AND
			dbo.TreeNodeOrder.numParentID = 6 AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE
			numListID = 5 AND
			(ListDetails.numDomainID = @numDomainID  OR constFlag = 1) AND
			(ISNULL(bitDelete,0) = 0 OR constFlag = 1) AND
			ListDetails.numListItemID <> 46 

		INSERT INTO 
			@TEMP
		SELECT 
			0,
			(SELECT ID FROM @TEMP WHERE numListItemID = FRDTL.numPrimaryListItemID),
			L2.numListItemID,   
			@numTabID,
			0,
			L2.vcData AS [vcNodeName],
			CONCAT('../prospects/frmCompanyList.aspx?RelId=',FRDTL.numPrimaryListItemID,'&profileid=',numSecondaryListItemID),
			--CONCAT('~/include/frmAddOrganization.aspx?RelID=',FRDTL.numPrimaryListItemID,'&profileid=',numSecondaryListItemID,'&FormID=36'),
			CASE WHEN L2.numListItemID = 37257 THEN '' ELSE CONCAT('~/include/frmAddOrganization.aspx?RelID=',FRDTL.numPrimaryListItemID,'&profileid=',numSecondaryListItemID,'&FormID=36') END,
			--1,
			CASE WHEN L2.numListItemID = 37257 THEN 0 ELSE 1 END,
			ISNULL(TreeNodeOrder.numOrder,3000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM      
			FieldRelationship FR
		join 
			FieldRelationshipDTL FRDTL on FRDTL.numFieldRelID = FR.numFieldRelID
		join 
			ListDetails L1 on numPrimaryListItemID = L1.numListItemID
		join 
			ListDetails L2 on numSecondaryListItemID = L2.numListItemID
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
			dbo.TreeNodeOrder.numParentID = FRDTL.numPrimaryListItemID AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE     
			numPrimaryListItemID <> 46
			and FR.numPrimaryListID = 5
			and FR.numSecondaryListID = 21
			and FR.numDomainID = @numDomainID
			and L2.numDomainID = @numDomainID

		INSERT INTO
			@TEMP
		SELECT 
			0,
			ISNULL((SELECT ID FROM @TEMP WHERE numPageNavID=11),0),
			L2.numListItemID,
			@numTabID,
			11,
			L2.vcData AS [vcNodeName],
			'../prospects/frmProspectList.aspx?numProfile=' + CONVERT(VARCHAR(10), L2.numListItemID) AS vcNavURL,
			CONCAT('~/include/frmAddOrganization.aspx?RelID=3&profileid=',numSecondaryListItemID,'&FormID=35'),
			1,
			ISNULL(TreeNodeOrder.numOrder,4000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM    
			FieldRelationship AS FR
		INNER JOIN 
			FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
		INNER JOIN 
			ListDetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
		INNER JOIN 
			ListDetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
			dbo.TreeNodeOrder.numParentID = 11 AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE 
			FRD.numPrimaryListItemID = 46
			AND FR.numDomainID = @numDomainID

		INSERT INTO
			@TEMP
		SELECT 
			0,
			(SELECT ID FROM @TEMP WHERE numPageNavID=12),
			L2.numListItemID,
			@numTabID,
			12,
			L2.vcData AS [vcNodeName],
			'../account/frmAccountList.aspx?numProfile=' + CONVERT(VARCHAR(10), L2.numListItemID) AS vcNavURL,
			CONCAT('~/include/frmAddOrganization.aspx?RelID=2&profileid=',numSecondaryListItemID,'&FormID=36'),
			1,
			ISNULL(TreeNodeOrder.numOrder,5000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM    
			FieldRelationship AS FR
		INNER JOIN 
			FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
		INNER JOIN 
			ListDetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
		INNER JOIN 
			ListDetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
			dbo.TreeNodeOrder.numParentID = 12 AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE 
			FRD.numPrimaryListItemID = 46
			AND FR.numDomainID = @numDomainID

		INSERT INTO 
			@TEMP
		SELECT 
			0,
			(SELECT ID FROM @TEMP WHERE numListItemID=FRD.numPrimaryListItemID AND numPageNavID=11),
			L2.numListItemID,
			@numTabID,
			0,
			L2.vcData AS [vcNodeName],
			'../account/frmProspectList.aspx?numProfile=' + CONVERT(VARCHAR(10), FRD.numPrimaryListItemID) + '&numTerritoryID=' + CONVERT(VARCHAR(10), L2.numListItemID) AS vcNavURL,
			CONCAT('~/include/frmAddOrganization.aspx?RelID=3&profileid=',FRD.numPrimaryListItemID,'&FormID=35','&numTerritoryID=',L2.numListItemID),
			1,
			ISNULL(TreeNodeOrder.numOrder,7000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM    
			FieldRelationship AS FR
		INNER JOIN 
			FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
		INNER JOIN 
			ListDetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
		INNER JOIN 
			ListDetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
			dbo.TreeNodeOrder.numParentID = 11 AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE   
			FRD.numPrimaryListItemID IN (SELECT numSecondaryListItemID FROM FieldRelationshipDTL FRDL JOIN FieldRelationship FRP ON FRDL.numFieldRelID = FRP.numFieldRelID WHERE numDomainID=@numDomainID AND numPrimaryListItemID=46)
			AND FR.numDomainID = @numDomainID

		INSERT INTO 
			@TEMP
		SELECT 
			0,
			(SELECT ID FROM @TEMP WHERE numListItemID=FRD.numPrimaryListItemID AND numPageNavID=12),
			L2.numListItemID,
			@numTabID,
			0,
			L2.vcData AS [vcNodeName],
			'../account/frmAccountList.aspx?numProfile=' + CONVERT(VARCHAR(10), FRD.numPrimaryListItemID) + '&numTerritoryID=' + CONVERT(VARCHAR(10), L2.numListItemID) AS vcNavURL,
			CONCAT('~/include/frmAddOrganization.aspx?RelID=2&profileid=',FRD.numPrimaryListItemID,'&FormID=36','&numTerritoryID=',L2.numListItemID),
			1,
			ISNULL(TreeNodeOrder.numOrder,8000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM    
			FieldRelationship AS FR
		INNER JOIN 
			FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
		INNER JOIN 
			ListDetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
		INNER JOIN 
			ListDetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
			dbo.TreeNodeOrder.numParentID = 12 AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE   
			FRD.numPrimaryListItemID IN (SELECT numSecondaryListItemID FROM FieldRelationshipDTL FRDL JOIN FieldRelationship FRP ON FRDL.numFieldRelID = FRP.numFieldRelID WHERE numDomainID=@numDomainID AND numPrimaryListItemID=46)
			AND FR.numDomainID = @numDomainID

		

	END

	BEGIN /******************** DOCUMENTS ***********************/
		SET @numTabID = 8

		INSERT INTO 
			@TEMP
		SELECT 
			0,
			0,
			0,
			@numTabID,
			0,
			'Regular Documents' as vcPageNavName,
			'../Documents/frmRegularDocList.aspx?Category=0',
			'~/Documents/frmGenDocPopUp.aspx',
			1,
			1,
			1,
			''


		INSERT INTO 
			@TEMP
		SELECT  
			0,
			SCOPE_IDENTITY(),
			Ld.numListItemId,
			@numTabID,
			0,
			vcData vcPageNavName,
			'../Documents/frmRegularDocList.aspx?Category=' + cast(Ld.numListItemId as varchar(10)),
			CASE WHEN vcData = 'Email Template' THEN '~/Marketing/frmEmailTemplate.aspx' ELSE '' END,
			CASE WHEN vcData = 'Email Template' THEN 1 ELSE 0 END,
			ISNULL(intSortOrder, LD.sintOrder) SortOrder,
			1,
			''
		FROM    
			ListDetails LD
		LEFT JOIN 
			ListOrder LO 
		ON 
			LD.numListItemID = LO.numListItemID
			AND LO.numDomainId = @numDomainID
		WHERE   
			Ld.numListID = 29
			AND (constFlag = 1 OR Ld.numDomainID = @numDomainID)

	END

	BEGIN /******************** ACCOUNTING **********************/
	
		SET @numTabID = 45
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID

		INSERT INTO 
			@TEMP
		SELECT 
		    ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL,'') AS vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1 ,
			ISNULL(PND.vcImageURL,'')     
		FROM 
			PageNavigationDTL PND
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE  
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND numModuleID=@numModuleID
			AND TNA.numDomainID = @numDomainID
			AND numGroupID = @numGroupID
			AND numParentID <> 0
		ORDER BY 
			numParentID,PND.numPageNavID 
	
		INSERT INTO 
			@TEMP
		SELECT 
			0,
			(CASE numFinancialViewID 
				WHEN 26770 THEN 96 /*Profit & Loss*/
				WHEN 26769 THEN 176 /*Income & Expense*/
				WHEN 26768 THEN 98 /*Cash Flow Statement*/
				WHEN 26767 THEN 97 /*Balance Sheet*/
				WHEN 26766 THEN 81 /*A/R & A/P Aging*/
				WHEN 26766 THEN 82 /*A/R & A/P Aging*/
				ELSE 0
			END),
			numFRID,
			@numTabID,
			0,
			vcReportName,
			CASE numFinancialViewID 
				WHEN 26770 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =96 AND numModuleID=35)  /*Profit & Loss*/
				WHEN 26769 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =176 AND numModuleID=35) /*Income & Expense*/
				WHEN 26768 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =98 AND numModuleID=35) /*Cash Flow Statement*/
				WHEN 26767 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =97 AND numModuleID=35) /*Balance Sheet*/
				WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =81 AND numModuleID=35) /*A/R & A/P Aging*/
				WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =82 AND numModuleID=35) /*A/R & A/P Aging*/
				ELSE ''
			END + 
			CASE CHARINDEX('?',(CASE numFinancialViewID 
				WHEN 26770 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =96 AND numModuleID=35)  /*Profit & Loss*/
				WHEN 26769 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =176 AND numModuleID=35) /*Income & Expense*/
				WHEN 26768 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =98 AND numModuleID=35) /*Cash Flow Statement*/
				WHEN 26767 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =97 AND numModuleID=35) /*Balance Sheet*/
				WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =81 AND numModuleID=35) /*A/R & A/P Aging*/
				WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =82 AND numModuleID=35) /*A/R & A/P Aging*/
				ELSE ''
			END)) 
				WHEN 0 THEN '?FRID='
				ELSE '&FRID='
			END + CAST(numFRID AS VARCHAR),
			'',
			0,
			0,
			1,
			''
		FROM 
			dbo.FinancialReport 
		WHERE 
			numDomainID=@numDomainID
	
		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID
	  
	END

	BEGIN /******************** ITEMS **********************/

		SET @numTabID = 80
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
			ISNULL(PND.numParentID,0),
			numParentID,
			0,
			@numTabID,
			PND.numPageNavID,
			vcPageNavName,
			isnull(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM 
			PageNavigationDTL PND
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE
			ISNULL(TNA.bitVisible, 0) = 1
			AND ISNULL(PND.bitVisible, 0) = 1
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
			AND numParentID <> 0
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID
	END

	BEGIN /******************** Activities ***************************/

		SET @numTabID = 36
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
			ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

	END

	BEGIN /******************** HUMAN RESOURCE ***************************/

		SET @numTabID = (SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1)
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
			ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

	END

	BEGIN /******************** CONTRACTS ***************************/

		SET @numTabID = (SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Contracts' AND numDomainID=1)
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
			ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

	END

	BEGIN /******************** Procurement ***************************/

		SET @numTabID = (SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Procurement' AND numDomainID=1)
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
			ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

	END
	BEGIN /******************** CONTACTS ***************************/

		SET @numTabID = (SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Contacts' AND numDomainID=1 AND tintTabType=1)
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
			ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

		INSERT INTO
			@TEMP
		SELECT 
			0,
			(SELECT ISNULL(ID,0) FROM @TEMP WHERE numPageNavID=13),
			listdetails.numListItemID,
			@numTabID,
			0,
			listdetails.vcData AS [vcNodeName],
			'../contact/frmContactList.aspx?ContactType=' + CONVERT(VARCHAR(10), listdetails.numListItemID) AS vcNavURL,
		   '~/Contact/newcontact.aspx',
		   1,
			ISNULL(TreeNodeOrder.numOrder,6000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM    
			listdetails
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = listdetails.numListItemID AND
			dbo.TreeNodeOrder.numParentID = 13 AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE   
			numListID = 8
			AND (constFlag = 1 OR listdetails.numDomainID = @numDomainID)

		INSERT INTO
			@TEMP
		SELECT 
			0,
			(SELECT ISNULL(ID,0) FROM @TEMP WHERE numPageNavID=13),
			101 AS numListItemID,
			@numTabID,
			0,
			'Primary Contact' AS [vcNodeName],
			'../Contact/frmcontactList.aspx?ContactType=101',
		 '~/Contact/newcontact.aspx',
		   1,
			ISNULL((
						SELECT 
							numOrder 
						FROM 
							dbo.TreeNodeOrder 
						WHERE
							dbo.TreeNodeOrder.numListItemID = 101 AND
							dbo.TreeNodeOrder.numParentID = 13 AND
							dbo.TreeNodeOrder.numTabID = @numTabID AND
							dbo.TreeNodeOrder.numDomainID = @numDomainID AND
							dbo.TreeNodeOrder.numGroupID = @numGroupID AND
							dbo.TreeNodeOrder.numPageNavID IS NULL
					),7000) AS numOrder,
			ISNULL((
						SELECT 
							bitVisible 
						FROM 
							dbo.TreeNodeOrder 
						WHERE
							dbo.TreeNodeOrder.numListItemID = 101 AND
							dbo.TreeNodeOrder.numParentID = 13 AND
							dbo.TreeNodeOrder.numTabID = @numTabID AND
							dbo.TreeNodeOrder.numDomainID = @numDomainID AND
							dbo.TreeNodeOrder.numGroupID = @numGroupID AND
							dbo.TreeNodeOrder.numPageNavID IS NULL
					),1) AS bitVisible,
					''

		UPDATE 
			t1
		SET 
			t1.numParentID = 13
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID AND numParentID IS NULL

	END

	BEGIN /******************** Manufacturing ***************************/

		SET @numTabID = (SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Manufacturing' AND numDomainID=1)
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
			ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

	END

	BEGIN /******************** Projects ***************************/

		SET @numTabID = (SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Projects' AND numDomainID=1 AND tintTabType=1)
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
			ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

	END


	;WITH CTE(ID) AS 
	(
		SELECT
			ID
		FROM
			@TEMP t1
		WHERE 
			ISNULL(numOrigParentID,0) > 0
			AND numOrigParentID NOT IN (SELECT numPageNavID FROM PageNavigationDTL WHERE numParentID=0 AND numTabID=t1.numTabID)
			AND numOrigParentID NOT IN (SELECT numPageNavID FROM @TEMP t2 WHERE t2.numTabID = t1.numTabID)
		UNION ALL
		SELECT 
			t2.ID
		FROM
			@TEMP t2
		JOIN
			CTE c
		ON
			t2.numParentID=c.ID
	)

	DELETE FROM @TEMP WHERE ID IN (SELECT ID FROM CTE)

	
	/** Admin Menu **/
	INSERT INTO 
		@TEMP
	SELECT 
		numParentID,
		numParentID,
		0,
		-1,
		PND.numPageNavID,
		vcPageNavName,
		ISNULL(vcNavURL, '') as vcNavURL,
		'',
		0,
		ISNULL(PND.intSortOrder,0) sintOrder,
		1,
		ISNULL(vcImageURL,'')
	FROM   
		PageNavigationDTL PND
	JOIN 
		dbo.TreeNavigationAuthorization TNA 
	ON 
		PND.numPageNavID = TNA.numPageNavID
		AND PND.numTabID = TNA.numTabID
	WHERE
		ISNULL(TNA.bitVisible, 0) = 1
		AND ISNULL(PND.bitVisible, 0) = 1
		AND PND.numModuleID = 13 --AND bitVisible=1  
		AND TNA.numDomainID = @numDomainID
		AND TNA.numGroupID = @numGroupID
	ORDER BY
		PND.numParentID, 
		PND.intSortOrder,
		(CASE WHEN PND.numPageNavID=79 THEN 2000 ELSE 0 END),	
		numPageNavID   

	-- MAKE FIRST LEVEL CHILD AS PARENT BECUASE WE ARE NOW NOT USING TREE STRUCTURE AND DIPLAYING ITEMS IN MENU
	UPDATE 
		@TEMP
	SET 
		numParentID = 0
	WHERE
		numTabID = -1
		AND numPageNavID = (SELECT numPageNavID FROM @TEMP WHERE numTabID=-1 AND numParentID = 0)

	-- REMOVE ALL CHILD WHOSE PARENT IS HIDDEN
	DELETE FROM @TEMP WHERE numTabID = -1 AND ISNULL(numPageNavID,0) <> 0 AND numParentID NOT IN (SELECT numPageNavID FROM @TEMP WHERE numTabID = -1)

	-- REMOVE ALL CHILD WHOSE PARENT IS HIDDEN
	DELETE FROM @TEMP WHERE numParentID = 0 AND numTabID=-1

	-- UPDATE PARENT DETAIL
	UPDATE 
		t1
	SET 
		t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numTabID=-1 AND t2.numPageNavID=t1.numParentID),0)
	FROM
		@TEMP t1
	WHERE
		t1.numTabID = -1


	/** Advance Search **/
	INSERT INTO 
		@TEMP
	SELECT 
		numParentID,
		numParentID,
		0,
		-3,
		PND.numPageNavID,
		vcPageNavName,
		ISNULL(vcNavURL, '') as vcNavURL,
		'',
		0,
		ISNULL(PND.intSortOrder,0) sintOrder,
		1,
		ISNULL(vcImageURL,'')
	FROM   
		PageNavigationDTL PND
	JOIN 
		dbo.TreeNavigationAuthorization TNA 
	ON 
		PND.numPageNavID = TNA.numPageNavID
		AND PND.numTabID = TNA.numTabID
	WHERE
		ISNULL(TNA.bitVisible, 0) = 1
		AND ISNULL(PND.bitVisible, 0) = 1
		AND PND.numModuleID = 9
		AND TNA.numDomainID = @numDomainID
		AND TNA.numGroupID = @numGroupID
	ORDER BY
		PND.numParentID, 
		sintOrder,
		numPageNavID   

	-- MAKE FIRST LEVEL CHILD AS PARENT BECUASE WE ARE NOW NOT USING TREE STRUCTURE AND DIPLAYING ITEMS IN MENU
	UPDATE 
		@TEMP
	SET 
		numParentID = 0
	WHERE
		numTabID = -3
		AND numPageNavID = (SELECT numPageNavID FROM @TEMP WHERE numTabID=-3 AND numParentID = 0)

	-- REMOVE ALL CHILD WHOSE PARENT IS HIDDEN
	DELETE FROM @TEMP WHERE numTabID = -3 AND ISNULL(numPageNavID,0) <> 0 AND numParentID NOT IN (SELECT numPageNavID FROM @TEMP WHERE numTabID = -3)

	-- REMOVE ALL CHILD WHOSE PARENT IS HIDDEN
	DELETE FROM @TEMP WHERE numParentID = 0 AND numTabID=-3

	-- UPDATE PARENT DETAIL
	UPDATE 
		t1
	SET 
		t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numTabID=-3 AND t2.numPageNavID=t1.numParentID),0)
	FROM
		@TEMP t1
	WHERE
		t1.numTabID = -3

	UPDATE 
		t1
	SET 
		t1.bitVisible = (SELECT bitVisible FROM @TEMP t2 WHERE t2.ID = t1.numParentID)
	FROM
		@TEMP t1
	WHERE
		ISNULL(t1.numParentID,0) > 0

	SELECT * FROM @TEMP WHERE bitVisible=1 ORDER BY numTabID, numParentID, [order]
END
GO
--created by anoop jayaraj

-- exec USP_GetOppItemBizDocs @numBizDocID=644,@numDomainID=169,@numOppID=476291,@numOppBizDocID=0,@bitRentalBizDoc=0
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetOppItemBizDocs' ) 
    DROP PROCEDURE USP_GetOppItemBizDocs
GO
CREATE PROCEDURE USP_GetOppItemBizDocs
    @numBizDocID AS NUMERIC(9),
    @numDomainID AS NUMERIC(9),
    @numOppID AS NUMERIC(9),
    @numOppBizDocID AS NUMERIC(9),
    @bitRentalBizDoc AS BIT = 0
AS 
BEGIN

	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
	SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM OpportunityMaster INNER JOIN DivisionMaster ON OpportunityMaster.numDivisionId=DivisionMaster.numDivisionID WHERE numOppId=@numOppID

	DECLARE @numSalesAuthoritativeBizDocID AS INT
	SELECT @numSalesAuthoritativeBizDocID = numAuthoritativeSales FROM [AuthoritativeBizDocs] WHERE numDomainID = @numDomainID


    IF @numOppBizDocID = 0 
        IF @bitRentalBizDoc = 1 
        BEGIN
            SELECT  *
            FROM    ( SELECT    OI.numoppitemtCode,
                                OI.numItemCode,
                                OI.vcItemName,
                                OI.vcModelID,
                                OI.numUnitHour AS QtyOrdered,
                                (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour), 0)) AS QtytoFulFilled,
                                (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour), 0)) AS QtytoFulFill,
								(CASE WHEN ISNULL(OI.numUnitHour,0) > 0 THEN ((OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) / OI.numUnitHour) * 100 ELSE 0 END) AS numRemainingPercent,
								(CASE WHEN ISNULL(OI.numUnitHour,0) > 0 THEN ((ISNULL(SUM(OBI.numUnitHour),0) * 100) / OI.numUnitHour) ELSE 0 END) AS numIncludedPercent,
                                ISNULL(numAllocation, 0) AS numAllocation,
                                ISNULL(numBackOrder, 0) AS numBackOrder,
                                CASE WHEN OBI.vcNotes IS NULL THEN OI.vcNotes ELSE OBI.vcNotes END AS vcNotes,
                                '' AS vcTrackingNo,
                                '' vcShippingMethod,
                                0 monShipCost,
                                GETDATE() dtDeliveryDate,
                                CONVERT(BIT, 1) AS Included,
                                ISNULL(OI.[numQtyShipped], 0) numQtyShipped,
                                ISNULL(OI.[numUnitHourReceived], 0) numQtyReceived,
                                I.[charItemType],
                                SUBSTRING(( SELECT  ',' + vcSerialNo
                                                    + CASE WHEN ISNULL(I.bitLotNo, 0) = 1 THEN ' (' + CONVERT(VARCHAR(15), oppI.numQty) + ')'
                                                            ELSE ''
                                                        END
                                            FROM    OppWarehouseSerializedItem oppI
                                                    JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                            WHERE   oppI.numOppID = @numOppID
                                                    AND oppI.numOppItemID = OI.numoppitemtCode
                                                    AND oppI.numOppBizDocsId = @numOppBizDocID
                                            ORDER BY vcSerialNo
                                            FOR
                                            XML PATH('')
                                            ), 2, 200000) AS SerialLotNo,
                                I.bitSerialized,
                                ISNULL(I.bitLotNo, 0) AS bitLotNo,
                                OI.numWarehouseItmsID,
                                ISNULL(I.numItemClassification, 0) AS numItemClassification,
                                I.vcSKU,
                                dbo.fn_GetAttributes(OI.numWarehouseItmsID,
                                                        I.bitSerialized) AS Attributes,
                                OM.bintCreatedDate AS dtRentalStartDate,
                                GETDATE() AS dtRentalReturnDate,
                                ISNULL(OI.numUOMId, 0) AS numUOM,
                                OI.monPrice AS monOppItemPrice,
                                ISNULL(OBI.monPrice, OI.monPrice) AS monOppBizDocPrice,
								OI.monTotAmount AS monOppBizDocAmount,
                                ISNULL(OI.vcItemDesc, '') AS [vcItemDesc],
								ISNULL(OI.bitDropShip,0) AS bitDropShip,
								ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) dtReleaseDate
                        FROM      OpportunityItems OI
                                LEFT JOIN OpportunityMaster OM ON OI.numOppID = oM.numOppID
                                LEFT JOIN OpportunityBizDocs OB ON OB.numOppId = OI.numOppId
                                                                    AND OB.numBizDocId = @numBizDocID
                                LEFT JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocId
                                                                        AND OBI.numOppItemID = OI.numoppitemtCode
                                LEFT JOIN WareHouseItems WI ON WI.numWareHouseItemID = OI.numWarehouseItmsID
                                INNER JOIN Item I ON I.[numItemCode] = OI.[numItemCode]
                        WHERE     OI.numOppId = @numOppID
                                AND OI.numoppitemtCode NOT IN (
                                SELECT  OBDI.numOppItemID
                                FROM    OpportunityBizDocs OBD
                                        JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId = OBDI.numOppBizDocId
                                WHERE   OBD.numOppId = @numOppID )
                        GROUP BY  OI.numoppitemtCode,
                                OI.numItemCode,
                                OI.vcItemName,
                                OI.vcModelID,
								OI.vcNotes,
								OBI.vcNotes,
                                OI.numUnitHour,
                                numAllocation,
                                numBackOrder,
                                OI.numQtyShipped,
                                OI.[numUnitHourReceived],
                                I.[charItemType],
                                I.bitSerialized,
                                I.bitLotNo,
                                OI.numWarehouseItmsID,
                                I.numItemClassification,
                                I.vcSKU,
                                OI.numWarehouseItmsID,
                                OBI.dtRentalStartDate,
                                OBI.dtRentalReturnDate,
                                OI.numUOMId,
                                OM.bintCreatedDate,
                                OI.monPrice,
								OI.monTotAmount,
                                OBI.monPrice,
                                OI.vcItemDesc,OI.bitDropShip,OI.ItemReleaseDate,OM.dtReleaseDate
                    ) X
            WHERE   X.QtytoFulFill > 0
        END
        ELSE 
        BEGIN
            SELECT  *
            FROM    ( SELECT    OI.numoppitemtCode,
                                OI.numItemCode,
                                OI.vcItemName,
                                OI.vcModelID,
                                OI.numUnitHour AS QtyOrdered,
								CASE 
									WHEN @numBizDocId = 296 
									THEN
										(CASE 
											WHEN (I.charItemType = 'p' AND ISNULL(OI.bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
											THEN 
												(CASE WHEN (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) <= dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList) THEN (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) ELSE dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList) END)
											ELSE (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0))
										END)
									ELSE
										(OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0))
								END AS QtytoFulFilled,
                                CASE 
									WHEN @numBizDocId = 296 
									THEN
										(CASE 
											WHEN (I.charItemType = 'p' AND ISNULL(OI.bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
											THEN 
												(CASE WHEN (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) <= dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList) THEN (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) ELSE dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList) END)
											ELSE (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0))
										END)
									ELSE
										(OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0))
								END AS QtytoFulFill,
								(CASE WHEN ISNULL(OI.numUnitHour,0) > 0 THEN ((OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) / OI.numUnitHour) * 100 ELSE 0 END) AS numRemainingPercent,
								(CASE WHEN ISNULL(OI.numUnitHour,0) > 0 THEN ((ISNULL(SUM(OBI.numUnitHour),0) * 100) / OI.numUnitHour) ELSE 0 END) AS numIncludedPercent,
                                ISNULL(numAllocation, 0) AS numAllocation,
                                ISNULL(numBackOrder, 0) AS numBackOrder,
                                OI.vcNotes AS vcNotes,
                                '' AS vcTrackingNo,
                                '' vcShippingMethod,
                                0 monShipCost,
                                GETDATE() dtDeliveryDate,
                                CONVERT(BIT, 1) AS Included,
                                ISNULL(OI.[numQtyShipped], 0) numQtyShipped,
                                ISNULL(OI.[numUnitHourReceived], 0) numQtyReceived,
                                I.[charItemType],
                                SUBSTRING(( SELECT  ',' + vcSerialNo
                                                    + CASE WHEN ISNULL(I.bitLotNo, 0) = 1 THEN ' (' + CONVERT(VARCHAR(15), oppI.numQty) + ')'
                                                            ELSE ''
                                                        END
                                            FROM    OppWarehouseSerializedItem oppI
                                                    JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                            WHERE   oppI.numOppID = @numOppID
                                                    AND oppI.numOppItemID = OI.numoppitemtCode
                                                    AND oppI.numOppBizDocsId = @numOppBizDocID
                                            ORDER BY vcSerialNo
                                            FOR
                                            XML PATH('')
                                            ), 2, 200000) AS SerialLotNo,
                                I.bitSerialized,
                                ISNULL(I.bitLotNo, 0) AS bitLotNo,
                                OI.numWarehouseItmsID,
                                ISNULL(I.numItemClassification, 0) AS numItemClassification,
                                I.vcSKU,
                                dbo.fn_GetAttributes(OI.numWarehouseItmsID,
                                                        I.bitSerialized) AS Attributes,
                                NULL AS dtRentalStartDate,
                                NULL AS dtRentalReturnDate,
                                ISNULL(OI.numUOMId, 0) AS numUOM,
                                OI.monPrice AS monOppItemPrice,
                                OI.monPrice AS monOppBizDocPrice,
								OI.monTotAmount AS monOppBizDocAmount,
                                ISNULL(OI.vcItemDesc, '') AS [vcItemDesc],
								ISNULL(OI.bitDropShip,0) AS bitDropShip,
								ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) dtReleaseDate
                        FROM      OpportunityItems OI
                                LEFT JOIN OpportunityMaster OM ON OI.numOppID = oM.numOppID 
                                LEFT JOIN 
									OpportunityBizDocItems OBI 
								ON 
									OBI.numOppItemID = OI.numoppitemtCode 
                                    AND OBI.numOppBizDocId IN ( SELECT  
																	numOppBizDocsId
                                                                FROM    
																	OpportunityBizDocs OB
                                                                WHERE   
																	OB.numOppId = OI.numOppId
                                                                    AND 1 = (CASE 
																				WHEN @numBizDocId = @numSalesAuthoritativeBizDocID --Invoice OR Deferred Income
																				THEN 
																					(CASE WHEN numBizDocId = 304 OR (ISNULL(OB.bitAuthoritativeBizDocs,0) = 1 AND ISNULL(OB.numDeferredBizDocID,0) = 0) THEN 1 ELSE 0 END)
																				WHEN  @numBizDocId = 304
																				THEN (CASE WHEN ISNULL(OB.bitAuthoritativeBizDocs,0) = 1 THEN 1 ELSE 0 END)
																				ELSE 
																					(CASE WHEN numBizDocId = @numBizDocId THEN 1 ELSE 0 END) 
																				END) 
																)
                                LEFT JOIN WareHouseItems WI ON WI.numWareHouseItemID = OI.numWarehouseItmsID
                                INNER JOIN Item I ON I.[numItemCode] = OI.[numItemCode]
                        WHERE     OI.numOppId = @numOppID
                        GROUP BY  OI.numoppitemtCode,
                                OI.numItemCode,
                                OI.vcItemName,
                                OI.vcModelID,
								OI.vcNotes,
                                OI.numUnitHour,
                                numAllocation,
                                numBackOrder,
                                OI.numQtyShipped,
                                OI.[numUnitHourReceived],
                                I.[charItemType],
                                I.bitSerialized,
                                I.bitLotNo,
								I.bitAsset,
								I.bitKitParent,
                                OI.numWarehouseItmsID,
                                I.numItemClassification,
                                I.vcSKU,
                                OI.numWarehouseItmsID,
                                OI.numUOMId,
                                OI.monPrice,
								OI.monTotAmount,
                                OI.vcItemDesc,OI.bitDropShip,OI.ItemReleaseDate,OM.dtReleaseDate
                    ) X
            WHERE   X.QtytoFulFill > 0
        END
    ELSE 
        IF @bitRentalBizDoc = 1 
        BEGIN
            SELECT  OI.numoppitemtCode,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.numUnitHour AS QtyOrdered,
                    OBI.numUnitHour AS QtytoFulFilled,
                    ( OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour), 0) ) AS QtytoFulFill,
					(CASE WHEN ISNULL(OI.numUnitHour,0) > 0 THEN ((OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) / OI.numUnitHour) * 100 ELSE 0 END) AS numRemainingPercent,
					(CASE WHEN ISNULL(OI.numUnitHour,0) > 0 THEN ((ISNULL(SUM(OBI.numUnitHour),0) * 100) / OI.numUnitHour) ELSE 0 END) AS numIncludedPercent,
                    ISNULL(numAllocation, 0) AS numAllocation,
                    ISNULL(numBackOrder, 0) AS numBackOrder,
                    CASE WHEN OBI.vcNotes IS NULL THEN OI.vcNotes ELSE OBI.vcNotes END AS vcNotes,
                    ISNULL(OBI.vcTrackingNo, '') AS vcTrackingNo,
                    OBI.vcShippingMethod,
                    OBI.monShipCost,
                    OBI.dtDeliveryDate,
                    CONVERT(BIT, 1) AS Included,
                    ISNULL(OI.[numQtyShipped], 0) numQtyShipped,
                    ISNULL(OI.[numUnitHourReceived], 0) numQtyReceived,
                    I.[charItemType],
                    SUBSTRING(( SELECT  ',' + vcSerialNo
                                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                                THEN ' ('
                                                    + CONVERT(VARCHAR(15), oppI.numQty)
                                                    + ')'
                                                ELSE ''
                                            END
                                FROM    OppWarehouseSerializedItem oppI
                                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                WHERE   oppI.numOppID = @numOppID
                                        AND oppI.numOppItemID = OI.numoppitemtCode
                                        AND oppI.numOppBizDocsId = @numOppBizDocID
                                ORDER BY vcSerialNo
                                FOR
                                XML PATH('')
                                ), 2, 200000) AS SerialLotNo,
                    I.bitSerialized,
                    ISNULL(I.bitLotNo, 0) AS bitLotNo,
                    OI.numWarehouseItmsID,
                    ISNULL(I.numItemClassification, 0) AS numItemClassification,
                    I.vcSKU,
                    dbo.fn_GetAttributes(OI.numWarehouseItmsID,
                                            I.bitSerialized) AS Attributes,
                    OBI.dtRentalStartDate,
                    OBI.dtRentalReturnDate,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    OI.monPrice AS monOppItemPrice,
                    ISNULL(OBI.monPrice, OI.monPrice) AS monOppBizDocPrice,
					OI.monTotAmount AS monOppBizDocAmount,
                    ISNULL(OI.vcItemDesc, '') AS [vcItemDesc],
					ISNULL(OI.bitDropShip,0) AS bitDropShip,
					ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) dtReleaseDate
            FROM    OpportunityItems OI
                    LEFT JOIN OpportunityMaster OM ON OI.numOppID = oM.numOppID
                    JOIN OpportunityBizDocs OB ON OB.numOppId = OI.numOppId
                    JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocId
                                                        AND OBI.numOppItemID = OI.numoppitemtCode
                    LEFT JOIN WareHouseItems WI ON WI.numWareHouseItemID = OI.numWarehouseItmsID
                    INNER JOIN Item I ON I.[numItemCode] = OI.[numItemCode]
            WHERE   OI.numOppId = @numOppID
                    AND OB.numOppBizDocsId = @numOppBizDocID
            GROUP BY OI.numoppitemtCode,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcModelID,
					OI.vcNotes,
                    OI.numUnitHour,
                    numAllocation,
                    numBackOrder,
                    OBI.vcNotes,
                    OBI.vcTrackingNo,
                    OBI.vcShippingMethod,
                    OBI.monShipCost,
                    OBI.dtDeliveryDate,
                    OBI.numUnitHour,
                    OI.[numQtyShipped],
                    OI.[numUnitHourReceived],
                    I.[charItemType],
                    I.bitSerialized,
                    I.bitLotNo,
                    OI.numWarehouseItmsID,
                    I.numItemClassification,
                    I.vcSKU,
                    OI.numWarehouseItmsID,
                    OBI.dtRentalStartDate,
                    OBI.dtRentalReturnDate,
                    OI.numUOMId,
                    OI.monPrice,
					OI.monTotAmount,
                    OBI.monPrice,
                    OI.vcItemDesc,OI.bitDropShip,OI.ItemReleaseDate,OM.dtReleaseDate
        END
        ELSE 
        BEGIN
            SELECT  OI.numoppitemtCode,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.numUnitHour AS QtyOrdered,
                    OBI.numUnitHour AS QtytoFulFilled,
                    (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour), 0) - ISNULL(TEMPDeferredIncomeItem.numUnitHour,0)) AS QtytoFulFill,
					(CASE WHEN ISNULL(OI.numUnitHour,0) > 0 THEN ((OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) / OI.numUnitHour) * 100 ELSE 0 END) AS numRemainingPercent,
					(CASE WHEN ISNULL(OI.numUnitHour,0) > 0 THEN ((ISNULL(SUM(OBI.numUnitHour),0) * 100) / OI.numUnitHour) ELSE 0 END) AS numIncludedPercent,
                    ISNULL(numAllocation, 0) AS numAllocation,
                    ISNULL(numBackOrder, 0) AS numBackOrder,
                    CASE WHEN OBI.vcNotes IS NULL THEN OI.vcNotes ELSE OBI.vcNotes END AS vcNotes,
                    ISNULL(OBI.vcTrackingNo, '') AS vcTrackingNo,
                    OBI.vcShippingMethod,
                    OBI.monShipCost,
                    OBI.dtDeliveryDate,
                    CONVERT(BIT, 1) AS Included,
                    ISNULL(OI.[numQtyShipped], 0) numQtyShipped,
                    ISNULL(OI.[numUnitHourReceived], 0) numQtyReceived,
                    I.[charItemType],
                    SUBSTRING(( SELECT  ',' + vcSerialNo
                                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                                THEN ' ('
                                                    + CONVERT(VARCHAR(15), oppI.numQty)
                                                    + ')'
                                                ELSE ''
                                            END
                                FROM    OppWarehouseSerializedItem oppI
                                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                WHERE   oppI.numOppID = @numOppID
                                        AND oppI.numOppItemID = OI.numoppitemtCode
                                        AND oppI.numOppBizDocsId = @numOppBizDocID
                                ORDER BY vcSerialNo
                                FOR
                                XML PATH('')
                                ), 2, 200000) AS SerialLotNo,
                    I.bitSerialized,
                    ISNULL(I.bitLotNo, 0) AS bitLotNo,
                    OI.numWarehouseItmsID,
                    ISNULL(I.numItemClassification, 0) AS numItemClassification,
                    I.vcSKU,
                    dbo.fn_GetAttributes(OI.numWarehouseItmsID,
                                            I.bitSerialized) AS Attributes,
                    OBI.dtRentalStartDate,
                    OBI.dtRentalReturnDate,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    OI.monPrice AS monOppItemPrice,
                    ISNULL(OBI.monPrice, OI.monPrice) AS monOppBizDocPrice,
					OI.monTotAmount AS monOppBizDocAmount,
                    ISNULL(OI.vcItemDesc, '') AS [vcItemDesc],
					ISNULL(OI.bitDropShip,0) AS bitDropShip,
					ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) dtReleaseDate
            FROM    OpportunityItems OI
                    LEFT JOIN OpportunityMaster OM ON OI.numOppID = oM.numOppID
                    JOIN OpportunityBizDocs OB ON OB.numOppId = OI.numOppId
                    JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocId
                                                        AND OBI.numOppItemID = OI.numoppitemtCode
                    LEFT JOIN WareHouseItems WI ON WI.numWareHouseItemID = OI.numWarehouseItmsID
                    INNER JOIN Item I ON I.[numItemCode] = OI.[numItemCode]
					OUTER APPLY
					(
						SELECT
							SUM(OpportunityBizDocItems.numUnitHour) AS numUnitHour
						FROM 
							OpportunityBizDocs
						INNER JOIN
							OpportunityBizDocItems 
						ON
							OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
						WHERE
							numOppId = @numOppID
							AND numOppBizDocID <> @numOppBizDocID
							AND	1 = (CASE 
									WHEN @numBizDocID = 304 --DEFERRED BIZDOC OR INVOICE
									THEN (CASE WHEN (numBizDocId = @numSalesAuthoritativeBizDocID OR numBizDocId=304) THEN 1 ELSE 0 END) -- WHEN DEFERRED BIZDOC(304) IS BEING ADDED SUBTRACT ITEMS ADDED TO INVOICE AND OTHER DEFERRED BIZDOC IN QtytoFulFill
									ELSE (CASE WHEN numBizDocId = @numBizDocID THEN 1 ELSE 0 END)
									END) 
							AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
					) TEMPDeferredIncomeItem
            WHERE   OI.numOppId = @numOppID
                    AND OB.numOppBizDocsId = @numOppBizDocID
            GROUP BY OI.numoppitemtCode,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcModelID,
					OI.vcNotes,
                    OI.numUnitHour,
                    numAllocation,
                    numBackOrder,
                    OBI.vcNotes,
                    OBI.vcTrackingNo,
                    OBI.vcShippingMethod,
                    OBI.monShipCost,
                    OBI.dtDeliveryDate,
                    OBI.numUnitHour,
                    OI.[numQtyShipped],
                    OI.[numUnitHourReceived],
                    I.[charItemType],
                    I.bitSerialized,
                    I.bitLotNo,
                    OI.numWarehouseItmsID,
                    I.numItemClassification,
                    I.vcSKU,
                    OI.numWarehouseItmsID,
                    OBI.dtRentalStartDate,
                    OBI.dtRentalReturnDate,
                    OI.numUOMId,
                    OI.monPrice,
					OI.monTotAmount,
                    OBI.monPrice,
                    OI.vcItemDesc,OI.bitDropShip,OI.ItemReleaseDate,OM.dtReleaseDate,
					TEMPDeferredIncomeItem.numUnitHour
            UNION
            SELECT  *
            FROM    ( SELECT    OI.numoppitemtCode,
                                OI.numItemCode,
                                OI.vcItemName,
                                OI.vcModelID,
                                OI.numUnitHour AS QtyOrdered,
                                (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) AS QtytoFulFilled,
                                (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour), 0) ) AS QtytoFulFill,
								(CASE WHEN ISNULL(OI.numUnitHour,0) > 0 THEN ((OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) / OI.numUnitHour) * 100 ELSE 0 END) AS numRemainingPercent,
								(CASE WHEN ISNULL(OI.numUnitHour,0) > 0 THEN ((ISNULL(SUM(OBI.numUnitHour),0) * 100) / OI.numUnitHour) ELSE 0 END) AS numIncludedPercent,
                                ISNULL(numAllocation, 0) AS numAllocation,
                                ISNULL(numBackOrder, 0) AS numBackOrder,
                                CASE WHEN OBI.vcNotes IS NULL THEN OI.vcNotes ELSE OBI.vcNotes END AS vcNotes,
                                '' AS vcTrackingNo,
                                '' vcShippingMethod,
                                1 AS monShipCost,
                                NULL dtDeliveryDate,
                                CONVERT(BIT, 0) AS Included,
                                ISNULL(OI.[numQtyShipped], 0) numQtyShipped,
                                ISNULL(OI.[numUnitHourReceived], 0) numQtyReceived,
                                I.[charItemType],
                                SUBSTRING(( SELECT  ',' + vcSerialNo
                                                    + CASE WHEN ISNULL(I.bitLotNo, 0) = 1 THEN ' (' + CONVERT(VARCHAR(15), oppI.numQty) + ')'
                                                            ELSE ''
                                                        END
                                            FROM    OppWarehouseSerializedItem oppI
                                                    JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                            WHERE   oppI.numOppID = @numOppID
                                                    AND oppI.numOppItemID = OI.numoppitemtCode
                                                    AND oppI.numOppBizDocsId = @numOppBizDocID
                                            ORDER BY vcSerialNo
                                            FOR
                                            XML PATH('')
                                            ), 2, 200000) AS SerialLotNo,
                                I.bitSerialized,
                                ISNULL(I.bitLotNo, 0) AS bitLotNo,
                                OI.numWarehouseItmsID,
                                ISNULL(I.numItemClassification, 0) AS numItemClassification,
                                I.vcSKU,
                                dbo.fn_GetAttributes(OI.numWarehouseItmsID,
                                                        I.bitSerialized) AS Attributes,
                                OBI.dtRentalStartDate,
                                OBI.dtRentalReturnDate,
                                ISNULL(OI.numUOMId, 0) AS numUOM,
                                OI.monPrice AS monOppItemPrice,
                                ISNULL(OBI.monPrice, OI.monPrice) AS monOppBizDocPrice,
								OI.monTotAmount AS monOppBizDocAmount,
                                ISNULL(OI.vcItemDesc, '') AS [vcItemDesc],
								ISNULL(OI.bitDropShip,0) AS bitDropShip,
								ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) dtReleaseDate
                        FROM      OpportunityItems OI
                                LEFT JOIN OpportunityMaster OM ON OI.numOppID = oM.numOppID
                                LEFT JOIN OpportunityBizDocs OB ON OB.numOppId = OI.numOppId
                                                                    AND OB.numBizDocId = @numBizDocID
                                LEFT JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocId
                                                                        AND OBI.numOppItemID = OI.numoppitemtCode
                                LEFT JOIN WareHouseItems WI ON WI.numWareHouseItemID = OI.numWarehouseItmsID
                                INNER JOIN Item I ON I.[numItemCode] = OI.[numItemCode]
                        WHERE     OI.numOppId = @numOppID
                                AND OI.numoppitemtCode NOT IN (
                                    SELECT  numOppItemID
                                    FROM    OpportunityBizDocItems OBI2
                                            JOIN OpportunityBizDocs OB2 ON OBI2.numOppBizDocID = OB2.numOppBizDocsId
                                    WHERE   numOppID = @numOppID
                                            AND numBizDocId = @numBizDocID )
                        GROUP BY  OI.numoppitemtCode,
                                OI.numItemCode,
                                OI.vcItemName,
                                OI.vcModelID,
								OI.vcNotes,
								OBI.vcNotes,
                                OI.numUnitHour,
                                numAllocation,
                                numBackOrder,
                                OBI.numUnitHour,
                                OI.[numQtyShipped],
                                OI.[numUnitHourReceived],
                                I.[charItemType],
                                I.bitSerialized,
                                I.bitLotNo,
                                OI.numWarehouseItmsID,
                                I.numItemClassification,
                                I.vcSKU,
                                OI.numWarehouseItmsID,
                                OBI.dtRentalStartDate,
                                OBI.dtRentalReturnDate,
                                OI.numUOMId,
                                OI.monPrice,
								OI.monTotAmount,
                                OBI.monPrice,
                                OI.vcItemDesc,OI.bitDropShip,OI.ItemReleaseDate,OM.dtReleaseDate
                    ) X
            WHERE   X.QtytoFulFill > 0
        END
END


GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetOppItemsForPickPackShip' ) 
    DROP PROCEDURE USP_GetOppItemsForPickPackShip
GO
CREATE PROCEDURE USP_GetOppItemsForPickPackShip
    @numDomainID AS NUMERIC(18,0),
    @numOppID AS NUMERIC(18,0),
	@numBizDocID AS NUMERIC(18,0)
AS 
BEGIN
	IF @numBizDocID = 29397 -- Packing Slip
	BEGIN
		SELECT    
			OI.numoppitemtCode,
			OI.numUnitHour AS QtyOrdered,
			OI.numUnitHour - ISNULL(SUM(OBIPackingSlip.numUnitHour),0) AS QtytoFulFillOriginal,
			(OI.numUnitHour - (CASE WHEN ISNULL(OBDFulfillment.numUnitHour,0) > 0 THEN ISNULL(OBDFulfillment.numUnitHour,0) ELSE ISNULL(SUM(OBIPackingSlip.numUnitHour),0) END)) AS QtytoFulFill,
			CONCAT('<ul class="list-unstyled">',STUFF((SELECT 
														CONCAT('<li><a onclick="return OpenBizInvoice(''',OB.numOppId,''',''',OB.numOppBizDocsId,''',0)" href="#">',OB.vcBizDocID,' (',OBI.numUnitHour,')</a></li>') 
													FROM 
														OpportunityBizDocs OB
													INNER JOIN
														OpportunityBizDocItems OBI
													ON
														OB.numOppBizDocsId=OBI.numOppBizDocID
													WHERE 
														OB.numOppId = @numOppID
														AND OB.numBizDocId = @numBizDocID
														AND OBI.numOppItemID=OI.numoppitemtCode
														AND ISNULL(OBI.numUnitHour,0) <> 0
													ORDER BY
														OB.numOppBizDocsId
													FOR XML PATH(''), TYPE).value('.', 'varchar(max)'),1,0,''),'</ul>') vcPickLists
		FROM 
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		LEFT JOIN
			OpportunityBizDocs OBDPackingSlip
		ON
			OM.numOppId = OBDPackingSlip.numOppId
			AND OBDPackingSlip.numBizDocId = @numBizDocID
		LEFT JOIN
			OpportunityBizDocItems OBIPackingSlip
		ON
			OBDPackingSlip.numOppBizDocsId = OBIPackingSlip.numOppBizDocID
			AND OBIPackingSlip.numOppItemID = OI.numoppitemtCode
		OUTER APPLY
		(
			SELECT
				SUM(numUnitHour) AS numUnitHour
			FROM
				OpportunityBizDocs
			LEFT JOIN
				OpportunityBizDocItems 
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
			WHERE
				OpportunityBizDocs.numOppId = OM.numOppId
				AND OpportunityBizDocs.numBizDocId = 296
		) OBDFulfillment
		WHERE     
			OI.numOppId = @numOppID
		GROUP BY
			OI.numoppitemtCode
			,OI.numUnitHour
			,OBDFulfillment.numUnitHour
	END
	ELSE IF @numBizDocID = 296 -- Fulfillment
	BEGIN
		DECLARE @tintCommitAllocation AS TINYINT
		DECLARE @bitAllocateInventoryOnPickList AS BIT

		SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
		SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM OpportunityMaster INNER JOIN DivisionMaster ON OpportunityMaster.numDivisionId=DivisionMaster.numDivisionID WHERE numOppId=@numOppID

		SELECT    
			OI.numoppitemtCode,
			OI.numUnitHour AS QtyOrdered,
			ISNULL(CASE 
				WHEN @numBizDocId = 296 
				THEN
					(CASE 
						WHEN (I.charItemType = 'p' AND ISNULL(OI.bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
						THEN 
							(CASE 
								WHEN (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) <= dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList) 
								THEN (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) 
								ELSE dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList) 
							END)
						ELSE (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0))
					END)
				ELSE
					(OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0))
			END,0) AS QtytoFulFillOriginal,
			(OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) QtyToFulfillWithoutAllocationCheck,
			ISNULL((CASE 
				WHEN ISNULL(SUM(OBI.numUnitHour),0) >= ISNULL(OBDPackingSlip.numUnitHour,0) 
				THEN (CASE 
						WHEN (I.charItemType = 'p' AND ISNULL(OI.bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
						THEN 
							(CASE 
								WHEN (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) <= dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList) 
								THEN (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) 
								ELSE dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList)
							END)
						ELSE (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0))
					END) 
				ELSE (CASE 
						WHEN (I.charItemType = 'p' AND ISNULL(OI.bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
						THEN 
							(CASE 
								WHEN (ISNULL(OBDPackingSlip.numUnitHour,0) - ISNULL(SUM(OBI.numUnitHour),0)) <= dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList)
								THEN (ISNULL(OBDPackingSlip.numUnitHour,0) - ISNULL(SUM(OBI.numUnitHour),0)) 
								ELSE dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList)
							END)
						ELSE ISNULL(OBDPackingSlip.numUnitHour,0) - ISNULL(SUM(OBI.numUnitHour),0)
					END) 
			END),0) AS QtytoFulFill,
			CONCAT('<ul class="list-unstyled">',STUFF((SELECT 
														CONCAT('<li><a onclick="return OpenBizInvoice(''',OB.numOppId,''',''',OB.numOppBizDocsId,''',0)" href="#">',OB.vcBizDocID,' (',OBI.numUnitHour,')</a></li>') 
													FROM 
														OpportunityBizDocs OB
													INNER JOIN
														OpportunityBizDocItems OBI
													ON
														OB.numOppBizDocsId=OBI.numOppBizDocID
													WHERE 
														OB.numOppId = @numOppID
														AND OB.numBizDocId = @numBizDocID
														AND OBI.numOppItemID=OI.numoppitemtCode
														AND ISNULL(OBI.numUnitHour,0) <> 0
													ORDER BY
														OB.numOppBizDocsId
													FOR XML PATH(''), TYPE).value('.', 'varchar(max)'),1,0,''),'</ul>') vcFulfillmentBizDocs
		FROM 
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		LEFT JOIN
			OpportunityBizDocs OBD
		ON
			OM.numOppId = OBD.numOppId
			AND OBD.numBizDocId = @numBizDocID
		LEFT JOIN
			OpportunityBizDocItems OBI
		ON
			OBD.numOppBizDocsId = OBI.numOppBizDocID
			AND OBI.numOppItemID = OI.numoppitemtCode
		OUTER APPLY
		(
			SELECT
				SUM(numUnitHour) AS numUnitHour
			FROM
				OpportunityBizDocs
			LEFT JOIN
				OpportunityBizDocItems 
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
			WHERE
				OpportunityBizDocs.numOppId = OM.numOppId
				AND OpportunityBizDocs.numBizDocId = 29397
		) OBDPackingSlip
		WHERE     
			OI.numOppId = @numOppID
		GROUP BY
			OI.numoppitemtCode
			,OI.numUnitHour
			,OI.numWarehouseItmsID
			,OI.bitDropShip
			,I.charItemType
			,I.bitAsset
			,I.bitKitParent
			,OBDPackingSlip.numUnitHour
	END
	ELSE IF @numBizDocID = 287 -- Invoice
	BEGIN
		SELECT    
			OI.numoppitemtCode,
			OI.numUnitHour AS QtyOrdered,
			OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0) AS QtytoFulFillOriginal,
			ISNULL((CASE 
				WHEN ISNULL(SUM(OBI.numUnitHour),0) >= ISNULL(OBDFulfillment.numUnitHour,0) 
				THEN OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0) 
				ELSE ISNULL(OBDFulfillment.numUnitHour,0) - ISNULL(SUM(OBI.numUnitHour),0) 
			END),0) AS QtytoFulFill,
			CONCAT('<ul class="list-unstyled">',STUFF((SELECT 
														CONCAT('<li><a onclick="return OpenBizInvoice(''',OB.numOppId,''',''',OB.numOppBizDocsId,''',0)" href="#">',OB.vcBizDocID,' (',OBI.numUnitHour,')</a></li>') 
													FROM 
														OpportunityBizDocs OB
													INNER JOIN
														OpportunityBizDocItems OBI
													ON
														OB.numOppBizDocsId=OBI.numOppBizDocID
													WHERE 
														OB.numOppId = @numOppID
														AND OB.numBizDocId = @numBizDocID
														AND OBI.numOppItemID=OI.numoppitemtCode
														AND ISNULL(OBI.numUnitHour,0) <> 0
													ORDER BY
														OB.numOppBizDocsId
													FOR XML PATH(''), TYPE).value('.', 'varchar(max)'),1,0,''),'</ul>') vcInvoices
		FROM 
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		LEFT JOIN
			OpportunityBizDocs OBD
		ON
			OM.numOppId = OBD.numOppId
			AND OBD.numBizDocId = @numBizDocID
		LEFT JOIN
			OpportunityBizDocItems OBI
		ON
			OBD.numOppBizDocsId = OBI.numOppBizDocID
			AND OBI.numOppItemID = OI.numoppitemtCode
		OUTER APPLY
		(
			SELECT
				SUM(numUnitHour) AS numUnitHour
			FROM
				OpportunityBizDocs
			LEFT JOIN
				OpportunityBizDocItems 
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
			WHERE
				OpportunityBizDocs.numOppId = OM.numOppId
				AND OpportunityBizDocs.numBizDocId = 296
		) OBDFulfillment
		WHERE     
			OI.numOppId = @numOppID
		GROUP BY
			OI.numoppitemtCode
			,OI.numUnitHour
			,OBDFulfillment.numUnitHour
	END
END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetOrderItems' ) 
    DROP PROCEDURE USP_GetOrderItems
GO
CREATE PROCEDURE USP_GetOrderItems ( @tintMode TINYINT = 0,
                                     @numOppItemID NUMERIC(9) = 0,
                                     @numOppID NUMERIC(9) = 0,
                                     @numDomainId NUMERIC(9),
                                     @ClientTimeZoneOffset INT,
									 @vcOppItemIDs VARCHAR(2000)='' )
AS 
BEGIN
	
    IF @tintMode = 1 --Add Return grid data & For Recurring Get Original Units to split
        BEGIN
            SELECT  OI.numoppitemtCode,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcItemDesc,
					OI.vcNotes as txtNotes,ISNULL(OI.numCost,0) AS numCost,
                    OI.numUnitHour numOriginalUnitHour,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					OI.monPrice
            FROM    dbo.OpportunityMaster OM
                    INNER JOIN dbo.OpportunityItems OI ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
            WHERE   OM.numDomainId = @numDomainId
                    AND OM.numOppId = @numOppID
                    
        END
 
    IF @tintMode = 2 -- Get single item based on PK, or all items
		-- Used by Sales/Purchase Template Load items in New Sales Order
        BEGIN 
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
                    OI.numItemCode,
					OI.bitItemPriceApprovalRequired,
                    OI.numUnitHour numOriginalUnitHour,
                    OI.monPrice,
                    OI.monTotAmount,
                    OI.numSourceID,
					ISNULL(OI.numCost,0) AS numCost,
                    OI.vcItemDesc,
					OI.vcNotes as txtNotes,
                    OI.numWarehouseItmsID,
                    OI.vcType,
                    OI.vcAttributes,
                    OI.bitDropShip,
                    ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,
                    OI.bitDiscountType,
                    OI.fltDiscount,
                    OI.monTotAmtBefDiscount,
                    ISNULL(OI.numQtyShipped,0) AS numQtyShipped,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcPathForTImage,
                    OI.vcManufacturer,
                    OI.monVendorCost,
                    ISNULL(OI.numUOMId, 0) AS numUOMId,
                    ISNULL(OI.numUOMId, 0) numUOM, -- used from add edit order
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL) AS UOMConversionFactor,
                    OI.bitWorkOrder,
                    isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
                    OI.numShipmentMethod,
                    OI.numSOVendorId,
                    OI.numProjectID,
                    OI.numClassID,
                    OI.numProjectStageID,
                    OI.numToWarehouseItemID,
                    WItems.numWarehouseID,
                    W.vcWareHouse AS Warehouse,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,
					isnull(OI.numProjectID,0) as numProjectID,
					ISNULL(dbo.fn_GetUOMName(I.numBaseUnit),'') AS vcBaseUOMName,
					OI.bitDropShip AS bitAllowDropShip,
					I.numVendorID,
					ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.txtItemDesc,'') txtItemDesc,
					ISNULL(I.vcModelID,'') vcModelID,
					ISNULL(I.vcManufacturer,'') vcManufacturer,
					ISNULL(I.numBarCodeId,'') numBarCodeId,
					ISNULL(I.bitSerialized,0) bitSerialized,
					ISNULL(I.bitKitParent,0) bitKitParent,
					ISNULL(I.bitAssembly,0) bitAssembly,
					ISNULL(I.IsArchieve,0) IsArchieve,
					ISNULL(I.bitLotNo,0) bitLotNo,
					ISNULL(WItems.monWListPrice,0) monWListPrice,
					ISNULL(I.bitTaxable, 0) bitTaxable,
					ISNULL(I.numItemClassification,0) AS numItemClassification,
                    I.charItemType,
                    I.bitKitParent,
					I.bitAssembly,
					I.bitSerialized,
					ISNULL(I.fltWeight,0) AS [fltWeight],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					CASE WHEN I.numItemGroup > 0 AND ISNULL(I.bitMatrix,0)=0 THEN ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) ELSE ISNULL(I.vcSKU,'') END AS [vcSKU],
					(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END)  AS monAverageCost,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder,
					ISNULL(OI.vcAttrValues,'') AS vcAttrValues,
					ISNULL(OI.numSortOrder,0) numSortOrder
					,(CASE WHEN OM.tintOppType=2 THEN ISNULL(vcNotes,'') ELSE '' END) AS vcVendorNotes
					,CPN.CustomerPartNo
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
					INNER JOIN DivisionMaster DM ON OM.numDivisionId = DM.numDivisionID
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
                    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
					LEFT JOIN CustomerPartNumber CPN ON CPN.numItemCode = I.numItemCode 
						AND CPN.numCompanyId=DM.numCompanyID
            WHERE   OI.numOppId = @numOppID
                    AND ( OI.numoppitemtCode = @numOppItemID
                          OR @numOppItemID = 0
                        )
                    AND OM.numDomainId = @numDomainId
                    
        END
 
    IF @tintMode = 3 -- Get item for SO to PO and vice versa creation
        BEGIN
            DECLARE @tintOppType AS TINYINT
			SELECT @tintOppType=tintOppType FROM OpportunityMaster WHERE numOppId=@numOppID

			IF @tintOppType = 1
			BEGIN
				DECLARE @TempItems TABLE
				(
					ID INT IDENTITY(1,1),numItemCode NUMERIC(18,0),vcItemName VARCHAR(300),txtNotes Varchar(MAX),charItemType char,vcModelID VARCHAR(100),vcItemDesc TEXT,vcType VARCHAR(20),vcPathForTImage VARCHAR(200),
					numWarehouseItmsID NUMERIC(18,0),vcAttributes VARCHAR(500),bitDropShip BIT,numUnitHour FLOAT,numUOM NUMERIC(18,0),vcUOMName VARCHAR(100),UOMConversionFactor DECIMAL(18,5),
					monPrice DECIMAL(20,5),numVendorID NUMERIC(18,0),numCost decimal(30, 16),vcPartNo VARCHAR(300),monVendorCost DECIMAL(20,5),numSOVendorId NUMERIC(18,0),numClassID NUMERIC(18,0),numProjectID NUMERIC(18,0),tintLevel TINYINT, vcAttrValues VARCHAR(500),
					ItemRequiredDate DATETIME
				)

				INSERT INTO 
					@TempItems
				SELECT
					OI.numItemCode,OI.vcItemName,OI.vcNotes as txtNotes,I.charItemType,OI.vcModelID,OI.vcItemDesc,OI.vcType,ISNULL(OI.vcPathForTImage, ''),ISNULL(OI.numWarehouseItmsID,0),ISNULL(OI.vcAttributes, ''),
					ISNULL(OI.bitDropShip, 0),OI.numUnitHour * dbo.fn_UOMConversion(I.numBaseUnit, OI.numItemCode,OM.numDomainId, OI.numUOMId),ISNULL(OI.numUOMId, 0),ISNULL(U.vcUnitName, ''),dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,OM.numDomainId, NULL) AS UOMConversionFactor,
					OI.monPrice,ISNULL(I.numVendorID, 0),ISNULL(OI.numCost,0),ISNULL(V.vcPartNo, ''),ISNULL(V.monCost, 0),ISNULL(OI.numSOVendorId, 0),0,0,0,ISNULL(vcAttrValues,'')
					,(CASE WHEN ISNULL(OI.ItemReleaseDate,'') = '' THEN
							 ISNULL(OM.dtReleaseDate,'') 
						ELSE OI.ItemReleaseDate 
						END )
				FROM    
					dbo.OpportunityItems OI
					INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
					INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
					LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
					LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND 1 = (CASE WHEN (SELECT COUNT(*) FROM OpportunityLinking OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numChildOppID=OIInner.numOppId WHERE OMInner.numParentOppID = @numOppID AND OIInner.numItemCode=OI.numItemCode AND ISNULL(OIInner.numWarehouseItmsID,0)=ISNULL(OI.numWarehouseItmsID,0)) > 0 THEN 0 ELSE 1 END)
				ORDER BY
					OI.numSortOrder


				--INSERT KIT CHILD ITEMS WHICH ARE NOT KIT AND NOT YET ADDED
				INSERT INTO 
					@TempItems
				SELECT
					OKI.numChildItemID,'' as txtNotes,I.vcItemName,I.charItemType,I.vcModelID,I.txtItemDesc,(CASE WHEN I.charitemType = 'P' THEN 'Product' WHEN I.charitemType = 'S' THEN 'Service' END),
					ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault=1),''),ISNULL(OKI.numWareHouseItemId,0),
					ISNULL(dbo.USP_GetAttributes(OKI.numWarehouseItemID, bitSerialized),''),0,OKI.numQtyItemsReq,ISNULL(OKI.numUOMId, 0),ISNULL(U.vcUnitName, ''),
					dbo.fn_UOMConversion(OKI.numUOMId, OKI.numChildItemID,OM.numDomainId, NULL),
					(CASE WHEN I.charitemType = 'P' THEN WI.monWListPrice ELSE I.monListPrice END),
					ISNULL(I.numVendorID, 0),ISNULL(t1.numCost,0),ISNULL(V.vcPartNo, ''),ISNULL(V.monCost, 0),0,0,0,1,'',
					t1.ItemRequiredDate
				FROM    
					dbo.OpportunityKitItems OKI
				INNER JOIN @TempItems t1 ON OKI.numChildItemID = t1.numItemCode AND ISNULL(OKI.numWareHouseItemId,0) = ISNULL(t1.numWarehouseItmsID,0)
				LEFT JOIN WarehouseItems WI ON OKI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKI.numChildItemID
				LEFT JOIN UOM U ON U.numUOMId = OKI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OKI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t1.ID IS NULL


				-- FOR KIT CHILD ITEMS WHICH ARE ALREADY ADDED AT PARENT LEVEL - UPDATE QTY ONLY
				UPDATE 
					t2
				SET 
					t2.numUnitHour = ISNULL(t2.numUnitHour,0) + ISNULL(OKI.numQtyItemsReq,0),
					t2.monPrice = (CASE 
									WHEN ISNULL(t2.monPrice,0) < (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)  
									THEN t2.monPrice
									ELSE (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)
									END)
				FROM
					@TempItems t2
				INNER JOIN
					OpportunityKitItems OKI 
				ON
					OKI.numChildItemID = t2.numItemCode AND ISNULL(OKI.numWareHouseItemId,0) = ISNULL(t2.numWarehouseItmsID,0)
				LEFT JOIN WarehouseItems WI ON OKI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKI.numChildItemID
				LEFT JOIN UOM U ON U.numUOMId = OKI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OKI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t2.tintLevel <> 1

				--INSERT SUB KIT CHILD ITEMS WHICH ARE NOT KIT AND NOT YET ADDED
				INSERT INTO 
					@TempItems
				SELECT
					OKCI.numItemID,'' as txtNotes,I.vcItemName,I.charItemType,I.vcModelID,I.txtItemDesc,(CASE WHEN I.charitemType = 'P' THEN 'Product' WHEN I.charitemType = 'S' THEN 'Service' END),
					ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault=1),''),ISNULL(OKCI.numWareHouseItemId,0),
					ISNULL(dbo.USP_GetAttributes(OKCI.numWarehouseItemID, bitSerialized),''),0,OKCI.numQtyItemsReq,ISNULL(OKCI.numUOMId, 0),ISNULL(U.vcUnitName, ''),
					dbo.fn_UOMConversion(OKCI.numUOMId, OKCI.numItemID,OM.numDomainId, NULL),
					(CASE WHEN I.charitemType = 'P' THEN WI.monWListPrice ELSE I.monListPrice END),
					ISNULL(I.numVendorID, 0),ISNULL(t1.numCost,0),ISNULL(V.vcPartNo, ''),ISNULL(V.monCost, 0),0,0,0,2,'',
					t1.ItemRequiredDate
				FROM    
					dbo.OpportunityKitChildItems OKCI
				INNER JOIN @TempItems t1 ON OKCI.numItemID = t1.numItemCode AND ISNULL(OKCI.numWareHouseItemId,0) = ISNULL(t1.numWarehouseItmsID,0)
				LEFT JOIN WarehouseItems WI ON OKCI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKCI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKCI.numItemID
				LEFT JOIN UOM U ON U.numUOMId = OKCI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OKCI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t1.ID IS NULL

				-- FOR SUB KIT CHILD ITEMS WHICH ARE ALREADY ADDED AT PARENT LEVEL - UPDATE QTY ONLY
				UPDATE 
					t2
				SET 
					t2.numUnitHour = ISNULL(t2.numUnitHour,0) + ISNULL(OKCI.numQtyItemsReq,0),
					t2.monPrice = (CASE 
									WHEN ISNULL(t2.monPrice,0) < (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)  
									THEN t2.monPrice
									ELSE (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)
									END)
				FROM
					@TempItems t2
				INNER JOIN
					OpportunityKitChildItems OKCI 
				ON
					OKCI.numItemID = t2.numItemCode AND ISNULL(OKCI.numWareHouseItemId,0) = ISNULL(t2.numWarehouseItmsID,0)
				LEFT JOIN WarehouseItems WI ON OKCI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKCI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKCI.numItemID
				LEFT JOIN UOM U ON U.numUOMId = OKCI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OKCI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t2.tintLevel <> 2

				SELECT * FROM @TempItems
			END
			ELSE
			BEGIN
				SELECT  OI.numoppitemtCode,
                    OI.numOppId,
					OI.vcNotes as txtNotes,
					OI.bitItemPriceApprovalRequired,
                    OI.vcItemDesc,
                    ISNULL(OI.vcPathForTImage, '') vcPathForTImage,
                    ISNULL(OI.numWarehouseItmsID, 0) numWarehouseItmsID,
                    OI.vcItemName,
                    OI.vcModelID,
					ISNULL(OI.numCost,0) numCost,
                    ISNULL(OI.vcAttributes, '') vcAttributes,
                    OI.vcType,
                    ISNULL(OI.bitDropShip, 0) bitDropShip,
                    OI.numItemCode,
                    OI.numUnitHour numOriginalUnitHour,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL) AS UOMConversionFactor,
                    ISNULL(I.numVendorID, 0) numVendorID,
                    ISNULL(V.monCost, 0) monVendorCost,
                    ISNULL(V.vcPartNo, '') vcPartNo,
                    OI.monPrice,
                    ISNULL(OI.numSOVendorId, 0) numSOVendorId,
                    ISNULL(I.charItemType, 'N') charItemType,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL((SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numWarehouseItemID = OI.numWarehouseItmsID ),ISNULL(I.vcSKU,'')) AS [vcSKU],
					ISNULL(vcAttrValues,'') AS vcAttrValues,
					(CASE WHEN ISNULL(OI.ItemReleaseDate,'') = '' THEN
							 ISNULL(OM.dtReleaseDate,'') 
						ELSE  OI.ItemReleaseDate
						END ) AS ItemRequiredDate
				FROM    dbo.OpportunityItems OI
						INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
						INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
						LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
						LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID
												AND I.numItemCode = V.numItemCode
				WHERE   OI.numOppId = @numOppID
						AND OM.numDomainId = @numDomainId
			END
        END
        IF @tintMode = 4 -- Add Edit Order items
        BEGIN 
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
					OI.vcNotes as txtNotes,
                    OI.numItemCode,
					OI.bitItemPriceApprovalRequired,
					OI.numUnitHour AS numOrigUnitHour,
                    OI.numUnitHour,
                    OI.monPrice,
                    OI.monTotAmount,
                    OI.numSourceID,
                    OI.vcItemDesc,
					OI.vcNotes,
					ISNULL(OI.numCost,0) numCost,
                    OI.numWarehouseItmsID,
                    OI.vcType ItemType,
                    OI.vcAttributes Attributes,
                    OI.bitDropShip DropShip,
                    OI.numUnitHourReceived,
                    OI.bitDiscountType,
                    OI.fltDiscount,
                    OI.monTotAmtBefDiscount,
					ISNULL(OI.monTotAmtBefDiscount,0) - ISNULL(OI.monTotAmount,0) AS TotalDiscountAmount,
                    OI.numQtyShipped,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcPathForTImage,
                    OI.vcManufacturer,
                    OI.monVendorCost,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    ISNULL(dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL),1) AS UOMConversionFactor,
                    OI.bitWorkOrder,
                    isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
                    OI.numShipmentMethod,
                    OI.numSOVendorId,
                    OI.numProjectStageID,
                    OI.numToWarehouseItemID,
                   UPPER(I.charItemType) charItemType,
                    WItems.numWarehouseID,
                    W.vcWareHouse AS Warehouse,
                    0 as Op_Flag,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,
					CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON ob.numOppBizDocsId=obi.numOppBizDocID
					AND (ob.bitAuthoritativeBizDocs=1 OR ob.numBizDocId=304) AND obi.numOppItemID=oi.numoppitemtCode AND ob.numOppId=OM.numOppId)>0 THEN 1 ELSE 0 END  AS bitIsAuthBizDoc,
					ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(numQtyShipped,0) AS numQtyShipped,ISNULL(dbo.fn_GetUOMName(I.numBaseUnit),'') AS vcBaseUOMName,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) AS [vcSKU],
					(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) AS monAverageCost,
					ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.txtItemDesc,'') txtItemDesc,
					ISNULL(I.numBarCodeId,'') numBarCodeId,
					ISNULL(I.bitSerialized,0) bitSerialized,
					ISNULL(I.bitKitParent,0) bitKitParent,
					ISNULL(I.bitAssembly,0) bitAssembly,
					ISNULL(I.IsArchieve,0) IsArchieve,
					ISNULL(I.bitLotNo,0) bitLotNo,
					ISNULL(WItems.monWListPrice,0) monWListPrice,
					ISNULL(I.bitTaxable, 0) bitTaxable,
					ISNULL(I.numItemClassification,0) AS numItemClassification,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder,
					case when ISNULL(I.bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode,WItems.numWareHouseID) else 0 end AS numMaxWorkOrderQty,
					ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) AS numSerialNoAssigned,
					(CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON OB.numOppBizDocsId=OBI.numOppBizDocID WHERE OB.numOppId = OI.numOppId AND OBI.numOppItemID=OI.numoppitemtCode AND OB.numBizDocId=296) > 0 THEN 1 ELSE 0 END) AS bitAddedFulFillmentBizDoc,
					ISNULL(OI.vcAttrValues,'') AS AttributeIDs,
					ISNULL(I.numContainer,0) AS numContainer
					,ISNULL(I.numNoItemIntoContainer,0) AS numContainerQty
					,ISNULL(numPromotionID,0) AS numPromotionID,
					ISNULL(bitPromotionTriggered,0) AS bitPromotionTriggered,
					ISNULL(vcPromotionDetail,'') AS vcPromotionDetail,
					CASE WHEN ISNULL(OI.numSortOrder,0)=0 THEN row_number() OVER (ORDER BY OI.numOppId) ELSE ISNULL(OI.numSortOrder,0) END AS numSortOrder
					,ISNULL(vcNotes,'') AS vcVendorNotes
					,ISNULL(CPN.CustomerPartNo,'') CustomerPartNo
					,OI.ItemRequiredDate AS ItemRequiredDate
					,ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) ItemReleaseDate
					,(CASE WHEN ISNULL(bitMarkupDiscount,0) = 0 THEN '0' ELSE '1' END) AS bitMarkupDiscount
					,ISNULL(vcChildKitSelectedItems,'') KitChildItems
					,(CASE 
						WHEN ISNULL(I.bitKitParent,0) = 1 
						THEN 
							(CASE 
								WHEN EXISTS (SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID=IInner.numItemCode WHERE ID.numItemKitID=I.numItemCode AND ISNULL(IInner.bitKitParent,0)=1)
								THEN 1
								ELSE 0
							END)
						ELSE 0 
					END) bitHasKitAsChild
					,ISNULL(I.bitFreeShipping,0) IsFreeShipping
					,(CASE 
						WHEN ISNULL(I.bitKitParent,0)=1
								AND (CASE 
										WHEN EXISTS (SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID=IInner.numItemCode WHERE ID.numItemKitID=I.numItemCode AND ISNULL(IInner.bitKitParent,0)=1)
										THEN 1
										ELSE 0
									END) = 0
						THEN 
							dbo.GetKitWeightBasedOnItemSelection(I.numItemCode,OI.vcChildKitSelectedItems)
						ELSE 
							ISNULL(I.fltWeight,0) 
					END) fltItemWeight
					,ISNULL(I.fltWidth,0) fltItemWidth
					,ISNULL(I.fltHeight,0) fltItemHeight
					,ISNULL(I.fltLength,0) fltItemLength
					,ISNULL(OI.numWOQty,0) numWOQty
					,ISNULL(I.bitSOWorkOrder,0) bitAlwaysCreateWO
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
					INNER JOIN DivisionMaster DM ON OM.numDivisionId=DM.numDivisionID
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
                    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
					LEFT JOIN CustomerPartNumber CPN ON CPN.numItemCode = I.numItemCode AND CPN.numCompanyId=DM.numCompanyID
            WHERE   OI.numOppId = @numOppID
                    AND OM.numDomainId = @numDomainId
			ORDER BY OI.numSortOrder
                    
			SELECT  
				vcSerialNo,
				vcComments AS Comments,
				numOppItemID AS numoppitemtCode,
				W.numWarehouseItmsDTLID,
				numWarehouseItmsID AS numWItmsID,
				dbo.Fn_getattributes(W.numWarehouseItmsDTLID, 1) Attributes
			FROM    
				WareHouseItmsDTL W
			JOIN 
				OppWarehouseSerializedItem O 
			ON 
				O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
			WHERE   
				numOppID = @numOppID         
        END
        
        
		IF @tintMode = 5 -- Get WebApi Order Items to enter WebApi Order line Item Details 
		BEGIN 
			SELECT OI.numOppItemtCode,OI.numOppId,OI.numItemCode,OI.vcNotes as txtNotes,ISNULL(OI.numCost,0) numCost,
			ISNULL((SELECT vcWHSKU FROM dbo.WareHouseItems WHERE WareHouseItems.numWarehouseItemID = OI.numWarehouseItmsID),ISNULL(IT.vcSKU,'')) AS [vcSKU],
			OI.vcItemDesc,OMAPI.vcAPIOppId
			FROM OpportunityItems OI 
			INNER JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId 
			INNER JOIN dbo.OpportunityMasterAPI OMAPI ON OM.numOppId = OMAPI.numOppId
			INNER JOIN dbo.Item IT ON OI.numItemCode = IT.numItemCode
			WHERE OI.numOppId = @numOppID and OM.numDomainID = @numDomainId 
        END

	IF @tintMode = 6 -- Add Edit Order items
        BEGIN 
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
					OI.vcNotes as txtNotes,
                    OI.numItemCode,
					OI.bitItemPriceApprovalRequired,
					OI.numUnitHour AS numOrigUnitHour,
                    (OI.numUnitHour-ISNULL((SELECT SUM(numUnitHour) FROM ReturnItems where 
					numReturnHeaderID IN (SELECT numReturnHeaderID FROM ReturnHeader 
					WHERE numOppId=@numOppID) AND numOppItemID=OI.numoppitemtCode),0))
					AS numUnitHour,
                    OI.monPrice,
                    OI.monTotAmount,
                    OI.numSourceID,
                    OI.vcItemDesc,
					OI.numCost,
                    OI.numWarehouseItmsID,
                    OI.vcType ItemType,
                    OI.vcAttributes Attributes,
                    OI.bitDropShip DropShip,
                    OI.numUnitHourReceived,
                    OI.bitDiscountType,
                    OI.fltDiscount,
                    OI.monTotAmtBefDiscount,
                    OI.numQtyShipped,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcPathForTImage,
                    OI.vcManufacturer,
                    OI.monVendorCost,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    ISNULL(dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL),1) AS UOMConversionFactor,
                    OI.bitWorkOrder,
                    isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
                    OI.numShipmentMethod,
                    OI.numSOVendorId,
                    OI.numProjectID,
                    OI.numClassID,
                    OI.numProjectStageID,
                    OI.numToWarehouseItemID,
                   UPPER(I.charItemType) charItemType,
                    WItems.numWarehouseID,
                    W.vcWareHouse AS Warehouse,
                    0 as Op_Flag,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,
					CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON ob.numOppBizDocsId=obi.numOppBizDocID
					AND (ob.bitAuthoritativeBizDocs=1 OR ob.numBizDocId=304) AND obi.numOppItemID=oi.numoppitemtCode AND ob.numOppId=OM.numOppId)>0 THEN 1 ELSE 0 END  AS bitIsAuthBizDoc,
					ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(numQtyShipped,0) AS numQtyShipped,ISNULL(dbo.fn_GetUOMName(I.numBaseUnit),'') AS vcBaseUOMName,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) AS [vcSKU],
					(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) AS monAverageCost,
					ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.txtItemDesc,'') txtItemDesc,
					ISNULL(I.vcModelID,'') vcModelID,
					ISNULL(I.vcManufacturer,'') vcManufacturer,
					ISNULL(I.numBarCodeId,'') numBarCodeId,
					ISNULL(I.bitSerialized,0) bitSerialized,
					ISNULL(I.bitKitParent,0) bitKitParent,
					ISNULL(I.bitAssembly,0) bitAssembly,
					ISNULL(I.IsArchieve,0) IsArchieve,
					ISNULL(I.bitLotNo,0) bitLotNo,
					ISNULL(WItems.monWListPrice,0) monWListPrice,
					ISNULL(I.bitTaxable, 0) bitTaxable,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder,
					case when ISNULL(I.bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode,WItems.numWareHouseID) else 0 end AS numMaxWorkOrderQty,
					ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) AS numSerialNoAssigned,
					(CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON OB.numOppBizDocsId=OBI.numOppBizDocID WHERE OB.numOppId = OI.numOppId AND OBI.numOppItemID=OI.numoppitemtCode AND OB.numBizDocId=296) > 0 THEN 1 ELSE 0 END) AS bitAddedFulFillmentBizDoc,
					ISNULL(OI.vcAttrValues,'') AS AttributeIDs
					,(CASE WHEN OM.tintOppType=2 THEN ISNULL(vcNotes,'') ELSE '' END) AS vcVendorNotes
					,(CASE WHEN ISNULL(bitMarkupDiscount,0) = 0 THEN '0' ELSE '1' END) AS bitMarkupDiscount 
--					0 AS [Tax0],0 AS bitTaxable0
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
                    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
            WHERE   OI.numOppId = @numOppID
                    AND OM.numDomainId = @numDomainId
					AND (OI.numUnitHour-ISNULL((SELECT SUM(numUnitHour) FROM ReturnItems where 
					numReturnHeaderID IN (SELECT numReturnHeaderID FROM ReturnHeader 
					WHERE numOppId=@numOppID) AND numOppItemID=OI.numoppitemtCode),0))>0
                    
		SELECT  vcSerialNo,
				vcComments AS Comments,
				numOppItemID AS numoppitemtCode,
				W.numWarehouseItmsDTLID,
				numWarehouseItmsID AS numWItmsID,
				dbo.Fn_getattributes(W.numWarehouseItmsDTLID, 1) Attributes
		FROM    WareHouseItmsDTL W
				JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
		WHERE   numOppID = @numOppID AND numOppItemID IN (SELECT OI.numoppitemtCode FROM OpportunityItems OI WHERE OI.numOppId = @numOppID AND (OI.numUnitHour-ISNULL((SELECT SUM(numUnitHour) FROM ReturnItems where 
					numReturnHeaderID IN (SELECT numReturnHeaderID FROM ReturnHeader 
					WHERE numOppId=@numOppID) AND numOppItemID=OI.numoppitemtCode),0))>0)
					              
                   
END
END
/****** Object:  StoredProcedure [dbo].[USP_GetProjectTeamRights]    Script Date: 09/17/2010 17:35:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetProjectTeamRights')
DROP PROCEDURE USP_GetProjectTeamRights
GO
CREATE PROCEDURE [dbo].[USP_GetProjectTeamRights]
    @numDomainID AS NUMERIC(9) = 0,
    @numProId AS NUMERIC(9) = 0,
	@tintUserType as tinyint
AS 

IF @tintUserType=1 --Internal User
BEGIN	
 SELECT A.numContactID,A.numContactID AS numContactId,ISNULL(A.vcFirstName,'')+' '+ISNULL(A.vcLastName,'') as vcUserName
,isnull(TR.numRights,1) numRights,isnull(TR.numProjectRole,0) numProjectRole,A.vcEmail AS Email
 from UserMaster UM         
 join AdditionalContactsInformation A on UM.numUserDetailId=A.numContactID        
 left join ProjectTeamRights TR on TR.numContactId=A.numContactId and TR.numProId=@numProId and TR.tintUserType=1	        
 where UM.numDomainID=@numDomainID and UM.numDomainID=A.numDomainID    
 AND [bitActivateFlag]=1
END

Else IF @tintUserType=2 --External User
BEGIN	
 SELECT A.numContactID,A.numContactID AS numContactId,ISNULL(A.vcFirstName,'')+' '+ISNULL(A.vcLastName,'') as vcUserName
,isnull(TR.numRights,0) numRights,isnull(TR.numProjectRole,0) numProjectRole,
 isnull(C.vcCompanyName,'-') as vcCompanyName,A.vcEmail AS Email
 from ProjectTeamRights TR join AdditionalContactsInformation A on TR.numContactId=A.numContactId 
join DivisionMaster DM on DM.numDivisionID=A.numDivisionID
join CompanyInfo C on DM.numCompanyID=C.numCompanyID	        
 where A.numDomainID=@numDomainID and TR.numProId=@numProId and TR.tintUserType=2   
END


--Commented by chintan reason :Temperary change Bring it back when portal is ready for permission based projects
--Bug id - 386
--Union All Select 0,'Portal Contacts',isnull((select TR.numRights from 
--ProjectTeamRights TR where TR.numContactId=0 and TR.numProId=@numProId),0)

--UNION ALL
-- select  A.numContactID,A.vcFirstName +' '+ A.vcLastName + '(' + C.vcCompanyName +')'as vcUserName,
--isnull((select TR.numRights from 
--ProjectTeamRights TR where TR.numContactId=A.numContactId  and TR.numProId=@numProId),0) numRights  
--		from CommissionContacts CC JOIN DivisionMaster D ON CC.numDivisionID=D.numDivisionID
--		JOIN AdditionalContactsInformation A ON D.numDivisionID=A.numDivisionID
--		join CompanyInfo C on C.numCompanyID=D.numCompanyID  
--		where CC.numDomainID=@numDomainID 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetReceivePayment' ) 
    DROP PROCEDURE USP_GetReceivePayment
GO
CREATE PROCEDURE [dbo].USP_GetReceivePayment
    @numDomainId AS NUMERIC(9) = 0 ,
    @numDepositID NUMERIC = 0,
    @numDivisionID NUMERIC(9) = 0,
    @numCurrencyID numeric(9),
    @numAccountClass NUMERIC(18,0)=0,
	@numCurrentPage INT=1
AS 
BEGIN
	DECLARE @numPageSize INT = 40

	DECLARE @numDiscountItemID AS NUMERIC(18,0)
	DECLARE @numShippingServiceItemID AS NUMERIC(18,0)

	SELECT @numDiscountItemID = ISNULL(numDiscountServiceItemID,0),@numShippingServiceItemID=ISNULL(numShippingServiceItemID,0) FROM dbo.Domain WHERE numDomainID = @numDomainID
	PRINT @numDiscountItemID
			
	DECLARE @BaseCurrencySymbol 
	nVARCHAR(3);SET @BaseCurrencySymbol='$'
	SELECT @BaseCurrencySymbol = ISNULL(varCurrSymbol,'$') FROM dbo.Currency WHERE numCurrencyID IN ( SELECT numCurrencyID FROM dbo.Domain WHERE numDomainId=@numDomainId)
    
	SELECT [T].[numDivisionID],T.bitChild,dbo.[fn_GetComapnyName](T.[numDivisionID]) AS vcCompanyName,AD2.[vcCity]
	,isnull(dbo.fn_GetState(AD2.numState),'') AS vcState
	INTO #tempOrg
	FROM (SELECT @numDivisionID AS numDivisionID,cast(0 AS BIT) AS bitChild
	UNION
	SELECT [CA].[numDivisionID],CAST(1 AS BIT) AS bitChild
	FROM [dbo].[CompanyAssociations] AS CA 
	WHERE [CA].[numDomainID]=@numDomainID AND  ca.[numAssociateFromDivisionID]=@numDivisionID AND CA.[bitChildOrg]=1) AS T
	LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=@numDomainID 
	AND AD2.numRecordID= T.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppBizDocID NUMERIC(18,0)
	)

	INSERT INTO @TEMP
	(
		numOppBizDocID
	)
	SELECT 
		numOppBizDocsID
	FROM
	(
		SELECT
			OpportunityBizdocs.numOppBizDocsID
			,T.bitChild
			,OpportunityBizdocs.dtCreatedDate
		FROM    
			OpportunityMaster
		INNER JOIN 
			[#tempOrg] AS T 
		ON 
			OpportunityMaster.[numDivisionId] = T.[numDivisionID]
		INNER JOIN 
			OpportunityBizdocs
		ON
			OpportunityMaster.[numoppid] = OpportunityBizdocs.[numoppid]
		INNER JOIN
			DepositeDetails 
		ON 
			DepositeDetails.numOppBizDocsID = OpportunityBizdocs.numOppBizDocsID
		WHERE   
			OpportunityBizdocs.[bitauthoritativebizdocs] = 1
			AND OpportunityMaster.tintOppType = 1
			AND OpportunityMaster.numDomainID = @numDomainID
			AND DepositeDetails.numDepositID = @numDepositID
			AND (ISNULL(OpportunityMaster.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
		UNION
		SELECT
			numOppBizDocsID
			,T.bitChild
			,OpportunityBizdocs.dtCreatedDate
		FROM 
			OpportunityMaster
		INNER JOIN
			OpportunityBizdocs 
		ON 
			OpportunityMaster.[numoppid] = OpportunityBizdocs.[numoppid]
		INNER JOIN 
			[#tempOrg] AS T 
		ON 
			OpportunityMaster.[numDivisionId] = T.[numDivisionID]
		WHERE
			OpportunityMaster.numDomainID = @numDomainID
			AND OpportunityMaster.tintOppType = 1
			AND (OpportunityMaster.numCurrencyID = @numCurrencyID OR  @numCurrencyID = 0 )
			AND (ISNULL(OpportunityMaster.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
			AND OpportunityBizdocs.[bitauthoritativebizdocs] = 1
			AND (@numDepositID= 0 OR (@numDepositID > 0 AND T.bitChild = 0))
			AND OpportunityBizdocs.monDealAmount - OpportunityBizdocs.monAmountPaid > 0
			AND (SELECT COUNT(*) FROM DepositeDetails WHERE numOppID=OpportunityMaster.numOppId AND numOppBizDocsId=OpportunityBizdocs.numOppBizDocsID AND numDepositID=@numDepositID) = 0
			
	) TEMP
	ORDER BY 
		bitChild,dtCreatedDate DESC


	DECLARE @TEMPFinal TABLE
	(
		numOppBizDocID NUMERIC(18,0)
	)

	INSERT INTO @TEMPFinal SELECT numOppBizDocID FROM @TEMP ORDER BY ID OFFSET (@numCurrentPage - 1) * @numPageSize ROWS FETCH NEXT @numPageSize ROWS ONLY


	SELECT  
		om.vcPOppName ,
		om.numoppid ,
		OBD.[numoppbizdocsid] ,
		OBD.[vcbizdocid] ,
		OBD.[numbizdocstatus] ,
		OBD.[vccomments] ,
		OBD.monAmountPaid AS monAmountPaid ,
		OBD.monDealAmount ,
		OBD.monCreditAmount monCreditAmount ,
		ISNULL(C.varCurrSymbol, '') varCurrSymbol ,
		[dbo].[FormatedDateFromDate](dtFromDate, @numDomainID) dtFromDate ,
		dtFromDate AS [dtOrigFromDate],
		CASE ISNULL(om.bitBillingTerms, 0)
			WHEN 1
			--THEN [dbo].[FormatedDateFromDate](DATEADD(dd,CONVERT(INT, ISNULL(dbo.fn_GetListItemName(ISNULL(om.intBillingDays,0)), 0)), dtFromDate), 1)
			THEN [dbo].[FormatedDateFromDate](DATEADD(dd,CONVERT(INT, ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(om.intBillingDays,0)), 0)), dtFromDate), @numDomainID)
			WHEN 0
			THEN [dbo].[FormatedDateFromDate](OBD.[dtFromDate], @numDomainID)
		END AS dtDueDate ,
		om.numDivisionID ,
                
		(OBD.monDealAmount - ISNULL(OBD.monCreditAmount, 0) - ISNULL( OBD.monAmountPaid , 0) + ISNULL(DD.monAmountPaid,0)  ) AS baldue ,
		T.vcCompanyName ,T.vcCity,T.vcState,T.bitChild,
		DD.numDepositID ,
		DD.monAmountPaid monAmountPaidInDeposite ,
		ISNULL(DD.numDepositeDetailID,0) numDepositeDetailID,
		obd.dtCreatedDate,
		OM.numContactId,obd.vcRefOrderNo
		,OM.numCurrencyID
		,ISNULL(NULLIF(C.fltExchangeRate,0),1) fltExchangeRateCurrent
		,ISNULL(NULLIF(OM.fltExchangeRate,0),1) fltExchangeRateOfOrder
		,@BaseCurrencySymbol BaseCurrencySymbol,om.vcPOppName,
		CASE ISNULL(om.bitBillingTerms, 0)
			WHEN 1 THEN (SELECT numDiscount FROM dbo.BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainID)
			ELSE 0
		END AS [numDiscount],
		CASE ISNULL(om.bitBillingTerms, 0)
			WHEN 1 THEN (SELECT numDiscountPaidInDays FROM dbo.BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainID)
			ELSE 0
		END AS [numDiscountPaidInDays],
		CASE ISNULL(om.bitBillingTerms, 0)
			WHEN 1 THEN (SELECT numNetDueInDays FROM dbo.BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainID)
			ELSE 0
		END AS [numNetDueInDays],
		CASE WHEN (SELECT COUNT(*) FROM dbo.OpportunityBizDocItems OBDI
					JOIN dbo.OpportunityItems OI ON OI.numoppitemtCode = OBDI.numOppItemID
					WHERE 1=1 AND numOppId = om.[numoppid] AND numOppBizDocID = obd.numOppBizDocsId 
					AND OBDI.numItemCode = @numDiscountItemID
					AND OI.vcItemDesc LIKE '%Term Discount%') > 0 THEN 1 ELSE 0 END AS [IsDiscountItemAdded],
		(SELECT ISNULL(SUM(monTotAmount),0) FROM OpportunityBizDocItems WHERE numOppBizDocID=OBD.numOppBizDocsID AND numItemCode = @numShippingServiceItemID) AS monShippingCharge
		,ISNULL(numARAccountID,0) numARAccountID
	FROM    opportunitymaster om
			INNER JOIN [opportunitybizdocs] obd ON om.[numoppid] = OBD.[numoppid]
			INNER JOIN @TEMPFinal T1 ON obd.numOppBizDocsId = T1.numOppBizDocID
			INNER JOIN [#tempOrg] AS T ON OM.[numDivisionId] = T.[numDivisionID]
			LEFT OUTER JOIN [ListDetails] ld ON ld.[numListItemID] = OBD.[numbizdocstatus]
			LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = om.numCurrencyID
			JOIN DepositeDetails DD ON DD.numOppBizDocsID = OBD.numOppBizDocsID
	WHERE   
		om.numDomainID = @numDomainID
		AND om.tintOppType = 1
		AND OBD.[bitauthoritativebizdocs] = 1
		AND (ISNULL(OM.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
		AND DD.numDepositID = @numDepositID
	UNION
	SELECT  om.vcPOppName ,
			om.numoppid ,
			OBD.[numoppbizdocsid] ,
			OBD.[vcbizdocid] ,
			OBD.[numbizdocstatus] ,
			OBD.[vccomments] ,
			OBD.monAmountPaid AS monAmountPaid ,
			OBD.monDealAmount ,
			OBD.monCreditAmount monCreditAmount ,
			ISNULL(C.varCurrSymbol, '') varCurrSymbol ,
			[dbo].[FormatedDateFromDate](dtFromDate, @numDomainID) dtFromDate ,
			dtFromDate AS [dtOrigFromDate],
			CASE ISNULL(om.bitBillingTerms, 0)
				WHEN 1
				--THEN [dbo].[FormatedDateFromDate](DATEADD(dd,CONVERT(INT, ISNULL(dbo.fn_GetListItemName(ISNULL(om.intBillingDays,0)), 0)),dtFromDate), @numDomainID)
				THEN [dbo].[FormatedDateFromDate](DATEADD(dd,CONVERT(INT, ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(om.intBillingDays,0)), 0)),dtFromDate), @numDomainID)
				WHEN 0
				THEN [dbo].[FormatedDateFromDate](OBD.[dtFromDate], @numDomainID)
			END AS dtDueDate ,
			om.numDivisionID ,
			(OBD.monDealAmount - ISNULL(monCreditAmount, 0) - ISNULL( OBD.monAmountPaid , 0) ) AS baldue ,
			T.vcCompanyName ,T.vcCity,T.vcState,T.bitChild,
			0 numDepositID ,
			0 monAmountPaidInDeposite ,
			0 numDepositeDetailID,
			dtCreatedDate,
			OM.numContactId,obd.vcRefOrderNo
			,OM.numCurrencyID
			,ISNULL(NULLIF(C.fltExchangeRate,0),1) fltExchangeRateCurrent
			,ISNULL(NULLIF(OM.fltExchangeRate,0),1) fltExchangeRateOfOrder
			,@BaseCurrencySymbol BaseCurrencySymbol,om.vcPOppName,
			CASE ISNULL(om.bitBillingTerms, 0)
				WHEN 1 THEN (SELECT numDiscount FROM dbo.BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainID)
				ELSE 0
			END AS [numDiscount],
			CASE ISNULL(om.bitBillingTerms, 0)
				WHEN 1 THEN (SELECT numDiscountPaidInDays FROM dbo.BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainID)
				ELSE 0
			END AS [numDiscountPaidInDays],
			CASE ISNULL(om.bitBillingTerms, 0)
				WHEN 1 THEN (SELECT numNetDueInDays FROM dbo.BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainID)
				ELSE 0
			END AS [numNetDueInDays],
			CASE WHEN (SELECT COUNT(*) FROM dbo.OpportunityBizDocItems OBDI
						JOIN dbo.OpportunityItems OI ON OI.numoppitemtCode = OBDI.numOppItemID
						WHERE 1=1 AND numOppId = om.[numoppid] AND numOppBizDocID = obd.numOppBizDocsId 
						AND OBDI.numItemCode = @numDiscountItemID
						AND OI.vcItemDesc LIKE '%Term Discount%') > 0 THEN 1 ELSE 0 END AS [IsDiscountItemAdded],
			(SELECT ISNULL(SUM(monTotAmount),0) FROM OpportunityBizDocItems WHERE numOppBizDocID=OBD.numOppBizDocsID AND numItemCode = @numShippingServiceItemID) AS monShippingCharge
			,ISNULL(numARAccountID,0) numARAccountID
	FROM    opportunitymaster om
			INNER JOIN [opportunitybizdocs] obd ON om.[numoppid] = OBD.[numoppid]
			INNER JOIN @TEMPFinal T1 ON obd.numOppBizDocsId = T1.numOppBizDocID
			INNER JOIN [#tempOrg] AS T ON OM.[numDivisionId] = T.[numDivisionID]
			LEFT OUTER JOIN [ListDetails] ld ON ld.[numListItemID] = OBD.[numbizdocstatus]
			LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = om.numCurrencyID
	WHERE   
		om.numDomainID = @numDomainID
		AND om.tintOppType = 1
		AND OBD.[bitauthoritativebizdocs] = 1
		AND (OM.numCurrencyID = @numCurrencyID OR  @numCurrencyID = 0 )
		AND (ISNULL(OM.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
		AND (@numDepositID= 0 OR (@numDepositID > 0 AND T.bitChild = 0))
		AND OBD.monDealAmount - OBD.monAmountPaid > 0
		AND (SELECT COUNT(*) FROM DepositeDetails WHERE numOppID=OM.numOppId AND numOppBizDocsId=OBD.numOppBizDocsID AND numDepositID=@numDepositID) = 0		
	ORDER BY 
		T.bitChild,dtCreatedDate DESC
	
	SELECT COUNT(*) AS TotalRecords,@numPageSize AS PageSize FROM @TEMP

	DROP TABLE #tempOrg
END


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetVendorShipmentMethod')
DROP PROCEDURE USP_GetVendorShipmentMethod
GO
CREATE PROCEDURE [dbo].[USP_GetVendorShipmentMethod]         
@numDomainID as numeric(9)=0,    
@numVendorid AS NUMERIC(9)=0,
@numAddressID AS NUMERIC(9)=0,
@numWarehouseID AS NUMERIC(18,0) = 0,
@tintMode AS TINYINT        
AS    
BEGIN
	IF @tintMode=1
	BEGIN 
		IF EXISTS (SELECT numShipmentMethod FROM VendorShipmentMethod VM WHERE VM.numDomainID=@numDomainID AND VM.numVendorid=@numVendorid AND ISNULL(numWarehouseID,0) = @numWarehouseID)
		BEGIN
			SELECT 
				LD.numListItemID
				,LD.vcData AS ShipmentMethod
				,ISNULL(VM.numListValue,0) AS numListValue
				,VM.bitPrimary
				,ISNULL(VM.bitPreferredMethod,0) bitPreferredMethod
			FROM 
				ListDetails LD 
			LEFT JOIN 
				VendorShipmentMethod VM 
			ON 
				VM.numDomainID=@numDomainID
				AND VM.numVendorid=@numVendorid
				AND VM.numAddressID=@numAddressID 
				AND VM.numListItemID=LD.numListItemID
				AND VM.numWarehouseID=@numWarehouseID
			WHERE 
				LD.numListID=338 
				AND (LD.numDomainID=@numDomainID OR Ld.constFlag=1)
		END
		ELSE
		BEGIN
			SELECT 
				LD.numListItemID
				,LD.vcData AS ShipmentMethod
				,ISNULL(VM.numListValue,0) AS numListValue
				,VM.bitPrimary
				,ISNULL(VM.bitPreferredMethod,0) bitPreferredMethod
			FROM 
				ListDetails LD 
			LEFT JOIN 
				VendorShipmentMethod VM 
			ON 
				VM.numDomainID=@numDomainID
				AND VM.numVendorid=@numVendorid
				AND VM.numAddressID=@numAddressID 
				AND VM.numListItemID=LD.numListItemID
				AND ISNULL(VM.numWarehouseID,0)=0
			WHERE 
				LD.numListID=338 
				AND (LD.numDomainID=@numDomainID OR Ld.constFlag=1)
		END
	END
	ELSE IF @tintMode=2
	BEGIN 
		SELECT LD.numListItemID,LD.vcData AS ShipmentMethod,ISNULL(VM.numListValue,0) AS numListValue,VM.bitPrimary,
		cast(LD.numListItemID AS VARCHAR(10)) + '~' + cast(ISNULL(VM.numListValue,0) AS VARCHAR(10)) AS vcListValue
			from ListDetails LD JOIN VendorShipmentMethod VM ON VM.numListItemID=LD.numListItemID 
		AND VM.numVendorid=@numVendorid AND VM.numAddressID=@numAddressID AND VM.numDomainID=@numDomainID
		WHERE LD.numListID=338 AND (LD.numDomainID=@numDomainID or Ld.constFlag=1) ORDER BY ISNULL(bitPrimary,0) DESC, ISNULL(bitPreferredMethod,0) DESC
	END
END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetWorkFlowQueue' ) 
    DROP PROCEDURE USP_GetWorkFlowQueue
GO
CREATE PROCEDURE USP_GetWorkFlowQueue
AS 
BEGIN
	--IF Workflow is not active or deleted
	UPDATE 
		 WFQ
	SET 
		WFQ.tintProcessStatus=5
	FROM
		WorkFlowQueue WFQ
	LEFT JOIN	
		WorkFlowMaster WF 
	ON
		WFQ.numDomainID=WF.numDomainID
		AND WFQ.numFormID=WF.numFormID
		AND WF.tintWFTriggerOn=WFQ.tintWFTriggerOn
		AND WF.bitActive=1  
	WHERE 
		WF.numWFID IS NULL

	DELETE
		WFQ
	FROM
		WorkFlowQueue WFQ
	INNER JOIN
		WorkFlowMaster WF
	ON
		WFQ.numWFID = WF.numWFID
	WHERE
		ISNULL(WF.vcDateField,'') <> '' AND intDays > 0
			   
	--Select top 25 WorkFlow which are Pending Execution			  	
	SELECT TOP 25 
		WorkFlowQueue.[numWFQueueID]
		,WorkFlowQueue.[numDomainID]
		,WorkFlowQueue.[numRecordID]
		,WorkFlowQueue.[numFormID]
		,WorkFlowQueue.[tintProcessStatus]
		,WorkFlowQueue.[tintWFTriggerOn]
		,WorkFlowQueue.[numWFID]
	FROM 
		WorkFlowQueue 
	INNER JOIN 
		WorkFlowMaster 
	ON 
		WorkFlowQueue.numWFID=WorkFlowMaster.numWFID and WorkFlowQueue.numFormID=WorkFlowMaster.numFormID
		AND WorkFlowQueue.numDomainID=WorkFlowMaster.numDomainID AND WorkFlowMaster.tintWFTriggerOn=WorkFlowQueue.tintWFTriggerOn
	WHERE 
		WorkFlowQueue.tintProcessStatus IN (1) 
		AND WorkFlowMaster.bitActive=1 
	ORDER BY 
		numWFQueueID
END



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_InLineEditDataSave')
DROP PROCEDURE USP_InLineEditDataSave
GO
CREATE PROCEDURE [dbo].[USP_InLineEditDataSave] 
( 
 @numDomainID NUMERIC(18,0),
 @numUserCntID NUMERIC(18,0),
 @numFormFieldId NUMERIC(9),
 @bitCustom AS BIT = 0,
 @InlineEditValue VARCHAR(8000),
 @numContactID NUMERIC(9),
 @numDivisionID NUMERIC(9),
 @numWOId NUMERIC(9),
 @numProId NUMERIC(9),
 @numOppId NUMERIC(9),
 @numRecId NUMERIC(9),
 @vcPageName NVARCHAR(100),
 @numCaseId NUMERIC(9),
 @numWODetailId NUMERIC(9),
 @numCommID NUMERIC(9)
 )
AS 

declare @strSql as nvarchar(4000)
DECLARE @vcDbColumnName AS VARCHAR(100),@vcOrigDbColumnName AS VARCHAR(100),@vcLookBackTableName AS varchar(100)
DECLARE @vcListItemType as VARCHAR(3),@numListID AS numeric(9),@vcAssociatedControlType varchar(20)
declare @tempAssignedTo as numeric(9);set @tempAssignedTo=null           
DECLARE @Value1 AS VARCHAR(18),@Value2 AS VARCHAR(18)

IF  @bitCustom =0
BEGIN
	SELECT @vcDbColumnName=vcDbColumnName,@vcLookBackTableName=vcLookBackTableName,
		   @vcListItemType=vcListItemType,@numListID=numListID,@vcAssociatedControlType=vcAssociatedControlType,
		   @vcOrigDbColumnName=vcOrigDbColumnName
	 FROM DycFieldMaster WHERE numFieldId=@numFormFieldId
	 
	IF @vcAssociatedControlType = 'DateField' AND  @InlineEditValue=''
	BEGIN
		SET @InlineEditValue = null
	END

	DECLARE @intExecuteDiv INT=0
	if @vcLookBackTableName = 'DivisionMaster' 
	 BEGIN 
		
	   IF @vcDbColumnName='numFollowUpStatus' --Add to FollowUpHistory
	    Begin
			IF(@numCommID>0 AND @numDivisionID=0)
			BEGIN
				SET @numDivisionID=(SELECT TOP 1 numDivisionId FROM Communication WHERE numCommId=@numCommID)
			END
			declare @numFollow as varchar(10),@binAdded as varchar(20),@PreFollow as varchar(10),@RowCount as varchar(2)                            
			                      
			set @PreFollow=(select count(*)  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID)                          
			set @RowCount=@@rowcount  
			                        
			select   @numFollow=numFollowUpStatus, @binAdded=bintModifiedDate from divisionmaster where numDivisionID=@numDivisionID and numDomainId= @numDomainID                    
			
			if @numFollow <>'0' and @numFollow <> @InlineEditValue                          
			begin                          
				if @PreFollow<>0                          
					begin                          
			                   
						if @numFollow <> (select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID order by numFollowUpStatusID desc)                          
							begin                          
			                      	insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
									values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
							end                          
					end                          
				else                          
					begin                          
						insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
						 values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
					end                          
			   end 
			   
			
			UPDATE  DivisionMaster SET  dtLastFollowUp=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID
		END
		 IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from DivisionMaster where numDivisionID = @numDivisionID and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update DivisionMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID where numDivisionID = @numDivisionID  and numDomainId= @numDomainID  
						UPDATE CompanyInfo SET bintModifiedDate=GETUTCDATE() WHERE numCompanyId IN (SELECT numCompanyId 
																									FROM 
																										DivisionMaster
																									WHERE 
																										numDivisionID = @numDivisionID  and numDomainId= @numDomainID)         
					end					
				else if  (@InlineEditValue ='0')          
					begin          
						update DivisionMaster set numAssignedTo=0 ,numAssignedBy=0 where numDivisionID = @numDivisionID  and numDomainId= @numDomainID
						UPDATE CompanyInfo SET bintModifiedDate=GETUTCDATE() WHERE numCompanyId IN (SELECT numCompanyId 
																									FROM 
																										DivisionMaster
																									WHERE 
																										numDivisionID = @numDivisionID  and numDomainId= @numDomainID)          
					end    
			END
	    UPDATE CompanyInfo SET numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() WHERE numCompanyId=(SELECT TOP 1 numCompanyId FROM DivisionMaster where numDivisionID=@numDivisionID and numDomainID=@numDomainID) and numDomainID=@numDomainID
		
		IF @vcDbColumnName='vcPartnerCode'
		BEGIN
			IF(@InlineEditValue<>'')
			BEGIN
				IF EXISTS(SELECT vcPartnerCode FROM DivisionMaster where numDivisionID<>@numDivisionID and numDomainID=@numDomainID AND vcPartnerCode=@InlineEditValue)
				BEGIN
					SET @intExecuteDiv=1
					SET @InlineEditValue='-#12'
				END
			END
		END
		IF @intExecuteDiv=0
		BEGIN
			SET @strSql='update DivisionMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID'
			
		END
	 END 
	 ELSE if @vcLookBackTableName = 'AdditionalContactsInformation' 
	 BEGIN 
			IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='True'
			BEGIN
				DECLARE @bitPrimaryContactCheck AS BIT             
				DECLARE @numID AS NUMERIC(9) 
				
				SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
					FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID 
                   
					IF @@rowcount = 0 SET @numID = 0             
				
					WHILE @numID > 0            
					BEGIN            
						IF @bitPrimaryContactCheck = 1 
							UPDATE  AdditionalContactsInformation SET bitPrimaryContact = 0 WHERE numContactID = @numID  
                              
						SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
						FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactID > @numID    
                                
						IF @@rowcount = 0 SET @numID = 0             
					END 
				END
				ELSE IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='False'
				BEGIN
					IF (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactId!=@numContactId AND ISNULL(bitPrimaryContact,0)=1) = 0
						SET @InlineEditValue='True'
				END 
				-- Added by Priya(to check no Campaign got engaged wothout contacts email
				ELSE IF @vcDbColumnName = 'numECampaignID' 
				BEGIN
					
					IF EXISTS (select 1 from AdditionalContactsInformation WHERE numContactId = @numContactID AND (vcEmail IS NULL OR vcEmail = ''))
					BEGIN
						RAISERROR('EMAIL_ADDRESS_REQUIRED',16,1)
								RETURN
					END

				END
				
		SET @strSql='update AdditionalContactsInformation set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID and numContactId=@numContactId'
	 END 
	 ELSE if @vcLookBackTableName = 'CompanyInfo' 
	 BEGIN 
		SET @strSql='update CompanyInfo set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCompanyId=(select numCompanyId from divisionmaster where numDivisionID=@numDivisionID and numDomainID=@numDomainID) and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'WorkOrder' 
	 BEGIN 
		DECLARE @numItemCode AS NUMERIC(9)
	 
		IF @numWODetailId>0 AND @vcDbColumnName <> 'numQtyItemsReq'
		BEGIN
			SET @strSql='update WorkOrderDetails set ' + @vcDbColumnName + '=@InlineEditValue where numWOId=@numWOId and numWODetailId=@numWODetailId' 
		END 	
		ELSE
		BEGIN
			SET @strSql='update WorkOrder set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numWOId=@numWOId and numDomainID=@numDomainID'
			
			IF @vcDbColumnName='numQtyItemsReq'
			BEGIN 
				DECLARE @numPickedQty FLOAT = 0
				
				SET @numPickedQty = ISNULL((SELECT
												MAX(numPickedQty)
											FROM
											(
												SELECT 
													WorkOrderDetails.numWODetailId
													,CEILING(SUM(WorkOrderPickedItems.numPickedQty)/numQtyItemsReq_Orig) numPickedQty
												FROM
													WorkOrderDetails
												INNER JOIN
													WorkOrderPickedItems
												ON
													WorkOrderDetails.numWODetailId=WorkOrderPickedItems.numWODetailId
												WHERE
													WorkOrderDetails.numWOId=@numWOID
													AND ISNULL(WorkOrderDetails.numWareHouseItemId,0) > 0
												GROUP BY
													WorkOrderDetails.numWODetailId
													,WorkOrderDetails.numQtyItemsReq_Orig
											) TEMP),0)

				IF @InlineEditValue < ISNULL((@numPickedQty),0)
				BEGIN
					RAISERROR('WORKORDR_QUANITY_CAN_NOT_BE_LESS_THEN_PICKED_QTY',16,1)
					RETURN
				END

				IF EXISTS (SELECT 
								SPDTTL.ID 
							FROM 
								StagePercentageDetailsTask SPDT
							INNER JOIN
								StagePercentageDetailsTaskTimeLog SPDTTL
							ON
								SPDT.numTaskId = SPDTTL.numTaskID 
							WHERE 
								SPDT.numDomainID=@numDomainID 
								AND SPDT.numWorkOrderId = @numWOId)
				BEGIN
					RAISERROR('TASKS_ARE_ALREADY_STARTED',16,1)
					RETURN
				END

				IF EXISTS (SELECT numWOId FROM WorkOrder WHERE numDomainID=@numDomainID AND numWOId=@numWOId AND numWOStatus=23184)
				BEGIN
					RAISERROR('WORKORDR_COMPLETED',16,1)
					RETURN
				END

				SELECT @numItemCode=numItemCode FROM WorkOrder WHERE numWOId=@numWOId and numDomainID=@numDomainID
			
				EXEC USP_UpdateWorkOrderDetailQuantity @numDomainID,@numUserCntID,@numItemCode,@InlineEditValue,@numWOId
			END 
		END	
	 END
	 ELSE if @vcLookBackTableName = 'ProjectsMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if Project is assigned to someone 
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from ProjectsMaster where numProId=@numProId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update ProjectsMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numProId = @numProId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update ProjectsMaster set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numProId = @numProId  and numDomainId= @numDomainID        
					end    
			END
		else IF @vcDbColumnName='numProjectStatus'  ---Updating All Statge to 100% if Completed
			BEGIN
				IF @InlineEditValue='27492'
				BEGIN
					UPDATE PM SET dtCompletionDate=GETUTCDATE(),monTotalExpense=PIE.monExpense,
					monTotalIncome=PIE.monIncome,monTotalGrossProfit=PIE.monBalance,numProjectStatus=27492
					FROM ProjectsMaster PM,dbo.GetProjectIncomeExpense(@numDomainId,@numProId) PIE
					WHERE PM.numProId=@numProId
					
					EXEC USP_ManageProjectCommission @numDomainId,@numProId
						
					Update StagePercentageDetails set tinProgressPercentage=100,bitclose=1 where numProjectID=@numProId

					EXEC dbo.USP_GetProjectTotalProgress
							@numDomainID = @numDomainID, --  numeric(9, 0)
							@numProId = @numProId, --  numeric(9, 0)
							@tintMode = 1 --  tinyint
				END			
				ELSE
				BEGIN
					DELETE from BizDocComission WHERE numDomainID = @numDomainID AND numProId=@numProId AND numComissionID 
						NOT IN (SELECT BC.numComissionID FROM dbo.BizDocComission BC JOIN dbo.PayrollTracking PT 
							ON BC.numComissionID = PT.numComissionID AND BC.numDomainId = PT.numDomainId WHERE BC.numDomainID = @numDomainID AND ISNULL(BC.numProId,0) = @numProId)
				END
			END
			

		SET @strSql='update ProjectsMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numProId=@numProId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'OpportunityMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from OpportunityMaster where numOppId=@numOppId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update OpportunityMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update OpportunityMaster set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID        
					end    
			END
		ELSE IF @vcDbColumnName='numPartner' OR @vcDbColumnName='numPartenerSource'
		 BEGIN
			update OpportunityMaster set numPartner=@InlineEditValue ,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID           
		 END
		
		ELSE IF @vcDbColumnName='tintActive'
		 BEGIN
		 	SET @InlineEditValue= CASE @InlineEditValue WHEN 'True' THEN 1 ELSE 0 END 
		 END
		 ELSE IF @vcDbColumnName='tintSource'
		 BEGIN
			IF CHARINDEX('~', @InlineEditValue)>0
			BEGIN
				SET @Value1=SUBSTRING(@InlineEditValue, 1,CHARINDEX('~', @InlineEditValue)-1) 
				SET @Value2=SUBSTRING(@InlineEditValue, CHARINDEX('~', @InlineEditValue)+1,LEN(@InlineEditValue)) 
			END	
			ELSE
			BEGIN
				SET @Value1=@InlineEditValue
				SET @Value2=0
			END
			
		 	update OpportunityMaster set tintSourceType=@Value2 where numOppId = @numOppId  and numDomainId= @numDomainID        
		 	
		 	SET @InlineEditValue= @Value1
		 END
		ELSE IF @vcDbColumnName='tintOppStatus' --Update Inventory  0=Open,1=Won,2=Lost
		 BEGIN			
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT
		
			IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainId=@numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1)
			BEGIN
				SET @bitViolatePrice = 1
			END
			SET @intPendingApprovePending=(SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppId AND ISNULL(bitItemPriceApprovalRequired,0)=1)

			IF(@intPendingApprovePending > 0 AND @bitViolatePrice=1 AND @InlineEditValue='1')
			BEGIN
				SET @intExecuteDiv=1
				SET @InlineEditValue='-#123'
			END

			IF ISNULL(@intExecuteDiv,0)=0
			BEGIN
				DECLARE @tintOppStatus AS TINYINT
				SELECT @tintOppStatus=tintOppStatus FROM OpportunityMaster WHERE numOppID=@numOppID              

				IF (@tintOppStatus=0 OR @tintOppStatus=2) AND @InlineEditValue='1' --Open to Won
				BEGIN
					SELECT @numDivisionID=numDivisionID FROM OpportunityMaster WHERE numOppID=@numOppID   
					
					DECLARE @tintCRMType AS TINYINT      
					SELECT @tintCRMType=tintCRMType FROM divisionmaster WHERE numDivisionID =@numDivisionID 

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN        
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=GETUTCDATE()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=GETUTCDATE()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN        
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=GETUTCDATE()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END    

		 			EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID

					UPDATE OpportunityMaster SET bintOppToOrder=GETUTCDATE() WHERE numOppId=@numOppId and numDomainID=@numDomainID
				END
				ELSE IF (@InlineEditValue='2') -- Win to Lost
				BEGIN
					UPDATE OpportunityMaster SET dtDealLost=GETUTCDATE() WHERE numOppId=@numOppId and numDomainID=@numDomainID
				END
				
				IF (@tintOppStatus=1 and @InlineEditValue='2') or (@tintOppStatus=1 and @InlineEditValue='0') --Won to Lost or Won to Open
						EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID 

				UPDATE OpportunityMaster SET tintOppStatus = @InlineEditValue WHERE numOppId=@numOppID
				
			END
		 END
		 ELSE IF @vcDbColumnName='vcOppRefOrderNo' --Update vcRefOrderNo OpportunityBizDocs
		 BEGIN
			 IF LEN(ISNULL(@InlineEditValue,''))>0
			 BEGIN
	   			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@InlineEditValue WHERE numOppId=@numOppID
			 END
		 END
		 ELSE IF @vcDbColumnName='numStatus' 
		 BEGIN
			DECLARE @tintOppType AS TINYINT
			select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType from OpportunityMaster where numOppID=@numOppID

			IF @tintOppType=1 AND @tintOppStatus=1
			BEGIN
				DECLARE @numOppStatus AS NUMERIC(18,0) = CAST(@InlineEditValue AS NUMERIC(18,0))
				EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numOppStatus
			END

		 	 IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @InlineEditValue)
			 BEGIN
	   				EXEC USP_ManageOpportunityAutomationQueue
							@numOppQueueID = 0, -- numeric(18, 0)
							@numDomainID = @numDomainID, -- numeric(18, 0)
							@numOppId = @numOppID, -- numeric(18, 0)
							@numOppBizDocsId = 0, -- numeric(18, 0)
							@numOrderStatus = @InlineEditValue, -- numeric(18, 0)
							@numUserCntID = @numUserCntID, -- numeric(18, 0)
							@tintProcessStatus = 1, -- tinyint
							@tintMode = 1 -- TINYINT
			END 
		 END	
		 ELSE IF @vcDbColumnName = 'dtReleaseDate'
		 BEGIN
			IF EXISTS (SELECT 1 FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND tintOppType=1 AND tintOppStatus=0)
			BEGIN
				IF (SELECT dbo.GetListIemName(ISNULL(numPercentageComplete,0)) FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId) = '100'
				BEGIN
					SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), @InlineEditValue),numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
				END
				ELSE
				BEGIN
					RAISERROR('PERCENT_COMPLETE_MUST_BE_100',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), @InlineEditValue),numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
			END
		END
		ELSE IF @vcDbColumnName = 'numPercentageComplete'
		BEGIN
			IF EXISTS (SELECT 1 FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND tintOppType=1 AND tintOppStatus=0 AND dbo.GetListIemName(ISNULL(numPercentageComplete,0))='100')
			BEGIN
				IF EXISTS(SELECT dtReleaseDate FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND dtReleaseDate IS NULL)
				BEGIN
					SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
				END
				ELSE
				BEGIN
					RAISERROR('REMOVE_RELEASE_DATE',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
			END
		END
		ELSE IF @vcDbColumnName = 'numProjectID'
		BEGIN
			UPDATE OpportunityItems SET numProjectID=@InlineEditValue WHERE numOppId=@numOppId
		END
		ELSE IF @vcDbColumnName = 'intUsedShippingCompany'
		BEGIN
			UPDATE OpportunityMaster SET numShippingService=0 WHERE numOppId=@numOppId
		END

		IF(@vcDbColumnName!='tintOppStatus')
		BEGIN
			SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
		END
	 END
	 ELSE if @vcLookBackTableName = 'Cases' 
	 BEGIN 
	 		IF @vcDbColumnName='numAssignedTo' ---Updating if Case is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from Cases where numCaseId=@numCaseId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update Cases set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numCaseId = @numCaseId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update Cases set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numCaseId = @numCaseId  and numDomainId= @numDomainID        
					end    
			END
		UPDATE Cases SET bintModifiedDate=GETUTCDATE() WHERE numCaseId = @numCaseId  and numDomainId= @numDomainID    
		SET @strSql='update Cases set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCaseId=@numCaseId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'Tickler' 
	 BEGIN 	
		IF(@vcOrigDbColumnName = 'textDetails' AND EXISTS(SELECT * FROM Communication WHERE numCommId=@numCommID and numDomainID=@numDomainID))
		BEGIN
			SET @strSql='update Communication set ' + @vcOrigDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate() where numCommId=@numCommId and numDomainID=@numDomainID'
		END
		ELSE IF(@vcOrigDbColumnName = 'textDetails' AND EXISTS(SELECT * FROM Activity WHERE ActivityID=@numCommID))
		BEGIN
			SET @strSql='update Activity set Comments=@InlineEditValue where ActivityID=@numCommId'
		END
		ELSE
		BEGIN
			SET @strSql='update Communication set ' + @vcOrigDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate() where numCommId=@numCommId and numDomainID=@numDomainID'
		END
	 END
	 ELSE IF @vcLookBackTableName = 'ExtarnetAccounts'
	 BEGIN
		IF @vcDbColumnName='bitEcommerceAccess'
		BEGIN
			IF LOWER(@InlineEditValue) = 'yes' OR LOWER(@InlineEditValue) = 'true'
			BEGIN
				IF NOT EXISTS (SELECT * FROM ExtarnetAccounts where numDivisionID=@numDivisionID) 
				BEGIN
					DECLARE @numGroupID NUMERIC
					DECLARE @numCompanyID NUMERIC(18,0)
					SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionID

					SELECT TOP 1 @numGroupID=numGroupID FROM dbo.AuthenticationGroupMaster WHERE numDomainID=@numDomainID AND tintGroupType=2
				
					IF @numGroupID IS NULL SET @numGroupID = 0 
					INSERT INTO ExtarnetAccounts (numCompanyID,numDivisionID,numGroupID,numDomainID)                                                
					VALUES(@numCompanyID,@numDivisionID,@numGroupID,@numDomainID)
				END
			END
			ELSE
			BEGIN
				DELETE FROM ExtranetAccountsDtl WHERE numExtranetID IN (SELECT ISNULL(numExtranetID,0) FROM ExtarnetAccounts where numDivisionID=@numDivisionID)
				DELETE FROM ExtarnetAccounts where numDivisionID=@numDivisionID
			END

			SELECT (CASE WHEN LOWER(@InlineEditValue) = 'yes' OR LOWER(@InlineEditValue) = 'true' THEN 'Yes' ELSE 'No' END) AS vcData

			RETURN
		END
	 END
	ELSE IF @vcLookBackTableName = 'ExtranetAccountsDtl'
	BEGIN
		IF @vcDbColumnName='vcPassword'
		BEGIN
			IF LEN(@InlineEditValue) = 0
			BEGIN
				DELETE FROM ExtranetAccountsDtl WHERE numContactID=@numContactID
			END
			ELSE
			BEGIN
				IF EXISTS (SELECT numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID)
				BEGIN
					IF EXISTS (SELECT numExtranetDtlID FROM ExtranetAccountsDtl WHERE numContactID=@numContactID)
					BEGIN
						UPDATE
							ExtranetAccountsDtl
						SET 
							vcPassword=@InlineEditValue
							,tintAccessAllowed=1
						WHERE
							numContactID=@numContactID
					END
					ELSE
					BEGIN
						DECLARE @numExtranetID NUMERIC(18,0)
						SELECT @numExtranetID=numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID

						INSERT INTO ExtranetAccountsDtl (numExtranetID,numContactID,vcPassword,tintAccessAllowed,numDomainID)
						VALUES (@numExtranetID,@numContactID,@InlineEditValue,1,@numDomainID)
					END
				END
				ELSE
				BEGIN
					RAISERROR('Organization e-commerce access is not enabled',16,1)
				END
			END

			SELECT @InlineEditValue AS vcData

			RETURN
		END
	END
	ELSE IF @vcLookBackTableName = 'CustomerPartNumber'
	BEGIN
		IF @vcDbColumnName='CustomerPartNo'
		BEGIN
			SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionID
			
			IF EXISTS(SELECT CustomerPartNoID FROM CustomerPartNumber WHERE numItemCode = @numRecId AND numCompanyID = @numCompanyID)
			BEGIN
				UPDATE CustomerPartNumber SET CustomerPartNo = @InlineEditValue WHERE numItemCode = @numRecId AND numCompanyId = @numCompanyID AND numDomainID = @numDomainId 
			END
			ELSE
			BEGIN
				INSERT INTO CustomerPartNumber
				(numItemCode, numCompanyId, numDomainID, CustomerPartNo)
				VALUES
				(@numRecId, @numCompanyID, @numDomainID, @InlineEditValue)
			END

			SELECT @InlineEditValue AS vcData

			RETURN
		END
		
	END
	ELSE IF @vcLookBackTableName = 'CheckHeader'
	BEGIN
		IF EXISTS (SELECT 1 FROM CheckHeader WHERE numDomainID=@numDomainID AND numCheckNo=@InlineEditValue AND numCheckHeaderID <> @numRecId)
		BEGIN
			RAISERROR('DUPLICATE_CHECK_NUMBER',16,1)
			RETURN
		END

		SET @strSql='UPDATE CheckHeader SET ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate() WHERE numCheckHeaderID=@numRecId and numDomainID=@numDomainID'
	END
	ELSE IF @vcLookBackTableName = 'Item'
	BEGIN
		IF @vcDbColumnName = 'monListPrice'
		BEGIN
			SET @InlineEditValue = REPLACE(@InlineEditValue,',','')

			IF EXISTS (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numRecId AND 1 = (CASE WHEN ISNULL(numItemGroup,0) > 0 THEN (CASE WHEN ISNULL(bitMatrix,0)=1 THEN 1 ELSE 0 END) ELSE 1 END))
			BEGIN
				UPDATE WareHouseItems SET monWListPrice=@InlineEditValue WHERE numDomainID=@numDomainID AND numItemID=@numRecId
				SELECT @InlineEditValue AS vcData
			END
			ELSE
			BEGIN
				RAISERROR('You are not allowed to edit list price for warehouse level matrix item.',16,1)
			END

			RETURN
		END

		SET @strSql='UPDATE Item set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numItemCode=@numRecId AND numDomainID=@numDomainID'
	END

	EXECUTE sp_executeSQL @strSql, N'@numDomainID NUMERIC(9),@numUserCntID NUMERIC(9),@numDivisionID NUMERIC(9),@numContactID NUMERIC(9),@InlineEditValue VARCHAR(8000),@numWOId NUMERIC(9),@numProId NUMERIC(9),@numOppId NUMERIC(9),@numCaseId NUMERIC(9),@numWODetailId NUMERIC(9),@numCommID Numeric(9),@numRecId NUMERIC(18,0)',
		     @numDomainID,@numUserCntID,@numDivisionID,@numContactID,@InlineEditValue,@numWOId,@numProId,@numOppId,@numCaseId,@numWODetailId,@numCommID,@numRecId;
		     
	IF @vcAssociatedControlType='SelectBox'                                                    
       BEGIN       
			IF @vcDbColumnName='tintSource' AND @vcLookBackTableName = 'OpportunityMaster'
			BEGIN
				SELECT dbo.fn_GetOpportunitySourceValue(@Value1,@Value2,@numDomainID) AS vcData                                             
			END
			ELSE IF @vcDbColumnName='numContactID'
			BEGIN
				SELECT dbo.fn_GetContactName(@InlineEditValue)
			END
			ELSE IF @vcDbColumnName='numPartner' OR @vcDbColumnName='numPartenerSource'
			BEGIN
				SET @intExecuteDiv=1
				SELECT D.vcPartnerCode+'-'+C.vcCompanyName  AS vcData FROM DivisionMaster AS D 
				LEFT JOIN CompanyInfo AS C
				ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=@InlineEditValue
			END
			
			ELSE IF @vcDbColumnName='tintOppStatus' AND @intExecuteDiv=1
			BEGIN
				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numReleaseStatus'
			BEGIN
				If @InlineEditValue = '1'
				BEGIN
					SELECT 'Open'
				END
				ELSE IF @InlineEditValue = '2'
				BEGIN
					SELECT 'Purchased'
				END
				ELSE
				BEGIN
					SELECT ''
				END
			END
			ELSE IF @vcDbColumnName='numPartenerContact'
			BEGIN
				SET @InlineEditValue=(SELECT TOP 1 A.vcGivenName FROM AdditionalContactsInformation AS A 
										LEFT JOIN DivisionMaster AS D 
										ON D.numDivisionID=A.numDivisionId
										WHERE D.numDomainID=@numDomainID AND A.numCOntactId=@InlineEditValue )
				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numPartenerSource'
			BEGIN
				
				SET @InlineEditValue=(SELECT D.vcPartnerCode+'-'+C.vcCompanyName  AS vcData FROM DivisionMaster AS D 
				LEFT JOIN CompanyInfo AS C
				ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=@InlineEditValue)

				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numShippingService'
			BEGIN
				SELECT ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = @InlineEditValue),'') AS vcData
			END
			ELSE IF @vcDbColumnName='intDropShip'
			BEGIN
				 SELECT CASE WHEN ISNULL(@InlineEditValue,0)=1 THEN 'Not Available'
						 WHEN ISNULL(@InlineEditValue,0)=2 THEN 'Blank Available'
						 WHEN ISNULL(@InlineEditValue,0)=3 THEN 'Vendor Label'
						 WHEN ISNULL(@InlineEditValue,0)=4 THEN 'Private Label'
						 ELSE 'Select One'
					END AS vcData
			END
			else
			BEGIN
				SELECT dbo.fn_getSelectBoxValue(@vcListItemType,@InlineEditValue,@vcDbColumnName) AS vcData                                             
			END
		end 
		ELSE
		BEGIN
			 IF @vcDbColumnName='tintActive'
		  	 BEGIN
			 	SET @InlineEditValue= CASE @InlineEditValue WHEN 1 THEN 'True' ELSE 'False' END 
			 END

			IF @vcAssociatedControlType = 'Check box' or @vcAssociatedControlType = 'Checkbox'
			BEGIN
	 			SET @InlineEditValue= Case @InlineEditValue WHEN 'True' THEN 'Yes' ELSE 'No' END
			END
			
		    SELECT @InlineEditValue AS vcData
		END  
		
	IF @vcLookBackTableName = 'AdditionalContactsInformation' AND @vcDbColumnName = 'numECampaignID'
	BEGIN
		--Manage ECampiagn When Campaign is changed from inline edit
		DECLARE @Date AS DATETIME 
		SELECT @Date = dbo.[GetUTCDateWithoutTime]()
		DECLARE @numECampaignID AS NUMERIC(18,0)
		SET @numECampaignID = @InlineEditValue

		IF @numECampaignID = -1 
		SET @numECampaignID=0
	 
 		EXEC [USP_ManageConEmailCampaign]
		@numConEmailCampID = 0, --  numeric(9, 0)
		@numContactID = @numContactID, --  numeric(9, 0)
		@numECampaignID = @numECampaignID, --  numeric(9, 0)
		@intStartDate = @Date, --  datetime
		@bitEngaged = 1, --  bit
		@numUserCntID = @numUserCntID --  numeric(9, 0)
	END  
END
ELSE
BEGIN	 

	SET @InlineEditValue=Cast(@InlineEditValue as varchar(1000))
	SELECT @vcAssociatedControlType=Fld_type
	 FROM CFW_Fld_Master WHERE Fld_Id=@numFormFieldId 

	IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	    SET @InlineEditValue=(case when @InlineEditValue='False' then 0 ELSE 1 END)
	END

	IF @vcPageName='organization'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
  
	  END
	ELSE IF @vcPageName='contact'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Cont SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Cont (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END
			
			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Cont_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='project'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Pro SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Pro (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Pro_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END
	ELSE IF @vcPageName='opp'
	BEGIN 
		DECLARE @dtDate AS DATETIME = GETUTCDATE()

		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE 
				CFW_Fld_Values_Opp 
			SET 
				Fld_Value=@InlineEditValue ,
				numModifiedBy = @numUserCntID,
				bintModifiedDate = @dtDate
			WHERE 
				FldDTLID= (SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_Fld_Values_Opp (Fld_ID,Fld_Value,RecId,numModifiedBy,bintModifiedDate) VALUES (@numFormFieldId,@InlineEditValue,@numRecId,@numUserCntID,@dtDate)         
		END

		UPDATE OpportunityMaster SET numModifiedBy=@numUserCntID,bintModifiedDate=@dtDate WHERE numOppID = @numRecId 

		--Added By :Sachin Sadhu ||Date:14thOct2014
		--Purpose  :To manage CustomFields in Queue
		EXEC USP_CFW_Fld_Values_Opp_CT
				@numDomainID =@numDomainID,
				@numUserCntID =@numUserCntID,
				@numRecordID =@numRecId ,
				@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='oppitems'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_OppItems CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE 
				CFW_Fld_Values_OppItems 
			SET 
				Fld_Value=@InlineEditValue
			WHERE 
				FldDTLID= (SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_OppItems CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_Fld_Values_OppItems(Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
		END
	END
	ELSE IF @vcPageName='case'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Case SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Case (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Case_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END
	ELSE IF @vcPageName='item'
	BEGIN
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Item CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE CFW_FLD_Values_Item SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Item CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_FLD_Values_Item (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
		END
	END


	IF @vcAssociatedControlType = 'SelectBox'            
	BEGIN            
		    
		 SELECT vcData FROM ListDetails WHERE numListItemID=@InlineEditValue            
	END
	ELSE IF @vcAssociatedControlType = 'CheckBoxList'
	BEGIN
		DECLARE @str AS VARCHAR(MAX)
		SET @str = 'SELECT STUFF((SELECT CONCAT('','', vcData) FROM ListDetails WHERE numlistitemid IN (' + (CASE WHEN LEN(@InlineEditValue) > 0 THEN @InlineEditValue ELSE '0' END) + ') FOR XML PATH('''')), 1, 1, '''')'
		EXEC(@str)
	END
	ELSE IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	 	select CASE @InlineEditValue WHEN 1 THEN 'Yes' ELSE 'No' END AS vcData
	END
	ELSE
	BEGIN
		SELECT @InlineEditValue AS vcData
	END 
END

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p' AND NAME ='USP_Item_ElasticSearchBizCart_GetDynamicValues')
DROP PROCEDURE USP_Item_ElasticSearchBizCart_GetDynamicValues
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USP_Item_ElasticSearchBizCart_GetDynamicValues]
(
	@numSiteId AS NUMERIC(18,0)
	,@numDivisionID AS NUMERIC(18,0) = 0
	,@IsAvaiablityTokenCheck AS BIT = 0
	,@numWareHouseID AS NUMERIC(9) = 0
	--,@vcItemCodes AS NVARCHAR(MAX) = ''
	,@UTV_Item_UOMConversion UTV_Item_UOMConversion READONLY
)
AS
BEGIN
	DECLARE @numDomainID AS NUMERIC(18,0) = 0
	DECLARE @numDefaultRelationship AS NUMERIC(18,0) = 0
	DECLARE @numDefaultProfile AS NUMERIC(18,0) = 0
	DECLARE @bitShowInStock AS BIT = 0
	Declare @isPromotionExist as bit = 0 
	DECLARE @bitAutoSelectWarehouse AS BIT

	SELECT  
		@numDomainID = numDomainID
    FROM 
		[Sites]
    WHERE 
		numSiteID = @numSiteID

	IF ISNULL(@numDivisionID,0) > 0
		BEGIN
		SELECT 
			@numDefaultRelationship=ISNULL(numCompanyType,0)
			,@numDefaultProfile=ISNULL(vcProfile,0)
		FROM 
			DivisionMaster 
		INNER JOIN 
			CompanyInfo 
		ON 
			DivisionMaster.numCompanyID=CompanyInfo.numCompanyId 
		WHERE 
			numDivisionID=@numDivisionID 
			AND DivisionMaster.numDomainID=@numDomainID
	END

	SET @isPromotionExist = IIF(EXISTS(select 1 from PromotionOffer where numDomainId=@numDomainID),1,0)


	--KEEP THIS BELOW ABOVE IF CONDITION
	SELECT 
		@numDefaultRelationship=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultRelationship ELSE numRelationshipId END)
		,@numDefaultProfile=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultProfile ELSE numProfileId END)
		,@bitShowInStock = bitShowInStock
		,@bitAutoSelectWarehouse = ISNULL(bitAutoSelectWarehouse,0)
	FROM 
		dbo.eCommerceDTL 
	WHERE 
		numSiteID = @numSiteId

		DECLARE @vcWarehouseIDs AS VARCHAR(1000)
		IF (@numWareHouseID = 0 OR @bitAutoSelectWarehouse = 1)
		BEGIN
			IF @bitAutoSelectWarehouse = 1
			BEGIN
				SELECT @vcWarehouseIDs = ISNULL( STUFF((SELECT DISTINCT ',' + CAST(numWarehouseID AS VARCHAR(10)) FROM [dbo].[Warehouses] WHERE [Warehouses].[numDomainID] = @numDomainID FOR XML PATH('')),1, 1, '') , '') 
			END
			ELSE
			BEGIN
				SELECT 
					@numWareHouseID = ISNULL(numDefaultWareHouseID,0)
				FROM 
					[eCommerceDTL]
				WHERE 
					[numDomainID] = @numDomainID	

				SET @vcWarehouseIDs = @numWareHouseID
			END
		END
		ELSE
		BEGIN
			SET @vcWarehouseIDs = @numWareHouseID
		END


	SELECT 
		numItemCode
		,vcItemName
		,numTotalPromotions
		,vcPromoDesc
		,bitRequireCouponCode
		,numItemClassification
		,UOM
		,UOMPurchase
		,monListPrice
		,numVendorID
		,charItemType
		,(CASE  
			WHEN @IsAvaiablityTokenCheck = 0 THEN 0 
				ELSE (CASE WHEN charItemType <> 'S' AND charItemType <> 'N' AND ISNULL(bitKitParent,0)=0
						THEN (CASE 
								WHEN bitAllowBackOrder = 1 THEN 1
								WHEN (ISNULL(numOnHand,0)<=0) THEN 0
								ELSE 1
							END)
						ELSE 1
				END)
		END) AS bitInStock
		,(CASE  
			WHEN @IsAvaiablityTokenCheck = 0 THEN '' 
				ELSE (CASE WHEN charItemType <> 'S' AND charItemType <> 'N'
					THEN (CASE WHEN @bitShowInStock = 1
								THEN (CASE 
										WHEN bitAllowBackOrder = 1 AND numDomainID NOT IN (172) AND ISNULL(numOnHand,0)<=0 AND ISNULL(numVendorID,0) > 0 AND dbo.GetVendorPreferredMethodLeadTime(numVendorID,0) > 0 THEN CONCAT('<font class="vendorLeadTime" style="color:green">Usually ships in ',dbo.GetVendorPreferredMethodLeadTime(numVendorID,0),' days</font>')
										WHEN bitAllowBackOrder = 1 AND ISNULL(numOnHand,0)<=0 THEN (CASE WHEN numDomainID  IN (172) THEN '<font style="color:green">AVAILABLE</font>' ELSE '<font style="color:green">In Stock</font>' END)
										WHEN (ISNULL(numOnHand,0)<=0) THEN (CASE WHEN numDomainID  IN (172) THEN '<font style="color:red">ON HOLD</font>' ELSE '<font style="color:red">Out Of Stock</font>' END)
										ELSE (CASE WHEN numDomainID IN (172) THEN '<font style="color:green">AVAILABLE</font>' ELSE '<font style="color:green">In Stock</font>' END)  
									END)
								ELSE ''
							END)
					ELSE ''
				END)
			END) AS InStock ,
			CONVERT(FLOAT,0) AS monFirstPriceLevelPrice,
			CONVERT(FLOAT,0) AS fltFirstPriceLevelDiscount,
			W1.[monWListPrice],
			ISNULL(W1.numWareHouseItemID,0) [numWareHouseItemID]
			
			INTO #TempTbl
	FROM 
		(SELECT 
			I.numItemCode
			,I.vcItemName
			,I.charItemType
			,I.bitKitParent
			,I.bitAllowBackOrder
			,I.numDomainID
			,I.numVendorID
			,I.numItemClassification
			,CONVERT(INT,0) numTotalPromotions
			,CONVERT(NVARCHAR(100),'') vcPromoDesc
			,CONVERT(BIT,0) bitRequireCouponCode
			,SUM(ISNULL(WHI.numOnHand,0)) AS numOnHand
			,TEMPItem.UOM
			,TEMPItem.UOMPurchase
			,I.monListPrice
		FROM Item I 
		--INNER JOIN
		--(
		--	SELECT [OutParam] FROM dbo.[SplitString](@vcItemCodes,',')
		--) TEMPItem
		--ON
		--	I.numItemCode = TEMPItem.OutParam
		INNER JOIN @UTV_Item_UOMConversion AS TEMPItem ON (I.numItemCode = TEMPItem.numItemCode)
		LEFT JOIN WareHouseItems WHI ON (WHI.numItemID = I.numItemCode)
		--OUTER APPLY (SELECT * FROM dbo.fn_GetItemPromotions(I.numDomainID,@numDivisionID,numItemCode,numItemClassification,@numDefaultRelationship,@numDefaultProfile,1)) TablePromotion
		WHERE WHI.numdomainID = @numDomainID 
		GROUP BY 
			I.numItemCode
			,I.vcItemName
			,I.charItemType
			,I.bitKitParent
			,I.bitAllowBackOrder
			,I.numDomainID
			,I.numVendorID
			,I.numItemClassification
			--,ISNULL(TablePromotion.numTotalPromotions,0)
			--,ISNULL(TablePromotion.vcPromoDesc,'''') 
			--,ISNULL(TablePromotion.bitRequireCouponCode,0) 
			,TEMPItem.UOM
			,TEMPItem.UOMPurchase
			,monListPrice
		)T
		OUTER APPLY (SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems W 
						WHERE  W.numItemID = T.numItemCode 
						AND W.numDomainID = @numDomainID
						AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](@vcWarehouseIDs,','))) AS W1
		


		IF(@isPromotionExist =1)
		BEGIN 
			UPDATE t1 
			SET t1.numTotalPromotions = ISNULL(TablePromotion.numTotalPromotions,0),
			t1.vcPromoDesc = TablePromotion.vcPromoDesc,
			t1.bitRequireCouponCode = ISNULL(TablePromotion.bitRequireCouponCode,0)
			FROM #TempTbl t1
			OUTER APPLY (SELECT * FROM dbo.fn_GetItemPromotions(@numDomainID,@numDivisionID,numItemCode,t1.numItemClassification,@numDefaultRelationship,@numDefaultProfile,1)) TablePromotion
		END

		IF(@numDomainID = 172)
		BEGIN 
			
			UPDATE t1 
			SET t1.monFirstPriceLevelPrice = ISNULL(CASE 
						WHEN tintRuleType = 1 AND tintDiscountType = 1
						THEN ISNULL((CASE WHEN t1.[charItemType] = 'P' THEN ( UOM * ISNULL(t1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - (ISNULL((CASE WHEN t1.[charItemType] = 'P' THEN ( UOM * ISNULL(t1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) * ( decDiscount / 100 ) )
						WHEN tintRuleType = 1 AND tintDiscountType = 2
						THEN ISNULL((CASE WHEN t1.[charItemType] = 'P' THEN ( UOM * ISNULL(t1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - decDiscount
						WHEN tintRuleType = 2 AND tintDiscountType = 1
						THEN ISNULL(Vendor.monCost,0) + (ISNULL(Vendor.monCost,0) * ( decDiscount / 100 ) )
						WHEN tintRuleType = 2 AND tintDiscountType = 2
						THEN ISNULL(Vendor.monCost,0) + decDiscount
						WHEN tintRuleType = 3
						THEN decDiscount
					END, 0),
			t1.fltFirstPriceLevelDiscount = ISNULL(CASE 
					WHEN tintRuleType = 1 AND tintDiscountType = 1 
					THEN decDiscount 
					WHEN tintRuleType = 1 AND tintDiscountType = 2
					THEN (decDiscount * 100) / (ISNULL((CASE WHEN t1.[charItemType] = 'P' THEN ( UOM * ISNULL(t1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - decDiscount)
					WHEN tintRuleType = 2 AND tintDiscountType = 1
					THEN decDiscount
					WHEN tintRuleType = 2 AND tintDiscountType = 2
					THEN (decDiscount * 100) / (ISNULL(Vendor.monCost,0) + decDiscount)
					WHEN tintRuleType = 3
					THEN 0
				END,'')
			FROM #TempTbl t1
			--INNER JOIN Item I ON (I.numItemCode = t1.numItemCode)
			OUTER APPLY (SELECT TOP 1 * FROM PricingTable WHERE numItemCode=t1.numItemCode AND ISNULL(numCurrencyID,0) = 0) AS PT 
			LEFT JOIN Vendor ON Vendor.numVendorID =t1.numVendorID AND Vendor.numItemCode=t1.numItemCode
			 			  
			--CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numSaleUnit, 0),t1.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOM) AS UOM
			--CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numPurchaseUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOMPurchase) AS UOMPurchase
		END



		SELECT * From #TempTbl

		
	IF OBJECT_ID('tempdb..#TempTbl') IS NOT NULL
	DROP TABLE #TempTbl
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p' AND NAME ='USP_Item_ElasticSearchBizCart_GetDynamicValues')
DROP PROCEDURE USP_Item_ElasticSearchBizCart_GetDynamicValues
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USP_Item_ElasticSearchBizCart_GetDynamicValues]
(
	@numSiteId AS NUMERIC(18,0)
	,@numDivisionID AS NUMERIC(18,0) = 0
	,@IsAvaiablityTokenCheck AS BIT = 0
	,@numWareHouseID AS NUMERIC(9) = 0
	--,@vcItemCodes AS NVARCHAR(MAX) = ''
	,@UTV_Item_UOMConversion UTV_Item_UOMConversion READONLY
)
AS
BEGIN
	DECLARE @numDomainID AS NUMERIC(18,0) = 0
	DECLARE @numDefaultRelationship AS NUMERIC(18,0) = 0
	DECLARE @numDefaultProfile AS NUMERIC(18,0) = 0
	DECLARE @bitShowInStock AS BIT = 0
	Declare @isPromotionExist as bit = 0 
	DECLARE @bitAutoSelectWarehouse AS BIT

	SELECT  
		@numDomainID = numDomainID
    FROM 
		[Sites]
    WHERE 
		numSiteID = @numSiteID

	IF ISNULL(@numDivisionID,0) > 0
		BEGIN
		SELECT 
			@numDefaultRelationship=ISNULL(numCompanyType,0)
			,@numDefaultProfile=ISNULL(vcProfile,0)
		FROM 
			DivisionMaster 
		INNER JOIN 
			CompanyInfo 
		ON 
			DivisionMaster.numCompanyID=CompanyInfo.numCompanyId 
		WHERE 
			numDivisionID=@numDivisionID 
			AND DivisionMaster.numDomainID=@numDomainID
	END

	SET @isPromotionExist = IIF(EXISTS(select 1 from PromotionOffer where numDomainId=@numDomainID),1,0)


	--KEEP THIS BELOW ABOVE IF CONDITION
	SELECT 
		@numDefaultRelationship=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultRelationship ELSE numRelationshipId END)
		,@numDefaultProfile=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultProfile ELSE numProfileId END)
		,@bitShowInStock = bitShowInStock
		,@bitAutoSelectWarehouse = ISNULL(bitAutoSelectWarehouse,0)
	FROM 
		dbo.eCommerceDTL 
	WHERE 
		numSiteID = @numSiteId

		DECLARE @vcWarehouseIDs AS VARCHAR(1000)
		IF (@numWareHouseID = 0 OR @bitAutoSelectWarehouse = 1)
		BEGIN
			IF @bitAutoSelectWarehouse = 1
			BEGIN
				SELECT @vcWarehouseIDs = ISNULL( STUFF((SELECT DISTINCT ',' + CAST(numWarehouseID AS VARCHAR(10)) FROM [dbo].[Warehouses] WHERE [Warehouses].[numDomainID] = @numDomainID FOR XML PATH('')),1, 1, '') , '') 
			END
			ELSE
			BEGIN
				SELECT 
					@numWareHouseID = ISNULL(numDefaultWareHouseID,0)
				FROM 
					[eCommerceDTL]
				WHERE 
					[numDomainID] = @numDomainID	

				SET @vcWarehouseIDs = @numWareHouseID
			END
		END
		ELSE
		BEGIN
			SET @vcWarehouseIDs = @numWareHouseID
		END


	SELECT 
		numItemCode
		,vcItemName
		,numTotalPromotions
		,vcPromoDesc
		,bitRequireCouponCode
		,numItemClassification
		,UOM
		,UOMPurchase
		,monListPrice
		,numVendorID
		,charItemType
		,(CASE  
			WHEN @IsAvaiablityTokenCheck = 0 THEN 0 
				ELSE (CASE WHEN charItemType <> 'S' AND charItemType <> 'N' AND ISNULL(bitKitParent,0)=0
						THEN (CASE 
								WHEN bitAllowBackOrder = 1 THEN 1
								WHEN (ISNULL(numOnHand,0)<=0) THEN 0
								ELSE 1
							END)
						ELSE 1
				END)
		END) AS bitInStock
		,(CASE  
			WHEN @IsAvaiablityTokenCheck = 0 THEN '' 
				ELSE (CASE WHEN charItemType <> 'S' AND charItemType <> 'N'
					THEN (CASE WHEN @bitShowInStock = 1
								THEN (CASE 
										WHEN bitAllowBackOrder = 1 AND numDomainID NOT IN (172) AND ISNULL(numOnHand,0)<=0 AND ISNULL(numVendorID,0) > 0 AND dbo.GetVendorPreferredMethodLeadTime(numVendorID,0) > 0 THEN CONCAT('<font class="vendorLeadTime" style="color:green">Usually ships in ',dbo.GetVendorPreferredMethodLeadTime(numVendorID,0),' days</font>')
										WHEN bitAllowBackOrder = 1 AND ISNULL(numOnHand,0)<=0 THEN (CASE WHEN numDomainID  IN (172) THEN '<font style="color:green">AVAILABLE</font>' ELSE '<font style="color:green">In Stock</font>' END)
										WHEN (ISNULL(numOnHand,0)<=0) THEN (CASE WHEN numDomainID  IN (172) THEN '<font style="color:red">ON HOLD</font>' ELSE '<font style="color:red">Out Of Stock</font>' END)
										ELSE (CASE WHEN numDomainID IN (172) THEN '<font style="color:green">AVAILABLE</font>' ELSE '<font style="color:green">In Stock</font>' END)  
									END)
								ELSE ''
							END)
					ELSE ''
				END)
			END) AS InStock ,
			CONVERT(FLOAT,0) AS monFirstPriceLevelPrice,
			CONVERT(FLOAT,0) AS fltFirstPriceLevelDiscount,
			W1.[monWListPrice],
			ISNULL(W1.numWareHouseItemID,0) [numWareHouseItemID]
			
			INTO #TempTbl
	FROM 
		(SELECT 
			I.numItemCode
			,I.vcItemName
			,I.charItemType
			,I.bitKitParent
			,I.bitAllowBackOrder
			,I.numDomainID
			,I.numVendorID
			,I.numItemClassification
			,CONVERT(INT,0) numTotalPromotions
			,CONVERT(NVARCHAR(100),'') vcPromoDesc
			,CONVERT(BIT,0) bitRequireCouponCode
			,SUM(ISNULL(WHI.numOnHand,0)) AS numOnHand
			,TEMPItem.UOM
			,TEMPItem.UOMPurchase
			,I.monListPrice
		FROM Item I 
		--INNER JOIN
		--(
		--	SELECT [OutParam] FROM dbo.[SplitString](@vcItemCodes,',')
		--) TEMPItem
		--ON
		--	I.numItemCode = TEMPItem.OutParam
		INNER JOIN @UTV_Item_UOMConversion AS TEMPItem ON (I.numItemCode = TEMPItem.numItemCode)
		LEFT JOIN WareHouseItems WHI ON (WHI.numItemID = I.numItemCode)
		--OUTER APPLY (SELECT * FROM dbo.fn_GetItemPromotions(I.numDomainID,@numDivisionID,numItemCode,numItemClassification,@numDefaultRelationship,@numDefaultProfile,1)) TablePromotion
		WHERE WHI.numdomainID = @numDomainID 
		GROUP BY 
			I.numItemCode
			,I.vcItemName
			,I.charItemType
			,I.bitKitParent
			,I.bitAllowBackOrder
			,I.numDomainID
			,I.numVendorID
			,I.numItemClassification
			--,ISNULL(TablePromotion.numTotalPromotions,0)
			--,ISNULL(TablePromotion.vcPromoDesc,'''') 
			--,ISNULL(TablePromotion.bitRequireCouponCode,0) 
			,TEMPItem.UOM
			,TEMPItem.UOMPurchase
			,monListPrice
		)T
		OUTER APPLY (SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems W 
						WHERE  W.numItemID = T.numItemCode 
						AND W.numDomainID = @numDomainID
						AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](@vcWarehouseIDs,','))) AS W1
		


		IF(@isPromotionExist =1)
		BEGIN 
			UPDATE t1 
			SET t1.numTotalPromotions = ISNULL(TablePromotion.numTotalPromotions,0),
			t1.vcPromoDesc = TablePromotion.vcPromoDesc,
			t1.bitRequireCouponCode = ISNULL(TablePromotion.bitRequireCouponCode,0)
			FROM #TempTbl t1
			OUTER APPLY (SELECT * FROM dbo.fn_GetItemPromotions(@numDomainID,@numDivisionID,numItemCode,t1.numItemClassification,@numDefaultRelationship,@numDefaultProfile,1)) TablePromotion
		END

		IF(@numDomainID = 172)
		BEGIN 
			
			UPDATE t1 
			SET t1.monFirstPriceLevelPrice = ISNULL(CASE 
						WHEN tintRuleType = 1 AND tintDiscountType = 1
						THEN ISNULL((CASE WHEN t1.[charItemType] = 'P' THEN ( UOM * ISNULL(t1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - (ISNULL((CASE WHEN t1.[charItemType] = 'P' THEN ( UOM * ISNULL(t1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) * ( decDiscount / 100 ) )
						WHEN tintRuleType = 1 AND tintDiscountType = 2
						THEN ISNULL((CASE WHEN t1.[charItemType] = 'P' THEN ( UOM * ISNULL(t1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - decDiscount
						WHEN tintRuleType = 2 AND tintDiscountType = 1
						THEN ISNULL(Vendor.monCost,0) + (ISNULL(Vendor.monCost,0) * ( decDiscount / 100 ) )
						WHEN tintRuleType = 2 AND tintDiscountType = 2
						THEN ISNULL(Vendor.monCost,0) + decDiscount
						WHEN tintRuleType = 3
						THEN decDiscount
					END, 0),
			t1.fltFirstPriceLevelDiscount = ISNULL(CASE 
					WHEN tintRuleType = 1 AND tintDiscountType = 1 
					THEN decDiscount 
					WHEN tintRuleType = 1 AND tintDiscountType = 2
					THEN (decDiscount * 100) / (ISNULL((CASE WHEN t1.[charItemType] = 'P' THEN ( UOM * ISNULL(t1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - decDiscount)
					WHEN tintRuleType = 2 AND tintDiscountType = 1
					THEN decDiscount
					WHEN tintRuleType = 2 AND tintDiscountType = 2
					THEN (decDiscount * 100) / (ISNULL(Vendor.monCost,0) + decDiscount)
					WHEN tintRuleType = 3
					THEN 0
				END,'')
			FROM #TempTbl t1
			--INNER JOIN Item I ON (I.numItemCode = t1.numItemCode)
			OUTER APPLY (SELECT TOP 1 * FROM PricingTable WHERE numItemCode=t1.numItemCode AND ISNULL(numCurrencyID,0) = 0) AS PT 
			LEFT JOIN Vendor ON Vendor.numVendorID =t1.numVendorID AND Vendor.numItemCode=t1.numItemCode
			 			  
			--CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numSaleUnit, 0),t1.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOM) AS UOM
			--CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numPurchaseUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOMPurchase) AS UOMPurchase
		END



		SELECT * From #TempTbl

		
	IF OBJECT_ID('tempdb..#TempTbl') IS NOT NULL
	DROP TABLE #TempTbl
END


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetDetails')
DROP PROCEDURE dbo.USP_Item_GetDetails
GO
CREATE PROCEDURE [dbo].[USP_Item_GetDetails]                                        
@numItemCode as numeric(9),
@numWarehouseItemID as NUMERIC(9),
@ClientTimeZoneOffset AS INT,
@vcSelectedKitChildItems VARCHAR(MAX)                                               
as                                                 
   
   DECLARE @bitItemIsUsedInOrder AS BIT;SET @bitItemIsUsedInOrder=0
   
   DECLARE @numDomainId AS NUMERIC
   SELECT @numDomainId=numDomainId FROM Item WHERE numItemCode=@numItemCode
   
   IF GETUTCDATE()<'2013-03-15 23:59:39'
		SET @bitItemIsUsedInOrder=0
   ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) >0
		SET @bitItemIsUsedInOrder=1
   ELSE IF EXISTS(SELECT numItemID FROM dbo.General_Journal_Details WHERE numItemID= @numItemCode and numDomainId=@numDomainId AND chBizDocItems='IA1'   )
		set @bitItemIsUsedInOrder =1
                                               
	SELECT 
		I.numItemCode, 
		vcItemName, 
		ISNULL(txtItemDesc,'') txtItemDesc,
		CAST( ISNULL(IED.txtDesc,'')  AS VARCHAR(MAX) ) vcExtendedDescToAPI, 
		charItemType, 
		(CASE WHEN charItemType='P' THEN ISNULL(W.monWListPrice,0) ELSE ISNULL(monListPrice,0) END) AS monListPrice,                   
		numItemClassification, 
		isnull(bitTaxable,0) as bitTaxable, 
		vcSKU AS vcSKU, 
		isnull(bitKitParent,0) as bitKitParent,--, dtDateEntered,                  
		I.numVendorID, I.numDomainID, numCreatedBy, bintCreatedDate, bintModifiedDate,                   
		numModifiedBy,
		(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1 ) AS vcPathForImage ,
		(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1) AS  vcPathForTImage, isnull(bitSerialized,0) as bitSerialized, vcModelID,                   
		(SELECT numItemImageId,vcPathForImage,vcPathForTImage,bitDefault,case when bitdefault=1 then -1 
		else isnull(intDisplayOrder,0) end as intDisplayOrder 
		FROM ItemImages  
		WHERE numItemCode=@numItemCode order by case when bitdefault=1 then -1 else isnull(intDisplayOrder,0) end  asc 
		FOR XML AUTO,ROOT('Images')) AS xmlItemImages,
		(SELECT STUFF((SELECT ',' + CONVERT(VARCHAR(50) , numCategoryID)  FROM dbo.ItemCategory WHERE numItemID = I.numItemCode FOR XML PATH('')), 1, 1, '')) AS vcItemCategories ,
		numItemGroup, numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE monAverageCost END) monAverageCost,                   
		monCampaignLabourCost,dbo.fn_GetContactName(numCreatedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,bintCreatedDate )) as CreatedBy ,                                      
		dbo.fn_GetContactName(numModifiedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,bintModifiedDate )) as ModifiedBy,                      
		numOnHand as numOnHand,                      
		numOnOrder as numOnOrder,                      
		numReorder as numReorder,                      
		numAllocation as numAllocation,                      
		numBackOrder as numBackOrder,                   
		(CASE 
			WHEN ISNULL(bitKitParent,0)=1
					AND (CASE 
							WHEN EXISTS (SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID=IInner.numItemCode WHERE ID.numItemKitID=I.numItemCode AND ISNULL(IInner.bitKitParent,0)=1)
							THEN 1
							ELSE 0
						END) = 0
			THEN 
				dbo.GetKitWeightBasedOnItemSelection(@numItemCode,@vcSelectedKitChildItems)
			ELSE 
				ISNULL(fltWeight,0) 
		END) as fltWeight,                
		isnull(fltHeight,0) as fltHeight,                
		isnull(fltWidth,0) as fltWidth,                
		isnull(fltLength,0) as fltLength,                
		isnull(bitFreeShipping,0) as bitFreeShipping,              
		isnull(bitAllowBackOrder,0) as bitAllowBackOrder,              
		isnull(vcUnitofMeasure,'Units') as vcUnitofMeasure,      
		isnull(bitShowDeptItem,0) bitShowDeptItem,      
		isnull(bitShowDeptItemDesc,0) bitShowDeptItemDesc,  
		isnull(bitCalAmtBasedonDepItems ,0) bitCalAmtBasedonDepItems,
		isnull(bitAssembly ,0) bitAssembly ,
		isnull(cast(numBarCodeId as varchar(50)),'') as  numBarCodeId ,
		isnull(I.vcManufacturer,'') as vcManufacturer,
		ISNULL(I.numBaseUnit,0) AS numBaseUnit,ISNULL(I.numPurchaseUnit,0) AS numPurchaseUnit,ISNULL(I.numSaleUnit,0) AS numSaleUnit,
		dbo.fn_GetUOMName(ISNULL(I.numBaseUnit,0)) AS vcBaseUnit,dbo.fn_GetUOMName(ISNULL(I.numPurchaseUnit,0)) AS vcPurchaseUnit,dbo.fn_GetUOMName(ISNULL(I.numSaleUnit,0)) AS vcSaleUnit,
		isnull(bitLotNo,0) as bitLotNo,
		ISNULL(IsArchieve,0) AS IsArchieve,
		case when charItemType='P' then 'Inventory Item' when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory Item' end as ItemType,
		case when ISNULL(bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode,W.numWareHouseID) else 0 end AS numWOQty,
		isnull(I.numItemClass,0) as numItemClass,
		ISNULL(tintStandardProductIDType,0) tintStandardProductIDType,
		ISNULL(vcExportToAPI,'') vcExportToAPI,
		ISNULL(numShipClass,0) numShipClass,ISNULL(I.bitAllowDropShip,0) AS bitAllowDropShip,
		ISNULL(@bitItemIsUsedInOrder,0) AS bitItemIsUsedInOrder,
		ISNULL((SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID = I.numItemCode),0) AS intParentKitItemCount,
		ISNULL(I.bitArchiveItem,0) AS [bitArchiveItem],
		ISNULL(W.numWareHouseID,0) AS [numWareHouseID],
		ISNULL(W.numWareHouseItemID,0) AS [numWareHouseItemID],
		ISNULL((SELECT COUNT(WI.[numWareHouseItemID]) FROM [dbo].[WareHouseItems] WI WHERE WI.[numItemID] = I.numItemCode AND WI.[numItemID] = @numItemCode),0) AS [intTransCount],
		ISNULL(I.bitAsset,0) AS [bitAsset],
		ISNULL(I.bitRental,0) AS [bitRental],
		ISNULL(I.numContainer,0) AS [numContainer],
		ISNULL(I.numNoItemIntoContainer,0) AS [numNoItemIntoContainer],
		ISNULL(Vendor.monCost,0) AS monVendorCost, CPN.CustomerPartNo,
		ISNULL(WH.vcWareHouse,'') vcWareHouse
	FROM 
		Item I       
	left join  
		WareHouseItems W                  
	on 
		W.numItemID=I.numItemCode AND
		W.numWareHouseItemID = @numWarehouseItemID
	LEFT JOIN
		Warehouses WH
	ON
		W.numWareHouseID = WH.numWareHouseID
	LEFT JOIN 
		ItemExtendedDetails IED   
	ON 
		I.numItemCode = IED.numItemCode
	LEFT JOIN 
		Vendor
	ON
		I.numVendorID = Vendor.numVendorID
		AND Vendor.numItemCode = I.numItemCode    
	LEFT JOIN 
		CustomerPartNumber CPN   
	ON 
		I.numItemCode = CPN.numItemCode        
	WHERE 
		I.numItemCode=@numItemCode 

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_UpdateItemVendorCost')
DROP PROCEDURE USP_Item_UpdateItemVendorCost
GO
CREATE PROCEDURE [dbo].[USP_Item_UpdateItemVendorCost]  
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@vcSelectedRecords VARCHAR(MAX)
AS  
BEGIN
	DECLARE @hDocItem INT

	DECLARE @TEMP TABLE
	(
		numVendorID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,monUnitCost DECIMAL(20,5)
	)

	EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcSelectedRecords

	INSERT INTO @TEMP
	(
		numVendorID
		,numItemCode
		,monUnitCost
	)
	SELECT
		ISNULL(VendorID,0)
		,ISNULL(ItemCode,0)
		,ISNULL(UnitCost,0)
	FROM
		OPENXML (@hDocItem,'/NewDataSet/Table1',2)
	WITH
	(
		VendorID NUMERIC(18,0)
		,ItemCode NUMERIC(18,0)
		,UnitCost DECIMAL(20,5)
	)

	EXEC sp_xml_removedocument @hDocItem

	UPDATE
		V
	SET
		V.monCost = T.monUnitCost
	FROM
		@TEMP T
	INNER JOIN
		Vendor V
	ON
		V.numDomainID = @numDomainID
		AND T.numVendorID = V.numVendorID
		AND T.numItemCode = V.numItemCode
END
GO
--Created By Anoop Jayaraj          
--exec USP_ItemDetailsForEcomm @numItemCode=859064,@numWareHouseID=1074,@numDomainID=172,@numSiteId=90
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itemdetailsforecomm')
DROP PROCEDURE USP_ItemDetailsForEcomm
GO
CREATE PROCEDURE [dbo].[USP_ItemDetailsForEcomm]
@numItemCode as numeric(9),          
@numWareHouseID as numeric(9),    
@numDomainID as numeric(9),
@numSiteId as numeric(9),
@vcCookieId as VARCHAR(MAX)=null,
@numUserCntID AS NUMERIC(9)=0,
@numDivisionID AS NUMERIC(18,0) = 0                                        
AS     
BEGIN
/*Removed warehouse parameter, and taking Default warehouse by default from DB*/
    
	DECLARE @bitShowInStock AS BIT        
	DECLARE @bitShowQuantity AS BIT
	DECLARE @bitAutoSelectWarehouse AS BIT              
	DECLARE @numDefaultWareHouseID AS NUMERIC(9)
	DECLARE @numDefaultRelationship AS NUMERIC(18,0)
	DECLARE @numDefaultProfile AS NUMERIC(18,0)
	DECLARE @tintPreLoginPriceLevel AS TINYINT
	DECLARE @tintPriceLevel AS TINYINT
	DECLARE @bitMatrix BIT
	DECLARE @numItemGroup NUMERIC(18,0)
	SET @bitShowInStock=0        
	SET @bitShowQuantity=0        
       
        
	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT 
			@numDefaultRelationship=ISNULL(numCompanyType,0)
			,@numDefaultProfile=ISNULL(vcProfile,0)
			,@tintPriceLevel=ISNULL(tintPriceLevel,0) 
		FROM 
			DivisionMaster 
		INNER JOIN 
			CompanyInfo 
		ON 
			DivisionMaster.numCompanyID=CompanyInfo.numCompanyId 
		WHERE 
			numDivisionID=@numDivisionID 
			AND DivisionMaster.numDomainID=@numDomainID
	END     
      
	--KEEP THIS BELOW ABOVE IF CONDITION 
	SELECT 
		@bitShowInStock=ISNULL(bitShowInStock,0)
		,@bitShowQuantity=ISNULL(bitShowQOnHand,0)
		,@bitAutoSelectWarehouse=ISNULL(bitAutoSelectWarehouse,0)
		,@numDefaultRelationship=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultRelationship ELSE numRelationshipId END)
		,@numDefaultProfile=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultProfile ELSE numProfileId END)
		,@tintPreLoginPriceLevel=(CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN (CASE WHEN @numDivisionID > 0 THEN (CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN ISNULL(@tintPriceLevel,0) ELSE 0 END) ELSE ISNULL(tintPreLoginProceLevel,0) END) ELSE 0 END)
	FROM 
		eCommerceDTL 
	WHERE 
		numDomainID=@numDomainID

	IF @numWareHouseID=0
	BEGIN
		SELECT @numDefaultWareHouseID=ISNULL(numDefaultWareHouseID,0) FROM [eCommerceDTL] WHERE [numDomainID]=@numDomainID	
		SET @numWareHouseID =@numDefaultWareHouseID;
	END

	DECLARE @vcWarehouseIDs AS VARCHAR(1000)
	IF @bitAutoSelectWarehouse = 1
	BEGIN		
		SELECT @vcWarehouseIDs = ISNULL( STUFF((SELECT DISTINCT ',' + CAST(numWarehouseID AS VARCHAR(10)) FROM [dbo].[Warehouses] WHERE [Warehouses].[numDomainID] = @numDomainID FOR XML PATH('')),1, 1, '') , '')
	END
	ELSE
	BEGIN
		SET @vcWarehouseIDs = @numWareHouseID
	END

	/*If qty is not found in default warehouse then choose next warehouse with sufficient qty */
	IF @bitAutoSelectWarehouse = 1 AND ( SELECT COUNT(*) FROM dbo.WareHouseItems WHERE  numItemID = @numItemCode AND numWarehouseID= @numWareHouseID ) = 0 
	BEGIN
		SELECT TOP 1 
			@numDefaultWareHouseID = numWareHouseID 
		FROM 
			dbo.WareHouseItems 
		WHERE 
			numItemID = @numItemCode 
			AND numOnHand > 0 
		ORDER BY 
			numOnHand DESC 
		
		IF (ISNULL(@numDefaultWareHouseID,0)>0)
			SET @numWareHouseID =@numDefaultWareHouseID;
	END

    DECLARE @UOMConversionFactor AS DECIMAL(18,5)
      
    SELECT 
		@UOMConversionFactor= ISNULL(dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)),0)
		,@bitMatrix=ISNULL(bitMatrix,0)
		,@numItemGroup=ISNULL(numItemGroup,0)
    FROM 
		Item I 
	WHERE 
		numItemCode=@numItemCode   	
 
	DECLARE @strSql AS VARCHAR(MAX)
	
	SET @strSql = CONCAT('WITH tblItem AS 
							(SELECT 
								I.numItemCode
								,vcItemName
								,txtItemDesc
								,charItemType
								,ISNULL(I.bitKitParent,0) bitKitParent
								,ISNULL(bitCalAmtBasedonDepItems,0) bitCalAmtBasedonDepItems
								,ISNULL(I.bitMatrix,0) bitMatrix,(CASE WHEN (ISNULL(I.bitKitParent,0) = 1 OR ISNULL(I.bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1 THEN dbo.GetCalculatedPriceForKitAssembly(I.numDomainID,',@numDivisionID,',I.numItemCode,1,0,ISNULL(I.tintKitAssemblyPriceBasedOn,1),0,0,'''',0,1) ELSE 
								',(CASE 
									WHEN @tintPreLoginPriceLevel > 0 
									THEN CONCAT('ISNULL(PricingOption.monPrice,ISNULL(CASE WHEN I.[charItemType]=''P'' THEN ',@UOMConversionFactor,' * W.[monWListPrice] ELSE ',@UOMConversionFactor,' * monListPrice END,0))') 
									ELSE CONCAT('ISNULL(CASE WHEN I.[charItemType]=''P'' THEN ',@UOMConversionFactor,' * W.[monWListPrice] ELSE ',@UOMConversionFactor,' * monListPrice END,0)')
								END),'END) AS monListPrice
								,(CASE WHEN (ISNULL(I.bitKitParent,0) = 1 OR ISNULL(I.bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1 THEN dbo.GetCalculatedPriceForKitAssembly(I.numDomainID,',@numDivisionID,',I.numItemCode,1,0,ISNULL(I.tintKitAssemblyPriceBasedOn,1),0,0,'''',0,1) ELSE ISNULL(CASE WHEN I.[charItemType]=''P'' 
											THEN ',@UOMConversionFactor,' * W.[monWListPrice]  
											ELSE ',@UOMConversionFactor,' * monListPrice 
										END,0) END) AS monMSRP
										,numItemClassification
										,bitTaxable
										,vcSKU
										,I.numModifiedBy
										,(SELECT numItemImageId,vcPathForImage,vcPathForTImage,bitDefault,case when bitdefault=1  then -1 else isnull(intDisplayOrder,0) end as intDisplayOrder FROM ItemImages  WHERE numItemCode=I.numItemCode and bitIsImage = 1 order by case when bitdefault=1 then -1 else isnull(intDisplayOrder,0) end  asc FOR XML AUTO,ROOT(''Images''))  vcImages
										,(SELECT vcPathForImage FROM ItemImages  WHERE numItemCode=I.numItemCode and bitIsImage = 0 order by intDisplayOrder  asc FOR XML AUTO,ROOT(''Documents''))  vcDocuments
										,ISNULL(bitSerialized,0) as bitSerialized
										,vcModelID
										,numItemGroup
										,(ISNULL(sum(numOnHand),0) / ',@UOMConversionFactor,') as numOnHand
										,sum(numOnOrder) as numOnOrder,                    
										sum(numReorder)  as numReorder,                    
										sum(numAllocation)  as numAllocation,                    
										sum(numBackOrder)  as numBackOrder,              
										isnull(fltWeight,0) as fltWeight,              
										isnull(fltHeight,0) as fltHeight,              
										isnull(fltWidth,0) as fltWidth,              
										isnull(fltLength,0) as fltLength,              
										case when charItemType=''P''then isnull(bitFreeShipping,0) else 1 end as bitFreeShipping,
										Case When (case when charItemType=''P''then isnull(bitFreeShipping,0) else 1 end )=1 then ''Free shipping'' else ''Calculated at checkout'' end as Shipping,              
										isnull(bitAllowBackOrder,0) as bitAllowBackOrder,bitShowInStock,bitShowQOnHand,isnull(numWareHouseItemID,0)  as numWareHouseItemID,
										(CASE WHEN charItemType<>''S'' AND charItemType<>''N'' AND ISNULL(bitKitParent,0)=0 THEN         
										 (CASE 
											WHEN I.bitAllowBackOrder = 1 THEN 1
											WHEN (ISNULL(WAll.numTotalOnHand,0)<=0) THEN 0
											ELSE 1
										END) ELSE 1 END ) as bitInStock,
										(
											CASE WHEN ISNULL(bitKitParent,0) = 1
											THEN
												(CASE 
													WHEN ((SELECT COUNT(*) FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID = IInner.numItemCode WHERE ID.numItemKitID = I.numItemCode AND ISNULL(IInner.bitKitParent,0) = 1)) > 0 
													THEN 1 
													ELSE 0 
												END)
											ELSE
												0
											END
										) bitHasChildKits,
										(case when charItemType<>''S'' AND charItemType<>''N'' AND ISNULL(bitKitParent,0)=0 then         
										 (Case when ',@bitShowInStock,'=1  then 
										 (CASE 
											WHEN I.bitAllowBackOrder = 1 AND I.numDomainID <> 172 AND ISNULL(WAll.numTotalOnHand,0)<=0 AND ISNULL(I.numVendorID,0) > 0 AND dbo.GetVendorPreferredMethodLeadTime(I.numVendorID,0) > 0 THEN CONCAT(''<font class="vendorLeadTime" style="color:green">Usually ships in '',dbo.GetVendorPreferredMethodLeadTime(I.numVendorID,0),'' days</font>'')
											WHEN I.bitAllowBackOrder = 1 AND ISNULL(WAll.numTotalOnHand,0)<=0 THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)
											WHEN (ISNULL(WAll.numTotalOnHand,0)<=0) THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:red">ON HOLD</font>'' ELSE ''<font style="color:red">Out Of Stock</font>'' END)
											ELSE (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)  
										END) else '''' end) ELSE '''' END ) as InStock,
										ISNULL(numSaleUnit,0) AS numUOM,
										ISNULL(vcUnitName,'''') AS vcUOMName,
										 ',@UOMConversionFactor,' AS UOMConversionFactor,
										I.numCreatedBy,
										I.numBarCodeId,
										I.[vcManufacturer],
										(SELECT  COUNT(*) FROM  Review where vcReferenceId = CONVERT(VARCHAR(50) ,I.numItemCode) and numDomainId = I.numDomainId and numSiteId = ', @numSiteId,' AND bitHide = 0 ) as ReviewCount ,
										(SELECT  COUNT(*) FROM  Ratings where vcReferenceId = CONVERT(VARCHAR(50) ,I.numItemCode) and numDomainId = I.numDomainId and numSiteId = ', @numSiteId,'  ) as RatingCount ,
										(select top 1 vcMetaKeywords  from MetaTags where numReferenceID = I.numItemCode and tintMetaTagFor =2) as vcMetaKeywords,
										(select top 1 vcMetaDescription  from MetaTags where numReferenceID = I.numItemCode and tintMetaTagFor =2) as vcMetaDescription ,
										(SELECT vcCategoryName  FROM  dbo.Category WHERE numCategoryID =(SELECT TOP 1 numCategoryID FROM dbo.ItemCategory WHERE numItemID = I.numItemCode)) as vcCategoryName
										,ISNULL(TablePromotion.numTotalPromotions,0) numTotalPromotions
										,ISNULL(TablePromotion.vcPromoDesc,'''') vcPromoDesc
										,ISNULL(TablePromotion.bitRequireCouponCode,0) bitRequireCouponCode
										from Item I 
										OUTER APPLY (SELECT * FROM dbo.fn_GetItemPromotions(',@numDomainID,',',@numDivisionID,',I.numItemCode,I.numItemClassification',',',@numDefaultRelationship,',',@numDefaultProfile,',2)) TablePromotion ' + 
										(CASE WHEN @tintPreLoginPriceLevel > 0 THEN CONCAT(' OUTER APPLY(SELECT decDiscount AS monPrice FROM PricingTable WHERE tintRuleType=3 AND numItemCode=I.numItemCode AND ISNULL(numCurrencyID,0) = 0 ORDER BY numPricingID OFFSET ',@tintPreLoginPriceLevel-1,' ROWS FETCH NEXT 1 ROWS ONLY) As PricingOption ') ELSE '' END)
										,'                 
										left join  WareHouseItems W                
										on W.numItemID=I.numItemCode and numWareHouseID= ',@numWareHouseID,'
										 OUTER APPLY (SELECT SUM(numOnHand) numTotalOnHand FROM WareHouseItems W 
																							  WHERE  W.numItemID = I.numItemCode
																							  AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''',@vcWarehouseIDs,''','',''))
																							  AND W.numDomainID = ',@numDomainID,') AS WAll
										left join  eCommerceDTL E          
										on E.numDomainID=I.numDomainID     
										LEFT JOIN UOM u ON u.numUOMId=I.numSaleUnit    
										where ISNULL(I.IsArchieve,0)=0 AND I.numDomainID=',@numDomainID,' AND I.numItemCode=',@numItemCode,
										CASE WHEN (SELECT COUNT(*) FROM [dbo].[EcommerceRelationshipProfile] WHERE numSiteID=@numSiteID AND numRelationship = @numDefaultRelationship AND numProfile = @numDefaultProfile) > 0
										THEN
											CONCAT(' AND I.numItemClassification NOT IN (SELECT numItemClassification FROM EcommerceRelationshipProfile ERP INNER JOIN ECommerceItemClassification EIC ON EIC.numECommRelatiionshipProfileID=ERP.ID WHERE ERP.numSiteID=',@numSiteID,' AND ERP.numRelationship=',@numDefaultRelationship,' AND ERP.numProfile=', @numDefaultProfile,')')
										ELSE
											''
										END
										,'           
										group by I.numItemCode, vcItemName, txtItemDesc, charItemType, monListPrice,numItemClassification, bitTaxable, vcSKU, bitKitParent, bitCalAmtBasedonDepItems, bitAssembly, tintKitAssemblyPriceBasedOn, WAll.numTotalOnHand,         
										I.numDomainID,I.numModifiedBy,  bitSerialized, vcModelID,numItemGroup, fltWeight,fltHeight,fltWidth,          
										fltLength,bitFreeShipping,bitAllowBackOrder,bitShowInStock,bitShowQOnHand ,numWareHouseItemID,vcUnitName,I.numCreatedBy,I.numBarCodeId,W.[monWListPrice],I.[vcManufacturer],
										numSaleUnit, I.numVendorID, I.bitMatrix,numTotalPromotions,vcPromoDesc,bitRequireCouponCode ' + (CASE WHEN @tintPreLoginPriceLevel > 0 THEN ',PricingOption.monPrice' ELSE '' END) + ')')

										set @strSql=@strSql+' select numItemCode,vcItemName,txtItemDesc,charItemType, bitMatrix, monListPrice,monMSRP,numItemClassification, bitTaxable, vcSKU, bitKitParent, bitCalAmtBasedonDepItems,                  
										tblItem.numModifiedBy, vcImages,vcDocuments, bitSerialized, vcModelID, numItemGroup,numOnHand,numOnOrder,numReorder,numAllocation, 
										numBackOrder, fltWeight, fltHeight, fltWidth, fltLength, bitFreeShipping,bitAllowBackOrder,bitShowInStock,
										bitShowQOnHand,numWareHouseItemID,bitInStock,bitHasChildKits,InStock,numUOM,vcUOMName,UOMConversionFactor,tblItem.numCreatedBy,numBarCodeId,vcManufacturer,ReviewCount,RatingCount,Shipping,isNull(vcMetaKeywords,'''') as vcMetaKeywords ,isNull(vcMetaDescription,'''') as vcMetaDescription,isNull(vcCategoryName,'''') as vcCategoryName,numTotalPromotions,vcPromoDesc,bitRequireCouponCode'

	set @strSql=@strSql+ ' from tblItem ORDER BY numOnHand DESC'
	PRINT CAST(@strSql AS NTEXT)
	exec (@strSql)


	declare @tintOrder as tinyint                                                  
	declare @vcFormFieldName as varchar(50)                                                  
	declare @vcListItemType as varchar(1)                                             
	declare @vcAssociatedControlType varchar(10)                                                  
	declare @numListID AS numeric(9)                                                  
	declare @WhereCondition varchar(2000)                       
	Declare @numFormFieldId as numeric  
	DECLARE @vcFieldType CHAR(1)
	DECLARE @GRP_ID INT
                  
	set @tintOrder=0                                                  
	set @WhereCondition =''                 
                   
              
	CREATE TABLE #tempAvailableFields
	(
		numFormFieldId NUMERIC(9)
		,vcFormFieldName NVARCHAR(50)
		,vcFieldType CHAR(1)
		,vcAssociatedControlType NVARCHAR(50)
		,numListID NUMERIC(18)
		,vcListItemType CHAR(1)
		,intRowNum INT
		,vcItemValue VARCHAR(3000)
		,GRP_ID INT
	)
   
	INSERT INTO #tempAvailableFields 
	(
		numFormFieldId
		,vcFormFieldName
		,vcFieldType
		,vcAssociatedControlType
        ,numListID
		,vcListItemType
		,intRowNum
		,GRP_ID
	)                         
    SELECT 
		Fld_id
		,REPLACE(Fld_label,' ','') AS vcFormFieldName
		,CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType
		,fld_type as vcAssociatedControlType
		,ISNULL(C.numListID, 0) numListID
		,CASE WHEN C.numListID > 0 THEN 'L' ELSE '' END vcListItemType
		,ROW_NUMBER() OVER (ORDER BY Fld_id asc) AS intRowNum
		,GRP_ID
    FROM 
		CFW_Fld_Master C 
	LEFT JOIN 
		CFW_Validation V 
	ON 
		V.numFieldID = C.Fld_id
    WHERE 
		C.numDomainID = @numDomainId
        AND GRP_ID  = 5

    SELECT TOP 1 
		@tintOrder=intRowNum
		,@numFormFieldId=numFormFieldId
		,@vcFormFieldName=vcFormFieldName
		,@vcFieldType=vcFieldType
		,@vcAssociatedControlType=vcAssociatedControlType
		,@numListID=numListID
		,@vcListItemType=vcListItemType
		,@GRP_ID=GRP_ID
	FROM 
		#tempAvailableFields 
	ORDER BY 
		intRowNum ASC
   
	while @tintOrder>0                                                  
	begin                   
		IF @GRP_ID=5
		BEGIN
			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
			BEGIN  
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT Fld_Value FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END   
			ELSE IF @vcAssociatedControlType = 'CheckBox'
			BEGIN      
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT case when isnull(Fld_Value,0)=0 then 'No' when isnull(Fld_Value,0)=1 then 'Yes' END FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END                
			ELSE IF @vcAssociatedControlType = 'DateField'           
			BEGIN   
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT dbo.FormatedDateFromDate(Fld_Value,@numDomainId) FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END                
			ELSE IF @vcAssociatedControlType = 'SelectBox'           
			BEGIN 
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT L.vcData FROM CFW_FLD_Values_Item CFW left Join ListDetails L
					on L.numListItemID=CFW.Fld_Value                
					WHERE CFW.Fld_Id=@numFormFieldId AND CFW.RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END   
		END    
 
		SELECT TOP 1 
			@tintOrder=intRowNum
			,@numFormFieldId=numFormFieldId
			,@vcFormFieldName=vcFormFieldName
			,@vcFieldType=vcFieldType
			,@vcAssociatedControlType=vcAssociatedControlType
			,@numListID=numListID
			,@vcListItemType=vcListItemType
			,@GRP_ID=GRP_ID
		FROM 
			#tempAvailableFields 
		WHERE 
			intRowNum > @tintOrder 
		ORDER BY 
			intRowNum ASC
 
	   IF @@rowcount=0 set @tintOrder=0                                                  
	END   


	INSERT INTO #tempAvailableFields 
	(
		numFormFieldId
		,vcFormFieldName
		,vcFieldType
		,vcAssociatedControlType
		,numListID
		,vcListItemType
		,intRowNum
		,vcItemValue
		,GRP_ID
	)                         
	SELECT 
		Fld_id
		,REPLACE(Fld_label,' ','') AS vcFormFieldName
		,CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType
		,fld_type as vcAssociatedControlType
		,ISNULL(C.numListID, 0) numListID
		,CASE WHEN C.numListID > 0 THEN 'L' ELSE '' END vcListItemType
		,ROW_NUMBER() OVER (ORDER BY Fld_id asc) AS intRowNum
		,''
		,GRP_ID
	FROM 
		CFW_Fld_Master C 
	LEFT JOIN 
		CFW_Validation V 
	ON 
		V.numFieldID = C.Fld_id
	WHERE 
		C.numDomainID = @numDomainId
		AND GRP_ID = 9

	IF ISNULL(@bitMatrix,0) = 1 AND ISNULL(@numItemGroup,0) > 0
	BEGIN
		UPDATE
			TEMP
		SET
			TEMP.vcItemValue = FLD_ValueName
		FROM
			#tempAvailableFields TEMP
		INNER JOIN
		(
			SELECT 
				CFW_Fld_Master.fld_id
				,CASE 
					WHEN CFW_Fld_Master.fld_type='SelectBox' THEN dbo.GetListIemName(Fld_Value)
					WHEN CFW_Fld_Master.fld_type='CheckBox' THEN CASE WHEN isnull(Fld_Value,0)=1 THEN 'Yes' ELSE 'No' END
					ELSE CAST(Fld_Value AS VARCHAR)
				END AS FLD_ValueName
			FROM 
				CFW_Fld_Master 
			INNER JOIN
				ItemGroupsDTL 
			ON 
				CFW_Fld_Master.Fld_id = ItemGroupsDTL.numOppAccAttrID
				AND ItemGroupsDTL.tintType = 2
			LEFT JOIN
				ItemAttributes
			ON
				CFW_Fld_Master.Fld_id = ItemAttributes.FLD_ID
				AND ItemAttributes.numItemCode = @numItemCode
			LEFT JOIN
				ListDetails LD
			ON
				CFW_Fld_Master.numlistid = LD.numListID
			WHERE
				CFW_Fld_Master.numDomainID = @numDomainId
				AND ItemGroupsDTL.numItemGroupID = @numItemGroup
			GROUP BY
				CFW_Fld_Master.Fld_label
				,CFW_Fld_Master.fld_id
				,CFW_Fld_Master.fld_type
				,CFW_Fld_Master.bitAutocomplete
				,CFW_Fld_Master.numlistid
				,CFW_Fld_Master.vcURL
				,ItemAttributes.FLD_Value 
		) T1
		ON
			TEMP.numFormFieldId = T1.fld_id
		WHERE
			TEMP.GRP_ID=9
	END
  
	SELECT * FROM #tempAvailableFields

	DROP TABLE #tempAvailableFields
END
GO
--SELECT * FROM Category
--- Alterd By Chintan Prajapati
-- EXEC dbo.USP_ItemsForECommerce 9,60,1,1,15,0,''
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemsForECommerce' ) 
    DROP PROCEDURE USP_ItemsForECommerce
GO
CREATE PROCEDURE [dbo].[USP_ItemsForECommerce]
    @numSiteID AS NUMERIC(9) = 0,
    @numCategoryID AS NUMERIC(18) = 0,
    @numWareHouseID AS NUMERIC(9) = 0,
    @SortBy NVARCHAR(MAX) = 0,
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT = 0 OUTPUT,
    @SearchText AS NVARCHAR(MAX) = '',--Comma seperated item ids
    @FilterQuery AS NVARCHAR(MAX) = '',----Comma seperated Attributes Ids
    @FilterRegularCondition AS NVARCHAR(MAX),
    @FilterCustomFields AS NVARCHAR(MAX),
    @FilterCustomWhere AS NVARCHAR(MAX),
	@numDivisionID AS NUMERIC(18,0) = 0,
	@numManufacturerID AS NUMERIC(18,0)=0,
	@FilterItemAttributes AS NVARCHAR(MAX),
	@numESItemCodes AS NVARCHAR(MAX)=''
AS 
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @numDomainID AS NUMERIC(9)
	DECLARE @numDefaultRelationship AS NUMERIC(18,0)
	DECLARE @numDefaultProfile AS NUMERIC(18,0)
	DECLARE @tintPreLoginPriceLevel AS TINYINT
	DECLARE @tintPriceLevel AS TINYINT
	DECLARE @tintDisplayCategory AS TINYINT
	DECLARE @bitAutoSelectWarehouse AS BIT

	SELECT  
		@numDomainID = numDomainID
    FROM 
		[Sites]
    WHERE 
		numSiteID = @numSiteID
        
	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT 
			@numDefaultRelationship=ISNULL(numCompanyType,0)
			,@numDefaultProfile=ISNULL(vcProfile,0)
			,@tintPriceLevel=ISNULL(tintPriceLevel,0) 
		FROM 
			DivisionMaster 
		INNER JOIN 
			CompanyInfo 
		ON 
			DivisionMaster.numCompanyID=CompanyInfo.numCompanyId 
		WHERE 
			numDivisionID=@numDivisionID 
			AND DivisionMaster.numDomainID=@numDomainID
	END

	--KEEP THIS BELOW ABOVE IF CONDITION
	SELECT 
		@tintDisplayCategory = ISNULL(bitDisplayCategory,0)
		,@numDefaultRelationship=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultRelationship ELSE numRelationshipId END)
		,@numDefaultProfile=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultProfile ELSE numProfileId END)
		,@tintPreLoginPriceLevel=(CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN (CASE WHEN @numDivisionID > 0 THEN (CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN ISNULL(@tintPriceLevel,0) ELSE 0 END) ELSE ISNULL(tintPreLoginProceLevel,0) END) ELSE 0 END)
		,@bitAutoSelectWarehouse = ISNULL(bitAutoSelectWarehouse,0)
	FROM 
		dbo.eCommerceDTL 
	WHERE 
		numSiteID = @numSiteId
		
	CREATE TABLE #tmpItemCat(numCategoryID NUMERIC(18,0))
	CREATE TABLE #tmpItemCode(numItemCode NUMERIC(18,0))
		
	IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
	BEGIN			
		DECLARE @FilterCustomFieldIds AS NVARCHAR(MAX)
		SET @FilterCustomFieldIds = Replace(@FilterCustomFields,'[','')
		SET @FilterCustomFieldIds = Replace(@FilterCustomFieldIds,']','')
			
		DECLARE @strCustomSql AS NVARCHAR(MAX)
		SET @strCustomSql = 'INSERT INTO #tmpItemCode(numItemCode)
								SELECT DISTINCT T.RecId FROM 
														(
															SELECT * FROM 
																(
																	SELECT  t2.RecId, t1.fld_id, t2.Fld_Value FROM CFW_Fld_Master t1 
																	JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID 
																	WHERE t1.Grp_id  In (5,9)
																	AND t1.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
																	AND t1.fld_type <> ''Link'' 
																	AND t1.fld_id IN (' + @FilterCustomFieldIds + ')
																) p  PIVOT (MAX([Fld_Value]) FOR fld_id IN (' + @FilterCustomFields + ')) AS pvt 
														) T  WHERE 1=1 AND ' + @FilterCustomWhere
								 
		EXEC SP_EXECUTESQL @strCustomSql								 					 
	END
		
    DECLARE @strSQL NVARCHAR(MAX)
    DECLARE @firstRec AS INTEGER
    DECLARE @lastRec AS INTEGER
    DECLARE @Where NVARCHAR(MAX)
    DECLARE @SortString NVARCHAR(MAX)
    DECLARE @searchPositionColumn NVARCHAR(MAX)
	DECLARE @searchPositionColumnGroupBy NVARCHAR(MAX)
    DECLARE @row AS INTEGER
    DECLARE @data AS VARCHAR(100)
    DECLARE @dynamicFilterQuery NVARCHAR(MAX)
    DECLARE @checkFldType VARCHAR(100)
    DECLARE @count NUMERIC(9,0)
    DECLARE @whereNumItemCode NVARCHAR(MAX)
    DECLARE @filterByNumItemCode VARCHAR(100)
        
	SET @SortString = ''
    set @searchPositionColumn = ''
	SET @searchPositionColumnGroupBy = ''
    SET @strSQL = ''
	SET @whereNumItemCode = ''
	SET @dynamicFilterQuery = ''
		 
    SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
    SET @lastRec = ( @CurrentPage * @PageSize + 1 )
                
	DECLARE @vcWarehouseIDs AS VARCHAR(1000)
    IF (@numWareHouseID = 0 OR @bitAutoSelectWarehouse = 1)
    BEGIN
		IF @bitAutoSelectWarehouse = 1
		BEGIN
			SELECT @vcWarehouseIDs = ISNULL( STUFF((SELECT DISTINCT ',' + CAST(numWarehouseID AS VARCHAR(10)) FROM [dbo].[Warehouses] WHERE [Warehouses].[numDomainID] = @numDomainID FOR XML PATH('')),1, 1, '') , '') 
		END
		ELSE
		BEGIN
			SELECT 
				@numWareHouseID = ISNULL(numDefaultWareHouseID,0)
			FROM 
				[eCommerceDTL]
			WHERE 
				[numDomainID] = @numDomainID	

			SET @vcWarehouseIDs = @numWareHouseID
		END
    END
	ELSE
	BEGIN
		SET @vcWarehouseIDs = @numWareHouseID
	END

	--PRIAMRY SORTING USING CART DROPDOWN
	DECLARE @Join AS NVARCHAR(MAX)
	DECLARE @SortFields AS NVARCHAR(MAX)
	DECLARE @vcSort AS VARCHAR(MAX)

	SET @Join = ''
	SET @SortFields = ''
	SET @vcSort = ''

	IF @SortBy = ''
	BEGIN
		SET @SortString = ' vcItemName Asc '
	END
	ELSE IF CHARINDEX('~0',@SortBy) > 0 OR CHARINDEX('~1',@SortBy) > 0
	BEGIN
		DECLARE @fldID AS NUMERIC(18,0)
		DECLARE @RowNumber AS VARCHAR(MAX)
				
		SELECT * INTO #fldValues FROM dbo.SplitIDsWithPosition(@SortBy,'~')
		SELECT @RowNumber = RowNumber, @fldID = id FROM #fldValues WHERE RowNumber = 1
		SELECT @vcSort = CASE WHEN Id = 0 THEN 'ASC' ELSE 'DESC' END FROM #fldValues WHERE RowNumber = 2
				
		SET @Join = @Join + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber + ' ON I.numItemCode = CFVI' + @RowNumber + '.RecID AND CFVI' + @RowNumber + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID)
		---SET @SortString = 'CFVI' + @RowNumber + '.fld_Value' 
		SET @SortString = '[fld_Value' + @RowNumber + '] ' --+ @vcSort
		SET @SortFields = ', CFVI' + @RowNumber + '.fld_Value [fld_Value' + @RowNumber + ']'
	END
	ELSE
	BEGIN	
		IF CHARINDEX('monListPrice',@SortBy) > 0
		BEGIN
			DECLARE @bitSortPriceMode AS BIT
			SELECT @bitSortPriceMode = ISNULL(bitSortPriceMode,0) FROM dbo.eCommerceDTL WHERE numSiteId = @numSiteID
			
			IF @bitSortPriceMode = 1
			BEGIN
				SET @SortString = ' '
			END
			ELSE
			BEGIN
				SET @SortString = @SortBy	
			END
		END
		ELSE
		BEGIN
			SET @SortString = @SortBy	
		END	
	END	

	--SECONDARY SORTING USING DEFAULT SETTINGS FROM ECOMMERCE
	SELECT 
		ROW_NUMBER() OVER (ORDER BY Custom,vcFieldName) AS [RowID]
		,* 
	INTO 
		#tempSort 
	FROM 
	(
		SELECT 
			CONVERT(VARCHAR(9),numFieldID)+'~0' AS numFieldID,
			ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
			0 AS Custom,
			vcDbColumnName 
		FROM 
			View_DynamicColumns
		WHERE 
			numFormID = 84 
			AND ISNULL(bitSettingField,0) = 1 
			AND ISNULL(bitDeleted,0)=0 
			AND ISNULL(bitCustom,0) = 0
			AND numDomainID = @numDomainID	       
		UNION     
		SELECT 
			CONVERT(VARCHAR(9),numFieldID)+'~1' AS numFieldID,
			vcFieldName ,
			1 AS Custom,
			'' AS vcDbColumnName
		FROM 
			View_DynamicCustomColumns          
		WHERE 
			numFormID = 84
			AND numDomainID = @numDomainID
			AND grp_id IN (5,9)
			AND vcAssociatedControlType = 'TextBox' 
			AND ISNULL(bitCustom,0) = 1 
			AND tintPageType=1  
	) TABLE1 
	ORDER BY 
		Custom,vcFieldName
		
	--SELECT * FROM #tempSort
	DECLARE @DefaultSort AS NVARCHAR(MAX)
	DECLARE @DefaultSortFields AS NVARCHAR(MAX)
	DECLARE @DefaultSortJoin AS NVARCHAR(MAX)
	DECLARE @intCnt AS INT
	DECLARE @intTotalCnt AS INT	
	DECLARE @strColumnName AS VARCHAR(100)
	DECLARE @bitCustom AS BIT
	DECLARE @FieldID AS VARCHAR(100)				

	SET @DefaultSort = ''
	SET @DefaultSortFields = ''
	SET @DefaultSortJoin = ''		
	SET @intCnt = 0
	SET @intTotalCnt = ( SELECT COUNT(*) FROM #tempSort )
	SET @strColumnName = ''
		
	IF (SELECT ISNULL(bitEnableSecSorting,0) FROM EcommerceDtl WHERE numSiteID = @numSiteID ) = 1
	BEGIN
		CREATE TABLE #fldDefaultValues(RowNumber INT,Id VARCHAR(1000))

		WHILE(@intCnt < @intTotalCnt)
		BEGIN
			SET @intCnt = @intCnt + 1
			SELECT @FieldID = numFieldID, @strColumnName = vcDbColumnName, @bitCustom = Custom FROM #tempSort WHERE RowID = @intCnt

			IF @bitCustom = 0
			BEGIN
				SET @DefaultSort =  @strColumnName + ',' + @DefaultSort 
			END
			ELSE
			BEGIN
				DECLARE @fldID1 AS NUMERIC(18,0)
				DECLARE @RowNumber1 AS VARCHAR(MAX)
				DECLARE @vcSort1 AS VARCHAR(10)
				DECLARE @vcSortBy AS VARCHAR(1000)
				SET @vcSortBy = CONVERT(VARCHAR(100),@FieldID) + '~' + '0'

				INSERT INTO #fldDefaultValues(RowNumber,Id) SELECT RowNumber, id FROM dbo.SplitIDsWithPosition(@vcSortBy,'~')
				SELECT @RowNumber1 = RowNumber + 100 + @intCnt, @fldID1 = id FROM #fldDefaultValues WHERE RowNumber = 1
				SELECT @vcSort1 = 'ASC' 
						
				SET @DefaultSortJoin = @DefaultSortJoin + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber1 + ' ON I.numItemCode = CFVI' + @RowNumber1 + '.RecID AND CFVI' + @RowNumber1 + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID1)
				SET @DefaultSort = '[fld_Value' + @RowNumber1 + '] ' + ',' + @DefaultSort 
				SET @DefaultSortFields = @DefaultSortFields + ', CFVI' + @RowNumber1 + '.fld_Value [fld_Value' + @RowNumber1 + ']'
			END
		END

		IF LEN(@DefaultSort) > 0
		BEGIN
			SET  @DefaultSort = LEFT(@DefaultSort,LEN(@DefaultSort) - 1)
			SET @SortString = @SortString + ',' 
		END
	END
                                		
    IF LEN(@SearchText) > 0
	BEGIN
		IF CHARINDEX('SearchOrder',@SortString) > 0
		BEGIN
			SET @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 
			SET @searchPositionColumnGroupBy = ' ,SearchItems.RowNumber ' 
		END

		SET @Where = ' INNER JOIN dbo.SplitIDsWithPosition('''+ @SearchText +''','','') AS SearchItems 
                        ON SearchItems.Id = I.numItemCode  and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID) + ' WHERE ISNULL(IsArchieve,0) = 0'
	END
	ELSE IF @SearchText = ''
	BEGIN
		SET @Where = ' WHERE 1=2 AND I.numDomainID=' + CONVERT(VARCHAR(15), @numDomainID)
		+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
		+ ' AND ISNULL(IsArchieve,0) = 0'
	END
	ELSE
	BEGIN
		SET @Where = CONCAT(' WHERE 1=1 AND I.numDomainID=',@numDomainID,' and SC.numSiteID = ',@numSiteID,' AND ISNULL(IsArchieve,0) = 0')

		IF @tintDisplayCategory = 1
		BEGIN
					
			;WITH CTE AS(
			SELECT C.numCategoryID,C.tintLevel,C.numDepCategory,CASE WHEN tintLevel=1 THEN 1 ELSE 0 END AS bitChild 
				FROM SiteCategories SC INNER JOIN Category C ON SC.numCategoryID=C.numCategoryID WHERE SC.numSiteID=@numSiteID AND (C.numCategoryID = @numCategoryID OR ISNULL(@numCategoryID,0) = 0) AND numDomainID = @numDomainID
			UNION ALL
			SELECT  C.numCategoryID,C.tintLevel,C.numDepCategory,CTE.bitChild FROM Category C JOIN CTE ON 
			C.numDepCategory=CTE.numCategoryID AND CTE.bitChild=1 AND numDomainID = @numDomainID)
					
			INSERT INTO #tmpItemCat(numCategoryID)												 
			SELECT DISTINCT C.numCategoryID FROM CTE C JOIN dbo.ItemCategory IC ON C.numCategoryID = IC.numCategoryID 
												JOIN View_Item_Warehouse ON View_Item_Warehouse.numDOmainID=@numDomainID AND  View_Item_Warehouse.numItemID=IC.numItemID
												WHERE ISNULL(View_Item_Warehouse.numAvailable,0) > 0
		END

	END

	SET @strSQL = @strSQL + CONCAT(' WITH Items AS 
									( 
											SELECT
											I.numDomainID
											,I.numItemCode
											,ROW_NUMBER() OVER ( Partition by I.numItemCode Order by I.numItemCode) RowID
											,I.bitMatrix
											,I.bitKitParent
											,I.numItemGroup
											,vcItemName
											,I.bintCreatedDate
											,vcManufacturer
											,ISNULL(txtItemDesc,'''') txtItemDesc
											,vcSKU
											,fltHeight
											,fltWidth
											,fltLength
											,vcModelID
											,fltWeight
											,numBaseUnit
											,numSaleUnit
											,numPurchaseUnit
											,bitFreeShipping
											,C.vcCategoryName
											,C.vcDescription as CategoryDesc
											,charItemType
											,monListPrice
											,bitAllowBackOrder
											,bitShowInStock
											,numVendorID
											,numItemClassification',@searchPositionColumn,@SortFields,@DefaultSortFields,'
											,SUM(numOnHand) numOnHand
											,SUM(numAllocation) numAllocation
											' + (CASE WHEN LEN(ISNULL(@numESItemCodes,'')) > 0 THEN ',ISNULL(C.numCategoryID,0) numCategoryID' ELSE '' END)  + '
											,ISNULL(I.numManufacturer,0) numManufacturer
											FROM      
											Item AS I
											INNER JOIN eCommerceDTL E ON E.numDomainID = I.numDomainID AND E.numSiteId=',@numSiteID,'
											LEFT JOIN WareHouseItems WI ON WI.numDomainID=' + CONVERT(VARCHAR(10),@numDomainID) + ' AND WI.numItemID=I.numItemCode AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
											INNER JOIN ItemCategory IC ON I.numItemCode = IC.numItemID
											INNER JOIN Category C ON IC.numCategoryID = C.numCategoryID
											INNER JOIN SiteCategories SC ON IC.numCategoryID = SC.numCategoryID 
											',@Join,' ',@DefaultSortJoin)

	IF LEN(ISNULL(@FilterItemAttributes,'')) > 0
	BEGIN
		SET @strSQL = @strSQL + @FilterItemAttributes
	END

	IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
	BEGIN
		SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCode TP ON TP.numItemCode = I.numItemCode ' 
	END
			
	IF (SELECT COUNT(*) FROM #tmpItemCat) > 0
	BEGIN
		SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCat TC ON TC.numCategoryID = IC.numCategoryID '
	END
	ELSE IF @numManufacturerID>0
	BEGIN
		SET @Where = REPLACE(@Where,'WHERE',' WHERE I.numManufacturer = ' + CONVERT(VARCHAR(10),@numManufacturerID) + ' AND ')
	END 
	ELSE IF @numCategoryID>0
	BEGIN
		SET @Where = REPLACE(@Where,'WHERE',' WHERE IC.numCategoryID = ' + CONVERT(VARCHAR(10),@numCategoryID) + ' AND ')
	END 
			
	IF (SELECT COUNT(*) FROM [dbo].[EcommerceRelationshipProfile] WHERE numSiteID=@numSiteID AND numRelationship = @numDefaultRelationship AND numProfile = @numDefaultProfile) > 0
	BEGIN
		SET @Where = @Where + CONCAT(' AND I.numItemClassification NOT IN (SELECT numItemClassification FROM EcommerceRelationshipProfile ERP INNER JOIN ECommerceItemClassification EIC ON EIC.numECommRelatiionshipProfileID=ERP.ID WHERE ERP.numSiteID=',@numSiteID,' AND ERP.numRelationship=',@numDefaultRelationship,' AND ERP.numProfile=', @numDefaultProfile,')')
	END

	SET @strSQL = @strSQL + @Where
	
	IF LEN(@FilterRegularCondition) > 0
	BEGIN
		SET @strSQL = @strSQL + @FilterRegularCondition
	END                                         

    IF LEN(@whereNumItemCode) > 0
    BEGIN
			SET @strSQL = @strSQL + @filterByNumItemCode	
	END

    SET @strSQL = CONCAT(@strSQL,' GROUP BY
										I.numDomainID
										,I.numItemCode
										,I.bitMatrix
										,I.bitKitParent
										,I.numItemGroup
										,vcItemName
										,I.bintCreatedDate
										,vcManufacturer
										,txtItemDesc
										,vcSKU
										,fltHeight
										,fltWidth
										,fltLength
										,vcModelID
										,fltWeight
										,numBaseUnit
										,numSaleUnit
										,numPurchaseUnit
										,bitFreeShipping
										,C.vcCategoryName
										,C.vcDescription
										,charItemType
										,monListPrice
										,bitAllowBackOrder
										,bitShowInStock
										,numVendorID
										,numItemClassification
										' + (CASE WHEN LEN(ISNULL(@numESItemCodes,'')) > 0 THEN ',C.numCategoryID' ELSE '' END)  + '
										,I.numManufacturer',@searchPositionColumnGroupBy,@SortFields,@DefaultSortFields,(CASE WHEN ISNULL(@tintDisplayCategory,0) = 1 THEN ' HAVING 1 = (CASE WHEN (I.charItemType=''P'' AND ISNULL(I.bitKitParent,0) = 0) THEN CASE WHEN (ISNULL(SUM(numOnHand), 0) > 0 OR ISNULL(SUM(numAllocation), 0) > 0) THEN 1 ELSE 0 END ELSE 1 END) ' ELSE '' END), ') SELECT * INTO #TEMPItems FROM Items;')
                                        
	IF @numDomainID IN (204,214,215)
	BEGIN
		SET @strSQL = @strSQL + 'DELETE FROM 
									#TEMPItems
								WHERE 
									numItemCode IN (
														SELECT 
															F.numItemCode
														FROM 
															#TEMPItems AS F
														WHERE 
															ISNULL(F.bitMatrix,0) = 1 
															AND EXISTS (
																		SELECT 
																			t1.vcItemName, t1.bitMatrix, t1.numItemGroup, Count(t1.numItemCode)
																		FROM 
																			#TEMPItems t1
																		WHERE 
																			t1.vcItemName = F.vcItemName
																			AND t1.bitMatrix = 1
																			AND t1.numItemGroup = F.numItemGroup
																		GROUP BY 
																			t1.vcItemName, t1.bitMatrix, t1.numItemGroup
																		HAVING 
																			Count(t1.numItemCode) > 1
																	)
													)
									AND numItemCode NOT IN (
																			SELECT 
																				Min(numItemCode)
																			FROM 
																				#TEMPItems AS F
																			WHERE
																				ISNULL(F.bitMatrix,0) = 1 
																				AND Exists (
																							SELECT 
																								t2.vcItemName, t2.bitMatrix, t2.numItemGroup, Count(t2.numItemCode)
																							FROM 
																								#TEMPItems t2
																							WHERE 
																								t2.vcItemName = F.vcItemName
																								AND t2.bitMatrix = 1
																								AND t2.numItemGroup = F.numItemGroup
																							GROUP BY 
																								t2.vcItemName, t2.bitMatrix, t2.numItemGroup
																							HAVING 
																								Count(t2.numItemCode) > 1
																						)
																			GROUP BY 
																				vcItemName, bitMatrix, numItemGroup
																		);'

	END
									  
									  
	SET @strSQL = @strSQL + ' WITH ItemSorted AS (SELECT  ROW_NUMBER() OVER ( ORDER BY  ' + @SortString + @DefaultSort + ' ' + @vcSort + ' ) AS Rownumber,* FROM  #TEMPItems WHERE RowID = 1)'

    DECLARE @fldList AS NVARCHAR(MAX)
	DECLARE @fldList1 AS NVARCHAR(MAX)
	SELECT @fldList=COALESCE(@fldList + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_C') FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=5
	SELECT @fldList1=COALESCE(@fldList1 + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_' + CAST(Fld_Id AS VARCHAR)) FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=9
            
	/***Changed By: Chirag R:****************/
	/***Note: Changes for getting records Elastic Search Affected Items ********/
    --SET @strSQL = @strSQL + 'SELECT * INTO #tmpPagedItems FROM ItemSorted WHERE Rownumber > ' + CONVERT(VARCHAR(10), @firstRec) + '
    --                            AND   Rownumber < ' + CONVERT(VARCHAR(10), @lastRec) + '
    --                            order by Rownumber '

	DECLARE @bitGetItemForElasticSearch BIT;
	SET @bitGetItemForElasticSearch = IIF(LEN(@numESItemCodes) > 0,1,0)
    SET @strSQL = @strSQL + 'SELECT * INTO #tmpPagedItems FROM ItemSorted ';
	
	IF(@bitGetItemForElasticSearch = 1)
	BEGIN
		SET @strSQL = @strSQL + ' WHERE numItemCode IN (' + @numESItemCodes + ')'
	END 
	ELSE
	BEGIN
		SET @strSQL = @strSQL + ' WHERE Rownumber > ' + CONVERT(VARCHAR(10), @firstRec) + '
							AND   Rownumber < ' + CONVERT(VARCHAR(10), @lastRec)
	END
    SET @strSQL = @strSQL + ' order by Rownumber '

    /***Changes Completed: Chirag R:****************/                

	SET @strSQL = CONCAT(@strSQL,'SELECT  
								Rownumber
								,I.numItemCode
								,vcCategoryName
								,CategoryDesc
								,vcItemName
								,txtItemDesc
								,vcManufacturer
								,vcSKU
								,fltHeight
								,fltWidth
								,fltLength
								,vcModelID
								,fltWeight
								,bitFreeShipping
								,', 
									(CASE 
									WHEN @tintPreLoginPriceLevel > 0 
									THEN 'ISNULL(PricingOption.monPrice,ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0))' 
									ELSE 'ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0)' 
									END)
									,' AS monListPrice,
									ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0) monMSRP					   
									',(CASE WHEN @numDomainID = 172 THEN ',ISNULL(CASE 
											WHEN tintRuleType = 1 AND tintDiscountType = 1
											THEN ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - (ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) * ( decDiscount / 100 ) )
											WHEN tintRuleType = 1 AND tintDiscountType = 2
											THEN ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - decDiscount
											WHEN tintRuleType = 2 AND tintDiscountType = 1
											THEN ISNULL(Vendor.monCost,0) + (ISNULL(Vendor.monCost,0) * ( decDiscount / 100 ) )
											WHEN tintRuleType = 2 AND tintDiscountType = 2
											THEN ISNULL(Vendor.monCost,0) + decDiscount
											WHEN tintRuleType = 3
											THEN decDiscount
										END, 0) AS monFirstPriceLevelPrice,ISNULL(CASE 
										WHEN tintRuleType = 1 AND tintDiscountType = 1 
										THEN decDiscount 
										WHEN tintRuleType = 1 AND tintDiscountType = 2
										THEN (decDiscount * 100) / (ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - decDiscount)
										WHEN tintRuleType = 2 AND tintDiscountType = 1
										THEN decDiscount
										WHEN tintRuleType = 2 AND tintDiscountType = 2
										THEN (decDiscount * 100) / (ISNULL(Vendor.monCost,0) + decDiscount)
										WHEN tintRuleType = 3
										THEN 0
									END,'''') AS fltFirstPriceLevelDiscount' ELSE ',0 AS monFirstPriceLevelPrice, 0 AS fltFirstPriceLevelDiscount' END), '  
								,vcPathForImage
								,vcpathForTImage
								,UOM AS UOMConversionFactor
								,UOMPurchase AS UOMPurchaseConversionFactor
								,(CASE WHEN charItemType <> ''S'' AND charItemType <> ''N'' AND ISNULL(bitKitParent,0)=0
											THEN (CASE 
													WHEN bitAllowBackOrder = 1 THEN 1
													WHEN (ISNULL(I.numOnHand,0)<=0) THEN 0
													ELSE 1
												END)
											ELSE 1
								END) AS bitInStock,
								(
									CASE WHEN ISNULL(bitKitParent,0) = 1
									THEN
										(CASE 
											WHEN ((SELECT COUNT(*) FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID = IInner.numItemCode WHERE ID.numItemKitID = I.numItemCode AND ISNULL(IInner.bitKitParent,0) = 1)) > 0 
											THEN 1 
											ELSE 0 
										END)
									ELSE
										0
									END
								) bitHasChildKits
								,(CASE WHEN charItemType <> ''S'' AND charItemType <> ''N''
											THEN ( CASE WHEN bitShowInStock = 1
														THEN (CASE 
																WHEN bitAllowBackOrder = 1 AND I.numDomainID <> 172 AND ISNULL(I.numOnHand,0)<=0 AND ISNULL(I.numVendorID,0) > 0 AND dbo.GetVendorPreferredMethodLeadTime(I.numVendorID,0) > 0 THEN CONCAT(''<font class="vendorLeadTime" style="color:green">Usually ships in '',dbo.GetVendorPreferredMethodLeadTime(I.numVendorID,0),'' days</font>'')
																WHEN bitAllowBackOrder = 1 AND ISNULL(I.numOnHand,0)<=0 THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)
																WHEN (ISNULL(I.numOnHand,0)<=0) THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:red">ON HOLD</font>'' ELSE ''<font style="color:red">Out Of Stock</font>'' END)
																ELSE (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)  
															END)
														ELSE ''''
													END )
											ELSE ''''
										END ) AS InStock
								, ISNULL(W1.numWareHouseItemID,0) [numWareHouseItemID]
								,ISNULL(TablePromotion.numTotalPromotions,0) numTotalPromotions
								,ISNULL(TablePromotion.vcPromoDesc,'''') vcPromoDesc
								,ISNULL(TablePromotion.bitRequireCouponCode,0) bitRequireCouponCode
								' + (CASE WHEN LEN(ISNULL(@numESItemCodes,'')) > 0 THEN ',I.numCategoryID' ELSE '' END)  + '
								,I.numManufacturer
								,I.bintCreatedDate
								',(CASE WHEN LEN(ISNULL(@fldList,'')) > 0 THEN CONCAT(',',@fldList) ELSE '' END),(CASE WHEN LEN(ISNULL(@fldList1,'')) > 0 THEN CONCAT(',',@fldList1) ELSE '' END),'
                            FROM 
								#tmpPagedItems I
							LEFT JOIN ItemImages IM ON I.numItemCode = IM.numItemCode AND IM.bitdefault=1 AND IM.bitIsImage = 1
							OUTER APPLY (SELECT * FROM dbo.fn_GetItemPromotions(',@numDomainID,',',@numDivisionID,',I.numItemCode,I.numItemClassification',',',@numDefaultRelationship,',',@numDefaultProfile,',1)) TablePromotion ',
							(CASE 
								WHEN @tintPreLoginPriceLevel > 0 
								THEN CONCAT(' OUTER APPLY(SELECT decDiscount AS monPrice FROM PricingTable WHERE tintRuleType=3 AND numItemCode=I.numItemCode AND ISNULL(numCurrencyID,0) = 0 ORDER BY numPricingID OFFSET ',CAST(@tintPreLoginPriceLevel-1 AS VARCHAR),' ROWS FETCH NEXT 1 ROWS ONLY) As PricingOption ' )
								ELSE '' 
							END),(CASE 
								WHEN @numDomainID = 172
								THEN ' OUTER APPLY (SELECT TOP 1 * FROM PricingTable WHERE numItemCode=I.numItemCode AND ISNULL(numCurrencyID,0) = 0) AS PT LEFT JOIN Vendor ON Vendor.numVendorID =I.numVendorID AND Vendor.numItemCode=I.numItemCode'
								ELSE '' 
							END),' 
							OUTER APPLY (SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems W 
											WHERE  W.numItemID = I.numItemCode 
											AND W.numDomainID = ',CONVERT(VARCHAR(10),@numDomainID),'
											AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))) AS W1 			  
								CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numSaleUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOM) AS UOM
								CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numPurchaseUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOMPurchase) AS UOMPurchase')
			
	IF LEN(ISNULL(@fldList,'')) > 0
	BEGIN
		SET @strSQL = @strSQL +' left join (
											SELECT  t2.RecId, REPLACE(t1.Fld_label,'' '','''') + ''_C'' as Fld_label, 
												CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
																	WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
																	WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
																	ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
											JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 5 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
											AND t2.RecId IN (SELECT numItemCode FROM #tmpPagedItems )
											) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList + ')) AS pvt on I.numItemCode=pvt.RecId' 
	END

	IF LEN(ISNULL(@fldList1,'')) > 0
	BEGIN
					
		SET @strSQL = @strSQL +' left join (
											SELECT  t2.numItemCode AS RecId, REPLACE(t1.Fld_label,'' '','''') + ''_'' + CAST(t1.Fld_Id AS VARCHAR) as Fld_label, 
												CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
																	WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
																	WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
																	ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
											JOIN ItemAttributes AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 9 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
											AND t2.numItemCode IN (SELECT numItemCode FROM #tmpPagedItems )
											) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList1 + ')) AS pvt1 on I.numItemCode=pvt1.RecId'
	END
	ELSE
	BEGIN
		SET @strSQL = REPLACE(@strSQL, ',##FLDLIST1##', '');
	END

	SET  @strSQL = @strSQL +' order by Rownumber';                
    SET @strSQL = @strSQL + ' SELECT @TotRecs = COUNT(*) FROM #TEMPItems WHERE RowID=1 '                            


	DECLARE @tmpSQL AS NVARCHAR(MAX)
	SET @tmpSQL = @strSQL
	
	PRINT CAST(@tmpSQL AS NTEXT)
	EXEC sp_executeSQL @strSQL, N'@TotRecs INT OUTPUT', @TotRecs OUTPUT
             
             
    DECLARE @tintOrder AS TINYINT                                                  
	DECLARE @vcFormFieldName AS VARCHAR(50)                                                  
	DECLARE @vcListItemType AS VARCHAR(1)                                             
	DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
	DECLARE @numListID AS NUMERIC(9)                                                  
	DECLARE @WhereCondition VARCHAR(MAX)                       
	DECLARE @numFormFieldId AS NUMERIC  
	--DECLARE @vcFieldType CHAR(1)
	DECLARE @vcFieldType VARCHAR(15)
		                  
	SET @tintOrder=0                                                  
	SET @WhereCondition =''                 
                   
              
	CREATE TABLE #tempAvailableFields
	(
		numFormFieldId  NUMERIC(9)
		,vcFormFieldName NVARCHAR(50)
		,vcFieldType VARCHAR(15)
		,vcAssociatedControlType NVARCHAR(50)
		,numListID NUMERIC(18)
		,vcListItemType CHAR(1)
		,intRowNum INT
		,vcItemValue VARCHAR(3000)
	)
		   
	INSERT INTO #tempAvailableFields
	(
		numFormFieldId
		,vcFormFieldName
		,vcFieldType
		,vcAssociatedControlType
		,numListID
		,vcListItemType
		,intRowNum
	)                         
	SELECT 
		Fld_id
		,REPLACE(Fld_label,' ','') AS vcFormFieldName
		,CASE GRP_ID 
			WHEN 4 then 'C' 
			WHEN 1 THEN 'D'
			WHEN 9 THEN CONVERT(VARCHAR(15), Fld_id)
			ELSE 'C' 
		END AS vcFieldType
		,fld_type as vcAssociatedControlType
		,ISNULL(C.numListID, 0) numListID
		,CASE 
			WHEN C.numListID > 0 THEN 'L'
			ELSE ''
		END vcListItemType
		,ROW_NUMBER() OVER (ORDER BY Fld_id ASC) AS intRowNum
	FROM  
		CFW_Fld_Master C 
	LEFT JOIN 
		CFW_Validation V 
	ON 
		V.numFieldID = C.Fld_id
	WHERE 
		C.numDomainID = @numDomainId
		AND GRP_ID IN (5,9)

		  
	SELECT * FROM #tempAvailableFields
	
	IF OBJECT_ID('tempdb..#tempAvailableFields') IS NOT NULL
	DROP TABLE #tempAvailableFields
		
	IF OBJECT_ID('tempdb..#tmpItemCode') IS NOT NULL
	DROP TABLE #tmpItemCode
		
	IF OBJECT_ID('tempdb..#tmpItemCat') IS NOT NULL
	DROP TABLE #tmpItemCat
		
	IF OBJECT_ID('tempdb..#fldValues') IS NOT NULL
	DROP TABLE #fldValues
		
	IF OBJECT_ID('tempdb..#tempSort') IS NOT NULL
	DROP TABLE #tempSort
		
	IF OBJECT_ID('tempdb..#fldDefaultValues') IS NOT NULL
	DROP TABLE #fldDefaultValues

	IF OBJECT_ID('tempdb..#tmpPagedItems') IS NOT NULL
	DROP TABLE #tmpPagedItems


	IF OBJECT_ID('tempdb..#TEMPItems') IS NOT NULL
	DROP TABLE #TEMPItems
END
GO

GO
/****** Object:  StoredProcedure [dbo].[usp_ManageAddlContInfo]    Script Date: 04/02/2009 00:17:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified By Anoop jayaraj                  
 GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageaddlcontinfo')
DROP PROCEDURE usp_manageaddlcontinfo
GO
CREATE PROCEDURE [dbo].[usp_ManageAddlContInfo]                                    
 @numcontactId numeric=0,                                    
 @numContactType numeric=0,                                    
 @vcDepartment numeric(9)=0,                                    
 @vcCategory numeric(9)=0,                                    
 @vcGivenName varchar(100)='',                                    
 @vcFirstName varchar(50)='',                                    
 @vcLastName varchar(50)='',                                    
 @numDivisionId numeric,                                    
 @numPhone varchar(15)='',                                    
 @numPhoneExtension varchar(7)='',                                    
 @numCell varchar(15)='',                                    
 @NumHomePhone varchar(15)='',                                    
 @vcFax varchar(15)='',                                    
 @vcEmail varchar(50)='',                                    
 @VcAsstFirstName varchar(50)='',                                    
 @vcAsstLastName varchar(50)='',                                    
 @numAsstPhone varchar(15)='',                                    
 @numAsstExtn varchar(6)='',                                    
 @vcAsstEmail varchar(50)='',                                                      
 @charSex char(1)='',                                    
 @bintDOB datetime,                                    
 @vcPosition numeric(9)=0,                                                     
 @txtNotes text='',                                                     
 @numUserCntID numeric,                                                                                              
 @numDomainID numeric=1,                                                   
 @vcPStreet varchar(100)='',                                    
 @vcPCity varchar(50)='',                                    
 @vcPPostalCode varchar(15)='',                                    
 @vcPState numeric(9)=0,                                    
 @vcPCountry numeric(9)=0,                  
 @numManagerID numeric=0,        
 @numTeam as numeric(9)=0,        
 @numEmpStatus as numeric(9)=0,
 @vcTitle AS VARCHAR(100)='',
 @bitPrimaryContact AS BIT=0,
 @bitOptOut AS BIT=0,
 @vcLinkedinId VARCHAR(30)=NULL,
 @vcLinkedinUrl VARCHAR(300)=NULL,
 @numECampaignID NUMERIC(18)=0,
 @vcPassword VARCHAR(500) = '',
 @vcTaxID VARCHAR(100) = ''
AS   
	If LEN(ISNULL(@vcFirstName,'')) = 0
	BEGIN
		SET @vcFirstName = '-'
	END

	If LEN(ISNULL(@vcLastName,'')) = 0
	BEGIN
		SET @vcLastName = '-'
	END

	--IF LEN(ISNULL(@vcEmail,'')) > 0
	--BEGIN
	--	IF EXISTS (SELECT numContactId FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId <> ISNULL(@numcontactId,0) AND vcEmail=@vcEmail)
	--	BEGIN
	--		RAISERROR('DUPLICATE_EMAIL',16,1)
	--		RETURN
	--	END
	--END

 SELECT @vcGivenName = @vcLastName + ', ' + @vcFirstName  + '.eml'                                 
   if @bintDOB ='Jan  1 1753 12:00AM' set  @bintDOB=null                      
IF @numContactId=0
     BEGIN   

IF @bitPrimaryContact = 1 
    BEGIN            
        DECLARE @numID AS NUMERIC(9)            
        DECLARE @bitPrimaryContactCheck AS BIT             
             
        SELECT TOP 1
                @numID = numContactID,
                @bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
        FROM    AdditionalContactsInformation
        WHERE   numDivisionID = @numDivisionID 
                   
        IF @@rowcount = 0 
            SET @numID = 0             
        WHILE @numID > 0            
            BEGIN            
                IF @bitPrimaryContactCheck = 1 
                    UPDATE  AdditionalContactsInformation
                    SET     bitPrimaryContact = 0
                    WHERE   numContactID = @numID  
                              
                SELECT TOP 1
                        @numID = numContactID,
                        @bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
                FROM    AdditionalContactsInformation
                WHERE   numDivisionID = @numDivisionID
                        AND numContactID > @numID    
                                
                IF @@rowcount = 0 
                    SET @numID = 0             
            END            
    END  
    ELSE IF @bitPrimaryContact = 0
    BEGIN
		IF (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactId!=@numContactId AND ISNULL(bitPrimaryContact,0)=1)=0
			SET @bitPrimaryContact = 1
    END                 
    
    
declare @bitAutoPopulateAddress bit 
declare @tintPoulateAddressTo tinyint 

select @bitAutoPopulateAddress= isnull(bitAutoPopulateAddress,'0'),@tintPoulateAddressTo=isnull(tintPoulateAddressTo,'0') from Domain where numDomainId = @numDomainId ;
if (@bitAutoPopulateAddress = 1)
	if(@tintPoulateAddressTo = 1) --if primary address is not specified then getting billing address    
	begin
		 
		  select                   
			@vcPStreet = AD1.vcStreet,
			@vcPCity= AD1.vcCity,
			@vcPState= AD1.numState,
			@vcPPostalCode= AD1.vcPostalCode,
			@vcPCountry= AD1.numCountry              
		  from divisionMaster  DM              
		   LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
			AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
		  where numDivisionID=@numDivisionId    
	end  
	else if (@tintPoulateAddressTo = 2)-- Primary Address is Shipping Address
	begin
		  select                   
			@vcPStreet= AD2.vcStreet,
			@vcPCity=AD2.vcCity,
			@vcPState=AD2.numState,
			@vcPPostalCode=AD2.vcPostalCode,
			@vcPCountry=AD2.numCountry                   
		  from divisionMaster DM
		  LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
		  AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
		  where numDivisionID=@numDivisionId    
	
	end                       
                       
 if (@numEmpStatus is null or @numEmpStatus=0) set @numEmpStatus=658        
 
    INSERT into AdditionalContactsInformation                                    
    (                  
   numContactType,                  
   vcDepartment,                  
   vcCategory,                  
   vcGivenName,                  
   vcFirstName,                  
   vcLastName,                   
   numDivisionId ,                  
   numPhone ,                  
   numPhoneExtension,                  
   numCell ,                  
   numHomePhone ,                          
   vcFax ,                  
   vcEmail ,                  
   VcAsstFirstName ,                  
   vcAsstLastName ,                  
   numAsstPhone ,                  
   numAsstExtn ,                  
   vcAsstEmail  ,                  
   charSex ,                  
   bintDOB ,                  
   vcPosition ,                  
   txtNotes ,                  
   numCreatedBy ,                                    
   bintCreatedDate,             
   numModifiedBy,            
   bintModifiedDate,                   
   numDomainID,                  
   numManagerID,                   
   numRecOwner,                  
   numTeam,                  
   numEmpStatus,
   vcTitle,bitPrimaryContact,bitOptOut,
   vcLinkedinId,vcLinkedinUrl,numECampaignID,vcTaxID           
   )                                    
    VALUES                                    
      (                  
   @numContactType,                  
   @vcDepartment,                  
   @vcCategory,                  
   @vcGivenName ,                  
   @vcFirstName ,                  
   @vcLastName,                   
   @numDivisionId ,                  
   @numPhone ,                                    
   @numPhoneExtension,                  
   @numCell ,                  
   @NumHomePhone ,                  
   @vcFax ,                  
   @vcEmail ,                  
   @VcAsstFirstName,                         
   @vcAsstLastName,                  
   @numAsstPhone,                  
   @numAsstExtn,                  
   @vcAsstEmail,                  
   @charSex,                  
   @bintDOB ,                  
   @vcPosition,                                   
   @txtNotes,                  
   @numUserCntID,                  
   getutcdate(),             
   @numUserCntID,                  
   getutcdate(),                   
   @numDomainID,                   
   @numManagerID,                   
   @numUserCntID,                   
   @numTeam,                  
   @numEmpStatus,
   @vcTitle,@bitPrimaryContact,@bitOptOut,
   @vcLinkedinId,@vcLinkedinUrl,@numECampaignID,@vcTaxID           
   )                                    
                     
 set @numcontactId= SCOPE_IDENTITY()
 
 SELECT @numcontactId             

     INSERT INTO dbo.AddressDetails (
	 	vcAddressName,
	 	vcStreet,
	 	vcCity,
	 	vcPostalCode,
	 	numState,
	 	numCountry,
	 	bitIsPrimary,
	 	tintAddressOf,
	 	tintAddressType,
	 	numRecordID,
	 	numDomainID
	 ) VALUES ('Primary', @vcPStreet,@vcPCity,@vcPPostalCode,@vcPState,@vcPCountry,1,1,0,@numcontactId,@numDomainID)
     
	 IF ISNULL(@numECampaignID,0) > 0 
	 BEGIN
		  --Manage ECampiagn When Campaign is changed from inline edit
		DECLARE @Date AS DATETIME 
		SELECT @Date = dbo.[GetUTCDateWithoutTime]()

 		EXEC [USP_ManageConEmailCampaign]
		@numConEmailCampID = 0, --  numeric(9, 0)
		@numContactID = @numcontactId, --  numeric(9, 0)
		@numECampaignID = @numECampaignID, --  numeric(9, 0)
		@intStartDate = @Date, --  datetime
		@bitEngaged = 1, --  bit
		@numUserCntID = @numUserCntID --  numeric(9, 0)     
	END             
	
	IF @numDivisionId = ISNULL((SELECT numDivisionID FROM Domain WHERE numDomainID=@numDomainID),0)                            
	BEGIN
		DECLARE @vcName AS VARCHAR(200) = CONCAT(@vcFirstName,' ',@vcLastName)

		EXEC usp_SetUsersWithDomains
			@numUserID= 0,                                    
			@vcUserName = @vcName,                                    
			@vcUserDesc = '',                              
			@numGroupId = 0,                                           
			@numUserDetailID = @numcontactId,                              
			@numUserCntID = @numUserCntID,                              
			@strTerritory = '' ,                              
			@numDomainID = @numDomainID,
			@strTeam = '',
			@vcEmail = @vcEmail,
			@vcPassword = '',          
			@Active  = 0,
			@numDefaultClass = 0,
			@numDefaultWarehouse = 0,
			@vcLinkedinId = null,
			@intAssociate = 0,
			@ProfilePic =null
			,@bitPayroll = 0
			,@monHourlyRate = 0
	END
                                                 
  END                                    
 ELSE if @numContactId>0                                                        
    BEGIN                                    
    UPDATE AdditionalContactsInformation SET                                    
      numContactType=@numContactType,                                    
      vcGivenName=@vcGivenName,                                    
      vcFirstName=@vcFirstName ,                                    
      vcLastName =@vcLastName ,                   
      numDivisionId =@numDivisionId ,                                    
      numPhone=@numPhone ,                                    
      numPhoneExtension=@numPhoneExtension,                                    
      numCell =@numCell ,                                    
      NumHomePhone =@NumHomePhone ,                                    
      vcFax=@vcFax ,                                    
      vcEmail=@vcEmail ,                                    
      VcAsstFirstName=@VcAsstFirstName,                                    
      vcAsstLastName=@vcAsstLastName,                                    
      numAsstPhone=@numAsstPhone,                                    
      numAsstExtn=@numAsstExtn,                                    
      vcAsstEmail=@vcAsstEmail,                                                      
      charSex=@charSex,                            
      bintDOB=@bintDOB ,                                    
      vcPosition=@vcPosition,                                                    
      txtNotes=@txtNotes,                                                     
      numModifiedBy=@numUserCntID,                                    
      bintModifiedDate=getutcdate(),                                    
      numManagerID=@numManagerID,
      bitPrimaryContact=@bitPrimaryContact                              
     WHERE numContactId=@numContactId   
                    
     Update dbo.AddressDetails set                   
       vcStreet=@vcPStreet,                                    
       vcCity =@vcPCity,                                    
       vcPostalCode=@vcPPostalCode,                                     
       numState=@vcPState,                                    
       numCountry=@vcPCountry
	  where numRecordID=@numContactId AND bitIsPrimary=1 AND tintAddressOf=1 AND tintAddressType=0
 
     SELECT @numcontactId   
                                                
  END    
  ------Check the Email id Exist into the Email Master Table

	IF @vcPassword IS NOT NULL AND  LEN(@vcPassword) > 0
	BEGIN
		IF EXISTS (SELECT numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID)
		BEGIN
			IF EXISTS (SELECT numExtranetDtlID FROM ExtranetAccountsDtl WHERE numContactID=@numContactID)
			BEGIN
				UPDATE
					ExtranetAccountsDtl
				SET 
					vcPassword=@vcPassword
					,tintAccessAllowed=1
				WHERE
					numContactID=@numContactID
			END
			ELSE
			BEGIN
				DECLARE @numExtranetID NUMERIC(18,0)
				SELECT @numExtranetID=numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID

				INSERT INTO ExtranetAccountsDtl (numExtranetID,numContactID,vcPassword,tintAccessAllowed,numDomainID)
				VALUES (@numExtranetID,@numContactID,@vcPassword,1,@numDomainID)
			END
		END
		ELSE
		BEGIN
			RAISERROR('Organization e-commerce access is not enabled',16,1)
		END
	END



DECLARE @numCount AS NUMERIC(18,0)
SET @numCount = 0
IF @vcEmail <> ''
BEGIN 
SELECT @numCount = COUNT(*) FROM dbo.EmailMaster WHERE vcEmailID =@vcEmail and numDomainId=@numDomainID
IF @numCount = 0 
BEGIN
	INSERT INTO EmailMaster(vcEmailId,vcName,numDomainId,numContactId)
	VALUES(@vcEmail,@vcFirstName,@numDomainID,@numcontactId)
END
else
begin
	update EmailMaster set numContactId=@numcontactId where vcEmailID =@vcEmail and numDomainId=@numDomainID
end
END 
--IF @vcAsstEmail <> ''
--BEGIN
--SELECT @numCount = COUNT(*) FROM dbo.EmailMaster WHERE vcEmailID =@vcAsstEmail
--IF @numCount = 0 
--BEGIN
--	INSERT INTO EmailMaster(vcEmailId,vcName,numDomainId,numContactId)
--	VALUES(@vcAsstEmail,@vcFirstName,@numDomainID,@numcontactId)
--END
--END
---------------------------------------------------------

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManagePageNavigationAuthorization' ) 
    DROP PROCEDURE USP_ManagePageNavigationAuthorization
GO

CREATE PROCEDURE USP_ManagePageNavigationAuthorization
	@numGroupID		NUMERIC(18,0),
	@numTabID		NUMERIC(18,0),
    @numDomainID	NUMERIC(18,0),
    @strItems		VARCHAR(MAX)
AS 
BEGIN
    SET NOCOUNT ON  
    BEGIN TRY 
        BEGIN TRAN  
           
        DELETE  FROM dbo.TreeNavigationAuthorization WHERE numGroupID = @numGroupID 
													 AND numDomainID = @numDomainID
													 AND numTabID = @numTabID 
													 AND numPageNavID NOT IN (SELECT ISNULL(numPageNavID,0) FROM dbo.PageNavigationDTL WHERE bitVisible=0)
				
        DECLARE @hDocItem INT
        IF CONVERT(VARCHAR(MAX), @strItems) <> '' 
            BEGIN
                EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
                
				IF @numTabID = 7 --Relationships
				BEGIN

					DELETE FROM TreeNodeOrder WHERE numGroupID = @numGroupID AND numDomainID = @numDomainID AND numTabID = @numTabID	

					SELECT  
						@numGroupID AS numGroupID,
						@numTabID AS numTabID,
						X.[numPageNavID] AS numPageNavID,
						X.[numListItemID] As numListItemID,
						X.[bitVisible] AS bitVisible,
						@numDomainID AS numDomainID,
						X.[numParentID] AS numParentID,
						X.tintType AS tintType,
						X.[numOrder] AS numOrder
					INTO
						#TMEP
					FROM    
						( 
							SELECT    
								*
							FROM      
								OPENXML (@hDocItem, '/NewDataSet/Table1', 2)
								WITH ( [numPageNavID] NUMERIC(18, 0), bitVisible BIT, tintType TINYINT, numListItemID NUMERIC(18,0),numParentID NUMERIC(18,0), numOrder INT)
						) X


					INSERT  INTO [dbo].[TreeNavigationAuthorization]
							(
							  [numGroupID]
							  ,[numTabID]
							  ,[numPageNavID]
							  ,[bitVisible]
							  ,[numDomainID]
							  ,tintType
							)
					SELECT
						numGroupID,
						numTabID,
						numPageNavID,
						bitVisible,
						numDomainID,
						tintType
					FROM
						#TMEP
					WHERE
						ISNULL(numPageNavID,0) <> 0

					INSERT  INTO [dbo].[TreeNodeOrder]
					(
						numTabID,
						numDomainID,
						numGroupID,
						numPageNavID,
						numListItemID,
						numParentID,
						bitVisible,
						tintType,
						numOrder
					)
					SELECT
						numTabID,
						numDomainID,
						numGroupID,
						numPageNavID,
						numListItemID,
						numParentID,
						bitVisible,
						tintType,
						numOrder
					FROM
						#TMEP
				END
				ELSE
				BEGIN
					INSERT  INTO [dbo].[TreeNavigationAuthorization]
                        (
                          [numGroupID]
						  ,[numTabID]
						  ,[numPageNavID]
						  ,[bitVisible]
						  ,[numDomainID]
						  ,tintType
                        )
                        SELECT  @numGroupID,
							    @numTabID,
							    X.[numPageNavID],
							    X.[bitVisible],
							    @numDomainID,
							    X.tintType
                        FROM    ( SELECT    *
                                  FROM      OPENXML (@hDocItem, '/NewDataSet/Table1', 2)
                                            WITH ( [numPageNavID] NUMERIC(18, 0), bitVisible BIT, tintType TINYINT)
                                ) X

					-- SHOW PARENT RECURSIVELY
					;WITH CTE (numPageNavID,numParentID) AS
					(
						SELECT
							PND.numPageNavID
							,PND.numParentID
						FROM
							TreeNavigationAuthorization TNA
						INNER JOIN
							PageNavigationDTL PND
						ON
							TNA.numPageNavID = PND.numPageNavID
						WHERE
							TNA.bitVisible = 1
							AND TNA.numDomainID = @numDomainID
							AND TNA.numGroupID = @numGroupID
							AND TNA.numPageNavID NOT IN (26)
							AND TNA.numTabID = @numTabID
						UNION ALL
						SELECT 
							PND.numPageNavID
							,PND.numParentID
						FROM
							TreeNavigationAuthorization TNA
						INNER JOIN
							PageNavigationDTL PND
						ON
							TNA.numPageNavID = PND.numPageNavID
						JOIN
							CTE c
						ON
							TNA.numPageNavID= c.numParentID
						WHERE
							TNA.numDomainID = @numDomainID
							AND TNA.numGroupID = @numGroupID
							AND TNA.numTabID = @numTabID
					)

					UPDATE 
						TreeNavigationAuthorization 
					SET 
						bitVisible = 1 
					WHERE 
						numDomainID = @numDomainID
						AND numGroupID = @numGroupID
						AND numTabID = @numTabID 
						AND bitVisible=0 
						AND numPageNavID IN (SELECT numPageNavID FROM CTE)


					--HIDE CHILD RECURSIVELY
					;WITH CTE (numPageAuthID,numDomainID,numGroupID,numTabID,numPageNavID) AS
					(
						SELECT
							numPageAuthID,
							numDomainID,
							numGroupID,
							numTabID,
							numPageNavID
						FROM
							TreeNavigationAuthorization TNA
						WHERE
							TNA.bitVisible = 0
							AND TNA.numDomainID = @numDomainID
							AND TNA.numGroupID = @numGroupID
							AND TNA.numPageNavID NOT IN (26)
							AND TNA.numTabID = @numTabID
						UNION ALL
						SELECT 
							TNA.numPageAuthID,
							TNA.numDomainID,
							TNA.numGroupID,
							TNA.numTabID,
							TNA.numPageNavID
						FROM
							TreeNavigationAuthorization TNA
						JOIN
							PageNavigationDTL PND
						ON
							TNA.numPageNavID = PND.numPageNavID
						JOIN
							CTE c
						ON
							PND.numParentID= c.numPageNavID
						WHERE
							TNA.numDomainID = @numDomainID
							AND TNA.numGroupID = @numGroupID
							AND TNA.numTabID = @numTabID
					)

					UPDATE TreeNavigationAuthorization SET bitVisible = 0 WHERE bitVisible=1 AND numPageAuthID IN (SELECT numPageAuthID FROM CTE) 
				END
                EXEC sp_xml_removedocument @hDocItem
            END


        COMMIT TRAN 
    END TRY 
    BEGIN CATCH		
        DECLARE @strMsg VARCHAR(200)
        SET @strMsg = ERROR_MESSAGE()
        IF ( @@TRANCOUNT > 0 ) 
            BEGIN
                RAISERROR ( @strMsg, 16, 1 ) ;
                ROLLBACK TRAN
                RETURN 1
            END
    END CATCH
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageVendorShipmentMethod')
DROP PROCEDURE USP_ManageVendorShipmentMethod
GO
CREATE PROCEDURE [dbo].[USP_ManageVendorShipmentMethod] 
( 
	@numDomainID as numeric(18,0)=0,    
	@numVendorid AS NUMERIC(18,0)=0,
	@numAddressID AS NUMERIC(18,0)=0,
	@numWarehouseID AS NUMERIC(18,0) = 0,
	@str AS TEXT = '',
	@bitPrimary AS bit=0
)
AS 
BEGIN
DECLARE @hDoc INT
	EXEC sp_xml_preparedocument @hDoc OUTPUT, @str

	DELETE FROM VendorShipmentMethod WHERE numDomainID=@numDomainID AND numVendorid=@numVendorid AND numAddressID=@numAddressID AND numWarehouseID=@numWarehouseID
	
	UPDATE VendorShipmentMethod SET bitPrimary=0 WHERE numDomainID=@numDomainID AND numVendorid=@numVendorid AND numWarehouseID=@numWarehouseID

	INSERT INTO VendorShipmentMethod (
		numDomainID,numVendorid,numAddressID,numWarehouseID,bitPrimary,
		numListItemID,
		numListValue,
		bitPreferredMethod		
	) 
	SELECT 
		@numDomainID,@numVendorid,@numAddressID,@numWarehouseID,@bitPrimary,
		X.numListItemID,
		X.numListValue,
		ISNULL(X.bitPreferredMethod,0)
		FROM 	( SELECT    *
          FROM      OPENXML (@hDoc, '/NewDataSet/ShipmentMethod',2) WITH (numListItemID NUMERIC(9), numListValue INT, bitPreferredMethod BIT)
        ) X

	EXEC sp_xml_removedocument @hDoc
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassPurchaseFulfillment_GetRecords')
DROP PROCEDURE dbo.USP_MassPurchaseFulfillment_GetRecords
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassPurchaseFulfillment_GetRecords]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@numViewID NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@bitGroupByOrder BIT
	,@tintFlag tinyint -- if 1 then ShowBilled = true, 2 then received but not billed
	,@bitIncludeSearch BIT
	,@vcOrderStauts VARCHAR(MAX)
	,@vcCustomSearchValue VARCHAR(MAX)
	,@numPageIndex INT
	,@vcSortColumn VARCHAR(100) OUTPUT
	,@vcSortOrder VARCHAR(4) OUTPUT
	,@numTotalRecords NUMERIC(18,0) OUTPUT
)
AS 
BEGIN
	DECLARE @numDivisionID NUMERIC(18,0)
	DECLARE @bitShowOnlyFullyReceived BIT = 0
	DECLARE @tintBillType TINYINT = 1
	
	SELECT @numDivisionID=ISNULL(numDivisionID,0) FROM Domain WHERE numDomainId=@numDomainID
	
	IF EXISTS (SELECT ID FROM MassPurchaseFulfillmentConfiguration WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID)
	BEGIN
		SELECT 
			@bitShowOnlyFullyReceived=ISNULL(bitShowOnlyFullyReceived,0)
			,@tintBillType = ISNULL(tintBillType,1)
		FROM 
			MassPurchaseFulfillmentConfiguration 
		WHERE 
			numDomainID=@numDomainID 
			AND numUserCntID=@numUserCntID
	END

	IF @numViewID = 4
	BEGIN
		SET @bitGroupByOrder = 1
	END

	DECLARE @TempFieldsLeft TABLE
	(
		numPKID INT IDENTITY(1,1)
		,ID VARCHAR(50)
		,numFieldID NUMERIC(18,0)
		,vcFieldName VARCHAR(100)
		,vcOrigDbColumnName VARCHAR(100)
		,vcListItemType VARCHAR(10)
		,numListID NUMERIC(18,0)
		,bitAllowSorting BIT
		,bitAllowFiltering BIT
		,vcAssociatedControlType VARCHAR(20)
		,vcLookBackTableName VARCHAR(100)
		,bitCustomField BIT
		,intRowNum INT
		,intColumnWidth FLOAT
		,bitIsRequired BIT
		,bitIsEmail BIT
		,bitIsAlphaNumeric BIT
		,bitIsNumeric BIT
		,bitIsLengthValidation BIT
		,intMaxLength INT
		,intMinLength INT
		,bitFieldMessage BIT
		,vcFieldMessage VARCHAR(MAX)
	)

	IF @bitGroupByOrder = 1
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName','OpportunityMaster.bintCreatedDate','OpportunityMaster.vcPoppName','OpportunityMaster.numStatus','OpportunityMaster.numAssignedTo','OpportunityMaster.numRecOwner')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'DESC'
		END
	END
	ELSE 
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName'
								,'OpportunityMaster.bintCreatedDate'
								,'OpportunityMaster.vcPoppName'
								,'OpportunityMaster.numStatus'
								,'OpportunityMaster.numAssignedTo'
								,'OpportunityMaster.numRecOwner'
								,'WarehouseItems.numOnHand'
								,'WarehouseItems.numOnOrder'
								,'WarehouseItems.numAllocation'
								,'WarehouseItems.numBackOrder'
								,'Item.numItemCode'
								,'Item.vcItemName'
								,'OpportunityMaster.numUnitHour'
								,'OpportunityMaster.numUnitHourReceived')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'DESC'
		END
	END

	INSERT INTO @TempFieldsLeft
	(
		ID
		,numFieldID
		,vcFieldName
		,vcOrigDbColumnName
		,vcListItemType
		,numListID
		,bitAllowSorting
		,bitAllowFiltering
		,vcAssociatedControlType
		,vcLookBackTableName
		,bitCustomField
		,intRowNum
		,intColumnWidth
		,bitIsRequired
		,bitIsEmail
		,bitIsAlphaNumeric
		,bitIsNumeric
		,bitIsLengthValidation
		,intMaxLength
		,intMinLength
		,bitFieldMessage
		,vcFieldMessage
	)
	SELECT
		CONCAT(135,'~',DFM.numFieldID,'~',0)
		,DFM.numFieldID
		,(CASE WHEN @numViewID=6 AND DFM.numFieldID=248 THEN 'BizDoc' ELSE DFFM.vcFieldName END)
		,DFM.vcOrigDbColumnName
		,ISNULL(DFM.vcListItemType,'')
		,ISNULL(DFM.numListID,0)
		,DFFM.bitAllowSorting
		,DFFM.bitAllowFiltering
		,DFM.vcAssociatedControlType
		,DFM.vcLookBackTableName
		,0
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
		,0
		,0
		,0
		,0
		,0
		,0
		,0
		,0
		,''
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		DycFormField_Mapping DFFM
	ON
		DFCD.numFormId = DFFM.numFormID
		AND DFCD.numFieldId = DFFM.numFieldID
	INNER JOIN
		DycFieldMaster DFM
	ON
		DFFM.numFieldID = DFM.numFieldId
	WHERE 
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=135 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFFM.bitDeleted,0) = 0
		AND ISNULL(DFFM.bitSettingField,0) = 1
		AND ISNULL(DFCD.bitCustom,0) = 0
	UNION
	SELECT
		CONCAT(135,'~',CFM.Fld_id,'~',0)
		,CFM.Fld_id
		,CFM.Fld_label
		,CONCAT('CUST',CFM.Fld_id)
		,''
		,ISNULL(CFM.numListID,0)
		,0
		,0
		,CFM.Fld_type
		,''
		,1
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
		,ISNULL(bitIsRequired,0)
		,ISNULL(bitIsEmail,0)
		,ISNULL(bitIsAlphaNumeric,0)
		,ISNULL(bitIsNumeric,0)
		,ISNULL(bitIsLengthValidation,0)
		,ISNULL(intMaxLength,0)
		,ISNULL(intMinLength,0)
		,ISNULL(bitFieldMessage,0)
		,ISNULL(vcFieldMessage,'')
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		CFW_Fld_Master CFM
	ON
		DFCD.numFieldId = CFM.Fld_id
	LEFT JOIN
		CFW_Validation CV
	ON
		CFM.Fld_id = CV.numFieldID
	WHERE
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=135 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFCD.bitCustom,0) = 1

	IF (SELECT COUNT(*) FROM @TempFieldsLeft) = 0
	BEGIN
		INSERT INTO @TempFieldsLeft
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,vcListItemType
			,numListID
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,vcLookBackTableName
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(135,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,ISNULL(DFM.vcListItemType,'')
			,ISNULL(DFM.numListID,0)
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,DFM.vcLookBackTableName
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=135 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND ISNULL(DFFM.bitDefault,0) = 1
	END

	DECLARE @j INT = 0
	DECLARE @jCount INT
	DECLARE @numFieldId NUMERIC(18,0)
	DECLARE @vcFieldName AS VARCHAR(50)                                                                                                
	DECLARE @vcAssociatedControlType VARCHAR(10)  
	DECLARE @vcDbColumnName VARCHAR(100)                                                
	--DECLARE @numListID AS NUMERIC(9)       
	DECLARE @bitCustomField BIT       
	SET @jCount = (SELECT COUNT(*) FROM @TempFieldsLeft)
	DECLARE @vcCustomFields VARCHAR(MAX) = ''
	DECLARE @vcCustomWhere VARCHAR(MAX) = ''

	WHILE @j <= @jCount
	BEGIN
		SELECT
			@numFieldId=numFieldID,
			@vcFieldName = vcFieldName,
			@vcAssociatedControlType = vcAssociatedControlType,
			@vcDbColumnName = CONCAT('Cust',numFieldID),
			@bitCustomField=ISNULL(bitCustomField,0)
		FROM 
			@TempFieldsLeft
		WHERE 
			numPKID = @j     
		
		
			
		IF @bitCustomField = 1
		BEGIN         
			IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea' 
			BEGIN
				SET @vcCustomFields = CONCAT(@vcCustomFields,',CFW',@numFieldId,'.Fld_Value  [',@vcDbColumnName,']')
             
				SET @vcCustomWhere = CONCAT(@vcCustomWhere,' left Join CFW_FLD_Values_Opp CFW',@numFieldId
								,' ON CFW' , @numFieldId , '.Fld_Id='
								,@numFieldId
								, ' and CFW' , @numFieldId
								, '.RecId=T1.numOppID')                                                         
			END   
			ELSE IF @vcAssociatedControlType = 'CheckBox' 
			BEGIN
				SET @vcCustomFields =CONCAT( @vcCustomFields
					, ',case when isnull(CFW'
					, @numFieldId
					, '.Fld_Value,0)=0 then 0 when isnull(CFW'
					, @numFieldId
					, '.Fld_Value,0)=1 then 1 end   ['
					,  @vcDbColumnName
					, ']')               
 
				SET @vcCustomWhere = CONCAT(@vcCustomWhere
					, ' left Join CFW_FLD_Values_Opp CFW'
					, @numFieldId
					, ' ON CFW',@numFieldId,'.Fld_Id='
					, @numFieldId
					, ' and CFW'
					, @numFieldId
					, '.RecId=T1.numOppID   ')                                                    
			END                
			ELSE IF @vcAssociatedControlType = 'DateField' 
			BEGIN
				SET @vcCustomFields = CONCAT(@vcCustomFields
					, ',dbo.FormatedDateFromDate(CFW'
					, @numFieldId
					, '.Fld_Value,'
					, @numDomainId
					, ')  [',@vcDbColumnName ,']' )  
					                  
				SET @vcCustomWhere = CONCAT(@vcCustomWhere
					, ' left Join CFW_FLD_Values_Opp CFW'
					, @numFieldId
					, ' on CFW', @numFieldId, '.Fld_Id='
					, @numFieldId
					, ' and CFW'
					, @numFieldId
					, '.RecId=T1.numOppID   ' )                                                        
			END                
			ELSE IF @vcAssociatedControlType = 'SelectBox' 
			BEGIN
				SET @vcCustomFields = CONCAT(@vcCustomFields,',L',@numFieldId,'.vcData',' [',@vcDbColumnName,']')
					                                                    
				SET @vcCustomWhere = CONCAT(@vcCustomWhere
					, ' left Join CFW_FLD_Values_Opp CFW'
					, @numFieldId
					, ' on CFW',@numFieldId ,'.Fld_Id='
					, @numFieldId
					, ' and CFW'
					, @numFieldId
					, '.RecId=T1.numOppID    ')     
					                                                    
				SET @vcCustomWhere = CONCAT(@vcCustomWhere
					, ' left Join ListDetails L'
					, @numFieldId
					, ' on L'
					, @numFieldId
					, '.numListItemID=CFW'
					, @numFieldId
					, '.Fld_Value' )              
			END
			ELSE
			BEGIN
				SET @vcCustomFields = CONCAT(@vcCustomFields,CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',T1.numOppID)'),' [',@vcDbColumnName,']')
			END 
		END

		SET @j = @j + 1
	END
	
	CREATE TABLE #TEMPMSRecords
	(
		numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numWOID NUMERIC(18,0)
	)

	DECLARE @vcSQL NVARCHAR(MAX)
	DECLARE @vcSQLFinal NVARCHAR(MAX)
	
	SET @vcSQL = CONCAT('INSERT INTO #TEMPMSRecords
						(
							numOppID
							,numOppItemID
							,numWOID
						)
						SELECT
							OpportunityMaster.numOppId
							,',(CASE WHEN @bitGroupByOrder = 1 THEN '0' ELSE 'OpportunityItems.numoppitemtCode' END),'
							,0
						FROM
							OpportunityMaster
						INNER JOIN
							DivisionMaster
						ON
							OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
						INNER JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						INNER JOIN 
							OpportunityItems
						ON
							OpportunityMaster.numOppId = OpportunityItems.numOppId
						INNER JOIN
							Item
						ON
							OpportunityItems.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
						WHERE
							OpportunityMaster.numDomainId=@numDomainID
							AND OpportunityMaster.tintOppType = 2
							AND OpportunityMaster.tintOppStatus = 1
							AND ISNULL(OpportunityMaster.tintshipped,0) = 0
							AND 1 = (CASE 
										WHEN @numViewID=1 
										THEN (CASE WHEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyReceived,0) > 0 THEN 1 ELSE 0 END)
										WHEN @numViewID=2
										THEN (CASE WHEN ISNULL(OpportunityItems.numQtyReceived,0) > 0 AND ISNULL(OpportunityItems.numQtyReceived,0) - ISNULL(OpportunityItems.numUnitHourReceived,0) > 0 THEN 1 ELSE 0 END)
										WHEN @numViewID=3
										THEN (CASE 
												WHEN (CASE 
														WHEN @tintBillType=2 
														THEN ISNULL(OpportunityItems.numQtyReceived,0) 
														WHEN @tintBillType=3 THEN ISNULL(OpportunityItems.numUnitHourReceived,0) 
														ELSE ISNULL(OpportunityItems.numUnitHour,0) 
														END)  - ISNULL((SELECT 
																			SUM(OpportunityBizDocItems.numUnitHour)
																		FROM
																			OpportunityBizDocs
																		INNER JOIN
																			OpportunityBizDocItems
																		ON
																			OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																		WHERE
																			OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																			AND OpportunityBizDocs.numBizDocId=644
																			AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0 
												THEN 1
												ELSE 0 
											END)
										WHEN @numViewID=4 AND ISNULL(@bitShowOnlyFullyReceived,0) = 1
										THEN (CASE 
												WHEN (SELECT COUNT(*) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppID AND ISNULL(OpportunityItems.numUnitHour,0) <> ISNULL(OpportunityItems.numUnitHourReceived,0) ) > 0 
												THEN 0 
												ELSE 1 
											END)
										ELSE 1
									END)
							AND 1 = (CASE WHEN @numViewID IN (1,2) AND ISNULL(@numWarehouseID,0) > 0 AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID THEN 1 ELSE 0 END) ELSE 1 END)
							AND 1 = (CASE WHEN LEN(ISNULL(@vcOrderStauts,'''')) > 0 THEN (CASE WHEN OpportunityMaster.numStatus ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) ELSE 1 END)'
							,@vcCustomSearchValue
							,(CASE WHEN @bitGroupByOrder = 1 THEN ' GROUP BY OpportunityMaster.numOppId' ELSE '' END));

	EXEC sp_executesql @vcSQL, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @numViewID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @bitShowOnlyFullyReceived BIT, @tintBillType TINYINT', @numDomainID, @vcOrderStauts, @numViewID, @numWarehouseID, @bitShowOnlyFullyReceived, @tintBillType;


	SET @vcSQL = CONCAT('INSERT INTO #TEMPMSRecords
						(
							numOppID
							,numOppItemID
							,numWOID
						)
						SELECT
							0
							,0
							,WorkOrder.numWOID
						FROM
							WorkOrder
						LEFT JOIN
							OpportunityMaster
						ON
							WorkOrder.numOppID = OpportunityMaster.numOppID
						LEFT JOIN
							DivisionMaster
						ON
							OpportunityMaster.numDivisionId = @numDivisionID
						LEFT JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						INNER JOIN
							Item
						ON
							WorkOrder.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							WorkOrder.numWarehouseItemID = WareHouseItems.numWareHouseItemID
						WHERE
							WorkOrder.numDomainId=@numDomainID
							AND 1 = (CASE 
										WHEN @numViewID=1 
										THEN (CASE WHEN ISNULL(WorkOrder.numQtyBuilt,0) - ISNULL(WorkOrder.numQtyReceived,0) > 0 THEN 1 ELSE 0 END)
										WHEN @numViewID=2
										THEN (CASE WHEN ISNULL(WorkOrder.numQtyReceived,0) > 0 AND ISNULL(WorkOrder.numQtyReceived,0) - ISNULL(WorkOrder.numUnitHourReceived,0) > 0 THEN 1 ELSE 0 END)
										ELSE 0
									END)
							AND 1 = (CASE WHEN @numViewID IN (1,2) AND ISNULL(@numWarehouseID,0) > 0 AND ISNULL(WorkOrder.numWareHouseItemId,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID THEN 1 ELSE 0 END) ELSE 1 END)'
							,@vcCustomSearchValue);

	EXEC sp_executesql @vcSQL, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @numViewID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @bitShowOnlyFullyReceived BIT, @tintBillType TINYINT, @numDivisionID NUMERIC(18,0)', @numDomainID, @vcOrderStauts, @numViewID, @numWarehouseID, @bitShowOnlyFullyReceived, @tintBillType, @numDivisionID;



	SET @numTotalRecords = (SELECT COUNT(*) FROM #TEMPMSRecords)
	
	CREATE TABLE #TEMPResult
	(
		numOppID NUMERIC(18,0)
		,numWOID NUMERIC(18,0)
		,numDivisionID NUMERIC(18,0)
		,numTerID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(18,0)
		,vcCompanyName VARCHAR(300)
		,vcPoppName VARCHAR(300)
		,numStatus VARCHAR(300)
		,bintCreatedDate VARCHAR(50)
		,numAssignedTo VARCHAR(100)
		,numRecOwner VARCHAR(100)
		,vcItemName VARCHAR(300)
		,numOnHand FLOAT
		,numOnOrder FLOAT
		,numAllocation FLOAT
		,numBackOrder FLOAT
		,numBarCodeId VARCHAR(50)
		,vcLocation VARCHAR(100)
		,vcModelID VARCHAR(200)
		,vcItemDesc VARCHAR(2000)
		,numUnitHour FLOAT
		,numUOMId VARCHAR(50)
		,vcAttributes VARCHAR(300)
		,vcPathForTImage VARCHAR(300)
		,numUnitHourReceived FLOAT
		,numQtyReceived FLOAT
		,vcNotes VARCHAR(MAX)
		,SerialLotNo VARCHAR(MAX)
		,vcSKU VARCHAR(50)
		,numRemainingQty FLOAT
		,numQtyToShipReceive FLOAT
		,monPrice DECIMAL(20,5)
		,monTotAmount DECIMAL(20,5)
		,vcBilled FLOAT
	)
	
	IF @bitGroupByOrder = 1
	BEGIN
		SET @vcSQLFinal = CONCAT('INSERT INTO #TEMPResult
								(
									numOppID
									,numWOID
									,numDivisionID
									,numTerID
									,vcCompanyName
									,vcPoppName
									,bintCreatedDate
									,numStatus
									,numAssignedTo
									,numRecOwner
								)
								SELECT
									OpportunityMaster.numOppID
									,WorkOrder.numWOID
									,DivisionMaster.numDivisionID
									,ISNULL(DivisionMaster.numTerID,0) numTerID
									,ISNULL(CompanyInfo.vcCompanyName,'''')
									,(CASE WHEN WorkOrder.numWOID IS NOT NULL THEN CONCAT(ISNULL(WorkOrder.vcWorkOrderName,''-''),(CASE WHEN (ISNULL(WorkOrder.numQtyItemsReq,0) - ISNULL(WorkOrder.numQtyBuilt,0)) > 0 THEN CONCAT('' Remaining build qty:'',ISNULL(WorkOrder.numQtyItemsReq,0) - ISNULL(WorkOrder.numQtyBuilt,0)) ELSE '''' END)) ELSE ISNULL(OpportunityMaster.vcPoppName,'''') END)
									,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,(CASE WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.bintCreatedDate  ELSE OpportunityMaster.bintCreatedDate END)),@numDomainID)
									,dbo.GetListIemName(OpportunityMaster.numStatus)
									,(CASE WHEN WorkOrder.numWOID IS NOT NULL THEN dbo.fn_GetContactName(WorkOrder.numAssignedTo) ELSE dbo.fn_GetContactName(OpportunityMaster.numAssignedTo) END)
									,(CASE WHEN WorkOrder.numWOID IS NOT NULL THEN dbo.fn_GetContactName(WorkOrder.numCreatedBy) ELSE dbo.fn_GetContactName(OpportunityMaster.numRecOwner) END)
								FROM
									#TEMPMSRecords TEMPOrder
								LEFT JOIN
									OpportunityMaster
								ON
									TEMPOrder.numOppID = OpportunityMaster.numOppId
								LEFT JOIN
									WorkOrder
								ON
									TEMPOrder.numWOID = WorkOrder.numWOID
								INNER JOIN
									DivisionMaster
								ON
									DivisionMaster.numDivisionID = (CASE WHEN WorkOrder.numWOID IS NOT NULL THEN @numDivisionID ELSE OpportunityMaster.numDivisionId END)
								INNER JOIN
									CompanyInfo
								ON
									DivisionMaster.numCompanyID = CompanyInfo.numCompanyId ORDER BY ',
								(CASE @vcSortOrder
									WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName '
									WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName'
									WHEN 'OpportunityMaster.bintCreatedDate' THEN 'OpportunityMaster.bintCreatedDate'
									WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(OpportunityMaster.numStatus)'
									WHEN 'OpportunityMaster.numAssignedTo' THEN 'dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)'
									WHEN 'OpportunityMaster.numRecOwner' THEN 'dbo.fn_GetContactName(OpportunityMaster.numRecOwner)'
									ELSE 'OpportunityMaster.bintCreatedDate'
								END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'DESC' END),' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY')


		EXEC sp_executesql @vcSQLFinal, N'@numDomainID NUMERIC(18,0), @ClientTimeZoneOffset INT, @numPageIndex INT, @numDivisionID NUMERIC(18,0)', @numDomainID, @ClientTimeZoneOffset, @numPageIndex, @numDivisionID;
	END
	ELSE
	BEGIN
		SET @vcSQLFinal = CONCAT('INSERT INTO #TEMPResult
								(
									numOppID
									,numWOID
									,numDivisionID
									,numTerID
									,vcCompanyName
									,vcPoppName
									,bintCreatedDate
									,numStatus
									,numAssignedTo
									,numRecOwner
									,numOppItemID
									,numWarehouseItemID
									,numItemCode
									,vcItemName
									,numOnHand
									,numOnOrder
									,numAllocation
									,numBackOrder
									,numBarCodeId
									,vcLocation
									,vcModelID
									,vcItemDesc
									,numUnitHour
									,numUOMId
									,vcAttributes
									,vcPathForTImage
									,numUnitHourReceived
									,numQtyReceived
									,vcNotes
									,SerialLotNo
									,vcSKU
									,numRemainingQty
									,numQtyToShipReceive
									,monPrice
									,monTotAmount
									,vcBilled
								)
								SELECT
									OpportunityMaster.numOppID
									,WorkOrder.numWOID
									,DivisionMaster.numDivisionID
									,ISNULL(DivisionMaster.numTerID,0) numTerID
									,ISNULL(CompanyInfo.vcCompanyName,'''')
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN ISNULL(WorkOrder.vcWorkOrderName,'''')
										ELSE ISNULL(OpportunityMaster.vcPoppName,'''')
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,WorkOrder.bintCreatedDate),@numDomainID)
										ELSE dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate),@numDomainID)
									END)
									,dbo.GetListIemName(OpportunityMaster.numStatus)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN dbo.fn_GetContactName(WorkOrder.numAssignedTo)
										ELSE dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN dbo.fn_GetContactName(WorkOrder.numCreatedBy)
										ELSE dbo.fn_GetContactName(OpportunityMaster.numRecOwner)
									END) 
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN 0
										ELSE OpportunityItems.numoppitemtCode
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.numWareHouseItemId
										ELSE ISNULL(OpportunityItems.numWarehouseItmsID,0)
									END)
									,Item.numItemCode
									,ISNULL(Item.vcItemName,'''')
									,ISNULL(numOnHand,0) + ISNULL(numAllocation,0)
									,ISNULL(numOnOrder,0)
									,ISNULL(numAllocation,0)
									,ISNULL(numBackOrder,0)
									,ISNULL(Item.numBarCodeId,'''')
									,ISNULL(WarehouseLocation.vcLocation,'''')
									,ISNULL(Item.vcModelID,'''')
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN ISNULL(WorkOrder.vcInstruction,'''')
										ELSE ISNULL(OpportunityItems.vcItemDesc,'''')
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN ISNULL(WorkOrder.numQtyItemsReq,0)
										ELSE ISNULL(OpportunityItems.numUnitHour,0)
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN ''Units''
										ELSE dbo.fn_GetUOMName(OpportunityItems.numUOMId)
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN ''''
										ELSE ISNULL(OpportunityItems.vcAttributes,'''')
									END)
									,ISNULL(Item.vcPathForTImage,'''')
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN ISNULL(WorkOrder.numUnitHourReceived,0)
										ELSE ISNULL(OpportunityItems.numUnitHourReceived,0)
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN ISNULL(WorkOrder.numQtyReceived,0)
										ELSE ISNULL(OpportunityItems.numQtyReceived,0)
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN ''''
										ELSE ISNULL(OpportunityItems.vcNotes,'''')
									END)
									,SUBSTRING((SELECT '','' + vcSerialNo + CASE WHEN isnull(Item.bitLotNo,0)=1 THEN '' ('' + CONVERT(VARCHAR(15),whi.numQty) + '')'' ELSE '''' end 
									FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=OpportunityItems.numOppId and oppI.numOppItemID=OpportunityItems.numoppitemtCode 
									ORDER BY vcSerialNo FOR XML PATH('''')),2,200000)
									,ISNULL(Item.vcSKU,'''')
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN (CASE 
																					WHEN @numViewID=1 THEN ISNULL(WorkOrder.numQtyBuilt,0) - ISNULL(WorkOrder.numQtyReceived,0) 
																					WHEN @numViewID=2 THEN ISNULL(WorkOrder.numQtyReceived,0) - ISNULL(WorkOrder.numUnitHourReceived,0) 
																					ELSE 0 
																				END)
										ELSE (CASE 
										WHEN @numViewID=1 THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyReceived,0) 
										WHEN @numViewID=2 THEN ISNULL(OpportunityItems.numQtyReceived,0) - ISNULL(OpportunityItems.numUnitHourReceived,0) 
										WHEN @numViewID=3 THEN (CASE 
																	WHEN @tintBillType=2 
																	THEN ISNULL(OpportunityItems.numQtyReceived,0) 
																	WHEN @tintBillType=3 THEN ISNULL(OpportunityItems.numUnitHourReceived,0) 
																	ELSE ISNULL(OpportunityItems.numUnitHour,0) 
																END)  - ISNULL((SELECT 
																					SUM(OpportunityBizDocItems.numUnitHour)
																				FROM
																					OpportunityBizDocs
																				INNER JOIN
																					OpportunityBizDocItems
																				ON
																					OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																				WHERE
																					OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																					AND OpportunityBizDocs.numBizDocId=644
																					AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
										ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numUnitHourReceived,0) 
									END)
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN (CASE 
																					WHEN @numViewID=1 THEN ISNULL(WorkOrder.numQtyBuilt,0) - ISNULL(WorkOrder.numQtyReceived,0) 
																					WHEN @numViewID=2 THEN ISNULL(WorkOrder.numQtyReceived,0) - ISNULL(WorkOrder.numUnitHourReceived,0) 
																					ELSE 0 
																				END)
										ELSE (CASE 
										WHEN @numViewID=1 THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyReceived,0) 
										WHEN @numViewID=2 THEN ISNULL(OpportunityItems.numQtyReceived,0) - ISNULL(OpportunityItems.numUnitHourReceived,0) 
										ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numUnitHourReceived,0) 
									END)
									END)
									,ISNULL(OpportunityItems.monPrice,0)
									,ISNULL(OpportunityItems.monTotAmount,0)
									,ISNULL((SELECT 
												SUM(OpportunityBizDocItems.numUnitHour)
											FROM
												OpportunityBizDocs
											INNER JOIN
												OpportunityBizDocItems
											ON
												OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
											WHERE
												OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
												AND OpportunityBizDocs.numBizDocId=644
												AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
												
								FROM
									#TEMPMSRecords TEMPOrder
								LEFT JOIN
									OpportunityMaster
								ON
									TEMPOrder.numOppID = OpportunityMaster.numOppId
								LEFT JOIN
									WorkOrder
								ON
									TEMPOrder.numWOID = WorkOrder.numWOID
								INNER JOIN
									DivisionMaster
								ON
									DivisionMaster.numDivisionID = (CASE WHEN WorkOrder.numWOID IS NOT NULL THEN @numDivisionID ELSE OpportunityMaster.numDivisionId END)
								INNER JOIN
									CompanyInfo
								ON
									DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
								LEFT JOIN 
									OpportunityItems
								ON
									OpportunityMaster.numOppId = OpportunityItems.numOppId
									AND TEMPOrder.numOppItemID = OpportunityItems.numoppitemtCode
								INNER JOIN
									Item
								ON
									(CASE WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.numItemCode ELSE OpportunityItems.numItemCode END) = Item.numItemCode
								INNER JOIN
									WareHouseItems
								ON
									(CASE WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.numWareHouseItemId ELSE OpportunityItems.numWarehouseItmsID END) = WareHouseItems.numWareHouseItemID
								LEFT JOIN
									WarehouseLocation
								ON
									WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID ORDER BY ',
								(CASE @vcSortOrder
									WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName '
									WHEN 'OpportunityMaster.vcPoppName' THEN '(CASE 
																					WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.vcWorkOrderName 
																					ELSE OpportunityMaster.vcPoppName
																				END)'
									WHEN 'OpportunityMaster.bintCreatedDate' THEN '(CASE 
																						WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.bintCreatedDate 
																						ELSE OpportunityMaster.bintCreatedDate
																					END)'
									WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(OpportunityMaster.numStatus)'
									WHEN 'OpportunityMaster.numAssignedTo' THEN '(CASE 
																						WHEN WorkOrder.numWOID IS NOT NULL THEN dbo.fn_GetContactName(WorkOrder.numAssignedTo) 
																						ELSE dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)
																					END)'
									WHEN 'OpportunityMaster.numRecOwner' THEN '(CASE 
																					WHEN WorkOrder.numWOID IS NOT NULL THEN dbo.fn_GetContactName(WorkOrder.numCreatedBy) 
																					ELSE dbo.fn_GetContactName(OpportunityMaster.numRecOwner)
																				END)'
									WHEN 'numOnHand' THEN 'ISNULL(WareHouseItems.numOnHand,0) + ISNULL(WareHouseItems.numAllocation,0)'
									WHEN 'numOnOrder' THEN 'ISNULL(WareHouseItems.numOnOrder,0)'
									WHEN 'numAllocation' THEN 'ISNULL(WareHouseItems.numAllocation,0)'
									WHEN 'numBackOrder' THEN 'ISNULL(WareHouseItems.numBackOrder,0)'
									WHEN 'numItemCode' THEN 'Item.numItemCode'
									WHEN 'vcItemName' THEN 'Item.vcItemName'
									WHEN 'numUnitHour' THEN '(CASE 
																WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.numQtyItemsReq
																ELSE OpportunityItems.numUnitHour
															END)'
									WHEN 'numUnitHourReceived' THEN '(CASE 
																		WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.numUnitHourReceived
																		ELSE OpportunityItems.numUnitHourReceived
																	END)'
									ELSE '(CASE 
												WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.bintCreatedDate 
												ELSE OpportunityMaster.bintCreatedDate
											END)'
								END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'DESC' END),' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY')

		EXEC sp_executesql @vcSQLFinal, N'@numDomainID NUMERIC(18,0), @numViewID NUMERIC(18,0), @ClientTimeZoneOffset INT, @numPageIndex INT, @tintBillType TINYINT, @numDivisionID NUMERIC(18,0)', @numDomainID, @numViewID, @ClientTimeZoneOffset, @numPageIndex, @tintBillType, @numDivisionID;
	END
	
	/*Show records which have been billed*/
	IF @numViewID=1 AND  @tintFlag=1 
	BEGIN
	
		SET @vcSQLFinal = CONCAT('SELECT T1.*',@vcCustomFields,' FROM #TEMPResult T1 where vcBilled>0',@vcCustomWhere)
		SET @numTotalRecords = (SELECT COUNT(*) FROM #TEMPResult where vcBilled>0 )
	END
	ELSE IF @numViewID= 3 AND  @tintFlag=2 /* Received but not billed*/
	BEGIN
	SET @vcSQLFinal = CONCAT('SELECT T1.*',@vcCustomFields,' FROM #TEMPResult T1 where numQtyReceived>0',@vcCustomWhere)
	SET @numTotalRecords = (SELECT COUNT(*) FROM #TEMPResult where numQtyReceived>0 )
	END
	else
	BEGIN 
		SET @vcSQLFinal = CONCAT('SELECT T1.*',@vcCustomFields,' FROM #TEMPResult T1 ',@vcCustomWhere)
		END
	


	EXEC sp_executesql @vcSQLFinal

	SELECT * FROM @TempFieldsLeft ORDER BY intRowNum

	IF OBJECT_ID('tempdb..#TEMPMSRecords') IS NOT NULL DROP TABLE #TEMPMSRecords
	IF OBJECT_ID('tempdb..#TEMPResult') IS NOT NULL DROP TABLE #TEMPResult
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetConfiguration')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetConfiguration
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetConfiguration]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
)
AS
BEGIN
	DECLARE @numDefaultSalesShippingDoc NUMERIC(18,0)

	SELECT @numDefaultSalesShippingDoc=ISNULL(numDefaultSalesShippingDoc,0) FROM Domain WHERE numDomainId=@numDomainID

	IF EXISTS (SELECT ID FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID)
	BEGIN
		SELECT 
			ISNULL(bitGroupByOrderForPick,0) bitGroupByOrderForPick
			,ISNULL(bitGroupByOrderForShip,0) bitGroupByOrderForShip
			,ISNULL(bitGroupByOrderForInvoice,0) bitGroupByOrderForInvoice
			,ISNULL(bitGroupByOrderForPay,0) bitGroupByOrderForPay
			,ISNULL(bitGroupByOrderForClose,0) bitGroupByOrderForClose
			,ISNULL(tintInvoicingType,1) tintInvoicingType
			,ISNULL(tintScanValue,1) tintScanValue
			,ISNULL(tintPackingMode,1) tintPackingMode
			,@numDefaultSalesShippingDoc numDefaultSalesShippingDoc
			,ISNULL(tintPendingCloseFilter,1) tintPendingCloseFilter
			,ISNULL(bitGeneratePickListByOrder,0) bitGeneratePickListByOrder
			,ISNULL(bitPickWithoutInventoryCheck,0) bitPickWithoutInventoryCheck
			,ISNULL(bitEnablePickListMapping,0) bitEnablePickListMapping
			,ISNULL(bitEnableFulfillmentBizDocMapping,0) bitEnableFulfillmentBizDocMapping
		FROM	
			MassSalesFulfillmentConfiguration
		WHERE
			numDomainID=@numDomainID
	END
	ELSE 
	BEGIN
		SELECT 
			0 bitGroupByOrderForPick
			,0 bitGroupByOrderForShip
			,0 bitGroupByOrderForInvoice
			,0 bitGroupByOrderForPay
			,0 bitGroupByOrderForClose
			,2 tintInvoicingType
			,1 tintScanValue
			,1 tintPackingMode
			,@numDefaultSalesShippingDoc numDefaultSalesShippingDoc
			,1 tintPendingCloseFilter
			,0 bitGeneratePickListByOrder
			,0 bitPickWithoutInventoryCheck
			,0 bitEnablePickListMapping
			,0 bitEnableFulfillmentBizDocMapping
	END
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetRecords')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetRecords
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetRecords]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@numViewID NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@bitGroupByOrder BIT
	,@bitRemoveBO BIT
	,@numShippingZone NUMERIC(18,0)
	,@bitIncludeSearch BIT
	,@vcOrderStauts VARCHAR(MAX)
	,@tintFilterBy TINYINT
	,@vcFilterValue VARCHAR(MAX)
	,@vcCustomSearchValue VARCHAR(MAX)
	,@numBatchID NUMERIC(18,0)
	,@tintPackingViewMode TINYINT
	,@tintPrintBizDocViewMode TINYINT
	,@tintPendingCloseFilter TINYINT
	,@numPageIndex INT
	,@vcSortColumn VARCHAR(100) OUTPUT
	,@vcSortOrder VARCHAR(4) OUTPUT
	,@numTotalRecords NUMERIC(18,0) OUTPUT
)
AS 
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF @numViewID = 4 OR @numViewID = 5 OR @numViewID = 6
	BEGIN
		SET @bitGroupByOrder = 1
	END
	
	SET @vcCustomSearchValue = REPLACE(@vcCustomSearchValue,'OpportunityItems.vcItemReleaseDate','OpportunityItems.ItemReleaseDate')

	If @numViewID = 6 AND CHARINDEX('OpportunityMaster.bintCreatedDate',@vcCustomSearchValue) > 0
	BEGIN
		SET @vcCustomSearchValue = REPLACE(@vcCustomSearchValue,'OpportunityMaster.bintCreatedDate','DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityBizDocs.dtCreatedDate)')
	END
	ELSE IF CHARINDEX('OpportunityMaster.bintCreatedDate',@vcCustomSearchValue) > 0
	BEGIN
		SET @vcCustomSearchValue = REPLACE(@vcCustomSearchValue,'OpportunityMaster.bintCreatedDate','DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate)')
	END

	IF @bitGroupByOrder = 1
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName','OpportunityBizDocs.monAmountPaid','OpportunityBizDocs.vcBizDocID','OpportunityMaster.bintCreatedDate','OpportunityMaster.bitPaidInFull','OpportunityMaster.numAge','OpportunityMaster.numStatus','OpportunityMaster.vcPoppName','Item.dtItemReceivedDate','OpportunityMaster.numAssignedTo','OpportunityMaster.numRecOwner')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'DESC'
		END
	END
	ELSE 
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName'
								,'Item.charItemType'
								,'Item.numBarCodeId'
								,'Item.numItemClassification'
								,'Item.numItemGroup'
								,'Item.vcSKU'
								,'OpportunityBizDocs.monAmountPaid'
								,'OpportunityMaster.dtExpectedDate'
								,'OpportunityItems.numQtyPacked'
								,'OpportunityItems.numQtyPicked'
								,'OpportunityItems.numQtyShipped'
								,'OpportunityItems.numRemainingQty'
								,'OpportunityItems.numUnitHour'
								,'OpportunityItems.vcInvoiced'
								,'OpportunityItems.vcItemName'
								,'OpportunityItems.vcItemReleaseDate'
								,'OpportunityMaster.bintCreatedDate'
								,'OpportunityMaster.bitPaidInFull'
								,'OpportunityMaster.numAge'
								,'OpportunityMaster.numStatus'
								,'OpportunityMaster.vcPoppName'
								,'WareHouseItems.vcLocation'
								,'Item.dtItemReceivedDate'
								,'OpportunityBizDocs.vcBizDocID')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'DESC'
		END
	END

	DECLARE @tintPackingMode TINYINT
	DECLARE @tintInvoicingType TINYINT
	DECLARE @tintCommitAllocation TINYINT
	DECLARE @bitEnablePickListMapping BIT
	DECLARE @bitEnableFulfillmentBizDocMapping BIT
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @numDefaultSalesShippingDoc NUMERIC(18,0)
	SELECT 
		@tintCommitAllocation=ISNULL(tintCommitAllocation,1) 
		,@numShippingServiceItemID=ISNULL(numShippingServiceItemID,0)
		,@numDefaultSalesShippingDoc=ISNULL(numDefaultSalesShippingDoc,0)
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID 

	IF @numViewID = 2 AND @tintPackingViewMode=3 AND @numDefaultSalesShippingDoc = 0
	BEGIN
		RAISERROR('DEFAULT_SHIPPING_BIZDOC_NOT_CONFIGURED',16,1)
		RETURN
	END

	SELECT
		@tintPackingMode=tintPackingMode
		,@tintInvoicingType=tintInvoicingType
		,@bitEnablePickListMapping=bitEnablePickListMapping
		,@bitEnableFulfillmentBizDocMapping=bitEnableFulfillmentBizDocMapping
	FROM
		MassSalesFulfillmentConfiguration
	WHERE
		numDomainID = @numDomainID 

	DECLARE @TempFieldsLeft TABLE
	(
		numPKID INT IDENTITY(1,1)
		,ID VARCHAR(50)
		,numFieldID NUMERIC(18,0)
		,vcFieldName VARCHAR(100)
		,vcOrigDbColumnName VARCHAR(100)
		,vcListItemType VARCHAR(10)
		,numListID NUMERIC(18,0)
		,bitAllowSorting BIT
		,bitAllowFiltering BIT
		,vcAssociatedControlType VARCHAR(20)
		,vcLookBackTableName VARCHAR(100)
		,bitCustomField BIT
		,intRowNum INT
		,intColumnWidth FLOAT
		,bitIsRequired BIT
		,bitIsEmail BIT
		,bitIsAlphaNumeric BIT
		,bitIsNumeric BIT
		,bitIsLengthValidation BIT
		,intMaxLength INT
		,intMinLength INT
		,bitFieldMessage BIT
		,vcFieldMessage VARCHAR(MAX)
		,Grp_id INT
	)

	INSERT INTO @TempFieldsLeft
	(
		ID
		,numFieldID
		,vcFieldName
		,vcOrigDbColumnName
		,vcListItemType
		,numListID
		,bitAllowSorting
		,bitAllowFiltering
		,vcAssociatedControlType
		,vcLookBackTableName
		,bitCustomField
		,intRowNum
		,intColumnWidth
		,bitIsRequired
		,bitIsEmail
		,bitIsAlphaNumeric
		,bitIsNumeric
		,bitIsLengthValidation
		,intMaxLength
		,intMinLength
		,bitFieldMessage
		,vcFieldMessage
		,Grp_id
	)
	SELECT
		CONCAT(141,'~',DFM.numFieldID,'~',0)
		,DFM.numFieldID
		,(CASE WHEN @numViewID=6 AND DFM.numFieldID=248 THEN 'BizDoc' ELSE DFFM.vcFieldName END)
		,DFM.vcOrigDbColumnName
		,ISNULL(DFM.vcListItemType,'')
		,ISNULL(DFM.numListID,0)
		,DFFM.bitAllowSorting
		,DFFM.bitAllowFiltering
		,DFFM.vcAssociatedControlType
		,DFM.vcLookBackTableName
		,0
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
		,0
		,0
		,0
		,(CASE WHEN vcFieldDataType IN ('N','M') THEN 1 ELSE 0 END)
		,0
		,0
		,0
		,0
		,''
		,0
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		DycFormField_Mapping DFFM
	ON
		DFCD.numFormId = DFFM.numFormID
		AND DFCD.numFieldId = DFFM.numFieldID
	INNER JOIN
		DycFieldMaster DFM
	ON
		DFFM.numFieldID = DFM.numFieldId
	WHERE 
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=141 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFFM.bitDeleted,0) = 0
		AND ISNULL(DFFM.bitSettingField,0) = 1
		AND ISNULL(DFCD.bitCustom,0) = 0
	UNION
	SELECT
		CONCAT(141,'~',CFM.Fld_id,'~',0)
		,CFM.Fld_id
		,CFM.Fld_label
		,CONCAT('CUST',CFM.Fld_id)
		,''
		,ISNULL(CFM.numListID,0)
		,0
		,0
		,CFM.Fld_type
		,''
		,1
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
		,ISNULL(bitIsRequired,0)
		,ISNULL(bitIsEmail,0)
		,ISNULL(bitIsAlphaNumeric,0)
		,ISNULL(bitIsNumeric,0)
		,ISNULL(bitIsLengthValidation,0)
		,ISNULL(intMaxLength,0)
		,ISNULL(intMinLength,0)
		,ISNULL(bitFieldMessage,0)
		,ISNULL(vcFieldMessage,'')
		,CFM.Grp_id
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		CFW_Fld_Master CFM
	ON
		DFCD.numFieldId = CFM.Fld_id
	LEFT JOIN
		CFW_Validation CV
	ON
		CFM.Fld_id = CV.numFieldID
	WHERE
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=141 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFCD.bitCustom,0) = 1

	IF (SELECT COUNT(*) FROM @TempFieldsLeft) = 0
	BEGIN
		INSERT INTO @TempFieldsLeft
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,vcListItemType
			,numListID
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,vcLookBackTableName
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(141,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,ISNULL(DFM.vcListItemType,'')
			,ISNULL(DFM.numListID,0)
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFFM.vcAssociatedControlType
			,DFM.vcLookBackTableName
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=141 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND ISNULL(DFFM.bitDefault,0) = 1
	END

	DECLARE @vcSelect NVARCHAR(MAX) = CONCAT('SELECT 
												OpportunityMaster.numOppID
												,OpportunityMaster.dtExpectedDate AS dtExpectedDateOrig
												,DivisionMaster.numDivisionID
												,ISNULL(DivisionMaster.numTerID,0) numTerID
												,ISNULL(DivisionMaster.intShippingCompany,0) AS numPreferredShipVia
												,ISNULL(DivisionMaster.numDefaultShippingServiceID,0) numPreferredShipService
												,ISNULL(OpportunityBizDocs.numOppBizDocsId,0) AS numOppBizDocID
												,ISNULL(OpportunityBizDocs.vcBizDocID,'''') vcBizDocID
												,ISNULL(OpportunityBizDocs.monDealAmount,0) - ISNULL(OpportunityBizDocs.monAmountPaid,0) AS monAmountToPay
												,',(CASE WHEN ISNULL(@bitGroupByOrder,0)=1 THEN '0' ELSE 'ISNULL(OpportunityItems.numoppitemtCode,0)' END),' AS numOppItemID
												',(CASE WHEN ISNULL(@bitGroupByOrder,0)=1
														THEN ',0 AS numRemainingQty'
														ELSE ',(CASE 
															WHEN @numViewID = 1
															THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
															WHEN @numViewID = 2 AND @tintPackingViewMode = 2
															THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) END)
															WHEN @numViewID = 2 AND @tintPackingViewMode = 3 AND ISNULL(OpportunityBizDocs.numOppBizDocsId,0) = 0
															THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL((SELECT 
																																																						SUM(OBIInner.numUnitHour)
																																																					FROM
																																																						OpportunityBizDocs OBInner
																																																					INNER JOIN
																																																						OpportunityBizDocItems OBIInner
																																																					ON
																																																						OBInner.numOppBizDocsId = OBIInner.numOppBizDocID
																																																					WHERE
																																																						OBInner.numOppId = OpportunityMaster.numOppID
																																																						AND OBInner.numBizDocId=@numDefaultSalesShippingDoc
					 																																																	AND OBIInner.numOppItemID = OpportunityItems.numoppitemtcode),0)
															WHEN @numViewID = 2 AND @tintPackingViewMode = 3 AND ISNULL(OpportunityBizDocs.numOppBizDocsId,0) > 0
															THEN ISNULL(OpportunityBizDocItems.numUnitHour,0)
															WHEN @numViewID = 3
															THEN (CASE 
																	WHEN ISNULL(@tintInvoicingType,0)=1 
																	THEN ISNULL(OpportunityItems.numQtyShipped,0) 
																	ELSE ISNULL(OpportunityItems.numUnitHour,0) 
																END) - ISNULL((SELECT 
																					SUM(OpportunityBizDocItems.numUnitHour)
																				FROM
																					OpportunityBizDocs
																				INNER JOIN
																					OpportunityBizDocItems
																				ON
																					OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																				WHERE
																					OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																					AND OpportunityBizDocs.numBizDocId=287
																					AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
															ELSE 0
														END) numRemainingQty'
													END)
													,','''' dtAnticipatedDelivery,'''' vcShipStatus,'''' vcPaymentStatus,0 numShipRate
													,(CASE 
														WHEN @numViewID = 2 AND @tintPackingViewMode = 2 AND ISNULL(@bitEnablePickListMapping,0) = 1 
														THEN CONCAT(''['',STUFF((SELECT 
																	CONCAT('', {"numOppBizDocsId":'',OB.numOppBizDocsId,'', "vcBizDocName":"'',OB.vcBizDocID,'' '',ISNULL(BT.vcTemplateName,''''),''"}'') 
																FROM 
																	OpportunityBizDocs OB
																LEFT JOIN
																	BizDocTemplate AS BT
																ON
																	OB.numBizDocTempID=BT.numBizDocTempID
																WHERE 
																	OB.numOppId = OpportunityMaster.numOppID
																	AND OB.numBizDocId=29397
																ORDER BY
																	OB.numOppBizDocsId
																FOR XML PATH('''')),1,1,''''),'']'')
														WHEN @numViewID = 3 AND ISNULL(@bitEnableFulfillmentBizDocMapping,0) = 1 
														THEN  CONCAT(''['',STUFF((SELECT 
																	CONCAT('', {"numOppBizDocsId":'',OB.numOppBizDocsId,'', "vcBizDocName":"'',OB.vcBizDocID,'' '',ISNULL(BT.vcTemplateName,''''),''"}'') 
																FROM 
																	OpportunityBizDocs OB
																LEFT JOIN
																	BizDocTemplate AS BT
																ON
																	OB.numBizDocTempID=BT.numBizDocTempID
																WHERE 
																	OB.numOppId = OpportunityMaster.numOppID
																	AND OB.numBizDocId IN (29397,296)
																ORDER BY
																	OB.numOppBizDocsId
																FOR XML PATH('''')),1,1,''''),'']'')
														ELSE ''''
													END) vcLinkingBizDocs')

	DECLARE @vcFrom NVARCHAR(MAX) = CONCAT(' FROM
												OpportunityMaster
											INNER JOIN
												DivisionMaster
											ON
												OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
											INNER JOIN
												CompanyInfo
											ON
												DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
											INNER JOIN
												OpportunityItems
											ON
												OpportunityMaster.numOppId = OpportunityItems.numOppId
											LEFT JOIN 
												MassSalesFulfillmentBatchOrders
											ON
												MassSalesFulfillmentBatchOrders.numBatchID = @numBatchID
												AND OpportunityMaster.numOppId = MassSalesFulfillmentBatchOrders.numOppID
												AND (OpportunityItems.numOppItemtCode = MassSalesFulfillmentBatchOrders.numOppItemID OR ISNULL(MassSalesFulfillmentBatchOrders.numOppItemID,0) = 0)
											LEFT JOIN
												WareHouseItems
											ON
												OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
											LEFT JOIN
												WarehouseLocation
											ON
												WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
											INNER JOIN
												Item
											ON
												OpportunityItems.numItemCode = Item.numItemCode
											',(CASE WHEN @numViewID=6 THEN ' INNER JOIN ' ELSE ' LEFT JOIN ' END),'
												OpportunityBizDocs
											ON
												OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
												AND OpportunityBizDocs.numBizDocID= ',(CASE 
																						WHEN @numViewID=4 THEN 287 
																						WHEN @numViewID=6 AND @tintPrintBizDocViewMode=1 THEN 55206 
																						WHEN @numViewID=6 AND @tintPrintBizDocViewMode=2 THEN 29397 
																						WHEN @numViewID=6 AND @tintPrintBizDocViewMode=3 THEN 296 
																						WHEN @numViewID=6 AND @tintPrintBizDocViewMode=4 THEN 287 
																						ELSE 0
																					END),'
												AND @numViewID IN (4,6) 
											LEFT JOIN
												OpportunityBizDocItems
											ON
												OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocId
												AND OpportunityItems.numoppitemtCode = OpportunityBizDocItems.numOppItemID')

	DECLARE @vcFrom1 NVARCHAR(MAX) = ' FROM
										OpportunityMaster
									INNER JOIN
										DivisionMaster
									ON
										OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
									INNER JOIN
										CompanyInfo
									ON
										DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
									INNER JOIN
										OpportunityBizDocs
									ON
										OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
										AND OpportunityBizDocs.numBizDocID= @numDefaultSalesShippingDoc
									INNER JOIN
										OpportunityBizDocItems
									ON
										OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocId
									INNER JOIN
										OpportunityItems
									ON
										OpportunityMaster.numOppId = OpportunityItems.numOppId
										AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode 
									LEFT JOIN 
										MassSalesFulfillmentBatchOrders
									ON
										MassSalesFulfillmentBatchOrders.numBatchID = @numBatchID
										AND OpportunityMaster.numOppId = MassSalesFulfillmentBatchOrders.numOppID
										AND (OpportunityItems.numOppItemtCode = MassSalesFulfillmentBatchOrders.numOppItemID OR ISNULL(MassSalesFulfillmentBatchOrders.numOppItemID,0) = 0)
									LEFT JOIN
										WareHouseItems
									ON
										OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
									LEFT JOIN
										WarehouseLocation
									ON
										WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
									INNER JOIN
										Item
									ON
										OpportunityItems.numItemCode = Item.numItemCode
										AND 1 = (CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 3 THEN (CASE WHEN ISNULL(Item.bitContainer,0)=1 THEN 0 ELSE 1 END) ELSE 1 END)'

	DECLARE @vcWhere VARCHAR(MAX) = CONCAT(' WHERE
												OpportunityMaster.numDomainId=@numDomainID
												AND OpportunityMaster.tintOppType = 1
												AND OpportunityMaster.tintOppStatus = 1'
												,(CASE 
													WHEN @numViewID=6 AND @tintPendingCloseFilter=1 THEN ' AND ISNULL(OpportunityMaster.tintshipped,0) = 0'
													WHEN @numViewID=6 AND @tintPendingCloseFilter=2 THEN ' AND ISNULL(OpportunityMaster.tintshipped,0) = 1'
													WHEN @numViewID=6 AND @tintPendingCloseFilter=3 THEN ''
													ELSE ' AND ISNULL(OpportunityMaster.tintshipped,0) = 0'
												END)
												,(CASE WHEN @numViewID <> 6 AND ISNULL(@numWarehouseID,0) > 0 THEN ' AND 1 = (CASE WHEN ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID THEN 1 ELSE 0 END) ELSE 1 END)' ELSE '' END)
												,(CASE 
													WHEN @numViewID IN (1,2) AND ISNULL(@bitRemoveBO,0) = 1
													THEN ' AND 1 = (CASE 
																		WHEN (Item.charItemType = ''p'' AND ISNULL(OpportunityItems.bitDropShip, 0) = 0 AND ISNULL(Item.bitAsset,0)=0)
																		THEN 
																			(CASE 
																				WHEN dbo.CheckOrderItemInventoryStatus(OpportunityItems.numItemCode,(CASE 
																																						WHEN @numViewID = 1
																																						THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
																																						WHEN @numViewID = 2 AND @tintPackingViewMode = 2
																																						THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) END)
																																						WHEN @numViewID = 2 AND @tintPackingViewMode = 3
																																						THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL((SELECT 
																																																																													SUM(OpportunityBizDocItems.numUnitHour)
																																																																												FROM
																																																																													OpportunityBizDocs
																																																																												INNER JOIN
																																																																													OpportunityBizDocItems
																																																																												ON
																																																																													OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																																																												WHERE
																																																																													OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																																																													AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
					 																																																																							AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
				
																																						ELSE OpportunityItems.numUnitHour
																																					END),OpportunityItems.numoppitemtCode,OpportunityItems.numWarehouseItmsID,(CASE WHEN ISNULL(Item.bitKitParent,0) = 1 THEN 1 ELSE 0 END)) > 0 THEN 0 ELSE 1 END)
																							ELSE 1
																				END)'
														ELSE ''
												END)
												,(CASE 
													WHEN LEN(ISNULL(@vcOrderStauts,'')) > 0 
													THEN (CASE 
															WHEN ISNULL(@bitIncludeSearch,0) = 0 
															THEN ' AND 1 = (CASE WHEN OpportunityMaster.numStatus NOT IN (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END)'
															ELSE ' AND 1 = (CASE WHEN OpportunityMaster.numStatus IN (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END)'
														END)
													ELSE ''
												END)
												,(CASE 
													WHEN LEN(ISNULL(@vcFilterValue,'')) > 0
													THEN
														CASE 
															WHEN @tintFilterBy=1 --Item Classification
															THEN ' AND 1 = (CASE 
																				WHEN ISNULL(@bitIncludeSearch,0) = 0 
																				THEN (CASE WHEN Item.numItemClassification NOT IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END) 
																				ELSE (CASE WHEN Item.numItemClassification IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END) 
																			END)'
															ELSE ''
														END
													ELSE ''
												END)
												,(CASE
													WHEN @numViewID = 1 THEN ' AND 1 = (CASE WHEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) > 0 THEN 1 ELSE 0 END)'
													WHEN @numViewID = 2 THEN ' AND 1 = (CASE 
																							WHEN @tintPackingViewMode = 1 
																							THEN (CASE 
																									WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=ISNULL(@numShippingServiceItemID,0)),0) = 0 
																									THEN 1 
																									ELSE 0 
																								END)
																							WHEN @tintPackingViewMode = 2
																							THEN (CASE 
																									WHEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL(OpportunityItems.numQtyShipped,0) > 0
																									THEN 1 
																									ELSE 0 
																								END)
																							WHEN @tintPackingViewMode = 3
																							THEN (CASE 
																									WHEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL((SELECT 
																																																																SUM(OBIInner.numUnitHour)
																																																															FROM
																																																																OpportunityBizDocs OBInner
																																																															INNER JOIN
																																																																OpportunityBizDocItems OBIInner
																																																															ON
																																																																OBInner.numOppBizDocsId = OBIInner.numOppBizDocID
																																																															WHERE
																																																																OBInner.numOppId = OpportunityMaster.numOppID
																																																																AND OBInner.numBizDocId=@numDefaultSalesShippingDoc
																																																																AND OBIInner.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
																									THEN 1 
																									ELSE 0 
																								END)
																							ELSE 0
																						END)'
													WHEN @numViewID = 3
													THEN ' AND 1 = (CASE 
																		WHEN (CASE 
																				WHEN ISNULL(@tintInvoicingType,0)=1 
																				THEN ISNULL(OpportunityItems.numQtyShipped,0) 
																				ELSE ISNULL(OpportunityItems.numUnitHour,0) 
																			END) - ISNULL((SELECT 
																								SUM(OpportunityBizDocItems.numUnitHour)
																							FROM
																								OpportunityBizDocs
																							INNER JOIN
																								OpportunityBizDocItems
																							ON
																								OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																							WHERE
																								OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																								AND OpportunityBizDocs.numBizDocId=287
																								AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0 
																		THEN 1
																		ELSE 0 
																	END)'
												END)
												,(CASE
															WHEN @numViewID = 1 THEN 'AND 1 = (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)'
															WHEN @numViewID = 2 AND @tintPackingViewMode <> 2 THEN ' AND 1 = (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)'
															WHEN @numViewID = 2 AND @tintPackingViewMode = 2 THEN ' AND 1=1'
															WHEN @numViewID = 3 THEN ' AND 1=1'
															WHEN @numViewID = 4 THEN ' AND 1=1'
															WHEN @numViewID = 5 THEN ' AND 1=1'
															WHEN @numViewID = 6 THEN ' AND 1=1'
															ELSE ' AND 1=0'
													END)
												,(CASE WHEN ISNULL(@numBatchID,0) > 0 THEN ' AND 1 = (CASE WHEN MassSalesFulfillmentBatchOrders.ID IS NOT NULL THEN 1 ELSE 0 END)' ELSE '' END)
												,(CASE WHEN @numViewID = 4 OR @numViewID=6 THEN ' AND 1= (CASE WHEN OpportunityBizDocs.numOppBizDocsId IS NOT NULL THEN 1 ELSE 0 END)' ELSE '' END)
												,(CASE WHEN @numViewID = 4 THEN ' AND 1 = (CASE WHEN ISNULL(OpportunityBizDocs.monDealAmount,0) - ISNULL(OpportunityBizDocs.monAmountPaid,0) > 0 THEN 1 ELSE 0 END)' ELSE '' END)
												,(CASE 
													WHEN @numViewID = 5 AND ISNULL(@tintPendingCloseFilter,0) = 2
													THEN 
														' AND 1 = (CASE 
																	WHEN (SELECT 
																				COUNT(*) 
																			FROM 
																			(
																				SELECT
																					OI.numoppitemtCode,
																					ISNULL(OI.numUnitHour,0) AS OrderedQty,
																					ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
																				FROM
																					OpportunityItems OI
																				INNER JOIN
																					Item I
																				ON
																					OI.numItemCode = I.numItemCode
																				OUTER APPLY
																				(
																					SELECT
																						SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
																					FROM
																						OpportunityBizDocs
																					INNER JOIN
																						OpportunityBizDocItems 
																					ON
																						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																					WHERE
																						OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																						AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
																						AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
																				) AS TempFulFilled
																				WHERE
																					OI.numOppID = OpportunityMaster.numOppID
																					AND ISNULL(OI.numUnitHour,0) > 0
																					AND ISNULL(OI.bitDropShip,0) = 0
																			) X
																			WHERE
																				X.OrderedQty <> X.FulFilledQty) = 0
																		AND (SELECT 
																				COUNT(*) 
																			FROM 
																			(
																				SELECT
																					OI.numoppitemtCode,
																					ISNULL(OI.numUnitHour,0) AS OrderedQty,
																					ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
																				FROM
																					OpportunityItems OI
																				INNER JOIN
																					Item I
																				ON
																					OI.numItemCode = I.numItemCode
																				OUTER APPLY
																				(
																					SELECT
																						SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
																					FROM
																						OpportunityBizDocs
																					INNER JOIN
																						OpportunityBizDocItems 
																					ON
																						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																					WHERE
																						OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																						AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
																						AND ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
																				) AS TempInvoice
																				WHERE
																					OI.numOppID = OpportunityMaster.numOppID
																					AND ISNULL(OI.numUnitHour,0) > 0
																			) X
																			WHERE
																				X.OrderedQty > X.InvoicedQty) = 0
																	THEN 1 
																	ELSE 0 
																END)'
															ELSE ''
														END)
												,(CASE 
													WHEN ISNULL(@numShippingZone,0) > 0 
													THEN ' AND 1 = (CASE 
																		WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),0) > 0 
																		THEN
																			CASE 
																				WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=@numDomainId AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)) AND numShippingZone=@numShippingZone) > 0 
																				THEN 1
																				ELSE 0
																			END
																		ELSE 0 
																	END)'
													ELSE ''
												END)
												,(CASE WHEN ISNULL(@bitGroupByOrder,0)=0 THEN ' AND 1 = (CASE WHEN ISNULL(OpportunityBizDocs.numOppBizDocsId,0) > 0 THEN (CASE WHEN OpportunityBizDocItems.numOppItemID > 0 THEN 1 ELSE 0 END) ELSE 1 END)' ELSE '' END))

	DECLARE @vcWhere1 VARCHAR(MAX) = CONCAT(' WHERE
												OpportunityMaster.numDomainId=@numDomainID
												AND OpportunityMaster.tintOppType = 1
												AND OpportunityMaster.tintOppStatus = 1
												AND ISNULL(OpportunityMaster.tintshipped,0) = 0
												AND 1 = (CASE 
															WHEN LEN(ISNULL(@vcOrderStauts,'''')) > 0 
															THEN (CASE 
																	WHEN ISNULL(@bitIncludeSearch,0) = 0 
																	THEN (CASE WHEN OpportunityMaster.numStatus NOT IN (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) 
																	ELSE (CASE WHEN OpportunityMaster.numStatus IN (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) 
																END)
															ELSE 1 
														END)
												AND 1 = (CASE 
															WHEN LEN(ISNULL(@vcFilterValue,'''')) > 0
															THEN
																CASE 
																	WHEN @tintFilterBy=1 --Item Classification
																	THEN (CASE 
																			WHEN ISNULL(@bitIncludeSearch,0) = 0 
																			THEN (CASE WHEN Item.numItemClassification NOT IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END) 
																			ELSE (CASE WHEN Item.numItemClassification IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END) 
																		END)
																	ELSE 1
																END
															ELSE 1
														END)
												AND 1 = (CASE 
															WHEN ISNULL((SELECT COUNT(*) FROM ShippingBox WHERE ISNULL(vcShippingLabelImage,'''') <> '''' AND ISNULL(vcTrackingNumber,'''') <> '''' AND numShippingReportId IN (SELECT numShippingReportId FROM ShippingReport WHERE ShippingReport.numOppID=OpportunityMaster.numOppId AND ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId)),0) = 0 
															THEN 1 
															ELSE 0 
														END)
												AND 1 = (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)
												AND 1 = (CASE WHEN ISNULL(@numWarehouseID,0) > 0 AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID THEN 1 ELSE 0 END) ELSE 1 END)
												AND 1 = (CASE WHEN ISNULL(@numBatchID,0) > 0 THEN (CASE WHEN MassSalesFulfillmentBatchOrders.ID IS NOT NULL THEN 1 ELSE 0 END) ELSE 1 END)'
												,(CASE 
													WHEN ISNULL(@numShippingZone,0) > 0 
													THEN ' AND 1 = (CASE 
																		WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),0) > 0 
																		THEN
																			CASE 
																				WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=@numDomainId AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)) AND numShippingZone=@numShippingZone) > 0 
																				THEN 1
																				ELSE 0
																			END
																		ELSE 0 
																	END)'
													ELSE ''
												END)
												,(CASE WHEN ISNULL(@bitGroupByOrder,0)=0 THEN ' AND 1 = (CASE WHEN ISNULL(OpportunityBizDocs.numOppBizDocsId,0) > 0 THEN (CASE WHEN OpportunityBizDocItems.numOppItemID > 0 THEN 1 ELSE 0 END) ELSE 1 END)' ELSE '' END))

	SET @vcWhere = CONCAT(@vcWhere,' ',@vcCustomSearchValue)
	SET @vcWhere1 = CONCAT(@vcWhere1,' ',@vcCustomSearchValue)
	
	DECLARE @vcGroupBy NVARCHAR(MAX) = (CASE 
											WHEN @bitGroupByOrder = 1 
											THEN 'GROUP BY OpportunityMaster.numOppID
																	,DivisionMaster.numDivisionID
																	,DivisionMaster.numTerID
																	,DivisionMaster.intShippingCompany
																	,DivisionMaster.numDefaultShippingServiceID
																	,OpportunityBizDocs.numOppBizDocsId
																	,OpportunityBizDocs.monDealAmount
																	,OpportunityBizDocs.monAmountPaid
																	,CompanyInfo.vcCompanyName
																	,OpportunityMaster.vcPoppName
																	,OpportunityMaster.numAssignedTo
																	,OpportunityMaster.numStatus
																	,OpportunityMaster.numRecOwner
																	,OpportunityMaster.bintCreatedDate
																	,OpportunityMaster.txtComments
																	,OpportunityBizDocs.vcBizDocID
																	,OpportunityMaster.intUsedShippingCompany
																	,DivisionMaster.intShippingCompany
																	,OpportunityBizDocs.dtCreatedDate
																	,OpportunityMaster.dtExpectedDate'
											ELSE '' 
										END)

	DECLARE @vcOrderBy NVARCHAR(MAX) = CONCAT(' ORDER BY ',
					(CASE @vcSortColumn
						WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName' 
						WHEN 'Item.charItemType' THEN '(CASE 
															WHEN Item.charItemType=''P''  THEN ''Inventory Item''
															WHEN Item.charItemType=''N'' THEN ''Non Inventory Item'' 
															WHEN Item.charItemType=''S'' THEN ''Service''
															WHEN Item.charItemType=''A'' THEN ''Accessory''
														END)'
						WHEN 'Item.numBarCodeId' THEN 'Item.numBarCodeId'
						WHEN 'Item.numItemClassification' THEN 'dbo.GetListIemName(Item.numItemClassification)'
						WHEN 'Item.numItemGroup' THEN 'ISNULL((SELECT vcItemGroup FROM ItemGroups WHERE numItemGroupID = Item.numItemGroup),'''')'
						WHEN 'Item.vcSKU' THEN 'Item.vcSKU'
						WHEN 'OpportunityItems.vcItemName' THEN 'Item.vcItemName'
						WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(OpportunityMaster.numStatus)'
						WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName'
						WHEN 'OpportunityMaster.numAssignedTo' THEN 'dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)'
						WHEN 'OpportunityMaster.numRecOwner' THEN 'dbo.fn_GetContactName(OpportunityMaster.numRecOwner)'
						WHEN 'WareHouseItems.vcLocation' THEN 'WarehouseLocation.vcLocation'
						WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TempPaid.monAmountPaid'
						WHEN 'OpportunityBizDocs.vcBizDocID' THEN 'LEN(OpportunityBizDocs.vcBizDocID) DESC, OpportunityBizDocs.vcBizDocID'
						WHEN 'OpportunityItems.numQtyPacked' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)'
						WHEN 'OpportunityItems.numQtyPicked' THEN 'OpportunityItems.numQtyPicked'
						WHEN 'OpportunityItems.numQtyShipped' THEN 'OpportunityItems.numQtyShipped'
						WHEN 'OpportunityItems.numRemainingQty' THEN '(CASE 
																			WHEN @numViewID = 1
																			THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
																			WHEN @numViewID = 2
																			THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)
																			ELSE 0
																		END)'
						WHEN 'OpportunityItems.numUnitHour' THEN 'OpportunityItems.numUnitHour'
						WHEN 'OpportunityItems.vcInvoiced' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)'
						WHEN 'OpportunityMaster.dtExpectedDate' THEN 'OpportunityMaster.dtExpectedDate'
						WHEN 'OpportunityItems.vcItemReleaseDate' THEN 'OpportunityItems.ItemReleaseDate'
						WHEN 'OpportunityMaster.bintCreatedDate' THEN 'OpportunityMaster.bintCreatedDate'
						WHEN 'OpportunityMaster.numAge' THEN 'OpportunityMaster.bintCreatedDate'
						WHEN 'OpportunityMaster.bitPaidInFull' THEN '(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)'
						WHEN 'Item.dtItemReceivedDate' THEN 'Item.dtItemReceivedDate'
						ELSE 'OpportunityMaster.bintCreatedDate'
					END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'ASC' END))

	DECLARE @vcOrderBy1 NVARCHAR(MAX) = CONCAT(' ORDER BY ',
					(CASE @vcSortColumn
						WHEN 'CompanyInfo.vcCompanyName' THEN 'TEMP.vcCompanyNameOrig' 
						WHEN 'Item.charItemType' THEN '(CASE 
															WHEN TEMP.charItemTypeOrig=''P''  THEN ''Inventory Item''
															WHEN TEMP.charItemTypeOrig=''N'' THEN ''Non Inventory Item'' 
															WHEN TEMP.charItemTypeOrig=''S'' THEN ''Service''
															WHEN TEMP.charItemTypeOrig=''A'' THEN ''Accessory''
														END)'
						WHEN 'Item.numBarCodeId' THEN 'TEMP.numBarCodeIdOrig'
						WHEN 'Item.numItemClassification' THEN 'dbo.GetListIemName(TEMP.numItemClassificationOrig)'
						WHEN 'Item.numItemGroup' THEN 'ISNULL((SELECT vcItemGroup FROM ItemGroups WHERE numItemGroupID = TEMP.numItemGroupOrig),'''')'
						WHEN 'Item.vcSKU' THEN 'TEMP.vcSKUOrig'
						WHEN 'OpportunityItems.vcItemName' THEN 'TEMP.vcItemNameOrig'
						WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(TEMP.numStatusOrig)'
						WHEN 'OpportunityMaster.vcPoppName' THEN 'TEMP.vcPoppNameOrig'
						WHEN 'OpportunityMaster.numAssignedTo' THEN 'dbo.fn_GetContactName(TEMP.numAssignedToOrig)'
						WHEN 'OpportunityMaster.numRecOwner' THEN 'dbo.fn_GetContactName(TEMP.numRecOwnerOrig)'
						WHEN 'WareHouseItems.vcLocation' THEN 'TEMP.vcLocationOrig'
						WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TEMP.monAmountPaidOrig'
						WHEN 'OpportunityBizDocs.vcBizDocID' THEN 'LEN(TEMP.vcBizDocID) DESC, TEMP.vcBizDocID'
						WHEN 'OpportunityItems.numQtyPacked' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=TEMP.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=TEMP.numOppItemID),0)'
						WHEN 'OpportunityItems.numQtyPicked' THEN 'TEMP.numQtyPickedOrig'
						WHEN 'OpportunityItems.numQtyShipped' THEN 'TEMP.numQtyShippedOrig'
						WHEN 'OpportunityItems.numRemainingQty' THEN '(CASE 
																			WHEN @numViewID = 1
																			THEN ISNULL(TEMP.numUnitHourOrig,0) - ISNULL(OpportunityItems.numQtyPickedOrig,0)
																			WHEN @numViewID = 2
																			THEN ISNULL(TEMP.numUnitHourOrig,0) - ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=TEMP.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=TEMP.numOppItemID),0)
																			ELSE 0
																		END)'
						WHEN 'OpportunityItems.numUnitHour' THEN 'TEMP.numUnitHourOrig'
						WHEN 'OpportunityItems.vcInvoiced' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=TEMP.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=TEMP.numOppItemID),0)'
						WHEN 'OpportunityMaster.dtExpectedDate' THEN 'TEMP.dtExpectedDateOrig'
						WHEN 'OpportunityItems.vcItemReleaseDate' THEN 'TEMP.ItemReleaseDateOrig'
						WHEN 'OpportunityMaster.bintCreatedDate' THEN 'TEMP.bintCreatedDateOrig'
						WHEN 'OpportunityMaster.numAge' THEN 'TEMP.bintCreatedDateOrig'
						WHEN 'OpportunityMaster.bitPaidInFull' THEN '(CASE WHEN ISNULL(TEMP.monAmountPaidOrig,0) >= ISNULL(TEMP.monDealAmountOrig,0) THEN 1 ELSE 0 END)'
						WHEN 'Item.dtItemReceivedDate' THEN 'TEMP.dtItemReceivedDate'
						ELSE 'TEMP.bintCreatedDateOrig'
					END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'ASC' END))

	DECLARE @vcText NVARCHAR(MAX)

	IF EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcOrigDbColumnName='bitPaidInFull') OR @vcSortColumn='OpportunityMaster.bitPaidInFull'
	BEGIN
		SET @vcText = ' OUTER APPLY
						(
							SELECT 
								SUM(OBInner.monAmountPaid) monAmountPaid
							FROM 
								OpportunityBizDocs OBInner 
							WHERE 
								OBInner.numOppId=OpportunityMaster.numOppId 
								AND OBInner.numBizDocId=287
						) TempPaid'

		SET @vcFrom = CONCAT(@vcFrom,@vcText)
		SET @vcFrom1 = CONCAT(@vcFrom1,@vcText)
	END

	IF EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcOrigDbColumnName='numQtyPacked') OR @vcSortColumn='OpportunityItems.numQtyPacked'
	BEGIN
		SET @vcText = ' OUTER APPLY
						(
							SELECT 
								SUM(OpportunityBizDocItems.numUnitHour) numPackedQty
							FROM 
								OpportunityBizDocs
							INNER JOIN
								OpportunityBizDocItems
							ON
								OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
							WHERE 
								OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
								AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
								AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode
						) TempPacked'

		SET @vcFrom = CONCAT(@vcFrom,@vcText)
		SET @vcFrom1 = CONCAT(@vcFrom1,@vcText)
	END

	DECLARE @j INT = 0
	DECLARE @jCount INT
	DECLARE @numFieldId NUMERIC(18,0)
	DECLARE @vcListItemType VARCHAR(10)
	DECLARE @vcFieldName AS VARCHAR(50)                                                                                                
	DECLARE @vcAssociatedControlType VARCHAR(10)  
	DECLARE @vcLookBackTableName VARCHAR(100)
	DECLARE @vcDbColumnName VARCHAR(100)
	DECLARE @Grp_id INT                                               
	DECLARE @bitCustomField BIT      
	DECLARE @bitIsNumeric BIT
	SET @jCount = (SELECT COUNT(*) FROM @TempFieldsLeft)


	WHILE @j <= @jCount
	BEGIN
		SELECT
			@numFieldId=numFieldID,
			@vcFieldName = vcFieldName,
			@vcAssociatedControlType = vcAssociatedControlType,
			@vcDbColumnName = (CASE WHEN ISNULL(bitCustomField,0) = 1 THEN CONCAT('Cust',numFieldID) ELSE vcOrigDbColumnName END),
			@vcLookBackTableName = vcLookBackTableName,
			@bitCustomField=ISNULL(bitCustomField,0),
			@Grp_id = ISNULL(Grp_id,0),
			@bitIsNumeric=ISNULL(bitIsNumeric,0),
			@vcListItemType=ISNULL(vcListItemType,'')
		FROM 
			@TempFieldsLeft
		WHERE 
			numPKID = @j

		IF @vcDbColumnName NOT IN ('vcBizDocID','numRemainingQty','dtAnticipatedDelivery','vcShipStatus','vcPaymentStatus','numShipRate')
		BEGIN
			IF ISNULL(@bitCustomField,0) = 0
			BEGIN
				IF (@vcLookBackTableName = 'Item' OR @vcLookBackTableName = 'WareHouseItems' OR @vcLookBackTableName = 'OpportunityItems') AND ISNULL(@bitGroupByOrder,0)=1
				BEGIN
					SET @vcSelect = CONCAT(@vcSelect,',',(CASE WHEN @bitIsNumeric = 1 THEN '0' ELSE '''''' END),' [',@vcDbColumnName,']')
				END
				ELSE
				BEGIN
					IF @vcDbColumnName = 'numAge'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',CONCAT(''<span style="color:'',(CASE
																							WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 4  
																							THEN ''#00ff00''
																							WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 4 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 8)
																							THEN ''#00b050''
																							WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 8 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 12)
																							THEN ''#3399ff''
																							WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 12 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 24)
																							THEN ''#ff9900''
																							WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) BETWEEN 1 AND 2  
																							THEN ''#9b3596''
																							ELSE ''#ff0000''
																						END),''">'',CONCAT(DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24,''d '',DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24,''h''),''</span>'')',' [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'dtItemReceivedDate'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',dbo.FormatedDateFromDate(Item.dtItemReceivedDate,@numDomainID) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'bitPaidInFull'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'dtExpectedDate'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',dbo.FormatedDateFromDate(OpportunityMaster.dtExpectedDate,@numDomainID) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'bintCreatedDate'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',(CASE WHEN @numViewID = 6 THEN dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityBizDocs.dtCreatedDate),@numDomainID) ELSE dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate),@numDomainID) END) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'numQtyPacked'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',ISNULL(TempPacked.numPackedQty,0) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vcInvoiced'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vcLocation'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',ISNULL(WarehouseLocation.vcLocation,'''') [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'charItemType'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',(CASE 
																	WHEN Item.charItemType=''P'' 
																	THEN 
																		CASE 
																			WHEN ISNULL(Item.bitAssembly,0) = 1 THEN ''Assembly''
																			WHEN ISNULL(Item.bitKitParent,0) = 1 THEN ''Kit''
																			WHEN ISNULL(Item.bitAsset,0) = 1 AND ISNULL(Item.bitRental,0)=0 THEN ''Asset''
																			WHEN ISNULL(Item.bitAsset,0) = 1 AND ISNULL(Item.bitRental,0)=1 THEN ''Rental Asset''
																			WHEN ISNULL(Item.bitSerialized,0) = 1 AND ISNULL(Item.bitAsset,0) = 0  THEN ''Serialized''
																			WHEN ISNULL(Item.bitSerialized,0) = 1 AND ISNULL(Item.bitAsset,0) = 1 AND ISNULL(Item.bitRental,0)=0 THEN ''Serialized Asset''
																			WHEN ISNULL(Item.bitSerialized,0) = 1 AND ISNULL(Item.bitAsset,0) = 1 AND ISNULL(Item.bitRental,0)=1 THEN ''Serialized Rental Asset''
																			WHEN ISNULL(Item.bitLotNo,0)=1 THEN ''Lot #''
																			ELSE ''Inventory Item'' 
																		END
																	WHEN Item.charItemType=''N'' THEN ''Non Inventory Item'' 
																	WHEN Item.charItemType=''S'' THEN ''Service'' 
																	WHEN Item.charItemType=''A'' THEN ''Accessory'' 
																END)',' [',@vcDbColumnName,']') 
					END
					ELSE IF @vcDbColumnName = 'vcItemReleaseDate'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',dbo.FormatedDateFromDate(CAST(OpportunityItems.ItemReleaseDate AS DATETIME),@numDomainID) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'numStatus'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',dbo.GetListIemName(',@vcLookBackTableName,'.',@vcDbColumnName,') [',@vcDbColumnName,']')
					END
					ELSE IF @vcAssociatedControlType = 'SelectBox'
					BEGIN
						IF @vcListItemType = 'LI'
						BEGIN
							SET @vcSelect = CONCAT(@vcSelect,',L',CONVERT(VARCHAR(3),@j),'.vcData',' [',@vcDbColumnName,']')

							SET @vcFrom = CONCAT(@vcFrom ,' LEFT JOIN ListDetails L',@j,' ON L',@j,'.numListItemID=',@vcLookBackTableName,'.',@vcDbColumnName)
							SET @vcFrom1 = CONCAT(@vcFrom1 ,' LEFT JOIN ListDetails L',@j,' ON L',@j,'.numListItemID=',@vcLookBackTableName,'.',@vcDbColumnName)
						END
						ELSE IF @vcListItemType = 'IG'
						BEGIN
							SET @vcSelect = CONCAT(@vcSelect,',ISNULL((SELECT vcItemGroup FROM ItemGroups WHERE numItemGroupID = I.numItemGroup),'''')',' [',@vcDbColumnName,']') 
						END
						ELSE IF @vcListItemType = 'U'
						BEGIN
							SET @vcSelect = CONCAT(@vcSelect,',dbo.fn_GetContactName(',@vcLookBackTableName,'.',@vcDbColumnName,') [',@vcDbColumnName,']')
						END
						ELSE
						BEGIN
							SET @vcSelect = CONCAT(@vcSelect,',',(CASE WHEN @bitIsNumeric = 1 THEN '0' ELSE '''''' END),' [',@vcDbColumnName,']')
						END
					END
					ELSE IF @vcAssociatedControlType = 'DateField'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',dbo.FormatedDateFromDate(DATEADD(MINUTE,',-@ClientTimeZoneOffset,',',@vcLookBackTableName,'.',@vcDbColumnName,'),',@numDomainId,') [', @vcDbColumnName,']')
					END
					ELSE IF @vcAssociatedControlType = 'TextBox'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',ISNULL(',@vcLookBackTableName,'.',@vcDbColumnName,(CASE WHEN @bitIsNumeric = 1 THEN ',0' ELSE ',''''' END),') [',@vcDbColumnName,']')
					END
					ELSE IF @vcAssociatedControlType = 'TextArea'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',ISNULL(',@vcLookBackTableName,'.',@vcDbColumnName,(CASE WHEN @bitIsNumeric = 1 THEN ',0' ELSE ',''''' END),') [',@vcDbColumnName,']')
					END
					ELSE IF @vcAssociatedControlType = 'Label'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',ISNULL(',@vcLookBackTableName,'.',@vcDbColumnName,(CASE WHEN @bitIsNumeric = 1 THEN ',0' ELSE '''''' END),') [',@vcDbColumnName,']')
					END
					ELSE
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',ISNULL(',@vcLookBackTableName,'.',@vcDbColumnName,(CASE WHEN @bitIsNumeric = 1 THEN ',0' ELSE '''''' END),') [',@vcDbColumnName,']')
					END
				END
			END
			ELSE IF @bitCustomField = 1
			BEGIN         
				IF @Grp_id = 5
				BEGIN
					IF ISNULL(@bitGroupByOrder,0) = 1
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',''''',' [',@vcDbColumnName,']')
					END
					ELSE
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,CONCAT(',dbo.GetCustFldValueOppItems(',@numFieldId,',OpportunityItems.numoppitemtCode,Item.numItemCode)'),' [',@vcDbColumnName,']')
					END
				END
				ELSE
				BEGIN
					IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea' 
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',CFW',@numFieldId,'.Fld_Value  [',@vcDbColumnName,']')
             
						SET @vcText = CONCAT(' left Join CFW_FLD_Values_Opp CFW',@numFieldId
										,' ON CFW' , @numFieldId , '.Fld_Id='
										,@numFieldId
										, ' and CFW' , @numFieldId
										, '.RecId=OpportunityMaster.numOppID')

						SET @vcFrom = CONCAT(@vcFrom,@vcText)
						SET @vcFrom1 = CONCAT(@vcFrom1,@vcText)
					END   
					ELSE IF @vcAssociatedControlType = 'CheckBox' 
					BEGIN
						SET @vcSelect =CONCAT( @vcSelect
							, ',case when isnull(CFW'
							, @numFieldId
							, '.Fld_Value,0)=0 then 0 when isnull(CFW'
							, @numFieldId
							, '.Fld_Value,0)=1 then 1 end   ['
							,  @vcDbColumnName
							, ']')
							
						SET @vcText = CONCAT(' left Join CFW_FLD_Values_Opp CFW'
							, @numFieldId
							, ' ON CFW',@numFieldId,'.Fld_Id='
							, @numFieldId
							, ' and CFW'
							, @numFieldId
							, '.RecId=OpportunityMaster.numOppID')

						SET @vcFrom = CONCAT(@vcFrom,@vcText)
						SET @vcFrom1 = CONCAT(@vcFrom1,@vcText)                                                 
					END                
					ELSE IF @vcAssociatedControlType = 'DateField' 
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect
							, ',dbo.FormatedDateFromDate(CFW'
							, @numFieldId
							, '.Fld_Value,'
							, @numDomainId
							, ')  [',@vcDbColumnName ,']' )  
					                  
						SET @vcText = CONCAT(' left Join CFW_FLD_Values_Opp CFW'
							, @numFieldId
							, ' on CFW', @numFieldId, '.Fld_Id='
							, @numFieldId
							, ' and CFW'
							, @numFieldId
							, '.RecId=OpportunityMaster.numOppID')

						SET @vcFrom = CONCAT(@vcFrom,@vcText)
						SET @vcFrom1 = CONCAT(@vcFrom1,@vcText)                                                 
					END                
					ELSE IF @vcAssociatedControlType = 'SelectBox' 
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',L',@numFieldId,'.vcData',' [',@vcDbColumnName,']')
					        
						SET @vcText = CONCAT(' left Join CFW_FLD_Values_Opp CFW'
							, @numFieldId
							, ' on CFW',@numFieldId ,'.Fld_Id='
							, @numFieldId
							, ' and CFW'
							, @numFieldId
							, '.RecId=OpportunityMaster.numOppID')

						SET @vcFrom = CONCAT(@vcFrom,@vcText)
						SET @vcFrom1 = CONCAT(@vcFrom1,@vcText)

						SET @vcText = CONCAT(' left Join ListDetails L'
							, @numFieldId
							, ' on L'
							, @numFieldId
							, '.numListItemID=CFW'
							, @numFieldId
							, '.Fld_Value')

						SET @vcFrom = CONCAT(@vcFrom,@vcText)
						SET @vcFrom1 = CONCAT(@vcFrom1,@vcText)            
					END
					ELSE
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',OpportunityMaster.numOppID)'),' [',@vcDbColumnName,']')
					END 
				END
			END
		END

		SET @j = @j + 1
	END

	PRINT CAST(@vcSelect AS NTEXT)
	PRINT CAST(@vcFrom AS NTEXT)
	PRINT CAST(@vcWhere AS NTEXT)
	PRINT CAST(@vcGroupBy AS NTEXT)
	PRINT CAST(@vcOrderBy AS NTEXT)

	DECLARE @vcFinal NVARCHAR(MAX) = ''

	If @numViewID = 2 AND @tintPackingViewMode = 3
	BEGIN
		SET @vcSelect = CONCAT(@vcSelect,',',(CASE @vcSortColumn
													WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName AS vcCompanyNameOrig' 
													WHEN 'Item.charItemType' THEN 'Item.charItemType AS charItemTypeOrig'
													WHEN 'Item.numBarCodeId' THEN 'Item.numBarCodeId AS numBarCodeIdOrig'
													WHEN 'Item.numItemClassification' THEN 'Item.numItemClassification AS numItemClassificationOrig'
													WHEN 'Item.numItemGroup' THEN 'Item.numItemGroup AS numItemGroupOrig'
													WHEN 'Item.vcSKU' THEN 'Item.vcSKU AS vcSKUOrig'
													WHEN 'OpportunityItems.vcItemName' THEN 'Item.vcItemName AS vcItemNameOrig'
													WHEN 'OpportunityMaster.numStatus' THEN 'OpportunityMaster.numStatus AS numStatusOrig '
													WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName AS vcPoppNameOrig'
													WHEN 'OpportunityMaster.numAssignedTo' THEN 'OpportunityMaster.numAssignedTo AS numAssignedToOrig'
													WHEN 'OpportunityMaster.numRecOwner' THEN 'OpportunityMaster.numRecOwner AS numRecOwnerOrig'
													WHEN 'WareHouseItems.vcLocation' THEN 'WarehouseLocation.vcLocation AS vcLocationOrig'
													WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TempPaid.monAmountPaid AS monAmountPaidOrig'
													WHEN 'OpportunityItems.numQtyPicked' THEN 'OpportunityItems.numQtyPicked AS numQtyPickedOrig'
													WHEN 'OpportunityItems.numQtyShipped' THEN 'OpportunityItems.numQtyShipped AS numQtyShippedOrig'
													WHEN 'OpportunityItems.numRemainingQty' THEN 'OpportunityItems.numUnitHour AS numUnitHourOrig,OpportunityItems.numQtyPicked AS numQtyPickedOrig'
													WHEN 'OpportunityItems.numUnitHour' THEN 'OpportunityItems.numUnitHour AS numUnitHourOrig'
													WHEN 'OpportunityMaster.dtExpectedDate' THEN 'OpportunityMaster.dtExpectedDate AS dtExpectedDateOrig'
													WHEN 'OpportunityItems.vcItemReleaseDate' THEN 'OpportunityItems.ItemReleaseDate AS ItemReleaseDateOrig'
													WHEN 'OpportunityMaster.bintCreatedDate' THEN 'OpportunityMaster.bintCreatedDate AS bintCreatedDateOrig'
													WHEN 'OpportunityMaster.numAge' THEN 'OpportunityMaster.bintCreatedDate AS bintCreatedDateOrig'
													WHEN 'OpportunityMaster.bitPaidInFull' THEN 'TempPaid.monAmountPaid AS monAmountPaidOrig, OpportunityMaster.monDealAmount AS monDealAmountOrig'
													WHEN 'Item.dtItemReceivedDate' THEN 'Item.dtItemReceivedDate AS dtItemReceivedDateOrig'
													ELSE 'OpportunityMaster.bintCreatedDate AS bintCreatedDateOrig'
												END))

		DECLARE @vcGroupBy1 NVARCHAR(MAX) = (CASE 
											WHEN @bitGroupByOrder = 1 
											THEN CONCAT('GROUP BY TEMP.numOppId
																	,TEMP.numDivisionID
																	,TEMP.numTerID
																	,TEMP.intShippingCompany
																	,TEMP.numDefaultShippingServiceID
																	,TEMP.numOppBizDocsId
																	,TEMP.monDealAmount
																	,TEMP.monAmountPaid
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='CompanyInfo' AND vcOrigDbColumnName='vcCompanyName') THEN 'TEMP.vcCompanyName' ELSE '' END),'
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='vcPoppName') THEN 'TEMP.vcPoppName' ELSE '' END),'
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='numAssignedTo') THEN 'TEMP.numAssignedTo' ELSE '' END),'.
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='numRecOwner') THEN 'TEMP.numRecOwner' ELSE '' END),'.
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='numStatus') THEN 'TEMP.numStatus' ELSE '' END),'.
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='bintCreatedDate') THEN 'TEMP.bintCreatedDate' ELSE '' END),'.
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='txtComments') THEN 'TEMP.txtComments' ELSE '' END),'.
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityBizDocs' AND vcOrigDbColumnName='vcBizDocID') THEN 'TEMP.vcBizDocID' ELSE '' END),'.
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='intUsedShippingCompany') THEN 'TEMP.intUsedShippingCompany' ELSE '' END),'.
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='DivisionMaster' AND vcOrigDbColumnName='intShippingCompany') THEN 'TEMP.intShippingCompany' ELSE '' END),'.
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityBizDocs' AND vcOrigDbColumnName='dtCreatedDate') THEN 'TEMP.dtCreatedDate' ELSE '' END),'.
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='dtExpectedDate') THEN 'TEMP.dtExpectedDate' ELSE '' END),'.
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='Item' AND vcOrigDbColumnName='dtItemReceivedDate') THEN 'TEMP.dtItemReceivedDateOrig' ELSE '' END),'.')
											ELSE '' 
										END)
		SET @vcFinal= CONCAT('SELECT COUNT(*) OVER() AS TotalRecords,* FROM (',@vcSelect,@vcFrom,@vcWhere,' UNION ',@vcSelect,@vcFrom1,@vcWhere1,') TEMP',@vcGroupBy1,@vcOrderBy1,' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY OPTION (OPTIMIZE FOR (@numDomainID UNKNOWN))')
	END
	ELSE
	BEGIN
		SET @vcFinal = CONCAT(@vcSelect,',COUNT(*) OVER() AS TotalRecords',@vcFrom,@vcWhere,@vcGroupBy,@vcOrderBy,' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY OPTION (OPTIMIZE FOR (@numDomainID UNKNOWN))')
	END

	EXEC sp_executesql @vcFinal, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @tintPackingMode TINYINT, @bitRemoveBO BIT, @ClientTimeZoneOffset INT,@numBatchID NUMERIC(18,0),@tintPrintBizDocViewMode TINYINT,@tintPendingCloseFilter TINYINT, @bitIncludeSearch BIT, @bitEnablePickListMapping BIT, @bitEnableFulfillmentBizDocMapping BIT,@numPageIndex INT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @tintPackingMode, @bitRemoveBO, @ClientTimeZoneOffset,@numBatchID,@tintPrintBizDocViewMode,@tintPendingCloseFilter,@bitIncludeSearch,@bitEnablePickListMapping, @bitEnableFulfillmentBizDocMapping,@numPageIndex;

	SELECT * FROM @TempFieldsLeft ORDER BY intRowNum
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetRecordsForPicking')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetRecordsForPicking
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetRecordsForPicking]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@bitGroupByOrder BIT
	,@numBatchID NUMERIC(18,0)
	,@vcSelectedRecords VARCHAR(MAX)
)
AS
BEGIN
	DECLARE @TempFieldsRight TABLE
	(
		ID VARCHAR(50)
		,numFieldID NUMERIC(18,0)
		,vcFieldName VARCHAR(100)
		,vcOrigDbColumnName VARCHAR(100)
		,bitAllowSorting BIT
		,bitAllowFiltering BIT
		,vcAssociatedControlType VARCHAR(20)
		,bitCustomField BIT
		,intRowNum INT
		,intColumnWidth FLOAT
	)

	INSERT INTO @TempFieldsRight
	(
		ID
		,numFieldID
		,vcFieldName
		,vcOrigDbColumnName
		,bitAllowSorting
		,bitAllowFiltering
		,vcAssociatedControlType
		,bitCustomField
		,intRowNum
		,intColumnWidth
	)
	SELECT
		CONCAT(142,'~',DFM.numFieldID,'~',0)
		,DFM.numFieldID
		,DFFM.vcFieldName
		,DFM.vcOrigDbColumnName
		,DFFM.bitAllowSorting
		,DFFM.bitAllowFiltering
		,DFM.vcAssociatedControlType
		,0
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		DycFormField_Mapping DFFM
	ON
		DFCD.numFormId = DFFM.numFormID
		AND DFCD.numFieldId = DFFM.numFieldID
	INNER JOIN
		DycFieldMaster DFM
	ON
		DFFM.numFieldID = DFM.numFieldId
	WHERE 
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=142 
		AND ISNULL(DFFM.bitDeleted,0) = 0
		AND ISNULL(DFFM.bitSettingField,0) = 1
		AND ISNULL(DFCD.bitCustom,0) = 0

	IF (SELECT COUNT(*) FROM @TempFieldsRight) = 0
	BEGIN
		INSERT INTO @TempFieldsRight
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(142,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=142 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND (ISNULL(DFFM.bitDefault,0) = 1 OR ISNULL(DFFM.bitRequired,0) = 1)
	END
	ELSE
	BEGIN
		INSERT INTO @TempFieldsRight
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(142,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=142 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND ISNULL(DFFM.bitRequired,0) = 1
			AND DFM.numFieldId NOT IN (SELECT T1.numFieldID FROM @TempFieldsRight T1)
	END


	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
	)

	IF ISNULL(@numBatchID,0) > 0
	BEGIN
		INSERT INTO @TEMP
		(
			numOppID
			,numOppItemID
		)
		SELECT
			numOppID
			,ISNULL(numOppItemID,0)
		FROM
			MassSalesFulfillmentBatchOrders
		WHERE
			numBatchID=@numBatchID
	END
	ELSE
	BEGIN
		INSERT INTO @TEMP
		(
			numOppID
			,numOppItemID
		)
		SELECT
			CAST(SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)) AS NUMERIC(18,0)) numOppID
			,CAST(SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1,LEN(OutParam)) AS NUMERIC(18,0)) numOppItemID
		FROM
			dbo.SplitString(@vcSelectedRecords,',')
	END

	DECLARE @TEMPItems TABLE
	(	
		ID NUMERIC(18,0)
		,numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numWarehouseID NUMERIC(18,0)
		,numWareHouseItemID NUMERIC(18,0)
		,vcWarehouse VARCHAR(300)
		,vcLocation VARCHAR(300)
		,vcKitChildItems VARCHAR(MAX)
		,vcKitChildKitItems VARCHAR(MAX)
		,vcPoppName VARCHAR(300)
		,numBarCodeId VARCHAR(300)
		,vcItemName VARCHAR(300)
		,vcItemDesc VARCHAR(MAX)
		,bitSerial BIT
		,bitLot BIT
		,vcSKU VARCHAR(300)
		,vcPathForTImage VARCHAR(MAX)
		,vcInclusionDetails VARCHAR(MAX)
		,numRemainingQty FLOAT
		,numQtyPicked FLOAT
	)

	INSERT INTO @TEMPItems
	(	
		ID
		,numOppID
		,numOppItemID 
		,numItemCode
		,numWarehouseID
		,numWareHouseItemID
		,vcWarehouse 
		,vcLocation
		,vcKitChildItems
		,vcKitChildKitItems
		,vcPoppName
		,numBarCodeId
		,vcItemName
		,vcItemDesc
		,bitSerial
		,bitLot
		,vcSKU
		,vcPathForTImage
		,vcInclusionDetails
		,numRemainingQty
		,numQtyPicked
	)
	SELECT 
		T1.ID
		,OpportunityMaster.numOppID
		,OpportunityItems.numoppitemtCode
		,OpportunityItems.numItemCode
		,WareHouseItems.numWareHouseID
		,WareHouseItems.numWareHouseItemID
		,Warehouses.vcWareHouse
		,WarehouseLocation.vcLocation
		,ISNULL(STUFF((SELECT CONCAT(', ',OpportunityItems.numItemCode,'-',OKI.numChildItemID) FROM OpportunityKitItems OKI WHERE OKI.numOppItemID=1 ORDER BY OKI.numChildItemID FOR XML PATH('')),1,1,''),'') AS vcKitChildItems
		,ISNULL(STUFF((SELECT CONCAT(', ',OpportunityItems.numItemCode,'-',OKI.numChildItemID,'-',OKCI.numItemID) FROM OpportunityKitChildItems OKCI INNER JOIN OpportunityKitItems OKI ON OKCI.numOppChildItemID = OKI.numOppChildItemID  WHERE OKCI.numOppItemID=1 ORDER BY OKI.numChildItemID,OKCI.numItemID FOR XML PATH('')),1,1,''),'') vcKitChildKitItems
		,OpportunityMaster.vcPoppName
		,Item.numBarCodeId
		,Item.vcItemName
		,Item.txtItemDesc AS vcItemDesc
		,ISNULL(Item.bitSerialized,0)
		,ISNULL(Item.bitLotNo,0)
		,Item.vcSKU
		,ISNULL(ItemImages.vcPathForTImage,'') vcPathForTImage
		,ISNULL(OpportunityItems.vcInclusionDetail,'') AS vcInclusionDetails
		,ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) numRemainingQty
		,0 numQtyPicked
	FROM
		@TEMP T1
	INNER JOIN
		OpportunityMaster
	ON
		OpportunityMaster.numOppID= T1.numOppID
	INNER JOIN
		OpportunityItems
	ON
		OpportunityMaster.numOppID = OpportunityItems.numOppID
		AND (OpportunityItems.numOppItemtcode = T1.numOppItemID OR ISNULL(T1.numOppItemID,0) = 0)
	INNER JOIN
		Item 
	ON
		OpportunityItems.numItemCode = Item.numItemCode
	LEFT JOIN
		ItemImages
	ON
		ItemImages.numDomainId=@numDomainID
		AND ItemImages.numItemCode = Item.numItemCode
		AND ItemImages.bitDefault=1 
		AND ItemImages.bitIsImage=1
	INNER JOIN
		WareHouseItems
	ON
		OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
	INNER JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	LEFT JOIN
		WarehouseLocation
	ON
		WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
	WHERE
		OpportunityMaster.numDomainID=@numDomainID
		AND ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) > 0
	ORDER BY
		T1.ID, OpportunityItems.numSortOrder

	IF ISNULL(@bitGroupByOrder,0) = 1
	BEGIN
		SELECT
			TEMP.numOppId
			,0  AS numOppItemID
			,TEMP.numItemCode
			,TEMP.numWareHouseID
			,TEMP.vcKitChildItems
			,TEMP.vcKitChildKitItems
			,STUFF((SELECT 
						CONCAT(',',T1.numOppItemID,'(',T1.numRemainingQty,')') 
					FROM 
						@TEMPItems T1 
					WHERE 
						T1.numOppId = TEMP.numOppId
						AND T1.numItemCode = TEMP.numItemCode
						AND T1.numWareHouseID = TEMP.numWareHouseID
						AND ISNULL(T1.vcKitChildItems,'') = ISNULL(TEMP.vcKitChildItems,'')
						AND ISNULL(T1.vcKitChildKitItems,'') = ISNULL(TEMP.vcKitChildKitItems ,'')
					FOR XML PATH('')),1,1,'') vcOppItemIDs
			,vcPoppName
			,numBarCodeId
			,CONCAT(TEMP.vcWarehouse, (CASE WHEN LEN(ISNULL(TEMP.vcLocation,'')) > 0 THEN CONCAT(' (',TEMP.vcLocation,')') ELSE '' END)) vcLocation
			,vcItemName
			,vcItemDesc
			,bitSerial
			,bitLot
			,vcSKU
			,dbo.fn_GetItemAttributes(@numDomainID,TEMP.numItemCode) vcAttributes
			,vcPathForTImage
			,MIN(vcInclusionDetails) vcInclusionDetails
			,SUM(TEMP.numRemainingQty) numRemainingQty
			,0 numQtyPicked
			,(CASE 
				WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner INNER JOIN WarehouseLocation WL ON WIInner.numWLocationID=WL.numWLocationID WHERE WIInner.numDomainID=@numDomainID AND WIInner.numItemID=TEMP.numItemCode AND WIInner.numWareHouseID = TEMP.numWareHouseID)
				THEN CONCAT('[',STUFF((SELECT 
											CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "WarehouseLocationID":',ISNULL(WL.numWLocationID,0),', "Available":',(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)),', "Location":"',WL.vcLocation,'"}') 
										FROM 
											WareHouseItems WIInner
										LEFT JOIN
											WarehouseLocation WL
										ON
											WIInner.numWLocationID = WL.numWLocationID
										WHERE 
											WIInner.numDomainID=@numDomainID 
											AND WIInner.numItemID=TEMP.numItemCode 
											AND WIInner.numWareHouseID = TEMP.numWareHouseID
											AND (ISNULL(WL.numWLocationID,0) > 0 OR WIInner.numWareHouseItemID=TEMP.numWareHouseItemID)
										ORDER BY
											WL.vcLocation
										FOR XML PATH('')),1,1,''),']')
				ELSE CONCAT('[',STUFF((SELECT 
									CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "WarehouseLocationID":',ISNULL(WL.numWLocationID,0),', "Available":',(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)),', "Location":""}')
								FROM 
									WareHouseItems WIInner
								LEFT JOIN
									WarehouseLocation WL
								ON
									WIInner.numWLocationID = WL.numWLocationID
								WHERE 
									WIInner.numDomainID=@numDomainID 
									AND WIInner.numItemID=TEMP.numItemCode
									AND WIInner.numWareHouseID = TEMP.numWareHouseID
								FOR XML PATH('')),1,1,''),']')
			END)  vcWarehouseLocations
		FROM
			@TEMPItems TEMP
		GROUP BY
			TEMP.numOppId
			,TEMP.numItemCode
			,TEMP.numWareHouseID
			,TEMP.numWareHouseItemID
			,TEMP.vcKitChildItems
			,TEMP.vcKitChildKitItems
			,TEMP.vcPoppName
			,TEMP.vcPathForTImage
			,TEMP.vcItemName
			,TEMP.bitSerial
			,TEMP.bitLot
			,TEMP.vcItemDesc
			,TEMP.vcSKU
			,TEMP.numBarCodeId
			,TEMP.vcWarehouse
			,TEMP.vcLocation
		ORDER BY
			MIN(ID)
	END
	ELSE
	BEGIN
		SELECT
			0 AS numOppId
			,0 AS numOppItemID
			,TEMP.numItemCode
			,TEMP.numWareHouseID
			,TEMP.vcKitChildItems
			,TEMP.vcKitChildKitItems
			,STUFF((SELECT 
						CONCAT(', ',T1.numOppItemID,'(',T1.numRemainingQty,')') 
					FROM 
						@TEMPItems T1 
					WHERE 
						T1.numItemCode = TEMP.numItemCode
						AND T1.numWareHouseID = TEMP.numWareHouseID
						AND ISNULL(T1.vcKitChildItems,'') = ISNULL(TEMP.vcKitChildItems,'')
						AND ISNULL(T1.vcKitChildKitItems,'') = ISNULL(TEMP.vcKitChildKitItems ,'')
					FOR XML PATH('')),1,1,'') vcOppItemIDs
			,'' AS vcPoppName
			,numBarCodeId
			,CONCAT(TEMP.vcWarehouse, (CASE WHEN LEN(ISNULL(TEMP.vcLocation,'')) > 0 THEN CONCAT(' (',TEMP.vcLocation,')') ELSE '' END)) vcLocation
			,vcItemName
			,vcItemDesc
			,bitSerial
			,bitLot
			,vcSKU
			,dbo.fn_GetItemAttributes(@numDomainID,TEMP.numItemCode) vcAttributes
			,vcPathForTImage
			,MIN(vcInclusionDetails) vcInclusionDetails
			,SUM(TEMP.numRemainingQty) numRemainingQty
			,0 numQtyPicked
			,(CASE 
				WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner INNER JOIN WarehouseLocation WL ON WIInner.numWLocationID=WL.numWLocationID WHERE WIInner.numDomainID=@numDomainID AND WIInner.numItemID=TEMP.numItemCode AND WIInner.numWareHouseID = TEMP.numWareHouseID)
				THEN CONCAT('[',STUFF((SELECT 
											CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "WarehouseLocationID":',ISNULL(WL.numWLocationID,0),', "Available":',(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)),', "Location":"',WL.vcLocation,'"}') 
										FROM 
											WareHouseItems WIInner
										LEFT JOIN
											WarehouseLocation WL
										ON
											WIInner.numWLocationID = WL.numWLocationID
										WHERE 
											WIInner.numDomainID=@numDomainID 
											AND WIInner.numItemID=TEMP.numItemCode 
											AND WIInner.numWareHouseID = TEMP.numWareHouseID
											AND (ISNULL(WL.numWLocationID,0) > 0 OR WIInner.numWareHouseItemID=TEMP.numWareHouseItemID)
										ORDER BY
											WL.vcLocation
										FOR XML PATH('')),1,1,''),']')
				ELSE CONCAT('[',STUFF((SELECT 
									CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "WarehouseLocationID":',ISNULL(WL.numWLocationID,0),', "Available":',(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)),', "Location":""}')
								FROM 
									WareHouseItems WIInner
								LEFT JOIN
									WarehouseLocation WL
								ON
									WIInner.numWLocationID = WL.numWLocationID
								WHERE 
									WIInner.numDomainID=@numDomainID 
									AND WIInner.numItemID=TEMP.numItemCode
									AND WIInner.numWareHouseID = TEMP.numWareHouseID
								FOR XML PATH('')),1,1,''),']')
			END)  vcWarehouseLocations
		FROM
			@TEMPItems TEMP
		GROUP BY
			TEMP.numItemCode
			,TEMP.numWareHouseID
			,TEMP.numWareHouseItemID
			,TEMP.vcKitChildItems
			,TEMP.vcKitChildKitItems
			,TEMP.vcPathForTImage
			,TEMP.vcItemName
			,TEMP.vcItemDesc
			,TEMP.bitSerial
			,TEMP.bitLot
			,TEMP.vcSKU
			,TEMP.numBarCodeId
			,TEMP.vcWarehouse
			,TEMP.vcLocation
		ORDER BY
			MIN(ID)
	END
	
	SELECT * FROM @TempFieldsRight ORDER BY intRowNum
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_UpdateItemsPickedQty')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_UpdateItemsPickedQty
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_UpdateItemsPickedQty]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@vcSelectedRecords VARCHAR(MAX)
	,@ClientTimeZoneOffset INT
)
AS
BEGIN
	DECLARE @numStatus NUMERIC(18,0) = 0
	IF EXISTS (SELECT ID FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID)
	BEGIN
		SELECT @numStatus = ISNULL(numOrderStatusPicked,0) FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID
	END

	DECLARE @TempOrders TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numQtyPicked FLOAT
	)

	DECLARE @hDocItem INT

	DECLARE @Temp TABLE
	(
		ID INT IDENTITY(1,1)
		,numPickedQTY FLOAT
		,vcWarehouseItemIDs XML
		,vcOppItems VARCHAR(MAX)
	)

	EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcSelectedRecords

	INSERT INTO @Temp
	(
		numPickedQty,
		vcWarehouseItemIDs,
		vcOppItems
	)
	SELECT
		numPickedQty,
		WarehouseItemIDs,
		vcOppItems
	FROM
		OPENXML (@hDocItem,'/NewDataSet/Table1',2)
	WITH
	(
		numPickedQty FLOAT,
		WarehouseItemIDs xml,
		vcOppItems VARCHAR(MAX)
	)

	EXEC sp_xml_removedocument @hDocItem 

	DECLARE @TEMPItems TABLE
	(
		ID INT
		,numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,bitSerial BIT
		,bitLot BIT
		,numWarehouseItemID NUMERIC(18,0)
		,numQtyLeftToPick FLOAT		
	)

	DECLARE @TEMPWarehouseLocations TABLE
	(
		ID INT
		,numWarehouseItemID NUMERIC(18,0)
		,numPickedQty FLOAT
		,vcSerialLot# VARCHAR(MAX)
	)

	DECLARE @TEMPSerialLot TABLE
	(
		ID INT
		,numWarehouseItemID NUMERIC(18,0)
		,vcSerialNo VARCHAR(300)
		,numQty FLOAT
	)

	DECLARE @TEMPWarehouseDTL TABLE
	(
		ID INT
		,numWareHouseItmsDTLID NUMERIC(18,0)
	)


	DECLARE @i INT = 1
	DECLARE @iCount INT
	DECLARE @j INT = 1
	DECLARE @jCount INT
	DECLARE @x INT = 1
	DECLARE @xCount INT = 0
	DECLARE @y INT = 1
	DECLARE @yCount INT = 0
	DECLARE @numPickedQty FLOAT
	DECLARE @numOppID NUMERIC(18,0)
	DECLARE @numOppItemID NUMERIC(18,0)
	DECLARE @bitSerial BIT
	DECLARE @bitLot BIT
	DECLARE @numWarehouseItemID NUMERIC(18,0)
	DECLARE @numQtyLeftToPick FLOAT
	DECLARE @vcWarehouseItemIDs VARCHAR(4000)
	DECLARE @vcOppItems VARCHAR(MAX)
	DECLARE @numTempWarehouseItemID NUMERIC(18,0)
	DECLARE @numWarehousePickedQty FLOAT
	DECLARE @numFromOnHand FLOAT
	DECLARE @numFromOnOrder FLOAT
	DECLARE @numFromAllocation FLOAT
	DECLARE @numFromBackOrder FLOAT
	DECLARE @numToOnHand FLOAT
	DECLARE @numToOnOrder FLOAT
	DECLARE @numToAllocation FLOAT
	DECLARE @numToBackOrder FLOAT
	DECLARE @vcDescription VARCHAR(300)
	DECLARE @numSerialWarehoueItemID NUMERIC(18,0)
	DECLARE @numMaxID NUMERIC(18,0)
	DECLARE @vcSerialLot# VARCHAR(MAX)
	DECLARE @vcSerialLotNo VARCHAR(200)	
	DECLARE @numSerialLotQty FLOAT
	DECLARE @numSerialLotQtyToPick FLOAT
	DECLARE @numWareHouseItmsDTLID NUMERIC(18,0)

	SET @iCount = (SELECT COUNT(*) FROM @TEMP)

	BEGIN TRY
	BEGIN TRANSACTION

		WHILE @i <= @iCount
		BEGIN
			SELECT
				@numPickedQty=ISNULL(numPickedQty,0)
				,@vcWarehouseItemIDs = CAST(vcWarehouseItemIDs.query('//WarehouseItemIDs/child::*') AS VARCHAR(MAX))
				,@vcOppItems=ISNULL(vcOppItems,'')
			FROM 
				@TEMP 
			WHERE 
				ID=@i

		
			DELETE FROM @TEMPItems
			DELETE FROM @TEMPWarehouseLocations
			DELETE FROM @TEMPSerialLot

			INSERT INTO @TEMPItems
			(
				ID
				,numOppID
				,numOppItemID
				,bitSerial
				,bitLot
				,numWarehouseItemID
				,numQtyLeftToPick
			)
			SELECT 
				ROW_NUMBER() OVER(ORDER BY numOppItemID ASC)
				,OpportunityItems.numOppId
				,TEMP.numOppItemID
				,ISNULL(Item.bitSerialized,0)
				,ISNULL(Item.bitLotNo,0)
				,OpportunityItems.numWarehouseItmsID
				,ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
			FROM
			(
				SELECT
					CAST(SUBSTRING(OutParam,0,CHARINDEX('(',OutParam)) AS NUMERIC) numOppItemID
				FROM
					dbo.SplitString(@vcOppItems,',')
			) TEMP
			INNER JOIN
				OpportunityItems
			ON
				Temp.numOppItemID = OpportunityItems.numoppitemtCode
			INNER JOIN
				Item
			ON
				OpportunityItems.numItemCode = Item.numItemCode

			IF LEN(ISNULL(@vcWarehouseItemIDs,'')) > 0
			BEGIN
				DECLARE @hDocSerialLotItem int
				EXEC sp_xml_preparedocument @hDocSerialLotItem OUTPUT, @vcWarehouseItemIDs

				INSERT INTO @TEMPWarehouseLocations
				(
					ID
					,numWarehouseItemID
					,numPickedQty
					,vcSerialLot#
				)
				SELECT 
					ROW_NUMBER() OVER(ORDER BY ID DESC)
					,ID
					,ISNULL(Qty,0)
					,ISNULL(SerialLotNo,'') 
				FROM 
					OPENXML (@hDocSerialLotItem, '/NewDataSet/Table1',2)
				WITH 
				(
					ID NUMERIC(18,0),
					Qty FLOAT,
					SerialLotNo VARCHAR(MAX)
				)

				EXEC sp_xml_removedocument @hDocSerialLotItem
			END

			IF ISNULL((SELECT TOP 1 bitSerial FROM @TEMPItems),0) = 1 AND (SELECT COUNT(*) FROM @TEMPWarehouseLocations) > 0
			BEGIN
				SET @x = 1
				SELECT @xCount=COUNT(*) FROM @TEMPWarehouseLocations

				WHILE @x <= @xCount
				BEGIN
					SELECT @numSerialWarehoueItemID=numWarehouseItemID,@vcSerialLot#=ISNULL(vcSerialLot#,'') FROM @TEMPWarehouseLocations WHERE ID=@x
					SET @numMaxID = ISNULL((SELECT MAX(ID) FROM @TEMPSerialLot),0)

					IF @vcSerialLot# <> ''
					BEGIN
						INSERT INTO @TEMPSerialLot
						(
							ID
							,numWarehouseItemID
							,vcSerialNo
							,numQty
						)
						SELECT
							@numMaxID + ROW_NUMBER() OVER(ORDER BY OutParam)
							,@numSerialWarehoueItemID
							,RTRIM(LTRIM(OutParam))
							,1
						FROM
							dbo.SplitString(@vcSerialLot#,',')
					END
					

					SET @x = @x + 1
				END
			END
			ELSE IF ISNULL((SELECT TOP 1 bitLot FROM @TEMPItems),0) = 1
			BEGIN
				SET @x = 1
				SELECT @xCount=COUNT(*) FROM @TEMPWarehouseLocations

				WHILE @x <= @xCount
				BEGIN
					SELECT @numSerialWarehoueItemID=numWarehouseItemID,@vcSerialLot#=ISNULL(vcSerialLot#,'') FROM @TEMPWarehouseLocations WHERE ID=@x
					SET @numMaxID = ISNULL((SELECT MAX(ID) FROM @TEMPSerialLot),0)

					IF @vcSerialLot# <> ''
					BEGIN
						INSERT INTO @TEMPSerialLot
						(
							ID
							,numWarehouseItemID
							,vcSerialNo
							,numQty
						)
						SELECT
							@numMaxID + ROW_NUMBER() OVER(ORDER BY OutParam)
							,@numSerialWarehoueItemID
							,(CASE 
								WHEN PATINDEX('%(%', OutParam) > 0 AND PATINDEX('%)%', OutParam) > 0 AND PATINDEX('%)%',OutParam) > (PATINDEX('%(%',OutParam) + 1)
								THEN RTRIM(LTRIM(SUBSTRING(OutParam,0,PATINDEX('%(%', OutParam))))
								ELSE ''
							END)
							,(CASE 
								WHEN PATINDEX('%(%', OutParam) > 0 AND PATINDEX('%)%', OutParam) > 0 AND PATINDEX('%)%',OutParam) > (PATINDEX('%(%',OutParam) + 1)
								THEN (CASE WHEN ISNUMERIC(SUBSTRING(OutParam,(PATINDEX('%(%', OutParam) + 1),(PATINDEX('%)%', OutParam) - PATINDEX('%(%', OutParam) - 1))) = 1 
										THEN CAST(SUBSTRING(OutParam,(PATINDEX('%(%', OutParam) + 1),(PATINDEX('%)%', OutParam) - PATINDEX('%(%', OutParam) - 1)) AS FLOAT)  
										ELSE -1 
										END) 
								ELSE -1
							END)	
						FROM
							dbo.SplitString(@vcSerialLot#,',')
					END
					

					SET @x = @x + 1
				END
			END

			IF EXISTS (SELECT vcSerialNo FROM @TEMPSerialLot WHERE numQty = -1)
			BEGIN
				RAISERROR('INVALID_SERIALLOT_FORMAT',16,1)
				RETURN
			END
		

			SET @j = 1
			SET @jCount = (SELECT COUNT(*) FROM @TEMPItems)

			WHILE @j <= @jCount
			BEGIN
				SELECT 
					@numOppID=numOppID
					,@numOppItemID=numOppItemID
					,@bitSerial=bitSerial
					,@bitLot=bitLot
					,@numWarehouseItemID=numWarehouseItemID
					,@numQtyLeftToPick=numQtyLeftToPick
				FROM
					@TEMPItems
				WHERE
					ID=@j

				UPDATE
					OpportunityItems
				SET 
					numQtyPicked = ISNULL(numQtyPicked,0) + (CASE WHEN @numPickedQty > @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numPickedQty END)
				WHERE 
					numoppitemtCode=@numOppItemID

				IF NOT EXISTS (SELECT ID FROM @TempOrders WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID)
				BEGIN
					INSERT INTO @TempOrders
					(
						numOppID
						,numOppItemID
						,numQtyPicked
					)
					VALUES
					(
						@numOppID
						,@numOppItemID
						,(CASE WHEN @numPickedQty > @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numPickedQty END)
					)
				END
				ELSE
				BEGIN
					UPDATE
						@TempOrders
					SET
						numQtyPicked=ISNULL(numQtyPicked,0) + ISNULL((CASE WHEN @numPickedQty > @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numPickedQty END),0)
					WHERE
						numOppID=@numOppID
						AND numOppItemID=@numOppItemID
				END

				SET @numPickedQty = @numPickedQty - (CASE WHEN @numPickedQty > @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numPickedQty END)


				IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numoppitemtCode=@numOppItemID AND ISNULL(numQtyPicked,0) > ISNULL(numUnitHour,0))
				BEGIN
					RAISERROR('PICKED_QTY_IS_GREATER_THEN_REMAINING_QTY',16,1)
				END

				IF EXISTS (SELECT ID FROM @TEMPWarehouseLocations WHERE numWarehouseItemID=@numWarehouseItemID)
				BEGIN
					SELECT 
						@numTempWarehouseItemID = numWarehouseItemID
						,@numWarehousePickedQty = ISNULL(numPickedQty,0)
					FROM 
						@TEMPWarehouseLocations
					WHERE 
						numWarehouseItemID=@numWarehouseItemID

					UPDATE 
						@TEMPWarehouseLocations
					SET
						numPickedQty = ISNULL(numPickedQty,0) - (CASE WHEN ISNULL(numPickedQty,0) > @numQtyLeftToPick THEN @numQtyLeftToPick ELSE ISNULL(numPickedQty,0) END)
					WHERE
						numWarehouseItemID=@numTempWarehouseItemID

					IF ISNULL(@bitSerial,0)=1 OR ISNULL(@bitLot,0)=1
					BEGIN
						IF  ISNULL((SELECT 
										SUM(ISNULL(TSL.numQty,0))
									FROM 
										WareHouseItmsDTL WID
									INNER JOIN
										@TEMPSerialLot TSL
									ON
										WID.vcSerialNo = WID.vcSerialNo
									WHERE 
										WID.numWareHouseItemID=@numWarehouseItemID
										AND TSL.numWarehouseItemID = @numWarehouseItemID
										AND ISNULL(TSL.numQty,0) > 0
										AND ISNULL(WID.numQty,0) >= ISNULL(TSL.numQty,0)),0) >= (CASE WHEN @numWarehousePickedQty >= @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numWarehousePickedQty END)
										

						BEGIN
							SET @x = 1
							SELECT @xCount=COUNT(*) FROM @TEMPSerialLot
							SET @numSerialLotQtyToPick = (CASE WHEN @numWarehousePickedQty >= @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numWarehousePickedQty END)

							WHILE @x <= @xCount AND @numSerialLotQtyToPick > 0
							BEGIN
								SELECT @vcSerialLotNo=vcSerialNo,@numSerialLotQty=numQty FROM @TEMPSerialLot WHERE ID=@x

								IF ISNULL(@numSerialLotQty,0) > 0
								BEGIN
									IF @bitSerial = 1
									BEGIN
										IF EXISTS (SELECT numWareHouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo=@vcSerialLotNo AND numQty>=@numSerialLotQty)
										BEGIN
											SELECT TOP 1
												@numWareHouseItmsDTLID =numWareHouseItmsDTLID
											FROM
												WareHouseItmsDTL 
											WHERE 
												numWareHouseItemID=@numWarehouseItemID 
												AND vcSerialNo=@vcSerialLotNo 
												AND numQty>=@numSerialLotQty

											INSERT INTO OppWarehouseSerializedItem
											(
												numWarehouseItmsDTLID
												,numWarehouseItmsID
												,numOppID
												,numOppItemID
												,numQty
											)
											SELECT
												numWareHouseItmsDTLID
												,numWareHouseItemID
												,@numOppID
												,@numOppItemID
												,@numSerialLotQty
											FROM 
												WareHouseItmsDTL 
											WHERE 
												numWareHouseItmsDTLID=@numWareHouseItmsDTLID

											SET @numSerialLotQtyToPick = @numSerialLotQtyToPick - @numSerialLotQty
											UPDATE @TEMPSerialLot SET numQty = ISNULL(numQty,0) - @numSerialLotQty  WHERE ID=@x
											UPDATE WareHouseItmsDTL SET numQty = ISNULL(numQty,0) - @numSerialLotQty  WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID
										END
										ELSE
										BEGIN
											RAISERROR('INVLAID_SERIALLOT_QTY',16,1)
											RETURN
										END
									END
									ELSE IF @bitLot = 1
									BEGIN
										IF ISNULL((SELECT SUM(numQty) FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo=@vcSerialLotNo),0) >= @numSerialLotQty
										BEGIN
											DELETE FROM @TEMPWarehouseDTL

											INSERT INTO @TEMPWarehouseDTL
											(
												ID
												,numWareHouseItmsDTLID
											)
											SELECT
												ROW_NUMBER() OVER(ORDER BY numWareHouseItmsDTLID)
												,numWareHouseItmsDTLID
											FROM
												WareHouseItmsDTL 
											WHERE 
												numWareHouseItemID=@numWarehouseItemID 
												AND vcSerialNo=@vcSerialLotNo

											SET @y = 1
											SELECT COUNT(*) FROM @TEMPWarehouseDTL

											WHILE @y <= @yCount AND @numSerialLotQty > 0
											BEGIN
												SELECT @numWareHouseItmsDTLID=numWareHouseItmsDTLID FROM @TEMPWarehouseDTL WHERE ID=@y

												IF ISNULL((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID),0) > @numSerialLotQty
												BEGIN
													INSERT INTO OppWarehouseSerializedItem
													(
														numWarehouseItmsDTLID
														,numWarehouseItmsID
														,numOppID
														,numOppItemID
														,numQty
													)
													SELECT
														numWareHouseItmsDTLID
														,@numWarehouseItemID
														,@numOppID
														,@numOppItemID
														,@numSerialLotQty
													FROM 
														WareHouseItmsDTL 
													WHERE 
														numWareHouseItmsDTLID=@numWareHouseItmsDTLID

													SET @numSerialLotQtyToPick = @numSerialLotQtyToPick - @numSerialLotQty
													SET @numSerialLotQty = 0
													UPDATE @TEMPSerialLot SET numQty = ISNULL(numQty,0) - @numSerialLotQty WHERE ID=@x
													UPDATE WareHouseItmsDTL SET numQty = ISNULL(numQty,0) -  @numSerialLotQty  WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID
												END
												ELSE
												BEGIN
													INSERT INTO OppWarehouseSerializedItem
													(
														numWarehouseItmsDTLID
														,numWarehouseItmsID
														,numOppID
														,numOppItemID
														,numQty
													)
													SELECT
														numWareHouseItmsDTLID
														,@numWarehouseItemID
														,@numOppID
														,@numOppItemID
														,numQty
													FROM 
														WareHouseItmsDTL 
													WHERE 
														numWareHouseItmsDTLID=@numWareHouseItmsDTLID

													SET @numSerialLotQtyToPick = @numSerialLotQtyToPick - ISNULL((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID),0)
													SET @numSerialLotQty = @numSerialLotQty - ISNULL((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID),0)
													UPDATE @TEMPSerialLot SET numQty = ISNULL(numQty,0) - ISNULL((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID),0)  WHERE ID=@x
													UPDATE WareHouseItmsDTL SET numQty = 0  WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID
												END

												SET @y = @y + 1
											END

											IF ISNULL(@numSerialLotQty,0) > 0
											BEGIN
												RAISERROR('INVLAID_SERIALLOT_QTY',16,1)
												RETURN
											END
										ELSE
										BEGIN
											RAISERROR('INVLAID_SERIALLOT_QTY',16,1)
											RETURN
										END
									END
								END
								END

								SET @x = @x + 1
							END

							IF @numSerialLotQtyToPick > 0
							BEGIN
								RAISERROR('SUFFICIENT_SERIALLOT_QTY_NOT_PICKED',16,1)
								RETURN
							END
						END
						ELSE
						BEGIN
							RAISERROR('INVLAID_SERIALLOT_QTY',16,1)
							RETURN
						END

					END

					SET @numQtyLeftToPick = @numQtyLeftToPick - (CASE WHEN @numWarehousePickedQty >= @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numWarehousePickedQty END)
				END
				
				IF @numQtyLeftToPick > 0
				BEGIN
					DECLARE @k INT = 1
					DECLARE @kCount INT 

					SELECT @kCount=COUNT(*) FROM @TEMPWarehouseLocations

					WHILE @k <= @kCount
					BEGIN
						SELECT 
							@numTempWarehouseItemID = numWarehouseItemID
							,@numWarehousePickedQty = ISNULL(numPickedQty,0)
						FROM 
							@TEMPWarehouseLocations 
						WHERE 
							ID = @k

						UPDATE 
							@TEMPWarehouseLocations
						SET
							numPickedQty = ISNULL(numPickedQty,0) - (CASE WHEN ISNULL(numPickedQty,0) > @numQtyLeftToPick THEN @numQtyLeftToPick ELSE ISNULL(numPickedQty,0) END)
						WHERE
							numWarehouseItemID=@numTempWarehouseItemID

						IF ISNULL(@bitSerial,0)=1 OR ISNULL(@bitLot,0)=1
						BEGIN
							IF  ISNULL((SELECT 
											SUM(ISNULL(TSL.numQty,0))
										FROM 
											WareHouseItmsDTL WID
										INNER JOIN
											@TEMPSerialLot TSL
										ON
											WID.vcSerialNo = WID.vcSerialNo
										WHERE 
											WID.numWareHouseItemID=@numTempWarehouseItemID
											AND TSL.numWarehouseItemID = @numTempWarehouseItemID
											AND ISNULL(TSL.numQty,0) > 0
											AND ISNULL(WID.numQty,0) >= ISNULL(TSL.numQty,0)),0) >= (CASE WHEN @numWarehousePickedQty >= @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numWarehousePickedQty END)										
							BEGIN
								SET @x = 1
								SELECT @xCount=COUNT(*) FROM @TEMPSerialLot
								SET @numSerialLotQtyToPick = (CASE WHEN @numWarehousePickedQty >= @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numWarehousePickedQty END)

								WHILE @x <= @xCount AND @numSerialLotQtyToPick > 0
								BEGIN
									SELECT @vcSerialLotNo=vcSerialNo,@numSerialLotQty=numQty FROM @TEMPSerialLot WHERE ID=@x

									IF ISNULL(@numSerialLotQty,0) > 0
									BEGIN
										IF @bitSerial = 1
										BEGIN
											IF EXISTS (SELECT numWareHouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numTempWarehouseItemID AND vcSerialNo=@vcSerialLotNo AND numQty>=@numSerialLotQty)
											BEGIN
												SELECT TOP 1
													@numWareHouseItmsDTLID =numWareHouseItmsDTLID
												FROM
													WareHouseItmsDTL 
												WHERE 
													numWareHouseItemID=@numTempWarehouseItemID 
													AND vcSerialNo=@vcSerialLotNo 
													AND numQty>=@numSerialLotQty

												INSERT INTO OppWarehouseSerializedItem
												(
													numWarehouseItmsDTLID
													,numWarehouseItmsID
													,numOppID
													,numOppItemID
													,numQty
												)
												SELECT
													numWareHouseItmsDTLID
													,@numWarehouseItemID
													,@numOppID
													,@numOppItemID
													,@numSerialLotQty
												FROM 
													WareHouseItmsDTL 
												WHERE 
													numWareHouseItmsDTLID=@numWareHouseItmsDTLID

												SET @numSerialLotQtyToPick = @numSerialLotQtyToPick - @numSerialLotQty
												UPDATE @TEMPSerialLot SET numQty = ISNULL(numQty,0) - @numSerialLotQty  WHERE ID=@x
												UPDATE WareHouseItmsDTL SET numQty = ISNULL(numQty,0) - @numSerialLotQty,numWareHouseItemID=@numWarehouseItemID  WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID
											END
											ELSE
											BEGIN
												RAISERROR('INVLAID_SERIALLOT_QTY',16,1)
												RETURN
											END
										END
										ELSE IF @bitLot = 1
										BEGIN
											IF ISNULL((SELECT SUM(numQty) FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numTempWarehouseItemID AND vcSerialNo=@vcSerialLotNo),0) >= @numSerialLotQty
											BEGIN
												DELETE FROM @TEMPWarehouseDTL

												INSERT INTO @TEMPWarehouseDTL
												(
													ID
													,numWareHouseItmsDTLID
												)
												SELECT
													ROW_NUMBER() OVER(ORDER BY numWareHouseItmsDTLID)
													,numWareHouseItmsDTLID
												FROM
													WareHouseItmsDTL 
												WHERE 
													numWareHouseItemID=@numTempWarehouseItemID 
													AND vcSerialNo=@vcSerialLotNo

												SET @y = 1
												SELECT COUNT(*) FROM @TEMPWarehouseDTL

												WHILE @y <= @yCount AND @numSerialLotQty > 0
												BEGIN
													SELECT @numWareHouseItmsDTLID=numWareHouseItmsDTLID FROM @TEMPWarehouseDTL WHERE ID=@y

													IF ISNULL((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID),0) > @numSerialLotQty
													BEGIN
														INSERT INTO OppWarehouseSerializedItem
														(
															numWarehouseItmsDTLID
															,numWarehouseItmsID
															,numOppID
															,numOppItemID
															,numQty
														)
														SELECT
															numWareHouseItmsDTLID
															,@numWarehouseItemID
															,@numOppID
															,@numOppItemID
															,@numSerialLotQty
														FROM 
															WareHouseItmsDTL 
														WHERE 
															numWareHouseItmsDTLID=@numWareHouseItmsDTLID

														INSERT INTO WareHouseItmsDTL
														(
															numWareHouseItemID
															,vcSerialNo
															,numQty
														)
														VALUES
														(
															@numWarehouseItemID
															,@vcSerialLotNo
															,@numSerialLotQty
														)

														SET @numSerialLotQtyToPick = @numSerialLotQtyToPick - @numSerialLotQty
														SET @numSerialLotQty = 0
														UPDATE @TEMPSerialLot SET numQty = ISNULL(numQty,0) - @numSerialLotQty WHERE ID=@x
														UPDATE WareHouseItmsDTL SET numQty = ISNULL(numQty,0) -  @numSerialLotQty  WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID
													END
													ELSE
													BEGIN
														INSERT INTO OppWarehouseSerializedItem
														(
															numWarehouseItmsDTLID
															,numWarehouseItmsID
															,numOppID
															,numOppItemID
															,numQty
														)
														SELECT
															numWareHouseItmsDTLID
															,@numWarehouseItemID
															,@numOppID
															,@numOppItemID
															,numQty
														FROM 
															WareHouseItmsDTL 
														WHERE 
															numWareHouseItmsDTLID=@numWareHouseItmsDTLID

														SET @numSerialLotQtyToPick = @numSerialLotQtyToPick - ISNULL((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID),0)
														SET @numSerialLotQty = @numSerialLotQty - ISNULL((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID),0)
														UPDATE @TEMPSerialLot SET numQty = ISNULL(numQty,0) - ISNULL((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID),0)  WHERE ID=@x
														UPDATE WareHouseItmsDTL SET numWareHouseItemID=@numWarehouseItemID WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID
													END

													SET @y = @y + 1
												END

												IF ISNULL(@numSerialLotQty,0) > 0
												BEGIN
													RAISERROR('INVLAID_SERIALLOT_QTY',16,1)
													RETURN
												END
											ELSE
											BEGIN
												RAISERROR('INVLAID_SERIALLOT_QTY',16,1)
												RETURN
											END
										END
									END
									END

									SET @x = @x + 1
								END

								IF @numSerialLotQtyToPick > 0
								BEGIN
									RAISERROR('SUFFICIENT_SERIALLOT_QTY_NOT_PICKED',16,1)
									RETURN
								END
							END
							ELSE
							BEGIN
								RAISERROR('INVLAID_SERIALLOT_QTY',16,1)
								RETURN
							END
						END

						IF @numWarehousePickedQty > 0 AND (@numWarehouseItemID <> @numTempWarehouseItemID)
						BEGIN
							SELECT
								@numFromOnHand=ISNULL(numOnHand,0)
								,@numFromOnOrder=ISNULL(numOnOrder,0)
								,@numFromAllocation=ISNULL(numAllocation,0)
								,@numFromBackOrder=ISNULL(numBackOrder,0)
							FROM
								WareHouseItems
							WHERE
								numWareHouseItemID=@numTempWarehouseItemID

							SELECT
								@numToOnHand=ISNULL(numOnHand,0)
								,@numToOnOrder=ISNULL(numOnOrder,0)
								,@numToAllocation=ISNULL(numAllocation,0)
								,@numToBackOrder=ISNULL(numBackOrder,0)
							FROM
								WareHouseItems
							WHERE
								numWareHouseItemID=@numWarehouseItemID

							IF @numWarehousePickedQty >= @numQtyLeftToPick
							BEGIN
								SET @vcDescription = CONCAT('Items picked Qty(:',@numQtyLeftToPick,')') 

								IF (@numFromOnHand+@numFromAllocation) >= @numQtyLeftToPick
								BEGIN
									IF @numFromOnHand >= @numQtyLeftToPick
									BEGIN
										SET @numFromOnHand = @numFromOnHand - @numQtyLeftToPick
									END
									ELSE 
									BEGIN
										SET @numFromOnHand = 0
										SET @numFromAllocation = @numFromAllocation - (@numQtyLeftToPick - @numFromOnHand)
										SET @numFromBackOrder = @numFromBackOrder + (@numQtyLeftToPick - @numFromOnHand)
									END
								END
								ELSE
								BEGIN
									RAISERROR('AVAILABLE_QUANTITY_LESS_THEN_PICKED_QTY',16,1)
								END

								IF @numToBackOrder >= @numQtyLeftToPick
								BEGIN
									SET @numToBackOrder = @numToBackOrder - @numQtyLeftToPick
									SET @numToAllocation = @numToAllocation + @numQtyLeftToPick
								END
								ELSE
								BEGIN
									SET @numToAllocation = @numToAllocation + @numToBackOrder
									SET @numToOnHand = @numToOnHand + (@numQtyLeftToPick - @numToBackOrder)
									SET @numToBackOrder = 0
								END

								SET @numQtyLeftToPick = 0
							END
							ELSE
							BEGIN
								SET @vcDescription = CONCAT('Items picked Qty(:',@numWarehousePickedQty,')') 

								IF (@numFromOnHand+@numFromAllocation) >= @numWarehousePickedQty
								BEGIN
									IF @numFromOnHand >= @numWarehousePickedQty
									BEGIN
										SET @numFromOnHand = @numFromOnHand - @numWarehousePickedQty
									END
									ELSE 
									BEGIN
										SET @numFromOnHand = 0
										SET @numFromAllocation = @numFromAllocation - (@numWarehousePickedQty - @numFromOnHand)
										SET @numFromBackOrder = @numFromBackOrder + (@numWarehousePickedQty - @numFromOnHand)
									END
								END
								ELSE
								BEGIN
									RAISERROR('AVAILABLE_QUANTITY_LESS_THEN_PICKED_QTY',16,1)
								END

								IF @numToBackOrder >= @numWarehousePickedQty
								BEGIN
									SET @numToBackOrder = @numToBackOrder - @numWarehousePickedQty
									SET @numToAllocation = @numToAllocation + @numWarehousePickedQty
								END
								ELSE
								BEGIN
									SET @numToAllocation = @numToAllocation + @numToBackOrder
									SET @numToOnHand = @numToOnHand + (@numWarehousePickedQty - @numToBackOrder)
									SET @numToBackOrder = 0
								END
								

								SET @numQtyLeftToPick = @numQtyLeftToPick - @numWarehousePickedQty
							END

							--UPDATE INVENTORY AND WAREHOUSE TRACKING
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numFromOnHand,
							@numOnAllocation=@numFromAllocation,
							@numOnBackOrder=@numFromBackOrder,
							@numOnOrder=@numFromOnOrder,
							@numWarehouseItemID=@numTempWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@vcDescription 

							SET @vcDescription = REPLACE(@vcDescription,'picked','receieved')
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numToOnHand,
							@numOnAllocation=@numToAllocation,
							@numOnBackOrder=@numToBackOrder,
							@numOnOrder=@numToOnOrder,
							@numWarehouseItemID=@numWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@vcDescription 

						END
						ELSE IF @numWarehousePickedQty > 0
						BEGIN
							IF @numWarehousePickedQty > @numFromAllocation
							BEGIN
								RAISERROR('AVAILABLE_QUANTITY_LESS_THEN_PICKED_QTY',16,1)
							END
							ELSE
							BEGIN
								IF @numWarehousePickedQty >= @numQtyLeftToPick
								BEGIN
									SET @numQtyLeftToPick = 0
								END
								ELSE
								BEGIN
									SET @numQtyLeftToPick = @numQtyLeftToPick - @numWarehousePickedQty
								END
							END
						END

						IF @numQtyLeftToPick <= 0
							BREAK

						SET @k = @k + 1
					END
				END

				IF @numPickedQty <= 0
				BEGIN
					BREAK
				END

				SET @j = @j + 1
			END

			IF @numPickedQty > 0
			BEGIN
				RAISERROR('PICKED_QTY_IS_GREATER_THEN_REMAINING_QTY',16,1)
			END

			SET @i = @i + 1
		END

		IF ISNULL(@numStatus,0) > 0
		BEGIN
			UPDATE
				OpportunityMaster
			SET
				numStatus=@numStatus
			WHERE
				numDomainId=@numDomainID
				AND numOppId IN (SELECT DISTINCT OI.numOppId FROM @TEMPItems T1 INNER JOIN OpportunityItems OI ON T1.numOppItemID = OI.numoppitemtCode)
				AND NOT EXISTS (SELECT
									numOppItemtcode
								FROM
									OpportunityItems
								WHERE
									numOppId = OpportunityMaster.numOppId
									AND ISNULL(numWarehouseItmsID,0) > 0
									AND ISNULL(bitDropShip,0) = 0
									AND ISNULL(numUnitHour,0) <> ISNULL(numQtyPicked,0))
		END


		DECLARE @TempPickList TABLE
		(
			ID INT IDENTITY(1,1)
			,numOppID NUMERIC(18,0)
			,vcItems VARCHAR(MAX)
		)

		INSERT INTO @TempPickList
		(
			numOppID
			,vcItems
		)
		SELECT
			numOppID
			,CONCAT('<NewDataSet>',stuff((
					SELECT 
						CONCAT(' <BizDocItems><OppItemID>',numOppItemID,'</OppItemID>','<Quantity>',ISNULL(numQtyPicked,0),'</Quantity>','<Notes></Notes><monPrice>0</monPrice></BizDocItems>')
					FROM 
						@TempOrders TOR
					WHERE 
						TOR.numOppID = TORMain.numOppID
					FOR XML PATH, TYPE).value(N'.[1]', N'nvarchar(max)'),1,1,''),'</NewDataSet>')
		FROM
			@TempOrders TORMain
		GROUP BY
			numOppID


		SET @k = 1
		SELECT @kCount=COUNT(*) FROM @TempPickList
		DECLARE @numTempOppID NUMERIC(18,0)
		DECLARE @vcItems VARCHAR(MAX)
		DECLARE @intUsedShippingCompany NUMERIC(18,0)
		DECLARE @dtFromDate DATETIME
		DECLARE @numBizDocTempID NUMERIC(18,0)

		SELECT TOP 1
			@numBizDocTempID=numBizDocTempID
		FROM
			BizDocTemplate
		WHERE
			numDomainID=@numDomainID
			AND numBizDocID= 29397
		ORDER BY
			ISNULL(bitEnabled,0) DESC
			,ISNULL(bitDefault,0) DESC

		WHILE @k <= @kCount
		BEGIN
			SELECT @numTempOppID=numOppID,@vcItems=vcItems FROM @TempPickList WHERE ID=@k
			
			IF ISNULL(@vcItems,'') <> ''
			BEGIN
				SELECT 
					@intUsedShippingCompany = ISNULL(intUsedShippingCompany,0)
					,@dtFromDate = DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,GETUTCDATE())
				FROM 
					OpportunityMaster
				WHERE
					numOppId=@numTempOppID

				EXEC USP_CreateBizDocs                                  
					 @numTempOppID,                        
					 29397,                        
					 @numUserCntID,                        
					 0,
					 '',
					 1,
					 @vcItems,
					 @intUsedShippingCompany,
					 '',
					 @dtFromDate,
					 0,
					 0,
					 '',
					 0,
					 0,
					 @ClientTimeZoneOffset,
					 0,
					 0,
					 @numBizDocTempID,
					 '',
					 '',
					 0, --(joseph) Added to Get Online Marketplace Sales Tax Percentage
					 0,
					 1,
					 0,
					 0,
					 0,
					 0,
					 NULL,
					 NULL,
					 0,
					 '',
					 0,
					 0,
					 0,
					 0,
					 0,
					 NULL,
					 0
			END

			SET @k = @k + 1
		END
	COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorNumber INT
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT
		DECLARE @ErrorLine INT
		DECLARE @ErrorProcedure NVARCHAR(200)

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorNumber = ERROR_NUMBER(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@ErrorLine = ERROR_LINE(),
			@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

		RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
	END CATCH
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillmentConfiguration_GetByUser')
DROP PROCEDURE dbo.USP_MassSalesFulfillmentConfiguration_GetByUser
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillmentConfiguration_GetByUser]
(
	@numDomainID NUMERIC(18,0),
    @numUserCntID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT * FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillmentConfiguration_Save')
DROP PROCEDURE dbo.USP_MassSalesFulfillmentConfiguration_Save
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillmentConfiguration_Save]
(
	@numDomainID NUMERIC(18,0)
    ,@numUserCntID NUMERIC(18,0)
	,@bitGroupByOrderForPick BIT
	,@bitGroupByOrderForShip BIT
	,@bitGroupByOrderForInvoice BIT
	,@bitGroupByOrderForPay BIT
	,@bitGroupByOrderForClose BIT
	,@tintInvoicingType TINYINT
	,@tintScanValue TINYINT
	,@tintPackingMode TINYINT
	,@tintPendingCloseFilter TINYINT
	,@bitGeneratePickListByOrder BIT
	,@numOrderStatusPicked NUMERIC(18,0)
	,@numOrderStatusPacked1 NUMERIC(18,0)
	,@numOrderStatusPacked2 NUMERIC(18,0)
	,@numOrderStatusPacked3 NUMERIC(18,0)
	,@numOrderStatusPacked4 NUMERIC(18,0)
	,@numOrderStatusInvoiced NUMERIC(18,0)
	,@numOrderStatusPaid NUMERIC(18,0)
	,@numOrderStatusClosed NUMERIC(18,0)
	,@bitPickWithoutInventoryCheck BIT
	,@bitEnablePickListMapping BIT
	,@bitEnableFulfillmentBizDocMapping BIT
)
AS 
BEGIN
	IF EXISTS (SELECT ID FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID)
	BEGIN
		UPDATE
			MassSalesFulfillmentConfiguration
		SET
			bitGroupByOrderForPick=@bitGroupByOrderForPick
			,bitGroupByOrderForShip=@bitGroupByOrderForShip
			,bitGroupByOrderForInvoice=@bitGroupByOrderForInvoice
			,bitGroupByOrderForPay=@bitGroupByOrderForPay
			,bitGroupByOrderForClose=@bitGroupByOrderForClose
			,tintInvoicingType=@tintInvoicingType
			,tintScanValue=@tintScanValue
			,tintPackingMode=@tintPackingMode
			,tintPendingCloseFilter=@tintPendingCloseFilter
			,bitGeneratePickListByOrder=@bitGeneratePickListByOrder
			,numOrderStatusPicked=@numOrderStatusPicked
			,numOrderStatusPacked1=@numOrderStatusPacked1
			,numOrderStatusPacked2=@numOrderStatusPacked2
			,numOrderStatusPacked3=@numOrderStatusPacked3
			,numOrderStatusPacked4=@numOrderStatusPacked4
			,numOrderStatusInvoiced=@numOrderStatusInvoiced
			,numOrderStatusPaid=@numOrderStatusPaid
			,numOrderStatusClosed=@numOrderStatusClosed
			,bitPickWithoutInventoryCheck=@bitPickWithoutInventoryCheck
			,bitEnablePickListMapping=@bitEnablePickListMapping
			,bitEnableFulfillmentBizDocMapping=@bitEnableFulfillmentBizDocMapping
		WHERE
			numDomainID=@numDomainID 
	END
	ELSE
	BEGIN
		INSERT INTO MassSalesFulfillmentConfiguration
		(
			numDomainID 
			,bitGroupByOrderForPick
			,bitGroupByOrderForShip
			,bitGroupByOrderForInvoice
			,bitGroupByOrderForPay
			,bitGroupByOrderForClose
			,tintInvoicingType
			,tintScanValue
			,tintPackingMode
			,tintPendingCloseFilter
			,bitGeneratePickListByOrder
			,numOrderStatusPicked
			,numOrderStatusPacked1
			,numOrderStatusPacked2
			,numOrderStatusPacked3
			,numOrderStatusPacked4
			,numOrderStatusInvoiced
			,numOrderStatusPaid
			,numOrderStatusClosed
			,bitPickWithoutInventoryCheck
			,bitEnablePickListMapping
			,bitEnableFulfillmentBizDocMapping
		)
		VALUES
		(
			@numDomainID 
			,@bitGroupByOrderForPick
			,@bitGroupByOrderForShip
			,@bitGroupByOrderForInvoice
			,@bitGroupByOrderForPay
			,@bitGroupByOrderForClose
			,@tintInvoicingType
			,@tintScanValue
			,@tintPackingMode
			,@tintPendingCloseFilter
			,@bitGeneratePickListByOrder
			,@numOrderStatusPicked
			,@numOrderStatusPacked1
			,@numOrderStatusPacked2
			,@numOrderStatusPacked3
			,@numOrderStatusPacked4
			,@numOrderStatusInvoiced
			,@numOrderStatusPaid
			,@numOrderStatusClosed
			,@bitPickWithoutInventoryCheck
			,@bitEnablePickListMapping
			,@bitEnableFulfillmentBizDocMapping
		)
	END
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocItems_ChangePackingSlipQty')
DROP PROCEDURE USP_OpportunityBizDocItems_ChangePackingSlipQty
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocItems_ChangePackingSlipQty]                        
(              
	@numDomainID NUMERIC(18,0)                                
	,@numUserCntID NUMERIC(18,0)
	,@numOppId NUMERIC(18,0)
	,@numOppBizDocsId NUMERIC(18,0)
)
AS 
BEGIN
	-- CODE LEVEL TRANSACTION
	IF EXISTS (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppId=@numOppId AND numBizDocId=296 AND numSourceBizDocId=@numOppBizDocsId)
	BEGIN
		DECLARE @TEMP TABLE
		(
			numOppItemID NUMERIC(18,0)
			,numOldUnitHour FLOAT
			,numNewUnitHour FLOAT
		)

		INSERT INTO @TEMP
		(
			numOppItemID
			,numOldUnitHour
			,numNewUnitHour
		)
		SELECT
			OBDI.numOppItemID
			,OBDI.numUnitHour
			,SUM(OBDIFulfillment.numUnitHour)
		FROM 
			OpportunityBizDocs OBD
		INNER JOIN
			OpportunityBizDocItems OBDI
		ON
			OBDI.numOppBizDocID=OBD.numOppBizDocsId
		INNER JOIN 
			OpportunityBizDocs OBDFulfillment 
		ON  
			OBDFulfillment.numSourceBizDocId = OBD.numOppBizDocsId
		INNER JOIN 
			OpportunityBizDocItems OBDIFulfillment 
		ON  
			OBDIFulfillment.numOppBizDocID = OBDFulfillment.numOppBizDocsId
			AND OBDIFulfillment.numOppItemID = OBDI.numOppItemID
		WHERE 
			OBD.numOppId=@numOppId
			AND OBD.numOppBizDocsId=@numOppBizDocsId
		GROUP BY
			OBDI.numOppItemID
			,OBDI.numUnitHour

		IF EXISTS (SELECT 
					OBDI.numOppItemID
				FROM 
					OpportunityBizDocs OBD
				INNER JOIN
					OpportunityBizDocItems OBDI
				ON
					OBDI.numOppBizDocID=OBD.numOppBizDocsId
				INNER JOIN 
					OpportunityBizDocs OBDFulfillment 
				ON  
					OBDFulfillment.numSourceBizDocId = OBD.numOppBizDocsId
				INNER JOIN 
					OpportunityBizDocItems OBDIFulfillment 
				ON  
					OBDIFulfillment.numOppBizDocID = OBDFulfillment.numOppBizDocsId
					AND OBDIFulfillment.numOppItemID = OBDI.numOppItemID
				WHERE 
					OBD.numOppId=@numOppId
					AND OBD.numOppBizDocsId=@numOppBizDocsId
				GROUP BY
					OBDI.numOppItemID
					,OBDI.numUnitHour
				HAVING
					OBDI.numUnitHour > ISNULL(SUM(OBDIFulfillment.numUnitHour),0))
		BEGIN
			DECLARE @tintCommitAllocation AS TINYINT
			DECLARE @bitAllocateInventoryOnPickList AS BIT

			SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
			SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM OpportunityMaster INNER JOIN DivisionMaster ON OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID WHERE numOppID=@numOppID

			IF @numOppBizDocsId > 0 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
			BEGIN
				-- Revert Allocation
				EXEC USP_RevertAllocationPickList @numDomainID,@numOppID,@numOppBizDocsId,@numUserCntID
			END

			UPDATE 
				OpportunityBizDocs
			SET    
				numModifiedBy = @numUserCntID,
				dtModifiedDate = GETUTCDATE()
			WHERE  
				numOppBizDocsId = @numOppBizDocsId

			IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 )
			BEGIN
				EXEC 
				USP_ManageOpportunityAutomationQueue			
				@numOppQueueID = 0, -- numeric(18, 0)							
				@numDomainID = @numDomainID, -- numeric(18, 0)							
				@numOppId = @numOppID, -- numeric(18, 0)							
				@numOppBizDocsId = @numOppBizDocsId, -- numeric(18, 0)							
				@numOrderStatus = 0, -- numeric(18, 0)					
				@numUserCntID = @numUserCntID, -- numeric(18, 0)					
				@tintProcessStatus = 1, -- tinyint						
				@tintMode = 1 -- TINYINT
			END 

			UPDATE 
				OBI
			SET 
				OBI.numUnitHour = T1.numNewUnitHour
				,OBI.monPrice = OI.monPrice
				,OBI.monTotAmount = (OI.monTotAmount/OI.numUnitHour) * T1.numNewUnitHour
				,OBI.fltDiscount = (CASE WHEN OI.bitDiscountType=0 THEN OI.fltDiscount WHEN OI.bitDiscountType=1 THEN (OI.fltDiscount/OI.numUnitHour) * T1.numNewUnitHour ELSE OI.fltDiscount END)
				,OBI.monTotAmtBefDiscount = (OI.monTotAmtBefDiscount/OI.numUnitHour) * T1.numNewUnitHour 
			FROM 
				OpportunityBizDocItems OBI
			JOIN 
				OpportunityItems OI
			ON 
				OBI.numOppItemID=OI.numoppitemtCode
			INNER JOIN
				@TEMP T1
			ON
				OBI.numOppItemID = T1.numOppItemID
			WHERE 
				OI.numOppId=@numOppId 
				AND OBI.numOppBizDocID=@numOppBizDocsId
				AND OBI.numUnitHour > T1.numNewUnitHour

			DECLARE @monDealAmount as DECIMAL(20,5)
			SELECT @monDealAmount=isnull(dbo.[GetDealAmount](@numOppId,GETUTCDATE(),@numOppBizDocsId),0)

			UPDATE 
				OpportunityBizDocs 
			SET 
				monDealAmount=@monDealAmount
			WHERE  
				numOppBizDocsId = @numOppBizDocsId

			--IF @numOppBizDocsId > 0 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
			--BEGIN
			--	EXEC USP_PickListManageSOWorkOrder @numDomainID,@numUserCntID,@numOppID,@numOppBizDocsId
			--	-- Allocate Inventory
			--	EXEC USP_ManageInventoryPickList @numDomainID,@numOppID,@numOppBizDocsId,@numUserCntID
			--END
		END

		UPDATE 
			OBI
		SET 
			OBI.numUnitHour = 0
			,OBI.monPrice = OI.monPrice
			,OBI.monTotAmount = 0
			,OBI.fltDiscount = 0
			,OBI.monTotAmtBefDiscount = 0 
		FROM 
			OpportunityBizDocItems OBI
		JOIN 
			OpportunityItems OI
		ON 
			OBI.numOppItemID=OI.numoppitemtCode
		WHERE 
			OI.numOppId=@numOppId 
			AND OBI.numOppBizDocID=@numOppBizDocsId
			AND OBI.numOppItemID NOT IN (SELECT OBIInner.numOppItemID FROM OpportunityBizDocs OB INNER JOIN OpportunityBizDocItems OBIInner ON OB.numOppBizDocsId=OBIInner.numOppBizDocID WHERE OB.numOppId=@numOppId AND OB.numSourceBizDocId=@numOppBizDocsId)
			AND OBI.numOppItemID NOT IN (SELECT numOppItemID FROM @TEMP)

		UPDATE
			OI
		SET
			numQtyPicked = ISNULL((SELECT
										SUM(numUnitHour) 
									FROM 
										OpportunityBizDocs OB 
									INNER JOIN 
										OpportunityBizDocItems OBI 
									ON 
										OB.numOppBizDocsId=OBI.numOppBizDocID 
									WHERE 
										OB.numOppID=@numOppID 
										AND OB.numBizDocId=29397
										AND OBI.numOppItemID=OI.numOppItemtCode),0)
		FROM
			OpportunityItems OI
		WHERE
			numOppId=@numOppId
	END
END
GO

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetPOForMerge')
DROP PROCEDURE dbo.USP_OpportunityMaster_GetPOForMerge
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetPOForMerge]
(
	@numDomainID NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@bitOnlyMergable BIT
	,@bitExcludePOSendToVendor BIT
	,@tintCostType TINYINT
	,@ClientTimeZoneOffset INT
)
AS 
BEGIN
	DECLARE @TEMP TABLE
	(
		numDivisionID NUMERIC(18,0)
		,numOppId NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,vcCompanyName VARCHAR(300)
		,vcNavigationURL VARCHAR(300)
		,vcPOppName VARCHAR(300)
		,vcCreatedBy VARCHAR(300)
		,bintCreatedDate VARCHAR(50)
		,monDealAmount DECIMAL(20,5)
		,vcItemName VARCHAR(300)
		,vcSKU VARCHAR(100)
		,fltWeight FLOAT
		,numoppitemtCode NUMERIC(18,0)
		,numUnitHour FLOAT
		,monPrice DECIMAL(20,5)
		,vcItemCostSaving VARCHAR(MAX)
	)

	INSERT INTO @TEMP
	(
		numDivisionID
		,numOppId
		,numItemCode
		,vcCompanyName
		,vcNavigationURL
		,vcPOppName
		,vcCreatedBy
		,bintCreatedDate
		,monDealAmount
		,vcItemName
		,vcSKU
		,fltWeight
		,numoppitemtCode
		,numUnitHour
		,monPrice
		,vcItemCostSaving
	)
	SELECT
		DivisionMaster.numDivisionID
		,OpportunityMaster.numOppId
		,Item.numItemCode
		,CompanyInfo.vcCompanyName
		,(CASE DivisionMaster.tintCRMType 
			WHEN 2 THEN CONCAT('../account/frmAccounts.aspx?DivID=',DivisionMaster.numDivisionID) 
			WHEN 1 THEN CONCAT('../prospects/frmProspects.aspx?DivID=',DivisionMaster.numDivisionID) 
			ELSE CONCAT('../Leads/frmLeads.aspx?DivID=',DivisionMaster.numDivisionID) 
		END)
		,OpportunityMaster.vcPOppName
		,dbo.fn_GetContactName(OpportunityMaster.numCreatedBy) vcCreatedBy
		,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate),@numDomainID) bintCreatedDate
		,OpportunityMaster.monDealAmount
		,Item.vcItemName
		,Item.vcSKU
		,ISNULL(Item.fltWeight,0)
		,OpportunityItems.numoppitemtCode
		,ISNULL(OpportunityItems.numUnitHour,0)
		,ISNULL(OpportunityItems.monPrice,0)
		,(CASE 
			WHEN @tintCostType = 2 
			THEN ISNULL(STUFF((SELECT 
					CONCAT(', <input type="button" class="btn btn-xs btn-success" value="$" onClick="return RecordSelected(this,',OM.numDivisionId,',',ISNULL(OI.monPrice,0),',',ISNULL(V.intMinQty,0),')" /> <b>$'
					,FORMAT((ISNULL(OpportunityItems.numUnitHour,0) * ISNULL(OI.monPrice,0)) - (ISNULL(OpportunityItems.numUnitHour,0) * ISNULL(OpportunityItems.monPrice,0)), '#,##0.#####')
					,'</b> (',ISNULL(OI.numUnitHour,0),' U) '
					,CONCAT('<span style="color:',(CASE
								WHEN (DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()) = 0 AND DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()) <= 90)
								THEN '#00b050'
								WHEN (DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()) = 91 AND DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()) <= 180)
								THEN '#ff9933'
								WHEN (DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()) = 181 AND DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()) <= 270)
								THEN '#9148c8'
								ELSE '#ff0000'
							END),'">',DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()),'d</span> ')
						,'<b><a target="_blank" href="../opportunity/frmOpportunities.aspx?OpID=',OM.numOppId,'">',OM.vcPOppName,'</a></b>'
						,' (',CI.vcCompanyName,')') 
				FROM 
					OpportunityMaster OM 
				INNER JOIN
					DivisionMaster DM
				ON
					OM.numDivisionId = DM.numDivisionID
				INNER JOIN
					CompanyInfo CI
				ON
					DM.numCompanyID = CI.numCompanyId
				INNER JOIN 
					OpportunityItems OI 
				ON 
					OM.numOppID=OI.numOppID 
				LEFT JOIN
					Vendor V
				ON
					V.numVendorID = DM.numDivisionID
					AND V.numItemCode = OI.numItemCode
				WHERE 
					OM.numDomainId=@numDomainID 
					AND OM.numOppId <> OpportunityMaster.numOppId
					AND OM.tintOppType = 2 
					AND OM.tintOppStatus = 1 
					AND ISNULL(OM.bitStockTransfer,0) = 0
					AND OI.numItemCode = Item.numItemCode
					--AND ISNULL(OI.monPrice,0) < ISNULL(OpportunityItems.monPrice,0)
					AND DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()) <= 600
				ORDER BY
					(ISNULL(OpportunityItems.numUnitHour,0) * ISNULL(OI.monPrice,0)) - (ISNULL(OpportunityItems.numUnitHour,0) * ISNULL(OpportunityItems.monPrice,0)) ASC OFFSET 0 ROWS FETCH NEXT 5 ROWS ONLY
				FOR XML PATH(''), TYPE).value('.', 'varchar(max)'),1,1,''),'') 
			ELSE ''
		END) vcItemCostSaving
	FROM
		OpportunityMaster
	INNER JOIN
		DivisionMaster
	ON
		OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	INNER JOIN
		OpportunityItems
	ON
		OpportunityMaster.numOppId = OpportunityItems.numOppId
	INNER JOIN
		Item
	ON
		OpportunityItems.numItemCode = Item.numItemCode
	INNER JOIN
		WareHouseItems
	ON
		OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
	WHERE
		OpportunityMaster.numDomainId=@numDomainID
		AND OpportunityMaster.tintOppType = 2
		AND OpportunityMaster.tintOppStatus = 1
		AND ISNULL(OpportunityMaster.bitStockTransfer,0) = 0
		AND ISNULL(OpportunityMaster.tintshipped,0) = 0
		AND (SELECT COUNT(*) FROM OpportunityBizDocs WHERE OpportunityBizDocs.numOppId=OpportunityMaster.numOppId) = 0
		AND WareHouseItems.numWareHouseID=@numWarehouseID
		AND ISNULL(OpportunityItems.numUnitHourReceived,0) = 0
		AND 1 = (CASE 
					WHEN @tintCostType = 2 
					THEN (CASE WHEN EXISTS (SELECT 
												OI.numoppitemtCode
											FROM 
												OpportunityMaster OM 
											INNER JOIN
												DivisionMaster DM
											ON
												OM.numDivisionId = DM.numDivisionID
											INNER JOIN
												CompanyInfo CI
											ON
												DM.numCompanyID = CI.numCompanyId
											INNER JOIN 
												OpportunityItems OI 
											ON 
												OM.numOppID=OI.numOppID 
											WHERE 
												OM.numDomainId=@numDomainID 
												AND OM.numOppId <> OpportunityMaster.numOppId
												AND OM.tintOppType = 2 
												AND OM.tintOppStatus = 1 
												AND ISNULL(OM.bitStockTransfer,0) = 0
												AND OI.numItemCode = Item.numItemCode
												--AND ISNULL(OI.monPrice,0) < ISNULL(OpportunityItems.monPrice,0)
												AND DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()) <= 600) THEN 1 ELSE 0 END)
					ELSE 1 
				END)
	
	IF ISNULL(@bitOnlyMergable,0) = 1
	BEGIN
		DELETE FROM @TEMP WHERE numDivisionID IN (SELECT numDivisionID FROM (SELECT numDivisionID,numOppId FROM @TEMP GROUP BY numDivisionID,numOppId) TEMP GROUP BY numDivisionID HAVING COUNT(*) < 2)
	END

	SELECT 
		numDivisionID
		,vcCompanyName
		,vcNavigationURL
		,CONCAT('[',STUFF((SELECT 
					CONCAT(', {"intType":"',PurchaseIncentives.intType ,'", "vcIncentives":"',PurchaseIncentives.vcIncentives,'", "vcbuyingqty":"',PurchaseIncentives.vcbuyingqty,'", "tintDiscountType":"',ISNULL(PurchaseIncentives.tintDiscountType,0),'"}') 
				FROM 
					PurchaseIncentives 
				WHERE
					PurchaseIncentives.numDivisionID = T.numDivisionID
				FOR XML PATH('')),1,1,''),']') AS vcPurchaseIncentive 
	FROM 
		@TEMP T 
	GROUP BY 
		numDivisionID,vcCompanyName,vcNavigationURL
	ORDER BY
		T.vcCompanyName

	SELECT
		numDivisionID
		,numOppId
		,vcPOppName
		,vcCreatedBy
		,bintCreatedDate
		,monDealAmount
		,(CASE WHEN EXISTS (SELECT numCorrespondenceID FROM Correspondence WHERE numOpenRecordID=T.numOppId AND tintCorrType IN (2,4)) THEN 1 ELSE 0 END) AS bitPOSendToVendor
		,ISNULL((SELECT TOP 1
					(CASE 
						WHEN EmailHistory.numEmailHstrID IS NOT NULL 
						THEN CONCAT('<i></i>',dbo.FormatedDateFromDate(DateAdd(minute,-@ClientTimeZoneOffset,EmailHistory.dtReceivedOn),@numDomainID))
						ELSE dbo.FormatedDateFromDate(DateAdd(minute,-@ClientTimeZoneOffset, dtCreatedDate),@numDomainID)
					END) 
				FROM 
					Correspondence 
				LEFT JOIN 
					EmailHistory 
				ON 
					Correspondence.numEmailHistoryID=EmailHistory.numEmailHstrID 
				LEFT JOIN 
					Communication
				ON
					Correspondence.numCommID = Communication.numCommId
				WHERE 
					numOpenRecordID=T.numOppId
					AND tintCorrType IN (2,4)),'') vcPOSend
	FROM
		@TEMP T
	GROUP BY
		numDivisionID,numOppId,vcPOppName,vcCreatedBy,bintCreatedDate,monDealAmount
	ORDER BY
		numOppId

	SELECT
		numOppId
		,numoppitemtCode
		,numItemCode
		,vcItemName
		,vcSKU
		,fltWeight
		,numUnitHour
		,monPrice
		,vcItemCostSaving
	FROM 
		@TEMP T
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_MergePurchaseOrders')
DROP PROCEDURE USP_OpportunityMaster_MergePurchaseOrders
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_MergePurchaseOrders]  
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numMasterPOID NUMERIC(18,0),
	@vcItemsToMerge VARCHAR(MAX)
AS  
BEGIN
	IF EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numMasterPOID)
	BEGIN
		DECLARE @hDocItem INT

		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1)
			,numOppID NUMERIC(18,0)
			,numOppItemID NUMERIC(18,0)
			,numUnitHour FLOAT
			,monPrice DECIMAL(20,5)
		)

		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcItemsToMerge

		INSERT INTO @TEMP
		(
			numOppID
			,numOppItemID
			,numUnitHour
			,monPrice
		)
		SELECT
			ISNULL(OppID,0)
			,ISNULL(OppItemID,0)
			,ISNULL(Units,0)
			,ISNULL(UnitCost,0)
		FROM
			OPENXML (@hDocItem,'/NewDataSet/Table1',2)
		WITH
		(
			OppID  NUMERIC(18,0)
			,OppItemID NUMERIC(18,0)
			,Units FLOAT
			,UnitCost DECIMAL(20,5)
		)

		EXEC sp_xml_removedocument @hDocItem

		IF EXISTS (SELECT OB.numOppBizDocsId FROM OpportunityBizDocs OB INNER JOIN @TEMP T ON OB.numOppId = T.numOppID)
		BEGIN
			RAISERROR('BIZ_DOC_EXISTS',16,1)
		END
		ELSE
		BEGIN
			BEGIN TRY
			BEGIN TRANSACTION                                                                  
				DECLARE @numContactId NUMERIC(18,0)=null                                                                        
				DECLARE @numDivisionId numeric(9)=null                                                                          
				DECLARE @tintSource numeric(9)=null                                                                          
				DECLARE @vcPOppName Varchar(100)=''                                                                 
				DECLARE @Comments varchar(1000)=''                                                                          
				DECLARE @bitPublicFlag bit=0                                                                                                                                                         
				DECLARE @monPAmount DECIMAL(20,5) =0                                                 
				DECLARE @numAssignedTo as numeric(9)=0                                                                                                                                                                                                                                                                                         
				DECLARE @strItems as VARCHAR(MAX)=null                                                                          
				DECLARE @strMilestone as VARCHAR(100)=null                                                                          
				DECLARE @dtEstimatedCloseDate datetime                                                                          
				DECLARE @CampaignID as numeric(9)=null                                                                          
				DECLARE @lngPConclAnalysis as numeric(9)=null                                                                         
				DECLARE @tintOppType as tinyint                                                                                                                                             
				DECLARE @tintActive as tinyint                                                              
				DECLARE @numSalesOrPurType as numeric(9)              
				DECLARE @numRecurringId as numeric(9)
				DECLARE @numCurrencyID as numeric(9)=0
				DECLARE @DealStatus as tinyint =0
				DECLARE @numStatus AS NUMERIC(9)
				DECLARE @vcOppRefOrderNo VARCHAR(100)
				DECLARE @numBillAddressId numeric(9)=0
				DECLARE @numShipAddressId numeric(9)=0
				DECLARE @bitStockTransfer BIT=0
				DECLARE @WebApiId INT = 0
				DECLARE @tintSourceType TINYINT=0
				DECLARE @bitDiscountType as bit
				DECLARE @fltDiscount  as float
				DECLARE @bitBillingTerms as bit
				DECLARE @intBillingDays as integer
				DECLARE @bitInterestType as bit
				DECLARE @fltInterest as float
				DECLARE @tintTaxOperator AS TINYINT
				DECLARE @numDiscountAcntType AS NUMERIC(9)
				DECLARE @vcWebApiOrderNo VARCHAR(100)=NULL
				DECLARE @vcCouponCode		VARCHAR(100) = ''
				DECLARE @vcMarketplaceOrderID VARCHAR(100)=NULL
				DECLARE @vcMarketplaceOrderDate  datetime=NULL
				DECLARE @vcMarketplaceOrderReportId VARCHAR(100)=NULL
				DECLARE @numPercentageComplete NUMERIC(9)
				DECLARE @bitUseShippersAccountNo BIT = 0
				DECLARE @bitUseMarkupShippingRate BIT = 0
				DECLARE @numMarkupShippingRate NUMERIC(19,2) = 0
				DECLARE @intUsedShippingCompany INT = 0
				DECLARE @numShipmentMethod NUMERIC(18,0)=0
				DECLARE @dtReleaseDate DATE = NULL
				DECLARE @numPartner NUMERIC(18,0)=0
				DECLARE @tintClickBtn INT=0
				DECLARE @numPartenerContactId NUMERIC(18,0)=0
				DECLARE @numAccountClass NUMERIC(18,0) = 0
				DECLARE @numWillCallWarehouseID NUMERIC(18,0) = 0
				DECLARE @numVendorAddressID NUMERIC(18,0) = 0

				--GET MASTER PO FIELDS 
				SELECT
					@numContactId =numContactId,@numDivisionId=numDivisionId,@tintSource=tintSource,@vcPOppName=vcPOppName,@Comments=txtComments,
					@bitPublicFlag=bitPublicFlag,@monPAmount=monPAmount,@numAssignedTo=numAssignedTo,@strMilestone=null,@dtEstimatedCloseDate=intPEstimatedCloseDate,
					@CampaignID=numCampainID,@lngPConclAnalysis=lngPConclAnalysis,@tintOppType=tintOppType,@tintActive=tintActive,@numSalesOrPurType=numSalesOrPurType,
					@numRecurringId=null,@numCurrencyID=numCurrencyID,@DealStatus=tintOppStatus,@numStatus=numStatus,@vcOppRefOrderNo=vcOppRefOrderNo,@numBillAddressId=0,
					@numShipAddressId=0,@bitStockTransfer=0,@WebApiId=0,@tintSourceType=tintSourceType,@bitDiscountType=bitDiscountType,@fltDiscount=fltDiscount,
					@bitBillingTerms=bitBillingTerms,@intBillingDays=intBillingDays,@bitInterestType=bitInterestType,@fltInterest=fltInterest,@tintTaxOperator=tintTaxOperator,
					@numDiscountAcntType=numDiscountAcntType,@vcWebApiOrderNo=vcWebApiOrderNo,@vcCouponCode=vcCouponCode,@vcMarketplaceOrderID=vcMarketplaceOrderID,
					@vcMarketplaceOrderDate=bintCreatedDate,@vcMarketplaceOrderReportId=vcMarketplaceOrderReportId,@numPercentageComplete= numPercentageComplete,
					@bitUseShippersAccountNo=bitUseShippersAccountNo,@bitUseMarkupShippingRate=bitUseMarkupShippingRate,@numMarkupShippingRate=numMarkupShippingRate,
					@intUsedShippingCompany= intUsedShippingCompany,@numShipmentMethod=numShipmentMethod,@dtReleaseDate=dtReleaseDate,@numPartner=numPartner,@numPartenerContactId=numPartenerContact
					,@numAccountClass=numAccountClass,@numVendorAddressID=numVendorAddressID
				FROM
					OpportunityMaster
				WHERE
					numDomainId = @numDomainID
					AND numOppId = @numMasterPOID 

				DECLARE @TEMPMasterItems TABLE
				(
					numoppitemtCode NUMERIC(18,0),numItemCode NUMERIC(18,0),numUnitHour FLOAT,monPrice DECIMAL(20,5),
					monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000),numWarehouseItmsID NUMERIC(18,0),numToWarehouseItemID NUMERIC(18,0),
					Op_Flag INT, ItemType VARCHAR(50),DropShip BIT,bitDiscountType BIT, fltDiscount FLOAT, monTotAmtBefDiscount DECIMAL(20,5),
					vcItemName VARCHAR(300),numUOM NUMERIC(18,0), bitWorkOrder BIT,numVendorWareHouse NUMERIC(18,0),
					numShipmentMethod NUMERIC(18,0),numSOVendorId NUMERIC(18,0),numProjectID NUMERIC(18,0),
					numProjectStageID NUMERIC(18,0),Attributes VARCHAR(200),bitItemPriceApprovalRequired BIT
				)

				--GET ITEMS OF MASTER PO
				INSERT INTO 
					@TEMPMasterItems
				SELECT 
					numoppitemtCode,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,numToWarehouseItemID,
					2,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,numUOMId,bitWorkOrder,numVendorWareHouse,
					numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,vcAttributes,bitItemPriceApprovalRequired
				FROM 
					OpportunityItems
				WHERE 
					numOppId = @numMasterPOID

				--GET ITEMS OF PO NEEDS TO BE MERGED TO MASTER PO
				INSERT INTO 
					@TEMPMasterItems
				SELECT 
					0,numItemCode,T.numUnitHour,T.monPrice,(T.numUnitHour * T.monPrice),vcItemDesc,numWarehouseItmsID,numToWarehouseItemID,
					1,vcType,bitDropShip,bitDiscountType,0,(T.numUnitHour * T.monPrice),vcItemName,numUOMId,bitWorkOrder,numVendorWareHouse,
					numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,vcAttributes,bitItemPriceApprovalRequired
				FROM 
					@TEMP T
				INNER JOIN
					OpportunityItems OI
				ON
					T.numOppItemID = OI.numoppitemtCode
		
				-- Op_Flag is 2(UPDATE) FOR MASTER PO AND 1(INSERT) FOR OTHER
				SET @strItems =(
									SELECT 
										numoppitemtCode,numItemCode,CAST(numUnitHour AS DECIMAL(18,8)) AS numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,numToWarehouseItemID,
										Op_Flag, ItemType,DropShip,	bitDiscountType, CAST(fltDiscount AS DECIMAL(18,8)) AS fltDiscount, monTotAmtBefDiscount,	vcItemName,numUOM, bitWorkOrder,
										numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,bitItemPriceApprovalRequired
									FROM 
										@TEMPMasterItems
									FOR XML RAW ('Item'), ROOT ('NewDataSet'), ELEMENTS
								)
		
				--UPDATE MASTER PO AND ADD MERGE PO ITEMS
				EXEC USP_OppManage @numOppID=@numMasterPOID OUTPUT,@numContactId=@numContactId,@numDivisionId=@numDivisionId,@tintSource=@tintSource,@vcPOppName=@vcPOppName output,
									@Comments=@Comments,@bitPublicFlag=@bitPublicFlag,@numUserCntID=@numUserCntID,@monPAmount=@monPAmount,@numAssignedTo=@numAssignedTo,
									@numDomainId=@numDomainId,@strItems=@strItems,@strMilestone=@strMilestone,@dtEstimatedCloseDate=@dtEstimatedCloseDate,@CampaignID=@CampaignID,
									@lngPConclAnalysis=@lngPConclAnalysis,@tintOppType=@tintOppType,@tintActive=@tintActive,@numSalesOrPurType=@numSalesOrPurType,
									@numRecurringId=@numRecurringId,@numCurrencyID=@numCurrencyID,@DealStatus=@DealStatus,@numStatus=@numStatus,@vcOppRefOrderNo=@vcOppRefOrderNo,
									@numBillAddressId=@numBillAddressId,@numShipAddressId=@numShipAddressId,@bitStockTransfer=@bitStockTransfer,@WebApiId=@WebApiId,
									@tintSourceType=@tintSourceType,@bitDiscountType=@bitDiscountType,@fltDiscount=@fltDiscount,@bitBillingTerms=@bitBillingTerms,
									@intBillingDays=@intBillingDays,@bitInterestType=@bitInterestType,@fltInterest=@fltInterest,@tintTaxOperator=@tintTaxOperator,
									@numDiscountAcntType=@numDiscountAcntType,@vcWebApiOrderNo=@vcWebApiOrderNo,@vcCouponCode=@vcCouponCode,@vcMarketplaceOrderID=@vcMarketplaceOrderID,
									@vcMarketplaceOrderDate=@vcMarketplaceOrderDate,@vcMarketplaceOrderReportId=@vcMarketplaceOrderReportId,@numPercentageComplete=@numPercentageComplete,
									@bitUseShippersAccountNo=@bitUseShippersAccountNo,@bitUseMarkupShippingRate=@bitUseMarkupShippingRate,
									@numMarkupShippingRate=@numMarkupShippingRate,@intUsedShippingCompany=@intUsedShippingCompany,@numShipmentMethod=@numShipmentMethod,@dtReleaseDate=@dtReleaseDate,@numPartner=@numPartner,@numPartenerContactId=@numPartenerContactId
									,@numAccountClass=@numAccountClass,@numVendorAddressID=@numVendorAddressID					

				DECLARE @TEMPOpp TABLE
				(
					ID INT IDENTITY(1,1)
					,numOppID NUMERIC(18,0)
				)

				INSERT INTO @TEMPOpp
				(
					numOppID
				)
				SELECT DISTINCT
					numOppID
				FROM
					@TEMP


				DECLARE @i INT = 1
				DECLARE @iCount INT
				DECLARE @numOppID NUMERIC(18,0)

				SELECT @iCount=COUNT(*) FROM @TEMPOpp

				-- DELETE MERGED POS
				WHILE @i <= @iCount
				BEGIN
					SELECT @numOppID=numOppID FROM @TEMPOpp WHERE ID = @i

					IF EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppID) AND NOT EXISTS (SELECT numoppitemtCode FROM OpportunityItems OI WHERE OI.numOppId=@numOppID AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM @TEMP))
					BEGIN
						EXEC USP_DeleteOppurtunity @numOppId=@numOppID,@numDomainID=@numDomainID,@numUserCntID=@numUserCntID
					END
					ELSE
					BEGIN
						EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID

						DELETE
							OI
						FROM
							@TEMP T
						INNER JOIN
							OpportunityItems OI
						ON
							T.numOppItemID = OI.numoppitemtCode
						WHERE
							T.numOppID = @numOppID

						EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID


						UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0),bintModifiedDate=GETUTCDATE(),numModifiedBy=@numUserCntID WHERE numOppId=@numOppID

						--INSERT TAX FOR DIVISION   
						IF (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1
						BEGIN
							--Delete Tax for Opportunity Items if item deleted 
							DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

							--Insert Tax for Opportunity Items
							INSERT INTO dbo.OpportunityItemsTaxItems 
							(
								numOppId,
								numOppItemID,
								numTaxItemID,
								numTaxID
							) 
							SELECT 
								@numOppId,
								OI.numoppitemtCode,
								TI.numTaxItemID,
								0
							FROM 
								dbo.OpportunityItems OI 
							JOIN 
								dbo.ItemTax IT 
							ON 
								OI.numItemCode=IT.numItemCode 
							JOIN
								TaxItems TI 
							ON 
								TI.numTaxItemID = IT.numTaxItemID 
							WHERE 
								OI.numOppId=@numOppID 
								AND IT.bitApplicable=1 
								AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
							UNION
							SELECT 
								@numOppId,
								OI.numoppitemtCode,
								0,
								0
							FROM 
								dbo.OpportunityItems OI 
							JOIN 
								dbo.Item I 
							ON 
								OI.numItemCode=I.numItemCode
							WHERE 
								OI.numOppId=@numOppID 
								AND I.bitTaxable=1 
								AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
							UNION
							SELECT
								@numOppId,
								OI.numoppitemtCode,
								1,
								TD.numTaxID
							FROM
								dbo.OpportunityItems OI 
							INNER JOIN
								ItemTax IT
							ON
								IT.numItemCode = OI.numItemCode
							INNER JOIN
								TaxDetails TD
							ON
								TD.numTaxID = IT.numTaxID
								AND TD.numDomainId = @numDomainId
							WHERE
								OI.numOppId = @numOppID
								AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
						END
					END

					SET @i = @i + 1
				END
			COMMIT
			END TRY
			BEGIN CATCH
				DECLARE @ErrorMessage NVARCHAR(4000)
				DECLARE @ErrorNumber INT
				DECLARE @ErrorSeverity INT
				DECLARE @ErrorState INT
				DECLARE @ErrorLine INT
				DECLARE @ErrorProcedure NVARCHAR(200)

				IF @@TRANCOUNT > 0
					ROLLBACK TRANSACTION;

				SELECT 
					@ErrorMessage = ERROR_MESSAGE(),
					@ErrorNumber = ERROR_NUMBER(),
					@ErrorSeverity = ERROR_SEVERITY(),
					@ErrorState = ERROR_STATE(),
					@ErrorLine = ERROR_LINE(),
					@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

				RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
			END CATCH	
		END
	END
	ELSE
	BEGIN
		RAISERROR('MASTER_PO_DOES_NOT_EXISTS',16,1)
	END
END
GO



GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO

IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_PutAway')
DROP PROCEDURE USP_OpportunityMaster_PutAway
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_PutAway]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@numOppItemID NUMERIC(18,0)
	,@numQtyReceived FLOAT
	,@dtItemReceivedDate DATETIME
	,@vcWarehouses VARCHAR(MAX)
	,@numVendorInvoiceBizDocID NUMERIC(18,0)
	,@tintMode TINYINT
)
AS
BEGIN
	IF ISNULL(@numQtyReceived,0) <= 0 
	BEGIN
		RAISERROR('QTY_TO_RECEIVE_NOT_PROVIDED',16,1)
		RETURN
	END 

	IF LEN(ISNULL(@vcWarehouses,'')) = 0
	BEGIN
		RAISERROR('LOCATION_NOT_SELECTED',16,1)
		RETURN
	END

	DECLARE @TEMPWarehouseLot TABLE
	(
		ID INT
		,numWareHouseItmsDTLID NUMERIC(18,0)
		,vcSerialNo VARCHAR(300)
		,numQty FLOAT
	)

	DECLARE @TEMPSerialLot TABLE
	(
		ID INT
		,vcSerialNo VARCHAR(300)
		,numQty FLOAT
	)

	DECLARE @TEMPWarehouseLocations TABLE
	(
		ID INT IDENTITY(1,1)
		,numWarehouseLocationID NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(18,0)
		,numReceivedQty FLOAT
		,vcSerialLotNo VARCHAR(MAX)
	)

	DECLARE @hDocItem int
	EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcWarehouses

	INSERT INTO @TEMPWarehouseLocations
	(
		numWarehouseLocationID
		,numWarehouseItemID
		,numReceivedQty
		,vcSerialLotNo
	)
	SELECT 
		(CASE WHEN ISNULL(bitNewLocation,0) = 1 THEN ID ELSE 0 END)
		,(CASE WHEN ISNULL(bitNewLocation,0) = 1 THEN 0 ELSE ID END)
		,ISNULL(Qty,0)
		,ISNULL(SerialLotNo,'') 
	FROM 
		OPENXML (@hDocItem, '/NewDataSet/Table1',2)
	WITH 
	(
		bitNewLocation BIT, 
		ID NUMERIC(18,0),
		Qty FLOAT,
		SerialLotNo VARCHAR(MAX)
	)

	EXEC sp_xml_removedocument @hDocItem

	IF @numQtyReceived <> ISNULL((SELECT SUM(numReceivedQty) FROM @TEMPWarehouseLocations),0)
	BEGIN
		RAISERROR('INALID_QTY_RECEIVED',16,1)
		RETURN
	END

	IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
	BEGIN
		-- IF RECEIVED AGAINST VENDOR INVOICE MAKE SURE ENOUGH QTY IS PENDING TO RECIEVED AGAINST VEDOR INVOICE BECAUSE USER CAN RECEIVE QTY WITHOUT SELECTING VENDOR INVOCIE ALSO
		IF (SELECT
				COUNT(*)
			FROM
				OpportunityBizDocItems OBDI
			INNER JOIN
				OpportunityItems OI
			ON
				OBDI.numOppItemID=OI.numoppitemtCode
			WHERE
				numOppBizDocID=@numVendorInvoiceBizDocID
				AND OBDI.numOppItemID = @numOppItemID
				AND @numQtyReceived > (ISNULL(OI.numUnitHour,0) - ISNULL(OI.numUnitHourReceived,0))
			) > 0
		BEGIN
			RAISERROR('INVALID_VENDOR_INVOICE_RECEIVE_QTY',16,1)
			RETURN
		END
	END

	DECLARE @description AS VARCHAR(100)
	DECLARE @numWarehouseItemID AS NUMERIC(18,0)
	DECLARE @numFromWarehouseID AS NUMERIC(18,0)
	DECLARE @numFromWarehouseItemID NUMERIC(18,0)
	DECLARE @numWLocationID AS NUMERIC(18,0)     
	DECLARE @numOldQtyReceived AS FLOAT
	DECLARE @bitStockTransfer BIT
	DECLARE @numToWarehouseItemID NUMERIC
	DECLARE @numUnits FLOAT
	DECLARE @monPrice AS DECIMAL(20,5) 
	DECLARE @numItemCode NUMERIC(18,0)
	DECLARE @monListPrice DECIMAL(20,5)
	DECLARE @numWarehouseID NUMERIC(18,0)
	DECLARE @bitSerial BIT
	DECLARE @bitLot BIT
		
	SELECT  
		@numWarehouseItemID = ISNULL([numWarehouseItmsID], 0),
		@numFromWarehouseID=ISNULL(WI.numWareHouseID,0),
		@numFromWarehouseItemID = ISNULL([numWarehouseItmsID], 0),
		@numWLocationID = ISNULL(WI.numWLocationID,0),
		@numOldQtyReceived = ISNULL([numUnitHourReceived], 0),
		@bitStockTransfer = ISNULL(OM.bitStockTransfer,0),
		@numToWarehouseItemID= ISNULL(numToWarehouseItemID,0),
		@numOppId=OM.numOppId,
		@numUnits=OI.numUnitHour,
		@monPrice=isnull(monPrice,0) * (CASE WHEN ISNULL(OM.fltExchangeRate,0)=0 THEN 1 ELSE OM.fltExchangeRate END),
		@numItemCode=OI.numItemCode,
		@monListPrice=ISNULL(I.monListPrice,0),
		@numWarehouseID=WI.numWareHouseID,
		@bitSerial=ISNULL(I.bitSerialized,0),
		@bitLot=ISNULL(I.bitLotNo,0)
	FROM 
		[OpportunityItems] OI 
	INNER JOIN 
		dbo.OpportunityMaster OM
	ON 
		OI.numOppId = OM.numOppId
	INNER JOIN
		Item I
	ON
		OI.numItemCode = I.numItemCode
	LEFT JOIN
		WareHouseItems WI
	ON
		OI.numWarehouseItmsID = WI.numWareHouseItemID
	WHERE 
		[numoppitemtCode] = @numOppItemID

	IF (@numQtyReceived + @numOldQtyReceived) > @numUnits
	BEGIN
		RAISERROR('RECEIVED_QTY_MUST_BE_LESS_THEN_OR_EQUAL_TO_ORDERED_QTY',16,1)
		RETURN
	END

	IF @bitStockTransfer = 1 --added by chintan
	BEGIN
		DECLARE @p3 VARCHAR(MAX) = ''
		
		EXEC USP_UpdateQtyShipped @numQtyReceived,@numOppItemID,@numUserCntID,@vcError=@p3 OUTPUT

		IF LEN(@p3)>0 
		BEGIN
			RAISERROR ( @p3,16, 1 )
			RETURN ;
		END    

		SET @numWarehouseItemID = @numToWarehouseItemID
	END
	ELSE
	BEGIN
		--Updating the Average Cost
		DECLARE @TotalOnHand AS FLOAT = 0
		DECLARE @monAvgCost AS DECIMAL(20,5) 

		SELECT 
			@TotalOnHand=(SUM(ISNULL(numOnHand,0)) + SUM(ISNULL(numAllocation,0))) 
		FROM 
			dbo.WareHouseItems 
		WHERE 
			numItemID=@numItemCode 
						
		SELECT 
			@monAvgCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost, 0) END) 
		FROM 
			Item 
		WHERE 
			numitemcode = @numItemCode  
    
  		SET @monAvgCost = ((@TotalOnHand * @monAvgCost) + (@numQtyReceived * @monPrice)) / ( @TotalOnHand + @numQtyReceived )
                            
		UPDATE  
			Item
		SET 
			monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monAvgCost END)
		WHERE 
			numItemCode = @numItemCode
    END	

	UPDATE
		WareHouseItems
	SET 
		numOnOrder = ISNULL(numOnOrder,0) - @numQtyReceived,
		dtModified = GETDATE()
	WHERE
		numWareHouseItemID=@numWareHouseItemID
		
	SET @description = CONCAT('PO Qty Received (Qty:',@numQtyReceived,')')

	EXEC dbo.USP_ManageWareHouseItems_Tracking
		@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
		@numReferenceID = @numOppId, --  numeric(9, 0)
		@tintRefType = 4, --  tinyint
		@vcDescription = @description,
		@numModifiedBy = @numUserCntID,
		@dtRecordDate =  @dtItemReceivedDate,
		@numDomainID = @numDomainID


	DECLARE @i INT
	DECLARE @j INT
	DECLARE @k INT
	DECLARE @iCount INT = 0
	DECLARE @jCount INT = 0
	DECLARE @kCount INT = 0
	DECLARE @vcSerialLot# VARCHAR(MAX)
	DECLARE @numWareHouseItmsDTLID NUMERIC(18,0)
	DECLARE @vcSerailLotNumber VARCHAR(300)
	DECLARE @numSerialLotQty FLOAT
	DECLARE @numTempWLocationID NUMERIC(18,0)
	DECLARE @numTempWarehouseItemID NUMERIC(18,0)
	DECLARE @numTempReceivedQty FLOAT
	DECLARE @numLotWareHouseItmsDTLID NUMERIC(18,0)
	DECLARE @numLotQty FLOAT
	
	SET @i = 1
	SET @iCount = (SELECT COUNT(*) FROM @TEMPWarehouseLocations)

	WHILE @i <= @iCount
	BEGIN
		SELECT 
			@numTempWLocationID=ISNULL(numWarehouseLocationID,0)
			,@numTempWarehouseItemID=ISNULL(numWarehouseItemID,0)
			,@numTempReceivedQty=ISNULL(numReceivedQty,0)
			,@vcSerialLot# = ISNULL(vcSerialLotNo,'')
		FROM 
			@TEMPWarehouseLocations 
		WHERE 
			ID = @i

		IF (ISNULL(@bitLot,0)=1 OR ISNULL(@bitSerial,0)=1)
		BEGIN
			IF LEN(ISNULL(@vcSerialLot#,'')) = 0
			BEGIN
				RAISERROR('SERIALLOT_REQUIRED',16,1)
				RETURN
			END 
			ELSE
			BEGIN	
				DELETE FROM @TEMPSerialLot

				IF ISNULL(@bitSerial,0) = 1
				BEGIN
					INSERT INTO @TEMPSerialLot
					(
						ID
						,vcSerialNo
						,numQty
					)
					SELECT
						ROW_NUMBER() OVER(ORDER BY OutParam)
						,RTRIM(LTRIM(OutParam))
						,1
					FROM
						dbo.SplitString(@vcSerialLot#,',')
				END
				ELSE IF @bitLot = 1
				BEGIN
					INSERT INTO @TEMPSerialLot
					(
						ID
						,vcSerialNo
						,numQty
					)
					SELECT
						ROW_NUMBER() OVER(ORDER BY OutParam)
						,(CASE 
							WHEN PATINDEX('%(%', OutParam) > 0 AND PATINDEX('%)%', OutParam) > 0 AND PATINDEX('%)%',OutParam) > (PATINDEX('%(%',OutParam) + 1)
							THEN RTRIM(LTRIM(SUBSTRING(OutParam,0,PATINDEX('%(%', OutParam))))
							ELSE ''
						END)
						,(CASE 
							WHEN PATINDEX('%(%', OutParam) > 0 AND PATINDEX('%)%', OutParam) > 0 AND PATINDEX('%)%',OutParam) > (PATINDEX('%(%',OutParam) + 1)
							THEN (CASE WHEN ISNUMERIC(SUBSTRING(OutParam,(PATINDEX('%(%', OutParam) + 1),(PATINDEX('%)%', OutParam) - PATINDEX('%(%', OutParam) - 1))) = 1 
									THEN CAST(SUBSTRING(OutParam,(PATINDEX('%(%', OutParam) + 1),(PATINDEX('%)%', OutParam) - PATINDEX('%(%', OutParam) - 1)) AS FLOAT)  
									ELSE -1 
									END) 
							ELSE -1
						END)	
	
					FROM
						dbo.SplitString(@vcSerialLot#,',')
				END

				IF EXISTS (SELECT vcSerialNo FROM @TEMPSerialLot WHERE numQty = -1)
				BEGIN
					RAISERROR('INVALID_SERIAL_NO',16,1)
					RETURN
				END
				ELSE IF @numTempReceivedQty <> ISNULL((SELECT SUM(numQty) FROM @TEMPSerialLot),0)
				BEGIN
					RAISERROR('INALID_SERIALLOT',16,1)
					RETURN
				END
			END
		END

		IF ISNULL(@numTempWarehouseItemID,0) = 0
		BEGIN
			IF EXISTS (SELECT numWLocationID FROM WarehouseLocation WHERE numDomainID=@numDomainID AND numWLocationID=ISNULL(@numTempWLocationID,0))
			BEGIN
				IF EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode AND numWareHouseID=@numWarehouseID AND numWLocationID=@numTempWLocationID)
				BEGIN
					SET @numTempWarehouseItemID = (SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode AND numWareHouseID=@numWarehouseID AND numWLocationID=@numTempWLocationID)
				END
				ELSE 
				BEGIN
					INSERT INTO WareHouseItems 
					(
						numItemID, 
						numWareHouseID,
						numWLocationID,
						numOnHand,
						numAllocation,
						numOnOrder,
						numBackOrder,
						numReorder,
						monWListPrice,
						vcLocation,
						vcWHSKU,
						vcBarCode,
						numDomainID,
						dtModified
					)  
					VALUES
					(
						@numItemCode,
						@numWareHouseID,
						@numTempWLocationID,
						0,
						0,
						0,
						0,
						0,
						@monListPrice,
						ISNULL((SELECT vcLocation FROM WarehouseLocation WHERE numDomainID=@numDomainID AND numWLocationID=ISNULL(@numTempWLocationID,0)),''),
						'',
						'',
						@numDomainID,
						GETDATE()
					)  

					SELECT @numTempWarehouseItemID = SCOPE_IDENTITY()

					DECLARE @dtDATE AS DATETIME = GETUTCDATE()
					DECLARE @vcDescription VARCHAR(100)='INSERT WareHouse'

					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numTempWarehouseItemID,
						@numReferenceID = @numItemCode,
						@tintRefType = 1,
						@vcDescription = @vcDescription,
						@numModifiedBy = @numUserCntID,
						@ClientTimeZoneOffset = 0,
						@dtRecordDate = @dtDATE,
						@numDomainID = @numDomainID 
				END
			END
			ELSE 
			BEGIN
				RAISERROR('INVALID_WAREHOUSE_LOCATION',16,1)
				RETURN
			END
		END

		IF NOT EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode AND numWareHouseItemID=@numTempWarehouseItemID)
		BEGIN
			RAISERROR('INVALID_WAREHOUSE',16,1)
			RETURN
		END
		ELSE
		BEGIN
			-- INCREASE THE OnHand Of Destination Location
			UPDATE
				WareHouseItems
			SET
				numBackOrder = (CASE WHEN numBackOrder > @numTempReceivedQty THEN ISNULL(numBackOrder,0) - @numTempReceivedQty ELSE 0 END),         
				numAllocation = (CASE WHEN numBackOrder > @numTempReceivedQty THEN ISNULL(numAllocation,0) + @numTempReceivedQty ELSE ISNULL(numAllocation,0) + ISNULL(numBackOrder,0) END),
				numOnHand = (CASE WHEN numBackOrder > @numTempReceivedQty THEN numOnHand ELSE ISNULL(numOnHand,0) + (@numTempReceivedQty - ISNULL(numBackOrder,0)) END),
				dtModified = GETDATE()
			WHERE
				numWareHouseItemID=@numTempWarehouseItemID

			SET @description = CONCAT('PO Qty Put-Away (Qty:',@numTempReceivedQty,')')

			EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numTempWarehouseItemID, --  numeric(9, 0)
				@numReferenceID = @numOppId, --  numeric(9, 0)
				@tintRefType = 4, --  tinyint
				@vcDescription = @description,
				@numModifiedBy = @numUserCntID,
				@dtRecordDate =  @dtItemReceivedDate,
				@numDomainID = @numDomainID

			IF ISNULL(@numTempWarehouseItemID,0) > 0 AND ISNULL(@numTempWarehouseItemID,0) <> @numWareHouseItemID		
			BEGIN
				INSERT INTO OpportunityItemsReceievedLocation
				(
					numDomainID,
					numOppID,
					numOppItemID,
					numWarehouseItemID,
					numUnitReceieved
				)
				VALUES
				(
					@numWarehouseItemID,
					@numOppId,
					@numOppItemID,
					@numTempWarehouseItemID,
					@numTempReceivedQty
				)
			END

			IF (ISNULL(@bitLot,0)=1 OR ISNULL(@bitSerial,0)=1)
			BEGIN
				SET @j = 1
				SET @jCount = (SELECT COUNT(*) FROM @TEMPSerialLot)

				WHILE (@j <= @jCount AND @numTempReceivedQty > 0)
				BEGIN
					SELECT 
						@vcSerailLotNumber=ISNULL(vcSerialNo,'')
						,@numSerialLotQty=ISNULL(numQty,0)
					FROM 
						@TEMPSerialLot 
					WHERE 
						ID=@j

					IF @numSerialLotQty > 0
					BEGIN
						IF @bitStockTransfer = 1
						BEGIN
							IF (SELECT COUNT(*) FROM WareHouseItems INNER JOIN WareHouseItmsDTL ON WareHouseItems.numWareHouseItemID = WareHouseItmsDTL.numWareHouseItemID WHERE WareHouseItems.numItemID=@numItemCode AND WareHouseItems.numWareHouseID=@numFromWarehouseID AND LOWER(vcSerialNo)=LOWER(@vcSerailLotNumber)) = 0
							BEGIN
								RAISERROR('INVALID_SERIAL_NO',16,1)
								RETURN
							END
					
							IF @bitLot = 1
							BEGIN
								IF (SELECT SUM(WareHouseItmsDTL.numQty) FROM WareHouseItems INNER JOIN WareHouseItmsDTL ON WareHouseItems.numWareHouseItemID = WareHouseItmsDTL.numWareHouseItemID WHERE WareHouseItems.numItemID=@numItemCode AND WareHouseItems.numWareHouseID=@numFromWarehouseID AND LOWER(vcSerialNo)=LOWER(@vcSerailLotNumber)) < @numTempReceivedQty
								BEGIN
									RAISERROR('INVALID_LOT_QUANTITY',16,1)
									RETURN
								END

								DELETE FROM @TEMPWarehouseLot

								INSERT INTO @TEMPWarehouseLot
								(
									ID
									,numWareHouseItmsDTLID
									,numQty
								)
								SELECT
									ROW_NUMBER() OVER(ORDER BY WareHouseItmsDTL.numQty)
									,WareHouseItmsDTL.numWareHouseItmsDTLID
									,ISNULL(WareHouseItmsDTL.numQty,0)
								FROM
									WareHouseItems 
								INNER JOIN 
									WareHouseItmsDTL 
								ON 
									WareHouseItems.numWareHouseItemID = WareHouseItmsDTL.numWareHouseItemID 
								WHERE 
									WareHouseItems.numItemID=@numItemCode 
									AND WareHouseItems.numWareHouseID=@numFromWarehouseID 
									AND LOWER(vcSerialNo)=LOWER(@vcSerailLotNumber)

								SET @k = 1
								SELECT @kCount=COUNT(*) FROM @TEMPWarehouseLot

								WHILE @k <= @kCount AND @numTempReceivedQty > 0
								BEGIN
									SELECT
										@numLotWareHouseItmsDTLID=numWareHouseItmsDTLID
										,@numLotQty = numQty
									FROM
										@TEMPWarehouseLot
									WHERE
										ID=@k

									IF @numTempReceivedQty > @numLotQty
									BEGIN
										UPDATE
											WareHouseItmsDTL
										SET
											numWareHouseItemID = @numTempWarehouseItemID
										WHERE 
											numWareHouseItmsDTLID=@numLotWareHouseItmsDTLID

										UPDATE @TEMPSerialLot SET numQty = numQty-@numLotQty WHERE ID=@j
										SET @numTempReceivedQty = @numTempReceivedQty - @numLotQty
									END
									ELSE
									BEGIN
										UPDATE 
											WareHouseItmsDTL 
										SET 
											numQty = numQty - @numTempReceivedQty
										WHERE 
											numWareHouseItmsDTLID=@numLotWareHouseItmsDTLID

										INSERT INTO WareHouseItmsDTL
										(
											numWareHouseItemID,
											vcSerialNo,
											numQty,
											dExpirationDate,
											bitAddedFromPO
										)
										SELECT
											@numTempWarehouseItemID,
											vcSerialNo,
											@numTempReceivedQty,
											dExpirationDate,
											bitAddedFromPO
										FROM 
											WareHouseItmsDTL
										WHERE
											numWareHouseItmsDTLID=@numLotWareHouseItmsDTLID

										UPDATE @TEMPSerialLot SET numQty = numQty-@numTempReceivedQty WHERE ID=@j
										SET @numTempReceivedQty = 0
									END
									

									SET @k = @k + 1
								END
							END
							ELSE
							BEGIN
								IF NOT EXISTS (SELECT WareHouseItmsDTL.numWareHouseItmsDTLID FROM WareHouseItems INNER JOIN WareHouseItmsDTL ON WareHouseItems.numWareHouseItemID = WareHouseItmsDTL.numWareHouseItemID WHERE WareHouseItems.numItemID=@numItemCode AND WareHouseItems.numWareHouseID=@numFromWarehouseID AND LOWER(vcSerialNo)=LOWER(@vcSerailLotNumber) AND ISNULL(WareHouseItmsDTL.numQty,0) = 1)
								BEGIN
									RAISERROR('INVALID_SERIAL_QUANTITY',16,1)
									RETURN
								END

								UPDATE
									WHID
								SET
									WHID.numWareHouseItemID = @numTempWarehouseItemID
								FROM
									WareHouseItems WI
								INNER JOIN
									WareHouseItmsDTL WHID
								ON 
									WI.numWareHouseItemID = WHID.numWareHouseItemID 
								WHERE 
									WI.numItemID=@numItemCode 
									AND WI.numWareHouseID=@numFromWarehouseID 
									AND LOWER(WHID.vcSerialNo)=LOWER(@vcSerailLotNumber)

								UPDATE @TEMPSerialLot SET numQty = 0 WHERE ID=@j
								SET @numTempReceivedQty = @numTempReceivedQty - 1
							END
						END
						ELSE
						BEGIN
							IF @bitSerial = 1 AND (SELECT COUNT(*) FROM WareHouseItems INNER JOIN WareHouseItmsDTL ON WareHouseItems.numWareHouseItemID = WareHouseItmsDTL.numWareHouseItemID WHERE WareHouseItems.numItemID=@numItemCode AND LOWER(vcSerialNo)=LOWER(@vcSerailLotNumber)) > 0
							BEGIN
								RAISERROR('DUPLICATE_SERIAL_NO',16,1)
								RETURN
							END

							INSERT INTO WareHouseItmsDTL
							(
								numWareHouseItemID
								,vcSerialNo
								,numQty
								,bitAddedFromPO
							)  
							VALUES
							( 
								@numTempWarehouseItemID
								,@vcSerailLotNumber
								,(CASE WHEN @numTempReceivedQty >= @numSerialLotQty THEN @numSerialLotQty ELSE @numTempReceivedQty END)
								,1 
							)

		    
							SET @numWareHouseItmsDTLID=SCOPE_IDENTITY()
		
							INSERT INTO OppWarehouseSerializedItem
							(
								numWarehouseItmsDTLID
								,numOppId
								,numOppItemId
								,numWarehouseItmsID
								,numQty
							)  
							VALUES
							( 
								@numWareHouseItmsDTLID
								,@numOppID
								,@numOppItemID
								,@numTempWarehouseItemID
								,(CASE WHEN @numTempReceivedQty >= @numSerialLotQty THEN @numSerialLotQty ELSE @numTempReceivedQty END)
							)

							IF @numTempReceivedQty >= @numSerialLotQty
							BEGIN
								SET @numTempReceivedQty = @numTempReceivedQty - @numSerialLotQty
								UPDATE @TEMPSerialLot SET numQty = 0  WHERE ID = @j
							END
							ELSE
							BEGIN
								UPDATE @TEMPSerialLot SET numQty = numQty - @numTempReceivedQty  WHERE ID = @j
								SET @numTempReceivedQty = 0
							END
						END
					END

					SET @j = @j + 1
				END

				IF @numTempReceivedQty > 0
				BEGIN
					RAISERROR('INALID_SERIALLOT',16,1)
					RETURN
				END
			END
		END


		SET @i = @i + 1
	END

	UPDATE  
		[OpportunityItems]
	SET 
		numUnitHourReceived = ISNULL(numUnitHourReceived,0) + ISNULL(@numQtyReceived,0)
		,numQtyReceived = (CASE WHEN ISNULL(@tintMode,0)=1 THEN ISNULL(numQtyReceived,0) + ISNULL(@numQtyReceived,0) ELSE ISNULL(numQtyReceived,0) END)
	WHERE
		[numoppitemtCode] = @numOppItemID


	UPDATE I SET dtItemReceivedDate=@dtItemReceivedDate FROM OpportunityItems OI INNER JOIN Item I ON OI.numItemCode=I.numItemCode WHERE OI.numoppitemtCode=@numOppItemID


	IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
	BEGIN
		UPDATE  
			OpportunityBizDocItems
		SET 
			numVendorInvoiceUnitReceived = ISNULL(numVendorInvoiceUnitReceived,0) +  ISNULL(@numQtyReceived,0)
		WHERE
			numOppBizDocID=@numVendorInvoiceBizDocID
			AND numOppItemID = @numOppItemID
	END

	UPDATE dbo.OpportunityMaster SET dtItemReceivedDate=@dtItemReceivedDate WHERE numOppId=@numOppId

END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_UpdatePurchaseOrderCost')
DROP PROCEDURE USP_OpportunityMaster_UpdatePurchaseOrderCost
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_UpdatePurchaseOrderCost]  
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@vcSelectedRecords VARCHAR(MAX),
	@ClientTimeZoneOffset INT
AS  
BEGIN
	DECLARE @hDocItem INT

	DECLARE @TEMP TABLE
	(
		numVendorID NUMERIC(18,0)
		,numSelectedVendorID NUMERIC(18,0)
		,numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numUnits FLOAT
		,monUnitCost DECIMAL(20,5)
	)

	EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcSelectedRecords

	INSERT INTO @TEMP
	(
		numVendorID
		,numSelectedVendorID
		,numOppID
		,numOppItemID
		,numUnits
		,monUnitCost
	)
	SELECT
		ISNULL(VendorID,0)
		,ISNULL(SelectedVendorID,0)
		,ISNULL(OppID,0)
		,ISNULL(OppItemID,0)
		,ISNULL(Units,0)
		,ISNULL(UnitCost,0)
	FROM
		OPENXML (@hDocItem,'/NewDataSet/Table1',2)
	WITH
	(
		VendorID NUMERIC(18,0)
		,SelectedVendorID NUMERIC(18,0)
		,OppID NUMERIC(18,0)
		,OppItemID NUMERIC(18,0)
		,Units FLOAT
		,UnitCost DECIMAL(20,5)
	)

	EXEC sp_xml_removedocument @hDocItem

	IF EXISTS (SELECT OB.numOppBizDocsId FROM OpportunityBizDocs OB INNER JOIN @TEMP T ON OB.numOppId = T.numOppID)
	BEGIN
		RAISERROR('BIZ_DOC_EXISTS',16,1)
		RETURN
	END
	ELSE
	BEGIN
		DECLARE @TEMPVendorItems TABLE
		(
			ID INT IDENTITY(1,1)
			,numVendorID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numWarehouseItemID NUMERIC(18,0)
			,numUnitHour FLOAT
			,monPrice DECIMAL(20,5)
		)

		INSERT INTO @TEMPVendorItems
		(
			numVendorID
			,numItemCode
			,numWarehouseItemID
			,numUnitHour
			,monPrice
		)
		SELECT 
			T.numSelectedVendorID
			,OI.numItemCode
			,OI.numWarehouseItmsID
			,OI.numUnitHour
			,T.monUnitCost
		FROM
			@TEMP T
		INNER JOIN
			OpportunityItems OI
		ON
			T.numOppID = OI.numOppId
			AND T.numOppItemID = OI.numoppitemtCode
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppId
		WHERE
			OM.numDomainId = @numDomainID
			AND OM.numDivisionId <> T.numSelectedVendorID

		BEGIN TRY
		BEGIN TRANSACTION
			DECLARE @TEMPOpp TABLE
			(
				ID INT IDENTITY(1,1)
				,numOppID NUMERIC(18,0)
			)

			INSERT INTO @TEMPOpp
			(
				numOppID
			)
			SELECT DISTINCT
				numOppID
			FROM
				@TEMP

			DECLARE @i INT = 1
			DECLARE @iCount INT
			DECLARE @numOppID NUMERIC(18,0)

			SELECT @iCount=COUNT(*) FROM @TEMPOpp

			-- DELETE PO NOT ITEM LEFT IN ORDER OR UPDATE ORDER TOTAL AMOUNT AND TAXES
			WHILE @i <= @iCount
			BEGIN
				SELECT @numOppID=numOppID FROM @TEMPOpp WHERE ID = @i

				EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID

				-- DELETE ITEMS FROM ORDER FOR WHOM ANOTHER VENDOR IS SELECTED TO CREATE NEW ORDER
				DELETE 
					OI
				FROM
					OpportunityMaster OM
				INNER JOIN
					OpportunityItems OI
				ON
					OM.numOppId = OI.numOppId
				INNER JOIN
					@TEMP T
				ON
					OI.numOppId = T.numOppID
					AND OI.numoppitemtCode = T.numOppItemID
				WHERE
					OM.numDomainId = @numDomainID
					AND OM.numOppId = @numOppID
					AND OM.numDivisionId <> T.numSelectedVendorID

				-- UPDATE LINE ITEM UNITS AND COST WHERE VENDOR IS SAME
				UPDATE
					OI
				SET
					OI.numUnitHour = ISNULL(T.numUnits,0)
					,OI.monPrice = ISNULL(T.monUnitCost,0)
					,monTotAmount = (ISNULL(T.numUnits,0) * (CASE 
																	WHEN ISNULL(OI.bitDiscountType,0) = 0 
																	THEN (ISNULL(T.monUnitCost,0) - (ISNULL(T.monUnitCost,0) * (ISNULL(OI.fltDiscount,0) / 100)))
																	ELSE (ISNULL(T.monUnitCost,0) -ISNULL(OI.fltDiscount,0)) 
																END))  
					,monTotAmtBefDiscount = ISNULL(T.numUnits,0) * ISNULL(T.monUnitCost,0)
				FROM
					OpportunityMaster OM
				INNER JOIN
					OpportunityItems OI
				ON
					OM.numOppId = OI.numOppId
				INNER JOIN
					@TEMP T
				ON
					OI.numOppId = T.numOppID
					AND OI.numoppitemtCode = T.numOppItemID
				WHERE
					OM.numDomainId = @numDomainID
					AND OM.numOppId = @numOppID
					AND OM.numDivisionId = T.numSelectedVendorID

				EXEC USP_UpdatingInventoryonCloseDeal @numOppID=@numOppID,@numUserCntID=@numUserCntID

				IF EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppID) AND NOT EXISTS (SELECT numoppitemtCode FROM OpportunityItems OI WHERE OI.numOppId=@numOppID)
				BEGIN
					EXEC USP_DeleteOppurtunity @numOppId=@numOppID,@numDomainID=@numDomainID,@numUserCntID=@numUserCntID
				END
				ELSE
				BEGIN
					UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,GETDATE(),0),bintModifiedDate=GETUTCDATE(),numModifiedBy=@numUserCntID WHERE numOppId=@numOppID

					--INSERT TAX FOR DIVISION   
					IF (select ISNULL(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1
					BEGIN
						--Delete Tax for Opportunity Items if item deleted 
						DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

						--Insert Tax for Opportunity Items
						INSERT INTO dbo.OpportunityItemsTaxItems 
						(
							numOppId,
							numOppItemID,
							numTaxItemID,
							numTaxID
						) 
						SELECT 
							@numOppId,
							OI.numoppitemtCode,
							TI.numTaxItemID,
							0
						FROM 
							dbo.OpportunityItems OI 
						JOIN 
							dbo.ItemTax IT 
						ON 
							OI.numItemCode=IT.numItemCode 
						JOIN
							TaxItems TI 
						ON 
							TI.numTaxItemID = IT.numTaxItemID 
						WHERE 
							OI.numOppId=@numOppID 
							AND IT.bitApplicable=1 
							AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
						UNION
						SELECT 
							@numOppId,
							OI.numoppitemtCode,
							0,
							0
						FROM 
							dbo.OpportunityItems OI 
						JOIN 
							dbo.Item I 
						ON 
							OI.numItemCode=I.numItemCode
						WHERE 
							OI.numOppId=@numOppID 
							AND I.bitTaxable=1 
							AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
						UNION
						SELECT
							@numOppId,
							OI.numoppitemtCode,
							1,
							TD.numTaxID
						FROM
							dbo.OpportunityItems OI 
						INNER JOIN
							ItemTax IT
						ON
							IT.numItemCode = OI.numItemCode
						INNER JOIN
							TaxDetails TD
						ON
							TD.numTaxID = IT.numTaxID
							AND TD.numDomainId = @numDomainId
						WHERE
							OI.numOppId = @numOppID
							AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
					END
				END

				SET @i = @i + 1
			END

			DECLARE @TEMPVendor TABLE
			(
				ID INT IDENTITY(1,1)
				,numVendorID NUMERIC(18,0)
				,numContactID NUMERIC(18,0)
				,intBillingDays INT
			)

			INSERT INTO @TEMPVendor
			(
				numVendorID
				,numContactID
				,intBillingDays
			)
			SELECT DISTINCT
				numVendorID
				,ISNULL((SELECT TOP 1 numContactId FROM AdditionalContactsInformation WHERE numDivisionId=numVendorID ORDER BY ISNULL(bitPrimaryContact,0) DESC),0)
				,ISNULL(numBillingDays,0)
			FROM
				@TEMPVendorItems T
			INNER JOIN
				DivisionMaster
			ON
				T.numVendorID = DivisionMaster.numDivisionID

			DECLARE @j INT = 1
			DECLARE @jCount INT
			DECLARE @numVendorID NUMERIC(18,0)
			DECLARE @numContactID NUMERIC(18,0)
			DECLARE @numCurrencyID NUMERIC(18,0)
			DECLARE @intBillingDays INT
			DECLARE @strItems NVARCHAR(MAX) = ''
			DECLARE @dtEstimatedCloseDate DATETIME

			SELECT @numCurrencyID = ISNULL(numCurrencyID,0) FROM Domain WHERE numDomainId = @numDomainID

			SELECT @jCount=COUNT(*) FROM @TEMPVendor

			WHILE @j <= @jCount
			BEGIN
				SELECT @numVendorID=numVendorID,@numContactID=numContactID,@intBillingDays=intBillingDays FROM @TEMPVendor WHERE ID = @j

				SET @strItems =(
									SELECT 
										0 AS numoppitemtCode
										,T.numItemCode
										,CAST(numUnitHour AS VARCHAR) AS numUnitHour
										,CAST(monPrice AS VARCHAR)
										,CAST(ISNULL(T.numUnitHour,0) * ISNULL(T.monPrice,0) AS VARCHAR) monTotAmount
										,ISNULL(I.txtItemDesc,'')
										,T.numWarehouseItemID AS numWarehouseItmsID
										,0 AS numToWarehouseItemID
										,1 AS Op_Flag
										,I.charItemType AS  ItemType
										,0 AS DropShip
										,0 AS bitDiscountType
										,0 AS fltDiscount
										,CAST(ISNULL(T.numUnitHour,0) * ISNULL(T.monPrice,0) AS VARCHAR) AS monTotAmtBefDiscount
										,vcItemName
										,I.numBaseUnit AS numUOM
										,0 AS bitWorkOrder
										,0 AS numVendorWareHouse
										,0 AS numShipmentMethod
										,0
										,0 AS numProjectID
										,0 AS numProjectStageID
										,'' Attributes
										,0 AS bitItemPriceApprovalRequired
									FROM 
										@TEMPVendorItems T
									INNER JOIN
										Item I
									ON
										T.numItemCode = I.numItemCode
									WHERE
										T.numVendorID = @numVendorID
									FOR XML RAW ('Item'), ROOT ('NewDataSet'), ELEMENTS
								)
		
				SET @dtEstimatedCloseDate = DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,GETUTCDATE())

				--UPDATE MASTER PO AND ADD MERGE PO ITEMS
				EXEC USP_OppManage @numOppID=0,@numContactId=@numContactId,@numDivisionId=@numVendorID,@tintSource=0,@vcPOppName='',
									@Comments='',@bitPublicFlag=0,@numUserCntID=@numUserCntID,@monPAmount=0,@numAssignedTo=@numUserCntID,
									@numDomainId=@numDomainId,@strItems=@strItems,@strMilestone='',@dtEstimatedCloseDate=@dtEstimatedCloseDate,@CampaignID=0,
									@lngPConclAnalysis=0,@tintOppType=2,@tintActive=1,@numSalesOrPurType=0,
									@numRecurringId=0,@numCurrencyID=@numCurrencyID,@DealStatus=1,@numStatus=0,@vcOppRefOrderNo='',
									@numBillAddressId=0,@numShipAddressId=0,@bitStockTransfer=0,@WebApiId=0,
									@tintSourceType=0,@bitDiscountType=0,@fltDiscount=0,@bitBillingTerms=0,
									@intBillingDays=@intBillingDays,@bitInterestType=0,@fltInterest=0,@tintTaxOperator=0,
									@numDiscountAcntType=0,@vcWebApiOrderNo='',@vcCouponCode='',@vcMarketplaceOrderID=0,
									@vcMarketplaceOrderDate=NULL,@vcMarketplaceOrderReportId='',@numPercentageComplete=0,
									@bitUseShippersAccountNo=0,@bitUseMarkupShippingRate=0,
									@numMarkupShippingRate=0,@intUsedShippingCompany=0,@numShipmentMethod=0,@dtReleaseDate=NULL,@numPartner=0,@numPartenerContactId=0
									,@numAccountClass=0,@numVendorAddressID=0	

				SET @j = @j + 1
			END

		COMMIT
		END TRY
		BEGIN CATCH
			DECLARE @ErrorMessage NVARCHAR(4000)
			DECLARE @ErrorNumber INT
			DECLARE @ErrorSeverity INT
			DECLARE @ErrorState INT
			DECLARE @ErrorLine INT
			DECLARE @ErrorProcedure NVARCHAR(200)

			IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION;

			SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorNumber = ERROR_NUMBER(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorLine = ERROR_LINE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

			RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
		END CATCH	
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportQueryExecute')
DROP PROCEDURE USP_ReportQueryExecute
GO
CREATE PROCEDURE [dbo].[USP_ReportQueryExecute]     
    @numDomainID NUMERIC(18,0),
    @numUserCntID NUMERIC(18,0),
	@numReportID NUMERIC(18,0),
    @ClientTimeZoneOffset INT, 
    @textQuery NTEXT,
	@numCurrentPage NUMERIC(18,0)
AS                 
BEGIN
	DECLARE @tintReportType INT
	SELECT @tintReportType=ISNULL(tintReportType,0) FROM ReportListMaster WHERE numReportID=@numReportID

	DECLARE @avgCost int
	SET @avgCost =(select TOP 1 numCost from Domain where numDOmainId=@numDomainID)

	IF CHARINDEX('isnull(OpportunityItems.numRemainingQtyToPick,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityItems.numRemainingQtyToPick,0)','(isnull(OpportunityItems.numUnitHour,0) - isnull(OpportunityItems.numQtyPicked,0))')	
	END	

	IF CHARINDEX('OpportunityItems.numRemainingQtyToPick',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityItems.numRemainingQtyToPick','(isnull(OpportunityItems.numUnitHour,0) - isnull(OpportunityItems.numQtyPicked,0))')	
	END		

	IF CHARINDEX('isnull(OpportunityItems.numRemainingQtyToShip,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityItems.numRemainingQtyToShip,0)','(isnull(OpportunityItems.numUnitHour,0) - isnull(OpportunityItems.numQtyShipped,0))')	
	END	

	IF CHARINDEX('OpportunityItems.numRemainingQtyToShip',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityItems.numRemainingQtyToShip','(isnull(OpportunityItems.numUnitHour,0) - isnull(OpportunityItems.numQtyShipped,0))')	
	END	

	IF CHARINDEX('isnull(OpportunityItems.numRemainingQtyToInvoice,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityItems.numRemainingQtyToInvoice,0)','(isnull(OpportunityItems.numUnitHour,0) - isnull((SELECT SUM(numUnitHour) FROM OpportunityBizDocItems INNER JOIN OpportunityBizDocs ON OpportunityBizDocItems.numOppBizDocID = OpportunityBizDocs.numOppBizDocsId  WHERE OpportunityBizDocs.numOppId = OpportunityItems.numOppId AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode AND ISNULL(bitAuthoritativeBizDocs, 0) = 1 AND ISNULL(OpportunityBizDocs.numBizDocID,0) = 287),0))')	
	END	

	IF CHARINDEX('OpportunityItems.numRemainingQtyToInvoice',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityItems.numRemainingQtyToInvoice','(isnull(OpportunityItems.numUnitHour,0) - isnull((SELECT SUM(numUnitHour) FROM OpportunityBizDocItems INNER JOIN OpportunityBizDocs ON OpportunityBizDocItems.numOppBizDocID = OpportunityBizDocs.numOppBizDocsId  WHERE OpportunityBizDocs.numOppId = OpportunityItems.numOppId AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode AND ISNULL(bitAuthoritativeBizDocs, 0) = 1 AND ISNULL(OpportunityBizDocs.numBizDocID,0) = 287),0))')	
	END	

	IF CHARINDEX('AND OpportunityItems.monPendingSales',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'AND OpportunityItems.monPendingSales','AND (ISNULL((ISNULL(ISNULL(dbo.fn_UOMConversion(ISNULL(Item.numBaseUnit, 0), OpportunityItems.numItemCode, Item.numDomainId, ISNULL(OpportunityItems.numUOMId, 0)), 1) * OpportunityItems.numUnitHour, 0) - isnull(OpportunityItems.numQtyShipped, 0) ) * (isnull(OpportunityItems.monPrice, 0)), 0))')	
	END	
	
	IF CHARINDEX('AND OpportunityItems.numPendingToBill',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'AND OpportunityItems.numPendingToBill','AND ISNULL((isnull(OpportunityItems.numUnitHourReceived, 0)) - (ISNULL(( SELECT SUM(numUnitHour)  FROM	 OpportunityBizDocItems  INNER JOIN	OpportunityBizDocs ON OpportunityBizDocItems.numOppBizDocID = OpportunityBizDocs.numOppBizDocsId  WHERE OpportunityBizDocs.numOppId = OpportunityItems.numOppId AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode AND ISNULL(bitAuthoritativeBizDocs, 0) = 1), 0)), 0)')	
	END	
		
	IF CHARINDEX('AND OpportunityItems.numPendingtoReceive',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'AND OpportunityItems.numPendingtoReceive','AND isnull((isnull((SELECT SUM(numUnitHour) FROM OpportunityBizDocItems INNER JOIN OpportunityBizDocs ON OpportunityBizDocItems.numOppBizDocID = OpportunityBizDocs.numOppBizDocsId WHERE OpportunityBizDocs.numOppId = OpportunityItems.numOppId AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode AND ISNULL(bitAuthoritativeBizDocs, 0) = 1), 0)) - (isnull(OpportunityItems.numUnitHourReceived, 0)), 0)')	
	END

	IF CHARINDEX('AND WareHouseItems.vcLocation',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'AND WareHouseItems.vcLocation','AND WareHouseItems.numWLocationID')
	END

	IF CHARINDEX('ReturnItems.monPrice',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'ReturnItems.monPrice','CAST(ReturnItems.monPrice AS NUMERIC(18,4))')
	END

	IF CHARINDEX('OpportunityItems.monPrice',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityItems.monPrice','CAST(OpportunityItems.monPrice AS NUMERIC(18,4))')
	END

	IF CHARINDEX('isnull(category.numCategoryID,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(category.numCategoryID,0)' ,'(SELECT ISNULL(STUFF((SELECT '','' + CONVERT(VARCHAR(50) , Category.vcCategoryName) FROM ItemCategory LEFT JOIN Category ON ItemCategory.numCategoryID = Category.numCategoryID WHERE ItemCategory.numItemID = Item.numItemCode FOR XML PATH('''')), 1, 1, ''''),''''))')
	END

	IF CHARINDEX('dbo.FormatedDateFromDate(dateadd(minute,-@ClientTimeZoneOffset,OpportunityBizDocs.dtFullPaymentDate),@numDomainId)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'dbo.FormatedDateFromDate(dateadd(minute,-@ClientTimeZoneOffset,OpportunityBizDocs.dtFullPaymentDate),@numDomainId)' ,'(CASE 
																																															WHEN ISNULL(OpportunityMaster.tintOppType,0) = 2 AND ISNULL(OpportunityBizDocs.monDealAmount,0) > 0 AND ISNULL(OpportunityBizDocs.monAmountPaid,0) >= ISNULL(OpportunityBizDocs.monDealAmount,0)
																																															THEN dbo.FormatedDateFromDate((SELECT MAX(BPH.dtPaymentDate) FROM BillPaymentDetails BPD INNER JOIN BillPaymentHeader BPH ON BPD.numBillPaymentID=BPH.numBillPaymentID  WHERE BPD.numOppBizDocsID=OpportunityBizDocs.numOppBizDocsId),@numDomainID)
																																															WHEN ISNULL(OpportunityMaster.tintOppType,0) = 1 AND ISNULL(OpportunityBizDocs.monDealAmount,0) > 0 AND ISNULL(OpportunityBizDocs.monAmountPaid,0) >= ISNULL(OpportunityBizDocs.monDealAmount,0)
																																															THEN dbo.FormatedDateFromDate((SELECT MAX(dtCreatedDate) FROM DepositeDetails WHERE DepositeDetails.numOppBizDocsID=OpportunityBizDocs.numOppBizDocsId),@numDomainID)
																																															ELSE ''''
																																														END)')
	END

	--KEEP IT BELOW ABOVE REPLACEMENT
	IF CHARINDEX('OpportunityBizDocs.dtFullPaymentDate',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityBizDocs.dtFullPaymentDate' ,'CAST((CASE 
																												WHEN ISNULL(OpportunityMaster.tintOppType,0) = 2 AND ISNULL(OpportunityBizDocs.monDealAmount,0) > 0 AND ISNULL(OpportunityBizDocs.monAmountPaid,0) >= ISNULL(OpportunityBizDocs.monDealAmount,0)
																												THEN (SELECT MAX(BPH.dtPaymentDate) FROM BillPaymentDetails BPD INNER JOIN BillPaymentHeader BPH ON BPD.numBillPaymentID=BPH.numBillPaymentID  WHERE BPD.numOppBizDocsID=OpportunityBizDocs.numOppBizDocsId)
																												WHEN ISNULL(OpportunityMaster.tintOppType,0) = 1 AND ISNULL(OpportunityBizDocs.monDealAmount,0) > 0 AND ISNULL(OpportunityBizDocs.monAmountPaid,0) >= ISNULL(OpportunityBizDocs.monDealAmount,0)
																												THEN (SELECT MAX(dtCreatedDate) FROM DepositeDetails WHERE DepositeDetails.numOppBizDocsID=OpportunityBizDocs.numOppBizDocsId)
																												ELSE ''''
																											END) AS DATE)')
	END

	IF CHARINDEX('isnull(OpportunityMaster.monProfit,''0'')',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityMaster.monProfit,''0'')' ,CONCAT('CAST(ISNULL(OpportunityItems.monTotAmount,0) - (ISNULL(OpportunityItems.numUnitHour,0) * (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityItems.monAvgCost,0) END)) AS DECIMAL(20,5))'))
	END

	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityMaster.monProfit',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityMaster.monProfit' ,CONCAT('CAST(ISNULL(OpportunityItems.monTotAmount,0) - (ISNULL(OpportunityItems.numUnitHour,0) * (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityItems.monAvgCost,0) END)) AS DECIMAL(20,5))'))
	END

	IF CHARINDEX('isnull(OpportunityBizDocItems.monProfit,''0'')',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityBizDocItems.monProfit,''0'')' ,CONCAT('CAST(ISNULL(OpportunityBizDocItems.monTotAmount,0) - (ISNULL(OpportunityBizDocItems.numUnitHour,0) * (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityBizDocItems.monAverageCost,ISNULL(OpportunityItems.monAvgCost,0)) END)) AS DECIMAL(20,5))'))
	END

	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityBizDocItems.monProfit',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityBizDocItems.monProfit' ,CONCAT('CAST(ISNULL(OpportunityBizDocItems.monTotAmount,0) - (ISNULL(OpportunityBizDocItems.numUnitHour,0) * (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityBizDocItems.monAverageCost,ISNULL(OpportunityItems.monAvgCost,0)) END)) AS DECIMAL(20,5))'))
	END

	IF CHARINDEX('ISNULL(PT.numItemCode,0)=Item.numItemCode',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'ISNULL(PT.numItemCode,0)=Item.numItemCode' ,'Item.numItemCode=PT.numItemCode')
	END

	IF CHARINDEX('isnull(OpportunityMaster.numProfitPercent,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityMaster.numProfitPercent,0)' ,CONCAT('CAST((((ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1)) - (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityItems.monAvgCost,0) END)) / (CASE WHEN (ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1))=0 then 1 ELSE (ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1)) end) * 100) AS NUMERIC(18,2))'))
	END
	
	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityMaster.numProfitPercent',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityMaster.numProfitPercent' ,CONCAT('CAST((((ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1)) - (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityItems.monAvgCost,0) END)) / (CASE WHEN (ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1))=0 then 1 ELSE (ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1)) end) * 100) AS NUMERIC(18,2))'))
	END

	IF CHARINDEX('isnull(OpportunityBizDocItems.numProfitPercent,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityBizDocItems.numProfitPercent,0)' ,CONCAT('CAST((((ISNULL(OpportunityBizDocItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityBizDocItems.numUnitHour,0),1)) - (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityBizDocItems.monAverageCost,ISNULL(OpportunityItems.monAvgCost,0)) END)) / (CASE WHEN (ISNULL(OpportunityBizDocItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityBizDocItems.numUnitHour,0),1))=0 then 1 ELSE (ISNULL(OpportunityBizDocItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityBizDocItems.numUnitHour,0),1)) end) * 100) AS NUMERIC(18,2))'))
	END
	
	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityBizDocItems.numProfitPercent',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityBizDocItems.numProfitPercent' ,CONCAT('CAST((((ISNULL(OpportunityBizDocItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityBizDocItems.numUnitHour,0),1)) - (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityBizDocItems.monAverageCost,ISNULL(OpportunityItems.monAvgCost,0)) END)) / (CASE WHEN (ISNULL(OpportunityBizDocItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityBizDocItems.numUnitHour,0),1))=0 then 1 ELSE (ISNULL(OpportunityBizDocItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityBizDocItems.numUnitHour,0),1)) end) * 100) AS NUMERIC(18,2))'))
	END

	IF ISNULL(@numCurrentPage,0) > 0 
		AND ISNULL(@tintReportType,0) <> 3 
		AND ISNULL(@tintReportType,0) <> 4 
		AND ISNULL(@tintReportType,0) <> 5 
		AND CHARINDEX('Select top',CAST(@textQuery AS VARCHAR(MAX))) <> 1
		AND (SELECT COUNT(*) FROM ReportSummaryGroupList WHERE numReportID=@numReportID) = 0
	BEGIN
		SET @textQuery = CONCAT(CAST(@textQuery AS VARCHAR(MAX)),' OFFSET ',(@numCurrentPage-1) * 100 ,' ROWS FETCH NEXT 100 ROWS ONLY OPTION (OPTIMIZE FOR (@numDomainID UNKNOWN));')
	END
	ELSE
	BEGIN
		SET @textQuery = CONCAT(CAST(@textQuery AS VARCHAR(MAX)),' OPTION (OPTIMIZE FOR (@numDomainID UNKNOWN));')
	END
	 
	 print CAST(@textQuery AS ntext)
	EXECUTE sp_executesql @textQuery,N'@numDomainID numeric(18, 0),@numUserCntID numeric(18,0),@ClientTimeZoneOffset int',@numDomainID=@numDomainID,@numUserCntID=@numUserCntID,@ClientTimeZoneOffset=@ClientTimeZoneOffset
END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_SalesOrderConfiguration_Save' ) 
    DROP PROCEDURE USP_SalesOrderConfiguration_Save
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 18 Feb 2014
-- Description:	Saves new sales order form configuration
-- =============================================
CREATE PROCEDURE USP_SalesOrderConfiguration_Save
	@numSOCID NUMERIC(18,0) = NULL,
	@numDomainID NUMERIC(18,0) = NULL,
	@bitAutoFocusCustomer BIT,
	@bitAutoFocusItem BIT,
	@bitDisplayRootLocation BIT,
	@bitAutoAssignOrder BIT,
	@bitDisplayLocation BIT,
	@bitDisplayFinancialStamp BIT,
	@bitDisplayItemDetails BIT,
	@bitDisplayUnitCost BIT,
	@bitDisplayProfitTotal BIT,
	@bitDisplayShippingRates BIT,
	@bitDisplayPaymentMethods BIT,
	@bitCreateOpenBizDoc BIT,
	@numListItemID NUMERIC(18,0),
	@vcPaymentMethodIDs VARCHAR(100),
	@bitDisplayCurrency BIT,
	@bitDisplayAssignTo BIT,
	@bitDisplayTemplate BIT,
	@bitDisplayCouponDiscount BIT,
	@bitDisplayShippingCharges BIT,
	@bitDisplayDiscount BIT,
	@bitDisplaySaveNew BIT,
	@bitDisplayAddToCart BIT,
	@bitDisplayCreateInvoice BIT,
	@bitDisplayShipVia BIT,
	@bitDisplayComments BIT,
	@bitDisplayReleaseDate BIT,
	@bitDisplayCustomerPart#Entry BIT,
	@bitDisplayItemGridOrderAZ BIT,
	@bitDisplayItemGridItemID BIT,
	@bitDisplayItemGridSKU BIT,
	@bitDisplayItemGridUnitListPrice BIT,
	@bitDisplayItemGridUnitSalePrice BIT,
	@bitDisplayItemGridDiscount BIT,
	@bitDisplayItemGridItemRelaseDate BIT,
	@bitDisplayItemGridLocation BIT,
	@bitDisplayItemGridShipTo BIT,
	@bitDisplayItemGridOnHandAllocation BIT,
	@bitDisplayItemGridDescription BIT,
	@bitDisplayItemGridNotes BIT,
	@bitDisplayItemGridAttributes BIT,
	@bitDisplayItemGridInclusionDetail BIT,
	@bitDisplayItemGridItemClassification BIT,
	@bitDisplayExpectedDate BIT,
	@bitDisplayItemGridItemPromotion BIT,
	@bitDisplayApplyPromotionCode BIT,
	@bitDisplayPayButton BIT,
	@bitDisplayPOSPay BIT,
	@vcPOHiddenColumns VARCHAR(200)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF (SELECT COUNT(*) FROM SalesOrderConfiguration WHERE numDomainID = @numDomainID) = 0
	BEGIN
		INSERT INTO dbo.SalesOrderConfiguration
		(
			numDomainID,
			bitAutoFocusCustomer,
			bitAutoFocusItem,
			bitDisplayRootLocation,
			bitAutoAssignOrder,
			bitDisplayLocation,
			bitDisplayFinancialStamp,
			bitDisplayItemDetails,
			bitDisplayUnitCost,
			bitDisplayProfitTotal,
			bitDisplayShippingRates,
			bitDisplayPaymentMethods,
			bitCreateOpenBizDoc,
			numListItemID,
			vcPaymentMethodIDs,
			bitDisplayCurrency,
			bitDisplayAssignTo,
			bitDisplayTemplate,
			bitDisplayCouponDiscount,
			bitDisplayShippingCharges,
			bitDisplayDiscount,
			bitDisplaySaveNew,
			bitDisplayAddToCart,
			bitDisplayCreateInvoice,
			bitDisplayShipVia,
			bitDisplayComments,
			bitDisplayReleaseDate,
			bitDisplayCustomerPart#Entry,
			bitDisplayItemGridOrderAZ,
			bitDisplayItemGridItemID,
			bitDisplayItemGridSKU,
			bitDisplayItemGridUnitListPrice,
			bitDisplayItemGridUnitSalePrice,
			bitDisplayItemGridDiscount,
			bitDisplayItemGridItemRelaseDate,
			bitDisplayItemGridLocation,
			bitDisplayItemGridShipTo,
			bitDisplayItemGridOnHandAllocation,
			bitDisplayItemGridDescription,
			bitDisplayItemGridNotes,
			bitDisplayItemGridAttributes,
			bitDisplayItemGridInclusionDetail,
			bitDisplayItemGridItemClassification,
			bitDisplayExpectedDate,
			bitDisplayItemGridItemPromotion,
			bitDisplayApplyPromotionCode,
			bitDisplayPayButton,
			bitDisplayPOSPay,
			vcPOHiddenColumns
			)
		VALUES
		(
			@numDomainID,
			@bitAutoFocusCustomer,
			@bitAutoFocusItem,
			@bitDisplayRootLocation,
			@bitAutoAssignOrder,
			@bitDisplayLocation,
			@bitDisplayFinancialStamp,
			@bitDisplayItemDetails,
			@bitDisplayUnitCost,
			@bitDisplayProfitTotal,
			@bitDisplayShippingRates,
			@bitDisplayPaymentMethods,
			@bitCreateOpenBizDoc,
			@numListItemID,
			@vcPaymentMethodIDs,
			@bitDisplayCurrency,
			@bitDisplayAssignTo,
			@bitDisplayTemplate,
			@bitDisplayCouponDiscount,
			@bitDisplayShippingCharges,
			@bitDisplayDiscount,
			@bitDisplaySaveNew,
			@bitDisplayAddToCart,
			@bitDisplayCreateInvoice,
			@bitDisplayShipVia,
			@bitDisplayComments,
			@bitDisplayReleaseDate,
			@bitDisplayCustomerPart#Entry,
			@bitDisplayItemGridOrderAZ,
			@bitDisplayItemGridItemID,
			@bitDisplayItemGridSKU,
			@bitDisplayItemGridUnitListPrice,
			@bitDisplayItemGridUnitSalePrice,
			@bitDisplayItemGridDiscount,
			@bitDisplayItemGridItemRelaseDate,
			@bitDisplayItemGridLocation,
			@bitDisplayItemGridShipTo,
			@bitDisplayItemGridOnHandAllocation,
			@bitDisplayItemGridDescription,
			@bitDisplayItemGridNotes,
			@bitDisplayItemGridAttributes,
			@bitDisplayItemGridInclusionDetail,
			@bitDisplayItemGridItemClassification
			,@bitDisplayExpectedDate
			,@bitDisplayItemGridItemPromotion
			,@bitDisplayApplyPromotionCode
			,@bitDisplayPayButton
			,@bitDisplayPOSPay
			,@vcPOHiddenColumns
		)
	END
	ELSE
	BEGIN
		UPDATE
			dbo.SalesOrderConfiguration
		SET
			bitAutoFocusCustomer = @bitAutoFocusCustomer,
			bitAutoFocusItem = @bitAutoFocusItem,
			bitDisplayRootLocation = @bitDisplayRootLocation,
			bitAutoAssignOrder = @bitAutoAssignOrder,
			bitDisplayLocation = @bitDisplayLocation,
			bitDisplayFinancialStamp = @bitDisplayFinancialStamp,
			bitDisplayItemDetails = @bitDisplayItemDetails,
			bitDisplayUnitCost = @bitDisplayUnitCost,
			bitDisplayProfitTotal = @bitDisplayProfitTotal,
			bitDisplayShippingRates = @bitDisplayShippingRates,
			bitDisplayPaymentMethods = @bitDisplayPaymentMethods,
			bitCreateOpenBizDoc = @bitCreateOpenBizDoc,
			numListItemID = @numListItemID,
			vcPaymentMethodIDs  = @vcPaymentMethodIDs,
			bitDisplayCurrency = @bitDisplayCurrency,
			bitDisplayAssignTo = @bitDisplayAssignTo,
			bitDisplayTemplate = @bitDisplayTemplate,
			bitDisplayCouponDiscount = @bitDisplayCouponDiscount,
			bitDisplayShippingCharges = @bitDisplayShippingCharges,
			bitDisplayDiscount = @bitDisplayDiscount,
			bitDisplaySaveNew = @bitDisplaySaveNew,
			bitDisplayAddToCart = @bitDisplayAddToCart,
			bitDisplayCreateInvoice = @bitDisplayCreateInvoice,
			bitDisplayShipVia = @bitDisplayShipVia,
			bitDisplayComments = @bitDisplayComments,
			bitDisplayReleaseDate = @bitDisplayReleaseDate,
			bitDisplayCustomerPart#Entry = @bitDisplayCustomerPart#Entry,
			bitDisplayItemGridOrderAZ = @bitDisplayItemGridOrderAZ,
			bitDisplayItemGridItemID = @bitDisplayItemGridItemID,
			bitDisplayItemGridSKU = @bitDisplayItemGridSKU,
			bitDisplayItemGridUnitListPrice = @bitDisplayItemGridUnitListPrice,
			bitDisplayItemGridUnitSalePrice = @bitDisplayItemGridUnitSalePrice,
			bitDisplayItemGridDiscount = @bitDisplayItemGridDiscount,
			bitDisplayItemGridItemRelaseDate = @bitDisplayItemGridItemRelaseDate,
			bitDisplayItemGridLocation = @bitDisplayItemGridLocation,
			bitDisplayItemGridShipTo = @bitDisplayItemGridShipTo,
			bitDisplayItemGridOnHandAllocation = @bitDisplayItemGridOnHandAllocation,
			bitDisplayItemGridDescription = @bitDisplayItemGridDescription,
			bitDisplayItemGridNotes = @bitDisplayItemGridNotes,
			bitDisplayItemGridAttributes = @bitDisplayItemGridAttributes,
			bitDisplayItemGridInclusionDetail = @bitDisplayItemGridInclusionDetail,
			bitDisplayItemGridItemClassification = @bitDisplayItemGridItemClassification,
			bitDisplayExpectedDate=@bitDisplayExpectedDate
			,bitDisplayItemGridItemPromotion=@bitDisplayItemGridItemPromotion
			,bitDisplayApplyPromotionCode=@bitDisplayApplyPromotionCode
			,bitDisplayPayButton=@bitDisplayPayButton
			,bitDisplayPOSPay=@bitDisplayPOSPay
			,vcPOHiddenColumns=@vcPOHiddenColumns
		WHERE
			numDomainID = @numDomainID
	END
    
END
GO


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SelectCommunicationAttendees')
DROP PROCEDURE USP_SelectCommunicationAttendees
GO
CREATE PROCEDURE [dbo].[USP_SelectCommunicationAttendees]
@numCommId AS numeric(9)=0
AS
BEGIN
 SELECT ISNULL(vcFirstName,'')+' '+ISNULL(vcLastName,'') as vcContact,CA.numContactID,CI.vcCompanyName AS vcCompanyname,
  CASE WHEN UM.numUserDetailId IS NULL THEN 2 ELSE 1 END AS tinUserType,CA.ActivityID,AC.vcEmail as Email,
  dbo.GetListIemName(CA.numStatus) vcStatus
 FROM CommunicationAttendees CA LEFT JOIN AdditionalContactsInformation AC ON CA.numContactID=AC.numContactID  
 LEFT JOIN divisionmaster DM on DM.numDivisionID=AC.numDivisionID 
 LEFT JOIN CompanyInfo CI on DM.numCompanyId=CI.numCompanyId 
 LEFT JOIN UserMaster UM  ON UM.numUserDetailId=CA.numContactID
 WHERE CA.numCommId=@numCommId
END
/****** Object:  StoredProcedure [dbo].[usp_SetUsersWithDomains]    Script Date: 07/26/2008 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_setuserswithdomains')
DROP PROCEDURE usp_setuserswithdomains
GO
CREATE  PROCEDURE [dbo].[usp_SetUsersWithDomains]
	@numUserID NUMERIC(9),                                    
	@vcUserName VARCHAR(50),                                    
	@vcUserDesc VARCHAR(250),                              
	@numGroupId as numeric(9),                                           
	@numUserDetailID numeric ,                              
	@numUserCntID as numeric(9),                              
	@strTerritory as varchar(4000) ,                              
	@numDomainID as numeric(9),
	@strTeam as varchar(4000),
	@vcEmail as varchar(100),
	@vcPassword as varchar(100),          
	@Active as BIT,
	@numDefaultClass NUMERIC,
	@numDefaultWarehouse as numeric(18),
	@vcLinkedinId varchar(300)=null,
	@intAssociate int=0,
	@ProfilePic varchar(100)=null
	,@bitPayroll BIT = 0
	,@monHourlyRate DECIMAL(20,5) = 0
	,@tintPayrollType TINYINT = 0
	,@tintHourType TINYINT = 0
	,@monOverTimeRate DECIMAL(20,5) = 0
	,@bitOauthImap BIT = 0
AS
BEGIN
	DECLARE @numNewFullUsers AS INT
	DECLARE @numNewLimitedAccessUsers AS INT

	SET @numNewFullUsers = ISNULL((SELECT
										COUNT(*)
									FROM
										UserMaster 
									INNER JOIN
										AuthenticationGroupMaster
									ON
										UserMaster.numGroupID = AuthenticationGroupMaster.numGroupID
									WHERE
										UserMaster.numDomainID = @numDomainID
										AND AuthenticationGroupMaster.tintGroupType=1
										AND ISNULL(UserMaster.bitActivateFlag,0)=1
										AND UserMaster.numUserId <> ISNULL(@numUserID,0)),0)

	SET @numNewLimitedAccessUsers = ISNULL((SELECT
										COUNT(*)
									FROM
										UserMaster 
									INNER JOIN
										AuthenticationGroupMaster
									ON
										UserMaster.numGroupID = AuthenticationGroupMaster.numGroupID
									WHERE
										UserMaster.numDomainID = @numDomainID
										AND AuthenticationGroupMaster.tintGroupType=4
										AND ISNULL(UserMaster.bitActivateFlag,0)=1
										AND UserMaster.numUserId <> ISNULL(@numUserID,0)),0)
		
	IF ISNULL(@Active,0)=1 AND ISNULL((SELECT tintGroupType FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId),0)=1 AND EXISTS (SELECT numSubscriberID FROM Subscribers WHERE numTargetDomainID=@numDomainID AND (@numNewFullUsers + 1) > ISNULL(intNoofUsersSubscribed,0))
	BEGIN
		RAISERROR('FULL_USERS_EXCEED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@Active,0)=1 AND ISNULL((SELECT tintGroupType FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId),0)=4 AND EXISTS (SELECT numSubscriberID FROM Subscribers WHERE numTargetDomainID=@numDomainID AND (@numNewLimitedAccessUsers + 1) > ISNULL(intNoofLimitedAccessUsers,0))
	BEGIN
		RAISERROR('LIMITED_ACCESS_USERS_EXCEED',16,1)
		RETURN
	END

	IF @numUserID=0             
	BEGIN 
		DECLARE @APIPublicKey varchar(20)
		SELECT @APIPublicKey = dbo.GenerateBizAPIPublicKey() 
           
		INSERT INTO UserMaster
		(
			vcUserName
			,vcUserDesc
			,numGroupId
			,numUserDetailId
			,numModifiedBy
			,bintModifiedDate
			,vcEmailID
			,vcPassword
			,numDomainID
			,numCreatedBy
			,bintCreatedDate
			,bitActivateFlag
			,numDefaultClass
			,numDefaultWarehouse
			,vcBizAPIPublicKey
			,vcBizAPISecretKey
			,bitBizAPIAccessEnabled
			,vcLinkedinId
			,intAssociate
			,bitPayroll
			,monHourlyRate
			,tintPayrollType
			,tintHourType
			,monOverTimeRate
			,bitOauthImap
		)
		VALUES
		(
			@vcUserName
			,@vcUserDesc
			,@numGroupId
			,@numUserDetailID
			,@numUserCntID
			,GETUTCDATE()
			,@vcEmail
			,@vcPassword
			,@numDomainID
			,@numUserCntID
			,GETUTCDATE()
			,@Active
			,@numDefaultClass
			,@numDefaultWarehouse
			,@APIPublicKey
			,NEWID()
			,0
			,@vcLinkedinId
			,@intAssociate
			,@bitPayroll
			,@monHourlyRate
			,@tintPayrollType
			,@tintHourType
			,(CASE WHEN ISNULL(@monOverTimeRate,0) = 0 THEN @monHourlyRate ELSE @monOverTimeRate END)
			,@bitOauthImap
		)            
		
		SET @numUserID=@@identity          
           
		INSERT INTO BizAPIThrottlePolicy 
		(
			[RateLimitKey],
			[bitIsIPAddress],
			[numPerSecond],
			[numPerMinute],
			[numPerHour],
			[numPerDay],
			[numPerWeek]
		)
		VALUES
		(
			@APIPublicKey,
			0,
			3,
			60,
			1200,
			28800,
			200000
		)
    
		EXECUTE [dbo].[Resource_Add]   @vcUserName  ,@vcUserDesc  ,@vcEmail  ,1  ,@numDomainID  ,@numUserDetailID  
	END
	ELSE            
	BEGIN
		UPDATE 
			UserMaster 
		SET 
			vcUserName = @vcUserName,                                    
			vcUserDesc = @vcUserDesc,                              
			numGroupId =@numGroupId,                                    
			numUserDetailId = @numUserDetailID ,                              
			numModifiedBy= @numUserCntID ,                              
			bintModifiedDate= getutcdate(),                          
			vcEmailID=@vcEmail,            
			vcPassword=@vcPassword,          
			bitActivateFlag=@Active,
			numDefaultClass=@numDefaultClass,
			numDefaultWarehouse=@numDefaultWarehouse,
			vcLinkedinId=@vcLinkedinId,
			intAssociate=@intAssociate
			,bitPayroll=@bitPayroll
			,monHourlyRate=@monHourlyRate
			,tintPayrollType=@tintPayrollType
			,tintHourType=@tintHourType
			,monOverTimeRate=(CASE WHEN ISNULL(@monOverTimeRate,0) = 0 THEN @monHourlyRate ELSE @monOverTimeRate END)
			,bitOauthImap=@bitOauthImap
		WHERE 
			numUserID = @numUserID          
          
  
		IF NOT EXISTS (SELECT * FROM RESOURCE WHERE numUserCntId = @numUserDetailID and numdomainId = @numDomainId)  
		BEGIN   
			EXECUTE [dbo].[Resource_Add]   @vcUserName  ,@vcUserDesc  ,@vcEmail  ,1  ,@numDomainID  ,@numUserDetailID     
		END      
	END
                       
	DECLARE @separator_position AS INTEGER
	DECLARE @strPosition AS VARCHAR(1000)                 
                                   
	DELETE FROM UserTerritory WHERE numUserCntID = @numUserDetailID and numDomainId=@numDomainID                              
          
	WHILE PATINDEX('%,%' , @strTerritory) <> 0                              
    BEGIN -- Begin for While Loop                              
		SELECT @separator_position = PATINDEX('%,%' , @strTerritory)                        
		SELECT @strPosition = LEFT(@strTerritory, @separator_position - 1)      
		SELECT @strTerritory = STUFF(@strTerritory, 1, @separator_position,'')                              
     
		INSERT INTO UserTerritory 
		(
			numUserCntID
			,numTerritoryID
			,numDomainID
		)                              
		VALUES
		(
			@numUserDetailID
			,@strPosition
			,@numDomainID
		)                        
	END                              
                                           
	DELETE FROM UserTeams WHERE numUserCntID = @numUserDetailID AND numDomainId=@numDomainID                              
                               
	WHILE PATINDEX('%,%' , @strTeam) <> 0                              
    BEGIN -- Begin for While Loop                              
		SELECT @separator_position =  PATINDEX('%,%' , @strTeam)                              
		SELECT @strPosition = left(@strTeam, @separator_position - 1)                  
		SELECT @strTeam = stuff(@strTeam, 1, @separator_position,'')                              
     
		INSERT INTO UserTeams 
		(
			numUserCntID
			,numTeam
			,numDomainID
		)                              
		VALUES 
		(
			@numUserDetailID
			,@strPosition
			,@numDomainID
		)                        
	END              
                
                                      
                              
	DELETE FROM 
		ForReportsByTeam 
	WHERE 
		numUserCntID = @numUserDetailID                              
		AND numDomainId=@numDomainID AND numTeam NOT IN (SELECT numTeam FROM UserTeams WHERE numUserCntID = @numUserCntID AND numDomainId=@numDomainID)                                                
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
       
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Subscribers_GetSubscribtionCount')
DROP PROCEDURE USP_Subscribers_GetSubscribtionCount
GO
CREATE PROCEDURE [dbo].[USP_Subscribers_GetSubscribtionCount]
(       
	@numDomainID NUMERIC(18,0)
)
AS
BEGIN
	SELECT 
		CONCAT(ISNULL((SELECT COUNT(*) FROM UserMaster LEFT JOIN AuthenticationGroupMaster ON UserMaster.numGroupID=AuthenticationGroupMaster.numGroupID WHERE UserMaster.numDomainID=@numDomainID AND ISNULL(bitActivateFlag,0)=1 AND (AuthenticationGroupMaster.tintGroupType=1 OR AuthenticationGroupMaster.tintGroupType IS NULL)),0),' used / ',ISNULL(intNoofUsersSubscribed,0) - ISNULL((SELECT COUNT(*) FROM UserMaster LEFT JOIN AuthenticationGroupMaster ON UserMaster.numGroupID=AuthenticationGroupMaster.numGroupID WHERE UserMaster.numDomainID=@numDomainID AND ISNULL(bitActivateFlag,0)=1 AND (AuthenticationGroupMaster.tintGroupType=1 OR AuthenticationGroupMaster.tintGroupType IS NULL)),0),' remaining') vcFullUsers
		,CONCAT(ISNULL((SELECT COUNT(*) FROM UserMaster LEFT JOIN AuthenticationGroupMaster ON UserMaster.numGroupID=AuthenticationGroupMaster.numGroupID WHERE UserMaster.numDomainID=@numDomainID AND ISNULL(bitActivateFlag,0)=1 AND AuthenticationGroupMaster.tintGroupType=4),0),' used / ',ISNULL(intNoofLimitedAccessUsers,0) - ISNULL((SELECT COUNT(*) FROM UserMaster LEFT JOIN AuthenticationGroupMaster ON UserMaster.numGroupID=AuthenticationGroupMaster.numGroupID WHERE UserMaster.numDomainID=@numDomainID AND ISNULL(bitActivateFlag,0)=1 AND AuthenticationGroupMaster.tintGroupType=4),0),' remaining') vcLimitedAccessUsers
		,CONCAT(ISNULL((SELECT COUNT(*) FROM ExtranetAccountsDtl WHERE numDomainID=@numDomainID AND ISNULL(bitPartnerAccess,0)=1),0),' used / ',ISNULL(intNoofBusinessPortalUsers,0) - ISNULL((SELECT COUNT(*) FROM ExtranetAccountsDtl WHERE numDomainID=@numDomainID AND ISNULL(bitPartnerAccess,0)=1),0),' remaining') vcBusinessPortalUsers
	FROM
		Subscribers
	WHERE
		numTargetDomainID=@numDomainID
END
GO
/****** Object:  StoredProcedure [dbo].[USP_UpdatedomainDetails]    Script Date: 05/07/2009 21:56:59 ******/
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatedomaindetails')
DROP PROCEDURE usp_updatedomaindetails
GO
CREATE PROCEDURE [dbo].[USP_UpdatedomainDetails]                                      
@numDomainID as numeric(9)=0,                                      
@vcDomainName as varchar(50)='',                                      
@vcDomainDesc as varchar(100)='',                                      
@bitExchangeIntegration as bit,                                      
@bitAccessExchange as bit,                                      
@vcExchUserName as varchar(100)='',                                      
@vcExchPassword as varchar(100)='',                                      
@vcExchPath as varchar(200)='',                                      
@vcExchDomain as varchar(100)='',                                    
@tintCustomPagingRows as tinyint,                                    
@vcDateFormat as varchar(30)='',                                    
@numDefCountry as numeric(9),                                    
@tintComposeWindow as tinyint,                                    
@sintStartDate as smallint,                                    
@sintNoofDaysInterval as smallint,                                    
@tintAssignToCriteria as tinyint,                                    
@bitIntmedPage as bit,                                    
@tintFiscalStartMonth as tinyint,                                      
@tintPayPeriod as tinyint,                        
@numCurrencyID as numeric(9) ,                
@charUnitSystem as char(1)='',              
@vcPortalLogo as varchar(100)='',            
@tintChrForComSearch as tinyint  ,            
@intPaymentGateWay as int, -- DO NOT UPDATE VALUE FROM THIS PARAMETER
@tintChrForItemSearch as tinyint,
@ShipCompany as numeric(9),
@bitMultiCurrency as bit,
@bitCreateInvoice AS tinyint = null,
@numDefaultSalesBizDocID AS NUMERIC(9)=NULL,
@numDefaultPurchaseBizDocID AS NUMERIC(9)=NULL,
@bitSaveCreditCardInfo AS BIT = NULL,
@intLastViewedRecord AS INT=10,
@tintCommissionType AS TINYINT,
@tintBaseTax AS TINYINT,
@bitEmbeddedCost AS BIT,
@numDefaultSalesOppBizDocId NUMERIC=NULL,
@tintSessionTimeOut TINYINT=1,
@tintDecimalPoints TINYINT,
@bitCustomizePortal BIT=0,
@tintBillToForPO TINYINT,
@tintShipToForPO TINYINT,
@tintOrganizationSearchCriteria TINYINT,
@bitDocumentRepositary BIT=0,
@tintComAppliesTo TINYINT=0,
@bitGtoBContact BIT=0,
@bitBtoGContact BIT=0,
@bitGtoBCalendar BIT=0,
@bitBtoGCalendar BIT=0,
@bitExpenseNonInventoryItem BIT=0,
@bitInlineEdit BIT=0,
@bitRemoveVendorPOValidation BIT=0,
@tintBaseTaxOnArea tinyint=0,
@bitAmountPastDue BIT=0,
@monAmountPastDue DECIMAL(20,5),
@bitSearchOrderCustomerHistory BIT=0,
@tintCalendarTimeFormat as tinyint=NULL,
@tintDefaultClassType as tinyint=NULL,
@tintPriceBookDiscount as tinyint=0,
@bitAutoSerialNoAssign as bit=0,
@bitIsShowBalance AS BIT = 0,
@numIncomeAccID NUMERIC(18, 0) = 0,
@numCOGSAccID NUMERIC(18, 0) = 0,
@numAssetAccID NUMERIC(18, 0) = 0,
@IsEnableProjectTracking AS BIT = 0,
@IsEnableCampaignTracking AS BIT = 0,
@IsEnableResourceScheduling AS BIT = 0,
@ShippingServiceItem  AS NUMERIC(9)=0,
@numSOBizDocStatus AS NUMERIC(9)=0,
@bitAutolinkUnappliedPayment AS BIT=0,
@numDiscountServiceItemID AS NUMERIC(9)=0,
@vcShipToPhoneNumber as VARCHAR(50)='',
@vcGAUserEmailId as VARCHAR(50)='',
@vcGAUserPassword as VARCHAR(50)='',
@vcGAUserProfileId as VARCHAR(50)='',
@bitDiscountOnUnitPrice AS BIT=0,
@intShippingImageWidth INT = 0,
@intShippingImageHeight INT = 0,
@numTotalInsuredValue NUMERIC(10,2) = 0,
@numTotalCustomsValue NUMERIC(10,2) = 0,
@vcHideTabs VARCHAR(1000) = '',
@bitUseBizdocAmount BIT = 0,
@bitDefaultRateType BIT = 0,
@bitIncludeTaxAndShippingInCommission  BIT = 0,
@bitPurchaseTaxCredit BIT=0,
@bitLandedCost BIT=0,
@vcLanedCostDefault VARCHAR(10)='',
@bitMinUnitPriceRule BIT = 0,
@numAbovePercent AS NUMERIC(18,2),
@numBelowPercent AS NUMERIC(18,2),
@numBelowPriceField AS NUMERIC(18,0),
@vcUnitPriceApprover AS VARCHAR(100),
@numDefaultSalesPricing AS TINYINT,
@bitReOrderPoint AS BIT = 0,
@numReOrderPointOrderStatus AS NUMERIC(18,0) = 0,
@numPODropShipBizDoc AS NUMERIC(18,0) = 0,
@numPODropShipBizDocTemplate AS NUMERIC(18,0) = 0,
@IsEnableDeferredIncome AS BIT = 0,
@bitApprovalforTImeExpense AS BIT=0,
@numCost AS Numeric(9,0)=0,
@intTimeExpApprovalProcess int=0,
@bitApprovalforOpportunity AS BIT=0,
@intOpportunityApprovalProcess int=0,
@numDefaultSalesShippingDoc NUMERIC(18,0)=0,
@numDefaultPurchaseShippingDoc NUMERIC(18,0)=0,
@bitchkOverRideAssignto AS BIT=0,
@vcPrinterIPAddress VARCHAR(15) = '',
@vcPrinterPort VARCHAR(5) = '',
@numDefaultSiteID NUMERIC(18,0) = 0,
@bitRemoveGlobalLocation BIT = 0,
@numAuthorizePercentage NUMERIC(18,0)=0,
@bitEDI BIT = 0,
@bit3PL BIT = 0,
@bitAllowDuplicateLineItems BIT = 0,
@tintCommitAllocation TINYINT = 1,
@tintInvoicing TINYINT = 1,
@tintMarkupDiscountOption TINYINT = 1,
@tintMarkupDiscountValue TINYINT = 1,
@bitCommissionBasedOn BIT = 0,
@tintCommissionBasedOn TINYINT = 1,
@bitDoNotShowDropshipPOWindow BIT = 0,
@tintReceivePaymentTo TINYINT = 2,
@numReceivePaymentBankAccount NUMERIC(18,0) = 0,
@numOverheadServiceItemID NUMERIC(18,0) = 0,
@bitDisplayCustomField BIT = 0,
@bitFollowupAnytime BIT = 0,
@bitpartycalendarTitle BIT = 0,
@bitpartycalendarLocation BIT = 0,
@bitpartycalendarDescription BIT = 0,
@bitREQPOApproval BIT = 0,
@bitARInvoiceDue BIT = 0,
@bitAPBillsDue BIT = 0,
@bitItemsToPickPackShip BIT = 0,
@bitItemsToInvoice BIT = 0,
@bitSalesOrderToClose BIT = 0,
@bitItemsToPutAway BIT = 0,
@bitItemsToBill BIT = 0,
@bitPosToClose BIT = 0,
@bitPOToClose BIT = 0,
@bitBOMSToPick BIT = 0,
@vchREQPOApprovalEmp  VARCHAR(500)='',
@vchARInvoiceDue VARCHAR(500)='',
@vchAPBillsDue VARCHAR(500)='',
@vchItemsToPickPackShip VARCHAR(500)='',
@vchItemsToInvoice VARCHAR(500)='',
@vchPriceMarginApproval VARCHAR(500)='',
@vchSalesOrderToClose VARCHAR(500)='',
@vchItemsToPutAway VARCHAR(500)='',
@vchItemsToBill VARCHAR(500)='',
@vchPosToClose VARCHAR(500)='',
@vchPOToClose VARCHAR(500)='',
@vchBOMSToPick VARCHAR(500)='',
@decReqPOMinValue VARCHAR(500)='',
@bitUseOnlyActionItems BIT = 0,
@bitDisplayContractElement BIT,
@vcEmployeeForContractTimeElement VARCHAR(500)='',
@numARContactPosition NUMERIC(18,0) = 0,
@bitShowCardConnectLink BIT = 0,
@vcBluePayFormName VARCHAR(500)='',
@vcBluePaySuccessURL VARCHAR(500)='',
@vcBluePayDeclineURL VARCHAR(500)='',
@bitUseDeluxeCheckStock BIT = 1
as                                      
BEGIN
BEGIN TRY
	IF ISNULL(@bitMinUnitPriceRule,0) = 1 AND ((SELECT COUNT(*) FROM UnitPriceApprover WHERE numDomainID=@numDomainID) = 0 OR ISNULL(@intOpportunityApprovalProcess,0) = 0)
	BEGIN
		RAISERROR('INCOMPLETE_MIN_UNIT_PRICE_CONFIGURATION',16,1)
		RETURN
	END

	If ISNULL(@bitApprovalforTImeExpense,0) = 1 AND (SELECT
														COUNT(*) 
													FROM
														UserMaster
													LEFT JOIN
														ApprovalConfig
													ON
														ApprovalConfig.numUserId = UserMaster.numUserDetailId
													WHERE
														UserMaster.numDomainID=@numDomainID
														AND numModule=1
														AND ISNULL(numGroupID,0) > 0
														AND ISNULL(bitActivateFlag,0) = 1
														AND ISNULL(numLevel1Authority,0) = 0
														AND ISNULL(numLevel2Authority,0) = 0
														AND ISNULL(numLevel3Authority,0) = 0
														AND ISNULL(numLevel4Authority,0) = 0
														AND ISNULL(numLevel5Authority,0) = 0) > 0
	BEGIN
		RAISERROR('INCOMPLETE_TIMEEXPENSE_APPROVAL',16,1)
		RETURN
	END

	DECLARE @tintCommitAllocationOld AS TINYINT
	SELECT @tintCommitAllocationOld=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainID=@numDomainID

	IF @tintCommitAllocation <> @tintCommitAllocationOld AND (SELECT COUNT(*) FROM OpportunityMaster WHERE numDomainId=@numDomainID AND tintOppType=1 AND tintOppStatus=1 AND ISNULL(tintshipped,0)=0) > 0
	BEGIN
		RAISERROR('NOT_ALLOWED_CHANGE_COMMIT_ALLOCATION_OPEN_SALES_ORDER',16,1)
		RETURN
	END
                                  
	UPDATE 
		Domain                                       
	SET                                       
		vcDomainName=@vcDomainName,                                      
		vcDomainDesc=@vcDomainDesc,                                      
		bitExchangeIntegration=@bitExchangeIntegration,                                      
		bitAccessExchange=@bitAccessExchange,                                      
		vcExchUserName=@vcExchUserName,                                      
		vcExchPassword=@vcExchPassword,                                      
		vcExchPath=@vcExchPath,                                      
		vcExchDomain=@vcExchDomain,                                    
		tintCustomPagingRows =@tintCustomPagingRows,                                    
		vcDateFormat=@vcDateFormat,                                    
		numDefCountry =@numDefCountry,                                    
		tintComposeWindow=@tintComposeWindow,                                    
		sintStartDate =@sintStartDate,                                    
		sintNoofDaysInterval=@sintNoofDaysInterval,                                    
		tintAssignToCriteria=@tintAssignToCriteria,                                    
		bitIntmedPage=@bitIntmedPage,                                    
		tintFiscalStartMonth=@tintFiscalStartMonth,                                                      
		tintPayPeriod=@tintPayPeriod,
		vcCurrency=isnull((select varCurrSymbol from Currency Where numCurrencyID=@numCurrencyID  ),'')  ,                      
		numCurrencyID=@numCurrencyID,                
		charUnitSystem= @charUnitSystem,              
		vcPortalLogo=@vcPortalLogo ,            
		tintChrForComSearch=@tintChrForComSearch  ,      
		tintChrForItemSearch=@tintChrForItemSearch,
		numShipCompany= @ShipCompany,
		bitMultiCurrency=@bitMultiCurrency,
		bitCreateInvoice= @bitCreateInvoice,
		[numDefaultSalesBizDocId] =@numDefaultSalesBizDocId,
		bitSaveCreditCardInfo=@bitSaveCreditCardInfo,
		intLastViewedRecord=@intLastViewedRecord,
		[tintCommissionType]=@tintCommissionType,
		[tintBaseTax]=@tintBaseTax,
		[numDefaultPurchaseBizDocId]=@numDefaultPurchaseBizDocID,
		bitEmbeddedCost=@bitEmbeddedCost,
		numDefaultSalesOppBizDocId=@numDefaultSalesOppBizDocId,
		tintSessionTimeOut=@tintSessionTimeOut,
		tintDecimalPoints=@tintDecimalPoints,
		bitCustomizePortal=@bitCustomizePortal,
		tintBillToForPO = @tintBillToForPO,
		tintShipToForPO = @tintShipToForPO,
		tintOrganizationSearchCriteria=@tintOrganizationSearchCriteria,
		bitDocumentRepositary=@bitDocumentRepositary,
		bitGtoBContact=@bitGtoBContact,
		bitBtoGContact=@bitBtoGContact,
		bitGtoBCalendar=@bitGtoBCalendar,
		bitBtoGCalendar=@bitBtoGCalendar,
		bitExpenseNonInventoryItem=@bitExpenseNonInventoryItem,
		bitInlineEdit=@bitInlineEdit,
		bitRemoveVendorPOValidation=@bitRemoveVendorPOValidation,
		tintBaseTaxOnArea=@tintBaseTaxOnArea,
		bitAmountPastDue = @bitAmountPastDue,
		monAmountPastDue = @monAmountPastDue,
		bitSearchOrderCustomerHistory=@bitSearchOrderCustomerHistory,
		tintCalendarTimeFormat=@tintCalendarTimeFormat,
		tintDefaultClassType=@tintDefaultClassType,
		tintPriceBookDiscount=@tintPriceBookDiscount,
		bitAutoSerialNoAssign=@bitAutoSerialNoAssign,
		bitIsShowBalance = @bitIsShowBalance,
		numIncomeAccID = @numIncomeAccID,
		numCOGSAccID = @numCOGSAccID,
		numAssetAccID = @numAssetAccID,
		IsEnableProjectTracking = @IsEnableProjectTracking,
		IsEnableCampaignTracking = @IsEnableCampaignTracking,
		IsEnableResourceScheduling = @IsEnableResourceScheduling,
		numShippingServiceItemID = @ShippingServiceItem,
		numSOBizDocStatus=@numSOBizDocStatus,
		bitAutolinkUnappliedPayment=@bitAutolinkUnappliedPayment,
		numDiscountServiceItemID=@numDiscountServiceItemID,
		vcShipToPhoneNo = @vcShipToPhoneNumber,
		vcGAUserEMail = @vcGAUserEmailId,
		vcGAUserPassword = @vcGAUserPassword,
		vcGAUserProfileId = @vcGAUserProfileId,
		bitDiscountOnUnitPrice=@bitDiscountOnUnitPrice,
		intShippingImageWidth = @intShippingImageWidth ,
		intShippingImageHeight = @intShippingImageHeight,
		numTotalInsuredValue = @numTotalInsuredValue,
		numTotalCustomsValue = @numTotalCustomsValue,
		vcHideTabs = @vcHideTabs,
		bitUseBizdocAmount = @bitUseBizdocAmount,
		bitDefaultRateType = @bitDefaultRateType,
		bitIncludeTaxAndShippingInCommission = @bitIncludeTaxAndShippingInCommission,
		bitPurchaseTaxCredit=@bitPurchaseTaxCredit,
		bitLandedCost=@bitLandedCost,
		vcLanedCostDefault=@vcLanedCostDefault,
		bitMinUnitPriceRule = @bitMinUnitPriceRule,
		numDefaultSalesPricing = @numDefaultSalesPricing,
		numPODropShipBizDoc=@numPODropShipBizDoc,
		numPODropShipBizDocTemplate=@numPODropShipBizDocTemplate,
		IsEnableDeferredIncome=@IsEnableDeferredIncome,
		bitApprovalforTImeExpense=@bitApprovalforTImeExpense,
		numCost= @numCost,intTimeExpApprovalProcess=@intTimeExpApprovalProcess,
		bitApprovalforOpportunity=@bitApprovalforOpportunity,intOpportunityApprovalProcess=@intOpportunityApprovalProcess,
		numDefaultPurchaseShippingDoc=@numDefaultPurchaseShippingDoc,numDefaultSalesShippingDoc=@numDefaultSalesShippingDoc,
		bitchkOverRideAssignto=@bitchkOverRideAssignto,
		vcPrinterIPAddress=@vcPrinterIPAddress,
		vcPrinterPort=@vcPrinterPort,
		numDefaultSiteID=ISNULL(@numDefaultSiteID,0),
		bitRemoveGlobalLocation=@bitRemoveGlobalLocation,
		numAuthorizePercentage=@numAuthorizePercentage,
		bitEDI=ISNULL(@bitEDI,0),
		bit3PL=ISNULL(@bit3PL,0),
		bitAllowDuplicateLineItems=ISNULL(@bitAllowDuplicateLineItems,0),
		tintCommitAllocation=ISNULL(@tintCommitAllocation,1),
		tintInvoicing = ISNULL(@tintInvoicing,1),
		tintMarkupDiscountOption = ISNULL(@tintMarkupDiscountOption,1),
		tintMarkupDiscountValue = ISNULL(@tintMarkupDiscountValue,1)
		,bitCommissionBasedOn=ISNULL(@bitCommissionBasedOn,0)
		,tintCommissionBasedOn=ISNULL(@tintCommissionBasedOn,1)
		,bitDoNotShowDropshipPOWindow=ISNULL(@bitDoNotShowDropshipPOWindow,0)
		,tintReceivePaymentTo=ISNULL(@tintReceivePaymentTo,2)
		,numReceivePaymentBankAccount = ISNULL(@numReceivePaymentBankAccount,0)
		,numOverheadServiceItemID=@numOverheadServiceItemID,
		bitDisplayCustomField=@bitDisplayCustomField,
		bitFollowupAnytime=@bitFollowupAnytime,
		bitpartycalendarTitle=@bitpartycalendarTitle,
		bitpartycalendarLocation=@bitpartycalendarLocation,
		bitpartycalendarDescription=@bitpartycalendarDescription,
		bitREQPOApproval=@bitREQPOApproval,
		bitARInvoiceDue=@bitARInvoiceDue,
		bitAPBillsDue=@bitAPBillsDue,
		bitItemsToPickPackShip=@bitItemsToPickPackShip,
		bitItemsToInvoice=@bitItemsToInvoice,
		bitSalesOrderToClose=@bitSalesOrderToClose,
		bitItemsToPutAway=@bitItemsToPutAway,
		bitItemsToBill=@bitItemsToBill,
		bitPosToClose=@bitPosToClose,
		bitPOToClose=@bitPOToClose,
		bitBOMSToPick=@bitBOMSToPick,
		vchREQPOApprovalEmp=@vchREQPOApprovalEmp,
		vchARInvoiceDue=@vchARInvoiceDue,
		vchAPBillsDue=@vchAPBillsDue,
		vchItemsToPickPackShip=@vchItemsToPickPackShip,
		vchItemsToInvoice=@vchItemsToInvoice,
		vchSalesOrderToClose=@vchSalesOrderToClose,
		vchItemsToPutAway=@vchItemsToPutAway,
		vchItemsToBill=@vchItemsToBill,
		vchPosToClose=@vchPosToClose,
		vchPOToClose=@vchPOToClose,
		vchBOMSToPick=@vchBOMSToPick,
		decReqPOMinValue=@decReqPOMinValue,
		bitUseOnlyActionItems = @bitUseOnlyActionItems,
		bitDisplayContractElement = @bitDisplayContractElement,
		vcEmployeeForContractTimeElement=@vcEmployeeForContractTimeElement,
		numARContactPosition= @numARContactPosition,
		bitShowCardConnectLink = @bitShowCardConnectLink,
		vcBluePayFormName=@vcBluePayFormName,
		vcBluePaySuccessURL=@vcBluePaySuccessURL,
		vcBluePayDeclineURL=@vcBluePayDeclineURL,
		bitUseDeluxeCheckStock=@bitUseDeluxeCheckStock
	WHERE 
		numDomainId=@numDomainID

	IF(LEN(@vchPriceMarginApproval)>0)
	BEGIN
		DELETE FROM UnitPriceApprover WHERE numDomainID = @numDomainID

		INSERT INTO UnitPriceApprover
			(
				numDomainID,
				numUserID
			)SELECT @numDomainID,Items FROM Split(@vchPriceMarginApproval,',') WHERE Items<>''
	END
	IF ISNULL(@bitRemoveGlobalLocation,0) = 1
	BEGIN
		DECLARE @TEMPGlobalWarehouse TABLE
		(
			ID INT IDENTITY(1,1)
			,numItemCode NUMERIC(18,0)
			,numWarehouseID NUMERIC(18,0)
			,numWarehouseItemID NUMERIC(28,0)
		)

		INSERT INTO @TEMPGlobalWarehouse
		(
			numItemCode
			,numWarehouseID
			,numWarehouseItemID
		)
		SELECT
			numItemID
			,numWareHouseID
			,numWareHouseItemID
		FROM
			WareHouseItems
		WHERE
			numDomainID=@numDomainID
			AND numWLocationID = -1
			AND ISNULL(numOnHand,0) = 0
			AND ISNULL(numOnOrder,0)=0
			AND ISNULL(numAllocation,0)=0
			AND ISNULL(numBackOrder,0)=0


		DECLARE @i AS INT = 1
		DECLARE @iCount AS INT
		DECLARE @numTempItemCode AS NUMERIC(18,0)
		DECLARE @numTempWareHouseID AS NUMERIC(18,0)
		DECLARE @numTempWareHouseItemID AS NUMERIC(18,0)
		SELECT @iCount=COUNT(*) FROM @TEMPGlobalWarehouse

		WHILE @i <= @iCount
		BEGIN
			SELECT @numTempItemCode=numItemCode,@numTempWareHouseID=numWarehouseID,@numTempWareHouseItemID=numWarehouseItemID FROM @TEMPGlobalWarehouse WHERE ID=@i

			BEGIN TRY
				EXEC USP_AddUpdateWareHouseForItems @numTempItemCode,  
													@numTempWareHouseID,
													@numTempWareHouseItemID,
													'',
													0,
													0,
													0,
													'',
													'',
													@numDomainID,
													'',
													'',
													'',
													0,
													3,
													0,
													0,
													NULL,
													0
			END TRY
			BEGIN CATCH

			END CATCH

			SET @i = @i + 1
		END
	END
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH

END
/****** Object:  StoredProcedure [dbo].[USP_UpdateOppItems]    Script Date: 07/26/2008 16:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updateoppitems')
DROP PROCEDURE usp_updateoppitems
GO
CREATE PROCEDURE [dbo].[USP_UpdateOppItems]
@numOppID as numeric(9)=0,
@numItemCode as numeric(9)=0,
@numUnitHour as FLOAT=0,
@monPrice as DECIMAL(20,5),
@vcItemName VARCHAR(300),
@vcModelId VARCHAR(200),
@vcPathForTImage VARCHAR(300),
@vcItemDesc as varchar(1000),
@numWarehouseItmsID as numeric(9),
@ItemType as Varchar(50),
@DropShip as bit=0,
@numUOM as numeric(9)=0,
@numClassID as numeric(9)=0,
@numProjectID as numeric(9)=0,
@vcNotes varchar(1000)=null,
@dtItemRequiredDate DATETIME = null

as                                                   
	SET @vcNotes = (CASE WHEN @vcNotes='&nbsp;' THEN '' ELSE @vcNotes END)
	SET @vcItemDesc = (CASE WHEN ISNULL(@vcItemDesc,'')='' THEN ISNULL((SELECT txtItemDesc FROM Item WHERE numItemCode=@numItemCode),'') ELSE @vcItemDesc END)
	
   if @numWarehouseItmsID=0 set @numWarehouseItmsID=null 
	
	DECLARE @vcManufacturer VARCHAR(250)
	SELECT @vcManufacturer = vcManufacturer FROM item WHERE [numItemCode]=@numItemCode
    insert into OpportunityItems                                                    
	(numOppId,numItemCode,vcNotes,numUnitHour,monPrice,numCost,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,[vcItemName],[vcModelID],[vcPathForTImage],[vcManufacturer],numUOMId,numClassID,numProjectID,numSortOrder,ItemRequiredDate)
	values
	(@numOppID,@numItemCode,@vcNotes,@numUnitHour,@monPrice,@monPrice,@numUnitHour*@monPrice,@vcItemDesc,@numWarehouseItmsID,@ItemType,@DropShip,0,0,@numUnitHour*@monPrice,@vcItemName,@vcModelId,@vcPathForTImage,@vcManufacturer,@numUOM,@numClassID,@numProjectID,ISNULL((SELECT MAX(numSortOrder) FROM OpportunityItems WHERE numOppID=@numOppID),0) + 1,@dtItemRequiredDate)
	
	select SCOPE_IDENTITY()
	                                 
	update OpportunityMaster set  monPamount=(select sum(monTotAmount)from OpportunityItems where numOppId=@numOppID)                                                     
	where numOppId=@numOppID
GO
--Created by Anoop Jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdatePOItemsForFulfillment')
DROP PROCEDURE USP_UpdatePOItemsForFulfillment
GO
CREATE PROCEDURE  USP_UpdatePOItemsForFulfillment
    @numQtyReceived FLOAT,
    @numOppItemID NUMERIC(9),
    @vcError VARCHAR(200) = '' OUTPUT,
    @numUserCntID AS NUMERIC(9),
    @dtItemReceivedDate AS DATETIME,
	@numSelectedWarehouseItemID AS NUMERIC(18,0) = 0,
	@numVendorInvoiceBizDocID NUMERIC(18,0) = 0,
	@tintMode TINYINT = 0
AS 
BEGIN
--Transaction is invoked from ADO.net (Purchase Fulfillment) Do not add transaction code here.
-- BEGIN TRY
-- BEGIN TRANSACTION
	IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
	BEGIN
		-- IF RECEIVED AGAINST VENDOR INVOICE MAKE SURE ENOUGH QTY IS PENDING TO RECIEVED AGAINST VEDOR INVOICE BECAUSE USER CAN RECEIVE QTY WITHOUT SELECTING VENDOR INVOCIE ALSO
		IF (SELECT
				COUNT(*)
			FROM
				OpportunityBizDocItems OBDI
			INNER JOIN
				OpportunityItems OI
			ON
				OBDI.numOppItemID=OI.numoppitemtCode
			WHERE
				numOppBizDocID=@numVendorInvoiceBizDocID
				AND OBDI.numOppItemID = @numOppItemID
				AND @numQtyReceived > (ISNULL(OI.numUnitHour,0) - ISNULL(OI.numUnitHourReceived,0))
			) > 0
		BEGIN
			RAISERROR('INVALID_VENDOR_INVOICE_RECEIVE_QTY',16,1)
			RETURN
		END
	END


	DECLARE @numDomain AS NUMERIC(18,0)
	          
	DECLARE @numWarehouseItemID AS NUMERIC
	DECLARE @numWLocationID AS NUMERIC(18,0)     
	DECLARE @numOldQtyReceived AS FLOAT       
	DECLARE @numNewQtyReceived AS FLOAT
	              
	DECLARE @bitStockTransfer BIT
	DECLARE @numToWarehouseItemID NUMERIC
	DECLARE @numOppId NUMERIC
	DECLARE @numUnits FLOAT
	DECLARE @monPrice AS DECIMAL(20,5) 
	DECLARE @numItemCode NUMERIC 
		
	SELECT  
		@numWarehouseItemID = ISNULL([numWarehouseItmsID], 0),
		@numWLocationID = ISNULL(WI.numWLocationID,0),
		@numOldQtyReceived = ISNULL([numUnitHourReceived], 0),
		@bitStockTransfer = ISNULL(OM.bitStockTransfer,0),
		@numToWarehouseItemID= ISNULL(numToWarehouseItemID,0),
		@numOppId=OM.numOppId,
		@numUnits=OI.numUnitHour,
		@monPrice=isnull(monPrice,0) * (CASE WHEN ISNULL(OM.fltExchangeRate,0)=0 THEN 1 ELSE OM.fltExchangeRate END),
		@numItemCode=OI.numItemCode,
		@numDomain = OM.[numDomainId]
	FROM 
		[OpportunityItems] OI 
	INNER JOIN 
		dbo.OpportunityMaster OM
	ON 
		OI.numOppId = OM.numOppId
	LEFT JOIN
		WareHouseItems WI
	ON
		OI.numWarehouseItmsID = WI.numWareHouseItemID
	WHERE 
		[numoppitemtCode] = @numOppItemID


	IF @bitStockTransfer = 1 --added by chintan
	BEGIN
	--ship item from FROM warehouse
		DECLARE @p3 VARCHAR(MAX) = ''
		
		EXEC USP_UpdateQtyShipped @numQtyReceived,@numOppItemID,@numUserCntID,@vcError=@p3 OUTPUT

		IF LEN(@p3)>0 
		BEGIN
			RAISERROR ( @p3,16, 1 )
			RETURN ;
		END
		
		-- Receive item from To Warehouse
		SET @numWarehouseItemID=@numToWarehouseItemID
    
	END  
    

	DECLARE @numTotalQuantityReceived FLOAT
	SET @numTotalQuantityReceived = @numQtyReceived + @numOldQtyReceived ;
	SET @numNewQtyReceived = @numQtyReceived;
	DECLARE @description AS VARCHAR(100)

	SET @description=CONCAT('PO Qty Received (Qty:',@numTotalQuantityReceived,')')
	
	IF @numTotalQuantityReceived > @numUnits
	BEGIN
		RAISERROR('RECEIVED_QTY_MUST_BE_LESS_THEN_OR_EQUAL_TO_ORDERED_QTY',16,1)
	END

	IF @numNewQtyReceived <= 0 
		RETURN 
  
	
	IF ISNULL(@bitStockTransfer,0) = 0
	BEGIN
		DECLARE @TotalOnHand AS FLOAT;SET @TotalOnHand=0  
		SELECT @TotalOnHand=(SUM(ISNULL(numOnHand,0)) + SUM(ISNULL(numAllocation,0))) FROM dbo.WareHouseItems WHERE numItemID=@numItemCode 
						
		--Updating the Average Cost
		DECLARE @monAvgCost AS DECIMAL(20,5) 
		SELECT  @monAvgCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost, 0) END) FROM Item WHERE   numitemcode = @numItemCode  
    
  		SET @monAvgCost = ((@TotalOnHand * @monAvgCost) + (@numNewQtyReceived * @monPrice)) / ( @TotalOnHand + @numNewQtyReceived )
                            
		UPDATE  
			item
		SET 
			monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monAvgCost END)
		WHERE 
			numItemCode = @numItemCode
    END	

	IF ISNULL(@numSelectedWarehouseItemID,0) > 0 AND ISNULL(@numSelectedWarehouseItemID,0) <> @numWareHouseItemID		
	BEGIN
		DECLARE @vcFromLocation AS VARCHAR(300)
		DECLARE @vcToLocation AS VARCHAR(300)
		SELECT @vcFromLocation=ISNULL(WL.vcLocation,'') FROM WareHouseItems WI LEFT JOIN WarehouseLocation WL ON WI.numWLocationID=WL.numWLocationID WHERE WI.numWareHouseItemID=@numWareHouseItemID
		SELECT @vcToLocation=ISNULL(WL.vcLocation,'') FROM WareHouseItems WI LEFT JOIN WarehouseLocation WL ON WI.numWLocationID=WL.numWLocationID WHERE WI.numWareHouseItemID=@numSelectedWarehouseItemID

		UPDATE
			WareHouseItems
		SET 
			numOnOrder = ISNULL(numOnOrder,0) - @numNewQtyReceived,
			dtModified = GETDATE()
		WHERE
			numWareHouseItemID=@numWareHouseItemID
		
		SET @description = CONCAT('PO Qty Received To Internal Location ',@vcToLocation,' (Qty:',@numNewQtyReceived,')')

		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppId, --  numeric(9, 0)
			@tintRefType = 4, --  tinyint
			@vcDescription = @description,
			@numModifiedBy = @numUserCntID,
			@dtRecordDate =  @dtItemReceivedDate,
			@numDomainID = @numDomain

		-- INCREASE THE OnHand Of Destination Location
		UPDATE
			WareHouseItems
		SET
			numBackOrder = (CASE WHEN numBackOrder >= @numNewQtyReceived THEN ISNULL(numBackOrder,0) - @numNewQtyReceived ELSE 0 END),         
			numAllocation = (CASE WHEN numBackOrder >= @numNewQtyReceived THEN ISNULL(numAllocation,0) + @numNewQtyReceived ELSE ISNULL(numAllocation,0) + ISNULL(numBackOrder,0) END),
			numOnHand = (CASE WHEN numBackOrder >= @numNewQtyReceived THEN numOnHand ELSE ISNULL(numOnHand,0) + @numNewQtyReceived - ISNULL(numBackOrder,0) END),
			dtModified = GETDATE()
		WHERE
			numWareHouseItemID=@numSelectedWarehouseItemID

		SET @description = CONCAT('PO Qty Received From Internal Location ',@vcFromLocation,' (Qty:',@numNewQtyReceived,')')

		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numSelectedWarehouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppId, --  numeric(9, 0)
			@tintRefType = 4, --  tinyint
			@vcDescription = @description,
			@numModifiedBy = @numUserCntID,
			@dtRecordDate =  @dtItemReceivedDate,
			@numDomainID = @numDomain

		INSERT INTO OpportunityItemsReceievedLocation
		(
			numDomainID,
			numOppID,
			numOppItemID,
			numWarehouseItemID,
			numUnitReceieved
		)
		VALUES
		(
			@numDomain,
			@numOppId,
			@numOppItemID,
			@numSelectedWarehouseItemID,
			@numNewQtyReceived
		)
	END
	ELSE IF ISNULL(@numWareHouseItemID,0) > 0
	BEGIN
		DECLARE @onHand AS FLOAT
		DECLARE @onAllocation AS FLOAT    
		DECLARE @onOrder AS FLOAT            
		DECLARE @onBackOrder AS FLOAT

		SELECT  
			@onHand = ISNULL(numOnHand, 0),
			@onAllocation = ISNULL(numAllocation, 0),
			@onOrder = ISNULL(numOnOrder, 0),
			@onBackOrder = ISNULL(numBackOrder, 0)
		FROM 
			WareHouseItems
		WHERE 
			numWareHouseItemID = @numWareHouseItemID

		IF @onOrder >= @numNewQtyReceived 
		BEGIN
			PRINT '1 case'

			SET @onOrder = @onOrder - @numNewQtyReceived             
			
			IF @onBackOrder >= @numNewQtyReceived 
			BEGIN            
				SET @onBackOrder = @onBackOrder - @numNewQtyReceived             
				SET @onAllocation = @onAllocation + @numNewQtyReceived             
			END            
			ELSE 
			BEGIN            
				SET @onAllocation = @onAllocation + @onBackOrder            
				SET @numNewQtyReceived = @numNewQtyReceived - @onBackOrder            
				SET @onBackOrder = 0            
				SET @onHand = @onHand + @numNewQtyReceived             
			END         
		END            
		ELSE IF @onOrder < @numNewQtyReceived 
		BEGIN
			PRINT '2 case'        
			SET @onHand = @onHand + @onOrder
			SET @onOrder = @numNewQtyReceived - @onOrder
		END   

		SELECT 
			@onHand onHand,
			@onAllocation onAllocation,
			@onBackOrder onBackOrder,
			@onOrder onOrder
                
		UPDATE 
			WareHouseItems
		SET     
			numOnHand = @onHand,
			numAllocation = @onAllocation,
			numBackOrder = @onBackOrder,
			numOnOrder = @onOrder,
			dtModified = GETDATE() 
		WHERE 
			numWareHouseItemID = @numWareHouseItemID
    
		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppId, --  numeric(9, 0)
			@tintRefType = 4, --  tinyint
			@vcDescription = @description, --  varchar(100)
			@numModifiedBy = @numUserCntID,
			@dtRecordDate =  @dtItemReceivedDate,
			@numDomainID = @numDomain
	END
 
	UPDATE  
		[OpportunityItems]
	SET 
		numUnitHourReceived = @numTotalQuantityReceived
		,numQtyReceived = (CASE WHEN ISNULL(@tintMode,0)=1 THEN ISNULL(numQtyReceived,0) + ISNULL(@numQtyReceived,0) ELSE ISNULL(numQtyReceived,0) END)
	WHERE
		[numoppitemtCode] = @numOppItemID

	UPDATE I SET dtItemReceivedDate=@dtItemReceivedDate FROM OpportunityItems OI INNER JOIN Item I ON OI.numItemCode=I.numItemCode WHERE OI.numoppitemtCode=@numOppItemID

	IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
	BEGIN
		UPDATE  
			OpportunityBizDocItems
		SET 
			numVendorInvoiceUnitReceived = ISNULL(numVendorInvoiceUnitReceived,0) +  ISNULL(@numQtyReceived,0)
		WHERE
			numOppBizDocID=@numVendorInvoiceBizDocID
			AND numOppItemID = @numOppItemID
	END

	UPDATE dbo.OpportunityMaster SET dtItemReceivedDate=@dtItemReceivedDate WHERE numOppId=@numOppId
END	

GO
ALTER FUNCTION [dbo].[GetTimeSpendOnTaskByProject]
(
	@numDomainID NUMERIC(18,0)
	,@numTaskID NUMERIC(18,0)
)    
RETURNS VARCHAR(50)    
AS
BEGIN
	DECLARE @vcTotalTimeSpendOnTask VARCHAR(50) = ''
	DECLARE @numTotalMinutes INT = 0

	DECLARE @TEMTIMELong TABLE
	(
		ID INT IDENTITY(1,1)
		,tintAction TINYINT
		,dtActionTime DATETIME
	)

	INSERT INTO @TEMTIMELong
	(
		tintAction
		,dtActionTime
	)
	SELECT
		tintAction
		,dtActionTime
	FROM
		StagePercentageDetailsTaskTimeLog SPDTTL
	WHERE
		SPDTTL.numDomainID=@numDomainID
		AND SPDTTL.numTaskID=@numTaskID
	ORDER BY
		dtActionTime
		,ID 

	DECLARE @i INT = 1
	DECLARE @iCount INT
	DECLARE @tintLastAction TINYINT
	DECLARE @dtLastActionTime DATETIME

	DECLARE @tintAction TINYINT
	DECLARE @dtActionTime DATETIME
	SELECT @iCount = COUNT(*) FROM @TEMTIMELong

	WHILE @i <= @iCount
	BEGIN
		SELECT
			@tintAction=tintAction
			,@dtActionTime=dtActionTime
		FROM
			@TEMTIMELong
		WHERE
			ID = @i

		IF @i = 1 
		BEGIN
		
			IF @tintAction <> 1
			BEGIN
				SET @vcTotalTimeSpendOnTask = 'Invalid time sequence'
				BREAK;
			END
			ELSE
			BEGIN
				SET @tintLastAction = @tintAction
				SET @dtLastActionTime = @dtActionTime
			END
		END
		ELSE
		BEGIN
			
			IF @tintLastAction = 1 AND @tintAction NOT IN (2,4)
			BEGIN
				SET @vcTotalTimeSpendOnTask = 'Invalid time sequence'
				BREAK;
			END
			ELSE IF @tintLastAction = 2 AND @tintAction NOT IN(3,4)
			BEGIN
				SET @vcTotalTimeSpendOnTask = 'Invalid time sequence'
				BREAK;
			END
			ELSE IF @tintLastAction = 3 AND @tintAction NOT IN (2,4)
			BEGIN
				SET @vcTotalTimeSpendOnTask = 'Invalid time sequence'
				BREAK;
			END
			ELSE IF @tintLastAction = 4
			BEGIN
				SET @vcTotalTimeSpendOnTask = 'Invalid time sequence'
				BREAK;
			END
			ELSE
			BEGIN
				IF @tintAction IN (2,4)
				BEGIN
					SET @numTotalMinutes = @numTotalMinutes + DATEDIFF(MINUTE,@dtLastActionTime,@dtActionTime)
				END

				SET @tintLastAction = @tintAction
				SET @dtLastActionTime = @dtActionTime
			END
		END

		SET @i = @i + 1
	END

	IF @vcTotalTimeSpendOnTask <> 'Invalid time sequence'
	BEGIN
		RETURN FORMAT(@numTotalMinutes / 60,'00') + ':' + FORMAT(@numTotalMinutes % 60,'00')
	END
	
	RETURN @vcTotalTimeSpendOnTask
END

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='ItemUnitPriceApproval_CheckAndGetItemDetails')
DROP FUNCTION ItemUnitPriceApproval_CheckAndGetItemDetails
GO
CREATE FUNCTION ItemUnitPriceApproval_CheckAndGetItemDetails (
@numOppId NUMERIC(18,0),
@numDomainID NUMERIC(18,0)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
DECLARE @returnFormattedItems VARCHAR(MAX)=''
SET @returnFormattedItems=''
DECLARE  @tempTableData TABLE
(
	ID INT IDENTITY(1, 1) primary key ,
	numItemCode NUMERIC(18,0),
	numUnitHour DECIMAL(18,2),
	monPrice DECIMAL(18,2),
	monTotAmount DECIMAL(18,2),
	monCost DECIMAL(18,2),
	numWarehouseItmsID NUMERIC(18,2),
	ItemName VARCHAR(MAX),
	numProductCost NUMERIC(18,0)

)
DECLARE @numDivisionID AS NUMERIC(18,0)
DECLARE @numWarehouseItemID AS NUMERIC(18,0)
DECLARE @ItemName AS VARCHAR(MAX)
SELECT TOP 1 @numDivisionID=numDivisionId FROM OpportunityMaster WHERE numOppId=@numOppId
INSERT INTO @tempTableData
SELECT I.numItemCode,O.numUnitHour,monPrice * ISNULL(OM.fltExchangeRate,1),monTotAmount,ISNULL(V.monCost,0) AS monCost,O.numWarehouseItmsID,I.vcItemName,I.monListPrice FROM OpportunityMaster OM INNER JOIN OpportunityItems As O ON OM.numOppId=O.numOppId LEFT JOIN
Item AS I ON O.numItemCode=I.numItemCode
LEFT JOIN 
		Vendor V 
	ON 
		I.numVendorID = V.numVendorID AND I.numItemCode = V.numItemCode  
where OM.numOppId=@numOppId AND O.bitItemPriceApprovalRequired=1

DECLARE @numItemClassification NUMERIC(18,0)
	DECLARE @numAbovePercent FLOAT
	DECLARE @numAboveField FLOAT
	DECLARE @numBelowPercent FLOAT
	DECLARE @numBelowField FLOAT
	DECLARE @numDefaultCost TINYINT
	DECLARE @bitCostApproval BIT
	DECLARE @bitListPriceApproval BIT
	DECLARE @numCostDomain INT
	DECLARE @tintKitAssemblyPriceBasedOn TINYINT
	DECLARE @bitCalAmtBasedonDepItems BIT
	DECLARE @numItemCode NUMERIC(18,0)
	DECLARE @numQuantity NUMERIC(18,0)
	DECLARE @numUnitPrice NUMERIC(18,0)
	DECLARE @numTotalAmount NUMERIC(18,0)
	DECLARE @monCost NUMERIC(18,0)
	DECLARE @numProductCost DECIMAL(18,2)=0
	SELECT @numCostDomain = numCost FROM Domain WHERE numDomainId = @numDomainID


DECLARE @i AS INT=1
DECLARE @RowCount AS INT=0
DECLARE @IsApprovalRequired BIT
	DECLARE @ItemAbovePrice FLOAT
	DECLARE @ItemBelowPrice FLOAT
DECLARE @numProposedPercentage AS DECIMAL(18,2)=0
SET @RowCount = (SELECT COUNT(*) FROM @tempTableData)
DECLARE @TEMPPrice TABLE
			(
				bitSuccess BIT
				,monPrice DECIMAL(20,5)
			)
WHILE (@i<=@RowCount)
BEGIN	
		SET @numProposedPercentage = 0
		SELECT 
			@numItemCode=numItemCode,
			@numQuantity=numUnitHour,
			@numUnitPrice=monPrice,
			@numTotalAmount=monTotAmount,
			@monCost=monCost,
			@numWarehouseItemID = numWarehouseItmsID,
			@ItemName=ItemName,
			@numProductCost=numProductCost
		FROM
			@tempTableData
		WHERE
			ID=@i

		SELECT 
			@numItemClassification=ISNULL(numItemClassification,0)
			,@tintKitAssemblyPriceBasedOn=ISNULL(tintKitAssemblyPriceBasedOn,1)
			,@bitCalAmtBasedonDepItems=ISNULL(bitCalAmtBasedonDepItems,0) 
		FROM 
			Item 
		WHERE 
			numItemCode=@numItemCode

		IF ISNULL(@numItemClassification,0) > 0 AND EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainID=@numDomainID AND numListItemID=@numItemClassification)
		BEGIN
			SELECT
				@numAbovePercent = ISNULL(numAbovePercent,0)
				,@numBelowPercent = ISNULL(numBelowPercent,0)
				,@numBelowField = ISNULL(numBelowPriceField,0)
				,@numAboveField=ISNULL(@numCostDomain,1)
				,@bitCostApproval=ISNULL(bitCostApproval,0)
				,@bitListPriceApproval=ISNULL(bitListPriceApproval,0)
			FROM
				ApprovalProcessItemsClassification
			WHERE
				numDomainId=@numDomainID
				AND ISNULL(numListItemID,0)=@numItemClassification
		END
		ELSE
		BEGIN
			SELECT
				@numAbovePercent = ISNULL(numAbovePercent,0)
				,@numBelowPercent = ISNULL(numBelowPercent,0)
				,@numBelowField = ISNULL(numBelowPriceField,0)
				,@numAboveField=ISNULL(@numCostDomain,1)
				,@bitCostApproval=ISNULL(bitCostApproval,0)
				,@bitListPriceApproval=ISNULL(bitListPriceApproval,0)
			FROM
				ApprovalProcessItemsClassification
			WHERE
				numDomainId=@numDomainID
				AND ISNULL(numListItemID,0)=0
		END	


			SET @IsApprovalRequired = 0
			SET @ItemAbovePrice = 0
			SET @ItemBelowPrice = 0

			IF @numQuantity > 0
				SET @numUnitPrice = (@numTotalAmount / @numQuantity)
			ELSE
				SET @numUnitPrice = 0

			
			DELETE FROM @TEMPPrice
			IF @numAboveField > 0 AND ISNULL(@bitCostApproval,0) = 1
			BEGIN
			  
				IF (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode  AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 AND @bitCalAmtBasedonDepItems=1
				BEGIN
					DELETE FROM @TEMPPrice

					INSERT INTO @TEMPPrice
					(
						bitSuccess
						,monPrice
					)
					SELECT
						bitSuccess
						,monPrice
					FROM
						dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,@numDivisionID,@numItemCode,@numQuantity,@numWarehouseItemID,@tintKitAssemblyPriceBasedOn,0,0,'',0,1)
					
					IF (SELECT bitSuccess FROM @TEMPPrice) = 1
					BEGIN
						SET @ItemAbovePrice = (SELECT monPrice FROM @TEMPPrice)
					END
				END
				ELSE
				BEGIN
					IF @numAboveField = 3 -- Primaty Vendor Cost
						SELECT @ItemAbovePrice = ISNULL(monCost,0) FROM Vendor WHERE numItemCode = @numItemCode AND numVendorID = (SELECT numVendorID FROM Item WHERE numItemCode = @numItemCode)
					ELSE IF @numAboveField = 2 -- Product & Sevices Cost
						SELECT @ItemAbovePrice = ISNULL(@numProductCost,0)
					ELSE IF @numAboveField = 1 -- Average Cost
						SELECT @ItemAbovePrice = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode
				END
				IF @ItemAbovePrice > 0
				BEGIN
					IF @numUnitPrice < (@ItemAbovePrice + (@ItemAbovePrice * (@numAbovePercent/100)))
						SET @numProposedPercentage = (((@numUnitPrice - @ItemAbovePrice) /@ItemAbovePrice)*100)

						SET @returnFormattedItems= @returnFormattedItems + '<b>' + @ItemName + '</b>' +'('+CAST(@numQuantity As VARCHAR(500))+') Proposed '+CAST(@numProposedPercentage AS VARCHAR(100))+'%, Minimum Allowed '+CAST(@numAbovePercent As VARCHAR(500))+'%, '
						SET @IsApprovalRequired = 1
				END
				ELSE IF @ItemAbovePrice = 0
				BEGIN
					SET @returnFormattedItems= @returnFormattedItems + '<b>' + @ItemName + '</b>' +'('+CAST(@numQuantity As VARCHAR(500))+') Proposed 0%, Minimum Allowed '+CAST(@numAbovePercent As VARCHAR(500))+'%, '
					SET @IsApprovalRequired = 1
				END
			END

			IF @IsApprovalRequired = 0 AND @numBelowField > 0  AND ISNULL(@bitListPriceApproval,0) = 1
			BEGIN
				IF @numBelowField = 1 -- List Price
				BEGIN
					IF (SELECT ISNULL(charItemType,'') AS charItemType FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode) = 'P' -- Inventory Item
						IF (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode  AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 AND @bitCalAmtBasedonDepItems=1
						BEGIN
							DELETE FROM @TEMPPrice

							INSERT INTO @TEMPPrice
							(
								bitSuccess
								,monPrice
							)
							SELECT
								bitSuccess
								,monPrice
							FROM
								dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,@numDivisionID,@numItemCode,@numQuantity,@numWarehouseItemID,@tintKitAssemblyPriceBasedOn,0,0,'',0,1)

							IF (SELECT bitSuccess FROM @TEMPPrice) = 1
							BEGIN
								SET @ItemBelowPrice = (SELECT monPrice FROM @TEMPPrice)
							END
						END
						ELSE
						BEGIN
							SELECT @ItemBelowPrice = ISNULL(monWListPrice,0) FROM WareHouseItems WHERE numDomainID = @numDomainID AND numItemID = @numItemCode AND numWareHouseItemID = @numWarehouseItemID
						END
					ELSE
						SELECT @ItemBelowPrice = ISNULL(monListPrice,0) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode
				END
				ELSE IF @numBelowField = 2 -- Price Rule
				BEGIN
					/* Check if valid price book rules exists for sales in domain */
					IF (SELECT COUNT(*) FROM PriceBookRules WHERE numDomainId = @numDomainID AND tintRuleFor = 1 AND tintStep2 > 0 AND tintStep3 > 0) > 0
					BEGIN
						DECLARE @j INT = 0
						DECLARE @Count int = 0
						DECLARE @tempPriority INT
						DECLARE @tempNumPriceRuleID NUMERIC(18,0)
						DECLARE @numPriceRuleIDApplied NUMERIC(18,0) = 0
						DECLARE @TempTable TABLE (numID INT, numRuleID numeric(18,0), numPriority INT)

						INSERT INTO 
							@TempTable
						SELECT 
							ROW_NUMBER() OVER (ORDER BY PriceBookPriorities.Priority) AS id,
							numPricRuleID,
							PriceBookPriorities.Priority 
						FROM 
							PriceBookRules
						INNER JOIN
							PriceBookPriorities
						ON
							PriceBookRules.tintStep2 = PriceBookPriorities.Step2Value AND
							PriceBookRules.tintStep3 = PriceBookPriorities.Step3Value
						WHERE 
							PriceBookRules.numDomainId = @numDomainID AND 
							PriceBookRules.tintRuleFor = 1 AND 
							PriceBookRules.tintStep2 > 0 AND PriceBookRules.tintStep3 > 0
						ORDER BY
							PriceBookPriorities.Priority

						SELECT @Count = COUNT(*) FROM @TempTable

						/* Loop all price rule with priority */
						WHILE (@j < @count)
						BEGIN
							SELECT @tempNumPriceRuleID = numRuleID, @tempPriority = numPriority FROM @TempTable WHERE numID = (@i + 1)
					
							/* IF proprity is 9 then price rule is applied to all items and all customers. 
							So price rule must be applied to item or not.*/
							IF @tempPriority = 9
							BEGIN
								SET @numPriceRuleIDApplied = @tempNumPriceRuleID
								BREAK
							END
							/* Check if current item exists in rule. if eixts then exit loop with rule id else continie loop. if item not exist in any rule then nothing to check */
							ELSE
							BEGIN
								DECLARE @isRuleApplicable BIT = 0
								EXEC @isRuleApplicable = dbo.CheckIfPriceRuleApplicableToItem @numRuleID = @tempNumPriceRuleID, @numItemID = @numItemCode, @numDivisionID = @numDivisionID 

								IF @isRuleApplicable = 1
								BEGIN
									SET @numPriceRuleIDApplied = @tempNumPriceRuleID
									BREAK
								END
							END

							SET @j = @j + 1
						END

						/* If @numPriceRuleIDApplied > 0 Get final unit price limit after applying below rule */

						IF @numPriceRuleIDApplied > 0
						BEGIN
							SET @ItemBelowPrice = dbo.GetUnitPriceAfterPriceRuleApplication(@numPriceRuleIDApplied,@numDomainID,@numItemCode,@numQuantity,@numWarehouseItemID,@numDivisionID)
						END
					END
				END
				ELSE IF @numBelowField = 3 -- Price Level
				BEGIN
					EXEC @ItemBelowPrice = GetUnitPriceAfterPriceLevelApplication @numDomainID = @numDomainID, @numItemCode = @numItemCode, @numQuantity = @numQuantity, @numWarehouseItemID = @numWarehouseItemID, @isPriceRule = 0, @numPriceRuleID = 0, @numDivisionID = @numDivisionID
				END
		
				IF @numUnitPrice < (@ItemBelowPrice - (@ItemBelowPrice * (@numBelowPercent/100)))
				SET @numProposedPercentage = (((@ItemBelowPrice- @numUnitPrice) /@ItemBelowPrice)*100)

				
				SET @returnFormattedItems=@returnFormattedItems + '<b>' + @ItemName + '</b>' +'('+CAST(@numQuantity As VARCHAR(500))+') Proposed '+CAST(@numProposedPercentage AS VARCHAR(500))+'%, Minimum Allowed '+CAST(@numBelowPercent As VARCHAR(500))+'%, '
						SET @IsApprovalRequired = 1
			END



	SET @i=@i+1
END

    RETURN ISNULL(@returnFormattedItems,'')
END