/******************************************************************
Project: Release 13.5 Date: 27.APR.2020
Comments: ALTER SCRIPTS
*******************************************************************/

/******************************************** PRASHANT *********************************************/

DELETE FROM PageNavigationDTL WHERE vcPageNavName='Activities 1.0'

DELETE FROM DycFormConfigurationDetails WHERE numFormId=43 AND 
numFieldId IN (SELECT numFieldId FROM DycFieldMaster WHERE
vcOrigDbColumnName IN('numActivity','numStatus','intTotalProgress','textDetails','vcCompanyName','bitTask','numAssignedTo','FormattedDate'))

DELETE FROM DycFormField_Mapping WHERE numFormID=43 AND numFieldId IN (SELECT numFieldId FROM DycFieldMaster WHERE
vcOrigDbColumnName IN('numActivity','numStatus','intTotalProgress','textDetails','vcCompanyName','bitTask','numAssignedTo','FormattedDate'))

/******************************************** SANDEEP *********************************************/


USE [Production.2014]
GO

/****** Object:  Table [dbo].[DycFieldMasterSynonym]    Script Date: 14-Apr-20 11:13:41 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DycFieldMasterSynonym](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numFieldID] [numeric](18, 0) NOT NULL,
	[bitCustomField] [bit] NOT NULL,
	[vcSynonym] [varchar](200) NOT NULL,
	[bitDefault] [bit] NOT NULL,
 CONSTRAINT [PK_DycFieldMasterSynonym] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

------------------------------------------

INSERT INTO DycFieldMasterSynonym 
(numDomainID,numFieldID,bitCustomField,vcSynonym,bitDefault)
VALUES
(0,270,0,'Income',1)
,(0,271,0,'Asset',1)
,(0,272,0,'COGS',1)
,(0,294,0,'Item Type',1)
,(0,6,0,'Relationship',1)
,(0,451,0,'Relationship Type',1)
,(0,19,0,'Group',1)

----------------------------------------------


UPDATE DycFieldMaster SET intFieldMaxLength=7 WHERE numFieldId IN (60)
UPDATE DycFieldMaster SET intFieldMaxLength=15 WHERE numFieldId IN (220,225)
UPDATE DycFieldMaster SET intFieldMaxLength=50 WHERE numFieldId IN (10,32,218,223,203,281,51,52,53,59,61,62,63,530)
UPDATE DycFieldMaster SET intFieldMaxLength=100 WHERE numFieldId IN (3,203,217,222)
UPDATE DycFieldMaster SET intFieldMaxLength=200 WHERE numFieldId IN (193)
UPDATE DycFieldMaster SET intFieldMaxLength=250 WHERE numFieldId IN (202)
UPDATE DycFieldMaster SET intFieldMaxLength=255 WHERE numFieldId IN (47,48,49,50)
UPDATE DycFieldMaster SET intFieldMaxLength=300 WHERE numFieldId IN (189)


UPDATE DycFieldMaster SET vcFieldDataType='V' WHERE numFieldId IN (203,380,386,287)
UPDATE DycFieldMaster SET vcFieldDataType='M' WHERE numFieldId IN (209,289)
UPDATE DycFieldMaster SET vcFieldDataType='M' WHERE numFieldId IN (195,200)


/******************************************** SATVA *********************************************/

ALTER TABLE ecommercedtl ADD dtElasticSearchDisableDatetime DATETIME
ALTER TABLE ElasticSearchBizCart DROP COLUMN numSiteID;
ALTER TABLE ElasticSearchBizCart ALTER COLUMN numRecordID NVARCHAR(200);
EXEC sp_rename 'ElasticSearchBizCart.numRecordID', 'vcValue', 'COLUMN';