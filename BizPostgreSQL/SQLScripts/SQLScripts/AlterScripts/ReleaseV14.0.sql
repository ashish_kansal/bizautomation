/******************************************************************
Project: Release 14.0 Date: 05.SEP.2020
Comments: ALTER SCRIPTS
*******************************************************************/

/******************************************** SANDEEP *********************************************/


-- ALREADY EXECUTED ON PRODUCTION 
INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField
)
VALUES
(
	4,196,129,0,0,'On-Order','TextBox',0,1,0,0,1
)
