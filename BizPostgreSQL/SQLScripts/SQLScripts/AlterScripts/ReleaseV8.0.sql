/******************************************************************
Project: Release 8.0 Date: 28.AUGUST.2017
Comments: ALTER SCRIPTS
*******************************************************************/

/******************   SANDEEP  *****************/

ALTER TABLE Category ADD vcMetaTitle VARCHAR(1000)
ALTER TABLE Category ADD vcMetaKeywords VARCHAR(1000)
ALTER TABLE Category ADD vcMetaDescription VARCHAR(1000)

------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------

BEGIN TRY
BEGIN TRANSACTION

	 --add new values for slide 1
	 DECLARE @numFieldID AS NUMERIC(18,0)
	 DECLARE @numFormFieldID AS NUMERIC(18,0)

	 INSERT INTO DycFieldMaster
	 (numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
	 bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
	 VALUES
	 (3,'Inclusion details','vcInclusionDetails','vcInclusionDetails','','OpportunityItems','V','R','Label',48,1,1,1,0,0,0,1,0,0,0,0,0)

	 SELECT @numFieldID = SCOPE_IDENTITY()

	 INSERT INTO DynamicFormFieldMaster
	 (numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
	  bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
	 VALUES
	 (26,'Inclusion details','R','Label','',0,0,'V','',0,'',0,48,1,'',47,1,1,0,0,0)

	 SELECT @numFormFieldID = SCOPE_IDENTITY()

	 INSERT INTO DycFormField_Mapping
	 (numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
	 bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
	 VALUES
	 (3,@numFieldID,26,0,0,'Inclusion details','Label','',48,1,1,1,0,0,1,0,0,0,0,@numFormFieldID)

	  INSERT INTO DynamicFormFieldMaster
	 (numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
	  bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
	 VALUES
	 (7,'Inclusion details','R','Label','',0,0,'V','',0,'',0,48,1,'',47,1,1,0,0,0)

	 SELECT @numFormFieldID = SCOPE_IDENTITY()

	 INSERT INTO DycFormField_Mapping
	 (numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
	 bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
	 VALUES
	 (3,@numFieldID,7,0,0,'Inclusion details','Label','',48,1,1,1,0,0,1,0,0,0,0,@numFormFieldID)
COMMIT
END TRY
BEGIN CATCH
 IF @@TRANCOUNT > 0
  ROLLBACK TRANSACTION;

 SELECT 
  ERROR_MESSAGE(),
  ERROR_NUMBER(),
  ERROR_SEVERITY(),
  ERROR_STATE(),
  ERROR_LINE(),
  ISNULL(ERROR_PROCEDURE(), '-');
END CATCH