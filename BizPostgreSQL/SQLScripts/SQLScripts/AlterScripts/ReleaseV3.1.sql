/******************************************************************
Project: Release 3.1 Date: 05.06.2014
Comments: 
*******************************************************************/


----- Sachin
/**************************************------------- Adding Notes Field :Date-4thJune2014--------***********************************************************************************************/

INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength)
VALUES        (3,Null,'Notes','vcNotes','vcNotes','vcNotes','OpportunityBizDocItems','V','R','TextBox',Null,NULL,0,Null,1,1,1,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null)

DECLARE @numFiedlID numeric(9,0)
SELECT @numFiedlID= MAX([DFM].[numFieldId])  FROM [dbo].[DycFieldMaster] AS DFM

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,@numFiedlID,Null,15,0,0,'Notes','TextBox','vcNotes',Null,Null,Null,Null,Null,Null,Null,0,0,0,0,0,0,0,1,Null,0,3,Null)

INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength)
VALUES        (3,Null,'Location','vcLocation','vcLocation','vcLocation','WarehouseLocation','V','R','TextBox',Null,NULL,0,Null,NULL,NULL,NULL,0,0,0,0,0,Null,Null,Null,Null,Null,Null,1,Null,Null,Null,Null)


DECLARE @numFiedlID numeric(9,0)
SELECT @numFiedlID= MAX([DFM].[numFieldId])  FROM [dbo].[DycFieldMaster] AS DFM
select @numFiedlID
INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,@numFiedlID,Null,7,0,0,'Location','TextBox','vcLocation',Null,Null,Null,Null,Null,Null,Null,0,0,0,0,0,0,0,1,Null,Null,Null,Null)

-------- Sandeep
