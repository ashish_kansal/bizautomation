/******************************************************************
Project: Release 2.2 Date: 29.10.2013
Comments: 
*******************************************************************/

/*******************Kamal Script******************/
--/************************************************************************************************/
--/************************************************************************************************/

--------Portal--------

--11	Commission Report	11
DELETE FROM PortalDashboardDtl WHERE numReportID=11
DELETE FROM PortalDashboardReports WHERE numreportid=11

--6	Items awaiting return	5
DELETE FROM PortalDashboardDtl WHERE numReportID=6
DELETE FROM PortalDashboardReports WHERE numreportid=6



--------Move to New Reports & Dashboard--------

--SELECT * FROM TabMaster
--DashBoard/frmDashBoard.aspx -> ReportDashboard/frmNewDashBoard.aspx
UPDATE TabMaster SET vcURL='ReportDashboard/frmNewDashBoard.aspx' WHERE numTabId=68

--SELECT * FROM  ShortCutBar WHERE numTabId=6
--../reports/frmCustomRptList.aspx -> ../reports/frmCustomReportList.aspx
UPDATE ShortCutBar SET link='../reports/frmCustomReportList.aspx' WHERE id=130 AND numTabId=6

--../DashBoard/frmDashBoard.aspx -> ../ReportDashboard/frmNewDashBoard.aspx
UPDATE ShortCutBar SET link='../ReportDashboard/frmNewDashBoard.aspx' WHERE id=135 AND numTabId=68

--SELECT * FROM PageNavigationDTL WHERE numModuleID=13
--Remove Administration -> User Administration -> Dashboard
UPDATE PageNavigationDTL SET bitVisible=0 WHERE numPageNavID=91 AND numModuleID=13

--SELECT * FROM PageNavigationDTL WHERE numModuleID=8
--../DashBoard/frmDashBoard.aspx -> ../ReportDashboard/frmNewDashBoard.aspx
UPDATE PageNavigationDTL SET vcNavURL='../ReportDashboard/frmNewDashBoard.aspx' WHERE numPageNavID=23 AND numModuleID=8
--../reports/frmCustomRptList.aspx -> ../reports/frmCustomReportList.aspx
UPDATE PageNavigationDTL SET vcNavURL='../reports/frmCustomReportList.aspx' WHERE numPageNavID=92 AND numModuleID=8


/*******************Manish Script******************/
/******************************************************************/
/******************************************************************/

/******************************************************************
Project: PORTAL   Date: 30.Sep.2013
Comments: Portal Column settings, adding form ids, form fields 
*******************************************************************/

SELECT * FROM DynamicFormMaster 
INSERT dbo.DynamicFormMaster 
(numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,tintFlag,bitWorkFlow,vcLocationID,bitAllowGridColor )
SELECT 78 ,'Sales Order (Portal)', 'Y', 'N',0,0, NULL,'5', NULL 
UNION ALL
SELECT 79 ,'Purchase Order (Portal)', 'Y', 'N',0,0, NULL,'5', NULL 
UNION ALL
SELECT 80 ,'Cases (Portal)', 'Y', 'N',0,0, NULL,'5', NULL 
UNION ALL
SELECT 81 ,'Knowledge Base (Portal)', 'Y', 'N',0,0, NULL,'5', NULL 
UNION ALL
SELECT 82 ,'Projects (Portal)', 'Y', 'N',0,0, NULL,'5', NULL 
UNION ALL
SELECT 83 ,'Documents (Portal)', 'Y', 'N',0,0, NULL,'5', NULL 
---------------------------

SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID = 39  -- Sales Order
SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID = 41  -- Purchase Order
SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID = 72  -- Cases
--SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID = 
SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID = 73  -- Projects
--SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID = 

INSERT INTO dbo.DycFormField_Mapping 
	(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
	 bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
SELECT numModuleID,numFieldID,numDomainID,78,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,1,
	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,0,bitExport,
	 1,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor 
	 FROM dbo.DycFormField_Mapping WHERE numFormID = 39 AND bitSettingField = 1

UNION ALL 

SELECT numModuleID,numFieldID,numDomainID,79,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,1,
	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,0,bitExport,
	 1,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor 
	 FROM dbo.DycFormField_Mapping WHERE numFormID = 41 AND bitSettingField = 1

UNION ALL 

SELECT numModuleID,numFieldID,numDomainID,80,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,1,
	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,0,bitExport,
	 1,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor 
	 FROM dbo.DycFormField_Mapping WHERE numFormID = 72 AND vcFieldName <> 'Contact' AND bitSettingField = 1

UNION ALL 

SELECT numModuleID,numFieldID,numDomainID,82,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,1,
	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,0,bitExport,
	 1,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor 
	 FROM dbo.DycFormField_Mapping WHERE numFormID = 73 AND bitSettingField = 1
	 	 
ROLLBACK


/******************************************************************
Project: BACRMUI   Date: 20.Sep.2013
Comments: Please go to Manage Authorization, and pull up the Opportunity/Orders module. In that list, 
		  you will see "Sales Opportunity Details", which needs to be relabeld to "Sales Opportunity/Order Details" 
		  there is also one called "Purchase Opportunity Details" and that one must be relabeled "Purchase Opportunity/Order Details" 
*******************************************************************/
BEGIN TRANSACTION
--SELECT * FROM PageMaster WHERE numModuleID = 10 AND vcPageDesc = 'Sales Opportunity Details'
--SELECT * FROM PageMaster WHERE numModuleID = 10 AND vcPageDesc = 'Purchase Opportunity Details'
UPDATE PageMaster SET vcPageDesc = 'Sales Opportunity/Order Details' WHERE numModuleID = 10 AND vcPageDesc = 'Sales Opportunity Details'
UPDATE PageMaster SET vcPageDesc = 'Purchase Opportunity/Order Details' WHERE numModuleID = 10 AND vcPageDesc = 'Purchase Opportunity Details'

ROLLBACK

/******************************************************************
Project: BACRM / PORTAL   Date: 26.Oct.2013
Comments: Remove 2 forms from grid column config / Change in column configuration
*******************************************************************/
BEGIN TRANSACTION
UPDATE DynamicFormMaster SET bitDeleted = 1 WHERE vcFormName IN ('Knowledge Base (Portal)','Documents (Portal)') 

SELECT * FROM dycFormField_Mapping WHERE numFormID = 82  AND vcAssociatedControlType = 'Popup'
UPDATE  dycFormField_Mapping SET bitSettingField = 0 WHERE numFormID = 82  AND vcAssociatedControlType = 'Popup'

UPDATE  dycFormField_Mapping SET bitSettingField = 0 WHERE numFormID = 78 AND vcFieldName IN ('Active','Billing Address','Linked Items','Tracking No','Shipping Address','Contact','Deal Status','Documents','Recurring Template','Share With')

UPDATE  dycFormField_Mapping SET bitSettingField = 0 WHERE numFormID = 79 AND vcFieldName IN ('Active','Billing Address','Linked Items','Tracking No','Shipping Address','Contact','Deal Status','Documents','Recurring Template','Share With')

SELECT * FROM dycFormField_Mapping WHERE numFormID = 80  AND vcAssociatedControlType = 'Popup'
UPDATE  dycFormField_Mapping SET bitSettingField = 0 WHERE numFormID = 80  AND vcAssociatedControlType = 'Popup'

UPDATE  dycFormField_Mapping SET bitSettingField = 0 
WHERE numFormID = 80 
 AND vcFieldName IN ('Email','Phone','')
ROLLBACK

/******************************************************************
Project: BACRM / PORTAL   Date: 17.Oct.2013
Comments: Add New Field to Hide Tabs from Portal
*******************************************************************/
BEGIN TRANSACTION
 ALTER TABLE DOmain ADD vcHideTabs VARCHAR(1000)
ROLLBACK

/******************************************************************
Project: PORTAL   Date: 11.Oct.2013
Comments: Added 3 new fields for Item List
*******************************************************************/
BEGIN TRANSACTION

INSERT INTO dbo.EmailMergeFields (vcMergeField,vcMergeFieldValue,numModuleID,tintModuleType)
SELECT 'ModelID','##ModelID##',2,1
UNION 
SELECT 'FirstPriceLevelValue','##FirstPriceLevelValue##',2,1
UNION 
SELECT 'FirstPriceLevelDiscount','##FirstPriceLevelDiscount##',2,1
	
ROLLBACK

/******************************************************************
Project: PORTAL   Date: 07.Oct.2013
Comments: Removed Default PRM from AuthenticationGroupMaster
*******************************************************************/
BEGIN TRANSACTION
DELETE FROM AuthenticationGroupMaster WHERE tintGroupType = 3
ROLLBACK

