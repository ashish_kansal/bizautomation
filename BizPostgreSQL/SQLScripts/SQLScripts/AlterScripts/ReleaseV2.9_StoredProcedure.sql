/****** Object:  StoredProcedure [dbo].[USP_EcommerceSettings]    Script Date: 03/25/2009 16:46:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ecommercesettings')
DROP PROCEDURE usp_ecommercesettings
GO
CREATE PROCEDURE [dbo].[USP_EcommerceSettings]              
@numDomainID as numeric(9),              
@vcPaymentGateWay as int ,                
@vcPGWUserId as varchar(100)='' ,                
@vcPGWPassword as varchar(100),              
@bitShowInStock as bit,              
@bitShowQOnHand as bit,        
--@tintColumns as tinyint,      
--@numCategory as numeric(9),
--@bitHideNewUsers as bit,
@bitCheckCreditStatus as BIT,
@numDefaultWareHouseID as numeric(9),
@numDefaultIncomeChartAcntId as numeric(9),
@numDefaultAssetChartAcntId as numeric(9),
@numDefaultCOGSChartAcntId as numeric(9),
@numRelationshipId as numeric(9),
@numProfileId as numeric(9),
@bitEnableDefaultAccounts BIT,
--@bitEnableCreditCart	BIT,
@bitHidePriceBeforeLogin	BIT,
@bitHidePriceAfterLogin	BIT,
@numRelationshipIdHidePrice	numeric(18, 0),
@numProfileIDHidePrice	numeric(18, 0),
--@numBizDocForCreditCard numeric(18, 0),
--@numBizDocForCreditTerm numeric(18, 0),
@bitAuthOnlyCreditCard BIT=0,
@bitSendMail BIT = 1,
@vcGoogleMerchantID VARCHAR(1000)  = '',
@vcGoogleMerchantKey VARCHAR(1000) = '',
@IsSandbox BIT ,
@numAuthrizedOrderStatus NUMERIC,
@numSiteID numeric(18, 0),
@vcPaypalUserName VARCHAR(50)  = '',
@vcPaypalPassword VARCHAR(50)  = '',
@vcPaypalSignature VARCHAR(500)  = '',
@IsPaypalSandbox BIT,
@bitSkipStep2 BIT = 0,
@bitDisplayCategory BIT = 0,
@bitShowPriceUsingPriceLevel BIT = 0,
@bitEnableSecSorting BIT = 0,
@bitSortPriceMode   BIT=0,
@numPageSize    INT=15,
@numPageVariant  INT=6,
@bitAutoSelectWarehouse BIT =0 
as              
            
            
if not exists(select 'col1' from eCommerceDTL where numDomainID=@numDomainID)            
INSERT INTO eCommerceDTL
           (numDomainId,
            vcPaymentGateWay,
            vcPGWManagerUId,
            vcPGWManagerPassword,
            bitShowInStock,
            bitShowQOnHand,
            --tintItemColumns,
           -- numDefaultCategory,
            numDefaultWareHouseID,
            numDefaultIncomeChartAcntId,
            numDefaultAssetChartAcntId,
            numDefaultCOGSChartAcntId,
            --bitHideNewUsers,
            bitCheckCreditStatus,
            [numRelationshipId],
            [numProfileId],
            [bitEnableDefaultAccounts],
 --           [bitEnableCreditCart],
            [bitHidePriceBeforeLogin],
            [bitHidePriceAfterLogin],
            [numRelationshipIdHidePrice],
            [numProfileIDHidePrice],
--            numBizDocForCreditCard,
--            numBizDocForCreditTerm,
            bitAuthOnlyCreditCard,
--            numAuthrizedOrderStatus,
            bitSendEmail,
            vcGoogleMerchantID,
            vcGoogleMerchantKey,
            IsSandbox ,
            numAuthrizedOrderStatus,
			numSiteId,
			vcPaypalUserName,
			vcPaypalPassword,
			vcPaypalSignature,
			IsPaypalSandbox,
			bitSkipStep2,
			bitDisplayCategory,
			bitShowPriceUsingPriceLevel,
			bitEnableSecSorting,
			bitSortPriceMode,
			numPageSize,
			numPageVariant,
			bitAutoSelectWarehouse
			)

VALUES     (@numDomainID,
            @vcPaymentGateWay,
            @vcPGWUserId,
            @vcPGWPassword,
            @bitShowInStock,
            @bitShowQOnHand,
           -- @tintColumns,
           -- @numCategory,
            @numDefaultWareHouseID,
            @numDefaultIncomeChartAcntId,
            @numDefaultAssetChartAcntId,
            @numDefaultCOGSChartAcntId,
           -- @bitHideNewUsers,
            @bitCheckCreditStatus,
            @numRelationshipId,
            @numProfileId,
            @bitEnableDefaultAccounts,
 --           @bitEnableCreditCart,
            @bitHidePriceBeforeLogin,
            @bitHidePriceAfterLogin,
            @numRelationshipIdHidePrice,
            @numProfileIDHidePrice,
--            @numBizDocForCreditCard,
--            @numBizDocForCreditTerm,
            @bitAuthOnlyCreditCard,
--            @numAuthrizedOrderStatus,
            @bitSendMail,
            @vcGoogleMerchantID,
            @vcGoogleMerchantKey ,
            @IsSandbox,
            @numAuthrizedOrderStatus,
			@numSiteID,
			@vcPaypalUserName,
			@vcPaypalPassword,
			@vcPaypalSignature,
			@IsPaypalSandbox,
			@bitSkipStep2,
			@bitDisplayCategory,
			@bitShowPriceUsingPriceLevel,
			@bitEnableSecSorting,
			@bitSortPriceMode,
			@numPageSize,
			@numPageVariant,
			@bitAutoSelectWarehouse
            )
 else            
 update eCommerceDTL                                   
   set              
 vcPaymentGateWay=@vcPaymentGateWay,                
 vcPGWManagerUId=@vcPGWUserId ,                
 vcPGWManagerPassword= @vcPGWPassword,              
 bitShowInStock=@bitShowInStock ,               
 bitShowQOnHand=@bitShowQOnHand,        
 --tintItemColumns= @tintColumns,      
 --numDefaultCategory=@numCategory,
 numDefaultWareHouseID =@numDefaultWareHouseID,
 numDefaultIncomeChartAcntId =@numDefaultIncomeChartAcntId ,
 numDefaultAssetChartAcntId =@numDefaultAssetChartAcntId ,
 numDefaultCOGSChartAcntId =@numDefaultCOGSChartAcntId ,
 --bitHideNewUsers =@bitHideNewUsers,
 bitCheckCreditStatus=@bitCheckCreditStatus ,
 numRelationshipId = @numRelationshipId,
 numProfileId = @numProfileId,
 bitEnableDefaultAccounts = @bitEnableDefaultAccounts,
 --[bitEnableCreditCart]=@bitEnableCreditCart,
 [bitHidePriceBeforeLogin]=@bitHidePriceBeforeLogin,
 [bitHidePriceAfterLogin]=@bitHidePriceAfterLogin,
 [numRelationshipIdHidePrice]=@numRelationshipIdHidePrice,
 [numProfileIDHidePrice]=@numProfileIDHidePrice,
-- numBizDocForCreditCard=@numBizDocForCreditCard,
-- numBizDocForCreditTerm=@numBizDocForCreditTerm,
 bitAuthOnlyCreditCard=@bitAuthOnlyCreditCard,
-- numAuthrizedOrderStatus=@numAuthrizedOrderStatus,
 bitSendEmail = @bitSendMail,
 vcGoogleMerchantID = @vcGoogleMerchantID ,
 vcGoogleMerchantKey =  @vcGoogleMerchantKey ,
 IsSandbox = @IsSandbox,
 numAuthrizedOrderStatus = @numAuthrizedOrderStatus,
	numSiteId = @numSiteID,
vcPaypalUserName = @vcPaypalUserName ,
vcPaypalPassword = @vcPaypalPassword,
vcPaypalSignature = @vcPaypalSignature,
IsPaypalSandbox = @IsPaypalSandbox,
bitSkipStep2 = @bitSkipStep2,
bitDisplayCategory = @bitDisplayCategory,
bitShowPriceUsingPriceLevel = @bitShowPriceUsingPriceLevel,
bitEnableSecSorting = @bitEnableSecSorting,
bitSortPriceMode=@bitSortPriceMode,
numPageSize=@numPageSize,
numPageVariant=@numPageVariant,
bitAutoSelectWarehouse=@bitAutoSelectWarehouse
 where numDomainId=@numDomainID
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetDataForIndexing' ) 
    DROP PROCEDURE USP_GetDataForIndexing
GO
-- exec USP_GetDataForIndexing @tintMode=0,@numDomainID=172,@numSiteID=90,@bitSchemaOnly=0,@tintAddUpdateMode=1,@bitDeleteAll=1
CREATE PROCEDURE USP_GetDataForIndexing
    @tintMode AS TINYINT = 0,
    @numDomainID AS NUMERIC,
    @numSiteID AS NUMERIC,
    @bitSchemaOnly AS BIT=0,
    @tintAddUpdateMode AS TINYINT, -- 1 = add , 2= update ,3=Deleted items
    @bitDeleteAll BIT=0 --Used for rebuilding all index from scratch
AS
BEGIN
    IF @tintMode = 0 -- items 
        BEGIN
        DECLARE @strSQL VARCHAR(4000)
        DECLARE @Fields VARCHAR(1000)
        DECLARE @numFldID VARCHAR(15)
        DECLARE @vcFldname VARCHAR(200)
        DECLARE @vcDBFldname VARCHAR(200)
        DECLARE @vcLookBackTableName VARCHAR(200)
        DECLARE @intRowNum INT
        DECLARE @strSQLUpdate VARCHAR(4000)
        DECLARE @Custom AS BIT
--SELECT * FROM View_DynamicColumns WHERE numFormid=30


--Select fields set for simple search from e-com settings as well fields from advance search
					SELECT  ROW_NUMBER() 
        OVER (ORDER BY vcFormFieldName) AS tintOrder,*
					INTO    #tempCustomField
					FROM    ( SELECT   distinct numFieldID as numFormFieldId,
										vcDbColumnName,
										vcFieldName as vcFormFieldName,
										0 AS Custom,
										vcLookBackTableName
							  FROM      View_DynamicColumns
							  WHERE     numFormId = 30
										AND numDomainID = @numDomainID
--										AND DFC.tintPageType = 1 --Commented so can agreegate all fields of simple and advance search
										AND isnull(numRelCntType,0) = 0
										AND isnull(bitCustom,0) = 0
										AND ISNULL(bitSettingField, 0) = 1
							  UNION
							  SELECT    numFieldID as numFormFieldId,
										vcFieldName,
										vcFieldName,
										1 AS Custom,
										'' vcLookBackTableName
							  FROM      View_DynamicCustomColumns
							  WHERE     Grp_id = 5
										AND numFormId = 30
										AND numDomainID = @numDomainID
										AND isnull(numRelCntType,0) = 0
--										AND isnull(DFC.bitCustom,0) = 1
--										AND ISNULL(DFC.tintPageType,0) = 1 --Commented so can agreegate all fields of simple and advance search

							) X
       
       IF @bitSchemaOnly =1
       BEGIN
			SELECT * FROM #tempCustomField
	   		RETURN
	   END
	   IF @bitDeleteAll = 1
	   BEGIN
	   		DELETE FROM LuceneItemsIndex WHERE numDomainID = @numDomainID AND numSiteID = @numSiteID
	   END
       
       --Create Fields query 
		 SET @Fields = '';
         SELECT TOP 1  @intRowNum = (tintOrder +1), @numFldID=numFormFieldId,@vcFldname=vcFormFieldName,@vcDBFldName=vcDbColumnName,@vcLookBackTableName=vcLookBackTableName
				FROM #tempCustomField WHERE Custom=0 ORDER BY tintOrder
				while @intRowNum>0                                                                                  
				 BEGIN
--					PRINT @numFldID
					IF(@vcDBFldName='numItemCode')
					set @vcDBFldName ='I.numItemCode'
				 
					SET @Fields = @Fields + ','  + @vcDBFldName + ' as ['  + @vcFldname + ']' 
					 

						SELECT TOP 1 @intRowNum = (tintOrder +1), @numFldID=numFormFieldId,@vcFldname=vcFormFieldName ,@vcDBFldName=vcDbColumnName,@vcLookBackTableName=vcLookBackTableName
						FROM #tempCustomField WHERE tintOrder >=@intRowNum AND Custom=0 ORDER BY tintOrder
							           
						if @@rowcount=0 set @intRowNum=0                                                                                          
				 END
       
--       PRINT @Fields
       
       
       SET @strSQL =  'SELECT  I.numItemCode AS numItemCode
			' + @Fields + '            
            INTO #DataForIndexing
            FROM    dbo.Item I 
            LEFT JOIN dbo.ItemExtendedDetails IED
            ON I.numItemCode = IED.numItemCode
            WHERE   I.numDomainID = ' + convert(varchar(10),@numDomainID) + '
                    AND I.numItemCode IN (
                    SELECT  numItemID
                    FROM    dbo.ItemCategory
                    WHERE   numCategoryID IN ( SELECT   numCategoryID
                                               FROM     dbo.SiteCategories
                                               WHERE    numSiteID = ' + convert(varchar(10),@numSiteID) +' )) '
            
        IF @tintAddUpdateMode = 1 -- Add mode
			SET @strSQL = @strSQL +' and I.numItemCode Not in (SELECT numItemCode FROM LuceneItemsIndex WHERE numDomainID = '+ CONVERT(VARCHAR(10),@numDomainID) +  ' and numSiteID = ' + CONVERT(VARCHAR(10),@numSiteID) + ') '
		IF @tintAddUpdateMode = 2 -- Update mode
			SET @strSQL = @strSQL +' and bintModifiedDate BETWEEN  DATEADD( hour,-1,GETUTCDATE()) AND GETUTCDATE() 
			AND I.numItemCode in(SELECT I1.numItemCode FROM dbo.Item I1 INNER JOIN dbo.ItemCategory IC ON I1.numItemCode = IC.numItemID
			INNER JOIN dbo.SiteCategories SC ON IC.numCategoryID = SC.numCategoryID
			WHERE SC.numSiteID = ' + CONVERT(VARCHAR(10),@numSiteID) + ' AND I1.numDomainID= ' + CONVERT(VARCHAR(10),@numDomainID) + ')'
		IF @tintAddUpdateMode = 3 -- Delete mode
		BEGIN
			SET @strSQL = 'SELECT numItemCode FROM LuceneItemsIndex WHERE bitItemDeleted=1 and numDomainID = '+ CONVERT(VARCHAR(10),@numDomainID) +  ' and numSiteID = ' + CONVERT(VARCHAR(10),@numSiteID) 
			EXEC(@strSQL);
			RETURN
		END
			
       
     
       
       

				SET @strSQLUpdate=''
			--Add  custom Fields and data to table
				SELECT TOP 1  @intRowNum = (tintOrder +1), @numFldID=numFormFieldId,@vcFldname=vcFormFieldName
				FROM #tempCustomField WHERE Custom=1 ORDER BY tintOrder
				while @intRowNum>0                                                                                  
				 BEGIN
--				 PRINT @numFldID
					
				 			SET @strSQLUpdate = @strSQLUpdate + 'alter table #DataForIndexing add ['+@vcFldname+'_CustomField] varchar(500);'
							set @strSQLUpdate=@strSQLUpdate+ 'update #DataForIndexing set [' +@vcFldname + '_CustomField]=dbo.GetCustFldValueItem('+@numFldID+',numItemCode) where numItemCode>0'
							
					 

						SELECT TOP 1 @intRowNum = (tintOrder +1), @numFldID=numFormFieldId,@vcFldname=vcFormFieldName
						FROM #tempCustomField WHERE tintOrder >=@intRowNum AND Custom=1 ORDER BY tintOrder
							           
						if @@rowcount=0 set @intRowNum=0
				 END
				 SET @strSQLUpdate = @strSQLUpdate + ' SELECT * FROM #DataForIndexing'
				 
				 
				 IF @bitSchemaOnly=0
				 BEGIN
				 	SET @strSQLUpdate = @strSQLUpdate + ' INSERT INTO LuceneItemsIndex
				 	SELECT numItemCode,1,0,'+ CONVERT(VARCHAR(10), @numDomainID) +','+ CONVERT(VARCHAR(10), @numSiteID) +'  FROM #DataForIndexing where numItemCode not in (select numItemCode from LuceneItemsIndex where numDomainID ='+ CONVERT(VARCHAR(10), @numDomainID) +' and numSiteID = ' + CONVERT(VARCHAR(10),@numSiteID) + ' )'
				 END
				 
				 
				 SET @strSQLUpdate = @strSQLUpdate + ' DROP TABLE #DataForIndexing
												DROP TABLE #tempCustomField '
				 
				 SET @strSQL =@strSQL + @strSQLUpdate;
				 
				 
				 PRINT @strSQL
				 exec (@strSQL)
				 
				 
				 
				 
			
        END
        
	IF @tintMode= 1 
	BEGIN
		RETURN	
	END
	
	IF @tintMode= 2
	BEGIN
		RETURN	
	END
	
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetECommerceDetails')
DROP PROCEDURE USP_GetECommerceDetails
GO
CREATE PROCEDURE [dbo].[USP_GetECommerceDetails]      
@numDomainID as numeric(9)=0                              
as                              
select                               
isnull(vcPaymentGateWay,'0') as vcPaymentGateWay,                
isnull(vcPGWManagerUId,'')as vcPGWManagerUId ,                
isnull(vcPGWManagerPassword,'') as vcPGWManagerPassword,              
isnull(bitShowInStock,0) as bitShowInStock,              
isnull(bitShowQOnHand,0) as bitShowQOnHand,        
--isnull(tintItemColumns,0) as tintItemColumns,
--isnull(numDefaultCategory,0) as numDefaultCategory,  
isnull([numDefaultWareHouseID],0) numDefaultWareHouseID,
isnull([numDefaultIncomeChartAcntId],0) numDefaultIncomeChartAcntId,
isnull([numDefaultAssetChartAcntId],0) numDefaultAssetChartAcntId,
isnull([numDefaultCOGSChartAcntId],0) numDefaultCOGSChartAcntId,
--isnull(bitHideNewUsers,0) as bitHideNewUsers,
isnull(bitCheckCreditStatus,0) as bitCheckCreditStatus,
isnull([numRelationshipId],0) numRelationshipId,
isnull([numProfileId],0) numProfileId,
isnull(bitEnableDefaultAccounts,0) as bitEnableDefaultAccounts,
--ISNULL([bitEnableCreditCart],0) AS bitEnableCreditCart,
ISNULL([bitHidePriceBeforeLogin],0) AS bitHidePriceBeforeLogin,
ISNULL([bitHidePriceAfterLogin],0) AS bitHidePriceAfterLogin,
ISNULL([numRelationshipIdHidePrice],0) AS numRelationshipIdHidePrice,
ISNULL([numProfileIDHidePrice],0) AS numProfileIDHidePrice,
ISNULL([numBizDocForCreditCard],0) AS numBizDocForCreditCard,
ISNULL([numBizDocForCreditTerm],0) AS numBizDocForCreditTerm,
ISNULL(bitAuthOnlyCreditCard,0) AS bitAuthOnlyCreditCard,
ISNULL(numAuthrizedOrderStatus,0) AS numAuthrizedOrderStatus,
ISNULL(bitSendEmail,1) AS bitSendEmail,
ISNULL(vcGoogleMerchantID , '') AS vcGoogleMerchantID,
ISNULL(vcGoogleMerchantKey,'') AS vcGoogleMerchantKey,
ISNULL(IsSandbox,0) AS IsSandbox,
ISNULL(numSiteID,0) AS numSiteID,
ISNULL(vcPaypalUserName,'') AS vcPaypalUserName,
ISNULL(vcPaypalPassword,'') AS vcPaypalPassword,
ISNULL(vcPaypalSignature,'') AS vcPaypalSignature,
ISNULL(IsPaypalSandbox,0) AS IsPaypalSandbox,
ISNULL(bitSkipStep2,0) AS [bitSkipStep2],
ISNULL(bitDisplayCategory,0) AS [bitDisplayCategory],
ISNULL(bitShowPriceUsingPriceLevel,0) AS [bitShowPriceUsingPriceLevel],
ISNULL(bitEnableSecSorting,0) AS [bitEnableSecSorting],
ISNULL(bitSortPriceMode,0) AS [bitSortPriceMode],
ISNULL(numPageSize,0) AS [numPageSize],
ISNULL(numPageVariant,0) AS [numPageVariant],
ISNULL(bitAutoSelectWarehouse,0) AS bitAutoSelectWarehouse
FROM eCommerceDTL 
WHERE numDomainID=@numDomainID
/****** Object:  StoredProcedure [dbo].[usp_GetLayoutInfoDdl]    Script Date: 07/26/2008 16:17:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
exec usp_GetLayoutInfoDdl @numUserCntId=85098,@intcoulmn=0,@numDomainID=110,@Ctype='P',@PageId=1,@numRelation=46
exec usp_GetLayoutInfoDdl @numUserCntId=85098,@intcoulmn=1,@numDomainID=110,@Ctype='P',@PageId=1,@numRelation=46
*/
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getlayoutinfoddl')
DROP PROCEDURE usp_getlayoutinfoddl
GO
CREATE PROCEDURE [dbo].[usp_GetLayoutInfoDdl]                                
@numUserCntId as numeric(9)=0,                              
@intcoulmn as numeric(9),                              
@numDomainID as numeric(9)=0 ,                    
--@Ctype as char ,              
@PageId as numeric(9)=4,              
@numRelation as numeric(9)=0,
@numFormID as numeric(9)=0,
@tintPageType TINYINT             
as                        
                      
 if( @intcoulmn <> 0 )--Fields added to layout
begin                         
                        
         
SELECT ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,numFieldId,
convert(char(1),bitCustom) as bitCustomField,tintRow,bitRequired
FROM View_DynamicColumns
WHERE numUserCntID=@numUserCntID and  numDomainID=@numDomainID  and tintColumn = @intcoulmn      
 and numFormId=@numFormID and ISNULL(bitCustom,0)=0 and numRelCntType=@numRelation 
 AND tintPageType=@tintPageType AND 
  1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
		  WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
		  ELSE 0 END)  order by tintRow  
       
if @pageid =1 or @pageid =4  OR @pageid =12 OR @pageid =13 OR @pageid =14
 begin    
 
 select vcfieldName,numFieldId,'1' as bitCustomField,tintRow,bitIsRequired AS bitRequired
  from View_DynamicCustomColumns_RelationShip
 where grp_id=@PageId and numRelation=@numRelation and numDomainID=@numDomainID and subgrp=0           
 and numUserCntID=@numUserCntID and tintColumn = @intcoulmn
 AND numFormId=@numFormID and ISNULL(bitCustom,0)=1 and numRelCntType=@numRelation AND tintPageType=@tintPageType
  
end      
      
if @PageId= 2 or  @PageId= 3 or @PageId= 5 or @PageId= 6 or @PageId= 7 or @PageId= 8    or @PageId= 11               
begin 

 select vcfieldName,numFieldId,'1' as bitCustomField,tintRow,bitIsRequired AS bitRequired
  from View_DynamicCustomColumns
 where grp_id=@PageId and numDomainID=@numDomainID and subgrp=0           
 and numUserCntID=@numUserCntID and  tintColumn = @intcoulmn
 AND numFormId=@numFormID and ISNULL(bitCustom,0)=1 and numRelCntType=@numRelation AND tintPageType=@tintPageType

end       
                      
end                        
                        
                          
if @intcoulmn = 0 --Available Fields
begin                          
                      
 
 SELECT ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,numFieldId,'0' as bitCustomField,bitRequired
FROM View_DynamicDefaultColumns
 WHERE numFormId=@numFormID AND numDomainID=@numDomainID  AND
   1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
		 WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END
		 ELSE 0 END)
 AND numFieldId NOT IN 
 (SELECT numFieldId FROM View_DynamicColumns
WHERE numUserCntID=@numUserCntID and  numDomainID=@numDomainID       
 and numFormId=@numFormID and ISNULL(bitCustom,0)=0 and numRelCntType=@numRelation 
 AND tintPageType=@tintPageType AND 1=(CASE WHEN @tintPageType=2 THEN Case WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
		 WHEN @tintPageType=3 THEN Case WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
		 ELSE 0 END))

       
if @pageid=4 or @pageid=1  OR @pageid =12 OR @pageid =13  OR @pageid =14                
begin           

 select fld_label as vcfieldName ,fld_id as numFieldId,'1' as bitCustomField,CAST(0 as bit) AS bitRequired
  from CFW_Fld_Master CFM join CFW_Fld_Dtl CFD on Fld_id=numFieldId 
						  left join CFw_Grp_Master on subgrp=CFM.Grp_id                              
 where CFM.grp_id=@PageId and CFD.numRelation=@numRelation and CFM.numDomainID=@numDomainID and subgrp=0 
 AND fld_id not IN (SELECT numFieldId FROM View_DynamicCustomColumns_RelationShip where           
 numUserCntID=@numUserCntID and  numDomainID=@numDomainID  
 AND numFormId=@numFormID and ISNULL(bitCustom,0)=1 and numRelCntType=@numRelation AND tintPageType=@tintPageType)
 order by subgrp,numOrder 

end        
      
if @PageId= 2 or  @PageId= 3 or @PageId= 5 or @PageId= 6 or @PageId= 7 or @PageId= 8   or @PageId= 11                
begin 

 select fld_label as vcfieldName ,fld_id as numFieldId,'1' as bitCustomField,CAST(0 as bit) AS bitRequired
  from CFW_Fld_Master CFM left join CFw_Grp_Master on subgrp=CFM.Grp_id                              
 where CFM.grp_id=@PageId and CFM.numDomainID=@numDomainID and subgrp=0 
 AND fld_id not IN (SELECT numFieldId FROM View_DynamicCustomColumns where           
 numUserCntID=@numUserCntID and  numDomainID=@numDomainID  
 AND numFormId=@numFormID and ISNULL(bitCustom,0)=1 and numRelCntType=@numRelation AND tintPageType=@tintPageType)
 order by subgrp 
                          
end       
      
end
GO
GO
/****** Object:  StoredProcedure [dbo].[USP_ItemDetails]    Script Date: 02/07/2010 15:31:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itemdetails')
DROP PROCEDURE usp_itemdetails
GO
CREATE PROCEDURE [dbo].[USP_ItemDetails]                                                  
@numItemCode as numeric(9) ,          
@ClientTimeZoneOffset as int                                               
as                                                 
   
   DECLARE @bitItemIsUsedInOrder AS BIT;SET @bitItemIsUsedInOrder=0
   
   DECLARE @numDomainId AS NUMERIC
   SELECT @numDomainId=numDomainId FROM Item WHERE numItemCode=@numItemCode
   
   IF GETUTCDATE()<'2013-03-15 23:59:39'
		SET @bitItemIsUsedInOrder=0
   ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) >0
		SET @bitItemIsUsedInOrder=1
   ELSE IF EXISTS(SELECT numItemID FROM dbo.General_Journal_Details WHERE numItemID= @numItemCode and numDomainId=@numDomainId AND chBizDocItems='IA1'   )
		set @bitItemIsUsedInOrder =1
                                               
select I.numItemCode, vcItemName, ISNULL(txtItemDesc,'') txtItemDesc,CAST( ISNULL(IED.txtDesc,'')  AS VARCHAR(MAX) ) vcExtendedDescToAPI, charItemType, monListPrice,                   
numItemClassification, isnull(bitTaxable,0) as bitTaxable, vcSKU AS vcSKU, isnull(bitKitParent,0) as bitKitParent,--, dtDateEntered,                  
 numVendorID, I.numDomainID, numCreatedBy, bintCreatedDate, bintModifiedDate,                   
numModifiedBy,
(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1 ) AS vcPathForImage ,
(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1) AS  vcPathForTImage, isnull(bitSerialized,0) as bitSerialized, vcModelID,                   
(SELECT numItemImageId,vcPathForImage,vcPathForTImage,bitDefault,case when bitdefault=1 then -1 
else isnull(intDisplayOrder,0) end as intDisplayOrder 
FROM ItemImages  
WHERE numItemCode=@numItemCode order by case when bitdefault=1 then -1 else isnull(intDisplayOrder,0) end  asc 
FOR XML AUTO,ROOT('Images')) AS xmlItemImages,
(SELECT STUFF((SELECT ',' + CONVERT(VARCHAR(50) , numCategoryID)  FROM dbo.ItemCategory WHERE numItemID = I.numItemCode FOR XML PATH('')), 1, 1, '')) AS vcItemCategories ,
numItemGroup, numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, monAverageCost,                   
monCampaignLabourCost,dbo.fn_GetContactName(numCreatedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,bintCreatedDate )) as CreatedBy ,                                      
dbo.fn_GetContactName(numModifiedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,bintModifiedDate )) as ModifiedBy,                      
sum(numOnHand) as numOnHand,                      
sum(numOnOrder) as numOnOrder,                      
sum(numReorder)  as numReorder,                      
sum(numAllocation)  as numAllocation,                      
sum(numBackOrder)  as numBackOrder,                
isnull(fltWeight,0) as fltWeight,                
isnull(fltHeight,0) as fltHeight,                
isnull(fltWidth,0) as fltWidth,                
isnull(fltLength,0) as fltLength,                
isnull(bitFreeShipping,0) as bitFreeShipping,              
isnull(bitAllowBackOrder,0) as bitAllowBackOrder,              
isnull(vcUnitofMeasure,'Units') as vcUnitofMeasure,      
isnull(bitShowDeptItem,0) bitShowDeptItem,      
isnull(bitShowDeptItemDesc,0) bitShowDeptItemDesc,  
isnull(bitCalAmtBasedonDepItems ,0) bitCalAmtBasedonDepItems,
isnull(bitAssembly ,0) bitAssembly ,
isnull(cast(numBarCodeId as varchar(50)),'') as  numBarCodeId ,
isnull(I.vcManufacturer,'') as vcManufacturer,
ISNULL(I.numBaseUnit,0) AS numBaseUnit,ISNULL(I.numPurchaseUnit,0) AS numPurchaseUnit,ISNULL(I.numSaleUnit,0) AS numSaleUnit,
dbo.fn_GetUOMName(ISNULL(I.numBaseUnit,0)) AS vcBaseUnit,dbo.fn_GetUOMName(ISNULL(I.numPurchaseUnit,0)) AS vcPurchaseUnit,dbo.fn_GetUOMName(ISNULL(I.numSaleUnit,0)) AS vcSaleUnit,
isnull(bitLotNo,0) as bitLotNo,
ISNULL(IsArchieve,0) AS IsArchieve,
case when charItemType='P' then 'Inventory Item' when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory Item' end as ItemType,
isnull(I.numItemClass,0) as numItemClass,
ISNULL(tintStandardProductIDType,0) tintStandardProductIDType,
ISNULL(vcExportToAPI,'') vcExportToAPI,
ISNULL(numShipClass,0) numShipClass,ISNULL(I.bitAllowDropShip,0) AS bitAllowDropShip,
ISNULL(@bitItemIsUsedInOrder,0) AS bitItemIsUsedInOrder,
ISNULL((SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID = I.numItemCode),0) AS intParentKitItemCount,
ISNULL(I.bitArchiveItem,0) AS [bitArchiveItem]
from Item I                    
left join  WareHouseItems W                  
on W.numItemID=I.numItemCode   
LEFT JOIN ItemExtendedDetails IED   ON I.numItemCode = IED.numItemCode               
where I.numItemCode=@numItemCode  group by I.numItemCode, vcItemName, txtItemDesc, charItemType, monListPrice,                   
numItemClassification, bitTaxable,vcSKU, bitKitParent,numVendorID, I.numDomainID,               
numCreatedBy, bintCreatedDate, bintModifiedDate,                   
numModifiedBy,bitAllowBackOrder, bitSerialized, vcModelID,                   
numItemGroup, numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, monAverageCost,                   
monCampaignLabourCost,fltWeight,fltHeight,fltWidth,fltLength,bitFreeShipping,vcUnitofMeasure,bitShowDeptItemDesc,bitShowDeptItem,bitCalAmtBasedonDepItems,bitAssembly,numBarCodeId,I.vcManufacturer
	,I.numBaseUnit,I.numPurchaseUnit,I.numSaleUnit,I.bitLotNo,I.IsArchieve,I.numItemClass,I.tintStandardProductIDType,I.vcExportToAPI,I.numShipClass,CAST( ISNULL(IED.txtDesc,'')  AS VARCHAR(MAX) ),I.bitAllowDropShip,I.bitArchiveItem 


--exec USP_ItemDetails @numItemCode = 197605 ,@ClientTimeZoneOffset =330

--Created By Anoop Jayaraj          
--exec USP_ItemDetailsForEcomm @numItemCode=859064,@numWareHouseID=1074,@numDomainID=172,@numSiteId=90
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itemdetailsforecomm')
DROP PROCEDURE USP_ItemDetailsForEcomm
GO
CREATE PROCEDURE [dbo].[USP_ItemDetailsForEcomm]
@numItemCode as numeric(9),          
@numWareHouseID as numeric(9),    
@numDomainID as numeric(9),
@numSiteId as numeric(9)                                              
as     
/*Removed warehouse parameter, and taking Default warehouse by default from DB*/
    
declare @bitShowInStock as bit        
declare @bitShowQuantity as BIT
declare @bitAutoSelectWarehouse as bit        
--declare @tintColumns as tinyint        
DECLARE @numDefaultWareHouseID as numeric(9)

set @bitShowInStock=0        
set @bitShowQuantity=0        
--set @tintColumns=1        
        
select @bitShowInStock=isnull(bitShowInStock,0),@bitShowQuantity=isnull(bitShowQOnHand,0),@bitAutoSelectWarehouse=ISNULL(bitAutoSelectWarehouse,0)--,@tintColumns=isnull(tintItemColumns,1)        
from eCommerceDTL where numDomainID=@numDomainID
IF @numWareHouseID=0
BEGIN
	SELECT @numDefaultWareHouseID=ISNULL(numDefaultWareHouseID,0) FROM [eCommerceDTL] WHERE [numDomainID]=@numDomainID	
	SET @numWareHouseID =@numDefaultWareHouseID;
END
/*If qty is not found in default warehouse then choose next warehouse with sufficient qty */
IF @bitAutoSelectWarehouse = 1 AND ( SELECT ISNULL(numOnHand,0) FROM dbo.WareHouseItems WHERE  numItemID = @numItemCode AND numWarehouseID= @numWareHouseID ) = 0 
BEGIN
	SELECT TOP 1 @numDefaultWareHouseID = numWareHouseID FROM dbo.WareHouseItems WHERE  numItemID = @numItemCode AND numOnHand>0 /*TODO: Pass actuall qty from cart*/ ORDER BY numOnHand DESC 
	IF (ISNULL(@numDefaultWareHouseID,0)>0)
		SET @numWareHouseID =@numDefaultWareHouseID;
END

      DECLARE @UOMConversionFactor AS DECIMAL(18,5)
      
      Select @UOMConversionFactor= ISNULL(dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)),0)
      FROM Item I where numItemCode=@numItemCode                                
 
 
declare @strSql as varchar(5000)
set @strSql=' With tblItem AS (                  
select I.numItemCode, vcItemName, txtItemDesc, charItemType,
ISNULL(CASE WHEN I.[charItemType]=''P'' 
			THEN CASE WHEN I.bitSerialized = 1 
					  THEN '+ convert(varchar(15),@UOMConversionFactor) + ' * monListPrice 
					  ELSE '+ convert(varchar(15),@UOMConversionFactor) + ' * W.[monWListPrice] 
				 END 
			ELSE '+ convert(varchar(15),@UOMConversionFactor) + ' * monListPrice 
	   END,0) monListPrice,
numItemClassification, bitTaxable, vcSKU, bitKitParent,              
I.numModifiedBy, (SELECT numItemImageId,vcPathForImage,vcPathForTImage,bitDefault,case when bitdefault=1  then -1 else isnull(intDisplayOrder,0) end as intDisplayOrder FROM ItemImages  WHERE numItemCode=I.numItemCode and bitIsImage = 1 order by case when bitdefault=1 then -1 else isnull(intDisplayOrder,0) end  asc FOR XML AUTO,ROOT(''Images''))  vcImages,
(SELECT vcPathForImage FROM ItemImages  WHERE numItemCode=I.numItemCode and bitIsImage = 0 order by intDisplayOrder  asc FOR XML AUTO,ROOT(''Documents''))  vcDocuments ,
 isnull(bitSerialized,0) as bitSerialized, vcModelID,                 
numItemGroup,                   
(isnull(sum(numOnHand),0) / '+ convert(varchar(15),@UOMConversionFactor) + ') as numOnHand,                    
sum(numOnOrder) as numOnOrder,                    
sum(numReorder)  as numReorder,                    
sum(numAllocation)  as numAllocation,                    
sum(numBackOrder)  as numBackOrder,              
isnull(fltWeight,0) as fltWeight,              
isnull(fltHeight,0) as fltHeight,              
isnull(fltWidth,0) as fltWidth,              
isnull(fltLength,0) as fltLength,              
case when charItemType=''P''then isnull(bitFreeShipping,0) else 1 end as bitFreeShipping,
Case When (case when charItemType=''P''then isnull(bitFreeShipping,0) else 1 end )=1 then ''Free shipping'' else ''Calculated at checkout'' end as Shipping,              
isnull(bitAllowBackOrder,0) as bitAllowBackOrder,bitShowInStock,bitShowQOnHand,isnull(numWareHouseItemID,0)  as numWareHouseItemID,    
(case when charItemType<>''S'' and charItemType<>''N'' then         
 (Case when '+ convert(varchar(15),@bitShowInStock) + '=1  then ( Case when numWareHouseItemID is null then ''<font color=red>Out Of Stock</font>'' when bitAllowBackOrder=1 then ''In Stock'' else         
 (Case when isnull(sum(numOnHand),0)>0 then ''In Stock''         
 else ''<font color=red>Out Of Stock</font>'' end ) end)    else '''' end) else '''' end) as InStock,
ISNULL(numSaleUnit,0) AS numUOM,
ISNULL(vcUnitName,'''') AS vcUOMName,
 '+ convert(varchar(15),@UOMConversionFactor) + ' AS UOMConversionFactor,
I.numCreatedBy,
I.[vcManufacturer],
(SELECT  COUNT(*) FROM  Review where vcReferenceId = CONVERT(VARCHAR(50) ,I.numItemCode) and numDomainId = I.numDomainId and numSiteId = '+ CONVERT(VARCHAR(20) , @numSiteId)  +' AND bitHide = 0 ) as ReviewCount ,
(SELECT  COUNT(*) FROM  Ratings where vcReferenceId = CONVERT(VARCHAR(50) ,I.numItemCode) and numDomainId = I.numDomainId and numSiteId = '+ CONVERT(VARCHAR(20) , @numSiteId)  +'  ) as RatingCount ,
(select top 1 vcMetaKeywords  from MetaTags where numReferenceID = I.numItemCode and tintMetaTagFor =2) as vcMetaKeywords,
(select top 1 vcMetaDescription  from MetaTags where numReferenceID = I.numItemCode and tintMetaTagFor =2) as vcMetaDescription ,
(SELECT vcCategoryName  FROM  dbo.Category WHERE numCategoryID =(SELECT TOP 1 numCategoryID FROM dbo.ItemCategory WHERE numItemID = I.numItemCode)) as vcCategoryName

from Item I                  
left join  WareHouseItems W                
on W.numItemID=I.numItemCode and numWareHouseID= '+ convert(varchar(15),@numWareHouseID) + '
left join  eCommerceDTL E          
on E.numDomainID=I.numDomainID     
LEFT JOIN UOM u ON u.numUOMId=I.numSaleUnit    
where I.numItemCode='+ convert(varchar(15),@numItemCode) + '             
group by I.numItemCode, vcItemName, txtItemDesc, charItemType, monListPrice,numItemClassification, bitTaxable, vcSKU, bitKitParent,           
I.numDomainID,I.numModifiedBy,  bitSerialized, vcModelID,numItemGroup, fltWeight,fltHeight,fltWidth,          
fltLength,bitFreeShipping,bitAllowBackOrder,bitShowInStock,bitShowQOnHand ,numWareHouseItemID,vcUnitName,I.numCreatedBy,W.[monWListPrice],I.[vcManufacturer],
numSaleUnit 

)'

--,
--


set @strSql=@strSql+' select numItemCode,vcItemName,txtItemDesc,charItemType,monListPrice,numItemClassification, bitTaxable, vcSKU, bitKitParent,                  
tblItem.numModifiedBy, vcImages,vcDocuments, bitSerialized, vcModelID, numItemGroup,numOnHand,numOnOrder,numReorder,numAllocation, 
numBackOrder, fltWeight, fltHeight, fltWidth, fltLength, bitFreeShipping,bitAllowBackOrder,bitShowInStock,
bitShowQOnHand,numWareHouseItemID,InStock,numUOM,vcUOMName,UOMConversionFactor,tblItem.numCreatedBy,vcManufacturer,ReviewCount,RatingCount,Shipping,isNull(vcMetaKeywords,'''') as vcMetaKeywords ,isNull(vcMetaDescription,'''') as vcMetaDescription,isNull(vcCategoryName,'''') as vcCategoryName'     

set @strSql=@strSql+ ' from tblItem'
PRINT @strSql
exec (@strSql)


declare @tintOrder as tinyint                                                  
declare @vcFormFieldName as varchar(50)                                                  
declare @vcListItemType as varchar(1)                                             
declare @vcAssociatedControlType varchar(10)                                                  
declare @numListID AS numeric(9)                                                  
declare @WhereCondition varchar(2000)                       
Declare @numFormFieldId as numeric  
DECLARE @vcFieldType CHAR(1)
                  
set @tintOrder=0                                                  
set @WhereCondition =''                 
                   
              
  CREATE TABLE #tempAvailableFields(numFormFieldId  numeric(9),vcFormFieldName NVARCHAR(50),
        vcFieldType CHAR(1),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),
        vcListItemType CHAR(1),intRowNum INT,vcItemValue VARCHAR(3000))
   
  INSERT INTO #tempAvailableFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
        ,numListID,vcListItemType,intRowNum)                         
            SELECT  Fld_id,REPLACE(Fld_label,' ','') AS vcFormFieldName,
                   CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType,
                    fld_type as vcAssociatedControlType,
                    ISNULL(C.numListID, 0) numListID,
                    CASE WHEN C.numListID > 0 THEN 'L'
                         ELSE ''
                    END vcListItemType,ROW_NUMBER() OVER (ORDER BY Fld_id asc) AS intRowNum
            FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
            WHERE   C.numDomainID = @numDomainId
                    AND GRP_ID IN (5)

     select top 1 @tintOrder=intRowNum,@numFormFieldId=numFormFieldId,@vcFormFieldName=vcFormFieldName,
				  @vcFieldType=vcFieldType,@vcAssociatedControlType=vcAssociatedControlType,@numListID=numListID,
				  @vcListItemType=vcListItemType
	from #tempAvailableFields order by intRowNum ASC
   
while @tintOrder>0                                                  
begin                                                  
    if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
    begin  
		UPDATE #tempAvailableFields SET vcItemValue=(SELECT Fld_Value FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
    end   
    else if @vcAssociatedControlType = 'CheckBox'
	begin      
		UPDATE #tempAvailableFields SET vcItemValue=(SELECT case when isnull(Fld_Value,0)=0 then 'No' when isnull(Fld_Value,0)=1 then 'Yes' END FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
   end                
   else if @vcAssociatedControlType = 'DateField'           
   begin   
		UPDATE #tempAvailableFields SET vcItemValue=(SELECT dbo.FormatedDateFromDate(Fld_Value,@numDomainId) FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
   end                
    else if @vcAssociatedControlType = 'SelectBox'           
   begin 
		UPDATE #tempAvailableFields SET vcItemValue=(SELECT L.vcData FROM CFW_FLD_Values_Item CFW left Join ListDetails L
			on L.numListItemID=CFW.Fld_Value                
			WHERE CFW.Fld_Id=@numFormFieldId AND CFW.RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
end          
               
 
    select top 1 @tintOrder=intRowNum,@numFormFieldId=numFormFieldId,@vcFormFieldName=vcFormFieldName,
				  @vcFieldType=vcFieldType,@vcAssociatedControlType=vcAssociatedControlType,@numListID=numListID,
				  @vcListItemType=vcListItemType
	from #tempAvailableFields WHERE intRowNum > @tintOrder order by intRowNum ASC
 
   if @@rowcount=0 set @tintOrder=0                                                  
end   


  
SELECT * FROM #tempAvailableFields

DROP TABLE #tempAvailableFields

--exec USP_ItemDetailsForEcomm @numItemCode=197611,@numWareHouseID=58,@numDomainID=1,@numSiteId=18
--exec USP_ItemDetailsForEcomm @numItemCode=735364,@numWareHouseID=1039,@numDomainID=156,@numSiteId=104
/****** Object:  StoredProcedure [dbo].[usp_UpdateSignature]    Script Date: 07/26/2008 16:21:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatesignature')
DROP PROCEDURE usp_updatesignature
GO
CREATE PROCEDURE [dbo].[usp_UpdateSignature]  
@numUserID as numeric(9),  
@txtSignature varchar(8000)=''  
as  
  
update UserMaster set txtSignature=@txtSignature where numUserId=@numUserID
GO
