/******************************************************************
Project: Release 8.4 Date: 31.OCTOBER.2017
Comments: ALTER SCRIPTS
*******************************************************************/

/******************   SANDEEP  *****************/

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAllowFiltering
)
VALUES
(
	39,'Units','numUnitHour','numUnitHour','ReturnItems','M','R','TextBox','',0,0,0,1,0,1,1
),
(
	39,'Unit Price','monPrice','monPrice','ReturnItems','M','R','TextBox','',0,0,0,1,0,1,1
),
(
	39,'Amount','monTotAmount','monTotAmount','ReturnItems','M','R','TextBox','',0,0,0,1,0,1,1
)

SET IDENTITY_INSERT ReportFieldGroupMaster ON

INSERT INTO ReportFieldGroupMaster
(
	numReportFieldGroupID,vcFieldGroupName,bitActive
)
VALUES
(
	33,'Return Items',1
)

SET IDENTITY_INSERT ReportFieldGroupMaster OFF

INSERT INTO ReportFieldGroupMappingMaster
(
	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
)
SELECT
	33,numFieldId,vcFieldName,vcAssociatedControlType,vcFieldDataType,1,1,1,1
FROM
	DycFieldMaster
WHERE
	vcLookBackTableName='ReturnItems'


INSERT INTO ReportModuleGroupFieldMappingMaster
(
	numReportModuleGroupID,numReportFieldGroupID
)
VALUES
(
	24,33
)

ALTER TABLE Warehouses ADD vcPrintNodeAPIKey VARCHAR(100),  vcPrintNodePrinterID VARCHAR(100)


ALTER TABLE OpportunityMaster ADD numShipFromWarehouse NUMERIC(18,0)