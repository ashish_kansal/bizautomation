/******************************************************************
Stored Procedures
Project: Release 3.0 Date: 31.05.2014
Comments: 
*******************************************************************/



GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'FN'
                    AND NAME = 'CheckOrderInventoryStatus' ) 
    DROP FUNCTION CheckOrderInventoryStatus
GO
CREATE FUNCTION CheckOrderInventoryStatus
    (
      @numOppID AS NUMERIC(9),
      @numDomainID AS NUMERIC(9)
    )
RETURNS NVARCHAR(MAX)
AS BEGIN
	DECLARE @vcInventoryStatus AS NVARCHAR(MAX);SET @vcInventoryStatus='<font color="#000000">Not Applicable<font>';
	
	IF EXISTS(SELECT tintshipped FROM dbo.OpportunityMaster WHERE numDomainId=@numDomainID 
			  AND numOppId=@numOppID AND ISNULL(tintshipped,0)=1)
	BEGIN
		SET @vcInventoryStatus='<font color="#008000">Allocation Cleared<font>';
	END
	ELSE
	BEGIN
		DECLARE @ItemCount AS INT;SET @ItemCount=0
		DECLARE @BackOrderItemCount AS INT;SET @BackOrderItemCount=0
    
		SELECT @ItemCount=COUNT(*),
			@BackOrderItemCount=SUM(CASE WHEN (ISNULL(Opp.numUnitHour,0) - ISNULL(Opp.numQtyShipped,0))>isnull(WItems.numAllocation,0) THEN 1 ELSE 0 END)
			FROM dbo.OpportunityItems Opp
			JOIN item I ON Opp.numItemCode = i.numItemcode
			JOIN WareHouseItems WItems ON Opp.numItemCode = WItems.numItemID 
			AND WItems.numWareHouseItemID = Opp.numWarehouseItmsID
			WHERE numOppId=@numOppID AND I.numDomainID=@numDomainID AND charItemType='P'
			and (bitDropShip=0 or bitDropShip is null)
	    
		IF @ItemCount>0 
		BEGIN
			IF @BackOrderItemCount>0
				SET @vcInventoryStatus = '<font color="#FF0000">Back Order (' + CAST(@BackOrderItemCount AS VARCHAR(18)) + '/' + CAST(@ItemCount AS VARCHAR(18)) +')<font>'
			ELSE
				SET @vcInventoryStatus = '<font color="#800080">Ready to Ship<font>';			
		END 
	END
    
    RETURN ISNULL(@vcInventoryStatus,'')
END
/****** Object:  UserDefinedFunction [dbo].[fn_GetKitInventoryByWarehouse]    Script Date: 05/30/2014  ******/
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'fn'
                    AND NAME = 'fn_GetKitInventoryByWarehouse' ) 
    DROP FUNCTION fn_GetKitInventoryByWarehouse
GO
CREATE FUNCTION [dbo].[fn_GetKitInventoryByWarehouse]
    (
      @numKitId NUMERIC,
      @numWarehouseID NUMERIC(18, 0)
    )
RETURNS NUMERIC(9)
AS BEGIN          
    DECLARE @OnHand AS NUMERIC(9)       
    SET @OnHand = 0 ;
    WITH    CTE ( numItemKitID, numItemCode, numWarehouseItmsID, numQtyItemsReq, numCalculatedQty )
              AS ( SELECT   CONVERT(NUMERIC(18, 0), 0),
                            numItemCode,
                            ISNULL(Dtl.numWareHouseItemId, 0) AS numWarehouseItmsID,
                            DTL.numQtyItemsReq,
                            CAST(DTL.numQtyItemsReq AS NUMERIC(9, 0)) AS numCalculatedQty
                   FROM     item
                            INNER JOIN ItemDetails Dtl ON numChildItemID = numItemCode
                   WHERE    numItemKitID = @numKitId
                   UNION ALL
                   SELECT   Dtl.numItemKitID,
                            i.numItemCode,
                            ISNULL(Dtl.numWareHouseItemId, 0) AS numWarehouseItmsID,
                            DTL.numQtyItemsReq,
                            CAST(( DTL.numQtyItemsReq * c.numCalculatedQty ) AS NUMERIC(9, 0)) AS numCalculatedQty
                   FROM     item i
                            INNER JOIN ItemDetails Dtl ON Dtl.numChildItemID = i.numItemCode
                            INNER JOIN CTE c ON Dtl.numItemKitID = c.numItemCode
                   WHERE    Dtl.numChildItemID != @numKitId
                 )
        SELECT  @OnHand = CAST(FLOOR(MIN(CASE WHEN ISNULL(numOnHand, 0) = 0
                                              THEN 0
                                              WHEN ISNULL(numOnHand, 0) >= numQtyItemsReq
                                                   AND numQtyItemsReq > 0
                                              THEN ISNULL(numOnHand, 0)
                                                   / numQtyItemsReq
                                              ELSE 0
                                         END)) AS NUMERIC(9))
        FROM    cte c
                LEFT OUTER JOIN [WareHouseItems] WI ON WI.[numItemID] = c.numItemCode
                                                       AND WI.[numWareHouseItemID] = c.numWarehouseItmsID
                LEFT OUTER JOIN Warehouses W ON W.numWareHouseID = WI.numWareHouseID
        WHERE   WI.numWareHouseID = @numWarehouseID
        
    RETURN @OnHand         
          
   END
GO
     
/****** Object:  UserDefinedFunction [dbo].[fn_GetOpeningBalance]    Script Date: 10/05/2009 17:46:28 ******/

GO

GO
--DROP FUNCTION [dbo].[fn_GetOpeningBalance]
-- SELECT * FROM dbo.fn_GetOpeningBalance(',74,666,658,77,263,668,1156,1162,1166,1167,1170,1172,1174',72,'2009-01-01 00:00:00:000')
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='fn_getopeningbalance')
DROP FUNCTION fn_getopeningbalance
GO
CREATE FUNCTION [dbo].[fn_GetOpeningBalance]
(@vcChartAcntId as varchar(500),
@numDomainId as numeric(9),
@dtFromDate as DATETIME,
@ClientTimeZoneOffset Int  --Added by Chintan to enable calculation of date according to client machine
)                                 
RETURNS 

@COAOpeningBalance table
(
	numAccountId int,
	vcAccountName varchar(100),
	numParntAcntTypeID int,
	vcAccountDescription varchar(100),
	vcAccountCode varchar(50),
	dtAsOnDate datetime,
	mnOpeningBalance money
)                               
As                                  
Begin          

DECLARE @numFinYear INT;
DECLARE @dtFinFromDate datetime;

set @numFinYear= (SELECT numFinYearId FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND  
dtPeriodTo >=@dtFromDate AND numDomainId= @numDomainId);

set @dtFinFromDate= (SELECT dtPeriodFrom  FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND  
dtPeriodTo >=@dtFromDate AND numDomainId= @numDomainId);


SET @dtFromDate = DateAdd(minute, -@ClientTimeZoneOffset, @dtFromDate);

INSERT INTO @COAOpeningBalance

SELECT  COA.numAccountId,vcAccountName,numParntAcntTypeID,vcAccountDescription,vcAccountCode,
 @dtFromDate,

--isnull((SELECT TOP 1 isnull(monOpening,0) from CHARTACCOUNTOPENING CAO WHERE
--numFinYearId=@numFinYear and numDomainID=@numDomainId and
--CAO.numAccountId=COA.numAccountId),0) 
--+
--CASE WHEN  COA.dtOpeningDate<= @dtFromDate THEN COA.numOriginalOpeningBal 
--ELSE 0 END
--+ 
(SELECT ISNULL(SUM(GJD.numDebitAmt),0) - ISNULL(SUM(GJD.numCreditAmt),0)
FROM
General_Journal_Details GJD 
JOIN Domain D ON D.numDOmainID = GJD.numDomainID AND COA.numDomainId = D.numDOmainID
INNER JOIN General_Journal_Header GJH ON GJD.numJournalId =GJH.numJournal_Id AND 
GJD.numDomainId=COA.numDomainId AND 
GJD.numChartAcntId=COA.numAccountId AND 
GJH.numDomainID = D.numDomainId AND 
GJH.datEntry_Date <=@dtFromDate /*BETWEEN @dtFinFromDate AND @dtFromDate*/ ) /*DATEADD(DAY,-1,@dtFromDate) removed as Date we are passing is 12AM which is 1 second more, compare to opening balance of pervious date */

 FROM Chart_of_Accounts COA
WHERE COA.numDomainId=@numDomainId AND
	COA.numAccountId in (select CAST(ID AS NUMERIC(9)) from SplitIDs(@vcChartAcntId,','));

RETURN 

END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GenerateBizAPIPublicKey')
DROP FUNCTION GenerateBizAPIPublicKey
GO
CREATE FUNCTION [dbo].[GenerateBizAPIPublicKey]()  
returns varchar(10)  
as  
begin  

DECLARE @chars NCHAR(62);
DECLARE @PublicKey VARCHAR(10)
DECLARE @IsUnique BIT
SET @IsUnique = 0
SET @chars = N'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz'

WHILE (@IsUnique = 0)
BEGIN
    SET @PublicKey = SUBSTRING(@chars, CAST(((SELECT rnd FROM [dbo].[View_Random]) * LEN(@chars)) AS INT) + 1, 1)
            + SUBSTRING(@chars, CAST(((SELECT rnd FROM [dbo].[View_Random]) * LEN(@chars)) AS INT) + 1, 1)
            + SUBSTRING(@chars, CAST(((SELECT rnd FROM [dbo].[View_Random]) * LEN(@chars)) AS INT) + 1, 1)
            + SUBSTRING(@chars, CAST(((SELECT rnd FROM [dbo].[View_Random]) * LEN(@chars)) AS INT) + 1, 1)
            + SUBSTRING(@chars, CAST(((SELECT rnd FROM [dbo].[View_Random]) * LEN(@chars)) AS INT) + 1, 1)
            + SUBSTRING(@chars, CAST(((SELECT rnd FROM [dbo].[View_Random]) * LEN(@chars)) AS INT) + 1, 1)
            + SUBSTRING(@chars, CAST(((SELECT rnd FROM [dbo].[View_Random]) * LEN(@chars)) AS INT) + 1, 1)
            + SUBSTRING(@chars, CAST(((SELECT rnd FROM [dbo].[View_Random]) * LEN(@chars)) AS INT) + 1, 1)
            + SUBSTRING(@chars, CAST(((SELECT rnd FROM [dbo].[View_Random]) * LEN(@chars)) AS INT) + 1, 1)
            + SUBSTRING(@chars, CAST(((SELECT rnd FROM [dbo].[View_Random]) * LEN(@chars)) AS INT) + 1, 1)
    IF (SELECT COUNT(*) FROM dbo.UserMaster WHERE vcBizAPIPublicKey = @PublicKey COLLATE Latin1_General_CS_AS) = 0
		SET @IsUnique = 1
    
END
  
return @PublicKey
end
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'GetCommissionAmountOfBizDoc' ) 
    DROP PROCEDURE GetCommissionAmountOfBizDoc
GO
CREATE PROCEDURE GetCommissionAmountOfBizDoc
    (
      @numDomainId NUMERIC,
      @numOppBizDocID NUMERIC(9),
      @numOppID NUMERIC(9)
    )
AS 
    BEGIN

        CREATE TABLE #TempUserComission
            (
              numcontactid NUMERIC,
              numComission MONEY,
              numCOmissionID NUMERIC,
              vcContactName VARCHAR(250),
              numoppitemtCode NUMERIC,
              bitEmpContract BIT,
              vcItemName VARCHAR(300)
            ) ;
    

/*Do not include Shipping Item & Discount Item*/
        DECLARE @numShippingServiceItemID AS NUMERIC(18) ;
            SET @numShippingServiceItemID = 0
        DECLARE @numDiscountServiceItemID AS NUMERIC(18) ;
            SET @numDiscountServiceItemID = 0
        DECLARE @tintCommissionType AS TINYINT,
            @tintComAppliesTo TINYINT 
        DECLARE @bitIncludeTaxAndShippingInCommission AS BIT


--Get CommissionType (total amounts paid : 1 ,gross profit : 2 ,Project Gross Profit : 3)
--        SELECT  @numShippingServiceItemID = CASE WHEN ISNULL(bitIncludeTaxAndShippingInCommission,
--                                                             0) = 1
--                                                 THEN ISNULL(numShippingServiceItemID,
--                                                             0)
--                                                 WHEN ISNULL(bitIncludeTaxAndShippingInCommission,
--                                                             0) = 0 THEN 0
--                                                 ELSE ISNULL(numShippingServiceItemID,
--                                                             0)
--                                            END, -- Applying Domain Settings for Including Shipping Cost for Commission Calculations
				SELECT @numShippingServiceItemID =ISNULL(numShippingServiceItemID, 0), 
                @numDiscountServiceItemID = ISNULL(numDiscountServiceItemID, 0),
                @tintCommissionType = ISNULL(tintCommissionType, 1),
                @tintComAppliesTo = ISNULL(tintComAppliesTo, 3),
                @bitIncludeTaxAndShippingInCommission = ISNULL(bitIncludeTaxAndShippingInCommission, 1)
        FROM    dbo.Domain
        WHERE   numDomainId = @numDomainId

--If Commission set for Project Gross Profit then don't calculate for Order
        IF @tintCommissionType = 3 
            BEGIN
                SELECT  *
                FROM    #TempUserComission ;
                DROP TABLE #TempUserComission
	
                RETURN	
            END


--bug id 982: No need to check for biz docs AmountPaid and DealAmount must equal 
        IF EXISTS ( SELECT  *
                    FROM    opportunitymaster OM
                            LEFT JOIN opportunitybizdocs BD ON BD.numoppid = OM.numoppid
                    WHERE   tintopptype = 1
                            AND tintoppstatus = 1
                            AND numdomainid = @numDomainId
                            AND bitAuthoritativeBizDocs = 1 /*AND isnull(BD.monAmountPaid,0) >=BD.monDealAmount*/--bug id 982
                            AND BD.numOppBizDocsId = @numOppBizDocID /*AND isnull(BD.monAmountPaid,0)>0*/
                            AND BD.monDealAmount > 0 ) 
            BEGIN
                DECLARE @numassignedto NUMERIC,
                    @numrecowner NUMERIC,
                    @numItemCode NUMERIC,
                    @numUnitHour NUMERIC,
                    @monTotAmount AS MONEY,
                    @numTotalUnitHour NUMERIC,
                    @monTotalTotAmount AS MONEY,
                    @monCommission AS MONEY
                DECLARE @numComRuleID NUMERIC,
                    @tintComBasedOn TINYINT,
                    @tinComDuration TINYINT,
                    @tintComType TINYINT
                DECLARE @dFrom DATETIME,
                    @dTo DATETIME
                DECLARE @decCommission DECIMAL(18, 0),
                    @numDivisionID NUMERIC,
                    @bitCommContact AS BIT,
                    @numItemClassification NUMERIC
                DECLARE @numUserCntID NUMERIC,
                    @VendorCostAssignee MONEY,
                    @TotalVendorCostAssignee MONEY,
                    @numOppBizDocItemID NUMERIC,
                    @numoppitemtCode NUMERIC
                DECLARE @UserName AS VARCHAR(100),
                    @bitEmpContract AS BIT
                DECLARE @TempmonTotAmount AS MONEY,
                    @TempVendorCostAssignee MONEY
                DECLARE @numCOmissionID AS NUMERIC ;
                    SET @numCOmissionID = 0
                DECLARE @vcItemName AS VARCHAR(300)

			--Get AssignTo and Record Owner of Oppertunity
                SELECT  @numassignedto = numassignedto,
                        @numrecowner = numrecowner
                FROM    opportunitymaster OM
                        LEFT JOIN opportunitybizdocs BD ON BD.numoppid = OM.numoppid
                WHERE   tintopptype = 1
                        AND tintoppstatus = 1
                        AND numdomainid = @numDomainId
                        AND bitAuthoritativeBizDocs = 1
                        AND BD.numOppBizDocsId = @numOppBizDocID
			

			--Cursor for each Items belogs to Oppertunity Biz Docs
                DECLARE Biz_Items CURSOR
                    FOR SELECT  BDI.numItemCode,
                                SUM(ISNULL(BDI.numUnitHour, 0)),
                                SUM(ISNULL(BDI.monTotAmount, 0)),
                                SUM(ISNULL(OT.monVendorCost, 0)
                                    * ISNULL(BDI.[numUnitHour], 0)),
                                MAX(BDI.numOppBizDocItemID),
                                MAX(OT.numoppitemtCode)
                        FROM    opportunitymaster OM
                                JOIN opportunitybizdocs BD ON BD.numoppid = OM.numoppid
                                JOIN OpportunityBizDocItems BDI ON BD.numOppBizDocsId = BDI.numOppBizDocID
                                INNER JOIN OpportunityItems OT ON OT.numOppItemtCode = BDI.numOppItemID
                        WHERE   tintopptype = 1
                                AND tintoppstatus = 1
                                AND numdomainid = @numDomainId
                                AND bitAuthoritativeBizDocs = 1 /*AND isnull(BD.monAmountPaid,0) >=BD.monDealAmount*/ --bug id 982
                                AND BD.numOppBizDocsId = @numOppBizDocID /*AND isnull(BD.monAmountPaid,0)>0 */
                                AND BD.monDealAmount > 0
                                AND BDI.numItemCode NOT IN (
                                @numDiscountServiceItemID )  /*Do not include Discount Item*/
                                AND 1 = CASE WHEN BDI.numItemCode <> @numShippingServiceItemID
                                             THEN 1
                                             ELSE 0
                                        END /*Condition to include/exclude include Shipping Item */
                        GROUP BY BDI.numItemCode
                        UNION ALL
                        SELECT  BDI.numItemCode,
                                SUM(ISNULL(BDI.numUnitHour, 0)),
                                SUM(ISNULL(BDI.monTotAmount, 0)),
                                SUM(ISNULL(OT.monVendorCost, 0)
                                    * ISNULL(BDI.[numUnitHour], 0)),
                                MAX(BDI.numOppBizDocItemID),
                                MAX(OT.numoppitemtCode)
                        FROM    opportunitymaster OM
                                JOIN opportunitybizdocs BD ON BD.numoppid = OM.numoppid
                                JOIN OpportunityBizDocItems BDI ON BD.numOppBizDocsId = BDI.numOppBizDocID
                                INNER JOIN OpportunityItems OT ON OT.numOppItemtCode = BDI.numOppItemID
                        WHERE   tintopptype = 1
                                AND tintoppstatus = 1
                                AND numdomainid = @numDomainId
                                AND bitAuthoritativeBizDocs = 1 /*AND isnull(BD.monAmountPaid,0) >=BD.monDealAmount*/ --bug id 982
                                AND BD.numOppBizDocsId = @numOppBizDocID /*AND isnull(BD.monAmountPaid,0)>0 */
                                AND BD.monDealAmount > 0
                                AND BDI.numItemCode NOT IN (
                                @numDiscountServiceItemID )  /*Do not include Discount Item*/
                               AND 1 = CASE WHEN @bitIncludeTaxAndShippingInCommission = 1
                                             THEN ( CASE WHEN BDI.numItemCode = @numShippingServiceItemID
                                                         THEN 1
                                                         ELSE 0
                                                    END )
                                             ELSE 0
                                        END /*Condition to include/exclude include Shipping Item */
                                        
                        GROUP BY BDI.numItemCode
							
                OPEN Biz_Items

                FETCH NEXT FROM Biz_Items INTO @numItemCode, @numUnitHour,
                    @monTotAmount, @VendorCostAssignee, @numOppBizDocItemID,
                    @numoppitemtCode
                WHILE @@FETCH_STATUS = 0
                    BEGIN  
				
                        DECLARE @i AS INT ;
                            SET @i = 1
					
					--Loop for AssignTo i=1 and RecordOwner i=2 (set tintAssignTo=i)
                        WHILE @i < 3
                            BEGIN
					
                                IF @i = 1 
                                    SET @numUserCntID = @numassignedto
                                ELSE 
                                    IF @i = 2 
                                        SET @numUserCntID = @numrecowner
						
					--Check For Commission Contact	
                                IF EXISTS ( SELECT  UM.numUserId
                                            FROM    UserMaster UM
                                                    JOIN AdditionalContactsInformation A ON UM.numUserDetailId = A.numContactID
                                            WHERE   UM.numDomainID = @numDomainID
                                                    AND UM.numDomainID = A.numDomainID
                                                    AND A.numContactID = @numUserCntID ) 
                                    BEGIN
                                        SET @bitCommContact = 0
                                        SET @numDivisionID = 0
                                    END	
                                ELSE 
                                    BEGIN
                                        SET @bitCommContact = 1
                                        SELECT  @numDivisionID = A.numDivisionId
                                        FROM    AdditionalContactsInformation A
                                        WHERE   A.numContactId = @numUserCntID 
                                    END  		
						
                                SELECT  @TempmonTotAmount = @monTotAmount,
                                        @TempVendorCostAssignee = @VendorCostAssignee
						
						--SELECT @numUserCntID,@i,@numItemCode
					--Individual Items tintComAppliesTo=1 and tintType=1
                                IF EXISTS ( SELECT  CR.numComRuleID
                                            FROM    CommissionRules CR
                                                    JOIN CommissionRuleItems CRI ON CR.numComRuleID = CRI.numComRuleID
                                                    JOIN CommissionRuleContacts CRC ON CR.numComRuleId = CRC.numComRuleId
                                            WHERE   CR.numDomainID = @numDomainID
                                                    AND CR.tintComAppliesTo = 1
                                                    AND CR.tintAssignTo = @i
                                                    AND CRI.numValue = @numItemCode
                                                    AND CRI.tintType = 1
                                                    AND CRC.numValue = ( CASE @bitCommContact
                                                                           WHEN 0 THEN @numUserCntID
                                                                           WHEN 1 THEN @numDivisionID
                                                                         END )
                                                    AND CRC.bitCommContact = @bitCommContact )
                                    AND @tintComAppliesTo = 1 
                                    BEGIN
						
                                        SELECT TOP 1
                                                @numComRuleID = CR.numComRuleID,
                                                @tintComBasedOn = CR.tintComBasedOn,
                                                @tinComDuration = CR.tinComDuration,
                                                @tintComType = CR.tintComType
                                        FROM    CommissionRules CR
                                                JOIN CommissionRuleItems CRI ON CR.numComRuleID = CRI.numComRuleID
                                                JOIN CommissionRuleContacts CRC ON CR.numComRuleId = CRC.numComRuleId
                                        WHERE   CR.numDomainID = @numDomainID
                                                AND CR.tintComAppliesTo = 1
                                                AND CR.tintAssignTo = @i
                                                AND CRI.numValue = @numItemCode
                                                AND CRI.tintType = 1
                                                AND CRC.numValue = ( CASE @bitCommContact
                                                                       WHEN 0 THEN @numUserCntID
                                                                       WHEN 1 THEN @numDivisionID
                                                                     END )
                                                AND CRC.bitCommContact = @bitCommContact
                                    END
						
					--Items Classification	tintComAppliesTo=2 and tintType=2
                                ELSE 
                                    IF EXISTS ( SELECT  CR.numComRuleID
                                                FROM    CommissionRules CR
                                                        JOIN CommissionRuleItems CRI ON CR.numComRuleID = CRI.numComRuleID
                                                        JOIN CommissionRuleContacts CRC ON CR.numComRuleId = CRC.numComRuleId
                                                        JOIN Item I ON CRI.numValue = I.numItemClassification
                                                WHERE   CR.numDomainID = @numDomainID
                                                        AND CR.tintComAppliesTo = 2
                                                        AND CR.tintAssignTo = @i
                                                        AND I.numItemCode = @numItemCode
                                                        AND CRI.tintType = 2
                                                        AND CRC.numValue = ( CASE @bitCommContact
                                                                               WHEN 0 THEN @numUserCntID
                                                                               WHEN 1 THEN @numDivisionID
                                                                             END )
                                                        AND CRC.bitCommContact = @bitCommContact )
                                        AND @tintComAppliesTo = 2 
                                        BEGIN
						
                                            SELECT TOP 1
                                                    @numComRuleID = CR.numComRuleID,
                                                    @tintComBasedOn = CR.tintComBasedOn,
                                                    @tinComDuration = CR.tinComDuration,
                                                    @tintComType = CR.tintComType,
                                                    @numItemClassification = CRI.numValue
                                            FROM    CommissionRules CR
                                                    JOIN CommissionRuleItems CRI ON CR.numComRuleID = CRI.numComRuleID
                                                    JOIN CommissionRuleContacts CRC ON CR.numComRuleId = CRC.numComRuleId
                                                    JOIN Item I ON CRI.numValue = I.numItemClassification
                                            WHERE   CR.numDomainID = @numDomainID
                                                    AND CR.tintComAppliesTo = 2
                                                    AND CR.tintAssignTo = @i
                                                    AND I.numItemCode = @numItemCode
                                                    AND CRI.tintType = 2
                                                    AND CRC.numValue = ( CASE @bitCommContact
                                                                           WHEN 0 THEN @numUserCntID
                                                                           WHEN 1 THEN @numDivisionID
                                                                         END )
                                                    AND CRC.bitCommContact = @bitCommContact
                                        END
						
					--All Items	 tintComAppliesTo=3
                                    ELSE 
                                        IF EXISTS ( SELECT  CR.numComRuleID
                                                    FROM    CommissionRules CR
                                                            JOIN CommissionRuleContacts CRC ON CR.numComRuleId = CRC.numComRuleId
                                                    WHERE   CR.tintComAppliesTo = 3
                                                            AND CR.numDomainID = @numDomainID
                                                            AND CR.tintAssignTo = @i
                                                            AND CRC.numValue = ( CASE @bitCommContact
                                                                                   WHEN 0 THEN @numUserCntID
                                                                                   WHEN 1 THEN @numDivisionID
                                                                                 END )
                                                            AND CRC.bitCommContact = @bitCommContact )
                                            AND @tintComAppliesTo = 3 
                                            BEGIN
						
                                                SELECT TOP 1
                                                        @numComRuleID = CR.numComRuleID,
                                                        @tintComBasedOn = CR.tintComBasedOn,
                                                        @tinComDuration = CR.tinComDuration,
                                                        @tintComType = CR.tintComType
                                                FROM    CommissionRules CR
                                                        JOIN CommissionRuleContacts CRC ON CR.numComRuleId = CRC.numComRuleId
                                                WHERE   CR.tintComAppliesTo = 3
                                                        AND CR.numDomainID = @numDomainID
                                                        AND CR.tintAssignTo = @i
                                                        AND CRC.numValue = ( CASE @bitCommContact
                                                                               WHEN 0 THEN @numUserCntID
                                                                               WHEN 1 THEN @numDivisionID
                                                                             END )
                                                        AND CRC.bitCommContact = @bitCommContact
                                            END
					
                                IF @numComRuleID > 0 
                                    BEGIN
						--Get From and To date for Commission Calculation
                                        IF @tinComDuration = 1 --Month
                                            SELECT  @dFrom = DATEADD(m, DATEDIFF(m, 0, GETDATE()), 0),
                                                    @dTo = DATEADD(d, -1, DATEADD(m, DATEDIFF(m, 0, GETDATE()) + 1, 0)) 
	
                                        ELSE 
                                            IF @tinComDuration = 2 --Quarter
                                                SELECT  @dFrom = DATEADD(q, DATEDIFF(q, 0, GETDATE()), 0),
                                                        @dTo = DATEADD(d, -1, DATEADD(q, DATEDIFF(q, 0, GETDATE()) + 1, 0)) 
									
                                            ELSE 
                                                IF @tinComDuration = 3 --Year
                                                    SELECT  @dFrom = DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0),
                                                            @dTo = DATEADD(d, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) + 1, 0)) 
						
						
						--Total of Units and Amount for all oppertunity belogs to selected Item and selected AssignTo/RecordOwner
                                        SELECT  @numTotalUnitHour = ISNULL(SUM(BDI.numUnitHour), 0),
                                                @monTotalTotAmount = ISNULL(SUM(BDI.monTotAmount), 0),
                                                @TotalVendorCostAssignee = ISNULL(SUM(ISNULL(OT.monVendorCost, 0) * ISNULL(BDI.[numUnitHour], 0)), 0)
                                        FROM    opportunitymaster OM
                                                JOIN opportunitybizdocs BD ON BD.numoppid = OM.numoppid
                                                JOIN OpportunityBizDocItems BDI ON BD.numOppBizDocsId = BDI.numOppBizDocID
                                                INNER JOIN OpportunityItems OT ON OT.numOppItemtCode = BDI.numOppItemID
                                        WHERE   tintopptype = 1
                                                AND tintoppstatus = 1
                                                AND numdomainid = @numDomainId
                                                AND bitAuthoritativeBizDocs = 1 /*AND isnull(BD.monAmountPaid,0) >=BD.monDealAmount*/ --bug id 982
                                                AND BD.numOppBizDocsId <> @numOppBizDocID /*AND isnull(BD.monAmountPaid,0)>0*/
                                                AND BD.monDealAmount > 0 
							--AND (numassignedto=@numUserCntID OR numrecowner=@numUserCntID) 
                                                AND 1 = ( CASE @i
                                                            WHEN 1
                                                            THEN CASE @bitCommContact
                                                                   WHEN 0 THEN CASE WHEN numassignedto = @numUserCntID THEN 1
                                                                                    ELSE 0
                                                                               END
                                                                   WHEN 1 THEN CASE WHEN numassignedto IN ( SELECT  numContactId
                                                                                                            FROM    AdditionalContactsInformation AC
                                                                                                            WHERE   AC.numDivisionID = @numDivisionID ) THEN 1
                                                                                    ELSE 0
                                                                               END
                                                                 END
                                                            WHEN 2
                                                            THEN CASE @bitCommContact
                                                                   WHEN 0 THEN CASE WHEN numrecowner = @numUserCntID THEN 1
                                                                                    ELSE 0
                                                                               END
                                                                   WHEN 1 THEN CASE WHEN numrecowner IN ( SELECT    numContactId
                                                                                                          FROM      AdditionalContactsInformation AC
                                                                                                          WHERE     AC.numDivisionID = @numDivisionID ) THEN 1
                                                                                    ELSE 0
                                                                               END
                                                                 END
                                                          END )	 				 	 
							--BDI.numItemCode=@numItemCode
                                                AND 1 = ( CASE @tintComAppliesTo
                                                            WHEN 1
                                                            THEN CASE WHEN BDI.numItemCode = @numItemCode THEN 1
                                                                      ELSE 0
                                                                 END
                                                            WHEN 2
                                                            THEN CASE WHEN BDI.numItemCode IN ( SELECT  numItemCode
                                                                                                FROM    Item
                                                                                                WHERE   numItemClassification = @numItemClassification
                                                                                                        AND numDomainID = @numDomainID ) THEN 1
                                                                      ELSE 0
                                                                 END
                                                            WHEN 3 THEN 1
                                                            ELSE 0
                                                          END )
                                                AND BD.numOppBizDocsId IN (
                                                SELECT  OBD.numOppBizDocsId
                                                FROM    OpportunityBizDocsDetails BDD
                                                        INNER JOIN dbo.OpportunityBizDocs OBD ON BDD.numBizDocsId = OBD.numOppBizDocsId
                                                        INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OBD.numOppId
                                                WHERE   tintopptype = 1
                                                        AND tintoppstatus = 1
                                                        AND OM.numdomainid = @numDomainId
                                                        AND OBD.bitAuthoritativeBizDocs = 1
                                                        AND BDD.dtCreationDate BETWEEN @dFrom AND @dTo
                                                        AND 1 = ( CASE @i
                                                                    WHEN 1 THEN CASE @bitCommContact
                                                                                  WHEN 0 THEN CASE WHEN OM.numassignedto = @numUserCntID THEN 1
                                                                                                   ELSE 0
                                                                                              END
                                                                                  WHEN 1 THEN CASE WHEN OM.numassignedto IN ( SELECT    numContactId
                                                                                                                              FROM      AdditionalContactsInformation AC
                                                                                                                              WHERE     AC.numDivisionID = @numDivisionID ) THEN 1
                                                                                                   ELSE 0
                                                                                              END
                                                                                END
                                                                    WHEN 2 THEN CASE @bitCommContact
                                                                                  WHEN 0 THEN CASE WHEN OM.numrecowner = @numUserCntID THEN 1
                                                                                                   ELSE 0
                                                                                              END
                                                                                  WHEN 1 THEN CASE WHEN OM.numrecowner IN ( SELECT  numContactId
                                                                                                                            FROM    AdditionalContactsInformation AC
                                                                                                                            WHERE   AC.numDivisionID = @numDivisionID ) THEN 1
                                                                                                   ELSE 0
                                                                                              END
                                                                                END
                                                                  END )	 				 	 				 
														--(OM.numassignedto=@numUserCntID OR OM.numrecowner=@numUserCntID)
                                                GROUP BY OBD.numOppBizDocsId /*HAVING SUM(BDD.monAmount)>=MAX(OBD.monDealAmount)*/ ) --bug id 982
			
			
						--If Commission based on gross profit	
                                        IF @tintCommissionType = 2 
                                            BEGIN
                                                SET @TempmonTotAmount = @TempmonTotAmount
                                                    - @TempVendorCostAssignee
                                                SET @monTotalTotAmount = @monTotalTotAmount
                                                    - @TotalVendorCostAssignee
                                                    + @TempmonTotAmount
                                            END
                                        ELSE 
                                            SET @monTotalTotAmount = @monTotalTotAmount
                                                + @TempmonTotAmount
						
						
                                        SET @numTotalUnitHour = @numTotalUnitHour
                                            + @numUnitHour
						
                                        DECLARE @intFromNextTier AS INT
						
                                        IF @tintComBasedOn = 1 --Amount Sold
                                            BEGIN
                                                SELECT TOP 1
                                                        @decCommission = decCommission,
                                                        @intFromNextTier = intTo
                                                        + 1
                                                FROM    CommissionRuleDtl
                                                WHERE   numComRuleID = @numComRuleID
                                                        AND @monTotalTotAmount BETWEEN intFrom AND intTo
                                            END			
                                        ELSE 
                                            IF @tintComBasedOn = 2 --Units Sold
                                                BEGIN
                                                    SELECT TOP 1
                                                            @decCommission = decCommission,
                                                            @intFromNextTier = intTo
                                                            + 1
                                                    FROM    CommissionRuleDtl
                                                    WHERE   numComRuleID = @numComRuleID
                                                            AND @numTotalUnitHour BETWEEN intFrom AND intTo			
                                                END		
				
						
                                        DECLARE @nexttier AS VARCHAR(100)

                                        SELECT TOP 1
                                                @nexttier = CAST(numComRuleDtlID AS VARCHAR(10))
                                                + ','
                                                + CAST(numComRuleID AS VARCHAR(10))
                                                + ','
                                                + CAST(intFrom AS VARCHAR(10))
                                                + ','
                                                + CAST(intTo AS VARCHAR(10))
                                                + ','
                                                + CAST(decCommission AS VARCHAR(10))
                                        FROM    CommissionRuleDtl
                                        WHERE   numComRuleID = @numComRuleID
                                                AND intFrom >= @intFromNextTier
                                        ORDER BY intFrom 
							
							
                                        IF @tintComType = 1 --Percentage
                                            SET @monCommission = @TempmonTotAmount
                                                * @decCommission / 100
                                        ELSE 
                                            IF @tintComType = 2 --Flat
                                                SET @monCommission = @decCommission
                                                    * @numUnitHour
						
                                        IF @monCommission > 0 
                                            BEGIN
							
                                                SELECT  @UserName = ISNULL(adc.vcfirstname + ' ' + adc.vclastname, '-'),
                                                        @bitEmpContract = CASE WHEN um.numuserdetailid > 0 THEN 0
                                                                               ELSE 1
                                                                          END
                                                FROM    additionalcontactsinformation adc
                                                        LEFT JOIN usermaster um ON adc.numcontactid = um.numuserdetailid
                                                                                   AND um.numdomainid = @numDomainId
                                                                                   AND um.bitactivateflag = 1
                                                WHERE   adc.numcontactid = @numUserCntID
							
                                                SELECT  @vcItemName = vcItemName
                                                FROM    Item
                                                WHERE   numItemCode = @numItemCode
                                                        AND numdomainid = @numDomainId 

                                                IF NOT EXISTS ( SELECT  *
                                                                FROM    [BizDocComission]
                                                                WHERE   numDomainId = @numDomainId
                                                                        AND numUserCntID = @numUserCntID
                                                                        AND numOppBizDocId = @numOppBizDocID
                                                                        AND numOppBizDocItemID = @numOppBizDocItemID ) 
                                                    BEGIN
                                                        INSERT  INTO [BizDocComission]
                                                                (
                                                                  [numDomainId],
                                                                  [numUserCntID],
                                                                  [numOppBizDocId],
                                                                  [numComissionAmount],
                                                                  [numOppBizDocItemID],
                                                                  [numComRuleID],
                                                                  [tintComType],
                                                                  [tintComBasedOn],
                                                                  [decCommission],
                                                                  vcnexttier,
                                                                  numOppID
                                                                )
                                                        VALUES  (
                                                                  @numDomainId,
                                                                  @numUserCntID,
                                                                  @numOppBizDocID,
                                                                  @monCommission,
                                                                  @numOppBizDocItemID,
                                                                  @numComRuleID,
                                                                  @tintComType,
                                                                  @tintComBasedOn,
                                                                  @decCommission,
                                                                  @nexttier,
                                                                  @numOppID
                                                                ) ;	

                                                        SET @numCOmissionID = @@IDENTITY
                                                    END
                                                ELSE 
                                                    SELECT  @numCOmissionID = numCOmissionID
                                                    FROM    [BizDocComission]
                                                    WHERE   numDomainId = @numDomainId
                                                            AND numUserCntID = @numUserCntID
                                                            AND numOppBizDocId = @numOppBizDocID
                                                            AND numOppBizDocItemID = @numOppBizDocItemID

                                                INSERT  INTO #TempUserComission
                                                        SELECT  @numUserCntID,
                                                                @monCommission,
                                                                @numCOmissionID,
                                                                @UserName,
                                                                @numoppitemtCode,
                                                                @bitEmpContract,
                                                                @vcItemName ;

                                            END
                                        ELSE 
                                            BEGIN
                                                DELETE  FROM [BizDocComission]
                                                WHERE   numDomainId = @numDomainId
                                                        AND numUserCntID = @numUserCntID
                                                        AND numOppBizDocId = @numOppBizDocID
                                                        AND numOppBizDocItemID = @numOppBizDocItemID
                                            END
                                    END
                                SELECT  @numComRuleID = 0,
                                        @tintComBasedOn = 0,
                                        @tinComDuration = 0,
                                        @tintComType = 0,
                                        @monCommission = 0,
                                        @bitCommContact = 0,
                                        @numDivisionID = 0
                                SET @i = @i + 1
                            END
					
                        FETCH NEXT FROM Biz_Items INTO @numItemCode,
                            @numUnitHour, @monTotAmount, @VendorCostAssignee,
                            @numOppBizDocItemID, @numoppitemtCode
                    END ;

                CLOSE Biz_Items ;
                DEALLOCATE Biz_Items ;
            END

        SELECT  *
        FROM    #TempUserComission ;
        DROP TABLE #TempUserComission


--    DECLARE @numUserId NUMERIC
--    DECLARE @numUserCntID NUMERIC
--    DECLARE @vcContactName varchar(250)
--    DECLARE @AssignedAmount AS MONEY
--    DECLARE @OwnerAmount AS MONEY
--    DECLARE @bitCommOwner AS BIT
--    DECLARE @bitCommAssignee AS BIT
--    DECLARE @decCommOwner AS DECIMAL(10, 2)
--    DECLARE @decCommAssignee AS DECIMAL(10, 2)
--    DECLARE @CommAssignedAmt AS MONEY
--    DECLARE @CommOwnerAmt AS MONEY
--    DECLARE @TotalCommAmt AS MONEY
--    DECLARE @bitRoleComm AS BIT
--    DECLARE @CommRoleAmt AS MONEY
--    DECLARE @bitManagerOfOwner AS BIT
--    DECLARE @decManagerOfOwner AS DECIMAL(10, 2)
--    DECLARE @CommissionType TINYINT
--    DECLARE @VendorCostOwner MONEY
--    DECLARE @VendorCostAssignee MONEY
--    DECLARE @VendorCostManager MONEY
--    DECLARE @VendorCostRole MONEY
--	DECLARE @ManagerOfOwnerAmt  AS MONEY
--    DECLARE @CommManagerOfOwnerAmt AS MONEY
--    
--    
--
--    
--    CREATE TABLE #TempUserData
--    (numUserId numeric,
--    bitCommOwner numeric,
--    decCommOwner numeric,
--    bitCommAssignee numeric,
--    decCommAssignee numeric,
--    bitManagerOfOwner numeric,
--    decManagerOfOwner numeric,
--    bitRoleComm numeric)
--    
--    SELECT  @CommissionType = ISNULL(tintCommissionType, 1)
--    FROM    domain
--    WHERE   numDomainID = @numDomainID
--    
--    
--    
--    
--    declare Biz_Users CURSOR FOR
--       
--    SELECT um.numuserid,adc.numcontactid,Isnull(adc.vcfirstname
--                    + ' '
--                    + adc.vclastname,'-')  as UserName
--    FROM   usermaster um
--           JOIN additionalcontactsinformation adc
--             ON adc.numcontactid = um.numuserdetailid             
--           LEFT OUTER JOIN listdetails ld
--             ON adc.vcdepartment = ld.numlistitemid
--                AND ld.numlistid = 19
--    WHERE  um.bitactivateflag = 1
--           AND um.numdomainid = @numDomainId
--		   AND adc.numcontactid not in (select numUserCntId from BizDocComission BC
--		    where BC.numOppBizDocId = @numOppBizDocID)
--
--	OPEN Biz_Users
--
--    FETCH NEXT FROM Biz_Users into @numUserId,@numUserCntID,@vcContactName
--WHILE @@FETCH_STATUS = 0
--   BEGIN
--		set @bitCommOwner=0;
--		set @bitCommAssignee=0;
--		set @decCommOwner=0;
--		set @decCommAssignee=0;
--		set @bitRoleComm=0;
--		set @bitManagerOfOwner=0;
--		set @decManagerOfOwner=0;
--		
--		SELECT  @bitCommOwner = bitcommowner,
--            @bitCommAssignee = bitcommassignee,
--            @decCommOwner = ISNULL(fltcommowner, 0),
--            @decCommAssignee = ISNULL(fltcommassignee, 0),
--            @bitRoleComm = bitrolecomm,
--            @bitManagerOfOwner = bitManagerOfOwner,
--            @decManagerOfOwner = fltManagerOfOwner
--    FROM    usermaster
--    WHERE   numuserid = @numUserId
--    
--		insert into #TempUserData
--     SELECT @numUserId,@bitCommOwner,@decCommOwner,@bitCommAssignee,@decCommAssignee,@bitManagerOfOwner,@decManagerOfOwner,@bitRoleComm
--     
-----------------When user is Assignee of Deal ----------------------------
--	IF @bitCommAssignee = 1 AND @decCommAssignee <> 0
--	BEGIN
--		SELECT  @AssignedAmount = SUM(ISNULL(BD.monAmountPaid, 0)) --monpamount
--		FROM    opportunitymaster OM
--				LEFT JOIN opportunitybizdocs BD ON BD.numoppid = OM.numoppid
--		WHERE   tintopptype = 1
--				AND tintoppstatus = 1
--				AND numassignedto = @numUserCntID
--				AND numdomainid = @numDomainId
--				AND bitAuthoritativeBizDocs = 1 --
--				AND isnull(BD.monAmountPaid,0) >=BD.monDealAmount
--				AND BD.numOppBizDocsId=@numOppBizDocID
--		IF @CommissionType = 2 
--			BEGIN
--				SELECT  @VendorCostAssignee = SUM(ISNULL(OT.monVendorCost, 0)* ISNULL(BI.[numUnitHour],0))
--				FROM    opportunitymaster OM
--						LEFT JOIN opportunitybizdocs BD ON BD.numoppid = OM.numoppid
--						LEFT JOIN [OpportunityBizDocItems] BI ON BI.[numOppBizDocID] = BD.[numOppBizDocsId]
--						INNER JOIN OpportunityItems OT ON OT.numOppItemtCode=BI.numOppItemID 						
--				WHERE   tintopptype = 1
--						AND tintoppstatus = 1
--						AND numassignedto = @numUserCntID
--						AND OM.numdomainid = @numDomainId
--						AND bitAuthoritativeBizDocs = 1 --
--						AND isnull(BD.monAmountPaid,0) >0
--						AND  BD.numOppBizDocsId=@numOppBizDocID
--	           
--				SET @AssignedAmount = @AssignedAmount - @VendorCostAssignee ;
--				--RETURN @VendorCostAssignee
--			END            
--	END
--	
--   
-----------------When user is Owner of Deal ----------------------------
--	IF @bitCommOwner = 1 AND @decCommOwner <> 0
--	BEGIN
--		SELECT  @OwnerAmount = SUM(ISNULL(BD.monAmountPaid, 0)) --monpamount
--		FROM    opportunitymaster OM
--				LEFT JOIN opportunitybizdocs BD ON BD.numoppid = OM.numoppid
--		WHERE   tintopptype = 1
--				AND tintoppstatus = 1
--				AND numrecowner = @numUserCntID
--				AND numdomainid = @numDomainId
--				AND bitAuthoritativeBizDocs = 1 --
--				AND isnull(BD.monAmountPaid,0) >=BD.monDealAmount
--				AND BD.numOppBizDocsId=@numOppBizDocID
--	      
--		IF @CommissionType = 2 
--			BEGIN
--				SELECT  @VendorCostOwner = SUM(ISNULL(OT.monVendorCost, 0)* ISNULL(BI.[numUnitHour],0))
--				FROM    opportunitymaster OM
--						LEFT JOIN opportunitybizdocs BD ON BD.numoppid = OM.numoppid
--						LEFT JOIN [OpportunityBizDocItems] BI ON BI.[numOppBizDocID] = BD.[numOppBizDocsId]
--						INNER JOIN OpportunityItems OT ON OT.numOppItemtCode=BI.numOppItemID 						
--						
--				WHERE   tintopptype = 1
--						AND tintoppstatus = 1
--						AND numrecowner = @numUserCntID
--						AND OM.numdomainid = @numDomainId
--						AND bitAuthoritativeBizDocs = 1
--						AND BD.monAmountPaid > 0
--						AND BD.numOppBizDocsId=@numOppBizDocID
--	           
--				SET @OwnerAmount = @OwnerAmount - @VendorCostOwner ;
--	--            RETURN @VendorCostOwner
--			END     	
--	END
--    
-- ---------------When user is Manager of Deal Owner----------------------------
--	IF @bitManagerOfOwner = 1 AND @decManagerOfOwner <> 0
--	BEGIN
--		
--		
--		SELECT  @ManagerOfOwnerAmt = SUM(ISNULL(BD.monAmountPaid, 0)) --monpamount
--		FROM    opportunitymaster OM
--				LEFT JOIN opportunitybizdocs BD ON BD.numoppid = OM.numoppid
--		WHERE   tintopptype = 1
--				AND tintoppstatus = 1
--				AND numrecowner IN (SELECT [numContactId] FROM [AdditionalContactsInformation] WHERE [numManagerID]=@numUserCntID AND [numDomainID]=@numDomainID)
--				AND numdomainid = @numDomainId
--				AND bitAuthoritativeBizDocs = 1 --
--				AND isnull(BD.monAmountPaid,0) >=BD.monDealAmount
--				AND BD.numOppBizDocsId=@numOppBizDocID
--	       
--		IF @CommissionType = 2 
--			BEGIN
--				SELECT  @VendorCostManager = SUM(ISNULL(OT.monVendorCost, 0)* ISNULL(BI.[numUnitHour],0))
--				FROM    opportunitymaster OM
--						LEFT JOIN opportunitybizdocs BD ON BD.numoppid = OM.numoppid
--						LEFT JOIN [OpportunityBizDocItems] BI ON BI.[numOppBizDocID] = BD.[numOppBizDocsId]
--						INNER JOIN OpportunityItems OT ON OT.numOppItemtCode=BI.numOppItemID 						
--				WHERE   tintopptype = 1
--						AND tintoppstatus = 1
--						AND numrecowner IN (SELECT [numContactId] FROM [AdditionalContactsInformation] WHERE [numManagerID]=@numUserCntID AND [numDomainID]=@numDomainID)
--						AND OM.numdomainid = @numDomainId
--						AND bitAuthoritativeBizDocs = 1
--						AND BD.monAmountPaid > 0
--						AND BD.numOppBizDocsId=@numOppBizDocID
--	           
--				SET @ManagerOfOwnerAmt = @ManagerOfOwnerAmt - @VendorCostManager ;
----			 RETURN @ManagerOfOwnerAmt    
--			END     	
--	END
--                               
-----------------When user beloings to selceted Role ----------------------------
--    IF @bitRoleComm = 1 
--        BEGIN
--            SELECT  @CommRoleAmt = SUM(ISNULL(monamountpaid, 0)
--                                       * fltpercentage / 100)
--            FROM    opportunitymaster opp
--                    JOIN opportunitycontact oppcont ON opp.numoppid = oppcont.numoppid
--                    LEFT JOIN opportunitybizdocs oppbiz ON oppbiz.numoppid = opp.numoppid
--                    JOIN userroles ur ON oppcont.numrole = ur.numrole
--                    JOIN usermaster um ON ur.numusercntid = um.numuserid
--                                          AND oppcont.numcontactid = um.numuserdetailid
----               INNER JOIN opportunitybizdocsdetails obd
----                 ON obd.numbizdocsid = oppbiz.numoppbizdocsid
--            WHERE   tintopptype = 1
--                    AND opp.tintoppstatus = 1
--                    AND opp.numdomainid = @numDomainId
--                    AND oppcont.numcontactid = @numUserCntID
--                    AND bitAuthoritativeBizDocs = 1 --
--                    AND  isnull(oppbiz.monAmountPaid,0) >=oppbiz.monDealAmount
--                    AND oppbiz.numOppBizDocsId=@numOppBizDocID
----       RETURN @CommRoleAmt
--            IF @CommissionType = 2 
--                BEGIN
--                    SELECT  @VendorCostRole = SUM(ISNULL(OT.monVendorCost, 0)* ISNULL(BI.[numUnitHour],0) * fltpercentage / 100 )
--                    FROM    opportunitymaster opp
--                            JOIN opportunitycontact oppcont ON opp.numoppid = oppcont.numoppid
--                            LEFT JOIN opportunitybizdocs oppbiz ON oppbiz.numoppid = opp.numoppid
--                            JOIN userroles ur ON oppcont.numrole = ur.numrole
--                            JOIN usermaster um ON ur.numusercntid = um.numuserid
--                                                  AND oppcont.numcontactid = um.numuserdetailid
--                            LEFT JOIN [OpportunityBizDocItems] BI ON BI.[numOppBizDocID] = oppbiz.[numOppBizDocsId]
--							INNER JOIN OpportunityItems OT ON OT.numOppItemtCode=BI.numOppItemID 						
--                    WHERE   tintopptype = 1
--                            AND opp.tintoppstatus = 1
--                            AND opp.numdomainid = @numDomainId
--                            AND oppcont.numcontactid = @numUserCntID
--                            AND bitAuthoritativeBizDocs = 1 --
--                            AND oppbiz.monAmountPaid > 0
--                            AND oppbiz.numOppBizDocsId=@numOppBizDocID
--                                                    
--					SET @CommRoleAmt =@CommRoleAmt -  @VendorCostRole;
--                END 
--        END
-- ---------------------------------------------------------------------------------
--		
--		
--		IF @bitManagerOfOwner = 1
--        AND @decManagerOfOwner <> 0 
--        SET @CommManagerOfOwnerAmt = @ManagerOfOwnerAmt * @decManagerOfOwner / 100
--    IF @bitCommOwner = 1
--        AND @decCommOwner <> 0 
--        SET @CommOwnerAmt = @OwnerAmount * @decCommOwner / 100
--    IF @bitCommAssignee = 1
--        AND @decCommAssignee <> 0 
--        SET @CommAssignedAmt = @AssignedAmount * @decCommAssignee / 100
--
--
--    SET @TotalCommAmt = ISNULL(@CommManagerOfOwnerAmt, 0)
--        + ISNULL(@CommOwnerAmt, 0) + ISNULL(@CommAssignedAmt, 0)
--        + ISNULL(@CommRoleAmt, 0)
--        
--        if @TotalCommAmt>0
--			begin
--				
--				insert into BizDocComission select @numDomainId,@numUserCntID,@numOppBizDocID,@TotalCommAmt;
--				insert into #TempUserComission select @numUserCntID,@TotalCommAmt,@@IDENTITY,@vcContactName;
--			end
--        
--      
--      
--      set @TotalCommAmt=0;
--      set @CommManagerOfOwnerAmt=0;
--      set @CommOwnerAmt=0;
--      set @CommAssignedAmt=0;
--      set @CommRoleAmt=0;
--      
--      
--      FETCH NEXT FROM Biz_Users into @numUserId,@numUserCntID,@vcContactName
--   END;
--
--CLOSE Biz_Users;
--DEALLOCATE Biz_Users;
--
--    
--SELECT * FROM #TempUserComission;
----select * from #TempUserData
--
--DROP TABLE #TempUserComission;
--drop table #TempUserData;
    
    END

GO


/****** Object:  StoredProcedure [dbo].[USP_AssignCaseRecOwner]    Script Date: 07/26/2008 16:21:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AssignCaseRecOwner')
DROP PROCEDURE USP_AssignCaseRecOwner
GO
CREATE PROCEDURE [dbo].[USP_AssignCaseRecOwner]   
@numRecOwner as numeric(9)=0,
@numAssignedTo as numeric(9)=0,  
@numCaseId as numeric(9)=0,
@numDomainID as numeric(9)=0  
as  
	 update [Cases] set [numRecOwner]=@numRecOwner,
	 [numAssignedBy]=@numRecOwner,
	 [numAssignedTo]=@numAssignedTo where [numCaseId]=@numCaseId  and numDomainID=@numDomainID
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Usp_CheckServiceItemInOrder')
DROP PROCEDURE Usp_CheckServiceItemInOrder
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE Usp_CheckServiceItemInOrder
(
	@numOppID		NUMERIC(18,0),
	@numDomainID	NUMERIC(18,0),
	@numItemCode	NUMERIC(18,0),
	@sMode			TINYINT = 0
)
AS 
BEGIN
	DECLARE @RecCount AS INT
	IF @sMode = 0
		BEGIN
			IF ISNULL(@numOppID,0) <> 0
				BEGIN
					
					SELECT @RecCount = COUNT(*) FROM 
					dbo.OpportunityMaster OM 
					JOIN dbo.OpportunityItems OI ON OM.numOppId = OI.numOppId
					WHERE OI.numOppId = @numOppID
					AND numDomainID = @numDomainID
					AND numItemCode = @numItemCode
					
					SELECT CASE WHEN @RecCount > 0 THEN 1 ELSE 0 END AS Result
				END
			ELSE
				BEGIN
					SELECT @RecCount = COUNT(*) FROM dbo.Item WHERE numItemCode = @numItemCode AND numDomainID = @numDomainID AND item.charItemType = 'S'
					SELECT CASE WHEN @RecCount > 0 THEN 1 ELSE 0 END AS Result
				END	
		 END		
	ELSE IF @sMode = 1
		BEGIN
			SELECT @RecCount = COUNT(*) FROM 
				dbo.OpportunityMaster OM 
				JOIN dbo.OpportunityBizDocs OI ON OM.numOppId = OI.numOppId AND bitAuthoritativeBizDocs = 1
				JOIN dbo.OpportunityBizDocItems OBI ON OI.numOppBizDocsId = OBI.numOppBizDocID AND OBI.numItemCode = @numItemCode
				WHERE OI.numOppId = @numOppID
				AND numDomainID = @numDomainID
			
			IF @RecCount = 0
			BEGIN
				SELECT @RecCount = COUNT(*) FROM 
				dbo.OpportunityMaster OM 
				JOIN dbo.OpportunityBizDocs OI ON OM.numOppId = OI.numOppId AND bitAuthoritativeBizDocs = 1
				WHERE OI.numOppId = @numOppID
				AND numDomainID = @numDomainID	
			END				
			
			SELECT CASE WHEN @RecCount > 0 THEN 1 ELSE 0 END AS Result
		END			
END

--Created by anoop jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_checkuseratlogin')
DROP PROCEDURE usp_checkuseratlogin
GO
CREATE PROCEDURE [dbo].[USP_CheckUserAtLogin]                                                              
(                                                                                    
@UserName as varchar(100)='',                                    
@vcPassword as varchar(100)=''                                                                           
)                                                              
as                                                              


/*check subscription validity */
IF EXISTS( SELECT *
FROM UserMaster U                              
 Join Domain D                              
 on D.numDomainID=U.numDomainID
 Join  Subscribers S                            
 on S.numTargetDomainID=D.numDomainID
 WHERE vcEmailID=@UserName and vcPassword=@vcPassword and bitActive=1 AND getutcdate() > dtSubEndDate)
BEGIN
	RAISERROR('SUB_RENEW',16,1)
	RETURN;
END



                                                                
 SELECT top 1 U.numUserID,numUserDetailId,isnull(vcEmailID,'') vcEmailID,isnull(numGroupID,0) numGroupID,bitActivateFlag,                              
 isnull(vcMailNickName,'') vcMailNickName,isnull(txtSignature,'') txtSignature,isnull(U.numDomainID,0) numDomainID,                              
 isnull(Div.numDivisionID,0) numDivisionID,isnull(vcCompanyName,'') vcCompanyName,isnull(E.bitExchangeIntegration,0) bitExchangeIntegration,                              
 isnull(E.bitAccessExchange,0) bitAccessExchange,isnull(E.vcExchPath,'') vcExchPath,isnull(E.vcExchDomain,'') vcExchDomain,                              
 isnull(D.vcExchUserName,'') vcExchUserName ,isnull(vcFirstname,'')+' '+isnull(vcLastName,'') as ContactName,                              
 Case when E.bitAccessExchange=0 then  E.vcExchPassword when E.bitAccessExchange=1 then D.vcExchPassword end as vcExchPassword,                          
 tintCustomPagingRows,                         
 vcDateFormat,                         
 numDefCountry,                     
 tintComposeWindow,
 dateadd(day,-sintStartDate,getutcdate()) as StartDate,                        
 dateadd(day,sintNoofDaysInterval,dateadd(day,-sintStartDate,getutcdate())) as EndDate,                        
 tintAssignToCriteria,                         
 bitIntmedPage,                         
 tintFiscalStartMonth,numAdminID,isnull(A.numTeam,0) numTeam ,                
 isnull(D.vcCurrency,'') as vcCurrency, 
 isnull(D.numCurrencyID,0) as numCurrencyID, 
 isnull(D.bitMultiCurrency,0) as bitMultiCurrency,              
 bitTrial,isnull(tintChrForComSearch,0) as tintChrForComSearch,  
isnull(tintChrForItemSearch,0) as tintChrForItemSearch,              
 case when bitSMTPServer= 1 then vcSMTPServer else '' end as vcSMTPServer  ,          
 case when bitSMTPServer= 1 then  isnull(numSMTPPort,0)  else 0 end as numSMTPPort      
,isnull(bitSMTPAuth,0) bitSMTPAuth    
,isnull([vcSmtpPassword],'') vcSmtpPassword    
,isnull(bitSMTPSSL,0) bitSMTPSSL   ,isnull(bitSMTPServer,0) bitSMTPServer,       
isnull(IM.bitImap,0) bitImapIntegration, isnull(charUnitSystem,'E')   UnitSystem, datediff(day,getutcdate(),dtSubEndDate) as NoOfDaysLeft,
ISNULL(bitCreateInvoice,0) bitCreateInvoice,
ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
vcDomainName,
S.numSubscriberID,
D.[tintBaseTax],
ISNULL(D.[numShipCompany],0) numShipCompany,
ISNULL(D.[bitEmbeddedCost],0) bitEmbeddedCost,
(Select isnull(numAuthoritativeSales,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativeSales,
(Select isnull(numAuthoritativePurchase,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativePurchase,
ISNULL(D.[numDefaultSalesOppBizDocId],0) numDefaultSalesOppBizDocId,
ISNULL(tintDecimalPoints,2) tintDecimalPoints,
ISNULL(D.tintBillToForPO,0) tintBillToForPO, 
ISNULL(D.tintShipToForPO,0) tintShipToForPO,
ISNULL(D.tintOrganizationSearchCriteria,0) tintOrganizationSearchCriteria,
ISNULL(D.tintLogin,0) tintLogin,
ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,ISNULL(D.tintComAppliesTo,0) tintComAppliesTo,
ISNULL(D.vcPortalName,'') vcPortalName,
(SELECT COUNT(*) FROM dbo.Subscribers WHERE getutcdate() between dtSubStartDate and dtSubEndDate and bitActive=1),
ISNULL(D.bitGtoBContact,0) bitGtoBContact,
ISNULL(D.bitBtoGContact,0) bitBtoGContact,
ISNULL(D.bitGtoBCalendar,0) bitGtoBCalendar,
ISNULL(D.bitBtoGCalendar,0) bitBtoGCalendar,
ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
ISNULL(D.bitInlineEdit,0) bitInlineEdit,
ISNULL(U.numDefaultClass,0) numDefaultClass,
ISNULL(D.bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
ISNULL(D.bitTrakDirtyForm,1) bitTrakDirtyForm,
isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
ISNULL(D.bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
isnull(U.tintTabEvent,0) as tintTabEvent,
isnull(D.bitRentalItem,0) as bitRentalItem,
isnull(D.numRentalItemClass,0) as numRentalItemClass,
isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
ISNULL(U.numDefaultWarehouse,0) numDefaultWarehouse,
ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
ISNULL(D.bitAutoSerialNoAssign,0) bitAutoSerialNoAssign,
ISNULL(IsEnableClassTracking,0) AS [IsEnableClassTracking],
ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
ISNULL(D.tintCommissionType,1) AS tintCommissionType,
ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
ISNULL(D.IsEnableUserLevelClassTracking,0) AS IsEnableUserLevelClassTracking,
ISNULL(D.bitUseBizdocAmount,1) AS [bitUseBizdocAmount],
ISNULL(D.bitDefaultRateType,1) AS [bitDefaultRateType],
ISNULL(D.bitIncludeTaxAndShippingInCommission,1) AS [bitIncludeTaxAndShippingInCommission]
/*,ISNULL(D.bitAllowPPVariance,0) AS bitAllowPPVariance*/
 FROM UserMaster U                              
 left join ExchangeUserDetails E                              
 on E.numUserID=U.numUserID                              
left join ImapUserDetails IM                              
 on IM.numUserCntID=U.numUserDetailID          
 Join Domain D                              
 on D.numDomainID=U.numDomainID                                           
 left join DivisionMaster Div                                            
 on D.numDivisionID=Div.numDivisionID                               
 left join CompanyInfo C                              
 on C.numCompanyID=Div.numDivisionID                               
 left join AdditionalContactsInformation A                              
 on  A.numContactID=U.numUserDetailId                            
 Join  Subscribers S                            
 on S.numTargetDomainID=D.numDomainID                             
 WHERE vcEmailID=@UserName and vcPassword=@vcPassword and bitActive IN (1,2) AND getutcdate() between dtSubStartDate and dtSubEndDate
             
select numTerritoryId from UserTerritory UT                              
join UserMaster U                              
on numUserDetailId =UT.numUserCntID                                         
where vcEmailID=@UserName and vcPassword=@vcPassword
/****** Object:  StoredProcedure [dbo].[usp_CompanyDivision]    Script Date: 07/26/2008 16:15:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_companydivision')
DROP PROCEDURE usp_companydivision
GO
CREATE PROCEDURE [dbo].[usp_CompanyDivision]                                         
 @numDivisionID  numeric=0,                                           
 @numCompanyID  numeric=0,                                           
 @vcDivisionName  varchar (100)='',                                                                
 @numGrpId   numeric=0,                                                                       
 @numTerID   numeric=0,                                          
 @bitPublicFlag  bit=0,                                          
 @tintCRMType  tinyint=0,                                          
 @numUserCntID  numeric=0,                                                                                                                                     
 @numDomainID  numeric=0,                                          
 @bitLeadBoxFlg  bit=0,  --Added this because when called form leadbox this value will be 1 else 0                                          
 @numStatusID  numeric=0,                                        
 @numCampaignID numeric=0,                            
 @numFollowUpStatus numeric(9)=0,                                                           
 @tintBillingTerms as tinyint,                                          
 @numBillingDays as numeric(9),                                         
 @tintInterestType as tinyint,                                          
 @fltInterest as float,                          
 @vcComPhone as varchar(50),                
 @vcComFax as varchar(50),        
 @numAssignedTo as numeric(9)=0,
 @bitNoTax as BIT,
 @bitSelectiveUpdate BIT=0,
@numCompanyDiff as numeric(9)=0,
@vcCompanyDiff as varchar(100)='',
@bitActiveInActive as bit=1,
@numCurrencyID AS numeric(9)=0,
@vcShipperAccountNo	VARCHAR(100),
@intShippingCompany INT = 0,
@numDefaultExpenseAccountID NUMERIC(18,0) = 0         
AS                                                                       
BEGIN                                          
                                         
                                         
 IF @numDivisionId is null OR @numDivisionId=0                                          
 BEGIN                                                                       
  INSERT INTO DivisionMaster                      
(numCompanyID,vcDivisionName,numGrpId,numFollowUpStatus,                      
    bitPublicFlag,numCreatedBy,bintCreatedDate,                      
     numModifiedBy,bintModifiedDate,tintCRMType,numDomainID,                      
     bitLeadBoxFlg,numTerID,numStatusID,bintLeadProm,bintLeadPromBy,bintProsProm,bintProsPromBy,                      
     numRecOwner,tintBillingTerms,                      
     numBillingDays,tintInterestType,fltInterest,vcComPhone,vcComFax,numCampaignID,bitNoTax,numAssignedTo,numAssignedBy,numCompanyDiff,vcCompanyDiff,numCurrencyID,vcShippersAccountNo,intShippingCompany,numDefaultExpenseAccountID)                     
   VALUES(@numCompanyID, @vcDivisionName, @numGrpId, 0,                                                                         
   @bitPublicFlag, @numUserCntID, getutcdate(), @numUserCntID,getutcdate(), @tintCRMType, @numDomainID, @bitLeadBoxFlg, @numTerID, @numStatusID,                                   
   NULL, NULL, NULL, NULL, @numUserCntID,0,0,0,0,@vcComPhone,@vcComFax,@numCampaignID,@bitNoTax,@numAssignedTo,@numUserCntID,@numCompanyDiff,@vcCompanyDiff,@numCurrencyID,@vcShipperAccountNo,@intShippingCompany,@numDefaultExpenseAccountID)                                       
                                    
  --Return the ID auto generated by the above INSERT.                                          
  SELECT @numDivisionId = @@IDENTITY
	DECLARE @numGroupID NUMERIC       
	SELECT TOP 1 @numGroupID=numGroupID FROM dbo.AuthenticationGroupMaster WHERE numDomainID=@numDomainID AND tintGroupType=2
	IF @numGroupID IS NULL SET @numGroupID = 0          
                                    
   insert into ExtarnetAccounts (numCompanyID,numDivisionID,numGroupID,numDomainID)                                  
     values(@numCompanyID,@numDivisionId,@numGroupID,@numDomainID)                                  
                              
                                         
                                       
       SELECT @numDivisionId                                    
                                     
 END                                          
 ELSE                                         
 BEGIN                                          


	IF @bitSelectiveUpdate = 0 
	BEGIN
			declare @numFollow as varchar(10)                          
			declare @binAdded as varchar(20)                          
			declare @PreFollow as varchar(10)                          
			declare @RowCount as varchar(2)                          
			set @PreFollow=(select count(*)  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID)                          
			set @RowCount=@@rowcount                          
			select   @numFollow=numFollowUpStatus,   @binAdded=bintModifiedDate from divisionmaster where numDivisionID=@numDivisionID and numDomainId= @numDomainID                    
			if @numFollow <>'0' and @numFollow <> @numFollowUpStatus                          
			begin                          
			select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID order by numFollowUpStatusID desc                          
			                    
			if @PreFollow<>0                          
			begin                          
			                   
			if @numFollow <> (select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID order by numFollowUpStatusID desc)                          
			begin                          
			                      
				  insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
						 values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
				end                          
			end                          
			else                          
			begin                          
			                     
			insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
						 values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
			end                          
			                         
			end                          
			              
			                                      
			UPDATE DivisionMaster SET  numCompanyID = @numCompanyID ,vcDivisionName = @vcDivisionName,                                      
			numGrpId = @numGrpId,                                                           
			numFollowUpStatus =@numFollowUpStatus,                                                                 
			numTerID = @numTerID,                                       
			bitPublicFlag =@bitPublicFlag,                                            
			numModifiedBy = @numUserCntID,                                            
			bintModifiedDate = getutcdate(),                                         
			tintCRMType = @tintCRMType,                                          
			numStatusID = @numStatusID,                                                                            
			tintBillingTerms = @tintBillingTerms,                                          
			numBillingDays = @numBillingDays,                                         
			tintInterestType = @tintInterestType,                                           
			fltInterest =@fltInterest,                
			vcComPhone=@vcComPhone,                
			vcComFax=@vcComFax,            
			numCampaignID=@numCampaignID,
			bitNoTax=@bitNoTax,numCompanyDiff=@numCompanyDiff,vcCompanyDiff=@vcCompanyDiff,bitActiveInActive=@bitActiveInActive,numCurrencyID=@numCurrencyID,
			vcShippersAccountNo = @vcShipperAccountNo,
			intShippingCompany = @intShippingCompany,
			numDefaultExpenseAccountID = @numDefaultExpenseAccountID                                                  
			WHERE numDivisionID = @numDivisionID   and numDomainId= @numDomainID     
			    
			    
			---Updating if organization is assigned to someone            
			declare @tempAssignedTo as numeric(9)          
			set @tempAssignedTo=null           
			select @tempAssignedTo=isnull(numAssignedTo,0) from DivisionMaster where numDivisionID = @numDivisionID and numDomainId= @numDomainID           
			print @tempAssignedTo          
			if (@tempAssignedTo<>@numAssignedTo and  @numAssignedTo<>'0')          
			begin            
			update DivisionMaster set numAssignedTo=@numAssignedTo ,numAssignedBy=@numUserCntID where numDivisionID = @numDivisionID  and numDomainId= @numDomainID           
			end           
			else if  (@numAssignedTo =0)          
			begin          
			update DivisionMaster set numAssignedTo=0 ,numAssignedBy=0 where numDivisionID = @numDivisionID  and numDomainId= @numDomainID        
			end                                            
			                      
			                                 
			SELECT @numDivisionID
 
 
 
		END                     
	END
	
	IF @bitSelectiveUpdate = 1
	BEGIN
		DECLARE @sql VARCHAR(1000)
		SET @sql = 'update DivisionMaster Set '

		IF(LEN(@vcComPhone)>0)
		BEGIN
			SET @sql =@sql + ' vcComPhone=''' + @vcComPhone + ''', '
		END
		IF(LEN(@vcComFax)>0)
		BEGIN
			SET @sql =@sql + ' vcComFax=''' + @vcComFax + ''', '
		END
		
		IF(@numUserCntID>0)
		BEGIN
			SET @sql =@sql + ' numModifiedBy='+ CONVERT(VARCHAR(10),@numUserCntID) +', '
		END
	
		SET @sql=SUBSTRING(@sql,0,LEN(@sql)) + ' '
		SET @sql =@sql + 'where numDivisionID= '+CONVERT(VARCHAR(10),@numDivisionID)

		PRINT @sql
		EXEC(@sql)
	END
	
	

end
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_createbizdocs')
DROP PROCEDURE usp_createbizdocs
GO
CREATE PROCEDURE [dbo].[USP_CreateBizDocs]                        
(                                              
 @numOppId as numeric(9)=null,                        
 @numBizDocId as numeric(9)=null,                        
 @numUserCntID as numeric(9)=null,                        
 @numOppBizDocsId as numeric(9)=NULL OUTPUT,
 @vcComments as varchar(1000),
 @bitPartialFulfillment as bit,
 @strBizDocItems as text='' ,
 --@bitDiscountType as bit,
 --@fltDiscount  as float,
 --@monShipCost as money,
 @numShipVia as numeric(9),
 @vcTrackingURL as varchar(1000),
 --@bitBillingTerms as bit,
 --@intBillingDays as integer,
 @dtFromDate AS DATETIME,
 --@bitInterestType as bit,
 --@fltInterest as float,
-- @numShipDoc as numeric(9),
 @numBizDocStatus as numeric(9) = 0,
 @bitRecurringBizDoc AS BIT = NULL,
 @numSequenceId AS VARCHAR(50) = '',
 --@tintTaxOperator AS tinyint,
 @tintDeferred as tinyint=0,
 @monCreditAmount as money= 0 OUTPUT,
 @ClientTimeZoneOffset as int=0,
 @monDealAmount as money= 0 OUTPUT,
 @bitRentalBizDoc as bit=0,
 @numBizDocTempID as numeric(9)=0,
 @vcTrackingNo AS VARCHAR(500),
 --@vcShippingMethod AS VARCHAR(100),
 @vcRefOrderNo AS VARCHAR(100),
 --@dtDeliveryDate AS DATETIME,
 @OMP_SalesTaxPercent AS FLOAT = 0, --(joseph) Added to Get Online Marketplace Sales Tax Percentage
 @numFromOppBizDocsId AS NUMERIC(9)=0,
 @bitTakeSequenceId AS BIT=0,
 @bitAllItems AS BIT=0,
 @bitNotValidateBizDocFulfillment AS BIT=0,
 @fltExchangeRateBizDoc AS FLOAT=0
)                        
as 

BEGIN TRY

declare @hDocItem as INTEGER
declare @numDivisionID as numeric(9)
DECLARE @numDomainID NUMERIC(9)
DECLARE @tintOppType AS TINYINT	
DECLARE @numFulfillmentOrderBizDocId NUMERIC(9);SET @numFulfillmentOrderBizDocId=296 
	    
IF ISNULL(@fltExchangeRateBizDoc,0)=0
	SELECT @fltExchangeRateBizDoc=fltExchangeRate FROM OpportunityMaster where numOppID=@numOppId
	
select @numDomainID=numDomainID,@numDivisionID=numDivisionID,@tintOppType=tintOppType from OpportunityMaster where numOppID=@numOppId

IF @numBizDocTempID=0
BEGIN
	select @numBizDocTempID=numBizDocTempID from BizDocTemplate where numBizDocId=@numBizDocId 
	and numDomainID=@numDomainID and numOpptype=@tintOppType 
	and tintTemplateType=0 and isnull(bitDefault,0)=1 and isnull(bitEnabled,0)=1
END

if @numOppBizDocsId=0
BEGIN
IF (
	(@bitPartialFulfillment = 1 AND NOT EXISTS (SELECT * FROM   OpportunityBizDocs WHERE  numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 0))
     OR (NOT EXISTS (SELECT * FROM   OpportunityBizDocs WHERE  numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 0)
         AND NOT EXISTS (SELECT * FROM   OpportunityBizDocs WHERE  numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 1))
     OR (@bitRecurringBizDoc = 1) -- Added new flag to add unit qty in case of recurring bizdoc
     OR (@numFromOppBizDocsId >0)
    )
begin                      
		
		
		--DECLARE  @vcBizDocID  AS VARCHAR(100)
        --DECLARE  @numBizMax  AS NUMERIC(9)
        DECLARE @tintShipped AS TINYINT	
        DECLARE @dtShipped AS DATETIME
        DECLARE @bitAuthBizdoc AS BIT
        
        
		select @tintShipped=ISNULL(tintShipped,0),@numDomainID=numDomainID from OpportunityMaster where numOppId=@numOppId                        
		
		IF @tintOppType = 1 AND @bitNotValidateBizDocFulfillment=0
			EXEC USP_ValidateBizDocFulfillment @numDomainID,@numOppId,@numFromOppBizDocsId,@numBizDocId
		
--		Select @numBizMax=count(*) from  OpportunityBizDocs                        
--		IF @numBizMax > 0
--          BEGIN
--				SELECT @numBizMax = MAX(numOppBizDocsId) FROM   OpportunityBizDocs
--          END
--		SET @numBizMax = @numBizMax
--        SET @vcBizDocID = @vcBizDocID + '-BD-' + CONVERT(VARCHAR(10),@numBizMax)
        IF @tintShipped =1 
			SET @dtShipped = GETUTCDATE();
		ELSE
			SET @dtShipped = null;
		IF @tintOppType = 1 AND (SELECT numAuthoritativeSales FROM [AuthoritativeBizDocs] WHERE numDomainID = @numDomainID) = @numBizDocId
			SET @bitAuthBizdoc = 1
		ELSE if @tintOppType = 2 AND (SELECT numAuthoritativePurchase FROM [AuthoritativeBizDocs] WHERE numDomainID = @numDomainID) = @numBizDocId
			SET @bitAuthBizdoc = 1
		ELSE 
			SET @bitAuthBizdoc = 0
		
		Declare @dtCreatedDate as datetime;SET @dtCreatedDate=Getutcdate()
		IF @tintDeferred=1
		BEGIn
			SET @dtCreatedDate=DateAdd(minute, @ClientTimeZoneOffset,@dtFromDate)
		END
	
	
		IF @bitTakeSequenceId=1
		BEGIN
			--Sequence #
			CREATE TABLE #tempSequence (numSequenceId bigint )
			INSERT INTO #tempSequence EXEC [dbo].[USP_GetScalerValue] @numDomainID ,33,@numBizDocId,0
			SELECT @numSequenceId =ISNULL(numSequenceId,0) FROM #tempSequence
			DROP TABLE #tempSequence
			
			--BizDoc Template ID
			CREATE TABLE #tempBizDocTempID (numBizDocTempID NUMERIC(9) )
			INSERT INTO #tempBizDocTempID EXEC [dbo].[USP_GetBizDocTemplateList] @numDomainID,@numBizDocId,1,1
			SELECT @numBizDocTempID =ISNULL(numBizDocTempID,0) FROM #tempBizDocTempID
			DROP TABLE #tempBizDocTempID
		END	
			
			
		IF @numFromOppBizDocsId>0 --If Created from Sales Fulfillment Workflow
		BEGIN
			INSERT INTO OpportunityBizDocs
                   (numOppId,numBizDocId,numCreatedBy,dtCreatedDate,numModifiedBy,dtModifiedDate,vcComments,bitPartialFulfilment,
                    monShipCost,numShipVia,vcTrackingURL,dtFromDate,numBizDocStatus,[dtShippedDate],
                    [bitAuthoritativeBizDocs],tintDeferred,bitRentalBizDoc,numBizDocTempID,/*vcTrackingNo,vcShippingMethod,dtDeliveryDate,*/vcRefOrderNo,numSequenceId,numBizDocStatusOLD,bitAutoCreated)
            select @numOppId,@numBizDocId,@numUserCntID,Getutcdate(),@numUserCntID,Getutcdate(),@vcComments,@bitPartialFulfillment,
                    monShipCost,numShipVia,vcTrackingURL,dtFromDate,0,[dtShippedDate],
                    @bitAuthBizdoc,tintDeferred,bitRentalBizDoc,@numBizDocTempID,/*vcTrackingNo,vcShippingMethod,dtDeliveryDate,*/vcRefOrderNo,@numSequenceId,0,1
		    from OpportunityBizDocs OBD where  numOppId=@numOppId AND OBD.numOppBizDocsId=@numFromOppBizDocsId
		             
		END
		ELSE
		BEGIN
			INSERT INTO OpportunityBizDocs
                   (numOppId,numBizDocId,
                    --vcBizDocID,
                    numCreatedBy,
                    dtCreatedDate,
                    numModifiedBy,
                    dtModifiedDate,
                    vcComments,
                    bitPartialFulfilment,
                    --bitDiscountType,
                    --fltDiscount,
                    --monShipCost,
                    numShipVia,
                    vcTrackingURL,
                    --bitBillingTerms,
                    --intBillingDays,
                    dtFromDate,
                    --bitInterestType,
                    --fltInterest,
--                    numShipDoc,
                    numBizDocStatus,
                    --vcBizDocName,
                    [dtShippedDate],
                    [bitAuthoritativeBizDocs],tintDeferred,bitRentalBizDoc,numBizDocTempID,vcTrackingNo,
                    --vcShippingMethod,dtDeliveryDate,
                    vcRefOrderNo,numSequenceId,numBizDocStatusOLD,fltExchangeRateBizDoc,bitAutoCreated)
        VALUES     (@numOppId,
                    @numBizDocId,
                    --@vcBizDocID,
                    @numUserCntID,
                    @dtCreatedDate,
                    @numUserCntID,
                    Getutcdate(),
                    @vcComments,
                    @bitPartialFulfillment,
                    --@bitDiscountType,
                    --@fltDiscount,
                    --@monShipCost,
                    @numShipVia,
                    @vcTrackingURL,
                    --@bitBillingTerms,
                    --@intBillingDays,
                    @dtFromDate,
                    --@bitInterestType,
                    --@fltInterest,
--                    @numShipDoc,
                    @numBizDocStatus,--@numBizDocStatus,--@numBizDocStatus,
                    --@vcBizDocName,
                    @dtShipped,
                    ISNULL(@bitAuthBizdoc,0),@tintDeferred,@bitRentalBizDoc,@numBizDocTempID,@vcTrackingNo,
                    --@vcShippingMethod,@dtDeliveryDate,
                    @vcRefOrderNo,@numSequenceId,0,@fltExchangeRateBizDoc,0)
        END
        
		SET @numOppBizDocsId = @@IDENTITY
		
		--Deferred BizDocs : Create Recurring entry only for create Account Journal
		if @tintDeferred=1
		BEGIN
			exec USP_RecurringAddOpportunity @numOppId,@numOppBizDocsId,0,4,@dtCreatedDate,0,@numDomainID,0,0,100,''
		END

		-- Update name template set for bizdoc
		--DECLARE @tintType TINYINT
		--SELECT @tintType = CASE @tintOppType WHEN 1 THEN 3 WHEN 2 THEN 4 END;
		EXEC dbo.USP_UpdateBizDocNameTemplate @tintOppType,@numDomainID,@numOppBizDocsId

		IF @numFromOppBizDocsId>0
		BEGIN
			insert into                       
		   OpportunityBizDocItems                                                                          
  		   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes,/*vcTrackingNo,*/bitEmbeddedCost,monEmbeddedCost,/*monShipCost,vcShippingMethod,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate,tintTrackingStatus)
		   select @numOppBizDocsId,OBDI.numOppItemID,OBDI.numItemCode,OBDI.numUnitHour,OBDI.monPrice,OBDI.monTotAmount,OBDI.vcItemDesc,OBDI.numWarehouseItmsID,OBDI.vcType,OBDI.vcAttributes,OBDI.bitDropShip,OBDI.bitDiscountType,OBDI.fltDiscount,OBDI.monTotAmtBefDiscount,OBDI.vcNotes,/*OBDI.vcTrackingNo,*/OBDI.bitEmbeddedCost,OBDI.monEmbeddedCost,/*OBDI.monShipCost,OBDI.vcShippingMethod,OBDI.dtDeliveryDate,*/OBDI.dtRentalStartDate,OBDI.dtRentalReturnDate,OBDI.tintTrackingStatus
		   from OpportunityBizDocs OBD JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID 
		   JOIN OpportunityItems OI ON OBDI.numOppItemID=OI.numoppitemtCode
		   JOIN Item I ON OBDI.numItemCode=I.numItemCode
		   where  OBD.numOppId=@numOppId AND OBD.numOppBizDocsId=@numFromOppBizDocsId
		   AND OI.numoppitemtCode NOT IN(SELECT OBDI.numOppItemID FROM OpportunityBizDocs OBD JOIN dbo.OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocId WHERE numOppId=@numOppId AND numBizDocId=@numBizDocId)
		END
		ELSE IF /*@bitPartialFulfillment=1 or*/ (@bitPartialFulfillment=1 and DATALENGTH(@strBizDocItems)>2)
			BEGIN
				
			if DATALENGTH(@strBizDocItems)>2
			begin
				EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strBizDocItems 

					IF @bitRentalBizDoc=1
					BEGIN
						update OI set OI.monPrice=OBZ.monPrice,OI.monTotAmount=OBZ.monTotAmount,OI.monTotAmtBefDiscount=OBZ.monTotAmount
						from OpportunityItems OI
						Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
							WITH ( OppItemID numeric(9),Quantity numeric(18,10),monPrice money,monTotAmount money)) OBZ
						on OBZ.OppItemID=OI.numoppitemtCode
						where OBZ.OppItemID=OI.numoppitemtCode
					END
	
					insert into                       
				   OpportunityBizDocItems                                                                          
				   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes,/*vcTrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate)
				   
				   select @numOppBizDocsId,numoppitemtCode,numItemCode,Quantity,CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmount/numUnitHour)*Quantity END,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,
				   case when bitDiscountType=0 then fltDiscount when bitDiscountType=1 then (fltDiscount/numUnitHour)*Quantity else fltDiscount end ,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmtBefDiscount/numUnitHour)*Quantity END,Notes,/*TrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate  from OpportunityItems OI
				   Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
				   WITH                       
				   (OppItemID numeric(9),                               
				    Quantity numeric(18,10),
					Notes varchar(500),
					--TrackingNo varchar(500),
					--vcShippingMethod varchar(100),
					--monShipCost money,
					--dtDeliveryDate datetime
					monPrice MONEY,
					dtRentalStartDate datetime,dtRentalReturnDate datetime
					)) OBZ
				   on OBZ.OppItemID=OI.numoppitemtCode
				   where  numOppId=@numOppId AND ISNULL(OI.numUnitHour,0)>0 AND ISNULL(OI.numUnitHour,0)>0
				   
				EXEC sp_xml_removedocument @hDocItem 
			END

		end
		else
		BEGIN
		   	   insert into OpportunityBizDocItems                                                                          
  			   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount)
			   select @numOppBizDocsId,numoppitemtCode,numItemCode,CASE @bitRecurringBizDoc WHEN 1 THEN 1 ELSE numUnitHour END AS numUnitHour ,monPrice,CASE @bitRecurringBizDoc WHEN 1 THEN monPrice * 1 ELSE monTotAmount END AS monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount from OpportunityItems OI
			   where  numOppId=@numOppId AND ISNULL(OI.numUnitHour,0)>0
			   AND numoppitemtCode NOT IN(SELECT OBDI.numOppItemID FROM OpportunityBizDocs OBD JOIN dbo.OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocId WHERE numOppId=@numOppId AND numBizDocId=@numBizDocId)
		   
			IF @bitAuthBizdoc = 1 and @tintOppType = 1
				exec USP_AutoAssign_OppSerializedItem @numDomainID,@numOppID,@numOppBizDocsId
		end


               
--		insert into OpportunityBizDocTaxItems(numOppBizDocID, numTaxItemID, fltPercentage)
--		select @numOppBizDocsId,numTaxItemID,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,numTaxItemID,@numOppId,0) from TaxItems
--        where numDomainID= @numDomainID
--        union 
--        select @numOppBizDocsId,0,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,0)

		SET @numOppBizDocsID = @numOppBizDocsId
		--select @numOppBizDocsId
	end
	ELSE
	  BEGIN
  			SET @numOppBizDocsId=0
			SET @monDealAmount = 0
			SET @monCreditAmount = 0
			RAISERROR ('NOT_ALLOWED',16,1);
	  END
--	IF @tintTaxOperator = 3 -- Remove default Sales Tax percentage defined and Add Sales Tax percentage Used for Online Market Place
--		BEGIN
--				DELETE FROM [OpportunityBizDocTaxItems] WHERE [numOppBizDocID] = @numOppBizDocsId AND [numTaxItemID] = 0 
--				insert into OpportunityBizDocTaxItems(numOppBizDocID, numTaxItemID, fltPercentage)
--					select @numOppBizDocsId,0,@OMP_SalesTaxPercent
--		END	
end
else
BEGIN
	  UPDATE OpportunityBizDocs
	  SET    numModifiedBy = @numUserCntID,
			 dtModifiedDate = Getdate(),
			 vcComments = @vcComments,
			 --bitDiscountType = @bitDiscountType,
			 --fltDiscount = @fltDiscount,
			 --monShipCost = @monShipCost,
			 numShipVia = @numShipVia,
			 vcTrackingURL = @vcTrackingURL,
			 --bitBillingTerms = @bitBillingTerms,
			 --intBillingDays = @intBillingDays,
			 dtFromDate = @dtFromDate,
			 --bitInterestType = @bitInterestType,
			 --fltInterest = @fltInterest,
--			 numShipDoc = @numShipDoc,
			 --numBizDocStatus = @numBizDocStatus,
			 numSequenceId=@numSequenceId,
			 --[tintTaxOperator]=@tintTaxOperator,
			 numBizDocTempID=@numBizDocTempID,
			 vcTrackingNo=@vcTrackingNo,
			 --vcShippingMethod=@vcShippingMethod,dtDeliveryDate=@dtDeliveryDate,
			 vcRefOrderNo=@vcRefOrderNo,
			 fltExchangeRateBizDoc=@fltExchangeRateBizDoc
			 --,numBizDocStatusOLD=ISNULL(numBizDocStatus,0)
	  WHERE  numOppBizDocsId = @numOppBizDocsId
	  --SELECT @numOppBizDocsId


   	IF DATALENGTH(@strBizDocItems)>2
			BEGIN

				EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strBizDocItems 

                    delete from OpportunityBizDocItems  
					where numOppItemID not in (SELECT OppItemID FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
					WITH                       
				   ( OppItemID numeric(9)
					)) and numOppBizDocID=@numOppBizDocsId  
                  
                     
					IF @bitRentalBizDoc=1
					BEGIN
						update OI set OI.monPrice=OBZ.monPrice,OI.monTotAmount=OBZ.monTotAmount,OI.monTotAmtBefDiscount=OBZ.monTotAmount
						from OpportunityItems OI
						Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
							WITH ( OppItemID numeric(9),Quantity numeric(18,10),monPrice money,monTotAmount money)) OBZ
						on OBZ.OppItemID=OI.numoppitemtCode
						where OBZ.OppItemID=OI.numoppitemtCode
					END

					update OBI set OBI.numUnitHour= Quantity,OBI.monPrice=CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,OBI.monTotAmount=CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (OI.monTotAmount/OI.numUnitHour)*Quantity END,OBI.vcNotes= Notes,
					OBI.fltDiscount=case when OI.bitDiscountType=0 then OI.fltDiscount when OI.bitDiscountType=1 then (OI.fltDiscount/OI.numUnitHour)*Quantity else OI.fltDiscount end ,
					OBI.monTotAmtBefDiscount=(OI.monTotAmtBefDiscount/OI.numUnitHour)*Quantity ,                                                                    
					/*OBI.vcTrackingNo=TrackingNo,OBI.vcShippingMethod=OBZ.vcShippingMethod,OBI.monShipCost=OBZ.monShipCost,OBI.dtDeliveryDate=OBZ.dtDeliveryDate*/
				    OBI.dtRentalStartDate=OBZ.dtRentalStartDate,OBI.dtRentalReturnDate=OBZ.dtRentalReturnDate
					from OpportunityBizDocItems OBI
					join OpportunityItems OI
					on OBI.numOppItemID=OI.numoppitemtCode
					Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
					WITH                       
				   ( OppItemID numeric(9),                                     
				   Quantity numeric(18,10),
					Notes varchar(500),
					--TrackingNo varchar(500),
					--vcShippingMethod varchar(100),
					--monShipCost money,
					--dtDeliveryDate datetime,
					monPrice MONEY,
					dtRentalStartDate datetime,dtRentalReturnDate datetime
					)) OBZ
				   on OBZ.OppItemID=OI.numoppitemtCode
				   where  OI.numOppId=@numOppId and OBI.numOppBizDocID=@numOppBizDocsId AND ISNULL(OI.numUnitHour,0)>0


                   insert into                       
				   OpportunityBizDocItems                                                                          
				   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes/*,vcTrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate*/)                      
				   select @numOppBizDocsId,numoppitemtCode,numItemCode,Quantity,CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmount/numUnitHour)*Quantity END,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,
				   case when bitDiscountType=0 then fltDiscount when bitDiscountType=1 then (fltDiscount/numUnitHour)*Quantity else fltDiscount end ,(monTotAmtBefDiscount/numUnitHour)*Quantity,Notes/*,TrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate*/ from OpportunityItems OI
				   Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
				   WITH                       
				   ( OppItemID numeric(9),                                     
				   Quantity numeric(18,10),
					Notes varchar(500),
					monPrice MONEY
					--TrackingNo varchar(500),
					--vcShippingMethod varchar(100),
					--monShipCost money,
					--dtDeliveryDate DATETIME
					)) OBZ
				   on OBZ.OppItemID=OI.numoppitemtCode and OI.numoppitemtCode not in (select numOppItemID from OpportunityBizDocItems where numOppBizDocID =@numOppBizDocsId)
				   where  numOppId=@numOppId AND ISNULL(OI.numUnitHour,0)>0
                 
				EXEC sp_xml_removedocument @hDocItem 
			END


		
		/*Note: by chintan
			numTaxItemID= 0 which stands for default sales tax
		*/
--		IF @tintTaxOperator = 1 -- Add Sales Tax Blindly which doesn't  depends on if company/item has sales tax enabled
--		BEGIN
--				DECLARE @SalesTax float
--				SELECT @SalesTax = dbo.fn_CalSalesTaxAmt(@numOppBizDocsId,@numDomainID);
--				IF @SalesTax > 0 
--				BEGIN
--					DELETE FROM [OpportunityBizDocTaxItems] WHERE [numOppBizDocID]=@numOppBizDocsId AND [numTaxItemID]=0
--					insert into OpportunityBizDocTaxItems(numOppBizDocID, numTaxItemID, fltPercentage)
--					select @numOppBizDocsId,0,@SalesTax
--				END
--				
--		END
--		ELSE IF @tintTaxOperator = 2 -- Remove Sales Tax Blindly which doesn't  depends on if company/item has sales tax enabled
--		BEGIN
--				DELETE FROM [OpportunityBizDocTaxItems] WHERE [numOppBizDocID] = @numOppBizDocsId AND [numTaxItemID] = 0 
--		END
END

IF @numOppBizDocsId>0
BEGIN
	select @monDealAmount=isnull(dbo.[GetDealAmount](@numOppId,GETUTCDATE(),@numOppBizDocsId),0)

	--Credit Balance
--	Declare @CAError as int;Set @CAError=0
--
--   	Declare @monOldCreditAmount as money;Set @monOldCreditAmount=0
--	Select @monOldCreditAmount=isnull(monCreditAmount,0) from OpportunityBizDocs WHERE  numOppBizDocsId = @numOppBizDocsId
--
--	if @monCreditAmount>0
--	BEGIN
--			IF (@monDealAmount + @monOldCreditAmount) >= @monCreditAmount
--			BEGIN
--				IF exists (select * from [CreditBalanceHistory] where  numOppBizDocsId=@numOppBizDocsId)
--						Update [CreditBalanceHistory] set [monAmount]=@monCreditAmount * -1 where numOppBizDocsId=@numOppBizDocsId
--				ELSE
--						 INSERT INTO [CreditBalanceHistory]
--							(numOppBizDocsId,[monAmount],[dtCreateDate],[dtCreatedBy],[numDomainId],numDivisionID)
--						VALUES (@numOppBizDocsId,@monCreditAmount * -1,GETDATE(),@numUserCntID,@numDomainId,@numDivisionID)
--			END	
--			ELSE
--			BEGIN	
--				SET @monCreditAmount=0
--				SET @CAError=-1
--			END
--	  END
--	  ELSE
--	  BEGIN
--			delete from [CreditBalanceHistory] where numOppBizDocsId=@numOppBizDocsId
--	  END
--	
--	IF @tintOppType=1 --Sales
--	  update DivisionMaster set monSCreditBalance=isnull(monSCreditBalance,0) + isnull(@monOldCreditAmount,0) - isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
--    else IF @tintOppType=2 --Purchase
--	  update DivisionMaster set monPCreditBalance=isnull(monPCreditBalance,0) + isnull(@monOldCreditAmount,0) - isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
--		

	  --Update OpportunityBizDocs set monCreditAmount=@monCreditAmount   WHERE  numOppBizDocsId = @numOppBizDocsId
	  Update OpportunityBizDocs set vcBizDocID=vcBizDocName + ISNULL(@numSequenceId,''),monDealAmount=isnull(dbo.[GetDealAmount](@numOppId,GETUTCDATE(),@numOppBizDocsId),0)   WHERE  numOppBizDocsId = @numOppBizDocsId

	 --SET @monCreditAmount =Case when @CAError=-1 then -1 else @monCreditAmount END
END
 
	 END TRY
BEGIN CATCH
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return error
    -- information about the original error that caused
    -- execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
               );
END CATCH;
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteWorkFlowMaster')
DROP PROCEDURE USP_DeleteWorkFlowMaster
GO
CREATE PROCEDURE [dbo].[USP_DeleteWorkFlowMaster]     
    @numDomainID numeric(18, 0),
	@numWFID numeric(18, 0)
as                 

DELETE FROM WorkFlowTriggerFieldList WHERE numWFID=@numWFID 

DELETE FROM WorkFlowConditionList WHERE numWFID=@numWFID 

DELETE FROM WorkFlowActionUpdateFields WHERE numWFActionID IN(SELECT numWFActionID FROM WorkFlowActionList WHERE numWFID=@numWFID)

DELETE FROM WorkFlowActionList WHERE numWFID=@numWFID 


DELETE FROM WorkFlowMaster WHERE numDomainID=@numDomainID AND numWFID=@numWFID
/****** Object:  StoredProcedure [dbo].[USP_DocumentWorkFlow]    Script Date: 07/26/2008 16:15:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_documentworkflow')
DROP PROCEDURE usp_documentworkflow
GO
CREATE PROCEDURE [dbo].[USP_DocumentWorkFlow]      
@numDocID as numeric(9)=0,      
@numContactID as numeric(9)=0,      
@cDocType as varchar(10)='',      
@byteMode as tinyint,
@vcComment as text='',
@numUserCntID AS NUMERIC(9)=0
as      
      
DECLARE @numBizActionId NUMERIC;SET @numBizActionId=0

if @byteMode=1      
begin

/*validate if given contact is internal/external user*/
IF NOT EXISTS (SELECT 'col1' FROM dbo.UserMaster WHERE numUserDetailID = @numContactID )
BEGIN
	IF NOT EXISTS(SELECT 'col1' FROM dbo.ExtranetAccountsDtl EAD INNER JOIN dbo.AdditionalContactsInformation ACI ON EAD.numDomainID = ACI.numDomainID
	AND EAD.numContactID =ACI.numContactId WHERE EAD.numContactID=@numContactID)
	BEGIN
		raiserror('No_EXTRANET',16,1);
		RETURN ;		
	END
END



 delete from DocumentWorkflow where numDocID=@numDocID and numContactID=@numContactID and cDocType=@cDocType      
 insert into DocumentWorkflow(numDocID,numContactID,cDocType,dtCreatedDate)      
 values (@numDocID,@numContactID,@cDocType,GETUTCDATE()) 
 --Add to Tickler,bizdoc action item
 IF @cDocType='B'OR @cDocType='D'--bizdoc,generic docs & specific
 BEGIN
		DECLARE @numDivisionID NUMERIC
		DECLARE @numDomainID NUMERIC
		SELECT  @numDivisionID = numDivisionId,@numDomainID=numDomainID FROM dbo.AdditionalContactsInformation WHERE numContactId=@numContactID
	IF NOT EXISTS(SELECT * FROM BizDocAction B JOIN dbo.BizActionDetails BA
	ON BA.numBizActionId = B.numBizActionId WHERE B.numDomainID=@numDomainID AND B.numContactId = @numContactID AND BA.numOppBizDocsId=@numDocID)
	BEGIN
		insert into dbo.BizDocAction(numContactId ,numDivisionId,numStatus,numDomainID,dtCreatedDate,numAssign,numCreatedBy,bitTask,numBizDocAppId)
		values (@numContactID,@numDivisionID,0,@numDomainID,getdate(),@numContactID,@numUserCntID,972,0);
		set @numBizActionId=SCOPE_IDENTITY()							
		
--		PRINT @numBizActionId
		-- btDocType =2 for Document approval request, =1 for bizdoc
		INSERT INTO BizActionDetails (numBizActionId,numOppBizDocsId,btStatus,btDocType)
		VALUES (@numBizActionId,@numDocID,0,CASE WHEN @cDocType='D' THEN 2 ELSE 1 END)
	END
		 
		

 END
 
end
else if @byteMode=2      
begin      
 delete from DocumentWorkflow where numDocID=@numDocID and numContactID=@numContactID and cDocType=@cDocType      
  IF @cDocType='B'OR @cDocType='D'--bizdoc,generic docs & specific
 BEGIN
	Select @numBizActionId=BA.numBizActionId from BizDocAction BA join BizActionDetails BD on BA.numBizActionId=BD.numBizActionId
		 where BD.numOppBizDocsId=@numDocID and BD.btDocType=(CASE WHEN @cDocType='D' THEN 2 ELSE 1 END)
		 and BA.numContactID=@numContactID 

	DELETE from BizActionDetails where numBizActionId=@numBizActionId 
	DELETE from  BizDocAction where numBizActionId=@numBizActionId
--	DELETE FROM dbo.BizDocAction WHERE numBizActionId IN (SELECT numBizActionId FROM dbo.BizActionDetails WHERE numOppBizDocsId=@numDocID)
--	DELETE FROM dbo.BizActionDetails WHERE numOppBizDocsId=@numDocID
 END
 
end      
else if @byteMode=3      
begin      
	 update DocumentWorkflow set dtApprovedOn=getutcdate(),tintApprove=1,vcComment =@vcComment  where numDocID=@numDocID and numContactID=@numContactID and cDocType=@cDocType
	 
	 IF @cDocType='B'OR @cDocType='D'--bizdoc,generic docs & specific
	 BEGIN
	 Select @numBizActionId=BA.numBizActionId from BizDocAction BA join BizActionDetails BD on BA.numBizActionId=BD.numBizActionId
		 where BD.numOppBizDocsId=@numDocID and BD.btDocType=(CASE WHEN @cDocType='D' THEN 2 ELSE 1 END)
		 and BA.numContactID=@numContactID 

		UPDATE dbo.BizActionDetails SET btStatus=1 WHERE numBizActionId=@numBizActionId
		UPDATE dbo.BizDocAction SET numStatus=1 WHERE numBizActionId=@numBizActionId
	 END
end    
else if @byteMode=4     
begin      
 update DocumentWorkflow set dtApprovedOn=getutcdate(),tintApprove=2,vcComment =@vcComment where numDocID=@numDocID and numContactID=@numContactID and cDocType=@cDocType      
end
GO
/****** Object:  StoredProcedure [dbo].[USP_GetBizDocDetail]    Script Date: 09/01/2009 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetBizDocDetail')
DROP PROCEDURE USP_GetBizDocDetail
GO
CREATE PROCEDURE [dbo].[USP_GetBizDocDetail]
               @numOppBizDocsId NUMERIC(9),
               @numDomainId     NUMERIC(9)
AS
  BEGIN
    SELECT BD.vcBizDocID,
           AD1.vcCity vcBillCity,
           isnull(dbo.fn_GetListName(AD1.numCountry,0),'')  as vcBillCountry,
           isnull(dbo.fn_GetState(AD1.numState),'') as vcBilState,
		   isnull(AD1.vcPostalCode,'') as vcBillPostCode,	
           AD1.vcStreet vcBillStreet,
           AC.vcFirstName,
           AC.vcLastName,
           AC.vcEmail,
           AC.numPhone,
           AC.numContactId,
           ISNULL(OM.[bitBillingTerms],0) bitBillingTerms,
           ISNULL(OM.[intBillingDays],0) intBillingDays,
           OM.[numDivisionId],
           OM.[tintOppType],
           ISNULL(BD.monAmountPaid,0) monAmountPaid,
           ISNULL(BD.monDealAmount,0) monDealAmount,
           dbo.fn_GetComapnyName(DM.numDivisionID) AS vcCompanyName ,--added by chintan, to be used in Receive payment
           BD.numOppId,
           Bd.numBizDocTempID,
           Bd.numBizDocId
    FROM   OpportunityBizDocs BD
           INNER JOIN OpportunityMaster OM
             ON BD.numOppId = OM.numOppId
           INNER JOIN AdditionalContactsInformation AC
             ON OM.numContactId = AC.numContactId
           INNER JOIN DivisionMaster DM
             ON AC.numDivisionId = DM.numDivisionID
           LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID
			AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
    WHERE  bd.numOppBizDocsId = @numOppBizDocsId
           AND OM.numDomainId = @numDomainId

  END
/****** Object:  StoredProcedure [dbo].[USP_GetCaseList1]    Script Date: 07/26/2008 16:16:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--  By Anoop Jayaraj                                                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcaselist1')
DROP PROCEDURE usp_getcaselist1
GO
CREATE PROCEDURE [dbo].[USP_GetCaseList1]                                                               
 @numUserCntId numeric(9)=0,                                            
 @tintSortOrder numeric=0,                                                                                
 @numDomainID numeric(9)=0,                                                            
 @tintUserRightType tinyint,                                                            
 @SortChar char(1)='0' ,                                                            
 @CustName varchar(100)='',                                                            
 @FirstName varChar(100)= '',                                                            
 @LastName varchar(100)='',                                                            
 @CurrentPage int,                                                            
 @PageSize int,                                                            
 @TotRecs int output,                                                            
 @columnName as Varchar(50),                                                            
 @columnSortOrder as Varchar(10),                                                          
 @numDivisionID as numeric(9)=0,                              
 @bitPartner as bit=0,
 @vcRegularSearchCriteria varchar(1000)='',
 @vcCustomSearchCriteria varchar(1000)='' ,
 @ClientTimeZoneOffset as INT,                                                          
 @numStatus AS BIGINT = 0
AS          
declare @column as varchar(50)         
set @column = @columnName                
declare @join as varchar(400)          
set @join = ''         
declare @lookbckTable as varchar(50)              
set @lookbckTable = ''          
          
if @column like 'CFW.Cust%'         
begin        
        
 set @column='CFW.Fld_Value'        
 set @join= ' left Join CFW_FLD_Values_Case CFW on CFW.RecId=cs.numCaseId  and CFW.fld_id= '+replace(@columnName,'CFW.Cust','')                                                  
        
          
end                                 
if @Column like 'DCust%'        
begin        
        
 set @column='LstCF.vcData'                                                  
 set @join= ' left Join CFW_FLD_Values_Case CFW on CFW.RecId=cs.numCaseId and CFW.fld_id= '+replace(@columnName,'DCust','')                                                                                           
 set @join= @join +' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value'        
        
end         
                                                       
declare @firstRec as integer                                                            
declare @lastRec as integer                                                            
set @firstRec= (@CurrentPage-1) * @PageSize                                                            
set @lastRec= (@CurrentPage*@PageSize+1)                                                   

DECLARE @strShareRedordWith AS VARCHAR(300);
SET @strShareRedordWith=' Cs.numCaseID in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=7 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '
                                                              
declare @strSql as varchar(8000)                                                            
set @strSql='with tblcases as (SELECT  '                                               
if @tintSortOrder=3 or @tintSortOrder=4  set @strSql=@strSql + ' top 20 '                  
   set  @strSql= @strSql+ ' ROW_NUMBER() OVER (ORDER BY '+ CASE WHEN PATINDEX('%text%',@column)>0 THEN ' CONVERT(VARCHAR(Max),' + @column + ')'  ELSE @column end +' '+ @columnSortOrder+') AS RowNumber,Cs.numCaseID,
   COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount                                                                             
 FROM                
   AdditionalContactsInformation ADC                                                             
 JOIN Cases  Cs                                        
   ON ADC.numContactId =Cs.numContactId                                                             
 JOIN DivisionMaster Div                                                               
  ON ADC.numDivisionId = Div.numDivisionID'                              
   if @bitPartner=1 set @strSql=@strSql+' left join CaseContacts CCont on CCont.numCaseID=Cs.numCaseID and CCont.bitPartner=1 and CCont.numContactId='+convert(varchar(15),@numUserCntID)+ ''                              
                                                               
   set @strSql=@strSql+' JOIN CompanyInfo CMP                                                             
 ON Div.numCompanyID = CMP.numCompanyId                                                             
 left join listdetails lst                                                            
 on lst.numListItemID= cs.numPriority                       
 left join listdetails lst1                                                            
 on lst1.numListItemID= cs.numStatus                                                           
 left join AdditionalContactsInformation ADC1                          
 on Cs.numAssignedTo=ADC1.numContactId          
  '+@join +'                                                            
 Where cs.numDomainID='+ convert(varchar(15),@numDomainID ) + ''
 --+ ' AND numStatus = (SELECT numListItemID FROM dbo.ListDetails WHERE numListID = 14 AND constFlag = 1)'                                      
 
 IF @numStatus <> 0
	 BEGIN
		SET @strSql= @strSql + ' AND cs.numStatus = ' + CONVERT(VARCHAR(10),@numStatus)                                      	
	 END
 ELSE
	 BEGIN
 		SET @strSql= @strSql + ' AND cs.numStatus <> 136 ' 
	 END
 
 
 if @FirstName<>'' set @strSql=@strSql+'and ADC.vcFirstName  like '''+@FirstName+'%'''                                                                 
 
 if @LastName<>'' set @strSql=@strSql+'and ADC.vcLastName like '''+@LastName+'%'''                                      
 
 if @CustName<>'' set @strSql=@strSql+' and CMP.vcCompanyName like '''+@CustName+'%'''                                                            
                                                             
                                                           
if @numDivisionID <>0 set @strSql=@strSql + ' And div.numDivisionID =' + convert(varchar(15),@numDivisionID)                                                      
if @SortChar<>'0' set @strSql=@strSql + ' And (ADC.vcFirstName like '''+@SortChar+'%'''                                                              
if @SortChar<>'0' set @strSql=@strSql + ' or ADC.vcLastName like '''+@SortChar+'%'')'                                                             

IF @tintSortOrder!=6
BEGIN
	if @tintUserRightType=1 set @strSql=@strSql + ' AND (Cs.numRecOwner = '+convert(varchar(15),@numUserCntId)+ ' or Cs.numAssignedTo='+convert(varchar(15),@numUserCntID) + 
	+ case when @tintSortOrder=1 THEN ' OR (Cs.numRecOwner = -1 or Cs.numAssignedTo= -1)' ELSE '' end
	+ case when @bitPartner=1 then ' or (CCont.bitPartner=1 and CCont.numContactId='+convert(varchar(15),@numUserCntID)+ ')' else '' end  + ' or ' + @strShareRedordWith +')'                
	else if @tintUserRightType=2 set @strSql=@strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntId)+' ) or Div.numTerID=0 or Cs.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ' or ' + @strShareRedordWith +')'                   
END
ELSE
BEGIN
	SET @strSql=@strSql + ' AND (Cs.numRecOwner = -1 or Cs.numAssignedTo= -1)'
END                 
--if @numDivisionID=0                                                    
--begin                                                    
 if @tintSortOrder=0  set @strSql=@strSql + ' AND (Cs.numRecOwner = '+convert(varchar(15),@numUserCntId)+' or Cs.numAssignedTo = '+convert(varchar(15),@numUserCntId)+ ' or ' + @strShareRedordWith +')'                                         
--end                                                            
ELSE if @tintSortOrder=2  set @strSql=@strSql + 'And cs.numStatus<>136  ' + ' AND cs.bintCreatedDate> '''+convert(varchar(20),dateadd(day,-7,getutcdate()))+''''                                 
else if @tintSortOrder=3  set @strSql=@strSql+'And cs.numStatus<>136  ' + ' and cs.numCreatedBy ='+ convert(varchar(15),@numUserCntId)       
else if @tintSortOrder=4  set @strSql=@strSql+ 'And cs.numStatus<>136  ' + ' and cs.numModifiedBy ='+ convert(varchar(15),@numUserCntId)                                                            
else if @tintSortOrder=5  set @strSql=@strSql+ 'And cs.numStatus=136  ' + ' and cs.numModifiedBy ='+ convert(varchar(15),@numUserCntId)                                                            
 
IF @vcRegularSearchCriteria<>'' set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
IF @vcCustomSearchCriteria<>'' set @strSql=@strSql+' and cs.numCaseId in (select distinct CFW.RecId from CFW_FLD_Values_Case CFW where ' + @vcCustomSearchCriteria + ')'
    
set @strSql=@strSql + ')'             
                                    
                                                             
declare @tintOrder as tinyint                                                  
declare @vcFieldName as varchar(50)                                                  
declare @vcListItemType as varchar(3)                                             
declare @vcListItemType1 as varchar(1)                                                 
declare @vcAssociatedControlType varchar(20)                                                  
declare @numListID AS numeric(9)                                                  
declare @vcDbColumnName varchar(20)                      
declare @WhereCondition varchar(2000)                       
declare @vcLookBackTableName varchar(2000)                
Declare @bitCustom as bit          
Declare @numFieldId as numeric  
DECLARE @bitAllowSorting AS CHAR(1)   
DECLARE @bitAllowEdit AS CHAR(1)                 
declare @vcColumnName AS VARCHAR(500)                  
               
set @tintOrder=0                                                  
set @WhereCondition =''                 
                   
declare @Nocolumns as tinyint                
set @Nocolumns=0                
 
select @Nocolumns=isnull(sum(TotalRow),0)  from(            
Select count(*) TotalRow from View_DynamicColumns where numFormId=12 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1
Union 
Select count(*) TotalRow from View_DynamicCustomColumns where numFormId=12 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1) TotalRows
                  
-- declare @DefaultNocolumns as tinyint             
--set @DefaultNocolumns=  @Nocolumns    

  CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1))

set @strSql=@strSql+' select ADC.numContactId,DM.numDivisionID,isnull(DM.numTerID,0) numTerID,cs.numRecOwner,ISNULL(cs.numAssignedTo,0) AS numAssignedTo,DM.tintCRMType,cs.numCaseID,RowNumber'                
  
if @Nocolumns=0
BEGIN
INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
select 12,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,NULL,1,0,intColumnWidth
 FROM View_DynamicDefaultColumns
 where numFormId=12 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID
order by tintOrder asc      

END
            
  

INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
 FROM View_DynamicColumns 
 where numFormId=12 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
       
       UNION
    
       select tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,0,''
 from View_DynamicCustomColumns
where numFormId=12 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
 AND ISNULL(bitCustom,0)=1
 
  order by tintOrder asc  
  
                                         
              
--set @DefaultNocolumns=  @Nocolumns              
Declare @ListRelID as numeric(9) 
       
  select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from  #tempForm order by tintOrder asc            
                                        
while @tintOrder>0                                
begin                                                  
 if @bitCustom = 0        
 begin        
    declare @Prefix as varchar(5)                
      if @vcLookBackTableName = 'AdditionalContactsInformation'                
    set @Prefix = 'ADC.'                
      if @vcLookBackTableName = 'DivisionMaster'                
    set @Prefix = 'DM.'                
      if @vcLookBackTableName = 'Cases'                
    set @PreFix ='Cs.'      
    
    SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'
    
     if @vcAssociatedControlType='SelectBox'                                                  
     begin                                                  
                                     
     if @vcListItemType='LI'                                                   
     begin                                                  
      set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                  
      set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                  
     end                                                  
     else if @vcListItemType='S'                                                   
     begin                                                  
      set @strSql=@strSql+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName+']'                                                  
      set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID='+@vcDbColumnName                                                  
     end                                                  
     else if @vcListItemType='T'                                                   
     begin                                                  
      set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                  
      set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                  
     end                 
     else if   @vcListItemType='U'                                               
    begin                 
                
                 
    set @strSql=@strSql+',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'                                                  
    end                
    end           
    else if @vcAssociatedControlType='DateField'                                                  
   begin            
		IF @vcDbColumnName='intTargetResolveDate'
		BEGIN
	 set @strSql=@strSql+',case when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
     set @strSql=@strSql+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
     set @strSql=@strSql+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '      
     set @strSql=@strSql+'else dbo.FormatedDateFromDate('+@Prefix+@vcDbColumnName+','+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'               
		END
	ELSE
	BEGIN
     set @strSql=@strSql+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
     set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
     set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ' 
     set @strSql=@strSql+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'
END
   -- end      
--  else      
--     set @strSql=@strSql+',dbo.FormatedDateFromDate('+@vcDbColumnName+','++convert(varchar(10),@numDomainId)+') ['+ @vcFieldName+'~'+ @vcDbColumnName+']'            
   end          
    else if @vcAssociatedControlType='TextBox'                                                  
   begin           
     set @strSql=@strSql+','+ case                
    when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'               
    when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID'               
    when @vcDbColumnName='textSubject' then 'convert(varchar(max),textSubject)' 
	when @vcDbColumnName='textDesc' then    'convert(varchar(max),textDesc)'           
	when @vcDbColumnName='textInternalComments' then 'convert(varchar(max),textInternalComments)'
    else @vcDbColumnName end+' ['+ @vcColumnName+']'            
   end  
  else if  @vcAssociatedControlType='TextArea'                                              
  begin  
       set @strSql=@strSql+', '+ @vcDbColumnName +' ['+ @vcColumnName+']'            
   end 
    	else if @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
		begin      
			 set @strSql=@strSql+',(SELECT SUBSTRING(
	(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=Cs.numDomainID AND SR.numModuleID=7 AND SR.numRecordID=Cs.numCaseID
				AND UM.numDomainID=Cs.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
		 end     
 end                                            
 Else                                                    
 Begin        
       
--   select @vcFieldName=FLd_label,@vcAssociatedControlType=fld_type,@vcDbColumnName='Cust'+Convert(varchar(10),Fld_Id)  from CFW_Fld_Master                                         
--   where  CFW_Fld_Master.Fld_Id = @numFieldId                                    
       
   SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'
       
        
    if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'          
   begin         
           
    set @strSql= @strSql+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName + ']'           
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Case CFW'+ convert(varchar(3),@tintOrder)+ '         
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=cs.numCaseId   '                                                 
   end        
           
   else if @vcAssociatedControlType = 'CheckBox'          
   begin          
             
    set @strSql= @strSql+',case when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=0 then ''No'' when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=1 then ''Yes'' end   ['+ @vcColumnName + ']'             
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Case CFW'+ convert(varchar(3),@tintOrder)+ '           
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=cs.numCaseId   '                                                   
   end          
   else if @vcAssociatedControlType = 'DateField'           
   begin        
           
    set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName + ']'           
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Case CFW'+ convert(varchar(3),@tintOrder)+ '         
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=cs.numCaseId   '                                                 
   end        
   else if @vcAssociatedControlType = 'SelectBox'            
   begin          
    set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)        
    set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName + ']'                                                  
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Case CFW'+ convert(varchar(3),@tintOrder)+ '         
     on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=cs.numCaseId   '                                                 
    set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'        
   end           
 End        
                                                
   select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc            
 
  if @@rowcount=0 set @tintOrder=0 
           
end                       

      
-------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

--select top 1 @vcCSOrigDbCOlumnName=DFM.vcOrigDbCOlumnName,@vcCSLookBackTableName=DFM.vcLookBackTableName from DycFieldColorScheme DFCS join DycFieldMaster DFM on 
--DFCS.numModuleID=DFM.numModuleID and DFCS.numFieldID=DFM.numFieldID
--where DFCS.numDomainID=@numDomainID and DFM.numModuleID=1

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=12 AND DFCS.numFormID=12 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
---------------------------- 
 
IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
set @strSql=@strSql+',tCS.vcColorScheme'
END
                                                                   
 set @strSql=@strSql+' ,TotalRowCount FROM                                                               
   AdditionalContactsInformation ADC                                                             
 JOIN Cases  Cs                                                              
   ON ADC.numContactId =Cs.numContactId                                                             
 JOIN DivisionMaster DM                                                              
  ON ADC.numDivisionId = DM.numDivisionID                                                             
  JOIN CompanyInfo CMP                                                             
 ON DM.numCompanyID = CMP.numCompanyId                                                             
 left join listdetails lst                                                            
 on lst.numListItemID= cs.numPriority                                                  
     '+@WhereCondition+  '                              
 join tblcases T on T.numCaseID=Cs.numCaseID'                                                            
              
--' union select count(*),null,null,null,null,null '+@strColumns+' from tblcases order by RowNumber'               
                                                         
   
   IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
	 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
		set @Prefix = 'ADC.'                  
     if @vcCSLookBackTableName = 'DivisionMaster'                  
		set @Prefix = 'DM.'
	 if @vcCSLookBackTableName = 'CompanyInfo'                  
		set @Prefix = 'CMP.' 
	 if @vcCSLookBackTableName = 'Cases'                  
		set @Prefix = 'Cs.'   
 
	IF @vcCSAssociatedControlType='DateField'
	BEGIN
			set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
			 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
	END
	ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
	BEGIN
		set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
	END
END
    

set @strSql=@strSql+' WHERE RowNumber > '+convert(varchar(10),@firstRec)+ ' and RowNumber <'+ convert(varchar(10),@lastRec) + ' order by RowNumber'

                                          
print @strSql                      
              
exec (@strSql)  

SELECT * FROM #tempForm

DROP TABLE #tempForm

drop table #tempColorScheme
GO
/****** Object:  StoredProcedure [dbo].[USP_GetColumnConfiguration1]    Script Date: 07/26/2008 16:16:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcolumnconfiguration1')
DROP PROCEDURE usp_getcolumnconfiguration1
GO
CREATE PROCEDURE [dbo].[USP_GetColumnConfiguration1]                    
 @numDomainID Numeric(9),                      
 @numUserCntId Numeric(9),                    
 @FormId tinyint ,                  
 @numtype numeric(9),
@numViewID numeric(9)=0                        
AS                                
Declare @PageId as tinyint         
if @FormId = 10   OR @FormId = 44        
 set @PageId = 4          
else if @FormId = 11  OR @FormId = 34 OR @FormId = 35  OR @FormId = 36 OR @FormId = 96 OR @FormId = 97
 set @PageId = 1          
else if @FormId = 12          
 set @PageId = 3          
else if @FormId = 13          
 set @PageId = 11       
else if (@FormId = 14 and  ( @numtype= 1 or @numtype= 3)) OR @FormId = 33 OR @FormId = 38 OR @FormId = 39
 set @PageId = 2          
else if @FormId = 14 and   (@numtype= 2 or @numtype= 4) OR @FormId = 40 OR @FormId = 41
 set @PageId = 6  
else if @FormId = 21 OR @FormId = 26 OR @FormId = 32 
 set @PageId = 5   
else if @FormId = 22 OR @FormId = 30           
 set @PageId = 5    
ELSE IF @FormId = 76
 set @PageId = 10 
ELSE IF @FormId = 84 OR @FormId = 85 
 SET @PageId = 5
                     
if @PageId = 1 Or @PageId = 4          
 begin          
   select convert(varchar(9),numFieldID)+'~0' as numFieldID,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,0 as Custom 
			FROM View_DynamicDefaultColumns
			 where numFormID=@FormId AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitDeleted,0)=0 and numDomainID=@numDomainID
             
   union    
         
   select convert(varchar(9),fld_id)+'~1' as numFieldID ,fld_label as vcFieldName,1 as Custom           
   from CFW_Fld_Master 
   left join CFW_Fld_Dtl                                          
   on Fld_id=numFieldId  and numRelation=case when @numtype in (1,2,3) then numRelation else @numtype end                                      
   left join CFw_Grp_Master                                           
   on subgrp=CFw_Grp_Master.Grp_id                                          
   where CFW_Fld_Master.grp_id=@PageId      
   and CFW_Fld_Master.numDomainID=@numDomainID   and Fld_type <> 'Link'             
   order by Custom,vcFieldName                                       
 end  
ELSE IF @PageId = 10 
	BEGIN          
	   SELECT CONVERT(VARCHAR(9),numFieldID)+'~0' as numFieldID,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,0 as Custom 
	   FROM View_DynamicDefaultColumns
	   WHERE numFormID=@FormId AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitDeleted,0)=0 and numDomainID=@numDomainID
	  
	   UNION 
	   
	   SELECT CONVERT(VARCHAR(9),fld_id)+'~1' as numFieldID ,fld_label as vcFieldName,1 as Custom           
	   FROM CFW_Fld_Master 
	   LEFT JOIN CFW_Fld_Dtl                                          
	   on Fld_id=numFieldId  and numRelation=CASE WHEN @numtype IN (1,2,3) THEN numRelation ELSE @numtype END                                      
	   LEFT JOIN CFw_Grp_Master ON subgrp=CFw_Grp_Master.Grp_id                                          
	   WHERE CFW_Fld_Master.grp_id = 5
	   AND CFW_Fld_Master.numDomainID = @numDomainID   
	   AND Fld_Type = 'SelectBox'           
	   ORDER BY Custom,vcFieldName       
	                                     
	END 

ELSE IF  @PageId = 5 AND ( @FormId = 84 OR @FormId = 85 )
BEGIN          
	SELECT  CONVERT(VARCHAR(9),numFieldID)+'~0' AS numFieldID,
			ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
			0 AS Custom,vcDbColumnName 
	FROM View_DynamicDefaultColumns
	WHERE numFormID=@FormId 
	AND ISNULL(bitSettingField,0)=1 
	AND ISNULL(bitDeleted,0)=0 
	AND numDomainID=@numDomainID
				       
	UNION     
	      
	SELECT  CONVERT(VARCHAR(9),fld_id)+'~1' AS numFieldID ,
			fld_label AS vcFieldName,1 AS Custom, 
			CONVERT(VARCHAR(10),fld_id) AS vcDbColumnName                               
	FROM CFW_Fld_Master                                           
	LEFT JOIN CFw_Grp_Master ON subgrp=CFw_Grp_Master.Grp_id                                          
	WHERE CFW_Fld_Master.grp_id = @PageId 
	AND CFW_Fld_Master.numDomainID=@numDomainID 
	AND Fld_type = 'TextBox'             
	ORDER BY Custom,vcFieldName             
END  
             
Else          
 Begin          
      
  select convert(varchar(9),numFieldID)+'~0' as numFieldID,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,0 AS Custom,vcDbColumnName 
			FROM View_DynamicDefaultColumns
			 where numFormID=@FormId AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitDeleted,0)=0 and numDomainID=@numDomainID
			       
  union     
      
  select convert(varchar(9),fld_id)+'~1' as numFieldID ,fld_label as vcFieldName,1 as Custom, CONVERT(VARCHAR(10),fld_id) AS vcDbColumnName                               
  from CFW_Fld_Master                                           
  left join CFw_Grp_Master                                           
  on subgrp=CFw_Grp_Master.Grp_id                                          
  where CFW_Fld_Master.grp_id=@PageId and CFW_Fld_Master.numDomainID=@numDomainID and Fld_type <> 'Link'             
      order by Custom,vcFieldName             
 End                      
                      
                   
if @PageId = 1 Or @PageId = 4     
 begin
          
   select convert(varchar(9),numFieldID)+'~0' numFieldID,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,vcDbColumnName ,0 as Custom,tintRow AS tintOrder          
   from View_DynamicColumns
   where numFormId=@FormId and numUserCntID=@numUserCntID and  numDomainID=@numDomainID  
   and ISNULL(bitCustom,0)=0 AND tintPageType=1 AND ISNULL(bitSettingField,0)=1 AND numRelCntType= @numtype AND ISNULL(bitDeleted,0)=0
           
   union
   
   select  convert(varchar(9),numFieldID)+'~1' as numFieldID ,vcFieldName,'' as vcDbColumnName,1 as Custom,tintRow AS tintOrder           
   from View_DynamicCustomColumns         
   where numFormID=@FormId  and numUserCntID=@numUserCntID and  numDomainID=@numDomainID   
   and grp_id=@PageId    
    and numDomainID=@numDomainID and vcAssociatedControlType <> 'Link'             
   and ISNULL(bitCustom,0)=1 AND tintPageType=1 AND numRelCntType= @numtype               
   order by tintOrder          
 End     
ELSE IF @PageId = 10
BEGIN
	
   SELECT CONVERT(VARCHAR(9),numFieldID)+'~0' numFieldID,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
   vcDbColumnName ,0 as Custom,tintRow AS tintOrder,numListID,numFieldID AS [numFieldID1]
   FROM View_DynamicColumns
   WHERE numFormId = @FormId 
   AND numUserCntID = @numUserCntID 
   AND  numDomainID = @numDomainID  
   AND ISNULL(bitCustom,0)=0 AND tintPageType=1 
   AND ISNULL(bitSettingField,0)=1 
   AND numRelCntType= @numtype 
   AND ISNULL(bitDeleted,0)=0
           
   union
   
   select  convert(varchar(9),numFieldID)+'~1' as numFieldID ,vcFieldName,'' as vcDbColumnName,1 as Custom,tintRow AS tintOrder,numListID,numFieldID AS [numFieldID1]
   from View_DynamicCustomColumns         
   where numFormID=@FormId  and numUserCntID=@numUserCntID and  numDomainID=@numDomainID   
   --and grp_id=@PageId    
   and numDomainID=@numDomainID 
   and vcAssociatedControlType = 'SelectBox'             
   and ISNULL(bitCustom,0)=1 
   AND tintPageType=1 
   AND numRelCntType= @numtype               
   order by tintOrder    
--   
--    SELECT DISTINCT numFieldID as numFieldID,'Archived Items' AS vcFieldName,bitCustom as Custom 
--	   FROM DycFormConfigurationDetails
--	   WHERE 1=1
--	   AND numFormID = @FormId
--	   AND ISNULL(numFieldID,0)= -1
--	   AND numDomainID = @numDomainID	
END	   
--ELSE IF @PageId = 12
--	BEGIN
--		SELECT  CONVERT(VARCHAR(9),numFieldID) + '~0' numFieldID,
--				ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
--				vcDbColumnName ,
--				0 AS Custom,
--				tintRow AS tintOrder,
--				vcAssociatedControlType,
--				numlistid,
--				vcLookBackTableName,
--				numFieldID AS FieldId,
--				bitAllowEdit                    
--		FROM View_DynamicColumns
--		WHERE numFormId = @FormId 
--		AND numUserCntID = @numUserCntID 
--		AND  numDomainID = @numDomainID  
--	    AND ISNULL(bitCustom,0) = 0 
--		AND tintPageType = 1 
--		AND ISNULL(bitSettingField,0) = 1 
--		AND ISNULL(numRelCntType,0) = @numtype 
--		AND ISNULL(bitDeleted,0) = 0
--	    AND  ISNULL(numViewID,0) = @numViewID   
--	   
--	    UNION
--	   
--	    SELECT  CONVERT(VARCHAR(9),numFieldID)+'~1' AS numFieldID ,
--				vcFieldName ,'' AS vcDbColumnName,
--				1 AS Custom,
--				tintRow AS tintOrder,
--				vcAssociatedControlType,
--				numlistid,'' AS vcLookBackTableName,
--				numFieldID AS FieldId,
--				0 AS bitAllowEdit           
--	    FROM View_DynamicCustomColumns          
--	    WHERE numFormID = @FormId 
--		AND numUserCntID = @numUserCntID 
--		--AND numDomainID=@numDomainID
--		AND grp_id = @PageId  
--		AND numDomainID = @numDomainID  
--		AND vcAssociatedControlType <> 'Link' 
--		AND ISNULL(bitCustom,0) = 1 
--		--AND tintPageType = 1  
--		---AND ISNULL(numRelCntType,0)= @numtype     
--		--AND  ISNULL(numViewID,0) = @numViewID
--	    ORDER BY tintOrder     
--
--	END 
ELSE IF  @PageId = 5 AND ( @FormId = 84 OR @FormId = 85 )     
BEGIN

IF Exists(Select 'col1' from DycFormConfigurationDetails where numDomainID = @numDomainID and numFormId = @FormID and numFieldID in (507,508,509,510,511,512) )
	BEGIN
		SELECT CONVERT(VARCHAR(9),numFieldID)+'~0' numFieldID,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,vcDbColumnName ,0 AS Custom,tintRow AS tintOrder,  
		 vcAssociatedControlType,numlistid,vcLookBackTableName,numFieldID AS FieldId,bitAllowEdit ,0 as AZordering                     
		 FROM View_DynamicColumns  
		 WHERE numFormId = @FormID  
		 AND  numDomainID = @numDomainID  
		 AND ISNULL(bitCustom,0) = 0   
		 --AND tintPageType = 1   
		 AND ISNULL(bitSettingField,0) = 1   
		 AND ISNULL(bitDeleted,0) = 0  
		  
		 UNION  
		     
		 SELECT  CONVERT(VARCHAR(9),numFieldID)+'~1' AS numFieldID ,vcFieldName ,'' AS vcDbColumnName,1 AS Custom,tintRow AS tintOrder,  
		 vcAssociatedControlType,numlistid,'' AS vcLookBackTableName,numFieldID AS FieldId,0 AS bitAllowEdit    ,0 as AZordering          
		 FROM View_DynamicCustomColumns            
		 WHERE numFormID = @FormID  
		 AND  numDomainID = @numDomainID  
		 AND grp_id = @PageID  
		 AND vcAssociatedControlType = 'TextBox'   
		 AND ISNULL(bitCustom,0)=1   
		ORDER BY tintOrder 
	END
ELSE

	BEGIN
	SELECT CONVERT(VARCHAR(9),numFieldID)+'~0' numFieldID,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,vcDbColumnName ,0 AS Custom,tintRow AS tintOrder,  
		 vcAssociatedControlType,numlistid,vcLookBackTableName,numFieldID AS FieldId,bitAllowEdit ,0 as AZordering                     
		 FROM View_DynamicColumns  
		 WHERE numFormId = @FormID  
		 AND  numDomainID = @numDomainID  
		 AND ISNULL(bitCustom,0) = 0   
		 --AND tintPageType = 1   
		 AND ISNULL(bitSettingField,0) = 1   
		 AND ISNULL(bitDeleted,0) = 0  
		  
		 UNION  
		     
		 SELECT  CONVERT(VARCHAR(9),numFieldID)+'~1' AS numFieldID ,vcFieldName ,'' AS vcDbColumnName,1 AS Custom,tintRow AS tintOrder,  
		 vcAssociatedControlType,numlistid,'' AS vcLookBackTableName,numFieldID AS FieldId,0 AS bitAllowEdit    ,0 as AZordering          
		 FROM View_DynamicCustomColumns            
		 WHERE numFormID = @FormID  
		 AND  numDomainID = @numDomainID  
		 AND grp_id = @PageID  
		 AND vcAssociatedControlType = 'TextBox'   
		 AND ISNULL(bitCustom,0)=1 
	UNION
	SELECT  '507~0' AS numFieldID ,'Title:A-Z' as vcFieldName ,'Title:A-Z' AS vcDbColumnName,1 AS Custom,
	 0 AS tintOrder,  
	 ' ' as vcAssociatedControlType,1 as numlistid,'' AS vcLookBackTableName,507 AS FieldId,0 AS bitAllowEdit ,1 as IsFixed
	UNION
	SELECT  '508~0' AS numFieldID ,'Title:Z-A' as vcFieldName ,'Title:Z-A' AS vcDbColumnName,1 AS Custom,
	 0 AS tintOrder,  
	 ' ' as vcAssociatedControlType,1 as numlistid,'' AS vcLookBackTableName,508 AS FieldId,0 AS bitAllowEdit ,1 as IsFixed
	UNION
	SELECT  '509~0' AS numFieldID ,'Price:Low to High' as vcFieldName ,'Price:Low to High' AS vcDbColumnName,1 AS Custom,
	 0 AS tintOrder,  
	 ' ' as vcAssociatedControlType,1 as numlistid,'' AS vcLookBackTableName,509 AS FieldId,0 AS bitAllowEdit ,1 as IsFixed
	UNION
	SELECT  '510~0' AS numFieldID ,'Price:High to Low' as vcFieldName ,'Price:High to Low' AS vcDbColumnName,1 AS Custom,
	 0 AS tintOrder,  
	 ' ' as vcAssociatedControlType,1 as numlistid,'' AS vcLookBackTableName,510 AS FieldId,0 AS bitAllowEdit ,1 as IsFixed
	UNION

	 SELECT  '511~0' AS numFieldID ,'New Arrival' as vcFieldName ,'New Arrival' AS vcDbColumnName,1 AS Custom,
	 0 AS tintOrder,  
	 ' ' as vcAssociatedControlType,1 as numlistid,'' AS vcLookBackTableName,511 AS FieldId,0 AS bitAllowEdit ,1 as IsFixed
	UNION
	SELECT  '512~0' AS numFieldID ,'Oldest' as vcFieldName ,'Oldest' AS vcDbColumnName,1 AS Custom,
	 0 AS tintOrder,  
	 ' ' as vcAssociatedControlType,1 as numlistid,'' AS vcLookBackTableName,512 AS FieldId,0 AS bitAllowEdit ,1 as IsFixed  
		ORDER BY tintOrder 

	END



    

END
Else          
 Begin  
 
   select convert(varchar(9),numFieldID)+'~0' numFieldID,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,vcDbColumnName ,0 as Custom,tintRow AS tintOrder,
   vcAssociatedControlType,numlistid,vcLookBackTableName,numFieldID as FieldId,bitAllowEdit                    
   from View_DynamicColumns
   where numFormId=@FormId and numUserCntID=@numUserCntID and  numDomainID=@numDomainID  
   and ISNULL(bitCustom,0)=0 AND tintPageType=1 AND ISNULL(bitSettingField,0)=1 AND isnull(numRelCntType,0)= @numtype AND ISNULL(bitDeleted,0)=0
    and  ISNULL(numViewID,0)=@numViewID   
   union
   
   select  convert(varchar(9),numFieldID)+'~1' as numFieldID ,vcFieldName ,'' as vcDbColumnName,1 as Custom,tintRow AS tintOrder,
	vcAssociatedControlType,numlistid,'' as vcLookBackTableName,numFieldID as FieldId,0 AS bitAllowEdit           
   from View_DynamicCustomColumns          
   where numFormID=@FormId and numUserCntID=@numUserCntID and  numDomainID=@numDomainID
		  and grp_id=@PageId  and numDomainID=@numDomainID  
		   and vcAssociatedControlType <> 'Link' and ISNULL(bitCustom,0)=1 AND tintPageType=1  AND isnull(numRelCntType,0)= @numtype     
			and  ISNULL(numViewID,0)=@numViewID
   order by tintOrder          
 End
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCompanyInfo]    Script Date: 07/26/2008 16:16:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--modified by anoop jayaraj                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcompanyinfo')
DROP PROCEDURE usp_getcompanyinfo
GO
CREATE PROCEDURE [dbo].[usp_GetCompanyInfo]                                                                                                   
 @numDivisionID numeric,        
 @numDomainID as numeric(9)   ,        
@ClientTimeZoneOffset  int           
    --                                                         
AS                                                          
BEGIN                                                          
 SELECT CMP.numCompanyID, CMP.vcCompanyName,DM.numStatusID as numCompanyStatus,                                                          
  CMP.numCompanyRating, CMP.numCompanyType, DM.bitPublicFlag, DM.numDivisionID,                                                           
  DM. vcDivisionName, 
  
  isnull(AD1.vcStreet,'') as vcBillStreet, 
  isnull(AD1.vcCity,'') as vcBillCity, 
  isnull(dbo.fn_GetState(AD1.numState),'') as vcBilState,
  isnull(AD1.vcPostalCode,'') as vcBillPostCode,
  isnull(dbo.fn_GetListName(AD1.numCountry,0),'')  as vcBillCountry,
  isnull(AD2.vcStreet,'') as vcShipStreet,
  isnull(AD2.vcCity,'') as vcShipCity,
  isnull(dbo.fn_GetState(AD2.numState),'')  as vcShipState,
  isnull(AD2.vcPostalCode,'') as vcShipPostCode, 
  AD2.numCountry vcShipCountry,
  numFollowUpStatus,
  CMP.vcWebLabel1, CMP.vcWebLink1, CMP.vcWebLabel2, CMP.vcWebLink2,                                                           
  CMP.vcWebLabel3, CMP.vcWebLink3, CMP.vcWeblabel4, CMP.vcWebLink4,  
   tintBillingTerms,numBillingDays,
   --ISNULL(dbo.fn_GetListItemName(isnull(numBillingDays,0)),0) AS numBillingDaysName,
   (SELECT numNetDueInDays FROM dbo.BillingTerms WHERE numTermsID = numBillingDays AND numDomainID = DM.numDomainID) AS numBillingDaysName,
   tintInterestType,fltInterest,                                                          
   DM.bitPublicFlag, DM.numTerID,                                                           
  CMP.numCompanyIndustry, CMP.numCompanyCredit,                                                           
  isnull(CMP.txtComments,'') as txtComments, isnull(CMP.vcWebSite,'') vcWebSite, CMP.numAnnualRevID, CMP.numNoOfEmployeesID, dbo.fn_GetListItemName(CMP.numNoOfEmployeesID) as NoofEmp,                                                         
  CMP.vcProfile, DM.numCreatedBy, dbo.fn_GetContactName(DM.numRecOwner) as RecOwner,       
dbo.fn_GetContactName(DM.numCreatedBy)+' ' +convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintCreatedDate)) as vcCreatedBy,                                                  
  DM.numRecOwner,    
 dbo.fn_GetContactName(DM.numModifiedBy)+' ' +convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintModifiedDate)) as vcModifiedBy,vcComPhone,vcComFax,                                                 
  DM.numGrpID, CMP.vcHow,                                                          
  DM.tintCRMType,isnull(DM.bitNoTax,0) bitNoTax ,                                                      
   numCampaignID,numAssignedBy,numAssignedTo,            
(select  count(*) from dbo.GenericDocuments where numRecID= @numDivisionID and  vcDocumentSection='A' AND tintDocumentType=2) as DocumentCount ,          
(SELECT count(*)from CompanyAssociations where numDivisionID=@numDivisionID and bitDeleted=0 ) as AssociateCountFrom,          
(SELECT count(*)from CompanyAssociations where numAssociateFromDivisionID=@numDivisionID and bitDeleted=0 ) as AssociateCountTo,
 ISNULL(DM.numCurrencyID,0) AS numCurrencyID,ISNULL(DM.numDefaultPaymentMethod,0) AS numDefaultPaymentMethod,ISNULL(DM.numDefaultCreditCard,0) AS numDefaultCreditCard,ISNULL(DM.bitOnCreditHold,0) AS bitOnCreditHold,
 ISNULL(vcShippersAccountNo,'') AS [vcShippersAccountNo],
 ISNULL(intShippingCompany,0) AS [intShippingCompany],ISNULL(DM.bitEmailToCase,0) AS bitEmailToCase,
 ISNULL(numDefaultExpenseAccountID,0) AS [numDefaultExpenseAccountID]
 FROM  CompanyInfo CMP                                                          
 join DivisionMaster DM    
	on DM.numCompanyID=CMP.numCompanyID
 LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
   AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
   LEFT JOIN dbo.AddressDetails AD2 ON AD1.numDomainID=DM.numDomainID 
   AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1

where                                            
   DM.numDivisionID=@numDivisionID   and DM.numDomainID=@numDomainID                                                                        
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCompanyInfoDtlPl]    Script Date: 07/26/2008 16:16:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--usp_GetCompanyInfoDtlPl 91099,72,-333
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcompanyinfodtlpl')
DROP PROCEDURE usp_getcompanyinfodtlpl
GO
CREATE PROCEDURE [dbo].[usp_GetCompanyInfoDtlPl]                                                                                                                   
@numDivisionID numeric(9) ,                      
@numDomainID numeric(9) ,        
@ClientTimeZoneOffset as int                              
    --                                                                           
AS                                                                            
BEGIN                                                                            
 SELECT CMP.numCompanyID,               
 CMP.vcCompanyName,                
 (select vcdata from listdetails where numListItemID = DM.numStatusID) as numCompanyStatusName,
 DM.numStatusID as numCompanyStatus,                                                                           
 (select vcdata from listdetails where numListItemID = CMP.numCompanyRating) as numCompanyRatingName, 
 CMP.numCompanyRating,             
 (select vcdata from listdetails where numListItemID = CMP.numCompanyType) as numCompanyTypeName,
 CMP.numCompanyType,              
 CMP.numCompanyType as numCmptype,               
 DM.bitPublicFlag,                 
 DM.numDivisionID,                                   
 DM. vcDivisionName,        
dbo.getCompanyAddress(dm.numdivisionId,1,dm.numDomainID) as Address,
dbo.getCompanyAddress(dm.numdivisionId,2,dm.numDomainID) as ShippingAddress,          
  isnull(AD2.vcStreet,'') as vcShipStreet,
  isnull(AD2.vcCity,'') as vcShipCity,
  isnull(dbo.fn_GetState(AD2.numState),'')  as vcShipState,
 (select vcdata from listdetails where numListItemID = numFollowUpStatus) as numFollowUpStatusName,
 numFollowUpStatus,                
 CMP.vcWebLabel1,               
 CMP.vcWebLink1,               
 CMP.vcWebLabel2,               
 CMP.vcWebLink2,                                                                             
 CMP.vcWebLabel3,               
 CMP.vcWebLink3,               
 CMP.vcWeblabel4,               
 CMP.vcWebLink4,                                                                       
 tintBillingTerms,              
 numBillingDays,              
 tintInterestType,              
 fltInterest,                                                  
 isnull(AD2.vcPostalCode,'') as vcShipPostCode, 
 AD2.numCountry vcShipCountry,                          
 DM.bitPublicFlag,              
 (select vcdata from listdetails where numListItemID = DM.numTerID)  as numTerIDName,
 DM.numTerID,                                                                         
 (select vcdata from listdetails where numListItemID = CMP.numCompanyIndustry) as numCompanyIndustryName, 
  CMP.numCompanyIndustry,              
 (select vcdata from listdetails where numListItemID = CMP.numCompanyCredit) as  numCompanyCreditName,
 CMP.numCompanyCredit,                                                                             
 isnull(CMP.txtComments,'') as txtComments,               
 isnull(CMP.vcWebSite,'') vcWebSite,              
 (select vcdata from listdetails where numListItemID = CMP.numAnnualRevID) as  numAnnualRevIDName,
 CMP.numAnnualRevID,                
 (select vcdata from listdetails where numListItemID = CMP.numNoOfEmployeesID) as numNoOfEmployeesIDName,               
 numNoOfEmployeesID,                                                                           
    (select vcdata from listdetails where numListItemID = CMP.vcProfile) as vcProfileName, 
 CMP.vcProfile,              
 DM.numCreatedBy,               
 dbo.fn_GetContactName(DM.numRecOwner) as RecOwner,               
 dbo.fn_GetContactName(DM.numCreatedBy)+' ' + convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintCreatedDate)) as vcCreatedBy,               
 DM.numRecOwner,                
 dbo.fn_GetContactName(DM.numModifiedBy)+' ' +convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintModifiedDate)) as vcModifiedBy,                    
 isnull(vcComPhone,'') as vcComPhone, 
 vcComFax,                   
 DM.numGrpID,  
 (select vcGrpName from Groups where numGrpId= DM.numGrpID) as numGrpIDName,              
 (select vcdata from listdetails where numListItemID = CMP.vcHow) as vcInfoSourceName,
 CMP.vcHow as vcInfoSource,                                                                            
 DM.tintCRMType,                                                                         
 (SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= DM.numCampaignID) as  numCampaignIDName,
 numCampaignID,              
 numAssignedBy, isnull(bitNoTax,0) bitNoTax,                     
 (select A.vcFirstName+' '+A.vcLastName                       
 from AdditionalContactsInformation A                  
 join DivisionMaster D                        
 on D.numDivisionID=A.numDivisionID
 where A.numContactID=DM.numAssignedTo) as numAssignedToName,numAssignedTo,
 (select  count(*) from dbo.GenericDocuments where numRecID= @numDivisionID and vcDocumentSection='A' AND tintDocumentType=2) as DocumentCount ,
 (SELECT count(*)from CompanyAssociations where numDivisionID=@numDivisionID and bitDeleted=0 ) as AssociateCountFrom,                            
 (SELECT count(*)from CompanyAssociations where numAssociateFromDivisionID=@numDivisionID and bitDeleted=0 ) as AssociateCountTo,
 (select vcdata from listdetails where numListItemID = DM.numCompanyDiff) as numCompanyDiffName,               
 DM.numCompanyDiff,DM.vcCompanyDiff,isnull(DM.bitActiveInActive,1) as bitActiveInActive,
(SELECT SUBSTRING(
	(SELECT ','+  A.vcFirstName+' ' +A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN '(' + dbo.fn_GetListItemName(SR.numContactType) + ')' ELSE '' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=DM.numDomainID AND SR.numModuleID=1 AND SR.numRecordID=DM.numDivisionID
				AND UM.numDomainID=DM.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('')),2,200000)) AS numShareWith,
 DM.bitOnCreditHold                                                              
 FROM  CompanyInfo CMP    
  join DivisionMaster DM    
  on DM.numCompanyID=CMP.numCompanyID                                                                                                                                        
   LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
   AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
   LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
   AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1

 where DM.numDivisionID=@numDivisionID   and DM.numDomainID=@numDomainID                                                                                          
END
GO




/****** Object:  StoredProcedure [dbo].[USP_GetCompSpecificValues]    Script Date: 07/26/2008 16:16:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcompspecificvalues')
DROP PROCEDURE usp_getcompspecificvalues
GO
CREATE PROCEDURE [dbo].[USP_GetCompSpecificValues]        
@numCommID as numeric(9)=0,        
@numOppID as numeric(9)=0,        
@numProID as numeric(9)=0,        
@numCasesID as numeric(9)=0,        
@numDivID as numeric(9)=0 output,        
@numCompID as numeric(9)=0 output,        
@numContID as numeric(9)=0 output,        
@tintCRMType as numeric(9)=0 output,        
@numTerID as numeric(9)=0 output,        
@vcCompanyName as varchar(100)='' output,        
@vcContactName as varchar(100)='' output,        
@vcEmail as varchar(100)='' output,        
@vcPhone as varchar(20)='' output,
@charModule as char(1),        
@vcNoOfEmployeesId AS VARCHAR(30)='' OUTPUT,
@numCurrencyID AS numeric(9)=0 OUTPUT,
@bitOnCreditHold AS BIT=0 OUTPUT,
@numBillingDays AS BIGINT = 0 OUTPUT,
@numDefaultExpenseAccountID NUMERIC(18,0) = 0 OUTPUT
as        
        
if @charModule='A'         
begin        
 select @numDivID=isnull(D.numDivisionID,0),        
 @numCompID=isnull(D.numCompanyID,0),         
 @numContID=isnull(ADC.numContactID,0),        
  @tintCRMType=isnull(tintCRMType,0),        
 @numTerID=isnull(numTerID,0),        
        @vcCompanyName=isnull(vcCompanyName,''),        
        @vcContactName=isnull(vcFirstName,'')+' '+isnull(vcLastName,''),        
        @vcEmail=isnull(vcEmail,''),        
        @vcPhone=isnull(numPhone,''),
        @vcNoOfEmployeesId=dbo.GetListIemName(Com.numNoOfEmployeesId),
        @numCurrencyID=ISNULL(D.numCurrencyID,0),
        @bitOnCreditHold=ISNULL(D.bitOnCreditHold,0),
        @numBillingDays = ISNULL(D.numBillingDays,0),
        @numDefaultExpenseAccountID = ISNULL(numDefaultExpenseAccountID,0)
 from Communication C        
 join AdditionalContactsInformation ADC        
 on ADC.numContactID=C.numContactID        
 join DivisionMaster D        
 on D.numDivisionID=ADC.numDivisionID        
 join CompanyInfo Com        
        on Com.numCompanyID=D.numCompanyID        
 where numCommID=@numCommID        
end        
else        
        
if @charModule='O'       
begin        
 select @numDivID=isnull(D.numDivisionID,0),        
 @numCompID=isnull(D.numCompanyID,0),         
 @numContID=isnull(ADC.numContactID,0),        
  @tintCRMType=isnull(tintCRMType,0),        
 @numTerID=isnull(numTerID,0),        
 @vcCompanyName=isnull(vcCompanyName,'-'),        
        @vcContactName=isnull(vcFirstName,'-')+' '+isnull(vcLastName,''),        
        @vcEmail=isnull(vcEmail,''),        
        @vcPhone=isnull(numPhone,''),
        @vcNoOfEmployeesId=dbo.GetListIemName(Com.numNoOfEmployeesId),
        @numCurrencyID=ISNULL(D.numCurrencyID,0),
        @bitOnCreditHold=ISNULL(D.bitOnCreditHold,0),
        @numBillingDays = ISNULL(D.numBillingDays,0),
        @numDefaultExpenseAccountID = ISNULL(numDefaultExpenseAccountID,0) 
 from OpportunityMaster O        
 join AdditionalContactsInformation ADC        
 on ADC.numContactID=O.numContactID        
 join DivisionMaster D        
 on D.numDivisionID=ADC.numDivisionID        
 join CompanyInfo Com        
        on Com.numCompanyID=D.numCompanyID        
 where numOppID=@numOppID        
end        
else        
        
if @charModule='P'         
begin        
 select @numDivID=isnull(D.numDivisionID,0),        
 @numCompID=isnull(D.numCompanyID,0),         
 @numContID=isnull(ADC.numContactID,0),        
  @tintCRMType=isnull(tintCRMType,0),        
 @numTerID=isnull(numTerID,0),        
 @vcCompanyName=isnull(vcCompanyName,'-'),        
        @vcContactName=isnull(vcFirstName,'-')+' '+isnull(vcLastName,''),        
        @vcEmail=isnull(vcEmail,''),        
        @vcPhone=isnull(numPhone,''),
        @vcNoOfEmployeesId=dbo.GetListIemName(Com.numNoOfEmployeesId)  ,
        @numCurrencyID=ISNULL(D.numCurrencyID,0),
        @bitOnCreditHold=ISNULL(D.bitOnCreditHold,0),
        @numBillingDays = ISNULL(D.numBillingDays,0),
        @numDefaultExpenseAccountID = ISNULL(numDefaultExpenseAccountID,0)
 from ProjectsMaster P      
 left join AdditionalContactsInformation ADC      
 on P.numCustPrjMgr=ADC.numContactID        
 join DivisionMaster D        
 on D.numDivisionID=P.numDivisionID        
 join CompanyInfo Com        
        on Com.numCompanyID=D.numCompanyID        
 where numProID=@numProID        
end        
else        
        
if @charModule='S'         
begin        
 select @numDivID=isnull(D.numDivisionID,0),        
 @numCompID=isnull(D.numCompanyID,0),         
 @numContID=isnull(ADC.numContactID,0),        
  @tintCRMType=isnull(tintCRMType,0),        
 @numTerID=isnull(numTerID,0),        
 @vcCompanyName=isnull(vcCompanyName,'-'),        
        @vcContactName=isnull(vcFirstName,'-')+' '+isnull(vcLastName,''),        
        @vcEmail=isnull(vcEmail,''),        
        @vcPhone=isnull(numPhone,''),
        @vcNoOfEmployeesId=dbo.GetListIemName(Com.numNoOfEmployeesId) ,
        @numCurrencyID=ISNULL(D.numCurrencyID,0),
        @bitOnCreditHold=ISNULL(D.bitOnCreditHold,0),
        @numBillingDays = ISNULL(D.numBillingDays,0),
        @numDefaultExpenseAccountID = ISNULL(numDefaultExpenseAccountID,0)      
 from Cases C        
 join AdditionalContactsInformation ADC        
 on ADC.numContactID=C.numContactID        
 join DivisionMaster D        
 on D.numDivisionID=ADC.numDivisionID        
 join CompanyInfo Com        
        on Com.numCompanyID=D.numCompanyID        
 where numCaseID=@numCasesID        
end       
else        
        
if @charModule='D'         
begin        
 select @numDivID=isnull(D.numDivisionID,0),        
 @numCompID=isnull(D.numCompanyID,0),         
 @numContID=isnull(ADC.numContactID,0),        
  @tintCRMType=isnull(tintCRMType,0),        
 @numTerID=isnull(numTerID,0),        
 @vcCompanyName=isnull(vcCompanyName,'-'),        
        @vcContactName=isnull(vcFirstName,'-')+' '+isnull(vcLastName,''),        
        @vcEmail=isnull(vcEmail,''),        
        @vcPhone=isnull(numPhone,''),
        @vcNoOfEmployeesId=dbo.GetListIemName(Com.numNoOfEmployeesId) ,
        @numCurrencyID=ISNULL(D.numCurrencyID,0),
        @bitOnCreditHold=ISNULL(D.bitOnCreditHold,0),
        @numBillingDays = ISNULL(D.numBillingDays,0),
        @numDefaultExpenseAccountID = ISNULL(numDefaultExpenseAccountID,0)       
 from DivisionMaster D        
 join AdditionalContactsInformation ADC        
 on ADC.numDivisionID=D.numDivisionID         
 join CompanyInfo Com        
 on Com.numCompanyID=D.numCompanyID        
 where D.numDivisionID=@numDivID AND ISNULL(ADC.bitPrimaryContact,0)=1      
end       
if @charModule='C'         
begin        
 select @numDivID=isnull(D.numDivisionID,0),        
 @numCompID=isnull(D.numCompanyID,0),         
 @numContID=isnull(ADC.numContactID,0),        
  @tintCRMType=isnull(tintCRMType,0),        
 @numTerID=isnull(numTerID,0),        
 @vcCompanyName=isnull(vcCompanyName,'-'),        
        @vcContactName=isnull(vcFirstName,'-')+' '+isnull(vcLastName,''),        
        @vcEmail=isnull(vcEmail,''),        
        @vcPhone=isnull(numPhone,''),
        @vcNoOfEmployeesId=dbo.GetListIemName(Com.numNoOfEmployeesId)   ,
        @numCurrencyID=ISNULL(D.numCurrencyID,0),
        @bitOnCreditHold=ISNULL(D.bitOnCreditHold,0),
        @numBillingDays = ISNULL(D.numBillingDays,0),
        @numDefaultExpenseAccountID = ISNULL(numDefaultExpenseAccountID,0)  
 from DivisionMaster D        
 join AdditionalContactsInformation ADC        
 on ADC.numDivisionID=D.numDivisionID         
 join CompanyInfo Com        
 on Com.numCompanyID=D.numCompanyID        
 where ADC.numContactID=@numContID      
end
GO
/****** Object:  StoredProcedure [USP_GetCustomerCredits]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCustomerCredits')
DROP PROCEDURE USP_GetCustomerCredits
GO
CREATE PROCEDURE [USP_GetCustomerCredits]                              
(
    @numDomainId numeric(18, 0),
    @numDivisionId numeric(18, 0),
    @numReferenceId NUMERIC(18,0),
    @tintMode TINYINT,
    @numCurrencyID NUMERIC(18,0)=0,
    @numAccountClass NUMERIC(18,0)=0
)            
AS                                           
BEGIN 

    DECLARE @numBaseCurrencyID NUMERIC
	    DECLARE @BaseCurrencySymbol nVARCHAR(3);SET @BaseCurrencySymbol='$'
	    
        SELECT @numBaseCurrencyID = ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId
        SELECT @BaseCurrencySymbol = ISNULL(varCurrSymbol,'$') FROM dbo.Currency WHERE numCurrencyID = @numBaseCurrencyID
    
    
IF @tintMode=1
BEGIN
	
	DECLARE @tintDepositePage AS TINYINT;SET @tintDepositePage=0
	
	IF @numReferenceId>0
	BEGIN
		SELECT @tintDepositePage=tintDepositePage FROM depositMaster WHERE numDepositID=@numReferenceId
	END
	
	PRINT @tintDepositePage
	
	IF @tintDepositePage=3
	BEGIN
		SELECT tintDepositePage AS tintRefType,
		CASE WHEN tintDepositePage=3 THEN 'Credit Memo #' + CAST(numReturnHeaderID AS VARCHAR(20)) ELSE 'Unapplied Payment' END AS vcReference,
		numDepositId AS numReferenceID,CAST(monDepositAmount AS DECIMAL(19,2)) AS monAmount,
		ISNULL(CAST(monDepositAmount AS DECIMAL(19,2)),0) - ISNULL(CAST(monRefundAmount AS DECIMAL(19,2)),0) AS monNonAppliedAmount,
		ISNULL(numReturnHeaderID,0) AS numReturnHeaderID,CAST (1 AS BIT) AS bitIsPaid,ISNULL(CAST(monAppliedAmount AS DECIMAL(19,2)),0) AS monAmountPaid,
		DM.numCurrencyID,ISNULL(C.varCurrSymbol,'') varCurrSymbol,ISNULL(NULLIF(DM.fltExchangeRate,0),1) fltExchangeRateReceivedPayment
		,@BaseCurrencySymbol BaseCurrencySymbol,CASE WHEN tintDepositePage=3 THEN '' ELSE ISNULL(DM.vcReference,'') END  AS vcDepositReference 
		FROM DepositMaster DM LEFT JOIN dbo.Currency C ON C.numCurrencyID = DM.numCurrencyID
		WHERE DM.numDomainId=@numDomainId AND numDivisionID=@numDivisionID AND tintDepositePage IN(3)
		AND numDepositId=@numReferenceId AND (ISNULL(DM.numCurrencyID,@numBaseCurrencyID) = @numCurrencyID OR @numCurrencyID =0)
		AND (ISNULL(DM.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
		 
		UNION
		
		SELECT tintDepositePage AS tintRefType,CASE WHEN tintDepositePage=3 THEN 'Credit Memo #' + CAST(numReturnHeaderID AS VARCHAR(20)) ELSE 'Unapplied Payment' END AS vcReference,
		numDepositId AS numReferenceID,CAST(monDepositAmount AS DECIMAL(19,2)) AS monAmount,
		ISNULL(CAST(monDepositAmount AS DECIMAL(19,2)),0) - ISNULL(CAST(monAppliedAmount AS DECIMAL(19,2)),0) - ISNULL(CAST(monRefundAmount AS DECIMAL(19,2)),0) AS monNonAppliedAmount,
		ISNULL(numReturnHeaderID,0) AS numReturnHeaderID,CAST (0 AS BIT) AS bitIsPaid,0 AS monAmountPaid,DM.numCurrencyID,ISNULL(C.varCurrSymbol,'') varCurrSymbol,
		ISNULL(NULLIF(DM.fltExchangeRate,0),1) fltExchangeRateReceivedPayment,@BaseCurrencySymbol BaseCurrencySymbol,
		CASE WHEN tintDepositePage=3 THEN '' ELSE ISNULL(DM.vcReference,'') END AS vcDepositReference  
		FROM DepositMaster DM LEFT JOIN dbo.Currency C ON C.numCurrencyID = DM.numCurrencyID
		WHERE DM.numDomainId=@numDomainId AND numDivisionID=@numDivisionID AND tintDepositePage IN(2,3)
		AND (ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) > 0 AND numDepositId!=@numReferenceId
		AND (ISNULL(DM.numCurrencyID,@numBaseCurrencyID) = @numCurrencyID OR @numCurrencyID =0)
		AND (ISNULL(DM.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
	END
	ELSE
	BEGIN
		SELECT tintDepositePage AS tintRefType,CASE WHEN tintDepositePage=3 THEN 'Credit Memo #' + CAST(numReturnHeaderID AS VARCHAR(20)) ELSE 'Unapplied Payment' END AS vcReference,
		numDepositId AS numReferenceID,CAST(monDepositAmount AS DECIMAL(19,2)) AS monAmount,
		ISNULL(CAST(monDepositAmount AS DECIMAL(19,2)),0) - ISNULL(CAST(monAppliedAmount AS DECIMAL(19,2)),0) - ISNULL(CAST(monRefundAmount AS DECIMAL(19,2)),0) AS monNonAppliedAmount,
		ISNULL(numReturnHeaderID,0) AS numReturnHeaderID,CAST (0 AS BIT) AS bitIsPaid,0 AS monAmountPaid,DM.numCurrencyID,ISNULL(C.varCurrSymbol,'') varCurrSymbol
		,ISNULL(NULLIF(DM.fltExchangeRate,0),1) fltExchangeRateReceivedPayment,@BaseCurrencySymbol BaseCurrencySymbol,CASE WHEN tintDepositePage=3 THEN '' ELSE ISNULL(DM.vcReference,'') END  AS vcDepositReference 
		FROM DepositMaster DM LEFT JOIN dbo.Currency C ON C.numCurrencyID = DM.numCurrencyID
		WHERE DM.numDomainId=@numDomainId AND numDivisionID=@numDivisionID AND tintDepositePage IN(2,3)
		AND (ISNULL(CAST(monDepositAmount AS DECIMAL(19,2)),0) - ISNULL(CAST(monAppliedAmount AS DECIMAL(19,2)),0)) > 0 AND numDepositId!=@numReferenceId
		AND (ISNULL(DM.numCurrencyID,@numBaseCurrencyID) = @numCurrencyID OR @numCurrencyID =0)
		AND (ISNULL(DM.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
		ORDER BY numDepositId DESC
	END		
	
--UNION 
--
--SELECT 2 AS tintRefType,numReturnHeaderID AS numReferenceID,monAmount,monAmountUsed AS monAppliedAmount FROM ReturnHeader WHERE numDomainId=@numDomainId AND numDivisionID=@numDivisionID AND tintReturnType IN (1,4)
END

ELSE IF @tintMode=2
BEGIN

	DECLARE @numReturnHeaderID AS NUMERIC(18,0);SET @numReturnHeaderID=0
	
	IF @numReferenceId>0
	BEGIN
		SELECT @numReturnHeaderID=numReturnHeaderID FROM BillPaymentHeader WHERE numBillPaymentID=@numReferenceId
	END
	
	IF @numReturnHeaderID>0
	BEGIN
		SELECT 1 AS tintRefType,RH.vcRMA AS vcReference
		,BPH.numBillPaymentID AS numReferenceID,monPaymentAmount AS monAmount,ISNULL(monPaymentAmount,0) - ISNULL(monRefundAmount,0) AS monNonAppliedAmount,CAST (1 AS BIT) AS bitIsPaid,ISNULL(monAppliedAmount,0) AS monAmountPaid,ISNULL(BPH.numReturnHeaderID,0) AS numReturnHeaderID,
		BPH.numCurrencyID
						,ISNULL(C.varCurrSymbol,'') varCurrSymbol
						,ISNULL(NULLIF(BPH.fltExchangeRate,0),1) fltExchangeRateReceivedPayment
						,@BaseCurrencySymbol BaseCurrencySymbol
		FROM BillPaymentHeader BPH LEFT JOIN dbo.Currency C ON C.numCurrencyID = BPH.numCurrencyID 
		LEFT JOIN ReturnHeader RH ON RH.numReturnHeaderID=ISNULL(BPH.numReturnHeaderID,0)
		WHERE BPH.numDomainId=@numDomainId AND BPH.numDivisionID=@numDivisionID 
		 AND numBillPaymentID=@numReferenceId AND (ISNULL(BPH.numCurrencyID,@numBaseCurrencyID) = @numCurrencyID OR @numCurrencyID =0)
		 AND (ISNULL(BPH.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
		 
		UNION
		
		SELECT CASE WHEN ISNULL(BPH.numReturnHeaderID,0)>0 THEN 1 ELSE 0 END AS tintRefType,
			  CASE WHEN ISNULL(BPH.numReturnHeaderID,0)>0 THEN RH.vcRMA ELSE 'Unapplied Bill Payment' END AS vcReference
		,BPH.numBillPaymentID AS numReferenceID,monPaymentAmount AS monAmount,ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0) - ISNULL(monRefundAmount,0) AS monNonAppliedAmount,CAST (0 AS BIT) AS bitIsPaid,0 AS monAmountPaid,ISNULL(BPH.numReturnHeaderID,0) AS numReturnHeaderID,
		BPH.numCurrencyID
						,ISNULL(C.varCurrSymbol,'') varCurrSymbol
						,ISNULL(NULLIF(BPH.fltExchangeRate,0),1) fltExchangeRateReceivedPayment
						,@BaseCurrencySymbol BaseCurrencySymbol
		FROM BillPaymentHeader BPH LEFT JOIN dbo.Currency C ON C.numCurrencyID = BPH.numCurrencyID 
		LEFT JOIN ReturnHeader RH ON RH.numReturnHeaderID=ISNULL(BPH.numReturnHeaderID,0)
		WHERE BPH.numDomainId=@numDomainId AND BPH.numDivisionID=@numDivisionID 
		AND (ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0)) > 0 AND numBillPaymentID!=@numReferenceId
		AND (ISNULL(BPH.numCurrencyID,@numBaseCurrencyID) = @numCurrencyID OR @numCurrencyID =0)
		AND (ISNULL(BPH.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
		
	END
	ELSE
	BEGIN
		SELECT CASE WHEN ISNULL(BPH.numReturnHeaderID,0)>0 THEN 1 ELSE 0 END AS tintRefType,
			  CASE WHEN ISNULL(BPH.numReturnHeaderID,0)>0 THEN RH.vcRMA ELSE 'Unapplied Bill Payment' END AS vcReference
		,BPH.numBillPaymentID AS numReferenceID,monPaymentAmount AS monAmount,ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0) - ISNULL(monRefundAmount,0) AS monNonAppliedAmount,CAST (0 AS BIT) AS bitIsPaid,0 AS monAmountPaid  ,ISNULL(BPH.numReturnHeaderID,0) AS numReturnHeaderID,
		BPH.numCurrencyID
						,ISNULL(C.varCurrSymbol,'') varCurrSymbol
						,ISNULL(NULLIF(BPH.fltExchangeRate,0),1) fltExchangeRateReceivedPayment
						,@BaseCurrencySymbol BaseCurrencySymbol
		FROM BillPaymentHeader BPH LEFT JOIN dbo.Currency C ON C.numCurrencyID = BPH.numCurrencyID 
		LEFT JOIN ReturnHeader RH ON RH.numReturnHeaderID=ISNULL(BPH.numReturnHeaderID,0)
		WHERE BPH.numDomainId=@numDomainId AND BPH.numDivisionID=@numDivisionID 
		AND (ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0)) > 0 AND numBillPaymentID!=@numReferenceId
		AND (ISNULL(BPH.numCurrencyID,@numBaseCurrencyID) = @numCurrencyID OR @numCurrencyID =0)
		AND (ISNULL(BPH.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
	END		
	
--	SELECT tintReturnType AS tintRefType,'Purchase Return Credit' AS vcReference,numReturnHeaderID AS numReferenceID,monBizDocAmount AS monAmount, ISNULL(monBizDocAmount,0) - ISNULL(monBizDocUsedAmount,0) AS monNonAppliedAmount,CAST (0 AS BIT) AS bitIsPaid,0 AS monAmountPaid  
--		FROM ReturnHeader WHERE numDomainId=@numDomainId AND numDivisionID=@numDivisionID
--		AND numReturnHeaderID NOT IN (SELECT numReturnHeaderID FROM ReturnPaymentHistory WHERE numReferenceID=@numReferenceId AND tintReferenceType=2)
--		AND (ISNULL(monBizDocAmount,0) - ISNULL(monBizDocUsedAmount,0)) >0 AND tintReturnType=2 AND tintReceiveType=2
--		
--	UNION
--	
--	SELECT tintReturnType AS tintRefType,'Purchase Return Credit' AS vcReference,RH.numReturnHeaderID AS numReferenceID,RH.monBizDocAmount, ISNULL(RH.monBizDocAmount,0) - ISNULL(RH.monBizDocUsedAmount,0) + ISNULL(SUM(ISNULL(RPH.monAmount,0)),0) AS monNonAppliedAmount,CAST (1 AS BIT) AS bitIsPaid,ISNULL(SUM(ISNULL(RPH.monAmount,0)),0) AS monAmountPaid  
--		FROM ReturnHeader RH JOIN ReturnPaymentHistory RPH ON RH.numReturnHeaderID=RPH.numReturnHeaderID  
--		WHERE RH.numDomainId=@numDomainId AND RH.numDivisionID=@numDivisionID 
--		AND RPH.numReferenceID=@numReferenceId AND RPH.tintReferenceType=2 AND tintReturnType=2 AND tintReceiveType=2
--		GROUP BY RH.numReturnHeaderID,RH.monBizDocAmount,RH.monBizDocUsedAmount,RH.tintReturnType
--	
	
END
  
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetDealsList1]    Script Date: 05/07/2009 22:12:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj

--exec USP_GetDealsList1 
--@numUserCntID=85098,
--@numDomainID=110,
--@tintSalesUserRightType=3,
--@tintPurchaseUserRightType=3,
--@tintSortOrder=1,
--@SortChar=0,
--@FirstName='',
--@LastName='',
--@CustName='',
--@CurrentPage=1,
--@PageSize=50,
--@TotRecs=0,
--@columnName='vcCompanyName',
--@columnSortOrder='Asc',
--@numCompanyID=0,
--@bitPartner=0,
--@ClientTimeZoneOffset=-180,
--@tintShipped=0,
--@intOrder=0,
--@intType=1,
--@tintFilterBy=0


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdealslist1')
DROP PROCEDURE usp_getdealslist1
GO
CREATE PROCEDURE [dbo].[USP_GetDealsList1]
               @numUserCntID              NUMERIC(9)  = 0,
               @numDomainID               NUMERIC(9)  = 0,
               @tintSalesUserRightType    TINYINT  = 0,
               @tintPurchaseUserRightType TINYINT  = 0,
               @tintSortOrder             TINYINT  = 4,
               @SortChar                  CHAR(1)  = '0',
               @FirstName                 VARCHAR(100)  = '',
               @LastName                  VARCHAR(100)  = '',
               @CustName                  VARCHAR(100)  = '',
               @CurrentPage               INT,
               @PageSize                  INT,
               @TotRecs                   INT  OUTPUT,
               @columnName                AS VARCHAR(50),
               @columnSortOrder           AS VARCHAR(10),
               @numCompanyID              AS NUMERIC(9)  = 0,
               @bitPartner                AS BIT  = 0,
               @ClientTimeZoneOffset      AS INT,
               @tintShipped               AS TINYINT,
               @intOrder                  AS TINYINT,
               @intType                   AS TINYINT,
               @tintFilterBy              AS TINYINT  = 0,
               @numOrderStatus			  AS NUMERIC,
			   @vcRegularSearchCriteria varchar(1000)='',
			   @vcCustomSearchCriteria varchar(1000)='' 
AS
  DECLARE  @PageId  AS TINYINT
  DECLARE  @numFormId  AS INT 
  
  SET @PageId = 0
  IF @inttype = 1
  BEGIN
  	SET @PageId = 2
  	SET @inttype = 3
  	SET @numFormId=39
  END
  IF @inttype = 2
  BEGIN
  	SET @PageId = 6
    SET @inttype = 4
    SET @numFormId=41
  END
  --Create a Temporary table to hold data
  CREATE TABLE #tempTable (
    ID              INT   IDENTITY   PRIMARY KEY,
    numOppId        NUMERIC(9),intTotalProgress int)
    
 DECLARE @strShareRedordWith AS VARCHAR(300);
 SET @strShareRedordWith=' Opp.numOppId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=3 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '

  DECLARE  @strSql  AS VARCHAR(8000)
  SET @strSql = 'SELECT  Opp.numOppId,PP.intTotalProgress '
  
  DECLARE  @strOrderColumn  AS VARCHAR(100);SET @strOrderColumn=@columnName
--  
--   IF LEN(@columnName)>0
--   BEGIN
--	 IF CHARINDEX(@columnName,@strSql)=0
--	 BEGIN
--		SET @strSql= @strSql + ',' + @columnName
--	 	SET @strOrderColumn=@columnName
--	 END
--   END
   
  SET @strSql = @strSql + ' FROM OpportunityMaster Opp                                                               
                                 INNER JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId                                                               
                                 INNER JOIN DivisionMaster Div ON Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID
								 INNER JOIN CompanyInfo cmp ON Div.numCompanyID = cmp.numCompanyId 
								 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId '
                                 --left JOIN OpportunityLinking OL on OL.numChildOppID=Opp.numOppId '
								 
  IF @bitPartner = 1
    SET @strSql = @strSql + ' left join OpportunityContact OppCont on OppCont.numOppId=Opp.numOppId and OppCont.bitPartner=1 and OppCont.numContactId=' + CONVERT(VARCHAR(15),@numUserCntID)
                    
   
   if @columnName like 'CFW.Cust%'             
	begin            
		SET @strSql = @strSql + ' left Join CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid  and CFW.fld_id= '+replace(@columnName,'CFW.Cust','') 
		SET @strOrderColumn = 'CFW.Fld_Value'   
	end                                     
  ELSE if @columnName like 'DCust%'            
	begin            
	 SET @strSql = @strSql + ' left Join CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid   and CFW.fld_id= '+replace(@columnName,'DCust','')                                                                                               
	 SET @strSql = @strSql + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '            
	 SET @strOrderColumn = 'LstCF.vcData'  
	end       

             
  IF @tintFilterBy = 1 --Partially Fulfilled Orders 
    SET @strSql = @strSql + ' left join AdditionalContactsInformation ADC1 on ADC1.numContactId=Opp.numRecOwner                                                               
                             LEFT OUTER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = Opp.[numOppId] 
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and Opp.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped)
  ELSE
    SET @strSql = @strSql + ' left join AdditionalContactsInformation ADC1 on ADC1.numContactId=Opp.numRecOwner                                                               
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and Opp.tintShipped='+ CONVERT(VARCHAR(15),@tintShipped) 
            
                    
  IF @FirstName <> ''
    SET @strSql = @strSql + ' and ADC.vcFirstName  like ''' + @FirstName + '%'''
    
  IF @LastName <> '' 
    SET @strSql = @strSql + ' and ADC.vcLastName like ''' + @LastName + '%'''
  
  IF @CustName <> ''
    SET @strSql = @strSql + ' and cmp.vcCompanyName like ''' + @CustName + '%'''
  
  
  IF @intOrder <> 0
    BEGIN
      IF @intOrder = 1
        SET @strSql = @strSql + ' and Opp.bitOrder = 0 '
      IF @intOrder = 2
        SET @strSql = @strSql + ' and Opp.bitOrder = 1'
    END
    
  IF @numCompanyID <> 0
    SET @strSql = @strSql + ' And Div.numCompanyID ='+ CONVERT(VARCHAR(15),@numCompanyID)
    
  IF @SortChar <> '0'
    SET @strSql = @strSql + ' And Opp.vcPOppName like ''' + @SortChar + '%'''
  
  
  IF @tintSalesUserRightType = 0 OR @tintPurchaseUserRightType = 0
    SET @strSql = @strSql + ' AND opp.tintOppType =0'
  ELSE IF @tintSalesUserRightType = 1 OR @tintPurchaseUserRightType = 1
    SET @strSql = @strSql + ' AND (Opp.numRecOwner= ' + CONVERT(VARCHAR(15),@numUserCntID)
                    + ' or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
                    --+ ' or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where  numAssignTo ='+ CONVERT(VARCHAR(15),@numUserCntID)+ ')'
                    + CASE WHEN @bitPartner = 1 THEN ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+ CONVERT(VARCHAR(15),@numUserCntID) + ')' ELSE '' END
                    + ' or ' + @strShareRedordWith +')'
  ELSE IF @tintSalesUserRightType = 2 OR @tintPurchaseUserRightType = 2
      SET @strSql = @strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) 
								or div.numTerID=0 or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
								 --+ ' or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo ='+ CONVERT(VARCHAR(15),@numUserCntID)+ ')' 
								+ ' or ' + @strShareRedordWith +')'
	
								
  IF @tintSortOrder <> '0'
    SET @strSql = @strSql + '  AND Opp.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder)
    
    
 IF @tintFilterBy = 1 --Partially Fulfilled Orders
    SET @strSql = @strSql + ' AND OB.bitPartialFulfilment = 1 AND OB.bitAuthoritativeBizDocs = 1 '
 ELSE IF @tintFilterBy = 2 --Fulfilled Orders
    SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT OM.numOppId FROM [OpportunityMaster] OM
                      			INNER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = OM.[numOppId]
                      			INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId]
                      			INNER JOIN [OpportunityBizDocItems] BI ON BI.[numOppBizDocID] = OB.[numOppBizDocsId]
                      			Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + 'AND OB.[bitAuthoritativeBizDocs] = 1 GROUP BY OM.[numOppId]
                      			HAVING   COUNT(OI.[numUnitHour]) = COUNT(BI.[numUnitHour])) '
 ELSE IF @tintFilterBy = 3 --Orders without Authoritative-BizDocs
    SET @strSql = @strSql + ' AND Opp.[numOppId] not IN (SELECT DISTINCT OM.[numOppId]
                      		  FROM [OpportunityMaster] OM JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		  Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OM.tintOppstatus=1 AND OM.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped) + ' AND OM.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder) + ' AND isnull(OB.[bitAuthoritativeBizDocs],0)=1) '
--ELSE IF @tintFilterBy = 4 --Orders with child records
--    SET @strSql = @strSql
--                    + ' AND Opp.[numOppId] IN (SELECT numParentOppID FROM dbo.OpportunityLinking OL INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OL.numParentOppID WHERE OM.numDomainId='+ CONVERT(VARCHAR(15),@numDomainID) + ' )  '

 ELSE IF @tintFilterBy = 4 --Orders with items yet to be added to Invoices
    SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT distinct OPPM.numOppId FROM [OpportunityMaster] OPPM
                                  INNER JOIN [OpportunityItems] OPPI ON OPPM.[numOppId] = OPPI.[numOppId]
                                  Where OPPM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' 
								  and OPPI.numoppitemtCode not in (select numOppItemID from [OpportunityBizDocs] OB INNER JOIN [OpportunityBizDocItems] BI
                      			  ON BI.[numOppBizDocID] = OB.[numOppBizDocsId] where OB.[numOppId] = OPPM.[numOppId]))'      
 ELSE IF @tintFilterBy = 6 --Orders with deferred Income/Invoices
    SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT DISTINCT OM.[numOppId]
                      		 FROM [OpportunityMaster] OM LEFT OUTER JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		 Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OB.[bitAuthoritativeBizDocs] =1 and OB.tintDeferred=1)'

IF @numOrderStatus <>0 
	SET @strSql = @strSql + ' AND Opp.numStatus = ' + CONVERT(VARCHAR(15),@numOrderStatus);


IF @vcRegularSearchCriteria<>'' 
	set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
	
IF @vcCustomSearchCriteria<>'' 
	set @strSql=@strSql+' and Opp.numOppid in (select distinct CFW.RecId from CFW_Fld_Values_Opp CFW where ' + @vcCustomSearchCriteria + ')'

 --SET @strSql = 'SELECT numOppId from (' + @strSql + ') a'

 IF LEN(@strOrderColumn)>0
	IF @strOrderColumn = 'OI.vcInventoryStatus'
		BEGIN
			SET @strSql =  @strSql + ' ORDER BY  RTRIM(LTRIM(REPLACE(SUBSTRING(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),CHARINDEX(''>'',dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) + 1, CHARINDEX(''<font>'',dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID))),''<font>'','''')))'  + ' ' + @columnSortOrder    
		END
	ELSE
		BEGIN
			SET @strSql =  @strSql + ' ORDER BY  ' + @strOrderColumn + ' ' + @columnSortOrder    
		END

	
  PRINT @strSql
  INSERT INTO #tempTable (numOppId,intTotalProgress) EXEC( @strSql)
  
  DECLARE  @firstRec  AS INTEGER
  DECLARE  @lastRec  AS INTEGER
  SET @firstRec = (@CurrentPage - 1) * @PageSize
  SET @lastRec = (@CurrentPage * @PageSize + 1)
  
  SET @TotRecs = (SELECT COUNT(* ) FROM #tempTable)
  
  DECLARE  @tintOrder  AS TINYINT;SET @tintOrder = 0
  DECLARE  @vcFieldName  AS VARCHAR(50)
  DECLARE  @vcListItemType  AS VARCHAR(3)
  DECLARE  @vcListItemType1  AS VARCHAR(1)
  DECLARE  @vcAssociatedControlType VARCHAR(30)
  DECLARE  @numListID  AS NUMERIC(9)
  DECLARE  @vcDbColumnName VARCHAR(40)
  DECLARE  @WhereCondition VARCHAR(2000);SET @WhereCondition = ''
  DECLARE  @vcLookBackTableName VARCHAR(2000)
  DECLARE  @bitCustom  AS BIT
  Declare @numFieldId as numeric  
  DECLARE @bitAllowSorting AS CHAR(1)   
  DECLARE @bitAllowEdit AS CHAR(1)                 
  DECLARE @vcColumnName AS VARCHAR(500)                  
  DECLARE  @Nocolumns  AS TINYINT;SET @Nocolumns = 0

  SELECT @Nocolumns=isnull(sum(TotalRow),0) from(            
	SELECT count(*) TotalRow from View_DynamicColumns where numFormId=@numFormId and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType = @numFormId
	UNION 
	SELECT count(*) TotalRow from View_DynamicCustomColumns where numFormId=@numFormId and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType = @numFormId) TotalRows


  CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1))
    
  SET @strSql = ''
  SET @strSql = 'select ADC.numContactId,DM.numDivisionID,isnull(DM.numTerID,0) numTerID,opp.numRecOwner as numRecOwner,DM.tintCRMType,opp.numOppId'

if @Nocolumns=0
BEGIN
INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
select @numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
 FROM View_DynamicDefaultColumns
 where numFormId=@numFormId and bitDefault=1 AND ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID
order by tintOrder asc   

END

    
    INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
 FROM View_DynamicColumns 
 where numFormId=@numFormId and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0  AND numRelCntType = @numFormId
 
 UNION
    
     select tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,0,'' 
 from View_DynamicCustomColumns
 where numFormId=@numFormId and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
 AND ISNULL(bitCustom,0)=1  AND numRelCntType = @numFormId
 
  order by tintOrder asc  



--  SET @DefaultNocolumns = @Nocolumns
Declare @ListRelID as numeric(9) 

  select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
@ListRelID=ListRelID
  from  #tempForm order by tintOrder asc            

  WHILE @tintOrder > 0
    BEGIN
      IF @bitCustom = 0
        BEGIN
          DECLARE  @Prefix  AS VARCHAR(5)
          IF @vcLookBackTableName = 'AdditionalContactsInformation'
            SET @Prefix = 'ADC.'
          IF @vcLookBackTableName = 'DivisionMaster'
            SET @Prefix = 'DM.'
          IF @vcLookBackTableName = 'OpportunityMaster'
            SET @PreFix = 'Opp.'
          IF @vcLookBackTableName = 'OpportunityRecurring'
            SET @PreFix = 'OPR.'
            
		 SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'
            
          IF @vcAssociatedControlType = 'SelectBox'
            BEGIN
            IF @vcDbColumnName = 'tintSource'
            BEGIN
              SET @strSql = @strSql + ',ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'')' + ' [' + @vcColumnName + ']'
            END
            
            ELSE IF @vcDbColumnName = 'numCampainID'
            BEGIN
              SET @strSql = @strSql + ', (SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID) ' + ' [' + @vcColumnName + ']'
            END
            
            ELSE IF @vcListItemType = 'LI'
            BEGIN
                  SET @strSql = @strSql + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
                  SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
            END
            ELSE IF @vcListItemType = 'S'
            BEGIN
                    SET @strSql = @strSql + ',S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState' + ' [' + @vcColumnName + ']'
                    SET @WhereCondition = @WhereCondition + ' left join State S' + CONVERT(VARCHAR(3),@tintOrder) + ' on S' + CONVERT(VARCHAR(3),@tintOrder) + '.numStateID=' + @vcDbColumnName
            END
            ELSE IF @vcListItemType = 'BP'
            BEGIN
                    SET @strSql = @strSql + ',SPLM.Slp_Name' + ' [' + @vcColumnName + ']'
                    SET @WhereCondition = @WhereCondition + ' LEFT JOIN Sales_process_List_Master SPLM on SPLM.Slp_Id = OPP.numBusinessProcessID '
                  END
            ELSE IF @vcListItemType = 'PP'
            BEGIN
                              SET @strSql = @strSql + ',ISNULL(T.intTotalProgress,0)' + ' [' + @vcColumnName + ']'

--                SET @strSql = @strSql 
--                                + ',ISNULL(PP.intTotalProgress,0)'
--                                + ' [' +
--                                @vcColumnName + ']'
--                SET @WhereCondition = @WhereCondition
--                                        + ' LEFT JOIN ProjectProgress PP ON PP.numDomainID='+ convert(varchar(15),@numDomainID )+' and OPP.numOppID = PP.numOppID '
            END
            ELSE IF @vcListItemType = 'T'
                    BEGIN
                      SET @strSql = @strSql + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
                      SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
                    END
             ELSE IF @vcListItemType = 'U'
                      BEGIN
                        SET @strSql = @strSql + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
                      END
              ELSE IF @vcListItemType = 'WI'
				BEGIN
					SET @strSql = @strSql + ', Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end [' + @vcColumnName + ']'
					set @WhereCondition= @WhereCondition +' left JOIN OpportunityLinking OL on OL.numChildOppID=Opp.numOppId left join WebAPI on WebAPI.WebApiId=isnull(OL.numWebApiId,0)
													left join Sites on isnull(OL.numSiteID,0)=Sites.numSIteID and Sites.numDomainID=' + convert(varchar(15),@numDomainID)                                                       
				END 
            END
          ELSE
            IF @vcAssociatedControlType = 'DateField'
              BEGIN
              if @Prefix ='OPR.'
				 BEGIN
				  	SET @strSql = @strSql + ', (SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'+convert(varchar(15),-@ClientTimeZoneOffset)+', dtRecurringDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) AS  ['+ @vcColumnName + ']'
				 END
				 else
				 BEGIN
					SET @strSql = @strSql
                                + ',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
                                + @Prefix
                                + @vcDbColumnName
                                + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''
					SET @strSql = @strSql
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''
					SET @strSql = @strSql
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strSql = @strSql
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']' 	
				 END
                
                                
					
						
              END
            ELSE
              IF @vcAssociatedControlType = 'TextBox'
                BEGIN
                  SET @strSql = @strSql
                                  + ','
                                  + CASE 
                                      WHEN @vcDbColumnName = 'numAge' THEN 'year(getutcdate())-year(bintDOB)'
                                      WHEN @vcDbColumnName = 'monPAmount' THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(MONEY,monPAmount),1)'
                                      WHEN @vcDbColumnName = 'numDivisionID' THEN 'DM.numDivisionID'
                                      WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
                                      WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
                                      WHEN @vcDbColumnName = 'vcPOppName' THEN 'Opp.vcPOppName'
                                      ELSE @vcDbColumnName
                                    END
                                  + ' ['
                                  + @vcColumnName + ']'
                END
              ELSE
                IF @vcAssociatedControlType = 'TextArea'
                  BEGIN
						set @strSql=@strSql+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']'  
                  END
                   else if  @vcAssociatedControlType='Label'                                              
begin  
 set @strSql=@strSql+','+ case                    
	WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'--'[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
	WHEN @vcDbColumnName = 'vcInventoryStatus' THEN 'CASE WHEN opp.tintOppType=1 THEN dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID) ELSE '''' END'
    WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
    WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
    WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
	FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId FOR XML PATH('''')),4,200000)),'''')'
    else @vcDbColumnName END +' ['+ @vcColumnName+']'            
 END

                   	else if @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
		begin      
			 set @strSql=@strSql+',(SELECT SUBSTRING(
	(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
				AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
		 end 
        END
      ELSE
        BEGIN
        
            SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

          IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
            BEGIN
              SET @strSql = @strSql
                              + ',CFW'
                              + CONVERT(VARCHAR(3),@tintOrder)
                              + '.Fld_Value  ['
                              + @vcColumnName + ']'
              SET @WhereCondition = @WhereCondition
                                      + ' left Join CFW_FLD_Values_Opp CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '               
                                                                                                                            on CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.Fld_Id='
                                      + CONVERT(VARCHAR(10),@numFieldId)
                                      + 'and CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.RecId=Opp.numOppid   '
            END
          ELSE
            IF @vcAssociatedControlType = 'CheckBox' 
              BEGIN
                SET @strSql = @strSql
                                + ',case when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=0 then ''No'' when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=1 then ''Yes'' end   ['
                                + @vcColumnName + ']'
                SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '           
                                                                                                                                  on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid   '
              END
            ELSE
              IF @vcAssociatedControlType = 'DateField'
                BEGIN
                  SET @strSql = @strSql
                                  + ',dbo.FormatedDateFromDate(CFW'
                                  + CONVERT(VARCHAR(3),@tintOrder)
                                  + '.Fld_Value,'
                                  + CONVERT(VARCHAR(10),@numDomainId)
                                  + ')  ['
                                  + @vcColumnName + ']'
                  SET @WhereCondition = @WhereCondition
                                          + ' left Join CFW_FLD_Values_Opp CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '               
                                                                                                                                        on CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.Fld_Id='
                                          + CONVERT(VARCHAR(10),@numFieldId)
                                          + 'and CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.RecId=Opp.numOppid    '
                END
              ELSE
                IF @vcAssociatedControlType = 'SelectBox'
                  BEGIN
                    SET @vcDbColumnName = 'DCust'
                                            + CONVERT(VARCHAR(10),@numFieldId)
                    SET @strSql = @strSql
                                    + ',L'
                                    + CONVERT(VARCHAR(3),@tintOrder)
                                    + '.vcData'
                                    + ' ['
                                    + @vcColumnName + ']'
                    SET @WhereCondition = @WhereCondition
                                            + ' left Join CFW_FLD_Values_Opp CFW'
                                            + CONVERT(VARCHAR(3),@tintOrder)
                                            + '               
                                                                                                                                               on CFW'
                                            + CONVERT(VARCHAR(3),@tintOrder)
                                            + '.Fld_Id='
                                            + CONVERT(VARCHAR(10),@numFieldId)
                                            + 'and CFW'
                                            + CONVERT(VARCHAR(3),@tintOrder)
                                            + '.RecId=Opp.numOppid     '
                    SET @WhereCondition = @WhereCondition
                                            + ' left Join ListDetails L'
                                            + CONVERT(VARCHAR(3),@tintOrder)
                                            + ' on L'
                                            + CONVERT(VARCHAR(3),@tintOrder)
                                            + '.numListItemID=CFW'
                                            + CONVERT(VARCHAR(3),@tintOrder)
                                            + '.Fld_Value'
                  END
        END
      
     
       select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
@ListRelID=ListRelID
  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc            
 
  if @@rowcount=0 set @tintOrder=0 
 END 
  
  -------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

--select top 1 @vcCSOrigDbCOlumnName=DFM.vcOrigDbCOlumnName,@vcCSLookBackTableName=DFM.vcLookBackTableName from DycFieldColorScheme DFCS join DycFieldMaster DFM on 
--DFCS.numModuleID=DFM.numModuleID and DFCS.numFieldID=DFM.numFieldID
--where DFCS.numDomainID=@numDomainID and DFM.numModuleID=1

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=@numFormId AND DFCS.numFormID=@numFormId and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
----------------------------                       

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
set @strSql=@strSql+',tCS.vcColorScheme'
END

  
  SET @strSql = @strSql + ' FROM OpportunityMaster Opp                                                               
                            INNER JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId                                                               
							INNER JOIN DivisionMaster DM ON Opp.numDivisionId = DM.numDivisionID AND ADC.numDivisionId = DM.numDivisionID                                                               
                            INNER JOIN CompanyInfo cmp ON DM.numCompanyID = cmp.numCompanyId '
  
  IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
	 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
		set @Prefix = 'ADC.'                  
     if @vcCSLookBackTableName = 'DivisionMaster'                  
		set @Prefix = 'DM.'
	 if @vcCSLookBackTableName = 'CompanyInfo'                  
		set @Prefix = 'CMP.'   
	 if @vcCSLookBackTableName = 'OpportunityMaster'                  
		set @Prefix = 'Opp.'   
 
	IF @vcCSAssociatedControlType='DateField'
	BEGIN
			set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
			 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
	END
	ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
	BEGIN
		set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
	END
END
                          
if @columnName like 'CFW.Cust%'             
begin            
 SET @strSql = @strSql + ' left Join CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid and CFW.fld_id= '+replace(@columnName,'CFW.Cust','') +' '                                                     
 set @columnName='CFW.Fld_Value'            
end 

SET @strSql = @strSql   + @WhereCondition + ' join (select ID,numOppId,intTotalProgress FROM  #tempTable )T on T.numOppId=Opp.numOppId                                              
                          WHERE ID > ' + CONVERT(VARCHAR(10),@firstRec) + ' and ID <' + CONVERT(VARCHAR(10),@lastRec)

IF (@tintFilterBy = 5) --Sort alphabetically
   SET @strSql = @strSql +' order by ID '
ELSE IF @columnName = 'OI.vcInventoryStatus'  
   SET @strSql =  @strSql + ' ORDER BY  RTRIM(LTRIM(REPLACE(SUBSTRING(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),CHARINDEX(''>'',dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) + 1, CHARINDEX(''<font>'',dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID))),''<font>'','''')))'  + ' ' + @columnSortOrder     
else  IF (@columnName = 'numAssignedBy' OR @columnName = 'numAssignedTo')
   SET @strSql = @strSql + ' ORDER BY  opp.' +  @columnName + ' ' + @columnSortOrder 
else  IF (@columnName = 'PP.intTotalProgress')
   SET @strSql = @strSql + ' ORDER BY  T.intTotalProgress ' + @columnSortOrder 
ELSE IF @columnName <> 'opp.bintCreatedDate' and @columnName <> 'vcPOppName' and @columnName <> 'bintCreatedDate' and @columnName <> 'vcCompanyName'
   SET @strSql = @strSql + ' ORDER BY  ' +  @columnName + ' '+ @columnSortOrder   
ELSE 
   SET @strSql = @strSql +' order by ID '
		                                             
print @strSql                      
              
exec (@strSql)  

SELECT * FROM #tempForm

DROP TABLE #tempForm

drop table #tempColorScheme

DROP TABLE #tempTable
--created by anoop jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdomaindetails')
DROP PROCEDURE usp_getdomaindetails
GO
CREATE PROCEDURE [dbo].[USP_GetDomainDetails]                                          
@numDomainID as numeric(9)=0                                          
as                                          
select                                           
vcDomainName,                                           
isnull(vcDomainDesc,'') vcDomainDesc,                                           
isnull(bitExchangeIntegration,0) bitExchangeIntegration,                                           
isnull(bitAccessExchange,0) bitAccessExchange,                                           
isnull(vcExchUserName,'') vcExchUserName,                                           
isnull(vcExchPassword,'') vcExchPassword,                                           
isnull(vcExchPath,'') vcExchPath,                                           
isnull(vcExchDomain,'') vcExchDomain,                                         
tintCustomPagingRows,                                         
vcDateFormat,                                         
numDefCountry,                                         
tintComposeWindow,                                         
sintStartDate,                                         
sintNoofDaysInterval,                                         
tintAssignToCriteria,                                         
bitIntmedPage,                                         
tintFiscalStartMonth,                                    
--isnull(bitDeferredIncome,0) as bitDeferredIncome,                                  
isnull(tintPayPeriod,0) as tintPayPeriod,                                
isnull(numCurrencyID,0) as numCurrencyID,                            
isnull(vcCurrency,'') as vcCurrency,                      
isnull(charUnitSystem,'') as charUnitSystem ,                    
isnull(vcPortalLogo,'') as vcPortalLogo,                  
isnull(tintChrForComSearch,2) as tintChrForComSearch,                
isnull(tintChrForItemSearch,'') as tintChrForItemSearch,            
isnull(intPaymentGateWay,0) as intPaymentGateWay  ,        
 case when bitPSMTPServer= 1 then vcPSMTPServer else '' end as vcPSMTPServer  ,              
 case when bitPSMTPServer= 1 then  isnull(numPSMTPPort,0)  else 0 end as numPSMTPPort          
,isnull(bitPSMTPAuth,0) bitPSMTPAuth        
,isnull([vcPSmtpPassword],'')vcPSmtpPassword        
,isnull(bitPSMTPSSL,0) bitPSMTPSSL     
,isnull(bitPSMTPServer,0) bitPSMTPServer  ,isnull(vcPSMTPUserName,'') vcPSMTPUserName,      
--isnull(numDefaultCategory,0) numDefaultCategory,
isnull(numShipCompany,0)   numShipCompany,
isnull(bitAutoPopulateAddress,0) bitAutoPopulateAddress,
isnull(tintPoulateAddressTo,0) tintPoulateAddressTo,
isnull(bitMultiCompany,0) bitMultiCompany,
isnull(bitMultiCurrency,0) bitMultiCurrency,
ISNULL(bitCreateInvoice,0) bitCreateInvoice,
ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
ISNULL([bitSaveCreditCardInfo],0) bitSaveCreditCardInfo,
ISNULL([intLastViewedRecord],10) intLastViewedRecord,
ISNULL([tintCommissionType],1) tintCommissionType,
ISNULL([tintBaseTax],2) tintBaseTax,
ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
ISNULL(bitEmbeddedCost,0) bitEmbeddedCost,
ISNULL(numDefaultSalesOppBizDocId,0) numDefaultSalesOppBizDocId,
ISNULL(tintSessionTimeOut,1) tintSessionTimeOut,
ISNULL(tintDecimalPoints,2) tintDecimalPoints,
ISNULL(bitCustomizePortal,0) bitCustomizePortal,
ISNULL(tintBillToForPO,0) tintBillToForPO,
ISNULL(tintShipToForPO,0) tintShipToForPO,
ISNULL(tintOrganizationSearchCriteria,1) tintOrganizationSearchCriteria,
ISNULL(tintLogin,0) tintLogin,
lower(ISNULL(vcPortalName,'')) vcPortalName,
ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
D.numDomainID,ISNULL(D.tintComAppliesTo,0) tintComAppliesTo,
ISNULL(vcPSMTPDisplayName,'') vcPSMTPDisplayName,
ISNULL(bitGtoBContact,0) bitGtoBContact,
ISNULL(bitBtoGContact,0) bitBtoGContact,
ISNULL(bitGtoBCalendar,0) bitGtoBCalendar,
ISNULL(bitBtoGCalendar,0) bitBtoGCalendar,
ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
ISNULL(bitInlineEdit,0) bitInlineEdit,
ISNULL(bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
ISNULL(bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
isnull(D.bitRentalItem,0) as bitRentalItem,
isnull(D.numRentalItemClass,0) as numRentalItemClass,
isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
ISNULL(D.bitAutoSerialNoAssign,0) as bitAutoSerialNoAssign,
ISNULL(D.bitIsShowBalance,0) AS [bitIsShowBalance],
ISNULL(numIncomeAccID,0) AS [numIncomeAccID],
ISNULL(numCOGSAccID,0) AS [numCOGSAccID],
ISNULL(numAssetAccID,0) AS [numAssetAccID],
ISNULL(IsEnableClassTracking,0) AS [IsEnableClassTracking],
ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
ISNULL(D.vcShipToPhoneNo,0) AS vcShipToPhoneNo,
ISNULL(D.vcGAUserEMail,'') AS vcGAUserEMail,
ISNULL(D.vcGAUserPassword,'') AS vcGAUserPassword,
ISNULL(D.vcGAUserProfileId,0) AS vcGAUserProfileId,
ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
ISNULL(D.vcHideTabs,'') AS vcHideTabs,
ISNULL(D.bitUseBizdocAmount,0) AS bitUseBizdocAmount,
ISNULL(D.IsEnableUserLevelClassTracking,0) AS IsEnableUserLevelClassTracking,
ISNULL(D.bitDefaultRateType,0) AS bitDefaultRateType,
ISNULL(D.bitIncludeTaxAndShippingInCommission,0) AS bitIncludeTaxAndShippingInCommission
/*,ISNULL(D.bitAllowPPVariance,0) AS bitAllowPPVariance*/
--,ISNULL(D.bitPurchaseTaxCredit,0) AS bitPurchaseTaxCredit
from Domain D  
left join eCommerceDTL eComm  
on eComm.numDomainID=D.numDomainID  
where (D.numDomainID=@numDomainID OR @numDomainID=-1)



/****** Object:  StoredProcedure [dbo].[usp_GetDuplicateCompany]    Script Date: 07/26/2008 16:17:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdueamount')
DROP PROCEDURE usp_getdueamount
GO
CREATE PROCEDURE [dbo].[USP_GetDueAmount]                      
@numDivisionID numeric(9)=0                      
as                      
declare @Amt as MONEY;set @Amt=0                        
declare @Paid as MONEY;set @Paid=0      
                 
declare @AmountDueSO as money;set @AmountDueSO=0                    
declare @AmountPastDueSO as money;set @AmountPastDueSO=0 

declare @AmountDuePO as money;set @AmountDuePO=0                    
declare @AmountPastDuePO as money;set @AmountPastDuePO=0 
                   
declare @CompanyCredit as money;set @CompanyCredit=0   
          
declare @PCreditMemo as money;set @PCreditMemo=0            
declare @SCreditMemo as money;set @SCreditMemo=0            

DECLARE @UTCtime AS datetime	       
SET	@UTCtime = dbo.[GetUTCDateWithoutTime]()

--Sales Order Total Amount Due                
select @Amt=isnull(sum(OppBD.[monDealAmount]),0) from OpportunityMaster  Opp
join  OpportunityBizDocs OppBD
on    OppBD.numOppID=Opp.numOppID               
where bitAuthoritativeBizDocs=1 and numDivisionID=@numDivisionID and tintOppStatus=1   and tintOpptype=1              
               
select @Paid=isnull(sum(monAmountPaid),0) from OpportunityMaster  Opp
join  OpportunityBizDocs OppBD
on    OppBD.numOppID=Opp.numOppID               
where bitAuthoritativeBizDocs=1 and numDivisionID=@numDivisionID and tintOppStatus=1   and tintOpptype=1               
                
set @AmountDueSO=@Amt-@Paid                
         
--Sales Order Total Amount Past Due                
select @Amt=isnull(sum(OppBD.[monDealAmount]),0) from OpportunityMaster   Opp
join  OpportunityBizDocs OppBD
on    OppBD.numOppID=Opp.numOppID               
where  bitAuthoritativeBizDocs=1 and numDivisionID=@numDivisionID and tintOppStatus=1   and tintOpptype=1              
AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
					 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)),0))  
					 ELSE 0 
				END, OppBD.[dtFromDate]) < @UTCtime
--AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
--					 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0))  
--					 ELSE 0 
--				END, OppBD.[dtFromDate]) < @UTCtime				

select @Paid=isnull(SUM(OppBD.monAmountPaid),0) from OpportunityMaster  Opp
join  OpportunityBizDocs OppBD on    OppBD.numOppID=Opp.numOppID               
where  OppBD.bitAuthoritativeBizDocs=1 and Opp.numDivisionID=@numDivisionID and tintOppStatus=1  and tintOpptype=1               
AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
					 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)),0)) 
					 ELSE 0 
				END, OppBD.[dtFromDate]) <@UTCtime
--AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
--					 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0)) 
--					 ELSE 0 
--				END, OppBD.[dtFromDate]) <@UTCtime				

set @AmountPastDueSO=@Amt-@Paid                    
                    
   
--Purchase Order Total Amount Due                
select @Amt=isnull(sum(OppBD.[monDealAmount]),0) from OpportunityMaster  Opp
join  OpportunityBizDocs OppBD
on    OppBD.numOppID=Opp.numOppID               
where bitAuthoritativeBizDocs=1 and numDivisionID=@numDivisionID and tintOppStatus=1   and tintOpptype=2              
               
select @Paid=isnull(sum(monAmountPaid),0) from OpportunityMaster  Opp
join  OpportunityBizDocs OppBD
on    OppBD.numOppID=Opp.numOppID               
where bitAuthoritativeBizDocs=1 and numDivisionID=@numDivisionID and tintOppStatus=1   and tintOpptype=2              
                
set @AmountDuePO=@Amt-@Paid                
         
--Purchase Order Total Amount Past Due                
select @Amt=isnull(sum(OppBD.[monDealAmount]),0) from OpportunityMaster   Opp
join  OpportunityBizDocs OppBD
on    OppBD.numOppID=Opp.numOppID               
where  bitAuthoritativeBizDocs=1 and numDivisionID=@numDivisionID and tintOppStatus=1   and tintOpptype=2              
AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
					 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)),0))  
					 ELSE 0 
				END, OppBD.[dtFromDate]) <@UTCtime
--AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
--					 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0))  
--					 ELSE 0 
--				END, OppBD.[dtFromDate]) <@UTCtime
				

select @Paid=isnull(SUM(OppBD.monAmountPaid),0) from OpportunityMaster  Opp
join  OpportunityBizDocs OppBD on    OppBD.numOppID=Opp.numOppID               
where  OppBD.bitAuthoritativeBizDocs=1 and Opp.numDivisionID=@numDivisionID and tintOppStatus=1  and tintOpptype=2               
AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
					 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)),0)) 
					 ELSE 0 
				END, OppBD.[dtFromDate]) <@UTCtime
--AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
--					 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0)) 
--					 ELSE 0 
--				END, OppBD.[dtFromDate]) <@UTCtime

set @AmountPastDuePO=@Amt-@Paid  


--Company Cresit Limit                    
select @CompanyCredit=convert(money,case when isnumeric(dbo.fn_GetListItemName(numCompanyCredit))=1 then dbo.fn_GetListItemName(numCompanyCredit) else '0' end)
	 from companyinfo C                    
join divisionmaster d                    
on d.numCompanyID=C.numCompanyID                    
where d.numdivisionid=@numDivisionID                    
                    
 
--Purchase Credit Memo
select @PCreditMemo=ISNULL(BPH.monPaymentAmount,0) - ISNULL (BPH.monAppliedAmount,0)
	 from BillPaymentHeader BPH          
where BPH.numdivisionid=@numDivisionID AND ISNULL(numReturnHeaderID,0)>0                  

--Sales Credit Memo
select @SCreditMemo=ISNULL(DM.monDepositAmount,0) - ISNULL (DM.monAppliedAmount,0)
	 from dbo.DepositMaster DM          
where DM.numdivisionid=@numDivisionID AND tintDepositePage=3 AND ISNULL(numReturnHeaderID,0)>0  

DECLARE @vcShippersAccountNo AS VARCHAR(100)
SELECT @vcShippersAccountNo = ISNULL(vcShippersAccountNo,'') FROM dbo.DivisionMaster WHERE numDivisionID = @numDivisionID

select isnull(@AmountDueSO,0) as AmountDueSO,isnull(@AmountPastDueSO,0) as AmountPastDueSO, 
isnull(@AmountDuePO,0) as AmountDuePO,isnull(@AmountPastDuePO,0) as AmountPastDuePO,                      
ISNULL(@CompanyCredit,0) - (isnull(@AmountDueSO,0) + isnull(@AmountDuePO,0)) as RemainingCredit,
isnull(@PCreditMemo,0) as PCreditMemo,
isnull(@SCreditMemo,0) as SCreditMemo,
ISNULL(@CompanyCredit,0) AS CreditLimit,
@vcShippersAccountNo AS vcShippersAccountNo 

GO
/****** Object:  StoredProcedure [dbo].[USP_GetEmailToCaseCheck]    Script Date: 07/26/2008 16:19:07 ******/

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetEmailToCaseCheck')
DROP PROCEDURE USP_GetEmailToCaseCheck
GO
Create PROCEDURE [dbo].[USP_GetEmailToCaseCheck]            
@numDomainID as numeric(9),            
@vcFromEmail VARCHAR(50),
@vcCaseNumber VARCHAR(50)
as             

   Declare @numcontactid numeric(9);SET @numcontactid=0;
   DECLARE @numDivisionId NUMERIC(9);SET @numDivisionId=0;
   DECLARE @numCaseId NUMERIC(9);SET @numCaseId=0;
   
   SELECT TOP 1 @numcontactid=ADC.numcontactid,@numDivisionId=ADC.numDivisionId from AdditionalContactsInformation ADC 
   JOIN DivisionMaster DM ON ADC.numDivisionId=DM.numDivisionId 
	WHERE ISNULL(DM.bitEmailToCase,0)=1 AND DM.numDomainID=@numDomainID AND ADC.vcEmail = @vcFromEmail
		AND ADC.numDomainId = @numDomainId
		
	IF LEN(@vcCaseNumber)>0 AND @vcCaseNumber!='0' 
		AND ISNULL(@numcontactid,0) <> 0 AND ISNULL(@numDivisionId,0)<> 0
	BEGIN
		SELECT TOP 1 @numCaseId=numCaseId FROM dbo.Cases WHERE numDomainID=@numDomainID
		AND numDivisionID=@numDivisionId AND numContactId=@numcontactid
		AND vcCaseNumber LIKE '%' + @vcCaseNumber
	END
	
	SELECT ISNULL(@numcontactid,0) AS numcontactid,ISNULL(@numDivisionId,0) AS numDivisionId,ISNULL(@numCaseId,0) AS numCaseId
          
GO
    
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetGrossProfitEstimate')
DROP PROCEDURE USP_GetGrossProfitEstimate
GO
CREATE PROCEDURE [dbo].[USP_GetGrossProfitEstimate] 
( 
@numDomainID as numeric(9)=0,    
@numOppID AS NUMERIC(9)=0
)
AS 

select numOppId,vcPOppName,vcItemName,monAverageCost,vcVendor,monPrice,VendorCost,numUnitHour,
isnull(numUnitHour,0) * (isnull(monPrice,0) - isnull(monAverageCost,0)) as Profit,
((isnull(monPrice,0) - isnull(monAverageCost,0)) 
/ (Case when isnull(monPrice,0)=0 then 1 else monPrice end)) * 100 ProfitPer
from
(
select opp.numOppId,Opp.vcPOppName,OppI.vcItemName,I.monAverageCost,
Case when isnull(OppVendor.numDivisionId,0)>0 then OppVendor.numDivisionId
     when isnull(OppI.numSOVendorId,0)=0 then V.numVendorID else OppI.numSOVendorId end as numVendorID,
dbo.fn_getcomapnyname(Case when isnull(OppVendor.numDivisionId,0)>0 then OppVendor.numDivisionId when isnull(OppI.numSOVendorId,0)=0 then V.numVendorID else OppI.numSOVendorId end) as vcVendor,
oppI.numUnitHour,oppI.monPrice,
Case when isnull(OppVendor.numDivisionId,0)>0 then OppVendor.monPrice
else dbo.fn_FindVendorCost(oppI.numItemCode,Case when isnull(OppI.numSOVendorId,0)=0 then V.numVendorID else OppI.numSOVendorId end,@numDomainID,oppI.numUnitHour) end VendorCost
      FROM OpportunityMaster Opp INNER JOIN OpportunityItems OppI ON opp.numOppId=OppI.numOppId 
		   INNER JOIN item I on OppI.numItemCode=i.numItemcode
		   Left JOIN Vendor V on V.numVendorID=I.numVendorID and V.numItemCode=I.numItemCode
		   outer apply(select top 1 oppV.numDivisionId,oppIV.monPrice 
			from OpportunityMaster OppV INNER JOIN OpportunityItems OppIV ON oppV.numOppId=OppIV.numOppId and OppIV.numItemCode=OppI.numItemCode
			Inner JOin OpportunityLinking OppLV on OppLV.numChildOppId=OppV.numOppId and OppLV.numParentOppId=Opp.numOppId where OppV.tintOppType=2
			) OppVendor	
           WHERE Opp.numDomainId=@numDomainID AND opp.numOppId=@numOppID) temp
/****** Object:  StoredProcedure [dbo].[usp_GetImapDTl]    Script Date: 07/26/2008 16:17:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getimapdtl')
DROP PROCEDURE usp_getimapdtl
GO
CREATE PROCEDURE [dbo].[usp_GetImapDTl]
    @numUserCntId AS NUMERIC(9),
    @numDomainId AS NUMERIC(9)
AS 

IF @numUserCntId = -1 AND @numDomainID > 0 --Support Email
 BEGIN                            
	  SELECT  ISNULL(bitImap, 0) bitImap,
        vcImapServerUrl,
        vcImapPassword,
        ISNULL(bitSSl, 0) bitSSl,
        numPort,
        vcImapUserName AS vcEmailID,
        ISNULL(numLastUID,0) AS LastUID,
        ISNULL(bitUseUserName, 0) bitUseUserName,isnull(vcGoogleRefreshToken,'') as vcGoogleRefreshToken
	FROM [ImapUserDetails] ImapUserDTL   
	where ImapUserDTL.numUserCntId = @numUserCntId 
	AND ImapUserDTL.numDomainID = @numDomainID
 END   
 ELSE
 BEGIN
SELECT  ISNULL(bitImap, 0) bitImap,
        vcImapServerUrl,
        vcImapPassword,
        ISNULL(bitSSl, 0) bitSSl,
        numPort,
        vcEmailID,
        ISNULL(numLastUID,0) AS LastUID,
        ISNULL(bitUseUserName, 0) bitUseUserName,isnull(vcGoogleRefreshToken,'') as vcGoogleRefreshToken
FROM    [ImapUserDetails]
        JOIN usermaster ON numUserDetailId = numUserCntId
WHERE   numUserCntId = @numUserCntId
        AND [ImapUserDetails].numDomainId = @numDomainId
END        
GO
/****** Object:  StoredProcedure [dbo].[usp_GetImapEnabledUsers]    Script Date: 06/04/2009 15:11:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_GetImapEnabledUsers')
DROP PROCEDURE usp_GetImapEnabledUsers
GO
CREATE PROCEDURE [dbo].[usp_GetImapEnabledUsers]
@tinyMode AS TINYINT=0,
@numDomainId AS NUMERIC(9)=0 
AS 
IF @tinyMode=0
  SELECT DISTINCT I.[numDomainId],I.[numUserCntId],I.* FROM [ImapUserDetails] I
	join Subscribers S on S.numTargetDomainId=I.numDomainId WHERE [bitImap] = 1 and S.bitActive=1
  
ELSE IF @tinyMode=1 --Get users to whom we need to send notification mail about error in last 10 attempt.
  SELECT DISTINCT I.[numDomainId],[numUserCntId],ISNULL(UM.vcEmailID,'') vcEmailID FROM [ImapUserDetails] I
	JOIN dbo.UserMaster UM ON UM.numUserDetailId = I.numUserCntId
	join Subscribers S on S.numTargetDomainId=I.numDomainId
  WHERE [bitImap] = 0 AND bitNotified=0 AND tintRetryLeft=0 and S.bitActive=1

ELSE IF @tinyMode=2  --Google to Biz Contact Distinct Domain 
  SELECT DISTINCT I.[numDomainId] FROM [ImapUserDetails] I JOIN Domain D ON I.numDomainId=D.numDomainId
	join Subscribers S on S.numTargetDomainId=I.numDomainId
   WHERE I.[bitImap] = 1 AND ISNULL(D.bitGtoBContact,0)=1 and S.bitActive=1

ELSE IF @tinyMode=3 --Google to Biz Calendar
   SELECT DISTINCT I.[numDomainId],I.[numUserCntId],I.* FROM [ImapUserDetails] I JOIN Domain D ON I.numDomainId=D.numDomainId
	join Subscribers S on S.numTargetDomainId=I.numDomainId
    join UserAccessedDTL UAD on UAD.numContactId=I.numUserCntId
   WHERE I.[bitImap] = 1 AND ISNULL(D.bitGtoBCalendar,0)=1 and S.bitActive=1 and len(isnull(vcGoogleRefreshToken,''))>0
		 and UAD.dtLoggedOutTime is null and getutcdate()<dateadd(hh,D.tintSessionTimeOut,UAD.dtLoggedInTime)

ELSE IF @tinyMode=4 --Biz to Google Calendar
  SELECT DISTINCT I.[numDomainId],I.[numUserCntId],I.* FROM [ImapUserDetails] I JOIN Domain D ON I.numDomainId=D.numDomainId
	join Subscribers S on S.numTargetDomainId=I.numDomainId
    join UserAccessedDTL UAD on UAD.numContactId=I.numUserCntId
   WHERE I.[bitImap] = 1 AND ISNULL(D.bitBtoGCalendar,0)=1 and S.bitActive=1 and len(isnull(vcGoogleRefreshToken,''))>0
	 and UAD.dtLoggedOutTime is null and getutcdate()<dateadd(hh,D.tintSessionTimeOut,UAD.dtLoggedInTime)

ELSE IF @tinyMode=5  --Google to Biz Contact Domain 
  SELECT DISTINCT I.[numDomainId],I.[numUserCntId],I.* FROM [ImapUserDetails] I JOIN Domain D ON I.numDomainId=D.numDomainId
	join Subscribers S on S.numTargetDomainId=I.numDomainId
   WHERE I.[bitImap] = 1 AND ISNULL(D.bitGtoBContact,0)=1 AND I.[numDomainId]=@numDomainId and S.bitActive=1

ELSE IF @tinyMode=6  --Biz to Google Email
  SELECT DISTINCT I.[numDomainId],I.[numUserCntId],I.* FROM [ImapUserDetails] I JOIN Domain D ON I.numDomainId=D.numDomainId
	join Subscribers S on S.numTargetDomainId=I.numDomainId
   WHERE I.[bitImap] = 1 AND ISNULL(D.bitGtoBContact,0)=1 AND I.[numDomainId]=@numDomainId and S.bitActive=1

  
  
  

 
  

  

/****** Object:  StoredProcedure [dbo].[USP_GetInvoiceList]    Script Date: 07/26/2008 16:17:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getinvoicelist')
DROP PROCEDURE usp_getinvoicelist
GO
CREATE PROCEDURE [dbo].[USP_GetInvoiceList]                            
 @numDivisionID as numeric(9)=0 ,
 @tintOppType AS TINYINT=1,
 @numDomainID AS NUMERIC(9)
as                        
 
 
 
IF (@tintOppType = 1 OR  @tintOppType =2 )
BEGIN
	
	SELECT OM.numOppID,OM.vcPOppName,OBD.numOppBizDocsId,OBD.vcBizDocID,
		isnull(OBD.monDealAmount * OM.fltExchangeRate,0) AS monDealAmount,ISNULL((OBD.[monAmountPaid] * OM.fltExchangeRate),0) AS monAmountPaid,
		isnull(OBD.monDealAmount * OM.fltExchangeRate - OBD.[monAmountPaid] * OM.fltExchangeRate,0) AS BalanceDue,
		[dbo].[FormatedDateFromDate](OBD.dtFromDate,OM.numDomainId) AS BillingDate,
		CASE ISNULL(OM.bitBillingTerms,0)  
                 WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(OM.intBillingDays,0)), 0)),OBD.dtFromDate),OM.numDomainId)
                 WHEN 0 THEN [dbo].[FormatedDateFromDate](OBD.[dtFromDate],OM.numDomainId)
               END AS DueDate,
        dbo.GetCreditTerms(OM.numOppId) as Credit,
		dbo.fn_GetContactName(OM.numCreatedBy)  as CreatedName,
		CONVERT(DATETIME, OM.bintCreatedDate) AS  dtCreatedDate,
		CONVERT(DATETIME, OM.bintCreatedDate) AS  numCreatedDate,
		isnull(OM.numPClosingPercent,0)as monAmountPaid,
		isnull(dbo.GetDealAmount(OM.numOppID,getdate(),0),0) as TotalAmt,
		OM.numContactId,
		ISNULL(OM.tintOppStatus,0) As [tintOppStatus]
		FROM dbo.OpportunityMaster OM 
		JOIN dbo.OpportunityBizDocs OBD ON OM.numoppID=OBD.numoppID
		JOIN dbo.DivisionMaster D ON D.numDivisionID = OM.numDivisionId
		JOIN dbo.CompanyInfo C ON C.numCompanyId = D.numCompanyID
		WHERE OM.numDomainId=@numDomainID AND OM.numDomainId = D.numDomainID AND OM.tintOppType= @tintOppType
		AND D.numDivisionID= @numDivisionID AND ISNULL(bitAuthoritativeBizDocs,0)=1
		AND OM.tintOppStatus=1 
		AND ISNULL(OBD.tintDeferred,0) <> 1 AND  isnull(OBD.monDealAmount * OM.fltExchangeRate,0) > 0 
		AND isnull(OBD.monDealAmount * OM.fltExchangeRate - OBD.[monAmountPaid] * OM.fltExchangeRate,0) > 0

----Create a Temporary table to hold data                        
--Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                         
--      numOppID varchar(15),vcPOppName varchar(100),                  
--      vcBizDocID varchar(100),                
--   BizDocName varchar(100),              
--   CreatedName varchar(100),                       
--      Credit VARCHAR(100),                        
--      numCreatedDate datetime,                        
-- monAmountPaid money,    
-- TotalAmt MONEY,
-- numContactId NUMERIC(9),
-- tintOppStatus TINYINT)                        
--                  
--                  
--declare @strSql as varchar(8000)                        
--set @strSql=
--'SELECT 
--O.numOppID,
--O.vcPOppName,               
--CASE WHEN O.tintOppType = 1 AND O.tintOppStatus = 0 THEN ''Sales Opportunity''
--	 WHEN O.tintOppType = 1 AND O.tintOppStatus = 1 THEN ''Sales Order''
--END as BizDocName,              
--dbo.fn_GetContactName(O.numCreatedBy)  as CreatedName,     
--dbo.GetCreditTerms(O.numOppId) as Credit,                  
--CONVERT(DATETIME, O.bintCreatedDate) AS  dtCreatedDate,
--isnull(O.numPClosingPercent,0)as monAmountPaid,
--isnull(dbo.GetDealAmount(O.numOppID,getdate(),0),0) as TotalAmt,
--O.numContactId,ISNULL(O.tintOppStatus,0) As [tintOppStatus]
--FROM dbo.OpportunityMaster O 
--     JOIN dbo.DivisionMaster D 
--        ON D.numDivisionID = O.numDivisionId
--     JOIN dbo.CompanyInfo C 
--        ON C.numCompanyId = D.numCompanyID
--WHERE O.numDomainId = D.numDomainID '
--      
--set @strSql=@strSql + ' and O.tintOppType= ' + convert(varchar(3),@tintOppType)
--
--if @numDivisionID<>0 set @strSql=@strSql + ' and D.numDivisionID= ' + convert(varchar(15),@numDivisionID)                        
--if @SortChar<>'0' set @strSql=@strSql + ' And vcBizDocID like '''+@SortChar+'%'''   
--IF @bitflag =0
--BEGIN
--	SET @strSql = @strSql + ' And O.tintShipped  =1'                                            
--END 
--ELSE IF @bitflag=1
--BEGIN
--SET @strSql = @strSql + ' And (O.tintOppStatus = 1 OR O.tintOppStatus = 0)'
--	
--END
--
--set @strSql=@strSql +' ORDER BY ' + @columnName +' '+ @columnSortOrder                     
--print @strSql                      
--insert into #tempTable(                        
--      numOppID,                  
-- vcBizDocID,                
-- BizDocName,              
--  CreatedName,                        
--      Credit,                        
--      numCreatedDate,                        
-- monAmountPaid,
-- TotalAmt,
-- numContactId,tintOppStatus)                        
--exec (@strSql)                         
--                        
--  declare @firstRec as integer                         
--  declare @lastRec as integer                        
-- set @firstRec= (@CurrentPage-1) * @PageSize                        
--     set @lastRec= (@CurrentPage*@PageSize+1)                        
--select *,(TotalAmt-monAmountPaid) AS BalanceDue,
--CASE WHEN @tintOppType = 1 AND tintOppStatus = 0 THEN 'Sales Opportunity' 
--	 WHEN @tintOppType = 1 AND tintOppStatus = 1 THEN 'Sales Order' 
--     WHEN @tintOppType = 2 THEN 'Purchase Order' END AS [Type]
-- from #tempTable where ID > @firstRec and ID < @lastRec                        
--set @TotRecs=(select count(*) from #tempTable)                        
--drop table #tempTable
END 
--For bills
IF @tintOppType =  3 
BEGIN
-- SELECT 'Bill' AS [Type],OBD.vcMemo,OBD.vcReference,OBD.monAmount,dbo.FormatedDateFromDate(PD.dtDueDate,OBD.numDomainId) DueDate
-- FROM   dbo.OpportunityBizDocsDetails OBD
--        INNER JOIN dbo.OpportunityBizDocsPaymentDetails PD ON PD.numBizDocsPaymentDetId = OBD.numBizDocsPaymentDetId
--  WHERE OBD.numDivisionID = @numDivisionID AND PD.bitIntegrated=0

SELECT 0 AS numOppId,'Bill' vcPOppName,0 AS numOppBizDocsId,'Bill' + CASE WHEN len(BH.vcReference)=0 THEN '' ELSE '-' + BH.vcReference END AS [vcBizDocID],
						   BH.monAmountDue as monDealAmount,
						   ISNULL(BH.monAmtPaid, 0) as monAmountPaid,
					       ISNULL(BH.monAmountDue, 0) - ISNULL(BH.monAmtPaid, 0) as BalanceDue,
					       [dbo].[FormatedDateFromDate](BH.dtBillDate,BH.numDomainID) AS BillingDate,
						   [dbo].[FormatedDateFromDate](BH.dtDueDate,BH.numDomainID) AS DueDate
						   FROM    
							BillHeader BH 
							WHERE BH.numDomainId=@numDomainID AND BH.numDivisionId = @numDivisionID
							AND ISNULL(BH.monAmountDue, 0) - ISNULL(BH.monAmtPaid, 0) > 0
							
END



--created by anoop jayaraj
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetOppItemBizDocs' ) 
    DROP PROCEDURE USP_GetOppItemBizDocs
GO
CREATE PROCEDURE USP_GetOppItemBizDocs
    @numBizDocID AS NUMERIC(9),
    @numDomainID AS NUMERIC(9),
    @numOppID AS NUMERIC(9),
    @numOppBizDocID AS NUMERIC(9),
    @bitRentalBizDoc AS BIT = 0
AS 
    IF @numOppBizDocID = 0 
        IF @bitRentalBizDoc = 1 
            BEGIN
                SELECT  *
                FROM    ( SELECT    OI.numoppitemtCode,
                                    OI.numItemCode,
                                    OI.vcItemName,
                                    OI.vcModelID,
                                    OI.numUnitHour AS QtyOrdered,
                                    ( OI.numUnitHour
                                      - ISNULL(SUM(OBI.numUnitHour), 0) ) AS QtytoFulFilled,
                                    ( OI.numUnitHour
                                      - ISNULL(SUM(OBI.numUnitHour), 0) ) AS QtytoFulFill,
                                    ISNULL(numAllocation, 0) AS numAllocation,
                                    ISNULL(numBackOrder, 0) AS numBackOrder,
                                    '' AS vcNotes,
                                    '' AS vcTrackingNo,
                                    '' vcShippingMethod,
                                    0 monShipCost,
                                    GETDATE() dtDeliveryDate,
                                    CONVERT(BIT, 1) AS Included,
                                    ISNULL(OI.[numQtyShipped], 0) numQtyShipped,
                                    ISNULL(OI.[numUnitHourReceived], 0) numQtyReceived,
                                    I.[charItemType],
                                    SUBSTRING(( SELECT  ',' + vcSerialNo
                                                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1 THEN ' (' + CONVERT(VARCHAR(15), oppI.numQty) + ')'
                                                               ELSE ''
                                                          END
                                                FROM    OppWarehouseSerializedItem oppI
                                                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                                WHERE   oppI.numOppID = @numOppID
                                                        AND oppI.numOppItemID = OI.numoppitemtCode
                                                        AND oppI.numOppBizDocsId = @numOppBizDocID
                                                ORDER BY vcSerialNo
                                              FOR
                                                XML PATH('')
                                              ), 2, 200000) AS SerialLotNo,
                                    I.bitSerialized,
                                    ISNULL(I.bitLotNo, 0) AS bitLotNo,
                                    OI.numWarehouseItmsID,
                                    ISNULL(I.numItemClassification, 0) AS numItemClassification,
                                    I.vcSKU,
                                    dbo.fn_GetAttributes(OI.numWarehouseItmsID,
                                                         I.bitSerialized) AS Attributes,
                                    OM.bintCreatedDate AS dtRentalStartDate,
                                    GETDATE() AS dtRentalReturnDate,
                                    ISNULL(OI.numUOMId, 0) AS numUOM,
                                    OI.monPrice AS monOppItemPrice,
                                    ISNULL(OBI.monPrice, OI.monPrice) AS monOppBizDocPrice,
                                    ISNULL(OI.vcItemDesc, '') AS [vcItemDesc]
                          FROM      OpportunityItems OI
                                    LEFT JOIN OpportunityMaster OM ON OI.numOppID = oM.numOppID
                                    LEFT JOIN OpportunityBizDocs OB ON OB.numOppId = OI.numOppId
                                                                       AND OB.numBizDocId = @numBizDocID
                                    LEFT JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocId
                                                                            AND OBI.numOppItemID = OI.numoppitemtCode
                                    LEFT JOIN WareHouseItems WI ON WI.numWareHouseItemID = OI.numWarehouseItmsID
                                    INNER JOIN Item I ON I.[numItemCode] = OI.[numItemCode]
                          WHERE     OI.numOppId = @numOppID
                                    AND OI.numoppitemtCode NOT IN (
                                    SELECT  OBDI.numOppItemID
                                    FROM    OpportunityBizDocs OBD
                                            JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId = OBDI.numOppBizDocId
                                    WHERE   OBD.numOppId = @numOppID )
                          GROUP BY  OI.numoppitemtCode,
                                    OI.numItemCode,
                                    OI.vcItemName,
                                    OI.vcModelID,
                                    OI.numUnitHour,
                                    numAllocation,
                                    numBackOrder,
                                    OI.numQtyShipped,
                                    OI.[numUnitHourReceived],
                                    I.[charItemType],
                                    I.bitSerialized,
                                    I.bitLotNo,
                                    OI.numWarehouseItmsID,
                                    I.numItemClassification,
                                    I.vcSKU,
                                    OI.numWarehouseItmsID,
                                    OBI.dtRentalStartDate,
                                    OBI.dtRentalReturnDate,
                                    OI.numUOMId,
                                    OM.bintCreatedDate,
                                    OI.monPrice,
                                    OBI.monPrice,
                                    OI.vcItemDesc
                        ) X
                WHERE   X.QtytoFulFill > 0

            END

        ELSE 
            BEGIN
                SELECT  *
                FROM    ( SELECT    OI.numoppitemtCode,
                                    OI.numItemCode,
                                    OI.vcItemName,
                                    OI.vcModelID,
                                    OI.numUnitHour AS QtyOrdered,
                                    ( OI.numUnitHour
                                      - ISNULL(SUM(OBI.numUnitHour), 0) ) AS QtytoFulFilled,
                                    ( OI.numUnitHour
                                      - ISNULL(SUM(OBI.numUnitHour), 0) ) AS QtytoFulFill,
                                    ISNULL(numAllocation, 0) AS numAllocation,
                                    ISNULL(numBackOrder, 0) AS numBackOrder,
                                    '' AS vcNotes,
                                    '' AS vcTrackingNo,
                                    '' vcShippingMethod,
                                    0 monShipCost,
                                    GETDATE() dtDeliveryDate,
                                    CONVERT(BIT, 1) AS Included,
                                    ISNULL(OI.[numQtyShipped], 0) numQtyShipped,
                                    ISNULL(OI.[numUnitHourReceived], 0) numQtyReceived,
                                    I.[charItemType],
                                    SUBSTRING(( SELECT  ',' + vcSerialNo
                                                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1 THEN ' (' + CONVERT(VARCHAR(15), oppI.numQty) + ')'
                                                               ELSE ''
                                                          END
                                                FROM    OppWarehouseSerializedItem oppI
                                                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                                WHERE   oppI.numOppID = @numOppID
                                                        AND oppI.numOppItemID = OI.numoppitemtCode
                                                        AND oppI.numOppBizDocsId = @numOppBizDocID
                                                ORDER BY vcSerialNo
                                              FOR
                                                XML PATH('')
                                              ), 2, 200000) AS SerialLotNo,
                                    I.bitSerialized,
                                    ISNULL(I.bitLotNo, 0) AS bitLotNo,
                                    OI.numWarehouseItmsID,
                                    ISNULL(I.numItemClassification, 0) AS numItemClassification,
                                    I.vcSKU,
                                    dbo.fn_GetAttributes(OI.numWarehouseItmsID,
                                                         I.bitSerialized) AS Attributes,
                                    OBI.dtRentalStartDate,
                                    OBI.dtRentalReturnDate,
                                    ISNULL(OI.numUOMId, 0) AS numUOM,
                                    OI.monPrice AS monOppItemPrice,
                                    ISNULL(OBI.monPrice, OI.monPrice) AS monOppBizDocPrice,
                                    ISNULL(OI.vcItemDesc, '') AS [vcItemDesc]
                          FROM      OpportunityItems OI
                                    LEFT JOIN OpportunityMaster OM ON OI.numOppID = oM.numOppID 
--	left join OpportunityBizDocs OB
--	on OB.numOppId=OI.numOppId and OB.numBizDocId=@numBizDocID 
                                    LEFT JOIN OpportunityBizDocItems OBI ON OBI.numOppItemID = OI.numoppitemtCode --AND OB.numOppBizDocsId=OBI.numOppBizDocId 
                                                                            AND OBI.numOppBizDocId IN ( SELECT  numOppBizDocsId
                                                                                                        FROM    OpportunityBizDocs OB
                                                                                                        WHERE   OB.numOppId = OI.numOppId
                                                                                                                AND numBizDocID = @numBizDocID )
                                    LEFT JOIN WareHouseItems WI ON WI.numWareHouseItemID = OI.numWarehouseItmsID
                                    INNER JOIN Item I ON I.[numItemCode] = OI.[numItemCode]
                          WHERE     OI.numOppId = @numOppID
                          GROUP BY  OI.numoppitemtCode,
                                    OI.numItemCode,
                                    OI.vcItemName,
                                    OI.vcModelID,
                                    OI.numUnitHour,
                                    numAllocation,
                                    numBackOrder,
                                    OI.numQtyShipped,
                                    OI.[numUnitHourReceived],
                                    I.[charItemType],
                                    I.bitSerialized,
                                    I.bitLotNo,
                                    OI.numWarehouseItmsID,
                                    I.numItemClassification,
                                    I.vcSKU,
                                    OI.numWarehouseItmsID,
                                    OBI.dtRentalStartDate,
                                    OBI.dtRentalReturnDate,
                                    OI.numUOMId,
                                    OI.monPrice,
                                    OBI.monPrice,
                                    OI.vcItemDesc
                        ) X
                WHERE   X.QtytoFulFill > 0
            END
    ELSE 
        IF @bitRentalBizDoc = 1 
            BEGIN
                SELECT  OI.numoppitemtCode,
                        OI.numItemCode,
                        OI.vcItemName,
                        OI.vcModelID,
                        OI.numUnitHour AS QtyOrdered,
                        OBI.numUnitHour AS QtytoFulFilled,
                        ( OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour), 0) ) AS QtytoFulFill,
                        ISNULL(numAllocation, 0) AS numAllocation,
                        ISNULL(numBackOrder, 0) AS numBackOrder,
                        ISNULL(vcNotes, '') AS vcNotes,
                        ISNULL(OBI.vcTrackingNo, '') AS vcTrackingNo,
                        OBI.vcShippingMethod,
                        OBI.monShipCost,
                        OBI.dtDeliveryDate,
                        CONVERT(BIT, 1) AS Included,
                        ISNULL(OI.[numQtyShipped], 0) numQtyShipped,
                        ISNULL(OI.[numUnitHourReceived], 0) numQtyReceived,
                        I.[charItemType],
                        SUBSTRING(( SELECT  ',' + vcSerialNo
                                            + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                                   THEN ' ('
                                                        + CONVERT(VARCHAR(15), oppI.numQty)
                                                        + ')'
                                                   ELSE ''
                                              END
                                    FROM    OppWarehouseSerializedItem oppI
                                            JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                    WHERE   oppI.numOppID = @numOppID
                                            AND oppI.numOppItemID = OI.numoppitemtCode
                                            AND oppI.numOppBizDocsId = @numOppBizDocID
                                    ORDER BY vcSerialNo
                                  FOR
                                    XML PATH('')
                                  ), 2, 200000) AS SerialLotNo,
                        I.bitSerialized,
                        ISNULL(I.bitLotNo, 0) AS bitLotNo,
                        OI.numWarehouseItmsID,
                        ISNULL(I.numItemClassification, 0) AS numItemClassification,
                        I.vcSKU,
                        dbo.fn_GetAttributes(OI.numWarehouseItmsID,
                                             I.bitSerialized) AS Attributes,
                        OBI.dtRentalStartDate,
                        OBI.dtRentalReturnDate,
                        ISNULL(OI.numUOMId, 0) AS numUOM,
                        OI.monPrice AS monOppItemPrice,
                        ISNULL(OBI.monPrice, OI.monPrice) AS monOppBizDocPrice,
                        ISNULL(OI.vcItemDesc, '') AS [vcItemDesc]
                FROM    OpportunityItems OI
                        LEFT JOIN OpportunityMaster OM ON OI.numOppID = oM.numOppID
                        JOIN OpportunityBizDocs OB ON OB.numOppId = OI.numOppId
                        JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocId
                                                           AND OBI.numOppItemID = OI.numoppitemtCode
                        LEFT JOIN WareHouseItems WI ON WI.numWareHouseItemID = OI.numWarehouseItmsID
                        INNER JOIN Item I ON I.[numItemCode] = OI.[numItemCode]
                WHERE   OI.numOppId = @numOppID
                        AND OB.numOppBizDocsId = @numOppBizDocID
                GROUP BY OI.numoppitemtCode,
                        OI.numItemCode,
                        OI.vcItemName,
                        OI.vcModelID,
                        OI.numUnitHour,
                        numAllocation,
                        numBackOrder,
                        vcNotes,
                        OBI.vcTrackingNo,
                        OBI.vcShippingMethod,
                        OBI.monShipCost,
                        OBI.dtDeliveryDate,
                        OBI.numUnitHour,
                        OI.[numQtyShipped],
                        OI.[numUnitHourReceived],
                        I.[charItemType],
                        I.bitSerialized,
                        I.bitLotNo,
                        OI.numWarehouseItmsID,
                        I.numItemClassification,
                        I.vcSKU,
                        OI.numWarehouseItmsID,
                        OBI.dtRentalStartDate,
                        OBI.dtRentalReturnDate,
                        OI.numUOMId,
                        OI.monPrice,
                        OBI.monPrice,
                        OI.vcItemDesc

            END

        ELSE 
            BEGIN

                SELECT  OI.numoppitemtCode,
                        OI.numItemCode,
                        OI.vcItemName,
                        OI.vcModelID,
                        OI.numUnitHour AS QtyOrdered,
                        OBI.numUnitHour AS QtytoFulFilled,
                        ( OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour), 0) ) AS QtytoFulFill,
                        ISNULL(numAllocation, 0) AS numAllocation,
                        ISNULL(numBackOrder, 0) AS numBackOrder,
                        ISNULL(vcNotes, '') AS vcNotes,
                        ISNULL(OBI.vcTrackingNo, '') AS vcTrackingNo,
                        OBI.vcShippingMethod,
                        OBI.monShipCost,
                        OBI.dtDeliveryDate,
                        CONVERT(BIT, 1) AS Included,
                        ISNULL(OI.[numQtyShipped], 0) numQtyShipped,
                        ISNULL(OI.[numUnitHourReceived], 0) numQtyReceived,
                        I.[charItemType],
                        SUBSTRING(( SELECT  ',' + vcSerialNo
                                            + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                                   THEN ' ('
                                                        + CONVERT(VARCHAR(15), oppI.numQty)
                                                        + ')'
                                                   ELSE ''
                                              END
                                    FROM    OppWarehouseSerializedItem oppI
                                            JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                    WHERE   oppI.numOppID = @numOppID
                                            AND oppI.numOppItemID = OI.numoppitemtCode
                                            AND oppI.numOppBizDocsId = @numOppBizDocID
                                    ORDER BY vcSerialNo
                                  FOR
                                    XML PATH('')
                                  ), 2, 200000) AS SerialLotNo,
                        I.bitSerialized,
                        ISNULL(I.bitLotNo, 0) AS bitLotNo,
                        OI.numWarehouseItmsID,
                        ISNULL(I.numItemClassification, 0) AS numItemClassification,
                        I.vcSKU,
                        dbo.fn_GetAttributes(OI.numWarehouseItmsID,
                                             I.bitSerialized) AS Attributes,
                        OBI.dtRentalStartDate,
                        OBI.dtRentalReturnDate,
                        ISNULL(OI.numUOMId, 0) AS numUOM,
                        OI.monPrice AS monOppItemPrice,
                        ISNULL(OBI.monPrice, OI.monPrice) AS monOppBizDocPrice,
                        ISNULL(OI.vcItemDesc, '') AS [vcItemDesc]
                FROM    OpportunityItems OI
                        LEFT JOIN OpportunityMaster OM ON OI.numOppID = oM.numOppID
                        JOIN OpportunityBizDocs OB ON OB.numOppId = OI.numOppId
                        JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocId
                                                           AND OBI.numOppItemID = OI.numoppitemtCode
                        LEFT JOIN WareHouseItems WI ON WI.numWareHouseItemID = OI.numWarehouseItmsID
                        INNER JOIN Item I ON I.[numItemCode] = OI.[numItemCode]
                WHERE   OI.numOppId = @numOppID
                        AND OB.numOppBizDocsId = @numOppBizDocID
                GROUP BY OI.numoppitemtCode,
                        OI.numItemCode,
                        OI.vcItemName,
                        OI.vcModelID,
                        OI.numUnitHour,
                        numAllocation,
                        numBackOrder,
                        vcNotes,
                        OBI.vcTrackingNo,
                        OBI.vcShippingMethod,
                        OBI.monShipCost,
                        OBI.dtDeliveryDate,
                        OBI.numUnitHour,
                        OI.[numQtyShipped],
                        OI.[numUnitHourReceived],
                        I.[charItemType],
                        I.bitSerialized,
                        I.bitLotNo,
                        OI.numWarehouseItmsID,
                        I.numItemClassification,
                        I.vcSKU,
                        OI.numWarehouseItmsID,
                        OBI.dtRentalStartDate,
                        OBI.dtRentalReturnDate,
                        OI.numUOMId,
                        OI.monPrice,
                        OBI.monPrice,
                        OI.vcItemDesc
                UNION
                SELECT  *
                FROM    ( SELECT    OI.numoppitemtCode,
                                    OI.numItemCode,
                                    OI.vcItemName,
                                    OI.vcModelID,
                                    OI.numUnitHour AS QtyOrdered,
                                    ( OI.numUnitHour
                                      - ISNULL(SUM(OBI.numUnitHour), 0) ) AS QtytoFulFilled,
                                    ( OI.numUnitHour
                                      - ISNULL(SUM(OBI.numUnitHour), 0) ) AS QtytoFulFill,
                                    ISNULL(numAllocation, 0) AS numAllocation,
                                    ISNULL(numBackOrder, 0) AS numBackOrder,
                                    '' AS vcNotes,
                                    '' AS vcTrackingNo,
                                    '' vcShippingMethod,
                                    1 AS monShipCost,
                                    NULL dtDeliveryDate,
                                    CONVERT(BIT, 0) AS Included,
                                    ISNULL(OI.[numQtyShipped], 0) numQtyShipped,
                                    ISNULL(OI.[numUnitHourReceived], 0) numQtyReceived,
                                    I.[charItemType],
                                    SUBSTRING(( SELECT  ',' + vcSerialNo
                                                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1 THEN ' (' + CONVERT(VARCHAR(15), oppI.numQty) + ')'
                                                               ELSE ''
                                                          END
                                                FROM    OppWarehouseSerializedItem oppI
                                                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                                WHERE   oppI.numOppID = @numOppID
                                                        AND oppI.numOppItemID = OI.numoppitemtCode
                                                        AND oppI.numOppBizDocsId = @numOppBizDocID
                                                ORDER BY vcSerialNo
                                              FOR
                                                XML PATH('')
                                              ), 2, 200000) AS SerialLotNo,
                                    I.bitSerialized,
                                    ISNULL(I.bitLotNo, 0) AS bitLotNo,
                                    OI.numWarehouseItmsID,
                                    ISNULL(I.numItemClassification, 0) AS numItemClassification,
                                    I.vcSKU,
                                    dbo.fn_GetAttributes(OI.numWarehouseItmsID,
                                                         I.bitSerialized) AS Attributes,
                                    OBI.dtRentalStartDate,
                                    OBI.dtRentalReturnDate,
                                    ISNULL(OI.numUOMId, 0) AS numUOM,
                                    OI.monPrice AS monOppItemPrice,
                                    ISNULL(OBI.monPrice, OI.monPrice) AS monOppBizDocPrice,
                                    ISNULL(OI.vcItemDesc, '') AS [vcItemDesc]
                          FROM      OpportunityItems OI
                                    LEFT JOIN OpportunityMaster OM ON OI.numOppID = oM.numOppID
                                    LEFT JOIN OpportunityBizDocs OB ON OB.numOppId = OI.numOppId
                                                                       AND OB.numBizDocId = @numBizDocID
                                    LEFT JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocId
                                                                            AND OBI.numOppItemID = OI.numoppitemtCode
                                    LEFT JOIN WareHouseItems WI ON WI.numWareHouseItemID = OI.numWarehouseItmsID
                                    INNER JOIN Item I ON I.[numItemCode] = OI.[numItemCode]
                          WHERE     OI.numOppId = @numOppID
                                    AND OI.numoppitemtCode NOT IN (
                                    SELECT  numOppItemID
                                    FROM    OpportunityBizDocItems OBI2
                                            JOIN OpportunityBizDocs OB2 ON OBI2.numOppBizDocID = OB2.numOppBizDocsId
                                    WHERE   numOppID = @numOppID
                                            AND numBizDocId = @numBizDocID )
                          GROUP BY  OI.numoppitemtCode,
                                    OI.numItemCode,
                                    OI.vcItemName,
                                    OI.vcModelID,
                                    OI.numUnitHour,
                                    numAllocation,
                                    numBackOrder,
                                    OBI.numUnitHour,
                                    OI.[numQtyShipped],
                                    OI.[numUnitHourReceived],
                                    I.[charItemType],
                                    I.bitSerialized,
                                    I.bitLotNo,
                                    OI.numWarehouseItmsID,
                                    I.numItemClassification,
                                    I.vcSKU,
                                    OI.numWarehouseItmsID,
                                    OBI.dtRentalStartDate,
                                    OBI.dtRentalReturnDate,
                                    OI.numUOMId,
                                    OI.monPrice,
                                    OBI.monPrice,
                                    OI.vcItemDesc
                        ) X
                WHERE   X.QtytoFulFill > 0

            END



GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetPageNavigationAuthorizationDetails' ) 
    DROP PROCEDURE USP_GetPageNavigationAuthorizationDetails
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- EXEC USP_GetPageNavigationAuthorizationDetails 1,1
CREATE PROCEDURE USP_GetPageNavigationAuthorizationDetails
    (
      @numGroupID NUMERIC(18, 0),
      @numTabID NUMERIC(18, 0),
      @numDomainID NUMERIC(18, 0)
    )
AS 
    SET NOCOUNT ON
    SET TRANSACTION ISOLATION LEVEL READ COMMITTED
  
    BEGIN
		/*
		SELECT * FROM dbo.TabMaster
		SELECT * FROM dbo.ModuleMaster
		SELECT * FROM 
		*/
        PRINT @numTabID 
        DECLARE @numModuleID AS NUMERIC(18, 0)
        SELECT TOP 1
                @numModuleID = numModuleID
        FROM    dbo.PageNavigationDTL
        WHERE   numTabID = @numTabID 
        PRINT @numModuleID
        
        IF @numModuleID = 10 
            BEGIN
                PRINT 10
                SELECT  *
                FROM    ( SELECT    ISNULL(( SELECT TOP 1
                                                    TNA.[numPageAuthID]
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = PND.[numPageNavID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 0) AS [numPageAuthID],
                                    ISNULL(@numGroupID, 0) AS [numGroupID],
                                    ISNULL(PND.[numTabID], 0) AS [numTabID],
                                    ISNULL(PND.[numPageNavID], 0) AS [numPageNavID],
                                    ISNULL(( SELECT TOP 1
                                                    TNA.[bitVisible]
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = PND.[numPageNavID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 0) AS [bitVisible],
                                    @numDomainID AS [numDomainID],
                                    ( SELECT TOP 1
                                                vcPageNavName
                                      FROM      dbo.PageNavigationDTL
                                      WHERE     PageNavigationDTL.numPageNavID = PND.numPageNavID
                                    ) AS [vcNodeName],
                                    ISNULL(( SELECT TOP 1
                                                    TNA.tintType
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = PND.[numPageNavID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 1) AS [tintType]
                          FROM      dbo.PageNavigationDTL PND
                          WHERE     ISNULL(PND.bitVisible, 0) = 1
                                    AND PND.numTabID = @numTabID
                                    AND numModuleID = @numModuleID
                          UNION ALL
                          SELECT    ISNULL(( SELECT TOP 1
                                                    TNA.[numPageAuthID]
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = LD.[numListItemID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 0) AS [numPageAuthID],
                                    @numGroupID AS [numGroupID],
                                    1 AS [numTabID],
                                    ISNULL(LD.[numListItemID], 0) AS [numPageNavID],
                                    ISNULL(( SELECT TOP 1
                                                    TNA.[bitVisible]
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = LD.[numListItemID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 0) AS [bitVisible],
                                    ISNULL(LD.[numDomainID], 0) AS [numDomainID],
                                    ISNULL(vcData, '') + 's' AS [vcNodeName],
                                    ISNULL(( SELECT TOP 1
                                                    TNA.tintType
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = LD.[numListItemID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 1) AS [tintType]
                          FROM      dbo.ListDetails LD
						--LEFT JOIN dbo.TreeNavigationAuthorization TNA ON LD.numListItemID = TNA.numPageNavID 
                          WHERE     numListID = 27
                                    AND LD.numDomainID = @numDomainID
                                    AND ISNULL(constFlag, 0) = 0
						--AND ( numTabID = (CASE WHEN @numModuleID = 10 THEN 1 ELSE 0 END) OR ISNULL(numTabID,0) = 0)
                          UNION ALL
                          SELECT    ISNULL(( SELECT TNA.[numPageAuthID]
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = LD.[numListItemID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 0) AS [numPageAuthID],
                                    @numGroupID AS [numGroupID],
                                    1 AS [numTabID],
                                    ISNULL(LD.[numListItemID], 0) AS [numPageNavID],
                                    ISNULL(( SELECT TNA.[bitVisible]
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = LD.[numListItemID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 0) AS [bitVisible],
                                    @numDomainID AS [numDomainID],
                                    ISNULL(vcData, '') + 's' AS [vcNodeName],
                                    ISNULL(( SELECT TNA.tintType
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = LD.[numListItemID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 1) AS [tintType]
                          FROM      dbo.ListDetails LD
						--LEFT JOIN dbo.TreeNavigationAuthorization TNA ON LD.numListItemID = TNA.numPageNavID AND ISNULL(TNA.[numDomainID], 0) = @numDomainID
                          WHERE     numListID = 27
                                    AND ISNULL(constFlag, 0) = 1
                        ) TABLE1
                ORDER BY numPageNavID
            END
		
        IF @numModuleID = 14 
            BEGIN
                IF NOT EXISTS ( SELECT  *
                                FROM    TreeNavigationAuthorization
                                WHERE   numTabID = 8
                                        AND numDomainID = 1
                                        AND numGroupID = 1 ) 
                    BEGIN
                        SELECT  TNA.numPageAuthID,
                                TNA.numGroupID,
                                TNA.numTabID,
                                TNA.numPageNavID,
                                TNA.bitVisible,
                                TNA.numDomainID,
                                ISNULL(PND.vcPageNavName, '') AS [vcNodeName],
                                tintType
                        FROM    PageNavigationDTL PND
                                JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
                        WHERE   1 = 1
                                AND ISNULL(TNA.bitVisible, 0) = 1
                                AND ISNULL(PND.bitVisible, 0) = 1
                                AND numModuleID = @numModuleID
                                AND TNA.numDomainID = @numDomainID
                                AND numGroupID = @numGroupID
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                1111 numPageNavID,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                'Regular Documents' AS [vcNodeName],
                                1 tintType
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                Ld.numListItemId,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                ISNULL(vcData, '') AS [vcNodeName],
                                1 tintType
                        FROM    listdetails Ld
                                LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
                                                          AND Lo.numDomainId = @numDomainID
                        WHERE   Ld.numListID = 29
                                AND ( constFlag = 1
                                      OR Ld.numDomainID = @numDomainID
                                    )
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                ld.numListItemID numPageNavID,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                ISNULL(vcData, '') AS [vcNodeName],
                                1 tintType
                        FROM    listdetails Ld
                                LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
                                                          AND lo.numDomainId = @numDomainID
                                INNER JOIN AuthoritativeBizDocs AB ON ( LD.numListItemID = AB.numAuthoritativeSales )
                        WHERE   LD.numListID = 27
                                AND ( constFlag = 1
                                      OR LD.numDomainID = @numDomainID
                                    )
                                AND AB.numDomainID = @numDomainID
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                ld.numListItemID numPageNavID,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                ISNULL(vcData, '') AS [vcNodeName],
                                1 tintType
                        FROM    listdetails Ld
                                LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
                                                          AND lo.numDomainId = @numDomainID
                                INNER JOIN AuthoritativeBizDocs AB ON ( ld.numListItemID = AB.numAuthoritativePurchase )
                        WHERE   LD.numListID = 27
                                AND ( constFlag = 1
                                      OR LD.numDomainID = @numDomainID
                                    )
                                AND AB.numDomainID = @numDomainID
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                0 numPageNavID,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                ISNULL(LT.vcData, '') AS [vcNodeName],
                                1 tintType
                        FROM    ListDetails LT,
                                listdetails ld
                        WHERE   LT.numListID = 11
                                AND Lt.numDomainID = @numDomainID
                                AND LD.numListID = 27
                                AND ( ld.constFlag = 1
                                      OR LD.numDomainID = @numDomainID
                                    )
                                AND ( LD.numListItemID IN (
                                      SELECT    AB.numAuthoritativeSales
                                      FROM      AuthoritativeBizDocs AB
                                      WHERE     AB.numDomainId = @numDomainID )
                                      OR ld.numListItemID IN (
                                      SELECT    AB.numAuthoritativePurchase
                                      FROM      AuthoritativeBizDocs AB
                                      WHERE     AB.numDomainId = @numDomainID )
                                    )
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                2222 numPageNavID,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                'Other BizDocs' AS [vcNodeName],
                                1 tintType
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                ld.numListItemID numPageNavID,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                ISNULL(vcData, '') AS [vcNodeName],
                                1 tintType
                        FROM    listdetails Ld
                        WHERE   ld.numListID = 27
                                AND ( constFlag = 1
                                      OR LD.numDomainID = @numDomainID
                                    )
                                AND LD.numListItemID NOT IN (
                                SELECT  AB.numAuthoritativeSales
                                FROM    AuthoritativeBizDocs AB
                                WHERE   AB.numDomainID = @numDomainID )
                                AND LD.numListItemID NOT IN (
                                SELECT  AB.numAuthoritativePurchase
                                FROM    AuthoritativeBizDocs AB
                                WHERE   AB.numDomainID = @numDomainID )
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                ls.numListItemID numPageNavID,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                ISNULL(ls.vcData, '') AS [vcNodeName],
                                1 tintType
                        FROM    listdetails Ld,
                                ( SELECT    *
                                  FROM      ListDetails LS
                                  WHERE     LS.numDomainID = @numDomainID
                                            AND LS.numListID = 11
                                ) LS
                        WHERE   ld.numListID = 27
                                AND ( ld.constFlag = 1
                                      OR LD.numDomainID = @numDomainID
                                    )
                                AND LD.numListItemID NOT IN (
                                SELECT  AB.numAuthoritativeSales
                                FROM    AuthoritativeBizDocs AB
                                WHERE   AB.numDomainID = @numDomainID
                                UNION
                                SELECT  AB.numAuthoritativePurchase
                                FROM    AuthoritativeBizDocs AB
                                WHERE   AB.numDomainID = @numDomainID )
                        ORDER BY tintType,
                                numPageNavID
                    END
            END
        ELSE 
            BEGIN
				
                SELECT  *
                FROM    ( SELECT    TNA.numPageAuthID,
                                    TNA.numGroupID,
                                    TNA.numTabID,
                                    TNA.numPageNavID,
                                    TNA.bitVisible,
                                    TNA.numDomainID,
                                    ISNULL(PND.vcPageNavName, '') AS [vcNodeName],
                                    tintType
                          FROM      PageNavigationDTL PND
                                    JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
                          WHERE     1 = 1
                                    AND ISNULL(TNA.bitVisible, 0) = 1
                                    AND ISNULL(PND.bitVisible, 0) = 1
                                    AND numModuleID = @numModuleID
                                    AND TNA.numDomainID = @numDomainID
                                    AND numGroupID = @numGroupID
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    1111 numPageNavID,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    'Regular Documents' AS [vcNodeName],
                                    1 tintType
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    Ld.numListItemId,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    ISNULL(vcData, '') AS [vcNodeName],
                                    1 tintType
                          FROM      listdetails Ld
                                    LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
                                                              AND Lo.numDomainId = @numDomainID
                          WHERE     Ld.numListID = 29
                                    AND ( constFlag = 1
                                          OR Ld.numDomainID = @numDomainID
                                        )
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    ld.numListItemID numPageNavID,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    ISNULL(vcData, '') AS [vcNodeName],
                                    1 tintType
                          FROM      listdetails Ld
                                    LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
                                                              AND lo.numDomainId = @numDomainID
                                    INNER JOIN AuthoritativeBizDocs AB ON ( LD.numListItemID = AB.numAuthoritativeSales )
                          WHERE     LD.numListID = 27
                                    AND ( constFlag = 1
                                          OR LD.numDomainID = @numDomainID
                                        )
                                    AND AB.numDomainID = @numDomainID
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    ld.numListItemID numPageNavID,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    ISNULL(vcData, '') AS [vcNodeName],
                                    1 tintType
                          FROM      listdetails Ld
                                    LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
                                                              AND lo.numDomainId = @numDomainID
                                    INNER JOIN AuthoritativeBizDocs AB ON ( ld.numListItemID = AB.numAuthoritativePurchase )
                          WHERE     LD.numListID = 27
                                    AND ( constFlag = 1
                                          OR LD.numDomainID = @numDomainID
                                        )
                                    AND AB.numDomainID = @numDomainID
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    0 numPageNavID,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    ISNULL(LT.vcData, '') AS [vcNodeName],
                                    1 tintType
                          FROM      ListDetails LT,
                                    listdetails ld
                          WHERE     LT.numListID = 11
                                    AND Lt.numDomainID = @numDomainID
                                    AND LD.numListID = 27
                                    AND ( ld.constFlag = 1
                                          OR LD.numDomainID = @numDomainID
                                        )
                                    AND ( LD.numListItemID IN (
                                          SELECT    AB.numAuthoritativeSales
                                          FROM      AuthoritativeBizDocs AB
                                          WHERE     AB.numDomainId = @numDomainID )
                                          OR ld.numListItemID IN (
                                          SELECT    AB.numAuthoritativePurchase
                                          FROM      AuthoritativeBizDocs AB
                                          WHERE     AB.numDomainId = @numDomainID )
                                        )
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    2222 numPageNavID,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    'Other BizDocs' AS [vcNodeName],
                                    1 tintType
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    ld.numListItemID numPageNavID,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    ISNULL(vcData, '') AS [vcNodeName],
                                    1 tintType
                          FROM      listdetails Ld
                          WHERE     ld.numListID = 27
                                    AND ( constFlag = 1
                                          OR LD.numDomainID = @numDomainID
                                        )
                                    AND LD.numListItemID NOT IN (
                                    SELECT  AB.numAuthoritativeSales
                                    FROM    AuthoritativeBizDocs AB
                                    WHERE   AB.numDomainID = @numDomainID )
                                    AND LD.numListItemID NOT IN (
                                    SELECT  AB.numAuthoritativePurchase
                                    FROM    AuthoritativeBizDocs AB
                                    WHERE   AB.numDomainID = @numDomainID )
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    ls.numListItemID numPageNavID,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    ISNULL(ls.vcData, '') AS [vcNodeName],
                                    1 tintType
                          FROM      listdetails Ld,
                                    ( SELECT    *
                                      FROM      ListDetails LS
                                      WHERE     LS.numDomainID = @numDomainID
                                                AND LS.numListID = 11
                                    ) LS
                          WHERE     ld.numListID = 27
                                    AND ( ld.constFlag = 1
                                          OR LD.numDomainID = @numDomainID
                                        )
                                    AND LD.numListItemID NOT IN (
                                    SELECT  AB.numAuthoritativeSales
                                    FROM    AuthoritativeBizDocs AB
                                    WHERE   AB.numDomainID = @numDomainID
                                    UNION
                                    SELECT  AB.numAuthoritativePurchase
                                    FROM    AuthoritativeBizDocs AB
                                    WHERE   AB.numDomainID = @numDomainID )
                        ) PageNavTable
                        JOIN dbo.TreeNavigationAuthorization TreeNav ON PageNavTable.numPageNavID = TreeNav.numPageNavID
                                                                        AND PageNavTable.numGroupID = TreeNav.numGroupID
                                                                        AND PageNavTable.numTabID = TreeNav.numTabID
                ORDER BY TreeNav.tintType,
                        TreeNav.numPageNavID

            END

        SELECT  *
        FROM    ( SELECT    ISNULL(( SELECT TOP 1
                                            TNA.[numPageAuthID]
                                     FROM   dbo.TreeNavigationAuthorization TNA
                                     WHERE  TNA.numPageNavID = PND.[numPageNavID]
                                            AND TNA.numDomainID = @numDomainID
                                            AND TNA.numGroupID = @numGroupID
                                            AND TNA.numTabID = @numTabID
                                   ), 0) AS [numPageAuthID],
                            ISNULL(@numGroupID, 0) AS [numGroupID],
                            ISNULL(PND.[numTabID], 0) AS [numTabID],
                            ISNULL(PND.[numPageNavID], 0) AS [numPageNavID],
                            ISNULL(( SELECT TOP 1
                                            TNA.[bitVisible]
                                     FROM   dbo.TreeNavigationAuthorization TNA
                                     WHERE  TNA.numPageNavID = PND.[numPageNavID]
                                            AND TNA.numDomainID = @numDomainID
                                            AND TNA.numGroupID = @numGroupID
                                            AND TNA.numTabID = @numTabID
                                   ), 0) AS [bitVisible],
                            @numDomainID AS [numDomainID],
                            ( SELECT TOP 1
                                        vcPageNavName
                              FROM      dbo.PageNavigationDTL
                              WHERE     PageNavigationDTL.numPageNavID = PND.numPageNavID
                            ) AS [vcNodeName],
                            ISNULL(( SELECT TOP 1
                                            TNA.tintType
                                     FROM   dbo.TreeNavigationAuthorization TNA
                                     WHERE  TNA.numPageNavID = PND.[numPageNavID]
                                            AND TNA.numDomainID = @numDomainID
                                            AND TNA.numGroupID = @numGroupID
                                            AND TNA.numTabID = @numTabID
                                   ), 1) AS [tintType]
                  FROM      dbo.PageNavigationDTL PND
                  WHERE     ISNULL(PND.bitVisible, 0) = 1
                            AND PND.numTabID = @numTabID
                            AND numModuleID = @numModuleID
                  UNION ALL
                  SELECT    ISNULL([numPageAuthID], 0) AS [numPageAuthID],
                            @numGroupID AS [numGroupID],
                            @numTabID AS [numTabID],
                            ISNULL(LD.[numListItemID], 0) AS [numPageNavID],
                            ISNULL(TNA.[bitVisible], 0) AS [bitVisible],
                            ISNULL(LD.[numDomainID], 0) AS [numDomainID],
                            vcData AS [vcNodeName],
                            1 AS [tintType]
                  FROM      dbo.ListDetails LD
                            LEFT JOIN dbo.TreeNavigationAuthorization TNA ON LD.numListItemID = TNA.numPageNavID
                  WHERE     numListID = 27
                            AND LD.numDomainID = @numDomainID
                            AND ISNULL(constFlag, 0) = 0
                            AND numTabID = ( CASE WHEN @numModuleID = 10
                                                  THEN 1
                                                  ELSE 0
                                             END )
                  UNION ALL
                  SELECT    ISNULL([numPageAuthID], 0) AS [numPageAuthID],
                            @numGroupID AS [numGroupID],
                            @numTabID AS [numTabID],
                            ISNULL(LD.[numListItemID], 0) AS [numPageNavID],
                            ISNULL(TNA.[bitVisible], 0) AS [bitVisible],
                            ISNULL(TNA.[numDomainID], 0) AS [numDomainID],
                            vcData AS [vcNodeName],
                            1 AS [tintType]
                  FROM      dbo.ListDetails LD
                            LEFT JOIN dbo.TreeNavigationAuthorization TNA ON LD.numListItemID = TNA.numPageNavID
                                                                             AND ISNULL(TNA.[numDomainID], 0) = @numDomainID
                  WHERE     numListID = 27
                            AND ISNULL(constFlag, 0) = 1
                            AND numTabID = @numTabID
                ) TABLE2
        ORDER BY numPageNavID  
    END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetShippingBox')
DROP PROCEDURE USP_GetShippingBox
GO
CREATE PROCEDURE USP_GetShippingBox
    @ShippingReportId NUMERIC(9)
AS 
    BEGIN
        SELECT DISTINCT
                [numBoxID],
                [vcBoxName],
                [numShippingReportId],
                CASE WHEN [fltTotalWeight] = 0 THEN SUM([fltWeight]) ELSE [fltTotalWeight] END AS fltTotalWeight,
                [fltHeight],
                [fltWidth],
                [fltLength],
				dbo.FormatedDateFromDate(dtDeliveryDate,numDomainID) dtDeliveryDate,
				ISNULL(monShippingRate,0) [monShippingRate],
				vcShippingLabelImage,
				vcTrackingNumber,
				tintServiceType,--Your Packaging
				tintPayorType,
				vcPayorAccountNo,
				vcPayorZip,
				numPayorCountry,
				numPackageTypeID,
				ISNULL(vcPackageName,'') AS [vcPackageName],
				numServiceTypeID,
				CAST(ROUND(
				(CASE WHEN SUM(fltDimensionalWeight) > fltTotalRegularWeight
				      THEN SUM(fltDimensionalWeight) 
				      ELSE fltTotalRegularWeight
				END), 2) AS DECIMAL(9,2)) AS [fltTotalWeightForShipping],
				SUM(fltDimensionalWeight) AS [fltTotalDimensionalWeight],
				ISNULL(fltDimensionalWeight,0) AS [fltDimensionalWeight],
				ISNULL(numShipCompany,0) AS [numShipCompany],
				numOppID
--				,intBoxQty
				--ShippingReportItemId
        FROM    View_ShippingBox
        WHERE   [numShippingReportId] = @ShippingReportId
        GROUP BY [numBoxID],
                [vcBoxName],
                [numShippingReportId],
                [fltHeight],
                [fltWidth],
                [fltLength],
                fltTotalWeight,dtDeliveryDate,numOppID,View_ShippingBox.numOppBizDocID,
				monShippingRate,vcShippingLabelImage,
				vcTrackingNumber,numDomainID,
				tintServiceType,tintPayorType,
				vcPayorAccountNo,
				vcPayorZip,
				numPayorCountry,numPackageTypeID,vcPackageName,numServiceTypeID,fltDimensionalWeight,fltTotalRegularWeight,numShipCompany--,intBoxQty
				                
        SELECT  [numBoxID],
                [vcBoxName],
                [ShippingReportItemId],
                [numShippingReportId],
                fltTotalWeightItem [fltTotalWeight],
                fltHeightItem [fltHeight],
                fltWidthItem [fltWidth],
                fltLengthItem [fltLength],
                [numOppBizDocItemID],
                [numItemCode],
                [numoppitemtCode],
                ISNULL([vcItemName],'') vcItemName,
                [vcModelID],
                CASE WHEN LEN (ISNULL(vcItemDesc,''))> 100 THEN  CONVERT(VARCHAR(100),[vcItemDesc]) + '..'
                ELSE ISNULL(vcItemDesc,'') END vcItemDesc,
                intBoxQty,
                numServiceTypeID,
                vcUnitName,monUnitPrice,
				numOppID
        FROM    [View_ShippingBox]
        WHERE   [numShippingReportId] = @ShippingReportId        
                
                
    END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Usp_GetShippingDetailsForOrder')
DROP PROCEDURE Usp_GetShippingDetailsForOrder
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- EXEC Usp_GetShippingDetailsForOrder 95379,170
CREATE PROCEDURE Usp_GetShippingDetailsForOrder
(
	@numOppID		NUMERIC(18),
	@numDomainID	NUMERIC(18)
)
AS 
BEGIN
	SELECT TOP 1 OM.numOppId,OBD.numBizDocId,OBD.numOppBizDocsId, SR.numShippingReportId, SR.numShippingCompany, 
	STUFF ((SELECT ',' + ISNULL(vcTrackingNumber,'') FROM dbo.ShippingBox WHERE numShippingReportId = SR.numShippingReportId FOR XML PATH('')), 1, 1, '') [vcTrackingNumber],
--	CONVERT(VARCHAR(20),(SELECT COUNT(numBoxID) FROM dbo.ShippingBox WHERE numShippingReportId = SR.numShippingReportId)) + ' (' + 
--	(
--		STUFF ((SELECT ', ' + CONVERT(VARCHAR(20),ISNULL(fltLength,0)) + 'x' +  CONVERT(VARCHAR(20),ISNULL(fltWidth,0)) + 'x' + CONVERT(VARCHAR(20),ISNULL(fltHeight,0)) FROM dbo.ShippingBox WHERE numShippingReportId = SR.numShippingReportId FOR XML PATH('')), 1, 1, '')	
--	) + ')' AS [vcBoxDimensions],
	CONVERT(VARCHAR(20),(SELECT COUNT(numBoxID) FROM dbo.ShippingBox WHERE numShippingReportId = SR.numShippingReportId)) AS [numBoxCount],
	CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingReport WHERE dbo.ShippingReport.numOppBizDocId = OBD.numOppBizDocsId AND numDomainID = @numDomainID) > 0 THEN 1 
		 ELSE 0 
	END AS [IsShippingReportGenerated],
	CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingBox 
			   WHERE numShippingReportId IN (SELECT numShippingReportId FROM dbo.ShippingReport WHERE numOppBizDocId = OBD.numOppBizDocsId AND numDomainID = @numDomainID)
			   AND LEN(ISNULL(vcTrackingNumber,'')) > 0) > 0 THEN 1 
		 ELSE 0 
	END AS [ISTrackingNumGenerated],
	LD.vcData as BizDoc,
	CASE WHEN tintOppType = 1  THEN (CASE WHEN ISNULL(numAuthoritativeSales,0) = numBizDocId THEN 'Authoritative' + CASE WHEN ISNULL(OBD.tintDeferred,0) = 1 THEN ' (Deferred)' 
																														 ELSE '' 
																													END 
									 ELSE 'Non-Authoritative' 
									 END ) 
		 WHEN tintOppType = 2  THEN (CASE WHEN ISNULL(numAuthoritativePurchase,0) = numBizDocId THEN 'Authoritative' + CASE WHEN ISNULL(OBD.tintDeferred,0) = 1 THEN ' (Deferred)' 
																															ELSE '' 
																													   END 
										  ELSE 'Non-Authoritative' 
									 END ) 
	END AS BizDocType,
	ISNULL(vcBizDocID,'') [vcBizDocID],
	(
		SELECT SUM(fltTotalWeight) FROM dbo.ShippingBox SBOX WHERE SBOX.numShippingReportId = SR.numShippingReportId
		GROUP BY SBOX.numShippingReportId
	) AS [BoxWeight],
	(
		STUFF ((SELECT '~' + ISNULL(dbo.ShippingBox.vcBoxName,'') + '- Weight: ' + CONVERT(VARCHAR(20),ISNULL(fltTotalWeight,0)) + ' lbs, LxBxH: ' + CONVERT(VARCHAR(20),ISNULL(fltLength,0)) + 'x' +  CONVERT(VARCHAR(20),ISNULL(fltWidth,0)) + 'x' + CONVERT(VARCHAR(20),ISNULL(fltHeight,0)) FROM dbo.ShippingBox WHERE numShippingReportId = SR.numShippingReportId FOR XML PATH('')), 1, 1, '') 
	) AS [WeightPerBox],
	STUFF ((SELECT ', ' + vcBizDocID + '~' + CONVERT(VARCHAR(10), numOppBizDocsId) FROM dbo.OpportunityBizDocs OppBiz WHERE OppBiz.numOppId = OM.numOppId  FOR XML PATH('')), 1, 1, '') [vcBizDocs]
	FROM dbo.OpportunityBizDocs OBD
	JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
	JOIN dbo.ShippingReport SR ON OBD.numOppBizDocsId = SR.numOppBizDocId AND SR.numOppID = OM.numOppId
	LEFT JOIN ListDetails LD on LD.numListItemID = OBD.numBizDocId 
	LEFT JOIN AuthoritativeBizDocs AB on AB.numDomainId = OM.numDomainID  
	WHERE OM.numDomainId = @numDomainID
	AND OM.numOppId = @numOppID
	ORDER BY SR.numShippingReportId DESC
END

/****** Object:  StoredProcedure [dbo].[USP_GetShippingReport]    Script Date: 05/07/2009 17:34:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SELECT * FROM ShippingReport
--SELECT * FROM ShippingReportItems
-- USP_GetShippingReport 1,60754,792
-- exec USP_GetShippingReport @numDomainID=72,@numOppBizDocId=5465,@ShippingReportId=4
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetShippingReport' ) 
    DROP PROCEDURE USP_GetShippingReport
GO
CREATE PROCEDURE [dbo].[USP_GetShippingReport]
    @numDomainID NUMERIC(9),
    @numOppBizDocId NUMERIC(9),
    @ShippingReportId NUMERIC(9)
AS 
    BEGIN
  
        SELECT  SRI.numItemCode,
                I.vcItemName,
                I.vcModelID,
                CASE WHEN LEN(BDI.vcItemDesc) > 100
                     THEN CONVERT(VARCHAR(100), BDI.vcItemDesc) + '..'
                     ELSE BDI.vcItemDesc
                END vcItemDesc,
                dbo.FormatedDateFromDate(SRI.dtDeliveryDate, @numDomainID) dtDeliveryDate,
                SRI.tintServiceType,
                ISNULL(I.fltWeight, 0) AS [fltTotalWeight],
                ISNULL(I.[fltHeight], 0) AS [fltHeight],
                ISNULL(I.[fltLength], 0) AS [fltLength],
                ISNULL(I.[fltWidth], 0) AS [fltWidth],
                ISNULL(SRI.monShippingRate,(SELECT monTotAmount FROM dbo.OpportunityItems 
											WHERE SR.numOppID = dbo.OpportunityItems.numOppId
											AND numItemCode = (SELECT numShippingServiceITemID FROM dbo.Domain WHERE numDomainId = @numDomainId))) [monShippingRate],
                SB.vcShippingLabelImage,
                SB.vcTrackingNumber,
                SRI.ShippingReportItemId,
                SRI.intNoOfBox,
                SR.[vcValue3],
                SR.[vcValue4],
                CASE WHEN ISNUMERIC(SR.vcFromState) = 1
                     THEN ( SELECT  vcStateCode
                            FROM    ShippingStateMaster
                            WHERE   vcStateName = ( SELECT TOP 1
                                                            vcState
                                                    FROM    State
                                                    WHERE   numStateID = SR.vcFromState
                                                  )
                                    AND [numShipCompany] = SR.[numShippingCompany]
                          )
                     ELSE ''
                END vcFromState,
                CASE WHEN ISNUMERIC(SR.vcFromCountry) = 1
                     THEN ( SELECT  vcCountryCode
                            FROM    ShippingCountryMaster
                            WHERE   vcCountryName = ( SELECT TOP 1
                                                                vcData
                                                      FROM      ListDetails
                                                      WHERE     numListItemID = SR.vcFromCountry
                                                    )
                                    AND [numShipCompany] = SR.[numShippingCompany]
                          )
                     ELSE ''
                END vcFromCountry,
                CASE WHEN ISNUMERIC(SR.vcToState) = 1
                     THEN ( SELECT  vcStateCode
                            FROM    ShippingStateMaster
                            WHERE   vcStateName = ( SELECT TOP 1
                                                            vcState
                                                    FROM    State
                                                    WHERE   numStateID = SR.vcToState
                                                  )
                                    AND [numShipCompany] = SR.[numShippingCompany]
                          )
                     ELSE ''
                END vcToState,
                CASE WHEN ISNUMERIC(SR.vcToCountry) = 1
                     THEN ( SELECT  vcCountryCode
                            FROM    ShippingCountryMaster
                            WHERE   vcCountryName = ( SELECT TOP 1
                                                                vcData
                                                      FROM      ListDetails
                                                      WHERE     numListItemID = SR.vcToCountry
                                                    )
                                    AND [numShipCompany] = SR.[numShippingCompany]
                          )
                     ELSE ''
                END vcToCountry,
                SR.vcToZip,
                SR.vcFromZip,
                SR.[numShippingCompany],
                SB.[numBoxID],
                SB.[vcBoxName],
                BDI.numOppBizDocItemID,
                SR.tintPayorType,
                ISNULL(SR.vcPayorAccountNo, '') vcPayorAccountNo,
                ISNULL(SR.numPayorCountry, 0) numPayorCountry,
                ( SELECT    vcCountryCode
                  FROM      ShippingCountryMaster
                  WHERE     vcCountryName = ( SELECT TOP 1
                                                        vcData
                                              FROM      ListDetails
                                              WHERE     numListItemID = SR.numPayorCountry
                                            )
                            AND [numShipCompany] = SR.[numShippingCompany]
                ) vcPayorCountryCode,
                SR.vcPayorZip,
                SR.vcFromCity,
                SR.vcFromAddressLine1,
                SR.vcFromAddressLine2,
                ISNULL(SR.vcFromCountry,0) [numFromCountry],
                ISNULL(SR.vcFromState,0) [numFromState],
                SR.vcToCity,
                SR.vcToAddressLine1,
                SR.vcToAddressLine2,
                ISNULL(SR.vcToCountry,0) [numToCountry],
                ISNULL(SR.vcToState,0) [numToState],
                SRI.intBoxQty,
                ISNULL(SB.numServiceTypeID, 0) AS [numServiceTypeID],
                ISNULL(SR.vcFromCompany, '') AS vcFromCompany,
                ISNULL(SR.vcFromName, '') AS vcFromName,
                ISNULL(SR.vcFromPhone, '') AS vcFromPhone,
                ISNULL(SR.vcToCompany, '') AS vcToCompany,
                ISNULL(SR.vcToName, '') AS vcToName,
                ISNULL(SR.vcToPhone, '') AS vcToPhone,
                ISNULL(SB.fltDimensionalWeight, 0) AS [fltDimensionalWeight],
                dbo.fn_GetContactName(SR.numCreatedBy) + ' : '
                + CONVERT(VARCHAR(20), SR.dtCreateDate) AS [CreatedDetails],
                CASE WHEN SR.numShippingCompany = 91 THEN 'Fedex'
                     WHEN SR.numShippingCompany = 88 THEN 'UPS'
                     WHEN SR.numShippingCompany = 90 THEN 'USPS'
                     ELSE 'Other'
                END AS [vcShipCompany],
                CAST(ROUND(( ( SELECT   SUM(fltTotalWeight)
                               FROM     dbo.ShippingReportItems
                               WHERE    numShippingReportId = SR.numShippingReportId
                             ) * intBoxQty ), 2) AS DECIMAL(9, 2)) AS [fltTotalRegularWeight],
                CAST(ROUND(( SELECT SUM(fltDimensionalWeight)
                             FROM   dbo.ShippingBox
                             WHERE  numShippingReportId = SR.numShippingReportId
                           ), 2) AS DECIMAL(9, 2)) AS [fltTotalDimensionalWeight],
                ISNULL(monTotAmount, 0) AS [monTotAmount],
                ISNULL(bitFromResidential,0) [bitFromResidential],
                ISNULL(bitToResidential,0) [bitToResidential],
                ISNULL(IsCOD,0) [IsCOD],
				ISNULL(IsDryIce,0) [IsDryIce],
				ISNULL(IsHoldSaturday,0) [IsHoldSaturday],
				ISNULL(IsHomeDelivery,0) [IsHomeDelivery],
				ISNULL(IsInsideDelevery,0) [IsInsideDelevery],
				ISNULL(IsInsidePickup,0) [IsInsidePickup],
				ISNULL(IsReturnShipment,0) [IsReturnShipment],
				ISNULL(IsSaturdayDelivery,0) [IsSaturdayDelivery],
				ISNULL(IsSaturdayPickup,0) [IsSaturdayPickup],
				ISNULL(IsAdditionalHandling,0) [IsAdditionalHandling],
				ISNULL(vcCODType,0) [vcCODType],
				numOppID,
				ISNULL(numCODAmount,0) [numCODAmount]
        FROM    ShippingReport AS SR
                INNER JOIN ShippingBox SB ON SB.numShippingReportId = SR.numShippingReportId
                INNER JOIN ShippingReportItems AS SRI ON SR.numShippingReportId = SRI.numShippingReportId
                                                         AND SRI.numBoxID = SB.numBoxID
                INNER JOIN OpportunityBizDocItems BDI ON SRI.numItemCode = BDI.numItemCode
                                                         AND SR.numOppBizDocId = BDI.numOppBizDocID
                INNER JOIN Item I ON BDI.numItemCode = I.numItemCode
        WHERE   SR.[numOppBizDocId] = @numOppBizDocId
                AND SR.numDomainId = @numDomainID
                AND SR.[numShippingReportId] = @ShippingReportId
--           AND (SB.bitIsMasterTrackingNo = 1 OR SB.bitIsMasterTrackingNo IS NULL)
           
        SELECT  vcBizDocID
        FROM    [OpportunityBizDocs]
        WHERE   [numOppBizDocsId] = @numOppBizDocId

    END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetUserSMTPDetails')
DROP PROCEDURE USP_GetUserSMTPDetails
GO
CREATE PROCEDURE [dbo].[USP_GetUserSMTPDetails]                            
 @numUserID NUMERIC(9 )= 0,
 @numDomainID NUMERIC(9)
--                          
AS                            
BEGIN                            
  SELECT       
 S.[numDomainId]
      ,S.[numUserId]
      ,S.[vcSMTPUserName]
      ,S.[vcSMTPPassword]
      ,S.[vcSMTPServer]
      ,S.[numSMTPPort]
      ,S.[bitSMTPSSL]
      ,S.[bitSMTPServer]
      ,S.[bitSMTPAuth]
FROM SMTPUserDetails S JOIN Domain D on D.numDomainID =  S.numDomainID      
   WHERE  S.numUserId=@numUserID AND S.numDomainID=@numDomainID                            
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetUsersWithDomains]    Script Date: 07/26/2008 16:18:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getuserswithdomains')
DROP PROCEDURE usp_getuserswithdomains
GO
CREATE PROCEDURE [dbo].[usp_GetUsersWithDomains]                            
 @numUserID NUMERIC(9 )= 0,
 @numDomainID NUMERIC(9)
--                          
AS                            
BEGIN                            
 IF @numUserID = 0 AND @numDomainID > 0 --Check if request is made while session is on
 BEGIN                            
  SELECT numUserID, vcUserName,UserMaster.numGroupID,isnull(vcGroupName,'-')as vcGroupName,              
 vcUserDesc,ISNULL(ADC.vcfirstname,'') +' '+ ISNULL(ADC.vclastname,'') as Name,               
 UserMaster.numDomainID, vcDomainName, UserMaster.numUserDetailId ,isnull(UserMaster.SubscriptionId,'') as SubscriptionId
,isnull(UserMaster.tintDefaultRemStatus,0) tintDefaultRemStatus,isnull(UserMaster.numDefaultTaskType,0) numDefaultTaskType,
ISNULL(UserMaster.bitOutlook,0) bitOutlook
   FROM UserMaster                      
   join Domain                        
   on UserMaster.numDomainID =  Domain.numDomainID                       
   left join AdditionalContactsInformation ADC                      
   on ADC.numContactid=UserMaster.numUserDetailId                     
   left join AuthenticationGroupMaster GM                     
   on Gm.numGroupID= UserMaster.numGroupID                    
   where tintGroupType=1 -- Application Users                    
   ORDER BY  Domain.numDomainID, vcUserDesc                            
 END   
 ELSE IF @numUserID = -1 AND @numDomainID > 0 --Support Email
 BEGIN                            
  SELECT '' AS vcEmailId,vcImapUserName,isnull(bitUseUserName,0) bitUseUserName, isnull(ImapUserDTL.bitImap,0) bitImap,  
	ImapUserDTL.vcImapServerUrl,  
	isnull(ImapUserDTL.vcImapPassword,'') vcImapPassword,  
	ImapUserDTL.bitSSl,  
	ImapUserDTL.numPort 
FROM [ImapUserDetails] ImapUserDTL   
where ImapUserDTL.numUserCntId = @numUserID 
AND ImapUserDTL.numDomainID = @numDomainID

 END                         
 ELSE                            
 BEGIN                            
  SELECT       
 U.numUserID, vcUserName,numGroupID,vcUserDesc, U.numDomainID,vcDomainName, U.numUserDetailId,bitHourlyRate,bitActivateFlag,      
 monHourlyRate,bitSalary,numDailyHours,bitOverTime,numLimDailHrs,monOverTimeRate,bitMainComm,fltMainCommPer,      
 bitRoleComm,(select count(*) from UserRoles where UserRoles.numUserCntID=U.numUserDetailId)  as Roles ,vcEmailid,      
 vcPassword,isnull(ExcUserDTL.bitExchangeIntegration,0) bitExchangeIntegration, isnull(ExcUserDTL.bitAccessExchange,0) bitAccessExchange,       
 isnull(ExcUserDTL.vcExchPassword,'') as vcExchPassword, isnull(ExcUserDTL.vcExchPath,'') as vcExchPath ,       
 isnull(ExcUserDTL.vcExchDomain,'')  as vcExchDomain   ,isnull(U.SubscriptionId,'') as SubscriptionId ,  
 isnull(ImapUserDTL.bitImap,0) bitImap,  
	ImapUserDTL.vcImapServerUrl,  
	isnull(ImapUserDTL.vcImapPassword,'') vcImapPassword,  
	ImapUserDTL.bitSSl,  
	ImapUserDTL.numPort 
	,isnull(bitSMTPAuth,0) bitSMTPAuth
      ,isnull([vcSmtpPassword],'')vcSmtpPassword
      ,isnull([vcSMTPServer],0) vcSMTPServer
      ,isnull(numSMTPPort,0)numSMTPPort
      ,isnull(bitSMTPSSL,0) bitSMTPSSL
,	isnull(bitSMTPServer,0)bitSMTPServer
    ,isnull(bitUseUserName,0) bitUseUserName
,isnull(U.tintDefaultRemStatus,0) tintDefaultRemStatus,isnull(U.numDefaultTaskType,0) numDefaultTaskType,
ISNULL(U.bitOutlook,0) bitOutlook,
ISNULL(numDefaultClass,0) numDefaultClass,isnull(numDefaultWarehouse,0) numDefaultWarehouse
FROM UserMaster U                        
 join Domain D                       
 on   U.numDomainID =  D.numDomainID       
left join  ExchangeUserDetails ExcUserDTL      
 on ExcUserDTL.numUserID=U.numUserID                      
left join  [ImapUserDetails] ImapUserDTL   
on ImapUserDTL.numUserCntId = U.numUserDetailId   
AND ImapUserDTL.numDomainID = D.numDomainID
   WHERE  U.numUserID=@numUserID                            
 END                            
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetWareHouseItems]    Script Date: 07/26/2008 16:18:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_GetWareHouseItems 6                            
--created by anoop jayaraj                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getwarehouseitems')
DROP PROCEDURE usp_getwarehouseitems
GO
CREATE PROCEDURE [dbo].[USP_GetWareHouseItems]                              
@numItemCode as numeric(9)=0,
@byteMode as tinyint=0                              
as                              
                              
                          
                              
declare @bitSerialize as bit                      
declare @str as nvarchar(max)                 
declare @str1 as nvarchar(max)               
declare @ColName as varchar(25)                      
set @str=''                       
                        
DECLARE @bitKitParent BIT
declare @numItemGroupID as numeric(9)                        
                        
select @numItemGroupID=numItemGroup,@bitSerialize=CASE WHEN bitSerialized=0 THEN bitLotNo ELSE bitSerialized END,
@bitKitParent = ( CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END ) from Item where numItemCode=@numItemCode                        
if @bitSerialize=1 set @ColName='numWareHouseItmsDTLID,1'              
else set @ColName='numWareHouseItemID,0'              
              
--Create a Temporary table to hold data                                                            
create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                      
 numCusFlDItemID numeric(9)                                                         
 )                         
                        
insert into #tempTable                         
(numCusFlDItemID)                                                            
select distinct(numOppAccAttrID) from ItemGroupsDTL where numItemGroupID=@numItemGroupID and tintType=2                       
                          
                 
 declare @ID as numeric(9)                        
 declare @numCusFlDItemID as varchar(20)                        
 declare @fld_label as varchar(100),@fld_type as varchar(100)                        
 set @ID=0                        
 select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label,@fld_type=fld_type from #tempTable                         
 join CFW_Fld_Master on numCusFlDItemID=Fld_ID                        
                         
 while @ID>0                        
 begin                        
                          
   set @str=@str+',  dbo.GetCustFldItems('+@numCusFlDItemID+',9,'+@ColName+') as ['+ @fld_label+']'
	
	IF @byteMode=1                                        
		set @str=@str+',  dbo.GetCustFldItemsValue('+@numCusFlDItemID+',9,'+@ColName+','''+@fld_type+''') as ['+ @fld_label+'Value]'                                        
                          
   select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempTable                         
   join CFW_Fld_Master on numCusFlDItemID=Fld_ID and ID >@ID                        
   if @@rowcount=0 set @ID=0                        
                          
 end                        
                       
--      

DECLARE @KitOnHand AS NUMERIC(9);SET @KitOnHand=0

--IF @bitKitParent=1
	--SELECT @KitOnHand=ISNULL(dbo.fn_GetKitInventory(@numItemCode),0)          
  
set @str1='select '

IF @byteMode=1                                        
		set @str1 =@str1 +'I.numItemCode,'
	
set @str1 =@str1 + 'numWareHouseItemID,isnull(vcWarehouse,'''') + '': '' + isnull(WL.vcLocation,'''') vcWarehouse,W.numWareHouseID,
Case when @bitKitParent=1 then ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) else isnull(numOnHand,0) end as [OnHand],
CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numPurchaseUnit,0)) * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as numeric(18,2)) as PurchaseOnHand,
CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numSaleUnit,0)) * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as numeric(18,2)) as SalesOnHand,
Case when @bitKitParent=1 THEN 0 ELSE isnull(numReorder,0) END as [Reorder] 
,Case when @bitKitParent=1 THEN 0 ELSE isnull(numOnOrder,0) END as [OnOrder]
,Case when @bitKitParent=1 THEN 0 ELSE isnull(numAllocation,0) END as [Allocation] 
,Case when @bitKitParent=1 THEN 0 ELSE isnull(numBackOrder,0) END as [BackOrder]
,0 as Op_Flag , isnull(WL.vcLocation,'''') Location,isnull(WL.numWLocationID,0) as numWLocationID,
round(isnull(monWListPrice,0),2) Price,isnull(vcWHSKU,'''') as SKU,isnull(vcBarCode,'''') as BarCode '+ case when @bitSerialize=1 then  ' ' else  @str end +'                   
,(SELECT CASE WHEN ISNULL(subI.bitKitParent,0) = 1 AND ISNULL((SELECT COUNT(*) FROM OpportunityItems 
															   WHERE OpportunityItems.numItemCode = WareHouseItems.numItemID
															   AND WareHouseItems.numWareHouseItemID = OpportunityItems.numWarehouseItmsID),0) = 0 THEN 1
			ELSE 0 
	    END 
FROM Item subI WHERE subI.numItemCode = WareHouseItems.numItemID) [IsDeleteKitWarehouse],@bitKitParent [bitKitParent]
from WareHouseItems                           
join Warehouses W                             
on W.numWareHouseID=WareHouseItems.numWareHouseID 
left join WarehouseLocation WL on WL.numWLocationID =WareHouseItems.numWLocationID
join Item I on I.numItemCode=WareHouseItems.numItemID and WareHouseItems.numDomainId=I.numDomainId
where numItemID='+convert(varchar(15),@numItemCode)                  
   
print(@str1)           

EXECUTE sp_executeSQL @str1, N'@bitKitParent bit',@bitKitParent
                       
                        
set @str1='select numWareHouseItmsDTLID,WDTL.numWareHouseItemID, vcSerialNo,0 as Op_Flag,WDTL.vcComments as Comments,WDTL.numQty,WDTL.numQty as OldQty,W.vcWarehouse '+ case when @bitSerialize=1 then @str else '' end +'
--,WDTL.dExpirationDate 
from WareHouseItmsDTL WDTL                             
join WareHouseItems WI                             
on WDTL.numWareHouseItemID=WI.numWareHouseItemID                              
join Warehouses W                             
on W.numWareHouseID=WI.numWareHouseID  
where (tintStatus is null or tintStatus=0)  and  numItemID='+ convert(varchar(15),@numItemCode)                          
print @str1                       
exec (@str1)                       
                      
                      
select Fld_label,fld_id,fld_type,numlistid,vcURL from #tempTable                         
 join CFW_Fld_Master on numCusFlDItemID=Fld_ID                      
drop table #tempTable


-----------------For Kendo UI testting purpose
--create table #tempColumnConfig ( field varchar(100),                                                                      
-- title varchar(100) , format varchar(100)                                                      
-- )  
--
--insert into #tempColumnConfig
--	select 'vcWarehouse','WareHouse','' union all
--	select 'OnHand','On Hand','' union all
--	select 'Reorder','Re Order','' union all
--	select 'Allocation','Allocation','' union all
--	select 'BackOrder','Back Order','' union all
--	select 'Price','Price','{0:c}'
--
--select * from #tempColumnConfig
--
--drop table #tempColumnConfig

-----------------

--set @str1='select case when COUNT(*)=1 then MIN(numWareHouseItmsDTLID) else 0 end as numWareHouseItmsDTLID,MIN(WDTL.numWareHouseItemID) as numWareHouseItemID, vcSerialNo,0 as Op_Flag,WDTL.vcComments as Comments'+ case when @bitSerialize=1 then @str else '' end +',W.vcWarehouse,COUNT(*) Qty
--from WareHouseItmsDTL WDTL                             
--join WareHouseItems WI                             
--on WDTL.numWareHouseItemID=WI.numWareHouseItemID                              
--join Warehouses W                             
--on W.numWareHouseID=WI.numWareHouseID  
--where (tintStatus is null or tintStatus=0)  and  numItemID='+ convert(varchar(15),@numItemCode) +'
--GROUP BY vcSerialNo,WDTL.vcComments,W.vcWarehouse'
--                          
--print @str1                       
--exec (@str1)                       
              
GO
/****** Object:  StoredProcedure [dbo].[USP_InsertDepositHeaderDet]    Script Date: 07/26/2008 16:19:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertdepositheaderdet')
DROP PROCEDURE usp_insertdepositheaderdet
GO
CREATE PROCEDURE [dbo].[USP_InsertDepositHeaderDet]                              
(@numDepositId AS NUMERIC(9)=0,        
@numChartAcntId AS NUMERIC(9)=0,                             
@datEntry_Date AS DATETIME,                                          
@numAmount AS MONEY,        
@numRecurringId AS NUMERIC(9)=0,                               
@numDomainId AS NUMERIC(9)=0,              
@numUserCntID AS NUMERIC(9)=0,
@strItems TEXT=''
,@numPaymentMethod NUMERIC
,@vcReference VARCHAR(500)
,@vcMemo VARCHAR(1000)
,@tintDepositeToType TINYINT=1 --1 = Direct Deposite to Bank Account , 2= Deposite to Default Undeposited Funds Account
,@numDivisionID NUMERIC
,@tintMode TINYINT =0 --0 = when called from receive payment page, 1 = when called from MakeDeposit Page
,@tintDepositePage TINYINT --1:Make Deposite 2:Receive Payment 3:Credit Memo(Sales Return)
,@numTransHistoryID NUMERIC(9)=0,
@numReturnHeaderID NUMERIC(9)=0,
@numCurrencyID numeric(9) =0,
@fltExchangeRate FLOAT = 1,
@numAccountClass NUMERIC(18,0)=0
)            
AS                                           
BEGIN 

IF ISNULL(@numCurrencyID,0) = 0 
BEGIN
 SET @fltExchangeRate=1
 SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
END
 

--Validation of closed financial year
EXEC dbo.USP_ValidateFinancialYearClosingDate @numDomainID,@datEntry_Date

DECLARE  @hDocItem INT
 
 IF @tintMode != 2
 BEGIN
	 IF @numRecurringId=0 SET @numRecurringId=NULL 
	 IF @numDivisionID=0 SET @numDivisionID=NULL
	 IF @numReturnHeaderID=0 SET @numReturnHeaderID=NULL
	 
	 IF @numDepositId=0                                  
	  BEGIN                              
		  INSERT INTO DepositMaster(numChartAcntId,dtDepositDate,monDepositAmount,numRecurringId,numDomainId,numCreatedBy,dtCreationDate,numPaymentMethod,vcReference,vcMemo,tintDepositeToType,numDivisionID,tintDepositePage,numTransHistoryID,numReturnHeaderID,numCurrencyID,fltExchangeRate,numAccountClass) VALUES            
	   (@numChartAcntId,@datEntry_Date,@numAmount,@numRecurringId,@numDomainId,@numUserCntID,GETUTCDATE(),@numPaymentMethod,@vcReference,@vcMemo,@tintDepositeToType,@numDivisionID,@tintDepositePage,@numTransHistoryID,@numReturnHeaderID,@numCurrencyID,ISNULL(@fltExchangeRate,1),@numAccountClass)                                        
	                         
	   SET @numDepositId = @@IDENTITY                                        
	   SELECT @numDepositId
	  END                            
	                        
	 IF @numDepositId<>0                        
	  BEGIN                        
		UPDATE DepositMaster SET numChartAcntId=@numChartAcntId,dtDepositDate=@datEntry_Date,monDepositAmount=@numAmount,numRecurringId=@numRecurringId,
	   numModifiedBy=@numUserCntID,dtModifiedDate=GETUTCDATE(),numPaymentMethod =@numPaymentMethod,vcReference=@vcReference,vcMemo=@vcMemo,tintDepositeToType =@tintDepositeToType,numDivisionID=@numDivisionID,tintDepositePage=@tintDepositePage,numTransHistoryID=@numTransHistoryID ,numCurrencyID=@numCurrencyID,fltExchangeRate=@fltExchangeRate
	   WHERE numDepositId=@numDepositId AND numDomainId=@numDomainId                       
	   SELECT @numDepositId
	  END            
 END 
  
  IF @tintMode = 1
  BEGIN
  
 
      IF CONVERT(VARCHAR(10),@strItems) <> ''
        BEGIN
          EXEC sp_xml_preparedocument
            @hDocItem OUTPUT ,
            @strItems
            
				-- When user unchecks deposit entry of undeposited fund, reset original entry
				UPDATE dbo.DepositMaster 
				SET bitDepositedToAcnt = 0 WHERE numDepositId IN (  
				
				 SELECT numChildDepositID FROM dbo.DepositeDetails WHERE numDepositID = @numDepositId AND numChildDepositID>0 
				 AND numDepositeDetailID NOT IN (SELECT X.numDepositeDetailID 
												FROM OPENXML(@hDocItem,'/NewDataSet/Item [numDepositeDetailID>0]',2)                                                                                                              
												WITH(numDepositeDetailID NUMERIC(9))X)
				
				)
			
			
              DELETE FROM dbo.DepositeDetails WHERE numDepositID = @numDepositId
              AND numDepositeDetailID NOT IN (SELECT X.numDepositeDetailID 
				 FROM OPENXML(@hDocItem,'/NewDataSet/Item [numDepositeDetailID>0]',2)                                                                                                              
				WITH(numDepositeDetailID NUMERIC(9))X)
			
			  INSERT INTO dbo.DepositeDetails
					  ( 
						numDepositID ,
						numOppBizDocsID ,
						numOppID ,
						monAmountPaid,
						numChildDepositID,
						numPaymentMethod,
						vcMemo,
						vcReference,
						numAccountID,
						numClassID,
						numProjectID,
						numReceivedFrom,dtCreatedDate,dtModifiedDate
					  )
			  SELECT 
					 @numDepositId,
					 NULL ,
					 NULL ,
					 X.monAmountPaid,
					 X.numChildDepositID,
					 X.numPaymentMethod,
						X.vcMemo,
						X.vcReference,
						X.numAccountID,
						X.numClassID,
						X.numProjectID,
						X.numReceivedFrom,GETUTCDATE(),GETUTCDATE()
			  FROM   (SELECT *
					  FROM   OPENXML (@hDocItem, '/NewDataSet/Item [numDepositeDetailID=0]', 2)
								WITH (
								numDepositeDetailID NUMERIC,
								numChildDepositID NUMERIC(9),
								monAmountPaid MONEY,
								numPaymentMethod NUMERIC(18, 0),
								vcMemo VARCHAR(1000),
								vcReference VARCHAR(500),
								numClassID	NUMERIC(18, 0),	
								numProjectID	NUMERIC(18, 0),
								numReceivedFrom	NUMERIC(18, 0),
								numAccountID NUMERIC(18, 0)
										)) X 
								
				UPDATE dbo.DepositeDetails
				SET
						[monAmountPaid] = X.monAmountPaid,
						[numChildDepositID] = X.numChildDepositID,
						[numPaymentMethod] = X.numPaymentMethod,
						[vcMemo] = X.vcMemo,
						[vcReference] = X.vcReference,
						[numClassID] = X.numClassID,
						[numProjectID] = X.numProjectID,
						[numReceivedFrom] = X.numReceivedFrom,
						[numAccountID] = X.numAccountID,
						dtModifiedDate=GETUTCDATE()
				FROM (SELECT *
									  FROM   OPENXML (@hDocItem, '/NewDataSet/Item [numDepositeDetailID>0]', 2)
												WITH (
												numDepositeDetailID NUMERIC,
												numChildDepositID NUMERIC(9),
												monAmountPaid MONEY,
												numPaymentMethod NUMERIC(18, 0),
												vcMemo VARCHAR(1000),
												vcReference VARCHAR(500),
												numClassID	NUMERIC(18, 0),	
												numProjectID	NUMERIC(18, 0),
												numReceivedFrom	NUMERIC(18, 0),
												numAccountID NUMERIC(18, 0)
														)) X 
				WHERE X.numDepositeDetailID =DepositeDetails.numDepositeDetailID AND dbo.DepositeDetails.numDepositID = @numDepositId




				UPDATE dbo.DepositMaster SET bitDepositedToAcnt=1 WHERE numDomainId=@numDomainID AND  numDepositId IN (
									   SELECT numChildDepositID
									  FROM   OPENXML (@hDocItem, '/NewDataSet/Item', 2)
												WITH (numChildDepositID NUMERIC(9)) WHERE numChildDepositID > 0
				)
				
								
  								
          EXEC sp_xml_removedocument
            @hDocItem
        END
  
  
  END 
  
  
  
  IF @tintMode = 0 OR @tintMode = 2
  BEGIN
      IF CONVERT(VARCHAR(10),@strItems) <> ''
        BEGIN
				  EXEC sp_xml_preparedocument
					@hDocItem OUTPUT ,
					@strItems
		            
						/*UPDATE dbo.OpportunityBizDocs 
						SET dbo.OpportunityBizDocs.monAmountPaid = ISNULL(dbo.OpportunityBizDocs.monAmountPaid,0) - ISNULL(X.monAmountPaid,0)
						FROM 				(SELECT numOppBizDocsID,monAmountPaid FROM dbo.DepositeDetails WHERE numDepositID = @numDepositId AND numOppBizDocsID>0 
												AND numDepositeDetailID NOT IN (SELECT numDepositeDetailID 
														FROM OPENXML(@hDocItem,'/NewDataSet/Item [numDepositeDetailID>0]',2)                                                                                                              
														WITH(numDepositeDetailID NUMERIC(9)) )) AS X
						WHERE  X.numOppBizDocsID = OpportunityBizDocs.numOppBizDocsId*/
						
					  SELECT 
					  numDepositeDetailID,numOppBizDocsID ,numOppID ,monAmountPaid INTO #temp
					  FROM   (SELECT *
							  FROM   OPENXML (@hDocItem, '/NewDataSet/Item', 2)
										WITH (numDepositeDetailID NUMERIC(9),numOppBizDocsID NUMERIC(9),numOppID NUMERIC(9),monAmountPaid MONEY)) X
				 	
					
					IF @tintMode = 0
					BEGIN
						UPDATE OBD
						SET monAmountPaid = ISNULL(OBD.monAmountPaid,0) - DD.monAmountPaid
						FROM OpportunityBizDocs OBD JOIN DepositeDetails DD ON OBD.numOppBizDocsId=DD.numOppBizDocsID
						WHERE DD.numDepositID = @numDepositId AND DD.numOppBizDocsID>0 
							 AND DD.numDepositeDetailID NOT IN (SELECT numDepositeDetailID 
														FROM #temp WHERE numDepositeDetailID>0)
														
													
						  DELETE FROM dbo.DepositeDetails WHERE numDepositID = @numDepositId
						  AND numDepositeDetailID NOT IN (SELECT X.numDepositeDetailID 
									FROM OPENXML(@hDocItem,'/NewDataSet/Item [numDepositeDetailID>0]',2)                                                                                                              
									WITH(numDepositeDetailID NUMERIC(9))X)
--							 AND numDepositeDetailID NOT IN (SELECT numDepositeDetailID 
--														FROM #temp WHERE numDepositeDetailID>0)
					 END
		              
		              
					  UPDATE  DD SET [monAmountPaid] = DD.monAmountPaid +  X.monAmountPaid 
								FROM DepositeDetails DD JOIN #temp X ON DD.numOppBizDocsID=X.numOppBizDocsID AND 
								DD.numOppID = X.numOppID
								WHERE X.numDepositeDetailID = 0 AND DD.numDepositID = @numDepositId
										
										
					  INSERT INTO dbo.DepositeDetails
							  ( numDepositID ,numOppBizDocsID ,numOppID ,monAmountPaid,dtCreatedDate,dtModifiedDate )
					  SELECT @numDepositId, numOppBizDocsID , numOppID , monAmountPaid,GETUTCDATE(),GETUTCDATE()
					  FROM #temp WHERE numDepositeDetailID=0 AND numOppBizDocsID NOT IN (SELECT numOppBizDocsID FROM DepositeDetails WHERE numDepositID = @numDepositId)
										
						
						IF @tintMode = 0
						BEGIN
							
		--				UPDATE OBD
		--				SET monAmountPaid = ISNULL(OBD.monAmountPaid,0) - DD.monAmountPaid
		--				FROM OpportunityBizDocs OBD JOIN DepositeDetails DD ON OBD.numOppBizDocsId=DD.numOppBizDocsID
		--				WHERE DD.numDepositID = @numDepositId AND DD.numOppBizDocsID>0 
		--					 AND DD.numDepositeDetailID IN (SELECT X.numDepositeDetailID 
		--												FROM OPENXML(@hDocItem,'/NewDataSet/Item [numDepositeDetailID>0]',2)                                                                                                              
		--												WITH(numDepositeDetailID NUMERIC(9))X)
					
						
								UPDATE  dbo.DepositeDetails
								SET     [monAmountPaid] = X.monAmountPaid ,
										numOppBizDocsID = X.numOppBizDocsID ,
										numOppID = X.numOppID,
										dtModifiedDate=GETUTCDATE()
								FROM    #temp X
								WHERE   X.numDepositeDetailID = DepositeDetails.numDepositeDetailID
										AND dbo.DepositeDetails.numDepositID = @numDepositId
						END


					UPDATE dbo.OpportunityBizDocs 
					SET dbo.OpportunityBizDocs.monAmountPaid = (SELECT ISNULL(SUM(ISNULL(monAmountPaid,0)),0) FROM DepositeDetails WHERE numOppBizDocsID  = OpportunityBizDocs.numOppBizDocsId AND numOppID  = OpportunityBizDocs.numOppID)
					WHERE  OpportunityBizDocs.numOppBizDocsId IN (SELECT numOppBizDocsId FROM DepositeDetails WHERE numDepositId=@numDepositId)

					--Add to OpportunityAutomationQueue if full Amount Paid	
					INSERT INTO [dbo].[OpportunityAutomationQueue] ([numDomainID], [numOppId], [numOppBizDocsId], [numBizDocStatus], [numCreatedBy], [dtCreatedDate], [tintProcessStatus],numRuleID)
						SELECT OM.numDomainID, OM.numOppId, OBD.numOppBizDocsId, 0, @numUserCntID, GETUTCDATE(), 1,7	
						    FROM OpportunityBizDocs OBD JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
								WHERE OM.numDomainId=@numDomainId AND OBD.numOppBizDocsId IN (SELECT numOppBizDocsId FROM DepositeDetails WHERE numDepositId=@numDepositId)
								  AND ISNULL(OBD.monDealAmount,0)=ISNULL(OBD.monAmountPaid,0) AND ISNULL(OBD.monAmountPaid,0)>0

					--Add to OpportunityAutomationQueue if Balance due
					INSERT INTO [dbo].[OpportunityAutomationQueue] ([numDomainID], [numOppId], [numOppBizDocsId], [numBizDocStatus], [numCreatedBy], [dtCreatedDate], [tintProcessStatus],numRuleID)
						SELECT OM.numDomainID, OM.numOppId, OBD.numOppBizDocsId, 0, @numUserCntID, GETUTCDATE(), 1,14	
						    FROM OpportunityBizDocs OBD JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
								WHERE OM.numDomainId=@numDomainId AND OBD.numOppBizDocsId IN (SELECT numOppBizDocsId FROM DepositeDetails WHERE numDepositId=@numDepositId)
								  AND ISNULL(OBD.monDealAmount,0)>ISNULL(OBD.monAmountPaid,0) AND ISNULL(OBD.monAmountPaid,0)>0

														
  					UPDATE DepositMaster SET monAppliedAmount=(SELECT ISNULL(SUM(ISNULL(monAmountPaid,0)),0) FROM DepositeDetails WHERE numDepositId=@numDepositId)
  							WHERE numDepositId=@numDepositId
		  				
		  								
  					DROP TABLE #temp
		  								
				  EXEC sp_xml_removedocument
					@hDocItem
        END
        
        ELSE
        BEGIN
			--IF @tintMode = 0
			--BEGIN
				UPDATE OBD
				SET monAmountPaid = ISNULL(OBD.monAmountPaid,0) - DD.monAmountPaid
				FROM OpportunityBizDocs OBD JOIN DepositeDetails DD ON OBD.numOppBizDocsId=DD.numOppBizDocsID
				WHERE DD.numDepositID = @numDepositId 
											
				DELETE FROM dbo.DepositeDetails WHERE numDepositID = @numDepositId
				  
				UPDATE DepositMaster SET monAppliedAmount=(SELECT ISNULL(SUM(ISNULL(monAmountPaid,0)),0) FROM DepositeDetails WHERE numDepositId=@numDepositId)
  					WHERE numDepositId=@numDepositId
  		  
			--END
        END 
    END
               
END
GO
/****** Object:  StoredProcedure [dbo].[USP_InsertImapUserDtls]    Script Date: 07/26/2008 16:19:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertimapuserdtls')
DROP PROCEDURE usp_insertimapuserdtls
GO
CREATE PROCEDURE [dbo].[USP_InsertImapUserDtls]
    @vcImapPassword AS VARCHAR(100),
    @vcImapServerUrl AS VARCHAR(200),
    @numImapSSLPort AS NUMERIC,
    @bitImapSsl AS BIT,
    @bitImap AS BIT,
    @numUserCntId AS NUMERIC(9),--tintMode=0 then supply UserID instead of UserContactID
    @numDomainID AS NUMERIC(9),
    @bitUseUserName AS BIT,
    @numLastUID AS NUMERIC(9),
    @tintMode TINYINT,
    @vcImapUserName AS VARCHAR(50)--For Support Email Address
AS 
DECLARE @numUserDetailId NUMERIC(9);SET @numUserDetailId=@numUserCntId

--IF @numUserCntId=-1 : For Support Email 
IF @numUserCntId>0
BEGIN
	SELECT  @numUserDetailId = numUserDetailId
	FROM    UserMaster
	WHERE   numUserId = @numUserCntId
END

--delete [ImapUserDetails] where numdomainId = @numDomainID and numUserCntId = @numUserDetailId
IF @tintMode = 0 
    BEGIN
        IF NOT EXISTS ( SELECT  *
                        FROM    [ImapUserDetails]
                        WHERE   numdomainId = @numDomainID
                                AND numUserCntId = @numUserDetailId ) 
            BEGIN
                INSERT  INTO [ImapUserDetails] ( bitImap,
                                                 [vcImapServerUrl],
                                                 [vcImapPassword],
                                                 [bitSSl],
                                                 [numPort],
                                                 [numDomainId],
                                                 [numUserCntId],
                                                 bitUseUserName,
                                                 tintRetryLeft,
                                                 numLastUID,
                                                 bitNotified,vcImapUserName)
                VALUES  (
                          @bitImap,
                          @vcImapServerUrl,
                          @vcImapPassword,
                          @bitImapSsl,
                          @numImapSSLPort,
                          @numDomainID,
                          @numUserDetailId,
                          @bitUseUserName,
                          10,
                          0,
                          1,@vcImapUserName
                        )
            END
        ELSE 
            BEGIN
                UPDATE  ImapUserDetails
                SET     bitImap = @bitImap,
                        [vcImapServerUrl] = @vcImapServerUrl,
                        [vcImapPassword] = @vcImapPassword,
                        [bitSSl] = @bitImapSsl,
                        [numPort] = @numImapSSLPort,
                        bitUseUserName = @bitUseUserName,
                        bitNotified=1,
                        tintRetryLeft=10,vcImapUserName=@vcImapUserName
                WHERE   numdomainId = @numDomainID
                        AND numUserCntId = @numUserDetailId 
            END
            
    END 
ELSE 
    IF @tintMode = 1 
        BEGIN
		
            UPDATE  ImapUserDetails
            SET     numLastUID = @numLastUID
            WHERE   numdomainId = @numDomainID
                    AND numUserCntId = @numUserCntId
        END
        
ELSE IF @tintMode = 2 
        BEGIN
		
            UPDATE  ImapUserDetails
            SET     bitNotified=1
            WHERE   numdomainId = @numDomainID
                    AND numUserCntId = @numUserCntId
        END
/****** Object:  StoredProcedure [dbo].[USP_LeadsUpdateConInfo]    Script Date: 07/26/2008 16:19:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_leadsupdateconinfo')
DROP PROCEDURE usp_leadsupdateconinfo
GO
CREATE PROCEDURE [dbo].[USP_LeadsUpdateConInfo]
@vcFirstName as varchar(50),        
@vcLastName as varchar(50),        
@vcEmail as varchar(50),        
@numFollowUpStatus as numeric(9),        
@numPosition as numeric(9),        
@numPhone as varchar(15),        
@numPhoneExtension as varchar(7),        
@Comments as Text='',        
@numContactId as numeric(9),        
--@vcStreet as varchar(50),        
--@vcCity as varchar(50),        
--@intPostalCode as varchar(12),        
--@vcState as numeric(9),        
--@vcCountry as numeric(9),     
@vcTitle as varchar(100),        
@vcAltEmail as varchar(100),
@numECampaignID NUMERIC,
@numUserCntID NUMERIC
as        
 update AdditionalContactsInformation        
         
 set  vcFirstName=@vcFirstName,        
  vcLastName=@vcLastName,        
  vcEmail=@vcEmail,         
  vcPosition=@numPosition,        
  numPhone=@numPhone,        
  numPhoneExtension=@numPhoneExtension,        
  txtNotes=@Comments,          
  vcTitle=@vcTitle,    
  vcAltEmail=@vcAltEmail,
  numECampaignID=@numECampaignID,
  [numModifiedBy]=@numUserCntID
 where numContactId=@numContactId  

/*Added by chintan BugID-262*/
	 DECLARE @Date AS DATETIME 
	 SELECT @Date = dbo.[GetUTCDateWithoutTime]()
	 IF @numECampaignID = -1 
		SET @numECampaignID=0
	 
 	EXEC [USP_ManageConEmailCampaign]
	@numConEmailCampID = 0, --  numeric(9, 0)
	@numContactID = @numContactID, --  numeric(9, 0)
	@numECampaignID = @numECampaignID, --  numeric(9, 0)
	@intStartDate = @Date, --  datetime
	@bitEngaged = 1, --  bit
	@numUserCntID = @numUserCntID --  numeric(9, 0)
  
--  
--update ContactAddress set  vcStreet=@vcStreet,        
--  vcCity=@vcCity,        
--  intPostalCode =@intPostalCode,        
--  vcState=@vcState,        
--  vcCountry=@vcCountry where   numContactId=@numContactId
GO

GO
/****** Object:  StoredProcedure [dbo].[usp_ManageAddlContInfo]    Script Date: 04/02/2009 00:17:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified By Anoop jayaraj                  
 GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageaddlcontinfo')
DROP PROCEDURE usp_manageaddlcontinfo
GO
CREATE PROCEDURE [dbo].[usp_ManageAddlContInfo]                                    
 @numcontactId numeric=0,                                    
 @numContactType numeric=0,                                    
 @vcDepartment numeric(9)=0,                                    
 @vcCategory numeric(9)=0,                                    
 @vcGivenName varchar(100)='',                                    
 @vcFirstName varchar(50)='',                                    
 @vcLastName varchar(50)='',                                    
 @numDivisionId numeric,                                    
 @numPhone varchar(15)='',                                    
 @numPhoneExtension varchar(7)='',                                    
 @numCell varchar(15)='',                                    
 @NumHomePhone varchar(15)='',                                    
 @vcFax varchar(15)='',                                    
 @vcEmail varchar(50)='',                                    
 @VcAsstFirstName varchar(50)='',                                    
 @vcAsstLastName varchar(50)='',                                    
 @numAsstPhone varchar(15)='',                                    
 @numAsstExtn varchar(6)='',                                    
 @vcAsstEmail varchar(50)='',                                                      
 @charSex char(1)='',                                    
 @bintDOB datetime,                                    
 @vcPosition numeric(9)=0,                                                     
 @txtNotes text='',                                                     
 @numUserCntID numeric,                                                                                              
 @numDomainID numeric=1,                                                   
 @vcPStreet varchar(100)='',                                    
 @vcPCity varchar(50)='',                                    
 @vcPPostalCode varchar(15)='',                                    
 @vcPState numeric(9)=0,                                    
 @vcPCountry numeric(9)=0,                  
 @numManagerID numeric=0,        
 @numTeam as numeric(9)=0,        
 @numEmpStatus as numeric(9)=0,
 @vcTitle AS VARCHAR(100)='',
 @bitPrimaryContact AS BIT=0,
 @bitOptOut AS BIT=0    
AS   


 SELECT @vcGivenName = @vcLastName + ', ' + @vcFirstName  + '.eml'                                 
   if @bintDOB ='Jan  1 1753 12:00AM' set  @bintDOB=null                      
IF @numContactId=0
     BEGIN   

IF @bitPrimaryContact = 1 
    BEGIN            
        DECLARE @numID AS NUMERIC(9)            
        DECLARE @bitPrimaryContactCheck AS BIT             
             
        SELECT TOP 1
                @numID = numContactID,
                @bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
        FROM    AdditionalContactsInformation
        WHERE   numDivisionID = @numDivisionID 
                   
        IF @@rowcount = 0 
            SET @numID = 0             
        WHILE @numID > 0            
            BEGIN            
                IF @bitPrimaryContactCheck = 1 
                    UPDATE  AdditionalContactsInformation
                    SET     bitPrimaryContact = 0
                    WHERE   numContactID = @numID  
                              
                SELECT TOP 1
                        @numID = numContactID,
                        @bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
                FROM    AdditionalContactsInformation
                WHERE   numDivisionID = @numDivisionID
                        AND numContactID > @numID    
                                
                IF @@rowcount = 0 
                    SET @numID = 0             
            END            
    END  
    ELSE IF @bitPrimaryContact = 0
    BEGIN
		IF (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactId!=@numContactId AND ISNULL(bitPrimaryContact,0)=1)=0
			SET @bitPrimaryContact = 1
    END                 
    
    
declare @bitAutoPopulateAddress bit 
declare @tintPoulateAddressTo tinyint 

select @bitAutoPopulateAddress= isnull(bitAutoPopulateAddress,'0'),@tintPoulateAddressTo=isnull(tintPoulateAddressTo,'0') from Domain where numDomainId = @numDomainId ;
if (@bitAutoPopulateAddress = 1)
	if(@tintPoulateAddressTo = 1) --if primary address is not specified then getting billing address    
	begin
		 
		  select                   
			@vcPStreet = AD1.vcStreet,
			@vcPCity= AD1.vcCity,
			@vcPState= AD1.numState,
			@vcPPostalCode= AD1.vcPostalCode,
			@vcPCountry= AD1.numCountry              
		  from divisionMaster  DM              
		   LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
			AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
		  where numDivisionID=@numDivisionId    
	end  
	else if (@tintPoulateAddressTo = 2)-- Primary Address is Shipping Address
	begin
		  select                   
			@vcPStreet= AD2.vcStreet,
			@vcPCity=AD2.vcCity,
			@vcPState=AD2.numState,
			@vcPPostalCode=AD2.vcPostalCode,
			@vcPCountry=AD2.numCountry                   
		  from divisionMaster DM
		  LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
		  AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
		  where numDivisionID=@numDivisionId    
	
	end                       
                       
 if (@numEmpStatus is null or @numEmpStatus=0) set @numEmpStatus=658        
 
    INSERT into AdditionalContactsInformation                                    
    (                  
   numContactType,                  
   vcDepartment,                  
   vcCategory,                  
   vcGivenName,                  
   vcFirstName,                  
   vcLastName,                   
   numDivisionId ,                  
   numPhone ,                  
   numPhoneExtension,                  
   numCell ,                  
   numHomePhone ,                          
   vcFax ,                  
   vcEmail ,                  
   VcAsstFirstName ,                  
   vcAsstLastName ,                  
   numAsstPhone ,                  
   numAsstExtn ,                  
   vcAsstEmail  ,                  
   charSex ,                  
   bintDOB ,                  
   vcPosition ,                  
   txtNotes ,                  
   numCreatedBy ,                                    
   bintCreatedDate,             
   numModifiedBy,            
   bintModifiedDate,                   
   numDomainID,                  
   numManagerID,                   
   numRecOwner,                  
   numTeam,                  
   numEmpStatus,
   vcTitle,bitPrimaryContact,bitOptOut                 
   )                                    
    VALUES                                    
      (                  
   @numContactType,                  
   @vcDepartment,                  
   @vcCategory,                  
   @vcGivenName ,                  
   @vcFirstName ,                  
   @vcLastName,                   
   @numDivisionId ,                  
   @numPhone ,                                    
   @numPhoneExtension,                  
   @numCell ,                  
   @NumHomePhone ,                  
   @vcFax ,                  
   @vcEmail ,                  
   @VcAsstFirstName,                         
   @vcAsstLastName,                  
   @numAsstPhone,                  
   @numAsstExtn,                  
   @vcAsstEmail,                  
   @charSex,                  
   @bintDOB ,                  
   @vcPosition,                                   
   @txtNotes,                  
   @numUserCntID,                  
   getutcdate(),             
   @numUserCntID,                  
   getutcdate(),                   
   @numDomainID,                   
   @numManagerID,                   
   @numUserCntID,                   
   @numTeam,                  
   @numEmpStatus,
   @vcTitle,@bitPrimaryContact,@bitOptOut                
   )                                    
                     
 set @numcontactId= @@IDENTITY                    

     INSERT INTO dbo.AddressDetails (
	 	vcAddressName,
	 	vcStreet,
	 	vcCity,
	 	vcPostalCode,
	 	numState,
	 	numCountry,
	 	bitIsPrimary,
	 	tintAddressOf,
	 	tintAddressType,
	 	numRecordID,
	 	numDomainID
	 ) VALUES ('Primary', @vcPStreet,@vcPCity,@vcPPostalCode,@vcPState,@vcPCountry,1,1,0,@numcontactId,@numDomainID)
                                                    
                               
 select @numcontactId                                                  
  END                                    
 ELSE if @numContactId>0                                                        
    BEGIN                                    
    UPDATE AdditionalContactsInformation SET                                    
      numContactType=@numContactType,                                    
      vcGivenName=@vcGivenName,                                    
      vcFirstName=@vcFirstName ,                                    
      vcLastName =@vcLastName ,                   
      numDivisionId =@numDivisionId ,                                    
      numPhone=@numPhone ,                                    
      numPhoneExtension=@numPhoneExtension,                                    
      numCell =@numCell ,                                    
      NumHomePhone =@NumHomePhone ,                                    
      vcFax=@vcFax ,                                    
      vcEmail=@vcEmail ,                                    
      VcAsstFirstName=@VcAsstFirstName,                                    
      vcAsstLastName=@vcAsstLastName,                                    
      numAsstPhone=@numAsstPhone,                                    
      numAsstExtn=@numAsstExtn,                                    
      vcAsstEmail=@vcAsstEmail,                                                      
      charSex=@charSex,                            
      bintDOB=@bintDOB ,                                    
      vcPosition=@vcPosition,                                                    
      txtNotes=@txtNotes,                                                     
      numModifiedBy=@numUserCntID,                                    
      bintModifiedDate=getutcdate(),                                    
      numManagerID=@numManagerID,
      bitPrimaryContact=@bitPrimaryContact                                    
     WHERE numContactId=@numContactId   
                    
     Update dbo.AddressDetails set                   
       vcStreet=@vcPStreet,                                    
       vcCity =@vcPCity,                                    
       vcPostalCode=@vcPPostalCode,                                     
       numState=@vcPState,                                    
       numCountry=@vcPCountry
	  where numRecordID=@numContactId AND bitIsPrimary=1 AND tintAddressOf=1 AND tintAddressType=0
 
     SELECT @numcontactId   
                                                
  END    
  ------Check the Email id Exist into the Email Master Table

DECLARE @numCount AS NUMERIC(18,0)
SET @numCount = 0
IF @vcEmail <> ''
BEGIN 
SELECT @numCount = COUNT(*) FROM dbo.EmailMaster WHERE vcEmailID =@vcEmail and numDomainId=@numDomainID
IF @numCount = 0 
BEGIN
	INSERT INTO EmailMaster(vcEmailId,vcName,numDomainId,numContactId)
	VALUES(@vcEmail,@vcFirstName,@numDomainID,@numcontactId)
END
else
begin
	update EmailMaster set numContactId=@numcontactId where vcEmailID =@vcEmail and numDomainId=@numDomainID
end
END 
--IF @vcAsstEmail <> ''
--BEGIN
--SELECT @numCount = COUNT(*) FROM dbo.EmailMaster WHERE vcEmailID =@vcAsstEmail
--IF @numCount = 0 
--BEGIN
--	INSERT INTO EmailMaster(vcEmailId,vcName,numDomainId,numContactId)
--	VALUES(@vcAsstEmail,@vcFirstName,@numDomainID,@numcontactId)
--END
--END
---------------------------------------------------------

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageBillHeader' ) 
    DROP PROCEDURE USP_ManageBillHeader
GO

CREATE PROCEDURE USP_ManageBillHeader
    @numBillID [numeric](18, 0) OUTPUT,
    @numDivisionID [numeric](18, 0),
    @numAccountID [numeric](18, 0),
    @dtBillDate [datetime],
    @numTermsID [numeric](18, 0),
    @numDueAmount [numeric](18, 2),
    @dtDueDate [datetime],
    @vcMemo [varchar](1000),
    @vcReference [varchar](500),
    @numBillTotalAmount [numeric](18, 2),
    @bitIsPaid [bit],
    @numDomainID [numeric](18, 0),
    @strItems VARCHAR(MAX),
    @numUserCntID NUMERIC(18, 0)
AS 
    SET NOCOUNT ON  
    BEGIN TRY 
        BEGIN TRAN  
        --Validation of closed financial year
		EXEC dbo.USP_ValidateFinancialYearClosingDate @numDomainID,@dtBillDate
        
        IF EXISTS ( SELECT  [numBillID]
                    FROM    [dbo].[BillHeader]
                    WHERE   [numBillID] = @numBillID ) 
            BEGIN
                UPDATE  [dbo].[BillHeader]
                SET     numDivisionID = @numDivisionID,
                        numAccountID = @numAccountID,
                        dtBillDate = @dtBillDate,
                        numTermsID = @numTermsID,
                        monAmountDue = @numDueAmount,
                        dtDueDate = @dtDueDate,
                        vcMemo = @vcMemo,
                        vcReference = @vcReference,
                        --monAmtPaid = @numBillTotalAmount,
                        bitIsPaid = (CASE WHEN ISNULL(monAmtPaid,0)=@numDueAmount THEN 1
									ELSE 0 END),
                        numDomainID = @numDomainID,
                        dtModifiedDate = GETUTCDATE(),
                        numModifiedBy = @numUserCntID
                WHERE   [numBillID] = @numBillID
                        AND numDomainID = @numDomainID
            END
        ELSE 
            BEGIN
            
				--Set Default Class If enable User Level Class Accountng 
				  DECLARE @numAccountClass AS NUMERIC(18);SET @numAccountClass=0
				  SELECT @numAccountClass=ISNULL(numDefaultClass,0) FROM dbo.UserMaster UM JOIN dbo.Domain D ON UM.numDomainID=D.numDomainId
				  WHERE D.numDomainId=@numDomainId AND UM.numUserDetailId=@numUserCntID AND ISNULL(D.IsEnableUserLevelClassTracking,0)=1
				  
                INSERT  INTO [dbo].[BillHeader]
                        (
                          [numDivisionID],
                          [numAccountID],
                          [dtBillDate],
                          [numTermsID],
                          [monAmountDue],
                          [dtDueDate],
                          [vcMemo],
                          [vcReference],
                          --[monAmtPaid],
                          [bitIsPaid],
                          [numDomainID],
                          [numCreatedBy],
                          [dtCreatedDate],numAccountClass
                        )
                VALUES  (
                          @numDivisionID,
                          @numAccountID,
                          @dtBillDate,
                          @numTermsID,
                          @numDueAmount,
                          @dtDueDate,
                          @vcMemo,
                          @vcReference,
                          --@numBillTotalAmount,
                          @bitIsPaid,
                          @numDomainID,
                          @numUserCntID,
                          GETUTCDATE(),@numAccountClass
                        )
                SET @numBillID = SCOPE_IDENTITY()
            END
				
		PRINT @numBillID		
--        DELETE  FROM dbo.BillDetails
--        WHERE   numBillID = @numBillID 
				
        DECLARE @hDocItem INT
        IF CONVERT(VARCHAR(MAX), @strItems) <> '' 
            BEGIN
                EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
                
                 DELETE FROM BillDetails WHERE numBillID = @numBillID 
					AND numBillDetailID NOT IN (SELECT X.numBillDetailID 
			 FROM OPENXML(@hDocItem,'/NewDataSet/Table1 [numBillDetailID>0]',2)                                                                                                              
			With(numBillDetailID numeric(9))X)
			                                                           
			--Update transactions
			Update BillDetails  Set numExpenseAccountID=X.numExpenseAccountID,monAmount=X.monAmount,                                                                                              
			vcDescription=X.vcDescription,numProjectID=X.numProjectID
			,numClassID=X.numClassID,numCampaignID=X.numCampaignID
			From (SELECT * FROM OPENXML(@hDocItem,'/NewDataSet/Table1 [numBillDetailID>0]',2)                                                                                                              
			With(                                                                                       
			numBillDetailID NUMERIC(18,0),[numExpenseAccountID] NUMERIC(18, 0), [monAmount] MONEY, [vcDescription] VARCHAR(1000), [numProjectID] NUMERIC(18, 0), [numClassID] NUMERIC(18, 0), [numCampaignID] NUMERIC(18, 0)                                                                                                 
			))X                                                                                                  
			Where BillDetails.numBillDetailID=X.numBillDetailID AND numBillID = @numBillID                                                                                             
			               
			
                INSERT  INTO [dbo].[BillDetails]
                        (
                          [numBillID],
                          [numExpenseAccountID],
                          [monAmount],
                          [vcDescription],
                          [numProjectID],
                          [numClassID],
                          [numCampaignID]
                        )
                        SELECT  @numBillID,
                                X.[numExpenseAccountID],
                                X.[monAmount],
                                X.[vcDescription],
                                X.[numProjectID],
                                X.[numClassID],
                                X.[numCampaignID]
                        FROM    ( SELECT    *
                                  FROM      OPENXML (@hDocItem, '/NewDataSet/Table1 [numBillDetailID=0]', 2)
                                            WITH (numBillDetailID NUMERIC(18,0), [numExpenseAccountID] NUMERIC(18, 0), [monAmount] MONEY, [vcDescription] VARCHAR(1000), [numProjectID] NUMERIC(18, 0), [numClassID] NUMERIC(18, 0), [numCampaignID] NUMERIC(18, 0) )
                                ) X
                EXEC sp_xml_removedocument @hDocItem
            END

		-- Update leads,prospects to Accounts
		EXEC dbo.USP_UpdateCRMTypeToAccounts @numDivisionID ,@numDomainID ,@numUserCntID
		
        COMMIT TRAN 
    END TRY 
    BEGIN CATCH		
        DECLARE @strMsg VARCHAR(200)
        SET @strMsg = ERROR_MESSAGE()
        IF ( @@TRANCOUNT > 0 ) 
            BEGIN
                RAISERROR ( @strMsg, 16, 1 ) ;
                ROLLBACK TRAN
                RETURN 1
            END
    END CATCH	
/****** Object:  StoredProcedure [dbo].[usp_ManageContInfo]    Script Date: 07/26/2008 16:19:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managecontinfo')
DROP PROCEDURE usp_managecontinfo
GO
CREATE PROCEDURE [dbo].[usp_ManageContInfo]                  
 @numContactId numeric=0,                  
 @vcDepartment numeric(9)=0,                  
 @vcFirstName varchar(50)='',                  
 @vcLastName varchar(50)='',                  
 @vcEmail varchar(50)='',                  
 @vcSex char(1)='',                  
 @vcPosition  numeric(9)=0,                  
 @numPhone varchar(15)='',                  
 @numPhoneExtension varchar(7)='',                  
 @bintDOB DATETIME=null,                  
 @txtNotes text='',                  
 @numUserCntID numeric=0,                      
 @numCategory numeric=0,                   
 @numCell varchar(15)='',                  
 @NumHomePhone varchar(15)='',                  
 @vcFax varchar(15)='',                  
 @vcAsstFirstName varchar(50)='',                  
 @vcAsstLastName varchar(50)='',                  
 @numAsstPhone varchar(15)='',                  
 @numAsstExtn varchar(6)='',                  
 @vcAsstEmail varchar(50)='',                      
 @numContactType numeric=0 ,               
 @bitOptOut bit=0,                
 @numManagerId numeric=0,                
 @numTeam as numeric(9)=0,                
 @numEmpStatus as numeric(9)=0,        
 @vcAltEmail as varchar(100),        
 @vcTitle as varchar(100),
 @numECampaignID AS NUMERIC(9),
 @bitSelectiveUpdate AS BIT = 0, --Update fields whose value are supplied
 @bitPrimaryContact AS BIT=0        
AS                  
IF @bitSelectiveUpdate = 0
BEGIN

if @bintDOB ='Jan  1 1753 12:00AM' set  @bintDOB=null                  
       
        DECLARE @numDivisionID AS NUMERIC(9)            
        SELECT  @numDivisionID = numDivisionID FROM AdditionalContactsInformation WHERE numContactID = @numContactId   
           
 IF @bitPrimaryContact = 1 
    BEGIN            
        DECLARE @numID AS NUMERIC(9)            
        DECLARE @bitPrimaryContactCheck AS BIT             
             
        SELECT TOP 1
                @numID = numContactID,
                @bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
        FROM    AdditionalContactsInformation
        WHERE   numDivisionID = @numDivisionID 
                   
        IF @@rowcount = 0 
            SET @numID = 0             
        WHILE @numID > 0            
            BEGIN            
                IF @bitPrimaryContactCheck = 1 
                    UPDATE  AdditionalContactsInformation
                    SET     bitPrimaryContact = 0
                    WHERE   numContactID = @numID  
                              
                SELECT TOP 1
                        @numID = numContactID,
                        @bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
                FROM    AdditionalContactsInformation
                WHERE   numDivisionID = @numDivisionID
                        AND numContactID > @numID    
                                
                IF @@rowcount = 0 
                    SET @numID = 0             
            END            
    END 
    ELSE IF @bitPrimaryContact = 0
    BEGIN
		IF (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactId!=@numContactId AND ISNULL(bitPrimaryContact,0)=1)=0
			SET @bitPrimaryContact = 1
    END                 
    
    UPDATE AdditionalContactsInformation SET                  
     vcDepartment=@vcDepartment,                  
     vcFirstName=@vcFirstName ,                  
     vcLastName =@vcLastName ,                  
     numPhone=@numPhone ,                  
     numPhoneExtension=@numPhoneExtension,                  
     vcEmail=@vcEmail ,                  
     charSex=@vcSex ,                  
     vcPosition = @vcPosition,                  
     bintDOB=@bintDOB ,                  
     txtNotes=@txtNotes,                  
     numModifiedBy=@numUserCntID,                  
     bintModifiedDate=getutcdate(),                
     vcCategory = @numCategory,                  
     numCell=@numCell,                  
     NumHomePhone=@NumHomePhone,                  
     vcFax=@vcFax,                  
     vcAsstFirstName=@vcAsstFirstName,                  
     vcAsstLastName=@vcAsstLastName,                  
     numAsstPhone=@numAsstPhone,                  
     numAsstExtn=@numAsstExtn,                  
     vcAsstEmail=@vcAsstEmail,                     
     numContactType=@numContactType,                      
     bitOptOut=@bitOptOut,                
     numManagerId=@numManagerId,                
     numTeam=@numTeam,           
     numEmpStatus = @numEmpStatus,        
     vcTitle=@vcTitle,        
     vcAltEmail= @vcAltEmail,
     numECampaignID = @numECampaignID,
     bitPrimaryContact=@bitPrimaryContact  
 WHERE numContactId=@numContactId
 
 /*Added by chintan BugID-262*/
	 DECLARE @Date AS DATETIME 
	 SELECT @Date = dbo.[GetUTCDateWithoutTime]()
	 IF @numECampaignID = -1 
		SET @numECampaignID=0
	 
 	EXEC [USP_ManageConEmailCampaign]
	@numConEmailCampID = 0, --  numeric(9, 0)
	@numContactID = @numContactID, --  numeric(9, 0)
	@numECampaignID = @numECampaignID, --  numeric(9, 0)
	@intStartDate = @Date, --  datetime
	@bitEngaged = 1, --  bit
	@numUserCntID = @numUserCntID --  numeric(9, 0)
END


IF @bitSelectiveUpdate = 1
BEGIN
		DECLARE @sql VARCHAR(1000)
		SET @sql = 'update AdditionalContactsInformation Set '

		IF(LEN(@vcFirstName)>0)
		BEGIN
			SET @sql =@sql + ' vcFirstName=''' + @vcFirstName + ''', '
		END
		IF(LEN(@vcLastName)>0)
		BEGIN
			SET @sql =@sql + ' vcLastName=''' + @vcLastName + ''', '
		END
		
		IF(LEN(@vcEmail)>0)
		BEGIN
			SET @sql =@sql + ' vcEmail=''' + @vcEmail + ''', '
		END
		
		IF(LEN(@numPhone)>0)
		BEGIN
			SET @sql =@sql + ' numPhone=''' + @numPhone + ''', '
		END
		
		IF(LEN(@numPhoneExtension)>0)
		BEGIN
			SET @sql =@sql + ' numPhoneExtension=''' + @numPhoneExtension + ''', '
		END
		
		IF(LEN(@numCell)>0)
		BEGIN
			SET @sql =@sql + ' numCell=''' + @numCell + ''', '
		END
		
		IF(LEN(@vcFax)>0)
		BEGIN
			SET @sql =@sql + ' vcFax=''' + @vcFax + ''', '
		END
		
		SET @sql =@sql + ' bitOptOut=''' + CONVERT(VARCHAR(10),@bitOptOut) + ''', '
		
		IF(@numUserCntID>0)
		BEGIN
			SET @sql =@sql + ' numModifiedBy='+ CONVERT(VARCHAR(10),@numUserCntID) +', '
		END
		
	
	
		SET @sql=SUBSTRING(@sql,0,LEN(@sql)) + ' '
		SET @sql =@sql + 'where numContactId= '+CONVERT(VARCHAR(10),@numContactId)

		PRINT @sql
		EXEC(@sql) 
END 
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageItemsAndKits]    Script Date: 02/15/2010 21:44:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageitemsandkits')
DROP PROCEDURE usp_manageitemsandkits
GO
CREATE PROCEDURE [dbo].[USP_ManageItemsAndKits]                                                                
@numItemCode as numeric(9) output,                                                                
@vcItemName as varchar(300),                                                                
@txtItemDesc as varchar(1000),                                                                
@charItemType as char(1),                                                                
@monListPrice as money,                                                                
@numItemClassification as numeric(9),                                                                
@bitTaxable as bit,                                                                
@vcSKU as varchar(50),                                                                                                                                      
@bitKitParent as bit,                                                                                   
--@dtDateEntered as datetime,                                                                
@numDomainID as numeric(9),                                                                
@numUserCntID as numeric(9),                                                                                                  
@numVendorID as numeric(9),                                                            
@bitSerialized as bit,                                               
@strFieldList as text,                                            
@vcModelID as varchar(200)='' ,                                            
@numItemGroup as numeric(9)=0,                                      
@numCOGSChartAcntId as numeric(9)=0,                                      
@numAssetChartAcntId as numeric(9)=0,                                      
@numIncomeChartAcntId as numeric(9)=0,                            
@monAverageCost as money,                          
@monLabourCost as money,                  
@fltWeight as float,                  
@fltHeight as float,                  
@fltLength as float,                  
@fltWidth as float,                  
@bitFreeshipping as bit,                
@bitAllowBackOrder as bit,              
@UnitofMeasure as varchar(30)='',              
@strChildItems as text,            
@bitShowDeptItem as bit,              
@bitShowDeptItemDesc as bit,        
@bitCalAmtBasedonDepItems as bit,    
@bitAssembly as BIT,
@intWebApiId INT=null,
@vcApiItemId AS VARCHAR(50)=null,
@numBarCodeId as VARCHAR(25)=null,
@vcManufacturer as varchar(250)=NULL,
@numBaseUnit AS NUMERIC(18)=0,
@numPurchaseUnit AS NUMERIC(18)=0,
@numSaleUnit AS NUMERIC(18)=0,
@bitLotNo as BIT,
@IsArchieve AS BIT,
@numItemClass as numeric(9)=0,
@tintStandardProductIDType AS TINYINT=0,
@vcExportToAPI VARCHAR(50),
@numShipClass NUMERIC,
@ProcedureCallFlag SMALLINT =0,
@vcCategories AS VARCHAR(2000) = '',
@bitAllowDropShip AS BIT=0,
@bitArchiveItem AS BIT = 0
as                                                                
declare @hDoc as INT

if @numCOGSChartAcntId=0 set @numCOGSChartAcntId=null                      
if  @numAssetChartAcntId=0 set @numAssetChartAcntId=null  
if  @numIncomeChartAcntId=0 set @numIncomeChartAcntId=null     

DECLARE @ParentSKU VARCHAR(50)                                        
DECLARE @ItemID AS NUMERIC(9)
DECLARE @cnt AS INT
--if @dtDateEntered = 'Jan  1 1753 12:00:00:000AM' set @dtDateEntered=null 

IF ISNULL(@charItemType,'') = '' OR ISNULL(@charItemType,'') = '0'  SET @charItemType = 'N'

IF @intWebApiId = 2 
    AND LEN(ISNULL(@vcApiItemId, '')) > 0
    AND ISNULL(@numItemGroup, 0) > 0 
    BEGIN      
    
        -- check wether this id already Mapped in ITemAPI 
        SELECT  @cnt = COUNT([numItemID])
        FROM    [ItemAPI]
        WHERE   [WebApiId] = @intWebApiId
                AND [numDomainId] = @numDomainID
                AND [vcAPIItemID] = @vcApiItemId
        IF @cnt > 0 
            BEGIN
                SELECT  @ItemID = [numItemID]
                FROM    [ItemAPI]
                WHERE   [WebApiId] = @intWebApiId
                        AND [numDomainId] = @numDomainID
                        AND [vcAPIItemID] = @vcApiItemId
        --Update Existing Product coming from API
                SET @numItemCode = @ItemID
            END
    
    END

ELSE 
    IF @intWebApiId > 1 --AND  @intWebApiId <> 2
        AND LEN(ISNULL(@vcSKU, '')) > 0 
        BEGIN
            SET @ParentSKU = @vcSKU
            PRINT 1
            --DECLARE @ItemID AS NUMERIC(9)
            --DECLARE @cnt AS INT
    
       -- check wether this id already exist in Domain
       
            SELECT  @cnt = COUNT([numItemCode])
            FROM    [Item]
            WHERE   [numDomainId] = @numDomainID
                    AND ( vcSKU = @vcSKU
                          OR @vcSKU IN ( SELECT vcWHSKU
                                         FROM   dbo.WareHouseItems
                                         WHERE  numItemID = Item.[numItemCode]
                                                AND vcWHSKU = @vcSKU )
                        )
            IF @cnt > 0 
                BEGIN
                    SELECT  @ItemID = [numItemCode],
                            @ParentSKU = vcSKU
                    FROM    [Item]
                    WHERE   [numDomainId] = @numDomainID
                            AND ( vcSKU = @vcSKU
                                  OR @vcSKU IN (
                                  SELECT    vcWHSKU
                                  FROM      dbo.WareHouseItems
                                  WHERE     numItemID = Item.[numItemCode]
                                            AND vcWHSKU = @vcSKU )
                                )
                    SET @numItemCode = @ItemID
                END
            ELSE 
                BEGIN
			
    -- check wether this id already Mapped in ITemAPI 
                    SELECT  @cnt = COUNT([numItemID])
                    FROM    [ItemAPI]
                    WHERE   [WebApiId] = @intWebApiId
                            AND [numDomainId] = @numDomainID
                            AND [vcAPIItemID] = @vcApiItemId
                    IF @cnt > 0 
                        BEGIN
                            SELECT  @ItemID = [numItemID]
                            FROM    [ItemAPI]
                            WHERE   [WebApiId] = @intWebApiId
                                    AND [numDomainId] = @numDomainID
                                    AND [vcAPIItemID] = @vcApiItemId
        --Update Existing Product coming from API
                            SET @numItemCode = @ItemID
                        END
     
                END
        END

                                                           
if @numItemCode=0 or  @numItemCode is null
begin                                                                 
 insert into Item (                                             
  vcItemName,                                             
  txtItemDesc,                                             
  charItemType,                                             
  monListPrice,                                             
  numItemClassification,                                             
  bitTaxable,                                             
  vcSKU,                                             
  bitKitParent,                                             
  --dtDateEntered,                                              
  numVendorID,                                             
  numDomainID,                                             
  numCreatedBy,                                             
  bintCreatedDate,                                             
  bintModifiedDate,                                             
 numModifiedBy,                                              
  bitSerialized,                                     
  vcModelID,                                            
  numItemGroup,                                          
  numCOGsChartAcntId,                        
  numAssetChartAcntId,                                      
  numIncomeChartAcntId,                            
  monAverageCost,                          
  monCampaignLabourCost,                  
  fltWeight,                  
  fltHeight,                  
  fltWidth,                  
  fltLength,                  
  bitFreeShipping,                
  bitAllowBackOrder,              
  vcUnitofMeasure,            
  bitShowDeptItem,            
  bitShowDeptItemDesc,        
  bitCalAmtBasedonDepItems,    
  bitAssembly,
  numBarCodeId,
  vcManufacturer,numBaseUnit,numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,tintStandardProductIDType,vcExportToAPI,numShipClass,
  bitAllowDropShip,bitArchiveItem
   )                                                              
   values                                                                
    (                                                                
  @vcItemName,                                             
  @txtItemDesc,                                  
  @charItemType,                                             
  @monListPrice,                                             
  @numItemClassification,                                             
  @bitTaxable,                                             
  @ParentSKU,                       
  @bitKitParent,                                             
  --@dtDateEntered,                                             
  @numVendorID,                                             
  @numDomainID,                                             
  @numUserCntID,                                           
  getutcdate(),                                             
  getutcdate(),                                             
  @numUserCntID,                                             
  @bitSerialized,                                       
  @vcModelID,                                            
  @numItemGroup,                                      
  @numCOGsChartAcntId,                                      
  @numAssetChartAcntId,                                      
  @numIncomeChartAcntId,                            
  @monAverageCost,                          
  @monLabourCost,                  
  @fltWeight,                  
  @fltHeight,                  
  @fltWidth,                  
  @fltLength,                  
  @bitFreeshipping,                
  @bitAllowBackOrder,              
  @UnitofMeasure,            
  @bitShowDeptItem,            
  @bitShowDeptItemDesc,        
  @bitCalAmtBasedonDepItems,    
  @bitAssembly,                                                          
   @numBarCodeId,
@vcManufacturer,@numBaseUnit,@numPurchaseUnit,@numSaleUnit,@bitLotNo,@IsArchieve,@numItemClass,@tintStandardProductIDType,@vcExportToAPI,@numShipClass,
@bitAllowDropShip,@bitArchiveItem)                                             
 
 set @numItemCode = SCOPE_IDENTITY()
 
 --insert warehouse for inventory item
-- EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList      
-- insert into WareHouseItems (numItemID, numWareHouseID,numOnHand,monWListPrice,vcLocation,vcWHSKU,vcBarCode,numDomainID,numReorder)  
--	SELECT @numItemCode,numWareHouseID,OnHand,Price,Location,SKU,BarCode,@numDomainID,Reorder
--	FROM OPENXML(@hDoc,'/NewDataSet/WareHouse[Op_Flag=1]',2)                                                    
--    with(numWareHouseItemID numeric(9),                                                                        
--    numWareHouseID numeric(9),                    
--    Reorder numeric(9),                                                                       
--    OnHand numeric(9),                                            
--    Op_Flag tinyint,      
--    Price money,
--    Location Varchar(100),
--    SKU varchar(100),
--    BarCode varchar(100))
--    
--update WareHouseItems  set                                                                         
--   numItemID=@numItemCode,                                                                       
--   numWareHouseID=X.numWareHouseID,                                                              
--   numOnHand=X.OnHand,                     
--   numReorder=X.Reorder,      
--   monWListPrice=X.Price,
--   vcLocation=X.Location,
--   vcWHSKU=X.SKU,
--	vcBarCode=X.BarCode                              
--    From (SELECT numWareHouseItemID as WareHouseItemID,numWareHouseID,Reorder,OnHand,Op_Flag,Price,Location,SKU,BarCode                                                                        
--   FROM OPENXML(@hDoc,'/NewDataSet/WareHouse[Op_Flag=2]',2)                                                                         
--    with(numWareHouseItemID numeric(9),                                                                        
--    numWareHouseID numeric(9),                    
--    Reorder numeric(9),                                                                       
--    OnHand numeric(9),                                            
--    Op_Flag tinyint,      
--    Price money,
--    Location Varchar(100),
--    SKU varchar(100),
--    BarCode varchar(100)))X                                                                         
--  where  numWareHouseItemID=X.WareHouseItemID 
--    insert into ItemUOM (numItemCode, numUOMId,numDomainId)  
--	SELECT @numItemCode,numUOMId,@numDomainID
--	FROM OPENXML(@hDoc,'/NewDataSet/TableUOM',2)                                                    
--    with(numUOMId numeric(9))
  
 IF  @intWebApiId > 1 
   BEGIN
     -- insert new product
     --insert this id into linking table
     INSERT INTO [ItemAPI] (
	 	[WebApiId],
	 	[numDomainId],
	 	[numItemID],
	 	[vcAPIItemID],
	 	[numCreatedby],
	 	[dtCreated],
	 	[numModifiedby],
	 	[dtModified],vcSKU
	 ) VALUES     (@intWebApiId,
                 @numDomainID,
                 @numItemCode,
                 @vcApiItemId,
	 			 @numUserCntID,
	 			 GETUTCDATE(),
	 			 @numUserCntID,
	 			 GETUTCDATE(),@vcSKU
	 			 ) 
   
	--update lastUpdated Date                  
	--UPDATE [WebAPIDetail] SET [vcThirteenthFldValue] = GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
   END
 
 
end         
else if    @numItemCode>0  AND @intWebApiId > 0 
BEGIN 
	IF @ProcedureCallFlag =1 
		BEGIN
		DECLARE @ExportToAPIList AS VARCHAR(30)
		SELECT @ExportToAPIList = vcExportToAPI FROM dbo.Item WHERE numItemCode = @numItemCode
		IF @ExportToAPIList != ''
			BEGIN
			IF NOT EXISTS(select * from (SELECT * FROM dbo.Split(@ExportToAPIList ,','))as A where items= @vcExportToAPI ) 
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList + ',' + @vcExportToAPI
				End
			ELSE
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList 
				End
			END
		--update ExportToAPI String value
		UPDATE dbo.Item SET vcExportToAPI=@vcExportToAPI where numItemCode=@numItemCode  AND numDomainID=@numDomainID
					
			SELECT @cnt = COUNT([numItemID]) FROM [ItemAPI] WHERE  [WebApiId] = @intWebApiId 
																AND [numDomainId] = @numDomainID
																AND [vcAPIItemID] = @vcApiItemId
																AND numItemID = @numItemCode
			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE
				--Insert ItemAPI mapping
				BEGIN
					INSERT INTO [ItemAPI] (
						[WebApiId],
						[numDomainId],
						[numItemID],
						[vcAPIItemID],
						[numCreatedby],
						[dtCreated],
						[numModifiedby],
						[dtModified],vcSKU
					) VALUES     (@intWebApiId,
						@numDomainID,
						@numItemCode,
						@vcApiItemId,
						@numUserCntID,
						GETUTCDATE(),
						@numUserCntID,
						GETUTCDATE(),@vcSKU
					)	
				END

		END   
END
                                                       
else if    @numItemCode>0  AND @intWebApiId <= 0                                                           
begin

	DECLARE @OldGroupID NUMERIC 
	SELECT @OldGroupID = ISNULL(numItemGroup,0) FROM [Item] WHERE numitemcode = @numItemCode
	
--	--validate average cost, do not update average cost if item has sales/purchase orders
--	IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) >0
--	BEGIN
--		SELECT @monAverageCost = monAverageCost FROM dbo.Item WHERE numItemCode=@numItemCode
--	END

	DECLARE @monOldAverageCost AS DECIMAL(18,4);SET @monOldAverageCost=0
	SELECT @monOldAverageCost=ISNULL(monAverageCost,0) FROM Item WHERE numItemCode =@numItemCode 
	
----	-- Remove below code block while you update production server
----	PRINT 'FROM ITEM UPDATE : Supplied VendorID =' + CONVERT(VARCHAR(10), @numVendorID)
----	IF ISNULL(@numVendorID,0) = 0
----	BEGIN
----		SELECT @numVendorID = ISNULL(numVendorID,0) FROM dbo.Item WHERE numItemCode = @numItemCode
----		--PRINT @numVendorID
----	END
----	PRINT 'FROM ITEM UPDATE : Modified VendorID =' + CONVERT(VARCHAR(10), @numVendorID)
----	--------------------------------------------------------                                          
	
 update item set vcItemName=@vcItemName,                           
  txtItemDesc=@txtItemDesc,                                             
  charItemType=@charItemType,                                             
  monListPrice=@monListPrice,                                             
  numItemClassification=@numItemClassification,                                             
  bitTaxable=@bitTaxable,                                             
  vcSKU=@ParentSKU,                                             
  bitKitParent=@bitKitParent,                                             
  --dtDateEntered=@dtDateEntered,                                             
  numVendorID=@numVendorID,                                             
  numDomainID=@numDomainID,                                             
  bintModifiedDate=getutcdate(),                                             
  numModifiedBy=@numUserCntID,                                   
  bitSerialized=@bitSerialized,                                                         
  vcModelID=@vcModelID,                                            
  numItemGroup=@numItemGroup,                                      
  numCOGsChartAcntId=@numCOGsChartAcntId,                                      
  numAssetChartAcntId=@numAssetChartAcntId,                                      
  numIncomeChartAcntId=@numIncomeChartAcntId,                            
  --monAverageCost=@monAverageCost,                          
  monCampaignLabourCost=@monLabourCost,                
  fltWeight=@fltWeight,                  
  fltHeight=@fltHeight,                  
  fltWidth=@fltWidth,                  
  fltLength=@fltLength,                  
  bitFreeShipping=@bitFreeshipping,                
  bitAllowBackOrder=@bitAllowBackOrder,              
  vcUnitofMeasure=@UnitofMeasure,            
  bitShowDeptItem=@bitShowDeptItem,            
  bitShowDeptItemDesc=@bitShowDeptItemDesc,        
  bitCalAmtBasedonDepItems=@bitCalAmtBasedonDepItems,    
  bitAssembly=@bitAssembly ,
numBarCodeId=@numBarCodeId,
vcManufacturer=@vcManufacturer,numBaseUnit=@numBaseUnit,numPurchaseUnit=@numPurchaseUnit,numSaleUnit=@numSaleUnit,bitLotNo=@bitLotNo,
IsArchieve = @IsArchieve,numItemClass=@numItemClass,tintStandardProductIDType=@tintStandardProductIDType,vcExportToAPI=@vcExportToAPI,
numShipClass=@numShipClass,bitAllowDropShip=@bitAllowDropShip, bitArchiveItem = @bitArchiveItem
where numItemCode=@numItemCode  AND numDomainID=@numDomainID

IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
	BEGIN
		IF (((SELECT ISNULL(SUM(numOnHand),0) FROM WareHouseItems WHERE numItemID = @numItemCode AND numDomainId=@numDomainID) = 0) AND ((SELECT ISNULL(monAverageCost,0) FROM Item  WHERE numItemCode = @numItemCode AND numDomainId=@numDomainID ) = 0))
		BEGIN
			UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
		END
		--UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
	END
	
 --If Average Cost changed then add in Tracking
	IF ISNULL(@monAverageCost,0) != ISNULL(@monOldAverageCost,0)
	BEGIN
		DECLARE @numWareHouseItemID NUMERIC(9);SET @numWareHouseItemID=0
		DECLARE @numTotal int;SET @numTotal=0
		DECLARE @i int;SET @i=0
		DECLARE @vcDescription AS VARCHAR(100);SET @vcDescription=''
		
	    SELECT @numTotal=COUNT(*) FROM WareHouseItems where numItemID=@numItemCode AND numDomainId=@numDomainId
	     
		WHILE @i < @numTotal
		BEGIN
			SELECT @numWareHouseItemID = min(numWareHouseItemID) FROM WareHouseItems 
				WHERE numItemID=@numItemCode AND numDomainId=@numDomainId and numWareHouseItemID > @numWareHouseItemID 
				
			IF @numWareHouseItemID>0
			BEGIN
				SET @vcDescription='Average Cost Changed Manually OLD : ' + CAST(ISNULL(@monOldAverageCost,0) AS VARCHAR(20))
				
				EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)		
					@numReferenceID = @numItemCode, --  numeric(9, 0)
					@tintRefType = 1, --  tinyint
					@vcDescription = @vcDescription, --  varchar(100)
					@tintMode = 0, --  tinyint
					@numModifiedBy = @numUserCntID --  numeric(9, 0)
		    END
		    
		    SET @i = @i + 1
		END
	END
 
 DECLARE @bitCartFreeShipping as  bit 
 SELECT @bitCartFreeShipping = bitFreeShipping FROM dbo.CartItems where numItemCode = @numItemCode  AND numDomainID = @numDomainID 
 
 IF @bitCartFreeShipping <> @bitFreeshipping
 BEGIN
    UPDATE dbo.CartItems SET bitFreeShipping = @bitFreeshipping WHERE numItemCode = @numItemCode  AND numDomainID=@numDomainID
 END 
 
 
 IF  @intWebApiId > 1 
   BEGIN
      SELECT @cnt = COUNT([numItemID])
    FROM   [ItemAPI]
    WHERE  [WebApiId] = @intWebApiId
           AND [numDomainId] = @numDomainID
           AND [vcAPIItemID] = @vcApiItemId
    IF @cnt > 0
      BEGIN
      --update lastUpdated Date 
      UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
      END
      ELSE
      --Insert ItemAPI mapping
      BEGIN
        INSERT INTO [ItemAPI] (
	 	[WebApiId],
	 	[numDomainId],
	 	[numItemID],
	 	[vcAPIItemID],
	 	[numCreatedby],
	 	[dtCreated],
	 	[numModifiedby],
	 	[dtModified],vcSKU
	 ) VALUES     (@intWebApiId,
                 @numDomainID,
                 @numItemCode,
                 @vcApiItemId,
	 			 @numUserCntID,
	 			 GETUTCDATE(),
	 			 @numUserCntID,
	 			 GETUTCDATE(),@vcSKU
	 			 ) 
      END

--UPDATE [WebAPIDetail] SET [vcThirteenthFldValue] = GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
--UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
--  SELECT * FROM dbo.ItemCategory
--  Insert into ItemCategory(numItemID,numCategoryID)                                            
--  (SELECT numItemID,numCategoryID
--  FROM OPENXML(@hDoc,'/ItemCategories/',2)                                                                         
--    with(numWareHouseItmsDTLID numeric(9),                                                                        
--    numWareHouseItemID numeric(9),                                                  
--   ))
   
   
   END                                                                               
-- kishan It is necessary to add item into itemTax table . 
 IF	@bitTaxable = 1
	BEGIN
		IF NOT EXISTS(SELECT * FROM dbo.ItemTax WHERE numItemCode = @numItemCode AND numTaxItemID = 0)
			BEGIN
			    	INSERT INTO dbo.ItemTax (
						numItemCode,
						numTaxItemID,
						bitApplicable
					) VALUES ( 
						/* numItemCode - numeric(18, 0) */ @numItemCode,
						/* numTaxItemID - numeric(18, 0)*/ 0,
						/* bitApplicable - bit */ 1 ) 						
			END
	END


--  Insert into WareHouseItmsDTL(numWareHouseItemID,vcSerialNo,vcComments,numQty)                                            
--  (SELECT numWareHouseItemID,vcSerialNo,Comments,numQty                                                                        
--  FROM OPENXML(@hDoc,'/NewDataSet/SerializedItems[Op_Flag=1]',2)                                                                         
--    with(numWareHouseItmsDTLID numeric(9),                                                                        
--    numWareHouseItemID numeric(9),                                                  
--    vcSerialNo varchar(100),                                            
--    Op_Flag tinyint,          
--    Comments varchar(1000),numQty NUMERIC(9)))
--                                                                                
--  update WareHouseItmsDTL set                                                                         
--   numWareHouseItemID=X.numWareHouseItemID,                                                                       
--   vcSerialNo=X.vcSerialNo,          
--    vcComments=X.Comments,numQty=X.numQty                                                                                          
--    From (SELECT numWareHouseItmsDTLID as WareHouseItmsDTLID,numWareHouseItemID,vcSerialNo,Op_Flag,Comments,numQty                              
--   FROM OPENXML(@hDoc,'/NewDataSet/SerializedItems[Op_Flag=2]',2)                                                                         
--    with(numWareHouseItmsDTLID numeric(9),                                                          
--    numWareHouseItemID numeric(9),                                                                        
--    vcSerialNo varchar(100),                                            
--    Op_Flag tinyint,          
--    Comments varchar(1000),numQty NUMERIC(9)))X                                                                         
--  where  numWareHouseItmsDTLID=X.WareHouseItmsDTLID                                            
--                                             
--  delete from  WareHouseItmsDTL where numWareHouseItmsDTLID in (SELECT numWareHouseItmsDTLID                                                                        
--   FROM OPENXML(@hDoc,'/NewDataSet/SerializedItems[Op_Flag=3]',2)                                   
--    with(numWareHouseItmsDTLID numeric(9),                              
--    numWareHouseItemID numeric(9),                                                                        
--    vcSerialNo varchar(100),                                            
--  Op_Flag tinyint)) 


--  SELECT X.*,ROW_NUMBER() OVER( order by X.numQty) AS ROWNUMBER
--INTO #TempTableWareHouseItmsDTLID
--FROM ( SELECT  numWareHouseItmsDTLID,numWareHouseItemID,vcSerialNo,Op_Flag,Comments,numQty,OldQty
--                  FROM      OPENXML(@hDoc, '/NewDataSet/SerializedItems',2)
--                            WITH ( numWareHouseItmsDTLID NUMERIC(9), numWareHouseItemID NUMERIC(9), vcSerialNo VARCHAR(100), Op_Flag TINYINT, Comments VARCHAR(1000),OldQty NUMERIC(9),numQty NUMERIC(9) )
--                ) X


--DECLARE @minROWNUMBER INT
--DECLARE @maxROWNUMBER INT
--DECLARE @Diff INT,@Op_Flag AS int
--DECLARE @Diff1 INT
--DECLARE @numWareHouseItemID NUMERIC(9)
--DECLARE @numWareHouseItmsDTLID NUMERIC(9)
--
--SELECT  @minROWNUMBER = MIN(ROWNUMBER) , @maxROWNUMBER =MAX(ROWNUMBER) FROM #TempTableWareHouseItmsDTLID
--
--WHILE  @minROWNUMBER <= @maxROWNUMBER
--    BEGIN
--   	    SELECT @Diff = numQty - OldQty,@numWareHouseItemID=numWareHouseItemID,@Op_Flag=Op_Flag,@numWareHouseItmsDTLID=numWareHouseItmsDTLID FROM #TempTableWareHouseItmsDTLID WHERE ROWNUMBER=@minROWNUMBER
--   	    
--   	    IF @Op_Flag=2 
--   	    BEGIN
--		   	 UPDATE WareHouseItmsDTL SET  numWareHouseItemID = X.numWareHouseItemID,
--                vcSerialNo = X.vcSerialNo,vcComments = X.Comments,numQty=X.numQty 
--                FROM #TempTableWareHouseItmsDTLID X INNER JOIN WareHouseItmsDTL W
--                ON X.numWareHouseItmsDTLID=W.numWareHouseItmsDTLID WHERE X.ROWNUMBER=@minROWNUMBER
--		END 
--   	    
--   	    ELSE IF @Op_Flag=1 
--   	    BEGIN
--   	       	  INSERT INTO WareHouseItmsDTL(numWareHouseItemID,vcSerialNo,vcComments,numQty)  
--		   	   SELECT  X.numWareHouseItemID,X.vcSerialNo,X.Comments,X.numQty  FROM #TempTableWareHouseItmsDTLID X 
--		   	   WHERE X.ROWNUMBER=@minROWNUMBER
--		END
--   	    
--   	    ELSE IF @Op_Flag=3 
--   	    BEGIN
--   	       	  delete from  WareHouseItmsDTL where numWareHouseItmsDTLID=@numWareHouseItmsDTLID
--		END
--		
--   	    update WareHouseItems SET numOnHand=numOnHand + @Diff where numWareHouseItemID = @numWareHouseItemID AND (numOnHand + @Diff)>=0
--  
--        SELECT  @minROWNUMBER = MIN(ROWNUMBER) FROM  #TempTableWareHouseItmsDTLID WHERE  [ROWNUMBER] > @minROWNUMBER
--    END	 
      
IF @charItemType='S' or @charItemType='N'                                                                       
BEGIN
	delete from WareHouseItmsDTL where numWareHouseItemID in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode)
	delete from WareHouseItems where numItemID=@numItemCode
END

--	insert into WareHouseItems (numItemID, numWareHouseID,numOnHand,monWListPrice,vcLocation,vcWHSKU,vcBarCode,numDomainID,numReorder)  
--	SELECT @numItemCode,numWareHouseID,OnHand,Price,Location,SKU,BarCode,@numDomainID,Reorder
--	FROM OPENXML(@hDoc,'/NewDataSet/WareHouse[Op_Flag=1]',2)                                                    
--    with(numWareHouseItemID numeric(9),                                                                        
--    numWareHouseID numeric(9),                    
--    Reorder numeric(9),                                                                       
--    OnHand numeric(9),                                            
--    Op_Flag tinyint,      
--    Price money,
--    Location Varchar(100),
--    SKU varchar(100),
--    BarCode varchar(100))
--SELECT @@IDENTITY	

--  update WareHouseItems  set                                                                         
--   numItemID=@numItemCode,                                                                       
--   numWareHouseID=X.numWareHouseID,                                                              
--   numOnHand=X.OnHand,                     
--   numReorder=X.Reorder,      
--   monWListPrice=X.Price,
--   vcLocation=X.Location,
--   vcWHSKU=X.SKU,
--	vcBarCode=X.BarCode                              
--    From (SELECT numWareHouseItemID as WareHouseItemID,numWareHouseID,Reorder,OnHand,Op_Flag,Price,Location,SKU,BarCode                                                                        
--   FROM OPENXML(@hDoc,'/NewDataSet/WareHouse[Op_Flag=2]',2)                                                                         
--    with(numWareHouseItemID numeric(9),                                                                        
--    numWareHouseID numeric(9),                    
--    Reorder numeric(9),                                                                       
--    OnHand numeric(9),                                            
--    Op_Flag tinyint,      
--    Price money,
--    Location Varchar(100),
--    SKU varchar(100),
--    BarCode varchar(100)))X                                                                         
--  where  numWareHouseItemID=X.WareHouseItemID                                   
                                   
                                   
--    delete from  WareHouseItmsDTL where numWareHouseItemID in (SELECT numWareHouseItemID                                                                 
--   FROM OPENXML(@hDoc,'/NewDataSet/WareHouse[Op_Flag=3]',2)                                                                         
--    with(numWareHouseItemID numeric(9),                                            
--    Op_Flag tinyint))                                             
	
	/*Enforce valiation for child records*/
--	IF exists (SELECT * FROM [OpportunityMaster] OM INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId] WHERE OM.[numDomainId]=@numDomainID AND [numWarehouseItmsID] IN (SELECT numWareHouseItemID FROM OPENXML(@hDoc,'/NewDataSet/WareHouse[Op_Flag=3]',2)
--    with(numWareHouseItemID numeric(9),
--    Op_Flag TINYINT)) )
--	BEGIN
--		raiserror('CHILD_WAREHOUSE',16,1);
--		RETURN ;
--	END

--  delete from  WareHouseItems where numWareHouseItemID in (SELECT numWareHouseItemID FROM OPENXML(@hDoc,'/NewDataSet/WareHouse[Op_Flag=3]',2)
--    with(numWareHouseItemID numeric(9),                                                                 
--    numWareHouseID numeric(9),                                                                        
--    OnHand numeric(9),                                            
--    Op_Flag tinyint))                                  
    /*commented by chintan, Obsolete, Reason: While inserting new Row numWareHouseItemID will be 0,1,2 incrementally. */
--  delete from  WareHouseItems where numItemID=@numItemCode and  numWareHouseItemID not in (SELECT numWareHouseItemID                                                                    
--   FROM OPENXML(@hDoc,'/NewDataSet/WareHouse',2)                                                                         
--    with(numWareHouseItemID numeric(9),                                    
--    numWareHouseID numeric(9),                                                                        
--    OnHand numeric(9),                                            
--    Op_Flag tinyint))                                        

/*If Item is Matrix item and changed to non matrix item then remove matrix attributes associated with- by chintan*/
--IF @OldGroupID>0 AND @numItemGroup = 0 
--BEGIN
--	DELETE FROM [CFW_Fld_Values_Serialized_Items] WHERE [bitSerialized] =0 AND 
--	[RecId] IN ( SELECT numWareHouseItemID FROM  OPENXML(@hDoc,'/NewDataSet/WareHouse',2) with(numWareHouseItemID numeric(9)) )
--END
                                         
                                             
                                                       
                                             
                                             
-- delete from WareHouseItmsDTL where numWareHouseItemID not in (select numWareHouseItemID from WareHouseItems)                                            
 
-- if @bitSerialized=0 AND @bitLotNo=0 delete from WareHouseItmsDTL where  numWareHouseItemID  in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode)                                             
    
--    DELETE FROM  ItemUOM WHERE numItemCode=@numItemCode AND numDomainId=@numDomainId
    
--    insert into ItemUOM (numItemCode, numUOMId,numDomainId)  
--	SELECT @numItemCode,numUOMId,@numDomainID
--	FROM OPENXML(@hDoc,'/NewDataSet/TableUOM',2)                                                    
--    with(numUOMId numeric(9))                                        
   
    if @bitKitParent=1  OR @bitAssembly = 1              
    begin              
  declare @hDoc1 as int                                            
  EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strChildItems                                                                        
                                                                                                 
                                                                
   update ItemDetails set                                                                         
    numQtyItemsReq=X.QtyItemsReq,numWareHouseItemId=X.numWarehouseItmsID,numUOMId=X.numUOMId,vcItemDesc=X.vcItemDesc,sintOrder=X.sintOrder                                                                                          
     From (SELECT QtyItemsReq,ItemKitID,ChildItemID,numWarehouseItmsID,numUOMId,vcItemDesc,sintOrder                             
    FROM OPENXML(@hDoc1,'/NewDataSet/Table1',2)                                                                         
     with(ItemKitID numeric(9),                                                          
     ChildItemID numeric(9),                                                                        
     QtyItemsReq numeric(9),  
     numWarehouseItmsID numeric(9),numUOMId NUMERIC(9),vcItemDesc VARCHAR(1000),sintOrder int))X                                                                         
   where  numItemKitID=X.ItemKitID and numChildItemID=ChildItemID                
              
 end   
 ELSE
 BEGIN
 	DELETE FROM ItemDetails WHERE numItemKitID=@numItemCode
 END           
                                                               
end

declare  @rows as integer                                        
 EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList                                                                        
                                         
 SELECT @rows=count(*) FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
  WITH (Fld_ID numeric(9),Fld_Value varchar(100),RecId numeric(9))                                        
                                                                           
                                          
 if @rows>0                                        
 begin                                        
  Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                    
  Fld_ID numeric(9),                                        
  Fld_Value varchar(100),                                        
  RecId numeric(9),                                    
  bitSItems bit                                                                         
  )                                         
  insert into #tempTable (Fld_ID,Fld_Value,RecId,bitSItems)                                        
  SELECT * FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
  WITH (Fld_ID numeric(9),Fld_Value varchar(100),RecId numeric(9),bitSItems bit)                                           
                                         
  delete CFW_Fld_Values_Serialized_Items from CFW_Fld_Values_Serialized_Items C                                        
  inner join #tempTable T                          
  on   C.Fld_ID=T.Fld_ID and C.RecId=T.RecId and bitSerialized=bitSItems                                         
                                         
  drop table #tempTable                                        
                                                            
  insert into CFW_Fld_Values_Serialized_Items(Fld_ID,Fld_Value,RecId,bitSerialized)                                                  
  select Fld_ID,isnull(Fld_Value,'') as Fld_Value,RecId,bitSItems from(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
  WITH (Fld_ID numeric(9),Fld_Value varchar(100),RecId numeric(9),bitSItems bit)) X                                         
 end  

IF @numItemCode > 0
	BEGIN
			IF EXISTS(SELECT * FROM dbo.ItemCategory WHERE numItemID = @numItemCode)
			 BEGIN
					DELETE FROM dbo.ItemCategory WHERE numItemID = @numItemCode		
			 END 
	  IF @vcCategories <> ''	
		BEGIN
        INSERT INTO ItemCategory( numItemID , numCategoryID )  
        SELECT  @numItemCode AS numItemID,ID from dbo.SplitIDs(@vcCategories,',')	
		END
	END

 EXEC sp_xml_removedocument @hDoc              
/****** Object:  StoredProcedure [USP_ManageReturnHeaderItems]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageReturnHeaderItems' ) 
    DROP PROCEDURE USP_ManageReturnHeaderItems
GO
CREATE PROCEDURE [USP_ManageReturnHeaderItems]
    (
      @numReturnHeaderID NUMERIC(18, 0) OUTPUT,
      @vcRMA VARCHAR(100),
      @vcBizDocName VARCHAR(100),
      @numDomainId NUMERIC(18, 0),
      @numUserCntID AS NUMERIC(9) = 0,
      @numDivisionId NUMERIC(18, 0),
      @numContactId NUMERIC(18, 0),
      @numOppId NUMERIC(18, 0),
      @tintReturnType TINYINT,
      @numReturnReason NUMERIC(18, 0),
      @numReturnStatus NUMERIC(18, 0),
      @monAmount MONEY,
      @monTotalTax MONEY,
      @monTotalDiscount MONEY,
      @tintReceiveType TINYINT,
      @vcComments TEXT,
      @strItems TEXT = '',
      @tintMode TINYINT = 0,
      @numBillAddressId NUMERIC(18, 0),
      @numShipAddressId NUMERIC(18, 0),
      @numDepositIDRef NUMERIC(18,0),
      @numBillPaymentIDRef NUMERIC(18,0)
    )
AS 
    BEGIN 
        DECLARE @hDocItem INT                                                                                                                                                                
 
        IF @numReturnHeaderID = 0 
            BEGIN             
				--Set Default Class If enable User Level Class Accountng 
				  DECLARE @numAccountClass AS NUMERIC(18);SET @numAccountClass=0
				  SELECT @numAccountClass=ISNULL(numDefaultClass,0) FROM dbo.UserMaster UM JOIN dbo.Domain D ON UM.numDomainID=D.numDomainId
				  WHERE D.numDomainId=@numDomainId AND UM.numUserDetailId=@numUserCntID AND ISNULL(D.IsEnableUserLevelClassTracking,0)=1
                   
                       INSERT  INTO .[ReturnHeader]([vcRMA],[vcBizDocName],[numDomainId],[numDivisionId],[numContactId],
                                  [numOppId],[tintReturnType],[numReturnReason],[numReturnStatus],[monAmount],
                                  [monTotalTax],[monTotalDiscount],[tintReceiveType],[vcComments],
                                  [numCreatedBy],[dtCreatedDate],monBizDocAmount,monBizDocUsedAmount,numDepositIDRef,numBillPaymentIDRef,numAccountClass)
                                SELECT  @vcRMA,@vcBizDocName,@numDomainId,@numDivisionId,@numContactId,@numOppId,
                                        @tintReturnType,@numReturnReason,@numReturnStatus,@monAmount,
                                        @monTotalTax,@monTotalDiscount,@tintReceiveType,@vcComments,@numUserCntID,GETUTCDATE(),0,0,@numDepositIDRef,@numBillPaymentIDRef,@numAccountClass
                    
                        SET @numReturnHeaderID = SCOPE_IDENTITY()                                        
                        SELECT  @numReturnHeaderID
                        
                        --Update DepositMaster if Refund UnApplied Payment
                        IF @numDepositIDRef>0 AND @tintReturnType=4
                        BEGIN
							UPDATE dbo.DepositMaster SET monRefundAmount=@monAmount WHERE numDepositId=@numDepositIDRef
						END
						
						--Update BillPaymentHeader if Refund UnApplied Payment
						IF @numBillPaymentIDRef>0 AND @tintReturnType=4
						BEGIN
							UPDATE dbo.BillPaymentHeader SET monRefundAmount=@monAmount WHERE numBillPaymentID=@numBillPaymentIDRef
						END
						
                        DECLARE @tintType TINYINT 
                        SET @tintType=CASE When @tintReturnType=1 OR @tintReturnType=2 THEN 7 
												WHEN @tintReturnType=3 THEN 6
												WHEN @tintReturnType=4 THEN 5 END
												
                        EXEC dbo.USP_UpdateBizDocNameTemplate
								@tintType =  @tintType, --  tinyint
								@numDomainID = @numDomainID, --  numeric(18, 0)
								@RecordID = @numReturnHeaderID --  numeric(18, 0)

                        DECLARE @numRMATempID AS NUMERIC(18, 0)
                        DECLARE @numBizdocTempID AS NUMERIC(18, 0)
                        
                        --                            IF @tintReceiveType = 1 
--                            BEGIN 
--                                SELECT TOP 1 @numBizdocTempID = numBizdocTempID FROM BizDocTemplate
--                                WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1 AND numDomainID = @numDomainID
--                                        AND numBizDocID = ( SELECT TOP 1 numListItemID 
--                                        FROM    dbo.ListDetails WHERE   vcData = 'Refund Receipt' AND constFlag = 1)
--                            END
--							ELSE IF @tintReceiveType = 2 AND @tintReturnType=1
--                            BEGIN
--                                SELECT TOP 1 @numBizdocTempID = numBizDocTempID FROM    BizDocTemplate
--                                WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
--                                        AND numDomainID = @numDomainID AND numBizDocID IN (SELECT TOP 1 numListItemID
--                                        FROM    dbo.ListDetails WHERE   vcData = 'Sales Credit Memo' AND constFlag = 1 )
--                            END
--                            ELSE IF @tintReceiveType = 2 AND @tintReturnType=2
--                            BEGIN
--                                SELECT TOP 1 @numBizdocTempID = numBizDocTempID FROM    BizDocTemplate
--                                WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
--                                        AND numDomainID = @numDomainID AND numBizDocID IN (SELECT TOP 1 numListItemID
--                                        FROM    dbo.ListDetails WHERE   vcData = 'Purchase Credit Memo' AND constFlag = 1 )
--                            END

                        IF @tintReturnType=1 
                        BEGIN
							SELECT TOP 1 @numRMATempID = numBizDocTempID FROM BizDocTemplate
							WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
                                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
									    FROM   dbo.ListDetails WHERE  vcData = 'RMA' AND constFlag = 1 )							
						END
						ELSE IF @tintReturnType=2
                        BEGIN
							SELECT TOP 1 @numRMATempID = numBizDocTempID FROM BizDocTemplate
							WHERE   numOppType = 2 AND bitDefault = 1 AND bitEnabled = 1
                                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
									    FROM   dbo.ListDetails WHERE  vcData = 'RMA' AND constFlag = 1 )	
						END			    
                        ELSE IF @tintReturnType=3
                        BEGIN
							SELECT TOP 1 @numRMATempID = numBizDocTempID FROM    BizDocTemplate
							WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
                                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
                                FROM   dbo.ListDetails WHERE  vcData = 'Credit Memo' AND constFlag = 1 )							
						END
						ELSE IF @tintReturnType=4
                        BEGIN
							SELECT TOP 1 @numRMATempID = numBizDocTempID FROM    BizDocTemplate
							WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
                                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
                                FROM   dbo.ListDetails WHERE  vcData = 'Refund Receipt' AND constFlag = 1 )							
                                
                            SET @numBizdocTempID=@numRMATempID    
						END
                                               
                        UPDATE  dbo.ReturnHeader
                        SET     numRMATempID = ISNULL(@numRMATempID,0),
                                numBizdocTempID = ISNULL(@numBizdocTempID,0)
                        WHERE   numReturnHeaderID = @numReturnHeaderID
			                
			                --Add/Update Address Details
        SET @numOppID = NULLIF(@numOppID, 0)
		            
        DECLARE @vcStreet VARCHAR(100),
            @vcCity VARCHAR(50),
            @vcPostalCode VARCHAR(15),
            @vcCompanyName VARCHAR(100),
            @vcAddressName VARCHAR(50)      
        DECLARE @numState NUMERIC(9),
            @numCountry NUMERIC(9),
            @numCompanyId NUMERIC(9) 
        DECLARE @bitIsPrimary BIT ;

        SELECT  @vcCompanyName = vcCompanyName,
                @numCompanyId = div.numCompanyID
        FROM    CompanyInfo Com
                JOIN divisionMaster Div ON div.numCompanyID = com.numCompanyID
        WHERE   div.numdivisionID = @numDivisionId

--Bill Address
        IF @numBillAddressId > 0 
            BEGIN
                SELECT  @vcStreet = ISNULL(vcStreet, ''),
                        @vcCity = ISNULL(vcCity, ''),
                        @vcPostalCode = ISNULL(vcPostalCode, ''),
                        @numState = ISNULL(numState, 0),
                        @numCountry = ISNULL(numCountry, 0),
                        @bitIsPrimary = bitIsPrimary,
                        @vcAddressName = vcAddressName
                FROM    dbo.AddressDetails
                WHERE   numDomainID = @numDomainID
                        AND numAddressID = @numBillAddressId
                EXEC dbo.USP_UpdateOppAddress @numOppID = @numOppID, --  numeric(9, 0)
                    @byteMode = 0, --  tinyint
                    @vcStreet = @vcStreet, --  varchar(100)
                    @vcCity = @vcCity, --  varchar(50)
                    @vcPostalCode = @vcPostalCode, --  varchar(15)
                    @numState = @numState, --  numeric(9, 0)
                    @numCountry = @numCountry, --  numeric(9, 0)
                    @vcCompanyName = @vcCompanyName, --  varchar(100)
                    @numCompanyId = 0, --  numeric(9, 0)
                    @vcAddressName = @vcAddressName,
                    @numReturnHeaderID = @numReturnHeaderID
            END
    
  --Ship Address
        IF @numShipAddressId > 0 
            BEGIN
                SELECT  @vcStreet = ISNULL(vcStreet, ''),
                        @vcCity = ISNULL(vcCity, ''),
                        @vcPostalCode = ISNULL(vcPostalCode, ''),
                        @numState = ISNULL(numState, 0),
                        @numCountry = ISNULL(numCountry, 0),
                        @bitIsPrimary = bitIsPrimary,
                        @vcAddressName = vcAddressName
                FROM    dbo.AddressDetails
                WHERE   numDomainID = @numDomainID
                        AND numAddressID = @numShipAddressId
 
                EXEC dbo.USP_UpdateOppAddress @numOppID = @numOppID, --  numeric(9, 0)
                    @byteMode = 1, --  tinyint
                    @vcStreet = @vcStreet, --  varchar(100)
                    @vcCity = @vcCity, --  varchar(50)
                    @vcPostalCode = @vcPostalCode, --  varchar(15)
                    @numState = @numState, --  numeric(9, 0)
                    @numCountry = @numCountry, --  numeric(9, 0)
                    @vcCompanyName = @vcCompanyName, --  varchar(100)
                    @numCompanyId = @numCompanyId, --  numeric(9, 0)
                    @vcAddressName = @vcAddressName,
                    @numReturnHeaderID = @numReturnHeaderID
            END
            
							IF @tintReturnType=1
							BEGIN    
								IF @numOppId>0
								BEGIN
									INSERT dbo.OpportunityMasterTaxItems (
											numReturnHeaderID,
											numTaxItemID,
											fltPercentage
										) SELECT @numReturnHeaderID,numTaxItemID,fltPercentage FROM OpportunityMasterTaxItems WHERE numOppID=@numOppID
								END
								ELSE
								BEGIN 
															--Insert Tax for Division                       
										INSERT dbo.OpportunityMasterTaxItems (
											numReturnHeaderID,
											numTaxItemID,
											fltPercentage
										) SELECT @numReturnHeaderID,TI.numTaxItemID,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,NULL,0,@numReturnHeaderID) FROM TaxItems TI JOIN DivisionTaxTypes DTT ON TI.numTaxItemID = DTT.numTaxItemID
										 WHERE DTT.numDivisionID=@numDivisionId AND DTT.bitApplicable=1
										   union 
										  select @numReturnHeaderID,0,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,NULL,1,@numReturnHeaderID) 
										  FROM dbo.DivisionMaster WHERE bitNoTax=0 AND numDivisionID=@numDivisionID
								END			  
							END
          
          IF CONVERT(VARCHAR(10), @strItems) <> '' 
            BEGIN
                PRINT 1
                EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                                                                                             
                SELECT  *
                INTO    #temp
                FROM    OPENXML (@hDocItem, '/NewDataSet/Item',2) WITH ( numReturnItemID NUMERIC, numItemCode NUMERIC, numUnitHour NUMERIC, numUnitHourReceived NUMERIC, monPrice MONEY, monTotAmount MONEY, vcItemDesc VARCHAR(1000), numWareHouseItemID NUMERIC, vcModelID VARCHAR(200), vcManufacturer VARCHAR(250), numUOMId NUMERIC,numOppItemID NUMERIC )

                EXEC sp_xml_removedocument @hDocItem 
                
                        INSERT  INTO [ReturnItems]
                                (
                                  [numReturnHeaderID],
                                  [numItemCode],
                                  [numUnitHour],
                                  [numUnitHourReceived],
                                  [monPrice],
                                  [monTotAmount],
                                  [vcItemDesc],
                                  [numWareHouseItemID],
                                  [vcModelID],
                                  [vcManufacturer],
                                  [numUOMId],numOppItemID
                                )
                                SELECT  @numReturnHeaderID,
                                        numItemCode,
                                        numUnitHour,
                                        0,
                                        monPrice,
                                        monTotAmount,
                                        vcItemDesc,
                                        NULLIF(numWareHouseItemID, 0) numWareHouseItemID,
                                        vcModelID,
                                        vcManufacturer,
                                        numUOMId,numOppItemID
                                FROM    #temp
                                WHERE   numReturnItemID = 0
					
                     
                DROP TABLE #temp
   
						IF @tintReturnType=1
						BEGIN               
							IF @numOppId>0
								BEGIN
									INSERT INTO dbo.OpportunityItemsTaxItems (
										numReturnHeaderID,numReturnItemID,numTaxItemID
									)  SELECT @numReturnHeaderID,OI.numReturnItemID,IT.numTaxItemID 
									FROM dbo.ReturnItems OI JOIN dbo.OpportunityItemsTaxItems IT ON OI.numOppItemID=IT.numOppItemID 
									WHERE OI.numReturnHeaderID=@numReturnHeaderID AND IT.numOppId=@numOppId
								END
								ELSE
								BEGIN
										--Delete Tax for ReturnItems if item deleted 
						--DELETE FROM OpportunityItemsTaxItems WHERE numReturnHeaderID=@numReturnHeaderID AND numReturnItemID NOT IN (SELECT numReturnItemID FROM ReturnItems WHERE numReturnHeaderID=@numReturnHeaderID)

						--Insert Tax for ReturnItems
						INSERT INTO dbo.OpportunityItemsTaxItems (
							numReturnHeaderID,
							numReturnItemID,
							numTaxItemID
						) SELECT @numReturnHeaderID,OI.numReturnItemID,TI.numTaxItemID FROM dbo.ReturnItems OI JOIN dbo.ItemTax IT ON OI.numItemCode=IT.numItemCode JOIN
						TaxItems TI ON TI.numTaxItemID=IT.numTaxItemID WHERE OI.numReturnHeaderID=@numReturnHeaderID AND IT.bitApplicable=1  
						--AND OI.numReturnItemID NOT IN (SELECT numReturnItemID FROM OpportunityItemsTaxItems WHERE numReturnHeaderID=@numReturnHeaderID)
						UNION
						  select @numReturnHeaderID,OI.numReturnItemID,0 FROM dbo.ReturnItems OI JOIN dbo.Item I ON OI.numItemCode=I.numItemCode
						   WHERE OI.numReturnHeaderID=@numReturnHeaderID  AND I.bitTaxable=1
						--AND OI.numReturnItemID NOT IN (SELECT numReturnItemID FROM OpportunityItemsTaxItems WHERE numReturnHeaderID=@numReturnHeaderID)
							END
						END
            END 
            
            -- Update leads,prospects to Accounts
		EXEC dbo.USP_UpdateCRMTypeToAccounts @numDivisionID ,@numDomainID ,@numUserCntID
                          
            END    
        ELSE IF @numReturnHeaderID <> 0 
                BEGIN    
                   IF ISNULL(@tintMode, 0) = 0
                   BEGIN
				   			UPDATE  [ReturnHeader]
							SET     [vcRMA] = @vcRMA,
									[numReturnReason] = @numReturnReason,
									[numReturnStatus] = @numReturnStatus,
									[monAmount] = ISNULL(monAmount,0) + ISNULL(monTotalDiscount,0) - ISNULL(@monTotalDiscount,0),
									[monTotalDiscount] = @monTotalDiscount,
									[tintReceiveType] = @tintReceiveType,
									[vcComments] = @vcComments,
									[numModifiedBy] = @numUserCntID,
									[dtModifiedDate] = GETUTCDATE()
							WHERE   [numDomainId] = @numDomainId
									AND [numReturnHeaderID] = @numReturnHeaderID
				   END                 
                   ELSE IF ISNULL(@tintMode, 0) = 1 
                   BEGIN
                   	UPDATE  ReturnHeader
								SET     numReturnStatus = @numReturnStatus
								WHERE   numReturnHeaderID = @numReturnHeaderID
							
				   	IF CONVERT(VARCHAR(10), @strItems) <> '' 
					BEGIN
						EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                                                                                             
						SELECT  *
						INTO    #temp1
						FROM    OPENXML (@hDocItem, '/NewDataSet/Item',2) WITH ( numReturnItemID NUMERIC, numUnitHourReceived NUMERIC, numWareHouseItemID NUMERIC)
						EXEC sp_xml_removedocument @hDocItem 

								
								UPDATE  [ReturnItems]
								SET     [numUnitHourReceived] = X.numUnitHourReceived,
										[numWareHouseItemID] =CASE WHEN X.numWareHouseItemID=0 THEN NULL ELSE X.numWareHouseItemID END 
								FROM    #temp1 AS X
								WHERE   X.numReturnItemID = ReturnItems.numReturnItemID
										AND ReturnItems.numReturnHeaderID = @numReturnHeaderID
						DROP TABLE #temp1
				END 
			END
       END            
    END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageShippingBox')
DROP PROCEDURE USP_ManageShippingBox
GO
CREATE PROCEDURE USP_ManageShippingBox
   @numShippingReportId NUMERIC,
    @strItems TEXT,
    @numUserCntId NUMERIC,
	@tintMode TINYINT,
	@numBoxID	NUMERIC(18)	OUT,
	@PageMode TINYINT
AS 
    BEGIN
  
  
     DECLARE @hDocItem INT
    
    IF @tintMode = 1 
        BEGIN
  
            IF CONVERT(VARCHAR(10), @strItems) <> '' 
                BEGIN
            
            --Update Box Rates and devivery time
                    EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
                  
                    UPDATE  dbo.ShippingBox
                    SET     dtDeliveryDate = X.dtDeliveryDate ,
                            monShippingRate = X.monShippingRate,
							fltTotalWeight =X.[fltTotalWeight],
							[fltHeight] =X.[fltHeight],
							[fltWidth] = X.[fltWidth],
							[fltLength] = X.[fltLength],
							[numPackageTypeID] = X.numPackageTypeID,
							[numServiceTypeID] = X.numServiceTypeID,
							[fltDimensionalWeight] = X.fltDimensionalWeight,
							numShipCompany = X.numShipCompany
                    FROM    ( SELECT    *
                              FROM      OPENXML (@hDocItem, '/NewDataSet/Box', 2)
                                            WITH ( dtDeliveryDate DATETIME, 
												   monShippingRate MONEY, 
												   numBoxID NUMERIC, 
												   [fltTotalWeight] FLOAT, 
												   [fltHeight] FLOAT, 
												   [fltWidth] FLOAT, 
												   [fltLength] FLOAT, 
												   numPackageTypeID BIGINT, 
												   numServiceTypeID BIGINT, 
												   fltDimensionalWeight FLOAT,
												   numShipCompany INT)
                            ) X
                    WHERE   X.numBoxID = dbo.ShippingBox.numBoxID
                            AND numShippingReportID = @numShippingReportId
                            --AND X.numBoxID = @numBoxID

                    EXEC sp_xml_removedocument @hDocItem
                
                END

        END 
  
    
	
	IF @tintMode=0
	BEGIN
--		IF ISNULL(@PageMode,0) <> 1 
--		BEGIN
			DELETE FROM [ShippingReportItems] WHERE [numShippingReportId]=@numShippingReportId
			
			DELETE  FROM [ShippingBox] WHERE [numShippingReportId] = @numShippingReportId
--		END
		
	
	
        IF CONVERT(VARCHAR(10), @strItems) <> '' 
            BEGIN
            
            --Add Box 
                EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
                                
                INSERT  INTO [ShippingBox]
                        (
                          [vcBoxName],
                          [numShippingReportId],
                          [fltTotalWeight],
                          [fltHeight],
                          [fltWidth],
                          [fltLength],
						  numCreatedBy,
						  numPackageTypeID,
						  numServiceTypeID,
						  fltDimensionalWeight,
						  numShipCompany
	                  )
                        SELECT  X.vcBoxName,
                                @numShippingReportId,
                                X.[fltTotalWeight],
                                X.[fltHeight],
                                X.[fltWidth],
                                X.[fltLength],
								@numUserCntId,
								numPackageTypeID,
								numServiceTypeID,
								fltDimensionalWeight,
								numShipCompany
                        FROM    ( SELECT    *
                                  FROM      OPENXML (@hDocItem, '/NewDataSet/Box', 2)
                                            WITH ( vcBoxName VARCHAR(20), 
												   [fltTotalWeight] FLOAT, 
												   [fltHeight] FLOAT, 
												   [fltWidth] FLOAT, 
												   [fltLength] FLOAT, 
												   numPackageTypeID BIGINT, 
												   numServiceTypeID BIGINT,
												   fltDimensionalWeight FLOAT,
												   numShipCompany INT )
                                ) X
				
				SET @numBoxID = @@IDENTITY
                                                  
                EXEC sp_xml_removedocument @hDocItem
                                               
                --Add items to box
                EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
  
  PRINT 'Item'
                INSERT  INTO [ShippingReportItems]
                        (
                          [numShippingReportId],
                          [numItemCode],
                          [tintServiceType],
                          [dtDeliveryDate],
                          [monShippingRate],
                          [fltTotalWeight],
                          [intNoOfBox],
                          [fltHeight],
                          [fltWidth],
                          [fltLength],
                          [dtCreateDate],
                          [numCreatedBy],
                          [numBoxID],
                          numOppBizDocItemID,
                          intBoxQty
                        )
                        SELECT  @numShippingReportId,
                                OBI.[numItemCode],
                                0,--Unspecified
                                NULL,
                                ISNULL((SELECT ISNULL(monTotAmount,0) FROM dbo.OpportunityItems OI
								WHERE OI.numOppId = (SELECT DISTINCT numOppID FROM dbo.OpportunityBizDocs WHERE numOppBizDocsID = OBI.numOppBizDocID )
								AND numItemCode = (SELECT numSHippingServiceItemID FROM dbo.Domain 
												   WHERE numDomainId = (SELECT numDomainID FROM dbo.ShippingReport 
																		WHERE numShippingReportID = @numShippingReportId ))),0),
                                I.[fltWeight],
                                1,
                                I.[fltHeight],
                                I.[fltWidth],
                                I.[fltLength],
                                GETUTCDATE(),
                                @numUserCntId,
                                ( SELECT    [numBoxID]
                                  FROM      [ShippingBox]
                                  WHERE     vcBoxName = X.vcBoxName
                                            AND numShippingReportId = @numShippingReportId
                                ),
                                X.[numOppBizDocItemID],
                                X.intBoxQty
                        FROM    ( SELECT    * 
                                  FROM      OPENXML (@hDocItem, '/NewDataSet/Item', 2)
                                            WITH ( vcBoxName VARCHAR(20), numOppBizDocItemID NUMERIC, intBoxQty BIGINT )
                                ) X     
                                INNER JOIN [OpportunityBizDocItems] OBI ON X.numOppBizDocItemID = OBI.[numOppBizDocItemID]
                                INNER JOIN Item I ON OBI.[numItemCode] = I.[numItemCode]
                        
                PRINT 5
                EXEC sp_xml_removedocument @hDocItem

            END
	END
	
		
	
    END
/****** Object:  StoredProcedure [dbo].[USP_ManageShippingReport]    Script Date: 05/07/2009 17:31:18 ******/
SET ANSI_NULLS ON
/****** Object:  StoredProcedure [dbo].[USP_ManageShippingReport]    Script Date: 05/07/2009 17:31:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SELECT * FROM [ShippingReport]
--SELECT * FROM [ShippingReportItems]
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageShippingReport')
DROP PROCEDURE USP_ManageShippingReport
GO
CREATE PROCEDURE [dbo].[USP_ManageShippingReport]
  @numDomainId NUMERIC(9),
    @numShippingReportId NUMERIC(9) = 0 OUTPUT,
    @numOppBizDocId NUMERIC(9),
    @numShippingCompany NUMERIC(9),
    @Value1 VARCHAR(100) = NULL,
    @Value2 VARCHAR(100) = NULL,
    @Value3 VARCHAR(100) = NULL,
    @Value4 VARCHAR(100) = NULL,
    @vcFromState VARCHAR(50) = '',
    @vcFromZip VARCHAR(50) = '',
    @vcFromCountry VARCHAR(50) = '',
    @vcToState VARCHAR(50) = '',
    @vcToZip VARCHAR(50) = '',
    @vcToCountry VARCHAR(50) = '',
    @numUserCntId NUMERIC(9),
    @strItems TEXT = NULL,
    @tintMode TINYINT=0,
	@tintPayorType TINYINT=0,
	@vcPayorAccountNo VARCHAR(20)='',
	@numPayorCountry NUMERIC(9),
	@vcPayorZip VARCHAR(50)='',
	@vcFromCity VARCHAR(50)='',
    @vcFromAddressLine1 VARCHAR(50)='',
    @vcFromAddressLine2 VARCHAR(50)='',
    @vcToCity VARCHAR(50)='',
    @vcToAddressLine1 VARCHAR(50)='',
    @vcToAddressLine2 VARCHAR(50)='',
    @vcFromName			VARCHAR(1000)='',
    @vcFromCompany		VARCHAR(1000)='',
    @vcFromPhone		VARCHAR(100)='',
    @bitFromResidential	BIT=0,
    @vcToName			VARCHAR(1000)='',
    @vcToCompany		VARCHAR(1000)='',
    @vcToPhone			VARCHAR(100)='',
    @bitToResidential	BIT=0,
    @IsCOD					BIT = 0,
	@IsDryIce				BIT = 0,
	@IsHoldSaturday			BIT = 0,
	@IsHomeDelivery			BIT = 0,
	@IsInsideDelevery		BIT = 0,
	@IsInsidePickup			BIT = 0,
	@IsReturnShipment		BIT = 0,
	@IsSaturdayDelivery		BIT = 0,
	@IsSaturdayPickup		BIT = 0,
	@numCODAmount			NUMERIC(18,0) = 0,
	@vcCODType				VARCHAR(50) = 'Any',
	@numTotalInsuredValue	NUMERIC(18,0) = 0,
	@IsAdditionalHandling	BIT,
	@IsLargePackage			BIT,
	@vcDeliveryConfirmation	VARCHAR(1000),
	@vcDescription			VARCHAR(MAX),
	@numOppId				NUMERIC(18,0),
	@numTotalCustomsValue	NUMERIC(18,2)
AS 
    BEGIN
  
        IF @numShippingReportId = 0 
            BEGIN
                INSERT  INTO [ShippingReport]
                        (
                          [numOppBizDocId],
                          [numShippingCompany],
                          vcValue1,
                          vcValue2,
                          vcValue3,
                          vcValue4,
                          vcFromState,
                          vcFromZip,
                          vcFromCountry,
                          vcToState,
                          vcToZip,
                          vcToCountry,
                          [dtCreateDate],
                          [numCreatedBy],
                          numDomainId,
						  tintPayorType,
						  vcPayorAccountNo,
						  numPayorCountry,
						  vcPayorZip,
						  vcFromCity ,
						  vcFromAddressLine1 ,
						  vcFromAddressLine2 ,
						  vcToCity ,
						  vcToAddressLine1 ,
						  vcToAddressLine2,
						  vcFromName,
						  vcFromCompany,
						  vcFromPhone,
						  bitFromResidential,
						  vcToName,
						  vcToCompany,
						  vcToPhone,
						  bitToResidential,
						  IsCOD,					
						  IsDryIce,				
						  IsHoldSaturday,			
						  IsHomeDelivery,			
						  IsInsideDelevery,		
						  IsInsidePickup,			
						  IsReturnShipment,		
						  IsSaturdayDelivery,		
						  IsSaturdayPickup,		
						  numCODAmount,			
						  vcCODType,				
						  numTotalInsuredValue,
						  IsAdditionalHandling,
						  IsLargePackage,
						  vcDeliveryConfirmation,
						  vcDescription,
						  numOppId,numTotalCustomsValue
                        )
                VALUES  (
                          @numOppBizDocId,
                          @numShippingCompany,
                          @value1,
                          @value2,
                          @value3,
                          @value4,
                          @vcFromState,
                          @vcFromZip,
                          @vcFromCountry,
                          @vcToState,
                          @vcToZip,
                          @vcToCountry,
                          GETUTCDATE(),
                          @numUserCntId,
                          @numDomainId,
						  @tintPayorType,
						  @vcPayorAccountNo,
						  @numPayorCountry,
						  @vcPayorZip,
						  @vcFromCity ,
						  @vcFromAddressLine1 ,
						  @vcFromAddressLine2 ,
						  @vcToCity ,
						  @vcToAddressLine1 ,
						  @vcToAddressLine2,
						  @vcFromName,
						  @vcFromCompany,
						  @vcFromPhone,
						  @bitFromResidential,
						  @vcToName,
						  @vcToCompany,
						  @vcToPhone,
						  @bitToResidential,
						  @IsCOD,					
						  @IsDryIce,				
						  @IsHoldSaturday,			
						  @IsHomeDelivery,			
						  @IsInsideDelevery,		
						  @IsInsidePickup,			
						  @IsReturnShipment,		
						  @IsSaturdayDelivery,		
						  @IsSaturdayPickup,		
						  @numCODAmount,			
						  @vcCODType,				
						  @numTotalInsuredValue,
						  @IsAdditionalHandling,
						  @IsLargePackage,
						  @vcDeliveryConfirmation,
						  @vcDescription,
						  @numOppId,@numTotalCustomsValue							  
                        )
                        
				SET @numShippingReportId = @@IDENTITY
				
                DECLARE @numBoxID NUMERIC 
--				IF ISNULL(@value2,0) >0 -- Service type i.e Priority overnight,Ground
--				BEGIN
					-- create shipping box and Put all items into single box
						INSERT INTO [ShippingBox] (
							[vcBoxName],
							[numShippingReportId],
							[dtCreateDate],
							[numCreatedBy]
						) VALUES ( 
							/* vcBoxName - varchar(20) */ 'Box1',
							/* numShippingReportId - numeric(18, 0) */ @numShippingReportId,
							/* dtCreateDate - datetime */ GETUTCDATE(),
							/* numCreatedBy - numeric(18, 0) */ @numUserCntId ) 
		                
						SET @numBoxID = @@IDENTITY
--				END
                
                
                IF (@strItems IS NOT NULL)
				BEGIN
				PRINT 'XML'
					DECLARE @hDocItem INT
					EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
	  
					INSERT  INTO [ShippingReportItems]
							(
							  [numShippingReportId],
							  [numItemCode],
							  [tintServiceType],
							  [dtDeliveryDate],
							  [monShippingRate],
							  [fltTotalWeight],
							  [intNoOfBox],
							  [fltHeight],
							  [fltWidth],
							  [fltLength],
							  [dtCreateDate],
							  [numCreatedBy],
							  numBoxID,
							  [numOppBizDocItemID]
							)
							SELECT  @numShippingReportId,--X.[numShippingReportId],
									X.[numItemCode],
									X.[tintServiceType],
									X.[dtDeliveryDate],
									X.[monShippingRate],
									X.[fltTotalWeight],
									X.[intNoOfBox],
									X.[fltHeight],
									X.[fltWidth],
									X.[fltLength],
									GETUTCDATE(),--X.[dtCreateDate],
									@numUserCntId,
									@numBoxID,
									CASE @tintMode WHEN 0 THEN X.numOppBizDocItemID ELSE (SELECT TOP 1 numOppBizDocItemID FROM OpportunityBizDocItems OBI WHERE OBI.numItemCode = X.numItemCode) END AS numOppBizDocItemID
							FROM    ( SELECT    *
									  FROM      OPENXML (@hDocItem, '/NewDataSet/Items', 2)
												WITH ( --numShippingReportId  NUMERIC(18,0),
												numItemCode NUMERIC(18, 0), tintServiceType TINYINT, dtDeliveryDate DATETIME, monShippingRate MONEY, 
												fltTotalWeight FLOAT, intNoOfBox INT, fltHeight FLOAT, fltWidth FLOAT, fltLength FLOAT,numOppBizDocItemID NUMERIC)
									) X
					
					EXEC sp_xml_removedocument @hDocItem	
				END                                
                

            END
        ELSE 
            IF @numShippingReportId > 0 
                BEGIN
                    UPDATE  [ShippingReport]
                    SET     [numShippingCompany] = @numShippingCompany,
                            vcValue1 = @Value1,
                            vcValue2 = @Value2,
                            vcValue3 = @Value3,
                            vcValue4 = @Value4,
                            vcFromState = @vcFromState,
                            vcFromZip = @vcFromZip,
                            vcFromCountry = @vcFromCountry,
                            vcToState = @vcToState,
                            vcToZip = @vcToZip,
                            vcToCountry = @vcToCountry,
							tintPayorType = @tintPayorType,
						    vcPayorAccountNo=@vcPayorAccountNo,
						    numPayorCountry=@numPayorCountry,
						    vcPayorZip=@vcPayorZip,
							vcFromCity=@vcFromCity,
							vcFromAddressLine1=@vcFromAddressLine1,
							vcFromAddressLine2=@vcFromAddressLine2,
							vcToCity=@vcToCity,
							vcToAddressLine1=@vcToAddressLine1,
							vcToAddressLine2=@vcToAddressLine2,
							vcFromName = @vcFromName,
							vcFromCompany = @vcFromCompany,
							vcFromPhone = @vcFromPhone,
							bitFromResidential = @bitFromResidential,
							vcToName = @vcToName,
							vcToCompany = @vcToCompany,
							vcToPhone = @vcToPhone,
							bitToResidential = @bitToResidential,
						    IsCOD = @IsCOD ,					
						    IsDryIce = @IsDryIce,				
						    IsHoldSaturday  = @IsHoldSaturday,			
						    IsHomeDelivery = @IsHomeDelivery,			
						    IsInsideDelevery = @IsInsideDelevery,		
						    IsInsidePickup = @IsInsidePickup,			
						    IsReturnShipment = @IsReturnShipment,		
						    IsSaturdayDelivery = @IsSaturdayDelivery,		
						    IsSaturdayPickup = @IsSaturdayPickup,		
						    numCODAmount = @numCODAmount,			
						    vcCODType = @vcCODType,				
						    numTotalInsuredValue = @numTotalInsuredValue,
						    IsAdditionalHandling = @IsAdditionalHandling,
							IsLargePackage = @IsLargePackage,
							vcDeliveryConfirmation = @vcDeliveryConfirmation,
							vcDescription = @vcDescription,							
						    numOppId = @numOppId,
						    numTotalCustomsValue = @numTotalCustomsValue
                    WHERE   [numShippingReportId] = @numShippingReportId
                    
                    UPDATE [ShippingReportItems] 
                    SET [tintServiceType] = @Value2 
                    WHERE [numShippingReportId]=@numShippingReportId
                  
                END
    
       
    END

/****** Object:  StoredProcedure [dbo].[USP_ManageSubscribers]    Script Date: 07/26/2008 16:20:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                    
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_managesubscribers' ) 
    DROP PROCEDURE usp_managesubscribers
GO
CREATE PROCEDURE [dbo].[USP_ManageSubscribers]
    @numSubscriberID AS NUMERIC(9) OUTPUT,
    @numDivisionID AS NUMERIC(9),
    @intNoofUsersSubscribed AS INTEGER,
    @intNoOfPartners AS INTEGER,
    @dtSubStartDate AS DATETIME,
    @dtSubEndDate AS DATETIME,
    @bitTrial AS BIT,
    @numAdminContactID AS NUMERIC(9),
    @bitActive AS TINYINT,
    @vcSuspendedReason AS VARCHAR(100),
    @numTargetDomainID AS NUMERIC(9) OUTPUT,
    @numDomainID AS NUMERIC(9),
    @numUserContactID AS NUMERIC(9),
    @strUsers AS TEXT = '',
    @bitExists AS BIT = 0 OUTPUT,
    @TargetGroupId AS NUMERIC(9) = 0 OUTPUT,
    @tintLogin TINYINT,
    @intNoofPartialSubscribed AS INTEGER,
    @intNoofMinimalSubscribed AS INTEGER,
	@dtEmailStartDate DATETIME,
	@dtEmailEndDate DATETIME,
	@intNoOfEmail int
AS 
    IF @numSubscriberID = 0 
        BEGIN                                 
            IF NOT EXISTS ( SELECT  *
                            FROM    Subscribers
                            WHERE   numDivisionID = @numDivisionID
                                    AND numDomainID = @numDomainID ) 
                BEGIN              
                    SET @bitExists = 0                              
                    DECLARE @vcCompanyName AS VARCHAR(100)                             
                    DECLARE @numCompanyID AS NUMERIC(9)                                   
                    SELECT  @vcCompanyName = vcCompanyName,
                            @numCompanyID = C.numCompanyID
                    FROM    CompanyInfo C
                            JOIN DivisionMaster D ON D.numCompanyID = C.numCompanyID
                    WHERE   D.numDivisionID = @numDivisionID                                    
                                
                    DECLARE @numNewCompanyID AS NUMERIC(9)                                   
                    DECLARE @numNewDivisionID AS NUMERIC(9)                                    
                    DECLARE @numNewContactID AS NUMERIC(9)                                    
                                      
                    INSERT  INTO Domain
                            (
                              vcDomainName,
                              vcDomainDesc,
                              numDivisionID,
                              numAdminID,
                              tintLogin,tintDecimalPoints,tintChrForComSearch,tintChrForItemSearch, bitIsShowBalance,tintComAppliesTo,tintCommissionType
                            )
                    VALUES  (
                              @vcCompanyName,
                              @vcCompanyName,
                              @numDivisionID,
                              @numAdminContactID,
                              @tintLogin,2,1,1,0,3,1
                            )                                    
                    SET @numTargetDomainID = @@identity                                       
                                       
                    INSERT  INTO CompanyInfo
                            (
                              vcCompanyName,
                              numCompanyType,
                              numCompanyRating,
                              numCompanyIndustry,
                              numCompanyCredit,
                              vcWebSite,
                              vcWebLabel1,
                              vcWebLink1,
                              vcWebLabel2,
                              vcWebLink2,
                              vcWebLabel3,
                              vcWebLink3,
                              vcWeblabel4,
                              vcWebLink4,
                              numAnnualRevID,
                              numNoOfEmployeesId,
                              vcHow,
                              vcProfile,
                              bitPublicFlag,
                              numDomainID
                            )
                            SELECT  vcCompanyName,
                                    93,--i.e employer --numCompanyType,
                                    numCompanyRating,
                                    numCompanyIndustry,
                                    numCompanyCredit,
                                    vcWebSite,
                                    vcWebLabel1,
                                    vcWebLink1,
                                    vcWebLabel2,
                                    vcWebLink2,
                                    vcWebLabel3,
                                    vcWebLink3,
                                    vcWeblabel4,
                                    vcWebLink4,
                                    numAnnualRevID,
                                    numNoOfEmployeesId,
                                    vcHow,
                                    vcProfile,
                                    bitPublicFlag,
                                    numDomainID
                            FROM    CompanyInfo
                            WHERE   numCompanyID = @numCompanyID                            
                    SET @numNewCompanyID = @@identity                            
                                     
                    INSERT  INTO DivisionMaster
                            (
                              numCompanyID,
                              vcDivisionName,
                              numGrpId,
                              bitPublicFlag,
                              tintCRMType,
                              numStatusID,
                              tintBillingTerms,
                              numBillingDays,
                              tintInterestType,
                              fltInterest,
                              vcComPhone,
                              vcComFax,
                              numDomainID
                            )
                            SELECT  @numNewCompanyID,
                                    vcDivisionName,
                                    numGrpId,
                                    bitPublicFlag,
                                    2,
                                    numStatusID,
                                    tintBillingTerms,
                                    numBillingDays,
                                    tintInterestType,
                                    fltInterest,
                                    vcComPhone,
                                    vcComFax,
                                    numDomainID
                            FROM    DivisionMaster
                            WHERE   numDivisionID = @numDivisionID                     
                    SET @numNewDivisionID = @@identity     
                                            
                            INSERT INTO dbo.AddressDetails (
								vcAddressName,
								vcStreet,
								vcCity,
								vcPostalCode,
								numState,
								numCountry,
								bitIsPrimary,
								tintAddressOf,
								tintAddressType,
								numRecordID,
								numDomainID
							) SELECT 
									 vcAddressName,
									 vcStreet,
									 vcCity,
									 vcPostalCode,
									 numState,
									 numCountry,
									 bitIsPrimary,
									 tintAddressOf,
									 tintAddressType,
									 @numNewDivisionID,
									 numDomainID FROM dbo.AddressDetails WHERE numRecordID= @numDivisionID AND tintAddressOf=2
                                
                                    
                    INSERT  INTO AdditionalContactsInformation
                            (
                              vcDepartment,
                              vcCategory,
                              vcGivenName,
                              vcFirstName,
                              vcLastName,
                              numDivisionId,
                              numContactType,
                              numTeam,
                              numPhone,
                              numPhoneExtension,
                              numCell,
                              numHomePhone,
                              vcFax,
                              vcEmail,
                              VcAsstFirstName,
                              vcAsstLastName,
                              numAsstPhone,
                              numAsstExtn,
                              vcAsstEmail,
                              charSex,
                              bintDOB,
                              vcPosition,
                              numEmpStatus,
                              vcTitle,
                              vcAltEmail,
                              numDomainID,
                              bitPrimaryContact
                            )
                            SELECT  vcDepartment,
                                    vcCategory,
                                    vcGivenName,
                                    vcFirstName,
                                    vcLastName,
                                    @numNewDivisionID,
                                    numContactType,
                                    numTeam,
                                    numPhone,
                                    numPhoneExtension,
                                    numCell,
                                    numHomePhone,
                                    vcFax,
                                    vcEmail,
                                    VcAsstFirstName,
                                    vcAsstLastName,
                                    numAsstPhone,
                                    numAsstExtn,
                                    vcAsstEmail,
                                    charSex,
                                    bintDOB,
                                    vcPosition,
                                    numEmpStatus,
                                    vcTitle,
                                    vcAltEmail,
                                    numDomainID,
                                    1
                            FROM    AdditionalContactsInformation
                            WHERE   numContactID = @numAdminContactID                                       
                    SET @numNewContactID = @@identity                             
                             
                    UPDATE  CompanyInfo
                    SET     numCreatedBy = @numNewContactID,
                            bintCreatedDate = GETUTCDATE(),
                            numModifiedBy = @numNewContactID,
                            bintModifiedDate = GETUTCDATE(),
                            numDomainID = @numTargetDomainID
                    WHERE   numCompanyID = @numNewCompanyID                            
                             
                    UPDATE  DivisionMaster
                    SET     numCompanyID = @numNewCompanyID,
                            numCreatedBy = @numNewContactID,
                            bintCreatedDate = GETUTCDATE(),
                            numModifiedBy = @numNewContactID,
                            bintModifiedDate = GETUTCDATE(),
                            numDomainID = @numTargetDomainID,
                            tintCRMType = 2,
                            numRecOwner = @numNewContactID
                    WHERE   numDivisionID = @numNewDivisionID                            
                            
                            
                    UPDATE  AdditionalContactsInformation
                    SET     numDivisionId = @numNewDivisionID,
                            numCreatedBy = @numNewContactID,
                            bintCreatedDate = GETUTCDATE(),
                            numModifiedBy = @numNewContactID,
                            bintModifiedDate = GETUTCDATE(),
                            numDomainID = @numTargetDomainID,
                            numRecOwner = @numNewContactID
                    WHERE   numContactId = @numNewContactID                            
                                
                    DECLARE @vcFirstname AS VARCHAR(50)                            
                    DECLARE @vcEmail AS VARCHAR(100)                            
                    SELECT  @vcFirstname = vcFirstName,
                            @vcEmail = vcEmail
                    FROM    AdditionalContactsInformation
                    WHERE   numContactID = @numNewContactID                            
                    EXEC Resource_Add @vcFirstname, '', @vcEmail, 1,
                        @numTargetDomainID, @numNewContactID   
                        
     -- Added by sandeep to add required fields for New Item form field management in Admnistration section
     
     EXEC USP_ManageNewItemRequiredFields @numDomainID = @numTargetDomainID
     
     --Add Default Subtabs and assign permission to all roles by default --added by chintan

                    EXEC USP_ManageTabsInCuSFields @byteMode = 6, @LocID = 0,
                        @TabName = '', @TabID = 0,
                        @numDomainID = @numTargetDomainID, @vcURL = ''
                        
                    INSERT  INTO RoutingLeads
                            (
                              numDomainId,
                              vcRoutName,
                              tintEqualTo,
                              numValue,
                              numCreatedBy,
                              dtCreatedDate,
                              numModifiedBy,
                              dtModifiedDate,
                              bitDefault,
                              tintPriority
                            )
                    VALUES  (
                              @numTargetDomainID,
                              'Default',
                              0,
                              @numNewContactID,
                              @numNewContactID,
                              GETUTCDATE(),
                              @numNewContactID,
                              GETUTCDATE(),
                              1,
                              0
                            )                          
                    INSERT  INTO RoutingLeadDetails
                            (
                              numRoutID,
                              numEmpId,
                              intAssignOrder
                            )
                    VALUES  (
                              @@identity,
                              @numNewContactID,
                              1
                            )                  
                      
                    IF ( SELECT COUNT(*)
                         FROM   AuthenticationGroupMaster
                         WHERE  numDomainId = @numTargetDomainID
                                AND vcGroupName = 'System Administrator'
                       ) = 0 
                        BEGIN                       
                         
                            DECLARE @numGroupId1 AS NUMERIC(9)                      
                            INSERT  INTO AuthenticationGroupMaster
                                    (
                                      vcGroupName,
                                      numDomainID,
                                      tintGroupType,
                                      bitConsFlag 
                                    )
                            VALUES  (
                                      'System Administrator',
                                      @numTargetDomainID,
                                      1,
                                      0
                                    )                      
                            SET @numGroupId1 = @@identity                        
                            SET @TargetGroupId = @numGroupId1                     
                       
INSERT  INTO GroupAuthorization(numGroupID,numModuleID,numPageID,intExportAllowed,intPrintAllowed,intViewAllowed,
                                      intAddAllowed,intUpdateAllowed,intDeleteAllowed,numDomainID)
select @numGroupId1,numModuleID,numPageID,bitISExportApplicable,0,bitIsViewApplicable,bitIsAddApplicable,bitISUpdateApplicable,bitISDeleteApplicable,@numTargetDomainID
  from PageMaster
--                            INSERT  INTO GroupAuthorization
--                                    (
--                                      numGroupID,
--                                      numModuleID,
--                                      numPageID,
--                                      intExportAllowed,
--                                      intPrintAllowed,
--                                      intViewAllowed,
--                                      intAddAllowed,
--                                      intUpdateAllowed,
--                                      intDeleteAllowed                      
--                       
--                                    )
--                                    SELECT  @numGroupId1,
--                                            x.numModuleID,
--                                            x.numPageID,
--                                            x.intExportAllowed,
--                                            x.intPrintAllowed,
--                                            x.intViewAllowed,
--                                            x.intAddAllowed,
--                                            x.intUpdateAllowed,
--                                            x.intDeleteAllowed
--                                    FROM    ( SELECT    *
--                                              FROM      GroupAuthorization
--                                              WHERE     numGroupid = 1
--                                            ) x            
        
--MainTab From TabMaster 
                            INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId1,
                                            x.numTabId,
                                            0,
                                            1,
                                            x.numOrder,0
                                    FROM    ( SELECT    *,Row_Number() over(Order by numtabid) as numOrder
                                              FROM      tabmaster
                                              WHERE     tintTabtype=1 and bitFixed=1
                                            ) x 

--default Sub tab From cfw_grp_master
INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId1,
                                            x.Grp_id,
                                            0,
                                            1,
                                            x.numOrder,1
                                    FROM    ( SELECT    *,Row_Number() over(Order by Grp_id) as numOrder
                                              FROM      cfw_grp_master
                                              WHERE     tintType=2 AND numDomainID = @numTargetDomainID
                                            ) x 


--Added by Anoop for Dashboard +++++++++++++++++++

                            IF ( SELECT COUNT(*)
                                 FROM   customreport
                                 WHERE  numDomainId = @numTargetDomainID
                               ) = 0 
                                BEGIN                  
                                    EXEC CreateCustomReportsForNewDomain @numTargetDomainID,
                                        @numNewContactID              
                                END 

                            DECLARE @numGroupId2 AS NUMERIC(9)                      
                            INSERT  INTO AuthenticationGroupMaster
                                    (
                                      vcGroupName,
                                      numDomainID,
                                      tintGroupType,
                                      bitConsFlag 
                                    )
                            VALUES  (
                                      'Executive',
                                      @numTargetDomainID,
                                      1,
                                      0
                                    )                      
                            SET @numGroupId2 = @@identity 

INSERT  INTO GroupAuthorization(numGroupID,numModuleID,numPageID,intExportAllowed,intPrintAllowed,intViewAllowed,
                                      intAddAllowed,intUpdateAllowed,intDeleteAllowed,numDomainID)
select @numGroupId2,numModuleID,numPageID,bitISExportApplicable,0,bitIsViewApplicable,bitIsAddApplicable,bitISUpdateApplicable,bitISDeleteApplicable,@numTargetDomainID
  from PageMaster

--                            INSERT  INTO GroupAuthorization
--                                    (
--                                      numGroupID,
--                                      numModuleID,
--                                      numPageID,
--                                      intExportAllowed,
--                                      intPrintAllowed,
--                                      intViewAllowed,
--                                      intAddAllowed,
--                                      intUpdateAllowed,
--                                      intDeleteAllowed                      
--                       
--                                    )
--                                    SELECT  @numGroupId2,
--                                            x.numModuleID,
--                                            x.numPageID,
--                                            x.intExportAllowed,
--                                            x.intPrintAllowed,
--                                            x.intViewAllowed,
--                                            x.intAddAllowed,
--                                            x.intUpdateAllowed,
--                                            x.intDeleteAllowed
--                                    FROM    ( SELECT    *
--                                              FROM      GroupAuthorization
--                                              WHERE     numGroupid = 1
--                                            ) x            
        
                
--MainTab From TabMaster 
                            INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.numTabId,
                                            0,
                                            1,
                                            x.numOrder,0
                                    FROM    ( SELECT    *,Row_Number() over(Order by numtabid) as numOrder
                                              FROM      tabmaster
                                              WHERE     tintTabtype=1 and bitFixed=1
                                            ) x 

--default Sub tab From cfw_grp_master
INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.Grp_id,
                                            0,
                                            1,
                                            x.numOrder,1
                                    FROM    ( SELECT    *,Row_Number() over(Order by Grp_id) as numOrder
                                              FROM      cfw_grp_master
                                              WHERE     tintType=2 AND numDomainID = @numTargetDomainID
                                            ) x 

                            INSERT  INTO DashBoardSize
                                    (
                                      tintColumn,
                                      tintSize,
                                      numGroupUserCntID,
                                      bitGroup
                                    )
                                    SELECT  tintColumn,
                                            tintSize,
                                            @numGroupId2,
                                            1
                                    FROM    DashBoardSize
                                    WHERE   bitGroup = 1
                                            AND numGroupUserCntID = 258
                                    ORDER BY tintColumn 


                            INSERT  INTO Dashboard
                                    (
                                      numReportID,
                                      numGroupUserCntID,
                                      tintRow,
                                      tintColumn,
                                      tintReportType,
                                      vcHeader,
                                      vcFooter,
                                      tintChartType,
                                      bitGroup
                                    )
                                    SELECT  numReportID,
                                            @numGroupId2,
                                            tintRow,
                                            tintColumn,
                                            tintReportType,
                                            vcHeader,
                                            vcFooter,
                                            tintChartType,
                                            1
                                    FROM    Dashboard
                                    WHERE   bitGroup = 1
                                            AND numGroupUserCntID = 258
                                    ORDER BY tintColumn,
                                            tintRow

                            INSERT  INTO DashboardAllowedReports
                                    SELECT  numCustomReportID,
                                            @numGroupId2
                                    FROM    CustomReport
                                    WHERE   numDomainID = @numTargetDomainID 





                            INSERT  INTO AuthenticationGroupMaster
                                    (
                                      vcGroupName,
                                      numDomainID,
                                      tintGroupType,
                                      bitConsFlag 
                                    )
                            VALUES  (
                                      'Sales Staff',
                                      @numTargetDomainID,
                                      1,
                                      0
                                    )                      
                            SET @numGroupId2 = @@identity 

INSERT  INTO GroupAuthorization(numGroupID,numModuleID,numPageID,intExportAllowed,intPrintAllowed,intViewAllowed,
                                      intAddAllowed,intUpdateAllowed,intDeleteAllowed,numDomainID)
select @numGroupId2,numModuleID,numPageID,bitISExportApplicable,0,bitIsViewApplicable,bitIsAddApplicable,bitISUpdateApplicable,bitISDeleteApplicable,@numTargetDomainID
  from PageMaster

--                            INSERT  INTO GroupAuthorization
--                                    (
--                                      numGroupID,
--                                      numModuleID,
--                                      numPageID,
--                                      intExportAllowed,
--                                      intPrintAllowed,
--                                      intViewAllowed,
--                                      intAddAllowed,
--                                      intUpdateAllowed,
--                                      intDeleteAllowed                      
--                       
--                                    )
--                                    SELECT  @numGroupId2,
--                                            x.numModuleID,
--                                            x.numPageID,
--                                            x.intExportAllowed,
--                                            x.intPrintAllowed,
--                                            x.intViewAllowed,
--                                            x.intAddAllowed,
--                                            x.intUpdateAllowed,
--                                            x.intDeleteAllowed
--                                    FROM    ( SELECT    *
--                                              FROM      GroupAuthorization
--                                              WHERE     numGroupid = 1
--                                            ) x            
        
--MainTab From TabMaster 
                            INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.numTabId,
                                            0,
                                            1,
                                            x.numOrder,0
                                    FROM    ( SELECT    *,Row_Number() over(Order by numtabid) as numOrder
                                              FROM      tabmaster
                                              WHERE     tintTabtype=1 and bitFixed=1
                                            ) x 

--default Sub tab From cfw_grp_master
INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.Grp_id,
                                            0,
                                            1,
                                            x.numOrder,1
                                    FROM    ( SELECT    *,Row_Number() over(Order by Grp_id) as numOrder
                                              FROM      cfw_grp_master
                                              WHERE     tintType=2 AND numDomainID = @numTargetDomainID
                                            ) x 

                           

                            INSERT  INTO DashBoardSize
                                    (
                                      tintColumn,
                                      tintSize,
                                      numGroupUserCntID,
                                      bitGroup
                                    )
                                    SELECT  tintColumn,
                                            tintSize,
                                            @numGroupId2,
                                            1
                                    FROM    DashBoardSize
                                    WHERE   bitGroup = 1
                                            AND numGroupUserCntID = 259
                                    ORDER BY tintColumn 


                            INSERT  INTO Dashboard
                                    (
                                      numReportID,
                                      numGroupUserCntID,
                                      tintRow,
                                      tintColumn,
                                      tintReportType,
                                      vcHeader,
                                      vcFooter,
                                      tintChartType,
                                      bitGroup
                                    )
                                    SELECT  numReportID,
                                            @numGroupId2,
                                            tintRow,
                                            tintColumn,
                                            tintReportType,
                                            vcHeader,
                                            vcFooter,
                                            tintChartType,
                                            1
                                    FROM    Dashboard
                                    WHERE   bitGroup = 1
                                            AND numGroupUserCntID = 259
                                    ORDER BY tintColumn,
                                            tintRow

                            INSERT  INTO DashboardAllowedReports
                                    SELECT  numCustomReportID,
                                            @numGroupId2
                                    FROM    CustomReport
                                    WHERE   numDomainID = @numTargetDomainID 






                            INSERT  INTO AuthenticationGroupMaster
                                    (
                                      vcGroupName,
                                      numDomainID,
                                      tintGroupType,
                                      bitConsFlag 
                                    )
                            VALUES  (
                                      'Warehouse Staff',
                                      @numTargetDomainID,
                                      1,
                                      0
                                    )                      
                            SET @numGroupId2 = @@identity 

INSERT  INTO GroupAuthorization(numGroupID,numModuleID,numPageID,intExportAllowed,intPrintAllowed,intViewAllowed,
                                      intAddAllowed,intUpdateAllowed,intDeleteAllowed,numDomainID)
select @numGroupId2,numModuleID,numPageID,bitISExportApplicable,0,bitIsViewApplicable,bitIsAddApplicable,bitISUpdateApplicable,bitISDeleteApplicable,@numTargetDomainID
  from PageMaster

--                            INSERT  INTO GroupAuthorization
--                                    (
--                                      numGroupID,
--                                      numModuleID,
--                                      numPageID,
--                                      intExportAllowed,
--                                      intPrintAllowed,
--                                      intViewAllowed,
--                                      intAddAllowed,
--                                      intUpdateAllowed,
--                                      intDeleteAllowed                      
--                       
--                                    )
--                                    SELECT  @numGroupId2,
--                                            x.numModuleID,
--                                            x.numPageID,
--                                            x.intExportAllowed,
--                                            x.intPrintAllowed,
--                                            x.intViewAllowed,
--                                            x.intAddAllowed,
--                                            x.intUpdateAllowed,
--                                            x.intDeleteAllowed
--                                    FROM    ( SELECT    *
--                                              FROM      GroupAuthorization
--                                              WHERE     numGroupid = 1
--                                            ) x            
        
                           
--MainTab From TabMaster 
                            INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.numTabId,
                                            0,
                                            1,
                                            x.numOrder,0
                                    FROM    ( SELECT    *,Row_Number() over(Order by numtabid) as numOrder
                                              FROM      tabmaster
                                              WHERE     tintTabtype=1 and bitFixed=1
                                            ) x 

--default Sub tab From cfw_grp_master
INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.Grp_id,
                                            0,
                                            1,
                                            x.numOrder,1
                                    FROM    ( SELECT    *,Row_Number() over(Order by Grp_id) as numOrder
                                              FROM      cfw_grp_master
                                              WHERE     tintType=2 AND numDomainID = @numTargetDomainID
                                            ) x 

                           


                            INSERT  INTO DashBoardSize
                                    (
                                      tintColumn,
                                      tintSize,
                                      numGroupUserCntID,
                                      bitGroup
                                    )
                                    SELECT  tintColumn,
                                            tintSize,
                                            @numGroupId2,
                                            1
                                    FROM    DashBoardSize
                                    WHERE   bitGroup = 1
                                            AND numGroupUserCntID = 260
                                    ORDER BY tintColumn 


                            INSERT  INTO Dashboard
                                    (
                                      numReportID,
                                      numGroupUserCntID,
                                      tintRow,
                                      tintColumn,
                                      tintReportType,
                                      vcHeader,
                                      vcFooter,
                                      tintChartType,
                                      bitGroup
                                    )
                                    SELECT  numReportID,
                                            @numGroupId2,
                                            tintRow,
                                            tintColumn,
                                            tintReportType,
                                            vcHeader,
                                            vcFooter,
                                            tintChartType,
                                            1
                                    FROM    Dashboard
                                    WHERE   bitGroup = 1
                                            AND numGroupUserCntID = 260
                                    ORDER BY tintColumn,
                                            tintRow

                            INSERT  INTO DashboardAllowedReports
                                    SELECT  numCustomReportID,
                                            @numGroupId2
                                    FROM    CustomReport
                                    WHERE   numDomainID = @numTargetDomainID 


                            UPDATE  Table2
                            SET     Table2.numReportID = Table1.numOrgReportID
                            FROM    dashboard Table2
                                    JOIN ( SELECT   numReportID,
                                                    ( SELECT TOP 1
                                                                C1.numCustomReportID
                                                      FROM      Customreport C1
                                                                JOIN Customreport C2 ON C1.vcReportName = C2.vcReportName
                                                      WHERE     C1.numDomainID = @numTargetDomainID
                                                                AND C2.numCustomReportID = Dashboard.numReportID
                                                                AND C2.numDomainID = 1
                                                    ) numOrgReportID
                                           FROM     Dashboard
                                           WHERE    bitGroup = 1
                                                    AND numGroupUserCntID = 258
                                         ) Table1 ON Table2.numReportID = Table1.numReportID
                            WHERE   bitGroup = 1
                                    AND numGroupUserCntID = ( SELECT TOP 1
                                                                        numGroupID
                                                              FROM      AuthenticationGroupMaster
                                                              WHERE     numDomainID = @numTargetDomainID
                                                                        AND vcGroupName = 'Executive'
                                                            )

                            UPDATE  Table2
                            SET     Table2.numReportID = Table1.numOrgReportID
                            FROM    dashboard Table2
                                    JOIN ( SELECT   numReportID,
                                                    ( SELECT TOP 1
                                                                C1.numCustomReportID
                                                      FROM      Customreport C1
                                                                JOIN Customreport C2 ON C1.vcReportName = C2.vcReportName
                                                      WHERE     C1.numDomainID = @numTargetDomainID
                                                                AND C2.numCustomReportID = Dashboard.numReportID
                                                                AND C2.numDomainID = 1
                                                    ) numOrgReportID
                                           FROM     Dashboard
                                           WHERE    bitGroup = 1
                                                    AND numGroupUserCntID = 259
                                         ) Table1 ON Table2.numReportID = Table1.numReportID
                            WHERE   bitGroup = 1
                                    AND numGroupUserCntID = ( SELECT TOP 1
                                                                        numGroupID
                                                              FROM      AuthenticationGroupMaster
                                                              WHERE     numDomainID = @numTargetDomainID
                                                                        AND vcGroupName = 'Sales Staff'
                                                            )


                            UPDATE  Table2
                            SET     Table2.numReportID = Table1.numOrgReportID
                            FROM    dashboard Table2
                                    JOIN ( SELECT   numReportID,
                                                    ( SELECT TOP 1
                                                                C1.numCustomReportID
                                                      FROM      Customreport C1
                                                                JOIN Customreport C2 ON C1.vcReportName = C2.vcReportName
                                                      WHERE     C1.numDomainID = @numTargetDomainID
                                                                AND C2.numCustomReportID = Dashboard.numReportID
                                                                AND C2.numDomainID = 1
                                                    ) numOrgReportID
                                           FROM     Dashboard
                                           WHERE    bitGroup = 1
                                                    AND numGroupUserCntID = 260
                                         ) Table1 ON Table2.numReportID = Table1.numReportID
                            WHERE   bitGroup = 1
                                    AND numGroupUserCntID = ( SELECT TOP 1
                                                                        numGroupID
                                                              FROM      AuthenticationGroupMaster
                                                              WHERE     numDomainID = @numTargetDomainID
                                                                        AND vcGroupName = 'Warehouse Staff'
                                                            )



------ Added by Anoop for Dashboard-----------       
                        END                      
                    IF ( SELECT COUNT(*)
                         FROM   AuthenticationGroupMaster
                         WHERE  numDomainId = @numTargetDomainID
                                AND vcGroupName = 'Default Extranet'
                       ) = 0 
                        BEGIN                       
                                             
                            INSERT  INTO AuthenticationGroupMaster
                                    (
                                      vcGroupName,
                                      numDomainID,
                                      tintGroupType,
                                      bitConsFlag 
                                    )
                            VALUES  (
                                      'Default Extranet',
                                      @numTargetDomainID,
                                      2,
                                      0
                                    )                      
                            SET @numGroupId2 = @@identity                        
                      
                       
                            INSERT  INTO GroupAuthorization
                                    (
                                      numGroupID,
                                      numModuleID,
                                      numPageID,
                                      intExportAllowed,
                                      intPrintAllowed,
                                      intViewAllowed,
                                      intAddAllowed,
                                      intUpdateAllowed,
                                      intDeleteAllowed,
                                      numDomainID                      
                       
                                    )
                                    SELECT  @numGroupId2,
                                            x.numModuleID,
                                            x.numPageID,
                                            x.intExportAllowed,
                                            x.intPrintAllowed,
                                            x.intViewAllowed,
                                            x.intAddAllowed,
                                            x.intUpdateAllowed,
                                            x.intDeleteAllowed,
                                            @numTargetDomainID
                                    FROM    ( SELECT    *
                                              FROM      GroupAuthorization
                                              WHERE     numGroupid = 2
                                            ) x           
        
                          
--MainTab From TabMaster for Customer:46
                            INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.numTabId,
                                            46,
                                            1,
                                            x.numOrder,0
                                    FROM    ( SELECT    *,Row_Number() over(Order by numtabid) as numOrder
                                              FROM      tabmaster
                                              WHERE     tintTabtype=2 and bitFixed=1
                                            ) x 

--default Sub tab From cfw_grp_master for Customer:46
INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.Grp_id,
                                            46,
                                            1,
                                            x.numOrder,1
                                    FROM    ( SELECT    *,Row_Number() over(Order by Grp_id) as numOrder
                                              FROM      cfw_grp_master
                                              WHERE     tintType=2 AND numDomainID = @numTargetDomainID
                                            ) x 


                          
--MainTab From TabMaster  for Employer:93
                            INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.numTabId,
                                           93,
                                            1,
                                            x.numOrder,0
                                    FROM    ( SELECT    *,Row_Number() over(Order by numtabid) as numOrder
                                              FROM      tabmaster
                                              WHERE     tintTabtype=2 and bitFixed=1
                                            ) x 

--default Sub tab From cfw_grp_master for Employer:93
INSERT  INTO GroupTabDetails
                                    (
                                      numGroupId,
                                      numTabId,
                                      numRelationShip,
                                      bitallowed,
                                      numOrder,tintType
                                    )
                                    SELECT  @numGroupId2,
                                            x.Grp_id,
                                            93,
                                            1,
                                            x.numOrder,1
                                    FROM    ( SELECT    *,Row_Number() over(Order by Grp_id) as numOrder
                                              FROM      cfw_grp_master
                                              WHERE     tintType=2 AND numDomainID = @numTargetDomainID
                                            ) x 
      
                   
                        END                      
----                    IF ( SELECT COUNT(*)
----                         FROM   AuthenticationGroupMaster
----                         WHERE  numDomainId = @numTargetDomainID
----                                AND vcGroupName = 'Default PRM'
----                       ) = 0 
----                        BEGIN                       
----                         
----                            DECLARE @numGroupId3 AS NUMERIC(9)                      
----                            INSERT  INTO AuthenticationGroupMaster
----                                    (
----                                      vcGroupName,
----                                      numDomainID,
----                                      tintGroupType,
----                                      bitConsFlag 
----                                    )
----                            VALUES  (
----                                      'Default PRM',
----                                      @numTargetDomainID,
----                                      3,
----                                      0
----                                    )                      
----                            SET @numGroupId3 = @@identity                        
----                      
----                       
----                            INSERT  INTO GroupAuthorization
----                                    (
----                                      numGroupID,
----                                      numModuleID,
----                                      numPageID,
----                                      intExportAllowed,
----                                      intPrintAllowed,
----                                      intViewAllowed,
----                                      intAddAllowed,
----                                      intUpdateAllowed,
----                                      intDeleteAllowed,
----                                      numDomainID                   
----                   
----                                    )
----                                    SELECT  @numGroupId3,
----                                            x.numModuleID,
----                                            x.numPageID,
----                                            x.intExportAllowed,
----                                            x.intPrintAllowed,
----                                            x.intViewAllowed,
----                                            x.intAddAllowed,
----                                            x.intUpdateAllowed,
----                                            x.intDeleteAllowed,
----                                            @numTargetDomainID
----                                    FROM    ( SELECT    *
----                                              FROM      GroupAuthorization
----                                              WHERE     numGroupid = 46
----                                            ) x             
----        
------                            INSERT  INTO GroupTabDetails
------                                    (
------                                      numGroupId,
------                                      numTabId,
------                                      numRelationShip,
------                                      bitallowed,
------                                      numOrder
------                                    )
------                                    SELECT  @numGroupId3,
------                                            x.numTabId,
------                                            x.numRelationShip,
------                                            x.bitallowed,
------                                            x.numOrder
------                                    FROM    ( SELECT    *
------                                              FROM      GroupTabDetails
------                                              WHERE     numGroupid = 46
------                                            ) x         
----                 
----                        END                      
                    
       

      
--- Set default Chart of Accounts 
                    EXEC USP_ChartAcntDefaultValues @numDomainId = @numTargetDomainID,
                        @numUserCntId = @numNewContactID      
    
    
-- Dashboard Size    
                    INSERT  INTO DashBoardSize
                            SELECT  1,
                                    1,
                                    @numGroupId1,
                                    1
                            UNION
                            SELECT  2,
                                    1,
                                    @numGroupId1,
                                    1
                            UNION
                            SELECT  3,
                                    2,
                                    @numGroupId1,
                                    1     
    
----inserting all the Custome Reports for the newly created group    
    
                    INSERT  INTO DashboardAllowedReports
                            SELECT  numCustomReportID,
                                    @numGroupId1
                            FROM    CustomReport
                            WHERE   numDomainID = @numTargetDomainID    
    
    
           
 --inserting Default BizDocs for new domain
INSERT INTO dbo.AuthoritativeBizDocs
        ( numAuthoritativePurchase ,
          numAuthoritativeSales ,
          numDomainId
        )
SELECT  
ISNULL((SELECT numListItemID  FROM dbo.ListDetails WHERE numListID=27 AND constFlag =1 AND vcData = 'bill'),0),
ISNULL((SELECT numListItemID  FROM dbo.ListDetails WHERE numListID=27 AND constFlag =1 AND vcData = 'invoice'),0),
@numTargetDomainID


IF NOT EXISTS(SELECT * FROM PortalBizDocs WHERE numDomainID=@numTargetDomainID)
BEGIN
	--delete from PortalBizDocs WHERE numDomainID=176
	INSERT INTO dbo.PortalBizDocs
        ( numBizDocID, numDomainID )
	SELECT numListItemID,@numTargetDomainID FROM dbo.ListDetails WHERE numListID=27 AND constFlag=1
END



---Set BizDoc Type Filter for Sales and Purchase BizDoc Type
INSERT INTO dbo.BizDocFilter( numBizDoc ,tintBizocType ,numDomainID)
SELECT numListItemID,1,@numTargetDomainID FROM dbo.ListDetails WHERE numListID=27 AND constFlag=1 AND vcData IN ('Invoice','Sales Opportunity','Sales Order','Sales Credit Memo','Fulfillment Order','Credit Memo','Refund Receipt','RMA','Packing Slip')

INSERT INTO dbo.BizDocFilter( numBizDoc ,tintBizocType ,numDomainID)
SELECT numListItemID,2,@numTargetDomainID FROM dbo.ListDetails WHERE numListID=27 AND constFlag=1 AND vcData IN ('Purchase Opportunity','Bill','Purchase Order','Purchase Credit Memo','RMA')



--- Give permission of all tree node to all Groups
exec USP_CreateTreeNavigationForDomain @numTargetDomainID,0

--Create Default BizDoc template for all system bizdocs, Css and template html will be updated though same SP from code.
EXEC dbo.USP_CreateBizDocTemplateByDefault @numDomainID = @numTargetDomainID, -- numeric
    @txtBizDocTemplate = '', -- text
    @txtCss = '', -- text
    @tintMode = 2,
    @txtPackingSlipBizDocTemplate = ''
 

          
 --inserting the details of BizForms Wizard          
                    INSERT  INTO DycFormConfigurationDetails(numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,numAuthGroupID
								,numDomainId,numSurId)
                            SELECT  numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,@numGroupId1,
									@numTargetDomainID,0
                            FROM    DycFormConfigurationDetails
                            WHERE   numDomainID = 0
                                    AND numFormID IN ( 1, 2, 6 )          
          
                    INSERT  INTO DycFormConfigurationDetails(numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,numAuthGroupID
								,numDomainId,numSurId)
                            SELECT  numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,0,
									@numTargetDomainID,0
                            FROM    DycFormConfigurationDetails
                            WHERE   numDomainID = 0
                                    AND numFormID IN ( 3, 4, 5 )          
          
                    INSERT  INTO DycFormConfigurationDetails(numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,numAuthGroupID
								,numDomainId,numSurId)
                            SELECT  numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,numAuthGroupID,
									@numTargetDomainID,0
                            FROM    DycFormConfigurationDetails
                            WHERE   numDomainID = 0
                                    AND numFormID IN ( 7, 8 )          
          
          
          
          ---Insert Default Add Relationship Field Lead/Prospect/Account and Other
          INSERT INTO DycFormConfigurationDetails(numFormId,numFieldId,intColumnNum,intRowNum,
								numDomainId,numUserCntID,numRelCntType,tintPageType,bitCustom)
               SELECT  numFormId,numFieldId,intColumnNum,intRowNum,@numTargetDomainID,0,
								numRelCntType,tintPageType,bitCustom
                     FROM DycFormConfigurationDetails
                     WHERE numDomainID = 0 AND numFormID IN (34,35,36) AND tintPageType=2
          
          ---Set Default Validation for Lead/Prospect/Account
         INSERT INTO DynamicFormField_Validation(numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,
   bitIsRequired,bitIsNumeric,bitIsAlphaNumeric,bitIsEmail,bitIsLengthValidation,intMaxLength,intMinLength,
   bitFieldMessage,vcFieldMessage)
    SELECT numFormId,numFormFieldId,@numTargetDomainID,vcNewFormFieldName,
   bitIsRequired,bitIsNumeric,bitIsAlphaNumeric,bitIsEmail,bitIsLengthValidation,intMaxLength,intMinLength,
   bitFieldMessage,vcFieldMessage
                     FROM DynamicFormField_Validation
                     WHERE numDomainID = 0 AND numFormID IN (34,35,36)
                              
                    
                    DECLARE @numListItemID AS NUMERIC(9)                    
                    DECLARE @numNewListItemID AS NUMERIC(9)                    
                    SELECT TOP 1
                            @numListItemID = numListItemID
                    FROM    ListDetails
                    WHERE   numDomainID = 1
                            AND numListID = 40                    
                    
                    WHILE @numListItemID > 0                    
                        BEGIN                    
                    
                            INSERT  INTO ListDetails
                                    (
                                      numListID,
                                      vcData,
                                      numCreatedBY,
                                      bintCreatedDate,
                                      numModifiedBy,
                                      bintModifiedDate,
                                      bitDelete,
                                      numDomainID,
                                      constFlag,
                                      sintOrder
                                    )
                                    SELECT  numListID,
                                            vcData,
                                            @numNewContactID,
                                            GETUTCDATE(),
                                            @numNewContactID,
                                            GETUTCDATE(),
                                            bitDelete,
                                            @numTargetDomainID,
                                            constFlag,
                                            sintOrder
                                    FROM    ListDetails
                                    WHERE   numListItemID = @numListItemID                    
                            SELECT  @numNewListItemID = @@identity                    
                     
                            INSERT  INTO [state]
                                    (
                                      numCountryID,
                                      vcState,
                                      numCreatedBy,
                                      bintCreatedDate,
                                      numModifiedBy,
                                      bintModifiedDate,
                                      numDomainID,
                                      constFlag
                                    )
                                    SELECT  @numNewListItemID,
                                            vcState,
                                            @numNewContactID,
                                            GETUTCDATE(),
                                            @numNewContactID,
                                            GETUTCDATE(),
                                            @numTargetDomainID,
                                            constFlag
                                    FROM    [State]
                                    WHERE   numCountryID = @numListItemID                    
                            SELECT TOP 1
                                    @numListItemID = numListItemID
                            FROM    ListDetails
                            WHERE   numDomainID = 1
                                    AND numListID = 40
                                    AND numListItemID > @numListItemID                    
                            IF @@rowcount = 0 
                                SET @numListItemID = 0                    
                        END                    
                    
                    --INsert Net Terms
                   	INSERT INTO [ListDetails] ([numListID],[vcData],[numCreatedBY],[bitDelete],[numDomainID],[constFlag],[sintOrder])
								  SELECT 296,'0',1,0,@numTargetDomainID,0,1
						UNION ALL SELECT 296,'7',1,0,@numTargetDomainID,0,2
						UNION ALL SELECT 296,'15',1,0,@numTargetDomainID,0,3
						UNION ALL SELECT 296,'30',1,0,@numTargetDomainID,0,4
						UNION ALL SELECT 296,'45',1,0,@numTargetDomainID,0,5
						UNION ALL SELECT 296,'60',1,0,@numTargetDomainID,0,6
                    --Insert Default email templates
                    INSERT INTO dbo.GenericDocuments (
						VcFileName,
						vcDocName,
						numDocCategory,
						cUrlType,
						vcFileType,
						numDocStatus,
						vcDocDesc,
						numDomainID,
						numCreatedBy,
						bintCreatedDate,
						numModifiedBy,
						bintModifiedDate,
						vcSubject,
						vcDocumentSection,
						numRecID,
						tintCheckOutStatus,
						intDocumentVersion,
						numLastCheckedOutBy,
						dtCheckOutDate,
						dtCheckInDate,
						tintDocumentType,
						numOldSpecificDocID,
						numModuleID
					)  
					SELECT  VcFileName,
							vcDocName,
							numDocCategory,
							cUrlType,
							vcFileType,
							numDocStatus,
							vcDocDesc,
							@numTargetDomainID,
							@numNewContactID,
							GETUTCDATE(),
							@numNewContactID,
							GETUTCDATE(),
							vcSubject,
							vcDocumentSection,
							numRecID,
							tintCheckOutStatus,
							intDocumentVersion,
							numLastCheckedOutBy,
							dtCheckOutDate,
							dtCheckInDate,
							tintDocumentType,
							numOldSpecificDocID,
							numModuleID
					FROM    dbo.GenericDocuments
					WHERE   numDomainID = 0
                       
                    INSERT  INTO UserMaster
                            (
                              vcUserName,
                              vcUserDesc,
                              vcMailNickName,
                              numGroupID,
                              numDomainID,
                              numCreatedBy,
                              bintCreatedDate,
                              numModifiedBy,
                              bintModifiedDate,
                              bitActivateFlag,
                              numUserDetailId,
                              vcEmailID
                            )
                            SELECT  vcFirstName,
                                    vcFirstName,
                                    vcFirstName,
                                    @numGroupId1,
                                    @numTargetDomainID,
                                    @numUserContactID,
                                    GETUTCDATE(),
                                    @numUserContactID,
                                    GETUTCDATE(),
                                    0,
                                    @numNewContactID,
                                    vcEmail
                            FROM    AdditionalContactsInformation
                            WHERE   numContactID = @numAdminContactID                                            
                          
                           INSERT INTO UOM (numDomainId,vcUnitName,tintUnitType,bitEnabled)
			                SELECT @numTargetDomainID, vcUnitName,tintUnitType,bitEnabled FROM UOM WHERE numDomainId=0    
                    
                    if not exists (select * from Currency where numDomainID=@numTargetDomainID)
						insert into Currency (vcCurrencyDesc,chrCurrency,varCurrSymbol,numDomainID,fltExchangeRate,bitEnabled)
							select vcCurrencyDesc, chrCurrency,varCurrSymbol, @numTargetDomainID, fltExchangeRate, bitEnabled from Currency where numDomainID=0
                    
                    ------- Insert Detail For Default Currency In New Domain Entry -------------------------
					DECLARE @numCountryID AS NUMERIC(18,0)
					SELECT @numCountryID = numListItemID FROM dbo.ListDetails WHERE numListID = 40 AND vcData = 'United States' AND numDomainID = @numTargetDomainID

					--IF NOT EXISTS(SELECT * FROM Currency WHERE numCountryId = @numCountryID AND numDomainID = @numTargetDomainID AND vcCurrencyDesc = 'USD-U.S. Dollar')
					IF NOT EXISTS(SELECT * FROM Currency WHERE numDomainID = @numTargetDomainID AND vcCurrencyDesc = 'USD-U.S. Dollar')
					BEGIN
					PRINT 1
						INSERT  INTO dbo.Currency(vcCurrencyDesc,chrCurrency,varCurrSymbol,numDomainID,fltExchangeRate,bitEnabled,numCountryId)
						SELECT  TOP 1 vcCurrencyDesc,chrCurrency,varCurrSymbol,@numTargetDomainID,fltExchangeRate,1 AS bitEnabled,@numCountryId FROM dbo.Currency 
						WHERE vcCurrencyDesc = 'USD-U.S. Dollar' AND numDomainID=0
						UNION 
						SELECT  vcCurrencyDesc,chrCurrency,varCurrSymbol,@numTargetDomainID,fltExchangeRate,0 AS bitEnabled,NULL FROM dbo.Currency 
						WHERE vcCurrencyDesc <> 'USD-U.S. Dollar' AND numDomainID=0
					END
					ELSE
						BEGIN
						PRINT 2
							UPDATE Currency SET numCountryId = @numCountryID, bitEnabled = 1
							WHERE numDomainID = @numTargetDomainID
							AND vcCurrencyDesc = 'USD-U.S. Dollar'                        
						PRINT 3							
						END
						
					--Set Default Currency
                    DECLARE @numCurrencyID AS NUMERIC(18)
                    SET @numCurrencyID=(SELECT TOP 1 numCurrencyID FROM Currency WHERE (bitEnabled=1 or chrCurrency='USD') AND numDomainID=@numTargetDomainID)
                   
					--Set Default Country
					DECLARE @numDefCountry AS NUMERIC(18)
                    SET @numDefCountry=(SELECT TOP 1 numListItemID FROM ListDetails where numlistid=40 AND numDomainID=@numTargetDomainID and vcdata like '%United States%')		
					                    
                    UPDATE  Domain
                    SET     numDivisionID = @numNewDivisionID,
                            numAdminID = @numNewContactID,numCurrencyID=@numCurrencyID,numDefCountry=isnull(@numDefCountry,0)
                    WHERE   numdomainID = @numTargetDomainID                                      
                         	
					------------------


                         
                    INSERT  INTO Subscribers
                            (
                              numDivisionID,
                              numAdminContactID,
                              numTargetDomainID,
                              numDomainID,
                              numCreatedBy,
                              dtCreatedDate,
                              numModifiedBy,
                              dtModifiedDate,
                              dtEmailStartDate,
                              dtEmailEndDate
                            )
                    VALUES  (
                              @numDivisionID,
                              @numNewContactID,
                              @numTargetDomainID,
                              @numDomainID,
                              @numUserContactID,
                              GETUTCDATE(),
                              @numUserContactID,
                              GETUTCDATE(),
                              dbo.GetUTCDateWithoutTime(),
                              dbo.GetUTCDateWithoutTime()
                            )                                    
                    SET @numSubscriberID = @@identity                        
                      
                      
                              
                END                                
            ELSE 
                BEGIN                        
                    SELECT  @numSubscriberID = numSubscriberID
                    FROM    Subscribers
                    WHERE   numDivisionID = @numDivisionID
                            AND numDomainID = @numDomainID                                
                    UPDATE  Subscribers
                    SET     bitDeleted = 0
                    WHERE   numSubscriberID = @numSubscriberID                                
                END                                 
                                   
                     
        END                                    
    ELSE 
        BEGIN                                    
                                  
            DECLARE @bitOldStatus AS BIT                                  
            SET @bitExists = 1                                 
            SELECT  @bitOldStatus = bitActive,
                    @numTargetDomainID = numTargetDomainID
            FROM    Subscribers
            WHERE   numSubscriberID = @numSubscriberID                                    
                                  
            IF ( @bitOldStatus = 1
                 AND @bitActive = 0
               ) 
                UPDATE  Subscribers
                SET     dtSuspendedDate = GETUTCDATE()
                WHERE   numSubscriberID = @numSubscriberID                                  
                                  
            UPDATE  Subscribers
            SET     intNoofUsersSubscribed = @intNoofUsersSubscribed,
                    intNoOfPartners = @intNoOfPartners,
                    dtSubStartDate = @dtSubStartDate,
                    dtSubEndDate = @dtSubEndDate,
                    bitTrial = @bitTrial,
                    numAdminContactID = @numAdminContactID,
                    bitActive = @bitActive,
                    vcSuspendedReason = @vcSuspendedReason,
                    numModifiedBy = @numUserContactID,
                    dtModifiedDate = GETUTCDATE(),
                    intNoofPartialSubscribed = @intNoofPartialSubscribed,
                    intNoofMinimalSubscribed = @intNoofMinimalSubscribed,
					dtEmailStartDate= @dtEmailStartDate,
					dtEmailEndDate= @dtEmailEndDate,
					intNoOfEmail=@intNoOfEmail
            WHERE   numSubscriberID = @numSubscriberID                 
                       
                         UPDATE  Domain
                    SET     tintLogin = @tintLogin,
							numAdminID = @numAdminContactID
                    WHERE   numdomainID = @numTargetDomainID   
                           
            DECLARE @hDoc1 INT                                                              
            EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strUsers                                                              
                                                               
            INSERT  INTO UserMaster
                    (
                      vcUserName,
                      vcMailNickName,
                      vcUserDesc,
                      numGroupID,
                      numDomainID,
                      numCreatedBy,
                      bintCreatedDate,
                      numModifiedBy,
                      bintModifiedDate,
                      bitActivateFlag,
                      numUserDetailId,
                      vcEmailID,
                      vcPassword
                    )
                    SELECT  UserName,
                            UserName,
                            UserName,
                            0,
                            @numTargetDomainID,
                            @numUserContactID,
                            GETUTCDATE(),
                            @numUserContactID,
                            GETUTCDATE(),
                            Active,
                            numContactID,
                            Email,
                            vcPassword
                    FROM    ( SELECT    *
                              FROM      OPENXML (@hDoc1, '/NewDataSet/Users[numUserID=0]',2)
                                        WITH ( numContactID NUMERIC(9), numUserID NUMERIC(9), Email VARCHAR(100), vcPassword VARCHAR(100), UserName VARCHAR(100), Active TINYINT )
                            ) X                                
                
                              
            UPDATE  UserMaster
            SET     vcUserName = X.UserName,
                    numModifiedBy = @numUserContactID,
                    bintModifiedDate = GETUTCDATE(),
                    vcEmailID = X.Email,
                    vcPassword = X.vcPassword,
                    bitActivateFlag = Active
            FROM    ( SELECT    numUserID AS UserID,
                                numContactID,
                                Email,
                                vcPassword,
                                UserName,
                                Active
                      FROM      OPENXML (@hDoc1, '/NewDataSet/Users[numUserID>0]',2)
                                WITH ( numContactID NUMERIC(9), numUserID NUMERIC(9), Email VARCHAR(100), vcPassword VARCHAR(100), UserName VARCHAR(100), Active TINYINT )
                    ) X
            WHERE   numUserID = X.UserID                                                              
                                                               
                                                               
            EXEC sp_xml_removedocument @hDoc1                               
                                   
        END                                    
    SELECT  @numSubscriberID
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageUserSMTPDetails')
DROP PROCEDURE USP_ManageUserSMTPDetails
GO
CREATE PROCEDURE  USP_ManageUserSMTPDetails
    @numUserId numeric(18, 0),
	@numDomainId numeric(18, 0),
    @vcSMTPUserName varchar(50),
    @vcSMTPPassword varchar(100),
    @vcSMTPServer varchar(100),
    @numSMTPPort numeric(18, 0),
    @bitSMTPSSL bit,
    @bitSMTPServer bit,
    @bitSMTPAuth bit
AS                                    
BEGIN

	IF NOT EXISTS(SELECT 1 FROM [dbo].[SMTPUserDetails] WHERE [numDomainId] = @numDomainId AND [numUserId] = @numUserId)
	BEGIN
		INSERT INTO [dbo].[SMTPUserDetails] ([numDomainId], [numUserId], [vcSMTPUserName], [vcSMTPPassword], [vcSMTPServer], [numSMTPPort], [bitSMTPSSL], [bitSMTPServer], [bitSMTPAuth])
		SELECT @numDomainId, @numUserId, @vcSMTPUserName, @vcSMTPPassword, @vcSMTPServer, @numSMTPPort, @bitSMTPSSL, @bitSMTPServer, @bitSMTPAuth
	END
	ELSE
	BEGIN
		UPDATE [dbo].[SMTPUserDetails]
			SET    [numDomainId] = @numDomainId, [numUserId] = @numUserId, [vcSMTPUserName] = @vcSMTPUserName, [vcSMTPPassword] = @vcSMTPPassword, [vcSMTPServer] = @vcSMTPServer, [numSMTPPort] = @numSMTPPort, [bitSMTPSSL] = @bitSMTPSSL, [bitSMTPServer] = @bitSMTPServer, [bitSMTPAuth] = @bitSMTPAuth
			WHERE  [numDomainId] = @numDomainId
				   AND [numUserId] = @numUserId
	END
END
/****** Object:  StoredProcedure [dbo].[USP_OPPBizDocItems]    Script Date: 07/26/2008 16:20:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                                                                                                                                        
-- exec USP_OPPBizDocItems @numOppId=12388,@numOppBizDocsId=11988,@numDomainID=110
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_oppbizdocitems' ) 
    DROP PROCEDURE usp_oppbizdocitems
GO
CREATE PROCEDURE [dbo].[USP_OPPBizDocItems]
    (
      @numOppId AS NUMERIC(9) = NULL,
      @numOppBizDocsId AS NUMERIC(9) = NULL,
      @numDomainID AS NUMERIC(9) = 0      
    )
AS 
    DECLARE @DivisionID AS NUMERIC(9)      
    DECLARE @tintTaxOperator AS TINYINT   
                                                                                                                                                                                                                                                                                                                                          
    SELECT  @DivisionID = numDivisionID, @tintTaxOperator = [tintTaxOperator]           
    FROM    OpportunityMaster
    WHERE   numOppId = @numOppId                                                                                                                                                                          
--Decimal points to show in bizdoc 
    DECLARE @DecimalPoint AS TINYINT
    SELECT  @DecimalPoint = tintDecimalPoints
    FROM    domain
    WHERE   numDomainID = @numDomainID
                                                                                          
    DECLARE @strSQL AS VARCHAR(MAX)                                                                                          
    DECLARE @strSQL1 AS VARCHAR(8000)                                       
    DECLARE @strSQLCusFields VARCHAR(1000)                      
    DECLARE @strSQLEmpFlds VARCHAR(500)                                                                         
    SET @strSQLCusFields = ''                                                                                          
    SET @strSQLEmpFlds = ''                                                                                          
    DECLARE @tintType AS TINYINT   
    DECLARE @tintOppType AS TINYINT                                                      
    DECLARE @intRowNum AS INT                                                  
    DECLARE @numFldID AS VARCHAR(15)                                          
    DECLARE @vcFldname AS VARCHAR(50)                                            
    DECLARE @numBizDocId AS NUMERIC(9)
    DECLARE @numBizDocTempID AS NUMERIC(9) ;
        SET @numBizDocTempID = 0
                                           
    SELECT  @numBizDocTempID = ISNULL(numBizDocTempID, 0),
            @numBizDocId = numBizDocId
    FROM    OpportunityBizDocs
    WHERE   numOppBizDocsId = @numOppBizDocsId
    SELECT  @tintType = tintOppType,
            @tintOppType = tintOppType
    FROM    OpportunityMaster
    WHERE   numOppID = @numOppId                                                                                          
                                                                                   
    IF @tintType = 1 
        SET @tintType = 7
    ELSE 
        SET @tintType = 8                                                                                      
                                                                                
                                                                                        
                                                                                          
 

    SELECT  *
    INTO    #Temp1
    FROM    ( SELECT   Opp.vcitemname AS vcItemName,
                        ISNULL(OBD.numOppBizDocItemID, 0) OppBizDocItemID,
                        CASE WHEN charitemType = 'P' THEN 'Product'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS charitemType,
                        CASE WHEN charitemType = 'P' THEN 'Inventory'
	                         WHEN charitemType = 'N' THEN 'Non-Inventory'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS vcItemType,
                        CASE WHEN bitShowDeptItemDesc = 0
                             THEN CONVERT(VARCHAR(2500), OBD.vcitemDesc)
                             WHEN bitShowDeptItemDesc = 1
                                  AND bitKitParent = 1
                                  AND bitAssembly = 0
                             THEN dbo.fn_PopulateKitDesc(opp.numOppId,
                                                         opp.numoppitemtCode)
                             WHEN bitShowDeptItemDesc = 1
                                  AND bitKitParent = 1
                                  AND bitAssembly = 1
                             THEN dbo.fn_PopulateAssemblyDesc(opp.numoppitemtCode, OBD.numUnitHour)
                             ELSE CONVERT(VARCHAR(2500), OBD.vcitemDesc)
                        END AS txtItemDesc,                                      
--vcitemdesc as [desc],                                      
                        dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),
                                             i.numItemCode, @numDomainID,
                                             ISNULL(opp.numUOMId, 0))
                        * OBD.numUnitHour AS numUnitHour,
                        CONVERT(DECIMAL(18, 4), dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0))
                        * OBD.monPrice) monPrice,
                        CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4), OBD.monTotAmount)) Amount,
                        OBD.monTotAmount/*Fo calculating sum*/,
--                        CASE WHEN @tintOppType = 1
--                             THEN CASE WHEN bitTaxable = 0 THEN 0
--                                       WHEN bitTaxable = 1
--                                       THEN ( SELECT    fltPercentage
--                                              FROM      OpportunityBizDocTaxItems
--                                              WHERE     numOppBizDocID = @numOppBizDocsId
--                                                        AND numTaxItemID = 0
--                                            )
--                                  END
--                             ELSE 0
--                        END AS Tax,
                        ISNULL(CONVERT(VARCHAR, i.monListPrice), 0) AS monListPrice,
                        i.numItemCode AS ItemCode,
                        opp.numoppitemtCode,
                        L.vcdata AS numItemClassification,
                        CASE WHEN bitTaxable = 0 THEN 'No'
                             ELSE 'Yes'
                        END AS Taxable,
                        numIncomeChartAcntId AS itemIncomeAccount,
                        'NI' AS ItemType,
                        ISNULL(GJH.numJournal_Id, 0) AS numJournalId,
                        ( SELECT TOP 1
                                    ISNULL(GJD.numTransactionId, 0)
                          FROM      General_Journal_Details GJD
                          WHERE     GJH.numJournal_Id = GJD.numJournalId
                                    AND GJD.chBizDocItems = 'NI'
                                    AND GJD.numoppitemtCode = opp.numoppitemtCode
                        ) AS numTransactionId,
                        ISNULL(Opp.vcModelID, '') vcModelID,
                        dbo.USP_GetAttributes(OBD.numWarehouseItmsID,
                                              bitSerialized) AS vcAttributes,
                        ISNULL(vcPartNo, '') AS vcPartNo,
                        ( ISNULL(OBD.monTotAmtBefDiscount, OBD.monTotAmount)
                          - OBD.monTotAmount ) AS DiscAmt,
                        OBD.vcNotes,
                        OBD.vcTrackingNo,
                        OBD.vcShippingMethod,
                        OBD.monShipCost,
                        [dbo].[FormatedDateFromDate](OBD.dtDeliveryDate,
                                                     @numDomainID) dtDeliveryDate,
                        Opp.numWarehouseItmsID,
--ISNULL(I.vcUnitofMeasure,'Units') vcUnitofMeasure,
                        ISNULL(u.vcUnitName, '') numUOMId,
                        ISNULL(Opp.vcManufacturer, '') AS vcManufacturer,
                        ISNULL(V.monCost, 0) AS VendorCost,
                        Opp.vcPathForTImage,
                        i.[fltLength],
                        i.[fltWidth],
                        i.[fltHeight],
                        i.[fltWeight],
                        ISNULL(I.numVendorID, 0) numVendorID,
                        Opp.bitDropShip AS DropShip,
                        SUBSTRING(( SELECT  ' ,' + vcSerialNo
                                            + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                                   THEN ' ('
                                                        + CONVERT(VARCHAR(15), oppI.numQty)
                                                        + ')'
                                                   ELSE ''
                                              END
                                    FROM    OppWarehouseSerializedItem oppI
                                            JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                    WHERE   oppI.numOppID = Opp.numOppId
                                            AND oppI.numOppItemID = Opp.numoppitemtCode
                                            AND 1=(CASE WHEN @tintOppType=1 then CASE WHEN oppI.numOppBizDocsID=OBD.numOppBizDocID THEN 1 ELSE 0 END
														ELSE 1 END)
                                    ORDER BY vcSerialNo
                                  FOR
                                    XML PATH('')
                                  ), 3, 200000) AS SerialLotNo,
                        ISNULL(opp.numUOMId, 0) AS numUOM,
                        ISNULL(u.vcUnitName, '') vcUOMName,
                        dbo.fn_UOMConversion(opp.numUOMId, Opp.numItemCode,
                                             @numDomainID, NULL) AS UOMConversionFactor,
                        ISNULL(Opp.numSOVendorId, 0) AS numSOVendorId,
                        [dbo].FormatedDateTimeFromDate(OBD.dtRentalStartDate,
                                                       @numDomainID) dtRentalStartDate,
                        [dbo].FormatedDateTimeFromDate(OBD.dtRentalReturnDate,
                                                       @numDomainID) dtRentalReturnDate,
-- Added for packing slip
                        ISNULL(W.vcWareHouse, '') AS Warehouse,
                        CASE WHEN charitemType = 'P' THEN WI.vcWHSKU
                             ELSE i.vcSKU
                        END vcSKU,
                        CASE WHEN charitemType = 'P' THEN WI.vcBarCode
                             ELSE i.numBarCodeId
                        END vcBarcode,
                        ISNULL(opp.numClassID, 0) AS numClassID,
                        ISNULL(opp.numProjectID, 0) AS numProjectID,
                        i.numShipClass,OBD.numOppBizDocID,WL.vcLocation,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
                        opp.numOppId,OBD.numUnitHour AS numUnitHourOrig,ISNULL(I.numItemCode,0) AS [numItemCode]
              FROM      OpportunityItems opp
                        JOIN OpportunityBizDocItems OBD ON OBD.numOppItemID = opp.numoppitemtCode
                        LEFT JOIN item i ON opp.numItemCode = i.numItemCode
                        LEFT JOIN ListDetails L ON i.numItemClassification = L.numListItemID
                        LEFT JOIN Vendor V ON V.numVendorID = @DivisionID
                                              AND V.numItemCode = opp.numItemCode
                        LEFT JOIN General_Journal_Header GJH ON opp.numoppid = GJH.numOppId
                                                                AND GJH.numBizDocsPaymentDetId IS NULL
                                                                AND GJH.numOppBizDocsId = @numOppBizDocsId
                        LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId                                    
-- left join General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId And GJD.chBizDocItems='NI' And GJD.numoppitemtCode=opp.numoppitemtCode                                       
                        LEFT JOIN dbo.WareHouseItems WI ON opp.numWarehouseItmsID = WI.numWareHouseItemID
                                                           AND WI.numDomainID = @numDomainID
                        LEFT JOIN dbo.Warehouses W ON WI.numDomainID = W.numDomainID
                                                      AND WI.numWareHouseID = W.numWareHouseID
                        LEFT JOIN dbo.WarehouseLocation WL ON WL.numWLocationID = WI.numWLocationID
              WHERE     opp.numOppId = @numOppId
                        AND ( bitShowDeptItem IS NULL
                              OR bitShowDeptItem = 0
                            )
                        AND OBD.numOppBizDocID = @numOppBizDocsId
       
--For Kit Items where following condition is true( Show dependant items as line items on BizDoc (instead of kit item name))
-- Commented by chintan, Reason: Bug #1829 
/*
union
select Opp.vcitemname as Item,                                      
case when i.charitemType='P' then 'Product' when i.charitemType='S' then 'Service' end as type,                                      
convert(varchar(500),OBD.vcitemDesc) as [Desc],                                      
--vcitemdesc as [desc],                                      
OBD.numUnitHour*intQuantity as Unit,
CONVERT(VARCHAR,CONVERT(DECIMAL(18, 4), OBD.monPrice)) Price,
CONVERT(VARCHAR,CONVERT(DECIMAL(18, 4), OBD.monTotAmount)) Amount,
OBD.monTotAmount/*Fo calculating sum*/,
case when @tintOppType=1 then case when i.bitTaxable =0 then 0 when i.bitTaxable=1 then (select fltPercentage from OpportunityBizDocTaxItems where numOppBizDocID=@numOppBizDocsId and numTaxItemID=0) end else 0 end  as Tax,                                    
isnull(convert(varchar,i.monListPrice),0) as monListPrice,i.numItemCode as ItemCode,opp.numoppitemtCode,                                      
L.vcdata as vcItemClassification,case when i.bitTaxable=0 then 'No' else 'Yes' end as Taxable,i.numIncomeChartAcntId as itemIncomeAccount,                                      
'NI' as ItemType                                      
,isnull(GJH.numJournal_Id,0) as numJournalId,(SELECT TOP 1 isnull(GJD.numTransactionId, 0) FROM General_Journal_Details GJD WHERE GJH.numJournal_Id = GJD.numJournalId And GJD.chBizDocItems = 'NI' And GJD.numoppitemtCode = opp.numoppitemtCode) as numTransactionId,isnull(Opp.vcModelID,'') vcModelID, dbo.USP_GetAttributes(OBD.numWarehouseItmsID,i.bitSerialized) as Attributes,isnull(vcPartNo,'') as Part 
,(isnull(OBD.monTotAmtBefDiscount,OBD.monTotAmount)-OBD.monTotAmount) as DiscAmt,OBD.vcNotes,OBD.vcTrackingNo,OBD.vcShippingMethod,OBD.monShipCost,[dbo].[FormatedDateFromDate](OBD.dtDeliveryDate,@numDomainID) dtDeliveryDate,Opp.numWarehouseItmsID,
--ISNULL(I.vcUnitofMeasure,'Units') vcUnitofMeasure,
ISNULL(u.vcUnitName,'') vcUnitofMeasure,
isnull(i2.vcManufacturer,'') as vcManufacturer,
isnull(V.monCost,0) as VendorCost,
Opp.vcPathForTImage,i.[fltLength],i.[fltWidth],i.[fltHeight],i.[fltWeight],isnull(I.numVendorID,0) numVendorID,Opp.bitDropShip AS DropShip,
SUBSTRING(
(SELECT ',' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN ' (' + CONVERT(VARCHAR(15),oppI.numQty) + ')' ELSE '' end 
FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=Opp.numOppId and oppI.numOppItemID=Opp.numoppitemtCode 
ORDER BY vcSerialNo FOR XML PATH('')),2,200000) AS SerialLotNo,isnull(opp.numUOMId,0) AS numUOM,ISNULL(u.vcUnitName,'') vcUOMName,dbo.fn_UOMConversion(opp.numUOMId,Opp.numItemCode,@numDomainID,null) AS UOMConversionFactor,
ISNULL(Opp.numSOVendorId,0) as numSOVendorId,
[dbo].FormatedDateTimeFromDate(OBD.dtRentalStartDate,@numDomainID) dtRentalStartDate,
[dbo].FormatedDateTimeFromDate(OBD.dtRentalReturnDate,@numDomainID) dtRentalReturnDate
from OpportunityItems opp       
join OpportunityBizDocItems OBD on OBD.numOppItemID= opp.numoppitemtCode                                   
join  OpportunityKitItems OppKitItems      
on OppKitItems.numOppKitItem=opp.numoppitemtCode                                     
join item i on OppKitItems.numChildItem=i.numItemCode      
join item i2 on opp.numItemCode=i2.numItemCode                                      
left join ListDetails L on i.numItemClassification=L.numListItemID
left join Vendor V on V.numVendorID=@DivisionID and V.numItemCode=opp.numItemCode                                       
left join General_Journal_Header GJH on opp.numoppid=GJH.numOppId And GJH.numBizDocsPaymentDetId is null And GJH.numOppBizDocsId=@numOppBizDocsId                                      
-- left join General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId And GJD.chBizDocItems='NI' And GJD.numoppitemtCode=opp.numoppitemtCode
LEFT JOIN  UOM u ON u.numUOMId = opp.numUOMId                                    
where opp.numOppId=@numOppId and i2.bitShowDeptItem=1      and   OBD.numOppBizDocID=@numOppBizDocsId   */
--union
--select case when numcategory=1 then                             
--case when isnull(te.numcontractid,0) = 0 then 'Time' else 'Time(Contract)' end                              
--when numcategory=2 then                             
--case when isnull(te.numcontractid,0) = 0 then 'Expense' else 'Expense(Contract)' end                              
--end  as item,                           
--case when numcategory=1 then 'Time' when numcategory=2 then 'Expense' end                                      
-- as Type,                                      
--convert(varchar(100),txtDesc) as [Desc],                                      
----txtDesc as [Desc],                                      
--convert(decimal(18,2),datediff(minute,te.dtfromdate,te.dttodate))/60 as unit,                                      
--convert(varchar(100),monamount) as Price,                                      
--case when isnull(te.numcontractid,0) = 0                    
--then                    
-- case when numCategory =1  then                    
--   isnull(convert(decimal(18,2),datediff(minute,dtfromdate,dttodate))*monAmount/60,0)                    
--   when numCategory =2  then                    
--   isnull(monamount,0)                    
--                     
--   end                    
--else                                  
--  0                    
--end as amount,                                      
--0.00 as Tax,'' as listPrice,0 as ItemCode,0 as numoppitemtCode,'' as vcItemClassification,                                      
--'No' as Taxable,0 as itemIncomeAccount,'TE' as ItemType                                      
--,0 as numJournalId,0 as numTransactionId ,'' vcModelID,''as Attributes,'' as Part,0 as DiscAmt ,'' as vcNotes,'' as vcTrackingNo                                      
--from timeandexpense TE                            
--                                    
--where                                       
--(numtype= 1)   and   numDomainID=@numDomainID and                                    
--(numOppBizDocsId = @numOppBizDocsId  or numoppid = @numOppId)
            ) X


    IF @DecimalPoint = 1 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 1), monPrice),
                    Amount = CONVERT(DECIMAL(18, 1), Amount)
        END
    IF @DecimalPoint = 2 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 2), monPrice),
                    Amount = CONVERT(DECIMAL(18, 2), Amount)
        END
    IF @DecimalPoint = 3 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 3), monPrice),
                    Amount = CONVERT(DECIMAL(18, 3), Amount)
        END
    IF @DecimalPoint = 4 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 4), monPrice),
                    Amount = CONVERT(DECIMAL(18, 4), Amount)
        END

    DECLARE @strSQLUpdate AS VARCHAR(2000);SET @strSQLUpdate=''
   
    EXEC ( 'alter table #Temp1 add [TotalTax] money'
        )

    IF @tintOppType = 1 AND @tintTaxOperator = 1 --add Sales tax
	--set @strSQLUpdate=@strSQLUpdate +',[TotalTax]= dbo.fn_CalSalesTaxAmt('+ convert(varchar(20),@numOppBizDocsId)+','+ convert(varchar(20),@numDomainID)+')*Amount/100'
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1)*Amount/100'
    ELSE IF @tintOppType = 1 AND @tintTaxOperator = 2 -- remove sales tax
            SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
    ELSE IF @tintOppType = 1 
                SET @strSQLUpdate = @strSQLUpdate
                    + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
                    + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
                    + CONVERT(VARCHAR(20), @numOppId) +','+
                    + 'numoppitemtCode,1)*Amount/100'
    ELSE 
                SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'


 DECLARE @vcTaxName AS VARCHAR(100)
    DECLARE @numTaxItemID AS NUMERIC(9)
    SET @numTaxItemID = 0
    SELECT TOP 1
            @vcTaxName = vcTaxName,
            @numTaxItemID = numTaxItemID
    FROM    TaxItems
    WHERE   numDomainID = @numDomainID


    WHILE @numTaxItemID > 0
        BEGIN

            EXEC ( 'alter table #Temp1 add [' + @vcTaxName + '] money'
                )

            IF @tintOppType = 1 
                SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName
                    + ']= dbo.fn_CalBizDocTaxAmt('
                    + CONVERT(VARCHAR(20), @numDomainID) + ','
                    + CONVERT(VARCHAR(20), @numTaxItemID) + ','
                    + CONVERT(VARCHAR(20), @numOppId) +','+
                    + 'numoppitemtCode,1)*Amount/100'
            ELSE 
                SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName + ']= 0'

            SELECT TOP 1
                    @vcTaxName = vcTaxName,
                    @numTaxItemID = numTaxItemID
            FROM    TaxItems
            WHERE   numDomainID = @numDomainID
                    AND numTaxItemID > @numTaxItemID

            IF @@rowcount = 0 
                SET @numTaxItemID = 0


        END

    IF @strSQLUpdate <> '' 
        BEGIN
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode'
                + @strSQLUpdate + ' where ItemCode>0'
            PRINT @strSQLUpdate
            EXEC ( @strSQLUpdate
                )
        END

    DECLARE @vcDbColumnName NVARCHAR(50)
    SELECT TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow                                                                                          
    WHILE @intRowNum > 0                                                                                  
        BEGIN    
            EXEC
                ( 'alter table #Temp1 add [' + @vcDbColumnName
                  + '] varchar(100)'
                )
	
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode,['
                + @vcDbColumnName + ']=dbo.GetCustFldValueItem(' + @numFldID
                + ',ItemCode) where ItemCode>0'
            EXEC ( @strSQLUpdate
                )
		                   
            SELECT TOP 1
                    @numFldID = numfieldID,
                    @vcFldname = vcFieldName,
                    @vcDbColumnName = vcDbColumnName,
                    @intRowNum = ( tintRow + 1 )
            FROM    View_DynamicCustomColumns
            WHERE   numFormID = @tintType
                    AND Grp_id = 5
                    AND numDomainID = @numDomainID
                    AND numAuthGroupID = @numBizDocId
                    AND bitCustom = 1
                    AND tintRow >= @intRowNum
                    AND ISNULL(numRelCntType, 0) = @numBizDocTempID
            ORDER BY tintRow                                                                  
		           
            IF @@rowcount = 0 
                SET @intRowNum = 0                                                                                          
        END


    SELECT  Row_NUmber() OVER ( ORDER BY numoppitemtCode ) RowNumber,
            *
    FROM    #Temp1
    ORDER BY numoppitemtCode ASC 

    DROP TABLE #Temp1

    SELECT  ISNULL(tintOppStatus, 0)
    FROM    OpportunityMaster
    WHERE   numOppID = @numOppId                                                                                                 


IF (SELECT COUNT(*) FROM View_DynamicColumns WHERE   numFormID = @tintType AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId AND vcFieldType = 'R'AND ISNULL(numRelCntType, 0) = @numBizDocTempID)=0
BEGIN            
	INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType)
	select numFormId,numFieldId,1,tintRow,@numDomainID,@numBizDocId,@numBizDocTempID
	 FROM View_DynamicDefaultColumns
	 where numFormId=@tintType and bitDefault=1 and numDomainID=@numDomainID order by tintRow ASC	
	 
	 
	 IF(SELECT COUNT(*) FROM DycFrmConfigBizDocsSumm Ds where numBizDocID=@numBizDocId and Ds.numFormID=@tintType 
	and isnull(Ds.numBizDocTempID,0)=@numBizDocTempID and Ds.numDomainID=@numDomainID)=0
	BEGIN
		INSERT INTO DycFrmConfigBizDocsSumm (numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID)
		 SELECT @tintType,@numBizDocId,DFM.numFieldID,@numDomainID,@numBizDocTempID FROM
		 DycFormField_Mapping DFFM 
			JOIN DycFieldMaster DFM on DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
			where numFormID=16 and DFFM.numDomainID is null
	END

END      


    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicColumns
    WHERE   numFormID = @tintType
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND vcFieldType = 'R'
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    UNION
    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            'C' vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow
GO
/****** Object:  StoredProcedure [dbo].[USP_OPPDetails]    Script Date: 03/25/2009 15:10:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By Anoop Jayaraj
-- [dbo].[USP_OPPDetails] 1362,1,-333
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppdetails')
DROP PROCEDURE usp_oppdetails
GO
CREATE PROCEDURE [dbo].[USP_OPPDetails]
(
               @numOppID             AS NUMERIC(9)  = NULL,
               @numDomainID          AS NUMERIC(9)  = 0,
               @ClientTimeZoneOffset AS INT)
AS
	
  SELECT Opp.numoppid,
         numCampainID,
         ADC.vcFirstname + ' ' + ADC.vcLastName AS numContactId,
         isnull(ADC.vcEmail,'') AS vcEmail,
         Opp.txtComments,
         Opp.numDivisionID,
         tintOppType,
         ISNULL(dateadd(MINUTE,-@ClientTimeZoneOffset,bintAccountClosingDate),'') AS bintAccountClosingDate,
         Opp.numContactID AS ContactID,
         tintSource,
         C2.vcCompanyName + Case when isnull(D2.numCompanyDiff,0)>0 then  '  ' + dbo.fn_getlistitemname(D2.numCompanyDiff) + ':' + isnull(D2.vcCompanyDiff,'') else '' end as vcCompanyname,
         D2.tintCRMType,
         Opp.vcPoppName,
         intpEstimatedCloseDate,
         [dbo].[GetDealAmount](@numOppID,getutcdate(),0) AS monPAmount,
         lngPConclAnalysis,
         monPAmount AS OppAmount,
         numSalesOrPurType,
         Opp.tintActive,
         dbo.fn_GetContactName(Opp.numCreatedby)
           + ' '
           + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintCreatedDate)) AS CreatedBy,
         dbo.fn_GetContactName(Opp.numModifiedBy)
           + ' '
           + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintModifiedDate)) AS ModifiedBy,
         dbo.fn_GetContactName(Opp.numRecOwner) AS RecordOwner,
         Opp.numRecOwner,
         isnull(tintshipped,0) AS tintshipped,
         isnull(tintOppStatus,0) AS tintOppStatus,
         dbo.OpportunityLinkedItems(@numOppID) AS NoOfProjects,
         Opp.numAssignedTo,
         Opp.numAssignedBy,
         (SELECT COUNT(* )
          FROM   dbo.GenericDocuments
          WHERE  numRecID = @numOppID
                 AND vcDocumentSection = 'O') AS DocumentCount,
         isnull(OPR.numRecurringId,0) AS numRecurringId,
		 OPR.dtRecurringDate AS dtLastRecurringDate,
         D2.numTerID,
         Link.numOppID AS numParentOppID,
         Link.vcPoppName AS ParentOppName,
         Link.vcSource AS Source,
         Link.numParentProjectID,
         Link.vcProjectName,
         ISNULL(C.varCurrSymbol,'') varCurrSymbol,
         ISNULL(Opp.fltExchangeRate,0) AS [fltExchangeRate],
         ISNULL(C.chrCurrency,'') AS [chrCurrency],
         ISNULL(C.numCurrencyID,0) AS [numCurrencyID],
		 (Select Count(*) from OpportunityBizDocs 
		 where numOppId=@numOppID and bitAuthoritativeBizDocs=1) As AuthBizDocCount,
		 (SELECT COUNT(distinct numChildOppID) FROM [OpportunityLinking] OL WHERE OL.[numParentOppID] = Opp.numOppId) AS ChildCount,
		 (SELECT COUNT(*) FROM [RecurringTransactionReport] RTR WHERE RTR.[numRecTranSeedId] = Opp.numOppId)  AS RecurringChildCount
		 ,Opp.bintCreatedDate,
		 ISNULL(Opp.bitStockTransfer ,0) bitStockTransfer,	ISNULL(Opp.tintTaxOperator,0) AS tintTaxOperator, 
         isnull(Opp.intBillingDays,0) AS numBillingDays,isnull(Opp.bitInterestType,0) AS bitInterestType,
         isnull(opp.fltInterest,0) AS fltInterest,  isnull(Opp.fltDiscount,0) AS fltDiscount,
         isnull(Opp.bitDiscountType,0) AS bitDiscountType,ISNULL(Opp.bitBillingTerms,0) AS bitBillingTerms,ISNULL(OPP.numDiscountAcntType,0) AS numDiscountAcntType,
         (SELECT TOP 1 numCountry FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)) AS [numShipCountry],
		 (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)) AS [numShipState],
		 (SELECT TOP 1 vcState FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)) AS [vcShipState],
		 (SELECT TOP 1 vcCountry FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)) AS [vcShipCountry],
		 (SELECT TOP 1 vcPostalCode FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)) AS [vcPostalCode],
		 ISNULL(vcCouponCode,'') AS [vcCouponCode],ISNULL(Opp.bitPPVariance,0) AS bitPPVariance,
		 Opp.dtItemReceivedDate,ISNULL(bitUseShippersAccountNo,0) [bitUseShippersAccountNo], ISNULL(vcShippersAccountNo,'') [vcShippersAccountNo],
		 ISNULL(bitUseMarkupShippingRate,0) [bitUseMarkupShippingRate],
		 ISNULL(numMarkupShippingRate,0) [numMarkupShippingRate],
		 ISNULL(intUsedShippingCompany,0) [intUsedShippingCompany]
  FROM   OpportunityMaster Opp 
         JOIN divisionMaster D2
           ON Opp.numDivisionID = D2.numDivisionID
         LEFT JOIN CompanyInfo C2
           ON C2.numCompanyID = D2.numCompanyID
         LEFT JOIN (SELECT TOP 1 OM.vcPoppName,
                                 OM.numOppID,
                                 numChildOppID,
                                 vcSource,
                                 numParentProjectID,
                                 vcProjectName
                    FROM   OpportunityLinking
                          LEFT JOIN OpportunityMaster OM
                             ON numOppID = numParentOppID
                          LEFT JOIN [ProjectsMaster]
							 ON numParentProjectID = numProId   
                    WHERE  numChildOppID = @numOppID) Link
           ON Link.numChildOppID = Opp.numOppID
         LEFT OUTER JOIN [Currency] C
					ON C.numCurrencyID = Opp.numCurrencyID
		 LEFT OUTER JOIN [OpportunityRecurring] OPR
					ON OPR.numOppId = Opp.numOppId
		 LEFT OUTER JOIN additionalContactsinformation ADC ON
		 ADC.numdivisionId = D2.numdivisionId AND  ADC.numContactId = Opp.numContactId		
  WHERE  Opp.numOppId = @numOppID
         AND Opp.numDomainID = @numDomainID

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppdetailsdtlpl')
DROP PROCEDURE usp_oppdetailsdtlpl
GO
CREATE PROCEDURE [dbo].[USP_OPPDetailsDTLPL]
(
               @OpportunityId        AS NUMERIC(9)  = NULL,
               @numDomainID          AS NUMERIC(9),
               @ClientTimeZoneOffset AS INT)
AS
  SELECT Opp.numoppid,numCampainID,
--         (SELECT vcdata
--          FROM   listdetails
--          WHERE  numlistitemid = numCampainID) AS numCampainIDName,
		 CAMP.vcCampaignName AS [numCampainIDName],
         dbo.Fn_getcontactname(Opp.numContactId) numContactIdName,
		 numContactId,
         Opp.txtComments,
         Opp.numDivisionID,
         tintOppType,
         Dateadd(MINUTE,-@ClientTimeZoneOffset,bintAccountClosingDate) AS bintAccountClosingDate,
         dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID) AS tintSourceName,
          tintSource,
         Opp.VcPoppName,
         intpEstimatedCloseDate,
         [dbo].[getdealamount](@OpportunityId,Getutcdate(),0) AS CalAmount,
         --monPAmount,
         CONVERT(DECIMAL(10,2),Isnull(monPAmount,0)) AS monPAmount,
		lngPConclAnalysis,
         (SELECT vcdata
          FROM   listdetails
          WHERE  numlistitemid = lngPConclAnalysis) AS lngPConclAnalysisName,
         (SELECT vcdata
          FROM   listdetails
          WHERE  numlistitemid = numSalesOrPurType) AS numSalesOrPurTypeName,
		  numSalesOrPurType,
		 Opp.vcOppRefOrderNo,
		 Opp.vcMarketplaceOrderID,
         C2.vcCompanyName,
         D2.tintCRMType,
         Opp.tintActive,
         dbo.Fn_getcontactname(Opp.numCreatedby)
           + ' '
           + CONVERT(VARCHAR(20),Dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintCreatedDate)) AS CreatedBy,
         dbo.Fn_getcontactname(Opp.numModifiedBy)
           + ' '
           + CONVERT(VARCHAR(20),Dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintModifiedDate)) AS ModifiedBy,
         dbo.Fn_getcontactname(Opp.numRecOwner) AS RecordOwner,
         Opp.numRecOwner,
         Isnull(tintshipped,0) AS tintshipped,
         Isnull(tintOppStatus,0) AS tintOppStatus,
         [dbo].[GetOppStatus](Opp.numOppId) AS vcDealStatus,
         dbo.Opportunitylinkeditems(@OpportunityId) +
         (SELECT COUNT(*) FROM  dbo.Communication Comm JOIN Correspondence Corr ON Comm.numCommId=Corr.numCommID
			WHERE Comm.numDomainId=Opp.numDomainId AND Corr.numDomainID=Comm.numDomainID AND Corr.tintCorrType IN (1,2,3,4) AND Corr.numOpenRecordID=Opp.numOppId) AS NoOfProjects,
         (SELECT A.vcFirstName
                   + ' '
                   + A.vcLastName
          FROM   AdditionalContactsInformation A
          WHERE  A.numcontactid = opp.numAssignedTo) AS numAssignedToName,
		  opp.numAssignedTo,
         Opp.numAssignedBy,
         (SELECT COUNT(* )
          FROM   GenericDocuments
          WHERE  numRecID = @OpportunityId
                 AND vcDocumentSection = 'O') AS DocumentCount,
         CASE 
           WHEN tintOppStatus = 1 THEN 100
           ELSE isnull((SELECT SUM(tintPercentage)
                 FROM   OpportunityStageDetails
                 WHERE  numOppId = @OpportunityId
                        AND bitStageCompleted = 1),0)
         END AS TProgress,
         (SELECT TOP 1 Isnull(varRecurringTemplateName,'')
          FROM   RecurringTemplate
          WHERE  numRecurringId = OPR.numRecurringId) AS numRecurringIdName,
		 OPR.numRecurringId,
         Link.numOppID AS numParentOppID,
         Link.vcPoppName AS ParentOppName,
         Link.vcSource AS Source,
         ISNULL(C.varCurrSymbol,'') varCurrSymbol,
          ISNULL(Opp.fltExchangeRate,0) AS [fltExchangeRate],
           ISNULL(C.chrCurrency,'') AS [chrCurrency],
         ISNULL(C.numCurrencyID,0) AS [numCurrencyID],
         (SELECT COUNT(*) FROM [OpportunityLinking] OL WHERE OL.[numParentOppID] = Opp.numOppId) AS ChildCount,
         (SELECT COUNT(*) FROM [RecurringTransactionReport] RTR WHERE RTR.[numRecTranSeedId] = Opp.numOppId)  AS RecurringChildCount,
         (SELECT COUNT(*) FROM [ShippingReport] WHERE numOppBizDocId IN (SELECT numOppBizDocsId FROM   OpportunityBizDocs BD WHERE  BD.numOppId = @OpportunityId))AS ShippingReportCount,
         ISNULL(Opp.numStatus ,0) numStatus,
         (SELECT vcdata
          FROM   listdetails
          WHERE  numlistitemid = Opp.numStatus) AS numStatusName,
          (SELECT SUBSTRING(
	(SELECT ','+  A.vcFirstName+' ' +A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN '(' + dbo.fn_GetListItemName(SR.numContactType) + ')' ELSE '' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
				AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('')),2,200000)) AS numShareWith,
		ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = @OpportunityId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) AS monDealAmount,
		ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = @OpportunityId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) AS monAmountPaid,
		ISNULL(Opp.tintSourceType,0) AS tintSourceType,
		ISNULL(Opp.tintTaxOperator,0) AS tintTaxOperator,  
             isnull(Opp.intBillingDays,0) AS numBillingDays,isnull(Opp.bitInterestType,0) AS bitInterestType,
             isnull(opp.fltInterest,0) AS fltInterest,  isnull(Opp.fltDiscount,0) AS fltDiscount,
             isnull(Opp.bitDiscountType,0) AS bitDiscountType,ISNULL(Opp.bitBillingTerms,0) AS bitBillingTerms,
             ISNULL(OPP.numDiscountAcntType,0) AS numDiscountAcntType,
              dbo.fn_getOPPAddress(Opp.numOppId,@numDomainID,1) AS BillingAddress,
            dbo.fn_getOPPAddress(Opp.numOppId,@numDomainID,2) AS ShippingAddress,
             ISNULL((SELECT SUBSTRING((SELECT '$^$' + ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=Opp.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OBD.numShipVia  AND vcFieldName='Tracking URL')),'') +'#^#'+ RTRIM(LTRIM(vcTrackingNo) )  FROM dbo.OpportunityBizDocs OBD WHERE OBD.numOppId=Opp.numOppID FOR XML PATH('')),4,200000)),'') AS  vcTrackingNo,
            ISNULL(numPercentageComplete,0) numPercentageComplete,
            dbo.GetListIemName(ISNULL(numPercentageComplete,0)) AS PercentageCompleteName,
            ISNULL(numBusinessProcessID,0) AS [numBusinessProcessID],
            CASE WHEN opp.tintOppType=1 THEN dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID) ELSE '' END AS vcInventoryStatus
            ,ISNULL(bitUseShippersAccountNo,0) [bitUseShippersAccountNo], ISNULL(vcShippersAccountNo,'') [vcShippersAccountNo] 
  FROM   OpportunityMaster Opp
         JOIN divisionMaster D2 ON Opp.numDivisionID = D2.numDivisionID
         JOIN CompanyInfo C2 ON C2.numCompanyID = D2.numCompanyID
         LEFT JOIN (SELECT TOP 1 vcPoppName,
                                 numOppID,
                                 numChildOppID,
                                 vcSource,numParentOppID,OpportunityMaster.numDivisionID AS numParentDivisionID 
                    FROM   OpportunityLinking
                          left JOIN OpportunityMaster
                             ON numOppID = numParentOppID
                    WHERE  numChildOppID = @OpportunityId) Link
           ON Link.numChildOppID = Opp.numOppID
           LEFT OUTER JOIN [Currency] C
					ON C.numCurrencyID = Opp.numCurrencyID
			LEFT OUTER JOIN [OpportunityRecurring] OPR
					ON OPR.numOppId = Opp.numOppId
					 LEFT JOIN AddressDetails AD --Billing add
				ON AD.numDomainID=D2.numDomainID AND AD.numRecordID=D2.numDivisionID AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1
			 LEFT JOIN AddressDetails AD2--Shipping address
				ON AD2.numDomainID=D2.numDomainID AND AD2.numRecordID=D2.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary=1	
		LEFT JOIN dbo.CampaignMaster CAMP ON CAMP.numCampaignID = Opp.numCampainID 		
  WHERE  Opp.numOppId = @OpportunityId
         AND Opp.numDomainID = @numDomainID
/****** Object:  StoredProcedure [dbo].[USP_OPPItemDetails]    Script Date: 09/01/2009 18:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_OPPItemDetails @numItemCode=31455,@OpportunityId=12387,@numDomainId=110
--created by anooop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppitemdetails')
DROP PROCEDURE usp_oppitemdetails
GO
CREATE PROCEDURE [dbo].[USP_OPPItemDetails]
    (
      @numUserCntID NUMERIC(9) = 0,
      @OpportunityId NUMERIC(9) = 0,
      @numDomainId NUMERIC(9),
      @ClientTimeZoneOffset  INT
    )
AS 
    BEGIN
        IF @OpportunityId >0
            BEGIN
                DECLARE @DivisionID AS NUMERIC(9)
                DECLARE @TaxPercentage AS FLOAT
                DECLARE @tintOppType AS TINYINT
                DECLARE @fld_Name AS VARCHAR(500)
                DECLARE @strSQL AS VARCHAR(8000)
                DECLARE @fld_ID AS NUMERIC(9)
                DECLARE @numBillCountry AS NUMERIC(9)
                DECLARE @numBillState AS NUMERIC(9)
                
                SELECT  @DivisionID = numDivisionID,
                        @tintOppType = tintOpptype
                FROM    OpportunityMaster
                WHERE   numOppId = @OpportunityId
               -- Calculate Tax based on Country and state
             /*  SELECT  @numBillState = ISNULL(numState, 0),
						@numBillCountry = ISNULL(numCountry, 0)
			   FROM     dbo.AddressDetails
			   WHERE    numDomainID = @numDomainID
						AND numRecordID = @DivisionID
						AND tintAddressOf = 2
						AND tintAddressType = 1
						AND bitIsPrimary=1
                SET @TaxPercentage = 0
                IF @numBillCountry > 0
                    AND @numBillState > 0 
                    BEGIN
                        IF @numBillState > 0 
                            BEGIN
                                IF EXISTS ( SELECT  COUNT(*)
                                            FROM    TaxDetails
                                            WHERE   numCountryID = @numBillCountry
                                                    AND numStateID = @numBillState
                                                    AND numDomainID = @numDomainID ) 
                                    SELECT  @TaxPercentage = decTaxPercentage
                                    FROM    TaxDetails
                                    WHERE   numCountryID = @numBillCountry
                                            AND numStateID = @numBillState
                                            AND numDomainID = @numDomainID
                                ELSE 
                                    SELECT  @TaxPercentage = decTaxPercentage
                                    FROM    TaxDetails
                                    WHERE   numCountryID = @numBillCountry
                                            AND numStateID = 0
                                            AND numDomainID = @numDomainID
                            END
                    END
                ELSE 
                    IF @numBillCountry > 0 
                        SELECT  @TaxPercentage = decTaxPercentage
                        FROM    TaxDetails
                        WHERE   numCountryID = @numBillCountry
                                AND numStateID = 0
                                AND numDomainID = @numDomainID
               
               */
               

SET @strSQL = ''

--------------Make Dynamic Query--------------
SET @strSQL = @strSQL + '
Select 
Opp.numoppitemtCode,
OM.tintOppType,
Opp.numItemCode,
I.charItemType,
Opp.numOppId,
Opp.bitWorkOrder,
Opp.fltDiscount,
ISNULL(( SELECT numReturnID FROM   Returns WHERE  numOppItemCode = Opp.numoppitemtCode AND numOppId = Opp.numOppID), 0) AS numReturnID,
dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) AS ReturrnedQty,
[dbo].fn_GetReturnStatus(Opp.numoppitemtCode) AS ReturnStatus,
ISNULL(opp.numUOMId, 0) AS numUOM,
(SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
Opp.numUnitHour AS numOriginalUnitHour, 
isnull(M.vcPOppName,''Internal'') as Source,
numSourceID,
  CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END as bitKitParent,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(Opp.numQtyShipped,0) numQtyShipped,ISNULL(Opp.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(OM.fltExchangeRate,0) AS fltExchangeRate,ISNULL(OM.numCurrencyID,0) AS numCurrencyID,ISNULL(OM.numDivisionID,0) AS numDivisionID,
isnull(i.numIncomeChartAcntId,0) as itemIncomeAccount,isnull(i.numAssetChartAcntId,0) as itemInventoryAsset,isnull(i.numCOGsChartAcntId,0) as itemCoGs,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(bitFreeShipping,0) AS [IsFreeShipping],Opp.bitDiscountType,
'

--	IF @fld_Name = 'vcPathForTImage' 
		SET @strSQL = @strSQL + 'Opp.vcPathForTImage,'
--	ELSE IF @fld_Name = 'vcItemName' 
		SET @strSQL = @strSQL + 'Opp.vcItemName,'
--	ELSE IF @fld_Name = 'vcModelID,' 
		SET @strSQL = @strSQL + 'Opp.vcModelID,'
--	ELSE IF @fld_Name = 'vcWareHouse' 
		SET @strSQL = @strSQL + 'vcWarehouse,'
		SET @strSQL = @strSQL + 'WItems.numOnHand,isnull(WItems.numAllocation,0) as numAllocation,'
		SET @strSQL = @strSQL + 'WL.vcLocation,'
--	ELSE IF @fld_Name = 'ItemType' 
		SET @strSQL = @strSQL + 'Opp.vcType ItemType,Opp.vcType,'
--	ELSE IF @fld_Name = 'vcItemDesc' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.vcItemDesc, '''') vcItemDesc,'
--	ELSE IF @fld_Name = 'Attributes' 
		SET @strSQL = @strSQL + 'Opp.vcAttributes,' --dbo.fn_GetAttributes(Opp.numWarehouseItmsID, I.bitSerialized) AS Attributes,
--	ELSE IF @fld_Name = 'bitDropShip' 
		SET @strSQL = @strSQL + 'ISNULL(bitDropShip, 0) as DropShip,CASE ISNULL(bitDropShip, 0) WHEN 1 THEN ''Yes'' ELSE '''' END AS bitDropShip,'
--	ELSE IF @fld_Name = 'numUnitHour' 
		SET @strSQL = @strSQL + 'CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,om.numDomainId, ISNULL(opp.numUOMId, 0))* Opp.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,'
--	ELSE IF @fld_Name = 'monPrice' 
		SET @strSQL = @strSQL + 'Opp.monPrice,'
--	ELSE IF @fld_Name = 'monTotAmount' 
		SET @strSQL = @strSQL + 'Opp.monTotAmount,'
--	ELSE IF @fld_Name = 'vcSKU' 
		SET @strSQL = @strSQL + 'ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'''')) [vcSKU],'
--	ELSE IF @fld_Name = 'vcManufacturer' 
		SET @strSQL = @strSQL + 'Opp.vcManufacturer,'
--	ELSE IF @fld_Name = 'vcItemClassification' 
		SET @strSQL = @strSQL + ' dbo.[fn_GetListItemName](I.[numItemClassification]) numItemClassification,'
--	ELSE IF @fld_Name = 'numItemGroup' 
		SET @strSQL = @strSQL + '( SELECT    [vcItemGroup] FROM      [ItemGroups] WHERE     [numItemGroupID] = I.numItemGroup ) numItemGroup,'
--	ELSE IF @fld_Name = 'vcUOMName' 
		SET @strSQL = @strSQL + 'ISNULL(u.vcUnitName, '''') numUOMId,'
--	ELSE IF @fld_Name = 'fltWeight' 
		SET @strSQL = @strSQL + 'I.fltWeight,'
--	ELSE IF @fld_Name = 'fltHeight' 
		SET @strSQL = @strSQL + 'I.fltHeight,'
--	ELSE IF @fld_Name = 'fltWidth' 
		SET @strSQL = @strSQL + 'I.fltWidth,'
--	ELSE IF @fld_Name = 'fltLength' 
		SET @strSQL = @strSQL + 'I.fltLength,'
--	ELSE IF @fld_Name = 'numBarCodeId' 
		SET @strSQL = @strSQL + 'I.numBarCodeId,'
		SET @strSQL = @strSQL + 'I.IsArchieve,'
--	ELSE IF @fld_Name = 'vcWorkOrderStatus' 
		SET @strSQL = @strSQL + 'CASE WHEN ISNULL(Opp.bitWorkOrder, 0) = 1
				 THEN ( SELECT  dbo.GetListIemName(numWOStatus)
								+ CASE WHEN ISNULL(numWOStatus, 0) = 23184
									   THEN '' - ''
											+ CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo),
														  '''') + '',''
											+ CONVERT(VARCHAR(20), DATEADD(minute, '+ CONVERT(VARCHAR(15),-@ClientTimeZoneOffset)  +', bintCompletedDate)) AS VARCHAR(100))
									   ELSE ''''
								  END
						FROM    WorkOrder
						WHERE   ISNULL(numOppId, 0) = Opp.numOppId
								AND numItemCode = Opp.numItemCode
					  )
				 ELSE ''''
			END AS vcWorkOrderStatus,'
--	ELSE IF @fld_Name = 'DiscAmt' 
		SET @strSQL = @strSQL + 'CASE WHEN Opp.bitDiscountType = 0
				 THEN CAST(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount AS VARCHAR(10)) + '' (''
					  + CAST(Opp.fltDiscount AS VARCHAR(10)) + ''%)''
				 ELSE CAST(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount AS VARCHAR(10))
			END AS DiscAmt,'	
--	ELSE IF @fld_Name = 'numProjectID' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectID, 0) numProjectID,
		 CASE WHEN ( SELECT TOP 1
								ISNULL(OBI.numOppBizDocItemID, 0)
						FROM    dbo.OpportunityBizDocs OBD
								INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID
						WHERE   OBI.numOppItemID = Opp.numoppitemtCode
								AND ISNULL(OBD.bitAuthoritativeBizDocs, 0) = 1
								and isnull(OBD.numOppID,0)=OM.numOppID
					  ) > 0 THEN 1
				 ELSE 0
			END AS bitItemAddedToAuthBizDoc,'	
--	ELSE IF @fld_Name = 'numClassID' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.numClassID, 0) numClassID,'	
--	ELSE IF @fld_Name = 'vcProjectStageName' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectStageID, 0) numProjectStageID,
				CASE WHEN OPP.numProjectStageID > 0
				 THEN dbo.fn_GetProjectStageName(OPP.numProjectId,
												 OPP.numProjectStageID)
				 WHEN OPP.numProjectStageID = -1 THEN ''T&E''
				 WHEN OPP.numProjectId > 0
				 THEN CASE WHEN OM.tintOppType = 1 THEN ''S.O.''
						   ELSE ''P.O.''
					  END
				 ELSE ''-''
			END AS vcProjectStageName,'	
	        
	
	

--------------Add custom fields to query----------------
SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM View_DynamicCustomColumns
WHERE 
grp_id = 5 AND numDomainID=@numDomainId
AND numFormId=26 AND bitCustom=1 AND numUserCntID = @numUserCntID ORDER BY numFieldId
WHILE @fld_ID > 0
BEGIN ------------------

	 PRINT @fld_Name
	
	       SET @strSQL = @strSQL + 'dbo.GetCustFldValueItem('
                                    + CONVERT(VARCHAR(15), @fld_ID)
                                    + ',Opp.numItemCode) as ''' + @fld_Name + ''','
       

SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM View_DynamicCustomColumns
WHERE 
grp_id = 5 AND numDomainID=@numDomainId
AND numFormId=26 AND bitCustom=1  AND numUserCntID = @numUserCntID AND numFieldId > @fld_ID ORDER BY numFieldId
 IF @@ROWCOUNT=0
 SET @fld_ID = 0
END ------------------


                                
--Purchase Opportunity ?
IF @tintOppType = 2 
    BEGIN
        SET @strSQL =  @strSQL + 'SUBSTRING(( SELECT  '','' + vcSerialNo
            + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                   THEN '' ('' + CONVERT(VARCHAR(15), oppI.numQty)
                        + '')''
                   ELSE ''''
              END
			FROM    OppWarehouseSerializedItem oppI
					JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
			WHERE   oppI.numOppID = Opp.numOppId
					AND oppI.numOppItemID = Opp.numoppitemtCode
			ORDER BY vcSerialNo
		  FOR
			XML PATH('''')
		  ), 2, 200000) AS SerialLotNo,
		  CAST(Opp.numUnitHour AS NUMERIC(9, 0)) numQty,
		  (SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  isnull(Opp.numWarehouseItmsID,0) as numWarehouseItmsID,'
	END
ELSE
	BEGIN	
        SET @strSQL =  @strSQL + ' '''' AS SerialLotNo,
		   CAST(Opp.numUnitHour AS NUMERIC(9, 0)) numQty,
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  isnull(Opp.numWarehouseItmsID,0) as numWarehouseItmsID,'
	END


        
 SET @strSQL = left(@strSQL,len(@strSQL)-1) + '   
        FROM    OpportunityItems Opp
        LEFT JOIN OpportunityMaster OM ON Opp.numOppID = oM.numOppID
        JOIN item I ON Opp.numItemCode = i.numItemcode
        LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = Opp.numWarehouseItmsID
        LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
        LEFT JOIN WarehouseLocation WL ON WL.numWLocationID = WItems.numWLocationID
        LEFT JOIN OpportunityMaster M ON Opp.numSourceID = M.numOppID
        LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId
WHERE   om.numDomainId = '+ CONVERT(VARCHAR(10),@numDomainId) + '
        AND Opp.numOppId = '+ CONVERT(VARCHAR(15),@OpportunityId) +'
ORDER BY numoppitemtCode ASC '
  
PRINT @strSQL;
EXEC (@strSQL);
SELECT  vcSerialNo,
       vcComments AS Comments,
        numOppItemID AS numoppitemtCode,
        W.numWarehouseItmsDTLID,
        numWarehouseItmsID AS numWItmsID,
        dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                             1) Attributes
FROM    WareHouseItmsDTL W
        JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
WHERE   numOppID = @OpportunityId


CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,intVisible int,vcFieldDataType CHAR(1))


INSERT INTO #tempForm
select isnull(DC.tintRow,0)+1 as tintOrder,DDF.vcDbColumnName,DDF.vcOrigDbColumnName,ISNULL(DDF.vcCultureFieldName,DDF.vcFieldName),                  
 DDF.vcAssociatedControlType,DDF.vcListItemType,DDF.numListID,DDF.vcLookBackTableName                                                 
,DDF.bitCustom,DDF.numFieldId,DDF.bitAllowSorting,DDF.bitInlineEdit,
DDF.bitIsRequired,DDF.bitIsEmail,DDF.bitIsAlphaNumeric,DDF.bitIsNumeric,DDF.bitIsLengthValidation,
DDF.intMaxLength,DDF.intMinLength,DDF.bitFieldMessage,DDF.vcFieldMessage vcFieldMessage,DDF.ListRelID,DDF.intColumnWidth,Case when DC.numFieldID is null then 0 else 1 end,DDF.vcFieldDataType
 FROM View_DynamicDefaultColumns DDF left join View_DynamicColumns DC on DC.numFormId=26 and DDF.numFieldId=DC.numFieldId and DC.numUserCntID=@numUserCntID and DC.numDomainID=@numDomainID and numRelCntType=0 AND tintPageType=1 
 where DDF.numFormId=26 and DDF.numDomainID=@numDomainID AND ISNULL(DDF.bitSettingField,0)=1 AND ISNULL(DDF.bitDeleted,0)=0 AND ISNULL(DDF.bitCustom,0)=0
       
 UNION
    
    select tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
 from View_DynamicCustomColumns
 where numFormId=26 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0
 AND ISNULL(bitCustom,0)=1


select * from #tempForm order by tintOrder

drop table #tempForm

/*
                        SET @strSQL = 'select  Opp.numoppitemtCode,Opp.numOppId,
                               Opp.numItemCode,                                    
                               Opp.vcItemName,                                                                    
                               ISNULL(Opp.vcItemDesc,'''') as [Desc],                                                                    
                               Opp.vcType ItemType,                                   
                               Opp.vcPathForTImage,
                               Opp.vcModelID,
							    CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),Opp.numItemCode,om.numDomainId,ISNULL(opp.numUOMId,0)) * Opp.numUnitHour) as numeric(18,2)) as numUnitHour,   
                               Opp.monPrice,                                                                    
                               Opp.monTotAmount,                                                     
							   ISNULL(Opp.vcItemDesc,'''') vcItemDesc,
							   Opp.vcManufacturer,
							   I.charItemType,
							   I.bitKitParent,
							   I.bitAssembly,
							   I.vcSKU,                                   
							   I.numBarCodeId,
							   dbo.[fn_GetListItemName](I.[numItemClassification]) vcClassification,
							   (SELECT [vcItemGroup] FROM [ItemGroups] WHERE [numItemGroupID] =I.numItemGroup) vcItemGroup,
							   ISNULL(I.vcUnitofMeasure,'''') vcUnitofMeasure,
							   I.fltWeight,I.fltHeight,I.fltWidth,I.fltLength,
							   WItems.numWarehouseID,
                               Opp.numWarehouseItmsID,isnull(Opp.numToWarehouseItemID,0) numToWarehouseItemID,bitDropShip DropShip, CASE ISNULL(bitDropShip,0) when 1 then ''Yes'' else '''' end as vcDropShip,
                               0 as Op_Flag,isnull(Opp.bitDiscountType,0) as bitDiscountType,isnull(Opp.fltDiscount,0) as fltDiscount,isnull(monTotAmtBefDiscount,0) as monTotAmtBefDiscount,                                    
                               vcWarehouse as Warehouse,I.bitSerialized,isnull(I.bitLotNo,0) as bitLotNo,vcAttributes, 
                               dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) as ReturrnedQty,
                               [dbo].fn_GetReturnStatus(Opp.numoppitemtCode) as ReturnStatus,
							   isnull((SELECT numReturnID FROM Returns WHERE numOppItemCode=Opp.numoppitemtCode and numOppId=Opp.numOppID),0) as numReturnID,
                               isnull(v.monCost,0) as VendorCost,
                               bitTaxable,
                               case when bitTaxable =0 then 0 when bitTaxable=1 then '
                            + CONVERT(VARCHAR(15), @TaxPercentage)
                            + ' end as Tax,                                                
                                   isnull(M.vcPOppName,''Internal'') as Source,dbo.fn_GetAttributes(Opp.numWarehouseItmsID,I.bitSerialized) as Attributes  ,numSourceID '
                            + CASE WHEN @strSQL = '' THEN ''
                                   ELSE ',' + @strSQL
                              END
                            + ',isnull(opp.numUOMId,0) AS numUOM,ISNULL(u.vcUnitName,'''') vcUOMName,dbo.fn_UOMConversion(opp.numUOMId,Opp.numItemCode,om.numDomainId,null) AS UOMConversionFactor
                          ,case when Opp.bitDiscountType=0 then  cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) +  '' ('' + CAST(Opp.fltDiscount AS VARCHAR(10)) +  ''%)'' 
                          ELSE cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) end as DiscAmt
                           ,isnull(I.numVendorID,0) numVendorID,OM.tintOppType,cast(Opp.numUnitHour as numeric(9,0)) as numQty,SUBSTRING(
(SELECT '','' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN '' ('' + CONVERT(VARCHAR(15),whi.numQty) + '')'' ELSE '''' end 
FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=Opp.numOppId and oppI.numOppItemID=Opp.numoppitemtCode 
ORDER BY vcSerialNo FOR XML PATH('''')),2,200000) AS SerialLotNo, ISNULL(Opp.bitWorkOrder,0) as bitWorkOrder,
CASE WHEN ISNULL(Opp.bitWorkOrder,0)=1 then (select dbo.GetListIemName(numWOStatus) + CASE WHEN isnull(numWOStatus,0)=23184 THEN
'' - '' + CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo), '''')+'',''+convert(varchar(20),dateadd(minute,' + CONVERT(VARCHAR(15),-@ClientTimeZoneOffset) + ',bintCompletedDate)) AS VARCHAR(100)) ELSE '''' end
 from WorkOrder where isnull(numOppId,0)=Opp.numOppId and numItemCode=Opp.numItemCode) else '''' end as vcWorkOrderStatus,
 ISNULL(Opp.numVendorWareHouse,0) as numVendorWareHouse,ISNULL(Opp.numShipmentMethod,0) as numShipmentMethod,ISNULL(Opp.numSOVendorId,0) as numSOVendorId,Opp.numUnitHour as numOriginalUnitHour,
 isnull(Opp.numProjectID,0) numProjectID,isnull(Opp.numClassID,0) numClassID,isnull(Opp.numProjectStageID,0) numProjectStageID,Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) when OPP.numProjectStageID=-1 then ''T&E'' When OPP.numProjectId>0 then case when OM.tintOppType=1 then ''S.O.'' else ''P.O.'' end else ''-'' end as vcProjectStageName,
 CASE WHEN (SELECT TOP 1 ISNULL(OBI.numOppBizDocItemID,0) FROM dbo.OpportunityBizDocs OBD INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID WHERE OBI.numOppItemID = Opp.numoppitemtCode AND ISNULL(OBD.bitAuthoritativeBizDocs,0)=1 ) > 0
      THEN 1 ELSE 0 END AS bitItemAddedToAuthBizDoc,isnull(I.numItemClass,0) as numItemClass
                              from OpportunityItems Opp                    
                               left join OpportunityMaster OM                                                            
                                  on Opp.numOppID=oM.numOppID                                                                     
                                 join item I                                                                    
                                  on Opp.numItemCode=i.numItemcode                                            
                                left join Vendor v 
									on v.numVendorID= I.numVendorID and I.numItemCode =v.numItemCode
                               left join  WareHouseItems  WItems                                        
                               on   WItems.numWareHouseItemID= Opp.numWarehouseItmsID                                            
                               left join  Warehouses  W                                            
                                  on   W.numWarehouseID= WItems.numWarehouseID                                         
                                  left join OpportunityMaster M                                                            
                                  on Opp.numSourceID=M.numOppID  
                                  LEFT JOIN  UOM u ON u.numUOMId = opp.numUOMId  
                                  where om.numDomainId ='
                            + CONVERT(VARCHAR(15), @numDomainId)
                            + ' and Opp.numOppId='+ CONVERT(VARCHAR(15), @OpportunityId) +  
                            + CASE WHEN @numItemCode > 0 THEN ' AND Opp.numOppItemTCode = ' + CONVERT(VARCHAR(15), @numItemCode)
                                   ELSE ''
                              END
                            +
                            ' order by numoppitemtCode ASC '
                        PRINT @strSQL
                        EXEC ( @strSQL
                            )
                        SELECT  vcSerialNo,
                                vcComments AS Comments,
                                numOppItemID AS numoppitemtCode,
                                W.numWarehouseItmsDTLID,
                                numWarehouseItmsID AS numWItmsID,
                                dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                                                     1) Attributes
                        FROM    WareHouseItmsDTL W
                                JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
                        WHERE   numOppID = @OpportunityId
*/
      /*              END
                ELSE 
                    BEGIN
                        SELECT  @numDomainID = numDomainId
                        FROM    OpportunityMaster
                        WHERE   numOppID = @OpportunityId
                        SET @strSQL = ''
                        SELECT TOP 1
                                @fld_ID = Fld_ID,
                                @fld_Name = Fld_label 
                        FROM    CFW_Fld_Master
                        WHERE   grp_id = 5
                                AND numDomainID = @numDomainID
                        WHILE @fld_ID > 0
                            BEGIN
                                SET @strSQL =@strSQL + 'dbo.GetCustFldValueItem('
                                    + CONVERT(VARCHAR(15), @fld_ID)
                                    + ',Opp.numItemCode) as ''' + @fld_Name + ''''
                                SELECT TOP 1
                                        @fld_ID = Fld_ID,
                                        @fld_Name = Fld_label
                                FROM    CFW_Fld_Master
                                WHERE   grp_id = 5
                                        AND numDomainID = @numDomainID
                                        AND Fld_ID > @fld_ID
                                IF @@ROWCOUNT = 0 
                                    SET @fld_ID = 0
                                IF @fld_ID <> 0 
                                    SET @strSQL = @strSQL + ','
                            END
                        SET @strSQL = 'select  Opp.numoppitemtCode,Opp.numOppId,                                       
                                Opp.numItemCode,                                    
                               Opp.vcItemName,                                                                    
                               ISNULL(Opp.vcItemDesc,'''') as [Desc],                                                                    
                               Opp.vcType ItemType,                                   
                               Opp.vcPathForTImage,
                               Opp.vcModelID,
                              CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),Opp.numItemCode,om.numDomainId,ISNULL(opp.numUOMId,0)) * Opp.numUnitHour) as numeric(18,2)) as numUnitHour,   
							   Opp.monPrice,                                                                    
                               Opp.monTotAmount,                                                     
							   ISNULL(Opp.vcItemDesc,'''') vcItemDesc,
							   Opp.vcManufacturer,
							   I.charItemType,
							   I.bitKitParent,
							   I.bitAssembly,
							   I.vcSKU,
							   I.numBarCodeId,
							   dbo.[fn_GetListItemName](I.[numItemClassification]) vcClassification,
							   (SELECT [vcItemGroup] FROM [ItemGroups] WHERE [numItemGroupID] =I.numItemGroup) vcItemGroup,
							   ISNULL(I.vcUnitofMeasure,'''') vcUnitofMeasure,
							   I.fltWeight,I.fltHeight,I.fltWidth,I.fltLength,
							   WItems.numWarehouseID,
                               Opp.numWarehouseItmsID,isnull(Opp.numToWarehouseItemID,0) numToWarehouseItemID,bitDropShip DropShip, CASE ISNULL(bitDropShip,0) when 1 then ''Yes'' else '''' end as vcDropShip,
                               0 as Op_Flag,isnull(Opp.bitDiscountType,0) bitDiscountType,isnull(Opp.fltDiscount,0) fltDiscount,isnull(monTotAmtBefDiscount,0) monTotAmtBefDiscount,                                           
                               vcWarehouse as Warehouse,I.bitSerialized,isnull(I.bitLotNo,0) as bitLotNo,vcAttributes,
                               dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) as ReturrnedQty,
                               [dbo].fn_GetReturnStatus(Opp.numoppitemtCode) as ReturnStatus,
							   isnull((SELECT numReturnID FROM Returns WHERE numOppItemCode=Opp.numoppitemtCode and numOppId=Opp.numOppID),0) as numReturnID,
                               isnull(v.monCost,0) as VendorCost,
                               bitTaxable,
                               case when bitTaxable =0 then 0 when bitTaxable=1 then '
                            + CONVERT(VARCHAR(15), @TaxPercentage)
                            + ' end as Tax,                                                
                                   isnull(m.vcPOppName,''Internal'') as Source,dbo.fn_GetAttributes(Opp.numWarehouseItmsID,I.bitSerialized) as Attributes ,numSourceID '
                            + CASE WHEN @strSQL = '' THEN ''
                                   ELSE ',' + @strSQL
                              END
                            + ',isnull(opp.numUOMId,0) AS numUOM,ISNULL(u.vcUnitName,'''') vcUOMName,dbo.fn_UOMConversion(opp.numUOMId,Opp.numItemCode,om.numDomainId,null) AS UOMConversionFactor
                          ,case when Opp.bitDiscountType=0 then  cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) +  '' ('' + CAST(Opp.fltDiscount AS VARCHAR(10)) +  ''%)'' 
                          ELSE cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) end as DiscAmt
                         ,isnull(I.numVendorID,0) numVendorID,OM.tintOppType,cast(Opp.numUnitHour as numeric(9,0)) numQty,SUBSTRING(
(SELECT '','' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN '' ('' + CONVERT(VARCHAR(15),whi.numQty) + '')'' ELSE '''' end 
FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=Opp.numOppId and oppI.numOppItemID=Opp.numoppitemtCode 
ORDER BY vcSerialNo FOR XML PATH('''')),2,200000) AS SerialLotNo, ISNULL(Opp.bitWorkOrder,0) as bitWorkOrder,
CASE WHEN ISNULL(Opp.bitWorkOrder,0)=1 then (select dbo.GetListIemName(numWOStatus) + CASE WHEN isnull(numWOStatus,0)=23184 THEN
'' - '' + CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo), '''')+'',''+convert(varchar(20),dateadd(minute,'+ CONVERT(VARCHAR(15),-@ClientTimeZoneOffset) +',bintCompletedDate)) AS VARCHAR(100)) ELSE '''' end
 from WorkOrder where isnull(numOppId,0)=Opp.numOppId and numItemCode=Opp.numItemCode) else '''' end as vcWorkOrderStatus,
 ISNULL(Opp.numVendorWareHouse,0) as numVendorWareHouse,ISNULL(Opp.numShipmentMethod,0) as numShipmentMethod,ISNULL(Opp.numSOVendorId,0) as numSOVendorId,Opp.numUnitHour as numOriginalUnitHour,
 isnull(Opp.numProjectID,0) numProjectID,isnull(Opp.numClassID,0) numClassID,isnull(Opp.numProjectStageID,0) numProjectStageID,Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) when OPP.numProjectStageID=-1 then ''T&E'' When OPP.numProjectId>0 then case when OM.tintOppType=1 then ''S.O.'' else ''P.O.'' end else ''-'' end as vcProjectStageName,
 CASE WHEN (SELECT TOP 1 ISNULL(OBI.numOppBizDocItemID,0) FROM dbo.OpportunityBizDocs OBD INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID WHERE OBI.numOppItemID = Opp.numoppitemtCode AND ISNULL(OBD.bitAuthoritativeBizDocs,0)=1 ) > 0
 THEN 1 ELSE 0 END AS bitItemAddedToAuthBizDoc,isnull(I.numItemClass,0) as numItemClass
              from OpportunityItems Opp                    
                               left join OpportunityMaster OM                                                            
                                  on Opp.numOppID=oM.numOppID                                    
                                 join item I                                                                    
                                  on Opp.numItemCode=i.numItemcode   
                                  left join Vendor v 
									on v.numVendorID= I.numVendorID and I.numItemCode =v.numItemCode
                                left join  WareHouseItems  WItems                                        
                                on   WItems.numWareHouseItemID= Opp.numWarehouseItmsID                                            
                                left join  Warehouses  W                                            
                                  on   W.numWarehouseID= WItems.numWarehouseID                                         
                                  left join OpportunityMaster M                                                            
                                  on Opp.numSourceID=M.numOppID                      
                                        LEFT JOIN  UOM u ON u.numUOMId = opp.numUOMId
                                  where om.numDomainId ='
                            + CONVERT(VARCHAR(15), @numDomainId)
                            + ' and Opp.numOppId='+ CONVERT(VARCHAR(15), @OpportunityId) +  
                            + CASE WHEN @numItemCode > 0 THEN ' AND Opp.numOppItemTCode = ' + CONVERT(VARCHAR(15), @numItemCode)
                                   ELSE ''
                              END
                            +
                            ' order by numoppitemtCode ASC '
                        PRINT @strSQL
                        EXEC ( @strSQL
                            )
                        SELECT  vcSerialNo,
                                vcComments AS Comments,
                                numOppItemID AS numoppitemtCode,
                                W.numWarehouseItmsDTLID,
                                numWarehouseItmsID AS numWItmsID,
                                dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                                                     1) Attributes
                        FROM    OppWarehouseSerializedItem O
                                LEFT JOIN WareHouseItmsDTL W ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
                        WHERE   numOppID = @OpportunityId
                    END
            */
            END
            
    END
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
--2 changes
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount money =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(9),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0
)                                                                          
                                                                          
as  

 DECLARE @fltExchangeRate float                                 
 DECLARE @hDocItem int                                                                                                                            
 DECLARE @TotalAmount as money                                                                          
 DECLARE @tintOppStatus as tinyint               
 DECLARE @tintDefaultClassType NUMERIC
 DECLARE @numDefaultClassID NUMERIC;SET @numDefaultClassID=0
 
 --Get Default Item Class for particular user based on Domain settings  
 SELECT @tintDefaultClassType=isnull(tintDefaultClassType,0) FROM dbo.Domain  WHERE numDomainID = @numDomainID 

 IF @tintDefaultClassType=0
      SELECT @numDefaultClassID=NULLIF(numDefaultClass,0) FROM dbo.UserMaster  WHERE numUserDetailId =@numUserCntID AND numDomainID = @numDomainID 


--If new Oppertunity
if @numOppID = 0                                                                          
 begin     
  declare @intOppTcode as numeric(9)                           
  Select @intOppTcode=max(numOppId)from OpportunityMaster                
  set @intOppTcode =isnull(@intOppTcode,0) + 1                                                
  set @vcPOppName=@vcPOppName+ '-'+convert(varchar(4),year(getutcdate()))  +'-A'+ convert(varchar(10),@intOppTcode)                                  
  
  IF ISNULL(@numCurrencyID,0) = 0 
	BEGIN
	 SET @fltExchangeRate=1
	 SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
	END
  else set @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
                                                                       
  --Set Default Class If enable User Level Class Accountng 
  DECLARE @numAccountClass AS NUMERIC(18);SET @numAccountClass=0
  SELECT @numAccountClass=ISNULL(numDefaultClass,0) FROM dbo.UserMaster UM JOIN dbo.Domain D ON UM.numDomainID=D.numDomainId
  WHERE D.numDomainId=@numDomainId AND UM.numUserDetailId=@numUserCntID AND ISNULL(D.IsEnableUserLevelClassTracking,0)=1
                                                                       
  insert into OpportunityMaster                                                                          
  (                                                                             
  numContactId,                                                                   
  numDivisionId,                                                                          
  txtComments,                                             
  numCampainID,                                                                          
  bitPublicFlag,                                                                          
  tintSource,
  tintSourceType,                                                             
  vcPOppName,                                                                          
  intPEstimatedCloseDate,                                                                          
  monPAmount,                                                                           
  numCreatedBy,                                                                          
  bintCreatedDate,                                                                           
  numModifiedBy,                                                                          
  bintModifiedDate,                                                                          
  numDomainId,                                                                                                             
  numRecOwner,                                                                          
  lngPConclAnalysis,                                                                         
  tintOppType,                                                              
  numSalesOrPurType,
  numCurrencyID,
  fltExchangeRate,
  numAssignedTo,
  numAssignedBy,
  [tintOppStatus],
  numStatus,
  vcOppRefOrderNo,
  --vcWebApiOrderNo,
  bitStockTransfer,bitBillingTerms,intBillingDays,bitInterestType,fltInterest,vcCouponCode,
  bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,vcMarketplaceOrderReportId,
  numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
  bitUseMarkupShippingRate,
  numMarkupShippingRate,
  intUsedShippingCompany
  )                                                                          
  Values                                                                          
  (                                                                          
  @numContactId,                                                                          
  @numDivisionId,                                                                          
  @Comments,                           
  @CampaignID,                                                                          
  @bitPublicFlag,                     
  @tintSource,
  @tintSourceType,                         
  @vcPOppName,                                                                          
  @dtEstimatedCloseDate,                                                                          
  @monPAmount,                                                                                
  @numUserCntID,                                                                          
 CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END,                                                                          
  @numUserCntID,                                                           
  getutcdate(),                                                                          
  @numDomainId,                                                                                   
  @numUserCntID,                                                                          
  @lngPConclAnalysis,                                                                                                                                                                      
  @tintOppType,                                                                                                                                      
  @numSalesOrPurType,              
  @numCurrencyID, 
  @fltExchangeRate,
  case when @numAssignedTo>0 then @numAssignedTo else null end,
  case when @numAssignedTo>0 then @numUserCntID else null   END,
  @DealStatus,
  @numStatus,
  @vcOppRefOrderNo,
 -- @vcWebApiOrderNo,
  @bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,@vcCouponCode,
  @bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,
  @numPercentageComplete,@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,
  @bitUseMarkupShippingRate,
  @numMarkupShippingRate,
  @intUsedShippingCompany
    )                                                                                                                      
  set @numOppID=scope_identity()                                                
  
  --Update OppName as per Name Template
  EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

  --Map Custom Field	
  DECLARE @tintPageID AS TINYINT;
  SET @tintPageID=CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
  EXEC dbo.USP_AddParentChildCustomFieldMap
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@numRecordID = @numOppID, --  numeric(18, 0)
	@numParentRecId = @numDivisionId, --  numeric(18, 0)
	@tintPageID = @tintPageID --  tinyint
 	
  
	IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
	   BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
	   END
  ---- inserting Items                                                                          
   
                                                   
  if convert(varchar(10),@strItems) <>'' AND @numOppID>0
  begin                      
   EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
  insert into                       
   OpportunityItems                                                                          
   (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,
   numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,monAvgCost)
   select @numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,
   x.monPrice,x.monTotAmount,X.vcItemDesc,
   CASE X.numWarehouseItmsID WHEN -1 
							 THEN (SELECT [numWareHouseItemID] FROM [WareHouseItems] 
								   WHERE [numItemID] = X.numItemCode 
								   AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								   AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail 
														  WHERE numDomainID =@numDomainId 
														  AND WebApiId = @WebApiId))  
							 WHEN 0 
							 THEN (SELECT TOP 1 [numWareHouseItemID] FROM [WareHouseItems] 
								   WHERE [numItemID] = X.numItemCode
								   AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')) 
							 ELSE  X.numWarehouseItmsID 
	END AS numWarehouseItmsID,
	X.ItemType,X.DropShip,X.bitDiscountType,
   X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,
   (SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
   (SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
   (SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
   (select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID),
   X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,Case When @tintDefaultClassType=0 then @numDefaultClassID else (SELECT numItemClass FROM item WHERE numItemCode = X.numItemCode) end,X.numToWarehouseItemID,X.Attributes,
   (SELECT ISNULL(monAverageCost,0) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode) from(
   SELECT *FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
   WITH                       
   (                                                                          
   numoppitemtCode numeric(9),                                     
   numItemCode numeric(9),                                                                          
   numUnitHour numeric(9,2),                                                                          
   monPrice money,                                                                       
   monTotAmount money,                                                                          
   vcItemDesc varchar(1000),                                    
   numWarehouseItmsID numeric(9),                          
   ItemType varchar(30),      
   DropShip bit,
   bitDiscountType bit,
   fltDiscount float,
   monTotAmtBefDiscount MONEY,
   vcItemName varchar(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9) ,numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(200),vcSKU VARCHAR(100)
   ))X    
    
    
    
   Update OpportunityItems                       
   set numItemCode=X.numItemCode,    
   numOppId=@numOppID,                     
   numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
   monPrice=x.monPrice,                      
   monTotAmount=x.monTotAmount,                                  
   vcItemDesc=X.vcItemDesc,                      
   numWarehouseItmsID=X.numWarehouseItmsID,                 
   bitDropShip=X.DropShip,
   bitDiscountType=X.bitDiscountType,
   fltDiscount=X.fltDiscount,
   monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,bitWorkOrder=X.bitWorkOrder,
   numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,
vcAttributes=X.Attributes,numClassID=(Case When @tintDefaultClassType=0 then @numDefaultClassID else (SELECT numItemClass FROM item WHERE numItemCode = X.numItemCode) END)
--   ,vcModelID=(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),vcManufacturer=(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode)
--  ,vcPathForTImage=(SELECT vcPathForTImage FROM item WHERE numItemCode = X.numItemCode),monVendorCost=(select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID)
   from (SELECT numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,
   numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,Attributes 
   FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
   WITH  (                      
    numoppitemtCode numeric(9),                                     
    numItemCode numeric(9),                                                                          
    numUnitHour numeric(9,2),                                                                          
    monPrice money,                                         
    monTotAmount money,                                                                          
    vcItemDesc varchar(1000),                                    
    numWarehouseItmsID numeric(9),      
    DropShip bit,
	bitDiscountType bit,
    fltDiscount float,
    monTotAmtBefDiscount MONEY,vcItemName varchar(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(200)
   ))X where numoppitemtCode=X.numOppItemID                                          
	
	
   -- Update UOM of opportunity items have not UOM set while exported from Marketplace (means tintSourceType = 3)
	IF ISNULL(@tintSourceType,0) = 3
	BEGIN
		--SELECT * FROM OpportunityMaster WHERE numOppId = 81654
		--SELECT * FROM OpportunityItems WHERE numOppId = 81654
		--SELECT * FROM Item WHERE numItemCode = 822078

		UPDATE OI SET numUOMID = ISNULL(I.numSaleUnit,0), 
					  numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
		FROM OpportunityItems OI
		JOIN Item I ON OI.numItemCode = I.numItemCode
		WHERE numOppId = @numOppID
		AND I.numDomainID = @numDomainId

	END	
   --Update OpportunityItems
   --set 
   --FROM (SELECT numItemCode,vcItemName,[txtItemDesc],vcModelID FROM item WHERE numItemCode IN (SELECT [numItemCode] FROM OpportunityItems WHERE numOppID = @numOppID))X
   --WHERE OpportunityItems.[numItemCode] = X.numItemCode 
                                     
   insert into OppWarehouseSerializedItem                                                                          
   (numOppID,numWarehouseItmsDTLID,numOppItemID,numWarehouseItmsID)                      
   select @numOppID,X.numWarehouseItmsDTLID,X.numoppitemtCode,X.numWItmsID from(                                                                          
   SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
   WITH                        
   (                                               
   numWarehouseItmsDTLID numeric(9),                      
   numoppitemtCode numeric(9),                      
   numWItmsID numeric(9)                                                     
   ))X                                    
               
                                 
   update OppWarehouseSerializedItem                       
   set numOppItemID=X.numoppitemtCode                                
   from                       
   (SELECT numWarehouseItmsDTLID as DTLID,O.numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
   WITH(numWarehouseItmsDTLID numeric(9),numoppitemtCode numeric(9),numWItmsID numeric(9))                                
   join OpportunityItems O on O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID)X                 
   where numOppID=@numOppID and numWarehouseItmsDTLID=X.DTLID      
    
--   insert into OpportunityKitItems(numOppKitItem,numChildItem,intQuantity,numWareHouseItemId)    
--   SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/ChildItems',2)                                                                          
--   WITH                        
--   (                                               
--   numoppitemtCode numeric(9),                      
--   numItemCode numeric(9),    
--   numQtyItemsReq integer,  
--   numWarehouseItmsID numeric(9)                                                       
--   )    
      
    
    
    
	EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
    
                              
                          
   EXEC sp_xml_removedocument @hDocItem                                     
        end          
   set @TotalAmount=0                                                           
   select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@numOppID                                                                          
  update OpportunityMaster set  monPamount=@TotalAmount                                                          
  where numOppId=@numOppID                                                 
                      
     
--Insert Tax for Division                       
INSERT dbo.OpportunityMasterTaxItems (
	numOppId,
	numTaxItemID,
	fltPercentage
) SELECT @numOppID,TI.numTaxItemID,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL) FROM TaxItems TI JOIN DivisionTaxTypes DTT ON TI.numTaxItemID = DTT.numTaxItemID
 WHERE DTT.numDivisionID=@numDivisionId AND DTT.bitApplicable=1
   union 
  select @numOppID,0,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL) 
  FROM dbo.DivisionMaster WHERE bitNoTax=0 AND numDivisionID=@numDivisionID

-- Archieve item based on Item's individual setting
--	UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT * FROM dbo.OpportunityItems WHERE OpportunityItems.numItemCode = I.numItemCode) AND ISNULL(I.bitArchiveItem,0) = 1 
--									  THEN 1 
--									  ELSE 0 
--									  END 
--	FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode
	
 END
 
 else                                                                                                                          
 BEGIN                  
	--Declaration
	 DECLARE @tempAssignedTo AS NUMERIC(9)                                                    
	 SET @tempAssignedTo = NULL 
	 
	 SELECT @tintOppStatus = tintOppStatus,@tempAssignedTo=isnull(numAssignedTo,0) FROM   OpportunityMaster WHERE  numOppID = @numOppID
 
	--Reverting back the warehouse items                  
	 IF @tintOppStatus = 1 
		BEGIN        
		PRINT 'inside revert'          
			EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID                  
		END                  
	-- Update Master table
	
	   IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
	   BEGIN
	   		UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
	   END
		
	   IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
	   BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
	   END 
						
	   UPDATE   OpportunityMaster
	   SET      vcPOppName = @vcPOppName,
				txtComments = @Comments,
				bitPublicFlag = @bitPublicFlag,
				numCampainID = @CampaignID,
				tintSource = @tintSource,
				tintSourceType=@tintSourceType,
				intPEstimatedCloseDate = @dtEstimatedCloseDate,
				numModifiedBy = @numUserCntID,
				bintModifiedDate = GETUTCDATE(),
				lngPConclAnalysis = @lngPConclAnalysis,
				monPAmount = @monPAmount,
				tintActive = @tintActive,
				numSalesOrPurType = @numSalesOrPurType,
				numStatus = @numStatus,
				tintOppStatus = @DealStatus,
				vcOppRefOrderNo=@vcOppRefOrderNo,
				--vcWebApiOrderNo = @vcWebApiOrderNo,
				bintOppToOrder=(Case when @tintOppStatus=0 and @DealStatus=1 then GETUTCDATE() else bintOppToOrder end),
				bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,bitBillingTerms=@bitBillingTerms,
				intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,
				tintTaxOperator=@tintTaxOperator,numDiscountAcntType=@numDiscountAcntType,vcCouponCode = @vcCouponCode,
				numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
				bitUseMarkupShippingRate = @bitUseMarkupShippingRate,
				numMarkupShippingRate = @numMarkupShippingRate,
				intUsedShippingCompany = @intUsedShippingCompany
	   WHERE    numOppId = @numOppID   
	   
	---Updating if organization is assigned to someone                                                      
	   IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN                                   
			UPDATE  OpportunityMaster SET     numAssignedTo = @numAssignedTo, numAssignedBy = @numUserCntID WHERE   numOppId = @numOppID                                                    
		END                                                     
	   ELSE 
	   IF ( @numAssignedTo = 0 ) 
		BEGIN                
			UPDATE  OpportunityMaster SET     numAssignedTo = 0, numAssignedBy = 0 WHERE   numOppId = @numOppID
		END                                                                      
		---- Updating Opp Items
		if convert(varchar(10),@strItems) <>'' AND @numOppID>0                                                                         
		begin
			   EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   --Delete Items
			   delete from OpportunityKitItems where numOppID=@numOppID and numOppItemID not in                       
			   (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
			   WITH  (numoppitemtCode numeric(9)))                      
                                        
--               ---- ADDED BY Manish Anjara : Jun 26,2013 - Archive item based on Item's individual setting
--               UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT * FROM dbo.OpportunityItems WHERE OpportunityItems.numItemCode = I.numItemCode) AND ISNULL(I.bitArchiveItem,0) = 1 
--												 THEN 1 
--												 ELSE 0 
--												 END 
--			   FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode
--					 	
--			   UPDATE Item SET IsArchieve = CASE WHEN ISNULL(I.bitArchiveItem,0) = 1 THEN 0 ELSE 1 END
--			   FROM Item I
--			   JOIN OpportunityItems OI ON I.numItemCode = OI.numItemCode
--			   WHERE numOppID = @numOppID and OI.numItemCode NOT IN (SELECT numItemCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) 
--																	 WITH (numItemCode numeric(9)))

			   delete from OpportunityItems where numOppID=@numOppID and numoppitemtCode not in                       
			   (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
			   WITH  (numoppitemtCode numeric(9)))   
	                                
			   insert into OpportunityItems                                                                          
			   (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost)
			   select @numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),(select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID),X.numUOM,X.bitWorkOrder,
			   X.numVendorWareHouse,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,Case When @tintDefaultClassType=0 then @numDefaultClassID else (SELECT numItemClass FROM item WHERE numItemCode = X.numItemCode) END,
			   (SELECT ISNULL(monAverageCost,0) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode) from(
			   SELECT *FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
			   WITH  (                      
				numoppitemtCode numeric(9),                                     
				numItemCode numeric(9),                                                                          
				numUnitHour numeric(9,2),                                                                          
				monPrice money,                                                                       
				monTotAmount money,                                                                          
				vcItemDesc varchar(1000),                                    
				numWarehouseItmsID numeric(9),                              
				Op_Flag tinyint,                            
				ItemType varchar(30),      
				DropShip bit,
				bitDiscountType bit,
				fltDiscount float,
				monTotAmtBefDiscount MONEY,
				vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(200)
				 ))X                                     
				--Update items                 
			   Update OpportunityItems                       
			   set numItemCode=X.numItemCode,    
			   numOppId=@numOppID,                       
			   numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
			   monPrice=x.monPrice,                      
			   monTotAmount=x.monTotAmount,                                  
			   vcItemDesc=X.vcItemDesc,                      
			   numWarehouseItmsID=X.numWarehouseItmsID,                 
			   bitDropShip=X.DropShip,
			   bitDiscountType=X.bitDiscountType,
			   fltDiscount=X.fltDiscount,
			   monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
			   numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes
			   from (SELECT numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes  FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
			   WITH  (                      
				numoppitemtCode numeric(9),                                     
				numItemCode numeric(9),                                                                          
				numUnitHour numeric(9,2),                                                                          
				monPrice money,                                         
				monTotAmount money,                                                                          
				vcItemDesc varchar(1000),                                    
				numWarehouseItmsID numeric(9),      
				DropShip bit,
				bitDiscountType bit,
				fltDiscount float,
				monTotAmtBefDiscount MONEY,vcItemName varchar(300),numUOM NUMERIC(9),numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(200)                                              
			   ))X where numoppitemtCode=X.numOppItemID                           
			                                    
			   EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			   		                                                                      
			   EXEC sp_xml_removedocument @hDocItem                                               
		end     
		                                                             
END

----------------generic section will be called in both insert and update ---------------------------------
-- Archieve item based on Item's individual setting
--	UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT * FROM dbo.OpportunityItems WHERE OpportunityItems.numItemCode = I.numItemCode) AND ISNULL(I.bitArchiveItem,0) = 1 
--									  THEN 1 
--									  ELSE 0 
--									  END 
--	FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode
	
if convert(varchar(10),@strItems) <>'' AND @numOppID>0                                                                        
  begin 
EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

--delete from OpportunityKitItems where numOppID=@numOppID and numOppItemID 
--	not in (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppID=@numOppID)                      
		
--Update Kit Items                 
Update OKI set numQtyItemsReq=numQtyItemsReq_Orig * OI.numUnitHour
			   FROM OpportunityKitItems OKI JOIN OpportunityItems OI ON OKI.numOppItemID=OI.numoppitemtCode and OKI.numOppId=OI.numOppId  
			   WHERE OI.numOppId=@numOppId 

--Insert Kit Items                 
INSERT into OpportunityKitItems                                                                          
		(numOppId,numOppItemID,numChildItemID,numWareHouseItemId,numQtyItemsReq,numQtyItemsReq_Orig,numUOMId,numQtyShipped)
  SELECT @numOppId,OI.numoppitemtCode,ID.numChildItemID,ID.numWareHouseItemId,ID.numQtyItemsReq * OI.numUnitHour,ID.numQtyItemsReq,ID.numUOMId,0
	 FROM OpportunityItems OI JOIN ItemDetails ID ON OI.numItemCode=ID.numItemKitID WHERE 
	OI.numOppId=@numOppId AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID)
--	OI.numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
--			   WITH  (                      
--				numoppitemtCode numeric(9))X)
			   
EXEC sp_xml_removedocument @hDocItem  
END

/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
declare @tintShipped as tinyint               
select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID              
if @tintOppStatus=1               
	begin         
	PRINT 'inside USP_UpdatingInventoryonCloseDeal'
		exec USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
	end              
            
if @tintOppType=1              
begin              
	if @tintOppStatus=1 Update WareHouseItmsDTL set tintStatus=1 where numWareHouseItmsDTLID in (select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppID=@numOppID)
	if @tintShipped=1 Update WareHouseItmsDTL set tintStatus=2 where numWareHouseItmsDTLID in (select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppID=@numOppID)              
END


declare @tintCRMType as numeric(9)        
declare @AccountClosingDate as datetime        

select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

--When deal is won                                         
if @DealStatus = 1 
begin           
	 if @AccountClosingDate is null                   
	  update OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numOppID              
end         
--When Deal is Lost
else if @DealStatus = 2
begin         
	 select @AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
	 if @AccountClosingDate is null                   
	  update OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numOppID
end                    

/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
begin        
	update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end        
-- Promote Lead to Account when Sales/Purchase Order is created against it
ELSE if @tintCRMType=0 AND @DealStatus = 1 --Lead & Order
begin        
	update divisionmaster set tintCRMType=2,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end
--Promote Prospect to Account
else if @tintCRMType=1 AND @DealStatus = 1 
begin        
	update divisionmaster set tintCRMType=2,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9) 
DECLARE @bitIsPrimary BIT;

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

--Bill Address
IF @numBillAddressId>0
BEGIN
 SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numBillAddressId
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName
END
  
  
  --Ship Address
IF @numShipAddressId>0
BEGIN
	SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numShipAddressId
 



  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName
END

--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems (
	numOppId,
	numOppItemID,
	numTaxItemID
) SELECT @numOppId,OI.numoppitemtCode,TI.numTaxItemID FROM dbo.OpportunityItems OI JOIN dbo.ItemTax IT ON OI.numItemCode=IT.numItemCode JOIN
TaxItems TI ON TI.numTaxItemID=IT.numTaxItemID WHERE OI.numOppId=@numOppID AND IT.bitApplicable=1 AND 
OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
  select @numOppId,OI.numoppitemtCode,0 FROM dbo.OpportunityItems OI JOIN dbo.Item I ON OI.numItemCode=I.numItemCode
   WHERE OI.numOppId=@numOppID  AND I.bitTaxable=1 AND
OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

  
 UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID

/****** Object:  StoredProcedure [dbo].[USP_SaveJournalDetails]    Script Date: 07/26/2008 16:21:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Create by Siva                                                                                        
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_SaveJournalDetails' ) 
    DROP PROCEDURE USP_SaveJournalDetails
GO
CREATE PROCEDURE [dbo].[USP_SaveJournalDetails]
    @numJournalId AS NUMERIC(9) = 0 ,
    @strRow AS TEXT = '' ,
    @Mode AS TINYINT = 0 ,
    @numDomainId AS NUMERIC(9) = 0                       
--@RecurringMode as tinyint=0,    
AS 
BEGIN           
                
BEGIN TRY 
        BEGIN TRAN  
        
        ---Always set default Currency ID as base currency ID of domain, and Rate =1
        DECLARE @numBaseCurrencyID NUMERIC
        SELECT @numBaseCurrencyID = ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId
        
        IF CONVERT(VARCHAR(100), @strRow) <> '' 
            BEGIN                                                                                                            
                DECLARE @hDoc3 INT                                                                                                                                                                
                EXEC sp_xml_preparedocument @hDoc3 OUTPUT, @strRow                                                                                                             
--                PRINT '---Insert xml into temp table ---'
                    SELECT numTransactionId ,numDebitAmt ,numCreditAmt ,numChartAcntId ,varDescription ,numCustomerId ,/*numDomainId ,*/bitMainDeposit ,bitMainCheck ,bitMainCashCredit ,numoppitemtCode ,chBizDocItems ,vcReference ,numPaymentMethod ,bitReconcile , 
                    ISNULL(NULLIF(numCurrencyID,0),@numBaseCurrencyID) AS numCurrencyID  ,ISNULL(NULLIF(fltExchangeRate,0.0000),1) AS fltExchangeRate ,numTaxItemID ,numBizDocsPaymentDetailsId ,numcontactid ,numItemID ,numProjectID ,numClassID ,numCommissionID ,numReconcileID ,bitCleared ,tintReferenceType ,numReferenceID ,numCampaignID 
                     INTO #temp FROM      OPENXML (@hdoc3,'/ArrayOfJournalEntryNew/JournalEntryNew',2)
												WITH (
												numTransactionId numeric(10, 0),numDebitAmt money,numCreditAmt money,numChartAcntId numeric(18, 0),varDescription varchar(1000),numCustomerId numeric(18, 0),/*numDomainId numeric(18, 0),*/bitMainDeposit bit,bitMainCheck bit,bitMainCashCredit bit,numoppitemtCode numeric(18, 0),chBizDocItems nchar(10),vcReference varchar(500),numPaymentMethod numeric(18, 0),bitReconcile bit,numCurrencyID numeric(18, 0),fltExchangeRate float,numTaxItemID numeric(18, 0),numBizDocsPaymentDetailsId numeric(18, 0),numcontactid numeric(9, 0),numItemID numeric(18, 0),numProjectID numeric(18, 0),numClassID numeric(18, 0),numCommissionID numeric(9, 0),numReconcileID numeric(18, 0),bitCleared bit,tintReferenceType tinyint,numReferenceID numeric(18, 0),numCampaignID NUMERIC(18,0)
													)

                 EXEC sp_xml_removedocument @hDoc3 
                                    

/*-----------------------Validation of balancing entry*/

IF EXISTS(SELECT * FROM #temp)
BEGIN
	DECLARE @SumTotal MONEY 
	SELECT @SumTotal = SUM(numDebitAmt)-SUM(numCreditAmt) FROM #temp
--	PRINT cast( @SumTotal AS DECIMAL(10,4))
	IF cast( @SumTotal AS DECIMAL(10,4)) <> 0.0000
	BEGIN
		 RAISERROR ( 'IM_BALANCE', 16, 1 ) ;
		 RETURN;
	END

	
END



DECLARE @numEntryDateSortOrder AS NUMERIC
DECLARE @datEntry_Date AS DATETIME
--Combine Date from datentryDate with time part of current time
SELECT @datEntry_Date=( CAST( CONVERT(VARCHAR,DATEPART(year, datEntry_Date)) + '-' + RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(month, datEntry_Date)),2)+ 
+ '-' + CONVERT(VARCHAR,DATEPART(day, datEntry_Date))
+ ' ' + CONVERT(VARCHAR,DATEPART(hour, GETDATE()))
+ ':' + CONVERT(VARCHAR,DATEPART(minute, GETDATE()))
+ ':' + CONVERT(VARCHAR,DATEPART(second, GETDATE())) AS DATETIME)  
) 
 from dbo.General_Journal_Header WHERE numJournal_Id=@numJournalId AND numDomainId=@numDomainId

SET @numEntryDateSortOrder = convert(numeric,
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(year, @datEntry_Date)),4)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(month, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(day, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(hour, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(minute, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(second, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(millisecond, @datEntry_Date)),3))

/*-----------------------Validation of balancing entry*/

			

                IF @Mode = 1  /*Edit Journal entries*/
                    BEGIN             
						PRINT '---delete removed transactions ---'
                        DELETE  FROM General_Journal_Details
                        WHERE   numJournalId = @numJournalId
                                AND numTransactionId NOT IN (SELECT numTransactionId FROM #temp as X )
			                                                           
						--Update transactions
						PRINT '---updated existing transactions ---'
                        UPDATE  dbo.General_Journal_Details 
                        SET    		[numDebitAmt] = X.numDebitAmt,
									[numCreditAmt] = X.numCreditAmt,
									[numChartAcntId] = X.numChartAcntId,
									[varDescription] = X.varDescription,
									[numCustomerId] = NULLIF(X.numCustomerId,0),
									[bitMainDeposit] = X.bitMainDeposit,
									[bitMainCheck] = X.bitMainCheck,
									[bitMainCashCredit] = X.bitMainCashCredit,
									[numoppitemtCode] = NULLIF(X.numoppitemtCode,0),
									[chBizDocItems] = X.chBizDocItems,
									[vcReference] = X.vcReference,
									[numPaymentMethod] = X.numPaymentMethod,
									[numCurrencyID] = NULLIF(X.numCurrencyID,0),
									[fltExchangeRate] = X.fltExchangeRate,
									[numTaxItemID] = NULLIF(X.numTaxItemID,0),
									[numBizDocsPaymentDetailsId] = NULLIF(X.numBizDocsPaymentDetailsId,0),
									[numcontactid] = NULLIF(X.numcontactid,0),
									[numItemID] = NULLIF(X.numItemID,0),
									[numProjectID] =NULLIF( X.numProjectID,0),
									[numClassID] = NULLIF(X.numClassID,0),
									[numCommissionID] = NULLIF(X.numCommissionID,0),
									numCampaignID=NULLIF(X.numCampaignID,0),
									numEntryDateSortOrder1=@numEntryDateSortOrder
--									[tintReferenceType] = X.tintReferenceType,
--									[numReferenceID] = X.numReferenceID
                          FROM    #temp as X 
						  WHERE   X.numTransactionId = General_Journal_Details.numTransactionId
			               
		                                                            
                    END 
                IF @Mode = 0 OR @Mode = 1 /*-- Insert journal only*/
                    BEGIN    
						PRINT '---insert existing transactions ---'                                                                                               
                        INSERT  INTO [dbo].[General_Journal_Details]
                                ( [numJournalId] ,
                                  [numDebitAmt] ,
                                  [numCreditAmt] ,
                                  [numChartAcntId] ,
                                  [varDescription] ,
                                  [numCustomerId] ,
                                  [numDomainId] ,
                                  [bitMainDeposit] ,
                                  [bitMainCheck] ,
                                  [bitMainCashCredit] ,
                                  [numoppitemtCode] ,
                                  [chBizDocItems] ,
                                  [vcReference] ,
                                  [numPaymentMethod] ,
                                  [bitReconcile] ,
                                  [numCurrencyID] ,
                                  [fltExchangeRate] ,
                                  [numTaxItemID] ,
                                  [numBizDocsPaymentDetailsId] ,
                                  [numcontactid] ,
                                  [numItemID] ,
                                  [numProjectID] ,
                                  [numClassID] ,
                                  [numCommissionID] ,
                                  [numReconcileID] ,
                                  [bitCleared],
                                  [tintReferenceType],
								  [numReferenceID],[numCampaignID],numEntryDateSortOrder1
	                                
                                )
                                SELECT  @numJournalId ,
                                        [numDebitAmt] ,
                                        [numCreditAmt] ,
                                        CASE WHEN [numChartAcntId]=0 THEN NULL ELSE numChartAcntId END  ,
                                        [varDescription] ,
                                        NULLIF(numCustomerId, 0) ,
                                        @numDomainId ,
                                        [bitMainDeposit] ,
                                        [bitMainCheck] ,
                                        [bitMainCashCredit] ,
                                        NULLIF([numoppitemtCode], 0) ,
                                        [chBizDocItems] ,
                                        [vcReference] ,
                                        [numPaymentMethod] ,
                                        [bitReconcile] ,
                                        NULLIF([numCurrencyID], 0) ,
                                        [fltExchangeRate] ,
                                        NULLIF([numTaxItemID], 0) ,
                                        NULLIF([numBizDocsPaymentDetailsId], 0) ,
                                        NULLIF([numcontactid], 0) ,
                                        NULLIF([numItemID], 0) ,
                                        NULLIF([numProjectID], 0) ,
                                        NULLIF([numClassID], 0) ,
                                        NULLIF([numCommissionID], 0) ,
                                        NULLIF([numReconcileID], 0) ,
                                        [bitCleared],
                                        tintReferenceType,
										NULLIF(numReferenceID,0),NULLIF(numCampaignID,0),@numEntryDateSortOrder
                                        
                                FROM #temp as X    
                                WHERE X.numTransactionId =0
                        
                        --Set Default Class If enable User Level Class Accountng 
						DECLARE @numAccountClass AS NUMERIC(18);SET @numAccountClass=0                             
                        SELECT @numAccountClass=ISNULL(numAccountClass,0) FROM General_Journal_Header WHERE numDomainId=@numDomainId AND numJournal_Id=@numJournalId
                        IF @numAccountClass>0
                        BEGIN
							UPDATE General_Journal_Details SET numClassID=@numAccountClass WHERE numJournalId=@numJournalId AND numDomainId=@numDomainId
						END
                    END 
                    
                    
                    UPDATE dbo.General_Journal_Header 
						SET numAmount=(SELECT SUM(numDebitAmt) FROM dbo.General_Journal_Details WHERE numJournalID=@numJournalID)
						WHERE numJournal_Id=@numJournalId AND numDomainId=@numDomainId
						
						
                    drop table #temp
                    
                    -- Update leads,prospects to Accounts
					DECLARE @numDivisionID AS NUMERIC(18)
					DECLARE @numUserCntID AS NUMERIC(18)
					
					SELECT TOP 1 @numDivisionID = ISNULL(numCustomerId,0), @numUserCntID = ISNULL(numCreatedBy,0)
					FROM dbo.General_Journal_Details GJD
					JOIN dbo.General_Journal_Header GJH ON GJD.numJournalId = GJH.numJournal_Id AND GJD.numDomainId = GJH.numDomainId
					WHERE numJournalId = @numJournalId 
					AND GJH.numDomainId = @numDomainID
					
					IF @numDivisionID > 0 AND @numUserCntID > 0
					BEGIN
						EXEC dbo.USP_UpdateCRMTypeToAccounts @numDivisionID ,@numDomainID ,@numUserCntID
					END
            END
            
SELECT GJD.numTransactionId,GJD.numEntryDateSortOrder1,Row_number() OVER(ORDER BY GJD.numTransactionId) AS NewSortId  INTO #Temp1 
FROM General_Journal_Details GJD WHERE GJD.numDomainId = @numDomainID and  GJD.numEntryDateSortOrder1  LIKE SUBSTRING(CAST(@numEntryDateSortOrder AS VARCHAR(25)),1,12) + '%'

UPDATE  GJD SET GJD.numEntryDateSortOrder1 = (Temp1.numEntryDateSortOrder1 + Temp1.NewSortId) 
FROM General_Journal_Details GJD INNER JOIN #Temp1 AS Temp1 ON GJD.numTransactionId = Temp1.numTransactionId

IF object_id('TEMPDB.DBO.#Temp1') IS NOT NULL DROP TABLE #Temp1

        
        COMMIT TRAN 
    END TRY 
    BEGIN CATCH		
        DECLARE @strMsg VARCHAR(200)
        SET @strMsg = ERROR_MESSAGE()
        IF ( @@TRANCOUNT > 0 ) 
            BEGIN
                RAISERROR ( @strMsg, 16, 1 ) ;
                ROLLBACK TRAN
                RETURN 1
            END
    END CATCH	

            
--End                                              
---For Recurring Transaction                                              
                                              
--if @RecurringMode=1                                              
--Begin    
--  print 'SIVA--Recurring'    
--  print  '@numDomainId=============='+Convert(varchar(10),@numDomainId)                                     
--  Declare @numMaxJournalId as numeric(9)                                              
--  Select @numMaxJournalId=max(numJournal_Id) From General_Journal_Header Where numDomainId=@numDomainId                                              
--  insert into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,numCustomerId,numDomainId,bitMainCheck,bitMainCashCredit)                                              
--  Select @numMaxJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,NULLIF(numCustomerId,0),numDomainId,bitMainCheck,bitMainCashCredit from General_Journal_Details Where numJournalId=@numJournalId  And numDomainId=@numDomainId                         
--   
--                   
--/*Commented by chintan Opening balance Will be updated by Trigger*/
----Exec USP_UpdateChartAcntOpnBalanceForJournalEntry @JournalId=@numMaxJournalId,@numDomainId=@numDomainId                                              
--End           
    END
GO
/****** Object:  StoredProcedure [dbo].[USP_TicklerActItems]    Script Date: 07/26/2008 16:21:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By Anoop jayaraj                                                        
                                                    
-- Modified by Tarun Juneja                                                    
-- date 26-08-2006                                                    
-- Reason:- Enhancement In Tickler                                                    
                                                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_tickleractitems')
DROP PROCEDURE usp_tickleractitems
GO
CREATE PROCEDURE [dbo].[USP_TicklerActItems]                                                                        
@numUserCntID as numeric(9)=null,                                                                        
@numDomainID as numeric(9)=null,                                                                        
@startDate as datetime,                                                                        
@endDate as datetime,                                                                    
@ClientTimeZoneOffset Int,  --Added by Debasish to enable calculation of date according to client machine                                                                      
@bitFilterRecord bit=0,
@columnName varchar(50),
@columnSortOrder varchar(50),
@vcBProcessValue varchar(500)
                                                  
As                                                                         
  
Create table #tempRecords (numContactID numeric(9),numDivisionID numeric(9),tintCRMType tinyint,
Id numeric(9),bitTask varchar(100),Startdate datetime,EndTime datetime,
itemDesc text,[Name] varchar(200),vcFirstName varchar(100),vcLastName varchar(100),numPhone varchar(15),numPhoneExtension varchar(7),
[Phone] varchar(30),vcCompanyName varchar(100),vcEmail varchar(50),Task varchar(100),Activity varchar(500),
Status varchar(100),numCreatedBy numeric(9),numTerId numeric(9),numAssignedTo varchar(1000),numAssignedBy varchar(50),
 caseid numeric(9),vcCasenumber varchar(50),casetimeId numeric(9),caseExpId numeric(9),type int,itemid varchar(50),
changekey varchar(50),DueDate varchar(50),bitFollowUpAnyTime bit,numNoOfEmployeesId varchar(50),vcComPhone varchar(50),vcColorScheme VARCHAR(50),
Slp_Name varchar(100),intTotalProgress INT,vcPoppName varchar(100), numOppId NUMERIC(18,0),numBusinessProcessID numeric(18,0) )                                                         
 
declare @strSql as varchar(8000)                                                            
declare @strSql1 as varchar(8000)                                                            
declare @strSql2 as varchar(8000)                                                            
 
   -------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

--select top 1 @vcCSOrigDbCOlumnName=DFM.vcOrigDbCOlumnName,@vcCSLookBackTableName=DFM.vcLookBackTableName from DycFieldColorScheme DFCS join DycFieldMaster DFM on 
--DFCS.numModuleID=DFM.numModuleID and DFCS.numFieldID=DFM.numFieldID
--where DFCS.numDomainID=@numDomainID and DFM.numModuleID=1

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=43 AND DFCS.numFormID=43 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
----------------------------                   


SET @strSql = 'insert into #tempRecords
Select * from (                                                    
 SELECT Addc.numContactID,Addc.numDivisionID,Div.tintCRMType,Comm.numCommId AS Id,convert(varchar(15),bitTask) as bitTask,                                               
 DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtStartTime) as Startdate,                        
 DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtEndTime) as EndTime ,                                                                                                                                                                  
 --DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtStartTime) AS CloseDate,                                                                         
 Comm.textDetails AS itemDesc,                                                                                                               
 AddC.vcFirstName + '' '' + AddC.vcLastName  as [Name], 
 AddC.vcFirstName,AddC.vcLastName,Addc.numPhone,AddC.numPhoneExtension,                               
 case when Addc.numPhone<>'''' then + AddC.numPhone +case when AddC.numPhoneExtension<>'''' then '' - '' + AddC.numPhoneExtension else '''' end  else '''' end as [Phone],                                                                       
 Comp.vcCompanyName As vcCompanyName , --changed by bharat so that the complete name is displayed                                                                
 AddC.vcEmail As vcEmail ,                                                                         
-- case when Comm.bitTask=971 then  ''Communication'' when Comm.bitTask=2 then  ''Opportunity''       
--when Comm.bitTask = 972 then  ''Task'' when Comm.bitTask=973 then  ''Notes'' When Comm.bitTask = 974 then        
--''Follow-up'' else ''Unknown'' end         
dbo.GetListIemName(Comm.bitTask)AS Task,                
 listdetailsActivity.VcData As Activity ,                                                    
 listdetailsStatus.VcData As Status   ,  -- Priority column                                                                                             
 Comm.numCreatedBy,                                                                                                             
 Div.numTerId,                                           
-- case When Len( dbo.fn_GetContactName(numAssign) ) > 12                                           
--  Then Substring( dbo.fn_GetContactName(numAssign) , 0  ,  12 ) + ''..''                                           
--  Else dbo.fn_GetContactName(numAssign)                                           
-- end                                          
-- +                                          
-- '' / ''                                             
-- +                                              
-- case                                                
--  When Len( dbo.fn_GetContactName(Comm.numAssignedBy) ) > 12                                           
--  Then Substring( dbo.fn_GetContactName(Comm.numAssignedBy) , 0  ,  12 ) + ''..''                                        Else dbo.fn_GetContactName(Comm.numAssignedBy)                                          
-- end  

Substring( 
dbo.fn_GetContactName(numAssign) + 
ISNULL((SELECT SUBSTRING((SELECT '','' + dbo.fn_GetContactName(numContactId) FROM CommunicationAttendees WHERE numCommId=Comm.numCommId FOR XML PATH('''')),1,200000)),''''),0,50)
As numAssignedTo , 
dbo.fn_GetContactName(Comm.numAssignedBy) AS numAssignedBy,        
convert(varchar(50),comm.caseid) as caseid,                          
(select top 1 vcCaseNumber from cases where cases.numcaseid= comm.caseid )as vcCasenumber,                          
comm.casetimeId,                          
comm.caseExpId ,                        
0 as type   ,                    
'''' as itemid ,                    
'''' as changekey ,cast(Null as datetime) as DueDate ,bitFollowUpAnyTime,
dbo.GetListIemName(Comp.numNoOfEmployeesId) AS numNoOfEmployeesId,Div.vcComPhone'

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	set @strSql=@strSql+',tCS.vcColorScheme'
ELSE
	set @strSql=@strSql+','''' as vcColorScheme'

SET @strSql =@strSql + ' ,ISNULL(Slp_Name,'''') AS Slp_Name,ISNULL(intTotalProgress,0) AS [intTotalProgress],ISNULL(vcPoppName,'''') AS [vcPoppName], ISNULL(Opp.numOppId,0) AS [numOppId],ISNULL(Opp.numBusinessProcessID,0) AS numBusinessProcessID'

SET @strSql =@strSql + ' FROM  Communication Comm                              
 JOIN AdditionalContactsInformation AddC                                                  
 ON Comm.numContactId = AddC.numContactId                                  
 JOIN DivisionMaster Div                                                                         
 ON AddC.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                                                                         
 ON Div.numCompanyID = Comp.numCompanyId                                                       
 Left Join listdetails listdetailsActivity                                                  
 On Comm.numActivity = listdetailsActivity.NumlistItemID                                               
 Left Join listdetails listdetailsStatus                                                 
 On Comm.numStatus = listdetailsStatus.NumlistItemID
 LEFT JOIN OpportunityMaster Opp ON Opp.numDivisionId = Div.numDivisionID AND Opp.numContactId = AddC.numContactId  AND Opp.numOppID = Comm.numOppID 
 LEFT JOIN Sales_process_List_Master splm ON splm.Slp_Id = Opp.numBusinessProcessID 
 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId '
 
 
	DECLARE @Prefix AS VARCHAR(5)
 
 IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
	 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
		set @Prefix = 'AddC.'                  
    else if @vcCSLookBackTableName = 'DivisionMaster'                  
		set @Prefix = 'Div.'
	else if @vcCSLookBackTableName = 'CompanyInfo'                  
		set @Prefix = 'Comp.'  
	ELSE IF @vcCSLookBackTableName='Tickler'	
		set @Prefix = 'Comm.'  
--	else if @vcCSLookBackTableName = 'ProjectProgress'                  
--		set @Prefix = 'pp.'  	
--	else if @vcCSLookBackTableName = 'OpportunityMaster'                  
--		set @Prefix = 'om.'  
--	else if @vcCSLookBackTableName = 'Sales_process_List_Master'                  
--		set @Prefix = 'splm.'  		
	else
		set @Prefix = ''   
	
	
	IF @vcCSAssociatedControlType='DateField'
	BEGIN
	IF @vcCSOrigDbCOlumnName='FormattedDate'
			SET @vcCSOrigDbCOlumnName='dtStartTime'
	
	
			set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',' + @Prefix + @vcCSOrigDbCOlumnName + '),111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
			 and CONVERT(VARCHAR(10),DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',' + @Prefix + @vcCSOrigDbCOlumnName + '),111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
	END
	ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'  OR @vcCSAssociatedControlType='TextBox'
	BEGIN
		PRINT 1
		set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
	END
END

set @strSql=@strSql+'  WHERE Comm.numDomainID = ' + Cast(@numDomainID as varchar(10)) 
 
 IF LEN(ISNULL(@vcBProcessValue,''))>0
	BEGIN
		SET @strSql = @strSql + ' and ISNULL(Opp.numBusinessProcessID,0) in (SELECT Items FROM dbo.Split(''' +@vcBProcessValue + ''','',''))' 			
	END
		
                                   
set @strSql=@strSql+' AND 1=(Case ' + Cast(@bitFilterRecord as varchar(10)) +'  when 1 then Case when Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +' then 1 else 0 end 
		else  Case when (Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  or Comm.numCreatedBy=' + Cast(@numUserCntID as varchar(10)) +' ) then 1 else 0 end end)                                                                      
 AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) <= '''+Cast(@endDate as varchar(30))+''')                                                     
 AND Comm.bitclosedflag=0                                                                         
 and Comm.bitTask <> 973  ) As X '              

IF LEN(ISNULL(@vcBProcessValue,''))=0
BEGIN
 
SET @strSql1 =  ' insert into #tempRecords 
select                          
 ACI.numContactID,ACI.numDivisionID,Div.tintCRMType,ac.activityid as id ,case when alldayevent = 1 then ''5'' else ''0'' end as bitTask,                        
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) startdate,                        
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dateadd(second,duration,startdatetimeutc)) as endtime,                        
activitydescription as itemdesc,                        
ACI.vcFirstName + '' '' + ACI.vcLastName name, 
 ACI.vcFirstName,ACI.vcLastName,ACI.numPhone,ACI.numPhoneExtension,                   
 case when ACI.numPhone<>'''' then + ACI.numPhone +case when ACI.numPhoneExtension<>'''' then '' - '' + ACI.numPhoneExtension else '''' end  else '''' end as [Phone],                        
 Substring ( Comp.vcCompanyName , 0 , 12 ) + ''..'' As vcCompanyName ,                      
vcEmail,                        
''Calendar'' as task,                        
[subject] as Activity,                        
case when importance =0 then ''Low'' when importance =1 then ''Normal'' when importance =2 then ''High'' end as status,                        
0 as numcreatedby,                        
0 as numterid,                        
''-/-'' as numAssignedTo, 
''-'' AS numAssignedBy,        
''0'' as caseid,                        
null as vccasenumber,                        
0 as casetimeid,                        
0 as caseexpid,                        
1 as type ,                    
isnull(itemid,'''') as itemid  ,                    
changekey  ,cast(Null as datetime) as DueDate,cast(0 as bit) as bitFollowUpAnyTime,
dbo.GetListIemName(Comp.numNoOfEmployeesId) AS numNoOfEmployeesId,Div.vcComPhone,'''' as vcColorScheme,'''',0,'''',0 AS [numOppId],0 AS numBusinessProcessID
from activity ac join             
activityresource ar on  ac.activityid=ar.activityid             
join resource res on ar.resourceid=res.resourceid                         
--join usermaster UM on um.numUserId = res.numUserCntId                
join AdditionalContactsInformation ACI on ACI.numContactId = res.numUserCntId                
 JOIN DivisionMaster Div                                                          
 ON ACI.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                             
 ON Div.numCompanyID = Comp.numCompanyId                 
 where  res.numUserCntId = ' + Cast(@numUserCntID as varchar(10)) +'             
and ac.OriginalStartDateTimeUtc IS NULL            
and res.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  and 
(DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) <= '''+Cast(@endDate as varchar(30))+''')
--(startdatetimeutc between dateadd(month,-1,getutcdate()) and dateadd(month,1,getutcdate())) 
and [status] <> -1                                                     
and ac.activityid not in 
(select distinct(numActivityId) from communication Comm where Comm.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  
AND 1=(Case ' + Cast(@bitFilterRecord as varchar(10)) +' when 1 then Case when Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  then 1 else 0 end 
		else  Case when (Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  or Comm.numCreatedBy=' + Cast(@numUserCntID as varchar(10)) +' ) then 1 else 0 end end)                                                                      
-- AND (dtStartTime >= '''+Cast(@startDate as varchar(30))+''' and dtStartTime <= '''+Cast(@endDate as varchar(30))+''') 
)
 Order by endtime'

-- btDocType =2 for Document approval request, =1 for bizdoc
SET @strSql2 = ' insert into #tempRecords
select 
B.numContactID,B.numDivisionID,E.tintCRMType,CASE BA.btDocType WHEN 2 THEN BA.numOppBizDocsId WHEN 3 THEN BA.numOppBizDocsId ELSE A.numBizActionID END as ID,
convert(varchar(15),A.bitTask) as bitTask,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) as Startdate,                        
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) as EndTime ,  
'''' as itemDesc,
vcFirstName + '' '' + vcLastName  as [Name],
B.vcFirstName , B.vcLastName ,B.numPhone,B.numPhoneExtension,  
case when B.numPhone<>'''' then + B.numPhone +case when B.numPhoneExtension<>'''' then '' - '' + B.numPhoneExtension else '''' end  else '''' end as [Phone],
vcCompanyName,
vcEmail,
CASE BA.btDocType WHEN 2  THEN  ''Document Approval Request'' ELSE ''BizDoc Approval Request'' END as Task,
dbo.GetListIemName(bitTask) as Activity,
'''' as Status,
A.numCreatedBy,
E.numTerId, 
case When Len( dbo.fn_GetContactName(numAssign) ) > 12                                           
  Then Substring( dbo.fn_GetContactName(numAssign) , 0  ,  12 ) + ''..''                                           
  Else dbo.fn_GetContactName(numAssign)                                           
 end                                          
 numAssignedTo ,  
 ''-'' AS numAssignedBy,        
''0'' as caseid,
''0'' as vcCasenumber,
0 as CaseTimeId,
0 as CaseExpId,
CASE BA.btDocType WHEN 2 THEN 2 ELSE 3 END as [type], 
'''' AS itemid  ,      
 '''' AS changekey ,   
dtCreatedDate as DueDate  ,cast(0 as bit) as bitFollowUpAnyTime,
dbo.GetListIemName(D.numNoOfEmployeesId) AS numNoOfEmployeesId,E.vcComPhone,'''' as vcColorScheme,'''',0,'''', 0 AS [numOppId],0 AS numBusinessProcessID
from BizDocAction A LEFT JOIN dbo.BizActionDetails BA ON BA.numBizActionId = A.numBizActionId, 
AdditionalContactsInformation B, 
CompanyInfo D,
DivisionMaster E
WHERE A.numContactId=b.numContactId and a.numDomainID=b.numDomainID  and 
D.numCompanyId=E.numCompanyId and 
D.numDomainID=a.numDomainID and 
E.numDivisionId=B.numDivisionId and 
a.numDomainID= ' + Cast(@numDomainID as varchar(10)) +' and a.numContactId = ' + Cast(@numUserCntID as varchar(10)) +'  and 
A.numStatus=0 
and (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) <= '''+Cast(@endDate as varchar(30))+''')'

END


declare @Nocolumns as tinyint                
set @Nocolumns=0       
         
Select @Nocolumns=isnull(count(*),0) from View_DynamicColumns where numFormId=43 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  
  CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType char(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9))
                 
if @Nocolumns > 0            
begin      
INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID 
 FROM View_DynamicColumns 
 where numFormId=43 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
  order by tintOrder asc  
  
end            
else            
begin    
 INSERT INTO #tempForm
select tintOrder,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID 
 FROM View_DynamicDefaultColumns
 where numFormId=43 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 AND numDomainID=@numDomainID
order by tintOrder asc   
end                                            

DECLARE @strSql3 AS VARCHAR(8000)

SET @strSql3=   ' select * from #tempRecords order by ' + @columnName +' ' + @columnSortOrder  

print (@strSql)
exec (@strSql + @strSql1 + @strSql2 + @strSql3 )
drop table #tempRecords

SELECT * FROM #tempForm

DROP TABLE #tempForm
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateBillingTerms')
	DROP PROCEDURE USP_UpdateBillingTerms
GO

CREATE PROCEDURE [dbo].[USP_UpdateBillingTerms]
    (
	 @numDomainID  numeric=0,                                          
     @numDivisionID  numeric=0, 
	 @numCompanyID numeric=0,
	 @numCompanyCredit numeric=0,                                   
	 @vcDivisionName  varchar (100)='',
	 @numUserCntID  numeric=0,                                                                                                                                     
	 @tintCRMType  tinyint=0,                                          
	 @tintBillingTerms as tinyint,                                          
	 @numBillingDays as numeric(9),                                         
	 @tintInterestType as tinyint,                                          
	 @fltInterest as float,                          
	 @bitNoTax as BIT,
	 @numCurrencyID AS numeric(9)=0,
	 @numDefaultPaymentMethod AS numeric(9)=0,         
	 @numDefaultCreditCard AS numeric(9)=0,         
	 @bitOnCreditHold AS bit=0,
	 @vcShipperAccountNo VARCHAR(100) = '',
	 @intShippingCompany INT = 0,
	 @bitEmailToCase BIT=0,
	 @numDefaultExpenseAccountID NUMERIC(18,0) = 0         
    )
AS 
	BEGIN

		UPDATE DivisionMaster 
		SET numCompanyID = @numCompanyID,
			vcDivisionName = @vcDivisionName,                                      
			numModifiedBy = @numUserCntID,                                            
			bintModifiedDate = GETUTCDATE(),                                         
			tintCRMType = @tintCRMType,                                          
			tintBillingTerms = @tintBillingTerms,                                          
			numBillingDays = @numBillingDays,                                         
			tintInterestType = @tintInterestType,                                           
			fltInterest =@fltInterest,                
			bitNoTax=@bitNoTax,
			numCurrencyID=@numCurrencyID,
			numDefaultPaymentMethod=@numDefaultPaymentMethod,                                                  
			numDefaultCreditCard=@numDefaultCreditCard,                                                 
			bitOnCreditHold=@bitOnCreditHold,
			vcShippersAccountNo = @vcShipperAccountNo,
			intShippingCompany = @intShippingCompany,
			bitEmailToCase = @bitEmailToCase,
			numDefaultExpenseAccountID = @numDefaultExpenseAccountID			                                                  
		WHERE numDivisionID = @numDivisionID AND numDomainId= @numDomainID     

		UPDATE CompanyInfo
		SET numCompanyCredit = @numCompanyCredit,               
			numModifiedBy = @numUserCntID,                
			bintModifiedDate = GETUTCDATE()               
		WHERE                 
			numCompanyId=@numCompanyId and numDomainID=@numDomainID    

	END

-- EXEC USP_UpdateCRMTypeToAccounts	255208,170,229543
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_UpdateCRMTypeToAccounts' ) 
    DROP PROCEDURE USP_UpdateCRMTypeToAccounts
GO
CREATE PROCEDURE [dbo].[USP_UpdateCRMTypeToAccounts]
    (
      @numDivisionID NUMERIC(18),
      @numDomainID NUMERIC(18),
      @numUserCntID NUMERIC(18)
    )
AS 
    BEGIN
/* Biz usually automatically promotes the record from Lead/Prospect to Account When add a bill, create credit memo,  against it.*/  
-- Promote Lead to Account when Sales/Purchase Order is created against it
        DECLARE @tintCRMType TINYINT
        SELECT  @tintCRMType = ISNULL(tintCRMType,0)
        FROM    dbo.DivisionMaster
        WHERE   numDomainID = @numDomainID
                AND numDivisionID = @numDivisionID
		
		-- Whether given company type are leads,prospects, update & promote them as Accounts
          
        IF @tintCRMType = 0 
            BEGIN        
                UPDATE  divisionmaster
                SET     tintCRMType = 2,
                        bintLeadProm = GETUTCDATE(),
                        bintLeadPromBy = @numUserCntID,
                        bintProsProm = GETUTCDATE(),
                        bintProsPromBy = @numUserCntID
                WHERE   numDivisionID = @numDivisionID        
            END
		
		--Promote Prospect to Account
        ELSE 
            IF @tintCRMType = 1 
                BEGIN        
                    UPDATE  divisionmaster
                    SET     tintCRMType = 2,
                            bintProsProm = GETUTCDATE(),
                            bintProsPromBy = @numUserCntID
                    WHERE   numDivisionID = @numDivisionID        
                END            
    END        
/****** Object:  StoredProcedure [dbo].[USP_UpdatedomainDetails]    Script Date: 05/07/2009 21:56:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatedomaindetails')
DROP PROCEDURE usp_updatedomaindetails
GO
CREATE PROCEDURE [dbo].[USP_UpdatedomainDetails]                                      
@numDomainID as numeric(9)=0,                                      
@vcDomainName as varchar(50)='',                                      
@vcDomainDesc as varchar(100)='',                                      
@bitExchangeIntegration as bit,                                      
@bitAccessExchange as bit,                                      
@vcExchUserName as varchar(100)='',                                      
@vcExchPassword as varchar(100)='',                                      
@vcExchPath as varchar(200)='',                                      
@vcExchDomain as varchar(100)='',                                    
@tintCustomPagingRows as tinyint,                                    
@vcDateFormat as varchar(30)='',                                    
@numDefCountry as numeric(9),                                    
@tintComposeWindow as tinyint,                                    
@sintStartDate as smallint,                                    
@sintNoofDaysInterval as smallint,                                    
@tintAssignToCriteria as tinyint,                                    
@bitIntmedPage as bit,                                    
@tintFiscalStartMonth as tinyint,                            
--@bitDeferredIncome as bit,                               
@tintPayPeriod as tinyint,                        
@numCurrencyID as numeric(9) ,                
@charUnitSystem as char(1)='',              
@vcPortalLogo as varchar(100)='',            
@tintChrForComSearch as tinyint  ,            
@intPaymentGateWay as int,    
@tintChrForItemSearch as tinyint,
@ShipCompany as numeric(9),
@bitAutoPopulateAddress as bit=null,
@tintPoulateAddressTo as tinyint=null,
@bitMultiCurrency as bit,
--@bitMultiCompany as BIT,
@bitCreateInvoice AS tinyint = null,
@numDefaultSalesBizDocID AS NUMERIC(9)=NULL,
@numDefaultPurchaseBizDocID AS NUMERIC(9)=NULL,
@bitSaveCreditCardInfo AS BIT = NULL,
@intLastViewedRecord AS INT=10,
@tintCommissionType AS TINYINT,
@tintBaseTax AS TINYINT,
@bitEmbeddedCost AS BIT,
@numDefaultSalesOppBizDocId NUMERIC=NULL,
@tintSessionTimeOut TINYINT=1,
@tintDecimalPoints TINYINT,
@bitCustomizePortal BIT=0,
@tintBillToForPO TINYINT,
@tintShipToForPO TINYINT,
@tintOrganizationSearchCriteria TINYINT,
@bitDocumentRepositary BIT=0,
@tintComAppliesTo TINYINT=0,
@bitGtoBContact BIT=0,
@bitBtoGContact BIT=0,
@bitGtoBCalendar BIT=0,
@bitBtoGCalendar BIT=0,
@bitExpenseNonInventoryItem BIT=0,
@bitInlineEdit BIT=0,
@bitRemoveVendorPOValidation BIT=0,
@tintBaseTaxOnArea tinyint=0,
@bitAmountPastDue BIT=0,
@monAmountPastDue money,
@bitSearchOrderCustomerHistory BIT=0,
@bitRentalItem as bit=0,
@numRentalItemClass as NUMERIC(9)=NULL,
@numRentalHourlyUOM as NUMERIC(9)=NULL,
@numRentalDailyUOM as NUMERIC(9)=NULL,
@tintRentalPriceBasedOn as tinyint=NULL,
@tintCalendarTimeFormat as tinyint=NULL,
@tintDefaultClassType as tinyint=NULL,
@tintPriceBookDiscount as tinyint=0,
@bitAutoSerialNoAssign as bit=0,
@bitIsShowBalance AS BIT = 0,
@numIncomeAccID NUMERIC(18, 0) = 0,
@numCOGSAccID NUMERIC(18, 0) = 0,
@numAssetAccID NUMERIC(18, 0) = 0,
@IsEnableClassTracking AS BIT = 0,
@IsEnableProjectTracking AS BIT = 0,
@IsEnableCampaignTracking AS BIT = 0,
@IsEnableResourceScheduling AS BIT = 0,
@ShippingServiceItem  AS NUMERIC(9)=0,
@numSOBizDocStatus AS NUMERIC(9)=0,
@bitAutolinkUnappliedPayment AS BIT=0,
@numDiscountServiceItemID AS NUMERIC(9)=0,
@vcShipToPhoneNumber as VARCHAR(50)='',
@vcGAUserEmailId as VARCHAR(50)='',
@vcGAUserPassword as VARCHAR(50)='',
@vcGAUserProfileId as VARCHAR(50)='',
@bitDiscountOnUnitPrice AS BIT=0,
@intShippingImageWidth INT = 0,
@intShippingImageHeight INT = 0,
@numTotalInsuredValue NUMERIC(10,2) = 0,
@numTotalCustomsValue NUMERIC(10,2) = 0,
@vcHideTabs VARCHAR(1000) = '',
@bitUseBizdocAmount BIT = 0,
@IsEnableUserLevelClassTracking BIT = 0,
@bitDefaultRateType BIT = 0,
@bitIncludeTaxAndShippingInCommission  BIT = 0,
/*,@bitAllowPPVariance AS BIT=0*/
@bitPurchaseTaxCredit BIT=0
as                                      
                                      
 update Domain                                       
   set                                       
   vcDomainName=@vcDomainName,                                      
   vcDomainDesc=@vcDomainDesc,                                      
   bitExchangeIntegration=@bitExchangeIntegration,                                      
   bitAccessExchange=@bitAccessExchange,                                      
   vcExchUserName=@vcExchUserName,                                      
   vcExchPassword=@vcExchPassword,                                      
   vcExchPath=@vcExchPath,                                      
   vcExchDomain=@vcExchDomain,                                    
   tintCustomPagingRows =@tintCustomPagingRows,                                    
   vcDateFormat=@vcDateFormat,                                    
   numDefCountry =@numDefCountry,                                    
   tintComposeWindow=@tintComposeWindow,                                    
   sintStartDate =@sintStartDate,                                    
   sintNoofDaysInterval=@sintNoofDaysInterval,                                    
   tintAssignToCriteria=@tintAssignToCriteria,                                    
   bitIntmedPage=@bitIntmedPage,                                    
   tintFiscalStartMonth=@tintFiscalStartMonth,                            
--   bitDeferredIncome=@bitDeferredIncome,                          
   tintPayPeriod=@tintPayPeriod,
   vcCurrency=isnull((select varCurrSymbol from Currency Where numCurrencyID=@numCurrencyID  ),'')  ,                      
  numCurrencyID=@numCurrencyID,                
  charUnitSystem= @charUnitSystem,              
  vcPortalLogo=@vcPortalLogo ,            
  tintChrForComSearch=@tintChrForComSearch  ,      
  intPaymentGateWay=@intPaymentGateWay,
 tintChrForItemSearch=@tintChrForItemSearch,
 numShipCompany= @ShipCompany,
 bitAutoPopulateAddress = @bitAutoPopulateAddress,
 tintPoulateAddressTo =@tintPoulateAddressTo,
 --bitMultiCompany=@bitMultiCompany ,
 bitMultiCurrency=@bitMultiCurrency,
 bitCreateInvoice= @bitCreateInvoice,
 [numDefaultSalesBizDocId] =@numDefaultSalesBizDocId,
 bitSaveCreditCardInfo=@bitSaveCreditCardInfo,
 intLastViewedRecord=@intLastViewedRecord,
 [tintCommissionType]=@tintCommissionType,
 [tintBaseTax]=@tintBaseTax,
 [numDefaultPurchaseBizDocId]=@numDefaultPurchaseBizDocID,
 bitEmbeddedCost=@bitEmbeddedCost,
 numDefaultSalesOppBizDocId=@numDefaultSalesOppBizDocId,
 tintSessionTimeOut=@tintSessionTimeOut,
 tintDecimalPoints=@tintDecimalPoints,
 bitCustomizePortal=@bitCustomizePortal,
 tintBillToForPO = @tintBillToForPO,
 tintShipToForPO = @tintShipToForPO,
 tintOrganizationSearchCriteria=@tintOrganizationSearchCriteria,
-- vcPortalName=@vcPortalName,
 bitDocumentRepositary=@bitDocumentRepositary,tintComAppliesTo=@tintComAppliesTo,
 bitGtoBContact=@bitGtoBContact,
 bitBtoGContact=@bitBtoGContact,
 bitGtoBCalendar=@bitGtoBCalendar,
 bitBtoGCalendar=@bitBtoGCalendar,
 bitExpenseNonInventoryItem=@bitExpenseNonInventoryItem,
 bitInlineEdit=@bitInlineEdit,
 bitRemoveVendorPOValidation=@bitRemoveVendorPOValidation,
tintBaseTaxOnArea=@tintBaseTaxOnArea,
bitAmountPastDue = @bitAmountPastDue,
monAmountPastDue = @monAmountPastDue,
bitSearchOrderCustomerHistory=@bitSearchOrderCustomerHistory,
bitRentalItem=@bitRentalItem,
numRentalItemClass=@numRentalItemClass,
numRentalHourlyUOM=@numRentalHourlyUOM,
numRentalDailyUOM=@numRentalDailyUOM,
tintRentalPriceBasedOn=@tintRentalPriceBasedOn,
tintCalendarTimeFormat=@tintCalendarTimeFormat,
tintDefaultClassType=@tintDefaultClassType,
tintPriceBookDiscount=@tintPriceBookDiscount,
bitAutoSerialNoAssign=@bitAutoSerialNoAssign,
bitIsShowBalance = @bitIsShowBalance,
numIncomeAccID = @numIncomeAccID,
numCOGSAccID = @numCOGSAccID,
numAssetAccID = @numAssetAccID,
IsEnableClassTracking = @IsEnableClassTracking,
IsEnableProjectTracking = @IsEnableProjectTracking,
IsEnableCampaignTracking = @IsEnableCampaignTracking,
IsEnableResourceScheduling = @IsEnableResourceScheduling,
numShippingServiceItemID = @ShippingServiceItem,
numSOBizDocStatus=@numSOBizDocStatus,
bitAutolinkUnappliedPayment=@bitAutolinkUnappliedPayment,
numDiscountServiceItemID=@numDiscountServiceItemID,
vcShipToPhoneNo = @vcShipToPhoneNumber,
vcGAUserEMail = @vcGAUserEmailId,
vcGAUserPassword = @vcGAUserPassword,
vcGAUserProfileId = @vcGAUserProfileId,
bitDiscountOnUnitPrice=@bitDiscountOnUnitPrice,
intShippingImageWidth = @intShippingImageWidth ,
intShippingImageHeight = @intShippingImageHeight,
numTotalInsuredValue = @numTotalInsuredValue,
numTotalCustomsValue = @numTotalCustomsValue,
vcHideTabs = @vcHideTabs,
bitUseBizdocAmount = @bitUseBizdocAmount,
IsEnableUserLevelClassTracking = @IsEnableUserLevelClassTracking,
bitDefaultRateType = @bitDefaultRateType,
bitIncludeTaxAndShippingInCommission = @bitIncludeTaxAndShippingInCommission
/*,bitAllowPPVariance=@bitAllowPPVariance*/
--,bitPurchaseTaxCredit=@bitPurchaseTaxCredit
 where numDomainId=@numDomainID

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemsInventory_API')
DROP PROCEDURE USP_GetItemsInventory_API
GO
CREATE PROCEDURE [dbo].[USP_GetItemsInventory_API]
          @numDomainID AS NUMERIC(9),
          @WebAPIId    AS int,
          @ModifiedDate AS VARCHAR(50)='' 
AS
     
     
SELECT   numItemCode,
		   vcItemName,
		    ISNULL((SELECT TOP 1([monWListPrice])
             FROM   [WareHouseItems]
             WHERE  numItemId = numItemCode
             AND WareHouseItems.vcWHSKU = WI.vcWHSKU	
                    AND [numWareHouseID] IN (SELECT ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail 
                                             WHERE  [numDomainId] = I.[numDomainID] AND WebApiId = @WebAPIId)),
            I.monListPrice) AS monListPrice,
		   vcModelID,
		   ISNULL(fltWeight,0) intWeight,
		   ISNULL((SELECT TOP 1([numOnHand])
             FROM   [WareHouseItems]
             WHERE  numItemId = numItemCode
             AND WareHouseItems.vcWHSKU = WI.vcWHSKU	
                    AND [numWareHouseID] IN (SELECT ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail 
                                             WHERE  [numDomainId] = I.[numDomainID] AND WebApiId = @WebAPIId)),
            0) AS QtyOnHand,
		   ISNULL(IA.vcAPIItemID,0) AS vcAPIItemID,
		   ISNULL(WI.vcWHSKU,'') [vcWHSKU]	
  FROM     Item I LEFT OUTER JOIN [ItemAPI] IA ON I.[numItemCode] = IA.[numItemID] AND IA.WebApiId = @WebAPIId 
  INNER JOIN dbo.WareHouseItems WI ON i.numItemCode = wi.numItemID
  WHERE    I.numDomainID = @numDomainID  
	--AND LEN(vcModelID) >0
	AND  LEN(vcItemName) >0
	AND I.charItemType ='P'
	AND vcExportToAPI IS NOT NULL
	AND @WebAPIId IN (SELECT * FROM dbo.Split(vcExportToAPI,','))
	AND wi.dtModified > @ModifiedDate
	--AND I.numItemCode = 712096 AND ISNULL(WI.vcWHSKU,'') = '1100AAAD11W'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkFlow_TimeBasdAction')
DROP PROCEDURE USP_WorkFlow_TimeBasdAction
GO
-- =============================================    
-- Author:  <Author,,Sachin Sadhu>    
-- Create date: <Create Date,,27thMarch2014>    
-- Description: <Description,,To Implement Time based action> 
--EXEC USP_WorkFlow_TimeBasdAction 95587,'OpportunityMaster','BINTCREATEDDATE',2,2,1,@result OUTPUT       
-- =============================================       
CREATE PROCEDURE USP_WorkFlow_TimeBasdAction     
 -- Add the parameters for the stored procedure here    
     
 @numRecordID numeric(18, 0)=0,      
 @TableName VARCHAR(50),    
 @columnname   VARCHAR(50),    
 @intDays INT,    
 @intActionOn INT,    
 @numDomainID numeric(18, 0)=0    
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 --SET NOCOUNT ON;    
    -- Insert statements for procedure here    
 DECLARE @MatchingDate DATETIME    
 DECLARE @CurrentDate DATETIME    
    SET @CurrentDate=DATEADD(DD,0, DATEDIFF(DD,0, GETDATE()))    
 PRINT 'sachin'    
 DECLARE @SQL NVARCHAR(4000)    
 SET @SQL='SELECT @MatchingDate=['+replace(@columnname,' ','')+'] FROM '+@TableName+' WHERE numOppID='+ CAST(@numRecordID as varchar(50))    
 PRINT @SQL    
 EXEC SP_EXECUTESQL @SQL, N'@MatchingDate DATETIME OUTPUT',@MatchingDate OUTPUT    
    DECLARE @result VARCHAR(50)    
    SET @result=0    
   IF (@intActionOn=1)    
   BEGIN    
   IF (@CurrentDate=(DATEADD(DD,0, DATEDIFF(DD,@intDays, @MatchingDate))))    
   BEGIN    
    set @result=1    
   END    
  ELSE    
   BEGIN    
    set @result=0    
   END    
       
   END    
   ELSE    
   BEGIN    
   IF (@CurrentDate=(DATEADD(DD,@intDays, DATEDIFF(DD,0, @MatchingDate))))    
   BEGIN    
    set @result=1    
   END    
  ELSE    
   BEGIN    
    set @result=0    
   END    
   END    
       
 SELECT @result AS TimeMatchStatus      
    
    
    
END 
GO
IF EXISTS ( SELECT * FROM sysobjects WHERE xtype = 'V' AND NAME = 'VIEW_JOURNAL' ) 
    DROP VIEW VIEW_JOURNAL
GO
CREATE VIEW [dbo].[VIEW_JOURNAL]
WITH SCHEMABINDING 
AS
SELECT     dbo.AccountTypeDetail.numAccountTypeID, dbo.AccountTypeDetail.vcAccountCode, dbo.AccountTypeDetail.vcAccountType, 
                      SUM(ISNULL(dbo.General_Journal_Details.numDebitAmt, 0)) AS Debit, SUM(ISNULL(dbo.General_Journal_Details.numCreditAmt, 0)) AS Credit, 
                      dbo.General_Journal_Header.datEntry_Date, dbo.Chart_Of_Accounts.numAccountId, dbo.Chart_Of_Accounts.vcAccountName, 
                      dbo.Chart_Of_Accounts.vcAccountCode AS COAvcAccountCode, dbo.General_Journal_Header.numDomainID,
                      ISNULL(dbo.General_Journal_Header.numAccountClass,0) AS numAccountClass
FROM         dbo.Chart_Of_Accounts 
INNER JOIN dbo.Domain ON dbo.Domain.numDomainID = dbo.Chart_Of_Accounts.numDomainId 
INNER JOIN dbo.AccountTypeDetail ON dbo.Chart_Of_Accounts.numParntAcntTypeID = dbo.AccountTypeDetail.numAccountTypeID INNER JOIN
                      dbo.General_Journal_Header INNER JOIN
                      dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId ON 
                      dbo.Chart_Of_Accounts.numAccountId = dbo.General_Journal_Details.numChartAcntId
                      where dbo.General_Journal_Header.numDomainId=dbo.General_Journal_Details.numDomainId
                      AND dbo.General_Journal_Header.numDomainID = dbo.Domain.numDomainID 
					  AND dbo.General_Journal_Details.numDomainID = dbo.Domain.numDomainID 
GROUP BY dbo.AccountTypeDetail.numAccountTypeID, dbo.AccountTypeDetail.vcAccountCode, dbo.AccountTypeDetail.vcAccountType, 
                      dbo.General_Journal_Header.datEntry_Date, dbo.Chart_Of_Accounts.numAccountId, dbo.Chart_Of_Accounts.vcAccountName, 
                      dbo.Chart_Of_Accounts.vcAccountCode, dbo.General_Journal_Header.numDomainID,dbo.General_Journal_Header.numAccountClass
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype = 'V' AND NAME='VIEW_JOURNALBS')
DROP VIEW VIEW_JOURNALBS
GO
CREATE VIEW [dbo].[VIEW_JOURNALBS]
WITH SCHEMABINDING 
AS
SELECT     dbo.AccountTypeDetail.numAccountTypeID, dbo.AccountTypeDetail.vcAccountCode, dbo.AccountTypeDetail.vcAccountType, 
                      SUM(ISNULL(dbo.General_Journal_Details.numDebitAmt, 0)) AS Debit, SUM(ISNULL(dbo.General_Journal_Details.numCreditAmt, 0)) AS Credit, 
                      dbo.General_Journal_Header.datEntry_Date, dbo.Chart_Of_Accounts.numAccountId, dbo.Chart_Of_Accounts.vcAccountName, 
                      dbo.Chart_Of_Accounts.vcAccountCode AS COAvcAccountCode, dbo.AccountTypeDetail.numDomainID, COUNT_BIG(*) AS Expr1,
                      ISNULL(dbo.General_Journal_Header.numAccountClass,0) AS numAccountClass
FROM         dbo.Chart_Of_Accounts 
INNER JOIN dbo.Domain ON dbo.Domain.numDomainID = dbo.Chart_Of_Accounts.numDomainId 
INNER JOIN dbo.AccountTypeDetail ON dbo.Chart_Of_Accounts.numParntAcntTypeID = dbo.AccountTypeDetail.numAccountTypeID INNER JOIN
                      dbo.General_Journal_Header INNER JOIN
                      dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId ON 
                      dbo.Chart_Of_Accounts.numAccountId = dbo.General_Journal_Details.numChartAcntId 
                      AND dbo.General_Journal_Header.numDomainID = dbo.Domain.numDomainID 
					  AND dbo.General_Journal_Details.numDomainID = dbo.Domain.numDomainID 
                      AND (dbo.AccountTypeDetail.vcAccountCode LIKE '0101%' OR
                      dbo.AccountTypeDetail.vcAccountCode LIKE '0102%' OR
                      dbo.AccountTypeDetail.vcAccountCode LIKE '0105%')
GROUP BY dbo.AccountTypeDetail.numAccountTypeID, dbo.AccountTypeDetail.vcAccountCode, dbo.AccountTypeDetail.vcAccountType, 
                      dbo.General_Journal_Header.datEntry_Date, dbo.Chart_Of_Accounts.numAccountId, dbo.Chart_Of_Accounts.vcAccountName, 
                      dbo.Chart_Of_Accounts.vcAccountCode, dbo.AccountTypeDetail.numDomainID,dbo.General_Journal_Header.numAccountClass

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='v'AND NAME ='View_Random')
DROP VIEW View_Random
GO
-- select * from Dim_OrderDate order by monthkey desc
CREATE VIEW [dbo].[View_Random]
AS  
SELECT RAND() AS rnd 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- created By Anoop Jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE  xtype = 'V' AND NAME ='View_ShippingBox')
DROP VIEW View_ShippingBox
GO
CREATE VIEW View_ShippingBox
AS 

    SELECT  SB.[numBoxID],
			SRI.ShippingReportItemId,
            SB.[vcBoxName],
            SB.[numShippingReportId],
            SB.[fltTotalWeight],
            SB.[fltHeight],
            SB.[fltWidth],
            SB.[fltLength],
			SB.dtDeliveryDate,
			ISNULL(SRI.monShippingRate,ISNULL(SB.monShippingRate,0)) [monShippingRate],
			SB.vcShippingLabelImage,
            SB.vcTrackingNumber,
            SRI.[numOppBizDocItemID],
            OBI.[numItemCode],
			SRI.[fltTotalWeight] fltTotalWeightItem,
            SRI.[fltHeight] fltHeightItem,
            SRI.[fltWidth] fltWidthItem,
            SRI.[fltLength] fltLengthItem,
			OI.numOppId,
			OBI.numOppBizDocID,
            OI.[numoppitemtCode],
--            I.[fltHeight],
--            I.[fltWidth],
--            I.[fltLength],
            I.[fltWeight],
            ISNULL(OI.vcItemName,'') AS [vcItemName],
            OI.[vcModelID],
            OBI.[vcItemDesc],
			SR.numDomainId,
			SR.tintPayorType,
			ISNULL(SR.vcPayorAccountNo,'') vcPayorAccountNo,
			ISNULL(SR.vcPayorZip,'') vcPayorZip,
			ISNULL(SR.numPayorCountry,0) numPayorCountry,
			ISNULL(SR.vcValue2,0) AS tintServiceType,
			ISNULL((CASE WHEN ISNULL(SB.numPackageTypeID,0) = 0 THEN ISNULL(CP.numPackageTypeID,0) ELSE ISNULL(SB.numPackageTypeID,0) END) ,0) AS [numPackageTypeID],
			CP.vcPackageName,
			ISNULL(SRI.intBoxQty,0) AS [intBoxQty],
			ISNULL(SB.numServiceTypeID,0) AS [numServiceTypeID],
			ISNULL(fltDimensionalWeight,0) AS [fltDimensionalWeight],
			CAST(ROUND(((SELECT SUM(fltTotalWeight) FROM dbo.ShippingReportItems WHERE numShippingReportId = SR.numShippingReportId) * SRI.intBoxQty), 2) AS DECIMAL(9,2)) AS [fltTotalRegularWeight],
			ISNULL(SB.numShipCompany,0) AS [numShipCompany],
			(SELECT ISNULL(vcUnitName,'') FROM dbo.UOM WHERE dbo.UOM.numUOMId = OI.numUOMId AND dbo.UOM.numDomainId = SR.numDomainId) AS [vcUnitName],
			 CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4), dbo.fn_UOMConversion(ISNULL(OI.numUOMId, 0), I.numItemCode,SR.numDomainId, ISNULL(I.numBaseUnit, 0)) * OBI.monPrice)) monUnitPrice
    FROM    [ShippingBox] SB
            LEFT JOIN [ShippingReportItems] SRI ON SB.[numBoxID] = SRI.[numBoxID]
			LEFT JOIN dbo.ShippingReport SR ON SRI.numShippingReportId = SR.numShippingReportId
            LEFT JOIN [OpportunityBizDocItems] OBI ON SRI.[numOppBizDocItemID] = OBI.[numOppBizDocItemID]
            LEFT JOIN [OpportunityItems] OI ON OBI.[numOppItemID] = OI.[numoppitemtCode]
            LEFT JOIN dbo.CustomPackages CP ON CP.numCustomPackageID = SB.numPackageTypeID 
											AND (ISNULL(CP.numShippingCompanyID,0) = SB.numShipCompany OR ISNULL(CP.numShippingCompanyID,0) = 0)
            LEFT JOIN [Item] I ON OBI.[numItemCode] = I.[numItemCode]	
GO            

-----------------------

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_Item_SearchForSalesOrder' ) 
    DROP PROCEDURE USP_Item_SearchForSalesOrder
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sandeep Patel
-- Create date: 3 March 2014
-- Description:	Search items for sales order
-- =============================================
CREATE PROCEDURE USP_Item_SearchForSalesOrder
	@tintOppType as tinyint,      
	@numDomainID as numeric(9),      
	@numDivisionID as numeric(9)=0,      
	@str as varchar(20) ,
	@numUserCntID as numeric(9),
	@tintSearchOrderCustomerHistory as tinyint=0,
	@numPageIndex AS INT,
	@numPageSize AS INT,
	@WarehouseID AS NUMERIC(18,0) = NULL,
	@TotalCount AS INT OUTPUT
AS
BEGIN
	
	SET NOCOUNT ON;

SELECT @str = REPLACE(@str,'''', '''''')
	
DECLARE @TableRowCount TABLE (Value int);
	
select * into #Temp1 from (select numFieldId,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,tintRow AS tintOrder,0 as Custom
    from View_DynamicColumns
   where numFormId=22 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
        AND bitCustom=0 AND ISNULL(bitSettingField,0)=1 AND numRelCntType=0
union   
 select numFieldId,vcFieldName,vcFieldName,tintRow AS tintOrder,1 as Custom
    from View_DynamicCustomColumns  
   where Grp_id=5 AND numFormId=22 and numUserCntID=@numUserCntID and numDomainID=@numDomainID 
   AND tintPageType=1 AND bitCustom=1 AND numRelCntType=1)X 
  
  if not exists(select * from #Temp1)
  begin
    insert into #Temp1
     select numFieldId,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,tintorder,0 from View_DynamicDefaultColumns                                                  
     where numFormId=22 and bitDefault = 1 and ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID 
  end

Create table #tempItemCode (numItemCode numeric(9))

if @tintOppType>0       
begin 
DECLARE @bitRemoveVendorPOValidation AS BIT

SELECT @bitRemoveVendorPOValidation=ISNULL(bitRemoveVendorPOValidation,0) FROM domain WHERE numDomainID=@numDomainID

declare @strNewQuery nvarchar(4000)
Declare @strSQL as nvarchar(4000)
declare @tintOrder as int
declare @Fld_id as nvarchar(20)
declare @Fld_Name as varchar(20)
set @strSQL=''
	select top 1 @tintOrder=tintOrder+1,@Fld_id=numFieldId,@Fld_Name=vcFieldName from #Temp1 where Custom=1 order by tintOrder
	while @tintOrder>0
    begin
	set @strSQL=@strSQL+', dbo.GetCustFldValueItem('+@Fld_id+', I.numItemCode) as ['+ @Fld_Name+']'

	select top 1 @tintOrder=tintOrder+1,@Fld_id=numFieldId,@Fld_Name=vcFieldName from #Temp1 where Custom=1 and tintOrder>=@tintOrder order by tintOrder
	if @@rowcount=0 set @tintOrder=0
	end


--Temp table for Item Search Configuration

 select * into #tempSearch from (select numFieldId,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,tintRow AS tintOrder,0 as Custom
    from View_DynamicColumns
   where numFormId=22 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
        AND bitCustom=0  AND ISNULL(bitSettingField,0)=1 AND numRelCntType=1
union   
  select numFieldId,vcFieldName,vcFieldName,tintRow AS tintOrder,1 as Custom
    from View_DynamicCustomColumns   
   where Grp_id=5 AND numFormId=22 and numUserCntID=@numUserCntID and numDomainID=@numDomainID 
   AND tintPageType=1 AND bitCustom=1 AND numRelCntType=1)X 
  
  if not exists(select * from #tempSearch)
  begin
    insert into #tempSearch
    select numFieldId,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,tintorder,0 from View_DynamicDefaultColumns                                                  
     where numFormId=22 and bitDefault = 1 and ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID 
  END

--Regular Search
Declare @strSearch as varchar(8000),@CustomSearch AS VARCHAR(4000),@numFieldId AS NUMERIC(9)
SET @strSearch=''
	select top 1 @tintOrder=tintOrder+1,@Fld_Name=vcDbColumnName,@numFieldId=numFieldId from #tempSearch WHERE Custom=0 order by tintOrder
	while @tintOrder>-1
    BEGIN
	IF @Fld_Name='vcPartNo'
		insert into #tempItemCode SELECT distinct Item.numItemCode FROM Vendor join Item on Item.numItemCode=Vendor.numItemCode 
										where Item.numDomainID=@numDomainID and Vendor.numDomainID=@numDomainID 
										and Vendor.vcPartNo is not null and len(Vendor.vcPartNo)>0 and vcPartNo LIKE '%'+@str+'%'

-- 		set @strSearch=@strSearch+ ' I.numItemCode in (SELECT distinct Item.numItemCode FROM Vendor join Item on Item.numItemCode=Vendor.numItemCode 
--										where Item.numDomainID='+convert(varchar(15),@numDomainID) + ' and Vendor.numDomainID='+convert(varchar(15),@numDomainID) + ' 
--										and Vendor.vcPartNo is not null and len(Vendor.vcPartNo)>0 and vcPartNo LIKE ''%'+@str+'%'')'
	ELSE IF @Fld_Name='vcBarCode'
		insert into #tempItemCode SELECT distinct Item.numItemCode FROM Item join WareHouseItems WI on WI.numDomainID=@numDomainID and Item.numItemCode=WI.numItemID join Warehouses W on W.numDomainID=@numDomainID and W.numWareHouseID=WI.numWareHouseID  
									 where Item.numDomainID=@numDomainID and isnull(Item.numItemGroup,0)>0 and WI.vcBarCode is not null and len(WI.vcBarCode)>0 and WI.vcBarCode LIKE '%'+@str+'%'

-- 		set @strSearch=@strSearch+ ' I.numItemCode in (SELECT distinct Item.numItemCode FROM Item join WareHouseItems WI on WI.numDomainID='+convert(varchar(15),@numDomainID) + ' and Item.numItemCode=WI.numItemID join Warehouses W on W.numDomainID='+convert(varchar(15),@numDomainID) + ' and W.numWareHouseID=WI.numWareHouseID  
--									 where Item.numDomainID='+convert(varchar(15),@numDomainID) + ' and isnull(Item.numItemGroup,0)>0 and WI.vcBarCode is not null and len(WI.vcBarCode)>0 and WI.vcBarCode LIKE ''%'+@str+'%'')'
	ELSE IF @Fld_Name='vcWHSKU'
		insert into #tempItemCode SELECT distinct Item.numItemCode FROM Item join WareHouseItems WI on WI.numDomainID=@numDomainID and Item.numItemCode=WI.numItemID join Warehouses W on W.numDomainID=@numDomainID and W.numWareHouseID=WI.numWareHouseID  
									 where Item.numDomainID=@numDomainID and isnull(Item.numItemGroup,0)>0 and WI.vcWHSKU is not null and len(WI.vcWHSKU)>0 and WI.vcWHSKU LIKE '%'+@str+'%'

-- 		set @strSearch=@strSearch+ ' I.numItemCode in (SELECT distinct Item.numItemCode FROM Item join WareHouseItems WI on WI.numDomainID='+convert(varchar(15),@numDomainID) + ' and Item.numItemCode=WI.numItemID join Warehouses W on W.numDomainID='+convert(varchar(15),@numDomainID) + ' and W.numWareHouseID=WI.numWareHouseID  
--									 where Item.numDomainID='+convert(varchar(15),@numDomainID) + ' and isnull(Item.numItemGroup,0)>0 and WI.vcWHSKU is not null and len(WI.vcWHSKU)>0 and WI.vcWHSKU LIKE ''%'+@str+'%'')'
	else    
		set @strSearch=@strSearch+' ['+ @Fld_Name + '] LIKE ''%'+@str+'%'''

	select top 1 @tintOrder=tintOrder+1,@Fld_Name=vcDbColumnName,@numFieldId=numFieldId from #tempSearch where tintOrder>=@tintOrder AND Custom=0 order by tintOrder
		if @@rowcount=0 
		begin
			set @tintOrder=-1
		END
		ELSE IF @Fld_Name!='vcPartNo' and @Fld_Name!='vcBarCode' and @Fld_Name!='vcWHSKU'
		begin
			set @strSearch=@strSearch + ' or '
		END
	END

IF (select count(*) from #tempItemCode)>0
	set @strSearch=@strSearch + ' or  I.numItemCode in (select numItemCode from #tempItemCode)' 
--Custom Search
SET @CustomSearch=''
select top 1 @tintOrder=tintOrder+1,@Fld_Name=vcDbColumnName,@numFieldId=numFieldId from #tempSearch WHERE Custom=1 order by tintOrder
	while @tintOrder>-1
    BEGIN
    
	set @CustomSearch=@CustomSearch + CAST(@numFieldId AS VARCHAR(10)) 

	select top 1 @tintOrder=tintOrder+1,@Fld_Name=vcDbColumnName,@numFieldId=numFieldId from #tempSearch where tintOrder>=@tintOrder AND Custom=1 order by tintOrder
		if @@rowcount=0 
		begin
			set @tintOrder=-1
		END
		ELSE
		begin
			set @CustomSearch=@CustomSearch + ' , '
		END
	END
	
IF LEN(@CustomSearch)>0
BEGIN
 	SET @CustomSearch= ' I.numItemCode in (SELECT RecId FROM dbo.CFW_FLD_Values_Item WHERE Fld_ID IN (' + @CustomSearch  + ') and Fld_Value like ''%' + @str + '%'')'

IF LEN(@strSearch)>0
       SET @strSearch=@strSearch + ' OR ' +  @CustomSearch
ELSE
       SET @strSearch=  @CustomSearch  
END


if @tintOppType=1 OR (@bitRemoveVendorPOValidation=1 AND @tintOppType=2)       
begin      
  set @strSQL='select SRNO=ROW_NUMBER() OVER( ORDER BY  I.vcItemName), I.numItemCode,II.vcPathForImage,II.vcPathForTImage,isnull(vcItemName,'''') vcItemName,CASE WHEN I.[charItemType]=''P'' AND ISNULL(I.bitSerialized,0) = 0 AND  ISNULL(I.bitLotNo,0) = 0 THEN convert(varchar(100),ISNULL((SELECT TOP 1 ROUND(monWListPrice,2) FROM [WareHouseItems] WHERE [numItemID]=I.numItemCode),0)) ELSE CONVERT(VARCHAR(100), ROUND(monListPrice, 2)) END AS monListPrice,isnull(vcSKU,'''') vcSKU,isnull(numBarCodeId,0) numBarCodeId,isnull(vcModelID,'''') vcModelID,isnull(txtItemDesc,'''') as txtItemDesc,isnull(C.vcCompanyName,'''') as vcCompanyName'+@strSQL+' from Item  I 
  Left join ItemImages II
  on I.numItemCode = II.numItemCode
  Left join DivisionMaster D              
  on I.numVendorID=D.numDivisionID  
  left join CompanyInfo C  
  on C.numCompanyID=D.numCompanyID  
  where ((I.charItemType <> ''P'') OR ((SELECT COUNT(*) FROM dbo.WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '+convert(varchar(20),@numDomainID)+' AND numWareHouseID = '+convert(varchar(20),ISNULL(@WarehouseID,-1))+') > 0 OR ' + convert(varchar(20),ISNULL(@WarehouseID,-1)) + ' = -1)) AND ISNULL(I.Isarchieve,0) <> 1 And I.charItemType NOT IN(''A'') AND  I.numDomainID='+convert(varchar(20),@numDomainID)+' and (' +@strSearch+ ') '         
--- added Asset validation  by sojan

IF @bitRemoveVendorPOValidation=1 AND @tintOppType=2
	set @strSQL=@strSQL + ' and ISNULL(I.bitKitParent,0) = 0 '

if @tintSearchOrderCustomerHistory=1 and @numDivisionID>0
BEGIN
	set @strSQL=@strSQL + ' and I.numItemCode in (select distinct oppI.numItemCode from 
							OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='+convert(varchar(20),@numDomainID)+' and oppM.numDivisionId='+convert(varchar(20),@numDivisionId)+')'
END

INSERT INTO @TableRowCount
EXEC('SELECT COUNT(*) FROM ( ' + @strSQL + ') as t2');
SELECT @TotalCount = Value FROM @TableRowCount;

set @strNewQuery ='select * from ('+@strSQL+') as t where SRNO> ' + CAST(((@numPageIndex - 1) * @numPageSize) AS VARCHAR(10)) + ' and SRNO < ' + CAST(((@numPageIndex * @numPageSize) + 1) AS VARCHAR(10)) + ' order by  vcItemName'
EXEC(@strNewQuery)

end      
else if @tintOppType=2       
begin      
 set @strSQL='SELECT SRNO=ROW_NUMBER() OVER( ORDER BY  I.vcItemName), I.numItemCode,II.vcPathForImage,II.vcPathForTImage,isnull(vcItemName,'''') vcItemName,convert(varchar(200),round(monListPrice,2)) as monListPrice,isnull(vcSKU,'''') vcSKU,isnull(numBarCodeId,0) numBarCodeId,isnull(vcModelID,'''') vcModelID,isnull(txtItemDesc,'''') as txtItemDesc,isnull(C.vcCompanyName,'''') as vcCompanyName'+@strSQL+'  from item I 
 Left join ItemImages II
 on I.numItemCode = II.numItemCode
 join Vendor V              
 on V.numItemCode=I.numItemCode   
 Left join DivisionMaster D              
 on V.numVendorID=D.numDivisionID  
 left join CompanyInfo C  
 on C.numCompanyID=D.numCompanyID             
 where ((I.charItemType <> ''P'') OR ((SELECT COUNT(*) FROM dbo.WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '+convert(varchar(20),@numDomainID)+' AND numWareHouseID = '+convert(varchar(20),ISNULL(@WarehouseID,-1))+') > 0 OR ' + convert(varchar(20),ISNULL(@WarehouseID,-1)) + ' = -1)) AND ISNULL(I.Isarchieve,0) <> 1 and ISNULL(I.bitKitParent,0) = 0 And I.charItemType NOT IN(''A'') AND V.numVendorID='+convert(varchar(20),@numDivisionID)+' and (' +@strSearch+ ') ' 
--- added Asset validation  by sojan

if @tintSearchOrderCustomerHistory=1 and @numDivisionID>0
BEGIN
	set @strSQL=@strSQL + ' and I.numItemCode in (select distinct oppI.numItemCode from 
							OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='+convert(varchar(20),@numDomainID)+' and oppM.numDivisionId='+convert(varchar(20),@numDivisionId)+')'
END


INSERT INTO @TableRowCount
EXEC('SELECT COUNT(*) FROM ( ' + @strSQL + ') as t2');
SELECT @TotalCount = Value FROM @TableRowCount;

set @strNewQuery ='select * from ('+@strSQL+') as t where SRNO> ' + CAST(((@numPageIndex - 1) * @numPageSize) AS VARCHAR(10)) + ' and SRNO < ' + CAST(((@numPageIndex * @numPageSize) + 1) AS VARCHAR(10)) + ' order by  vcItemName'
EXEC(@strNewQuery)

end
else
select 0  
end
ELSE
	select 0  

select * from  #Temp1 where vcDbColumnName not in ('vcPartNo','vcBarCode','vcWHSKU')ORDER BY tintOrder 

drop table #Temp1
         
drop table #tempItemCode

END

------------------------------------------

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetCustFldOrganization')
DROP FUNCTION fn_GetCustFldOrganization
GO
CREATE FUNCTION [dbo].[fn_GetCustFldOrganization](@numFldId numeric(18),@pageId as tinyint,@numRecordId as numeric(9))  
 RETURNS varchar (100)   
AS  
BEGIN  
DECLARE @vcValue as  varchar(300)  
DECLARE @fld_type AS VARCHAR(20)

SELECT @fld_type = Fld_type FROM dbo.CFW_Fld_Master WHERE Fld_id = @numFldId

SELECT
	@vcValue = (CASE 
				WHEN @fld_type = 'SelectBox' THEN (SELECT vcData FROM dbo.ListDetails WHERE numListItemID = Fld_Value)
				WHEN @fld_type = 'CheckBox' THEN CASE WHEN isnull(Fld_Value,0)=1 THEN 'Yes' ELSE 'No' END
				ELSE Fld_Value 
				END)
FROM 
	CFW_FLD_Values 
WHERE 
	Fld_ID=@numFldId AND 
	RecId=@numRecordId  

if @vcValue is null set @vcValue=''  
RETURN @vcValue  
END
GO


-------------------------------------------

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_CompanyInfo_Search' ) 
    DROP PROCEDURE USP_CompanyInfo_Search
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 15 April 2014
-- Description:	Gets company info based on search fileds configured
-- =============================================
CREATE PROCEDURE USP_CompanyInfo_Search
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@isStartWithSearch BIT = 0,
	@searchText VARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT @searchText = REPLACE(@searchText,'''', '''''')
	
DECLARE @CustomerSelectFields AS VARCHAR(MAX)
DECLARE @CustomerSelectCustomFields AS VARCHAR(MAX)
DECLARE @ProspectsRights AS TINYINT                    
DECLARE @accountsRights AS TINYINT                   
DECLARE @leadsRights AS TINYINT 

SET @CustomerSelectFields = ''
SET @CustomerSelectCustomFields = ''
               
SET @ProspectsRights = ( SELECT TOP 1
                                MAX(GA.intViewAllowed) AS intViewAllowed
                         FROM   UserMaster UM ,
                                GroupAuthorization GA ,
                                PageMaster PM
                         WHERE  GA.numGroupID = UM.numGroupID
                                AND PM.numPageID = GA.numPageID
                                AND PM.numModuleID = GA.numModuleID
                                AND PM.numpageID = 2
                                AND PM.numModuleID = 3
                                AND UM.numUserID = ( SELECT numUserID
                                                     FROM   userMaster
                                                     WHERE  numUserDetailId = @numUserCntID
                                                   )
                         GROUP BY UM.numUserId ,
                                UM.vcUserName ,
                                PM.vcFileName ,
                                GA.numModuleID ,
                                GA.numPageID
                       )                        
                    
SET @accountsRights = ( SELECT TOP 1
                                MAX(GA.intViewAllowed) AS intViewAllowed
                        FROM    UserMaster UM ,
                                GroupAuthorization GA ,
                                PageMaster PM
                        WHERE   GA.numGroupID = UM.numGroupID
                                AND PM.numPageID = GA.numPageID
                                AND PM.numModuleID = GA.numModuleID
                                AND PM.numpageID = 2
                                AND PM.numModuleID = 4
                                AND UM.numUserID = ( SELECT numUserID
                                                     FROM   userMaster
                                                     WHERE  numUserDetailId = @numUserCntID
                                                   )
                        GROUP BY UM.numUserId ,
                                UM.vcUserName ,
                                PM.vcFileName ,
                                GA.numModuleID ,
                                GA.numPageID
                      )                
              
SET @leadsRights = ( SELECT TOP 1
                            MAX(GA.intViewAllowed) AS intViewAllowed
                     FROM   UserMaster UM ,
                            GroupAuthorization GA ,
                            PageMaster PM
                     WHERE  GA.numGroupID = UM.numGroupID
                            AND PM.numPageID = GA.numPageID
                            AND PM.numModuleID = GA.numModuleID
                            AND PM.numpageID = 2
                            AND PM.numModuleID = 2
                            AND UM.numUserID = ( SELECT numUserID
                                                 FROM   userMaster
                                                 WHERE  numUserDetailId = @numUserCntID
                                               )
                     GROUP BY UM.numUserId ,
                            UM.vcUserName ,
                            PM.vcFileName ,
                            GA.numModuleID ,
                            GA.numPageID
                   )                   
                    
-- START: Get organization display fields
SELECT * INTO #tempOrgDisplayFields FROM
(
	SELECT 
		numFieldId,
		vcDbColumnName,
		ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
		tintRow AS tintOrder,
		0 as Custom
    FROM 
		View_DynamicColumns
	WHERE 
		numFormId=96 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=0  AND ISNULL(bitSettingField,0)=1 AND numRelCntType=0
	UNION   
	SELECT 
		numFieldId,
		vcFieldName,
		vcFieldName,
		tintRow AS tintOrder,
		1 as Custom
    FROM 
		View_DynamicCustomColumns   
	WHERE 
		Grp_id=1 AND numFormId=96 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=1 AND numRelCntType=0
)X

IF EXISTS (SELECT * FROM #tempOrgDisplayFields)
BEGIN

	SELECT 
		@CustomerSelectFields = COALESCE (@CustomerSelectFields,'') + CAST(vcDbColumnName AS VARCHAR(100)) + ','	
	FROM 
		#tempOrgDisplayFields 
	WHERE
		Custom = 0
		
	-- Removes last , from string
	IF DATALENGTH(@CustomerSelectFields) > 0
		SET @CustomerSelectFields = LEFT(@CustomerSelectFields, LEN(@CustomerSelectFields) - 1)

	IF EXISTS(SELECT * FROM #tempOrgDisplayFields WHERE Custom = 1)
	BEGIN
		SELECT 
			@CustomerSelectCustomFields = COALESCE (@CustomerSelectCustomFields,'') + 'dbo.fn_GetCustFldOrganization(' + CAST(numFieldId AS VARCHAR(100)) + ',1,TEMP1.numCompanyID) AS [' + CAST (vcDbColumnName AS VARCHAR (100)) + '],'	
		FROM 
			#tempOrgDisplayFields 
		WHERE
			Custom = 1
	END

	-- Removes last , from string
	IF DATALENGTH(@CustomerSelectCustomFields) > 0
		SET @CustomerSelectCustomFields = LEFT(@CustomerSelectCustomFields, LEN(@CustomerSelectCustomFields) - 1)
END

-- END: Get organization display fields

-- START: Generate default select statement 
                    
DECLARE @strSQL AS VARCHAR(MAX)    

SET @strSQL = 'SELECT numDivisionID, numCompanyID, vcCompanyName' + 
	CASE @CustomerSelectFields
	WHEN '' THEN ''
	ELSE ',' + @CustomerSelectFields
	END
	 + 
	CASE @CustomerSelectCustomFields
	WHEN '' THEN ''
	ELSE ',' + @CustomerSelectCustomFields
	END
	 + ' FROM (SELECT 
	CMP.numDomainID,
	CMP.numCompanyID, 
	CMP.vcCompanyName, 
	DM.numDivisionID, 
	DM.vcDivisionName, 
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 1 AND numDomainID = DM.numDomainID AND numListItemID = DM.numStatusID),'''') as numStatusID, 
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 2 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numCompanyRating),'''') as numCompanyRating, 
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 3 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numCompanyCredit),'''') as numCompanyCredit,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 4 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numCompanyIndustry),'''') as numCompanyIndustry,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 5 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numCompanyType),'''') as numCompanyType,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 6 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numAnnualRevID),'''') as numAnnualRevID,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 7 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numNoOfEmployeesID),'''') as numNoOfEmployeesID,
	vcComPhone,
	vcComFax,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 18 AND numDomainID = DM.numDomainID AND numListItemID = CMP.vcHow),'''') as vcHow,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 21 AND numDomainID = DM.numDomainID AND numListItemID = CMP.vcProfile),'''') as vcProfile,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 438 AND numDomainID = DM.numDomainID AND numListItemID = DM.numCompanyDiff),'''') as numCompanyDiff,
	DM.vcCompanyDiff,
	isnull(CMP.txtComments,'''') as txtComments, 
	isnull(CMP.vcWebSite,'''') vcWebSite, 
	isnull(AD1.vcStreet,'''') as vcBillStreet, 
	isnull(AD1.vcCity,'''') as vcBillCity, 
	isnull(dbo.fn_GetState(AD1.numState),'''') as numBillState,
	isnull(AD1.vcPostalCode,'''') as vcBillPostCode,
	isnull(dbo.fn_GetListName(AD1.numCountry,0),'''') as numBillCountry,
	isnull(AD2.vcStreet,'''') as vcShipStreet,
	isnull(AD2.vcCity,'''') as vcShipCity,
	isnull(dbo.fn_GetState(AD2.numState),'''')  as numShipState,
	isnull(AD2.vcPostalCode,'''') as vcShipPostCode, 
	isnull(dbo.fn_GetListName(AD2.numCountry,0),'''') as numShipCountry,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 78 AND numDomainID = DM.numDomainID AND numListItemID = DM.numTerID),'''') as numTerID
FROM  
(
SELECT
	CMP.numCompanyId
FROM
	dbo.CompanyInfo CMP
INNER JOIN
	dbo.DivisionMaster d
ON
	CMP.numCompanyId = d.numCompanyID
INNER JOIN
	AdditionalContactsInformation AC
ON 
	AC.numDivisionID=d.numDivisionID AND ISNULL(AC.bitPrimaryContact,0)=1
WHERE 
	CMP.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' AND d.tintCRMType<>0 and d.tintCRMType=1' +
	CASE @ProspectsRights WHEN 0 THEN ' and d.numRecOwner=0 ' ELSE '' END +
    CASE @ProspectsRights WHEN 1 THEN ' and d.numRecOwner='+convert(varchar(15),@numUserCntID) ELSE '' END +                        
    CASE @ProspectsRights WHEN 2 THEN ' and (d.numTerID in (select numTerritoryID  from UserTerritory where numUserCntID='+convert(varchar(15),@numUserCntID)+') or d.numTerID=0)' ELSE '' END +
'
UNION
SELECT
	CMP.numCompanyId
FROM
	dbo.CompanyInfo CMP
INNER JOIN
	dbo.DivisionMaster d
ON
	CMP.numCompanyId = d.numCompanyID
INNER JOIN
	AdditionalContactsInformation AC
ON 
	AC.numDivisionID=d.numDivisionID AND ISNULL(AC.bitPrimaryContact,0)=1
WHERE 
	CMP.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' AND d.tintCRMType<>0 and d.tintCRMType=2' +
	CASE @accountsRights WHEN 0 THEN ' and d.numRecOwner=0 ' ELSE '' END +
    CASE @accountsRights WHEN 1 THEN ' and d.numRecOwner='+convert(varchar(15),@numUserCntID) ELSE '' END +                        
    CASE @accountsRights WHEN 2 THEN ' and (d.numTerID in (select numTerritoryID  from UserTerritory where numUserCntID='+convert(varchar(15),@numUserCntID)+') or d.numTerID=0)' ELSE '' END +
'
UNION
SELECT
	CMP.numCompanyId
FROM
	dbo.CompanyInfo CMP
INNER JOIN
	dbo.DivisionMaster d
ON
	CMP.numCompanyId = d.numCompanyID
INNER JOIN
	AdditionalContactsInformation AC
ON 
	AC.numDivisionID=d.numDivisionID AND ISNULL(AC.bitPrimaryContact,0)=1
WHERE 
	CMP.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' AND d.tintCRMType=0' +
	CASE @leadsRights WHEN 0 THEN ' and d.numRecOwner=0 ' ELSE '' END +
    CASE @leadsRights WHEN 1 THEN ' and d.numRecOwner='+convert(varchar(15),@numUserCntID) ELSE '' END +                        
    CASE @leadsRights WHEN 2 THEN ' and (d.numTerID in (select numTerritoryID  from UserTerritory where numUserCntID='+convert(varchar(15),@numUserCntID)+') or d.numTerID=0)' ELSE '' END +
') AS TEMP2
INNER JOIN
	CompanyInfo CMP
ON
	TEMP2.numCompanyId =  CMP.numCompanyId                                                 
INNER JOIN 
	DivisionMaster DM    
ON 
	DM.numCompanyID=CMP.numCompanyID
LEFT JOIN 
	dbo.AddressDetails AD1 
ON 
    AD1.numAddressID =  (
							SELECT 
								MAX(numAddressID) 
							FROM 
								AddressDetails 
							WHERE 
								AddressDetails.numDomainID = DM.numDomainID AND 
								AddressDetails.numRecordID = DM.numDivisionID AND 
								AddressDetails.tintAddressOf = 2 AND 
								AddressDetails.tintAddressType = 1 AND 
								AddressDetails.bitIsPrimary = 1
						  )
LEFT JOIN 
	AddressDetails AD2 
ON 
    AD2.numAddressID = (
							SELECT 
								MAX(numAddressID) 
							FROM 
								AddressDetails 
							WHERE 
								AddressDetails.numDomainID = DM.numDomainID AND 
								AddressDetails.numRecordID = DM.numDivisionID AND 
								AddressDetails.tintAddressOf = 2 AND 
								AddressDetails.tintAddressType = 2 AND 
								AddressDetails.bitIsPrimary = 1
						)) AS TEMP1'
	
-- END: Generate default select statement 

                  
-- START: Generate user defined search fields condition

DECLARE @searchWhereCondition varchar(MAX)
DECLARE @singleValueSearchFields AS varchar(MAX)   
DECLARE @multiValueSearchFields AS varchar(MAX) 
DECLARE @customSearchFields AS varchar(MAX)    


SET @searchWhereCondition = ''
SET @singleValueSearchFields = ''   
SET @multiValueSearchFields = ''
SET @customSearchFields = ''
                

SELECT * INTO #tempOrgSearch FROM
(
	SELECT 
		numFieldId,
		vcDbColumnName,
		ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
		tintRow AS tintOrder,
		0 as Custom
    FROM 
		View_DynamicColumns
	WHERE 
		numFormId=97 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=0  AND ISNULL(bitSettingField,0)=1 AND numRelCntType=0
	UNION   
	SELECT 
		numFieldId,
		vcFieldName,
		vcFieldName,
		tintRow AS tintOrder,
		1 as Custom
    FROM 
		View_DynamicCustomColumns   
	WHERE 
		Grp_id=1 AND numFormId=97 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=1 AND numRelCntType=0
)Y

IF @searchText != ''
BEGIN
	IF (SELECT COUNT(*) FROM #tempOrgSearch) > 0 
	BEGIN
		-- START: Remove white spaces from search values
		DECLARE @finalSearchText VARCHAR(MAX)
		DECLARE @TempTable TABLE (vcValue VARCHAR(MAX))

		WHILE LEN(@searchText) > 0
		BEGIN
			SET @finalSearchText = LEFT(@searchText, 
									ISNULL(NULLIF(CHARINDEX(',', @searchText) - 1, -1),
									LEN(@searchText)))
			SET @searchText = SUBSTRING(@searchText,
										 ISNULL(NULLIF(CHARINDEX(',', @searchText), 0),
										 LEN(@searchText)) + 1, LEN(@searchText))

			INSERT INTO @TempTable (vcValue) VALUES ( @finalSearchText )
		END
		
		--Removes white spaces
		UPDATE
			@TempTable
		SET
			vcValue = LTRIM(RTRIM(vcValue))
	
		--Converting table to comma seperated string
		SELECT @searchText = COALESCE(@searchText,'') +  CONVERT(VARCHAR(MAX),vcValue) + ',' FROM @TempTable
	
		--Remove last comma from final search string
		IF DATALENGTH(@searchText) > 0
			SET @searchText = LEFT(@searchText, LEN(@searchText) - 1)
	
		-- END: Remove white spaces from search values
	
		--START: Gets search fields where there is one-one relation 
		SELECT 
			@singleValueSearchFields = COALESCE(@singleValueSearchFields,'') + '(' + CAST (vcDbColumnName AS VARCHAR (50)) + ' LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END + REPLACE(@searchText, ',', '%'' OR ' + vcDbColumnName + ' LIKE  ' + CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')' + ' OR '	
		FROM 
			#tempOrgSearch 
		WHERE 
			numFieldId NOT IN (51,52,96,148,127) AND
			Custom = 0
			
		
		-- Removes last OR from string
		IF DATALENGTH(@singleValueSearchFields) > 0
			SET @singleValueSearchFields = LEFT(@singleValueSearchFields, LEN(@singleValueSearchFields) - 3)
			
		--END: Gets search fields where there is one-one relation 
		
		
		--START: Gets search fields where there is one-many relation 
		-- 1. CONTACT FIRST NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 51) > 0
			SET @multiValueSearchFields = '(SELECT COUNT(*) FROM AdditionalContactsInformation ACI WHERE ACI.numDivisionID=TEMP1.numDivisionID AND (ACI.vcFirstName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR ACI.vcFirstName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'	
		
		-- 2. CONTACT LAST NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 52) > 0
		BEGIN
			IF @multiValueSearchFields = ''
				SET @multiValueSearchFields = '(SELECT COUNT(*) FROM AdditionalContactsInformation ACI WHERE ACI.numDivisionID=TEMP1.numDivisionID AND (ACI.vcLastName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR ACI.vcLastName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
			ELSE	
				SET @multiValueSearchFields = @multiValueSearchFields + ' OR (SELECT COUNT(*) FROM AdditionalContactsInformation ACI WHERE ACI.numDivisionID=TEMP1.numDivisionID AND (ACI.vcLastName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR ACI.vcLastName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
		END	
		
		-- 3. ORDER NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 96) > 0
		BEGIN
			IF @multiValueSearchFields = ''
				SET @multiValueSearchFields = '(SELECT COUNT(*) FROM OpportunityMaster OM WHERE OM.numDivisionId = TEMP1.numDivisionID AND (OM.vcPOppName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR OM.vcPOppName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
			ELSE	
				SET @multiValueSearchFields = @multiValueSearchFields + ' OR (SELECT COUNT(*) FROM OpportunityMaster OM WHERE OM.numDivisionId = TEMP1.numDivisionID AND (OM.vcPOppName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR OM.vcPOppName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
		END	
		
		-- 4.PROJECT NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 148) > 0
		BEGIN
			IF @multiValueSearchFields = ''
				SET @multiValueSearchFields = '(SELECT COUNT(*) FROM dbo.ProjectsMaster PM WHERE PM.numDivisionId = TEMP1.numDivisionID AND (PM.vcProjectName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR PM.vcProjectName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
			ELSE	
				SET @multiValueSearchFields = @multiValueSearchFields + ' OR (SELECT COUNT(*) FROM dbo.ProjectsMaster PM WHERE PM.numDivisionId = TEMP1.numDivisionID AND (PM.vcProjectName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR PM.vcProjectName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
		END	
		
		-- 5. CASE NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 127) > 0
		BEGIN		
			IF @multiValueSearchFields = ''
				SET @multiValueSearchFields = '(SELECT COUNT(*) FROM dbo.Cases C WHERE C.numDivisionId = TEMP1.numDivisionID AND (C.textSubject LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR C.textSubject LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
			ELSE	
				SET @multiValueSearchFields = @multiValueSearchFields + ' OR (SELECT COUNT(*) FROM dbo.Cases C WHERE C.numDivisionId = TEMP1.numDivisionID AND (C.textSubject LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR C.textSubject LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
		END	
		
		--END: Gets search fields where there is one-many relation 
		
		--START: Get custom search fields
		
		IF EXISTS (SELECT * FROM #tempOrgSearch WHERE Custom = 1)
		BEGIN
			SELECT 
				@customSearchFields = COALESCE (@customSearchFields,'') + '( dbo.fn_GetCustFldOrganization(' + CAST(numFieldId AS VARCHAR(20)) + ',1,TEMP1.numCompanyID) LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END + REPLACE(@searchText, ',', '%'' OR dbo.fn_GetCustFldOrganization(' + CAST(numFieldId AS VARCHAR(20)) + ',1,TEMP1.numCompanyID) LIKE  ' + CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')' + ' OR '	
			FROM 
				#tempOrgSearch 
			WHERE 
				Custom = 1
		END
		
		-- Removes last OR from string
		IF DATALENGTH(@customSearchFields) > 0
			SET @customSearchFields = LEFT(@customSearchFields, LEN(@customSearchFields) - 3)
		
		--END: Get custom search fields
		
		IF DATALENGTH(@singleValueSearchFields) > 0
			SET @searchWhereCondition = @singleValueSearchFields
			
		IF DATALENGTH(@multiValueSearchFields) > 0
			IF DATALENGTH(@searchWhereCondition) > 0
				SET @searchWhereCondition = @searchWhereCondition + ' OR ' + @multiValueSearchFields
			ELSE
				SET @searchWhereCondition = @multiValueSearchFields
				
		IF DATALENGTH(@customSearchFields) > 0
			IF 	DATALENGTH(@searchWhereCondition) = 0
				SET @searchWhereCondition = @customSearchFields
			ELSE
				SET @searchWhereCondition = @searchWhereCondition + ' OR ' + @customSearchFields
			
	END
	ELSE
	BEGIN
		SET @searchWhereCondition = '(vcCompanyName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR vcCompanyName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')'
	END
END

-- END: Generate user defined search fields condition

IF DATALENGTH(@searchWhereCondition) > 0
		SET @strSQL = @strSQL + ' WHERE ' + @searchWhereCondition 
    
 
SET @strSQL = @strSQL + ' ORDER BY vcCompanyname'    

--SELECT @strSQL
EXEC (@strSQL)

END
GO

-----------------------------------------------------

GO
/****** Object:  Table [dbo].[SalesOrderConfiguration]    Script Date: 05/30/2014 13:53:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SalesOrderConfiguration](
	[numSOCID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[bitAutoFocusCustomer] [bit] NULL,
	[bitAutoFocusItem] [bit] NULL,
	[bitDisplayRootLocation] [bit] NULL,
	[bitAutoAssignOrder] [bit] NULL,
	[bitDisplayLocation] [bit] NULL,
	[bitDisplayFinancialStamp] [bit] NULL,
	[bitDisplayItemDetails] [bit] NULL,
	[bitDisplayUnitCost] [bit] NULL,
	[bitDisplayProfitTotal] [bit] NULL,
	[bitDisplayShippingRates] [bit] NULL,
	[bitDisplayPaymentMethods] [bit] NULL,
	[bitCreateOpenBizDoc] [bit] NULL,
	[numListItemID] [numeric](18, 0) NULL,
	[vcPaymentMethodIDs] [varchar](100) NULL,
 CONSTRAINT [PK_SalesOrderConfiguration] PRIMARY KEY CLUSTERED 
(
	[numSOCID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON

--------------------------------------------------------------


GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_SalesOrderConfiguration_Save' ) 
    DROP PROCEDURE USP_SalesOrderConfiguration_Save
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 18 Feb 2014
-- Description:	Saves new sales order form configuration
-- =============================================
CREATE PROCEDURE USP_SalesOrderConfiguration_Save
	@numSOCID NUMERIC(18,0) = NULL,
	@numDomainID NUMERIC(18,0) = NULL,
	@bitAutoFocusCustomer BIT,
	@bitAutoFocusItem BIT,
	@bitDisplayRootLocation BIT,
	@bitAutoAssignOrder BIT,
	@bitDisplayLocation BIT,
	@bitDisplayFinancialStamp BIT,
	@bitDisplayItemDetails BIT,
	@bitDisplayUnitCost BIT,
	@bitDisplayProfitTotal BIT,
	@bitDisplayShippingRates BIT,
	@bitDisplayPaymentMethods BIT,
	@bitCreateOpenBizDoc BIT,
	@numListItemID NUMERIC(18,0),
	@vcPaymentMethodIDs VARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF @numSOCID IS NULL
	BEGIN
		INSERT INTO dbo.SalesOrderConfiguration
		(
			numDomainID,
			bitAutoFocusCustomer,
			bitAutoFocusItem,
			bitDisplayRootLocation,
			bitAutoAssignOrder,
			bitDisplayLocation,
			bitDisplayFinancialStamp,
			bitDisplayItemDetails,
			bitDisplayUnitCost,
			bitDisplayProfitTotal,
			bitDisplayShippingRates,
			bitDisplayPaymentMethods,
			bitCreateOpenBizDoc,
			numListItemID,
			vcPaymentMethodIDs
		)
		VALUES
		(
			@numDomainID,
			@bitAutoFocusCustomer,
			@bitAutoFocusItem,
			@bitDisplayRootLocation,
			@bitAutoAssignOrder,
			@bitDisplayLocation,
			@bitDisplayFinancialStamp,
			@bitDisplayItemDetails,
			@bitDisplayUnitCost,
			@bitDisplayProfitTotal,
			@bitDisplayShippingRates,
			@bitDisplayPaymentMethods,
			@bitCreateOpenBizDoc,
			@numListItemID,
			@vcPaymentMethodIDs
		)
	END
	ELSE
	BEGIN
		UPDATE
			dbo.SalesOrderConfiguration
		SET
			numDomainID = @numDomainID,
			bitAutoFocusCustomer = @bitAutoFocusCustomer,
			bitAutoFocusItem = @bitAutoFocusItem,
			bitDisplayRootLocation = @bitDisplayRootLocation,
			bitAutoAssignOrder = @bitAutoAssignOrder,
			bitDisplayLocation = @bitDisplayLocation,
			bitDisplayFinancialStamp = @bitDisplayFinancialStamp,
			bitDisplayItemDetails = @bitDisplayItemDetails,
			bitDisplayUnitCost = @bitDisplayUnitCost,
			bitDisplayProfitTotal = @bitDisplayProfitTotal,
			bitDisplayShippingRates = @bitDisplayShippingRates,
			bitDisplayPaymentMethods = @bitDisplayPaymentMethods,
			bitCreateOpenBizDoc = @bitCreateOpenBizDoc,
			numListItemID = @numListItemID,
			vcPaymentMethodIDs  = @vcPaymentMethodIDs
		WHERE
			numSOCID = @numSOCID
	END
    
END

-------------------------------------------------
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_SalesOrderConfiguration_Get' ) 
    DROP PROCEDURE USP_SalesOrderConfiguration_Get
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 18 Feb 2014
-- Description:	Gets new sales order form configuration by DomainID
-- =============================================
CREATE PROCEDURE USP_SalesOrderConfiguration_Get
	@numDomainID NUMERIC(18,0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT * FROM dbo.SalesOrderConfiguration WHERE numDomainID = @numDomainID
    
END
---------------------------------------------------

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageNewItemRequiredFields' ) 
    DROP PROCEDURE USP_ManageNewItemRequiredFields
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 30 May 2014
-- Description:	Adds default required fields for Field Management -> New Item in administation section
-- =============================================
CREATE PROCEDURE USP_ManageNewItemRequiredFields
	@numDomainID NUMERIC(18,0)
AS
BEGIN

-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		---- Inserts required field mapping in DycFormConfigurationDetails table so it can not be removed form configuration wizard for new inventory item form
	    IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 86 AND numFieldId = 189 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (86,189,1,1,@numDomainId,0,0,2,0)
		END
		IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 86 AND numFieldId = 270 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (86,270,1,2,@numDomainId,0,0,2,0)
		END
		IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 86 AND numFieldId = 271 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (86,271,2,1,@numDomainId,0,0,2,0)
		END
		IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 86 AND numFieldId = 272 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (86,272,2,2,@numDomainId,0,0,2,0)
		END
		
		--- Inserts required field mapping in DynamicFormField_Validation for validation on form new inventory item form
		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 86 AND numFormFieldId = 189 AND numDomainId = @numDomainId)
		BEGIN
			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (86,189,@numDomainId,'Item',1)
		END
		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 86 AND numFormFieldId = 270 AND numDomainId = @numDomainId)
		BEGIN
			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (86,270,@numDomainId,'Income Account',1)
		END
		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 86 AND numFormFieldId = 271 AND numDomainId = @numDomainId)
		BEGIN
			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (86,271,@numDomainId,'Asset Account',1)
		END
		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 86 AND numFormFieldId = 272 AND numDomainId = @numDomainId)
		BEGIN
			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (86,272,@numDomainId,'COGS Account',1)
		END
		
		---- Inserts required field mapping in DycFormConfigurationDetails table so it can not be removed form configuration wizard for new non inventory item form
		IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 87 AND numFieldId = 189 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (87,189,1,1,@numDomainId,0,0,2,0)
		END
		IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 87 AND numFieldId = 270 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (87,270,1,2,@numDomainId,0,0,2,0)
		END
		IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 87 AND numFieldId = 272 AND numDomainId = @numDomainId)
			BEGIN
		INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (87,272,2,1,@numDomainId,0,0,2,0)
		END
		
		--- Inserts resuired field mapping in DynamicFormField_Validation for validation on form
		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 87 AND numFormFieldId = 189 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (87,189,@numDomainId,'Item',1)
		END
		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 87 AND numFormFieldId = 270 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (87,270,@numDomainId,'Income Account',1)
		END
		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 87 AND numFormFieldId = 272 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (87,272,@numDomainId,'COGS Account',1)
		END
		
		 ---- Inserts required field mapping in DycFormConfigurationDetails table so it can not be removed form configuration wizard for new service/lot item form
		 IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 88 AND numFieldId = 189 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (88,189,1,1,@numDomainId,0,0,2,0)
		END
		IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 88 AND numFieldId = 270 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (88,270,1,2,@numDomainId,0,0,2,0)
		END
		IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 88 AND numFieldId = 271 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (88,271,2,1,@numDomainId,0,0,2,0)
		END
		IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 88 AND numFieldId = 272 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (88,272,2,2,@numDomainId,0,0,2,0)
		END
		
		--- Inserts required field mapping in DynamicFormField_Validation for validation on form new service lot item form
		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 88 AND numFormFieldId = 189 AND numDomainId = @numDomainId)
		BEGIN
			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (88,189,@numDomainId,'Item',1)
		END
		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 88 AND numFormFieldId = 270 AND numDomainId = @numDomainId)
		BEGIN
			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (88,270,@numDomainId,'Income Account',1)
		END
		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 88 AND numFormFieldId = 271 AND numDomainId = @numDomainId)
		BEGIN
			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (88,271,@numDomainId,'Asset Account',1)
		END
		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 88 AND numFormFieldId = 272 AND numDomainId = @numDomainId)
		BEGIN
			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (88,272,@numDomainId,'COGS Account',1)
		END

END
GO

----------------------------------------

