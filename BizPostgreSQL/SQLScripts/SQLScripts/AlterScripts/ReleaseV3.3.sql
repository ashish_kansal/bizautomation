/******************************************************************
Project: Release 3.3 Date: 19.07.2014
Comments: 
*******************************************************************/

------------------- SACHIN ------------------

	ALTER TABLE dbo.workFlowActionList ADD
	vcSMSText ntext NULL

----------------------------------------------


------------------- SANDEEP ------------------

-------------------------------------------------------------------------------------------------------------------------------------
/* Adds field numOrientation and bitKeepFooterBottom in Table BizDocTemplate to save bizdoc pdf orientation(Portrait OR Landscape) */
-------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE BizDocTemplate ADD 
bitKeepFooterBottom bit NULL,
numOrientation int NULL
GO

-------------------------------------------------------------------
/* Adds Net Days field in section 3 of sales order configuration */
-------------------------------------------------------------------
DECLARE @numFieldID NUMERIC(18,0)

SELECT	
	@numFieldID = numFieldId 
FROM 
	dycFieldMaster 
WHERE 
	numModuleID = 4 AND 
	vcDbColumnName = 'numBillingDays' AND 
	vcOrigDbColumnName = 'numBillingDays' AND 
	vcPropertyName = 'BillingDays' AND
	vcLookBackTableName = 'DivisionMaster'

IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = @numFieldID AND numFormID = 93)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,@numFieldID,93,1,0,0)
END



-------------------------------------------------------------------------------------------------
/* Adds new field bintClosedDate in OpportunityMaster to save close date when order is closed */
-------------------------------------------------------------------------------------------------
ALTER TABLE OpportunityMaster
ADD bintClosedDate DATETIME NULL

/* Updates OpportunityMaster to set bintClosedDate for all closed order */
UPDATE 
	OpportunityMaster
SET
	bintClosedDate = bintAccountClosingDate
WHERE 
	tintshipped = 1

/* Change the mapping of Close Date to column bintClosedDate from Old column bintAccountClosingDate which sets filed value even if order is not closed  */
UPDATE
	DycFieldMaster
SET
	vcDbColumnName = 'bintClosedDate',
	vcOrigDbColumnName = 'bintClosedDate'
WHERE
	numFieldId = 117

-----------------------------------------------------------------
/* Add field default payment method in New order configuration */
-----------------------------------------------------------------
DECLARE @numFieldID NUMERIC(18,0)
INSERT INTO 
	DycFieldMaster
(
	numModuleID,
	vcFieldName,
	vcDbColumnName,
	vcOrigDbColumnName,
	vcPropertyName,
	vcLookBackTableName,
	vcFieldDataType,
	vcFieldType,
	vcAssociatedControlType,
	vcListItemType,
	numListID,
	bitInResults,
	bitDeleted,
	bitAllowEdit,
	bitDefault
)
Values
(
	1,
	'Pref. Payment',
	'numDefaultPaymentMethod',
	'numDefaultPaymentMethod',
	'DefaultPaymentMethod',
	'DivisionMaster',
	'N',
	'R',
	'SelectBox',
	'LI',
	31,
	1,
	0,
	0,
	0
)

SELECT @numFieldID = SCOPE_IDENTITY() 
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = @numFieldID AND numFormID = 93)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,@numFieldID,93,1,0,0)
END

----------------------------------------------------------------------
/***************** Unit Price Approval ******************************/
----------------------------------------------------------------------

	/****** Object:  Table [dbo].[UnitPriceApprover]    Script Date: 6/25/2014 10:20:38 AM ******/
	SET ANSI_NULLS ON
	GO

	SET QUOTED_IDENTIFIER ON
	GO

	CREATE TABLE [dbo].[UnitPriceApprover](
		[numID] [int] IDENTITY(1,1) NOT NULL,
		[numDomainID] [numeric](18, 0) NOT NULL,
		[numUserID] [numeric](18, 0) NOT NULL,
	 CONSTRAINT [PK_UnitPriceApprover] PRIMARY KEY CLUSTERED 
	(
		[numID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

	GO

	/**** Added Unit Price Approval related fields in domain table ****/

	ALTER TABLE dbo.Domain ADD
		bitMinUnitPriceRule bit NULL,
		numAbovePercent numeric(18,2) NULL,
		numAbovePriceField numeric(18,0) NULL,
		numBelowPercent numeric(18,2) NULL,
		numBelowPriceField numeric(18,0) NULL,
		numOrderStatusBeforeApproval numeric(18,0) NULL,
		numOrderStatusAfterApproval numeric(18,0) NULL
	GO

	/**** Added bitItemPriceApprovalRequired field in OpportunityItems for identifying item requiring approval ****/
	ALTER TABLE dbo.OpportunityItems ADD
		bitItemPriceApprovalRequired bit NULL
	GO

-----------------------------------------
/**** Edit Unit Price Authorization ****/
-----------------------------------------
	INSERT INTO 
		PageMaster 
	(
		numPageID,
		numModuleID, 
		vcFileName, 
		vcPageDesc, 
		bitIsViewApplicable, 
		bitIsAddApplicable, 
		bitIsUpdateApplicable, 
		bitIsDeleteApplicable, 
		bitIsExportApplicable
	)
	VALUES
	(
		(SELECT ISNULL(MAX(numPageID),0) + 1 FROM PageMaster WHERE numModuleID = 10),
		10,
		'NewOrderOpportunity.aspx',
		'Edit Unit Price',
		0,
		0,
		1,
		0,
		0
	)

	DECLARE @newPageID numeric(18,0)
	SELECT @newPageID = ISNULL(MAX(numPageID),0) FROM PageMaster WHERE numModuleID = 10 

	/*************************** Allow Edit Unit Price access to all users for all domains *******************************/

	SELECT * INTO #temp FROM
    (
		SELECT 
			ROW_NUMBER() OVER(ORDER BY numDomainId DESC) AS Row, 
			numDomainId 
		FROM 
			Domain 
		WHERE 
			numDomainId <> -255
	) TABLE2
	
	DECLARE @RowCount INT
	SET @RowCount = (SELECT COUNT(Row) FROM #temp) 
	
	DECLARE @I INT
	DECLARE @numDomainId NUMERIC(18,0);
	
	SET @I = 1

	WHILE (@I <= @RowCount)
	BEGIN
			
		SELECT @numDomainId = numDomainID FROM #temp WHERE Row = @I
		
		INSERT INTO 
			dbo.GroupAuthorization
		(
			numDomainID,
			numModuleID,
			numPageID,
			numGroupID,
			intExportAllowed,
			intPrintAllowed,
			intViewAllowed,
			intAddAllowed,
			intUpdateAllowed,
			intDeleteAllowed
		)
		SELECT
			@numDomainId,
			10,
			@newPageID,
			numGroupID,
			0,
			0,
			0,
			0,
			3,
			0
		FROM
			AuthenticationGroupMaster
		WHERE
			numDomainID = @numDomainId AND
			tintGroupType = 1
			
		SET @I = @I  + 1
	END
	
	DROP TABLE #temp



----------------------------------------------


------------------- MANISH ------------------

------/******************************************************************
------Project: BACRMUI   Date: 03.07.2014
------Comments: CHange the size of decDiscount column within PricingTable
------*******************************************************************/
BEGIN TRANSACTION

ALTER TABLE [dbo].[PricingTable] ALTER COLUMN [decDiscount] [decimal] (18, 4) NOT NULL


<!--<add key="EmailBroadcastServerIP" value="71.189.47.60"/>--> <!--Production Server-->
        <!--<add key="EmailBroadcastServerIP" value="71.189.47.61"/>--> <!--Demo Server-->
        <add key="EmailBroadcastServerIP" value="71.189.47.59"/> <!--Mail Server-->

ROLLBACK


----------------------------------------------