------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------BizForm Wizard for CustomePart#-----------------------------------------------------------------------------------
--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numFieldID NUMERIC(18,0)

--	INSERT INTO DycFieldMaster
--	(
--		numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcPropertyName, vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,tintRow,tintColumn,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,bitAllowSorting,bitAllowFiltering
--	)
--	VALUES
--	(
--		3,'Customer Part#','CustomerPartNo','CustomerPartNo','CustomerPartNumber','CustomerPartNo','N','R','TextBox','',0,1,1,1,0,0,0,1,1,0,1
--	)

--	SET @numFieldID = SCOPE_IDENTITY()

--	INSERT INTO DycFormField_Mapping
--	(
--		numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--	)
--	VALUES
--	(
--		3,@numFieldID,7,0,0,'Customer Part#','TextBox',1,1,1,0,0,1,0,1,0,1
--	)

--	INSERT INTO DycFormField_Mapping
--	(
--		numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,bitAddField 
--	)
--	VALUES
--	(
--		4,@numFieldID,91,0,0,1
--	)

--	INSERT INTO DycFormField_Mapping
--	(
--		numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,
--		vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowFiltering
--	)
--	VALUES
--	(
--		3,@numFieldID,26,1,1,'Customer Part#','TextBox',
--		'CustomerPartNo',1,0,1,1,1
--	)

--	INSERT INTO DycFormField_Mapping
--	(
--		numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--	)
--	VALUES
--	(
--		3,@numFieldID,21,1,1,'Customer Part#','TextBox',1,1,1,0,0,1,0,1,0,1
--	)
--	--40853
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH
--------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------Manage Authorisation for CustomePart#-----------------------------------------------------------------------------------

--INSERT INTO PageMaster
--		(numPageID,numModuleID,vcFileName,vcPageDesc,bitIsViewApplicable,bitIsAddApplicable,bitIsDeleteApplicable,bitIsExportApplicable,bitIsUpdateApplicable)
--	VALUES
--		(136,10,'frmNewOrder.aspx','Customer Part#',0,1,0,0,1)

--------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------NewTable for CustomePart#-----------------------------------------------------------------------------------

--/****** Object:  Table [dbo].[CustomerPartNumber]    Script Date: 31-03-2018 12:54:08 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[CustomerPartNumber](
--	[CustomerPartNoID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numItemCode] [numeric](18, 0) NOT NULL,
--	[numCompanyId] [numeric](18, 0) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[CustomerPartNo] [varchar](100) NOT NULL,
-- CONSTRAINT [PK_CustomerPartNumber] PRIMARY KEY CLUSTERED 
--(
--	[CustomerPartNoID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]
--GO

--ALTER TABLE [dbo].[CustomerPartNumber]  WITH CHECK ADD  CONSTRAINT [FK_CustomerPartNumber_Domain] FOREIGN KEY([numDomainID])
--REFERENCES [dbo].[Domain] ([numDomainId])
--GO

--ALTER TABLE [dbo].[CustomerPartNumber] CHECK CONSTRAINT [FK_CustomerPartNumber_Domain]
--GO

--ALTER TABLE [dbo].[CustomerPartNumber]  WITH CHECK ADD  CONSTRAINT [FK_CustomerPartNumber_Item1] FOREIGN KEY([numItemCode])
--REFERENCES [dbo].[Item] ([numItemCode])
--GO

--ALTER TABLE [dbo].[CustomerPartNumber] CHECK CONSTRAINT [FK_CustomerPartNumber_Item1]
--GO


--------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------Updated label from Warehouse Stock OnHand To On-Hand-----------------------------------------------------------------------------------

----select * from DycFormField_Mapping where vcFieldName like '%Warehouse Stock OnHand%' and numFormID in (27,129,26)

--UPDATE DycFormField_Mapping SET vcFieldName = 'On-Hand' WHERE vcFieldName LIKE '%Warehouse Stock OnHand%' AND numFormID IN (27,129,26)
--------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------Added Column names to SO Bizdoc-----------------------------------------------------------------------------------
--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numFieldID NUMERIC(18,0)

--	INSERT INTO DycFieldMaster
--	(
--		numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName, vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,tintRow,tintColumn,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,bitAllowSorting,bitAllowFiltering
--	)
--	VALUES
--	(
--		3,'Available','numAvailable','numAvailable','WareHouseItems','N','R','TextBox','',0,1,1,1,0,0,0,1,1,0,1
--	)

--	SET @numFieldID = SCOPE_IDENTITY()

--	INSERT INTO DycFormField_Mapping
--	(
--		numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--	)
--	VALUES
--	(
--		3,@numFieldID,7,0,0,'Available','TextBox',1,1,1,0,0,1,0,1,0,1
--	)
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH
---------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------
----dtReleaseDate -- numFieldID = 40730
--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,
--	tintRow,bitInResults,bitDeleted,bitDefault,bitAllowFiltering
--)
--VALUES
--(
--	3,40730,7,0,0,'Item Release Date','TextBox', 1,1,0,0,1
--)

----vcWarehouse -- numFieldID = 199
--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,
--	tintRow,bitInResults,bitDeleted,bitDefault,bitAllowFiltering
--)
--VALUES
--(
--	3,199,7,0,0,'WH Location','TextBox', 1,1,0,0,1
--)

----numonhand -- numFieldID = 195
--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,
--	tintRow,bitInResults,bitDeleted,bitDefault,bitAllowFiltering
--)
--VALUES
--(
--	3,195,7,0,0,'On-Hand','TextBox', 1,1,0,0,1
--)
--------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------Changes to referring string - Lead source Admin-----------------------------------------------------------------------------------

--ALTER TABLE TrackingVisitorsHDR ADD numListItemID NUMERIC(18,0)
--------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------Changes to PO Fulfillment Table------------------------------------------------------------------------------------

--ALTER TABLE PurchaseOrderFulfillmentSettings DROP column numBizDocTempID 
--------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------Changes to Item Template------------------------------------------------------------------------------------

--ALTER TABLE OpportunitySalesTemplate ADD tintAppliesTo INT
------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------Changes to Leads Menus------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------Changes to Leads Menus------------------------------------------------------------------------------------

--DECLARE @numPageNavID INT, @numParentID INT

----SELECT * FROM TabMaster WHERE numTabName LIKE '%Relationships%' AND tintTabType=1
--SELECT @numPageNavID = numtabID FROM TabMaster WHERE numTabName LIKE '%Relationships%' AND tintTabType=1
----SELECT @numPageNavID 

----SELECT * FROM PageNavigationDTL WHERE vcPageNavName='Leads'
--SELECT @numParentID=numParentID  FROM PageNavigationDTL WHERE vcPageNavName='Leads'
----SELECT @numParentID

--UPDATE PageNavigationDTL SET vcNavURL ='../Leads/frmLeadList.aspx', vcaddurl='~/include/frmAddOrganization.aspx?RelID=1&FormID=34', bitAddIsPopUp=1
--WHERE numPageNavID=@numPageNavID AND numParentID=@numParentID


--UPDATE ShortCutBar SET vcLinkName='Leads', Link='../Leads/frmLeadList.aspx', newlink='../include/frmAddOrganization.aspx?RelID=1&FormID=34'
--WHERE vcLinkName='MyLeads'

--DELETE FROM ShortCutBar WHERE vcLinkName='WebLeads'

--DELETE FROM ShortCutBar WHERE vcLinkName='PublicLeads'

--update PageNavigationDTL set bitVisible=0 where vcPageNavName like '%My Leads%'
--update PageNavigationDTL set bitVisible=0 where vcPageNavName like '%WebLeads%'
--update PageNavigationDTL set bitVisible=0 where vcPageNavName like '%Public Leads%'

------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------Shipping Service/Signature Type IN SO Grid------------------------------------------------------------------------------------
--BEGIN TRY
--BEGIN TRANSACTION

--------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------Shipping Service/Signature Type------------------------------------------------------------------------------------
--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numFieldID NUMERIC(18,0)

--	INSERT INTO DycFieldMaster
--	(
--		numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,tintRow,tintColumn,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,bitAllowSorting,bitAllowFiltering
--	)
--	VALUES
--	(
--		3,'Shipping Service','numDefaultShippingServiceID','numDefaultShippingServiceID','ShippingService','OpportunityMaster','N','R','SelectBox','',0,1,1,1,0,0,0,1,1,0,1
--	)

--	SET @numFieldID = SCOPE_IDENTITY()

--	INSERT INTO DycFormField_Mapping
--	(
--		numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--	)
--	VALUES
--	(
--		3,@numFieldID,39,0,0,'Shipping Service','SelectBox','ShippingService',1,1,1,0,0,1,0,1,0,1
--	)
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numFieldID NUMERIC(18,0)

--	INSERT INTO DycFieldMaster
--	(
--		numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,tintRow,tintColumn,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,bitAllowSorting,bitAllowFiltering
--	)
--	VALUES
--	(
--		3,'Signature Type','vcSignatureType','vcSignatureType','SignatureType','OpportunityMaster','N','R','SelectBox','',0,1,1,1,0,0,0,1,1,0,1
--	)

--	SET @numFieldID = SCOPE_IDENTITY()

--	INSERT INTO DycFormField_Mapping
--	(
--		numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--	)
--	VALUES
--	(
--		3,@numFieldID,39,0,0,'Signature Type','SelectBox','SignatureType',1,1,1,0,0,1,0,1,0,1
--	)
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH
--------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------CREATE TABLE FOR PO Fulfillment Settings------------------------------------------------------------------------------------
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[PurchaseOrderFulfillmentSettings](
--	[numPOFulfillmentID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[bitPartialQtyOrderStatus] [bit] NULL,
--	[numPartialReceiveOrderStatus] [numeric](18, 0) NULL,
--	[bitAllQtyOrderStatus] [bit] NULL,
--	[numFullReceiveOrderStatus] [numeric](18, 0) NULL,
--	[bitBizDoc] [bit] NULL,
--	[numBizDocID] [numeric](18, 0) NULL,
--	[bitClosePO] [bit] NULL,
--	[numCreatedBY] [numeric](18, 0) NULL,
--	[bintCreatedDate] [datetime] NULL,
--	[numModifiedBy] [numeric](18, 0) NULL,
--	[bintModifiedDate] [datetime] NULL,
-- CONSTRAINT [PK_PurchaseOrderFulfillmentSettings] PRIMARY KEY CLUSTERED 
--(
--	[numPOFulfillmentID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]
--GO

--ALTER TABLE [dbo].[PurchaseOrderFulfillmentSettings]  WITH CHECK ADD  CONSTRAINT [FK_PurchaseOrderFulfillmentSettings_PurchaseOrderFulfillmentSettings] FOREIGN KEY([numDomainID])
--REFERENCES [dbo].[Domain] ([numDomainId])
--GO

--ALTER TABLE [dbo].[PurchaseOrderFulfillmentSettings] CHECK CONSTRAINT [FK_PurchaseOrderFulfillmentSettings_PurchaseOrderFulfillmentSettings]
--GO

------------------------------------------ADD NEW BizDocType-'Vendor Invoice'------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------
--INSERT INTO ListDetails
-- (numListID,vcData,numCreatedBY,bintCreatedDate,bitDelete,numDomainID,constFlag,sintOrder)
--VALUES
-- (27,'Vendor Invoice',17,GETDATE(),0,72,1,1)

--DECLARE @numBizDoc NUMERIC(18)
--SET @numBizDoc = SCOPE_IDENTITY()

--INSERT INTO BizDocFilter
-- (numBizDoc,tintBizocType,numDomainID)
--VALUES
-- (@numBizDoc,1,72)
------------------------------------------------------------------------------------------------------------------------------


--ALTER table PromotionOfferOrderBased ADD bitEnabled bit
--/*********************************************************************Admin - Price Rules************************************************************************************************/
--declare @numPageNavID numeric(18), @numPMPageNavID numeric(18),@numParentID numeric(18)

--select @numPageNavID=numPageNavID from PageNavigationDTL where vcPageNavName like '%Price & Cost Rules%'

--update PageNavigationDTL
--set vcPageNavName='Price Rules', vcImageUrl='~/images/PriceRules.png' 
--where numPageNavID = @numPageNavID
----where vcPageNavName like '%Price Rules%'

--select @numPMPageNavID=numPageNavID from PageNavigationDTL where vcPageNavName like '%Promotion management%'
--select @numParentID=numPageNavID from PageNavigationDTL where vcPageNavName = 'Administration'

--update PageNavigationDTL
--set vcImageUrl='~/images/Admin-Price-Control.png',numParentID=@numParentID
--where numPageNavID = @numParentID

----where vcPageNavName like '%Promotion management%'
--/*update PageNavigationDTL
--set vcPageNavName='Price Rules', vcImageUrl='~/images/PriceRules.png' 
--where vcPageNavName like '%Price Rules%'

--update PageNavigationDTL
--set vcImageUrl='~/images/Admin-Price-Control.png',numParentID=35
--where vcPageNavName like '%Promotion management%'*/
--/*********************************************************************************************************************************************************************/

--INSERT INTO DycFieldMaster
--(numModuleID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, vcFieldType,
--vcAssociatedControlType, numListID, [order], bitInResults, tintRow, tintColumn,
--bitDeleted, bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, 
--bitAllowFiltering)
--VALUES
--(3,'Customer PO#','vcCustomerPO#','vcCustomerPO#','CustomerPO','OpportunityMaster','V','R',
--'TextBox',0,48,1,1,1,
--0,1,0,1,1,1,1,1)

--DECLARE @numFieldID NUMERIC(18,0)
--SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcFieldName='Customer PO#' AND numModuleID = 3 AND vcOrigDbColumnName='CustomerPO#' 
--AND vcLookBackTableName='OpportunityMaster'

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID, numFieldID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, [order],tintRow,tintColumn,
--	bitInResults, bitDeleted, bitDefault, bitSettingField,  
--	bitAddField, bitDetailField, bitAllowSorting, bitAllowFiltering,numFormFieldID 
--)
--VALUES
--(
--	3, @numFieldID, 40, 1, 1, 'Customer PO#', 'TextBox', 'CustomerPO',48,1,1,
--	1, 0, 0, 1, 
--	1, 1,0,0,12149
--)

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID, numFieldID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, [order],tintRow,tintColumn,
--	bitInResults, bitDeleted, bitDefault, bitSettingField,  
--	bitAddField, bitDetailField, bitAllowSorting, bitAllowFiltering,numFormFieldID 
--)
--VALUES
--(
--	3, @numFieldID, 41, 1, 1, 'Customer PO#', 'TextBox', 'CustomerPO',48,1,1,
--	1, 0, 0, 1, 
--	1, 1,0,0,12150
--)
/*********************************************************************Order Based Promotion Offer************************************************************************************************/
--ALTER TABLE PromotionOffer ADD IsOrderBasedPromotion BIT
--ALTER TABLE PromotionOffer ADD SaleType CHAR
--ALTER TABLE PromotionOfferItems ADD SubTotal FLOAT

/*********************************************************************PRICE - MARGIN************************************************************************************************/
/*
INSERT INTO ApprovalProcessItemsClassification (numDomainID, numAbovePercent, numBelowPercent, 
	bitCostApproval, bitListPriceApproval, bitMarginPriceViolated)
SELECT numDomainID, numAbovePercent, numBelowPercent,  bitCostApproval, bitListPriceApproval, bitMarginPriceViolated
FROM Domain
*/

/*********************************************************************INVOICING************************************************************************************************/
--ALTER TABLE Domain DROP COLUMN [numAbovePercent]
--ALTER TABLE Domain DROP COLUMN [numBelowPercent]
--ALTER TABLE Domain DROP COLUMN [numBelowPriceField]
--ALTER TABLE Domain DROP COLUMN [bitCostApproval]
--ALTER TABLE Domain DROP COLUMN [bitListPriceApproval]
/*Have to confirm with Sandeep
--ALTER TABLE Domain DROP COLUMN [bitMarginPriceViolated]
				
/*********************************************************************ITEM-ATTRIBUTES************************************************************************************************/*/

--ALTER TABLE cfw_fld_master ADD bitAutoComplete bit
/*
IF NOT EXISTS(SELECT vcListName FROM ListMaster WHERE numModuleID = 8 AND numDomainID = 72 AND vcListName = 'Size')
BEGIN
	INSERT INTO [ListMaster]
		(vcListName, numCreatedBy, bintCreatedDate, bitDeleted, bitFixed, numDomainID, bitFlag, numModuleID)
	VALUES
		('Size', 1, GETDATE(), 0, 0, 72, 0, 8)

	INSERT INTO dbo.ListDetails  
	(vcdata,numlistid,numCreatedBy,bitdelete,numdomainid,constFlag,sintOrder,numlistType)
 
    SELECT distinct [Size],SCOPE_IDENTITY(),1,0,72,0,1,0 
    FROM autocomplete
END

IF NOT EXISTS(SELECT vcListName FROM ListMaster WHERE numModuleID = 8 AND numDomainID = 72 AND vcListName = 'ID Inch')
BEGIN
	INSERT INTO [ListMaster]
		(vcListName, numCreatedBy, bintCreatedDate, bitDeleted, bitFixed, numDomainID, bitFlag, numModuleID)
	VALUES
		('ID Inch', 1, GETDATE(), 0, 0, 72, 0, 8)

	INSERT INTO dbo.ListDetails  
	(vcdata,numlistid,numCreatedBy,bitdelete,numdomainid,constFlag,sintOrder,numlistType)
 
    SELECT distinct [ID Inch],SCOPE_IDENTITY(),1,0,72,0,1,0 
    FROM autocomplete
END

IF NOT EXISTS(SELECT vcListName FROM ListMaster WHERE numModuleID = 8 AND numDomainID = 72 AND vcListName = 'CS inch')
BEGIN
	INSERT INTO [ListMaster]
		(vcListName, numCreatedBy, bintCreatedDate, bitDeleted, bitFixed, numDomainID, bitFlag, numModuleID)
	VALUES
		('CS inch', 1, GETDATE(), 0, 0, 72, 0, 8)

	INSERT INTO dbo.ListDetails  
	(vcdata,numlistid,numCreatedBy,bitdelete,numdomainid,constFlag,sintOrder,numlistType)
 
    SELECT distinct [CS inch],SCOPE_IDENTITY(),1,0,72,0,1,0 
    FROM autocomplete
END


IF NOT EXISTS(SELECT vcListName FROM ListMaster WHERE numModuleID = 8 AND numDomainID = 72 AND vcListName = 'ID mm')
BEGIN
	INSERT INTO [ListMaster]
		(vcListName, numCreatedBy, bintCreatedDate, bitDeleted, bitFixed, numDomainID, bitFlag, numModuleID)
	VALUES
		('ID mm', 1, GETDATE(), 0, 0, 72, 0, 8)

	INSERT INTO dbo.ListDetails  
	(vcdata,numlistid,numCreatedBy,bitdelete,numdomainid,constFlag,sintOrder,numlistType)
 
    SELECT distinct [ID mm],SCOPE_IDENTITY(),1,0,72,0,1,0 
    FROM autocomplete
END

IF NOT EXISTS(SELECT vcListName FROM ListMaster WHERE numModuleID = 8 AND numDomainID = 72 AND vcListName = 'CS mm')
BEGIN
	INSERT INTO [ListMaster]
		(vcListName, numCreatedBy, bintCreatedDate, bitDeleted, bitFixed, numDomainID, bitFlag, numModuleID)
	VALUES
		('CS mm', 1, GETDATE(), 0, 0, 72, 0, 8)

	INSERT INTO dbo.ListDetails  
	(vcdata,numlistid,numCreatedBy,bitdelete,numdomainid,constFlag,sintOrder,numlistType)
 
    SELECT distinct [CS mm],SCOPE_IDENTITY(),1,0,72,0,1,0 
    FROM autocomplete
END

IF NOT EXISTS(SELECT vcListName FROM ListMaster WHERE numModuleID = 8 AND numDomainID = 72 AND vcListName = 'Material')
BEGIN
	INSERT INTO [ListMaster]
		(vcListName, numCreatedBy, bintCreatedDate, bitDeleted, bitFixed, numDomainID, bitFlag, numModuleID)
	VALUES
		('Material', 1, GETDATE(), 0, 0, 72, 0, 8)
END

IF NOT EXISTS(SELECT vcListName FROM ListMaster WHERE numModuleID = 8 AND numDomainID = 72 AND vcListName = 'Durometer')
BEGIN
	INSERT INTO [ListMaster]
		(vcListName, numCreatedBy, bintCreatedDate, bitDeleted, bitFixed, numDomainID, bitFlag, numModuleID)
	VALUES
		('Durometer', 1, GETDATE(), 0, 0, 72, 0, 8)
END

IF NOT EXISTS(SELECT vcListName FROM ListMaster WHERE numModuleID = 8 AND numDomainID = 72 AND vcListName = 'Color')
BEGIN
	INSERT INTO [ListMaster]
		(vcListName, numCreatedBy, bintCreatedDate, bitDeleted, bitFixed, numDomainID, bitFlag, numModuleID)
	VALUES
		('Color', 1, GETDATE(), 0, 0, 72, 0, 8)
END

IF NOT EXISTS(SELECT vcListName FROM ListMaster WHERE numModuleID = 8 AND numDomainID = 72 AND vcListName = 'Series')
BEGIN
	INSERT INTO [ListMaster]
		(vcListName, numCreatedBy, bintCreatedDate, bitDeleted, bitFixed, numDomainID, bitFlag, numModuleID)
	VALUES
		('Series', 1, GETDATE(), 0, 0, 72, 0, 8)
END
--------------------------------------------------------------------------------------------------------------------------
CREATE TABLE [dbo].[AutoComplete](
	[Size] [nvarchar](10) NULL,
	[ID Inch] [float] NULL,
	[CS Inch] [float] NULL,
	[ID mm] [float] NULL,
	[CS mm] [float] NULL
) ON [PRIMARY]
GO
*/
/*********************************************************************INVOICING************************************************************************************************/

--ALTER TABLE OpportunityBizDocs ADD bitIsEmailSent bit

--INSERT INTO DycFieldMaster
--(numModuleID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, vcFieldType,
--vcAssociatedControlType, numListID, [order], bitInResults, bitDeleted, bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, 
--bitWorkFlowField, bitAllowFiltering)
--VALUES
--(3,'Invoice(s)','tintInvoicing','tintInvoicing','Invoicing','OpportunityMaster','N','R',
--'SelectBox',0,50,1,0,0,0,1,0,0,0,0,1)

--DECLARE @numFieldID NUMERIC(18,0)
--SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcFieldName='Invoice(s)' AND numModuleID = 3 AND vcOrigDbColumnName='tintInvoicing' AND vcLookBackTableName='OpportunityMaster'
--INSERT INTO DycFormField_Mapping
--(
--	numModuleID, numFieldID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, bitInResults, bitDeleted, bitDefault, bitSettingField,  
--	bitAddField, bitDetailField, bitAllowSorting, bitAllowFiltering
--)
--VALUES
--(
--	3, @numFieldID, 39, 0, 0, 'Invoice(s)', 'SelectBox', 1, 0, 0, 1, 0, 0,0,1
--)



/*********************************************************************SHIPPING-PROMOTIONS************************************************************************************************/
--ALTER TABLE PromotionOffer DROP COLUMN [bitFreeShiping]
--ALTER TABLE PromotionOffer DROP COLUMN [monFreeShippingOrderAmount]
--ALTER TABLE PromotionOffer DROP COLUMN [numFreeShippingCountry]
--ALTER TABLE PromotionOffer DROP COLUMN [bitFixShipping1]
--ALTER TABLE PromotionOffer DROP COLUMN [monFixShipping1OrderAmount]
--ALTER TABLE PromotionOffer DROP COLUMN [monFixShipping1Charge]
--ALTER TABLE PromotionOffer DROP COLUMN [bitFixShipping2]
--ALTER TABLE PromotionOffer DROP COLUMN [monFixShipping2OrderAmount]
--ALTER TABLE PromotionOffer DROP COLUMN [monFixShipping2Charge]

--ALTER TABLE SimilarItems ADD bitRequired bit

--ALTER TABLE PromotionOffer ADD tintCustomersBasedOn TINYINT

--ALTER TABLE PromotionOffer ADD tintOfferTriggerValueTypeRange TINYINT
--ALTER TABLE PromotionOffer ADD fltOfferTriggerValueRange FLOAT



--ALTER TABLE PromotionOffer ADD tintOfferTriggerValueType TINYINT
--ALTER TABLE PromotionOffer ADD fltOfferTriggerValue FLOAT
--ALTER TABLE PromotionOffer ADD tintOfferBasedOn TINYINT
--ALTER TABLE PromotionOffer ADD tintDiscountType TINYINT
--ALTER TABLE PromotionOffer ADD fltDiscountValue FLOAT

--ALTER TABLE PromotionOffer ADD tintDiscoutBaseOn TINYINT
--ALTER TABLE PromotionOffer ADD tintDiscountType TINYINT

--/************************************************SHIPPING PROMOTIONS***************************************************************************************************/
--IF NOT EXISTS(SELECT vcPageNavName = 'Shipping Promotions' FROM [dbo].[PageNavigationDTL] )
--BEGIN
--	INSERT INTO PageNavigationDTL 
--		(numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID)
--	VALUES
--		(262,13,237,'Shipping Promotions','../ECommerce/frmShippingCriteria.aspx',1,-1)
--END
--ELSE
--	PRINT 'Already Exists Shipping Promotions'



--IF NOT EXISTS(SELECT numPageNavId = 262 FROM [dbo].[TreeNavigationAuthorization] )
--BEGIN
--	INSERT INTO TreeNavigationAuthorization 
--		(numGroupid,numTabID,numPageNavId,bitvisible,numdomainid,tinttype)
--	VALUES
--		(102,-1,262,1,72,1)
--END
--ELSE
--	PRINT 'Already Exists numPageNavId'

--/************************************************SHIPPING PROMOTIONS***************************************************************************************************/

--IF NOT EXISTS(SELECT vcCustomLookBackTableName = 'CFW_Fld_Values_OppItems' FROM [dbo].[CFW_Loc_Master] )
--BEGIN
--	INSERT INTO CFW_Loc_Master VALUES('Opportunity Item Details','O','CFW_Fld_Values_OppItems')
--END
--ELSE
--	PRINT 'Already Exists CFW_Fld_Values_OppItems'


------------------------------------------------------
------------- ADD NEW ALTER SCRIPTS ABOVE ------------
------------------------------------------------------

--ALTER TABLE CartItems ADD numParentItem NUMERIC(18,0)


--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[CFW_Fld_Values_OppItems]    Script Date: 11-Dec-17 9:36:58 AM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[CFW_Fld_Values_OppItems](
--	[FldDTLID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[Fld_ID] [numeric](18, 0) NULL,
--	[Fld_Value] [varchar](max) NULL,
--	[RecId] [numeric](18, 0) NULL,
-- CONSTRAINT [PK_CFW_Fld_Values_OppItems] PRIMARY KEY CLUSTERED 
--(
--	[FldDTLID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
--) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO

--ALTER TABLE [dbo].[CFW_Fld_Values_OppItems]  WITH CHECK ADD  CONSTRAINT [FK_CFW_Fld_Values_OppItems_CFW_Fld_Master] FOREIGN KEY([Fld_ID])
--REFERENCES [dbo].[CFW_Fld_Master] ([Fld_id])
--GO

--ALTER TABLE [dbo].[CFW_Fld_Values_OppItems] CHECK CONSTRAINT [FK_CFW_Fld_Values_OppItems_CFW_Fld_Master]
--GO