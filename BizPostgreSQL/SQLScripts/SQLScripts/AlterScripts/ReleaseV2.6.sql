/******************************************************************
Project: Release 2.6 Date: 04.01.2014
Comments: 
*******************************************************************/

/*******************Kamal Script******************/
--/************************************************************************************************/
--/************************************************************************************************/

BEGIN TRANSACTION
	DECLARE @numPageNavID numeric 
	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
	INSERT INTO dbo.PageNavigationDTL
			( numPageNavID ,
			  numModuleID ,
			  numParentID ,
			  vcPageNavName ,
			  vcNavURL ,
			  vcImageURL ,
			  bitVisible ,
			  numTabID
			)
	SELECT @numPageNavID ,35,93,'Other Reports',NULL,NULL,1,45 
	EXEC USP_CreateTreeNavigationForDomain 0,@numPageNavID --Give permission to tree node.
END TRANSACTION

BEGIN TRANSACTION
	DECLARE @numPageNavID numeric 
	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
	INSERT INTO dbo.PageNavigationDTL
			( numPageNavID ,
			  numModuleID ,
			  numParentID ,
			  vcPageNavName ,
			  vcNavURL ,
			  vcImageURL ,
			  bitVisible ,
			  numTabID
			)
	SELECT @numPageNavID ,35,222,'Check Register','../Accounting/frmAccountsReports.aspx?RType=1',NULL,1,45 
	EXEC USP_CreateTreeNavigationForDomain 0,@numPageNavID --Give permission to tree node.
	
	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
	INSERT INTO dbo.PageNavigationDTL
			( numPageNavID ,
			  numModuleID ,
			  numParentID ,
			  vcPageNavName ,
			  vcNavURL ,
			  vcImageURL ,
			  bitVisible ,
			  numTabID
			)
	SELECT @numPageNavID ,35,222,'Sales Journal Detail (By GL Account)','../Accounting/frmAccountsReports.aspx?RType=2',NULL,1,45 
	EXEC USP_CreateTreeNavigationForDomain 0,@numPageNavID --Give permission to tree node.
	
	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
	INSERT INTO dbo.PageNavigationDTL
			( numPageNavID ,
			  numModuleID ,
			  numParentID ,
			  vcPageNavName ,
			  vcNavURL ,
			  vcImageURL ,
			  bitVisible ,
			  numTabID
			)
	SELECT @numPageNavID ,35,222,'Purchase Journal Detail (By GL Account)','../Accounting/frmAccountsReports.aspx?RType=3',NULL,1,45 
	EXEC USP_CreateTreeNavigationForDomain 0,@numPageNavID --Give permission to tree node.
	
	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
	INSERT INTO dbo.PageNavigationDTL
			( numPageNavID ,
			  numModuleID ,
			  numParentID ,
			  vcPageNavName ,
			  vcNavURL ,
			  vcImageURL ,
			  bitVisible ,
			  numTabID
			)
	SELECT @numPageNavID ,35,222,'Invoice Register','../Accounting/frmAccountsReports.aspx?RType=4',NULL,1,45 
	EXEC USP_CreateTreeNavigationForDomain 0,@numPageNavID --Give permission to tree node.
	
	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
	INSERT INTO dbo.PageNavigationDTL
			( numPageNavID ,
			  numModuleID ,
			  numParentID ,
			  vcPageNavName ,
			  vcNavURL ,
			  vcImageURL ,
			  bitVisible ,
			  numTabID
			)
	SELECT @numPageNavID ,35,222,'Receipt Journal (Summary)','../Accounting/frmAccountsReports.aspx?RType=5',NULL,1,45 
	EXEC USP_CreateTreeNavigationForDomain 0,@numPageNavID --Give permission to tree node.
	
	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
	INSERT INTO dbo.PageNavigationDTL
			( numPageNavID ,
			  numModuleID ,
			  numParentID ,
			  vcPageNavName ,
			  vcNavURL ,
			  vcImageURL ,
			  bitVisible ,
			  numTabID
			)
	SELECT @numPageNavID ,35,222,'Receipt Journal (Detailed)','../Accounting/frmAccountsReports.aspx?RType=6',NULL,1,45 
	EXEC USP_CreateTreeNavigationForDomain 0,@numPageNavID --Give permission to tree node.
	
	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
	INSERT INTO dbo.PageNavigationDTL
			( numPageNavID ,
			  numModuleID ,
			  numParentID ,
			  vcPageNavName ,
			  vcNavURL ,
			  vcImageURL ,
			  bitVisible ,
			  numTabID
			)
	SELECT @numPageNavID ,35,222,'Total Disbursement Journal (Summary)','../Accounting/frmAccountsReports.aspx?RType=7',NULL,1,45 
	EXEC USP_CreateTreeNavigationForDomain 0,@numPageNavID --Give permission to tree node.
	
	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
	INSERT INTO dbo.PageNavigationDTL
			( numPageNavID ,
			  numModuleID ,
			  numParentID ,
			  vcPageNavName ,
			  vcNavURL ,
			  vcImageURL ,
			  bitVisible ,
			  numTabID
			)
	SELECT @numPageNavID ,35,222,'Total Disbursement Journal (Detailed)','../Accounting/frmAccountsReports.aspx?RType=8',NULL,1,45 
	EXEC USP_CreateTreeNavigationForDomain 0,@numPageNavID --Give permission to tree node.
END TRANSACTION


/*******************Manish Script******************/
--/************************************************************************************************/
--/************************************************************************************************/



/*******************Sachin Script******************/
--/************************************************************************************************/
--/************************************************************************************************/

CREATE TABLE [dbo].[InboxTreeSort](
	[numNodeID] [int] IDENTITY(1,1) NOT NULL,
	[numParentID] [int] NULL,
	[vcNodeName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[numDomainID] [int] NULL,
	[numUserCntID] [int] NULL,
	[numSortOrder] [int] NULL,
 CONSTRAINT [PK_InboxTreeSort] PRIMARY KEY CLUSTERED 
(
	[numNodeID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

*----------------------------ON Second Update:4thJan2014-----------------------------*
ALTER TABLE dbo.BizDocTemplate ADD
	vcBizDocImagePath varchar(100) NULL,
	vcBizDocFooter varchar(100) NULL,
	vcPurBizDocFooter varchar(100) NULL
	
INSERT INTO CFw_Grp_Master
                      (Grp_Name, Loc_Id, numDomainID, tintType, vcURLF)
VALUES     ('Bought & Sold',15,1,2,NULL)

INSERT INTO CFw_Grp_Master
                      (Grp_Name, Loc_Id, numDomainID, tintType, vcURLF)
VALUES     ('Vended',15,1,2,NULL)

INSERT INTO CFW_Loc_Master
                      (Loc_name, vcFieldType, vcCustomLookBackTableName)
VALUES     ('Items','D','CFW_FLD_Values')
