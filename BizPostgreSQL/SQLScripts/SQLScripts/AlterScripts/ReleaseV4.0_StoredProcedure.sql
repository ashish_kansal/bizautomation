
/******************************************************************
Project: Release 4.0 Date: 08.DEC.2014
Comments: STORED PROCEDURES
*******************************************************************/

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_BizFormWizardModule_GetAll' ) 
    DROP PROCEDURE USP_BizFormWizardModule_GetAll
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 21 July 2014
-- Description:	Gets list of bizform wizard modules
-- =============================================
CREATE PROCEDURE USP_BizFormWizardModule_GetAll
	@numDomainID NUMERIC(18,0)
AS
BEGIN
	
	SET NOCOUNT ON;

	IF (SELECT COUNT(*) FROM ListDetails WHERE numListID = 5 AND numDomainID = @numDomainID AND constFlag <> 1) > 0
		SELECT * FROM BizFormWizardModule
	ELSE
		SELECT * FROM BizFormWizardModule --WHERE numBizFormModuleID <> 7	
END
GO
/****** Object:  StoredProcedure [dbo].[USP_EcommerceSettings]    Script Date: 03/25/2009 16:46:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ecommercesettings')
DROP PROCEDURE usp_ecommercesettings
GO
CREATE PROCEDURE [dbo].[USP_EcommerceSettings]              
@numDomainID as numeric(9),              
@vcPaymentGateWay as int ,                
@vcPGWUserId as varchar(100)='' ,                
@vcPGWPassword as varchar(100),              
@bitShowInStock as bit,              
@bitShowQOnHand as bit,        
--@tintColumns as tinyint,      
--@numCategory as numeric(9),
--@bitHideNewUsers as bit,
@bitCheckCreditStatus as BIT,
@numDefaultWareHouseID as numeric(9),
@numDefaultIncomeChartAcntId as numeric(9),
@numDefaultAssetChartAcntId as numeric(9),
@numDefaultCOGSChartAcntId as numeric(9),
@numRelationshipId as numeric(9),
@numProfileId as numeric(9),
@bitEnableDefaultAccounts BIT,
--@bitEnableCreditCart	BIT,
@bitHidePriceBeforeLogin	BIT,
@bitHidePriceAfterLogin	BIT,
@numRelationshipIdHidePrice	numeric(18, 0),
@numProfileIDHidePrice	numeric(18, 0),
--@numBizDocForCreditCard numeric(18, 0),
--@numBizDocForCreditTerm numeric(18, 0),
@bitAuthOnlyCreditCard BIT=0,
@bitSendMail BIT = 1,
@vcGoogleMerchantID VARCHAR(1000)  = '',
@vcGoogleMerchantKey VARCHAR(1000) = '',
@IsSandbox BIT ,
@numAuthrizedOrderStatus NUMERIC,
@numSiteID numeric(18, 0),
@vcPaypalUserName VARCHAR(50)  = '',
@vcPaypalPassword VARCHAR(50)  = '',
@vcPaypalSignature VARCHAR(500)  = '',
@IsPaypalSandbox BIT,
@bitSkipStep2 BIT = 0,
@bitDisplayCategory BIT = 0,
@bitShowPriceUsingPriceLevel BIT = 0,
@bitEnableSecSorting BIT = 0,
@bitSortPriceMode   BIT=0,
@numPageSize    INT=15,
@numPageVariant  INT=6,
@bitAutoSelectWarehouse BIT =0,
@bitPreSellUp BIT = 0, 
@bitPostSellUp BIT = 0, 
@dcPostSellDiscount DECIMAL  = 0
as              
            
            
if not exists(select 'col1' from eCommerceDTL where numDomainID=@numDomainID AND [numSiteId] = @numSiteID)            
INSERT INTO eCommerceDTL
           (numDomainId,
            vcPaymentGateWay,
            vcPGWManagerUId,
            vcPGWManagerPassword,
            bitShowInStock,
            bitShowQOnHand,
            --tintItemColumns,
           -- numDefaultCategory,
            numDefaultWareHouseID,
            numDefaultIncomeChartAcntId,
            numDefaultAssetChartAcntId,
            numDefaultCOGSChartAcntId,
            --bitHideNewUsers,
            bitCheckCreditStatus,
            [numRelationshipId],
            [numProfileId],
            [bitEnableDefaultAccounts],
 --           [bitEnableCreditCart],
            [bitHidePriceBeforeLogin],
            [bitHidePriceAfterLogin],
            [numRelationshipIdHidePrice],
            [numProfileIDHidePrice],
--            numBizDocForCreditCard,
--            numBizDocForCreditTerm,
            bitAuthOnlyCreditCard,
--            numAuthrizedOrderStatus,
            bitSendEmail,
            vcGoogleMerchantID,
            vcGoogleMerchantKey,
            IsSandbox ,
            numAuthrizedOrderStatus,
			numSiteId,
			vcPaypalUserName,
			vcPaypalPassword,
			vcPaypalSignature,
			IsPaypalSandbox,
			bitSkipStep2,
			bitDisplayCategory,
			bitShowPriceUsingPriceLevel,
			bitEnableSecSorting,
			bitSortPriceMode,
			numPageSize,
			numPageVariant,
			bitAutoSelectWarehouse,
			[bitPreSellUp],
			[bitPostSellUp],
			[dcPostSellDiscount]
			)

VALUES     (@numDomainID,
            @vcPaymentGateWay,
            @vcPGWUserId,
            @vcPGWPassword,
            @bitShowInStock,
            @bitShowQOnHand,
           -- @tintColumns,
           -- @numCategory,
            @numDefaultWareHouseID,
            @numDefaultIncomeChartAcntId,
            @numDefaultAssetChartAcntId,
            @numDefaultCOGSChartAcntId,
           -- @bitHideNewUsers,
            @bitCheckCreditStatus,
            @numRelationshipId,
            @numProfileId,
            @bitEnableDefaultAccounts,
 --           @bitEnableCreditCart,
            @bitHidePriceBeforeLogin,
            @bitHidePriceAfterLogin,
            @numRelationshipIdHidePrice,
            @numProfileIDHidePrice,
--            @numBizDocForCreditCard,
--            @numBizDocForCreditTerm,
            @bitAuthOnlyCreditCard,
--            @numAuthrizedOrderStatus,
            @bitSendMail,
            @vcGoogleMerchantID,
            @vcGoogleMerchantKey ,
            @IsSandbox,
            @numAuthrizedOrderStatus,
			@numSiteID,
			@vcPaypalUserName,
			@vcPaypalPassword,
			@vcPaypalSignature,
			@IsPaypalSandbox,
			@bitSkipStep2,
			@bitDisplayCategory,
			@bitShowPriceUsingPriceLevel,
			@bitEnableSecSorting,
			@bitSortPriceMode,
			@numPageSize,
			@numPageVariant,
			@bitAutoSelectWarehouse,
			@bitPreSellUp,
			@bitPostSellUp,
			@dcPostSellDiscount
            )
 else            
 update eCommerceDTL                                   
   set              
 vcPaymentGateWay=@vcPaymentGateWay,                
 vcPGWManagerUId=@vcPGWUserId ,                
 vcPGWManagerPassword= @vcPGWPassword,              
 bitShowInStock=@bitShowInStock ,               
 bitShowQOnHand=@bitShowQOnHand,        
 --tintItemColumns= @tintColumns,      
 --numDefaultCategory=@numCategory,
 numDefaultWareHouseID =@numDefaultWareHouseID,
 numDefaultIncomeChartAcntId =@numDefaultIncomeChartAcntId ,
 numDefaultAssetChartAcntId =@numDefaultAssetChartAcntId ,
 numDefaultCOGSChartAcntId =@numDefaultCOGSChartAcntId ,
 --bitHideNewUsers =@bitHideNewUsers,
 bitCheckCreditStatus=@bitCheckCreditStatus ,
 numRelationshipId = @numRelationshipId,
 numProfileId = @numProfileId,
 bitEnableDefaultAccounts = @bitEnableDefaultAccounts,
 --[bitEnableCreditCart]=@bitEnableCreditCart,
 [bitHidePriceBeforeLogin]=@bitHidePriceBeforeLogin,
 [bitHidePriceAfterLogin]=@bitHidePriceAfterLogin,
 [numRelationshipIdHidePrice]=@numRelationshipIdHidePrice,
 [numProfileIDHidePrice]=@numProfileIDHidePrice,
-- numBizDocForCreditCard=@numBizDocForCreditCard,
-- numBizDocForCreditTerm=@numBizDocForCreditTerm,
 bitAuthOnlyCreditCard=@bitAuthOnlyCreditCard,
-- numAuthrizedOrderStatus=@numAuthrizedOrderStatus,
 bitSendEmail = @bitSendMail,
 vcGoogleMerchantID = @vcGoogleMerchantID ,
 vcGoogleMerchantKey =  @vcGoogleMerchantKey ,
 IsSandbox = @IsSandbox,
 numAuthrizedOrderStatus = @numAuthrizedOrderStatus,
	numSiteId = @numSiteID,
vcPaypalUserName = @vcPaypalUserName ,
vcPaypalPassword = @vcPaypalPassword,
vcPaypalSignature = @vcPaypalSignature,
IsPaypalSandbox = @IsPaypalSandbox,
bitSkipStep2 = @bitSkipStep2,
bitDisplayCategory = @bitDisplayCategory,
bitShowPriceUsingPriceLevel = @bitShowPriceUsingPriceLevel,
bitEnableSecSorting = @bitEnableSecSorting,
bitSortPriceMode=@bitSortPriceMode,
numPageSize=@numPageSize,
numPageVariant=@numPageVariant,
bitAutoSelectWarehouse=@bitAutoSelectWarehouse,
[bitPreSellUp] = @bitPreSellUp,
[bitPostSellUp] = @bitPostSellUp,
[dcPostSellDiscount] = @dcPostSellDiscount
 where numDomainId=@numDomainID AND [numSiteId] = @numSiteID
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetECommerceDetails')
DROP PROCEDURE USP_GetECommerceDetails
GO
CREATE PROCEDURE [dbo].[USP_GetECommerceDetails]      
@numDomainID as numeric(9)=0,
@numSiteID AS INTEGER = 0  
as                              
select                               
isnull(vcPaymentGateWay,'0') as vcPaymentGateWay,                
isnull(vcPGWManagerUId,'')as vcPGWManagerUId ,                
isnull(vcPGWManagerPassword,'') as vcPGWManagerPassword,              
isnull(bitShowInStock,0) as bitShowInStock,              
isnull(bitShowQOnHand,0) as bitShowQOnHand,        
--isnull(tintItemColumns,0) as tintItemColumns,
--isnull(numDefaultCategory,0) as numDefaultCategory,  
isnull([numDefaultWareHouseID],0) numDefaultWareHouseID,
isnull([numDefaultIncomeChartAcntId],0) numDefaultIncomeChartAcntId,
isnull([numDefaultAssetChartAcntId],0) numDefaultAssetChartAcntId,
isnull([numDefaultCOGSChartAcntId],0) numDefaultCOGSChartAcntId,
--isnull(bitHideNewUsers,0) as bitHideNewUsers,
isnull(bitCheckCreditStatus,0) as bitCheckCreditStatus,
isnull([numRelationshipId],0) numRelationshipId,
isnull([numProfileId],0) numProfileId,
isnull(bitEnableDefaultAccounts,0) as bitEnableDefaultAccounts,
--ISNULL([bitEnableCreditCart],0) AS bitEnableCreditCart,
ISNULL([bitHidePriceBeforeLogin],0) AS bitHidePriceBeforeLogin,
ISNULL([bitHidePriceAfterLogin],0) AS bitHidePriceAfterLogin,
ISNULL([numRelationshipIdHidePrice],0) AS numRelationshipIdHidePrice,
ISNULL([numProfileIDHidePrice],0) AS numProfileIDHidePrice,
ISNULL([numBizDocForCreditCard],0) AS numBizDocForCreditCard,
ISNULL([numBizDocForCreditTerm],0) AS numBizDocForCreditTerm,
ISNULL(bitAuthOnlyCreditCard,0) AS bitAuthOnlyCreditCard,
ISNULL(numAuthrizedOrderStatus,0) AS numAuthrizedOrderStatus,
ISNULL(bitSendEmail,1) AS bitSendEmail,
ISNULL(vcGoogleMerchantID , '') AS vcGoogleMerchantID,
ISNULL(vcGoogleMerchantKey,'') AS vcGoogleMerchantKey,
ISNULL(IsSandbox,0) AS IsSandbox,
ISNULL(numSiteID,0) AS numSiteID,
ISNULL(vcPaypalUserName,'') AS vcPaypalUserName,
ISNULL(vcPaypalPassword,'') AS vcPaypalPassword,
ISNULL(vcPaypalSignature,'') AS vcPaypalSignature,
ISNULL(IsPaypalSandbox,0) AS IsPaypalSandbox,
ISNULL(bitSkipStep2,0) AS [bitSkipStep2],
ISNULL(bitDisplayCategory,0) AS [bitDisplayCategory],
ISNULL(bitShowPriceUsingPriceLevel,0) AS [bitShowPriceUsingPriceLevel],
ISNULL(bitEnableSecSorting,0) AS [bitEnableSecSorting],
ISNULL(bitSortPriceMode,0) AS [bitSortPriceMode],
ISNULL(numPageSize,0) AS [numPageSize],
ISNULL(numPageVariant,0) AS [numPageVariant],
ISNULL(bitAutoSelectWarehouse,0) AS bitAutoSelectWarehouse,
ISNULL([bitPreSellUp],0) AS [bitPreSellUp],
ISNULL([bitPostSellUp],0) AS [bitPostSellUp],
ISNULL([dcPostSellDiscount],0) AS [dcPostSellDiscount]
FROM eCommerceDTL 
WHERE numDomainID=@numDomainID
AND ([numSiteId] = @numSiteID OR @numSiteID = 0)
-- EXEC USP_GetGeneralLedger 72,89,'2008-10-05 15:13:47.873','2009-10-05 15:13:47.873','','','','','','','',''

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetGeneralLedger')
DROP PROCEDURE USP_GetGeneralLedger
GO
CREATE PROCEDURE [dbo].[USP_GetGeneralLedger]
    (
      @numDomainID INT,
      @vcAccountId VARCHAR(4000),
      @dtFromDate DATETIME,
      @dtToDate DATETIME,
      @vcTranType VARCHAR(50) = '',
      @varDescription VARCHAR(50) = '',
      @CompanyName VARCHAR(50) = '',
      @vcBizPayment VARCHAR(50) = '',
      @vcCheqNo VARCHAR(50) = '',
      @vcBizDocID VARCHAR(50) = '',
      @vcTranRef VARCHAR(50) = '',
      @vcTranDesc VARCHAR(50) = '',
      @numDivisionID NUMERIC(9)=0,
      @ClientTimeZoneOffset INT,  --Added by Chintan to enable calculation of date according to client machine
      @charReconFilter CHAR(1)='',
      @tintMode AS TINYINT=0,
      @numItemID AS NUMERIC(9)=0,
      @CurrentPage INT=0,
	  @PageSize INT=0,
      @TotRecs INT=0  OUTPUT,
	  @TransactionID NUMERIC(9)=0
    )
WITH Recompile -- Added by Chandan 26 Feb 2013 to avoid parameter sniffing
AS 
    BEGIN
    
		/*RollUp of Sub Accounts */
		SELECT  COA2.numAccountId INTO #Temp /*,COA1.vcAccountCode ,COA2.vcAccountCode*/
		FROM    dbo.Chart_Of_Accounts COA1
				INNER JOIN dbo.Chart_Of_Accounts COA2 
				ON COA1.numDomainId = COA2.numDomainId
				AND COA2.vcAccountCode LIKE COA1.vcAccountCode  + '%'
		WHERE   COA1.numAccountId IN(SELECT  cast(ID AS NUMERIC(9)) FROM dbo.SplitIDs(@vcAccountId, ','))
				AND COA1.numDomainId = @numDomainID
		SELECT @vcAccountId = ISNULL( STUFF((SELECT ',' + CAST(numAccountID AS VARCHAR(10)) FROM #Temp FOR XML PATH('')),1, 1, '') , '') 
    
    	SELECT @numDomainID AS numDomainID,dbo.[FormatedDateFromDate](dtAsOnDate,@numDomainID) AS Date,* FROM dbo.fn_GetOpeningBalance(@vcAccountID,@numDomainID,@dtFromDate,@ClientTimeZoneOffset)
    	--following is commented as jeff demoed date GL report date issue in derek's account
--		SET @dtFromDate = DateAdd(minute, @ClientTimeZoneOffset, @dtFromDate);
--		SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate);

        SELECT  numDomainId,
                numAccountId,
                TransactionType,
                CompanyName,
                dbo.[FormatedDateFromDate](datEntry_Date,numDomainID) Date,
                varDescription,
                ISNULL(BizPayment,'') + ' ' + ISNULL(CheqNo,'') AS Narration,
                BizDocID,
                TranRef,
                TranDesc,
                CONVERT(VARCHAR(20),ISNULL(numDebitAmt,0)) numDebitAmt,
                CONVERT(VARCHAR(20),ISNULL(numCreditAmt,0)) numCreditAmt,
                vcAccountName,
                '' balance,
                numJournal_Id AS JournalId,
				numCheckHeaderID AS CheckId,
				numCashCreditCardId AS CashCreditCardId,
				numTransactionId,
				numOppId,
				numOppBizDocsId,
				numDepositId,
				numCategoryHDRID,
				tintTEType,
				numCategory,
				dtFromDate,
				numUserCntID,bitReconcile,bitCleared,numBillID,numBillPaymentID,numReturnID,
				STUFF((SELECT ', ' + CAST([COA].[vcAccountName] AS VARCHAR(4000))
				FROM [dbo].[General_Journal_Details] AS GJD JOIN [dbo].[Chart_Of_Accounts] AS COA ON GJD.[numChartAcntId] = COA.[numAccountId]
				WHERE [GJD].[numJournalId] = VIEW_GENERALLEDGER.numJournal_Id 
				AND [COA].[numDomainId] = VIEW_GENERALLEDGER.[numDomainId]
				AND [COA].[numAccountId] <> VIEW_GENERALLEDGER.numAccountId FOR XML PATH('')) ,1,2, '') AS [vcSplitAccountName],
				Row_number() over(order by datEntry_Date DESC) AS RowNumber INTO #tempTable
        FROM    VIEW_GENERALLEDGER
        WHERE   datEntry_Date BETWEEN  @dtFromDate AND @dtToDate -- Removed Tmezone bug id 1029 
        --instead of minus for datEntry_Date i have used it for From and To parameter .. DateAdd(minute, -@ClientTimeZoneOffset, datEntry_Date)
                AND (varDescription LIKE @varDescription + '%' OR LEN(@varDescription)=0)
                AND (BizPayment LIKE @vcBizPayment + '%' OR LEN(@vcBizPayment)=0)
                AND (CheqNo LIKE @vcCheqNo + '%' OR LEN(@vcCheqNo)=0)
                AND numDomainId = @numDomainID 
                AND (BizDocID LIKE @vcBizDocID OR LEN(@vcBizDocID)=0)
                AND (TranRef LIKE @vcTranRef OR LEN(@vcTranRef)=0)
                AND (TranDesc LIKE @vcTranDesc OR LEN(@vcTranDesc)=0)
                AND (TransactionType LIKE @vcTranType + '%' OR LEN(@vcTranType)=0)
                AND (bitCleared = CASE @charReconFilter WHEN 'C' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A')
                AND (bitReconcile = CASE @charReconFilter WHEN 'R' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A') 
                AND (CompanyName LIKE @CompanyName + '%' OR LEN(@CompanyName)=0)
                AND (numDivisionID = @numDivisionID OR @numDivisionID = 0)
                AND (numAccountId IN ( SELECT numAccountId FROM #Temp) OR  @tintMode=1)
                AND (numItemID = @numItemID OR @numItemID = 0)
                
         ORDER BY datEntry_Date ASC 
         
         IF @tintMode=1
         BEGIN
		  DECLARE  @firstRec  AS INTEGER
		  DECLARE  @lastRec  AS INTEGER

          SET @firstRec = (@CurrentPage - 1) * @PageSize
		  SET @lastRec = (@CurrentPage * @PageSize + 1)
		  SET @TotRecs = (SELECT COUNT(*) FROM   #tempTable)
         
          SELECT * FROM #tempTable WHERE RowNumber > @firstRec and RowNumber < @lastRec order by RowNumber
		 END
		 ELSE IF @tintMode=2
		 BEGIN
		 	SELECT * FROM #tempTable where numTransactionId = @TransactionID 
		 END
		ELSE
		 BEGIN
		 	SELECT * FROM #tempTable
		 END
         
		  DROP TABLE #tempTable
		  
         DROP TABLE #Temp
    END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOpenProjects')
DROP PROCEDURE USP_GetOpenProjects
GO
CREATE PROCEDURE USP_GetOpenProjects
    @numDomainID NUMERIC(9),
    @numProId NUMERIC(9),
    @tintMode TINYINT = 0,
    @numDivisionID NUMERIC(9)=0
AS 
    BEGIN
    
    IF @tintMode = 0 
    BEGIN
		IF @numProId > 0 
            BEGIN
                SELECT  numAccountID
                FROM    [ProjectsMaster]
                WHERE   [numProId] = @numProId
                        AND [numDomainId] = @numDomainID
            END
        ELSE 
            SELECT  [numProId],
                    [vcProjectName]
            FROM    [ProjectsMaster]
            WHERE   [numDomainId] = @numDomainID
                    AND [tintProStatus] <> 1
                    AND ([numDivisionId]=@numDivisionID OR @numDivisionID=0)
					AND isnull(numProjectStatus,0) <> 27492
            ORDER BY [vcProjectName],[numProId] desc
                    
	END
	ELSE IF @tintMode = 1
	BEGIN
		  SELECT  [numProId],
                    [vcProjectName]
            FROM    [ProjectsMaster]
            WHERE   [numDomainId] = @numDomainID
            ORDER BY [numProId] desc
            
                    
	END
        
    END 
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

--created by anoop jayaraj   
--EXEC [USP_GetPageNavigation] @numModuleID=9,@numDomainID=1

         
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getpagenavigation')
DROP PROCEDURE usp_getpagenavigation
GO
CREATE PROCEDURE [dbo].[USP_GetPageNavigation]            
@numModuleID as numeric(9),      
@numDomainID as numeric(9),
@numGroupID NUMERIC(18, 0)        
as            
if @numModuleID = 14      
begin
           
select  PND.numPageNavID,
        numModuleID,
        numParentID,
        vcPageNavName,
        isnull(vcNavURL, '') as vcNavURL,
        vcImageURL,
        0 intSortOrder
from    PageNavigationDTL PND
        JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
 WHERE  1 = 1
        AND ( ISNULL(TNA.bitVisible, 0) = 1
              --OR ISNULL(numParentID, 0) = 0
              --OR ISNULL(TNA.bitVisible, 0) = 0
            )
        AND ISNULL(PND.bitVisible, 0) = 1
        AND numModuleID = 14
        AND TNA.numDomainID = @numDomainID
        AND numGroupID = @numGroupID
union
select  1111 numPageNavID,
        14 numModuleID,
        133 numParentID,
        'Regular Documents' as vcPageNavName,
        '../Documents/frmRegularDocList.aspx?Category=0',
        '../images/tf_note.gif' vcImageURL,
        1
union

--select numListItemId,9 numModuleID,1111 numParentID,vcData vcPageNavName,'../Documents/frmDocList.aspx?TabID=1&Category=' + cast(numListItemId as varchar(10)) vcImageURL ,'../images/tf_note.gif',-2 vcImageURL
-- from ListDetails where numListid=29 and constFlag=1 
--UNION
--
--select numListItemId,9 numModuleID,1111 numParentID,vcData vcPageNavName,'../Documents/frmDocList.aspx?TabID=1&Category=' + cast(numListItemId as varchar(10)) vcImageURL ,'../images/tf_note.gif',-2 vcImageURL
-- from ListDetails where numListid=29 and numDomainID=@numDomainID
-- 
SELECT  Ld.numListItemId,
        14 numModuleID,
        1111 numParentID,
        vcData vcPageNavName,
        '../Documents/frmRegularDocList.aspx?Category='
        + cast(Ld.numListItemId as varchar(10)) vcImageURL,
        '../images/tf_note.gif',
        ISNULL(intSortOrder, LD.sintOrder) SortOrder
FROM    listdetails Ld
        left join listorder LO on Ld.numListItemID = LO.numListItemID
                                  and Lo.numDomainId = @numDomainID
WHERE   Ld.numListID = 29
        and ( constFlag = 1
              or Ld.numDomainID = @numDomainID
            )
--order by ISNULL(intSortOrder,LD.sintOrder)
--union
--   
-- SELECT 0 numPageNavID,9 numModuleID,1111 numParentID, vcData vcPageNavName,'../Documents/frmDocList.aspx?TabID=1&Category='+convert(varchar(10), LD.numListItemID) vcNavURL      
-- ,'../images/Text.gif' vcImageURL,0      
-- FROM listdetails Ld   
-- left join listorder LO on Ld.numListItemID= LO.numListItemID  and lo.numDomainId = @numDomainId        
-- WHERE  LD.numListID=28 and (constFlag=1 or  LD.numDomainID=@numDomainID)    
union
SELECT  ld.numListItemID AS numPageNavID,
        14 numModuleID,
        133 numParentID,
        vcData vcPageNavName,
        '../Documents/frmDocList.aspx?Category='
        + convert(varchar(10), LD.numListItemID) vcNavURL,
        '../images/tf_note.gif' vcImageURL,
        -3
FROM    listdetails Ld
        left join listorder LO on Ld.numListItemID = LO.numListItemID
                                  and lo.numDomainId = @numDomainId
        inner join AuthoritativeBizDocs AB on ( LD.numListItemID = AB.numAuthoritativeSales )
WHERE   LD.numListID = 27
        and ( constFlag = 1
              or LD.numDomainID = @numDomainID
            )
        and AB.numDomainID = @numDomainID
union
SELECT  ld.numListItemID AS numPageNavID,
        14 numModuleID,
        133 numParentID,
        vcData vcPageNavName,
        '../Documents/frmDocList.aspx?Category='
        + convert(varchar(10), LD.numListItemID) vcNavURL,
        '../images/tf_note.gif' vcImageURL,
        -3
FROM    listdetails Ld
        left join listorder LO on Ld.numListItemID = LO.numListItemID
                                  and lo.numDomainId = @numDomainId
        inner join AuthoritativeBizDocs AB on ( ld.numListItemID = AB.numAuthoritativePurchase )
WHERE   LD.numListID = 27
        and ( constFlag = 1
              or LD.numDomainID = @numDomainID
            )
        and AB.numDomainID = @numDomainID
UNION
select  0 numPageNavID,
        14 numModuleID,
        LD.numListItemID AS numParentID,
        LT.vcData as vcPageNavName,
        '../Documents/frmDocList.aspx?Category='
        + convert(varchar(10), Ld.numListItemID) + '&Status='
        + convert(varchar(10), LT.numListItemID) vcNavURL,
        '../images/Text.gif' vcImageURL,
        0
from    ListDetails LT,
        listdetails ld
where   LT.numListID = 11
        and Lt.numDomainID = @numDomainID
        AND LD.numListID = 27
        AND ( ld.constFlag = 1
              or LD.numDomainID = @numDomainID
            )
        and ( LD.numListItemID in ( select  AB.numAuthoritativeSales
                                    from    AuthoritativeBizDocs AB
                                    where   AB.numDomainId = @numDomainID )
              or ld.numListItemID in ( select   AB.numAuthoritativePurchase
                                       from     AuthoritativeBizDocs AB
                                       where    AB.numDomainId = @numDomainID )
            )


--SELECT * FROM AuthoritativeBizDocs where numDomainId=72

 
--UNION
--	SELECT 0 numPageNavID,9 numModuleID,133 numParentID, 'BizDoc Approval Rule' as  vcPageNavName,'../Documents/frmBizDocAppRule.aspx' as vcNavURL      
-- ,'../images/tf_note.gif' vcImageURL,0 
--
--UNION
--	SELECT 0 numPageNavID,9 numModuleID,133 numParentID, 'Order Auto Rule' as  vcPageNavName,'../Documents/frmOrderAutoRules.aspx' as vcNavURL      
-- ,'../images/tf_note.gif' vcImageURL,1  
union
select  2222 numPageNavID,
        14 numModuleID,
        133 numParentID,
        'Other BizDocs' as vcPageNavName,
        '',
        '../images/tf_note.gif' vcImageURL,
        -1
union
SELECT  ld.numListItemID AS numPageNavID,
        14 numModuleID,
        2222 numParentID,
        vcData vcPageNavName,
        '../Documents/frmDocList.aspx?Category='
        + convert(varchar(10), LD.numListItemID) vcNavURL,
        '../images/Text.gif' vcImageURL,
        -1
FROM    listdetails Ld
where   ld.numListID = 27
        and ( constFlag = 1
              or LD.numDomainID = @numDomainID
            )
        and LD.numListItemID not in ( select    AB.numAuthoritativeSales
                                      from      AuthoritativeBizDocs AB
                                      where     AB.numDomainID = @numDomainID )
        and LD.numListItemID not in ( select    AB.numAuthoritativePurchase
                                      from      AuthoritativeBizDocs AB
                                      where     AB.numDomainID = @numDomainID )
union
SELECT  ls.numListItemID AS numPageNavID,
        14 numModuleID,
        ld.numListItemID as numParentID,
        ls.vcData vcPageNavName,
        '../Documents/frmDocList.aspx?Category='
        + convert(varchar(10), LD.numListItemID) vcNavURL,
        null vcImageURL,
        0
FROM    listdetails Ld,
        ( select    *
          from      ListDetails LS
          where     LS.numDomainID = @numDomainID
                    and LS.numListID = 11
        ) LS
where   ld.numListID = 27
        and ( ld.constFlag = 1
              or LD.numDomainID = @numDomainID
            )
        and LD.numListItemID not in ( select    AB.numAuthoritativeSales
                                      from      AuthoritativeBizDocs AB
                                      where     AB.numDomainID = @numDomainID
                                      union
                                      select    AB.numAuthoritativePurchase
                                      from      AuthoritativeBizDocs AB
                                      where     AB.numDomainID = @numDomainID )
order by numParentID,
        intSortOrder,
        numPageNavID

END
ELSE IF @numModuleID = 35 
BEGIN
	select PND.numPageNavID,numModuleID,numParentID,vcPageNavName,isnull(vcNavURL,'') as vcNavURL,vcImageURL ,0 sintOrder      
	from PageNavigationDTL PND
	JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
	WHERE  1 = 1
        AND ( ISNULL(TNA.bitVisible, 0) = 1
              --OR ISNULL(numParentID, 0) = 0
              --OR ISNULL(TNA.bitVisible, 0) = 0
            ) 
        AND ISNULL(PND.bitVisible, 0) = 1
	AND numModuleID=@numModuleID --AND bitVisible=1  
	AND TNA.numDomainID = @numDomainID
	AND numGroupID = @numGroupID
	    
	UNION 
	 SELECT numFRID,35,CASE numFinancialViewID WHEN 26770 THEN 96 /*Profit & Loss*/
	WHEN 26769 THEN 176 /*Income & Expense*/
	WHEN 26768 THEN 98 /*Cash Flow Statement*/
	WHEN 26767 THEN 97 /*Balance Sheet*/
	WHEN 26766 THEN 81 /*A/R & A/P Aging*/
	WHEN 26766 THEN 82 /*A/R & A/P Aging*/
	ELSE 0
	END 
	  ,vcReportName,CASE numFinancialViewID 
	WHEN 26770 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =96 AND numModuleID=35)  /*Profit & Loss*/
	WHEN 26769 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =176 AND numModuleID=35) /*Income & Expense*/
	WHEN 26768 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =98 AND numModuleID=35) /*Cash Flow Statement*/
	WHEN 26767 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =97 AND numModuleID=35) /*Balance Sheet*/
	WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =81 AND numModuleID=35) /*A/R & A/P Aging*/
	WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =82 AND numModuleID=35) /*A/R & A/P Aging*/
	ELSE ''
	END + 
	CASE CHARINDEX('?',(CASE numFinancialViewID 
	WHEN 26770 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =96 AND numModuleID=35)  /*Profit & Loss*/
	WHEN 26769 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =176 AND numModuleID=35) /*Income & Expense*/
	WHEN 26768 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =98 AND numModuleID=35) /*Cash Flow Statement*/
	WHEN 26767 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =97 AND numModuleID=35) /*Balance Sheet*/
	WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =81 AND numModuleID=35) /*A/R & A/P Aging*/
	WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =82 AND numModuleID=35) /*A/R & A/P Aging*/
	ELSE ''
	END)) 
	WHEN 0 THEN '?FRID='
	ELSE '&FRID='
	END
	 + CAST(numFRID AS VARCHAR)
	 'url',NULL,0 FROM dbo.FinancialReport WHERE numDomainID=@numDomainID
	order by numParentID,numPageNavID  
END
-------------- ADDED BY MANISH ANJARA ON : 6th April,2012
ELSE IF @numModuleID = -1 AND @numDomainID = -1 
BEGIN 
	SELECT PND.numPageNavID,numModuleID,numParentID,vcPageNavName,isnull(vcNavURL,'') as vcNavURL,vcImageURL ,0 sintOrder      
	FROM PageNavigationDTL PND
	JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
	WHERE  1 = 1
        AND ( ISNULL(TNA.bitVisible, 0) = 1
              --OR ISNULL(numParentID, 0) = 0
              --OR ISNULL(TNA.bitVisible, 0) = 0
            )
        AND ISNULL(PND.bitVisible, 0) = 1
	--AND TNA.numDomainID = @numDomainID
	AND numGroupID = @numGroupID
	--WHERE bitVisible=1  
	ORDER BY numParentID,numPageNavID 
END
--------------------------------------------------------- 
-------------- ADDED BY MANISH ANJARA ON : 12th Oct,2012
ELSE IF @numModuleID = 10 
BEGIN 
	DECLARE @numParentID AS NUMERIC(18,0)
	SELECT TOP 1 @numParentID = numParentID FROM dbo.PageNavigationDTL WHERE numModuleID = 10
	PRINT @numParentID

	SELECT * FROM (
	SELECT PND.numPageNavID,numModuleID,numParentID,vcPageNavName,isnull(vcNavURL,'') as vcNavURL,vcImageURL ,0 sintOrder      
	FROM PageNavigationDTL PND
	JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
	WHERE  1 = 1
        AND ( ISNULL(TNA.bitVisible, 0) = 1
              --OR ISNULL(numParentID, 0) = 0
              --OR ISNULL(TNA.bitVisible, 0) = 0
            )
        AND ISNULL(PND.bitVisible, 0) = 1
	AND numModuleID = @numModuleID
	AND TNA.numDomainID = @numDomainID
	AND numGroupID = @numGroupID
	
	UNION ALL

	SELECT  numListItemID AS numPageNavID,
		    (SELECT numModuleID FROM ModuleMaster WHERE vcModuleName = 'Opportunities') AS [numModuleID],
			@numParentID AS numParentID,
			ISNULL(vcData,'') + 's' [vcPageNavName],
			'../Opportunity/frmOpenBizDocs.aspx?BizDocName=' + vcData + '&BizDocID=' + CONVERT(VARCHAR(10),LD.numListItemID) AS [vcNavURL],
			'../images/Text.gif' AS [vcImageURL],
			1 AS [bitVisible] 
	FROM dbo.ListDetails LD
	JOIN dbo.TreeNavigationAuthorization TNA ON LD.numListItemID = TNA.numPageNavID  AND ISNULL(bitVisible,0) = 1  
	WHERE numListID = 27 
	AND ISNULL(TNA.[numDomainID], 0) = @numDomainID
	AND ISNULL(TNA.[numGroupID], 0) = @numGroupID) AS [FINAL]
	ORDER BY numParentID,FINAL.numPageNavID 
END
--------------------------------------------------------- 
--ELSE IF @numModuleID = 13
--	BEGIN
--		select PND.numPageNavID,
--        numModuleID,
--        numParentID,
--        vcPageNavName,
--        isnull(vcNavURL, '') as vcNavURL,
--        vcImageURL,
--        0 sintOrder
-- from   PageNavigationDTL PND
-- WHERE  1 = 1
--        AND ISNULL(PND.bitVisible, 0) = 1
--        AND numModuleID = @numModuleID
-- order by numParentID,
--        numPageNavID 
--	END
--	
--	ELSE IF @numModuleID = 9
--	BEGIN
--			select PND.numPageNavID,
--        numModuleID,
--        numParentID,
--        vcPageNavName,
--        isnull(vcNavURL, '') as vcNavURL,
--        vcImageURL,
--        0 sintOrder
-- from   PageNavigationDTL PND
-- WHERE  1 = 1
--        AND ISNULL(PND.bitVisible, 0) = 1
--        AND numModuleID = @numModuleID
-- order by numParentID,
--        numPageNavID 
--	END

ELSE IF (@numModuleID = 7 OR @numModuleID = 36)
BEGIN
  select    PND.numPageNavID,
            numModuleID,
            numParentID,
            vcPageNavName,
            isnull(vcNavURL, '') as vcNavURL,
            vcImageURL,
            0 sintOrder
  from      PageNavigationDTL PND
  WHERE     1 = 1
            AND ISNULL(PND.bitVisible, 0) = 1
            AND numModuleID = @numModuleID
  order by  numParentID,
            numPageNavID 
END	
ELSE IF @numModuleID = 13
BEGIN
	select PND.numPageNavID,
        PND.numModuleID,
        numParentID,
        vcPageNavName,
        isnull(vcNavURL, '') as vcNavURL,
        vcImageURL,
        ISNULL(PND.intSortOrder,0) sintOrder
	 from   PageNavigationDTL PND
			JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
	 WHERE  1 = 1
			AND ( ISNULL(TNA.bitVisible, 0) = 1
				  --OR ISNULL(numParentID, 0) = 0
				  --OR ISNULL(TNA.bitVisible, 0) = 0
				)
			AND ISNULL(PND.bitVisible, 0) = 1
			AND PND.numModuleID = @numModuleID --AND bitVisible=1  
			AND numGroupID = @numGroupID
	 order by 
	    sintOrder,
		(CASE WHEN (@numModuleID = 13 AND PND.numPageNavID=79) THEN 2000 ELSE 0 END),
		numParentID,
        numPageNavID   
END
else      
begin            
 select PND.numPageNavID,
        PND.numModuleID,
        numParentID,
        vcPageNavName,
        isnull(vcNavURL, '') as vcNavURL,
        vcImageURL,
        0 sintOrder
 from   PageNavigationDTL PND
        JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
 WHERE  1 = 1
        AND ( ISNULL(TNA.bitVisible, 0) = 1
              --OR ISNULL(numParentID, 0) = 0
              --OR ISNULL(TNA.bitVisible, 0) = 0
            )
        AND ISNULL(PND.bitVisible, 0) = 1
        AND PND.numModuleID = @numModuleID --AND bitVisible=1  
        AND numGroupID = @numGroupID
 order by numParentID,
        numPageNavID      
END

--select * from TreeNavigationAuthorization where numModuleID=13 
--select * from TreeNavigationAuthorization where numTabID=-1 AND numDomainID = 1 AND numGroupID = 1
--select * from TreeNavigationAuthorization where numListID=11 and numDomainId=72
--select * from AuthoritativeBizDocs where numDomainId=72











GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetReportModuleGroupFieldMaster')
DROP PROCEDURE USP_GetReportModuleGroupFieldMaster
GO
CREATE PROCEDURE [dbo].[USP_GetReportModuleGroupFieldMaster]     
    @numDomainID numeric(18, 0),
	@numReportID numeric(18, 0)
as                 

--Get numReportModuleGroupID
DECLARE @numReportModuleGroupID AS NUMERIC(18,0);SET @numReportModuleGroupID=0
SELECT @numReportModuleGroupID=numReportModuleGroupID FROM dbo.ReportListMaster WHERE numDomainID=@numDomainID AND numReportID=@numReportID


IF @numReportModuleGroupID>0
BEGIN
CREATE TABLE #tempField(numReportFieldGroupID NUMERIC,vcFieldGroupName VARCHAR(100),numFieldID NUMERIC,
vcFieldName NVARCHAR(50),vcDbColumnName NVARCHAR(50),vcOrigDbColumnName NVARCHAR(50),vcFieldDataType CHAR(1),
vcAssociatedControlType NVARCHAR(50),vcListItemType VARCHAR(3),numListID NUMERIC,bitCustom BIT,
bitAllowSorting BIT,bitAllowGrouping BIT,bitAllowAggregate BIT,bitAllowFiltering BIT,vcLookBackTableName NVARCHAR(50))

--Regular Fields
INSERT INTO #tempField
	SELECT RFGM.numReportFieldGroupID,RFGM.vcFieldGroupName,RGMM.numFieldID,RGMM.vcFieldName,
		   DFM.vcDbColumnName,DFM.vcOrigDbColumnName,RGMM.vcFieldDataType,RGMM.vcAssociatedControlType,
		   ISNULL(DFM.vcListItemType,''),ISNULL(DFM.numListID,0),CAST(0 AS BIT) AS bitCustom,RGMM.bitAllowSorting,RGMM.bitAllowGrouping,
		   RGMM.bitAllowAggregate,RGMM.bitAllowFiltering,DFM.vcLookBackTableName
		FROM ReportFieldGroupMaster RFGM 
			JOIN ReportModuleGroupFieldMappingMaster RFMM ON RFGM.numReportFieldGroupID=RFMM.numReportFieldGroupID
			JOIN ReportFieldGroupMappingMaster RGMM ON RGMM.numReportFieldGroupID=RFGM.numReportFieldGroupID
			JOIN dbo.DycFieldMaster DFM ON DFM.numFieldId=RGMM.numFieldID
		WHERE RFMM.numReportModuleGroupID=@numReportModuleGroupID AND RFGM.bitActive=1
			AND ISNULL(RFGM.bitCustomFieldGroup,0)=0

IF @numReportModuleGroupID=9
BEGIN
INSERT INTO #tempField
SELECT 25 AS numReportFieldGroupID,'BizDocs Items Tax Field' AS vcFieldGroupName,TI.numTaxItemID,TI.vcTaxName,
		   vcTaxName AS vcDbColumnName,vcTaxName AS vcOrigDbColumnName,'M' AS vcFieldDataType,'TextBox' AS vcAssociatedControlType,
		   '' AS vcListItemType,0 AS numListID,CAST(0 AS BIT) AS bitCustom,0 AS bitAllowSorting,0 AS bitAllowGrouping,
		   1 AS bitAllowAggregate,1 AS bitAllowFiltering,'TaxItems' AS vcLookBackTableName
from TaxItems TI 
where TI.numDomainID =@numDomainID
UNION
SELECT 25 AS numReportFieldGroupID,'BizDocs Items Tax Field' AS vcFieldGroupName,0 AS numTaxItemID,'Sales Tax' AS vcTaxName,
		   'Sales Tax' AS vcDbColumnName,'Sales Tax' AS vcOrigDbColumnName,'M' AS vcFieldDataType,'TextBox' AS vcAssociatedControlType,
		   '' AS vcListItemType,0 AS numListID,CAST(0 AS BIT) AS bitCustom,0 AS bitAllowSorting,0 AS bitAllowGrouping,
		   1 AS bitAllowAggregate,1 AS bitAllowFiltering,'TaxItems' AS vcLookBackTableName
END

IF @numReportModuleGroupID=8 
BEGIN
INSERT INTO #tempField
SELECT 26 AS numReportFieldGroupID,'BizDocs Tax Field' AS vcFieldGroupName,TI.numTaxItemID,'Total ' + TI.vcTaxName,
		   'Total ' + vcTaxName AS vcDbColumnName,'Total ' +vcTaxName AS vcOrigDbColumnName,'M' AS vcFieldDataType,'TextBox' AS vcAssociatedControlType,
		   '' AS vcListItemType,0 AS numListID,CAST(0 AS BIT) AS bitCustom,0 AS bitAllowSorting,0 AS bitAllowGrouping,
		   1 AS bitAllowAggregate,1 AS bitAllowFiltering,'TaxItems' AS vcLookBackTableName
from TaxItems TI 
where TI.numDomainID =@numDomainID
UNION
SELECT 26 AS numReportFieldGroupID,'BizDocs Tax Field' AS vcFieldGroupName,0 AS numTaxItemID,'Total Sales Tax' AS vcTaxName,
		   'Total Sales Tax' AS vcDbColumnName,'Total Sales Tax' AS vcOrigDbColumnName,'M' AS vcFieldDataType,'TextBox' AS vcAssociatedControlType,
		   '' AS vcListItemType,0 AS numListID,CAST(0 AS BIT) AS bitCustom,0 AS bitAllowSorting,0 AS bitAllowGrouping,
		   1 AS bitAllowAggregate,1 AS bitAllowFiltering,'TaxItems' AS vcLookBackTableName
--UNION
--SELECT RFGM.numReportFieldGroupID,RFGM.vcFieldGroupName,ISNULL(CFM.Fld_Id,0) as numFieldID,CFM.Fld_label as vcFieldName,
--	   '' as vcDbColumnName,'' AS vcOrigDbColumnName,CASE CFM.Fld_type WHEN 'SelectBox' THEN 'V' WHEN 'DateField' THEN 'D' ELSE 'V' END AS vcFieldDataType,
--	   CFM.Fld_type AS vcAssociatedControlType,CASE WHEN CFM.Fld_type='SelectBox' THEN 'LI' ELSE '' END AS vcListItemType,isnull(CFM.numListID,0) as numListID,
--	   CAST(1 AS BIT) AS bitCustom,1 AS bitAllowSorting,CASE WHEN CFM.Fld_type='SelectBox' THEN 1 ELSE 0 END AS bitAllowGrouping,
--	   0 AS bitAllowAggregate,1 AS bitAllowFiltering,RFGM.vcCustomTableName AS vcLookBackTableName
--FROM ReportFieldGroupMaster RFGM 
--	 JOIN ReportModuleGroupFieldMappingMaster RFMM ON RFGM.numReportFieldGroupID=RFMM.numReportFieldGroupID
--	 JOIN CFW_Fld_Master CFM ON CFM.Grp_id=RFGM.numGroupID 
--	 LEFT JOIN CFw_Grp_Master CGM ON CFM.subgrp=CGM.Grp_id 
--	 --JOIN CFW_Loc_Master L on CFM.GRP_ID=L.Loc_Id
--	 WHERE RFMM.numReportModuleGroupID IN (18,19) AND RFGM.bitActive=1
--			AND ISNULL(RFGM.bitCustomFieldGroup,0)=1 AND ISNULL(RFGM.numGroupID,0)>0
--			AND CFM.numDomainID=@numDomainID  
END

IF @numReportModuleGroupID=9
BEGIN
INSERT INTO #tempField
SELECT 26 AS numReportFieldGroupID,'BizDocs Tax Field' AS vcFieldGroupName,TI.numTaxItemID,'Total ' + TI.vcTaxName,
		   'Total ' + vcTaxName AS vcDbColumnName,'Total ' +vcTaxName AS vcOrigDbColumnName,'M' AS vcFieldDataType,'TextBox' AS vcAssociatedControlType,
		   '' AS vcListItemType,0 AS numListID,CAST(0 AS BIT) AS bitCustom,0 AS bitAllowSorting,0 AS bitAllowGrouping,
		   1 AS bitAllowAggregate,1 AS bitAllowFiltering,'TaxItems' AS vcLookBackTableName
from TaxItems TI 
where TI.numDomainID =@numDomainID
UNION
SELECT 26 AS numReportFieldGroupID,'BizDocs Tax Field' AS vcFieldGroupName,0 AS numTaxItemID,'Total Sales Tax' AS vcTaxName,
		   'Total Sales Tax' AS vcDbColumnName,'Total Sales Tax' AS vcOrigDbColumnName,'M' AS vcFieldDataType,'TextBox' AS vcAssociatedControlType,
		   '' AS vcListItemType,0 AS numListID,CAST(0 AS BIT) AS bitCustom,0 AS bitAllowSorting,0 AS bitAllowGrouping,
		   1 AS bitAllowAggregate,1 AS bitAllowFiltering,'TaxItems' AS vcLookBackTableName
UNION
SELECT RFGM.numReportFieldGroupID,RFGM.vcFieldGroupName,ISNULL(CFM.Fld_Id,0) as numFieldID,CFM.Fld_label as vcFieldName,
	   '' as vcDbColumnName,'' AS vcOrigDbColumnName,CASE CFM.Fld_type WHEN 'SelectBox' THEN 'V' WHEN 'DateField' THEN 'D' ELSE 'V' END AS vcFieldDataType,
	   CFM.Fld_type AS vcAssociatedControlType,CASE WHEN CFM.Fld_type='SelectBox' THEN 'LI' ELSE '' END AS vcListItemType,isnull(CFM.numListID,0) as numListID,
	   CAST(1 AS BIT) AS bitCustom,1 AS bitAllowSorting,CASE WHEN CFM.Fld_type='SelectBox' THEN 1 ELSE 0 END AS bitAllowGrouping,
	   0 AS bitAllowAggregate,1 AS bitAllowFiltering,RFGM.vcCustomTableName AS vcLookBackTableName
FROM ReportFieldGroupMaster RFGM 
	 JOIN ReportModuleGroupFieldMappingMaster RFMM ON RFGM.numReportFieldGroupID=RFMM.numReportFieldGroupID
	 JOIN CFW_Fld_Master CFM ON CFM.Grp_id=RFGM.numGroupID 
	 LEFT JOIN CFw_Grp_Master CGM ON CFM.subgrp=CGM.Grp_id 
	 --JOIN CFW_Loc_Master L on CFM.GRP_ID=L.Loc_Id
	 WHERE RFMM.numReportModuleGroupID IN (18,19) AND RFGM.bitActive=1
			AND ISNULL(RFGM.bitCustomFieldGroup,0)=1 AND ISNULL(RFGM.numGroupID,0)>0
			AND CFM.numDomainID=@numDomainID  
END
IF @numReportModuleGroupID=19 OR @numReportModuleGroupID=23
BEGIN
INSERT INTO #tempField
SELECT RFGM.numReportFieldGroupID,RFGM.vcFieldGroupName,ISNULL(CFM.Fld_Id,0) as numFieldID,CFM.Fld_label as vcFieldName,
	   '' as vcDbColumnName,'' AS vcOrigDbColumnName,CASE CFM.Fld_type WHEN 'SelectBox' THEN 'V' WHEN 'DateField' THEN 'D' ELSE 'V' END AS vcFieldDataType,
	   CFM.Fld_type AS vcAssociatedControlType,CASE WHEN CFM.Fld_type='SelectBox' THEN 'LI' ELSE '' END AS vcListItemType,isnull(CFM.numListID,0) as numListID,
	   CAST(1 AS BIT) AS bitCustom,1 AS bitAllowSorting,CASE WHEN CFM.Fld_type='SelectBox' THEN 1 ELSE 0 END AS bitAllowGrouping,
	   0 AS bitAllowAggregate,1 AS bitAllowFiltering,RFGM.vcCustomTableName AS vcLookBackTableName
FROM ReportFieldGroupMaster RFGM 
	 JOIN ReportModuleGroupFieldMappingMaster RFMM ON RFGM.numReportFieldGroupID=RFMM.numReportFieldGroupID
	 JOIN CFW_Fld_Master CFM ON CFM.Grp_id=RFGM.numGroupID 
	 LEFT JOIN CFw_Grp_Master CGM ON CFM.subgrp=CGM.Grp_id 
	 --JOIN CFW_Loc_Master L on CFM.GRP_ID=L.Loc_Id
	 WHERE RFMM.numReportModuleGroupID IN (18,19) AND RFGM.bitActive=1
			AND ISNULL(RFGM.bitCustomFieldGroup,0)=1 AND ISNULL(RFGM.numGroupID,0)>0
			AND CFM.numDomainID=@numDomainID  
END

--Custom Fields			
INSERT INTO #tempField
SELECT RFGM.numReportFieldGroupID,RFGM.vcFieldGroupName,ISNULL(CFM.Fld_Id,0) as numFieldID,CFM.Fld_label as vcFieldName,
	   '' as vcDbColumnName,'' AS vcOrigDbColumnName,CASE CFM.Fld_type WHEN 'SelectBox' THEN 'V' WHEN 'DateField' THEN 'D' ELSE 'V' END AS vcFieldDataType,
	   CFM.Fld_type AS vcAssociatedControlType,CASE WHEN CFM.Fld_type='SelectBox' THEN 'LI' ELSE '' END AS vcListItemType,isnull(CFM.numListID,0) as numListID,
	   CAST(1 AS BIT) AS bitCustom,1 AS bitAllowSorting,CASE WHEN CFM.Fld_type='SelectBox' THEN 1 ELSE 0 END AS bitAllowGrouping,
	   0 AS bitAllowAggregate,1 AS bitAllowFiltering,RFGM.vcCustomTableName AS vcLookBackTableName
FROM ReportFieldGroupMaster RFGM 
	 JOIN ReportModuleGroupFieldMappingMaster RFMM ON RFGM.numReportFieldGroupID=RFMM.numReportFieldGroupID
	 JOIN CFW_Fld_Master CFM ON CFM.Grp_id=RFGM.numGroupID 
	 LEFT JOIN CFw_Grp_Master CGM ON CFM.subgrp=CGM.Grp_id 
	 --JOIN CFW_Loc_Master L on CFM.GRP_ID=L.Loc_Id
	 WHERE RFMM.numReportModuleGroupID=@numReportModuleGroupID AND RFGM.bitActive=1
			AND ISNULL(RFGM.bitCustomFieldGroup,0)=1 AND ISNULL(RFGM.numGroupID,0)>0
			AND CFM.numDomainID=@numDomainID  

SELECT * FROM #tempField ORDER BY numReportFieldGroupID

DROP TABLE #tempField
		
END
/****** Object:  StoredProcedure [dbo].[USP_GetReturns]    Script Date: 01/22/2009 01:39:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetReturns' ) 
    DROP PROCEDURE USP_GetReturns
GO
CREATE PROCEDURE [dbo].[USP_GetReturns]
    @numReturnId NUMERIC(9) = NULL,
    @numDomainId NUMERIC(9) = 0,
    @numReturnStatus NUMERIC(9) = NULL,
    @numReturnReason NUMERIC(9) = NULL,
    @charSortCharacter CHAR(1) = NULL,
    @vcSearchText VARCHAR(50) = NULL,
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT,
    @ClientTimeZoneOffset AS INT,
    @tintReturnType AS TINYINT
AS 
    BEGIN
        DECLARE @firstRec AS INTEGER
        DECLARE @lastRec AS INTEGER
        SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
        SET @lastRec = ( @CurrentPage * @PageSize + 1 )
        SELECT  @TotRecs = COUNT(*)
        FROM    dbo.ReturnHeader AS RH
        WHERE  ( RH.numReturnReason = @numReturnReason OR @numReturnReason IS NULL)
           AND ( RH.numReturnStatus = @numreturnstatus OR @numreturnstatus IS NULL)
           AND ( RH.[numReturnHeaderID] = @numReturnId OR @numReturnId IS NULL)
           AND RH.[numDomainId] = @numDomainId
           AND RH.tintReturnType = @tintReturnType
           AND ( RH.vcRMA LIKE @charSortCharacter + '%' OR @charSortCharacter IS NULL)
           AND ( ( RH.vcRMA LIKE '%' + @vcSearchText) OR @vcSearchText IS NULL) ;
                       
        WITH    salesreturn
                  AS ( SELECT   Row_number() OVER ( ORDER BY RH.dtCreatedDate DESC ) AS row,
                                RH.numoppid,
                                RH.numReturnHeaderID,
                                vcRMA,
                                RH.numReturnStatus,
                                RH.numReturnReason,
                                RH.numDivisionId,
                                CASE WHEN CONVERT(VARCHAR(11), DATEADD(minute, -@ClientTimeZoneOffset, RH.dtCreatedDate)) = CONVERT(VARCHAR(11), GETDATE())
                                     THEN '<b><font color=red>Today</font></b>'
                                     WHEN CONVERT(VARCHAR(11), DATEADD(minute, -@ClientTimeZoneOffset, RH.dtCreatedDate)) = CONVERT(VARCHAR(11), DATEADD(day, -1, GETDATE()))
                                     THEN '<b><font color=purple>YesterDay</font></b>'
                                     WHEN CONVERT(VARCHAR(11), DATEADD(minute, -@ClientTimeZoneOffset, RH.dtCreatedDate)) = CONVERT(VARCHAR(11), DATEADD(day, 1, GETDATE()))
                                     THEN '<b><font color=orange>Tommorow</font></b>'
                                     ELSE dbo.FormatedDateFromDate(RH.dtCreatedDate, RH.[numdomainid])
                                END dtCreateDate,RH.tintReturnType, SUBSTRING(com.vcCompanyName, 0, 30) AS vcCompanyName
								,RH.[numAccountID]
                       FROM    	dbo.ReturnHeader AS RH 
								JOIN divisionMaster div ON div.numdivisionid = RH.numdivisionid
                                JOIN companyInfo com ON com.numCompanyID = div.numCompanyID
                       WHERE   ( RH.numReturnReason = @numReturnReason
                                      OR @numReturnReason IS NULL
                                    )
                                AND ( RH.numReturnStatus = @numReturnStatus
                                      OR @numReturnStatus IS NULL
                                    )
                                AND ( RH.numReturnHeaderID = @numReturnId
                                      OR @numReturnId IS NULL
                                    )
                                AND RH.[numdomainid] = @numDomainId
                                AND RH.tintReturnType = @tintReturnType
                                AND ( RH.vcRMA LIKE @charSortCharacter
                                      + '%'
                                      OR @charSortCharacter IS NULL
                                    )
                                AND ( ( RH.vcRMA LIKE '%' + @vcSearchText + '%')
                                      OR @vcSearchText IS NULL
                                    )
                     )
            SELECT  *,
                    dbo.[getlistiemname](numReturnReason) reasonforreturn,
                    dbo.[getlistiemname](numReturnStatus) AS [status],
                    IsEditable = CASE dbo.[getlistiemname](numreturnstatus)
                                   WHEN 'Pending' THEN 'True'
                                   WHEN 'Confirmed' THEN 'True'
                                   ELSE 'True'
                                 END
					,(SELECT TOP 1 vcAccountName FROM [dbo].[Chart_Of_Accounts] WHERE numDOmainID = @numDomainId AND [Chart_Of_Accounts].[numAccountId] = salesreturn.[numAccountID]) AS [vcCreditFromAccount]
            FROM    salesreturn
            WHERE   row > @firstRec
                    AND row < @lastRec ;
  
    END
  
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
--2 changes
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount money =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(9),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0
)                                                                          
                                                                          
as  

 DECLARE @fltExchangeRate float                                 
 DECLARE @hDocItem int                                                                                                                            
 DECLARE @TotalAmount as money                                                                          
 DECLARE @tintOppStatus as tinyint               
 DECLARE @tintDefaultClassType NUMERIC
 DECLARE @numDefaultClassID NUMERIC;SET @numDefaultClassID=0
 
 --Get Default Item Class for particular user based on Domain settings  
 SELECT @tintDefaultClassType=isnull(tintDefaultClassType,0) FROM dbo.Domain  WHERE numDomainID = @numDomainID 

 IF @tintDefaultClassType=0
      SELECT @numDefaultClassID=NULLIF(numDefaultClass,0) FROM dbo.UserMaster  WHERE numUserDetailId =@numUserCntID AND numDomainID = @numDomainID 


--If new Oppertunity
if @numOppID = 0                                                                          
 begin     
  declare @intOppTcode as numeric(9)                           
  Select @intOppTcode=max(numOppId)from OpportunityMaster                
  set @intOppTcode =isnull(@intOppTcode,0) + 1                                                
  set @vcPOppName=@vcPOppName+ '-'+convert(varchar(4),year(getutcdate()))  +'-A'+ convert(varchar(10),@intOppTcode)                                  
  
  IF ISNULL(@numCurrencyID,0) = 0 
	BEGIN
	 SET @fltExchangeRate=1
	 SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
	END
  else set @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
                                                                       
  --Set Default Class If enable User Level Class Accountng 
  DECLARE @numAccountClass AS NUMERIC(18);SET @numAccountClass=0
  SELECT @numAccountClass=ISNULL(numDefaultClass,0) FROM dbo.UserMaster UM JOIN dbo.Domain D ON UM.numDomainID=D.numDomainId
  WHERE D.numDomainId=@numDomainId AND UM.numUserDetailId=@numUserCntID AND ISNULL(D.IsEnableUserLevelClassTracking,0)=1
                                                                       
  insert into OpportunityMaster                                                                          
  (                                                                             
  numContactId,                                                                   
  numDivisionId,                                                                          
  txtComments,                                             
  numCampainID,                                                                          
  bitPublicFlag,                                                                          
  tintSource,
  tintSourceType,                                                             
  vcPOppName,                                                                          
  intPEstimatedCloseDate,                                                                          
  monPAmount,                                                                           
  numCreatedBy,                                                                          
  bintCreatedDate,                                                                           
  numModifiedBy,                                                                          
  bintModifiedDate,                                                                          
  numDomainId,                                                                                                             
  numRecOwner,                                                                          
  lngPConclAnalysis,                                                                         
  tintOppType,                                                              
  numSalesOrPurType,
  numCurrencyID,
  fltExchangeRate,
  numAssignedTo,
  numAssignedBy,
  [tintOppStatus],
  numStatus,
  vcOppRefOrderNo,
  --vcWebApiOrderNo,
  bitStockTransfer,bitBillingTerms,intBillingDays,bitInterestType,fltInterest,vcCouponCode,
  bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,vcMarketplaceOrderReportId,
  numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
  bitUseMarkupShippingRate,
  numMarkupShippingRate,
  intUsedShippingCompany
  )                                                                          
  Values                                                                          
  (                                                                          
  @numContactId,                                                                          
  @numDivisionId,                                                                          
  @Comments,                           
  @CampaignID,                                                                          
  @bitPublicFlag,                     
  @tintSource,
  @tintSourceType,                         
  @vcPOppName,                                                                          
  @dtEstimatedCloseDate,                                                                          
  @monPAmount,                                                                                
  @numUserCntID,                                                                          
 CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END,                                                                          
  @numUserCntID,                                                           
  getutcdate(),                                                                          
  @numDomainId,                                                                                   
  @numUserCntID,                                                                          
  @lngPConclAnalysis,                                                                                                                                                                      
  @tintOppType,                                                                                                                                      
  @numSalesOrPurType,              
  @numCurrencyID, 
  @fltExchangeRate,
  case when @numAssignedTo>0 then @numAssignedTo else null end,
  case when @numAssignedTo>0 then @numUserCntID else null   END,
  @DealStatus,
  @numStatus,
  @vcOppRefOrderNo,
 -- @vcWebApiOrderNo,
  @bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,@vcCouponCode,
  @bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,
  @numPercentageComplete,@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,
  @bitUseMarkupShippingRate,
  @numMarkupShippingRate,
  @intUsedShippingCompany
    )                                                                                                                      
  set @numOppID=scope_identity()                                                
  
  --Update OppName as per Name Template
  EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

  --Map Custom Field	
  DECLARE @tintPageID AS TINYINT;
  SET @tintPageID=CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
  EXEC dbo.USP_AddParentChildCustomFieldMap
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@numRecordID = @numOppID, --  numeric(18, 0)
	@numParentRecId = @numDivisionId, --  numeric(18, 0)
	@tintPageID = @tintPageID --  tinyint
 	
  
	IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
	   BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
	   END
  ---- inserting Items                                                                          
   
                                                   
  if convert(varchar(10),@strItems) <>'' AND @numOppID>0
  begin                      
   EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
  insert into                       
   OpportunityItems                                                                          
   (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,
   numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,monAvgCost,bitItemPriceApprovalRequired)
   select @numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,
   x.monPrice,x.monTotAmount,X.vcItemDesc,
   CASE X.numWarehouseItmsID WHEN -1 
							 THEN (SELECT [numWareHouseItemID] FROM [WareHouseItems] 
								   WHERE [numItemID] = X.numItemCode 
								   AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								   AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail 
														  WHERE numDomainID =@numDomainId 
														  AND WebApiId = @WebApiId))  
							 WHEN 0 
							 THEN (SELECT TOP 1 [numWareHouseItemID] FROM [WareHouseItems] 
								   WHERE [numItemID] = X.numItemCode
								   AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')) 
							 ELSE  X.numWarehouseItmsID 
	END AS numWarehouseItmsID,
	X.ItemType,X.DropShip,X.bitDiscountType,
   X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,
   (SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
   (SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
   (SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
   (select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID),
   X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,Case When @tintDefaultClassType=0 then @numDefaultClassID else (SELECT numItemClass FROM item WHERE numItemCode = X.numItemCode) end,X.numToWarehouseItemID,X.Attributes,
   (SELECT ISNULL(monAverageCost,0) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),X.bitItemPriceApprovalRequired from(
   SELECT *FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
   WITH                       
   (                                                                          
   numoppitemtCode numeric(9),                                     
   numItemCode numeric(9),                                                                          
   numUnitHour numeric(9,2),                                                                          
   monPrice money,                                                                       
   monTotAmount money,                                                                          
   vcItemDesc varchar(1000),                                    
   numWarehouseItmsID numeric(9),                          
   ItemType varchar(30),      
   DropShip bit,
   bitDiscountType bit,
   fltDiscount float,
   monTotAmtBefDiscount MONEY,
   vcItemName varchar(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9) ,numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(200),vcSKU VARCHAR(100),bitItemPriceApprovalRequired BIT
   ))X    
   ORDER BY 
   X.numoppitemtCode
    
    
   Update OpportunityItems                       
   set numItemCode=X.numItemCode,    
   numOppId=@numOppID,                     
   numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
   monPrice=x.monPrice,                      
   monTotAmount=x.monTotAmount,                                  
   vcItemDesc=X.vcItemDesc,                      
   numWarehouseItmsID=X.numWarehouseItmsID,                 
   bitDropShip=X.DropShip,
   bitDiscountType=X.bitDiscountType,
   fltDiscount=X.fltDiscount,
   monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,bitWorkOrder=X.bitWorkOrder,
   numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,
vcAttributes=X.Attributes,numClassID=(Case When @tintDefaultClassType=0 then @numDefaultClassID else (SELECT numItemClass FROM item WHERE numItemCode = X.numItemCode) END),bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired
--   ,vcModelID=(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),vcManufacturer=(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode)
--  ,vcPathForTImage=(SELECT vcPathForTImage FROM item WHERE numItemCode = X.numItemCode),monVendorCost=(select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID)
   from (SELECT numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,
   numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,Attributes,bitItemPriceApprovalRequired
   FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
   WITH  (                      
    numoppitemtCode numeric(9),                                     
    numItemCode numeric(9),                                                                          
    numUnitHour numeric(9,2),                                                                          
    monPrice money,                                         
    monTotAmount money,                                                                          
    vcItemDesc varchar(1000),                                    
    numWarehouseItmsID numeric(9),      
    DropShip bit,
	bitDiscountType bit,
    fltDiscount float,
    monTotAmtBefDiscount MONEY,vcItemName varchar(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(200),bitItemPriceApprovalRequired BIT
   ))X where numoppitemtCode=X.numOppItemID                                          
	
	
   -- Update UOM of opportunity items have not UOM set while exported from Marketplace (means tintSourceType = 3)
	IF ISNULL(@tintSourceType,0) = 3
	BEGIN
		--SELECT * FROM OpportunityMaster WHERE numOppId = 81654
		--SELECT * FROM OpportunityItems WHERE numOppId = 81654
		--SELECT * FROM Item WHERE numItemCode = 822078

		UPDATE OI SET numUOMID = ISNULL(I.numSaleUnit,0), 
					  numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
		FROM OpportunityItems OI
		JOIN Item I ON OI.numItemCode = I.numItemCode
		WHERE numOppId = @numOppID
		AND I.numDomainID = @numDomainId

	END	
   --Update OpportunityItems
   --set 
   --FROM (SELECT numItemCode,vcItemName,[txtItemDesc],vcModelID FROM item WHERE numItemCode IN (SELECT [numItemCode] FROM OpportunityItems WHERE numOppID = @numOppID))X
   --WHERE OpportunityItems.[numItemCode] = X.numItemCode 
                                     
   insert into OppWarehouseSerializedItem                                                                          
   (numOppID,numWarehouseItmsDTLID,numOppItemID,numWarehouseItmsID)                      
   select @numOppID,X.numWarehouseItmsDTLID,X.numoppitemtCode,X.numWItmsID from(                                                                          
   SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
   WITH                        
   (                                               
   numWarehouseItmsDTLID numeric(9),                      
   numoppitemtCode numeric(9),                      
   numWItmsID numeric(9)                                                     
   ))X                                    
               
                                 
   update OppWarehouseSerializedItem                       
   set numOppItemID=X.numoppitemtCode                                
   from                       
   (SELECT numWarehouseItmsDTLID as DTLID,O.numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
   WITH(numWarehouseItmsDTLID numeric(9),numoppitemtCode numeric(9),numWItmsID numeric(9))                                
   join OpportunityItems O on O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID)X                 
   where numOppID=@numOppID and numWarehouseItmsDTLID=X.DTLID      
    
--   insert into OpportunityKitItems(numOppKitItem,numChildItem,intQuantity,numWareHouseItemId)    
--   SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/ChildItems',2)                                                                          
--   WITH                        
--   (                                               
--   numoppitemtCode numeric(9),                      
--   numItemCode numeric(9),    
--   numQtyItemsReq integer,  
--   numWarehouseItmsID numeric(9)                                                       
--   )    
      
    
    
    
	EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
    
                              
                          
   EXEC sp_xml_removedocument @hDocItem                                     
        end          
   set @TotalAmount=0                                                           
   select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@numOppID                                                                          
  update OpportunityMaster set  monPamount=@TotalAmount                                                          
  where numOppId=@numOppID                                                 
                      
     
--Insert Tax for Division   

IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN                   
	INSERT dbo.OpportunityMasterTaxItems (
		numOppId,
		numTaxItemID,
		fltPercentage
	) SELECT @numOppID,TI.numTaxItemID,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL) FROM TaxItems TI JOIN DivisionTaxTypes DTT ON TI.numTaxItemID = DTT.numTaxItemID
	 WHERE DTT.numDivisionID=@numDivisionId AND DTT.bitApplicable=1
	   union 
	  select @numOppID,0,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL) 
	  FROM dbo.DivisionMaster WHERE bitNoTax=0 AND numDivisionID=@numDivisionID
END
-- Archieve item based on Item's individual setting
--	UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT * FROM dbo.OpportunityItems WHERE OpportunityItems.numItemCode = I.numItemCode) AND ISNULL(I.bitArchiveItem,0) = 1 
--									  THEN 1 
--									  ELSE 0 
--									  END 
--	FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode
	
 END
 
 else                                                                                                                          
 BEGIN                  
	--Declaration
	 DECLARE @tempAssignedTo AS NUMERIC(9)                                                    
	 SET @tempAssignedTo = NULL 
	 
	 SELECT @tintOppStatus = tintOppStatus,@tempAssignedTo=isnull(numAssignedTo,0) FROM   OpportunityMaster WHERE  numOppID = @numOppID
 
	--Reverting back the warehouse items                  
	 IF @tintOppStatus = 1 
		BEGIN        
		PRINT 'inside revert'          
			EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID                  
		END                  
	-- Update Master table
	
	   IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
	   BEGIN
	   		UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
	   END
		
	   IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
	   BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
	   END 
						
	   UPDATE   OpportunityMaster
	   SET      vcPOppName = @vcPOppName,
				txtComments = @Comments,
				bitPublicFlag = @bitPublicFlag,
				numCampainID = @CampaignID,
				tintSource = @tintSource,
				tintSourceType=@tintSourceType,
				intPEstimatedCloseDate = @dtEstimatedCloseDate,
				numModifiedBy = @numUserCntID,
				bintModifiedDate = GETUTCDATE(),
				lngPConclAnalysis = @lngPConclAnalysis,
				monPAmount = @monPAmount,
				tintActive = @tintActive,
				numSalesOrPurType = @numSalesOrPurType,
				numStatus = @numStatus,
				tintOppStatus = @DealStatus,
				vcOppRefOrderNo=@vcOppRefOrderNo,
				--vcWebApiOrderNo = @vcWebApiOrderNo,
				bintOppToOrder=(Case when @tintOppStatus=0 and @DealStatus=1 then GETUTCDATE() else bintOppToOrder end),
				bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,bitBillingTerms=@bitBillingTerms,
				intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,
				tintTaxOperator=@tintTaxOperator,numDiscountAcntType=@numDiscountAcntType,vcCouponCode = @vcCouponCode,
				numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
				bitUseMarkupShippingRate = @bitUseMarkupShippingRate,
				numMarkupShippingRate = @numMarkupShippingRate,
				intUsedShippingCompany = @intUsedShippingCompany
	   WHERE    numOppId = @numOppID   
	   
	---Updating if organization is assigned to someone                                                      
	   IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN                                   
			UPDATE  OpportunityMaster SET     numAssignedTo = @numAssignedTo, numAssignedBy = @numUserCntID WHERE   numOppId = @numOppID                                                    
		END                                                     
	   ELSE 
	   IF ( @numAssignedTo = 0 ) 
		BEGIN                
			UPDATE  OpportunityMaster SET     numAssignedTo = 0, numAssignedBy = 0 WHERE   numOppId = @numOppID
		END                                                                      
		---- Updating Opp Items
		if convert(varchar(10),@strItems) <>'' AND @numOppID>0                                                                         
		begin
			   EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   --Delete Items
			   delete from OpportunityKitItems where numOppID=@numOppID and numOppItemID not in                       
			   (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
			   WITH  (numoppitemtCode numeric(9)))                      
                                        
--               ---- ADDED BY Manish Anjara : Jun 26,2013 - Archive item based on Item's individual setting
--               UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT * FROM dbo.OpportunityItems WHERE OpportunityItems.numItemCode = I.numItemCode) AND ISNULL(I.bitArchiveItem,0) = 1 
--												 THEN 1 
--												 ELSE 0 
--												 END 
--			   FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode
--					 	
--			   UPDATE Item SET IsArchieve = CASE WHEN ISNULL(I.bitArchiveItem,0) = 1 THEN 0 ELSE 1 END
--			   FROM Item I
--			   JOIN OpportunityItems OI ON I.numItemCode = OI.numItemCode
--			   WHERE numOppID = @numOppID and OI.numItemCode NOT IN (SELECT numItemCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) 
--																	 WITH (numItemCode numeric(9)))

			   delete from OpportunityItems where numOppID=@numOppID and numoppitemtCode not in                       
			   (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
			   WITH  (numoppitemtCode numeric(9)))   
	                                
			   insert into OpportunityItems                                                                          
			   (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired)
			   select @numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),(select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID),X.numUOM,X.bitWorkOrder,
			   X.numVendorWareHouse,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,Case When @tintDefaultClassType=0 then @numDefaultClassID else (SELECT numItemClass FROM item WHERE numItemCode = X.numItemCode) END,
			   (SELECT ISNULL(monAverageCost,0) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired from(
			   SELECT *FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
			   WITH  (                      
				numoppitemtCode numeric(9),                                     
				numItemCode numeric(9),                                                                          
				numUnitHour numeric(9,2),                                                                          
				monPrice money,                                                                       
				monTotAmount money,                                                                          
				vcItemDesc varchar(1000),                                    
				numWarehouseItmsID numeric(9),                              
				Op_Flag tinyint,                            
				ItemType varchar(30),      
				DropShip bit,
				bitDiscountType bit,
				fltDiscount float,
				monTotAmtBefDiscount MONEY,
				vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(200), bitItemPriceApprovalRequired BIT
				 ))X                                     
				--Update items                 
			   Update OpportunityItems                       
			   set numItemCode=X.numItemCode,    
			   numOppId=@numOppID,                       
			   numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
			   monPrice=x.monPrice,                      
			   monTotAmount=x.monTotAmount,                                  
			   vcItemDesc=X.vcItemDesc,                      
			   numWarehouseItmsID=X.numWarehouseItmsID,                 
			   bitDropShip=X.DropShip,
			   bitDiscountType=X.bitDiscountType,
			   fltDiscount=X.fltDiscount,
			   monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
			   numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired
			   from (SELECT numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,bitItemPriceApprovalRequired  FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
			   WITH  (                      
				numoppitemtCode numeric(9),                                     
				numItemCode numeric(9),                                                                          
				numUnitHour numeric(9,2),                                                                          
				monPrice money,                                         
				monTotAmount money,                                                                          
				vcItemDesc varchar(1000),                                    
				numWarehouseItmsID numeric(9),      
				DropShip bit,
				bitDiscountType bit,
				fltDiscount float,
				monTotAmtBefDiscount MONEY,vcItemName varchar(300),numUOM NUMERIC(9),numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(200),bitItemPriceApprovalRequired BIT                                              
			   ))X where numoppitemtCode=X.numOppItemID                           
			                                    
			   EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			   		                                                                      
			   EXEC sp_xml_removedocument @hDocItem                                               
		end     
	    --Delete previously added serial lot number added for opportunity
		DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
END

----------------generic section will be called in both insert and update ---------------------------------
-- Archieve item based on Item's individual setting
--	UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT * FROM dbo.OpportunityItems WHERE OpportunityItems.numItemCode = I.numItemCode) AND ISNULL(I.bitArchiveItem,0) = 1 
--									  THEN 1 
--									  ELSE 0 
--									  END 
--	FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode
	
if convert(varchar(10),@strItems) <>'' AND @numOppID>0                                                                        
  begin 
EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

--delete from OpportunityKitItems where numOppID=@numOppID and numOppItemID 
--	not in (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppID=@numOppID)                      
		
--Update Kit Items                 
Update OKI set numQtyItemsReq=numQtyItemsReq_Orig * OI.numUnitHour
			   FROM OpportunityKitItems OKI JOIN OpportunityItems OI ON OKI.numOppItemID=OI.numoppitemtCode and OKI.numOppId=OI.numOppId  
			   WHERE OI.numOppId=@numOppId 

--Insert Kit Items                 
INSERT into OpportunityKitItems                                                                          
		(numOppId,numOppItemID,numChildItemID,numWareHouseItemId,numQtyItemsReq,numQtyItemsReq_Orig,numUOMId,numQtyShipped)
  SELECT @numOppId,OI.numoppitemtCode,ID.numChildItemID,ID.numWareHouseItemId,ID.numQtyItemsReq * OI.numUnitHour,ID.numQtyItemsReq,ID.numUOMId,0
	 FROM OpportunityItems OI JOIN ItemDetails ID ON OI.numItemCode=ID.numItemKitID WHERE 
	OI.numOppId=@numOppId AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID)
--	OI.numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
--			   WITH  (                      
--				numoppitemtCode numeric(9))X)
			   
EXEC sp_xml_removedocument @hDocItem  
END

/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
declare @tintShipped as tinyint               
select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID              
if @tintOppStatus=1               
	begin         
	PRINT 'inside USP_UpdatingInventoryonCloseDeal'
		exec USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
	end              
            
if @tintOppType=1              
begin              
	if @tintOppStatus=1 Update WareHouseItmsDTL set tintStatus=1 where numWareHouseItmsDTLID in (select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppID=@numOppID)
	if @tintShipped=1 Update WareHouseItmsDTL set tintStatus=2 where numWareHouseItmsDTLID in (select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppID=@numOppID)              
END


declare @tintCRMType as numeric(9)        
declare @AccountClosingDate as datetime        

select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

--When deal is won                                         
if @DealStatus = 1 
begin           
	 if @AccountClosingDate is null                   
	  update OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numOppID              
end         
--When Deal is Lost
else if @DealStatus = 2
begin         
	 select @AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
	 if @AccountClosingDate is null                   
	  update OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numOppID
end                    

/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
begin        
	update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end        
-- Promote Lead to Account when Sales/Purchase Order is created against it
ELSE if @tintCRMType=0 AND @DealStatus = 1 --Lead & Order
begin        
	update divisionmaster set tintCRMType=2,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end
--Promote Prospect to Account
else if @tintCRMType=1 AND @DealStatus = 1 
begin        
	update divisionmaster set tintCRMType=2,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9) 
DECLARE @bitIsPrimary BIT;

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

--Bill Address
IF @numBillAddressId>0
BEGIN
 SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numBillAddressId
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName
END
  
  
  --Ship Address
IF @numShipAddressId>0
BEGIN
	SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numShipAddressId
 



  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName
END

--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems (
	numOppId,
	numOppItemID,
	numTaxItemID
) SELECT @numOppId,OI.numoppitemtCode,TI.numTaxItemID FROM dbo.OpportunityItems OI JOIN dbo.ItemTax IT ON OI.numItemCode=IT.numItemCode JOIN
TaxItems TI ON TI.numTaxItemID=IT.numTaxItemID WHERE OI.numOppId=@numOppID AND IT.bitApplicable=1 AND 
OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
  select @numOppId,OI.numoppitemtCode,0 FROM dbo.OpportunityItems OI JOIN dbo.Item I ON OI.numItemCode=I.numItemCode
   WHERE OI.numOppId=@numOppID  AND I.bitTaxable=1 AND
OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

  
 UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID
