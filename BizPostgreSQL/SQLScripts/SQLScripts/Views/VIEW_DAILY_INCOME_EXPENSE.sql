GO
IF EXISTS ( SELECT * FROM sysobjects WHERE xtype = 'V' AND NAME = 'VIEW_DAILY_INCOME_EXPENSE' ) 
    DROP VIEW VIEW_DAILY_INCOME_EXPENSE
GO
CREATE VIEW [dbo].[VIEW_DAILY_INCOME_EXPENSE]
AS
SELECT     numDomainID, vcAccountCode, datEntry_Date, SUM(Debit) AS Debit, SUM(Credit) AS Credit, COUNT_BIG(*) AS Expr3
FROM         dbo.VIEW_JOURNAL
WHERE     (COAvcAccountCode LIKE '0104%') OR
                      (COAvcAccountCode LIKE '0103%') AND 
                      (COAvcAccountCode LIKE '0106%') 
GROUP BY numDomainID, vcAccountCode, datEntry_Date

GO
