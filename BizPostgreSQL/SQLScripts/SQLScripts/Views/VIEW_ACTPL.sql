GO
IF EXISTS ( SELECT * FROM sysobjects WHERE xtype = 'V' AND NAME = 'VIEW_ACTPL' ) 
    DROP VIEW VIEW_ACTPL
GO
CREATE VIEW [dbo].[VIEW_ACTPL]
AS
SELECT     numAccountTypeID, vcAccountCode, vcAccountType, numParentID, numDomainID, dtCreateDate, dtModifiedDate
FROM         dbo.AccountTypeDetail
WHERE     (vcAccountCode LIKE '0103%')  OR(vcAccountCode LIKE '0104%') OR(vcAccountCode LIKE '0106%')
GO
