GO
IF EXISTS ( SELECT * FROM sysobjects WHERE xtype = 'V' AND NAME = 'VIEW_JOURNAL' ) 
    DROP VIEW VIEW_JOURNAL
GO
CREATE VIEW [dbo].[VIEW_JOURNAL]
WITH SCHEMABINDING 
AS
SELECT     dbo.AccountTypeDetail.numAccountTypeID, dbo.AccountTypeDetail.vcAccountCode, dbo.AccountTypeDetail.vcAccountType, 
                      SUM(ISNULL(dbo.General_Journal_Details.numDebitAmt, 0)) AS Debit, SUM(ISNULL(dbo.General_Journal_Details.numCreditAmt, 0)) AS Credit, 
                      dbo.General_Journal_Header.datEntry_Date, dbo.Chart_Of_Accounts.numAccountId, dbo.Chart_Of_Accounts.vcAccountName, 
                      dbo.Chart_Of_Accounts.vcAccountCode AS COAvcAccountCode, dbo.General_Journal_Header.numDomainID,
                      ISNULL(dbo.General_Journal_Details.numClassID,0) AS numAccountClass
FROM         dbo.Chart_Of_Accounts 
INNER JOIN dbo.Domain ON dbo.Domain.numDomainID = dbo.Chart_Of_Accounts.numDomainId 
INNER JOIN dbo.AccountTypeDetail ON dbo.Chart_Of_Accounts.numParntAcntTypeID = dbo.AccountTypeDetail.numAccountTypeID INNER JOIN
                      dbo.General_Journal_Header INNER JOIN
                      dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId ON 
                      dbo.Chart_Of_Accounts.numAccountId = dbo.General_Journal_Details.numChartAcntId
                      where dbo.General_Journal_Header.numDomainId=dbo.General_Journal_Details.numDomainId
                      AND dbo.General_Journal_Header.numDomainID = dbo.Domain.numDomainID 
					  AND dbo.General_Journal_Details.numDomainID = dbo.Domain.numDomainID 
GROUP BY dbo.AccountTypeDetail.numAccountTypeID, dbo.AccountTypeDetail.vcAccountCode, dbo.AccountTypeDetail.vcAccountType, 
                      dbo.General_Journal_Header.datEntry_Date, dbo.Chart_Of_Accounts.numAccountId, dbo.Chart_Of_Accounts.vcAccountName, 
                      dbo.Chart_Of_Accounts.vcAccountCode, dbo.General_Journal_Header.numDomainID,dbo.General_Journal_Details.numClassID
GO