GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype = 'V' AND NAME='VIEW_JOURNALBS')
DROP VIEW VIEW_JOURNALBS
GO
CREATE VIEW [dbo].[VIEW_JOURNALBS]
WITH SCHEMABINDING 
AS
SELECT     dbo.AccountTypeDetail.numAccountTypeID, dbo.AccountTypeDetail.vcAccountCode, dbo.AccountTypeDetail.vcAccountType, 
                      SUM(ISNULL(dbo.General_Journal_Details.numDebitAmt, 0)) AS Debit, SUM(ISNULL(dbo.General_Journal_Details.numCreditAmt, 0)) AS Credit, 
                      dbo.General_Journal_Header.datEntry_Date, dbo.Chart_Of_Accounts.numAccountId, dbo.Chart_Of_Accounts.vcAccountName, 
                      dbo.Chart_Of_Accounts.vcAccountCode AS COAvcAccountCode, dbo.AccountTypeDetail.numDomainID, COUNT_BIG(*) AS Expr1,
                      ISNULL(dbo.General_Journal_Header.numAccountClass,0) AS numAccountClass
FROM         dbo.Chart_Of_Accounts 
INNER JOIN dbo.Domain ON dbo.Domain.numDomainID = dbo.Chart_Of_Accounts.numDomainId 
INNER JOIN dbo.AccountTypeDetail ON dbo.Chart_Of_Accounts.numParntAcntTypeID = dbo.AccountTypeDetail.numAccountTypeID INNER JOIN
                      dbo.General_Journal_Header INNER JOIN
                      dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId ON 
                      dbo.Chart_Of_Accounts.numAccountId = dbo.General_Journal_Details.numChartAcntId 
                      AND dbo.General_Journal_Header.numDomainID = dbo.Domain.numDomainID 
					  AND dbo.General_Journal_Details.numDomainID = dbo.Domain.numDomainID 
                      AND (dbo.AccountTypeDetail.vcAccountCode LIKE '0101%' OR
                      dbo.AccountTypeDetail.vcAccountCode LIKE '0102%' OR
                      dbo.AccountTypeDetail.vcAccountCode LIKE '0105%')
GROUP BY dbo.AccountTypeDetail.numAccountTypeID, dbo.AccountTypeDetail.vcAccountCode, dbo.AccountTypeDetail.vcAccountType, 
                      dbo.General_Journal_Header.datEntry_Date, dbo.Chart_Of_Accounts.numAccountId, dbo.Chart_Of_Accounts.vcAccountName, 
                      dbo.Chart_Of_Accounts.vcAccountCode, dbo.AccountTypeDetail.numDomainID,dbo.General_Journal_Header.numAccountClass

GO
