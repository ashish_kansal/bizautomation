GO
IF EXISTS ( SELECT * FROM sysobjects WHERE xtype = 'V' AND NAME = 'View_COGSReport' ) 
    DROP VIEW View_COGSReport
GO
CREATE view [dbo].[View_COGSReport]
				as
				select OI.monPrice, IT.vcItemName,It.vcModelID,OI.numUnitHour, OB.vcBizDocID,OB.numOppBizDocsId,OI.numItemCode,OB.dtCreatedDate,OM.numDomainId, isnull(
				(select sum(CG.COGS) from View_COGS CG where CG.numItemCode=OI.numItemCode and CG.numOppBizDocsId=OB.numOppBizDocsId),
				isnull(OT.monVendorCost,0) * OI.numUnitHour) as COGS from OpportunityMaster OM inner join OpportunityBizDocs OB
				on OM.numOppId=OB.numOppId 
				inner join OpportunityBizDocItems OI on OI.numOppBizDocID=OB.numOppBizDocsId
				inner join OpportunityItems OT on OT.numOppId=OM.numOppId and OI.numItemCode=OT.numItemCode
				inner join Item IT on IT.numItemCode = OT.numItemCode
				where OM.tintOppType=1 and OB.bitAuthoritativeBizDocs=1
GO