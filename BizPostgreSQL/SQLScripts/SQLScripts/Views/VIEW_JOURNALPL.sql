GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype = 'V' AND NAME='VIEW_JOURNALPL')
DROP VIEW VIEW_JOURNALPL
GO
CREATE VIEW [dbo].[VIEW_JOURNALPL]
--WITH SCHEMABINDING 
AS
SELECT     dbo.VIEW_ACTPL.numAccountTypeID, dbo.VIEW_ACTPL.vcAccountCode, dbo.VIEW_ACTPL.vcAccountType, 
                      SUM(ISNULL(dbo.General_Journal_Details.numDebitAmt, 0)) AS Debit, SUM(ISNULL(dbo.General_Journal_Details.numCreditAmt, 0)) AS Credit, 
                      dbo.General_Journal_Header.datEntry_Date, dbo.VIEW_COAPL.numAccountId, dbo.VIEW_COAPL.vcAccountName, 
                      dbo.VIEW_COAPL.vcAccountCode AS COAvcAccountCode, dbo.VIEW_ACTPL.numDomainID, COUNT_BIG(*) AS Expr1
FROM         dbo.VIEW_COAPL INNER JOIN
                      dbo.VIEW_ACTPL ON dbo.VIEW_COAPL.numParntAcntTypeID = dbo.VIEW_ACTPL.numAccountTypeID INNER JOIN
                      dbo.General_Journal_Header INNER JOIN
                      dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId ON 
                      dbo.VIEW_COAPL.numAccountId = dbo.General_Journal_Details.numChartAcntId
GROUP BY dbo.VIEW_ACTPL.numAccountTypeID, dbo.VIEW_ACTPL.vcAccountCode, dbo.VIEW_ACTPL.vcAccountType, 
                      dbo.General_Journal_Header.datEntry_Date, dbo.VIEW_COAPL.numAccountId, dbo.VIEW_COAPL.vcAccountName, dbo.VIEW_COAPL.vcAccountCode, 
                      dbo.VIEW_ACTPL.numDomainID

GO

