GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'V '
                    AND NAME = 'View_DynamicCustomColumns' ) 
    DROP VIEW View_DynamicCustomColumns
GO
CREATE VIEW  [dbo].View_DynamicCustomColumns
AS
    
select DFC.intRowNum+1 AS tintOrder,'Cust'+Convert(varchar(10),Fld_Id) as vcDbColumnName,fld_label as vcFieldName,                  
 fld_type as vcAssociatedControlType,isnull(CFM.numListID,0) as numListID,DFC.numFieldID,
1 as bitAllowSorting,1 as bitAllowEdit,1 as bitInlineEdit,isnull(intRowNum,0) as tintRow,isnull(intColumnNum,0) as tintColumn,
ISNULL(V.bitIsRequired,0) as bitIsRequired,ISNULL(V.bitIsEmail,0) as bitIsEmail,ISNULL(V.bitIsAlphaNumeric,0) as bitIsAlphaNumeric,ISNULL(V.bitIsNumeric,0) as bitIsNumeric,ISNULL(V.bitIsLengthValidation,0) as bitIsLengthValidation,
V.intMaxLength,V.intMinLength,ISNULL(V.bitFieldMessage,0) as bitFieldMessage,ISNULL(V.vcFieldMessage,'') vcFieldMessage,
Case when fld_type='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=CFM.numListID and numDomainID=DFC.numDomainID  ),0) else 0 end as ListRelID,
DFC.numFormId,DFC.numUserCntID,DFC.numDomainID,DFC.tintPageType,DFC.numRelCntType,DFC.bitCustom,
DFC.numViewId,DFC.numAuthGroupID,CFM.grp_id,CFM.subgrp,vcURL,vcToolTip,isnull(DFC.boolAOIField,0) as boolAOIField,isnull(L.vcFieldType,'C') as vcFieldType,isnull(DFC.intColumnWidth,0) as intColumnWidth
,DFC.numSubFormID,
	  ISNULL(DFC.numFormFieldGroupId,0) AS numFormFieldGroupId,
	  FGF.vcGroupName,
	  ISNULL(FGF.numOrder,0) AS numOrder,bitDefaultByAdmin
 from DycFormConfigurationDetails DFC 
 JOIN CFW_Fld_Master CFM ON DFC.numFieldID=CFM.fld_id                                                                                    
 left join CFw_Grp_Master CGM on CFM.subgrp=CGM.Grp_id
 LEFT JOIN CFW_Validation V ON V.numFieldID = CFM.Fld_id
 JOIN CFW_Loc_Master L on CFM.GRP_ID=L.Loc_Id LEFT JOIN 
	dbo.FormFieldGroupConfigurarion AS FGF
ON
	DFC.numFormFieldGroupId=FGF.numFormFieldGroupId
 where ISNULL(DFC.bitCustom,0)=1