GO
IF EXISTS (SELECT * FROM sysobjects WHERE xtype = 'V' AND NAME = 'View_Item_Warehouse') 
    DROP VIEW View_Item_Warehouse
GO
CREATE VIEW [dbo].[View_Item_Warehouse]
AS
	SELECT 
		numDomainID
		,numItemID
		,SUM(numOnHand) AS numOnHand
		,SUM(numAllocation) AS numAllocation
		,SUM(numOnOrder) AS numOnOrder
		,SUM(numBackOrder) AS numBackOrder
		,SUM(numReorder) AS numReorder
		,SUM(numOnHand) + SUM(numAllocation) AS numAvailable
	FROM 
		dbo.WareHouseItems
	GROUP BY 
		numDomainID, numItemID
GO