GO
IF EXISTS ( SELECT * FROM sysobjects WHERE xtype = 'V' AND NAME = 'VIEW_JOURNALDEBIT' ) 
    DROP VIEW VIEW_JOURNALDEBIT
GO
CREATE VIEW [dbo].[VIEW_JOURNALDEBIT]
AS
SELECT     dbo.AccountTypeDetail.numAccountTypeID, dbo.AccountTypeDetail.vcAccountCode, dbo.AccountTypeDetail.vcAccountType, 
                      dbo.Chart_Of_Accounts.numAccountId, dbo.Chart_Of_Accounts.numParntAcntTypeID, dbo.Chart_Of_Accounts.vcAccountName, 
                      dbo.Chart_Of_Accounts.dtOpeningDate, dbo.General_Journal_Header.datEntry_Date, SUM(dbo.General_Journal_Details.numDebitAmt) AS Expr2, 
                      dbo.AccountTypeDetail.numDomainID
FROM         dbo.General_Journal_Header INNER JOIN
                      dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId INNER JOIN
                      dbo.AccountTypeDetail INNER JOIN
                      dbo.Chart_Of_Accounts ON dbo.AccountTypeDetail.numAccountTypeID = dbo.Chart_Of_Accounts.numParntAcntTypeID ON 
                      dbo.General_Journal_Details.numChartAcntId = dbo.Chart_Of_Accounts.numAccountId
GROUP BY dbo.AccountTypeDetail.numAccountTypeID, dbo.AccountTypeDetail.vcAccountCode, dbo.AccountTypeDetail.vcAccountType, 
                      dbo.Chart_Of_Accounts.numAccountId, dbo.Chart_Of_Accounts.numParntAcntTypeID, dbo.Chart_Of_Accounts.vcAccountName, 
                      dbo.Chart_Of_Accounts.dtOpeningDate, dbo.General_Journal_Header.datEntry_Date, dbo.AccountTypeDetail.numDomainID
HAVING      (SUM(dbo.General_Journal_Details.numDebitAmt) > 0)
GO