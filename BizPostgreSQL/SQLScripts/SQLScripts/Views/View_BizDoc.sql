GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype = 'V' AND NAME='View_BizDoc')
DROP VIEW View_BizDoc
GO
CREATE VIEW [dbo].[View_BizDoc]
as
select CASE [tintOppType] WHEN 1 THEN 'Sales' WHEN 2 THEN 'Purchase' ELSE '' END as OrderType, 
vcPOppName as [Order Id],OB.monDealAmount As OrderAmount,vcBizDocID as BizDocID,
dbo.[GetDealAmount](OB.numoppID,GETDATE(),OB.[numOppBizDocsId]) As BizDocAmount,
monAmountPaid as BizDocPaidAmount,
OM.numOppId,
numOppBizDocsId,
numBizDocId,
OM.numDomainID,
OB.dtCreatedDate,
tintOppType,
OB.numCreatedBy as BizDocCreatedBy,
OB.dtModifiedDate as BizDocModifiedDate,
numBizDocStatus,
numDivisionId as DivisionID
from 
OpportunityMaster OM
INNER JOIN
OpportunityBizDocs OB
ON
OB.numOppID=OM.numOppID 
--and 
--bitAuthoritativeBizDocs=1
GO

