GO
IF EXISTS ( SELECT * FROM sysobjects WHERE xtype = 'V' AND NAME = 'View_InboxCount' ) 
    DROP VIEW View_InboxCount
GO
-- SELECT * FROM dbo.View_InboxCount where numDomainID=1 and numUserCntID=1 and numNodeId = 0 
CREATE VIEW [dbo].[View_InboxCount]
WITH SCHEMABINDING
AS  
	SELECT  EH.[numDomainID],
            EH.[numUserCntId],
            EH.[numNodeId],
            COUNT_BIG(*) AS RecordCount,
            SUM(ISNULL(CONVERT(FLOAT,[vcSize]),0)) AS SpaceOccupied
    FROM    dbo.[EmailHistory] EH 
    WHERE EH.[chrSource] IN('B','I')
    GROUP BY EH.[numDomainID],EH.[numUserCntId],EH.[numNodeId]
GO