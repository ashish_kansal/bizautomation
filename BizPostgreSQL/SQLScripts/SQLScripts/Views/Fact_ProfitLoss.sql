GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'v'
                    AND NAME = 'Fact_ProfitLoss' ) 
    DROP VIEW Fact_ProfitLoss
GO
/* copy of VIEW_JOURNALPL*/
CREATE VIEW Fact_ProfitLoss
AS  SELECT  General_Journal_Header.numJournal_Id AS JournalKey,
            dbo.VIEW_COAPL.numAccountId AccountKey,
            dbo.VIEW_COAPL.vcAccountName AccountName,
            dbo.VIEW_COAPL.vcAccountCode AS AccountCode,
            dbo.VIEW_ACTPL.numAccountTypeID AccountTypeKey,
            dbo.VIEW_ACTPL.vcAccountType AccountTypeName,
            dbo.VIEW_ACTPL.vcAccountCode AccountTypeCode,
            SUM(ISNULL(dbo.General_Journal_Details.numCreditAmt, 0)) AS Income,
            0 AS Expense,
            dbo.General_Journal_Header.datEntry_Date DateKey,
            dbo.General_Journal_Details.numCustomerId CompanyKey,
            dbo.VIEW_ACTPL.numDomainID DomainKey,
            COUNT_BIG(*) AS Count
    FROM    dbo.VIEW_COAPL
            INNER JOIN dbo.VIEW_ACTPL ON dbo.VIEW_COAPL.numParntAcntTypeID = dbo.VIEW_ACTPL.numAccountTypeID
            INNER JOIN dbo.General_Journal_Header
            INNER JOIN dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId ON dbo.VIEW_COAPL.numAccountId = dbo.General_Journal_Details.numChartAcntId
            INNER JOIN dbo.DivisionMaster DM ON DM.numDivisionID = dbo.General_Journal_Details.numCustomerId
            INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = dbo.General_Journal_Details.numChartAcntId
    WHERE   dbo.General_Journal_Details.numCustomerId > 0
    AND (COA.vcAccountCode LIKE '0103%') 
    GROUP BY dbo.VIEW_ACTPL.numAccountTypeID,
            dbo.VIEW_ACTPL.vcAccountCode,
            dbo.VIEW_ACTPL.vcAccountType,
            dbo.General_Journal_Header.datEntry_Date,
            dbo.VIEW_COAPL.numAccountId,
            dbo.VIEW_COAPL.vcAccountName,
            dbo.VIEW_COAPL.vcAccountCode,
            dbo.VIEW_ACTPL.numDomainID,
            General_Journal_Header.numJournal_Id,
            dbo.General_Journal_Details.numCustomerId

UNION 
SELECT  General_Journal_Header.numJournal_Id AS JournalKey,
            dbo.VIEW_COAPL.numAccountId AccountKey,
            dbo.VIEW_COAPL.vcAccountName AccountName,
            dbo.VIEW_COAPL.vcAccountCode AS AccountCode,
            dbo.VIEW_ACTPL.numAccountTypeID AccountTypeKey,
            dbo.VIEW_ACTPL.vcAccountType AccountTypeName,
            dbo.VIEW_ACTPL.vcAccountCode AccountTypeCode,
            0 AS Income,
            SUM(ISNULL(dbo.General_Journal_Details.numDebitAmt, 0)) AS  Expense,
            dbo.General_Journal_Header.datEntry_Date DateKey,
            dbo.General_Journal_Details.numCustomerId CompanyKey,
            dbo.VIEW_ACTPL.numDomainID DomainKey,
            COUNT_BIG(*) AS Count
    FROM    dbo.VIEW_COAPL
            INNER JOIN dbo.VIEW_ACTPL ON dbo.VIEW_COAPL.numParntAcntTypeID = dbo.VIEW_ACTPL.numAccountTypeID
            INNER JOIN dbo.General_Journal_Header
            INNER JOIN dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId ON dbo.VIEW_COAPL.numAccountId = dbo.General_Journal_Details.numChartAcntId
            INNER JOIN dbo.DivisionMaster DM ON DM.numDivisionID = dbo.General_Journal_Details.numCustomerId
            INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = dbo.General_Journal_Details.numChartAcntId
    WHERE   dbo.General_Journal_Details.numCustomerId > 0
    AND (COA.vcAccountCode LIKE '0104%') 
    GROUP BY dbo.VIEW_ACTPL.numAccountTypeID,
            dbo.VIEW_ACTPL.vcAccountCode,
            dbo.VIEW_ACTPL.vcAccountType,
            dbo.General_Journal_Header.datEntry_Date,
            dbo.VIEW_COAPL.numAccountId,
            dbo.VIEW_COAPL.vcAccountName,
            dbo.VIEW_COAPL.vcAccountCode,
            dbo.VIEW_ACTPL.numDomainID,
            General_Journal_Header.numJournal_Id,
            dbo.General_Journal_Details.numCustomerId
