GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'v'
                    AND NAME = 'Dim_ProfitLossTime' ) 
    DROP VIEW Dim_ProfitLossTime
GO
/* copy of VIEW_JOURNALPL*/
CREATE VIEW Dim_ProfitLossTime
AS  SELECT  General_Journal_Header.numJournal_Id AS JournalKey,
            dbo.General_Journal_Header.datEntry_Date DateKey,
            CONVERT(VARCHAR, DATEPART(yy, datEntry_Date)) AS [Year],
            'Q' + CONVERT(VARCHAR, DATEPART(qq, datEntry_Date)) + '/'
            + CONVERT(VARCHAR, DATEPART(yy, datEntry_Date)) AS Quart,
            CONVERT(VARCHAR, DATEPART(yy, datEntry_Date))
            + CONVERT(VARCHAR, DATEPART(qq, datEntry_Date)) AS QuartKey,
            DATENAME(m, datEntry_Date) + '/' + CONVERT(VARCHAR, DATEPART(yy, datEntry_Date)) AS [Month],
            CONVERT(VARCHAR, DATEPART(yy, datEntry_Date))
            + CONVERT(VARCHAR, DATEPART(qq, datEntry_Date))
            + CONVERT(VARCHAR, DATEPART(m, datEntry_Date)) AS MonthKey,
            'Week ' + CONVERT(VARCHAR, DATEPART(ww, datEntry_Date)) + ' /'
            + CONVERT(VARCHAR, DATEPART(yy, datEntry_Date)) AS [Week],
            DATEPART(ww, datEntry_Date) AS WeekKey,
            CONVERT(VARCHAR, DATEPART(dd, datEntry_Date)) + '/'
            + LEFT(DATENAME(m, datEntry_Date), 3) + '/'
            + CONVERT(VARCHAR, DATEPART(yy, datEntry_Date)) AS [Day],
            dbo.VIEW_ACTPL.numDomainID DomainKey
    FROM    dbo.VIEW_COAPL
            INNER JOIN dbo.VIEW_ACTPL ON dbo.VIEW_COAPL.numParntAcntTypeID = dbo.VIEW_ACTPL.numAccountTypeID
            INNER JOIN dbo.General_Journal_Header
            INNER JOIN dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId ON dbo.VIEW_COAPL.numAccountId = dbo.General_Journal_Details.numChartAcntId
    GROUP BY dbo.VIEW_ACTPL.numAccountTypeID,
            dbo.VIEW_ACTPL.vcAccountCode,
            dbo.VIEW_ACTPL.vcAccountType,
            dbo.General_Journal_Header.datEntry_Date,
            dbo.VIEW_COAPL.numAccountId,
            dbo.VIEW_COAPL.vcAccountName,
            dbo.VIEW_COAPL.vcAccountCode,
            dbo.VIEW_ACTPL.numDomainID,
            General_Journal_Header.numJournal_Id 
