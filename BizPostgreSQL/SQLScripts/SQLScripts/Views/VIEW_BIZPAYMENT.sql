GO
IF EXISTS ( SELECT * FROM sysobjects WHERE xtype = 'V' AND NAME = 'VIEW_BIZPAYMENT' ) 
    DROP VIEW VIEW_BIZPAYMENT
GO
CREATE VIEW [dbo].[VIEW_BIZPAYMENT]
AS  SELECT  numDomainId,
            numBizDocsPaymentDetId,
            dbo.fn_GetPaymentBizDoc(numBizDocsPaymentDetId, numDomainId) AS Narration
    FROM    OpportunityBizDocsDetails 
Go