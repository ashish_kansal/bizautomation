GO
IF EXISTS ( SELECT * FROM sysobjects WHERE xtype = 'V' AND NAME = 'View_GetMasterListItems' ) 
    DROP VIEW View_GetMasterListItems
GO
create view [dbo].[View_GetMasterListItems]
as
SELECT Ld.numDomainID,constFlag, Ld.numListItemID, isnull(vcRenamedListName,vcData) as vcData FROM listdetails Ld        
left join listorder LO 
on Ld.numListItemID= LO.numListItemID 
GO
