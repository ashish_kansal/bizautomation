GO
/****** Object:  View [dbo].[View_BizDocForApproval]    Script Date: 02/15/2010 23:53:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT * FROM sysobjects WHERE xtype = 'V' AND NAME = 'View_BizDocForApproval' ) 
    DROP VIEW View_BizDocForApproval
GO
CREATE VIEW [dbo].[View_BizDocForApproval]
AS  SELECT  CASE [tintOppType]
              WHEN 1 THEN 'Sales'
              WHEN 2 THEN 'Purchase'
              ELSE ''
            END AS OrderType,
            vcPOppName AS [Order Id],
            OB.monDealAmount AS OrderAmount,
            vcBizDocID AS BizDocID,
            OB.monDealAmount AS BizDocAmount,
            monAmountPaid AS BizDocPaidAmount,
            OM.numOppId,
            numOppBizDocsId,
            numBizDocId,
            OM.numDomainID,
            OB.dtCreatedDate
    FROM    OpportunityMaster OM
	INNER JOIN OpportunityBizDocs OB ON OB.numOppID = OM.numOppID
    WHERE   
            bitAuthoritativeBizDocs = 1
            AND OB.numOppBizDocsId NOT IN (
            SELECT DISTINCT
                    numOppBizDocsId
            FROM    BizActionDetails BD INNER JOIN BizDocAction BA ON BD.numBizActionId = BA.numBizActionId
            WHERE  BA.numDomainID = OM.numDomainID ) 
Go                    
