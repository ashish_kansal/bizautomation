GO
IF EXISTS ( SELECT * FROM sysobjects WHERE xtype = 'V' AND NAME = 'VIEW_INVENTORYOPPSALES' ) 
    DROP VIEW VIEW_INVENTORYOPPSALES
GO
CREATE VIEW [dbo].[VIEW_INVENTORYOPPSALES]
AS  
SELECT  numOppBizDocItemID,
            I.numItemCode,
            OM.numDomainID,
            BD.dtCreatedDate AS OppDate,
            I.vcItemName,
            LD.vcData AS ItemClass,
            numUnitHour - ISNULL(numQtyReturned, 0) AS numUnitHour,
            numUnitHour AS SalesUnit,
            BDI.monPrice,
            ( BDI.monPrice * BDI.numUnitHOur ) - ISNULL(( R.numQtyReturned
                                                          * R.monPrice ), 0) AS COGS,
            ISNULL(monAverageCost, monListPrice) AS AvgCost,
            ( ( BDI.monPrice * BDI.numUnitHOur ) - ISNULL(( R.numQtyReturned
                                                            * R.monPrice ), 0) )
            - ( ISNULL(monAverageCost, monListPrice) * numUnitHOur ) AS Profit,
            ISNULL(( R.numQtyReturned * R.monPrice ), 0) AS SalesReturn,
            ISNULL(R.numQtyReturned, 0) AS ReturnQty
    FROM    OpportunityMaster OM
            INNER JOIN OpportunityBizDocs BD ON BD.numOppID = OM.numOppID
            INNER JOIN OpportunityBizDocItems BDI ON BDI.numOppBizDocID = BD.numOppBizDocsID
            LEFT OUTER JOIN [RETURNS] R ON R.numOppItemCode = BDI.numOppItemID
            INNER JOIN Item I ON I.numItemCode = BDI.numItemCode
            LEFT OUTER JOIN [ListDetails] LD 
            ON LD.[numListItemID]= I.[numItemClassification]
--            RIGHT OUTER JOIN ( SELECT   *
--                               FROM     listDetails D
--                               WHERE    d.numListID = 36
--                             ) D ON D.numListItemID = I.numItemClassification
    WHERE   tintOppType = 1 /*and BDI.vcType='Inventory Item'*/
GO    