	
GO
IF EXISTS ( SELECT * FROM sysobjects WHERE xtype = 'V' AND NAME = 'VIEW_JOURNALCHEQ' ) 
    DROP VIEW VIEW_JOURNALCHEQ
GO
CREATE VIEW [dbo].[VIEW_JOURNALCHEQ]
AS  SELECT  numDomainId,numCheckHeaderID,numReferenceID,tintReferenceType,
            --dbo.fn_GetJournalCheq(numCheckId, numDomainId) AS Narration
			' Memo: ' + isnull(vcMemo,'No-Memo' ) + ' Chek No: ' + isnull( CAST(numCheckNo AS VARCHAR(50)),'No-Cheq') + ' Amount: ' + cast(monAmount as varchar(50)) AS Narration
    FROM    CheckHeader 
    --WHERE tintReferenceType=1 
GO