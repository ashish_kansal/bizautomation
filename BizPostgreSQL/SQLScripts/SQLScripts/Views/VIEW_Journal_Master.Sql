GO 
IF EXISTS(SELECT * FROM sysobjects WHERE xtype = 'V' AND NAME='VIEW_Journal_Master')
DROP VIEW VIEW_Journal_Master
GO
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
GO 
CREATE VIEW VIEW_Journal_Master WITH SCHEMABINDING
AS 

/*indexed with with inner joins http://www.sqlservercentral.com/articles/Indexing/indexedviewswithouterjoins/1884/ */ 
    SELECT  GJD.numTransactionId,
			COA.numAccountId,
            COA.vcAccountName AS AccountName,
            COA.vcAccountCode AS AccountCode,
            ATD.numAccountTypeID,
            ATD.vcAccountType AS AccountTypeName,
            ATD.vcAccountCode AS AccountTypeCode,
            ISNULL(GJD.numDebitAmt, 0) AS Debit,
            ISNULL(GJD.numCreditAmt, 0) AS Credit,
            GJH.datEntry_Date,
            GJH.numDomainId,
            GJD.numItemID,
            NULLIF(I.numItemClassification,0) numItemClassification,
            GJD.numcontactid,
			CASE WHEN GJD.numProjectID >0 THEN GJD.numProjectID 
			WHEN GJH.numProjectID>0 THEN GJH.numProjectID END AS numProjectID,
            GJH.numOppId,
            GJH.numOppBizDocsId,
            (CASE WHEN ISNULL(RH.numReturnHeaderID,0) > 0 THEN  RH.numDivisionId ELSE OM.numDivisionId END) numDivisionId,
            OM.numContactId numOrderContactID,
            OM.numAssignedTo,
            GJH.numCategoryHDRID,
            GJH.numClassID,
            GJD.numClassID AS numClassIDDetail,
            CI.numCompanyType RelationshipID,
            CI.vcProfile ProfileID,
			DATEPART(year,GJH.datEntry_Date) YEAR,
			DATEPART(DayOfYear,GJH.datEntry_Date) DayOfYear
    FROM
            dbo.General_Journal_Details AS GJD 
            INNER JOIN dbo.General_Journal_Header AS GJH ON GJH.numJournal_Id = GJD.numJournalId
                                                         AND GJH.numDomainId = GJD.numDomainId 
			INNER JOIN dbo.Chart_Of_Accounts AS COA ON COA.numAccountId = GJD.numChartAcntId
            INNER JOIN dbo.AccountTypeDetail AS ATD ON COA.numParntAcntTypeId = ATD.numAccountTypeID
			INNER JOIN dbo.Item I ON I.numItemCode = ISNULL(GJD.numItemID,-255)
			INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = ISNULL(GJH.numOppId,-255)
			INNER JOIN dbo.DivisionMaster DM ON DM.numDivisionID = OM.numDivisionId
			INNER JOIN dbo.CompanyInfo CI ON CI.numCompanyId = DM.numCompanyID
			LEFT JOIN dbo.ReturnHeader RH ON GJH.numReturnID=RH.numReturnHeaderID
GO
