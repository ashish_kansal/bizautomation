GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype = 'V' AND NAME='VIEW_OPEN_ACTION_ITEMS')
DROP VIEW VIEW_OPEN_ACTION_ITEMS
GO
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
GO 
/*
SELECT * FROM VIEW_OPEN_ACTION_ITEMS where numDOmainID=1 and numDivisionID=1
*/
CREATE VIEW VIEW_OPEN_ACTION_ITEMS
WITH SCHEMABINDING
AS 
SELECT  COUNT_BIG(*) OpenActionItemCount,Comm.numDivisionId,Comm.numDomainID
FROM    dbo.Communication Comm
WHERE   
        ISNULL(Comm.bitclosedflag, 0) = 0
GROUP BY Comm.numDomainID,Comm.numDivisionId

GO 
CREATE UNIQUE CLUSTERED INDEX [VIEW_OPEN_ACTION_ITEMS_Index] ON [dbo].[VIEW_OPEN_ACTION_ITEMS] 
(
	[numDivisionId] ASC,
	[numDomainID] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]