GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='v'AND NAME ='Fact_Orders')
DROP VIEW Fact_Orders
GO
Create VIEW Fact_Orders
AS  SELECT  monDealAmount OrderAmount,
            numOppId OrderKey,
            DM.numDomainId DomainKey,
            ISNULL(DM.numAssignedTo,0) AS SalesExecutiveKey,
            DM.numRecOwner AS RecordOwnerKey,
            DM.bintCreatedDate AS OrderDate
    FROM    dbo.OpportunityMaster OM
            INNER JOIN dbo.DivisionMaster DM ON DM.numDivisionID = OM.numDivisionId
            INNER JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactId = DM.numAssignedTo

            
