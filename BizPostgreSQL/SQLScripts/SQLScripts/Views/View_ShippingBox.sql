SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- created By Anoop Jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE  xtype = 'V' AND NAME ='View_ShippingBox')
DROP VIEW View_ShippingBox
GO
CREATE VIEW View_ShippingBox
AS 

    SELECT  SB.[numBoxID],
			SRI.ShippingReportItemId,
            SB.[vcBoxName],
            SB.[numShippingReportId],
            SB.[fltTotalWeight],
            SB.[fltHeight],
            SB.[fltWidth],
            SB.[fltLength],
			SB.dtDeliveryDate,
			ISNULL(SRI.monShippingRate,ISNULL(SB.monShippingRate,0)) [monShippingRate],
			SB.vcShippingLabelImage,
            SB.vcTrackingNumber,
            SRI.[numOppBizDocItemID],
            OBI.[numItemCode],
			SRI.[fltTotalWeight] fltTotalWeightItem,
            SRI.[fltHeight] fltHeightItem,
            SRI.[fltWidth] fltWidthItem,
            SRI.[fltLength] fltLengthItem,
			OI.numOppId,
			OBI.numOppBizDocID,
            OI.[numoppitemtCode],
--            I.[fltHeight],
--            I.[fltWidth],
--            I.[fltLength],
            I.[fltWeight],
            ISNULL(OI.vcItemName,'') AS [vcItemName],
            OI.[vcModelID],
            ISNULL(OBI.[vcItemDesc],I.[vcItemName]) [vcItemDesc],
			SR.numDomainId,
			SR.tintPayorType,
			ISNULL(SR.vcPayorAccountNo,'') vcPayorAccountNo,
			ISNULL(SR.vcPayorZip,'') vcPayorZip,
			ISNULL(SR.numPayorCountry,0) numPayorCountry,
			ISNULL(SR.vcValue2,0) AS tintServiceType,
			ISNULL(SPT.numNsoftwarePackageTypeID,31) AS [numPackageTypeID],
			ISNULL(SPT.vcPackageName,'-') vcPackageName,
			ISNULL(SRI.intBoxQty,0) AS [intBoxQty],
			ISNULL(SB.numServiceTypeID,0) AS [numServiceTypeID],
			ISNULL(fltDimensionalWeight,0) AS [fltDimensionalWeight],
			CAST(ROUND((ISNULL(SRI.fltTotalWeight,0) * SRI.intBoxQty), 2) AS DECIMAL(9,2)) AS [fltTotalRegularWeight],
			ISNULL(SB.numShipCompany,0) AS [numShipCompany],
			CASE WHEN ISNULL(OI.numUOMId,0) = 0 AND ISNULL(I.[numSaleUnit],0) > 0
			THEN 
				(SELECT ISNULL(vcUnitName,'') FROM dbo.UOM 
				WHERE dbo.UOM.numUOMId = I.[numSaleUnit]
				AND dbo.UOM.numDomainId = SR.numDomainId) 
			WHEN ISNULL(OI.numUOMId,0) > 0 
			THEN
				(SELECT ISNULL(vcUnitName,'') FROM dbo.UOM 
				WHERE dbo.UOM.numUOMId = OI.numUOMId 
				AND dbo.UOM.numDomainId = SR.numDomainId) 
			ELSE
				(
					(CASE WHEN D.[charUnitSystem] = 'E' 
						  THEN (SELECT TOP 1 [U].[vcUnitName] FROM [dbo].[UOM] AS U WHERE [U].[vcUnitName] LIKE 'Unit%' AND [U].[numDomainId] = D.[numDomainId] AND [U].[tintUnitType] = 1) 
						  ELSE (SELECT TOP 1 [U].[vcUnitName] FROM [dbo].[UOM] AS U WHERE [U].[vcUnitName] LIKE 'Unit%' AND [U].[numDomainId] = D.[numDomainId] AND [U].[tintUnitType] = 2) 
					END )
				)
			END AS [vcUnitName],
			 CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4), dbo.fn_UOMConversion(ISNULL(OI.numUOMId, 0), I.numItemCode,SR.numDomainId, ISNULL(I.numBaseUnit, 0)) * OBI.monPrice)) monUnitPrice
    FROM    [ShippingBox] SB
            LEFT JOIN [ShippingReportItems] SRI ON SB.[numBoxID] = SRI.[numBoxID]
			LEFT JOIN dbo.ShippingReport SR ON SRI.numShippingReportId = SR.numShippingReportId
            LEFT JOIN [OpportunityBizDocItems] OBI ON SRI.[numOppBizDocItemID] = OBI.[numOppBizDocItemID]
            LEFT JOIN [OpportunityItems] OI ON OBI.[numOppItemID] = OI.[numoppitemtCode]
			LEFT JOIN  ShippingPackageType SPT ON SB.numPackageTypeID = SPT.ID AND SB.numShipCompany = SPT.numShippingCompanyID
            LEFT JOIN [Item] I ON OBI.[numItemCode] = I.[numItemCode]	
			JOIN [dbo].[Domain] AS D ON I.[numDomainID] = D.[numDomainId]	
GO            
