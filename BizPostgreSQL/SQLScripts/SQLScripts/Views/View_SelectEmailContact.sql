GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype = 'V' AND NAME='View_SelectEmailContact')
DROP VIEW View_SelectEmailContact
GO
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
GO 
CREATE VIEW [dbo].[View_SelectEmailContact] WITH SCHEMABINDING
As
 SELECT ADC.numContactID, C.vcCompanyName+'' AS CompanyName, ''+isnull(ld.vcData,'-') AS Company,                     
               ADC.vcEmail AS vcEmail ,ADC.vcFirstName AS vcFirstName , ADC.vcLastName AS vcLastName ,adc.numDomainID AS numDomainID
FROM dbo.AdditionalContactsInformation ADC INNER JOIN dbo.DivisionMaster DM 
ON adc.numDivisionId = dm.numDivisionID INNER JOIN dbo.CompanyInfo C ON
dm.numCompanyID = c.numCompanyId LEFT JOIN dbo.ListDetails ld ON ld.numListItemID=numCompanyType
--WHERE ADC.vcEmail NOT IN (SELECT vcEmailId FROM dbo.EmailMaster)

UNION 
SELECT 0 AS numContactId,'' AS  CompanyName,'' AS Company ,vcEmailId AS vcEmail ,vcName AS vcFirstName,'' AS vcLastName, numDomainID FROM dbo.emailmaster 
WHERE vcEmailId NOT IN (SELECT                 
               ADC.vcEmail  
FROM dbo.AdditionalContactsInformation ADC INNER JOIN dbo.DivisionMaster DM 
ON adc.numDivisionId = dm.numDivisionID INNER JOIN dbo.CompanyInfo C ON
dm.numCompanyID = c.numCompanyId LEFT JOIN dbo.ListDetails ld ON ld.numListItemID=numCompanyType)
/*
 SELECT ROW_NUMBER() OVER (ORDER BY ADC.numDomainID) AS ID, ADC.numContactID, C.vcCompanyName+'' AS CompanyName,
--  ''+isnull(ld.vcData,'-') AS Company,                     
               ADC.vcEmail AS vcEmail ,ADC.vcFirstName AS vcFirstName , ADC.vcLastName AS vcLastName ,adc.numDomainID AS numDomainID
FROM dbo.AdditionalContactsInformation ADC INNER JOIN dbo.DivisionMaster DM 
ON adc.numDivisionId = dm.numDivisionID INNER JOIN dbo.CompanyInfo C ON
dm.numCompanyID = c.numCompanyId */
--LEFT JOIN dbo.ListDetails ld ON ld.numListItemID=numCompanyType
--WHERE ADC.vcEmail NOT IN (SELECT vcEmailId FROM dbo.EmailMaster)

--UNION 
--SELECT 0 AS numContactId,'' AS  CompanyName,'' AS Company ,vcEmailId AS vcEmail ,vcName AS vcFirstName,'' AS vcLastName, numDomainID FROM dbo.emailmaster 
--WHERE vcEmailId NOT IN (SELECT                 
--               ADC.vcEmail  
--FROM dbo.AdditionalContactsInformation ADC INNER JOIN dbo.DivisionMaster DM 
--ON adc.numDivisionId = dm.numDivisionID INNER JOIN dbo.CompanyInfo C ON
--dm.numCompanyID = c.numCompanyId LEFT JOIN dbo.ListDetails ld ON ld.numListItemID=numCompanyType)

Go
--CREATE UNIQUE CLUSTERED INDEX View_SelectEmailContact_Index ON View_SelectEmailContact (numDomainID)
Go