GO
IF EXISTS ( SELECT * FROM sysobjects WHERE xtype = 'V' AND NAME = 'VIEW_BIZDOCPAYMENT' ) 
    DROP VIEW VIEW_BIZDOCPAYMENT
GO
CREATE VIEW [dbo].[VIEW_BIZDOCPAYMENT]
AS
SELECT     dbo.OpportunityBizDocsPaymentDetails.monAmount, dbo.OpportunityBizDocsPaymentDetails.numCheckNo, 
                      dbo.OpportunityBizDocsPaymentDetails.numBizDocsPaymentDetailsId, dbo.OpportunityBizDocsPaymentDetails.numBizDocsPaymentDetId, 
                      dbo.OpportunityBizDocsDetails.vcReference, dbo.OpportunityBizDocsDetails.vcMemo, dbo.OpportunityBizDocsDetails.numBizDocsId, 
                      dbo.OpportunityBizDocsDetails.numDomainId
FROM         dbo.OpportunityBizDocsPaymentDetails INNER JOIN
                      dbo.OpportunityBizDocsDetails ON 
                      dbo.OpportunityBizDocsPaymentDetails.numBizDocsPaymentDetId = dbo.OpportunityBizDocsDetails.numBizDocsPaymentDetId

GO
