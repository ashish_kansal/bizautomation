GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'v'
                    AND NAME = 'Dim_JournalCompany' ) 
    DROP VIEW Dim_JournalCompany
GO
/* copy of VIEW_JOURNALPL*/
CREATE VIEW Dim_JournalCompany
AS  SELECT  DM.numDomainID AS DomainKey,
			DOM.vcDomainName AS DomainName,
			H.numJournal_Id AS JournalKey,
			DM.numDivisionID AS CompanyKey,
            c.vcCompanyName CompanyName,
            DM.numAssignedTo AS AssigneeContactKey,
            ISNULL(ACI.vcFirstName,'') + ' ' + ISNULL(ACI.vcLastName,'') AS AssigneeContactName,
            DM.numTerID AS TerritoryKey,
            ISNULL(LD.vcData,'NA') AS TerritoryName
    FROM    dbo.General_Journal_Header H
            INNER JOIN dbo.General_Journal_Details D ON H.numJournal_Id = D.numJournalId
            INNER JOIN dbo.DivisionMaster DM ON DM.numDivisionID = D.numCustomerId
            INNER JOIN dbo.CompanyInfo C ON C.numCompanyId = DM.numCompanyID
            INNER JOIN dbo.Domain Dom ON dom.numDomainId = DM.numDomainID
            LEFT JOIN dbo.ListDetails LD ON LD.numListItemID = DM.numTerID
            LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactId = DM.numAssignedTo
   WHERE   D.numCustomerId > 0