GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'v'
                    AND NAME = 'Dim_ProfitLossByProduct' ) 
    DROP VIEW Dim_ProfitLossByProduct
GO
/* copy of VIEW_JOURNALPL*/
CREATE VIEW Dim_ProfitLossByProduct
AS  SELECT  I.numDomainID AS DomainKey,
			I.numItemCode ItemKey,
            I.vcItemName AS ItemName,
            I.numIncomeChartAcntId IncomeAccountKey,
            I.numAssetChartAcntId AssetAccountKey,
            I.numCOGsChartAcntId COGsAccountKey,
            I.numItemClassification ClassificationKey,
            ISNULL(LD.vcData,'NA') AS ItemClassification
    FROM    dbo.Item I 
    LEFT JOIN dbo.ListDetails LD ON LD.numListItemID = I.numItemClassification 
    INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = I.numIncomeChartAcntId
    LEFT JOIN dbo.Chart_Of_Accounts COA1 ON COA1.numAccountId = I.numCOGsChartAcntId