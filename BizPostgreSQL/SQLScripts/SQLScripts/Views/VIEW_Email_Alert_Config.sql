GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype = 'V' AND NAME='VIEW_Email_Alert_Config')
DROP VIEW VIEW_Email_Alert_Config
GO
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
GO 
/*
SELECT * FROM VIEW_Email_Alert_Config where numDOmainID=1 and numDivisionID=1
*/
CREATE VIEW VIEW_Email_Alert_Config 
--WITH SCHEMABINDING
AS 

	SELECT numContactId,numDivisionId,numDomainId,COUNT(*) AS OpenSalesOppCount,0 OpenPurchaseOppCount,0 AS OpenProjectCount
	,0 AS OpenCaseCount,0 AS OpenActionItemCount,0 AS TotalBalanceDue,0 AS ARAccountCount,0 AS APAccountCount,0 AS UnreadEmailCount,0 AS PastSalesOrderValue,0 AS PastPurchaseOrderValue,0 AS CustomFieldValueCount
	 FROM dbo.OpportunityMaster 
	WHERE ISNULL(tintshipped,0)=0 AND numContactId>0 AND tintOppType=1
	GROUP BY numDomainId,numDivisionId,numContactId
UNION 
	SELECT numContactId,numDivisionId,numDomainId,0,COUNT(*) AS OpenPurchaseOppCount,0 AS OpenProjectCount 
	,0 AS OpenCaseCount,0 AS OpenActionItemCount,0 AS TotalBalanceDue,0 AS ARAccountCount,0 AS APAccountCount,0 AS UnreadEmailCount,0 AS PastSalesOrderValue,0 AS PastPurchaseOrderValue,0 AS CustomFieldValueCount
	FROM dbo.OpportunityMaster 
	WHERE ISNULL(tintshipped,0)=0 AND numContactId>0 AND tintOppType=2
	GROUP BY numDomainId,numDivisionId,numContactId
UNION 
	SELECT numCustPrjMgr AS numContactID,numDivisionId,numDomainId,0 ,0, COUNT(*) AS OpenProjectCount
	,0 AS OpenCaseCount,0 AS OpenActionItemCount,0 AS TotalBalanceDue,0 AS ARAccountCount,0 AS APAccountCount,0 AS UnreadEmailCount,0 AS PastSalesOrderValue,0 AS PastPurchaseOrderValue,0 AS CustomFieldValueCount
	FROM dbo.ProjectsMaster where isnull(numProjectStatus,0)=0
	GROUP BY numDomainId,numDivisionId,numCustPrjMgr
UNION 
	SELECT numContactId ,numDivisionId,numDomainId,0 ,0, 0
	,COUNT(*) AS OpenCaseCount,0 AS OpenActionItemCount,0 AS TotalBalanceDue,0 AS ARAccountCount,0 AS APAccountCount,0 AS UnreadEmailCount,0 AS PastSalesOrderValue,0 AS PastPurchaseOrderValue,0 AS CustomFieldValueCount
	FROM dbo.Cases
	WHERE Cases.numstatus<>136 --Not Closed
	GROUP BY numDomainId,numDivisionId,numContactId
UNION 
	SELECT numContactId ,numDivisionId,numDomainId,0 ,0, 0
	,0 AS OpenCaseCount,COUNT(*) AS OpenActionItemCount,0 AS TotalBalanceDue,0 AS ARAccountCount,0 AS APAccountCount,0 AS UnreadEmailCount,0 AS PastSalesOrderValue,0 AS PastPurchaseOrderValue,0 AS CustomFieldValueCount
	FROM dbo.Communication
	WHERE bitClosedFlag = 0
	GROUP BY numDomainId,numDivisionId,numContactId
UNION 
	SELECT numContactId ,numDivisionId,numDomainId,0 ,0, 0
	,0 AS OpenCaseCount,0 AS OpenActionItemCount,SUM(OBD.monDealAmount) AS TotalBalanceDue,0 AS ARAccountCount,0 AS APAccountCount,0 AS UnreadEmailCount,0 AS PastSalesOrderValue,0 AS PastPurchaseOrderValue,0 AS CustomFieldValueCount
	FROM dbo.OpportunityBizDocs OBD INNER JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
	WHERE monAmountPaid< OBD.monDealAmount
	AND OBD.monDealAmount>0
	GROUP BY numDomainId,numDivisionId,numContactId
--UNION 
--	SELECT 0,0,0,0,0,0,
--	,0 AS OpenCaseCount,0 AS OpenActionItemCount,SUM(OBD.monDealAmount) AS TotalBalanceDue,0 AS ARAccountCount,0 AS APAccountCount,0 AS UnreadEmailCount,0 AS PastSalesOrderValue,0 AS PastPurchaseOrderValue,0 AS CustomFieldValueCount
--	* FROM dbo.EmailHistory WHERE chrSource ='I' AND bitIsRead=0
UNION 
	SELECT numContactId ,numDivisionId,numDomainId,0 ,0, 0
	,0 AS OpenCaseCount,0 AS OpenActionItemCount,0 AS TotalBalanceDue,0 AS ARAccountCount,0 AS APAccountCount,0 AS UnreadEmailCount,SUM(monDealAmount) AS PastSalesOrderValue,0 AS PastPurchaseOrderValue,0 AS CustomFieldValueCount
	FROM dbo.OpportunityMaster OM 
	WHERE tintOppType=1 AND tintOppStatus=1
	GROUP BY numDomainId,numDivisionId,numContactId	
UNION 
	SELECT numContactId ,numDivisionId,numDomainId,0 ,0, 0
	,0 AS OpenCaseCount,0 AS OpenActionItemCount,0 AS TotalBalanceDue,0 AS ARAccountCount,0 AS APAccountCount,0 AS UnreadEmailCount,0 AS PastSalesOrderValue,SUM(monDealAmount) AS PastPurchaseOrderValue,0 AS CustomFieldValueCount
	FROM dbo.OpportunityMaster OM 
	WHERE tintOppType=2 AND tintOppStatus=1 
	GROUP BY numDomainId,numDivisionId,numContactId	
UNION 
	SELECT ADC.numContactId ,ADC.numDivisionId,ADC.numDomainId,0 ,0, 0
	,0 AS OpenCaseCount,0 AS OpenActionItemCount,0 AS TotalBalanceDue,0 AS ARAccountCount,0 AS APAccountCount,Sum(Total) AS UnreadEmailCount,0 AS PastSalesOrderValue,0 AS PastPurchaseOrderValue,0 AS CustomFieldValueCount
	FROM View_InboxEmail VE join AdditionalContactsInformation ADC on VE.numDomainId=ADC.numDomainId and VE.numContactId=ADC.numContactId
	GROUP BY ADC.numDomainId,ADC.numDivisionId,ADC.numContactId	

--SELECT COUNT(*) FROM dbo.CFW_FLD_Values WHERE RecId = numDivisionID AND fld_Value =''

	
	
--GO 
--CREATE UNIQUE CLUSTERED INDEX VIEW_Email_Alert_Config_Index ON VIEW_Email_Alert_Config(numDomainID,numDivisionId,numContactid);
--GO 

	
	
	
	
	
	
	