GO
IF EXISTS ( SELECT * FROM sysobjects WHERE xtype = 'V' AND NAME = 'VIEW_COAPL' ) 
    DROP VIEW VIEW_COAPL
GO
CREATE VIEW [dbo].[VIEW_COAPL]
--WITH SCHEMABINDING , VIEW_METADATA
AS
SELECT     numAccountId, numParntAcntTypeID, vcAccountName, vcAccountDescription, numOriginalOpeningBal, numOpeningBal, dtOpeningDate, bitActive, 
                      bitFixed, numDomainId, numListItemID, bitDepreciation, bitOpeningBalanceEquity, monEndingOpeningBal, monEndingBal, dtEndStatementDate, 
                      chBizDocItems, vcAccountCode, bitProfitLoss, numAcntTypeId
FROM         dbo.Chart_Of_Accounts
WHERE     (vcAccountCode LIKE '0103%')  OR(vcAccountCode LIKE '0104%') OR(vcAccountCode LIKE '0106%')
WITH CHECK OPTION

