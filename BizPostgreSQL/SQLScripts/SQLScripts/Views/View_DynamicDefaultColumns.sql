GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'V '
                    AND NAME = 'View_DynamicDefaultColumns' ) 
    DROP VIEW View_DynamicDefaultColumns
GO
CREATE VIEW [dbo].[View_DynamicDefaultColumns]
AS  SELECT  DFM.numFieldID,
            DFM.numModuleID,
            ISNULL(DFV.vcNewFormFieldName,
                   ISNULL(DFFM.vcFieldName, DFM.vcFieldName)) AS vcFieldName,
            DFG.vcNewFieldName AS vcCultureFieldName,
            DFG.numCultureID,
            DFM.vcDbColumnName,
            ISNULL(DFFM.vcPropertyName, DFM.vcPropertyName) AS vcPropertyName,
            DFM.vcLookBackTableName,
            DFM.vcFieldDataType,
            DFM.vcFieldType,
            ISNULL(DFFM.vcAssociatedControlType, DFM.vcAssociatedControlType) AS vcAssociatedControlType,
            ISNULL(DFV.vcToolTip, DFM.vcToolTip) AS vcToolTip,
            DFG.vcToolTip AS vcCultureToolTip,
            ISNULL(DFM.vcListItemType, '') AS vcListItemType,
            DFM.numListID,
            ISNULL(DFFM.PopupFunctionName, DFM.PopupFunctionName) AS PopupFunctionName,
            ISNULL(DFFM.[Order] + 1, DFM.[Order] + 1) AS tintOrder,
            ISNULL(DFFM.tintRow, ISNULL(DFM.tintRow, 0)) AS tintRow,
            ISNULL(DFFM.tintColumn, ISNULL(DFM.tintColumn, 0)) AS tintColumn,
            ISNULL(DFFM.bitInResults, ISNULL(DFM.bitInResults, 0)) AS bitInResults,
            ISNULL(DFFM.bitDeleted, ISNULL(DFM.bitDeleted, 0)) AS bitDeleted,
            ISNULL(DFFM.bitAllowEdit, ISNULL(DFM.bitAllowEdit, 0)) AS bitAllowEdit,
            ISNULL(DFFM.bitDefault, ISNULL(DFM.bitDefault, 0)) AS bitDefault,
            ISNULL(DFFM.bitSettingField, 0) AS bitSettingField,
            ISNULL(DFFM.bitAddField, 0) AS bitAddField,
            ISNULL(DFFM.bitDetailField, 0) AS bitDetailField,
            ISNULL(DFFM.bitAllowSorting, ISNULL(DFM.bitAllowSorting, 0)) AS bitAllowSorting,
            ISNULL(DFFM.bitImport, ISNULL(DFM.bitImport, 0)) AS bitImport,
            ISNULL(DFFM.bitExport, ISNULL(DFM.bitExport, 0)) AS bitExport,
            ISNULL(DFFM.bitAllowFiltering, ISNULL(DFM.bitAllowFiltering, 0)) AS bitAllowFiltering,
            ISNULL(DFFM.bitInlineEdit, ISNULL(DFM.bitInlineEdit, 0)) AS bitInlineEdit,
            ISNULL(DFFM.bitRequired, ISNULL(DFM.bitRequired, 0)) AS bitRequired,
            DFM.intFieldMaxLength,
            ISNULL(DFV.bitIsRequired, 0) bitIsRequired,
            ISNULL(DFV.bitIsEmail, 0) bitIsEmail,
            ISNULL(DFV.bitIsAlphaNumeric, 0) bitIsAlphaNumeric,
            ISNULL(DFV.bitIsNumeric, 0) bitIsNumeric,
            ISNULL(DFV.bitIsLengthValidation, 0) bitIsLengthValidation,
            DFV.intMaxLength,
            DFV.intMinLength,
            ISNULL(DFV.bitFieldMessage, 0) bitFieldMessage,
            ISNULL(DFV.vcFieldMessage, '') vcFieldMessage,
            CASE WHEN DFM.vcAssociatedControlType = 'SelectBox'
                 THEN ISNULL(( SELECT   numPrimaryListID
                               FROM     FieldRelationship
                               WHERE    numSecondaryListID = DFM.numListID
                                        AND numDomainID = D.numDomainID
                             ), 0)
                 ELSE 0
            END AS ListRelID,
            DFFM.numFormId,
            D.numDomainID,
            0 AS bitCustom,
            ISNULL(DFFM.intSectionID, 0) AS intSectionID,
            vcOrigDbColumnName,
            ISNULL(DFFM.bitWorkFlowField, ISNULL(DFM.bitWorkFlowField, 0)) AS bitWorkFlowField,
            ISNULL(DFM.intColumnWidth, 0) AS intColumnWidth,
            ISNULL(DFFM.bitAllowGridColor, 0) AS bitAllowGridColor,
			DFM.vcGroup, 
            ISNULL(DFM.intWFCompare,0) as  intWFCompare,
			DFM.vcWFCompareField


    FROM    DycFormField_Mapping DFFM
            JOIN DycFieldMaster DFM ON DFFM.numModuleID = DFFM.numModuleID
                                       AND DFFM.numFieldID = DFM.numFieldID
            JOIN Domain D ON ( DFM.numDomainID IS NULL
                               OR DFM.numDomainID = D.numDomainID
                             )
            LEFT JOIN DynamicFormField_Validation DFV ON DFV.numDomainID = D.numDomainID
                                                         AND DFFM.numFormID = DFV.numFormID
                                                         AND DFM.numFieldID = DFV.numFormFieldID
            LEFT JOIN DycField_Globalization DFG ON DFG.numDomainID = D.numDomainID
                                                    AND DFM.numFieldID = DFG.numFieldID
                                                    AND DFM.numModuleID = DFG.numModuleID
--where DFM.bitDefault=1
GO