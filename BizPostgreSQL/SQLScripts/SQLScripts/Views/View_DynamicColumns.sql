GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'V '
                    AND NAME = 'View_DynamicColumns' ) 
    DROP VIEW View_DynamicColumns
GO
CREATE VIEW  [dbo].[View_DynamicColumns]
AS
select DFM.numFieldID,DFM.numModuleID,isnull(DFV.vcNewFormFieldName,isnull(DFFM.vcFieldName,DFM.vcFieldName)) as vcFieldName,DFG.vcNewFieldName as vcCultureFieldName,DFG.numCultureID,
DFM.vcDbColumnName,isnull(DFFM.vcPropertyName,DFM.vcPropertyName) as vcPropertyName,DFM.vcLookBackTableName,DFM.vcFieldDataType,DFM.vcFieldType,
isnull(DFFM.vcAssociatedControlType,DFM.vcAssociatedControlType) as vcAssociatedControlType,
isnull(DFV.vcToolTip,DFM.vcToolTip) as vcToolTip,DFG.vcToolTip as vcCultureToolTip,isnull(DFM.vcListItemType,'') as vcListItemType,DFM.numListID,                                                
isnull(DFFM.PopupFunctionName,DFM.PopupFunctionName) as PopupFunctionName,
isnull(DFFM.[Order]+1,DFM.[Order]+1) as  tintOrder,
isnull(DFC.intRowNum,DFM.tintRow) as tintRow,
isnull(DFC.intColumnNum,DFM.tintColumn) as tintColumn,
isnull(DFFM.bitInResults,isnull(DFM.bitInResults,0)) as bitInResults,
isnull(DFFM.bitDeleted,isnull(DFM.bitDeleted,0)) as bitDeleted,
isnull(DFFM.bitAllowEdit,isnull(DFM.bitAllowEdit,0)) as bitAllowEdit,
isnull(DFFM.bitDefault,isnull(DFM.bitDefault,0)) as bitDefault,
isnull(DFFM.bitSettingField,isnull(DFM.bitSettingField,0)) as bitSettingField,
isnull(DFFM.bitAddField,isnull(DFM.bitAddField,0)) as bitAddField,
isnull(DFFM.bitDetailField,isnull(DFM.bitDetailField,0)) as bitDetailField,
isnull(DFFM.bitAllowSorting,isnull(DFM.bitAllowSorting,0)) as bitAllowSorting,
isnull(DFFM.bitImport,isnull(DFM.bitImport,0)) as bitImport,
isnull(DFFM.bitExport,isnull(DFM.bitExport,0)) as bitExport,
isnull(DFFM.bitAllowFiltering,isnull(DFM.bitAllowFiltering,0)) as bitAllowFiltering,
isnull(DFFM.bitInlineEdit,isnull(DFM.bitInlineEdit,0)) as bitInlineEdit,
ISNULL(DFFM.bitRequired,isnull(DFM.bitRequired,0)) as bitRequired,DFM.intFieldMaxLength,
ISNULL(DFV.bitIsRequired,0) bitIsRequired,ISNULL(DFV.bitIsEmail,0) bitIsEmail,ISNULL(DFV.bitIsAlphaNumeric,0) bitIsAlphaNumeric,ISNULL(DFV.bitIsNumeric,0) bitIsNumeric,ISNULL(DFV.bitIsLengthValidation,0) bitIsLengthValidation,
DFV.intMaxLength,DFV.intMinLength,ISNULL(DFV.bitFieldMessage,0) bitFieldMessage,ISNULL(DFV.vcFieldMessage,'') vcFieldMessage,
Case when DFM.vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=DFM.numListID and numDomainID=DFC.numDomainID),0) else 0 end as ListRelID,
DFC.numFormId,DFC.numUserCntID,DFC.numDomainID,DFC.tintPageType,DFC.numRelCntType,ISNULL(DFC.bitCustom,0) as bitCustom,
DFC.numViewId,DFC.numAuthGroupID,ISNULL(DFC.boolAOIField,0) as boolAOIField,
isnull(DFFM.intSectionID,0) as intSectionID,vcOrigDbColumnName,isnull(DFFM.bitWorkFlowField,isnull(DFM.bitWorkFlowField,0)) as bitWorkFlowField
,isnull(DFC.intColumnWidth,isnull(DFM.intColumnWidth,0)) as intColumnWidth,ISNULL(DFFM.bitAllowGridColor,0) AS bitAllowGridColor,
ISNULL(DFC.numSubFormID,0) AS numSubFormID,
	  ISNULL(DFC.numFormFieldGroupId,0) AS numFormFieldGroupId,
	  FGF.vcGroupName,
	  ISNULL(FGF.numOrder,0) AS numOrder,ISNULL(DFC.bitDefaultByAdmin,0) AS bitDefaultByAdmin
 from DycFormConfigurationDetails DFC 
JOIN DycFormField_Mapping DFFM on DFC.numFormID=DFFM.numFormID and DFC.numFieldID=DFFM.numFieldID
JOIN DycFieldMaster DFM on DFFM.numModuleID=DFFM.numModuleID and DFC.numFieldID=DFM.numFieldID
left join DynamicFormField_Validation DFV on DFV.numDomainID=DFC.numDomainID AND DFFM.numFormID=DFV.numFormID AND DFM.numFieldID=DFV.numFormFieldID
left join DycField_Globalization DFG on DFG.numDomainID=DFC.numDomainID AND DFM.numFieldID=DFG.numFieldID AND DFM.numModuleID=DFG.numModuleID
LEFT JOIN 
	dbo.FormFieldGroupConfigurarion AS FGF
ON
	DFC.numFormFieldGroupId=FGF.numFormFieldGroupId
where ISNULL(DFC.bitCustom,0)=0


