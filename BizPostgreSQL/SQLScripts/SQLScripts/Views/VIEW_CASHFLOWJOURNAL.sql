GO
IF EXISTS ( SELECT * FROM sysobjects WHERE xtype = 'V' AND NAME = 'VIEW_CASHFLOWJOURNAL' ) 
    DROP VIEW VIEW_CASHFLOWJOURNAL
GO
CREATE VIEW [dbo].[VIEW_CASHFLOWJOURNAL]
AS  
	SELECT  numJournalID,
            numTranSactionID,
            GJD.numDomainID,
            GJH.datEntry_Date,
            ISNULL(SUM(ISNULL(numDebitAmt, 0)), 0) AS DEBIT,
            ISNULL(SUM(ISNULL(numCreditAmt, 0)), 0) AS CREDIT,
            ISNULL(GJH.numAccountClass,0) AS numAccountClass
    FROM    General_Journal_Details GJD
            INNER JOIN Chart_Of_Accounts COA ON GJD.numChartAcntID = COA.numAccountId
                                                AND ( COA.vcAccountCode LIKE '01010101%'
                                                      OR COA.vcAccountCode LIKE '01010102%'
                                                    )
            INNER JOIN General_Journal_Header GJH ON GJD.numJournalID = GJH.numJournal_ID
    GROUP BY numJournalID,
            numTranSactionID,
            GJD.numDomainID,
            GJH.datEntry_Date,GJH.numAccountClass
GO