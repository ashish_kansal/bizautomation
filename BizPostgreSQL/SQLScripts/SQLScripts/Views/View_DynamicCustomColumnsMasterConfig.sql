GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='v'AND NAME ='View_DynamicCustomColumnsMasterConfig')
DROP VIEW View_DynamicCustomColumnsMasterConfig
GO
-- select * from Dim_OrderDate order by monthkey desc
CREATE VIEW [dbo].[View_DynamicCustomColumnsMasterConfig]
AS  
	SELECT        
		DFC.intRowNum + 1 AS tintOrder, 
		'Cust' + CONVERT(varchar(10), CFM.Fld_id) AS vcDbColumnName, 
		CFM.Fld_label AS vcFieldName, 
		CFM.Fld_type AS vcAssociatedControlType, 
		ISNULL(CFM.numlistid, 0) AS numListID, 
		DFC.numFieldId, 1 AS bitAllowSorting, 
		1 AS bitAllowEdit, 1 AS bitInlineEdit, 
		ISNULL(DFC.intRowNum, 0) AS tintRow, 
		ISNULL(DFC.intColumnNum, 0) AS tintColumn, 
		ISNULL(V.bitIsRequired, 0) AS bitIsRequired, 
		ISNULL(V.bitIsEmail, 0) AS bitIsEmail, 
		ISNULL(V.bitIsAlphaNumeric, 0) AS bitIsAlphaNumeric, 
		ISNULL(V.bitIsNumeric, 0) AS bitIsNumeric, 
		ISNULL(V.bitIsLengthValidation, 0) AS bitIsLengthValidation, 
		V.intMaxLength, 
		V.intMinLength, 
		ISNULL(V.bitFieldMessage, 0) AS bitFieldMessage, 
		ISNULL(V.vcFieldMessage, '') AS vcFieldMessage, 
		CASE 
		WHEN fld_type = 'SelectBox' 
		THEN isnull((SELECT numPrimaryListID FROM FieldRelationship WHERE numSecondaryListID = CFM.numListID AND numDomainID = DFC.numDomainID), 0) 
		ELSE 0 
		END AS ListRelID, 
		DFC.numFormId, 
		DFC.intRowNum,
	    DFC.intColumnNum,
		--DFC.numUserCntID, 
		DFC.numDomainId, 
		DFC.tintPageType, 
		DFC.numRelCntType, 
		DFC.bitCustom, 
		--DFC.numViewId, 
		DFC.numGroupID AS numAuthGroupID, 
		DFC.bitGridConfiguration,
		CFM.Grp_id, 
		CFM.subgrp, 
		CFM.vcURL, 
		CFM.vcToolTip, 
		--ISNULL(DFC.boolAOIField, 0) AS boolAOIField, 
		ISNULL(L.vcFieldType, 'C') AS vcFieldType, 
		0 AS intColumnWidth,
	  ISNULL(DFC.numFormFieldGroupId,0) AS numFormFieldGroupId,
	  FGF.vcGroupName,
	  ISNULL(FGF.numOrder,0) AS numOrder
	FROM            
		dbo.BizFormWizardMasterConfiguration AS DFC 
	INNER JOIN
		dbo.CFW_Fld_Master AS CFM 
	ON 
		DFC.numFieldId = CFM.Fld_id 
	LEFT OUTER JOIN
		dbo.CFw_Grp_Master AS CGM 
	ON 
		CFM.subgrp = CGM.Grp_id 
	LEFT OUTER JOIN
		dbo.CFW_Validation AS V 
	ON 
		V.numFieldID = CFM.Fld_id 
	INNER JOIN
		dbo.CFW_Loc_Master AS L 
	ON 
		CFM.Grp_id = L.Loc_id
	LEFT JOIN 
		dbo.FormFieldGroupConfigurarion AS FGF
	ON
		DFC.numFormFieldGroupId=FGF.numFormFieldGroupId
	WHERE        
		(ISNULL(DFC.bitCustom, 0) = 1)
	