GO
IF EXISTS ( SELECT * FROM sysobjects WHERE xtype = 'V' AND NAME = 'View_COGS' ) 
    DROP VIEW View_COGS
GO
CREATE view View_COGS
as
select OM.numDomainId,OB.numOppBizDocsId,OB.dtCreatedDate, OL.numChildOppID,OI.numItemCode,OT.monPrice * OI.numUnitHour AS COGS from OpportunityMaster OM inner join 
				OpportunityBizDocs OB on OM.numOppId=OB.numOppId inner join
				OpportunityBizDocItems OI on OI.numOppBizDocID=OB.numOppBizDocsId inner join 
				OpportunityLinking OL on OL.numParentOppID=OB.numOppId inner join 
				OpportunityItems OT on OT.numItemCode=OI.numItemCode and OT.numOppId=OL.numChildOppID inner join 
				Item IT on IT.numItemCode=OT.numItemCode
				where OB.bitAuthoritativeBizDocs=1 
				and OM.tintOppType=1 
				
UNION
select OM.numDomainId,OB.numOppBizDocsId,OB.dtCreatedDate,OL.numParentOppID ,OI.numItemCode,OT.monPrice* OI.numUnitHour AS COGS from OpportunityMaster OM inner join 
				OpportunityBizDocs OB on OM.numOppId=OB.numOppId inner join
				OpportunityBizDocItems OI on OI.numOppBizDocID=OB.numOppBizDocsId inner join 
				OpportunityLinking OL on OL.numChildOppID=OB.numOppId inner join 
				OpportunityItems OT on OT.numItemCode=OI.numItemCode and OT.numOppId=OL.numParentOppID inner join 
				Item IT on IT.numItemCode=OT.numItemCode 
				where  OM.tintOppType=1 
				and OB.bitAuthoritativeBizDocs=1
GO
				