GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype = 'V' AND NAME='View_InboxEmail')
DROP VIEW View_InboxEmail
GO
--select * from View_InboxEmail where numContactId=84135
CREATE VIEW [dbo].[View_InboxEmail] WITH SCHEMABINDING
AS
SELECT     EH.numDomainID, EM.numContactId, COUNT_BIG(*) AS Total
FROM   
	dbo.EmailMaster EM  INNER JOIN dbo.EmailHistory AS EH 
	ON EM.numEmailId = EH.numEmailID
WHERE     EH.tintType=1 AND (EH.bitIsRead = 0)
AND EM.numContactId > 0
GROUP BY EH.numDomainID, EM.numContactId;

GO 
CREATE UNIQUE CLUSTERED INDEX [View_InboxEmail_Index] ON [dbo].[View_InboxEmail] 
(
	[numContactId] ASC,
	[numDomainID] ASC
)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]
GO
