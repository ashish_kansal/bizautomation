GO
IF EXISTS ( SELECT * FROM sysobjects WHERE xtype = 'V' AND NAME = 'View_BizDocsSummary' ) 
    DROP VIEW View_BizDocsSummary
GO
CREATE view View_BizDocsSummary
as

SELECT  
				COA.numDomainID,
				c.vcCompanyName,
				COA.vcAccountName,
				tintOppType,
				OppBizDocs.vcBizDocId,
				Opp.numOppId,
                LD.vcData AS BizDocType,
                Opp.vcPOppName AS Name,
                Opp.monPAmount AS TotalAmt,
                OppBizDocs.[monDealAmount] AS TotalBizDocAmt,
                ISNULL(SUM(OppBizDocsDet.monAmount),0) AS AmountPaid,
                CASE WHEN tintOppType = 1 THEN 'S.O'
                     ELSE CASE WHEN tintOppType = 2 THEN 'P.O'
                          END
                END AS OppType,
--   dateadd(minute,-@ClientTimeZoneOffset,OppBizDocsDet.dtCreationDate) as datePaid,            
                isnull(Opp.monPAmount,0)-ISNULL(SUM(OppBizDocsDet.monAmount),0)  AS BalanceAmt,
                --dbo.FormatedDateFromDate(DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0)) ELSE 0 END,OppBizDocs.[dtFromDate]),COA.numDomainID) AS DueDate,z
                dbo.FormatedDateFromDate(DATEADD(day,CASE 
														WHEN Opp.bitBillingTerms = 1 
														THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				 WHERE numTermsID = ISNULL(Opp.intBillingDays,0)), 0)) 
														ELSE 0 
													 END,OppBizDocs.[dtFromDate]),COA.numDomainID) AS DueDate,
                CASE when  isnull(Opp.monPAmount,0)-ISNULL(SUM(OppBizDocsDet.monAmount),0) = 0 THEN 0
					WHEN DATEDIFF(Day,
                                   DATEADD(day,
                                           CASE WHEN Opp.bitBillingTerms = 1
                                                --THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0)) 
                                                THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)), 0))  
                                                ELSE 0
                                           END, OppBizDocs.[dtCreatedDate]),
                                   GETDATE()) > 0 THEN 1
                     ELSE 0
                END AS Status,
                OppBizDocs.[numOppBizDocsId],
                dbo.FormatedDateFromDate(OppBizDocs.dtCreatedDate,COA.numDomainID) dtCreatedDate
        FROM    OpportunityMaster Opp
                INNER JOIN OpportunityBizDocs OppBizDocs ON OppBizDocs.numOppId = Opp.numOppId
                LEFT OUTER JOIN OpportunityBizDocsDetails OppBizDocsDet ON OppBizDocsDet.numBizDocsId = OppBizDocs.numOppBizDocsId
                INNER JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId
                INNER JOIN DivisionMaster Div ON Opp.numDivisionId = Div.numDivisionID
                                                 AND ADC.numDivisionId = Div.numDivisionID
                INNER JOIN CompanyInfo C ON Div.numCompanyID = C.numCompanyId
                INNER JOIN ListDetails LD ON OppBizDocs.numBizDocId = LD.numListItemID
				INNER JOIN COARelationShips CR on CR.numRelationshipID=C.numCompanyType 
				INNER JOIN Chart_of_Accounts COA on COA.numAccountId = Case tintOppType when 1 then CR.numARAccountId when 2 then CR.numAPAccountID end
				INNER JOIN (SELECT numAuthoritativePurchase as AuthBizDocId,numDomainID from AuthoritativeBizDocs
					UNION 	SELECT numAuthoritativeSales AS AuthBizDocId ,numDomainID from AuthoritativeBizDocs) AuthBizDoc
				ON OppBizDocs.numBizDocId =	AuthBizDoc.AuthBizDocId
							 
        WHERE   OPP.numDomainId = COA.numDomainID
--                AND Opp.tintOppStatus = 1
                AND Opp.numDivisionId = div.numDivisionId
                AND Div.numDomainID = COA.numDomainID
				AND AuthBizDoc.numDomainID=COA.numDomainID
        GROUP BY Opp.[numOppId],
                [vcData],
                Opp.vcPOppName,
                [monPAmount],
                Opp.[tintOppType],
                Opp.bitBillingTerms,
                Opp.[intBillingDays],
                OppBizDocs.[dtCreatedDate],
                OppBizDocs.[numOppBizDocsId],
                OppBizDocs.[monDealAmount],
				c.vcCompanyName,
				OppBizDocs.vcBizDocId,
				COA.numDomainID,
				COA.vcAccountName,
				tintOppType,
				OppBizDocs.[dtFromDate]
GO