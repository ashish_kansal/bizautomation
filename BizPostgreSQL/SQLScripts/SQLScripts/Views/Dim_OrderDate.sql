GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='v'AND NAME ='Dim_OrderDate')
DROP VIEW Dim_OrderDate
GO
-- select * from Dim_OrderDate order by monthkey desc
CREATE VIEW [dbo].[Dim_OrderDate]
AS  SELECT DISTINCT
			numOppId OrderKey,
            bintCreatedDate OrderDate,
            CONVERT(VARCHAR, DATEPART(yy, bintCreatedDate)) AS [Year],
            'Q' + CONVERT(VARCHAR, DATEPART(qq, bintCreatedDate)) + '/'
            + CONVERT(VARCHAR, DATEPART(yy, bintCreatedDate)) AS Quart,
            CONVERT(VARCHAR, DATEPART(yy, bintCreatedDate)) + CONVERT(VARCHAR,DATEPART(qq, bintCreatedDate)) AS QuartKey,
            DATENAME(m, bintCreatedDate) + '/' + CONVERT(VARCHAR, DATEPART(yy, bintCreatedDate)) AS [Month],
            CONVERT(VARCHAR, DATEPART(yy, bintCreatedDate)) + CONVERT(VARCHAR,DATEPART(qq, bintCreatedDate)) + CONVERT(VARCHAR,DATEPART(m, bintCreatedDate)) AS MonthKey,
            'Week ' + CONVERT(VARCHAR, DATEPART(ww, bintCreatedDate)) + ' /'
            + CONVERT(VARCHAR, DATEPART(yy, bintCreatedDate)) AS [Week],
            DATEPART(ww, bintCreatedDate) AS WeekKey,
            CONVERT(VARCHAR, DATEPART(dd, bintCreatedDate)) + '/'
            + LEFT(DATENAME(m, bintCreatedDate), 3) + '/'
            + CONVERT(VARCHAR, DATEPART(yy, bintCreatedDate)) AS [Day],
            CONVERT(CHAR, bintCreatedDate, 112) AS DayKey
    FROM    dbo.OpportunityMaster