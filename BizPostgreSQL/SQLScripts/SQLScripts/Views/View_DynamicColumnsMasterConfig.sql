GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='v'AND NAME ='View_DynamicColumnsMasterConfig')
DROP VIEW View_DynamicColumnsMasterConfig
GO
-- select * from Dim_OrderDate order by monthkey desc
CREATE VIEW [dbo].[View_DynamicColumnsMasterConfig]
AS  
	SELECT 
	  DFM.numFieldId, 
	  DFM.numModuleID, 
	  ISNULL(DFV.vcNewFormFieldName, 
	  ISNULL(DFFM.vcFieldName, DFM.vcFieldName)) AS vcFieldName, 
      DFG.vcNewFieldName AS vcCultureFieldName, 
	  DFG.numCultureID, 
	  DFM.vcDbColumnName, 
	  ISNULL(DFFM.vcPropertyName, DFM.vcPropertyName) AS vcPropertyName, 
	  DFM.vcLookBackTableName, 
	  DFM.vcFieldDataType, 
	  DFM.vcFieldType, 
	  ISNULL(DFFM.vcAssociatedControlType, DFM.vcAssociatedControlType) AS vcAssociatedControlType, 
	  ISNULL(DFV.vcToolTip, DFM.vcToolTip) AS vcToolTip, 
	  DFG.vcToolTip AS vcCultureToolTip, 
	  ISNULL(DFM.vcListItemType, '') AS vcListItemType, 
	  DFM.numListID, 
	  ISNULL(DFFM.PopupFunctionName, DFM.PopupFunctionName) AS PopupFunctionName, 
	  ISNULL(DFFM.[order] + 1, DFM.[order] + 1) AS tintOrder, 
	  ISNULL(DFC.intRowNum, DFM.tintRow) AS tintRow, 
	  ISNULL(DFC.intColumnNum, DFM.tintColumn) AS tintColumn, 
      ISNULL(DFFM.bitInResults, ISNULL(DFM.bitInResults, 0)) AS bitInResults, ISNULL(DFFM.bitDeleted, ISNULL(DFM.bitDeleted, 0)) AS bitDeleted, 
      ISNULL(DFFM.bitAllowEdit, ISNULL(DFM.bitAllowEdit, 0)) AS bitAllowEdit, ISNULL(DFFM.bitDefault, ISNULL(DFM.bitDefault, 0)) AS bitDefault, 
      ISNULL(DFFM.bitSettingField, ISNULL(DFM.bitSettingField, 0)) AS bitSettingField, ISNULL(DFFM.bitAddField, ISNULL(DFM.bitAddField, 0)) AS bitAddField, 
      ISNULL(DFFM.bitDetailField, ISNULL(DFM.bitDetailField, 0)) AS bitDetailField, ISNULL(DFFM.bitAllowSorting, ISNULL(DFM.bitAllowSorting, 0)) AS bitAllowSorting, 
      ISNULL(DFFM.bitImport, ISNULL(DFM.bitImport, 0)) AS bitImport, ISNULL(DFFM.bitExport, ISNULL(DFM.bitExport, 0)) AS bitExport, ISNULL(DFFM.bitAllowFiltering, 
      ISNULL(DFM.bitAllowFiltering, 0)) AS bitAllowFiltering, 
	  ISNULL(DFFM.bitInlineEdit, 
	  ISNULL(DFM.bitInlineEdit, 0)) AS bitInlineEdit, 
	  ISNULL(DFFM.bitRequired, 
      ISNULL(DFM.bitRequired, 0)) AS bitRequired, 
	  DFM.intFieldMaxLength, 
	  ISNULL(DFV.bitIsRequired, 0) AS bitIsRequired, 
	  ISNULL(DFV.bitIsEmail, 0) AS bitIsEmail, 
      ISNULL(DFV.bitIsAlphaNumeric, 0) AS bitIsAlphaNumeric, 
	  ISNULL(DFV.bitIsNumeric, 0) AS bitIsNumeric, 
	  ISNULL(DFV.bitIsLengthValidation, 0) AS bitIsLengthValidation, 
	  DFV.intMaxLength, 
	  DFV.intMinLength, 
	  ISNULL(DFV.bitFieldMessage, 0) AS bitFieldMessage, 
	  ISNULL(DFV.vcFieldMessage, '') AS vcFieldMessage, 
	  CASE 
		WHEN DFM.vcAssociatedControlType = 'SelectBox' 
		THEN isnull ((SELECT numPrimaryListID FROM FieldRelationship WHERE numSecondaryListID = DFM.numListID AND numDomainID = DFC.numDomainID), 0) 
		ELSE 0 
	  END AS ListRelID, 
	  DFC.numFormId, 
	  DFC.intRowNum,
	  DFC.intColumnNum,
	  --DFC.numUserCntID, 
      DFC.numDomainId, 
	  DFC.tintPageType, 
	  DFC.numRelCntType, 
	  ISNULL(DFC.bitCustom, 0) AS bitCustom, 
	  --DFC.numViewId, 
	  DFC.numGroupID AS numAuthGroupID, 
	  DFC.bitGridConfiguration,
      --ISNULL(DFC.boolAOIField, 0) AS boolAOIField, 
	  ISNULL(DFFM.intSectionID, 0) AS intSectionID, 
	  DFM.vcOrigDbColumnName, 
	  ISNULL(DFFM.bitWorkFlowField, ISNULL(DFM.bitWorkFlowField, 0)) AS bitWorkFlowField, 
	  ISNULL(DFM.intColumnWidth, 0) AS intColumnWidth, 
      ISNULL(DFFM.bitAllowGridColor, 0) AS bitAllowGridColor,
	  ISNULL(DFC.numFormFieldGroupId,0) AS numFormFieldGroupId,
	  FGF.vcGroupName,
	  ISNULL(FGF.numOrder,0) AS numOrder
FROM            
	dbo.BizFormWizardMasterConfiguration AS DFC 
INNER JOIN
    dbo.DycFormField_Mapping AS DFFM 
ON 
	DFC.numFormId = DFFM.numFormID AND 
	DFC.numFieldId = DFFM.numFieldID 
INNER JOIN
    dbo.DycFieldMaster AS DFM 
ON 
	DFFM.numModuleID = DFFM.numModuleID AND 
	DFC.numFieldId = DFM.numFieldId 
LEFT OUTER JOIN
    dbo.DynamicFormField_Validation AS DFV 
ON 
	DFV.numDomainId = DFC.numDomainId AND 
	DFFM.numFormID = DFV.numFormId AND 
    DFM.numFieldId = DFV.numFormFieldId 
LEFT OUTER JOIN
    dbo.DycField_Globalization AS DFG 
ON 
	DFG.numDomainID = DFC.numDomainId AND 
	DFM.numFieldId = DFG.numFieldID AND 
    DFM.numModuleID = DFG.numModuleID
LEFT JOIN 
	dbo.FormFieldGroupConfigurarion AS FGF
ON
	DFC.numFormFieldGroupId=FGF.numFormFieldGroupId
WHERE        
	(ISNULL(DFC.bitCustom, 0) = 0)