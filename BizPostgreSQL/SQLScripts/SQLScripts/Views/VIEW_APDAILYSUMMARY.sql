GO
IF EXISTS (SELECT * FROM sysobjects WHERE xtype = 'V' AND NAME = 'VIEW_APDAILYSUMMARY' ) 
    DROP VIEW VIEW_APDAILYSUMMARY
GO
CREATE VIEW [dbo].[VIEW_APDAILYSUMMARY]
AS
SELECT  DN.vcDomainCode, GJH.numDomainId,COA.numAccountId,COA.VCACCOUNTCODE,vcAccountName,datEntry_Date,
sum(isnull(numDebitAmt,0)) as Debit,
sum(isnull(numCreditAmt,0)) as Credit,
sum(isnull(numDebitAmt,0))- sum(isnull(numCreditAmt,0)) AS Total
FROM General_Journal_Header GJH INNER JOIN 
			GENERAL_JOURNAL_DETAILS GJD ON GJH.numJournal_Id=GJD.numJournalId  
			INNER JOIN
			CHART_OF_ACCOUNTS COA ON COA.numAccountId=GJD.numChartAcntId
			INNER JOIN Domain DN ON DN.numDomainID = COA.numDomainID			
			AND COA.VCACCOUNTCODE LIKE '01020102%'
GROUP BY DN.vcDomainCode,GJH.numDomainId,COA.numAccountId,COA.VCACCOUNTCODE,vcAccountName,datEntry_Date
GO