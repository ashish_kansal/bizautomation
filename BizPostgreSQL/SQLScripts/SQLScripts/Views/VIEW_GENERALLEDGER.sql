	
GO
IF EXISTS ( SELECT * FROM sysobjects WHERE xtype = 'V' AND NAME = 'VIEW_GENERALLEDGER' ) 
    DROP VIEW VIEW_GENERALLEDGER
GO
CREATE VIEW [dbo].[VIEW_GENERALLEDGER]
AS
SELECT     dbo.General_Journal_Header.numJournal_Id, dbo.General_Journal_Header.datEntry_Date, dbo.General_Journal_Header.varDescription, 
                      ISNULL(dbo.VIEW_BIZPAYMENT.Narration,dbo.General_Journal_Header.varDescription) AS BizPayment, dbo.General_Journal_Details.numTransactionId,
                     CASE WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN ' Chek No: ' + isnull( CAST(CH.numCheckNo AS VARCHAR(50)),'No-Cheq') ELSE  dbo.VIEW_JOURNALCHEQ.Narration END AS CheqNo, 
                      dbo.General_Journal_Header.numDomainId, 'Biz Doc Id: ' + dbo.OpportunityBizDocs.vcBizDocID AS BizDocID, 
                      dbo.General_Journal_Header.numOppBizDocsId, dbo.General_Journal_Details.vcReference AS TranRef, 
                      dbo.General_Journal_Details.varDescription AS TranDesc, dbo.General_Journal_Details.numDebitAmt, dbo.General_Journal_Details.numCreditAmt, 
                      dbo.Chart_Of_Accounts.numAccountId, dbo.Chart_Of_Accounts.vcAccountName,
                      CASE WHEN isnull(dbo.General_Journal_Header.numCheckHeaderID, 0) <> 0 THEN + 'Checks' 
                           WHEN isnull(dbo.General_Journal_Header.numCashCreditCardId, 0) <> 0 AND dbo.CashCreditCardDetails.bitMoneyOut = 0 THEN 'Cash' 
                           WHEN isnull(dbo.General_Journal_Header.numCashCreditCardId, 0) <> 0 AND dbo.CashCreditCardDetails.bitMoneyOut = 1 AND dbo.CashCreditCardDetails.bitChargeBack = 1 THEN 'Charge' 
                           WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 1 THEN 'Deposit' 
						   WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 2 THEN 'Receved Pmt' 
						   --WHEN isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) <> 0 AND dbo.OpportunityMaster.tintOppType = 1 THEN 'Receive Amt' 
						   --WHEN isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) <> 0 AND dbo.OpportunityMaster.tintOppType = 2 THEN 'Vendor Amt' 
						      WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
									dbo.OpportunityMaster.tintOppType = 1 THEN 'BizDocs Invoice' 
						   WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
									dbo.OpportunityMaster.tintOppType = 2 THEN 'BizDocs Purchase' 
						   WHEN isnull(dbo.General_Journal_Header.numCategoryHDRID, 0) <> 0 THEN 'Time And Expenses' 
						   WHEN ISNULL(General_Journal_Header.numBillId,0) <>0 AND isnull(BH.bitLandedCost,0)=1 then 'Landed Cost' 
						   WHEN isnull(dbo.General_Journal_Header.numBillID, 0) <> 0 THEN 'Bill'
						   WHEN isnull(dbo.General_Journal_Header.numBillPaymentID, 0) <> 0 THEN 'Pay Bill'
						   WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN 
									CASE WHEN RH.tintReturnType=1 AND RH.tintReceiveType=1 THEN 'Sales Return Refund'
									     WHEN RH.tintReturnType=1 AND RH.tintReceiveType=2 THEN 'Sales Return Credit Memo'
									     WHEN RH.tintReturnType=2 AND RH.tintReceiveType=2 THEN 'Purchase Return Credit Memo'
									     WHEN RH.tintReturnType=3 AND RH.tintReceiveType=2 THEN 'Credit Memo'
									     WHEN RH.tintReturnType=4 AND RH.tintReceiveType=1 THEN 'Refund Against Credit Memo' END 
						   WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) = 0 THEN 'PO Fulfillment'
						   WHEN dbo.General_Journal_Header.numJournal_Id <> 0 THEN 'Journal' END AS TransactionType,
						    dbo.CompanyInfo.vcCompanyName AS CompanyName, dbo.General_Journal_Header.numCheckHeaderID, 
                      dbo.General_Journal_Header.numCashCreditCardId, dbo.General_Journal_Header.numOppId, dbo.General_Journal_Header.numDepositId, 
                      dbo.General_Journal_Header.numCategoryHDRID, dbo.TimeAndExpense.tintTEType, dbo.TimeAndExpense.numCategory, 
                      dbo.TimeAndExpense.dtFromDate, dbo.TimeAndExpense.numUserCntID,
                      dbo.DivisionMaster.numDivisionID,ISNULL(dbo.General_Journal_Details.bitCleared,0) AS bitCleared,
                      ISNULL(dbo.General_Journal_Details.bitReconcile,0) AS bitReconcile,isnull(dbo.General_Journal_Header.numBillID, 0) AS numBillID,ISNULL(dbo.General_Journal_Header.numBillPaymentID,0) AS numBillPaymentID,
                      ISNULL(General_Journal_Details.numClassID,0) AS numClassID,ISNULL(General_Journal_Details.numProjectID,0) AS numProjectID,isnull(dbo.General_Journal_Header.numReturnID, 0) AS numReturnID,
                      General_Journal_Details.numCurrencyID,ISNULL(General_Journal_Details.numItemID,0) AS numItemID,
					  Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else 0 end AS numLandedCostOppId
FROM         dbo.General_Journal_Header INNER JOIN
                      dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId INNER JOIN
                      dbo.Chart_Of_Accounts ON dbo.General_Journal_Details.numChartAcntId = dbo.Chart_Of_Accounts.numAccountId LEFT OUTER JOIN
                      dbo.TimeAndExpense ON dbo.General_Journal_Header.numCategoryHDRID = dbo.TimeAndExpense.numCategoryHDRID LEFT OUTER JOIN
                      dbo.DivisionMaster LEFT OUTER JOIN
                      dbo.CompanyInfo ON dbo.DivisionMaster.numCompanyID = dbo.CompanyInfo.numCompanyId ON 
                      dbo.General_Journal_Details.numCustomerId = dbo.DivisionMaster.numDivisionID LEFT OUTER JOIN
                      dbo.OpportunityMaster ON dbo.General_Journal_Header.numOppId = dbo.OpportunityMaster.numOppId LEFT OUTER JOIN
                      dbo.CashCreditCardDetails ON dbo.General_Journal_Header.numCashCreditCardId = dbo.CashCreditCardDetails.numCashCreditId LEFT OUTER JOIN
                      dbo.OpportunityBizDocs ON dbo.General_Journal_Header.numOppBizDocsId = dbo.OpportunityBizDocs.numOppBizDocsId LEFT OUTER JOIN
                      dbo.VIEW_JOURNALCHEQ ON (dbo.General_Journal_Header.numCheckHeaderID = dbo.VIEW_JOURNALCHEQ.numCheckHeaderID 
                      or (dbo.General_Journal_Header.numBillPaymentID=dbo.VIEW_JOURNALCHEQ.numReferenceID and dbo.VIEW_JOURNALCHEQ.tintReferenceType=8))LEFT OUTER JOIN
                      dbo.VIEW_BIZPAYMENT ON dbo.General_Journal_Header.numBizDocsPaymentDetId = dbo.VIEW_BIZPAYMENT.numBizDocsPaymentDetId LEFT OUTER JOIN
                      dbo.DepositMaster DM ON DM.numDepositId = dbo.General_Journal_Header.numDepositId  LEFT OUTER JOIN
                      dbo.ReturnHeader RH ON RH.numReturnHeaderID = dbo.General_Journal_Header.numReturnID LEFT OUTER JOIN
                      dbo.CheckHeader CH ON CH.numReferenceID=RH.numReturnHeaderID AND CH.tintReferenceType=10
					  LEFT OUTER JOIN BillHeader BH  ON ISNULL(General_Journal_Header.numBillId,0) = BH.numBillId

GO
