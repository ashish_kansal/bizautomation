/****** Object:  Table [dbo].[SurveyRespondentsMaster]    Script Date: 07/26/2008 17:30:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SurveyRespondentsMaster](
	[numRespondantID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numSurID] [numeric](18, 0) NOT NULL,
	[numSurRating] [numeric](18, 0) NOT NULL CONSTRAINT [DF_SurveyRespondentsDynamicChild_numSurRating]  DEFAULT (0),
	[dtDateofResponse] [datetime] NOT NULL CONSTRAINT [DF_SurveyRespondentsDynamicChild_dtDateofResponse]  DEFAULT (getdate()),
	[bitRegisteredRespondant] [bit] NOT NULL CONSTRAINT [DF_SurveyRespondentsMaster_bitRegisteredRespondant]  DEFAULT (1),
	[numRegisteredRespondentContactId] [numeric](18, 0) NULL,
	[numDomainId] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_SurveyRespondentsDynamicChild] PRIMARY KEY CLUSTERED 
(
	[numRespondantID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SurveyRespondentsMaster]  WITH CHECK ADD  CONSTRAINT [FK_SurveyRespondentsMaster_SurveyMaster] FOREIGN KEY([numSurID])
REFERENCES [dbo].[SurveyMaster] ([numSurID])
GO
ALTER TABLE [dbo].[SurveyRespondentsMaster] CHECK CONSTRAINT [FK_SurveyRespondentsMaster_SurveyMaster]
GO
