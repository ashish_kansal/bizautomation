/****** Object:  Table [dbo].[ECampaignDTLs]    Script Date: 07/26/2008 17:24:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ECampaignDTLs](
	[numECampDTLId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numECampID] [numeric](18, 0) NULL,
	[numEmailTemplate] [numeric](18, 0) NULL,
	[tintDays] [tinyint] NULL,
 CONSTRAINT [PK_ECampaignDTLs] PRIMARY KEY CLUSTERED 
(
	[numECampDTLId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ECampaignDTLs]  WITH CHECK ADD  CONSTRAINT [FK_ECampaignDTLs_ECampaign] FOREIGN KEY([numECampID])
REFERENCES [dbo].[ECampaign] ([numECampaignID])
GO
ALTER TABLE [dbo].[ECampaignDTLs] CHECK CONSTRAINT [FK_ECampaignDTLs_ECampaign]
GO
