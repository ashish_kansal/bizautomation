/****** Object:  Table [dbo].[PaymentGatewayDTLID]    Script Date: 07/26/2008 17:28:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PaymentGatewayDTLID](
	[intPaymentGateWay] [int] NULL,
	[vcFirstFldValue] [varchar](1000) NULL,
	[vcSecndFldValue] [varchar](1000) NULL,
	[bitTest] [bit] NULL,
	[numDomainID] [numeric](18, 0) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
