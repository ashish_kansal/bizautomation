/****** Object:  Table [dbo].[InitialListColumnConf]    Script Date: 07/26/2008 17:25:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InitialListColumnConf](
	[numDomainId] [numeric](18, 0) NULL,
	[numUserCntId] [numeric](18, 0) NULL,
	[numFormId] [numeric](18, 0) NULL,
	[numFormFieldID] [numeric](18, 0) NULL,
	[numType] [numeric](18, 0) NULL,
	[tintOrder] [numeric](18, 0) NULL,
	[bitCustom] [bit] NULL
) ON [PRIMARY]
GO
