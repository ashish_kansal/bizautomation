/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.4206)
    Source Database Engine Edition : Microsoft SQL Server Express Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2016
    Target Database Engine Edition : Microsoft SQL Server Express Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [Production.2014]
GO

/****** Object:  Table [dbo].[PromotionOfferOrganizations]    Script Date: 10/26/2017 3:30:56 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create TABLE [dbo].[PromotionOfferOrganizations](
	[numProOrgId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numProId] [numeric](18, 0) NOT NULL,
	[numDivisionID] [numeric](18, 0) NULL,
	[numRelationship] [numeric](18, 0)  NULL,
	[numProfile] [numeric](18, 0) NULL,
	[tintType] [tinyint] NOT NULL,
 CONSTRAINT [PK_PromotionOfferOrganizations] PRIMARY KEY CLUSTERED 
(
	[numProOrgId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[PromotionOfferOrganizations]  WITH CHECK ADD  CONSTRAINT [FK_PromotionOfferOrganizations_PromotionOffer] FOREIGN KEY([numProId])
REFERENCES [dbo].[PromotionOffer] ([numProId])
GO

ALTER TABLE [dbo].[PromotionOfferOrganizations] CHECK CONSTRAINT [FK_PromotionOfferOrganizations_PromotionOffer]
GO


