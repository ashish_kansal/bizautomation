USE [Production.2014]
GO

/****** Object:  Table [dbo].[SalesOrderRule]    Script Date: 06-02-2018 02:51:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SalesOrderRule](
	[numSalesOrderRuleID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainId] [numeric](18, 0) NOT NULL,
	[numListItemID] [numeric](18, 0) NOT NULL,
	[numListID] [numeric](18, 0) NOT NULL,
	[bitEnforceMinOrderAmount] [bit] NULL,
	[fltMinOrderAmount] [float] NULL,
 CONSTRAINT [PK_SalesOrderRule] PRIMARY KEY CLUSTERED 
(
	[numSalesOrderRuleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


