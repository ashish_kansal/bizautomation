/****** Object:  Table [dbo].[BizDocAlerts]    Script Date: 07/26/2008 17:20:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BizDocAlerts](
	[numBizDocAlertID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numBizDocID] [numeric](18, 0) NOT NULL,
	[bitCreated] [bit] NULL,
	[bitModified] [bit] NULL,
	[bitApproved] [bit] NULL,
	[numEmailTemplate] [numeric](18, 0) NULL,
	[bitOppOwner] [bit] NULL,
	[bitCCManager] [bit] NULL,
	[bitPriConatct] [bit] NULL,
 CONSTRAINT [PK_BizDocAlerts] PRIMARY KEY CLUSTERED 
(
	[numBizDocAlertID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
