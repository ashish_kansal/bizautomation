/****** Object:  Table [dbo].[DashBoardSize]    Script Date: 07/26/2008 17:23:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DashBoardSize](
	[tintColumn] [tinyint] NOT NULL CONSTRAINT [DF_DashBoardSize_tintColumn]  DEFAULT ((0)),
	[tintSize] [tinyint] NOT NULL,
	[numGroupUserCntID] [numeric](18, 0) NOT NULL,
	[bitGroup] [bit] NOT NULL
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0-User,1-Group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DashBoardSize', @level2type=N'COLUMN',@level2name=N'bitGroup'
GO
