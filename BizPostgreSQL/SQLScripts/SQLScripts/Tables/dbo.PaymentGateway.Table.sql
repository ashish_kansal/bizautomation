/****** Object:  Table [dbo].[PaymentGateway]    Script Date: 07/26/2008 17:28:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PaymentGateway](
	[intPaymentGateWay] [int] IDENTITY(1,1) NOT NULL,
	[vcGateWayName] [varchar](100) NULL,
	[vcFirstFldName] [varchar](100) NULL,
	[vcSecndFldName] [varchar](100) NULL,
 CONSTRAINT [PK_PaymentGateway] PRIMARY KEY CLUSTERED 
(
	[intPaymentGateWay] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
