/****** Object:  Table [dbo].[ModuleMaster]    Script Date: 07/26/2008 17:26:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ModuleMaster](
	[numModuleID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[vcModuleName] [nvarchar](50) NOT NULL,
	[tintGroupType] [tinyint] NULL,
 CONSTRAINT [PK_ModuleMaster] PRIMARY KEY CLUSTERED 
(
	[numModuleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
