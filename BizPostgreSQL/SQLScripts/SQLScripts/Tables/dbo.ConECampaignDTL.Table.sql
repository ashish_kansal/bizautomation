/****** Object:  Table [dbo].[ConECampaignDTL]    Script Date: 07/26/2008 17:22:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ConECampaignDTL](
	[numConECampDTLID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numConECampID] [numeric](18, 0) NULL,
	[numECampDTLID] [numeric](18, 0) NULL,
	[bitSend] [bit] NULL,
	[bintSentON] [datetime] NULL,
 CONSTRAINT [PK_ConECampaignDTL] PRIMARY KEY CLUSTERED 
(
	[numConECampDTLID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ConECampaignDTL]  WITH CHECK ADD  CONSTRAINT [FK_ConECampaignDTL_ECampaignDTLs] FOREIGN KEY([numECampDTLID])
REFERENCES [dbo].[ECampaignDTLs] ([numECampDTLId])
GO
ALTER TABLE [dbo].[ConECampaignDTL] CHECK CONSTRAINT [FK_ConECampaignDTL_ECampaignDTLs]
GO
