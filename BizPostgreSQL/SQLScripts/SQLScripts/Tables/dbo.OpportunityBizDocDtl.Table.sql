/****** Object:  Table [dbo].[OpportunityBizDocDtl]    Script Date: 07/26/2008 17:26:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OpportunityBizDocDtl](
	[numOppBizDoCDtlID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numBizDocID] [numeric](18, 0) NULL,
	[vcItemName] [varchar](100) NULL,
	[vcItemType] [varchar](50) NULL,
	[vcDesc] [varchar](1000) NULL,
	[fltUnit] [float] NULL,
	[monItemPrice] [money] NULL,
	[monAmount] [money] NULL,
	[monTax] [money] NULL,
 CONSTRAINT [PK_OppBizDocDtl] PRIMARY KEY CLUSTERED 
(
	[numOppBizDoCDtlID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[OpportunityBizDocDtl]  WITH CHECK ADD  CONSTRAINT [FK_OpportunityBizDocDtl_OpportunityBizDocs] FOREIGN KEY([numBizDocID])
REFERENCES [dbo].[OpportunityBizDocs] ([numOppBizDocsId])
GO
ALTER TABLE [dbo].[OpportunityBizDocDtl] CHECK CONSTRAINT [FK_OpportunityBizDocDtl_OpportunityBizDocs]
GO
