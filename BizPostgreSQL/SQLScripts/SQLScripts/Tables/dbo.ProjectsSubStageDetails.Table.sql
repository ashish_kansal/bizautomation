/****** Object:  Table [dbo].[ProjectsSubStageDetails]    Script Date: 07/26/2008 17:28:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProjectsSubStageDetails](
	[numSubStageElmtId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numProId] [numeric](18, 0) NOT NULL,
	[numStageDetailsId] [numeric](18, 0) NOT NULL,
	[numSubStageID] [numeric](18, 0) NOT NULL,
	[numProcessListId] [numeric](18, 0) NULL,
	[vcSubStageDetail] [varchar](50) NULL,
	[bitStageCompleted] [bit] NULL,
 CONSTRAINT [PK_ProjectsSubStageDetails] PRIMARY KEY CLUSTERED 
(
	[numSubStageElmtId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ProjectsSubStageDetails]  WITH CHECK ADD  CONSTRAINT [FK_ProjectsSubStageDetails_ProjectsMaster] FOREIGN KEY([numProId])
REFERENCES [dbo].[ProjectsMaster] ([numProId])
GO
ALTER TABLE [dbo].[ProjectsSubStageDetails] CHECK CONSTRAINT [FK_ProjectsSubStageDetails_ProjectsMaster]
GO
