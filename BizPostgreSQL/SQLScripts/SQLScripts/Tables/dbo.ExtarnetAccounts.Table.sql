/****** Object:  Table [dbo].[ExtarnetAccounts]    Script Date: 07/26/2008 17:24:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExtarnetAccounts](
	[numExtranetID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numCompanyID] [numeric](18, 0) NOT NULL,
	[numDivisionID] [numeric](18, 0) NOT NULL,
	[numGroupID] [numeric](18, 0) NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[numPartnerGroupID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_ExtarnetAccounts] PRIMARY KEY CLUSTERED 
(
	[numExtranetID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ExtarnetAccounts]  WITH CHECK ADD  CONSTRAINT [FK_ExtarnetAccounts_CompanyInfo] FOREIGN KEY([numCompanyID])
REFERENCES [dbo].[CompanyInfo] ([numCompanyId])
GO
ALTER TABLE [dbo].[ExtarnetAccounts] CHECK CONSTRAINT [FK_ExtarnetAccounts_CompanyInfo]
GO
ALTER TABLE [dbo].[ExtarnetAccounts]  WITH NOCHECK ADD  CONSTRAINT [FK_ExtarnetAccounts_DivisionMaster] FOREIGN KEY([numDivisionID])
REFERENCES [dbo].[DivisionMaster] ([numDivisionID])
GO
ALTER TABLE [dbo].[ExtarnetAccounts] CHECK CONSTRAINT [FK_ExtarnetAccounts_DivisionMaster]
GO
