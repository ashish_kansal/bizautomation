/****** Object:  Table [dbo].[SolutionMaster]    Script Date: 07/26/2008 17:29:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SolutionMaster](
	[numSolnID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numCategory] [numeric](18, 0) NOT NULL,
	[vcSolnTitle] [varchar](1000) NOT NULL,
	[txtSolution] [text] NOT NULL,
	[vcLink] [varchar](500) NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[numCreatedby] [numeric](18, 0) NULL,
	[dtCreatedon] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[dtModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_SolutionMaster] PRIMARY KEY CLUSTERED 
(
	[numSolnID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
