/****** Object:  Table [dbo].[SurveyWorkflowRules]    Script Date: 07/26/2008 17:30:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SurveyWorkflowRules](
	[numAnsID] [numeric](18, 0) NOT NULL,
	[numRuleID] [numeric](18, 0) NOT NULL,
	[numQID] [numeric](18, 0) NOT NULL,
	[numSurID] [numeric](18, 0) NOT NULL,
	[boolActivation] [bit] NOT NULL CONSTRAINT [DF_SurveyWorkflowRules_boolActivation]  DEFAULT (0),
	[numEventOrder] [tinyint] NOT NULL,
	[vcQuestionList] [nvarchar](50) NULL CONSTRAINT [DF_SurveyWorkflowRules_vcQuestionList]  DEFAULT (''),
	[numLinkedSurID] [numeric](18, 0) NULL,
	[numResponseRating] [tinyint] NULL,
	[vcPopUpURL] [nvarchar](100) NULL,
	[numGrpId] [numeric](18, 0) NULL,
	[numCompanyType] [numeric](18, 0) NULL,
	[tIntCRMType] [tinyint] NULL,
	[numRecOwner] [numeric](18, 0) NULL,
 CONSTRAINT [PK_SurveyWorkflowRules] PRIMARY KEY CLUSTERED 
(
	[numAnsID] ASC,
	[numRuleID] ASC,
	[numQID] ASC,
	[numSurID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SurveyWorkflowRules]  WITH CHECK ADD  CONSTRAINT [FK_SurveyWorkflowRules_SurveyMaster] FOREIGN KEY([numSurID])
REFERENCES [dbo].[SurveyMaster] ([numSurID])
GO
ALTER TABLE [dbo].[SurveyWorkflowRules] CHECK CONSTRAINT [FK_SurveyWorkflowRules_SurveyMaster]
GO
