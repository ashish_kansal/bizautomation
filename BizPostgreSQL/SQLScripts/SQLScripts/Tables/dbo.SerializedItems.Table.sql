/****** Object:  Table [dbo].[SerializedItems]    Script Date: 07/26/2008 17:29:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SerializedItems](
	[numSerialId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numWarehouseLocation] [numeric](18, 0) NULL,
	[numItemCode] [numeric](18, 0) NULL,
 CONSTRAINT [PK_SerializedItems] PRIMARY KEY CLUSTERED 
(
	[numSerialId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
