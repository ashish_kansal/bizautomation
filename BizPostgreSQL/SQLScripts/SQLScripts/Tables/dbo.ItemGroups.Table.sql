/****** Object:  Table [dbo].[ItemGroups]    Script Date: 07/26/2008 17:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ItemGroups](
	[numItemGroupID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[vcItemGroup] [varchar](100) NULL,
	[numDomainID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_ItemGroups] PRIMARY KEY CLUSTERED 
(
	[numItemGroupID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
