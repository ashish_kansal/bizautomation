/****** Object:  Table [dbo].[OpportunityBizDocsDetails]    Script Date: 07/26/2008 17:27:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OpportunityBizDocsDetails](
	[numBizDocsPaymentDetId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numBizDocsId] [numeric](18, 0) NULL,
	[numPaymentMethod] [tinyint] NULL,
	[monAmount] [money] NULL,
	[numDepoistToChartAcntId] [numeric](18, 0) NULL,
	[numDomainId] [numeric](18, 0) NULL,
	[vcReference] [nvarchar](200) NULL,
	[vcMemo] [nvarchar](50) NULL,
	[bitAuthoritativeBizDocs] [bit] NULL,
	[bitIntegratedToAcnt] [bit] NULL,
	[bitDeferredIncome] [bit] NULL,
	[sintDeferredIncomePeriod] [smallint] NULL,
	[dtDeferredIncomeStartDate] [datetime] NULL,
	[NoofDeferredIncomeTransaction] [smallint] NULL,
	[bitSalesDeferredIncome] [bit] NULL,
	[numContractId] [numeric](18, 0) NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[dtCreationDate] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[dtModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_OpportunityBizDocsDetails] PRIMARY KEY CLUSTERED 
(
	[numBizDocsPaymentDetId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
