/****** Object:  Table [dbo].[ItemDiscountProfile]    Script Date: 07/26/2008 17:26:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ItemDiscountProfile](
	[numDiscProfileID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numItemID] [numeric](18, 0) NOT NULL,
	[numDivisionID] [numeric](18, 0) NULL,
	[numRelID] [numeric](18, 0) NULL,
	[numProID] [numeric](18, 0) NULL,
	[decDisc] [decimal](18, 0) NULL,
	[bitApplicable] [bit] NULL,
 CONSTRAINT [PK_ItemDiscountProfile] PRIMARY KEY CLUSTERED 
(
	[numDiscProfileID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ItemDiscountProfile]  WITH CHECK ADD  CONSTRAINT [FK_ItemDiscountProfile_Item] FOREIGN KEY([numItemID])
REFERENCES [dbo].[Item] ([numItemCode])
GO
ALTER TABLE [dbo].[ItemDiscountProfile] CHECK CONSTRAINT [FK_ItemDiscountProfile_Item]
GO
