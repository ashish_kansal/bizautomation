/****** Object:  Table [dbo].[ContactAddress]    Script Date: 07/26/2008 17:22:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContactAddress](
	[numConAddID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numContactID] [numeric](18, 0) NULL,
	[vcPStreet] [varchar](50) NULL,
	[vcPCity] [varchar](50) NULL,
	[vcPPostalCode] [varchar](15) NULL,
	[vcPState] [numeric](18, 0) NULL,
	[vcPCountry] [numeric](18, 0) NULL,
	[vcContactLocation] [numeric](18, 0) NULL,
	[vcStreet] [varchar](50) NULL,
	[vcCity] [varchar](50) NULL,
	[intPostalCode] [varchar](15) NULL,
	[vcState] [numeric](18, 0) NULL,
	[vcCountry] [numeric](18, 0) NULL,
	[vcContactLocation1] [numeric](18, 0) NULL,
	[vcStreet1] [varchar](50) NULL,
	[vcCity1] [varchar](50) NULL,
	[vcPostalCode1] [varchar](15) NULL,
	[vcState1] [numeric](18, 0) NULL,
	[vcCountry1] [numeric](18, 0) NULL,
	[vcContactLocation2] [numeric](18, 0) NULL,
	[vcStreet2] [varchar](50) NULL,
	[vcCity2] [varchar](50) NULL,
	[vcPostalCode2] [varchar](15) NULL,
	[vcState2] [numeric](18, 0) NULL,
	[vcCountry2] [numeric](18, 0) NULL,
	[vcContactLocation3] [numeric](18, 0) NULL,
	[vcStreet3] [varchar](50) NULL,
	[vcCity3] [varchar](50) NULL,
	[vcPostalCode3] [varchar](15) NULL,
	[vcState3] [numeric](18, 0) NULL,
	[vcCountry3] [numeric](18, 0) NULL,
	[vcContactLocation4] [numeric](18, 0) NULL,
	[vcStreet4] [varchar](50) NULL,
	[vcCity4] [varchar](50) NULL,
	[vcPostalCode4] [varchar](15) NULL,
	[vcState4] [numeric](18, 0) NULL,
	[vcCountry4] [numeric](18, 0) NULL,
 CONSTRAINT [PK_ContactAddress] PRIMARY KEY CLUSTERED 
(
	[numConAddID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
