/****** Object:  Table [dbo].[AlertHDR]    Script Date: 07/26/2008 17:20:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AlertHDR](
	[numAlertID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[vcAlert] [varchar](100) NOT NULL,
	[vcDesc] [varchar](1000) NULL,
	[vcPage] [varchar](300) NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[intPageHeight] [numeric](18, 0) NULL,
 CONSTRAINT [PK_AlertHDR] PRIMARY KEY CLUSTERED 
(
	[numAlertID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
