/****** Object:  Table [dbo].[ExtranetAccountsDtl]    Script Date: 07/26/2008 17:24:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ExtranetAccountsDtl](
	[numExtranetDtlID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numExtranetID] [numeric](18, 0) NOT NULL,
	[numContactID] [varchar](100) NULL,
	[vcPassword] [nvarchar](100) NULL,
	[tintAccessAllowed] [tinyint] NULL,
	[bintLastLoggedIn] [datetime] NULL,
	[numNoOfTimes] [numeric](18, 0) NULL,
	[bitPartnerAccess] [bit] NULL,
	[numDomainID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_ExtranetAccountDtl] PRIMARY KEY CLUSTERED 
(
	[numExtranetDtlID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ExtranetAccountsDtl]  WITH CHECK ADD  CONSTRAINT [FK_ExtranetAccountsDtl_ExtarnetAccounts] FOREIGN KEY([numExtranetID])
REFERENCES [dbo].[ExtarnetAccounts] ([numExtranetID])
GO
ALTER TABLE [dbo].[ExtranetAccountsDtl] CHECK CONSTRAINT [FK_ExtranetAccountsDtl_ExtarnetAccounts]
GO
