/****** Object:  Table [dbo].[OpportunityKitItems]    Script Date: 07/26/2008 17:27:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OpportunityKitItems](
	[numOppChildItemID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numOppKitItem] [numeric](18, 0) NULL,
	[numChildItem] [numeric](18, 0) NULL,
	[intQuantity] [int] NULL,
	[numWareHouseItemId] [numeric](18, 0) NULL,
 CONSTRAINT [PK_OpportunityKitItems] PRIMARY KEY CLUSTERED 
(
	[numOppChildItemID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
