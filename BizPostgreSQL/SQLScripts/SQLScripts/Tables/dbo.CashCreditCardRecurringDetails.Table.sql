/****** Object:  Table [dbo].[CashCreditCardRecurringDetails]    Script Date: 07/26/2008 17:21:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CashCreditCardRecurringDetails](
	[numCashCreditCardDetId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numCashCreditCardId] [numeric](18, 0) NULL,
	[dtRecurringDate] [datetime] NULL
) ON [PRIMARY]
GO
