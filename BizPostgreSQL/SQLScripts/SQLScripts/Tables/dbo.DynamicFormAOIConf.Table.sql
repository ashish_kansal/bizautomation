/****** Object:  Table [dbo].[DynamicFormAOIConf]    Script Date: 07/26/2008 17:24:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DynamicFormAOIConf](
	[numAOIID] [numeric](18, 0) NULL,
	[numFormID] [numeric](18, 0) NULL,
	[numGroupID] [numeric](18, 0) NULL,
	[numDomainID] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
