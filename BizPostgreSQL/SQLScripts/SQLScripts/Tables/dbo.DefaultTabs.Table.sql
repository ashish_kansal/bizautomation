/****** Object:  Table [dbo].[DefaultTabs]    Script Date: 07/26/2008 17:23:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DefaultTabs](
	[numTabId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainId] [numeric](18, 0) NOT NULL CONSTRAINT [DF_TabDtl_numDomainId]  DEFAULT ((1)),
	[numContactId] [numeric](18, 0) NULL,
	[vcMyLead] [varchar](200) NULL,
	[vcWebLead] [varchar](200) NULL,
	[vcPublicLead] [varchar](200) NULL,
	[vcContact] [varchar](50) NULL,
	[vcProspect] [varchar](50) NULL,
	[vcAccount] [varchar](50) NULL,
	[vcLead] [varchar](50) NULL,
 CONSTRAINT [PK_TabDtl] PRIMARY KEY CLUSTERED 
(
	[numTabId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
