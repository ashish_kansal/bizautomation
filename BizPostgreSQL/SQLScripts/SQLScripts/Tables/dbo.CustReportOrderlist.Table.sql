/****** Object:  Table [dbo].[CustReportOrderlist]    Script Date: 07/26/2008 17:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CustReportOrderlist](
	[numCustomReportId] [numeric](18, 0) NULL,
	[vcFieldText] [varchar](200) NULL,
	[vcValue] [varchar](200) NULL,
	[numOrder] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
