/****** Object:  Table [dbo].[EmailMergeFields]    Script Date: 07/26/2008 17:24:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmailMergeFields](
	[intEmailMergeFields] [int] IDENTITY(1,1) NOT NULL,
	[vcMergeField] [varchar](100) NULL,
	[vcMergeFieldValue] [varchar](100) NULL,
 CONSTRAINT [PK_EmailMergeFields] PRIMARY KEY CLUSTERED 
(
	[intEmailMergeFields] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
