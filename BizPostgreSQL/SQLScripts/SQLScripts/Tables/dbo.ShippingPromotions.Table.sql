/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.4206)
    Source Database Engine Edition : Microsoft SQL Server Express Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2016
    Target Database Engine Edition : Microsoft SQL Server Express Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [Production.2014]
GO

/****** Object:  Table [dbo].[ShippingPromotions]    Script Date: 11/1/2017 7:02:47 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ShippingPromotions](
	[numShipProId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[bitFixShipping1] [bit] NULL,
	[monFixShipping1OrderAmount] [money] NULL,
	[monFixShipping1Charge] [money] NULL,
	[bitFixShipping2] [bit] NULL,
	[monFixShipping2OrderAmount] [money] NULL,
	[monFixShipping2Charge] [money] NULL,
	[bitFreeShiping] [bit] NULL,
	[monFreeShippingOrderAmount] [money] NULL,
	[numFreeShippingCountry] [numeric](18, 0) NULL,
	[numDomainId] [numeric](18, 0) NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[dtCreated] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[dtModified] [datetime] NULL,
 CONSTRAINT [PK_ShippingPromotions] PRIMARY KEY CLUSTERED 
(
	[numShipProId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO


