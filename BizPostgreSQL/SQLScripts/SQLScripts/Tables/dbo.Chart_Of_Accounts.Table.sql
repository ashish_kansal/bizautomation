/****** Object:  Table [dbo].[Chart_Of_Accounts]    Script Date: 07/26/2008 17:21:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Chart_Of_Accounts](
	[numAccountId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numParntAcntId] [numeric](18, 0) NULL,
	[numAcntType] [numeric](18, 0) NULL,
	[vcCatgyName] [varchar](100) NULL,
	[vcCatgyDescription] [varchar](100) NULL,
	[numOriginalOpeningBal] [money] NULL,
	[numOpeningBal] [money] NULL,
	[dtOpeningDate] [smalldatetime] NULL,
	[bitActive] [bit] NULL,
	[bitFixed] [bit] NULL,
	[numDomainId] [numeric](18, 0) NULL,
	[numListItemID] [numeric](18, 0) NULL,
	[bitDepreciation] [numeric](18, 0) NULL,
	[bitOpeningBalanceEquity] [bit] NULL,
	[monEndingOpeningBal] [money] NULL,
	[monEndingBal] [money] NULL,
	[dtEndStatementDate] [datetime] NULL,
	[numServiceAcntId] [numeric](18, 0) NULL,
	[dtServiceCharge] [datetime] NULL,
	[monserviceCharges] [money] NULL,
	[numInterestEarnedAcntId] [numeric](18, 0) NULL,
	[dtInternestEarned] [datetime] NULL,
	[monInterestEarned] [money] NULL,
	[numFinancialAntId] [numeric](18, 0) NULL,
	[dtFinancialCharges] [datetime] NULL,
	[monFinancialCharges] [money] NULL,
	[chBizDocItems] [nchar](2) NULL,
 CONSTRAINT [PK_Chart_Of_Accounts] PRIMARY KEY CLUSTERED 
(
	[numAccountId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
