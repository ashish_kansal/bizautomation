/****** Object:  Table [dbo].[CustReportFilterlist]    Script Date: 07/26/2008 17:23:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CustReportFilterlist](
	[numCustomReportId] [numeric](18, 0) NULL,
	[vcFieldsText] [varchar](200) NULL,
	[vcFieldsValue] [varchar](200) NULL,
	[vcFieldsOperator] [varchar](200) NULL,
	[vcFilterValue] [varchar](200) NULL,
	[vcFilterValued] [varchar](200) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
