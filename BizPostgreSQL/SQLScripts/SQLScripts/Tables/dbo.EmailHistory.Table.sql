/****** Object:  Table [dbo].[EmailHistory]    Script Date: 07/26/2008 17:24:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmailHistory](
	[numEmailHstrID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[vcSubject] [varchar](500) NULL,
	[vcBody] [text] NULL,
	[bintCreatedOn] [datetime] NULL,
	[numNoofTimes] [numeric](18, 0) NULL,
	[vcItemId] [varchar](1500) NULL,
	[vcChangeKey] [varchar](1000) NULL,
	[bitIsRead] [bit] NULL,
	[vcSize] [varchar](50) NULL,
	[bitHasAttachments] [bit] NULL,
	[dtReceivedOn] [datetime] NULL,
	[tintType] [tinyint] NOT NULL CONSTRAINT [DF_EmailHistory_tintType]  DEFAULT ((1)),
	[chrSource] [char](1) NULL,
	[vcCategory] [varchar](200) NULL,
	[vcBodyText] [ntext] NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[numUserCntId] [numeric](18, 0) NULL,
	[numUid] [numeric](18, 0) NULL,
	[numNodeId] [numeric](18, 0) NOT NULL CONSTRAINT [DF_EmailHistory_numNodeId]  DEFAULT ((0)),
 CONSTRAINT [PK_EmailHistory] PRIMARY KEY CLUSTERED 
(
	[numEmailHstrID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 - for Inbox;2-Sent Items;3-Deleted Items;4-Imp Items;5-Mails From Imap' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EmailHistory', @level2type=N'COLUMN',@level2name=N'tintType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'O if the message was sent from Outlook, and B if it was sent from BizAutomation.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EmailHistory', @level2type=N'COLUMN',@level2name=N'chrSource'
GO
