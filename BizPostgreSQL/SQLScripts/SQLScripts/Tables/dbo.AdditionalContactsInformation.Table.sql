/****** Object:  Table [dbo].[AdditionalContactsInformation]    Script Date: 07/26/2008 17:20:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AdditionalContactsInformation](
	[numContactId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[vcDepartment] [numeric](18, 0) NULL,
	[vcCategory] [numeric](18, 0) NULL,
	[vcGivenName] [varchar](100) NULL,
	[vcFirstName] [varchar](50) NULL,
	[vcLastName] [varchar](50) NULL,
	[numDivisionId] [numeric](18, 0) NULL,
	[numContactType] [numeric](18, 0) NULL,
	[numTeam] [numeric](18, 0) NULL,
	[numPhone] [varchar](15) NULL,
	[numPhoneExtension] [varchar](6) NULL,
	[numCell] [varchar](15) NULL,
	[numHomePhone] [varchar](15) NULL,
	[vcFax] [varchar](15) NULL,
	[vcEmail] [varchar](50) NULL,
	[VcAsstFirstName] [varchar](50) NULL,
	[vcAsstLastName] [varchar](50) NULL,
	[numAsstPhone] [varchar](15) NULL,
	[numAsstExtn] [varchar](6) NULL,
	[vcAsstEmail] [varchar](50) NULL,
	[charSex] [char](1) NULL,
	[bintDOB] [datetime] NULL,
	[vcPosition] [numeric](18, 0) NULL,
	[txtNotes] [text] NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[bintCreatedDate] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[bintModifiedDate] [datetime] NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[bitOptOut] [bit] NULL CONSTRAINT [DF_AdditionalContactsInformation_bitOptout]  DEFAULT (0),
	[numManagerID] [numeric](18, 0) NULL CONSTRAINT [DF_AdditionalContactsInformation_numManagerID]  DEFAULT (0),
	[numRecOwner] [numeric](18, 0) NULL,
	[numEmpStatus] [bigint] NULL CONSTRAINT [DF_AdditionalContactsInformation_numEmpStatus]  DEFAULT (2),
	[vcTitle] [varchar](100) NULL,
	[vcAltEmail] [varchar](100) NULL,
	[vcItemId] [varchar](250) NULL,
	[vcChangeKey] [varchar](250) NULL,
 CONSTRAINT [PK_AdditionalContactsInformation] PRIMARY KEY CLUSTERED 
(
	[numContactId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[AdditionalContactsInformation]  WITH NOCHECK ADD  CONSTRAINT [FK_AdditionalContactsInformation_DivisionMaster] FOREIGN KEY([numDivisionId])
REFERENCES [dbo].[DivisionMaster] ([numDivisionID])
GO
ALTER TABLE [dbo].[AdditionalContactsInformation] CHECK CONSTRAINT [FK_AdditionalContactsInformation_DivisionMaster]
GO
