/****** Object:  Table [dbo].[CustRptAndFieldGroupRelation]    Script Date: 07/26/2008 17:23:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustRptAndFieldGroupRelation](
	[numReportOptionTypesId] [numeric](18, 0) NOT NULL,
	[numFieldGroupID] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_CustRptAndFieldGroupRelation] PRIMARY KEY CLUSTERED 
(
	[numReportOptionTypesId] ASC,
	[numFieldGroupID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
