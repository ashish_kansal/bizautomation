/****** Object:  Table [dbo].[Sales_process_List_Master]    Script Date: 07/26/2008 17:29:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sales_process_List_Master](
	[Slp_Id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Slp_Name] [varchar](50) NOT NULL,
	[numdomainid] [numeric](18, 0) NULL,
	[pro_type] [tinyint] NULL,
	[numCreatedby] [numeric](18, 0) NULL,
	[dtCreatedon] [datetime] NULL,
	[numModifedby] [numeric](18, 0) NULL,
	[dtModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_Sales_process_List_Master] PRIMARY KEY CLUSTERED 
(
	[Slp_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
