/****** Object:  Table [dbo].[General_Journal_Header]    Script Date: 07/26/2008 17:25:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[General_Journal_Header](
	[numJournal_Id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numRecurringId] [numeric](18, 0) NULL,
	[datEntry_Date] [smalldatetime] NULL,
	[varDescription] [varchar](200) NULL,
	[Posted_Date] [smalldatetime] NULL,
	[numAmount] [money] NULL,
	[numDomainId] [numeric](18, 0) NULL,
	[numCheckId] [numeric](18, 0) NULL,
	[numCashCreditCardId] [numeric](18, 0) NULL,
	[numChartAcntId] [numeric](18, 0) NULL,
	[numOppId] [numeric](18, 0) NULL,
	[numOppBizDocsId] [numeric](18, 0) NULL,
	[numDepositId] [numeric](18, 0) NULL,
	[numBizDocsPaymentDetId] [numeric](18, 0) NULL,
	[bitOpeningBalance] [bit] NULL,
	[dtLastRecurringDate] [datetime] NULL,
	[numNoTransactions] [numeric](18, 0) NULL,
	[numCategoryHDRID] [numeric](18, 0) NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[datCreatedDate] [datetime] NULL,
 CONSTRAINT [PK_General_Journal_Header] PRIMARY KEY CLUSTERED 
(
	[numJournal_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'General_Journal_Header', @level2type=N'COLUMN',@level2name=N'numCheckId'
GO
ALTER TABLE [dbo].[General_Journal_Header]  WITH CHECK ADD  CONSTRAINT [FK_General_Journal_Header_CashCreditCardDetails] FOREIGN KEY([numCashCreditCardId])
REFERENCES [dbo].[CashCreditCardDetails] ([numCashCreditId])
GO
ALTER TABLE [dbo].[General_Journal_Header] CHECK CONSTRAINT [FK_General_Journal_Header_CashCreditCardDetails]
GO
ALTER TABLE [dbo].[General_Journal_Header]  WITH CHECK ADD  CONSTRAINT [FK_General_Journal_Header_CheckDetails] FOREIGN KEY([numCheckId])
REFERENCES [dbo].[CheckDetails] ([numCheckId])
GO
ALTER TABLE [dbo].[General_Journal_Header] CHECK CONSTRAINT [FK_General_Journal_Header_CheckDetails]
GO
ALTER TABLE [dbo].[General_Journal_Header]  WITH CHECK ADD  CONSTRAINT [FK_General_Journal_Header_DepositDetails] FOREIGN KEY([numDepositId])
REFERENCES [dbo].[DepositDetails] ([numDepositId])
GO
ALTER TABLE [dbo].[General_Journal_Header] CHECK CONSTRAINT [FK_General_Journal_Header_DepositDetails]
GO
ALTER TABLE [dbo].[General_Journal_Header]  WITH CHECK ADD  CONSTRAINT [FK_General_Journal_Header_RecurringTemplate] FOREIGN KEY([numRecurringId])
REFERENCES [dbo].[RecurringTemplate] ([numRecurringId])
GO
ALTER TABLE [dbo].[General_Journal_Header] CHECK CONSTRAINT [FK_General_Journal_Header_RecurringTemplate]
GO
