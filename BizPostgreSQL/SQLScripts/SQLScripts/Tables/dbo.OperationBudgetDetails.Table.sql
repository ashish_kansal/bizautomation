/****** Object:  Table [dbo].[OperationBudgetDetails]    Script Date: 07/26/2008 17:26:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OperationBudgetDetails](
	[numBudgetDetId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numBudgetId] [numeric](18, 0) NULL,
	[numChartAcntId] [numeric](18, 0) NULL,
	[numParentAcntId] [numeric](18, 0) NULL,
	[txtChartAcntname] [varchar](8000) NULL,
	[tintMonth] [tinyint] NULL,
	[intYear] [int] NULL,
	[monAmount] [money] NULL,
	[vcComments] [varchar](50) NULL,
	[numDomainId] [numeric](18, 0) NULL,
 CONSTRAINT [PK_OperationBudgetDetails] PRIMARY KEY CLUSTERED 
(
	[numBudgetDetId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[OperationBudgetDetails]  WITH CHECK ADD  CONSTRAINT [FK_OperationBudgetDetails_OperationBudgetMaster] FOREIGN KEY([numBudgetId])
REFERENCES [dbo].[OperationBudgetMaster] ([numBudgetId])
GO
ALTER TABLE [dbo].[OperationBudgetDetails] CHECK CONSTRAINT [FK_OperationBudgetDetails_OperationBudgetMaster]
GO
