/****** Object:  Table [dbo].[OpportunityItems]    Script Date: 07/26/2008 17:27:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OpportunityItems](
	[numoppitemtCode] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numOppId] [numeric](18, 0) NULL,
	[numItemCode] [numeric](18, 0) NOT NULL,
	[numUnitHour] [numeric](18, 0) NOT NULL,
	[monPrice] [money] NOT NULL,
	[monTotAmount] [money] NOT NULL,
	[numSourceID] [numeric](18, 0) NULL,
	[vcItemDesc] [varchar](1000) NULL,
	[numWarehouseItmsID] [numeric](18, 0) NULL,
	[vcType] [varchar](max) NULL,
	[vcAttributes] [varchar](50) NULL,
	[bitDropShip] [bit] NULL,
 CONSTRAINT [PK_OpportunityItems] PRIMARY KEY CLUSTERED 
(
	[numoppitemtCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stores source ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OpportunityItems', @level2type=N'COLUMN',@level2name=N'numSourceID'
GO
ALTER TABLE [dbo].[OpportunityItems]  WITH NOCHECK ADD  CONSTRAINT [FK_OpportunityItems_Item] FOREIGN KEY([numItemCode])
REFERENCES [dbo].[Item] ([numItemCode])
GO
ALTER TABLE [dbo].[OpportunityItems] CHECK CONSTRAINT [FK_OpportunityItems_Item]
GO
ALTER TABLE [dbo].[OpportunityItems]  WITH CHECK ADD  CONSTRAINT [FK_OpportunityItems_WareHouseItems] FOREIGN KEY([numWarehouseItmsID])
REFERENCES [dbo].[WareHouseItems] ([numWareHouseItemID])
GO
ALTER TABLE [dbo].[OpportunityItems] CHECK CONSTRAINT [FK_OpportunityItems_WareHouseItems]
GO
