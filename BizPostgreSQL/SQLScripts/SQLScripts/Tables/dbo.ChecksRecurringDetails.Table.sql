/****** Object:  Table [dbo].[ChecksRecurringDetails]    Script Date: 07/26/2008 17:22:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChecksRecurringDetails](
	[numCheckDetId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numCheckId] [numeric](18, 0) NULL,
	[dtRecurringDate] [datetime] NULL,
 CONSTRAINT [PK_ChecksRecurringDetails] PRIMARY KEY CLUSTERED 
(
	[numCheckDetId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ChecksRecurringDetails]  WITH CHECK ADD  CONSTRAINT [FK_ChecksRecurringDetails_CheckDetails] FOREIGN KEY([numCheckId])
REFERENCES [dbo].[CheckDetails] ([numCheckId])
GO
ALTER TABLE [dbo].[ChecksRecurringDetails] CHECK CONSTRAINT [FK_ChecksRecurringDetails_CheckDetails]
GO
