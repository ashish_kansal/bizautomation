/****** Object:  Table [dbo].[ProjectsOpportunities]    Script Date: 07/26/2008 17:28:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProjectsOpportunities](
	[numProId] [numeric](18, 0) NULL,
	[numOppId] [numeric](18, 0) NULL,
	[numDomainId] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
