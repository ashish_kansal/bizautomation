/****** Object:  Table [dbo].[Recurrence]    Script Date: 07/26/2008 17:28:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Recurrence](
	[RecurrenceID] [int] IDENTITY(1,1) NOT NULL,
	[EndDateUtc] [datetime] NOT NULL CONSTRAINT [DF_Recurrence_EndDateUtc]  DEFAULT ('9999-12-31 23:59:59'),
	[DayOfWeekMaskUtc] [int] NOT NULL CONSTRAINT [DF_Recurrence_DayOfWeekMaskUtc]  DEFAULT ((0)),
	[UtcOffset] [int] NOT NULL CONSTRAINT [DF_Recurrence_UtcOffset]  DEFAULT ((0)),
	[DayOfMonth] [int] NOT NULL CONSTRAINT [DF_Recurrence_DayOfMonth]  DEFAULT ((0)),
	[MonthOfYear] [int] NOT NULL CONSTRAINT [DF_Recurrence_MonthOfYear]  DEFAULT ((0)),
	[PeriodMultiple] [int] NOT NULL CONSTRAINT [DF_Recurrence_PeriodMultiple]  DEFAULT ((1)),
	[Period] [nchar](1) NOT NULL CONSTRAINT [DF_Recurrence_Period]  DEFAULT ('W'),
	[EditType] [int] NOT NULL CONSTRAINT [DF_Recurrence_EditType]  DEFAULT ((0)),
	[LastReminderDateTimeUtc] [datetime] NULL,
	[_ts] [timestamp] NULL,
 CONSTRAINT [Recurrence_PK] PRIMARY KEY NONCLUSTERED 
(
	[RecurrenceID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
