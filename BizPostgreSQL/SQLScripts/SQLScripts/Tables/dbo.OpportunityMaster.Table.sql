/****** Object:  Table [dbo].[OpportunityMaster]    Script Date: 07/26/2008 17:27:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OpportunityMaster](
	[numOppId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[tintOppType] [tinyint] NULL,
	[numContactId] [numeric](18, 0) NULL,
	[numDivisionId] [numeric](18, 0) NOT NULL,
	[bintAccountClosingDate] [datetime] NULL,
	[txtComments] [text] NULL CONSTRAINT [DF_OpportunityMaster_txtComments]  DEFAULT (''),
	[bitPublicFlag] [bit] NULL CONSTRAINT [DF_Accounts_Public_Flag]  DEFAULT ((0)),
	[tintSource] [numeric](18, 0) NULL,
	[vcPOppName] [varchar](100) NOT NULL,
	[intPEstimatedCloseDate] [datetime] NULL,
	[numPClosingPercent] [money] NULL,
	[numCampainID] [numeric](18, 0) NULL,
	[monPAmount] [money] NULL CONSTRAINT [DF_OpportunityMaster_monPAmount]  DEFAULT ((0)),
	[lngPConclAnalysis] [numeric](18, 0) NULL,
	[tintActive] [tinyint] NULL CONSTRAINT [DF_OpportunityMaster_tintActive]  DEFAULT ((1)),
	[monShipCost] [money] NULL CONSTRAINT [DF_OpportunityMaster_monShipCost]  DEFAULT ((0)),
	[numShipVia] [numeric](18, 0) NULL,
	[vcTrackingURL] [varchar](1000) NULL,
	[numRecurringId] [numeric](18, 0) NULL,
	[dtLastRecurringDate] [datetime] NULL,
	[numNoTransactions] [numeric](18, 0) NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[bintCreatedDate] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[bintModifiedDate] [datetime] NULL,
	[numDomainId] [numeric](18, 0) NULL,
	[tintBillingTerms] [tinyint] NULL,
	[numBillingDays] [numeric](18, 0) NULL,
	[tintInterestType] [tinyint] NULL,
	[fltInterest] [float] NULL,
	[numRecOwner] [numeric](18, 0) NULL,
	[tintOppStatus] [tinyint] NULL CONSTRAINT [DF_OpportunityMaster_tintOppStatus]  DEFAULT ((0)),
	[tintshipped] [tinyint] NULL CONSTRAINT [DF_OpportunityMaster_tintshipped]  DEFAULT ((0)),
	[bintShippedDate] [datetime] NULL,
	[bitOrder] [bit] NULL CONSTRAINT [DF_OpportunityMaster_bitOrder]  DEFAULT ((0)),
	[decDiscount] [float] NULL,
	[numSalesOrPurType] [numeric](18, 0) NULL,
	[numAssignedTo] [numeric](18, 0) NULL,
	[numAssignedBy] [numeric](18, 0) NULL,
	[numUserClosed] [numeric](18, 0) NULL,
	[tintBillToType] [tinyint] NULL,
	[tintShipToType] [tinyint] NULL,
 CONSTRAINT [PK_Accounts] PRIMARY KEY CLUSTERED 
(
	[numOppId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[OpportunityMaster]  WITH NOCHECK ADD  CONSTRAINT [FK_OpportunityMaster_AdditionalContactsInformation] FOREIGN KEY([numContactId])
REFERENCES [dbo].[AdditionalContactsInformation] ([numContactId])
GO
ALTER TABLE [dbo].[OpportunityMaster] CHECK CONSTRAINT [FK_OpportunityMaster_AdditionalContactsInformation]
GO
ALTER TABLE [dbo].[OpportunityMaster]  WITH NOCHECK ADD  CONSTRAINT [FK_OpportunityMaster_DivisionMaster] FOREIGN KEY([numDivisionId])
REFERENCES [dbo].[DivisionMaster] ([numDivisionID])
GO
ALTER TABLE [dbo].[OpportunityMaster] CHECK CONSTRAINT [FK_OpportunityMaster_DivisionMaster]
GO
