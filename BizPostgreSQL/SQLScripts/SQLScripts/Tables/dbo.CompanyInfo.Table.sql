/****** Object:  Table [dbo].[CompanyInfo]    Script Date: 07/26/2008 17:22:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CompanyInfo](
	[numCompanyId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[vcCompanyName] [varchar](100) NULL,
	[numCompanyType] [numeric](18, 0) NULL CONSTRAINT [DF_CompanyInfo_numCompanyType]  DEFAULT (0),
	[numCompanyRating] [numeric](18, 0) NULL,
	[numCompanyIndustry] [numeric](18, 0) NULL,
	[numCompanyCredit] [numeric](18, 0) NULL,
	[txtComments] [text] NULL CONSTRAINT [DF_CompanyInfo_txtComments]  DEFAULT (''),
	[vcWebSite] [varchar](255) NULL CONSTRAINT [DF_CompanyInfo_vcWebSite]  DEFAULT (''),
	[vcWebLabel1] [varchar](100) NULL,
	[vcWebLink1] [varchar](255) NULL,
	[vcWebLabel2] [varchar](100) NULL,
	[vcWebLink2] [varchar](255) NULL,
	[vcWebLabel3] [varchar](100) NULL,
	[vcWebLink3] [varchar](255) NULL,
	[vcWeblabel4] [varchar](100) NULL,
	[vcWebLink4] [varchar](255) NULL,
	[numAnnualRevID] [numeric](18, 0) NULL,
	[numNoOfEmployeesId] [numeric](18, 0) NULL,
	[vcHow] [numeric](18, 0) NULL,
	[vcProfile] [numeric](18, 0) NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[bintCreatedDate] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[bintModifiedDate] [datetime] NULL,
	[bitPublicFlag] [bit] NULL CONSTRAINT [DF_CompanyInfo_bitPublicFlag]  DEFAULT (0),
	[numDomainID] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_CompanyInfo] PRIMARY KEY CLUSTERED 
(
	[numCompanyId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[CompanyInfo]  WITH NOCHECK ADD  CONSTRAINT [FK_CompanyInfo_Domain] FOREIGN KEY([numDomainID])
REFERENCES [dbo].[Domain] ([numDomainId])
GO
ALTER TABLE [dbo].[CompanyInfo] CHECK CONSTRAINT [FK_CompanyInfo_Domain]
GO
