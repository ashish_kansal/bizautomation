/****** Object:  Table [dbo].[AlertContactsAndCompany]    Script Date: 07/26/2008 17:20:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AlertContactsAndCompany](
	[numAlertConID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numAlerDTLId] [numeric](18, 0) NULL,
	[numContactID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_AlertContactsAndCompany] PRIMARY KEY CLUSTERED 
(
	[numAlertConID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
