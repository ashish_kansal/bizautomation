/****** Object:  Table [dbo].[BizDocAttachments]    Script Date: 07/26/2008 17:20:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BizDocAttachments](
	[numAttachmntID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numBizDocID] [numeric](18, 0) NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[vcURL] [varchar](1000) NULL,
	[numOrder] [numeric](18, 0) NULL,
	[vcDocName] [varchar](50) NULL,
 CONSTRAINT [PK_BizDocAttachments] PRIMARY KEY CLUSTERED 
(
	[numAttachmntID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
