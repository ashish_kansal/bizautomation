/****** Object:  Table [dbo].[DynamicFormFieldMaster]    Script Date: 07/26/2008 17:24:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DynamicFormFieldMaster](
	[numFormFieldId] [numeric](10, 0) IDENTITY(1,1) NOT NULL,
	[numFormID] [numeric](18, 0) NULL,
	[vcFormFieldName] [nvarchar](50) NOT NULL,
	[vcFieldType] [char](1) NOT NULL,
	[vcAssociatedControlType] [nvarchar](50) NOT NULL,
	[vcDbColumnName] [nvarchar](50) NOT NULL,
	[vcListItemType] [char](1) NULL,
	[numListID] [numeric](18, 0) NOT NULL,
	[bitDeleted] [bit] NOT NULL,
	[vcFieldDataType] [char](1) NOT NULL,
	[vcOrigDbColumnName] [nvarchar](50) NULL,
	[bitAllowEdit] [bit] NOT NULL CONSTRAINT [DF_DynamicFormFieldMaster_bitAllowEdit]  DEFAULT (0),
	[vcLookBackTableName] [nvarchar](50) NULL,
	[bitDefault] [bit] NOT NULL CONSTRAINT [DF_DynamicFormFieldMaster_bitDefault]  DEFAULT ((0)),
	[order] [tinyint] NULL,
	[bitInResults] [bit] NOT NULL CONSTRAINT [DF_DynamicFormFieldMaster_bitInResults]  DEFAULT ((1)),
 CONSTRAINT [PK_DynamicFormFieldMaster] PRIMARY KEY CLUSTERED 
(
	[numFormFieldId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
