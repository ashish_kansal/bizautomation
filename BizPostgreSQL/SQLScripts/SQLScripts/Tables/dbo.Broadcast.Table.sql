/****** Object:  Table [dbo].[Broadcast]    Script Date: 07/26/2008 17:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Broadcast](
	[numBroadCastId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[vcBroadCastName] [varchar](500) NOT NULL,
	[vcSubject] [varchar](500) NULL,
	[txtMessage] [text] NULL,
	[numEmailTemplateID] [numeric](18, 0) NULL,
	[numTotalRecipients] [numeric](18, 0) NULL,
	[numTotalSussessfull] [numeric](18, 0) NULL,
	[bintBroadCastDate] [datetime] NULL,
	[numBroadCastBy] [numeric](18, 0) NULL,
	[numDomainID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_Broadcast] PRIMARY KEY CLUSTERED 
(
	[numBroadCastId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
