/****** Object:  Table [dbo].[Item]    Script Date: 07/26/2008 17:26:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Item](
	[numItemCode] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[vcItemName] [varchar](300) NOT NULL,
	[txtItemDesc] [varchar](1000) NULL,
	[charItemType] [char](1) NULL,
	[monListPrice] [money] NULL,
	[numItemClassification] [numeric](18, 0) NULL,
	[bitTaxable] [bit] NULL,
	[vcSKU] [varchar](50) NULL,
	[bitKitParent] [bit] NULL,
	[dtDateEntered] [datetime] NULL,
	[numVendorID] [numeric](18, 0) NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[bintCreatedDate] [datetime] NULL,
	[bintModifiedDate] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[vcPathForImage] [varchar](300) NULL,
	[bitAllowBackOrder] [bit] NULL CONSTRAINT [DF_Item_bitShowOnHandPortal]  DEFAULT ((1)),
	[bitSerialized] [bit] NULL,
	[vcModelID] [varchar](200) NULL,
	[numItemGroup] [numeric](18, 0) NULL,
	[numCOGsChartAcntId] [numeric](18, 0) NULL,
	[numAssetChartAcntId] [numeric](18, 0) NULL,
	[numIncomeChartAcntId] [numeric](18, 0) NULL,
	[monAverageCost] [money] NULL,
	[monCampaignLabourCost] [money] NULL,
	[intWeight] [int] NULL,
	[intHeight] [int] NULL,
	[intWidth] [int] NULL,
	[intLength] [int] NULL,
	[bitFreeShipping] [bit] NULL,
	[vcPathForTImage] [varchar](300) NULL,
	[vcUnitofMeasure] [varchar](30) NULL,
	[bitShowDeptItem] [bit] NULL,
	[bitShowDeptItemDesc] [bit] NULL,
	[bitCalAmtBasedonDepItems] [bit] NULL,
	[bitAssembly] [bit] NULL,
 CONSTRAINT [PK_Item] PRIMARY KEY CLUSTERED 
(
	[numItemCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [FK_Item_Chart_Of_Accounts] FOREIGN KEY([numCOGsChartAcntId])
REFERENCES [dbo].[Chart_Of_Accounts] ([numAccountId])
GO
ALTER TABLE [dbo].[Item] CHECK CONSTRAINT [FK_Item_Chart_Of_Accounts]
GO
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [FK_Item_Chart_Of_Accounts1] FOREIGN KEY([numAssetChartAcntId])
REFERENCES [dbo].[Chart_Of_Accounts] ([numAccountId])
GO
ALTER TABLE [dbo].[Item] CHECK CONSTRAINT [FK_Item_Chart_Of_Accounts1]
GO
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [FK_Item_Chart_Of_Accounts2] FOREIGN KEY([numIncomeChartAcntId])
REFERENCES [dbo].[Chart_Of_Accounts] ([numAccountId])
GO
ALTER TABLE [dbo].[Item] CHECK CONSTRAINT [FK_Item_Chart_Of_Accounts2]
GO
