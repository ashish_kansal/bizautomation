/****** Object:  Table [dbo].[ReportFields]    Script Date: 07/26/2008 17:29:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ReportFields](
	[numRepFieldID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[vcFieldName] [varchar](100) NOT NULL,
	[vcFieldValue] [varchar](100) NULL,
	[tintFldType] [tinyint] NULL,
	[tintSelected] [tinyint] NULL CONSTRAINT [DF_ReportFields_tintSelected]  DEFAULT (0),
 CONSTRAINT [PK_ReportFields] PRIMARY KEY CLUSTERED 
(
	[numRepFieldID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
