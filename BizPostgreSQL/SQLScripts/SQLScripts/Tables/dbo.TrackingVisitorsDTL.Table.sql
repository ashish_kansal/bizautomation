/****** Object:  Table [dbo].[TrackingVisitorsDTL]    Script Date: 07/26/2008 17:31:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TrackingVisitorsDTL](
	[numTracVisitorsDTLID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numTracVisitorsHDRID] [numeric](18, 0) NULL,
	[vcPageName] [varchar](500) NULL,
	[vcElapsedTime] [varchar](100) NULL,
	[numPageID] [numeric](18, 0) NULL,
	[dtTime] [datetime] NULL,
 CONSTRAINT [PK_TrackingVisitorsDTL] PRIMARY KEY CLUSTERED 
(
	[numTracVisitorsDTLID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[TrackingVisitorsDTL]  WITH CHECK ADD  CONSTRAINT [FK_TrackingVisitorsDTL_PageMasterWebAnlys] FOREIGN KEY([numPageID])
REFERENCES [dbo].[PageMasterWebAnlys] ([numPageID])
GO
ALTER TABLE [dbo].[TrackingVisitorsDTL] CHECK CONSTRAINT [FK_TrackingVisitorsDTL_PageMasterWebAnlys]
GO
ALTER TABLE [dbo].[TrackingVisitorsDTL]  WITH CHECK ADD  CONSTRAINT [FK_TrackingVisitorsDTL_TrackingVisitorsHDR] FOREIGN KEY([numTracVisitorsHDRID])
REFERENCES [dbo].[TrackingVisitorsHDR] ([numTrackingID])
GO
ALTER TABLE [dbo].[TrackingVisitorsDTL] CHECK CONSTRAINT [FK_TrackingVisitorsDTL_TrackingVisitorsHDR]
GO
