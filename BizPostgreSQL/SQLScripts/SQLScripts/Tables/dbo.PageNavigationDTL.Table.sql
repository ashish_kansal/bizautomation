/****** Object:  Table [dbo].[PageNavigationDTL]    Script Date: 07/26/2008 17:28:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PageNavigationDTL](
	[numPageNavID] [numeric](18, 0) NOT NULL,
	[numModuleID] [numeric](18, 0) NULL,
	[numParentID] [numeric](18, 0) NULL,
	[vcPageNavName] [varchar](300) NULL,
	[vcNavURL] [varchar](1000) NULL,
	[vcImageURL] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
