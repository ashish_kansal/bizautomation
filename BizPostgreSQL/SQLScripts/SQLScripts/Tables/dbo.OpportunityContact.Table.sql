/****** Object:  Table [dbo].[OpportunityContact]    Script Date: 07/26/2008 17:27:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OpportunityContact](
	[numOppContactId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numOppId] [numeric](18, 0) NOT NULL,
	[numContactId] [numeric](18, 0) NOT NULL,
	[numRole] [numeric](18, 0) NULL,
	[bitPartner] [bit] NULL,
 CONSTRAINT [PK_OpportunityContact] PRIMARY KEY CLUSTERED 
(
	[numOppContactId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OpportunityContact]  WITH CHECK ADD  CONSTRAINT [FK_OpportunityContact_AdditionalContactsInformation] FOREIGN KEY([numContactId])
REFERENCES [dbo].[AdditionalContactsInformation] ([numContactId])
GO
ALTER TABLE [dbo].[OpportunityContact] CHECK CONSTRAINT [FK_OpportunityContact_AdditionalContactsInformation]
GO
ALTER TABLE [dbo].[OpportunityContact]  WITH NOCHECK ADD  CONSTRAINT [FK_OpportunityContact_OpportunityMaster] FOREIGN KEY([numOppId])
REFERENCES [dbo].[OpportunityMaster] ([numOppId])
GO
ALTER TABLE [dbo].[OpportunityContact] CHECK CONSTRAINT [FK_OpportunityContact_OpportunityMaster]
GO
