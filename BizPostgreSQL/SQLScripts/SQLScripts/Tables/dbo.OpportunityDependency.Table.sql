/****** Object:  Table [dbo].[OpportunityDependency]    Script Date: 07/26/2008 17:27:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OpportunityDependency](
	[numDependencyId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numOpportunityId] [numeric](18, 0) NOT NULL,
	[numOppStageId] [numeric](18, 0) NOT NULL,
	[numProcessStageId] [numeric](18, 0) NOT NULL,
	[tintType] [tinyint] NULL,
	[vcType] [varchar](1) NOT NULL,
 CONSTRAINT [PK_OpportunityDepdency] PRIMARY KEY CLUSTERED 
(
	[numDependencyId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[OpportunityDependency]  WITH NOCHECK ADD  CONSTRAINT [FK_OpportunityDependency_OpportunityMaster] FOREIGN KEY([numOpportunityId])
REFERENCES [dbo].[OpportunityMaster] ([numOppId])
GO
ALTER TABLE [dbo].[OpportunityDependency] CHECK CONSTRAINT [FK_OpportunityDependency_OpportunityMaster]
GO
