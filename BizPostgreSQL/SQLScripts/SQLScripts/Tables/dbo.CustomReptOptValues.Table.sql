/****** Object:  Table [dbo].[CustomReptOptValues]    Script Date: 07/26/2008 17:23:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CustomReptOptValues](
	[intCustomDataOpt] [int] IDENTITY(1,1) NOT NULL,
	[intOptions] [int] NOT NULL,
	[vcSQLValue] [varchar](100) NOT NULL,
	[vcOptionName] [varchar](100) NOT NULL,
 CONSTRAINT [PK_CustomReptOptValues] PRIMARY KEY CLUSTERED 
(
	[intCustomDataOpt] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
