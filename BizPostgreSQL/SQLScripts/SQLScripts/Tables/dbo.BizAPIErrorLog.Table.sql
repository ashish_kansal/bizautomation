GO
/****** Object:  Table [dbo].[BizAPIErrorLog]    Script Date: 04/15/2014 15:06:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BizAPIErrorLog](
	[vcErrorID] [int] IDENTITY(1,1) NOT NULL,
	[vcType] [nvarchar](100) NULL,
	[vcSource] [nvarchar](100) NULL,
	[vcMessage] [nvarchar](max) NULL,
	[vcStackStrace] [nvarchar](max) NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_BizAPIErrorLog] PRIMARY KEY CLUSTERED 
(
	[vcErrorID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]