

/****** Object:  Table [dbo].[ApprovalConfig]    Script Date: 4/25/2016 6:53:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ApprovalConfig](
	[numConfigID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[numModule] [numeric](18, 0) NULL,
	[numUserId] [numeric](18, 0) NULL,
	[numLevel1Authority] [numeric](18, 0) NULL,
	[numLevel2Authority] [numeric](18, 0) NULL,
	[numLevel3Authority] [numeric](18, 0) NULL,
	[numLevel4Authority] [numeric](18, 0) NULL,
	[numLevel5Authority] [numeric](18, 0) NULL,
 CONSTRAINT [PK_ApprovalConfig] PRIMARY KEY CLUSTERED 
(
	[numConfigID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


