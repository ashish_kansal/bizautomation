/****** Object:  Table [dbo].[WebApiOrderReports]    Script Date: 05/26/2012 15:01:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WebApiOrderReports](
	[OrdReportId] [int] IDENTITY(1,1) NOT NULL,
	[numDomainId] [bigint] NOT NULL,
	[WebApiId] [int] NOT NULL,
	[vcWebApiOrderReportId] [varbinary](50) NOT NULL,
	[tintStatus] [tinyint] NOT NULL CONSTRAINT [DF_WebApiOrderReports_tintStatus]  DEFAULT ((0)),
	[dtCreated] [datetime] NOT NULL CONSTRAINT [DF_WebApiOrderReports_dtCreated]  DEFAULT (getdate()),
	[dtModified] [datetime] NULL,
	[RStatus] [tinyint] NOT NULL CONSTRAINT [DF_WebApiOrderReports_RStatus]  DEFAULT ((0)),
 CONSTRAINT [PK_WebApiOrderReports] PRIMARY KEY CLUSTERED 
(
	[OrdReportId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [IX_WebApiOrderReports] UNIQUE NONCLUSTERED 
(
	[vcWebApiOrderReportId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON