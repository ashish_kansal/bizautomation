/****** Object:  Table [dbo].[ECampaign]    Script Date: 07/26/2008 17:24:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ECampaign](
	[numECampaignID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[vcECampName] [varchar](100) NOT NULL,
	[txtDesc] [text] NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[dtCreatedOn] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[dtModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_ECampaign] PRIMARY KEY CLUSTERED 
(
	[numECampaignID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
