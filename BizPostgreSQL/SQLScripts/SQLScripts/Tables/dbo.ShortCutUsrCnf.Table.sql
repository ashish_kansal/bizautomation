/****** Object:  Table [dbo].[ShortCutUsrCnf]    Script Date: 07/26/2008 17:29:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ShortCutUsrCnf](
	[numGroupId] [numeric](18, 0) NULL,
	[numLinkId] [numeric](18, 0) NULL,
	[numOrder] [numeric](18, 0) NULL,
	[bitType] [bit] NULL,
	[numDomainId] [numeric](18, 0) NULL,
	[numContactID] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
