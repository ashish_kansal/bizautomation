/****** Object:  Table [dbo].[EmailHStrToBCCAndCC]    Script Date: 07/26/2008 17:24:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmailHStrToBCCAndCC](
	[numEmailHstrDTLID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numEmailHstrID] [numeric](18, 0) NULL,
	[vcName] [varchar](100) NULL,
	[tintType] [tinyint] NULL,
	[numDivisionId] [numeric](18, 0) NULL,
	[numEmailId] [numeric](18, 0) NOT NULL CONSTRAINT [DF_EmailHStrToBCCAndCC_numEmailId]  DEFAULT ((0)),
 CONSTRAINT [PK_EmailHStrToBCCAndCC] PRIMARY KEY CLUSTERED 
(
	[numEmailHstrDTLID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
