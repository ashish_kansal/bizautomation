/****** Object:  Table [dbo].[CustReptModuleMaster]    Script Date: 07/26/2008 17:23:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CustReptModuleMaster](
	[numCustModuleId] [numeric](18, 0) NOT NULL,
	[vcCustModuleName] [varchar](100) NULL,
 CONSTRAINT [PK_CustReptModuleMaster] PRIMARY KEY CLUSTERED 
(
	[numCustModuleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
