GO
/****** Object:  Table [dbo].[SalesOrderConfiguration]    Script Date: 05/30/2014 13:53:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SalesOrderConfiguration](
	[numSOCID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[bitAutoFocusCustomer] [bit] NULL,
	[bitAutoFocusItem] [bit] NULL,
	[bitDisplayRootLocation] [bit] NULL,
	[bitAutoAssignOrder] [bit] NULL,
	[bitDisplayLocation] [bit] NULL,
	[bitDisplayFinancialStamp] [bit] NULL,
	[bitDisplayItemDetails] [bit] NULL,
	[bitDisplayUnitCost] [bit] NULL,
	[bitDisplayProfitTotal] [bit] NULL,
	[bitDisplayShippingRates] [bit] NULL,
	[bitDisplayPaymentMethods] [bit] NULL,
	[bitCreateOpenBizDoc] [bit] NULL,
	[numListItemID] [numeric](18, 0) NULL,
	[vcPaymentMethodIDs] [varchar](100) NULL,
 CONSTRAINT [PK_SalesOrderConfiguration] PRIMARY KEY CLUSTERED 
(
	[numSOCID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON