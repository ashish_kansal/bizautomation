/****** Object:  Table [dbo].[eCommerceDTL]    Script Date: 07/26/2008 17:24:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[eCommerceDTL](
	[numDomainId] [numeric](18, 0) NULL,
	[vcPaymentGateWay] [smallint] NULL,
	[vcPGWManagerUId] [varchar](100) NULL,
	[vcPGWManagerPassword] [varchar](100) NULL,
	[bitShowInStock] [bit] NULL,
	[bitShowQOnHand] [bit] NULL,
	[tintItemColumns] [tinyint] NULL,
	[vcStyleSheet] [varchar](100) NULL,
	[txtHeader] [ntext] NULL,
	[txtFooter] [ntext] NULL,
	[numDefaultCategory] [numeric](18, 0) NULL,
	[bitHideNewUsers] [bit] NULL,
	[bitCheckCreditStatus] [bit] NULL CONSTRAINT [DF_eCommerceDTL_bitCheckCreditStatus]  DEFAULT ((1))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
