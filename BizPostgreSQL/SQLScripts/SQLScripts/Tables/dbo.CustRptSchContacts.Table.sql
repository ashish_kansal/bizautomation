/****** Object:  Table [dbo].[CustRptSchContacts]    Script Date: 07/26/2008 17:23:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustRptSchContacts](
	[numId] [numeric](18, 0) NULL,
	[numScheduleId] [numeric](18, 0) NULL,
	[numContactId] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
