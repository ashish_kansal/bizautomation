/****** Object:  Table [dbo].[SurveyMaster]    Script Date: 07/26/2008 17:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SurveyMaster](
	[numSurID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[vcSurName] [varchar](100) NOT NULL,
	[vcRedirectURL] [varchar](255) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numCreatedBy] [numeric](18, 0) NOT NULL,
	[bintCreatedOn] [datetime] NOT NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[bintModifiedOn] [datetime] NULL,
	[bitSkipRegistration] [bit] NOT NULL,
	[bitRandomAnswers] [bit] NOT NULL,
	[bitSinglePageSurvey] [bit] NOT NULL,
	[bitPostRegistration] [bit] NOT NULL DEFAULT (0),
 CONSTRAINT [PK_SurveyMaster] PRIMARY KEY CLUSTERED 
(
	[numSurID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
