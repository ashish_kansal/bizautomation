/****** Object:  Table [dbo].[CustomReport]    Script Date: 07/26/2008 17:23:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CustomReport](
	[numCustomReportID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[vcReportName] [varchar](200) NULL,
	[vcReportDescription] [varchar](500) NULL,
	[textQuery] [text] NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[numModuleId] [numeric](18, 0) NULL,
	[numGroupId] [numeric](18, 0) NULL,
	[bitFilterRelAnd] [bit] NOT NULL CONSTRAINT [DF_CustomReport_bitFilterRelAnd]  DEFAULT ((1)),
	[varstdFieldId] [varchar](200) NULL,
	[custFilter] [varchar](200) NULL,
	[FromDate] [datetime] NULL,
	[ToDate] [datetime] NULL,
	[bintRows] [numeric](18, 0) NULL,
	[bitGridType] [bit] NOT NULL CONSTRAINT [DF_CustomReport_bitGridType]  DEFAULT ((1)),
	[varGrpfld] [varchar](200) NULL,
	[varGrpOrd] [varchar](200) NULL,
	[varGrpflt] [varchar](200) NULL,
	[textQueryGrp] [text] NULL,
	[FilterType] [tinyint] NULL CONSTRAINT [DF_CustomReport_FilterType]  DEFAULT ((0)),
	[AdvFilterStr] [varchar](max) NULL,
	[OrderbyFld] [varchar](200) NOT NULL CONSTRAINT [DF_CustomReport_OrderbyFld]  DEFAULT ((0)),
	[Orderf] [varchar](200) NOT NULL CONSTRAINT [DF_CustomReport_Orderf]  DEFAULT ('Asc'),
	[MttId] [int] NOT NULL CONSTRAINT [DF_CustomReport_MttId]  DEFAULT ((0)),
	[MttValue] [varchar](200) NOT NULL CONSTRAINT [DF_CustomReport_MttValue]  DEFAULT ((0)),
	[CompFilter1] [varchar](200) NOT NULL CONSTRAINT [DF_CustomReport_CompFilter1]  DEFAULT ((0)),
	[CompFilter2] [varchar](200) NOT NULL CONSTRAINT [DF_CustomReport_CompFilter2]  DEFAULT ((0)),
	[CompFilterOpr] [varchar](200) NOT NULL CONSTRAINT [DF_CustomReport_CompFilterOpr]  DEFAULT ((0)),
 CONSTRAINT [PK_CustomReport] PRIMARY KEY CLUSTERED 
(
	[numCustomReportID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[CustomReport]  WITH NOCHECK ADD  CONSTRAINT [FK_CustomReport_Domain] FOREIGN KEY([numDomainID])
REFERENCES [dbo].[Domain] ([numDomainId])
GO
ALTER TABLE [dbo].[CustomReport] CHECK CONSTRAINT [FK_CustomReport_Domain]
GO
