
GO
/****** Object:  Table [dbo].[BizDocAction]    Script Date: 02/15/2010 23:54:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BizDocAction](
	[numBizActionId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[bitTask] [numeric](18, 0) NULL CONSTRAINT [DF_BizDocAction_vcTask]  DEFAULT ((0)),
	[numContactId] [numeric](18, 0) NOT NULL,
	[numDivisionId] [numeric](18, 0) NOT NULL,
	[numStatus] [numeric](18, 0) NULL,
	[numActivity] [numeric](18, 0) NULL,
	[numAssign] [numeric](18, 0) NOT NULL CONSTRAINT [DF_BizDocAction_numAssign]  DEFAULT ((0)),
	[numCreatedBy] [numeric](18, 0) NOT NULL,
	[dtCreatedDate] [datetime] NOT NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[dtModifiedDate] [datetime] NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[numBizDocAppId] [numeric](18, 0) NULL
) ON [PRIMARY]
