USE [BizExch2007DB]
GO
/****** Object:  Table [dbo].[WebAPI]    Script Date: 02/18/2009 15:21:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WebAPI](
	[WebApiId] [int] IDENTITY(1,1) NOT NULL,
	[numDomainId] [numeric](18, 0) NOT NULL,
	[numContactId] [numeric](18, 0) NOT NULL,
	[uniqueAPIKey] [uniqueidentifier] NOT NULL CONSTRAINT [DF_WebAPI_uniqueAPIKey]  DEFAULT (newid()),
	[bitStatus] [bit] NOT NULL,
	[intCallLimitPerDay] [int] NULL,
	[dtCreateDate] [datetime] NOT NULL CONSTRAINT [DF_WebAPI_dtCreateDate]  DEFAULT (getdate()),
	[dtEndDate] [datetime] NULL,
 CONSTRAINT [PK_WebAPI] PRIMARY KEY CLUSTERED 
(
	[WebApiId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
