/****** Object:  Table [dbo].[HierChartOfAct]    Script Date: 07/26/2008 17:25:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HierChartOfAct](
	[numHChartOfAcntID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numChartOfAcntID] [numeric](18, 0) NOT NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[vcCategoryName] [varchar](100) NULL,
	[numAcntType] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
