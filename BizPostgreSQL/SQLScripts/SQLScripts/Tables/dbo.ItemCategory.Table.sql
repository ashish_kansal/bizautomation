/****** Object:  Table [dbo].[ItemCategory]    Script Date: 07/26/2008 17:26:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ItemCategory](
	[numItemID] [numeric](18, 0) NULL,
	[numCategoryID] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
