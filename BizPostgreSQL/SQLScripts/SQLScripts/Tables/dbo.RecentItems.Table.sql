/****** Object:  Table [dbo].[RecentItems]    Script Date: 07/26/2008 17:28:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RecentItems](
	[numRecentItemsID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numRecordID] [numeric](18, 0) NULL,
	[chrRecordType] [char](10) NULL,
	[bintVisitedDate] [datetime] NULL,
	[numUserCntID] [numeric](18, 0) NULL,
	[bitDeleted] [bit] NOT NULL CONSTRAINT [DF_RecentItems_bitDeleted]  DEFAULT ((0)),
 CONSTRAINT [PK_RecentItems] PRIMARY KEY CLUSTERED 
(
	[numRecentItemsID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
