/****** Object:  Table [dbo].[AlertDomainDtl]    Script Date: 07/26/2008 17:20:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AlertDomainDtl](
	[numAlertDDTLId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numAlertDTLid] [numeric](18, 0) NULL,
	[numDomainId] [numeric](18, 0) NULL,
	[numEmailTemplate] [numeric](18, 0) NULL,
	[numDaysAfterDue] [numeric](18, 0) NULL,
	[monAmount] [numeric](18, 0) NULL,
	[numBizDocs] [numeric](18, 0) NULL,
	[numDepartment] [numeric](18, 0) NULL,
	[numNoOfItems] [numeric](18, 0) NULL,
	[numAge] [numeric](18, 0) NULL,
	[tintAlertOn] [tinyint] NULL,
 CONSTRAINT [PK_AlertDomainDtl] PRIMARY KEY CLUSTERED 
(
	[numAlertDDTLId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
