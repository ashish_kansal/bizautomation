/****** Object:  Table [dbo].[ItemGroupsDTL]    Script Date: 07/26/2008 17:26:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ItemGroupsDTL](
	[numItemGroupDTL] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numItemGroupID] [numeric](18, 0) NULL,
	[numOppAccAttrID] [numeric](18, 0) NULL,
	[tintType] [tinyint] NULL
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1-Options,acceesories,2-Attributes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ItemGroupsDTL', @level2type=N'COLUMN',@level2name=N'tintType'
GO
