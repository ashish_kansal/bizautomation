/****** Object:  Table [dbo].[RoutingLeads]    Script Date: 07/26/2008 17:29:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RoutingLeads](
	[numRoutID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[vcRoutName] [varchar](50) NOT NULL,
	[tintEqualTo] [tinyint] NOT NULL,
	[numValue] [numeric](18, 0) NULL,
	[numCreatedBy] [numeric](18, 0) NOT NULL,
	[dtCreatedDate] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[dtModifiedDate] [datetime] NULL,
	[bitDefault] [bit] NOT NULL CONSTRAINT [DF_RoutingLeads_bitDefault]  DEFAULT (0),
	[tintPriority] [tinyint] NULL,
	[vcDBColumnName] [varchar](100) NULL,
	[vcSelectedCoulmn] [varchar](200) NULL,
 CONSTRAINT [PK_RoutingLeads] PRIMARY KEY CLUSTERED 
(
	[numRoutID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1=Equal to , 2 like, 3 AOI''s' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RoutingLeads', @level2type=N'COLUMN',@level2name=N'tintEqualTo'
GO
