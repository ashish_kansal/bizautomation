/****** Object:  Table [dbo].[ShortCutGrpConf]    Script Date: 07/26/2008 17:29:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ShortCutGrpConf](
	[numGroupId] [numeric](18, 0) NULL,
	[numLinkId] [numeric](18, 0) NULL,
	[numOrder] [numeric](18, 0) NULL,
	[bitType] [bit] NULL,
	[numDomainId] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
