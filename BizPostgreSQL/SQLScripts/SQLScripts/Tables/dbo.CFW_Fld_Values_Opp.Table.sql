/****** Object:  Table [dbo].[CFW_Fld_Values_Opp]    Script Date: 07/26/2008 17:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CFW_Fld_Values_Opp](
	[Fld_ID] [numeric](18, 0) NOT NULL,
	[Fld_Value] [varchar](1000) NULL,
	[RecId] [numeric](18, 0) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[CFW_Fld_Values_Opp]  WITH CHECK ADD  CONSTRAINT [FK_CFW_Fld_Values_Opp_CFW_Fld_Master] FOREIGN KEY([Fld_ID])
REFERENCES [dbo].[CFW_Fld_Master] ([Fld_id])
GO
ALTER TABLE [dbo].[CFW_Fld_Values_Opp] CHECK CONSTRAINT [FK_CFW_Fld_Values_Opp_CFW_Fld_Master]
GO
