/****** Object:  Table [dbo].[PageLayoutDTL]    Script Date: 07/26/2008 17:27:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PageLayoutDTL](
	[numFieldID] [numeric](18, 0) NULL,
	[tintRow] [tinyint] NULL,
	[intCoulmn] [int] NULL,
	[numUserCntID] [numeric](18, 0) NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[numRelCntType] [numeric](18, 0) NULL CONSTRAINT [DF_PageLayoutDTL_numRelCntType]  DEFAULT (0),
	[bitCustomField] [bit] NULL CONSTRAINT [DF_PageLayoutDTL_bitCustomField]  DEFAULT (0),
	[Ctype] [char](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
