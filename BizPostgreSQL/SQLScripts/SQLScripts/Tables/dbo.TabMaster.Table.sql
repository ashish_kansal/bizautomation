/****** Object:  Table [dbo].[TabMaster]    Script Date: 07/28/2009 21:42:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TabMaster](
	[numTabId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numTabName] [varchar](50) NULL,
	[Remarks] [varchar](50) NULL,
	[tintTabType] [tinyint] NULL,
	[vcURL] [varchar](1000) NULL,
	[bitFixed] [bit] NULL CONSTRAINT [DF_TabMaster_bitEditable]  DEFAULT ((0)),
	[numDomainID] [numeric](18, 0) NULL,
	[numModuleId] [numeric](18, 0) NULL,
 CONSTRAINT [PK_TabMaster] PRIMARY KEY CLUSTERED 
(
	[numTabId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF