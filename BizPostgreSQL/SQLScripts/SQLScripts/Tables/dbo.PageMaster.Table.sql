/****** Object:  Table [dbo].[PageMaster]    Script Date: 07/26/2008 17:27:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PageMaster](
	[numPageID] [numeric](18, 0) NOT NULL,
	[numModuleID] [numeric](18, 0) NOT NULL,
	[vcFileName] [varchar](50) NOT NULL,
	[vcPageDesc] [varchar](100) NULL,
	[bitIsViewApplicable] [int] NOT NULL CONSTRAINT [DF_PageMaster_bitIsViewApplicable]  DEFAULT (0),
	[bitIsAddApplicable] [int] NOT NULL CONSTRAINT [DF_PageMaster_bitIsAddApplicable]  DEFAULT (0),
	[bitIsUpdateApplicable] [int] NOT NULL CONSTRAINT [DF_PageMaster_bitIsUpdateApplicable]  DEFAULT (0),
	[bitIsDeleteApplicable] [int] NOT NULL CONSTRAINT [DF_PageMaster_bitIsDeleteApplicable]  DEFAULT (0),
	[bitIsExportApplicable] [int] NOT NULL CONSTRAINT [DF_PageMaster_bitIsExportApplicable]  DEFAULT (0),
 CONSTRAINT [PK_PageMaster] PRIMARY KEY CLUSTERED 
(
	[numPageID] ASC,
	[numModuleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[PageMaster]  WITH CHECK ADD  CONSTRAINT [FK_PageMaster_ModuleMaster] FOREIGN KEY([numModuleID])
REFERENCES [dbo].[ModuleMaster] ([numModuleID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PageMaster] CHECK CONSTRAINT [FK_PageMaster_ModuleMaster]
GO
