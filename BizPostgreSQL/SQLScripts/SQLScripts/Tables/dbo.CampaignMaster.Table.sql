/****** Object:  Table [dbo].[CampaignMaster]    Script Date: 07/26/2008 17:21:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CampaignMaster](
	[numCampaignId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numCampaign] [numeric](18, 0) NULL,
	[numCampaignStatus] [numeric](18, 0) NULL,
	[numCampaignType] [numeric](18, 0) NULL,
	[intLaunchDate] [datetime] NULL,
	[intEndDate] [datetime] NULL,
	[numRegion] [numeric](18, 0) NULL,
	[numNoSent] [numeric](18, 0) NULL,
	[monCampaignCost] [money] NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[bintCreatedDate] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[bintModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_CampaignMaster] PRIMARY KEY CLUSTERED 
(
	[numCampaignId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
