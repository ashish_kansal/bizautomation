/****** Object:  Table [dbo].[UserTeams]    Script Date: 07/26/2008 17:31:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserTeams](
	[numUserCntID] [numeric](18, 0) NOT NULL,
	[numTeam] [numeric](18, 0) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserTeams]  WITH CHECK ADD  CONSTRAINT [FK_UserTeams_AdditionalContactsInformation] FOREIGN KEY([numUserCntID])
REFERENCES [dbo].[AdditionalContactsInformation] ([numContactId])
GO
ALTER TABLE [dbo].[UserTeams] CHECK CONSTRAINT [FK_UserTeams_AdditionalContactsInformation]
GO
