SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WebAPIDetail](
	[WebApiIdDetId] [int] IDENTITY(1,1) NOT NULL,
	[WebApiId] [int] NOT NULL,
	[numDomainId] [numeric](18, 0) NOT NULL,
	[vcFirstFldValue] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[vcSecondFldValue] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[vcThirdFldValue] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[vcFourthFldValue] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[vcFifthFldValue] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[vcSixthFldValue] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[vcSeventhFldValue] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[vcEighthFldValue] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[vcNinthFldValue] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[vcTenthFldValue] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[bitEnableAPI] [bit] NULL,
	[vcEleventhFldValue] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[numBizDocId] [numeric](18, 0) NULL,
	[numOrderStatus] [numeric](18, 0) NULL,
	[numRecordOwner] [numeric](18, 0) NULL,
	[numAssignTo] [numeric](18, 0) NULL,
	[numWareHouseID] [numeric](18, 0) NULL,
	[numRelationshipId] [numeric](18, 0) NULL,
	[numProfileId] [numeric](18, 0) NULL,
 CONSTRAINT [PK_WebAPIDetail] PRIMARY KEY CLUSTERED 
(
	[WebApiIdDetId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON