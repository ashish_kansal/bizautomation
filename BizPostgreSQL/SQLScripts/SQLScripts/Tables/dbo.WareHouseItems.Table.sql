/****** Object:  Table [dbo].[WareHouseItems]    Script Date: 07/26/2008 17:31:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WareHouseItems](
	[numWareHouseItemID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numItemID] [numeric](18, 0) NULL,
	[numWareHouseID] [numeric](18, 0) NULL,
	[numOnHand] [numeric](18, 0) NULL,
	[numOnOrder] [numeric](18, 0) NULL,
	[numReorder] [numeric](18, 0) NOT NULL CONSTRAINT [DF_WareHouseItems_numReorder]  DEFAULT ((0)),
	[numAllocation] [numeric](18, 0) NULL,
	[numBackOrder] [numeric](18, 0) NULL,
	[monWListPrice] [money] NULL,
 CONSTRAINT [PK_WareHouseItems] PRIMARY KEY CLUSTERED 
(
	[numWareHouseItemID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WareHouseItems]  WITH CHECK ADD  CONSTRAINT [FK_WareHouseItems_Item] FOREIGN KEY([numItemID])
REFERENCES [dbo].[Item] ([numItemCode])
GO
ALTER TABLE [dbo].[WareHouseItems] CHECK CONSTRAINT [FK_WareHouseItems_Item]
GO
