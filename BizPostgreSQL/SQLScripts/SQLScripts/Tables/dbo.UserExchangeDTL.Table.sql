/****** Object:  Table [dbo].[UserExchangeDTL]    Script Date: 07/26/2008 17:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserExchangeDTL](
	[numUserID] [numeric](18, 0) NOT NULL,
	[bitExchangeIntegration] [bit] NULL,
	[bitAccessExchange] [bit] NULL,
	[vcExchUserName] [varchar](100) NULL,
	[vcExchPassword] [varchar](100) NULL,
	[vcExchPath] [varchar](200) NULL,
	[vcExchDomain] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
