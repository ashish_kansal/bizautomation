USE [Production.2014]
GO

/****** Object:  Table [dbo].[ActivityDisplayConfiguration]    Script Date: 3/20/2019 4:10:52 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ActivityDisplayConfiguration](
	[numDisplayId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainId] [numeric](18, 0) NULL,
	[numUserCntId] [numeric](18, 0) NULL,
	[bitTitle] [bit] NULL,
	[bitLocation] [bit] NULL,
	[bitDescription] [bit] NULL,
 CONSTRAINT [PK_ActivityDisplayConfiguration] PRIMARY KEY CLUSTERED 
(
	[numDisplayId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


