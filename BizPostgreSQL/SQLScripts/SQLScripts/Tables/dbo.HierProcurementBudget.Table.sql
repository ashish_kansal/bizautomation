/****** Object:  Table [dbo].[HierProcurementBudget]    Script Date: 07/26/2008 17:25:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HierProcurementBudget](
	[numHProcumentId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numItemGroupId] [numeric](18, 0) NULL,
	[numItemCode] [numeric](18, 0) NULL,
	[vcItemGroupName] [varchar](150) NULL,
	[numFiscalYear] [numeric](18, 0) NULL,
	[numDomainId] [numeric](18, 0) NULL,
 CONSTRAINT [PK_HierProcurementBudget] PRIMARY KEY CLUSTERED 
(
	[numHProcumentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
