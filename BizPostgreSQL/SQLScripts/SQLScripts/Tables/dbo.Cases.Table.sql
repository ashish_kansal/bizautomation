/****** Object:  Table [dbo].[Cases]    Script Date: 07/26/2008 17:21:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cases](
	[numCaseId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numOppId] [numeric](18, 0) NULL,
	[numDivisionID] [numeric](18, 0) NULL,
	[numContactId] [numeric](18, 0) NOT NULL,
	[vcCaseNumber] [varchar](50) NOT NULL,
	[intTargetResolveDate] [datetime] NULL,
	[textSubject] [text] NULL,
	[numStatus] [numeric](18, 0) NULL CONSTRAINT [DF_Cases_vcStatus]  DEFAULT (0),
	[numPriority] [numeric](18, 0) NULL,
	[textDesc] [text] NULL,
	[textInternalComments] [text] NULL,
	[numReason] [numeric](18, 0) NULL,
	[numOrigin] [numeric](18, 0) NULL,
	[numType] [numeric](18, 0) NULL,
	[numRecOwner] [numeric](18, 0) NULL,
	[numCreatedBy] [numeric](18, 0) NOT NULL,
	[bintCreatedDate] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[bintModifiedDate] [datetime] NULL,
	[numDomainID] [numeric](10, 0) NULL,
	[tintSupportKeyType] [tinyint] NULL CONSTRAINT [DF_Cases_supportKeyType]  DEFAULT (0),
	[numAssignedBy] [numeric](18, 0) NULL,
	[numAssignedTo] [numeric](18, 0) NULL,
	[numContractId] [numeric](18, 0) NOT NULL CONSTRAINT [DF_Cases_numContractId]  DEFAULT ((0)),
 CONSTRAINT [PK_Case] PRIMARY KEY CLUSTERED 
(
	[numCaseId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
