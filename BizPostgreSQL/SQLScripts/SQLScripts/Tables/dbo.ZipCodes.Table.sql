/****** Object:  Table [dbo].[ZipCodes]    Script Date: 07/26/2008 17:31:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ZipCodes](
	[ZipCode] [numeric](5, 0) NULL,
	[vcstreet] [varchar](40) NULL,
	[vcState] [varchar](40) NULL,
	[Longitude] [decimal](18, 6) NULL,
	[Latitude] [decimal](18, 6) NULL,
	[vcCity] [varchar](40) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
