/****** Object:  Table [dbo].[CustReportFields]    Script Date: 07/26/2008 17:23:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CustReportFields](
	[numCustomReportId] [numeric](18, 0) NOT NULL,
	[vcFieldName] [varchar](200) NULL,
	[vcValue] [bit] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
