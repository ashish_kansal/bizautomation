/****** Object:  Table [dbo].[InboxTree]    Script Date: 07/26/2008 17:25:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InboxTree](
	[numNodeID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numParentId] [numeric](18, 0) NULL,
	[vcName] [varchar](300) NULL,
	[numDomainId] [numeric](18, 0) NULL,
	[numUserCntId] [numeric](18, 0) NULL,
 CONSTRAINT [PK_InboxTable] PRIMARY KEY CLUSTERED 
(
	[numNodeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
