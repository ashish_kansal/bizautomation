/****** Object:  Table [dbo].[Communication]    Script Date: 07/26/2008 17:22:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Communication](
	[numCommId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[bitTask] [numeric](18, 0) NULL CONSTRAINT [DF_Communication_vcTask]  DEFAULT ((0)),
	[numContactId] [numeric](18, 0) NOT NULL,
	[numDivisionId] [numeric](18, 0) NOT NULL,
	[textDetails] [varchar](7500) NULL,
	[intSnoozeMins] [int] NULL,
	[intRemainderMins] [int] NULL,
	[numStatus] [numeric](18, 0) NULL,
	[numActivity] [numeric](18, 0) NULL,
	[numAssign] [numeric](18, 0) NOT NULL CONSTRAINT [DF_Communication_numAssign]  DEFAULT ((0)),
	[tintSnoozeStatus] [tinyint] NULL,
	[tintRemStatus] [tinyint] NULL,
	[numOppId] [numeric](18, 0) NULL,
	[numCreatedBy] [numeric](18, 0) NOT NULL,
	[dtCreatedDate] [datetime] NOT NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[dtModifiedDate] [datetime] NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[bitClosedFlag] [bit] NULL CONSTRAINT [DF__communica__bitCl__4CCB4BEB]  DEFAULT ((0)),
	[vcCalendarName] [varchar](200) NULL,
	[bitOutlook] [bit] NULL CONSTRAINT [DF_Communication_bitOutlook]  DEFAULT ((0)),
	[dtStartTime] [datetime] NULL,
	[dtEndTime] [datetime] NULL,
	[numAssignedBy] [numeric](18, 0) NULL,
	[bitSendEmailTemp] [bit] NULL CONSTRAINT [DF_Communication_bitSendEmailTemp]  DEFAULT ((0)),
	[numEmailTemplate] [numeric](18, 0) NULL,
	[tintHours] [tinyint] NULL,
	[bitAlert] [bit] NULL,
	[CaseId] [numeric](18, 0) NOT NULL CONSTRAINT [DF_Communication_CaseId]  DEFAULT ((0)),
	[CaseTimeId] [numeric](18, 0) NULL,
	[CaseExpId] [numeric](18, 0) NOT NULL CONSTRAINT [DF_Communication_CaseExpId]  DEFAULT ((0)),
	[numActivityId] [numeric](18, 0) NOT NULL CONSTRAINT [DF_Communication_numActivityId]  DEFAULT ((0)),
	[bitEmailSent] [bit] NOT NULL CONSTRAINT [DF_Communication_bitEmailSent]  DEFAULT ((0)),
 CONSTRAINT [PK_Communication] PRIMARY KEY CLUSTERED 
(
	[numCommId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0-before,1-after due date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Communication', @level2type=N'COLUMN',@level2name=N'bitSendEmailTemp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'CaseId for the Task in case''s' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Communication', @level2type=N'COLUMN',@level2name=N'CaseId'
GO
