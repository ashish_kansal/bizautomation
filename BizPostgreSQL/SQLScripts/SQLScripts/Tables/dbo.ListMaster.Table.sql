/****** Object:  Table [dbo].[ListMaster]    Script Date: 07/26/2008 17:26:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ListMaster](
	[numListID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[vcListName] [varchar](50) NOT NULL,
	[numCreatedBy] [numeric](18, 0) NOT NULL,
	[bintCreatedDate] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[bintModifiedDate] [datetime] NULL,
	[bitDeleted] [bit] NOT NULL CONSTRAINT [DF_ListMaster_bitDeleted]  DEFAULT (0),
	[bitFixed] [bit] NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[bitFlag] [bit] NULL CONSTRAINT [DF_ListMaster_bitFlag]  DEFAULT (0),
 CONSTRAINT [PK_ListMaster] PRIMARY KEY CLUSTERED 
(
	[numListID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
