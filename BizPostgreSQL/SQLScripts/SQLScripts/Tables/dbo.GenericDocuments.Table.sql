/****** Object:  Table [dbo].[GenericDocuments]    Script Date: 07/26/2008 17:25:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GenericDocuments](
	[numGenericDocID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[VcFileName] [varchar](200) NULL,
	[vcDocName] [varchar](50) NOT NULL,
	[numDocCategory] [numeric](18, 0) NULL,
	[cUrlType] [char](1) NULL,
	[vcFileType] [varchar](10) NULL,
	[numBusClass] [numeric](18, 0) NULL,
	[vcDocDesc] [text] NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[bintCreatedDate] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[bintModifiedDate] [datetime] NULL,
	[vcSubject] [varchar](500) NULL,
	[isForMarketingDept] [int] NULL CONSTRAINT [DF__GenericDo__isFor__34556F33]  DEFAULT (0),
 CONSTRAINT [PK_GenericDocuments] PRIMARY KEY CLUSTERED 
(
	[numGenericDocID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
