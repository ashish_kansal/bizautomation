GO
/****** Object:  Table [dbo].[WebApiOppItemDetails]    Script Date: 05/30/2012 13:00:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WebApiOppItemDetails](
	[WebApiOrderItemId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainId] [bigint] NOT NULL,
	[numWebApiId] [int] NOT NULL,
	[numOppId] [numeric](18, 0) NOT NULL,
	[numOppBizDocID] [numeric](18, 0) NOT NULL,
	[vcAPIOppId] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[vcApiOppItemId] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[numCreatedby] [numeric](18, 0) NOT NULL,
	[dtCreated] [datetime] NOT NULL,
	[numModifiedby] [numeric](18, 0) NULL,
	[dtModified] [datetime] NULL,
	[tintStatus] [tinyint] NOT NULL CONSTRAINT [DF_WebApiOppItemDetails_tintStatus]  DEFAULT ((0)),
	[numOppBizDocItemID] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_WebApiOppItemDetails] PRIMARY KEY CLUSTERED 
(
	[WebApiOrderItemId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON