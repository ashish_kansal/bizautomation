/****** Object:  Table [dbo].[PageLayout]    Script Date: 07/26/2008 17:27:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PageLayout](
	[numFieldID] [numeric](18, 0) NOT NULL,
	[vcFieldName] [varchar](100) NULL,
	[vcDBColumnName] [varchar](100) NULL,
	[tintRow] [tinyint] NULL,
	[intColumn] [int] NULL,
	[CType] [char](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
