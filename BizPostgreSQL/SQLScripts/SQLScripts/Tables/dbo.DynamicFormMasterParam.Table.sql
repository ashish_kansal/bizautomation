/****** Object:  Table [dbo].[DynamicFormMasterParam]    Script Date: 07/26/2008 17:24:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DynamicFormMasterParam](
	[numDomainID] [numeric](18, 0) NOT NULL,
	[vcAdditionalParam] [nvarchar](100) NULL,
	[numGrpId] [numeric](18, 0) NULL,
	[numFormID] [numeric](18, 0) NULL,
	[ntxtHeader] [ntext] NULL,
	[ntxtFooter] [ntext] NULL,
	[ntxtLeft] [ntext] NULL,
	[ntxtRight] [ntext] NULL,
	[ntxtStyle] [ntext] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
