/****** Object:  Table [dbo].[TimeAndExpense]    Script Date: 07/26/2008 17:30:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TimeAndExpense](
	[numCategoryHDRID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[tintTEType] [tinyint] NOT NULL CONSTRAINT [DF_TimeAndExpense_sintTEType]  DEFAULT ((0)),
	[numCategory] [tinyint] NOT NULL,
	[numType] [tinyint] NULL,
	[dtFromDate] [datetime] NULL,
	[dtToDate] [datetime] NULL,
	[bitFromFullDay] [bit] NULL,
	[bitToFullDay] [bit] NULL,
	[monAmount] [money] NULL,
	[numOppId] [numeric](18, 0) NULL,
	[numDivisionID] [numeric](18, 0) NULL,
	[txtDesc] [text] NULL,
	[numUserCntID] [numeric](18, 0) NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[bitApproved] [bit] NULL CONSTRAINT [DF_TimeAndExpense_bitApproved]  DEFAULT ((0)),
	[numCaseId] [numeric](18, 0) NULL CONSTRAINT [DF_TimeAndExpense_numCaseId]  DEFAULT ((0)),
	[numProId] [numeric](18, 0) NULL CONSTRAINT [DF_TimeAndExpense_numProId]  DEFAULT ((0)),
	[numStageId] [numeric](18, 0) NULL,
	[numContractId] [numeric](18, 0) NULL CONSTRAINT [DF_TimeAndExpense_numContractId]  DEFAULT ((0)),
	[numOppBizDocsId] [numeric](18, 0) NULL CONSTRAINT [DF_TimeAndExpense_numOppBizDocsId]  DEFAULT ((0)),
	[numTCreatedBy] [numeric](18, 0) NULL,
	[numTModifiedBy] [numeric](18, 0) NULL,
	[dtTCreatedOn] [datetime] NULL,
	[dtTModifiedOn] [datetime] NULL,
	[bitReimburse] [bit] NOT NULL CONSTRAINT [DF_TimeAndExpense_bitReimburse]  DEFAULT ((0)),
 CONSTRAINT [PK_TimeAndExpense] PRIMARY KEY CLUSTERED 
(
	[numCategoryHDRID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
