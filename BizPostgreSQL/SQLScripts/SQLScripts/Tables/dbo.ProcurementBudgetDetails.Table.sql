/****** Object:  Table [dbo].[ProcurementBudgetDetails]    Script Date: 07/26/2008 17:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProcurementBudgetDetails](
	[numProcurementDetId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numProcurementId] [numeric](18, 0) NOT NULL,
	[numItemGroupId] [numeric](18, 0) NULL,
	[tintMonth] [tinyint] NULL,
	[intYear] [int] NULL,
	[monAmount] [money] NULL,
	[vcComments] [varchar](1000) NULL,
 CONSTRAINT [PK_ProcurementBudgetDetails] PRIMARY KEY CLUSTERED 
(
	[numProcurementDetId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
