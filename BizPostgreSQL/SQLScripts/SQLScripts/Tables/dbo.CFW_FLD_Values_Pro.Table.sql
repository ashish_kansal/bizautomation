/****** Object:  Table [dbo].[CFW_FLD_Values_Pro]    Script Date: 07/26/2008 17:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CFW_FLD_Values_Pro](
	[Fld_ID] [numeric](18, 0) NOT NULL,
	[Fld_Value] [varchar](100) NULL,
	[RecId] [numeric](18, 0) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
