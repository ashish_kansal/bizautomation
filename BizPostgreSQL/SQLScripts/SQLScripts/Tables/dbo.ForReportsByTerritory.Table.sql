/****** Object:  Table [dbo].[ForReportsByTerritory]    Script Date: 07/26/2008 17:25:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ForReportsByTerritory](
	[numRepID] [numeric](9, 0) IDENTITY(1,1) NOT NULL,
	[numUserCntID] [numeric](9, 0) NULL,
	[numTerritory] [numeric](9, 0) NULL,
	[numDomainID] [numeric](9, 0) NULL,
	[tintType] [numeric](9, 0) NULL
) ON [PRIMARY]
GO
