/****** Object:  Table [dbo].[OpportunityBizDocsPaymentDetails]    Script Date: 07/26/2008 17:27:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OpportunityBizDocsPaymentDetails](
	[numBizDocsPaymentDetailsId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numBizDocsPaymentDetId] [numeric](18, 0) NULL,
	[monAmount] [money] NULL,
	[dtDueDate] [smalldatetime] NULL,
	[bitIntegrated] [bit] NULL,
 CONSTRAINT [PK_OpportunityBizDocsPaymentDetails] PRIMARY KEY CLUSTERED 
(
	[numBizDocsPaymentDetailsId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
