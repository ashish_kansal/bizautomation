USE [Production.2014]
GO

/****** Object:  Table [dbo].[ActivityAttendees]    Script Date: 3/20/2019 4:11:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ActivityAttendees](
	[AttendeeID] [int] IDENTITY(1,1) NOT NULL,
	[ActivityID] [int] NOT NULL,
	[ResponseStatus] [varchar](20) NULL,
	[AttendeeEmail] [varchar](50) NULL,
	[AttendeeFirstName] [varchar](50) NULL,
	[AttendeeLastName] [varchar](50) NULL,
	[AttendeePhone] [varchar](15) NULL,
	[AttendeePhoneExtension] [varchar](7) NULL,
	[CompanyNameinBiz] [varchar](100) NULL,
	[DivisionID] [numeric](18, 0) NULL,
	[tintCRMType] [tinyint] NULL,
	[ContactID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_ActivityAttendees] PRIMARY KEY CLUSTERED 
(
	[AttendeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

ALTER TABLE [dbo].[ActivityAttendees]  WITH CHECK ADD  CONSTRAINT [FK_ActivityID] FOREIGN KEY([ActivityID])
REFERENCES [dbo].[Activity] ([ActivityID])
GO

ALTER TABLE [dbo].[ActivityAttendees] CHECK CONSTRAINT [FK_ActivityID]
GO


