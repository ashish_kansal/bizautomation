/****** Object:  Table [dbo].[ProjectsContacts]    Script Date: 07/26/2008 17:28:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProjectsContacts](
	[numContactTcode] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numProId] [numeric](18, 0) NULL,
	[numContactId] [numeric](18, 0) NULL,
	[numRole] [numeric](18, 0) NULL,
	[bitPartner] [bit] NULL,
 CONSTRAINT [PK_ProjectsContacts] PRIMARY KEY CLUSTERED 
(
	[numContactTcode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ProjectsContacts]  WITH CHECK ADD  CONSTRAINT [FK_ProjectsContacts_AdditionalContactsInformation] FOREIGN KEY([numContactId])
REFERENCES [dbo].[AdditionalContactsInformation] ([numContactId])
GO
ALTER TABLE [dbo].[ProjectsContacts] CHECK CONSTRAINT [FK_ProjectsContacts_AdditionalContactsInformation]
GO
ALTER TABLE [dbo].[ProjectsContacts]  WITH CHECK ADD  CONSTRAINT [FK_ProjectsContacts_ProjectsMaster] FOREIGN KEY([numProId])
REFERENCES [dbo].[ProjectsMaster] ([numProId])
GO
ALTER TABLE [dbo].[ProjectsContacts] CHECK CONSTRAINT [FK_ProjectsContacts_ProjectsMaster]
GO
