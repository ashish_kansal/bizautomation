/****** Object:  Table [dbo].[Domain]    Script Date: 07/26/2008 17:24:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Domain](
	[numDomainId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[vcDomainName] [varchar](50) NOT NULL,
	[vcDomainDesc] [varchar](100) NULL,
	[numDivisionID] [numeric](18, 0) NULL,
	[numAdminID] [numeric](18, 0) NULL,
	[vcBizDocFooter] [varchar](100) NULL,
	[bitExchangeIntegration] [bit] NULL,
	[bitAccessExchange] [bit] NULL,
	[vcExchUserName] [varchar](100) NULL,
	[vcExchPassword] [varchar](100) NULL,
	[vcExchPath] [varchar](200) NULL,
	[vcExchDomain] [varchar](100) NULL,
	[vcCompanyImagePath] [varchar](100) NULL,
	[vcBankImagePath] [varchar](100) NULL,
	[vcBizDocImagePath] [varchar](100) NULL,
	[vcPortalLogo] [varchar](100) NULL,
	[tintCustomPagingRows] [tinyint] NOT NULL CONSTRAINT [DF_Domain_tintCustomPagingRows]  DEFAULT ((20)),
	[vcDateFormat] [varchar](30) NOT NULL CONSTRAINT [DF_Domain_vcDateFormat]  DEFAULT ('MM/DD/YYYY'),
	[numDefCountry] [numeric](18, 0) NOT NULL CONSTRAINT [DF_Domain_numDefCountry]  DEFAULT ((600)),
	[tintComposeWindow] [tinyint] NOT NULL CONSTRAINT [DF_Domain_tintComposeWindow]  DEFAULT ((2)),
	[sintStartDate] [smallint] NOT NULL CONSTRAINT [DF_Domain_sintStartDate]  DEFAULT ((365)),
	[sintNoofDaysInterval] [smallint] NOT NULL CONSTRAINT [DF_Domain_sintNoofDaysInterval]  DEFAULT ((730)),
	[tintAssignToCriteria] [tinyint] NOT NULL CONSTRAINT [DF_Domain_tintAssignToCriteria]  DEFAULT ((0)),
	[bitIntmedPage] [bit] NOT NULL CONSTRAINT [DF_Domain_bitIntmedPage]  DEFAULT ((1)),
	[tintFiscalStartMonth] [tinyint] NOT NULL CONSTRAINT [DF_Domain_tintFiscalStartMonth]  DEFAULT ((1)),
	[bitDeferredIncome] [bit] NULL,
	[tintPayPeriod] [tinyint] NULL,
	[vcCurrency] [varchar](3) NULL,
	[charUnitSystem] [char](1) NULL,
	[tintChrForComSearch] [tinyint] NULL,
	[intPaymentGateWay] [int] NULL,
	[bitPSMTPAuth] [bit] NULL,
	[vcPSmtpPassword] [varchar](100) NULL,
	[vcPSMTPServer] [varchar](500) NULL,
	[numPSMTPPort] [numeric](18, 0) NULL,
	[bitPSMTPSSL] [bit] NULL,
	[bitPSMTPServer] [bit] NULL,
	[vcPSMTPUserName] [varchar](100) NULL,
	[tintChrForItemSearch] [tinyint] NULL,
	[numShipCompany] [numeric](18, 0) NULL,
 CONSTRAINT [PK_Domain] PRIMARY KEY CLUSTERED 
(
	[numDomainId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
