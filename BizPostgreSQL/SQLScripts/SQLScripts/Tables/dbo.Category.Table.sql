/****** Object:  Table [dbo].[Category]    Script Date: 07/26/2008 17:21:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Category](
	[numCategoryID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[vcCategoryName] [varchar](1000) NULL,
	[tintLevel] [tinyint] NULL,
	[numDepCategory] [numeric](18, 0) NULL CONSTRAINT [DF_Category_numDepCategory]  DEFAULT (0),
	[bitLink] [bit] NULL CONSTRAINT [DF_Category_bitLink]  DEFAULT (0),
	[vcLink] [varchar](1000) NULL,
	[numDomainID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[numCategoryID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
