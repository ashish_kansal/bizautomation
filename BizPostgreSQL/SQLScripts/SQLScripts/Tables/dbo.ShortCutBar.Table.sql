/****** Object:  Table [dbo].[ShortCutBar]    Script Date: 07/26/2008 17:29:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ShortCutBar](
	[Id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[vcLinkName] [varchar](100) NULL,
	[Link] [varchar](250) NULL,
	[bitDefault] [bit] NULL,
	[bitType] [bit] NULL,
	[order] [numeric](18, 0) NULL,
	[numGroupId] [numeric](18, 0) NULL,
	[numDomainId] [numeric](18, 0) NULL,
	[numContactId] [numeric](18, 0) NOT NULL CONSTRAINT [DF_ShortCutBar_@numContactId]  DEFAULT ((0))
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Specfies whether Fav or Rec Link' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ShortCutBar', @level2type=N'COLUMN',@level2name=N'bitType'
GO
