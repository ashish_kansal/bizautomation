/****** Object:  Table [dbo].[ListDetails]    Script Date: 07/26/2008 17:26:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ListDetails](
	[numListItemID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numListID] [numeric](18, 0) NULL,
	[vcData] [varchar](50) NOT NULL,
	[numCreatedBY] [numeric](18, 0) NULL,
	[bintCreatedDate] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[bintModifiedDate] [datetime] NULL,
	[bitDelete] [bit] NOT NULL CONSTRAINT [DF_ListDetails_bitDeleted]  DEFAULT ((0)),
	[numDomainID] [numeric](18, 0) NULL,
	[constFlag] [bit] NULL CONSTRAINT [DF_ListDetails_constFlag]  DEFAULT ((0)),
	[sintOrder] [smallint] NULL CONSTRAINT [DF_ListDetails_tintOrder]  DEFAULT ((0)),
 CONSTRAINT [PK_ListDetails] PRIMARY KEY CLUSTERED 
(
	[numListItemID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ListDetails]  WITH NOCHECK ADD  CONSTRAINT [FK_ListDetails_ListMaster] FOREIGN KEY([numListID])
REFERENCES [dbo].[ListMaster] ([numListID])
GO
ALTER TABLE [dbo].[ListDetails] CHECK CONSTRAINT [FK_ListDetails_ListMaster]
GO
