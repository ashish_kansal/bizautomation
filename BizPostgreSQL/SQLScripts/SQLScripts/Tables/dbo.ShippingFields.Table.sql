/****** Object:  Table [dbo].[ShippingFields]    Script Date: 07/26/2008 17:29:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ShippingFields](
	[intShipFieldID] [int] IDENTITY(1,1) NOT NULL,
	[vcFieldName] [varchar](100) NULL,
	[numListItemID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_ShippingFields] PRIMARY KEY CLUSTERED 
(
	[intShipFieldID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
