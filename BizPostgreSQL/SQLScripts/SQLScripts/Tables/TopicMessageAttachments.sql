USE [Production.2014]
GO

/****** Object:  Table [dbo].[TopicMessageAttachments]    Script Date: 16-07-2020 22:02:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TopicMessageAttachments](
	[numAttachmentId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numTopicId] [numeric](18, 0) NULL,
	[numMessageId] [numeric](18, 0) NULL,
	[vcAttachmentName] [varchar](500) NULL,
	[vcAttachmentUrl] [varchar](500) NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[dtmCreatedOn] [datetime] NULL,
	[numUpdatedBy] [numeric](18, 0) NULL,
	[dtmUpdatedOn] [datetime] NULL,
	[numDomainId] [numeric](18, 0) NULL,
 CONSTRAINT [PK_TopicMessageAttachments] PRIMARY KEY CLUSTERED 
(
	[numAttachmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


