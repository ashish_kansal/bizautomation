/****** Object:  Table [dbo].[ForReportsByTeam]    Script Date: 07/26/2008 17:25:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ForReportsByTeam](
	[numRePID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numUserCntID] [numeric](18, 0) NOT NULL,
	[numTeam] [numeric](18, 0) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[tintType] [tinyint] NOT NULL,
 CONSTRAINT [PK_ForReportsByTeam] PRIMARY KEY CLUSTERED 
(
	[numRePID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
