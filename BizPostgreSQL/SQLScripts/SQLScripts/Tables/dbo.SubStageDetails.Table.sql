/****** Object:  Table [dbo].[SubStageDetails]    Script Date: 07/26/2008 17:30:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SubStageDetails](
	[numSubStageDtlID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numProcessListId] [numeric](18, 0) NULL,
	[numSubStageHdrID] [numeric](18, 0) NOT NULL,
	[vcSubStageElemt] [varchar](100) NULL,
 CONSTRAINT [PK_SubStageDetails] PRIMARY KEY CLUSTERED 
(
	[numSubStageDtlID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
