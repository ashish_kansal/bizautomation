
/****** Object:  Table [dbo].[DomainWiseSort]    Script Date: 09/03/2008 01:29:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DomainWiseSort](
	[numDomainWiseSortId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[vcModuleType] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[vcSortOn] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[numCreatedBY] [numeric](18, 0) NULL,
	[bintCreatedDate] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[bintModifiedDate] [datetime] NULL,
	[numDomainId] [numeric](18, 0) NULL,
 CONSTRAINT [PK_DomainWiseSort] PRIMARY KEY CLUSTERED 
(
	[numDomainWiseSortId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF