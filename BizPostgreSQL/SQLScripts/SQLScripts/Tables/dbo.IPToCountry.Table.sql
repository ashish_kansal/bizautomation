/****** Object:  Table [dbo].[IPToCountry]    Script Date: 07/26/2008 17:25:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IPToCountry](
	[IPFrom] [bigint] NULL,
	[IPTo] [bigint] NULL,
	[Con2] [varchar](5) NULL,
	[Con3] [varchar](5) NULL,
	[ConName] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
