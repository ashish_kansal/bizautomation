/****** Object:  Table [dbo].[Subscribers]    Script Date: 07/26/2008 17:30:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Subscribers](
	[numSubscriberID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDivisionID] [numeric](18, 0) NOT NULL,
	[intNoofUsersSubscribed] [int] NULL,
	[intNoOfPartners] [int] NULL,
	[dtSubStartDate] [datetime] NULL,
	[dtSubEndDate] [datetime] NULL,
	[bitTrial] [bit] NOT NULL CONSTRAINT [DF_Subscribers_bitTrial]  DEFAULT ((0)),
	[numAdminContactID] [numeric](18, 0) NULL,
	[bitActive] [bit] NOT NULL CONSTRAINT [DF_Subscribers_bitActive]  DEFAULT ((0)),
	[dtSuspendedDate] [datetime] NULL,
	[vcSuspendedReason] [varchar](1000) NULL,
	[dtLastLoggedIn] [datetime] NULL,
	[numLastLoggedInUser] [numeric](18, 0) NULL,
	[numTargetDomainID] [numeric](18, 0) NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numCreatedBy] [numeric](18, 0) NOT NULL,
	[dtCreatedDate] [datetime] NOT NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[dtModifiedDate] [datetime] NULL,
	[bitDeleted] [bit] NOT NULL CONSTRAINT [DF_Subscribers_bitDeleted]  DEFAULT ((0)),
 CONSTRAINT [PK_Subscribers] PRIMARY KEY CLUSTERED 
(
	[numSubscriberID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
