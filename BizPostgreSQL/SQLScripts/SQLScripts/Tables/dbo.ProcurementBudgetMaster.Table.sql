/****** Object:  Table [dbo].[ProcurementBudgetMaster]    Script Date: 07/26/2008 17:28:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProcurementBudgetMaster](
	[numProcurementBudgetId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[bitPurchaseDeal] [bit] NULL,
	[intFiscalYear] [int] NULL,
	[numDomainId] [numeric](18, 0) NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[dtCreationDate] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[dtModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_ProcurementBudgetMaster] PRIMARY KEY CLUSTERED 
(
	[numProcurementBudgetId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
