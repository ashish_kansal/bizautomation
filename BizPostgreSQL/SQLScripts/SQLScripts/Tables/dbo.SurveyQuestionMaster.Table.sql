/****** Object:  Table [dbo].[SurveyQuestionMaster]    Script Date: 07/26/2008 17:30:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SurveyQuestionMaster](
	[numQID] [numeric](18, 0) NOT NULL,
	[numSurID] [numeric](18, 0) NOT NULL,
	[vcQuestion] [nvarchar](250) NOT NULL,
	[tintAnsType] [tinyint] NOT NULL,
	[intNumOfAns] [int] NOT NULL,
 CONSTRAINT [PK_SurveyQuestionMaster] PRIMARY KEY CLUSTERED 
(
	[numQID] ASC,
	[numSurID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SurveyQuestionMaster]  WITH CHECK ADD  CONSTRAINT [FK_SurveyQuestionMaster_SurveyMaster] FOREIGN KEY([numSurID])
REFERENCES [dbo].[SurveyMaster] ([numSurID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SurveyQuestionMaster] CHECK CONSTRAINT [FK_SurveyQuestionMaster_SurveyMaster]
GO
