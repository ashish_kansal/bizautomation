

/****** Object:  Table [dbo].[Approval_transaction_log]    Script Date: 4/28/2016 11:19:58 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Approval_transaction_log](
	[int_transaction_id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainId] [numeric](18, 0) NULL,
	[numModuleId] [numeric](18, 0) NULL,
	[numRecordId] [numeric](18, 0) NULL,
	[numApprovedBy] [numeric](18, 0) NULL,
	[dtmApprovedOn] [datetime] NULL,
 CONSTRAINT [PK_Approval_transaction_log] PRIMARY KEY CLUSTERED 
(
	[int_transaction_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Approval_transaction_log] ADD  CONSTRAINT [DF_Approval_transaction_log_dtm_approved_on]  DEFAULT (getdate()) FOR [dtmApprovedOn]
GO


