/****** Object:  Table [dbo].[OpportunityAddress]    Script Date: 07/26/2008 17:26:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OpportunityAddress](
	[numOppID] [numeric](18, 0) NULL,
	[vcBillCompanyName] [varchar](100) NULL,
	[vcBillStreet] [varchar](50) NULL,
	[vcBillCity] [varchar](50) NULL,
	[numBillState] [numeric](18, 0) NULL,
	[vcBillPostCode] [varchar](15) NULL,
	[numBillCountry] [numeric](18, 0) NULL,
	[vcShipCompanyName] [varchar](100) NULL,
	[vcShipStreet] [varchar](100) NULL,
	[vcShipCity] [varchar](50) NULL,
	[numShipState] [numeric](18, 0) NULL,
	[vcShipPostCode] [varchar](15) NULL,
	[numShipCountry] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[OpportunityAddress]  WITH CHECK ADD  CONSTRAINT [FK_OpportunityAddress_OpportunityMaster] FOREIGN KEY([numOppID])
REFERENCES [dbo].[OpportunityMaster] ([numOppId])
GO
ALTER TABLE [dbo].[OpportunityAddress] CHECK CONSTRAINT [FK_OpportunityAddress_OpportunityMaster]
GO
