/****** Object:  Table [dbo].[CustRptDefaultFields]    Script Date: 07/26/2008 17:23:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CustRptDefaultFields](
	[numFieldGroupID] [numeric](18, 0) NOT NULL,
	[vcDbFieldName] [nvarchar](160) NOT NULL,
	[vcScrFieldName] [nvarchar](50) NOT NULL,
	[numOrderAppearance] [numeric](18, 0) NOT NULL,
	[boolSummationPossible] [bit] NOT NULL CONSTRAINT [DF_CustRptDefaultFields_boolSummationPossible]  DEFAULT ((0)),
	[boolDefaultColumnSelection] [bit] NOT NULL CONSTRAINT [DF_CustRptDefaultFields_boolDefaultColumnSelection]  DEFAULT ((0)),
	[vcFieldType] [varchar](50) NULL,
	[ListId] [numeric](18, 0) NOT NULL CONSTRAINT [DF_CustRptDefaultFields_ListId]  DEFAULT ((0)),
	[tintMyself] [tinyint] NOT NULL CONSTRAINT [DF_CustRptDefaultFields_tintMyself]  DEFAULT ((0)),
	[tintTeam] [tinyint] NOT NULL CONSTRAINT [DF_CustRptDefaultFields_tintTeam]  DEFAULT ((0)),
	[tintTerr] [tinyint] NOT NULL CONSTRAINT [DF_CustRptDefaultFields_tintTerr]  DEFAULT ((0)),
 CONSTRAINT [PK_CustRptDefaultFields] PRIMARY KEY CLUSTERED 
(
	[numFieldGroupID] ASC,
	[vcDbFieldName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
