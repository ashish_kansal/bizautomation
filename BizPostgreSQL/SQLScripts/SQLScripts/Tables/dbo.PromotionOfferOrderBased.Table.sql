USE [Production.2014]
GO

/****** Object:  Table [dbo].[PromotionOfferOrderBased]    Script Date: 31-01-2018 06:03:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PromotionOfferOrderBased](
	[numProOrderBasedID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numProID] [numeric](18, 0) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numProItemId] [numeric](18, 0) NULL,
	[fltSubTotal] [float] NULL,
	[cSaleType] [char](1) NULL,
	[cItems] [char](1) NULL,
 CONSTRAINT [PK_PromotionOfferOrderBased] PRIMARY KEY CLUSTERED 
(
	[numProOrderBasedID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


