/****** Object:  Table [dbo].[HierarchyArrangeDetails]    Script Date: 07/26/2008 17:25:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HierarchyArrangeDetails](
	[numHierarchyChartAcntId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numAcntId] [numeric](18, 0) NULL,
	[numParentAcntId] [numeric](18, 0) NULL,
	[numBudgetDetId] [numeric](18, 0) NULL,
 CONSTRAINT [PK_HierarchyArrangeDetails] PRIMARY KEY CLUSTERED 
(
	[numHierarchyChartAcntId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
