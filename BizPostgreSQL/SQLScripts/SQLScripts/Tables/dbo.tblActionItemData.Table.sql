/****** Object:  Table [dbo].[tblActionItemData]    Script Date: 07/26/2008 17:30:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblActionItemData](
	[RowID] [int] IDENTITY(1,1) NOT NULL,
	[TemplateName] [nvarchar](100) NULL,
	[DueDays] [int] NULL,
	[Priority] [int] NULL,
	[Type] [nvarchar](25) NULL,
	[Activity] [int] NULL,
	[Comments] [nvarchar](2000) NULL,
	[bitSendEmailTemplate] [bit] NULL CONSTRAINT [DF_tblActionItemData_bitSendEmailTemplate]  DEFAULT (0),
	[numEmailTemplate] [numeric](18, 0) NULL,
	[tintHours] [tinyint] NULL,
	[bitAlert] [bit] NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[dtCreatedBy] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[dtModifiedBy] [datetime] NULL
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0-before,1-after due date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tblActionItemData', @level2type=N'COLUMN',@level2name=N'bitSendEmailTemplate'
GO
