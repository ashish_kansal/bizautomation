/****** Object:  Table [dbo].[Remarket]    Script Date: 07/26/2008 17:29:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Remarket](
	[CLASSNUM] [varchar](100) NULL,
	[STARTDATE] [datetime] NULL,
	[ENDDATE] [datetime] NULL,
	[CLASSAT] [varchar](30) NULL,
	[DESCR] [text] NULL,
	[COURSECODE] [varchar](20) NULL,
	[COURSETITLE] [varchar](240) NULL,
	[PRICE] [money] NULL,
	[FACNAME] [varchar](120) NULL,
	[CITY] [varchar](84) NULL,
	[STATE] [varchar](10) NULL,
	[ADDRESS1] [varchar](max) NULL,
	[ADDRESS2] [varchar](max) NULL,
	[ABSTRACT] [text] NULL,
	[PREREQUISITS] [text] NULL,
	[LENGTH] [varchar](50) NULL,
	[DELIVERY] [varchar](50) NULL,
	[OBJECTIVE] [text] NULL,
	[TOPIC] [text] NULL,
	[AUDIENCE_TEXT] [text] NULL,
	[STUMAX] [varchar](20) NULL,
	[TOTSTBY] [varchar](20) NULL,
	[TOTCONF] [varchar](20) NULL,
	[REMARKS] [text] NULL,
	[CHAPTER] [varchar](384) NULL,
	[SUBCHAPTER] [varchar](384) NULL,
	[NEWEXTPRICEDATE] [varchar](50) NULL,
	[NEWEXTPRICE] [varchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
