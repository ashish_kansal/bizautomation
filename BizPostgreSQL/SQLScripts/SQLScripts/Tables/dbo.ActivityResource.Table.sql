/****** Object:  Table [dbo].[ActivityResource]    Script Date: 07/26/2008 17:20:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ActivityResource](
	[ActivityID] [int] NOT NULL CONSTRAINT [DF__ActivityResource__ActivityID]  DEFAULT ((0)),
	[ResourceID] [int] NOT NULL CONSTRAINT [DF__ActivityResource__ResourceID]  DEFAULT ((0)),
 CONSTRAINT [ActivityResource_PK] PRIMARY KEY NONCLUSTERED 
(
	[ActivityID] ASC,
	[ResourceID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ActivityResource]  WITH NOCHECK ADD  CONSTRAINT [ActivityResource_FK_Activity_ID] FOREIGN KEY([ActivityID])
REFERENCES [dbo].[Activity] ([ActivityID])
GO
ALTER TABLE [dbo].[ActivityResource] NOCHECK CONSTRAINT [ActivityResource_FK_Activity_ID]
GO
ALTER TABLE [dbo].[ActivityResource]  WITH NOCHECK ADD  CONSTRAINT [ActivityResource_FK_Resource_ID] FOREIGN KEY([ResourceID])
REFERENCES [dbo].[Resource] ([ResourceID])
GO
ALTER TABLE [dbo].[ActivityResource] NOCHECK CONSTRAINT [ActivityResource_FK_Resource_ID]
GO
