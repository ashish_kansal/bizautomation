/****** Object:  Table [dbo].[AuthenticationGroupMaster]    Script Date: 07/26/2008 17:20:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuthenticationGroupMaster](
	[numGroupID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[vcGroupName] [nvarchar](50) NOT NULL,
	[numDomainID] [numeric](18, 0) NULL CONSTRAINT [DF_AuthenticationGroupMaster_numDomainID]  DEFAULT (1),
	[tintGroupType] [tinyint] NOT NULL,
	[bitConsFlag] [bit] NULL CONSTRAINT [DF_AuthenticationGroupMaster_bitConsFlag]  DEFAULT (0),
 CONSTRAINT [PK_AuthenticationGroupMaster] PRIMARY KEY CLUSTERED 
(
	[numGroupID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
