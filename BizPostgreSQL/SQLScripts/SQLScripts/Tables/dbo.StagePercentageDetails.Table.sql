/****** Object:  Table [dbo].[StagePercentageDetails]    Script Date: 07/26/2008 17:29:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StagePercentageDetails](
	[numStageDetailsId] [numeric](10, 0) IDENTITY(1,1) NOT NULL,
	[numStagePercentageId] [numeric](10, 0) NOT NULL,
	[numStageNumber] [numeric](10, 0) NOT NULL,
	[vcStageDetail] [varchar](1000) NOT NULL,
	[numDomainId] [numeric](18, 0) NOT NULL,
	[numCreatedBy] [numeric](18, 0) NOT NULL,
	[bintCreatedDate] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[bintModifiedDate] [datetime] NULL,
	[slp_id] [numeric](18, 0) NOT NULL,
	[numAssignTo] [numeric](18, 0) NOT NULL CONSTRAINT [DF_StagePercentageDetails_numAssignTo]  DEFAULT ((0)),
	[vcStagePercentageDtl] [varchar](500) NULL,
	[numTemplateId] [numeric](18, 0) NOT NULL CONSTRAINT [DF_StagePercentageDetails_numTemplateId]  DEFAULT ((0)),
	[tintPercentage] [tinyint] NULL,
	[numEvent] [tinyint] NULL,
	[numReminder] [numeric](18, 0) NULL,
	[numET] [numeric](18, 0) NULL,
	[numActivity] [numeric](18, 0) NULL,
	[numStartDate] [tinyint] NULL,
	[numStartTime] [tinyint] NULL,
	[numStartTimePeriod] [tinyint] NULL,
	[numEndTime] [tinyint] NULL,
	[numEndTimePeriod] [tinyint] NULL,
	[txtCom] [varchar](500) NULL,
	[numChgStatus] [numeric](18, 0) NULL,
	[bitChgStatus] [bit] NULL,
	[bitClose] [bit] NULL,
	[numType] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
