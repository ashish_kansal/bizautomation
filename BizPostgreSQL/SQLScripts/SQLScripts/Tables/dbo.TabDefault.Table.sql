/****** Object:  Table [dbo].[TabDefault]    Script Date: 07/26/2008 17:30:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TabDefault](
	[numTabId] [numeric](18, 0) NOT NULL,
	[numTabName] [varchar](50) NULL,
	[tintTabType] [tinyint] NULL,
	[numDomainId] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
