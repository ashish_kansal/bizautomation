/****** Object:  Table [dbo].[RemarketDTL]    Script Date: 07/26/2008 17:29:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RemarketDTL](
	[numClassID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[vcClassNum] [varchar](100) NULL,
	[numItemID] [numeric](18, 0) NULL,
	[numWareHouseItemID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_RemarketDTL] PRIMARY KEY CLUSTERED 
(
	[numClassID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
