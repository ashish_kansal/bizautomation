/****** Object:  Table [dbo].[CFW_Fld_Values_Serialized_Items]    Script Date: 07/26/2008 17:21:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CFW_Fld_Values_Serialized_Items](
	[Fld_ID] [numeric](18, 0) NOT NULL,
	[Fld_Value] [varchar](1000) NOT NULL,
	[RecId] [numeric](18, 0) NOT NULL,
	[bitSerialized] [bit] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
