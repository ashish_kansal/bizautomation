/****** Object:  Table [dbo].[CashCreditCardDetails]    Script Date: 07/26/2008 17:21:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CashCreditCardDetails](
	[numCashCreditId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numRecurringId] [numeric](18, 0) NULL,
	[numAcntTypeId] [numeric](18, 0) NULL,
	[bitMoneyOut] [bit] NULL,
	[numPurchaseId] [numeric](18, 0) NULL,
	[vcReference] [varchar](50) NULL,
	[datEntry_Date] [smalldatetime] NULL,
	[monAmount] [money] NULL,
	[vcMemo] [varchar](50) NULL,
	[bitChargeBack] [bit] NULL,
	[numDomainId] [numeric](18, 0) NULL,
	[dtLastRecurringDate] [datetime] NULL,
	[numNoTransactions] [numeric](18, 0) NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[dtCreatedDate] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[dtModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_CashCreditCardDetails] PRIMARY KEY CLUSTERED 
(
	[numCashCreditId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[CashCreditCardDetails]  WITH CHECK ADD  CONSTRAINT [FK_CashCreditCardDetails_RecurringTemplate] FOREIGN KEY([numRecurringId])
REFERENCES [dbo].[RecurringTemplate] ([numRecurringId])
GO
ALTER TABLE [dbo].[CashCreditCardDetails] CHECK CONSTRAINT [FK_CashCreditCardDetails_RecurringTemplate]
GO
