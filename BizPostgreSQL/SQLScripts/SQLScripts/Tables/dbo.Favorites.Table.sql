/****** Object:  Table [dbo].[Favorites]    Script Date: 07/26/2008 17:25:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Favorites](
	[numFavoriteID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numUserCntID] [numeric](18, 0) NOT NULL,
	[numContactID] [numeric](18, 0) NOT NULL,
	[cType] [char](1) NULL,
 CONSTRAINT [PK_Favorites] PRIMARY KEY CLUSTERED 
(
	[numFavoriteID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
