/****** Object:  Table [dbo].[CaseContacts]    Script Date: 07/26/2008 17:21:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CaseContacts](
	[numCaseContID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numCaseID] [numeric](18, 0) NULL,
	[numContactID] [numeric](18, 0) NULL,
	[numRole] [numeric](18, 0) NULL,
	[bitPartner] [bit] NULL,
 CONSTRAINT [PK_CaseContacts] PRIMARY KEY CLUSTERED 
(
	[numCaseContID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CaseContacts]  WITH CHECK ADD  CONSTRAINT [FK_CaseContacts_AdditionalContactsInformation] FOREIGN KEY([numContactID])
REFERENCES [dbo].[AdditionalContactsInformation] ([numContactId])
GO
ALTER TABLE [dbo].[CaseContacts] CHECK CONSTRAINT [FK_CaseContacts_AdditionalContactsInformation]
GO
ALTER TABLE [dbo].[CaseContacts]  WITH CHECK ADD  CONSTRAINT [FK_CaseContacts_Cases] FOREIGN KEY([numCaseID])
REFERENCES [dbo].[Cases] ([numCaseId])
GO
ALTER TABLE [dbo].[CaseContacts] CHECK CONSTRAINT [FK_CaseContacts_Cases]
GO
