create table CompanyAssetSerial
(numAssetItemDTLID numeric(18,0) primary key identity,
numAssetItemID numeric(18,0),
vcSerialNo varchar(100),
numBarCodeId numeric(18,0),
vcModelId varchar(100),
dtPurchase datetime,
dtWarrante datetime,
vcLocation varchar(250))