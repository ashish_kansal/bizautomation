/****** Object:  Table [dbo].[DynamicFormMaster]    Script Date: 07/26/2008 17:24:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DynamicFormMaster](
	[numFormId] [numeric](10, 0) NOT NULL,
	[vcFormName] [nvarchar](75) NOT NULL,
	[cCustomFieldsAssociated] [char](1) NOT NULL,
	[cAOIAssociated] [char](1) NOT NULL,
	[bitDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_DynamicFormMaster] PRIMARY KEY CLUSTERED 
(
	[numFormId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
