/****** Object:  Table [dbo].[MyReportList]    Script Date: 07/26/2008 17:26:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MyReportList](
	[numMyRPTId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numRPTId] [numeric](18, 0) NULL,
	[numUserCntID] [numeric](18, 0) NULL,
	[tintRptSequence] [tinyint] NULL,
	[tintType] [tinyint] NOT NULL CONSTRAINT [DF_MyReportList_tintReportType]  DEFAULT ((0)),
 CONSTRAINT [PK_MyReportList] PRIMARY KEY CLUSTERED 
(
	[numMyRPTId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
