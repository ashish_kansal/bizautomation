/****** Object:  Table [dbo].[MarketBudgetMaster]    Script Date: 07/26/2008 17:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MarketBudgetMaster](
	[numMarketBudgetId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[intFiscalYear] [int] NULL,
	[bitMarketCampaign] [bit] NULL,
	[numDomainId] [numeric](18, 0) NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[dtCreationDate] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[dtModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_MarketBudgetMaster] PRIMARY KEY CLUSTERED 
(
	[numMarketBudgetId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
