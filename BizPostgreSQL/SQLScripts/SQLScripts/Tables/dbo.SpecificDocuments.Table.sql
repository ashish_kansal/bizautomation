/****** Object:  Table [dbo].[SpecificDocuments]    Script Date: 07/26/2008 17:29:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SpecificDocuments](
	[numSpecificDocID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[VcFileName] [varchar](200) NOT NULL,
	[vcDocName] [varchar](50) NOT NULL,
	[numDocCategory] [numeric](18, 0) NULL,
	[cUrlType] [char](1) NULL,
	[vcFileType] [varchar](10) NULL,
	[numBusClass] [numeric](18, 0) NULL,
	[vcDocDesc] [varchar](1000) NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[vcDocumentSection] [varchar](10) NULL,
	[numRecID] [numeric](18, 0) NOT NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[bintCreatedDate] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[bintModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_SpecificDocuments] PRIMARY KEY CLUSTERED 
(
	[numSpecificDocID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
