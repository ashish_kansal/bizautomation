/****** Object:  Table [dbo].[OpportunitySubStageDetails]    Script Date: 07/26/2008 17:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OpportunitySubStageDetails](
	[numSubStageElmtID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numOppId] [numeric](18, 0) NOT NULL,
	[numStageDetailsId] [numeric](18, 0) NOT NULL,
	[numSubStageID] [numeric](18, 0) NOT NULL,
	[numProcessListId] [numeric](18, 0) NULL,
	[vcSubStageDetail] [varchar](1000) NULL,
	[vcComments] [varchar](250) NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[bintCreatedDate] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[bintModifiedDate] [datetime] NULL,
	[bitStageCompleted] [bit] NULL CONSTRAINT [DF__Opportuni__bitSt__511AFFBC]  DEFAULT ((0)),
 CONSTRAINT [PK_OpportunitySubStageDetails] PRIMARY KEY CLUSTERED 
(
	[numSubStageElmtID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[OpportunitySubStageDetails]  WITH NOCHECK ADD  CONSTRAINT [FK_OpportunitySubStageDetails_OpportunityMaster] FOREIGN KEY([numOppId])
REFERENCES [dbo].[OpportunityMaster] ([numOppId])
GO
ALTER TABLE [dbo].[OpportunitySubStageDetails] CHECK CONSTRAINT [FK_OpportunitySubStageDetails_OpportunityMaster]
GO
