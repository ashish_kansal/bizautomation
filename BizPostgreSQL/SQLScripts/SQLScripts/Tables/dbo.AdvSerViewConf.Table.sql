/****** Object:  Table [dbo].[AdvSerViewConf]    Script Date: 07/26/2008 17:20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AdvSerViewConf](
	[numAdvSerViewID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numFormFieldID] [numeric](18, 0) NOT NULL,
	[tintViewID] [tinyint] NULL,
	[numGroupID] [numeric](18, 0) NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[tintOrder] [numeric](18, 0) NULL,
	[numFormId] [numeric](18, 0) NOT NULL CONSTRAINT [DF_AdvSerViewConf_numFormId]  DEFAULT ((1)),
 CONSTRAINT [PK_AdvSerViewConf] PRIMARY KEY CLUSTERED 
(
	[numAdvSerViewID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
