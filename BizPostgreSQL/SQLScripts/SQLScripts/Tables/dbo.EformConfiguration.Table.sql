/****** Object:  Table [dbo].[EformConfiguration]    Script Date: 07/26/2008 17:24:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EformConfiguration](
	[numEformFldID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numSpecDocID] [numeric](18, 0) NULL,
	[vcColumnName] [varchar](100) NULL,
	[vcEFormFld] [varchar](100) NULL,
	[VcFldType] [varchar](1) NULL,
 CONSTRAINT [PK_EformConfiguration] PRIMARY KEY CLUSTERED 
(
	[numEformFldID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
