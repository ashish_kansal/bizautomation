USE [Production.2014]
GO

/****** Object:  Table [dbo].[tblStageGradeDetails]    Script Date: 12/11/2019 8:57:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblStageGradeDetails](
	[numStageDetailsGradeId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numStageDetailsId] [numeric](18, 0) NOT NULL,
	[numAssigneId] [numeric](18, 0) NOT NULL,
	[vcGradeId] [varchar](100) NULL,
	[numDomainId] [numeric](18, 0) NOT NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[bintCreatedDate] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[bintModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tblStageGradeDetails] PRIMARY KEY CLUSTERED 
(
	[numStageDetailsGradeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


