/****** Object:  Table [dbo].[WareHouseItmsDTL]    Script Date: 07/26/2008 17:31:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WareHouseItmsDTL](
	[numWareHouseItmsDTLID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numWareHouseItemID] [numeric](18, 0) NULL,
	[vcSerialNo] [varchar](100) NULL,
	[tintStatus] [tinyint] NULL,
	[vcComments] [varchar](1000) NULL,
 CONSTRAINT [PK_WareHouseItmsDTL] PRIMARY KEY CLUSTERED 
(
	[numWareHouseItmsDTLID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[WareHouseItmsDTL]  WITH CHECK ADD  CONSTRAINT [FK_WareHouseItmsDTL_WareHouseItems] FOREIGN KEY([numWareHouseItemID])
REFERENCES [dbo].[WareHouseItems] ([numWareHouseItemID])
GO
ALTER TABLE [dbo].[WareHouseItmsDTL] CHECK CONSTRAINT [FK_WareHouseItmsDTL_WareHouseItems]
GO
