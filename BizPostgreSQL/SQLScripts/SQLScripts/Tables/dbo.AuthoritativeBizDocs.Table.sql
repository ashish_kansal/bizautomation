/****** Object:  Table [dbo].[AuthoritativeBizDocs]    Script Date: 07/26/2008 17:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuthoritativeBizDocs](
	[numAuthoritativePurchase] [numeric](18, 0) NULL,
	[numAuthoritativeSales] [numeric](18, 0) NULL,
	[numAuthoritativeeCommerce] [numeric](18, 0) NULL,
	[numDomainId] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
