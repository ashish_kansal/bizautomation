/****** Object:  Table [dbo].[FixedAssetDetails]    Script Date: 07/26/2008 17:25:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FixedAssetDetails](
	[numFixedAssetId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numCompanyId] [numeric](18, 0) NULL,
	[numAssetClass] [numeric](18, 0) NULL,
	[vcAssetName] [varchar](50) NULL,
	[vcAssetDescription] [varchar](100) NULL,
	[ntxtLocation] [ntext] NULL,
	[vcSerialNo] [varchar](50) NULL,
	[dtDatePurchased] [datetime] NULL,
	[monCost] [money] NULL,
	[decDep_Year_Per] [decimal](18, 0) NULL,
	[numVendor] [numeric](18, 0) NULL,
	[monFiscalDepOrAppAmt] [money] NULL,
	[monCurrentValue] [money] NULL,
	[numDomainId] [numeric](18, 0) NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[dtCreatedOn] [datetime] NULL,
 CONSTRAINT [PK_FixedAssetDetails] PRIMARY KEY CLUSTERED 
(
	[numFixedAssetId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
