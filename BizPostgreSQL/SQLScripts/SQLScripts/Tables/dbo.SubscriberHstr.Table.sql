/****** Object:  Table [dbo].[SubscriberHstr]    Script Date: 07/26/2008 17:30:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SubscriberHstr](
	[numSubscriberDTLID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numSubscriberID] [numeric](18, 0) NOT NULL,
	[intNoofSubscribers] [int] NOT NULL,
	[dtSubStartDate] [datetime] NULL,
	[dtSubRenewDate] [datetime] NULL,
	[bitTrial] [bit] NOT NULL,
	[numAdminContactID] [numeric](18, 0) NULL,
	[bitStatus] [bit] NOT NULL,
	[dtSuspendedDate] [datetime] NULL,
	[vcSuspendedReason] [varchar](1000) NULL,
	[numTargetDomainID] [numeric](18, 0) NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[numCreatedBy] [numeric](18, 0) NOT NULL,
	[dtCreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_SubscriberHstr] PRIMARY KEY CLUSTERED 
(
	[numSubscriberDTLID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[SubscriberHstr]  WITH CHECK ADD  CONSTRAINT [FK_SubscriberHstr_Subscribers] FOREIGN KEY([numSubscriberID])
REFERENCES [dbo].[Subscribers] ([numSubscriberID])
GO
ALTER TABLE [dbo].[SubscriberHstr] CHECK CONSTRAINT [FK_SubscriberHstr_Subscribers]
GO
