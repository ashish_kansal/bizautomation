CREATE TABLE [dbo].[PurchaseIncentives](
	[numPurchaseIncentiveId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDivisionID] [numeric](18, 0) NULL,
	[decBuyingQty] [decimal](18, 2) NULL,
	[intType] [int] NULL,
	[vcIncentives] [varchar](500) NULL,
	[numDomainId] [numeric](18, 0) NULL,
 CONSTRAINT [PK_PurchaseIncentives] PRIMARY KEY CLUSTERED 
(
	[numPurchaseIncentiveId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


