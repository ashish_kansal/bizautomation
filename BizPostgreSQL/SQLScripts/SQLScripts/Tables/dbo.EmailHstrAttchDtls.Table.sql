/****** Object:  Table [dbo].[EmailHstrAttchDtls]    Script Date: 07/26/2008 17:24:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmailHstrAttchDtls](
	[numEmailHstrAttID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numEmailHstrID] [numeric](18, 0) NULL,
	[vcFileName] [varchar](500) NULL,
	[vcAttachmentItemId] [varchar](1000) NULL,
	[vcAttachmentType] [varchar](500) NULL,
	[vcLocation] [varchar](500) NULL,
 CONSTRAINT [PK_EmailHstrAttchDtls] PRIMARY KEY CLUSTERED 
(
	[numEmailHstrAttID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
