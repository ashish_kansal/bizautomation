/****** Object:  Table [dbo].[UserTerritory]    Script Date: 07/26/2008 17:31:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserTerritory](
	[numUserCntID] [numeric](18, 0) NOT NULL,
	[numTerritoryID] [numeric](18, 0) NOT NULL,
	[numDomainID] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserTerritory]  WITH CHECK ADD  CONSTRAINT [FK_UserTerritory_AdditionalContactsInformation] FOREIGN KEY([numUserCntID])
REFERENCES [dbo].[AdditionalContactsInformation] ([numContactId])
GO
ALTER TABLE [dbo].[UserTerritory] CHECK CONSTRAINT [FK_UserTerritory_AdditionalContactsInformation]
GO
