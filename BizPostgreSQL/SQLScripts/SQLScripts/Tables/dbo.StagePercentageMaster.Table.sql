/****** Object:  Table [dbo].[StagePercentageMaster]    Script Date: 07/26/2008 17:30:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StagePercentageMaster](
	[numStagePercentageId] [numeric](10, 0) NOT NULL,
	[numStagePercentage] [numeric](10, 0) NOT NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[dtCreatedDate] [datetime] NULL,
 CONSTRAINT [PK_StagePercentageMaster] PRIMARY KEY CLUSTERED 
(
	[numStagePercentageId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
