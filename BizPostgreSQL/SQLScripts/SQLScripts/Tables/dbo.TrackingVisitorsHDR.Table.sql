/****** Object:  Table [dbo].[TrackingVisitorsHDR]    Script Date: 07/26/2008 17:31:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TrackingVisitorsHDR](
	[numTrackingID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[vcUserHostAddress] [varchar](100) NULL,
	[vcUserDomain] [varchar](100) NULL,
	[vcUserAgent] [varchar](1000) NULL,
	[vcBrowser] [varchar](50) NULL,
	[vcCrawler] [varchar](50) NULL,
	[vcURL] [varchar](500) NULL,
	[vcReferrer] [varchar](500) NULL,
	[vcSearchTerm] [varchar](100) NULL,
	[numVisits] [numeric](18, 0) NULL,
	[vcOrginalURL] [varchar](500) NULL,
	[vcOrginalRef] [varchar](500) NULL,
	[numVistedPages] [numeric](18, 0) NULL,
	[vcTotalTime] [varchar](100) NULL,
	[dtCreated] [datetime] NULL,
	[vcCountry] [varchar](50) NULL,
	[numReferrerID] [numeric](18, 0) NULL,
	[numDivisionID] [numeric](18, 0) NULL,
	[numDomainID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_TrackingVisitorsHDR] PRIMARY KEY CLUSTERED 
(
	[numTrackingID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
