/****** Object:  Table [dbo].[ExchangeUserDetails]    Script Date: 07/26/2008 17:24:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ExchangeUserDetails](
	[numUserID] [numeric](18, 0) NULL,
	[bitExchangeIntegration] [bit] NULL,
	[bitAccessExchange] [bit] NOT NULL,
	[vcExchPassword] [varchar](100) NULL,
	[vcExchPath] [varchar](200) NULL,
	[vcExchDomain] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
