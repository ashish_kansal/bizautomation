/****** Object:  Table [dbo].[UserAccessedDTL]    Script Date: 07/26/2008 17:31:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserAccessedDTL](
	[numAppAccessID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numSubscriberID] [numeric](18, 0) NULL,
	[numDivisionID] [numeric](18, 0) NOT NULL,
	[numContactID] [numeric](18, 0) NOT NULL,
	[dtLoggedInTime] [datetime] NULL,
	[dtLoggedOutTime] [datetime] NULL,
	[bitPortal] [bit] NOT NULL CONSTRAINT [DF_UserAccessedDTL_bitPortal]  DEFAULT ((0)),
	[numDomainID] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_UserAccessedDTL] PRIMARY KEY CLUSTERED 
(
	[numAppAccessID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
