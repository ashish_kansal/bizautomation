/****** Object:  Table [dbo].[News]    Script Date: 07/26/2008 17:26:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[News](
	[numNewsID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[vcNewsHeading] [varchar](1000) NOT NULL,
	[vcDate] [varchar](50) NULL,
	[vcDesc] [text] NULL,
	[vcImagePath] [varchar](500) NULL,
	[vcURL] [varchar](500) NULL,
	[vcCreatedDate] [datetime] NULL,
	[vcModifieddate] [datetime] NULL,
	[bitActive] [bit] NULL,
	[tintType] [tinyint] NULL,
 CONSTRAINT [PK_News] PRIMARY KEY CLUSTERED 
(
	[numNewsID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
