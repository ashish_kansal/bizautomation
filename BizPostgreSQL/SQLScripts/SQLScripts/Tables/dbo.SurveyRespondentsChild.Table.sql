/****** Object:  Table [dbo].[SurveyRespondentsChild]    Script Date: 07/26/2008 17:30:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SurveyRespondentsChild](
	[numRespondantID] [numeric](18, 0) NOT NULL,
	[numSurId] [numeric](18, 0) NOT NULL,
	[vcDbColumnName] [nvarchar](50) NOT NULL,
	[vcFormFieldName] [nvarchar](50) NOT NULL,
	[vcDbColumnValue] [nvarchar](50) NULL,
	[vcDbColumnValueText] [nvarchar](50) NULL,
	[intRowNum] [numeric](18, 0) NOT NULL,
	[intColumnNum] [numeric](18, 0) NOT NULL,
	[vcAssociatedControlType] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_SurveyRespondentsChild] PRIMARY KEY CLUSTERED 
(
	[numRespondantID] ASC,
	[numSurId] ASC,
	[vcDbColumnName] ASC,
	[intRowNum] ASC,
	[intColumnNum] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SurveyRespondentsChild]  WITH CHECK ADD  CONSTRAINT [FK_SurveyRespondentsChild_SurveyRespondentsMaster] FOREIGN KEY([numRespondantID])
REFERENCES [dbo].[SurveyRespondentsMaster] ([numRespondantID])
GO
ALTER TABLE [dbo].[SurveyRespondentsChild] CHECK CONSTRAINT [FK_SurveyRespondentsChild_SurveyRespondentsMaster]
GO
