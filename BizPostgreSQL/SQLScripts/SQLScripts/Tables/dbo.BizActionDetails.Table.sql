
GO
/****** Object:  Table [dbo].[BizActionDetails]    Script Date: 02/15/2010 23:53:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BizActionDetails](
	[numBizActionDtlId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numBizActionId] [numeric](18, 0) NULL,
	[numOppBizDocsId] [numeric](18, 0) NULL,
	[btStatus] [numeric](1, 0) NULL DEFAULT ((0)),
	[btDocType] [bit] NULL DEFAULT ((1)),
	[dtApprovedOn] [datetime] NULL
) ON [PRIMARY]
