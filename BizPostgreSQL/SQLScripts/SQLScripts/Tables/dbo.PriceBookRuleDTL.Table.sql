/****** Object:  Table [dbo].[PriceBookRuleDTL]    Script Date: 07/26/2008 17:28:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PriceBookRuleDTL](
	[numPriceBookRuleDTLID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numRuleID] [numeric](18, 0) NOT NULL,
	[numValue] [numeric](18, 0) NOT NULL,
	[numProfile] [numeric](18, 0) NULL,
	[tintType] [tinyint] NULL,
 CONSTRAINT [PK_PriceBookRuleDTL] PRIMARY KEY CLUSTERED 
(
	[numPriceBookRuleDTLID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1- item , 2- item group, 3 - apply to items within all item group, 4- org, 5-relationship, 6 rel-profile, 7- all organizations' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PriceBookRuleDTL', @level2type=N'COLUMN',@level2name=N'tintType'
GO
