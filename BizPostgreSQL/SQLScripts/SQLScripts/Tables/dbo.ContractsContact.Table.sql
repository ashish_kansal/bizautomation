/****** Object:  Table [dbo].[ContractsContact]    Script Date: 07/26/2008 17:22:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContractsContact](
	[numContractId] [numeric](18, 0) NOT NULL,
	[numContactId] [numeric](18, 0) NOT NULL,
	[numDomainId] [numeric](18, 0) NOT NULL
) ON [PRIMARY]
GO
