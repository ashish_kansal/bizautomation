/****** Object:  Table [dbo].[SurveyAnsMaster]    Script Date: 07/26/2008 17:30:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SurveyAnsMaster](
	[numAnsID] [numeric](18, 0) NOT NULL,
	[numQID] [numeric](18, 0) NOT NULL,
	[numSurID] [numeric](18, 0) NOT NULL,
	[vcAnsLabel] [nvarchar](100) NOT NULL,
	[boolSurveyRuleAttached] [bit] NOT NULL CONSTRAINT [DF_SurveyAnsMaster_boolSurveyRuleAttached]  DEFAULT (0),
 CONSTRAINT [PK_SurveyAnsMaster] PRIMARY KEY CLUSTERED 
(
	[numAnsID] ASC,
	[numQID] ASC,
	[numSurID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SurveyAnsMaster]  WITH CHECK ADD  CONSTRAINT [FK_SurveyAnsMaster_SurveyMaster] FOREIGN KEY([numSurID])
REFERENCES [dbo].[SurveyMaster] ([numSurID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SurveyAnsMaster] CHECK CONSTRAINT [FK_SurveyAnsMaster_SurveyMaster]
GO
