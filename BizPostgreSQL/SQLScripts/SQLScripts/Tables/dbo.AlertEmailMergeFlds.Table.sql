/****** Object:  Table [dbo].[AlertEmailMergeFlds]    Script Date: 07/26/2008 17:20:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AlertEmailMergeFlds](
	[numEmailAlertDTLID] [numeric](18, 0) NULL,
	[intMergeFldID] [int] NULL
) ON [PRIMARY]
GO
