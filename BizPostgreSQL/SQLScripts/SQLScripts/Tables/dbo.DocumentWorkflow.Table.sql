/****** Object:  Table [dbo].[DocumentWorkflow]    Script Date: 07/26/2008 17:23:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DocumentWorkflow](
	[numDocID] [numeric](18, 0) NULL,
	[numContactID] [numeric](18, 0) NULL,
	[tintApprove] [tinyint] NULL CONSTRAINT [DF_DocumentWorkflow_bitApprove]  DEFAULT (0),
	[dtApprovedOn] [datetime] NULL,
	[cDocType] [char](1) NULL,
	[vcComment] [varchar](1000) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
