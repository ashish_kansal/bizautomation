/****** Object:  Table [dbo].[ProjectsDependency]    Script Date: 07/26/2008 17:28:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProjectsDependency](
	[numProDepID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numProId] [numeric](18, 0) NOT NULL,
	[numProStageId] [numeric](18, 0) NOT NULL,
	[numProcessStageID] [numeric](18, 0) NOT NULL,
	[tintType] [tinyint] NULL,
	[vcType] [varchar](1) NOT NULL,
 CONSTRAINT [PK_ProjectDependency] PRIMARY KEY CLUSTERED 
(
	[numProDepID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ProjectsDependency]  WITH CHECK ADD  CONSTRAINT [FK_ProjectsDependency_ProjectsMaster] FOREIGN KEY([numProId])
REFERENCES [dbo].[ProjectsMaster] ([numProId])
GO
ALTER TABLE [dbo].[ProjectsDependency] CHECK CONSTRAINT [FK_ProjectsDependency_ProjectsMaster]
GO
