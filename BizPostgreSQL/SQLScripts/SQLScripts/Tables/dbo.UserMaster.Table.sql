/****** Object:  Table [dbo].[UserMaster]    Script Date: 07/26/2008 17:31:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserMaster](
	[numUserId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[vcUserName] [varchar](50) NOT NULL,
	[vcMailNickName] [varchar](50) NULL,
	[vcUserDesc] [varchar](250) NULL,
	[numGroupID] [numeric](18, 0) NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[bintCreatedDate] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[bintModifiedDate] [datetime] NULL,
	[bitActivateFlag] [bit] NULL CONSTRAINT [DF_UserMaster_bitActivateFlag]  DEFAULT ((0)),
	[numUserDetailId] [numeric](18, 0) NULL,
	[vcEmailID] [varchar](100) NULL,
	[vcPassword] [varchar](100) NULL,
	[txtSignature] [varchar](1000) NULL,
	[bitHourlyRate] [bit] NULL,
	[monHourlyRate] [money] NULL,
	[bitSalary] [bit] NULL,
	[numDailyHours] [float] NULL,
	[dtSalaryDateTime] [datetime] NULL,
	[bitOverTime] [bit] NULL,
	[bitOverTimeHrsDailyOrWeekly] [bit] NULL,
	[numLimDailHrs] [float] NULL,
	[monOverTimeRate] [money] NULL,
	[numLimWeeklyHrs] [float] NULL,
	[monOverTimeWeeklyRate] [money] NULL,
	[bitMainComm] [char](10) NULL,
	[fltMainCommPer] [float] NULL,
	[bitRoleComm] [bit] NULL,
	[SubscriptionId] [varchar](150) NULL,
	[bitPaidLeave] [bit] NULL,
	[fltPaidLeaveHrs] [float] NULL,
	[fltPaidLeaveEmpHrs] [float] NULL,
	[bitCommOwner] [bit] NULL,
	[bitCommAssignee] [bit] NULL,
	[fltCommOwner] [float] NULL,
	[fltCommAssignee] [float] NULL,
	[bitEmpNetAccount] [bit] NULL,
	[dtHired] [datetime] NULL,
	[dtReleased] [datetime] NULL,
	[vcEmployeeId] [varchar](150) NULL,
	[vcEmergencyContact] [varchar](250) NULL,
	[bitPayroll] [bit] NULL,
	[fltEmpRating] [float] NULL,
	[dtEmpRating] [datetime] NULL,
	[monNetPay] [money] NULL,
	[monAmtWithheld] [money] NULL,
	[bitSMTPAuth] [bit] NULL,
	[vcSmtpPassword] [varchar](100) NULL,
	[vcSMTPServer] [varchar](500) NULL,
	[numSMTPPort] [numeric](18, 0) NULL,
	[bitSMTPSSL] [bit] NULL,
	[bitSMTPServer] [bit] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[numUserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[UserMaster]  WITH NOCHECK ADD  CONSTRAINT [FK_User_Domain] FOREIGN KEY([numDomainID])
REFERENCES [dbo].[Domain] ([numDomainId])
GO
ALTER TABLE [dbo].[UserMaster] CHECK CONSTRAINT [FK_User_Domain]
GO
