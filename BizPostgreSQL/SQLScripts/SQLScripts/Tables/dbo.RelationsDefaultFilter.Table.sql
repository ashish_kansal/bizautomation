/****** Object:  Table [dbo].[RelationsDefaultFilter]    Script Date: 07/26/2008 17:29:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RelationsDefaultFilter](
	[numRelationId] [numeric](18, 0) NULL,
	[numDomainId] [numeric](18, 0) NULL,
	[numUserCntId] [numeric](18, 0) NULL,
	[numFilterId] [numeric](18, 0) NULL,
	[bitInitialPage] [bit] NULL CONSTRAINT [DF_RelationsDefaultFilter_bitInitialPage]  DEFAULT ((0)),
	[numformId] [numeric](18, 0) NULL,
	[tintGroupId] [tinyint] NULL CONSTRAINT [DF_RelationsDefaultFilter_GroupId]  DEFAULT ((0))
) ON [PRIMARY]
GO
