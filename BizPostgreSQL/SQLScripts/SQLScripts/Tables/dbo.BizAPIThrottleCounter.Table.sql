GO
/****** Object:  Table [dbo].[BizAPIThrottleCounter]    Script Date: 04/30/2014 10:31:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BizAPIThrottleCounter](
	[vcID] [varchar](100) NOT NULL,
	[vcBizAPIAccessKey] [varchar](50) NULL,
	[dtTimeSpan] [datetime] NULL,
	[numTotalRequest] [numeric](18, 0) NULL,
	[bintExpirationTime] [bigint] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING ON