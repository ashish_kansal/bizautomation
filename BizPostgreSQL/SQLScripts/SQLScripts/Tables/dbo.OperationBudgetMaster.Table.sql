/****** Object:  Table [dbo].[OperationBudgetMaster]    Script Date: 07/26/2008 17:26:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OperationBudgetMaster](
	[numBudgetId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[vcBudgetName] [varchar](50) NULL,
	[numDivisionId] [numeric](18, 0) NULL,
	[numDepartmentId] [numeric](18, 0) NULL,
	[numCostCenterId] [numeric](18, 0) NULL,
	[intFiscalYear] [int] NULL,
	[numDomainId] [numeric](18, 0) NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[dtCreationDate] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[dtModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_OperationBudgetMaster] PRIMARY KEY CLUSTERED 
(
	[numBudgetId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
