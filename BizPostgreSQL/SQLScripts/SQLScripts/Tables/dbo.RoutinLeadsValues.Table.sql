/****** Object:  Table [dbo].[RoutinLeadsValues]    Script Date: 07/26/2008 17:29:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RoutinLeadsValues](
	[numRoutID] [numeric](18, 0) NULL,
	[vcValue] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[RoutinLeadsValues]  WITH CHECK ADD  CONSTRAINT [FK_RoutinLeadsValues_RoutingLeads] FOREIGN KEY([numRoutID])
REFERENCES [dbo].[RoutingLeads] ([numRoutID])
GO
ALTER TABLE [dbo].[RoutinLeadsValues] CHECK CONSTRAINT [FK_RoutinLeadsValues_RoutingLeads]
GO
