/****** Object:  Table [dbo].[MarketBudgetDetails]    Script Date: 07/26/2008 17:26:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MarketBudgetDetails](
	[numMarketDetId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numMarketId] [numeric](18, 0) NULL,
	[numListItemID] [numeric](18, 0) NULL,
	[tintMonth] [tinyint] NULL,
	[intYear] [int] NULL,
	[monAmount] [money] NULL,
	[vcComments] [varchar](1000) NULL,
 CONSTRAINT [PK_MarketBudgetDetails] PRIMARY KEY CLUSTERED 
(
	[numMarketDetId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[MarketBudgetDetails]  WITH CHECK ADD  CONSTRAINT [FK_MarketBudgetDetails_MarketBudgetDetails] FOREIGN KEY([numMarketId])
REFERENCES [dbo].[MarketBudgetMaster] ([numMarketBudgetId])
GO
ALTER TABLE [dbo].[MarketBudgetDetails] CHECK CONSTRAINT [FK_MarketBudgetDetails_MarketBudgetDetails]
GO
