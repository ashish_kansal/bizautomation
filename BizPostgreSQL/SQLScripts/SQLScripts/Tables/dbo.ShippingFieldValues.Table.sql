/****** Object:  Table [dbo].[ShippingFieldValues]    Script Date: 07/26/2008 17:29:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ShippingFieldValues](
	[intShipFieldID] [int] NULL,
	[vcShipFieldValue] [varchar](200) NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[numListItemID] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
