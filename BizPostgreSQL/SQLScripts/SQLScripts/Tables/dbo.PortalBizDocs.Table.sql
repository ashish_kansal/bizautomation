/****** Object:  Table [dbo].[PortalBizDocs]    Script Date: 07/26/2008 17:28:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PortalBizDocs](
	[numPorBizBizDocID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numBizDocID] [numeric](18, 0) NOT NULL,
	[numDomainID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_PortalBizDocs] PRIMARY KEY CLUSTERED 
(
	[numPorBizBizDocID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
