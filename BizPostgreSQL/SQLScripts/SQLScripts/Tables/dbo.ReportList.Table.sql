/****** Object:  Table [dbo].[ReportList]    Script Date: 07/26/2008 17:29:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportList](
	[RptId] [numeric](18, 0) NOT NULL,
	[NumId] [numeric](18, 0) NOT NULL,
	[RptHeading] [nvarchar](50) NULL,
	[RptDesc] [nvarchar](255) NULL,
	[rptGroup] [numeric](18, 0) NULL,
	[rptSequence] [numeric](18, 0) NULL,
 CONSTRAINT [PK_rptList] PRIMARY KEY CLUSTERED 
(
	[RptId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
