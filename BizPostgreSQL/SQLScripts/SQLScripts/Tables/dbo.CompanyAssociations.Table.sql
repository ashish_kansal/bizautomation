/****** Object:  Table [dbo].[CompanyAssociations]    Script Date: 07/26/2008 17:22:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CompanyAssociations](
	[numAssociationID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numAssociateFromDivisionID] [numeric](18, 0) NOT NULL,
	[numCompanyID] [numeric](18, 0) NOT NULL,
	[numDivisionID] [numeric](18, 0) NOT NULL,
	[numReferralType] [numeric](18, 0) NOT NULL,
	[bitDeleted] [bit] NOT NULL CONSTRAINT [DF_CompanyAssociations_bitDeleted]  DEFAULT (0),
	[numCreateBy] [numeric](18, 0) NOT NULL,
	[numCreatedOn] [datetime] NOT NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[numModifiedOn] [datetime] NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[bitParentOrg] [bit] NULL CONSTRAINT [DF_CompanyAssociations_bitParentOrg]  DEFAULT (0),
	[bitChildOrg] [bit] NULL CONSTRAINT [DF_CompanyAssociations_bitParentOrg1]  DEFAULT (0),
	[bitShareportal] [bit] NULL CONSTRAINT [DF_CompanyAssociations_bitShareportal]  DEFAULT (0),
 CONSTRAINT [PK_CompanyAssociations] PRIMARY KEY CLUSTERED 
(
	[numAssociationID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CompanyAssociations]  WITH NOCHECK ADD  CONSTRAINT [FK_CompanyAssociations_Domain] FOREIGN KEY([numDomainID])
REFERENCES [dbo].[Domain] ([numDomainId])
GO
ALTER TABLE [dbo].[CompanyAssociations] CHECK CONSTRAINT [FK_CompanyAssociations_Domain]
GO
