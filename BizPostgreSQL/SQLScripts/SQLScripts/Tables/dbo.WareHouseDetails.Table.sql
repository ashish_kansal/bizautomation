/****** Object:  Table [dbo].[WareHouseDetails]    Script Date: 07/26/2008 17:31:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WareHouseDetails](
	[numWareHouseDetailID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numCountryID] [numeric](18, 0) NULL,
	[numStateID] [numeric](18, 0) NULL,
	[numWareHouse] [numeric](18, 0) NOT NULL,
	[numDomainId] [numeric](18, 0) NULL,
	[numFromPinCode] [int] NULL,
	[numToPinCode] [int] NULL
) ON [PRIMARY]
GO
