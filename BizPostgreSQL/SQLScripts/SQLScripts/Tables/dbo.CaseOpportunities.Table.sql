/****** Object:  Table [dbo].[CaseOpportunities]    Script Date: 07/26/2008 17:21:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CaseOpportunities](
	[numCaseId] [numeric](18, 0) NULL,
	[numOppId] [numeric](18, 0) NULL,
	[numDomainId] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
