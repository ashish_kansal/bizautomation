/****** Object:  Table [dbo].[OppWarehouseSerializedItem]    Script Date: 07/26/2008 17:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OppWarehouseSerializedItem](
	[numWarehouseItmsDTLID] [numeric](18, 0) NULL,
	[numOppID] [numeric](18, 0) NULL,
	[numOppItemID] [numeric](18, 0) NULL,
	[numWarehouseItmsID] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OppWarehouseSerializedItem]  WITH CHECK ADD  CONSTRAINT [FK_OppWarehouseSerializedItem_OpportunityMaster] FOREIGN KEY([numOppID])
REFERENCES [dbo].[OpportunityMaster] ([numOppId])
GO
ALTER TABLE [dbo].[OppWarehouseSerializedItem] CHECK CONSTRAINT [FK_OppWarehouseSerializedItem_OpportunityMaster]
GO
