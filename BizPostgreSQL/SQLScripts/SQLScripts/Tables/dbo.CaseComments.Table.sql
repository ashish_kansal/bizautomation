/****** Object:  Table [dbo].[CaseComments]    Script Date: 07/26/2008 17:21:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CaseComments](
	[numCommentID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numCaseID] [numeric](18, 0) NULL,
	[vcHeading] [varchar](1000) NULL,
	[txtComments] [text] NULL,
	[numCreatedby] [numeric](18, 0) NULL,
	[bintCreatedDate] [datetime] NULL,
 CONSTRAINT [PK_CaseComments] PRIMARY KEY CLUSTERED 
(
	[numCommentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
