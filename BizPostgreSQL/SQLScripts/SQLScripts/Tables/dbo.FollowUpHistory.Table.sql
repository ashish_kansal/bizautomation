/****** Object:  Table [dbo].[FollowUpHistory]    Script Date: 07/26/2008 17:25:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FollowUpHistory](
	[numFollowUpStatusID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numFollowUpstatus] [numeric](18, 0) NULL,
	[numDivisionID] [numeric](18, 0) NULL,
	[bintAddedDate] [datetime] NULL,
	[numDomainID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_FollowUpHistory] PRIMARY KEY CLUSTERED 
(
	[numFollowUpStatusID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
