/****** Object:  Table [dbo].[OpportunityItemLinking]    Script Date: 07/26/2008 17:27:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OpportunityItemLinking](
	[numOppOldItemID] [numeric](18, 0) NULL,
	[numOppNewItemID] [numeric](18, 0) NULL,
	[numNewOppID] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OpportunityItemLinking]  WITH CHECK ADD  CONSTRAINT [FK_OpportunityItemLinking_OpportunityMaster] FOREIGN KEY([numNewOppID])
REFERENCES [dbo].[OpportunityMaster] ([numOppId])
GO
ALTER TABLE [dbo].[OpportunityItemLinking] CHECK CONSTRAINT [FK_OpportunityItemLinking_OpportunityMaster]
GO
