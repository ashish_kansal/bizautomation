/****** Object:  Table [dbo].[DashboardAllowedReports]    Script Date: 07/26/2008 17:23:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DashboardAllowedReports](
	[numReportId] [numeric](18, 0) NOT NULL,
	[numGrpId] [numeric](18, 0) NOT NULL
) ON [PRIMARY]
GO
