/****** Object:  Table [dbo].[Resource]    Script Date: 07/26/2008 17:29:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Resource](
	[ResourceID] [int] IDENTITY(1,1) NOT NULL,
	[ResourceName] [nvarchar](64) NOT NULL,
	[ResourceDescription] [ntext] NULL,
	[EmailAddress] [nvarchar](64) NULL,
	[_ts] [timestamp] NULL,
	[numUserCntId] [numeric](18, 0) NULL,
	[numDomainId] [numeric](18, 0) NULL,
 CONSTRAINT [Resource_PK] PRIMARY KEY NONCLUSTERED 
(
	[ResourceID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
