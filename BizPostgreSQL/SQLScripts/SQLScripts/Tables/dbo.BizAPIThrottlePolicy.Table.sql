GO
/****** Object:  Table [dbo].[BizAPIThrottlePolicy]    Script Date: 04/30/2014 10:31:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BizAPIThrottlePolicy](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RateLimitKey] [varchar](50) NOT NULL,
	[bitIsIPAddress] [bit] NOT NULL,
	[numPerSecond] [numeric](18, 0) NULL,
	[numPerMinute] [numeric](18, 0) NULL,
	[numPerHour] [numeric](18, 0) NULL,
	[numPerDay] [numeric](18, 0) NULL,
	[numPerWeek] [numeric](18, 0) NULL,
 CONSTRAINT [PK_BizAPIIPRateLimit] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON