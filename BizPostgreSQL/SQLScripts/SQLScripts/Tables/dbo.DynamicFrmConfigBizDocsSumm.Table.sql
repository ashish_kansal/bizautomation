/****** Object:  Table [dbo].[DynamicFrmConfigBizDocsSumm]    Script Date: 07/26/2008 17:24:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DynamicFrmConfigBizDocsSumm](
	[numFormID] [numeric](18, 0) NOT NULL,
	[numBizDocID] [numeric](18, 0) NOT NULL,
	[numFormFieldID] [numeric](18, 0) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL
) ON [PRIMARY]
GO
