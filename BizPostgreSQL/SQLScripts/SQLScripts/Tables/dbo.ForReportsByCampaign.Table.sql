/****** Object:  Table [dbo].[ForReportsByCampaign]    Script Date: 07/26/2008 17:25:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ForReportsByCampaign](
	[numRepID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numUserCntID] [numeric](18, 0) NULL,
	[numCampaignID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_ForReportsByCampaign] PRIMARY KEY CLUSTERED 
(
	[numRepID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
