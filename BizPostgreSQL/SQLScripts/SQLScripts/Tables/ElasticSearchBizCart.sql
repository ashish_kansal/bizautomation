/****** Object:  Table [dbo].[ElasticSearchBizCart]    Script Date: 10/04/2020 10:01:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ElasticSearchBizCart](
	[numElasticSearchBizCartID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[vcModule] [varchar](50) NULL,
	[numRecordID] [numeric](18, 0) NULL,
	[vcAction] [varchar](50) NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[numSiteID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_ElasticSearchBizCart] PRIMARY KEY CLUSTERED 
(
	[numElasticSearchBizCartID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO