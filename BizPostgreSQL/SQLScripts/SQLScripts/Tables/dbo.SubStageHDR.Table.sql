/****** Object:  Table [dbo].[SubStageHDR]    Script Date: 07/26/2008 17:30:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SubStageHDR](
	[numSubStageHdrID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numProcessId] [numeric](18, 0) NOT NULL,
	[vcSubStageName] [varchar](100) NOT NULL,
 CONSTRAINT [PK_SubStageHDR] PRIMARY KEY CLUSTERED 
(
	[numSubStageHdrID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
