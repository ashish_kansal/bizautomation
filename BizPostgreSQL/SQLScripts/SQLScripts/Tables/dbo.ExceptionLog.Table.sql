USE [BizExch2007DB]
GO
/****** Object:  Table [dbo].[ExceptionLog]    Script Date: 08/10/2008 09:45:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ExceptionLog](
	[numExceptionID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[vcMessage] [varchar](max) NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[numUserContactID] [numeric](18, 0) NULL,
	[dtCreatedDate] [datetime] NULL,
	[numCaseID] [numeric](18, 0) NULL,
	[vcURL] [varchar](1000) NULL,
	[vcReferrer] [varchar](1000) NULL,
	[vcBrowser] [varchar](100) NULL,
	[vcBrowserAgent] [varchar](500) NULL,
 CONSTRAINT [PK_ExceptionLog] PRIMARY KEY CLUSTERED 
(
	[numExceptionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
