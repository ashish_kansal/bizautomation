USE [Production.2014]
GO

/****** Object:  Table [dbo].[MessageMaster]    Script Date: 17-07-2020 13:56:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MessageMaster](
	[numMessageId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numTopicId] [numeric](18, 0) NULL,
	[numParentMessageId] [numeric](18, 0) NULL,
	[intRecordType] [int] NULL,
	[numRecordId] [numeric](18, 0) NULL,
	[vcMessage] [varchar](max) NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[dtmCreatedOn] [datetime] NULL,
	[numUpdatedBy] [numeric](18, 0) NULL,
	[dtmUpdatedOn] [datetime] NULL,
	[bitIsInternal] [bit] NULL,
	[numDomainId] [numeric](18, 0) NULL,
 CONSTRAINT [PK_MessageMaster] PRIMARY KEY CLUSTERED 
(
	[numMessageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


