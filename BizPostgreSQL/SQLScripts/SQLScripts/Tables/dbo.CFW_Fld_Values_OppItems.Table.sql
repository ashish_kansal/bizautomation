USE [Production.2014]
GO

/****** Object:  Table [dbo].[CFW_Fld_Values_OppItems]    Script Date: 01-12-2017 15:33:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CFW_Fld_Values_OppItems](
	[FldDTLID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fld_ID] [numeric](18, 0) NULL,
	[Fld_Value] [varchar](max) NULL,
	[RecId] [numeric](18, 0) NULL,
 CONSTRAINT [PK_CFW_Fld_Values_OppItems] PRIMARY KEY CLUSTERED 
(
	[FldDTLID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[CFW_Fld_Values_OppItems]  WITH CHECK ADD  CONSTRAINT [FK_CFW_Fld_Values_OppItems_CFW_Fld_Master] FOREIGN KEY([Fld_ID])
REFERENCES [dbo].[CFW_Fld_Master] ([Fld_id])
GO

ALTER TABLE [dbo].[CFW_Fld_Values_OppItems] CHECK CONSTRAINT [FK_CFW_Fld_Values_OppItems_CFW_Fld_Master]
GO


