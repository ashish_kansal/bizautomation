/****** Object:  Table [dbo].[CompanyAssets]    Script Date: 07/26/2008 17:22:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CompanyAssets](
	[numAItemCode] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numWareHouseItmsDtlId] [numeric](18, 0) NULL,
	[numItemCode] [numeric](18, 0) NULL,
	[bitSerializable] [bit] NULL,
	[numUnit] [numeric](18, 0) NULL,
	[numDivId] [numeric](18, 0) NOT NULL CONSTRAINT [DF_CompanyAssets_numDivId]  DEFAULT ((0)),
	[numDomainId] [numeric](18, 0) NOT NULL CONSTRAINT [DF_CompanyAssets_numDomainId]  DEFAULT ((0)),
	[numOppId] [numeric](18, 0) NOT NULL CONSTRAINT [DF_CompanyAssets_numOppId]  DEFAULT ((0)),
 CONSTRAINT [PK_CompanyAssets] PRIMARY KEY CLUSTERED 
(
	[numAItemCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
