/****** Object:  Table [dbo].[CFW_FLD_Values_Case]    Script Date: 07/26/2008 17:21:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CFW_FLD_Values_Case](
	[Fld_ID] [numeric](18, 0) NOT NULL,
	[Fld_Value] [varchar](100) NULL,
	[RecId] [numeric](18, 0) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[CFW_FLD_Values_Case]  WITH CHECK ADD  CONSTRAINT [FK_CFW_FLD_Values_Case_CFW_Fld_Master] FOREIGN KEY([Fld_ID])
REFERENCES [dbo].[CFW_Fld_Master] ([Fld_id])
GO
ALTER TABLE [dbo].[CFW_FLD_Values_Case] CHECK CONSTRAINT [FK_CFW_FLD_Values_Case_CFW_Fld_Master]
GO
