/****** Object:  Table [dbo].[RelFollowupStatus]    Script Date: 07/26/2008 17:29:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RelFollowupStatus](
	[numRelFolID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numRelationshipID] [numeric](18, 0) NOT NULL,
	[numFollowID] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_RelFollowupStatus] PRIMARY KEY CLUSTERED 
(
	[numRelFolID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
