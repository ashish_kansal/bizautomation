/****** Object:  Table [dbo].[ItemExtendedDetails]    Script Date: 07/26/2008 17:26:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ItemExtendedDetails](
	[numItemCode] [numeric](18, 0) NOT NULL,
	[txtDesc] [ntext] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
