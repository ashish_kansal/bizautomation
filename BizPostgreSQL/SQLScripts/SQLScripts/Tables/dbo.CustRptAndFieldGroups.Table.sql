/****** Object:  Table [dbo].[CustRptAndFieldGroups]    Script Date: 07/26/2008 17:23:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustRptAndFieldGroups](
	[vcFieldGroupName] [nvarchar](50) NOT NULL,
	[numReportOptionGroupId] [numeric](18, 0) NOT NULL,
	[numCustModuleId] [numeric](18, 0) NOT NULL CONSTRAINT [DF_CustRptAndFieldGroups_numCustModuleId]  DEFAULT ((0)),
 CONSTRAINT [PK_CustRptAndFieldGroups] PRIMARY KEY CLUSTERED 
(
	[numReportOptionGroupId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
