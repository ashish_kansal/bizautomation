/****** Object:  Table [dbo].[EmailUsers]    Script Date: 07/26/2008 17:24:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailUsers](
	[numContactID] [numeric](18, 0) NULL,
	[numUserCntID] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
