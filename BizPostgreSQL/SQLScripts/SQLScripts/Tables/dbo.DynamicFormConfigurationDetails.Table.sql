/****** Object:  Table [dbo].[DynamicFormConfigurationDetails]    Script Date: 07/26/2008 17:24:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DynamicFormConfigurationDetails](
	[numFormId] [numeric](18, 0) NOT NULL,
	[numFormFieldId] [numeric](18, 0) NOT NULL,
	[vcDbColumnName] [varchar](50) NOT NULL,
	[vcAssociatedControlType] [nvarchar](50) NOT NULL,
	[vcFieldType] [char](1) NULL,
	[intColumnNum] [int] NOT NULL,
	[intRowNum] [int] NOT NULL,
	[boolRequired] [bit] NOT NULL,
	[numListID] [numeric](18, 0) NULL,
	[boolAOIField] [bit] NULL,
	[numAuthGroupID] [numeric](18, 0) NOT NULL,
	[numDomainId] [numeric](18, 0) NOT NULL CONSTRAINT [DF_DynamicFormConfigurationDetails_numDomainId]  DEFAULT (1)
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
