/****** Object:  Table [dbo].[OpportunityStageDetails]    Script Date: 07/26/2008 17:27:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OpportunityStageDetails](
	[numOppStageId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numOppId] [numeric](18, 0) NOT NULL,
	[numstagepercentage] [numeric](18, 0) NOT NULL,
	[vcStageDetail] [varchar](1000) NOT NULL,
	[vcComments] [varchar](1000) NULL,
	[numDomainId] [numeric](18, 0) NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[bintCreatedDate] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[bintModifiedDate] [datetime] NULL,
	[numAssignTo] [numeric](18, 0) NULL CONSTRAINT [DF__Opportuni__numAs__4B622666]  DEFAULT ((0)),
	[numStage] [numeric](18, 0) NULL,
	[bintStageComDate] [datetime] NULL,
	[bintDueDate] [datetime] NULL,
	[bitAlert] [bit] NULL CONSTRAINT [DF__Opportuni__bitAl__4C564A9F]  DEFAULT ((0)),
	[bitStageCompleted] [bit] NULL CONSTRAINT [DF__Opportuni__bitSt__4D4A6ED8]  DEFAULT ((0)),
	[vcStagePercentageDtl] [varchar](500) NULL,
	[numTemplateId] [numeric](18, 0) NULL CONSTRAINT [DF_OpportunityStageDetails_numTemplateId]  DEFAULT ((0)),
	[tintPercentage] [tinyint] NULL,
	[numEvent] [tinyint] NULL,
	[numReminder] [numeric](18, 0) NULL,
	[numET] [numeric](18, 0) NULL,
	[numActivity] [numeric](18, 0) NULL,
	[numStartDate] [tinyint] NULL,
	[numStartTime] [tinyint] NULL,
	[numStartTimePeriod] [tinyint] NULL,
	[numEndTime] [tinyint] NULL,
	[numEndTimePeriod] [tinyint] NULL,
	[txtCom] [varchar](500) NULL,
	[numChgStatus] [numeric](18, 0) NULL,
	[bitChgStatus] [bit] NULL,
	[bitClose] [bit] NULL,
	[numType] [numeric](18, 0) NULL,
	[numCommActId] [numeric](18, 0) NULL,
 CONSTRAINT [PK_OpportunityStageDetails] PRIMARY KEY CLUSTERED 
(
	[numOppStageId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[OpportunityStageDetails]  WITH NOCHECK ADD  CONSTRAINT [FK_OpportunityStageDetails_OpportunityMaster] FOREIGN KEY([numOppId])
REFERENCES [dbo].[OpportunityMaster] ([numOppId])
GO
ALTER TABLE [dbo].[OpportunityStageDetails] CHECK CONSTRAINT [FK_OpportunityStageDetails_OpportunityMaster]
GO
