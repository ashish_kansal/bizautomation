/****** Object:  Table [dbo].[PortalWorkflow]    Script Date: 07/26/2008 17:28:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PortalWorkflow](
	[numPortalWFID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[tintCreateOppStatus] [tinyint] NULL,
	[numStatus] [numeric](18, 0) NULL,
	[numRelationShip] [numeric](18, 0) NULL,
	[tintCreateBizDoc] [tinyint] NULL,
	[numBizDocName] [numeric](18, 0) NULL,
	[tintAccessAllowed] [tinyint] NULL,
	[vcOrderSuccessfullPage] [varchar](50) NULL,
	[vcOrderUnsucessfullPage] [varchar](100) NULL,
	[numDomainID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_PortalWorkflow] PRIMARY KEY CLUSTERED 
(
	[numPortalWFID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
