/****** Object:  Table [dbo].[ProjectsStageDetails]    Script Date: 07/26/2008 17:28:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProjectsStageDetails](
	[numProStageId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numProId] [numeric](18, 0) NULL,
	[numStagePercentage] [numeric](18, 0) NULL,
	[vcStageDetail] [varchar](1000) NULL,
	[numDomainId] [numeric](18, 0) NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[bintCreatedDate] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[bintModifiedDate] [datetime] NULL,
	[bintDueDate] [datetime] NULL,
	[bintStageComDate] [datetime] NULL,
	[vcComments] [varchar](1000) NULL,
	[numStage] [numeric](18, 0) NULL,
	[numAssignTo] [numeric](18, 0) NULL,
	[bitAlert] [bit] NULL,
	[bitStageCompleted] [bit] NULL,
	[vcStagePercentageDtl] [varchar](500) NULL,
	[numTemplateId] [numeric](18, 0) NULL CONSTRAINT [DF_ProjectsStageDetails_numTemplateId]  DEFAULT ((0)),
	[tintPercentage] [tinyint] NULL,
	[numEvent] [tinyint] NULL,
	[numReminder] [numeric](18, 0) NULL,
	[numET] [numeric](18, 0) NULL,
	[numActivity] [numeric](18, 0) NULL,
	[numStartDate] [tinyint] NULL,
	[numStartTime] [tinyint] NULL,
	[numStartTimePeriod] [tinyint] NULL,
	[numEndTime] [tinyint] NULL,
	[numEndTimePeriod] [tinyint] NULL,
	[txtCom] [varchar](500) NULL,
	[numChgStatus] [numeric](18, 0) NULL,
	[bitChgStatus] [bit] NULL,
	[bitClose] [bit] NULL,
	[numType] [numeric](18, 0) NULL,
	[numCommActId] [numeric](18, 0) NULL,
 CONSTRAINT [PK_ProjectsStageDetails] PRIMARY KEY CLUSTERED 
(
	[numProStageId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ProjectsStageDetails]  WITH CHECK ADD  CONSTRAINT [FK_ProjectsStageDetails_ProjectsMaster] FOREIGN KEY([numProId])
REFERENCES [dbo].[ProjectsMaster] ([numProId])
GO
ALTER TABLE [dbo].[ProjectsStageDetails] CHECK CONSTRAINT [FK_ProjectsStageDetails_ProjectsMaster]
GO
