/****** Object:  Table [dbo].[Forecast]    Script Date: 07/26/2008 17:25:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Forecast](
	[numForecastID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[sintYear] [smallint] NOT NULL,
	[tintquarter] [tinyint] NOT NULL,
	[tintMonth] [tinyint] NOT NULL,
	[numItemCode] [numeric](18, 0) NULL,
	[monQuota] [money] NULL,
	[monForecastAmount] [money] NULL CONSTRAINT [DF_Forecast_forecastamount]  DEFAULT (0),
	[tintForecastType] [tinyint] NULL,
	[numCreatedBy] [numeric](18, 0) NOT NULL,
	[bintCreatedDate] [datetime] NULL,
	[bintModifiedDate] [datetime] NULL,
	[numModifiedBy] [numeric](19, 0) NULL,
	[numDomainID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_Forecast] PRIMARY KEY CLUSTERED 
(
	[numForecastID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
