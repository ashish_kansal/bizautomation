/****** Object:  Table [dbo].[ContractManagement]    Script Date: 07/26/2008 17:22:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContractManagement](
	[numContractId] [bigint] IDENTITY(1,1) NOT NULL,
	[numDivisionID] [int] NULL,
	[vcContractName] [varchar](250) NULL,
	[bintStartDate] [datetime] NULL,
	[bintExpDate] [datetime] NULL,
	[numIncidents] [int] NULL,
	[numHours] [int] NULL,
	[numEmailDays] [int] NULL,
	[numEmailIncidents] [int] NULL,
	[numEmailHours] [int] NULL,
	[numUserCntID] [int] NULL,
	[numDomainID] [int] NULL,
	[vcNotes] [text] NULL,
	[bintCreatedOn] [datetime] NULL,
	[numAmount] [int] NULL,
	[bitAmount] [bit] NOT NULL CONSTRAINT [DF_ContractManagement_bitAmount]  DEFAULT ((1)),
	[bitIncidents] [bit] NOT NULL CONSTRAINT [DF_ContractManagement_bitIncidents]  DEFAULT ((1)),
	[bitDays] [bit] NOT NULL CONSTRAINT [DF_ContractManagement_bitDays]  DEFAULT ((1)),
	[bitHour] [bit] NOT NULL CONSTRAINT [DF_ContractManagement_bitHour]  DEFAULT ((1)),
	[bitEDays] [bit] NOT NULL CONSTRAINT [DF_ContractManagement_bitEDays]  DEFAULT ((0)),
	[bitEIncidents] [bit] NOT NULL CONSTRAINT [DF_ContractManagement_bitEIncidents]  DEFAULT ((0)),
	[bitEHour] [bit] NOT NULL CONSTRAINT [DF_ContractManagement_bitEHours]  DEFAULT ((0)),
	[numTemplateId] [numeric](18, 0) NOT NULL CONSTRAINT [DF_ContractManagement_numTemplateId]  DEFAULT ((0)),
	[decRate] [decimal](10, 2) NOT NULL CONSTRAINT [DF_ContractManagement_decRate]  DEFAULT ((0)),
 CONSTRAINT [PK_ContractManagement] PRIMARY KEY CLUSTERED 
(
	[numContractId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
