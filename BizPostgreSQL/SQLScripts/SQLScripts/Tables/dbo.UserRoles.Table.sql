/****** Object:  Table [dbo].[UserRoles]    Script Date: 07/26/2008 17:31:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRoles](
	[numRoleHDRID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numRole] [numeric](18, 0) NULL,
	[numUserCntID] [numeric](18, 0) NULL,
	[fltPercentage] [float] NULL,
 CONSTRAINT [PK_UserRoles] PRIMARY KEY CLUSTERED 
(
	[numRoleHDRID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
