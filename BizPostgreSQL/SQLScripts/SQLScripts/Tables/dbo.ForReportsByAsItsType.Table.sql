/****** Object:  Table [dbo].[ForReportsByAsItsType]    Script Date: 07/26/2008 17:25:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ForReportsByAsItsType](
	[numRepID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numUserCntID] [numeric](18, 0) NULL,
	[numListItemID] [numeric](18, 0) NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[tintType] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
