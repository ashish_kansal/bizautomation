/****** Object:  Table [dbo].[CFW_Fld_Master]    Script Date: 07/26/2008 17:21:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CFW_Fld_Master](
	[Fld_id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fld_type] [varchar](20) NOT NULL,
	[Fld_label] [varchar](45) NOT NULL,
	[Fld_tab_ord] [tinyint] NULL,
	[Grp_id] [numeric](18, 0) NOT NULL,
	[subgrp] [varchar](30) NULL,
	[numlistid] [numeric](18, 0) NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[vcURL] [varchar](1000) NULL,
 CONSTRAINT [PK_CFW_Fld_Master] PRIMARY KEY CLUSTERED 
(
	[Fld_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[CFW_Fld_Master]  WITH CHECK ADD  CONSTRAINT [FK_CFW_Fld_Master_CFW_Loc_Master] FOREIGN KEY([Grp_id])
REFERENCES [dbo].[CFW_Loc_Master] ([Loc_id])
GO
ALTER TABLE [dbo].[CFW_Fld_Master] CHECK CONSTRAINT [FK_CFW_Fld_Master_CFW_Loc_Master]
GO
