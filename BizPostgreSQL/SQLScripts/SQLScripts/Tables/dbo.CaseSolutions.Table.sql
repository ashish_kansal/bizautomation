/****** Object:  Table [dbo].[CaseSolutions]    Script Date: 07/26/2008 17:21:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CaseSolutions](
	[numCaseID] [numeric](18, 0) NOT NULL,
	[numSolnID] [numeric](18, 0) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CaseSolutions]  WITH CHECK ADD  CONSTRAINT [FK_CaseSolutions_Cases] FOREIGN KEY([numCaseID])
REFERENCES [dbo].[Cases] ([numCaseId])
GO
ALTER TABLE [dbo].[CaseSolutions] CHECK CONSTRAINT [FK_CaseSolutions_Cases]
GO
ALTER TABLE [dbo].[CaseSolutions]  WITH CHECK ADD  CONSTRAINT [FK_CaseSolutions_SolutionMaster] FOREIGN KEY([numSolnID])
REFERENCES [dbo].[SolutionMaster] ([numSolnID])
GO
ALTER TABLE [dbo].[CaseSolutions] CHECK CONSTRAINT [FK_CaseSolutions_SolutionMaster]
GO
