/****** Object:  Table [dbo].[BroadCastDtls]    Script Date: 07/26/2008 17:20:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BroadCastDtls](
	[numBroadCastDtlId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numBroadCastID] [numeric](18, 0) NULL,
	[numDivisionID] [numeric](18, 0) NULL,
	[numContactID] [numeric](18, 0) NULL,
	[tintSucessfull] [tinyint] NULL,
	[numNoofTimes] [numeric](18, 0) NULL,
 CONSTRAINT [PK_BroadCastDtls] PRIMARY KEY CLUSTERED 
(
	[numBroadCastDtlId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BroadCastDtls]  WITH CHECK ADD  CONSTRAINT [FK_BroadCastDtls_Broadcast] FOREIGN KEY([numBroadCastID])
REFERENCES [dbo].[Broadcast] ([numBroadCastId])
GO
ALTER TABLE [dbo].[BroadCastDtls] CHECK CONSTRAINT [FK_BroadCastDtls_Broadcast]
GO
