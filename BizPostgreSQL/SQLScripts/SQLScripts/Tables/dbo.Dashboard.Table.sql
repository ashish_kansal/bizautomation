/****** Object:  Table [dbo].[Dashboard]    Script Date: 07/26/2008 17:23:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Dashboard](
	[numDashBoardReptID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numReportID] [numeric](18, 0) NOT NULL,
	[numGroupUserCntID] [numeric](18, 0) NOT NULL,
	[tintRow] [tinyint] NOT NULL,
	[tintColumn] [tinyint] NOT NULL,
	[tintReportType] [tinyint] NOT NULL,
	[vcHeader] [varchar](100) NULL,
	[vcFooter] [varchar](100) NULL,
	[tintChartType] [tinyint] NULL,
	[bitGroup] [bit] NOT NULL CONSTRAINT [DF_Dashboard_bitGroup]  DEFAULT ((0)),
 CONSTRAINT [PK_Dashboard] PRIMARY KEY CLUSTERED 
(
	[numDashBoardReptID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0-User,1-Group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Dashboard', @level2type=N'COLUMN',@level2name=N'bitGroup'
GO
