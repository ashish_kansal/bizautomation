/****** Object:  Table [dbo].[tmpCalandarTbl]    Script Date: 07/26/2008 17:31:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tmpCalandarTbl](
	[ItemId] [nvarchar](300) NOT NULL,
	[ItemIdOccur] [nvarchar](300) NOT NULL,
	[StartDateTimeUtc] [datetime] NULL,
	[OccurIndex] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
