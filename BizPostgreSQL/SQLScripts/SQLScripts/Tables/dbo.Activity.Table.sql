/****** Object:  Table [dbo].[Activity]    Script Date: 07/26/2008 17:20:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Activity](
	[ActivityID] [int] IDENTITY(1,1) NOT NULL,
	[StartDateTimeUtc] [datetime] NOT NULL CONSTRAINT [DF_Activity_StartDateTimeUtc]  DEFAULT (getutcdate()),
	[Duration] [int] NOT NULL CONSTRAINT [DF_Activity_Duration]  DEFAULT ((3600)),
	[Subject] [nvarchar](255) NULL,
	[ActivityDescription] [ntext] NULL,
	[AllDayEvent] [bit] NOT NULL CONSTRAINT [DF_Activity_AllDayEvent]  DEFAULT ((0)),
	[Location] [nvarchar](64) NULL,
	[Status] [int] NOT NULL CONSTRAINT [DF_Activity_Status]  DEFAULT ((0)),
	[EnableReminder] [bit] NOT NULL CONSTRAINT [DF_Activity_EnableReminder]  DEFAULT ((1)),
	[ReminderInterval] [int] NOT NULL CONSTRAINT [DF_Activity_ReminderInterval]  DEFAULT ((900)),
	[ShowTimeAs] [int] NOT NULL CONSTRAINT [DF_Activity_ShowTimeAs]  DEFAULT ((3)),
	[Importance] [int] NOT NULL CONSTRAINT [DF_Activity_Importance]  DEFAULT ((1)),
	[RecurrenceID] [int] NOT NULL CONSTRAINT [DF_Activity_RecurrenceID]  DEFAULT ((-999)),
	[OriginalStartDateTimeUtc] [datetime] NULL,
	[VarianceID] [uniqueidentifier] NULL,
	[_ts] [timestamp] NULL,
	[ItemId] [varchar](500) NULL CONSTRAINT [DF_Activity_ItemId]  DEFAULT ('Id'),
	[ChangeKey] [varchar](500) NULL CONSTRAINT [DF_Activity_ChangeKey]  DEFAULT ('Change'),
	[ItemIdOccur] [varchar](250) NULL,
 CONSTRAINT [Activity_PK] PRIMARY KEY NONCLUSTERED 
(
	[ActivityID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Activity]  WITH NOCHECK ADD  CONSTRAINT [Activity_FK_Recurrence_ID] FOREIGN KEY([RecurrenceID])
REFERENCES [dbo].[Recurrence] ([RecurrenceID])
GO
ALTER TABLE [dbo].[Activity] NOCHECK CONSTRAINT [Activity_FK_Recurrence_ID]
GO
