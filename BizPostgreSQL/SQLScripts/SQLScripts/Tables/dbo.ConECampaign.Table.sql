/****** Object:  Table [dbo].[ConECampaign]    Script Date: 07/26/2008 17:22:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ConECampaign](
	[numConEmailCampID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numContactID] [numeric](18, 0) NULL,
	[numECampaignID] [numeric](18, 0) NULL,
	[intStartDate] [datetime] NULL,
	[bitEngaged] [bit] NULL CONSTRAINT [DF_ConECampaign_bitEngaged]  DEFAULT (0),
	[numRecOwner] [numeric](18, 0) NULL,
 CONSTRAINT [PK_ConECampaign] PRIMARY KEY CLUSTERED 
(
	[numConEmailCampID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ConECampaign]  WITH CHECK ADD  CONSTRAINT [FK_ConECampaign_AdditionalContactsInformation] FOREIGN KEY([numContactID])
REFERENCES [dbo].[AdditionalContactsInformation] ([numContactId])
GO
ALTER TABLE [dbo].[ConECampaign] CHECK CONSTRAINT [FK_ConECampaign_AdditionalContactsInformation]
GO
ALTER TABLE [dbo].[ConECampaign]  WITH CHECK ADD  CONSTRAINT [FK_ConECampaign_ECampaign] FOREIGN KEY([numECampaignID])
REFERENCES [dbo].[ECampaign] ([numECampaignID])
GO
ALTER TABLE [dbo].[ConECampaign] CHECK CONSTRAINT [FK_ConECampaign_ECampaign]
GO
