/****** Object:  Table [dbo].[ItemDetails]    Script Date: 07/26/2008 17:26:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ItemDetails](
	[numItemKitID] [numeric](18, 0) NULL,
	[numChildItemID] [numeric](18, 0) NULL,
	[numQtyItemsReq] [numeric](18, 0) NULL,
	[numWareHouseItemId] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ItemDetails]  WITH CHECK ADD  CONSTRAINT [FK_ItemDetails_Item] FOREIGN KEY([numItemKitID])
REFERENCES [dbo].[Item] ([numItemCode])
GO
ALTER TABLE [dbo].[ItemDetails] CHECK CONSTRAINT [FK_ItemDetails_Item]
GO
ALTER TABLE [dbo].[ItemDetails]  WITH CHECK ADD  CONSTRAINT [FK_ItemDetails_Item1] FOREIGN KEY([numChildItemID])
REFERENCES [dbo].[Item] ([numItemCode])
GO
ALTER TABLE [dbo].[ItemDetails] CHECK CONSTRAINT [FK_ItemDetails_Item1]
GO
