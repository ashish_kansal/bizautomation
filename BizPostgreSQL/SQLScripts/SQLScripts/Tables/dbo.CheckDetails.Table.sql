/****** Object:  Table [dbo].[CheckDetails]    Script Date: 07/26/2008 17:22:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CheckDetails](
	[numCheckId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numAcntTypeId] [numeric](18, 0) NULL,
	[datCheckDate] [smalldatetime] NULL,
	[monCheckAmt] [money] NULL,
	[varMemo] [varchar](200) NULL,
	[numCustomerId] [numeric](18, 0) NULL,
	[numDomainId] [numeric](18, 0) NULL,
	[numRecurringId] [numeric](18, 0) NULL,
	[LastRecurringDate] [datetime] NULL,
	[numNoTransactions] [numeric](18, 0) NULL,
	[numCheckNo] [numeric](18, 0) NULL,
	[numBankRoutingNo] [numeric](18, 0) NULL,
	[numBankAccountNo] [numeric](18, 0) NULL,
	[numCheckCompanyId] [numeric](18, 0) NULL,
	[bitIsPrint] [bit] NOT NULL CONSTRAINT [DF_CheckDetails_bitIsPrint]  DEFAULT ((0)),
	[numCreatedBy] [numeric](18, 0) NULL,
	[dtCreatedDate] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[dtModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_CheckDetails] PRIMARY KEY CLUSTERED 
(
	[numCheckId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[CheckDetails]  WITH CHECK ADD  CONSTRAINT [FK_CheckDetails_CheckCompanyDetails] FOREIGN KEY([numCheckCompanyId])
REFERENCES [dbo].[CheckCompanyDetails] ([numCheckCompanyId])
GO
ALTER TABLE [dbo].[CheckDetails] CHECK CONSTRAINT [FK_CheckDetails_CheckCompanyDetails]
GO
ALTER TABLE [dbo].[CheckDetails]  WITH CHECK ADD  CONSTRAINT [FK_CheckDetails_RecurringTemplate] FOREIGN KEY([numRecurringId])
REFERENCES [dbo].[RecurringTemplate] ([numRecurringId])
GO
ALTER TABLE [dbo].[CheckDetails] CHECK CONSTRAINT [FK_CheckDetails_RecurringTemplate]
GO
