/****** Object:  Table [dbo].[ImapUserDetails]    Script Date: 07/26/2008 17:25:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ImapUserDetails](
	[bitImap] [bit] NULL,
	[vcImapServerUrl] [varchar](200) NULL,
	[vcImapPassword] [varchar](200) NULL,
	[bitSSl] [bit] NULL,
	[numPort] [numeric](18, 0) NULL,
	[numDomainId] [numeric](18, 0) NULL,
	[numUserCntId] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
