/****** Object:  Table [dbo].[RecImportConfg]    Script Date: 07/26/2008 17:28:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RecImportConfg](
	[numFormFieldId] [numeric](18, 0) NULL,
	[numDomainId] [numeric](18, 0) NULL,
	[bitCustomFld] [bit] NULL,
	[numRelationShip] [numeric](18, 0) NULL,
	[intColumn] [numeric](18, 0) NULL,
	[cCType] [char](1) NULL,
	[ImportType] [tinyint] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
