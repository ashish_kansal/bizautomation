/****** Object:  Table [dbo].[ListOrder]    Script Date: 07/26/2008 17:26:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ListOrder](
	[numListId] [numeric](18, 0) NULL,
	[numListItemId] [numeric](18, 0) NULL,
	[numDomainId] [numeric](18, 0) NULL,
	[intSortOrder] [tinyint] NULL
) ON [PRIMARY]
GO
