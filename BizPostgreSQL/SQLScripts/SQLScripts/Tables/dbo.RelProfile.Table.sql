/****** Object:  Table [dbo].[RelProfile]    Script Date: 07/26/2008 17:29:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RelProfile](
	[numRelProID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numRelationshipID] [numeric](18, 0) NOT NULL,
	[numProfileID] [numeric](18, 0) NOT NULL,
	[numOrder] [tinyint] NOT NULL CONSTRAINT [DF_RelProfile_numOrder]  DEFAULT ((0)),
 CONSTRAINT [PK_RelProfile] PRIMARY KEY CLUSTERED 
(
	[numRelProID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
