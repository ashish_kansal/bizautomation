/****** Object:  Table [dbo].[MergeFldDtls]    Script Date: 07/26/2008 17:26:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MergeFldDtls](
	[numAlertDTLID] [numeric](18, 0) NULL,
	[numMergeFlsID] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
