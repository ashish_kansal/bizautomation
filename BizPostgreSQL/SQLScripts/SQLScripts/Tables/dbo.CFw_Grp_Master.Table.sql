/****** Object:  Table [dbo].[CFw_Grp_Master]    Script Date: 07/26/2008 17:21:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CFw_Grp_Master](
	[Grp_id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Grp_Name] [varchar](50) NULL,
	[Loc_Id] [numeric](18, 0) NOT NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[tintType] [tinyint] NOT NULL CONSTRAINT [DF_CFw_Grp_Master_tintType]  DEFAULT ((0)),
	[vcURLF] [varchar](1000) NULL,
 CONSTRAINT [PK_CFw_Grp_Master] PRIMARY KEY CLUSTERED 
(
	[Grp_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
