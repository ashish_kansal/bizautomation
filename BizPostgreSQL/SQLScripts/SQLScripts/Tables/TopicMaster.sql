USE [Production.2014]
GO

/****** Object:  Table [dbo].[TopicMaster]    Script Date: 16-07-2020 21:19:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TopicMaster](
	[numTopicId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[intRecordType] [int] NULL,
	[numRecordId] [numeric](18, 0) NULL,
	[vcTopicTitle] [varchar](max) NULL,
	[numCreateBy] [numeric](18, 0) NULL,
	[dtmCreatedOn] [datetime] NULL,
	[numUpdatedBy] [numeric](18, 0) NULL,
	[dtmUpdatedOn] [datetime] NULL,
	[bitIsInternal] [bit] NULL,
	[numDomainId] [numeric](18, 0) NULL,
 CONSTRAINT [PK_TopicMaster] PRIMARY KEY CLUSTERED 
(
	[numTopicId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


