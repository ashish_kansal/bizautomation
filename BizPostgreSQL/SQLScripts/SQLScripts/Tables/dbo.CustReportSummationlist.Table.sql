/****** Object:  Table [dbo].[CustReportSummationlist]    Script Date: 07/26/2008 17:23:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CustReportSummationlist](
	[numCustomReportId] [numeric](18, 0) NULL,
	[vcFieldName] [varchar](200) NULL,
	[bitSum] [bit] NULL CONSTRAINT [DF_CustReportSummationlist_bitSum]  DEFAULT ((0)),
	[bitAvg] [bit] NULL CONSTRAINT [DF_CustReportSummationlist_bitAvg]  DEFAULT ((0)),
	[bitMin] [bit] NULL CONSTRAINT [DF_CustReportSummationlist_bitMin]  DEFAULT ((0)),
	[bitMax] [bit] NULL CONSTRAINT [DF_CustReportSummationlist_bitMax]  DEFAULT ((0))
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
