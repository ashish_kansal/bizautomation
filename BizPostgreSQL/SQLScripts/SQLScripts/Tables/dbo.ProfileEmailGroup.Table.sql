/****** Object:  Table [dbo].[ProfileEmailGroup]    Script Date: 07/26/2008 17:28:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProfileEmailGroup](
	[numEmailGroupID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[vcEmailGroupName] [varchar](100) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[dtCreatedDate] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[dtModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_ProfileEmailGroup] PRIMARY KEY CLUSTERED 
(
	[numEmailGroupID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
