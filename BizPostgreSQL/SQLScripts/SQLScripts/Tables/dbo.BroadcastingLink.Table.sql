GO
/****** Object:  Table [dbo].[BroadcastingLink]    Script Date: 07/27/2012 11:19:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BroadcastingLink](
	[numLinkId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numBroadcastId] [numeric](18, 0) NOT NULL,
	[vcOriginalLink] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[vcBizLink] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_BroadcastingLink] PRIMARY KEY CLUSTERED 
(
	[numLinkId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
