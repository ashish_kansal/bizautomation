/****** Object:  Table [dbo].[DepositDetails]    Script Date: 07/26/2008 17:23:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DepositDetails](
	[numDepositId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numChartAcntId] [numeric](18, 0) NULL,
	[dtDepositDate] [datetime] NULL,
	[monDepositAmount] [money] NULL,
	[numDomainId] [numeric](18, 0) NULL,
	[numRecurringId] [numeric](18, 0) NULL,
	[dtLastRecurringDate] [datetime] NULL,
	[numNoTransactions] [numeric](18, 0) NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[dtCreationDate] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[dtModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_DepositDetails] PRIMARY KEY CLUSTERED 
(
	[numDepositId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DepositDetails]  WITH CHECK ADD  CONSTRAINT [FK_DepositDetails_RecurringTemplate] FOREIGN KEY([numRecurringId])
REFERENCES [dbo].[RecurringTemplate] ([numRecurringId])
GO
ALTER TABLE [dbo].[DepositDetails] CHECK CONSTRAINT [FK_DepositDetails_RecurringTemplate]
GO
