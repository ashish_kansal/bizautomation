/****** Object:  Table [dbo].[ProjectsMaster]    Script Date: 07/26/2008 17:28:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProjectsMaster](
	[numProId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[vcProjectName] [varchar](100) NOT NULL,
	[numIntPrjMgr] [numeric](18, 0) NULL,
	[numOppId] [numeric](18, 0) NULL,
	[intDueDate] [datetime] NULL,
	[numCustPrjMgr] [numeric](18, 0) NULL,
	[numDivisionId] [numeric](18, 0) NULL,
	[txtComments] [varchar](1000) NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[bintCreatedDate] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[bintModifiedDate] [datetime] NULL,
	[numDomainId] [numeric](18, 0) NULL,
	[numRecOwner] [numeric](18, 0) NULL,
	[tintProStatus] [tinyint] NULL CONSTRAINT [DF_ProjectsMaster_tintProStatus]  DEFAULT (0),
	[bintProClosingDate] [datetime] NULL,
	[numAssignedby] [numeric](18, 0) NULL,
	[numAssignedTo] [numeric](18, 0) NULL,
	[numContractId] [numeric](18, 0) NOT NULL CONSTRAINT [DF_ProjectsMaster_numContractId]  DEFAULT ((0)),
 CONSTRAINT [PK_ProjectsMaster] PRIMARY KEY CLUSTERED 
(
	[numProId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
