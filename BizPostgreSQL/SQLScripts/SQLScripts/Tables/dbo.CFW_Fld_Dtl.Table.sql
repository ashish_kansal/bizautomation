/****** Object:  Table [dbo].[CFW_Fld_Dtl]    Script Date: 07/26/2008 17:21:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CFW_Fld_Dtl](
	[numFieldDtlId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numFieldId] [numeric](18, 0) NOT NULL,
	[numRelation] [numeric](18, 0) NULL,
	[tintFldReq] [tinyint] NULL CONSTRAINT [DF_CFW_Fld_Dtl_tintFldReq]  DEFAULT ((0)),
	[numOrder] [numeric](18, 0) NULL,
 CONSTRAINT [PK_CLF_Field_Dtl] PRIMARY KEY CLUSTERED 
(
	[numFieldDtlId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CFW_Fld_Dtl]  WITH CHECK ADD  CONSTRAINT [FK_CFW_Fld_Dtl_CFW_Fld_Master] FOREIGN KEY([numFieldId])
REFERENCES [dbo].[CFW_Fld_Master] ([Fld_id])
GO
ALTER TABLE [dbo].[CFW_Fld_Dtl] CHECK CONSTRAINT [FK_CFW_Fld_Dtl_CFW_Fld_Master]
GO
