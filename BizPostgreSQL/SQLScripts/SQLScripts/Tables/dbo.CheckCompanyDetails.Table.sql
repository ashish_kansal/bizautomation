/****** Object:  Table [dbo].[CheckCompanyDetails]    Script Date: 07/26/2008 17:21:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CheckCompanyDetails](
	[numCheckCompanyId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numCompanyId] [numeric](18, 0) NULL,
	[monAmount] [money] NULL,
	[vcMemo] [varchar](200) NULL,
	[numDomainId] [numeric](18, 0) NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[dtCreatedDate] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[dtModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_CheckCompanyDetails] PRIMARY KEY CLUSTERED 
(
	[numCheckCompanyId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
