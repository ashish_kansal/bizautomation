/****** Object:  Table [dbo].[ResourcePreference]    Script Date: 07/26/2008 17:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ResourcePreference](
	[ResourceID] [int] NOT NULL,
	[EnableEmailReminders] [int] NOT NULL CONSTRAINT [DF_ResourcePreference_EnableEmailReminders]  DEFAULT ((0)),
 CONSTRAINT [ResourcePreference_PK] PRIMARY KEY NONCLUSTERED 
(
	[ResourceID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ResourcePreference]  WITH NOCHECK ADD  CONSTRAINT [ResourcePreference_FK_Resource_ID] FOREIGN KEY([ResourceID])
REFERENCES [dbo].[Resource] ([ResourceID])
GO
ALTER TABLE [dbo].[ResourcePreference] NOCHECK CONSTRAINT [ResourcePreference_FK_Resource_ID]
GO
