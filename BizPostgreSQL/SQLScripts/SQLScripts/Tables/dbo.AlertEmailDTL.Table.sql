/****** Object:  Table [dbo].[AlertEmailDTL]    Script Date: 07/26/2008 17:20:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AlertEmailDTL](
	[numAlertEmailID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numAlertDTLId] [numeric](18, 0) NULL,
	[vcEmailID] [varchar](100) NULL,
	[numDomainID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_AlertEmailDTL] PRIMARY KEY CLUSTERED 
(
	[numAlertEmailID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[AlertEmailDTL]  WITH CHECK ADD  CONSTRAINT [FK_AlertEmailDTL_AlertDTL] FOREIGN KEY([numAlertDTLId])
REFERENCES [dbo].[AlertDTL] ([numAlertDTLid])
GO
ALTER TABLE [dbo].[AlertEmailDTL] CHECK CONSTRAINT [FK_AlertEmailDTL_AlertDTL]
GO
