/****** Object:  Table [dbo].[CampaignDetails]    Script Date: 07/26/2008 17:20:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CampaignDetails](
	[numCampaignId] [numeric](18, 0) NOT NULL,
	[numContactId] [numeric](18, 0) NOT NULL,
	[numCompanyId] [numeric](18, 0) NOT NULL
) ON [PRIMARY]
GO
