/****** Object:  Table [dbo].[RoutingLeadDetails]    Script Date: 07/26/2008 17:29:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoutingLeadDetails](
	[numRoutOwnerDTLID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numRoutID] [numeric](18, 0) NOT NULL,
	[numEmpId] [numeric](18, 0) NOT NULL,
	[intAssignOrder] [int] NOT NULL,
 CONSTRAINT [PK_RoutingLeadDetails] PRIMARY KEY CLUSTERED 
(
	[numRoutOwnerDTLID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RoutingLeadDetails]  WITH NOCHECK ADD  CONSTRAINT [FK_RoutingLeadDetails_RoutingLeads] FOREIGN KEY([numRoutID])
REFERENCES [dbo].[RoutingLeads] ([numRoutID])
GO
ALTER TABLE [dbo].[RoutingLeadDetails] CHECK CONSTRAINT [FK_RoutingLeadDetails_RoutingLeads]
GO
