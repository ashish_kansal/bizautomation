/****** Object:  Table [dbo].[OrganisationUnit]    Script Date: 07/26/2008 17:27:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrganisationUnit](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ParentId] [int] NULL CONSTRAINT [DF_OrganisationUnit_ParentId]  DEFAULT (NULL),
	[Name] [varchar](100) NOT NULL,
	[Path] [varchar](200) NOT NULL CONSTRAINT [DF_OrganisationUnit_Path]  DEFAULT (''),
	[PathLevel] [int] NOT NULL CONSTRAINT [DF_OrganisationUnit_PathLevel]  DEFAULT ((0)),
	[ChildCount] [int] NOT NULL CONSTRAINT [DF_OrganisationUnit_ChildCount]  DEFAULT ((0)),
 CONSTRAINT [PK_OrganisationUnit] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[OrganisationUnit]  WITH NOCHECK ADD  CONSTRAINT [FK_OrganisationUnit_OrganisationUnit] FOREIGN KEY([ParentId])
REFERENCES [dbo].[OrganisationUnit] ([Id])
NOT FOR REPLICATION
GO
ALTER TABLE [dbo].[OrganisationUnit] NOCHECK CONSTRAINT [FK_OrganisationUnit_OrganisationUnit]
GO
