/****** Object:  Table [dbo].[State]    Script Date: 07/26/2008 17:30:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[State](
	[numStateID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numCountryID] [numeric](18, 0) NOT NULL,
	[vcState] [varchar](100) NOT NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[bintCreatedDate] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[bintModifiedDate] [datetime] NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[constFlag] [bit] NULL CONSTRAINT [DF_State_constFlag1]  DEFAULT (0),
 CONSTRAINT [PK_State] PRIMARY KEY CLUSTERED 
(
	[numStateID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
