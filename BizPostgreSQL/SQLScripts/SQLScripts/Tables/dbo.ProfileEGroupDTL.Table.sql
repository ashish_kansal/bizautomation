/****** Object:  Table [dbo].[ProfileEGroupDTL]    Script Date: 07/26/2008 17:28:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProfileEGroupDTL](
	[numEGroupDTLID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numEmailGroupID] [numeric](18, 0) NULL,
	[numContactID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_ProfileEGroupDTL] PRIMARY KEY CLUSTERED 
(
	[numEGroupDTLID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ProfileEGroupDTL]  WITH CHECK ADD  CONSTRAINT [FK_ProfileEGroupDTL_ProfileEmailGroup] FOREIGN KEY([numEmailGroupID])
REFERENCES [dbo].[ProfileEmailGroup] ([numEmailGroupID])
GO
ALTER TABLE [dbo].[ProfileEGroupDTL] CHECK CONSTRAINT [FK_ProfileEGroupDTL_ProfileEmailGroup]
GO
