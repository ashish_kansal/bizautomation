/****** Object:  Table [dbo].[GroupTabDetails]    Script Date: 07/28/2009 21:52:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupTabDetails](
	[numGroupId] [numeric](18, 0) NULL,
	[numTabId] [numeric](18, 0) NULL,
	[numRelationShip] [numeric](18, 0) NULL,
	[bitallowed] [bit] NULL,
	[numOrder] [numeric](18, 0) NULL,
	[numModuleId] [numeric](18, 0) NULL
) ON [PRIMARY]
