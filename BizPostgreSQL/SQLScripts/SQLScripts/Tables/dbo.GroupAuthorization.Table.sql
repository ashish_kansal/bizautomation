/****** Object:  Table [dbo].[GroupAuthorization]    Script Date: 07/26/2008 17:25:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupAuthorization](
	[numGroupID] [numeric](18, 0) NOT NULL,
	[numModuleID] [numeric](18, 0) NOT NULL,
	[numPageID] [numeric](18, 0) NOT NULL,
	[intExportAllowed] [int] NOT NULL CONSTRAINT [DF_GroupAuthorization_bitExportAllowed]  DEFAULT (0),
	[intPrintAllowed] [int] NOT NULL CONSTRAINT [DF_GroupAuthorization_intPrintAllowed]  DEFAULT (0),
	[intViewAllowed] [int] NOT NULL CONSTRAINT [DF_GroupAuthorization_intViewAllowed]  DEFAULT (0),
	[intAddAllowed] [int] NOT NULL CONSTRAINT [DF_GroupAuthorization_intAddAllowed]  DEFAULT (0),
	[intUpdateAllowed] [int] NOT NULL CONSTRAINT [DF_GroupAuthorization_intUpdateAllowed]  DEFAULT (0),
	[intDeleteAllowed] [int] NOT NULL CONSTRAINT [DF_GroupAuthorization_intDeleteAllowed]  DEFAULT (0),
 CONSTRAINT [PK_GroupAuthorization] PRIMARY KEY CLUSTERED 
(
	[numGroupID] ASC,
	[numModuleID] ASC,
	[numPageID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[GroupAuthorization]  WITH CHECK ADD  CONSTRAINT [FK_GroupAuthorization_ModuleMaster] FOREIGN KEY([numModuleID])
REFERENCES [dbo].[ModuleMaster] ([numModuleID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[GroupAuthorization] CHECK CONSTRAINT [FK_GroupAuthorization_ModuleMaster]
GO
