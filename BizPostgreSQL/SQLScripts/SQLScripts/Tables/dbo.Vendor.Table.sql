/****** Object:  Table [dbo].[Vendor]    Script Date: 07/26/2008 17:31:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Vendor](
	[numVendorTcode] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numVendorID] [numeric](18, 0) NOT NULL,
	[vcPartNo] [varchar](100) NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[monCost] [money] NULL,
	[numItemCode] [numeric](18, 0) NULL,
 CONSTRAINT [PK_Vendor] PRIMARY KEY CLUSTERED 
(
	[numVendorTcode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Vendor]  WITH NOCHECK ADD  CONSTRAINT [FK_Vendor_DivisionMaster] FOREIGN KEY([numVendorID])
REFERENCES [dbo].[DivisionMaster] ([numDivisionID])
GO
ALTER TABLE [dbo].[Vendor] CHECK CONSTRAINT [FK_Vendor_DivisionMaster]
GO
ALTER TABLE [dbo].[Vendor]  WITH CHECK ADD  CONSTRAINT [FK_Vendor_Item] FOREIGN KEY([numItemCode])
REFERENCES [dbo].[Item] ([numItemCode])
GO
ALTER TABLE [dbo].[Vendor] CHECK CONSTRAINT [FK_Vendor_Item]
GO
