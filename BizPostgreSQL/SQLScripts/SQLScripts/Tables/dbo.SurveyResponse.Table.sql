/****** Object:  Table [dbo].[SurveyResponse]    Script Date: 07/26/2008 17:30:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SurveyResponse](
	[numRespondantID] [numeric](18, 0) NOT NULL,
	[numParentSurID] [numeric](18, 0) NOT NULL,
	[numSurID] [numeric](18, 0) NOT NULL,
	[numQuestionID] [numeric](18, 0) NOT NULL,
	[numAnsID] [numeric](18, 0) NOT NULL,
	[vcAnsText] [varchar](500) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[SurveyResponse]  WITH CHECK ADD  CONSTRAINT [FK_SurveyResponse_SurveyMaster] FOREIGN KEY([numSurID])
REFERENCES [dbo].[SurveyMaster] ([numSurID])
GO
ALTER TABLE [dbo].[SurveyResponse] CHECK CONSTRAINT [FK_SurveyResponse_SurveyMaster]
GO
