USE [BizExch2007DB]
GO
/****** Object:  Table [dbo].[AccountingIntegration]    Script Date: 07/26/2008 17:20:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AccountingIntegration](
	[numQBAccountID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[vcQBAccountName] [varchar](100) NULL,
	[vcConnectionstring] [varchar](1000) NULL,
	[vcQBXMLVer] [varchar](10) NULL,
	[tintSelected] [tinyint] NULL CONSTRAINT [DF_AccountingIntegration_tintSelected]  DEFAULT (0),
	[numDomainID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_AccountingIntegration] PRIMARY KEY CLUSTERED 
(
	[numQBAccountID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
