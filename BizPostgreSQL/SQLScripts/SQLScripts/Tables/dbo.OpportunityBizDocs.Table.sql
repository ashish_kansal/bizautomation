/****** Object:  Table [dbo].[OpportunityBizDocs]    Script Date: 07/26/2008 17:26:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OpportunityBizDocs](
	[numOppBizDocsId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numOppId] [numeric](18, 0) NOT NULL,
	[vcBizDocID] [varchar](100) NULL,
	[numBizDocId] [numeric](18, 0) NOT NULL,
	[vcPurchaseOdrNo] [varchar](20) NULL,
	[decDiscount] [float] NULL,
	[numTerms] [numeric](18, 0) NULL,
	[tintFinal] [tinyint] NULL CONSTRAINT [DF_OpportunityBizDocs_tintFinal]  DEFAULT ((0)),
	[monAmountPaid] [money] NOT NULL CONSTRAINT [DF_OpportunityBizDocs_monAmountPaid]  DEFAULT ((0)),
	[vcComments] [varchar](1000) NULL,
	[numCreatedBy] [numeric](18, 0) NULL CONSTRAINT [DF_OpportunityBizDocs_numCreatedBy]  DEFAULT ((0)),
	[dtCreatedDate] [datetime] NULL CONSTRAINT [DF_OpportunityBizDocs_numCreatedDate]  DEFAULT ((0)),
	[numModifiedBy] [numeric](18, 0) NULL CONSTRAINT [DF_OpportunityBizDocs_numModifiedBy]  DEFAULT ((0)),
	[dtModifiedDate] [datetime] NULL CONSTRAINT [DF_OpportunityBizDocs_numModifiedDate]  DEFAULT ((0)),
	[numViewedBy] [numeric](18, 0) NULL CONSTRAINT [DF_OpportunityBizDocs_numViewedBy]  DEFAULT ((0)),
	[dtViewedDate] [datetime] NULL CONSTRAINT [DF_OpportunityBizDocs_numViewedDate]  DEFAULT ((0)),
	[vcQBBizDocID] [varchar](100) NULL,
	[numApprovedBy] [numeric](18, 0) NULL,
	[dtApprovedDate] [datetime] NULL,
	[bitAuthoritativeBizDocs] [bit] NULL,
	[bitAuthoritativeBizDocs1] [bit] NULL,
 CONSTRAINT [PK_OpportunityBizDocs] PRIMARY KEY CLUSTERED 
(
	[numOppBizDocsId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[OpportunityBizDocs]  WITH NOCHECK ADD  CONSTRAINT [FK_OpportunityBizDocs_OpportunityMaster] FOREIGN KEY([numOppId])
REFERENCES [dbo].[OpportunityMaster] ([numOppId])
GO
ALTER TABLE [dbo].[OpportunityBizDocs] CHECK CONSTRAINT [FK_OpportunityBizDocs_OpportunityMaster]
GO
