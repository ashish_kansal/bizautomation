/****** Object:  Table [dbo].[Warehouses]    Script Date: 07/26/2008 17:31:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Warehouses](
	[numWareHouseID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[vcWareHouse] [varchar](200) NULL,
	[vcWStreet] [varchar](50) NULL,
	[vcWCity] [varchar](50) NULL,
	[vcWPinCode] [varchar](15) NULL,
	[numWState] [numeric](18, 0) NULL,
	[numWCountry] [numeric](18, 0) NULL,
	[numDomainID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_Warehouses] PRIMARY KEY CLUSTERED 
(
	[numWareHouseID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
