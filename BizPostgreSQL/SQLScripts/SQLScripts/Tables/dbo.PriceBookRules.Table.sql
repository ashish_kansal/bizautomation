/****** Object:  Table [dbo].[PriceBookRules]    Script Date: 07/26/2008 17:28:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PriceBookRules](
	[numPricRuleID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[vcRuleName] [varchar](100) NULL,
	[tintRuleType] [tinyint] NULL,
	[fltDedPercentage] [float] NULL,
	[decflatAmount] [decimal](18, 2) NULL,
	[bitVolDisc] [bit] NULL,
	[intQntyItems] [int] NULL,
	[decMaxDedPerAmt] [decimal](18, 2) NULL,
	[bitTillDate] [bit] NULL,
	[dtTillDate] [datetime] NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[tintRuleAppType] [tinyint] NULL,
 CONSTRAINT [PK_PriceBookRules] PRIMARY KEY CLUSTERED 
(
	[numPricRuleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
