/****** Object:  Table [dbo].[Competitor]    Script Date: 07/26/2008 17:22:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Competitor](
	[numCompetitorTcode] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numCompetitorID] [numeric](18, 0) NOT NULL,
	[vcPrice] [varchar](100) NULL,
	[numItemCode] [numeric](18, 0) NULL,
	[dtDateEntered] [datetime] NULL,
	[numDomainID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_Competitor] PRIMARY KEY CLUSTERED 
(
	[numCompetitorTcode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
