/****** Object:  Table [dbo].[General_Journal_Details]    Script Date: 07/26/2008 17:25:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[General_Journal_Details](
	[numTransactionId] [numeric](10, 0) IDENTITY(1,1) NOT NULL,
	[numJournalId] [numeric](18, 0) NULL,
	[numDebitAmt] [money] NULL,
	[numCreditAmt] [money] NULL,
	[numChartAcntId] [numeric](18, 0) NULL,
	[varDescription] [varchar](200) NULL,
	[numCustomerId] [numeric](18, 0) NULL,
	[numDomainId] [numeric](18, 0) NULL,
	[numBalance] [money] NULL,
	[bitMainDeposit] [bit] NULL,
	[bitMainCheck] [bit] NULL,
	[bitMainCashCredit] [bit] NULL,
	[numoppitemtCode] [numeric](18, 0) NULL,
	[chBizDocItems] [nchar](5) NULL,
	[vcReference] [varchar](50) NULL,
	[numPaymentMethod] [numeric](18, 0) NULL,
	[bitReconcile] [bit] NULL,
 CONSTRAINT [PK_General_Journal_Details] PRIMARY KEY CLUSTERED 
(
	[numTransactionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Project Expenses->PE   Project Time ->PT  Opportunity Expenses->OE Opportunity Time->OT' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'General_Journal_Details', @level2type=N'COLUMN',@level2name=N'chBizDocItems'
GO
ALTER TABLE [dbo].[General_Journal_Details]  WITH CHECK ADD  CONSTRAINT [FK_General_Journal_Details_General_Journal_Header] FOREIGN KEY([numJournalId])
REFERENCES [dbo].[General_Journal_Header] ([numJournal_Id])
GO
ALTER TABLE [dbo].[General_Journal_Details] CHECK CONSTRAINT [FK_General_Journal_Details_General_Journal_Header]
GO
