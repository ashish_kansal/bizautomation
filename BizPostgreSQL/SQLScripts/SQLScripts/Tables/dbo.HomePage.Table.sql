/****** Object:  Table [dbo].[HomePage]    Script Date: 07/26/2008 17:25:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HomePage](
	[numHomePageID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numRelationship] [numeric](18, 0) NULL,
	[numContactType] [numeric](18, 0) NULL,
	[vcHomePage] [varchar](1000) NULL,
	[numDomainID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_HomePage] PRIMARY KEY CLUSTERED 
(
	[numHomePageID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
