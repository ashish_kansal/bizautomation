/****** Object:  Table [dbo].[TaxDetails]    Script Date: 07/26/2008 17:30:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TaxDetails](
	[numTaxID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numCountryID] [numeric](18, 0) NULL,
	[numStateID] [numeric](18, 0) NULL,
	[decTaxPercentage] [decimal](18, 2) NOT NULL,
	[numDomainId] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
