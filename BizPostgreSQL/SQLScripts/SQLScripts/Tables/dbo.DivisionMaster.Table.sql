/****** Object:  Table [dbo].[DivisionMaster]    Script Date: 07/26/2008 17:23:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DivisionMaster](
	[numDivisionID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numCompanyID] [numeric](18, 0) NOT NULL,
	[vcDivisionName] [varchar](100) NOT NULL,
	[vcBillStreet] [varchar](50) NULL,
	[vcBillCity] [varchar](50) NULL,
	[vcBilState] [numeric](18, 0) NULL,
	[vcBillPostCode] [varchar](15) NULL CONSTRAINT [DF_DivisionMaster_vcBillPostCode]  DEFAULT (''),
	[vcBillCountry] [numeric](18, 0) NULL,
	[bitSameAddr] [bit] NULL,
	[vcShipStreet] [varchar](50) NULL,
	[vcShipCity] [varchar](50) NULL,
	[vcShipState] [numeric](18, 0) NULL,
	[vcShipPostCode] [varchar](15) NULL,
	[vcShipCountry] [numeric](18, 0) NULL,
	[numGrpId] [numeric](18, 0) NOT NULL,
	[numFollowUpStatus] [numeric](18, 0) NULL,
	[bitPublicFlag] [bit] NULL CONSTRAINT [DF_DivisionMaster_bitPublicFlag]  DEFAULT ((0)),
	[numCreatedBy] [numeric](18, 0) NULL,
	[bintCreatedDate] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[bintModifiedDate] [datetime] NULL,
	[tintCRMType] [tinyint] NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[bitLeadBoxFlg] [bit] NOT NULL CONSTRAINT [DF_DivisionMaster_bitLeadBoxFlg]  DEFAULT ((0)),
	[numTerID] [numeric](18, 0) NULL,
	[numStatusID] [numeric](18, 0) NULL,
	[bintLeadProm] [datetime] NULL,
	[bintLeadPromBy] [numeric](18, 0) NULL,
	[bintProsProm] [datetime] NULL,
	[bintProsPromBy] [numeric](18, 0) NULL,
	[numRecOwner] [numeric](18, 0) NULL,
	[tintBillingTerms] [tinyint] NULL,
	[numBillingDays] [numeric](18, 0) NULL,
	[tintInterestType] [tinyint] NULL,
	[fltInterest] [float] NULL,
	[QBCustomerID] [varchar](100) NULL,
	[vcComPhone] [varchar](50) NULL,
	[vcComFax] [varchar](50) NULL,
	[numCampaignID] [numeric](18, 0) NULL,
	[numAssignedBy] [numeric](18, 0) NULL,
	[numAssignedTo] [numeric](18, 0) NULL,
	[bitNoTax] [bit] NULL,
 CONSTRAINT [PK_DivisionMaster] PRIMARY KEY CLUSTERED 
(
	[numDivisionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
