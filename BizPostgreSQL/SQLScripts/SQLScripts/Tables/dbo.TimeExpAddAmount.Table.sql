/****** Object:  Table [dbo].[TimeExpAddAmount]    Script Date: 07/26/2008 17:31:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TimeExpAddAmount](
	[numAddHDRID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numAmtCategory] [numeric](18, 0) NULL,
	[monAmount] [money] NULL,
	[dtAmtAdded] [datetime] NULL,
	[numUserCntID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_TimeExpAddAmount] PRIMARY KEY CLUSTERED 
(
	[numAddHDRID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
