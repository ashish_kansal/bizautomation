/****** Object:  Table [dbo].[CustRptScheduler]    Script Date: 07/26/2008 17:23:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CustRptScheduler](
	[numScheduleId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[varScheduleName] [varchar](50) NULL,
	[chrIntervalType] [char](1) NULL,
	[tintIntervalDays] [smallint] NULL,
	[tintMonthlyType] [smallint] NULL,
	[tintFirstDet] [smallint] NULL,
	[tintWeekDays] [smallint] NULL,
	[tintMonths] [smallint] NULL,
	[dtStartDate] [datetime] NULL,
	[dtEndDate] [datetime] NULL,
	[bitEndType] [bit] NULL,
	[bitRecurring] [bit] NULL,
	[bitEndTransactionType] [bit] NULL,
	[numNoTransaction] [numeric](18, 0) NULL,
	[numDomainId] [numeric](18, 0) NULL,
	[numReportId] [numeric](18, 0) NULL,
	[LastRecurringDate] [datetime] NULL,
	[numNoTransactionCmp] [numeric](18, 0) NULL,
 CONSTRAINT [PK_CustRptScheduler] PRIMARY KEY CLUSTERED 
(
	[numScheduleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'D-Daily,M-Monthly,Y-Yearly' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CustRptScheduler', @level2type=N'COLUMN',@level2name=N'chrIntervalType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TextBox Days values' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CustRptScheduler', @level2type=N'COLUMN',@level2name=N'tintIntervalDays'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Monthly Radio Button' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CustRptScheduler', @level2type=N'COLUMN',@level2name=N'tintMonthlyType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1st+First+1st' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CustRptScheduler', @level2type=N'COLUMN',@level2name=N'tintFirstDet'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Sunday,Monday....saturday' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CustRptScheduler', @level2type=N'COLUMN',@level2name=N'tintWeekDays'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Jan,feb....' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CustRptScheduler', @level2type=N'COLUMN',@level2name=N'tintMonths'
GO
