-- Stored procedure definition script USP_ShippingPackageType_GetByShippingCompany for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ShippingPackageType_GetByShippingCompany(v_numShippingCompanyID NUMERIC(18,0)
	,v_bitEndicia BOOLEAN,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		ID
		,numNsoftwarePackageTypeID
		,vcPackageName
   FROM
   ShippingPackageType
   WHERE
		(numShippingCompanyID = coalesce(v_numShippingCompanyID,0) OR v_numShippingCompanyID = 0)
   AND 1 =(CASE
   WHEN (numShippingCompanyID = 88 OR numShippingCompanyID = 91) THEN 1
   ELSE(CASE WHEN coalesce(bitEndicia,false) = v_bitEndicia THEN 1 ELSE 0 END)
   END);
END; $$;













