-- Stored procedure definition script USP_GetContractList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetContractList(v_numUserCntID NUMERIC(9,0) DEFAULT 0,                                                                                          
 v_numDomainID NUMERIC(9,0) DEFAULT 0,                                                                                          
 v_tintSortOrder Integer DEFAULT 4,                                                                                          
 v_SortChar CHAR(1) DEFAULT '0',                                                                                                                                            
 v_CurrentPage INTEGER DEFAULT NULL,                                                                                        
v_PageSize INTEGER DEFAULT NULL,                                                                                        
INOUT v_TotRecs INTEGER  DEFAULT NULL,                                                                                        
v_columnName VARCHAR(50) DEFAULT NULL,                                                                                        
v_columnSortOrder VARCHAR(10) DEFAULT 'desc'                                                                             ,        
 v_ClientTimeZoneOffset INTEGER DEFAULT NULL   ,      
v_numDivisionID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSql  VARCHAR(8000);                                                                
   v_firstRec  INTEGER;                                                          
   v_lastRec  INTEGER;
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   drop table IF EXISTS tt_TEMPTABLE CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLE 
   ( 
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numContractID NUMERIC(18,0)
   );                      
                                                                      
   v_strSql := 'select                                                   
c.numContractId 
from ContractManagement c                                                      
join DivisionMaster d on c.numDivisionID = d.numDivisionID                                    
join CompanyInfo com on d.numCompanyID = com.numCompanyId                                    
where          
c.numDomainID =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(100)),1,100) || '                    
';                    
   if v_SortChar <> '0' then 
      v_strSql := coalesce(v_strSql,'') || ' And vcContractName ilike ''' || coalesce(v_SortChar,'') || '%''';
   end if;                 
   if v_tintSortOrder = 1 then 
      v_strSql := coalesce(v_strSql,'') || 'and c.numContractId in                
 (select distinct(numcontractId) from TimeAndExpense Te            
  where numcaseid <> 0 and numDomainId =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(100)),1,100) || ')';
   end if;           
   if v_tintSortOrder = 2 then 
      v_strSql := coalesce(v_strSql,'') || 'and c.numContractId in              
(select distinct(numcontractId) from TimeAndExpense where numOppid <> 0 and   
 numDomainId =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(100)),1,100) || ')';
   end if;           
   if v_tintSortOrder = 3 then 
      v_strSql := coalesce(v_strSql,'') || 'and c.numContractId in                
 (select distinct(numcontractId) from TimeAndExpense where numProid <> 0   
 and numDomainId =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(100)),1,100) || ')';
   end if;
   if v_tintSortOrder = 4 then 
      v_strSql := coalesce(v_strSql,'') || 'and c.bitDays = true and c.bintExpDate > LOCALTIMESTAMP
 and c.numDomainID =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(100)),1,100) || ' ';
   end if;
  
   if v_numDivisionID <> 0 then 
      v_strSql := coalesce(v_strSql,'') || 'and c.numDivisionID =' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(100)),1,100);
   end if;      
   v_strSql := coalesce(v_strSql,'') || 'ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');                       
                    
   RAISE NOTICE '%',v_strSql;                          
   EXECUTE 'insert into tt_TEMPTABLE(numContractID)                                                    
' || v_strSql;                    
                    
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                                                      
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                                                                       
   select count(*) INTO v_TotRecs from tt_TEMPTABLE;                     
                    
   open SWV_RefCur for
   select
   TO_CHAR(c.bintExpDate,'dd-mm-yyyy') AS bintExpDate,
c.numContractId,
c.numDivisionID,
vcContractName,
case when bitdays = true then
      coalesce(CAST(case when DATE_PART('day',TIMEZONE('UTC',now()):: timestamp -bintStartdate:: timestamp) >= 0 then DATE_PART('day',TIMEZONE('UTC',now()):: timestamp -bintStartdate:: timestamp) else 0 end
      AS VARCHAR(100)),
      cast(0 as TEXT)) || ' of ' || coalesce(CAST(DATE_PART('day',bintExpDate:: timestamp -bintStartDate:: timestamp) AS VARCHAR(100)),cast(0 as TEXT))
   else '-' end as Days,
case when bitincidents = true then
      coalesce(CAST(GetIncidents(c.numContractId,v_numDomainID,c.numDivisionID) AS VARCHAR(100)),cast(0 as TEXT)) || ' of ' || coalesce(CAST(numincidents AS VARCHAR(100)),cast(0 as TEXT))
   else '-' end as Incidents,
case when bitamount = true then
      coalesce(CAST(GetContractRemainingAmount(c.numContractId,c.numDivisionID,v_numDomainID) AS VARCHAR(100)),cast(0 as TEXT)) || ' of ' || CAST(CAST(c.numAmount+(Select coalesce(sum(monAmount),0) From OpportunityBizDocsDetails Where
         numContractId = c.numContractId) AS DECIMAL(10,2)) AS VARCHAR(100))
   else '-' end  as Amount,
case when bithour = true then
      coalesce(CAST(GetContractRemainingHours(c.numContractId,c.numDivisionID,v_numDomainID) AS VARCHAR(100)),cast(0 as TEXT)) || ' of ' || CAST(c.numHours AS VARCHAR(100))
   else '-' end as Hours,
FormatedDateFromDate(c.bintcreatedOn+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),v_numDomainID)
   as bintCreatedOn ,
com.vcCompanyName
   from ContractManagement c
   join DivisionMaster d on c.numDivisionID = d.numDivisionID
   join CompanyInfo com on d.numCompanyID = com.numCompanyId
   join tt_TEMPTABLE T on T.numContractId = c.numContractId
   WHERE ID > v_firstRec and ID < v_lastRec
   order by ID;                                                     
                                                                    

/****** Object:  StoredProcedure [dbo].[USP_GetContractListForBizDocs]    Script Date: 07/26/2008 16:16:58 ******/
   RETURN;
END; $$;


