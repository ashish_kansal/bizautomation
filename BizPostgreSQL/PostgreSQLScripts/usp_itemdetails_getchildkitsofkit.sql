-- Stored procedure definition script USP_ItemDetails_GetChildKitsOfKit for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_ItemDetails_GetChildKitsOfKit(v_numDomainID NUMERIC(18,0),  
	v_numItemCode NUMERIC(18,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
  
	-- SET NOCOUNT ON added to prevent extra result sets from  
	-- interfering with SELECT statements.  
	
   open SWV_RefCur for
   SELECT
		coalesce(vcItemName,'') AS "vcItemName"
		,coalesce(txtItemDesc,'') AS "txtItemDesc"
		,coalesce(monListPrice,0) AS "monListPrice"
		,coalesce((SELECT  vcPathForTImage FROM ItemImages WHERE numItemCode = v_numItemCode AND bitDefault = true AND bitIsImage = true LIMIT 1),'') AS "vcPathForTImage"
		,coalesce(bitCalAmtBasedonDepItems,false) AS "bitCalAmtBasedonDepItems"
		,coalesce(D.vcCurrency,'') as "vcCurrency"
		,COALESCE(bitPreventOrphanedParents,false) as "bitPreventOrphanedParents"
   FROM
   Item
   INNER JOIN
   Domain D
   ON
   Item.numDomainID = D.numDomainId
   WHERE
   Item.numDomainID = v_numDomainID
   AND Item.numItemCode = v_numItemCode;


   open SWV_RefCur2 for
   SELECT
   Item.numItemCode AS "numItemCode"
		,Item.vcItemName AS "vcItemName"
		,coalesce(Item.bitKitSingleSelect,false) AS "bitKitSingleSelect"
		,coalesce(ItemDetails.tintView,1) AS "tintView"
		,coalesce((SELECT string_agg(numSecondaryListID::VARCHAR,',') FROM (SELECT numSecondaryListID
      FROM
      FieldRelationship FR
      WHERE
      FR.numDomainID = v_numDomainID
      AND FR.numModuleID = 14
      AND numPrimaryListID = Item.numItemCode
      AND numSecondaryListID IN(SELECT numChildItemID FROM ItemDetails WHERE numItemKitID = v_numItemCode)
      GROUP BY
      numSecondaryListID) X),'') AS "vcDependedKits"
		,coalesce((SELECT string_agg(numPrimaryListID::VARCHAR,',') FROM (SELECT
      numPrimaryListID
      FROM
      FieldRelationship FR
      WHERE
      FR.numDomainID = v_numDomainID
      AND FR.numModuleID = 14
      AND numSecondaryListID = Item.numItemCode
      AND numPrimaryListID IN(SELECT numChildItemID FROM ItemDetails WHERE numItemKitID = v_numItemCode)
      GROUP BY
      numPrimaryListID) X),'') AS "vcParentKits"
		,coalesce(bitOrderEditable,false) AS "bitOrderEditable"
   FROM
   ItemDetails
   INNER JOIN
   Item
   ON
   ItemDetails.numChildItemID = Item.numItemCode
   AND coalesce(Item.bitKitParent,false) = true
   WHERE
   ItemDetails.numItemKitID = v_numItemCode
   ORDER BY
   coalesce(sintOrder,0);
   RETURN;
END; $$;  


