-- Stored procedure definition script USP_MarketBudgetDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_MarketBudgetDetails(v_numDomainId NUMERIC(9,0) DEFAULT 0,            
 v_numMarketId NUMERIC(9,0) DEFAULT 0,            
 v_intType INTEGER DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numItemGroupId  NUMERIC(9,0);                                
   v_vcItemGroup  VARCHAR(150);                                
   v_Count  NUMERIC(9,0);                   
   v_lstrSQL  VARCHAR(8000);               
   v_lstr  VARCHAR(8000);                   
   v_numMonth  SMALLINT;
   v_Date  TIMESTAMP;   
   v_dtFiscalStDate  TIMESTAMP;                          
   v_dtFiscalEndDate  TIMESTAMP;
BEGIN
   v_numMonth := 1;                  
   v_lstrSQL := '';               
   v_lstr := '';               
   v_Date := TIMEZONE('UTC',now())+CAST(v_intType || 'year' as interval);                 
                
   While v_numMonth <= 12 LOOP
      v_dtFiscalStDate := GetFiscalStartDate(GetFiscalyear(v_Date,v_numDomainId)::INTEGER,v_numDomainId);
      v_dtFiscalStDate := v_dtFiscalStDate+CAST(v_numMonth -1 || 'month' as interval);
      v_dtFiscalEndDate :=  GetFiscalStartDate(GetFiscalyear(v_Date,v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '1 year'+INTERVAL '-1 day';
     
      v_lstrSQL := coalesce(v_lstrSQL,'') || ' ' ||  'fn_GetMarketBudgetMonthDetail(' || SUBSTR(CAST(v_numMarketId AS VARCHAR(10)),1,10) || ',' || CAST(EXTRACT(month FROM v_dtFiscalStDate) AS VARCHAR(2)) || '::SMALLINT,' || CAST(EXTRACT(year FROM v_dtFiscalStDate) AS VARCHAR(4)) || ',' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || ',LD.numListItemID)'
      || ' as "' || CAST(EXTRACT(month FROM v_dtFiscalStDate) AS VARCHAR(2)) || '~' || CAST(EXTRACT(year FROM v_dtFiscalStDate) AS VARCHAR(4)) || '",';
      v_numMonth := v_numMonth+1;
   END LOOP;     
   v_dtFiscalStDate := GetFiscalStartDate(GetFiscalyear(v_Date,v_numDomainId)::INTEGER,v_numDomainId);                       
              
   v_lstrSQL := coalesce(v_lstrSQL,'') ||  
 --  ' fn_GetMarketBudgetMonthTotalAmt('+Convert(varchar(10),@numMarketId)+','''+convert(varchar(500),@Date)+''',LD.numListItemID,'+convert(varchar(10),@numDomainId)+') as Total' +           
 -- ',fn_GetMarketBudgetComments('+Convert(varchar(10),@numMarketId)+','+Convert(varchar(10),@numDomainId)+',LD.numListItemID) as  Comments'           
   ' (Select Sum(MBD.monAmount)  From MarketBudgetMaster MBM    
 inner join MarketBudgetDetails MBD on MBM.numMarketBudgetId=MBD.numMarketId    
 Where MBM.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || ' And MBD.numMarketId=' || SUBSTR(CAST(v_numMarketId AS VARCHAR(10)),1,10) || ' And MBD.numListItemID=LD.numListItemID  And (((MBD.tintMonth between EXTRACT(MONTH FROM ''' || SUBSTR(CAST(v_dtFiscalStDate AS VARCHAR(100)),1,100) || '''::TIMESTAMP) and 12) and MBD
.intYear=EXTRACT(YEAR FROM ''' || SUBSTR(CAST(v_dtFiscalStDate AS VARCHAR(100)),1,100) ||
   '''::TIMESTAMP)) Or ((MBD.tintMonth between 1 and EXTRACT(MONTH FROM ''' || SUBSTR(CAST(v_dtFiscalEndDate AS VARCHAR(100)),1,100) || '''::TIMESTAMP)) and MBD.intYear=EXTRACT(YEAR FROM ''' || SUBSTR(CAST(v_dtFiscalEndDate AS VARCHAR(100)),1,100) || '''::TIMESTAMP))))  as Total  
 , ( Select COALESCE(MBD.vcComments,'''') From MarketBudgetMaster MBM      
 inner join MarketBudgetDetails MBD on MBM.numMarketBudgetId=MBD.numMarketId          
 Where MBM.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || ' And MBD.numMarketId=' || SUBSTR(CAST(v_numMarketId AS VARCHAR(10)),1,10) || ' And MBD.numListItemID=LD.numListItemID    
 And MBD.tintMonth=EXTRACT(MONTH FROM ''' || SUBSTR(CAST(v_dtFiscalEndDate AS VARCHAR(100)),1,100) || '''::TIMESTAMP) And MBD.intYear=EXTRACT(YEAR FROM ''' || SUBSTR(CAST(v_dtFiscalEndDate AS VARCHAR(100)),1,100) || '''::TIMESTAMP) LIMIT 1)as  Comments';    
                        
   v_lstr  := 'Select LD.numListItemID as numListItemID,LD.vcData as vcData,LD.numDomainID as numDomainID,' || SUBSTR(CAST(v_lstrSQL AS VARCHAR(8000)),1,8000)
   || ' From ListDetails LD Where LD.numListID = 22 And LD.numDomainID =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10);                  
                 
   RAISE NOTICE '%',(v_lstr);              
   OPEN SWV_RefCur FOR EXECUTE v_lstr;
   RETURN;
END; $$;


