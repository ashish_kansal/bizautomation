-- Stored procedure definition script usp_GetItemDetailsForCase for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetItemDetailsForCase(v_numOppID NUMERIC(9,0) DEFAULT 0,
	v_numDomainID NUMERIC(9,0) DEFAULT 0   
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT     cast(OpportunityItems.numUnitHour as VARCHAR(255)),cast(OpportunityItems.monTotAmount as VARCHAR(255)), cast(Item.vcItemName as VARCHAR(255)),cast(Item.numItemCode as VARCHAR(255)), cast(OpportunityItems.monPrice as VARCHAR(255)), cast(Item.charItemType as VARCHAR(255)), cast(Item.txtItemDesc as VARCHAR(255)),
                      OpportunityMaster.vcpOppName, cast(CompanyInfo.vcCompanyName as VARCHAR(255))
   FROM Item INNER JOIN OpportunityItems ON Item.numItemCode = OpportunityItems.numItemCode INNER JOIN
   OpportunityMaster ON OpportunityItems.numOppId = OpportunityMaster.numOppId INNER JOIN
   DivisionMaster ON OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID INNER JOIN
   CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
   WHERE
   OpportunityMaster.numOppId = v_numOppID
   AND Item.numDomainID = v_numDomainID;
END; $$;












