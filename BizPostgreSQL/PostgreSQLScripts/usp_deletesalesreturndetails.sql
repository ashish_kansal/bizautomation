-- Stored procedure definition script USP_DeleteSalesReturnDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_DeleteSalesReturnDetails(v_numReturnHeaderID NUMERIC(9,0),
      v_numReturnStatus	NUMERIC(18,0),
      v_numDomainId NUMERIC(9,0),
      v_numUserCntID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintReturnType  SMALLINT;
   v_tintReceiveType  SMALLINT;
    
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   v_i  INTEGER DEFAULT 1;
   v_COUNT  INTEGER; 
   v_numTempOppID  NUMERIC(18,0);
   v_numTempOppItemID  NUMERIC(18,0);
   v_bitTempLotNo  BOOLEAN;
   v_numTempQty  INTEGER; 
   v_numTempWarehouseItemID  INTEGER;
   v_numTempWareHouseItmsDTLID  INTEGER;
	
   v_monAppliedAmount  DECIMAL(20,5);
   v_numDepositIDRef  NUMERIC(18,0);
   v_numParentID  NUMERIC(18,0);
BEGIN
   -- BEGIN TRANSACTION
select   tintReturnType, tintReceiveType INTO v_tintReturnType,v_tintReceiveType FROM ReturnHeader WHERE numReturnHeaderID = v_numReturnHeaderID;
   IF (v_tintReturnType = 1 AND v_tintReceiveType = 2) OR v_tintReturnType = 3 then
    
      IF EXISTS(SELECT numDepositId FROM DepositMaster WHERE tintDepositePage = 3 AND numReturnHeaderID = v_numReturnHeaderID AND coalesce(monAppliedAmount,0) > 0) then
		
         RAISE EXCEPTION 'CreditMemo_PAID';
         RETURN;
      end if;
   end if;
   IF (v_tintReturnType = 2 AND v_tintReceiveType = 2) OR v_tintReturnType = 3 then
    
      IF EXISTS(SELECT numReturnHeaderID FROM BillPaymentHeader WHERE numReturnHeaderID = v_numReturnHeaderID AND coalesce(monAppliedAmount,0) > 0) then
		
         RAISE EXCEPTION 'CreditMemo_PAID';
         RETURN;
      end if;
   end if;
   IF (v_tintReturnType = 1 or v_tintReturnType = 2) then
    

		--RE INSERT ENTERIES SERIAL/LOT# ENTERIES WITH ORDER IN OppWarehouseSerializedItem TABLE WHICH ARE ASSOCIATED BECAUSE NOW SERIALS ARE RETURNED TO/FROM WAREHOUSE
		--SELECT @COUNT = COUNT(*) FROM @TempOppSerial

		--WHILE @i <= @COUNT
		--BEGIN
		--	SELECT
		--		@bitTempLotNo = bitLotNo,
		--		@numTempQty = numQty,
		--		@numTempOppID = numOppId,
		--		@numTempOppItemID = numOppItemID,
		--		@numTempWareHouseItmsDTLID = numWareHouseItmsDTLID,
		--		@numTempWarehouseItemID = numWarehouseItemID
		--	FROM
		--		@TempOppSerial
		--	WHERE
		--		ID = @i


		--	IF EXISTS (SELECT * FROM OppWarehouseSerializedItem WHERE numOppID=@numTempOppID AND numOppItemID=@numTempOppItemID AND numWarehouseItmsID=@numTempWarehouseItemID AND numWarehouseItmsDTLID=@numTempWareHouseItmsDTLID)
		--	BEGIN
		--		UPDATE 
		--			OppWarehouseSerializedItem
		--		SET 
		--			numQty = ISNULL(numQty,0) + ISNULL(@numTempQty,0)
		--		WHERE
		--			numOppID=@numTempOppID 
		--			AND numOppItemID=@numTempOppItemID 
		--			AND numWarehouseItmsID=@numTempWarehouseItemID
		--			AND numWarehouseItmsDTLID=@numTempWareHouseItmsDTLID
		--	END
		--	ELSE
		--	BEGIN
		--		INSERT INTO OppWarehouseSerializedItem
		--		(
		--			numWarehouseItmsDTLID,
		--			numOppID,
		--			numOppItemID,
		--			numWarehouseItmsID,
		--			numQty
		--		)
		--		VALUES
		--		(
		--			@numTempWareHouseItmsDTLID,
		--			@numTempOppID,
		--			@numTempOppItemID,
		--			@numTempWarehouseItemID,
		--			@numTempQty
		--		)
		--	END

		--	SET @i = @i + 1
		--END
      BEGIN
         CREATE TEMP SEQUENCE tt_TempOppSerial_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMPOPPSERIAL CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPOPPSERIAL
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         numOppId NUMERIC(18,0),
         numOppItemID NUMERIC(18,0),
         numWarehouseItemID NUMERIC(18,0),
         numWarehouseItmsDTLID NUMERIC(18,0),
         numQty INTEGER,
         bitLotNo BOOLEAN
      );
      INSERT INTO tt_TEMPOPPSERIAL(numOppId,
			numOppItemID,
			numWarehouseItemID,
			numWarehouseItmsDTLID,
			numQty,
			bitLotNo)
      SELECT
      RH.numOppId,
			RI.numOppItemID,
			OWSIReturn.numWarehouseItmsID,
			OWSIReturn.numWarehouseItmsDTLID,
			OWSIReturn.numQty,
			I.bitLotNo
      FROM
      ReturnHeader RH
      INNER JOIN
      ReturnItems RI
      ON
      RH.numReturnHeaderID = RI.numReturnHeaderID
      INNER JOIN
      OppWarehouseSerializedItem OWSIReturn
      ON
      RH.numReturnHeaderID = OWSIReturn.numReturnHeaderID
      AND RI.numReturnItemID = OWSIReturn.numReturnItemID
      INNER JOIN
      OpportunityItems OI
      ON
      RI.numOppItemID = OI.numoppitemtCode
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      WHERE
      RH.numReturnHeaderID = v_numReturnHeaderID;
      IF v_tintReturnType = 1 then --SALES RETURN
		
			--CHECK IF SERIAL/LOT# ARE NOT USED IN SALES ORDER
         IF(SELECT
         COUNT(*)
         FROM
         WareHouseItmsDTL WHIDL
         INNER JOIN
         OppWarehouseSerializedItem OWSI
         ON
         WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
         AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
         INNER JOIN
         WareHouseItems
         ON
         WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
         INNER JOIN
         Item
         ON
         WareHouseItems.numItemID = Item.numItemCode
         WHERE
         numReturnHeaderID = v_numReturnHeaderID
         AND 1 =(CASE
         WHEN Item.bitLotNo = true AND coalesce(WHIDL.numQty,0) < coalesce(OWSI.numQty,0) THEN 1
         WHEN Item.bitSerialized = true AND coalesce(WHIDL.numQty,0) = 0 THEN 1
         ELSE 0
         END)) > 0 then
			
            RAISE NOTICE 'Serial_LotNo_Used';
         ELSE
				-- MAKE SERIAL QTY 0 OR DECREASE LOT QUANTITY BECAUSE IT IS USED IN ORDER 
            UPDATE WareHouseItmsDTL WHIDL
            SET numQty =(CASE WHEN Item.bitLotNo = true THEN coalesce(WHIDL.numQty,0) -coalesce(OWSI.numQty,0) ELSE 0 END)
            FROM
            OppWarehouseSerializedItem OWSI, WareHouseItems INNER JOIN Item ON WareHouseItems.numItemID = Item.numItemCode
            WHERE(WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
            AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID) AND WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID AND OWSI.numReturnHeaderID = v_numReturnHeaderID;
         end if;
      ELSEIF v_tintReturnType = 2
      then --PURCHASE RETURN
		
			-- MAKE SERIAL QTY 1 OR INCREASE LOT QUANTITY BECAUSE IT IS RETURNED TO WAREHOUSE
         UPDATE WareHouseItmsDTL WHIDL
         SET numQty =(CASE WHEN Item.bitLotNo = true THEN coalesce(WHIDL.numQty,0)+coalesce(OWSI.numQty,0) ELSE 1 END)
         FROM
         OppWarehouseSerializedItem OWSI, WareHouseItems INNER JOIN Item ON WareHouseItems.numItemID = Item.numItemCode
         WHERE(WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
         AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID) AND WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID AND OWSI.numReturnHeaderID = v_numReturnHeaderID;
      end if;
   end if;
   IF v_tintReturnType = 1 OR v_tintReturnType = 2 then
    
      PERFORM usp_ManageRMAInventory(v_numReturnHeaderID,v_numDomainId,v_numUserCntID,2::SMALLINT);
   end if;
   UPDATE
   ReturnHeader
   SET
   numReturnStatus = v_numReturnStatus,numBizdocTempID = CASE WHEN v_tintReturnType = 1 OR v_tintReturnType = 2 THEN NULL ELSE numBizdocTempID END,
   vcBizDocName = CASE WHEN v_tintReturnType = 1 OR v_tintReturnType = 2 THEN '' ELSE vcBizDocName END,IsCreateRefundReceipt = false,tintReceiveType = 0,monBizDocAmount = 0, 
   numItemCode = 0
   WHERE
   numReturnHeaderID = v_numReturnHeaderID
   AND	numDomainID = v_numDomainId;
   IF (v_tintReturnType = 1 AND v_tintReceiveType = 2) OR v_tintReturnType = 3 then
     
      DELETE FROM DepositMaster WHERE tintDepositePage = 3 AND numReturnHeaderID = v_numReturnHeaderID;
   end if;
   IF (v_tintReturnType = 2 AND v_tintReceiveType = 2) OR v_tintReturnType = 3 then
     
      DELETE FROM BillPaymentHeader WHERE numReturnHeaderID = v_numReturnHeaderID;
   end if;
   RAISE NOTICE '%',v_tintReceiveType;
   RAISE NOTICE '%',v_tintReturnType;
   IF v_tintReceiveType = 1 AND v_tintReturnType = 4 then
      select   coalesce(numDepositIDRef,0), coalesce(numParentID,0) INTO v_numDepositIDRef,v_numParentID FROM ReturnHeader AS RH WHERE RH.numReturnHeaderID = v_numReturnHeaderID;
      select   coalesce(RH.monAmount,0) INTO v_monAppliedAmount FROM
      ReturnHeader AS RH WHERE
      RH.numReturnHeaderID = v_numReturnHeaderID
      AND RH.numDomainID = v_numDomainId;
      IF v_numDepositIDRef > 0 then
		
         UPDATE
         DepositMaster
         SET
         monAppliedAmount = coalesce(monAppliedAmount,0) -v_monAppliedAmount,monRefundAmount = coalesce(monRefundAmount,0)+coalesce(monDepositAmount,0),numReturnHeaderID =(CASE WHEN coalesce(v_numParentID,0) > 0 THEN v_numParentID ELSE NULL END)
         WHERE
         numDepositId = v_numDepositIDRef
         AND numDomainId = v_numDomainId;
      end if;
   end if;
   DELETE FROM CheckHeader WHERE CheckHeader.numReferenceID = v_numReturnHeaderID;
   DELETE FROM
   General_Journal_Details
   WHERE
   numJournalId IN(SELECT numJOurnal_Id FROM General_Journal_Header WHERE numReturnID = v_numReturnHeaderID AND General_Journal_Header.numDomainId = v_numDomainId)
   AND General_Journal_Details.numDomainId = v_numDomainId;
   DELETE FROM General_Journal_Header WHERE numReturnID = v_numReturnHeaderID AND numDomainId = v_numDomainId;
   -- COMMIT
EXCEPTION WHEN OTHERS THEN
       GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
END; $$;
--Created by chintan
--EXEC [dbo].USP_GetAccountTypes 72,0,0


