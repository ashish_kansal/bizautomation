-- Stored procedure definition script USP_GetBroadcastingLink for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetBroadcastingLink(v_numBroadcastId NUMERIC(18,0),
  v_numLinkId NUMERIC(18,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(select * from BroadcastingLink where numLinkId = v_numLinkId) then
   
      open SWV_RefCur for
      select * from BroadcastingLink where numLinkId = v_numLinkId;
   ELSE
      open SWV_RefCur for
      select * from BroadcastingLink where numBroadcastID = v_numBroadcastId;
   end if;
/****** Object:  StoredProcedure [dbo].[USP_GetCategoriesAndItems]    Script Date: 07/26/2008 16:16:31 ******/
   RETURN;
END; $$;


