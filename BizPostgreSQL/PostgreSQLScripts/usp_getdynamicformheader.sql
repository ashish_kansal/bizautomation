-- Stored procedure definition script usp_GetDynamicFormHeader for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetDynamicFormHeader(v_numFormId NUMERIC(9,0),    
 v_numDomainID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT 
		vcFormName AS "vcFormName"
		,coalesce(vcAdditionalParam,'') AS "vcAdditionalParam"
		,coalesce(numGrpId,0) as "numGrpId"
		,coalesce(ntxtHeader,'') as "ntxtHeader"
		,coalesce(ntxtFooter,'') as "ntxtFooter"
		,coalesce(ntxtLeft,'') as "ntxtLeft"
		,coalesce(ntxtRight,'') as "ntxtRight"
		,coalesce(ntxtStyle,'') AS "ntxtStyle"
	FROM dynamicFormMaster DM
	left join DynamicFormMasterParam DMP on
	DMP.numFormID = DM.numFormID  and numDomainID = v_numDomainID
	WHERE DM.numFormID = v_numFormId;
END; $$;












