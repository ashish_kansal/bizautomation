-- Function definition script fn_GetExpenseDtlsbyProStgID for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetExpenseDtlsbyProStgID(v_numProID NUMERIC,
                                                        v_numProStageID NUMERIC,
                                                        v_tintType SMALLINT)
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcRetValue  VARCHAR(100);
BEGIN
   v_vcRetValue := CAST(0 AS VARCHAR(100)); 
     
--Expenses =[ sum(item*rate) of PO linked with stage + Bills added to stage]

   IF v_numProID > 0 then
	
	
						--AND PO.numStageID = @numProStageID
--                        AND COA.[numAccountId] IS NOT NULL
      select   CAST(coalesce(SUM(X.ExpenseAmount),0) AS DECIMAL(10,2)) INTO v_vcRetValue FROM(SELECT    SUM(OPP.monPrice*OPP.numUnitHour) AS ExpenseAmount
         FROM      OpportunityMaster OM
         JOIN OpportunityItems OPP ON OM.numOppId = OPP.numOppId
         LEFT OUTER JOIN Item I ON OPP.numItemCode = I.numItemCode
         LEFT OUTER JOIN Chart_Of_Accounts COA ON I.numCOGsChartAcntId = COA.numAccountId
         WHERE     OPP.numProjectID = v_numProID
         AND (OPP.numProjectStageID = v_numProStageID OR v_numProStageID = 0)
						--AND PO.numStageID = @numProStageID
--                        AND OB.[bitAuthoritativeBizDocs] = 1
         AND OM.tintopptype = 2
--                        AND I.[charItemType] = 'P'
--                        AND COA.[numAccountId] IS NOT NULL
         UNION
-- Bills expense
         SELECT    coalesce(SUM(OBD.monAmount),0) AS ExpenseAmount
         FROM      ProjectsOpportunities PO
         LEFT OUTER JOIN OpportunityBizDocsDetails OBD ON OBD.numBizDocsPaymentDetId = PO.numBillId
         LEFT OUTER JOIN Chart_Of_Accounts COA ON OBD.numExpenseAccount = COA.numAccountId
         WHERE     PO.numProId = v_numProID
         AND (PO.numStageID = v_numProStageID OR v_numProStageID = 0)
         and coalesce(PO.numBillId,0) > 0) X;
   end if;

            

    
   RETURN v_vcRetValue;
END; $$;

