-- Stored procedure definition script USP_GetTopicMessage for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetTopicMessage(v_numTopicId NUMERIC(18,0),
	v_numDomainID NUMERIC(18,0),
	v_numMessageId NUMERIC(18,0),
	v_bitExternalUser BOOLEAN DEFAULT false, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_sqlQuery  TEXT DEFAULT '';
BEGIN
   v_sqlQuery := 'SELECT numMessageId AS "numMessageId", 
				mm.numTopicId AS "numTopicId", 
				numParentMessageId AS "numParentMessageId", 
				mm.intRecordType AS "intRecordType", 
				mm.numRecordId AS "numRecordId", 
				vcMessage AS "vcMessage", 
				mm.numCreatedBy AS "numCreatedBy", 
				mm.dtmCreatedOn AS "dtmCreatedOn", 
				mm.numUpdatedBy AS "numUpdatedBy", 
				mm.dtmUpdatedOn AS "dtmUpdatedOn", 
				mm.bitIsInternal AS "bitIsInternal", 
				mm.numDomainId AS "numDomainId",
				(select Count(*) FROM MessageMaster where numParentMessageId = mm.numMessageId) AS "MessageCount",
				fn_GetContactName(mm.numCreatedBy) AS "CreatedBy",
				fn_GetContactName(mm.numUpdatedBy) AS "UpdatedBy",
				C.vcCompanyName AS "CreatedByOrganization",
				CUP.vcCompanyName AS "UpdatedByOrganization",
				coalesce(LI.vcData,''-'') AS "CreatedByDesignation",
				coalesce(LIUP.vcData,''-'') AS "UpdatedByDesignation",
				(select
				   distinct  
					COALESCE((
						select string_agg(TM.vcAttachmentName || ''$@:#'' || CAST(TM.numAttachmentId AS VARCHAR(100)),'','' order by TM.numAttachmentId desc)
						from TopicMessageAttachments TM
						where TM.vcAttachmentName = vcAttachmentName AND numMessageId = mm.numMessageId
					),'''') as "userlist"
				from TopicMessageAttachments WHERE numMessageId = mm.numMessageId
				group by vcAttachmentName) AS vcAttachmentName,
				C.vcCompanyName AS CreatedByOrgName,
				ADC.vcEmail AS CreatedByEmail,
				TP.dtmCreatedOn AS TopicPostedDate,
				fn_GetContactName(TP.numCreateBy) AS TopicCreatedBy,
				TPC.vcCompanyName AS TopicOrgName,
				TPLI.vcData AS TopicCreatedByDesignation,
				TP.vcTopicTitle
				From MessageMaster As mm
				LEFT JOIN AdditionalContactsInformation AS ADC ON ADC.numContactId = mm.numCreatedBy
				LEFT JOIN AdditionalContactsInformation AS ADCUP ON ADCUP.numContactId = mm.numUpdatedBy
				LEFT JOIN DivisionMaster AS Div ON Div.numDivisionID = ADC.numDivisionId
				LEFT JOIN DivisionMaster AS DivUP ON DivUP.numDivisionID = ADCUP.numDivisionId
				LEFT JOIN CompanyInfo as C on Div.numCompanyID = C.numCompanyId
				LEFT JOIN CompanyInfo as CUP on DivUP.numCompanyID = CUP.numCompanyId
				LEFT JOIN ListDetails AS LI ON LI.numListItemID = ADC.vcPosition
				LEFT JOIN ListDetails AS LIUP ON LIUP.numListItemID = ADCUP.vcPosition
				LEFT JOIN TopicMaster AS TP ON mm.numTopicId = TP.numTopicId
				LEFT JOIN AdditionalContactsInformation AS TPADC ON TPADC.numContactId = TP.numCreateBy
				LEFT JOIN DivisionMaster AS TPDiv ON TPDiv.numDivisionID = TPADC.numDivisionId
				LEFT JOIN CompanyInfo as TPC on TPDiv.numCompanyID = TPC.numCompanyId
				LEFT JOIN ListDetails AS TPLI ON TPLI.numListItemID = TPADC.vcPosition ';
   v_sqlQuery := coalesce(v_sqlQuery,'') || ' WHERE mm.numDomainId = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(200)),1,200) || '	 ';
   IF(v_numMessageId = 0) then
	
      v_sqlQuery := coalesce(v_sqlQuery,'') || ' AND mm.numTopicId =' || SUBSTR(CAST(v_numTopicId AS VARCHAR(200)),1,200) || ' ';
   ELSEIF (v_numMessageId > 0)
   then
	
      v_sqlQuery := coalesce(v_sqlQuery,'') || ' AND mm.numMessageId =' || SUBSTR(CAST(v_numMessageId AS VARCHAR(200)),1,200) || ' ';
   end if;
	
   IF(v_bitExternalUser = true) then
	
      v_sqlQuery := coalesce(v_sqlQuery,'') || ' AND coalesce(mm.bitIsInternal,false) = false ';
   end if;
   v_sqlQuery := coalesce(v_sqlQuery,'') || ' ORDER BY dtmCreatedOn desc ';	
   RAISE NOTICE '%',v_sqlQuery;
   OPEN SWV_RefCur FOR EXECUTE v_sqlQuery;
   RETURN;
END; $$;



