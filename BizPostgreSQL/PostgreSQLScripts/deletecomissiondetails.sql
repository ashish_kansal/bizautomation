-- Stored procedure definition script DeleteComissionDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION DeleteComissionDetails(v_numDomainId NUMERIC,
      v_numOppBizDocID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
AS $$
	DECLARE
	my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
	BEGIN
		DELETE FROM General_Journal_Details USING BizDocComission where General_Journal_Details.numCommissionID = BizDocComission.numComissionID AND BizDocComission.numOppBizDocId = v_numOppBizDocID;
		DELETE FROM General_Journal_Header USING General_Journal_Details INNER JOIN BizDocComission ON General_Journal_Details.numCommissionID = BizDocComission.numComissionID WHERE  BizDocComission.numOppBizDocId = v_numOppBizDocID AND General_Journal_Header.numJOurnal_Id = General_Journal_Details.numJournalId;
		DELETE FROM BizDocComission WHERE numOppBizDocId = v_numOppBizDocID;
      -- COMMIT
	EXCEPTION WHEN OTHERS THEN
		GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

		raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;

   RETURN;
END; $$;
-- SELECT dbo.GetPriceBasedOnPriceBook(366,100,1)


