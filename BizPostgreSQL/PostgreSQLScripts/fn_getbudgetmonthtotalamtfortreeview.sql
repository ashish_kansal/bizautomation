-- Function definition script fn_GetBudgetMonthTotalAmtForTreeView for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetBudgetMonthTotalAmtForTreeView(v_numBudgetId NUMERIC(9,0),v_Date TIMESTAMP,v_Id NUMERIC(9,0) DEFAULT 0,v_sintByte SMALLINT DEFAULT NULL,v_numDomainId NUMERIC(9,0) DEFAULT NULL)
RETURNS DECIMAL(20,5) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_monAmount  DECIMAL(20,5);                  
   v_montotalAmt  DECIMAL(20,5);                 
   v_numMonth  SMALLINT;
BEGIN
   v_numMonth := EXTRACT(month FROM TIMEZONE('UTC',now()));               
   v_montotalAmt := 0;      
   v_monAmount := 0;         
    
   If v_sintByte = 0 then
      select   sum(coalesce(monAmount,0)) INTO v_monAmount From OperationBudgetDetails Where  numChartAcntId = v_Id And numBudgetID = v_numBudgetId And (intYear = EXTRACT(year FROM v_Date) Or intYear = EXTRACT(year FROM v_Date)+1);
   ELSEIF v_sintByte = 1
   then
      select   sum(coalesce(monAmount,0)) INTO v_monAmount From MarketBudgetDetails Where  numListItemID = v_Id And numMarketId = v_numBudgetId  And (intYear = EXTRACT(year FROM v_Date) Or intYear = EXTRACT(year FROM v_Date)+1);
   ELSEIF v_sintByte = 2
   then
      select   sum(coalesce(monAmount,0)) INTO v_monAmount From ProcurementBudgetDetails Where  numItemGroupId = v_Id And numProcurementId = v_numBudgetId  And (intYear = EXTRACT(year FROM v_Date) Or intYear = EXTRACT(year FROM v_Date)+1);
   end if;              
    
                        
   Return v_monAmount;
END; $$;

