-- Stored procedure definition script USP_BizAPI_GetItemCustomFieldsValue for PostgreSQL
CREATE OR REPLACE FUNCTION USP_BizAPI_GetItemCustomFieldsValue(v_numDomainID NUMERIC(18,0),
	v_numItemCode NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	
	open SWV_RefCur for SELECT
		CFW_Fld_Master.Fld_id AS Id,
		CFW_Fld_Master.FLd_label AS Name,
		CFW_FLD_Values_Item.Fld_Value AS ValueId,
		 GetCustFldValueItem(CFW_Fld_Master.Fld_id,Item.numItemCode) AS Value
   FROM
   Item
   INNER JOIN
   CFW_FLD_Values_Item
   ON
   Item.numItemCode = CFW_FLD_Values_Item.RecId
   INNER JOIN
   CFW_Fld_Master
   ON
   CFW_FLD_Values_Item.Fld_ID = CFW_Fld_Master.Fld_id
   WHERE
   Item.numDomainID = v_numDomainID
   AND Item.numItemCode = v_numItemCode;
END; $$;













