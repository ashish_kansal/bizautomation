-- Stored procedure definition script USP_ReportListMaster_Top10LeadSource for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ReportListMaster_Top10LeadSource(v_numDomainID NUMERIC(18,0)
	,v_ClientTimeZoneOffset INTEGER
	,v_vcTimeLine VARCHAR(50)
	,v_vcGroupBy VARCHAR(50)
	,v_vcTeritorry TEXT
	,v_vcFilterBy SMALLINT  -- 1:Assign TO, 2: Record Owner, 3: Teams (Based on Assigned To) 
	,v_vcFilterValue TEXT
	,v_dtFromDate DATE
	,v_dtToDate DATE
	,v_numRecordCount INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_TotalLeads  NUMERIC(18,2);
BEGIN
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      numDivisionID NUMERIC(18,0),
      RowID NUMERIC(18,0)
   );
   INSERT INTO tt_TEMP(numDivisionID
		,RowID)
   SELECT
   numDivisionID
		,MIN(ID)
   FROM
   DivisionMasterPromotionHistory
   WHERE
   numDomainID = v_numDomainID
   GROUP BY
   numDivisionID;


   select   COUNT(DM.numDivisionID) INTO v_TotalLeads FROM
   DivisionMaster DM
   INNER JOIN
   CompanyInfo CI
   ON
   DM.numCompanyID = CI.numCompanyId
   LEFT JOIN
   Listdetails LD
   ON
   LD.numListID = 18
   AND CI.vcHow = LD.numListItemID
   AND (LD.constFlag = true OR LD.numDomainid = v_numDomainID)
   LEFT JOIN
   tt_TEMP T1
   ON
   DM.numDivisionID = T1.numDivisionID
   LEFT JOIN
   DivisionMasterPromotionHistory DMPH
   ON
   T1.RowID = DMPH.ID WHERE
   DM.numDomainID = v_numDomainID
   AND ((coalesce(DM.tintCRMType,0) = 0 AND DMPH.ID IS NULL) OR coalesce(DMPH.tintPreviousCRMType,0) = 0);

   open SWV_RefCur for SELECT cast(LD.vcData as VARCHAR(255))
		,CAST((COUNT(*)*100)/v_TotalLeads AS NUMERIC(18,2)) AS LeadSourcePercent
   FROM
   DivisionMaster DM
   INNER JOIN
   CompanyInfo CI
   ON
   DM.numCompanyID = CI.numCompanyId
   LEFT JOIN
   Listdetails LD
   ON
   LD.numListID = 18
   AND CI.vcHow = LD.numListItemID
   AND (LD.constFlag = true OR LD.numDomainid = v_numDomainID)
   LEFT JOIN
   tt_TEMP T1
   ON
   DM.numDivisionID = T1.numDivisionID
   LEFT JOIN
   DivisionMasterPromotionHistory DMPH
   ON
   T1.RowID = DMPH.ID
   WHERE
   DM.numDomainID = v_numDomainID
   AND ((coalesce(DM.tintCRMType,0) = 0 AND DMPH.ID IS NULL) OR coalesce(DMPH.tintPreviousCRMType,0) = 0)
   AND LD.numListItemID IS NOT NULL
   GROUP BY
   LD.numListItemID,LD.vcData
   ORDER BY(COUNT(*)*100)/v_TotalLeads DESC;
END; $$;












