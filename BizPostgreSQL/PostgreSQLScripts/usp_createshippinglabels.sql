-- Stored procedure definition script USP_CreateShippingLabels for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
Create or replace FUNCTION USP_CreateShippingLabels(v_numUserCntId NUMERIC(9,0),
                v_strItems     TEXT  DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDocItem  INTEGER;
BEGIN
   SELECT * INTO v_hDocItem FROM SWF_Xml_PrepareDocument(v_strItems);
    
   INSERT INTO ShippingLabel(ShippingReportItemId,
                vcShippingLabelImage,
                vcTrackingNumber,
                dtCreateDate,
                numCreatedBy)
   SELECT X.ShippingReportItemId,
           X.vcShippingLabelImage,
           X.vcTrackingNumber,
           TIMEZONE('UTC',now()),--X.[dtCreateDate],
           v_numUserCntId
   FROM(SELECT * FROM   SWF_OpenXml(v_hDocItem,'/NewDataSet/Items','ShippingReportItemId |vcShippingLabelImage |vcTrackingNumber') SWA_OpenXml(ShippingReportItemId NUMERIC(18,0),vcShippingLabelImage VARCHAR(100),vcTrackingNumber VARCHAR(50))) X;
   PERFORM SWF_Xml_RemoveDocument(v_hDocItem);
   RETURN;
END; $$;


