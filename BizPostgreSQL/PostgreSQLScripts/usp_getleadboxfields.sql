-- Stored procedure definition script USP_GetLeadboxFields for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetLeadboxFields(v_numDomainID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	select 
		vcFieldName as vcFormFieldname
		,CONCAT(vcFieldType,'~',vcAssociatedControlType,'~',vcDbColumnName,'~',vcListItemType,'~',CAST(numListID AS VARCHAR(15)),'~',CAST(numFieldID AS VARCHAR(15))) as ID
   from View_DynamicDefaultColumns where numDomainID = v_numDomainID and numFormId = 3 and vcDbColumnName != 'bitSameAddr'
   union
   select CAST('Areas of Interests' AS CHAR(18)),CAST('AOI~AOI~AOI~AOI~0~0' AS VARCHAR(1))
   union
   select CAST('--Select One--' AS CHAR(14)) ,CAST('0' AS VARCHAR(1));
END; $$;












