-- Stored procedure definition script USP_OpenOrdersList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OpenOrdersList(v_numUserCntID              NUMERIC(9,0)  DEFAULT 0,
    v_numDomainID               NUMERIC(9,0)  DEFAULT 0,
    v_SortChar                  CHAR(1)  DEFAULT '0',
    v_CurrentPage               INTEGER DEFAULT NULL,
    v_PageSize                  INTEGER DEFAULT NULL,
    INOUT v_TotRecs                   INTEGER   DEFAULT NULL,
    v_columnName                VARCHAR(50) DEFAULT NULL,
    v_columnSortOrder           VARCHAR(10) DEFAULT NULL,
    v_ClientTimeZoneOffset      INTEGER DEFAULT NULL,
	v_vcRegularSearchCriteria VARCHAR(1000) DEFAULT '',
	v_vcCustomSearchCriteria VARCHAR(1000) DEFAULT '',
	v_numViewID NUMERIC(9,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_PageId  SMALLINT;
   v_numFormId  INTEGER;
  
   v_strSql  VARCHAR(8000);
   v_strFrom  VARCHAR(2000);

   v_tintOrder  SMALLINT;
   v_vcFieldName  VARCHAR(50);
   v_vcListItemType  VARCHAR(3);
   v_vcListItemType1  VARCHAR(1);
   v_vcAssociatedControlType  VARCHAR(30);
   v_numListID  NUMERIC(9,0);
   v_vcDbColumnName  VARCHAR(40);
   v_WhereCondition  VARCHAR(2000);
   v_vcLookBackTableName  VARCHAR(2000);
   v_bitCustom  BOOLEAN;
   v_numFieldId  NUMERIC;  
   v_bitAllowSorting  CHAR(1);   
   v_bitAllowEdit  CHAR(1);                 
   v_vcColumnName  VARCHAR(500);                  

   v_Nocolumns  SMALLINT;
   v_numViewDomainID  NUMERIC(9,0);
   v_numViewUserCntID  NUMERIC(9,0);
   v_ListRelID  NUMERIC(9,0); 

   v_column  VARCHAR(50);              
   v_vcStatus  TEXT;
   v_strWHERE  TEXT;
   v_numBizDocType  TEXT;
   v_strSql1  VARCHAR(8000);                                                            
   v_firstRec  INTEGER;                                                            
   v_lastRec  INTEGER;
   v_Prefix  VARCHAR(5);
   SWV_RowCount INTEGER;
BEGIN
   v_numFormId := 57;
  
   v_tintOrder := 0;
   v_WhereCondition := '';
   v_Nocolumns := 0;


   DROP TABLE IF EXISTS tt_TEMPFORM CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPFORM 
   (
      tintOrder SMALLINT,
      vcDbColumnName VARCHAR(50),
      vcOrigDbColumnName VARCHAR(50),
      vcFieldName VARCHAR(50),
      vcAssociatedControlType VARCHAR(50),
      vcListItemType VARCHAR(3),
      numListID NUMERIC(9,0),
      vcLookBackTableName VARCHAR(50),
      bitCustomField BOOLEAN,
      numFieldId NUMERIC,
      bitAllowSorting BOOLEAN,
      bitAllowEdit BOOLEAN,
      bitIsRequired BOOLEAN,
      bitIsEmail BOOLEAN,
      bitIsAlphaNumeric BOOLEAN,
      bitIsNumeric BOOLEAN,
      bitIsLengthValidation BOOLEAN,
      intMaxLength INTEGER,
      intMinLength INTEGER,
      bitFieldMessage BOOLEAN,
      vcFieldMessage VARCHAR(500),
      ListRelID NUMERIC(9,0),
      intColumnWidth INTEGER,
      bitAllowFiltering BOOLEAN,
      vcFieldDataType CHAR(1)
   );
  
   IF v_numViewID = 1 OR v_numViewID = 2 then

      v_numViewDomainID := 0;
      v_numViewUserCntID := 0;
   ELSE
      v_numViewDomainID := v_numDomainID;
      v_numViewUserCntID := v_numUserCntID;
   end if;
 
   select   coalesce(sum(TotalRow),0) INTO v_Nocolumns from(Select count(*) AS TotalRow from View_DynamicColumns where numFormId = v_numFormId and numUserCntID = v_numViewUserCntID and numDomainID = v_numViewDomainID AND tintPageType = 1 and numViewID = v_numViewID
      Union
      Select count(*) AS TotalRow from View_DynamicCustomColumns where numFormId = v_numFormId and numUserCntID = v_numViewUserCntID and numDomainID = v_numViewDomainID AND tintPageType = 1 and numViewID = v_numViewID) TotalRows;
 
   if v_Nocolumns = 0 then

      INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,tintPageType,bitCustom,intColumnWidth,numViewId)
      select v_numFormId,numFieldId,0,Row_number() over(order by tintRow desc),v_numDomainID,v_numUserCntID,1,0,intColumnWidth,v_numViewID
      FROM View_DynamicDefaultColumns
      where numFormId = v_numFormId and bitDefault = true AND coalesce(bitSettingField,false) = true and numDomainID = v_numDomainID
      order by tintOrder asc;
   end if;

    
   INSERT INTO tt_TEMPFORM
   select tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,coalesce(vcCultureFieldName,vcFieldName),
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName
,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage AS vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType
   FROM View_DynamicColumns
   where numFormId = v_numFormId and numUserCntID = v_numViewUserCntID and numDomainID = v_numViewDomainID AND tintPageType = 1
   AND coalesce(bitSettingField,false) = true AND coalesce(bitCustom,false) = false and numViewID = v_numViewID
   UNION
   select tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,
 vcAssociatedControlType,'' as vcListItemType,numListID,''
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,false,''
   from View_DynamicCustomColumns
   where numFormId = v_numFormId and numUserCntID = v_numViewUserCntID and numDomainID = v_numViewDomainID AND tintPageType = 1
   AND coalesce(bitCustom,false) = true  and numViewID = v_numViewID
   order by tintOrder asc;  



   v_strSql := '';
   v_strSql := ' select ADC.numContactId,DM.numDivisionID,coalesce(DM.numTerID,0) AS numTerID,Opp.numRecOwner as numRecOwner,DM.tintCRMType,Opp.numOppId,OBD.numOppBizDocsID';


   v_strFrom := 'FROM OpportunityMaster Opp 
							 JOIN OpportunityBizDocs OBD ON Opp.numOppId = OBD.numOppId --and (OBD.bitAuthoritativeBizDocs = 1 OR OBD.bitAuthoritativeBizDocs = 0)
						     JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId                                                               
                             JOIN DivisionMaster DM ON Opp.numDivisionId = DM.numDivisionID AND ADC.numDivisionId = DM.numDivisionID                                                               
                             JOIN CompanyInfo cmp ON DM.numCompanyID = cmp.numCompanyId 
                             left JOIN OpportunityLinking OL on OL.numChildOppID=Opp.numOppId left join WebAPI on WebAPI.WebApiId=isnull(OL.numWebApiId,0)
							left join Sites on isnull(OL.numSiteID,0)=Sites.numSIteID and Sites.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15);
							

							
   select   tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
   v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,
   v_bitAllowSorting,v_bitAllowEdit,v_ListRelID from  tt_TEMPFORM    order by tintOrder asc LIMIT 1;            

   drop table IF EXISTS tt_TEMPOPP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPOPP
   (
      numOppID NUMERIC(18,0),
      numDivisionID NUMERIC(18,0),
      tintBillToType SMALLINT,
      tintShipToType SMALLINT
   ); 

   drop table IF EXISTS tt_TEMPOPPADDRESS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPOPPADDRESS
   (
      numOppID NUMERIC(18,0),
      vcBillStreet VARCHAR(100),
      vcBillCity VARCHAR(50),
      numBillState NUMERIC(18,0),
      vcBillPostCode VARCHAR(15),
      numBillCountry NUMERIC(18,0),
      vcShipStreet VARCHAR(100),
      vcShipCity VARCHAR(50),
      numShipState NUMERIC(18,0),
      vcShipPostCode VARCHAR(15),
      numShipCountry NUMERIC(18,0) 
   ); 

   WHILE v_tintOrder > 0 LOOP
      IF v_bitCustom = false then
         IF v_vcLookBackTableName = 'AdditionalContactsInformation' then
            v_Prefix := 'ADC.';
         end if;
         IF v_vcLookBackTableName = 'DivisionMaster' then
            v_Prefix := 'DM.';
         end if;
         IF v_vcLookBackTableName = 'CompanyInfo' then
            v_Prefix := 'CMP.';
         end if;
         IF v_vcLookBackTableName = 'OpportunityMaster' then
            v_Prefix := 'Opp.';
         end if;
         IF v_vcLookBackTableName = 'OpportunityBizDocs' then
            v_Prefix := 'OBD.';
         end if;
         IF v_vcLookBackTableName = 'Item' then
            v_Prefix := 'I.';
         end if;
         IF v_vcLookBackTableName = 'OpportunityBizDocItems' then
            v_Prefix := 'OBDI.';
         end if;
         IF v_vcLookBackTableName = 'OpportunityItems' then
            v_Prefix := 'OI.';
         end if;
         IF v_vcLookBackTableName = 'WareHouseItems' then
            v_Prefix := 'WHI.';
         end if;
         IF v_vcLookBackTableName = 'Warehouses' then
            v_Prefix := 'WH.';
         end if;
         IF v_vcLookBackTableName = 'AddressDetails' then
            v_Prefix := 'AD.';
         end if;
         IF v_vcLookBackTableName = 'OpportunityLinking' then
            v_Prefix := 'OL.';
         end if;
         IF v_vcLookBackTableName = 'Item' or v_vcLookBackTableName = 'OpportunityBizDocItems' or v_vcLookBackTableName = 'OpportunityItems'	or v_vcLookBackTableName = 'Warehouses' or v_vcLookBackTableName = 'WareHouseItems' OR v_vcLookBackTableName = 'ShippingBox' then
			
            IF POSITION('Item I' IN v_strFrom) = 0 AND POSITION('OpportunityBizDocItems OBDI' IN v_strFrom) = 0 AND POSITION('OpportunityItems OI' IN v_strFrom) = 0 AND POSITION('ShippingBox SB' IN v_strFrom) = 0 then
					
               v_strFrom := coalesce(v_strFrom,'') || ' JOIN [OpportunityItems] OI ON Opp.[numOppId] = OI.[numOppId]
                      					   JOIN [OpportunityBizDocItems] OBDI ON OBDI.numOppItemID= OI.numoppitemtCode and  OBD.numOppBizDocsID=OBDI.numOppBizDocID 
										   join Item I on OI.numItemCode=I.numItemCode ';
               v_strSql := coalesce(v_strSql,'') || ',OBDI.numOppBizDocItemID';
               IF coalesce(v_numViewID,0) = 2 then
						
                  v_strSql := coalesce(v_strSql,'') || ',OI.numoppitemtCode ';
               end if;
            end if;
         end if;
--		 IF @vcLookBackTableName = 'ShippingBox'
--			BEGIN
--				IF CHARINDEX('ShippingBox SB') = 0
--				BEGIN
--					SET @strFrom = @strFrom + ' '
--				END
--			END
         IF v_vcLookBackTableName = 'Warehouses' or v_vcLookBackTableName = 'WareHouseItems' then
			
            IF POSITION('Warehouses WH' IN v_strFrom) = 0 AND POSITION('WareHouseItems WHI' IN v_strFrom) = 0 then
               v_strFrom := coalesce(v_strFrom,'') || ' LEFT JOIN WareHouseItems WHI ON OI.numWarehouseItmsID = WHI.numWareHouseItemID AND WHI.numDomainID= ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || '
											  LEFT JOIN dbo.Warehouses WH ON WHI.numDomainID = WH.numDomainID AND WHI.numWareHouseID = WH.numWareHouseID ';
            end if;
         end if;
         IF v_vcLookBackTableName = 'AddressDetails' then
			
            IF POSITION('#tempOppAddress AD' IN v_strFrom) = 0 then
				
               insert into tt_TEMPOPP
               select distinct Opp.numOppId,Opp.numDivisionId,coalesce(Opp.tintBillToType,0) as tintBillToType,coalesce(Opp.tintShipToType,0) as tintShipToType
               FROM OpportunityMaster Opp JOIN OpportunityBizDocs OBD ON Opp.numOppId = OBD.numoppid and OBD.bitAuthoritativeBizDocs = 1
							--LEFT join OpportunityLinking OL on OL.numChildOppID=Opp.numOppId left join WebAPI on WebAPI.WebApiId=isnull(OL.numWebApiId,0)
               where Opp.numDomainId = v_numDomainID;
               insert into tt_TEMPOPPADDRESS(numOppID) select numOppId from tt_TEMPOPP;

					--Billing Address from Address Detils
               Update tt_TEMPOPPADDRESS tempOppAdd set vcBillStreet = AD.vcStreet,vcBillCity = AD.vcCity,numBillState = AD.numState,
               vcBillPostCode = AD.vcPostalCode,numBillCountry = AD.numCountry from tt_TEMPOPP Opp join AddressDetails AD ON AD.numRecordID = Opp.numDivisionID and Opp.tintBillToType in(0,1)
               where Opp.numOppId = tempOppAdd.numOppID AND(AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary = true and AD.numDomainID = v_numDomainID);

					--Billing Address from OpportunityAddress
               Update tt_TEMPOPPADDRESS tempOppAdd set vcBillStreet = OppAdd.vcBillStreet,vcBillCity = OppAdd.vcBillCity,numBillState = OppAdd.numBillState,
               vcBillPostCode = OppAdd.vcBillPostCode,numBillCountry = OppAdd.numBillCountry from tt_TEMPOPP Opp join OpportunityAddress OppAdd ON OppAdd.numOppID = Opp.numOppId and Opp.tintBillToType in(2,3) WHERE Opp.numOppId = tempOppAdd.numOppID;

					--Shipping Address from Address Detils
               Update tt_TEMPOPPADDRESS tempOppAdd set vcShipStreet = AD.vcStreet,vcShipCity = AD.vcCity,numShipState = AD.numState,
               vcShipPostCode = AD.vcPostalCode,numShipCountry = AD.numCountry from tt_TEMPOPP Opp join AddressDetails AD ON AD.numRecordID = Opp.numDivisionID and Opp.tintShipToType in(0,1)
               where Opp.numOppId = tempOppAdd.numOppID AND(AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary = true and AD.numDomainID = v_numDomainID);

					--Shipping Address from OpportunityAddress
               Update tt_TEMPOPPADDRESS tempOppAdd set vcShipStreet = OppAdd.vcShipStreet,vcShipCity = OppAdd.vcShipCity,numShipState = OppAdd.numShipState,
               vcShipPostCode = OppAdd.vcShipPostCode,numShipCountry = OppAdd.numShipCountry from tt_TEMPOPP Opp join OpportunityAddress OppAdd ON OppAdd.numOppID = Opp.numOppId and Opp.tintShipToType in(2,3) WHERE Opp.numOppId = tempOppAdd.numOppID;
               v_strFrom := coalesce(v_strFrom,'') || ' LEFT JOIN #tempOppAddress AD ON AD.numOppID = Opp.numOppID ';
            end if;
         end if;
         v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || '~0';
         IF v_vcAssociatedControlType = 'SelectBox' then
            
            IF v_vcListItemType = 'LI' then
                
               v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData[' || coalesce(v_vcColumnName,'') || ']';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'S'
            then
                  
               v_strSql := coalesce(v_strSql,'') || ',S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcState[' || coalesce(v_vcColumnName,'') || ']';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left join State S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || ' on S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numStateID=' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'T'
            then
                    
               v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData[' || coalesce(v_vcColumnName,'') || ']';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID='
               || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'UOM'
            then
                  
               v_strSql := coalesce(v_strSql,'') || ',UOM' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcUnitName[' || coalesce(v_vcColumnName,'') || ']';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left join UOM UOM' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || ' on UOM' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numUOMId=OI.numUOMId';
            ELSEIF v_vcListItemType = 'U'
            then
                      
               v_strSql := coalesce(v_strSql,'') || ',dbo.fn_GetContactName(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') [' || coalesce(v_vcColumnName,'') || ']';
            ELSEIF v_vcListItemType = 'WI'
            then
                
               v_strSql := coalesce(v_strSql,'') || ', Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end [' || coalesce(v_vcColumnName,'') || ']';
            end if;
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
              
            v_strSql := coalesce(v_strSql,'')
            || ',case when convert(varchar(11),DateAdd(minute, ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ','
            || coalesce(v_Prefix,'')
            || coalesce(v_vcDbColumnName,'')
            || ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>''';
            v_strSql := coalesce(v_strSql,'')
            || 'when convert(varchar(11),DateAdd(minute, ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ','
            || coalesce(v_Prefix,'')
            || coalesce(v_vcDbColumnName,'')
            || ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>''';
            v_strSql := coalesce(v_strSql,'')
            || 'when convert(varchar(11),DateAdd(minute, ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ','
            || coalesce(v_Prefix,'')
            || coalesce(v_vcDbColumnName,'')
            || ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ';
            v_strSql := coalesce(v_strSql,'')
            || 'else dbo.FormatedDateFromDate('
            || coalesce(v_Prefix,'')
            || coalesce(v_vcDbColumnName,'')
            || ','
            || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10)
            || ') end  ['
            || coalesce(v_vcColumnName,'') || ']';
         ELSEIF v_vcAssociatedControlType = 'TextBox'
         then
                
            IF v_vcDbColumnName = 'DiscAmt' then
               v_strSql := coalesce(v_strSql,'') || ',(isnull(OBDI.monTotAmtBefDiscount,OBDI.monTotAmount)-OBDI.monTotAmount) [' || coalesce(v_vcColumnName,'') || ']';
            ELSE
               v_strSql := coalesce(v_strSql,'') || ',' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' [' || coalesce(v_vcColumnName,'') || ']';
            end if;
         ELSEIF v_vcAssociatedControlType = 'TextArea'
         then
                  
            IF v_vcDbColumnName = 'numShippingReportID' then
					
               v_strSql := coalesce(v_strSql,'') || ', (
													SELECT TOP 1 numShippingReportID FROM ShippingReport
													WHERE numOppBizDocID = OBD.numOppBizDocsID
													AND numDomainID = Opp.numDomainID
													ORDER BY numShippingReportID DESC
													) AS [' || coalesce(v_vcColumnName,'') || ']';
            ELSE
               v_strSql := coalesce(v_strSql,'') || ',' ||  coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' [' || coalesce(v_vcColumnName,'') || ']';
            end if;
         end if;
      end if;
      select   tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
      v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,
      v_bitAllowSorting,v_bitAllowEdit,v_ListRelID from tt_TEMPFORM WHERE tintOrder > v_tintOrder -1   order by tintOrder asc LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount = 0 then 
         v_tintOrder := 0;
      end if;
   END LOOP; 
  
  

   DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTABLE 
   (
      ROWNUMBER INTEGER,
      TotalRowCount INTEGER,
      numOppID        NUMERIC(9,0)
   );

   v_column := v_columnName;              

-- ADDED BY MANISH ANJARA - 15th Aug,2012
   select   coalesce(vcOrderStatus,'') INTO v_vcStatus FROM OpportunityOrderStatus WHERE numDomainID = v_numDomainID;
   IF coalesce(v_vcStatus,'') <> '' then
      v_strWHERE := ' AND ISNULL(numStatus,0) IN ( ' || coalesce(v_vcStatus,'') || ' ) ';
   ELSE
      v_strWHERE := ' AND 1=1 ';
   end if;

   select   coalesce(numBizDocType,0) INTO v_numBizDocType FROM OpportunityOrderStatus WHERE numDomainID = v_numDomainID;
   IF coalesce(v_numBizDocType,'') <> '' then
      v_strWHERE := ' AND ISNULL(numBizDocId,0) = ' || coalesce(v_numBizDocType,'');
   ELSE
      v_strWHERE := ' AND 1=1 ';
   end if;	
-----------------------------------------

   v_strSql1 := 'insert into #tempTable
SELECT ROW_NUMBER() OVER (ORDER BY ' || CASE WHEN coalesce(POSITION(substring(v_column from 'txt') IN v_column),0) > 0 THEN ' CONVERT(VARCHAR(Max),' || coalesce(v_column,'') || ')'  ELSE v_column end || ' ' || coalesce(v_columnSortOrder,'') || ') AS RowNumber,                                                             
	COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount,Opp.numOppId ' || coalesce(v_strFrom,'')  || ' where Opp.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' AND tintOppType=1 AND tintOppStatus=1 ' || coalesce(v_strWHERE,'');                                               

   IF v_vcRegularSearchCriteria <> '' then

      v_strSql1 := coalesce(v_strSql1,'') || ' and ' || coalesce(v_vcRegularSearchCriteria,'');
   end if;

   if v_SortChar <> '0' then 
      v_strSql1 := coalesce(v_strSql1,'') || ' And Opp.vcPoppName like ''' || coalesce(v_SortChar,'') || '%''';
   end if;                    

   RAISE NOTICE '%',v_strSql1;   
   EXECUTE v_strSql1; 




   DELETE FROM tt_TEMPTABLE WHERE ROWNUMBER NOT IN(SELECT Min(ROWNUMBER) FROM tt_TEMPTABLE GROUP BY numOppID);

   SELECT COUNT(* ) INTO v_TotRecs FROM   tt_TEMPTABLE;

   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                                            
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);   

   IF POSITION('OBDI.numOppBizDocItemID' IN v_strSql) = 0 then
      v_strSql := coalesce(v_strSql,'') || ',0 as numOppBizDocItemID ';
   end if;
		
   IF POSITION('OI.numoppitemtCode' IN v_strSql) = 0 then
      v_strSql := coalesce(v_strSql,'') || ',0 as numoppitemtCode ';
   end if;


   v_strSql := coalesce(v_strSql,'') || coalesce(v_strFrom,'') || coalesce(v_WhereCondition,'') || ' join #tempTable temp on temp.numOppID=Opp.numOppID  where Opp.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) ||
   'and RowNumber > ' || SUBSTR(CAST(v_firstRec AS VARCHAR(10)),1,10) || ' and RowNumber <' || SUBSTR(CAST(v_lastRec AS VARCHAR(10)),1,10) || ' ' || coalesce(v_strWHERE,'');

   IF v_vcRegularSearchCriteria <> '' then

      v_strSql := coalesce(v_strSql,'') || ' and ' || coalesce(v_vcRegularSearchCriteria,'');
   end if;
 
   v_strSql := coalesce(v_strSql,'') || ' order by RowNumber';

                                             
   RAISE NOTICE '%',v_strSql;                      
              
   OPEN SWV_RefCur FOR EXECUTE v_strSql;  

   open SWV_RefCur for
   SELECT * FROM tt_TEMPFORM;
 

   DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;


   drop table IF EXISTS tt_TEMPOPP CASCADE;
   drop table IF EXISTS tt_TEMPOPPADDRESS CASCADE;
/****** Object:  StoredProcedure [dbo].[USP_OPPBizDocItems]    Script Date: 07/26/2008 16:20:13 ******/
   RETURN;
END; $$;


