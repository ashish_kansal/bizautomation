-- Stored procedure definition script USP_UpdateProjectAccount for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateProjectAccount(v_numDomainID NUMERIC(9,0),
v_numProID NUMERIC(9,0),
v_numAccountID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE ProjectsMaster SET numAccountID = v_numAccountID WHERE numProId = v_numProID AND numdomainId = v_numDomainID;
   RETURN;
END; $$;


