-- Stored procedure definition script USP_StagePercentageDetailsTask_GetByStage for PostgreSQL
CREATE OR REPLACE FUNCTION USP_StagePercentageDetailsTask_GetByStage(v_numDomainID NUMERIC(18,0)                            
	,v_numStageDetailsId NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		numTaskId AS "numTaskId" 
		,vcTaskName AS "vcTaskName" 
   FROM
   StagePercentageDetailsTask
   WHERE
   numDomainID = v_numDomainID
   AND numStageDetailsId = v_numStageDetailsId;
END; $$;












