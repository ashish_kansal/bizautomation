-- Stored procedure definition script usp_SetAuthorizationGroupName for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_SetAuthorizationGroupName(   
--
v_numGroupID NUMERIC(9,0) DEFAULT 0,
	v_vcGroupName VARCHAR(30) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numGrpCount  INTEGER;
BEGIN
   IF v_numGroupID = 0 then
      select   COUNT(*) INTO v_numGrpCount FROM AuthenticationGroupMaster WHERE vcGroupName = v_vcGroupName;
      IF v_numGrpCount <= 0 then
				
         INSERT INTO AuthenticationGroupMaster(vcGroupName)  VALUES(v_vcGroupName);
					
         open SWV_RefCur for
         SELECT  CURRVAL('AuthenticationGroupMaster_seq');
      ELSE
         open SWV_RefCur for
         SELECT 0;
      end if;
   ELSE
      UPDATE AuthenticationGroupMaster
      SET vcGroupName = v_vcGroupName
      WHERE numGroupID = v_numGroupID;
   end if;
   RETURN;
END; $$;


