-- Stored procedure definition script USP_ManageSurveyTemplate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageSurveyTemplate(v_numDomainID NUMERIC,
    v_numSurId NUMERIC,
    v_numPageType SMALLINT, 
    v_txtTemplate TEXT,
    v_strItems      TEXT  DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numSurTemplateId  NUMERIC;

   v_hDocItem  INTEGER;
BEGIN
   IF NOT EXISTS(SELECT * FROM  SurveyTemplate WHERE   numDomainID = v_numDomainID AND numSurID = v_numSurId AND numPageType = v_numPageType) then
        
      INSERT  INTO SurveyTemplate(numDomainID,
                      numSurID,
                      txtTemplate,
					  numPageType)
            VALUES(v_numDomainID,
                      v_numSurId,
                      v_txtTemplate,
					  v_numPageType);
            
      v_numSurTemplateId := CURRVAL('SurveyTemplate_seq');
   ELSE
      UPDATE  SurveyTemplate
      SET   txtTemplate = v_txtTemplate
      WHERE   numDomainID = v_numDomainID AND numSurID = v_numSurId AND numPageType = v_numPageType;
      select   numSurTemplateId INTO v_numSurTemplateId FROM SurveyTemplate WHERE numDomainID = v_numDomainID AND numSurID = v_numSurId AND numPageType = v_numPageType;
   end if;

   DELETE FROM StyleSheetDetails
   WHERE       numSurTemplateId = v_numSurTemplateId;
   IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then

      INSERT INTO StyleSheetDetails(numSurTemplateId,
                  numCssID)
      SELECT v_numSurTemplateId,
             X.numCssID
      FROM
	  XMLTABLE
			(
				'NewDataSet/Item'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numCssID NUMERIC(18,0) PATH 'numCssID'
			) AS X;

   end if;
   RETURN;
END; $$;


