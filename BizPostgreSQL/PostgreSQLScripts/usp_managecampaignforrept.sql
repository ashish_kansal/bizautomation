-- Stored procedure definition script USP_ManageCampaignForRept for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageCampaignForRept(v_numUserCntID NUMERIC(9,0),                     
v_strTerritory VARCHAR(4000))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_separator_position  INTEGER;            
   v_strPosition  VARCHAR(1000);
BEGIN
   delete from ForReportsByCampaign where numUserCntID = v_numUserCntID;                    
             
            
   while coalesce(POSITION(substring(v_strTerritory from E'\\,') IN v_strTerritory),
   0) <> 0 LOOP -- Begin for While Loop            
      v_separator_position := coalesce(POSITION(substring(v_strTerritory from E'\\,') IN v_strTerritory),
      0);
      v_strPosition := SUBSTR(v_strTerritory,1,v_separator_position -1);
      v_strTerritory := OVERLAY(v_strTerritory placing '' from 1 for v_separator_position);
      insert into ForReportsByCampaign(numUserCntID,numCampaignID)
     values(v_numUserCntID,CAST(v_strPosition AS NUMERIC));
   END LOOP;
   RETURN;
END; $$;


