-- Stored procedure definition script USP_GetPageElementAttributes for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetPageElementAttributes(v_numSiteID    NUMERIC(9,0),
               v_numElementID NUMERIC(9,0),
			   v_numPageID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
       
   open SWV_RefCur for SELECT PA.numAttributeID,
               PA.numElementID,
               PA.vcAttributeName,
			   CASE	WHEN  v_numElementID =(SELECT numElementID
      FROM PageElementMaster
      WHERE vcTagName = '{#BreadCrumb#}')
   THEN(SELECT vcPageName
         FROM SitePages
         WHERE numPageID = v_numPageID)
   WHEN coalesce(PA.bitEditor,false) = true THEN(SELECT vcHtml
         FROM   PageElementDetail
         WHERE  numSiteID = v_numSiteID
         AND numElementID = v_numElementID
         AND numAttributeID = PA.numAttributeID)
   ELSE coalesce((SELECT vcAttributeValue
         FROM   PageElementDetail
         WHERE  numSiteID = v_numSiteID
         AND numElementID = v_numElementID
         AND numAttributeID = PA.numAttributeID),'')
   END AS vcAttributeValue,
			   PA.vcControlType,
			   PA.vcControlValues,coalesce(PA.bitEditor,false) AS bitEditor
   FROM   PageElementAttributes PA
   WHERE PA.numElementID = v_numElementID;
   RETURN;
END; $$;









