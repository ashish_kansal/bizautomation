-- Stored procedure definition script USP_DeleteSitePage for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteSitePage(v_numPageID NUMERIC(9,0),
	v_numDomainID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM MetaTags WHERE numReferenceID = v_numPageID;
   DELETE FROM StyleSheetDetails WHERE numPageID = v_numPageID;
   DELETE FROM SitePages
   WHERE numPageID = v_numPageID AND numDomainID = v_numDomainID;
   RETURN;
END; $$;





