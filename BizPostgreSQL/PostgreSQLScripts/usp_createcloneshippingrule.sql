-- Stored procedure definition script Usp_CreateCloneShippingRule for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Usp_CreateCloneShippingRule(INOUT v_numRuleId BIGINT 
	,v_vcRuleName VARCHAR(50)
	,v_numDomainID NUMERIC(18,0)
	,v_numRelationship NUMERIC(18,0)
	,v_numProfile NUMERIC(18,0))
LANGUAGE plpgsql
   AS $$
   DECLARE
  my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   v_numNewRuleID  BIGINT;
BEGIN
   -- BEGIN TRANSACTION
IF(SELECT
   COUNT(*)
   FROM
   ShippingRules SR
   WHERE
   numDomainId = v_numDomainID
   AND numRuleID <> coalesce(v_numRuleId,0)
   AND coalesce(numRelationship,0) = coalesce(v_numRelationship,0)
   AND coalesce(numProfile,0) = coalesce(v_numProfile,0)
   AND EXISTS(SELECT
      SRSL1.numRuleID
      FROM
      ShippingRuleStateList SRSL1
      INNER JOIN
      ShippingRuleStateList SRSL2
      ON
      coalesce(SRSL1.numStateID,0) = coalesce(SRSL2.numStateID,0)
      AND coalesce(SRSL1.vcZipPostal,'') = coalesce(SRSL2.vcZipPostal,'')
      AND coalesce(SRSL1.numCountryID,0) = coalesce(SRSL2.numCountryID,0)
      WHERE
      SRSL1.numDomainID = v_numDomainID
      AND SRSL1.numRuleID = SR.numRuleID
      AND SRSL2.numRuleID = v_numRuleId)) > 0
   then
	
      RAISE EXCEPTION 'DUPLICATE RELATIONSHIP, PROFILE AND STATE-ZIP';
      RETURN;
   ELSE
      INSERT INTO ShippingRules(vcRuleName,vcDescription,tintBasedOn,tinShippingMethod,tintIncludeType,numSiteID,numDomainId,tintFixedShippingQuotesMode,tintTaxMode,bitFreeShipping,FreeShippingOrderAmt,
		numRelationship,numProfile, numWareHouseID)
      SELECT v_vcRuleName,vcDescription,tintBasedOn,tinShippingMethod,tintIncludeType,numSiteID,numDomainId,tintFixedShippingQuotesMode,tintTaxMode,bitFreeShipping,FreeShippingOrderAmt,
		v_numRelationship,v_numProfile, numWareHouseID
      FROM ShippingRules WHERE numRuleID = v_numRuleId AND numDomainId = v_numDomainID;
      v_numNewRuleID := CURRVAL('ShippingRules_seq');
      INSERT INTO ShippingServiceTypes(numShippingCompanyID,vcServiceName,intNsoftEnum,intFrom,intTo,fltMarkup,bitMarkupType,monRate,bitEnabled,numRuleID,numDomainID)
      SELECT numShippingCompanyID,vcServiceName,intNsoftEnum,intFrom,intTo,fltMarkup,bitMarkupType,monRate,bitEnabled,v_numNewRuleID,numDomainID
      FROM ShippingServiceTypes WHERE numRuleID = v_numRuleId AND numDomainID = v_numDomainID;
      INSERT INTO ShippingRuleStateList(numRuleID,numCountryID,numStateID,numDomainID,tintMode,vcFromZip,vcToZip,vcZipPostal)
      SELECT v_numNewRuleID, numCountryID,numStateID,numDomainID,tintMode,vcFromZip,vcToZip,vcZipPostal FROM ShippingRuleStateList WHERE numRuleID = v_numRuleId;
      v_numRuleId := v_numNewRuleID;
   end if;
   -- COMMIT
EXCEPTION WHEN OTHERS THEN
    GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
END; $$;


