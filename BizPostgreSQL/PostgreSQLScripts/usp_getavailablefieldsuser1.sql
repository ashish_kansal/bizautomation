-- Stored procedure definition script usp_GetAvailableFieldsUser1 for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetAvailableFieldsUser1(v_numGroupType NUMERIC,          
v_numDomainId NUMERIC,          
v_numContactId NUMERIC, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor , INOUT SWV_RefCur3 refcursor , INOUT SWV_RefCur4 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   if(select count(*) from ShortCutUsrCnf where bittype = true and numDomainID = v_numDomainId and numGroupID = v_numGroupType and numContactId = v_numContactId) > 0 then
			
      open SWV_RefCur for
      Select Hdr.ID,Hdr.vcLinkName,sconf.numOrder as "Order" from ShortCutGrpConf sconf join  ShortCutBar Hdr on  Hdr.ID = sconf.numLinkId
      where sconf.bitType = true and sconf.numDomainId = v_numDomainId and sconf.numGroupId = v_numGroupType  and numContactId = 0  and sconf.numLinkId not in(select numLinkid from ShortCutUsrCnf where bittype = true and numDomainID = v_numDomainId and numGroupID = v_numGroupType
         and numContactId = v_numContactId)
      union
      select Hdr.ID,Hdr.vcLinkName,"order" from  ShortCutBar Hdr where Hdr.bitType = true and Hdr.numDomainID = v_numDomainId and Hdr.numGroupID = v_numGroupType
      and numContactId = v_numContactId and ID not in(select numLinkid from ShortCutUsrCnf where bittype = true and numDomainID = v_numDomainId and numGroupID = v_numGroupType
         and numContactId = v_numContactId) order by "order";

      open SWV_RefCur2 for
      Select Hdr.ID,Hdr.vcLinkName from ShortCutUsrCnf Sconf join  ShortCutBar Hdr on  Hdr.ID = Sconf.numLinkid
      where Sconf.bittype = true and Sconf.numDomainID = v_numDomainId and Sconf.numGroupID = v_numGroupType  and Sconf.numContactId = v_numContactId  order by Sconf.numOrder;
   else
      open SWV_RefCur for
      select Hdr.ID,Hdr.vcLinkName,numorder as "order" from ShortCutGrpConf  Sconf  join  ShortCutBar Hdr  on  Hdr.ID = Sconf.numLinkId  where Sconf.bitType = true And Sconf.numGroupId = v_numGroupType
      and Sconf.numDomainId = v_numDomainId and numContactId = 0
      union
      select ID,vcLinkName,"order" from ShortCutBar where bitType = true and bitdefault = false and numGroupID = v_numGroupType
      and numDomainID = v_numDomainId   and numContactId = v_numContactId order by 1;

      open SWV_RefCur2 for
      select Hdr.ID,Hdr.vcLinkName from ShortCutGrpConf  Sconf  join  ShortCutBar Hdr  on  Hdr.ID = Sconf.numLinkId  where  Sconf.numGroupId = 0;
   end if;          
      
       
        
        
   if(select count(*) from ShortCutUsrCnf where bittype = false and numDomainID = v_numDomainId and numGroupID = v_numGroupType  and numContactId = v_numContactId) > 0 then
	  
      open SWV_RefCur3 for
      Select Hdr.ID,Hdr.vcLinkName from ShortCutGrpConf Sconf join  ShortCutBar Hdr on  Hdr.ID = Sconf.numLinkId
      where Sconf.bitType = false and Sconf.numDomainId = v_numDomainId and Sconf.numGroupId = v_numGroupType   and numContactId = 0
      and Sconf.numLinkId not in(select numLinkid from ShortCutUsrCnf Sconf where Sconf.bittype = false and Sconf.numDomainID = v_numDomainId and Sconf.numGroupID = v_numGroupType  and numContactId = v_numContactId)
      order by Sconf.numOrder;

      open SWV_RefCur4 for
      Select Hdr.ID,Hdr.vcLinkName from ShortCutUsrCnf Sconf join  ShortCutBar Hdr on  Hdr.ID = Sconf.numLinkid
      where Sconf.bittype = false and Sconf.numDomainID = v_numDomainId and Sconf.numGroupID = v_numGroupType  and Sconf.numContactId = v_numContactId order by Sconf.numOrder;
   else
      open SWV_RefCur3 for
      select Hdr.ID,Hdr.vcLinkName from ShortCutGrpConf  Sconf  join  ShortCutBar Hdr  on  Hdr.ID = Sconf.numLinkId  where Sconf.bitType = false And Sconf.numGroupId = v_numGroupType
      and numContactId = 0 and Sconf.numDomainId = v_numDomainId  order by Sconf.numOrder;

      open SWV_RefCur4 for
      select Hdr.ID,Hdr.vcLinkName from ShortCutGrpConf  Sconf  join  ShortCutBar Hdr  on  Hdr.ID = Sconf.numLinkId  where  Sconf.numGroupId = 0;
   end if;
   RETURN;
END; $$;


