-- Stored procedure definition script usp_GetDuplicateCompany_new for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetDuplicateCompany_new(v_vcCompanyName VARCHAR(100)
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(CI.numCompanyId as VARCHAR(255)), cast(CI.vcCompanyName as VARCHAR(255)), DM.vcDivisionName
   from CompanyInfo CI, DivisionMaster DM
   where CI.numCompanyId = DM.numCompanyID And
   CI.vcCompanyName = v_vcCompanyName
--And DM.numDivisionID <> @numDivisionID and DM.bitPublicFlag=0 and DM.tintCRMType in (1,2)
   Group by
   CI.numCompanyId,CI.vcCompanyName,DM.vcDivisionName;
END; $$;












