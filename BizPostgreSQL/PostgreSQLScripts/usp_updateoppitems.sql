DROP FUNCTION IF EXISTS USP_UpdateOppItems;
CREATE OR REPLACE FUNCTION USP_UpdateOppItems(v_numOppID NUMERIC(9,0) DEFAULT 0,
v_numItemCode NUMERIC(9,0) DEFAULT 0,
v_numUnitHour NUMERIC DEFAULT 0,
v_monPrice DECIMAL(20,5) DEFAULT NULL,
v_vcItemName VARCHAR(300) DEFAULT NULL,
v_vcModelId VARCHAR(200) DEFAULT NULL,
v_vcPathForTImage VARCHAR(300) DEFAULT NULL,
v_vcItemDesc VARCHAR(4000) DEFAULT NULL,
v_numWarehouseItmsID NUMERIC(9,0) DEFAULT NULL,
v_ItemType VARCHAR(50) DEFAULT NULL,
v_DropShip BOOLEAN DEFAULT false,
v_numUOM NUMERIC(9,0) DEFAULT 0,
v_numClassID NUMERIC(9,0) DEFAULT 0,
v_numProjectID NUMERIC(9,0) DEFAULT 0,
v_vcNotes VARCHAR(4000) DEFAULT null,
v_dtItemRequiredDate TIMESTAMP DEFAULT null,
v_vcAttributes TEXT DEFAULT NULL,
v_vcAttributeIDs TEXT DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcManufacturer  VARCHAR(250);
   v_ID NUMERIC(18,0);
BEGIN
   v_vcNotes :=(CASE WHEN v_vcNotes = '&nbsp;' THEN '' ELSE v_vcNotes END);
   v_vcItemDesc :=(CASE WHEN coalesce(v_vcItemDesc,'') = '' THEN coalesce((SELECT txtItemDesc FROM Item WHERE numItemCode = v_numItemCode),
      '') ELSE v_vcItemDesc END);
	
   if v_numWarehouseItmsID = 0 then 
      v_numWarehouseItmsID := null;
   end if; 
	
   select   vcManufacturer INTO v_vcManufacturer FROM Item WHERE numItemCode = v_numItemCode;
   insert into OpportunityItems(numOppId,numItemCode,vcNotes,numUnitHour,monPrice,numCost,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,vcModelID,vcPathForTImage,vcManufacturer,numUOMId,numClassID,numProjectID,numSortOrder,ItemRequiredDate,vcAttributes,vcAttrValues)
	values(v_numOppID,v_numItemCode,v_vcNotes,v_numUnitHour,v_monPrice,v_monPrice,v_numUnitHour*v_monPrice,v_vcItemDesc,v_numWarehouseItmsID,v_ItemType,v_DropShip,false,0,v_numUnitHour*v_monPrice,v_vcItemName,v_vcModelId,v_vcPathForTImage,v_vcManufacturer,v_numUOM,v_numClassID,v_numProjectID,coalesce((SELECT MAX(numSortOrder) FROM OpportunityItems WHERE numOppId = v_numOppID), 0)+1,v_dtItemRequiredDate,v_vcAttributes,v_vcAttributeIDs) RETURNING numoppitemtcode INTO v_ID;
	
	
open SWV_RefCur for select v_ID;
	                                 
   update OpportunityMaster set  monPAmount =(select sum(monTotAmount) from OpportunityItems where numOppId = v_numOppID)
   where numOppId = v_numOppID;
   RETURN;
END; $$;












