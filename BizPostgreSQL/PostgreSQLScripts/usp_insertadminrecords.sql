-- Stored procedure definition script USP_InsertAdminRecords for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_InsertAdminRecords(v_vcCompanyName VARCHAR(100) DEFAULT '',  
v_vcDomainName VARCHAR(50) DEFAULT '')
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strDomainID  NUMERIC(15);  
   v_strCompanyID  NUMERIC(15);  
   v_strDivisionID  NUMERIC(15);  
   v_strContactID  NUMERIC(15);
BEGIN
   insert into Domain(vcDomainName)
values(v_vcDomainName);  

   v_strDomainID := CAST(CURRVAL('domain_seq') AS NUMERIC(18,0));  
  
   insert  into CompanyInfo(vcCompanyName,numCompanyType,numNoOfEmployeesId,numCompanyRating,numCompanyIndustry,numAnnualRevID,numCompanyCredit,numCreatedBy,bintCreatedDate,
 numModifiedBy,bintModifiedDate,numDomainID)
values(v_vcCompanyName,0,0,0,0,0,0,1,TIMEZONE('UTC',now()),1,TIMEZONE('UTC',now()),v_strDomainID);  
  

   v_strCompanyID := CAST(CURRVAL('CompanyInfo_seq') AS NUMERIC(18,0));  
  
   insert into DivisionMaster(numCompanyID,vcDivisionName,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,tintCRMType,numDomainID,numGrpId,numRecOwner,numTerID)
values(CAST(v_strCompanyID AS NUMERIC(18,0)),'-',1,TIMEZONE('UTC',now()),1,TIMEZONE('UTC',now()),1,CAST(v_strDomainID AS NUMERIC(18,0)),0,1,0);  
  

   v_strDivisionID := CAST(CURRVAL('DivisionMaster_seq') AS NUMERIC(18,0));  
  
   insert into AdditionalContactsInformation(vcGivenName,vcFirstName,vcLastname,vcEmail,numPhone,numDivisionId,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainID,bintDOB)
values('Administrator','Administrator','','','',CAST(v_strDivisionID AS NUMERIC(18,0)),1,TIMEZONE('UTC',now()),1,TIMEZONE('UTC',now()),v_strDomainID,null::TIMESTAMP);  

   v_strContactID := CAST(CURRVAL('AdditionalContactsInformation_seq') AS NUMERIC(18,0));  

   insert into UserMaster(vcUserName,vcUserDesc,numGroupID,numDomainID,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,bitactivateflag,numUserDetailId)
values('Administrator','BACRM Admin',1,1,1,TIMEZONE('UTC',now()),1,TIMEZONE('UTC',now()),false,v_strContactID);
RETURN;
END; $$;


