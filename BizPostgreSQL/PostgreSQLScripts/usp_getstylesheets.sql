-- Stored procedure definition script USP_GetStyleSheets for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetStyleSheets( -- 0- css , 1- javascript
v_numCssID NUMERIC(9,0) DEFAULT 0,
	v_numSiteID NUMERIC(9,0) DEFAULT NULL,
    v_numDomainID NUMERIC(9,0) DEFAULT NULL,
	v_tintType SMALLINT DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numSiteID = 0 then
      v_numSiteID := NULL;
   end if;
	
   IF v_tintType = 0 then
	  
      open SWV_RefCur for
      SELECT numCssID,
	               StyleName,
	               styleFileName,
	               numSiteID,
	               numDomainID,
	               0 AS intDisplayOrder
      FROM StyleSheets
      WHERE (numCssID = v_numCssID OR v_numCssID = 0)
      AND (numSiteID = v_numSiteID OR v_numSiteID IS NULL)
      AND numDomainID = v_numDomainID
      AND tintType = v_tintType;
   ELSEIF v_tintType = 1
   then
      
      open SWV_RefCur for
      SELECT numCssID,
	               StyleName,
	               styleFileName,
	               numSiteID,
	               numDomainID,
	               coalesce(intDisplayOrder,0) AS intDisplayOrder
      FROM StyleSheets
      WHERE (numCssID = v_numCssID OR v_numCssID = 0)
      AND (numSiteID = v_numSiteID OR v_numSiteID IS NULL)
      AND numDomainID = v_numDomainID
      AND tintType = v_tintType;
   ELSEIF v_tintType = 3
   then
      
      open SWV_RefCur for
      SELECT numCssID,
	               StyleName,
	               styleFileName,
	               numSiteID,
	               numDomainID,
	               coalesce(intDisplayOrder,0) AS intDisplayOrder
      FROM StyleSheets
      WHERE (numCssID = v_numCssID OR v_numCssID = 0)
      AND (numSiteID = v_numSiteID OR v_numSiteID IS NULL)
      AND numDomainID = v_numDomainID
      AND tintType = v_tintType;
   ELSEIF v_tintType = 4
   then
      
      open SWV_RefCur for
      SELECT numCssID,
	               StyleName,
	               styleFileName,
	               numSiteID,
	               numDomainID,
	               coalesce(intDisplayOrder,0) AS intDisplayOrder
      FROM StyleSheets
      WHERE (numCssID = v_numCssID OR v_numCssID = 0)
      AND (numSiteID = v_numSiteID OR v_numSiteID IS NULL)
      AND numDomainID = v_numDomainID
      AND tintType = v_tintType;
   end if;
   RETURN;
END; $$;
--exec USP_GetStyleSheets @numCssID=0,@numSiteID=0,@numDomainID=1,@tintType=3


