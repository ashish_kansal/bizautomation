-- Stored procedure definition script Resource_Sel for PostgreSQL
CREATE OR REPLACE FUNCTION Resource_Sel(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   cast(Resource.ResourceID as VARCHAR(255)),
		cast(Resource.ResourceName as VARCHAR(255)),
		cast(Resource.ResourceDescription as VARCHAR(255)),
		cast(Resource.EmailAddress as VARCHAR(255)),
		cast(coalesce(ResourcePreference.EnableEmailReminders,0) as VARCHAR(255)) AS EnableEmailReminders,
		cast(Resource._ts as VARCHAR(255))
   FROM
   Resource LEFT JOIN ResourcePreference ON Resource.ResourceID = ResourcePreference.ResourceID;
END; $$;













