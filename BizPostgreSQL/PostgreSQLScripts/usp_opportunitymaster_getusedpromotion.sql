-- Stored procedure definition script USP_OpportunityMaster_GetUsedPromotion for PostgreSQL
CREATE OR REPLACE FUNCTION USP_OpportunityMaster_GetUsedPromotion(v_numDomainID NUMERIC(18,0),
	v_numOppID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDivisionID  NUMERIC(18,0);
   v_tintShipToAddressType  SMALLINT;
   v_numShipToCountry  NUMERIC(18,0);
BEGIN
   select   tintShipToType, numDivisionId INTO v_tintShipToAddressType,v_numDivisionID FROM
   OpportunityMaster WHERE
   numOppId = v_numOppID;
               
   IF (v_tintShipToAddressType IS NULL OR v_tintShipToAddressType = 1) then
      select   AD.numCountry INTO v_numShipToCountry FROM
      AddressDetails AD WHERE
      AD.numRecordID = v_numDivisionID
      AND AD.tintAddressOf = 2
      AND AD.tintAddressType = 2
      AND AD.bitIsPrimary = true;
   ELSEIF v_tintShipToAddressType = 0
   then
      select   AD1.numCountry INTO v_numShipToCountry FROM
      CompanyInfo Com1
      JOIN
      DivisionMaster div1 ON Com1.numCompanyId = div1.numCompanyID
      JOIN
      Domain D1 ON D1.numDivisionId = div1.numDivisionID
      JOIN
      AddressDetails AD1 ON AD1.numDomainID = div1.numDomainID
      AND AD1.numRecordID = div1.numDivisionID
      AND tintAddressOf = 2
      AND tintAddressType = 2
      AND bitIsPrimary = true WHERE
      D1.numDomainId = v_numDomainID;
   ELSEIF v_tintShipToAddressType = 2
   then
      select   numShipCountry INTO v_numShipToCountry FROM
      OpportunityAddress WHERE
      numOppID = v_numOppID;
   ELSEIF v_tintShipToAddressType = 3
   then
      select   numShipCountry INTO v_numShipToCountry FROM
      OpportunityAddress WHERE
      numOppID = v_numOppID;
   end if;
				
   open SWV_RefCur for SELECT
   numProId
		,vcProName
		,bitNeverExpires
		,bitRequireCouponCode
		,txtCouponCode
		,tintUsageLimit
		,intCouponCodeUsed
		,bitFreeShiping
		,monFreeShippingOrderAmount
		,numFreeShippingCountry
		,bitFixShipping1
		,monFixShipping1OrderAmount
		,monFixShipping1Charge
		,bitFixShipping2
		,monFixShipping2OrderAmount
		,monFixShipping2Charge
		,tintOfferTriggerValueType
		,fltOfferTriggerValue
		,tintOfferBasedOn
		,fltDiscountValue
		,tintDiscountType
		,tintDiscoutBaseOn
		,FormatedDateFromDate(dtValidTo,v_numDomainID) AS dtExpire
		,(CASE tintOfferBasedOn
   WHEN 1 THEN COALESCE((SELECT string_agg(CAST(numValue AS VARCHAR),',') FROM PromotionOfferItems WHERE numProId = PO.numProId AND tintRecordType = 5 AND tintType = 1),'')
   WHEN 2 THEN COALESCE((SELECT string_agg(CAST(numValue AS VARCHAR),',') FROM PromotionOfferItems WHERE numProId = PO.numProId AND tintRecordType = 5 AND tintType = 2),'')
   END) As vcPromotionItems
		,(CASE tintDiscoutBaseOn
   WHEN 1 THEN COALESCE((SELECT string_agg(CAST(numValue AS VARCHAR),',') FROM PromotionOfferItems WHERE numProId = PO.numProId AND tintRecordType = 6 AND tintType = 1),'')
   WHEN 2 THEN COALESCE((SELECT string_agg(CAST(numValue AS VARCHAR),',') FROM PromotionOfferItems WHERE numProId = PO.numProId AND tintRecordType = 6 AND tintType = 2),'')
   WHEN 3 THEN COALESCE((SELECT string_agg(CAST(SimilarItems.numItemCode AS VARCHAR),',') FROM SimilarItems WHERE 1 =(CASE
            WHEN tintOfferBasedOn = 1 THEN CASE WHEN numParentItemCode IN(SELECT numValue FROM PromotionOfferItems WHERE numProId = PO.numProId AND tintRecordType = 5 AND tintType = 1) THEN 1 ELSE 0 END
            WHEN tintOfferBasedOn = 2 THEN CASE WHEN numParentItemCode IN(SELECT numItemCode FROM Item WHERE numDomainID = v_numDomainID AND numItemClassification IN(SELECT numValue FROM PromotionOfferItems WHERE numProId = PO.numProId AND tintRecordType = 5 AND tintType = 2)) THEN 1 ELSE 0 END
            ELSE 0
            END)),'')
   END) As vcItems
		,CONCAT('Buy ',CASE WHEN tintOfferTriggerValueType = 1 THEN CAST(fltOfferTriggerValue AS VARCHAR(30)) ELSE CONCAT('$',fltOfferTriggerValue) END,CASE tintOfferBasedOn
   WHEN 1 THEN CONCAT(' of "',COALESCE((SELECT string_agg(Item.vcItemName,', ') FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId = PO.numProId AND tintRecordType = 5 AND tintType = 1),''),'"')
   WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',COALESCE((SELECT string_agg(Listdetails.vcData,', ') FROM PromotionOfferItems INNER JOIN Listdetails ON PromotionOfferItems.numValue = Listdetails.numListItemID WHERE numListID = 36 AND numProId = PO.numProId AND tintRecordType = 5 AND tintType = 2),''), 
      '"')
   END,' & get ',CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue,' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END, 
   CASE
   WHEN tintDiscoutBaseOn = 1 THEN
      CONCAT(CONCAT(' "',COALESCE((SELECT string_agg(Item.vcItemName,',') FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId = PO.numProId AND tintRecordType = 6 AND tintType = 1),''), 
      '"'),(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END),
      '.')
   WHEN tintDiscoutBaseOn = 2 THEN
      CONCAT(CONCAT(' items belonging to classification(s) "',COALESCE((SELECT string_agg(Listdetails.vcData,',') FROM PromotionOfferItems INNER JOIN Listdetails ON PromotionOfferItems.numValue = Listdetails.numListItemID WHERE numListID = 36 AND numProId = PO.numProId AND tintRecordType = 6 AND tintType = 2),''), 
      '"'),(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END),
      '.')
   WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
   ELSE ''
   END) AS vcPromotionDescription
			,CONCAT((CASE WHEN coalesce(bitFixShipping1,false) = true THEN CONCAT('$',monFixShipping1Charge,' shipping on order of $',monFixShipping1OrderAmount,
      ' or more. ') ELSE '' END),(CASE WHEN coalesce(bitFixShipping2,false) = true  THEN CONCAT('$',monFixShipping2Charge,' shipping on order of $',monFixShipping2OrderAmount,
      ' or more. ') ELSE '' END),(CASE WHEN (bitFreeShiping = true AND numFreeShippingCountry = v_numShipToCountry) THEN CONCAT('Free shipping on order of $',monFreeShippingOrderAmount,'. ') ELSE '' END)) AS vcShippingDescription,
			CASE
   WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
   WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
   WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
   WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
   WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
   WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
   END AS tintPriority
   FROM
   PromotionOffer PO
   LEFT JOIN ShippingPromotions SP ON PO.numDomainId = SP.numDomainID
   WHERE
   PO.numDomainId = v_numDomainID
   AND numProId IN(SELECT Distinct numPromotionID FROM OpportunityItems WHERE numOppId = v_numOppID);
END; $$;












