-- Stored procedure definition script USP_GetTimeAndExpensByRecordId for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetTimeAndExpensByRecordId(v_numDomainID NUMERIC(9,0) DEFAULT 0,
	v_numRecordId NUMERIC(9,0) DEFAULT 0,
    v_ClientTimeZoneOffset INTEGER DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT  CASE WHEN tintTEType = 0 THEN 'T & E'
   WHEN tintTEType = 1
   THEN CASE WHEN numCategory = 1
      THEN CASE WHEN(SELECT  tintopptype
            FROM    OpportunityMaster
            WHERE   numOppId = TE.numOppId) = 1 THEN 'Sales Time'
         ELSE 'Purch Time'
         END
      ELSE CASE WHEN(SELECT  tintopptype
            FROM    OpportunityMaster
            WHERE   numOppId = TE.numOppId) = 1 THEN 'Sales Exp'
         ELSE 'Purch Exp'
         END
      END
   WHEN tintTEType = 2
   THEN CASE WHEN numCategory = 1 THEN 'Project Time'
      ELSE 'Project Exp'
      END
   WHEN tintTEType = 3
   THEN CASE WHEN numCategory = 1 THEN 'Case Time'
      ELSE 'Case Exp'
      END
   WHEN tintTEType = 4
   THEN 'Non-Paid Leave'
   WHEN tintTEType = 5
   THEN 'Expense'
   END AS chrFrom,
                    numCategoryHDRID ,
                    tintTEType ,
                    CASE WHEN numCategory = 1
   THEN 'Time (' ||(CAST(dtFromDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(30))
      || '-'
      || CAST(dtToDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(30)))
      || ')'
   WHEN numCategory = 2 AND coalesce(bitReimburse,false) = true AND numtype = 2  THEN 'Reimbursable Expense'
   WHEN numCategory = 2 AND coalesce(bitReimburse,false) = true AND numtype = 6 THEN 'Billable + Reimbursable Expense'
   WHEN numCategory = 2 AND coalesce(bitReimburse,false) = false AND numtype = 1 THEN 'Billable Expense'
   END AS Category,
                    CASE WHEN numtype = 1 THEN 'Billable'
   WHEN numtype = 2 THEN 'Non-Billable'
   WHEN numtype = 5 THEN 'Reimbursable Expense'
   WHEN numtype = 6 THEN 'Billable'
                         --WHEN numType = 3 THEN 'Paid Leave'
                         --WHEN numType = 4 THEN 'Non-Paid'
   END AS Type,
                    CASE WHEN numCategory = 1
   THEN dtFromDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)
   ELSE dtFromDate
   END AS dtFromDate,
                    CASE WHEN numCategory = 1
   THEN dtToDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)
   ELSE dtToDate
   END AS dtToDate,
                    bitFromFullDay ,
                    bitToFullDay ,
                    monAmount AS monAmount,
                    TE.numOppId ,
                    OM.vcpOppName,
					CA.vcAccountName,
                    TE.numDivisionID ,
                    txtDesc ,
                    numUserCntID ,
                    coalesce(ACI.vcFirstName,'') || ' ' || coalesce(ACI.vcLastname,'') AS vcEmployee,
                    TE.numDomainID ,
                    TE.numcontractId ,
                    numCaseid ,
                    TE.numProId ,
                    PM.vcProjectID ,
                    numOppBizDocsId ,
                    numStageID  AS numStageID,
                    numCategory ,
                    numtype ,
                    CASE WHEN numCategory = 1
   THEN CAST(CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DOUBLE PRECISION)/60 AS VARCHAR(30))
   WHEN numCategory = 2
   THEN CAST(monAmount AS VARCHAR)
   WHEN numCategory = 3
   THEN(CASE WHEN bitFromFullDay = false THEN 'HDL'
      WHEN bitFromFullDay = true THEN 'FDL'
      END)
   WHEN numCategory = 4
   THEN(CASE WHEN bitFromFullDay = false THEN 'HDL'
      WHEN bitFromFullDay = true THEN 'FDL'
      END)
   END AS Detail
                    ,CAST(CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DOUBLE PRECISION)/60 AS VARCHAR(30)) AS TimeValue
                    ,CASE WHEN numCategory = 1 THEN CAST((CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DOUBLE PRECISION)/60)*coalesce(TE.monAmount,1) AS DECIMAL(10,2))
   WHEN numCategory = 2 THEN coalesce(TE.monAmount,1)
   ELSE 0
   END AS ExpenseValue
                    ,(SELECT coalesce(numItemCode,0) FROM Item  WHERE numItemCode IN(SELECT numItemCode FROM OpportunityItems WHERE numoppitemtCode = TE.numOppItemID))  AS numItemCode
					,TE.numClassID 
					,(SELECT coalesce(vcItemDesc,'') FROM OpportunityItems WHERE numoppitemtCode = TE.numOppItemID)  AS vcItemDesc
					,CASE WHEN TE.numApprovalComplete = 6 THEN 'Approved' WHEN TE.numApprovalComplete = -1 THEN 'Declined'  WHEN coalesce(TE.numApprovalComplete,0) = 0 THEN '' ELSE 'Waiting For Approve' END as ApprovalStatus
					,TE.numApprovalComplete ,TE.numServiceItemID ,TE.numClassID  as numEClassId,TE.numExpId 
   FROM    timeandexpense TE
   LEFT JOIN OpportunityMaster OM ON OM.numOppId = TE.numOppId AND TE.numDomainID = OM.numDomainId
   LEFT JOIN ProjectsMaster PM ON PM.numProId = TE.numProId AND TE.numDomainID = PM.numdomainId
   LEFT JOIN Chart_Of_Accounts CA ON CA.numAccountId = TE.numExpId AND TE.numDomainID = CA.numDomainId
   LEFT JOIN AdditionalContactsInformation ACI ON ACI.numContactId = TE.numUserCntID
   LEFT JOIN ApprovalConfig AS A on TE.numUserCntID = A.numUserId AND A.numDomainID = v_numDomainID AND A.numModule = 1 WHERE TE.numCategoryHDRID = v_numRecordId LIMIT 1;
END; $$;

