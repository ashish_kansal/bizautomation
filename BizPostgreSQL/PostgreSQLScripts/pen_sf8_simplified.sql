-- Function definition script pen_SF8_simplified for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION pen_SF8_simplified(v_SF8PF INTEGER)
RETURNS DECIMAL(9,2) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_return_value  DECIMAL(9,2); 
   v_SF8PF_val1  DECIMAL(4,2); 
   v_SF8PF_val2  DECIMAL(4,2); 
   v_SF8PF_val3  DECIMAL(4,2); 
   v_SF8PF_val4  DECIMAL(4,2);
   v_SF8PF_val5  DECIMAL(4,2);
BEGIN
   v_SF8PF_val1 := 21.46; 
   v_SF8PF_val2 := 30.31; 
   v_SF8PF_val3 := 40.07; 
   v_SF8PF_val4 := 48.33; 
   v_SF8PF_val5 := 54.05;

   v_return_value := Case v_SF8PF when 1 then v_SF8PF_val1 when 2 then v_SF8PF_val2 when
   3 then v_SF8PF_val3 when 4 then v_SF8PF_val4 when 5 then v_SF8PF_val5 End;

   RETURN v_return_value;
END; $$;

