-- Stored procedure definition script USP_UserList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UserList(v_numDomainID NUMERIC(18,0),             
	 v_CurrentPage INTEGER,                  
	 v_PageSize INTEGER,                
	 v_columnName VARCHAR(100),                  
	 v_columnSortOrder VARCHAR(100),
	 v_vcCustomSearch TEXT,
	 INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSql  VARCHAR(8000);
BEGIN
   v_strSql := CONCAT('SELECT 
							COUNT(*) OVER() numTotalRecords
							,numUserID
							,COALESCE(vcEmailID,''-'') as vcEmailID
							,COALESCE(vcEmailAlias,'''') as vcEmailAlias
							,COALESCE(vcEmailAliasPassword,'''') vcEmailAliasPassword
							,CONCAT(''<a onclick="return fnEditUser('',numUserID,'')">'',COALESCE(ADC.vcfirstname,''''),'' '',COALESCE(ADC.vclastname,''''),''</a>'') AS Name
							,COALESCE(vcGroupName,''-'') AS vcGroupName
							,vcDomainName
							,(CASE WHEN bitActivateFlag=true THEN ''Active'' ELSE ''Suspended'' END) AS Active
							,COALESCE(bitActivateFlag,false) bitActivateFlag
							,numDefaultWarehouse
							,COALESCE((SELECT COUNT(*) FROM UserAccessedDTL UAD WHERE UAD.numDomainID=',v_numDomainID,
   ' AND UAD.numDivisionID=Div.numDivisionID AND UAD.numContactID=ADC.numContactID),0) intLoginCount      
						FROM 
							UserMaster                    
						JOIN 
							Domain                      
						ON 
							UserMaster.numDomainID =  Domain.numDomainID 
						INNER JOIN 
							DivisionMaster Div 
						ON 
							Domain.numDivisionID=Div.numDivisionID
						INNER JOIN 
							AdditionalContactsInformation ADC                    
						ON 
							ADC.numContactid=UserMaster.numUserDetailId                   
						LEFT JOIN 
							AuthenticationGroupMaster GM                   
						ON 
							Gm.numGroupID= UserMaster.numGroupID              
						WHERE 
							UserMaster.numDomainID=',v_numDomainID);       

   IF LENGTH(coalesce(v_vcCustomSearch,'')) > 0 then
	
      v_strSql := CONCAT(v_strSql,' AND ',v_vcCustomSearch);
   end if;	
						
   IF coalesce(v_columnName,'') <> '' then
	
      IF v_columnName = 'UserName' then
		
         v_columnName := 'CONCAT(ADC.vcfirstname,'' '',ADC.vcLastName)';
      ELSEIF v_columnName = 'Email'
      then
		
         v_columnName := 'UserMaster.vcEmailID';
      ELSEIF v_columnName = 'LoginActivity'
      then
		
         v_columnName := CONCAT('COALESCE((SELECT COUNT(*) FROM UserAccessedDTL UAD WHERE UAD.numDomainID=',v_numDomainID,' AND UAD.numDivisionID=Div.numDivisionID AND UAD.numContactID=ADC.numContactID),0)');
      end if;
      v_strSql := CONCAT(v_strSql,' ORDER BY ',v_columnName,' ',v_columnSortOrder);
   ELSE
      v_strSql := CONCAT(v_strSql,' ORDER BY CONCAT(ADC.vcfirstname,'' '',ADC.vcLastName) ASC');
   end if;
   
   v_strSql := CONCAT(v_strSql,' OFFSET ',(v_CurrentPage::bigint -1)*v_PageSize::bigint,' ROWS FETCH NEXT ',
   v_PageSize,' ROWS ONLY');

   OPEN SWV_RefCur FOR EXECUTE v_strSql;
   RETURN;
END; $$;


