-- Function definition script fn_GetEmployeeName for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetEmployeeName(v_numBizDocAppID NUMERIC(8,0),
 v_numDomainId INTEGER)
RETURNS VARCHAR(2000) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcEmployee  VARCHAR(2000);
   v_vcUserName  VARCHAR(100);


   Employee_Cursor CURSOR FOR
	
   select ACI.vcFirstName || ' ' || ACI.vcLastname as vcUserName 
   from AdditionalContactsInformation ACI
   INNER JOIN BizDocApprovalRuleEmployee BAR
   ON BAR.numEmployeeID = ACI.numContactId AND 
   BAR.numBizDocAppID = v_numBizDocAppID;
BEGIN
   v_vcEmployee := '';

   OPEN Employee_Cursor;

   FETCH NEXT FROM Employee_Cursor into v_vcUserName;
   WHILE FOUND LOOP
      v_vcEmployee := coalesce(v_vcEmployee,'') || ',' || coalesce(v_vcUserName,'');
      FETCH NEXT FROM Employee_Cursor into v_vcUserName;
   END LOOP;

   CLOSE Employee_Cursor;


   RETURN SUBSTR(v_vcEmployee,2,LENGTH(v_vcEmployee));
END; $$;
/****** Object:  UserDefinedFunction [dbo].[fn_GetExpenseDtlsbyProStgID]    Script Date: 07/26/2008 18:12:36 ******/

