-- Stored procedure definition script USP_EditDelShortCutGrpCnf for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_EditDelShortCutGrpCnf(v_Bytemode BOOLEAN,  
v_LinkId NUMERIC,  
v_Name VARCHAR(100),  
v_Link VARCHAR(100))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   if v_Bytemode = false then

      delete from ShortCutBar where ID = v_LinkId;
      delete from ShortCutGrpConf where numLinkId = v_LinkId;
      delete from ShortCutUsrCnf where numLinkid = v_LinkId;
   end if;  
  
   if v_Bytemode = true then

      update ShortCutBar set
      vcLinkName = v_Name,Link = v_Link
      where ID = v_LinkId;
   end if;
   RETURN;
END; $$;


