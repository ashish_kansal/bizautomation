-- Stored procedure definition script USP_ItemDepreciation_Get for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ItemDepreciation_Get(v_numDomainID NUMERIC(18,0),
	v_numItemCode NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		ID
		,ItemDepreciation.numItemCode
		,ItemDepreciation.numOppID
		,ItemDepreciation.numOppItemID
		,intYear
		,monStartingValue
		,numRate
		,monDepreciation
		,monEndingValue
		,monStartingValue*coalesce(numUnitHourReceived,0) AS monStartingValueTotal
		,monDepreciation*coalesce(numUnitHourReceived,0) AS monDepreciationTotal
		,monEndingValue*coalesce(numUnitHourReceived,0) AS monEndingValueTotal
		,(CASE WHEN EXISTS(SELECT numFinYearId FROM FinancialYear WHERE numDomainId = v_numDomainID AND intYear BETWEEN EXTRACT(YEAR FROM dtPeriodFrom) AND EXTRACT(YEAR FROM dtPeriodTo) AND coalesce(bitCloseStatus,false) = true) THEN true ELSE false END) AS bitClosed
		,(CASE WHEN EXISTS(SELECT numFinYearId FROM FinancialYear WHERE numDomainId = v_numDomainID AND intYear BETWEEN EXTRACT(YEAR FROM dtPeriodFrom) AND EXTRACT(YEAR FROM dtPeriodTo) AND coalesce(bitCurrentYear,false) = true) THEN true ELSE false END) AS bitCurrent
		,(CASE WHEN(ROW_NUMBER() OVER(ORDER BY intYear DESC)) = 1 THEN true ELSE false END) AS bitLastYear
   FROM
   ItemDepreciation
   INNER JOIN
   OpportunityMaster
   ON
   ItemDepreciation.numOppID = OpportunityMaster.numOppId
   INNER JOIN
   OpportunityItems
   ON
   ItemDepreciation.numOppItemID = OpportunityItems.numoppitemtCode
   WHERE
   ItemDepreciation.numItemCode = v_numItemCode
   ORDER BY
   ItemDepreciation.intYear ASC;
END; $$;












