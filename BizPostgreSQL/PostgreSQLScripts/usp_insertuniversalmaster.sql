-- Stored procedure definition script usp_InsertUniversalMaster for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_InsertUniversalMaster(v_numDivID NUMERIC(9,0) DEFAULT 0,
	v_numUnisprtId NUMERIC(9,0) DEFAULT 0
	--
)
RETURNS VOID LANGUAGE plpgsql

	--Input Parameter List
   AS $$
BEGIN
	--Insertion into Table  
	
   INSERT INTO UniversalSupportKeyMaster(numDivisionID)
	VALUES(v_numDivID);
RETURN;
END; $$;


