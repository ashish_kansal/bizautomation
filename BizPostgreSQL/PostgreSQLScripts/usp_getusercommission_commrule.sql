-- Stored procedure definition script USP_GetUserCommission_CommRule for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetUserCommission_CommRule(v_numDomainId          NUMERIC(9,0)  DEFAULT 0,
               v_numUserCntID         NUMERIC(9,0)  DEFAULT 0,
			   v_numComRuleID            NUMERIC(9,0)  DEFAULT 0,
               v_numItemCode NUMERIC(9,0) DEFAULT NULL,
               v_bitCommContact BOOLEAN DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintComAppliesTo  SMALLINT; 
   v_tintAssignTo  SMALLINT;
   v_tinComDuration  SMALLINT; 
   v_dFrom  TIMESTAMP;
   v_dTo  TIMESTAMP;
BEGIN
   select   CR.tinComDuration, CR.tintAssignTo, coalesce(CR.tintComAppliesTo,0) INTO v_tinComDuration,v_tintAssignTo,v_tintComAppliesTo FROM CommissionRules CR WHERE CR.numDomainID = v_numDomainId AND CR.numComRuleID = v_numComRuleID    LIMIT 1;
	
	IF v_tinComDuration = 1 then --Month
        v_dFrom := DATE_TRUNC('month', now())::timestamp;
        v_dTo := DATE_TRUNC('month', now())::timestamp + INTERVAL '1 month' - INTERVAL '1 day';
    ELSEIF v_tinComDuration = 2
    then --Quarter
        v_dFrom := DATE_TRUNC('quarter', now())::timestamp;
        v_dTo := DATE_TRUNC('quarter', now())::timestamp + INTERVAL '3 months' - INTERVAL '1 day';
    ELSEIF v_tinComDuration = 3
    then --Year
        v_dFrom := DATE_TRUNC('year', now())::timestamp;
        v_dTo := DATE_TRUNC('year', now())::timestamp + INTERVAL '12 months' - INTERVAL '1 day';
    end if; 																
 
   open SWV_RefCur for Select oppBiz.numBizDocId,oppBiz.vcBizDocID,Opp.numOppId,oppBiz.numOppBizDocsId,cast(coalesce(oppBiz.monAmountPaid,0) as DECIMAL(20,5)) AS monAmountPaid,Opp.bintCreatedDate, Opp.vcpOppName AS Name ,Case when Opp.tintoppstatus = 0 then 'Open' Else 'Close' End as OppStatus, cast(coalesce(oppBiz.monAmountPaid,0) as DECIMAL(20,5)) as DealAmount,
		BC.numComissionID,BC.decCommission,Case when BC.tintComBasedOn = 1 then 'Amount' when BC.tintComBasedOn = 2 then 'Units' end AS BasedOn,
		Case when BC.tintComType = 1 then 'Percentage' when BC.tintComType = 2 then 'Flat' end as CommissionType,
                        numComissionAmount  as CommissionAmt,cast(I.vcItemName as VARCHAR(255)),cast(BDI.numUnitHour as VARCHAR(255)),cast(BDI.monTotAmount as VARCHAR(255)),coalesce(OT.monVendorCost,0)*coalesce(BDI.numUnitHour,0) AS VendorCost
   From OpportunityMaster Opp left join OpportunityBizDocs oppBiz on oppBiz.numoppid = Opp.numOppId
   INNER JOIN BizDocComission BC on BC.numOppBizDocId = oppBiz.numOppBizDocsId
   JOIN OpportunityBizDocItems BDI ON oppBiz.numOppBizDocsId = BDI.numOppBizDocID AND BC.numOppBizDocItemID = BDI.numOppBizDocItemID
   INNER JOIN OpportunityItems OT ON OT.numoppitemtCode = BDI.numOppItemID
   INNER JOIN Item I ON BDI.numItemCode = I.numItemCode
   Where oppBiz.bitAuthoritativeBizDocs = 1 AND oppBiz.monAmountPaid > 0 and Opp.tintoppstatus = 1 And Opp.tintopptype = 1 And
   1 =(CASE v_tintAssignTo WHEN 1 THEN CASE v_bitCommContact WHEN false THEN CASE WHEN Opp.numassignedto = v_numUserCntID THEN 1 ELSE 0 END
      WHEN true THEN CASE WHEN Opp.numassignedto IN(SELECT numContactId FROM AdditionalContactsInformation AC WHERE AC.numDivisionId = v_numUserCntID) THEN 1 ELSE 0 END  END
   WHEN 2 THEN CASE v_bitCommContact WHEN false THEN CASE WHEN Opp.numrecowner = v_numUserCntID THEN 1 ELSE 0 END
      WHEN true THEN CASE WHEN Opp.numrecowner IN(SELECT numContactId FROM AdditionalContactsInformation AC WHERE AC.numDivisionId = v_numUserCntID) THEN 1 ELSE 0 END END END)
   AND 1 =(CASE v_bitCommContact WHEN false THEN CASE WHEN BC.numUserCntID = v_numUserCntID THEN 1 else 0 END
   WHEN true THEN CASE WHEN BC.numUserCntID IN(SELECT numContactId FROM AdditionalContactsInformation AC WHERE AC.numDivisionId = v_numUserCntID) THEN 1 ELSE 0 END  END)
   And Opp.numDomainId = v_numDomainId AND BC.numComRuleID = v_numComRuleID
   AND 1 =(CASE v_tintComAppliesTo WHEN 1 THEN CASE WHEN BDI.numItemCode = v_numItemCode THEN 1 ELSE 0 END
   WHEN 2 THEN CASE WHEN BDI.numItemCode IN(SELECT cast(numItemCode as NUMERIC(18,0)) FROM Item WHERE numItemClassification = v_numItemCode AND numDomainID = v_numDomainId) THEN 1 ELSE 0 END
   WHEN 3 THEN 1 ELSE 0 END)
   AND oppBiz.numOppBizDocsId IN(SELECT OBD.numOppBizDocsId FROM OpportunityBizDocsDetails BDD
      INNER JOIN OpportunityBizDocs OBD ON BDD.numBizDocsId = OBD.numOppBizDocsId
      INNER JOIN OpportunityMaster OM ON OM.numOppId = OBD.numoppid
      WHERE tintopptype = 1 AND tintoppstatus = 1 AND OM.numDomainId = v_numDomainId
      AND OBD.bitAuthoritativeBizDocs = 1 AND BDD.dtCreationDate BETWEEN v_dFrom AND v_dTo AND
      1 =(CASE v_tintAssignTo WHEN 1 THEN CASE v_bitCommContact WHEN false THEN CASE WHEN OM.numassignedto = v_numUserCntID THEN 1 ELSE 0 END
         WHEN true THEN CASE WHEN OM.numassignedto IN(SELECT numContactId FROM AdditionalContactsInformation AC WHERE AC.numDivisionId = v_numUserCntID) THEN 1 ELSE 0 END  END
      WHEN 2 THEN CASE v_bitCommContact WHEN false THEN CASE WHEN OM.numrecowner = v_numUserCntID THEN 1 ELSE 0 END
         WHEN true THEN CASE WHEN OM.numrecowner IN(SELECT numContactId FROM AdditionalContactsInformation AC WHERE AC.numDivisionId = v_numUserCntID) THEN 1 ELSE 0 END END END)
      GROUP BY OBD.numOppBizDocsId HAVING SUM(BDD.monAmount) >= MAX(OBD.monDealAmount))
   ORDER BY  oppBiz.numOppBizDocsId desc;
END; $$;












