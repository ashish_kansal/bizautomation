-- Stored procedure definition script USP_Item_GetAttributes for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Item_GetAttributes(v_numDomainID NUMERIC(18,0)
	,v_numItemCode NUMERIC(18,0)
	,v_numItemGroup NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		CFW_Fld_Master.FLd_label
		,CFW_Fld_Master.Fld_id 
		,CFW_Fld_Master.fld_type
		,coalesce(CFW_Fld_Master.bitAutocomplete,false) AS bitAutocomplete
		,CFW_Fld_Master.numlistid
		,CFW_Fld_Master.vcURL
		,ItemAttributes.Fld_Value
		,COUNT(LD.numListItemID) AS numVariations
   FROM
   CFW_Fld_Master
   INNER JOIN
   ItemGroupsDTL
   ON
   CFW_Fld_Master.Fld_id = ItemGroupsDTL.numOppAccAttrID
   AND ItemGroupsDTL.tintType = 2
   LEFT JOIN
   ItemAttributes
   ON
   CFW_Fld_Master.Fld_id = ItemAttributes.Fld_ID
   AND ItemAttributes.numItemCode = v_numItemCode
   LEFT JOIN
   Listdetails LD
   ON
   CFW_Fld_Master.numlistid = LD.numListID
   WHERE
   CFW_Fld_Master.numDomainID = v_numDomainID
   AND ItemGroupsDTL.numItemGroupID = v_numItemGroup
   GROUP BY
   CFW_Fld_Master.FLd_label,CFW_Fld_Master.Fld_id,CFW_Fld_Master.fld_type,
   CFW_Fld_Master.bitAutocomplete,CFW_Fld_Master.numlistid,CFW_Fld_Master.vcURL,
   ItemAttributes.Fld_Value;
   RETURN;
END; $$;













