-- Function definition script GetFQDate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetFQDate(v_date TIMESTAMP,v_quarter SMALLINT,v_Period CHAR(1) DEFAULT 'S',v_numDomainId NUMERIC(4,0) DEFAULT NULL)
RETURNS TIMESTAMP LANGUAGE plpgsql
   AS $$
   DECLARE
   v_year  NUMERIC(4,0);
   v_PeriodDate  TIMESTAMP;
BEGIN
   v_year := GetFiscalyear(v_date,v_numDomainId); 
  
  
   IF
   v_Period = 'S' then
      v_PeriodDate := CASE v_quarter
      WHEN 1 THEN GetFiscalStartDate(v_year::INTEGER,v_numDomainId)+INTERVAL '0 month'
      WHEN 2 THEN GetFiscalStartDate(v_year::INTEGER,v_numDomainId)+INTERVAL '3 month'
      WHEN 3 THEN GetFiscalStartDate(v_year::INTEGER,v_numDomainId)+INTERVAL '6 month'
      WHEN 4 THEN GetFiscalStartDate(v_year::INTEGER,v_numDomainId)+INTERVAL '9 month'
      end;
   else
      v_PeriodDate := CASE v_quarter
      WHEN 1 THEN GetFiscalStartDate(v_year::INTEGER,v_numDomainId)+INTERVAL '3 month'+INTERVAL '-1 day'
      WHEN 2 THEN GetFiscalStartDate(v_year::INTEGER,v_numDomainId)+INTERVAL '6 month'+INTERVAL '-1 day'
      WHEN 3 THEN  GetFiscalStartDate(v_year::INTEGER,v_numDomainId)+INTERVAL '9 month'+INTERVAL '-1 day'
      WHEN 4 THEN  GetFiscalStartDate(v_year::INTEGER,v_numDomainId)+INTERVAL '12 month'+INTERVAL '-1 day'
      End;
   end if;
	
  
 
   return v_PeriodDate;
END; $$;

