DROP FUNCTION IF EXISTS USP_WarehouseItems_Save;

CREATE OR REPLACE FUNCTION USP_WarehouseItems_Save(v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0),
	INOUT v_numWareHouseItemID NUMERIC(18,0) ,
	v_numWareHouseID NUMERIC(18,0),
	v_numWLocationID NUMERIC(18,0),
	v_numItemCode NUMERIC(18,0),
	v_vcLocation VARCHAR(250),
	v_monWListPrice DECIMAL(20,5),
	v_numOnHand DOUBLE PRECISION,
	v_numReorder DOUBLE PRECISION,
	v_vcWHSKU VARCHAR(100),
	v_vcBarCode VARCHAR(100),
	v_strFieldList TEXT,
	v_vcSerialLotNo TEXT,
	v_bitCreateGlobalLocation BOOLEAN DEFAULT false)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitLotNo  BOOLEAN DEFAULT false;  
   v_bitSerialized  BOOLEAN DEFAULT false;
   v_bitMatrix  BOOLEAN DEFAULT false;
   v_numItemGroup  NUMERIC(18,0);
   v_vcDescription  VARCHAR(100);
   v_numGlobalWarehouseItemID  NUMERIC(18,0) DEFAULT 0;
   v_monPrice  DECIMAL(20,5);
   v_vcSKU  VARCHAR(200) DEFAULT '';
   v_vcUPC  VARCHAR(200) DEFAULT '';
   v_bitRemoveGlobalLocation  BOOLEAN;

   v_dtDATE  TIMESTAMP DEFAULT TIMEZONE('UTC',now());

   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;

	-- SAVE SERIAL/LOT# 
   v_bitDuplicateLot  BOOLEAN;
   v_idoc  INTEGER;
   v_numTotalQty  INTEGER;
 
   SWV_RCur REFCURSOR;
   SWV_RCur2 REFCURSOR;
BEGIN
   -- BEGIN TRANSACTION
select   coalesce(Item.bitLotNo,false), coalesce(Item.bitSerialized,false), coalesce(Item.numItemGroup,0), coalesce(Item.monListPrice,0), coalesce(bitMatrix,false), coalesce(vcSKU,''), coalesce(numBarCodeId,''), coalesce(Domain.bitRemoveGlobalLocation,false) INTO v_bitLotNo,v_bitSerialized,v_numItemGroup,v_monPrice,v_bitMatrix,v_vcSKU,
   v_vcUPC,v_bitRemoveGlobalLocation FROM
   Item
   INNER JOIN
   Domain
   ON
   Item.numDomainID = Domain.numDomainId WHERE
   numItemCode = v_numItemCode
   AND Item.numDomainID = v_numDomainID;
   IF coalesce(v_numWareHouseItemID,0) = 0 then
	 --INSERT
      IF coalesce(v_numWareHouseID,0) > 0 AND coalesce(v_numWLocationID,0) > 0 AND EXISTS(SELECT numWareHouseItemID FROM WareHouseItems WHERE numItemID = v_numItemCode AND coalesce(numWareHouseID,0) = v_numWareHouseID AND coalesce(numWLocationID,0) = 0 AND coalesce(numOnHand,0) > 0) then
		
         RAISE EXCEPTION 'QTY_EXISTS_IN_EXTERNAL_LOCATION';
         RETURN;
      end if;
      INSERT INTO WareHouseItems(numItemID,
			numWareHouseID,
			numWLocationID,
			numOnHand,
			numReorder,
			monWListPrice,
			vcLocation,
			vcWHSKU,
			vcBarCode,
			numDomainID,
			dtModified)
		VALUES(v_numItemCode,
			v_numWareHouseID,
			v_numWLocationID,
			(CASE WHEN v_bitLotNo = true OR v_bitSerialized = true THEN 0 ELSE v_numOnHand END),
			v_numReorder,
			v_monPrice,
			v_vcLocation,
			(CASE WHEN v_numItemGroup > 0 AND coalesce(v_bitMatrix, false) = true THEN v_vcSKU ELSE v_vcWHSKU END),
			(CASE WHEN v_numItemGroup > 0 AND coalesce(v_bitMatrix, false) = true THEN v_vcUPC ELSE v_vcBarCode END),
			v_numDomainID,
			LOCALTIMESTAMP) RETURNING numWareHouseItemID INTO v_numWareHouseItemID;

		-- WAREHOUSE LOCATION LEVEL DIFFERENT ITEM PRICE IS AVAILABLE FOR MATRIX ITEM ONLY
      IF v_numItemGroup > 0 AND coalesce(v_bitMatrix,false) = false then
		
         UPDATE WareHouseItems SET monWListPrice = v_monWListPrice WHERE numDomainID = v_numDomainID AND numItemID = v_numItemCode AND numWareHouseItemID = v_numWareHouseItemID;
      end if;
      v_vcDescription := 'INSERT WareHouse';
   ELSE --UPDATE
      UPDATE
      WareHouseItems
      SET
      numReorder = v_numReorder,vcLocation = v_vcLocation,vcWHSKU =(CASE WHEN v_numItemGroup > 0 AND coalesce(v_bitMatrix,false) = true THEN v_vcSKU ELSE v_vcWHSKU END),
      vcBarCode =(CASE WHEN v_numItemGroup > 0 AND coalesce(v_bitMatrix,false) = true THEN v_vcUPC ELSE v_vcBarCode END),dtModified = LOCALTIMESTAMP
      WHERE
      numItemID = v_numItemCode
      AND numDomainID = v_numDomainID
      AND numWareHouseItemID = v_numWareHouseItemID;

		-- WAREHOUSE LOCATION LEVEL DIFFERENT ITEM PRICE IS AVAILABLE FOR MATRIX ITEM ONLY
      IF v_numItemGroup > 0 AND coalesce(v_bitMatrix,false) = false then
		
         UPDATE WareHouseItems SET monWListPrice = v_monWListPrice WHERE numDomainID = v_numDomainID AND numItemID = v_numItemCode AND numWareHouseItemID = v_numWareHouseItemID;
      end if;
      v_vcDescription := 'UPDATE WareHouse';
   end if;

	-- SAVE MATRIX ITEM ATTRIBUTES
   IF OCTET_LENGTH(v_strFieldList) > 2 then
	
		--INSERT CUSTOM FIELDS BASE ON WAREHOUSEITEMS

      BEGIN
         CREATE TEMP SEQUENCE tt_tempTable_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPTABLE
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         Fld_ID NUMERIC(18,0),
         Fld_Value VARCHAR(300)
      );
      INSERT INTO tt_TEMPTABLE(Fld_ID,
			Fld_Value)
      SELECT Fld_ID,Fld_Value FROM
	   XMLTABLE
		(
			'NewDataSet/CusFlds'
			PASSING 
				CAST(v_strFieldList AS XML)
			COLUMNS
				id FOR ORDINALITY,
				Fld_ID NUMERIC(18,0) PATH 'Fld_ID',
				Fld_Value VARCHAR(300) PATH 'Fld_Value'
		) AS X;

      IF(SELECT COUNT(*) FROM tt_TEMPTABLE) > 0 then
		
         DELETE FROM CFW_Fld_Values_Serialized_Items C using tt_TEMPTABLE T where C.Fld_ID = T.Fld_ID AND C.RecId = v_numWareHouseItemID;
         INSERT INTO CFW_Fld_Values_Serialized_Items(Fld_ID,
				Fld_Value,
				RecId,
				bitSerialized)
         SELECT
         Fld_ID,
				coalesce(Fld_Value,'') AS Fld_Value,
				v_numWareHouseItemID,
				false
         FROM
         tt_TEMPTABLE;
         IF coalesce(v_bitMatrix,false) = true then
			
            DELETE FROM CFW_Fld_Values_Serialized_Items C using tt_TEMPTABLE T where C.Fld_ID = T.Fld_ID AND C.RecId = v_numGlobalWarehouseItemID;
            INSERT INTO CFW_Fld_Values_Serialized_Items(Fld_ID,
					Fld_Value,
					RecId,
					bitSerialized)
            SELECT
            Fld_ID,
					coalesce(Fld_Value,'') AS Fld_Value,
					v_numGlobalWarehouseItemID,
					false
            FROM
            tt_TEMPTABLE;
         end if;
      end if;

   end if;	  

	-- SAVE SERIAL/LOT# 
   IF (v_bitSerialized = true OR v_bitLotNo = true) AND OCTET_LENGTH(v_vcSerialLotNo) > 0 then
      SELECT * INTO SWV_RCur2 FROM USP_WareHouseItmsDTL_CheckForDuplicate(v_numDomainID,v_numWareHouseID,v_numWLocationID,v_numItemCode,v_bitSerialized,v_vcSerialLotNo,SWV_RCur);
	  FETCH SWV_RCur2 INTO v_bitDuplicateLot;

      IF v_bitDuplicateLot = true then
		
         RAISE EXCEPTION 'DUPLICATE_SERIALLOT';
      end if;
      DROP TABLE IF EXISTS tt_TEMPSERIALLOT CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPSERIALLOT
      (
         vcSerialLot VARCHAR(100),
         numQty INTEGER,
         dtExpirationDate TIMESTAMP
      );

      INSERT INTO tt_TEMPSERIALLOT
      SELECT
		vcSerialLot,
		numQty,
		(CASE WHEN dtExpirationDate = '' THEN NULL::TIMESTAMP ELSE NULLIF(dtExpirationDate,'1900-01-01 00:00:00.000')::TIMESTAMP END)
      FROM
		XMLTABLE
		(
			'SerialLots/SerialLot'
			PASSING 
				CAST(v_vcSerialLotNo AS XML)
			COLUMNS
				vcSerialLot TEXT PATH 'vcSerialLot',
				numQty NUMERIC PATH 'numQty',
				dtExpirationDate TEXT PATH 'dtExpirationDate' DEFAULT NULL
		) AS X;

      INSERT INTO WareHouseItmsDTL(numWareHouseItemID,
			vcSerialNo,
			numQty,
			dExpirationDate,
			bitAddedFromPO)
      SELECT
      v_numWareHouseItemID,
			vcSerialLot,
			numQty,
			dtExpirationDate,
			false
      FROM
      tt_TEMPSERIALLOT;
      select   SUM(numQty) INTO v_numTotalQty FROM tt_TEMPSERIALLOT;
      UPDATE WareHouseItems SET numOnHand = coalesce(numOnHand,0)+v_numTotalQty WHERE numWareHouseItemID = v_numWareHouseItemID;
      v_vcDescription := CONCAT(coalesce(v_vcDescription,'') || ' (Serial/Lot# Qty Added:',v_numTotalQty,
      ')');
   end if;
   PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numWareHouseItemID,v_numReferenceID := v_numItemCode,
   v_tintRefType := 1::SMALLINT,v_vcDescription := v_vcDescription,
   v_numModifiedBy := v_numUserCntID,v_ClientTimeZoneOffset := 0,v_dtRecordDate := v_dtDATE,
   v_numDomainID := v_numDomainID,SWV_RefCur := null);
   IF coalesce(v_numGlobalWarehouseItemID,0) > 0 then
	
      PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numGlobalWarehouseItemID,v_numReferenceID := v_numItemCode,
      v_tintRefType := 1::SMALLINT,v_vcDescription := v_vcDescription,
      v_numModifiedBy := v_numUserCntID,v_ClientTimeZoneOffset := 0,
      v_dtRecordDate := v_dtDATE,v_numDomainID := v_numDomainID,SWV_RefCur := null);
   end if;
   UPDATE Item SET bintModifiedDate = v_dtDATE,numModifiedBy = v_numUserCntID WHERE numItemCode = v_numItemCode AND numDomainID = v_numDomainID;
   -- COMMIT
EXCEPTION WHEN OTHERS THEN
      -- ROLLBACK TRANSACTION
v_numWareHouseItemID := 0;
      GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
END; $$;



