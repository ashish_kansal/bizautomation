-- Stored procedure definition script USP_ShippingReport_UpdateTrackingDetail for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ShippingReport_UpdateTrackingDetail(v_numShippingReportID NUMERIC(18,0)
	,v_bitDelivered BOOLEAN
	,v_vcTrackingDetail VARCHAR(300))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcCurrentTrackingDetail  VARCHAR(300);
   v_dtLastTrackingUpdate  TIMESTAMP;
BEGIN
   IF EXISTS(SELECT numShippingReportID FROM ShippingReport WHERE numShippingReportID = v_numShippingReportID) then
	
      select   coalesce(vcTrackingDetail,''), coalesce(dtLastTrackingUpdate,TIMEZONE('UTC',now())) INTO v_vcCurrentTrackingDetail,v_dtLastTrackingUpdate FROM
      ShippingReport WHERE
      numShippingReportID = v_numShippingReportID;
      IF v_vcCurrentTrackingDetail = coalesce(v_vcTrackingDetail,'') AND DATE_PART('day',TIMEZONE('UTC',now()):: timestamp -v_dtLastTrackingUpdate) > 3 then
		
         v_bitDelivered  := true;
         v_vcTrackingDetail := '';
      end if;
      UPDATE
      ShippingReport
      SET
      bitDelivered = v_bitDelivered,vcTrackingDetail = v_vcTrackingDetail,dtLastTrackingUpdate = TIMEZONE('UTC',now())
      WHERE
      numShippingReportID = v_numShippingReportID;
   end if;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_ShoppingCart]    Script Date: 05/07/2009 22:09:30 ******/



