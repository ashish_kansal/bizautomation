-- Stored procedure definition script USP_GetOPPPro for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetOPPPro(v_numDomainId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	select 
		CAST(numOppId AS VARCHAR(15)) || ',O' as ID,vcpOppName 
	from OpportunityMaster 
	where 
		tintopptype = 1 
		and tintoppstatus = 0 AND numDomainId = v_numDomainId
	union 
	select 
		CAST(numProId AS VARCHAR(15)) || ',P',cast(vcProjectName as VARCHAR(100)) 
	from  
		ProjectsMaster 
	where 
		tintProStatus = 0 AND numdomainId = v_numDomainId;
   RETURN;
END; $$;












