-- Stored procedure definition script usp_ManageContInfo for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_ManageContInfo(v_numContactId NUMERIC DEFAULT 0,                  
 v_vcDepartment NUMERIC(9,0) DEFAULT 0,                  
 v_vcFirstName VARCHAR(50) DEFAULT '',                  
 v_vcLastName VARCHAR(50) DEFAULT '',                  
 v_vcEmail VARCHAR(50) DEFAULT '',                  
 v_vcSex CHAR(1) DEFAULT '',                  
 v_vcPosition  NUMERIC(9,0) DEFAULT 0,                  
 v_numPhone VARCHAR(15) DEFAULT '',                  
 v_numPhoneExtension VARCHAR(7) DEFAULT '',                  
 v_bintDOB TIMESTAMP DEFAULT null,                  
 v_txtNotes TEXT DEFAULT '',                  
 v_numUserCntID NUMERIC DEFAULT 0,                      
 v_numCategory NUMERIC DEFAULT 0,                   
 v_numCell VARCHAR(15) DEFAULT '',                  
 v_NumHomePhone VARCHAR(15) DEFAULT '',                  
 v_vcFax VARCHAR(15) DEFAULT '',                  
 v_vcAsstFirstName VARCHAR(50) DEFAULT '',                  
 v_vcAsstLastName VARCHAR(50) DEFAULT '',                  
 v_numAsstPhone VARCHAR(15) DEFAULT '',                  
 v_numAsstExtn VARCHAR(6) DEFAULT '',                  
 v_vcAsstEmail VARCHAR(50) DEFAULT '',                      
 v_numContactType NUMERIC DEFAULT 0 ,               
 v_bitOptOut BOOLEAN DEFAULT false,                
 v_numManagerId NUMERIC DEFAULT 0,                
 v_numTeam NUMERIC(9,0) DEFAULT 0,                
 v_numEmpStatus NUMERIC(9,0) DEFAULT 0,        
 v_vcAltEmail VARCHAR(100) DEFAULT NULL,        
 v_vcTitle VARCHAR(100) DEFAULT NULL,
 v_numECampaignID NUMERIC(9,0) DEFAULT NULL,
 v_bitSelectiveUpdate BOOLEAN DEFAULT false, --Update fields whose value are supplied
 v_bitPrimaryContact BOOLEAN DEFAULT false,
 v_vcTaxID VARCHAR(100) DEFAULT '')
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDivisionID  NUMERIC(9,0);            
   v_Date  TIMESTAMP;
   v_numID  NUMERIC(9,0);            
   v_bitPrimaryContactCheck  BOOLEAN;                 
    
   v_sql  VARCHAR(1000);
   SWV_RowCount INTEGER;
BEGIN
   IF v_bitSelectiveUpdate = false then
 --  numeric(9, 0)
      if v_bintDOB = '1753-01-01 12:00:00 AM' then 
         v_bintDOB := null;
      end if;
      select   numDivisionId INTO v_numDivisionID FROM AdditionalContactsInformation WHERE numContactId = v_numContactId;
      IF v_bitPrimaryContact = true then
         select   numContactId, coalesce(bitPrimaryContact,false) INTO v_numID,v_bitPrimaryContactCheck FROM    AdditionalContactsInformation WHERE   numDivisionId = v_numDivisionID    LIMIT 1;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         IF SWV_RowCount = 0 then
            v_numID := 0;
         end if;
         WHILE v_numID > 0 LOOP
            IF v_bitPrimaryContactCheck = true then
               UPDATE  AdditionalContactsInformation
               SET     bitPrimaryContact = false
               WHERE   numContactId = v_numID;
            end if;
            select   numContactId, coalesce(bitPrimaryContact,false) INTO v_numID,v_bitPrimaryContactCheck FROM    AdditionalContactsInformation WHERE   numDivisionId = v_numDivisionID
            AND numContactId > v_numID    LIMIT 1;
            GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
            IF SWV_RowCount = 0 then
               v_numID := 0;
            end if;
         END LOOP;
      ELSEIF v_bitPrimaryContact = false
      then
    
         IF(SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionId = v_numDivisionID AND numContactId != v_numContactId AND coalesce(bitPrimaryContact,false) = true) = 0 then
            v_bitPrimaryContact := true;
         end if;
      end if;
      UPDATE AdditionalContactsInformation SET
      vcDepartment = v_vcDepartment,vcFirstName = v_vcFirstName,vcLastname = v_vcLastName,
      numPhone = v_numPhone,numPhoneExtension = v_numPhoneExtension,vcEmail = v_vcEmail,
      charSex = v_vcSex,vcPosition = v_vcPosition,bintDOB = v_bintDOB,
      txtNotes = v_txtNotes,numModifiedBy = v_numUserCntID,bintModifiedDate = TIMEZONE('UTC',now()),
      vcCategory = v_numCategory,numCell = v_numCell,numHomePhone = v_NumHomePhone,
      vcFax = v_vcFax,VcAsstFirstName = v_vcAsstFirstName,
      vcAsstLastName = v_vcAsstLastName,numAsstPhone = v_numAsstPhone,numAsstExtn = v_numAsstExtn,
      vcAsstEmail = v_vcAsstEmail,numContactType = v_numContactType,
      bitOptOut = v_bitOptOut,numManagerID = v_numManagerId,numTeam = v_numTeam,
      numEmpStatus = v_numEmpStatus,vcTitle = v_vcTitle,vcAltEmail = v_vcAltEmail,
      numECampaignID = v_numECampaignID,bitPrimaryContact = v_bitPrimaryContact,
      vcTaxID = v_vcTaxID
      WHERE numContactId = v_numContactId;
 
 /*Added by chintan BugID-262*/
      v_Date := GetUTCDateWithoutTime();
	  --  numeric(9, 0)
	 --  numeric(9, 0)
	 --  numeric(9, 0)
	 --  datetime
	 --  bit
      IF v_numECampaignID = -1 then
         v_numECampaignID := 0;
      end if;
      PERFORM USP_ManageConEmailCampaign(v_numConEmailCampID := 0,v_numContactId := v_numContactId,v_numECampaignID := v_numECampaignID,
      v_intStartDate := v_Date,v_bitEngaged := true,
      v_numUserCntID := v_numUserCntID);
   end if;


   IF v_bitSelectiveUpdate = true then
      v_sql := 'update AdditionalContactsInformation Set ';
      IF(LENGTH(v_vcFirstName) > 0) then
		
         v_sql := coalesce(v_sql,'') || ' vcFirstName=''' || coalesce(v_vcFirstName,'') || ''', ';
      end if;
      IF(LENGTH(v_vcLastName) > 0) then
		
         v_sql := coalesce(v_sql,'') || ' vcLastName=''' || coalesce(v_vcLastName,'') || ''', ';
      end if;
      IF(LENGTH(v_vcEmail) > 0) then
		
         v_sql := coalesce(v_sql,'') || ' vcEmail=''' || coalesce(v_vcEmail,'') || ''', ';
      end if;
      IF(LENGTH(v_numPhone) > 0) then
		
         v_sql := coalesce(v_sql,'') || ' numPhone=''' || coalesce(v_numPhone,'') || ''', ';
      end if;
      IF(LENGTH(v_numPhoneExtension) > 0) then
		
         v_sql := coalesce(v_sql,'') || ' numPhoneExtension=''' || coalesce(v_numPhoneExtension,'') || ''', ';
      end if;
      IF(LENGTH(v_numCell) > 0) then
		
         v_sql := coalesce(v_sql,'') || ' numCell=''' || coalesce(v_numCell,'') || ''', ';
      end if;
      IF(LENGTH(v_vcFax) > 0) then
		
         v_sql := coalesce(v_sql,'') || ' vcFax=''' || coalesce(v_vcFax,'') || ''', ';
      end if;
      v_sql := coalesce(v_sql,'') || ' bitOptOut=''' || SUBSTR(CAST(v_bitOptOut AS VARCHAR(10)),1,10) || ''', ';
      IF(v_numUserCntID > 0) then
		
         v_sql := coalesce(v_sql,'') || ' numModifiedBy=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(10)),1,10) || ', ';
      end if;
      v_sql := SUBSTR(v_sql,0,LENGTH(v_sql)) || ' ';
      v_sql := coalesce(v_sql,'') || 'where numContactId= ' || SUBSTR(CAST(v_numContactId AS VARCHAR(10)),1,10);
      RAISE NOTICE '%',v_sql;
      EXECUTE v_sql;
   end if; 
/****** Object:  StoredProcedure [dbo].[USP_ManageDivisions]    Script Date: 07/26/2008 16:19:46 ******/
   RETURN;
END; $$;


