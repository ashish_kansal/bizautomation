-- Stored procedure definition script USP_GetOnlineBillPayeeDetail for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetOnlineBillPayeeDetail(v_numPayeeDetailId NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN

   /* SQLWays Notice: SET TRANSACTION ISOLATION LEVEL READ COMMITTED must be called before procedure call */
-- SET TRANSACTION ISOLATION LEVEL READ COMMITTED
open SWV_RefCur for SELECT
	numPayeeDetailId,
	vcPayeeFIId,
	numBankDetailId,
	vcPayeeFIListID,
	vcPayeeName,
	vcAccount,
	vcAddress1,
	vcAddress2,
	vcAddress3,
	vcCity,
	vcState,
	vcPostalCode,
	vcCountry ,
	vcPhone
   FROM
   OnlineBillPayeeDetails
   WHERE
   numPayeeDetailId = v_numPayeeDetailId AND bitIsActive = true;
END; $$;
 