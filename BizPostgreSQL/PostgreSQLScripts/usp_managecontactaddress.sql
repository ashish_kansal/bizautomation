-- Stored procedure definition script USP_ManageContactAddress for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageContactAddress(v_numContactId NUMERIC DEFAULT 0,            
 v_vcCity VARCHAR(50) DEFAULT '',              
 v_vcStreet VARCHAR(50) DEFAULT '',              
 v_vcState NUMERIC(9,0) DEFAULT NULL,              
 v_vcCountry NUMERIC(9,0) DEFAULT NULL,              
 v_intPostalCode VARCHAR(15) DEFAULT '',            
 v_vcContactLocation VARCHAR(50) DEFAULT '',              
 v_vcCity1 VARCHAR(50) DEFAULT '',              
 v_vcStreet1 VARCHAR(50) DEFAULT '',              
 v_vcState1 NUMERIC(9,0) DEFAULT NULL,              
 v_vcCountry1 NUMERIC(9,0) DEFAULT NULL,              
 v_vcPostalCode1 VARCHAR(15) DEFAULT '',              
 v_vcContactLocation1 VARCHAR(50) DEFAULT '',              
 v_vcCity2 VARCHAR(50) DEFAULT '',              
 v_vcStreet2 VARCHAR(50) DEFAULT '',              
 v_vcState2 NUMERIC(9,0) DEFAULT NULL,              
 v_vcCountry2 NUMERIC(9,0) DEFAULT NULL,              
 v_vcPostalCode2 VARCHAR(15) DEFAULT '',              
 v_vcContactLocation2 VARCHAR(50) DEFAULT '',              
 v_vcCity3 VARCHAR(50) DEFAULT '',              
 v_vcStreet3 VARCHAR(50) DEFAULT '',              
 v_vcState3 NUMERIC(9,0) DEFAULT NULL,              
 v_vcCountry3 NUMERIC(9,0) DEFAULT NULL,              
 v_vcPostalCode3 VARCHAR(15) DEFAULT '',              
 v_vcContactLocation3 VARCHAR(50) DEFAULT '',              
 v_vcCity4 VARCHAR(50) DEFAULT '',              
 v_vcStreet4 VARCHAR(50) DEFAULT '',              
 v_vcState4 NUMERIC(9,0) DEFAULT NULL,              
 v_vcCountry4 NUMERIC(9,0) DEFAULT NULL,              
 v_vcPostalCode4 VARCHAR(15) DEFAULT '',              
 v_vcContactLocation4 VARCHAR(50) DEFAULT '',              
 v_vcPCity VARCHAR(50) DEFAULT '',              
 v_vcPStreet VARCHAR(50) DEFAULT '',              
 v_vcPState NUMERIC(9,0) DEFAULT NULL,              
 v_vcPCountry NUMERIC(9,0) DEFAULT NULL,              
 v_vcPPostalCode VARCHAR(15) DEFAULT '')
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   if exists(select * from contactAddress where numContactID = v_numContactId) then

      UPDATE contactAddress SET
      vcCity = v_vcCity,vcStreet = v_vcStreet,vcState = v_vcState,vcCountry = v_vcCountry,
      intPostalCode = v_intPostalCode,vcContactLocation = v_vcContactLocation,
      vcCity1 = v_vcCity1,vcStreet1 = v_vcStreet1,vcState1 = v_vcState1,vcCountry1 = v_vcCountry1,
      vcPostalCode1 = v_vcPostalCode1,vcContactLocation1 = v_vcContactLocation1,
      vcCity2 = v_vcCity2,vcStreet2 = v_vcStreet2,vcState2 = v_vcState2,
      vcCountry2 = v_vcCountry2,vcPostalCode2 = v_vcPostalCode2,vcContactLocation2 = v_vcContactLocation2,
      vcCity3 = v_vcCity3,vcStreet3 = v_vcStreet3,
      vcState3 = v_vcState3,vcCountry3 = v_vcCountry3,vcPostalCode3 = v_vcPostalCode3,
      vcContactLocation3 = v_vcContactLocation3,vcCity4 = v_vcCity4,vcStreet4 = v_vcStreet4,
      vcState4 = v_vcState4,vcCountry4 = v_vcCountry4,vcPostalCode4 = v_vcPostalCode4,
      vcContactLocation4 = v_vcContactLocation4,vcPCity = v_vcPCity,
      vcPStreet = v_vcPStreet,vcPState = v_vcPState,vcPCountry = v_vcPCountry,
      vcPPostalCode = v_vcPPostalCode
      WHERE numContactID = v_numContactId;
   else
      insert into contactAddress(numContactID,vcCity,
     vcStreet,
     vcState,
     vcCountry,
     intPostalCode,
     vcContactLocation,
     vcCity1,
     vcStreet1,
     vcState1,
     vcCountry1,
     vcPostalCode1,
     vcContactLocation1,
     vcCity2,
     vcStreet2,
     vcState2,
     vcCountry2,
     vcPostalCode2,
     vcContactLocation2,
     vcCity3,
     vcStreet3,
     vcState3,
     vcCountry3,
     vcPostalCode3,
     vcContactLocation3,
     vcCity4,
     vcStreet4,
     vcState4,
     vcCountry4,
     vcPostalCode4,
     vcContactLocation4,
     vcPCity,
     vcPStreet,
     vcPState,
     vcPCountry,
     vcPPostalCode)
     values(v_numContactId,v_vcCity,
     v_vcStreet,
     v_vcState,
     v_vcCountry,
     v_intPostalCode,
     v_vcContactLocation,
     v_vcCity1,
     v_vcStreet1,
     v_vcState1,
     v_vcCountry1,
     v_vcPostalCode1,
     v_vcContactLocation1,
     v_vcCity2,
     v_vcStreet2,
     v_vcState2,
     v_vcCountry2,
     v_vcPostalCode2,
     v_vcContactLocation2,
     v_vcCity3,
     v_vcStreet3,
     v_vcState3,
     v_vcCountry3,
     v_vcPostalCode3,
     v_vcContactLocation3,
     v_vcCity4,
     v_vcStreet4,
     v_vcState4,
     v_vcCountry4,
     v_vcPostalCode4,
     v_vcContactLocation4,
     v_vcPCity,
     v_vcPStreet,
     v_vcPState,
     v_vcPCountry,
     v_vcPPostalCode);
   end if;
   RETURN;
END; $$;


