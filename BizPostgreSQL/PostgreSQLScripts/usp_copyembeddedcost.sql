-- Stored procedure definition script USP_CopyEmbeddedCost for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
-- EXEC USP_CopyEmbeddedCost 12003,12002,110
CREATE OR REPLACE FUNCTION USP_CopyEmbeddedCost(v_numFromBizDocID NUMERIC,
    v_numToBizDocID NUMERIC,
    v_numDomainID NUMERIC)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numCostCatID  NUMERIC;
   v_numCostCenterID  NUMERIC;
   v_MaxID  NUMERIC;
   v_MinID  NUMERIC;
BEGIN
   INSERT  INTO EmbeddedCost(numOppBizDocID,
                  numCostCatID,
                  numAccountID,
                  numDivisionID,
                  numPaymentMethod,
                  monCost,
                  monActualCost,
                  vcMemo,
                  dtDueDate,
                  numDomainID,
                  numCreatedBy,
                  dtCreatedDate,
                  numModifiedBy,
                  dtModifiedDate,
                  numCostCenterID,
                  numBizDocsPaymentDetId)
   SELECT  v_numToBizDocID,
                        numCostCatID,
                        numAccountID,
                        numDivisionID,
                        numPaymentMethod,
                        monCost,
                        monActualCost,
                        vcMemo,
                        dtDueDate,
                        numDomainID,
                        numCreatedBy,
                        TIMEZONE('UTC',now()),
                        numModifiedBy,
                        TIMEZONE('UTC',now()),
                        numCostCenterID,
                        null
   FROM    EmbeddedCost
   WHERE   numOppBizDocID = v_numFromBizDocID;
                
		
		
   select   MAX(numEmbeddedCostID), MIN(numEmbeddedCostID) INTO v_MaxID,v_MinID FROM EmbeddedCost WHERE numOppBizDocID = v_numToBizDocID;
		
   RAISE NOTICE '%',v_MinID;
   RAISE NOTICE '%',v_MaxID;
   WHILE v_MaxID >= v_MinID LOOP
      select   numCostCenterID, numCostCatID INTO v_numCostCenterID,v_numCostCatID FROM EmbeddedCost EC WHERE numEmbeddedCostID = v_MinID;
      RAISE NOTICE 'costcenter id';
      RAISE NOTICE '%',v_numCostCenterID;
      RAISE NOTICE 'costcat id';
      RAISE NOTICE '%',v_numCostCatID;
      INSERT INTO EmbeddedCostItems(numEmbeddedCostID,
				numOppBizDocItemID,
				monAmount,
				numDomainID)
      SELECT
      v_MinID,
				   (SELECT  numOppBizDocItemID FROM OpportunityBizDocItems WHERE numOppItemID = OBI.numOppItemID AND numOppBizDocID = v_numToBizDocID LIMIT 1),
				   ECI.monAmount,
				   ECI.numDomainID
      FROM EmbeddedCost EC INNER JOIN EmbeddedCostItems ECI ON EC.numEmbeddedCostID = ECI.numEmbeddedCostID
      INNER JOIN OpportunityBizDocItems OBI ON OBI.numOppBizDocItemID = ECI.numOppBizDocItemID
      WHERE EC.numCostCatID = v_numCostCatID
      AND EC.numCostCenterID = v_numCostCenterID
      AND EC.numOppBizDocID = v_numFromBizDocID;
      select   MIN(numEmbeddedCostID) INTO v_MinID FROM EmbeddedCost WHERE numOppBizDocID = v_numToBizDocID
      AND numEmbeddedCostID > v_MinID;
   END LOOP;
   RETURN;
END; $$;


