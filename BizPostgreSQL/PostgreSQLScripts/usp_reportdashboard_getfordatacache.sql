-- Stored procedure definition script USP_ReportDashboard_GetForDataCache for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ReportDashboard_GetForDataCache(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
	ReportDashboard.numDomainID
	,ReportDashboard.numUserCntID
	,ReportDashboard.numDashBoardID
	,ReportDashboard.numReportID
	,coalesce(ReportListMaster.bitDefault,false) AS bitDefault
	,coalesce(ReportListMaster.intDefaultReportID,0) AS intDefaultReportID
	,coalesce(ReportListMaster.tintReportType,0) AS tintReportType
   FROM
   ReportDashboard
   INNER JOIN
   Domain
   ON
   ReportDashboard.numDomainID = Domain.numDomainId
   INNER JOIN
   Subscribers
   ON
   Domain.numDomainId = Subscribers.numTargetDomainID
   INNER JOIN
   ReportListMaster
   ON
   ReportDashboard.numReportID = ReportListMaster.numReportID
   WHERE
   CAST(Subscribers.dtSubEndDate AS DATE) >= CAST(TIMEZONE('UTC',now()) AS DATE)
   AND (EXTRACT(HOUR FROM LOCALTIMESTAMP) >= 0 AND EXTRACT(HOUR FROM LOCALTIMESTAMP) <= 7) -- BETWEEN 0 AM TO 7 AM
   AND (ReportDashboard.dtLastCacheDate IS NULL OR ReportDashboard.dtLastCacheDate <> CAST(TIMEZONE('UTC',now()) AS DATE));
END; $$;












