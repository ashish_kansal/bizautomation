-- Function definition script fn_IsSubTabInGroupTabDtl for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_IsSubTabInGroupTabDtl(v_numModuleId NUMERIC(9,0),
	v_numTabId NUMERIC(9,0))
RETURNS BOOLEAN LANGUAGE plpgsql
	-- Add the parameters for the function here
	
	-- Declare the return variable here
   AS $$
   DECLARE
   v_Result  BOOLEAN;

	-- Add the T-SQL statements to compute the return value here
--    set @Result=0
--	if ( not exists(select numTabId from GroupTabDetails where numModuleId=@numModuleId)) or exists(select numTabId from GroupTabDetails where numModuleId=@numModuleId and numTabId=@numTabId) 
--     begin
--      set @Result=1
--     end
    -- Return the result of the function
BEGIN
   RETURN v_Result;
END; $$;
/****** Object:  UserDefinedFunction [dbo].[fn_MonthName]    Script Date: 07/26/2008 18:12:48 ******/

