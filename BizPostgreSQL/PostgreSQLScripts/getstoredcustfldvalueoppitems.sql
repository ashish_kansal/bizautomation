DROP FUNCTION IF EXISTS GetStoredCustFldValueOppItems;
CREATE OR REPLACE FUNCTION GetStoredCustFldValueOppItems(v_numFldId NUMERIC(18,0),
 v_numRecordId NUMERIC(18,0),
 v_numItemCode NUMERIC(18,0))
RETURNS TEXT LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcValue  TEXT DEFAULT '';
BEGIN
   IF COALESCE(v_numRecordId,0) > 0 AND EXISTS(SELECT Fld_Value FROM CFW_Fld_Values_OppItems WHERE fld_id = v_numFldId  AND RecId = v_numRecordId) then
	
      select   coalesce(Fld_Value,'') INTO v_vcValue FROM
      CFW_Fld_Values_OppItems WHERE
      fld_id = v_numFldId
      AND RecId = v_numRecordId;
   ELSE
      select   coalesce(Fld_Value,'') INTO v_vcValue FROM
      CFW_FLD_Values_Item WHERE
      Fld_ID = v_numFldId
      AND RecId = v_numItemCode;
   end if;

   RETURN v_vcValue;
END; $$;

