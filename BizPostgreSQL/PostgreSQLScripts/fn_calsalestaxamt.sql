-- Function definition script fn_CalSalesTaxAmt for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_CalSalesTaxAmt(v_numOppBizDocID NUMERIC,
      v_numDomainID NUMERIC)
RETURNS DOUBLE PRECISION LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDivisionID  NUMERIC(9,0);
   v_numOppID  NUMERIC(9,0);
   v_numCountry  NUMERIC(9,0);
   v_numState  NUMERIC(9,0);
   v_TaxPercentage  DECIMAL(20,5);
BEGIN
   select   numoppid INTO v_numOppID FROM    OpportunityBizDocs WHERE   numOppBizDocsId = v_numOppBizDocID;

   select   numState, numCountry INTO v_numState,v_numCountry FROM  fn_getTaxableStateCountry(v_numDomainID,v_numOppID)     LIMIT 1;
--             RETURN @numState
--              RETURN @numCountry
   v_TaxPercentage := 0;              
   IF v_numCountry > 0
   AND v_numState > 0 then
        
      select   decTaxPercentage INTO v_TaxPercentage FROM    TaxDetails WHERE   numCountryID = v_numCountry
      AND numStateID = v_numState
      AND numDomainId = v_numDomainID
      and numTaxItemID = 0;
   end if; 
   IF coalesce(v_TaxPercentage,0) = 0 then
    
      select   decTaxPercentage INTO v_TaxPercentage FROM    TaxDetails WHERE   numCountryID = v_numCountry
      AND numStateID = 0
      AND numDomainId = v_numDomainID
      and numTaxItemID = 0;
   end if;    
	
   RETURN coalesce(v_TaxPercentage,0);
END; $$;

