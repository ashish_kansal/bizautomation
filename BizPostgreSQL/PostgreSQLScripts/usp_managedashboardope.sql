-- Stored procedure definition script USP_ManageDashboardOpe for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageDashboardOpe(v_numDomainID NUMERIC(9,0),
v_numDashBoardID NUMERIC(9,0),
v_charOpe CHAR(1),
v_strText TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDocItem  INTEGER;
BEGIN
   IF v_charOpe = 'X' then

      delete FROM ReportDashboard where numDomainID = v_numDomainID AND numDashBoardID = v_numDashBoardID;
   ELSEIF v_charOpe = 'A'
   then
      IF SUBSTR(CAST(v_strText AS VARCHAR(10)),1,10) <> '' then
		UPDATE 
			ReportDashboard 
		SET 
			tintColumn = X.tintColumn
			,tintRow = X.tintRow
			,intHeight = X.intHeight
			,intWidth = X.intWidth
        FROM
		XMLTABLE
		(
			'NewDataSet/Table1'
			PASSING 
				CAST(v_strText AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numDashBoardID NUMERIC(9,0) PATH 'numDashBoardID',
				tintColumn SMALLINT PATH 'tintColumn',
				tintRow SMALLINT PATH 'tintRow',
				intHeight INTEGER PATH 'intHeight',
				intWidth INTEGER PATH 'intWidth'
		) AS X 
         WHERE ReportDashboard.numDomainID = v_numDomainID AND ReportDashboard.numDashBoardID = x.numDashBoardID;

      end if;
   end if;
   RETURN;
END; $$;



