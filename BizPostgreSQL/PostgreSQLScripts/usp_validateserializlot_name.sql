-- Stored procedure definition script usp_ValidateSerializLot_Name for PostgreSQL
CREATE OR REPLACE FUNCTION usp_ValidateSerializLot_Name(v_numItemCode NUMERIC(9,0) DEFAULT 0,
    v_numWarehouseItmsID NUMERIC(9,0) DEFAULT 0,
    v_strItems VARCHAR(3000) DEFAULT '',
    v_bitSerialized BOOLEAN DEFAULT NULL,
    v_bitLotNo BOOLEAN DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_posComma  INTEGER;
   v_strKeyVal  VARCHAR(20);
   v_posBStart  INTEGER;
   v_posBEnd  INTEGER;
   v_strQty  VARCHAR(20);
   v_strName  VARCHAR(20);

   v_AvailableQty  NUMERIC(9,0);
BEGIN
   DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLE
   (
      vcSerialNo VARCHAR(100),
      TotalQty NUMERIC(18,0),
      numWarehouseItmsDTLID NUMERIC(18,0)
   );                       

   INSERT INTO tt_TEMPTABLE(vcSerialNo,TotalQty,numWarehouseItmsDTLID)
   SELECT
   vcSerialNo,numWareHouseItmsDTLID,coalesce(numQty,0) AS TotalQty
   FROM
   WareHouseItmsDTL
   WHERE
   numWareHouseItemID = v_numWarehouseItmsID
   ORDER BY
   vcSerialNo,TotalQty desc;

   DROP TABLE IF EXISTS tt_TEMPERROR CASCADE;
   Create TEMPORARY TABLE tt_TEMPERROR
   (
      vcSerialNo VARCHAR(100),
      UsedQty NUMERIC(18,0),
      AvailableQty NUMERIC(18,0),
      vcError VARCHAR(100)
   );                       

  
   v_strItems := RTRIM(v_strItems);
   IF SUBSTR(v_strItems,length(v_strItems) -1+1) != ',' then 
      v_strItems := coalesce(v_strItems,'') || ',';
   end if;

   v_posComma := coalesce(POSITION(substring(v_strItems from E'\\,') IN v_strItems),0);
   WHILE v_posComma > 1 LOOP
      v_strKeyVal := ltrim(rtrim(SUBSTR(v_strItems,1,v_posComma -1)));
      IF v_bitLotNo = true then
    
         v_posBStart := coalesce(POSITION(substring(v_strKeyVal from E'\\(') IN v_strKeyVal),0);
         v_posBEnd := coalesce(POSITION(substring(v_strKeyVal from E'\\)') IN v_strKeyVal),0);
         IF(v_posBStart > 1 AND v_posBEnd > 1) then
		
            v_strName := LTRIM(RTRIM(SUBSTR(v_strKeyVal,1,v_posBStart -1)));
            v_strQty := LTRIM(RTRIM(SUBSTR(v_strKeyVal,v_posBStart+1,LENGTH(v_strKeyVal) -v_posBStart -1)));
         ELSE
            v_strName := v_strKeyVal;
            v_strQty := CAST(1 AS VARCHAR(20));
         end if;
      ELSE
         v_strName := v_strKeyVal;
         v_strQty := CAST(1 AS VARCHAR(20));
      end if;
      IF NOT EXISTS(SELECT 1 FROM tt_TEMPTABLE WHERE vcSerialNo = v_strName) then
	 
         INSERT INTO tt_TEMPERROR(vcSerialNo,UsedQty,AvailableQty,vcError) VALUES(CAST(v_strName AS VARCHAR(100)),CAST(v_strQty AS NUMERIC(18,0)),0,CAST('Serial / Lot # not available' AS VARCHAR(100)));
      ELSE
         select   TotalQty INTO v_AvailableQty FROM tt_TEMPTABLE WHERE vcSerialNo = v_strName    LIMIT 1;
         IF  (cast(NULLIF(v_strQty,'') as NUMERIC(9,0)) > v_AvailableQty) then
            INSERT INTO tt_TEMPERROR(vcSerialNo,UsedQty,AvailableQty,vcError) VALUES(CAST(v_strName AS VARCHAR(100)),CAST(v_strQty AS NUMERIC(18,0)),v_AvailableQty,CAST('Used Serial / Lot # more than Available Serial / Lot #' AS VARCHAR(100)));
         end if;
      end if;
      v_strItems := SUBSTR(v_strItems,v_posComma+1,LENGTH(v_strItems) -v_posComma);
      v_posComma := coalesce(POSITION(substring(v_strItems from E'\\,') IN v_strItems),0);
   END LOOP;
--SELECT * FROM #tempTable

   open SWV_RefCur for SELECT * FROM tt_TEMPERROR;

   DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
   RETURN;
END; $$;












