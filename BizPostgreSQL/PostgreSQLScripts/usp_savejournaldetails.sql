DROP FUNCTION IF EXISTS USP_SaveJournalDetails;

CREATE OR REPLACE FUNCTION USP_SaveJournalDetails(v_numJournalId NUMERIC(9,0) DEFAULT 0 ,
    v_strRow TEXT DEFAULT '' ,
    v_Mode SMALLINT DEFAULT 0 ,
    v_numDomainId NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numBaseCurrencyID  NUMERIC;
   v_strMsg  VARCHAR(200);

   v_numEntryDateSortOrder  NUMERIC;
   v_datEntry_Date  TIMESTAMP;
		--Combine Date from datentryDate with time part of current time
   v_numDivisionID  NUMERIC(18,0);
   v_numUserCntID  NUMERIC(18,0);
	   
   v_SumTotal  DECIMAL(20,5);
    my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;


   v_numAccountClass  NUMERIC(18,0) DEFAULT 0;
BEGIN
	-- INCLUDE ENCODING OTHER WISE IT WILL THROW ERROR WHEN SPECIAL CHARACTERS ARE AVAILABLE IN DESCRIPT FIELDS
   IF coalesce(SUBSTR(CAST(v_strRow AS VARCHAR(30)),1,30),'') <> '' AND POSITION('<?xml' IN v_strRow) = 0 then
	
      v_strRow := CONCAT('<?xml version="1.0" encoding="ISO-8859-1"?>',v_strRow);
   end if;
   -- BEGIN TRAN
  
        
    ---Always set default Currency ID as base currency ID of domain, and Rate =1
    select   coalesce(numCurrencyID,0) INTO v_numBaseCurrencyID FROM Domain WHERE numDomainId = v_numDomainId;
   IF SUBSTR(CAST(v_strRow AS VARCHAR(100)),1,100) <> '' then

      DROP TABLE IF EXISTS tt_TEMPSaveJournalDetails CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPSaveJournalDetails
      (
         numTransactionId NUMERIC(10,0),
         numDebitAmt DOUBLE PRECISION,
         numCreditAmt DOUBLE PRECISION,
         numChartAcntId NUMERIC(18,0),
         varDescription VARCHAR(1000),
         numCustomerId NUMERIC(18,0),
         bitMainDeposit BOOLEAN,
         bitMainCheck BOOLEAN,
         bitMainCashCredit BOOLEAN,
         numoppitemtCode NUMERIC(18,0),
         chBizDocItems CHAR(10),
         vcReference VARCHAR(500),
         numPaymentMethod NUMERIC(18,0),
         bitReconcile BOOLEAN,
         numCurrencyID NUMERIC(18,0),
         fltExchangeRate DOUBLE PRECISION,
         numTaxItemID NUMERIC(18,0),
         numBizDocsPaymentDetailsId NUMERIC(18,0),
         numContactID NUMERIC(9,0),
         numItemID NUMERIC(18,0),
         numProjectID NUMERIC(18,0),
         numClassID NUMERIC(18,0),
         numCommissionID NUMERIC(9,0),
         numReconcileID NUMERIC(18,0),
         bitCleared BOOLEAN,
         tintReferenceType SMALLINT,
         numReferenceID NUMERIC(18,0),
         numCampaignID NUMERIC(18,0)
      );
      INSERT INTO tt_TEMPSaveJournalDetails(numTransactionId
			,numDebitAmt
			,numCreditAmt
			,numChartAcntId
			,varDescription
			,numCustomerId
			,bitMainDeposit
			,bitMainCheck
			,bitMainCashCredit
			,numoppitemtCode
			,chBizDocItems
			,vcReference
			,numPaymentMethod
			,bitReconcile
			,numCurrencyID
			,fltExchangeRate
			,numTaxItemID
			,numBizDocsPaymentDetailsId
			,numContactID
			,numItemID
			,numProjectID
			,numClassID
			,numCommissionID
			,numReconcileID
			,bitCleared
			,tintReferenceType
			,numReferenceID
			,numCampaignID)
      SELECT
      numTransactionId
			,numDebitAmt
			,numCreditAmt
			,numChartAcntId
			,varDescription
			,numCustomerId
			,bitMainDeposit
			,bitMainCheck
			,bitMainCashCredit
			,numoppitemtCode
			,chBizDocItems
			,vcReference
			,numPaymentMethod
			,bitReconcile
			,coalesce(NULLIF(numCurrencyID,0),v_numBaseCurrencyID) AS numCurrencyID
			,coalesce(NULLIF(fltExchangeRate,0.0000),1) AS fltExchangeRate
			,numTaxItemID
			,numBizDocsPaymentDetailsId
			,numcontactid
			,numItemID
			,numProjectID
			,numClassID
			,numCommissionID
			,numReconcileID
			,bitCleared
			,tintReferenceType
			,numReferenceID
			,numCampaignID
      FROM
	  XMLTABLE
		(
			'ArrayOfJournalEntryNew/JournalEntryNew'
			PASSING 
				CAST(v_strRow AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numTransactionId NUMERIC(10,0) PATH 'numTransactionId',
				numDebitAmt DOUBLE PRECISION PATH 'numDebitAmt',
				numCreditAmt DOUBLE PRECISION PATH 'numCreditAmt',
				numChartAcntId NUMERIC(18,0) PATH 'numChartAcntId',
				varDescription VARCHAR(1000) PATH 'varDescription',
				numCustomerId NUMERIC(18,0) PATH 'numCustomerId',
				bitMainDeposit BOOLEAN PATH 'bitMainDeposit',
				bitMainCheck BOOLEAN PATH 'bitMainCheck',
				bitMainCashCredit BOOLEAN PATH 'bitMainCashCredit',
				numoppitemtCode NUMERIC(18,0) PATH 'numoppitemtCode',
				chBizDocItems CHAR(10) PATH 'chBizDocItems',
				vcReference VARCHAR(500) PATH 'vcReference',
				numPaymentMethod NUMERIC(18,0) PATH 'numPaymentMethod',
				bitReconcile BOOLEAN PATH 'bitReconcile',
				numCurrencyID NUMERIC(18,0) PATH 'numCurrencyID',
				fltExchangeRate DOUBLE PRECISION PATH 'fltExchangeRate',
				numTaxItemID NUMERIC(18,0) PATH 'numTaxItemID',
				numBizDocsPaymentDetailsId NUMERIC(18,0) PATH 'numBizDocsPaymentDetailsId',
				numcontactid NUMERIC(9,0) PATH 'numcontactid',
				numItemID NUMERIC(18,0) PATH 'numItemID',
				numProjectID NUMERIC(18,0) PATH 'numProjectID',
				numClassID NUMERIC(18,0) PATH 'numClassID',
				numCommissionID NUMERIC(9,0) PATH 'numCommissionID',
				numReconcileID NUMERIC(18,0) PATH 'numReconcileID',
				bitCleared BOOLEAN PATH 'bitCleared',
				tintReferenceType SMALLINT PATH 'tintReferenceType',
				numReferenceID NUMERIC(18,0) PATH 'numReferenceID',
				numCampaignID NUMERIC(18,0) PATH 'numCampaignID'
		) AS X;

                         

/*-----------------------Validation of balancing entry*/

      IF EXISTS(SELECT * FROM tt_TEMPSaveJournalDetails) then
         select   CAST(SUM(numDebitAmt) AS DECIMAL(20,5)) -CAST(SUM(numCreditAmt) AS DECIMAL(20,5)) INTO v_SumTotal FROM tt_TEMPSaveJournalDetails;
         IF cast(v_SumTotal AS DECIMAL(10,4)) <> 0.0000 then
			
            INSERT INTO GenealEntryAudit(numDomainID
				   ,numJournalID
				   ,numTransactionID
				   ,dtCreatedDate
				   ,varDescription)
            SELECT v_numDomainId,
						v_numJournalId,
						0,
						TIMEZONE('UTC',now()),
						'IM_BALANCE';
            RAISE EXCEPTION 'IM_BALANCE';
            RETURN;
         end if;
      end if;
      select(CAST(CAST(EXTRACT(YEAR FROM datEntry_Date) AS VARCHAR(30)) || '-' || SUBSTR('00000' || CAST(EXTRACT(MONTH FROM datEntry_Date) AS VARCHAR(30)),length('00000' || CAST(EXTRACT(MONTH FROM datEntry_Date) AS VARCHAR(30))) -2+1) ||
      '-' || CAST(EXTRACT(DAY FROM datEntry_Date) AS VARCHAR(30))
      || ' ' || CAST(EXTRACT(HOUR FROM LOCALTIMESTAMP) AS VARCHAR(30))
      || ':' || CAST(EXTRACT(MINUTE FROM LOCALTIMESTAMP) AS VARCHAR(30))
      || ':' || CAST(EXTRACT(SECOND FROM LOCALTIMESTAMP) AS VARCHAR(30)) AS TIMESTAMP)) INTO v_datEntry_Date FROM
      General_Journal_Header WHERE
      numJOurnal_Id = v_numJournalId
      AND numDomainId = v_numDomainId;
      v_numEntryDateSortOrder := CAST(SUBSTR('00000' || CAST(EXTRACT(YEAR FROM v_datEntry_Date) AS VARCHAR(30)),length('00000' || CAST(EXTRACT(YEAR FROM v_datEntry_Date) AS VARCHAR(30))) -4+1) ||
      SUBSTR('00000' || CAST(EXTRACT(MONTH FROM v_datEntry_Date) AS VARCHAR(30)),
      length('00000' || CAST(EXTRACT(MONTH FROM v_datEntry_Date) AS VARCHAR(30))) -2+1) ||
      SUBSTR('00000' || CAST(EXTRACT(DAY FROM v_datEntry_Date) AS VARCHAR(30)),length('00000' || CAST(EXTRACT(DAY FROM v_datEntry_Date) AS VARCHAR(30))) -2+1) ||
      SUBSTR('00000' || CAST(EXTRACT(HOUR FROM v_datEntry_Date) AS VARCHAR(30)),length('00000' || CAST(EXTRACT(HOUR FROM v_datEntry_Date) AS VARCHAR(30))) -2+1) ||
      SUBSTR('00000' || CAST(EXTRACT(MINUTE FROM v_datEntry_Date) AS VARCHAR(30)),
      length('00000' || CAST(EXTRACT(MINUTE FROM v_datEntry_Date) AS VARCHAR(30))) -2+1) ||
      SUBSTR('00000' || CAST(EXTRACT(SECOND FROM v_datEntry_Date) AS VARCHAR(30)),
      length('00000' || CAST(EXTRACT(SECOND FROM v_datEntry_Date) AS VARCHAR(30))) -2+1) ||
      SUBSTR('00000' || CAST(CAST(TO_CHAR(v_datEntry_Date,'MS') AS BIGINT) AS VARCHAR(30)),length('00000' || CAST(CAST(TO_CHAR(v_datEntry_Date,'MS') AS BIGINT) AS VARCHAR(30))) -3+1) AS NUMERIC);

		/*-----------------------Validation of balancing entry*/

			

      IF v_Mode = 1 then  /*Edit Journal entries*/
        
         DELETE FROM General_Journal_Details GJD WHERE numJournalId = v_numJournalId  AND NOT EXiSTS (SELECT numTransactionId FROM tt_TEMPSaveJournalDetails T1 where T1.numTransactionId = GJD.numTransactionId);
			                                                           
			--Update transactions
         UPDATE  General_Journal_Details
         SET    		numDebitAmt = X.numDebitAmt,numCreditAmt = X.numCreditAmt,numChartAcntId = X.numChartAcntId,
         varDescription = X.varDescription,numCustomerId = NULLIF(X.numCustomerId,0),
         bitMainDeposit = X.bitMainDeposit,bitMainCheck = X.bitMainCheck,
         bitMainCashCredit = X.bitMainCashCredit,numoppitemtCode = NULLIF(X.numoppitemtCode,0),
         chBizDocItems = X.chBizDocItems,
         vcReference = X.vcReference,numPaymentMethod = X.numPaymentMethod,numCurrencyID = NULLIF(X.numCurrencyID,0),
         fltExchangeRate = X.fltExchangeRate,
         numTaxItemID = NULLIF(X.numTaxItemID,0),numBizDocsPaymentDetailsId = NULLIF(X.numBizDocsPaymentDetailsId,0),
         numcontactid = NULLIF(X.numContactID,0),
         numItemID = NULLIF(X.numItemID,0),numProjectID = NULLIF(X.numProjectID,0),
         numClassID = NULLIF(X.numClassID,0),numCommissionID = NULLIF(X.numCommissionID,0),
         numCampaignID = NULLIF(X.numCampaignID,0),numEntryDateSortOrder1 = v_numEntryDateSortOrder
--									[tintReferenceType] = X.tintReferenceType,
--									[numReferenceID] = X.numReferenceID
         FROM    tt_TEMPSaveJournalDetails as X
         WHERE   X.numTransactionId = General_Journal_Details.numTransactionId;
         INSERT INTO GenealEntryAudit(numDomainID
						,numJournalID
						,numTransactionID
						,dtCreatedDate
						,varDescription)
         SELECT GJD.numDomainId, GJD.numJournalId, GJD.numTransactionId, TIMEZONE('UTC',now()),GJD.varDescription
         FROM General_Journal_Details AS GJD
         JOIN tt_TEMPSaveJournalDetails AS X ON  X.numTransactionId = GJD.numTransactionId;
      end if;
      IF v_Mode = 0 OR v_Mode = 1 then /*-- Insert journal only*/
         INSERT  INTO General_Journal_Details(numJournalId ,
                numDebitAmt ,
                numCreditAmt ,
                numChartAcntId ,
                varDescription ,
                numCustomerId ,
                numDomainId ,
                bitMainDeposit ,
                bitMainCheck ,
                bitMainCashCredit ,
                numoppitemtCode ,
                chBizDocItems ,
                vcReference ,
                numPaymentMethod ,
                bitReconcile ,
                numCurrencyID ,
                fltExchangeRate ,
                numTaxItemID ,
                numBizDocsPaymentDetailsId ,
                numcontactid ,
                numItemID ,
                numProjectID ,
                numClassID ,
                numCommissionID ,
                numReconcileID ,
                bitCleared,
                tintReferenceType,
				numReferenceID,numCampaignID,numEntryDateSortOrder1)
         SELECT
         v_numJournalId ,
                numDebitAmt ,
                numCreditAmt ,
                CAST(CASE WHEN numChartAcntId = 0 THEN NULL ELSE numChartAcntId END AS NUMERIC(18,0))  ,
                varDescription ,
                NULLIF(numCustomerId,0) ,
                v_numDomainId ,
                bitMainDeposit ,
                bitMainCheck ,
                bitMainCashCredit ,
                NULLIF(numoppitemtCode,0) ,
                chBizDocItems ,
                vcReference ,
                numPaymentMethod ,
                bitReconcile ,
                NULLIF(numCurrencyID,0) ,
                fltExchangeRate ,
                NULLIF(numTaxItemID,0) ,
                NULLIF(numBizDocsPaymentDetailsId,0) ,
                NULLIF(numContactID,0) ,
                NULLIF(numItemID,0) ,
                NULLIF(numProjectID,0) ,
                NULLIF(numClassID,0) ,
                NULLIF(numCommissionID,0) ,
                NULLIF(numReconcileID,0) ,
                bitCleared,
                tintReferenceType,
				NULLIF(numReferenceID,0),NULLIF(numCampaignID,0),v_numEntryDateSortOrder
         FROM
         tt_TEMPSaveJournalDetails as X
         WHERE
         X.numTransactionId = 0;

			--Set Default Class If enable User Level Class Accountng 
         select   coalesce(numAccountClass,0) INTO v_numAccountClass FROM General_Journal_Header WHERE numDomainId = v_numDomainId AND numJOurnal_Id = v_numJournalId;
         If coalesce((SELECT  numClassID FROM General_Journal_Details WHERE numJournalId = v_numJournalId AND numDomainId = v_numDomainId AND coalesce(numClassID,0) > 0 LIMIT 1),0) > 0
         AND coalesce(v_numAccountClass,0) <> coalesce((SELECT  numClassID FROM General_Journal_Details WHERE numJournalId = v_numJournalId AND numDomainId = v_numDomainId AND coalesce(numClassID,0) > 0 LIMIT 1),0) then
			
            v_numAccountClass := coalesce((SELECT  numClassID FROM General_Journal_Details WHERE numJournalId = v_numJournalId AND numDomainId = v_numDomainId AND coalesce(numClassID,0) > 0 LIMIT 1),0);
         end if;
         IF v_numAccountClass > 0 then
            
            UPDATE General_Journal_Details SET numClassID = v_numAccountClass WHERE numJournalId = v_numJournalId AND numDomainId = v_numDomainId AND coalesce(numClassID,0) = 0;
         end if;
         INSERT INTO GenealEntryAudit(numDomainID
						,numJournalID
						,numTransactionID
						,dtCreatedDate
						,varDescription)
         SELECT v_numDomainId, v_numJournalId, X.numTransactionId, TIMEZONE('UTC',now()),X.varDescription
         FROM tt_TEMPSaveJournalDetails AS X WHERE X.numTransactionId = 0;
      end if;
      UPDATE
      General_Journal_Header
      SET
      numAmount =(SELECT SUM(numDebitAmt) FROM General_Journal_Details WHERE numJournalId = v_numJournalId)
      WHERE
      numJOurnal_Id = v_numJournalId AND numDomainId = v_numDomainId;
      select   CAST(SUM(numDebitAmt) AS DECIMAL(20,5)) -CAST(SUM(numCreditAmt) AS DECIMAL(20,5)) INTO v_SumTotal FROM
      General_Journal_Details WHERE
      numJournalId = v_numJournalId
      AND numDomainId = v_numDomainId;
      IF cast(v_SumTotal AS DECIMAL(10,4)) > 0.0001 then
		
         INSERT INTO GenealEntryAudit(numDomainID
				,numJournalID
				,numTransactionID
				,dtCreatedDate
				,varDescription)
         SELECT
         v_numDomainId,
				v_numJournalId,
				0,
				TIMEZONE('UTC',now()),
				'IM_BALANCE';
         RAISE EXCEPTION 'IM_BALANCE';
         RETURN;
      end if;
                    
        -- Update leads,prospects to Accounts
      select   coalesce(numCustomerId,0), coalesce(numCreatedBy,0) INTO v_numDivisionID,v_numUserCntID FROM General_Journal_Details GJD
      JOIN General_Journal_Header GJH ON GJD.numJournalId = GJH.numJOurnal_Id AND GJD.numDomainId = GJH.numDomainId WHERE numJournalId = v_numJournalId
      AND GJH.numDomainId = v_numDomainId    LIMIT 1;
      IF v_numDivisionID > 0 AND v_numUserCntID > 0 then
		
         PERFORM USP_UpdateCRMTypeToAccounts(v_numDivisionID,v_numDomainId,v_numUserCntID);
      end if;
   ELSEIF v_Mode = 1
   then  /*Edit Journal entries*/    
	
      DELETE FROM
      General_Journal_Details
      WHERE
      numJournalId = v_numJournalId;
      UPDATE
      General_Journal_Header
      SET
      numAmount = coalesce((SELECT SUM(numDebitAmt) FROM General_Journal_Details WHERE numJournalId = v_numJournalId),0)
      WHERE
      numJOurnal_Id = v_numJournalId AND numDomainId = v_numDomainId;
      select   CAST(SUM(numDebitAmt) AS DECIMAL(20,5)) -CAST(SUM(numCreditAmt) AS DECIMAL(20,5)) INTO v_SumTotal FROM
      General_Journal_Details WHERE
      numJournalId = v_numJournalId
      AND numDomainId = v_numDomainId;
      IF cast(v_SumTotal AS DECIMAL(10,4)) > 0.0001 then
		
         INSERT INTO GenealEntryAudit(numDomainID
				,numJournalID
				,numTransactionID
				,dtCreatedDate
				,varDescription)
         SELECT
         v_numDomainId,
				v_numJournalId,
				0,
				TIMEZONE('UTC',now()),
				'IM_BALANCE';
         RAISE EXCEPTION 'IM_BALANCE';
     
         RETURN;
      end if;
   end if;
   DROP TABLE IF EXISTS tt_TEMP1 CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP1
   (
      numTransactionId NUMERIC(18,0),
      numEntryDateSortOrder1 NUMERIC(18,0),
      NewSortId NUMERIC(18,0)
   );
   INSERT INTO tt_TEMP1(numTransactionId
		,numEntryDateSortOrder1
		,NewSortId)
   SELECT
   GJD.numTransactionId
		,GJD.numEntryDateSortOrder1
		,Row_number() OVER(ORDER BY GJD.numTransactionId) AS NewSortId
   FROM
   General_Journal_Details GJD
   WHERE
   GJD.numDomainId = v_numDomainId
   AND CAST(GJD.numEntryDateSortOrder1 AS VARCHAR) ilike SUBSTR(CAST(v_numEntryDateSortOrder AS VARCHAR(25)),1,12) || '%';
   UPDATE
   General_Journal_Details GJD
   SET
   numEntryDateSortOrder1 =(Temp1.numEntryDateSortOrder1+Temp1.NewSortId)
   FROM
   tt_TEMP1 AS Temp1 WHERE GJD.numTransactionId = Temp1.numTransactionId;
   -- COMMIT
EXCEPTION WHEN OTHERS THEN
       GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 
      INSERT INTO GenealEntryAudit(numDomainID
								,numJournalID
								,numTransactionID
								,dtCreatedDate
								,varDescription)
      SELECT v_numDomainId, v_numJournalId,0, TIMEZONE('UTC',now()),my_ex_message;

     raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;

      RETURN;	          
END; $$;


