CREATE OR REPLACE FUNCTION USP_ManageOpportunityFulfillmentBizDocs(v_numDomainID NUMERIC,
    v_strItems TEXT,
    v_tintMode SMALLINT, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDocItem  INTEGER;
BEGIN
   IF v_tintMode = 0 OR v_tintMode = 2 then
        
			--If Fulfillment Order bizdocs is not added
      IF NOT EXISTS(SELECT numBizDocTypeID FROM OpportunityFulfillmentBizDocs WHERE numDomainID = v_numDomainID) then
			
         INSERT INTO OpportunityFulfillmentBizDocs(numBizDocTypeID,numDomainID,ItemType)
         select 296,v_numDomainID,'P' UNION
         select 296,v_numDomainID,'N' UNION
         select 296,v_numDomainID,'S';
      end if;
      IF v_tintMode = 0 then
				
				--Fetch Fulfillment Order bizdocs and all bizdocs for domain
         open SWV_RefCur for
         SELECT LD.numListItemID AS numBizDocTypeID,vcData AS vcBizDocType
         FROM Listdetails LD
         WHERE LD.numListID = 27 and ((LD.constFlag = 1 AND LD.numListItemID = 296) or (LD.constFlag = 0 AND LD.numDomainid = v_numDomainID))
         ORDER BY LD.numListItemID;
      ELSEIF v_tintMode = 2
      then
				
         open SWV_RefCur for
         SELECT LD.numListItemID AS numBizDocTypeID,vcData AS vcBizDocType,
					  CASE WHEN coalesce(FO.numBizDocTypeID,0) > 0 THEN true ELSE false END AS bitIsFulfillmentOrder,
					  coalesce(FO.ItemType,'') AS ItemType
         FROM Listdetails LD JOIN OpportunityFulfillmentBizDocs FO ON LD.numListItemID = FO.numBizDocTypeID
         AND FO.numDomainID = v_numDomainID
         WHERE LD.numListID = 27 and ((LD.constFlag = 1 AND LD.numListItemID = 296) or (LD.constFlag = 0 AND LD.numDomainid = v_numDomainID))
         ORDER BY LD.numListItemID;
      end if;
   ELSEIF v_tintMode = 1
   then
      DELETE FROM OpportunityFulfillmentBizDocs WHERE numDomainID = v_numDomainID;
      IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then
    
         INSERT  INTO OpportunityFulfillmentBizDocs(numDomainID,numBizDocTypeID,ItemType)
         SELECT  v_numDomainID,
                                    X.numBizDocTypeID,
                                    X.ItemType
         FROM
		 XMLTABLE
			(
				'NewDataSet/Table'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numBizDocTypeID NUMERIC(18,0) PATH 'numBizDocTypeID',
					ItemType CHAR(2) PATH 'ItemType'
			) AS X;
      
      end if;
      open SWV_RefCur for
      SELECT  '';
   end if;
   RETURN;
END; $$;





