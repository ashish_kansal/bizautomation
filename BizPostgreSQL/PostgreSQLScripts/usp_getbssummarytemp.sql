-- Stored procedure definition script USP_GetBSSummaryTemp for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetBSSummaryTemp(v_numDomainId INTEGER,
v_dtFromDate TIMESTAMP,
v_dtToDate TIMESTAMP,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_CURRENTPL  NUMERIC(19,4);
   v_PLOPENING  NUMERIC(19,4);
   v_PLCHARTID  NUMERIC(8,0);
   v_PLCODE  VARCHAR(50);

--select * from VIEW_JOURNALPL where numDomainid=72

--------------------------------------------------------
-- GETTING P&L VALUE

BEGIN
   v_CURRENTPL := CAST(0 AS NUMERIC(19,4));
   v_PLOPENING := CAST(0 AS NUMERIC(19,4));

   select   coalesce(SUM(Debit),0) -coalesce(SUM(Credit),0) INTO v_CURRENTPL FROM VIEW_DAILY_INCOME_EXPENSE WHERE
   numDomainID = v_numDomainId AND datEntry_Date <= v_dtToDate;


   select   COA.numAccountId INTO v_PLCHARTID FROM Chart_Of_Accounts COA WHERE numDomainId = v_numDomainId and
   bitProfitLoss = true;


--SELECT  @CURRENTPL=@CURRENTPL + 		   
--ISNULL((SELECT sum(Debit-Credit) FROM VIEW_JOURNALPL VJ
--WHERE VJ.numDomainId=@numDomainId AND
--	VJ.numAccountId=@PLCHARTID AND
--	datEntry_Date BETWEEN @dtFromDate AND @dtToDate),0) ;

--SET @CURRENTPL=@CURRENTPL * (-1)
--SELECT @PLCHARTID,@CURRENTPL,@PLOPENING

-----------------------------------------------------------------


   DROP TABLE IF EXISTS tt_PLSUMMARY CASCADE;
   CREATE TEMPORARY TABLE tt_PLSUMMARY 
   (
      numAccountId NUMERIC(9,0),
      vcAccountName VARCHAR(250),
      numParntAcntTypeID NUMERIC(9,0),
      vcAccountDescription VARCHAR(250),
      vcAccountCode VARCHAR(50),
      Opening NUMERIC(19,4),
      Debit NUMERIC(19,4),
      Credit NUMERIC(19,4)
   );

   INSERT INTO  tt_PLSUMMARY
   SELECT COA.numAccountId,vcAccountName,numParntAcntTypeID,vcAccountDescription,vcAccountCode,
CAST(coalesce(COA.numOriginalOpeningBal,0)+coalesce((SELECT sum(coalesce(Debit,cast(0 as NUMERIC(19,4))) -coalesce(Credit,cast(0 as NUMERIC(19,4)))) FROM VIEW_JOURNALBS VJ
      WHERE VJ.numdomainid = v_numDomainId AND
      VJ.numAccountId = COA.numAccountId AND
      datEntry_Date < v_dtFromDate),cast(0 as NUMERIC(19,4))) AS NUMERIC(19,4)) AS OPENING,
coalesce((SELECT sum(coalesce(Debit,cast(0 as NUMERIC(19,4)))) FROM VIEW_JOURNALBS VJ
      WHERE VJ.numdomainid = v_numDomainId AND
      VJ.numAccountId = COA.numAccountId AND
      datEntry_Date BETWEEN  v_dtFromDate AND v_dtToDate),
   cast(0 as NUMERIC(19,4))) as DEBIT,
coalesce((SELECT sum(coalesce(Credit,cast(0 as NUMERIC(19,4)))) FROM VIEW_JOURNALBS VJ
      WHERE VJ.numdomainid = v_numDomainId AND
      VJ.numAccountId = COA.numAccountId AND
      datEntry_Date BETWEEN  v_dtFromDate AND v_dtToDate),cast(0 as NUMERIC(19,4))) as CREDIT
   FROM Chart_Of_Accounts COA
   WHERE COA.numDomainId = v_numDomainId AND
   coalesce(bitProfitLoss,false) = false AND
      (COA.vcAccountCode ilike '0101%' OR
   COA.vcAccountCode ilike '0102%' OR
   COA.vcAccountCode ilike '0105%')
   UNION
   SELECT v_PLCHARTID,vcAccountName,numParntAcntTypeID,vcAccountDescription,
vcAccountCode,
CAST(coalesce(numOriginalOpeningBal,0)+coalesce((SELECT sum(coalesce(Debit,cast(0 as NUMERIC(19,4))) -coalesce(Credit,cast(0 as NUMERIC(19,4)))) FROM VIEW_JOURNALPL VJ
      WHERE VJ.numDomainId = v_numDomainId AND
      VJ.numAccountId = v_PLCHARTID AND
      datEntry_Date <= v_dtToDate),cast(0 as NUMERIC(19,4)))+(v_CURRENTPL*(-1)) AS NUMERIC(19,4))
,CAST(0 AS NUMERIC(19,4)),CAST(0 AS NUMERIC(19,4))
   FROM Chart_Of_Accounts COA WHERE numDomainId = v_numDomainId and
   bitProfitLoss = true AND COA.numAccountId = v_PLCHARTID;


   DROP TABLE IF EXISTS tt_PLOUTPUT CASCADE;
   CREATE TEMPORARY TABLE tt_PLOUTPUT 
   (
      numAccountId NUMERIC(9,0),
      vcAccountName VARCHAR(250),
      numParntAcntTypeID NUMERIC(9,0),
      vcAccountDescription VARCHAR(250),
      vcAccountCode VARCHAR(50),
      Opening NUMERIC(19,4),
      Debit NUMERIC(19,4),
      Credit NUMERIC(19,4)
   );

   INSERT INTO tt_PLOUTPUT
   SELECT ATD.numAccountTypeID,CAST(ATD.vcAccountType AS VARCHAR(250)),ATD.numParentID, CAST('' AS VARCHAR(250)),ATD.vcAccountCode,
 coalesce(SUM(coalesce(Opening,cast(0 as NUMERIC(19,4)))),cast(0 as NUMERIC(19,4))) as Opening,
coalesce(Sum(coalesce(Debit,cast(0 as NUMERIC(19,4)))),cast(0 as NUMERIC(19,4))) as Debit,coalesce(Sum(coalesce(Credit,cast(0 as NUMERIC(19,4)))),cast(0 as NUMERIC(19,4))) as Credit
   FROM
   AccountTypeDetail ATD RIGHT OUTER JOIN
   tt_PLSUMMARY PL ON
   PL.vcAccountCode ilike ATD.vcAccountCode || '%'
   AND ATD.numDomainID = v_numDomainId AND
(ATD.vcAccountCode ilike '0101%' OR
   ATD.vcAccountCode ilike '0102%' OR
   ATD.vcAccountCode ilike '0105%')
   GROUP BY
   ATD.numAccountTypeID,ATD.vcAccountCode,ATD.vcAccountType,ATD.numParentID;


   DROP TABLE IF EXISTS tt_PLSHOW CASCADE;
   CREATE TEMPORARY TABLE tt_PLSHOW 
   (
      numAccountId NUMERIC(9,0),
      vcAccountName VARCHAR(250),
      numParntAcntTypeID NUMERIC(9,0),
      vcAccountDescription VARCHAR(250),
      vcAccountCode VARCHAR(50),
      Opening NUMERIC(19,4),
      Debit NUMERIC(19,4),
      Credit NUMERIC(19,4),
      Balance NUMERIC(19,4),
      AccountCode1  VARCHAR(100),
      vcAccountName1 VARCHAR(250),
      Type INTEGER
   );

   INSERT INTO tt_PLSHOW
   SELECT *,CAST(Opening+Debit -Credit AS VARCHAR(250)) as Balance,
CAST(CASE
   WHEN LENGTH(P.vcAccountCode) > 4 THEN REPEAT(' ',LENGTH(P.vcAccountCode) -4) || P.vcAccountCode
   ELSE  P.vcAccountCode
   END AS NUMERIC(9,0)) AS AccountCode1,
 CASE
   WHEN LENGTH(P.vcAccountCode) > 4 THEN REPEAT(' ',LENGTH(P.vcAccountCode) -4) || P.vcAccountName
   ELSE P.vcAccountName
   END AS vcAccountName1, CAST(1 AS VARCHAR(50)) AS Type
   FROM tt_PLSUMMARY P
   UNION
   SELECT *,CAST(Opening+Debit -Credit AS VARCHAR(250)) as Balance,
CAST(CASE
   WHEN LENGTH(O.vcAccountCode) > 4 THEN REPEAT(' ',LENGTH(O.vcAccountCode) -4) || O.vcAccountCode
   ELSE  O.vcAccountCode
   END AS NUMERIC(9,0)) AS AccountCode1,
 CASE
   WHEN LENGTH(O.vcAccountCode) > 4 THEN REPEAT(' ',LENGTH(O.vcAccountCode) -4) || O.vcAccountName
   ELSE O.vcAccountName
   END AS vcAccountName1, CAST(2 AS VARCHAR(50)) as Type
   FROM tt_PLOUTPUT O;

   UPDATE tt_PLSHOW SET TYPE = 3 WHERE numAccountId = v_PLCHARTID;

   open SWV_RefCur for select * from tt_PLSHOW A ORDER BY A.vcAccountCode;

   DROP TABLE IF EXISTS tt_PLOUTPUT CASCADE;
   DROP TABLE IF EXISTS tt_PLSUMMARY CASCADE;
   DROP TABLE IF EXISTS tt_PLSHOW CASCADE;
   RETURN;
END; $$;












