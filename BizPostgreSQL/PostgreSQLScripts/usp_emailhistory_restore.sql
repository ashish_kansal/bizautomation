-- Stored procedure definition script USP_EmailHistory_Restore for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_EmailHistory_Restore(v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0),
    v_numEmailHstrID TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numInboxNodeID  NUMERIC(18,0);
   v_strSQL  TEXT;
BEGIN
   select   coalesce(numNodeID,0) INTO v_numInboxNodeID FROM InboxTreeSort WHERE numDomainID = v_numDomainID AND numUserCntID = v_numUserCntID AND numFixID = 1 AND coalesce(bitSystem,false) = true;

   UPDATE EmailHistory SET bitArchived = false,numNodeId =v_numInboxNodeID WHERE numEmailHstrID IN (SELECT Id FROM SplitIds(coalesce(v_numEmailHstrID,''),','));

   RETURN;
END; $$;





