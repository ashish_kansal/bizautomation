-- Stored procedure definition script usp_ManageBankDetail for PostgreSQL
CREATE OR REPLACE FUNCTION usp_ManageBankDetail(v_numBankDetailID NUMERIC(18,0),
	v_numBankMasterID NUMERIC(18,0),
	v_numDomainID NUMERIC(18,0),
	v_numUserContactID NUMERIC(18,0),
	v_numAccountID NUMERIC(18,0),
	v_vcBankID VARCHAR(50),
	v_vcAccountNumber VARCHAR(50),
	v_vcAccountType VARCHAR(50),
	v_vcUserName VARCHAR(50),
	v_vcPassword VARCHAR(50),
	v_bitIsActive BOOLEAN,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN


   IF EXISTS(SELECT numBankDetailID FROM BankDetails WHERE numBankDetailID = v_numBankDetailID) then

      UPDATE BankDetails SET
      numBankMasterID = v_numBankMasterID,numDomainID = v_numDomainID,numUserContactID = v_numUserContactID,
      numAccountID = v_numAccountID,vcBankID = v_vcBankID,
      vcAccountNumber = v_vcAccountNumber,vcAccountType = v_vcAccountType,
      vcUserName = v_vcUserName,vcPassword = v_vcPassword,dtModifiedDate = LOCALTIMESTAMP,
      bitIsActive = v_bitIsActive
      WHERE
      numBankDetailID = v_numBankDetailID;
   ELSE
      INSERT INTO BankDetails(numBankMasterID,
		numDomainID,
		numUserContactID,
		numAccountID,
		vcBankID,
		vcAccountNumber,
		vcAccountType,
		vcUserName,
		vcPassword,
		dtCreatedDate,
		bitIsActive) VALUES(v_numBankMasterID,
		v_numDomainID,
		v_numUserContactID,
		v_numAccountID,
		v_vcBankID,
		v_vcAccountNumber,
		v_vcAccountType,
		v_vcUserName,
		v_vcPassword,
		LOCALTIMESTAMP ,
		v_bitIsActive);
	
      v_numBankDetailID := CURRVAL('BankDetails_seq');
      UPDATE Chart_Of_Accounts SET numBankDetailID = v_numBankDetailID,IsConnected = true
      WHERE numAccountId = v_numAccountID
      AND numDomainId = v_numDomainID;
   end if;


   open SWV_RefCur for SELECT v_numBankDetailID;
END; $$;












