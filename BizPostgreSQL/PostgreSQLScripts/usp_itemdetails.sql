DROP FUNCTION IF EXISTS USP_ItemDetails;

CREATE OR REPLACE FUNCTION USP_ItemDetails(v_numItemCode NUMERIC(9,0) ,          
v_ClientTimeZoneOffset INTEGER,
v_numDivisionId NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitItemIsUsedInOrder  BOOLEAN DEFAULT 0;
   v_bitOppOrderExists  BOOLEAN DEFAULT 0;
   
   v_numCompanyID  NUMERIC(18,0) DEFAULT 0;
   v_numDomainId  NUMERIC;
   v_numItemGroup  NUMERIC(18,0);
BEGIN
   IF coalesce(v_numDivisionId,0) > 0 then
   
      select   numCompanyID INTO v_numCompanyID FROM DivisionMaster WHERE numDivisionID = v_numDivisionId;
   end if;

   select   numDomainID, numItemGroup INTO v_numDomainId,v_numItemGroup FROM Item WHERE numItemCode = v_numItemCode;
   
	
   IF(SELECT COUNT(OI.numOppId) FROM OpportunityItems OI WHERE OI.numItemCode = v_numItemCode) > 0 then
	
      v_bitItemIsUsedInOrder := true;
      v_bitOppOrderExists := true;
   end if;
	
   IF(SELECT COUNT(OI.numOppId) FROM OpportunityKitItems OI WHERE OI.numChildItemID = v_numItemCode) > 0 then
	
      v_bitItemIsUsedInOrder := true;
      v_bitOppOrderExists := true;
   end if;
	
   IF(SELECT COUNT(OI.numOppId) FROM OpportunityKitChildItems OI WHERE OI.numItemID = v_numItemCode) > 0 then
	
      v_bitItemIsUsedInOrder := true;
      v_bitOppOrderExists := true;
   end if;
	
   IF EXISTS(SELECT numItemID FROM General_Journal_Details WHERE numItemID = v_numItemCode and numDomainId = v_numDomainId AND chBizDocItems <> 'IA1' AND chBizDocItems <> 'OE1') then
	
      v_bitItemIsUsedInOrder := true;
   end if;
	                                      
   open SWV_RefCur for SELECT
   I.numItemCode, vcItemName, coalesce(txtItemDesc,'') AS txtItemDesc,coalesce(IED.txtDesc,'')  AS vcExtendedDescToAPI, charItemType,
		CASE
   WHEN charItemType = 'P' AND coalesce(bitAssembly,false) = true then 'Assembly'
   WHEN charItemType = 'P' AND coalesce(bitKitParent,false) = true then 'Kit'
   WHEN charItemType = 'P' AND coalesce(bitAsset,false) = true AND coalesce(bitRental,false) = false then 'Asset'
   WHEN charItemType = 'P' AND coalesce(bitAsset,false) = true AND coalesce(bitRental,false) = true then 'Rental Asset'
   WHEN charItemType = 'P' AND coalesce(bitSerialized,false) = true AND coalesce(bitAsset,false) = false  then 'Serialized'
   WHEN charItemType = 'P' AND coalesce(bitSerialized,false) = true AND coalesce(bitAsset,false) = true AND coalesce(bitRental,false) = false then 'Serialized Asset'
   WHEN charItemType = 'P' AND coalesce(bitSerialized,false) = true AND coalesce(bitAsset,false) = true AND coalesce(bitRental,false) = true then 'Serialized Rental Asset'
   WHEN charItemType = 'P' AND coalesce(bitLotNo,false) = true THEN 'Lot #'
   ELSE CASE WHEN charItemType = 'P' THEN 'Inventory' ELSE '' END
   END AS InventoryItemType
		,fn_GetItemChildMembershipCount(v_numDomainId,I.numItemCode) AS numChildMembershipCount
		,CASE WHEN coalesce(bitAssembly,false) = true THEN fn_GetAssemblyPossibleWOQty(I.numItemCode,coalesce(W.numWareHouseID,0)) ELSE 0 END AS numWOQty
		,CASE WHEN charItemType = 'P' THEN coalesce((SELECT  monWListPrice FROM WareHouseItems WHERE numDomainID = v_numDomainId AND numItemID = v_numItemCode AND coalesce(monWListPrice,0) > 0 LIMIT 1),0) ELSE monListPrice END AS monListPrice,
		numItemClassification, coalesce(bitTaxable,false) as bitTaxable, coalesce(vcSKU,'') AS vcItemSKU,
		(CASE WHEN I.numItemGroup > 0 THEN coalesce(W.vcWHSKU,'') ELSE coalesce(vcSKU,'') END) AS vcSKU,
		coalesce(bitKitParent,false) as bitKitParent, V.numVendorID, V.monCost, I.numDomainID, I.numCreatedBy, I.bintCreatedDate, I.bintModifiedDate,I.numModifiedBy,
		(SELECT  vcPathForImage FROM ItemImages WHERE numItemCode = v_numItemCode AND bitDefault = true AND bitIsImage = true LIMIT 1) AS vcPathForImage ,
		(SELECT  vcPathForTImage FROM ItemImages WHERE numItemCode = v_numItemCode AND bitDefault = true AND bitIsImage = true LIMIT 1) AS  vcPathForTImage
		,coalesce(bitSerialized,false) as bitSerialized, vcModelID,
		CONCAT('<Images>',(SELECT string_agg(CONCAT('<ItemImages ',
						' numItemImageId="',numItemImageId,'"'
						' vcPathForImage="',vcPathForImage,'"'
						' vcPathForTImage="',vcPathForTImage,'"'
						' bitDefault="',(CASE WHEN bitDefault THEN 1 ELSE 0 END),'"'
						' intDisplayOrder="',(CASE
												WHEN bitDefault = true THEN -1
												ELSE coalesce(intDisplayOrder,0)
											END),'" />'),'' ORDER BY CASE WHEN bitDefault = true THEN -1 ELSE coalesce(intDisplayOrder,0) END ASC)
					FROM
						ItemImages
					WHERE
						numItemCode = v_numItemCode
					),'</Images>') AS xmlItemImages,
		COALESCE((SELECT string_agg(numCategoryID::VARCHAR,',') FROM ItemCategory WHERE numItemID = I.numItemCode),'') AS vcItemCategories ,
		numItemGroup,
		(CASE WHEN coalesce(numItemGroup,0) > 0 AND(SELECT COUNT(*) FROM CFW_Fld_Master CFW JOIN ItemGroupsDTL ON numOppAccAttrID = Fld_id WHERE numItemGroupID = numItemGroup AND tintType = 2) > 0 THEN true ELSE false END) AS bitItemGroupHasAttributes,
		numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, coalesce(bitExpenseItem,false) AS bitExpenseItem, coalesce(numExpenseChartAcntId,0) AS numExpenseChartAcntId, (CASE WHEN coalesce(I.bitVirtualInventory,false) = true THEN 0 ELSE COALESCE(monAverageCost,0) END) AS monAverageCost,
		monCampaignLabourCost,fn_GetContactName(I.numCreatedBy) || ' ,' || I.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) as CreatedBy ,
		fn_GetContactName(I.numModifiedBy) || ' ,' || I.bintModifiedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) as ModifiedBy,
		sum(numOnHand) as numOnHand,
		sum(numOnOrder) as numOnOrder,
		MAX(numReOrder)  as numReOrder,
		sum(numAllocation)  as numAllocation,
		sum(numBackOrder)  as numBackOrder,
		coalesce(fltWeight,0) as fltWeight,
		coalesce(fltHeight,0) as fltHeight,
		coalesce(fltWidth,0) as fltWidth,
		coalesce(fltLength,0) as fltLength,
		coalesce(bitFreeShipping,false) as bitFreeShipping,
		coalesce(bitAllowBackOrder,false) as bitAllowBackOrder,
		coalesce(vcUnitofMeasure,'Units') as vcUnitofMeasure,
		coalesce(bitShowDeptItem,false) AS bitShowDeptItem,
		coalesce(bitShowDeptItemDesc,false) AS bitShowDeptItemDesc,
		coalesce(bitCalAmtBasedonDepItems,false) AS bitCalAmtBasedonDepItems,
		coalesce(bitAssembly,false) AS bitAssembly ,
		coalesce(SUBSTR(numBarCodeId,1,50),'') as  numBarCodeId ,
		(CASE WHEN coalesce(I.numManufacturer,0) > 0 THEN coalesce((SELECT vcCompanyName FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId WHERE numDivisionID = I.numManufacturer),'')  ELSE coalesce(I.vcManufacturer,'') END) AS vcManufacturer,
		coalesce(I.numManufacturer,0) AS numManufacturer,
		coalesce(I.numBaseUnit,0) AS numBaseUnit,coalesce(I.numPurchaseUnit,0) AS numPurchaseUnit,coalesce(I.numSaleUnit,0) AS numSaleUnit,
		fn_GetUOMName(coalesce(I.numBaseUnit,0)) AS vcBaseUnit,fn_GetUOMName(coalesce(I.numPurchaseUnit,0)) AS vcPurchaseUnit,fn_GetUOMName(coalesce(I.numSaleUnit,0)) AS vcSaleUnit,
		coalesce(bitLotNo,false) as bitLotNo,
		coalesce(IsArchieve,false) AS IsArchieve,
		case when charItemType = 'P' then 'Inventory Item' when charItemType = 'S' then 'Service' when charItemType = 'A' then 'Accessory' when charItemType = 'N' then 'Non-Inventory Item' end as ItemType,
		coalesce(I.numItemClass,0) as numItemClass,
		coalesce(tintStandardProductIDType,0) AS tintStandardProductIDType,
		coalesce(vcExportToAPI,'') AS vcExportToAPI,
		coalesce(numShipClass,0) AS numShipClass,coalesce(I.bitAllowDropShip,false) AS bitAllowDropShip,
		coalesce(v_bitItemIsUsedInOrder,false) AS bitItemIsUsedInOrder,
		coalesce((SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID = I.numItemCode),0) AS intParentKitItemCount,
		coalesce(I.bitArchiveItem,false) AS bitArchiveItem,
		coalesce(W.numWareHouseID,0) AS numWareHouseID,
		coalesce(W.numWareHouseItemID,0) AS numWareHouseItemID,
		coalesce((SELECT COUNT(WI.numWareHouseItemID) FROM WareHouseItems WI WHERE WI.numItemID = I.numItemCode AND WI.numItemID = v_numItemCode),0) AS intTransCount,
		coalesce(I.bitAsset,false) AS bitAsset,
		coalesce(I.bitRental,false) AS bitRental,
		coalesce(W.vcBarCode,coalesce(I.numBarCodeId,'')) AS vcBarCode,
		coalesce((SELECT COUNT(*) FROM ItemUOMConversion WHERE numItemCode = I.numItemCode),0) AS tintUOMConvCount,
		coalesce(I.bitVirtualInventory,false) AS bitVirtualInventory ,
		coalesce(I.bitContainer,false) AS bitContainer
		,coalesce(I.numContainer,0) AS numContainer
		,coalesce(I.numNoItemIntoContainer,0) AS numNoItemIntoContainer
		,coalesce(I.bitMatrix,false) AS bitMatrix
		,coalesce(v_bitOppOrderExists,false) AS bitOppOrderExists
		,CPN.CustomerPartNo
		,coalesce(I.vcASIN,'') AS vcASIN
		,coalesce(I.tintKitAssemblyPriceBasedOn,1) AS tintKitAssemblyPriceBasedOn
		,coalesce(I.fltReorderQty,0) AS fltReorderQty
		,coalesce(bitSOWorkOrder,false) AS bitSOWorkOrder
		,coalesce(bitKitSingleSelect,false) AS bitKitSingleSelect
		,coalesce(I.numBusinessProcessId,0) AS numBusinessProcessId,
		coalesce(I.bitTimeContractFromSalesOrder,false) AS bitTimeContractFromSalesOrder
		,coalesce(I.bitAutomateReorderPoint,false) AS bitAutomateReorderPoint
		,coalesce(IDC.numDepreciationPeriod,0) AS numDepreciationPeriod
		,coalesce(IDC.monResidualValue,0) AS monResidualValue
		,coalesce(IDC.tintDepreciationMethod,1) AS tintDepreciationMethod
		,coalesce(I.bitPreventOrphanedParents,true) as bitPreventOrphanedParents
   FROM
   Item I
   left join
   WareHouseItems W
   on
   W.numItemID = I.numItemCode
   LEFT JOIN
   ItemExtendedDetails IED
   ON
   I.numItemCode = IED.numItemCode
   LEFT JOIN
   Vendor V
   ON
   I.numVendorID = V.numVendorID AND I.numItemCode = V.numItemCode
   LEFT JOIN
   CustomerPartNumber CPN
   ON
   I.numItemCode = CPN.numItemCode
   AND CPN.numCompanyId = v_numCompanyID
   LEFT JOIN
   ItemDepreciationConfig IDC
   ON
   I.numItemCode = IDC.numItemCode
   WHERE
   I.numItemCode = v_numItemCode
   GROUP BY
   I.numItemCode,W.vcWHSKU,V.numVendorID,V.monCost,I.numDomainID,I.numCreatedBy, 
   I.bintCreatedDate,I.bintModifiedDate,I.numModifiedBy,I.bitVirtualInventory,  
   I.vcManufacturer,I.numBaseUnit,I.numPurchaseUnit,I.numSaleUnit,
   I.bitContainer,I.bitLotNo,I.IsArchieve,I.numItemClass,I.tintStandardProductIDType,
   I.vcExportToAPI,I.numShipClass,coalesce(IED.txtDesc,'') ,
   I.bitAllowDropShip,I.bitArchiveItem,I.bitAsset,I.bitRental,
   W.numWareHouseID,W.numWareHouseItemID,W.vcBarCode,I.bitMatrix,
   I.numManufacturer,CPN.CustomerPartNo,I.vcASIN,I.tintKitAssemblyPriceBasedOn,
   I.fltReorderQty,I.numBusinessProcessId,I.bitTimeContractFromSalesOrder,
   I.bitAutomateReorderPoint,IDC.numDepreciationPeriod,IDC.monResidualValue,
   IDC.tintDepreciationMethod,I.bitPreventOrphanedParents;
END; $$;
