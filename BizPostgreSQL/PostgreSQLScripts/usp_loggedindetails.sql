CREATE OR REPLACE FUNCTION USP_LoggedinDetails(v_numDivisionID NUMERIC(18,0),      
	v_numContactID NUMERIC(18,0),      
	v_numDomainID NUMERIC(18,0)
	,INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
AS 
$BODY$
   DECLARE
   v_numSubID  NUMERIC(18,0);
BEGIN
   select   numSubscriberID INTO v_numSubID FROM
   Subscribers WHERE
   numTargetDomainID = v_numDomainID;   
     
   UPDATE
   Subscribers
   SET
   dtLastLoggedIn = TIMEZONE('UTC',now())
   WHERE
   numSubscriberID = v_numSubID;  
     
   INSERT INTO UserAccessedDTL(numSubscriberID
		,numDivisionID
		,numContactID
		,dtLoggedInTime
		,bitPortal
		,numDomainId)
    VALUES(v_numSubID
		,v_numDivisionID
		,v_numContactID
		,TIMEZONE('UTC',now())
		,false
		,v_numDomainID);      
         
	
open SWV_RefCur for SELECT cast(CURRVAL('UserAccessedDTL_seq') as VARCHAR(255));
END;
$BODY$;
