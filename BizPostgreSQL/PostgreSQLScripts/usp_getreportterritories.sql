-- Stored procedure definition script USP_GetReportTerritories for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetReportTerritories(v_numUserCntID NUMERIC(9,0),        
v_numDomainId NUMERIC(9,0),        
v_tintType SMALLINT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select numListItemID as "numTerID",vcData as "vcTerName" from ForReportsByTerritory F
   join Listdetails L
   on L.numListItemID = F.numTerritory
   where F.numUserCntId = v_numUserCntID  and L.numListID = 78
   and F.numDomainID = v_numDomainId
   and tintType = v_tintType;
END; $$;












