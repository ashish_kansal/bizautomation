-- Stored procedure definition script usp_GetProfileForCampaign for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetProfileForCampaign(v_numDomainID NUMERIC(9,0) DEFAULT 0   
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   vcProfile
   FROM
   CompanyInfo
   where numDomainID = v_numDomainID
   order by vcProfile;
END; $$;












