-- Function definition script fn_GetAttributesIds for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetAttributesIds(v_numRecID NUMERIC)
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strAttribute  VARCHAR(100);
   v_numCusFldID  NUMERIC(9,0);
   v_numCusFldValue  VARCHAR(100);
   SWV_RowCount INTEGER;
BEGIN
   v_numCusFldID := 0;
   v_strAttribute := '';
   select   Fld_ID, Fld_Value INTO v_numCusFldID,v_numCusFldValue from CFW_Fld_Values_Serialized_Items where RecId = v_numRecID and Fld_Value != '0' and Fld_Value != ''   order by Fld_ID LIMIT 1;
   while v_numCusFldID > 0 LOOP
      IF SWF_IsNumeric(v_numCusFldValue) = true then
	
		--set @strAttribute=@strAttribute+@numCusFldValue
         select   CONCAT(v_strAttribute,Fld_id) INTO v_strAttribute from  CFW_Fld_Master where Fld_id = v_numCusFldID AND Grp_id = 9;
         IF LENGTH(v_strAttribute) > 0 then
        
            select   CONCAT(v_strAttribute,':',numListItemID,',') INTO v_strAttribute from Listdetails where numListItemID::VARCHAR = v_numCusFldValue;
         end if;
      end if;
      select   Fld_ID, Fld_Value INTO v_numCusFldID,v_numCusFldValue from CFW_Fld_Values_Serialized_Items where RecId = v_numRecID and Fld_Value != '0' and Fld_Value != '' and Fld_ID > v_numCusFldID   order by Fld_ID LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount = 0 then 
         v_numCusFldID := 0;
      end if;
   END LOOP;

   If LENGTH(v_strAttribute) > 0 then

      v_strAttribute := SUBSTR(v_strAttribute,1,LENGTH(v_strAttribute) -1);
   end if;

   return v_strAttribute;
END; $$;

