-- Stored procedure definition script USP_BizAPIThrottleCounter_Check for PostgreSQL
CREATE OR REPLACE FUNCTION USP_BizAPIThrottleCounter_Check(v_ID VARCHAR(100),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	
   open SWV_RefCur for SELECT COUNT(*) FROM BizAPIThrottleCounter WHERE vcID = v_ID;
END; $$;













