-- Stored procedure definition script USP_ProjectsMaster_ChangeOrganization for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ProjectsMaster_ChangeOrganization(v_numDomainID NUMERIC(18,0),    
	v_numUserCntID NUMERIC(18,0),
	v_numProId NUMERIC(18,0),
	v_numNewDivisionID NUMERIC(18,0),
	v_numNewContactID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numOldDivisionID  NUMERIC(18,0);
    my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
   select   numDivisionId INTO v_numOldDivisionID FROM ProjectsMaster WHERE numdomainId = v_numDomainID AND numProId = v_numProId;


   BEGIN
      -- BEGIN TRANSACTION
UPDATE
      ProjectsMaster
      SET
      numDivisionId = v_numNewDivisionID,numCustPrjMgr = v_numNewContactID
      WHERE
      numdomainId = v_numDomainID
      AND numProId = v_numProId;
      INSERT INTO RecordOrganizationChangeHistory(numDomainID,numProjectID,numOldDivisionID,numNewDivisionID,numModifiedBy,dtModified)
		VALUES(v_numDomainID,v_numProId,v_numOldDivisionID,v_numNewDivisionID,v_numUserCntID,TIMEZONE('UTC',now()));
		
      DELETE FROM AddressDetails WHERE numRecordID = v_numProId AND tintAddressOf = 4;
      INSERT INTO AddressDetails(vcAddressName
			,vcStreet
			,vcCity
			,vcPostalCode
			,numState
			,numCountry
			,bitIsPrimary
			,tintAddressOf
			,tintAddressType
			,numRecordID
			,numDomainID)
      SELECT
      vcAddressName
			,vcStreet
			,vcCity
			,vcPostalCode
			,numState
			,numCountry
			,bitIsPrimary
			,4,tintAddressType
			,v_numProId
			,numDomainID
      FROM
      AddressDetails
      WHERE
      numDomainID = v_numDomainID
      AND numRecordID = v_numNewDivisionID
      AND tintAddressOf = 2
      AND tintAddressType = 2
      AND bitIsPrimary = true;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
    GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;


