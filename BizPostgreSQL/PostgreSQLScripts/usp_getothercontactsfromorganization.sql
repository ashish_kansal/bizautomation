-- Stored procedure definition script USP_GetOtherContactsFromOrganization for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetOtherContactsFromOrganization(v_numDomainID NUMERIC(18,0)
	,v_tintMode SMALLINT
	,v_vcContactIds TEXT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for SELECT COALESCE((SELECT 
					string_agg(CONCAT(' <li><input type="checkbox" onchange="return OtherContactSelectionChanged(',v_tintMode,',this);" id="', CONCAT(ADC.vcEmail,'~',ADC.numContactID,'~',ADC.vcFirstName,'~',ADC.vcLastName),'" />&nbsp;&nbsp;<label style="font-weight:normal;">',CONCAT(COALESCE(ADC.vcFirstName,'-'),' ',COALESCE(ADC.vcLastName,'-'),', ',ADC.vcEmail,', ',C.vcCompanyName,(CASE WHEN LD.vcData IS NOT NULL THEN CONCAT(' (',LD.vcData,')') ELSE '' END)),'</label></li>'),'' ORDER BY ADC.vcFirstName)
				FROM
					AdditionalContactsInformation ADC
				INNER JOIN 
					DivisionMaster DM 
				ON 
					ADC.numDivisionId = DM.numDivisionID 
				INNER JOIN 
					CompanyInfo C 
				ON
					DM.numCompanyID = C.numCompanyId 
				LEFT JOIN 
					ListDetails LD 
				ON 
					LD.numListItemID=numCompanyType
				WHERE
					ADC.numDomainID = v_numDomainID
					AND ADC.numDivisionId IN (SELECT numDivisionId FROM AdditionalContactsInformation ACI WHERE ACI.numDomainID=v_numDomainID AND ACI.numContactId IN (SELECT Id FROM SplitIDs(v_vcContactIds,',')))
					AND ADC.numContactId NOT IN (SELECT Id FROM SplitIDs(v_vcContactIds,','))
					AND LENGTH(COALESCE(ADC.vcEmail,'')) > 0
				),'');
END; $$;












