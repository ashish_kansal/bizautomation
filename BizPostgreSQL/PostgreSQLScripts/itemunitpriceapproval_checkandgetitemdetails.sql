-- Function definition script ItemUnitPriceApproval_CheckAndGetItemDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION ItemUnitPriceApproval_CheckAndGetItemDetails(v_numOppId NUMERIC(18,0),
v_numDomainID NUMERIC(18,0))
RETURNS TEXT LANGUAGE plpgsql
   AS $$
   DECLARE
   v_returnFormattedItems  TEXT DEFAULT '';
   v_numDivisionID  NUMERIC(18,0);
   v_numWarehouseItemID  NUMERIC(18,0);
   v_ItemName  TEXT;
   v_numItemClassification  NUMERIC(18,0);
   v_numAbovePercent  DOUBLE PRECISION;
   v_numAboveField  DOUBLE PRECISION;
   v_numBelowPercent  DOUBLE PRECISION;
   v_numBelowField  DOUBLE PRECISION;
   v_numDefaultCost  SMALLINT;
   v_bitCostApproval  BOOLEAN;
   v_bitListPriceApproval  BOOLEAN;
   v_numCostDomain  INTEGER;
   v_tintKitAssemblyPriceBasedOn  SMALLINT;
   v_bitCalAmtBasedonDepItems  BOOLEAN;
   v_numItemCode  NUMERIC(18,0);
   v_numQuantity  NUMERIC(18,0);
   v_numUnitPrice  NUMERIC(18,0);
   v_numTotalAmount  NUMERIC(18,0);
   v_monCost  NUMERIC(18,0);
   v_numProductCost  DECIMAL(18,2) DEFAULT 0;
   v_i  INTEGER DEFAULT 1;
   v_RowCount  INTEGER DEFAULT 0;
   v_IsApprovalRequired  BOOLEAN;
   v_ItemAbovePrice  DOUBLE PRECISION;
   v_ItemBelowPrice  DOUBLE PRECISION;
   v_numProposedPercentage  DECIMAL(18,2) DEFAULT 0;
   v_j  INTEGER DEFAULT 0;
   v_Count  INTEGER DEFAULT 0;
   v_tempPriority  INTEGER;
   v_tempNumPriceRuleID  NUMERIC(18,0);
   v_numPriceRuleIDApplied  NUMERIC(18,0) DEFAULT 0;
   v_isRuleApplicable  BOOLEAN DEFAULT 0;
BEGIN
   v_returnFormattedItems := '';
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTableData_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPTABLEDATA CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTABLEDATA
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) primary key,
      numItemCode NUMERIC(18,0),
      numUnitHour DECIMAL(18,2),
      monPrice DECIMAL(18,2),
      monTotAmount DECIMAL(18,2),
      monCost DECIMAL(18,2),
      numWarehouseItmsID NUMERIC(18,2),
      ItemName TEXT,
      numProductCost NUMERIC(18,0)
   );
   select   numDivisionId INTO v_numDivisionID FROM OpportunityMaster WHERE numOppId = v_numOppId    LIMIT 1;
   INSERT INTO tt_TEMPTABLEDATA(numItemCode, numUnitHour, monPrice, monTotAmount, monCost, numWarehouseItmsID, ItemName, numProductCost)
   SELECT I.numItemCode,O.numUnitHour,monPrice*coalesce(OM.fltExchangeRate,1),monTotAmount,coalesce(V.monCost,0) AS monCost,O.numWarehouseItmsID,I.vcItemName,I.monListPrice FROM OpportunityMaster OM INNER JOIN OpportunityItems As O ON OM.numOppId = O.numOppId LEFT JOIN
   Item AS I ON O.numItemCode = I.numItemCode
   LEFT JOIN
   Vendor V
   ON
   I.numVendorID = V.numVendorID AND I.numItemCode = V.numItemCode
   where OM.numOppId = v_numOppId AND O.bitItemPriceApprovalRequired = true;

   select   numCost INTO v_numCostDomain FROM Domain WHERE numDomainId = v_numDomainID;


   SELECT COUNT(*) INTO v_RowCount FROM tt_TEMPTABLEDATA;
   DROP TABLE IF EXISTS tt_TEMPPRICE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPPRICE
   (
      bitSuccess BOOLEAN,
      monPrice DECIMAL(20,5)
   );
   WHILE (v_i <= v_RowCount) LOOP
      v_numProposedPercentage := 0;
      select   numItemCode, numUnitHour, monPrice, monTotAmount, monCost, numWarehouseItmsID, ItemName, numProductCost INTO v_numItemCode,v_numQuantity,v_numUnitPrice,v_numTotalAmount,v_monCost,
      v_numWarehouseItemID,v_ItemName,v_numProductCost FROM
      tt_TEMPTABLEDATA WHERE
      ID = v_i;
      select   coalesce(numItemClassification,0), coalesce(tintKitAssemblyPriceBasedOn,1), coalesce(bitCalAmtBasedonDepItems,false) INTO v_numItemClassification,v_tintKitAssemblyPriceBasedOn,v_bitCalAmtBasedonDepItems FROM
      Item WHERE
      numItemCode = v_numItemCode;
      IF coalesce(v_numItemClassification,0) > 0 AND EXISTS(SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainID = v_numDomainID AND numListItemID = v_numItemClassification) then
		
         select   coalesce(numAbovePercent,0), coalesce(numBelowPercent,0), coalesce(numBelowPriceField,0), coalesce(v_numCostDomain,1), coalesce(bitCostApproval,false), coalesce(bitListPriceApproval,false) INTO v_numAbovePercent,v_numBelowPercent,v_numBelowField,v_numAboveField,v_bitCostApproval,
         v_bitListPriceApproval FROM
         ApprovalProcessItemsClassification WHERE
         numDomainID = v_numDomainID
         AND coalesce(numListItemID,0) = v_numItemClassification;
      ELSE
         select   coalesce(numAbovePercent,0), coalesce(numBelowPercent,0), coalesce(numBelowPriceField,0), coalesce(v_numCostDomain,1), coalesce(bitCostApproval,false), coalesce(bitListPriceApproval,false) INTO v_numAbovePercent,v_numBelowPercent,v_numBelowField,v_numAboveField,v_bitCostApproval,
         v_bitListPriceApproval FROM
         ApprovalProcessItemsClassification WHERE
         numDomainID = v_numDomainID
         AND coalesce(numListItemID,0) = 0;
      end if;
      v_IsApprovalRequired := false;
      v_ItemAbovePrice := 0;
      v_ItemBelowPrice := 0;
      IF v_numQuantity > 0 then
         v_numUnitPrice :=(v_numTotalAmount/v_numQuantity);
      ELSE
         v_numUnitPrice := 0;
      end if;
      DELETE FROM tt_TEMPPRICE;
      IF v_numAboveField > 0 AND coalesce(v_bitCostApproval,false) = true then
			
         IF(SELECT COUNT(*) FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode  AND (bitKitParent = true OR bitAssembly = true)) > 0 AND v_bitCalAmtBasedonDepItems = true then
				
            DELETE FROM tt_TEMPPRICE;
            INSERT INTO tt_TEMPPRICE(bitSuccess
						,monPrice)
            SELECT
            bitSuccess
						,monPrice
            FROM
            fn_GetKitAssemblyCalculatedPrice(v_numDomainID,v_numDivisionID,v_numItemCode,v_numQuantity,v_numWarehouseItemID,
            v_tintKitAssemblyPriceBasedOn,0,0,'',0,1);
            IF(SELECT bitSuccess FROM tt_TEMPPRICE) = true then
					
               SELECT monPrice INTO v_ItemAbovePrice FROM tt_TEMPPRICE;
            end if;
         ELSE
            IF v_numAboveField = 3 then -- Primaty Vendor Cost
               select   coalesce(monCost,0) INTO v_ItemAbovePrice FROM Vendor WHERE numItemCode = v_numItemCode AND numVendorID =(SELECT numVendorID FROM Item WHERE numItemCode = v_numItemCode);
            ELSEIF v_numAboveField = 2
            then -- Product & Sevices Cost
               v_ItemAbovePrice := coalesce(v_numProductCost,0);
            ELSEIF v_numAboveField = 1
            then -- Average Cost
               select(CASE WHEN coalesce(bitVirtualInventory,false) = true THEN 0 ELSE coalesce(monAverageCost,0) END) INTO v_ItemAbovePrice FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode;
            end if;
         end if;
         IF v_ItemAbovePrice > 0 then
				
            IF v_numUnitPrice <(v_ItemAbovePrice+(v_ItemAbovePrice*(v_numAbovePercent/100))) then
               v_numProposedPercentage :=(((v_numUnitPrice -v_ItemAbovePrice)/v_ItemAbovePrice)*100);
            end if;
            v_returnFormattedItems := coalesce(v_returnFormattedItems,'') || '<b>' || coalesce(v_ItemName,'') || '</b>' || '(' || SUBSTR(CAST(v_numQuantity As VARCHAR(500)),1,500) || ') Proposed ' || SUBSTR(CAST(v_numProposedPercentage AS VARCHAR(100)),1,100) || '%, Minimum Allowed ' || SUBSTR(CAST(v_numAbovePercent As VARCHAR(500)),1,500) || '%, ';
            v_IsApprovalRequired := true;
         ELSEIF v_ItemAbovePrice = 0
         then
				
            v_returnFormattedItems := coalesce(v_returnFormattedItems,'') || '<b>' || coalesce(v_ItemName,'') || '</b>' || '(' || SUBSTR(CAST(v_numQuantity As VARCHAR(500)),1,500) || ') Proposed 0%, Minimum Allowed ' || SUBSTR(CAST(v_numAbovePercent As VARCHAR(500)),1,500) || '%, ';
            v_IsApprovalRequired := true;
         end if;
      end if;
      IF v_IsApprovalRequired = false AND v_numBelowField > 0  AND coalesce(v_bitListPriceApproval,false) = true then
			
         IF v_numBelowField = 1 then -- List Price
				
            IF(SELECT coalesce(charItemType,'') AS charItemType FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode) = 'P' then -- Inventory Item
               IF(SELECT COUNT(*) FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode  AND (bitKitParent = true OR bitAssembly = true)) > 0 AND v_bitCalAmtBasedonDepItems = true then
						
                  DELETE FROM tt_TEMPPRICE;
                  INSERT INTO tt_TEMPPRICE(bitSuccess
								,monPrice)
                  SELECT
                  bitSuccess
								,monPrice
                  FROM
                  fn_GetKitAssemblyCalculatedPrice(v_numDomainID,v_numDivisionID,v_numItemCode,v_numQuantity,v_numWarehouseItemID,
                  v_tintKitAssemblyPriceBasedOn,0,0,'',0,1);
                  IF(SELECT bitSuccess FROM tt_TEMPPRICE) = true then
							
                     SELECT monPrice INTO v_ItemBelowPrice FROM tt_TEMPPRICE;
                  end if;
               ELSE
                  select   coalesce(monWListPrice,0) INTO v_ItemBelowPrice FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numItemID = v_numItemCode AND numWareHouseItemID = v_numWarehouseItemID;
               end if;
            ELSE
               select   coalesce(monListPrice,0) INTO v_ItemBelowPrice FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode;
            end if;
         ELSEIF v_numBelowField = 2
         then -- Price Rule
				
					/* Check if valid price book rules exists for sales in domain */
            IF(SELECT COUNT(*) FROM PriceBookRules WHERE numDomainID = v_numDomainID AND tintRuleFor = 1 AND tintStep2 > 0 AND tintStep3 > 0) > 0 then
               DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
               CREATE TEMPORARY TABLE tt_TEMPTABLE
               (
                  numID INTEGER, 
                  numRuleID NUMERIC(18,0), 
                  numPriority INTEGER
               );
               INSERT INTO
               tt_TEMPTABLE
               SELECT
               ROW_NUMBER() OVER(ORDER BY PriceBookPriorities.Priority) AS id,
							numPricRuleID,
							PriceBookPriorities.Priority
               FROM
               PriceBookRules
               INNER JOIN
               PriceBookPriorities
               ON
               PriceBookRules.tintStep2 = PriceBookPriorities.Step2Value AND
               PriceBookRules.tintStep3 = PriceBookPriorities.Step3Value
               WHERE
               PriceBookRules.numDomainID = v_numDomainID AND
               PriceBookRules.tintRuleFor = 1 AND
               PriceBookRules.tintStep2 > 0 AND PriceBookRules.tintStep3 > 0
               ORDER BY
               PriceBookPriorities.Priority;
               select   COUNT(*) INTO v_Count FROM tt_TEMPTABLE;

						/* Loop all price rule with priority */
               WHILE (v_j < v_Count) LOOP
                  select   numRuleID, numPriority INTO v_tempNumPriceRuleID,v_tempPriority FROM tt_TEMPTABLE WHERE numID =(v_i::bigint+1);
					
							/* IF proprity is 9 then price rule is applied to all items and all customers. 
							So price rule must be applied to item or not.*/
                  IF v_tempPriority = 9 then
							
                     v_numPriceRuleIDApplied := v_tempNumPriceRuleID;
                     EXIT;
							/* Check if current item exists in rule. if eixts then exit loop with rule id else continie loop. if item not exist in any rule then nothing to check */
                  ELSE
                     SELECT * INTO v_isRuleApplicable FROM CheckIfPriceRuleApplicableToItem(v_numRuleID := v_tempNumPriceRuleID,v_numItemID := v_numItemCode,v_numDivisionID := v_numDivisionID);
                     IF v_isRuleApplicable = true then
								
                        v_numPriceRuleIDApplied := v_tempNumPriceRuleID;
                        EXIT;
                     end if;
                  end if;
                  v_j := v_j::bigint+1;
               END LOOP;

						/* If @numPriceRuleIDApplied > 0 Get final unit price limit after applying below rule */

               IF v_numPriceRuleIDApplied > 0 then
						
                  v_ItemBelowPrice := GetUnitPriceAfterPriceRuleApplication(v_numPriceRuleIDApplied,v_numDomainID,v_numItemCode,v_numQuantity,v_numWarehouseItemID,
                  v_numDivisionID);
               end if;
            end if;
         ELSEIF v_numBelowField = 3
         then -- Price Level
				
            SELECT * INTO v_ItemBelowPrice FROM GetUnitPriceAfterPriceLevelApplication(v_numDomainID := v_numDomainID,v_numItemCode := v_numItemCode,v_numQuantity := v_numQuantity,
            v_numWarehouseItemID := v_numWarehouseItemID,v_isPriceRule := 0::BOOLEAN,
            v_numPriceRuleID := 0,v_numDivisionID := v_numDivisionID);
         end if;
         IF v_numUnitPrice <(v_ItemBelowPrice -(v_ItemBelowPrice*(v_numBelowPercent/100))) then
            v_numProposedPercentage :=(((v_ItemBelowPrice -v_numUnitPrice)/v_ItemBelowPrice)*100);
         end if;
         v_returnFormattedItems := coalesce(v_returnFormattedItems,'') || '<b>' || coalesce(v_ItemName,'') || '</b>' || '(' || SUBSTR(CAST(v_numQuantity As VARCHAR(500)),1,500) || ') Proposed ' || SUBSTR(CAST(v_numProposedPercentage AS VARCHAR(500)),1,500) || '%, Minimum Allowed ' || SUBSTR(CAST(v_numBelowPercent As VARCHAR(500)),1,500) || '%, ';
         v_IsApprovalRequired := true;
      end if;
      v_i := v_i::bigint+1;
   END LOOP;

   RETURN coalesce(v_returnFormattedItems,'');
END; $$;

