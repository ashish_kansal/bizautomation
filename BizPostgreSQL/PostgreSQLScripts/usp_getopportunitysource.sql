-- Stored procedure definition script USP_GetOpportunitySource for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetOpportunitySource(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcMarketPlaces  VARCHAR(500);
BEGIN
   select   coalesce(vcMarketplaces,'') INTO v_vcMarketPlaces FROM Domain WHERE numDomainId = v_numDomainID;

	IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema like 'pg_temp_%' AND table_name=LOWER('tt_TEMPGetOpportunitySource')) THEN
		DELETE FROM tt_TEMPGetOpportunitySource;
	ELSE
		CREATE TEMPORARY TABLE IF NOT EXISTS tt_TEMPGetOpportunitySource
	   (
		  numItemID VARCHAR(50),
		  vcItemName VARCHAR(100),
		  constFlag BOOLEAN,
		  bitDelete BOOLEAN,
		  intSortOrder INTEGER,
		  numListItemID NUMERIC(9,0),
		  vcData VARCHAR(100),
		  vcListType VARCHAR(1000),
		  vcOrderType VARCHAR(50),
		  vcOppOrOrder VARCHAR(20),
		  numListType NUMERIC(18,0),
		  tintOppOrOrder SMALLINT,
		  bitEnforceMinOrderAmount BOOLEAN,
		  fltMinOrderAmount DECIMAL(20,5),
		  vcOriginalBizDocName VARCHAR(100),
		  numListItemGroupId NUMERIC(18,0),
		  vcListItemGroupName VARCHAR(100),
		  vcColorScheme VARCHAR(100),
		  tintSourceType SMALLINT
	   );
	END IF;

   INSERT INTO
   tt_TEMPGetOpportunitySource
   SELECT
   CAST(CAST('0~1' AS VARCHAR(250)) AS VARCHAR(50)),CAST('Internal Order' AS VARCHAR(100)),true AS constFlag,false AS bitDelete,0 AS intSortOrder,-1 AS numListItemID,CAST('Internal Order' AS VARCHAR(100)),CAST(CAST('Internal Order' AS VARCHAR(100)) AS VARCHAR(1000)),CAST(CAST('' AS VARCHAR(3)) AS VARCHAR(50)),CAST(CAST('' AS VARCHAR(3)) AS VARCHAR(20)),CAST(0 AS NUMERIC(18,0)),CAST(0 AS SMALLINT),CAST(0 AS BOOLEAN),0,CAST('' AS VARCHAR(100)),CAST(0 AS NUMERIC(18,0)),CAST('' AS VARCHAR(100)),CAST('' AS VARCHAR(100)),1
   UNION
   SELECT
   CAST(CAST(Ld.numListItemID  AS VARCHAR(250)) || '~1' AS VARCHAR(50))
		,coalesce(vcRenamedListName,vcData) AS vcData
		,Ld.constFlag,bitDelete
		,1 AS intSortOrder
		,CAST(CAST(Ld.numListItemID  AS VARCHAR(18)) AS INTEGER)
		,coalesce(vcRenamedListName,vcData) AS vcData
		,CAST((SELECT vcData FROM Listdetails WHERE numListID = 9 AND numListItemID = Ld.numListType AND Listdetails.numDomainid = v_numDomainID) AS VARCHAR(1000))
		,CAST((CASE when Ld.numListType = 1 then 'Sales' when Ld.numListType = 2 then 'Purchase' else 'All' END) AS VARCHAR(50))
		,CAST((CASE WHEN Ld.tintOppOrOrder = 1 THEN 'Opportunity' WHEN Ld.tintOppOrOrder = 2 THEN 'Order' ELSE 'All' END) AS VARCHAR(20))
		,numListType
		,tintOppOrOrder
		,CAST(0 AS BOOLEAN)
		,0
		,CAST('' AS VARCHAR(100))
		,coalesce(numListItemGroupId,0)
		,CAST('' AS VARCHAR(100))
		,CAST('' AS VARCHAR(100))
		,1
   FROM
   Listdetails Ld
   LEFT JOIN
   listorder LO
   ON
   Ld.numListItemID = LO.numListItemID
   AND LO.numDomainId = v_numDomainID
   WHERE
   Ld.numListID = 9 AND (constFlag = true OR Ld.numDomainid = v_numDomainID)
   UNION
   SELECT
   CONCAT(numSiteID,'~2')
		,vcSiteName
		,CAST(1 AS BOOLEAN) AS constFlag
		,false AS bitDelete
		,2 AS intSortOrder
		,CAST(CAST(numSiteID AS VARCHAR(18)) AS INTEGER) AS numListItemID
		,vcSiteName
		,Sites.vcSiteName
		,CAST(CAST('' AS VARCHAR(3)) AS VARCHAR(50))
		,CAST(CAST('' AS VARCHAR(3)) AS VARCHAR(20))
		,CAST(0 AS NUMERIC(18,0))
		,CAST(0 AS SMALLINT)
		,CAST(0 AS BOOLEAN)
		,0
		,CAST('' AS VARCHAR(100))
		,CAST(0 AS NUMERIC(18,0))
		,CAST('' AS VARCHAR(100))
		,CAST('' AS VARCHAR(100))
		,2
   FROM
   Sites
   WHERE
   numDomainID = v_numDomainID
   UNION
   SELECT DISTINCT
   CONCAT(WebApiId,'~3')
		,vcProviderName
		,CAST(1 AS BOOLEAN) AS constFlag
		,CAST(0 AS BOOLEAN) AS bitDelete
		,3 AS intSortOrder
		,CAST(CAST(WebApiId AS VARCHAR(18)) AS INTEGER) AS numListItemID
		,vcProviderName
		,WebAPI.vcProviderName
		,CAST(CAST('' AS VARCHAR(3)) AS VARCHAR(50))
		,CAST(CAST('' AS VARCHAR(3)) AS VARCHAR(20))
		,CAST(0 AS NUMERIC(18,0))
		,CAST(0 AS SMALLINT)
		,false
		,0
		,CAST('' AS VARCHAR(100))
		,CAST(0 AS NUMERIC(18,0))
		,CAST('' AS VARCHAR(100))
		,CAST('' AS VARCHAR(100))
		,3
   FROM
   WebAPI
   UNION
   SELECT DISTINCT
   CONCAT(eChannelHub.ID,'~4')
		,vcMarketplace
		,true AS constFlag
		,false AS bitDelete
		,4 AS intSortOrder
		,CAST(CAST(eChannelHub.ID AS VARCHAR(18)) AS INTEGER) AS numListItemID
		,vcMarketplace
		,CAST(CAST('' AS VARCHAR(100)) AS VARCHAR(1000))
		,CAST(CAST('' AS VARCHAR(3)) AS VARCHAR(50))
		,CAST(CAST('' AS VARCHAR(3)) AS VARCHAR(20))
		,CAST(0 AS NUMERIC(18,0))
		,CAST(0 AS SMALLINT)
		,false
		,0
		,CAST('' AS VARCHAR(100))
		,CAST(0 AS NUMERIC(18,0))
		,CAST('' AS VARCHAR(100))
		,CAST('' AS VARCHAR(100))
		,3
   FROM
   eChannelHub
   INNER JOIN(SELECT
      Id
      FROM
      SplitIDs(v_vcMarketPlaces,',')) TEMP
   ON
   eChannelHub.ID = TEMP.Id;
		 
	
open SWV_RefCur for SELECT numItemID AS "numItemID",
      vcItemName AS "vcItemName",
      constFlag AS "constFlag",
      bitDelete AS "bitDelete",
      intSortOrder AS "intSortOrder",
      numListItemID AS "numListItemID",
      vcData AS "vcData",
      vcListType AS "vcListType",
      vcOrderType AS "vcOrderType",
      vcOppOrOrder AS "vcOppOrOrder",
      numListType AS "numListType",
      tintOppOrOrder AS "tintOppOrOrder",
      bitEnforceMinOrderAmount AS "bitEnforceMinOrderAmount",
      fltMinOrderAmount AS "fltMinOrderAmount",
      vcOriginalBizDocName AS "vcOriginalBizDocName",
      numListItemGroupId AS "numListItemGroupId",
      vcListItemGroupName AS "vcListItemGroupName",
      vcColorScheme AS "vcColorScheme",
      tintSourceType AS "tintSourceType" FROM tt_TEMPGetOpportunitySource ORDER BY intSortOrder,vcItemName;

   RETURN;
END; $$;












