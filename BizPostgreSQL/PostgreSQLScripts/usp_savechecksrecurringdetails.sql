-- Stored procedure definition script USP_SaveChecksRecurringDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SaveChecksRecurringDetails(v_numCheckId NUMERIC(9,0) DEFAULT 0,  
v_dtRecurringDate TIMESTAMP DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   Insert into ChecksRecurringDetails(numCheckId,dtRecurringDate) Values(v_numCheckId,v_dtRecurringDate);
RETURN;
END; $$;


