-- Stored procedure definition script USP_GetTopWebApiOrderID for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetTopWebApiOrderID(v_numDomainID NUMERIC(9,0),
          v_numWebApiId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT coalesce((SELECT vcWebApiOrderId FROM WebApiOrderDetails
      WHERE numDomainID = v_numDomainID AND WebApiId = v_numWebApiId AND tintStatus = 0 LIMIT 1),'') AS WebApiOrderId;
END; $$; 

