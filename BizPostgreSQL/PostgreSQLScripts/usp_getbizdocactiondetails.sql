-- Stored procedure definition script USP_GetBizDocActionDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetBizDocActionDetails(v_numUserCntID NUMERIC(8,0),
v_numDomainID NUMERIC(8,0), 
 v_numBizActionID NUMERIC(8,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT OrderType,SUBSTR("Order Id",1,20) || '...' as OrderId,
OrderAmount,
SUBSTR(BizDocID,1,20) || '...' as BizDocID,CAST('' AS CHAR(1)) as FileType,
BizDocAmount,
BizDocPaidAmount,
(SELECT count(btStatus) from BizActionDetails C where C.numOppBizDocsId = a.numOppBizDocsId and btStatus = 0) as Pending,
(SELECT count(btStatus) from BizActionDetails C where C.numOppBizDocsId = a.numOppBizDocsId and btStatus = 1) as Approved,
(SELECT count(btStatus) from BizActionDetails C where C.numOppBizDocsId = a.numOppBizDocsId and btStatus = 2) as Declined,
 0 as Accept, 0 as Decline,
numOppId
,a.numOppBizDocsId
,numBizDocId
,'' as  VcFileName
,'' as cUrlType
,'' as vcDocumentSection
,tintOppType,
'B' AS BizDocTypeID,
a.vcComment
   FROM
   BizActionDetails a
   INNER JOIN
   View_BizDoc B
   ON
   a.numOppBizDocsId = B.numOppBizDocsId
   INNER JOIN
   BizDocAction D
   ON
   a.numBizActionId = D.numBizActionId and
   D.numDomainID = B.numDomainID
   where
   D.numDomainID = v_numDomainID and
   D.numBizActionId = v_numBizActionID and
   D.numContactId = v_numUserCntID and
   a.btDocType = 1 and a.btStatus = 0
   UNION
   select
   fn_GetListItemName(numDocCategory) as OrderType,
numGenericDocID::VARCHAR as OrderId,
  0 as OrderAmount,
  vcDocName as BizDocID,
vcfiletype as FileType,
0 as BizDocAmount,
0 as BizDocPaidAmount,
 (select count(*) from DocumentWorkflow
      where numDocID = numGenericDocID and cDocType = vcDocumentSection and tintApprove = 0) as Pending,
(select count(*) from DocumentWorkflow
      where numDocID = numGenericDocID and cDocType = vcDocumentSection and tintApprove = 1) as Approved,
 (select count(*) from DocumentWorkflow
      where numDocID = numGenericDocID and cDocType = vcDocumentSection and tintApprove = 2) as Declined,
  0 as Accept, 0 as Declain,
  0 as numOppId
  ,numGenericDocID as numOppBizDocsId
  , 0 as numBizDocId
  ,  VcFileName ,
cUrlType 
,vcDocumentSection 
,0 AS tintOppType
,'S' AS BizDocTypeID,
B.vcComment
   from GenericDocuments a
   INNER JOIN
   BizActionDetails B
   ON
   a.numGenericDocID = B.numOppBizDocsId
   INNER JOIN
   BizDocAction C
   ON
   C.numBizActionId = B.numBizActionId
   AND C.numDomainID = a.numDomainId
   WHERE
   C.numContactId = v_numUserCntID AND
   B.numBizActionId = v_numBizActionID AND
   a.numDomainId = v_numDomainID    and
   B.btDocType = 0 and
   B.btStatus = 0
   UNION
   select
   fn_GetListItemName(numDocCategory) as OrderType,
numGenericDocID::VARCHAR as OrderId,
  0 as OrderAmount,
  vcDocName as BizDocID,
vcfiletype as FileType,
0 as BizDocAmount,
0 as BizDocPaidAmount,
 (select count(*) from DocumentWorkflow
      where numDocID = numGenericDocID and cDocType = 'D' and tintApprove = 0) as Pending,
(select count(*) from DocumentWorkflow
      where numDocID = numGenericDocID and cDocType = 'D' and tintApprove = 1) as Approved,
 (select count(*) from DocumentWorkflow
      where numDocID = numGenericDocID and cDocType = 'D' and tintApprove = 2) as Declined,
  0 as Accept, 0 as Declain,
  0 as numOppId,numGenericDocID as numOppBizDocsId, 0 as numBizDocId,  VcFileName ,
 cUrlType ,'D'  AS vcDocumentSection,0 AS tintOppType,'D' AS BizDocTypeID,B.vcComment
   from GenericDocuments a
   INNER JOIN
   BizActionDetails B
   ON
   a.numGenericDocID = B.numOppBizDocsId
   INNER JOIN
   BizDocAction C
   ON
   C.numBizActionId = B.numBizActionId AND C.numDomainID = a.numDomainId
   where
   C.numContactId = v_numUserCntID AND
   B.numBizActionId = v_numBizActionID AND
   a.numDomainId = v_numDomainID   AND
   B.btDocType = 0 and
   B.btStatus = 0;
   RETURN;
END; $$;

/****** Object:  StoredProcedure [dbo].[usp_getSaveCustomReport]    Script Date: 07/26/2008 16:18:26 ******/













