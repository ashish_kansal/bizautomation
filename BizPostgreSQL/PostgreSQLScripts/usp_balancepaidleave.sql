-- Stored procedure definition script USP_BalancePaidLeave for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_BalancePaidLeave(v_numUserId NUMERIC(9,0) DEFAULT 0,    
v_numUserCntId NUMERIC(9,0) DEFAULT 0,    
v_numDomainId NUMERIC(9,0) DEFAULT 0,    
INOUT v_decCurrentBalancePaidLeaveHrs DOUBLE PRECISION DEFAULT 0)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_decRegularHrsWorked  DECIMAL(10,2);    
   v_decOverHrsWorked  DECIMAL(10,2);    
   v_decLimDailHrs  DECIMAL(10,2);    
   v_decPaidLeaveHrs  DECIMAL(10,2);    
      
   v_decTotalHrsWorked  DECIMAL(10,2);    
   v_decCurrentPaidLeaveHrs  DECIMAL(10,2);
BEGIN
   select   coalesce(numLimDailHrs,0), coalesce(fltPaidLeaveHrs,0) INTO v_decLimDailHrs,v_decPaidLeaveHrs From UserMaster Where numUserId = v_numUserId And numDomainID = v_numDomainId;    
   select   Sum(Cast((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) as DOUBLE PRECISION)/Cast(60 as DOUBLE PRECISION)) INTO v_decRegularHrsWorked from timeandexpense Where (numtype = 1 Or numtype = 2)  And numCategory = 1  And numUserCntID = v_numUserCntId And numDomainID = v_numDomainId;                    
      
   select   Sum(Cast((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) as DOUBLE PRECISION)/Cast(60 as DOUBLE PRECISION)) INTO v_decCurrentPaidLeaveHrs from timeandexpense Where numtype = 3 And numCategory = 3 And numUserCntID = v_numUserCntId And numDomainID = v_numDomainId;    
    
     
   v_decTotalHrsWorked := coalesce(v_decRegularHrsWorked,0);    
     
	 IF v_decPaidLeaveHrs > 0 THEN
		v_decPaidLeaveHrs := v_decTotalHrsWorked/v_decPaidLeaveHrs;    
	 ELSE
		v_decPaidLeaveHrs := 0;    
	 END IF;
    
		v_decCurrentBalancePaidLeaveHrs := coalesce(v_decPaidLeaveHrs,0) -coalesce(v_decCurrentPaidLeaveHrs,0);
   RETURN;
END; $$;


