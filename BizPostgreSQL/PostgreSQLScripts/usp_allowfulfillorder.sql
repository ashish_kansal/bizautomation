-- Stored procedure definition script USP_AllowFulfillOrder for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_AllowFulfillOrder(v_numDomainID NUMERIC(9,0),
      v_numOppID NUMERIC(9,0),
      INOUT v_bitAllowFulfillOrder BOOLEAN)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numPackingSlipBizDocId  NUMERIC(9,0);
   v_numAuthInvoice  NUMERIC(9,0);
BEGIN
   v_bitAllowFulfillOrder := false;
   
   select   numListItemID INTO v_numPackingSlipBizDocId FROM Listdetails WHERE vcData = 'Packing Slip' AND constFlag = true AND numListID = 27;
	    
   select   coalesce(numAuthoritativeSales,0) INTO v_numAuthInvoice From AuthoritativeBizDocs Where numDomainId = v_numDomainID; 
	    
	    
   IF NOT EXISTS(SELECT numOppBizDocsId FROM OpportunityMaster OM JOIN OpportunityBizDocs OBD ON OM.numOppId = OBD.numoppid
   WHERE OM.numDomainId = v_numDomainID AND OM.numOppId = v_numOppID  AND OBD.numBizDocId IN(v_numPackingSlipBizDocId,v_numAuthInvoice,296)) then
			
      v_bitAllowFulfillOrder := true;
   end if;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_CheckCanbeReOpenOppertunity]    Script Date: 07/26/2008 16:20:14 ******/



