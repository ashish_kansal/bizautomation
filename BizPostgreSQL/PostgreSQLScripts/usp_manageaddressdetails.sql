-- Stored procedure definition script USP_ManageAddressDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageAddressDetails(INOUT v_numAddressID NUMERIC(18,0) DEFAULT 0 ,
v_vcAddressName  VARCHAR(50) DEFAULT NULL ,
v_vcStreet  VARCHAR(100) DEFAULT NULL ,
v_vcCity  VARCHAR(50) DEFAULT NULL ,
v_vcPostalCode  VARCHAR(15) DEFAULT NULL ,
v_numState  NUMERIC(18,0) DEFAULT NULL ,
v_numCountry  NUMERIC(18,0) DEFAULT NULL ,
v_bitIsPrimary  BOOLEAN DEFAULT NULL ,
v_tintAddressOf  SMALLINT DEFAULT NULL ,
v_tintAddressType  SMALLINT DEFAULT NULL ,
v_numRecordID  NUMERIC(18,0) DEFAULT NULL,
v_numDomainID  NUMERIC(18,0) DEFAULT NULL,
v_bitResidential BOOLEAN DEFAULT NULL,
v_vcPhone VARCHAR(20) DEFAULT '',
v_bitFromEcommerce BOOLEAN DEFAULT false,
v_numContact NUMERIC(18,0) DEFAULT 0,
v_bitAltContact BOOLEAN DEFAULT false,
v_vcAltContact VARCHAR(200) DEFAULT '',
v_numUserCntID NUMERIC(18,0) DEFAULT 0)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintAddressOf = 2 AND coalesce(v_numContact,0) = 0 AND coalesce(v_bitAltContact,false) = false then
	
      select   numContactId INTO v_numContact FROM
      AdditionalContactsInformation WHERE
      numDomainID = v_numDomainID
      AND numDivisionId = v_numRecordID
      AND coalesce(bitPrimaryContact,false) = true    LIMIT 1;
   end if;

   IF coalesce(v_bitFromEcommerce,false) = true then

      UPDATE
      DivisionMaster
      SET
      vcComPhone = CASE WHEN coalesce(v_vcPhone,'') <> '' THEN coalesce(v_vcPhone,'')  ELSE  vcComPhone END
      WHERE
      numDomainID = v_numDomainID AND numDivisionID = v_numRecordID;
   end if;

   IF v_numAddressID = 0 then

      IF v_bitIsPrimary = true then
	
         UPDATE AddressDetails SET bitIsPrimary = false WHERE numDomainID = v_numDomainID AND numRecordID = v_numRecordID AND tintAddressOf = v_tintAddressOf
         AND tintAddressType = v_tintAddressType;
      end if;
      IF NOT EXISTS(SELECT
      numAddressID
      FROM
      AddressDetails
      WHERE
      numRecordID = v_numRecordID
      AND numDomainID = v_numDomainID
      AND tintAddressOf = v_tintAddressOf
      AND tintAddressType = v_tintAddressType
      AND vcStreet = v_vcStreet
      AND vcCity = v_vcCity
      AND vcPostalCode = v_vcPostalCode
      AND numState = v_numState
      AND numCountry = v_numCountry) then
	
         INSERT INTO AddressDetails(vcAddressName,
				vcStreet,
				vcCity,
				vcPostalCode,
				numState,
				numCountry,
				bitIsPrimary,
				tintAddressOf,
				tintAddressType,
				numRecordID,
				numDomainID,
				bitResidential,
				numContact,
				bitAltContact,
				vcAltContact)
			VALUES(v_vcAddressName,
				v_vcStreet,
				v_vcCity,
				v_vcPostalCode,
				v_numState,
				v_numCountry,
				v_bitIsPrimary,
				v_tintAddressOf,
				v_tintAddressType,
				v_numRecordID,
				v_numDomainID,
				v_bitResidential,
				v_numContact,
				v_bitAltContact,
				v_vcAltContact) RETURNING numAddressID  INTO v_numAddressID;
      ELSE
         select   numAddressID INTO v_numAddressID FROM
         AddressDetails WHERE
         numRecordID = v_numRecordID
         AND numDomainID = v_numDomainID
         AND tintAddressOf = v_tintAddressOf
         AND tintAddressType = v_tintAddressType
         AND vcStreet = v_vcStreet
         AND vcCity = v_vcCity
         AND vcPostalCode = v_vcPostalCode
         AND numState = v_numState
         AND numCountry = v_numCountry    LIMIT 1;
         UPDATE
         AddressDetails
         SET
         bitIsPrimary = v_bitIsPrimary,bitResidential = v_bitResidential,vcAddressName = v_vcAddressName,
         numContact = v_numContact,bitAltContact = v_bitAltContact,
         vcAltContact = v_vcAltContact
         WHERE
         numRecordID = v_numRecordID
         AND numDomainID = v_numDomainID
         AND tintAddressOf = v_tintAddressOf
         AND tintAddressType = v_tintAddressType
         AND vcStreet = v_vcStreet
         AND vcCity = v_vcCity
         AND vcPostalCode = v_vcPostalCode
         AND numState = v_numState
         AND numCountry = v_numCountry;
      end if;
   ELSEIF v_numAddressID > 0
   then

      select   tintAddressOf, tintAddressType, numRecordID INTO v_tintAddressOf,v_tintAddressType,v_numRecordID FROM AddressDetails WHERE numAddressID = v_numAddressID;
      IF v_bitIsPrimary = true then
	
         UPDATE AddressDetails SET bitIsPrimary = false WHERE numDomainID = v_numDomainID
         AND numRecordID = v_numRecordID AND tintAddressOf = v_tintAddressOf AND tintAddressType = v_tintAddressType;
      end if;
      UPDATE AddressDetails
      SET
      vcAddressName = v_vcAddressName,vcStreet = v_vcStreet,vcCity = v_vcCity,vcPostalCode = v_vcPostalCode,
      numState = v_numState,numCountry = v_numCountry,
      bitIsPrimary = v_bitIsPrimary,bitResidential = v_bitResidential,numContact = v_numContact,
      bitAltContact = v_bitAltContact,vcAltContact = v_vcAltContact
      WHERE
      numDomainID = v_numDomainID
      AND numAddressID = v_numAddressID;
      If v_tintAddressOf = 2 then --Organization Address
	
         IF coalesce(v_numUserCntID,0) > 0 then
		
            IF NOT EXISTS(SELECT ID FROM OrgOppAddressModificationHistory WHERE numRecordID = v_numRecordID AND tintAddressOf = 1 AND tintAddressType = v_tintAddressType AND numAddressID = v_numAddressID) then
			
               INSERT INTO OrgOppAddressModificationHistory(tintAddressOf
					,tintAddressType
					,numRecordID
					,numAddressID
					,numModifiedBy
					,dtModifiedDate)
				VALUES(1
					,v_tintAddressType
					,v_numRecordID
					,v_numAddressID
					,v_numUserCntID
					,TIMEZONE('UTC',now()));
            ELSE
               UPDATE
               OrgOppAddressModificationHistory
               SET
               numModifiedBy = v_numUserCntID,dtModifiedDate = TIMEZONE('UTC',now())
               WHERE
               numRecordID = v_numRecordID
               AND numAddressID = v_numAddressID
               AND tintAddressOf = 1
               AND tintAddressType = v_tintAddressType;
            end if;
         end if;
      end if;
   end if;
   RETURN;
END; $$;


