-- Stored procedure definition script USP_GET_CONTROL_TYPE_FOR_IMPORT for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GET_CONTROL_TYPE_FOR_IMPORT(v_intImportFileID	BIGINT,
	v_numDomainID		NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT PARENT.intImportFileID,
			vcImportFileName,
			PARENT.numDomainID,
		    DYNAMIC_FIELD.numFieldID AS numFormFieldId,
		    DYNAMIC_FIELD.vcAssociatedControlType,
		    DYNAMIC_FIELD.vcDbColumnName,
		    DYNAMIC_FIELD.vcFieldName AS vcFormFieldName,
		    FIELD_MAP.intMapColumnNo
   FROM Import_File_Master PARENT
   INNER JOIN Import_File_Field_Mapping FIELD_MAP ON PARENT.intImportFileID = FIELD_MAP.intImportFileID
   INNER JOIN DycFieldMaster DYNAMIC_FIELD ON FIELD_MAP.numFormFieldID = DYNAMIC_FIELD.numFieldID
   AND DYNAMIC_FIELD.numFieldID = FIELD_MAP.numFormFieldID
   WHERE vcAssociatedControlType = 'SelectBox'
   AND PARENT.intImportFileID = v_intImportFileID
   AND PARENT.numDomainID = v_numDomainID;
END; $$;

