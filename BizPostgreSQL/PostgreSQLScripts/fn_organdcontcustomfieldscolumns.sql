CREATE OR REPLACE FUNCTION fn_OrgAndContCustomFieldsColumns(v_numLocationId SMALLINT, v_ReturnType SMALLINT)
RETURNS TEXT LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcCustomOrgContactFieldList TEXT;
BEGIN
   IF v_numLocationId  = 1 then  --ORGANIZATION          
 
      IF v_ReturnType = 0 then --Column list along with their datatypes          
         select STRING_AGG('CFld' || SUBSTR(CAST(cfm.Fld_id AS VARCHAR(30)),1,30) || ' NVarchar(50)',',') INTO v_vcCustomOrgContactFieldList FROM CFW_Fld_Master cfm WHERE cfm.Grp_id = 1;
      end if;
      IF v_ReturnType = 1 then --Column list          
         select string_agg('CFld' || SUBSTR(CAST(cfm.Fld_id AS VARCHAR(30)),1,30),',') INTO v_vcCustomOrgContactFieldList FROM CFW_Fld_Master cfm WHERE cfm.Grp_id = 1;
      end if;
      IF v_ReturnType = 2 then --Function list to populate the data          
  
         select string_agg('(SELECT Case WHEN M.Fld_type = ''CheckBox'' THEN cfcov' || SUBSTR(CAST(cfm.Fld_id AS VARCHAR(30)),1,30) || '.Fld_Value WHEN M.Fld_type = ''SelectBox'' THEN dbo.fn_AdvSearchColumnName(cfcov' || SUBSTR(CAST(cfm.Fld_id AS VARCHAR(30)),1,30) || '.Fld_Value,''L'')   
     ELSE cfcov' || SUBSTR(CAST(cfm.Fld_id AS VARCHAR(30)),1,30) || '.Fld_Value END FROM CFW_Fld_Master M, CFW_Fld_Values cfcov' || SUBSTR(CAST(cfm.Fld_id AS VARCHAR(30)),1,30) || ' WHERE M.Fld_ID = ' || SUBSTR(CAST(cfm.Fld_id AS VARCHAR(30)),1,30) || ' AND recId = numDivisionId AND   
     cfcov' || SUBSTR(CAST(cfm.Fld_id AS VARCHAR(30)),1,30) || '.Fld_Id = M.Fld_Id AND M.Grp_Id = 1 LIMIT 1) As CFld' || SUBSTR(CAST(cfm.Fld_id AS VARCHAR(30)),1,30),',')
          INTO v_vcCustomOrgContactFieldList FROM CFW_Fld_Master cfm WHERE cfm.Grp_id = 1;
      end if;
   ELSEIF v_numLocationId = 4
   then --CONTACTS          
 
      IF v_ReturnType = 0 then --Column list along with their datatypes          
  
         select string_agg('CFld' || SUBSTR(CAST(cfm.Fld_id AS VARCHAR(30)),1,30) || ' NVarchar(50)',',') INTO v_vcCustomOrgContactFieldList FROM CFW_Fld_Master cfm WHERE cfm.Grp_id = 4;
      end if;
      IF v_ReturnType = 1 then --Column list          
  
         select string_agg('CFld' || SUBSTR(CAST(cfm.Fld_id AS VARCHAR(30)),1,30),',') INTO v_vcCustomOrgContactFieldList FROM CFW_Fld_Master cfm WHERE cfm.Grp_id = 4;
      end if;
      IF v_ReturnType = 2 then --Function list to populate the data          
  
         select string_agg('(SELECT Case WHEN M.Fld_type = ''CheckBox'' THEN cfcnv' || SUBSTR(CAST(cfm.Fld_id AS VARCHAR(30)),1,30) || '.Fld_Value WHEN M.Fld_type = ''SelectBox'' THEN dbo.fn_AdvSearchColumnName(cfcnv' || SUBSTR(CAST(cfm.Fld_id AS VARCHAR(30)),1,30) || '.Fld_Value,''L'')   
     ELSE cfcnv' || SUBSTR(CAST(cfm.Fld_id AS VARCHAR(30)),1,30) || '.Fld_Value END FROM CFW_Fld_Master M, CFW_Fld_Values_Cont cfcnv' || SUBSTR(CAST(cfm.Fld_id AS VARCHAR(30)),1,30) || ' WHERE M.Fld_ID = ' || SUBSTR(CAST(cfm.Fld_id AS VARCHAR(30)),1,30) || ' AND recId = numContactId   
    AND cfcnv' || SUBSTR(CAST(cfm.Fld_id AS VARCHAR(30)),1,30) || '.Fld_Id = M.Fld_Id AND M.Grp_Id = 4 LIMIT 1) As CFld' || SUBSTR(CAST(cfm.Fld_id AS VARCHAR(30)),1,30),',') INTO v_vcCustomOrgContactFieldList FROM CFW_Fld_Master cfm WHERE cfm.Grp_id = 4;
      end if;
   end if;          
   RETURN v_vcCustomOrgContactFieldList;
END; $$;

