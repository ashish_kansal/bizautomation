-- Stored procedure definition script USP_CompanyTransactionDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_CompanyTransactionDetails(v_numDomainId NUMERIC(9,0) DEFAULT 0,
      v_numDivisionId NUMERIC(9,0) DEFAULT 0,
      v_ClientTimeZoneOffset INTEGER DEFAULT NULL ,
      v_CurrentPage INTEGER DEFAULT NULL,
	  v_PageSize INTEGER DEFAULT NULL,
      INOUT v_TotRecs INTEGER    DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql

-- show transaction details of last 12 months 
   AS $$
   DECLARE
   v_numAuthoritativePurchase  NUMERIC(18,0);
   v_numAuthoritativeSales  NUMERIC(18,0);
   v_firstRec  INTEGER;
   v_lastRec  INTEGER;
   v_StartDate  TIMESTAMP;
   v_EndDate  TIMESTAMP;
   v_year  VARCHAR(5);    
   v_Month  VARCHAR(5);   
   v_day  VARCHAR(5);
BEGIN
   v_numAuthoritativePurchase := 0;
   v_numAuthoritativeSales := 0;
   select   coalesce(numAuthoritativePurchase,0), coalesce(numAuthoritativeSales,0) INTO v_numAuthoritativePurchase,v_numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = v_numDomainId; 

   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);
   v_year := CAST(EXTRACT(YEAR FROM TIMEZONE('UTC',now())) AS VARCHAR(5));     
   v_Month := CAST(EXTRACT(MONTH FROM TIMEZONE('UTC',now())) AS VARCHAR(5));    
   IF cast(NULLIF(v_Month,'') as INTEGER) < 10 then 
      v_Month := '0' || coalesce(v_Month,'');
   end if;   
		
   v_day := CAST(CASE WHEN v_Month = '01'
   THEN 31
   WHEN v_Month = '02'
   THEN CASE WHEN (MOD(v_year:: NUMERIC,4) = 0 AND MOD(v_year:: NUMERIC,100) <> 0) OR
      MOD(v_year:: NUMERIC,400) = 0
      THEN 29
      ELSE 28
      END
   WHEN v_Month = '03'
   THEN 31
   WHEN v_Month = '04'
   THEN 30
   WHEN v_Month = '05'
   THEN 31
   WHEN v_Month = '06'
   THEN 30
   WHEN v_Month = '07'
   THEN 31
   WHEN v_Month = '08'
   THEN 31
   WHEN v_Month = '09'
   THEN 30
   WHEN v_Month = '10'
   THEN 31
   WHEN v_Month = '11'
   THEN 30
   WHEN v_Month = '12'
   THEN 31 END AS VARCHAR(5));  
		  
   v_EndDate := CAST(coalesce(v_Month,'') || '/' || coalesce(v_day,'') || '/' || coalesce(v_year,'') AS TIMESTAMP); 
   v_year := CAST(EXTRACT(year FROM (TIMEZONE('UTC',now())::TIMESTAMP) + make_interval(days => -365)) AS VARCHAR(5));     
   v_Month := CAST(EXTRACT(month FROM (TIMEZONE('UTC',now())::TIMESTAMP) + make_interval(days => -365)) AS VARCHAR(5));  
   v_StartDate := CAST(coalesce(v_Month,'') || '/01/' || coalesce(v_year,'') AS TIMESTAMP);
	
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTABLE 
   (
      ID INTEGER   GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1)   PRIMARY KEY,
      numOppID NUMERIC(9,0),
      vcData VARCHAR(1000),
      Name VARCHAR(1000),
      TotalAmt DECIMAL(20,5),
      TotalBizDocAmt DECIMAL(20,5),
      AmountPaid DECIMAL(20,5),
      OppType VARCHAR(50),
      BalanceAmt DECIMAL(20,5),
      DueDate VARCHAR(50),
      Status  BOOLEAN,
      numOppBizDocsId INTEGER,
      dtCreatedDate VARCHAR(50),
      vcBizDocIdName VARCHAR(500) 
   );
   INSERT INTO tt_TEMPTABLE(numOppID, vcData, Name, TotalAmt, TotalBizDocAmt, AmountPaid, OppType, BalanceAmt, DueDate, Status, numOppBizDocsId, dtCreatedDate, vcBizDocIdName)
   SELECT  Opp.numOppId,
                LD.vcData AS vcData,
                CAST(Opp.vcpOppName AS VARCHAR(1000)) AS Name,
                Opp.monDealAmount AS TotalAmt,
                OppBizDocs.monDealAmount AS TotalBizDocAmt,
                coalesce(OppBizDocs.monAmountPaid,0) AS AmountPaid,
                CAST(CASE WHEN tintopptype = 1 THEN 'S.O'
   ELSE CASE WHEN tintopptype = 2 THEN 'P.O'
      END
   END AS VARCHAR(50)) AS OppType,
--   dateadd(minute,-@ClientTimeZoneOffset,OppBizDocsDet.dtCreationDate) as datePaid,    
                --isnull(Opp.monPAmount,0) 
                coalesce(OppBizDocs.monDealAmount,0) -coalesce(OppBizDocs.monAmountPaid,0)  AS BalanceAmt,
--                dbo.FormatedDateFromDate(DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
--														  THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0))  
--														  ELSE 0 
--													 END,OppBizDocs.[dtFromDate]),@numDomainId) AS DueDate,
				CAST(FormatedDateFromDate(OppBizDocs.dtFromDate+CAST(CASE WHEN Opp.bitBillingTerms = true
   THEN CAST(coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(Opp.intBillingDays,0)),0) AS INTEGER)
   ELSE 0
   END || 'day' as interval),v_numDomainId) AS VARCHAR(50)) AS DueDate,
                CAST(CASE when  coalesce(OppBizDocs.monDealAmount,0) -coalesce(OppBizDocs.monAmountPaid,0) = 0 THEN 0
--					WHEN DATEDIFF(Day,
--                                   DATEADD(day,
--                                           CASE WHEN Opp.bitBillingTerms = 1
--                                                THEN  convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0)) 
--                                                ELSE 0
--                                           END, OppBizDocs.[dtFromDate]),
--                                   GETDATE()) > 0 THEN 1
   WHEN DATE_PART('day',LOCALTIMESTAMP  -OppBizDocs.dtFromDate+CAST(CASE WHEN Opp.bitBillingTerms = true
   THEN  CAST(coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(Opp.intBillingDays,0)),0) AS INTEGER)
   ELSE 0
   END || 'day' as interval)) > 0 THEN 1
   ELSE 0
   END AS BOOLEAN) AS Status,
                OppBizDocs.numOppBizDocsId,
                CAST(FormatedDateFromDate(OppBizDocs.dtCreatedDate,v_numDomainId) AS VARCHAR(50)) AS dtCreatedDate,CAST('BD-' || SUBSTR(OppBizDocs.vcBizDocID,LENGTH(OppBizDocs.vcBizDocID) -POSITION('-' IN REVERSE(OppBizDocs.vcBizDocID))+2,POSITION('-' IN REVERSE(vcBizDocID))) AS VARCHAR(500)) AS vcBizDocIdName
   FROM    OpportunityMaster Opp
   INNER JOIN OpportunityBizDocs OppBizDocs ON OppBizDocs.numoppid = Opp.numOppId
   INNER JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId
   INNER JOIN DivisionMaster Div ON Opp.numDivisionId = Div.numDivisionID
   AND ADC.numDivisionId = Div.numDivisionID
   INNER JOIN CompanyInfo C ON Div.numCompanyID = C.numCompanyId
   INNER JOIN Listdetails LD ON OppBizDocs.numBizDocId = LD.numListItemID
   WHERE   Opp.numDomainId = v_numDomainId
--                AND Opp.tintOppStatus = 1
   AND Opp.numDivisionId = v_numDivisionId
   AND Div.numDomainID = v_numDomainId
   AND OppBizDocs.bitAuthoritativeBizDocs = 1
   AND CAST(TO_CHAR(OppBizDocs.dtCreatedDate,'dd Mon yyyy') AS TIMESTAMP) >= v_StartDate
   AND CAST(TO_CHAR(OppBizDocs.dtCreatedDate,'dd Mon yyyy') AS TIMESTAMP) <= v_EndDate
   AND OppBizDocs.numBizDocId IN(v_numAuthoritativePurchase,v_numAuthoritativeSales);
--                AND ( OppBizDocs.numBizDocId IN (
--                      SELECT    numAuthoritativePurchase
--                      FROM      AuthoritativeBizDocs
--                      WHERE     numDomainId = @numDomainId )
--                      OR OppBizDocs.numBizDocId IN (
--                      SELECT    numAuthoritativeSales
--                      FROM      AuthoritativeBizDocs
--                      WHERE     numDomainId = @numDomainId )
--                    )


--        GROUP BY Opp.[numOppId],
--                [vcData],
--                Opp.vcPOppName,
--                [monPAmount],
--                Opp.monDealAmount,
--                Opp.[tintOppType],
--                Opp.bitBillingTerms,
--                Opp.[intBillingDays],
--                OppBizDocs.[dtCreatedDate],
--                OppBizDocs.[numOppBizDocsId],
--                OppBizDocs.[monDealAmount],
--                OppBizDocs.dtFromDate,OppBizDocs.vcBizDocId --OppBizDocsDet.monAmount
                
   SELECT COUNT(*) INTO v_TotRecs FROM tt_TEMPTABLE;
   open SWV_RefCur for
   SELECT * FROM tt_TEMPTABLE WHERE ID > v_firstRec AND ID < v_lastRec;

   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_ContractManage]    Script Date: 07/26/2008 16:15:21 ******/



