-- Stored procedure definition script USP_RepPortal for PostgreSQL
CREATE OR REPLACE FUNCTION USP_RepPortal(v_numDomainID NUMERIC,                
  v_dtFromDate TIMESTAMP,                
  v_dtToDate TIMESTAMP,                
  v_numUserCntID NUMERIC DEFAULT 0,                     
  v_tintType SMALLINT DEFAULT NULL,
  INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	select 
		Com.numCompanyId
		,div.tintCRMType
		,fn_GetPrimaryContact(div.numDivisionID) as ContactID
		,div.numDivisionID
		,(select max(bintLastLoggedIn) from ExtranetAccountsDtl E where E.numExtranetID = Ext.numExtranetID) as LastLoggedIn
		,vcCompanyName || ' ,' || vcDivisionName as Company
		,fn_GetListItemName(numCompanyType) as RelationShip
		,coalesce((select sum(numNoOfTimes) from ExtranetAccountsDtl E where E.numExtranetID = Ext.numExtranetID),0) as Times
		,(select count(*) from OpportunityMaster where tintopptype = 1 and tintoppstatus = 1 and numDivisionId = div.numDivisionID) as NoofDeals
		,coalesce((select sum(monPAmount) from OpportunityMaster where tintopptype = 1 and tintoppstatus = 1 and numDivisionId = div.numDivisionID),0) as AmtWon
   from ExtarnetAccounts Ext
   join DivisionMaster div
   on div.numDivisionID = Ext.numDivisionID
   join CompanyInfo Com
   on Com.numCompanyId = div.numCompanyID
   where div.numTerID in(select F.numTerritory from ForReportsByTerritory F
      where F.numUserCntId = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_tintType)
   and  div.bintCreatedDate >= v_dtFromDate AND div.bintCreatedDate <= v_dtToDate
   and div.numDomainID = v_numDomainID;
END; $$;












