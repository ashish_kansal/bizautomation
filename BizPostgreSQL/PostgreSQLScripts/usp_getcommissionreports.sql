-- Stored procedure definition script usp_GetCommissionReports for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetCommissionReports(v_numContactID NUMERIC,
v_numDomainID NUMERIC,
v_bitCommContact BOOLEAN, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintComAppliesTo  SMALLINT;
BEGIN
   select   coalesce(tintComAppliesTo,0) INTO v_tintComAppliesTo FROM Domain WHERE numDomainId = v_numDomainID;

   if v_tintComAppliesTo = 1 then

      open SWV_RefCur for
      select CR.numComReports,CR.numContactID,I.vcItemName,CASE tintAssignTo WHEN 1 THEN 'Assign To' WHEN 2 THEN 'Record Owner' END AS ReportOwner
 ,CASE WHEN  v_bitCommContact = true THEN fn_GetComapnyName(CR.numContactID) ELSE '' END AS vcCompanyName
      from CommissionReports CR JOIN Item I ON CR.numItemCode = I.numItemCode
      WHERE CR.numContactID =(CASE WHEN v_bitCommContact = true THEN CR.numContactID ELSE v_numContactID END) AND CR.numDomainID = v_numDomainID AND CR.bitCommContact = v_bitCommContact;
   ELSEIF v_tintComAppliesTo = 2
   then

      open SWV_RefCur for
      select CR.numComReports,CR.numContactID,GetListIemName(numItemCode) as vcItemName,CASE tintAssignTo WHEN 1 THEN 'Assign To' WHEN 2 THEN 'Record Owner' END AS ReportOwner
 ,CASE WHEN  v_bitCommContact = true THEN fn_GetComapnyName(CR.numContactID) ELSE '' END AS vcCompanyName
      from CommissionReports CR where CR.numContactID =(CASE WHEN v_bitCommContact = true THEN CR.numContactID ELSE v_numContactID END)  AND CR.numDomainID = v_numDomainID  AND CR.bitCommContact = v_bitCommContact;
   ELSEIF v_tintComAppliesTo = 3
   then

      open SWV_RefCur for
      select CR.numComReports,CR.numContactID,'All Items' as vcItemName,CASE tintAssignTo WHEN 1 THEN 'Assign To' WHEN 2 THEN 'Record Owner' END AS ReportOwner
 ,CASE WHEN  v_bitCommContact = true THEN fn_GetComapnyName(CR.numContactID) ELSE '' END AS vcCompanyName
      from CommissionReports CR where CR.numContactID =(CASE WHEN v_bitCommContact = true THEN CR.numContactID ELSE v_numContactID END)  AND CR.numDomainID = v_numDomainID  AND CR.bitCommContact = v_bitCommContact;
   end if;
   RETURN;
END; $$;  



