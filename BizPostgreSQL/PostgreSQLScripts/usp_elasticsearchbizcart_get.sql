CREATE OR REPLACE FUNCTION USP_ElasticSearchBizCart_Get(INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor , INOUT SWV_RefCur3 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      ID NUMERIC(18,0),
      numDomainID NUMERIC(18,0),
      vcModule VARCHAR(15),
      vcValue VARCHAR(200),
      vcAction VARCHAR(10)
		--,numSiteID NUMERIC(18,0)
   );
   INSERT INTO tt_TEMP(ID,numDomainID,vcModule,vcValue,vcAction)--,numSiteID) 
   SELECT  numElasticSearchBizCartID,numDomainID,vcModule,vcValue,vcAction--,numSiteID 
   FROM ElasticSearchBizCart ORDER BY numElasticSearchBizCartID LIMIT 400;
   DELETE FROM ElasticSearchBizCart WHERE numElasticSearchBizCartID IN(SELECT ID FROM tt_TEMP);

	--,T1.numSiteId
   open SWV_RefCur for
   SELECT * FROM(SELECT MIN(ID) AS ID,T1.numDomainID,vcModule,vcValue,vcAction--,T1.numSiteId 
      FROM tt_TEMP T1 
		--INNER JOIN eCommerceDTL ECD ON T1.numDomainID=ECD.numDomainId 
      WHERE vcAction = 'Insert'
      GROUP BY T1.numDomainID,vcModule,vcValue,vcAction) TEMP
   ORDER BY ID;
	
	
	--,T1.numSiteId
   open SWV_RefCur2 for
   SELECT * FROM(SELECT MIN(ID) AS ID,T1.numDomainID,vcModule,vcValue,vcAction --,T1.numSiteId
      FROM tt_TEMP T1 
		--INNER JOIN eCommerceDTL ECD ON T1.numDomainID=ECD.numDomainId 
      WHERE vcAction = 'Update' 
      GROUP BY T1.numDomainID,vcModule,vcValue,vcAction) TEMP
   ORDER BY ID;
	
	--,T1.numSiteId
   open SWV_RefCur3 for
   SELECT * FROM(SELECT MIN(ID) AS ID,T1.numDomainID,vcModule,vcValue,vcAction --,T1.numSiteId
      FROM tt_TEMP T1 
		--INNER JOIN eCommerceDTL ECD ON T1.numDomainID=ECD.numDomainId 
      WHERE vcAction = 'Delete'
      GROUP BY T1.numDomainID,vcModule,vcValue,vcAction) TEMP
   ORDER BY ID;
   RETURN;
END; $$;


