-- Stored procedure definition script usp_TicklerAssignedStages for PostgreSQL
CREATE OR REPLACE FUNCTION usp_TicklerAssignedStages(v_numUserCntID NUMERIC(9,0) DEFAULT null,                            
v_numDomainID NUMERIC(9,0) DEFAULT null,                            
v_startDate TIMESTAMP DEFAULT NULL,                            
v_endDate TIMESTAMP DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select opp.numOppId as ID,vcpOppName,cast((select cast(sum(tintPercentage) as NUMERIC(18,0)) from OpportunityStageDetails where numoppid = opp.numOppId and bitstagecompleted = true) as VARCHAR(255)) as Tprogress,
cast(OppStg.bintDueDate as VARCHAR(255)) AS closeDate,
opp.numrecowner,
AddC.vcFirstName || ' ' || AddC.vcLastname as Name ,
case when AddC.numPhone <> '' then AddC.numPhone || case when AddC.numPhoneExtension <> '' then ' - ' || AddC.numPhoneExtension else '' end  else '' end as Phone,
cast(Com.vcCompanyName as VARCHAR(255)),
CAST('' AS CHAR(1)) as vcEmail,
case when opp.tintopptype = 1 then 'Sales Opportunity' when opp.tintopptype = 2 then 'Purchase Opportunity' end as Type,
Div.numTerID,
CAST(numstagepercentage AS VARCHAR(50)) as numstagepercentage,
cast(vcStageDetail as VARCHAR(255))  ,
fn_GetContactName(OppStg.numAssignTo) || '/' ||   fn_GetContactName(v_numUserCntID) AS AssignedToBy
   from OpportunityMaster opp
   join AdditionalContactsInformation AddC
   on AddC.numContactId = opp.numContactId
   join DivisionMaster Div
   on Div.numDivisionID = opp.numDivisionId
   join CompanyInfo Com
   on Com.numCompanyId = Div.numCompanyID
   join OpportunityStageDetails OppStg
   on OppStg.numoppid = opp.numOppId
   where   Com.numDomainID = v_numDomainID and OppStg.numAssignTo = v_numUserCntID
   and bitStageCompleted = false
   and tintoppstatus = 0 and (bintDueDate  >= v_startDate and bintDueDate  <= v_endDate)
   union
   select cast(pro.numProId as NUMERIC(18,0)) as ID,cast(vcProjectName as VARCHAR(100)) as vcPOppname,cast((select cast(sum(tintPercentage) as NUMERIC(18,0)) from ProjectsStageDetails where numProId = pro.numProId and bitStageCompleted = true) as VARCHAR(255)) as Tprogress,
cast(ProStg.bintDueDate as VARCHAR(255)) AS closeDate,
cast(pro.numRecOwner as NUMERIC(18,0)),                                              
--AddC.vcFirstName+', '+numPhone +'- '+numPhoneExtension as Name ,         
AddC.vcFirstName || ' ' || AddC.vcLastname as Name ,
case when AddC.numPhone <> '' then AddC.numPhone || case when AddC.numPhoneExtension <> '' then ' - ' || AddC.numPhoneExtension else '' end  else '' end as Phone,
cast(Com.vcCompanyName as VARCHAR(255)),
CAST('' AS CHAR(1)) as vcEmail,
 CAST('Project' AS VARCHAR(20)) as Type,
Div.numTerID,
CAST(numstagepercentage AS VARCHAR(50)) as numstagepercentage,
cast(vcStageDetail as VARCHAR(255)) ,
fn_GetContactName(ProStg.numAssignTo) || '/' ||   fn_GetContactName(v_numUserCntID) AS AssignedToBy
   from ProjectsMaster pro
   left join AdditionalContactsInformation AddC
   on AddC.numContactId = pro.numCustPrjMgr
   join DivisionMaster Div
   on Div.numDivisionID = pro.numDivisionId
   join CompanyInfo Com
   on Com.numCompanyId = Div.numCompanyID
   join ProjectsStageDetails ProStg
   on ProStg.numProId = pro.numProId
   where  Com.numDomainID = v_numDomainID
   and ProStg.numAssignTo = v_numUserCntID
   and bitStageCompleted = false
   and tintProStatus = 0 and (bintDueDate  >= v_startDate and bintDueDate  <= v_endDate);
END; $$;












