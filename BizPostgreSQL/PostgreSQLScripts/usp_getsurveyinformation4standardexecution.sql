-- Stored procedure definition script usp_GetSurveyInformation4StandardExecution for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetSurveyInformation4StandardExecution(v_numSurId NUMERIC, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor , INOUT SWV_RefCur3 refcursor , INOUT SWV_RefCur4 refcursor , INOUT SWV_RefCur5 refcursor)
LANGUAGE plpgsql                  
--This procedure will return all survey master information for the selected survey                
   AS $$
BEGIN
   open SWV_RefCur for
   SELECT numSurID, vcSurName, vcRedirectURL, bitSkipRegistration, bitRandomAnswers, bitSinglePageSurvey
   FROM SurveyMaster
   WHERE numSurID = v_numSurId;           
                 
   open SWV_RefCur2 for
   SELECT numSurID, numQID, vcQuestion, tIntAnsType, intNumOfAns,boolMatrixColumn,intNumOfMatrixColumn
   FROM SurveyQuestionMaster
   WHERE numSurID = v_numSurId
   ORDER BY numQID;        
              
   open SWV_RefCur3 for
   SELECT numSurID, numQID, numAnsID, vcAnsLabel, boolSurveyRuleAttached, 0 as boolDeleted
   FROM SurveyAnsMaster
   WHERE  numSurID = v_numSurId
   ORDER BY numQID,numAnsID;                    
              
   open SWV_RefCur4 for
   SELECT numRuleID, numSurID, numAnsID, numQID, boolActivation, numEventOrder,
  vcQuestionList, numLinkedSurID, numResponseRating, vcPopUpURL, numGrpId, numCompanyType,
  tIntCRMType, numRecOwner,tIntRuleFourRadio,tIntCreateNewID,vcRuleFourSelectFields,boolPopulateSalesOpportunity,vcRuleFiveSelectFields,numMatrixID,vcItem
   FROM SurveyWorkflowRules
   WHERE  numSurID = v_numSurId
   ORDER BY numSurID,numQID,numAnsID,numEventOrder;   
  
   open SWV_RefCur5 for
   SELECT numSurID, numQID, numMatrixID, vcAnsLabel, boolSurveyRuleAttached, 0 as boolDeleted
   FROM SurveyMatrixAnsMaster
   WHERE  numSurID = v_numSurId
   ORDER BY numQID,numMatrixID;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[usp_GetSurveyMasterInfo]    Script Date: 07/26/2008 16:18:35 ******/



