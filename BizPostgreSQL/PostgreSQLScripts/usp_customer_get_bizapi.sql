CREATE OR REPLACE FUNCTION USP_Customer_Get_BIZAPI(v_numDomainID NUMERIC(18,0) DEFAULT NULL,
	v_SearchText VARCHAR(100) DEFAULT NULL,
	v_numPageIndex INTEGER DEFAULT 0,
    v_numPageSize INTEGER DEFAULT 0,
    INOUT v_TotalRecords INTEGER  DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   select   COUNT(*) INTO v_TotalRecords FROM
   CompanyInfo
   INNER JOIN
   DivisionMaster
   ON
   CompanyInfo.numCompanyId = DivisionMaster.numCompanyID WHERE
   CompanyInfo.numDomainID = v_numDomainID AND (v_SearchText = '' OR CompanyInfo.vcCompanyName ilike '%' || coalesce(v_SearchText,'') || '%');

   open SWV_RefCur for
   SELECT * FROM(SELECT
      ROW_NUMBER() OVER(ORDER BY CompanyInfo.vcCompanyName asc) AS RowNumber,
			DivisionMaster.numDivisionID,
			CompanyInfo.vcCompanyName,
			COALESCE((SELECT string_agg(CONCAT('<Contact'
						,' numContactId="',numContactId,'"'
						,' Name="',vcFirstName || ' ' || vcLastname,'"'
						,' vcFirstName="',coalesce(vcFirstName,''),'"'
						,' vcLastName="',coalesce(vcLastname,''),'"'
						,' numPhoneExtension="',coalesce(numPhoneExtension,''),'"'
						,' numPhone="',coalesce(numPhone,''),'"'
						,' vcEmail="',coalesce(vcEmail,''),'"'
						,' vcFax="',coalesce(vcFax,''),'"'
						,' bitPrimaryContact="',coalesce(bitPrimaryContact,false),'"'
						,' numContactType="',GetListIemName(coalesce(numContactType,0)),'"'
						,' vcDepartment="',GetListIemName(coalesce(vcDepartment,0)),'"'
						,' vcPosition="',GetListIemName(coalesce(vcPosition,0)),'"' ,'/>'),'')
						
            FROM
            AdditionalContactsInformation
            WHERE
            AdditionalContactsInformation.numDomainID = v_numDomainID AND
            AdditionalContactsInformation.numDivisionId = DivisionMaster.numDivisionID AND
						(coalesce(vcFirstName,'')  <> '' OR coalesce(vcLastname,'') <> '')),null) AS Contacts
      FROM
      CompanyInfo
      INNER JOIN
      DivisionMaster
      ON
      CompanyInfo.numCompanyId = DivisionMaster.numCompanyID
      WHERE
      CompanyInfo.numDomainID = v_numDomainID AND
			(v_SearchText = '' OR CompanyInfo.vcCompanyName ilike '%' || coalesce(v_SearchText,'') || '%')) AS I
   WHERE
		(v_numPageIndex = 0 OR v_numPageSize = 0) OR
		(RowNumber >((v_numPageIndex::bigint -1)*v_numPageSize::bigint) and RowNumber <((v_numPageIndex::bigint*v_numPageSize::bigint)+1));
   RETURN;
END; $$;


