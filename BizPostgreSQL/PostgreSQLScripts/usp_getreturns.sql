-- Stored procedure definition script USP_GetReturns for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetReturns( --1 = ASC, 2 = DESC
v_numReturnId NUMERIC(18,0) DEFAULT NULL,
    v_numDomainId NUMERIC(18,0) DEFAULT 0,
	v_numUserCntID NUMERIC(18,0) DEFAULT 0,
    v_numReturnStatus NUMERIC(9,0) DEFAULT NULL,
    v_numReturnReason NUMERIC(9,0) DEFAULT NULL,
    v_vcOrgSearchText VARCHAR(50) DEFAULT NULL,
    v_CurrentPage INTEGER DEFAULT NULL,
    v_PageSize INTEGER DEFAULT NULL,
    INOUT v_TotRecs INTEGER  DEFAULT NULL,
    v_ClientTimeZoneOffset INTEGER DEFAULT NULL,
    v_tintReturnType SMALLINT DEFAULT NULL,
	v_vcRMASearchText VARCHAR(50) DEFAULT NULL,
	v_vcSortColumn VARCHAR(50) DEFAULT NULL,
    v_tintSortDirection SMALLINT DEFAULT 1, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_firstRec  INTEGER;
   v_lastRec  INTEGER;
   v_strSQL  VARCHAR(500) DEFAULT '';
   v_vcSort  VARCHAR(500) DEFAULT '';
BEGIN
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);
       
   DROP TABLE IF EXISTS tt_TEMPSALESRETURN CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPSALESRETURN
   (
      numOppId NUMERIC(18,0),
      numReturnHeaderID NUMERIC(18,0),
      vcRMA VARCHAR(300),
      numReturnStatus NUMERIC(18,0),
      numReturnReason NUMERIC(18,0),
      numDivisionId NUMERIC(18,0),
      dtCreateDate VARCHAR(50),
      dtOriginal TIMESTAMP,
      tintReturnType SMALLINT,
      tintReceiveType SMALLINT,
      vcCompanyName VARCHAR(300),
      numAccountID NUMERIC(18,0),
      reasonforreturn VARCHAR(200),
      status VARCHAR(200),
      vcCreditFromAccount VARCHAR(300),
      IsEditable BOOLEAN,
      vcBizDocName VARCHAR(300)
   );
	
   INSERT INTO
   tt_TEMPSALESRETURN
   SELECT
   RH.numOppId,
			RH.numReturnHeaderID,
			vcRMA,
			RH.numReturnStatus,
			RH.numReturnReason,
			RH.numDivisionId,
			CAST(CASE WHEN CAST(RH.dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(11)) = CAST(LOCALTIMESTAMP AS VARCHAR(11))
   THEN '<b><font color=red>Today</font></b>'
   WHEN CAST(RH.dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(11)) = CAST(LOCALTIMESTAMP+INTERVAL '-1 day' AS VARCHAR(11))
   THEN '<b><font color=purple>YesterDay</font></b>'
   WHEN CAST(RH.dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(11)) = CAST(LOCALTIMESTAMP+INTERVAL '1 day' AS VARCHAR(11))
   THEN '<b><font color=orange>Tommorow</font></b>'
   ELSE FormatedDateFromDate(RH.dtCreatedDate,RH.numDomainID)
   END AS VARCHAR(50)) AS dtCreateDate,
			RH.dtCreatedDate,
			RH.tintReturnType,
			RH.tintReceiveType,
			SUBSTR(com.vcCompanyName,0,30) AS vcCompanyName
			,RH.numAccountID
			,CAST(GetListIemName(RH.numReturnReason) AS VARCHAR(200)) AS reasonforreturn
			,CAST(GetListIemName(RH.numReturnStatus) AS VARCHAR(200)) AS status
			,CAST(COA.vcAccountName AS VARCHAR(300)) AS vcCreditFromAccount,
			CAST(CASE GetListIemName(numreturnstatus)
   WHEN 'Pending' THEN 'True'
   WHEN 'Confirmed' THEN 'True'
   ELSE 'True'
   END AS BOOLEAN)
			,RH.vcBizDocName
   FROM
   ReturnHeader AS RH
   JOIN DivisionMaster div ON div.numDivisionID = RH.numDivisionId
   JOIN CompanyInfo com ON com.numCompanyId = div.numCompanyID
   LEFT JOIN Chart_Of_Accounts COA ON RH.numAccountID = COA.numAccountId AND COA.numDomainId = RH.numDomainID
   WHERE
			(RH.numReturnReason = v_numReturnReason OR v_numReturnReason IS NULL OR v_numReturnReason = 0)
   AND (RH.numReturnStatus = v_numReturnStatus OR v_numReturnStatus IS NULL OR v_numReturnStatus = 0)
   AND (RH.numReturnHeaderID = v_numReturnId OR v_numReturnId IS NULL)
   AND RH.numDomainID = v_numDomainId
   AND 1 =(CASE
   WHEN NOT EXISTS(SELECT numUserDetailId FROM UserMaster WHERE numUserDetailId = v_numUserCntID)
   THEN(CASE
      WHEN EXISTS(SELECT EAD.numExtranetDtlID FROM ExtranetAccountsDtl EAD INNER JOIN ExtarnetAccounts EA ON EAD.numExtranetID = EA.numExtranetID WHERE EAD.numDomainID = v_numDomainId AND EAD.numContactID = CAST(v_numUserCntID AS VARCHAR) AND EA.numDivisionID = div.numDivisionID)
      THEN 1
      ELSE 0
      END)
   ELSE 1
   END)
   AND 1 =(CASE v_tintReturnType
   WHEN 3 THEN(CASE WHEN (RH.tintReturnType = 1 AND tintReceiveType = 2) OR RH.tintReturnType = 3 THEN 1 ELSE 0 END)
   WHEN 4 THEN(CASE WHEN (RH.tintReturnType = 1 AND tintReceiveType = 1) OR RH.tintReturnType = 4 THEN 1 ELSE 0 END)
   ELSE(CASE WHEN RH.tintReturnType = v_tintReturnType THEN 1 ELSE 0 END)
   END)
   AND (RH.vcRMA ilike '%' || coalesce(v_vcRMASearchText,'') || '%' OR NULLIF(v_vcRMASearchText,'') IS NULL OR LENGTH(v_vcRMASearchText) = 0)
   AND (com.vcCompanyName ilike('%' || coalesce(v_vcOrgSearchText,'') || '%') OR NULLIF(v_vcOrgSearchText,'') IS NULL OR LENGTH(v_vcOrgSearchText) = 0);

		--SELECT * FROM  #TempSalesReturn ORDER BY vcCreditFromAccount DESC OFFSET ((1 - 1) * 20) ROWS FETCH NEXT 20 ROWS ONLY
   select   COUNT(*) INTO v_TotRecs FROM tt_TEMPSALESRETURN;
      
   v_vcSort :=(CASE
   WHEN v_vcSortColumn = 'vcCompanyName' THEN
      'vcCompanyName ' ||(CASE WHEN v_tintSortDirection = 1 THEN 'ASC' ELSE 'DESC' END)
   WHEN v_vcSortColumn = 'vcRMA' THEN
      'vcRMA ' ||(CASE WHEN v_tintSortDirection = 1 THEN 'ASC' ELSE 'DESC' END)
   WHEN v_vcSortColumn = 'vcCreditFromAccount' THEN
      'vcCreditFromAccount ' ||(CASE WHEN v_tintSortDirection = 1 THEN 'ASC' ELSE 'DESC' END)
   WHEN v_vcSortColumn = 'ReasonforReturn' THEN
      'reasonforreturn ' ||(CASE WHEN v_tintSortDirection = 1 THEN 'ASC' ELSE 'DESC' END)
   WHEN v_vcSortColumn = 'Status' THEN
      '[status] ' ||(CASE WHEN v_tintSortDirection = 1 THEN 'ASC' ELSE 'DESC' END)
   ELSE
      'dtOriginal ' ||(CASE WHEN v_tintSortDirection = 1 THEN 'ASC' ELSE 'DESC' END)
   END); 
	                   
   v_strSQL := 'SELECT * FROM(SELECT Row_Number() OVER(ORDER BY ' || coalesce(v_vcSort,'') || ',numReturnHeaderID ASC) AS row,* FROM  tt_TEMPSALESRETURN) t WHERE row > ' || SUBSTR(CAST(v_firstRec AS VARCHAR(10)),1,10) || ' AND row < ' || SUBSTR(CAST(v_lastRec AS VARCHAR(10)),1,10);

   OPEN SWV_RefCur FOR EXECUTE v_strSQL;
	
   RETURN;
END; $$;
  
--EXEC USP_GetSalesTemplateItems 6,72
--SELECT * FROM SalesTemplateItems


