-- Stored procedure definition script USP_GetFirstAuthoritativeBizDocID for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetFirstAuthoritativeBizDocID(v_numOppId NUMERIC(9,0)  DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numBizDocID  NUMERIC(9,0);
BEGIN
   select   coalesce(numOppBizDocsId,0) INTO v_numBizDocID FROM   OpportunityBizDocs WHERE  numoppid = v_numOppId
   AND coalesce(bitAuthoritativeBizDocs,0) = 1    LIMIT 1;
   open SWV_RefCur for SELECT cast(coalesce(v_numBizDocID,0) as NUMERIC(9,0));
   RETURN;
END; $$;













