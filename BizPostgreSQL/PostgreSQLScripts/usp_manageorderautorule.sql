CREATE OR REPLACE FUNCTION USP_ManageOrderAutoRule(v_numMode INTEGER,
v_numDomainID INTEGER,
v_numRuleID INTEGER,
v_numBillToID INTEGER,
v_numShipToID INTEGER,
v_btFullPaid BOOLEAN,
v_numBizDocStatus NUMERIC,
v_btActive BOOLEAN, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_numMode = 0 then
	
      delete from OrderAutoRuleDetails where numRuleID = v_numRuleID;
      insert into OrderAutoRule  
	  (numDomainID,numBillToID,numShipToID,btFullPaid,numBizDocStatus,btActive)
	  select v_numDomainID,v_numBillToID,v_numShipToID,v_btFullPaid,v_numBizDocStatus,v_btActive;
      open SWV_RefCur for
      select CURRVAL('OrderAutoRule_seq');
   ELSEIF v_numMode = 1
   then
	
      delete from OrderAutoRuleDetails where numRuleID = v_numRuleID;
      update OrderAutoRule set numBillToID = v_numBillToID,numShipToID = v_numShipToID,btFullPaid = v_btFullPaid,
      numBizDocStatus = v_numBizDocStatus,btActive = v_btActive where numDomainID = v_numDomainID and numRuleID = v_numRuleID;
      open SWV_RefCur for
      select v_numRuleID;
   ELSEIF v_numMode = 2
   then
	
      delete from OrderAutoRuleDetails where numRuleID = v_numRuleID;
      DELETE FROM OrderAutoRule WHERE  numDomainID = v_numDomainID and numRuleID = v_numRuleID;
      open SWV_RefCur for
      select v_numRuleID;
   end if;
   RETURN;
END; $$;
--select * from OrderAutoRuleDetails



