-- Stored procedure definition script usp_cfwListFieldsDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_cfwListFieldsDetails(v_TabId INTEGER DEFAULT null,  
v_loc_id SMALLINT DEFAULT null,  
v_relId NUMERIC(9,0) DEFAULT null,
v_numDomainID NUMERIC(9,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_TabId = -1 then

      open SWV_RefCur for
      Select numFieldDtlID,fld_id,fld_label,fld_type,numOrder from CFW_Fld_Master join CFW_Fld_Dtl
      on Fld_id = numFieldId
      where Grp_id = v_loc_id and numrelation = v_relId  and numDomainID = v_numDomainID
      order by numOrder;
   end if;  
  
   if v_TabId != -1 then

      open SWV_RefCur for
      Select numFieldDtlID,fld_id,fld_label,fld_type,numOrder from CFW_Fld_Master join CFW_Fld_Dtl
      on Fld_id = numFieldId
      where Grp_id = v_loc_id  and subgrp = CAST(v_TabId AS VARCHAR) and numrelation = v_relId  and numDomainID = v_numDomainID
      order by numOrder;
   end if;
   RETURN;
END; $$;


