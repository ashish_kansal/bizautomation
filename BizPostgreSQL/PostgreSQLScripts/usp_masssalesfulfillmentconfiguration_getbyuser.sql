-- Stored procedure definition script USP_MassSalesFulfillmentConfiguration_GetByUser for PostgreSQL
CREATE OR REPLACE FUNCTION USP_MassSalesFulfillmentConfiguration_GetByUser(v_numDomainID NUMERIC(18,0),
    v_numUserCntID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT * FROM MassSalesFulfillmentConfiguration WHERE numDomainID = v_numDomainID;
END; $$;












