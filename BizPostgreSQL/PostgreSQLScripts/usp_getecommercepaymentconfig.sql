-- Stored procedure definition script USP_GeteCommercePaymentConfig for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GeteCommercePaymentConfig(v_numDomainID NUMERIC(9,0),
v_numSiteId NUMERIC(9,0),
v_numPaymentMethodId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT numId, numSiteId, numPaymentMethodId, bitEnable, numBizDocId, numOrderStatus, numRecordOwner, numAssignTo, numDomainID, numMirrorBizDocTemplateId, numFailedOrderStatus, numBizDocStatus
   FROM   eCommercePaymentConfig
   WHERE  numDomainID = v_numDomainID AND numSiteId = v_numSiteId AND numPaymentMethodId = v_numPaymentMethodId;
END; $$;
