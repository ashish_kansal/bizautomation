-- Stored procedure definition script USP_ProjectList1 for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ProjectList1(v_numUserCntID NUMERIC(18,0) DEFAULT 0,                                              
	v_numDomainID NUMERIC(18,0) DEFAULT 0,                                              
	v_tintUserRightType SMALLINT DEFAULT 0,                                                    
	v_SortChar CHAR(1) DEFAULT '0',                                                                                           
	v_CurrentPage INTEGER DEFAULT NULL,                                              
	v_PageSize INTEGER DEFAULT NULL,                                              
	INOUT v_TotRecs INTEGER  DEFAULT NULL,                                              
	v_columnName VARCHAR(50) DEFAULT NULL,                                              
	v_columnSortOrder VARCHAR(10) DEFAULT NULL,                                              
	v_numDivisionID NUMERIC(9,0) DEFAULT NULL ,                                             
	v_bitPartner BOOLEAN DEFAULT false,          
	v_ClientTimeZoneOffset INTEGER DEFAULT NULL,
	v_numProjectStatus NUMERIC(9,0) DEFAULT NULL,
	v_vcRegularSearchCriteria TEXT DEFAULT '',
	v_vcCustomSearchCriteria TEXT DEFAULT '' ,
	v_tintDashboardReminderType SMALLINT DEFAULT 0, 
    INOUT SWV_RefCur refcursor DEFAULT NULL,
	INOUT SWV_RefCur2 refcursor DEFAULT NULL
)
LANGUAGE plpgsql
AS $$

   DECLARE
   v_Nocolumns  SMALLINT DEFAULT 0;		             
		
   v_IsMasterConfAvailable  BOOLEAN DEFAULT 0;
   v_numUserGroup  NUMERIC(18,0);

   v_strColumns  TEXT;
   v_tintOrder  SMALLINT;                                                    
   v_vcFieldName  VARCHAR(50);                                                    
   v_vcListItemType  VARCHAR(3);                                               
   v_vcListItemType1  VARCHAR(1);                                                   
   v_vcAssociatedControlType  VARCHAR(20);                                                    
   v_numListID  NUMERIC(9,0);                                                    
   v_vcDbColumnName  VARCHAR(20);                        
                      
   v_vcLookBackTableName  VARCHAR(2000);                  
   v_bitCustom  BOOLEAN;  
   v_bitAllowEdit BOOLEAN;                 
   v_numFieldId  NUMERIC;  
   v_bitAllowSorting BOOLEAN; 
   v_vcColumnName  VARCHAR(500);    
                  
   v_ListRelID  NUMERIC(9,0); 
   v_SearchQuery  TEXT DEFAULT ''; 

   v_WhereCondition  TEXT DEFAULT '';

   v_strShareRedordWith  TEXT;
   v_strExternalUser  TEXT DEFAULT '';
   v_StrSql  TEXT DEFAULT '';
	
   v_vcCSOrigDbCOlumnName  VARCHAR(50);
   v_vcCSLookBackTableName  VARCHAR(50);
   v_vcCSAssociatedControlType  VARCHAR(50);
   v_firstRec  INTEGER DEFAULT 0;
   v_lastRec  INTEGER DEFAULT 0;
   v_strFinal  TEXT;
  
   v_Prefix  VARCHAR(5);
   SWV_RowCount INTEGER;

BEGIN
   	  DROP TABLE IF EXISTS tt_TEMPFORM CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPFORM
      (
         tintOrder SMALLINT,
         vcDbColumnName VARCHAR(50),
         vcOrigDbColumnName VARCHAR(50),
         vcFieldName VARCHAR(50),
         vcAssociatedControlType VARCHAR(50),
         vcListItemType VARCHAR(3),
         numListID NUMERIC(9,0),
         vcLookBackTableName VARCHAR(50),
         bitCustomField BOOLEAN,	
         numFieldId NUMERIC,
         bitAllowSorting BOOLEAN,
         bitAllowEdit BOOLEAN,
         bitIsRequired BOOLEAN,
         bitIsEmail BOOLEAN,
         bitIsAlphaNumeric BOOLEAN,
         bitIsNumeric BOOLEAN,
         bitIsLengthValidation BOOLEAN,
         intMaxLength INTEGER,
         intMinLength INTEGER,
         bitFieldMessage BOOLEAN,
         vcFieldMessage VARCHAR(500),
         ListRelID NUMERIC(9,0),
         intColumnWidth INTEGER,
         bitAllowFiltering BOOLEAN,
         vcFieldDataType CHAR(1)
      );
                
	  -- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
      SELECT COALESCE(SUM(TotalRow),0) 
	  INTO v_Nocolumns 
	  FROM(SELECT
         COUNT(*) AS TotalRow
         FROM
         View_DynamicColumns
         WHERE
         numFormId = 13
         AND numUserCntID = v_numUserCntID
         AND numDomainID = v_numDomainID
         AND tintPageType = 1
         UNION
         SELECT
         COUNT(*) AS TotalRow
         FROM
         View_DynamicCustomColumns
         WHERE
         numFormId = 13
         AND numUserCntID = v_numUserCntID
         AND numDomainID = v_numDomainID
         AND tintPageType = 1) TotalRows;
		
		-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
      IF v_Nocolumns = 0 then
		
         select numGroupID INTO v_numUserGroup FROM UserMaster WHERE numUserDetailId = v_numUserCntID;
         IF(SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = v_numDomainID AND numFormID = 13 AND bitGridConfiguration = true AND numGroupID = v_numUserGroup) > 0 then
            v_IsMasterConfAvailable := true;
         end if;
      end if;


		--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
      IF v_IsMasterConfAvailable = true then
		
         INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
         SELECT
         13,numFieldId,intColumnNum,intRowNum,v_numDomainID,v_numUserCntID,CAST(null as INTEGER),1,0,intColumnWidth
         FROM
         View_DynamicColumnsMasterConfig
         WHERE
         View_DynamicColumnsMasterConfig.numFormId = 13 AND
         View_DynamicColumnsMasterConfig.numDomainID = v_numDomainID AND
         View_DynamicColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
         View_DynamicColumnsMasterConfig.bitGridConfiguration = true AND
         coalesce(View_DynamicColumnsMasterConfig.bitSettingField,false) = true AND
         coalesce(View_DynamicColumnsMasterConfig.bitCustom,false) = false
         UNION
         SELECT
         13,numFieldId,intColumnNum,intRowNum,v_numDomainID,v_numUserCntID,CAST(null as INTEGER),1,1,intColumnWidth
         FROM
         View_DynamicCustomColumnsMasterConfig
         WHERE
         View_DynamicCustomColumnsMasterConfig.numFormId = 13 AND
         View_DynamicCustomColumnsMasterConfig.numDomainID = v_numDomainID AND
         View_DynamicCustomColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
         View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = true AND
         coalesce(View_DynamicCustomColumnsMasterConfig.bitCustom,false) = true;
         
		 INSERT INTO tt_TEMPFORM
         SELECT(intRowNum+1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	coalesce(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
				vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
				bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage AS vcFieldMessage,
				ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType
         FROM
         View_DynamicColumnsMasterConfig
         WHERE
         View_DynamicColumnsMasterConfig.numFormId = 13 AND
         View_DynamicColumnsMasterConfig.numDomainID = v_numDomainID AND
         View_DynamicColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
         View_DynamicColumnsMasterConfig.bitGridConfiguration = true AND
         coalesce(View_DynamicColumnsMasterConfig.bitSettingField,false) = true AND
         coalesce(View_DynamicColumnsMasterConfig.bitCustom,false) = false
         UNION
         SELECT
         tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
				bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
				intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,true,''
         FROM
         View_DynamicCustomColumnsMasterConfig
         WHERE
         View_DynamicCustomColumnsMasterConfig.numFormId = 13 AND
         View_DynamicCustomColumnsMasterConfig.numDomainID = v_numDomainID AND
         View_DynamicCustomColumnsMasterConfig.numAuthGroupID = v_numUserGroup AND
         View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = true AND
         coalesce(View_DynamicCustomColumnsMasterConfig.bitCustom,false) = true
         ORDER BY
         tintOrder ASC;
      ELSE
         IF v_Nocolumns = 0 then
			
            INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
            SELECT
            13,numFieldId,0,Row_number() over(order by tintRow desc),v_numDomainID,v_numUserCntID,NULL,1,false,intColumnWidth
            FROM
            View_DynamicDefaultColumns
            WHERE
            numFormId = 13
            AND bitDefault = true
            AND coalesce(bitSettingField,false) = true
            AND numDomainID = v_numDomainID
            ORDER BY
            tintOrder ASC;
         end if;
      
	  
	  INSERT INTO tt_TEMPFORM
         SELECT tintRow+1 AS tintOrder,vcDbColumnName,vcOrigDbColumnName,coalesce(vcCultureFieldName,vcFieldName),vcAssociatedControlType,vcListItemType,
				numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,
				bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage AS vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,
				vcFieldDataType
         FROM
         View_DynamicColumns
         WHERE
         numFormId = 13
         AND numUserCntID = v_numUserCntID
         AND numDomainID = v_numDomainID
         AND tintPageType = 1
         AND coalesce(bitSettingField,false) = true 
		 AND coalesce(bitCustom,false) = false
         UNION
         SELECT tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,''
				,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
				intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,true,''
         FROM
         View_DynamicCustomColumns
         WHERE
         numFormId = 13
         AND numUserCntID = v_numUserCntID
         AND numDomainID = v_numDomainID
         AND tintPageType = 1
         AND coalesce(bitCustom,false) = true
         ORDER BY
         tintOrder asc;
      end if;
      
	  v_strColumns := ' addCon.numContactId,Pro.vcProjectName,Div.numDivisionID,COALESCE(Div.numTerID,0) as numTerID,pro.numCustPrjMgr,Div.tintCRMType,pro.numProId,Pro.numRecOwner';
      
	  v_tintOrder := 0;
      
	  select tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID 
	  INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,v_bitAllowSorting,v_bitAllowEdit,v_ListRelID 
	  FROM tt_TEMPFORM    
	  ORDER BY tintOrder ASC 
	  LIMIT 1;
      
	  WHILE v_tintOrder > 0 LOOP
         RAISE NOTICE '%',v_vcDbColumnName;
         RAISE NOTICE '%',v_vcListItemType;
         RAISE NOTICE '%',v_vcAssociatedControlType;
         IF v_bitCustom = false then
            IF v_vcLookBackTableName = 'AdditionalContactsInformation' then
               v_Prefix := 'ADC.';
            end if;
            IF v_vcLookBackTableName = 'DivisionMaster' then
               v_Prefix := 'DM.';
            end if;
            IF v_vcLookBackTableName = 'ProjectsMaster' then
               v_Prefix := 'Pro.';
            end if;
            v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~' || SUBSTR(CAST(COALESCE(v_numFieldId, 0) AS VARCHAR(10)),1,10) || '~0';
            IF v_vcAssociatedControlType = 'SelectBox' then
				
               IF v_vcListItemType = 'LI' then	
                  v_strColumns := coalesce(v_strColumns,'') || ',L' || SUBSTR(CAST(COALESCE(v_tintOrder, 0) AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
                  v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(COALESCE(v_tintOrder,0) AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(COALESCE(v_tintOrder,0) AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
               ELSEIF v_vcListItemType = 'S'
               then
                  v_strColumns := coalesce(v_strColumns,'') || ',S' || SUBSTR(CAST(COALESCE(v_tintOrder, 0) AS VARCHAR(3)),1,3) || '.vcState' || ' "' || coalesce(v_vcColumnName,'') || '"';
                  v_WhereCondition := coalesce(v_WhereCondition,'') || ' left join State S' || SUBSTR(CAST(COALESCE(v_tintOrder, 0) AS VARCHAR(3)),1,3) || ' on S' || SUBSTR(CAST(COALESCE(v_tintOrder,0) AS VARCHAR(3)),1,3) || '.numStateID=' || coalesce(v_vcDbColumnName,'');
               ELSEIF v_vcListItemType = 'PP'
               then
					
                  v_strColumns := coalesce(v_strColumns,'') || ',COALESCE(PP.intTotalProgress,0)' || ' "' || coalesce(v_vcColumnName,'') || '"';
               ELSEIF v_vcListItemType = 'T'
               then
					
                  v_strColumns := coalesce(v_strColumns,'') || ',L' || SUBSTR(CAST(COALESCE(v_tintOrder, 0) AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
                  v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(COALESCE(v_tintOrder, 0) AS VARCHAR(3)),1,3) || ' on CAST(L' || SUBSTR(CAST(COALESCE(v_tintOrder, 0) AS VARCHAR(3)),1,3) || '.numListItemID AS VARCHAR)=' || coalesce(v_vcDbColumnName,'');
               ELSEIF v_vcListItemType = 'U'
               then
					
                  v_strColumns := coalesce(v_strColumns,'') || ',fn_GetContactName(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') "' || coalesce(v_vcColumnName,'') || '"';
               end if;
            ELSEIF v_vcAssociatedControlType = 'DateField'
            then
				
               IF v_vcDbColumnName = 'intDueDate' then
                  v_strColumns := coalesce(v_strColumns,'') || ',case when cast(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' as Date)= cast(now() as Date) then ''<b><font color=red>Today</font></b>''';
                  v_strColumns := coalesce(v_strColumns,'') || 'when cast(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' as Date)= cast((now() - interval ''1 day'') as Date) then ''<b><font color=purple>YesterDay</font></b>''';
                  v_strColumns := coalesce(v_strColumns,'') || 'when cast(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' as Date)= cast((now() + interval ''1 day'') as Date) then''<b><font color=orange>Tommorow</font></b>'' ';
                  v_strColumns := coalesce(v_strColumns,'') || 'else FormatedDateFromDate(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ',' || SUBSTR(CAST(COALESCE(v_numDomainID, 0) AS VARCHAR(10)),1,10) || ') end  "' || coalesce(v_vcColumnName,'') || '"';
               ELSE
                  v_strColumns := coalesce(v_strColumns,'') || ',case when cast(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'')  || ' + (' || CAST(COALESCE(-v_ClientTimeZoneOffset, 0) AS VARCHAR(15)) || ' || '' minutes'')::interval as Date)= cast(now() as Date) then ''<b><font color=red>Today</font></b>''';
                  v_strColumns := coalesce(v_strColumns,'') || 'when cast(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'')  || ' + (' || CAST(COALESCE(-v_ClientTimeZoneOffset, 0) AS VARCHAR(15)) || ' || '' minutes'')::interval as Date)= cast(now() - interval ''1 day'' as Date) then ''<b><font color=purple>YesterDay</font></b>''';
                  v_strColumns := coalesce(v_strColumns,'') || 'when cast(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + (' || CAST(COALESCE(-v_ClientTimeZoneOffset, 0) AS VARCHAR(15)) || ' || '' minutes'')::interval as Date)= cast(now() - interval ''1 day'' as Date) then''<b><font color=orange>Tommorow</font></b>'' ';
                  v_strColumns := coalesce(v_strColumns,'') || 'else FormatedDateFromDate((' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + (' || CAST(COALESCE(-v_ClientTimeZoneOffset, 0) AS VARCHAR(15)) || ' || '' minutes'')::interval),' || SUBSTR(CAST(COALESCE(v_numDomainID, 0) AS VARCHAR(10)),1,10) || ') end  "' || coalesce(v_vcColumnName,'') || '"';
               end if;
            ELSEIF v_vcAssociatedControlType = 'TextBox'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',' || case
               WHEN v_vcDbColumnName = 'numAge' then 'extract(year from current_timestamp at time zone ''utc'')-extract(year from bintDOB)'
               WHEN v_vcDbColumnName = 'numDivisionID' then 'DM.numDivisionID'
               WHEN v_vcDbColumnName = 'textSubject' then 'cast(textSubject as varchar(max))'
               ELSE coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') end || ' "' || coalesce(v_vcColumnName,'') || '"';
            ELSEIF v_vcAssociatedControlType = 'TextArea'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ', '''' ' || ' "' || coalesce(v_vcColumnName,'') || '"';
            ELSEIF v_vcAssociatedControlType = 'CheckBox'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ', COALESCE(' || coalesce(v_vcDbColumnName,'') || ',0) "' || coalesce(v_vcColumnName,'') || '"';
            ELSEIF v_vcAssociatedControlType = 'Popup' and v_vcDbColumnName = 'numShareWith'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',(SELECT STRING_AGG(A.vcFirstName || '' '' || A.vcLastName || CASE WHEN COALESCE(SR.numContactType,0)>0 THEN ''('' || fn_GetListItemName(SR.numContactType) || '')'' ELSE '''' END
								FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
								JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
								WHERE SR.numDomainID=pro.numDomainID AND SR.numModuleID=5 AND SR.numRecordID=pro.numProId
								AND UM.numDomainID=pro.numDomainID and UM.numDomainID=A.numDomainID ), '','') "' || coalesce(v_vcColumnName,'') || '"';
            end if;
         ELSEIF v_bitCustom = true
         then
			
            v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || '~1';
            IF v_vcAssociatedControlType = 'TextBox' OR v_vcAssociatedControlType = 'TextArea' then
				
               v_strColumns := CONCAT(v_strColumns,',CFW',COALESCE(v_tintOrder,0),'.Fld_Value  "',v_vcColumnName,'"');
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Pro CFW' || SUBSTR(CAST(COALESCE(v_tintOrder,0) AS VARCHAR(3)),1,3) || ' on CFW' || SUBSTR(CAST(COALESCE(v_tintOrder,0) AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(COALESCE(v_numFieldId, 0) AS VARCHAR(10)),1,10) || ' and CFW' || SUBSTR(CAST(COALESCE(v_tintOrder,0) AS VARCHAR(3)),1,3) || '.RecId=pro.numProid   ';
            ELSEIF v_vcAssociatedControlType = 'Checkbox'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',case when COALESCE(CFW' || SUBSTR(CAST(COALESCE(v_tintOrder,0) AS VARCHAR(3)),1,3) || '.Fld_Value,0)=0 then 0 when COALESCE(CFW' || SUBSTR(CAST(COALESCE(v_tintOrder,0) AS VARCHAR(3)),1,3) || '.Fld_Value,0)=1 then 1 end   "' || coalesce(v_vcColumnName,'') || '"';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Pro CFW' || SUBSTR(CAST(COALESCE(v_tintOrder,0) AS VARCHAR(3)),1,3) || ' on CFW' || SUBSTR(CAST(COALESCE(v_tintOrder,0) AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(COALESCE(v_numFieldId,0) AS VARCHAR(10)),1,10) || ' and CFW' || SUBSTR(CAST(COALESCE(v_tintOrder,0) AS VARCHAR(3)),1,3) || '.RecId=pro.numProid   ';
            ELSEIF v_vcAssociatedControlType = 'DateField'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',FormatedDateFromDate(CFW' || SUBSTR(CAST(COALESCE(v_tintOrder,0) AS VARCHAR(3)),1,3) || '.Fld_Value,' || SUBSTR(CAST(COALESCE(v_numDomainID,0) AS VARCHAR(10)),1,10) || ')  "' || coalesce(v_vcColumnName,'') || '"';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Pro CFW' || SUBSTR(CAST(COALESCE(v_tintOrder,0) AS VARCHAR(3)),1,3) || ' on CFW' || SUBSTR(CAST(COALESCE(v_tintOrder,0) AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(COALESCE(v_numFieldId,0) AS VARCHAR(10)),1,10) || ' and CFW' || SUBSTR(CAST(COALESCE(v_tintOrder,0) AS VARCHAR(3)),1,3) || '.RecId=pro.numProid   ';
            ELSEIF v_vcAssociatedControlType = 'SelectBox'
            then
				
               v_vcDbColumnName := 'DCust' || SUBSTR(CAST(COALESCE(v_numFieldId,0) AS VARCHAR(10)),1,10);
               v_strColumns := coalesce(v_strColumns,'') || ',L' || SUBSTR(CAST(COALESCE(v_tintOrder,0) AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Pro CFW' || SUBSTR(CAST(COALESCE(v_tintOrder,0) AS VARCHAR(3)),1,3) || ' on CFW' || SUBSTR(CAST(COALESCE(v_tintOrder,0) AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(COALESCE(v_numFieldId,0) AS VARCHAR(10)),1,10) || ' and CFW' || SUBSTR(CAST(COALESCE(v_tintOrder,0) AS VARCHAR(3)),1,3) || '.RecId=pro.numProid   ';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(COALESCE(v_tintOrder,0) AS VARCHAR(3)),1,3) || ' on CAST(L' || SUBSTR(CAST(COALESCE(v_tintOrder,0) AS VARCHAR(3)),1,3) || '.numListItemID AS VARCHAR)=CFW' || SUBSTR(CAST(COALESCE(v_tintOrder,0) AS VARCHAR(3)),1,3) || '.Fld_Value';
            ELSE
               v_strColumns := coalesce(v_strColumns,'') || CONCAT(',GetCustFldValuePro(',v_numFieldId,',pro.numProID)') || ' "' || coalesce(v_vcColumnName,'') || '"';
            end if;
         end if;
         
		 select   tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
         v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,
         v_bitAllowSorting,v_bitAllowEdit,v_ListRelID 
		 FROM tt_TEMPFORM 
		 WHERE tintOrder > v_tintOrder -1   
		 ORDER BY tintOrder asc LIMIT 1;
         
		 GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         
		 IF SWV_RowCount = 0 then
            v_tintOrder := 0;
         end if;
      
	  END LOOP;
      
	  v_strShareRedordWith := ' pro.numProId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID=' || SUBSTR(CAST(COALESCE(v_numDomainID,0) AS VARCHAR(15)),1,15) || ' AND SR.numModuleID=5 AND SR.numAssignedTo=' || SUBSTR(CAST(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ') ';
      v_strExternalUser := CONCAT(' (NOT EXISTS (SELECT numUserDetailID FROM UserMaster WHERE numDomainID=',CAST(COALESCE(v_numDomainID,0) AS VARCHAR),' AND numUserDetailID=',CAST(COALESCE(v_numUserCntID,0) AS VARCHAR),') AND EXISTS (SELECT EAD.numExtranetDtlID FROM ExtranetAccountsDtl EAD INNER JOIN ExtarnetAccounts EA ON EAD.numExtranetID=EA.numExtranetID WHERE EAD.numDomainID=',
      CAST(COALESCE(v_numDomainID,0) AS VARCHAR),' AND EAD.numContactID=''',CAST(COALESCE(v_numUserCntID,0) AS VARCHAR),''' AND EA.numDivisionID=pro.numDivisionID))');
      
	  v_StrSql := coalesce(v_StrSql,'') || ' FROM ProjectsMaster pro                                                               
								LEFT JOIN ProjectsContacts ProCont on ProCont.numProId=Pro.numProId and ProCont.bitPartner=true and ProCont.numContactId=' || SUBSTR(CAST(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || '                                                              
								JOIN additionalContactsinformation addCon on addCon.numContactId=pro.numCustPrjMgr
								JOIN DivisionMaster Div on Div.numDivisionID=addCon.numDivisionID    
								JOIN CompanyInfo cmp on cmp.numCompanyID=div.numCompanyID    
								left join ProjectProgress PP on PP.numProID=pro.numProID ' || coalesce(v_WhereCondition,'');
      
	  IF v_columnName ilike 'CFW.Cust%' then
         v_StrSql := coalesce(v_StrSql,'') || ' left Join CFW_FLD_Values_Pro CFW on CFW.RecId=pro.numProid  and CAST(CFW.fld_id AS VARCHAR)= ' || replace(v_columnName,'CFW.Cust','''''');
         v_columnName := 'CFW.Fld_Value';
      end if;
      IF v_columnName ilike 'DCust%' then
         v_StrSql := coalesce(v_StrSql,'') || ' left Join CFW_FLD_Values_Pro CFW on CFW.RecId=pro.numProid and CAST(CFW.fld_id AS VARCHAR)= ' || replace(v_columnName,'DCust','''''');
         v_StrSql := coalesce(v_StrSql,'') || ' left Join ListDetails LstCF on CAST(LstCF.numListItemID AS VARCHAR)=CFW.Fld_Value';
         v_columnName := 'LstCF.vcData';
      end if;
      IF v_bitPartner = true then
         v_StrSql := coalesce(v_StrSql,'') || ' left join ProjectsContacts ProCont1 on ProCont1.numProId=Pro.numProId and ProCont1.bitPartner=true and ProCont.numContactId=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || '';
      end if;   

	  -------Change Row Color-------
      v_vcCSOrigDbCOlumnName := '';
      v_vcCSLookBackTableName := '';
      
	  DROP TABLE IF EXISTS tt_TEMPCOLORSCHEME CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPCOLORSCHEME
      (
         vcOrigDbCOlumnName VARCHAR(50),
         vcLookBackTableName VARCHAR(50),
         vcFieldValue VARCHAR(50),
         vcFieldValue1 VARCHAR(50),
         vcColorScheme VARCHAR(50),
         vcAssociatedControlType VARCHAR(50)
      );
      insert into tt_TEMPCOLORSCHEME  
	  select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType
      from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID = DFFM.numFieldID
      join DycFieldMaster DFM on DFM.numModuleID = DFFM.numModuleID and DFM.numFieldID = DFFM.numFieldID
      where DFCS.numDomainID = v_numDomainID and DFFM.numFormID = 13 AND DFCS.numFormID = 13 and coalesce(DFFM.bitAllowGridColor,false) = true;
      
	  IF(SELECT COUNT(*) FROM tt_TEMPCOLORSCHEME) > 0 then	
         SELECT vcOrigDbCOlumnName, vcLookBackTableName, vcAssociatedControlType 
		 INTO v_vcCSOrigDbCOlumnName,v_vcCSLookBackTableName,v_vcCSAssociatedControlType 
		 FROM tt_TEMPCOLORSCHEME     
		 LIMIT 1;
      end if;   
	  ----------------------------             
    
      IF LENGTH(v_vcCSOrigDbCOlumnName) > 0 and LENGTH(v_vcCSLookBackTableName) > 0 then
         v_strColumns := coalesce(v_strColumns,'') || ',tCS.vcColorScheme';
      end if;
      IF LENGTH(v_vcCSOrigDbCOlumnName) > 0 and LENGTH(v_vcCSLookBackTableName) > 0 then
         if v_vcCSLookBackTableName = 'AdditionalContactsInformation' then
            v_Prefix := 'ADC.';
         end if;
         if v_vcCSLookBackTableName = 'DivisionMaster' then
            v_Prefix := 'DM.';
         end if;
         if v_vcCSLookBackTableName = 'CompanyInfo' then
            v_Prefix := 'CMP.';
         end if;
         if v_vcCSLookBackTableName = 'ProjectsMaster' then
            v_Prefix := 'pro.';
         end if;
         IF v_vcCSAssociatedControlType = 'DateField' then
			
            v_StrSql := coalesce(v_StrSql,'') || ' left join tt_TEMPCOLORSCHEME tCS on CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcCSOrigDbCOlumnName,'') || ' AS VARCHAR(10)) >= CAST(current_timestamp at time zone ''utc'' + (' || cast(tCS.vcFieldValue as int) || ') || '' days'')::interval AS VARCHAR(10))
					 and CAST(' || coalesce(v_Prefix,'') || coalesce(v_vcCSOrigDbCOlumnName,'') || ' AS VARCHAR(10)) <= CAST(current_timestamp at time zone ''utc'' + (' || cast(tCS.vcFieldValue1 as int) || ') || '' days'')::interval AS VARCHAR(10))';
         ELSEIF v_vcCSAssociatedControlType = 'SelectBox' OR v_vcCSAssociatedControlType = 'ListBox' OR v_vcCSAssociatedControlType = 'CheckBox'
         then
			
            v_StrSql := coalesce(v_StrSql,'') || ' left join tt_TEMPCOLORSCHEME tCS on tCS.vcFieldValue=' || coalesce(v_Prefix,'') || coalesce(v_vcCSOrigDbCOlumnName,'');
         end if;
      end if;
	  
      v_StrSql := coalesce(v_StrSql,'') || CONCAT(' WHERE tintProStatus=0 AND Div.numDomainID= ',COALESCE(v_numDomainID,0));
      v_StrSql := coalesce(v_StrSql,'') || ' AND (COALESCE(pro.numProjectStatus,0) = ' || SUBSTR(CAST(COALESCE(v_numProjectStatus,0) AS VARCHAR(10)),1,10) || ' OR (' || SUBSTR(CAST(COALESCE(v_numProjectStatus,0) AS VARCHAR(10)),1,10) || '=0 and COALESCE(pro.numProjectStatus,0) NOT IN (27492,27493)))';
      if v_numDivisionID <> 0 then 
         v_StrSql := coalesce(v_StrSql,'') || ' And div.numDivisionID =' || SUBSTR(CAST(COALESCE(v_numDivisionID,0) AS VARCHAR(15)),1,15);
      end if;
      if v_SortChar <> '0' then 
         v_StrSql := coalesce(v_StrSql,'') || ' And Pro.vcProjectName ilike ''' || coalesce(v_SortChar,'') || '%''';
      end if;
      if v_tintUserRightType = 1 then
         v_StrSql := coalesce(v_StrSql,'') || ' AND (Pro.numRecOwner = ' || SUBSTR(CAST(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15)
         || ' or Pro.numAssignedTo = ' || SUBSTR(CAST(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15)
         || ' or Pro.numProId in (select distinct(numProID) from ProjectsStageDetails where numAssignTo =' || SUBSTR(CAST(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ')'
         || CASE WHEN v_bitPartner = true THEN ' or ( ProCont.bitPartner=true and ProCont.numContactId=' || SUBSTR(CAST(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ')' else '' end
         || ' or ' || coalesce(v_strShareRedordWith,'')
         || ' or ' || coalesce(v_strExternalUser,'') || ')';
      ELSEIF v_tintUserRightType = 2
      then 
         v_StrSql := coalesce(v_StrSql,'') || ' and  (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ' || SUBSTR(CAST(COALESCE(v_numUserCntID,0) AS VARCHAR(15)),1,15) || ' ) or Pro.numAssignedTo= ' || SUBSTR(CAST(v_numUserCntID
         AS VARCHAR(15)),1,15)  || ' or  Pro.numProId in (select distinct(numProID) from ProjectsStageDetails                     
		where numAssignTo =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')' || ' or ' || coalesce(v_strShareRedordWith,'') || ')';
      end if;
      IF v_vcRegularSearchCriteria <> '' then 
         v_StrSql := coalesce(v_StrSql,'') || ' AND ' || coalesce(v_vcRegularSearchCriteria,'');
      end if;
      IF v_vcCustomSearchCriteria <> '' then 
         v_StrSql := coalesce(v_StrSql,'') || ' AND ' || coalesce(v_vcCustomSearchCriteria,'');
      end if;
      IF LENGTH(v_SearchQuery) > 0 then
         v_StrSql := coalesce(v_StrSql,'') || ' AND (' || coalesce(v_SearchQuery,'') || ') ';
      end if;
      
	  IF coalesce(v_tintDashboardReminderType,0) = 5 then
		
      v_StrSql := CONCAT(v_StrSql,' AND Pro.numProId IN (','SELECT DISTINCT
																		ProjectsMaster.numProId 
																	FROM 
																		StagePercentageDetails SPD 
																	INNER JOIN 
																		ProjectsMaster 
																	ON 
																		SPD.numProjectID=ProjectsMaster.numProId 
																	WHERE 
																		SPD.numDomainId=',
         COALESCE(v_numDomainID,0),'
																		AND ProjectsMaster.numDomainId=',
         COALESCE(v_numDomainID,0),'
																		AND tinProgressPercentage <> 100',
         ') ');
      end if;
      
	  v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;
      v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);

	  drop table IF EXISTS tt_TempProjectList1 CASCADE;
   v_strFinal := CONCAT('CREATE TEMPORARY TABLE tt_TempProjectList1 AS  SELECT ROW_NUMBER() OVER(ORDER BY ',v_columnName,' ',v_columnSortOrder,') ID, ',v_strColumns,v_StrSql,';');
   EXECUTE v_strFinal;

   open SWV_RefCur for SELECT * FROM tt_TempProjectList1 WHERE ID >  v_firstRec and ID < v_lastRec ORDER BY ID;
   SELECT COUNT(*) INTO v_TotRecs FROM tt_TempProjectList1;

   open SWV_RefCur2 for
   SELECT * FROM tt_TEMPFORM;
 
	DROP TABLE IF EXISTS tt_TEMPCOLORSCHEME CASCADE;

	RETURN;
END;
$$;
