-- Stored procedure definition script USP_ECampaignHstr for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ECampaignHstr(v_numConatctID NUMERIC(9,0),
    v_numConEmailCampID NUMERIC(9,0),
	v_ClientTimeZoneOffset INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   cast(ECampaign.vcECampName as VARCHAR(255)),
			(CASE
   WHEN coalesce(ECampaign.bitDeleted,false) = false
   THEN CASE WHEN coalesce(ConECampaign.bitEngaged,false) = false THEN 'Disengaged' ELSE 'Engaged' END
   ELSE 'Campaign Deleted'
   END) AS Status,
			(CASE
   WHEN coalesce(ECampaignDTLs.numEmailTemplate,0) > 0 THEN GenericDocuments.VcDocName
   WHEN coalesce(ECampaignDTLs.numActionItemTemplate,0) > 0 THEN tblActionItemData.TemplateName
   ELSE ''
   END) AS Template,
			cast(coalesce(FormatedDateTimeFromDate(ConECampaignDTL.dtExecutionDate+CAST(coalesce(ECampaign.fltTimeZone,0)*60 || 'minute' as interval),ACI.numDomainID),'') as VARCHAR(50)) AS vcExecutionDate,
			(CASE
   WHEN coalesce(ConECampaignDTL.bitSend,false) = true AND coalesce(ConECampaignDTL.tintDeliveryStatus,0) = 0 THEN 'Failed'
   WHEN coalesce(ConECampaignDTL.bitSend,false) = true AND coalesce(ConECampaignDTL.tintDeliveryStatus,0) = 1 THEN 'Success'
   ELSE 'Pending'
   END) AS SendStatus,
			--ISNULL(dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,ISNULL(ECampaign.fltTimeZone,0)*60, ConECampaignDTL.[bintSentON]),ACI.[numDomainID]),'') vcSendDate,
			CAST(ConECampaignDTL.bintSentON+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) as vcSendDate,
			cast(coalesce(ConECampaignDTL.bitEmailRead,false) as VARCHAR(255)) AS bitEmailRead,
			(SELECT COUNT(*) FROM ConECampaignDTLLinks WHERE numConECampaignDTLID = ConECampaignDTL.numConECampDTLID) AS NoOfLinks,
			(SELECT COUNT(*) FROM ConECampaignDTLLinks WHERE coalesce(bitClicked,false) = true AND numConECampaignDTLID = ConECampaignDTL.numConECampDTLID) AS NoOfLinkClicked
   FROM ConECampaignDTL
   INNER JOIN ECampaignDTLs ON ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId
   INNER JOIN ConECampaign	ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
   INNER JOIN ECampaign ON	ConECampaign.numECampaignID = ECampaign.numECampaignID
   INNER JOIN AdditionalContactsInformation ACI ON ACI.numContactId = ConECampaign.numContactID
   LEFT JOIN GenericDocuments ON GenericDocuments.numGenericDocID = ECampaignDTLs.numEmailTemplate
   LEFT JOIN tblActionItemData ON tblActionItemData.RowID = ECampaignDTLs.numActionItemTemplate
   WHERE
			(ConECampaign.numConEmailCampID = v_numConEmailCampID OR coalesce(v_numConEmailCampID,0) = 0) AND
			(ACI.numContactId = v_numConatctID OR coalesce(v_numConatctID,0) = 0)
   ORDER BY numConECampDTLID ASC;
END; $$; 












