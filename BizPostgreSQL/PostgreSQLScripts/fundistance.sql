-- Function definition script funDistance for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION funDistance(v_LAT1 DOUBLE PRECISION,
 v_LON1 DOUBLE PRECISION,
 v_LAT2 DOUBLE PRECISION,
 v_LON2 DOUBLE PRECISION)
RETURNS DOUBLE PRECISION LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Distance  DOUBLE PRECISION;
BEGIN
   IF v_LAT1 = v_LAT2 and v_LON1 = v_LON2 then
      v_Distance := 0;
   ELSE
      v_Distance := 3963.0*acos(sin(v_LAT1/57.2958)*sin(v_LAT2/57.2958)+cos(v_LAT1/57.2958)*cos(v_LAT2/57.2958)*cos(v_LON2/57.2958 -v_LON1/57.2958));
   end if;
 
 -- Return the result of the function
   RETURN v_Distance;
END; $$;

