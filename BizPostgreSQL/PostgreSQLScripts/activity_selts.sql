CREATE OR REPLACE FUNCTION Activity_SelTs
(
	v_ActivityID INTEGER -- primary key number of the activity
	,INOUT SWV_RefCur Refcursor
)
LANGUAGE plpgsql
AS $$
BEGIN
	OPEN SWV_RefCur FOR 
	SELECT
		_ts
	FROM
		Activity
	WHERE
		Activity.ActivityID = v_ActivityID;
END; $$;


