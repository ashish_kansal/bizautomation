CREATE OR REPLACE FUNCTION USP_ValidateItemAccounts( -- 1 for sales, 2 for purchase
	v_numItemCode NUMERIC(9,0),
    v_numDomainID NUMERIC(9,0),
    v_tintOppType SMALLINT DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numCOGsChartAcntId  NUMERIC(9,0);
   v_numAssetChartAcntId  NUMERIC(9,0);
   v_numIncomeChartAcntId  NUMERIC(9,0);
   v_ItemType  CHAR(1);
   v_bitKitParent  BOOLEAN;
   v_bitExpenseNonInventoryItem  BOOLEAN;
   SWV_RCur REFCURSOR;
BEGIN
   select   coalesce(bitExpenseNonInventoryItem,false) INTO v_bitExpenseNonInventoryItem FROM Domain WHERE numDomainId = v_numDomainID;
   select   numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, charItemType, (CASE WHEN coalesce(bitKitParent,false) = true
   AND coalesce(bitAssembly,false) = true THEN false
   WHEN coalesce(bitKitParent,false) = true THEN true
   ELSE false END) INTO v_numCOGsChartAcntId,v_numAssetChartAcntId,v_numIncomeChartAcntId,v_ItemType,
   v_bitKitParent FROM    Item WHERE   numItemCode = v_numItemCode
   AND numDomainID = v_numDomainID;
       
   IF (v_ItemType = 'P' OR v_ItemType = 'A') then
            
      IF (v_numAssetChartAcntId > 0
      AND v_numCOGsChartAcntId > 0
      AND v_numIncomeChartAcntId > 0) then
                   
         IF v_bitKitParent = true then

			SELECT * INTO SWV_RefCur FROM USP_ValidateItemAccountsForEmbeddedKits(v_numItemCode,SWV_RCur);
         ELSE
            open SWV_RefCur for
            SELECT  1;
         end if;
      ELSE
         open SWV_RefCur for
         SELECT  0;
      end if;
   end if;        
		
   IF (v_ItemType = 'N' OR v_ItemType = 'S') then
            
      IF v_bitExpenseNonInventoryItem = true then
				
         IF v_tintOppType = 2 then -- Validate Item Expense Account for Purchase Order Only
					
            IF (v_numCOGsChartAcntId > 0) then
							
               open SWV_RefCur for
               SELECT  1;
               RETURN;
            ELSE
               open SWV_RefCur for
               SELECT  0;
               RETURN;
            end if;
         end if;
      end if;
      IF (v_numIncomeChartAcntId > 0) then
         open SWV_RefCur for
         SELECT  1;
      ELSE
         open SWV_RefCur for
         SELECT  0;
      end if;
   end if;
END; $$;


