CREATE OR REPLACE FUNCTION USP_GetShippingPromotions(v_numDomainID NUMERIC(9,0) DEFAULT 0,
	v_byteMode SMALLINT DEFAULT 0,
	v_numUserCntId NUMERIC DEFAULT 0,
	v_numShippingCountry NUMERIC(18,0) DEFAULT 0,
	v_cookieId TEXT DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcShippingDescription  TEXT;
   v_vcQualifiedShipping  TEXT;
   v_decmPrice  DECIMAL(18,2);
BEGIN
   IF v_byteMode = 0 then
    
      open SWV_RefCur for
      SELECT  monFreeShippingOrderAmount, * FROM ShippingPromotions WHERE numDomainID = v_numDomainID;
   ELSEIF v_byteMode = 1
   then
      select coalesce(SUM(monTotAmount),0) INTO v_decmPrice from CartItems where numDomainId = v_numDomainID AND numUserCntId = v_numUserCntId AND vcCookieId = v_cookieId;
      select(CONCAT((CASE WHEN coalesce(bitFixShipping1,false) = true AND v_decmPrice < monFixShipping1OrderAmount THEN CONCAT('Spend $',(CAST((CAST(ROUND(CAST(monFixShipping1OrderAmount -v_decmPrice AS NUMERIC),2) AS DECIMAL(10,2))) AS VARCHAR(30))),CASE WHEN v_decmPrice > 0 THEN ' more ' ELSE '' END || ' and get $',
         CAST((CAST(ROUND(CAST(monFixShipping1Charge AS NUMERIC),2) AS DECIMAL(10,2))) AS VARCHAR(30)),' shipping. ') ELSE '' END),(CASE WHEN coalesce(bitFixShipping2,false) = true AND v_decmPrice < monFixShipping2OrderAmount THEN CONCAT('Spend $',(CAST((CAST(ROUND(CAST(monFixShipping2OrderAmount -v_decmPrice AS NUMERIC),2) AS DECIMAL(10,2))) AS VARCHAR(30))),CASE WHEN v_decmPrice > 0 THEN ' more ' ELSE '' END || ' and get $',
         CAST((CAST(ROUND(CAST(monFixShipping2Charge AS NUMERIC),2) AS DECIMAL(10,2))) AS VARCHAR(30)),' shipping. ') ELSE '' END),
      (CASE WHEN (bitFreeShiping = true AND numFreeShippingCountry = v_numShippingCountry) AND v_decmPrice < monFreeShippingOrderAmount THEN CONCAT('Spend $',(CAST((CAST(ROUND(CAST(monFreeShippingOrderAmount -v_decmPrice AS NUMERIC),2) AS DECIMAL(10,2))) AS VARCHAR(30))),CASE WHEN v_decmPrice > 0 THEN ' more ' ELSE '' END || ' and shipping is FREE! ') ELSE '' END))), ((CASE WHEN coalesce(bitFixShipping1,false) = true AND v_decmPrice > monFixShipping1OrderAmount AND 1 =(CASE WHEN coalesce(bitFixShipping2,false) = true AND v_decmPrice < monFixShipping2OrderAmount THEN 1 WHEN coalesce(bitFixShipping2,false) = false THEN 1 ELSE 0 END) THEN 'You are qualified for $' || CAST((CAST(ROUND(CAST(monFixShipping1Charge AS NUMERIC),2) AS DECIMAL(10,2))) AS VARCHAR(30)) || ' shipping'
      WHEN coalesce(bitFixShipping2,false) = true AND v_decmPrice > monFixShipping2OrderAmount  AND 1 =(CASE WHEN coalesce(bitFreeShiping,false) = true AND v_decmPrice < monFreeShippingOrderAmount THEN 1 WHEN coalesce(bitFreeShiping,false) = false THEN 1 ELSE 0 END) THEN 'You are qualified for $' || CAST((CAST(ROUND(CAST(monFixShipping2Charge AS NUMERIC),2) AS DECIMAL(10,2))) AS VARCHAR(30)) || ' shipping'
      WHEN (coalesce(bitFreeShiping,false) = true AND numFreeShippingCountry = v_numShippingCountry AND v_decmPrice > monFreeShippingOrderAmount) THEN 'You are qualified for Free Shipping' END)) INTO v_vcShippingDescription,v_vcQualifiedShipping FROM
      PromotionOffer PO
      LEFT JOIN ShippingPromotions SP ON PO.numDomainId = SP.numDomainID WHERE
      PO.numDomainId = v_numDomainID;
      open SWV_RefCur for
      SELECT v_vcShippingDescription AS ShippingDescription, v_vcQualifiedShipping AS QualifiedShipping;
   end if;
   RETURN;
END; $$;  


