CREATE OR REPLACE FUNCTION USP_WorkOrder_GetStages(v_numDomainID NUMERIC(18,0)
	,v_numWOID NUMERIC(18,0)
	,v_vcMilestoneName VARCHAR(500),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT
		StagePercentageDetails.numStageDetailsId AS "numStageDetailsId"
		,StagePercentageDetails.vcStageName AS "vcStageName"
		,GetTotalProgress(v_numDomainID,v_numWOID,1::SMALLINT,3::SMALLINT,'',StagePercentageDetails.numStageDetailsId) AS "numTotalProgress"
	FROM
		StagePercentageDetails
	WHERE
		StagePercentageDetails.numdomainid = v_numDomainID
		AND StagePercentageDetails.numWorkOrderId = v_numWOID
		AND vcMileStoneName = v_vcMilestoneName;
END; $$;












