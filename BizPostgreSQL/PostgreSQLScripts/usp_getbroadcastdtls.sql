-- FUNCTION: public.usp_getbroadcastdtls(numeric, refcursor)

-- DROP FUNCTION public.usp_getbroadcastdtls(numeric, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getbroadcastdtls(
	v_numbroadcastid numeric DEFAULT 0,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
   open SWV_RefCur for select vcFirstName || ' ' || vcLastname as Name,vcCompanyName || ', ' || vcDivisionName as Company,vcEmail,case when tintSucessfull = 0 then 'No' when tintSucessfull = 1 then 'Yes' end as Sent,coalesce(numNoofTimes,0) as numNoofTimes ,intNoOfClick ,bitUnsubscribe from BroadCastDtls Dtl
   join Broadcast B
   on B.numBroadCastId = Dtl.numBroadcastID
   join AdditionalContactsInformation A
   on Dtl.numContactID = A.numContactId
   join DivisionMaster D
   on D.numDivisionID = Dtl.numDivisionID
   join CompanyInfo C
   on D.numCompanyID = C.numCompanyId
   where Dtl.numBroadcastID = v_numBroadCastID;
END;
$BODY$;

ALTER FUNCTION public.usp_getbroadcastdtls(numeric, refcursor)
    OWNER TO postgres;
