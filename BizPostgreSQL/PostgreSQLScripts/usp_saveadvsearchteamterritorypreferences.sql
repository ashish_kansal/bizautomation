-- Stored procedure definition script usp_saveAdvSearchTeamTerritoryPreferences for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_saveAdvSearchTeamTerritoryPreferences(v_numDomainID NUMERIC,    
 v_numUserCntID NUMERIC,    
 v_vcTerritoriesSelected VARCHAR(2000),    
 v_vcTeamsSelected VARCHAR(2000))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT * FROM AdvSearchTeamTerritoryPreferences Where numUserCntId = v_numUserCntID And v_numDomainID = v_numDomainID) then
 
      IF LTRIM(v_vcTerritoriesSelected) = '' then
         UPDATE AdvSearchTeamTerritoryPreferences SET
         vcTeamsSelected = v_vcTeamsSelected
         Where numUserCntId = v_numUserCntID And v_numDomainID = v_numDomainID;
      ELSE
         UPDATE AdvSearchTeamTerritoryPreferences SET
         vcTerritoriesSelected = v_vcTerritoriesSelected
         Where numUserCntId = v_numUserCntID And v_numDomainID = v_numDomainID;
      end if;
   ELSE
      INSERT INTO AdvSearchTeamTerritoryPreferences(numUserCntId, vcTerritoriesSelected, vcTeamsSelected, numDomainID)
   VALUES(v_numUserCntID, v_vcTerritoriesSelected, v_vcTeamsSelected, v_numDomainID);
   end if;
   RETURN;
END; $$;


