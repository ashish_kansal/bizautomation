-- Function definition script fn_ProTotalProgress for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_ProTotalProgress(v_numProId  NUMERIC)
RETURNS VARCHAR(10) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Percentage  DECIMAL(10,0);
BEGIN
   select sum(tintPercentage) INTO v_Percentage from ProjectsStageDetails where numProId = v_numProId and bitStageCompleted = true;  
  
   return COALESCE(v_Percentage,0) || '%';
END; $$;

