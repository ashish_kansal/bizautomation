-- Stored procedure definition script USP_Import_SaveContactDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Import_SaveContactDetails(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_numDivisionID NUMERIC(18,0)
	,v_numContactID NUMERIC(18,0)
	,v_vcFirstName VARCHAR(50)
	,v_vcLastName VARCHAR(50)
	,v_vcEmail VARCHAR(80)
	,v_numContactType NUMERIC(18,0)
	,v_numPhone VARCHAR(15)
	,v_numPhoneExtension VARCHAR(7)
	,v_numCell VARCHAR(15)
	,v_numHomePhone VARCHAR(15)
	,v_vcFax VARCHAR(15)
	,v_vcCategory NUMERIC(18,0)
	,v_numTeam NUMERIC(18,0)
	,v_vcPosition NUMERIC(18,0)
	,v_vcTitle VARCHAR(100)
	,v_vcDepartment NUMERIC(18,0)
	,v_txtNotes TEXT
	,v_bitIsPrimaryContact BOOLEAN
	,v_vcStreet VARCHAR(100)
	,v_vcCity VARCHAR(50)
	,v_numState NUMERIC(18,0)
	,v_numCountry NUMERIC(18,0)
	,v_vcPostalCode VARCHAR(15),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
	-- IMPORTANT: PLEASE MAKE SURE NEW FIELD WHICH YOUR ARE ADDING IN STORE PROCEDURE FOR UPDATE ARE ADDED IN SELECT IN PROCEDURE USP_Import_GetContactDetails

   AS $$
   DECLARE
   v_numCount  NUMERIC(18,0) DEFAULT 0;

   v_bitAutoPopulateAddress  BOOLEAN; 
   v_tintPoulateAddressTo  SMALLINT;
BEGIN
   IF LENGTH(coalesce(v_vcFirstName,'')) = 0 then
	
      v_vcFirstName := '-';
   end if;

   IF LENGTH(coalesce(v_vcLastName,'')) = 0 then
	
      v_vcLastName := '-';
   end if;

	--IF LEN(ISNULL(@vcEmail,'')) > 0
	--BEGIN
	--	IF EXISTS (SELECT numContactId FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId <> ISNULL(@numcontactId,0) AND vcEmail=@vcEmail)
	--	BEGIN
	--		RAISERROR('DUPLICATE_EMAIL',16,1)
	--		RETURN
	--	END
	--END

   IF coalesce(v_numDivisionID,0) > 0 then
      IF coalesce(v_numContactID,0) = 0 then
         IF(SELECT COUNT(numContactId) FROM AdditionalContactsInformation WHERE numDivisionId = v_numDivisionID) = 0 then
			
            v_bitIsPrimaryContact := true;
         end if;
         IF coalesce(v_numContactType,0) = 0 then
			
            v_numContactType := 70;
         end if;
         INSERT into AdditionalContactsInformation(numContactType,
				vcDepartment,
				vcCategory,
				vcGivenName,
				vcFirstName,
				vcLastname,
				numDivisionId,
				numPhone,
				numPhoneExtension,
				numCell,
				numHomePhone,
				vcFax,
				vcEmail,
				vcPosition,
				txtNotes,
				numCreatedBy,
				bintCreatedDate,
				numModifiedBy,
				bintModifiedDate,
				numDomainID,
				numRecOwner,
				numTeam,
				numEmpStatus,
				vcTitle
				,bitPrimaryContact)
			VALUES(v_numContactType,
				v_vcDepartment,
				v_vcCategory,
				CONCAT(v_vcLastName,', ',v_vcFirstName,'.eml'),
				v_vcFirstName ,
				v_vcLastName,
				v_numDivisionID ,
				v_numPhone ,
				v_numPhoneExtension,
				v_numCell ,
				v_numHomePhone ,
				v_vcFax ,
				v_vcEmail ,
				v_vcPosition,
				v_txtNotes,
				v_numUserCntID,
				TIMEZONE('UTC',now()),
				v_numUserCntID,
				TIMEZONE('UTC',now()),
				v_numDomainID,
				v_numUserCntID,
				v_numTeam,
				658,
				v_vcTitle
				,v_bitIsPrimaryContact) RETURNING numContactID INTO v_numContactID;

         select   coalesce(bitAutoPopulateAddress,false), coalesce(tintPoulateAddressTo,'0') INTO v_bitAutoPopulateAddress,v_tintPoulateAddressTo FROM
         Domain WHERE
         numDomainId = v_numDomainID;
         IF (v_bitAutoPopulateAddress = true) then
			
            IF(v_tintPoulateAddressTo = 1) then --if primary address is not specified then getting billing address    
				
               select   AD1.vcStreet, AD1.vcCity, AD1.numState, AD1.vcPostalCode, AD1.numCountry INTO v_vcStreet,v_vcCity,v_numState,v_vcPostalCode,v_numCountry FROM
               DivisionMaster  DM
               LEFT JOIN
               AddressDetails AD1
               ON
               AD1.numDomainID = DM.numDomainID
               AND AD1.numRecordID = DM.numDivisionID
               AND AD1.tintAddressOf = 2
               AND AD1.tintAddressType = 1
               AND AD1.bitIsPrimary = true WHERE
               numDivisionID = v_numDivisionID;
            ELSEIF (v_tintPoulateAddressTo = 2)
            then-- Primary Address is Shipping Address
				
               select   AD2.vcStreet, AD2.vcCity, AD2.numState, AD2.vcPostalCode, AD2.numCountry INTO v_vcStreet,v_vcCity,v_numState,v_vcPostalCode,v_numCountry FROM
               DivisionMaster DM
               LEFT JOIN
               AddressDetails AD2
               ON
               AD2.numDomainID = DM.numDomainID
               AND AD2.numRecordID = DM.numDivisionID
               AND AD2.tintAddressOf = 2
               AND AD2.tintAddressType = 2
               AND AD2.bitIsPrimary = true WHERE
               numDivisionID = v_numDivisionID;
            end if;
         end if;
         INSERT INTO AddressDetails(vcAddressName,
	 			vcStreet,
	 			vcCity,
	 			vcPostalCode,
	 			numState,
	 			numCountry,
	 			bitIsPrimary,
	 			tintAddressOf,
	 			tintAddressType,
	 			numRecordID,
	 			numDomainID)
			VALUES('Primary'
				,v_vcStreet
				,v_vcCity
				,v_vcPostalCode
				,v_numState
				,v_numCountry
				,true
				,1
				,0
				,v_numContactID
				,v_numDomainID);
      ELSE
         UPDATE
         AdditionalContactsInformation
         SET
         numContactType = v_numContactType,vcGivenName = CONCAT(v_vcLastName,', ',v_vcFirstName,'.eml'),
         vcFirstName = v_vcFirstName,vcLastname = v_vcLastName,
         numDivisionId = v_numDivisionID,numPhone = v_numPhone,numPhoneExtension = v_numPhoneExtension,
         numCell = v_numCell,numHomePhone = v_numHomePhone,vcFax = v_vcFax,
         vcEmail = v_vcEmail,vcPosition = v_vcPosition,txtNotes = v_txtNotes,
         numModifiedBy = v_numUserCntID,bintModifiedDate = TIMEZONE('UTC',now()),
         bitPrimaryContact = v_bitIsPrimaryContact
         WHERE
         numContactId = v_numContactID;
      end if;
      IF coalesce(v_vcEmail,'') <> '' then
		
         select   COUNT(*) INTO v_numCount FROM EmailMaster WHERE vcEmailId = v_vcEmail AND numDomainID = v_numDomainID;
         IF v_numCount = 0 then
			
            INSERT INTO EmailMaster(vcEmailId,vcName,numDomainID,numContactID)
				VALUES(v_vcEmail,v_vcFirstName,v_numDomainID,v_numContactID);
         end if;
      ELSE
         UPDATE EmailMaster set numContactID = v_numContactID WHERE vcEmailId = v_vcEmail and numDomainID = v_numDomainID;
      end if;
   end if;

   open SWV_RefCur for SELECT cast(coalesce(v_numContactID,0) as NUMERIC(18,0)) AS numContactID;
   RETURN;
END; $$;

/****** Object:  StoredProcedure [dbo].[USP_InsertCartItem]    Script Date: 11/08/2011 17:53:22 ******/













