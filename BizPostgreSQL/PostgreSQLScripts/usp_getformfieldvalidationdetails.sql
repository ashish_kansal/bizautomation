-- Stored procedure definition script usp_getFormFieldValidationDetails for PostgreSQL
CREATE OR REPLACE FUNCTION usp_getFormFieldValidationDetails(v_numDomainID NUMERIC(9,0),
 v_numFormId NUMERIC(9,0),
 v_numFormFieldId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select DFV.numFormId
      ,DFV.numFormFieldId
      ,DFV.numDomainID
      ,DFV.vcNewFormFieldName
      ,DFV.bitIsRequired
      ,DFV.bitIsNumeric
      ,DFV.bitIsAlphaNumeric
      ,DFV.bitIsEmail
      ,DFV.bitIsLengthValidation
      ,DFV.intMaxLength
      ,DFV.intMinLength
      ,DFV.bitFieldMessage
      ,DFV.vcFieldMessage
      ,coalesce(DFV.vcNewFormFieldName,coalesce(DFFM.vcFieldName,DFM.vcFieldName)) AS vcFormFieldName,DFM.vcAssociatedControlType,
 coalesce(DFV.vcToolTip,coalesce(DFM.vcToolTip,'')) AS vcToolTip
   FROM DycFormField_Mapping DFFM
   JOIN DycFieldMaster DFM on DFFM.numModuleID = DFFM.numModuleID and DFFM.numFieldID = DFM.numFieldID
   left join DynamicFormField_Validation DFV on DFV.numDomainID = v_numDomainID AND DFM.numFieldID = DFV.numFormFieldId AND DFV.numFormId = DFFM.numFormID
   LEFT JOIN dynamicFormMaster DFMMaster ON DFFM.numFormID = DFMMaster.numFormID
   where DFFM.numFieldID = v_numFormFieldId AND DFFM.numFormID = v_numFormId AND coalesce(DFM.bitDeleted,false) = false ORDER BY vcFormFieldName;
END; $$;

