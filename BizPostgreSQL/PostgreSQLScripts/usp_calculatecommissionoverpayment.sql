DROP FUNCTION IF EXISTS USP_CalculateCommissionOverPayment;

CREATE OR REPLACE FUNCTION USP_CalculateCommissionOverPayment(v_numDomainID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
   AS $$
   DECLARE
   v_dtPayStart  DATE;
   v_dtPayEnd  DATE;

   v_tintCommissionType  SMALLINT;
BEGIN

   BEGIN
      CREATE TEMP SEQUENCE tt_TABLEPAID_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TABLEPAID CASCADE;
   CREATE TEMPORARY TABLE tt_TABLEPAID
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numComissionID NUMERIC(18,0),
      numComRuleID NUMERIC(18,0),
      tintComType SMALLINT,
      tintComBasedOn SMALLINT,
      bitDomainCommissionBasedOn BOOLEAN,
      tintDomainCommissionBasedOn SMALLINT,
      numUnitHour DOUBLE PRECISION,
      monVendorCost DECIMAL(20,5),
      monAvgCost DECIMAL(20,5),
      monTotAmount DECIMAL(20,5),
      decCommission DECIMAL(18,2),
      monCommission DECIMAL(20,5)
   );

   select   tintCommissionType INTO v_tintCommissionType FROM
   Domain WHERE
   numDomainId = v_numDomainID;

   DELETE FROM BizDocComissionPaymentDifference BDCPD using BizDocComission BC where BDCPD.numComissionID = BC.numComissionID AND BC.numDomainId = v_numDomainID
   AND coalesce(bitDifferencePaid,false) = false;

   INSERT INTO BizDocComissionPaymentDifference(numComissionID
		,tintReason
		,vcReason
		,monDifference
		,bitDifferencePaid)
   SELECT
   BC.numComissionID
		,1
		,'Sales Order deleted'
		,coalesce(numComissionAmount,0)*-1
		,false
   FROM
   BizDocComission BC
   LEFT JOIN
   BizDocComissionPaymentDifference BCPD
   ON
   BC.numComissionID = BCPD.numComissionID
   AND BCPD.tintReason = 1
   LEFT JOIN
   OpportunityMaster OM
   ON
   BC.numOppID = OM.numOppId
   WHERE
   BC.numDomainId = v_numDomainID
   AND coalesce(BC.numOppID,0) > 0
   AND coalesce(BC.bitCommisionPaid,false) = true
   AND coalesce(BC.bitNewImplementation,false) = true
   AND OM.numOppId IS NULL
   AND BCPD.ID IS NULL; 

   INSERT INTO BizDocComissionPaymentDifference(numComissionID
		,tintReason
		,vcReason
		,monDifference
		,bitDifferencePaid)
   SELECT
   BC.numComissionID
		,1
		,'Sales Order line item deleted'
		,coalesce(numComissionAmount,0)*-1
		,false
   FROM
   BizDocComission BC
   LEFT JOIN
   BizDocComissionPaymentDifference BCPD
   ON
   BC.numComissionID = BCPD.numComissionID
   AND BCPD.tintReason = 1
   LEFT JOIN
   OpportunityItems OI
   ON
   BC.numOppItemID = OI.numoppitemtCode
   WHERE
   BC.numDomainId = v_numDomainID
   AND coalesce(BC.numOppItemID,0) > 0
   AND coalesce(BC.bitCommisionPaid,false) = true
   AND coalesce(BC.bitNewImplementation,false) = true
   AND OI.numoppitemtCode IS NULL
   AND BCPD.ID IS NULL; 

   INSERT INTO BizDocComissionPaymentDifference(numComissionID
		,tintReason
		,vcReason
		,monDifference
		,bitDifferencePaid)
   SELECT
   BC.numComissionID
		,1
		,'Invoice deleted'
		,coalesce(numComissionAmount,0)*-1
		,false
   FROM
   BizDocComission BC
   LEFT JOIN
   BizDocComissionPaymentDifference BCPD
   ON
   BC.numComissionID = BCPD.numComissionID
   AND BCPD.tintReason = 1
   LEFT JOIN
   OpportunityBizDocs OBD
   ON
   BC.numOppBizDocId = OBD.numOppBizDocsId
   WHERE
   BC.numDomainId = v_numDomainID
   AND coalesce(BC.numOppBizDocId,0) > 0
   AND coalesce(BC.bitCommisionPaid,false) = true
   AND coalesce(BC.bitNewImplementation,false) = true
   AND OBD.numOppBizDocsId IS NULL
   AND BCPD.ID IS NULL;

   INSERT INTO BizDocComissionPaymentDifference(numComissionID
		,tintReason
		,vcReason
		,monDifference
		,bitDifferencePaid)
   SELECT
   BC.numComissionID
		,1
		,'Invoice line item deleted'
		,coalesce(numComissionAmount,0)*-1
		,false
   FROM
   BizDocComission BC
   LEFT JOIN
   BizDocComissionPaymentDifference BCPD
   ON
   BC.numComissionID = BCPD.numComissionID
   AND BCPD.tintReason = 1
   LEFT JOIN
   OpportunityBizDocItems OBDI
   ON
   BC.numOppBizDocItemID = OBDI.numOppBizDocItemID
   WHERE
   BC.numDomainId = v_numDomainID
   AND coalesce(BC.numOppBizDocItemID,0) > 0
   AND coalesce(BC.bitCommisionPaid,false) = true
   AND coalesce(BC.bitNewImplementation,false) = true
   AND OBDI.numOppBizDocItemID IS NULL
   AND BCPD.ID IS NULL;

	-- DELETE UNPAID COMMISSION ENTRIES BETWEEN PAY PERIOD DATE RANGE	
   IF v_tintCommissionType = 3 then --Sales Order Sub-Total Amount
	
      INSERT INTO tt_TABLEPAID(numComissionID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			bitDomainCommissionBasedOn,
			tintDomainCommissionBasedOn,
			numUnitHour,
			monVendorCost,
			monAvgCost,
			monTotAmount,
			decCommission,
			monCommission)
      SELECT
      BC.numComissionID,
			BC.numComRuleID,
			BC.tintComType,
			BC.tintComBasedOn,
			BC.bitDomainCommissionBasedOn,
			BC.tintDomainCommissionBasedOn,
			coalesce(OI.numUnitHour,0),
			(coalesce(OI.monVendorCost,0)*fn_UOMConversion(Item.numPurchaseUnit,Item.numItemCode,v_numDomainID,Item.numBaseUnit)*coalesce(OI.numUnitHour,0)),
			(coalesce(OI.monAvgCost,0)*coalesce(OI.numUnitHour,0)),
			coalesce(OI.monTotAmount,0),
			coalesce(decCommission,0),
			coalesce(numComissionAmount,0)
      FROM
      BizDocComission BC
      INNER JOIN
      OpportunityMaster OM
      ON
      BC.numOppID = OM.numOppId
      INNER JOIN
      DivisionMaster
      ON
      OM.numDivisionId = DivisionMaster.numDivisionID
      INNER JOIN
      CompanyInfo
      ON
      DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
      INNER JOIN
      OpportunityItems OI
      ON
      BC.numOppItemID = OI.numoppitemtCode
      INNER JOIN
      Item
      ON
      OI.numItemCode = Item.numItemCode
      WHERE
      BC.numDomainId = v_numDomainID
      AND coalesce(BC.bitCommisionPaid,false) = true
      AND coalesce(BC.bitNewImplementation,false) = true;
   ELSEIF v_tintCommissionType = 2
   then --Invoice Sub-Total Amount (Paid or Unpaid)
	
      INSERT INTO tt_TABLEPAID(numComissionID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			bitDomainCommissionBasedOn,
			tintDomainCommissionBasedOn,
			numUnitHour,
			monVendorCost,
			monAvgCost,
			monTotAmount,
			decCommission,
			monCommission)
      SELECT
      BC.numComissionID,
			BC.numComRuleID,
			BC.tintComType,
			BC.tintComBasedOn,
			BC.bitDomainCommissionBasedOn,
			BC.tintDomainCommissionBasedOn,
			coalesce(OppBizItems.numUnitHour,0),
			(coalesce(OppMItems.monVendorCost,0)*fn_UOMConversion(Item.numPurchaseUnit,Item.numItemCode,v_numDomainID,Item.numBaseUnit)*coalesce(OppBizItems.numUnitHour,0)),
			(coalesce(OppMItems.monAvgCost,0)*coalesce(OppBizItems.numUnitHour,0)),
			coalesce(OppBizItems.monTotAmount,0),
			coalesce(decCommission,0),
			coalesce(numComissionAmount,0)
      FROM
      BizDocComission BC
      INNER JOIN
      OpportunityMaster OM
      ON
      BC.numOppID = OM.numOppId
      INNER JOIN
      OpportunityBizDocs OBD
      ON
      OM.numOppId = OBD.numoppid
      AND BC.numOppBizDocId = OBD.numOppBizDocsId
      INNER JOIN
      DivisionMaster
      ON
      OM.numDivisionId = DivisionMaster.numDivisionID
      INNER JOIN
      CompanyInfo
      ON
      DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
      INNER JOIN
      OpportunityBizDocItems OppBizItems
      ON
      BC.numOppBizDocItemID = OppBizItems.numOppBizDocItemID
      INNER JOIN
      OpportunityItems OppMItems
      ON
      BC.numOppItemID = OppMItems.numoppitemtCode
      INNER JOIN
      Item
      ON
      OppBizItems.numItemCode = Item.numItemCode
      WHERE
      BC.numDomainId = v_numDomainID
      AND coalesce(BC.bitCommisionPaid,false) = true
      AND coalesce(BC.bitNewImplementation,false) = true;
   ELSE
      INSERT INTO tt_TABLEPAID(numComissionID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			bitDomainCommissionBasedOn,
			tintDomainCommissionBasedOn,
			numUnitHour,
			monVendorCost,
			monAvgCost,
			monTotAmount,
			decCommission,
			monCommission)
      SELECT
      BC.numComissionID,
			BC.numComRuleID,
			BC.tintComType,
			BC.tintComBasedOn,
			BC.bitDomainCommissionBasedOn,
			BC.tintDomainCommissionBasedOn,
			coalesce(OppBizItems.numUnitHour,0),
			(coalesce(OppMItems.monVendorCost,0)*fn_UOMConversion(Item.numPurchaseUnit,Item.numItemCode,v_numDomainID,Item.numBaseUnit)*coalesce(OppBizItems.numUnitHour,0)),
			(coalesce(OppMItems.monAvgCost,0)*coalesce(OppBizItems.numUnitHour,0)),
			coalesce(OppBizItems.monTotAmount,0),
			coalesce(decCommission,0),
			coalesce(numComissionAmount,0)
      FROM
      BizDocComission BC
      INNER JOIN
      OpportunityMaster OppM
      ON
      OppM.numOppId = BC.numOppID
      INNER JOIN
      OpportunityBizDocs OppBiz
      ON
      OppBiz.numoppid = OppM.numOppId
      AND OppBiz.numOppBizDocsId = BC.numOppBizDocId
      INNER JOIN
      DivisionMaster
      ON
      DivisionMaster.numDivisionID = OppM.numDivisionId
      INNER JOIN
      CompanyInfo
      ON
      CompanyInfo.numCompanyId = DivisionMaster.numCompanyID
      INNER JOIN
      OpportunityBizDocItems OppBizItems
      ON
	  OppBizItems.numoppbizdocid=BC.numOppBizDocID
      AND OppBizItems.numOppBizDocItemID = BC.numOppBizDocItemID
      INNER JOIN
      OpportunityItems OppMItems
      ON
      OppMItems.numoppitemtCode = BC.numOppItemID
      INNER JOIN
      Item
      ON
      Item.numItemCode = OppBizItems.numItemCode
      WHERE
      BC.numDomainId = v_numDomainID
      AND BC.bitCommisionPaid = true
      AND COALESCE(BC.bitNewImplementation,false) = true;
   end if;	

   INSERT INTO BizDocComissionPaymentDifference(numComissionID
		,tintReason
		,vcReason
		,monDifference
		,bitDifferencePaid)
   SELECT
   numComissionID
		,tintReason
		,vcReason
		,monDifference
		,bitDifferencePaid
   FROM(SELECT
      T1.numComissionID
		,2 AS tintReason
		,'Change in qty/amount' AS vcReason
		,coalesce(CASE tintComType
      WHEN 1 --PERCENT
      THEN
         CASE
         WHEN coalesce(bitDomainCommissionBasedOn,false) = true
         THEN
            CASE tintDomainCommissionBasedOn
										--ITEM GROSS PROFIT (VENDOR COST)
            WHEN 1 THEN(coalesce(monTotAmount,0) -coalesce(monVendorCost,0))*(T1.decCommission/CAST(100 AS DOUBLE PRECISION))
										--ITEM GROSS PROFIT (AVERAGE COST)
            WHEN 2 THEN(coalesce(monTotAmount,0) -coalesce(monAvgCost,0))*(T1.decCommission/CAST(100 AS DOUBLE PRECISION))
            END
         ELSE
            monTotAmount*(T1.decCommission/CAST(100 AS DOUBLE PRECISION))
         END
      ELSE  --FLAT
         T1.decCommission
      END,
      0) -(coalesce(monCommission,0)+coalesce((SELECT SUM(monDifference) FROM BizDocComissionPaymentDifference WHERE numComissionID = T1.numComissionID),0)) AS monDifference
		,false AS bitDifferencePaid
      FROM
      tt_TABLEPAID AS T1
      WHERE
      coalesce(CASE tintComType
      WHEN 1 --PERCENT
      THEN
         CASE
         WHEN coalesce(bitDomainCommissionBasedOn,false) = true
         THEN
            CASE tintDomainCommissionBasedOn
										--ITEM GROSS PROFIT (VENDOR COST)
            WHEN 1 THEN(coalesce(monTotAmount,0) -coalesce(monVendorCost,0))*(T1.decCommission/CAST(100 AS DOUBLE PRECISION))
										--ITEM GROSS PROFIT (AVERAGE COST)
            WHEN 2 THEN(coalesce(monTotAmount,0) -coalesce(monAvgCost,0))*(T1.decCommission/CAST(100 AS DOUBLE PRECISION))
            END
         ELSE
            monTotAmount*(T1.decCommission/CAST(100 AS DOUBLE PRECISION))
         END
      ELSE  --FLAT
         T1.decCommission
      END,
      0) <>(coalesce(monCommission,0)+coalesce((SELECT SUM(monDifference) FROM BizDocComissionPaymentDifference WHERE numComissionID = T1.numComissionID),0))) TEMP
   WHERE
   CAST(TEMP.monDifference AS DECIMAL(20,4)) <> 0;
RETURN;
END; $$;


