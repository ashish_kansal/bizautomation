-- Stored procedure definition script Activity_GoogleEventId for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Activity_GoogleEventId(v_ActivityID INTEGER, 
 v_GoogleEventId TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE Activity SET GoogleEventId = v_GoogleEventId WHERE ActivityID = v_ActivityID;
   RETURN;
END; $$;


