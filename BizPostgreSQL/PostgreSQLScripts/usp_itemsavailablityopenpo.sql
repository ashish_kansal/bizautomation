-- Stored procedure definition script USP_ItemsAvailablityOpenPO for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ItemsAvailablityOpenPO(v_numItemCode NUMERIC(18,0) DEFAULT 0,
    v_numDomainID NUMERIC(18,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if((select COUNT(W.numOnHand)
   from
   WareHouseItems  AS W
   LEFT JOIN
   Warehouses AS WH ON WH.numWareHouseID = W.numWareHouseID
   LEFT JOIN
   WarehouseLocation AS WL ON WL.numWLocationID = W.numWLocationID
   WHERE
   W.numItemID = v_numItemCode) = 0) then
		
      open SWV_RefCur for
      SELECT(CASE WHEN v_numDomainID = 209 THEN 'RFQ' ELSE '0' END) AS numOnHand,0 AS numOnOrder,'NOT AVAILABLE' AS vcWareHouse,NULL AS bintCreatedDate,NULL AS dtDeliveryDate,0 AS LeadDays;
   ELSE
      open SWV_RefCur for
      select distinct
      CASE WHEN coalesce(W.numOnHand,0) = 0 THEN(CASE WHEN v_numDomainID = 209 THEN 'RFQ' ELSE '0' END) ELSE CAST(W.numOnHand AS VARCHAR(30)) END AS numOnHand,
			W.numonOrder,
			WH.vcWareHouse || ' ' ||(CASE WHEN W.numWLocationID = -1 THEN 'Global' WHEN W.numWLocationID = 0 THEN '' ELSE WL.vcLocation END) AS vcWareHouse,
			(select  bintCreatedDate FROM OpportunityMaster where tintshipped = 0 AND tintopptype = 2 AND numDomainId = v_numDomainID AND numOppId IN(SELECT numOppId FROM OpportunityItems where numItemCode = v_numItemCode) ORDER BY bintCreatedDate desc LIMIT 1) as bintCreatedDate,
			(select  dtDeliveryDate FROM OpportunityBizDocs where dtDeliveryDate IS NOT NULL AND numoppid =(select  numOppId FROM OpportunityMaster where tintshipped = 0 AND tintopptype = 2 AND numDomainId = v_numDomainID AND numOppId IN(SELECT numOppId FROM OpportunityItems where numItemCode = v_numItemCode) LIMIT 1) LIMIT 1) AS dtDeliveryDate,
			coalesce(DATE_PART('day',CAST((select  dtDeliveryDate FROM OpportunityBizDocs where dtDeliveryDate IS NOT NULL AND numoppid =(select  numOppId FROM OpportunityMaster where tintshipped = 0 AND tintopptype = 2 AND numDomainId = v_numDomainID AND numOppId IN(SELECT numOppId FROM OpportunityItems where numItemCode = v_numItemCode) LIMIT 1) LIMIT 1) AS DATE):: timestamp -CAST((select  bintCreatedDate FROM OpportunityMaster where tintshipped = 0 AND numDomainId = v_numDomainID AND tintopptype = 2 AND numOppId IN(SELECT numOppId FROM OpportunityItems where numItemCode = v_numItemCode) ORDER BY bintCreatedDate desc LIMIT 1) AS DATE):: timestamp),
      0) AS LeadDays
      from
      WareHouseItems  AS W
      LEFT JOIN
      Warehouses AS WH ON WH.numWareHouseID = W.numWareHouseID
      LEFT JOIN
      WarehouseLocation AS WL ON WL.numWLocationID = W.numWLocationID
      WHERE
      W.numItemID = v_numItemCode;
   end if;
   RETURN;
END; $$;


