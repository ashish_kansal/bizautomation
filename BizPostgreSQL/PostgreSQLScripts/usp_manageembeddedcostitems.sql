-- Stored procedure definition script USP_ManageEmbeddedCostItems for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageEmbeddedCostItems(v_numEmbeddedCostID NUMERIC,
    v_numDomainID NUMERIC,
    v_strItems TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_sumTotal  DECIMAL(20,5);
BEGIN


   DELETE  FROM EmbeddedCostItems
   WHERE   numEmbeddedCostID = v_numEmbeddedCostID
   AND numDomainID = v_numDomainID;

   IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then
		INSERT  INTO EmbeddedCostItems
		(
			numEmbeddedCostID,
            numOppBizDocItemID,
            monAmount,
            numDomainID
		)
		SELECT  
			v_numEmbeddedCostID,
            X.numOppBizDocItemID,
            X.monAmount,
            v_numDomainID
		FROM
		XMLTABLE
		(
			'NewDataSet/Table1'
			PASSING 
				CAST(v_strItems AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numOppBizDocItemID NUMERIC PATH 'numOppBizDocItemID',
				monAmount DECIMAL(20,5) PATH 'monAmount'
		) AS X;

		SELECT 
			SUM(coalesce(X.monAmount,0)) INTO v_sumTotal 
		FROM
		XMLTABLE
		(
			'NewDataSet/Table1'
			PASSING 
				CAST(v_strItems AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numOppBizDocItemID NUMERIC PATH 'numOppBizDocItemID',
				monAmount DECIMAL(20,5) PATH 'monAmount'
		) AS X;
		
        -- update estimated cost        
		UPDATE EmbeddedCost SET monCost = coalesce(v_sumTotal,0) WHERE numEmbeddedCostID = v_numEmbeddedCostID;	  
   end if;
   RETURN;
END; $$;

	



