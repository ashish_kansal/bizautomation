-- Stored procedure definition script USP_GetShippingDTLs for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetShippingDTLs(v_numListItemID NUMERIC(9,0) ,
    v_numDomainID NUMERIC(9,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numListItemID = 0 then
      select   coalesce(numShipCompany,0) INTO v_numListItemID FROM    Domain WHERE   numDomainId = v_numDomainID;
   end if;

   open SWV_RefCur for
   SELECT  SF.intShipFieldID ,
            vcFieldName ,
            coalesce(SFV.vcShipFieldValue,'') AS vcShipFieldValue ,
            coalesce(SF.vcToolTip,'') AS vcToolTip ,
            coalesce(SF.numListItemID,0) AS numListItemID
   FROM    ShippingFields SF
   LEFT JOIN ShippingFieldValues SFV ON SFV.intShipFieldID = SF.intShipFieldID
   AND SFV.numDomainID = v_numDomainID
   WHERE   SF.numListItemID = v_numListItemID;

   open SWV_RefCur2 for
   SELECT  v_numListItemID;
   RETURN;
END; $$;


