-- Function definition script GetDomainNameFromURL for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetDomainNameFromURL(v_vcURL VARCHAR(1000))
RETURNS TEXT LANGUAGE plpgsql
   AS $$
   DECLARE
   v_DomainName TEXT;
   v_index  INTEGER;
BEGIN
   v_DomainName := REPLACE(v_vcURL,'http://','');
   v_DomainName := REPLACE(v_DomainName,'https://','');
   v_index := POSITION('/' IN v_DomainName);
   IF v_index > 0 then
      v_DomainName := SUBSTR(v_DomainName,0,v_index);
   end if;
   v_DomainName := REPLACE(v_DomainName,'www.','');
	--SET @index = ISNULL(CHARINDEX('.',@DomainName),0)
   v_index := POSITION('/' IN v_DomainName);
   IF v_index > 0 then
      v_DomainName := SUBSTR(v_DomainName,0,v_index);
   end if;
	
   RETURN coalesce(v_DomainName,'');
END; $$;
/****** Object:  UserDefinedFunction [dbo].[GetECamLastAct]    Script Date: 07/26/2008 18:13:01 ******/

