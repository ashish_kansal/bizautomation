-- Stored procedure definition script USP_GetCompanyDetailsFrom850 for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetCompanyDetailsFrom850(v_numDomainID NUMERIC(18,0)
	,v_numDivisionID NUMERIC(18,0)
	,v_vcFirstName VARCHAR(300)
	,v_vcLastName VARCHAR(300)
	,v_vcEmail VARCHAR(300)
	,v_vcMarketplace VARCHAR(300),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
	--1: SKU
	--2: UPC
	--3: ItemName
	--4: BizItemID
	--5: ASIN
	--6: CustomerPart
	--7: VendorPart

   AS $$
   DECLARE
   v_numMarketplaceID  NUMERIC(18,0) DEFAULT 0;
   v_bitUsePredefinedCustomer  BOOLEAN;
BEGIN
   IF EXISTS(SELECT ID FROM eChannelHub WHERE vcMarketplace = coalesce(v_vcMarketplace,'')) then
	
      select   ID INTO v_numMarketplaceID FROM
      eChannelHub WHERE
      vcMarketplace = coalesce(v_vcMarketplace,'');
   end if;

   select   coalesce(bitUsePredefinedCustomer,false) INTO v_bitUsePredefinedCustomer FROM Domain WHERE numDomainId = v_numDomainID;

   IF v_bitUsePredefinedCustomer = true then
	
      v_numDivisionID := coalesce((SELECT numDivisionID FROM DomainMarketplace WHERE numDomainID = v_numDomainID AND numMarketplaceID = v_numMarketplaceID),0);
   ELSE
      IF coalesce(v_vcEmail,'') = '' then
		
         v_numDivisionID := 0;
      ELSE
         v_numDivisionID := coalesce((SELECT numDivisionId FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND vcEmail = coalesce(v_vcEmail,'')),0);
      end if;
   end if;

   open SWV_RefCur for SELECT
   TEMP.numContactId AS numContactID
		,TEMP.numAssignedTo AS numAssignedTo
		,TEMP.numDivisionID
		,coalesce(DMEmployer.tintInbound850PickItem,0) AS tintInbound850PickItem
		,TEMP.numCompanyId
		,TEMP.vcCompanyName
		,v_numMarketplaceID AS numMarketplaceID
		,coalesce(bitUsePredefinedCustomer,false) AS bitUsePredefinedCustomer
		,coalesce(bitReceiveOrderWithNonMappedItem,false) AS bitReceiveOrderWithNonMappedItem
		,coalesce(numItemToUseForNonMappedItem,0) AS numItemToUseForNonMappedItem
   FROM
   Domain
   INNER JOIN
   DivisionMaster DMEmployer
   ON
   Domain.numDivisionId = DMEmployer.numDivisionID
   LEFT JOIN LATERAL(SELECT 
      DM.numDivisionID
			,CI.numCompanyId
			,CI.vcCompanyName
			,DM.numAssignedTo
			,ACI.numContactId
      FROM
      DivisionMaster DM
      INNER JOIN
      CompanyInfo CI
      ON
      DM.numCompanyID = CI.numCompanyId
      INNER JOIN
      AdditionalContactsInformation ACI
      ON
      DM.numDivisionID = ACI.numDivisionId
      WHERE
      DM.numDomainID = v_numDomainID
      AND DM.numDivisionID = v_numDivisionID
      AND coalesce(ACI.bitPrimaryContact,false) = true LIMIT 1) TEMP on TRUE
   WHERE
   Domain.numDomainId = v_numDomainID;
END; $$;












