-- Stored procedure definition script USP_GetListWebSourceLeads for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetListWebSourceLeads(v_numDomainId     NUMERIC(9,0),
	v_SourceDomain    VARCHAR(1000),
	v_FromDate        TIMESTAMP,
	v_LeadsOrAccounts SMALLINT, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_LeadsOrAccounts = 1 then

      open SWV_RefCur for
      SELECT CI.vcCompanyName,DM.numDivisionID,DM.numCompanyID,DM.numGrpId,DM.tintCRMType,GetDomainNameFromURL(TV.vcReferrer) AS SourceDomain,
CASE WHEN DM.tintCRMType = 0 THEN 'Lead' WHEN DM.tintCRMType = 1 THEN 'Prospects'  WHEN DM.tintCRMType = 2 THEN 'Account' END AS CRMType,
CASE WHEN CAST(DM.bintCreatedDate+INTERVAL '330 minute' AS VARCHAR(11)) = CAST(LOCALTIMESTAMP AS VARCHAR(11)) THEN '<b><font color=red>Today</font></b>'
      WHEN CAST(DM.bintCreatedDate+INTERVAL '330 minute' AS VARCHAR(11)) = CAST(LOCALTIMESTAMP+INTERVAL '-1 day' AS VARCHAR(11))
      THEN '<b><font color=purple>YesterDay</font></b>'
      WHEN CAST(DM.bintCreatedDate+INTERVAL '330 minute' AS VARCHAR(11)) = CAST(LOCALTIMESTAMP+INTERVAL '1 day' AS VARCHAR(11))
      THEN '<b><font color=orange>Tommorow</font></b>' ELSE FormatedDateFromDate(DM.bintCreatedDate+INTERVAL '330 minute',1::NUMERIC) END AS CompanyCreatedOn,
L4.vcData AS Survey ,
numPhone,
numPhoneExtension,
L7.vcData AS NumberOfUsers,
ADC.numContactId,DM.numTerID,ADC.numRecOwner,
vcFirstName,
vcLastname,
vcEmail
      FROM CompanyInfo CI
      INNER JOIN DivisionMaster DM ON DM.numCompanyID = CI.numCompanyId
      INNER JOIN TrackingVisitorsHDR TV ON TV.numDivisionID = DM.numDivisionID
      LEFT JOIN Listdetails lst ON lst.numListItemID = DM.numFollowUpStatus
      JOIN AdditionalContactsInformation ADC ON ADC.numDivisionId = DM.numDivisionID
      LEFT JOIN Listdetails L4 ON L4.numListItemID = DM.numFollowUpStatus
      LEFT JOIN Listdetails L7 ON L7.numListItemID = numNoOfEmployeesId
      WHERE CI.numDomainID = v_numDomainId AND 
--DM.tintCRMType = (CASE WHEN  @LeadsOrAccounts = 1 THEN 0
--WHEN @LeadsOrAccounts = 2 THEN  2 END)
      DM.tintCRMType IN(0,1)
      AND  coalesce(ADC.bitPrimaryContact,false) = true
      AND CI.numDomainID = DM.numDomainID
      AND DM.numDomainID = ADC.numDomainID
      AND DM.bitActiveInActive = true
      AND GetDomainNameFromURL(TV.vcReferrer) = v_SourceDomain
      AND TV.dtcreated > v_FromDate;
   end if;

   IF v_LeadsOrAccounts = 2 then

      open SWV_RefCur for
      SELECT CI.vcCompanyName,DM.numDivisionID,DM.numCompanyID,DM.numGrpId,DM.tintCRMType,GetDomainNameFromURL(TV.vcReferrer) AS SourceDomain,
CASE WHEN DM.tintCRMType = 0 THEN 'Lead' WHEN DM.tintCRMType = 1 THEN 'Prospects'  WHEN DM.tintCRMType = 2 THEN 'Account' END AS CRMType,
CASE WHEN CAST(DM.bintCreatedDate+INTERVAL '330 minute' AS VARCHAR(11)) = CAST(LOCALTIMESTAMP AS VARCHAR(11)) THEN '<b><font color=red>Today</font></b>'
      WHEN CAST(DM.bintCreatedDate+INTERVAL '330 minute' AS VARCHAR(11)) = CAST(LOCALTIMESTAMP+INTERVAL '-1 day' AS VARCHAR(11))
      THEN '<b><font color=purple>YesterDay</font></b>'
      WHEN CAST(DM.bintCreatedDate+INTERVAL '330 minute' AS VARCHAR(11)) = CAST(LOCALTIMESTAMP+INTERVAL '1 day' AS VARCHAR(11))
      THEN '<b><font color=orange>Tommorow</font></b>' ELSE FormatedDateFromDate(DM.bintCreatedDate+INTERVAL '330 minute',1::NUMERIC) END AS CompanyCreatedOn,
L4.vcData AS Survey ,
numPhone,
numPhoneExtension,
L7.vcData AS NumberOfUsers,
ADC.numContactId,DM.numTerID,ADC.numRecOwner,
vcFirstName,
vcLastname,
vcEmail
      FROM CompanyInfo CI
      INNER JOIN DivisionMaster DM ON DM.numCompanyID = CI.numCompanyId
      INNER JOIN TrackingVisitorsHDR TV ON TV.numDivisionID = DM.numDivisionID
      LEFT JOIN Listdetails lst ON lst.numListItemID = DM.numFollowUpStatus
      JOIN AdditionalContactsInformation ADC ON ADC.numDivisionId = DM.numDivisionID
      LEFT JOIN Listdetails L4 ON L4.numListItemID = DM.numFollowUpStatus
      LEFT JOIN Listdetails L7 ON L7.numListItemID = numNoOfEmployeesId
      WHERE CI.numDomainID = v_numDomainId AND 
--DM.tintCRMType = (CASE WHEN  @LeadsOrAccounts = 1 THEN 0
--WHEN @LeadsOrAccounts = 2 THEN  2 END)
      DM.tintCRMType = 2
      AND  coalesce(ADC.bitPrimaryContact,false) = true
      AND CI.numDomainID = DM.numDomainID
      AND DM.numDomainID = ADC.numDomainID
      AND DM.bitActiveInActive = true
      AND GetDomainNameFromURL(TV.vcReferrer) = v_SourceDomain
      AND TV.dtcreated > v_FromDate;
   end if;
   RETURN;
END; $$;


