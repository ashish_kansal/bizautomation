-- Stored procedure definition script USP_DepositAndOtherCreditDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_DepositAndOtherCreditDetails(v_numDomainId NUMERIC(9,0) DEFAULT 0,      
 v_numChartAcntId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select GJD.numChartAcntId as numChartAcntId, datEntry_Date as EntryDate,cast(CI.vcCompanyName as VARCHAR(255)) AS CompanyName,GJD.numDebitAmt as DebitAmt,
cast((Select cast(numAcntType as NUMERIC(18,0)) From Chart_Of_Accounts Where numAccountId = GJD.numChartAcntId And numDomainId = v_numDomainId) as VARCHAR(255)) as numAcntType From General_Journal_Header GJH
   inner join General_Journal_Details GJD on GJH.numJOurnal_Id = GJD.numJournalId
   Left outer join DivisionMaster as DM on GJD.numCustomerId = DM.numDivisionID
   Left outer join CompanyInfo as CI on  DM.numCompanyID = CI.numCompanyId
   Where GJD.bitReconcile = false And GJD.numDebitAmt <> 0 And GJD.numChartAcntId = v_numChartAcntId;
END; $$;












