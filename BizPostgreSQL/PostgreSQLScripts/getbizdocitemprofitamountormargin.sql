DROP FUNCTION IF EXISTS GetBizDocItemProfitAmountOrMargin;

CREATE OR REPLACE FUNCTION GetBizDocItemProfitAmountOrMargin(v_numDomainID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0)
	,v_numOppBizDocID NUMERIC(18,0)
	,v_numOppBizDocItemID NUMERIC(18,0)
	,v_tintMode SMALLINT --1: Profit Amount, 2:Profit Margin
)
RETURNS DECIMAL(20,5) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Value  DECIMAL(20,5);

   v_numDiscountItemID  NUMERIC(18,0);
   v_ProfitCost  INTEGER;
BEGIN
   select   numDiscountServiceItemID, numCost INTO v_numDiscountItemID,v_ProfitCost FROM Domain WHERE numDomainId = v_numDomainID;

   If v_tintMode = 1 then
	
      select   SUM(coalesce(TEMP.monTotAmount,0) -(coalesce(TEMP.numUnitHour,0)*(CASE
      WHEN coalesce(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode) IS NOT NULL
      THEN coalesce(coalesce(TEMPMatchedPO.monPrice,TEMPPO.monPrice),0)
      ELSE(CASE v_ProfitCost
         WHEN 3 THEN coalesce(TEMP.monCost,0)*fn_UOMConversion(TEMP.numBaseUnit,TEMP.numItemCode,v_numDomainID,TEMP.numPurchaseUnit)
         ELSE coalesce(TEMP.monAverageCost,0)
         END)
      END))) INTO v_Value FROM(SELECT
         OM.numOppId
				,OI.numoppitemtCode
				,OI.vcAttrValues
				,OBDI.numUnitHour
				,OBDI.monTotAmount
				,I.numItemCode
				,I.numBaseUnit
				,I.numPurchaseUnit
				,V.monCost
				,(CASE
         WHEN coalesce(I.bitKitParent,false) = true
         THEN coalesce((SELECT SUM(coalesce(OKI.numQtyItemsReq_Orig,0)*coalesce(OKI.monAvgCost,0)) FROM OpportunityKitItems OKI INNER JOIN Item IInner ON OKI.numChildItemID = IInner.numItemCode WHERE OKI.numOppId = OM.numOppId AND OKI.numOppItemID = OI.numoppitemtCode),0)+coalesce((SELECT SUM(coalesce(OKCI.numQtyItemsReq_Orig,0)*coalesce(OKI.numQtyItemsReq_Orig,0)*coalesce(OKCI.monAvgCost,0)) FROM OpportunityKitChildItems OKCI INNER JOIN OpportunityKitItems OKI ON OKCI.numOppChildItemID = OKI.numOppChildItemID INNER JOIN Item IInner ON OKCI.numItemID = IInner.numItemCode WHERE OKI.numOppId = OM.numOppId AND OKI.numOppItemID = OI.numoppitemtCode),0)
         ELSE coalesce(OBDI.monAverageCost,0)
         END) AS monAverageCost
         FROM
         OpportunityBizDocs OBD
         INNER JOIN
         OpportunityMaster OM
         ON
         OBD.numoppid = OM.numOppId
         INNER JOIN
         OpportunityBizDocItems OBDI
         ON
         OBD.numOppBizDocsId = OBDI.numOppBizDocID
         INNER JOIN
         OpportunityItems OI
         ON
         OBDI.numOppItemID = OI.numoppitemtCode
         INNER JOIN
         Item I
         ON
         OBDI.numItemCode = I.numItemCode
         LEFT JOIN
         Vendor V
         ON
         V.numVendorID = I.numVendorID
         AND V.numItemCode = I.numItemCode
         WHERE
         OBD.numoppid = v_numOppID
         AND OBD.numOppBizDocsId = v_numOppBizDocID
         AND OBDI.numOppBizDocItemID = v_numOppBizDocItemID
         AND I.numItemCode NOT IN(v_numDiscountItemID)) TEMP
      LEFT JOIN LATERAL(SELECT 
         OMInner.numOppId
				,OMInner.vcpOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
         FROM
         SalesOrderLineItemsPOLinking SOLIPL
         INNER JOIN
         OpportunityMaster OMInner
         ON
         SOLIPL.numPurchaseOrderID = OMInner.numOppId
         INNER JOIN
         OpportunityItems OIInner
         ON
         OMInner.numOppId = OIInner.numOppId
         AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
         WHERE
         SOLIPL.numSalesOrderID = TEMP.numOppId
         AND SOLIPL.numSalesOrderItemID = TEMP.numoppitemtCode 
		 --AND COALESCE(SOLIPL.numSalesOrderOppChildItemID,0) = 0 
		 --AND COALESCE(SOLIPL.numSalesOrderOppKitChildItemID,0) = 0 
		 LIMIT 1) TEMPMatchedPO
      LEFT JOIN LATERAL(SELECT 
         OMInner.numOppId
				,OMInner.vcpOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
         FROM
         OpportunityLinking OL
         INNER JOIN
         OpportunityMaster OMInner
         ON
         OL.numChildOppID = OMInner.numOppId
         INNER JOIN
         OpportunityItems OIInner
         ON
         OMInner.numOppId = OIInner.numOppId
         WHERE
         OL.numParentOppID = TEMP.numOppId
         AND OIInner.numItemCode = TEMP.numItemCode
         AND OIInner.vcAttrValues = TEMP.vcAttrValues LIMIT 1) TEMPPO on TRUE on TRUE;
   ELSEIF v_tintMode = 2
   then
	
      select(SUM(Profit)/CASE WHEN SUM(monTotAmount) = 0 THEN 1.0 ELSE SUM(monTotAmount) END)*100 INTO v_Value FROM(SELECT
         coalesce(OBDI.monTotAmount,0) AS monTotAmount
				,coalesce(OBDI.monTotAmount,0) -(coalesce(OBDI.numUnitHour,0)*(CASE
         WHEN coalesce(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode) IS NOT NULL
         THEN coalesce(coalesce(TEMPMatchedPO.monPrice,TEMPPO.monPrice),0)
         ELSE(CASE v_ProfitCost
            WHEN 3 THEN coalesce(V.monCost,0)*fn_UOMConversion(numBaseUnit,I.numItemCode,v_numDomainID,numPurchaseUnit)
            ELSE(CASE
               WHEN coalesce(I.bitKitParent,false) = true
               THEN coalesce((SELECT SUM(coalesce(OKI.numQtyItemsReq_Orig,0)*coalesce(OKI.monAvgCost,0)) FROM OpportunityKitItems OKI INNER JOIN Item IInner ON OKI.numChildItemID = IInner.numItemCode WHERE OKI.numOppId = OM.numOppId AND OKI.numOppItemID = OI.numoppitemtCode),0)+coalesce((SELECT SUM(coalesce(OKCI.numQtyItemsReq_Orig,0)*coalesce(OKI.numQtyItemsReq_Orig,0)*coalesce(OKCI.monAvgCost,0)) FROM OpportunityKitChildItems OKCI INNER JOIN OpportunityKitItems OKI ON OKCI.numOppChildItemID = OKI.numOppChildItemID INNER JOIN Item IInner ON OKCI.numItemID = IInner.numItemCode WHERE OKI.numOppId = OM.numOppId AND OKI.numOppItemID = OI.numoppitemtCode),0)
               ELSE coalesce(OBDI.monAverageCost,0)
               END)
            END)
         END)) AS Profit
         FROM
         OpportunityBizDocs OBD
         INNER JOIN
         OpportunityMaster OM
         ON
         OBD.numoppid = OM.numOppId
         INNER JOIN
         OpportunityBizDocItems OBDI
         ON
         OBD.numOppBizDocsId = OBDI.numOppBizDocID
         INNER JOIN
         OpportunityItems OI
         ON
         OBDI.numOppItemID = OI.numoppitemtCode
         INNER JOIN
         Item I
         ON
         OBDI.numItemCode = I.numItemCode
         LEFT JOIN LATERAL(SELECT 
            OMInner.numOppId
					,OMInner.vcpOppName
					,OIInner.numoppitemtCode
					,OIInner.monPrice
            FROM
            SalesOrderLineItemsPOLinking SOLIPL
            INNER JOIN
            OpportunityMaster OMInner
            ON
            SOLIPL.numPurchaseOrderID = OMInner.numOppId
            INNER JOIN
            OpportunityItems OIInner
            ON
            OMInner.numOppId = OIInner.numOppId
            AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
            WHERE
            SOLIPL.numSalesOrderID = OM.numOppId
            AND SOLIPL.numSalesOrderItemID = OI.numoppitemtCode
			--AND COALESCE(SOLIPL.numSalesOrderOppChildItemID,0) = 0 
			--AND COALESCE(SOLIPL.numSalesOrderOppKitChildItemID,0) = 0 
			LIMIT 1) TEMPMatchedPO ON TRUE
         LEFT JOIN LATERAL(SELECT 
            OMInner.numOppId
					,OMInner.vcpOppName
					,OIInner.numoppitemtCode
					,OIInner.monPrice
            FROM
            OpportunityLinking OL
            INNER JOIN
            OpportunityMaster OMInner
            ON
            OL.numChildOppID = OMInner.numOppId
            INNER JOIN
            OpportunityItems OIInner
            ON
            OMInner.numOppId = OIInner.numOppId
            WHERE
            OL.numParentOppID = OM.numOppId
            AND OIInner.numItemCode = OI.numItemCode
            AND OIInner.vcAttrValues = OI.vcAttrValues LIMIT 1) TEMPPO ON TRUE
         Left JOIN
         Vendor V
         ON
         V.numVendorID = I.numVendorID
         AND V.numItemCode = I.numItemCode
         WHERE
         OBD.numoppid = v_numOppID
         AND OBD.numOppBizDocsId = v_numOppBizDocID
         AND OBDI.numOppBizDocItemID = v_numOppBizDocItemID
         AND I.numItemCode NOT IN(v_numDiscountItemID)) TEMP;
   end if;

   RETURN v_Value;
END; $$;

