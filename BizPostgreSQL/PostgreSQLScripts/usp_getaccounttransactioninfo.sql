CREATE OR REPLACE FUNCTION public.usp_getaccounttransactioninfo(
	v_numdomainid numeric,
	v_tintreferencetype numeric,
	v_numreferenceid numeric,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)    
LANGUAGE 'plpgsql'
AS $BODY$
DECLARE
   v_numJournal_Id  NUMERIC(18,0);
   v_numTransactionId  NUMERIC(18,0);
   v_bitReconcile  BOOLEAN;
BEGIN
-- This function was converted on Sat Apr 10 14:55:55 2021 using Ispirer SQLWays 9.0 Build 6532 64bit Licensed to BizAutomation - Sandeep Patel - India - Ispirer MnMTK 2020 MSSQLServer to PostgreSQL Database Migration Professional Project License (50 GB, 750 Tables, 426500 LOC, 3 Extension Requests, 1 Month, 20210508).
   v_numJournal_Id := 0;
   v_numTransactionId := 0;
   v_bitReconcile := false;

   IF v_numReferenceID > 0 then

      IF v_tintReferenceType = 1 then --Write Check
	
         select   numJOurnal_Id INTO v_numJournal_Id FROM General_Journal_Header WHERE numDomainId = v_numDomainId AND numCheckHeaderID = v_numReferenceID;
      ELSEIF v_tintReferenceType = 5
      then --Direct Journal
	
         select   numJOurnal_Id INTO v_numJournal_Id FROM General_Journal_Header WHERE numDomainId = v_numDomainId AND numJOurnal_Id = v_numReferenceID;
      ELSEIF v_tintReferenceType = 3
      then --Add Bill
	
         select   numJOurnal_Id INTO v_numJournal_Id FROM General_Journal_Header WHERE numDomainId = v_numDomainId AND numBillID = v_numReferenceID;
      ELSEIF v_tintReferenceType = 8
      then --Bill Payment Header
	
         select   numJOurnal_Id INTO v_numJournal_Id FROM General_Journal_Header WHERE numDomainId = v_numDomainId AND numBillPaymentID = v_numReferenceID;
      ELSEIF v_tintReferenceType = 6
      then --Deposit Header
	
         select   numJOurnal_Id INTO v_numJournal_Id FROM General_Journal_Header WHERE numDomainId = v_numDomainId AND numDepositId = v_numReferenceID;
      ELSEIF v_tintReferenceType = 12
      then --TimeAndExpense
	
         select   numJOurnal_Id INTO v_numJournal_Id FROM General_Journal_Header WHERE numDomainId = v_numDomainId AND numCategoryHDRID = v_numReferenceID;
      ELSEIF v_tintReferenceType = 10
      then --RMA
	
         select   numJOurnal_Id INTO v_numJournal_Id FROM General_Journal_Header WHERE numDomainId = v_numDomainId AND numReturnID = v_numReferenceID;
      end if;
      IF v_numJournal_Id > 0 then
	
         select   numTransactionId INTO v_numTransactionId FROM General_Journal_Details WHERE numJournalId = v_numJournal_Id AND numReferenceID = v_numReferenceID AND tintReferenceType = v_tintReferenceType;
         IF(SELECT COUNT(numTransactionId) FROM General_Journal_Details WHERE numJournalId = v_numJournal_Id AND coalesce(bitReconcile,false) = true) > 0 then
		
            v_bitReconcile := true;
         end if;
      end if;
   end if;
	 
   open SWV_RefCur for SELECT v_numJournal_Id AS numJournal_Id,v_bitReconcile AS bitReconcile,v_numTransactionId AS numTransactionId;
END;
$BODY$;

