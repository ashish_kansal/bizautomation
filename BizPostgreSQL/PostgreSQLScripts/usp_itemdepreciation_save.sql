-- Stored procedure definition script USP_ItemDepreciation_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ItemDepreciation_Save(v_numDomainID NUMERIC(18,0),
	v_numOppId NUMERIC(18,0),
	v_numOppItemID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_monPrice  DECIMAL(20,5);
   v_numUnitHourReceived  DOUBLE PRECISION;
   v_numItemCode  NUMERIC(18,0);
   v_bitAsset  BOOLEAN;
   v_numDepreciationPeriod  NUMERIC(18,1);
   v_monResidualValue  DECIMAL(20,5);
   v_tintDepreciationMethod  SMALLINT;
   v_dtItemReceivedDate  TIMESTAMP;
   v_monStartingValue  DECIMAL(20,5) DEFAULT v_monPrice -v_monResidualValue;

   v_numRate  DECIMAL(20,2) DEFAULT((v_monPrice/v_numDepreciationPeriod)/v_monPrice)*100;
   v_i  INTEGER DEFAULT 0;
BEGIN
   select   I.numItemCode, coalesce(I.bitAsset,false), coalesce(IDC.numDepreciationPeriod,0), coalesce(IDC.monResidualValue,0), coalesce(IDC.tintDepreciationMethod,1), coalesce(OI.numUnitHourReceived,0), coalesce(monPrice,0), dtItemReceivedDate INTO v_numItemCode,v_bitAsset,v_numDepreciationPeriod,v_monResidualValue,v_tintDepreciationMethod,
   v_numUnitHourReceived,v_monPrice,v_dtItemReceivedDate FROM
   OpportunityItems OI
   INNER JOIN
   Item I
   ON
   OI.numItemCode = I.numItemCode
   LEFT JOIN
   ItemDepreciationConfig IDC
   ON
   I.numItemCode = IDC.numItemCode WHERE
   OI.numOppId = v_numOppId
   AND OI.numoppitemtCode = v_numOppItemID;


   IF EXISTS(SELECT ID FROM ItemDepreciation WHERE numItemCode = v_numItemCode AND numOppID <> v_numOppId) then
	
      RAISE EXCEPTION 'CAN_NOT_CREATE_MULTIPLE_PO_FOR_ASSET_ITEMS';
   ELSEIF v_numUnitHourReceived = 0
   then
	
      DELETE FROM ItemDepreciation WHERE numItemCode = v_numItemCode AND numOppID = v_numOppId AND numOppItemID = v_numOppItemID;
   ELSEIF v_bitAsset = true AND v_monPrice > 0 AND v_numDepreciationPeriod > 0 AND v_monPrice > v_monResidualValue
   then
      WHILE v_numDepreciationPeriod > 0 LOOP
         INSERT INTO ItemDepreciation(numItemCode
				,numOppID
				,numOppItemID
				,intYear
				,monStartingValue
				,numRate
				,monDepreciation
				,monEndingValue)
			VALUES(v_numItemCode
				,v_numOppId
				,v_numOppItemID
				,EXTRACT(YEAR FROM v_dtItemReceivedDate)+v_i::bigint
				,v_monStartingValue
				,v_numRate
				,(CASE WHEN v_tintDepreciationMethod = 2 THEN v_monStartingValue*(v_numRate/100) ELSE v_monPrice*(v_numRate/100) END)
				,(CASE
         WHEN v_numDepreciationPeriod = 1
         THEN v_monResidualValue
         ELSE(CASE WHEN v_tintDepreciationMethod = 2 THEN v_monStartingValue -(v_monStartingValue*(v_numRate/100))  ELSE v_monStartingValue -(v_monPrice*(v_numRate/100))  END)
         END));
			
         IF v_tintDepreciationMethod = 2 then
			
            v_monStartingValue := v_monStartingValue -(v_monStartingValue*(v_numRate/100));
         ELSE
            v_monStartingValue := v_monStartingValue -(v_monPrice*(v_numRate/100));
         end if;
         v_i := v_i::bigint+1;
         v_numDepreciationPeriod := v_numDepreciationPeriod -1;
      END LOOP;
   end if;
   RETURN;
END; $$;


