-- Stored procedure definition script USP_CategoryProfile_GetWithSites for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_CategoryProfile_GetWithSites(v_ID NUMERIC(18,0),
	v_numDomainID NUMERIC(18,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   SELECT ID,vcName FROM CategoryProfile WHERE ID = v_ID;

   open SWV_RefCur2 for
   SELECT
   CategoryProfileSites.* ,
		Sites.vcSiteName
   FROM
   CategoryProfileSites
   INNER JOIN
   Sites
   ON
   CategoryProfileSites.numSiteID = Sites.numSiteID
   WHERE
   numCategoryProfileID = v_ID;
   RETURN;
END; $$;



