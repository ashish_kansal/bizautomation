-- Stored procedure definition script USP_GetParentToAddRequiredItem for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetParentToAddRequiredItem(v_numDomainId NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numItemCode  NUMERIC(18,0);
BEGIN
   select   numItemCode INTO v_numItemCode FROM CartItems WHERE numDomainId = v_numDomainId   ORDER BY numCartId DESC LIMIT 1;
	--SELECT numItemCode FROM SimilarItems WHERE numParentItemCode = @numItemCode AND bitRequired = 1
	
   open SWV_RefCur for SELECT I.numItemCode,vcItemName FROM Item I
   LEFT JOIN SimilarItems  AS SI ON I.numItemCode = SI.numItemCode
   WHERE numParentItemCode = v_numItemCode AND bitRequired = true AND I.numDomainID = v_numDomainId;
   RETURN;
END; $$;














