-- Stored procedure definition script USP_BulkOppTemp for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_BulkOppTemp(v_numContactId BIGINT,
	v_numDivisionId BIGINT,
	v_numOppId BIGINT,
	v_IsCreated BOOLEAN)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   INSERT INTO MassSalesOrderQueue(numContactId
		,numDivisionId
		,numOppId
		,bitExecuted)
	VALUES(v_numContactId
		,v_numDivisionId
		,v_numOppId
		,v_IsCreated);


RETURN;
END; $$;




