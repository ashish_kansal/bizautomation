-- Stored procedure definition script USP_Journal_EntryList for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Journal_EntryList(v_numChartAcntId NUMERIC(9,0),                                                                                                                          
v_dtDateFrom TIMESTAMP,                        
v_numDomainId NUMERIC(9,0),
v_numDivisionID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSQL  VARCHAR(8000);                                                                                                                      
   v_strChildCategory  VARCHAR(5000);                                                                              
   v_numAcntTypeId  INTEGER;                                                                                                                 
   v_OpeningBal  VARCHAR(8000);  
   v_vcAccountCode  VARCHAR(50); 
   v_vcDate  VARCHAR(50);
   SWV_ExecDyn  VARCHAR(5000);
BEGIN
   v_OpeningBal := 'Opening Balance';                                                                                                  
   v_strSQL := '';                                                                                              
--Create a Temporary table to hold data     

   v_vcDate := SUBSTR(CAST(v_dtDateFrom AS VARCHAR(50)),1,50);

                                                                                                                           
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   Drop table IF EXISTS tt_TEMPTABLE CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLE 
   ( 
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numCustomerId NUMERIC(9,0),
      JournalId NUMERIC(9,0),
      TransactionType VARCHAR(500),
      EntryDate TIMESTAMP,
      CompanyName VARCHAR(8000),
      CheckId NUMERIC(9,0),
      CashCreditCardId NUMERIC(9,0),
      Memo VARCHAR(8000),
      Payment DECIMAL(20,5),
      Deposit DECIMAL(20,5),
      numBalance DECIMAL(20,5),
      numChartAcntId NUMERIC(9,0),
      numTransactionId NUMERIC(9,0),
      numOppID NUMERIC(9,0),
      numOppBizDocsId NUMERIC(9,0),
      numDepositId NUMERIC(9,0),
      numBizDocsPaymentDetId NUMERIC(9,0),
      numCategoryHDRID NUMERIC(9,0),
      tintTEType   NUMERIC(9,0),
      numCategory  NUMERIC(9,0),
      numUserCntID   NUMERIC(9,0),
      dtFromDate TIMESTAMP,
      numCheckNo VARCHAR(20),
      bitReconcile BOOLEAN,
      bitCleared BOOLEAN
   );
     
     
                                            
   if v_dtDateFrom = '1900-01-01 00:00:00.000' then 
      v_dtDateFrom := CAST('Jan  1 1753 12:00:00:000AM' AS TIMESTAMP);
   end if;                                               
   
   select   vcAccountCode INTO v_vcAccountCode FROM Chart_Of_Accounts WHERE numAccountId = v_numChartAcntId;
   select   COALESCE(coalesce(v_strChildCategory,'') || ',','') ||
   SUBSTR(CAST(numAccountId AS VARCHAR(10)),1,10) INTO v_strChildCategory FROM Chart_Of_Accounts WHERE vcAccountCode ilike coalesce(v_vcAccountCode,'') || '%';

   RAISE NOTICE '%',v_strChildCategory;
                                           
   if v_dtDateFrom = 'Jan  1 1753 12:00:00:000AM' then
                                                                                  
--Commented by chintan
--Set @strChildCategory = dbo.fn_ChildCategory(@numChartAcntId,@numDomainId) + Convert(varchar(10),@numChartAcntId)                                                           

                                                                          
      select   coalesce(numParntAcntTypeID,0) INTO v_numAcntTypeId from Chart_Of_Accounts Where numAccountId = v_numChartAcntId And numDomainId = v_numDomainId;
      RAISE NOTICE '%',v_numAcntTypeId;
      if v_numAcntTypeId = 815 or v_numAcntTypeId = 816 or v_numAcntTypeId = 820  or v_numAcntTypeId = 821  or v_numAcntTypeId = 822  or v_numAcntTypeId = 825 or v_numAcntTypeId = 827 then

         v_strSQL := ' Select GJD.numCustomerId AS numCustomerId,GJH.numJournal_Id as JournalId,        
case when isnull(GJH.numCheckId,0) <> 0 then ''Checks''  
Else case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=false then ''Cash''        
ELse Case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=true And CCCD.bitChargeBack=true  then ''Charge''        
ELse Case when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=1 then ''BizDocs Invoice''        
ELse Case when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=2 then ''BizDocs Purchase''        
ELse Case when isnull(GJH.numDepositId,0) <> 0 then ''Deposit''        
ELse Case when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=1 then ''Receive Amt''        
ELse Case when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=2 then ''Vendor Amt''        
Else Case when isnull(GJH.numCategoryHDRID,0)<>0 then ''Time And Expenses''  
Else Case When GJH.numJournal_Id <>0 then ''Journal'' End End End End End End End End End End  as TransactionType,        
GJH.datEntry_Date as EntryDate,CI.vcCompanyName AS CompanyName,GJH.numCheckId As CheckId,        
GJH.numCashCreditCardId as CashCreditCardId,        
(case when len(GJD.varDescription) =0 then GJH.varDescription else GJD.varDescription END)  as Memo,        
(case when GJD.numCreditAmt=0 then GJD.numdebitAmt  end) as Deposit,        
(case when GJD.numdebitAmt=0 then GJD.numCreditAmt end) as Payment,        
null numBalance,        
isnull(GJD.numChartAcntId,0) as numChartAcntId,        
GJD.numTransactionId as numTransactionId,               
GJH.numOppId as numOppId,        
GJH.numOppBizDocsId as numOppBizDocsId,        
GJH.numDepositId as numDepositId,          
GJH.numBizDocsPaymentDetId as numBizDocsPaymentDetId,  
isnull(GJH.numCategoryHDRID,0) as numCategoryHDRID,  
isnull(TE.tintTEType,0) as tintTEType,  
TE.numCategory as numCategory,  
TE.numUserCntID as numUserCntID,
TE.dtFromDate as dtFromDate,
isnull(CAST(CD.numCheckNo AS VARCHAR(20)),'''') numCheckNo,
isnull(GJD.bitReconcile,false) as bitReconcile,
isnull(GJD.bitCleared,false) as bitCleared
From General_Journal_Header as GJH        
Left Outer Join General_Journal_Details as GJD  on GJH.numJournal_Id=GJD.numJournalId        
Left outer join DivisionMaster as DM on GJD.numCustomerId=DM.numDivisionID        
Left outer join CompanyInfo as CI on  DM.numCompanyID=CI.numCompanyId        
Left outer join Chart_Of_Accounts CA on GJD.numChartAcntId=CA.numAccountId        
Left outer join CashCreditCardDetails CCCD on GJH.numCashCreditCardId=CCCD.numCashCreditId        
Left outer join CheckDetails CD on GJH.numCheckId=CD.numCheckId        
Left outer join OpportunityMaster Opp on GJH.numOppId=Opp.numOppId    
Left outer join TimeAndExpense TE on GJH.numCategoryHDRID=TE.numCategoryHDRID     
Where GJH.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(4)),1,4) || ' And  GJD.numChartAcntId in (' || coalesce(v_strChildCategory,'') || ')'
         || 'ORDER BY GJH.datEntry_Date';
      Else
         v_strSQL := ' Select GJD.numCustomerId AS numCustomerId,GJH.numJournal_Id as JournalId,        
case when isnull(GJH.numCheckId,0) <> 0 then +''Checks'' Else case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=false then ''Cash''        
ELse Case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=true And CCCD.bitChargeBack=true  then ''Charge''        
ELse Case when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=1 then ''BizDocs Invoice''        
ELse Case when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=2 then ''BizDocs Purchase''        
ELse Case when isnull(GJH.numDepositId,0) <> 0 then ''Deposit''        
ELse Case when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=1 then ''Receive Amt''        
ELse Case when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=2 then ''Vendor Amt''  
Else Case when isnull(GJH.numCategoryHDRID,0)<>0 then ''Time And Expenses''        
Else Case When GJH.numJournal_Id <>0 then ''Journal'' End End End End End End End End End End as TransactionType,        
GJH.datEntry_Date as EntryDate,CI.vcCompanyName AS CompanyName,GJH.numCheckId As CheckId,        
GJH.numCashCreditCardId as CashCreditCardId,        
(case when len(GJD.varDescription) =0 then GJH.varDescription else GJD.varDescription END) as Memo,        
(case when GJD.numCreditAmt<>0 then numCreditAmt end) as Payment,        
(case when GJD.numdebitAmt<>0 then numDebitAmt  end) as Deposit,        
null numBalance,        
isnull(GJD.numChartAcntId,0) as numChartAcntId,        
GJD.numTransactionId as numTransactionId,        
GJH.numOppId as numOppId,        
GJH.numOppBizDocsId as numOppBizDocsId,        
GJH.numDepositId as numDepositId,        
GJH.numBizDocsPaymentDetId as numBizDocsPaymentDetId,  
isnull(GJH.numCategoryHDRID,0) as numCategoryHDRID,  
isnull(TE.tintTEType,0) as tintTEType,  
TE.numCategory as numCategory,  
TE.numUserCntID as numUserCntID,
TE.dtFromDate as dtFromDate,
isnull(CAST(CD.numCheckNo AS VARCHAR(20)),'''') numCheckNo,
isnull(GJD.bitReconcile,false) as bitReconcile,
isnull(GJD.bitCleared,false) as bitCleared
From General_Journal_Header as GJH        
Left Outer Join General_Journal_Details as GJD  on GJH.numJournal_Id=GJD.numJournalId        
Left outer join DivisionMaster as DM on GJD.numCustomerId=DM.numDivisionID        
Left outer join CompanyInfo as CI on  DM.numCompanyID=CI.numCompanyId        
Left outer join Chart_Of_Accounts CA on GJD.numChartAcntId=CA.numAccountId        
Left outer join CashCreditCardDetails CCCD on GJH.numCashCreditCardId=CCCD.numCashCreditId        
Left outer join CheckDetails CD on GJH.numCheckId=CD.numCheckId        
Left outer join OpportunityMaster Opp on GJH.numOppId=Opp.numOppId   
Left outer join TimeAndExpense TE on GJH.numCategoryHDRID=TE.numCategoryHDRID        
Where GJH.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(4)),1,4) || ' /*And GJH.numChartAcntId is null */ And  GJD.numChartAcntId in (';
         if coalesce(v_strChildCategory,'0') = '0' then
            v_strSQL := coalesce(v_strSQL,'') || 'select COAN.numAccountId from Chart_Of_Accounts COAN where COAN.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(50)),1,50) || ')';
         else
            v_strSQL := coalesce(v_strSQL,'') || coalesce(v_strChildCategory,'') || ')';
         end if;
         if v_numDivisionID > 0 then
            v_strSQL := coalesce(v_strSQL,'') || ' AND GJD.numCustomerId=' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(50)),1,50);
         end if;
         v_strSQL := coalesce(v_strSQL,'') || 'ORDER BY GJH.datEntry_Date';
      end if;
      RAISE NOTICE '%',(v_strChildCategory);
      RAISE NOTICE '%',(v_strSQL);
   Else        
                                                                       
--Set @strChildCategory = dbo.fn_ChildCategory(@numChartAcntId,@numDomainId) + Convert(varchar(10),@numChartAcntId)                                                                                                               
      select   coalesce(numParntAcntTypeID,0) INTO v_numAcntTypeId from Chart_Of_Accounts Where numAccountId = v_numChartAcntId;
      RAISE NOTICE '%',coalesce(v_strChildCategory,'0') || ' One';
      if v_numAcntTypeId = 815 or v_numAcntTypeId = 816 or v_numAcntTypeId = 820  or v_numAcntTypeId = 821  or v_numAcntTypeId = 822  or v_numAcntTypeId = 825 or v_numAcntTypeId = 827 then

         v_strSQL := coalesce(v_strSQL,'') || ' Select GJD.numCustomerId,GJH.numJournal_Id as JournalId,         
case when isnull(GJH.numCheckId,0) <> 0 then +''Checks'' Else case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=false then ''Cash''        
ELse Case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=true And CCCD.bitChargeBack=true  then  ''Charge''        
ELse Case when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=1 then ''BizDocs Invoice''        
ELse Case when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=2 then ''BizDocs Purchase''        
ELse Case when isnull(GJH.numDepositId,0) <> 0 then ''Deposit''        
ELse Case when isnull(GJH.numBizDocsPaymentDetId,0) <> 0 And Opp.tintOppType=1 then ''Receive Amt''        
ELse Case when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=2 then ''Vendor Amt''        
Else Case when isnull(GJH.numCategoryHDRID,0)<>0 then ''Time And Expenses''  
Else Case When GJH.numJournal_Id <>0 then ''Journal'' End End End End End End End End End End as TransactionType,        
GJH.datEntry_Date as EntryDate,CI.vcCompanyName CompanyName,GJH.numCheckId As CheckId,        
GJH.numCashCreditCardId as CashCreditCardId,        
(case when len(GJD.varDescription) =0 then GJH.varDescription else GJD.varDescription END)  as Memo,        
(case when GJD.numCreditAmt=0 then GJD.numdebitAmt  end) as Deposit,        
(case when GJD.numdebitAmt=0 then GJD.numCreditAmt end) as Payment,        
null numBalance,        
isnull(GJD.numChartAcntId,0) as numChartAcntId,        
GJD.numTransactionId as numTransactionId,        
GJH.numOppId as numOppId,        
GJH.numOppBizDocsId as numOppBizDocsId,        
GJH.numDepositId as numDepositId,        
GJH.numBizDocsPaymentDetId as numBizDocsPaymentDetId,  
isnull(GJH.numCategoryHDRID,0) as numCategoryHDRID,  
isnull(TE.tintTEType,0) as tintTEType,  
TE.numCategory as numCategory,  
TE.numUserCntID as numUserCntID,
TE.dtFromDate as dtFromDate,
isnull(CAST(CD.numCheckNo AS VARCHAR(20)),'''') numCheckNo,
isnull(GJD.bitReconcile,false) as bitReconcile,
isnull(GJD.bitCleared,false) as bitCleared
From General_Journal_Header as GJH        
Left Outer Join General_Journal_Details as GJD on GJH.numJournal_Id=GJD.numJournalId        
Left outer join DivisionMaster as DM on GJD.numCustomerId=DM.numDivisionID        
Left outer join CompanyInfo as CI on DM.numCompanyID=CI.numCompanyId        
Left outer join CashCreditCardDetails CCCD on GJH.numCashCreditCardId=CCCD.numCashCreditId        
Left outer join CheckDetails CD on GJH.numCheckId=CD.numCheckId        
Left outer join OpportunityMaster Opp on GJH.numOppId=Opp.numOppId   
Left outer join TimeAndExpense TE on GJH.numCategoryHDRID=TE.numCategoryHDRID        
Where GJH.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(4)),1,4) || ' And GJH.numChartAcntId is null And  GJD.numChartAcntId in (';
         if coalesce(v_strChildCategory,'0') = '0' then
            v_strSQL := coalesce(v_strSQL,'') || 'select COAN.numAccountId from Chart_Of_Accounts COAN where COAN.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(50)),1,50) || ')';
         else
            v_strSQL := coalesce(v_strSQL,'') || coalesce(v_strChildCategory,'') || ')';
         end if;
         v_strSQL := coalesce(v_strSQL,'') || ' And day(GJH.datEntry_Date)=day(''' || coalesce(v_vcDate,'') || ''') and month( GJH.datEntry_Date)=month(''' || coalesce(v_vcDate,'') || ''') and year(GJH.datEntry_Date)=year(''' || coalesce(v_vcDate,'') || ''')';
         if v_numDivisionID > 0 then
            v_strSQL := coalesce(v_strSQL,'') || ' AND GJD.numCustomerId=' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(50)),1,50);
         end if;
         v_strSQL := coalesce(v_strSQL,'') || 'ORDER BY GJH.datEntry_Date';
      Else
         RAISE NOTICE '%',coalesce(v_strChildCategory,'0') || ' two';
         v_strSQL := ' Select GJD.numCustomerId,GJH.numJournal_Id as JournalId,        
case when isnull(GJH.numCheckId,0) <> 0 then +''Checks'' Else case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=false then ''Cash''        
ELse Case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=true And CCCD.bitChargeBack=true  then ''Charge''        
ELse Case when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=1 then ''BizDocs Invoice''        
ELse Case when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=2 then ''BizDocs Purchase''        
ELse Case when isnull(GJH.numDepositId,0) <> 0 then ''Deposit''        
ELse Case when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=1 then ''Receive Amt''        
ELse Case when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=2 then ''Vendor Amt''    
Else Case when isnull(GJH.numCategoryHDRID,0)<>0 then ''Time And Expenses''      
Else Case When GJH.numJournal_Id <>0 then ''Journal'' End End End End End End End End End End as TransactionType,        
GJH.datEntry_Date as EntryDate,CI.vcCompanyName CompanyName,GJH.numCheckId As CheckId,        
GJH.numCashCreditCardId as CashCreditCardId,        
(case when len(GJD.varDescription) =0 then GJH.varDescription else GJD.varDescription END)  as Memo,        
(case when GJD.numCreditAmt<>0 then GJD.numCreditAmt end) as Payment,        
(case when GJD.numdebitAmt<>0 then GJD.numDebitAmt  end) as Deposit,        
null numBalance,        
isnull(GJD.numChartAcntId,0) as numChartAcntId,        
GJD.numTransactionId as numTransactionId,        
GJH.numOppId as numOppId,        
GJH.numOppBizDocsId as numOppBizDocsId,        
GJH.numDepositId as numDepositId,        
GJH.numBizDocsPaymentDetId as numBizDocsPaymentDetId,  
isnull(GJH.numCategoryHDRID,0) as numCategoryHDRID,  
isnull(TE.tintTEType,0) as tintTEType,  
TE.numCategory as numCategory,  
TE.numUserCntID as numUserCntID,
TE.dtFromDate as dtFromDate,
isnull(CAST(CD.numCheckNo AS VARCHAR(20)),'''') numCheckNo,
isnull(GJD.bitReconcile,false) as bitReconcile,
isnull(GJD.bitCleared,false) as bitCleared
From General_Journal_Header as GJH        
Left Outer Join General_Journal_Details as GJD  on GJH.numJournal_Id=GJD.numJournalId        
Left outer join DivisionMaster as DM on GJD.numCustomerId=DM.numDivisionID        
Left outer join CompanyInfo as CI on  DM.numCompanyID=CI.numCompanyId        
Left outer join CashCreditCardDetails CCCD on GJH.numCashCreditCardId=CCCD.numCashCreditId        
Left outer join CheckDetails CD on GJH.numCheckId=CD.numCheckId        
Left outer join OpportunityMaster Opp on GJH.numOppId=Opp.numOppId        
Left outer join TimeAndExpense TE on GJH.numCategoryHDRID=TE.numCategoryHDRID   
Where GJH.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(4)),1,4) || ' And GJH.numChartAcntId is null And  GJD.numChartAcntId in (';
         if coalesce(v_strChildCategory,'0') = '0' then
            v_strSQL := coalesce(v_strSQL,'') || 'select COAN.numAccountId from Chart_Of_Accounts COAN where COAN.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(50)),1,50) || ')';
         else
            v_strSQL := coalesce(v_strSQL,'') || coalesce(v_strChildCategory,'') || ')';
         end if;
         v_strSQL := coalesce(v_strSQL,'') || ' And day(GJH.datEntry_Date)=day(''' || coalesce(v_vcDate,'') || ''') and month( GJH.datEntry_Date)=month(''' || coalesce(v_vcDate,'') || ''') and year(GJH.datEntry_Date)=year(''' || coalesce(v_vcDate,'') || ''')';
         if v_numDivisionID > 0 then
            v_strSQL := coalesce(v_strSQL,'') || ' AND GJD.numCustomerId=' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(50)),1,50);
         end if;
         v_strSQL := coalesce(v_strSQL,'') || 'ORDER BY GJH.datEntry_Date';
      end if;
      RAISE NOTICE '%',(v_strChildCategory);
      RAISE NOTICE '%',(v_strSQL);
   end if;                                                                                                                                                                                 
   SWV_ExecDyn := 'insert into #tempTable' || coalesce(v_strSQL,'');
   EXECUTE SWV_ExecDyn;                                                                                           
                                                                                       
   open SWV_RefCur for Select * from tt_TEMPTABLE; 

   Drop table IF EXISTS tt_TEMPTABLE CASCADE;
   RETURN;
END; $$;













