-- Stored procedure definition script USP_UpdateAPISettingDate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateAPISettingDate(v_WebApiId    INTEGER,
                v_numDomainId NUMERIC(9,0),
                v_tintMode     SMALLINT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF (v_tintMode = 1) then --OrdersLastUpdatedDate'
      
      UPDATE WebAPIDetail
      SET    vcNinthFldValue = TIMEZONE('UTC',now())
      WHERE  WebApiId = v_WebApiId
      AND numDomainID = v_numDomainId;
   ELSEIF (v_tintMode = 0)
   then --ProductsLastUpdatedDate
        
      UPDATE WebAPIDetail
      SET    vcTenthFldValue = TIMEZONE('UTC',now())
      WHERE  WebApiId = v_WebApiId
      AND numDomainID = v_numDomainId;
   ELSEIF (v_tintMode = 2)
   then --ProductsLastUpdatedDate_ItemAPI
        
--        SELECT * FROM dbo.WebAPI
--        SELECT * FROM WebAPIDetail
      UPDATE WebAPIDetail
      SET    vcEleventhFldValue = TIMEZONE('UTC',now())
      WHERE  WebApiId = v_WebApiId
      AND numDomainID = v_numDomainId;
   ELSEIF (v_tintMode = 3)
   then --InventoryLastUpdatedDate
        
      UPDATE WebAPIDetail
      SET    vcTwelfthFldValue = LOCALTIMESTAMP
      WHERE  WebApiId = v_WebApiId
      AND numDomainID = v_numDomainId;
   ELSEIF (v_tintMode = 4)
   then --SyncProductListingInBiz
        
      UPDATE WebAPIDetail
      SET    vcThirteenthFldValue = TIMEZONE('UTC',now())
      WHERE  WebApiId = v_WebApiId
      AND numDomainID = v_numDomainId;
   ELSEIF (v_tintMode = 5)
   then --Last Updated FBA Order Id list(Only Of Amazon)
        
      UPDATE WebAPIDetail
      SET    vcSixteenthFldValue = LOCALTIMESTAMP
      WHERE  WebApiId = v_WebApiId
      AND numDomainID = v_numDomainId;
   end if;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_UpdateAuthorizativeBizDocsForPurchase]    Script Date: 07/26/2008 16:21:30 ******/



