-- Stored procedure definition script USP_MassSalesFulfillment_UpdateShippingRate for PostgreSQL
CREATE OR REPLACE FUNCTION USP_MassSalesFulfillment_UpdateShippingRate(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_vcSelectedRecords TEXT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numStatus  NUMERIC(18,0) DEFAULT 0;
   v_bitSuccessful  BOOLEAN DEFAULT 1;
   v_hDocItem  INTEGER;
   v_numShippingServiceItemID  NUMERIC(18,0);

   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER;
   v_numOppID  NUMERIC(18,0);
   v_numShipVia  NUMERIC(18,0);
   v_numShipService  NUMERIC(18,0);
   v_vcShipService  VARCHAR(300);
   v_monShipRate  DECIMAL(20,5);
BEGIN
   IF EXISTS(SELECT ID FROM MassSalesFulfillmentConfiguration WHERE numDomainID = v_numDomainID) then
	
      select   coalesce(numOrderStatusPacked1,0) INTO v_numStatus FROM MassSalesFulfillmentConfiguration WHERE numDomainID = v_numDomainID;
   end if;
	
   select   coalesce(numShippingServiceItemID,0) INTO v_numShippingServiceItemID FROM
   Domain WHERE
   numDomainId = v_numDomainID; 

   IF NOT EXISTS(SELECT numItemCode FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = v_numShippingServiceItemID) then
	
      RAISE NOTICE 'SHIPPING_ITEM_NOT_SET';
      RETURN;
   end if;

   BEGIN
      CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numOppID NUMERIC(18,0),
      numShipVia NUMERIC(18,0),
      numShipService NUMERIC(18,0),
      monShipRate DECIMAL(20,5)
   );

   SELECT * INTO v_hDocItem FROM SWF_Xml_PrepareDocument(v_vcSelectedRecords);

   INSERT INTO tt_TEMP(numOppID
		,numShipVia
		,numShipService
		,monShipRate)
   SELECT
   coalesce(OppID,0)
		,coalesce(PreferredShipVia,0)
		,coalesce(PreferredShipService,0)
		,coalesce(ShipRate,0)
   FROM
   SWF_OpenXml(v_hDocItem,'/NewDataSet/Table1','OppID |PreferredShipVia |PreferredShipService |ShipRate') SWA_OpenXml(OppID NUMERIC(18,0),PreferredShipVia NUMERIC(18,0),PreferredShipService NUMERIC(18,0),
   ShipRate DECIMAL(20,5));

   PERFORM SWF_Xml_RemoveDocument(v_hDocItem); 


   SELECT COUNT(*) INTO v_iCount FROM tt_TEMP;

   WHILE v_i <= v_iCount LOOP
      select   numOppID, numShipVia, numShipService, monShipRate INTO v_numOppID,v_numShipVia,v_numShipService,v_monShipRate FROM
      tt_TEMP WHERE
      ID = v_i;
      v_vcShipService := coalesce((SELECT  vcShipmentService FROM ShippingService WHERE (numDomainID = v_numDomainID OR coalesce(numDomainID,0) = 0) AND numShippingServiceID = v_numShipService LIMIT 1),'');
      BEGIN
         -- BEGIN TRANSACTION
IF EXISTS(SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId = v_numOppID AND numItemCode = v_numShippingServiceItemID) then
			
            UPDATE
            OpportunityItems
            SET
            numUnitHour = 1,monPrice = v_monShipRate,monTotAmount = v_monShipRate,monTotAmtBefDiscount = v_monShipRate,
            fltDiscount = 0,vcItemDesc = v_vcShipService
            WHERE
            numOppId = v_numOppID
            AND numItemCode = v_numShippingServiceItemID;
         ELSE
            INSERT INTO OpportunityItems(numOppId
					,numItemCode
					,numUnitHour
					,monPrice
					,monTotAmount
					,monTotAmtBefDiscount
					,bitDiscountType
					,fltDiscount
					,vcItemName
					,vcItemDesc
					,vcSKU
					,vcManufacturer
					,vcModelID)
            SELECT
            v_numOppID
					,v_numShippingServiceItemID
					,1
					,v_monShipRate
					,v_monShipRate
					,v_monShipRate
					,1
					,0
					,vcItemName
					,v_vcShipService
					,vcSKU
					,vcManufacturer
					,vcModelID
            FROM
            Item
            WHERE
            numDomainID = v_numDomainID
            AND numItemCode = v_numShippingServiceItemID;
         end if;
         UPDATE
         OpportunityMaster
         SET
         intUsedShippingCompany = v_numShipVia,numShippingService = v_numShipService,
         numModifiedBy = v_numUserCntID,bintModifiedDate = TIMEZONE('UTC',now()),
         monPAmount =(SELECT SUM(monTotAmount) FROM OpportunityItems WHERE numOppId = v_numOppID),numStatus =(CASE WHEN coalesce(v_numStatus,0) > 0 THEN v_numStatus ELSE numStatus END)
         WHERE
         numDomainId = v_numDomainID
         AND numOppId = v_numOppID;
         -- COMMIT
EXCEPTION WHEN OTHERS THEN
            -- ROLLBACK TRANSACTION
v_bitSuccessful := false;
      END;
      v_i := v_i::bigint+1;
   END LOOP;

   open SWV_RefCur for SELECT v_bitSuccessful;
END; $$;












