CREATE OR REPLACE FUNCTION USP_GetRedirectConfig
(
	v_numDomainID NUMERIC(18,0),
	v_numSiteID NUMERIC(18,0),
	v_numRedirectConfigID NUMERIC(18,0) DEFAULT 0,
	v_CurrentPage INTEGER DEFAULT NULL,                                                            
	v_PageSize INTEGER DEFAULT NULL,
	INOUT v_TotRecs INTEGER  DEFAULT NULL,
	INOUT SWV_RefCur refcursor default null
)
LANGUAGE plpgsql
AS $$
	DECLARE
	v_firstRec INTEGER;                                                            
	v_lastRec INTEGER;                                                            
	v_strSql TEXT;   
	v_strSql1 TEXT;
BEGIN
	v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                                            
	v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                  
  
	v_strSql := 'SELECT
					numRedirectConfigID,
					numDomainId,
					numUserCntId,
					numSiteId,
					vcOldUrl,
					vcNewUrl,
					numRedirectType,
					numReferenceType,
					numReferenceNo,
					dtCreated,
					dtModified 
				FROM 
					RedirectConfig
				WHERE 
					numSiteId = ' || COALESCE(v_numSiteID,0)  || ' 
					AND (numDomainId = ' || COALESCE(v_numDomainID,0)  || ' or ' || COALESCE(v_numDomainID,0) || ' = 0) 
					AND (numRedirectConfigID =  ' || COALESCE(v_numRedirectConfigID,0)  || ' or ' || COALESCE(v_numRedirectConfigID,0) || ' = 0) 
					AND bitIsActive = true';

	DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
	Create TEMPORARY TABLE tt_TEMPTABLE 
	(
		numRedirectConfigID NUMERIC(18,0),
		numDomainId NUMERIC(18,0),
		numUserCntID NUMERIC(18,0),
		numSiteId  NUMERIC(18,0),
		vcOldUrl VARCHAR(500),
		vcNewUrl VARCHAR(500),
		numRedirectType NUMERIC(18,0),
		numReferenceType NUMERIC(18,0),
		numReferenceNo NUMERIC(18,0),
		dtCreated TIMESTAMP,
		dtModified TIMESTAMP
	); 

	EXECUTE 'INSERT into tt_TEMPTABLE ' || v_strSql;

	IF v_PageSize > 0 then
		v_strSql1 := 'WITH Paging
			AS(SELECT Row_number() OVER(ORDER BY numRedirectConfigID) AS row,* from tt_TEMPTABLE) select * from Paging where  row >' || COALESCE(v_firstRec,0) || ' and row < ' || COALESCE(v_lastRec,0);
	ELSE
		v_strSql1 := 'SELECT * from tt_TEMPTABLE';
	end if;

	RAISE NOTICE '%',v_strSql1;
	OPEN SWV_RefCur FOR EXECUTE v_strSql1;

	SELECT COUNT(*) INTO v_TotRecs from tt_TEMPTABLE;

	RAISE NOTICE '%',v_TotRecs;
	RETURN;
END; $$;



