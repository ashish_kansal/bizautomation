-- Stored procedure definition script USP_ItemforOptions for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ItemforOptions(v_numDomainId NUMERIC(9,0) DEFAULT null,  
 v_numItemGroupID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_numItemGroupID = 0 then

      open SWV_RefCur for
      Select vcItemName,numItemCode from Item
      where numDomainID = v_numDomainId
      and charItemType <> 'S' and (charItemType = 'A' or(charItemType = 'P' and bitSerialized = false))
      ORDER by vcItemName;
   ELSEIF v_numItemGroupID > 0
   then

      open SWV_RefCur for
      Select vcItemName,numItemCode,monListPrice,txtItemDesc from Item
      where numDomainID = v_numDomainId
      and charItemType <> 'S' and (charItemType = 'A' or(charItemType = 'P' and bitSerialized = false))
      and numItemCode in(select numCusFlDItemID from  ItemGroupsDTL GDTL join ItemOppAccAttrDTL ODTL on GDTL.numOppAccAttrID = ODTL.numOptAccAttrID
         where GDTL.numItemGroupID = v_numItemGroupID and GDTL.tintType = 1)
      ORDER by vcItemName;
   end if;
   RETURN;
END; $$;


