-- Stored procedure definition script USP_UpdateCRMTypeToAccounts for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateCRMTypeToAccounts(v_numDivisionID NUMERIC(18,0),
      v_numDomainID NUMERIC(18,0),
      v_numUserCntID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
/* Biz usually automatically promotes the record from Lead/Prospect to Account When add a bill, create credit memo,  against it.*/  
-- Promote Lead to Account when Sales/Purchase Order is created against it
   AS $$
   DECLARE
   v_tintCRMType  SMALLINT;
BEGIN
   select   coalesce(tintCRMType,0) INTO v_tintCRMType FROM    DivisionMaster WHERE   numDomainID = v_numDomainID
   AND numDivisionID = v_numDivisionID;
		
		-- Whether given company type are leads,prospects, update & promote them as Accounts
          
   IF v_tintCRMType = 0 then
            
      UPDATE  DivisionMaster
      SET     tintCRMType = 2,bintLeadProm = TIMEZONE('UTC',now()),bintLeadPromBy = v_numUserCntID,
      bintProsProm = TIMEZONE('UTC',now()),bintProsPromBy = v_numUserCntID
      WHERE   numDivisionID = v_numDivisionID;
		
		--Promote Prospect to Account
   ELSEIF v_tintCRMType = 1
   then
                
      UPDATE  DivisionMaster
      SET     tintCRMType = 2,bintProsProm = TIMEZONE('UTC',now()),bintProsPromBy = v_numUserCntID
      WHERE   numDivisionID = v_numDivisionID;
   end if;
   RETURN;
END; $$;        
/****** Object:  StoredProcedure [dbo].[USP_UpdatedomainDetails]    Script Date: 05/07/2009 21:56:59 ******/



