DROP FUNCTION IF EXISTS USP_ManagePriceBookRules;

CREATE OR REPLACE FUNCTION USP_ManagePriceBookRules(v_numPricRuleID NUMERIC(9,0) DEFAULT 0,
    v_vcRuleName VARCHAR(100) DEFAULT NULL,
    v_vcRuleDescription VARCHAR(100) DEFAULT NULL,
    v_tintRuleType SMALLINT DEFAULT NULL,
    v_decDiscount DECIMAL(18,2) DEFAULT NULL,
    v_intQntyItems INTEGER DEFAULT NULL,
    v_decMaxDedPerAmt DECIMAL(18,2) DEFAULT NULL,
    v_numDomainID NUMERIC(9,0) DEFAULT 0,
    v_tintPricingMethod SMALLINT DEFAULT NULL,
    v_tintDiscountType SMALLINT DEFAULT NULL,
    v_tintStep2 SMALLINT DEFAULT NULL,
    v_tintStep3 SMALLINT DEFAULT NULL,
    v_tintRuleFor SMALLINT DEFAULT NULL, 
	v_bitRoundTo BOOLEAN DEFAULT FALSE,
	v_tintRoundTo SMALLINT DEFAULT NULL,
	v_tintVendorCostType SMALLINT DEFAULT NULL,
	INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strDiscount DECIMAL(20,5);
   v_isItemDuplicate  BOOLEAN DEFAULT 0;
   v_isOrganizationDupicate  BOOLEAN DEFAULT 0;
BEGIN
   IF (v_tintStep2 = 3 and v_tintStep3 = 3) then
	
      IF(SELECT COUNT(*) FROM PriceBookRules WHERE tintStep2 = v_tintStep2 AND tintStep3 = v_tintStep3 AND numPricRuleID <> v_numPricRuleID AND numDomainID = v_numDomainID AND tintRuleFor = v_tintRuleFor) > 0 then
		
         RAISE EXCEPTION 'DUPLICATE';
         RETURN;
      end if;
   ELSEIF v_numPricRuleID > 0
   then
      IF EXISTS(SELECT
      PriceBookRules.numPricRuleID
      FROM
      PriceBookRuleItems
      INNER JOIN
      PriceBookRules
      ON
      PriceBookRuleItems.numRuleID = PriceBookRules.numPricRuleID
      INNER JOIN
      PriceBookRuleItems t1
      ON
      t1.numRuleID = v_numPricRuleID
      AND PriceBookRuleItems.tintType = t1.tintType
      AND t1.numValue = PriceBookRuleItems.numValue
      WHERE
      PriceBookRules.numDomainID = v_numDomainID
      AND PriceBookRules.numPricRuleID <> v_numPricRuleID
      AND PriceBookRules.tintStep2 = v_tintStep2
      AND PriceBookRules.tintStep3 = v_tintStep3
      AND PriceBookRules.tintRuleFor = v_tintRuleFor) OR (v_tintStep2 = 3 AND(SELECT COUNT(*) FROM PriceBookRules WHERE tintStep2 = v_tintStep2 AND numPricRuleID <> v_numPricRuleID AND numDomainID = v_numDomainID AND tintRuleFor = v_tintRuleFor) > 0) then
		
         v_isItemDuplicate := true;
      end if;
      IF EXISTS(SELECT
      PriceBookRules.numPricRuleID
      FROM
      PriceBookRuleDTL
      INNER JOIN
      PriceBookRules
      ON
      PriceBookRuleDTL.numRuleID = PriceBookRules.numPricRuleID
      INNER JOIN
      PriceBookRuleDTL t1
      ON
      t1.numRuleID = v_numPricRuleID
      AND PriceBookRuleDTL.tintType = t1.tintType
      AND t1.numValue = PriceBookRuleDTL.numValue
      AND coalesce(t1.numProfile,0) = coalesce(PriceBookRuleDTL.numProfile,0)
      WHERE
      numDomainID = v_numDomainID
      AND numPricRuleID <> v_numPricRuleID
      AND tintStep2 = v_tintStep2
      AND tintStep3 = v_tintStep3
      AND tintRuleFor = v_tintRuleFor) OR (v_tintStep3 = 3 AND(SELECT COUNT(*) FROM PriceBookRules WHERE tintStep3 = v_tintStep3 AND numPricRuleID <> v_numPricRuleID AND numDomainID = v_numDomainID AND tintRuleFor = v_tintRuleFor) > 0) then
		
         v_isOrganizationDupicate := true;
      end if;
      IF v_isOrganizationDupicate = true AND v_isItemDuplicate = true then
		
         RAISE EXCEPTION 'DUPLICATE';
         RETURN;
      end if;
   end if;


   IF v_tintDiscountType = 1 then
      v_strDiscount := CAST(CAST(v_decDiscount AS DECIMAL(9,2)) AS VARCHAR(10));
   ELSE
      v_strDiscount := CAST(CAST(v_decDiscount AS INTEGER) AS VARCHAR(10));
   end if;
 
	IF v_numPricRuleID = 0 then
        
		INSERT  INTO PriceBookRules
		(
			vcRuleName,
			vcRuleDescription,
			tintRuleType,
			decDiscount,
			intQntyItems,
			decMaxDedPerAmt,
			numDomainID,
			tintPricingMethod,
			tintDiscountType,
			tintStep2,
			tintStep3,
			tintRuleFor,
			bitRoundTo,
			tintRoundTo,
			tintVendorCostType
		)
        VALUES
		(
			v_vcRuleName,
			v_vcRuleDescription,
			v_tintRuleType,
			v_strDiscount,
			v_intQntyItems,
			v_decMaxDedPerAmt,
			v_numDomainID,
			v_tintPricingMethod,
			v_tintDiscountType,
			v_tintStep2,
			v_tintStep3,
			v_tintRuleFor,
			v_bitRoundTo,
			v_tintRoundTo,
			v_tintVendorCostType
		) 
		RETURNING 
			numPricRuleID 
		INTO 
			v_numPricRuleID;
            
		open SWV_RefCur for SELECT  v_numPricRuleID AS numPricRuleID;
	ELSE
		UPDATE 
			PriceBookRules
		SET 
			vcRuleName = v_vcRuleName
			,vcRuleDescription = v_vcRuleDescription
			,tintRuleType = v_tintRuleType
			,decDiscount = v_strDiscount
			,intQntyItems = v_intQntyItems
			,decMaxDedPerAmt = v_decMaxDedPerAmt
			,tintPricingMethod = v_tintPricingMethod
			,tintDiscountType = v_tintDiscountType
			,tintStep2 = v_tintStep2
			,tintStep3 = v_tintStep3
			,tintRuleFor = v_tintRuleFor
			,bitRoundTo = v_bitRoundTo
			,tintRoundTo = v_tintRoundTo
			,tintVendorCostType = v_tintVendorCostType
		WHERE 
			numPricRuleID = v_numPricRuleID;
      
		open SWV_RefCur for SELECT  v_numPricRuleID;
	end if;
END; $$;


