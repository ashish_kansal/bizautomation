-- Stored procedure definition script USP_DeleteOrderRuleDetail for PostgreSQL
CREATE OR REPLACE FUNCTION USP_DeleteOrderRuleDetail(v_numRuleDetailsId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM OrderAutoRuleDetails WHERE numRuleDetailsId = v_numRuleDetailsId;
   open SWV_RefCur for SELECT 1;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[usp_DeletePageElement]    Script Date: 07/26/2008 16:15:38 ******/













