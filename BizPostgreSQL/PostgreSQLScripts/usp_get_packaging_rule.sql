CREATE OR REPLACE FUNCTION USP_GET_PACKAGING_RULE
(
	v_numPackagingRuleID NUMERIC(18,0),
	v_numDomainID NUMERIC(18,0),
	INOUT SWV_RefCur refcursor
)
LANGUAGE plpgsql
AS $$
	DECLARE v_strSQL TEXT;
BEGIN
	v_strSQL := 'SELECT DISTINCT  
					numPackagingRuleID,
					vcRuleName,
					numPackagingTypeID AS numPackageTypeID,
					numFromQty,
					numShipClassId,
					vcData AS ShipClass,
					PR.numDomainID,
					CP.vcPackageName
				FROM
					PackagingRules PR
				LEFT JOIN ListDetails LD ON PR.numShipClassId = LD.numListItemID
				LEFT JOIN CustomPackages CP ON CP.numCustomPackageID = PR.numPackagingTypeID
				WHERE PR.numDomainID = ' || v_numDomainID || ' AND (numPackagingRuleID = ' || v_numPackagingRuleID || ' OR ' || coalesce(v_numPackagingRuleID,0) || ' = 0)';

	OPEN SWV_RefCur FOR EXECUTE v_strSQL;
	RETURN;
END; $$;



