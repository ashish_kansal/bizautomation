-- Function definition script GetPromotionOfferItemList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetPromotionOfferItemList(v_numProId NUMERIC,
      v_tintMode SMALLINT,
      v_tintType SMALLINT,
      v_tintRecordType SMALLINT--1=Promotion,2=ShippingRule
    )
RETURNS VARCHAR(2000) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_ItemList  VARCHAR(2000);
BEGIN
   IF v_tintType = 1 AND v_tintRecordType = 1 then

      IF v_tintMode = 1 then
        
         select string_agg(vcItemName,', ') INTO v_ItemList FROM    PromotionOfferItems POI
         INNER JOIN Item I ON I.numItemCode = POI.numValue
         LEFT OUTER JOIN PromotionOffer PO ON PO.numProId = POI.numProId WHERE   PO.tintAppliesTo = 1 AND PO.numProId = v_numProId AND POI.tintType = 1
         AND tintRecordType = v_tintRecordType    LIMIT 10;
      end if;
      IF v_tintMode = 2 then
        
         select string_agg(vcCategoryName,', ') INTO v_ItemList FROM    PromotionOfferItems POI
         INNER JOIN Category SC ON SC.numCategoryID = POI.numValue
         LEFT OUTER JOIN PromotionOffer PO ON PO.numProId = POI.numProId WHERE  PO.tintAppliesTo = 2 AND PO.numProId = v_numProId  AND POI.tintType = 2
         AND tintRecordType = v_tintRecordType    LIMIT 10;
      end if;
   ELSEIF v_tintType = 2 AND v_tintRecordType = 1
   then
	  
      IF v_tintMode = 1 then
        
         select string_agg(C.vcCompanyName,', ') INTO v_ItemList FROM    PromotionOfferContacts POC
         INNER JOIN DivisionMaster DM ON DM.numDivisionID = POC.numValue
         LEFT OUTER JOIN CompanyInfo C ON C.numCompanyId = DM.numCompanyID
         LEFT OUTER JOIN PromotionOffer PO ON PO.numProId = POC.numProId WHERE  PO.tintContactsType = 1 AND PO.numProId = v_numProId  AND POC.tintType = 1
         AND tintRecordType = v_tintRecordType    LIMIT 10;
      end if;
      IF v_tintMode = 2 then
        
         select string_agg((LI.vcData || '/' || LI1.vcData),', ') INTO v_ItemList FROM    PromotionOfferContacts POC
         INNER JOIN Listdetails LI ON LI.numListItemID = POC.numValue
         Inner JOIN Listdetails LI1 ON LI1.numListItemID = POC.numProfile
         LEFT OUTER JOIN PromotionOffer PO ON PO.numProId = POC.numProId WHERE  PO.tintContactsType = 2 AND PO.numProId = v_numProId  AND POC.tintType = 2
         AND tintRecordType = v_tintRecordType    LIMIT 10;
      end if;
   end if; 
       
--For shipping rules
--IF @tintType=1 AND @tintRecordType =2
--BEGIN
--    IF @tintMode = 1 
--        BEGIN
--
--            SELECT TOP 10
--                    @ItemList = COALESCE(@ItemList + ', ', '') + vcItemName
--            FROM    [PromotionOfferItems] POI
--                    INNER JOIN Item I ON I.[numItemCode] = POI.[numValue]
--                    LEFT OUTER JOIN [dbo].[ShippingRules] SR ON SR.[numRuleID] = POI.[numProId]
--            WHERE   --SR.tintAppliesTo = 1 AND 
--                    SR.[numRuleID] = @numProId AND POI.tintType=1
--					AND tintRecordType=@tintRecordType
--        END
--      
--      IF @tintMode = 2
--        BEGIN
--
--            SELECT TOP 10
--                    @ItemList = COALESCE(@ItemList + ', ', '') + vcCategoryName
--            FROM    [PromotionOfferItems] POI
--                    INNER JOIN [Category] SC ON SC.[numCategoryID] = POI.[numValue]
--                     LEFT OUTER JOIN [ShippingRules] SR ON SR.[numRuleID] = POI.[numProId]
--            WHERE   --SR.tintAppliesTo = 2 AND 
--                    SR.[numRuleID] = @numProId  AND POI.tintType=2
--					AND tintRecordType=@tintRecordType
--        END
--  END
--        
--     ELSE IF @tintType=2 AND @tintRecordType =2
--	  BEGIN   
--        IF @tintMode = 1
--        BEGIN
--
--            SELECT TOP 10
--                    @ItemList = COALESCE(@ItemList + ', ', '') + C.[vcCompanyName]
--            FROM    [PromotionOfferContacts] POC
--                    INNER JOIN [DivisionMaster] DM ON DM.[numDivisionID] = POC.[numValue]
--                    LEFT OUTER JOIN [CompanyInfo] C ON C.[numCompanyId] = DM.[numCompanyID]
--                    LEFT OUTER JOIN [ShippingRules] SR ON SR.[numRuleID] = POC.[numProId]
--            WHERE   --SR.tintContactsType = 1 AND 
--                    SR.[numRuleID] = @numProId  AND POC.tintType=1
--					AND tintRecordType=@tintRecordType
--        END
--        
--        IF @tintMode = 2
--        BEGIN
--
--             SELECT TOP 10
--                    @ItemList = COALESCE(@ItemList + ', ', '') + (LI.vcData +'/' + LI1.vcData)
--            FROM    [PromotionOfferContacts] POC
--                    INNER JOIN [ListDetails] LI ON LI.[numListItemID] = POC.[numValue]
--                    Inner JOIN [ListDetails] LI1 ON LI1.[numListItemID] = POC.[numProfile]
--                    LEFT OUTER JOIN [ShippingRules] SR ON SR.[numRuleID] = POC.[numProId]
--            WHERE   --SR.tintContactsType = 2 AND 
--                    SR.[numRuleID] = @numProId  AND POC.tintType=2
--					AND tintRecordType=@tintRecordType
--        END
--       END 
        
   RETURN coalesce(v_ItemList,'');
END; $$;

