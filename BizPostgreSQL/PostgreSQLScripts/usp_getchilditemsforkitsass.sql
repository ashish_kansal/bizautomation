-- Stored procedure definition script USP_GetChildItemsForKitsAss for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetChildItemsForKitsAss(v_numKitId NUMERIC(18,0)                            
	,v_numWarehouseItemID NUMERIC(18,0)
	,v_bitApplyFilter BOOLEAN DEFAULT false, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numWarehouseID  NUMERIC(18,0);
BEGIN
   DROP TABLE IF EXISTS tt_TEMPGetChildItemsForKitsAss CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPGetChildItemsForKitsAss AS
   WITH RECURSIVE CTE
	(
		ID
		,numItemDetailID
		,numParentID
		,numItemKitID
		,numItemCode
		,vcItemName
		,vcSKU
		,bitKitParent
		,monAverageCost
		,numAssetChartAcntId
		,txtItemDesc
		,numQtyItemsReq
		,numOppChildItemID
		,charItemType
		,ItemType
		,StageLevel
		,monListPriceC
		,numBaseUnit
		,numCalculatedQty
		,numIDUOMId,sintOrder
		,numRowNumber
		,RStageLevel
		,numUOMQuantity
		,tintView
		,vcFields
		,bitUseInDynamicSKU
		,bitOrderEditable
		,bitUseInNewItemConfiguration
	)
	AS
	(
		SELECT 
			CAST(CONCAT('#0#',COALESCE(Dtl.numItemDetailID,'0'),'#') AS VARCHAR(1000))
			,Dtl.numItemDetailID
			,CAST('' AS VARCHAR(1000))
			,CAST(0 AS NUMERIC)
			,numItemCode
			,CONCAT(vcItemName,(CASE WHEN LENGTH(COALESCE(vcSKU,'')) > 0 THEN CONCAT(' (',vcSKU,')') ELSE '' END)) vcItemName
			,vcSKU
			,COALESCE(Item.bitKitParent,false)
			,(CASE WHEN COALESCE(bitVirtualInventory,false) = true THEN 0 ELSE monAverageCost END)
			,numAssetChartAcntId
			,COALESCE(Dtl.vcItemDesc,txtItemDesc) AS txtItemDesc
			,DTL.numQtyItemsReq
			,0 as numOppChildItemID
			,case when charItemType='P' then CONCAT('Inventory',CASE WHEN COALESCE(item.bitKitParent,false)=true THEN ' (Kit)' WHEN COALESCE(item.bitAssembly,false)=true THEN ' (Assembly)' WHEN COALESCE(item.numItemGroup,0) > 0 AND (COALESCE(item.bitMatrix,false)=true OR EXISTS (SELECT numItemGroupDTL FROM ItemGroupsDTL WHERE numItemGroupID=item.numItemGroup AND tintType=2)) THEN ' (Matrix)' ELSE '' END) when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory' end as charItemType
			,charItemType as ItemType                            
			,1
			,Item.monListPrice
			,COALESCE(numBaseUnit,0)
			,CAST(DTL.numQtyItemsReq AS DOUBLE PRECISION) AS numCalculatedQty
			,COALESCE(Dtl.numUOMId,COALESCE(numBaseUnit,0)) AS numIDUOMId
			,COALESCE(sintOrder,0) sintOrder
			,ROW_NUMBER() OVER (ORDER BY COALESCE(sintOrder,0)) AS numRowNumber
			,0
			,(DTL.numQtyItemsReq * COALESCE(fn_UOMConversion(DTL.numUOMID,item.numItemCode,item.numDomainID,item.numBaseUnit),1))
			,COALESCE(Dtl.tintView,1) tintView
			,COALESCE(Dtl.vcFields,'') vcFields
			,COALESCE(Dtl.bitUseInDynamicSKU,false) bitUseInDynamicSKU
			,COALESCE(Dtl.bitOrderEditable,false) bitOrderEditable
			,COALESCE(Dtl.bitUseInNewItemConfiguration,false) bitUseInNewItemConfiguration
		FROM 
			Item                                
		INNER JOIN 
			ItemDetails Dtl 
		ON 
			numChildItemID=numItemCode
		WHERE 
			numItemKitID=v_numKitId 
		UNION ALL
		SELECT 
			CAST(CONCAT(c.ID,'-#',(c.StageLevel + 1),'#',COALESCE(Dtl.numItemDetailID,'0'),'#') AS VARCHAR(1000))
			,Dtl.numItemDetailID
			,C.ID
			,Dtl.numItemKitID
			,i.numItemCode
			,CONCAT(i.vcItemName,(CASE WHEN LENGTH(COALESCE(i.vcSKU,'')) > 0 THEN CONCAT(' (',i.vcSKU,')') ELSE '' END)) vcItemName
			,i.vcSKU
			,COALESCE(i.bitKitParent,false)
			,(CASE WHEN COALESCE(i.bitVirtualInventory,false) = true THEN 0 ELSE i.monAverageCost END)
			,i.numAssetChartAcntId
			,COALESCE(Dtl.vcItemDesc,i.txtItemDesc) AS txtItemDesc
			,DTL.numQtyItemsReq
			,0 as numOppChildItemID
			,CASE WHEN i.charItemType='P' THEN CONCAT('Inventory',CASE WHEN COALESCE(i.bitKitParent,false) = true THEN ' (Kit)' WHEN COALESCE(i.bitAssembly,false) = true THEN ' (Assembly)' WHEN COALESCE(i.numItemGroup,0) > 0 AND (COALESCE(i.bitMatrix,false) = true OR EXISTS (SELECT numItemGroupDTL FROM ItemGroupsDTL WHERE numItemGroupID=i.numItemGroup AND tintType=2)) THEN ' (Matrix)' ELSE '' END) when i.charItemType='S' then 'Service' when i.charItemType='A' then 'Accessory' when i.charItemType='N' then 'Non-Inventory' end as charItemType
			,i.charItemType as ItemType                            
			,c.StageLevel + 1
			,i.monListPrice,COALESCE(i.numBaseUnit,0),CAST((DTL.numQtyItemsReq * c.numCalculatedQty) AS FLOAT) AS numCalculatedQty
			,COALESCE(Dtl.numUOMId,COALESCE(i.numBaseUnit,0)) AS numIDUOMId
			,COALESCE(Dtl.sintOrder,0) sintOrder
			,ROW_NUMBER() OVER (ORDER BY COALESCE(Dtl.sintOrder,0)) AS numRowNumber
			,0
			,(DTL.numQtyItemsReq * COALESCE(fn_UOMConversion(DTL.numUOMID,i.numItemCode,i.numDomainID,i.numBaseUnit),1))
			,COALESCE(Dtl.tintView,1) tintView
			,COALESCE(Dtl.vcFields,'') vcFields
			,COALESCE(Dtl.bitUseInDynamicSKU,false) bitUseInDynamicSKU
			,COALESCE(Dtl.bitOrderEditable,false) bitOrderEditable
			,COALESCE(Dtl.bitUseInNewItemConfiguration,false) bitUseInNewItemConfiguration
		FROM
			Item i                               
		INNER JOIN 
			ItemDetails Dtl
		ON 
			Dtl.numChildItemID=i.numItemCode
		INNER JOIN 
			CTE c 
		ON 
			Dtl.numItemKitID = c.numItemCode 
		WHERE 
			Dtl.numChildItemID!=v_numKitId
	)   SELECT * FROM CTE;

   WITH RECURSIVE Final AS(SELECT *
			,CAST(1 AS INTEGER) AS RStageLevel1
   FROM
   tt_TEMPGetChildItemsForKitsAss
   WHERE
   numItemCode NOT IN(SELECT numItemKitID FROM tt_TEMPGetChildItemsForKitsAss)
   UNION ALL
   SELECT
   t.*
			,c.RStageLevel1+1 AS RStageLevel1
   FROM
   tt_TEMPGetChildItemsForKitsAss t
   JOIN
   Final c
   ON
   t.numItemCode = c.numItemKitID)
   UPDATE
   tt_TEMPGetChildItemsForKitsAss t
   SET
   RStageLevel = f.RStageLevel
   FROM(SELECT numitemcode,numItemKitID,MAX(RStageLevel1) AS RStageLevel FROM Final GROUP BY numitemcode,numItemKitID) f
   WHERE
   t.numItemCode = f.numitemcode
   AND t.numItemKitID = f.numItemKitID; 


   IF coalesce(v_numWarehouseItemID,0) > 0 then
      select   numWareHouseID INTO v_numWarehouseID FROM WareHouseItems WHERE numWareHouseItemID = v_numWarehouseItemID;
      IF(v_bitApplyFilter = false) then
		
         IF EXISTS(SELECT
         c.numItemCode
         FROM
         tt_TEMPGetChildItemsForKitsAss c
         WHERE
         c.ItemType = 'P'
         AND(SELECT COUNT(*) FROM WareHouseItems WHERE numItemID = c.numItemCode AND numWareHouseID = v_numWarehouseID) = 0) then
		
            RAISE EXCEPTION 'SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS';
            RETURN;
         end if;
      end if;

	  IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema like 'pg_temp_%' AND table_name=LOWER('tt_TEMPFINALGetChildItemsForKitsAss')) THEN
			DELETE FROM tt_TEMPFINALGetChildItemsForKitsAss;

			INSERT INTO tt_TEMPFINALGetChildItemsForKitsAss
			SELECT DISTINCT
         c.*
			,CASE WHEN coalesce(WI.numWareHouseItemID,0) > 0 THEN coalesce(WI.monWListPrice,0) ELSE c.monListPriceC END AS monListPrice
			,CAST(CASE WHEN coalesce(WI.numWareHouseItemID,0) > 0 THEN coalesce(WI.monWListPrice,0) ELSE c.monListPriceC END AS VARCHAR(30)) || '/' || coalesce(UOM.vcUnitName,'Units') as UnitPrice
			,UOM.vcUnitName
			,coalesce(UOM.numUOMId,0) AS numUOMId
			,WI.numItemID,vcWareHouse
			,IDUOM.vcUnitName AS vcIDUnitName
			,fn_UOMConversion(IDUOM.numUOMId,0::NUMERIC,IDUOM.numDomainID,UOM.numUOMId) AS UOMConversionFactor
			,CAST(c.numQtyItemsReq*fn_UOMConversion(UOM.numUOMId,0::NUMERIC,IDUOM.numDomainID,IDUOM.numUOMId) AS DOUBLE PRECISION) AS numConQty
			,(c.numQtyItemsReq*fn_UOMConversion(UOM.numUOMId,0::NUMERIC,IDUOM.numDomainID,IDUOM.numUOMId))*c.monAverageCost AS monAverageCostTotalForQtyRequired
			,coalesce(WI.numWareHouseItemID,0) AS numWarehouseItmsID
			,coalesce(WI.numWareHouseItemID,0) AS numWareHouseItemID
			,coalesce(numOnHand,0) AS numOnHand
			,coalesce(numOnHand,0)+coalesce(numAllocation,0) AS numAvailable
			,coalesce(numOnOrder,0) AS numOnOrder
			,coalesce(numReorder,0) AS numReorder
			,coalesce(numAllocation,0) AS numAllocation
			,coalesce(numBackOrder,0) AS numBackOrder
		
         FROM
         tt_TEMPGetChildItemsForKitsAss c
         LEFT JOIN
         UOM
         ON
         UOM.numUOMId = c.numBaseUnit
         LEFT JOIN
         UOM IDUOM
         ON
         IDUOM.numUOMId = c.numIDUOMId
         LEFT JOIN LATERAL(SELECT 
         numWareHouseItemID
				,numOnHand
				,numOnOrder
				,numReorder
				,numAllocation
				,numBackOrder
				,numItemID
				,vcWareHouse
				,monWListPrice
         FROM
         WareHouseItems
         LEFT JOIN
         Warehouses W
         ON
         W.numWareHouseID = WareHouseItems.numWareHouseID
         WHERE
         WareHouseItems.numItemID = c.numItemCode
         AND WareHouseItems.numWareHouseID = v_numWarehouseID
         ORDER BY
         WareHouseItems.numWareHouseID,numWareHouseItemID LIMIT 1) WI on TRUE
         ORDER BY
         c.StageLevel;
	ELSE
		CREATE TEMPORARY TABLE IF NOT EXISTS tt_TEMPFINALGetChildItemsForKitsAss ON COMMIT DROP AS
         SELECT DISTINCT
         c.*
			,CASE WHEN coalesce(WI.numWareHouseItemID,0) > 0 THEN coalesce(WI.monWListPrice,0) ELSE c.monListPriceC END AS monListPrice
			,CAST(CASE WHEN coalesce(WI.numWareHouseItemID,0) > 0 THEN coalesce(WI.monWListPrice,0) ELSE c.monListPriceC END AS VARCHAR(30)) || '/' || coalesce(UOM.vcUnitName,'Units') as UnitPrice
			,UOM.vcUnitName
			,coalesce(UOM.numUOMId,0) AS numUOMId
			,WI.numItemID,vcWareHouse
			,IDUOM.vcUnitName AS vcIDUnitName
			,fn_UOMConversion(IDUOM.numUOMId,0::NUMERIC,IDUOM.numDomainID,UOM.numUOMId) AS UOMConversionFactor
			,CAST(c.numQtyItemsReq*fn_UOMConversion(UOM.numUOMId,0::NUMERIC,IDUOM.numDomainID,IDUOM.numUOMId) AS DOUBLE PRECISION) AS numConQty
			,(c.numQtyItemsReq*fn_UOMConversion(UOM.numUOMId,0::NUMERIC,IDUOM.numDomainID,IDUOM.numUOMId))*c.monAverageCost AS monAverageCostTotalForQtyRequired
			,coalesce(WI.numWareHouseItemID,0) AS numWarehouseItmsID
			,coalesce(WI.numWareHouseItemID,0) AS numWareHouseItemID
			,coalesce(numOnHand,0) AS numOnHand
			,coalesce(numOnHand,0)+coalesce(numAllocation,0) AS numAvailable
			,coalesce(numOnOrder,0) AS numOnOrder
			,coalesce(numReorder,0) AS numReorder
			,coalesce(numAllocation,0) AS numAllocation
			,coalesce(numBackOrder,0) AS numBackOrder
		
         FROM
         tt_TEMPGetChildItemsForKitsAss c
         LEFT JOIN
         UOM
         ON
         UOM.numUOMId = c.numBaseUnit
         LEFT JOIN
         UOM IDUOM
         ON
         IDUOM.numUOMId = c.numIDUOMId
         LEFT JOIN LATERAL(SELECT 
         numWareHouseItemID
				,numOnHand
				,numOnOrder
				,numReorder
				,numAllocation
				,numBackOrder
				,numItemID
				,vcWareHouse
				,monWListPrice
         FROM
         WareHouseItems
         LEFT JOIN
         Warehouses W
         ON
         W.numWareHouseID = WareHouseItems.numWareHouseID
         WHERE
         WareHouseItems.numItemID = c.numItemCode
         AND WareHouseItems.numWareHouseID = v_numWarehouseID
         ORDER BY
         WareHouseItems.numWareHouseID,numWareHouseItemID LIMIT 1) WI on TRUE
         ORDER BY
         c.StageLevel;
			
	  END IF;
      
      
      UPDATE
      tt_TEMPFINALGetChildItemsForKitsAss T
      SET
      numOnHand = coalesce((SELECT MIN(FLOOR(TF.numOnHand/TF.numQtyItemsReq:: NUMERIC)) FROM tt_TEMPFINALGetChildItemsForKitsAss TF WHERE TF.numParentID = T.ID),0),numAvailable = coalesce((SELECT MIN(FLOOR(TF.numAvailable/TF.numQtyItemsReq:: NUMERIC)) FROM tt_TEMPFINALGetChildItemsForKitsAss TF WHERE TF.numParentID = T.ID),0),numBackOrder = 0,
      numOnOrder = 0
		
      WHERE
      coalesce(T.bitKitParent,false) = true;
      open SWV_RefCur for
      SELECT 
		ID AS "ID"
		,numItemDetailID AS "numItemDetailID"
		,numParentID AS "numParentID"
		,numItemKitID AS "numItemKitID"
		,numItemCode AS "numItemCode"
		,vcItemName AS "vcItemName"
		,vcSKU AS "vcSKU"
		,bitKitParent AS "bitKitParent"
		,monAverageCost AS "monAverageCost"
		,numAssetChartAcntId AS "numAssetChartAcntId"
		,txtItemDesc AS "txtItemDesc"
		,numQtyItemsReq AS "numQtyItemsReq"
		,numOppChildItemID AS "numOppChildItemID"
		,charItemType AS "charItemType"
		,ItemType AS "ItemType"
		,StageLevel AS "StageLevel"
		,monListPriceC AS "monListPriceC"
		,numBaseUnit AS "numBaseUnit"
		,numCalculatedQty AS "numCalculatedQty"
		,numIDUOMId AS "numIDUOMId"
		,sintOrder AS "sintOrder"
		,numRowNumber AS "numRowNumber"
		,RStageLevel AS "RStageLevel"
		,numUOMQuantity AS "numUOMQuantity"
		,tintView AS "tintView"
		,vcFields AS "vcFields"
		,bitUseInDynamicSKU AS "bitUseInDynamicSKU"
		,bitOrderEditable AS "bitOrderEditable"
		,bitUseInNewItemConfiguration AS "bitUseInNewItemConfiguration"
		,monListPrice AS "monListPrice"
		,UnitPrice as "UnitPrice"
		,vcUnitName AS "vcUnitName"
		,numUOMId AS "numUOMId"
		,numItemID AS "numItemID"
		,vcWareHouse AS "vcWareHouse"
		,vcIDUnitName AS "vcIDUnitName"
		,UOMConversionFactor AS "UOMConversionFactor"
		,numConQty AS "numConQty"
		,monAverageCostTotalForQtyRequired AS "monAverageCostTotalForQtyRequired"
		,numWarehouseItmsID AS "numWarehouseItmsID"
		,numWareHouseItemID AS "numWareHouseItemID"
		,numOnHand AS "numOnHand"
		,numAvailable AS "numAvailable"
		,numOnOrder AS "numOnOrder"
		,numReorder AS "numReorder"
		,numAllocation AS "numAllocation"
		,numBackOrder AS "numBackOrder"
	  FROM tt_TEMPFINALGetChildItemsForKitsAss;
		
   ELSEIF v_bitApplyFilter = true
   then
		IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema like 'pg_temp_%' AND table_name=LOWER('tt_TEMPFINAL1GetChildItemsForKitsAss')) THEN
			DELETE FROM tt_TEMPFINAL1GetChildItemsForKitsAss;
			INSERT INTO tt_TEMPFINAL1GetChildItemsForKitsAss
			SELECT DISTINCT
			 c.*
				,CASE WHEN coalesce(WI.numWareHouseItemID,0) > 0 THEN coalesce(WI.monWListPrice,0) ELSE c.monListPriceC END AS monListPrice
				,CAST(CASE WHEN coalesce(WI.numWareHouseItemID,0) > 0 THEN coalesce(WI.monWListPrice,0) ELSE c.monListPriceC END AS VARCHAR(30)) || '/' || coalesce(UOM.vcUnitName,'Units') as UnitPrice
				,UOM.vcUnitName
				,coalesce(UOM.numUOMId,0) AS numUOMId
				,WI.numItemID,vcWareHouse
				,IDUOM.vcUnitName AS vcIDUnitName
				,fn_UOMConversion(IDUOM.numUOMId,0::NUMERIC,IDUOM.numDomainID,UOM.numUOMId) AS UOMConversionFactor
				,CAST(c.numQtyItemsReq*fn_UOMConversion(UOM.numUOMId,0::NUMERIC,IDUOM.numDomainID,IDUOM.numUOMId) AS DOUBLE PRECISION) AS numConQty
				,(c.numQtyItemsReq*fn_UOMConversion(UOM.numUOMId,0::NUMERIC,IDUOM.numDomainID,IDUOM.numUOMId))*c.monAverageCost AS monAverageCostTotalForQtyRequired
				,coalesce(WI.numWareHouseItemID,0) AS numWarehouseItmsID
				,coalesce(numOnHand,0) AS numOnHand
				,coalesce(numOnHand,0)+coalesce(numAllocation,0) AS numAvailable
				,coalesce(numOnOrder,0) AS numOnOrder
				,coalesce(numReorder,0) AS numReorder
				,coalesce(numAllocation,0) AS numAllocation
				,coalesce(numBackOrder,0) AS numBackOrder
		
			 FROM
			 tt_TEMPGetChildItemsForKitsAss c
			 LEFT JOIN
			 UOM
			 ON
			 UOM.numUOMId = c.numBaseUnit
			 LEFT JOIN
			 UOM IDUOM
			 ON
			 IDUOM.numUOMId = c.numIDUOMId
			 LEFT JOIN LATERAL(SELECT 
			 1 AS numWareHouseItemID
					,SUM(numOnHand) AS numOnHand
					,SUM(numOnOrder) AS numOnOrder
					,0 AS numReorder
					,SUM(numAllocation) AS numAllocation
					,SUM(numBackOrder) AS numBackOrder
					,0 AS numItemID
					,'' AS vcWareHouse
					,0 AS monWListPrice
			 FROM
			 WareHouseItems
			 LEFT JOIN
			 Warehouses W
			 ON
			 W.numWareHouseID = WareHouseItems.numWareHouseID
			 WHERE
			 WareHouseItems.numItemID = c.numItemCode
			 GROUP BY
				WareHouseItems.numItemID) WI on TRUE
			 ORDER BY
			 c.StageLevel;
		ELSE
			CREATE TEMPORARY TABLE IF NOT EXISTS tt_TEMPFINAL1GetChildItemsForKitsAss ON COMMIT DROP AS
			 SELECT DISTINCT
			 c.*
				,CASE WHEN coalesce(WI.numWareHouseItemID,0) > 0 THEN coalesce(WI.monWListPrice,0) ELSE c.monListPriceC END AS monListPrice
				,CAST(CASE WHEN coalesce(WI.numWareHouseItemID,0) > 0 THEN coalesce(WI.monWListPrice,0) ELSE c.monListPriceC END AS VARCHAR(30)) || '/' || coalesce(UOM.vcUnitName,'Units') as UnitPrice
				,UOM.vcUnitName
				,coalesce(UOM.numUOMId,0) AS numUOMId
				,WI.numItemID,vcWareHouse
				,IDUOM.vcUnitName AS vcIDUnitName
				,fn_UOMConversion(IDUOM.numUOMId,0::NUMERIC,IDUOM.numDomainID,UOM.numUOMId) AS UOMConversionFactor
				,CAST(c.numQtyItemsReq*fn_UOMConversion(UOM.numUOMId,0::NUMERIC,IDUOM.numDomainID,IDUOM.numUOMId) AS DOUBLE PRECISION) AS numConQty
				,(c.numQtyItemsReq*fn_UOMConversion(UOM.numUOMId,0::NUMERIC,IDUOM.numDomainID,IDUOM.numUOMId))*c.monAverageCost AS monAverageCostTotalForQtyRequired
				,coalesce(WI.numWareHouseItemID,0) AS numWarehouseItmsID
				,coalesce(WI.numWareHouseItemID,0) AS numWareHouseItemID
				,coalesce(numOnHand,0) AS numOnHand
				,coalesce(numOnHand,0)+coalesce(numAllocation,0) AS numAvailable
				,coalesce(numOnOrder,0) AS numOnOrder
				,coalesce(numReorder,0) AS numReorder
				,coalesce(numAllocation,0) AS numAllocation
				,coalesce(numBackOrder,0) AS numBackOrder
		
			 FROM
			 tt_TEMPGetChildItemsForKitsAss c
			 LEFT JOIN
			 UOM
			 ON
			 UOM.numUOMId = c.numBaseUnit
			 LEFT JOIN
			 UOM IDUOM
			 ON
			 IDUOM.numUOMId = c.numIDUOMId
			 LEFT JOIN LATERAL(SELECT 
			 1 AS numWareHouseItemID
					,SUM(numOnHand) AS numOnHand
					,SUM(numOnOrder) AS numOnOrder
					,0 AS numReorder
					,0 AS numAllocation
					,0 AS numBackOrder
					,0 AS numItemID
					,'' AS vcWareHouse
					,0 AS monWListPrice
			 FROM
			 WareHouseItems
			 LEFT JOIN
			 Warehouses W
			 ON
			 W.numWareHouseID = WareHouseItems.numWareHouseID
			 WHERE
			 WareHouseItems.numItemID = c.numItemCode
			 ORDER BY
			 numWareHouseItemID LIMIT 1) WI on TRUE
			 ORDER BY
			 c.StageLevel;
		END IF;
      
      
      UPDATE
      tt_TEMPFINAL1GetChildItemsForKitsAss T
      SET
      numOnHand = coalesce((SELECT MIN(FLOOR(TF.numOnHand/TF.numQtyItemsReq:: NUMERIC)) FROM tt_TEMPFINAL1GetChildItemsForKitsAss TF WHERE TF.numParentID = T.ID),0),numAvailable = coalesce((SELECT MIN(FLOOR(TF.numAvailable/TF.numQtyItemsReq)) FROM tt_TEMPFINAL1GetChildItemsForKitsAss TF WHERE TF.numParentID = T.ID),0),
      numBackOrder = 0,
      numOnOrder = 0
		
      WHERE
      coalesce(T.bitKitParent,false) = true;
      open SWV_RefCur for
      SELECT ID AS "ID"
		,numItemDetailID AS "numItemDetailID"
		,numParentID AS "numParentID"
		,numItemKitID AS "numItemKitID"
		,numItemCode AS "numItemCode"
		,vcItemName AS "vcItemName"
		,vcSKU AS "vcSKU"
		,bitKitParent AS "bitKitParent"
		,monAverageCost AS "monAverageCost"
		,numAssetChartAcntId AS "numAssetChartAcntId"
		,txtItemDesc AS "txtItemDesc"
		,numQtyItemsReq AS "numQtyItemsReq"
		,numOppChildItemID AS "numOppChildItemID"
		,charItemType AS "charItemType"
		,ItemType AS "ItemType"
		,StageLevel AS "StageLevel"
		,monListPriceC AS "monListPriceC"
		,numBaseUnit AS "numBaseUnit"
		,numCalculatedQty AS "numCalculatedQty"
		,numIDUOMId AS "numIDUOMId"
		,sintOrder AS "sintOrder"
		,numRowNumber AS "numRowNumber"
		,RStageLevel AS "RStageLevel"
		,numUOMQuantity AS "numUOMQuantity"
		,tintView AS "tintView"
		,vcFields AS "vcFields"
		,bitUseInDynamicSKU AS "bitUseInDynamicSKU"
		,bitOrderEditable AS "bitOrderEditable"
		,bitUseInNewItemConfiguration AS "bitUseInNewItemConfiguration"
		,monListPrice AS "monListPrice"
		,UnitPrice as "UnitPrice"
		,vcUnitName AS "vcUnitName"
		,numUOMId AS "numUOMId"
		,numItemID AS "numItemID"
		,vcWareHouse AS "vcWareHouse"
		,vcIDUnitName AS "vcIDUnitName"
		,UOMConversionFactor AS "UOMConversionFactor"
		,numConQty AS "numConQty"
		,monAverageCostTotalForQtyRequired AS "monAverageCostTotalForQtyRequired"
		,numWarehouseItmsID AS "numWarehouseItmsID"
		,numWareHouseItemID AS "numWareHouseItemID"
		,numOnHand AS "numOnHand"
		,numAvailable AS "numAvailable"
		,numOnOrder AS "numOnOrder"
		,numReorder AS "numReorder"
		,numAllocation AS "numAllocation"
		,numBackOrder AS "numBackOrder" FROM tt_TEMPFINAL1GetChildItemsForKitsAss;
   ELSE
      open SWV_RefCur for
      SELECT DISTINCT
     c.ID AS "ID"
		,c.numItemDetailID AS "numItemDetailID"
		,c.numParentID AS "numParentID"
		,c.numItemKitID AS "numItemKitID"
		,c.numItemCode AS "numItemCode"
		,c.vcItemName AS "vcItemName"
		,c.vcSKU AS "vcSKU"
		,c.bitKitParent AS "bitKitParent"
		,c.monAverageCost AS "monAverageCost"
		,c.numAssetChartAcntId AS "numAssetChartAcntId"
		,c.txtItemDesc AS "txtItemDesc"
		,c.numQtyItemsReq AS "numQtyItemsReq"
		,c.numOppChildItemID AS "numOppChildItemID"
		,c.charItemType AS "charItemType"
		,c.ItemType AS "ItemType"
		,c.StageLevel AS "StageLevel"
		,c.monListPriceC AS "monListPriceC"
		,c.numBaseUnit AS "numBaseUnit"
		,c.numCalculatedQty AS "numCalculatedQty"
		,c.numIDUOMId AS "numIDUOMId"
		,c.sintOrder AS "sintOrder"
		,c.numRowNumber AS "numRowNumber"
		,c.RStageLevel AS "RStageLevel"
		,c.numUOMQuantity AS "numUOMQuantity"
		,c.tintView AS "tintView"
		,c.vcFields AS "vcFields"
		,c.bitUseInDynamicSKU AS "bitUseInDynamicSKU"
		,c.bitOrderEditable AS "bitOrderEditable"
		,c.bitUseInNewItemConfiguration AS "bitUseInNewItemConfiguration"
		,UOM.vcUnitName AS "vcUnitName"
		,coalesce(UOM.numUOMId,0) AS "numUOMId"
		,IDUOM.vcUnitName AS "vcIDUnitName"
		,fn_UOMConversion(IDUOM.numUOMId,0::NUMERIC,IDUOM.numDomainID,UOM.numUOMId) AS "UOMConversionFactor"
		,c.numQtyItemsReq*fn_UOMConversion(UOM.numUOMId,0::NUMERIC,IDUOM.numDomainID,IDUOM.numUOMId) AS "numConQty"
		,(c.numQtyItemsReq*fn_UOMConversion(UOM.numUOMId,0::NUMERIC,IDUOM.numDomainID,IDUOM.numUOMId))*c.monAverageCost AS "monAverageCostTotalForQtyRequired"
		,c.sintOrder AS "sintOrder"
		,c.numUOMQuantity AS "numUOMQuantity"
      FROM
      tt_TEMPGetChildItemsForKitsAss c
      LEFT JOIN
      UOM
      ON
      UOM.numUOMId = c.numBaseUnit
      LEFT JOIN
      UOM IDUOM
      ON
      IDUOM.numUOMId = c.numIDUOMId
      ORDER BY
      c.StageLevel;
   end if;
END; $$;


