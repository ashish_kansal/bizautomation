-- Function definition script fn_ParentCategory for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_ParentCategory(v_CategoryId VARCHAR(100),v_numDomainId INTEGER)
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_parentId  VARCHAR(100);      
   v_parentId1  VARCHAR(100);
   v_Parentlen  INTEGER;
   v_ParentCategories  VARCHAR(1000);
BEGIN
   v_parentId1 := '';      
   select   coalesce(numParntAcntTypeID,0) INTO v_parentId from Chart_Of_Accounts where numAccountId = cast(NULLIF(v_CategoryId,'') as NUMERIC(18,0)) And numDomainId = v_numDomainId;      
   while cast(NULLIF(v_parentId,'') as INTEGER) > 1 LOOP
      v_parentId1 :=  coalesce(v_parentId1,'') || coalesce(v_parentId,'') || ',';
      select   coalesce(numParntAcntTypeID,0) INTO v_parentId from Chart_Of_Accounts where numAccountId = cast(NULLIF(v_parentId,'') as NUMERIC(18,0))    And numDomainId = v_numDomainId;
   END LOOP;      
  
   v_Parentlen := LENGTH(v_parentId1);      
   if v_Parentlen > 0 then

      v_ParentCategories := SUBSTR(v_parentId1,1,v_Parentlen -1);
   end if;  
  
   return v_ParentCategories;
END; $$;

