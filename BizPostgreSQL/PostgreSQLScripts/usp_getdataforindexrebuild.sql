DROP FUNCTION IF EXISTS USP_GetDataForIndexRebuild;

CREATE OR REPLACE FUNCTION USP_GetDataForIndexRebuild(v_numDomainID NUMERIC(18,0)
	,v_numSiteID NUMERIC(18,0)
	,v_numCurrentPage INTEGER DEFAULT 1
	,INOUT SWV_RefCur refcursor default null
	,INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_TotalRecords  INTEGER;
   v_BatchSize  INTEGER DEFAULT 2000;

   v_Fields  VARCHAR(1000);
   v_numFldID  VARCHAR(15);
   v_vcFldname  VARCHAR(200);
   v_vcDBFldname  VARCHAR(200);
   v_vcLookBackTableName  VARCHAR(200);
   v_intRowNum  INTEGER;
   v_strSQLUpdate  VARCHAR(4000);
   v_Custom  BOOLEAN;
   v_strSQL  TEXT;
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   SWV_RowCount INTEGER;
BEGIN
      -- BEGIN TRANSACTION
IF v_numCurrentPage = 1 then
	
         DELETE FROM LuceneItemsIndex WHERE numDomainID = v_numDomainID AND numSiteID = v_numSiteID;
      end if;
      DROP TABLE IF EXISTS tt_TEMPCUSTOMFIELD CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPCUSTOMFIELD AS
         SELECT
         ROW_NUMBER() OVER(ORDER BY vcFormFieldName) AS tintOrder
		,* FROM(SELECT
         DISTINCT numFieldID as numFormFieldId
			,vcDbColumnName
			,vcFieldName as vcFormFieldName
			,0 AS Custom
			,vcLookBackTableName
         FROM
         View_DynamicColumns
         WHERE
         numFormId = 30
         AND numDomainID = v_numDomainID
         AND coalesce(numRelCntType,0) = 0
         AND coalesce(bitCustom,false) = false
         AND coalesce(bitSettingField,false) = true
         UNION
         SELECT
         numFieldId as numFormFieldId
			,vcFieldName
			,vcFieldName
			,1 AS Custom
			,'' AS vcLookBackTableName
         FROM
         View_DynamicCustomColumns
         WHERE
         Grp_id = 5
         AND numFormId = 30
         AND numDomainID = v_numDomainID
         AND coalesce(numRelCntType,0) = 0) X;
      v_Fields := '';
      select(tintOrder+1), numFormFieldId, vcFormFieldName, vcDbColumnName, vcLookBackTableName INTO v_intRowNum,v_numFldID,v_vcFldname,v_vcDBFldname,v_vcLookBackTableName FROM
      tt_TEMPCUSTOMFIELD WHERE
      Custom = 0   ORDER BY
      tintOrder LIMIT 1;
      WHILE v_intRowNum > 0 LOOP
         IF(v_vcDBFldname = 'numItemCode') then
            v_vcDBFldname := 'I.numItemCode';
         end if;
         v_Fields := coalesce(v_Fields,'') || ','  || coalesce(v_vcDBFldname,'') || ' as "'  || coalesce(v_vcFldname,'') || '"';
         select(tintOrder+1), numFormFieldId, vcFormFieldName, vcDbColumnName, vcLookBackTableName INTO v_intRowNum,v_numFldID,v_vcFldname,v_vcDBFldname,v_vcLookBackTableName FROM
         tt_TEMPCUSTOMFIELD WHERE
         tintOrder >= v_intRowNum
         AND Custom = 0   ORDER BY tintOrder LIMIT 1;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         IF SWV_RowCount = 0 then 
            v_intRowNum := 0;
         end if;
      END LOOP;
      select   COUNT(*) INTO v_TotalRecords FROM
      Item I
      LEFT JOIN
      ItemExtendedDetails IED
      ON
      I.numItemCode = IED.numItemCode WHERE
      I.numDomainID = v_numDomainID
	  AND coalesce(I.IsArchieve,false) = false
      AND I.numItemCode IN (SELECT numItemID FROM SiteCategories INNER JOIN ItemCategory ON SiteCategories.numCategoryID = ItemCategory.numCategoryID  WHERE numSiteID = v_numSiteID);
      v_strSQL :=  CONCAT('DROP TABLE IF EXISTS tt_DATAFORINDEXING CASCADE;
							CREATE TEMPORARY TABLE tt_DATAFORINDEXING AS SELECT  
								I.numItemCode AS numItemCode',v_Fields,'            
							FROM 
								Item I 
							LEFT JOIN 
								ItemExtendedDetails IED
							ON 
								I.numItemCode = IED.numItemCode
							WHERE 
								I.numDomainID =',
      v_numDomainID,' AND coalesce(I.IsArchieve,false) = false 
								AND I.numItemCode IN (SELECT numItemID FROM SiteCategories INNER JOIN ItemCategory ON SiteCategories.numCategoryID = ItemCategory.numCategoryID  WHERE numSiteID = ',
      v_numSiteID,')
							ORDER BY 
								I.numItemCode
							OFFSET ', 
      (v_numCurrentPage::bigint -1)*v_BatchSize::bigint,' ROWS
							FETCH NEXT ',
      v_BatchSize,' ROWS ONLY; ');

	  RAISE NOTICE '%',v_strSQL;
      EXECUTE v_strSQL;

      v_strSQLUpdate := '';
      select(tintOrder+1), numFormFieldId, vcFormFieldName INTO v_intRowNum,v_numFldID,v_vcFldname FROM
      tt_TEMPCUSTOMFIELD WHERE
      Custom = 1   ORDER BY
      tintOrder LIMIT 1;
      WHILE v_intRowNum > 0 LOOP
         EXECUTE 'alter table tt_DATAFORINDEXING add "' || coalesce(v_vcFldname,'') || '_CustomField" varchar(500);';
         EXECUTE 'update tt_DATAFORINDEXING set "' || coalesce(v_vcFldname,'') || '_CustomField"=GetCustFldValueItem(' || coalesce(v_numFldID,'') || ',numItemCode) where numItemCode>0';
         
		 select(tintOrder+1), numFormFieldId, vcFormFieldName INTO v_intRowNum,v_numFldID,v_vcFldname FROM tt_TEMPCUSTOMFIELD WHERE tintOrder >= v_intRowNum AND Custom = 1   ORDER BY tintOrder LIMIT 1;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         IF SWV_RowCount = 0 then 
            v_intRowNum := 0;
         end if;
      END LOOP;
      
      v_strSQLUpdate := coalesce(v_strSQLUpdate,'') || ' INSERT INTO LuceneItemsIndex SELECT numItemCode,true,false,' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ',' || SUBSTR(CAST(v_numSiteID AS VARCHAR(10)),1,10) || '  FROM tt_DATAFORINDEXING where numItemCode not in (select numItemCode from LuceneItemsIndex where numDomainID =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ' and numSiteID = ' || SUBSTR(CAST(v_numSiteID AS VARCHAR(10)),1,10) || ' );';

      
	  RAISE NOTICE '%',v_strSQLUpdate;
	  EXECUTE v_strSQLUpdate;

	  OPEN SWV_RefCur FOR SELECT v_BatchSize AS BatchSize,v_TotalRecords AS TotalRecords;
	  OPEN SWV_RefCur2 FOR SELECT * FROM tt_DATAFORINDEXING;


	  DROP TABLE tt_TEMPCUSTOMFIELD;

   RETURN;
END; $$;



