-- Stored procedure definition script USP_DashboardTemplate_GetAllByDomain for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DashboardTemplate_GetAllByDomain(v_numDomainID NUMERIC(18,0)
,v_numUserCntID NUMERIC(18,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcDashboardTemplateIDs  VARCHAR(250);
BEGIN
   IF(v_numUserCntID = 0) then
		
      open SWV_RefCur for
      SELECT * FROM DashboardTemplate WHERE numDomainID = v_numDomainID;
   ELSE
      select   vcDashboardTemplateIDs INTO v_vcDashboardTemplateIDs from UserMaster where numDomainID = v_numDomainID	AND numUserDetailId	= v_numUserCntID;
      IF LENGTH(coalesce(v_vcDashboardTemplateIDs,'')) = 0 then
				
         select   numDashboardTemplateID INTO v_vcDashboardTemplateIDs FROM UserMaster WHERE numDomainID = v_numDomainID;
      end if;
      open SWV_RefCur for
      SELECT * FROM DashboardTemplate WHERE numDomainID = v_numDomainID AND
      1 =(CASE WHEN LENGTH(coalesce(v_vcDashboardTemplateIDs,'')) > 0 THEN(CASE WHEN numTemplateID IN(SELECT Id FROM SplitIDs((v_vcDashboardTemplateIDs),',')) THEN 1 ELSE 0 END) ELSE 1 END);
   end if;
   RETURN;
END; $$;


