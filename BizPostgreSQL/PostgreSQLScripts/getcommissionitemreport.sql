DROP FUNCTION IF EXISTS GetCommissionItemReport;

CREATE OR REPLACE FUNCTION GetCommissionItemReport(v_numDomainId NUMERIC(9,0) ,
      v_numUserCntID NUMERIC(9,0) ,
      v_numItemCode NUMERIC(9,0),
      v_tintAssignTo SMALLINT,
      v_bitCommContact BOOLEAN, INOUT SWV_RefCur refcursor, INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintComAppliesTo  SMALLINT;
   v_numComRuleID  NUMERIC;
   v_tintComBasedOn  SMALLINT;
   v_tintComType  SMALLINT;
   v_tinComDuration  SMALLINT; 
   v_dFrom  TIMESTAMP;
   v_dTo  TIMESTAMP;    
  
      
--DECLARE @i AS INT;SET @i=1
					
					--Loop for AssignTo i=1 and RecordOwner i=2 (set tintAssignTo=i)
					--WHILE @i<3
					--BEGIN
					
--Individual Items tintComAppliesTo=1 and tintType=1
   v_vcNextTier  VARCHAR(100);

   v_sInputText  VARCHAR(100);
   v_sItem  VARCHAR(100);
   v_sDelimiter  VARCHAR(100);
   v_vcNextTierTo  VARCHAR(30);
   v_vcNextTierCommission  VARCHAR(30);
   v_i  INTEGER;
   v_sql  VARCHAR(500);
BEGIN
   select   coalesce(tintComAppliesTo,0) INTO v_tintComAppliesTo FROM Domain WHERE numDomainId = v_numDomainId;


						
					--All Items	 tintComAppliesTo=3
   IF EXISTS(SELECT CR.numComRuleID FROM CommissionRules CR JOIN CommissionRuleItems CRI ON CR.numComRuleID = CRI.numComRuleID
   JOIN CommissionRuleContacts CRC ON CR.numComRuleID = CRC.numComRuleId
   WHERE CR.numDomainID = v_numDomainId AND CR.tintComAppliesTo = 1 AND CR.tintAssignTo = v_tintAssignTo AND CRI.numValue = v_numItemCode
   AND CRI.tintType = 1 AND CRC.numValue = v_numUserCntID AND CRC.bitCommContact = v_bitCommContact) AND v_tintComAppliesTo = 1 then
						
      select   CR.numComRuleID, CR.tintComBasedOn, CR.tintComType, CR.tinComDuration INTO v_numComRuleID,v_tintComBasedOn,v_tintComType,v_tinComDuration FROM CommissionRules CR JOIN CommissionRuleItems CRI ON CR.numComRuleID = CRI.numComRuleID
      JOIN CommissionRuleContacts CRC ON CR.numComRuleID = CRC.numComRuleId WHERE CR.numDomainID = v_numDomainId AND CR.tintComAppliesTo = 1 AND CR.tintAssignTo = v_tintAssignTo AND CRI.numValue = v_numItemCode
      AND CRI.tintType = 1 AND CRC.numValue = v_numUserCntID AND CRC.bitCommContact = v_bitCommContact    LIMIT 1;
						
					--Items Classification	tintComAppliesTo=2 and tintType=2
   ELSEIF EXISTS(SELECT CR.numComRuleID FROM CommissionRules CR JOIN CommissionRuleItems CRI ON CR.numComRuleID = CRI.numComRuleID	JOIN CommissionRuleContacts CRC ON CR.numComRuleID = CRC.numComRuleId
   WHERE CR.numDomainID = v_numDomainId AND CR.tintComAppliesTo = 2 AND CR.tintAssignTo = v_tintAssignTo AND CRI.numValue = v_numItemCode
   AND CRI.tintType = 2 AND CRC.numValue = v_numUserCntID AND CRC.bitCommContact = v_bitCommContact) AND v_tintComAppliesTo = 2
   then
						
      select   CR.numComRuleID, CR.tintComBasedOn, CR.tintComType, CR.tinComDuration INTO v_numComRuleID,v_tintComBasedOn,v_tintComType,v_tinComDuration FROM CommissionRules CR JOIN CommissionRuleItems CRI ON CR.numComRuleID = CRI.numComRuleID	JOIN CommissionRuleContacts CRC ON CR.numComRuleID = CRC.numComRuleId WHERE CR.numDomainID = v_numDomainId AND CR.tintComAppliesTo = 2 AND CR.tintAssignTo = v_tintAssignTo AND CRI.numValue = v_numItemCode
      AND CRI.tintType = 2 AND CRC.numValue = v_numUserCntID AND CRC.bitCommContact = v_bitCommContact    LIMIT 1;
   ELSEIF EXISTS(SELECT CR.numComRuleID FROM CommissionRules CR JOIN CommissionRuleContacts CRC ON CR.numComRuleID = CRC.numComRuleId  WHERE CR.tintComAppliesTo = 3 AND CR.numDomainID = v_numDomainId AND CR.tintAssignTo = v_tintAssignTo AND CRC.numValue = v_numUserCntID AND CRC.bitCommContact = v_bitCommContact) AND v_tintComAppliesTo = 3
   then
						
      select   CR.numComRuleID, CR.tintComBasedOn, CR.tintComType, CR.tinComDuration INTO v_numComRuleID,v_tintComBasedOn,v_tintComType,v_tinComDuration FROM CommissionRules CR JOIN CommissionRuleContacts CRC ON CR.numComRuleID = CRC.numComRuleId WHERE CR.tintComAppliesTo = 3 AND CR.numDomainID = v_numDomainId AND CR.tintAssignTo = v_tintAssignTo AND CRC.numValue = v_numUserCntID AND CRC.bitCommContact = v_bitCommContact    LIMIT 1;
   end if;
	
	
	--Get From and To date for Commission Calculation
   IF v_tinComDuration = 1 then --Month
      v_dFrom := '1900-01-01':: date+CAST((DATE_PART('year',AGE(LOCALTIMESTAMP,'1900-01-01':: DATE))*12+DATE_PART('month',AGE(LOCALTIMESTAMP,'1900-01-01':: DATE))) || 'month' as interval);
      v_dTo := '1900-01-01':: date+CAST((DATE_PART('year',AGE(LOCALTIMESTAMP,'1900-01-01':: DATE))*12+DATE_PART('month',AGE(LOCALTIMESTAMP,'1900-01-01':: DATE)))+1 || 'month' as interval)+INTERVAL '-1 day';
   ELSEIF v_tinComDuration = 2
   then --Quarter
      v_dFrom := '1900-01-01'::date + make_interval(months => 3 * CAST(DATEDIFF('month', '1900-01-01'::TIMESTAMP, now()::TIMESTAMP)/3 AS INT)) ;
      v_dTo := '1900-01-01'::date + make_interval(months => 3 * CAST((DATEDIFF('month', '1900-01-01'::TIMESTAMP, now()::TIMESTAMP)/3) + 1 AS INT)) + INTERVAL '-1 day';
   ELSEIF v_tinComDuration = 3
   then --Year
      v_dFrom := '1900-01-01':: date+CAST(extract(year from LOCALTIMESTAMP) -extract(year from 0:: date) || 'year' as interval);
      v_dTo := '1900-01-01':: date+CAST(extract(year from LOCALTIMESTAMP) -extract(year from 0:: date)+1 || 'year' as interval)+INTERVAL '-1 day';
   end if; 

									
   DROP table IF EXISTS tt_TEMPGetCommissionItemReport CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPGetCommissionItemReport ON COMMIT DROP AS
      Select SUM(numComissionAmount) as CommissionAmt,CASE v_tintComBasedOn WHEN 1 THEN SUM(BDI.monTotAmount) WHEN 2 THEN SUM(BDI.numUnitHour) end AS numBase,BC.numComRuleID,BC.decCommission,v_tintComType AS tintComType,MAX(BC.numComissionID) AS numComissionID

--Select CAST(SUM(numComissionAmount) AS VARCHAR(30)) + ' - ' + CAST(BC.decCommission AS VARCHAR(30)) + CASE @tintComType WHEN 1 THEN '--' WHEN 2 THEN ' Flat' END AS vcVertical,
--CASE @tintComBasedOn WHEN 1 THEN SUM(BDI.monTotAmount) WHEN 2 THEN SUM(BDI.numUnitHour) END AS numBase
      From OpportunityMaster Opp left join OpportunityBizDocs oppBiz on oppBiz.numoppid = Opp.numOppId
      INNER JOIN BizDocComission BC on BC.numOppBizDocId = oppBiz.numOppBizDocsId
      JOIN OpportunityBizDocItems BDI ON oppBiz.numOppBizDocsId = BDI.numOppBizDocID AND BC.numOppBizDocItemID = BDI.numOppBizDocItemID
      INNER JOIN OpportunityItems OT ON OT.numoppitemtCode = BDI.numOppItemID
      Where oppBiz.bitAuthoritativeBizDocs = 1 AND oppBiz.monAmountPaid > 0 and Opp.tintoppstatus = 1 And Opp.tintopptype = 1 And 
--(Opp.numRecOwner=@numUserCntID OR Opp.numAssignedTo=@numUserCntID)

      1 =(CASE v_tintAssignTo WHEN 1 THEN CASE v_bitCommContact WHEN false THEN CASE WHEN Opp.numassignedto = v_numUserCntID THEN 1 ELSE 0 END
         WHEN true THEN CASE WHEN Opp.numassignedto IN(SELECT numContactId FROM AdditionalContactsInformation AC WHERE AC.numDivisionId = v_numUserCntID) THEN 1 ELSE 0 END  END
      WHEN 2 THEN CASE v_bitCommContact WHEN false THEN CASE WHEN Opp.numrecowner = v_numUserCntID THEN 1 ELSE 0 END
         WHEN true THEN CASE WHEN Opp.numrecowner IN(SELECT numContactId FROM AdditionalContactsInformation AC WHERE AC.numDivisionId = v_numUserCntID) THEN 1 ELSE 0 END END END)
      AND 1 =(CASE v_bitCommContact WHEN false THEN CASE WHEN BC.numUserCntID = v_numUserCntID THEN 1 else 0 END
      WHEN true THEN CASE WHEN BC.numUserCntID IN(SELECT numContactId FROM AdditionalContactsInformation AC WHERE AC.numDivisionId = v_numUserCntID) THEN 1 ELSE 0 END  END)
      And Opp.numDomainId = v_numDomainId AND BC.numComRuleID = v_numComRuleID
      AND 1 =(CASE v_tintComAppliesTo WHEN 1 THEN CASE WHEN BDI.numItemCode = v_numItemCode THEN 1 ELSE 0 END
      WHEN 2 THEN CASE WHEN BDI.numItemCode IN(SELECT numItemCode FROM Item WHERE numItemClassification = v_numItemCode AND numDomainID = v_numDomainId) THEN 1 ELSE 0 END
      WHEN 3 THEN 1 ELSE 0 END)
      AND oppBiz.numOppBizDocsId IN(SELECT OBD.numOppBizDocsId FROM OpportunityBizDocsDetails BDD
      INNER JOIN OpportunityBizDocs OBD ON BDD.numBizDocsId = OBD.numOppBizDocsId
      INNER JOIN OpportunityMaster OM ON OM.numOppId = OBD.numoppid
      WHERE tintopptype = 1 AND tintoppstatus = 1 AND OM.numDomainId = v_numDomainId
      AND OBD.bitAuthoritativeBizDocs = 1 AND BDD.dtCreationDate BETWEEN v_dFrom AND v_dTo AND 
--(OM.numassignedto=@numUserCntID OR OM.numrecowner=@numUserCntID)

      1 =(CASE v_tintAssignTo WHEN 1 THEN CASE v_bitCommContact WHEN false THEN CASE WHEN OM.numassignedto = v_numUserCntID THEN 1 ELSE 0 END
         WHEN true THEN CASE WHEN OM.numassignedto IN(SELECT numContactId FROM AdditionalContactsInformation AC WHERE AC.numDivisionId = v_numUserCntID) THEN 1 ELSE 0 END  END
      WHEN 2 THEN CASE v_bitCommContact WHEN false THEN CASE WHEN OM.numrecowner = v_numUserCntID THEN 1 ELSE 0 END
         WHEN true THEN CASE WHEN OM.numrecowner IN(SELECT numContactId FROM AdditionalContactsInformation AC WHERE AC.numDivisionId = v_numUserCntID) THEN 1 ELSE 0 END END END)
      GROUP BY OBD.numOppBizDocsId HAVING SUM(BDD.monAmount) >= MAX(OBD.monDealAmount))
      GROUP BY BC.numComRuleID,BC.decCommission ORDER BY numComissionID;
    

   select   coalesce(vcnexttier,'') INTO v_vcNextTier FROM BizDocComission where numComissionID =(select max(numComissionID) from tt_TEMPGetCommissionItemReport)    LIMIT 1;

   v_i := 0;
   v_sDelimiter := ',';

   IF LENGTH(v_vcNextTier) > 0 then

      v_sInputText := v_vcNextTier;
      WHILE POSITION(v_sDelimiter IN v_sInputText) <> 0 LOOP
         v_sItem := RTRIM(LTRIM(SUBSTR(v_sInputText,1,POSITION(v_sDelimiter IN v_sInputText) -1)));
         v_sInputText := RTRIM(LTRIM(SUBSTR(v_sInputText,POSITION(v_sDelimiter IN v_sInputText)+LENGTH(v_sDelimiter),
         LENGTH(v_sInputText))));
         v_i := v_i::bigint+1;
         IF v_i = 4 then

            v_vcNextTierTo := v_sItem;
            v_vcNextTierCommission := v_sInputText;
         end if;
      END LOOP;
   end if;

   v_sql := 'SELECT  CAST(decCommission AS VARCHAR(30)) || CASE ' || COALESCE(v_tintComType,0) || ' WHEN ''1'' THEN ''%'' WHEN ''2'' THEN '' Flat'' END AS vcVertical,
 numBase,''Commission Made: '' || CAST(CommissionAmt AS VARCHAR(30)) AS vcText FROM tt_TEMPGetCommissionItemReport';

   IF LENGTH(COALESCE(v_vcNextTier,'')) > 0 then
      v_sql := coalesce(v_sql,'') || ' Union all SELECT ''' || COALESCE(v_vcNextTierCommission,0) || ''' || CASE ' || COALESCE(v_tintComType,0) || ' WHEN ''1'' THEN ''%'' WHEN ''2'' THEN '' Flat'' END,CAST(' || COALESCE(v_vcNextTierTo,0) || ' AS INTEGER),CAST(''Next Tier'' AS VARCHAR(17))';
   end if;

   RAISE NOTICE '%',(v_sql);
   OPEN SWV_RefCur FOR EXECUTE v_sql;

   open SWV_RefCur2 for
   SELECT CASE v_tinComDuration WHEN 1 THEN 'This Month' WHEN 2 THEN 'This Quarter' WHEN 3 THEN 'This Year' END AS vcCommissionDuration,(SELECT sum(CommissionAmt) FROM tt_TEMPGetCommissionItemReport) AS TotalCommissionAmt,CASE v_tintComBasedOn WHEN 1 THEN 'Amount Sold' WHEN 2 THEN 'Units Sold' END AS vcComBasedOn ,v_numItemCode AS numItemCode,v_numComRuleID AS numComRuleID;

   RETURN;
END; $$;



