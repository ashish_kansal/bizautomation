-- Stored procedure definition script USP_CategoryProfile_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_CategoryProfile_Save(v_ID NUMERIC(18,0),
	v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0),
	v_vcName VARCHAR(200),
	v_vcSiteIDs VARCHAR(500))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_ErrorMessage  VARCHAR(4000);
   v_ErrorNumber  TEXT;
   v_ErrorSeverity  INTEGER;
   v_ErrorState  INTEGER;
   v_ErrorLine  INTEGER;
   v_ErrorProcedure  VARCHAR(200);
BEGIN
   BEGIN
      -- BEGIN TRANSACTION
IF v_ID = 0 then --INSERT 
	
         INSERT INTO CategoryProfile(vcName,
			numDomainID,
			numCreatedBy,
			dtCreated)
		VALUES(v_vcName,
			v_numDomainID,
			v_numUserCntID,
			TIMEZONE('UTC',now()));
		
         v_ID := CURRVAL('CategoryProfile_seq');
      ELSE
         UPDATE
         CategoryProfile
         SET
         vcName = v_vcName,numModifiedBy = v_numUserCntID,dtModified = TIMEZONE('UTC',now())
         WHERE
         ID = v_ID;
      end if;
      IF LENGTH(v_vcSiteIDs) = 0 then
	
		-- REMOVE SITE CATEGORY ASSIGNEDMENT AS SITES DOES NOT BELONG TO THIS CATEGORY PROFILE
         DELETE FROM SiteCategories WHERE numSiteID IN(SELECT coalesce(numSiteID,0) FROM CategoryProfileSites WHERE numCategoryProfileID = v_ID);

		-- REMOVE SITES ASSIGNED TO CATEGORY PROFILE
         DELETE FROM CategoryProfileSites WHERE numCategoryProfileID = v_ID;
      ELSE
         DROP TABLE IF EXISTS tt_TEMP CASCADE;
         CREATE TEMPORARY TABLE tt_TEMP
         (
            ID NUMERIC(18,0)
         );
         INSERT INTO tt_TEMP(ID) SELECT Id FROM SplitIDs(v_vcSiteIDs,',');

		--DELETE SITE CATEGORY CONFIGURATION AS THEY BELONGS TO NEW CATEGORY PROFILE NOW
         DELETE FROM SiteCategories WHERE numSiteID IN(SELECT coalesce(ID,0) FROM tt_TEMP);
		--DELETE SITES WHICH ARE ASSIGNED TO OTHER CATEGORY AS THEY BELONGS TO NEW CATEGORY PROFILE NOW
         DELETE FROM CategoryProfileSites WHERE numCategoryProfileID <> v_ID AND numSiteID IN(SELECT coalesce(ID,0) FROM tt_TEMP);


		--DELETE SITE CATEGORY CONFIGURATION FOR SITES AS THEY DOES NOT BELONGS TO THIS CATEGORY PROFILE
         DELETE FROM SiteCategories WHERE numSiteID IN(SELECT coalesce(numSiteID,0) FROM CategoryProfileSites WHERE numCategoryProfileID = v_ID AND numSiteID NOT IN(SELECT coalesce(ID,0) FROM tt_TEMP));
		-- DELETE SITES WHICH ARE REMOVED FROM CATEGORY PROFILE
         DELETE FROM CategoryProfileSites WHERE numCategoryProfileID = v_ID AND numSiteID NOT IN(SELECT coalesce(ID,0) FROM tt_TEMP);

		-- INSERT NEW SELECTED SITES
         INSERT INTO CategoryProfileSites(numCategoryProfileID,
			numSiteID)
         SELECT
         v_ID,
			ID
         FROM
         tt_TEMP
         WHERE
         ID NOT IN(SELECT numSiteID FROM CategoryProfileSites WHERE numCategoryProfileID = v_ID);
         INSERT INTO SiteCategories(numSiteID,
			numCategoryID)
         SELECT
         CategoryProfileSites.numSiteID,
			Category.numCategoryID
         FROm
         CategoryProfileSites
         JOIN
         Category
         ON
         CategoryProfileSites.numCategoryProfileID = Category.numCategoryProfileID
         WHERE
         CategoryProfileSites.numCategoryProfileID = v_ID
         ORDER BY
         CategoryProfileSites.numSiteID;
      end if;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
         -- ROLLBACK TRANSACTION
v_ErrorMessage := SQLERRM;
         v_ErrorNumber := sqlstate;
         v_ErrorSeverity := ERROR_SEVERITY();
         v_ErrorState := ERROR_STATE();
         v_ErrorLine := ERROR_LINE();
         v_ErrorProcedure := coalesce(ERROR_PROCEDURE(),'-');
         RAISE NOTICE '% % % % % % ',v_ErrorMessage,v_ErrorNumber,v_ErrorSeverity,v_ErrorState,v_ErrorProcedure,
         v_ErrorLine;
   END;
   RETURN;
END; $$;
--Created by anoop jayaraj                                                          


