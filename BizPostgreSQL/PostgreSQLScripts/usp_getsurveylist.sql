-- Stored procedure definition script usp_GetSurveyList for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetSurveyList(v_numDomainID NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT sm.numSurID, sm.bitSkipRegistration,vcSurName,
  bintCreatedOn,
  sm.numSurID || ',' || sm.bitSkipRegistration as LinkedReport ,
(SELECT  Count(numRespondantID)
      FROM SurveyRespondentsMaster srm
      WHERE     bitRegisteredRespondant = true
      and srm.numDomainId = v_numDomainID
      AND sm.numSurID = srm.numSurID) as numTotalRespondents
   FROM SurveyMaster sm
   WHERE sm.numDomainID = v_numDomainID;              
          
          --UNION select 0,0,'--Select One--',0,Convert(Nvarchar,0),0  
END; $$;












