-- Stored procedure definition script USP_SendActionItemAlert for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SendActionItemAlert()
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcSubject  VARCHAR(100);
   v_vcBody  VARCHAR(8000);
   v_hours  SMALLINT;
   v_email  VARCHAR(100);
   v_firstname  VARCHAR(50);
   v_lastname  VARCHAR(50);
   v_numCommId  NUMERIC(9,0);
   v_dtStartTime  TIMESTAMP;
   SWV_RowCount INTEGER;
BEGIN
   v_numCommId := 0;

   select   numCommId, vcFirstName, vcEmail, vcLastname, vcDocDesc, vcSubject, tinthours INTO v_numCommId,v_firstname,v_email,v_lastname,v_vcBody,v_vcSubject,v_hours from Communication
   join GenericDocuments
   on numEmailTemplate = numGenericDocID
   join AdditionalContactsInformation A
   on A.numContactId = numAssignedBy where bitalert = true and bitClosedFlag = false and TIMEZONE('UTC',now()) >=(case when bitsendEmailTemp = false then dtStartTime+CAST(-tinthours || 'hour' as interval)
   when bitsendEmailTemp = true then dtStartTime+CAST(tinthours || 'hour' as interval) end);  
   while v_numCommId > 0 LOOP
      v_vcBody := replace(v_vcBody,'##vcFirstName##',v_firstname);
      v_vcBody := replace(v_vcBody,'##vcLastName##',v_lastname);
      v_vcBody := replace(v_vcBody,'##hours##',v_hours:: TEXT);
		--print @email+@vcSubject+ @vcBody
      PERFORM sendMail_With_CDOMessage(v_email,v_vcSubject,v_vcBody,'','admin@bizautomation.com,');
      update 	Communication set bitalert = false where numCommId = v_numCommId;
      select   numCommId, vcFirstName, vcLastname, vcDocDesc, vcSubject, tinthours INTO v_numCommId,v_firstname,v_lastname,v_vcBody,v_vcSubject,v_hours from Communication
      join GenericDocuments
      on numEmailTemplate = numGenericDocID
      join AdditionalContactsInformation A
      on A.numContactId = numAssignedBy where bitalert = true and TIMEZONE('UTC',now()) >=(case when bitsendEmailTemp = false then dtStartTime+CAST(-tinthours || 'hour' as interval)
      when bitsendEmailTemp = true and bitClosedFlag = false then dtStartTime+CAST(tinthours || 'hour' as interval) end) and numCommId > v_numCommId;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount = 0 then 
         v_numCommId := 0;
      end if;
   END LOOP;
   RETURN;
END; $$;


