-- Stored procedure definition script USP_GetProductionPlanningReportData for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetProductionPlanningReportData(v_numDomainID NUMERIC(18,0)                            
	,v_tintProcessType SMALLINT
	,v_dtFromDate TIMESTAMP
	,v_dtToDate TIMESTAMP
	,v_numPageIndex INTEGER
	,v_numPageSize INTEGER
	,v_ClientTimeZoneOffset INTEGER, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER; 	
   v_numTempTaskID  NUMERIC(18,0);
   v_numTaskAssignee  NUMERIC(18,0);
   v_intTaskType  INTEGER;
   v_numTotalTaskInMinutes  NUMERIC(18,0);
   v_numWorkOrderID  NUMERIC(18,0);
   v_numQtyItemsReq  DOUBLE PRECISION;

   v_numWorkScheduleID  NUMERIC(18,0);
   v_numProductiveTimeInMinutes  NUMERIC(18,0);
   v_tmStartOfDay  TIME;
   v_numTimeLeftForDay  NUMERIC(18,0);
   v_vcWorkDays  VARCHAR(20);
   v_dtStartDate  TIMESTAMP;
   v_bitTaskStarted  BOOLEAN;
BEGIN
   DROP TABLE IF EXISTS tt_TEMPDATA CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPDATA
   (
      id VARCHAR(50),
      text VARCHAR(1000),
      start_date TIMESTAMP,
      end_date TIMESTAMP,
      duration DOUBLE PRECISION,
      progress DOUBLE PRECISION,
      sortorder INTEGER,
      parent VARCHAR(50),
      open BOOLEAN,
      color VARCHAR(50),
      progressColor VARCHAR(50),
      numWOID NUMERIC(18,0),
      vcMilestone VARCHAR(500),
      numStageDetailsId NUMERIC(18,0),
      numTaskID NUMERIC(18,0),
      vcItem VARCHAR(300),
      dtRequestedFinish VARCHAR(20),
      dtProjectedFinish VARCHAR(20),
      dtItemReleaseDate VARCHAR(20),
      dtActualStartDate VARCHAR(20),
      vcDurationHours VARCHAR(20)
   );
   BEGIN
      CREATE TEMP SEQUENCE tt_TempLinks_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPLINKS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPLINKS
   (
      id INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      source VARCHAR(50),
      target VARCHAR(50),
      type INTEGER
   );
   INSERT INTO tt_TEMPDATA(id
		,text
		,start_date
		,duration
		,progress
		,sortorder
		,parent
		,open
		,numWOID
		,vcItem
		,dtRequestedFinish
		,dtProjectedFinish
		,dtItemReleaseDate
		,dtActualStartDate)
   SELECT 
   CONCAT('WO',WorkOrder.numWOId)
		,CONCAT(coalesce(WorkOrder.vcWorkOrderName,'-'),' (',GetTotalProgress(v_numDomainID,WorkOrder.numWOId,1::SMALLINT,1::SMALLINT,'',0),
   ') ',' <a href="javascript:void(0);" type="button" class="btn btn-secondary" data-toggle="tooltip" data-placement="right" data-html="true" title="<table><tr><td style=''white-space:nowrap;text-align:right;padding:5px''>Item (Qty):</td><td style=''white-space:nowrap;text-align:left;''>',CONCAT(Item.vcItemName,' (',coalesce(WorkOrder.numQtyItemsReq,0),')'),'</td></tr><tr><td style=''white-space:nowrap;text-align:right;padding:5px''>Requested Finish:</td><td style=''white-space:nowrap;text-align:left;''>',FormatedDateFromDate(WorkOrder.dtmEndDate+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval),v_numDomainID),
   '</td></tr><tr><td style=''white-space:nowrap;text-align:right;padding:5px''>Projected Finish:</td><td style=''white-space:nowrap;text-align:left;''>',FormatedDateFromDate(GetProjectedFinish(v_numDomainID,2::SMALLINT,WorkOrder.numWOId,0,0,NULL,v_ClientTimeZoneOffset,0),v_numDomainID),
   '</td></tr><tr><td style=''white-space:nowrap;text-align:right;padding:5px''>Item Release:</td><td style=''white-space:nowrap;text-align:left;''>',FormatedDateFromDate(OpportunityItems.ItemReleaseDate,v_numDomainID),
   '</td></tr><tr><td style=''white-space:nowrap;text-align:right;padding:5px''>Actual Start:</td><td style=''white-space:nowrap;text-align:left;''>',(CASE
   WHEN TEMP.dtActualStartDate IS NOT NULL
   THEN FormatedDateFromDate(TEMP.dtActualStartDate+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval),v_numDomainID)
   ELSE NULL
   END),
   '</td></tr></table>"><i class="fa fa-bars"></i></a>')
		,NULL
		,NULL
		,GetTotalProgress(v_numDomainID,WorkOrder.numWOId,1::SMALLINT,1::SMALLINT,'',0)::bigint/100.0
		,ROW_NUMBER() OVER(ORDER BY WorkOrder.dtmStartDate)
		,CAST(0 AS VARCHAR(50))
		,CAST(1 AS BOOLEAN)
		,WorkOrder.numWOId
		,CONCAT(Item.vcItemName,' (',coalesce(WorkOrder.numQtyItemsReq,0),')')
		,CAST(FormatedDateFromDate(WorkOrder.dtmEndDate+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval),v_numDomainID) AS VARCHAR(20))
		,CAST(FormatedDateFromDate(GetProjectedFinish(v_numDomainID,2::SMALLINT,WorkOrder.numWOId,0,0,NULL,v_ClientTimeZoneOffset,0),v_numDomainID) AS VARCHAR(20))
		,CAST(FormatedDateFromDate(OpportunityItems.ItemReleaseDate,v_numDomainID) AS VARCHAR(20))
		,CAST((CASE
   WHEN TEMP.dtActualStartDate IS NOT NULL
   THEN FormatedDateFromDate(TEMP.dtActualStartDate+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval),v_numDomainID)
   ELSE NULL
   END) AS VARCHAR(20))
   FROM
   WorkOrder
   INNER JOIN
   Item
   ON
   WorkOrder.numItemCode = Item.numItemCode
   LEFT JOIN
   OpportunityItems
   ON
   WorkOrder.numOppItemID = OpportunityItems.numoppitemtCode
   LEFT JOIN LATERAL(SELECT
      MIN(SPDTTL.dtActionTime) AS dtActualStartDate
      FROM
      StagePercentageDetailsTask SPDT
      INNER JOIN
      StagePercentageDetailsTaskTimeLog SPDTTL
      ON
      SPDT.numTaskId = SPDTTL.numTaskId
      WHERE
      SPDT.numWorkOrderId = WorkOrder.numWOId) TEMP on TRUE
   WHERE
   WorkOrder.numDomainId = v_numDomainID
   AND WorkOrder.numWOStatus <> 23184
   AND (CAST(coalesce(WorkOrder.dtmStartDate,WorkOrder.bintCreatedDate) AS DATE) BETWEEN CAST(v_dtFromDate AS DATE) AND CAST(v_dtToDate AS DATE) OR EXISTS(SELECT * FROM StagePercentageDetailsTask SPDT WHERE SPDT.numWorkOrderId = WorkOrder.numWOId AND(SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId) > 0))
   AND EXISTS(SELECT numTaskId FROM StagePercentageDetailsTask SPDT WHERE SPDT.numWorkOrderId = WorkOrder.numWOId)
   ORDER BY
   WorkOrder.dtmStartDate LIMIT 10;

   INSERT INTO tt_TEMPDATA(id
		,text
		,start_date
		,duration
		,progress
		,sortorder
		,parent
		,open
		,numWOID
		,vcMilestone)
   SELECT
   CONCAT('MS',ROW_NUMBER() OVER(ORDER BY vcMileStoneName))
		,vcMileStoneName
		,NULL
		,NULL
		,GetTotalProgress(v_numDomainID,TD.numWOID,1::SMALLINT,2::SMALLINT,vcMileStoneName,0)::bigint/100.0
		,ROW_NUMBER() OVER(ORDER BY vcMileStoneName)
		,TD.id
		,CAST(1 AS BOOLEAN)
		,numWOID
		,vcMileStoneName
   FROM
   StagePercentageDetails SPD
   INNER JOIN
   tt_TEMPDATA TD
   ON
   SPD.numWorkOrderId = TD.numWOID
   GROUP BY
   vcMileStoneName,TD.id,TD.numWOID;

   INSERT INTO tt_TEMPDATA(id
		,text
		,start_date
		,duration
		,progress
		,sortorder
		,parent
		,open
		,numStageDetailsId)
   SELECT
   CONCAT('S',SPD.numStageDetailsId)
		,SPD.vcStageName
		,NULL
		,NULL
		,GetTotalProgress(v_numDomainID,TD.numWOID,1::SMALLINT,3::SMALLINT,'',SPD.numStageDetailsId)::bigint/100.0
		,ROW_NUMBER() OVER(ORDER BY SPD.numStageDetailsId)
		,TD.id
		,CAST(1 AS BOOLEAN)
		,SPD.numStageDetailsId
   FROM
   StagePercentageDetails SPD
   INNER JOIN
   tt_TEMPDATA TD
   ON
   SPD.numWorkOrderId = TD.numWOID
   AND SPD.vcMileStoneName = TD.vcMilestone;

   INSERT INTO tt_TEMPDATA(id
		,text
		,start_date
		,end_date
		,duration
		,progress
		,sortorder
		,parent
		,open
		,color
		,progressColor
		,numTaskID
		,vcDurationHours)
   SELECT
   CONCAT((CASE
   WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId AND tintAction = 4)
   THEN 'TC'
   WHEN(SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId) > 0
   THEN 'TS'
   ELSE 'T'
   END),SPDT.numTaskId)
		,SPDT.vcTaskName
		,(CASE
   WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId AND tintAction = 1) AND EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId AND tintAction = 4)
   THEN(SELECT MIN(dtActionTime)+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId AND tintAction = 1)
   ELSE SPDT.dtStartTime+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval)
   END)
		,(CASE
   WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId AND tintAction = 1) AND EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId AND tintAction = 4)
   THEN(SELECT MAX(dtActionTime)+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId AND tintAction = 4)
   ELSE SPDT.dtEndTime+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval)
   END)
		,((coalesce(numHours,0)*60)+coalesce(numMinutes,0))*coalesce(WorkOrder.numQtyItemsReq,0)
		,(CASE
   WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId AND tintAction = 4)
   THEN CAST(100 AS decimal)/100
   ELSE 0
   END)
		,ROW_NUMBER() OVER(ORDER BY SPDT.numTaskId)
		,TD.id
		,CAST(1 AS BOOLEAN)
		,CAST((CASE
   WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId AND tintAction = 4)
   THEN '#2ecc71'
   WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId AND tintAction = 1)
   THEN '#fd7622'
   ELSE '#3498db'
   END) AS VARCHAR(50))
		,CAST((CASE
   WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId AND tintAction = 4)
   THEN '#02ce58'
   WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId AND tintAction = 1)
   THEN '#ff6200'
   ELSE '#0286de'
   END) AS VARCHAR(50))
		,SPDT.numTaskId
		,CAST(TO_CHAR(FLOOR(CAST((((coalesce(SPDT.numHours,0)*60)+coalesce(SPDT.numMinutes,0))*coalesce(WorkOrder.numQtyItemsReq,0)) AS decimal)/60),
   '00') || ':' || TO_CHAR(MOD((((coalesce(SPDT.numHours,0)*60)+coalesce(SPDT.numMinutes,0))*CAST(coalesce(WorkOrder.numQtyItemsReq,0) AS DECIMAL)),60),
   '00') AS VARCHAR(20))
   FROM
   StagePercentageDetailsTask SPDT
   INNER JOIN
   WorkOrder
   ON
   SPDT.numWorkOrderId = WorkOrder.numWOId
   INNER JOIN
   tt_TEMPDATA TD
   ON
   SPDT.numStageDetailsId = TD.numStageDetailsId;


   DROP TABLE IF EXISTS tt_TEMPTASKASSIGNEE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTASKASSIGNEE
   (
      numWorkOrderID NUMERIC(18,0),
      numAssignedTo NUMERIC(18,0),
      numLastTaskID NUMERIC(18,0),
      dtLastTaskCompletionTime TIMESTAMP
   );

   DROP TABLE IF EXISTS tt_TEMPTIMELOG CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTIMELOG
   (
      numWrokOrder NUMERIC(18,0),
      numTaskID NUMERIC(18,0),
      numQtyItemsReq DOUBLE PRECISION,
      numAssignTo NUMERIC(18,0),
      numProductiveMinutes NUMERIC(18,0),
      dtDate TIMESTAMP,
      numTotalMinutes NUMERIC(18,0)
   );

   BEGIN
      CREATE TEMP SEQUENCE tt_TempTasks_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPTASKS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTASKS
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numTaskID NUMERIC(18,0),
      numWorkOrderID NUMERIC(18,0),
      numAssignTo NUMERIC(18,0),
      numTaskTimeInMinutes NUMERIC(18,0),
      intTaskType INTEGER,
      dtStartDate TIMESTAMP,
      dtFinishDate TIMESTAMP,
      numQtyItemsReq DOUBLE PRECISION,
      numProcessedQty DOUBLE PRECISION,
      bitTaskStarted BOOLEAN
   );

   INSERT INTO tt_TEMPTASKS(numTaskID
		,numWorkOrderID
		,numAssignTo
		,numTaskTimeInMinutes
		,intTaskType
		,dtStartDate
		,numQtyItemsReq
		,numProcessedQty
		,bitTaskStarted)
   SELECT
   SPDT.numTaskId
		,WorkOrder.numWOId
		,SPDT.numAssignTo
		,((coalesce(SPDT.numHours,0)*60)+coalesce(SPDT.numMinutes,0))*(coalesce(numQtyItemsReq,0) -coalesce((SELECT SUM(SPDTTL.numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId),0))
		,SPDT.intTaskType
		,(CASE
   WHEN(SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId AND tintAction = 1) > 0
   THEN(SELECT MIN(dtActionTime) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId AND tintAction = 1)
   ELSE coalesce(WorkOrder.dtmStartDate,WorkOrder.bintCreatedDate)
   END)
		,WorkOrder.numQtyItemsReq
		,coalesce((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId),0)
		,CAST((CASE
   WHEN(SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId) > 0
   THEN 1
   ELSE 0
   END) AS BOOLEAN)
   FROM
   tt_TEMPDATA TD
   INNER JOIN
   StagePercentageDetailsTask SPDT
   ON
   TD.numTaskID = SPDT.numTaskId
   INNER JOIN
   WorkOrder
   ON
   SPDT.numWorkOrderId = WorkOrder.numWOId
   WHERE
   TD.numTaskID > 0
   AND(SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId AND tintAction = 4) = 0; -- Task in not finished
	
   INSERT INTO tt_TEMPTASKASSIGNEE(numWorkOrderID
		,numAssignedTo)
   SELECT
   numWorkOrderID
		,numAssignTo
   FROM
   tt_TEMPTASKS
   GROUP BY
   numWorkOrderID,numAssignTo;

   select   COUNT(*) INTO v_iCount FROM tt_TEMPTASKS;

   WHILE v_i <= v_iCount LOOP
      select   numTaskID, numAssignTo, numTaskTimeInMinutes, intTaskType, dtStartDate, numWorkOrderID, numQtyItemsReq, bitTaskStarted INTO v_numTempTaskID,v_numTaskAssignee,v_numTotalTaskInMinutes,v_intTaskType,
      v_dtStartDate,v_numWorkOrderID,v_numQtyItemsReq,v_bitTaskStarted FROM
      tt_TEMPTASKS WHERE
      ID = v_i;

		-- GET TASK COMPLETION TIME WHICH WILL BE START DATETIME FOR NEXT TASK
      select   WS.ID, (coalesce(WS.numProductiveHours,0)*60)+coalesce(WS.numProductiveMinutes,0), tmStartOfDay, CONCAT(',',vcWorkDays,','), (CASE
      WHEN v_bitTaskStarted = true
      THEN v_dtStartDate
      ELSE CAST(TO_CHAR(v_dtStartDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),
         'yyyymmdd') AS TIMESTAMP)+tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval)
      END) INTO v_numWorkScheduleID,v_numProductiveTimeInMinutes,v_tmStartOfDay,v_vcWorkDays,
      v_dtStartDate FROM
      WorkSchedule WS
      INNER JOIN
      UserMaster
      ON
      WS.numUserCntID = UserMaster.numUserDetailId WHERE
      WS.numUserCntID = v_numTaskAssignee;
      IF EXISTS(SELECT numAssignedTo FROM tt_TEMPTASKASSIGNEE WHERE numWorkOrderID = v_numWorkOrderID AND numAssignedTo = v_numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL) then
		
         INSERT INTO tt_TEMPLINKS(source
				,target
				,type)
			VALUES(CONCAT((CASE WHEN v_bitTaskStarted = true THEN 'TS' ELSE 'T' END),(SELECT numLastTaskID FROM tt_TEMPTASKASSIGNEE WHERE numWorkOrderID = v_numWorkOrderID AND numAssignedTo = v_numTaskAssignee))
				,CONCAT((CASE WHEN v_bitTaskStarted = true THEN 'TS' ELSE 'T' END),v_numTempTaskID)
				,0);
			
         select   dtLastTaskCompletionTime INTO v_dtStartDate FROM tt_TEMPTASKASSIGNEE WHERE numWorkOrderID = v_numWorkOrderID AND numAssignedTo = v_numTaskAssignee;
      ELSE
         INSERT INTO tt_TEMPLINKS(source
				,target
				,type)
			VALUES(CONCAT('WO',v_numWorkOrderID)
				,CONCAT((CASE WHEN v_bitTaskStarted = true THEN 'TS' ELSE 'T' END),v_numTempTaskID)
				,1);
      end if;
      UPDATE tt_TEMPTASKS SET dtStartDate = v_dtStartDate WHERE ID = v_i;
      IF v_numProductiveTimeInMinutes > 0 AND v_numTotalTaskInMinutes > 0 then
		
         WHILE v_numTotalTaskInMinutes > 0 LOOP
				-- IF ITS WORKING DAY AND ALSO NOT HOLIDAY THEN PROCEED OR INCRESE DATE TO NEXT DAY
            IF POSITION(CONCAT(',',EXTRACT(DOW FROM v_dtStartDate)+1,',') IN v_vcWorkDays) > 0 AND NOT EXISTS(SELECT ID FROM WorkScheduleDaysOff WHERE numWorkScheduleID = v_numWorkScheduleID AND CAST(v_dtStartDate+CAST(v_ClientTimeZoneOffset::bigint*-1 || 'minute' as interval) AS DATE) BETWEEN dtDayOffFrom:: DATE AND dtDayOffTo:: DATE) then
				
					-- CHECK TIME LEFT FOR DAY BASED
               v_numTimeLeftForDay :=(EXTRACT(DAY FROM CAST(TO_CHAR(v_dtStartDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),
               'yyyymmdd') AS TIMESTAMP)+v_tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numProductiveTimeInMinutes || 'minute' as interval) -v_dtStartDate)*60*24+EXTRACT(HOUR FROM CAST(TO_CHAR(v_dtStartDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),
               'yyyymmdd') AS TIMESTAMP)+v_tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numProductiveTimeInMinutes || 'minute' as interval) -v_dtStartDate)*60+EXTRACT(MINUTE FROM CAST(TO_CHAR(v_dtStartDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),
               'yyyymmdd') AS TIMESTAMP)+v_tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numProductiveTimeInMinutes || 'minute' as interval) -v_dtStartDate));
               IF v_numTimeLeftForDay > 0 then
					
                  INSERT INTO tt_TEMPTIMELOG(numWrokOrder
							,numTaskID
							,numQtyItemsReq
							,numAssignTo
							,numProductiveMinutes
							,dtDate
							,numTotalMinutes)
						VALUES(v_numWorkOrderID
							,v_numTempTaskID
							,v_numQtyItemsReq
							,v_numTaskAssignee
							,v_numProductiveTimeInMinutes
							,v_dtStartDate
							,(CASE WHEN v_numTimeLeftForDay > v_numTotalTaskInMinutes THEN v_numTotalTaskInMinutes ELSE v_numTimeLeftForDay END));
						
                  IF v_numTimeLeftForDay > v_numTotalTaskInMinutes then
						
                     v_dtStartDate := v_dtStartDate+CAST(v_numTotalTaskInMinutes || 'minute' as interval);
                  ELSE
                     v_dtStartDate := CAST(TO_CHAR(v_dtStartDate+INTERVAL '1 day'+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),'yyyymmdd') AS TIMESTAMP)+v_tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval);
                  end if;
                  v_numTotalTaskInMinutes := v_numTotalTaskInMinutes -v_numTimeLeftForDay;
               ELSE
                  v_dtStartDate := CAST(TO_CHAR(v_dtStartDate+INTERVAL '1 day'+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),'yyyymmdd') AS TIMESTAMP)+v_tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval);
               end if;
            ELSE
               v_dtStartDate := CAST(TO_CHAR(v_dtStartDate+INTERVAL '1 day'+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),'yyyymmdd') AS TIMESTAMP)+v_tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval);
            end if;
         END LOOP;
      end if;
      UPDATE tt_TEMPTASKS SET dtFinishDate = v_dtStartDate WHERE ID = v_i;
      UPDATE tt_TEMPTASKASSIGNEE SET numLastTaskID = v_numTempTaskID,dtLastTaskCompletionTime = v_dtStartDate WHERE numWorkOrderID = v_numWorkOrderID AND numAssignedTo = v_numTaskAssignee;
      v_i := v_i::bigint+1;
   END LOOP;

   UPDATE
   tt_TEMPDATA TD
   SET
   start_date =(CASE WHEN TD.start_date IS NULL THEN TT.dtStartDate+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval) ELSE TD.start_date END),end_date =(CASE WHEN TD.end_date IS NULL THEN TT.dtFinishDate+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval) ELSE TD.end_date END),progress =(CASE WHEN coalesce(TT.numQtyItemsReq,0) > 0 THEN(coalesce(TT.numProcessedQty,0)*100)/TT.numQtyItemsReq ELSE 0 END)
   FROM
   tt_TEMPTASKS TT WHERE TD.numTaskID = TT.numTaskID;


   UPDATE
   tt_TEMPDATA
   SET
   color =(CASE
   WHEN (NULLIF(dtItemReleaseDate,'') IS NOT NULL AND dtProjectedFinish > dtItemReleaseDate) OR (NULLIF(dtRequestedFinish,'') IS NOT NULL AND dtProjectedFinish > dtRequestedFinish)
   THEN '#f65a5b'
   ELSE '#2ecc71'
   END),progressColor =(CASE
   WHEN (NULLIF(dtItemReleaseDate,'') IS NOT NULL AND dtProjectedFinish > dtItemReleaseDate) OR (NULLIF(dtRequestedFinish,'') IS NOT NULL AND dtProjectedFinish > dtRequestedFinish)
   THEN '#f91e1f'
   ELSE '#02ce58'
   END)
   WHERE
   numWOID > 0
   AND parent = '0';

   open SWV_RefCur for
   SELECT id AS "id",
      text AS "text",
      start_date AS "start_date",
      end_date AS "end_date",
      duration AS "duration",
      progress AS "progress",
      sortorder AS "sortorder",
      parent AS "parent",
      open AS "open",
      color AS "color",
      progressColor AS "progressColor",
      numWOID AS "numWOID",
      vcMilestone AS "vcMilestone",
      numStageDetailsId AS "numStageDetailsId",
      numTaskID AS "numTaskID",
      vcItem AS "vcItem",
      dtRequestedFinish AS "dtRequestedFinish",
      dtProjectedFinish AS "dtProjectedFinish",
      dtItemReleaseDate AS "dtItemReleaseDate",
      dtActualStartDate AS "dtActualStartDate",
      vcDurationHours AS "vcDurationHours" FROM tt_TEMPDATA;

   open SWV_RefCur2 for
   SELECT * FROM tt_TEMPLINKS;
   RETURN;
END; $$;


