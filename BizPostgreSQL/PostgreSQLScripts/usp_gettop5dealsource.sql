-- Stored procedure definition script usp_GetTop5DealSource for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetTop5DealSource(         
--      
v_numDomainID NUMERIC,      
 v_dtDateFrom TIMESTAMP,      
 v_dtDateTo TIMESTAMP,          
 v_numUserCntID NUMERIC DEFAULT 0,      
 v_tintRights SMALLINT DEFAULT 3,      
 v_intType NUMERIC DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintRights = 3 then --All Records      
 
      IF v_intType = 0 then
   
         open SWV_RefCur for
         SELECT  LD.vcData, SUM(OM.monPAmount) As Amount
         FROM OpportunityMaster OM INNER JOIN Listdetails LD ON LD.numListItemID = OM.tintSource
         WHERE
         OM.tintoppstatus = 1  and OM.tintopptype = 1
         AND OM.bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND OM.numDomainId = v_numDomainID
         GROUP BY LD.vcData
         ORDER BY Amount DESC LIMIT 5;
      ELSE
         open SWV_RefCur for
         SELECT  LD.vcData, SUM(OM.monPAmount) As Amount
         FROM AdditionalContactsInformation AI
         INNER JOIN OpportunityMaster OM ON AI.numContactId = OM.numrecowner
         INNER JOIN Listdetails LD ON LD.numListItemID = OM.tintSource
         WHERE
         OM.tintoppstatus = 1  and OM.tintopptype = 1
         AND OM.bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND OM.numDomainId = v_numDomainID
         AND AI.numTeam is not null
         AND AI.numTeam in(select F.numTeam from ForReportsByTeam F
            where F.numUserCntID = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType)
         GROUP BY LD.vcData
         ORDER BY Amount DESC LIMIT 5;
      end if;
   end if;      
       
   IF v_tintRights = 2 then --Territory Records      
 
      IF v_intType = 0 then
   
         open SWV_RefCur for
         SELECT  LD.vcData, SUM(OM.monPAmount) As Amount
         FROM OpportunityMaster OM
         INNER JOIN Listdetails LD ON LD.numListItemID = OM.tintSource
         INNER JOIN DivisionMaster DM ON DM.numDivisionID = OM.numDivisionId
         WHERE
         OM.tintoppstatus = 1  and OM.tintopptype = 1
         AND OM.bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND OM.numDomainId = v_numDomainID
         AND DM.numTerID in(SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID = v_numUserCntID)
         GROUP BY LD.vcData
         ORDER BY Amount DESC LIMIT 5;
      ELSE
         open SWV_RefCur for
         SELECT  LD.vcData, SUM(OM.monPAmount) As Amount
         FROM AdditionalContactsInformation AI
         INNER JOIN OpportunityMaster OM ON AI.numContactId = OM.numrecowner
         INNER JOIN Listdetails LD ON LD.numListItemID = OM.tintSource
         INNER JOIN DivisionMaster DM ON DM.numDivisionID = OM.numDivisionId
         WHERE
         OM.tintoppstatus = 1 and OM.tintopptype = 1
         AND OM.bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND OM.numDomainId = v_numDomainID
         AND DM.numTerID in(SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID = v_numUserCntID)
         AND AI.numTeam is not null
         AND AI.numTeam in(select F.numTeam from ForReportsByTeam F
            where F.numUserCntID = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType)
         GROUP BY LD.vcData
         ORDER BY Amount DESC LIMIT 5;
      end if;
   end if;      
       
   IF v_tintRights = 1 then --Owner Records      
 
      IF v_intType = 0 then
   
         open SWV_RefCur for
         SELECT  LD.vcData, SUM(OM.monPAmount) As Amount
         FROM OpportunityMaster OM
         INNER JOIN Listdetails LD ON LD.numListItemID = OM.tintSource
         INNER JOIN DivisionMaster DM ON DM.numDivisionID = OM.numDivisionId
         WHERE
         OM.tintoppstatus = 1   and OM.tintopptype = 1
         AND OM.bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND OM.numDomainId = v_numDomainID
         AND OM.numrecowner = v_numUserCntID
         GROUP BY LD.vcData
         ORDER BY Amount DESC LIMIT 5;
      ELSE
         open SWV_RefCur for
         SELECT  LD.vcData, SUM(OM.monPAmount) As Amount
         FROM AdditionalContactsInformation AI
         INNER JOIN OpportunityMaster OM ON AI.numContactId = OM.numrecowner
         INNER JOIN Listdetails LD ON LD.numListItemID = OM.tintSource
         INNER JOIN DivisionMaster DM ON DM.numDivisionID = OM.numDivisionId
         WHERE
         OM.tintoppstatus = 1  and OM.tintopptype = 1
         AND OM.bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND OM.numDomainId = v_numDomainID
         AND OM.numrecowner = v_numUserCntID
         AND AI.numTeam is not null
         AND AI.numTeam in(select F.numTeam from ForReportsByTeam F
            where F.numUserCntID = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType)
         GROUP BY LD.vcData
         ORDER BY Amount DESC LIMIT 5;
      end if;
   end if;
   RETURN;
END; $$;


