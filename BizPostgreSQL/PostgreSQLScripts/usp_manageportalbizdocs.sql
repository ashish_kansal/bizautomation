-- Stored procedure definition script USP_ManagePortalBizDocs for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManagePortalBizDocs(v_strFiledId VARCHAR(4000) DEFAULT '',
v_numDomainID NUMERIC(9,0) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   delete from PortalBizDocs   WHERE numDomainID = v_numDomainID;
   insert into PortalBizDocs(numBizDocID,numDomainID)
   SELECT Id,v_numDomainID FROM SplitIDs(v_strFiledId,',');
RETURN;
END; $$;


