CREATE OR REPLACE FUNCTION USP_ValidateFinancialYearClosingDate(v_numDomainID NUMERIC,
v_dtEntryDate TIMESTAMP DEFAULT NULL, -- pass date on which journal will be passed to accounting 
v_tintRecordType SMALLINT DEFAULT 0,
v_numRecordID NUMERIC DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintRecordType = 0 then --General for Date
	
      IF EXISTS(SELECT * FROM FinancialYear WHERE numDomainId = v_numDomainID AND (coalesce(bitCloseStatus,false) = true OR coalesce(bitAuditStatus,false) = true)
      AND v_dtEntryDate BETWEEN dtPeriodFrom AND dtPeriodTo) then
			
         RAISE EXCEPTION 'FY_CLOSED';
         RETURN;
      end if;
   ELSEIF v_tintRecordType = 1
   then --For Item Delete
	
      IF EXISTS(SELECT GJD.numJournalId FROM General_Journal_Header GJH JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId
      JOIN FinancialYear FY ON GJH.numDomainId = FY.numDomainId WHERE GJH.numDomainId = v_numDomainID AND GJD.numItemID = v_numRecordID AND (coalesce(FY.bitCloseStatus,false) = true OR coalesce(FY.bitAuditStatus,false) = true)
      AND GJH.datEntry_Date BETWEEN FY.dtPeriodFrom AND FY.dtPeriodTo) then
			
         RAISE EXCEPTION 'FY_CLOSED';
         RETURN;
      end if;
   ELSEIF v_tintRecordType = 2
   then --For Opportunity Delete
	
      IF EXISTS(SELECT GJD.numJournalId FROM General_Journal_Header GJH JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId
      JOIN FinancialYear FY ON GJH.numDomainId = FY.numDomainId WHERE GJH.numDomainId = v_numDomainID AND GJH.numOppId = v_numRecordID AND (coalesce(FY.bitCloseStatus,false) = true OR coalesce(FY.bitAuditStatus,false) = true)
      AND GJH.datEntry_Date BETWEEN FY.dtPeriodFrom AND FY.dtPeriodTo) then
			
         RAISE EXCEPTION 'FY_CLOSED';
         RETURN;
      end if;
   end if;
END; $$;

