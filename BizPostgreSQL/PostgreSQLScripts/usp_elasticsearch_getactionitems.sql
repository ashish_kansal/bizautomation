DROP FUNCTION IF EXISTS USP_ElasticSearch_GetActionItems;
CREATE OR REPLACE FUNCTION USP_ElasticSearch_GetActionItems(v_numDomainID NUMERIC(18,0)
	,v_vcActionItemIds TEXT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   numCommId AS id
		,'actionitem' AS module
		,coalesce(Communication.numCreatedBy,0) AS "numRecOwner"
		,coalesce(Communication.numAssign,0) AS "numAssignedTo"
		,coalesce(DivisionMaster.numTerID,0) AS "numTerID"
		,CONCAT('<b style="color:#9900cc">Action Item',CASE WHEN LENGTH(Listdetails.vcData) > 0 THEN CONCAT(' (',Listdetails.vcData,')') ELSE '' END,':</b> ',
   vcCompanyName,', ',coalesce(Communication.textDetails,'')) AS displaytext
		,coalesce(Communication.textDetails,'') AS text
		,CONCAT('/admin/actionitemdetails.aspx?CommId=',numCommId) AS url
		,coalesce(vcCompanyName,'') AS "Search_vcCompanyName"
		,coalesce(AdditionalContactsInformation.vcFirstName,'') AS "Search_vcFirstName"
		,coalesce(AdditionalContactsInformation.vcLastname,'') AS "Search_vcLastName"
		,coalesce(Listdetails.vcData,'') AS "Search_vcActionItemType"
		,coalesce(Communication.textDetails,'') AS "Search_textDetails"
   FROM
   Communication
   INNER JOIN
   DivisionMaster
   ON
   Communication.numDivisionID = DivisionMaster.numDivisionID
   INNER JOIN
   CompanyInfo
   ON
   DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
   LEFT JOIN
   AdditionalContactsInformation
   ON
   Communication.numContactId = AdditionalContactsInformation.numContactId
   LEFT JOIN
   Listdetails
   ON
   Listdetails.numListID = 73
   AND Communication.bitTask = Listdetails.numListItemID
   WHERE
   Communication.numDomainID = v_numDomainID
   AND coalesce(bitClosedFlag,false) = false
   AND (NULLIF(v_vcActionItemIds,'') IS NULL OR Communication.numCommId IN(SELECT id FROM SplitIDs(coalesce(v_vcActionItemIds,'0'),',')));

   IF v_vcActionItemIds IS NULL then
	
      DELETE FROM ElasticSearchModifiedRecords WHERE numDomainID = v_numDomainID AND vcModule = 'ActionItem';
   end if;
   RETURN;
END; $$;
