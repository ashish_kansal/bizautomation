-- Stored procedure definition script USP_GetBizDocInventoryItems for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetBizDocInventoryItems(v_numOppBizDocID NUMERIC(9,0)  ,
                v_numDomainID     NUMERIC(9,0)  DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
--                @numOppId        AS NUMERIC(9)  = NULL,
   AS $$
BEGIN
   open SWV_RefCur for
   SELECT
   BI.numOppBizDocItemID,
			I.numItemCode,
			WI.numWareHouseID,
			I.vcItemName,
           I.vcModelID,
           coalesce(I.fltWeight,0) AS fltWeight,
           coalesce(I.fltHeight,0) AS fltHeight,
           coalesce(I.fltWidth,0) AS fltWidth,
           coalesce(I.fltLength,0) AS fltLength,
           0 AS intNoOfBox,
           BI.vcItemDesc,
           (coalesce(BI.numUnitHour,0)*coalesce(fltWeight,0)) AS TotalWeight,
		   coalesce(BI.numUnitHour,0) AS numUnitHour,
		   coalesce(I.bitContainer,false) AS bitContainer,coalesce(I.numNoItemIntoContainer,0) AS numNoItemIntoContainer,
		   coalesce(I.numContainer,0) AS numContainer
   FROM
   OpportunityBizDocItems AS BI
   INNER JOIN
   Item AS I
   ON
   BI.numItemCode = I.numItemCode
   INNER JOIN
   WareHouseItems WI
   ON
   BI.numWarehouseItmsID = WI.numWareHouseItemID
   WHERE
   BI.numOppBizDocID = v_numOppBizDocID
   AND I.charItemType = 'P' --Only Inventory Items
   AND I.numDomainID = v_numDomainID
   AND coalesce((SELECT bitDropShip FROM OpportunityItems WHERE numoppitemtCode = BI.numOppItemID AND numItemCode = I.numItemCode),false) = false;
           
	-- GET CONTAINER BY WAREHOUSE AND NOOFITEMSCANBEADDED
   open SWV_RefCur2 for
   SELECT
   T1.numContainer
		,T1.numWareHouseID
		,T1.numNoItemIntoContainer
		,T2.numTotalContainer
		,false AS bitItemAdded
		,T2.fltWeight
		,T2.fltHeight
		,T2.fltWidth
		,T2.fltLength
   FROM(SELECT
      I.numContainer
			,WI.numWareHouseID
			,I.numNoItemIntoContainer
      FROM
      OpportunityBizDocItems OBI
      INNER JOIN
      Item AS I
      ON
      OBI.numItemCode = I.numItemCode
      INNER JOIN
      WareHouseItems WI
      ON
      OBI.numWarehouseItmsID = WI.numWareHouseItemID
      WHERE
      numOppBizDocID = v_numOppBizDocID
      AND I.charItemType = 'P'
      AND coalesce(numContainer,0) > 0
      GROUP BY
      I.numContainer,WI.numWareHouseID,I.numNoItemIntoContainer) AS T1
   INNER JOIN(SELECT
      I.numItemCode
			,WI.numWareHouseID
			,SUM(OBI.numUnitHour) AS numTotalContainer
			,coalesce(I.fltWeight,0) AS fltWeight
			,coalesce(I.fltHeight,0) AS fltHeight
			,coalesce(I.fltWidth,0) AS fltWidth
			,coalesce(I.fltLength,0) AS fltLength
      FROM
      OpportunityBizDocItems OBI
      INNER JOIN
      Item AS I
      ON
      OBI.numItemCode = I.numItemCode
      INNER JOIN
      WareHouseItems WI
      ON
      OBI.numWarehouseItmsID = WI.numWareHouseItemID
      WHERE
      numOppBizDocID = v_numOppBizDocID
      AND I.charItemType = 'P'
      AND coalesce(I.bitContainer,false) = true
      GROUP BY
      I.numItemCode,WI.numWareHouseID,coalesce(I.fltWeight,0),coalesce(I.fltHeight,0),
      coalesce(I.fltWidth,0),coalesce(I.fltLength,0)) AS T2
   ON
   T1.numContainer = T2.numItemCode;
   RETURN;
END; $$;


