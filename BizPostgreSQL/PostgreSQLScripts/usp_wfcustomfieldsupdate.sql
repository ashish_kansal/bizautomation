-- Stored procedure definition script USP_WFCustomFieldsUpdate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,,10Oct2014>
-- Description:	<Description,,Work Flow Action :Update fields >
-- =============================================
Create or replace FUNCTION USP_WFCustomFieldsUpdate(v_numRecordID NUMERIC(18,0),
	v_numFormID NUMERIC(18,0),
	v_Fld_ID  NUMERIC(18,0),
	v_Fld_Value VARCHAR(1000),
	INOUT v_boolInsert BOOLEAN)
LANGUAGE plpgsql
	-- Add the parameters for the stored procedure here
   AS $$
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	

    -- Insert statements for procedure here
   IF (v_numFormID = 68) then--Organization
			
      IF Not Exists(Select 'col1' from CFW_FLD_Values where RecId = v_numRecordID and Fld_ID = v_Fld_ID) then
						
         INSERT INTO CFW_FLD_Values(Fld_ID, Fld_Value, RecId)
										VALUES(v_Fld_ID,v_Fld_Value,v_numRecordID);
								
         v_boolInsert := false;
      ELSE
         v_boolInsert := true;
      end if;
   ELSEIF (v_numFormID = 69)
   then--contacts
			
      IF Not Exists(Select 'col1' from CFW_FLD_Values_Cont where RecId = v_numRecordID and fld_id = v_Fld_ID) then
								
         INSERT INTO CFW_FLD_Values_Cont(fld_id, Fld_Value, RecId)
												VALUES(v_Fld_ID,v_Fld_Value,v_numRecordID);
										
         v_boolInsert := false;
      ELSE
         v_boolInsert := true;
      end if;
   ELSEIF (v_numFormID = 70)
   then--Ordres
			
      IF Not Exists(Select 'col1' from CFW_Fld_Values_Opp where RecId = v_numRecordID and Fld_ID = v_Fld_ID) then
								
         INSERT INTO CFW_Fld_Values_Opp(Fld_ID, Fld_Value, RecId)
												VALUES(v_Fld_ID,v_Fld_Value,v_numRecordID);
										
         v_boolInsert := false;
      ELSE
         v_boolInsert := true;
      end if;
   ELSEIF (v_numFormID = 72)
   then--Cases
			
      IF Not Exists(Select 'col1' from CFW_FLD_Values_Case where RecId = v_numRecordID and Fld_ID = v_Fld_ID) then
								
         INSERT INTO CFW_FLD_Values_Case(Fld_ID, Fld_Value, RecId)
												VALUES(v_Fld_ID,v_Fld_Value,v_numRecordID);
										
         v_boolInsert := false;
      ELSE
         v_boolInsert := true;
      end if;
   ELSEIF (v_numFormID = 73)
   then--Projects
			
      IF Not Exists(Select 'col1' from CFW_Fld_Values_Pro where RecId = v_numRecordID and Fld_ID = v_Fld_ID) then
								
         INSERT INTO CFW_Fld_Values_Pro(Fld_ID, Fld_Value, RecId)
												VALUES(v_Fld_ID,v_Fld_Value,v_numRecordID);
										
         v_boolInsert := false;
      ELSE
         v_boolInsert := true;
      end if;
   ELSEIF (v_numFormID = 94)
   then--Sales/Stage/Business Process
			
      IF Not Exists(Select 'col1' from CFW_Fld_Values_Opp where RecId = v_numRecordID and Fld_ID = v_Fld_ID) then
								
         INSERT INTO CFW_Fld_Values_Opp(Fld_ID, Fld_Value, RecId)
												VALUES(v_Fld_ID,v_Fld_Value,v_numRecordID);
										
         v_boolInsert := false;
      ELSE
         v_boolInsert := true;
      end if;
   ELSEIF (v_numFormID = 124)
   then--Action Items
			
      IF Not Exists(Select 'col1' from CFW_Fld_Values_Opp where RecId = v_numRecordID and Fld_ID = v_Fld_ID) then
								
         INSERT INTO CFW_Fld_Values_Opp(Fld_ID, Fld_Value, RecId)
												VALUES(v_Fld_ID,v_Fld_Value,v_numRecordID);
										
         v_boolInsert := false;
      ELSE
         v_boolInsert := true;
      end if;
   ELSE
      IF Not Exists(Select 'col1' from CFW_FLD_Values where RecId = v_numRecordID and Fld_ID = v_Fld_ID) then
						
         INSERT INTO CFW_FLD_Values(Fld_ID, Fld_Value, RecId)
										VALUES(v_Fld_ID,v_Fld_Value,v_numRecordID);
								
         v_boolInsert := false;
      ELSE
         v_boolInsert := true;
      end if;
   end if;
   RETURN;
END; $$;


