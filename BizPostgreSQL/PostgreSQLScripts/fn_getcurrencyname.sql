-- Function definition script fn_GetCurrencyName for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetCurrencyName(v_numCurrencyID NUMERIC)
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_varCurrSymbol  VARCHAR(100);
BEGIN
   select   coalesce(varCurrSymbol,'') INTO v_varCurrSymbol from Currency where numCurrencyID = v_numCurrencyID;

   return coalesce(v_varCurrSymbol,'-');
END; $$;

