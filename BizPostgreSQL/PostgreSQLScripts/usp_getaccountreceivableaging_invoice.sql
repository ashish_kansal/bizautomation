-- Stored procedure definition script USP_GetAccountReceivableAging_Invoice for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetAccountReceivableAging_Invoice(v_numDomainId NUMERIC(9,0) DEFAULT 0,
	v_numDivisionId NUMERIC(9,0) DEFAULT NULL,
	v_vcFlag VARCHAR(20) DEFAULT NULL,
	v_numAccountClass NUMERIC(9,0) DEFAULT 0,
	v_dtFromDate DATE DEFAULT NULL,
	v_dtToDate DATE DEFAULT NULL,
	v_ClientTimeZoneOffset INTEGER DEFAULT 0,
	v_vcRegularSearch TEXT DEFAULT '',
	v_bitCustomerStatement BOOLEAN DEFAULT false,
	v_tintBasedOn SMALLINT DEFAULT 1, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDefaultARAccount  NUMERIC(18,0);
   v_AuthoritativeSalesBizDocId  INTEGER;
   v_baseCurrency  NUMERIC;
   v_strSql  TEXT;
   v_strSql1  TEXT;
   v_strCredit  TEXT;
BEGIN
   v_numDefaultARAccount := coalesce((SELECT numAccountID FROM AccountingCharges WHERE numDomainID = v_numDomainId AND numChargeTypeId = 4),0);

   select   coalesce(numAuthoritativeSales,0) INTO v_AuthoritativeSalesBizDocId FROM   AuthoritativeBizDocs WHERE  numDomainId = v_numDomainId;--287
    ------------------------------------------      
   select   numCurrencyID INTO v_baseCurrency FROM Domain WHERE numDomainId = v_numDomainId;
	/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */
	------------------------------------------      
   IF v_dtFromDate IS NULL then
      v_dtFromDate := '1990-01-01 00:00:00.000';
   end if;
   IF v_dtToDate IS NULL then
      v_dtToDate := TIMEZONE('UTC',now());
   end if;

   v_numDivisionId := coalesce(v_numDivisionId,0);

   v_strCredit := '';
    
    
   v_strSql := ' DROP TABLE IF EXISTS tt_TempRecords CASCADE; CREATE TEMPORARY TABLE tt_TempRecords AS SELECT DISTINCT OM.numOppId,
                        OM.vcPOppName,
                        OM.tintOppType,
                        OB.numOppBizDocsId,
                        OB.vcBizDocID,
                        COALESCE(OB.monDealAmount * OM.fltExchangeRate,0) TotalAmount,
                        (COALESCE(TablePayments.monPaidAmount,0) * OM.fltExchangeRate) AmountPaid,
                        COALESCE(OB.monDealAmount
                                 * OM.fltExchangeRate
                                 - COALESCE(TablePayments.monPaidAmount,0)
                                     * OM.fltExchangeRate,0) BalanceDue,
						FormatedDateFromDate(ob.dtFromDate,' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ') dtDate,
						CASE COALESCE(bitBillingTerms,false) 
                          WHEN true THEN OB.dtFromDate + make_interval(days => COALESCE((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = COALESCE(intBillingDays,0)), 0))
                          WHEN false THEN ob.dtFromDate
                        END AS dtDueDate,
                        CASE COALESCE(bitBillingTerms,false) 
                          WHEN true THEN FormatedDateFromDate(OB.dtFromDate + make_interval(days => COALESCE((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = COALESCE(intBillingDays,0)), 0)),
                                                                   ' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ')
                          WHEN false THEN FormatedDateFromDate(ob.dtFromDate,' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ')
                        END AS DueDate,
                        bitBillingTerms,
                        intBillingDays,
                        OB.dtCreatedDate,
                        COALESCE(C.varCurrSymbol,'''') varCurrSymbol,CO.vcCompanyName,ADC.vcFirstname || '' ''|| ADC.vcLastName AS vcContactName,
                           fn_GetContactName(OM.numRecOwner) AS RecordOwner,
						   fn_GetContactName(OM.numAssignedTo) AS AssignedTo
						   ,CASE WHEN ADC.numPhone IS NULL THEN '''' ELSE ADC.numPhone || (CASE WHEN numPhoneExtension IS NULL THEN '''' WHEN numPhoneExtension='''' THEN '''' ELSE '','' || ADC.numPhoneExtension END) END AS vcCustPhone,
						   OM.numCurrencyID,OB.vcRefOrderNo,FormatedDateFromDate(ob.dtFromDate,' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ') AS dtFromDate,
						    -1 AS numJournal_Id,-1 as numReturnHeaderID,
							CASE WHEN COALESCE(BT.numDiscount,0) > 0 AND COALESCE(BT.numInterestPercentage,0) > 0 
								 THEN ' || '''-''||' || 'COALESCE(BT.numDiscount,0)::VARCHAR ||' || '''% if paid ''||' || ' COALESCE(BT.numDiscountPaidInDays,0)::VARCHAR ||' || ''' days early / ''||' || ' COALESCE(BT.numInterestPercentage,0)::VARCHAR ||' || '''% if paid ''||' || ' COALESCE(BT.numInterestPercentageIfPaidAfterDays,0)::VARCHAR ||' || ''' days or later ''' ||
   ' WHEN COALESCE(BT.numDiscount,0) > 0 THEN ' || '''-''||' || 'COALESCE(BT.numDiscount,0)::VARCHAR ||' || '''% if paid ''||' || ' COALESCE(BT.numDiscountPaidInDays,0)::VARCHAR ||' || ''' days early ''' ||
   ' WHEN COALESCE(BT.numInterestPercentage,0) > 0  THEN  COALESCE(BT.numInterestPercentage,0)::VARCHAR ||' || '''% if paid ''||' || ' COALESCE(BT.numInterestPercentageIfPaidAfterDays,0)::VARCHAR ||' || ''' days or later ''' ||
   ' END AS vcTerms,
							COALESCE(BT.numDiscount,0) AS numDiscount,
							COALESCE(BT.numInterestPercentage,0) AS numInterest,
							0 AS tintLevel
							,COALESCE(OB.numARAccountID,' || SUBSTR(CAST(v_numDefaultARAccount AS VARCHAR(30)),1,30) || ') AS numChartAcntId              
        FROM   OpportunityMaster OM
               INNER JOIN DivisionMaster DM
                 ON OM.numDivisionId = DM.numDivisionID
                  INNER JOIN CompanyInfo CO ON CO.numCompanyId = DM.numCompanyID
                  LEFT JOIN AdditionalContactsInformation ADC ON ADC.numDivisionId=DM.numDivisionID AND ADC.numContactId=OM.numContactId
               LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN Currency C ON OM.numCurrencyID = C.numCurrencyID
			   LEFT JOIN BillingTerms AS BT ON OM.intBillingDays = BT.numTermsID 
			   LEFT JOIN LATERAL
			   (
					SELECT 
						SUM(monAmountPaid) AS monPaidAmount
					FROM
						DepositeDetails DD
					INNER JOIN
						DepositMaster DM
					ON
						DD.numDepositID=DM.numDepositId
					WHERE 
						numOppID = OM.numOppID
						AND numOppBizDocsID=OB.numOppBizDocsID
						AND 1 = (CASE WHEN COALESCE(DM.numReturnHeaderID,0) > 0 THEN (CASE WHEN (DD.dtCreatedDate + make_interval(mins => -1 * ' || SUBSTR(CAST(v_ClientTimeZoneOffset AS VARCHAR(30)),1,30) || '))::DATE <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(30)),1,30) || ''' THEN 1 ELSE 0 END) ELSE (CASE WHEN DM.dtDepositDate::DATE <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(30)),1,30) || ''' THEN 1 ELSE 0 END) END)
				) TablePayments ON TRUE
        WHERE  OM.tintOppType = 1
               AND OM.tintOppStatus = 1
               AND om.numDomainId = ' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || '
               AND OB.numBizDocId = ' || CAST(coalesce(v_AuthoritativeSalesBizDocId,0) AS VARCHAR(15)) || ' 
			   AND (OM.numDivisionId = ' || SUBSTR(CAST(v_numDivisionId AS VARCHAR(30)),1,30) || ' OR ' || SUBSTR(CAST(v_numDivisionId AS VARCHAR(30)),1,30) || ' = 0)
				AND (OM.numAccountClass= ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || ' OR ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || '=0)
				AND ' ||(CASE
   WHEN coalesce(v_bitCustomerStatement,false) = true
   THEN 'OB.dtCreatedDate::DATE'
   ELSE '(dtFromDate + make_interval(days => CASE WHEN OM.bitBillingTerms = true AND ' || SUBSTR(CAST(v_tintBasedOn AS VARCHAR(30)),1,30) || '=1
								 THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = COALESCE(OM.intBillingDays,0)), 0)
								 ELSE 0 
									END))::DATE'
   END) || ' <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(30)),1,30) || '''
               AND OB.bitAuthoritativeBizDocs=1
               AND COALESCE(OB.tintDeferred,0) <> 1
			   AND COALESCE(OB.monDealAmount
                                 * OM.fltExchangeRate
                                 - COALESCE(TablePayments.monPaidAmount,0)
                                     * OM.fltExchangeRate,0) <> 0';     
       
   v_strSql1 := '  UNION 
		 SELECT 
				-1 numOppId,
				''Journal'' vcPOppName,
				0 tintOppType,
				0 numOppBizDocsId,
				''Journal'' vcBizDocID,
				CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END TotalAmount,
				0 AmountPaid,
				CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END BalanceDue,
				FormatedDateFromDate(GJH.datEntry_Date,' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ') AS dtDate,
				GJH.datEntry_Date AS dtDueDate,
				FormatedDateFromDate(GJH.datEntry_Date,' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ') AS DueDate,
				false bitBillingTerms,
				0 intBillingDays,
				GJH.datCreatedDate,
                COALESCE(C.varCurrSymbol, '''') varCurrSymbol
                ,CO.vcCompanyName,ADC.vcFirstname || '' ''|| ADC.vcLastName AS vcContactName,
                           '''' AS RecordOwner,
						   '''' AS AssignedTo
						   ,CASE WHEN ADC.numPhone IS NULL THEN '''' ELSE ADC.numPhone || (CASE WHEN numPhoneExtension IS NULL THEN '''' WHEN numPhoneExtension='''' THEN '''' ELSE '','' || ADC.numPhoneExtension END) END AS vcCustPhone,
						   GJD.numCurrencyID,'''' as vcRefOrderNo,NULL, COALESCE(GJH.numJournal_Id,0) AS numJournal_Id,-1 as numReturnHeaderID,
							CASE WHEN COALESCE(BT.numDiscount,0) > 0 AND COALESCE(BT.numInterestPercentage,0) > 0 
								 THEN ' || '''-''||' || 'COALESCE(BT.numDiscount,0)::VARCHAR ||' || '''% if paid ''||' || ' COALESCE(BT.numDiscountPaidInDays,0)::VARCHAR ||' || ''' days early / ''||' || ' COALESCE(BT.numInterestPercentage,0) ||' || '''% if paid ''||' || ' COALESCE(BT.numInterestPercentageIfPaidAfterDays,0)::VARCHAR ||' || ''' days or later ''' ||
   ' WHEN COALESCE(BT.numDiscount,0) > 0 THEN ' || '''-''||' || 'COALESCE(BT.numDiscount,0)::VARCHAR ||' || '''% if paid ''||' || ' COALESCE(BT.numDiscountPaidInDays,0)::VARCHAR ||' || ''' days early ''' ||
   ' WHEN COALESCE(BT.numInterestPercentage,0) > 0  THEN COALESCE(BT.numInterestPercentage,0)::VARCHAR ||' || '''% if paid ''||' || ' COALESCE(BT.numInterestPercentageIfPaidAfterDays,0)::VARCHAR ||' || ''' days or later ''' ||
   ' END AS vcTerms,
							COALESCE(BT.numDiscount,0) AS numDiscount,
							COALESCE(BT.numInterestPercentage,0) AS numInterest,
							0 AS tintLevel,
							GJD.numChartAcntId
		 FROM   General_Journal_Header GJH
				INNER JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
															  AND GJH.numDomainId = GJD.numDomainId
				INNER JOIN Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
				INNER JOIN DivisionMaster DM ON GJD.numCustomerID = DM.numDivisionID
				INNER JOIN CompanyInfo CO ON CO.numCompanyId = DM.numCompanyID
                Left JOIN AdditionalContactsInformation ADC ON ADC.numContactId=GJD.numContactId 
				LEFT OUTER JOIN Currency C ON GJD.numCurrencyID = C.numCurrencyID
				LEFT JOIN BillingTerms AS BT ON DM.numBillingDays = BT.numTermsID
		 WHERE  GJH.numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || '
				AND COA.vcAccountCode LIKE ''01010105%'' 
				AND COALESCE(numOppId,0)=0 
				AND COALESCE(numOppBizDocsId,0)=0 
				AND COALESCE(GJH.numDepositID,0)=0 
				AND COALESCE(GJH.numReturnID,0)=0
				AND (GJD.numCustomerId = ' || SUBSTR(CAST(v_numDivisionId AS VARCHAR(30)),1,30) || ' OR ' || SUBSTR(CAST(v_numDivisionId AS VARCHAR(30)),1,30) || '  = 0)
				AND (GJH.numAccountClass= ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || ' OR ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || '=0)
				AND GJH.datEntry_Date::DATE <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(30)),1,30) || '''';
   
   v_strCredit := ' UNION SELECT -1 numOppId,
									CASE WHEN tintDepositePage=2 THEN ''Unapplied Payment'' ELSE ''Credit Memo'' END AS vcPOppName,
									0 tintOppType,
									0 numOppBizDocsId,
									''''  vcBizDocID,
									(COALESCE(monDepositAmount,0) - COALESCE(monAppliedAmount,0)) * -1 AS TotalAmount,
									0 AmountPaid,
									(COALESCE(monDepositAmount,0) - COALESCE(monAppliedAmount,0)) * -1 AS BalanceDue,
									FormatedDateFromDate(DM.dtDepositDate,' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ') AS dtDate,
									DM.dtDepositDate AS dtDueDate,
									FormatedDateFromDate(DM.dtDepositDate,' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ') AS DueDate,
									false bitBillingTerms,
									0 intBillingDays,
									DM.dtCreationDate AS datCreatedDate,
									'''' varCurrSymbol,
									fn_GetComapnyName(DM.numDivisionId) vcCompanyName,
									'''' vcContactName,
									'''' AS RecordOwner,
									'''' AS AssignedTo,
									'''' AS vcCustPhone,
									COALESCE(DM.numCurrencyID,0) AS numCurrencyID,
									'''' as vcRefOrderNo,
									NULL, 
									-1 AS numJournal_Id,COALESCE(numReturnHeaderID,0) as numReturnHeaderID,
									'''' AS vcTerms,
									0 AS numDiscount,
									0 AS numInterest,
									0 AS tintLeve,
									0 AS numChartAcntId
					FROM  DepositMaster DM 
					WHERE DM.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ' AND tintDepositePage IN(2,3)  
					AND ((COALESCE(monDepositAmount,0) - COALESCE(monAppliedAmount,0)) <> 0)
					AND (DM.numDivisionId = ' || SUBSTR(CAST(v_numDivisionId AS VARCHAR(30)),1,30) || ' OR ' || SUBSTR(CAST(v_numDivisionId AS VARCHAR(30)),1,30) || ' = 0)
					AND (DM.numAccountClass= ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || ' OR ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || '=0)
					AND DM.dtDepositDate::DATE <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(30)),1,30) || '''
					AND COALESCE(DM.numReturnHeaderID,0) = 0
					UNION SELECT -1 numOppId,
									CASE WHEN tintDepositePage=2 THEN ''Unapplied Payment'' ELSE ''Credit Memo'' END AS vcPOppName,
									0 tintOppType,
									0 numOppBizDocsId,
									''''  vcBizDocID,
									(COALESCE(monDepositAmount,0) - (CASE 
																		WHEN RH.tintReturnType=4 
																		THEN COALESCE(monAppliedAmount,0)
																		ELSE COALESCE((SELECT SUM(DD.monAmountPaid) FROM DepositeDetails DD WHERE DD.numDepositID=DM.numDepositId AND DD.dtCreatedDate + make_interval(mins => -1 * ' || SUBSTR(CAST(v_ClientTimeZoneOffset AS VARCHAR(30)),1,30) || ') <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(30)),1,30) || '''),0)
																	END)) * -1 AS TotalAmount,
									0 AmountPaid,
									(COALESCE(monDepositAmount,0) - (CASE 
																	WHEN RH.tintReturnType=4 
																	THEN COALESCE(monAppliedAmount,0)
																	ELSE COALESCE((SELECT SUM(DD.monAmountPaid) FROM DepositeDetails DD WHERE DD.numDepositID=DM.numDepositId AND DD.dtCreatedDate + make_interval(mins => -1 * ' || SUBSTR(CAST(v_ClientTimeZoneOffset AS VARCHAR(30)),1,30) || ') <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(30)),1,30) || '''),0)
																END)) * -1 AS BalanceDue,
									FormatedDateFromDate(DM.dtDepositDate,' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ') AS dtDate,
									DM.dtDepositDate AS dtDueDate,
									FormatedDateFromDate(DM.dtDepositDate,' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ') AS DueDate,
									false bitBillingTerms,
									0 intBillingDays,
									DM.dtCreationDate AS datCreatedDate,
									'''' varCurrSymbol,
									fn_GetComapnyName(DM.numDivisionId) vcCompanyName,
									'''' vcContactName,
									'''' AS RecordOwner,
									'''' AS AssignedTo,
									'''' AS vcCustPhone,
									COALESCE(DM.numCurrencyID,0) AS numCurrencyID,
									'''' as vcRefOrderNo,
									NULL, 
									-1 AS numJournal_Id,COALESCE(DM.numReturnHeaderID,0) as numReturnHeaderID,
									'''' AS vcTerms,
									0 AS numDiscount,
									0 AS numInterest,
									0 AS tintLeve,
									0 AS numChartAcntId
					FROM  DepositMaster DM
					LEFT JOIN	
						ReturnHeader RH
					ON
						DM.numReturnHeaderID = RH.numReturnHeaderID
					WHERE DM.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ' AND tintDepositePage IN(2,3)  
					AND CAST(COALESCE(monDepositAmount,0) - (CASE 
															WHEN RH.tintReturnType=4 
															THEN COALESCE(monAppliedAmount,0)
															ELSE COALESCE((SELECT SUM(DD.monAmountPaid) FROM DepositeDetails DD WHERE DD.numDepositID=DM.numDepositId AND DD.dtCreatedDate + make_interval(mins => -1 * ' || SUBSTR(CAST(v_ClientTimeZoneOffset AS VARCHAR(30)),1,30) || ') <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(30)),1,30) || '''),0)
														END) AS DECIMAL(18,2)) <> 0
					AND (DM.numDivisionId = ' || SUBSTR(CAST(v_numDivisionId AS VARCHAR(30)),1,30) || ' OR ' || SUBSTR(CAST(v_numDivisionId AS VARCHAR(30)),1,30) || ' = 0)
					AND (DM.numAccountClass= ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || ' OR ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || '=0)
					AND DM.dtDepositDate::DATE <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(30)),1,30) || '''
					AND COALESCE(DM.numReturnHeaderID,0) > 0
					UNION
					SELECT 
						-1 numOppId
						,''Refund'' AS vcPOppName
						,0 tintOppType
						,0 numOppBizDocsId
						,''''  vcBizDocID
						,(CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1 * GJD.numCreditAmt END) AS TotalAmount
						,0 AmountPaid
						,(CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1 * GJD.numCreditAmt END) AS BalanceDue
						,FormatedDateFromDate(GJH.datEntry_Date,' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ') AS dtDate
						,GJH.datEntry_Date AS dtDueDate
						,FormatedDateFromDate(GJH.datEntry_Date,' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ') AS DueDate
						,false bitBillingTerms
						,0 intBillingDays
						,RH.dtCreatedDate AS datCreatedDate
						,'''' varCurrSymbol
						,fn_GetComapnyName(DIV.numDivisionId) vcCompanyName
						,'''' vcContactName
						,'''' AS RecordOwner
						,'''' AS AssignedTo
						,'''' AS vcCustPhone
						,COALESCE(DIV.numCurrencyID,0) AS numCurrencyID
						,'''' as vcRefOrderNo
						,NULL
						,GJH.numJournal_Id AS numJournal_Id
						,COALESCE(numReturnHeaderID,0) as numReturnHeaderID
						,'''' AS vcTerms
						,0 AS numDiscount
						,0 AS numInterest
						,0 AS tintLevel
						,GJD.numChartAcntId
					 FROM   ReturnHeader RH JOIN General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
					 JOIN DivisionMaster AS DIV ON DIV.numDivisionID = RH.numDivisionId
					JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId 
					JOIN Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId AND COA.vcAccountCode LIKE ''0101010501''
					WHERE 
						RH.tintReturnType=4 
						AND RH.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(30)),1,30) || '  
						AND COALESCE(RH.numParentID,0) = 0
						AND COALESCE(RH.IsUnappliedPayment,false) = false
						AND (monBizDocAmount > 0) AND COALESCE(RH.numBillPaymentIDRef,0)=0
						AND (RH.numDivisionId = ' || SUBSTR(CAST(v_numDivisionId AS VARCHAR(30)),1,30) || ' OR ' || SUBSTR(CAST(v_numDivisionId AS VARCHAR(30)),1,30) || ' = 0)
						AND (RH.numAccountClass= ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || ' OR ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || '=0)
						AND GJH.datEntry_Date::DATE <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(30)),1,30) || '''';
	            	
   IF (v_vcFlag = '0+30') then
      
      v_strSql := coalesce(v_strSql,'') ||
      'AND OB.dtFromDate + make_interval(days => CASE 
                                 --WHEN bitBillingTerms = true THEN COALESCE(fn_GetListItemName(COALESCE(intBillingDays,0)),0)
                                 WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                 ELSE 0
                               END) BETWEEN GetUTCDateWithoutTime() AND public.GetUTCDateWithoutTime()+ interval ''31 day''
               AND GetUTCDateWithoutTime() <= OB.dtFromDate + make_interval(days => CASE 
                                                                --WHEN bitBillingTerms = true THEN COALESCE(fn_GetListItemName(COALESCE(intBillingDays,0)),0)
                                                                WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                                                ELSE 0
                                                              END)';
      v_strSql1 := coalesce(v_strSql1,'') || 'AND GJH.datEntry_Date BETWEEN GetUTCDateWithoutTime() AND public.GetUTCDateWithoutTime()+ interval ''31 day''';
      v_strCredit := coalesce(v_strCredit,'') || 'AND DM.dtDepositDate BETWEEN GetUTCDateWithoutTime() AND public.GetUTCDateWithoutTime()+ interval ''31 day''';
   ELSEIF (v_vcFlag = '30+60')
   then
        
      v_strSql := coalesce(v_strSql,'') ||
      'AND OB.dtFromDate + make_interval(days => CASE 
                                   --WHEN bitBillingTerms = true THEN COALESCE(fn_GetListItemName(COALESCE(intBillingDays,0)),0)
                                   WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                   ELSE 0
                                 END) BETWEEN public.GetUTCDateWithoutTime()+ interval ''31 day'' AND public.GetUTCDateWithoutTime()+ interval ''61 day''
                 AND GetUTCDateWithoutTime() <= OB.dtFromDate + make_interval(days => CASE 
                                                                  --WHEN bitBillingTerms = true THEN COALESCE(fn_GetListItemName(COALESCE(intBillingDays,0)),0)
                                                                  WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                                                  ELSE 0
                                                                END)';
      v_strSql1 := coalesce(v_strSql1,'') || 'AND GJH.datEntry_Date BETWEEN public.GetUTCDateWithoutTime()+ interval ''31 day'' AND public.GetUTCDateWithoutTime()+ interval ''61 day''';
      v_strCredit := coalesce(v_strCredit,'') || 'AND DM.dtDepositDate BETWEEN public.GetUTCDateWithoutTime()+ interval ''31 day'' AND public.GetUTCDateWithoutTime()+ interval ''61 day''';
   ELSEIF (v_vcFlag = '60+90')
   then
          
      v_strSql := coalesce(v_strSql,'') ||
      'AND OB.dtFromDate + make_interval(days => CASE 
                                     --WHEN bitBillingTerms = true THEN COALESCE(fn_GetListItemName(COALESCE(intBillingDays,0)),0)
                                     WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                     ELSE 0
                                   END) BETWEEN public.GetUTCDateWithoutTime()+ interval ''61 day'' AND public.GetUTCDateWithoutTime()+ interval ''91 day''
                   AND GetUTCDateWithoutTime() <= OB.dtFromDate + make_interval(days => CASE 
                                                                    --WHEN bitBillingTerms = true THEN COALESCE(fn_GetListItemName(COALESCE(intBillingDays,0)),0)
                                                                    WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                                                    ELSE 0
                                                                  END)';
      v_strSql1 := coalesce(v_strSql1,'') || 'AND GJH.datEntry_Date BETWEEN public.GetUTCDateWithoutTime()+ interval ''61 day'' AND public.GetUTCDateWithoutTime()+ interval ''91 day''';
      v_strCredit := coalesce(v_strCredit,'') || 'AND DM.dtDepositDate BETWEEN public.GetUTCDateWithoutTime()+ interval ''61 day'' AND public.GetUTCDateWithoutTime()+ interval ''91 day''';
   ELSEIF (v_vcFlag = '90+')
   then
            
      v_strSql := coalesce(v_strSql,'') ||
      'AND OB.dtFromDate + make_interval(days => CASE 
                                       --WHEN bitBillingTerms = true THEN COALESCE(fn_GetListItemName(COALESCE(intBillingDays,0)),0)
                                       WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                       ELSE 0
                                     END) > public.GetUTCDateWithoutTime()+ interval ''91 day''
                     AND GetUTCDateWithoutTime() <= OB.dtFromDate + make_interval(days => CASE 
                                                                      --WHEN bitBillingTerms = true THEN COALESCE(fn_GetListItemName(COALESCE(intBillingDays,0)),0)
                                                                      WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                                                      ELSE 0
                                                                    END)';
      v_strSql1 := coalesce(v_strSql1,'') || 'AND GJH.datEntry_Date  > public.GetUTCDateWithoutTime()+ interval ''91 day''';
      v_strCredit := coalesce(v_strCredit,'') || 'AND DM.dtDepositDate  > public.GetUTCDateWithoutTime()+ interval ''91 day''';
   ELSEIF (v_vcFlag = '0-30')
   then
              
      v_strSql := coalesce(v_strSql,'') ||
      'AND GetUTCDateWithoutTime() > OB.dtFromDate + make_interval(days => CASE 
                                                                       --WHEN bitBillingTerms = true THEN COALESCE(fn_GetListItemName(COALESCE(intBillingDays,0)),0)
                                                                       WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                                                       ELSE 0
                                                                     END)
                       AND DATEDIFF(''day'',OB.dtFromDate + make_interval(days => CASE 
                                                      --WHEN bitBillingTerms = true THEN COALESCE(fn_GetListItemName(COALESCE(intBillingDays,0)),0)
                                                      WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                                      ELSE 0
                                                    END),GetUTCDateWithoutTime()) BETWEEN 0 AND 30 ';
      v_strSql1 := coalesce(v_strSql1,'') || 'AND DATEDIFF(''day'',GJH.datEntry_Date, GetUTCDateWithoutTime()) BETWEEN 0 AND 30 ';
      v_strCredit := coalesce(v_strCredit,'') || 'AND DATEDIFF(''day'',DM.dtDepositDate, GetUTCDateWithoutTime()) BETWEEN 0 AND 30 ';
   ELSEIF (v_vcFlag = '30-60')
   then
                
      v_strSql := coalesce(v_strSql,'') ||
      'AND GetUTCDateWithoutTime() > OB.dtFromDate + make_interval(days => CASE 
                                                                         --WHEN bitBillingTerms = true THEN COALESCE(fn_GetListItemName(COALESCE(intBillingDays,0)),0)
                                                                         WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                                                         ELSE 0
                                                                       END)
                         AND DATEDIFF(''day'',OB.dtFromDate + make_interval(days => CASE 
                                                        --WHEN bitBillingTerms = true THEN COALESCE(fn_GetListItemName(COALESCE(intBillingDays,0)),0)
                                                        WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                                        ELSE 0
                                                      END),GetUTCDateWithoutTime()) BETWEEN 31 AND 60';
      v_strSql1 := coalesce(v_strSql1,'') || 'AND DATEDIFF(''day'',GJH.datEntry_Date, GetUTCDateWithoutTime()) BETWEEN 31 AND 60 ';
      v_strCredit := coalesce(v_strCredit,'') || 'AND DATEDIFF(''day'',DM.dtDepositDate, GetUTCDateWithoutTime()) BETWEEN 31 AND 60 ';
   ELSEIF (v_vcFlag = '60-90')
   then
                  
      v_strSql := coalesce(v_strSql,'') ||
      'AND GetUTCDateWithoutTime() > OB.dtFromDate + make_interval(days => CASE 
                                                                           --WHEN bitBillingTerms = true THEN COALESCE(fn_GetListItemName(COALESCE(intBillingDays,0)),0)
                                                                           WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                                                           ELSE 0
                                                                         END)
                           AND DATEDIFF(''day'',OB.dtFromDate + make_interval(days => CASE 
                                                          --WHEN bitBillingTerms = true THEN COALESCE(fn_GetListItemName(COALESCE(intBillingDays,0)),0)
                                                          WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                                          ELSE 0
                                                        END),GetUTCDateWithoutTime()) BETWEEN 61 AND 90';
      v_strSql1 := coalesce(v_strSql1,'') || 'AND DATEDIFF(''day'',GJH.datEntry_Date, GetUTCDateWithoutTime()) BETWEEN 61 AND 90 ';
      v_strCredit := coalesce(v_strCredit,'') || 'AND DATEDIFF(''day'',DM.dtDepositDate, GetUTCDateWithoutTime()) BETWEEN 61 AND 90 ';
   ELSEIF (v_vcFlag = '90-')
   then
                    
      v_strSql := coalesce(v_strSql,'') ||
      'AND GetUTCDateWithoutTime() > OB.dtFromDate + make_interval(days => CASE 
                                                                             --WHEN bitBillingTerms = true THEN ,COALESCE(fn_GetListItemName(COALESCE(intBillingDays,0)),0)
                                                                             WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms 
																															   WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                                                             ELSE 0
                                                                           END)
                             AND DATEDIFF(''day'',OB.dtFromDate + make_interval(days => CASE 
                                                            --WHEN bitBillingTerms = true THEN COALESCE(fn_GetListItemName(COALESCE(intBillingDays,0)),0)
                                                            WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                                            ELSE 0
                                                          END),GetUTCDateWithoutTime()) > 90';
      v_strSql1 := coalesce(v_strSql1,'') || 'AND DATEDIFF(''day'',GJH.datEntry_Date, GetUTCDateWithoutTime()) > 90';
      v_strCredit := coalesce(v_strCredit,'') || 'AND DATEDIFF(''day'',DM.dtDepositDate, GetUTCDateWithoutTime()) > 90';
   end if;
     
	        
   v_strSql := coalesce(v_strSql,'') || coalesce(v_strSql1,'') ||  coalesce(v_strCredit,'');

	EXECUTE v_strSql;

   v_strSql := CONCAT(' SELECT *,CASE WHEN DATEDIFF (''day'' ,dtDueDate ,now()::TIMESTAMP) > 0 
										  THEN DATEDIFF (''day'' ,dtDueDate ,now()::TIMESTAMP) 
										  ELSE NULL 
									 END AS DaysLate,
									 CASE WHEN DATEDIFF (''day'' ,dtDueDate,now()::TIMESTAMP) > 0 
										  THEN ''',' Past Due (',''' || CAST(DATEDIFF (''day'',dtDueDate,now()::TIMESTAMP) AS VARCHAR(10))||''',')','''
										  ELSE NULL 
									 END AS "Status(DaysLate)",
									 CASE WHEN ',v_baseCurrency,
   ' <> numCurrencyID THEN 2 ELSE 1 END AS CurrencyCount,
									 fn_GetChart_Of_AccountsName(numChartAcntId) vcARAccount
							FROM tt_TempRecords WHERE (COALESCE(BalanceDue,0) <> 0 or numOppId=-1)');
	
   IF coalesce(v_vcRegularSearch,'') <> '' then
	
      v_strSql := CONCAT(v_strSql,' AND ',v_vcRegularSearch);
   end if;

   
   RAISE NOTICE '%',CAST(v_strSql AS TEXT);
   OPEN SWV_RefCur FOR EXECUTE v_strSql;
   RETURN;
END; $$;


