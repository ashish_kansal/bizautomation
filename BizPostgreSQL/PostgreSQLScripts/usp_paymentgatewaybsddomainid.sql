-- Stored procedure definition script USP_PaymentGateWayBsdDomainID for PostgreSQL
CREATE OR REPLACE FUNCTION USP_PaymentGateWayBsdDomainID(v_numDomainID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for 
   select 
	P.intPaymentGateWay AS "intPaymentGateWay"
	,coalesce(vcFirstFldValue,'') AS "vcFirstFldValue"
	,coalesce(vcSecndFldValue,'') AS "vcSecndFldValue"
	,coalesce(vcThirdFldValue,'') AS "vcThirdFldValue"
	,coalesce(bitTest,false) AS "bitTest"
	from Domain D
   join PaymentGateway  P
   on P.intPaymentGateWay = D.intPaymentGateWay
   left join PaymentGatewayDTLID  PDTL
   on PDTL.intPaymentGateWay = P.intPaymentGateWay and PDTL.numDomainID = v_numDomainID
   where D.numDomainId = v_numDomainID;
END; $$;












