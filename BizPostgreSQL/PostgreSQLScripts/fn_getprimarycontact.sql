-- Function definition script fn_GetPrimaryContact for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetPrimaryContact(v_numDivisionID NUMERIC)
RETURNS VARCHAR(15) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_contactID  VARCHAR(15);
BEGIN
   select   coalesce(numContactId,0) INTO v_contactID from AdditionalContactsInformation where  (coalesce(bitPrimaryContact,false) = true
   OR numContactType IS NULL
   OR numContactType = 92)
   and  numDivisionId = v_numDivisionID;
 
   return v_contactID;
END; $$;

