-- Stored procedure definition script usp_GetAllItems for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetAllItems(v_numDomainID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT cast(numItemCode as VARCHAR(255)), cast(vcItemName as VARCHAR(255)) FROM Item
   where numDomainID = v_numDomainID
   ORDER BY vcItemName;
END; $$;












