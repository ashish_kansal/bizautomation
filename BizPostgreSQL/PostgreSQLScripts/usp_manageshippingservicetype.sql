-- Stored procedure definition script USP_ManageShippingServiceType for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageShippingServiceType(v_byteMode SMALLINT DEFAULT 0,
    v_numServiceTypeID NUMERIC DEFAULT NULL,
    v_numDomainID NUMERIC DEFAULT NULL,
    v_numRuleID NUMERIC DEFAULT NULL,
    v_vcServiceName VARCHAR(100) DEFAULT NULL,
    v_intNsoftEnum INTEGER DEFAULT 0,
    v_intFrom INTEGER DEFAULT NULL,
    v_intTo INTEGER DEFAULT NULL,
    v_fltMarkup DOUBLE PRECISION DEFAULT NULL,
    v_bitMarkupType BOOLEAN DEFAULT NULL,
    v_monRate NUMERIC(20,5) DEFAULT NULL,
    v_bitEnabled BOOLEAN DEFAULT NULL,
	v_vcItemClassification VARCHAR(500) DEFAULT NULL,
    v_strItems XML DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_byteMode = 0 then
        
      IF v_numServiceTypeID = 0 then
                
         INSERT  INTO ShippingServiceTypes(vcServiceName,
                                                            intNsoftEnum,
                                                            intFrom,
                                                            intTo,
                                                            fltMarkup,
                                                            bitMarkupType,
                                                            monRate,
                                                            numRuleID,
                                                            numDomainID,
															vcItemClassification)
                    VALUES(v_vcServiceName,
                              v_intNsoftEnum,
                              v_intFrom,
                              v_intTo,
                              v_fltMarkup,
                              v_bitMarkupType,
                              v_monRate,
                              v_numRuleID,
                              v_numDomainID,
							  v_vcItemClassification);
      ELSE
         UPDATE  ShippingServiceTypes
         SET     vcServiceName = v_vcServiceName,intNsoftEnum = v_intNsoftEnum,intFrom = v_intFrom,
         intTo = v_intTo,fltMarkup = v_fltMarkup,bitMarkupType = v_bitMarkupType,
         monRate = v_monRate,bitEnabled = v_bitEnabled,vcItemClassification = v_vcItemClassification
         WHERE   numServiceTypeID = v_numServiceTypeID
         AND numDomainID = v_numDomainID;
      end if;
   end if;
   IF v_byteMode = 1 then
      DELETE FROM ShippingServiceTypes WHERE numServiceTypeID = v_numServiceTypeID AND numDomainID = v_numDomainID;
   end if;
	
   IF v_byteMode = 2 then
      IF v_numServiceTypeID = 2 then
		
         UPDATE ShippingServiceTypes SET bitEnabled = false WHERE intNsoftEnum = 0 AND numRuleID = v_numRuleID;
      end if;
      IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then
		
         
         UPDATE
         ShippingServiceTypes
         SET
         vcServiceName = X.vcServiceName,intFrom = X.intFrom,intTo = X.intTo,fltMarkup = X.fltMarkup,
         bitMarkupType = X.bitMarkupType,monRate = X.monRate,bitEnabled = X.bitEnabled
		  FROM  XMLTABLE
	(
		'NewDataSet/Item'
        PASSING 
			v_strItems 
		COLUMNS
			id FOR ORDINALITY,
			numServiceTypeID NUMERIC(9,0) PATH 'numServiceTypeID'
			,vcServiceName VARCHAR(100) PATH 'vcServiceName'
			,intFrom INTEGER PATH 'intFrom'
			,intTo INTEGER PATH 'intTo'
			,fltMarkup DOUBLE PRECISION PATH 'fltMarkup'
			,bitMarkupType BOOLEAN PATH 'bitMarkupType'
			,monRate NUMERIC(20,5) PATH 'monRate'
			,bitEnabled BOOLEAN PATH 'bitEnabled'
	) AS X
         
         WHERE
         ShippingServiceTypes.numServiceTypeID = X.numServiceTypeID
         AND ShippingServiceTypes.numDomainID = v_numDomainID;

      end if;
   end if;
   RETURN;
END; $$;


