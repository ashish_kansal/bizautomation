-- Stored procedure definition script Insert_Action_Template_Data for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Insert_Action_Template_Data(v_templateName VARCHAR(100) ,    
v_dueDays INTEGER ,    
v_status INTEGER ,    
v_type VARCHAR(25) ,     
v_activity INTEGER ,    
v_comments VARCHAR(2000),  
v_bitSendEmailTemplate BOOLEAN,  
v_numEmailTemplate NUMERIC(9,0),  
v_tintHours SMALLINT DEFAULT 0,  
v_bitAlert BOOLEAN DEFAULT NULL ,
v_numUserCntID NUMERIC(9,0) DEFAULT NULL   ,
v_numDomainID NUMERIC(9,0) DEFAULT NULL,
v_numTaskType NUMERIC(9,0) DEFAULT NULL,
v_bitRemind BOOLEAN DEFAULT false,
v_numRemindBeforeMinutes INTEGER DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   If Not Exists(Select 1 From tblActionItemData Where RTrim(TemplateName) = RTrim(v_templateName)) then

      Insert Into tblActionItemData(TemplateName,DueDays,Priority,Type,Activity,Comments,bitSendEmailTemplate,numEmailTemplate,
		tintHours,bitAlert,	numDomainID,numCreatedBy,dtCreatedBy,numTaskType,bitRemind,numRemindBeforeMinutes)
	Values(RTrim(v_templateName),v_dueDays,v_status,v_type,v_activity,v_comments,v_bitSendEmailTemplate,v_numEmailTemplate,
		v_tintHours,v_bitAlert,v_numDomainID,v_numUserCntID,TIMEZONE('UTC',now()),v_numTaskType,CAST(v_bitRemind AS BOOLEAN),v_numRemindBeforeMinutes);
   end if;
   RETURN;
END; $$;


