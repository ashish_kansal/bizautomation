-- Stored procedure definition script USP_Item_GetFromAttributeIds for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Item_GetFromAttributeIds(v_numDomainID NUMERIC(18,0)
	,v_numItemCode NUMERIC(18,0) DEFAULT 0    
	,v_vcAttributeIDs VARCHAR(500) DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcItemName  VARCHAR(300) DEFAULT '';
   v_numItemGroup  NUMERIC(18,0);
BEGIN
   select   coalesce(vcItemName,''), coalesce(numItemGroup,0) INTO v_vcItemName,v_numItemGroup FROM Item WHERE numItemCode = v_numItemCode AND numDomainID = v_numDomainID;


   open SWV_RefCur for 
   SELECT 
		numItemCode
		,(SELECT numWareHouseItemID FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numItemID = Item.numItemCode AND coalesce(numWLocationID,0) <> -1 LIMIT 1) AS numWarehouseItemID
   FROM
   Item
   WHERE
   numDomainID = v_numDomainID
   AND vcItemName = v_vcItemName
   AND bitMatrix = true
   AND numItemGroup = v_numItemGroup
   AND COALESCE((SELECT string_agg(CONCAT(Fld_ID,':',Fld_Value),',' ORDER BY ItemAttributes.ID) FROM ItemAttributes WHERE numDomainID = v_numDomainID AND numItemCode = Item.numItemCode),'') = v_vcAttributeIDs LIMIT 1;
END; $$;












