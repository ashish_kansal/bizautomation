-- Stored procedure definition script usp_GetFirstEmployer for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetFirstEmployer(v_numDivId NUMERIC DEFAULT 0   
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT vcFirstName,vcLastname,numContactId FROM AdditionalContactsInformation WHERE numContactType = 92 and numDivisionId = v_numDivId;
END; $$;












