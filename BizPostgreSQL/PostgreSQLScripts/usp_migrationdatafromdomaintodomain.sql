-- Stored procedure definition script USP_MigrationDataFromDomainToDomain for PostgreSQL
CREATE OR REPLACE FUNCTION USP_MigrationDataFromDomainToDomain(v_sourceDomainId NUMERIC(18,0) DEFAULT 0, -- 153 Kirk & Matz
v_destinationDomainId NUMERIC(18,0) DEFAULT 0, --172 Boneta, Inc
v_sourceGroupId NUMERIC(18,0) DEFAULT 0,--710
v_destinationGroupId NUMERIC(18,0) DEFAULT 0,--858
v_taskToPerform VARCHAR(100) DEFAULT NULL--'1,2,3'
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_totalListRecords  INTEGER DEFAULT 0;
   v_i  INTEGER DEFAULT 1;
   v_vcListName  TEXT;
   v_bitDeleted  BOOLEAN;
   v_bitFixed  BOOLEAN;
   v_bitFlag  BOOLEAN;
   v_vcDataType  TEXT;
   v_numModuleID  NUMERIC(18,0);
   v_numListID  NUMERIC(18,0);
   v_numExistListID  NUMERIC(18,0);
-----------------------------------------------------------Biz Form Wizard Configuration Sync----------------------------------------------------------------------
   v_numFormFieldGroupId  NUMERIC(18,0) DEFAULT 0;
   v_numOldFormFieldGroupId  NUMERIC(18,0) DEFAULT 0;

   v_numFieldId  NUMERIC(18,0) DEFAULT 0;
   v_numOldFieldId  NUMERIC(18,0) DEFAULT 0;
   v_numOldModuleId  NUMERIC(18,0) DEFAULT 0;
   v_numUserCntID  NUMERIC(18,0) DEFAULT 0;
-----------------------------------------------------------Global Setting Configuration----------------------------------------------------------------------
--ADD Custom Text Field to DycFieldMaster
   v_numOldTabId  NUMERIC(18,0) DEFAULT 0;
   v_numTabId  NUMERIC(18,0) DEFAULT 0;
   v_bitTabFixed  BOOLEAN DEFAULT 0;
BEGIN
   BEGIN
      -- BEGIN TRANSACTION
  

-----------------------------------------------------------Dropdown List Sync----------------------------------------------------------------------
IF(1 IN(SELECT Items FROM Split(v_taskToPerform,','))) then
         RAISE NOTICE 'Start Dropdown sync';
         BEGIN
            CREATE TEMP SEQUENCE tt_tempSrcListMaster_seq INCREMENT BY 1 START WITH 1;
            EXCEPTION WHEN OTHERS THEN
               NULL;
         END;
         DROP TABLE IF EXISTS tt_TEMPSRCLISTMASTER CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPSRCLISTMASTER
         (
            numExistListID NUMERIC(18,0),
            vcListName TEXT,
            bitDeleted BOOLEAN,
            bitFixed BOOLEAN,
            bitFlag BOOLEAN,
            vcDataType TEXT,
            numModuleID NUMERIC(18,0),
            ID NUMERIC(18,0) DEFAULT NEXTVAL('tt_tempSrcListMaster_seq')
         );
         INSERT INTO tt_TEMPSRCLISTMASTER(numExistListID, vcListName, bitDeleted, bitFixed, bitFlag, vcDataType, numModuleID)
         SELECT numListID,vcListName,bitDeleted,bitFixed,bitFlag,vcDataType,numModuleID FROM listmaster WHERE numDomainID = v_sourceDomainId OR bitFlag = true;
         SELECT COUNT(*) INTO v_totalListRecords FROM tt_TEMPSRCLISTMASTER;
         WHILE(v_i <= v_totalListRecords) LOOP
            select   numExistListID, vcListName, bitDeleted, bitFixed, bitFlag, vcDataType, numModuleID INTO v_numExistListID,v_vcListName,v_bitDeleted,v_bitFixed,v_bitFlag,v_vcDataType,
            v_numModuleID FROM tt_TEMPSRCLISTMASTER WHERE ID = v_i;
            IF(v_bitFlag = true) then
	
               v_numListID := v_numExistListID;
            ELSEIF NOT EXISTS(SELECT * FROM listmaster WHERE vcListName = v_vcListName AND numDomainID = v_destinationDomainId AND numModuleID = v_numModuleID)
            then
	
               INSERT INTO listmaster(vcListName, numCreatedBy, bintCreatedDate, numModifiedBy, bintModifiedDate, bitDeleted, bitFixed, numDomainID, bitFlag, vcDataType, numModuleId)
		VALUES(v_vcListName,1,LOCALTIMESTAMP ,1,LOCALTIMESTAMP ,v_bitDeleted,v_bitFixed,v_destinationDomainId,v_bitFlag,v_vcDataType,v_numModuleID);
		
               v_numListID := CURRVAL('ListMaster_seq');
            ELSE
               SELECT  numListID INTO v_numListID FROM listmaster WHERE vcListName = v_vcListName AND numDomainID = v_destinationDomainId AND numModuleID = v_numModuleID     LIMIT 1;
            end if;
            INSERT INTO Listdetails(numListID, vcData, bitDelete, numDomainid, constFlag, sintOrder, numListType, tintOppOrOrder,numListItemGroupId)
            SELECT v_numListID,vcData, bitDelete, v_destinationDomainId, constFlag, sintOrder, numListType, tintOppOrOrder,
	CASE WHEN coalesce(numListItemGroupId,0) > 0 THEN(SELECT  numListItemID FROM Listdetails WHERE numDomainid = v_destinationDomainId AND numListID = v_numListID AND
                  vcData =(SELECT  vcData FROM Listdetails As L where L.numListItemID = numListItemGroupId AND numDomainid = v_sourceDomainId LIMIT 1) LIMIT 1) ELSE 0 END
            FROM Listdetails WHERE
            numDomainid = v_sourceDomainId AND  numListID = v_numExistListID AND
            vcData NOT IN(SELECT vcData FROM Listdetails WHERE numDomainid = v_destinationDomainId AND numListID = v_numListID);
            INSERT INTO listorder(numListId,numListItemID,numDomainId,intSortOrder)
            SELECT v_numListID,(SELECT  numListItemID FROM Listdetails WHERE numListID = v_numListID AND vcData = LD.vcData LIMIT 1),v_destinationDomainId,intSortOrder FROM
            listorder AS LO LEFT JOIN Listdetails AS LD ON LO.numListItemID = LD.numListItemID where LO.numListId = v_numExistListID AND LO.numDomainId = v_sourceDomainId;
            v_i := v_i::bigint+1;
         END LOOP;
         DROP TABLE IF EXISTS tt_TEMPSRCLISTMASTER CASCADE;
      end if;
-----------------------------------------------------------Biz Form Wizard Configuration Sync----------------------------------------------------------------------
      IF(2 IN(SELECT Items FROM Split(v_taskToPerform,','))) then
         BEGIN
            CREATE TEMP SEQUENCE tt_tempFormFieldGroupConfigurarionMaster_seq INCREMENT BY 1 START WITH 1;
            EXCEPTION WHEN OTHERS THEN
               NULL;
         END;
         DROP TABLE IF EXISTS tt_TEMPFORMFIELDGROUPCONFIGURARIONMASTER CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPFORMFIELDGROUPCONFIGURARIONMASTER
         (
            numOldFormFieldGroupId NUMERIC(18,0),
            numFormFieldGroupId NUMERIC(18,0),
            ID NUMERIC(18,0) DEFAULT NEXTVAL('tt_tempFormFieldGroupConfigurarionMaster_seq')
         );
         INSERT INTO tt_TEMPFORMFIELDGROUPCONFIGURARIONMASTER(numOldFormFieldGroupId,numFormFieldGroupId)
         SELECT numFormFieldGroupId,0 FROM FormFieldGroupConfigurarion WHERE numDomainID = v_sourceDomainId;
         v_i := 1;
         select   COUNT(*) INTO v_totalListRecords FROM tt_TEMPFORMFIELDGROUPCONFIGURARIONMASTER;
         WHILE(v_i <= v_totalListRecords) LOOP
            select   numOldFormFieldGroupId INTO v_numOldFormFieldGroupId FROM tt_TEMPFORMFIELDGROUPCONFIGURARIONMASTER WHERE ID = v_i;
            IF EXISTS(SELECT numFormFieldGroupId FROM FormFieldGroupConfigurarion WHERE vcGroupName =(SELECT  vcGroupName FROM FormFieldGroupConfigurarion WHERE numFormFieldGroupId = v_numOldFormFieldGroupId LIMIT 1) AND numDomainID = v_destinationDomainId) then
	
               select   numFormFieldGroupId INTO v_numFormFieldGroupId FROM FormFieldGroupConfigurarion WHERE vcGroupName =(SELECT  vcGroupName FROM FormFieldGroupConfigurarion WHERE numFormFieldGroupId = v_numOldFormFieldGroupId LIMIT 1) AND numDomainID = v_destinationDomainId;
            ELSE
               INSERT INTO FormFieldGroupConfigurarion(numFormId, numGroupId, vcGroupName, numDomainID, numOrder)
               SELECT F.numFormId,F.numGroupId,F.vcGroupName,v_destinationDomainId,F.numOrder FROM tt_TEMPFORMFIELDGROUPCONFIGURARIONMASTER AS T LEFT JOIN FormFieldGroupConfigurarion AS F ON T.numOldFormFieldGroupId = F.numFormFieldGroupId WHERE F.numDomainID = v_sourceDomainId AND ID = v_i;
               v_numFormFieldGroupId := CURRVAL('FormFieldGroupConfigurarion_seq');
            end if;
            UPDATE tt_TEMPFORMFIELDGROUPCONFIGURARIONMASTER SET numFormFieldGroupId = v_numFormFieldGroupId WHERE ID = v_i;
            v_i := v_i::bigint+1;
         END LOOP;
--ADD Custom Text Field to DycFieldMaster
         BEGIN
            CREATE TEMP SEQUENCE tt_tempDycFieldMaster_seq INCREMENT BY 1 START WITH 1;
            EXCEPTION WHEN OTHERS THEN
               NULL;
         END;
         DROP TABLE IF EXISTS tt_TEMPDYCFIELDMASTER CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPDYCFIELDMASTER
         (
            numModuleId NUMERIC(18,0),
            numOldFieldId NUMERIC(18,0),
            numFieldId NUMERIC(18,0),
            ID NUMERIC(18,0) DEFAULT NEXTVAL('tt_tempDycFieldMaster_seq')
         );
         INSERT INTO tt_TEMPDYCFIELDMASTER(numOldFieldId,numFieldId,numModuleId)
         SELECT  numFieldId,0,numModuleId FROM DycFieldMaster where numDomainID > 0 AND numDomainID = v_sourceDomainId;
         v_i := 1;
         select   COUNT(*) INTO v_totalListRecords FROM tt_TEMPDYCFIELDMASTER;
         WHILE(v_i <= v_totalListRecords) LOOP
            select   numOldFieldId, numModuleId INTO v_numOldFieldId,v_numOldModuleId FROM tt_TEMPDYCFIELDMASTER WHERE ID = v_i;
            IF EXISTS(SELECT numFieldId FROM DycFieldMaster WHERE vcFieldName =(SELECT  vcFieldName FROM DycFieldMaster WHERE numFieldId = v_numOldFieldId AND numModuleID = v_numOldModuleId LIMIT 1) AND numDomainID = v_destinationDomainId AND numModuleID = v_numOldModuleId) then
	
               select   numFieldId INTO v_numFieldId FROM DycFieldMaster WHERE vcFieldName =(SELECT  vcFieldName FROM DycFieldMaster WHERE numFieldId = v_numOldFieldId AND numModuleID = v_numOldModuleId LIMIT 1) AND numDomainID = v_destinationDomainId  AND numModuleID = v_numOldModuleId;
            ELSE
               INSERT INTO DycFieldMaster(numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbCOlumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, "order", tintRow, tintColumn, bitInResults, bitDeleted, bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, intColumnWidth, intFieldMaxLength, intWFCompare, vcGroup, vcWFCompareField)
               SELECT D.numModuleID, v_destinationDomainId, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, "order", tintRow, tintColumn, bitInResults, bitDeleted, bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, intColumnWidth, intFieldMaxLength, intWFCompare, vcGroup, vcWFCompareField
               FROM tt_TEMPDYCFIELDMASTER AS T LEFT JOIN DycFieldMaster AS D ON T.numOldFieldId = D.numFieldID WHERE D.numDomainID = v_sourceDomainId AND D.numModuleID = v_numOldModuleId AND ID = v_i;
               v_numFieldId := CURRVAL('DycFieldMaster_seq');
            end if;
            UPDATE tt_TEMPDYCFIELDMASTER SET numFieldId = v_numFieldId WHERE ID = v_i;
            v_i := v_i::bigint+1;
         END LOOP;
         DELETE FROM DycFormField_Mapping WHERE  numDomainID > 0 AND numDomainID = v_destinationDomainId;
         INSERT INTO DycFormField_Mapping(numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, "order", tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
         SELECT numModuleID, CASE WHEN(SELECT COUNT(*) FROM tt_TEMPDYCFIELDMASTER WHERE numOldFieldId = D.numFieldID) > 0 THEN(SELECT   numFieldId FROM tt_TEMPDYCFIELDMASTER WHERE numOldFieldId = D.numFieldID LIMIT 1) ELSE 0 END, v_destinationDomainId, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, "order", tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitRequired,
CASE WHEN(SELECT COUNT(*) FROM tt_TEMPDYCFIELDMASTER WHERE numOldFieldId = D.numFormFieldID) > 0 THEN(SELECT   numFieldId FROM tt_TEMPDYCFIELDMASTER WHERE numOldFieldId = D.numFormFieldID LIMIT 1) ELSE 0 END
, intSectionID, bitAllowGridColor
         FROM DycFormField_Mapping AS D WHERE numDomainID = v_sourceDomainId AND numDomainID > 0 AND(SELECT COUNT(*) FROM DycFormField_Mapping As DM WHERE DM.numModuleID = D.numModuleID AND numDomainID > 0 AND numDomainID = v_destinationDomainId AND DM.numFormID = D.numFormID AND DM.numFieldID =(CASE WHEN(SELECT COUNT(*) FROM tt_TEMPDYCFIELDMASTER WHERE numOldFieldId = D.numFieldID) > 0 THEN(SELECT   numFieldId FROM tt_TEMPDYCFIELDMASTER WHERE numOldFieldId = D.numFieldID LIMIT 1) ELSE D.numFieldID END)) = 0
         AND(SELECT COUNT(*) FROM tt_TEMPDYCFIELDMASTER WHERE numOldFieldId = D.numFieldID) > 0;
         DELETE FROM DynamicFormField_Validation WHERE  numDomainID > 0 AND  numDomainID = v_destinationDomainId;
         INSERT INTO DynamicFormField_Validation(numFormId, numFormFieldId, numDomainID, vcNewFormFieldName, bitIsRequired, bitIsNumeric, bitIsAlphaNumeric, bitIsEmail, bitIsLengthValidation, intMaxLength, intMinLength, bitFieldMessage, vcFieldMessage, vcToolTip)
         SELECT numFormId, CASE WHEN(SELECT COUNT(*) FROM tt_TEMPDYCFIELDMASTER WHERE numOldFieldId = D.numFormFieldId) > 0 THEN(SELECT   numFieldId FROM tt_TEMPDYCFIELDMASTER WHERE numOldFieldId = D.numFormFieldId LIMIT 1) ELSE D.numFormFieldId END, v_destinationDomainId, vcNewFormFieldName, bitIsRequired, bitIsNumeric, bitIsAlphaNumeric, bitIsEmail, bitIsLengthValidation, intMaxLength, intMinLength, bitFieldMessage, vcFieldMessage, vcToolTip FROM DynamicFormField_Validation AS D WHERE numDomainID = v_sourceDomainId;

--Dynamic Configuration For User
         DELETE FROM BizFormWizardMasterConfiguration  WHERE  numDomainID = v_destinationDomainId AND numGroupID = v_destinationGroupId  AND bitCustom = false;
         INSERT INTO BizFormWizardMasterConfiguration(numFormID,
			numFieldID,
			intColumnNum,
			intRowNum,
			numDomainID,
			numGroupID,
			numRelCntType,
			tintPageType,
			bitCustom,
			bitGridConfiguration,
			numFormFieldGroupId)
         SELECT  numFormID,
			CASE WHEN(SELECT COUNT(*) FROM tt_TEMPDYCFIELDMASTER WHERE numOldFieldId = D.numFieldID) > 0 THEN(SELECT   numFieldId FROM tt_TEMPDYCFIELDMASTER WHERE numOldFieldId = D.numFieldID LIMIT 1) ELSE D.numFieldID END,
			intColumnNum,
			intRowNum,
			v_destinationDomainId,
			v_destinationGroupId,
			numRelCntType,
			tintPageType,
			bitCustom,
			bitGridConfiguration,
			CASE WHEN(SELECT COUNT(*) FROM tt_TEMPFORMFIELDGROUPCONFIGURARIONMASTER WHERE numOldFormFieldGroupId = D.numFormFieldGroupId) > 0 THEN(SELECT   numFormFieldGroupId FROM tt_TEMPFORMFIELDGROUPCONFIGURARIONMASTER WHERE numOldFormFieldGroupId = D.numFormFieldGroupId LIMIT 1) ELSE D.numFormFieldGroupId END
         FROM BizFormWizardMasterConfiguration AS D WHERE numDomainID = v_sourceDomainId AND numGroupID = v_sourceGroupId  AND bitCustom = false;
         BEGIN
            CREATE TEMP SEQUENCE tt_tempDycUserMaster_seq INCREMENT BY 1 START WITH 1;
            EXCEPTION WHEN OTHERS THEN
               NULL;
         END;
         DROP TABLE IF EXISTS tt_TEMPDYCUSERMASTER CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPDYCUSERMASTER
         (
            numUserCntID NUMERIC(18,0),
            ID NUMERIC(18,0) DEFAULT NEXTVAL('tt_tempDycUserMaster_seq')
         );
         INSERT INTO tt_TEMPDYCUSERMASTER(numUserCntID) SELECT numUserDetailId FROM UserMaster WHERE numGroupID = v_destinationGroupId AND numDomainID = v_destinationDomainId;
         v_i := 1;
         select   COUNT(*) INTO v_totalListRecords FROM tt_TEMPDYCUSERMASTER;
         WHILE(v_i <= v_totalListRecords) LOOP
            select   numUserCntID INTO v_numUserCntID FROM tt_TEMPDYCUSERMASTER WHERE ID = v_i;
            DELETE FROM DycFormConfigurationDetails WHERE numDomainID = v_destinationDomainId AND numUserCntID = v_numUserCntID  AND bitCustom = 0;
            INSERT INTO DycFormConfigurationDetails(numFormID,
				numFieldID,
				intColumnNum,
				intRowNum,
				numDomainID,
				numAuthGroupID,
				numRelCntType,
				tintPageType,
				bitCustom,
				numUserCntID,
				numFormFieldGroupId,
				bitDefaultByAdmin)
            SELECT  numFormID,
			numFieldID,
			intColumnNum,
			intRowNum,
			v_destinationDomainId,
			v_destinationGroupId,
			numRelCntType,
			tintPageType,
			bitCustom,
			v_numUserCntID
            AS bitGridConfiguration,
			CASE WHEN(SELECT COUNT(*) FROM tt_TEMPFORMFIELDGROUPCONFIGURARIONMASTER WHERE numOldFormFieldGroupId = D.numFormFieldGroupId) > 0 THEN(SELECT   numFormFieldGroupId FROM tt_TEMPFORMFIELDGROUPCONFIGURARIONMASTER WHERE numOldFormFieldGroupId = D.numFormFieldGroupId LIMIT 1) ELSE D.numFormFieldGroupId END,
			1
            FROM BizFormWizardMasterConfiguration AS D WHERE numDomainID = v_destinationDomainId AND numGroupID = v_destinationGroupId  AND bitCustom = false;
            v_i := v_i::bigint+1;
         END LOOP;
         delete  FROM DycFormConfigurationDetails where coalesce(numUserCntId,0) = 0 AND numAuthGroupID = v_destinationGroupId
         and numFormID > 0
         AND numDomainID = v_destinationDomainId;
         INSERT INTO DycFormConfigurationDetails(numFormID,
				numFieldID,
				intColumnNum,
				intRowNum,
				numDomainID,
				numAuthGroupID,
				numRelCntType,
				tintPageType,
				bitCustom,
				numUserCntID,
				numFormFieldGroupId,
				bitDefaultByAdmin)
         SELECT	    numFormID,
			CASE WHEN(SELECT COUNT(*) FROM tt_TEMPDYCFIELDMASTER WHERE numOldFieldId = D.numFieldID) > 0 THEN(SELECT   numFieldId FROM tt_TEMPDYCFIELDMASTER WHERE numOldFieldId = D.numFieldID LIMIT 1) ELSE D.numFieldID END,
			intColumnNum,
			intRowNum,
			v_destinationDomainId,
			v_destinationGroupId,
			numRelCntType,
			tintPageType,
			bitCustom,
			NULL,
			CASE WHEN(SELECT COUNT(*) FROM tt_TEMPFORMFIELDGROUPCONFIGURARIONMASTER WHERE numOldFormFieldGroupId = D.numFormFieldGroupId) > 0 THEN(SELECT   numFormFieldGroupId FROM tt_TEMPFORMFIELDGROUPCONFIGURARIONMASTER WHERE numOldFormFieldGroupId = D.numFormFieldGroupId LIMIT 1) ELSE D.numFormFieldGroupId END,
			bitDefaultByAdmin
         FROM DycFormConfigurationDetails AS D WHERE coalesce(numUserCntId,0) = 0  AND numDomainID = v_sourceDomainId  AND numAuthGroupID = v_sourceGroupId and numFormID > 0;

--delete  DycFormConfigurationDetails where ISNULL(numUserCntId,0)=0  AND numAuthGroupID=@destinationGroupId
--			and numFormId>0
--			AND tintPageType=2 AND numDomainId =@destinationDomainId
--INSERT INTO DycFormConfigurationDetails(numFormID,
--				numFieldID,
--				intColumnNum,
--				intRowNum,
--				numDomainID,
--				numAuthGroupID,
--				numRelCntType,
--				tintPageType,
--				bitCustom,
--				numUserCntID,
--				numFormFieldGroupId,
--				bitDefaultByAdmin)
--SELECT	    numFormID,
--			CASE WHEN (SELECT COUNT(*) FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFieldId) > 0 THEN (SELECT TOP 1  numFieldId FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFieldId) ELSE D.numFieldId END,
--			intColumnNum,
--			intRowNum,
--			@destinationDomainId,
--			NULL,
--			numRelCntType,
--			tintPageType,
--			bitCustom,
--			0,
--			CASE WHEN (SELECT COUNT(*) FROM #tempFormFieldGroupConfigurarionMaster WHERE numOldFormFieldGroupId=D.numFormFieldGroupId) > 0 THEN (SELECT TOP 1  numFormFieldGroupId FROM #tempFormFieldGroupConfigurarionMaster WHERE numOldFormFieldGroupId=D.numFormFieldGroupId) ELSE D.numFormFieldGroupId END,
--			bitDefaultByAdmin
--FROM DycFormConfigurationDetails AS D WHERE numUserCntId=0 AND numDomainId=@sourceDomainId  AND numAuthGroupID=@sourceGroupId

         DROP TABLE IF EXISTS tt_TEMPFORMFIELDGROUPCONFIGURARIONMASTER CASCADE;
         DROP TABLE IF EXISTS tt_TEMPDYCFIELDMASTER CASCADE;
         DROP TABLE IF EXISTS tt_TEMPDYCUSERMASTER CASCADE;
      end if;
-----------------------------------------------------------Global Setting Configuration----------------------------------------------------------------------
--ADD Custom Text Field to DycFieldMaster
      IF(3 IN(SELECT Items FROM Split(v_taskToPerform,','))) then
         BEGIN
            CREATE TEMP SEQUENCE tt_tempTabMaster_seq INCREMENT BY 1 START WITH 1;
            EXCEPTION WHEN OTHERS THEN
               NULL;
         END;
         DROP TABLE IF EXISTS tt_TEMPTABMASTER CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPTABMASTER
         (
            numOldTabId NUMERIC(18,0),
            numTabId NUMERIC(18,0),
            bitFixed BOOLEAN,
            ID NUMERIC(18,0) DEFAULT NEXTVAL('tt_tempTabMaster_seq')
         );
         INSERT INTO tt_TEMPTABMASTER(numOldTabId, numTabId,bitFixed)
         SELECT numTabId,0,bitFixed FROM TabMaster WHERE numDomainID = v_sourceDomainId OR bitFixed = true;
         v_i := 1;
         select   COUNT(*) INTO v_totalListRecords FROM tt_TEMPTABMASTER;
         WHILE(v_i <= v_totalListRecords) LOOP
            v_numTabId := 0;
            v_numOldTabId := 0;
            select   numOldTabId, bitFixed INTO v_numOldTabId,v_bitTabFixed FROM tt_TEMPTABMASTER WHERE ID = v_i;
            RAISE NOTICE '%',v_numOldTabId;
            IF(v_bitTabFixed = true) then
	
               RAISE NOTICE 'FIXED';
               v_numTabId := v_numOldTabId;
            ELSEIF EXISTS(SELECT numTabId FROM TabMaster WHERE numTabName =(SELECT  numTabName FROM TabMaster WHERE numTabId = v_numOldTabId LIMIT 1) AND numDomainID = v_destinationDomainId)
            then
	
               RAISE NOTICE 'EXIST';
               select   numTabId INTO v_numTabId FROM TabMaster WHERE numTabName =(SELECT  numTabName FROM TabMaster WHERE numTabId = v_numOldTabId LIMIT 1) AND numDomainID = v_destinationDomainId;
            ELSE
               RAISE NOTICE 'NEW';
               INSERT INTO TabMaster(numTabName, Remarks, tintTabType, vcURL, bitFixed, numDomainID, vcImage, vcAddURL, bitAddIsPopUp)
               SELECT numTabName, Remarks, tintTabType, vcURL, bitFixed, v_destinationDomainId, vcImage, vcAddURL, bitAddIsPopUp FROM TabMaster WHERE numTabId = v_numOldTabId;
               v_numTabId := CURRVAL('TabMaster_seq');
            end if;
            UPDATE tt_TEMPTABMASTER SET numTabId = v_numTabId WHERE ID = v_i;
            IF EXISTS(SELECT * FROM TabDefault WHERE numTabId = v_numOldTabId AND numDomainID = v_sourceDomainId) then
	
               IF EXISTS(SELECT * FROM TabDefault WHERE numTabId = v_numTabId AND numDomainID = v_destinationDomainId) then
		
                  UPDATE TabDefault SET numTabName =(SELECT   numTabName FROM TabDefault WHERE numTabId = v_numOldTabId AND numDomainID = v_sourceDomainId LIMIT 1)  WHERE numTabId = v_numTabId AND numDomainID = v_destinationDomainId;
               ELSE
                  INSERT INTO TabDefault(numTabId, numTabName, tintTabType, numDomainID)
                  SELECT  v_numTabId,numTabName,tintTabType,v_destinationDomainId FROM TabDefault WHERE numTabId = v_numOldTabId AND numDomainID = v_sourceDomainId;
               end if;
            end if;
            v_i := v_i::bigint+1;
         END LOOP;
         delete from GroupTabDetails where numGroupID = v_destinationGroupId  AND tintType <> 1;
         insert into GroupTabDetails(numGroupID,numTabId,bitallowed,numOrder,numRelationShip,tintType,numProfileID)
         SELECT v_destinationGroupId, CASE WHEN(SELECT COUNT(*) FROM tt_TEMPTABMASTER WHERE numTabId = TB.numTabId) > 0 THEN(SELECT   numTabId FROM tt_TEMPTABMASTER WHERE numTabId = TB.numTabId LIMIT 1) ELSE TB.numTabId END,
TB.bitallowed,TB.numOrder,0,TB.tintType,0 FROM
         GroupTabDetails AS TB WHERE numGroupID = v_sourceGroupId  AND tintType <> 1;
         delete from GroupTabDetails where numGroupID = v_destinationGroupId  AND tintType = 1;
         insert into GroupTabDetails(numGroupID,numTabId,bitallowed,numOrder,numRelationShip,tintType,numProfileID)
         SELECT v_destinationGroupId,(SELECT  Grp_id FROM CFw_Grp_Master CTG WHERE CTG.numDomainID = v_destinationDomainId AND CTG.Grp_Name = CFW.Grp_Name AND CTG.loc_Id = CFW.loc_Id LIMIT 1),
TB.bitallowed,TB.numOrder,0,TB.tintType,0 FROM
         GroupTabDetails AS TB LEFT JOIN CFw_Grp_Master AS CFW ON TB.numTabId = CFW.Grp_id WHERE numGroupId = v_sourceGroupId  AND TB.tintType = 1;
         DELETE FROM TreeNavigationAuthorization WHERE numDomainID = v_destinationDomainId AND numGroupID = v_destinationGroupId;
         INSERT INTO TreeNavigationAuthorization(numGroupID, numTabID, numPageNavID, bitVisible, numDomainID, tintType)
         SELECT  v_destinationGroupId,
CASE WHEN(SELECT COUNT(*) FROM tt_TEMPTABMASTER WHERE numOldTabId = T.numTabID) > 0 THEN(SELECT   numTabId FROM tt_TEMPTABMASTER WHERE numOldTabId = T.numTabID LIMIT 1) ELSE T.numTabID END,
numPageNavID, bitVisible, v_destinationDomainId, tintType FROM
         TreeNavigationAuthorization AS T WHERE T.numDomainID = v_sourceDomainId AND T.numGroupID = v_sourceGroupId;
         DROP TABLE IF EXISTS tt_TEMPTABMASTER CASCADE;
      end if;

--DELETE FROM [AccountTypeDetail] WHERE numDomainID=@destinationDomainId
--INSERT INTO 
--UPDATE Domain SET 
--	numARContactPosition=(SELECT TOP 1 numListID FROM ListDetails WHERE numDomainId=@destinationDomainId AND numListID=41 AND vcData = (SELECT TOP 1 L.vcData FROM Domain AS D LEFT JOIN 
--						 ListDetails AS L ON L.numListItemID=numARContactPosition AND L.numDomainID=@sourceDomainId WHERE D.numDomainId=@sourceDomainId)),
--	bitShowCardConnectLink = (SELECT TOP 1 bitShowCardConnectLink FROM Domain WHERE numDomainId=@sourceDomainId),
--	tintReceivePaymentTo =  (SELECT TOP 1 tintReceivePaymentTo FROM Domain WHERE numDomainId=@sourceDomainId),

--WHERE 
--	numDomainId=@destinationDomainId
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
         open SWV_RefCur for SELECT SQLERRM;
         -- ROLLBACK TRANSACTION
END;
   RETURN;
END; $$;












