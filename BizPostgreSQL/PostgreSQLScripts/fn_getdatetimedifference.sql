-- Function definition script fn_GetDateTimeDifference for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION fn_GetDateTimeDifference(v_StartDateTime TIMESTAMP,
v_EndDateTime TIMESTAMP)
RETURNS VARCHAR(50) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Seconds  INTEGER;
   v_Minute  INTEGER;
   v_Hour  INTEGER;
   v_Day  INTEGER;
   v_Week  INTEGER;
   v_month  INTEGER;
   v_Elapsed  VARCHAR(50);
   SWV_Label_value varchar(30);
BEGIN
   SWV_Label_value := 'SWL_Start_Label';
   << SWL_Label >>
   LOOP
      CASE
      WHEN SWV_Label_value = 'SWL_Start_Label' THEN
         v_Elapsed := '';
         v_Seconds := ABS((EXTRACT(DAY FROM v_EndDateTime -v_StartDateTime)*60*60*24+EXTRACT(HOUR FROM v_EndDateTime -v_StartDateTime)*60*60+EXTRACT(MINUTE FROM v_EndDateTime -v_StartDateTime)*60+EXTRACT(SECOND FROM v_EndDateTime -v_StartDateTime)));
         If v_Seconds >= 60 then

            v_Minute := v_Seconds/60;
            v_Seconds := MOD(v_Seconds,60);
            If v_Minute >= 60 then

               v_Hour := v_Minute/60;
               v_Minute := MOD(v_Minute,60);
            end if;
            if v_Hour >= 24 then

               v_Day := v_Hour/24;
               v_Hour := MOD(v_Hour,24);
            end if;
            IF v_Day >= 7 then

               v_Week := v_Day/7;
               v_Day := MOD(v_Day,7);
            Else
               SWV_Label_value := 'Final';
               CONTINUE SWL_Label;
            end if;
         end if;
         SWV_Label_value := 'Final';
      WHEN SWV_Label_value = 'Final' THEN
--IF @Week > 0 
--BEGIN
--IF  @Week > 1 
--		BEGIN
--				SET @Elapsed = @Elapsed  + Cast(@Week as Varchar) + ' Weeks '
--		END
--	ELSE
--		BEGIN
--				SET @Elapsed = @Elapsed  + Cast(@Week as Varchar) + ' Week '
--		END
--END
--ELSE
         IF v_Day > 0 then

            IF  v_Day > 1 then
		
               v_Elapsed := coalesce(v_Elapsed,'')  || SUBSTR(Cast(v_Day as VARCHAR(30)),1,30) || ' Days ';
            ELSE
               v_Elapsed := coalesce(v_Elapsed,'')  || SUBSTR(Cast(v_Day as VARCHAR(30)),1,30) || ' Day ';
            end if;
         ELSEIF v_Hour > 0
         then

            IF  v_Hour > 1 then
		
               v_Elapsed := coalesce(v_Elapsed,'')  || SUBSTR(Cast(v_Hour as VARCHAR(30)),1,30) || ' Hours ';
            ELSE
               v_Elapsed := coalesce(v_Elapsed,'')  || SUBSTR(Cast(v_Hour as VARCHAR(30)),1,30) || ' Hour ';
            end if;
         ELSEIF v_Minute > 0
         then

            IF  v_Minute > 1 then
		
               v_Elapsed := coalesce(v_Elapsed,'')  || SUBSTR(Cast(v_Minute as VARCHAR(30)),1,30) || ' Minutes ';
            ELSE
               v_Elapsed := coalesce(v_Elapsed,'')  || SUBSTR(Cast(v_Minute as VARCHAR(30)),1,30) || ' Minute ';
            end if;
         end if;
         Return(v_Elapsed);
         EXIT SWL_Label;
      END CASE;
   END LOOP;
END; $$;
/****** Object:  UserDefinedFunction [dbo].[fn_GetKitInventory]    Script Date: 08/22/2012  ******/

