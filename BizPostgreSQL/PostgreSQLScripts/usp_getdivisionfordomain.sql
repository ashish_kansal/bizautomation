-- Stored procedure definition script USP_GetDivisionForDomain for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetDivisionForDomain(v_numDomainId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT cast(Dom.numDivisionId as VARCHAR(255)) as numDivisionId,cast(LD.vcData as VARCHAR(255)) as vcData
   FROM  DomainDivision Dom
   join Listdetails LD
   on Dom.numDivisionId = LD.numListItemID
   WHERE Dom.numdomainid = v_numDomainId;
END; $$;












