-- Stored procedure definition script usp_getAssets for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_getAssets(v_numOppId NUMERIC DEFAULT 0 ,      
v_numDomainId NUMERIC DEFAULT 0 ,    
v_numItemCode NUMERIC DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   RETURN;
END; $$;      
-- THIS PROCEDURE IS NOT USED AND ITS LOGIC IS NOT RELEVANT TO BIZ NOW SO DO NOT USE IT
--if @numOppId <> 0    
--begin    
-- (select OI.vcitemName,'' as vcserialno ,0 as numWareHouseItmsDtlId,It.numItemCode,numunithour as unit,0 as [type]      
-- from opportunityitems OI      
-- join Item IT on IT.numItemcode = OI.numItemcode        
-- where OI.numoppid = @numOppid and bitserialized <> 1 and numDomainId = @numDomainId      
-- union      
-- select       
-- OI.vcitemname,vcSerialNo,o.numWareHouseItmsDtlId ,It.numItemCode ,1 as unit,1 as [type]      
-- from  OppWarehouseSerializedItem O                          
-- left join  WareHouseItmsDTL W                        
-- on O.numWarehouseItmsDTLID=W.numWareHouseItmsDTLID                                
-- join opportunityitems OI on OI.numoppitemtcode = o.numOppItemID       
-- join Item IT on IT.numItemcode = OI.numItemcode                
-- where o.numOppID=@numOppid and numDomainId = @numDomainId      
-- ) order by it.numItemCode desc      
    
--end    
--if @numItemCode <>0    
--begin     
-- select  It.numItemCode ,0 as numWareHouseItmsDTLID, vcitemName ,'' vcSerialNo ,1 as unit,0 as type from WareHouseItems                       
-- join Warehouses W                         
-- on W.numWareHouseID=WareHouseItems.numWareHouseID    
-- join Item It on It.numItemCode=WareHouseItems.numItemID                                
-- where numItemID=@numItemCode and bitSerialized=0 and It.numDomainid =@numDomainId    
    
-- union     
-- select numItemID as numItemCode ,numWareHouseItmsDTLID, vcitemName ,isnull(vcSerialNo,'') ,1 as unit,1 as type from WareHouseItmsDTL WDTL                         
-- join WareHouseItems WI                          
-- on WDTL.numWareHouseItemID=WI.numWareHouseItemID      
-- join Warehouses W                    
-- on W.numWareHouseID=WI.numWareHouseID   
--join Item It on It.numItemCode=wi.numItemID           
-- where (tintStatus is null or tintStatus=0)  and  numItemID=@numItemCode and it.numDomainid =@numDomainId    
--end    


