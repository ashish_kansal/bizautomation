-- Stored procedure definition script USP_DELETE_MESSAGEMASTER for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DELETE_MESSAGEMASTER(v_numMessageId NUMERIC(18,0) DEFAULT 0,
	v_numDomainID NUMERIC(18,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF(v_numMessageId != 0) then
	
      DELETE FROM MessageMaster WHERE numMessageId = v_numMessageId AND numDomainID = v_numDomainID;
   end if;
   RETURN;
END; $$;

	


