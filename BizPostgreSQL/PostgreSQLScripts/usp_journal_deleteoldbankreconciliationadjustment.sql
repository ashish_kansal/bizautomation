-- Stored procedure definition script USP_Journal_DeleteOldBankReconciliationAdjustment for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_Journal_DeleteOldBankReconciliationAdjustment(v_numDomainID NUMERIC(18,0)
	,v_numReconcileID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
	DECLARE
	my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
   BEGIN
      -- BEGIN TRANSACTION
IF coalesce(v_numReconcileID,0) > 0 then
		
         DELETE FROM General_Journal_Details WHERE numDomainId = v_numDomainID AND numJournalId IN(SELECT coalesce(numJOurnal_Id,0) FROM General_Journal_Header WHERE numReconcileID = v_numReconcileID AND coalesce(bitReconcileInterest,false) = false);
         DELETE FROM General_Journal_Header WHERE numDomainId = v_numDomainID AND numReconcileID = v_numReconcileID AND coalesce(bitReconcileInterest,false) = false;
      end if;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
        GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;


