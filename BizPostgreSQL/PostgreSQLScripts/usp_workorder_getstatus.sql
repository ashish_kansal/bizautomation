-- Stored procedure definition script USP_WorkOrder_GetStatus for PostgreSQL
CREATE OR REPLACE FUNCTION USP_WorkOrder_GetStatus(v_numDomainID NUMERIC(18,0)
	,v_vcWorkOrderIds VARCHAR(1000),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_TEMPWorkOrder_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPWORKORDER CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPWORKORDER
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numWOID NUMERIC(18,0),
      numItemCode NUMERIC(18,0),
      numQtyToBuild DOUBLE PRECISION,
      numWarehouseID NUMERIC(18,0),
      numWorkOrderStatus SMALLINT,
      vcWorkOrderStatus VARCHAR(300)
   );
   INSERT INTO tt_TEMPWORKORDER(numWOId
		,numItemCode
		,numQtyToBuild
		,numWarehouseID
		,numWorkOrderStatus
		,vcWorkOrderStatus)
   SELECT
   numWOID
		,numItemCode
		,numQtyToBuild
		,numWarehouseID
		,numWorkOrderStatus
		,vcWorkOrderStatus
   FROM
   GetWOStatus(v_numDomainID,v_vcWorkOrderIds);
	

	
open SWV_RefCur for SELECT  numWOID AS "numWOID",
      numItemCode AS "numItemCode",
      numQtyToBuild AS "numQtyToBuild",
      numWarehouseID AS "numWarehouseID",
      numWorkOrderStatus AS "numWorkOrderStatus",
      vcWorkOrderStatus AS "vcWorkOrderStatus" FROM tt_TEMPWORKORDER WHERE numWOId IN(SELECT ID FROM SplitIDs(v_vcWorkOrderIds,','));
END; $$;












