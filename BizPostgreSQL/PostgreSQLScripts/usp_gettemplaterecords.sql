-- Stored procedure definition script usp_GetTemplateRecords for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetTemplateRecords(v_numTemplateID NUMERIC
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select * from TemplateDocuments where numTemplateID = v_numTemplateID;
END; $$;












