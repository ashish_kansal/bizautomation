-- Stored procedure definition script USP_GetPriceLevelAndDiscount for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetPriceLevelAndDiscount(v_numDomainID NUMERIC(18,0),
    v_numItemCode NUMERIC(9,0),
    v_numWareHouseItemID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_monHighestDiscount  NUMERIC(18,0);
   v_monHighestPrice  NUMERIC(18,0);
	
   v_monListPrice  NUMERIC(18,2);
   v_monVendorCost  NUMERIC(18,2);
BEGIN
   DROP TABLE IF EXISTS tt_TMPPRICELEVEL CASCADE;
   CREATE TEMPORARY TABLE tt_TMPPRICELEVEL 
   ( 
      numItemCode NUMERIC(9,0),
      numWareHouseItemID NUMERIC(9,0), 
      monHighestPrice NUMERIC(18,0), 
      monHighestDiscount NUMERIC(18,0) 
   );
	
   v_monListPrice := 0;
   v_monVendorCost := 0;

   IF ( (v_numWareHouseItemID > 0)
   AND EXISTS(SELECT * FROM      Item
   WHERE     numItemCode = v_numItemCode
   AND charItemType = 'P')) then
        
      select   coalesce(monWListPrice,0) INTO v_monListPrice FROM    WareHouseItems WHERE   numWareHouseItemID = v_numWareHouseItemID;
      IF v_monListPrice = 0 then
         select   monListPrice INTO v_monListPrice FROM    Item WHERE   numItemCode = v_numItemCode;
      end if;
   ELSE
      select   monListPrice INTO v_monListPrice FROM    Item WHERE   numItemCode = v_numItemCode;
   end if; 

   v_monVendorCost := fn_GetVendorCost(v_numItemCode);
	
   select   CASE WHEN tintRuleType = 1
   AND tintDiscountType = 1 --Deduct from List price & Percentage
   THEN v_monListPrice -(v_monListPrice*(decDiscount/100))
   WHEN tintRuleType = 1
   AND tintDiscountType = 2 --Deduct from List price & Flat discount
   THEN v_monListPrice -decDiscount
   WHEN tintRuleType = 2
   AND tintDiscountType = 1 --Add to Primary Vendor Cost & Percentage
   THEN v_monVendorCost+(v_monVendorCost*(decDiscount/100))
   WHEN tintRuleType = 2
   AND tintDiscountType = 2 --Add to Primary Vendor Cost & Flat discount
   THEN v_monVendorCost+decDiscount
   WHEN tintRuleType = 3 --Named Price
   THEN decDiscount
   END, CASE WHEN tintRuleType = 1 AND tintDiscountType = 1 --Deduct from List price & Percentage
   THEN decDiscount
   WHEN tintRuleType = 1 AND tintDiscountType = 2 --Deduct from List price & Flat discount
   THEN(decDiscount*100)/(v_monListPrice -decDiscount)
   WHEN tintRuleType = 2 AND tintDiscountType = 1 --Add to Primary Vendor Cost & Percentage
   THEN decDiscount
   WHEN tintRuleType = 2 AND tintDiscountType = 2 --Add to Primary Vendor Cost & Flat discount
   THEN(decDiscount*100)/(v_monVendorCost+decDiscount)
   WHEN tintRuleType = 3 --Named Price 
   THEN 0
   END INTO v_monHighestPrice,v_monHighestDiscount FROM    PricingTable WHERE   coalesce(numItemCode,0) = v_numItemCode AND coalesce(numCurrencyID,0) = 0   ORDER BY numPricingID LIMIT 1; 

    
   INSERT INTO tt_TMPPRICELEVEL(numItemCode,numWareHouseItemID,monHighestPrice,monHighestDiscount)
    VALUES(v_numItemCode, v_numWareHouseItemID, coalesce(v_monHighestPrice, 0), coalesce(v_monHighestDiscount, 0));
    

open SWV_RefCur for SELECT numItemCode,numWareHouseItemID, monHighestPrice, monHighestDiscount FROM tt_TMPPRICELEVEL;

   RETURN;
END; $$;












