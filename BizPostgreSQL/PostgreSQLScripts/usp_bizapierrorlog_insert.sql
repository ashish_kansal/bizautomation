-- Stored procedure definition script USP_BizAPIErrorLog_Insert for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
-- =============================================
-- Author: Sandeep Patel
-- Create date: 10 April 2014
-- Description:	Logs biz api errors
-- =============================================
CREATE OR REPLACE FUNCTION USP_BizAPIErrorLog_Insert(v_Type VARCHAR(100),
	v_Source VARCHAR(100),
	v_Message TEXT,
    v_StackStrace TEXT,
    v_DomainID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   INSERT INTO BizAPIErrorLog (vcType,vcSource,vcMessage,vcStackStrace,numDomainID,CreatedDate)  VALUES(v_Type,
		v_Source,
		v_Message,
		v_StackStrace,
		v_DomainID,
		LOCALTIMESTAMP);
RETURN;
END; $$;


