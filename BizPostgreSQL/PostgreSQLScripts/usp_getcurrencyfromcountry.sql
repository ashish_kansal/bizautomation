-- Stored procedure definition script USP_GetCurrencyFromCountry for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetCurrencyFromCountry(v_numDomanID NUMERIC(9,0), 
v_numSiteID NUMERIC(9,0),                       
v_numCountryId NUMERIC(9,0), INOUT SWV_RefCur refcursor )
LANGUAGE plpgsql
   AS $$
BEGIN
   IF(Select COUNT(*) from Currency where numDomainId = v_numDomanID AND numCountryId = v_numCountryId AND bitEnabled = true) > 0 AND v_numCountryId > 0 then

      open SWV_RefCur for
      Select numCurrencyID,varCurrSymbol, CASE WHEN coalesce(fltExchangeRate,0) = 0 THEN 1 ELSE fltExchangeRate END AS fltExchangeRate
      from Currency
      where numDomainId = v_numDomanID AND numCountryId = v_numCountryId AND bitEnabled = true;
   ELSEIF(Select COUNT(*) from Currency C JOIN Sites S ON C.numDomainId = S.numDomainID where S.numDomainID = v_numDomanID AND S.numSiteID = v_numSiteID AND C.numCurrencyID = S.numCurrencyID AND C.bitEnabled = true) > 0
   then

      open SWV_RefCur for
      Select S.numCurrencyID,varCurrSymbol, CASE WHEN coalesce(fltExchangeRate,0) = 0 THEN 1 ELSE fltExchangeRate END AS fltExchangeRate
      from Currency C JOIN Sites S ON C.numDomainId = S.numDomainID
      where S.numDomainID = v_numDomanID AND S.numSiteID = v_numSiteID AND C.numCurrencyID = S.numCurrencyID AND C.bitEnabled = true;
   ELSE
      open SWV_RefCur for
      Select D.numCurrencyID,varCurrSymbol, CASE WHEN coalesce(fltExchangeRate,0) = 0 THEN 1 ELSE fltExchangeRate END AS fltExchangeRate
      from Currency C JOIN Domain D ON C.numDomainId = D.numDomainId
      where D.numDomainId = v_numDomanID AND C.numCurrencyID = D.numCurrencyID AND C.bitEnabled = true;
   end if;
   RETURN;
END; $$;


