-- Stored procedure definition script USP_SetMyReportsOrder for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SetMyReportsOrder(v_numUserCntID NUMERIC(9,0) DEFAULT 0,    
v_strLinks TEXT DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc  INTEGER;
BEGIN
   delete from MyReportList where numUserCntID = v_numUserCntID;    
  
   insert into MyReportList(numUserCntID,numRPTId,tintRptSequence,tintType)
   select v_numUserCntID,X.RPTID,x.Sequence,type 
   from
   XMLTABLE
	(
		'NewDataSet/Table'
		PASSING 
			CAST(v_strLinks AS XML)
		COLUMNS
			id FOR ORDINALITY,
			RPTID NUMERIC(9,0) PATH 'RPTID',
			Sequence SMALLINT PATH 'Sequence',
			Type SMALLINT PATH 'Type'
	) AS X;

RETURN;
END; $$;


