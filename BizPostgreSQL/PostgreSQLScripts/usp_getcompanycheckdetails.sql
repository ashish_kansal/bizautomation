-- Stored procedure definition script USP_GetCompanyCheckDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetCompanyCheckDetails(v_numDomainId NUMERIC(9,0) DEFAULT 0,     
 v_numCheckId NUMERIC(9,0) DEFAULT 0,         
 v_numCheckCompanyId NUMERIC(9,0) DEFAULT 0,    
 v_byteMode SMALLINT DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_lSQL  VARCHAR(8000);
BEGIN
   if v_byteMode = 0 then
 
      v_lSQL := 'Select DM.numDivisionID AS numDivisionID,CI.vcCompanyName AS vcCompanyName,CD.numCheckCompanyId as numCheckCompanyId,CD.monAmount as monAmount,              
  CD.vcMemo as vcMemo From CheckCompanyDetails CD              
  inner join DivisionMaster DM on CD.numCompanyId = DM.numDivisionID               
  inner join CompanyInfo CI on DM.numCompanyID = CI.numCompanyId         
  Where CD.numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(100)),1,100) || ' And CD.numCheckCompanyId not in(Select coalesce(numCheckCompanyId,0) From CheckDetails Where  CheckDetails.numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || ')';
   ELSEIF v_byteMode = 1
   then
 
      v_lSQL := 'Select DM.numDivisionID AS numDivisionID,CI.vcCompanyName AS vcCompanyName,CD.numCheckCompanyId as numCheckCompanyId,CD.monAmount as monAmount,              
  CD.vcMemo as vcMemo From CheckCompanyDetails CD              
  inner join DivisionMaster DM on CD.numCompanyId = DM.numDivisionID               
  inner join CompanyInfo CI on DM.numCompanyID = CI.numCompanyId         
  Where CD.numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(100)),1,100) || ' And CD.numCheckCompanyId not in(Select coalesce(numCheckCompanyId,0) From CheckDetails Where CheckDetails.numCheckId <>' || SUBSTR(CAST(v_numCheckId AS VARCHAR(10)),1,10) || ' And CheckDetails.numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || ')';
   end if;    
    
   if v_numCheckCompanyId <> 0 then 
      v_lSQL := coalesce(v_lSQL,'') || 'And CD.numCheckCompanyId <>' || SUBSTR(CAST(v_numCheckCompanyId AS VARCHAR(100)),1,100);
   end if;          
   RAISE NOTICE '%',v_lSQL;        
   OPEN SWV_RefCur FOR EXECUTE v_lSQL;
   RETURN;
END; $$;


