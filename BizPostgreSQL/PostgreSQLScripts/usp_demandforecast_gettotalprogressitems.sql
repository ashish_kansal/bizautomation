-- Stored procedure definition script USP_DemandForecast_GetTotalProgressItems for PostgreSQL
CREATE OR REPLACE FUNCTION USP_DemandForecast_GetTotalProgressItems(v_numDomainID NUMERIC(18,0)
	,v_numDFID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDays  INTEGER;
   v_dtTo  DATE DEFAULT CAST(LOCALTIMESTAMP+CAST(v_numDays || 'day' as interval) AS DATE);




   v_bitWarehouseFilter  BOOLEAN DEFAULT false;
   v_bitItemClassificationFilter  BOOLEAN DEFAULT false;
   v_bitItemGroupFilter  BOOLEAN DEFAULT false;
BEGIN
   select   coalesce(DemandForecastDays.numDays,0) INTO v_numDays FROM
   DemandForecast
   INNER JOIN
   DemandForecastDays
   ON
   DemandForecast.numDFDaysID = DemandForecastDays.numDFDaysID WHERE
   numDFID = v_numDFID;


   IF(SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = v_numDFID) > 0 then
	
      v_bitWarehouseFilter := true;
   end if;

   IF(SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = v_numDFID) > 0 then
	
      v_bitItemClassificationFilter := true;
   end if;

   IF(SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = v_numDFID) > 0 then
	
      v_bitItemGroupFilter := true;
   end if;

   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      numOppID NUMERIC(18,0),
      numOppItemID NUMERIC(18,0),
      numItemCode NUMERIC(18,0),
      numWarehouseItemID NUMERIC(18,0),
      bitKit BOOLEAN,
      bitAssembly BOOLEAN,
      vcItemName VARCHAR(300),
      vcCustomer VARCHAR(300),
      vcOppName VARCHAR(300),
      dtEstimatedClosedDate VARCHAR(15),
      vcUOM VARCHAR(50),
      vcTotalProgress VARCHAR(100),
      numOrderReleaseQty DOUBLE PRECISION,
      numPurchasedQty DOUBLE PRECISION,
      fltUOMConversionFactor DOUBLE PRECISION
   );


	-- GET ITEMS QTY BASED ON SALES OPORTUNITIES TOTAL PROGRESS
   INSERT INTO
   tt_TEMP
   SELECT
   OM.numOppId
		,OI.numoppitemtCode
		,I.numItemCode
		,(CASE
   WHEN coalesce(I.numItemGroup,0) > 0 AND LENGTH(fn_GetAttributes(WI.numWareHouseItemID,0::BOOLEAN)) > 0
   THEN(CASE
      WHEN EXISTS(SELECT numWarehouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID = v_numDomainID AND  WIInner.numItemID = I.numItemCode AND WIInner.numWareHouseID = WI.numWareHouseID AND WIInner.numWareHouseID = -1 AND  fn_GetAttributes(WIInner.numWareHouseItemID,0::BOOLEAN) = fn_GetAttributes(WI.numWareHouseItemID,0::BOOLEAN))
      THEN(SELECT  numWarehouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID = v_numDomainID AND  WIInner.numItemID = I.numItemCode AND WIInner.numWareHouseID = WI.numWareHouseID AND WIInner.numWareHouseID = -1 AND  fn_GetAttributes(WIInner.numWareHouseItemID,0::BOOLEAN) = fn_GetAttributes(WI.numWareHouseItemID,0::BOOLEAN) LIMIT 1)
      ELSE
         WI.numWareHouseItemID
      END)
   ELSE(SELECT  numWarehouseItemID FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numItemID = I.numItemCode AND numWareHouseID = WI.numWareHouseID AND numWLocationID = -1 LIMIT 1)
   END) AS numWarehouseItemID
		,coalesce(I.bitKitParent,false)
		,coalesce(I.bitAssembly,false)
		,coalesce(I.vcItemName,'')
		,coalesce(CI.vcCompanyName,'')
		,coalesce(OM.vcpOppName,'')
		,FormatedDateFromDate(OM.intPEstimatedCloseDate,v_numDomainID)
		,coalesce((SELECT vcUnitName FROM UOM WHERE numUOMId = OI.numUOMId),'-')
		,CONCAT(PP.intTotalProgress,'% of',OI.numUnitHour)
		,CEIL(CAST(OI.numUnitHour -coalesce(TEMPItemRelease.numItemReleaseQty,0) AS DOUBLE PRECISION)*(PP.intTotalProgress/100))
		,coalesce(TEMPItemRelease.numItemReleaseQty,0)
		,fn_UOMConversion(coalesce(OI.numUOMId,0),I.numItemCode,I.numDomainID,coalesce(I.numBaseUnit,0))
   FROM
   OpportunityMaster OM
   INNER JOIN
   DivisionMaster DM
   ON
   DM.numDivisionID = OM.numDivisionId
   INNER JOIN
   CompanyInfo CI
   ON
   DM.numCompanyID = CI.numCompanyId
   INNER JOIN
   OpportunityItems OI
   ON
   OM.numOppId = OI.numOppId
   INNER JOIN
   Item I
   ON
   OI.numItemCode = I.numItemCode
   INNER JOIN
   WareHouseItems WI
   ON
   OI.numWarehouseItmsID = WI.numWareHouseItemID
   LEFT JOIN
   ProjectProgress PP
   ON
   OM.numOppId = PP.numOppId
   LEFT JOIN LATERAL(SELECT
      SUM(numQty) AS numItemReleaseQty
      FROM
      OpportunityItemsReleaseDates OIRD
      WHERE
      OIRD.numOppId = OM.numOppId
      AND OIRD.numOppItemID = OI.numoppitemtCode
      AND ((OIRD.dtReleaseDate <= v_dtTo AND coalesce(OIRD.tintStatus,0) = 1) OR coalesce(OIRD.tintStatus,0) = 2)) TEMPItemRelease on TRUE
   WHERE
   OM.numDomainId = v_numDomainID
   AND OM.tintopptype = 1
   AND coalesce(OM.tintoppstatus,0) = 0
   AND (OM.intPEstimatedCloseDate IS NOT NULL AND CAST(OM.intPEstimatedCloseDate AS DATE) <= v_dtTo)
   AND coalesce(numReleaseStatus,1) = 1
   AND coalesce(PP.intTotalProgress,0) > 0
   AND
		((SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = v_numDFID) = 0 OR
   WI.numWareHouseID IN(SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID = v_numDFID))
   AND
		((SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = v_numDFID) = 0 OR
   I.numItemClassification IN(SELECT numItemClassification FROM DemandForecastItemClassification WHERE numDFID = v_numDFID))
   AND
		((SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = v_numDFID) = 0 OR
   I.numItemGroup IN(SELECT numItemGroup FROM DemandForecastItemGroup WHERE numDFID = v_numDFID));

	
open SWV_RefCur for SELECT * FROM tt_TEMP WHERE coalesce(numOrderReleaseQty,0) > 0 AND coalesce(numWarehouseItemID,0) > 0 ORDER BY numItemCode,numOppID,numOppItemID;
   RETURN;
END; $$;













