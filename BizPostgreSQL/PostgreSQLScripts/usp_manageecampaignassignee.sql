-- Stored procedure definition script USP_ManageECampaignAssignee for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageECampaignAssignee(v_numECampaignAssigneeID NUMERIC(9,0)  DEFAULT 0,
          v_numECampaignID         NUMERIC(9,0) DEFAULT NULL,
          v_numEmailGroupID        NUMERIC(9,0) DEFAULT NULL,
          v_dtStartDate            TIMESTAMP DEFAULT null,
          v_numDomainID            NUMERIC(9,0) DEFAULT NULL,
          v_numUserCntID           NUMERIC(9,0) DEFAULT NULL,
          v_bitMode BOOLEAN DEFAULT false, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_bitMode = true then --delete record

      RAISE NOTICE 'inside';
      DELETE FROM ECampaignAssignee WHERE
      numECampaignAssigneeID = v_numECampaignAssigneeID AND numDomainID = v_numDomainID;
      open SWV_RefCur for
      SELECT 0;
      RETURN;
   ELSE
      RAISE NOTICE 'insidef';
      IF v_numECampaignAssigneeID = 0 then
    
         INSERT INTO ECampaignAssignee(numECampaignID,
                  numEmailGroupID,
                  dtStartDate,
                  numDomainID,
                  numCreatedBy,
                  dtCreateDate,
                  numModifiedBy,
                  dtModifiedDate)
      VALUES(v_numECampaignID,
                  v_numEmailGroupID,
                  v_dtStartDate,
                  v_numDomainID,
                  v_numUserCntID,
                  TIMEZONE('UTC',now()),
                  v_numUserCntID,
                  TIMEZONE('UTC',now()));
      
         v_numECampaignAssigneeID := CURRVAL('ECampaignAssignee_seq');
      ELSE
         UPDATE ECampaignAssignee
         SET    numECampaignID = v_numECampaignID,numEmailGroupID = v_numEmailGroupID,
         dtStartDate = v_dtStartDate,numModifiedBy = v_numUserCntID,dtModifiedDate = TIMEZONE('UTC',now())
         WHERE  numECampaignAssigneeID = v_numECampaignAssigneeID;
      end if;
      open SWV_RefCur for
      SELECT v_numECampaignAssigneeID;
   end if;
END; $$;



