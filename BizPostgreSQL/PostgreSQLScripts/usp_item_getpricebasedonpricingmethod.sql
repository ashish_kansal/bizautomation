-- Stored procedure definition script USP_Item_GetPriceBasedOnPricingMethod for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Item_GetPriceBasedOnPricingMethod(v_numDomainID NUMERIC(18,0)
	,v_numDivisionID NUMERIC(18,0)
	,v_numItemCode NUMERIC(18,0)
	,v_numWarehouseItemID NUMERIC(18,0) 
	,v_numQty DOUBLE PRECISION
	,v_vcChildKits TEXT
	,v_numCurrencyID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_fltExchangeRate  DOUBLE PRECISION;
   v_monPrice  DECIMAL(20,5);
BEGIN
   v_fltExchangeRate := coalesce((SELECT fltExchangeRate FROM Currency WHERE numDomainId = v_numDomainID AND numCurrencyID = v_numCurrencyID),0);

   IF coalesce(v_fltExchangeRate,0) = 0 then
	
      v_fltExchangeRate := 1;
   end if;

   select(CASE
   WHEN EXISTS(SELECT ID FROM ItemCurrencyPrice WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode AND numCurrencyID = coalesce(v_numCurrencyID,0))
   THEN coalesce((SELECT monListPrice FROM ItemCurrencyPrice WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode AND numCurrencyID = coalesce(v_numCurrencyID,0)),0)
   ELSE(CASE WHEN v_fltExchangeRate <> 1 THEN CAST((coalesce(Item.monListPrice,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(Item.monListPrice,0) END)
   END) INTO v_monPrice FROM Item WHERE numItemCode = v_numItemCode;

   IF EXISTS(SELECT
   numItemCode
   FROM
   Item
   WHERE
   numDomainID = v_numDomainID
   AND numItemCode = v_numItemCode
   AND coalesce(bitCalAmtBasedonDepItems,false) = true
   AND (coalesce(bitKitParent,false) = true OR coalesce(bitAssembly,false) = true)) then
	
      v_monPrice := GetCalculatedPriceForKitAssembly(v_numDomainID,v_numDivisionID,v_numItemCode,v_numQty,v_numWarehouseItemID,
      1::SMALLINT,0,0,v_vcChildKits,v_numCurrencyID,v_fltExchangeRate);
   ELSE
      select   monFinalPrice INTO v_monPrice FROM
      fn_GetItemPriceBasedOnMethod(v_numDomainID,v_numDivisionID,v_numItemCode,v_numQty,false,0,v_numWarehouseItemID,
      v_vcChildKits,v_numCurrencyID);
   end if;

   open SWV_RefCur for SELECT v_monPrice AS monPrice;
END; $$;












