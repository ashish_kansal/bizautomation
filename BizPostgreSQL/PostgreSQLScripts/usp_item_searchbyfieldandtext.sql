CREATE OR REPLACE FUNCTION USP_Item_SearchByFieldAndText(v_numDomainId NUMERIC(18,0)
	,v_numDivisionID NUMERIC(18,0)
	,v_numFieldID NUMERIC(18,0)
	,v_vcSearchText VARCHAR(300), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_monCost  DECIMAL(20,5) DEFAULT 0;

   v_avgCost  INTEGER;
   v_numCompanyID  NUMERIC(18,0);
BEGIN
   v_avgCost := coalesce((SELECT 1 AS numCost FROM Domain WHERE numDomainId = v_numDomainId),
   0);

   IF v_numFieldID = 281 then --SKU
	
      open SWV_RefCur for
      SELECT 
      Item.numItemCode
			,Item.vcItemName
			,Item.vcModelID
			,Item.charItemType
			,Item.txtItemDesc
			,Item.bitKitParent
			,Item.vcSKU
			,(CASE v_avgCost WHEN 3 THEN coalesce(Vendor.monCost,0) WHEN 2 THEN 0 ELSE coalesce(monAverageCost,0) END) AS monCost
      FROM
      Item
      LEFT JOIN
      Vendor
      ON
      Item.numVendorID = Vendor.numVendorID
      AND Item.numItemCode = Vendor.numItemCode
      WHERE
      LOWER(Item.vcSKU) = LOWER(v_vcSearchText)
      AND Item.numDomainID = v_numDomainId
      ORDER BY
      numItemCode LIMIT 1;
   ELSEIF v_numFieldID = 203
   then  -- UPC
	
      open SWV_RefCur for
      SELECT 
      Item.numItemCode
			,Item.vcItemName
			,Item.vcModelID
			,Item.charItemType
			,Item.txtItemDesc
			,Item.bitKitParent
			,Item.vcSKU
			,(CASE v_avgCost WHEN 3 THEN coalesce(Vendor.monCost,0) WHEN 2 THEN 0 ELSE coalesce(monAverageCost,0) END) AS monCost
      FROM
      Item
      LEFT JOIN
      Vendor
      ON
      Item.numVendorID = Vendor.numVendorID
      AND Item.numItemCode = Vendor.numItemCode
      WHERE
      LOWER(Item.numBarCodeId) = LOWER(v_vcSearchText)
      AND Item.numDomainID = v_numDomainId
      ORDER BY
      numItemCode LIMIT 1;
   ELSEIF v_numFieldID = 189
   then  -- ItemName
	
      open SWV_RefCur for
      SELECT 
      Item.numItemCode
			,Item.vcItemName
			,Item.vcModelID
			,Item.charItemType
			,Item.txtItemDesc
			,Item.bitKitParent
			,Item.vcSKU
			,(CASE v_avgCost WHEN 3 THEN coalesce(Vendor.monCost,0) WHEN 2 THEN 0 ELSE coalesce(monAverageCost,0) END) AS monCost
      FROM
      Item
      LEFT JOIN
      Vendor
      ON
      Item.numVendorID = Vendor.numVendorID
      AND Item.numItemCode = Vendor.numItemCode
      WHERE
      LOWER(Item.vcItemName) = LOWER(v_vcSearchText)
      AND Item.numDomainID = v_numDomainId
      ORDER BY
      numItemCode LIMIT 1;
   ELSEIF v_numFieldID = 211
   then   -- ItemCode
	
      open SWV_RefCur for
      SELECT 
      Item.numItemCode
			,Item.vcItemName
			,Item.vcModelID
			,Item.charItemType
			,Item.txtItemDesc
			,Item.bitKitParent
			,Item.vcSKU
			,(CASE v_avgCost WHEN 3 THEN coalesce(Vendor.monCost,0) WHEN 2 THEN 0 ELSE coalesce(monAverageCost,0) END) AS monCost
      FROM
      Item
      LEFT JOIN
      Vendor
      ON
      Item.numVendorID = Vendor.numVendorID
      AND Item.numItemCode = Vendor.numItemCode
      WHERE
      Item.numItemCode::VARCHAR = LOWER(v_vcSearchText)
      AND Item.numDomainID = v_numDomainId
      ORDER BY
      numItemCode LIMIT 1;
   ELSEIF v_numFieldID = 899
   then  -- CustomerPart#
      select   numCompanyID INTO v_numCompanyID FROM DivisionMaster WHERE numDivisionID = v_numDivisionID;
      open SWV_RefCur for
      SELECT 
      I.numItemCode
			,I.vcItemName
			,I.vcModelID
			,I.charItemType
			,I.txtItemDesc
			,I.bitKitParent
			,I.vcSKU
			,(CASE v_avgCost WHEN 3 THEN coalesce(Vendor.monCost,0) WHEN 2 THEN 0 ELSE coalesce(monAverageCost,0) END) AS monCost
      FROM
      Item I
      LEFT JOIN
      Vendor
      ON
      I.numVendorID = Vendor.numVendorID
      AND I.numItemCode = Vendor.numItemCode
      INNER JOIN
      CustomerPartNumber CPN
      ON
      I.numItemCode = CPN.numItemCode
      WHERE
      CPN.numDomainID = v_numDomainId
      AND LOWER(CPN.CustomerPartNo) = LOWER(v_vcSearchText)
      AND CPN.numCompanyId = v_numCompanyID
      ORDER BY
      I.numItemCode LIMIT 1;
   ELSEIF v_numFieldID = 291
   then -- VendorPart#
	
      open SWV_RefCur for
      SELECT 
      I.numItemCode
			,I.vcItemName
			,I.vcModelID
			,I.charItemType
			,I.txtItemDesc
			,I.bitKitParent
			,I.vcSKU
			,(CASE v_avgCost WHEN 3 THEN coalesce(V.monCost,0) WHEN 2 THEN 0 ELSE coalesce(monAverageCost,0) END) AS monCost
      FROM
      Item I
      INNER JOIN
      Vendor V
      ON
      I.numItemCode = V.numItemCode
      WHERE
      V.numDomainID = v_numDomainId
      AND LOWER(V.vcPartNo) = LOWER(v_vcSearchText)
      ORDER BY
      I.numItemCode LIMIT 1;
   end if;
   RETURN;
END; $$;
