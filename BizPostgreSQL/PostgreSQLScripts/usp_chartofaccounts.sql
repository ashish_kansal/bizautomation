-- Stored procedure definition script USP_ChartofAccounts for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ChartofAccounts(v_numDomainId NUMERIC(9,0) DEFAULT 0,      
v_numUserCntId NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   RETURN; 
-- Select numAccountId,numParntAcntId,vcCatgyName,vcData,numOpeningBal,numAcntType From Chart_Of_Accounts        
-- left join ListDetails         
-- on numAcntType=ListDetails.numListItemID         
-- Where  Chart_Of_Accounts.numDomainId=@numDomainId  order by numParntAcntId,numAccountID           
END; $$;


