DROP FUNCTION IF EXISTS USP_OPPGetINItems;

CREATE OR REPLACE FUNCTION USP_OPPGetINItems(v_byteMode SMALLINT  DEFAULT NULL,
	v_numOppId NUMERIC(18,0)  DEFAULT NULL,
	v_numOppBizDocsId NUMERIC(18,0)  DEFAULT NULL,
	v_numDomainID NUMERIC(18,0)  DEFAULT 0,
	v_numUserCntID NUMERIC(9,0)  DEFAULT 0,
	v_ClientTimeZoneOffset INTEGER DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
	-- TRANSFERRED PARAMETER VALUES TO TEMPORARY VARIABLES TO AVAOID PARAMETER SNIFFING
   AS $$
   DECLARE
   v_byteModeTemp  SMALLINT;
   v_numOppIdTemp  NUMERIC(18,0);
   v_numOppBizDocsIdTemp  NUMERIC(18,0);
   v_numDomainIDTemp  NUMERIC(18,0);
   v_numUserCntIDTemp  NUMERIC(18,0);
   v_ClientTimeZoneOffsetTemp  INTEGER;
   v_BizDcocName  VARCHAR(100);
   v_OppName  VARCHAR(100);
   v_Contactid  VARCHAR(100);
   v_shipAmount  DECIMAL(20,5);
   v_tintOppType  SMALLINT;
   v_numlistitemid  VARCHAR(15);
   v_RecOwner  VARCHAR(15);
   v_MonAmount  VARCHAR(15);
   v_OppBizDocID  VARCHAR(100);
   v_bizdocOwner  VARCHAR(15);
   v_tintBillType  SMALLINT;
   v_tintShipType  SMALLINT;
   v_numCustomerDivID  NUMERIC(18,0);
   v_numDivisionID  NUMERIC(18,0);

   v_numARContactPosition  NUMERIC(18,0);
   v_numBizDocID  NUMERIC(18,0);
BEGIN
   v_byteModeTemp := v_byteMode;
   v_numOppIdTemp := v_numOppId;
   v_numOppBizDocsIdTemp := v_numOppBizDocsId;
   v_numDomainIDTemp := v_numDomainID;
   v_numUserCntIDTemp := v_numUserCntID;
   v_ClientTimeZoneOffsetTemp := v_ClientTimeZoneOffset;


   IF v_byteModeTemp = 1 then
      select   coalesce(numARContactPosition,0) INTO v_numARContactPosition FROM Domain WHERE numDomainId = v_numDomainID;
      select   numrecowner, SUBSTR(CAST(monPAmount AS VARCHAR(20)),1,20), numContactId, vcpOppName, tintopptype, tintBillToType, tintShipToType, numDivisionId INTO v_RecOwner,v_MonAmount,v_Contactid,v_OppName,v_tintOppType,v_tintBillType,
      v_tintShipType,v_numDivisionID FROM   OpportunityMaster WHERE  numOppId = v_numOppIdTemp;
      
      -- When Creating PO from SO and Bill type is Customer selected 
      select   coalesce(numDivisionId,0) INTO v_numCustomerDivID FROM OpportunityMaster WHERE
      numOppId IN(SELECT  coalesce(numParentOppID,0) FROM OpportunityLinking WHERE numChildOppID = v_numOppIdTemp);
			
      --select @BizDcocName=vcBizDocID from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsIdTemp
      select   vcdata, monShipCost, numlistitemid, vcBizDocID, OpportunityBizDocs.numCreatedBy, coalesce(OpportunityBizDocs.numBizDocId,0) INTO v_BizDcocName,v_shipAmount,v_numlistitemid,v_OppBizDocID,v_bizdocOwner,
      v_numBizDocID FROM   Listdetails JOIN OpportunityBizDocs ON numBizDocId = numlistitemid WHERE  numOppBizDocsId = v_numOppBizDocsIdTemp;
      IF v_numBizDocID = 287 AND coalesce(v_numARContactPosition,0) > 0 AND EXISTS(SELECT numContactId FROM AdditionalContactsInformation ADC WHERE ADC.numDivisionId = v_numDivisionID AND ADC.vcPosition = v_numARContactPosition) then
		
         select   numContactId INTO v_Contactid FROM AdditionalContactsInformation ADC WHERE ADC.numDivisionId = v_numDivisionID AND ADC.vcPosition = v_numARContactPosition    LIMIT 1;
      end if;
      open SWV_RefCur for
      SELECT v_bizdocOwner AS BizDocOwner,
             v_OppBizDocID AS BizDocID,
             v_MonAmount AS Amount,
             coalesce(v_RecOwner,cast(1 as TEXT)) AS Owner,
             v_BizDcocName AS BizDcocName,
             v_numlistitemid AS BizDoc,
             v_OppName AS OppName,
             VcCompanyName AS CompName,
             v_shipAmount AS ShipAmount,
             fn_getOPPAddress(v_numOppIdTemp,v_numDomainIDTemp,1::SMALLINT) AS BillAdd,
             fn_getOPPAddress(v_numOppIdTemp,v_numDomainIDTemp,2::SMALLINT) AS ShipAdd,
             AD.vcStreet,
             AD.vcCity,
             coalesce(fn_GetState(AD.numState),'') AS vcBilState,
             AD.vcPostalCode,
             coalesce(fn_GetListItemName(AD.numCountry),'') AS vcBillCountry,
             con.vcFirstName AS ConName,
             coalesce(con.numPhone,'') AS numPhone,
             con.vcFax,
             coalesce(con.vcEmail,'') AS vcEmail,
             div.numDivisionID,
             numContactId,
             coalesce(vcFirstFldValue,'') AS vcFirstFldValue,
             coalesce(vcSecndFldValue,'') AS vcSecndFldValue,
             coalesce(bitTest,false) AS bitTest,
			fn_getOPPAddress(v_numOppIdTemp,v_numDomainIDTemp,1::SMALLINT) AS BillToAddressName,
			fn_getOPPAddress(v_numOppIdTemp,v_numDomainIDTemp,1::SMALLINT) AS ShipToAddressName,
             CASE
      WHEN v_tintBillType IS NULL THEN Com.vcCompanyName
             	-- When Sales order and bill to is set to customer 
      WHEN v_tintBillType = 1 AND v_tintOppType = 1 THEN Com.vcCompanyName
             -- When Create PO from SO and Bill to is set to Customer 
      WHEN v_tintBillType = 1 AND v_tintOppType = 2 THEN fn_GetComapnyName(v_numCustomerDivID)
      WHEN v_tintBillType = 0 THEN(SELECT  Com1.vcCompanyName
            FROM   CompanyInfo Com1
            JOIN DivisionMaster div1
            ON Com1.numCompanyId = div1.numCompanyID
            JOIN Domain D1
            ON D1.numDivisionId = div1.numDivisionID
            JOIN AddressDetails AD1
            ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary = true
            WHERE  D1.numDomainId = v_numDomainIDTemp LIMIT 1)
      WHEN v_tintBillType = 2 THEN(SELECT vcBillCompanyName
            FROM   OpportunityAddress
            WHERE  numOppID = v_numOppIdTemp)
      WHEN v_tintBillType = 3 THEN  Com.vcCompanyName
      END AS BillToCompanyName,
             CASE
      WHEN v_tintShipType IS NULL THEN Com.vcCompanyName
      WHEN v_tintShipType = 1 AND v_tintOppType = 1 THEN Com.vcCompanyName
				-- When Create PO from SO and Ship to is set to Customer 
      WHEN v_tintShipType = 1 AND v_tintOppType = 2 THEN fn_GetComapnyName(v_numCustomerDivID)
      WHEN v_tintShipType = 0 THEN(SELECT  Com1.vcCompanyName
            FROM   CompanyInfo Com1
            JOIN DivisionMaster div1
            ON Com1.numCompanyId = div1.numCompanyID
            JOIN Domain D1
            ON D1.numDivisionId = div1.numDivisionID
            JOIN AddressDetails AD1
            ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 2 AND bitIsPrimary = true
            WHERE  D1.numDomainId = v_numDomainIDTemp LIMIT 1)
      WHEN v_tintShipType = 2 THEN(SELECT vcShipCompanyName
            FROM   OpportunityAddress
            WHERE  numOppID = v_numOppIdTemp)
      WHEN v_tintShipType = 3 THEN(SELECT vcShipCompanyName
            FROM   OpportunityAddress
            WHERE  numOppID = v_numOppIdTemp)
      END AS ShipToCompanyName
      FROM   CompanyInfo Com JOIN DivisionMaster div ON Com.numCompanyId = div.numCompanyID
      JOIN AdditionalContactsInformation con ON con.numDivisionId = div.numDivisionID
      JOIN Domain D ON D.numDomainId = div.numDomainID
      LEFT JOIN PaymentGatewayDTLID PGDTL ON PGDTL.intPaymentGateWay = D.intPaymentGateWay AND D.numDomainId = PGDTL.numDomainID
      LEFT JOIN AddressDetails AD --Billing add
      ON AD.numDomainID = div.numDomainID AND AD.numRecordID = div.numDivisionID AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary = true
      LEFT JOIN AddressDetails AD2--Shipping address
      ON AD2.numDomainID = div.numDomainID AND AD2.numRecordID = div.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary = true
      WHERE  numContactId = cast(NULLIF(v_Contactid,'') as NUMERIC(18,0)) AND div.numDomainID = v_numDomainIDTemp;
   end if;
   IF v_byteModeTemp = 2 then
    
      open SWV_RefCur for
      SELECT opp.vcBizDocID,
             Mst.fltDiscount AS decDiscount,
             coalesce(opp.monAmountPaid,0) AS monAmountPaid,
             coalesce(opp.vcComments,'') AS vcComments,
             CAST(opp.dtCreatedDate+CAST(-v_ClientTimeZoneOffsetTemp || 'minute' as interval) AS VARCHAR(20)) AS dtCreatedDate,
			 fn_GetContactName(opp.numCreatedBy) || ' ' || CAST(opp.dtCreatedDate+CAST(-v_ClientTimeZoneOffsetTemp || 'minute' as interval) AS VARCHAR(20)) AS CreatedBy,
             fn_GetContactName(opp.numCreatedBy) AS numCreatedby,
             fn_GetContactName(opp.numModifiedBy) AS numModifiedBy,
             CAST(opp.dtModifiedDate+CAST(-v_ClientTimeZoneOffsetTemp || 'minute' as interval) AS VARCHAR(20)) AS dtModifiedDate,
			 fn_GetContactName(opp.numModifiedBy) || ' ' || CAST(opp.dtModifiedDate+CAST(-v_ClientTimeZoneOffsetTemp || 'minute' as interval) AS VARCHAR(20)) AS ModifiedBy,
             CASE
      WHEN coalesce(opp.numViewedBy,0) = 0 THEN ''
      ELSE fn_GetContactName(opp.numViewedBy)
      END AS numViewedBy,
             CASE
      WHEN opp.numViewedBy = 0 THEN NULL
      ELSE opp.dtViewedDate
      END AS dtViewedDate,
             (CASE WHEN coalesce(Mst.intBillingDays,0) > 0 THEN true ELSE coalesce(Mst.bitBillingTerms,false) END) AS tintBillingTerms,
             coalesce(Mst.intBillingDays,0) AS numBillingDays,
			 coalesce(BTR.numNetDueInDays,0) AS numBillingDaysName,
			 BTR.vcTerms AS vcBillingTermsName,
             coalesce(Mst.bitInterestType,false) AS tintInterestType,
             coalesce(Mst.fltInterest,0) AS fltInterest,
             tintopptype,
			 tintoppstatus,
             opp.numBizDocId,
             fn_GetContactName(numApprovedBy) AS ApprovedBy,
             dtApprovedDate,
             Mst.bintAccountClosingDate,
             tintShipToType,
             tintBillToType,
             tintshipped,
             dtShippedDate AS bintShippedDate,
             coalesce(opp.monShipCost,0) AS monShipCost,
             (SELECT COUNT(* )
         FROM   DocumentWorkflow
         WHERE  numDocID = v_numOppBizDocsIdTemp
         AND numContactID = v_numUserCntIDTemp
         AND cDocType = 'B'
         AND tintApprove = 0) AS AppReq,
             (SELECT COUNT(* )
         FROM   DocumentWorkflow
         WHERE  numDocID = v_numOppBizDocsIdTemp
         AND cDocType = 'B'
         AND tintApprove = 1) AS Approved,
             (SELECT COUNT(* )
         FROM   DocumentWorkflow
         WHERE  numDocID = v_numOppBizDocsIdTemp
         AND cDocType = 'B'
         AND tintApprove = 2) AS Declined,
             (SELECT COUNT(* )
         FROM   DocumentWorkflow
         WHERE  numDocID = v_numOppBizDocsIdTemp
         AND cDocType = 'B'
         AND tintApprove = 0) AS Pending,
             coalesce(BT.vcBizDocFooter,D.vcBizDocFooter) AS BizdocFooter,
             coalesce(BT.vcBizDocImagePath,D.vcBizDocImagePath) As vcBizDocImagePath,
             Mst.numDivisionId,
             coalesce(BT.vcPurBizDocFooter,D.vcPurBizDocFooter) AS PurBizdocFooter,
             coalesce(Mst.fltDiscount,0) AS fltDiscount,
             coalesce(Mst.bitDiscountType,false) AS bitDiscountType,
             coalesce(opp.numShipVia,coalesce(Mst.intUsedShippingCompany,0)) AS numShipVia,
--             isnull(vcTrackingURL,'') AS vcTrackingURL,
			coalesce((SELECT vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID = Mst.numDomainId AND intShipFieldID IN(SELECT intShipFieldID FROM ShippingFields WHERE numListItemID = opp.numShipVia  AND vcFieldName = 'Tracking URL')),'')		AS vcTrackingURL,
             coalesce(opp.numBizDocStatus,0) AS numBizDocStatus,
             CASE
      WHEN opp.numBizDocStatus IS NULL THEN '-'
      ELSE fn_GetListItemName(opp.numBizDocStatus)
      END AS BizDocStatus,
             CASE
      WHEN coalesce(opp.numShipVia,0) = 0 THEN(CASE WHEN Mst.intUsedShippingCompany IS NULL THEN '-' WHEN Mst.intUsedShippingCompany = -1 THEN 'Will-call' ELSE fn_GetListItemName(Mst.intUsedShippingCompany::NUMERIC) END)
      WHEN opp.numShipVia = -1 THEN 'Will-call'
      ELSE fn_GetListItemName(opp.numShipVia)
      END AS ShipVia,
			 coalesce((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID = v_numDomainIDTemp OR coalesce(numDomainID,0) = 0) AND numShippingServiceID = Mst.numShippingService),'') AS vcShippingService,
             coalesce(C.varCurrSymbol,'') AS varCurrSymbol,
             (SELECT COUNT(*) FROM ShippingReport WHERE numOppBizDocId = opp.numOppBizDocsId) AS ShippingReportCount,
			 coalesce(bitPartialFulfilment,false) AS bitPartialFulfilment,
			 coalesce(opp.vcBizDocName,'') AS vcBizDocName,
			 fn_GetContactName(opp.numModifiedBy)  || ', '
      || CAST(opp.dtModifiedDate+CAST(-v_ClientTimeZoneOffsetTemp || 'minute' as interval) AS VARCHAR(20)) AS ModifiedBy,
			 fn_GetContactName(Mst.numrecowner) || ', ' ||  fn_GetContactName(Mst.numassignedto) AS OrderRecOwner,
			 fn_GetContactName(DM.numRecOwner) || ', ' ||  fn_GetContactName(DM.numAssignedTo) AS AccountRecOwner,
			 opp.dtFromDate+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval) AS dtFromDate,
			 Mst.tintTaxOperator,
			 GetAccountedEmbeddedCost(opp.numOppBizDocsId,1::SMALLINT) AS monTotalEmbeddedCost,
			 GetAccountedEmbeddedCost(opp.numOppBizDocsId,2::SMALLINT) AS monTotalAccountedCost,
			 coalesce(BT.bitEnabled,false) AS bitEnabled,
			 coalesce(BT.txtBizDocTemplate,'') AS txtBizDocTemplate,
			 coalesce(BT.txtCSS,'') AS txtCSS,
			 coalesce(BT.numOrientation,0) AS numOrientation,
			 coalesce(BT.bitKeepFooterBottom,false) AS bitKeepFooterBottom,
			 fn_GetContactName(Mst.numassignedto) AS AssigneeName,
			 coalesce((SELECT vcEmail FROM AdditionalContactsInformation WHERE numContactId = Mst.numassignedto),'') AS AssigneeEmail,
			 coalesce((SELECT numPhone FROM AdditionalContactsInformation WHERE numContactId = Mst.numassignedto),'') AS AssigneePhone,
			 fn_GetComapnyName(Mst.numDivisionId) AS OrganizationName,
			 fn_GetContactName(Mst.numContactId)  AS OrgContactName,

--CASE WHEN Mst.tintOppType = 1 THEN 
--dbo.getCompanyAddress((select ISNULL(DM.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC INNER JOIN DivisionMaster DM 
--on AC.numDivisionID = DM.numDivisionID where numContactID = Mst.numCreatedBy),1,Mst.numDomainId)
--               ELSE dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId)
--             END AS CompanyBillingAddress1,

CASE WHEN Mst.tintopptype = 1 THEN
         getCompanyAddress((select coalesce(AC.numDivisionId,0)  as numDivisionID from AdditionalContactsInformation AC
            INNER JOIN Domain D ON D.numDomainId = AC.numDomainID where AC.numContactId = D.numAdminID AND D.numDomainId = Mst.numDomainId),1::SMALLINT,Mst.numDomainId)
      ELSE getCompanyAddress(Mst.numDivisionId,1::SMALLINT,Mst.numDomainId)
      END AS CompanyBillingAddress,
CASE WHEN Mst.tintopptype = 1 THEN
         getCompanyAddress((select coalesce(AC.numDivisionId,0)  as numDivisionID from AdditionalContactsInformation AC
            INNER JOIN Domain D ON D.numDomainId = AC.numDomainID where AC.numContactId = D.numAdminID AND D.numDomainId = Mst.numDomainId),1::SMALLINT,Mst.numDomainId)
      ELSE getCompanyAddress(Mst.numDivisionId,2::SMALLINT,Mst.numDomainId)
      END AS CompanyShippingAddress,
--
--			 dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId) CompanyBillingAddress,
--			 dbo.getCompanyAddress(Mst.numDivisionId,2,Mst.numDomainId) CompanyShippingAddress,
			 case when ACI.numPhone <> '' then ACI.numPhone || case when ACI.numPhoneExtension <> '' then ' - ' || ACI.numPhoneExtension else '' end  else '' END AS OrgContactPhone,
			 DM.vcComPhone as OrganizationPhone,
             coalesce(ACI.vcEmail,'') AS OrgContactEmail,
 			 fn_GetContactName(Mst.numrecowner) AS OnlyOrderRecOwner,
 			 coalesce(opp.bitAuthoritativeBizDocs,0) AS bitAuthoritativeBizDocs,
			 coalesce(opp.tintDeferred,0) AS tintDeferred,coalesce(opp.monCreditAmount,0) AS monCreditAmount,
			 coalesce(opp.bitRentalBizDoc,false) as bitRentalBizDoc,coalesce(BT.numBizDocTempID,0) as numBizDocTempID,coalesce(BT.vcTemplateName,'') as vcTemplateName,
		     GetDealAmount(opp.numoppid,TIMEZONE('UTC',now()),opp.numOppBizDocsId) as monPAmount,
			 --(select top 1 SD.vcSignatureFile from SignatureDetail SD join OpportunityBizDocsDetails OBD on SD.numSignID=OBD.numSignID where SD.numDomainID = @numDomainIDTemp and OBD.numDomainID = @numDomainIDTemp and OBD.numSignID IS not null and OBD.numBizDocsId=opp.numOppBizDocsId order by OBD.numBizDocsPaymentDetId desc) as vcSignatureFile,
			 coalesce(CMP.txtComments,'') as vcOrganizationComments,
			 coalesce(opp.vcTrackingNo,'') AS  vcTrackingNo
			 ,'' AS  vcShippingMethod
			 ,opp.dtDeliveryDate,
			 CASE WHEN NULLIF(opp.vcRefOrderNo,'') IS NULL OR LENGTH(opp.vcRefOrderNo) = 0 THEN coalesce(Mst.vcOppRefOrderNo,'') ELSE
         coalesce(opp.vcRefOrderNo,'') END AS vcRefOrderNo,coalesce(Mst.numDiscountAcntType,0) AS numDiscountAcntType,
			 coalesce(opp.numSequenceId,'') AS numSequenceId,
			 CASE WHEN coalesce(opp.numBizDocId,0) = 296 THEN coalesce((SELECT SUBSTR((SELECT '$^$' || fn_GetListItemName(numBizDocStatus) || '#^#' || fn_GetContactName(numUserCntID) || '#^#' || FormatedDateTimeFromDate(dtCreatedDate+CAST(-v_ClientTimeZoneOffsetTemp || 'minute' as interval),v_numDomainIDTemp) || '#^#' || coalesce(DFCS.vcColorScheme,'NoForeColor')
               FROM OppFulfillmentBizDocsStatusHistory OFBS left join DycFieldColorScheme DFCS on OFBS.numBizDocStatus = CAST(DFCS.vcFieldValue AS NUMERIC) AND DFCS.numDomainID = v_numDomainIDTemp
               WHERE numOppId = opp.numoppid and numOppBizDocsId = opp.numOppBizDocsId),4,200000)),'') ELSE '' END AS vcBizDocsStatusList,coalesce(Mst.numCurrencyID,0) AS numCurrencyID,
			coalesce(opp.fltExchangeRateBizDoc,Mst.fltExchangeRate) AS fltExchangeRateBizDoc,
			coalesce(TBLEmployer.vcCompanyName,'') AS EmployerOrganizationName,coalesce(TBLEmployer.vcComPhone,'') as EmployerOrganizationPhone,coalesce(opp.bitAutoCreated,false) AS bitAutoCreated,
			coalesce(RecurrenceConfiguration.numRecConfigID,0) AS numRecConfigID,
			(CASE WHEN RecurrenceConfiguration.numRecConfigID IS NULL THEN false ELSE true END) AS bitRecur,
			(CASE WHEN RecurrenceTransaction.numRecTranID IS NULL THEN false ELSE true END) AS bitRecurred,
			coalesce(Mst.bitRecurred,false) AS bitOrderRecurred,
			FormatedDateFromDate(RecurrenceConfiguration.dtStartDate,v_numDomainIDTemp) AS dtStartDate,
			RecurrenceConfiguration.vcFrequency AS vcFrequency,
			coalesce(RecurrenceConfiguration.bitDisabled,false) AS bitDisabled,
			coalesce(opp.bitFulFilled,false) AS bitFulfilled,
			coalesce(opp.numDeferredBizDocID,0) AS numDeferredBizDocID,
			GetTotalQtyByUOM(opp.numOppBizDocsId) AS vcTotalQtybyUOM,
			coalesce(Mst.numShippingService,0) AS numShippingService,
			FormatedDateFromDate(Mst.dtReleaseDate,v_numDomainIDTemp) AS vcReleaseDate,
			FormatedDateFromDate(Mst.intPEstimatedCloseDate,v_numDomainIDTemp) AS vcRequiredDate,
			(CASE WHEN coalesce(Mst.numPartner,0) > 0 THEN coalesce((SELECT CONCAT(DivisionMaster.vcPartnerCode,'-',CompanyInfo.vcCompanyName) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId WHERE DivisionMaster.numDivisionID = Mst.numPartner),'') ELSE '' END) AS vcPartner,
			FormatedDateFromDate(Mst.dtReleaseDate,v_numDomainIDTemp) AS dtReleaseDate,
			(select  vcBizDocID from OpportunityBizDocs where numoppid = opp.numoppid and numBizDocId = 296 LIMIT 1) as vcFulFillment,
			(select  vcOppRefOrderNo from OpportunityMaster WHERE numOppId = opp.numoppid LIMIT 1) AS vcPOName,
			coalesce("vcCustomerPO#",'') AS "vcCustomerPO#",
			coalesce(Mst.txtComments,'') AS vcSOComments,
			coalesce((SELECT DMSA.vcAccountNumber FROm DivisionMasterShippingAccount DMSA WHERE DMSA.numDivisionID = DM.numDivisionID AND DMSA.numShipViaID = opp.numShipVia),'') AS vcShippersAccountNo,
			CAST(Mst.bintCreatedDate+CAST(-v_ClientTimeZoneOffsetTemp || 'minute' as interval) AS VARCHAR(20)) AS OrderCreatedDate,
			(CASE WHEN coalesce(opp.numSourceBizDocId,0) > 0 THEN coalesce((SELECT vcVendorInvoice FROM OpportunityBizDocs WHERE OpportunityBizDocs.numOppBizDocsId = opp.numSourceBizDocId),'') ELSE coalesce(vcVendorInvoice,'') END) AS vcVendorInvoice,
			(CASE
      WHEN coalesce(opp.numSourceBizDocId,0) > 0
      THEN coalesce((SELECT vcBizDocID FROM OpportunityBizDocs WHERE OpportunityBizDocs.numOppBizDocsId = opp.numSourceBizDocId),'')
      ELSE ''
      END) AS vcPackingSlip
			,coalesce(numARAccountID,0) AS numARAccountID
			,(CASE WHEN(SELECT COUNT(*) FROM OpportunityBizDocs OBInner WHERE OBInner.numoppid = v_numOppId AND OBInner.numBizDocId = 296 AND OBInner.numSourceBizDocId = opp.numOppBizDocsId AND opp.numBizDocId = 29397) > 0 THEN 1 ELSE 0 END) AS IsFulfillmentGeneratedOnPickList
			,coalesce((SELECT COUNT(*) FROM OpportunityBizDocs WHERE numoppid = v_numOppId AND coalesce(numDeferredBizDocID,0) = coalesce(opp.numOppBizDocsId,0)),0) AS intInvoiceForDiferredBizDoc
			,coalesce(CheckOrderInventoryStatus(Mst.numOppId,Mst.numDomainId,2::SMALLINT),'') AS vcInventoryStatus,
			CASE WHEN coalesce(DM.intdropship,0) = 1 THEN 'Not Available'
      WHEN coalesce(DM.intdropship,0) = 2 THEN 'Blank Available'
      WHEN coalesce(DM.intdropship,0) = 3 THEN 'Vendor Label'
      WHEN coalesce(DM.intdropship,0) = 4 THEN 'Private Label'
      ELSE '-'
      END AS vcDropShip
			,coalesce(D.numShippingServiceItemID,0) AS numShippingServiceItemID
			,coalesce(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID
			,CASE WHEN coalesce(D.bitInventoryInvoicing,false) = true AND coalesce(opp.numSourceBizDocId,0) = 0 THEN
         coalesce((SELECT  vcBizDocID FROM OpportunityBizDocs WHERE numSourceBizDocId = opp.numOppBizDocsId LIMIT 1),'NA') WHEN  coalesce(D.bitInventoryInvoicing,false) = true THEN
         coalesce((SELECT  vcBizDocID FROM OpportunityBizDocs WHERE numOppBizDocsId = opp.numSourceBizDocId LIMIT 1),'NA') ELSE '' END AS vcSourceBizDocID
      FROM   OpportunityBizDocs opp JOIN OpportunityMaster Mst ON Mst.numOppId = opp.numoppid
      JOIN Domain D ON D.numDomainId = Mst.numDomainId
      LEFT OUTER JOIN Currency C ON C.numCurrencyID = Mst.numCurrencyID
      LEFT JOIN DivisionMaster DM ON DM.numDivisionID = Mst.numDivisionId
      LEFT JOIN CompanyInfo CMP ON DM.numCompanyID = CMP.numCompanyId and CMP.numDomainID = v_numDomainIDTemp
      LEFT JOIN AdditionalContactsInformation ACI ON ACI.numContactId = Mst.numContactId
      LEFT JOIN BizDocTemplate BT ON BT.numDomainID = v_numDomainIDTemp AND BT.numBizDocTempID = opp.numBizDocTempID
      LEFT JOIN BillingTerms BTR ON BTR.numTermsID = coalesce(Mst.intBillingDays,0)
      JOIN DivisionMaster div1 ON Mst.numDivisionId = div1.numDivisionID
      JOIN CompanyInfo Com1 ON Com1.numCompanyId = div1.numCompanyID
      LEFT JOIN RecurrenceConfiguration ON opp.numOppBizDocsId = RecurrenceConfiguration.numOppBizDocID
      LEFT JOIN RecurrenceTransaction ON opp.numOppBizDocsId = RecurrenceTransaction.numRecurrOppBizDocID
      LEFT JOIN LATERAL(SELECT 
         Com1.vcCompanyName, div1.vcComPhone
         FROM   CompanyInfo Com1
         JOIN DivisionMaster div1
         ON Com1.numCompanyId = div1.numCompanyID
         JOIN Domain D1
         ON D1.numDivisionId = div1.numDivisionID
         JOIN AddressDetails AD1
         ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary = true
         WHERE  D1.numDomainId = v_numDomainIDTemp LIMIT 1) AS TBLEmployer on TRUE
      WHERE  opp.numOppBizDocsId = v_numOppBizDocsIdTemp
      AND Mst.numDomainId = v_numDomainIDTemp;
   end if;
   RETURN;
END; $$;


