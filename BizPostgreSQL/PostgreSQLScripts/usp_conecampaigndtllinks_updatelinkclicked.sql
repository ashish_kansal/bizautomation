-- Stored procedure definition script USP_ConECampaignDTLLinks_UpdateLinkClicked for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ConECampaignDTLLinks_UpdateLinkClicked(v_numLinkID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF(SELECT coalesce(bitClicked,false) FROM ConECampaignDTLLinks WHERE numLinkID = v_numLinkID) = false then
	
      UPDATE
      ConECampaignDTLLinks
      SET
      bitClicked = true
      WHERE
      numLinkID = v_numLinkID;
   end if;

   open SWV_RefCur for SELECT coalesce(vcOriginalLink,'') AS "vcOriginalLink" FROM ConECampaignDTLLinks WHERE numLinkID = v_numLinkID;
END; $$;












