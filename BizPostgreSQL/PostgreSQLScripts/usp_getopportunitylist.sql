-- Stored procedure definition script USP_GetOpportunityList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetOpportunityList(v_numUserCntID NUMERIC(9,0) DEFAULT 0,                                                    
 v_numDomainID NUMERIC(9,0) DEFAULT 0,                                                    
 v_tintUserRightType SMALLINT DEFAULT 0,                                                    
 v_tintSortOrder SMALLINT DEFAULT 4,                                                    
 v_dtLastDate TIMESTAMP DEFAULT NULL,                                                    
 v_SortChar CHAR(1) DEFAULT '0',                                                    
 v_FirstName VARCHAR(100) DEFAULT '',                                                    
 v_LastName VARCHAR(100) DEFAULT '',                                                    
 v_CustName VARCHAR(100) DEFAULT '' ,                                                    
 v_OppType SMALLINT DEFAULT NULL,                                                    
 v_CurrentPage INTEGER DEFAULT NULL,                                                    
 v_PageSize INTEGER DEFAULT NULL,                                                    
 INOUT v_TotRecs INTEGER  DEFAULT NULL,                                                    
 v_columnName VARCHAR(50) DEFAULT NULL,                                                    
 v_columnSortOrder VARCHAR(10) DEFAULT NULL,                                                  
 v_numDivisionID NUMERIC(9,0) DEFAULT 0,                      
 v_bitPartner BOOLEAN DEFAULT false ,        
 v_ClientTimeZoneOffset INTEGER DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql                                                    
                                                    
--Create a Temporary table to hold data                                                    
   AS $$
   DECLARE
   v_strSql  VARCHAR(8000);                          
   v_firstRec  INTEGER;                                              
   v_lastRec  INTEGER;
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   drop table IF EXISTS tt_TEMPTABLE CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLE 
   ( 
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numOppID NUMERIC(9,0)
   );                                                    
   v_strSql := '';                                                                     
   v_strSql := coalesce(v_strSql,'') || 'SELECT   Opp.numOppId                                     
  FROM OpportunityMaster Opp                                                     
  INNER JOIN AdditionalContactsInformation ADC                                                     
  ON Opp.numContactId = ADC.numContactId                                                     
  INNER JOIN DivisionMaster Div                                                     
  ON Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID                                                     
  INNER JOIN CompanyInfo C                                                     
  ON Div.numCompanyID = C.numCompanyId                                                          
  left join AdditionalContactsInformation ADC1 on ADC1.numContactId = Opp.numRecOwner ';                        
                     
                        
   if v_bitPartner = true then 
      v_strSql := coalesce(v_strSql,'') || ' left join OpportunityContact OppCont on OppCont.numOppId = Opp.numOppId';
   end if;                                                               
   v_strSql := coalesce(v_strSql,'') || ' WHERE Opp.tintOppStatus = 0                                                   
  and Div.numDomainID =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || '                                                    
  and Opp.tintOppType = ' || SUBSTR(CAST(v_OppType AS VARCHAR(1)),1,1) || '                                                    
  and ADC.vcFirstName  ilike ''' || coalesce(v_FirstName,'') || '%''                                                     
  and ADC.vcLastName ilike ''' || coalesce(v_LastName,'') || '%''                                                    
  and C.vcCompanyName ilike ''' || coalesce(v_CustName,'') || '%''';                                                    
   if v_numDivisionID <> 0 then 
      v_strSql := coalesce(v_strSql,'') || ' And Div.numDivisionID =' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(15)),1,15);
   end if;                                                   
   if v_SortChar <> '0' then 
      v_strSql := coalesce(v_strSql,'') || ' And Opp.vcPOppName ilike ''' || coalesce(v_SortChar,'') || '%''';
   end if;                                                     
   if v_tintUserRightType = 1 then 
      v_strSql := coalesce(v_strSql,'') || ' AND (Opp.numRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' or Opp.numAssignedTo= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15)  || ' or  Opp.numOppId in (select distinct(numOppId)             
from OpportunityStageDetails where numAssignTo =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')' || case when v_bitPartner = true then ' or ( OppCont.bitPartner=true and OppCont.numContactId=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')' else '' end || ')';
   ELSEIF v_tintUserRightType = 2
   then 
      v_strSql := coalesce(v_strSql,'') || ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' ) or div.numTerID=0 or             
Opp.numAssignedTo= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15)  || ' or  Opp.numOppId in (select distinct(numOppId)             
from OpportunityStageDetails where numAssignTo =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || '))';
   end if;                                                        
    --                                                
   if v_tintSortOrder = 1 then  
      v_strSql := coalesce(v_strSql,'') || ' AND (Opp.numRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' or Opp.numAssignedTo= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15)  || ' or  Opp.numOppId in (select distinct(numOppId)             
from OpportunityStageDetails where numAssignTo =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')' || case when v_bitPartner = true then ' or ( OppCont.bitPartner=true and OppCont.numContactId=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')' else '' end || ')';
   ELSEIF v_tintSortOrder = 2
   then  
      v_strSql := coalesce(v_strSql,'') || 'AND (Opp.numRecOwner in (select A.numContactID from AdditionalContactsInformation A                                                                             
where A.numManagerID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ') or Opp.numAssignedTo in (select A.numContactID from AdditionalContactsInformation A                                                                             
where A.numManagerID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15)  || ') or  Opp.numOppId in (select distinct(numOppId)             
from OpportunityStageDetails where numAssignTo in (select A.numContactID from AdditionalContactsInformation A                                                                             
where A.numManagerID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || '))' || case when v_bitPartner = true then ' or ( OppCont.bitPartner=true and OppCont.numContactId in (select A.numContactID from AdditionalContactsInformation A                                              
  
    
      
        
          
                               
where A.numManagerID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || '))' else '' end || ')';
   ELSEIF v_tintSortOrder = 4
   then  
      v_strSql := coalesce(v_strSql,'') || ' AND Opp.bintCreatedDate> ''' || CAST(v_dtLastDate+INTERVAL '-8 day' AS VARCHAR(20)) || '''';
   ELSEIF v_tintSortOrder = 6
   then  
      v_strSql := coalesce(v_strSql,'') || ' ORDER BY Opp.bintModifiedDate desc ';
   end if;                                                    
                                                    
   if v_tintSortOrder = 1 then  
      v_strSql := coalesce(v_strSql,'') || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   ELSEIF v_tintSortOrder = 2
   then  
      v_strSql := coalesce(v_strSql,'') || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   ELSEIF v_tintSortOrder = 3
   then  
      v_strSql := coalesce(v_strSql,'') || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   ELSEIF v_tintSortOrder = 4
   then  
      v_strSql := coalesce(v_strSql,'') || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   ELSEIF v_tintSortOrder = 5
   then  
      v_strSql := coalesce(v_strSql,'') || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   ELSEIF v_tintSortOrder = 6
   then  
      v_strSql := coalesce(v_strSql,'') || ' , ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   end if;                                                   
   if v_numDivisionID <> 0 then 
      v_strSql := coalesce(v_strSql,'') || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   end if;                                   
                     
   RAISE NOTICE '%',v_strSql;                                                  
   EXECUTE 'insert into tt_TEMPTABLE(numOppId)                                        
' || v_strSql;                    
                                                     
                                                    
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;             
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                                                    
                                                  
   select count(*) INTO v_TotRecs from tt_TEMPTABLE;                                         
                                        
   open SWV_RefCur for
   SELECT  Opp.numOppId,
  Opp.vcpOppName AS Name,
  GetOppStatus(Opp.numOppId) as STAGE,
  Opp.intPEstimatedCloseDate AS CloseDate,
  Opp.numrecowner,
  ADC.vcFirstName || ' ' || ADC.vcLastname AS Contact,
  C.vcCompanyName AS Company,
                C.numCompanyId,
  Div.tintCRMType,
  ADC.numContactId,
  Div.numDivisionID,
  Div.numTerID,
  Opp.monPAmount,
  ADC1.vcFirstName || ' ' || ADC1.vcLastname as UserName,
  fn_GetContactName(Opp.numassignedto) || '/ ' || fn_GetContactName(Opp.numAssignedBy) as AssignedToBy
   FROM OpportunityMaster Opp
   INNER JOIN AdditionalContactsInformation ADC
   ON Opp.numContactId = ADC.numContactId
   INNER JOIN DivisionMaster Div
   ON Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID
   INNER JOIN CompanyInfo C
   ON Div.numCompanyID = C.numCompanyId
   left join AdditionalContactsInformation ADC1 on ADC1.numContactId = Opp.numrecowner
   join tt_TEMPTABLE T on T.numOppID = Opp.numOppId
   WHERE ID > v_firstRec and ID < v_lastRec order by ID;                                        
                                        
                                                   
   RETURN;
END; $$;


