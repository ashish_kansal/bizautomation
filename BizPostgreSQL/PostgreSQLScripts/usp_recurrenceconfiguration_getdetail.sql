Create or replace FUNCTION USP_RecurrenceConfiguration_GetDetail(v_numRecConfigID NUMERIC(18,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numType  SMALLINT;
   v_numDomainID  NUMERIC(18,0);
BEGIN
   select   numType, numDomainID INTO v_numType,v_numDomainID FROM
   RecurrenceConfiguration WHERE
   numRecConfigID = v_numRecConfigID;

   open SWV_RefCur for
   SELECT
   numRecConfigID,
		numType,
		vcType,
		vcFrequency,
		FormatedDateFromDate(dtNextRecurrenceDate,v_numDomainID) AS dtNextRecurrenceDate,
		FormatedDateFromDate(dtStartDate,v_numDomainID) AS dtStartDate,
		FormatedDateFromDate(dtEndDate,v_numDomainID) AS dtEndDate,
		numTransaction,
		bitCompleted,
		bitDisabled
   FROM
   RecurrenceConfiguration
   WHERE
   numRecConfigID = v_numRecConfigID;
 
   If v_numType = 1 then --Sales Order
 
      open SWV_RefCur2 for
      SELECT
      OpportunityMaster.numOppId,
			OpportunityMaster.vcpOppName,
			FormatedDateFromDate(RecurrenceTransaction.dtCreatedDate,v_numDomainID) AS dtCreatedDate,
			(CASE WHEN RecurrenceTransaction.numErrorID IS NULL THEN false ELSE true END) AS bitErrorOccured,
			RecurrenceErrorLog.vcMessage
      FROM
      RecurrenceTransaction
      INNER JOIN
      RecurrenceConfiguration
      ON
      RecurrenceTransaction.numRecConfigID = RecurrenceConfiguration.numRecConfigID
      LEFT JOIN
      OpportunityMaster
      ON
      RecurrenceTransaction.numRecurrOppID = OpportunityMaster.numOppId
      LEFT JOIN
      RecurrenceErrorLog
      ON
      RecurrenceErrorLog.numRecConfigID = RecurrenceConfiguration.numRecConfigID AND
      RecurrenceErrorLog.numErrorID = RecurrenceTransaction.numErrorID
      WHERE
      RecurrenceTransaction.numRecConfigID = v_numRecConfigID;
   end if;
  
   IF v_numType = 2 then --Invoice
 
      open SWV_RefCur2 for
      SELECT
      OpportunityBizDocs.numoppid,
		OpportunityBizDocs.numOppBizDocsId,
		OpportunityBizDocs.vcBizDocID AS vcBizDocName,
		FormatedDateFromDate(RecurrenceTransaction.dtCreatedDate,v_numDomainID) AS dtCreatedDate,
		(CASE WHEN RecurrenceTransaction.numErrorID IS NULL THEN false ELSE true END) AS bitErrorOccured,
		RecurrenceErrorLog.vcMessage
      FROM
      RecurrenceTransaction
      INNER JOIN
      RecurrenceConfiguration
      ON
      RecurrenceTransaction.numRecConfigID = RecurrenceConfiguration.numRecConfigID
      LEFT JOIN
      OpportunityBizDocs
      ON
      RecurrenceTransaction.numRecurrOppBizDocID = OpportunityBizDocs.numOppBizDocsId
      LEFT JOIN
      RecurrenceErrorLog
      ON
      RecurrenceErrorLog.numRecConfigID = RecurrenceConfiguration.numRecConfigID AND
      RecurrenceErrorLog.numErrorID = RecurrenceTransaction.numErrorID
      WHERE
      RecurrenceConfiguration.numRecConfigID = v_numRecConfigID;
   end if;
   RETURN;
END; $$;  


