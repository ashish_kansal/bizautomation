CREATE OR REPLACE FUNCTION USP_GetApiOpportunities(v_numDomainID NUMERIC(9,0),
		  v_numWebApiId NUMERIC(9,0),  
		  v_Mode SMALLINT DEFAULT 0,	       
		  v_vcApiOrderID VARCHAR(50) DEFAULT NULL,
		  v_dtFromDate TIMESTAMP DEFAULT NULL,
		  v_dtToDate TIMESTAMP DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if(v_Mode = 0) then
      open SWV_RefCur for
      SELECT OM.numOppId,OM.bintCreatedDate,OM.vcMarketplaceOrderID,IAO.numOppId,IAO.bitIsActive ,OM.vcMarketplaceOrderReportId
      FROM OpportunityMaster OM left Join ImportApiOrder IAO ON OM.numOppId = IAO.numOppId
      WHERE OM.numDomainId = v_numDomainID
      AND OM.bintCreatedDate  BETWEEN v_dtFromDate AND v_dtToDate
      AND OM.tintSourceType = 3
      AND OM.tintSource = v_numWebApiId
      AND (IAO.bitIsActive IS NULL OR IAO.bitIsActive = false);
   end if;


   if(v_Mode = 1) then
      open SWV_RefCur for
      SELECT numOppId,bintCreatedDate,vcMarketplaceOrderID,vcMarketplaceOrderReportId FROM OpportunityMaster WHERE numDomainId = v_numDomainID
      AND vcMarketplaceOrderID = v_vcApiOrderID AND tintSourceType = 3 AND tintSource = v_numWebApiId;
   end if;
   RETURN;
END; $$;





