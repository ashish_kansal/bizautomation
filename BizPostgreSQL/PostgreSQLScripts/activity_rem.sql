CREATE OR REPLACE FUNCTION Activity_Rem(
	v_DataKey NUMERIC		-- activity primary key number to be deleted
)
RETURNS VOID LANGUAGE plpgsql
AS $$
	DECLARE
	v_RecurrenceKey  INTEGER;
	v_VarianceKey  UUID;
BEGIN
	SELECT 
		Activity.RecurrenceID
		,Activity.VarianceID 
	INTO 
		v_RecurrenceKey,v_VarianceKey 
	FROM 
		Activity 
	WHERE 
		Activity.ActivityID = v_DataKey;

	-- Delete the relationship between this Activity and it's Resource(s).
	--
	-- modified to delete the child activities record 
	IF (NOT v_VarianceKey IS NULL) then
		DELETE FROM ActivityResource WHERE ActivityResource.ActivityID in(select ActivityID  FROM Activity WHERE  Activity.VarianceID = v_VarianceKey);
	end if;

	IF (v_VarianceKey IS NULL) then
		DELETE FROM ActivityResource WHERE ActivityResource.ActivityID = v_DataKey;
	end if;

	--REMOVE ACTIVITYID FROM COMMUNICATION
	UPDATE Communication SET numActivityId = 0,bitOutlook = false WHERE numActivityId = v_DataKey;


	-- Delete this Activity row.
	--
	DELETE FROM Activity WHERE  Activity.ActivityID = v_DataKey;

	-- If this Activity had Variances, delete all of them (they will share
	-- the same VarianceID uniqueidentifier.)
	--
	IF (NOT v_VarianceKey IS NULL) then
		DELETE FROM Activity WHERE  Activity.VarianceID = v_VarianceKey;
	end if;

	-- If this Activity had a Recurrence, delete it.
	--
	IF ((-999) <> v_RecurrenceKey) then	
		DELETE FROM Recurrence WHERE  Recurrence.recurrenceid = v_RecurrenceKey;
	end if;

	

   -- COMMIT
RETURN;
END; $$;


