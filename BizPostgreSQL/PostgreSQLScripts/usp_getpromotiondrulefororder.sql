CREATE OR REPLACE FUNCTION USP_GetPromotiondRuleForOrder(v_numDomainID NUMERIC(18,0)
	,v_numSubTotal DOUBLE PRECISION DEFAULT 0
	,v_numDivisionID NUMERIC(18,0) DEFAULT NULL
	,v_numSiteID NUMERIC(18,0) DEFAULT NULL
	,v_numPromotionID NUMERIC(18,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numRelationship  NUMERIC(18,0) DEFAULT 0;
   v_numProfile  NUMERIC(18,0) DEFAULT 0;
   v_numProID  NUMERIC;
BEGIN
   IF coalesce(v_numDivisionID,0) > 0 then
	
      select   coalesce(numCompanyType,0), coalesce(vcProfile,0) INTO v_numRelationship,v_numProfile FROM
      DivisionMaster
      INNER JOIN
      CompanyInfo
      ON
      DivisionMaster.numCompanyID = CompanyInfo.numCompanyId WHERE
      numDivisionID = v_numDivisionID;
   ELSEIF coalesce(v_numSiteID,0) > 0
   then
	
      select   coalesce(numRelationshipId,0), coalesce(numProfileId,0) INTO v_numRelationship,v_numProfile FROM
      eCommerceDTL WHERE
      numSiteId = v_numSiteID;
   end if;



   SELECT 
   PO.numProId INTO v_numProID FROM
   PromotionOffer PO
   INNER JOIN
   PromotionOfferOrganizations PORG
   ON
   PO.numProId = PORG.numProId WHERE
   PO.numDomainId = v_numDomainID
   AND coalesce(PO.IsOrderBasedPromotion,false) = true
   AND (PO.numProId = v_numPromotionID OR coalesce(bitRequireCouponCode,false) = false)
   AND coalesce(bitEnabled,false) = true
   AND numRelationship = v_numRelationship
   AND coalesce(bitUseForCouponManagement,false) = false
   AND numProfile = v_numProfile
   AND  1 =(CASE
   WHEN coalesce(PO.bitNeverExpires,false) = true
   THEN 1
   WHEN coalesce(PO.bitNeverExpires,false) = false
   THEN
      CASE WHEN (PO.dtValidFrom <= TIMEZONE('UTC',now()) AND PO.dtValidTo >= TIMEZONE('UTC',now()))
      THEN 1
      ELSE 0 END
   ELSE 0 END)   ORDER BY(CASE WHEN PO.numProId = coalesce(v_numPromotionID,0) THEN 1 ELSE 0 END) DESC  LIMIT 1;


   open SWV_RefCur for
   SELECT * FROM PromotionOffer WHERE numProId = v_numProID; 
	 	
   open SWV_RefCur2 for
   SELECT ROW_NUMBER() OVER(ORDER BY numProOfferOrderID ASC) AS RowNum, numOrderAmount,fltDiscountValue FROM PromotionOfferOrder WHERE numPromotionID = v_numProID;
   RETURN;
END; $$; 


