-- Stored procedure definition script USP_CheckOrderedAndInvoicedOrBilledQty for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_CheckOrderedAndInvoicedOrBilledQty(v_numOppID NUMERIC(18,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintOppType  SMALLINT;
   v_tintOppStatus  SMALLINT;
BEGIN
   DROP TABLE IF EXISTS tt_TMEP CASCADE;
   CREATE TEMPORARY TABLE tt_TMEP
   (
      ID INTEGER,
      vcDescription VARCHAR(100),
      bitTrue BOOLEAN
   );
   INSERT INTO
   tt_TMEP
	 VALUES(1,CAST('Fulfillment bizdoc is added but not yet fulfilled' AS VARCHAR(100)),CAST(0 AS BOOLEAN)),(2,CAST('Invoice/Bill and Ordered Qty is not same' AS VARCHAR(100)),CAST(0 AS BOOLEAN)),(3,CAST('Ordered & Fulfilled Qty is not same' AS VARCHAR(100)),CAST(0 AS BOOLEAN)),(4,CAST('Invoice is not generated against diferred income bizDocs.' AS VARCHAR(100)),CAST(0 AS BOOLEAN)),
   (5,CAST('Qty is not available to add in deferred bizdoc.' AS VARCHAR(100)),CAST(0 AS BOOLEAN)),(6,CAST('Ordered & Purchase Fulfilled Qty is not same' AS VARCHAR(100)),CAST(0 AS BOOLEAN));

	
   select   tintopptype, tintoppstatus INTO v_tintOppType,v_tintOppStatus FROM OpportunityMaster WHERe numOppId = v_numOppID;


	-- CHECK IF FULFILLMENT BIZDOC IS ADDED TO SALES ORDER BUT IT IS NOT FULFILLED FROM SALES FULFILLMENT SCREEN
   IF v_tintOppType = 1 AND v_tintOppStatus = 1 then  --SALES ORDER
	
      IF(SELECT COUNT(*) FROM OpportunityBizDocs WHERE numoppid = v_numOppID AND numBizDocId = 296 AND coalesce(bitFulFilled,false) = false) > 0 then
		
         UPDATE tt_TMEP SET bitTrue = true WHERE ID = 1;
      end if;

		-- CHECK IF FULFILLMENT BIZDOC QTY OF ITEMS IS NOT SAME AS ORDERED QTY
      IF(SELECT
      COUNT(*)
      FROM(SELECT
         OI.numoppitemtCode,
					coalesce(OI.numUnitHour,0) AS OrderedQty,
					coalesce(TempFulFilled.FulFilledQty,0) AS FulFilledQty
         FROM
         OpportunityItems OI
         INNER JOIN
         Item I
         ON
         OI.numItemCode = I.numItemCode
         LEFT JOIN LATERAL(SELECT
            SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
            FROM
            OpportunityBizDocs
            INNER JOIN
            OpportunityBizDocItems
            ON
            OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
            WHERE
            OpportunityBizDocs.numoppid = v_numOppID
            AND coalesce(OpportunityBizDocs.numBizDocId,0) = 296
            AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode) AS TempFulFilled on TRUE
         WHERE
         OI.numOppId = v_numOppID
         AND coalesce(OI.numUnitHour,0) > 0
         AND UPPER(I.charItemType) = 'P'
         AND coalesce(OI.bitDropShip,false) = false) X
      WHERE
      X.OrderedQty <> X.FulFilledQty) > 0 then
		
         UPDATE tt_TMEP SET bitTrue = true WHERE ID = 3;
      end if;

		-- CHECK WHETHER INVOICES ARE GENERATED AGAINST DIFERRED INCOME BIZDOC
      IF(SELECT
      COUNT(*)
      FROM(SELECT
         OBDI.numOppItemID,
					(coalesce(SUM(OBDI.numUnitHour),0) -coalesce(SUM(TEMPInvoiceAgainstDeferred.numUnitHour),0)) AS intQtyRemaining
         FROM
         OpportunityBizDocs OBD
         INNER JOIN
         OpportunityBizDocItems OBDI
         ON
         OBD.numOppBizDocsId = OBDI.numOppBizDocID
         CROSS JOIN LATERAL(SELECT
            SUM(OpportunityBizDocItems.numUnitHour) AS numUnitHour
            FROM
            OpportunityBizDocs
            INNER JOIN
            OpportunityBizDocItems
            ON
            OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
            WHERE
            numoppid = v_numOppID
            AND coalesce(numDeferredBizDocID,0) > 0
            AND OpportunityBizDocItems.numOppItemID = OBDI.numOppItemID) AS TEMPInvoiceAgainstDeferred
         WHERE
         numoppid = v_numOppID
         AND numBizDocId = 304
         GROUP BY
         OBDI.numOppItemID) AS TEMP
      WHERE
      intQtyRemaining > 0) > 0 then
		
         UPDATE tt_TMEP SET bitTrue = true WHERE ID = 4;
      end if;

		-- CHECK WHETHER ITEM QTY LEFT TO ADD TO DIFERRED INCOME BIZDOC
      IF(SELECT
      COUNT(*)
      FROM(SELECT
         OI.numoppitemtCode,
					coalesce(OI.numUnitHour,0) AS OrderedQty,
					coalesce(TempQtyLeftForDifferedIncome.intQty,0) AS intQty
         FROM
         OpportunityItems OI
         INNER JOIN
         Item I
         ON
         OI.numItemCode = I.numItemCode
         LEFT JOIN LATERAL(SELECT
            SUM(OpportunityBizDocItems.numUnitHour) AS intQty
            FROM
            OpportunityBizDocs
            INNER JOIN
            OpportunityBizDocItems
            ON
            OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
            WHERE
            OpportunityBizDocs.numoppid = v_numOppID
            AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
            AND (coalesce(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1 OR OpportunityBizDocs.numBizDocId = 304)) AS TempQtyLeftForDifferedIncome on TRUE
         WHERE
         OI.numOppId = v_numOppID) X
      WHERE
      X.OrderedQty <> X.intQty) = 0 then
		
         UPDATE tt_TMEP SET bitTrue = true WHERE ID = 5;
      end if;
   ELSEIF v_tintOppType = 2 AND v_tintOppStatus = 1
   then  --SALES ORDER
	
      IF(SELECT
      COUNT(*)
      FROM(SELECT
         OI.numoppitemtCode,
					coalesce(OI.numUnitHour,0) AS OrderedQty,
					coalesce(OI.numUnitHourReceived,0) AS FulFilledQty
         FROM
         OpportunityItems OI
         WHERE
         OI.numOppId = v_numOppID
         AND coalesce(OI.numUnitHour,0) > 0
         AND coalesce(OI.bitDropShip,false) = false) X
      WHERE
      X.OrderedQty <> X.FulFilledQty) > 0 then
		
         UPDATE tt_TMEP SET bitTrue = true WHERE ID = 6;
      end if;
   end if;

	-- IF INVOICED/BILLED QTY OF ITEMS IS NOT SAME AS ORDERED QTY THEN ORDER CAN NOT BE SHIPPED/RECEIVED
   IF(SELECT
   COUNT(*)
   FROM(SELECT
      OI.numoppitemtCode,
				coalesce(OI.numUnitHour,0) AS OrderedQty,
				coalesce(TempInvoice.InvoicedQty,0) AS InvoicedQty
      FROM
      OpportunityItems OI
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      LEFT JOIN LATERAL(SELECT
         SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
         FROM
         OpportunityBizDocs
         INNER JOIN
         OpportunityBizDocItems
         ON
         OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
         WHERE
         OpportunityBizDocs.numoppid = v_numOppID
         AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
         AND coalesce(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1) AS TempInvoice on TRUE
      WHERE
      OI.numOppId = v_numOppID
      AND coalesce(OI.numUnitHour,0) > 0) X
   WHERE
   X.OrderedQty > X.InvoicedQty) > 0 then
	
      UPDATE tt_TMEP SET bitTrue = true WHERE ID = 2;
   end if;

   open SWV_RefCur for
   SELECT * FROM tt_TMEP;

	--GET FULFILLMENT BIZDOCS WHICH ARE NOT FULFILLED YET
   open SWV_RefCur2 for
   SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE OpportunityBizDocs.numoppid = v_numOppID AND coalesce(OpportunityBizDocs.numBizDocId,0) = 296 AND coalesce(OpportunityBizDocs.bitFulFilled,false) = false;
   RETURN;
END; $$;


/****** Object:  StoredProcedure [dbo].[USP_DeleteCartItem]    Script Date: 11/08/2011 17:28:13 ******/



