-- Stored procedure definition script usp_CheckCompany for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_CheckCompany(
--   	
v_numListId NUMERIC DEFAULT 0,
	v_numDomainId NUMERIC DEFAULT 0,
	v_numCompId NUMERIC DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numcompanytype  NUMERIC;
BEGIN
   IF v_numCompId = 0 then
		
      open SWV_RefCur for
      SELECT COUNT(*) FROM CompanyInfo WHERE numCompanyType = v_numListId AND numDomainID = v_numDomainId;
   ELSE
      select   numCompanyType INTO v_numcompanytype FROM CompanyInfo WHERE numCompanyId = v_numCompId;
      IF v_numcompanytype = 93 then
				
         open SWV_RefCur for
         SELECT -1;
      ELSE
         open SWV_RefCur for
         SELECT 0;
      end if;
   end if;
   RETURN;
END; $$;


