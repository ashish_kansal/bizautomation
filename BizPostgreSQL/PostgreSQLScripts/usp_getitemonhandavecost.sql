-- Stored procedure definition script Usp_GetItemOnHandAveCost for PostgreSQL
CREATE OR REPLACE FUNCTION Usp_GetItemOnHandAveCost(v_numDomainID	NUMERIC(18,0), v_strItemIDs		TEXT,
	INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF coalesce(v_strItemIDs,'') <> '' then
	
      DROP TABLE IF EXISTS tt_TEMP CASCADE;
      CREATE TEMPORARY TABLE tt_TEMP
      (
         ID INTEGER PRIMARY KEY
      );
      INSERT INTO tt_TEMP  SELECT DISTINCT ID FROM SplitIDs(v_strItemIDs,',');
		
open SWV_RefCur for SELECT cast(numItemCode as VARCHAR(255)), cast(numWareHouseItemID as VARCHAR(255)) ,(CASE WHEN coalesce(I.bitVirtualInventory,false) = true THEN 0 ELSE monAverageCost END) ,cast(numOnHand as VARCHAR(255))
      FROM Item I
      JOIN WareHouseItems WI ON I.numItemCode = WI.numItemID AND I.numDomainID = WI.numDomainID
      JOIN tt_TEMP SP ON SP.ID = I.numItemCode
      WHERE I.numDomainID = v_numDomainID
      ORDER BY numItemCode;
  
   end if;
END; $$;













