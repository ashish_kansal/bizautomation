DROP FUNCTION IF EXISTS USP_GetMonthlySummary;

CREATE OR REPLACE FUNCTION USP_GetMonthlySummary(v_numDomainId   INTEGER,
               v_dtFromDate    TIMESTAMP,
               v_dtToDate      TIMESTAMP,
               v_vcChartAcntId VARCHAR(500),
               v_ClientTimeZoneOffset INTEGER, --Added by Chintan to enable calculation of date according to client machine,
			   v_numAccountClass NUMERIC(18,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
  
	--SET @dtFromDate = DateAdd(minute, @ClientTimeZoneOffset, @dtFromDate);
	--SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate);

   AS $$
   DECLARE
   v_mnOpeningBalance  DECIMAL(20,5);
   v_numMaxFinYearID  NUMERIC;
   v_Month  INTEGER;
   v_year  INTEGER;
   SWV_RowCount INTEGER;
BEGIN
   RAISE NOTICE '%',v_dtFromDate; 
   RAISE NOTICE '%',v_dtToDate;

   select   coalesce(mnOpeningBalance,0) INTO v_mnOpeningBalance FROM fn_GetOpeningBalance(v_vcChartAcntId,v_numDomainId,v_dtFromDate,v_ClientTimeZoneOffset);
	

		/*RollUp of Sub Accounts */
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP ON COMMIT DROP AS
      SELECT  COA2.numAccountId  /*,COA1.vcAccountCode ,COA2.vcAccountCode*/
      FROM    Chart_Of_Accounts COA1
      INNER JOIN Chart_Of_Accounts COA2 ON COA1.numDomainId = COA2.numDomainId
      WHERE   COA1.numAccountId IN(SELECT  cast(ID AS NUMERIC(9,0)) FROM SplitIDs(v_vcChartAcntId,','))
      AND COA1.numDomainId = v_numDomainId
      AND (COA2.vcAccountCode ilike COA1.vcAccountCode OR (COA2.vcAccountCode ilike COA1.vcAccountCode || '%' AND COA2.numParentAccId > 0));
				
   v_vcChartAcntId := coalesce((SELECT string_agg(numAccountID::VARCHAR,',') FROM tt_TEMP),''); 
  
  
   DROP TABLE IF EXISTS tt_TEMP1 CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP1 ON COMMIT DROP AS
      SELECT   numAccountId,
             COAvcAccountCode,
             vcAccountName,
             numAccountTypeID,
             vcAccountType,
             TO_CHAR(datEntry_Date, 'FMMon') || ' ' ||  CAST(EXTRACT(YEAR FROM datEntry_Date) AS VARCHAR(4)) AS MONTH,
             EXTRACT(MONTH FROM datEntry_Date) AS MonthNumber,
             EXTRACT(YEAR FROM datEntry_Date) AS YEAR,
             SUM(coalesce(Debit,0)) AS Debit,
             SUM(coalesce(Credit,0)) AS Credit,
             coalesce(v_mnOpeningBalance,0) AS Opening,
             0.00 AS Closing
             
      FROM     VIEW_JOURNAL
      WHERE    datEntry_Date BETWEEN v_dtFromDate AND v_dtToDate
      AND numDomainID = v_numDomainId
      AND numAccountId IN(SELECT * FROM   SplitIDs(v_vcChartAcntId,','))
      AND (numAccountClass = v_numAccountClass OR coalesce(v_numAccountClass,0) = 0)
      GROUP BY numAccountId,COAvcAccountCode,vcAccountName,numAccountTypeID,vcAccountType,
      EXTRACT(MONTH FROM datEntry_Date),EXTRACT(YEAR FROM datEntry_Date),TO_CHAR(datEntry_Date, 'FMMon') || ' ' ||  CAST(EXTRACT(YEAR FROM datEntry_Date) AS VARCHAR(4))
      ORDER BY EXTRACT(YEAR FROM datEntry_Date),EXTRACT(MONTH FROM datEntry_Date);
    
    
--    SELECT DATEPART(MONTH,dtPeriodFrom) FY_Opening_Month,DATEPART(year,dtPeriodFrom) FY_Opening_year, * FROM dbo.FinancialYear WHERE numDomainId=@numDomainId
--Following logic applieds to only income, expense and cogs account
   IF EXISTS(SELECT * FROM tt_TEMP1 where SUBSTR(COAvcAccountCode,0,5) IN('0103','0104','0106')) then
      alter table tt_TEMP1 ALTER COLUMN MONTH TYPE VARCHAR(50);
      ALTER TABLE tt_TEMP1 add IsFinancialMonth BOOLEAN;
      UPDATE tt_TEMP1 SET IsFinancialMonth = false;
      select   MAX(numFinYearId) INTO v_numMaxFinYearID FROM FinancialYear WHERE numDomainId = v_numDomainId; 
--    PRINT @numMaxFinYearID
      WHILE v_numMaxFinYearID > 0 LOOP
         select   EXTRACT(MONTH FROM dtPeriodFrom), EXTRACT(YEAR FROM dtPeriodFrom) INTO v_Month,v_year FROM FinancialYear WHERE numDomainId = v_numDomainId AND numFinYearId = v_numMaxFinYearID;
    
--    PRINT @Month
--    PRINT @year
	/*Rule:make first month of each FY zero as opening balance and show report respectively */
         UPDATE tt_TEMP1  SET Opening = -0.0001,MONTH = MONTH || ' FY Start',IsFinancialMonth = true
         WHERE MonthNumber = v_Month AND YEAR = v_year;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         IF SWV_RowCount = 0 then -- when No data avail for First month of FY
			
            select   MonthNumber INTO v_Month FROM tt_TEMP1 WHERE MonthNumber > v_Month AND YEAR = v_year    LIMIT 1;
            UPDATE tt_TEMP1  SET Opening = -0.0001,MONTH = MONTH || ' FY Start',IsFinancialMonth = true
            WHERE MonthNumber = v_Month AND YEAR = v_year;
         end if;
         select   MAX(numFinYearId) INTO v_numMaxFinYearID FROM FinancialYear WHERE numDomainId = v_numDomainId AND numFinYearId < v_numMaxFinYearID;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         IF SWV_RowCount = 0 then
            v_numMaxFinYearID := 0;
         end if;
      END LOOP;
   end if;
   

    
   open SWV_RefCur for SELECT * FROM tt_TEMP1 ORDER BY YEAR,MonthNumber;
    
   DROP TABLE IF EXISTS tt_TEMP CASCADE;

   RETURN;
END; $$;
  
--  exec USP_GetMonthlySummary @numDomainId=89,@dtFromDate='2011-12-01 00:00:00:000',@dtToDate='2013-03-01 23:59:59:000',@vcChartAcntId='1444',@ClientTimeZoneOffset=-330













