-- FUNCTION: public.usp_getalertconfig(numeric, numeric)

-- DROP FUNCTION public.usp_getalertconfig(numeric, numeric);

CREATE OR REPLACE FUNCTION public.usp_getalertconfig(
	v_numdomainid numeric,
	v_numcontactId numeric,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
-- This function was converted on Sat Apr 10 14:57:57 2021 using Ispirer SQLWays 9.0 Build 6532 64bit Licensed to BizAutomation - Sandeep Patel - India - Ispirer MnMTK 2020 MSSQLServer to PostgreSQL Database Migration Professional Project License (50 GB, 750 Tables, 426500 LOC, 3 Extension Requests, 1 Month, 20210508).
    open SWV_RefCur for  SELECT * FROM AlertConfig WHERE numDomainID = v_numDomainId AND numContactId = v_numContactId;
 RETURN;
END;
$BODY$;

ALTER FUNCTION public.usp_getalertconfig(numeric, numeric)
    OWNER TO postgres;
