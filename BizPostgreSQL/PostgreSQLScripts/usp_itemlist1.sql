DROP FUNCTION IF EXISTS USP_ItemList1;

CREATE OR REPLACE FUNCTION USP_ItemList1(v_ItemClassification NUMERIC(9,0) DEFAULT 0,
    v_IsKit BOOLEAN DEFAULT NULL,
    v_SortChar CHAR(1) DEFAULT '0',
    v_CurrentPage INTEGER DEFAULT NULL,
    v_PageSize INTEGER DEFAULT NULL,
    INOUT v_TotRecs INTEGER  DEFAULT NULL, 
    v_columnName VARCHAR(50) DEFAULT NULL,
    v_columnSortOrder VARCHAR(10) DEFAULT NULL,
    v_numDomainID NUMERIC(18,0) DEFAULT 0,
    v_ItemType CHAR(1) DEFAULT NULL,
    v_bitSerialized BOOLEAN DEFAULT NULL,
    v_numItemGroup NUMERIC(9,0) DEFAULT NULL,
    v_bitAssembly BOOLEAN DEFAULT NULL,
    v_numUserCntID NUMERIC(18,0) DEFAULT NULL,
    v_Where	TEXT DEFAULT NULL,
    v_IsArchive BOOLEAN DEFAULT false,
    v_byteMode SMALLINT DEFAULT NULL,
	v_bitAsset BOOLEAN DEFAULT false,
	v_bitRental BOOLEAN DEFAULT false,
	v_bitContainer BOOLEAN DEFAULT false,
	v_SearchText VARCHAR(300) DEFAULT NULL,
	v_vcRegularSearchCriteria TEXT DEFAULT '',
	v_vcCustomSearchCriteria TEXT DEFAULT '',
	v_tintDashboardReminderType SMALLINT DEFAULT 0,
	v_bitMultiSelect Boolean DEFAULT False,
	v_numFilteredWarehouseID NUMERIC(18,0) DEFAULT 0,
	v_vcAsile VARCHAR(500) DEFAULT '',
	v_vcRack VARCHAR(500) DEFAULT '',
	v_vcShelf VARCHAR(500) DEFAULT '',
	v_vcBin VARCHAR(500) DEFAULT '',
	v_bitApplyFilter BOOLEAN DEFAULT false,
	v_tintUserRightType SMALLINT DEFAULT NULL, 
	INOUT SWV_RefCur refcursor DEFAULT NULL,
    INOUT SWV_RefCur2 refcursor DEFAULT NULL
)
LANGUAGE plpgsql
AS $$

DECLARE
   v_numWarehouseID  NUMERIC(18,0);
   v_Nocolumns  SMALLINT DEFAULT 0;
   v_IsMasterConfAvailable  BOOLEAN DEFAULT 0;
   v_numUserGroup  NUMERIC(18,0);
   v_strColumns  TEXT;
   v_tintOrder  SMALLINT;                                                  
   v_vcFieldName  VARCHAR(50);                                                  
   v_vcListItemType  VARCHAR(3);                                             
   v_vcListItemType1  VARCHAR(1);                                                 
   v_vcAssociatedControlType  VARCHAR(10);                                                  
   v_numListID  NUMERIC(9,0);                                                  
   v_vcDbColumnName  VARCHAR(50);                      
   v_WhereCondition  TEXT;                       
   v_vcLookBackTableName  VARCHAR(2000);                
   v_bitCustom  BOOLEAN;                  
   v_numFieldId  NUMERIC;  
   v_bitAllowSorting BOOLEAN;   
   v_bitAllowEdit BOOLEAN;                   
   v_vcColumnName  VARCHAR(200);
   v_Grp_ID  SMALLINT;
   v_ListRelID  NUMERIC(9,0); 
   v_SearchQuery  TEXT DEFAULT '';

   v_j  INTEGER DEFAULT 0;
  
   v_strSQL  TEXT;
   v_column  VARCHAR(50);              
   v_strWhere  VARCHAR(8000);
   v_firstRec  INTEGER;                                        
   v_lastRec  INTEGER;                                        
   v_strFinal  TEXT DEFAULT '';

   v_CustomFieldType  VARCHAR(10);
   v_fldId  VARCHAR(10);
   v_Prefix  VARCHAR(5);
   SWV_RowCount INTEGER;
BEGIN
   select   numWareHouseID INTO v_numWarehouseID FROM Warehouses WHERE numDomainID = COALESCE(v_numDomainID,0)    LIMIT 1;

   v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'I.numItemCode ilike','I.numItemCode::VARCHAR ilike');
   v_vcCustomSearchCriteria := REPLACE(v_vcCustomSearchCriteria,'ctl00_ctl00_MainContent_GridPlaceHolder_gvSearch_ctl00_ctl02_ctl00_','');

   DROP TABLE IF EXISTS tt_TEMPFORM CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPFORM
   (
      tintOrder SMALLINT,
      vcDbColumnName VARCHAR(50),
      vcOrigDbColumnName VARCHAR(50),
      vcFieldName VARCHAR(50),
      vcAssociatedControlType VARCHAR(50),
      vcListItemType VARCHAR(3),
      numListID NUMERIC(9,0),
      vcLookBackTableName VARCHAR(50),
      bitCustomField BOOLEAN,
      numFieldId NUMERIC,
      bitAllowSorting BOOLEAN,
      bitAllowEdit BOOLEAN,
      bitIsRequired BOOLEAN,
      bitIsEmail BOOLEAN,
      bitIsAlphaNumeric BOOLEAN,
      bitIsNumeric BOOLEAN,
      bitIsLengthValidation BOOLEAN,
      intMaxLength INTEGER,
      intMinLength INTEGER,
      bitFieldMessage BOOLEAN,
      vcFieldMessage VARCHAR(500),
      ListRelID NUMERIC(9,0),
      intColumnWidth INTEGER,
      bitAllowFiltering BOOLEAN,
      vcFieldDataType CHAR(1),
      Grp_Id SMALLINT
   );

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
   select   coalesce(SUM(TotalRow),0) INTO v_Nocolumns FROM(SELECT
      COUNT(*) AS TotalRow
      FROM
      View_DynamicColumns
      WHERE
      numFormId = 21
      AND numUserCntID = COALESCE(v_numUserCntID,0)
      AND numDomainID = COALESCE(v_numDomainID,0)
      AND tintPageType = 1
      AND numRelCntType = 0
      UNION
      SELECT
      COUNT(*) AS TotalRow
      FROM
      View_DynamicCustomColumns
      WHERE
      numFormId = 21
      AND numUserCntID = COALESCE(v_numUserCntID,0)
      AND numDomainID = COALESCE(v_numDomainID,0)
      AND tintPageType = 1
      AND numRelCntType = 0) TotalRows;

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
   IF v_Nocolumns = 0 then
	
      select   numGroupID INTO v_numUserGroup FROM UserMaster WHERE numUserDetailId = v_numUserCntID;
      IF(SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = COALESCE(v_numDomainID,0) AND numFormID = 21 AND numRelCntType = 0 AND bitGridConfiguration = true AND numGroupID = v_numUserGroup) > 0 then
		
         v_IsMasterConfAvailable := true;
      end if;
   end if;

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
   IF v_IsMasterConfAvailable = true then
	
      INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
      SELECT
      21,numFieldId,intColumnNum,intRowNum,v_numDomainID,v_numUserCntID,0,1,false,intColumnWidth
      FROM
      View_DynamicColumnsMasterConfig
      WHERE
      View_DynamicColumnsMasterConfig.numFormId = 21 AND
      View_DynamicColumnsMasterConfig.numDomainID = COALESCE(v_numDomainID,0) AND
      View_DynamicColumnsMasterConfig.numAuthGroupID = COALESCE(v_numUserGroup,0) AND
      View_DynamicColumnsMasterConfig.numRelCntType = 0 AND
      View_DynamicColumnsMasterConfig.bitGridConfiguration = true AND
      coalesce(View_DynamicColumnsMasterConfig.bitSettingField,false) = true AND
      coalesce(View_DynamicColumnsMasterConfig.bitCustom,false) = false
      UNION
      SELECT
      21,numFieldId,intColumnNum,intRowNum,v_numDomainID,v_numUserCntID,0,1,1,intColumnWidth
      FROM
      View_DynamicCustomColumnsMasterConfig
      WHERE
      View_DynamicCustomColumnsMasterConfig.numFormId = 21 AND
      View_DynamicCustomColumnsMasterConfig.numDomainID = COALESCE(v_numDomainID,0) AND
      View_DynamicCustomColumnsMasterConfig.numAuthGroupID = COALESCE(v_numUserGroup,0) AND
      View_DynamicCustomColumnsMasterConfig.numRelCntType = 0 AND
      View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = true AND
      coalesce(View_DynamicCustomColumnsMasterConfig.bitCustom,false) = true;
      INSERT INTO
      tt_TEMPFORM
      SELECT(intRowNum+1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	coalesce(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage AS vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType,CAST(0 AS SMALLINT)
      FROM
      View_DynamicColumnsMasterConfig
      WHERE
      View_DynamicColumnsMasterConfig.numFormId = 21 AND
      View_DynamicColumnsMasterConfig.numDomainID = COALESCE(v_numDomainID,0) AND
      View_DynamicColumnsMasterConfig.numAuthGroupID = COALESCE(v_numUserGroup,0) AND
      View_DynamicColumnsMasterConfig.numRelCntType = 0 AND
      View_DynamicColumnsMasterConfig.bitGridConfiguration = true AND
      coalesce(View_DynamicColumnsMasterConfig.bitSettingField,false) = true AND
      coalesce(View_DynamicColumnsMasterConfig.bitCustom,false) = false
      UNION
      SELECT
      tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,true,'',Grp_Id
      FROM
      View_DynamicCustomColumnsMasterConfig
      WHERE
      View_DynamicCustomColumnsMasterConfig.numFormId = 21 AND
      View_DynamicCustomColumnsMasterConfig.numDomainID = COALESCE(v_numDomainID,0) AND
      View_DynamicCustomColumnsMasterConfig.numAuthGroupID = COALESCE(v_numUserGroup,0) AND
      View_DynamicCustomColumnsMasterConfig.numRelCntType = 0 AND
      View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = true AND
      coalesce(View_DynamicCustomColumnsMasterConfig.bitCustom,false) = true
      ORDER BY
      tintOrder ASC;
   ELSE
      IF v_Nocolumns = 0 then
		
         INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
         SELECT
         21,numFieldId,0,Row_number() over(order by tintRow desc),v_numDomainID,v_numUserCntID,0,1,false,intColumnWidth
         FROM
         View_DynamicDefaultColumns
         WHERE
         numFormId = 21
         AND bitDefault = true
         AND coalesce(bitSettingField,false) = true
         AND numDomainID = COALESCE(v_numDomainID,0)
         ORDER BY
         tintOrder asc;
      end if;
      INSERT INTO
      tt_TEMPFORM
      SELECT
      tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,coalesce(vcCultureFieldName,vcFieldName),
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage AS vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType,CAST(0 AS SMALLINT)
      FROM
      View_DynamicColumns
      WHERE
      numFormId = 21
      AND numUserCntID = v_numUserCntID
      AND numDomainID = COALESCE(v_numDomainID,0)
      AND tintPageType = 1
      AND coalesce(bitSettingField,false) = true
      AND coalesce(bitCustom,false) = false
      AND numRelCntType = 0
      UNION
      SELECT
      tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,true,'',Grp_Id
      FROM
      View_DynamicCustomColumns
      WHERE
      numFormId = 21
      AND numUserCntID = v_numUserCntID
      AND numDomainID = COALESCE(v_numDomainID,0)
      AND tintPageType = 1
      AND coalesce(bitCustom,false) = true
      AND numRelCntType = 0
      ORDER BY
      tintOrder asc;
   end if;   

   IF v_byteMode = 0 then
      v_strColumns := ' I.numItemCode,I.vcItemName,COALESCE(DivisionMaster.numDivisionID,0) numDivisionID,COALESCE(DivisionMaster.tintCRMType,0) tintCRMType, ' || CAST(coalesce(v_numWarehouseID,0) AS VARCHAR(30)) || ' AS numWarehouseID, I.numDomainID,0 AS numRecOwner,0 AS numTerID, I.charItemType,I.bitSerialized,I.bitLotNo, I.numItemCode AS "numItemCode~211~0"';
      v_tintOrder := 0;
      v_WhereCondition := '';
      IF LENGTH(v_SearchText) > 0 then
		
         v_SearchQuery := ' CAST(COALESCE(I.numItemCode, 0) AS VARCHAR) ILIKE ''%' || coalesce(v_SearchText,'') || '%'' OR I.vcItemName ILIKE ''%' || coalesce(v_SearchText,'') || '%'' OR I.vcModelID ILIKE ''%' || coalesce(v_SearchText,'') || '%'' OR I.vcSKU ILIKE ''%' || coalesce(v_SearchText,'') || '%'' OR cmp.vcCompanyName ILIKE ''%' || coalesce(v_SearchText,'') || '%'' OR I.vcManufacturer ILIKE ''%' || coalesce(v_SearchText,'') || '%''';
      end if;
		 --WHERE bitCustomField=1
      select   tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, 
	  vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID 
	  INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
      v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,
      v_bitAllowSorting,v_bitAllowEdit,v_ListRelID 
	  FROM    tt_TEMPFORM    
	  ORDER BY tintOrder ASC 
	  LIMIT 1;
	  
      IF((SELECT COUNt(*) FROM tt_TEMPFORM WHERE vcDbColumnName = 'StockQtyCount'  OR  vcDbColumnName = 'StockQtyAdjust')  > 0) then
		
         IF coalesce(v_numFilteredWarehouseID,0) = 0 then
		
            v_strColumns := coalesce(v_strColumns,'') || ',0 as numIndividualOnHandQty,0 as numAvailabilityQty,0 AS monAverageCost,0 as numWareHouseItemId,0 AS numAssetChartAcntId';
         ELSEIF coalesce(v_numFilteredWarehouseID,0) > 0
         then
		
            v_strColumns := coalesce(v_strColumns,'') || ',WarehouseItems.numOnHand as numIndividualOnHandQty,(WarehouseItems.numOnHand + WarehouseItems.numAllocation) as numAvailabilityQty,I.monAverageCost,WarehouseItems.numWareHouseItemId as numWareHouseItemId,I.numAssetChartAcntId,
			((COALESCE(WarehouseItems.numOnHand,0) + COALESCE(WarehouseItems.numAllocation,0))*COALESCE(I.monAverageCost ,0)) as StockValue';
         end if;
      end if;
      WHILE v_tintOrder > 0 LOOP
         IF v_bitCustom = false then
            IF v_vcLookBackTableName = 'Item' then
               v_Prefix := 'I.';
            ELSEIF v_vcLookBackTableName = 'CompanyInfo'
            then
               v_Prefix := 'cmp.';
            ELSEIF v_vcLookBackTableName = 'Vendor'
            then
               v_Prefix := 'V.';
            end if;
            v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || '~0';
            IF v_vcAssociatedControlType = 'SelectBox' then
				
               IF v_vcListItemType = 'LI' then
					
                  v_strColumns := coalesce(v_strColumns,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
                  v_WhereCondition := coalesce(v_WhereCondition,'') || ' LEFT JOIN ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' ON L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
               ELSEIF v_vcListItemType = 'UOM'
               then
					
                  v_strColumns := coalesce(v_strColumns,'') || ',COALESCE((SELECT vcUnitName FROM UOM WHERE numUOMId = I.' || coalesce(v_vcDbColumnName,'') || '),'''')' || ' "' || coalesce(v_vcColumnName,'') || '"';
               ELSEIF v_vcListItemType = 'IG'
               then
					
                  v_strColumns := coalesce(v_strColumns,'') || ',COALESCE((SELECT vcItemGroup FROM ItemGroups WHERE numItemGroupID = I.numItemGroup),'''')' || ' "' || coalesce(v_vcColumnName,'') || '"';
               end if;
            ELSEIF v_vcDbColumnName = 'vcPathForTImage'
            then
		   		
               v_strColumns := coalesce(v_strColumns,'') || ',COALESCE(II.vcPathForTImage,'''') AS ' || ' "' || coalesce(v_vcColumnName,'') || '"';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' LEFT JOIN ItemImages II ON II.numItemCode = I.numItemCode AND bitDefault = true';
            ELSEIF v_vcLookBackTableName = 'Item' AND v_vcDbColumnName = 'monListPrice'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',CASE WHEN I.charItemType=''P'' THEN COALESCE((SELECT monWListPrice FROM WarehouseItems WHERE numDomainID=I.numDomainID AND numItemID=I.numItemCode AND monWListPrice > 0 LIMIT 1),0.00) ELSE COALESCE(I.monListPrice,0.00) END AS ' || ' "' || coalesce(v_vcColumnName,'') || '"';
            ELSEIF v_vcLookBackTableName = 'Item' AND v_vcDbColumnName = 'ItemType'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',' || '(CASE 
																WHEN I.charItemType=''P'' 
																THEN 
																	CASE 
																		WHEN COALESCE(I.bitAssembly,false) = true THEN ''Assembly''
																		WHEN COALESCE(I.bitKitParent,false) = true THEN ''Kit''
																		WHEN COALESCE(I.bitAsset,false) = true AND COALESCE(I.bitRental,false) = false THEN ''Asset''
																		WHEN COALESCE(I.bitAsset,false) = true AND COALESCE(I.bitRental,false) = true THEN ''Rental Asset''
																		WHEN COALESCE(I.bitSerialized,false) = true AND COALESCE(I.bitAsset,false) = false  THEN ''Serialized''
																		WHEN COALESCE(I.bitSerialized,false) = true AND COALESCE(I.bitAsset,false) = true AND COALESCE(I.bitRental,false) = false THEN ''Serialized Asset''
																		WHEN COALESCE(I.bitSerialized,false) = true AND COALESCE(I.bitAsset,false) = true AND COALESCE(I.bitRental,false) = true THEN ''Serialized Rental Asset''
																		WHEN COALESCE(I.bitLotNo,false) = true THEN ''Lot #''
																		ELSE ''Inventory Item'' 
																	END
																WHEN I.charItemType=''N'' THEN ''Non Inventory Item'' 
																WHEN I.charItemType=''S'' THEN ''Service'' 
																WHEN I.charItemType=''A'' THEN ''Accessory'' 
															END)' || ' AS ' || ' "' || coalesce(v_vcColumnName,'') || '"';
            ELSEIF v_vcLookBackTableName = 'Item' AND v_vcDbColumnName = 'monStockValue'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',WarehouseItems.monStockValue' || ' "' || coalesce(v_vcColumnName,'') || '"';
            ELSEIF v_vcLookBackTableName = 'Item' AND v_vcDbColumnName = 'StockQtyCount'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',0' || ' "' || coalesce(v_vcColumnName,'') || '"';
            ELSEIF v_vcLookBackTableName = 'Item' AND v_vcDbColumnName = 'StockQtyAdjust'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',0' || ' "' || coalesce(v_vcColumnName,'') || '"';
            ELSEIF v_vcLookBackTableName = 'WareHouseItems' AND v_vcDbColumnName = 'vcLocation' AND coalesce(v_numFilteredWarehouseID,0) = 0
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ', (SELECT string_agg(WIL.vcLocation, '', '') FROM WareHouseItems WI  LEFT JOIN WarehouseLocation AS WIL ON WI.numWLocationId=WIL.numWLocationId WHERE WI.numItemID=I.numItemCode )' || ' AS ' || ' "' || coalesce(v_vcColumnName,'') || '" ';
            ELSEIF v_vcLookBackTableName = 'WareHouseItems' AND v_vcDbColumnName = 'vcLocation' AND coalesce(v_numFilteredWarehouseID,0) > 0
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ', WarehouseItems.vcLocation AS ' || ' "' || coalesce(v_vcColumnName,'') || '" ';
            ELSEIF v_vcLookBackTableName = 'Warehouses' AND v_vcDbColumnName = 'vcWareHouse'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ', (SELECT string_agg(W.vcWareHouse, '', '') FROM WareHouseItems WI JOIN Warehouses W ON WI.numWareHouseID=W.numWareHouseID WHERE WI.numItemID=I.numItemCode )' || ' AS ' || ' "' || coalesce(v_vcColumnName,'') || '"';
            ELSEIF v_vcLookBackTableName = 'WorkOrder' AND v_vcDbColumnName = 'WorkOrder'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ', (CASE WHEN (SELECT SUM(WO.numQtyItemsReq) from WorkOrder WO where WO.numItemCode=I.numItemCode and WO.numWOStatus=0) > 0 THEN 1 ELSE 0 END)' || ' AS ' || ' "' || coalesce(v_vcColumnName,'') || '"';
            ELSEIF v_vcDbColumnName = 'vcPriceLevelDetail'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || CONCAT(', fn_GetItemPriceLevelDetail( ',COALESCE(v_numDomainID,0),',I.numItemCode,',COALESCE(v_numWarehouseID,0),')') || ' "' || coalesce(v_vcColumnName,'') || '"';
            ELSE
               If v_vcDbColumnName <> 'numItemCode' AND v_vcDbColumnName <> 'vcPriceLevelDetail' then --WE AE ALREADY ADDING COLUMN IN SELECT CLUASE DIRECTLY
					
                  v_strColumns := coalesce(v_strColumns,'') || ',' ||(CASE v_vcLookBackTableName WHEN 'Item' THEN 'I' WHEN 'CompanyInfo' THEN 'cmp' WHEN 'Vendor' THEN 'V' ELSE v_vcLookBackTableName END) || '.' || coalesce(v_vcDbColumnName,'') || ' AS ' || ' "' || coalesce(v_vcColumnName,'') || '"';
               end if;
            end if;
         ELSEIF v_bitCustom = true
         then
			
            v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || '~1';
            select   FLd_label, fld_type, 'Cust' || SUBSTR(CAST(Fld_id AS VARCHAR(10)),1,10), Grp_id INTO v_vcFieldName,v_vcAssociatedControlType,v_vcDbColumnName,v_Grp_ID FROM
            CFW_Fld_Master WHERE
            CFW_Fld_Master.Fld_id = v_numFieldId;
            IF v_Grp_ID = 9 then
				
               v_strColumns := coalesce(v_strColumns,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ItemAttributes CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || ' ON CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numDOmainID=' || SUBSTR(CAST(COALESCE(v_numDomainID,0) AS VARCHAR(30)),1,30) || ' AND CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id='
               || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
               || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || '.numItemCode=I.numItemCode   ';
               v_WhereCondition := coalesce(v_WhereCondition,'')
               || ' left Join ListDetails L'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || ' on L'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || '.numListItemID=(CASE WHEN isnumeric(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value::VARCHAR) THEN CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value ELSE ''0'' END)::NUMERIC';
            ELSEIF v_vcAssociatedControlType = 'TextBox' OR v_vcAssociatedControlType = 'TextArea'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',CFW'  || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)  || '.Fld_Value  "' || coalesce(v_vcColumnName,'') || '"';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Item CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || ' ON CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id='
               || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
               || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || '.RecId=I.numItemCode   ';
            ELSEIF v_vcAssociatedControlType = 'CheckBox'
            then
				
               v_strColumns := coalesce(v_strColumns,'')
               || ',case when COALESCE(CFW'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || '.Fld_Value,''0'')=''0'' then 0 when COALESCE(CFW'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || '.Fld_Value,''0'')=''1'' then 1 end   "'
               ||  coalesce(v_vcColumnName,'')
               || '"';
               v_WhereCondition := coalesce(v_WhereCondition,'')
               || ' left Join CFW_FLD_Values_Item CFW'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || ' ON CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id='
               || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
               || ' and CFW'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || '.RecId=I.numItemCode   ';
            ELSEIF v_vcAssociatedControlType = 'DateField'
            then
				
               v_strColumns := coalesce(v_strColumns,'')
               || ',FormatedDateFromDate(CFW'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || '.Fld_Value,'
               || SUBSTR(CAST(COALESCE(v_numDomainID,0) AS VARCHAR(10)),1,10)
               || ')  "' || coalesce(v_vcColumnName,'') || '"';
               v_WhereCondition := coalesce(v_WhereCondition,'')
               || ' left Join CFW_FLD_Values_Item CFW'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || ' on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id='
               || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
               || ' and CFW'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || '.RecId=I.numItemCode   ';
            ELSEIF v_vcAssociatedControlType = 'SelectBox'
            then
				
               v_vcDbColumnName := 'Cust' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10);
               v_strColumns := coalesce(v_strColumns,'') || ',L'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
               v_WhereCondition := coalesce(v_WhereCondition,'')
               || ' left Join CFW_FLD_Values_Item CFW'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || ' on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id='
               || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
               || ' and CFW'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || '.RecId=I.numItemCode    ';
               v_WhereCondition := coalesce(v_WhereCondition,'')
               || ' left Join ListDetails L'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || ' on L'
               || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
               || '.numListItemID=(CASE WHEN isnumeric(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value::VARCHAR) THEN CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value ELSE ''0'' END)::NUMERIC ';
            ELSE
               v_strColumns := coalesce(v_strColumns,'') || CONCAT(',GetCustFldValueItem(',v_numFieldId,',I.numItemCode)') || ' "' || coalesce(v_vcColumnName,'') || '"';
            end if;
         end if;
			 --AND bitCustomField=1
         select   tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
         v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,
         v_bitAllowSorting,v_bitAllowEdit,v_ListRelID 
		 FROM    tt_TEMPFORM 
		 WHERE   tintOrder > v_tintOrder -1   
		 ORDER BY tintOrder ASC 
		 LIMIT 1;
         
		 GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         IF SWV_RowCount = 0 THEN
            v_tintOrder := 0;
         END IF;
      END LOOP;  
		
		--DECLARE @vcApplyRackFilter as VARCHAR(MAX)=''
		--DECLARE @vcApplyRackFilterSearch as VARCHAR(MAX)=''
		--IF(COALESCE(@numFilteredWarehouseID,0)>0)
		--BEGIN
		--	SET @vcApplyRackFilter=' LEFT JOIN WareHouseItems AS WRF ON WRF.numItemId=I.numItemCode LEFT JOIN WarehouseLocation AS WFL ON WRF.numWLocationId=WFL.numWLocationId AND WRF.numWareHouseId=WFL.numWareHouseId '
		--	SET @vcApplyRackFilterSearch=@vcApplyRackFilterSearch + ' AND WRF.numWareHouseId='''+CAST(@numFilteredWarehouseID AS VARCHAR(500))+''' '
		--	SET @vcApplyRackFilterSearch=@vcApplyRackFilterSearch + ' AND WFL.numWareHouseId='''+CAST(@numFilteredWarehouseID AS VARCHAR(500))+''' '
		--	IF(COALESCE(@vcAsile,'')<>'')
		--	BEGIN
		--		SET @vcApplyRackFilterSearch=@vcApplyRackFilterSearch + ' AND WFL.vcAisle='''+CAST(@vcAsile AS VARCHAR(500))+''' '
		--	END
		--	IF(COALESCE(@vcRack,'')<>'')
		--	BEGIN
		--		SET @vcApplyRackFilterSearch=@vcApplyRackFilterSearch + ' AND WFL.vcRack='''+CAST(@vcRack AS VARCHAR(500))+''' '
		--	END
		--	IF(COALESCE(@vcShelf,'')<>'')
		--	BEGIN
		--		SET @vcApplyRackFilterSearch=@vcApplyRackFilterSearch + ' AND WFL.vcShelf='''+CAST(@vcShelf AS VARCHAR(500))+''' '
		--	END
		--	IF(COALESCE(@vcBin,'')<>'')
		--	BEGIN
		--		SET @vcApplyRackFilterSearch=@vcApplyRackFilterSearch + ' AND WFL.vcBin='''+CAST(@vcBin AS VARCHAR(500))+''' '
		--	END
		--END
		
      v_strSQL := CONCAT(' FROM 
							Item I ##JOINPLACEHOLDER##
						LEFT JOIN
							Vendor V
						ON
							I.numItemCode = V.numItemCode
							AND I.numVendorID = V.numVendorID
						LEFT JOIN
							DivisionMaster
						ON
							V.numVendorID = DivisionMaster.numDivisionID
						LEFT JOIN
							CompanyInfo cmp
						ON
							DivisionMaster.numCompanyId = cmp.numCompanyId
						LEFT JOIN
							CustomerPartNumber
						ON
							CustomerPartNumber.numItemCode = I.numItemCode AND CustomerPartNumber.numCompanyId = 0
						',(CASE
      WHEN coalesce(v_numFilteredWarehouseID,0) > 0
      THEN CONCAT(' INNER JOIN LATERAL (
											SELECT
												WareHouseItems.numWareHouseItemID,
												numOnHand AS numOnHand,
												COALESCE(numOnHand,0) + COALESCE(numAllocation,0) AS numAvailable,
												numBackOrder AS numBackOrder,
												numOnOrder AS numOnOrder,
												numAllocation AS numAllocation,
												numReorder AS numReorder,
												numOnHand * (CASE WHEN COALESCE(I.bitVirtualInventory,false) = true THEN 0 ELSE COALESCE(I.monAverageCost,0) END) AS monStockValue,
												WarehouseLocation.vcLocation
											FROM
												WareHouseItems
											LEFT JOIN 
												WarehouseLocation 
											ON 
												WareHouseItems.numWLocationId=WarehouseLocation.numWLocationId
											WHERE
												numItemID = I.numItemCode
												AND (WareHouseItems.numWareHouseID=',coalesce(v_numFilteredWarehouseID,0),
         ' OR ',coalesce(v_numFilteredWarehouseID,0),'=0)',CASE WHEN coalesce(v_vcAsile,'') <> '' THEN CONCAT(' AND WarehouseLocation.vcAisle=''',coalesce(v_vcAsile,'') || '''') ELSE '' END,
         CASE WHEN coalesce(v_vcRack,'') <> '' THEN CONCAT(' AND WarehouseLocation.vcRack=''',coalesce(v_vcRack,'') || '''') ELSE '' END,
         CASE WHEN coalesce(v_vcShelf,'') <> '' THEN CONCAT(' AND WarehouseLocation.vcShelf=''',coalesce(v_vcShelf,'') || '''') ELSE '' END,
         CASE WHEN coalesce(v_vcBin,'') <> '' THEN CONCAT(' AND WarehouseLocation.vcBin=''',coalesce(v_vcBin,'') || '''') ELSE '' END,
         ') WarehouseItems ON true ')
      ELSE ' LEFT JOIN LATERAL (
							SELECT
								SUM(numOnHand) AS numOnHand,
								SUM(COALESCE(numOnHand,0) + COALESCE(numAllocation,0)) AS numAvailable,
								SUM(numBackOrder) AS numBackOrder,
								SUM(numOnOrder) AS numOnOrder,
								SUM(numAllocation) AS numAllocation,
								SUM(numReorder) AS numReorder,
								SUM(numOnHand) * (CASE WHEN COALESCE(I.bitVirtualInventory,false) = true THEN 0 ELSE COALESCE(I.monAverageCost,0) END) AS monStockValue
							FROM
								WareHouseItems
							WHERE
								numItemID = I.numItemCode) WarehouseItems ON true '
      END),'
						  ', 
      v_WhereCondition);
      IF v_columnName = 'Backorder' OR v_columnName = 'WHI.numBackOrder' then
         v_columnName := 'numBackOrder';
      ELSEIF v_columnName = 'OnOrder' OR v_columnName = 'WHI.numOnOrder'
      then
         v_columnName := 'numOnOrder';
      ELSEIF v_columnName = 'OnAllocation' OR v_columnName = 'WHI.numAllocation'
      then
         v_columnName := 'numAllocation';
      ELSEIF v_columnName = 'Reorder' OR v_columnName = 'WHI.numReorder'
      then
         v_columnName := 'numReorder';
      ELSEIF v_columnName = 'WHI.numOnHand'
      then
         v_columnName := 'numOnHand';
      ELSEIF v_columnName = 'WHI.numAvailable'
      then
         v_columnName := 'numAvailable';
      ELSEIF v_columnName = 'monStockValue'
      then
         v_columnName := 'sum(numOnHand) * (CASE WHEN COALESCE(bitVirtualInventory,false) = true THEN 0 ELSE COALESCE(monAverageCost,0) END)';
      ELSEIF v_columnName = 'ItemType'
      then
         v_columnName := 'charItemType';
      ELSEIF v_columnName = 'numItemCode'
      then
         v_columnName := 'I.numItemCode';
      end if;
      v_column := v_columnName;
      IF v_columnName ilike '%Cust%' then
         IF POSITION('CFW.Cust' IN v_columnName) > 0 then
			
            v_fldId := REPLACE(v_columnName,'CFW.Cust','');
         ELSE
            v_fldId := REPLACE(v_columnName,'Cust','');
         end if;
         select   coalesce(vcAssociatedControlType,'') INTO v_CustomFieldType FROM View_DynamicCustomColumns WHERE numFieldID = cast(NULLIF(v_fldId,'') as NUMERIC)    LIMIT 1;
         IF coalesce(v_CustomFieldType,'') = 'SelectBox' OR coalesce(v_CustomFieldType,'') = 'ListBox' then
			
            v_strSQL := coalesce(v_strSQL,'') || ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=i.numItemCode and CFW.fld_id= ' || coalesce(v_fldId,'') || ' ';
            v_strSQL := coalesce(v_strSQL,'') || ' left Join ListDetails LstCF on LstCF.numListItemID = (CASE WHEN isnumeric(CFW.Fld_Value) THEN CFW.Fld_Value::NUMERIC ELSE 0 END)  ';
            v_columnName := ' LstCF.vcData ';
         ELSE
            v_strSQL :=  coalesce(v_strSQL,'') || ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=i.numItemCode and CFW.fld_id= ' || coalesce(v_fldId,'') || ' ';
            v_columnName := 'CFW.Fld_Value';
         end if;
      end if;
      v_strWhere := CONCAT(' WHERE I.numDomainID = ', COALESCE(v_numDomainID,0));
      IF v_ItemType = 'P' then
		
         v_strWhere := coalesce(v_strWhere,'') || ' AND I.charItemType= ''P''';
      ELSEIF v_ItemType = 'S'
      then
		
         v_strWhere := coalesce(v_strWhere,'') || ' AND I.charItemType= ''S''';
      ELSEIF v_ItemType = 'N'
      then
		
         v_strWhere := coalesce(v_strWhere,'') || ' AND I.charItemType= ''N''';
      end if;
      IF v_bitContainer = true then
		
         v_strWhere := coalesce(v_strWhere,'') || ' AND I.bitContainer= ' || COALESCE(v_bitContainer, false)|| '';
      end if;
      IF v_bitAssembly = true then
		
         v_strWhere := coalesce(v_strWhere,'') || ' AND I.bitAssembly	= ' || COALESCE(v_bitAssembly, false) || '';
      ELSEIF v_ItemType = 'P'
      then
		
         v_strWhere := coalesce(v_strWhere,'') || ' AND COALESCE(I.bitAssembly, false)=false ';
      end if;
      IF v_IsKit then
		
         v_strWhere := coalesce(v_strWhere,'') || ' AND COALESCE(I.bitAssembly,false)=false AND I.bitKitParent=true';
      ELSEIF v_ItemType = 'P'
      then
		
         v_strWhere := coalesce(v_strWhere,'') || ' AND COALESCE(I.bitKitParent,false)=false ';
      end if;
      IF v_bitSerialized = true then
		
         v_strWhere := coalesce(v_strWhere,'') || ' AND (I.bitSerialized=true OR I.bitLotNo=true)';
      end if;
		--ELSE IF @ItemType = 'P'
		--BEGIN
		--	SET @strWhere = @strWhere + ' AND COALESCE(Item.bitSerialized,0)=0 AND COALESCE(Item.bitLotNo,0)=0 '
		--END

      IF v_bitAsset = true then
		
         v_strWhere := coalesce(v_strWhere,'') || ' AND COALESCE(I.bitAsset,false) = true';
      ELSEIF v_ItemType = 'P'
      then
		
         v_strWhere := coalesce(v_strWhere,'') || ' AND COALESCE(I.bitAsset,false) = false ';
      end if;
      IF v_bitRental = true then
		
         v_strWhere := coalesce(v_strWhere,'') || ' AND COALESCE(I.bitRental,false) = true AND COALESCE(I.bitAsset,false) = true ';
      ELSEIF v_ItemType = 'P'
      then
		
         v_strWhere := coalesce(v_strWhere,'') || ' AND COALESCE(I.bitRental,false) = false ';
      end if;
      IF v_SortChar <> '0' then
		
         v_strWhere := coalesce(v_strWhere,'') || ' AND I.vcItemName ilike ''' || coalesce(v_SortChar,'') || '%''';
      end if;
      IF v_ItemClassification <> cast(NULLIF('0','') as NUMERIC(9,0)) then
		
         v_strWhere := coalesce(v_strWhere,'') || ' AND I.numItemClassification=' || SUBSTR(CAST(v_ItemClassification AS VARCHAR(15)),1,15);
      end if;
      IF v_IsArchive = false then
		
         v_strWhere := coalesce(v_strWhere,'') || ' AND COALESCE(I.IsArchieve,false) = false';
      end if;
      IF v_numItemGroup > 0 then
		
         v_strWhere := coalesce(v_strWhere,'') || ' AND I.numItemGroup = ' || SUBSTR(CAST(COALESCE(v_numItemGroup,0) AS VARCHAR(20)),1,20);
      end if;
      IF v_numItemGroup = -1 then
		
         v_strWhere := coalesce(v_strWhere,'') || ' AND I.numItemGroup in (select numItemGroupID from ItemGroups where numDomainID=' || SUBSTR(CAST(COALESCE(v_numDomainID,0) AS VARCHAR(20)),1,20) || ')';
      end if;
	  
      IF v_bitMultiSelect then
		
         v_strWhere := coalesce(v_strWhere,'') || ' AND COALESCE(I.bitKitParent,false)=false ';
      end if;
      IF v_vcRegularSearchCriteria <> '' then
		
         v_strWhere := coalesce(v_strWhere,'') || ' AND ' || coalesce(v_vcRegularSearchCriteria,'');
      end if;
      IF v_vcCustomSearchCriteria <> '' then
		 
		--comment
         v_strWhere := coalesce(v_strWhere,'') || ' AND ' || coalesce(v_vcCustomSearchCriteria,'');
      end if;
      IF LENGTH(v_SearchQuery) > 0 then
		
         v_strWhere := coalesce(v_strWhere,'') || ' AND (' || coalesce(v_SearchQuery,'') || ') ';
      end if;
      v_strWhere := coalesce(v_strWhere,'') || coalesce(v_Where,'');
      IF coalesce(v_tintDashboardReminderType,0) = 17 then
		
         v_strWhere := CONCAT(v_strWhere,' AND I.numItemCode IN (','SELECT DISTINCT
																		numItemID
																	FROM
																		WareHouseItems WIInner
																	INNER JOIN
																		Item IInner
																	ON
																		WIInner.numItemID = IInner.numItemCode
																	WHERE
																		WIInner.numDomainID=',COALESCE(v_numDomainID,0),
         '
																		AND IInner.numDomainID = ',COALESCE(v_numDomainID,0),'
																		AND COALESCE(IInner.bitArchiveItem,false) = false
																		AND COALESCE(IInner.IsArchieve,false) = false
																		AND COALESCE(WIInner.numReorder,0) > 0 
																		AND COALESCE(WIInner.numOnHand,0) < COALESCE(WIInner.numReorder,0)',
         ') ');
      end if;
      IF v_tintUserRightType = 1 AND NOT EXISTS(SELECT numUserDetailId FROM UserMaster WHERE numDomainID = COALESCE(v_numDomainID,0) AND numUserDetailId = v_numUserCntID) then
		
         v_strWhere := CONCAT(v_strWhere,' AND I.numItemCode IN (SELECT 
																		V.numItemCode
																	FROM 
																		ExtranetAccountsDtl EAD 
																	INNER JOIN 
																		ExtarnetAccounts EA
																	ON 
																		EAD.numExtranetID=EA.numExtranetID 
																	INNER JOIN 
																		Vendor V 
																	ON 
																		EA.numDivisionID=V.numVendorID 
																	WHERE 
																		EAD.numDomainID=',COALESCE(v_numDomainID,0),' 
																		AND V.numDomainID=',
         COALESCE(v_numDomainID,0),' 
																		AND EAD.numContactID=',
         '''' || CAST(COALESCE(v_numUserCntID,0) AS VARCHAR) || '''' ,')');
      end if;
      v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;
      v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);

		-- EXECUTE FINAL QUERY
      IF coalesce(v_numFilteredWarehouseID,0) > 0 then
		v_strSQL := REPLACE(v_strSQL,'##JOINPLACEHOLDER##','');
         v_strFinal := CONCAT(v_strFinal,' DROP TABLE IF EXISTS tt_TEMP2; 
							  			   CREATE TEMPORARY TABLE tt_TEMP2 AS 
							  			   SELECT ROW_NUMBER() OVER(ORDER BY ',v_columnName,' ',v_columnSortOrder,
         								   ',WarehouseItems.vcLocation asc) RowID, ',v_strColumns,v_strSQL,v_strWhere,' ;');
         EXECUTE v_strFinal;
		 RAISE NOTICE '%',v_strFinal;
		 SELECT COUNT(*) INTO v_TotRecs FROM tt_TEMP2; 
		 v_strFinal := '
		 SELECT * FROM tt_TEMP2 
		 WHERE RowID > ' || SUBSTR(CAST(v_firstRec AS VARCHAR(100)),1,100) || ' 
		 AND RowID <' || SUBSTR(CAST(v_lastRec AS VARCHAR(100)),1,100) || '; ';
      ELSEIF v_CurrentPage = -1 AND v_PageSize = -1
      then
         v_strFinal := CONCAT(' CREATE TEMPORARY TABLE tt_TEMPTable AS 
		 						SELECT I.numItemCode, I.vcItemName, I.vcModelID ',REPLACE(v_strSQL,'##JOINPLACEHOLDER##',''),v_strWhere,';'); 

		RAISE NOTICE '%',v_strFinal;
		EXECUTE v_strFinal;
		SELECT COUNT(*) INTO v_TotRecs FROM tt_TEMPTable;
		v_strFinal := ' SELECT * FROM tt_TEMPTable; ';
      ELSE
         v_strFinal := CONCAT(' DROP TABLE IF EXISTS tt_TEMP2; 
						 	  	CREATE TEMPORARY TABLE tt_TEMP2 AS 
							    SELECT ROW_NUMBER() OVER(ORDER BY ',v_columnName,' ',v_columnSortOrder,
       							') RowID, I.numItemCode ',REPLACE(v_strSQL,'##JOINPLACEHOLDER##',''),v_strWhere,';'); 
		 RAISE NOTICE '%',v_strFinal;
		 EXECUTE v_strFinal;			
		 v_strSQL := REPLACE(v_strSQL,'##JOINPLACEHOLDER##','JOIN tt_TEMP2 tblAllItems ON I.numItemCode = tblAllItems.numItemCode');
		 v_strFinal := CONCAT(' DROP TABLE IF EXISTS tt_TEMPTable; 
						 		CREATE TEMPORARY TABLE tt_TEMPTable AS 
						 		SELECT RowID, ',v_strColumns,v_strSQL,v_strWhere,' AND tblAllItems.RowID > ',COALESCE(v_firstRec, 0),' and tblAllItems.RowID <',v_lastRec,';'); 
	     
		 RAISE NOTICE '%',v_strFinal;
		 EXECUTE v_strFinal;
		 SELECT COUNT(*) INTO v_TotRecs FROM tt_TEMP2; 
		 v_strFinal := ' SELECT  * FROM  tt_TEMPTable ORDER BY RowID; ';
      END IF;
	  
   END IF;

   UPDATE tt_TEMPFORM
   SET intColumnWidth =(CASE WHEN coalesce(intColumnWidth,0) <= 0 THEN 25 ELSE intColumnWidth END);
 
   RAISE NOTICE '%',v_strFinal;
   OPEN SWV_RefCur FOR 
   EXECUTE v_strFinal;
	  
   OPEN SWV_RefCur2 FOR
   SELECT * FROM tt_TEMPFORM;
   
END; 
$$;


