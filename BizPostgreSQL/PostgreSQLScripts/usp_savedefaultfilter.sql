-- Stored procedure definition script USP_SaveDefaultFilter for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SaveDefaultFilter(v_numDomainId NUMERIC(9,0),
    v_numUserCntId NUMERIC(9,0),
    v_numRelation NUMERIC(9,0),
    v_FilterId NUMERIC(9,0),
    v_bitDefault BOOLEAN,
    v_FormId NUMERIC(9,0),
    v_GroupId NUMERIC(9,0),
    v_vcFilter VARCHAR(4000),
    v_bitFlag BOOLEAN DEFAULT false  -- 0 = set initial page and default filter id, 1= Update additinal filters of page to vcFilter
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_bitFlag = false then
        
      IF v_bitDefault = true
      AND (v_FormId = 34
      OR v_FormId = 35
      OR v_FormId = 36
      OR v_FormId = 10) then
                
         UPDATE  RelationsDefaultFilter
         SET     bitInitialPage = false
         WHERE   numDomainID = v_numDomainId
         AND numUserCntId = v_numUserCntId
         AND numformId IN(34,35,36,10);
      end if;
      IF v_bitDefault = true
      AND (v_FormId = 38
      OR v_FormId = 39
      OR v_FormId = 40
      OR v_FormId = 41) then
                
         UPDATE  RelationsDefaultFilter
         SET     bitInitialPage = false
         WHERE   numDomainID = v_numDomainId
         AND numUserCntId = v_numUserCntId
         AND numformId IN(38,39,40,41);
      end if;
      IF NOT EXISTS(SELECT * FROM    RelationsDefaultFilter
      WHERE   numDomainID = v_numDomainId
      AND numUserCntId = v_numUserCntId
      AND numformId = v_FormId) then
                
         INSERT  INTO RelationsDefaultFilter(numRelationId,
                              numDomainID,
                              numUserCntId,
                              numFilterId,
                              bitInitialPage,
                              numformId,
                              tintGroupId)
                    VALUES(v_numRelation,
                              v_numDomainId,
                              v_numUserCntId,
                              v_FilterId,
                              v_bitDefault,
                              v_FormId,
                              v_GroupId);
      ELSE
         UPDATE  RelationsDefaultFilter
         SET
         bitInitialPage = v_bitDefault
         WHERE   numDomainID = v_numDomainId
         AND numUserCntId = v_numUserCntId
         AND numformId = v_FormId
         AND tintGroupId = v_GroupId;
      end if;
   ELSE
      IF NOT EXISTS(SELECT * FROM    RelationsDefaultFilter
      WHERE   numDomainID = v_numDomainId
      AND numUserCntId = v_numUserCntId
      AND numformId = v_FormId  AND tintGroupId = v_GroupId AND numRelationId = v_numRelation) then
                
         INSERT  INTO RelationsDefaultFilter(numRelationId,
                              numDomainID,
                              numUserCntId,
                              numFilterId,
                              bitInitialPage,
                              numformId,
                              tintGroupId,vcFilter)
                    VALUES(v_numRelation,
                              v_numDomainId,
                              v_numUserCntId,
                              v_FilterId,
                              v_bitDefault,
                              v_FormId,
                              v_GroupId,v_vcFilter);
      ELSE
         UPDATE  RelationsDefaultFilter
         SET     numRelationId = v_numRelation,tintGroupId = v_GroupId,vcFilter = v_vcFilter,
         numFilterId = v_FilterId
         WHERE   numDomainID = v_numDomainId
         AND numUserCntId = v_numUserCntId
         AND numformId = v_FormId AND tintGroupId = v_GroupId AND numRelationId = v_numRelation;
      end if;
   end if;
   RETURN;
END; $$;


