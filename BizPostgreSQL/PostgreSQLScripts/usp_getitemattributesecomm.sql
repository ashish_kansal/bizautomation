-- Stored procedure definition script USP_GetItemAttributesEcomm for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetItemAttributesEcomm(v_numItemCode NUMERIC(9,0) DEFAULT 0,  
v_numWarehouseID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitMatrix  BOOLEAN;  
   v_numItemGroup  NUMERIC(18,0);
BEGIN
   select   coalesce(bitMatrix,false), coalesce(numItemGroup,0) INTO v_bitMatrix,v_numItemGroup FROM
   Item WHERE
   numItemCode = v_numItemCode;  

   IF v_bitMatrix = true AND v_numItemGroup > 0 then
	
      open SWV_RefCur for
      SELECT DISTINCT
      CFW_Fld_Master.Fld_id
			,Fld_label
			,fld_type
			,CFW_Fld_Master.numlistid
			,vcURL
			,coalesce(ItemAttributes.Fld_Value,0) AS Fld_Value
      FROM
      ItemGroupsDTL
      INNER JOIN
      CFW_Fld_Master
      ON
      ItemGroupsDTL.numOppAccAttrID = CFW_Fld_Master.Fld_id
      INNER JOIN
      ItemAttributes
      ON
      ItemAttributes.numItemCode = v_numItemCode
      AND ItemAttributes.Fld_ID = CFW_Fld_Master.Fld_id
      WHERE
      ItemGroupsDTL.numItemGroupID = v_numItemGroup
      AND tintType = 2
      ORDER BY
      CFW_Fld_Master.Fld_id;
   ELSE
      open SWV_RefCur for
      SELECT DISTINCT
      CFW_Fld_Master.Fld_id
			,Fld_label
			,fld_type
			,CFW_Fld_Master.numlistid
			,vcURL
			,0 AS Fld_Value
      FROM
      ItemGroupsDTL
      JOIN
      CFW_Fld_Master
      ON
      numOppAccAttrID = Fld_ID
      INNER JOIN
      Item
      ON
      numItemGroup = ItemGroupsDTL.numItemGroupID
      AND tintType = 2
      LEFT JOIN
      WareHouseItems WI
      ON
      WI.numItemID = numItemcode
      LEFT JOIN
      CFW_Fld_Values_Serialized_Items
      ON
      RecId = WI.numWareHouseItemID
      WHERE
      numItemcode = v_numItemCode
      AND (numWarehouseID = v_numWarehouseID OR charItemType <> 'P')
      ORDER BY
      CFW_Fld_Master.Fld_id;
   end if;
   RETURN;
END; $$;


