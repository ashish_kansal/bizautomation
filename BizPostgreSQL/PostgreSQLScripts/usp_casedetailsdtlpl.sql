-- Stored procedure definition script USP_CaseDetailsdtlPL for PostgreSQL
CREATE OR REPLACE FUNCTION USP_CaseDetailsdtlPL(v_numCaseId NUMERIC(9,0),                
v_numDomainID NUMERIC(18,0) ,      
v_ClientTimeZoneOffset INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strOppName  VARCHAR(200);
BEGIN
   v_strOppName := '';            
   select   coalesce(v_strOppName,'') || ' <br> ' || vcpOppName INTO v_strOppName from CaseOpportunities CO
   join OpportunityMaster OM on OM.numOppId = CO.numoppid and numCaseId = v_numCaseId
   and CO.numDomainid = v_numDomainID;            
            
                                  
   open SWV_RefCur for select v_strOppName as "OppName",C.numContactId AS "numContactId",vcCaseNumber AS "vcCaseNumber",
 intTargetResolveDate AS "intTargetResolveDate",textSubject AS "textSubject",Div.numDivisionID AS "numDivisionID",Div.tintCRMType AS "tintCRMType",  C.numContractID AS "numContractID",
 (select vcData from Listdetails where numListItemID = numStatus) as "numStatusName",
 numStatus AS "numStatus",
 (select vcData from Listdetails where numListItemID = numPriority) as "numPriorityName",
 numPriority AS "numPriority",
 textDesc AS "textDesc",
 textInternalComments AS "textInternalComments",
 (select vcData from Listdetails where numListItemID = numReason) as "numReasonName",
 numReason AS "numReason",
 (select vcData from Listdetails where numListItemID = numOrigin) as "numOriginName",
  numOrigin AS "numOrigin",
 (select vcData from Listdetails where numListItemID = numType) as "numTypeName",
 numType AS "numType",
 fn_GetContactName(C.numCreatedby) || ' ' || C.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) as "CreatedBy",
 fn_GetContactName(C.numModifiedBy) || ' ' || C.bintModifiedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) as "ModifiedBy",
 fn_GetContactName(C.numRecOwner) as "RecOwner",
 tintSupportKeyType AS "tintSupportKeyType",
 fn_GetContactName(C.numAssignedTo) as "numAssignedToName",
 fn_GetContactName(C.numContactId) as "numContactIdName",
 C.numAssignedTo AS "numAssignedTo",
 C.numAssignedBy AS "numAssignedBy",coalesce(Addc.vcFirstName,'') || ' ' || coalesce(Addc.vcLastname,'') as "Name",
 coalesce(vcEmail,'') as "vcEmail",coalesce(numPhone,'') || case when NULLIF(numPhoneExtension,'') is null then '' when numPhoneExtension = '' then '' else '/ ' || numPhoneExtension end  as "Phone",
 Com.numCompanyId AS "numCompanyId",Com.vcCompanyName AS "vcCompanyName",(select count(*) from Comments where numCaseID = v_numCaseId)  as "NoofCases",
 coalesce((SELECT COUNT(*) FROM CaseOpportunities WHERE numCaseId = v_numCaseId),
   0) AS "LinkedItemCount",
 coalesce((SELECT CM.vcContractName FROM ContractManagement CM WHERE CM.numContractId = C.numContractID),'') AS "ContractName",
COALESCE((SELECT string_agg(A.vcFirstName || ' ' || A.vcLastname || CASE WHEN coalesce(SR.numContactType,0) > 0 THEN '(' || fn_GetListItemName(SR.numContactType) || ')' ELSE '' END,',')
         FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId = SR.numAssignedTo
         JOIN AdditionalContactsInformation A ON UM.numUserDetailId = A.numContactId
         WHERE SR.numDomainID = C.numDomainID AND SR.numModuleID = 7 AND SR.numRecordID = C.numCaseId
         AND UM.numDomainID = C.numDomainID and UM.numDomainID = A.numDomainID),'') AS "numShareWith",
 (SELECT COUNT(*) FROM   GenericDocuments WHERE  numRecID = v_numCaseId AND vcDocumentSection = 'CS') AS "DocumentCount",
 coalesce(C.vcSolutionShortMessage,'')  AS "SoultionName",
 (SELECT COUNT(*) FROM   CaseSolutions where numCaseId = v_numCaseId) AS "SolutionCount",
 (SELECT (coalesce(C.numIncidents,0)  -coalesce(C.numIncidentsUsed,0)) FROM Contracts AS C WHERE C.numDivisonId = Div.numDivisionID AND numDomainId = v_numDomainID AND intType = 3) AS "numIncidentLeft"
   from Cases C
   left join AdditionalContactsInformation Addc
   on Addc.numContactId = C.numContactId
   join DivisionMaster Div
   on Div.numDivisionID = Addc.numDivisionId
   join CompanyInfo Com
   on Com.numCompanyId = Div.numCompanyID
   where numCaseId = v_numCaseId;
END; $$;













