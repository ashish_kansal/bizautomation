-- Stored procedure definition script usp_getCustReportsummationlist for PostgreSQL
CREATE OR REPLACE FUNCTION usp_getCustReportsummationlist(v_ReportId NUMERIC DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select * from CustReportSummationlist where numCustomReportId = v_ReportId;
END; $$;












