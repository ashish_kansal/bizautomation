CREATE OR REPLACE FUNCTION Recurrence_Sel(v_OrganizerName VARCHAR(64), v_StartDateTimeUtc TIMESTAMP, -- the start date before which no activities are retrieved  
 v_EndDateTimeUtc TIMESTAMP, INOUT SWV_RefCur refcursor
)
LANGUAGE plpgsql  
   AS $$
   DECLARE
   v_ResourceID  INTEGER;
BEGIN
   if SWF_IsNumeric(v_OrganizerName) = true then

      v_ResourceID := CAST(v_OrganizerName AS NUMERIC(9,0));
   else
      v_ResourceID := -999;
   end if;  

   open SWV_RefCur for SELECT
   Recurrence.recurrenceid,
  Recurrence.EndDateUtc,
  Recurrence.DayOfWeekMaskUtc,
  Recurrence.UtcOffset,
  Recurrence.DayOfMonth,
  Recurrence.MonthOfYear,
  Recurrence.PeriodMultiple,
  Recurrence.Period,
  Recurrence.EditType,
  cast(coalesce(Recurrence.LastReminderDateTimeUtc,'1900-01-01 00:00:00.000') as TIMESTAMP) AS LastReminderDateTimeUtc,
  Recurrence._ts
   FROM
   Recurrence INNER JOIN(Activity INNER JOIN(Resource INNER JOIN ActivityResource
   ON Resource.ResourceID = ActivityResource.ResourceID) ON Activity.ActivityID = ActivityResource.ActivityID)
   ON Activity.RecurrenceID = Recurrence.recurrenceid
   WHERE
  (Resource.ResourceID = v_ResourceID 
   AND  Activity.StartDateTimeUtc <= v_EndDateTimeUtc 
   AND  Recurrence.EndDateUtc > v_StartDateTimeUtc 
   AND  Activity.OriginalStartDateTimeUtc IS NULL) and
   Recurrence.recurrenceid > 0;
END; $$;






