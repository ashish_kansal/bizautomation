CREATE OR REPLACE FUNCTION USP_GetItemGroups(v_numDomainID NUMERIC(9,0) DEFAULT 0,    
v_numItemGroupID NUMERIC(9,0) DEFAULT 0,
v_numItemCode NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null, INOUT SWV_RefCur3 refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_numItemGroupID = 0 then

      IF v_numItemCode > 0 then
		
         select   numItemGroup INTO v_numItemGroupID FROM Item WHERE numItemCode = v_numItemCode;
         open SWV_RefCur for
         select numItemCode,vcItemName,coalesce(Dtl.numWareHouseItemId,0) as numWarehouseItmsID,txtItemDesc,Dtl.numQtyItemsReq,0 as numOppChildItemID,
CASE WHEN coalesce(Dtl.numWareHouseItemId,0) > 0 THEN coalesce(WI.monWListPrice,0) ELSE Item.monListPrice END AS monListPrice,
CAST(CASE WHEN coalesce(Dtl.numWareHouseItemId,0) > 0 THEN coalesce(WI.monWListPrice,0) ELSE Item.monListPrice END AS VARCHAR(30)) || '/' || coalesce(UOM.vcUnitName,'Units') as UnitPrice,
case when charItemType = 'P' then 'Inventory Item' when charItemType = 'S' then 'Service' when charItemType = 'A' then 'Accessory' when charItemType = 'N' then 'Non-Inventory Item' end as charItemType,charItemType as ItemType
,WI.numItemID,numDefaultSelect,(SELECT vcPathForImage FROM ItemImages WHERE numItemCode = v_numItemCode AND numDomainId = v_numDomainID AND bitDefault = true AND bitIsImage = true) AS  vcPathForImage,vcWareHouse,coalesce(Dtl.numListid,0) AS numListId
         from Item
         join ItemGroupsDTL Dtl
         LEFT OUTER JOIN WareHouseItems WI ON WI.numItemID = Dtl.numOppAccAttrID AND WI.numWareHouseItemID = Dtl.numWareHouseItemId
         LEFT OUTER JOIN Warehouses W ON W.numWareHouseID = WI.numWareHouseID
         on numOppAccAttrID = numItemCode
         LEFT JOIN UOM ON UOM.numUOMId = Item.numBaseUnit
         where  Dtl.numItemGroupID = v_numItemGroupID and tintType = 1;
      ELSE
         open SWV_RefCur for
         select numItemGroupID, vcItemGroup, numDomainID,bitCombineAssemblies,
		coalesce(CAST((SELECT  Fld_id FROM CFW_Fld_Master WHERE numlistid = numMapToDropdownFld LIMIT 1) AS VARCHAR(30)),cast(0 as TEXT)) || '-' || CAST(numMapToDropdownFld AS VARCHAR(30)) AS numMapToDropdownFld
		,numProfileItem from ItemGroups
         where numDomainID = v_numDomainID;
      end if;
   ELSEIF v_numItemGroupID > 0
   then

      open SWV_RefCur for
      select numItemGroupID, vcItemGroup, numDomainID,bitCombineAssemblies,
 coalesce(CAST((SELECT  Fld_id FROM CFW_Fld_Master WHERE numlistid = numMapToDropdownFld LIMIT 1) AS VARCHAR(30)),cast(0 as TEXT)) || '-' || CAST(numMapToDropdownFld AS VARCHAR(30)) AS numMapToDropdownFld
 ,numProfileItem from ItemGroups
      where numDomainID = v_numDomainID and numItemGroupID = v_numItemGroupID;    

      open SWV_RefCur2 for
      select numItemCode,vcItemName,coalesce(Dtl.numWareHouseItemId,0) as numWarehouseItmsID,txtItemDesc,Dtl.numQtyItemsReq,0 as numOppChildItemID,
CASE WHEN coalesce(Dtl.numWareHouseItemId,0) > 0 THEN coalesce(WI.monWListPrice,0) ELSE Item.monListPrice END AS monListPrice,
CAST(CASE WHEN coalesce(Dtl.numWareHouseItemId,0) > 0 THEN coalesce(WI.monWListPrice,0) ELSE Item.monListPrice END AS VARCHAR(30)) || '/' || coalesce(UOM.vcUnitName,'Units') as UnitPrice,
case when charItemType = 'P' then 'Inventory Item' when charItemType = 'S' then 'Service' when charItemType = 'A' then 'Accessory' when charItemType = 'N' then 'Non-Inventory Item' end as charItemType,charItemType as ItemType
,WI.numItemID,numDefaultSelect,(SELECT vcPathForImage FROM ItemImages WHERE numItemCode = v_numItemCode AND numDomainId = v_numDomainID AND bitDefault = true AND bitIsImage = true) AS  vcPathForImage,vcWareHouse,coalesce(Dtl.numListid,0) AS numListId
      from Item
      join ItemGroupsDTL Dtl
      LEFT OUTER JOIN WareHouseItems WI ON WI.numItemID = Dtl.numOppAccAttrID AND WI.numWareHouseItemID = Dtl.numWareHouseItemId
      on numOppAccAttrID = numItemCode
      LEFT OUTER JOIN Warehouses W ON W.numWareHouseID = WI.numWareHouseID
      LEFT JOIN UOM ON UOM.numUOMId = Item.numBaseUnit
      where  numItemGroupID = v_numItemGroupID and tintType = 1;
      
	  open SWV_RefCur3 for
      SELECT CFW.Fld_id,Fld_label, CFW.numlistid
      FROM CFW_Fld_Master CFW
      JOIN ItemGroupsDTL
      ON numOppAccAttrID = Fld_id
      WHERE numItemGroupID = v_numItemGroupID and tintType = 2;
   end if;
   RETURN;
END; $$;


