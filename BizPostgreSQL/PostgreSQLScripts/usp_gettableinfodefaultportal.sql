-- Stored procedure definition script usp_GetTableInfoDefaultPortal for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetTableInfoDefaultPortal(                              
v_numUserCntID NUMERIC DEFAULT 0,              
v_numRecordID NUMERIC DEFAULT 0,                          
v_numDomainId NUMERIC DEFAULT 0,                          
v_charCoType CHAR(1) DEFAULT 'a',              
v_pageId NUMERIC DEFAULT NULL,              
v_numRelCntType NUMERIC DEFAULT NULL, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if((select count(*) from PageLayoutDTL DTL where DTL.numUserCntID = v_numUserCntID and DTL.numDomainID = v_numDomainId and DTL.Ctype = v_charCoType and DTL.numRelCntType = v_numRelCntType) <> 0) then

      open SWV_RefCur for
      SELECT HDR.numFieldId,HDR.vcFieldName,'' as vcURL,'' as fld_type,DTL.tintRow,DTL.intCoulmn,DTL.bitCustomField as bitCustomField,HDR.vcDBColumnName,'0' as TabId,'' as tabname from PageLayoutDTL DTL
      join PageLayout HDR on DTL.numFieldId = HDR.numFieldId
      where HDR.Ctype = v_charCoType and numUserCntId = v_numUserCntID  and bitCustomField = false and  numDomainId = v_numDomainId and DTL.numRelCntType = v_numRelCntType order by DTL.intCoulmn,DTL.tintRow;
      if v_pageId = 1 or v_pageId = 4 then

         open SWV_RefCur2 for
         select fld_id as numFieldId,fld_label as vcfieldName ,vcURL,fld_type,
(SELECT DTL.tintRow  FROM PageLayoutDTL DTL WHERE DTL.numUserCntID = v_numUserCntID and  DTL.numDomainID = v_numDomainId
            and DTL.numRelCntType = v_numRelCntType and DTL.Ctype = v_charCoType and bitCustomField = true and DTL.numFieldId = fld_id) as tintRow,
(SELECT DTL.intCoulmn  FROM PageLayoutDTL DTL  WHERE DTL.numUserCntID = v_numUserCntID and  DTL.numDomainID = v_numDomainId
            and DTL.numRelCntType = v_numRelCntType and DTL.Ctype = v_charCoType and bitCustomField = true and DTL.numFieldId = fld_id) as intcoulmn,
case when fld_type = 'SelectBox' then(select vcData from Listdetails where CAST(numListItemID AS VARCHAR) = GetCustFldValue(fld_id,v_pageId::SMALLINT,v_numRecordID))
         else GetCustFldValue(fld_id,v_pageId::SMALLINT,v_numRecordID) end as vcDBColumnName,
 true as bitCustomField,'0' as tabletype ,subgrp as TabId,Grp_Name as tabname from CFW_Fld_Master join CFW_Fld_Dtl
         on Fld_id = numFieldId
         left join CFw_Grp_Master
         on subgrp = CAST(CFw_Grp_Master.Grp_id AS VARCHAR)
         where CFW_Fld_Master.Grp_id = v_pageId and numRelation = v_numRelCntType and CFW_Fld_Master.numDomainID = v_numDomainId  order by subgrp,numOrder;
      end if;
      if v_pageId = 2 or  v_pageId = 3 or v_pageId = 5 or v_pageId = 6 or v_pageId = 7 or v_pageId = 8 then

         open SWV_RefCur2 for
         select fld_id as numFieldId,fld_label as vcfieldName,vcURL,fld_type,
(SELECT DTL.tintRow  FROM PageLayoutDTL DTL WHERE DTL.numUserCntID = v_numUserCntID and  DTL.numDomainID = v_numDomainId
            and DTL.numRelCntType = v_numRelCntType and DTL.Ctype = v_charCoType and bitCustomField = true and DTL.numFieldId = fld_id) as tintRow,
(SELECT DTL.intCoulmn  FROM PageLayoutDTL DTL  WHERE DTL.numUserCntID = v_numUserCntID and  DTL.numDomainID = v_numDomainId
            and DTL.numRelCntType = v_numRelCntType and DTL.Ctype = v_charCoType and bitCustomField = true and DTL.numFieldId = fld_id) as intcoulmn,
case when fld_type = 'SelectBox' then(select vcData from Listdetails where CAST(numListItemID AS VARCHAR) = GetCustFldValue(fld_id,v_pageId::SMALLINT,v_numRecordID))
         else GetCustFldValue(fld_id,v_pageId::SMALLINT,v_numRecordID) end as vcDBColumnName,
 true as bitCustomField,'0' as tabletype,subgrp as TabId,Grp_Name as tabname  from CFW_Fld_Master
         left join CFw_Grp_Master
         on subgrp = CAST(CFw_Grp_Master.Grp_id AS VARCHAR)
         where CFW_Fld_Master.Grp_id = v_pageId and CFW_Fld_Master.numDomainID = v_numDomainId;
      end if;
   else
      open SWV_RefCur for
      SELECT numFieldId,vcFieldName,'' as vcURL,'' as fld_type,vcDBColumnName,tintrow,intColumn as intcoulmn,false as bitCustomField,Ctype,'0' as TabId ,'' as tabname from PageLayout where Ctype = v_charCoType
      order by intcoulmn,tintrow;
      if v_pageId = 1 or v_pageId = 4 then

         open SWV_RefCur2 for
         select fld_id as numFieldId,fld_label as vcfieldName ,vcURL,fld_type,
case when fld_type = 'SelectBox' then(select vcData from Listdetails where CAST(numListItemID AS VARCHAR) = GetCustFldValue(fld_id,v_pageId::SMALLINT,v_numRecordID))
         else GetCustFldValue(fld_id,v_pageId::SMALLINT,v_numRecordID) end as vcDBColumnName,
CAST(0 AS SMALLINT) as tintRow,CAST(0 AS INTEGER) as intcolumn,true as bitCustomField,'1' as tabletype,subgrp as TabId,Grp_Name as tabname from CFW_Fld_Master join CFW_Fld_Dtl
         on Fld_id = numFieldId
         left join CFw_Grp_Master
         on subgrp = CAST(CFw_Grp_Master.Grp_id AS VARCHAR)
         where CFW_Fld_Master.Grp_id = v_pageId and numRelation = v_numRelCntType and CFW_Fld_Master.numDomainID = v_numDomainId   order by subgrp,numOrder;
      end if;
      if v_pageId = 2 or  v_pageId = 3 or v_pageId = 5 or v_pageId = 6 or v_pageId = 7 or v_pageId = 8 then

         open SWV_RefCur2 for
         select fld_id as numFieldId,fld_label as vcfieldName ,vcURL,fld_type,
case when fld_type = 'SelectBox' then(select vcData from Listdetails where CAST(numListItemID AS VARCHAR) = GetCustFldValue(fld_id,v_pageId::SMALLINT,v_numRecordID))
         else GetCustFldValue(fld_id,v_pageId::SMALLINT,v_numRecordID) end as vcDBColumnName,
CAST(0 AS SMALLINT) as tintRow,CAST(0 AS INTEGER) as intcolumn,true as bitCustomField,'1' as tabletype,subgrp as TabId,Grp_Name as tabname from CFW_Fld_Master
         left join CFw_Grp_Master
         on subgrp = CAST(CFw_Grp_Master.Grp_id AS VARCHAR)
         where CFW_Fld_Master.Grp_id = v_pageId and CFW_Fld_Master.numDomainID = v_numDomainId;
      end if;
   end if;
   RETURN;
END; $$;


