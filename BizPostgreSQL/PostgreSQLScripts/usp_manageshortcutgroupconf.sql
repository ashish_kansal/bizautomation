-- Stored procedure definition script USP_ManageShortCutGroupConf for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageShortCutGroupConf(v_numGroupID NUMERIC(9,0) DEFAULT 0,            
v_numDomainId NUMERIC(9,0) DEFAULT 0,            
v_strFav TEXT DEFAULT '',        
v_numTabId NUMERIC(9,0) DEFAULT NULL,
v_bitDefaultTab BOOLEAN DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc  INTEGER;
BEGIN
   DELETE
   FROM ShortCutUsrCnf
   WHERE
   numTabId = v_numTabId
   AND numDomainID = v_numDomainId
   AND numGroupID = v_numGroupID
   AND numLinkid in(select numLinkId from ShortCutGrpConf where numTabId = v_numTabId and numDomainId = v_numDomainId and numGroupId = v_numGroupID);	
  
   DELETE
   FROM ShortCutGrpConf
   WHERE
   numTabId = v_numTabId
   AND numDomainId = v_numDomainId
   AND numGroupId = v_numGroupID;  
-- end  
           
   INSERT INTO ShortCutGrpConf(numGroupId
		,numDomainId
		,numTabId
		,numOrder
		,numLinkId
		,bitInitialPage
		,tintLinkType)
   SELECT
   v_numGroupID
		,v_numDomainId
		,v_numTabId
		,X.numOrder
		,X.numLinkId
		,X.bitInitialPage
		,X.tintLinkType
	FROM
	 XMLTABLE
	(
		'NewDataSet/Table'
		PASSING 
			CAST(v_strFav AS XML)
		COLUMNS
			id FOR ORDINALITY,
			numOrder NUMERIC PATH 'numOrder',
			numLinkId NUMERIC PATH 'numLinkId',
			bitInitialPage BOOLEAN PATH 'bitInitialPage',
			tintLinkType SMALLINT PATH 'tintLinkType'
	) X;            

   IF v_bitDefaultTab = true then
	
      UPDATE GroupTabDetails SET bitInitialTab = false WHERE numGroupID = v_numGroupID;
      UPDATE GroupTabDetails SET bitInitialTab = true WHERE numGroupID = v_numGroupID AND numTabId = v_numTabId;
      UPDATE ShortCutGrpConf SET bitDefaultTab = false WHERE numDomainId = v_numDomainId AND numGroupId = v_numGroupID;
      UPDATE ShortCutGrpConf SET bitDefaultTab = true WHERE numTabId = v_numTabId AND numDomainId = v_numDomainId AND numGroupId = v_numGroupID;
   end if;
   RETURN;
END; $$;


