CREATE OR REPLACE FUNCTION USP_GetDocumentFilesLink(v_numRecID NUMERIC(18,0) DEFAULT 0,    
	v_numDomainID NUMERIC(18,0) DEFAULT 0,
	v_calledFrom CHAR DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_calledFrom = 'P' then
      open SWV_RefCur for
      SELECT
      numGenericDocID
			,VcFileName
			,cUrlType
			,coalesce(vcfiletype,'') AS vcFileType
			,(CASE WHEN coalesce(VcDocName,'') = '' THEN '-' ELSE VcDocName END) AS VcDocName
			,coalesce(numFormFieldGroupId,0) AS numFormFieldGroupId
      FROM
      GenericDocuments
      WHERE
      numDomainId = v_numDomainID
      AND numRecID = v_numRecID
      AND coalesce(v_numRecID,0) > 0
      AND coalesce(VcFileName,'') <> '';
   ELSEIF v_calledFrom = 'T'
   then
      open SWV_RefCur for
      SELECT
      numGenericDocID
			,VcFileName
			,cUrlType
			,coalesce(vcfiletype,'') AS vcFileType
			,(CASE WHEN coalesce(VcDocName,'') = '' THEN '-' ELSE VcDocName END) AS VcDocName
			,coalesce(numFormFieldGroupId,0) AS numFormFieldGroupId
      FROM
      GenericDocuments
      WHERE
      numDomainId = v_numDomainID
      AND numRecID = v_numRecID
      AND coalesce(v_numRecID,0) > 0
      AND vcDocumentSection = 'T'
      AND coalesce(VcFileName,'') <> '';
   ELSEIF v_calledFrom = 'O'
   then
      open SWV_RefCur for
      SELECT
      numGenericDocID
			,VcFileName
			,cUrlType
			,coalesce(vcfiletype,'') AS vcFileType
			,(CASE WHEN coalesce(VcDocName,'') = '' THEN '-' ELSE VcDocName END) AS VcDocName
			,coalesce(numFormFieldGroupId,0) AS numFormFieldGroupId
      FROM
      GenericDocuments
      WHERE
      numDomainId = v_numDomainID
      AND numRecID = v_numRecID
      AND coalesce(v_numRecID,0) > 0
      AND vcDocumentSection = 'O'
      AND coalesce(VcFileName,'') <> '';
   ELSEIF v_calledFrom = 'E'
   then
      open SWV_RefCur for
      SELECT
      numGenericDocID
			,VcFileName
			,cUrlType
			,coalesce(vcfiletype,'') AS vcFileType
			,(CASE WHEN coalesce(VcDocName,'') = '' THEN '-' ELSE VcDocName END) AS VcDocName
			,coalesce(numFormFieldGroupId,0) AS numFormFieldGroupId
      FROM
      GenericDocuments
      WHERE
      numDomainId = v_numDomainID
      AND numRecID = v_numRecID
      AND cUrlType = 'L';
   end if;
   RETURN;
END; $$;


