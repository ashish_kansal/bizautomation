-- Stored procedure definition script USP_GetTimeAndExpenseDate for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetTimeAndExpenseDate(v_numUserCntID NUMERIC(9,0) DEFAULT 0,
    v_numDomainID NUMERIC(9,0) DEFAULT 0,
    v_strDate VARCHAR(10) DEFAULT '',
    v_numCategoryHDRID NUMERIC(9,0) DEFAULT 0,
    v_ClientTimeZoneOffset INTEGER DEFAULT NULL,
    v_tintTEType SMALLINT DEFAULT 0,
    v_numCaseId NUMERIC(9,0) DEFAULT 0,
    v_numProId NUMERIC(9,0) DEFAULT 0,
    v_numOppId NUMERIC(9,0) DEFAULT 0,
    v_numStageId NUMERIC(9,0) DEFAULT 0,
    v_numCategory SMALLINT DEFAULT 0,
	v_FromDate DATE DEFAULT null,
	v_ToDate DATE DEFAULT null,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_TotalTime  DECIMAL(10,2);
   v_TotalExpense  DECIMAL(10,2);
   v_TimeBudget  DECIMAL(10,2);
   v_ExpenseBudget  DECIMAL(10,2);
   v_bitTimeBudget  BOOLEAN;
   v_bitExpenseBudget  BOOLEAN;
   v_sql  VARCHAR(8000);
BEGIN
   IF (v_numCategoryHDRID = 0
   AND v_tintTEType = 0) then
        
      open SWV_RefCur for SELECT  CASE WHEN tintTEType = 0 THEN 'T & E'
      WHEN tintTEType = 1
      THEN CASE WHEN numCategory = 1
         THEN CASE WHEN(SELECT  tintopptype
               FROM    OpportunityMaster
               WHERE   numOppId = TE.numOppId) = 1 THEN 'Sales Time'
            ELSE 'Purch Time'
            END
         ELSE CASE WHEN(SELECT  tintopptype
               FROM    OpportunityMaster
               WHERE   numOppId = TE.numOppId) = 1 THEN 'Sales Exp'
            ELSE 'Purch Exp'
            END
         END
      WHEN tintTEType = 2
      THEN CASE WHEN numCategory = 1 THEN 'Project Time'
         ELSE 'Project Exp'
         END
      WHEN tintTEType = 3
      THEN CASE WHEN numCategory = 1 THEN 'Case Time'
         ELSE 'Case Exp'
         END
      WHEN tintTEType = 4
      THEN 'Non-Paid Leave'
      WHEN tintTEType = 5
      THEN 'Expense'
      END AS chrFrom,
                    numCategoryHDRID,
                    tintTEType,
                    CASE WHEN numCategory = 1 AND numtype = 1 THEN 'Billable Time'
      WHEN numCategory = 1 AND numtype = 2 THEN 'Non-Billable Time'
      WHEN numCategory = 2 AND coalesce(bitReimburse,false) = true AND numtype = 2  THEN 'Reimbursable Expense'
      WHEN numCategory = 2 AND coalesce(bitReimburse,false) = true AND numtype = 6 THEN 'Billable + Reimbursable Expense'
      WHEN numCategory = 2 AND coalesce(bitReimburse,false) = false AND numtype = 1 THEN 'Billable Expense'
      END  AS Category,
                    CASE WHEN numtype = 1 THEN 'Billable'
      WHEN numtype = 2 THEN 'Non-Billable'
      WHEN numtype = 5 THEN 'Reimbursable Expense'
      WHEN numtype = 6 THEN 'Billable'
                         --WHEN numType = 3 THEN 'Paid Leave'
                         --WHEN numType = 4 THEN 'Non-Paid'
      END AS Type,
                    CASE WHEN numCategory = 1
      THEN dtFromDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)
      ELSE dtFromDate
      END AS dtFromDate,
                    CASE WHEN numCategory = 1
      THEN dtToDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)
      ELSE dtToDate
      END AS dtToDate,
                    bitFromFullDay,
                    bitToFullDay,
                    CAST(monAmount AS DECIMAL(10,2)) AS monAmount,
                    TE.numOppId,
                    OM.vcpOppName,
					CA.vcAccountName,
                    TE.numDivisionID,
                    txtDesc,
                    numUserCntID,
                    coalesce(ACI.vcFirstName,'') || ' ' || coalesce(ACI.vcLastname,'') AS vcEmployee,
                    TE.numDomainID,
                    TE.numcontractId,
                    numCaseid,
                    TE.numProId,
                    PM.vcProjectID,
                    numOppBizDocsId,
                    numStageID AS numStageID,
                    numCategory,
                    numtype,
                    CASE WHEN numCategory = 1
      THEN CAST(CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DOUBLE PRECISION)/60 AS VARCHAR(30))
      WHEN numCategory = 2
      THEN CAST(CAST(monAmount AS DECIMAL(10,2)) AS VARCHAR(30))
      WHEN numCategory = 3
      THEN(CASE WHEN bitFromFullDay = false THEN 'HDL'
         WHEN bitFromFullDay = true THEN 'FDL'
         END)
      WHEN numCategory = 4
      THEN(CASE WHEN bitFromFullDay = false THEN 'HDL'
         WHEN bitFromFullDay = true THEN 'FDL'
         END)
      END AS Detail
                    ,CAST(CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DOUBLE PRECISION)/60 AS VARCHAR(30)) AS TimeValue
                    ,CASE WHEN numCategory = 1 THEN CAST((CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DOUBLE PRECISION)/60)*coalesce(TE.monAmount,1) AS DECIMAL(10,2))
      WHEN numCategory = 2 THEN CAST(coalesce(TE.monAmount,1) AS DECIMAL(10,2))
      ELSE 0
      END AS ExpenseValue
                    ,(SELECT cast(coalesce(numItemCode,0) as TEXT) FROM Item  WHERE numItemCode IN(SELECT numItemCode  FROM OpportunityItems WHERE numoppitemtCode = TE.numOppItemID)) AS numItemCode
					,(SELECT cast(coalesce(numClassID,0) as TEXT) FROM OpportunityItems WHERE numoppitemtCode = TE.numOppItemID) AS numClassID
					,(SELECT cast(coalesce(numClassID,0) as TEXT) FROM OpportunityItems WHERE numoppitemtCode = TE.numOppItemID) AS numClassID
					,(SELECT cast(coalesce(vcItemDesc,'') as TEXT) FROM OpportunityItems WHERE numoppitemtCode = TE.numOppItemID) AS vcItemDesc
					,CASE WHEN coalesce(TE.numApprovalComplete,0) = 0 THEN 'Approved' WHEN TE.numApprovalComplete = -1 THEN 'Declined' ELSE 'Pending Approval' END as ApprovalStatus
					, coalesce(ACIC.vcFirstName,'') || ' ' || coalesce(ACIC.vcLastname,'') || ',' || CAST((TE.dtTCreatedOn+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)) AS VARCHAR(30)) as CreatedDate
					,CASE WHEN I.vcItemName = '' THEN '' ELSE '(' || I.vcItemName || ')' END as vcItemName
					,Case WHEN numCategory = 1 AND numtype = 1 AND OM.numOppId > 0
      THEN((coalesce(CAST((EXTRACT(DAY FROM TE.dtToDate -TE.dtFromDate)*60*24+EXTRACT(HOUR FROM TE.dtToDate -TE.dtFromDate)*60+EXTRACT(MINUTE FROM TE.dtToDate -TE.dtFromDate)) AS DOUBLE PRECISION)/CAST(60 AS DOUBLE PRECISION),
         0))*TE.monAmount)
      WHEN  OM.numOppId > 0
      THEN TE.monAmount
      ELSE cast('0' as DOUBLE PRECISION)
      END AS ClientCharge,
					Case WHEN numCategory = 1 AND (numtype = 1 or numtype = 2)
      THEN((coalesce(CAST((EXTRACT(DAY FROM TE.dtToDate -TE.dtFromDate)*60*24+EXTRACT(HOUR FROM TE.dtToDate -TE.dtFromDate)*60+EXTRACT(MINUTE FROM TE.dtToDate -TE.dtFromDate)) AS DOUBLE PRECISION)/CAST(60 AS DOUBLE PRECISION),
         0))*(select coalesce(monHourlyRate,0) from UserMaster where numUserDetailId = TE.numUserCntID
            and UserMaster.numDomainID = v_numDomainID))
      WHEN numCategory = 2
      THEN TE.monAmount
      ELSE  0
      END AS EmployeeCharge
      FROM    timeandexpense TE
      LEFT JOIN OpportunityMaster OM ON OM.numOppId = TE.numOppId AND TE.numDomainID = OM.numDomainId
      LEFT JOIN Item as I ON TE.numServiceItemID = I.numItemCode
      LEFT JOIN ProjectsMaster PM ON PM.numProId = TE.numProId AND TE.numDomainID = PM.numdomainId
      LEFT JOIN Chart_Of_Accounts CA ON CA.numAccountId = TE.numExpId AND TE.numDomainID = CA.numDomainId
      LEFT JOIN AdditionalContactsInformation ACI ON ACI.numContactId = TE.numUserCntID
      LEFT JOIN AdditionalContactsInformation ACIC ON ACIC.numContactId = TE.numTCreatedBy
      WHERE   (numUserCntID = v_numUserCntID OR v_numUserCntID = 0)
      AND TE.numDomainID = v_numDomainID
      AND (TE.numtype = v_numCategory OR v_numCategory = 0)
      AND                                   
--  (@strDate between dateadd(minute,-@ClientTimeZoneOffset,dtFromDate) and dateadd(minute,-@ClientTimeZoneOffset,dtToDate)   
-- or( day(dateadd(minute,-@ClientTimeZoneOffset,dtFromDate))=day(@strDate)   
--and month(dateadd(minute,-@ClientTimeZoneOffset,dtFromDate))=month(@strDate)   
--and year(dateadd(minute,-@ClientTimeZoneOffset,dtFromDate))=year(@strDate)))       
                    ( 
--						( @strDate >= CASE WHEN numCategory = 1
--                                         THEN DATEADD(minute,
--                                                      -@ClientTimeZoneOffset,
--                                                      dtFromDate)
--                                         ELSE dtFromDate
--                                    END
--                        AND @strDate <= CASE WHEN numCategory = 1
--                                             THEN DATEADD(minute,
--                                                          -@ClientTimeZoneOffset,
--                                                          dttoDate)
--                                             ELSE dttoDate
--                                        END
--                      )
--                      OR 
                       CASE WHEN numCategory = 1
      THEN dtFromDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)
      ELSE dtFromDate END
      BETWEEN v_FromDate AND v_ToDate);
   ELSE
      v_TotalTime := 0;
      v_TotalExpense := 0;
      v_TimeBudget := 0;
      v_ExpenseBudget := 0;
      v_bitTimeBudget := false;
      v_bitExpenseBudget := false;
      select   coalesce(CAST(CAST(SUM(monAmount*CAST(CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DECIMAL(10,2))/60 AS DECIMAL(10,2))) AS DECIMAL(10,2)) AS DECIMAL(10,2)),0) INTO v_TotalTime from  timeandexpense where numProId = v_numProId and numStageId = v_numStageId AND numCategory = 1 and tintTEType = 2
      AND numtype = 1 GROUP BY numtype;
      v_TotalExpense := CAST(coalesce(fn_GetExpenseDtlsbyProStgID(v_numProId,v_numStageId,1::SMALLINT),
      cast(0 as TEXT)) AS DECIMAL(10,2));
      select   coalesce(monTimeBudget,0), coalesce(monExpenseBudget,0), coalesce(bitTimeBudget,false), coalesce(bitExpenseBudget,false) INTO v_TimeBudget,v_ExpenseBudget,v_bitTimeBudget,v_bitExpenseBudget from StagePercentageDetails where numProjectid = v_numProId and numStageDetailsId = v_numStageId;
      if v_bitTimeBudget = true then
         v_TotalTime := v_TimeBudget -v_TotalTime;
      end if;
      if v_bitExpenseBudget = true then
         v_TotalExpense := v_TotalExpense -v_TotalExpense;
      end if;
      v_sql := CONCAT('          
 select           
 COALESCE(numCategoryHDRID,0)as numCategoryHDRID,                                  
  numCategory,                                  
  numType,                                  
  Case when numCategory =1 then dtFromDate + make_interval(mins => ',-v_ClientTimeZoneOffset,') else  dtFromDate end as dtFromDate,                                  
  Case when numCategory =1 then dtToDate + make_interval(mins => ',-v_ClientTimeZoneOffset,') else dtToDate end as dtToDate ,                                   
  bitFromFullDay,                                  
  bitToFullDay,                                  
  CAST(monAmount AS decimal(10,2)) as monAmount,                                  
  numOppId,
   Case When numOppId>0 then GetOppName(T.numOppId)  
  When numExpId>0 then (select vcAccountName from Chart_Of_Accounts where T.numExpId=numAccountid and numDomainId=T.numDomainId LIMIT 1) else '''' end as  vcPOppName,                     
  T.numDivisionID,                            
  D.numCompanyID,                                  
  txtDesc,                                  
numContractId,                      
numCaseid,                      
numProid,
Case When numProid>0 then (select vcProjectID from ProjectsMaster where numProId=T.numProId and numDomainId=T.numDomainId) 
  When numExpId>0 then (select CAST(numAccountId AS VARCHAR) from Chart_Of_Accounts where T.numExpId=numAccountid and numDomainId=T.numDomainId LIMIT 1)
else '''' end as  vcProjectID,                     
  numUserCntID,                                  
  T.numDomainID  ,                    
numOppBizDocsId  ,          
numStageId ,
Case when numStageId>0 and  numProid>0 then fn_GetProjectStageName(T.numProid,T.numStageId) else '''' end as vcProjectStageName,
bitReimburse,
Case when numCategory =2 then (SELECT COALESCE(numChartAcntId,0) FROM General_Journal_Header WHERE numCategoryHDRID=',v_numCategoryHDRID,')
else 0 END as numExpenseAccountID
,',v_TotalTime ,' TotalTime,',v_TotalExpense,' TotalExpense
,',CAST(COALESCE(v_bitTimeBudget,false) AS VARCHAR),' bitTimeBudget,',CAST(COALESCE(v_bitExpenseBudget,false) AS VARCHAR),' bitExpenseBudget
,(SELECT COALESCE(numItemCode,0) FROM item  WHERE numItemCode IN (SELECT numItemCode FROM OpportunityItems WHERE numoppitemtCode = numOppItemID )) AS numItemCode
,(SELECT COALESCE(numClassID,0) FROM OpportunityItems WHERE numoppitemtCode = numOppItemID ) AS numClassID
,(SELECT COALESCE(vcItemDesc,'''') FROM OpportunityItems WHERE numoppitemtCode = T.numOppItemID ) AS vcItemDesc
,CASE WHEN COALESCE(T.numApprovalComplete,0)=0 THEN ''Approved and Added'' WHEN T.numApprovalComplete=-1 THEN ''Declined'' ELSE ''Waiting For Approve'' END as ApprovalStatus
 from                                   
TimeAndExpense T                            
Left join DivisionMaster D                            
on  D.numDivisionID=T.numDivisionID                                   
 where  T.numDomainID=',v_numDomainID);
      IF v_numCategoryHDRID <> 0
      AND v_tintTEType <> 0 then
         v_sql := coalesce(v_sql,'') || ' and numCategoryHDRID='
         || SUBSTR(CAST(v_numCategoryHDRID AS VARCHAR(20)),1,20);
      ELSE
         IF v_tintTEType = 0 then
            v_sql := coalesce(v_sql,'')
            || ' and tintTEType = 0 and numCategoryHDRID='
            || SUBSTR(CAST(v_numCategoryHDRID AS VARCHAR(20)),1,20)
            || '          
   and numUserCntID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(20)),1,20);
         end if;
         IF v_tintTEType = 1 then
            v_sql := coalesce(v_sql,'')
            || ' and tintTEType = 1  and numOppId = '
            || SUBSTR(CAST(v_numOppId AS VARCHAR(20)),1,20) || '           
   and numStageId = ' || SUBSTR(CAST(v_numStageId AS VARCHAR(20)),1,20)
            || ' and numCategory = '
            || SUBSTR(CAST(v_numCategory AS VARCHAR(20)),1,20);
         end if;
         IF v_tintTEType = 2 then
            v_sql := coalesce(v_sql,'')
            || ' and tintTEType = 2  and numProid = '
            || SUBSTR(CAST(v_numProId AS VARCHAR(20)),1,20) || '           
   and numStageId = ' || SUBSTR(CAST(v_numStageId AS VARCHAR(20)),1,20)
            || ' and numCategory = '
            || SUBSTR(CAST(v_numCategory AS VARCHAR(20)),1,20);
         end if;
         IF v_tintTEType = 3 then
            v_sql := coalesce(v_sql,'')
            || ' and tintTEType = 3  and numCaseid = '
            || SUBSTR(CAST(v_numCaseId AS VARCHAR(20)),1,20) || '            
   and numStageId = ' || SUBSTR(CAST(v_numStageId AS VARCHAR(20)),1,20)
            || ' and numCategory = '
            || SUBSTR(CAST(v_numCategory AS VARCHAR(20)),1,20);
         end if;
      end if;
      RAISE NOTICE '%',v_sql;
      OPEN SWV_RefCur FOR EXECUTE v_sql;
   end if;
END; $$;












