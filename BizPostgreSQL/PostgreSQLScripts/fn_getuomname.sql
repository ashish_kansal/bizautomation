-- Function definition script fn_GetUOMName for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetUOMName(v_numUOMId NUMERIC)
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcUnitName  VARCHAR(100);
BEGIN
   select   coalesce(vcUnitName,'') INTO v_vcUnitName from UOM where numUOMId = v_numUOMId;

   return coalesce(v_vcUnitName,'-');
END; $$;

