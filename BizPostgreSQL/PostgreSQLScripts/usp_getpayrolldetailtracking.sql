-- Stored procedure definition script USP_GetPayrollDetailTracking for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetPayrollDetailTracking(v_numDomainId NUMERIC(9,0),
    v_numUserCntID NUMERIC(9,0),
    v_numPayrollHeaderID NUMERIC(9,0),
    v_numPayrollDetailID NUMERIC(9,0),
    v_tintMode SMALLINT,
    v_ClientTimeZoneOffset INTEGER, INOUT SWV_RefCur refcursor )
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_dtStartDate  TIMESTAMP;
   v_dtEndDate  TIMESTAMP;
BEGIN
   select   dtFromDate, dtToDate INTO v_dtStartDate,v_dtEndDate FROM PayrollHeader WHERE numPayrollHeaderID = v_numPayrollHeaderID AND numDomainID = v_numDomainId;

   IF v_tintMode = 1 then --RegularHrs

      open SWV_RefCur for
      Select TE.numCategoryHDRID,coalesce(numPayrollDetailID,0) AS numPayrollDetailID
  ,CASE WHEN coalesce(numPayrollDetailID,0) > 0 THEN true ELSE false END AS bitPaid
  ,dtFromDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS dtFromDate,
  dtToDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS dtToDate,
  coalesce((Cast((EXTRACT(DAY FROM dttodate -dtfromdate)*60*24+EXTRACT(HOUR FROM dttodate -dtfromdate)*60+EXTRACT(MINUTE FROM dttodate -dtfromdate)) as DOUBLE PRECISION)/Cast(60 as DOUBLE PRECISION)),0) as Hrs,0 AS monamount
      from timeandexpense TE
      LEFT JOIN PayrollTracking PT ON  coalesce(TE.numCategoryHDRID,0) = coalesce(PT.numCategoryHDRID,0)
      AND numPayrollHeaderID = v_numPayrollHeaderID AND numPayrollDetailID = v_numPayrollDetailID
      Where (numType = 1 Or numType = 2)  And numCategory = 1                 
--  And ((dateadd(minute,-@ClientTimeZoneOffset,dtFromDate) between @dtStartDate And 	@dtEndDate )
--	Or (dateadd(minute,-@ClientTimeZoneOffset,dtToDate) between @dtStartDate And @dtEndDate)) 
      And
      TE.numUserCntID = v_numUserCntID  And TE.numDomainID = v_numDomainId
      AND TE.numCategoryHDRID NOT IN(SELECT numCategoryHDRID FROM PayrollTracking WHERE numDomainID = v_numDomainId AND numPayrollHeaderID != v_numPayrollHeaderID AND numCategoryHDRID IS NOT NULL);
--ELSE IF @tintMode=2 --OvertimeHrs
--BEGIN
--	PRINT @tintMode
--END
   ELSEIF v_tintMode = 3
   then --PaidLeaveHrs

      open SWV_RefCur for
      Select TE.numCategoryHDRID,coalesce(numPayrollDetailID,0) AS numPayrollDetailID
  ,CASE WHEN coalesce(numPayrollDetailID,0) > 0 THEN true ELSE false END AS bitPaid
  ,dtFromDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS dtFromDate,
  dtToDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS dtToDate,
  coalesce(CASE WHEN dtfromdate = dttodate THEN 24 -(CASE WHEN bitfromfullday = false THEN 12 ELSE 0 END)
      ELSE(DATE_PART('day',dttodate:: timestamp -dtfromdate:: timestamp)+1)*24
         -(CASE WHEN bitfromfullday = 0 THEN 12 ELSE 0 END)
         -(CASE WHEN bittofullday = 0 THEN 12 ELSE 0 END) END,0) as Hrs,0 AS monamount
      from timeandexpense TE
      LEFT JOIN PayrollTracking PT ON  coalesce(TE.numCategoryHDRID,0) = coalesce(PT.numCategoryHDRID,0)
      AND numPayrollHeaderID = v_numPayrollHeaderID AND numPayrollDetailID = v_numPayrollDetailID
      Where numType = 3 And numCategory = 3               
--  And ((dateadd(minute,-@ClientTimeZoneOffset,dtFromDate) between @dtStartDate And 	@dtEndDate )
--	Or (dateadd(minute,-@ClientTimeZoneOffset,dtToDate) between @dtStartDate And @dtEndDate)) 
      And
      TE.numUserCntID = v_numUserCntID  And TE.numDomainID = v_numDomainId
      AND TE.numCategoryHDRID NOT IN(SELECT numCategoryHDRID FROM PayrollTracking WHERE numDomainID = v_numDomainId AND numPayrollHeaderID != v_numPayrollHeaderID AND numCategoryHDRID IS NOT NULL);
   ELSEIF v_tintMode = 4
   then --PaidInvoice

	
  -- AND ((dateadd(minute,-@ClientTimeZoneOffset,oppBiz.dtCreatedDate) between @dtStartDate And @dtEndDate))
      open SWV_RefCur for
      SELECT * FROM(SELECT BC.numComissionID,Opp.vcpOppName,BC.numComissionAmount,
	coalesce((SELECT SUM(coalesce(monCommissionAmt,0))
            FROM PayrollTracking WHERE coalesce(BC.numComissionID,0) = coalesce(numComissionID,0) AND numPayrollHeaderID != v_numPayrollHeaderID),0) AS monPaidAmount
	,coalesce(PT1.monCommissionAmt,0) AS monUsedAmount ,CASE WHEN coalesce(PT1.numPayrollDetailID,0) > 0 THEN true ELSE false END AS bitPaid
         FROM OpportunityMaster Opp
         INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numoppid = Opp.numOppId
         INNER JOIN BizDocComission BC on BC.numOppBizDocId = oppBiz.numOppBizDocsId
         LEFT JOIN PayrollTracking PT1 ON  coalesce(BC.numComissionID,0) = coalesce(PT1.numComissionID,0)
         AND PT1.numPayrollHeaderID = v_numPayrollHeaderID AND PT1.numPayrollDetailID = v_numPayrollDetailID
         where Opp.numDomainId = v_numDomainId and
         BC.numUserCntID = v_numUserCntID AND BC.bitCommisionPaid = false
         AND oppBiz.bitAuthoritativeBizDocs = 1 AND coalesce(oppBiz.monAmountPaid,0) >= oppBiz.monDealAmount) T
      WHERE(numComissionAmount -monPaidAmount) > 0;
   ELSEIF v_tintMode = 5
   then --UNPaidInvoice


   --AND ((dateadd(minute,-@ClientTimeZoneOffset,oppBiz.dtCreatedDate) between @dtStartDate And @dtEndDate))
      open SWV_RefCur for
      SELECT * FROM(SELECT BC.numComissionID,Opp.vcpOppName,BC.numComissionAmount,
	coalesce((SELECT SUM(coalesce(monCommissionAmt,0))
            FROM PayrollTracking WHERE coalesce(BC.numComissionID,0) = coalesce(numComissionID,0) AND numPayrollHeaderID != v_numPayrollHeaderID),0) AS monPaidAmount
	,coalesce(PT1.monCommissionAmt,0) AS monUsedAmount ,CASE WHEN coalesce(PT1.numPayrollDetailID,0) > 0 THEN true ELSE false END AS bitPaid
         FROM OpportunityMaster Opp
         INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numoppid = Opp.numOppId
         INNER JOIN BizDocComission BC on BC.numOppBizDocId = oppBiz.numOppBizDocsId
         LEFT JOIN PayrollTracking PT1 ON  coalesce(BC.numComissionID,0) = coalesce(PT1.numComissionID,0)
         AND PT1.numPayrollHeaderID = v_numPayrollHeaderID AND PT1.numPayrollDetailID = v_numPayrollDetailID
         where Opp.numDomainId = v_numDomainId and
         BC.numUserCntID = v_numUserCntID AND BC.bitCommisionPaid = false
         AND oppBiz.bitAuthoritativeBizDocs = 1 AND coalesce(oppBiz.monAmountPaid,0) < oppBiz.monDealAmount) T
      WHERE(numComissionAmount -monPaidAmount) > 0;
   ELSEIF v_tintMode = 6
   then --Expense

      open SWV_RefCur for
      SELECT TE.numCategoryHDRID,coalesce(numPayrollDetailID,0) AS numPayrollDetailID
  ,CASE WHEN coalesce(numPayrollDetailID,0) > 0 THEN true ELSE false END AS bitPaid
  ,dtFromDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS dtFromDate,
  dtToDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS dtToDate,
  coalesce(monamount,0) AS monamount,0 AS Hrs
      from timeandexpense TE
      LEFT JOIN PayrollTracking PT ON  coalesce(TE.numCategoryHDRID,0) = coalesce(PT.numCategoryHDRID,0)
      AND numPayrollHeaderID = v_numPayrollHeaderID AND numPayrollDetailID = v_numPayrollDetailID
      Where numCategory = 2 And numType in(1,2)               
--  And ((dateadd(minute,-@ClientTimeZoneOffset,dtFromDate) between @dtStartDate And 	@dtEndDate )
--	Or (dateadd(minute,-@ClientTimeZoneOffset,dtToDate) between @dtStartDate And @dtEndDate)) 
      And
      TE.numUserCntID = v_numUserCntID  And TE.numDomainID = v_numDomainId
      AND TE.numCategoryHDRID NOT IN(SELECT numCategoryHDRID FROM PayrollTracking WHERE numDomainID = v_numDomainId AND numPayrollHeaderID != v_numPayrollHeaderID AND numCategoryHDRID IS NOT NULL);
   ELSEIF v_tintMode = 7
   then --Reimburse

      open SWV_RefCur for
      SELECT TE.numCategoryHDRID,coalesce(numPayrollDetailID,0) AS numPayrollDetailID
  ,CASE WHEN coalesce(numPayrollDetailID,0) > 0 THEN true ELSE false END AS bitPaid
  ,dtFromDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS dtFromDate,
  dtToDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS dtToDate,
  coalesce(monamount,0) AS monamount,0 AS Hrs
      from timeandexpense TE
      LEFT JOIN PayrollTracking PT ON  coalesce(TE.numCategoryHDRID,0) = coalesce(PT.numCategoryHDRID,0)
      AND numPayrollHeaderID = v_numPayrollHeaderID AND numPayrollDetailID = v_numPayrollDetailID
      Where bitreimburse = true AND numcategory = 2              
--  And ((dateadd(minute,-@ClientTimeZoneOffset,dtFromDate) between @dtStartDate And 	@dtEndDate )
--	Or (dateadd(minute,-@ClientTimeZoneOffset,dtToDate) between @dtStartDate And @dtEndDate)) 
      And
      TE.numUserCntID = v_numUserCntID  And TE.numDomainID = v_numDomainId
      AND TE.numCategoryHDRID NOT IN(SELECT numCategoryHDRID FROM PayrollTracking WHERE numDomainID = v_numDomainId AND numPayrollHeaderID != v_numPayrollHeaderID AND numCategoryHDRID IS NOT NULL);
   ELSEIF v_tintMode = 8
   then --Project Commission

      open SWV_RefCur for
      SELECT * FROM(SELECT BC.numComissionID,PM.vcProjectID,PM.monTotalIncome,PM.monTotalExpense,PM.monTotalGrossProfit,
	BC.monProTotalIncome,BC.monProTotalExpense,BC.monProTotalGrossProfit,BC.numComissionAmount,
	coalesce((SELECT SUM(coalesce(monCommissionAmt,0))
            FROM PayrollTracking WHERE coalesce(BC.numComissionID,0) = coalesce(numComissionID,0) AND numPayrollHeaderID != v_numPayrollHeaderID),0) AS monPaidAmount
	,coalesce(PT1.monCommissionAmt,0) AS monUsedAmount ,CASE WHEN coalesce(PT1.numPayrollDetailID,0) > 0 THEN true ELSE false END AS bitPaid,
	FormatedDateFromDate(BC.dtProCompletionDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)::VARCHAR(50),PM.numdomainId) AS dtCompletionDate
         FROM ProjectsMaster PM
         INNER JOIN BizDocComission BC on BC.numProId = PM.numProId
         LEFT JOIN PayrollTracking PT1 ON  coalesce(BC.numComissionID,0) = coalesce(PT1.numComissionID,0)
         AND PT1.numPayrollHeaderID = v_numPayrollHeaderID AND PT1.numPayrollDetailID = v_numPayrollDetailID
         where PM.numdomainId = v_numDomainId AND --PM.numProjectStatus=27492 and
         BC.numUserCntID = v_numUserCntID AND BC.bitCommisionPaid = false) T
      WHERE(numComissionAmount -monPaidAmount) > 0;
   end if;
   RETURN;
END; $$;


