-- Stored procedure definition script USP_ReportListMaster_SalesOppWonLostPreBuildReport for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ReportListMaster_SalesOppWonLostPreBuildReport(v_numDomainID NUMERIC(18,0)
	,v_ClientTimeZoneOffset INTEGER
	,v_vcTimeLine VARCHAR(50)
	,v_vcGroupBy VARCHAR(50)
	,v_vcTeritorry TEXT
	,v_vcFilterBy SMALLINT  -- 1:Assign TO, 2: Record Owner, 3: Teams (Based on Assigned To) 
	,v_vcFilterValue TEXT
	,v_dtFromDate DATE
	,v_dtToDate DATE,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintFiscalStartMonth  SMALLINT;
   v_CurrentYearStartDate  DATE; 
   v_CurrentYearEndDate  DATE;

   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER;
BEGIN
   select   coalesce(tintFiscalStartMonth,1) INTO v_tintFiscalStartMonth FROM Domain WHERE numDomainId = v_numDomainID;
   v_CurrentYearStartDate := make_date(EXTRACT(YEAR FROM TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval))::INTEGER,v_tintFiscalStartMonth,1);
   IF v_CurrentYearStartDate > TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) then
	
      v_CurrentYearStartDate := v_CurrentYearStartDate+INTERVAL '-1 year';
   end if;	
   v_CurrentYearEndDate := v_CurrentYearStartDate+INTERVAL '1 year'+INTERVAL '-1 day';

   DROP TABLE IF EXISTS tt_TABLEQUATER CASCADE;
   CREATE TEMPORARY TABLE tt_TABLEQUATER
   (
      ID INTEGER,
      QuaterStartDate DATE,
      QuaterEndDate DATE
   );

   INSERT INTO tt_TABLEQUATER(ID
		,QuaterStartDate
		,QuaterEndDate)
	VALUES(1,make_date(EXTRACT(YEAR FROM v_CurrentYearStartDate)::INTEGER,v_tintFiscalStartMonth,1),make_date(EXTRACT(YEAR FROM v_CurrentYearStartDate)::INTEGER,v_tintFiscalStartMonth,1)+INTERVAL '3 month'+INTERVAL '-1 day'),(2,make_date(EXTRACT(YEAR FROM v_CurrentYearStartDate)::INTEGER,v_tintFiscalStartMonth,1)+INTERVAL '3 month',make_date(EXTRACT(YEAR FROM v_CurrentYearStartDate)::INTEGER,v_tintFiscalStartMonth,1)+INTERVAL '6 month'+INTERVAL '-1 day'),
   (3,make_date(EXTRACT(YEAR FROM v_CurrentYearStartDate)::INTEGER,v_tintFiscalStartMonth,1)+INTERVAL '6 month',make_date(EXTRACT(YEAR FROM v_CurrentYearStartDate)::INTEGER,v_tintFiscalStartMonth,1)+INTERVAL '9 month'+INTERVAL '-1 day'),(4,make_date(EXTRACT(YEAR FROM v_CurrentYearStartDate)::INTEGER,v_tintFiscalStartMonth,1)+INTERVAL '9 month',make_date(EXTRACT(YEAR FROM v_CurrentYearStartDate)::INTEGER,v_tintFiscalStartMonth,1)+INTERVAL '12 month'+INTERVAL '-1 day');

	
   IF v_vcTimeLine = 'CurYear' then
	
      v_dtFromDate := v_CurrentYearStartDate;
      v_dtToDate := v_CurrentYearEndDate;
   ELSEIF v_vcTimeLine = 'PreYear'
   then
	
      v_dtFromDate :=  v_CurrentYearStartDate+INTERVAL '-1 year';
      v_dtToDate := v_dtFromDate+INTERVAL '1 year'+INTERVAL '-1 day';
   ELSEIF v_vcTimeLine = 'Pre2Year'
   then
	
      v_dtFromDate := v_CurrentYearStartDate+INTERVAL '-2 year';
      v_dtToDate := v_dtFromDate+INTERVAL '2 year'+INTERVAL '-1 day';
   ELSEIF v_vcTimeLine = 'Ago2Year'
   then
	
      v_dtFromDate :=  NULL;
      v_dtToDate := v_CurrentYearStartDate+INTERVAL '-2 year'+INTERVAL '-1 day';
   ELSEIF v_vcTimeLine = 'CurPreYear'
   then
	
      v_dtFromDate := v_CurrentYearStartDate+INTERVAL '-1 year';
      v_dtToDate := v_dtFromDate+INTERVAL '1 year'+INTERVAL '-1 day';
   ELSEIF v_vcTimeLine = 'CurPre2Year'
   then
	
      v_dtFromDate := v_CurrentYearStartDate+INTERVAL '-2 year';
      v_dtToDate := make_date(EXTRACT(YEAR FROM TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval))::INTEGER,v_tintFiscalStartMonth,1)+INTERVAL '1 year'+INTERVAL '-1 day';
   ELSEIF v_vcTimeLine = 'CuQur'
   then
	
      select   QuaterStartDate, QuaterEndDate INTO v_dtFromDate,v_dtToDate FROM
      tt_TABLEQUATER WHERE
      TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) BETWEEN QuaterStartDate AND QuaterEndDate;
   ELSEIF v_vcTimeLine = 'PreQur'
   then
	
      select   QuaterStartDate+INTERVAL '-3 month', QuaterEndDate+INTERVAL '-3 month' INTO v_dtFromDate,v_dtToDate FROM
      tt_TABLEQUATER WHERE
      TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) BETWEEN QuaterStartDate AND QuaterEndDate;
   ELSEIF v_vcTimeLine = 'CurPreQur'
   then
	
      select   QuaterStartDate+INTERVAL '-3 month', QuaterEndDate INTO v_dtFromDate,v_dtToDate FROM
      tt_TABLEQUATER WHERE
      TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) BETWEEN QuaterStartDate AND QuaterEndDate;
   ELSEIF v_vcTimeLine = 'ThisMonth'
   then
	
      v_dtFromDate := get_month_start(TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval));
      v_dtToDate := get_month_end(TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval));
   ELSEIF v_vcTimeLine = 'LastMonth'
   then
	
      v_dtFromDate := get_month_start(TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval))+INTERVAL '-1 month';
      v_dtToDate := get_month_end(TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval))+INTERVAL '-1 month';
   ELSEIF v_vcTimeLine = 'CurPreMonth'
   then
	
      v_dtFromDate := get_month_start(TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval))+INTERVAL '-1 month';
      v_dtToDate := get_month_end(TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval));
   ELSEIF v_vcTimeLine = 'LastWeek'
   then
	
      v_dtFromDate := get_week_start(TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval))+INTERVAL '-7 day';
      v_dtToDate := get_week_end(TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval))+INTERVAL '-7 day';
   ELSEIF v_vcTimeLine = 'ThisWeek'
   then
	
      v_dtFromDate := get_week_start(TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval));
      v_dtToDate := get_week_end(TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval));
   ELSEIF v_vcTimeLine = 'Yesterday'
   then
	
      v_dtFromDate := TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+INTERVAL '-1 day';
      v_dtToDate := TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+INTERVAL '-1 day';
   ELSEIF v_vcTimeLine = 'Today'
   then
	
      v_dtFromDate := TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval);
      v_dtToDate := TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval);
   ELSEIF v_vcTimeLine = 'Last7Day'
   then
	
      v_dtFromDate := TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+INTERVAL '-7 day';
      v_dtToDate := TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval);
   ELSEIF v_vcTimeLine = 'Last30Day'
   then
	
      v_dtFromDate := TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+INTERVAL '-30 day';
      v_dtToDate := TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval);
   ELSEIF v_vcTimeLine = 'Last60Day'
   then
	
      v_dtFromDate := TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+INTERVAL '-60 day';
      v_dtToDate := TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval);
   ELSEIF v_vcTimeLine = 'Last90Day'
   then
	
      v_dtFromDate := TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+INTERVAL '-90 day';
      v_dtToDate := TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval);
   ELSEIF v_vcTimeLine = 'Last120Day'
   then
	
      v_dtFromDate := TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)+INTERVAL '-120 day';
      v_dtToDate := TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval);
   end if;

   BEGIN
      CREATE TEMP SEQUENCE tt_TempResult_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPRESULT CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPRESULT
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      GroupValue VARCHAR(100),
      StartDate DATE,
      EndDate DATE,
      SalesOppAmount DECIMAL(20,5),
      ClosedWonAmount DECIMAL(20,5),
      CloseWonPercent DECIMAL(20,2),
      CloseLostAmount DECIMAL(20,5),
      CloseLostPercent DECIMAL(20,2)
   );


   IF v_vcGroupBy = 'Quater' then
	
      INSERT INTO tt_TEMPRESULT(GroupValue
			,StartDate
			,EndDate) with recursive CTEYear
      AS(SELECT
      CASE WHEN v_dtFromDate < make_date(EXTRACT(YEAR FROM v_dtFromDate)::INTEGER,v_tintFiscalStartMonth,1) THEN EXTRACT(YEAR FROM v_dtFromDate+INTERVAL '-1 year') ELSE EXTRACT(YEAR FROM v_dtFromDate) END  AS yr
      UNION ALL
      SELECT
      yr+1
      FROM
      CTEYear
      WHERE
      yr < EXTRACT(YEAR FROM v_dtToDate)) SELECT
      CONCAT(yr,'-',Quater)
			,QuaterStart
			,QuaterEnd
      FROM
      CTEYear
      CROSS JOIN LATERAL(select * from(SELECT
            'Q1' AS Quater,
				make_date(yr,v_tintFiscalStartMonth,1) AS QuaterStart
				,make_date(yr,v_tintFiscalStartMonth,1)+INTERVAL '3 month'+INTERVAL '-1 day' AS QuaterEnd
            UNION
            SELECT
            'Q2' AS Quater,
				make_date(yr,v_tintFiscalStartMonth,1)+INTERVAL '3 month' AS QuaterStart
				,make_date(yr,v_tintFiscalStartMonth,1)+INTERVAL '6 month'+INTERVAL '-1 day' AS QuaterEnd
            UNION
            SELECT
            'Q3' AS Quater,
				make_date(yr,v_tintFiscalStartMonth,1)+INTERVAL '6 month' AS QuaterStart
				,make_date(yr,v_tintFiscalStartMonth,1)+INTERVAL '9 month'+INTERVAL '-1 day' AS QuaterEnd
            UNION
            SELECT
            'Q4' AS Quater,
				make_date(yr,v_tintFiscalStartMonth,1)+INTERVAL '9 month' AS QuaterStart
				,make_date(yr,v_tintFiscalStartMonth,1)+INTERVAL '12 month'+INTERVAL '-1 day' AS QuaterEnd) Test) AS TabAl
      WHERE
      QuaterStart BETWEEN v_dtFromDate AND v_dtToDate OR
      QuaterEnd BETWEEN v_dtFromDate AND v_dtToDate OR
			(QuaterStart <= v_dtFromDate AND QuaterEnd >= v_dtToDate);
   ELSEIF v_vcGroupBy = 'Year'
   then
	
      INSERT INTO tt_TEMPRESULT(GroupValue
			,StartDate
			,EndDate) with recursive CTEYear
      AS(SELECT
      EXTRACT(YEAR FROM v_dtFromDate) AS yr
      UNION ALL
      SELECT
      yr+1
      FROM
      CTEYear
      WHERE
      yr < EXTRACT(YEAR FROM v_dtToDate)) SELECT
      CAST(yr AS VARCHAR(100))
			,make_date(yr,v_tintFiscalStartMonth,1)
			,make_date(yr,v_tintFiscalStartMonth,1)+INTERVAL '1 year'+INTERVAL '-1 day'
      FROM
      CTEYear;
   ELSE
      INSERT INTO tt_TEMPRESULT(GroupValue
			,StartDate
			,EndDate) with recursive CTEMonth(monthDate,StartDate,EndDate)
      AS(SELECT
      v_dtFromDate::TIMESTAMP AS monthDate
				,get_month_start(v_dtFromDate::TIMESTAMP) AS StartDate
				,get_month_end(v_dtFromDate::TIMESTAMP) AS EndDate
      UNION ALL
      SELECT
      monthDate+INTERVAL '1 month' AS monthDate
				,get_month_start(monthDate+INTERVAL '1 month') AS StartDate
				,get_month_end(monthDate+INTERVAL '1 month') AS EndDate
      FROM
      CTEMonth
      WHERE
      monthDate+INTERVAL '1 month' <= v_dtToDate) SELECT
      CONCAT(TO_CHAR(monthDate,'FMMonth'),'-',TO_CHAR(monthDate,'YYYY'))
			,StartDate
			,EndDate
      FROM
      CTEMonth;
   end if;

   DROP TABLE IF EXISTS tt_TEMPOPPDATA CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPOPPDATA
   (
      TotalRows NUMERIC(18,0),
      CreatedDate TIMESTAMP,
      WonDate DATE,
      LostDate DATE,
      monDealAmount DECIMAL(20,5)
   );

   INSERT INTO tt_TEMPOPPDATA(TotalRows
		,CreatedDate
		,WonDate
		,LostDate
		,monDealAmount)
   SELECT
   COUNT(numOppId) OVER() AS TotalRows
		,OpportunityMaster.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS CreatedDate
		,(CASE WHEN coalesce(tintoppstatus,0) = 1 AND OpportunityMaster.bintOppToOrder IS NOT NULL THEN OpportunityMaster.bintOppToOrder+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) ELSE NULL END) AS WonDate
		,(CASE WHEN coalesce(tintoppstatus,0) = 2 AND OpportunityMaster.dtDealLost IS NOT NULL THEN OpportunityMaster.dtDealLost+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) ELSE NULL END) AS LostDate
		,monDealAmount
   FROM
   OpportunityMaster
   INNER JOIN
   DivisionMaster
   ON
   OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
   WHERE
   OpportunityMaster.numDomainId = v_numDomainID
   AND coalesce(tintopptype,0) = 1
   AND (OpportunityMaster.bintOppToOrder IS NOT NULL OR OpportunityMaster.dtDealLost IS NOT NULL)
   AND 1 =(CASE WHEN LENGTH(coalesce(v_vcTeritorry,'')) > 0 THEN(CASE WHEN DivisionMaster.numTerID IN(SELECT ID FROM SplitIDs(v_vcFilterValue,',')) THEN 1 ELSE 0 END) ELSE 1 END)
   AND 1 =(CASE
   WHEN LENGTH(coalesce(v_vcFilterValue,cast(0 as TEXT))) > 0 AND v_vcFilterBy IN(1,2,3)
   THEN(CASE v_vcFilterBy
      WHEN 1  -- Assign To
      THEN(CASE WHEN OpportunityMaster.numassignedto IN(SELECT ID FROM SplitIDs(v_vcFilterValue,',')) THEN 1 ELSE 0 END)
      WHEN 2  -- Record Owner
      THEN(CASE WHEN OpportunityMaster.numrecowner IN(SELECT ID FROM SplitIDs(v_vcFilterValue,',')) THEN 1 ELSE 0 END)
      WHEN 3  -- Teams (Based on Assigned To)
      THEN(CASE WHEN OpportunityMaster.numassignedto IN(SELECT numUserCntID FROM UserTeams WHERE numDomainID = v_numDomainID AND numTeam IN(SELECT ID FROM SplitIDs(v_vcFilterValue,','))) THEN 1 ELSE 0 END)
      END)
   ELSE 1
   END)
   AND (CAST(OpportunityMaster.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS DATE) BETWEEN v_dtFromDate AND v_dtToDate
   OR CAST(OpportunityMaster.bintOppToOrder+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS DATE) BETWEEN v_dtFromDate AND v_dtToDate
   OR CAST(OpportunityMaster.dtDealLost+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS DATE) BETWEEN v_dtFromDate AND v_dtToDate);

   select   COUNT(*) INTO v_iCount FROM tt_TEMPRESULT;

   WHILE v_i <= v_iCount LOOP
      UPDATE
      tt_TEMPRESULT
      SET
      SalesOppAmount = coalesce((SELECT SUM(monDealAmount) FROM tt_TEMPOPPDATA WHERE (CreatedDate BETWEEN StartDate AND EndDate OR LostDate BETWEEN StartDate AND EndDate OR WonDate BETWEEN StartDate AND EndDate)),0),ClosedWonAmount = coalesce((SELECT SUM(monDealAmount) FROM tt_TEMPOPPDATA WHERE WonDate BETWEEN StartDate AND EndDate),0),CloseWonPercent =(CASE WHEN coalesce((SELECT COUNT(*) FROM tt_TEMPOPPDATA WHERE (CreatedDate BETWEEN StartDate AND EndDate OR LostDate BETWEEN StartDate AND EndDate OR WonDate BETWEEN StartDate AND EndDate)),0) > 0 THEN CAST((coalesce((SELECT COUNT(*) FROM tt_TEMPOPPDATA WHERE WonDate BETWEEN StartDate AND EndDate),0)*100) AS decimal)/coalesce((SELECT COUNT(*) FROM tt_TEMPOPPDATA WHERE (CreatedDate BETWEEN StartDate AND EndDate OR LostDate BETWEEN StartDate AND EndDate OR WonDate BETWEEN StartDate AND EndDate)),0) ELSE 0 END),
      CloseLostAmount = coalesce((SELECT SUM(monDealAmount) FROM tt_TEMPOPPDATA WHERE LostDate BETWEEN StartDate AND EndDate),0),CloseLostPercent =(CASE WHEN coalesce((SELECT COUNT(*) FROM tt_TEMPOPPDATA WHERE (CreatedDate BETWEEN StartDate AND EndDate OR LostDate BETWEEN StartDate AND EndDate OR WonDate BETWEEN StartDate AND EndDate)),0) > 0 THEN CAST((coalesce((SELECT COUNT(*) FROM tt_TEMPOPPDATA WHERE LostDate BETWEEN StartDate AND EndDate),0)*100) AS decimal)/coalesce((SELECT COUNT(*) FROM tt_TEMPOPPDATA WHERE (CreatedDate BETWEEN StartDate AND EndDate OR LostDate BETWEEN StartDate AND EndDate OR WonDate BETWEEN StartDate AND EndDate)),0) ELSE 0 END)
      WHERE
      ID = v_i;
      v_i := v_i::bigint+1;
   END LOOP;

   open SWV_RefCur for SELECT * FROM tt_TEMPRESULT;
END; $$;












