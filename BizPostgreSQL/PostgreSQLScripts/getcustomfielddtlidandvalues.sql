-- Function definition script GetCustomFieldDTLIDAndValues for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetCustomFieldDTLIDAndValues(v_PageID INTEGER
	,v_RecID NUMERIC(9,0))
RETURNS TABLE 
(
   FldDTLID NUMERIC(9,0),
   Fld_ID NUMERIC(9,0),
   Fld_Value VARCHAR(1000)
) LANGUAGE plpgsql
   AS $$
BEGIN
	DROP TABLE IF EXISTS tt_GETCUSTOMFIELDDTLIDANDVALUES_RESULTS CASCADE;
	CREATE TEMPORARY TABLE IF NOT EXISTS  tt_GETCUSTOMFIELDDTLIDANDVALUES_RESULTS
	(
		FldDTLID NUMERIC(9,0),
		Fld_ID NUMERIC(9,0),
		Fld_Value VARCHAR(1000)
	);

	IF v_PageID = 1 OR v_PageID = 12 OR v_PageID = 13 OR v_PageID = 14 then
		INSERT INTO tt_GETCUSTOMFIELDDTLIDANDVALUES_RESULTS
		(
			FldDTLID
			,Fld_ID
			,Fld_Value
		)
		SELECT
			CFW_FLD_Values.FldDTLID
			,CFW_FLD_Values.Fld_ID
			,CFW_FLD_Values.Fld_Value
		FROM
			CFW_FLD_Values
		WHERE
			RecId = v_RecID;
	ELSEIF v_PageID = 4 then
		INSERT INTO tt_GETCUSTOMFIELDDTLIDANDVALUES_RESULTS
		(
			FldDTLID
			,Fld_ID
			,Fld_Value
		)
		SELECT
			CFW_FLD_Values_Cont.FldDTLID
			,CFW_FLD_Values_Cont.Fld_ID
			,CFW_FLD_Values_Cont.Fld_Value
		FROM
			CFW_FLD_Values_Cont
		WHERE
			RecId = v_RecID;
	ELSEIF v_PageID = 3 then
		INSERT INTO tt_GETCUSTOMFIELDDTLIDANDVALUES_RESULTS
		(
			FldDTLID
			,Fld_ID
			,Fld_Value
		)
		SELECT
			CFW_FLD_Values_Case.FldDTLID
			,CFW_FLD_Values_Case.Fld_ID
			,CFW_FLD_Values_Case.Fld_Value
		FROM
			CFW_FLD_Values_Case
		WHERE
			RecId = v_RecID;
	ELSEIF v_PageID = 5 then
		INSERT INTO tt_GETCUSTOMFIELDDTLIDANDVALUES_RESULTS
		(
			FldDTLID
			,Fld_ID
			,Fld_Value
		)
		SELECT
			CFW_FLD_Values_Item.FldDTLID
			,CFW_FLD_Values_Item.Fld_ID
			,CFW_FLD_Values_Item.Fld_Value
		FROM
			CFW_FLD_Values_Item
		WHERE
			RecId = v_RecID;
	ELSEIF v_PageID = 2 or v_PageID = 6 then	
		INSERT INTO tt_GETCUSTOMFIELDDTLIDANDVALUES_RESULTS
		(
			FldDTLID
			,Fld_ID
			,Fld_Value
		)
		SELECT
			CFW_Fld_Values_Opp.FldDTLID
			,CFW_Fld_Values_Opp.Fld_ID
			,CFW_Fld_Values_Opp.Fld_Value
		FROM
			CFW_Fld_Values_Opp
		WHERE
			RecId = v_RecID;
	ELSEIF v_PageID = 7 or v_PageID = 8 then
		INSERT INTO tt_GETCUSTOMFIELDDTLIDANDVALUES_RESULTS
		(
			FldDTLID
			,Fld_ID
			,Fld_Value
		)
		SELECT
			CFW_Fld_Values_Product.FldDTLID
			,CFW_Fld_Values_Product.Fld_ID
			,CFW_Fld_Values_Product.Fld_Value
		FROM
			CFW_Fld_Values_Product
		WHERE
			RecId = v_RecID;
	ELSEIF v_PageID = 9 then
		INSERT INTO tt_GETCUSTOMFIELDDTLIDANDVALUES_RESULTS
		(
			FldDTLID
			,Fld_ID
			,Fld_Value
		)
		SELECT
			CFW_Fld_Values_Serialized_Items.FldDTLID
			,CFW_Fld_Values_Serialized_Items.Fld_ID
			,CFW_Fld_Values_Serialized_Items.Fld_Value
		FROM
			CFW_Fld_Values_Serialized_Items
		WHERE
			RecId = v_RecID;
	ELSEIF v_PageID = 11 then
		INSERT INTO tt_GETCUSTOMFIELDDTLIDANDVALUES_RESULTS
		(
			FldDTLID
			,Fld_ID
			,Fld_Value
		)
		SELECT
			CFW_Fld_Values_Pro.FldDTLID
			,CFW_Fld_Values_Pro.Fld_ID
			,CFW_Fld_Values_Pro.Fld_Value
		FROM
			CFW_Fld_Values_Pro
		WHERE
			RecId = v_RecID;
	ELSEIF v_PageID = 18 then
		INSERT INTO tt_GETCUSTOMFIELDDTLIDANDVALUES_RESULTS
		(
			FldDTLID
			,Fld_ID
			,Fld_Value
		)
		SELECT
			CFW_Fld_Values_OppItems.FldDTLID
			,CFW_Fld_Values_OppItems.Fld_ID
			,CFW_Fld_Values_OppItems.Fld_Value
		FROM
			CFW_Fld_Values_OppItems
		WHERE
			RecId = v_RecID;
	end if;    

	RETURN QUERY (SELECT * FROM tt_GETCUSTOMFIELDDTLIDANDVALUES_RESULTS);
END; $$;

