-- Stored procedure definition script USP_GetOppWareHouseItemAttributes for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetOppWareHouseItemAttributes(v_numRecID NUMERIC(9,0) DEFAULT 0,
v_bitSerialize BOOLEAN DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF coalesce(v_numRecID,0) > 0 then
	
      open SWV_RefCur for
      SELECT fn_GetAttributes(v_numRecID,v_bitSerialize);
   ELSE
      open SWV_RefCur for
      SELECT '';
   end if;
   RETURN;
END; $$;


