-- Stored procedure definition script USP_GetEmbeddedCost for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetEmbeddedCost(v_numEmbeddedCostID NUMERIC DEFAULT 0,
    v_numOppBizDocID NUMERIC DEFAULT NULL,
    v_numCostCatID NUMERIC DEFAULT 0,
    v_numDomainID NUMERIC DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT * FROM EmbeddedCost WHERE numOppBizDocID = v_numOppBizDocID and numCostCatID = v_numCostCatID AND numDomainID = v_numDomainID) then
    
      open SWV_RefCur for
      SELECT  numEmbeddedCostID AS numDefaultEmbeddedCostID,
				numEmbeddedCostID,
                numOppBizDocID,
                numCostCatID,
                numCostCenterID,
                numAccountID,
                numDivisionID,
                fn_GetComapnyName(numDivisionID) AS vcCompanyName,
                numPaymentMethod,
                numDomainID,
                vcMemo,
                monCost,
                monActualCost,
                FormatedDateFromDate(dtDueDate,numDomainID) AS dtDueDate,
                CASE WHEN coalesce(EmbeddedCost.numBizDocsPaymentDetId,0) > 0 THEN(SELECT  bitIntegrated FROM OpportunityBizDocsPaymentDetails WHERE numBizDocsPaymentDetId = EmbeddedCost.numBizDocsPaymentDetId LIMIT 1) ELSE false END AS IsDisabled
      FROM    EmbeddedCost
      WHERE   numOppBizDocID = v_numOppBizDocID
      AND numCostCatID = v_numCostCatID
      AND numDomainID = v_numDomainID;
   ELSE
      IF v_numCostCatID > 0 then
		
         open SWV_RefCur for
         SELECT
         0 AS numEmbeddedCostID,
					numEmbeddedCostID AS numDefaultEmbeddedCostID,
					numCostCatID,
					numCostCenterID,
					numAccountID,
					numDivisionID,
					fn_GetComapnyName(numDivisionID) AS vcCompanyName,
					numPaymentMethod,
					numDomainID,
					'' AS vcMemo,
					0 AS monCost,
					0 AS monActualCost,
					FormatedDateFromDate(TIMEZONE('UTC',now()),numDomainID) AS dtDueDate,
					0 AS IsDisabled
         FROM    EmbeddedCostDefaults
         WHERE
         numCostCatID = v_numCostCatID
         AND numDomainID = v_numDomainID;
      ELSE
         open SWV_RefCur for
         SELECT
         EC.numEmbeddedCostID,
					EC.numCostCatID,
					EC.numCostCenterID,
					EC.numAccountID,
					CA.vcAccountName,
					EC.numDivisionID,
					fn_GetComapnyName(EC.numDivisionID) AS vcCompanyName,
					EC.numPaymentMethod,
					EC.numDomainID,
					EC.vcMemo AS vcMemo,
					EC.monCost,
					EC.monActualCost,
					coalesce(EC.numBizDocsPaymentDetId,0) AS numBizDocsPaymentDetId,
					FormatedDateFromDate(TIMEZONE('UTC',now()),EC.numDomainID) AS dtDueDate,
					dtDueDate AS dtDueDate1,
					EC.numBizDocsPaymentDetId,
					CASE WHEN coalesce(EC.numBizDocsPaymentDetId,0) > 0 THEN(SELECT  bitIntegrated FROM OpportunityBizDocsPaymentDetails WHERE numBizDocsPaymentDetId = EC.numBizDocsPaymentDetId LIMIT 1) ELSE false END AS IsDisabled
         FROM    EmbeddedCost EC LEFT OUTER JOIN Chart_Of_Accounts CA ON CA.numAccountId = EC.numAccountID
         WHERE
         numOppBizDocID = v_numOppBizDocID
         AND EC.numDomainID = v_numDomainID;
      end if;
   end if;
   RETURN;
END; $$;



