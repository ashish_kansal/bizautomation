-- FUNCTION: public.usp_getemailfromuserid(numeric, refcursor)

-- DROP FUNCTION public.usp_getemailfromuserid(numeric, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getemailfromuserid(
	v_numuserid numeric,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
-- This function was converted on Sat Apr 10 16:42:42 2021 using Ispirer SQLWays 9.0 Build 6532 64bit Licensed to BizAutomation - Sandeep Patel - India - Ispirer MnMTK 2020 MSSQLServer to PostgreSQL Database Migration Professional Project License (50 GB, 750 Tables, 426500 LOC, 3 Extension Requests, 1 Month, 20210508).
   open SWV_RefCur for Select vcEmail From AdditionalContactsInformation where numContactId =CAST((Select cast(numUserDetailId as VARCHAR(50)) from UserMaster where numUserId = v_numUserId) As Numeric);
END;
$BODY$;

ALTER FUNCTION public.usp_getemailfromuserid(numeric, refcursor)
    OWNER TO postgres;
