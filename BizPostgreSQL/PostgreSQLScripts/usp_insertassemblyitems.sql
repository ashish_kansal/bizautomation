-- Stored procedure definition script USP_InsertAssemblyItems for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_InsertAssemblyItems(v_numItemKitID NUMERIC(18,0) DEFAULT 0,
v_numChildItemID NUMERIC(18,0) DEFAULT 0,
v_numQtyItemsReq DOUBLE PRECISION DEFAULT 0,
v_numUOMId NUMERIC(18,0) DEFAULT 0,
v_numWareHouseId NUMERIC(18,0) DEFAULT 0,
v_vcItemDesc TEXT DEFAULT NULL,
v_sintOrder INTEGER DEFAULT 0,
INOUT v_numItemDetailID NUMERIC(18,0)  DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF(SELECT COUNT(*) FROM ItemDetails WHERE numItemKitID = v_numItemKitID AND numChildItemID = v_numChildItemID) = 0 then

      insert into ItemDetails(numItemKitID,numChildItemID,numQtyItemsReq,numUOMID,vcItemDesc,sintOrder)
	values(v_numItemKitID,v_numChildItemID,v_numQtyItemsReq,v_numUOMId,v_vcItemDesc,v_sintOrder) RETURNING numItemDetailID INTO v_numItemDetailID;
	
      RAISE NOTICE '%',v_numItemDetailID;
   end if;
   RETURN;
END; $$;


