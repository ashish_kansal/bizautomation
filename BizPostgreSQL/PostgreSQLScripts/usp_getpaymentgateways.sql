-- Stored procedure definition script USP_GetPaymentGateways for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetPaymentGateways(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(intPaymentGateWay as VARCHAR(255)), cast(vcGateWayName as VARCHAR(255))
   from PaymentGateway;
END; $$;












