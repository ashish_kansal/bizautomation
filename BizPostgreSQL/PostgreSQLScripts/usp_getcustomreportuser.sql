-- Stored procedure definition script USP_getCustomReportUser for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_getCustomReportUser(v_numUserCntID NUMERIC(9,0),      
v_numDomainId NUMERIC(9,0)  ,    
v_numGroupId NUMERIC(9,0)  DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if  v_numGroupId = 0 then
	
      open SWV_RefCur for
      select vcReportName || ' - ' || vcReportDescription as vcReportName,numCustomReportId as numReportId from CustomReport
      where numCreatedBy = v_numUserCntID and numdomainId = v_numDomainId;
   else
      open SWV_RefCur for
      select vcReportName || ' - ' || vcReportDescription as vcReportName,numCustomReportId as numReportId from CustomReport cr
      join  DashboardAllowedReports DAR on cr.numCustomReportId = DAR.numReportId
      where  numdomainId = v_numDomainId and numgrpId = v_numGroupId;
   end if;
   RETURN;
END; $$;


