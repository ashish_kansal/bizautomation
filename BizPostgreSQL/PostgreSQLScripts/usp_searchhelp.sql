-- Stored procedure definition script USP_SearchHelp for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SearchHelp(v_strsearch VARCHAR(500), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strqry  VARCHAR(2000);
BEGIN
      
   OPEN SWV_RefCur FOR select * from HelpMaster where helpDescription ilike '%' || coalesce(v_strsearch,'') || '%' or helpMetatag ilike '%' || coalesce(v_strsearch,'') || '%';
   RETURN;
END; $$;


