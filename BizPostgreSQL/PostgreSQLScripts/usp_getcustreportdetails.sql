-- Stored procedure definition script usp_getCustReportDetails for PostgreSQL
CREATE OR REPLACE FUNCTION usp_getCustReportDetails(v_ReportId NUMERIC DEFAULT 0,              
v_numDomainId NUMERIC DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select numCustomReportId,vcReportName,vcReportDescription,textQuery,numCreatedBy,numdomainId,numModifiedBy,numModuleId,
numGroupId , bitFilterRelAnd , varstdFieldId,custFilter,FromDate,
ToDate,bintRows  ,bitGridType,varGrpfld,varGrpOrd,varGrpflt,textQueryGrp ,FilterType,AdvFilterStr,
OrderbyFld,Orderf,MttId,MttValue,CompFilter1,CompFilter2,CompFilterOpr,cast(coalesce(bitDrillDown,false) as BOOLEAN) as bitDrillDown,cast(coalesce(bitSortRecCount,false) as BOOLEAN) as bitSortRecCount
   from CustomReport where numCustomReportId = v_ReportId;
END; $$;












