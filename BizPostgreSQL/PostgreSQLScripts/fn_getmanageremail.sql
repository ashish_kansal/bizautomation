-- Function definition script fn_GetManagerEmail for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetManagerEmail(v_byteMode SMALLINT,
v_numUserID NUMERIC(9,0) DEFAULT 0,
v_numContactID NUMERIC(9,0) DEFAULT 0)
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcEmail  VARCHAR(100);
BEGIN
   v_vcEmail := '';
   if v_byteMode = 0 then --- If Userid Is Available

      select   coalesce(vcEmail,'') INTO v_vcEmail from AdditionalContactsInformation where
      numContactId =(select numManagerID from AdditionalContactsInformation Addc
         join UserMaster U
         on U.numUserDetailId = Addc.numContactId
         where numUserID = v_numUserID);
   end if;
   if v_byteMode = 1 then  --- If Conatct ID is Available

      select   coalesce(vcEmail,'') INTO v_vcEmail from AdditionalContactsInformation where
      numContactId =(select numManagerID from AdditionalContactsInformation where numContactId = v_numContactID);
   end if;

   return v_vcEmail;
END; $$;

