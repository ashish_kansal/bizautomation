CREATE OR REPLACE FUNCTION usp_GetTop5SalesRep(              
--              
v_numDomainID NUMERIC,              
 v_dtDateFrom TIMESTAMP,              
 v_dtDateTo TIMESTAMP,              
 v_numTerID NUMERIC DEFAULT 0,              
 v_tintRights SMALLINT DEFAULT 3,    --ByDefault show all records.              
 v_intType NUMERIC DEFAULT 0,              
 v_numUserCntId NUMERIC DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintRights = 3 then  --ALL RECORDS (As top 5 Sales people cannot be divided as per the Owner records)              
 
      IF v_intType = 0 then
   
         open SWV_RefCur for
         select  A.vcFirstName || ' ' || A.vcLastname as Name,sum(monPAmount) as Amount  from OpportunityMaster OM
         join AdditionalContactsInformation A on A.numContactId = OM.numrecowner
         where OM.tintoppstatus = 1
         AND OM.numDomainId = v_numDomainID
         AND OM.bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo
         group by A.vcFirstName,A.vcLastname order by Amount desc LIMIT 5;
      ELSE
         open SWV_RefCur for
         select  A.vcFirstName || ' ' || A.vcLastname as Name,sum(monPAmount) as Amount  from OpportunityMaster OM
         join AdditionalContactsInformation A on A.numContactId = OM.numrecowner
         where OM.tintoppstatus = 1
         AND OM.numDomainId = v_numDomainID
         AND OM.bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND OM.numrecowner = v_numUserCntId
         AND A.numTeam is not null
         AND A.numTeam in(select F.numTeam from ForReportsByTeam F
            where F.numUserCntID = v_numUserCntId and F.numDomainID = v_numDomainID and F.tintType = v_intType)
         group by A.vcFirstName,A.vcLastname  order by Amount desc LIMIT 5;
      end if;
   end if;              
   IF v_tintRights = 2 then --TERRITORY RECORDS              
 
      IF v_intType = 0 then
   
         open SWV_RefCur for
         select  A.vcFirstName || ' ' || A.vcLastname as Name,sum(monPAmount) as Amount  from OpportunityMaster OM
         join AdditionalContactsInformation A on A.numContactId = OM.numrecowner
         JOIN DivisionMaster DM
         ON DM.numDivisionID = OM.numDivisionId
         WHERE OM.tintoppstatus = 1
         AND OM.numDomainId = v_numDomainID
         AND OM.bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND DM.numTerID in(Select numTerritoryID FROM UserTerritory WHERE numUserCntID = v_numUserCntId)
         group by A.vcFirstName,A.vcLastname order by Amount desc LIMIT 5;
      ELSE
         open SWV_RefCur for
         select  A.vcFirstName || ' ' || A.vcLastname as Name,sum(monPAmount) as Amount  from OpportunityMaster OM
         join AdditionalContactsInformation A on A.numContactId = OM.numrecowner
         JOIN DivisionMaster DM
         ON DM.numDivisionID = OM.numDivisionId
         WHERE OM.tintoppstatus = 1
         AND OM.numDomainId = v_numDomainID
         AND OM.bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND DM.numTerID in(Select numTerritoryID FROM UserTerritory WHERE numUserCntID = v_numUserCntId)
         AND A.numTeam is not null
         AND A.numTeam in(select F.numTeam from ForReportsByTeam F
            where F.numUserCntID = v_numUserCntId and F.numDomainID = v_numDomainID and F.tintType = v_intType)
         group by A.vcFirstName,A.vcLastname order by Amount desc LIMIT 5;
      end if;
   end if;              
            
            
   IF v_tintRights = 1 then  --ALL RECORDS (As top 5 Sales people cannot be divided as per the Owner records)              
 
      IF v_intType = 0 then
   
         open SWV_RefCur for
         select  A.vcFirstName || ' ' || A.vcLastname as Name,sum(monPAmount) as Amount   from OpportunityMaster OM
         join AdditionalContactsInformation A on A.numContactId = OM.numrecowner
         where OM.tintoppstatus = 1
         AND OM.numDomainId = v_numDomainID
         and OM.numrecowner = v_numUserCntId
         AND OM.bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo
         group by A.vcFirstName,A.vcLastname order by Amount desc LIMIT 5;
      ELSE
         open SWV_RefCur for
         select  A.vcFirstName || ' ' || A.vcLastname as Name,sum(monPAmount) as Amount  from OpportunityMaster OM
         join AdditionalContactsInformation A on A.numContactId = OM.numrecowner
         where OM.tintoppstatus = 1
         AND OM.numDomainId = v_numDomainID
         and OM.numrecowner = v_numUserCntId
         AND OM.bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND A.numTeam is not null
         AND A.numTeam in(select F.numTeam from ForReportsByTeam F
            where F.numUserCntID = v_numUserCntId and F.numDomainID = v_numDomainID and F.tintType = v_intType)
         group by A.vcFirstName,A.vcLastname order by Amount desc LIMIT 5;
      end if;
   end if;
   RETURN;
END; $$;


