CREATE OR REPLACE FUNCTION USP_UpdateBizDocsShippingCost
(
	v_numOppBizDocsId NUMERIC(18,0),
    v_numShippingReportId NUMERIC(18,0)
)
RETURNS VOID LANGUAGE plpgsql
AS $$
   DECLARE
   v_listStr  VARCHAR(500);
   v_numShipVia  NUMERIC;
BEGIN
	SELECT 
		string_agg(vcTrackingNumber,',') INTO v_listStr 
	FROM 
		ShippingBox 
	WHERE 
		numShippingReportID = v_numShippingReportId;
   
	RAISE NOTICE '%',v_listStr;

	SELECT numShippingCompany INTO v_numShipVia FROM ShippingReport WHERE numShippingReportID = v_numShippingReportId;


	UPDATE 
		OpportunityBizDocs
	SET 
		vcTrackingNo = v_listStr
		,numShipVia = v_numShipVia
	WHERE 
		numOppBizDocsId = v_numOppBizDocsId;

   RETURN;
END; $$; 
