-- Function definition script ConvertToTZOffset for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION ConvertToTZOffset(v_current TIMESTAMP)
RETURNS VARCHAR(34) LANGUAGE plpgsql
 
 -- if the input has full precision we can emit up to 34 characters of data
 
   AS $$
   DECLARE
   v_withTZ  VARCHAR(34);
 
   v_hour  INTEGER;
 
   v_minute  INTEGER;
 
 -- calculate the difference between UTC time and current server time
 
 -- note: some daylight savings time rules are incremented in 15 minute intervals
 
BEGIN
   v_hour := CAST(EXTRACT(HOUR FROM TIMEZONE('UTC',now())) -EXTRACT(HOUR FROM LOCALTIMESTAMP) AS INTEGER);
 
   v_minute := CAST(EXTRACT(MONTH FROM TIMEZONE('UTC',now())) -EXTRACT(MONTH FROM LOCALTIMESTAMP) AS INTEGER);
 
 -- get the current date and time
 
   v_withTZ := TO_CHAR(v_current,'yyyy-mm-dd hh24:mi:ss:ms');
 
 -- format with plus /minus sign which is required by datetimeoffset
 
   IF v_hour >= 0 then
      v_withTZ := coalesce(v_withTZ,'') || '+';
   ELSE
      v_withTZ := coalesce(v_withTZ,'') || '-';
   end if;
 
 -- add leading zero, if required 
 
   IF ABS(v_hour) < 10 then
      v_withTZ := coalesce(v_withTZ,'') || '0';
   end if;
 
   v_withTZ := coalesce(v_withTZ,'') || CAST(ABS(v_hour) AS VARCHAR(2));
 
   v_withTZ := coalesce(v_withTZ,'') || ':';
 
 -- same with minutes
 
   IF v_minute < 10 then
      v_withTZ := coalesce(v_withTZ,'') || '0';
   end if;
 
   v_withTZ := coalesce(v_withTZ,'') || SUBSTR(CAST(v_minute AS VARCHAR(2)),1,2);
 
   RETURN v_withTZ;
END; $$;

