-- Stored procedure definition script USP_ShippingFieldValues_GetAllByDomain for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ShippingFieldValues_GetAllByDomain(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		ShippingFieldValues.numListItemID AS "numShipVia"
		,ShippingFieldValues.intShipFieldID AS "intShipFieldID"
		,ShippingFields.vcFieldName AS "vcFieldName"
		,coalesce(ShippingFieldValues.vcShipFieldValue,'') AS "vcShipFieldValue"
   FROM
   ShippingFieldValues
   INNER JOIN
   ShippingFields
   ON
   ShippingFieldValues.intShipFieldID = ShippingFields.intShipFieldID
   WHERE
   ShippingFieldValues.numDomainID = v_numDomainID
   ORDER BY
   ShippingFieldValues.numListItemID;
END; $$;












