-- Stored procedure definition script usp_GetAllAuthenticationGroups for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetAllAuthenticationGroups(v_numGroupID NUMERIC(18,0) DEFAULT 0,  
 v_vcGroupTypes TEXT DEFAULT '1',
 v_numDomainID NUMERIC(18,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numGroupID = 0 then
	
      open SWV_RefCur for
      SELECT
      numGroupID
			,vcGroupName
      FROM
      AuthenticationGroupMaster
      WHERE
      numDomainID = v_numDomainID
      AND 1 =(CASE
      WHEN LENGTH(coalesce(v_vcGroupTypes,'')) > 0
      THEN(CASE WHEN tintGroupType IN(SELECT Id FROM SplitIDs(v_vcGroupTypes,',')) THEN 1 ELSE 0 END)
      ELSE 1
      END)
      ORDER BY
      numGroupID;
   ELSE
      open SWV_RefCur for
      SELECT
      numGroupID
			,vcGroupName
      FROM
      AuthenticationGroupMaster
      WHERE
      numGroupID = v_numGroupID;
   end if;
   RETURN;
END; $$;


