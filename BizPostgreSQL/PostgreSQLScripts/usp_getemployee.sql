-- Stored procedure definition script USP_GetEmployee for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetEmployee(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   A.numContactId AS "numContactID"
		,CONCAT(A.vcFirstName,' ',A.vcLastname) as "vcUserName"
		,coalesce(A.numTeam,0) AS "numTeam"
   FROM
   UserMaster UM
   INNER JOIN
   AdditionalContactsInformation AS A
   ON
   UM.numUserDetailId = A.numContactId
   WHERE
   UM.numDomainID = v_numDomainID
   AND A.numDomainID = v_numDomainID
   AND coalesce(UM.bitactivateflag,false) = true
   ORDER BY
   CONCAT(A.vcFirstName,' ',A.vcLastname);
END; $$;












