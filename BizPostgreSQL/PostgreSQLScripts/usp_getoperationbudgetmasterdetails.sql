-- Stored procedure definition script USP_GetOperationBudgetMasterDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetOperationBudgetMasterDetails(v_numDomainId NUMERIC(9,0) DEFAULT 0,
 v_numBudgetId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select * From  OperationBudgetMaster Where numBudgetId = v_numBudgetId
   And numDomainId = v_numDomainId;
END; $$;












