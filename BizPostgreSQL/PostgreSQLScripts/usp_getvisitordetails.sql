-- Stored procedure definition script USP_GetVisitorDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetVisitorDetails(v_numTrackingID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select
   cast(vcUserHostAddress as VARCHAR(255)),
cast(vcUserDomain as VARCHAR(255)),
cast(vcUserAgent as VARCHAR(255)),
cast(vcBrowser as VARCHAR(255)),
cast(vcCrawler as VARCHAR(255)),
cast(replace(replace(replace(replace(coalesce(vcURL,'-'),'%3A',':'),'%3F','?'),'%3D',
   '='),'%26','&') as VARCHAR(255))
   as vcURL,
cast(replace(replace(replace(replace(coalesce(vcReferrer,'-'),'%3A',':'),'%3F','?'),
   '%3D','='),'%26','&') as VARCHAR(255))
   as vcReferrer,
cast(coalesce(vcSearchTerm,'-') as VARCHAR(255)) as vcSearchTerm,
cast(numVisits as VARCHAR(255)),
cast(replace(replace(replace(replace(coalesce(vcOrginalURL,'-'),'%3A',':'),'%3F','?'),
   '%3D','='),'%26','&') as VARCHAR(255))
   as vcOrginalURL,
cast(replace(replace(replace(replace(coalesce(vcOrginalRef,'-'),'%3A',':'),'%3F','?'),
   '%3D','='),'%26','&') as VARCHAR(255))
   as vcOrginalRef,
cast(numVistedPages as VARCHAR(255)),
CAST(vcTotalTime AS VARCHAR(8)) as vcTotalTime,
cast(dtCreated as VARCHAR(255)),
cast(replace(replace(replace(replace(coalesce(vcPageName,'-'),'%3A',':'),'%3F','?'),
   '%3D','='),'%26','&') as VARCHAR(255))  as   vcPageName,
cast(vcElapsedTime as VARCHAR(255)),
cast(dtCreated as VARCHAR(255)),
cast(dtTime as VARCHAR(255))
   from TrackingVisitorsHDR
   join TrackingVisitorsDTL
   on numTrackingID = numTracVisitorsHDRID        
--left join PageMasterWebAnlys P        
--on P.numPageID=TrackingVisitorsDTL.numPageID        
   where numTrackingID = v_numTrackingID;
END; $$;












