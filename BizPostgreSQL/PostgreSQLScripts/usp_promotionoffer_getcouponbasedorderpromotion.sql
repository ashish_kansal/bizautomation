-- Stored procedure definition script USP_PromotionOffer_GetCouponBasedOrderPromotion for PostgreSQL
CREATE OR REPLACE FUNCTION USP_PromotionOffer_GetCouponBasedOrderPromotion(v_numDomainId NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   cast(numProId as VARCHAR(255))
		,cast(vcProName as VARCHAR(255))
   FROM
   PromotionOffer
   WHERE
   numDomainId = v_numDomainId
   AND coalesce(bitEnabled,false) = true
   AND coalesce(IsOrderBasedPromotion,false) = true
   AND coalesce(bitRequireCouponCode,false) = true
   ORDER BY
   vcProName;
END; $$;












