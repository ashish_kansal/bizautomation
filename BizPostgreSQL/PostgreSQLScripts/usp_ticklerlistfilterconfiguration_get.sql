-- Stored procedure definition script USP_TicklerListFilterConfiguration_Get for PostgreSQL
CREATE OR REPLACE FUNCTION USP_TicklerListFilterConfiguration_Get(v_numDomainID NUMERIC(18,0),
      v_numUserCntID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT * FROM
   TicklerListFilterConfiguration
   WHERE
   numDomainID = v_numDomainID AND
   numUserCntID = v_numUserCntID;
   RETURN;
END; $$;













