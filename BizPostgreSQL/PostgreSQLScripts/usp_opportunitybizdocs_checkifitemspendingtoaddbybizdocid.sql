-- Stored procedure definition script USP_OpportunityBizDocs_CheckIfItemsPendingToAddByBizDocID for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OpportunityBizDocs_CheckIfItemsPendingToAddByBizDocID(v_numOppID NUMERIC(18,0)
	,v_numBizDocID NUMERIC(18,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF(SELECT
   COUNT(*)
   FROM(SELECT
      OI.numoppitemtCode,
				coalesce(OI.numUnitHour,0) AS OrderedQty,
				coalesce(TempInvoice.AddedQty,0) AS AddedQty
      FROM
      OpportunityItems OI
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      LEFT JOIN LATERAL(SELECT
         SUM(OpportunityBizDocItems.numUnitHour) AS AddedQty
         FROM
         OpportunityBizDocs
         INNER JOIN
         OpportunityBizDocItems
         ON
         OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
         WHERE
         OpportunityBizDocs.numoppid = v_numOppID
         AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
         AND OpportunityBizDocs.numBizDocId = v_numBizDocID) AS TempInvoice on TRUE
      WHERE
      OI.numOppId = v_numOppID) X
   WHERE
   X.OrderedQty <> X.AddedQty) > 0 then
	
      open SWV_RefCur for
      SELECT 1;
   ELSE
      open SWV_RefCur for
      SELECT 0;
   end if;
   RETURN;
END; $$;

	



