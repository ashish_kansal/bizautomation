-- Stored procedure definition script usp_GetDivPartnerContacts for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetDivPartnerContacts(v_numDomainID  NUMERIC DEFAULT 0,
 v_numOppID NUMERIC DEFAULT 0    ,
 v_DivisionID NUMERIC DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF(v_numOppID > 0 AND v_DivisionID = 0) then
	
      SELECT  numPartenerSource INTO v_DivisionID FROM DivisionMaster where numDivisionID = v_numOppID and numDomainID = v_numDomainID     LIMIT 1;
   end if;
	open SWV_RefCur for 
	SELECT 
		A.numContactId AS "numContactId"
		,A.vcFirstName || ' ' || A.vcLastname AS "vcGivenName" 
	FROM 
		AdditionalContactsInformation AS A
	LEFT JOIN 
		DivisionMaster AS D
   ON D.numDivisionID = A.numDivisionId
   WHERE D.numDomainID = v_numDomainID AND D.numDivisionID = v_DivisionID AND A.vcGivenName <> '';
   RETURN;
END; $$;

/****** Object:  StoredProcedure [dbo].[USP_ShoppingCart]    Script Date: 05/07/2009 22:09:30 ******/













