-- Stored procedure definition script USP_StagePercentageDetailsTask_ChangeTime for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_StagePercentageDetailsTask_ChangeTime(v_numDomainID NUMERIC(18,0)
	,v_numTaskID NUMERIC(18,0)
	,v_numHours NUMERIC(18,0)
	,v_numMinutes NUMERIC(18,0)
	,v_bitMasterUpdateAlso BOOLEAN)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numToTalTimeSpendOnTask  NUMERIC(18,0);
   v_vcTimeSpend  VARCHAR(20) DEFAULT '';
   v_numReferenceTaskId  NUMERIC(18,0);
BEGIN
   IF EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID = v_numDomainID AND numTaskId = v_numTaskID AND tintAction = 4) then
	
      RAISE EXCEPTION 'TASK_IS_MARKED_AS_FINISHED';
      RETURN;
   ELSEIF coalesce((SELECT  tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID = v_numDomainID AND numTaskId = v_numTaskID ORDER BY tintAction LIMIT 1),0) IN(1,3)
   then
	
      RAISE EXCEPTION 'TASK_IS_ALREADY_STARTED';
      RETURN;
   ELSE
      v_vcTimeSpend := GetTimeSpendOnTask(v_numDomainID,v_numTaskID,0::SMALLINT);
      IF v_vcTimeSpend = 'Invalid time sequence' then
		
         RAISE EXCEPTION 'INVALID_TIME_ENTRY_SEQUENCE';
         RETURN;
      end if;
      v_numToTalTimeSpendOnTask :=(CAST(SUBSTR(v_vcTimeSpend,0,POSITION(':' IN v_vcTimeSpend)) AS INTEGER)*60)+CAST(SUBSTR(v_vcTimeSpend,POSITION(':' IN v_vcTimeSpend)+1,LENGTH(v_vcTimeSpend)) AS INTEGER);
      If v_numToTalTimeSpendOnTask >((coalesce(v_numHours,0)*60)+coalesce(v_numMinutes,0)) then
		
         RAISE EXCEPTION 'MORE_TIME_SPEND_ON_TASK';
         RETURN;
      end if;
      UPDATE StagePercentageDetailsTask SET numHours = v_numHours,numMinutes = v_numMinutes WHERE numDomainID = v_numDomainID AND numTaskId = v_numTaskID;
      IF v_bitMasterUpdateAlso = true then
         select   coalesce(numReferenceTaskId,0) INTO v_numReferenceTaskId FROM StagePercentageDetailsTask WHERE numDomainID = v_numDomainID AND numTaskId = v_numTaskID;
         UPDATE StagePercentageDetailsTask SET numHours = v_numHours,numMinutes = v_numMinutes WHERE numDomainID = v_numDomainID AND numTaskId = v_numReferenceTaskId;
      end if;
   end if;
END; $$;


