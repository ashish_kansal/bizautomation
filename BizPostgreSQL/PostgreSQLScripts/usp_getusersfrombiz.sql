CREATE OR REPLACE FUNCTION USP_GetUsersFromBiz(v_bitActivateFlag BOOLEAN,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select vcUserName as "vcUserName",vcMailNickName as "vcMailNickName" from UserMaster where bitactivateflag = v_bitActivateFlag;
END; $$;
