-- Stored procedure definition script USP_ManageWarehouseLocation for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
-- EXEC USP_GetWarehouseLocation 1 , 58 , 11
CREATE OR REPLACE FUNCTION USP_ManageWarehouseLocation(v_numDomainID NUMERIC(9,0) ,
    v_numWarehouseID NUMERIC(8,0),
    v_numWLocationID NUMERIC(9,0) DEFAULT 0,
    v_strItems TEXT DEFAULT '',
    v_tintMode SMALLINT DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDocItem  INTEGER;
BEGIN
   IF v_tintMode = 0 then
      IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then

         INSERT INTO WarehouseLocation(numWarehouseID ,
				  vcAisle ,
				  vcRack ,
				  vcShelf ,
				  vcBin ,
				  vcLocation ,
				  bitDefault ,
				  bitSystem ,
				  intQty ,
				  numDomainID)
         SELECT
         v_numWarehouseID,
			X.vcAisle,X.vcRack,X.vcShelf,X.vcBin,X.vcLocation,false,false,0,v_numDomainID
         FROM
		  XMLTABLE
		(
			'NewDataSet/Item'
			PASSING 
				CAST(v_strItems AS XML)
			COLUMNS
				id FOR ORDINALITY,
				vcAisle VARCHAR(50) PATH 'vcAisle',
				vcRack VARCHAR(50) PATH 'vcRack',
				vcShelf VARCHAR(50) PATH 'vcShelf',
				vcBin VARCHAR(50) PATH 'vcBin',
				vcLocation VARCHAR(100) PATH 'vcLocation',
				bitDefault BOOLEAN PATH 'bitDefault',
				bitSystem BOOLEAN PATH 'bitSystem',
				intQty INTEGER PATH 'intQty'
		) X;
      end if;
   end if;

   RETURN; 	
END; $$;
 


