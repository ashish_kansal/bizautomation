-- Stored procedure definition script USP_DemandForecast_GetByID for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DemandForecast_GetByID(v_numDFID NUMERIC(18,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor , INOUT SWV_RefCur3 refcursor , INOUT SWV_RefCur4 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   SELECT * FROM DemandForecast WHERE numDFID = v_numDFID;
   open SWV_RefCur2 for
   SELECT * FROM DemandForecastItemClassification WHERE numDFID = v_numDFID;
   open SWV_RefCur3 for
   SELECT * FROM DemandForecastItemGroup WHERE numDFID = v_numDFID;
   open SWV_RefCur4 for
   SELECT * FROM DemandForecastWarehouse WHERE numDFID = v_numDFID;
   RETURN;
END; $$;



