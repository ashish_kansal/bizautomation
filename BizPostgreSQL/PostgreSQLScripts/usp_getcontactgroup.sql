-- Stored procedure definition script USP_GetContactGroup for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetContactGroup(v_ContactId VARCHAR(100),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select ADC.numContactId ,ADC.vcFirstName,ADC.vcLastname,ADC.vcEmail,C.vcCompanyName
   FROM AdditionalContactsInformation ADC
   JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID
   JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId
   where ADC.numContactId in(select Id from SplitIds(v_ContactId,','));
END; $$;












