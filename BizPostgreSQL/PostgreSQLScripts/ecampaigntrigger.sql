CREATE OR REPLACE FUNCTION ECampaignTrigger_TrFunc()
RETURNS TRIGGER LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numEmailId  NUMERIC(9,0); 
   v_numEmailHstrIDId  NUMERIC(9,0);
   v_numDomainId  NUMERIC(9,0);
   v_tintType  SMALLINT;    
   v_vcMessageFrom  VARCHAR(100);    
   v_vcEmail  VARCHAR(100);        
   v_numConEmailCampID  NUMERIC(9,0);
   SWV_RowCount INTEGER;
BEGIN
   select   numEmailId, tintType, numEmailHstrID INTO v_numEmailId,v_tintType,v_numEmailHstrIDId FROM  new_table;   

   if v_tintType = 4 then

      select   vcEmailId INTO v_vcMessageFrom from EmailMaster where numEmailId = v_numEmailId;
      select   numDomainID INTO v_numDomainId from EmailHistory where numEmailHstrID = v_numEmailHstrIDId;
      select   vcEmail, numConEmailCampID INTO v_vcEmail,v_numConEmailCampID from ConECampaign C
      join AdditionalContactsInformation A
      on A.numContactId = C.numContactID where bitEngaged = true  and vcEmail = v_vcMessageFrom and numDomainID = v_numDomainId    LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount > 0 then 
         update ConECampaign set  bitEngaged = false where numConEmailCampID = v_numConEmailCampID;
      end if;
   end if;
   RETURN NULL;
END; $$;
DROP TRIGGER IF EXISTS ECampaignTrigger ON EmailHStrToBCCAndCC;
CREATE TRIGGER ECampaignTrigger AFTER INSERT ON EmailHStrToBCCAndCC REFERENCING NEW TABLE AS new_table FOR EACH ROW EXECUTE PROCEDURE ECampaignTrigger_TrFunc();
        
 

