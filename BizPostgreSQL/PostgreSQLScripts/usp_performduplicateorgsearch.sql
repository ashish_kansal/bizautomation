CREATE OR REPLACE FUNCTION usp_PerformDuplicateOrgSearch
(
	v_vcCRMTypes VARCHAR(5),                                              
	v_numCompanyType NUMERIC,                                              
	v_vcOrgCreationStartDateRange VARCHAR(10),                                              
	v_vcOrgCreationEndDateRange VARCHAR(10),                                              
	v_boolAssociation BOOLEAN,                                              
	v_boolSameDivisionName BOOLEAN,                                              
	v_boolSameWebsite BOOLEAN,                                              
	v_vcMatchingField  VARCHAR(50),                                            
	v_vcSortColumn VARCHAR(50),                                         
	v_vcSortOrder VARCHAR(50),                                    
	v_numNosRowsReturned INTEGER,                                       
	v_numCurrentPage INTEGER,                                              
	v_numDomainId NUMERIC,
	INOUT SWV_RefCur refcursor
)
LANGUAGE plpgsql                                                                           
AS $$
	DECLARE
	v_intTopOfResultSet  INTEGER;
	v_intStartOfResultSet  INTEGER;                                    
	v_OrgTempCustomTableColumns TEXT;                                              
	v_vcOrgDivisionName  VARCHAR(20);                        
	v_vcOrgWebsite VARCHAR(20);                      
	v_vcOrgCompanyName VARCHAR(200);                      
	v_vcOrgMatchingFieldName VARCHAR(25);                                                               
	v_vcComparisionFieldsNames VARCHAR(200);                        
	v_OrgTempCustomTableDataEntryQuery TEXT;                
	v_OrgTempCustomTableDeleteQuery VARCHAR(100);                      
	v_DuplicateOrgFixedQuery TEXT;                                              
	v_DuplicateOrgCRMTypesQuery VARCHAR(60);                                 
	v_DuplicateCompanyTypeQuery VARCHAR(50);                                        
	v_DuplicateDateRangeQuery VARCHAR(100);                                              
	v_DuplicateAssociationQuery VARCHAR(30);                                             
	v_DuplicateDivisionNameQuery VARCHAR(200);                                              
	v_DuplicateWebsiteQuery VARCHAR(150);                                        
	v_DuplicateMatchingFieldQuery VARCHAR(150);                                              
	v_GroupOrderColumnFieldQuery TEXT;                        
	v_vcDuplicateCompleteQuery TEXT;                    
	v_vcRowBoundOrgDuplicateQuery TEXT;                    
	v_vcDuplicateCountQuery TEXT;                    
	v_vcCompleteOrgDuplicateSearchQuery TEXT;
BEGIN
   v_intTopOfResultSet :=(v_numNosRowsReturned::bigint*v_numCurrentPage::bigint);                    
   v_intStartOfResultSet := v_numNosRowsReturned::bigint*(v_numCurrentPage::bigint -1);                       
   v_vcSortColumn := Replace(v_vcSortColumn,'[Date/Time  Created Last Modified & By Whom]', '3');                      
   v_vcSortColumn := Replace(v_vcSortColumn,'[Organization Name/ Division]','4');                      
   v_vcSortColumn := Replace(v_vcSortColumn,'[Relationship]','5');                      
   v_vcSortColumn := Replace(v_vcSortColumn,'[Zip]','6');                     
   v_vcSortColumn := Replace(v_vcSortColumn,'[Nos. Contacts]','7');                      
   v_vcSortColumn := Replace(v_vcSortColumn,'[Open Opportunities?]','8');                      
   v_vcSortColumn := Replace(v_vcSortColumn,'[Closed Deals?]','9');                      
   v_vcSortColumn := Replace(v_vcSortColumn,'[Status]','10');                      
   v_vcSortColumn := Replace(v_vcSortColumn,'[Date/Time last Contact was Created/Modified]', 
   '11');                      
                                
   --FIND THE CUSTOM FIELDS FOR THE CONTACT                        
   v_OrgTempCustomTableColumns := fn_OrgAndContCustomFieldsColumns(1::SMALLINT,0::SMALLINT);                        
   --PRINT '@ContTempCustomTableColumns: ' +  @ContTempCustomTableColumns                        
   --CREATE A STRING OF FIELDS WHICH ARE TO BE COMPARED                       
   IF v_boolSameDivisionName = true then  --Same Division name                        
      v_vcOrgDivisionName := 'vcOrgDivisionName,';
   ELSE
      v_vcOrgDivisionName := '';
   end if;                        
                        
   IF v_boolSameWebsite = true then  --Same Website                       
      v_vcOrgWebsite := 'vcOrgWebSite,';
   ELSE
      v_vcOrgWebsite := '';
   end if;                        
                                     
   v_vcOrgCompanyName := 'numDivisionId,vcOrgCompanyName,vcContCreatedDate,vcContCreatedBy,numContModifiedDate,numContModifiedBy,';                       
                        
   IF v_vcMatchingField <> '' then --Other Matching Fields                        
      v_vcOrgMatchingFieldName := coalesce(v_vcMatchingField,'') || ',';
   ELSE
      v_vcOrgMatchingFieldName := '';
   end if;                        
                       
   v_vcComparisionFieldsNames := v_vcOrgCompanyName;                      
   IF v_boolSameDivisionName = true then
      v_vcComparisionFieldsNames := coalesce(v_vcComparisionFieldsNames,'') || coalesce(v_vcOrgDivisionName,'');
   end if;                        
   IF v_boolSameWebsite = true then
      v_vcComparisionFieldsNames := coalesce(v_vcComparisionFieldsNames,'') || coalesce(v_vcOrgWebsite,'');
   end if;                        
                    
   IF v_vcMatchingField <> '' AND (POSITION(v_vcMatchingField IN v_vcComparisionFieldsNames) <= 0 AND POSITION(v_vcMatchingField IN coalesce(fn_OrgAndContCustomFieldsColumns(1::SMALLINT,1::SMALLINT),'')) <= 0) then
      v_vcComparisionFieldsNames := coalesce(v_vcComparisionFieldsNames,'') || coalesce(v_vcOrgMatchingFieldName,'');
   end if;                     
                                                              
   v_vcComparisionFieldsNames := SUBSTR(v_vcComparisionFieldsNames,1,LENGTH(v_vcComparisionFieldsNames) -1);                        
   --PRINT '@vcComparisionFieldsNames: ' + @vcComparisionFieldsNames                      
                        
                        
   v_OrgTempCustomTableDataEntryQuery := ' SELECT DISTINCT numCompanyId' || ',' || coalesce(v_vcComparisionFieldsNames,'');                        
                                      
   IF (v_OrgTempCustomTableColumns Is Not NULL) then
      v_OrgTempCustomTableDataEntryQuery := coalesce(v_OrgTempCustomTableDataEntryQuery,'') || ' ' || fn_OrgAndContCustomFieldsColumns(1::SMALLINT,2::SMALLINT);
   end if;                                      
                        
   v_OrgTempCustomTableDataEntryQuery := coalesce(v_OrgTempCustomTableDataEntryQuery,'') || ' FROM vw_Records4Duplicates ORDER BY numCompanyId';                              
                                
   v_OrgTempCustomTableDataEntryQuery := 'Select * INTO TEMPORARY TABLE tt_TableCstFld FROM (' || coalesce(v_OrgTempCustomTableDataEntryQuery,'') || ') AS tmpTable1;';                        
    --PRINT @OrgTempCustomTableDataEntryQuery                      
    --EXECUTE (@OrgTempCustomTableDataEntryQuery)                       
   v_OrgTempCustomTableDeleteQuery := 'drop table IF EXISTS tt_TableCstFld CASCADE;' || CHR(10) || 'drop table IF EXISTS tt_TableDuplicateOrg CASCADE;';                     
                                           
   v_DuplicateOrgFixedQuery := 'SELECT DISTINCT
									Rec1.numCompanyId
									,Rec1.numDivisionId
									,fn_GetLatestEntityEditedDateAndUser(Rec1.numDivisionId, 0, ''Org'') AS "Date/Time  Created Last Modified & By Whom"
									,LTrim(Rec1.vcOrgCompanyName) + ''/ '' + Rec1.vcOrgDivisionName As "Organization Name/ Division"
									,fn_AdvSearchColumnName(coalesce(Rec1.vcOrgCompanyType,0),''L'') AS "Relationship"
									,Rec1.vcOrgBillPostCode as "Zip"
									,Count(DISTINCT(numContactId)) AS "Nos. Contacts"
									,coalesce(recOpp.boolOpportunityOpen,''No'') AS "Open Opportunities?"
									,coalesce(recOpp.boolClosedDeal,''No'') AS "Closed Deals?"
									,fn_AdvSearchColumnName(coalesce(numStatusID,0),''L'') AS "Status"
									,CASE 
										WHEN Max(Rec2.vcContCreatedDate) > Max(coalesce(Rec2.numContModifiedDate,0)) 
										THEN Cast(Max(Rec2.vcContCreatedDate) As NVarchar)
										ELSE Cast(Max(Rec2.numContModifiedDate) As NVarchar)                      
									END AS "Date/Time last Contact was Created/Modified"                                                                
 FROM    vw_Records4Duplicates Rec1  INNER JOIN tt_TableCstFld recCustom ON recCustom.numDivisionId = Rec1.numDivisionId                               
  LEFT OUTER JOIN                                               
  (SELECT numDivisionId, Case WHEN SUM(tintOppStatus) = 0 THEN ''No'' ELSE ''Yes'' END AS boolOpportunityOpen,                                               
   Case WHEN SUM(tintShipped) = 0 THEN ''No'' ELSE ''Yes'' END AS boolClosedDeal                                               
   FROM OpportunityMaster GROUP BY numDivisionId                                            
  ) recOpp                                              
    ON Rec1.numDivisionId = recOpp.numDivisionId INNER JOIN                                                
    tt_TableCstFld Rec2                                            
   ON (LTRIM(Rec1.vcOrgCompanyName) = LTRIM(Rec2.vcOrgCompanyName)                             
   AND  Rec1.numCompanyId <> Rec2.numCompanyId AND Rec1.numDomainId = ' || SUBSTR(Cast(v_numDomainId as VARCHAR(30)),1,30) || ') ';    
--PRINT @DuplicateOrgFixedQuery                                              
                                              
   v_DuplicateOrgCRMTypesQuery := ' WHERE Convert(NVarchar(1), Rec1.vcOrgCRMType) IN (' || coalesce(v_vcCRMTypes,'') || ') ';                                              
--PRINT @DuplicateOrgCRMTypesQuery                                              
                                              
   v_DuplicateCompanyTypeQuery := '';                      
   IF v_numCompanyType <> 0 then
      v_DuplicateCompanyTypeQuery := ' AND Rec1.vcOrgCompanyType = ' || SUBSTR(CAST(v_numCompanyType AS VARCHAR(30)),1,30);
   end if;                                              
--PRINT @DuplicateCompanyTypeQuery                                              
                                              
   v_DuplicateDateRangeQuery := '';                      
   IF LTRIM(v_vcOrgCreationStartDateRange) <> '' AND LTrim(v_vcOrgCreationEndDateRange) <> '' then
      v_DuplicateDateRangeQuery := ' AND (Rec1.vcOrgCreatedDate >= ''' || coalesce(v_vcOrgCreationStartDateRange,'') || '''' ||
      ' AND Rec1.vcOrgCreatedDate <= ''' || coalesce(v_vcOrgCreationEndDateRange,'') || ''') ';
   ELSEIF LTRIM(v_vcOrgCreationStartDateRange) <> '' AND LTrim(v_vcOrgCreationEndDateRange) = ''
   then
      v_DuplicateDateRangeQuery := ' AND (Rec1.vcOrgCreatedDate >= ''' || coalesce(v_vcOrgCreationStartDateRange,'') || ''') ';
   ELSEIF LTRIM(v_vcOrgCreationStartDateRange) = '' AND LTrim(v_vcOrgCreationEndDateRange) <> ''
   then
      v_DuplicateDateRangeQuery := ' AND Rec1.vcOrgCreatedDate <= ''' || coalesce(v_vcOrgCreationEndDateRange,'') || ''') ';
   end if;                                              
--PRINT @DuplicateDateRangeQuery                                              
                                              
                                              
   v_DuplicateAssociationQuery := '';           
   IF v_boolAssociation = true then
      v_DuplicateAssociationQuery := ' AND vcAssociation IS NULL';
   end if;                                              
--PRINT @DuplicateAssociationQuery                                              
                                              
         
   v_DuplicateDivisionNameQuery := '';                                            
   IF v_boolSameDivisionName = true then
      v_DuplicateDivisionNameQuery := ' AND Rec1.vcOrgDivisionName = Rec2.vcOrgDivisionName                                            
 AND Rec1.numDivisionId <> Rec2.numDivisionId ';
   end if;                                            
--PRINT @DuplicateDivisionNameQuery                                              
                                            
   v_DuplicateWebsiteQuery := '';                                            
   IF v_boolSameWebsite = true then
      v_DuplicateWebsiteQuery := ' AND Rec1.vcOrgWebSite = Rec2.vcOrgWebSite                                             
 AND Rec1.numCompanyId <> Rec2.numCompanyId ';
   end if;                                              
---PRINT @DuplicateWebsiteQuery                                              
                                    
                                            
   v_DuplicateMatchingFieldQuery := '';                                            
   IF v_vcMatchingField <> '' then
      v_DuplicateMatchingFieldQuery := ' AND recCustom.' || coalesce(v_vcMatchingField,'') || ' = Rec2.' || coalesce(v_vcMatchingField,'') || ' ';
   end if;                                             
--PRINT @DuplicateMatchingFieldQuery                                              
                                              
                                            
   v_GroupOrderColumnFieldQuery := ' GROUP BY Rec1.numCompanyId, Rec1.numDivisionId, Rec1.vcOrgCompanyName, Rec1.vcOrgDivisionName, Rec1.vcOrgCompanyType, Rec1.vcOrgBillPostCode, recOpp.boolOpportunityOpen, recOpp.boolClosedDeal, Rec1.numStatusID ORDER BY
  
     
      
' || coalesce(v_vcSortColumn,'') || ' ' || coalesce(v_vcSortOrder,'');                                      
--PRINT @GroupOrderColumnFieldQuery                                              
                       
   v_vcDuplicateCompleteQuery := coalesce(v_DuplicateOrgFixedQuery,'') || coalesce(v_DuplicateOrgCRMTypesQuery,'') || coalesce(v_DuplicateCompanyTypeQuery,'') || coalesce(v_DuplicateDateRangeQuery,'') || coalesce(v_DuplicateAssociationQuery,'') || coalesce(v_DuplicateDivisionNameQuery,'')
   || coalesce(v_DuplicateWebsiteQuery,'') ||  coalesce(v_DuplicateMatchingFieldQuery,'') || coalesce(v_GroupOrderColumnFieldQuery,'');  
   v_vcDuplicateCompleteQuery := CHR(10) || 'Select IDENTITY(int) AS numRow, * INTO TEMPORARY TABLE tt_TableDuplicateOrg FROM (' || coalesce(v_vcDuplicateCompleteQuery,'') || ') AS tmpTableAllFields;
  
    
      
';                      
                    
   v_vcDuplicateCountQuery := CHR(10) || 'SELECT coalesce(Max(numRow),0) AS DupCount FROM tt_TableDuplicateOrg;';                    
   v_vcRowBoundOrgDuplicateQuery := 'SELECT * FROM tt_TableDuplicateOrg WHERE numRow > ' || SUBSTR(CAST(v_intStartOfResultSet AS VARCHAR(9)),1,9) ||  ' AND numRow <= ' || SUBSTR(CAST(v_intTopOfResultSet AS VARCHAR(9)),1,9) || '            
 ORDER BY ' || (case when v_vcSortColumn = '' then '0' else v_vcSortColumn end) || ' ' || coalesce(v_vcSortOrder,'') || '; ' || coalesce(v_vcDuplicateCountQuery,'');                    
                    
   v_OrgTempCustomTableDataEntryQuery := coalesce(v_OrgTempCustomTableDataEntryQuery,'') || CHR(10);        
   v_vcRowBoundOrgDuplicateQuery := coalesce(v_vcRowBoundOrgDuplicateQuery,'') || CHR(10);        
        
   v_vcCompleteOrgDuplicateSearchQuery := coalesce(v_vcDuplicateCompleteQuery,'') || CHR(10);                      
   RAISE NOTICE '%',(coalesce(v_OrgTempCustomTableDataEntryQuery,'') || coalesce(v_vcCompleteOrgDuplicateSearchQuery,'') || coalesce(v_vcRowBoundOrgDuplicateQuery,'') || coalesce(v_OrgTempCustomTableDeleteQuery,''));        
   OPEN SWV_RefCur FOR EXECUTE coalesce(v_OrgTempCustomTableDataEntryQuery,'') || coalesce(v_vcCompleteOrgDuplicateSearchQuery,'') || coalesce(v_vcRowBoundOrgDuplicateQuery,'') || coalesce(v_OrgTempCustomTableDeleteQuery,'');
   RETURN;
END; $$;


