-- Stored procedure definition script USP_BroadCastDtls_UpdateSentStatus for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_BroadCastDtls_UpdateSentStatus(v_numBroadCastDtlID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
  
-- SET NOCOUNT ON added to prevent extra result sets from  
-- interfering with SELECT statements.  
  

   UPDATE BroadCastDtls SET tintSucessfull = 1 WHERE numBroadCastDtlId = v_numBroadCastDtlID;
   UPDATE Broadcast SET numTotalSussessfull = numTotalSussessfull+1,bitBroadcasted = true WHERE numBroadCastId = coalesce((SELECT numBroadcastID FROM BroadCastDtls WHERE numBroadCastDtlId = v_numBroadCastDtlID),0);
   RETURN;
END; $$;  



