-- Stored procedure definition script USP_AdvancedSearchGroup for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_AdvancedSearchGroup(v_WhereCondition VARCHAR(1000) DEFAULT '',                  
v_ViewID SMALLINT DEFAULT NULL,                  
v_AreasofInt VARCHAR(100) DEFAULT '',                  
v_tintUserRightType SMALLINT DEFAULT NULL,                  
v_numDomainID NUMERIC(9,0) DEFAULT 0,                  
v_numUserCntID NUMERIC(9,0) DEFAULT 0,                  
v_numGroupID NUMERIC(9,0) DEFAULT 0,                  
v_CurrentPage INTEGER DEFAULT NULL,                                                        
v_PageSize INTEGER DEFAULT NULL,                                                        
INOUT v_TotRecs INTEGER  DEFAULT NULL,                                                                                                 
v_columnSortOrder VARCHAR(10) DEFAULT NULL,                  
v_ColumnSearch VARCHAR(100) DEFAULT '',               
v_ColumnName VARCHAR(20) DEFAULT '',                 
v_GetAll BOOLEAN DEFAULT NULL,                  
v_SortCharacter CHAR(1) DEFAULT NULL,            
v_SortColumnName VARCHAR(20) DEFAULT '', INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintOrder  SMALLINT;                  
   v_vcFormFieldName  VARCHAR(50);                  
   v_vcListItemType  VARCHAR(3);             
   v_vcListItemType1  VARCHAR(3);                 
   v_vcAssociatedControlType  VARCHAR(10);                  
   v_numListID  NUMERIC(9,0);                  
   v_vcDbColumnName  VARCHAR(20);                   
            
   v_strSql  VARCHAR(8000);                                                  
   v_firstRec  INTEGER;                                                        
   v_lastRec  INTEGER;
   SWV_RowCount INTEGER;
BEGIN
   if (v_SortCharacter <> '0' and v_SortCharacter <> '') then 
      v_ColumnSearch := v_SortCharacter;
   end if;            
            
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   drop table IF EXISTS tt_TEMPTABLE CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLE 
   ( 
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numcontactid VARCHAR(15)
   );                                                        
                                                        
   v_strSql := 'Select ADC.numContactId        
  FROM AdditionalContactsInformation ADC                                       
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                      
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId   ';              
   if (v_ColumnName <> '' and  v_ColumnSearch <> '') then
  
      select   vcListItemType INTO v_vcListItemType from View_DynamicDefaultColumns where vcDbColumnName = v_ColumnName and numFormId = 1 and numDomainID = v_numDomainID;
      if v_vcListItemType = 'LI' then
    
         v_strSql := coalesce(v_strSql,'') || ' left Join ListDetails L1 on L1.numListItemID =' || coalesce(v_ColumnName,'');
      ELSEIF v_vcListItemType = 'S'
      then
    
         v_strSql := coalesce(v_strSql,'') || ' left join State S1 on S1.numStateID =' || coalesce(v_ColumnName,'');
      ELSEIF v_vcListItemType = 'T'
      then
    
         v_strSql := coalesce(v_strSql,'') || ' left join TerritoryMaster T1 on T1.numTerID =' || coalesce(v_ColumnName,'');
      end if;
   end if;              
   if (v_SortColumnName <> '') then
  
      select   vcListItemType INTO v_vcListItemType1 from View_DynamicDefaultColumns where vcDbColumnName = v_SortColumnName and numFormId = 1 and numDomainID = v_numDomainID;
      if v_vcListItemType1 = 'LI' then
    
         v_strSql := coalesce(v_strSql,'') || ' left Join ListDetails L2 on L2.numListItemID =' || coalesce(v_SortColumnName,'');
      ELSEIF v_vcListItemType1 = 'S'
      then
    
         v_strSql := coalesce(v_strSql,'') || ' left join State S2 on S2.numStateID =' || coalesce(v_SortColumnName,'');
      ELSEIF v_vcListItemType1 = 'T'
      then
    
         v_strSql := coalesce(v_strSql,'') || ' left join TerritoryMaster T2 on T2.numTerID =' || coalesce(v_SortColumnName,'');
      end if;
   end if;              
   v_strSql := coalesce(v_strSql,'') || ' where DM.numDomainID  = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) ||   coalesce(v_WhereCondition,'');                      
   if v_tintUserRightType = 1 then 
      v_strSql := coalesce(v_strSql,'') || ' AND (ADC.numRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')';
   ELSEIF v_tintUserRightType = 2
   then 
      v_strSql := coalesce(v_strSql,'') || ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' ) or DM.numTerID=0) ';
   end if;                                   
            
              
                
                                
   if    v_AreasofInt <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and ADC.numContactID in (select numContactID from  AOIContactLink where numAOIID in(' || coalesce(v_AreasofInt,'') || '))';
   end if;                     
   if (v_ColumnName <> '' and  v_ColumnSearch <> '') then

      if v_vcListItemType = 'LI' then
    
         v_strSql := coalesce(v_strSql,'') || ' and L1.vcData like ''' || coalesce(v_ColumnSearch,'') || '%''';
      ELSEIF v_vcListItemType = 'S'
      then
    
         v_strSql := coalesce(v_strSql,'') || ' and S1.vcState like ''' || coalesce(v_ColumnSearch,'') || '%''';
      ELSEIF v_vcListItemType = 'T'
      then
    
         v_strSql := coalesce(v_strSql,'') || ' and T1.vcTerName like ''' || coalesce(v_ColumnSearch,'')  || '%''';
      else 
         v_strSql := coalesce(v_strSql,'') || ' and ' || coalesce(v_ColumnName,'') || ' like ''' || coalesce(v_ColumnSearch,'')  || '%''';
      end if;
   end if;              
   if (v_SortColumnName <> '') then
  
      if v_vcListItemType1 = 'LI' then
    
         v_strSql := coalesce(v_strSql,'') || ' order by L2.vcData ' || coalesce(v_columnSortOrder,'');
      ELSEIF v_vcListItemType1 = 'S'
      then
    
         v_strSql := coalesce(v_strSql,'') || ' order by S2.vcState ' || coalesce(v_columnSortOrder,'');
      ELSEIF v_vcListItemType1 = 'T'
      then
    
         v_strSql := coalesce(v_strSql,'') || ' order by T2.vcTerName ' || coalesce(v_columnSortOrder,'');
      else  
         v_strSql := coalesce(v_strSql,'') || ' order by ' || coalesce(v_SortColumnName,'') || ' ' || coalesce(v_columnSortOrder,'');
      end if;
   end if;            
             
                   
   RAISE NOTICE '%',v_strSql;                  
                     
   EXECUTE 'insert into tt_TEMPTABLE(numContactID)                                                        
' || v_strSql;                   
   v_strSql := '';                  
                  
                 
   v_tintOrder := 0;                  
   v_WhereCondition := '';                  
   v_strSql := 'select ADC.numContactId,ADC.VcFirstName,ADC.vcLastName,c.vcCompanyName,adc.vcEmail ';                  
   select   A.tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID INTO v_tintOrder,v_vcDbColumnName,v_vcFormFieldName,v_vcAssociatedControlType,
   v_vcListItemType,v_numListID from AdvSerViewConf A
   join View_DynamicDefaultColumns D
   on D.numFieldID = A.numFormFieldID where tintViewID = v_ViewID and numGroupID = v_numGroupID AND D.numDomainID = v_numDomainID AND A.numDomainID = v_numDomainID   order by A.tintOrder asc LIMIT 1;      
            
   while v_tintOrder > 0 LOOP
      if v_vcAssociatedControlType = 'SelectBox' then
        
         if v_vcListItemType = 'LI' then
  
            v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' [' || coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || ']';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
         ELSEIF v_vcListItemType = 'S'
         then
  
            v_strSql := coalesce(v_strSql,'') || ',S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcState' || ' [' || coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || ']';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left join State S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numStateID=' || coalesce(v_vcDbColumnName,'');
         ELSEIF v_vcListItemType = 'T'
         then
  
            v_strSql := coalesce(v_strSql,'') || ',T' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcTerName' || ' [' || coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || ']';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left join TerritoryMaster T' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on T' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numTerID=' || coalesce(v_vcDbColumnName,'');
         end if;
      else 
         v_strSql := coalesce(v_strSql,'') || ',' || coalesce(v_vcDbColumnName,'') || ' [' || coalesce(v_vcFormFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || ']';
      end if;
      select   A.tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID INTO v_tintOrder,v_vcDbColumnName,v_vcFormFieldName,v_vcAssociatedControlType,
      v_vcListItemType,v_numListID from AdvSerViewConf A
      join View_DynamicDefaultColumns D
      on D.numFieldID = A.numFormFieldID where tintViewID = v_ViewID and numGroupID = v_numGroupID AND D.numDomainID = v_numDomainID AND A.numDomainID = v_numDomainID and A.tintOrder > v_tintOrder -1   order by A.tintOrder asc LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount = 0 then 
         v_tintOrder := 0;
      end if;
   END LOOP;                  
                  
                  
                  
                  
                                     
                                                        
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                                        
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                                                         
   select count(*) INTO v_TotRecs from tt_TEMPTABLE;                                       
                              
   if v_GetAll = false then
      v_strSql := coalesce(v_strSql,'') || 'FROM AdditionalContactsInformation ADC                                       
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                      
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                  
   ' || coalesce(v_WhereCondition,'') || ' join #tempTable T on T.numContactId=ADC.numContactId                                                   
   WHERE ID > ' || SUBSTR(CAST(v_firstRec AS VARCHAR(10)),1,10) || 'and ID <' || SUBSTR(CAST(v_lastRec AS VARCHAR(10)),1,10);
   else
      v_strSql := coalesce(v_strSql,'') || 'FROM AdditionalContactsInformation ADC                                       
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                      
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                  
   ' || coalesce(v_WhereCondition,'') || ' join #tempTable T on T.numContactId=ADC.numContactId';
   end if;                                  
   RAISE NOTICE '%',v_strSql;                     
   OPEN SWV_RefCur FOR EXECUTE v_strSql;                                                     
   drop table IF EXISTS tt_TEMPTABLE CASCADE;
   RETURN;
END; $$;


