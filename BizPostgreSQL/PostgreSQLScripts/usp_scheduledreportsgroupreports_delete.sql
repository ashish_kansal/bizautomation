-- Stored procedure definition script USP_ScheduledReportsGroupReports_Delete for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ScheduledReportsGroupReports_Delete(v_numDomainID NUMERIC(18,0)
	,v_numSRGID NUMERIC(18,0)
	,v_vcReports TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF coalesce(v_vcReports,'') <> '' then
	
      DELETE FROM ScheduledReportsGroupReports SRGR using ScheduledReportsGroup SRG where SRG.ID = SRGR.numSRGID AND SRG.numDomainID = v_numDomainID
      AND numSRGID = v_numSRGID
      AND SRGR.ID IN(SELECT Id FROM SplitIDs(v_vcReports,','));
   end if;
   RETURN;
END; $$;


