DROP FUNCTION IF EXISTS USP_ManageCheckHeader;

CREATE OR REPLACE FUNCTION USP_ManageCheckHeader(v_tintMode SMALLINT ,
    v_numDomainID NUMERIC(18,0) ,
	v_numUserCntID NUMERIC(18,0) ,
    v_numCheckHeaderID NUMERIC(18,0) ,
    v_numChartAcntId NUMERIC(18,0) ,
    v_numDivisionID NUMERIC(18,0) ,
    v_monAmount DECIMAL(20,5) ,
    v_numCheckNo NUMERIC(18,0) ,
    v_tintReferenceType SMALLINT ,
    v_numReferenceID NUMERIC(18,0) ,
    v_dtCheckDate TIMESTAMP ,
    v_bitIsPrint BOOLEAN ,
    v_vcMemo VARCHAR(1000) ,
    INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql --Validation of closed financial year
   AS $$
   DECLARE
   v_numAccountClass  NUMERIC(18,0);	
			
   v_tintDefaultClassType  INTEGER DEFAULT 0;
BEGIN
   PERFORM USP_ValidateFinancialYearClosingDate(v_numDomainID,v_dtCheckDate);
	
   IF v_tintMode = 1 then --SELECT Particulat Check Header
    
      open SWV_RefCur for
      SELECT
      CH.numCheckHeaderID ,
            CH.numChartAcntId ,
            CH.numDomainID ,
            CH.numDivisionID ,
            CH.monAmount ,
            CH.numCheckNo ,
            CH.tintReferenceType ,
            CH.numReferenceID ,
            CH.dtCheckDate ,
            CH.bitIsPrint ,
            CH.vcMemo ,
            CH.numCreatedBy ,
            CH.dtCreatedDate ,
            CH.numModifiedBy ,
            CH.dtModifiedDate ,
            coalesce(GJD.numTransactionId,0) AS numTransactionId
      FROM
      CheckHeader CH
      LEFT OUTER JOIN
      General_Journal_Details GJD
      ON
      GJD.tintReferenceType = 1
      AND GJD.numReferenceID = CH.numCheckHeaderID
      WHERE
      numCheckHeaderID = v_numCheckHeaderID
      AND CH.numDomainID = v_numDomainID;
   ELSEIF v_tintMode = 2
   then --Insert/Update Check Header
    
      IF v_numCheckHeaderID > 0 then --Update
        
         UPDATE
         CheckHeader
         SET
         numChartAcntId = v_numChartAcntId,numDivisionID = v_numDivisionID,monAmount = v_monAmount,
         numCheckNo = v_numCheckNo,tintReferenceType = v_tintReferenceType,
         numReferenceID = v_numReferenceID,dtCheckDate = v_dtCheckDate,
         bitIsPrint = v_bitIsPrint,vcMemo = v_vcMemo,numModifiedBy = v_numUserCntID,
         dtModifiedDate = TIMEZONE('UTC',now())
         WHERE
         numCheckHeaderID = v_numCheckHeaderID
         AND numDomainID = v_numDomainID;
      ELSE
			--Set Default Class If enable User Level Class Accountng 
             --Insert
         v_numAccountClass := 0;
         IF v_tintReferenceType = 1 then --Only for Direct Check
            select   coalesce(tintDefaultClassType,0) INTO v_tintDefaultClassType FROM Domain WHERE numDomainId = v_numDomainID;
            IF v_tintDefaultClassType = 1 then --USER
				
               select   coalesce(numDefaultClass,0) INTO v_numAccountClass FROM
               UserMaster UM
               JOIN
               Domain D
               ON
               UM.numDomainID = D.numDomainId WHERE
               D.numDomainId = v_numDomainID
               AND UM.numUserDetailId = v_numUserCntID;
            ELSEIF v_tintDefaultClassType = 2
            then --COMPANY
				
               select   coalesce(numAccountClassID,0) INTO v_numAccountClass FROM
               DivisionMaster DM WHERE
               DM.numDomainID = v_numDomainID
               AND DM.numDivisionID = v_numDivisionID;
            ELSE
               v_numAccountClass := 0;
            end if;
         end if;
         INSERT  INTO CheckHeader(numChartAcntId ,
                numDomainID ,
                numDivisionID ,
                monAmount ,
                numCheckNo ,
                tintReferenceType ,
                numReferenceID ,
                dtCheckDate ,
                bitIsPrint ,
                vcMemo ,
                numCreatedBy ,
                dtCreatedDate ,
                numAccountClass)
         SELECT
         v_numChartAcntId ,
				v_numDomainID ,
				v_numDivisionID ,
				v_monAmount ,
				v_numCheckNo ,
				v_tintReferenceType ,
				v_numReferenceID ,
				v_dtCheckDate ,
				v_bitIsPrint ,
				v_vcMemo ,
				v_numUserCntID ,
				TIMEZONE('UTC',now()) ,
				v_numAccountClass;
         v_numCheckHeaderID := CURRVAL('CheckHeader_seq');
      end if;

      open SWV_RefCur for
      SELECT
      numCheckHeaderID ,
            numChartAcntId ,
            numDomainID ,
            numDivisionID ,
            monAmount ,
            numCheckNo ,
            tintReferenceType ,
            numReferenceID ,
            dtCheckDate ,
            bitIsPrint ,
            vcMemo ,
            numCreatedBy ,
            dtCreatedDate ,
            numModifiedBy ,
            dtModifiedDate
      FROM
      CheckHeader
      WHERE
      numCheckHeaderID = v_numCheckHeaderID
      AND numDomainID = v_numDomainID;
   ELSEIF v_tintMode = 3
   then --Print Check List
    
      open SWV_RefCur for
      SELECT  CH.numCheckHeaderID ,
                CH.numChartAcntId ,
                CH.numDomainID ,
                CH.numDivisionID ,
                CH.monAmount ,
                CH.numCheckNo ,
                CH.tintReferenceType ,
                CH.numReferenceID ,
                CH.dtCheckDate ,
                CH.bitIsPrint ,
                CH.vcMemo ,
                CH.numCreatedBy ,
                CH.dtCreatedDate ,
                CH.numModifiedBy ,
                CH.dtModifiedDate ,
                CASE CH.tintReferenceType
      WHEN 11
      THEN fn_GetUserName(coalesce(PD.numUserCntID,0))
      ELSE fn_GetComapnyName(coalesce(CH.numDivisionID,0))
      END AS vcCompanyName ,
                CASE CH.tintReferenceType
      WHEN 1 THEN 'Checks'
      WHEN 8 THEN 'Bill Payment'
      WHEN 10 THEN 'RMA'
      WHEN 11 THEN 'Payroll'
      END AS vcReferenceType ,
                fn_GetContactName(CH.numCreatedBy) AS EmployerName ,
                CASE CH.tintReferenceType
      WHEN 1 THEN ''
      WHEN 8
      THEN coalesce((SELECT string_agg(CASE
                  WHEN coalesce(BPD.numBillID,0) > 0
                  THEN 'Bill-'
                     || CAST(BPD.numBillID AS VARCHAR(18))
                  ELSE CAST(OBD.vcBizDocID AS VARCHAR(18))
                  END,',') 
                  FROM
                  BillPaymentHeader BPH
                  JOIN BillPaymentDetails BPD ON BPH.numBillPaymentID = BPD.numBillPaymentID
                  LEFT JOIN OpportunityBizDocs OBD ON OBD.numOppBizDocsId = BPD.numOppBizDocsID
                  WHERE
                  BPH.numDomainId = CH.numDomainID
                  AND BPH.numBillPaymentID = CH.numReferenceID),'')
      END AS vcBizDoc ,
                CASE CH.tintReferenceType
      WHEN 1 THEN ''
      WHEN 8
      THEN coalesce((SELECT string_agg(CASE
                  WHEN BPD.numOppBizDocsID > 0
                  THEN CAST(coalesce(OBD.vcRefOrderNo,'') AS VARCHAR(18))
                  WHEN BPD.numBillID > 0
                  THEN CAST(coalesce(BH.vcReference,'') AS VARCHAR(500))
                  END,',')
                 
                  FROM
                  BillPaymentHeader BPH
                  JOIN BillPaymentDetails BPD ON BPH.numBillPaymentID = BPD.numBillPaymentID
                  LEFT JOIN OpportunityBizDocs OBD ON OBD.numOppBizDocsId = BPD.numOppBizDocsID
                  LEFT JOIN BillHeader BH ON BPD.numBillID = BH.numBillID
                  WHERE
                  BPH.numDomainId = CH.numDomainID
                  AND BPH.numBillPaymentID = CH.numReferenceID),'')
      ELSE ''
      END AS vcRefOrderNo
      FROM    CheckHeader CH
      LEFT JOIN PayrollDetail PD ON CH.numReferenceID = PD.numPayrollDetailID
      AND CH.tintReferenceType = 11
      WHERE   CH.numDomainID = v_numDomainID
      AND (numCheckHeaderID = v_numCheckHeaderID
      OR v_numCheckHeaderID = 0)
      AND (CH.tintReferenceType = v_tintReferenceType
      OR v_tintReferenceType = 0)
      AND (CH.numReferenceID = v_numReferenceID
      OR v_numReferenceID = 0)
      AND CH.numChartAcntId = v_numChartAcntId
      AND coalesce(bitIsPrint,false) = false;
   ELSEIF v_tintMode = 4
   then --New Print Check List
	
      open SWV_RefCur for
      SELECT
      CH.numCheckHeaderID ,
			CH.numChartAcntId ,
			CH.numDomainID ,
			CH.numDivisionID ,
			(CASE WHEN tintReferenceType = 10 OR tintReferenceType = 1 THEN coalesce(CH.monAmount,0) ELSE  coalesce(BPD.monAmount,0) END) AS monAmount,
			CH.numCheckNo ,
			CH.tintReferenceType ,
			CH.numReferenceID ,
			CH.dtCheckDate ,
			CH.bitIsPrint ,
			coalesce(CH.vcMemo,'') AS vcMemo,
			CH.numCreatedBy ,
			CH.dtCreatedDate ,
			CH.numModifiedBy ,
			CH.dtModifiedDate ,
			CASE CH.tintReferenceType
      WHEN 11
      THEN fn_GetUserName(coalesce(PD.numUserCntID,0))
      ELSE fn_GetComapnyName(coalesce(CH.numDivisionID,0))
      END AS vcCompanyName ,
			CASE CH.tintReferenceType
      WHEN 1 THEN 'Checks'
      WHEN 8 THEN 'Bill Payment'
      WHEN 10 THEN 'RMA'
      WHEN 11 THEN 'Payroll'
      END AS vcReferenceType ,
			fn_GetContactName(CH.numCreatedBy) AS EmployerName ,
			CASE CH.tintReferenceType
      WHEN 1 THEN ''
      WHEN 8
      THEN CASE WHEN coalesce(BPD.numBillID,0) > 0
         THEN 'Bill-'
            || CAST(BPD.numBillID AS VARCHAR(18))
         ELSE CAST(OBD.vcBizDocID AS VARCHAR(18))
         END
      END AS vcBizDoc ,
			CASE CH.tintReferenceType
      WHEN 1 THEN ''
      WHEN 8
      THEN CASE WHEN BPD.numOppBizDocsID > 0
         THEN CAST(coalesce(OBD.vcRefOrderNo,'') AS VARCHAR(18))
         WHEN BPD.numBillID > 0
         THEN CAST(coalesce(BH.vcReference,'') AS VARCHAR(500))
         END
      ELSE ''
      END AS vcRefOrderNo
      FROM
      CheckHeader CH
      LEFT JOIN PayrollDetail PD ON CH.numReferenceID = PD.numPayrollDetailID AND CH.tintReferenceType = 11
      LEFT JOIN BillPaymentHeader BPH ON BPH.numBillPaymentID = CH.numReferenceID AND BPH.numDomainId = CH.numDomainID
      LEFT JOIN BillPaymentDetails BPD ON BPH.numBillPaymentID = BPD.numBillPaymentID
      LEFT JOIN OpportunityBizDocs OBD ON OBD.numOppBizDocsId = BPD.numOppBizDocsID
      LEFT JOIN BillHeader BH ON BPD.numBillID = BH.numBillID
      WHERE   CH.numDomainID = v_numDomainID
      AND (CH.numDivisionID = v_numDivisionID OR v_numDivisionID = 0)
      AND (numCheckHeaderID = v_numCheckHeaderID
      OR v_numCheckHeaderID = 0)
      AND (CH.tintReferenceType = v_tintReferenceType
      OR v_tintReferenceType = 0)
      AND (CH.numReferenceID = v_numReferenceID
      OR v_numReferenceID = 0)
      AND CH.numChartAcntId = v_numChartAcntId
      AND coalesce(bitIsPrint,false) = false;
   end if;
   RETURN;
END; $$;


