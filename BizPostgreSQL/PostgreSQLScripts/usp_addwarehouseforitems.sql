-- Stored procedure definition script USP_AddWareHouseForItems for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_AddWareHouseForItems(v_numItemCode NUMERIC(9,0) DEFAULT 0,  
v_numWareHouseID NUMERIC(9,0) DEFAULT 0,
v_numWareHouseItemID NUMERIC(9,0) DEFAULT 0 ,
v_vcLocation VARCHAR(250) DEFAULT '',
v_monWListPrice NUMERIC(19,4) DEFAULT 0,
v_numOnHand NUMERIC(18,3) DEFAULT 0,
v_vcWHSKU VARCHAR(100) DEFAULT '',
v_vcBarCode VARCHAR(50) DEFAULT '',
v_numDomainID NUMERIC(9,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_numWareHouseID > 0 then

      insert into WareHouseItems(numItemID, numWareHouseID,numOnHand,monWListPrice,vcLocation,vcWHSKU,vcBarCode,numDomainID)
	values(v_numItemCode,v_numWareHouseID,v_numOnHand,v_monWListPrice,v_vcLocation,v_vcWHSKU,v_vcBarCode,v_numDomainID);
	
      v_numWareHouseItemID :=  CURRVAL('WareHouseItems_seq');
	 --  numeric(9, 0)
	 --  numeric(9, 0)
	 --  tinyint
      open SWV_RefCur for
      select v_numWareHouseItemID;
      
	  PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numWareHouseItemID,v_numReferenceID := v_numItemCode,
      v_tintRefType := 1::SMALLINT,v_vcDescription := 'INSERT WareHouse',
      SWV_RefCur := null);
   else
	
      insert into WareHouseItmsDTL(numWareHouseItemID)
	values(v_numWareHouseItemID);
   end if;  
   open SWV_RefCur for
   select CURRVAL('Table_seq');
/****** Object:  StoredProcedure [dbo].[USP_BroadCastList]    Script Date: 07/26/2008 16:14:57 ******/
   RETURN;
END; $$;


