CREATE OR REPLACE FUNCTION usp_GetOppItemDetails(   
v_numOppId NUMERIC(9,0),
	v_numItemCode NUMERIC DEFAULT 0,
	v_numCountFlg NUMERIC DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numCount  NUMERIC(9,0);
   v_numUserCreatedby  NUMERIC(9,0);
   v_numTerId  NUMERIC(9,0);
BEGIN

	
	--Implementing the Security rights
   select   numCreatedBy INTO v_numUserCreatedby from OpportunityMaster where numOppId = v_numOppId;
	--SELECT @numusercreatedby as UserId	

   select   numTerID INTO v_numTerId from DivisionMaster where numDivisionID in(select numDivisionId from OpportunityMaster where numOppId = v_numOppId);
	--SELECT @numterid as TerritoryId

   IF v_numCountFlg = 0 then
		
      IF v_numItemCode = 0 then
				
         open SWV_RefCur for
         SELECT v_numTerId as TerritoryId,v_numUserCreatedby as UserId, a.*,b.vcItemName,b.txtItemDesc,b.numItemCode,b.charItemType FROM OpportunityItems a INNER JOIN Item b ON a.numItemCode = b.numItemCode
         WHERE a.numOppId = v_numOppId;
      ELSE
         open SWV_RefCur for
         SELECT v_numTerId as TerritoryId,v_numUserCreatedby as UserId, a.*,b.vcItemName,b.txtItemDesc,b.numItemCode,b.charItemType FROM OpportunityItems a INNER JOIN Item b ON a.numItemCode = b.numItemCode
         WHERE a.numOppId = v_numOppId
         AND a.numItemCode = v_numItemCode;
      end if;
   ELSE
			--SELECT @numcount=count(a.*),b.numitemcode FROM opportunityitems a,item b
			--			WHERE a.numitemcode=b.numitemcode and a.numoppid=@numoppid
      select   COUNT(*) INTO v_numCount from OpportunityItems WHERE numOppId = v_numOppId;
      open SWV_RefCur for
      SELECT v_numCount;
   end if;
   RETURN;
END; $$;


