-- Stored procedure definition script USP_DeleteShippingReportItem for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteShippingReportItem(v_ShippingReportItemId NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM ShippingReportItems
   WHERE ShippingReportItemId = v_ShippingReportItemId;
   RETURN;
END; $$;


