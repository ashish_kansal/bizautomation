-- Stored procedure definition script USP_tmpCalandarSave for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_tmpCalandarSave(v_strXml TEXT DEFAULT '' ,                       
v_ItemId  VARCHAR(250) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc4  INTEGER;
BEGIN
   if(select count(*) from tmpCalandarTbl where itemId = v_ItemId) = 0 then

      if SUBSTR(CAST(v_strXml AS VARCHAR(10)),1,10) <> '' then
         SELECT * INTO v_hDoc4 FROM SWF_Xml_PrepareDocument(v_strXml);
         insert into  tmpCalandarTbl(itemId,ItemIdOccur,StartDateTimeUtc,OccurIndex)
         select * from(SELECT * FROM SWF_OpenXml(v_hDoc4,'/NewDataSet/Table','ItemId |ItemIdOccur |StartDateTimeUtc |OccurIndex') SWA_OpenXml(ItemId VARCHAR(300),ItemIdOccur VARCHAR(300),StartDateTimeUtc VARCHAR(19),
            OccurIndex NUMERIC(9,0))) X;
         PERFORM SWF_Xml_RemoveDocument(v_hDoc4);
      end if;
   end if;
   RETURN;
END; $$;


