-- Stored procedure definition script usp_GetContactDTlPL for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetContactDTlPL(v_numContactID NUMERIC(9,0) DEFAULT 0  ,    
v_numDomainID NUMERIC(9,0) DEFAULT 0   ,  
v_ClientTimeZoneOffset INTEGER DEFAULT NULL                                    
--                                                            
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   A.numContactId,
A.vcFirstName,
A.vcLastname,
		A.vcFirstName || ' ' || A.vcLastname AS vcGivenName,
		A.numPhoneExtension As ContactPhoneExt,
		A.numPhone AS ContactPhone,
		A.numContactId AS ContactID,
		A.vcEmail AS Email,
 D.numDivisionID,
 C.numCompanyId ,
 C.vcCompanyName,
 D.vcDivisionName,
 D.numDomainID,
 D.tintCRMType,
 A.numPhone,
 A.numPhoneExtension,
 A.vcEmail,
 A.numTeam,
 A.numTeam as vcTeam,
GetListIemName(A.numTeam) as vcTeamName,
 A.vcFax,
 A.numContactType as vcContactType,
 A.numContactType,
GetListIemName(A.numContactType) as vcContactTypeName,
 A.charSex,
 A.bintDOB,
 GetAge(A.bintDOB,TIMEZONE('UTC',now())) as Age,
 GetListIemName(A.vcPosition) as vcPositionName,
 A.vcPosition,
 A.txtNotes,
 A.numCreatedBy,
 A.numCell,
 A.numHomePhone,
 A.VcAsstFirstName,
 A.vcAsstLastName,
 A.numAsstPhone,
 A.numAsstExtn,
 A.vcAsstEmail,
 A.charSex,
 A.vcLinkedinId,
  A.vcLinkedinUrl,
 case when A.charSex = 'M' then 'Male' when A.charSex = 'F' then 'Female' else  '-' end as charSexName,
GetListIemName(A.vcDepartment) as vcDepartmentName,
A.vcDepartment,                                                                 
 getContactAddress(A.numContactId) as Address,                                                                        
    A.bitOptOut,
 fn_GetContactName(A.numManagerID) as ManagerName,
 A.numManagerID   as   Manager,
GetListIemName(A.vcCategory) as vcCategoryName  ,
 A.vcCategory,
 fn_GetContactName(A.numCreatedBy) || ' ' || CAST(A.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) AS CreatedBy,
 fn_GetContactName(A.numModifiedBy) || ' ' || CAST(A.bintModifiedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) AS ModifiedBy,
 A.vcTitle,
 A.vcAltEmail,
 fn_GetContactName(A.numRecOwner) as RecordOwner,
 A.numEmpStatus,
GetListIemName(A.numEmpStatus::NUMERIC(9,0)) as numEmpStatusName,
(select  count(*) from GenericDocuments   where numRecID = A.numContactId and  vcDocumentSection = 'C') as DocumentCount,
(SELECT count(*) from CompanyAssociations where numDivisionID = A.numContactId and bitDeleted = false) as AssociateCountFrom,
(SELECT count(*) from CompanyAssociations where numAssociateFromDivisionID = A.numContactId and bitDeleted = false) as AssociateCountTo,

	cast((CASE WHEN
   coalesce((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
      WHERE ConECampaign.numContactID = A.numContactId AND  ConECampaign.numECampaignID = A.numECampaignID AND bitEngaged = true AND coalesce(bitSend,false) = true),0)
   = coalesce((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
      WHERE ConECampaign.numContactID = A.numContactId AND ConECampaign.numECampaignID = A.numECampaignID AND bitEngaged = true),0)
   THEN
      coalesce((SELECT CONCAT('<img alt="Campaign completed" height="16px" width="16px" title="Campaign Completed" src="../images/comflag.png"> ',vcECampName,' (' ||
         CAST(coalesce((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
            WHERE ConECampaign.numContactID = A.numContactId AND  ECampaign.numECampaignID = ConECampaign.numECampaignID AND bitEngaged = true AND coalesce(bitSend,false) = true),0) AS VARCHAR(30)) || ' Of ' ||
         CAST(coalesce((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
            WHERE ConECampaign.numContactID = A.numContactId AND ECampaign.numECampaignID = ConECampaign.numECampaignID AND bitEngaged = true),0) AS VARCHAR(30)) || ') ',
         '<a onclick="openDrip(' ||(cast(coalesce((SELECT cast(MAX(numConEmailCampID) as NUMERIC(18,0)) FROM ConECampaign WHERE numContactID = A.numContactId AND numECampaignID = A.numECampaignID AND coalesce(bitEngaged,false) = true),0) AS VARCHAR(30))) || ');">
		 <img alt="Follow-up Campaign history" height="16px" width="16px" title="Follow-up campaign history" src="../images/GLReport.png"
		 ></a>') FROM ECampaign WHERE numECampaignID = A.numECampaignID),'')
   ELSE
      coalesce((SELECT CONCAT('<img alt="Active Campaign" height="16px" width="16px" title="Active Campaign" src="../images/GreenFlag.png"> ',vcECampName,' (' ||
         CAST(coalesce((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
            WHERE ConECampaign.numContactID = A.numContactId AND  ECampaign.numECampaignID = ConECampaign.numECampaignID AND bitEngaged = true AND coalesce(bitSend,false) = true),0) AS VARCHAR(30)) || ' Of ' ||
         CAST(coalesce((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
            WHERE ConECampaign.numContactID = A.numContactId AND ECampaign.numECampaignID = ConECampaign.numECampaignID AND bitEngaged = true),0) AS VARCHAR(30)) || ') ',
         '<a onclick="openDrip(' ||(cast(coalesce((SELECT cast(MAX(numConEmailCampID) as NUMERIC(18,0)) FROM ConECampaign WHERE numContactID = A.numContactId AND numECampaignID = A.numECampaignID AND coalesce(bitEngaged,false) = true),0) AS VARCHAR(30))) || ');">
		 <img alt="Follow-up Campaign history" height="16px" width="16px" title="Follow-up campaign history" src="../images/GLReport.png"
		 ></a>') FROM ECampaign WHERE numECampaignID = A.numECampaignID),'')
   END) as VARCHAR(255)) AS vcECampName,

		----Fetch Last Followup(Template) and EmailSentDate(if any) Value
		  
----Fetch Last Followup(Template) and EmailSentDate(if any) Value
	(SELECT dtExecutionDate FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID = A.numContactId AND coalesce(A.numECampaignID,0) > 0 AND numConECampDTLID IN(SELECT cast(MAX(numConECampDTLID) as NUMERIC(18,0)) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID  WHERE ConECampaign.numContactID = A.numContactId AND coalesce(A.numECampaignID,0) > 0 AND coalesce(bitSend,false) = true)) AS vcLastFollowup,

		----Fetch Last Followup(Template) Value

		----Fetch Next Followup(Template) and ExecutionDate Value
	(CASE WHEN A.numECampaignID > 0 THEN fn_FollowupDetailsInOrgList(A.numContactId,2,v_numDomainID) ELSE '' END) AS vcNextFollowup,										 

		----Fetch Next Followup(Template) Value
---- Edited by Priya for Follow-up Changes (10 Jan 2018)


 coalesce(A.numECampaignID,0) AS numECampaignID,
 coalesce((SELECT cast(MAX(numConEmailCampID) as NUMERIC(18,0)) FROM ConECampaign WHERE numContactID = A.numContactId AND numECampaignID = A.numECampaignID AND coalesce(bitEngaged,false) = true),0) AS numConEmailCampID,
 coalesce(A.bitPrimaryContact,false) AS bitPrimaryContact,D.vcPartnerCode AS vcPartnerCode,
 coalesce(D.numPartenerSource,0)  AS numPartenerSourceId,
 (D3.vcPartnerCode || '-' || C3.vcCompanyName) as numPartenerSource,
 cast(coalesce(D.numPartenerContact,0) as NUMERIC(18,0)) AS numPartenerContactId,
 A1.vcFirstName || ' ' || A1.vcLastname AS numPartenerContact,
 coalesce((SELECT vcPassword FROM ExtranetAccountsDtl WHERE numContactID = CAST(A.numContactId AS VARCHAR)),'') AS vcPassword,coalesce(A.vcTaxID,'') AS vcTaxID
   FROM AdditionalContactsInformation A INNER JOIN
   DivisionMaster D ON A.numDivisionId = D.numDivisionID INNER JOIN
   CompanyInfo C ON D.numCompanyID = C.numCompanyId
   LEFT JOIN DivisionMaster D3 ON D.numPartenerSource = D3.numDivisionID
   LEFT JOIN CompanyInfo C3 ON C3.numCompanyId = D3.numCompanyID
   LEFT JOIN AdditionalContactsInformation A1 ON D.numPartenerContact = A1.numContactId
   left join UserMaster U on U.numUserDetailId = A.numContactId
   WHERE A.numContactId = v_numContactID and A.numDomainID = v_numDomainID;
END; $$;












