DROP FUNCTION IF EXISTS USP_OppManage;

CREATE OR REPLACE FUNCTION USP_OppManage(INOUT v_numOppID NUMERIC DEFAULT 0 ,                                                                          
  v_numContactId NUMERIC(9,0) DEFAULT null,                                                                          
  v_numDivisionId NUMERIC(9,0) DEFAULT null,                                                                          
  v_tintSource NUMERIC(9,0) DEFAULT null,                                                                          
  INOUT v_vcPOppName VARCHAR(100) DEFAULT '' ,                                                                          
  v_Comments VARCHAR(1000) DEFAULT '',                                                                          
  v_bitPublicFlag BOOLEAN DEFAULT false,                                                                          
  v_numUserCntID NUMERIC(9,0) DEFAULT 0,                                                                                 
  v_monPAmount DECIMAL(20,5) DEFAULT 0,                                                 
  v_numAssignedTo NUMERIC(9,0) DEFAULT 0,                                                                                                        
  v_numDomainId NUMERIC(9,0) DEFAULT 0,                                                                                                                                                                                   
  v_strItems TEXT DEFAULT null,                                                                          
  v_strMilestone TEXT DEFAULT null,                                                                          
  v_dtEstimatedCloseDate TIMESTAMP DEFAULT NULL,                                                                          
  v_CampaignID NUMERIC(9,0) DEFAULT null,                                                                          
  v_lngPConclAnalysis NUMERIC(9,0) DEFAULT null,                                                                         
  v_tintOppType SMALLINT DEFAULT NULL,                                                                                                                                             
  v_tintActive SMALLINT DEFAULT NULL,                                                              
  v_numSalesOrPurType NUMERIC(9,0) DEFAULT NULL,              
  v_numRecurringId NUMERIC(9,0) DEFAULT NULL,
  v_numCurrencyID NUMERIC(9,0) DEFAULT 0,
  v_DealStatus SMALLINT DEFAULT 0,-- 0=Open,1=Won,2=Lost
  v_numStatus NUMERIC(18,0) DEFAULT NULL,
  v_vcOppRefOrderNo VARCHAR(100) DEFAULT NULL,
  v_numBillAddressId NUMERIC(9,0) DEFAULT 0,
  v_numShipAddressId NUMERIC(9,0) DEFAULT 0,
  v_bitStockTransfer BOOLEAN DEFAULT false,
  v_WebApiId INTEGER DEFAULT 0,
  v_tintSourceType SMALLINT DEFAULT 0,
  v_bitDiscountType BOOLEAN DEFAULT NULL,
  v_fltDiscount NUMERIC DEFAULT NULL,
  v_bitBillingTerms BOOLEAN DEFAULT NULL,
  v_intBillingDays INTEGER DEFAULT NULL,
  v_bitInterestType BOOLEAN DEFAULT NULL,
  v_fltInterest NUMERIC DEFAULT NULL,
  v_tintTaxOperator SMALLINT DEFAULT NULL,
  v_numDiscountAcntType NUMERIC(9,0) DEFAULT NULL,
  v_vcWebApiOrderNo VARCHAR(100) DEFAULT NULL,
  v_vcCouponCode		VARCHAR(100) DEFAULT '',
  v_vcMarketplaceOrderID VARCHAR(100) DEFAULT NULL,
  v_vcMarketplaceOrderDate  TIMESTAMP DEFAULT NULL,
  v_vcMarketplaceOrderReportId VARCHAR(100) DEFAULT NULL,
  v_numPercentageComplete NUMERIC(9,0) DEFAULT NULL,
  v_bitUseShippersAccountNo BOOLEAN DEFAULT false,
  v_bitUseMarkupShippingRate BOOLEAN DEFAULT false,
  v_numMarkupShippingRate DECIMAL(20,5) DEFAULT 0,
  v_intUsedShippingCompany INTEGER DEFAULT 0,
  v_numShipmentMethod NUMERIC(18,0) DEFAULT 0,
  v_dtReleaseDate DATE DEFAULT NULL,
  v_numPartner NUMERIC(18,0) DEFAULT 0,
  v_tintClickBtn INTEGER DEFAULT 0,
  v_numPartenerContactId NUMERIC(18,0) DEFAULT 0,
  v_numAccountClass NUMERIC(18,0) DEFAULT 0,
  v_numWillCallWarehouseID NUMERIC(18,0) DEFAULT 0,
  v_numVendorAddressID NUMERIC(18,0) DEFAULT 0,
  v_numShipFromWarehouse NUMERIC(18,0) DEFAULT 0,
  v_numShippingService NUMERIC(18,0) DEFAULT 0,
  v_ClientTimeZoneOffset INTEGER DEFAULT 0,
  v_vcCustomerPO VARCHAR(100) DEFAULT NULL,
  v_bitDropShipAddress BOOLEAN DEFAULT false,
  v_PromCouponCode VARCHAR(100) DEFAULT NULL,
  v_numPromotionId NUMERIC(18,0) DEFAULT 0,
  v_dtExpectedDate TIMESTAMP DEFAULT null,
  v_numProjectID NUMERIC(18,0) DEFAULT 0
  -- USE PARAMETER WITH OPTIONAL VALUE BECAUSE PROCEDURE IS CALLED FROM OTHER PROCEDURE WHICH WILL NOT PASS NEW PARAMETER VALUE
)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_fltExchangeRate  DOUBLE PRECISION;                                 
   v_hDocItem  INTEGER;                                                                                                                            
   v_TotalAmount  DECIMAL(20,5);                                                                          
   v_tintOppStatus  SMALLINT;
   v_bitNewOrder  BOOLEAN DEFAULT(CASE WHEN coalesce(v_numOppID,0) = 0 THEN true ELSE false END);

	

   v_dtItemRelease  DATE;
   v_numCompany  NUMERIC(18,0);
   v_numRelationship  NUMERIC(18,0);
   v_numProfile  NUMERIC(18,0);
   v_tintCommitAllocation  SMALLINT;
   v_bitAllocateInventoryOnPickList  BOOLEAN;

   v_bitViolatePrice  BOOLEAN;
   v_intPendingApprovePending  INTEGER;
   v_numAddressID NUMERIC(18,0);
   v_vcStreet  VARCHAR(100);
   v_vcCity  VARCHAR(50);
   v_vcPostalCode  VARCHAR(15);
   v_vcCompanyName  VARCHAR(100);
   v_vcAddressName  VARCHAR(50);
   v_vcAltContact  VARCHAR(200);      
   v_numState  NUMERIC(9,0);
   v_numCountry  NUMERIC(9,0);
   v_numCompanyId  NUMERIC(9,0);
   v_numContact  NUMERIC(18,0);
   v_bitIsPrimary  BOOLEAN;
   v_bitAltContact  BOOLEAN;
   v_tintAddressOf  SMALLINT;

	my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   v_bitIsInitalSalesOrder  BOOLEAN;
   v_intOppTcode  NUMERIC(9,0);  
  
   v_numDiscountID  NUMERIC(18,0) DEFAULT NULL;
   v_tintPageID  SMALLINT;

   v_tintDefaultClassType  INTEGER DEFAULT 0;

   v_tempAssignedTo  NUMERIC(9,0) DEFAULT NULL;

   v_tintShipped  SMALLINT;               
   v_tintCRMType  NUMERIC(9,0);        
   v_AccountClosingDate  TIMESTAMP;
BEGIN
   IF coalesce(v_numOppID,0) = 0 AND coalesce(v_numDomainId,0) = 209 AND coalesce(v_vcOppRefOrderNo,'') <> '' AND coalesce(v_tintSourceType,0) = 4 then
	
      IF EXISTS(SELECT numOppId FROM OpportunityMaster WHERE numDomainId = 209 AND tintopptype = 1 AND vcOppRefOrderNo = v_vcOppRefOrderNo) then
		
         RAISE EXCEPTION 'CUSTOMERPO_ALREADY_EXISTS';
      end if;
   end if;
   IF coalesce(v_numContactId,0) = 0 then
	
      RAISE EXCEPTION 'CONTACT_REQUIRED';
   end if;
   IF coalesce(v_tintOppType,0) = 0 then
	
      RAISE EXCEPTION 'OPP_TYPE_CAN_NOT_BE_0';
   end if;
   IF coalesce(v_numOppID,0) > 0 AND EXISTS(SELECT numOppId FROM OpportunityMaster WHERE numDomainId = v_numDomainId AND numOppId = v_numOppID AND coalesce(tintshipped,0) = 1) then
	
      RAISE EXCEPTION 'OPP/ORDER_IS_CLOSED';
   end if;
   BEGIN
v_dtItemRelease := TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval);
   select   coalesce(tintCommitAllocation,1) INTO v_tintCommitAllocation FROM Domain WHERE numDomainId = v_numDomainId;
   select   D.numCompanyID, coalesce(bitAllocateInventoryOnPickList,false), numCompanyType, vcProfile INTO v_numCompany,v_bitAllocateInventoryOnPickList,v_numRelationship,v_numProfile FROM
   DivisionMaster D
   JOIN
   CompanyInfo C
   ON
   C.numCompanyId = D.numCompanyID WHERE
   D.numDivisionID = v_numDivisionId;
   IF v_numOppID = 0 AND SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then
	
		--VALIDATE PROMOTION OF COUPON CODE
      BEGIN
         CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMP CASCADE;
      CREATE TEMPORARY TABLE tt_TEMP
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         numOppItemID NUMERIC(18,0),
         numPromotionID NUMERIC(18,0)
      );
      INSERT INTO tt_TEMP(numOppItemID,
			numPromotionID)
      SELECT
			numoppitemtCode,
			numPromotionID
      FROM
		XMLTABLE
		(
			'NewDataSet/Item'
			PASSING 
				CAST(v_strItems AS XML)
			COLUMNS
				numoppitemtCode NUMERIC(18,0) PATH 'numoppitemtCode',
				numPromotionID NUMERIC(18,0) PATH 'numPromotionID',
				bitDisablePromotion BOOLEAN PATH 'bitDisablePromotion'
		) AS X 
      WHERE
      X.numPromotionID > 0
	  AND bitDisablePromotion = false;

      IF(SELECT COUNT(*) FROM tt_TEMP) > 0 then
		
			-- IF ITEM PROMOTION USED IN ORDER EXIPIRES THAN RAISE ERROR
         IF(SELECT
         COUNT(PO.numProId)
         FROM
         PromotionOffer PO
         LEFT JOIN
         PromotionOffer POOrder
         ON
         PO.numOrderPromotionID = POOrder.numProId
         INNER JOIN
         tt_TEMP T1
         ON
         PO.numProId = T1.numPromotionID
         WHERE
         PO.numDomainId = v_numDomainId
         AND coalesce(PO.IsOrderBasedPromotion,false) = false
         AND coalesce(PO.bitEnabled,false) = true
         AND 1 =(CASE
         WHEN coalesce(PO.numOrderPromotionID,0) > 0
         THEN(CASE WHEN POOrder.bitNeverExpires = true THEN 1 ELSE(CASE WHEN TIMEZONE('UTC',now()) >= POOrder.dtValidFrom AND TIMEZONE('UTC',now()) <= POOrder.dtValidTo THEN 1 ELSE 0 END) END)
         ELSE(CASE WHEN PO.bitNeverExpires = true THEN 1 ELSE(CASE WHEN TIMEZONE('UTC',now()) >= PO.dtValidFrom AND TIMEZONE('UTC',now()) <= PO.dtValidTo THEN 1 ELSE 0 END) END)
         END)
         AND 1 =(CASE
         WHEN coalesce(PO.numOrderPromotionID,0) > 0
         THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = PO.numOrderPromotionID AND tintType = 2 AND numRelationship = v_numRelationship AND numProfile = v_numProfile) > 0 THEN 1 ELSE 0 END)
         ELSE(CASE PO.tintCustomersBasedOn
            WHEN 1 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = PO.numProId AND tintType = 1 AND numDivisionID = v_numDivisionId) > 0 THEN 1 ELSE 0 END)
            WHEN 2 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = PO.numProId AND tintType = 2 AND numRelationship = v_numRelationship AND numProfile = v_numProfile) > 0 THEN 1 ELSE 0 END)
            WHEN 3 THEN 1
            ELSE 0
            END)
         END)) <>(SELECT COUNT(*) FROM tt_TEMP) then
			
            RAISE EXCEPTION 'ITEM_PROMOTION_EXPIRED';
            RETURN;
         end if;

			-- NOW IF COUPON BASED ITEM PROMOTION IS USED THEN VALIDATE COUPON AND ITS USAGE
         IF(SELECT
         COUNT(*)
         FROM
         PromotionOffer PO
         INNER JOIN
         tt_TEMP T1
         ON
         PO.numProId = T1.numPromotionID
         WHERE
         PO.numDomainId = v_numDomainId
         AND coalesce(IsOrderBasedPromotion,false) = false
         AND coalesce(numOrderPromotionID,0) > 0) = 1 then
			
            IF coalesce(v_PromCouponCode,cast(0 as TEXT)) <> '' then
				
               IF(SELECT
               COUNT(*)
               FROM
               PromotionOffer PO
               INNER JOIN
               PromotionOffer POOrder
               ON
               PO.numOrderPromotionID = POOrder.numProId
               INNER JOIN
               tt_TEMP T1
               ON
               PO.numProId = T1.numPromotionID
               INNER JOIN
               DiscountCodes DC
               ON
               POOrder.numProId = DC.numPromotionID
               WHERE
               PO.numDomainId = v_numDomainId
               AND coalesce(PO.IsOrderBasedPromotion,false) = false
               AND coalesce(DC.vcDiscountCode,'') = v_PromCouponCode) = 0 then
					
                  RAISE EXCEPTION 'INVALID_COUPON_CODE_ITEM_PROMOTION';
                  RETURN;
               ELSE
						-- IF THERE IS COUPON CODE USAGE LIMIT THAN VALIDATE USAGE LIMIT
                  IF(SELECT
                  COUNT(*)
                  FROM
                  PromotionOffer PO
                  INNER JOIN
                  PromotionOffer POOrder
                  ON
                  PO.numOrderPromotionID = POOrder.numProId
                  INNER JOIN
                  tt_TEMP T1
                  ON
                  PO.numProId = T1.numPromotionID
                  INNER JOIN
                  DiscountCodes DC
                  ON
                  POOrder.numProId = DC.numPromotionID
                  WHERE
                  PO.numDomainId = v_numDomainId
                  AND coalesce(PO.IsOrderBasedPromotion,false) = false
                  AND coalesce(DC.vcDiscountCode,'') = v_PromCouponCode
                  AND 1 =(CASE WHEN coalesce(DC.CodeUsageLimit,0) <> 0 THEN 1 ELSE 0 END)
                  AND coalesce((SELECT intCodeUsed FROM DiscountCodeUsage WHERE DiscountCodeUsage.numDiscountId = DC.numDiscountId AND numDivisionId = v_numDivisionId),
                  0) >= DC.CodeUsageLimit) > 0 then
						
                     RAISE EXCEPTION 'ITEM_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED';
                     RETURN;
                  end if;
               end if;
            ELSE
               RAISE EXCEPTION 'COUPON_CODE_REQUIRED_ITEM_PROMOTION';
               RETURN;
            end if;
         end if;
      end if;
   end if;

   --INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
    DROP TABLE IF EXISTS tt_TEMPKITCONFIGURATION CASCADE;
    CREATE TEMPORARY TABLE tt_TEMPKITCONFIGURATION
    (
		numOppItemID NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		numKitItemID NUMERIC(18,0),
		numKitChildItemID NUMERIC(18,0),
		numQty DOUBLE PRECISION,
		numSequence INTEGER
    );

	--IF NEW OPPERTUNITY
   IF v_numOppID = 0 then
      IF(v_DealStatus = 1 AND v_tintOppType = 1) then
		
         v_bitIsInitalSalesOrder := true;
      end if;
      select   MAX(numOppId) INTO v_intOppTcode FROM OpportunityMaster;
      v_intOppTcode := coalesce(v_intOppTcode,0)+1;
      v_vcPOppName := coalesce(v_vcPOppName,'') || '-' || CAST(EXTRACT(YEAR FROM TIMEZONE('UTC',now())) AS VARCHAR(4))  || '-A' || SUBSTR(CAST(v_intOppTcode AS VARCHAR(10)),1,10);
      IF coalesce(v_numCurrencyID,0) = 0 then
		
         v_fltExchangeRate := 1;
         select   coalesce(numCurrencyID,0) INTO v_numCurrencyID FROM Domain WHERE numDomainId = v_numDomainId;
      ELSE
         v_fltExchangeRate := GetExchangeRate(v_numDomainId,v_numCurrencyID);
      end if;
      IF coalesce(v_numAccountClass,0) = 0 then
         select   coalesce(tintDefaultClassType,0) INTO v_tintDefaultClassType FROM Domain WHERE numDomainId = v_numDomainId;
         IF v_tintDefaultClassType = 1 then --USER
		
            select   coalesce(numDefaultClass,0) INTO v_numAccountClass FROM
            UserMaster UM
            JOIN
            Domain D
            ON
            UM.numDomainID = D.numDomainId WHERE
            D.numDomainId = v_numDomainId
            AND UM.numUserDetailId = v_numUserCntID;
         ELSEIF v_tintDefaultClassType = 2
         then --COMPANY
		
            select   coalesce(numAccountClassID,0) INTO v_numAccountClass FROM
            DivisionMaster DM WHERE
            DM.numDomainID = v_numDomainId
            AND DM.numDivisionID = v_numDivisionId;
         ELSE
            v_numAccountClass := 0;
         end if;
      end if;
      IF(v_DealStatus = 1 AND v_tintOppType = 1 AND v_tintClickBtn = 1) then
		
         v_DealStatus := 0;
      end if;         
		--PRINT ('Deal Staus ' +@DealStatus)  

		-------- VALIDATE ORDER BASED PROMOTION -------------

      IF v_numPromotionId > 0 then
		
			-- VALIDTE IF ITS ORDER BASED PROMOTION (BOTH ITEM AND ORDER BASED PROMOTION CAN HAVE COUPON CODE)
         IF EXISTS(SELECT numProId FROM PromotionOffer WHERE numDomainId = v_numDomainId AND numProId = v_numPromotionId AND coalesce(IsOrderBasedPromotion,false) = true) then
			
				-- CHECK IF ORDER PROMOTION IS STILL VALID
            IF NOT EXISTS(SELECT
            PO.numProId
            FROM
            PromotionOffer PO
            INNER JOIN
            PromotionOfferOrganizations PORG
            ON
            PO.numProId = PORG.numProId
            WHERE
            numDomainId = v_numDomainId
            AND PO.numProId = v_numPromotionId
            AND coalesce(IsOrderBasedPromotion,false) = true
            AND coalesce(bitEnabled,false) = true
            AND 1 =(CASE WHEN bitNeverExpires = true THEN 1 ELSE(CASE WHEN TIMEZONE('UTC',now()) >= dtValidFrom AND TIMEZONE('UTC',now()) <= dtValidTo THEN 1 ELSE 0 END) END)
            AND numRelationship = v_numRelationship
            AND numProfile = v_numProfile) then
				
               RAISE EXCEPTION 'ORDER_PROMOTION_EXPIRED';
               RETURN;
            ELSEIF coalesce((SELECT coalesce(bitRequireCouponCode,false) FROM PromotionOffer WHERE numDomainId = v_numDomainId AND numProId = v_numPromotionId),false) = true
            then
				
               IF coalesce(v_PromCouponCode,cast(0 as TEXT)) <> '' then
					
                  IF(SELECT
                  COUNT(*)
                  FROM
                  PromotionOffer PO
                  INNER JOIN
                  DiscountCodes DC
                  ON
                  PO.numProId = DC.numPromotionID
                  WHERE
                  PO.numDomainId = v_numDomainId
                  AND PO.numProId = v_numPromotionId
                  AND coalesce(IsOrderBasedPromotion,false) = true
                  AND coalesce(bitRequireCouponCode,false) = true
                  AND coalesce(DC.vcDiscountCode,'') = v_PromCouponCode) = 0 then
						
                     RAISE EXCEPTION 'INVALID_COUPON_CODE_ORDER_PROMOTION';
                     RETURN;
                  ELSE
							-- IF THERE IS COUPON CODE USAGE LIMIT THAN VALIDATE USAGE LIMIT
                     IF(SELECT
                     COUNT(*)
                     FROM
                     PromotionOffer PO
                     INNER JOIN
                     DiscountCodes DC
                     ON
                     PO.numProId = DC.numPromotionID
                     WHERE
                     PO.numDomainId = v_numDomainId
                     AND PO.numProId = v_numPromotionId
                     AND coalesce(IsOrderBasedPromotion,false) = true
                     AND coalesce(bitRequireCouponCode,false) = true
                     AND coalesce(DC.vcDiscountCode,'') = v_PromCouponCode
                     AND 1 =(CASE WHEN coalesce(DC.CodeUsageLimit,0) <> 0 THEN 1 ELSE 0 END)
                     AND coalesce((SELECT intCodeUsed FROM DiscountCodeUsage WHERE DiscountCodeUsage.numDiscountId = DC.numDiscountId AND numDivisionId = v_numDivisionId),
                     0) >= DC.CodeUsageLimit) > 0 then
							
                        RAISE EXCEPTION 'ORDER_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED';
                        RETURN;
                     ELSE
                        select   numDiscountId INTO v_numDiscountID FROM
                        DiscountCodes DC WHERE
                        DC.numPromotionID = v_numPromotionId
                        AND coalesce(DC.vcDiscountCode,'') = v_PromCouponCode;
                        IF EXISTS(SELECT DC.numDiscountId FROM DiscountCodes DC INNER JOIN DiscountCodeUsage DCU ON DC.numDiscountId = DCU.numDiscountId WHERE DC.numPromotionID = v_numPromotionId AND DCU.numDivisionId = v_numDivisionId AND coalesce(DC.vcDiscountCode,'') = v_PromCouponCode) then
								
                           UPDATE
                           DiscountCodeUsage DCU
                           SET
                           intCodeUsed = coalesce(DCU.intCodeUsed,0)+1
                           FROM
                           DiscountCodes DC
                           WHERE
                           DC.numDiscountId = DCU.numDiscountId AND(DC.numPromotionID = v_numPromotionId
                           AND DCU.numDivisionId = v_numDivisionId
                           AND coalesce(DC.vcDiscountCode,'') = v_PromCouponCode);
                        ELSE
                           INSERT INTO DiscountCodeUsage(numDiscountId
										,numDivisionId
										,intCodeUsed)
                           SELECT
                           DC.numDiscountId
										,v_numDivisionId
										,1
                           FROM
                           DiscountCodes DC
                           WHERE
                           DC.numPromotionID = v_numPromotionId
                           AND coalesce(DC.vcDiscountCode,'') = v_PromCouponCode;
                        end if;
                     end if;
                  end if;
               ELSE
                  RAISE EXCEPTION 'COUPON_CODE_REQUIRED_ORDER_PROMOTION';
                  RETURN;
               end if;
            end if;
         end if;
      end if;

		-------- ORDER BASED Promotion Logic-------------
		                                                    
      INSERT INTO OpportunityMaster(numContactId,numDivisionId,txtComments,numCampainID,bitPublicFlag,tintSource,tintSourceType,vcpOppName,
			intPEstimatedCloseDate,monPAmount,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,
			numrecowner,lngPConclAnalysis,tintopptype,numSalesOrPurType,numCurrencyID,fltExchangeRate,numassignedto,
			numAssignedBy,tintoppstatus,numStatus,vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,
			bitInterestType,fltInterest,vcCouponCode,bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,
			vcMarketplaceOrderReportId,numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
			bitUseMarkupShippingRate,numMarkupShippingRate,intUsedShippingCompany,numShipmentMethod,numShippingService,numPartner,numPartenerContact,dtReleaseDate,bitIsInitalSalesOrder,numVendorAddressID,numShipFromWarehouse
			,"vcCustomerPO#",bitDropShipAddress,numDiscountID,dtExpectedDate,numProjectID)
		Values(v_numContactId,v_numDivisionId,coalesce(v_Comments, ''),v_CampaignID,v_bitPublicFlag,v_tintSource,v_tintSourceType,v_vcPOppName,
			v_dtEstimatedCloseDate,v_monPAmount,v_numUserCntID,(CASE v_tintSourceType WHEN 3 THEN v_vcMarketplaceOrderDate ELSE TIMEZONE('UTC',now()) END),
			v_numUserCntID,TIMEZONE('UTC',now()),v_numDomainId,v_numUserCntID,v_lngPConclAnalysis,v_tintOppType,v_numSalesOrPurType,v_numCurrencyID,
			v_fltExchangeRate,(CASE WHEN v_numAssignedTo > 0 THEN v_numAssignedTo ELSE NULL END),(CASE WHEN v_numAssignedTo > 0 THEN v_numUserCntID ELSE NULL END),
			v_DealStatus,v_numStatus,v_vcOppRefOrderNo,v_bitStockTransfer,v_bitBillingTerms,v_intBillingDays,v_bitInterestType,v_fltInterest,
			v_PromCouponCode,v_bitDiscountType,v_fltDiscount,true,v_vcMarketplaceOrderID,v_vcMarketplaceOrderReportId,v_numPercentageComplete,
			v_numAccountClass,v_tintTaxOperator,v_bitUseShippersAccountNo,v_bitUseMarkupShippingRate,v_numMarkupShippingRate,v_intUsedShippingCompany,v_numShipmentMethod,v_numShippingService,v_numPartner,v_numPartenerContactId,v_dtReleaseDate,v_bitIsInitalSalesOrder,v_numVendorAddressID,v_numShipFromWarehouse
			,v_vcCustomerPO,v_bitDropShipAddress,v_numDiscountID,v_dtExpectedDate,v_numProjectID);
		
      v_numOppID := CURRVAL('OpportunityMaster_seq');
      PERFORM usp_OppDefaultAssociateContacts(v_numOppID := v_numOppID,v_numDomainId := v_numDomainId);                    
		--UPDATE OPPNAME AS PER NAME TEMPLATE
      PERFORM USP_UpdateNameTemplateValue(v_tintOppType::SMALLINT,v_numDomainId,v_numOppID);
      select   coalesce(vcpOppName,'') INTO v_vcPOppName FROM OpportunityMaster WHERE numOppId = v_numOppID;

		--MAP CUSTOM FIELD	
		 --  numeric(18, 0)
				 --  numeric(18, 0)
				 --  numeric(18, 0)
      v_tintPageID := CASE WHEN v_tintOppType = 1 THEN 2 ELSE 6 END;
      PERFORM USP_AddParentChildCustomFieldMap(v_numDomainId := v_numDomainId,v_numRecordID := v_numOppID,v_numParentRecId := v_numDivisionId,
      v_tintPageID := v_tintPageID::SMALLINT); --  tinyint
 	
      IF coalesce(v_numStatus,0) > 0 AND v_tintOppType = 1 AND coalesce(v_DealStatus,0) = 1 then
		
	   		 -- numeric(18, 0)
					 -- numeric(18, 0)
					 -- numeric(18, 0)
					 -- numeric(18, 0)
					 -- numeric(18, 0)
					 -- numeric(18, 0)
					 -- tinyint
         PERFORM USP_ManageOpportunityAutomationQueue(v_numOppQueueID := 0,v_numDomainId := v_numDomainId,v_numOppID := v_numOppID,
         v_numOppBizDocsId := 0,v_numOrderStatus := v_numStatus,v_numUserCntID := v_numUserCntID,
         v_tintProcessStatus := 1::SMALLINT,v_tintMode := 1::SMALLINT);
      end if;
  
		-- INSERTING ITEMS                                                                                                                    
      IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' AND v_numOppID > 0 then
		
         INSERT INTO OpportunityItems(numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,
				monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
				numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,vcAttrValues,monAvgCost,bitItemPriceApprovalRequired,
				numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,dtPlannedStart,numWOAssignedTo,numShipToAddressID,ItemReleaseDate
				,bitMarkupDiscount,vcChildKitSelectedItems,numWOQty,bitMappingRequired,vcSKU,vcASIN,bitDisablePromotion)
         SELECT
         v_numOppID,X.numItemCode,fn_UOMConversion(X.numUOM,numItemCode,v_numDomainId,null::NUMERIC)*x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
				(CASE X.numWarehouseItmsID
         WHEN -1
         THEN
            CASE
            WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID = v_numDomainId AND vcSKU = X.vcSKU) > 0)
            THEN(SELECT 
                  numWareHouseItemID
                  FROM
                  WareHouseItems
                  WHERE
                  numItemID = X.numItemCode
                  AND numWareHouseID IN(SELECT  coalesce(numwarehouseID,0) AS numDefaultWareHouseID FROM WebAPIDetail WHERE numDomainID = v_numDomainId AND WebApiId = v_WebApiId LIMIT 1) LIMIT 1)
            ELSE(SELECT
                  numWareHouseItemID
                  FROM
                  WareHouseItems
                  WHERE
                  numItemID = X.numItemCode
                  AND coalesce(WareHouseItems.vcWHSKU,'') = coalesce(X.vcSKU,'')
                  AND numWareHouseID IN(SELECT  coalesce(numwarehouseID,0) AS numDefaultWareHouseID FROM WebAPIDetail WHERE numDomainID = v_numDomainId AND WebApiId = v_WebApiId LIMIT 1) LIMIT 1)
            END
         WHEN 0
         THEN
            CASE
            WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID = v_numDomainId AND vcSKU = X.vcSKU) > 0)
            THEN(SELECT 
                  numWareHouseItemID
                  FROM
                  WareHouseItems
                  WHERE
                  numItemID = X.numItemCode
                  AND numWareHouseID IN(SELECT  coalesce(numwarehouseID,0) AS numDefaultWareHouseID FROM WebAPIDetail WHERE numDomainID = v_numDomainId AND WebApiId = v_WebApiId LIMIT 1) LIMIT 1)
            ELSE(SELECT
                  numWareHouseItemID
                  FROM
                  WareHouseItems
                  WHERE
                  numItemID = X.numItemCode
                  AND coalesce(WareHouseItems.vcWHSKU,'') = coalesce(X.vcSKU,'')
                  AND numWareHouseID IN(SELECT  coalesce(numwarehouseID,0) AS numDefaultWareHouseID FROM WebAPIDetail WHERE numDomainID = v_numDomainId AND WebApiId = v_WebApiId LIMIT 1) LIMIT 1)
            END
         ELSE
            X.numWarehouseItmsID
         END) AS numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM Item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM Item WHERE numItemCode = X.numItemCode)
				,(SELECT  vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND bitDefault = true AND numDomainId = v_numDomainId LIMIT 1)
				,fn_FindVendorCost(X.numItemCode,v_numDivisionId,v_numDomainId,fn_UOMConversion(X.numUOM,numItemCode,v_numDomainId,null::NUMERIC)*x.numUnitHour),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,
				0,
				X.numToWarehouseItemID,X.Attributes,X.AttributeIDs,(SELECT(CASE WHEN coalesce(I.bitVirtualInventory,false) = true THEN 0 ELSE coalesce(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainID = v_numDomainId AND I.numItemCode = X.numItemCode),
				X.bitItemPriceApprovalRequired,X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,coalesce(X.numCost,0),coalesce(X.vcVendorNotes,'')
				,X.vcInstruction,X.bintCompliationDate+CAST(v_ClientTimeZoneOffset || 'minute' as interval),X.dtPlannedStart+CAST(v_ClientTimeZoneOffset || 'minute' as interval),X.numWOAssignedTo,X.numShipToAddressID
				,coalesce(ItemReleaseDate,v_dtItemRelease),X.bitMarkupDiscount,coalesce(X.KitChildItems,''),X.numWOQty,coalesce(X.bitMappingRequired,false),X.vcSKU,X.vcASIN,X.bitDisablePromotion
         FROM
		 XMLTABLE
			(
				'NewDataSet/Item'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					Op_Flag SMALLINT PATH 'Op_Flag'
					,numoppitemtCode NUMERIC(18,0) PATH 'numoppitemtCode'
					,numItemCode NUMERIC(9,0) PATH 'numItemCode'
					,numUnitHour DOUBLE PRECISION PATH 'numUnitHour'
					,monPrice DECIMAL(30,16) PATH 'monPrice'
					,monTotAmount DECIMAL(20,5) PATH 'monTotAmount'
					,vcItemDesc VARCHAR(4000) PATH 'vcItemDesc'
					,numWarehouseItmsID NUMERIC(9,0) PATH 'numWarehouseItmsID'
					,ItemType VARCHAR(30) PATH 'ItemType'
					,DropShip BOOLEAN PATH 'DropShip'
					,bitDiscountType BOOLEAN PATH 'bitDiscountType'
					,fltDiscount DECIMAL(30,16) PATH 'fltDiscount'
					,monTotAmtBefDiscount DECIMAL(20,5) PATH 'monTotAmtBefDiscount'
					,vcItemName VARCHAR(300) PATH 'vcItemName'
					,numUOM NUMERIC(9,0) PATH 'numUOM'
					,bitWorkOrder BOOLEAN PATH 'bitWorkOrder'
					,numVendorWareHouse NUMERIC(9,0) PATH 'numVendorWareHouse'
					,numShipmentMethod NUMERIC(9,0) PATH 'numShipmentMethod'
					,numSOVendorId NUMERIC(9,0) PATH 'numSOVendorId'
					,numProjectID NUMERIC(9,0) PATH 'numProjectID'
					,numProjectStageID NUMERIC(9,0) PATH 'numProjectStageID'
					,numToWarehouseItemID NUMERIC(9,0) PATH 'numToWarehouseItemID'
					,Attributes VARCHAR(500) PATH 'Attributes'
					,AttributeIDs VARCHAR(500) PATH 'AttributeIDs'
					,vcSKU VARCHAR(100) PATH 'vcSKU'
					,bitItemPriceApprovalRequired BOOLEAN PATH 'bitItemPriceApprovalRequired'
					,numPromotionID NUMERIC(18,0) PATH 'numPromotionID'
					,bitPromotionTriggered BOOLEAN PATH 'bitPromotionTriggered'
					,vcPromotionDetail VARCHAR(2000) PATH 'vcPromotionDetail'
					,numSortOrder NUMERIC(18,0) PATH 'numSortOrder'
					,numCost DECIMAL(20,5) PATH 'numCost'
					,vcVendorNotes VARCHAR(4000) PATH 'vcVendorNotes'
					,vcInstruction VARCHAR(1000) PATH 'vcInstruction'
					,bintCompliationDate TIMESTAMP PATH 'bintCompliationDate'
					,dtPlannedStart TIMESTAMP PATH 'dtPlannedStart'
					,numWOAssignedTo NUMERIC(18,0) PATH 'numWOAssignedTo'
					,numShipToAddressID NUMERIC(18,0) PATH 'numShipToAddressID'
					,ItemReleaseDate TIMESTAMP PATH 'ItemReleaseDate'
					,bitMarkupDiscount BOOLEAN PATH 'bitMarkupDiscount'
					,KitChildItems TEXT PATH 'KitChildItems'
					,numWOQty DOUBLE PRECISION PATH 'numWOQty'
					,bitMappingRequired BOOLEAN PATH 'bitMappingRequired'
					,vcASIN VARCHAR(100) PATH 'vcASIN'
					,bitDisablePromotion BOOLEAN PATH 'bitDisablePromotion'
			) AS X 
		WHERE
			X.Op_Flag=1
        ORDER BY
			X.numoppitemtCode;
			
    
			-- UPDATE EXISTING CUSTOMER PART NO
         UPDATE
         CustomerPartNumber CPN
         SET
         CustomerPartNo = coalesce(TEMPCPN.CustomerPartNo,'')
         FROM(SELECT
         X.numItemCode
					,MIN(X.CustomerPartNo) AS CustomerPartNo
         FROM
		 XMLTABLE
			(
				'NewDataSet/Item'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					Op_Flag SMALLINT PATH 'Op_Flag'
					,CustomerPartNo VARCHAR(300) PATH 'CustomerPartNo'
					,numItemCode NUMERIC(18,0) PATH 'numItemCode'
			) AS X 
		WHERE
			X.Op_Flag=1
         GROUP BY
          X.numItemCode) AS TEMPCPN
         WHERE(CPN.numCompanyId = v_numCompany
         AND CPN.numItemCode = TEMPCPN.numItemCode) AND(CPN.numDomainID = v_numDomainId
         AND CPN.numCompanyId = v_numCompany
         AND LENGTH(coalesce(TEMPCPN.CustomerPartNo,'')) > 0
         AND coalesce(TEMPCPN.CustomerPartNo,'') <> '0');
			
			-- INSERT NEW CUSTOMER PART NO
         INSERT INTO CustomerPartNumber(numCompanyId, numDomainID, CustomerPartNo, numItemCode)
         SELECT
         v_numCompany, v_numDomainId, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
         FROM(SELECT
         X.numItemCode
					,MIN(X.CustomerPartNo) AS CustomerPartNo
         FROM
		 XMLTABLE
			(
				'NewDataSet/Item'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					Op_Flag SMALLINT PATH 'Op_Flag'
					,CustomerPartNo VARCHAR(300) PATH 'CustomerPartNo'
					,numItemCode NUMERIC(18,0) PATH 'numItemCode'
			) AS X 
		WHERE
			X.Op_Flag=1
         GROUP BY
          X.numItemCode) TEMPCPN
         LEFT JOIN
         CustomerPartNumber CPN
         ON
         CPN.numCompanyId = v_numCompany
         AND CPN.numItemCode = TEMPCPN.numItemCode
         WHERE
         CPN.CustomerPartNoID IS NULL
         AND LENGTH(coalesce(TEMPCPN.CustomerPartNo,'')) > 0
         AND coalesce(TEMPCPN.CustomerPartNo,'') <> '0';

         UPDATE
         OpportunityItems
         SET
         numItemCode = X.numItemCode,numOppId = v_numOppID,numUnitHour = fn_UOMConversion(X.numUOM,X.numItemCode,v_numDomainId,null::NUMERIC)*X.numUnitHour,
         monPrice = x.monPrice,monTotAmount = x.monTotAmount,vcItemDesc = X.vcItemDesc,
         numWarehouseItmsID = X.numWarehouseItmsID,bitDropShip = X.DropShip,bitDiscountType = X.bitDiscountType,
         fltDiscount = X.fltDiscount,monTotAmtBefDiscount = X.monTotAmtBefDiscount,
         vcItemName = X.vcItemName,numUOMId = numUOM,
         bitWorkOrder = X.bitWorkOrder,numVendorWareHouse = X.numVendorWareHouse,
         numShipmentMethod = X.numShipmentMethod,numSOVendorId = X.numSOVendorId,numProjectID = X.numProjectID,
         numProjectStageID = X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,
         vcAttributes = X.Attributes,numClassID = 0,
         vcModelID = X.vcModelID,bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired,
         vcAttrValues = X.AttributeIDs,numPromotionID = X.numPromotionID,
         bitPromotionTriggered = X.bitPromotionTriggered,vcPromotionDetail = X.vcPromotionDetail,
         numSortOrder = X.numSortOrder,numCost = X.numCost,
         vcNotes = X.vcVendorNotes,vcInstruction = X.vcInstruction,bintCompliationDate = X.bintCompliationDate+CAST(v_ClientTimeZoneOffset || 'minute' as interval),
         dtPlannedStart = X.dtPlannedStart+CAST(v_ClientTimeZoneOffset || 'minute' as interval),numWOAssignedTo = X.numWOAssignedTo,ItemRequiredDate = X.ItemRequiredDate,
         bitMarkupDiscount = X.bitMarkupDiscount,vcChildKitSelectedItems = coalesce(X.KitChildItems,''),bitDisablePromotion=X.bitDisablePromotion
         FROM(SELECT
         numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,
					monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,
					Attributes,bitItemPriceApprovalRequired,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,coalesce(numCost,0) AS numCost
					,coalesce(vcVendorNotes,'') AS vcVendorNotes,vcInstruction,bintCompliationDate,dtPlannedStart,numWOAssignedTo,ItemRequiredDate,bitMarkupDiscount,KitChildItems,bitDisablePromotion
         FROM
		 XMLTABLE
			(
				'NewDataSet/Item'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					Op_Flag SMALLINT PATH 'Op_Flag'
					,numoppitemtCode NUMERIC(9,0) PATH 'numoppitemtCode'
					 ,numItemCode NUMERIC(9,0) PATH 'numItemCode'
					 ,numUnitHour DOUBLE PRECISION PATH 'numUnitHour'
					 ,monPrice DECIMAL(30,16) PATH 'monPrice'
					 ,monTotAmount DECIMAL(20,5) PATH 'monTotAmount'
					 ,vcItemDesc VARCHAR(4000) PATH 'vcItemDesc'
					 ,vcModelID VARCHAR(300) PATH 'vcModelID'
					 ,numWarehouseItmsID NUMERIC(9,0) PATH 'numWarehouseItmsID'
					 ,DropShip BOOLEAN PATH 'DropShip'
					 ,bitDiscountType BOOLEAN PATH 'bitDiscountType'
					 ,fltDiscount DECIMAL(30,16) PATH 'fltDiscount'
					 ,monTotAmtBefDiscount DECIMAL(20,5) PATH 'monTotAmtBefDiscount'
					 ,vcItemName VARCHAR(300) PATH 'vcItemName'
					 ,numUOM NUMERIC(9,0) PATH 'numUOM'
					 ,bitWorkOrder BOOLEAN PATH 'bitWorkOrder'
					 ,numVendorWareHouse NUMERIC(9,0) PATH 'numVendorWareHouse'
					 ,numShipmentMethod NUMERIC(9,0) PATH 'numShipmentMethod'
					 ,numSOVendorId NUMERIC(9,0) PATH 'numSOVendorId'
					 ,numProjectID NUMERIC(9,0) PATH 'numProjectID'
					 ,numProjectStageID NUMERIC(9,0) PATH 'numProjectStageID'
					 ,numToWarehouseItemID NUMERIC(9,0) PATH 'numToWarehouseItemID'
					 ,Attributes VARCHAR(500) PATH 'Attributes'
					 ,bitItemPriceApprovalRequired BOOLEAN PATH 'bitItemPriceApprovalRequired'
					 ,AttributeIDs VARCHAR(500) PATH 'AttributeIDs'
					 ,numPromotionID NUMERIC(18,0) PATH 'numPromotionID'
					 ,bitPromotionTriggered BOOLEAN PATH 'bitPromotionTriggered'
					 ,vcPromotionDetail VARCHAR(2000) PATH 'vcPromotionDetail'
					 ,numSortOrder NUMERIC(18,0) PATH 'numSortOrder'
					 ,numCost DECIMAL(20,5) PATH 'numCost'
					 ,vcVendorNotes VARCHAR(4000) PATH 'vcVendorNotes'
					 ,vcInstruction VARCHAR(1000) PATH 'vcInstruction'
					 ,bintCompliationDate TIMESTAMP PATH 'bintCompliationDate'
					 ,dtPlannedStart TIMESTAMP PATH 'dtPlannedStart'
					 ,numWOAssignedTo NUMERIC(18,0) PATH 'numWOAssignedTo'
					 ,ItemRequiredDate TIMESTAMP PATH 'ItemRequiredDate'
					 ,bitMarkupDiscount BOOLEAN PATH 'bitMarkupDiscount'
					 ,KitChildItems TEXT  PATH 'KitChildItems'
					 ,bitDisablePromotion BOOLEAN PATH 'bitDisablePromotion'
			) AS TEMP
			WHERE
			TEMP.Op_Flag=2
		) X 
		WHERE
			OpportunityItems.numoppitemtCode = X.numOppItemID 
			AND OpportunityItems.numOppId = v_numOppID;
				
         INSERT INTO tt_TEMPKITCONFIGURATION(numOppItemID,
				numItemCode,
				numKitItemID,
				numKitChildItemID,
				numQty,
				numSequence)
         SELECT
         numoppitemtCode,
				numItemCode,
				numKitItemID,
				numChildItemID,
				numQty,
				numSequence
         FROM
         OpportunityItems
         CROSS JOIN LATERAL(SELECT
            split_part(OutParam,'-',1)::NUMERIC As numKitItemID
					,split_part(OutParam,'-',2)::NUMERIC As numChildItemID
					,split_part(OutParam,'-',3)::DOUBLE PRECISION As numQty
					,CASE WHEN COALESCE(split_part(OutParam,'-',4),'') = '' THEN 0 ELSE split_part(OutParam,'-',4)::NUMERIC END As numSequence
            FROM(SELECT
               OutParam
               FROM
               SplitString(vcChildKitSelectedItems,',')) X) Y
         WHERE
         numOppId = v_numOppID
         AND LENGTH(coalesce(vcChildKitSelectedItems,'')) > 0
         ORDER BY
         coalesce(Y.numSequence,0);

			--INSERT KIT ITEMS 
         INSERT INTO OpportunityKitItems(numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost)
         SELECT
         v_numOppID,
				OI.numoppitemtCode,
				T1.numKitChildItemID,
				coalesce((SELECT  numWareHouseItemID FROM WareHouseItems WHERE numItemID = T1.numKitChildItemID AND WareHouseItems.numWareHouseID = W.numWareHouseID ORDER BY numWareHouseItemID LIMIT 1),0),
				T1.numQty*OI.numUnitHour,
				T1.numQty,
				I.numBaseUnit,
				0,
				coalesce(I.monAverageCost,0)
         FROM
         OpportunityItems OI
         LEFT JOIN
         WareHouseItems WI
         ON
         OI.numWarehouseItmsID = WI.numWareHouseItemID
         LEFT JOIN
         Warehouses W
         ON
         WI.numWareHouseID = W.numWareHouseID
         INNER JOIN
         tt_TEMPKITCONFIGURATION T1
         ON
         T1.numOppItemID = OI.numoppitemtCode
         AND T1.numItemCode = OI.numItemCode
         JOIN
         Item I
         ON
         T1.numKitChildItemID = I.numItemCode
         WHERE
         OI.numOppId = v_numOppID
         AND(SELECT COUNT(*) FROM tt_TEMPKITCONFIGURATION t2 WHERE t2.numOppItemID = OI.numoppitemtCode AND t2.numItemCode = OI.numItemCode AND coalesce(t2.numKitItemID,0) = 0) > 0
         ORDER BY
         coalesce(T1.numSequence,0);
         INSERT INTO OpportunityKitItems(numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost)
         SELECT
         v_numOppID,
				OI.numoppitemtCode,
				ID.numChildItemID,
				coalesce((SELECT  numWareHouseItemID FROM WareHouseItems WHERE numItemID = ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWareHouseID ORDER BY numWareHouseItemID LIMIT 1),0),
				(ID.numQtyItemsReq*coalesce(fn_UOMConversion(ID.numUOMID,ID.numChildItemID,v_numDomainId,I.numBaseUnit),1))*OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMID,
				0,
				coalesce(I.monAverageCost,0)
         FROM
         OpportunityItems OI
         LEFT JOIN
         WareHouseItems WI
         ON
         OI.numWarehouseItmsID = WI.numWareHouseItemID
         LEFT JOIN
         Warehouses W
         ON
         WI.numWareHouseID = W.numWareHouseID
         JOIN
         ItemDetails ID
         ON
         OI.numItemCode = ID.numItemKitID
         JOIN
         Item I
         ON
         ID.numChildItemID = I.numItemCode
         WHERE
         OI.numOppId = v_numOppID
         AND(SELECT COUNT(*) FROM tt_TEMPKITCONFIGURATION t2 WHERE t2.numOppItemID = OI.numoppitemtCode AND t2.numItemCode = OI.numItemCode AND coalesce(t2.numKitItemID,0) = 0) = 0
         ORDER BY
         coalesce(ID.sintOrder,0);
         IF(SELECT
         COUNT(*)
         FROM
         OpportunityKitItems OI
         INNER JOIN
         Item I
         ON
         OI.numChildItemID = I.numItemCode
         WHERE
         numOppId = v_numOppID
         AND coalesce(charItemType,'') = 'P'
         AND numWareHouseItemId = 0) > 0 then
			
            RAISE EXCEPTION 'SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS';
         end if;
         IF(SELECT
         COUNT(*)
         FROM(SELECT
            t1.numOppItemID
							,t1.numItemCode
							,numKitItemID
							,numKitChildItemID
            FROM
            tt_TEMPKITCONFIGURATION t1
            WHERE
            coalesce(t1.numKitItemID,0) > 0) TempChildItems
         INNER JOIN
         OpportunityKitItems OKI
         ON
         OKI.numOppId = v_numOppID
         AND OKI.numOppItemID = TempChildItems.numOppItemID
         AND OKI.numChildItemID = TempChildItems.numKitItemID
         INNER JOIN
         OpportunityItems OI
         ON
         OI.numItemCode = TempChildItems.numItemCode
         AND OI.numoppitemtCode = OKI.numOppItemID
         LEFT JOIN
         WareHouseItems WI
         ON
         OI.numWarehouseItmsID = WI.numWareHouseItemID
         LEFT JOIN
         Warehouses W
         ON
         WI.numWareHouseID = W.numWareHouseID
         INNER JOIN
         ItemDetails
         ON
         TempChildItems.numKitItemID = ItemDetails.numItemKitID
         AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
         INNER JOIN
         Item
         ON
         ItemDetails.numChildItemID = Item.numItemCode
         INNER JOIN
         Item IMain
         ON
         OKI.numChildItemID = IMain.numItemCode
         WHERE
         Item.charItemType = 'P'
         AND coalesce(IMain.bitKitParent,false) = true
         AND coalesce((SELECT  numWareHouseItemID FROM WareHouseItems WHERE numItemID = ItemDetails.numChildItemID AND numWareHouseID = W.numWareHouseID LIMIT 1),
         0) = 0) > 0 then
				
            RAISE EXCEPTION 'SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS';
         end if;
         INSERT INTO OpportunityKitChildItems(numOppId,
				numOppItemID,
				numOppChildItemID,
				numItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost)
         SELECT
         v_numOppID,
				OKI.numOppItemID,
				OKI.numOppChildItemID,
				ItemDetails.numChildItemID,
				(SELECT  numWareHouseItemID FROM WareHouseItems WHERE numItemID = ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWareHouseID ORDER BY numWareHouseItemID LIMIT 1),
				(CASE WHEN coalesce(TempChildItems.numQty,0) > 0 THEN coalesce(TempChildItems.numQty,0) ELSE(ItemDetails.numQtyItemsReq*coalesce(fn_UOMConversion(ItemDetails.numUOMID,ItemDetails.numChildItemID,v_numDomainId,Item.numBaseUnit),1)) END)*OKI.numQtyItemsReq,
				(CASE WHEN coalesce(TempChildItems.numQty,0) > 0 THEN coalesce(TempChildItems.numQty,0) ELSE ItemDetails.numQtyItemsReq END),
				(CASE WHEN coalesce(TempChildItems.numQty,0) > 0 THEN 0 ELSE ItemDetails.numUOMID END),
				0,
				coalesce(Item.monAverageCost,0)
         FROM(SELECT
            t1.numOppItemID
					,t1.numItemCode
					,numKitItemID
					,numKitChildItemID
					,t1.numQty
            FROM
            tt_TEMPKITCONFIGURATION t1
            WHERE
            coalesce(t1.numKitItemID,0) > 0) TempChildItems
         INNER JOIN
         OpportunityKitItems OKI
         ON
         OKI.numOppId = v_numOppID
         AND OKI.numOppItemID = TempChildItems.numOppItemID
         AND OKI.numChildItemID = TempChildItems.numKitItemID
         INNER JOIN
         OpportunityItems OI
         ON
         OI.numItemCode = TempChildItems.numItemCode
         AND OI.numoppitemtCode = OKI.numOppItemID
         LEFT JOIN
         WareHouseItems WI
         ON
         OI.numWarehouseItmsID = WI.numWareHouseItemID
         LEFT JOIN
         Warehouses W
         ON
         WI.numWareHouseID = W.numWareHouseID
         INNER JOIN
         ItemDetails
         ON
         TempChildItems.numKitItemID = ItemDetails.numItemKitID
         AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
         INNER JOIN
         Item
         ON
         ItemDetails.numChildItemID = Item.numItemCode;

			-- UPDATE UOM OF OPPORTUNITY ITEMS HAVE NOT UOM SET WHILE EXPORTED FROM MARKETPLACE (MEANS TINTSOURCETYPE = 3)
         IF coalesce(v_tintSourceType,0) = 3 then
			
            UPDATE
            OpportunityItems OI
            SET
            numUOMID = coalesce(I.numSaleUnit,0),numUnitHour = fn_UOMConversion(coalesce(I.numSaleUnit,0),OI.numItemCode,v_numDomainId,null::NUMERIC)*OI.numUnitHour
            FROM
            Item I
            WHERE
            OI.numItemCode = I.numItemCode AND(numOppId = v_numOppID
            AND I.numDomainID = v_numDomainId);
         end if;
         INSERT INTO OppWarehouseSerializedItem(numOppID,
				numWarehouseItmsDTLID,
				numOppItemID,
				numWarehouseItmsID)
         SELECT
         v_numOppID,
				X.numWarehouseItmsDTLID,
				X.numoppitemtCode,
				X.numWItmsID
         FROM
		 XMLTABLE
			(
				'NewDataSet/SerialNo'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					numWarehouseItmsDTLID NUMERIC(9,0) PATH 'numWarehouseItmsDTLID'
					,numoppitemtCode NUMERIC(9,0) PATH 'numoppitemtCode'
					,numWItmsID NUMERIC(9,0) PATH 'numWItmsID'
			) AS X;

         UPDATE
         OppWarehouseSerializedItem
         SET
         numOppItemID = X.numoppitemtCode
         FROM(SELECT
         numWarehouseItmsDTLID AS DTLID,
					O.numoppitemtCode
         FROM
		  XMLTABLE
			(
				'NewDataSet/SerialNo'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					numWarehouseItmsDTLID NUMERIC(9,0) PATH 'numWarehouseItmsDTLID'
					,numoppitemtCode NUMERIC(9,0) PATH 'numoppitemtCode'
					,numWItmsID NUMERIC(9,0) PATH 'numWItmsID'
			) AS TEMP
         JOIN
         OpportunityItems O
         ON
         O.numWarehouseItmsID = TEMP.numWItmsID and O.numOppId = v_numOppID) X
         WHERE(numOppID = v_numOppID
         AND numWarehouseItmsDTLID = X.DTLID);
         IF NOT (v_tintCommitAllocation = 2 AND v_bitAllocateInventoryOnPickList = true) then
			
            PERFORM USP_ManageSOWorkOrder(v_numDomainId,v_numOppID,v_numUserCntID);
         end if;
      end if;
   ELSE

		--FOLLOWING DELETE STATEMENT IS COMMENTED BECUASE NOW WE ARE NOT ALLOWING USER TO EIDT SERIAL/LOT ITEM UNTILL USER REMOVED ASSIGNED SERIAL/LOT NUMBERS
	    --Delete previously added serial lot number added for opportunity
		--DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
	
      select   tintoppstatus, coalesce(numassignedto,0) INTO v_tintOppStatus,v_tempAssignedTo FROM
      OpportunityMaster WHERE
      numOppId = v_numOppID;
      IF v_tempAssignedTo <> v_numAssignedTo then
		
         IF(SELECT COUNT(*) FROM BizDocComission WHERE coalesce(numOppID,0) = v_numOppID AND coalesce(bitCommisionPaid,false) = true) > 0 then
			
            RAISE EXCEPTION 'COMMISSION_PAID_AGAINST_BIZDOC';
         end if;
      end if;
 
		--REVERTING BACK THE WAREHOUSE ITEMS                  
      IF v_tintOppStatus = 1 AND v_tintCommitAllocation = 1 then
		
         PERFORM USP_RevertDetailsOpp(v_numOppID,0,0::SMALLINT,v_numUserCntID);
      end if;
      IF LENGTH(coalesce(v_vcOppRefOrderNo,'')) > 0 then
		
         UPDATE OpportunityBizDocs SET vcRefOrderNo = v_vcOppRefOrderNo WHERE numoppid = v_numOppID;
      end if;
      IF EXISTS(SELECT 1 FROM OpportunityMaster WHERE numOppId = v_numOppID AND tintopptype = 1 AND coalesce(tintoppstatus,0) = 1 AND coalesce(numStatus,0) != v_numStatus) then
		
			 -- numeric(18, 0)
					 -- numeric(18, 0)
					 -- numeric(18, 0)
					 -- numeric(18, 0)
					 -- numeric(18, 0)
					 -- numeric(18, 0)
					 -- tinyint
         PERFORM USP_ManageOpportunityAutomationQueue(v_numOppQueueID := 0,v_numDomainId := v_numDomainId,v_numOppID := v_numOppID,
         v_numOppBizDocsId := 0,v_numOrderStatus := v_numStatus,v_numUserCntID := v_numUserCntID,
         v_tintProcessStatus := 1::SMALLINT,v_tintMode := 1::SMALLINT);
      end if;
      UPDATE
      OpportunityMaster
      SET
      vcpOppName = v_vcPOppName,txtComments = coalesce(v_Comments,''),bitPublicFlag = v_bitPublicFlag,
      numCampainID = v_CampaignID,tintSource = v_tintSource,
      tintSourceType = v_tintSourceType,intPEstimatedCloseDate = v_dtEstimatedCloseDate,
      numModifiedBy = v_numUserCntID,bintModifiedDate = TIMEZONE('UTC',now()),
      lngPConclAnalysis = v_lngPConclAnalysis,monPAmount = v_monPAmount,
      tintActive = v_tintActive,numSalesOrPurType = v_numSalesOrPurType,
      numStatus = v_numStatus,vcOppRefOrderNo = v_vcOppRefOrderNo,
      bintOppToOrder =(CASE WHEN v_tintOppStatus = 0 AND v_DealStatus = 1 THEN TIMEZONE('UTC',now()) ELSE bintOppToOrder END),bitDiscountType = v_bitDiscountType,
      fltDiscount = v_fltDiscount,bitBillingTerms = v_bitBillingTerms,
      intBillingDays = v_intBillingDays,bitInterestType = v_bitInterestType,fltInterest = v_fltInterest,
      tintTaxOperator = v_tintTaxOperator,numDiscountAcntType = v_numDiscountAcntType,
      numPercentageComplete = v_numPercentageComplete,
      bitUseShippersAccountNo = v_bitUseShippersAccountNo,intUsedShippingCompany = v_intUsedShippingCompany, 
      numContactId = v_numContactId,numShipmentMethod = v_numShipmentMethod,
      numShippingService = v_numShippingService,
      numPartner = v_numPartner,numPartenerContact = v_numPartenerContactId,
      numProjectID = v_numProjectID
      WHERE
      numOppId = v_numOppID;   
	   
		-- UPDATING IF ORGANIZATION IS ASSIGNED TO SOMEONE                                                      
      IF (v_tempAssignedTo <> v_numAssignedTo AND v_numAssignedTo <> cast(NULLIF('0','') as NUMERIC(9,0))) then
		
         UPDATE
         OpportunityMaster
         SET
         numassignedto = v_numAssignedTo,numAssignedBy = v_numUserCntID
         WHERE
         numOppId = v_numOppID;
      ELSEIF (v_numAssignedTo = 0)
      then
		
         UPDATE
         OpportunityMaster
         SET
         numassignedto = 0,numAssignedBy = 0
         WHERE
         numOppId = v_numOppID;
      end if;  
			                                                                    
		-- UPDATING OPP ITEMS
      IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' AND v_numOppID > 0 then
         IF EXISTS(SELECT
         OI.numoppitemtCode
         FROM
         OpportunityItems OI
         WHERE
         numOppId = v_numOppID
         AND OI.numoppitemtCode NOT IN(SELECT numoppitemtCode FROM XMLTABLE
			(
				'NewDataSet/Item'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					numoppitemtCode NUMERIC(18,0) PATH 'numoppitemtCode'
			))
         AND coalesce((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppID = v_numOppID AND numOppItemID = OI.numoppitemtCode),0) > 0) then
			
            RAISE EXCEPTION 'SERIAL/LOT#_ASSIGNED_TO_ITEM_DELETED';
         end if;
         DELETE FROM OpportunityKitChildItems WHERE numOppId = v_numOppID;
         DELETE FROM OpportunityKitItems WHERE numOppId = v_numOppID;
         DELETE FROM OpportunityItems WHERE numOppId = v_numOppID AND numoppitemtCode NOT IN(SELECT numoppitemtCode FROM XMLTABLE
			(
				'NewDataSet/Item'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					numoppitemtCode NUMERIC(9,0) PATH 'numoppitemtCode'
			)); 
	          
			-- DELETE CORRESPONSING TIME AND EXPENSE ENTRIES OF DELETED ITEMS
         DELETE FROM
         timeandexpense
         WHERE
         numDomainID = v_numDomainId
         AND numOppId = v_numOppID
         AND numOppItemID NOT IN(SELECT numoppitemtCode FROM XMLTABLE
			(
				'NewDataSet/Item'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					numoppitemtCode NUMERIC(9,0) PATH 'numoppitemtCode'
			)); 

			--INSERT NEW ADDED ITEMS                        
         INSERT INTO OpportunityItems(numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,
				vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numToWarehouseItemID,numShipmentMethod,
				numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired,vcAttrValues,numPromotionID,bitPromotionTriggered
				,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,dtPlannedStart,numWOAssignedTo,bitMarkupDiscount,ItemReleaseDate
				,vcChildKitSelectedItems,numWOQty,vcSKU,vcASIN,bitDisablePromotion)
         SELECT
         v_numOppID,X.numItemCode,fn_UOMConversion(X.numUOM,numItemCode,v_numDomainId,null::NUMERIC)*x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM Item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM Item WHERE numItemCode = X.numItemCode),
				(SELECT  vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND bitDefault = true AND numDomainId = v_numDomainId LIMIT 1),
				fn_FindVendorCost(X.numItemCode,v_numDivisionId,v_numDomainId,fn_UOMConversion(X.numUOM,numItemCode,v_numDomainId,null::NUMERIC)*x.numUnitHour),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numToWarehouseItemID,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
				0,
				(SELECT(CASE WHEN coalesce(I.bitVirtualInventory,false) = true THEN 0 ELSE coalesce(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainID = v_numDomainId AND I.numItemCode = X.numItemCode), X.bitItemPriceApprovalRequired, X.AttributeIDs,
				X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,coalesce(X.numCost,0),coalesce(vcVendorNotes,'')
				,X.vcInstruction,X.bintCompliationDate+CAST(v_ClientTimeZoneOffset || 'minute' as interval),X.dtPlannedStart+CAST(v_ClientTimeZoneOffset || 'minute' as interval),X.numWOAssignedTo,X.bitMarkupDiscount
				,v_dtItemRelease,coalesce(KitChildItems,''),X.numWOQty,X.vcSKU,X.vcASIN,X.bitDisablePromotion
         FROM(SELECT * FROM
		 XMLTABLE
			(
				'NewDataSet/Item'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					Op_Flag SMALLINT PATH 'Op_Flag'
					,numoppitemtCode NUMERIC(9,0) PATH 'numoppitemtCode'
					,numItemCode NUMERIC(9,0) PATH 'numItemCode'
					,numUnitHour DOUBLE PRECISION PATH 'numUnitHour'
					,monPrice DECIMAL(30,16) PATH 'monPrice'
					,monTotAmount DECIMAL(20,5) PATH 'monTotAmount'
					,vcItemDesc VARCHAR(4000) PATH 'vcItemDesc'
					,numWarehouseItmsID NUMERIC(9,0) PATH 'numWarehouseItmsID'
					,numToWarehouseItemID NUMERIC(9,0) PATH 'numToWarehouseItemID'
					,ItemType VARCHAR(30) PATH 'ItemType'
					,DropShip BOOLEAN PATH 'DropShip'
					,bitDiscountType BOOLEAN PATH 'bitDiscountType'
					,fltDiscount DECIMAL(30,16) PATH 'fltDiscount'
					,monTotAmtBefDiscount DECIMAL(20,5) PATH 'monTotAmtBefDiscount'
					,vcItemName VARCHAR(300) PATH 'vcItemName'
					,numUOM NUMERIC(9,0) PATH 'numUOM'
					,bitWorkOrder BOOLEAN PATH 'bitWorkOrder'
					,numVendorWareHouse NUMERIC(9,0) PATH 'numVendorWareHouse'
					,numShipmentMethod NUMERIC(9,0) PATH 'numShipmentMethod'
					,numSOVendorId NUMERIC(9,0) PATH 'numSOVendorId'
					,numProjectID NUMERIC(9,0) PATH 'numProjectID'
					,numProjectStageID NUMERIC(9,0) PATH 'numProjectStageID'
					,Attributes VARCHAR(500) PATH 'Attributes'
					,bitItemPriceApprovalRequired BOOLEAN PATH 'bitItemPriceApprovalRequired'
					,AttributeIDs VARCHAR(500) PATH 'AttributeIDs'
					,numPromotionID NUMERIC(18,0) PATH 'numPromotionID'
					,bitPromotionTriggered BOOLEAN PATH 'bitPromotionTriggered'
					,vcPromotionDetail VARCHAR(2000) PATH 'vcPromotionDetail'
					,numSortOrder NUMERIC(18,0) PATH 'numSortOrder'
					,numCost DECIMAL(20,5) PATH 'numCost'
					,vcVendorNotes VARCHAR(4000) PATH 'vcVendorNotes'
					,vcInstruction VARCHAR(1000) PATH 'vcInstruction'
					,bintCompliationDate TIMESTAMP PATH 'bintCompliationDate'
					,dtPlannedStart TIMESTAMP PATH 'dtPlannedStart'
					,numWOAssignedTo NUMERIC(18,0) PATH 'numWOAssignedTo'
					,bitMarkupDiscount BOOLEAN PATH 'bitMarkupDiscount'
					,KitChildItems TEXT PATH 'KitChildItems'
					,numWOQty DOUBLE PRECISION PATH 'numWOQty'
					,vcSKU VARCHAR(100) PATH 'vcSKU'
					,vcASIN VARCHAR(100) PATH 'vcASIN'
					,bitDisablePromotion BOOLEAN PATH 'bitDisablePromotion'
			) TEMP
			WHERE TEMP.Op_Flag=1
        ) X; 
			
			-- UPDATE EXISTING CUSTOMER PART NO
         UPDATE
         CustomerPartNumber CPN
         SET
         CustomerPartNo = coalesce(TEMPCPN.CustomerPartNo,'')
         FROM(SELECT
         numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
         FROM
		  XMLTABLE
			(
				'NewDataSet/Item'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					Op_Flag SMALLINT PATH 'Op_Flag'
					,CustomerPartNo VARCHAR(300) PATH 'CustomerPartNo'
					,numItemCode NUMERIC(18,0) PATH 'numItemCode'
			) AS X         
         GROUP BY
         numItemCode) AS TEMPCPN
         WHERE(CPN.numCompanyId = v_numCompany
         AND CPN.numItemCode = TEMPCPN.numItemCode) AND(CPN.numDomainID = v_numDomainId
         AND CPN.numCompanyId = v_numCompany
         AND LENGTH(coalesce(TEMPCPN.CustomerPartNo,'')) > 0
         AND coalesce(TEMPCPN.CustomerPartNo,'') <> '0');
			
			-- INSERT NEW CUSTOMER PART NO
         INSERT INTO CustomerPartNumber(numCompanyId, numDomainID, CustomerPartNo, numItemCode)
         SELECT
         v_numCompany, v_numDomainId, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
         FROM(SELECT
            numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
            FROM
             XMLTABLE
			(
				'NewDataSet/Item'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					Op_Flag SMALLINT PATH 'Op_Flag'
					,CustomerPartNo VARCHAR(300) PATH 'CustomerPartNo'
					,numItemCode NUMERIC(18,0) PATH 'numItemCode'
			) AS X 
            GROUP BY
            numItemCode) TEMPCPN
         LEFT JOIN
         CustomerPartNumber CPN
         ON
         CPN.numCompanyId = v_numCompany
         AND CPN.numItemCode = TEMPCPN.numItemCode
         WHERE
         CPN.CustomerPartNoID IS NULL
         AND LENGTH(coalesce(TEMPCPN.CustomerPartNo,'')) > 0
         AND coalesce(TEMPCPN.CustomerPartNo,'') <> '0';
			
			-- UPDATE ITEMS
         UPDATE
         OpportunityItems
         SET
         numItemCode = X.numItemCode,numOppId = v_numOppID,numUnitHour = fn_UOMConversion(X.numUOM,X.numItemCode,v_numDomainId,null::NUMERIC)*x.numUnitHour,
         monPrice = x.monPrice,monTotAmount = x.monTotAmount,vcItemDesc = X.vcItemDesc,
         vcModelID = X.vcModelID,numWarehouseItmsID = X.numWarehouseItmsID,numToWarehouseItemID = X.numToWarehouseItemID,
         bitDropShip = X.DropShip,bitDiscountType = X.bitDiscountType,
         fltDiscount = X.fltDiscount,monTotAmtBefDiscount = X.monTotAmtBefDiscount,
         vcItemName = X.vcItemName,numUOMId = numUOM,
         numVendorWareHouse = X.numVendorWareHouse,numShipmentMethod = X.numShipmentMethod,
         numSOVendorId = X.numSOVendorId,numProjectID = X.numProjectID,numProjectStageID = X.numProjectStageID,
         vcAttributes = X.Attributes,bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired,
         bitWorkOrder = X.bitWorkOrder,
         vcAttrValues = AttributeIDs,numPromotionID = X.numPromotionID,bitPromotionTriggered = X.bitPromotionTriggered,
         vcPromotionDetail = X.vcPromotionDetail,
         numSortOrder = X.numSortOrder,numCost = X.numCost,vcNotes = vcVendorNotes,
         bitMarkupDiscount = X.bitMarkupDiscount,ItemRequiredDate = X.ItemRequiredDate,
         vcChildKitSelectedItems = coalesce(KitChildItems,''),bitDisablePromotion=X.bitDisablePromotion
         FROM(SELECT
         numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,numToWarehouseItemID,DropShip,bitDiscountType,
					fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
					bitItemPriceApprovalRequired,bitWorkOrder,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,coalesce(numCost,0) AS numCost,coalesce(vcVendorNotes,'') AS vcVendorNotes,
					ItemRequiredDate,bitMarkupDiscount,KitChildItems,bitDisablePromotion
         FROM
		 XMLTABLE
			(
				'NewDataSet/Item'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					Op_Flag SMALLINT PATH 'Op_Flag'
					,numoppitemtCode NUMERIC(9,0) PATH 'numoppitemtCode'
					 ,numItemCode NUMERIC(9,0) PATH 'numItemCode'
					 ,numUnitHour DOUBLE PRECISION PATH 'numUnitHour'
					 ,monPrice DECIMAL(30,16) PATH 'monPrice'
					 ,monTotAmount DECIMAL(20,5) PATH 'monTotAmount'
					 ,vcItemDesc VARCHAR(4000) PATH 'vcItemDesc'
					 ,vcModelID VARCHAR(300) PATH 'vcModelID'
					 ,numWarehouseItmsID NUMERIC(9,0) PATH 'numWarehouseItmsID'
					 ,numToWarehouseItemID NUMERIC(18,0) PATH 'numToWarehouseItemID'
					 ,DropShip BOOLEAN PATH 'DropShip'
					 ,bitDiscountType BOOLEAN PATH 'bitDiscountType'
					 ,fltDiscount DECIMAL(30,16) PATH 'fltDiscount'
					 ,monTotAmtBefDiscount DECIMAL(20,5) PATH 'monTotAmtBefDiscount'
					 ,vcItemName VARCHAR(300) PATH 'vcItemName'
					 ,numUOM NUMERIC(9,0) PATH 'numUOM'
					 ,numVendorWareHouse NUMERIC(9,0) PATH 'numVendorWareHouse'
					 ,numShipmentMethod NUMERIC(9,0) PATH 'numShipmentMethod'
					 ,numSOVendorId NUMERIC(9,0) PATH 'numSOVendorId'
					 ,numProjectID NUMERIC(9,0) PATH 'numProjectID'
					 ,numProjectStageID NUMERIC(9,0) PATH 'numProjectStageID'
					 ,Attributes VARCHAR(500) PATH 'Attributes'
					 ,bitItemPriceApprovalRequired BOOLEAN PATH 'bitItemPriceApprovalRequired'
					 ,bitWorkOrder BOOLEAN PATH 'bitWorkOrder'
					 ,AttributeIDs VARCHAR(500) PATH 'AttributeIDs'
					 ,numPromotionID NUMERIC(18,0) PATH 'numPromotionID'
					 ,bitPromotionTriggered BOOLEAN PATH 'bitPromotionTriggered'
					 ,vcPromotionDetail VARCHAR(2000) PATH 'vcPromotionDetail'
					 ,numSortOrder NUMERIC(18,0) PATH 'numSortOrder'
					 ,numCost DECIMAL(20,5) PATH 'numCost'
					 ,vcVendorNotes VARCHAR(4000) PATH 'vcVendorNotes'
					 ,ItemRequiredDate TIMESTAMP PATH 'ItemRequiredDate'
					 ,bitMarkupDiscount BOOLEAN PATH 'bitMarkupDiscount'
					 ,KitChildItems TEXT PATH 'KitChildItems'
					 ,bitDisablePromotion BOOLEAN PATH 'bitDisablePromotion'
			) TEMP
			WHERE
				TEMP.Op_Flag=2) X
         WHERE
         OpportunityItems.numoppitemtCode = X.numOppItemID AND OpportunityItems.numOppId = v_numOppID;

			--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
         DELETE FROM tt_TEMPKITCONFIGURATION;
         INSERT INTO tt_TEMPKITCONFIGURATION(numOppItemID,
				numItemCode,
				numKitItemID,
				numKitChildItemID,
				numQty,
				numSequence)
         SELECT
         numoppitemtCode,
				numItemCode,
				numKitItemID,
				numChildItemID,
				numQty,
				numSequence
         FROM
         OpportunityItems
         CROSS JOIN LATERAL(SELECT
            split_part(OutParam,'-',1)::NUMERIC As numKitItemID
					,split_part(OutParam,'-',2)::NUMERIC As numChildItemID
					,CASE WHEN COALESCE(split_part(OutParam,'-',3),'') = '' THEN -1 ELSE split_part(OutParam,'-',3)::NUMERIC END As numQty
					,CASE WHEN COALESCE(split_part(OutParam,'-',4),'') = '' THEN 0 ELSE split_part(OutParam,'-',4)::NUMERIC END As numSequence
            FROM(SELECT
               OutParam
               FROM
               SplitString(vcChildKitSelectedItems,',')) X) Y
         WHERE
         numOppId = v_numOppID
         AND LENGTH(coalesce(vcChildKitSelectedItems,'')) > 0
         ORDER BY
         coalesce(Y.numSequence,0);

			--INSERT KIT ITEMS 
         INSERT INTO OpportunityKitItems(numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost)
         SELECT
         v_numOppID,
				OI.numoppitemtCode,
				T1.numKitChildItemID,
				coalesce((SELECT  numWareHouseItemID FROM WareHouseItems WHERE numItemID = T1.numKitChildItemID AND WareHouseItems.numWareHouseID = W.numWareHouseID ORDER BY numWareHouseItemID LIMIT 1),0),
				(CASE WHEN T1.numQty = -1 THEN (ID.numQtyItemsReq*coalesce(fn_UOMConversion(ID.numUOMID,ID.numChildItemID,v_numDomainId,I.numBaseUnit),1)) ELSE T1.numQty END)*OI.numUnitHour,
				(CASE WHEN T1.numQty = -1 THEN ID.numQtyItemsReq ELSE T1.numQty END),
				I.numBaseUnit,
				0,
				coalesce(I.monAverageCost,0)
         FROM
         OpportunityItems OI
         LEFT JOIN
         WareHouseItems WI
         ON
         OI.numWarehouseItmsID = WI.numWareHouseItemID
         LEFT JOIN
         Warehouses W
         ON
         WI.numWareHouseID = W.numWareHouseID
         INNER JOIN
         tt_TEMPKITCONFIGURATION T1
         ON
         T1.numOppItemID = OI.numoppitemtCode
         AND T1.numItemCode = OI.numItemCode
		 INNER JOIN
			ItemDetails ID
		ON
			T1.numKitItemID = ID.numItemKitID
			AND T1.numKitChildItemID = ID.numChildItemID
         JOIN
         Item I
         ON
         T1.numKitChildItemID = I.numItemCode
         WHERE
         OI.numOppId = v_numOppID
         AND(SELECT COUNT(*) FROM tt_TEMPKITCONFIGURATION t2 WHERE t2.numOppItemID = OI.numoppitemtCode AND t2.numItemCode = OI.numItemCode AND coalesce(t2.numKitItemID,0) = 0) > 0
         ORDER BY
         coalesce(T1.numSequence,0);
         INSERT INTO OpportunityKitItems(numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost)
         SELECT
         v_numOppID,
				OI.numoppitemtCode,
				ID.numChildItemID,
				coalesce((SELECT  numWareHouseItemID FROM WareHouseItems WHERE numItemID = ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWareHouseID ORDER BY numWareHouseItemID LIMIT 1),0),
				(ID.numQtyItemsReq*coalesce(fn_UOMConversion(ID.numUOMID,ID.numChildItemID,v_numDomainId,I.numBaseUnit),1))*OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMID,
				0,
				coalesce(I.monAverageCost,0)
         FROM
         OpportunityItems OI
         LEFT JOIN
         WareHouseItems WI
         ON
         OI.numWarehouseItmsID = WI.numWareHouseItemID
         LEFT JOIN
         Warehouses W
         ON
         WI.numWareHouseID = W.numWareHouseID
         JOIN
         ItemDetails ID
         ON
         OI.numItemCode = ID.numItemKitID
         JOIN
         Item I
         ON
         ID.numChildItemID = I.numItemCode
         WHERE
         OI.numOppId = v_numOppID
         AND(SELECT COUNT(*) FROM tt_TEMPKITCONFIGURATION t2 WHERE t2.numOppItemID = OI.numoppitemtCode AND t2.numItemCode = OI.numItemCode AND coalesce(t2.numKitItemID,0) = 0) = 0
         ORDER BY
         coalesce(ID.sintOrder,0);
         IF(SELECT
         COUNT(*)
         FROM
         OpportunityKitItems OI
         INNER JOIN
         Item I
         ON
         OI.numChildItemID = I.numItemCode
         WHERE
         numOppId = v_numOppID
         AND coalesce(charItemType,'') = 'P'
         AND numWareHouseItemId = 0) > 0 then
			
            RAISE EXCEPTION 'SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS';
         end if;
         IF(SELECT
         COUNT(*)
         FROM(SELECT
            t1.numOppItemID
						,t1.numItemCode
						,numKitItemID
						,numKitChildItemID
            FROM
            tt_TEMPKITCONFIGURATION t1
            WHERE
            coalesce(t1.numKitItemID,0) > 0) TempChildItems
         INNER JOIN
         OpportunityKitItems OKI
         ON
         OKI.numOppId = v_numOppID
         AND OKI.numOppItemID = TempChildItems.numOppItemID
         AND OKI.numChildItemID = TempChildItems.numKitItemID
         INNER JOIN
         OpportunityItems OI
         ON
         OI.numItemCode = TempChildItems.numItemCode
         AND OI.numoppitemtCode = OKI.numOppItemID
         LEFT JOIN
         WareHouseItems WI
         ON
         OI.numWarehouseItmsID = WI.numWareHouseItemID
         LEFT JOIN
         Warehouses W
         ON
         WI.numWareHouseID = W.numWareHouseID
         INNER JOIN
         ItemDetails
         ON
         TempChildItems.numKitItemID = ItemDetails.numItemKitID
         AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
         INNER JOIN
         Item
         ON
         ItemDetails.numChildItemID = Item.numItemCode
         INNER JOIN
         Item IMain
         ON
         OKI.numChildItemID = IMain.numItemCode
         WHERE
         Item.charItemType = 'P'
         AND coalesce(IMain.bitKitParent,false) = true
         AND coalesce((SELECT  numWareHouseItemID FROM WareHouseItems WHERE numItemID = ItemDetails.numChildItemID AND numWareHouseID = W.numWareHouseID LIMIT 1),
         0) = 0) > 0 then
			
            RAISE EXCEPTION 'SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS';
         end if;
         INSERT INTO OpportunityKitChildItems(numOppId,
				numOppItemID,
				numOppChildItemID,
				numItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost)
         SELECT
         v_numOppID,
				OKI.numOppItemID,
				OKI.numOppChildItemID,
				ItemDetails.numChildItemID,
				(SELECT  numWareHouseItemID FROM WareHouseItems WHERE numItemID = ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWareHouseID ORDER BY numWareHouseItemID LIMIT 1),
				(ItemDetails.numQtyItemsReq*coalesce(fn_UOMConversion(ItemDetails.numUOMID,ItemDetails.numChildItemID,v_numDomainId,Item.numBaseUnit),1))*OKI.numQtyItemsReq,
				ItemDetails.numQtyItemsReq,
				ItemDetails.numUOMID,
				0,
				coalesce(Item.monAverageCost,0)
         FROM(SELECT
            t1.numOppItemID
					,t1.numItemCode
					,numKitItemID
					,numKitChildItemID
            FROM
            tt_TEMPKITCONFIGURATION t1
            WHERE
            coalesce(t1.numKitItemID,0) > 0) TempChildItems
         INNER JOIN
         OpportunityKitItems OKI
         ON
         OKI.numOppId = v_numOppID
         AND OKI.numOppItemID = TempChildItems.numOppItemID
         AND OKI.numChildItemID = TempChildItems.numKitItemID
         INNER JOIN
         OpportunityItems OI
         ON
         OI.numItemCode = TempChildItems.numItemCode
         AND OI.numoppitemtCode = OKI.numOppItemID
         LEFT JOIN
         WareHouseItems WI
         ON
         OI.numWarehouseItmsID = WI.numWareHouseItemID
         LEFT JOIN
         Warehouses W
         ON
         WI.numWareHouseID = W.numWareHouseID
         INNER JOIN
         ItemDetails
         ON
         TempChildItems.numKitItemID = ItemDetails.numItemKitID
         AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
         INNER JOIN
         Item
         ON
         ItemDetails.numChildItemID = Item.numItemCode;
         IF NOT (v_tintCommitAllocation = 2 AND v_bitAllocateInventoryOnPickList = true) then
			
            PERFORM USP_ManageSOWorkOrder(v_numDomainId,v_numOppID,v_numUserCntID);
         end if;
      end if;
      IF v_tintOppType = 1 AND v_tintOppStatus = 1 then --Sales Order
		
         IF EXISTS(SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId = v_numOppID AND coalesce(numWarehouseItmsID,0) > 0 AND coalesce(bitDropShip,false) = false AND(coalesce(numUnitHour,0) -coalesce(numQtyShipped,0)) < 0) then
			
            RAISE EXCEPTION 'EDITED_ITEMS_QTY_IS_LESS_THEN_SHIPPED_QTY';
            RETURN;
         end if;
         IF EXISTS(SELECT numoppitemtCode FROM OpportunityItems INNER JOIN WorkOrder ON OpportunityItems.numOppId = WorkOrder.numOppId AND OpportunityItems.numoppitemtCode = WorkOrder.numOppItemID AND coalesce(WorkOrder.numParentWOID,0) = 0 AND WorkOrder.numWOStatus = 23184 AND coalesce(OpportunityItems.numUnitHour,0) > coalesce(WorkOrder.numQtyItemsReq,0) WHERE OpportunityItems.numOppId = v_numOppID) then
			
            RAISE EXCEPTION 'EDITED_ITEMS_QTY_CAN_NOT_BE_GREATER_THEN_COMPLETED_WORK_ORDER';
            RETURN;
         end if;
      ELSEIF v_tintOppType = 2 AND v_tintOppStatus = 1
      then --Purchase Order
		
         IF EXISTS(SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId = v_numOppID AND coalesce(numWarehouseItmsID,0) > 0 AND coalesce(bitDropShip,false) = false AND(coalesce(numUnitHour,0) -coalesce(numUnitHourReceived,0)) < 0) then
			
            RAISE EXCEPTION 'EDITED_ITEMS_QTY_IS_LESS_THEN_RECEIVED_QTY';
            RETURN;
         end if;
      end if;
   end if;
   IF v_tintOppStatus = 1 then
	
      IF(SELECT COUNT(*) FROM OpportunityItems WHERE numOppId = v_numOppID AND coalesce(numWarehouseItmsID,0) > 0 AND coalesce(bitDropShip,false) = false) <>(SELECT COUNT(*) FROM OpportunityItems INNER JOIN WareHouseItems ON OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID AND OpportunityItems.numItemCode = WareHouseItems.numItemID WHERE numOppId = v_numOppID AND OpportunityItems.numWarehouseItmsID > 0 AND coalesce(bitDropShip,false) = false) then
		
         RAISE EXCEPTION 'INVALID_ITEM_WAREHOUSES';
         RETURN;
      end if;
   end if;
   IF(SELECT
   COUNT(*)
   FROM
   OpportunityBizDocItems OBDI
   INNER JOIN
   OpportunityBizDocs OBD
   ON
   OBDI.numOppBizDocID = OBD.numOppBizDocsId
   WHERE
   OBD.numoppid = v_numOppID AND OBDI.numOppItemID NOT IN(SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId = v_numOppID)) > 0 then
	
      RAISE EXCEPTION 'ITEM_USED_IN_BIZDOC';
      RETURN;
   end if;
   IF(SELECT
   COUNT(*)
   FROM
   OpportunityItems OI
   INNER JOIN(SELECT
      numOppItemID
				,SUM(numUnitHour) AS numInvoiceBillQty
      FROM
      OpportunityBizDocItems OBDI
      INNER JOIN
      OpportunityBizDocs OBD
      ON
      OBDI.numOppBizDocID = OBD.numOppBizDocsId
      WHERE
      OBD.numoppid = v_numOppID
      AND coalesce(OBD.bitAuthoritativeBizDocs,0) = 1
      GROUP BY
      numOppItemID) AS TEMP
   ON
   TEMP.numOppItemID = OI.numoppitemtCode
   WHERE
   OI.numOppId = v_numOppID
   AND OI.numUnitHour < Temp.numInvoiceBillQty) > 0 then
	
      RAISE EXCEPTION 'ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_INVOICE/BILL_QTY';
      RETURN;
   end if;
   IF v_tintOppType = 1 AND v_tintCommitAllocation = 2 AND v_bitAllocateInventoryOnPickList = true then
	
      IF(SELECT
      COUNT(*)
      FROM(SELECT
         numOppItemID
					,SUM(numUnitHour) AS PackingSlipUnits
         FROM
         OpportunityBizDocItems OBDI
         INNER JOIN
         OpportunityBizDocs OBD
         ON
         OBDI.numOppBizDocID = OBD.numOppBizDocsId
         WHERE
         OBD.numoppid = v_numOppID
         AND OBD.numBizDocId = 29397 --Picking Slip
         GROUP BY
         numOppItemID) AS TEMP
      INNER JOIN
      OpportunityItems OI
      ON
      TEMP.numOppItemID = OI.numoppitemtCode
      WHERE
      OI.numUnitHour < PackingSlipUnits) > 0 then
		
         RAISE EXCEPTION 'ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_PACKING_SLIP_QTY';
         RETURN;
      end if;
   end if;
   v_TotalAmount := 0;
   select   SUM(monTotAmount) INTO v_TotalAmount FROM OpportunityItems WHERE numOppId = v_numOppID;
   UPDATE OpportunityMaster SET monPAmount = v_TotalAmount WHERE numOppId = v_numOppID;

/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
            
			
   SELECT bitIsInitalSalesOrder INTO v_bitIsInitalSalesOrder FROM OpportunityMaster WHERE numOppId = v_numOppID AND numDomainId = v_numDomainId;
			--SET @bitViolatePrice=(SELECT bitMarginPriceViolated FROM Domain WHERE numDomainId=@numDomainID)
   IF EXISTS(SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainID = v_numDomainId AND coalesce(bitMarginPriceViolated,false) = true) then
			
      v_bitViolatePrice := true;
   end if;
   IF(v_bitViolatePrice = true AND v_bitIsInitalSalesOrder = true AND v_tintClickBtn = 1) then
			
      UPDATE OpportunityMaster SET tintoppstatus = v_DealStatus WHERE numOppId = v_numOppID;
   ELSE
      UPDATE OpportunityMaster SET tintoppstatus = v_DealStatus WHERE numOppId = v_numOppID;
      select   tintoppstatus, tintopptype, tintshipped INTO v_tintOppStatus,v_tintOppType,v_tintShipped from OpportunityMaster where numOppId = v_numOppID;
      IF v_tintOppStatus = 1 AND v_tintCommitAllocation = 1 then
				
         PERFORM USP_UpdatingInventoryonCloseDeal(v_numOppID,0,v_numUserCntID);
      end if;
      select   numDivisionId, bintAccountClosingDate INTO v_numDivisionId,v_AccountClosingDate from OpportunityMaster where numOppId = v_numOppID;
      select   tintCRMType INTO v_tintCRMType from DivisionMaster where numDivisionID = v_numDivisionId; 

				--When deal is won                                         
      IF v_DealStatus = 1 then
				
         IF v_AccountClosingDate IS NULL then
					
            UPDATE OpportunityMaster SET bintAccountClosingDate = TIMEZONE('UTC',now()) WHERE numOppId = v_numOppID;
         end if;

					-- Promote Lead to Account when Sales/Purchase Order is created against it
         IF v_tintCRMType = 0 then --Lead & Order
					
            UPDATE
            DivisionMaster
            SET
            tintCRMType = 2,bintLeadProm = TIMEZONE('UTC',now()),bintLeadPromBy = v_numUserCntID,
            bintProsProm = TIMEZONE('UTC',now()),bintProsPromBy = v_numUserCntID
            WHERE
            numDivisionID = v_numDivisionId;
					--Promote Prospect to Account
         ELSEIF v_tintCRMType = 1
         then
					
            UPDATE
            DivisionMaster
            SET
            tintCRMType = 2,bintProsProm = TIMEZONE('UTC',now()),bintProsPromBy = v_numUserCntID
            WHERE
            numDivisionID = v_numDivisionId;
         end if;         
				--When Deal is Lost
      ELSEIF v_DealStatus = 2
      then
				
         select   bintAccountClosingDate INTO v_AccountClosingDate FROM OpportunityMaster WHERE numOppId = v_numOppID;
         IF v_AccountClosingDate IS NULL then
					
            UPDATE OpportunityMaster SET bintAccountClosingDate = TIMEZONE('UTC',now()) WHERE numOppId = v_numOppID;
         end if;
      end if;
   end if;

			/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
			-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
   if v_tintCRMType = 0 AND v_DealStatus = 0 then --Lead & Open Opp
			
      update DivisionMaster set tintCRMType = 1,bintLeadProm = TIMEZONE('UTC',now()),bintLeadPromBy = v_numUserCntID,
      bintProsProm = TIMEZONE('UTC',now()),bintProsPromBy = v_numUserCntID
      where numDivisionID = v_numDivisionId;
   end if;        

--Add/Update Address Details
   select   vcCompanyName, div.numCompanyID INTO v_vcCompanyName,v_numCompanyId from CompanyInfo Com
   join DivisionMaster div
   on div.numCompanyID = Com.numCompanyId where div.numDivisionID = v_numDivisionId;
   IF EXISTS(SELECT numOppId FROM OpportunityMaster WHERE numOppId = v_numOppID AND coalesce(tintBillToType,0) = 0 AND coalesce(numBillToAddressID,0) = 0) then

      select coalesce(numAddressID,0), coalesce(vcStreet,''), coalesce(vcCity,''), coalesce(vcPostalCode,''), coalesce(numState,0), coalesce(numCountry,0), bitIsPrimary, vcAddressName, coalesce(tintAddressOf,0), numContact, bitAltContact, vcAltContact 
	  INTO v_numAddressID,v_vcStreet,v_vcCity,v_vcPostalCode,v_numState,v_numCountry,v_bitIsPrimary,
      v_vcAddressName,v_tintAddressOf,v_numContact,v_bitAltContact,v_vcAltContact FROM
      AddressDetails AD WHERE
      AD.numDomainID = v_numDomainId
      AND AD.numRecordID = v_numDivisionId
      AND AD.tintAddressOf = 2
      AND AD.tintAddressType = 1
      AND AD.bitIsPrimary = true;
	 --  numeric(9, 0)
	 --  tinyint
	 --  varchar(100)
	 --  varchar(50)
	 --  varchar(15)
	 --  numeric(9, 0)
	 --  numeric(9, 0)
	 --  varchar(100)
	 --  numeric(9, 0)
      UPDATE OpportunityMaster SET tintBillToType = 2,numBillToAddressID = 0 WHERE numOppId = v_numOppID;
      PERFORM USP_UpdateOppAddress(v_numOppID := v_numOppID,v_byteMode := 0::SMALLINT,p_numAddressID := v_numAddressID,v_vcStreet := v_vcStreet,
      v_vcCity := v_vcCity,v_vcPostalCode := v_vcPostalCode,v_numState := v_numState,
      v_numCountry := v_numCountry,v_vcCompanyName := v_vcCompanyName,
      v_numCompanyId := 0,v_vcAddressName := v_vcAddressName,v_bitCalledFromProcedure := true,
      v_numContact := v_numContact,v_bitAltContact := v_bitAltContact,
      v_vcAltContact := v_vcAltContact);
   end if;
   IF EXISTS(SELECT numOppId FROM OpportunityMaster WHERE numOppId = v_numOppID AND coalesce(tintShipToType,0) = 0 AND coalesce(numShipToAddressID,0) = 0) then

      select coalesce(numAddressID,0),coalesce(vcStreet,''), coalesce(vcCity,''), coalesce(vcPostalCode,''), coalesce(numState,0), coalesce(numCountry,0), bitIsPrimary, vcAddressName, coalesce(tintAddressOf,0), numContact, bitAltContact, vcAltContact 
	  INTO v_numAddressID,v_vcStreet,v_vcCity,v_vcPostalCode,v_numState,v_numCountry,v_bitIsPrimary,
      v_vcAddressName,v_tintAddressOf,v_numContact,v_bitAltContact,v_vcAltContact FROM
      AddressDetails AD WHERE
      AD.numDomainID = v_numDomainId
      AND AD.numRecordID = v_numDivisionId
      AND AD.tintAddressOf = 2
      AND AD.tintAddressType = 2
      AND AD.bitIsPrimary = true;
	 --  numeric(9, 0)
	 --  tinyint
	 --  varchar(100)
	 --  varchar(50)
	 --  varchar(15)
	 --  numeric(9, 0)
	 --  numeric(9, 0)
	 --  varchar(100)
	 --  numeric(9, 0)
      UPDATE OpportunityMaster SET tintShipToType = 2,numShipToAddressID = 0 WHERE numOppId = v_numOppID;
      PERFORM USP_UpdateOppAddress(v_numOppID := v_numOppID,v_byteMode := 1::SMALLINT,p_numAddressID := v_numAddressID,v_vcStreet := v_vcStreet,
      v_vcCity := v_vcCity,v_vcPostalCode := v_vcPostalCode,v_numState := v_numState,
      v_numCountry := v_numCountry,v_vcCompanyName := v_vcCompanyName,
      v_numCompanyId := 0,v_vcAddressName := v_vcAddressName,v_bitCalledFromProcedure := true,
      v_numContact := v_numContact,v_bitAltContact := v_bitAltContact,
      v_vcAltContact := v_vcAltContact);
   end if;

--Bill Address
   IF v_numBillAddressId > 0 then

      select   coalesce(vcStreet,''), coalesce(vcCity,''), coalesce(vcPostalCode,''), coalesce(numState,0), coalesce(numCountry,0), bitIsPrimary, vcAddressName, coalesce(tintAddressOf,0), numContact, bitAltContact, vcAltContact INTO v_vcStreet,v_vcCity,v_vcPostalCode,v_numState,v_numCountry,v_bitIsPrimary,
      v_vcAddressName,v_tintAddressOf,v_numContact,v_bitAltContact,v_vcAltContact FROM
      AddressDetails WHERE
      numDomainID = v_numDomainId
      AND numAddressID = v_numBillAddressId;
      IF v_tintAddressOf = 2 AND v_tintOppType = 2 then
	
         select   coalesce(vcCompanyName,''), coalesce(CompanyInfo.numCompanyId,0) INTO v_vcCompanyName,v_numCompanyId FROM
         AddressDetails
         INNER JOIN
         DivisionMaster
         ON
         AddressDetails.numRecordID = DivisionMaster.numDivisionID
         INNER JOIN
         CompanyInfo
         ON
         DivisionMaster.numCompanyID = CompanyInfo.numCompanyId WHERE
         AddressDetails.numDomainID = v_numDomainId
         AND numAddressID = v_numBillAddressId;
      end if;
	 --  numeric(9, 0)
	 --  tinyint
	 --  varchar(100)
	 --  varchar(50)
	 --  varchar(15)
	 --  numeric(9, 0)
	 --  numeric(9, 0)
	 --  varchar(100)
	 --  numeric(9, 0)
      UPDATE OpportunityMaster SET tintBillToType = 2,numBillToAddressID = 0 WHERE numOppId = v_numOppID;
      PERFORM USP_UpdateOppAddress(v_numOppID := v_numOppID,v_byteMode := 0::SMALLINT,p_numAddressID := v_numBillAddressId,v_vcStreet := v_vcStreet,
      v_vcCity := v_vcCity,v_vcPostalCode := v_vcPostalCode,v_numState := v_numState,
      v_numCountry := v_numCountry,v_vcCompanyName := v_vcCompanyName,
      v_numCompanyId := 0,v_vcAddressName := v_vcAddressName,v_bitCalledFromProcedure := true,
      v_numContact := v_numContact,v_bitAltContact := v_bitAltContact,
      v_vcAltContact := v_vcAltContact);
   end if;
  
--Ship Address
   IF v_intUsedShippingCompany = 92 then -- WILL CALL (PICK UP FROM Warehouse)

      IF EXISTS(SELECT numWareHouseID FROM Warehouses WHERE Warehouses.numDomainID = v_numDomainId AND numWareHouseID = v_numWillCallWarehouseID AND coalesce(numAddressID,0) > 0) then
	
         select   coalesce(vcStreet,''), coalesce(vcCity,''), coalesce(AddressDetails.vcPostalCode,''), coalesce(numState,0), coalesce(numCountry,0), vcAddressName INTO v_vcStreet,v_vcCity,v_vcPostalCode,v_numState,v_numCountry,v_vcAddressName FROM
         Warehouses
         INNER JOIN
         AddressDetails
         ON
         Warehouses.numAddressID = AddressDetails.numAddressID WHERE
         Warehouses.numDomainID = v_numDomainId
         AND numWareHouseID = v_numWillCallWarehouseID;
      ELSE
         select   coalesce(vcWStreet,''), coalesce(vcWCity,''), coalesce(vcWPinCode,''), coalesce(numWState,0), coalesce(numWCountry,0), '' INTO v_vcStreet,v_vcCity,v_vcPostalCode,v_numState,v_numCountry,v_vcAddressName FROM
         Warehouses WHERE
         numDomainID = v_numDomainId
         AND numWareHouseID = v_numWillCallWarehouseID;
      end if;
	 --  numeric(9, 0)
	 --  tinyint
	 --  varchar(100)
	 --  varchar(50)
	 --  varchar(15)
	 --  numeric(9, 0)
	 --  numeric(9, 0)
	 --  varchar(100)
	 --  numeric(9, 0)
      UPDATE OpportunityMaster SET tintShipToType = 2 WHERE numOppId = v_numOppID;
      PERFORM USP_UpdateOppAddress(v_numOppID := v_numOppID,v_byteMode := 1::SMALLINT,p_numAddressID := v_numWillCallWarehouseID,v_vcStreet := v_vcStreet,
      v_vcCity := v_vcCity,v_vcPostalCode := v_vcPostalCode,v_numState := v_numState,
      v_numCountry := v_numCountry,v_vcCompanyName := '',v_numCompanyId := 0,
      v_vcAddressName := v_vcAddressName,v_bitCalledFromProcedure := true,
      v_numContact := 0,v_bitAltContact := false,v_vcAltContact := '');
   ELSEIF v_numShipAddressId > 0
   then

      select   coalesce(vcStreet,''), coalesce(vcCity,''), coalesce(vcPostalCode,''), coalesce(numState,0), coalesce(numCountry,0), bitIsPrimary, vcAddressName, coalesce(tintAddressOf,0), numContact, bitAltContact, vcAltContact INTO v_vcStreet,v_vcCity,v_vcPostalCode,v_numState,v_numCountry,v_bitIsPrimary,
      v_vcAddressName,v_tintAddressOf,v_numContact,v_bitAltContact,v_vcAltContact FROM
      AddressDetails WHERE
      numDomainID = v_numDomainId
      AND numAddressID = v_numShipAddressId;
      IF v_tintAddressOf = 2 AND v_tintOppType = 2 then
	
         select   coalesce(vcCompanyName,''), coalesce(CompanyInfo.numCompanyId,0) INTO v_vcCompanyName,v_numCompanyId FROM
         AddressDetails
         INNER JOIN
         DivisionMaster
         ON
         AddressDetails.numRecordID = DivisionMaster.numDivisionID
         INNER JOIN
         CompanyInfo
         ON
         DivisionMaster.numCompanyID = CompanyInfo.numCompanyId WHERE
         AddressDetails.numDomainID = v_numDomainId
         AND numAddressID = v_numShipAddressId;
      end if;
	 --  numeric(9, 0)
	 --  tinyint
	 --  varchar(100)
	 --  varchar(50)
	 --  varchar(15)
	 --  numeric(9, 0)
	 --  numeric(9, 0)
	 --  varchar(100)
	 --  numeric(9, 0)
      UPDATE OpportunityMaster SET tintShipToType = 2,numShipToAddressID = 0 WHERE numOppId = v_numOppID;
      PERFORM USP_UpdateOppAddress(v_numOppID := v_numOppID,v_byteMode := 1::SMALLINT,p_numAddressID := v_numShipAddressId,v_vcStreet := v_vcStreet,
      v_vcCity := v_vcCity,v_vcPostalCode := v_vcPostalCode,v_numState := v_numState,
      v_numCountry := v_numCountry,v_vcCompanyName := v_vcCompanyName,
      v_numCompanyId := v_numCompanyId,v_vcAddressName := v_vcAddressName,
      v_bitCalledFromProcedure := true,v_numContact := v_numContact,v_bitAltContact := v_bitAltContact,
      v_vcAltContact := v_vcAltContact);
   end if;

-- IMPORTANT:KEEP THIS BELOW ADDRESS UPDATE LOGIC
-- GET TAX APPLICABLE TO DIVISION WHEN ORDER IS CREATED AFTER THAT NOT NEED TO CHANGE IT
   IF v_bitNewOrder = true then

	--INSERT TAX FOR DIVISION   
      IF v_tintOppType = 1 OR (v_tintOppType = 2 and(select coalesce(bitPurchaseTaxCredit,false) from Domain where numDomainId = v_numDomainId) = true) then
	
         INSERT INTO OpportunityMasterTaxItems(numOppId,
			numTaxItemID,
			fltPercentage,
			tintTaxType,
			numTaxID)
         SELECT
         v_numOppID,
			TI.numTaxItemID,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
         FROM
         TaxItems TI
         JOIN
         DivisionTaxTypes DTT
         ON
         TI.numTaxItemID = DTT.numTaxItemID
         CROSS JOIN LATERAL(SELECT
            decTaxValue,
				tintTaxType
            FROM
            fn_CalItemTaxAmt(v_numDomainId,v_numDivisionId,0,TI.numTaxItemID,v_numOppID,false,NULL)) AS TEMPTax
         WHERE
         DTT.numDivisionID = v_numDivisionId
         AND DTT.bitApplicable = true
         UNION
         SELECT
         v_numOppID,
			0,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
         FROM
         DivisionMaster
         CROSS JOIN LATERAL(SELECT
            decTaxValue,
				tintTaxType
            FROM
            fn_CalItemTaxAmt(v_numDomainId,v_numDivisionId,0,0,v_numOppID,true,NULL)) AS TEMPTax
         WHERE
         bitNoTax = false
         AND numDivisionID = v_numDivisionId
         UNION
         SELECT
         v_numOppID
			,1
			,decTaxValue
			,tintTaxType
			,numTaxID
         FROM
         fn_CalItemTaxAmt(v_numDomainId,v_numDivisionId,0,1,v_numOppID,true,NULL);
      end if;
   end if;

--INSERT TAX FOR DIVISION   
   IF v_tintOppType = 1 OR (v_tintOppType = 2 and(select coalesce(bitPurchaseTaxCredit,false) from Domain where numDomainId = v_numDomainId) = true) then

--Delete Tax for Opportunity Items if item deleted 
      DELETE FROM OpportunityItemsTaxItems WHERE numOppId = v_numOppID AND numOppItemID NOT IN(SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId = v_numOppID);

--Insert Tax for Opportunity Items
      INSERT INTO OpportunityItemsTaxItems(numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID)
      SELECT
      v_numOppID,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0
      FROM
      OpportunityItems OI
      JOIN
      ItemTax IT
      ON
      OI.numItemCode = IT.numItemCode
      JOIN
      TaxItems TI
      ON
      TI.numTaxItemID = IT.numTaxItemID
      WHERE
      OI.numOppId = v_numOppID
      AND IT.bitApplicable = true
      AND OI.numoppitemtCode NOT IN(SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId = v_numOppID)
      UNION
      SELECT
      v_numOppID,
	OI.numoppitemtCode,
	0,
	0
      FROM
      OpportunityItems OI
      JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      WHERE
      OI.numOppId = v_numOppID
      AND I.bitTaxable = true
      AND OI.numoppitemtCode NOT IN(SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId = v_numOppID)
      UNION
      SELECT
      v_numOppID,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
      FROM
      OpportunityItems OI
      INNER JOIN
      ItemTax IT
      ON
      IT.numItemCode = OI.numItemCode
      INNER JOIN
      TaxDetails TD
      ON
      TD.numTaxID = IT.numTaxID
      AND TD.numDomainId = v_numDomainId
      WHERE
      OI.numOppId = v_numOppID
      AND OI.numoppitemtCode NOT IN(SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId = v_numOppID);
   end if;
   UPDATE
   OpportunityItems OI
   SET
   vcInclusionDetail = GetOrderAssemblyKitInclusionDetails(OI.numOppId,OI.numoppitemtCode,OI.numUnitHour,1::SMALLINT,true),vcSKU =(CASE
   WHEN EXISTS(SELECT
   OKI.numOppChildItemID
   FROM
   OpportunityKitItems OKI
   INNER JOIN ItemDetails ID ON ID.numItemKitID = OI.numItemCode AND OKI.numChildItemID = ID.numChildItemID
   INNER JOIN Item IInner ON ID.numChildItemID = IInner.numItemCode
   WHERE OKI.numOppItemID = OI.numoppitemtCode AND coalesce(ID.bitUseInDynamicSKU,false) = true)
   THEN
      CONCAT(I.vcItemName,'-',COALESCE((SELECT string_agg(COALESCE((SELECT string_agg(I.vcItemName,',')
         FROM
         OpportunityKitChildItems OKCI
         INNER JOIN
         Item I
         ON
         OKCI.numItemID = I.numItemCode
         WHERE OKCI.numOppChildItemID = OKI.numOppChildItemID),''),'-' ORDER BY coalesce(ID.sintOrder,0))      
      FROM
      OpportunityKitItems OKI
      INNER JOIN ItemDetails ID ON ID.numItemKitID = OI.numItemCode AND OKI.numChildItemID = ID.numChildItemID
      INNER JOIN Item IInner ON ID.numChildItemID = IInner.numItemCode
      WHERE OKI.numOppItemID = OI.numoppitemtCode AND coalesce(ID.bitUseInDynamicSKU,false) = true
      ),''))
   ELSE OI.vcSKU
   END)
   FROM
   Item I
   WHERE
   OI.numItemCode = I.numItemCode AND(numOppId = v_numOppID
   AND coalesce(I.bitKitParent,false) = true);
   UPDATE OpportunityMaster SET monDealAmount = GetDealAmount(v_numOppID,LOCALTIMESTAMP,0::NUMERIC) WHERE numOppId = v_numOppID;
   IF v_tintOppType = 1 AND v_tintOppStatus = 1 then
	
      PERFORM USP_SalesFulfillmentQueue_Save(v_numDomainId,v_numUserCntID,v_numOppID,v_numStatus);
      PERFORM USP_OpportunityMaster_CreateBackOrderPO(v_numDomainId,v_numUserCntID,v_numOppID);
   end if;
   IF EXISTS(SELECT
   numoppitemtCode
   FROM
   OpportunityItems
   INNER JOIN
   Item
   ON
   OpportunityItems.numItemCode = Item.numItemCode
   WHERE
   numOppId = v_numOppID
   AND charItemType = 'P'
   AND coalesce(numWarehouseItmsID,0) = 0
   AND coalesce(bitDropShip,false) = false) then
	
      RAISE EXCEPTION 'WAREHOUSE_REQUIRED';
   end if;
   IF(SELECT COUNT(*) FROM WorkOrder WHERE numDomainId = v_numDomainId AND numOppId = v_numOppID AND coalesce(numOppItemID,0) > 0 AND numOppItemID NOT IN(SELECT OIInner.numoppitemtCode FROM OpportunityItems OIInner WHERE OIInner.numOppId = v_numOppID)) > 0 then
	
      RAISE EXCEPTION 'WORK_ORDER_EXISTS';
   end if;
   IF(SELECT
   COUNT(*)
   FROM
   WorkOrder
   INNER JOIN
   OpportunityItems
   ON
   OpportunityItems.numOppId = v_numOppID
   AND OpportunityItems.numoppitemtCode = WorkOrder.numOppItemID
   WHERE
   WorkOrder.numDomainId = v_numDomainId
   AND WorkOrder.numOppId = v_numOppID
   AND WorkOrder.numWareHouseItemId <> OpportunityItems.numWarehouseItmsID) > 0 then
	
      RAISE EXCEPTION 'CAN_NOT_CHANGE_ASSEMBLY_WAREHOUSE_WORK_ORDER_EXISTS';
   end if;
   IF v_tintOppType = 1 AND v_tintOppStatus = 1 then
	
      PERFORM USP_OpportunityItems_CheckWarehouseMapping(v_numDomainId,v_numOppID);
   ELSEIF v_tintOppType = 2 AND v_tintOppStatus = 1
   then
		PERFORM usp_vendorcosttable_updatecostfrompo(v_numDomainId,v_numUserCntID,v_numOppID);

      IF EXISTS(SELECT
      OM.numOppId
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OM.numOppId = OI.numOppId
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      WHERE
      OM.numDomainId = v_numDomainId
      AND OM.tintopptype = 2
      AND OM.tintoppstatus = 1
      AND OM.numOppId <> v_numOppID
      AND I.numItemCode IN(SELECT IInner.numItemCode FROM OpportunityItems OIInner INNER JOIN Item IInner ON OIInner.numItemCode = IInner.numItemCode WHERE OIInner.numOppId = v_numOppID AND coalesce(IInner.bitAsset,false) = true)) then
		
         RAISE EXCEPTION 'CAN_NOT_CREATE_MULTIPLE_PO_FOR_ASSET_ITEMS';
      end if;
      PERFORM USP_OpportunityMaster_AddVendor(v_numDomainId,v_numOppID);
   end if;
   -- COMMIT
EXCEPTION WHEN OTHERS THEN
      -- ROLLBACK TRANSACTION
v_numOppID := 0;
      GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
 END;
END; $$;

