-- Stored procedure definition script USP_OppUpDateBizDocs_AmountPaid for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OppUpDateBizDocs_AmountPaid(v_numOppBizDocsId NUMERIC(9,0) DEFAULT null,                                                      
 v_UserCntID NUMERIC(9,0) DEFAULT null,                                                      
 v_amountPaid DECIMAL(20,5) DEFAULT null,                                                      
 v_numPaymentMethod NUMERIC DEFAULT 0,                                      
 v_numDepoistToChartAcntId NUMERIC(9,0) DEFAULT 0,                                    
 v_numDomainId NUMERIC(9,0) DEFAULT 0,                                  
 v_vcReference VARCHAR(200) DEFAULT null,                                  
 v_vcMemo VARCHAR(50) DEFAULT null,                            
 v_bitIntegratedToAcnt BOOLEAN DEFAULT NULL,                        
 INOUT v_numBizDocsPaymentDetId NUMERIC(9,0) DEFAULT 0 ,
 v_bitDeferredIncome BOOLEAN DEFAULT NULL,                
 v_sintDeferredIncomePeriod SMALLINT DEFAULT 0,                
 v_dtDeferredIncomeStartDate TIMESTAMP DEFAULT NULL,                
 v_numContractId NUMERIC(9,0) DEFAULT 0,          
 v_bitSalesDeferredIncome BOOLEAN DEFAULT NULL,        
 v_bitDisable BOOLEAN DEFAULT NULL,
 v_numCardTypeID NUMERIC(9,0) DEFAULT NULL,
 v_bitCardAuthorized BOOLEAN DEFAULT false,
 v_bitAmountCaptured BOOLEAN DEFAULT false,
 v_vcSignatureFile VARCHAR(100) DEFAULT '', INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_AmtPaidComplete  BOOLEAN;
   v_OppID  NUMERIC(9,0);                            
   v_OldAmountPaid  DECIMAL(20,5);
   v_TotalAmtPaid  DECIMAL(20,5);                     
   v_tintOppType  SMALLINT;                    
   v_numBizDocId  NUMERIC(9,0); 
   v_numAuthoritativeBizDocsId  NUMERIC(9,0);                    
   v_numCurrentBizDocsPaymentDetId  NUMERIC(9,0); 
   v_numCurrencyID  NUMERIC(9,0);
   v_fltExchangeRate  DOUBLE PRECISION;
   v_monAmount  DECIMAL(20,5);              
   v_i  INTEGER;              
   v_dtDueDate  TIMESTAMP;              
   v_numSignID  NUMERIC;
   v_numReferenceID  NUMERIC;
BEGIN
   v_numSignID := NULL;
   v_numReferenceID := NULL;


   v_monAmount := 0;              
   v_AmtPaidComplete := false;
   IF v_numContractId = 0 then
      v_numContractId := NULL;
   end if;                
   IF v_sintDeferredIncomePeriod = 0 then
      v_sintDeferredIncomePeriod := 1;
   end if;                
   IF v_dtDeferredIncomeStartDate = 'Jan  1 1753 12:00AM' then
      v_dtDeferredIncomeStartDate := TIMEZONE('UTC',now());
   end if;  
	        
   select   numoppid, numBizDocId INTO v_OppID,v_numBizDocId from OpportunityBizDocs where numOppBizDocsId = v_numOppBizDocsId;                      
   select   monAmount INTO v_OldAmountPaid From OpportunityBizDocsDetails Where numBizDocsPaymentDetId = v_numBizDocsPaymentDetId;                      
   select   tintopptype, coalesce(numCurrencyID,0), numContactId INTO v_tintOppType,v_numCurrencyID,v_numReferenceID From OpportunityMaster Where numOppId = v_OppID;                     

   IF LENGTH(v_vcSignatureFile) > 0 then
 
      IF(select count(*) from SignatureDetail where numDomainID = v_numDomainId	and numReferenceID = v_numReferenceID and tintModuleType = 1) > 0 then
	
         UPDATE SignatureDetail set vcSignatureFile = v_vcSignatureFile where numDomainID = v_numDomainId and numReferenceID = v_numReferenceID and tintModuleType = 1;
         select   numSignID INTO v_numSignID from SignatureDetail where numDomainID = v_numDomainId	and numReferenceID = v_numReferenceID and tintModuleType = 1;
      ELSE
         insert into SignatureDetail(vcSignatureFile,tintModuleType,numReferenceID,numDomainID)
			values(v_vcSignatureFile,1,v_numReferenceID,v_numDomainId);
		
         v_numSignID := CURRVAL('SignatureDetail_seq');
      end if;
   end if;

   if v_numCurrencyID = 0 then 
      v_fltExchangeRate := 1;
   else 
      v_fltExchangeRate := GetExchangeRate(v_numDomainId,v_numCurrencyID);
   end if;                   
	              
   If v_tintOppType = 1 then
      select   coalesce(numAuthoritativeSales,0) INTO v_numAuthoritativeBizDocsId From AuthoritativeBizDocs Where numDomainId = v_numDomainId;
   ELSEIF v_tintOppType = 2
   then
      select   coalesce(numAuthoritativePurchase,0) INTO v_numAuthoritativeBizDocsId From AuthoritativeBizDocs Where numDomainId = v_numDomainId;
   end if;                     
	--  print @numAuthoritativeBizDocsId                    
	--  print @numBizDocID             



       
   If v_numBizDocsPaymentDetId = 0 then
   
      Update OpportunityBizDocs set monAmountPaid = coalesce(monAmountPaid,0)+v_amountPaid,numModifiedBy = v_UserCntID,
      dtModifiedDate = TIMEZONE('UTC',now())
      Where numoppid = v_OppID And numOppBizDocsId = v_numOppBizDocsId;
      select   coalesce(monAmountPaid,0) INTO v_TotalAmtPaid from OpportunityBizDocs where numOppBizDocsId = v_numOppBizDocsId;
      if  v_TotalAmtPaid >= coalesce(GetDealAmount(v_OppID,TIMEZONE('UTC',now()),v_numOppBizDocsId),
      0) then
   
         update OpportunityBizDocs set dtShippedDate = TIMEZONE('UTC',now()) where  numOppBizDocsId = v_numOppBizDocsId;
      end if;

   -- To update AmountPaid only if it is Authoritative BizDocs                    
      If v_numAuthoritativeBizDocsId = v_numBizDocId then
     
         v_AmtPaidComplete := true;                
--      Print '@numBizDocId==='+convert(varchar(100),@numBizDocId)
         Update OpportunityMaster set numPClosingPercent = coalesce(numPClosingPercent,0)+v_amountPaid  Where numOppId = v_OppID;                         
      -- To insert Payment Details in OpportunityBizDocsDetails Table for Accounting Purpose                                         
         Insert into OpportunityBizDocsDetails(numBizDocsId,numPaymentMethod,monAmount,numDepoistToChartAcntId,numDomainId,
      vcReference,vcMemo,bitIntegratedToAcnt,bitAuthoritativeBizDocs,bitSalesDeferredIncome,bitDeferredIncome,
		sintDeferredIncomePeriod,dtDeferredIncomeStartDate,numContractId,numCreatedBy,dtCreationDate,numCurrencyID,fltExchangeRate,tintPaymentType,numCardTypeID,bitCardAuthorized,bitAmountCaptured,numSignID)
      Values(v_numOppBizDocsId,v_numPaymentMethod,v_amountPaid,v_numDepoistToChartAcntId,v_numDomainId,v_vcReference,v_vcMemo,
      v_bitIntegratedToAcnt,1,v_bitSalesDeferredIncome,v_bitDeferredIncome,v_sintDeferredIncomePeriod,v_dtDeferredIncomeStartDate,v_numContractId,v_UserCntID,TIMEZONE('UTC',now()),v_numCurrencyID,v_fltExchangeRate,1,v_numCardTypeID,v_bitCardAuthorized,v_bitAmountCaptured,v_numSignID);
      
         v_numCurrentBizDocsPaymentDetId := CURRVAL('OpportunityBizDocsDetails_seq');
         v_numBizDocsPaymentDetId := v_numCurrentBizDocsPaymentDetId;
         RAISE NOTICE '@numCurrentBizDocsPaymentDetId=========%',SUBSTR(CAST(v_numCurrentBizDocsPaymentDetId AS VARCHAR(1000)),1,1000);
         v_i := 0;
         v_monAmount := v_amountPaid/v_sintDeferredIncomePeriod;
         While v_i < v_sintDeferredIncomePeriod LOOP
            v_dtDueDate := v_dtDeferredIncomeStartDate+CAST(v_i || 'month' as interval);
            Insert into OpportunityBizDocsPaymentDetails(numBizDocsPaymentDetId,monAmount,dtDueDate,bitIntegrated)
        Values(v_numCurrentBizDocsPaymentDetId,v_monAmount,v_dtDueDate,0);
        
            v_i := v_i::bigint+1;
         END LOOP;
      Else
         Insert into OpportunityBizDocsDetails(numBizDocsId,numPaymentMethod,monAmount,numDepoistToChartAcntId,numDomainId,
      vcReference,vcMemo,bitIntegratedToAcnt,bitAuthoritativeBizDocs,numCreatedBy,dtCreationDate,numCurrencyID,fltExchangeRate,tintPaymentType,numCardTypeID,bitCardAuthorized,bitAmountCaptured,numSignID)
      Values(v_numOppBizDocsId,v_numPaymentMethod,v_amountPaid,v_numDepoistToChartAcntId,v_numDomainId,v_vcReference,v_vcMemo,
      v_bitIntegratedToAcnt,0,v_UserCntID,TIMEZONE('UTC',now()),v_numCurrencyID,v_fltExchangeRate,1,v_numCardTypeID,v_bitCardAuthorized,v_bitAmountCaptured,v_numSignID);
      
         v_numBizDocsPaymentDetId := CURRVAL('OpportunityBizDocsDetails_seq');
      end if;
   Else
      if v_bitDisable = false then
   
         Update OpportunityBizDocs Set monAmountPaid = coalesce(monAmountPaid,0) -v_OldAmountPaid+v_amountPaid,numModifiedBy = v_UserCntID,dtModifiedDate = TIMEZONE('UTC',now())
         Where numoppid = v_OppID And numOppBizDocsId = v_numOppBizDocsId;
         select   coalesce(monAmountPaid,0) INTO v_TotalAmtPaid from OpportunityBizDocs where       numOppBizDocsId = v_numOppBizDocsId;
         if  v_TotalAmtPaid >= coalesce(GetDealAmount(v_OppID,TIMEZONE('UTC',now()),v_numOppBizDocsId),
         0) then
	   
            update OpportunityBizDocs set dtShippedDate = TIMEZONE('UTC',now()) where  numOppBizDocsId = v_numOppBizDocsId;
         end if;                 
                       
     -- To update AmountPaid only if it is Authoritative BizDocs                    
         If v_numAuthoritativeBizDocsId = v_numBizDocId then
            v_AmtPaidComplete := true;
         end if;
         Update OpportunityMaster Set  numPClosingPercent = coalesce(numPClosingPercent,0) -v_OldAmountPaid+v_amountPaid Where numOppId = v_OppID;                         
                         
     --To Update in Payment Details OpportunityBizDocsDetails                      
         Update OpportunityBizDocsDetails Set numPaymentMethod = v_numPaymentMethod,monAmount = v_amountPaid,vcReference = v_vcReference,
         vcMemo = v_vcMemo,bitSalesDeferredIncome = v_bitSalesDeferredIncome,
         bitDeferredIncome = v_bitDeferredIncome,sintDeferredIncomePeriod = v_sintDeferredIncomePeriod,
         dtDeferredIncomeStartDate = v_dtDeferredIncomeStartDate,
         numContractId = v_numContractId Where numBizDocsPaymentDetId = v_numBizDocsPaymentDetId;                        
                 
     --To Update in Payment Details OpportunityBizDocsPaymentDetails                  
         Delete FROM OpportunityBizDocsPaymentDetails Where numBizDocsPaymentDetId = v_numBizDocsPaymentDetId;
         v_i := 0;
         v_monAmount := v_amountPaid/v_sintDeferredIncomePeriod;
         While v_i < v_sintDeferredIncomePeriod LOOP
            v_dtDueDate := v_dtDeferredIncomeStartDate+CAST(v_i || 'month' as interval);
            Insert into OpportunityBizDocsPaymentDetails(numBizDocsPaymentDetId,monAmount,dtDueDate,bitIntegrated)
      Values(v_numBizDocsPaymentDetId,v_monAmount,v_dtDueDate,0);
      
            v_i := v_i::bigint+1;
         END LOOP;
      Else        
     --To Update in Payment Details OpportunityBizDocsDetails                      
         Update OpportunityBizDocsDetails Set numPaymentMethod = v_numPaymentMethod,vcReference = v_vcReference,vcMemo = v_vcMemo Where numBizDocsPaymentDetId = v_numBizDocsPaymentDetId;
      end if;
   end if; 

   open SWV_RefCur for
   select v_AmtPaidComplete;
   RETURN;
END; $$;                    



