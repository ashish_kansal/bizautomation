-- Stored procedure definition script USP_GetUSAEpayDtls for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetUSAEpayDtls(v_numDomainID NUMERIC(9,0),
v_intPaymentGateway INTEGER DEFAULT 0,
v_numSiteId INTEGER DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_intPaymentGateway = 0 then
	
      IF coalesce(v_numSiteId,0) > 0 AND(SELECT coalesce(intPaymentGateWay,0) FROM Sites WHERE numDomainID = v_numDomainID AND numSiteID = v_numSiteId) > 0 then
		
         open SWV_RefCur for
         SELECT
         S.intPaymentGateWay,
				coalesce(vcFirstFldValue,'') AS vcFirstFldValue,
				coalesce(vcSecndFldValue,'') AS vcSecndFldValue,
				coalesce(vcThirdFldValue,'') AS vcThirdFldValue,
				coalesce(bitTest,false) AS bitTest,
				coalesce(D.bitSaveCreditCardInfo,false) AS bitSaveCreditCardInfo
         FROM
         Sites S
         INNER JOIN
         Domain D
         ON
         S.numDomainID = D.numDomainId
         LEFT JOIN
         PaymentGatewayDTLID PD
         ON
         PD.intPaymentGateWay = S.intPaymentGateWay
         WHERE
         S.numDomainID = v_numDomainID
         AND PD.numDomainID = v_numDomainID
         AND coalesce(PD.numSiteId,0) = v_numSiteId;
      ELSE
         open SWV_RefCur for
         SELECT
         D.intPaymentGateWay,
				coalesce(vcFirstFldValue,'') AS vcFirstFldValue,
				coalesce(vcSecndFldValue,'') AS vcSecndFldValue,
				coalesce(vcThirdFldValue,'') AS vcThirdFldValue,
				coalesce(bitTest,false) AS bitTest,
				coalesce(D.bitSaveCreditCardInfo,false) AS bitSaveCreditCardInfo
         FROM
         Domain D
         LEFT JOIN
         PaymentGatewayDTLID PD
         ON
         PD.intPaymentGateWay = D.intPaymentGateWay
         WHERE
         D.numDomainId = v_numDomainID
         AND PD.numDomainID = v_numDomainID
         AND coalesce(PD.numSiteId,0) = 0;
      end if;
   ELSEIF v_intPaymentGateway > 0
   then
	
      open SWV_RefCur for
      SELECT intPaymentGateWay,
		coalesce(vcFirstFldValue,'') AS vcFirstFldValue,
		coalesce(vcSecndFldValue,'') AS vcSecndFldValue,
		coalesce(vcThirdFldValue,'') AS vcThirdFldValue,
		coalesce(bitTest,false) AS bitTest
      FROM   PaymentGatewayDTLID PD
      WHERE
      PD.numDomainID = v_numDomainID
      AND PD.intPaymentGateWay = v_intPaymentGateway
      AND PD.numSiteId = v_numSiteId;
   end if;
   RETURN;
END; $$;


