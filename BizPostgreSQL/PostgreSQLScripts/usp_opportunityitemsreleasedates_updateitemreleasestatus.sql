-- Stored procedure definition script USP_OpportunityItemsReleaseDates_UpdateItemReleaseStatus for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OpportunityItemsReleaseDates_UpdateItemReleaseStatus(v_ID NUMERIC(18,0),
	v_numDomainID NUMERIC(18,0),
	v_numOppID NUMERIC(18,0),
	v_numOppItemID NUMERIC(18,0),
	v_dtReleaseDate DATE,
	v_numQuantity NUMERIC(18,0),
	v_tintReleaseStatus SMALLINT,
	v_numPurchasedOrderID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
    my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
   BEGIN
      -- BEGIN TRANSACTION
IF v_ID = -1 then
	
         INSERT INTO OpportunityItemsReleaseDates(numDomainID,
			numOppId,
			numOppItemID,
			dtReleaseDate,
			numQty,
			tintStatus,
			numPurchasedOrderID)
		VALUES(v_numDomainID,
			v_numOppID,
			v_numOppItemID,
			LOCALTIMESTAMP ,
			v_numQuantity,
			v_tintReleaseStatus,
			v_numPurchasedOrderID);
      ELSE
         UPDATE
         OpportunityItemsReleaseDates
         SET
         tintStatus = v_tintReleaseStatus,numPurchasedOrderID = v_numPurchasedOrderID
         WHERE
         numID = v_ID;
      end if;
      IF(SELECT
      SUM(OIRD.numQty) -(OI.numUnitHour*fn_UOMConversion(coalesce(OI.numUOMId,0),Item.numItemCode,Item.numDomainID,coalesce(Item.numBaseUnit,0)))
      FROM
      OpportunityItems OI
      INNER JOIN
      Item
      ON
      OI.numItemCode = Item.numItemCode
      INNER JOIN
      OpportunityItemsReleaseDates OIRD
      ON
      OI.numOppId = OIRD.numOppId
      AND OI.numoppitemtCode = OIRD.numOppItemID
      WHERE
      OI.numOppId = v_numOppID
      AND OI.numoppitemtCode = v_numOppItemID
      GROUP BY
      OI.numoppitemtCode,Item.numBaseUnit,Item.numItemCode,Item.numDomainID,
      Item.numSaleUnit,OI.numUnitHour,OI.numUOMId) > 0 then
	
         RAISE EXCEPTION 'Release qty is more than ordered qty.';
      end if;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
  	GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 RAISE EXCEPTION '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;



