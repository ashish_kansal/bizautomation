-- Stored procedure definition script USP_GetIncomeExpenseStatement for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetIncomeExpenseStatement(v_numDomainId NUMERIC(9,0),                                          
v_dtFromDate TIMESTAMP,                                        
v_dtToDate TIMESTAMP,
v_ClientTimeZoneOffset INTEGER, --Added by Chintan to enable calculation of date according to client machine                                                   
v_numAccountClass NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_CURRENTPL  DECIMAL(20,5);  
   v_PLOPENING  DECIMAL(20,5);  
   v_PLCHARTID  NUMERIC(8,0);  
   v_numFinYear  INTEGER;  
  
   v_dtFinYearFrom  TIMESTAMP;
   v_monIncome  DECIMAL(20,5);  
   v_monCOGS  DECIMAL(20,5);  
   v_monOtherIncome  DECIMAL(20,5);  
   v_monOtherExpense  DECIMAL(20,5);  
   v_monExpenses  DECIMAL(20,5);
BEGIN
   SELECT numFinYearId INTO v_numFinYear FROM FinancialYear WHERE dtPeriodFrom <= v_dtFromDate AND
   dtPeriodTo >= v_dtFromDate AND numDomainId = v_numDomainId;  
  
   SELECT dtPeriodFrom INTO v_dtFinYearFrom FROM FinancialYear WHERE dtPeriodFrom <= v_dtFromDate AND
   dtPeriodTo >= v_dtFromDate AND numDomainId = v_numDomainId;  
  
   v_dtFromDate := v_dtFromDate+CAST(v_ClientTimeZoneOffset || 'minute' as interval);  
   v_dtToDate := v_dtToDate+CAST(v_ClientTimeZoneOffset || 'minute' as interval);  
  
  
--select * from view_journal where numDomainid=72  
  
   DROP TABLE IF EXISTS tt_PLSUMMARY CASCADE;
   CREATE TEMPORARY TABLE tt_PLSUMMARY 
   (
      numAccountId NUMERIC(9,0),
      vcAccountName VARCHAR(250),
      numParntAcntTypeID NUMERIC(9,0),
      vcAccountDescription VARCHAR(250),
      vcAccountCode VARCHAR(50),
      Opening DECIMAL(20,5),
      Debit DECIMAL(20,5),
      Credit DECIMAL(20,5),
      bitIsSubAccount BOOLEAN
   );  
 

   DROP TABLE IF EXISTS tt_VIEW_JOURNAL CASCADE;
   CREATE TEMPORARY TABLE tt_VIEW_JOURNAL AS
      select  numDomainID,Debit,Credit,COAvcAccountCode 
      from VIEW_JOURNAL where  datEntry_Date BETWEEN  v_dtFromDate AND v_dtToDate
      AND numDomainID = v_numDomainId AND (numAccountClass = v_numAccountClass OR v_numAccountClass = 0); 

   INSERT INTO  tt_PLSUMMARY
   SELECT COA.numAccountId,vcAccountName,numParntAcntTypeID,vcAccountDescription,vcAccountCode,
0 AS OPENING,
coalesce((SELECT SUM(Debit) FROM tt_VIEW_JOURNAL VJ
      WHERE VJ.numDomainId = v_numDomainId AND
      VJ.COAvcAccountCode ilike COA.vcAccountCode || '%'),
   0) /*VJ.numAccountId=COA.numAccountId*/   
   AS DEBIT,
coalesce((SELECT SUM(Credit) FROM tt_VIEW_JOURNAL VJ
      WHERE VJ.numDomainId = v_numDomainId AND
      VJ.COAvcAccountCode ilike COA.vcAccountCode || '%'),
   0) /*VJ.numAccountId=COA.numAccountId*/   
   AS CREDIT,coalesce(COA.bitIsSubAccount,false)
   FROM Chart_Of_Accounts COA
   WHERE   
--COA.numAccountId not in (select AC.numAccountID from AccountingCharges AC,AccountingChargeTypes AT where  
-- AC.numChargeTypeId=AT.numChargeTypeId and AT.chChargeCode='CG' and AC.numDomainID=@numDomainId) and  
   COA.numDomainId = v_numDomainId AND COA.bitActive = true AND
      (COA.vcAccountCode ilike '0103%' OR
   COA.vcAccountCode ilike '0104%' OR
   COA.vcAccountCode ilike '0106%');  
  
  
   DROP TABLE IF EXISTS tt_PLOUTPUT CASCADE;
   CREATE TEMPORARY TABLE tt_PLOUTPUT 
   (
      numAccountId NUMERIC(9,0),
      vcAccountName VARCHAR(250),
      numParntAcntTypeID NUMERIC(9,0),
      vcAccountDescription VARCHAR(250),
      vcAccountCode VARCHAR(50),
      Opening DECIMAL(20,5),
      Debit DECIMAL(20,5),
      Credit DECIMAL(20,5)
   );  
  
   INSERT INTO tt_PLOUTPUT
   SELECT ATD.numAccountTypeID,CAST(ATD.vcAccountType AS VARCHAR(250)),ATD.numParentID, CAST('' AS VARCHAR(250)),ATD.vcAccountCode,
 coalesce(SUM(Opening),0) AS Opening,
coalesce(SUM(Debit),0) AS Debit,coalesce(SUM(Credit),0) AS Credit
   FROM
   AccountTypeDetail ATD RIGHT OUTER JOIN
   tt_PLSUMMARY PL ON
   PL.vcAccountCode ilike ATD.vcAccountCode || '%'
   AND ATD.numDomainID = v_numDomainId AND
(ATD.vcAccountCode ilike '0103%' OR
   ATD.vcAccountCode ilike '0104%' OR
   ATD.vcAccountCode ilike '0106%')
   WHERE PL.bitIsSubAccount = false
   GROUP BY
   ATD.numAccountTypeID,ATD.vcAccountCode,ATD.vcAccountType,ATD.numParentID;  
  
  
  
--------------------------------------------------------  
-- GETTING P&L VALUE  
  
   v_CURRENTPL := 0;   
   v_PLOPENING := 0;  
  
   select   coalesce(SUM(Opening),0)+coalesce(SUM(Debit),0) -coalesce(SUM(Credit),0) INTO v_CURRENTPL FROM
   tt_PLOUTPUT P WHERE
   vcAccountCode IN('0103','0104','0106');  
  
--SELECT @CURRENTPL = @CURRENTPL - ISNULL(SUM(Opening),0)+ISNULL(sum(Debit),0)-ISNULL(sum(Credit),0) FROM  
--#PLOutPut P WHERE   
--vcAccountCode IN ('0104')  
  
   v_CURRENTPL := v_CURRENTPL*(-1);  
  
   select   COA.numAccountId INTO v_PLCHARTID FROM Chart_Of_Accounts COA WHERE numDomainId = v_numDomainId AND
   bitProfitLoss = true;  
  
--SELECT   @PLOPENING = isnull((SELECT isnull(monOpening,0) from CHARTACCOUNTOPENING CAO WHERE  
--numFinYearId=@numFinYear and numDomainID=@numDomainId and  
--CAO.numAccountId=COA.numAccountId),0)  +   
--ISNULL((SELECT sum(Debit-Credit) FROM view_journal VJ  
--WHERE VJ.numDomainId=@numDomainId AND  
-- VJ.numAccountId=COA.numAccountId AND  
-- datEntry_Date BETWEEN @dtFinYearFrom AND  @dtFromDate-1),0)  
-- FROM Chart_of_Accounts COA WHERE numDomainID=@numDomainId and  
--bitProfitLoss=1;  
  
--SELECT  @CURRENTPL=@CURRENTPL +        
--ISNULL((SELECT sum(Debit-Credit) FROM view_journal VJ  
--WHERE VJ.numDomainId=@numDomainId AND  
-- VJ.numAccountId=@PLCHARTID AND  
-- datEntry_Date BETWEEN @dtFromDate AND @dtToDate),0) ;  
  
   v_CURRENTPL := v_CURRENTPL*(-1);  
--SELECT @PLCHARTID,@CURRENTPL,@PLOPENING  
  
-----------------------------------------------------------------  
  
   DROP TABLE IF EXISTS tt_PLSHOW CASCADE;
   CREATE TEMPORARY TABLE tt_PLSHOW 
   (
      numAccountId NUMERIC(9,0),
      vcAccountName VARCHAR(250),
      numParntAcntTypeID NUMERIC(9,0),
      vcAccountDescription VARCHAR(250),
      vcAccountCode VARCHAR(50),
      Opening DECIMAL(20,5),
      Debit DECIMAL(20,5),
      Credit DECIMAL(20,5),
      Balance  VARCHAR(50),
      AccountCode1  VARCHAR(100),
      vcAccountName1 VARCHAR(250),
      Type INTEGER
   );  
  
   DROP TABLE IF EXISTS tt_PLSHOW1 CASCADE;
   CREATE TEMPORARY TABLE tt_PLSHOW1 
   (
      numAccountId NUMERIC(9,0),
      vcAccountName VARCHAR(250),
      numParntAcntTypeID NUMERIC(9,0),
      vcAccountDescription VARCHAR(250),
      vcAccountCode VARCHAR(50),
      Opening DECIMAL(20,5),
      Debit DECIMAL(20,5),
      Credit DECIMAL(20,5),
      Balance  VARCHAR(50),
      AccountCode1  VARCHAR(100),
      vcAccountName1 VARCHAR(250),
      Type INTEGER
   );  
  
   DROP TABLE IF EXISTS tt_PLSHOWGRID1 CASCADE;
   CREATE TEMPORARY TABLE tt_PLSHOWGRID1 
   (
      numAccountId NUMERIC(9,0),
      vcAccountName VARCHAR(250),
      numParntAcntTypeID NUMERIC(9,0),
      vcAccountDescription VARCHAR(250),
      vcAccountCode VARCHAR(50),
      Opening DECIMAL(20,5),
      Debit DECIMAL(20,5),
      Credit DECIMAL(20,5),
      Balance VARCHAR(50),
      AccountCode1  VARCHAR(100),
      vcAccountName1 VARCHAR(250),
      Type INTEGER
   );  
  
  
   DROP TABLE IF EXISTS tt_PLSHOWGRID2 CASCADE;
   CREATE TEMPORARY TABLE tt_PLSHOWGRID2 
   (
      numAccountId NUMERIC(9,0),
      vcAccountName VARCHAR(250),
      numParntAcntTypeID NUMERIC(9,0),
      vcAccountDescription VARCHAR(250),
      vcAccountCode VARCHAR(50),
      Opening DECIMAL(20,5),
      Debit DECIMAL(20,5),
      Credit DECIMAL(20,5),
      Balance  VARCHAR(50),
      AccountCode1  VARCHAR(100),
      vcAccountName1 VARCHAR(250),
      Type INTEGER
   );  
  
   select   coalesce(SUM(Credit) -SUM(Debit),0) INTO v_monIncome FROM tt_PLSUMMARY P WHERE vcAccountCode ilike '010301%' AND P.bitIsSubAccount = false;  
   
   select   coalesce(coalesce(SUM(Debit),0) -coalesce(SUM(Credit),0),0) INTO v_monOtherIncome FROM tt_PLSUMMARY P WHERE vcAccountCode ilike '010302%' AND P.bitIsSubAccount = false;  
   
   select   coalesce(coalesce(SUM(Debit),0) -coalesce(SUM(Credit),0),0) INTO v_monExpenses FROM tt_PLSUMMARY P WHERE vcAccountCode ilike '010401%' AND P.bitIsSubAccount = false;  
   
   select   coalesce(coalesce(SUM(Debit),0) -coalesce(SUM(Credit),0),0) INTO v_monOtherExpense FROM tt_PLSUMMARY P WHERE vcAccountCode ilike '010402%' AND P.bitIsSubAccount = false;  
   
  
   v_monOtherIncome := v_monOtherIncome*(-1);  
   
   
--SELECT  @monCOGS= dbo.GetCOGSValue(@numDomainId,@dtFromDate,@dtToDate )  
   
 --WHERE vcAccountCode LIKE '010403--'   
   select   coalesce(coalesce(SUM(Debit),0) -coalesce(SUM(Credit),0),0) INTO v_monCOGS FROM tt_PLSUMMARY P WHERE vcAccountCode ilike '0106%' AND P.bitIsSubAccount = false;  
   
   ALTER TABLE tt_PLSUMMARY
   DROP COLUMN bitIsSubAccount;  
  
  
   
   INSERT INTO tt_PLSHOW
   SELECT CAST(0 AS NUMERIC(9,0)),CAST('Direct Income' AS VARCHAR(250)),CAST(0 AS NUMERIC(9,0)),CAST('Direct Income' AS VARCHAR(250)),CAST('0101A' AS VARCHAR(50)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST('' AS VARCHAR(50)) ,CAST(CAST('' AS VARCHAR(50)) AS VARCHAR(100)),CAST('Direct Income' AS VARCHAR(250)),2
   UNION
   SELECT numAccountId ,vcAccountName ,
numParntAcntTypeID ,vcAccountDescription ,
CAST('0101A1' AS VARCHAR(50)),Opening ,Debit ,Credit, CAST((Credit -Debit) AS VARCHAR(50)) AS Balance,
CAST(vcAccountCode AS VARCHAR(100)) AS  AccountCode1,
 CASE
   WHEN LENGTH(P.vcAccountCode) > 4 THEN REPEAT('&nbsp;',LENGTH('0101A1') -4) || P.vcAccountName
   ELSE P.vcAccountName
   END AS vcAccountName1, 1 AS Type
   FROM tt_PLSUMMARY P
   WHERE vcAccountCode ilike '010301%'   
-- WHERE numAccountId IN (  
-- SELECT IT.numIncomeChartAcntId FROM Item IT where IT.numDomainID=@numDomainId )  
   UNION
   SELECT CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST('0101B' AS VARCHAR(50)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST('________________' AS VARCHAR(50)) ,CAST(CAST('' AS VARCHAR(50)) AS VARCHAR(100)),CAST('' AS VARCHAR(250)),2
   UNION
   SELECT CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST('0101B1' AS VARCHAR(50)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST(v_monIncome AS VARCHAR(50)),CAST(CAST('' AS VARCHAR(50)) AS VARCHAR(100)),CAST('A) Total Direct Income' AS VARCHAR(250)),2
   UNION
   SELECT CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST('0101B2' AS VARCHAR(50)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST('' AS VARCHAR(50)) ,CAST(CAST('&nbsp;' AS VARCHAR(50)) AS VARCHAR(100)),CAST('' AS VARCHAR(250)),2
   UNION
   SELECT CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST('0101C' AS VARCHAR(50)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST('' AS VARCHAR(50)) ,CAST(CAST('' AS VARCHAR(50)) AS VARCHAR(100)),CAST('Cost of Goods Sold' AS VARCHAR(250)),2
   UNION
   SELECT numAccountId ,vcAccountName ,
numParntAcntTypeID ,vcAccountDescription ,
CAST('0101C1' AS VARCHAR(50)),Opening ,Debit ,Credit, CAST((Debit -Credit) AS VARCHAR(50)) AS Balance,
CAST(vcAccountCode AS VARCHAR(100)) AS  AccountCode1,
 CASE
   WHEN LENGTH('0101C1') > 4 THEN REPEAT('&nbsp;',LENGTH('0101C1') -4) || P.vcAccountName
   ELSE P.vcAccountName
   END AS vcAccountName1, 1 AS Type
   FROM tt_PLSUMMARY P  
 --WHERE vcAccountCode LIKE '010403--'   
   WHERE vcAccountCode ilike '0106%'   
--select -1,'',0,'','0101C1',0,0,0,CAST(@monCOGS AS VARCHAR(50)),'','COGS',1  
   UNION
   SELECT CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST('0101C2' AS VARCHAR(50)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST('________________' AS VARCHAR(50)) ,CAST(CAST('' AS VARCHAR(50)) AS VARCHAR(100)),CAST('' AS VARCHAR(250)),2
   UNION
   SELECT CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST('0101C3' AS VARCHAR(50)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST(v_monCOGS AS VARCHAR(50)),CAST(CAST('' AS VARCHAR(50)) AS VARCHAR(100)),CAST('B) Total COGS' AS VARCHAR(250)),2
   UNION
   SELECT CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST('0101C4' AS VARCHAR(50)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST('________________' AS VARCHAR(50)) ,CAST(CAST('' AS VARCHAR(50)) AS VARCHAR(100)),CAST('' AS VARCHAR(250)),2
   UNION
   SELECT CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST('0101C5' AS VARCHAR(50)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST(v_monIncome -v_monCOGS AS VARCHAR(50)),CAST(CAST('' AS VARCHAR(50)) AS VARCHAR(100)),CAST('Gross Profit= (A- B)' AS VARCHAR(250)),2
   UNION
   SELECT CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST('0101C6' AS VARCHAR(50)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST('&nbsp;' AS VARCHAR(50)) ,CAST(CAST('' AS VARCHAR(50)) AS VARCHAR(100)),CAST('' AS VARCHAR(250)),2
   UNION
   SELECT CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST('0101D' AS VARCHAR(50)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST('' AS VARCHAR(50)) ,CAST(CAST('' AS VARCHAR(50)) AS VARCHAR(100)),CAST('Direct Expenses' AS VARCHAR(250)),2
   UNION
   SELECT numAccountId ,vcAccountName ,
numParntAcntTypeID ,vcAccountDescription ,
CAST('0101D1' AS VARCHAR(50)),Opening ,Debit ,Credit,CAST((Debit -Credit) AS VARCHAR(50)) AS Balance,
CAST(vcAccountCode AS VARCHAR(100)) AS  AccountCode1,
 CASE
   WHEN LENGTH('0101D1') > 4 THEN REPEAT('&nbsp;',LENGTH('0101D1') -4) || P.vcAccountName
   ELSE P.vcAccountName
   END AS vcAccountName1, 1 AS Type
   FROM tt_PLSUMMARY P
   WHERE vcAccountCode ilike '010401%'
   UNION
   SELECT CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST('0101D2' AS VARCHAR(50)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST('________________' AS VARCHAR(50)) ,CAST(CAST('' AS VARCHAR(50)) AS VARCHAR(100)),CAST('' AS VARCHAR(250)),2
   UNION
   SELECT CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST('0101D3' AS VARCHAR(50)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST(v_monExpenses AS VARCHAR(50)),CAST(CAST('' AS VARCHAR(50)) AS VARCHAR(100)),CAST('C) Total Direct Expenses' AS VARCHAR(250)),2
   UNION
   SELECT CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST('0101E' AS VARCHAR(50)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST('________________' AS VARCHAR(50)) ,CAST(CAST('' AS VARCHAR(50)) AS VARCHAR(100)),CAST('' AS VARCHAR(250)),2
   UNION
   SELECT CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST('0101E1' AS VARCHAR(50)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST(v_monIncome -v_monCOGS -v_monExpenses AS VARCHAR(50)),CAST(CAST('' AS VARCHAR(50)) AS VARCHAR(100)),CAST('Operating Income = (A -B - C)' AS VARCHAR(250)),2
   UNION
   SELECT CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST('0101E2' AS VARCHAR(50)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST('&nbsp;' AS VARCHAR(50)) ,CAST(CAST('' AS VARCHAR(50)) AS VARCHAR(100)),CAST('' AS VARCHAR(250)),2
   UNION
   SELECT CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST('0101F' AS VARCHAR(50)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST('' AS VARCHAR(50)) ,CAST(CAST('' AS VARCHAR(50)) AS VARCHAR(100)),CAST('Other Income' AS VARCHAR(250)),2
   UNION
   SELECT numAccountId ,vcAccountName ,
numParntAcntTypeID ,vcAccountDescription ,
CAST('0101F1' AS VARCHAR(50)),Opening ,Debit ,Credit, CAST((Debit -Credit)*(-1) AS VARCHAR(50)) AS Balance,
CAST(vcAccountCode AS VARCHAR(100)) AS  AccountCode1,
 CASE
   WHEN LENGTH('0101F1') > 4 THEN REPEAT('&nbsp;',LENGTH('0101F1') -4) || P.vcAccountName
   ELSE P.vcAccountName
   END AS vcAccountName1, 1 AS Type
   FROM tt_PLSUMMARY P
   WHERE vcAccountCode ilike '010302%'   
-- and numAccountId NOT IN (  
-- SELECT IT.numIncomeChartAcntId FROM Item IT where IT.numDomainID=@numDomainId )  
   UNION
   SELECT CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST('0101F2' AS VARCHAR(50)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST('________________' AS VARCHAR(50)) ,CAST(CAST('' AS VARCHAR(50)) AS VARCHAR(100)),CAST('' AS VARCHAR(250)),2
   UNION
   SELECT CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST('0101F3' AS VARCHAR(50)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST(v_monOtherIncome AS VARCHAR(50)),CAST(CAST('' AS VARCHAR(50)) AS VARCHAR(100)),CAST('D) Total Other Income' AS VARCHAR(250)),2
   UNION
   SELECT CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST('0101F4' AS VARCHAR(50)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST('&nbsp;' AS VARCHAR(50)) ,CAST(CAST('' AS VARCHAR(50)) AS VARCHAR(100)),CAST('' AS VARCHAR(250)),2
   UNION
   SELECT CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST('0101G' AS VARCHAR(50)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST('' AS VARCHAR(50)) ,CAST(CAST('' AS VARCHAR(50)) AS VARCHAR(100)),CAST('Other Expense' AS VARCHAR(250)),2
   UNION
   SELECT numAccountId ,vcAccountName ,
numParntAcntTypeID ,vcAccountDescription ,
CAST('0101G1' AS VARCHAR(50)),Opening ,Debit ,Credit, CAST((Debit -Credit) AS VARCHAR(50)) AS Balance,
CAST(vcAccountCode AS VARCHAR(100)) AS  AccountCode1,
 CASE
   WHEN LENGTH('0101G1') > 4 THEN REPEAT('&nbsp;',LENGTH('0101G1') -4) || P.vcAccountName
   ELSE P.vcAccountName
   END AS vcAccountName1, 1 AS Type
   FROM tt_PLSUMMARY P
   WHERE vcAccountCode ilike '010402%'  
-- and numAccountId NOT IN (  
-- SELECT IT.numIncomeChartAcntId FROM Item IT where IT.numDomainID=@numDomainId )  
   UNION
   SELECT CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST('0101G2' AS VARCHAR(50)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST('________________' AS VARCHAR(50)) ,CAST(CAST('' AS VARCHAR(50)) AS VARCHAR(100)),CAST('' AS VARCHAR(250)),2
   UNION
   SELECT CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST('0101G3' AS VARCHAR(50)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST(v_monOtherExpense AS VARCHAR(50)),CAST(CAST('' AS VARCHAR(50)) AS VARCHAR(100)),CAST('E) Total Other Expense' AS VARCHAR(250)),2
   UNION  
--select 0,'',0,'','0101G4',0,0,0,'________________' ,'','',2  
--UNION  
  
  
   SELECT CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST('0101H4' AS VARCHAR(50)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST('________________' AS VARCHAR(50)) ,CAST(CAST('' AS VARCHAR(50)) AS VARCHAR(100)),CAST('' AS VARCHAR(250)),2
   UNION
   SELECT CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST('0101H5' AS VARCHAR(50)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST((v_monIncome+v_monOtherIncome) -(v_monCOGS+v_monExpenses+v_monOtherExpense) AS VARCHAR(50)) ,CAST(CAST('' AS VARCHAR(50)) AS VARCHAR(100)),CAST('Net Income = (A + D) - (B + C + E)' AS VARCHAR(250)),2
   UNION
   SELECT CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(0 AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST('0101H6' AS VARCHAR(50)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST(0 AS DECIMAL(20,5)),CAST('&nbsp;' AS VARCHAR(50)) ,CAST(CAST('' AS VARCHAR(50)) AS VARCHAR(100)),CAST('' AS VARCHAR(250)),2;  
/*  
insert into #PLShowGrid1  
select * from #PLShow A  ORDER BY A.vcAccountCode  
insert into #PLShowGrid1  
SELECT * FROM #PLShow1 a ORDER BY A.vcAccountCode*/  
   INSERT INTO tt_PLSHOWGRID1
   SELECT  A.numAccountId ,A.vcAccountName ,A.numParntAcntTypeID ,A.vcAccountDescription ,A.vcAccountCode ,A.Opening ,A.Debit ,A.Credit ,A.Balance ,A.AccountCode1 ,
        CASE WHEN LENGTH(COA.vcAccountCode) > 4 AND  A.TYPE <> 2
   THEN REPEAT('&nbsp;',LENGTH(COA.vcAccountCode) -4) || A.vcAccountName
   WHEN A.TYPE = 2 THEN A.vcAccountName1
   ELSE A.vcAccountName
   END AS vcAccountName1 ,A.TYPE
   FROM    tt_PLSHOW A LEFT JOIN Chart_Of_Accounts COA ON A.numAccountId = COA.numAccountId ORDER BY A.vcAccountCode,COA.vcAccountCode;  
  
   INSERT INTO tt_PLSHOWGRID1
   SELECT  A.numAccountId ,A.vcAccountName ,A.numParntAcntTypeID ,A.vcAccountDescription ,A.vcAccountCode ,A.Opening ,A.Debit ,A.Credit ,A.Balance ,A.AccountCode1 ,
        CASE WHEN LENGTH(COA.vcAccountCode) > 4
   THEN REPEAT('&nbsp;',LENGTH(COA.vcAccountCode) -4) || A.vcAccountName
   ELSE A.vcAccountName
   END AS vcAccountName1 ,A.Type
   FROM    tt_PLSHOW1 A LEFT JOIN Chart_Of_Accounts COA ON A.numAccountId = COA.numAccountId ORDER BY A.vcAccountCode,COA.vcAccountCode;  
  
--insert into #PLShowGrid2  
--SELECT * FROM #PLShow1 a ORDER BY A.vcAccountCode  
  
  

open SWV_RefCur for SELECT * FROM tt_PLSHOWGRID1;  
  
--union  
--select * from #PLShowGrid2  
  
   DROP TABLE IF EXISTS tt_PLSHOWGRID1 CASCADE;  
   DROP TABLE IF EXISTS tt_PLSHOWGRID2 CASCADE;  
   DROP TABLE IF EXISTS tt_PLOUTPUT CASCADE;  
   DROP TABLE IF EXISTS tt_PLSUMMARY CASCADE;  
   DROP TABLE IF EXISTS tt_PLSHOW CASCADE;  
   DROP TABLE IF EXISTS tt_PLSHOW1 CASCADE;  
   DROP TABLE IF EXISTS tt_VIEW_JOURNAL CASCADE;
   RETURN;
END; $$;  
/****** Object:  StoredProcedure [dbo].[USP_GetInvoiceList]    Script Date: 07/26/2008 16:17:34 ******/













