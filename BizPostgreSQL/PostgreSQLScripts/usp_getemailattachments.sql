-- Stored procedure definition script USP_GetEmailAttachments for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetEmailAttachments(v_numEmailHstrID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(numEmailHstrAttID as VARCHAR(255)),cast(numEmailHstrID as VARCHAR(255)),cast(coalesce(vcFileName,'-') as VARCHAR(255)) as vcFileName from EmailHstrAttchDtls where numEmailHstrID = v_numEmailHstrID;
END; $$;












