DROP FUNCTION IF EXISTS USP_GetExistingAddress;
CREATE OR REPLACE FUNCTION USP_GetExistingAddress(v_numDomainId NUMERIC(9,0),        
v_numUserId NUMERIC(9,0),        
v_numDivisionId NUMERIC(9,0) 
--@vcAddress varchar(500) output       
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcBillAddress  VARCHAR(250);
   v_vcShipAddress  VARCHAR(250);
   v_numBillAddress  NUMERIC(18,0);
   v_numShipAddress  NUMERIC(18,0);
   v_numBillToCountry  NUMERIC(10,0);
   v_numBillToState  NUMERIC(10,0);
   v_vcBillToCity  VARCHAR(100);
   v_vcBillToPostal  VARCHAR(100);
   v_numShipToCountry  NUMERIC(10,0);
   v_numShipToState  NUMERIC(10,0);
   v_vcShipToCity  VARCHAR(100);
   v_vcShipToPostal  VARCHAR(10);
   v_numWarehouseID  NUMERIC(18,0);
BEGIN
	v_numBillAddress := 0;
	v_numShipAddress := 0;
  
	SELECT 
		CONCAT((CASE WHEN LENGTH(COALESCE(vcAddressName,'')) > 0 THEN CONCAT('<i>(',vcAddressName,') </i>') ELSE '' END),COALESCE(vcStreet,''),'<br/>',coalesce((vcCity || ','),''),coalesce(fn_GetState(numState),''),', ',coalesce(vcPostalCode,''),'<br/>',coalesce(fn_GetListName(numCountry,0::BOOLEAN),''))
		,numAddressID
		,coalesce(numCountry,0)
		,coalesce(numState,0)
		,coalesce(vcCity,'')
		,coalesce(vcPostalCode,'') 
	INTO 
		v_vcBillAddress
		,v_numBillAddress
		,v_numBillToCountry
		,v_numBillToState
		,v_vcBillToCity
		,v_vcBillToPostal 
	FROM 
		AddressDetails 
	WHERE 
		numDomainID = v_numDomainId
		AND numRecordID = v_numDivisionId
		AND tintAddressOf = 2
		AND tintAddressType = 1
		AND bitIsPrimary = true;

	SELECT
		CONCAT((CASE WHEN LENGTH(COALESCE(vcAddressName,'')) > 0 THEN CONCAT('<i>(',vcAddressName,') </i>') ELSE '' END),COALESCE(vcStreet,''),'<br/>',coalesce((vcCity || ','),''),coalesce(fn_GetState(numState),''),', ',coalesce(vcPostalCode,''),'<br/>',coalesce(fn_GetListName(numCountry,0::BOOLEAN),''))
		, numAddressID
		,coalesce(numCountry,0)
		,coalesce(numState,0)
		,coalesce(vcCity,'')
		,coalesce(vcPostalCode,'')
		,coalesce((SELECT  numWareHouseID FROM Warehouses WHERE numDomainID = v_numDomainId AND numAddressID = numAddressID LIMIT 1),0) 
	INTO 
		v_vcShipAddress
		,v_numShipAddress
		,v_numShipToCountry
		,v_numShipToState
		,v_vcShipToCity
		,v_vcShipToPostal
		,v_numWarehouseID 
	FROM 
		AddressDetails 
	WHERE 
		numDomainID = v_numDomainId
		AND numRecordID = v_numDivisionId
		AND tintAddressOf = 2
		AND tintAddressType = 2
		AND bitIsPrimary = true;
	
	
	--set @vcAddress=(isnull(@vcBillAddress,''+ '||' + '' + '||' + '') + isnull(@vcShipAddress,''+ '||' + '' + '||' + '')); 
	OPEN SWV_RefCur FOR 
	SELECT 
		coalesce(v_vcBillAddress,'') AS vcBillAddress,
		v_numBillAddress AS numBillAddress,
		coalesce(v_vcShipAddress,'') AS vcShipAddress,
		v_numShipAddress AS numShipAddress,
		v_numBillToCountry AS numBillToCountry,
		v_numBillToState AS numBillToState,
		v_vcBillToCity AS vcBillToCity,
		v_vcBillToPostal AS vcBillToPostal,
		v_numShipToCountry AS numShipToCountry,
		v_numShipToState AS numShipToState,
		v_vcShipToCity AS vcShipToCity,
		v_vcShipToPostal AS vcShipToPostal,
		cast(coalesce(fn_GetState(v_numShipToState),'') as VARCHAR(100)) AS vcState,
		fn_GetComapnyName(v_numDivisionId) AS vcCompanyName,
		v_numWarehouseID AS numWarehouseID;
END; $$;  














