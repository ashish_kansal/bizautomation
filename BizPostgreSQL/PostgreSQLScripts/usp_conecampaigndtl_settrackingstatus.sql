-- Stored procedure definition script USP_ConECampaignDTL_SetTrackingStatus for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ConECampaignDTL_SetTrackingStatus(v_numConECampDTLID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE ConECampaignDTL SET bitEmailRead = true WHERE numConECampDTLID = v_numConECampDTLID;
   RETURN;
END; $$;


