DROP FUNCTION IF EXISTS USP_GetPriceLevelWithWarehouseItemPrice;

CREATE OR REPLACE FUNCTION USP_GetPriceLevelWithWarehouseItemPrice(v_numItemCode NUMERIC(9,0),
v_numDomainID NUMERIC(9,0) DEFAULT 0,            
v_numWareHouseID NUMERIC(9,0) DEFAULT NULL,
v_numDivisionID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null, INOUT SWV_RefCur3 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numWareHouseItemID  BIGINT;
   v_monListPrice  DECIMAL(20,5);
   v_monVendorCost  DECIMAL(20,5);

   v_numRelationship  NUMERIC(9,0);                  
   v_numProfile  NUMERIC(9,0);
BEGIN
   select   coalesce(numWareHouseItemID,0) INTO v_numWareHouseItemID FROM WareHouseItems WHERE numWareHouseID = v_numWareHouseID AND numItemID = v_numItemCode AND coalesce(numWLocationID,0) <> -1    LIMIT 1;

   open SWV_RefCur for
   SELECT  coalesce(I.numItemCode,0) AS numItemCode,
		coalesce(W.vcWareHouse,'') AS vcWareHouse,
		--ISNULL(I.monListPrice,ISNULL(WI.monWListPrice,0)) AS decDiscount,
		coalesce(WI.monWListPrice,0) AS decDiscount,
		CAST(W.numWareHouseID AS VARCHAR(10)) || '~' || CAST(WI.numWareHouseItemID AS VARCHAR(10)) || '~' || CAST(coalesce(monWListPrice,0) AS VARCHAR(10)) || '~' || coalesce(W.vcWareHouse,'') AS KeyValue
   FROM    Item I
   LEFT JOIN WareHouseItems WI ON WI.numItemID = I.numItemCode AND WI.numDomainID = I.numDomainID
   LEFT JOIN Warehouses W ON W.numWareHouseID = WI.numWareHouseID
   WHERE coalesce(I.numItemCode,0) = v_numItemCode AND (W.numWareHouseID = coalesce(v_numWareHouseID,0) OR coalesce(v_numWareHouseID,0) = 0) AND coalesce(numWLocationID,0) <> -1;

   IF v_numWareHouseID > 0 then
      v_monListPrice := 0;
      v_monVendorCost := 0;
      if ((v_numWareHouseItemID > 0) and exists(select * from Item where numItemCode = v_numItemCode and charItemType = 'P')) then
	
         select   coalesce(monWListPrice,0) INTO v_monListPrice from WareHouseItems where numWareHouseItemID = v_numWareHouseItemID;
         if v_monListPrice = 0 then 
            select   monListPrice INTO v_monListPrice from Item where numItemCode = v_numItemCode;
         end if;
      else
         select   monListPrice INTO v_monListPrice from Item where numItemCode = v_numItemCode;
      end if;
      v_monVendorCost := fn_GetVendorCost(v_numItemCode);
      open SWV_RefCur2 for
      SELECT  numPricingID,
		numPriceRuleID,
		intFromQty,
		intToQty,
		tintRuleType,
		tintDiscountType,
        CASE 
			WHEN tintRuleType = 1 AND tintDiscountType = 1 --Deduct from List price & Percentage
			THEN v_monListPrice -(v_monListPrice*(decDiscount/100))
			WHEN tintRuleType = 1 AND tintDiscountType = 2 --Deduct from List price & Flat discount
			THEN v_monListPrice -decDiscount
			WHEN tintRuleType = 2 AND tintDiscountType = 1 --Add to Primary Vendor Cost & Percentage
			THEN v_monVendorCost+(v_monVendorCost*(decDiscount/100))
			WHEN tintRuleType = 2 AND tintDiscountType = 2 --Add to Primary Vendor Cost & Flat discount
			THEN v_monVendorCost+decDiscount
			WHEN tintRuleType = 3 --Named Price
			THEN decDiscount
      END AS decDiscount
	  ,coalesce(W.vcWareHouse,'') AS vcWareHouse
	  ,WI.numWareHouseID
	  ,WI.numWareHouseItemID
      FROM    PricingTable PT,WareHouseItems WI JOIN Warehouses W ON W.numWareHouseID = WI.numWareHouseID
      WHERE   coalesce(PT.numItemCode,0) = v_numItemCode AND coalesce(PT.numCurrencyID,0) = 0 AND coalesce(WI.numItemID,0) = v_numItemCode AND (W.numWareHouseID = coalesce(v_numWareHouseID,0) OR coalesce(v_numWareHouseID,0) = 0) AND coalesce(numWLocationID,0) <> -1
      ORDER BY numPricingID;
      select   numCompanyType, vcProfile INTO v_numRelationship,v_numProfile from DivisionMaster D
      join CompanyInfo C on C.numCompanyId = D.numCompanyID where numDivisionID = v_numDivisionID;

		OPEN SWV_RefCur3 FOR
		SELECT 
			PT.numPricingID,
			PT.numPriceRuleID,
			PT.intFromQty,
			PT.intToQty,
			PT.tintRuleType,
			PT.tintDiscountType,
			(CASE 
				WHEN COALESCE(P.bitRoundTo,false) = true AND COALESCE(P.tintRoundTo,0) > 0
				THEN ROUND((CASE 
						WHEN PT.tintRuleType = 1 AND PT.tintDiscountType = 1 --Deduct from List price & Percentage
						THEN v_monListPrice -(v_monListPrice*(PT.decDiscount/100))
						WHEN PT.tintRuleType = 1 AND PT.tintDiscountType = 2 --Deduct from List price & Flat discount
						THEN v_monListPrice -PT.decDiscount
						WHEN PT.tintRuleType = 2 AND PT.tintDiscountType = 1 --Add to Primary Vendor Cost & Percentage
						THEN v_monVendorCost+(v_monVendorCost*(PT.decDiscount/100))
						WHEN PT.tintRuleType = 2 AND PT.tintDiscountType = 2 --Add to Primary Vendor Cost & Flat discount
						THEN v_monVendorCost+PT.decDiscount 
						WHEN PT.tintRuleType = 3 --Named Price
						THEN coalesce(PT.decDiscount,0)
						WHEN PT.tintDiscountType = 3 --Named Price
						THEN coalesce(PT.decDiscount,0)
					END)) + (CASE P.tintRoundTo WHEN 1 THEN -0.01 WHEN 2 THEN -0.05 WHEN 3 THEN 0 ELSE 0 END)
				ELSE
					(CASE 
						WHEN PT.tintRuleType = 1 AND PT.tintDiscountType = 1 --Deduct from List price & Percentage
						THEN v_monListPrice -(v_monListPrice*(PT.decDiscount/100))
						WHEN PT.tintRuleType = 1 AND PT.tintDiscountType = 2 --Deduct from List price & Flat discount
						THEN v_monListPrice -PT.decDiscount
						WHEN PT.tintRuleType = 2 AND PT.tintDiscountType = 1 --Add to Primary Vendor Cost & Percentage
						THEN v_monVendorCost+(v_monVendorCost*(PT.decDiscount/100))
						WHEN PT.tintRuleType = 2 AND PT.tintDiscountType = 2 --Add to Primary Vendor Cost & Flat discount
						THEN v_monVendorCost+PT.decDiscount
						WHEN PT.tintRuleType = 3 --Named Price
						THEN coalesce(PT.decDiscount,0)
						WHEN PT.tintDiscountType = 3 --Named Price
						THEN coalesce(PT.decDiscount,0)
					END) 
			END) AS decDiscount
			,coalesce(W.vcWareHouse,'') AS vcWareHouse
			,WI.numWareHouseID
			,WI.numWareHouseItemID
		FROM 
			Item I
		JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
		LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
		LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
		LEFT JOIN PriceBookPriorities PP ON PP.Step2Value = P.tintStep2 AND PP.Step3Value = P.tintStep3
		JOIN PricingTable PT ON P.numPricRuleID = PT.numPriceRuleID AND coalesce(PT.numCurrencyID,0) = 0,WareHouseItems WI JOIN Warehouses W ON W.numWareHouseID = WI.numWareHouseID
		WHERE 
			I.numItemCode = v_numItemCode 
			AND P.tintRuleFor = 1 
			AND P.tintPricingMethod = 1
			AND (((tintStep2 = 1 AND PBI.numValue = v_numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = v_numDivisionID)) -- Priority 1
				OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 1 AND PDTL.numValue = v_numDivisionID)) -- Priority 2
				OR (tintStep2 = 3 AND (tintStep3 = 1 AND PDTL.numValue = v_numDivisionID)) -- Priority 3
				OR ((tintStep2 = 1 AND PBI.numValue = v_numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = v_numRelationship AND numProfile = v_numProfile)) -- Priority 4
				OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = v_numRelationship AND numProfile = v_numProfile)) -- Priority 5
				OR (tintStep2 = 3 AND (tintStep3 = 2 AND PDTL.numValue = v_numRelationship AND numProfile = v_numProfile)) -- Priority 6
				OR ((tintStep2 = 1 AND PBI.numValue = v_numItemCode) AND tintStep3 = 3) -- Priority 7
				OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND tintStep3 = 3) -- Priority 8
				OR (tintStep2 = 3 and tintStep3 = 3) -- Priority 9
				) 
			AND coalesce(WI.numItemID,0) = v_numItemCode 
			AND (W.numWareHouseID = coalesce(v_numWareHouseID,0) OR coalesce(v_numWareHouseID,0) = 0)
		ORDER BY 
			PP.Priority ASC;
	END IF;
	
	RETURN;
END; $$; 
     
--USP_GetPriceTable 360


