-- Stored procedure definition script USP_SaveCusfldForLeadBox for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SaveCusfldForLeadBox(v_numContactID NUMERIC(9,0) DEFAULT null, 
v_numDivisionID NUMERIC(9,0) DEFAULT null,     
v_fld_id NUMERIC(9,0) DEFAULT NULL,
v_Fld_Value VARCHAR(100) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_PageId  INTEGER; 
   v_RecordId  NUMERIC(9,0);
BEGIN
   select   Grp_id INTO v_PageId from CFW_Fld_Master where Fld_id = v_fld_id;
   if v_PageId = 1 then 
      v_RecordId := v_numDivisionID;
   ELSEIF v_PageId = 4
   then 
      v_RecordId := v_numContactID;
   end if;

   if v_PageId = 1 then

      insert into CFW_FLD_Values(Fld_ID,Fld_Value,RecId)
values(v_fld_id,v_Fld_Value,v_RecordId);
   end if;      
      
   if v_PageId = 4 then

      insert into CFW_FLD_Values_Cont(fld_id,Fld_Value,RecId)
values(v_fld_id,v_Fld_Value,v_RecordId);
   end if;
   RETURN;
END; $$;


