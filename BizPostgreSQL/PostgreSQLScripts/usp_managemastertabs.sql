-- Stored procedure definition script USP_ManageMasterTabs for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageMasterTabs(v_numTabId NUMERIC(9,0) DEFAULT 0,  
v_numTabName VARCHAR(50) DEFAULT '',  
v_tintTabType INTEGER DEFAULT NULL,  
v_vcURL VARCHAR(1000) DEFAULT NULL,  
v_numDomainID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitFixed  BOOLEAN;
BEGIN
   if v_numTabId = 0 then

      insert into TabMaster(numTabName,tintTabType,vcURL,numDomainID)
 values(v_numTabName,v_tintTabType,v_vcURL,v_numDomainID);
   else
      v_bitFixed := false;
      select   coalesce(bitFixed,false) INTO v_bitFixed from TabMaster where numTabId = v_numTabId and tintTabType = v_tintTabType;
      RAISE NOTICE '%',v_bitFixed;
      if v_bitFixed = true then
	
         if exists(select * from TabDefault where numDomainID = v_numDomainID and numTabId = v_numTabId and tintTabType = v_tintTabType) then
		
            RAISE NOTICE 'exists';
            update TabDefault set numTabName = v_numTabName
            where numDomainID = v_numDomainID and numTabId = v_numTabId and tintTabType = v_tintTabType;
         else
            RAISE NOTICE 'insert';
            INSERT INTO TabDefault(numTabId,numTabName,tintTabType,numDomainID)
			 VALUES(v_numTabId,v_numTabName,v_tintTabType,v_numDomainID);
         end if;
      else
         update TabMaster set numTabName = v_numTabName,vcURL = v_vcURL
         where numDomainID = v_numDomainID and numTabId = v_numTabId and tintTabType = v_tintTabType;
      end if;
   end if;
   RETURN;
END; $$;


