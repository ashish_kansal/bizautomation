-- Stored procedure definition script usp_SetFollowUpStatusForSelectedRecords for PostgreSQL
CREATE OR REPLACE FUNCTION usp_SetFollowUpStatusForSelectedRecords(v_vcEntityIdList VARCHAR(4000),          
  v_numFollowUpStatusId NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_intFirstComma  INTEGER;            
   v_intFirstUnderScore  INTEGER;            
          
   v_numCompanyId  NUMERIC(9,0);          
   v_numContactId  NUMERIC(9,0);          
   v_numDivisionId  NUMERIC(9,0);          
          
   v_vcCompContDivValue  VARCHAR(30);          
   v_recordCount  NUMERIC;
BEGIN
   v_vcEntityIdList := coalesce(v_vcEntityIdList,'') || ',';          
   v_intFirstComma := POSITION(',' IN v_vcEntityIdList);            
          
   WHILE v_intFirstComma > 0 LOOP
      v_vcCompContDivValue := SUBSTR(v_vcEntityIdList,0,v_intFirstComma);
      v_intFirstUnderScore := POSITION('_' IN v_vcCompContDivValue);
      v_numCompanyId := CAST(SUBSTR(v_vcCompContDivValue,0,v_intFirstUnderScore) AS NUMERIC(9,0));
      v_vcCompContDivValue := SUBSTR(v_vcCompContDivValue,v_intFirstUnderScore+1,LENGTH(v_vcCompContDivValue));
      v_intFirstUnderScore := POSITION('_' IN v_vcCompContDivValue);
      v_numContactId := CAST(SUBSTR(v_vcCompContDivValue,0,v_intFirstUnderScore) AS NUMERIC(9,0));
      v_numDivisionId := CAST(SUBSTR(v_vcCompContDivValue,v_intFirstUnderScore+1,LENGTH(v_vcCompContDivValue)) AS NUMERIC(9,0));
      RAISE NOTICE '%',v_numCompanyId;
      RAISE NOTICE '%',v_numContactId;
      RAISE NOTICE '%',v_numDivisionId;
      UPDATE DivisionMaster SET numFollowUpStatus = v_numFollowUpStatusId
      WHERE numDivisionID = v_numDivisionId
      AND numCompanyID = v_numCompanyId;

      INSERT INTO FollowUpHistory(numFollowUpstatus, numDivisionID, bintAddedDate)
   VALUES(v_numFollowUpStatusId, v_numDivisionId, TIMEZONE('UTC',now()));
   
      GET DIAGNOSTICS v_recordCount = ROW_COUNT;
      v_vcEntityIdList := SUBSTR(v_vcEntityIdList,v_intFirstComma+1,LENGTH(v_vcEntityIdList));
      v_intFirstComma := POSITION(',' IN v_vcEntityIdList);
   END LOOP;          
   open SWV_RefCur for SELECT v_recordCount;
END; $$;












