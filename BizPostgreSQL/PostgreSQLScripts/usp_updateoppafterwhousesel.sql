-- Stored procedure definition script USP_UpdateOppAfterWhouseSel for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateOppAfterWhouseSel(INOUT v_numOppId NUMERIC DEFAULT 0 ,                                                                                                        
  v_strItems TEXT DEFAULT null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDocItem  INTEGER;
BEGIN
                                         
   Update OpportunityItems set numWarehouseItmsID = X.numWarehouseItmsID
   from
   XMLTABLE
			(
				'NewDataSet/Item'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					Op_Flag SMALLINT PATH 'Op_Flag',
					numoppitemtCode NUMERIC(9,0) PATH 'numoppitemtCode',
					numWarehouseItmsID NUMERIC(9,0) PATH 'numWarehouseItmsID'
			) AS X WHERE X.Op_Flag=2 AND OpportunityItems.numoppitemtCode = X.numoppitemtCode;        
                                                   
   update    OpportunityMaster set tintopptype = 1 where numOppId = v_numOppId;
   RETURN;
END; $$;


