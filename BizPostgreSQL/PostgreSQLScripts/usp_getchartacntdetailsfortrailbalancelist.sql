-- Stored procedure definition script USP_GetChartAcntDetailsForTrailBalanceList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetChartAcntDetailsForTrailBalanceList(v_numDomainId NUMERIC(9,0),          
v_dtFromDate TIMESTAMP,        
v_dtToDate TIMESTAMP, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSQL  VARCHAR(2000);        
   v_i  INTEGER;
BEGIN
   v_strSQL := '';       
   select   count(*) INTO v_i From General_Journal_Header Where datEntry_Date >= CAST('' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(300)),1,300) || '' AS timestamp) And   datEntry_Date <= CAST('' || SUBSTR(CAST(v_dtToDate AS VARCHAR(300)),1,300) || '' AS timestamp);  
   if v_i = 0 then
		
      v_strSQL := ' Select distinct numAccountId as numAccountId,vcCatgyName as CategoryName,numOpeningBal as OpeningBalance            
			From Chart_Of_Accounts COA            
			Left outer join General_Journal_Details GJD on COA.numAccountId = GJD.numChartAcntId            
			Where  COA.numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(5)),1,5) || 'And COA.numAccountId <> 1 And COA.numOpeningBal is not null And COA.numParntAcntId = 1';
      v_strSQL := coalesce(v_strSQL,'') || '  And COA.dtOpeningDate>=''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(300)),1,300) || ''' And  COA.dtOpeningDate<=''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(300)),1,300) || '''';
   Else
      v_strSQL := ' Select distinct numAccountId as numAccountId,vcCatgyName as CategoryName,numOpeningBal as OpeningBalance            
				From Chart_Of_Accounts COA            
				Left outer join General_Journal_Details GJD on COA.numAccountId = GJD.numChartAcntId            
				Where  COA.numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(5)),1,5) || 'And COA.numAccountId <> 1 And COA.numOpeningBal is not null And COA.numParntAcntId = 1';
   end if;  
   RAISE NOTICE '%',v_strSQL;        
   OPEN SWV_RefCur FOR EXECUTE v_strSQL;
   RETURN;
END; $$;


