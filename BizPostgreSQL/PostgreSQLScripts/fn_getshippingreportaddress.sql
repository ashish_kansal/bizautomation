-- Function definition script fn_GetShippingReportAddress for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetShippingReportAddress(v_tintType INTEGER -- 1: FROM Addres, 2: TO Address
	,v_numDomainID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0))
RETURNS TABLE
(
   vcName VARCHAR(100),
   vcCompanyName VARCHAR(100),
   vcPhone VARCHAR(50),
   vcStreet VARCHAR(300),
   vcCity VARCHAR(50),
   vcState NUMERIC(18,0),
   vcZipCode VARCHAR(50),
   vcCountry NUMERIC(18,0),
   bitResidential BOOLEAN
) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintShipToType  SMALLINT;
   v_numDivisionID  NUMERIC(18,0);
   v_numContactID  NUMERIC(18,0);
   v_vcName  VARCHAR(100);
   v_vcPhone  VARCHAR(50);
BEGIN
   DROP TABLE IF EXISTS tt_FN_GETSHIPPINGREPORTADDRESS_TEMP CASCADE;
   CREATE TEMPORARY TABLE IF NOT EXISTS  tt_FN_GETSHIPPINGREPORTADDRESS_TEMP
   (
      vcName VARCHAR(100),
      vcCompanyName VARCHAR(100),
      vcPhone VARCHAR(50),
      vcStreet VARCHAR(300),
      vcCity VARCHAR(50),
      vcState NUMERIC(18,0),
      vcZipCode VARCHAR(50),
      vcCountry NUMERIC(18,0),
      bitResidential BOOLEAN
   );
   IF v_tintType = 1 then
	
      INSERT INTO
      tt_FN_GETSHIPPINGREPORTADDRESS_TEMP
      SELECT 
      CONCAT(coalesce(A.vcFirstName,''),' ',coalesce(A.vcLastname,''))
			,coalesce(C.vcCompanyName,'')
			,coalesce(D.vcComPhone,'')
			,coalesce(AD2.vcStreet,'')
			,coalesce(AD2.vcCity,'')
			,coalesce(AD2.numState,0)
			,coalesce(AD2.vcPostalCode,'')
			,coalesce(AD2.numCountry,0)
			,coalesce(AD2.bitResidential,false)
      FROM
      DivisionMaster D
      INNER JOIN
      CompanyInfo C
      ON
      D.numCompanyID = C.numCompanyId
      LEFT JOIN
      AdditionalContactsInformation A
      ON
      A.numDivisionId = D.numDivisionID
      AND coalesce(A.bitPrimaryContact,false) = true
      LEFT JOIN
      AddressDetails AD2
      ON
      AD2.numDomainID = D.numDomainID
      AND AD2.numRecordID = D.numDivisionID
      AND AD2.tintAddressOf = 2
      AND AD2.tintAddressType = 2
      AND AD2.bitIsPrimary = true
      WHERE
      C.numDomainID = v_numDomainID
      AND C.numCompanyType = 93 LIMIT 1;
   ELSEIF v_tintType = 2
   then
      select   tintShipToType, numDivisionId, numContactId INTO v_tintShipToType,v_numDivisionID,v_numContactID FROM
      OpportunityMaster WHERE
      numOppId = v_numOppID;
      select   coalesce(vcFirstName,'') || ' ' || coalesce(vcLastname,''), (CASE WHEN LENGTH(coalesce(numPhone,'')) > 0 THEN  numPhone ELSE(SELECT coalesce(DM.vcComPhone,cast(0 as TEXT)) FROM DivisionMaster DM WHERE DM.numDivisionID = v_numDivisionID) END) INTO v_vcName,v_vcPhone FROM
      AdditionalContactsInformation WHERE
      numContactId = v_numContactID;
      IF (v_tintShipToType IS NULL OR v_tintShipToType = 1) then
		
         INSERT INTO
         tt_FN_GETSHIPPINGREPORTADDRESS_TEMP
         SELECT(CASE
         WHEN coalesce(bitAltContact,false) = true AND LENGTH(coalesce(vcAltContact,'')) > 0
         THEN vcAltContact
         WHEN coalesce(numContact,0) > 0
         THEN coalesce(vcFirstName,'') || ' ' || coalesce(vcLastname,'')
         ELSE v_vcName
         END)
				,CAST(fn_GetComapnyName(v_numDivisionID) AS VARCHAR(100))
				,(CASE
         WHEN coalesce(bitAltContact,false) = true AND LENGTH(coalesce(vcAltContact,'')) > 0
         THEN(SELECT coalesce(DM.vcComPhone,cast(0 as TEXT)) FROM DivisionMaster DM WHERE DM.numDivisionID = v_numDivisionID)
         WHEN coalesce(numContact,0) > 0
         THEN(CASE WHEN LENGTH(coalesce(numPhone,'')) > 0 THEN  numPhone ELSE(SELECT coalesce(DM.vcComPhone,cast(0 as TEXT)) FROM DivisionMaster DM WHERE DM.numDivisionID = v_numDivisionID) END)
         ELSE v_vcPhone
         END)
				,coalesce(AD.vcStreet,'')
                ,coalesce(AD.vcCity,'')
                ,coalesce(AD.numState,0)
                ,coalesce(AD.vcPostalCode,'')
                ,coalesce(AD.numCountry,0)
				,coalesce(AD.bitResidential,false)
         FROM
         AddressDetails AD
         LEFT JOIN
         AdditionalContactsInformation ACI
         ON
         AD.numContact = ACI.numContactId
         WHERE
         AD.numDomainID = v_numDomainID
         AND AD.numRecordID = v_numDivisionID
         AND AD.tintAddressOf = 2
         AND AD.tintAddressType = 2
         AND AD.bitIsPrimary = true;
      ELSEIF v_tintShipToType = 0
      then
		
         INSERT INTO
         tt_FN_GETSHIPPINGREPORTADDRESS_TEMP
         SELECT(CASE
         WHEN coalesce(bitAltContact,false) = true AND LENGTH(coalesce(vcAltContact,'')) > 0
         THEN vcAltContact
         WHEN coalesce(numContact,0) > 0
         THEN coalesce(vcFirstName,'') || ' ' || coalesce(vcLastname,'')
         ELSE v_vcName
         END)
				,CAST(fn_GetComapnyName(v_numDivisionID) AS VARCHAR(100))
				,(CASE
         WHEN coalesce(bitAltContact,false) = true AND LENGTH(coalesce(vcAltContact,'')) > 0
         THEN(SELECT coalesce(DM.vcComPhone,cast(0 as TEXT)) FROM DivisionMaster DM WHERE DM.numDivisionID = v_numDivisionID)
         WHEN coalesce(numContact,0) > 0
         THEN(CASE WHEN LENGTH(coalesce(numPhone,'')) > 0 THEN  numPhone ELSE(SELECT coalesce(DM.vcComPhone,cast(0 as TEXT)) FROM DivisionMaster DM WHERE DM.numDivisionID = v_numDivisionID) END)
         ELSE v_vcPhone
         END)
				,coalesce(AD1.vcStreet,'')
                ,coalesce(AD1.vcCity,'')
                ,coalesce(AD1.numState,0)
                ,coalesce(AD1.vcPostalCode,'')
                ,coalesce(AD1.numCountry,0)
				,coalesce(AD1.bitResidential,false)
         FROM
         CompanyInfo Com1
         INNER JOIN
         DivisionMaster div1
         ON
         Com1.numCompanyId = div1.numCompanyID
         INNER JOIN
         Domain D1
         ON
         D1.numDivisionId = div1.numDivisionID
         INNER JOIN
         AddressDetails AD1
         LEFT JOIN
         AdditionalContactsInformation ACI
         ON
         AD1.numContact = ACI.numContactId
         ON
         AD1.numDomainID = div1.numDomainID
         AND AD1.numRecordID = div1.numDivisionID
         AND tintAddressOf = 2
         AND tintAddressType = 2
         AND bitIsPrimary = true
         WHERE
         D1.numDomainId = v_numDomainID;
      ELSEIF v_tintShipToType = 2 OR v_tintShipToType = 3
      then
		
         INSERT INTO
         tt_FN_GETSHIPPINGREPORTADDRESS_TEMP
         SELECT(CASE
         WHEN coalesce(bitAltShippingContact,false) = true AND LENGTH(coalesce(vcAltShippingContact,'')) > 0
         THEN vcAltShippingContact
         WHEN coalesce(numShippingContact,0) > 0
         THEN coalesce(vcFirstName,'') || ' ' || coalesce(vcLastname,'')
         ELSE v_vcName
         END)
				,CAST(GetCompanyNameFromContactID(v_numContactID,v_numDomainID) AS VARCHAR(100))
				,(CASE
         WHEN coalesce(bitAltShippingContact,false) = true AND LENGTH(coalesce(vcAltShippingContact,'')) > 0
         THEN(SELECT coalesce(DM.vcComPhone,cast(0 as TEXT)) FROM DivisionMaster DM WHERE DM.numDivisionID = v_numDivisionID)
         WHEN coalesce(numShippingContact,0) > 0
         THEN(CASE WHEN LENGTH(coalesce(numPhone,'')) > 0 THEN  numPhone ELSE(SELECT coalesce(DM.vcComPhone,cast(0 as TEXT)) FROM DivisionMaster DM WHERE DM.numDivisionID = v_numDivisionID) END)
         ELSE v_vcPhone
         END)
				,coalesce(vcShipStreet,'')
                ,coalesce(vcShipCity,'')
                ,coalesce(numShipState,0)
                ,coalesce(vcShipPostCode,'')
                ,coalesce(numShipCountry,0)
				,CAST(0 AS BOOLEAN)
         FROM
         OpportunityAddress
         LEFT JOIN
         AdditionalContactsInformation ACI
         ON
         OpportunityAddress.numShippingContact = ACI.numContactId
         WHERE
         numOppID = v_numOppID;
      end if;
   end if;

   RETURN QUERY (SELECT * FROM tt_FN_GETSHIPPINGREPORTADDRESS_TEMP);
END; $$;

