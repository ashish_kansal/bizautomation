-- Stored procedure definition script USP_UpdateBizDocActionItem for PostgreSQL
CREATE OR REPLACE FUNCTION USP_UpdateBizDocActionItem(v_numDomainId NUMERIC(18,0),
v_numBizActionId NUMERIC(18,0),
v_numOppBizDocsId NUMERIC(18,0),
v_numStatus NUMERIC(1,0),
v_btType BOOLEAN,
v_vcComment VARCHAR(1000) DEFAULT Null,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_BizActionCount  NUMERIC(18,0);
   v_AcceptCount  NUMERIC(8,0);
   v_DeclineCount  NUMERIC(8,0);
BEGIN
   update BizActionDetails set btStatus = v_numStatus,dtApprovedOn = TIMEZONE('UTC',now()),vcComment = v_vcComment where numBizActionId = v_numBizActionId and numOppBizDocsId = v_numOppBizDocsId and btDocType = v_btType;

   v_BizActionCount := 0;

   select   count(btStatus) INTO v_BizActionCount from BizActionDetails where numBizActionId = v_numBizActionId and btStatus = 0;

   update BizDocAction set numStatus =(Case v_BizActionCount when 0 then 1 else 0 end) where numBizActionId = v_numBizActionId;

   if v_btType = false then
	
      update DocumentWorkflow set tintApprove = v_numStatus,dtApprovedOn = TIMEZONE('UTC',now()),vcComment = v_vcComment  where numDocID = v_numOppBizDocsId
      and numContactID in(select numContactId from BizDocAction where numBizActionId = v_numBizActionId);
   else
      update DocumentWorkflow set tintApprove = v_numStatus,dtApprovedOn = TIMEZONE('UTC',now()),vcComment = v_vcComment  where numDocID = v_numOppBizDocsId
      and numContactID in(select numContactId from BizDocAction where numBizActionId = v_numBizActionId) and
      cDocType = 'B';
   end if;


   v_DeclineCount := 0;
   select   COUNT(btStatus) INTO v_DeclineCount from BizActionDetails where numOppBizDocsId = v_numOppBizDocsId and btDocType = v_btType and btStatus = 2;

   if v_BizActionCount = 0 then
	
		--SELECT @AcceptCount=COUNT(btStatus) from BizActionDetails where numOppBizDocsId=@numOppBizDocsId and btDocType=@btType and btStatus=1
		
      if v_DeclineCount > 0 then
			
         update OpportunityBizDocs set
         numBizDocStatus =(select(case v_DeclineCount when 0 then numApproveDocStatusID  else numDeclainDocStatusID end) from  BizDocApprovalRule where numBizDocAppID in(select numBizDocAppId from BizDocAction where numBizActionId = v_numBizActionId))
         where numOppBizDocsId = v_numOppBizDocsId;
      end if;
   else
      update OpportunityBizDocs set
      numBizDocStatus =(select(case v_DeclineCount when 0 then null  else numDeclainDocStatusID end) from  BizDocApprovalRule where numBizDocAppID in(select numBizDocAppId from BizDocAction where numBizActionId = v_numBizActionId))
      where numOppBizDocsId = v_numOppBizDocsId;
   end if;

--if @BizActionCount=0
--	begin
--		update BizDocAction set numStatus=1 where numBizActionId=@numBizActionId;
--	end
--else
--	begin
--		update BizDocAction set numStatus=0 where numBizActionId=@numBizActionId;
--	end

   open SWV_RefCur for SELECT CAST('I' AS CHAR(1));
END; $$;

--select * from OpportunityBizDocs where numOppBizDocsId=2455
--SELECT * FROM BizActionDetails
--
--SELECT * FROM BizDocAction
--
--update BizDocAction set numStatus=0 where numBizActionId=1;
--
--update BizActionDetails set btStatus=0 where numBizActionId=1 and numOppBizDocsid=342
--select * from Documentworkflow where numDocId=342
--select * from BizDocApprovalRule
--select * from BizActionDetails












