CREATE OR REPLACE FUNCTION GetManagers(v_numDivisionID NUMERIC(9,0) DEFAULT 0,  
v_numContactID NUMERIC(9,0) DEFAULT 0,  
v_numDomainID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_numContactID = 0 then

      open SWV_RefCur for
      select numContactId,vcFirstName || ' ' || vcLastname as Name from AdditionalContactsInformation
      where numDivisionId = v_numDivisionID  and numDomainID = v_numDomainID;
   ELSEIF v_numContactID <> 0
   then

      open SWV_RefCur for
      select numContactId,vcFirstName || ' ' || vcLastname as Name from AdditionalContactsInformation
      where numDivisionId = v_numDivisionID and numContactId <> v_numContactID and numDomainID = v_numDomainID;
   end if;
   RETURN;
END; $$;


