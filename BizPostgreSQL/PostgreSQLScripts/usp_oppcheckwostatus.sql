-- Stored procedure definition script USP_OPPCheckWOStatus for PostgreSQL
CREATE OR REPLACE FUNCTION USP_OPPCheckWOStatus(v_intOpportunityId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_OppType  VARCHAR(2);   
   v_Sel  SMALLINT;
BEGIN
   v_Sel := 0;          
   select   tintopptype INTO v_OppType from OpportunityMaster where numOppId = v_intOpportunityId;
                                           
   if cast(NULLIF(v_OppType,'') as INTEGER) = 1 then
  
      SELECT COUNT(*) INTO v_Sel FROM WorkOrder WHERE coalesce(numOppId,0) = v_intOpportunityId AND numWOStatus != 23184;
   end if;    
  
   open SWV_RefCur for select v_Sel;
END; $$;












