-- Stored procedure definition script USP_DemandForecast_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DemandForecast_Save(v_numDFID NUMERIC(18,0),
	v_numDomainID NUMERIC(18,0),
	v_UserContactID NUMERIC(18,0),
	v_bitIncludeOpenReleaseDates BOOLEAN,
	v_vcItemClassification VARCHAR(500),
	v_vcItemGroup VARCHAR(500),
	v_vcWarehouse VARCHAR(500),
	v_bitLastYear BOOLEAN,
	v_numAnalysisPattern NUMERIC(18,0),
	v_numForecastDays NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDemandForecastID  NUMERIC(18,0) DEFAULT 0;
BEGIN
   IF(SELECT COUNT(*) FROM DemandForecast WHERE numDFID = v_numDFID) = 0 then
	
		-- INSERT
      INSERT INTO DemandForecast(numDomainID,
			numDFAPID,
			bitLastYear,
			numDFDaysID,
			bitIncludeOpenReleaseDates,
			dtCreatedDate,
			numCreatedBy)
		VALUES(v_numDomainID,
			v_numAnalysisPattern,
			v_bitLastYear,
			v_numForecastDays,
			v_bitIncludeOpenReleaseDates,
			LOCALTIMESTAMP ,
			v_UserContactID);
		
      v_numDemandForecastID := CURRVAL('DemandForecast_seq');
   ELSE
		-- UPDATE
      UPDATE
      DemandForecast
      SET
      numDFAPID = v_numAnalysisPattern,bitLastYear = v_bitLastYear,numDFDaysID = v_numForecastDays,
      bitIncludeOpenReleaseDates = v_bitIncludeOpenReleaseDates,
      dtModifiedDate = LOCALTIMESTAMP,numModifiedBy = v_UserContactID
      WHERE
      numDFID = v_numDFID;
      v_numDemandForecastID := v_numDFID;

		--Delete Existing Data
      DELETE FROM DemandForecastItemClassification WHERE numDFID = v_numDemandForecastID;
      DELETE FROM DemandForecastItemGroup WHERE numDFID = v_numDemandForecastID;
      DELETE FROM DemandForecastWarehouse WHERE numDFID = v_numDemandForecastID;
   end if;		
	

   IF LENGTH(coalesce(v_vcItemClassification,'')) > 0 then
	
      INSERT INTO DemandForecastItemClassification(numDFID,
			numItemClassificationID)
      SELECT
      v_numDemandForecastID,
			TabAl.a AS String
      FROM(SELECT
         CAST('<NewDataSet><M>' || REPLACE(v_vcItemClassification,',','</M><M>') || '</M></NewDataSet>' AS XML) AS String) AS A
      CROSS JOIN LATERAL(select * from XMLTABLE('NewDataSet/M'
         PASSING String
         COLUMNS a NUMERIC(18,0) PATH '.') AS Split) AS TabAl;
   end if;

   IF LENGTH(coalesce(v_vcItemGroup,'')) > 0 then
	
      INSERT INTO DemandForecastItemGroup(numDFID,
			numItemGroupID)
      SELECT
      v_numDemandForecastID,
			TabAl.a AS String
      FROM(SELECT
         CAST('<NewDataSet><M>' || REPLACE(v_vcItemGroup,',','</M><M>') || '</M></NewDataSet>' AS XML) AS String) AS A
      CROSS JOIN LATERAL(select * from XMLTABLE('NewDataSet/M'
         PASSING String
         COLUMNS a NUMERIC(18,0) PATH '.') AS Split) AS TabAl;
   end if;

   IF LENGTH(coalesce(v_vcWarehouse,'')) > 0 then
	
      INSERT INTO DemandForecastWarehouse(numDFID,
			numWareHouseID)
      SELECT
      v_numDemandForecastID,
			TabAl.a AS String
      FROM(SELECT
         CAST('<NewDataSet><M>' || REPLACE(v_vcWarehouse,',','</M><M>') || '</M></NewDataSet>' AS XML) AS String) AS A
      CROSS JOIN LATERAL(select * from XMLTABLE('NewDataSet/M'
         PASSING String
         COLUMNS a NUMERIC(18,0) PATH '.') AS Split) AS TabAl;
   end if;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[usp_GetCompanyInfoDtlPl]    Script Date: 07/26/2008 16:16:44 ******/



