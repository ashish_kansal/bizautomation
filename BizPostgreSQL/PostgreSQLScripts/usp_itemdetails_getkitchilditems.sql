-- Stored procedure definition script USP_ItemDetails_GetKitChildItems for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ItemDetails_GetKitChildItems(v_numDomainID NUMERIC(18,0),  
	v_numKitItemID NUMERIC(18,0),
	v_numItemCode NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
  
	-- SET NOCOUNT ON added to prevent extra result sets from  
	-- interfering with SELECT statements.  
	  

   open SWV_RefCur for SELECT
   v_numItemCode AS "numKitItemCode",
		Item.numItemCode AS "numItemCode",
		coalesce(Item.vcItemName,'') AS "vcItemName",
		GetKitChildItemsDisplay(v_numKitItemID,v_numItemCode,Item.numItemCode) AS "vcDisplayText",
		coalesce(UOM.vcUnitName,'') AS "UnitName",
		coalesce(ItemDetails.numQtyItemsReq,0) as "Qty",
		fn_GetUOMName(Item.numSaleUnit) AS "vcSaleUOMName",
		coalesce(fn_UOMConversion(Item.numSaleUnit,v_numItemCode,v_numDomainID,Item.numBaseUnit),1) AS "fltUOMConversion",
		coalesce((SELECT cast(tintView as NUMERIC(18,0)) FROM ItemDetails ID WHERE ID.numItemKitID = v_numKitItemID AND ID.numChildItemID = v_numItemCode),
   1) AS "tintView"
   FROM
   ItemDetails
   LEFT JOIN
   UOM
   ON
   ItemDetails.numUOMID = UOM.numUOMId
   INNER JOIN
   Item
   ON
   ItemDetails.numChildItemID = Item.numItemCode
   WHERE
   ItemDetails.numItemKitID = v_numItemCode
   ORDER BY
   sintOrder;
END; $$;  












