-- Function definition script GetWorkOrderPickedRemainingQty for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetWorkOrderPickedRemainingQty(v_numWOID NUMERIC(18,0)
	,v_numQtyItemsReq DOUBLE PRECISION)
RETURNS VARCHAR(50) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numPickedQty  DOUBLE PRECISION DEFAULT 0;
BEGIN
   v_numPickedQty := coalesce((SELECT
   MIN(numPickedQty)
   FROM(SELECT
      WorkOrderDetails.numWODetailId
										,FLOOR(SUM(WorkOrderPickedItems.numPickedQty)/numQtyItemsReq_Orig:: NUMERIC) AS numPickedQty
      FROM
      WorkOrderDetails
      LEFT JOIN
      WorkOrderPickedItems
      ON
      WorkOrderDetails.numWODetailId = WorkOrderPickedItems.numWODetailId
      WHERE
      WorkOrderDetails.numWOId = v_numWOID
      AND coalesce(WorkOrderDetails.numWarehouseItemID,0) > 0
      GROUP BY
      WorkOrderDetails.numWODetailId,WorkOrderDetails.numQtyItemsReq_Orig) TEMP),
   0);

   RETURN CONCAT(v_numPickedQty,' | ',(v_numQtyItemsReq -v_numPickedQty));
END; $$;

