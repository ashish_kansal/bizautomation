DROP FUNCTION IF EXISTS USP_OpportunityMaster_ChangeOrganization;

CREATE OR REPLACE FUNCTION USP_OpportunityMaster_ChangeOrganization(v_numDomainID NUMERIC(18,0),    
	v_numUserCntID NUMERIC(18,0),
	v_numOppID NUMERIC(18,0),
	v_numNewDivisionID NUMERIC(18,0),
	v_numNewContactID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numOldDivisionID  NUMERIC(18,0);
   v_numAddressID NUMERIC(18,0);
   v_vcStreet  VARCHAR(100);
   v_vcCity  VARCHAR(50);
   v_vcPostalCode  VARCHAR(15);
   v_vcCompanyName  VARCHAR(100);
   v_vcAddressName  VARCHAR(50);
   v_vcAltContact  VARCHAR(200);      
   v_numState  NUMERIC(9,0);
   v_numCountry  NUMERIC(9,0);
   v_numCompanyId  NUMERIC(9,0);
   v_numContact  NUMERIC(18,0);
   v_bitIsPrimary  BOOLEAN;
   v_bitAltContact  BOOLEAN;
   v_tintAddressOf  SMALLINT;

    my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
   select   numDivisionId INTO v_numOldDivisionID FROM OpportunityMaster WHERE numDomainId = v_numDomainID AND numOppId = v_numOppID;

   select   vcCompanyName, div.numCompanyID INTO v_vcCompanyName,v_numCompanyId FROM
   CompanyInfo Com
   JOIN
   DivisionMaster div
   ON
   div.numCompanyID = Com.numCompanyId WHERE
   div.numDivisionID = v_numNewDivisionID;

   BEGIN
      -- BEGIN TRANSACTION
UPDATE OpportunityMaster SET numDivisionId = v_numNewDivisionID,numContactId = v_numNewContactID,tintBillToType = 2,
      numBillToAddressID = 0,tintShipToType = 2,numShipToAddressID = 0 WHERE numDomainId = v_numDomainID AND numOppId = v_numOppID;
		
		--Update Billing Address Details
		 --  numeric(9, 0)
			 --  tinyint
			 --  varchar(100)
			 --  varchar(50)
			 --  varchar(15)
			 --  numeric(9, 0)
			 --  numeric(9, 0)
			 --  varchar(100)
			 --  numeric(9, 0)
      select  numAddressID,coalesce(vcStreet,''), coalesce(vcCity,''), coalesce(vcPostalCode,''), coalesce(numState,0), coalesce(numCountry,0), bitIsPrimary, vcAddressName, coalesce(tintAddressOf,0), numContact, bitAltContact, vcAltContact 
	  INTO v_numAddressID,v_vcStreet,v_vcCity,v_vcPostalCode,v_numState,v_numCountry,v_bitIsPrimary,
      v_vcAddressName,v_tintAddressOf,v_numContact,v_bitAltContact,v_vcAltContact FROM
      AddressDetails AD WHERE
      AD.numDomainID = v_numDomainID
      AND AD.numRecordID = v_numNewDivisionID
      AND AD.tintAddressOf = 2
      AND AD.tintAddressType = 1
      AND AD.bitIsPrimary = true;
      PERFORM USP_UpdateOppAddress(v_numOppID := v_numOppID,v_byteMode := 0::SMALLINT,p_numAddressID := v_numAddressID,v_vcStreet := v_vcStreet,
      v_vcCity := v_vcCity,v_vcPostalCode := v_vcPostalCode,v_numState := v_numState,
      v_numCountry := v_numCountry,v_vcCompanyName := v_vcCompanyName,
      v_numCompanyId := 0,v_vcAddressName := v_vcAddressName,v_bitCalledFromProcedure := true,
      v_numContact := v_numContact,v_bitAltContact := v_bitAltContact,
      v_vcAltContact := v_vcAltContact);

		--Update Shipping Address Details
		 --  numeric(9, 0)
			 --  tinyint
			 --  varchar(100)
			 --  varchar(50)
			 --  varchar(15)
			 --  numeric(9, 0)
			 --  numeric(9, 0)
			 --  varchar(100)
			 --  numeric(9, 0)
      select numAddressID,coalesce(vcStreet,''), coalesce(vcCity,''), coalesce(vcPostalCode,''), coalesce(numState,0), coalesce(numCountry,0), bitIsPrimary, vcAddressName, coalesce(tintAddressOf,0), numContact, bitAltContact, vcAltContact 
	  INTO 
	  v_numAddressID,v_vcStreet,v_vcCity,v_vcPostalCode,v_numState,v_numCountry,v_bitIsPrimary,
      v_vcAddressName,v_tintAddressOf,v_numContact,v_bitAltContact,v_vcAltContact FROM
      AddressDetails AD WHERE
      AD.numDomainID = v_numDomainID
      AND AD.numRecordID = v_numNewDivisionID
      AND AD.tintAddressOf = 2
      AND AD.tintAddressType = 2
      AND AD.bitIsPrimary = true;
      PERFORM USP_UpdateOppAddress(v_numOppID := v_numOppID,v_byteMode := 1::SMALLINT,p_numAddressID := v_numAddressID,v_vcStreet := v_vcStreet,
      v_vcCity := v_vcCity,v_vcPostalCode := v_vcPostalCode,v_numState := v_numState,
      v_numCountry := v_numCountry,v_vcCompanyName := v_vcCompanyName,
      v_numCompanyId := 0,v_vcAddressName := v_vcAddressName,v_bitCalledFromProcedure := true,
      v_numContact := v_numContact,v_bitAltContact := v_bitAltContact,
      v_vcAltContact := v_vcAltContact);


		--Delete Tax of old organization and add for new organization
      DELETE FROM OpportunityItemsTaxItems WHERE numOppId = v_numOppID;

		--Insert Tax for Opportunity Items
      INSERT INTO OpportunityItemsTaxItems(numOppId,
			numOppItemID,
			numTaxItemID,
			numTaxID)
      SELECT
      v_numOppID,
			OI.numoppitemtCode,
			TI.numTaxItemID,
			0
      FROM
      OpportunityItems OI
      JOIN
      ItemTax IT
      ON
      OI.numItemCode = IT.numItemCode
      JOIN
      TaxItems TI
      ON
      TI.numTaxItemID = IT.numTaxItemID
      WHERE
      OI.numOppId = v_numOppID
      AND IT.bitApplicable = true
      UNION
      SELECT
      v_numOppID,
			OI.numoppitemtCode,
			0,
			0
      FROM
      OpportunityItems OI
      JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      WHERE
      OI.numOppId = v_numOppID
      AND I.bitTaxable = true
      UNION
      SELECT
      v_numOppID,
			OI.numoppitemtCode,
			1,
			TD.numTaxID
      FROM
      OpportunityItems OI
      INNER JOIN
      ItemTax IT
      ON
      IT.numItemCode = OI.numItemCode
      INNER JOIN
      TaxDetails TD
      ON
      TD.numTaxID = IT.numTaxID
      AND TD.numDomainId = v_numDomainID
      WHERE
      OI.numOppId = v_numOppID;
      INSERT INTO RecordOrganizationChangeHistory(numDomainID,numOppID,numOldDivisionID,numNewDivisionID,numModifiedBy,dtModified)
		VALUES(v_numDomainID,v_numOppID,v_numOldDivisionID,v_numNewDivisionID,v_numUserCntID,TIMEZONE('UTC',now()));
	
      -- COMMIT

EXCEPTION WHEN OTHERS THEN
         -- ROLLBACK TRANSACTION
	GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 RAISE EXCEPTION '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;


