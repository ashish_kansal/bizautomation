-- Stored procedure definition script USP_OPPCheckCanBeShipped for PostgreSQL
CREATE OR REPLACE FUNCTION USP_OPPCheckCanBeShipped(v_intOpportunityId NUMERIC(9,0),
v_bitCheck BOOLEAN DEFAULT false,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_OppType  VARCHAR(2);
   v_vcItemName  VARCHAR(300);
   v_vcKitItemName  VARCHAR(300);
   v_Sel  SMALLINT;   
   v_itemcode  NUMERIC;     
   v_numWareHouseItemID  NUMERIC;                                 
   v_numUnits  DOUBLE PRECISION;                                            
   v_numQtyShipped  DOUBLE PRECISION;
   v_numAllocation  DOUBLE PRECISION;                                                                                                                          
   v_numoppitemtCode  NUMERIC(9,0);
   v_Kit  BOOLEAN;                   
   v_numQtyReceived  DOUBLE PRECISION;
   v_bitAsset  BOOLEAN;
   SWV_RowCount INTEGER;
BEGIN
   DROP TABLE IF EXISTS tt_TEMPITEM CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPITEM
   (
      vcItemName VARCHAR(300)
   );   

	---REVERTING BACK TO PREVIOUS STATE IF DEAL IS BEING EDITED AFTER IT IS CLOSED
   v_Sel := 0;          
   select   tintopptype INTO v_OppType from OpportunityMaster where numOppId = v_intOpportunityId;
                                            
   select   numoppitemtCode, OI.numItemCode, coalesce(numUnitHour,0), coalesce(numQtyShipped,0), coalesce(numUnitHourReceived,0), numWarehouseItmsID, coalesce(I.bitAsset,false), (CASE WHEN bitKitParent = true AND bitAssembly = true THEN 0 WHEN bitKitParent = true THEN 1 ELSE 0 END), OI.vcItemName INTO v_numoppitemtCode,v_itemcode,v_numUnits,v_numQtyShipped,v_numQtyReceived,
   v_numWareHouseItemID,v_bitAsset,v_Kit,v_vcItemName FROM
   OpportunityItems OI
   JOIN
   Item I
   ON
   OI.numItemCode = I.numItemCode
   AND numOppId = v_intOpportunityId
   AND (bitDropShip = false OR bitDropShip IS NULL) WHERE
   charitemtype = 'P'   ORDER BY
   OI.numoppitemtCode LIMIT 1;        
		                                      
   WHILE v_numoppitemtCode > 0 LOOP
      IF cast(NULLIF(v_OppType,'') as INTEGER) = 1 AND v_Kit = true then
         DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPTABLE
         (
            numItemCode NUMERIC(18,0),
            numParentItemID NUMERIC(18,0),
            numOppChildItemID NUMERIC(18,0),
            vcItemName VARCHAR(300),
            bitKitParent BOOLEAN,
            numWarehouseItmsID NUMERIC(18,0),
            numQtyItemsReq DOUBLE PRECISION,
            numAllocation DOUBLE PRECISION
         );
         INSERT INTO
         tt_TEMPTABLE
         SELECT
         OKI.numChildItemID,
				0,
				OKI.numOppChildItemID,
				I.vcItemName,
				I.bitKitParent,
				OKI.numWareHouseItemId,
				OKI.numQtyItemsReq_Orig*(coalesce(OI.numUnitHour,0) -coalesce(OI.numQtyShipped,0)),
				coalesce(WI.numAllocation,0)
         FROM
         OpportunityKitItems OKI
         INNER JOIN
         WareHouseItems WI
         ON
         OKI.numWareHouseItemId = WI.numWareHouseItemID
         INNER JOIN
         OpportunityItems OI
         ON
         OKI.numOppItemID = OI.numoppitemtCode
         INNER JOIN
         Item I
         ON
         OKI.numChildItemID = I.numItemCode
         WHERE
         OKI.numOppId = v_intOpportunityId
         AND OKI.numOppItemID = v_numoppitemtCode
         AND OI.numoppitemtCode = v_numoppitemtCode;
         INSERT INTO
         tt_TEMPTABLE
         SELECT
         OKCI.numItemID,
				t1.numItemCode,
				t1.numOppChildItemID,
				I.vcItemName,
				I.bitKitParent,
				OKCI.numWareHouseItemId,
				OKCI.numQtyItemsReq_Orig*t1.numQtyItemsReq,
				coalesce(WI.numAllocation,0)
         FROM
         OpportunityKitChildItems OKCI
         INNER JOIN
         WareHouseItems WI
         ON
         OKCI.numWareHouseItemId = WI.numWareHouseItemID
         INNER JOIN
         tt_TEMPTABLE t1
         ON
         OKCI.numOppChildItemID = t1.numOppChildItemID
         INNER JOIN
         Item I
         ON
         OKCI.numItemID = I.numItemCode
         WHERE
         OKCI.numOppId = v_intOpportunityId
         AND OKCI.numOppItemID = v_numoppitemtCode;
         IF(SELECT COUNT(*) FROM tt_TEMPTABLE WHERE coalesce(bitKitParent,false) = false AND coalesce(numQtyItemsReq,0) > coalesce(numAllocation,0)) > 0 then
			
            INSERT INTO tt_TEMPITEM  values(v_vcItemName);
         end if;
      ELSE
         select   coalesce(numAllocation,0) INTO v_numAllocation FROM
         WareHouseItems WHERE
         numWareHouseItemID = v_numWareHouseItemID;
         IF cast(NULLIF(v_OppType,'') as INTEGER) = 1 then
			          
				/*below is added to be validated from Sales fulfillment*/
            IF v_bitCheck = true then
				
               IF v_numUnits -v_numQtyShipped > 0 then
					
                  IF (v_bitAsset = false) then
						
                     INSERT INTO tt_TEMPITEM  values(v_vcItemName);
                  end if;
               end if;
            end if;
            v_numUnits := v_numUnits -v_numQtyShipped;
            IF v_numUnits > v_numAllocation then
				
               If (v_bitAsset = false) then
					
                  INSERT INTO tt_TEMPITEM  VALUES(v_vcItemName);
               end if;
            end if;
         ELSEIF cast(NULLIF(v_OppType,'') as INTEGER) = 2
         then
			
            IF v_bitCheck = true then
				
               IF v_numUnits -v_numQtyReceived > 0 then
					
                  INSERT INTO tt_TEMPITEM  VALUES(v_vcItemName);
               end if;
            end if;
         end if;
      end if;
      select   numoppitemtCode, OI.numItemCode, coalesce(numUnitHour,0), coalesce(numQtyShipped,0), coalesce(numUnitHourReceived,0), numWarehouseItmsID, (case when bitKitParent = true and bitAssembly = true then 0 when bitKitParent = true then 1 else 0 end), OI.vcItemName INTO v_numoppitemtCode,v_itemcode,v_numUnits,v_numQtyShipped,v_numQtyReceived,
      v_numWareHouseItemID,v_Kit,v_vcItemName from OpportunityItems OI
      JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode and numOppId = v_intOpportunityId WHERE
      charitemtype = 'P'
      AND OI.numoppitemtCode > v_numoppitemtCode
      AND (bitDropShip = false or bitDropShip is null)   ORDER BY
      OI.numoppitemtCode LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      IF SWV_RowCount = 0 then 
         v_numoppitemtCode := 0;
      end if;
   END LOOP;   
  
   open SWV_RefCur for SELECT * FROM tt_TEMPITEM;
   
   RETURN;
END; $$;












