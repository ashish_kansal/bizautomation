-- Stored procedure definition script USP_DeleteShippingRule for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteShippingRule(v_numRuleID NUMERIC(9,0),
    v_numDomainID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM PromotionOfferContacts WHERE numProId = v_numRuleID AND tintRecordType = 2;
   DELETE FROM PromotionOfferItems WHERE numProId = v_numRuleID AND tintRecordType = 2;
    
   DELETE FROM ShippingRuleStateList WHERE numRuleID = v_numRuleID; 
   DELETE FROM ShippingServiceTypes WHERE numRuleID =  v_numRuleID;
  
   DELETE  FROM ShippingRules
   WHERE   numRuleID = v_numRuleID
   AND numDomainId = v_numDomainID;
   RETURN;
END; $$;


