-- Stored procedure definition script usp_GetAOIs for PostgreSQL
create or replace FUNCTION usp_GetAOIs(v_numDomainID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT cast(numListItemID as VARCHAR(255)) as numAOIId, cast(vcData as VARCHAR(255)) as vcAOIName
   FROM Listdetails
   WHERE numDomainid = v_numDomainID and  numListID = 74
   ORDER BY vcData;
END; $$;












