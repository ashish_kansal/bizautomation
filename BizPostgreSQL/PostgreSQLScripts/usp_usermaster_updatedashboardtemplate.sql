-- Stored procedure definition script USP_UserMaster_UpdateDashboardTemplate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UserMaster_UpdateDashboardTemplate(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_numTemplateID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   BEGIN
      UPDATE
      UserMaster
      SET
      numDashboardTemplateID = v_numTemplateID
      WHERE
      numDomainID = v_numDomainID	AND  numUserDetailId = v_numUserCntID;
   END;
   RETURN;
END; $$;



