-- Stored procedure definition script USP_GetOperationBudgetFiscalMonth for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetOperationBudgetFiscalMonth(v_numDomainId NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_month  VARCHAR(2);
BEGIN
   select   coalesce(tintFiscalStartMonth,1) INTO v_month from Domain where numDomainId = v_numDomainId;  
   RAISE NOTICE '%',v_month;
   RETURN;
END; $$;


