-- Stored procedure definition script USP_GetChartofAccountDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetChartofAccountDetails(v_numAcntId NUMERIC(9,0) DEFAULT 0,      
v_numDomainId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select monEndingBal as monEndingBal,case when monEndingOpeningBal is null then  numOriginalOpeningBal else monEndingOpeningBal End  as monEndingOpeningBal
   From Chart_Of_Accounts Where numAccountId = v_numAcntId and numDomainId = v_numDomainId;
END; $$;












