-- Stored procedure definition script Usp_DeleteProjectBizDoc for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Usp_DeleteProjectBizDoc(v_numProjOppID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM ProjectsOpportunities WHERE numProjOppID = v_numProjOppID;
   RETURN;
END; $$;
--Created By Siva  
-- exec USP_DeleteReceivePaymentDetails @numBizDocsPaymentDetailsId=1134,@numBizDocsPaymentDetId=1855,@numBizDocsId=5078,@numOppId=5732,@numReturnID=0
--select * from BizDocComission




