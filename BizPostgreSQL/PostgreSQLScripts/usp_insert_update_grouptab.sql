-- Stored procedure definition script usp_Insert_Update_groupTab for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_Insert_Update_groupTab(v_numGroupId NUMERIC DEFAULT 0,
	v_bitstatus VARCHAR(100) DEFAULT ' '
	
--
)
RETURNS VOID LANGUAGE plpgsql

	--Input Parameter List

   AS $$
   DECLARE
   v_var1  NUMERIC;
BEGIN

	--Updating the field in LeadBox Table
   BEGIN
      delete from GroupTabDetails where numGroupID = v_numGroupId;
      insert into GroupTabDetails(numGroupID, numTabId, bitallowed) values(v_numGroupId, 1, (SUBSTR(SUBSTR(v_bitstatus,1,1),length(SUBSTR(v_bitstatus,1,1)) -1+1)));

      insert into GroupTabDetails(numGroupID, numTabId, bitallowed) values(v_numGroupId, 2, (SUBSTR(SUBSTR(v_bitstatus,1,2),length(SUBSTR(v_bitstatus,1,2)) -1+1)));

      insert into GroupTabDetails(numGroupID, numTabId, bitallowed) values(v_numGroupId, 3, (SUBSTR(SUBSTR(v_bitstatus,1,3),length(SUBSTR(v_bitstatus,1,3)) -1+1)));

      insert into GroupTabDetails(numGroupID, numTabId, bitallowed) values(v_numGroupId, 4, (SUBSTR(SUBSTR(v_bitstatus,1,4),length(SUBSTR(v_bitstatus,1,4)) -1+1)));

      insert into GroupTabDetails(numGroupID, numTabId, bitallowed) values(v_numGroupId, 5, (SUBSTR(SUBSTR(v_bitstatus,1,5),length(SUBSTR(v_bitstatus,1,5)) -1+1)));

      insert into GroupTabDetails(numGroupID, numTabId, bitallowed) values(v_numGroupId, 6, (SUBSTR(SUBSTR(v_bitstatus,1,6),length(SUBSTR(v_bitstatus,1,6)) -1+1)));
   END;
   RETURN;
END; $$;


