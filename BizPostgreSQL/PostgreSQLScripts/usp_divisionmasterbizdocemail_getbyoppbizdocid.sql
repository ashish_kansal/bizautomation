-- Stored procedure definition script USP_DivisionMasterBizDocEmail_GetByOppBizDocID for PostgreSQL
CREATE OR REPLACE FUNCTION USP_DivisionMasterBizDocEmail_GetByOppBizDocID(v_numDomainID NUMERIC(18,0)
	,v_numOppBizDocID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT(CASE WHEN LENGTH(coalesce(DMBE.vcTo,'')) > 0 THEN fn_GetEmailStringAsJson(DMBE.vcTo) ELSE '' END) AS vcTo
		,(CASE WHEN LENGTH(coalesce(DMBE.vcCC,'')) > 0 THEN fn_GetEmailStringAsJson(DMBE.vcCC) ELSE '' END) AS vcCC
		,(CASE WHEN LENGTH(coalesce(DMBE.vcBCC,'')) > 0 THEN fn_GetEmailStringAsJson(DMBE.vcBCC) ELSE '' END) AS vcBCC
   FROM
   OpportunityBizDocs OBD
   INNER JOIN
   OpportunityMaster OM
   ON
   OBD.numoppid = OM.numOppId
   INNER JOIN
   DivisionMasterBizDocEmail DMBE
   ON
   OM.numDivisionId = DMBE.numDivisionID
   AND OBD.numBizDocId = DMBE.numBizDocID
   INNER JOIN
   Domain D
   ON
   DMBE.numDomainID = D.numDomainId
   WHERE
   OBD.numOppBizDocsId = v_numOppBizDocID
   AND coalesce(D.bitUsePreviousEmailBizDoc,false) = true;
END; $$;












