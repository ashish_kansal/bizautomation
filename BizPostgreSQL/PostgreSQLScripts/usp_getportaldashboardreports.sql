-- Stored procedure definition script USP_GetPortalDashboardReports for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetPortalDashboardReports(v_numDomainID NUMERIC(9,0),
    v_numRelationshipID NUMERIC(9,0),
    v_numProfileID NUMERIC(9,0),
    v_numContactID NUMERIC DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null, INOUT SWV_RefCur3 refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   SELECT  numReportID,
            vcReportName,
            tintDisplayOrder
   FROM    PortalDashboardReports
   WHERE numReportID NOT IN(SELECT numReportID FROM PortalDashboardDtl WHERE  numDomainID = v_numDomainID
      AND numRelationshipID = v_numRelationshipID
      AND numProfileID = v_numProfileID AND numContactID = v_numContactID)
   ORDER BY tintDisplayOrder;
    
   open SWV_RefCur2 for
   SELECT  PD.numPortalDashboardDtlID,
            PD.numDomainID,
            PD.numRelationshipID,
            PD.numProfileID,
            PD.numReportID,
            PD.tintColumnNumber,
            PD.tintRowOrder,
            vcReportName
   FROM    PortalDashboardReports PDR
   INNER JOIN PortalDashboardDtl PD ON PD.numReportID = PDR.numReportID
   WHERE   numDomainID = v_numDomainID
   AND numRelationshipID = v_numRelationshipID
   AND numProfileID = v_numProfileID
   AND tintColumnNumber = 1
   AND numContactID = v_numContactID
   ORDER BY PD.tintRowOrder;
            
   open SWV_RefCur3 for
   SELECT  PD.numPortalDashboardDtlID,
            PD.numDomainID,
            PD.numRelationshipID,
            PD.numProfileID,
            PD.numReportID,
            PD.tintColumnNumber,
            PD.tintRowOrder,
            vcReportName
   FROM    PortalDashboardReports PDR
   INNER JOIN PortalDashboardDtl PD ON PD.numReportID = PDR.numReportID
   WHERE   numDomainID = v_numDomainID
   AND numRelationshipID = v_numRelationshipID
   AND numProfileID = v_numProfileID
   AND tintColumnNumber = 2
   AND numContactID = v_numContactID
   ORDER BY PD.tintRowOrder;
   RETURN;
END; $$;
--USP_GetPriceTable 360


