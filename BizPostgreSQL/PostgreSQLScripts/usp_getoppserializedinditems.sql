-- Stored procedure definition script USP_GetOppSerializedIndItems for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetOppSerializedIndItems(v_numOppItemCode NUMERIC(9,0) DEFAULT 0,      
v_numOppID NUMERIC(9,0) DEFAULT 0,  
v_tintOppType  SMALLINT DEFAULT NULL, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null, INOUT SWV_RefCur3 refcursor default null, INOUT SWV_RefCur4 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numWareHouseItemID  NUMERIC(9,0);      
   v_numItemID  NUMERIC(9,0);      
   v_bitSerialize  BOOLEAN;                    
   v_bitLotNo  BOOLEAN;                    

   v_str  VARCHAR(2000);  
   v_strSQL  VARCHAR(2000);                       
   v_ColName  VARCHAR(50);                    
   v_numItemGroupID  NUMERIC(9,0);                      
                      
   v_ID  NUMERIC(9,0);                      
   v_numCusFlDItemID  VARCHAR(20);                      
   v_fld_label  VARCHAR(100);
   SWV_RowCount INTEGER;
BEGIN
   v_str := '';        
      
   select   numWarehouseItmsID, numItemID INTO v_numWareHouseItemID,v_numItemID from   OpportunityItems
   join WareHouseItems
   on OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID where  numOppItemTcode = v_numOppItemCode;         
                      
                      
   select   numItemGroup, coalesce(bitSerialized,false), coalesce(bitLotNo,false) INTO v_numItemGroupID,v_bitSerialize,v_bitLotNo from Item where numItemCode = v_numItemID;                      
   v_ColName := 'WareHouseItmsDTL.numWareHouseItmsDTLID,1';                  
            
--Create a Temporary table to hold data                                                          
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   drop table IF EXISTS tt_TEMPTABLE CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLE 
   ( 
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numCusFlDItemID NUMERIC(9,0)
   );                       
                      
   insert into tt_TEMPTABLE(numCusFlDItemID)
   select numOppAccAttrID from ItemGroupsDTL where numItemGroupID = v_numItemGroupID and tintType = 2;     
              
   v_ID := 0;                      
   select   ID, numCusFlDItemID, fld_label INTO v_ID,v_numCusFlDItemID,v_fld_label from tt_TEMPTABLE
   join CFW_Fld_Master on numCusFlDItemID = Fld_ID     LIMIT 1;                                   
   while v_ID > 0 LOOP
      v_str := coalesce(v_str,'') || ',  GetCustFldItems(' || coalesce(v_numCusFlDItemID,'') || ',9::SMALLINT,' || coalesce(v_ColName,'') || '::SMALLINT) as "' || coalesce(v_fld_label,'') || '"';
      select   ID, numCusFlDItemID, fld_label INTO v_ID,v_numCusFlDItemID,v_fld_label from tt_TEMPTABLE
      join CFW_Fld_Master on numCusFlDItemID = Fld_ID and ID > v_ID     LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount = 0 then 
         v_ID := 0;
      end if;
   END LOOP;        
      
   open SWV_RefCur for
   select numWareHouseItemID,vcWareHouse,
--CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),Opp.numItemCode,I.numDomainId,ISNULL(opp.numUOMId,0)) * Opp.numUnitHour) as numeric(18,0)) as numUnitHour
numUnitHour,coalesce(u.vcUnitName,'') AS vcBaseUOMName
,v_bitSerialize AS bitSerialize,v_bitLotNo AS bitLotNo from OpportunityItems Opp
   join WareHouseItems
   on Opp.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
   join Warehouses
   on Warehouses.numWareHouseID = WareHouseItems.numWareHouseID
   join Item I on Opp.numItemCode = I.numItemCode
   LEFT JOIN  UOM u ON u.numUOMId = I.numBaseUnit
   where Opp.numoppitemtCode = v_numOppItemCode;      
      
       
      
           
   v_strSQL := 'select WareHouseItmsDTL.numWareHouseItmsDTLID,numWareHouseItemID,vcSerialNo,0 as Op_Flag,
  coalesce(WareHouseItmsDTL.numQty,0) -coalesce((select sum(w.numQty) from OppWarehouseSerializedItem w INNER JOIN OpportunityMaster opp ON (w.numOppID = opp.numOppId AND opp.tintOppType = 1) where coalesce(opp.tintshipped,0) = 0 and w.numWarehouseItmsDTLID = WareHouseItmsDTL.numWareHouseItmsDTLID),0)+coalesce(OppWarehouseSerializedItem.numQty,0) as TotalQty,coalesce(OppWarehouseSerializedItem.numQty,0) as UsedQty
  ' ||  coalesce(v_str,'')  || ',true as bitAdded
 from   OppWarehouseSerializedItem      
 join WareHouseItmsDTL      
 on WareHouseItmsDTL.numWareHouseItmsDTLID = OppWarehouseSerializedItem.numWarehouseItmsDTLID                           
 where numOppID =' || SUBSTR(CAST(v_numOppID AS VARCHAR(15)),1,15) || ' and  numWareHouseItemID =' || SUBSTR(CAST(v_numWareHouseItemID AS VARCHAR(15)),1,15);                        
   RAISE NOTICE '%',v_strSQL;  
   
   OPEN SWV_RefCur2 FOR EXECUTE v_strSQL;     
  
  
   open SWV_RefCur3 for
   select Fld_label,fld_id,fld_type,numlistid,vcURL from tt_TEMPTABLE
   join CFW_Fld_Master on numCusFlDItemID = Fld_ID;                    
   
  
   if v_tintOppType = 1 then

      v_strSQL := 'select numWareHouseItmsDTLID,numWareHouseItemID,vcSerialNo,0 as Op_Flag,
  coalesce(numQty,0) -coalesce((select sum(w.numQty) from OppWarehouseSerializedItem w INNER JOIN OpportunityMaster opp ON (w.numOppID = opp.numOppId AND opp.tintOppType = 1) where coalesce(opp.tintshipped,0) = 0 and w.numWarehouseItmsDTLID = WareHouseItmsDTL.numWareHouseItmsDTLID),0) as TotalQty,
 cast(0 as NUMERIC(9,0)) as UsedQty ' ||  coalesce(v_str,'')  || ',false as bitAdded
 from   WareHouseItmsDTL   
    where (tintStatus is null or tintStatus = 0) and  numWareHouseItemID =' || SUBSTR(CAST(v_numWareHouseItemID AS VARCHAR(15)),1,15) || '  
    and numWareHouseItmsDTLID not in(select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppID =' || SUBSTR(CAST(v_numOppID AS VARCHAR(15)),1,15) || ' and  numWarehouseItmsID =' || SUBSTR(CAST(v_numWareHouseItemID AS VARCHAR(15)),1,15) || ')';
      RAISE NOTICE '%',v_strSQL;
      OPEN SWV_RefCur4 FOR EXECUTE v_strSQL;
   end if;
   RETURN;
END; $$;


