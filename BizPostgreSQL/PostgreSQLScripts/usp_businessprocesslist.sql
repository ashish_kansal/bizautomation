-- Stored procedure definition script usp_BusinessProcessList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_BusinessProcessList(v_numDomainId NUMERIC(9,0),
v_Mode INTEGER, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF(v_Mode = -1) then

      open SWV_RefCur for
      select Slp_Id AS "Slp_Id",Slp_Name AS "Slp_Name" from Sales_process_List_Master where numDomainID = v_numDomainId AND coalesce(numOppId,0) = 0 AND coalesce(numProjectId,0) = 0 AND coalesce(numWorkOrderId,0) = 0 order by Slp_Name;
   ELSE
      open SWV_RefCur for
      select Slp_Id AS "Slp_Id",Slp_Name AS "Slp_Name" from Sales_process_List_Master where Pro_Type = v_Mode and numDomainID = v_numDomainId AND coalesce(numOppId,0) = 0 AND coalesce(numProjectId,0) = 0 AND coalesce(numWorkOrderId,0) = 0 order by Slp_Name;
   end if;
   RETURN;
END; $$;


