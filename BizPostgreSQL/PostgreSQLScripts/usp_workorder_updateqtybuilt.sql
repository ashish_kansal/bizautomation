-- Stored procedure definition script USP_WorkOrder_UpdateQtyBuilt for PostgreSQL
CREATE OR REPLACE FUNCTION USP_WorkOrder_UpdateQtyBuilt(v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0),
	v_numWOID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
	-- CODE LEVEL TRANSACTION IS USED

   AS $$
   DECLARE
   v_numQtyBuilt  DOUBLE PRECISION;
   v_numQtyItemsReq  DOUBLE PRECISION; 
   v_numProcessedQty  DOUBLE PRECISION DEFAULT 0;
   v_numWareHouseItemID  NUMERIC(18,0);
BEGIN
   select   coalesce(numQtyItemsReq,0), coalesce(numQtyBuilt,0), numWareHouseItemId INTO v_numQtyItemsReq,v_numQtyBuilt,v_numWareHouseItemID FROM
   WorkOrder WHERE
   WorkOrder.numDomainId = v_numDomainID
   AND WorkOrder.numWOId = v_numWOID;

   v_numProcessedQty := coalesce((SELECT
   MIN(numQtyBuilt) AS numQtyBuilt
   FROM(SELECT
      numTaskId
										,(CASE
      WHEN EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId AND tintAction = 4)
      THEN coalesce(WorkOrder.numQtyItemsReq,0)
      ELSE coalesce((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId),0)
      END) AS numQtyBuilt
      FROM
      StagePercentageDetailsTask SPDT
      INNER JOIN
      WorkOrder
      ON
      SPDT.numWorkOrderId = WorkOrder.numWOId
      WHERE
      numWorkOrderId = v_numWOID) TEMP),0);

	open SWV_RefCur for 
	SELECT
		Item.numItemCode AS "numItemCode"
		,coalesce(Item.numAssetChartAcntId,0) AS "numAssetChartAcntId"
		,coalesce(Item.monAverageCost,0) AS "monAverageCost"
		,(CASE
			WHEN coalesce(v_numProcessedQty,0) > v_numQtyBuilt AND coalesce(v_numProcessedQty,0) <= v_numQtyItemsReq  THEN coalesce(v_numProcessedQty,0) -v_numQtyBuilt
			ELSE 0
		END) AS "numQtyBuilt"
	FROM
		WorkOrder
	INNER JOIN
		Item
	ON
		WorkOrder.numItemCode = Item.numItemCode
	WHERE
		WorkOrder.numDomainId = v_numDomainID
		AND WorkOrder.numWOId = v_numWOID;

   UPDATE
   WorkOrder
   SET
   numQtyBuilt = coalesce(numQtyBuilt,0)+(CASE
   WHEN coalesce(v_numProcessedQty,0) > v_numQtyBuilt AND coalesce(v_numProcessedQty,0) <= v_numQtyItemsReq THEN(coalesce(v_numProcessedQty,0) -v_numQtyBuilt)
   ELSE 0
   END)
   WHERE
   WorkOrder.numDomainId = v_numDomainID
   AND WorkOrder.numWOId = v_numWOID;
   RETURN;
END; $$;












