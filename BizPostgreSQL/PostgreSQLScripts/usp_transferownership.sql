-- Stored procedure definition script USP_TransferOwnerShip for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_TransferOwnerShip(v_numUserCntID NUMERIC(9,0) DEFAULT 0,  
v_numRecID NUMERIC(9,0) DEFAULT 0,
v_byteMode SMALLINT DEFAULT NULL,
v_numDomainID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   if v_byteMode = 0 then

      update DivisionMaster set  numRecOwner = v_numUserCntID where numDivisionID = v_numRecID  and numDomainID = v_numDomainID;
      UPDATE CompanyInfo SET bintModifiedDate = TIMEZONE('UTC',now()) WHERE numCompanyId =(SELECT 
      numCompanyID
      FROM
      DivisionMaster
      where
      numDivisionID = v_numRecID  and numDomainID = v_numDomainID LIMIT 1);
   ELSEIF v_byteMode = 1
   then

      update OpportunityMaster set  numrecowner = v_numUserCntID,bintModifiedDate = TIMEZONE('UTC',now()) where numOppId = v_numRecID  and numDomainId = v_numDomainID;
   ELSEIF v_byteMode = 2
   then

      update ProjectsMaster set  numRecOwner = v_numUserCntID,bintModifiedDate = TIMEZONE('UTC',now()) where numProId = v_numRecID  and numdomainId = v_numDomainID;
   ELSEIF v_byteMode = 3
   then

      update AdditionalContactsInformation set  numRecOwner = v_numUserCntID where numContactId = v_numRecID  and numDomainID = v_numDomainID;
   ELSEIF v_byteMode = 4
   then

      update Cases set  numRecOwner = v_numUserCntID,bintModifiedDate = TIMEZONE('UTC',now()) where numCaseId = v_numRecID  and numDomainID = v_numDomainID;
   ELSEIF v_byteMode = 5
   then

      update StagePercentageDetails set  numAssignTo = v_numUserCntID,bintModifiedDate = TIMEZONE('UTC',now()) where numStageDetailsId = v_numRecID  and numdomainid = v_numDomainID;
   end if;
   RETURN;
END; $$;


