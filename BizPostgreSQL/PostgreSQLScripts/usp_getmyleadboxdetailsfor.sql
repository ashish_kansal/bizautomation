-- Stored procedure definition script usp_GetMyLeadBoxDetailsFor for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetMyLeadBoxDetailsFor(v_numUserID NUMERIC(9,0) DEFAULT 0  
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		LD.vcInterested1,
		LD.vcInterested2,
		LD.vcInterested3,
		LD.vcInterested4,
		LD.vcInterested5,
		LD.vcInterested6,
		LD.vcInterested7,
		LD.vcInterested8,
		vcRedirectURL,
		bitWebSite,
		bitPosition,
		bitNotes,
		bitAnnualRevenue,
		bitNumEmployees,
		bitHowHeard,
		bitProfile,
		bitPublicLead
   FROM
   LeadBox LD;
		
	--WHERE 
		 --numUserID=@numUserID
		
END; $$;












