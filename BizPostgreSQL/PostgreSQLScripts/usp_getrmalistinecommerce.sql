-- Stored procedure definition script USP_GetRMAListInEcommerce for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetRMAListInEcommerce(v_numDivisionID NUMERIC(9,0) DEFAULT 0 ,
 v_numDomainID NUMERIC(9,0) DEFAULT NULL,
 v_CurrentPage INTEGER DEFAULT NULL,
 v_PageSize INTEGER DEFAULT NULL,
 v_UserId NUMERIC(18,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_firstRec  INTEGER DEFAULT 0;
   v_lastRec  INTEGER DEFAULT 0;
BEGIN
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);
   open SWV_RefCur for
   SELECT * FROM(SELECT ROW_NUMBER() OVER(ORDER BY numReturnHeaderID) AS Rownumber,* FROM(SELECT distinct
         RH.vcRMA,
		(select  vcData from Listdetails WHERE numListItemID = RH.numReturnReason LIMIT 1) AS numReturnReason,
		(select  vcData from Listdetails WHERE numListItemID = RH.numReturnStatus LIMIT 1) AS numReturnStatus,
		COALESCE((SELECT string_agg(CONCAT(CAST(I.vcItemName AS VARCHAR(18)) || '#^#' || CAST(RI.numUnitHour AS VARCHAR(18))),'$^$')
               FROM ReturnItems AS RI LEFT JOIN Item AS I ON RI.numItemCode = I.numItemCode WHERE numReturnHeaderID = RH.numReturnHeaderID),'') AS vcProducts,
		RH.numReturnHeaderID
         FROM
         ReturnHeader AS RH
         WHERE
         RH.numDivisionId = v_numDivisionID AND
         RH.numContactID = v_UserId) AS K) AS T
   WHERE T.Rownumber > v_firstRec AND T.Rownumber < v_lastRec;

   open SWV_RefCur2 for
   select
   DISTINCT COUNT(RH.numReturnHeaderID)
   FROM
   ReturnHeader AS RH
   WHERE
   RH.numDivisionId = v_numDivisionID AND
   RH.numContactID = v_UserId;
   RETURN;
END; $$;




--EXEC USP_GetSalesTemplateItems 6,72
--SELECT * FROM SalesTemplateItems


