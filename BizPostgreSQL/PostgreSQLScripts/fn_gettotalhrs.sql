-- Function definition script fn_GetTotalHrs for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetTotalHrs(v_bitOverTime BOOLEAN,v_bitOverTimeHrsDailyOrWeekly BOOLEAN,v_decOverTime DECIMAL,v_dtStartDate TIMESTAMP,v_dtEndDate TIMESTAMP,v_numUserCntID NUMERIC,v_numDomainId NUMERIC,v_bitTotal BOOLEAN,v_ClientTimeZoneOffset INTEGER)
RETURNS DECIMAL(10,2) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_decTotalHrsWorked  DECIMAL(10,2);            
   v_decTotalOverTimeHrsWorked  DECIMAL(10,2);          
   v_decTotalHrs  DECIMAL(10,2);
BEGIN
   v_decTotalHrsWorked := 0;          
   v_decTotalOverTimeHrsWorked := 0;          
   v_decTotalHrs := 0;          
   select   sum(x.Hrs) INTO v_decTotalHrsWorked From(Select  coalesce(Sum(Cast((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) as DOUBLE PRECISION)/Cast(60 as DOUBLE PRECISION)),0) as Hrs
      from timeandexpense Where (numtype = 1 Or numtype = 2)  And numCategory = 1
      And (dtFromDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) between v_dtStartDate And 	v_dtEndDate 
      Or dtToDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) between v_dtStartDate And v_dtEndDate) And
      numUserCntID = v_numUserCntID  And numDomainID = v_numDomainId) x;                                                
   v_decTotalOverTimeHrsWorked := GetOverHrsWorked(v_bitOverTime,v_bitOverTimeHrsDailyOrWeekly,v_decOverTime,v_dtStartDate,
   v_dtEndDate,v_numUserCntID,v_numDomainId,v_ClientTimeZoneOffset);          
   if v_bitTotal = true then
      v_decTotalHrs := v_decTotalHrsWorked;
   ELSEIF v_bitTotal = false
   then
      v_decTotalHrs := v_decTotalHrsWorked -v_decTotalOverTimeHrsWorked;
   end if;      
       
   Return v_decTotalHrs;
END; $$;

