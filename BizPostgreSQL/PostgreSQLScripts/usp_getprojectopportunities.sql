-- Stored procedure definition script Usp_GetProjectOpportunities for PostgreSQL
CREATE OR REPLACE FUNCTION Usp_GetProjectOpportunities(v_numProjectId NUMERIC,
v_numDomainId NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select OpportunityMaster.numOppId,vcpOppName from ProjectsOpportunities
   join OpportunityMaster
   on ProjectsOpportunities.numOppId = OpportunityMaster.numOppId
   where numproid = v_numProjectId and OpportunityMaster.numDomainId = v_numDomainId;
END; $$;












