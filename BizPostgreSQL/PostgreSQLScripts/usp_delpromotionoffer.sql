-- Stored procedure definition script USP_DelPromotionOffer for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DelPromotionOffer(v_numDomainID NUMERIC(18,0),
	v_numProId NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
   IF EXISTS(SELECT numProId FROM PromotionOffer WHERE numDomainId = v_numDomainID AND numOrderPromotionID = v_numProId) then
	
      RAISE EXCEPTION 'ITEM_PROMO_USING_ORDER_PROMO';
      RETURN;
   end if;
   -- BEGIN TRANSACTION
DELETE FROM PromotionOfferOrganizations WHERE numProId = v_numProId;
   DELETE FROM PromotionOfferItems WHERE numProId = v_numProId;
   DELETE FROM PromotionOfferSites WHERE numPromotionID = v_numProId;
   DELETE FROM PromotionOfferOrder WHERE numPromotionID = v_numProId;
   DELETE FROM PromotionOffer WHERE numDomainId = v_numDomainID AND numProId = v_numProId;
   -- COMMIT
EXCEPTION WHEN OTHERS THEN
        GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
END; $$;


