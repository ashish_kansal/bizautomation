-- Stored procedure definition script USP_DeleteShippingBox for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteShippingBox(v_numBoxID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM ShippingReportItems
   WHERE numBoxID = v_numBoxID;

   DELETE FROM ShippingBox WHERE numBoxID = v_numBoxID;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_DeleteShippingReport]    Script Date: 05/07/2009 17:40:59 ******/



