-- Function definition script GetWarehouseSerialLot for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetWarehouseSerialLot(v_numDomainID NUMERIC(18,0),
	v_numWarehouseItemID NUMERIC(18,0),
	v_bitLot BOOLEAN DEFAULT false)
RETURNS VARCHAR(8000) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_SerialList  VARCHAR(8000);
BEGIN
   IF v_bitLot = true then
	
      select STRING_AGG(CONCAT(vcSerialNo,'(',numQty,')','(',FormatedDateFromDate(dExpirationDate,v_numDomainID),')'),', ' ORDER BY dExpirationDate ASC) INTO v_SerialList FROM
      WareHouseItmsDTL WHERE
      numWareHouseItemID = v_numWarehouseItemID
      AND numQty > 0;
   ELSE
      select STRING_AGG(vcSerialNo,', ' ORDER BY numWareHouseItmsDTLID ASC) INTO v_SerialList FROM
      WareHouseItmsDTL WHERE
      numWareHouseItemID = v_numWarehouseItemID
      AND numQty > 0;
   end if;

   v_SerialList := coalesce(SUBSTR(v_SerialList,2,LENGTH(v_SerialList)),cast(0 as TEXT));
	
   RETURN v_SerialList;
END; $$;

