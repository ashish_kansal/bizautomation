-- Stored procedure definition script USP_ExtranetDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ExtranetDetails(v_numExtranetID NUMERIC(9,0),  
v_numDomainID NUMERIC(9,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   select numExtranetID,vcCompanyName as Company,numGroupid,div.numDivisionID,numPartnerGroupID,((select coalesce(intNoOfPartners,0) from  Subscribers where numTargetDomainID = v_numDomainID) -(select count(*) from ExtranetAccountsDtl where bitPartnerAccess = true and
      numDomainID = v_numDomainID)) as NoRemPartLic from ExtarnetAccounts Ext
   join DivisionMaster div
   on div.numDivisionID = Ext.numDivisionID
   join CompanyInfo Com
   on Com.numCompanyId = div.numCompanyID
   where numExtranetID = v_numExtranetID  and Ext.numDomainID = v_numDomainID;  
  
  
  
   open SWV_RefCur2 for
   select coalesce(numExtranetDtlID,0) as numExtranetDtlID,vcFirstName || ' ' || vcLastname as ContactName,A.numContactId,vcEmail,coalesce(tintAccessAllowed,0) as PortalAccess,
coalesce(bitPartnerAccess,false) as PartnerAccess,
coalesce(vcPassword,'') as Password from ExtarnetAccounts Ext
   join AdditionalContactsInformation A   on A.numDivisionId = Ext.numDivisionID
   left join ExtranetAccountsDtl E on Ext.numExtranetID = E.numExtranetID and CAST(A.numContactId AS VARCHAR) = E.numContactID
   where vcEmail <> ''  and Ext.numExtranetID = v_numExtranetID;
   RETURN;
END; $$;


