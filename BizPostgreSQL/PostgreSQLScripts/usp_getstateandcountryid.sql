CREATE OR REPLACE FUNCTION USP_GetStateAndCountryID(v_byteMode SMALLINT DEFAULT 0,  
v_vcText VARCHAR(100) DEFAULT '',
v_numDomainID NUMERIC(9,0) DEFAULT NULL,
v_numListID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_byteMode = 0 then -- ListMaster  
      open SWV_RefCur for
      select coalesce(numListItemID,0) from Listdetails
      where vcData = v_vcText and (numDomainid = v_numDomainID or constFlag = true);
   end if;
  
  
   if v_byteMode = 1 then -- State  
      open SWV_RefCur for
      select coalesce(numStateID,0) from State
      where vcState = v_vcText and numDomainID = v_numDomainID;
   end if; 


   if v_byteMode = 2 then -- GroupID 
      open SWV_RefCur for
      select coalesce(numItemGroupID,0) from ItemGroups
      where vcItemGroup = v_vcText and numDomainID = v_numDomainID;
   end if;

   if v_byteMode = 3 then -- Item Type

      open SWV_RefCur for
      select case v_vcText when 'Inventory Item' then 'P' when 'Service' then 'S' else 'N' end;
   end if;

   if v_byteMode = 4 then -- Item Classification
      open SWV_RefCur for
      select  numListItemID from Listdetails where numListID = 36 and numDomainid = v_numDomainID and vcData = v_vcText LIMIT 1;
   end if;

   if v_byteMode = 5 then -- Income Account
      open SWV_RefCur for
      select  numAccountId from Chart_Of_Accounts where numDomainId = v_numDomainID and vcAccountName  = v_vcText LIMIT 1;
   end if;

   if v_byteMode = 6 then -- Warehouse
      open SWV_RefCur for
      select  numWareHouseID from Warehouses where numDomainID = v_numDomainID and vcWareHouse = v_vcText LIMIT 1;
   end if;

   if v_byteMode = 7 then -- Preffered Vendor
      open SWV_RefCur for
      select  DM.numDivisionID from DivisionMaster DM INNER JOIN CompanyInfo CI ON DM.numCompanyID = CI.numCompanyId
      where DM.numDomainID = v_numDomainID and CI.vcCompanyName = v_vcText LIMIT 1;
   end if;

   if v_byteMode = 8 then -- Department
      open SWV_RefCur for
      select numListItemID from Listdetails where numListID = 19 and numDomainid = v_numDomainID and vcData = v_vcText;
   end if;
   if v_byteMode = 9 then -- Category
      open SWV_RefCur for
      select  numCategoryID from Category where vcCategoryName = v_vcText and numDomainID = v_numDomainID LIMIT 1;
   end if;

   if v_byteMode = 10 then -- Action Item Priority
      open SWV_RefCur for
      select  numListItemID from Listdetails where numListID = 33 and numDomainid = v_numDomainID and vcData = v_vcText LIMIT 1;
   end if;

   if v_byteMode = 11 then -- Action Item Type
      open SWV_RefCur for
      select  numListItemID from Listdetails where numListID = 73 and (numDomainid = v_numDomainID OR constFlag = true) and vcData = v_vcText LIMIT 1;
   end if;

   if v_byteMode = 12 then -- Action Item Activity
      open SWV_RefCur for
      select  numListItemID from Listdetails where numListID = 32 and numDomainid = v_numDomainID and vcData = v_vcText LIMIT 1;
   end if;

   if v_byteMode = 13 then -- Association Type
      open SWV_RefCur for
      select  numListItemID from Listdetails where numListID = 43 and numDomainid = v_numDomainID and vcData = v_vcText LIMIT 1;
   end if;
	
   if v_byteMode = 14 then -- User Name
      open SWV_RefCur for
      select  numUserDetailID from UserMaster UM INNER JOIN AdditionalContactsInformation ACI ON ACI.numContactId = UM.numUserDetailId where UM.numDomainID = v_numDomainID AND coalesce(ACI.vcFirstName,'') || ' ' || coalesce(ACI.vcLastname,'') = v_vcText LIMIT 1;
   end if;
/*changed code: while importing username of usermaster doesn't containt updated first name last name.. no way to update that field */
   if v_byteMode = 15 then -- UOM
	
		--select top 1 numUOMId from dbo.UOM where numDomainID=@numDomainID AND vcUnitName = @vcText
      open SWV_RefCur for
      SELECT  u.numUOMId FROM UOM u INNER JOIN Domain d ON u.numDomainID = d.numDomainId
      WHERE u.numDomainID = v_numDomainID AND d.numDomainId = v_numDomainID AND u.vcUnitName = v_vcText AND
      u.tintUnitType =(CASE WHEN coalesce(d.charUnitSystem,'E') = 'E' THEN 1 WHEN d.charUnitSystem = 'M' THEN 2 END) LIMIT 1;
   end if;
   if v_byteMode = 16 then -- Assembly Item or Kit Item
      open SWV_RefCur for
      select  numItemCode from Item where numDomainID = v_numDomainID and vcItemName = v_vcText AND coalesce(bitKitParent,false) = true LIMIT 1;
   end if; 
--AND ISNULL(bitAssembly,0)=1

   if v_byteMode = 17 then -- Item
      open SWV_RefCur for
      select  numItemCode from Item where numDomainID = v_numDomainID and vcItemName = v_vcText LIMIT 1;
   end if;

   if v_byteMode = 18 then -- ListDetail
      open SWV_RefCur for
      select coalesce(numListItemID,0) from Listdetails
      where (numListID = v_numListID OR v_numListID = 0) and vcData = v_vcText and (numDomainid = v_numDomainID or constFlag = true);
   end if;
  
   if v_byteMode = 19 then -- TaxItems
      open SWV_RefCur for
      select  coalesce(numTaxItemID,0) from TaxItems where vcTaxName = v_vcText and numDomainID = v_numDomainID LIMIT 1;
   end if;
   RETURN;
END; $$;




