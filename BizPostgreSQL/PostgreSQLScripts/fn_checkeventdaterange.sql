-- Function definition script fn_checkEventDateRange for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_checkEventDateRange(v_bintDueDate TIMESTAMP,v_bintProcessCreateddate TIMESTAMP,v_startDate INTEGER
			,v_startTime INTEGER,v_StartTimePeriod INTEGER,v_numOppId NUMERIC(9,0),v_numOppStageId NUMERIC(9,0),v_Type CHAR(1))
RETURNS BOOLEAN LANGUAGE plpgsql
   AS $$
   DECLARE
   v_inRange  BOOLEAN;
   v_bintPrevClosedate  TIMESTAMP;
BEGIN
   v_inRange := false;

   if v_startDate = 1 then
		
      if  CAST(SUBSTR(CAST(v_bintDueDate AS VARCHAR(11)),1,11) || '  ' || fn_getCustomDataValue(coalesce(v_startTime,0)::VARCHAR(100),17::NUMERIC) || case when v_StartTimePeriod = 0 then 'AM' else 'PM' end AS TIMESTAMP)
      between  LOCALTIMESTAMP+INTERVAL '-1 hour' and LOCALTIMESTAMP  then
         v_inRange := true;
      end if;
   ELSEIF v_startDate = 2
   then
		
      if  CAST(SUBSTR(CAST(v_bintProcessCreateddate AS VARCHAR(11)),1,11) || '  ' || fn_getCustomDataValue(coalesce(v_startTime,0)::VARCHAR(100),17::NUMERIC) || case when v_StartTimePeriod = 0 then 'AM' else 'PM' end AS TIMESTAMP)
      between  LOCALTIMESTAMP+INTERVAL '-1 hour' and LOCALTIMESTAMP  then
         v_inRange := true;
      end if;
   else
      if v_Type = 'O' then
				
         select   bintStageComDate INTO v_bintPrevClosedate from  OpportunityStageDetails where
         numStagePercentage <> 0 and numoppid = v_numOppId and bitstagecompleted = true
         and numOppStageId = fn_getOppPreviousStage(v_numOppId,v_numOppStageId);
         v_bintPrevClosedate := v_bintPrevClosedate+CAST(v_startDate::bigint -3 || 'day' as interval);
      else
         select   bintStageComDate INTO v_bintPrevClosedate from  ProjectsStageDetails where
         numstagepercentage <> 0 and numProId = v_numOppId and bitStageCompleted = true
         and numProStageId = fn_getProPreviousStage(v_numOppId,v_numOppStageId);
         v_bintPrevClosedate := v_bintPrevClosedate+CAST(v_startDate::bigint -3 || 'day' as interval);
      end if;
      if  CAST(SUBSTR(CAST(v_bintPrevClosedate AS VARCHAR(11)),1,11) || '  ' || fn_getCustomDataValue(coalesce(v_startTime,0)::VARCHAR(100),17::NUMERIC) || case when v_StartTimePeriod = 0 then 'AM' else 'PM' end AS TIMESTAMP)
      between  LOCALTIMESTAMP+INTERVAL '-1 hour' and LOCALTIMESTAMP  then
         v_inRange := true;
      end if;
   end if;
   return  v_inRange;
END; $$;

