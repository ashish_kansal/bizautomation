-- Stored procedure definition script usp_GetSurveyHistoryList for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetSurveyHistoryList(v_numDomainID NUMERIC,               
 v_numContactID NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
--This procedure will return all surveys for the selected Contact         
   AS $$
BEGIN
   open SWV_RefCur for SELECT Distinct sm.numSurID as "numSurID", vcSurName as "vcSurName",
 bintCreatedOn as "dateCreatedOn",
 srm.numRespondantID as "numRespondantID", srm.numSurRating as "numSurRating", CAST(sm.numSurID AS VARCHAR(30)) || ',' || CAST(srm.numRespondantID AS VARCHAR(30)) AS "numContactSurResponseLink"
   FROM SurveyMaster sm INNER JOIN SurveyRespondentsMaster srm ON sm.numSurID = srm.numSurID
   INNER JOIN AdditionalContactsInformation aci ON aci.numContactId = srm.numRegisteredRespondentContactId
   WHERE sm.numDomainID = v_numDomainID
   AND aci.numContactId = v_numContactID
   AND aci.numDomainID = sm.numDomainID;
END; $$;












