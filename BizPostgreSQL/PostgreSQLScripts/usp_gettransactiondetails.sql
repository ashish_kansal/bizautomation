-- Stored procedure definition script USP_GetTransactionDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetTransactionDetails(v_numTransactionID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN

   /* SQLWays Notice: SET TRANSACTION ISOLATION LEVEL READ COMMITTED must be called before procedure call */
-- SET TRANSACTION ISOLATION LEVEL READ COMMITTED
BEGIN
      open SWV_RefCur for SELECT BST.numTransactionID,BST.monAmount,BST.vcCheckNumber,BST.dtDatePosted,BST.vcFITransactionID, BST.vcPayeeName, BST.vcMemo,
BST.vcTxType,BST.numAccountID,BTM.tintReferenceType,BTM.numReferenceID, BD.vcAccountNumber,BD.vcAccountType,BM.vcFIName,
CONCAT(BST.vcPayeeName,BST.vcMemo) AS vcDescription,
C.vcAccountName,
SUBSTR(TO_CHAR(BST.dtDatePosted,'yyyy-mm-dd hh24:mi:ss'),1,10) || ' CREDIT to (' || coalesce(C.vcAccountName,'') ||  ')' || CI.vcCompanyName || ' for $' || BST.monAmount AS TransMatchDetails
      FROM BankStatementTransactions BST
      LEFT JOIN BankTransactionMapping BTM ON BST.numTransactionID = BTM.numTransactionID
      INNER JOIN BankStatementHeader BSH ON BST.numStatementId = BSH.numStatementID
      INNER JOIN BankDetails BD ON BSH.numBankDetailID = BD.numBankDetailID
      INNER JOIN BankMaster BM ON BD.numBankMasterID = BM.numBankMasterID
      LEFT JOIN DepositMaster DM  ON BTM.numReferenceID = DM.numDepositId
      LEFT JOIN DepositeDetails DD ON DD.numDepositID = DM.numDepositId
      LEFT JOIN Chart_Of_Accounts C ON DD.numAccountID = C.numAccountId
      LEFT JOIN DivisionMaster D ON DD.numReceivedFrom	= D.numDivisionID
      LEFT JOIN CompanyInfo CI ON D.numCompanyID = CI.numCompanyId
      WHERE BST.numTransactionID = v_numTransactionID AND
      EXISTS(SELECT * FROM BankTransactionMapping BTM WHERE BTM.numTransactionID = BST.numTransactionID AND BTM.numReferenceID > 0 AND  BTM.tintReferenceType = 6 AND BTM.bitIsActive = true)
      UNION
      SELECT BST.numTransactionID,BST.monAmount,BST.vcCheckNumber,BST.dtDatePosted,BST.vcFITransactionID, BST.vcPayeeName, BST.vcMemo,
BST.vcTxType,BST.numAccountID,BTM.tintReferenceType,BTM.numReferenceID, BD.vcAccountNumber,BD.vcAccountType,BM.vcFIName,
CONCAT(BST.vcPayeeName,BST.vcMemo) AS vcDescription,
C.vcAccountName,
SUBSTR(TO_CHAR(BST.dtDatePosted,'yyyy-mm-dd hh24:mi:ss'),1,10) || ' DEBIT to (' || coalesce(C.vcAccountName,'') ||  ')' || CI.vcCompanyName || ' for $' || ABS(BST.monAmount:: NUMERIC) AS TransMatchDetails
      FROM BankStatementTransactions BST
      LEFT JOIN BankTransactionMapping BTM ON BST.numTransactionID = BTM.numTransactionID
      INNER JOIN BankStatementHeader BSH ON BST.numStatementId = BSH.numStatementID
      INNER JOIN BankDetails BD ON BSH.numBankDetailID = BD.numBankDetailID
      INNER JOIN BankMaster BM ON BD.numBankMasterID = BM.numBankMasterID
      LEFT JOIN CheckHeader CH ON BTM.numReferenceID = CH.numCheckHeaderID
      LEFT JOIN CheckDetails CD ON CH.numCheckHeaderID = CD.numCheckHeaderID
      LEFT JOIN Chart_Of_Accounts C ON CD.numChartAcntId = C.numAccountId
      LEFT JOIN DivisionMaster D ON CH.numDivisionID	= D.numDivisionID
      LEFT JOIN CompanyInfo CI ON D.numCompanyID = CI.numCompanyId
      WHERE BST.numTransactionID = v_numTransactionID AND
      EXISTS(SELECT * FROM BankTransactionMapping BTM WHERE BTM.numTransactionID = BST.numTransactionID AND BTM.numReferenceID > 0 AND  BTM.tintReferenceType = 1 AND BTM.bitIsActive = true)
      UNION
      SELECT BST.numTransactionID,BST.monAmount,BST.vcCheckNumber,BST.dtDatePosted,BST.vcFITransactionID, BST.vcPayeeName, BST.vcMemo,
BST.vcTxType,BST.numAccountID,BTM.tintReferenceType,BTM.numReferenceID, BD.vcAccountNumber,BD.vcAccountType,BM.vcFIName,
CONCAT(BST.vcPayeeName,BST.vcMemo) AS vcDescription,
'' AS vcAccountName,
'UnMatched' AS TransMatchDetails
      FROM BankStatementTransactions BST
      LEFT JOIN BankTransactionMapping BTM ON BST.numTransactionID = BTM.numTransactionID
      INNER JOIN BankStatementHeader BSH ON BST.numStatementId = BSH.numStatementID
      INNER JOIN BankDetails BD ON BSH.numBankDetailID = BD.numBankDetailID
      INNER JOIN BankMaster BM ON BD.numBankMasterID = BM.numBankMasterID
      WHERE BST.numTransactionID = v_numTransactionID AND
      NOT EXISTS(SELECT * FROM BankTransactionMapping BTM WHERE BTM.numTransactionID = BST.numTransactionID AND BTM.bitIsActive = true);
   END;
   RETURN;
END; $$;
	
