-- Stored procedure definition script USP_OpportunityItems_CheckWarehouseMapping for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OpportunityItems_CheckWarehouseMapping(v_numDomainID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numOrderSource  NUMERIC(18,0);
   v_tintSourceType  SMALLINT;
   v_numShipToState  NUMERIC(18,0);
   v_numShipToCountry  NUMERIC(18,0);
   v_bitBOOrderStatus  BOOLEAN;
   v_tintCommitAllocation  SMALLINT;
BEGIN
   IF EXISTS(SELECT * FROM MassSalesFulfillmentConfiguration WHERE numDomainID = v_numDomainID AND coalesce(bitAutoWarehouseSelection,false) = true) then
      select   coalesce(OpportunityMaster.tintSource,0), coalesce(OpportunityMaster.tintSourceType,0) INTO v_numOrderSource,v_tintSourceType FROM
      OpportunityMaster WHERE
      numDomainId = v_numDomainID
      AND numOppId = v_numOppID;
      select   coalesce(numState,0), coalesce(numCountry,0) INTO v_numShipToState,v_numShipToCountry FROM
      fn_getOPPAddressDetails(v_numOppID,v_numDomainID,2::SMALLINT);
      select   coalesce(bitBOOrderStatus,false) INTO v_bitBOOrderStatus FROM MassSalesFulfillmentConfiguration WHERE numDomainID = v_numDomainID;
      DROP TABLE IF EXISTS tt_TEMP CASCADE;
      CREATE TEMPORARY TABLE tt_TEMP
      (
         numWarehouseID NUMERIC(18,0)
      );
      INSERT INTO tt_TEMP(numWarehouseID)
      SELECT
      MSFWP.numWarehouseID
      FROM
      MassSalesFulfillmentWM MSFWM
      INNER JOIN
      MassSalesFulfillmentWMState MSFWMS
      ON
      MSFWM.ID = MSFWMS.numMSFWMID
      INNER JOIN
      MassSalesFulfillmentWP MSFWP
      ON
      MSFWM.ID = MSFWP.numMSFWMID
      WHERE
      numDomainID = v_numDomainID
      AND numOrderSource = v_numOrderSource
      AND tintSourceType = v_tintSourceType
      AND numCountryID = v_numShipToCountry
      AND numStateID = v_numShipToState
      ORDER BY
      intOrder;
      IF(SELECT COUNT(*) FROM tt_TEMP) > 0 then
         select   coalesce(tintCommitAllocation,1) INTO v_tintCommitAllocation FROM Domain WHERE numDomainId = v_numDomainID;
         UPDATE
         OpportunityItems OI
         SET
         bitRequiredWarehouseCorrection = true
         FROM
         Item I, WareHouseItems WI
         WHERE
         OI.numItemCode = I.numItemCode AND OI.numWarehouseItmsID = WI.numWareHouseItemID AND(numOppId = v_numOppID
         AND coalesce(bitDropShip,false) = false
         AND(coalesce(bitWorkOrder,false) = false OR(coalesce(bitWorkOrder,false) = true AND coalesce(numUnitHour,0) -coalesce(numWOQty,0) > 0))
         AND bitRequiredWarehouseCorrection IS NULL
         AND 1 =(CASE
         WHEN v_bitBOOrderStatus = false
         THEN(CASE WHEN WI.numWareHouseID NOT IN(SELECT T.numWarehouseID FROM tt_TEMP T) THEN 1 ELSE 0 END)
         ELSE(CASE
            WHEN WI.numWareHouseID NOT IN(SELECT T.numWarehouseID FROM tt_TEMP T)
            OR 1 =(CASE
            WHEN v_tintCommitAllocation = 1
            THEN(CASE
               WHEN coalesce(I.bitKitParent,false) = true
               THEN(CASE
                  WHEN EXISTS(SELECT OKI.numOppChildItemID FROM OpportunityKitItems OKI INNER JOIN WareHouseItems WIInner ON OKI.numWareHouseItemId = WIInner.numWareHouseItemID WHERE OKI.numOppId = v_numOppID AND OKI.numOppItemID = OI.numoppitemtCode AND coalesce(WIInner.numBackOrder,0) > 0)
                  OR EXISTS(SELECT OKCI.numOppKitChildItemID FROM OpportunityKitChildItems OKCI INNER JOIN WareHouseItems WIInner ON OKCI.numWareHouseItemId = WIInner.numWareHouseItemID WHERE OKCI.numOppId = v_numOppID AND OKCI.numOppItemID = OI.numoppitemtCode AND coalesce(WIInner.numBackOrder,0) > 0)
                  THEN 1
                  ELSE 0
                  END)
               ELSE(CASE WHEN coalesce(WI.numBackOrder,0) > 0 THEN 1 ELSE 0 END)
               END)
            ELSE(CASE
               WHEN coalesce(I.bitKitParent,false) = true
               THEN(CASE
                  WHEN EXISTS(SELECT OKI.numOppChildItemID FROM OpportunityKitItems OKI INNER JOIN WareHouseItems WIInner ON OKI.numWareHouseItemId = WIInner.numWareHouseItemID WHERE OKI.numOppId = v_numOppID AND OKI.numOppItemID = OI.numoppitemtCode AND coalesce((SELECT SUM(WIInner.numOnHand) FROM WareHouseItems WIInner WHERE WIInner.numItemID = WIInner.numItemID AND WIInner.numWareHouseID = WIInner.numWareHouseID),0) < coalesce(OKI.numQtyItemsReq,0))
                  OR EXISTS(SELECT OKCI.numOppKitChildItemID FROM OpportunityKitChildItems OKCI INNER JOIN WareHouseItems WIInner ON OKCI.numWareHouseItemId = WIInner.numWareHouseItemID WHERE OKCI.numOppId = v_numOppID AND OKCI.numOppItemID = OI.numoppitemtCode AND coalesce((SELECT SUM(WIInner.numOnHand) FROM WareHouseItems WIInner WHERE WIInner.numItemID = WIInner.numItemID AND WIInner.numWareHouseID = WIInner.numWareHouseID),0) < coalesce(OKCI.numQtyItemsReq,0))
                  THEN 1
                  ELSE 0
                  END)
               ELSE(CASE WHEN coalesce((SELECT SUM(WIInner.numOnHand) FROM WareHouseItems WIInner WHERE WIInner.numItemID = WI.numItemID AND WIInner.numWareHouseID = WI.numWareHouseID),0) <(coalesce(numUnitHour,0) -coalesce(numWOQty,0)) THEN 1 ELSE 0 END)
               END)
            END)
            THEN 1
            ELSE 0
            END)
         END));
      ELSE
         UPDATE OpportunityItems SET bitRequiredWarehouseCorrection = true WHERE numOppId = v_numOppID AND numWarehouseItmsID > 0 AND bitRequiredWarehouseCorrection IS NULL;
      end if;
      UPDATE OpportunityItems SET bitRequiredWarehouseCorrection = false WHERE numOppId = v_numOppID AND bitRequiredWarehouseCorrection IS NULL;
      IF EXISTS(SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId = v_numOppID AND bitRequiredWarehouseCorrection = true) then
		
         UPDATE OpportunityMaster SET numStatus = 15448 WHERE numDomainId = v_numDomainID AND numOppId = v_numOppID;
      end if;
   ELSE
      UPDATE OpportunityItems SET bitRequiredWarehouseCorrection = false WHERE numOppId = v_numOppID AND bitRequiredWarehouseCorrection IS NULL;
   end if;
   RETURN;
END; $$;


