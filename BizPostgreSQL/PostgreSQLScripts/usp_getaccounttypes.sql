CREATE OR REPLACE FUNCTION public.usp_getaccounttypes(
	v_numdomainid numeric DEFAULT 0,
	v_numaccounttypeid numeric DEFAULT 0,
	v_tintmode numeric DEFAULT 0,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor,
	INOUT swv_refcur2 refcursor DEFAULT NULL::refcursor)
    LANGUAGE 'plpgsql'
AS $BODY$
DECLARE
   v_vcParentAccountCode  VARCHAR(50);
BEGIN
-- This function was converted on Sat Apr 10 14:55:55 2021 using Ispirer SQLWays 9.0 Build 6532 64bit Licensed to BizAutomation - Sandeep Patel - India - Ispirer MnMTK 2020 MSSQLServer to PostgreSQL Database Migration Professional Project License (50 GB, 750 Tables, 426500 LOC, 3 Extension Requests, 1 Month, 20210508).
   IF v_tintMode = 1 then -- select all account type with len(accountcode) = 4
      
      open SWV_RefCur for
      SELECT numAccountTypeID,
               vcAccountCode,
               vcAccountType
      || '-'
      || vcAccountCode AS vcAccountType,
                 vcAccountType AS vcAccountType1,
               coalesce(numParentID,0) AS numParentID,
               numDomainID,
               vcAccountCode || '~' || CAST(numAccountTypeID AS VARCHAR(20)) AS CandidateKey
      FROM   AccountTypeDetail
      WHERE  numDomainID = v_numDomainId
      AND LENGTH(vcAccountCode) = 4;
   ELSEIF v_tintMode = 2
   then
      select   vcAccountCode INTO v_vcParentAccountCode FROM AccountTypeDetail WHERE numAccountTypeID = v_numAccountTypeID;
      open SWV_RefCur for
      SELECT numAccountTypeID,
               vcAccountCode,
               vcAccountType
      || '-'
      || vcAccountCode AS vcAccountType,
                  vcAccountType AS vcAccountType1,
               coalesce(numParentID,0) AS numParentID,
               numDomainID
      FROM   AccountTypeDetail
      WHERE  numDomainID = v_numDomainId
      AND LENGTH(vcAccountCode) > 4
      AND vcAccountCode ilike  coalesce(v_vcParentAccountCode,'') || '%';
   ELSEIF v_tintMode = 3
   then --Select all account type which has atleast one account 
      open SWV_RefCur for
      SELECT    numAccountTypeID,
				vcAccountCode,
				vcAccountType /*+ '-' + [vcAccountCode]*/ AS vcAccountType,
				coalesce(numParentID,0) AS numParentID,
				numDomainID
      FROM      AccountTypeDetail
      WHERE     numDomainID = v_numDomainId
      AND numAccountTypeID IN(SELECT  coalesce(numParntAcntTypeID,0)
         FROM    Chart_Of_Accounts
         WHERE   numDomainId = v_numDomainId);
   ELSE
      IF v_numAccountTypeID = 0 then
          
         open SWV_RefCur for
         SELECT numAccountTypeID,
                   vcAccountCode,
                   vcAccountType
         || '-'
         || vcAccountCode AS vcAccountType,
                   vcAccountType AS vcAccountType1,
                   coalesce(numParentID,0) AS numParentID,
                   numDomainID,
				   coalesce(bitActive,false) AS bitActive
         FROM   AccountTypeDetail
         WHERE  numDomainID = v_numDomainId
         AND numParentID IS NULL;

         open SWV_RefCur2 for
         SELECT numAccountTypeID,
                   vcAccountCode,
                   vcAccountType
         || '-'
         || vcAccountCode AS vcAccountType,
                     vcAccountType AS vcAccountType1,
                   coalesce(numParentID,0) AS numParentID,
                   numDomainID,
				   coalesce(bitActive,false) AS bitActive
         FROM   AccountTypeDetail
         WHERE  numDomainID = v_numDomainId
         AND numParentID IS NOT NULL
         ORDER BY(CASE vcAccountType WHEN 'Assets' THEN 1 WHEN 'Liabilities' THEN 2 WHEN 'Equity' THEN 3 WHEN 'Income' THEN 4 WHEN 'Cost Of Goods' THEN 5 WHEN 'Expense' THEN 6 ELSE 7 END),numParentID,coalesce(tintSortOrder,0),
         vcAccountCode;
      ELSEIF v_numAccountTypeID > 0
      then
            
         open SWV_RefCur for
         SELECT numAccountTypeID,
                     vcAccountCode,
                     vcAccountType,
                     coalesce(numParentID,0) AS numParentID,
                     numDomainID,
					 bitActive
         FROM   AccountTypeDetail
         WHERE  numDomainID = v_numDomainId
         AND numAccountTypeID = v_numAccountTypeID;
      end if;
   end if;
   RETURN;
END;
$BODY$;

