-- Stored procedure definition script USP_DeleteState for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteState(v_numStateID NUMERIC(9,0) DEFAULT 0,  
v_numDomainID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   delete from State where numStateID = v_numStateID and numDomainID = v_numDomainID;
   RETURN;
END; $$;


