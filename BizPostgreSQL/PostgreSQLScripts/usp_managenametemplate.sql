-- Stored procedure definition script USP_ManageNameTemplate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageNameTemplate(v_numDomainID NUMERIC,
    v_strItems TEXT,
    v_tintMode SMALLINT,
    v_tintModuleID SMALLINT, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDocItem  INTEGER;
BEGIN
   IF v_tintMode = 0 then
        
      IF v_tintModuleID = 1 then --SO/PO
			
				/*insert if doesn't exist*/
         IF NOT EXISTS(SELECT * FROM    NameTemplate WHERE   numDomainID = v_numDomainID AND tintModuleID = v_tintModuleID) then
                
            INSERT  INTO NameTemplate(numDomainID,vcModuleName,vcNameTemplate,tintModuleID,numSequenceId,numMinLength,numRecordID)
            SELECT  v_numDomainID,vcModuleName,vcNameTemplate,tintModuleID,numSequenceId,numMinLength,numRecordID
            FROM    NameTemplate WHERE   numDomainID = 0 AND tintModuleID = v_tintModuleID;
         end if;
         open SWV_RefCur for
         SELECT numDomainID,vcModuleName,vcNameTemplate,tintModuleID,coalesce(numSequenceId,1) AS numSequenceId,coalesce(numMinLength,4) AS numMinLength,numRecordID
         FROM NameTemplate WHERE numDomainID = v_numDomainID AND tintModuleID = v_tintModuleID;
      ELSEIF v_tintModuleID = 2
      then --BizDocs
			
         open SWV_RefCur for
         SELECT NT.numDomainID,LD.numListItemID AS numRecordID,vcData AS vcModuleName,coalesce(NT.vcNameTemplate,CASE WHEN LD.numListItemID = 290 THEN 'PROI' ELSE UPPER(SUBSTR(GetListIemName(LD.numListItemID),0,4)) END || '-') AS vcNameTemplate,coalesce(NT.numSequenceId,1) AS numSequenceId,coalesce(NT.numMinLength,4) AS numMinLength
         FROM Listdetails LD LEFT JOIN NameTemplate NT ON LD.numListItemID = NT.numRecordID AND NT.numDomainID = v_numDomainID AND NT.tintModuleID = v_tintModuleID
         WHERE LD.numListID = 27 and (LD.constFlag = true or LD.numDomainid = v_numDomainID);
      end if;
   end if;

   IF v_tintMode = 1 then
      DELETE  FROM NameTemplate WHERE numDomainID = v_numDomainID AND tintModuleID = v_tintModuleID;
      IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then
                
         INSERT  INTO NameTemplate(numDomainID,vcModuleName,vcNameTemplate,tintModuleID,numSequenceId,numMinLength,numRecordID,dtCreatedDate)
         SELECT  v_numDomainID,
                                    X.vcModuleName,
                                    X.vcNameTemplate,
                                    v_tintModuleID,X.numSequenceId,
                                    X.numMinLength,X.numRecordID,TIMEZONE('UTC',now())
         FROM
		  XMLTABLE
		(
			'NewDataSet/Table'
			PASSING 
				CAST(v_strItems AS XML)
			COLUMNS
				id FOR ORDINALITY,
				vcModuleName VARCHAR(50) PATH 'vcModuleName',
				vcNameTemplate VARCHAR(200) PATH 'vcNameTemplate',
				numSequenceId NUMERIC(9,0) PATH 'numSequenceId',
				numMinLength NUMERIC(9,0) PATH 'numMinLength',
				numRecordID NUMERIC(9,0) PATH 'numRecordID'
		) X;  
   
      end if;
      open SWV_RefCur for
      SELECT  '';
   end if;
   RETURN;
END; $$;






