-- Stored procedure definition script USP_GetMarketBudgetDetailsForImpact for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetMarketBudgetDetailsForImpact(v_numDomainId NUMERIC(9,0) DEFAULT 0,          
v_intFiscalYear INTEGER DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select distinct CAST(LD.numListItemID AS VARCHAR(10)) || '~' || CAST(numMarketBudgetId AS VARCHAR(10)) as "numListItemID",LD.vcData as "vcData" From MarketBudgetMaster MBM
   inner join MarketBudgetDetails MBD on MBM.numMarketBudgetId = MBD.numMarketId
   inner join  Listdetails LD on MBD.numListItemID = LD.numListItemID And LD.numListID = 22
   Where MBM.numDomainID = v_numDomainId;    --  MBM.intFiscalYear=@intFiscalYear And         
END; $$;












