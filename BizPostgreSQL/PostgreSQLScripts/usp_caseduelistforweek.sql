-- Stored procedure definition script USP_CaseDueListForWeek for PostgreSQL
CREATE OR REPLACE FUNCTION USP_CaseDueListForWeek(v_numDomainID BIGINT ,      
v_ClientTimeZoneOffset INTEGER   ,                   
v_numContactId NUMERIC(9,0) DEFAULT null,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   vcCaseNumber,
		textDesc,
		numCaseId,
		TO_CHAR(intTargetResolveDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),'mm/dd/yyyy') AS TargetResolveDate
   FROM
   Cases
   WHERE
   numDomainID = v_numDomainID AND (numRecOwner = v_numContactId OR numAssignedTo = v_numContactId) AND CAST(intTargetResolveDate AS DATE) BETWEEN(LOCALTIMESTAMP+INTERVAL '-2 day'):: DATE AND(LOCALTIMESTAMP+INTERVAL '5 day'):: DATE
   ORDER BY intTargetResolveDate;
   RETURN;
END; $$;














