-- Stored procedure definition script USP_ReportListMaster_EmployeeBenefitToCompany for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ReportListMaster_EmployeeBenefitToCompany(v_numDomainID NUMERIC(18,0)
	,v_ClientTimeZoneOffset INTEGER
	,v_vcTimeLine VARCHAR(50)
	,v_vcFilterBy SMALLINT  -- 1:Assign TO, 2: Record Owner, 3: Teams (Based on Assigned To), 4: Employees
	,v_vcFilterValue TEXT, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor , INOUT SWV_RefCur3 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numShippingItemID  NUMERIC(18,0);
   v_numDiscountItemID  NUMERIC(18,0);
   v_dtStartDate  TIMESTAMP; --= DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(MONTH,-12,GETUTCDATE()))
   v_dtEndDate  TIMESTAMP DEFAULT TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval);

   v_i  INTEGER DEFAULT 1;
   v_COUNT  INTEGER DEFAULT 0;
   v_numUserCntID  NUMERIC(18,0);
   v_decTotalHrsWorked  DECIMAL(10,2);
   v_decTotalPaidLeaveHrs  DECIMAL(10,2);
   v_monExpense  DECIMAL(20,5);
   v_monReimburse  DECIMAL(20,5);
   v_monCommPaidInvoice  DECIMAL(20,5);
   v_monCommPaidInvoiceDepositedToBank  DECIMAL(20,5);
   v_monCommUNPaidInvoice  DECIMAL(20,5);
   v_monCommPaidCreditMemoOrRefund  DECIMAL(20,5);
   v_monCommUnPaidCreditMemoOrRefund  DECIMAL(20,5);
   v_GrossProfit  DECIMAL(20,5);
BEGIN
   select   numShippingServiceItemID, numDiscountServiceItemID INTO v_numShippingItemID,v_numDiscountItemID FROM Domain WHERE numDomainId = v_numDomainID;

   IF v_vcTimeLine = 'Last12Months' then
	
      v_dtStartDate := '1900-01-01':: date+CAST((DATE_PART('year',AGE(v_dtEndDate,'1900-01-01':: DATE))*12+DATE_PART('month',AGE(v_dtEndDate,'1900-01-01':: DATE))) || 'month' as interval);
   end if;
   IF v_vcTimeLine = 'Last6Months' then
	
      v_dtStartDate := '1900-01-01':: date+CAST((DATE_PART('year',AGE(v_dtEndDate,'1900-01-01':: DATE))*12+DATE_PART('month',AGE(v_dtEndDate,'1900-01-01':: DATE))) || 'month' as interval);
   end if;
   IF v_vcTimeLine = 'Last3Months' then
	
      v_dtStartDate := '1900-01-01':: date+CAST((DATE_PART('year',AGE(v_dtEndDate,'1900-01-01':: DATE))*12+DATE_PART('month',AGE(v_dtEndDate,'1900-01-01':: DATE))) || 'month' as interval);
   end if;
   IF v_vcTimeLine = 'Last30Days' then
		v_dtStartDate := '1900-01-01':: date + make_interval(days => DATEDIFF('day','1900-01-01'::date + make_interval(days => -30), v_dtEndDate));
   end if;
   IF v_vcTimeLine = 'Last7Days' then
		 v_dtStartDate := '1900-01-01':: date + make_interval(days => DATEDIFF('day','1900-01-01'::date + make_interval(days => -7), v_dtEndDate));
   end if;
	
	-- FETCH ALLREADY PAID COMMISSION DATA AND STORE IN TEMPORARY TABLE
   DROP TABLE IF EXISTS tt_TEMPPAYROLLTRACKING CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPPAYROLLTRACKING AS
      SELECT * FROM PayrollTracking WHERE numDomainID = v_numDomainID;

   DROP TABLE IF EXISTS tt_TEMPCOMMISSIONPAIDCREDITMEMOORREFUND CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPCOMMISSIONPAIDCREDITMEMOORREFUND
   (
      numUserCntID NUMERIC(18,0),
      numReturnHeaderID NUMERIC(18,0),
      tintReturnType SMALLINT,
      numReturnItemID NUMERIC(18,0),
      monCommission DECIMAL(20,5),
      numComRuleID NUMERIC(18,0),
      tintComType SMALLINT,
      tintComBasedOn SMALLINT,
      decCommission DOUBLE PRECISION,
      tintAssignTo SMALLINT
   );    

   INSERT INTO
   tt_TEMPCOMMISSIONPAIDCREDITMEMOORREFUND
   SELECT
   numUserCntID,
		numReturnHeaderID,
		tintReturnType,
		numReturnItemID,
		monCommission,
		numComRuleID,
		tintComType,
		tintComBasedOn,
		decCommission,
		tintAssignTo
   FROM
   GetCommissionPaidCreditMemoOrRefund(v_numDomainID,v_ClientTimeZoneOffset,v_dtStartDate::DATE,v_dtEndDate::DATE);

   BEGIN
      CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numUserCntID NUMERIC(18,0),
      vcUserName VARCHAR(300),
      monHourlyRate DECIMAL(20,5),
      TotalHrsWorked DOUBLE PRECISION,
      TotalPayroll DECIMAL(20,5),
      ProfitMinusPayroll DECIMAL(20,5)
   );

   INSERT INTO tt_TEMP(numUserCntID
		,vcUserName
		,monHourlyRate)
   SELECT
   UM.numUserDetailId
		,CONCAT(ADC.vcFirstName,' ',ADC.vcLastname) AS vcUserName
		,coalesce(UM.monHourlyRate,0)
   FROM
   UserMaster UM
   JOIN
   AdditionalContactsInformation ADC
   ON
   ADC.numDomainID = v_numDomainID
   AND ADC.numContactId = UM.numUserDetailId
   JOIN
   UserTeams UT
   ON
   UT.numDomainID = v_numDomainID
   AND UM.numUserDetailId = UT.numUserCntID
   WHERE
   UM.numDomainID = v_numDomainID
   AND UM.bitactivateflag = true
   AND 1 =(CASE WHEN v_vcFilterBy = 4  THEN(CASE WHEN UT.numUserCntID IN(SELECT Id FROM SplitIDs((v_vcFilterValue),',')) THEN 1 ELSE 0 END)
   ELSE(CASE WHEN LENGTH(coalesce(v_vcFilterValue,'')) > 0 THEN(CASE WHEN UT.numTeam IN(SELECT Id FROM SplitIDs((v_vcFilterValue),',')) THEN 1 ELSE 0 END) ELSE 1 END)
   END);
		


   select   COUNT(*) INTO v_COUNT FROM tt_TEMP;

	-- LOOP OVER EACH EMPLOYEE
   WHILE v_i <= v_COUNT LOOP
      v_decTotalHrsWorked := 0;
      v_decTotalPaidLeaveHrs := 0;
      v_monExpense := 0;
      v_monReimburse := 0;
      v_monCommPaidInvoice := 0;
      v_monCommPaidInvoiceDepositedToBank := 0;
      v_monCommUNPaidInvoice := 0;
      v_numUserCntID := 0;
      v_GrossProfit := 0;
      select   numUserCntID INTO v_numUserCntID FROM tt_TEMP WHERE ID = v_i;
      select   sum(x.Hrs) INTO v_decTotalHrsWorked FROM(SELECT
         coalesce(SUM(CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DOUBLE PRECISION)/CAST(60 AS DOUBLE PRECISION)),0) AS Hrs
         FROM
         timeandexpense
         WHERE
				(numtype = 1 OR numtype = 2)
         AND numCategory = 1
         AND (dtFromDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) BETWEEN v_dtStartDate AND v_dtEndDate
         OR
         dtToDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) BETWEEN v_dtStartDate AND v_dtEndDate)
         AND numUserCntID = v_numUserCntID
         AND numDomainID = v_numDomainID
         AND numCategoryHDRID NOT IN(SELECT numCategoryHDRID FROM tt_TEMPPAYROLLTRACKING WHERE coalesce(numCategoryHDRID,0) > 0)) x;

		-- CALCULATE PAID LEAVE HRS
      select   coalesce(SUM(CASE
      WHEN dtFromDate = dtToDate
      THEN
         24 -(CASE WHEN bitFromFullDay = false THEN 12 ELSE 0 END)
      ELSE(DATE_PART('day',dtToDate:: timestamp -dtFromDate:: timestamp)+1)*24 -(CASE WHEN bitFromFullDay = false THEN 12 ELSE 0 END) -(CASE WHEN bittofullday = false THEN 12 ELSE 0 END)
      END),0) INTO v_decTotalPaidLeaveHrs FROM
      timeandexpense WHERE
      numtype = 3
      AND numCategory = 3
      AND numusercntid = v_numUserCntID
      AND numDomainID = v_numDomainID
      AND (dtFromDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) BETWEEN v_dtStartDate AND v_dtEndDate
      Or
      dtToDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) BETWEEN v_dtStartDate AND v_dtEndDate)
      AND numCategoryHDRID NOT IN(SELECT numCategoryHDRID FROM tt_TEMPPAYROLLTRACKING WHERE coalesce(numCategoryHDRID,0) > 0);		

		-- CALCULATE EXPENSES
      select   coalesce((SUM(CAST(monAmount AS DOUBLE PRECISION))),0) INTO v_monExpense FROM
      timeandexpense WHERE
      numCategory = 2
      AND numtype in(1,2)
      AND numUserCntID = v_numUserCntID
      AND numDomainID = v_numDomainID
      AND (dtFromDate BETWEEN v_dtStartDate And v_dtEndDate Or dtToDate BETWEEN v_dtStartDate And v_dtEndDate)
      AND numCategoryHDRID NOT IN(SELECT numCategoryHDRID FROM tt_TEMPPAYROLLTRACKING WHERE coalesce(numCategoryHDRID,0) > 0);		

		-- CALCULATE REIMBURSABLE EXPENSES
      select   coalesce((SUM(CAST(monAmount AS DOUBLE PRECISION))),0) INTO v_monReimburse FROM
      timeandexpense WHERE
      bitReimburse = true
      AND numCategory = 2
      AND numusercntid = v_numUserCntID
      AND numDomainID = v_numDomainID
      AND dtFromDate BETWEEN v_dtStartDate AND v_dtEndDate
      AND numCategoryHDRID NOT IN(SELECT numCategoryHDRID FROM tt_TEMPPAYROLLTRACKING WHERE coalesce(numCategoryHDRID,0) > 0);        

		-- CALCULATE COMMISSION PAID INVOICE
      select   coalesce(SUM(Amount),0) INTO v_monCommPaidInvoice FROM(SELECT
         oppBiz.numoppid,
					oppBiz.numOppBizDocsId,
					coalesce(SUM(BC.numComissionAmount -coalesce(PT.monCommissionAmt,0)),
         0) AS Amount
         FROM
         OpportunityMaster Opp
         INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numoppid = Opp.numOppId
         INNER JOIN BizDocComission BC on BC.numOppBizDocId = oppBiz.numOppBizDocsId
         LEFT JOIN tt_TEMPPAYROLLTRACKING PT ON PT.numComissionID = BC.numComissionID
         CROSS JOIN LATERAL(SELECT
            MAX(DM.dtDepositDate) AS dtDepositDate
            FROM
            DepositMaster DM
            JOIN DepositeDetails DD	ON DM.numDepositId = DD.numDepositID
            WHERE
            DM.tintDepositePage = 2
            AND DM.numDomainId = v_numDomainID
            AND DD.numOppID = oppBiz.numOppID
            AND DD.numOppBizDocsID = oppBiz.numOppBizDocsID) TEMPDeposit
         WHERE
         Opp.numDomainId = v_numDomainID
         AND (BC.numUserCntID = v_numUserCntID AND coalesce(BC.tintAssignTo,0) <> 3)
         AND BC.bitCommisionPaid = false
         AND oppBiz.bitAuthoritativeBizDocs = true
         AND coalesce(oppBiz.monAmountPaid,0) >= oppBiz.monDealAmount
         AND CAST(TEMPDeposit.dtDepositDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS DATE) BETWEEN CAST(v_dtStartDate AS DATE) AND CAST(v_dtEndDate AS DATE)
         GROUP BY
         oppBiz.numoppid,oppBiz.numOppBizDocsId) A;

		-- CALCULATE COMMISSION UNPAID INVOICE 
      select   coalesce(sum(BC.numComissionAmount -coalesce(PT.monCommissionAmt,0)),
      0) INTO v_monCommUNPaidInvoice FROM
      OpportunityMaster Opp
      INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numoppid = Opp.numOppId
      INNER JOIN BizDocComission BC on BC.numOppBizDocId = oppBiz.numOppBizDocsId
      LEFT JOIN tt_TEMPPAYROLLTRACKING PT ON PT.numComissionID = BC.numComissionID WHERE
      Opp.numDomainId = v_numDomainID
      AND (BC.numUserCntID = v_numUserCntID AND coalesce(BC.tintAssignTo,0) <> 3)
      AND BC.bitCommisionPaid = false
      AND oppBiz.bitAuthoritativeBizDocs = true
      AND coalesce(oppBiz.monAmountPaid,0) < oppBiz.monDealAmount
      AND (oppBiz.dtFromDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) BETWEEN v_dtStartDate AND v_dtEndDate);
      v_monCommPaidCreditMemoOrRefund := coalesce((SELECT SUM(monCommission) FROM tt_TEMPCOMMISSIONPAIDCREDITMEMOORREFUND WHERE (numUserCntId = v_numUserCntID AND coalesce(tintAssignTo,0) <> 3)),0);
      v_GrossProfit := coalesce((SELECT
      SUM(GetOrderItemProfitAmountOrMargin(v_numDomainID,OM.numOppId,OI.numoppitemtCode,1::SMALLINT)) AS Profit
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OI.numOppId = OM.numOppId
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      WHERE
      OM.numDomainId = v_numDomainID
      AND OM.numassignedto = v_numUserCntID
      AND coalesce(OI.monTotAmount,0) <> 0
      AND coalesce(OI.numUnitHour,0) <> 0
      AND coalesce(OM.tintopptype,0) = 1
      AND coalesce(OM.tintoppstatus,0) = 1
      AND OM.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) BETWEEN v_dtStartDate AND v_dtEndDate
      AND I.numItemCode NOT IN(v_numShippingItemID,v_numDiscountItemID)),0);
      UPDATE
      tt_TEMP
      SET
      TotalHrsWorked = v_decTotalHrsWorked,TotalPayroll = v_decTotalHrsWorked*monHourlyRate+v_monReimburse+v_monCommPaidInvoice+v_monCommUNPaidInvoice -coalesce(v_monCommPaidCreditMemoOrRefund,0),ProfitMinusPayroll = v_GrossProfit -(v_decTotalHrsWorked*monHourlyRate+v_monReimburse+v_monCommPaidInvoice+v_monCommUNPaidInvoice -coalesce(v_monCommPaidCreditMemoOrRefund,0))
      WHERE
      numUserCntID = v_numUserCntID;
      v_i := v_i::bigint+1;
   END LOOP;

   open SWV_RefCur for
   SELECT vcUserName,TotalHrsWorked FROM tt_TEMP ORDER BY TotalHrsWorked DESC;
   open SWV_RefCur2 for
   SELECT vcUserName,TotalPayroll FROM tt_TEMP ORDER BY TotalPayroll DESC;
   open SWV_RefCur3 for
   SELECT vcUserName,ProfitMinusPayroll FROM tt_TEMP ORDER BY ProfitMinusPayroll DESC;
   DROP TABLE IF EXISTS tt_TEMPPAYROLLTRACKING CASCADE;
   RETURN;
END; $$;


