-- Stored procedure definition script USP_ManageWorkflowAutomationRules for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ManageWorkflowAutomationRules(v_tintMode SMALLINT DEFAULT 0,
	v_numRuleID NUMERIC(18,0) DEFAULT NULL,
	v_numBizDocTypeId NUMERIC(18,0) DEFAULT 0,
	v_numBizDocStatus1 NUMERIC(18,0) DEFAULT 0,
	v_numBizDocStatus2 NUMERIC(18,0) DEFAULT 0,
	v_tintOppType NUMERIC(18,0) DEFAULT NULL,
	v_numOrderStatus NUMERIC(18,0) DEFAULT 0,
	v_numDomainID NUMERIC(18,0) DEFAULT NULL,
	v_numUserCntID NUMERIC(18,0) DEFAULT NULL,
	v_numEmailTemplate NUMERIC(18,0) DEFAULT NULL,
	v_numBizDocCreatedFrom NUMERIC(18,0) DEFAULT NULL,
	v_numOpenRecievePayment BOOLEAN DEFAULT false,
	v_numCreditCardOption INTEGER DEFAULT NULL,
	v_numBizDocTemplate NUMERIC(18,0) DEFAULT NULL,
	v_numOppSource NUMERIC(18,0) DEFAULT NULL,
	v_bitUpdatedTrackingNo BOOLEAN DEFAULT false,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numAutomationID  NUMERIC(18,0);
BEGIN


   v_numAutomationID := 0;
 
   IF EXISTS(SELECT numAutomationID FROM WorkflowAutomation
   WHERE numRuleID = v_numRuleID AND numDomainID = v_numDomainID AND tintOppType = v_tintOppType
   AND numBizDocTypeId = v_numBizDocTypeId AND numBizDocCreatedFrom = v_numBizDocCreatedFrom) then

      select   numAutomationID INTO v_numAutomationID FROM WorkflowAutomation WHERE numRuleID = v_numRuleID AND numDomainID = v_numDomainID AND tintOppType = v_tintOppType
      AND numBizDocTypeId = v_numBizDocTypeId AND numBizDocCreatedFrom = v_numBizDocCreatedFrom;
      open SWV_RefCur for SELECT v_numAutomationID;
      UPDATE WorkflowAutomation SET
      numRuleID = v_numRuleID,numBizDocTypeId = v_numBizDocTypeId,numBizDocStatus1 = v_numBizDocStatus1,
      numBizDocStatus2 = v_numBizDocStatus2,tintOppType = v_tintOppType,
      numOrderStatus = v_numOrderStatus,numDomainID = v_numDomainID,
      numUserCntID = v_numUserCntID,numEmailTemplate = v_numEmailTemplate,
      numBizDocCreatedFrom = v_numBizDocCreatedFrom,numOpenRecievePayment = v_numOpenRecievePayment,
      numCreditCardOption = v_numCreditCardOption,
      numBizDocTemplate = v_numBizDocTemplate,numOppSource = v_numOppSource,
      bitUpdatedTrackingNo = v_bitUpdatedTrackingNo,dtModified = LOCALTIMESTAMP
      WHERE
      numAutomationID = v_numAutomationID;
   ELSE
      INSERT INTO WorkflowAutomation(numRuleID,
		numBizDocTypeId,
		numBizDocStatus1,
		numBizDocStatus2,
		tintOppType,
		numOrderStatus,
		numDomainID,
		numUserCntID,
		numEmailTemplate,
		numBizDocCreatedFrom,
		numOpenRecievePayment,
		numCreditCardOption,
		numBizDocTemplate,
		numOppSource,
		bitUpdatedTrackingNo,
		dtCreated) VALUES(v_numRuleID,
		v_numBizDocTypeId,
		v_numBizDocStatus1,
		v_numBizDocStatus2,
		v_tintOppType,
		v_numOrderStatus,
		v_numDomainID,
		v_numUserCntID,
		v_numEmailTemplate,
		v_numBizDocCreatedFrom,
		v_numOpenRecievePayment,
		v_numCreditCardOption,
		v_numBizDocTemplate,
		v_numOppSource,
		v_bitUpdatedTrackingNo,
		LOCALTIMESTAMP);
   end if;

   RETURN;
END; $$;












