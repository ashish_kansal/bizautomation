-- Stored procedure definition script USP_CalculateItemProfit for PostgreSQL
CREATE OR REPLACE FUNCTION USP_CalculateItemProfit(v_numDomainID NUMERIC(18,0),
	v_numOppID NUMERIC(18,0),
	v_numUnitHour DOUBLE PRECISION,
	v_monPrice DECIMAL(20,5),
	v_numCost DECIMAL(20,5),
	v_numVendorID NUMERIC(18,0),
	v_numItemCode NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_fltExchangeRate  DOUBLE PRECISION;
   v_avgCost  INTEGER;
   v_VendorCost  DECIMAL(20,5);
BEGIN
   select   coalesce(fltExchangeRate,1) INTO v_fltExchangeRate FROM OpportunityMaster WHERE numOppId = v_numOppID;

   select  numCost INTO v_avgCost from Domain where numDomainId = v_numDomainID     LIMIT 1;

   select   monCost INTO v_VendorCost FROM Vendor WHERE numVendorID = v_numVendorID AND numItemCode = v_numItemCode;

   open SWV_RefCur for SELECT
   cast(coalesce(v_VendorCost,0) as DECIMAL(20,5)) AS numCost,
		fn_UOMConversion(numPurchaseUnit,numItemCode,v_numDomainID,numBaseUnit) as Factor,
		coalesce(v_numUnitHour,0)*(CAST((coalesce(v_monPrice,0)*v_fltExchangeRate) AS INTEGER) -(CASE v_avgCost WHEN 3 THEN CAST(v_VendorCost/fn_UOMConversion(numPurchaseUnit,numItemCode,v_numDomainID,numBaseUnit) AS DOUBLE PRECISION) WHEN 2 THEN v_numCost ELSE monAverageCost END)) AS Profit
   FROM
   Item
   WHERE
   numDomainID = v_numDomainID
   AND numItemCode = v_numItemCode;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_CaseDetails]    Script Date: 07/26/2008 16:15:02 ******/














