-- Function definition script fn_MonthName1 for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_MonthName1(v_intDate INTEGER)
RETURNS VARCHAR(20) LANGUAGE plpgsql
   AS $$
BEGIN
   RETURN fn_MonthName(cast(NULLIF(SUBSTR(cast(v_intDate as VARCHAR(30)),5,2),'') as INTEGER)) || ',' || SUBSTR(SUBSTR(cast(v_intDate as VARCHAR(30)),1,30),length(SUBSTR(cast(v_intDate as VARCHAR(30)),1,30)) -2+1) || ' ' || SUBSTR(SUBSTR(cast(v_intDate as VARCHAR(30)),1,30),1,4);
END; $$;

