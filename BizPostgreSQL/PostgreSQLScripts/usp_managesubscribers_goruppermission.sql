-- Stored procedure definition script USP_ManageSubscribers_GorupPermission for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageSubscribers_GorupPermission(v_numTargetDomainID NUMERIC(18,0)
	,v_numGroupId2 NUMERIC(18,0)
	,v_numGroupUserCntID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   INSERT  INTO AuthenticationGroupMaster(vcGroupName,
        numDomainID,
        tintGroupType,
        bitConsFlag)
    VALUES('Executive',
        v_numTargetDomainID,
        1,
        false);
    
   v_numGroupId2 := CURRVAL('AuthenticationGroupMaster_seq'); 

   INSERT  INTO GroupAuthorization(numGroupID
		,numModuleID
		,numPageID
		,intExportAllowed
		,intPrintAllowed
		,intViewAllowed
		,intAddAllowed
		,intUpdateAllowed
		,intDeleteAllowed
		,numDomainID)
   SELECT
   v_numGroupId2
		,numModuleID
		,numPageID
		,bitIsExportApplicable
		,0
		,bitIsViewApplicable
		,bitIsAddApplicable
		,bitIsUpdateApplicable
		,bitIsDeleteApplicable
		,v_numTargetDomainID
   FROM
   PageMaster;         
        
                
	--MainTab From TabMaster 
   INSERT INTO GroupTabDetails(numGroupID,
        numTabId,
        numRelationShip,
        bitallowed,
        numOrder,tintType)
   SELECT
   v_numGroupId2,
        x.numTabId,
        0,
        true,
        x.numOrder
		,0
   FROM(SELECT *
			,ROW_NUMBER() OVER(ORDER BY numTabId) AS numOrder
      FROM
      TabMaster
      WHERE
      tintTabType = 1
      AND bitFixed = true) x; 

	--default Sub tab From cfw_grp_master
   INSERT  INTO GroupTabDetails(numGroupID,
        numTabId,
        numRelationShip,
        bitallowed,
        numOrder,tintType)
   SELECT
   v_numGroupId2,
        x.Grp_id,
        0,
        true,
        x.numOrder
		,1
   FROM(SELECT *
			,ROW_NUMBER() OVER(ORDER BY Grp_id) AS numOrder
      FROM
      CFw_Grp_Master
      WHERE
      tintType = 2
      AND numDomainID = v_numTargetDomainID) x; 

   INSERT  INTO DashBoardSize(tintColumn,
        tintSize,
        numGroupUserCntID,
        bitGroup)
   SELECT
   tintColumn,
        tintSize,
        v_numGroupId2,
        true
   FROM
   DashBoardSize
   WHERE
   bitGroup = true
   AND numGroupUserCntID = v_numGroupUserCntID
   ORDER BY
   tintColumn; 

   INSERT INTO Dashboard(numReportID,
        numGroupUserCntID,
        tintRow,
        tintColumn,
        tintReportType,
        vcHeader,
        vcFooter,
        tintChartType,
        bitGroup)
   SELECT
   numReportID,
        v_numGroupId2,
        tintRow,
        tintColumn,
        tintReportType,
        vcHeader,
        vcFooter,
        tintChartType,
        true
   FROM
   Dashboard
   WHERE
   bitGroup = true
   AND numGroupUserCntID = v_numGroupUserCntID
   ORDER BY
   tintColumn,tintRow;

   INSERT INTO
   DashboardAllowedReports
   SELECT
   numCustomReportId,
        v_numGroupId2
   FROM
   CustomReport
   WHERE
   numdomainId = v_numTargetDomainID; 

   UPDATE
   Dashboard Table2
   SET
   numReportID = Table1.numOrgReportID
   FROM(SELECT
   numReportID
			,(SELECT  C1.numCustomReportId FROM CustomReport C1 JOIN CustomReport C2 ON C1.vcReportName = C2.vcReportName WHERE C1.numdomainId = v_numTargetDomainID AND C2.numCustomReportId = Dashboard.numReportID AND C2.numdomainId = 1 LIMIT 1) AS numOrgReportID
   FROM
   Dashboard
   WHERE
   bitGroup = true
   AND numGroupUserCntID = v_numGroupUserCntID) Table1
   WHERE
   Table2.numReportID = Table1.numReportID AND(bitGroup = true
   AND numGroupUserCntID = v_numGroupId2);
   RETURN;
END; $$;


