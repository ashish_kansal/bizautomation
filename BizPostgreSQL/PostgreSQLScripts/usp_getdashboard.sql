CREATE OR REPLACE FUNCTION USP_GetDashBoard(v_numUserCntID NUMERIC(9,0),            
v_numGroupID NUMERIC(9,0),            
v_numDomainID NUMERIC(9,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_numUserCntID = 0 then

      open SWV_RefCur for
      select tintColumn,tintSize from DashBoardSize where bitgroup = true and numGroupUserCntID = v_numGroupID order by tintColumn;
   ELSEIF exists(select * from DashBoardSize where bitgroup = false and numGroupUserCntID = v_numUserCntID)
   then

      open SWV_RefCur for
      select tintColumn,tintSize from DashBoardSize where bitgroup = false and numGroupUserCntID = v_numUserCntID order by tintColumn;
   else
      insert into DashBoardSize(tintColumn,tintSize,numGroupUserCntID,bitGroup)
      select tintColumn,tintSize,v_numUserCntID,0 from DashBoardSize where bitgroup = true and numGroupUserCntID = v_numGroupID order by tintColumn;
      open SWV_RefCur for
      select tintColumn,tintSize from DashBoardSize where bitgroup = false and numGroupUserCntID = v_numUserCntID order by tintColumn;
   end if;            
  
   DROP TABLE IF EXISTS tt_TEMPDASHBOARD CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPDASHBOARD
   (
      numDashBoardReptID NUMERIC,
      numReportID NUMERIC,
      tintColumn SMALLINT,
      tintRow SMALLINT,
      tintReportType SMALLINT,
      tintChartType SMALLINT,
      tintReportCategory SMALLINT
   );          
            
   if v_numUserCntID = 0 then

      INSERT INTO tt_TEMPDASHBOARD
      select numDashBoardReptID,numReportID,tintColumn,tintRow,tintReportType,tintChartType,tintReportCategory from Dashboard
      join CustomReport
      on numCustomReportId = numReportID
      where  bitgroup = true and numGroupUserCntID = v_numGroupID AND tintReportCategory = 1 order by tintColumn,tintRow;
   ELSEIF exists(select * from Dashboard  join CustomReport
   on numCustomReportId = numReportID where bitgroup = false and numGroupUserCntID = v_numUserCntID)
   then

      INSERT INTO tt_TEMPDASHBOARD
      select numDashBoardReptID,numReportID,tintColumn,tintRow,tintReportType,tintChartType,tintReportCategory from Dashboard  join CustomReport
      on numCustomReportId = numReportID where  bitgroup = false and numGroupUserCntID = v_numUserCntID AND tintReportCategory = 1 order by tintColumn,tintRow;
   else
      insert into Dashboard(numReportID,numGroupUserCntID,tintRow,tintColumn,tintReportType,vcHeader,vcFooter,tintChartType,bitGroup,tintReportCategory)
      select numReportID,v_numUserCntID,tintRow,tintColumn,tintReportType,vcHeader,vcFooter,tintChartType,0,1 from Dashboard where  bitgroup = true and numGroupUserCntID = v_numGroupID order by tintColumn,tintRow;
      INSERT INTO tt_TEMPDASHBOARD
      select numDashBoardReptID,numReportID,tintColumn,tintRow,tintReportType,tintChartType,tintReportCategory from Dashboard  join CustomReport
      on numCustomReportId = numReportID where  bitgroup = false and numGroupUserCntID = v_numUserCntID AND tintReportCategory = 1 order by tintColumn,tintRow;
   end if;


   IF v_numUserCntID > 0 then

      INSERT INTO tt_TEMPDASHBOARD
      select numDashBoardReptID,numReportID,tintColumn,tintRow,tintReportType,tintChartType,tintReportCategory from Dashboard  join CommissionReports
      on numReportID = numComReports AND numGroupUserCntID = numContactID where  numGroupUserCntID = v_numUserCntID AND tintReportCategory = 2 AND CommissionReports.numDomainID = v_numDomainID AND CommissionReports.bitCommContact = false order by tintColumn,tintRow;
   end if; 

   open SWV_RefCur2 for
   SELECT * FROM tt_TEMPDASHBOARD;

   RETURN;
END; $$;


