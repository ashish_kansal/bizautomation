-- Stored procedure definition script USP_UpdateTaskOrder for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateTaskOrder(v_numDomainID NUMERIC(9,0) DEFAULT 0,    
v_vcSortedTask VARCHAR(500) DEFAULT '')
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE StagePercentageDetailsTask AS ST SET numOrder = "Row#" FROM(SELECT Id,ROW_NUMBER()  OVER(ORDER BY(SELECT 1)) AS "Row#" FROM SplitIDs(v_vcSortedTask,',')) AS T
   WHERE ST.numTaskId = T.Id AND (ST.numDomainID = v_numDomainID AND ST.numTaskId = T.Id);

   RETURN;
END; $$; 




