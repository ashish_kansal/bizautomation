-- Function definition script GetOppLstStageCompltd for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetOppLstStageCompltd(v_numOppid NUMERIC(9,0))
RETURNS VARCHAR(500) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_StageDetail  VARCHAR(500);
   v_DueDate TIMESTAMP;
   v_ComDate TIMESTAMP;
   v_numOppStageId NUMERIC;
BEGIN
   select  numOppStageId INTO v_numOppStageId from OpportunityStageDetails where numoppid = v_numOppid and numStagePercentage != 100 and bitstagecompleted = true
   and bintStageComDate =(select  max(bintStageComDate) from OpportunityStageDetails
      where numoppid = v_numOppid and bitstagecompleted = true LIMIT 1)     LIMIT 1;
   if v_numOppStageId <> 0 then

      select vcstagedetail INTO v_StageDetail from OpportunityStageDetails where numOppStageId = v_numOppStageId;
   end if;

   return v_StageDetail;
END; $$;

