-- Stored procedure definition script usp_FindItem for PostgreSQL
CREATE OR REPLACE FUNCTION usp_FindItem(v_str VARCHAR(20) DEFAULT '',INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select numItemCode as "numItemCode",vcItemName as "vcItemName" from Item where vcItemName ilike coalesce(v_str,'') || '%';
END; $$;












