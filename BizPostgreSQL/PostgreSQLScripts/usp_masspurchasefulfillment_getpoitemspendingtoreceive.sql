-- Stored procedure definition script USP_MassPurchaseFulfillment_GetPOItemsPendingToReceive for PostgreSQL
CREATE OR REPLACE FUNCTION USP_MassPurchaseFulfillment_GetPOItemsPendingToReceive(v_numDomainID NUMERIC(18,0),
	v_numOppID NUMERIC(18,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   OpportunityMaster.numOppId AS OppID
		,coalesce(OpportunityMaster.bitStockTransfer,false) AS IsStockTransfer
		,OpportunityMaster.numDivisionId AS DivisionID
		,coalesce(OpportunityMaster.vcpOppName,'''')  AS OppName
		,coalesce(OpportunityMaster.bitPPVariance,false) AS IsPPVariance
		,coalesce(OpportunityMaster.numCurrencyID,0) AS CurrencyID
		,coalesce(OpportunityMaster.fltExchangeRate,0) AS ExchangeRate
		,coalesce(OpportunityItems.numoppitemtCode,0) AS OppItemID
        ,coalesce(Item.vcItemName,'') AS ItemName
        ,coalesce(OpportunityItems.numWarehouseItmsID,0) AS WarehouseItemID
        ,coalesce(OpportunityItems.numUnitHour,0) -coalesce(OpportunityItems.numQtyReceived,0) AS RemainingQty
        ,coalesce(OpportunityItems.numUnitHour,0) -coalesce(OpportunityItems.numQtyReceived,0) AS QtyToReceive
        ,'' AS SerialLot
        ,coalesce(Item.bitSerialized,false) AS IsSerial
        ,coalesce(Item.bitLotNo,false) AS IsLot
        ,coalesce(Item.charItemType,'') AS CharItemType
        ,coalesce(OpportunityItems.vcType,'') AS ItemType
        ,coalesce(OpportunityItems.monPrice,0) AS Price
        ,coalesce(OpportunityItems.bitDropShip,false) AS DropShip
        ,coalesce(OpportunityItems.numProjectID,0) AS ProjectID
        ,coalesce(OpportunityItems.numClassID,0) AS ClassID
        ,coalesce(Item.numAssetChartAcntId,0) AS AssetChartAcntId
        ,coalesce(Item.numCOGsChartAcntId,0) AS COGsChartAcntId
        ,coalesce(Item.numIncomeChartAcntId,0) AS IncomeChartAcntId
		,true AS IsSuccess
		,'' AS ErrorMessage
   FROM
   OpportunityMaster
   INNER JOIN
   OpportunityItems
   ON
   OpportunityMaster.numOppId = OpportunityItems.numoppitemtCode
   INNER JOIN
   Item
   ON
   OpportunityItems.numItemCode = Item.numItemCode
   WHERE
   OpportunityMaster.numDomainId = v_numDomainID
   AND OpportunityMaster.numOppId = v_numOppID
   AND OpportunityMaster.tintopptype = 2
   AND OpportunityMaster.tintoppstatus = 1
   AND coalesce(OpportunityMaster.tintshipped,0) = 0
   AND coalesce(OpportunityItems.numUnitHour,0) -coalesce(OpportunityItems.numUnitHourReceived,0) -coalesce(OpportunityItems.numQtyReceived,0) > 0;
END; $$;












