-- Stored procedure definition script USP_ManageParentChildCustomFieldMap for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_ManageParentChildCustomFieldMap(v_numParentChildFieldID NUMERIC(18,0),
	v_numDomainID NUMERIC(18,0),
	v_tintParentModule SMALLINT,
	v_numParentFieldID NUMERIC(18,0),
	v_tintChildModule SMALLINT,
	v_numChildFieldID NUMERIC(18,0),
	v_tintMode SMALLINT,
	v_bitCustomField BOOLEAN DEFAULT false)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_ParentFld_type  VARCHAR(20);
   v_ChildFld_type  VARCHAR(20);
BEGIN
   IF v_tintMode = 1 then

      DELETE FROM ParentChildCustomFieldMap WHERE numDomainID = v_numDomainID AND numParentChildFieldID = v_numParentChildFieldID;
   ELSE
      IF EXISTS(SELECT 1 FROM ParentChildCustomFieldMap WHERE numDomainID = v_numDomainID AND tintChildModule = v_tintChildModule AND numChildFieldID = v_numChildFieldID AND bitCustomField = v_bitCustomField) then
	
         RAISE EXCEPTION 'AlreadyExists';
      ELSEIF coalesce(v_bitCustomField,false) = false
      then
		
         select   vcAssociatedControlType INTO v_ParentFld_type FROM DycFieldMaster WHERE numFieldID = v_numParentFieldID;
         select   vcAssociatedControlType INTO v_ChildFld_type FROM DycFieldMaster WHERE numFieldID = v_numChildFieldID;
         IF v_ParentFld_type != v_ChildFld_type then
			
            RAISE EXCEPTION 'FieldTypeMisMatch';
         ELSE
            INSERT INTO ParentChildCustomFieldMap(numDomainID,tintParentModule,numParentFieldID,tintChildModule,numChildFieldID,bitCustomField)
            SELECT v_numDomainID,v_tintParentModule,v_numParentFieldID,v_tintChildModule,v_numChildFieldID,0;
         end if;
      ELSE
         select   fld_type INTO v_ParentFld_type FROM CFW_Fld_Master WHERE numDomainID = v_numDomainID AND Grp_id = v_tintParentModule AND Fld_id = v_numParentFieldID;
         select   fld_type INTO v_ChildFld_type FROM CFW_Fld_Master WHERE numDomainID = v_numDomainID AND Grp_id = v_tintChildModule AND Fld_id = v_numChildFieldID;
         IF v_ParentFld_type != v_ChildFld_type then
			
            RAISE EXCEPTION 'FieldTypeMisMatch';
         ELSE
            INSERT INTO ParentChildCustomFieldMap(numDomainID,tintParentModule,numParentFieldID,tintChildModule,numChildFieldID,bitCustomField) SELECT v_numDomainID,v_tintParentModule,v_numParentFieldID,v_tintChildModule,v_numChildFieldID,1;
         end if;
      end if;
   end if;
/****** Object:  StoredProcedure [USP_ManageReturnHeaderItems]    ******/
   RETURN;
END; $$;


