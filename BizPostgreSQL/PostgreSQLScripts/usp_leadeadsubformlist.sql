-- Stored procedure definition script USP_LeadeadSubformList for PostgreSQL
CREATE OR REPLACE FUNCTION USP_LeadeadSubformList(v_numDomainId NUMERIC DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   numSubFormId AS "numSubFormId",vcFormName AS "vcFormName"
   FROM
   LeadsubForms
   WHERE
   numDomainID = v_numDomainId;
END; $$;












