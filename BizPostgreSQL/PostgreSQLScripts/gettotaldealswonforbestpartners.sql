-- Function definition script GetTotalDealsWonForBestPartners for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetTotalDealsWonForBestPartners(v_numCompanyID NUMERIC(9,0))
RETURNS INTEGER LANGUAGE plpgsql
   AS $$
   DECLARE
   v_dcDealsWon  INTEGER;
BEGIN
   select   COUNT(OM.numOppId) INTO v_dcDealsWon FROM         AdditionalContactsInformation AI
   INNER JOIN DivisionMaster DM
   ON AI.numDivisionId = DM.numDivisionID
   INNER JOIN CompanyInfo CI
   ON DM.numCompanyID = CI.numCompanyId
   INNER JOIN OpportunityMaster OM
   ON DM.numDivisionID = OM.numDivisionId WHERE OM.tintoppstatus = 1
   AND CI.numCompanyId = v_numCompanyID GROUP BY OM.numOppId;



   RETURN v_dcDealsWon;
END; $$;

