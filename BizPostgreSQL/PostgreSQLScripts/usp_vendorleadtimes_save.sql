CREATE OR REPLACE FUNCTION usp_vendorleadtimes_save
(
	p_numdomainid NUMERIC(18,0)
	,p_numusercntid NUMERIC(18,0)
	,p_numvendorleadtimesid NUMERIC(18,0)
	,p_numdivisionid NUMERIC(18,0)
	,p_vcdata JSONB
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
	IF EXISTS (SELECT numvendorleadtimesid FROM vendorleadtimes vlt INNER JOIN DivisionMaster dm ON vlt.numdivisionid = dm.numdivisionid WHERE vlt.numvendorleadtimesid=p_numvendorleadtimesid AND dm.numdomainid=p_numdomainid) THEN
		UPDATE
			vendorleadtimes
		SET
			vcdata = p_vcdata
		WHERE
			numvendorleadtimesid = p_numvendorleadtimesid;
	ELSEIF p_numvendorleadtimesid = 0 THEN
		IF EXISTS (SELECT numdivisionid FROM DivisionMaster WHERE numdomainid=p_numdomainid AND numdivisionid=p_numdivisionid) THEN
			INSERT INTO vendorleadtimes
			(
				numdivisionid
				,vcdata
			)
			VALUES 
			(
				p_numdivisionid
				,p_vcdata
			);
		ELSE
			RAISE EXCEPTION 'VENDOR_DOES_NOT_EXISTS';
		END IF;
	END IF;
END; $$;