DROP FUNCTION IF EXISTS USP_ManageReturnHeaderItems;

CREATE OR REPLACE FUNCTION USP_ManageReturnHeaderItems(INOUT v_numReturnHeaderID NUMERIC(18,0) ,
      v_vcRMA VARCHAR(100),
      v_vcBizDocName VARCHAR(100),
      v_numDomainId NUMERIC(18,0),
      v_numUserCntID NUMERIC(9,0) DEFAULT 0,
      v_numDivisionId NUMERIC(18,0) DEFAULT NULL,
      v_numContactId NUMERIC(18,0) DEFAULT NULL,
      v_numOppId NUMERIC(18,0) DEFAULT NULL,
      v_tintReturnType SMALLINT DEFAULT NULL,
      v_numReturnReason NUMERIC(18,0) DEFAULT NULL,
      v_numReturnStatus NUMERIC(18,0) DEFAULT NULL,
      v_monAmount DECIMAL(20,5) DEFAULT NULL,
      v_monTotalTax DECIMAL(20,5) DEFAULT NULL,
      v_monTotalDiscount DECIMAL(20,5) DEFAULT NULL,
      v_tintReceiveType SMALLINT DEFAULT NULL,
      v_vcComments TEXT DEFAULT NULL,
      v_strItems TEXT DEFAULT '',
      v_tintMode SMALLINT DEFAULT 0,
      v_numBillAddressId NUMERIC(18,0) DEFAULT NULL,
      v_numShipAddressId NUMERIC(18,0) DEFAULT NULL,
      v_numDepositIDRef NUMERIC(18,0) DEFAULT NULL,
      v_numBillPaymentIDRef NUMERIC(18,0) DEFAULT NULL,
	  v_numParentID NUMERIC(18,0) DEFAULT 0,
	  v_numReferenceSalesOrder NUMERIC(18,0) DEFAULT 0,
	  v_numOppBizDocsId NUMERIC(18,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDocItem  INTEGER;                                                                                                                                                                
 
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   v_numAccountClass  NUMERIC(18,0) DEFAULT 0;

   v_tintDefaultClassType  INTEGER DEFAULT 0;
   v_tintType  SMALLINT; 
   v_numRMATempID  NUMERIC(18,0);
   v_numBizdocTempID  NUMERIC(18,0);

   v_vcStreet  VARCHAR(100);
   v_vcCity  VARCHAR(50);
   v_vcPostalCode  VARCHAR(15);
   v_vcCompanyName  VARCHAR(100);
   v_vcAddressName  VARCHAR(50);
   v_vcAltContact  VARCHAR(200);     
   v_numState  NUMERIC(9,0);
   v_numCountry  NUMERIC(9,0);
   v_numCompanyId  NUMERIC(9,0);
   v_numContact  NUMERIC(18,0); 
   v_bitIsPrimary  BOOLEAN;
   v_bitAltContact  BOOLEAN;
BEGIN
   BEGIN
      -- BEGIN TRANSACTION
IF v_numReturnHeaderID = 0 then
                 
		-- GET ACCOUNT CLASS IF ENABLED
         select   coalesce(tintDefaultClassType,0) INTO v_tintDefaultClassType FROM Domain WHERE numDomainId = v_numDomainId;
         IF v_tintDefaultClassType = 1 then --USER
		
            select   coalesce(numDefaultClass,0) INTO v_numAccountClass FROM
            UserMaster UM
            JOIN
            Domain D
            ON
            UM.numDomainID = D.numDomainId WHERE
            D.numDomainId = v_numDomainId
            AND UM.numUserDetailId = v_numUserCntID;
         ELSEIF v_tintDefaultClassType = 2
         then --COMPANY
		
            select   coalesce(numAccountClassID,0) INTO v_numAccountClass FROM
            DivisionMaster DM WHERE
            DM.numDomainID = v_numDomainId
            AND DM.numDivisionID = v_numDivisionId;
         ELSE
            v_numAccountClass := 0;
         end if;
         INSERT  INTO ReturnHeader(vcRMA,vcBizDocName,numDomainID,numDivisionId,numContactID,
            numOppId,tintReturnType,numReturnReason,numReturnStatus,monAmount,
            monTotalTax,monTotalDiscount,tintReceiveType,vcComments,
            numCreatedBy,dtCreatedDate,monBizDocAmount,monBizDocUsedAmount,numDepositIDRef,
			numBillPaymentIDRef,numAccountClass,numParentID,IsUnappliedPayment,numReferenceSalesOrder,numOppBizDocID)
         SELECT
         v_vcRMA,v_vcBizDocName,v_numDomainId,v_numDivisionId,v_numContactId,v_numOppId,
            v_tintReturnType,v_numReturnReason,v_numReturnStatus,v_monAmount,v_monTotalTax,
			v_monTotalDiscount,v_tintReceiveType,v_vcComments,v_numUserCntID,TIMEZONE('UTC',now()),0,0,
			v_numDepositIDRef,v_numBillPaymentIDRef,v_numAccountClass,v_numParentID,
			(CASE WHEN(SELECT DM.tintDepositePage FROM DepositMaster AS DM
            WHERE DM.numDepositId = v_numDepositIDRef AND DM.numDomainId = v_numDomainId) = 2 THEN true
         ELSE false
         END),v_numReferenceSalesOrder,v_numOppBizDocsId RETURNING numReturnHeaderID INTO v_numReturnHeaderID;
         
         open SWV_RefCur for
         SELECT  v_numReturnHeaderID;

		--Update DepositMaster if Refund UnApplied Payment
         IF v_numDepositIDRef > 0 AND v_tintReturnType = 4 then
		
            UPDATE DepositMaster SET monRefundAmount = v_monAmount WHERE numDepositId = v_numDepositIDRef;
         end if;
						
		--Update BillPaymentHeader if Refund UnApplied Payment
         IF v_numBillPaymentIDRef > 0 AND v_tintReturnType = 4 then
		
            UPDATE BillPaymentHeader SET monRefundAmount = v_monAmount WHERE numBillPaymentID = v_numBillPaymentIDRef;
         end if;
         --  tinyint
				 --  numeric(18, 0)
         v_tintType := CASE When v_tintReturnType = 1 OR v_tintReturnType = 2 THEN 7
         WHEN v_tintReturnType = 3 THEN 6
         WHEN v_tintReturnType = 4 THEN 5 END;
         PERFORM USP_UpdateBizDocNameTemplate(v_tintType := v_tintType::SMALLINT,v_numDomainId := v_numDomainId,v_RecordID := v_numReturnHeaderID); --  numeric(18, 0)

         IF v_tintReturnType = 1 then
        
            select   numBizDocTempID INTO v_numRMATempID FROM BizDocTemplate WHERE   numOppType = 1 AND bitDefault = true AND bitEnabled = true
            AND numDomainID = v_numDomainId AND numBizDocID IN(SELECT  numListItemID
               FROM   Listdetails WHERE  vcData = 'RMA' AND constFlag = true LIMIT 1)    LIMIT 1;
         ELSEIF v_tintReturnType = 2
         then
        
            select   numBizDocTempID INTO v_numRMATempID FROM BizDocTemplate WHERE   numOppType = 2 AND bitDefault = true AND bitEnabled = true
            AND numDomainID = v_numDomainId AND numBizDocID IN(SELECT  numListItemID
               FROM   Listdetails WHERE  vcData = 'RMA' AND constFlag = true LIMIT 1)    LIMIT 1;
         ELSEIF v_tintReturnType = 3
         then
        
            select   numBizDocTempID INTO v_numRMATempID FROM    BizDocTemplate WHERE   numOppType = 1 AND bitDefault = true AND bitEnabled = true
            AND numDomainID = v_numDomainId AND numBizDocID IN(SELECT  numListItemID
               FROM   Listdetails WHERE  vcData = 'Credit Memo' AND constFlag = true LIMIT 1)    LIMIT 1;
         ELSEIF v_tintReturnType = 4
         then
        
            select   numBizDocTempID INTO v_numRMATempID FROM    BizDocTemplate WHERE   numOppType = 1 AND bitDefault = true AND bitEnabled = true
            AND numDomainID = v_numDomainId AND numBizDocID IN(SELECT  numListItemID
               FROM   Listdetails WHERE  vcData = 'Refund Receipt' AND constFlag = true LIMIT 1)    LIMIT 1;
            v_numBizdocTempID := v_numRMATempID;
         end if;
         UPDATE  ReturnHeader
         SET     numRMATempID = coalesce(v_numRMATempID,0),numBizdocTempID = coalesce(v_numBizdocTempID,0)
         WHERE   numReturnHeaderID = v_numReturnHeaderID;
			                
		--Add/Update Address Details
         v_numOppId := NULLIF(v_numOppId,0);
         select   vcCompanyName, div.numCompanyID INTO v_vcCompanyName,v_numCompanyId FROM    CompanyInfo Com
         JOIN DivisionMaster div ON div.numCompanyID = Com.numCompanyId WHERE   div.numDivisionID = v_numDivisionId;

		--Bill Address
         IF v_numBillAddressId > 0 then
        
             --  numeric(9, 0)
                 --  tinyint
                 --  varchar(100)
                 --  varchar(50)
                 --  varchar(15)
                 --  numeric(9, 0)
                 --  numeric(9, 0)
                 --  varchar(100)
                 --  numeric(9, 0)
            select   coalesce(vcStreet,''), coalesce(vcCity,''), coalesce(vcPostalCode,''), coalesce(numState,0), coalesce(numCountry,0), bitIsPrimary, vcAddressName, numContact, bitAltContact, vcAltContact INTO v_vcStreet,v_vcCity,v_vcPostalCode,v_numState,v_numCountry,v_bitIsPrimary,
            v_vcAddressName,v_numContact,v_bitAltContact,v_vcAltContact FROM    AddressDetails WHERE   numDomainID = v_numDomainId
            AND numAddressID = v_numBillAddressId;
            PERFORM USP_UpdateOppAddress(v_numOppId := v_numOppId,v_byteMode := 0::SMALLINT,p_numAddressID := v_numBillAddressId,v_vcStreet := v_vcStreet,
            v_vcCity := v_vcCity,v_vcPostalCode := v_vcPostalCode,v_numState := v_numState,
            v_numCountry := v_numCountry,v_vcCompanyName := v_vcCompanyName,
            v_numCompanyId := 0,v_vcAddressName := v_vcAddressName,v_numReturnHeaderID := v_numReturnHeaderID,
            v_bitCalledFromProcedure := true,
            v_numContact := v_numContact,v_bitAltContact := v_bitAltContact,v_vcAltContact := v_vcAltContact);
         end if;
    
		--Ship Address
         IF v_numShipAddressId > 0 then
        
             --  numeric(9, 0)
                 --  tinyint
                 --  varchar(100)
                 --  varchar(50)
                 --  varchar(15)
                 --  numeric(9, 0)
                 --  numeric(9, 0)
                 --  varchar(100)
                 --  numeric(9, 0)
            select   coalesce(vcStreet,''), coalesce(vcCity,''), coalesce(vcPostalCode,''), coalesce(numState,0), coalesce(numCountry,0), bitIsPrimary, vcAddressName, numContact, bitAltContact, vcAltContact INTO v_vcStreet,v_vcCity,v_vcPostalCode,v_numState,v_numCountry,v_bitIsPrimary,
            v_vcAddressName,v_numContact,v_bitAltContact,v_vcAltContact FROM    AddressDetails WHERE   numDomainID = v_numDomainId
            AND numAddressID = v_numShipAddressId;
            PERFORM USP_UpdateOppAddress(v_numOppId := v_numOppId,v_byteMode := 1::SMALLINT,p_numAddressID := v_numShipAddressId,v_vcStreet := v_vcStreet,
            v_vcCity := v_vcCity,v_vcPostalCode := v_vcPostalCode,v_numState := v_numState,
            v_numCountry := v_numCountry,v_vcCompanyName := v_vcCompanyName,
            v_numCompanyId := v_numCompanyId,v_vcAddressName := v_vcAddressName,
            v_numReturnHeaderID := v_numReturnHeaderID,v_bitCalledFromProcedure := true,
            v_numContact := v_numContact,v_bitAltContact := v_bitAltContact,
            v_vcAltContact := v_vcAltContact);
         end if;
           
		   
		-- IMPORTANT: KEEP THIS BELOW ADDRESS UPDATE LOGIC 
         IF v_tintReturnType = 1 OR (v_tintReturnType = 2 AND(SELECT coalesce(bitPurchaseTaxCredit,false) FROM Domain WHERE numDomainId = v_numDomainId) = true) then
		
            IF v_numOppId > 0 then
			
               INSERT INTO OpportunityMasterTaxItems(numReturnHeaderID,
					numTaxItemID,
					fltPercentage,
					tintTaxType,
					numTaxID)
               SELECT
               v_numReturnHeaderID,
					numTaxItemID,
					fltPercentage,
					tintTaxType,
					numTaxID
               FROM
               OpportunityMasterTaxItems
               WHERE
               numOppId = v_numOppId;
            ELSE 
				--Insert Tax for Division                       
               INSERT INTO OpportunityMasterTaxItems(numReturnHeaderID,
					numTaxItemID,
					fltPercentage,
					tintTaxType,
					numTaxID)
               SELECT
               v_numReturnHeaderID,
					TI.numTaxItemID,
					TEMPTax.decTaxValue,
					TEMPTax.tintTaxType,
					0
               FROM
               TaxItems TI
               JOIN
               DivisionTaxTypes DTT
               ON
               TI.numTaxItemID = DTT.numTaxItemID
               CROSS JOIN LATERAL(SELECT
                  decTaxValue,
						tintTaxType,
						numTaxID
                  FROM
                  fn_CalItemTaxAmt(v_numDomainId,v_numDivisionId,0,TI.numTaxItemID,NULL,false,v_numReturnHeaderID)) AS TEMPTax
               WHERE
               DTT.numDivisionID = v_numDivisionId AND DTT.bitApplicable = true
               UNION
               SELECT
               v_numReturnHeaderID,
					0,
					TEMPTax.decTaxValue,
					TEMPTax.tintTaxType,
					0
               FROM
               DivisionMaster
               CROSS JOIN LATERAL(SELECT
                  decTaxValue,
						tintTaxType,
						numTaxID
                  FROM
                  fn_CalItemTaxAmt(v_numDomainId,v_numDivisionId,0,0,NULL,true,v_numReturnHeaderID)) AS TEMPTax
               WHERE
               bitNoTax = false
               AND numDivisionID = v_numDivisionId
               UNION
               SELECT
               v_numReturnHeaderID
					,1
					,decTaxValue
					,tintTaxType
					,numTaxID
               FROM
               fn_CalItemTaxAmt(v_numDomainId,v_numDivisionId,0,1,NULL,true,v_numReturnHeaderID);
            end if;
         end if;
         IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then    
		 
			v_strItems := REPLACE(v_strItems,'<bitMarkupDiscount />','<bitMarkupDiscount>0</bitMarkupDiscount>');
            DROP TABLE IF EXISTS tt_TEMP CASCADE;
            CREATE TEMPORARY TABLE tt_TEMP AS
               SELECT * FROM
			   XMLTABLE
				(
					'NewDataSet/Item'
					PASSING 
						CAST(v_strItems AS XML)
					COLUMNS
						id FOR ORDINALITY,
						numReturnItemID NUMERIC PATH 'numReturnItemID',
						numItemCode NUMERIC PATH 'numItemCode',
						numUnitHour DOUBLE PRECISION PATH 'numUnitHour',
						numUnitHourReceived DOUBLE PRECISION PATH 'numUnitHourReceived',
						monPrice DECIMAL(30,16) PATH 'monPrice',
						monTotAmount DECIMAL(20,5) PATH 'monTotAmount',
						vcItemDesc VARCHAR(1000) PATH 'vcItemDesc',
						numWareHouseItemID NUMERIC PATH 'numWareHouseItemID',
						vcModelID VARCHAR(200) PATH 'vcModelID',
						vcManufacturer VARCHAR(250) PATH 'vcManufacturer',
						numUOMId NUMERIC PATH 'numUOMId',
						numOppItemID NUMERIC PATH 'numOppItemID',
						bitDiscountType BOOLEAN PATH 'bitDiscountType',
						fltDiscount DOUBLE PRECISION PATH 'fltDiscount',
						bitMarkupDiscount BOOLEAN PATH 'bitMarkupDiscount',
						numOppBizDocItemID NUMERIC(18,0) PATH 'numOppBizDocItemID'
				) AS X;

    
            INSERT  INTO ReturnItems(numReturnHeaderID,
				numItemCode,
				numUnitHour,
				numUnitHourReceived,
				monPrice,
				monTotAmount,
				vcItemDesc,
				numWareHouseItemID,
				vcModelID,
				vcManufacturer,
				numUOMId,numOppItemID,numOppBizDocItemID,
				bitDiscountType,fltDiscount
				,bitMarkupDiscount)
            SELECT
            v_numReturnHeaderID,
				numItemCode,
				numUnitHour,
				0,
				monPrice,
				monTotAmount,
				vcItemDesc,
				NULLIF(numWarehouseItemID,0) AS numWareHouseItemID,
				vcModelID,
				vcManufacturer,
				numUOMId,numOppItemID,numOppBizDocItemID,
				bitDiscountType,
				fltDiscount
				,bitMarkupDiscount
            FROM
            tt_TEMP
            WHERE
            numReturnItemID = 0;
            UPDATE
            ReturnItems
            SET
            monAverageCost =(CASE
            WHEN v_numOppId > 0
            THEN(CASE
               WHEN v_tintReturnType = 2
               THEN
                  coalesce((SELECT monPrice FROM OpportunityItems WHERE numOppId = v_numOppId AND numoppitemtCode = ReturnItems.numOppItemID),0)
               ELSE
                  coalesce((SELECT monAvgCost FROM OpportunityItems WHERE numOppId = v_numOppId AND numoppitemtCode = ReturnItems.numOppItemID),0)
               END)
            ELSE
               coalesce((SELECT monAverageCost FROM Item WHERE numItemCode = ReturnItems.numItemCode),0)
            END),monVendorCost =(CASE
            WHEN v_numOppId > 0
            THEN coalesce((SELECT monVendorCost FROM OpportunityItems WHERE numOppId = v_numOppId AND numoppitemtCode = ReturnItems.numOppItemID),0)
            ELSE
               coalesce((SELECT Vendor.monCost FROM Item INNER JOIN Vendor ON Item.numVendorID = Vendor.numVendorID AND Vendor.numItemCode = Item.numItemCode WHERE Item.numItemCode = ReturnItems.numItemCode),0)
            END)
            WHERE
            numReturnHeaderID = v_numReturnHeaderID;
            DROP TABLE IF EXISTS tt_TEMP CASCADE;
            IF v_tintReturnType = 1 OR (v_tintReturnType = 2 AND(SELECT coalesce(bitPurchaseTaxCredit,false) FROM Domain WHERE numDomainId = v_numDomainId) = true) then
			
               IF v_numOppId > 0 then
				
                  INSERT INTO OpportunityItemsTaxItems(numReturnHeaderID,numReturnItemID,numTaxItemID,numTaxID)
                  SELECT
                  v_numReturnHeaderID,OI.numReturnItemID,IT.numTaxItemID,IT.numTaxID
                  FROM
                  ReturnItems OI
                  JOIN
                  OpportunityItemsTaxItems IT
                  ON
                  OI.numOppItemID = IT.numOppItemID
                  WHERE
                  OI.numReturnHeaderID = v_numReturnHeaderID
                  AND IT.numOppId = v_numOppId;
               ELSE
					--Insert Tax for ReturnItems
                  INSERT INTO OpportunityItemsTaxItems(numReturnHeaderID,
						numReturnItemID,
						numTaxItemID,
						numTaxID)
                  SELECT
                  v_numReturnHeaderID,
						OI.numReturnItemID,
						IT.numTaxItemID,
						(CASE WHEN IT.numTaxItemID = 1 THEN numTaxID ELSE 0 END)
                  FROM
                  ReturnItems OI
                  JOIN
                  ItemTax IT
                  ON
                  OI.numItemCode = IT.numItemCode
                  WHERE
                  OI.numReturnHeaderID = v_numReturnHeaderID
                  AND IT.bitApplicable = true;
               end if;
            end if;
         end if; 
            
            -- Update leads,prospects to Accounts
         PERFORM USP_UpdateCRMTypeToAccounts(v_numDivisionId,v_numDomainId,v_numUserCntID);
      ELSEIF v_numReturnHeaderID <> 0
      then
    
         IF coalesce(v_tintMode,0) = 0 then
        
            UPDATE
            ReturnHeader
            SET
            vcRMA = v_vcRMA,numReturnReason = v_numReturnReason,numReturnStatus = v_numReturnStatus,
            monAmount = coalesce(monAmount,0)+coalesce(monTotalDiscount,0) -coalesce(v_monTotalDiscount,0),monTotalDiscount = v_monTotalDiscount,
            tintReceiveType = v_tintReceiveType,vcComments = v_vcComments,
            numModifiedBy = v_numUserCntID,dtModifiedDate = TIMEZONE('UTC',now())
            WHERE
            numDomainID = v_numDomainId
            AND numReturnHeaderID = v_numReturnHeaderID;
         ELSEIF coalesce(v_tintMode,0) = 1
         then
        
            UPDATE
            ReturnHeader
            SET
            numReturnStatus = v_numReturnStatus
            WHERE
            numReturnHeaderID = v_numReturnHeaderID;
            IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then
               DROP TABLE IF EXISTS tt_TEMP1 CASCADE;
               CREATE TEMPORARY TABLE tt_TEMP1 AS
                  SELECT * FROM
				  XMLTABLE
				(
					'NewDataSet/Item'
					PASSING 
						CAST(v_strItems AS XML)
					COLUMNS
						id FOR ORDINALITY,
						numReturnItemID NUMERIC PATH 'numReturnItemID',
						numUnitHourReceived DOUBLE PRECISION PATH 'numUnitHourReceived',
						numWareHouseItemID NUMERIC PATH 'numWareHouseItemID'
				) AS X;

               UPDATE
               ReturnItems
               SET
               numUnitHourReceived = X.numUnitHourReceived,numWareHouseItemID = CASE WHEN X.numWareHouseItemID = 0 THEN NULL ELSE X.numWareHouseItemID END
               FROM
               tt_TEMP1 AS X
               WHERE
               X.numReturnItemID = ReturnItems.numReturnItemID
               AND ReturnItems.numReturnHeaderID = v_numReturnHeaderID;
               DROP TABLE IF EXISTS tt_TEMP1 CASCADE;
               IF EXISTS(SELECT
               RI.numReturnItemID
               FROM
               ReturnItems RI
               INNER JOIN
               WareHouseItems WI
               ON
               RI.numWareHouseItemID = WI.numWareHouseItemID
               INNER JOIN
               OpportunityItems OI
               ON
               RI.numOppItemID = OI.numoppitemtCode
               INNER JOIN
               OpportunityKitItems OKI
               ON
               OI.numoppitemtCode = OKI.numOppItemID
               AND coalesce(OKI.numWareHouseItemId,0) > 0
               INNER JOIN
               Item I
               ON
               OKI.numChildItemID = I.numItemCode
               WHERE
               RI.numReturnHeaderID = v_numReturnHeaderID
               AND coalesce(I.bitKitParent,false) = false
               AND 1 =(CASE WHEN(SELECT COUNT(WIInner.numWareHouseItemID) FROM WareHouseItems WIInner WHERE WIInner.numItemID = OKI.numChildItemID AND  WIInner.numWareHouseID = WI.numWareHouseID AND coalesce(WIInner.numWLocationID,0) = coalesce(WI.numWLocationID,0)) > 0 THEN 0 ELSE 1 END)) then
				
                  RAISE EXCEPTION 'KIT_ITEM_WAREHOUSE_NOT_AVAILABLE_IN_ALL_CHILD_ITEMS';
               end if;
               IF EXISTS(SELECT
               RI.numReturnItemID
               FROM
               ReturnItems RI
               INNER JOIN
               WareHouseItems WI
               ON
               RI.numWareHouseItemID = WI.numWareHouseItemID
               INNER JOIN
               OpportunityItems OI
               ON
               RI.numOppItemID = OI.numoppitemtCode
               INNER JOIN
               OpportunityKitChildItems OKCI
               ON
               OI.numoppitemtCode = OKCI.numOppItemID
               AND coalesce(OKCI.numWareHouseItemId,0) > 0
               INNER JOIN
               Item I
               ON
               OKCI.numItemID = I.numItemCode
               WHERE
               RI.numReturnHeaderID = v_numReturnHeaderID
               AND coalesce(I.bitKitParent,false) = false
               AND 1 =(CASE WHEN(SELECT COUNT(WIInner.numWareHouseItemID) FROM WareHouseItems WIInner WHERE WIInner.numItemID = OKCI.numItemID AND  WIInner.numWareHouseID = WI.numWareHouseID AND coalesce(WIInner.numWLocationID,0) = coalesce(WI.numWLocationID,0)) > 0 THEN 0 ELSE 1 END)) then
				
                  RAISE EXCEPTION 'KIT_ITEM_WAREHOUSE_NOT_AVAILABLE_IN_ALL_CHILD_ITEMS';
               end if;
            end if;
         end if;
      end if;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
         GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;


