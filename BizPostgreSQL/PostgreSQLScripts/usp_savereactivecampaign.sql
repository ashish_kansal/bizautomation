-- Stored procedure definition script usp_SaveReactiveCampaign for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_SaveReactiveCampaign(v_numItemDetailId NUMERIC,
		v_sintYear NUMERIC,
		v_tintquarter NUMERIC,
		v_tintMonth NUMERIC,
		v_FirstCampaignAmount DECIMAL(20,5),
		v_SecondCampaignAmount DECIMAL(20,5),
		v_ThirdCampaignAmount DECIMAL(20,5),
		v_numUserID NUMERIC,
		v_bintDate NUMERIC,
		v_numDomainID NUMERIC)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numReactiveCampaignId  NUMERIC;
BEGIN
   v_numReactiveCampaignId := 0;
   select   numReactiveCampaignID INTO v_numReactiveCampaignId from ReactiveCampaign where
   numItemDetailID = v_numItemDetailId and
   sintYear = v_sintYear and
   tintquarter = v_tintquarter and
   tintMonth = v_tintMonth;
   If v_numReactiveCampaignId < 1 then
		
      Insert into ReactiveCampaign(numItemDetailID, sintYear, tintquarter, tintMonth, FirstCampaignAmount, SecondCampaignAmount, ThirdCampaignAmount, numCreatedBy, bintCreatedDate, numDomainID)
			Values(v_numItemDetailId, v_sintYear, v_tintquarter, v_tintMonth, v_FirstCampaignAmount, v_SecondCampaignAmount, v_ThirdCampaignAmount, v_numUserID, v_bintDate, v_numDomainID);
   ELSE
      Update ReactiveCampaign set FirstCampaignAmount = v_FirstCampaignAmount,SecondCampaignAmount = v_SecondCampaignAmount, 
      ThirdCampaignAmount = v_ThirdCampaignAmount,numModifiedBy = v_numUserID, 
      bintModifiedDate = v_bintDate
      Where numItemDetailID = v_numItemDetailId and sintYear = v_sintYear and tintquarter = v_tintquarter and tintMonth = v_tintMonth;
   end if;
   RETURN;
END; $$;


