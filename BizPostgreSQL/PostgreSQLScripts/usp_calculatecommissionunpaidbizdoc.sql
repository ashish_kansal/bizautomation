-- Stored procedure definition script USP_CalculateCommissionUnPaidBizDoc for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_CalculateCommissionUnPaidBizDoc(v_numComPayPeriodID NUMERIC(18,0),
	 v_numDomainID NUMERIC(18,0),
	 v_ClientTimeZoneOffset INTEGER)
RETURNS VOID LANGUAGE plpgsql
	-- Add the parameters for the stored procedure here
	 
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
   AS $$
   DECLARE
   v_dtPayStart  DATE; 
   v_dtPayEnd  DATE;

   v_numDiscountServiceItemID  NUMERIC(18,0);
   v_numShippingServiceItemID  NUMERIC(18,0);
   v_tintCommissionType  SMALLINT;
   v_bitIncludeTaxAndShippingInCommission  BOOLEAN;
   v_bitCommissionBasedOn  BOOLEAN;
   v_tintCommissionBasedOn  SMALLINT;

   v_i  INTEGER DEFAULT 1;
   v_COUNT  INTEGER DEFAULT 0;

   v_ErrorMessage  VARCHAR(4000);
   v_ErrorNumber  TEXT;
   v_ErrorSeverity  INTEGER;
   v_ErrorState  INTEGER;
   v_ErrorLine  INTEGER;
   v_ErrorProcedure  VARCHAR(200);
   v_numComRuleID  NUMERIC(18,0);
   v_tintComBasedOn  SMALLINT;
   v_tintComAppliesTo  SMALLINT;
   v_tintComOrgType  SMALLINT;
   v_tintComType  SMALLINT;
   v_tintAssignTo  SMALLINT;
   v_numTotalAmount  DECIMAL(20,5);
   v_numTotalUnit  DOUBLE PRECISION;
BEGIN


   BEGIN
      -- BEGIN TRANSACTION
select   dtStart, dtEnd INTO v_dtPayStart,v_dtPayEnd FROM
      CommissionPayPeriod WHERE
      numComPayPeriodID = v_numComPayPeriodID;

	-- DELETE UNPAID COMMISSION ENTRIES BETWEEN PAY PERIOD DATE RANGE
      DELETE FROM
      BizDocComission
      WHERE
      numDomainId = v_numDomainID
      AND coalesce(bitCommisionPaid,false) = false
      AND numOppBizDocItemID IN(SELECT
      coalesce(numOppBizDocItemID,0)
      FROM
      OpportunityBizDocs OppBiz
      INNER JOIN
      OpportunityMaster OppM
      ON
      OppBiz.numoppid = OppM.numOppId
      INNER JOIN
      OpportunityBizDocItems OppBizItems
      ON
      OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
      WHERE
      OppM.numDomainId = v_numDomainID
      AND OppM.tintopptype = 1
      AND OppM.tintoppstatus = 1
      AND OppBiz.bitAuthoritativeBizDocs = 1
      AND coalesce(OppBiz.monAmountPaid,0) < OppBiz.monDealAmount
      AND (OppBiz.dtCreatedDate IS NOT NULL AND CAST(OppBiz.dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS DATE) BETWEEN v_dtPayStart AND v_dtPayEnd));
 
	--GET COMMISSION RULE OF DOMAIN
      BEGIN
         CREATE TEMP SEQUENCE tt_TempCommissionRule_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMPCOMMISSIONRULE CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPCOMMISSIONRULE
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         numComRuleID NUMERIC(18,0),
         tintComBasedOn SMALLINT,
         tintComType SMALLINT,
         tintComAppliesTo SMALLINT,
         tintComOrgType SMALLINT,
         tintAssignTo SMALLINT
      );
      INSERT INTO
      tt_TEMPCOMMISSIONRULE(numComRuleID, tintComBasedOn, tintComType, tintComAppliesTo, tintComOrgType, tintAssignTo)
      SELECT
      CR.numComRuleID,
		CR.tintComBasedOn,
		CR.tintComType,
		CR.tintComAppliesTo,
		CR.tintComOrgType,
		CR.tintAssignTo
      FROM
      CommissionRules CR
      LEFT JOIN
      PriceBookPriorities PBP
      ON
      CR.tintComAppliesTo = PBP.Step2Value
      AND CR.tintComOrgType = PBP.Step3Value
      WHERE
      CR.numDomainID = v_numDomainID
      AND coalesce(PBP.Priority,0) > 0
      ORDER BY
      coalesce(PBP.Priority,0) ASC;

	--GET COMMISSION GLOBAL SETTINGS
      select   numDiscountServiceItemID, numShippingServiceItemID, tintCommissionType, bitIncludeTaxAndShippingInCommission, coalesce(tintCommissionBasedOn,1), coalesce(bitCommissionBasedOn,false) INTO v_numDiscountServiceItemID,v_numShippingServiceItemID,v_tintCommissionType,
      v_bitIncludeTaxAndShippingInCommission,v_tintCommissionBasedOn,v_bitCommissionBasedOn FROM
      Domain WHERE
      numDomainId = v_numDomainID;

	--TEMPORARY TABLES TO HOLD DATA
      DROP TABLE IF EXISTS tt_TEMPBIZCOMMISSION CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPBIZCOMMISSION
      (
         numUserCntID NUMERIC(18,0),
         numOppID NUMERIC(18,0),
         numOppBizDocID NUMERIC(18,0),
         numOppBizDocItemID NUMERIC(18,0),
         monCommission DECIMAL(20,5),
         numComRuleID NUMERIC(18,0),
         tintComType SMALLINT,
         tintComBasedOn SMALLINT,
         decCommission DOUBLE PRECISION,
         tintAssignTo SMALLINT
      );
      BEGIN
         CREATE TEMP SEQUENCE tt_TABLEPAID_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TABLEPAID CASCADE;
      CREATE TEMPORARY TABLE tt_TABLEPAID
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         numDivisionID NUMERIC(18,0),
         numRelationship NUMERIC(18,0),
         numProfile NUMERIC(18,0),
         numAssignedTo NUMERIC(18,0),
         numRecordOwner NUMERIC(18,0),
         numItemCode NUMERIC(18,0),
         numItemClassification NUMERIC(18,0),
         numOppID NUMERIC(18,0),
         numOppBizDocID NUMERIC(18,0),
         numOppBizDocItemID NUMERIC(18,0),
         numUnitHour DOUBLE PRECISION,
         monTotAmount DECIMAL(20,5),
         monVendorCost DECIMAL(20,5),
         monAvgCost DECIMAL(20,5),
         numPartner NUMERIC(18,0),
         numPOID NUMERIC(18,0),
         numPOItemID NUMERIC(18,0),
         monPOCost DECIMAL(20,5)
      );

	-- GET FULLY PAID INVOICES WITHIN PAY PERIOD
      INSERT INTO
      tt_TABLEPAID(numDivisionID, numRelationship, numProfile, numAssignedTo, numRecordOwner, numItemCode, numItemClassification, numOppID, numOppBizDocID, numOppBizDocItemID, numUnitHour, monTotAmount, monVendorCost, monAvgCost, numPartner, numPOID, numPOItemID, monPOCost)
      SELECT
      OppM.numDivisionId,
		coalesce(CompanyInfo.numCompanyType,0),
		coalesce(CompanyInfo.vcProfile,0),
		OppM.numassignedto,
		OppM.numrecowner,
		OppBizItems.numItemCode,
		Item.numItemClassification,
		OppM.numOppId,
		OppBizItems.numOppBizDocID,
		OppBizItems.numOppBizDocItemID,
		coalesce(OppBizItems.numUnitHour,0),
		(CASE WHEN coalesce(OppM.fltExchangeRate,0) = 0 OR coalesce(OppM.fltExchangeRate,0) = 1 THEN coalesce(OppBizItems.monTotAmount,0) ELSE ROUND(CAST(coalesce(OppBizItems.monTotAmount,0)*OppM.fltExchangeRate AS NUMERIC),2) END),
		(coalesce(OppMItems.monVendorCost,0)*coalesce(OppBizItems.numUnitHour,0)),
		(coalesce(OppMItems.monAvgCost,0)*coalesce(OppBizItems.numUnitHour,0)),
		OppM.numPartner,
		coalesce(TEMPMatchedPO.numOppId,TEMPPO.numOppId),
		coalesce(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode),
		coalesce(coalesce(TEMPMatchedPO.monPrice,TEMPPO.monPrice),0)
      FROM
      OpportunityBizDocs OppBiz
      INNER JOIN
      OpportunityMaster OppM
      ON
      OppBiz.numoppid = OppM.numOppId
      INNER JOIN
      DivisionMaster
      ON
      OppM.numDivisionId = DivisionMaster.numDivisionID
      INNER JOIN
      CompanyInfo
      ON
      DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
      INNER JOIN
      OpportunityBizDocItems OppBizItems
      ON
      OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
      INNER JOIN
      OpportunityItems OppMItems
      ON
      OppBizItems.numOppItemID = OppMItems.numoppitemtCode
      INNER JOIN
      Item
      ON
      OppBizItems.numItemCode = Item.numItemCode
      LEFT JOIN LATERAL(SELECT 
         OMInner.numOppId
			,OMInner.vcpOppName
			,OIInner.numoppitemtCode
			,OIInner.monPrice
         FROM
         SalesOrderLineItemsPOLinking SOLIPL
         INNER JOIN
         OpportunityMaster OMInner
         ON
         SOLIPL.numPurchaseOrderID = OMInner.numOppId
         INNER JOIN
         OpportunityItems OIInner
         ON
         OMInner.numOppId = OIInner.numOppId
         AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
		 AND COALESCE(SOLIPL.numSalesOrderOppChildItemID,0) = 0 AND COALESCE(SOLIPL.numSalesOrderOppKitChildItemID,0) = 0
         WHERE
         SOLIPL.numSalesOrderID = OppM.numOppId
         AND SOLIPL.numSalesOrderItemID = OppMItems.numoppitemtCode LIMIT 1) TEMPMatchedPO
      LEFT JOIN LATERAL(SELECT 
         OMInner.numOppId
			,OMInner.vcpOppName
			,OIInner.numoppitemtCode
			,OIInner.monPrice
         FROM
         OpportunityLinking OL
         INNER JOIN
         OpportunityMaster OMInner
         ON
         OL.numChildOppID = OMInner.numOppId
         INNER JOIN
         OpportunityItems OIInner
         ON
         OMInner.numOppId = OIInner.numOppId
         WHERE
         OL.numParentOppID = OppM.numOppId
         AND OIInner.numItemCode = OppMItems.numItemCode
         AND OIInner.vcAttrValues = OppMItems.vcAttrValues LIMIT 1) TEMPPO on TRUE on TRUE
      WHERE
      OppM.numDomainId = v_numDomainID
      AND OppM.tintopptype = 1
      AND OppM.tintoppstatus = 1
      AND OppBiz.bitAuthoritativeBizDocs = 1
      AND coalesce(OppBiz.monAmountPaid,0) < OppBiz.monDealAmount
      AND (OppBiz.dtCreatedDate IS NOT NULL AND CAST(OppBiz.dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS DATE) BETWEEN v_dtPayStart AND v_dtPayEnd)
      AND OppBizItems.numItemCode NOT IN(v_numDiscountServiceItemID)  /*DO NOT INCLUDE DISCOUNT ITEM*/
      AND 1 =(CASE
      WHEN OppBizItems.numItemCode = v_numShippingServiceItemID
      THEN
         CASE
         WHEN coalesce(v_bitIncludeTaxAndShippingInCommission,false) = true
         THEN
            1
         ELSE
            0
         END
      ELSE
         1
      END);/*CONDITION TO INCLUDE/EXCLUDE INCLUDE SHIPPING ITEM */


		-- LOOP ALL COMMISSION RULES
      select   COUNT(*) INTO v_COUNT FROM tt_TEMPCOMMISSIONRULE;
      DROP TABLE IF EXISTS tt_TEMPITEM CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPITEM
      (
         ID INTEGER,
         UniqueID INTEGER,
         numUserCntID NUMERIC(18,0),
         numOppID NUMERIC(18,0),
         numOppBizDocID NUMERIC(18,0),
         numOppBizDocItemID NUMERIC(18,0),
         numItemCode NUMERIC(18,0),
         numUnitHour DOUBLE PRECISION,
         monTotAmount DECIMAL(20,5),
         monVendorCost DECIMAL(20,5),
         monAvgCost DECIMAL(20,5),
         numPOID NUMERIC(18,0),
         numPOItemID NUMERIC(18,0),
         monPOCost DECIMAL(20,5)
      );
		

			--CLEAR PREVIOUS VALUES FROM TEMP TABLE
      WHILE v_i <= v_COUNT LOOP
         DELETE FROM tt_TEMPITEM;

			--FETCH COMMISSION RULE
         select   numComRuleID, tintComBasedOn, tintComType, tintComAppliesTo, tintComOrgType, tintAssignTo INTO v_numComRuleID,v_tintComBasedOn,v_tintComType,v_tintComAppliesTo,v_tintComOrgType,
         v_tintAssignTo FROM tt_TEMPCOMMISSIONRULE WHERE ID = v_i;

			--FIND ALL BIZ DOC ITEMS WHERE COMMISSION IS NOT PAID AND CURRENT RULE ITEMS, ORGANIZATION AND ASSINGED TO FILTER CAN BE APPLIED
         INSERT INTO
         tt_TEMPITEM
         SELECT
         ROW_NUMBER() OVER(ORDER BY ID),
				ID,
				(CASE v_tintAssignTo WHEN 1 THEN numAssignedTo WHEN 2 THEN numRecordOwner ELSE numPartner END),
				numOppID,
				numOppBizDocID,
				numOppBizDocItemID,
				numItemCode,
				numUnitHour,
				monTotAmount,
				monVendorCost,
				monAvgCost,
				numPOID,
				numPOItemID,
				monPOCost
         FROM
         tt_TABLEPAID
         WHERE
         1 =(CASE v_tintComAppliesTo
						--SPECIFIC ITEMS
         WHEN 1 THEN CASE WHEN(SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = v_numComRuleID AND coalesce(numValue,0) = numItemCode) > 0 THEN 1 ELSE 0 END
						-- ITEM WITH SPECIFIC CLASSIFICATIONS
         WHEN 2 THEN CASE WHEN(SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = v_numComRuleID AND coalesce(numValue,0) = numItemClassification) > 0 THEN 1 ELSE 0 END
						-- ALL ITEMS
         ELSE 1
         END)
         AND 1 =(CASE v_tintComOrgType
							-- SPECIFIC ORGANIZATION
         WHEN 1 THEN CASE WHEN(SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = v_numComRuleID AND coalesce(numValue,0) = numDivisionID) > 0 THEN 1 ELSE 0 END
							-- ORGANIZATION WITH SPECIFIC RELATIONSHIP AND PROFILE
         WHEN 2 THEN CASE WHEN(SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = v_numComRuleID AND coalesce(numValue,0) = numRelationship AND coalesce(numProfile,0) = numProfile) > 0 THEN 1 ELSE 0 END
							-- ALL ORGANIZATIONS
         ELSE 1
         END)
         AND 1 =(CASE v_tintAssignTo 
							-- ORDER ASSIGNED TO
         WHEN 1 THEN CASE WHEN(SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = v_numComRuleID AND numValue = numAssignedTo) > 0 THEN 1 ELSE 0 END
							-- ORDER OWNER
         WHEN 2 THEN CASE WHEN(SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = v_numComRuleID AND numValue = numRecordOwner) > 0 THEN 1 ELSE 0 END
							-- ORDER OWNER
         WHEN 3 THEN CASE WHEN(SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = v_numComRuleID AND numValue = numPartner) > 0 THEN 1 ELSE 0 END
							-- NO OPTION SELECTED IN RULE
         ELSE 0
         END);

			--CALCULATE COMMISSION AND STORE IN TEMPARORY TABLE
         INSERT INTO tt_TEMPBIZCOMMISSION(numUserCntID,
				numOppID,
				numOppBizDocID,
				numOppBizDocItemID,
				monCommission,
				numComRuleID,
				tintComType,
				tintComBasedOn,
				decCommission,
				tintAssignTo)
         SELECT
         numUserCntID,
				numOppID,
				numOppBizDocID,
				numOppBizDocItemID,
				CASE v_tintComType
         WHEN 1 --PERCENT
         THEN
            CASE
            WHEN coalesce(v_bitCommissionBasedOn,true) = true
            THEN(CASE
               WHEN numPOItemID IS NOT NULL
               THEN(coalesce(monTotAmount,0) -coalesce(monPOCost,0))*(T2.decCommission/CAST(100 AS DOUBLE PRECISION))
               ELSE CASE v_tintCommissionBasedOn
												--ITEM GROSS PROFIT (VENDOR COST)
                  WHEN 1 THEN(coalesce(monTotAmount,0) -coalesce(monVendorCost,0))*(T2.decCommission/CAST(100 AS DOUBLE PRECISION))
												--ITEM GROSS PROFIT (AVERAGE COST)
                  WHEN 2 THEN(coalesce(monTotAmount,0) -coalesce(monAvgCost,0))*(T2.decCommission/CAST(100 AS DOUBLE PRECISION))
                  END
               END)
            ELSE
               monTotAmount*(T2.decCommission/CAST(100 AS DOUBLE PRECISION))
            END
         ELSE  --FLAT
            T2.decCommission
         END,
				v_numComRuleID,
				v_tintComType,
				v_tintComBasedOn,
				T2.decCommission,
				v_tintAssignTo
         FROM
         tt_TEMPITEM AS T1
         CROSS JOIN LATERAL(SELECT 
            coalesce(decCommission,0) AS decCommission
            FROM
            CommissionRuleDtl
            WHERE
            numComRuleID = v_numComRuleID
            AND coalesce(decCommission,0) > 0
            AND 1 =(CASE
            WHEN v_tintComBasedOn = 1 --BASED ON AMOUNT SOLD 
            THEN(CASE WHEN T1.monTotAmount BETWEEN intFrom:: DECIMAL(20,5) AND intTo:: DECIMAL(20,5) THEN 1 ELSE 0 END)
            WHEN v_tintComBasedOn = 2 --BASED ON UNITS SOLD
            THEN(CASE WHEN T1.numUnitHour BETWEEN intFrom:: DOUBLE PRECISION AND intTo:: DOUBLE PRECISION THEN 1 ELSE 0 END)
            ELSE 0
            END) LIMIT 1) AS T2
         WHERE(CASE
         WHEN coalesce(v_bitCommissionBasedOn,true) = true
         THEN(CASE
            WHEN numPOItemID IS NOT NULL
            THEN(coalesce(monTotAmount,0) -coalesce(monPOCost,0))*(T2.decCommission/CAST(100 AS DOUBLE PRECISION))
            ELSE CASE v_tintCommissionBasedOn
									--ITEM GROSS PROFIT (VENDOR COST)
               WHEN 1 THEN(coalesce(monTotAmount,0) -coalesce(monVendorCost,0))*(T2.decCommission/CAST(100 AS DOUBLE PRECISION))
									--ITEM GROSS PROFIT (AVERAGE COST)
               WHEN 2 THEN(coalesce(monTotAmount,0) -coalesce(monAvgCost,0))*(T2.decCommission/CAST(100 AS DOUBLE PRECISION))
               END
            END)
         ELSE
            monTotAmount*(T2.decCommission/CAST(100 AS DOUBLE PRECISION))
         END) > 0
         AND(SELECT
            COUNT(*)
            FROM
            tt_TEMPBIZCOMMISSION
            WHERE
            numUserCntID = T1.numUserCntID
            AND numOppID = T1.numOppID
            AND numOppBizDocID = T1.numOppBizDocID
            AND numOppBizDocItemID = T1.numOppBizDocItemID
            AND tintAssignTo = v_tintAssignTo) = 0;
         v_i := v_i::bigint+1;
         v_numComRuleID := 0;
         v_tintComBasedOn := 0;
         v_tintComAppliesTo := 0;
         v_tintComOrgType := 0;
         v_tintComType := 0;
         v_tintAssignTo := 0;
         v_numTotalAmount := 0;
         v_numTotalUnit := 0;
      END LOOP;

		--INSERT CALCULATED ENTERIES IN BIZDOCCOMMISSION TABLE
      INSERT INTO BizDocComission(numDomainId,
			numUserCntID,
			numOppBizDocId,
			numComissionAmount,
			numOppBizDocItemID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			numOppID,
			bitFullPaidBiz,
			numComPayPeriodID,
			tintAssignTo)
      SELECT
      v_numDomainID,
			numUserCntID,
			numOppBizDocID,
			monCommission,
			numOppBizDocItemID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			numOppID,
			false,
			v_numComPayPeriodID,
			tintAssignTo
      FROM
      tt_TEMPBIZCOMMISSION;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
         -- ROLLBACK TRANSACTION
v_ErrorMessage := SQLERRM;
         v_ErrorNumber := sqlstate;
         v_ErrorSeverity := ERROR_SEVERITY();
         v_ErrorState := ERROR_STATE();
         v_ErrorLine := ERROR_LINE();
         v_ErrorProcedure := coalesce(ERROR_PROCEDURE(),'-');
         RAISE NOTICE '% % % % % % ',v_ErrorMessage,v_ErrorNumber,v_ErrorSeverity,v_ErrorState,v_ErrorProcedure,
         v_ErrorLine;
   END;
   RETURN;
END; $$;



