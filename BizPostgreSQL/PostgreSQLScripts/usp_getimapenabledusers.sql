-- FUNCTION: public.usp_getimapenabledusers(smallint, numeric, refcursor)

-- DROP FUNCTION public.usp_getimapenabledusers(smallint, numeric, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getimapenabledusers(
	v_tinymode smallint DEFAULT 0,
	v_numdomainid numeric DEFAULT 0,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
   IF v_tinyMode = 0 then
      open SWV_RefCur for
      SELECT DISTINCT
      I.numDomainID
		,I.numUserCntID
		,I.vcImapServerUrl
		,I.vcImapPassword
		,I.bitSSl
		,I.numPort
		,I.bitUseUserName
		,I.tintRetryLeft
		,I.numLastUID
		,I.bitNotified
		,I.vcGoogleRefreshToken
		,I.vcImapUserName
      FROM
      ImapUserDetails I
      INNER JOIN
      Subscribers S
      ON
      S.numTargetDomainID = I.numDomainID
      WHERE
      bitImap = true
      AND S.bitActive = 1
      UNION
      SELECT
      U.numDomainID
		,U.numUserDetailId AS numUserCntId
		,'' AS vcImapServerUrl
		,'' AS vcImapPassword
		,false AS bitSSl
		,0 AS numPort
		,false AS bitUseUserName
		,0 AS tintRetryLeft
		,0 AS numLastUID
		,false AS bitNotified
		,'' AS vcGoogleRefreshToken
		,'' AS vcImapUserName
      FROM
      UserMaster U
      INNER JOIN
      Subscribers S
      ON
      S.numTargetDomainID = U.numDomainID
      WHERE
		(U.tintMailProvider = 1 OR U.tintMailProvider = 2)
      AND coalesce(vcMailAccessToken,'') <> ''
      AND coalesce(vcMailRefreshToken,'') <> ''
      AND coalesce(bitOauthImap,false) = true
      AND S.bitActive = 1;
   ELSEIF v_tinyMode = 1
   then --Get users to whom we need to send notification mail about error in last 10 attempt.
      open SWV_RefCur for
      SELECT DISTINCT I.numDomainID,numUserCntId,coalesce(UM.vcEmailID,'') AS vcEmailID FROM ImapUserDetails I
      JOIN UserMaster UM ON UM.numUserDetailId = I.numUserCntID
      join Subscribers S on S.numTargetDomainID = I.numDomainID
      WHERE bitImap = false AND bitNotified = false AND tintRetryLeft = 0 and S.bitActive = 1;
   ELSEIF v_tinyMode = 2
   then  --Google to Biz Contact Distinct Domain 
      open SWV_RefCur for
      SELECT DISTINCT I.numDomainID FROM ImapUserDetails I JOIN Domain D ON I.numDomainID = D.numDomainId
      join Subscribers S on S.numTargetDomainID = I.numDomainID
      WHERE I.bitImap = true AND coalesce(D.bitGtoBContact,false) = true and S.bitActive = 1;
   ELSEIF v_tinyMode = 3
   then --Google to Biz Calendar
      open SWV_RefCur for
      SELECT DISTINCT I.numDomainID,I.numUserCntID,I.* FROM ImapUserDetails I JOIN Domain D ON I.numDomainID = D.numDomainId
      join Subscribers S on S.numTargetDomainID = I.numDomainID
      join UserAccessedDTL UAD on UAD.numContactID = I.numUserCntID
      WHERE I.bitImap = true AND coalesce(D.bitGtoBCalendar,false) = true and S.bitActive = 1 and LENGTH(coalesce(vcGoogleRefreshToken,'')) > 0;
   ELSEIF v_tinyMode = 4
   then --Biz to Google Calendar
      open SWV_RefCur for
      SELECT DISTINCT I.numDomainID,I.numUserCntID,I.* FROM ImapUserDetails I JOIN Domain D ON I.numDomainID = D.numDomainId
      join Subscribers S on S.numTargetDomainID = I.numDomainID
      join UserAccessedDTL UAD on UAD.numContactID = I.numUserCntID
      WHERE I.bitImap = true AND coalesce(D.bitBtoGCalendar,false) = true and S.bitActive = 1 and LENGTH(coalesce(vcGoogleRefreshToken,'')) > 0;
   ELSEIF v_tinyMode = 5
   then  --Google to Biz Contact Domain 
      open SWV_RefCur for
      SELECT DISTINCT I.numDomainID,I.numUserCntID,I.* FROM ImapUserDetails I JOIN Domain D ON I.numDomainID = D.numDomainId
      join Subscribers S on S.numTargetDomainID = I.numDomainID
      WHERE I.bitImap = true AND coalesce(D.bitGtoBContact,false) = true AND I.numDomainID = v_numDomainId and S.bitActive = 1;
   ELSEIF v_tinyMode = 6
   then  --Biz to Google Email
      open SWV_RefCur for
      SELECT DISTINCT I.numDomainID,I.numUserCntID,I.* FROM ImapUserDetails I JOIN Domain D ON I.numDomainID = D.numDomainId
      join Subscribers S on S.numTargetDomainID = I.numDomainID
      WHERE I.bitImap = true AND coalesce(D.bitGtoBContact,false) = true AND I.numDomainID = v_numDomainId and S.bitActive = 1;
   end if;
   RETURN;
END;
$BODY$;

ALTER FUNCTION public.usp_getimapenabledusers(smallint, numeric, refcursor)
    OWNER TO postgres;
