-- Stored procedure definition script USP_ProjectSaveMilestone for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ProjectSaveMilestone(v_numProId NUMERIC(9,0) DEFAULT null,                    
 v_strMilestone TEXT DEFAULT '')
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_status  SMALLINT;                    
   v_hDoc3  INTEGER;
BEGIN
   if SUBSTR(CAST(v_strMilestone AS VARCHAR(10)),1,10) <> '' then

      SELECT * INTO v_hDoc3 FROM SWF_Xml_PrepareDocument(v_strMilestone);
      delete from ProjectsStageDetails
      where numProStageId not in(SELECT StageID FROM SWF_OpenXml(v_hDoc3,'/NewDataSet/Table','StageID |Op_Flag') SWA_OpenXml(StageID NUMERIC,Op_Flag NUMERIC))
      and numProId =  v_numProId;
      update ProjectsStageDetails set
      vcstagedetail = X.vcstageDetail,numModifiedBy = X.numModifiedBy,bintModifiedDate = X.bintModifiedDate,
      bintDueDate = X.bintDueDate,bintStageComDate = X.bintStageComDate,
      vcComments = X.vcComments,numAssignTo = X.numAssignTo,bitAlert = X.bitAlert,
      bitStageCompleted = X.bitStageCompleted,numStage = X.numStage,
      vcStagePercentageDtl = x.vcStagePercentageDtl,numTemplateId = x.numTemplateId,
      tintPercentage = X.tintPercentage,numEvent = X.numEvent,numReminder = X.numReminder,
      numET = X.numET,numActivity = X.numActivity,numStartDate = X.numStartDate,
      numStartTime = X.numStartTime,numStartTimePeriod = X.numStartTimePeriod,
      numEndTime = X.numEndTime,numEndTimePeriod = X.numEndTimePeriod,
      txtCom = X.txtCom,numChgStatus = x.numChgStatus,bitChgStatus = x.bitChgStatus,
      bitClose = x.bitClose,numType = x.numType,numCommActId = x.numCommActId
      From(SELECT * FROM SWF_OpenXml(v_hDoc3,'/NewDataSet/Table[StageID>0][Op_Flag=0]','StageID |vcstageDetail |numModifiedBy |bintModifiedDate |bintDueDate |bintStageComDate |vcComments |numAssignTo |bitAlert |bitStageCompleted |Op_Flag |numStage |vcStagePercentageDtl |numTemplateId |tintPercentage |numEvent |numReminder |numET |numActivity |numStartDate |numStartTime |numStartTimePeriod |numEndTime |numEndTimePeriod |txtCom |numChgStatus |bitChgStatus |bitClose |numType |numCommActId') SWA_OpenXml(StageID NUMERIC,vcstageDetail VARCHAR(100),numModifiedBy NUMERIC,bintModifiedDate TIMESTAMP,
      bintDueDate VARCHAR(23),bintStageComDate TIMESTAMP,
      vcComments VARCHAR(1000),numAssignTo NUMERIC,bitAlert BOOLEAN,bitStageCompleted BOOLEAN,
      Op_Flag NUMERIC,numStage NUMERIC,vcStagePercentageDtl VARCHAR(500),
      numTemplateId NUMERIC,tintPercentage SMALLINT,numEvent SMALLINT,
      numReminder NUMERIC,numET NUMERIC,numActivity NUMERIC,numStartDate SMALLINT,
      numStartTime SMALLINT,numStartTimePeriod SMALLINT,numEndTime SMALLINT,
      numEndTimePeriod SMALLINT,txtCom VARCHAR(1000),numChgStatus NUMERIC(9,0),
      bitChgStatus BOOLEAN,bitClose BOOLEAN,numType NUMERIC(9,0),
      numCommActId NUMERIC(9,0))) X
      where  numProStageId = X.StageID;
      insert into ProjectsStageDetails(numProId,vcstagedetail,numstagepercentage,numDomainID,
  numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,
  bintDueDate,bintStageComDate,vcComments,numAssignTo,bitAlert,
  bitStageCompleted,numStage,vcStagePercentageDtl,numTemplateId,tintPercentage,numEvent,numReminder,numET,numActivity ,numStartDate,numStartTime,numStartTimePeriod ,numEndTime ,
numEndTimePeriod, txtCom,numChgStatus ,bitChgStatus  ,bitClose,numType,numCommActId)
      select v_numProId,
  X.vcstageDetail ,
  X.numStagePercentage ,
  X.numDomainId,
  X.numCreatedBy ,
  X.bintCreatedDate ,
  X.numModifiedBy ,
  X.bintModifiedDate ,
  X.bintDueDate ,
  X.bintStageComDate ,
  X.vcComments ,
  X.numAssignTo,
  X.bitAlert ,
  X.bitStageCompleted,
  X.numStage,x.vcStagePercentageDtl,x.numTemplateId,X.tintPercentage,X.numEvent,
 X.numReminder,
 X.numET,
 X.numActivity ,
 X.numStartDate,
 X.numStartTime,
 X.numStartTimePeriod ,
 X.numEndTime ,
 X.numEndTimePeriod,
 X.txtCom ,x.numChgStatus ,x.bitChgStatus  ,x.bitClose,x.numType,x.numCommActId  from(SELECT *FROM SWF_OpenXml(v_hDoc3,'/NewDataSet/Table[StageID=0][Op_Flag=0]','StageID |vcstageDetail |numStagePercentage |numDomainId |numCreatedBy |bintCreatedDate |numModifiedBy |bintModifiedDate |bintDueDate |bintStageComDate |vcComments |numAssignTo |bitAlert |bitStageCompleted |Op_Flag |numStage |vcStagePercentageDtl |numTemplateId |tintPercentage |numEvent |numReminder |numET |numActivity |numStartDate |numStartTime |numStartTimePeriod |numEndTime |numEndTimePeriod |txtCom |numChgStatus |bitChgStatus |bitClose |numType |numCommActId') SWA_OpenXml(StageID NUMERIC,vcstageDetail VARCHAR(100),numStagePercentage NUMERIC,
         numDomainId NUMERIC,numCreatedBy NUMERIC,bintCreatedDate TIMESTAMP,numModifiedBy NUMERIC,
         bintModifiedDate TIMESTAMP,bintDueDate VARCHAR(23),
         bintStageComDate TIMESTAMP,vcComments VARCHAR(1000),numAssignTo NUMERIC,
         bitAlert BOOLEAN,bitStageCompleted BOOLEAN,Op_Flag NUMERIC,numStage NUMERIC,
         vcStagePercentageDtl VARCHAR(500),numTemplateId NUMERIC,tintPercentage SMALLINT,
         numEvent SMALLINT,numReminder NUMERIC,numET NUMERIC,
         numActivity NUMERIC,numStartDate SMALLINT,numStartTime SMALLINT,numStartTimePeriod SMALLINT,
         numEndTime SMALLINT,numEndTimePeriod SMALLINT,txtCom VARCHAR(1000),
         numChgStatus NUMERIC(9,0),bitChgStatus BOOLEAN,bitClose BOOLEAN,
         numType NUMERIC(9,0),numCommActId NUMERIC(9,0))) X;
      PERFORM SWF_Xml_RemoveDocument(v_hDoc3);
      select   bitstageCompleted INTO v_status from ProjectsMaster mst
      left join  ProjectsStageDetails  stg
      on mst.numProId = stg.numProId where stg.numstagepercentage = 100 and mst.numProId = v_numProId;
      if v_status = 1 then
 
         update ProjectsMaster set tintProStatus = 1,bintProClosingDate = TIMEZONE('UTC',now()) where numProId = v_numProId and tintProStatus <> 1;
      end if;
   end if;
   RETURN;
END; $$;


