-- Stored procedure definition script USP_Project_GetStages for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Project_GetStages(v_numDomainID NUMERIC(18,0)
	,v_numProId NUMERIC(18,0)
	,v_numMileStoneID NUMERIC(18,0)
	,v_vcMilestoneName VARCHAR(500),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   v_numMileStoneID AS numMileStoneID
		,StagePercentageDetails.numStageDetailsId 
		,StagePercentageDetails.vcStageName AS vcStageName
		,GetTotalProgress(v_numDomainID,v_numProId,2::SMALLINT,3::SMALLINT,'',StagePercentageDetails.numStageDetailsId) AS numTotalProgress
   FROM
   StagePercentageDetails
   WHERE
   StagePercentageDetails.numdomainid = v_numDomainID
   AND StagePercentageDetails.numProjectid = v_numProId
   AND LOWER(vcMileStoneName) = LOWER(v_vcMilestoneName);
END; $$;













